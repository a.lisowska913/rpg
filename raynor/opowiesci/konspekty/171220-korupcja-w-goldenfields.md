---
layout: inwazja-konspekt
title:  "Korupcja w Goldenfields"
campaign: raynor-prime
players: krzysiu, michał
gm: raynor
categories: raynor, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [171220 - Korupcja w Goldenfields](171220-korupcja-w-goldenfields.html)

### Chronologiczna

* [171220 - Korupcja w Goldenfields](171220-korupcja-w-goldenfields.html)

## Informacje

### Bohaterowie

#### Organizacje

* **Władza miasta Goldenfields** - urzędnicy, strażnicy miejscy.
* **Srebrne Lisy** - złodzieje, hitmani, drabony i żebracy. Zasięg lokalny - miasto. Frontem ich dzialanosci jest legalna firma Lisia Kuznia sp. z o. o.
* **Drewniany Jastrząb** - organizacja konkurencyjna do Srebrnych Lisów, o szerszym zasięgu działa na terenie wielu miast.

#### Postacie Graczy

* Dankan Kartson - kapitan Straży Miejskiej w Goldenfields (Krzysiu), nie jest swiadomy swoich umiejetnosci magicznych, czasem uzywa ich przypadkiem.
* Jasper Firetorcher - złodziejaszek z lokalnej organizacji przestępczej Srebrne Lisy (Michał)

#### NPCe

* **komendant Samuel Icek** - prawdopodobnie kupionny ostatnio przez Drewnianego Jastrzebia, rupert grint hitman grupy Srebrne Lisy
* **sierżant Kolon** - poważny, prawie emerytowany strażnik, praworządny, nie skorumpowany - alkoholik.
* **Brudny Harry** (bo caly czas upierdolony) - nieoficjalny lider Srebrnych Lisow.
* **Krab i Goil** - młode zlodziejaszki/stażyści ze Srebrnych Lisów 
* **Burmistrz Vaern Artofis** - ork, siedzi w kieszeni srebrnych lisow
* **Lech Slaw** - bandzior, oprych pracujacy dla Drewnianego Jstrzebia (lokalny przedstawiciel), przemytnik, kontakt komendanta strazy miejskiej
* **Rupert Grunt** - hitman wynajęty do zamordowania Lecha Slawa przez Srebrne Lisy.

### Opowieść:

#### PROBLEM:

Korupcja w mieście Goldenfields.

#### LOKALIZACJA:

Goldenfields was a walled abbey to Chauntea of spectacular size covering over 20 square miles and housing over 5,000 people. The settlement was founded as an abbey around 1359 DR by Tolgar Anuvien, but in a few years Goldenfields grew into a fortified farmland

Straznikow Miejskich w miescie 19.

#### PYTANIA SESYJNE:

* Czy zły komendant straży miejskiej zostanie zdymisjonowany? (Gram na: NIE)
* Czy miasto popadnie w chaos występku i bezprawia? (Gram na: TAK)
* Czy bohaterowie zostaną fałszywie oskarżeni i trafią do więzienia za czyny skorumpowanej straży miejskiej? (Gram na: TAK)

#### PYTANIA EKSPLORACYJNE:

* MG: Co robi w miescie Dankan?
* GRACZ: Zaprowadza ład i porządek, jako strażnik. Jest kapitanem, ale niedocenionym, bo jest zbyt praworzadny i idealistyczny. 
* MG: Co robi Jasper w Goldenfields?
* GRACZ: Jest w grupie rzezimieszków, wylądawal tu przypadkiem, kradną i sobie żyja. Robi robótki na miescie, jako zreczny zlodziej.
* MG: Jak bardzo niemoralny jest Jasper?
* GRACZ: Ceni życie, nie morduje, po prostu kradnie. Ale zlych ludzi może zabić.
* MG: Czy przyjąłby zlecenie morderstwa?
* GRACZ: Tylko w wyjatkowych sytuacjach, ale nie chce tego robić.
* MG: Jaka relacja łączy Dankana z komendantem straży?
* GRACZ: W jego mniemaniu dobra, ale komendant ma mieszane uczucia, bo musi na niego uwazać przy kreceniu lodów na lewo. Nie może Dankana wywalić, bo Dankan ma zbyt duży szacunek u obywateli.
* MG: Jaka dokłądnie jest opinia Dankana w mieście?
* GRACZ: Wśród praworzadnych obywateli jest bardzo dobra, ale ma malo kontaktow w wyzszych kregach ludzi w miescie. Nie lubi korupcji.
* MG: Jakie relacje ma Jasper ze straza miejska?
* GRACZ: Raczej nie wspolpracuja, unika i nie dazy do konfliktu. Jest zuchwaly, ale nie glupi.
* MG: Dlaczego odmowil ostatnio zlecenia, za ktore bylo bardzo duzo pieniedzy?
* GRACZ: Zlecenie dotyczyło wyciągnięcia czegoś z lochow miejskich w siedzibie miasta. Zbyt duze ryzyko. Szczegolnie, ze jego towarzysze nie chcieli sie w to mieszac.
* MG: Dankan, czemu akurat ty masz dzisiaj nocna zmiane w siedzibie miasta? 
* GRACZ: Taki grafik, nie dyskutuje z nim. Jest pracoholikiem i wszyscy go wykorzystują.
* MG: Jak wyglada ostatnie spiecie pomiedzy Dankanem, a Jasperem?
* GRACZ: Jasper mial robote w domu szlachcica, ktos go przywuazyl i wezwal straz. Pojawil sie Dankan Karotson i probowal go zlapac, ale sie nie udalo. Jasper wykpil go bezlitośnie uciekajac. 

#### Raport właściwy:

Dankan jest w siedzibie strazy, ma nocna zmiane. Patroluje budynek. Slyszy halas.

Schodzi do lochów, bo halas, ale nikogo nie ma. Wraca do biurka. Znowu hałas i znowu schodzi na dół. Dalej nic. Schodzi tak 3 razy, dopóki coś nie jebło - wybuch w lochu. Zbiega na dół, patrzy Kraty wysadzone, a jedyny więzień Rubert ucieka z dwoma ludźmi. Karotson dogonil Ruperta i unieruchomił, ale komendant staży zostal zaalarmowany i przyszedl zobaczyć co jest grane. Teraz wie, ze za wszelka cene musi sie go pozbyc. Karotson wzial Ruberta i probowal go przesluchiwac.

Meczyl, gnebil, nie pozwalal spac. Po jakims czasie, udalo mu sie wyciagnac gdzie jest siedziba srebrnych lisow, ale powiedzal ze sa bardzo wazne w miescie osoby zamieszane w tą drakę - nie chce wchodzic w te sprawy. Nagle do pokoju przesluchan wchodzi Klaudyna - sprzataczka.

Dwoch zlodziejaszkow wpada do siedziby srebrnych lisow. Zziajani, zmeczeni, obdrapani. Okazuje sie, ze sfailowali akcje - Dankan Karotson zlapal Ruperta, ktory zostal wynajety do ataku na szefa konkurencyjnego gangu - Drewnianego Jastrzebia. Jasper jest w siedzibie razem z Brudnym Harrym i oboje przesłuchują złodziejaszków - Kraba i Goila. W mieście źle się dzieje, skorumpowana straż miejska współpracuje z konkurencyjnym gangiem, morderstwa i porwania. Srebnre Lisy chcą interweniować i odzyskać kontrolę. Ustalone zostalo, ze Srebrne Lisy w postaci Jaspera dogadaja sie z Dankanem, zeby wspolpracowac i usunac skorumpowanego komendanta i  przedstawicieli Drewnianego Jastrzebia, ktora jest organizacja o wiele bardziej zla - zabijaja ludzi, porywaja i zjadaja, sprzedaja na niewolnikow i prostututki. Jasper pojdzie z nim porozmawiac.

Poludnie, Dankan Karotson wstal po nocnej zmianie. W porannej gazecie przeczytal, ze komendant uznal ze to w strazy miejskiej sa ludzie zamieszani we wspolprace ze swiatem przestepczymi. Zamierza wszcząć postępowanie. Obarcza winą oficerów średniego szczebla, którzy na pewno układają się z bandytami. Choc nie powiedzial tego otwarcie, to postepowanie bedzie wymierzone przeciwko Karotsonowi, ktorego komendant chce usunac.

Jasper wysluchal dokładnie historii mlodziakow wysłąnych do odbicia Ruperta Kraba i Gojla - ogolnie typowi juniorzy, spieprzyli wszystko co sie da. Wzial jednego z nich Kraba, bo potrafi wladac podstawami magii materii. Wyruszaja do Dankana Karotsona. Jasper rzuca zaklecie mentalne na mlodego, zeby nie spierdolil i stal na czatach, ostrzegał jakby straż lub wrogowie chcieli ich dopaść. Nie znajduje niczego w mieszkaniu Karotsona, wiec zostawia Karotsonowi list, wzywajacy go do spotkania.

W miedzyczasie Karotson umawia się z sierżantem Kolonem do knajpy. Chce na własną rękę przeprowadzić śledztwo. Kolon nie chce, ale Karotson go przekupuje tym, że będzie wykonywać pracę za niego. Idzie do knajpy kolo siedziby Srebrnych Lisow - "Pod Brudnym Wiepszem" (z bledem). Dzielnica handlowa. Sredniej jakości knajpa. Knajpa jest pod kuratelą Srebrnych Lisow i kilku przedstawicieli organizacji tam jest. Idą po cywilnemu.

Nagle do knajpy wpada ekipa 13 drabonow i robi rozpierdziel. Karotson wpada w wir walki, zeby uspokoic sytuacje (lol). Wbiega tez Jasper, ktory akurat wrocil z mieszkania Dankana Karotsona z reszta ekipy Srebrnych Lisow. Jest to atak definitywnie skierowany przeciw Srebrnym Lisom. Udaje im sie odepchnac atak i zdobyc jednego goscia do przesluchania. Niestety idzie fama ze Srebrne lisy wraz z czlonkami strazy miejskiej brali udzial w burdzie. Zaczyna sie klotnia o drabona,, kto go wezmie i przeslucha? Jasper i Dankan konfliktuja. Dankan chce przejac drabona i splawic Jaspera, ze jesli mu sie nie podoba to ma isc na komende, zlozyc skarge i wyjasnienia, to moze mu udziela dostepu do swiadka. Jasper chce drabona tylko dla siebie. Rezultatem jest, że Jasper moze zlozyc skarge i wyjaznienia tutaj, na miejscu zgodnie z prawem i nie musi isc do siedziby strazy. Oraz Jasper moze najpierw pogadac z drabonem. 

Jasper wyjawia o co chodzi z cala sytuacja Dankanowi. Ustalaja chwilowy rozejm i beda przesluchiwac drabona, zeby wyciagnac kto ich naslal i co sie dzieje. Dankan boi sie, ze chaos i bezprawie rozpleni sie po calym miescie. Dlatego godzi sie na chwilowy rozejm. Zaczynaja przesluchanie, Jasper chce uzyc magii mentalnej i przeczytac mu umysl. Skonfliktowany sukces - Dostal informacje, ze Lech - bandzior z Drewanineego Jastrzbeia pracuje z komendantem strazy. I to komendant naslal ekipe drabonow. 

Nagle wpadla ekipa Strazy Miejskiej - Kolon (pijany, bo pil przez cala akcje) od razu wstal i krzyknal gdzie znajduje sie drabon i ze trzeba go pojmac. Rozkazy od komendanta. Karotson zatrzymuje ich powolujac sie na swoj pozostajacy autorytet i range (kapitan strazy). Udaje mu sie calkowicie opoznic straz, dzieki czemu Jasper ucieka przez okno z wiezniem. Harry ostrzega Dankana zeby uwazal, bo komendant pojdzie na wojne, juz nie kryje sie z kim wspolpracuje. Dankan ma to w dupie i poszedl do pracy.

Jasper uciekl do innej kryjowki Srebrnych Lisow i ukryl sie. Dal znac harremu gdzie sie znajduje. Harry umowil Jaspera na spotkanie z burmistrzem - ktorzy siedzi w kieszeni Lisow, zeby zalatwic pomoc i srodki na walke z Drewnianym Jastrzebiem. Jasper decyduje najpierw isc obadac teren w okolicy siedziby Drewnianego Jastrzebia, a nastepnie udac sie do burmistrza po pomoc.

Poszedl do Lecha Slawa, przeszukac okolice i zobaczyc jak mozna by sie do niego wkrasc. Zobaczyl w okolicy 13 patrolujacych ludzi Lecha. Uznal, ze uzyje iluzji, aby stac sie mniej widocznym i sprobuje wejsc do srodka budynku. Udalo mu sie przekrasc, bo sprawny z niego akrobata i zlodziejaszek. Wykradl wszystkie dokumenty, terminarze, przeplywy pieniedzy itp. Wie juz o wielu ludziach polaczonych z Drewnianym Jastrzebiem. Te dane bedzie chcial wykorzystac w rozmowie z burmistrzem.

Dankan poszedl na sluzbe nocna za Kolona (znowu). Spotkal komendanta, ktory zaczal go wypytywac o to co sie dzalo. Jest wrogo nastawiony. Stara sie wymusic obciazajace zeznanie z Dankana. Komendant Icek uparl sie ze chce Karotsona dzisiaj wrzucic do aresztu i pozbyc sie problemu praworzadnego straznika. Zaczeli sie klocic i przerzucac argumentami. Karotsonowi ze zlosci przypadkiem eksplodowala energia magiczna. Poczul to i instyntktownie zaczal wykorzystywac. Magia zmusil komendanta do wejscia celi samemu i wyrzucenie klucza. Komendant grzecznie to uczynil, po czym gdy wrocil mu rozum zaczal wyzywac i przeklinac Dankana.

Jasper z burmistrzem obgadali problem konkurencyjnej organizacji. Burmistrz jest pozytywnie nastawiony do Srebrnych Lisów, ale stara się pilnować dyscypliny budzetowej, nie chce wywalac kasy na byle walki gangow. Jasper probuje namowic burmistrza na utworzenie nowego programu miejskiego: dotacje na miejsca pracy dla orkow i pol orkow z okolicy. Oczywiscie firma Lisia Kuznia (przykrywka dla srebrnych lisow), natychmiast zlozy wnioski o dotacje i zatrudni mnostwo biednych pol orkow i da im prace - jako mieso armatnie. Burmistrz po krotkich namowach zgadza sie i powoluje program atywizacji dla pol orkow i orkow. Srebrne lisy zyskuja pieniadze na stworzenie armii drabonow pol orkow i orkow. Wielki sukces programu aktywiacji zawodowej. Wielu pol orkow natychmiast przyjelo sie na dotowane miejsca pracy w Lisiej Kuzni (czyli u Srebrnych Lisow) jako drabony.

Dankan sprowadzil kaplana do siedziby strazy miejskiej i zmanipulowal go, ze komendant postradal zmysly. Udalo mu sie to doskonale i kaplan jest przekonany ze komendant jest swirem i sam sie zamknal w klatce - wyrzucajac klucz. Kaplan na bazie wyznan naocznych swiadkow, Dankana Karotsona i szalonego gniewu/zalosnych krzykow komendanta podejmuje decyzje o unieruchomieniu go natychmiast i wzieciu na leczenie mentalne. W miedyczasie przyszedl Jasper poinformowac co sie wydarzylo w sprawie tworzenia armii przeciw Drewnianemu Jastrzebowi. Zaczeli przygotowywac sie do zlikwidowania sie Drewnianego Jastrzebia. Brudny Harry w obliczu zaisntalych wypadkow oddal dowodzenie Jasperowi. Dankan Karotson zdal sobei sprawe, ze jako najwyzszy stopnien obecnie (biorac pod uwage, ze komendant lezy u czubkow) zostal p. o. Komendanndtem. Przejal tymczasowo wladze nad Straza.

Jasper wraz z Dankanem zaplanowal i skoordynowal akcje pojmania Lecha Slawa. Opracowali plan pojmania, obstawili drogi ucieczki z miasta i zaatakowali jego siedzibe polaczonymi silami strazy miejskiej i swiezo utworzonej armii drabonow pol orkow. Lechoslaw nawial kanalami. Prawie mu sie udalo uciec i poinformowac dowodztwo Drewnianego Jastrzebia o rozpetanej wojnie, ale na wyjsciu z kanalu dzieki straznikom i srebrnym lisom zostal pojmany i zaniesiony do aresztu. Chwilowo zostal zazegnany kryzys Drewnianego Jastrzebia.

# Wpływ:

* Jasper - claim na burmistrza, srebrne lisy podwajaja siatke informacyjna
* Dankan - drewniany jastrzab uzna ze wina ze utracili wplywy to wina nieudolnego Lecha Slawa, claim na sierzanta Kolona (michal: po chuj ci ten alkus)
* Ja - Drewniany jastrzab dowiedzial sie o agresywnej postawie Srebrnych Lisow i strazy miejkiej. Sa straznicy miejscy ktorzy raczej wierzyli temu co mowil komendant i nie ufaja Dankanowi

# Streszczenie

Na razie brak

# Progresja
## Frakcji

# Zasługi

* czł: Dankan Karotson, najprawszy z prawych i przyszly komendant strazy miejskiej goldenfields - pomogl pojmac Lecha Slawa z Drewnianego Jastrzebia i wyykryl spisek skorumpowanego poprzednika
* czł: Jasper Firetorcher, Przejal wladze w Srebrnych Lisach i uczynil z nich znaczacego gracza Uzyskal dotacje miejskie na zatrudnienie drabow do brudnej roboty i doprowadzil ujecia Lechoslawa.

# Plany
## Frakcji

# Lokalizacje

1. Świat

# Czas

* Opóźnienie: 0
* Dni: 2

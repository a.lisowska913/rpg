---
layout: post
title: "Mage, vicinius, human"
date: 2015-10-13
---

On Magi and humans in the world of Invasion.

Look outside your window. What you probably see is the real world, the one you live in. Well, that is, unless you live in a very strange place.

The world of Invasion is a very strange place. The dominant life form on this planet are humans. And humans think that they are the only dominant life form, even: that they are the only sentient life form. This is not really true. Another life form exists on this planet, one, which looks exactly like humans. Those are Magi.

A mage has the same DNA structure the human does. A mage – in early form – looks exactly the same human does. But what differs a mage from the human is the fact that mage has some kind of parasitic entity inside his body. Well, actually, a mage would say it is a symbiotic entity. This entity is called “magical tissue”.

How it begins: a human is influenced by some kind of magical radiation. This starts a mutation in a part of his body. Some of the human’s body becomes destroyed, and a mutated form of human body replaces it. This mutated tissue is receptive to magical power and it also is able to influence that magical power. The magical tissue is what differs a mage from the human. The more magical tissue is inside a mage’s body, the more powerful the mage is.

This means that, in effect, a mage is not biologically human.

In this reality, a mage is able to command the magical energy using his willpower and the energy. The energy is being catalyzed by the magical tissue, while the spell comes into effect from the willpower. If the mage has too much magical energy around, too much for his base magical tissue density, the mage will also feel sick and his body may get the deteriorated.
Not every human can become a mage. Only some are receptive to magical power, only in some magical power will actually cause the mutation of tissue into magical tissue. This is not a very common mutation. The tragedy of family of Magi is the fact, that two normal people are able to have a mage child, but also the fact, that two Magi may have a human child.

Magical power works a bit like water with gravity. So magical power flows with currents. When the magical power concentrates something we call “node” appears. Now what is the gravity – human emotions, concentration of willpower, and concentration of magical energy. Mind you, when I say “human”, I really mean “sentient”, with the dominant paradigm of the dominant sentient species. So for example, if we happened to live on the world, were the dominant life form would be squirrels, and they would be sentient, and their mind would differ from human mind, then the magic would gravitate towards this squirrel emotions and squirrel ideas. This means, that human Magi – Magi derived from humans – would be very weak in this world. As human Magi do not think like squirrels, they are not able to operate the reality, to program the reality as they wish, because the reality does not correspond to the way human Magi think; the dominant way of thinking in this world would be squirrels.

The more powerful the mage is, the less human he becomes. The more a mage moves away from human biology, the less he is able to use magical power simply because his way of thinking starts to differ from the general paradigm of the sentient dominant life forms on this planet – mainly, humans. This is why – in ancient myths – all those nymphs, dragons, trolls… All those fantastical creatures usually had low magical power. They used to be human – derived magi, but they strayed away from their humanity and they have lost the ability to directly influence the magical power using the willpower.

So we have two main life forms on this planet – humans and Magi. Yet in the same time we were talking about fantastical creatures. They are neither humans, nor Magi; at least – not at this point. Some of them were even never Magi; some creatures were created in the currents of magic, some were created by the legends and dreams which magic brought to life and some came from different worlds.

We call them vicini - singular form is vicinius. From Latin this means “a neighbor”.

A vicinius is a magical creature which is sentient and which is neither human nor a mage. A vicinius may have been a mage in the past but has ceased to be one because the way he thinks is simply far away from the human.

A special, very dangerous, breed of a vicinius is illuminati. Illuminati used to be Magi. There is an old legend from ancient days with Merlin and Morgana Le Fay who used to quarrel about the future of the world and who lived in the world where magic was far more powerful than today. They were the ones who has managed to destroy the magic; or rather, to phase shift it. That day illuminati were born; an extremely powerful creature who tries to devour every mage which exists and who tries to destroy all forms of magic.
Fortunately, there are about 10 illuminati in Europe. Their location is well known and they are being tracked. Destroying illuminati’s mortal form doesn’t help; from our point of view – they are completely invincible.

So, if magi are so very powerful, why don’t they rule the world? It’s kind of rock paper scissors. If a mage manages to acquire too much power he might get scented by illuminati. And illuminati will always win against a mage, no matter how powerful. So the only way not to get destroyed by illuminati is to never be spotted by one. Fortunately, humans have an influence on illuminati; illuminati does not feel good in the presence of humans and illuminati cannot live in the city. Yet this only works if the human is untainted by magic, because if he is eradicated by magic power he will become an illuminati magnet. Of course, it doesn’t mean that every person who has been touched by a spell is a magnet for illuminati. Think of it rather, that if you take over the whole city using magic, you may make an illuminati spot you even, if from thousands of kilometers afar. And humans, no matter how skilled, may have problems with a powerful mage.

In the world we live in, humans do not know of magic existence. They do not even know of vicinius existence. They cannot know – because illuminati might have an agent. So Magi stay undetected and they try to rule the world using money and political power, like normal humans.

About 10 years ago there has been another phase shift. It was called “an Eclipse”. It has shifted the phase of the magic, which means that a lot of Magi lost magical power and a lot of humans gained magical power becoming Magi, or gaining potential to become Magi. This seriously upsets the balance of power between powerful magical Bloodlines, guilds and politicians.

One last thing about spellcasting – you can cast only what you can realize, what you can understand, what you can define. To cast a spell, even the simplest one, at minimum you need to be able to target a particular target [usually using your eyes] and recite an incantation which is a focus. The incantation does not have to be exactly the same every time; for some people it will be a rhyme, for some it will be something more like programming language, some will chant something strange. The verbal component comes from the fact that in popular culture a mage casts a spell using voice, therefore to make a spell happen under a dominant paradigm of general population a mage needs to conform to it. This incantation – and the process of spellcasting – requires more or less five seconds. Usually more.

To make it impossible for the mage to cast a spell it is enough to: blind him, tie his hands or make it impossible for him to speak.

And that’s in general it.

If you are a mage, avoid silver. No, really – avoid silver. Do not cast anything on the person who has something silver. And – for the love of everything unholy – never cast a spell on a mirror.

And if you ever hear the phrase “black mithril” or “arildis”, run. Run as fast as you can. If your magical aura touches arildis, you are dead… Or you wish you were.

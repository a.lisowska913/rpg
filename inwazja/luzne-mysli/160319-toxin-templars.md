---
layout: post
title: "The Toxin Templars"
date: 2016-03-19
---

The Toxin Templars are a subset, a subfaction in the Millennium guild. They are led by Amanda Diakon, one could say they are her own private subfaction.

Generally, Millennium does not have as many magi as other guilds. This leads them to employ some interesting solutions to the problems, unusual allies and very pragmatic approach to everything, `. Toxin Templars are Millennium’s response to the powerful guilds out there.

Toxin Templars are composed of the following subsets of forces:
• humans who do not know about magic existence
• humans who know about magic existence
• vicinius who are mindless beasts
• intelligent and powerful vicinius who often commands the forces
• Magi who are on the back line, preparing stuff for the others to use
• magi who are on the front line
• chemicals, toxins, poisons, afflictions
• media coverage and properly bribed officials

All those are controlled by the single mind – Amanda. One of the Triumvirate, the Spider Queen, she is a very, very augmented mage. After the Eclipse her power has grown considerably and as she is a kralothborn, her body is her perfect tool. Things which normal humans would not be able to actually do, both in physical and mental levels, are completely at her disposal.

Her right hand is a vicinius called Parasite Diakon. He used to be a mage before the Eclipse, but as the Eclipse hit, his magical power started going haywire. He tried to change his Pattern and actually he has succeeded. At the cost of all his magical powers he has gained an ability to construct small creatures which infiltrate an organic life form’s body, meld with its nervous system and take over. That’s when he changed his name to Parasite.

Parasite serves as an organic computer and as mind controlling unit for Amanda. Amanda serves as a tactician and shock trooper for the Toxin Templars.

The natural doctrine is “go in, take a minimum forces required, subvert whatever is available, complete the mission and evacuate your native forces and most precious hostages you have subverted”.

Now, Toxin Templars have several interesting approaches to fighting magi.

First, most obvious way is to make Parasite course some scandals and problems especially with the Magi underlings and then take it to media. Preferably, to as neutral media as possible. Seriously, this has actually happened, and the impact of using neutral media is much higher than using Millennium controlled media. This leads to general unrest and erosion of trust which then disciplined Toxin Templars are able to use.

Second approach is to send people who do not know about magic against magi. That way, Masquerade is being upheld and even if the magi win, they will be prosecuted because they have managed to breach the masquerade. Of course, the plan usually goes in such a way that Templars have perfect plausible deniability. And that magi need to attack people first – that way they can to maximize the efficiency. And of course, media.

Third approach is to send the people who know about magic and vicinius against magi who think they are fighting people who do not know about magic. This gives them the first strike advantage.

Sometimes, though quite rarely, the Templars use specifically crafted poisons, venoms and toxins against the targets. They are able to bio engineer a disease or plague in order to weaken the opponents and build up the resistance in themselves. Amanda takes care that everyone around knows that they are capable of using bio weapons; that way she never has to use them.

One of the many tricks in Amanda’s arsenals is to give people stimulants. That way the humans are able to do feats impossible for normal people and afterwards when the aftereffect of using stimulants and other drugs kick in, a Millennium BioLab will be able to help them.

One of the more interesting tricks which actually happened in the past – Parasite infected a young mage. The mage got afterwards sent on the battlefield against the kraloth. When the kraloth dominated a mage and took over his mind, Parasite override it the mage’s body and killed the kraloth. Using plain, old-fashioned, mechanical chainsaw.

Amanda as a spider Queen is able to split herself into many spiders and reconstruct herself from the amount of those which are present. Her body is actually a set of many spiders held very tightly together so during the fight it is quite possible for a sword blade to pass her body; she simply disassembled herself at that moment in that area. She is one of the most dangerous combatants in Millennium.

Toxin Templars are the special operations unit of Millennium. They are very, very loyal to the cause and if they are on the battlefield it means that the situation is a high-stakes situation from the point of view of the millennium. Amanda does not deploy them unless really necessary. The presence of Templars reminds other guilds that Millennium is much more powerful than expected; this is something millennium does not want and so they avoid doing it. Usually, if Templars are doing something, they are doing it surreptitiously and no one ever knows that they are on the battlefield.

Even if this faction is actually quite powerful in terms of military, they would crumble against the might of every corresponding faction from any larger guild. This is why usually Templars do the intelligence warfare, heavily use the media and sets the conditions of the fight to be favorable to them.

Otherwise not even Parasite or Amanda are able to make them win.

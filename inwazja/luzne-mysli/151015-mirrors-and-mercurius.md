---
layout: post
title: "Mirrors and mercurius"
date: 2015-10-15
---

People used to say „when you look deeply into the mirror, the mirror looks deeply into you.” When you look at magic, it is actually quite true. Mirrors capture visions. They capture the dreams. They capture emotions. Mirrors happen to capture magical energy, which gets twisted and modified inside a mirror.

There exists a special type of a mage – it is called “a mirror mage”. This mage uses special qualities of a mirror, trying to use its properties as a gift. The spells which pass the mirror will be quite chaotic in nature; furthermore they may not have a normal energy source.

Mirror Magi are usually not regarded very highly by their peers. They do not control the magic very well, and they usually do not live very long.

What does not help is the fact, that a mirror is usually made of liquid – or worse, silver. Silver is one of special magical metals; it emits a kind of radiation under being touched by magical energy. This radiation destroys both human tissue and magical tissue, inflicting very heavy pain.

So a typical mirror which usually has something to do with silver and which is a reflective substance, which is able to modify spells, “capture the soul of the one looking inside the mirror”… Not the safest object to cast spells on. Not to mention, if the mirror is infused with some kind of magical energy, making it even more chaotic and volatile.

There exists a special blend of golems/magical creature having to do with the mirror qualities. They are called “mercurius”. Those are creatures created from reflective substance, usually from Mercury; you know, the liquid metal which used to be used in thermometers. In normal situations, Mercurius is a golem like entity, which does not have its own will and only completes the tasks commanded by the master. But because of reflective substance and mirror – like qualities, there exists a risk that a Mercurius will become something more.

Mercurius is usually very nimble and agile, it is kind of a liquid, not really a solid entity. It is assassin like. If you have seen the Terminator movie, you may know what I’m talking about. Yet Mercurius does not have any abilities to look like a person. It can only look like a mirror or a humanoid made of liquid.

You should not cast a spell on a Mercurius because of the same reason why you should not cast a spell on a mirror. This simply too dangerous – a golem like Mercurius is only a golem, but if you give a Mercurius something more using magic, you might create something you would look like to meet.

This is one of the reasons why creation of mercurius is not something that Magi really like and creation of one is usually frowned upon. If a Mercurius that something wrong from the perspective of the Magi, a creator will be put responsible, especially if Mercurius ran out of control.

Because you do not experiment with mirrors and you do not play with them. Just – too dangerous. Especially after the Eclipse.

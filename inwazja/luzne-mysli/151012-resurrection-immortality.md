---
layout: post
title: "Resurrection, immortality, magic as 'reality's programming language'"
date: 2015-10-12
---

On magic omnipotence.

The problem with magic and resurrection long time ago when you have designed the system we have decided that it would not be very good that mage could resurrect each other. It would remove all the danger from the system. Yet in the same case we do not want to delete magi and delete player characters.

If magic can do everything magi should be immortal. This would make the system quite impossible to play for the normal player. If we had immortal magi they would be omnipotent of many powerful and nothing could stand in the way. Even with very low density of magi let’s say one mage is being born for 10,000 people or even for 1 million people, with years since let’s say your 1000 A.D., magi would not die while people would die I’m in normal humans. This means the words would be composed solely of magi. This would not be the world we would like to play in.

In other words there has to be something to stop infinite amounts of magi in the world and the problem of immortality and resurrection.
The only solution was that magic is not omnipotent. So what is magic than magic is a tool I like to describe magic as the perfect computer language of the reality. You can make everything happen absolutely everything as long as you understand both the reality and the domain and of course you have enough time. So even if magic is all-powerful the mage is not.

So let us talk about resurrection and immortality. Resurrection and immortality are two topics which from the very beginning of the history I’ve been the basics of alchemy things which were supposed to be the liberators.

To explain why the resurrection or immortality is impossible in Invasion system I need to explain the process of magical creation of a car. What is a car? If a mage wants to create a call using magical means, he usually wants something which looks like a car similar to other ones on the road and is able to get the mage from point a to point B.
Let’s make an example a wizard wants to create a Porsche or another something which looks like a Porsche and to get him swiftly from point a to point B. The magic can draw information on Porsche car directly from the mind of a mage and his imagination. What is interesting in that is the fact that mage can only create what he can kind of visualize, understand or well grasp in his mind.

The Porsche car created by a student will be different inside from a Porsche car created by someone who works as a car mechanic. If a student understands that the car works because it has some funny wires inside or even a strange looking fish, then the student’s car will have a strange looking fish and wires inside even if they don’t make any sense. You could say that magic is a programming language of reality.

The problem with resurrection is the fact that no one knows about a person. Even the person themselves doesn’t know everything about themselves. As the previous document showed, mage’s emotional state influences the spell. Also the mage cannot really do something she doesn’t understand. If a mage tried to resurrect someone the mage would reconstruct that person but in the image the mage has of that person. The mage is not able to return the person she is only able to create kind of a homunculus which stems from the mage’s vision and conviction about that person.

The problem of immortality has been solved by giving limited time to magical tissue. Although magic is able to influence and to modify human flesh, it cannot keep magical tissue young and powerful forever. Magical tissue will decay with time therefore mage is not able to be forever.

There is one more concept combined with magic and magic omnipotence. Namely the information source. Let us assume that we have a mage who is quite good in the life magic but doesn’t know anything about healing. The friend of this mage has a horrible cut on his hand. Even if the mage cannot really know what should be done that the cut is repaired, he can imagine what should be done as in: how does a healthy hand look like. The mage can simply move the power with his intention of hear him towards the injured friend, and the magic can gather the information from the remainder of the human’s body.

There is one more type of information source; namely the Archer spells. Archer spells are the ones which use as the data source the general human common knowledge. Think of it a bit like database querying. So a mage casting an Archer spell kind of select the population [for example inhabitants of this building] and then tries to cast a spell using general knowledge of the population not his own expectations.

For example let’s assume Jack, young magician, wants to create an illusion of the most beautiful woman. The problem is his expectation of the most beautiful woman would be Kate, his classmate. He really doesn’t want that. So he can use an Archer spell, select the population from the audience and formulate this spell as “create an illusion of the most beautiful woman interpolated from the minds of this audience”. The magic will comply.

All in all, a mage can do a lot if he can properly formulate a spell. Magic is very powerful but if the mage is not able to actually make something happen, imagine something, grasp a concept; all if there exists no real data source, then the mage cannot cast this spell. Magic is a tool a very powerful tool. Magic still cannot do what is impossible, because of no information or not enough energy.

And this is why resurrection and immortality are still impossible. And this is why magi population is lower than human population.

---
layout: post
title: "Painful Wax - first cut"
date: 2015-12-26
---
The Painful Wax.

There are circles within the circles, secrets within the secrets, guilds within the guilds. And Painful Wax is one such secret, one such order within the Silver Candle. A small, secret guild located within more powerful entity.

Painful wax has only one purpose – to protect silver candle. To make sure, that silver candle will always do what it was supposed to do; to protect its members from outside and to make sure that silver candle will never torment the magi within to the point they will want to run away. Painful wax doesn’t care about particular people; they are interested mostly in higher-level maneuvers inside silver candle. They consider themselves the guardians and the sentinels of the guild. They are also usually fanatics of the Guild, of the very idea what the silver candle is supposed to be.

Magi of Painful Wax consider themselves above morality, acceptable armaments and rules. At any point of Painful Wax existence any moment 10 of their members decides any other of them should die, the blood magic kills that mage. This internal system makes sure that it is impossible to actually expand painful wax; the group never had more than 25 members. In the same time, it is completely against the rules for any mage of Painful Wax to ever do anything for themselves or for any other mage of the candle without a single purpose aligned to the wax ideas. It’s best to think of them as the monks; those who have to throw away everything otherwise 10 of their peers will decide you are dead.

The painful wax was supposed to be eradicated long time ago in the candle, but it seems the candle did not manage to eradicate them. The wax simply went into hiding.

---
layout: post
title: "Silver Candle - origin and purpose"
date: 2015-12-26
---
The Silver Candle is a mighty glacier, as powerful as one and as fast as one. The greatest strength of Silver Candle Guild is its age, its sheer size and the fact that this guild has people absolutely everywhere. When it lacks operating Magi it has other operatives or simply agents. Yet, as with most large and powerful organizations – especially old ones with multiple interests – Silver Candle has a lot of problems with bureaucracy.

Nowadays many magi believe that it would be far better if silver candle would simply cease to exist. They do not know why was it created and why is it so powerful. Silver candle will never cease to exist. A lot of powers are really interested in silver candle existing, and being restrictive – but not overly restrictive – while constraining its members.

In the very beginning this is why silver candle was actually created. Long time ago it used to be different. The world used to be a very “dog eats dog” scenario. Powerful barons use to hold their domains and they had absolute power within them. The wars between Magi used to be devastating for the environment, commoners and the magi themselves. Because of how blood magic and sympathy work, a mage couldn’t be sure that their beloved are safe – everyone could have been turned into a weapon, going into blood magic was the most sensible choice because otherwise you would be deprived of a weapon which others would be able to use against you.

Silver candle started as a defense organization. Organization which answered only to its members, banned several different weapons and the lead to ethical treating of other magi and people. They banned the blood magic, using mothers and sons as weapons, scorched-earth tactics. What they gave is “attack one of us, attack all of us”. With time they became more and more powerful up to the point they were able to expand upon other territories and other people – what helped was the fact that many magi so that this is the only logical solution; otherwise everyone would simply be ruined and destroyed with time.

Other forces started to cooperate with silver candle and they all decided to reduce the level of armaments. Seeing how powerful is a particular mage and how much can a mage do, there has to be some constraints on Magi, especially young ones – those who have never seen the devastation caused and those who are too young and too emotional to truly understands the consequences of some of the actions.

Silver candle has safeguarded Poland for a long time. The guild has morphed with time – more bureaucracy, loss of purpose, too much politics and not enough defense of its own members – but the very core of Silver Candle still made sure that the defense purpose is fulfilled and that nothing really serious happened. And it has succeeded – with time, Poland and Polish magi have prospered.

Then the Eclipse came.

Right now silver candle is in much worse shape. The “Silver candle is everywhere” ceased to be true. The fact that silver candle initiated wrong policies during the Eclipse only led to deterioration of trust between the guild members – the trust, which is never really existed there in the first place. A lot of new magi were added to the guild which never knew the purpose of the Candle and who did not really care. Magi started to expound the arsenal of weapons again and there are areas which are completely outside the Silver Candle’s control.

Still, the Candle still has magi willing to fulfill its original purpose and its power is not completely broken.

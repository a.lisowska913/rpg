---
layout: post
title: "About their art: Zajcew, Weiner, Maus, Diakon"
date: 2015-12-27
---
As Grand Admiral Thrawn would say, “if you understood your opponents’ art, you would understand them and the way they fight”. Let us look at particular forms of art which come from three bloodlines we were talking about before.

# Weiner. Keepers of secrets and demon hunters.

Their art form has something to do with different kinds of puzzles. They tend to be very intricate, very sophisticated, very often you do not even see that something was supposed to be a work of art, but after analyzing the object it has hidden meanings and the more you analyze it, the more facets you can actually see.

In terms of sculptures those will usually be mechanisms and interesting contraptions and devices. Very often Weiner’s sculptures would actually do something, not only be visible.

In terms of poetry, they are poetry has double or triple meanings. If you read a poem without knowing context of it, you will probably read a poem. If you know it’s context, you will be able to uncover a hidden meaning which will then lead you to further exploration and research. Then you will be able to understand what the point was really about.

What they love the most is mechanisms, puzzles and things which are small but very meaningful.

So one of the more interesting ways to entrap a Weiner would be to actually give him an empty box, one which is very well closed and with some defenses. Then tell him that there is a secret in the box. Then leave him for a week – probably he will stay there and fail to find something which was never there. To seduce one, this box would have a riddle where to find you.

# Zajcew, Masters of stories and the passion of an element.

Zajcew is a lot about boasting and doing things of grandeur’s caliber. A special type of art form which mostly Zajcew magi focus on is playing with fire. Now, when I say that it sounds very easy. It isn’t. A mage needs to unleash as much energy as he is able to, make complex shapes and patterns out of it, unleash those energies to create beautiful sight, in the same time do not die and do not get burned by the energy too much, and this cannot be an illusion. This has to be a row, dangerous energy which will manifest in the form of fire. A Zajcew mage needs to struggle against the fire and he needs to conquer it. Hard mode: during this time a Zajcew mage needs to weave a story and this story needs to correspond to the struggle between the mage and the energies. This type of art is absolutely unique; It Is Not Really Possible To actually capture it on video; this has to be simply experienced on live performances.

In terms of poems, sculptures, paintings – everything a Zajcew does needs to be grandiose. If it is culture, it will have to be a large one. Or it will have to explode; some Zajcew consider blowing things up a form of art. However stupid it sounds, the way an object is destroyed can be more or less beautiful. And this also falls under the jurisdiction of Zajcew. Fireworks?

# Diakon, sensual Magi of life shaping.

Now, this might sound unusual, but they specialize in normal actual performances. Theater, light shows, music – all those artistic expressions are something Diakon are really good at. The main motive of theme in their work would be change and evolution. Adaptation, change, evolution. They do not shy away from eroticism either; it is very difficult to say what type of art is especially close to Diakon, because they are so very different from each other. There is something specific to them though: patience. Everything they do usually requires extreme coordination and extreme patience and understanding of the nature of what they are working on. Usually, they are will somehow take inspiration from nature and sensuality.

So, plant gardens. Or a very complex multi-people dance which requires very good coordination and looks like leaves dancing in the wind. Rarely, their art is made singularly. Usually when they try to do something, they do it in groups. The botanical garden, to which every diakon can put something from themselves, which will create an entity much larger than any of them. Add to this a lot of artistic freedom and personal expression and the fact that a diakon often changes the name and the looks of themselves and you get something which apparently changes which is never the same which was different yesterday from today – and this perpetual change, focus on collectivity and flamboyant individualistic approach were the collective harmoniously comes from those parts of individualism – and this is diakon art.

Well, that, and plant shaping. And acts. Oh God those are beautiful.

# Maus, demonic unity in the magic.

Maus are really about the control. It is the control of magic, control of demons but also self-control. One of the forms of art which is unique to this bloodline and not pretty well understood for other magi is very closely aligned with quark crystals. A Maus artist can create a special quark crystal; sometimes several. Those crystals send impulses to mage who touches them. By combining several of those crystals and placing them on the body of a mage, that mage - victim (that is, art critic) will experience several different stimulating impulses. Very often this would be combined with some kind of mental magic from one of those crystals, taking the art critic for a very complex dreamscape. One, where for example a mage can feel pleasure smelling red flowers and pain smelling blue flowers, in the same time feeling overwhelming anger. In short, this allows an art critic to experience themselves and their own limits. Some specially designed dreamscapes can be used as torture chambers and there are contests who will remain in a chamber the longest. And contests who will manage to make others stop the fastest.

Maus art is a lot about introspection and control. No wonder one of their favorite art forms is a ballet.

---
layout: default
title: Magitrownia Histogram
---

# {{ page.title }}

## Lokalizacja 

1. Świat
    1. Primus
        1. Mazowsze
            1. Powiat Pustulski
                1. Męczymordy
                    1. Magitrownia Histogram

## Opis

Magitrownia Histogram jest złożoną magitrownią artezyjsko-geotermalną nowego typu. Złożona przez Bractwo Pary, stanowi nieco rozpadający się eksperymentalny system 'state-of-the-art'.

Składa się z grupy podstawowych komponentów:

* Ogrodzona płotem i bramą wejściową
* Cykl podstawowy magitrowni
    * Pompa wtłaczająca
    * Pompa ssąca
    * Kolektor Quark
    * Dekontaminator
* Cykl kontroli wody
    * Awaryjny basen
    * Jezioro Mordowe
    * Pompownia zewnętrzna
    * Monitory Skażenia Zewnętrznego
* Sterownia centralna
    * Czujnikownia
    * Monitory magitrowni
    * Dziwne czujniki Weinerów
* Ekrany ochronne
    * Pancerz magitrowni
    * Odpromiennik dachowy, często powoduje dziwne efekty
    * Radiostacja eteru
    * Awaryjny deaktywator
* Emitery magitrowni
    * Przepompownia dalekosiężna
    * Kompresor magii w wodę
* Szyby magitrowni
    * Szyby podstawowe
    * Szyby awaryjne
    * Krystalitowe Wiertło
* Obszar mieszkalny
    * Cztery pomieszczenia dwuosobowe
* Obszar użytkowy
    * Dwa spore pomieszczenia magazynowe, w czym sprzęt ciężki i paliwo
    * Generator prądu geotermalny (wspomagany elementalnie)
    * Awaryjny generator spalinowy
* Obszar zewnętrzny
    * Garaż samochodowy
    * Lasek wchłaniający
    * Promiennik aury (wszystko OK) i iluzji (wszystko normalne)

Magitrownia nie jest bardzo rozległa; jest stosunkowo wysokim i blokokształtnym budynkiem, z którego ciągle unosi się para.

## Fizyka

* http://swiatoze.pl/jak-dziala-elektrownia-geotermalna/
* http://www.plan-rozwoju.pcz.pl/dokumenty/konferencja/artykuly/35.pdf


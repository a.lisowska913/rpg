---
layout: inwazja-konspekt
title:  "Niespodziewane wsparcie"
campaign: druga-inwazja
players: kić, dzióbek
gm: żółw
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [140213 - Pułapka na Edwina(AW, WZ, HB)](140213-pulapka-na-edwina.html)

### Chronologiczna

* [140213 - Pułapka na Edwina(AW, WZ, HB)](140213-pulapka-na-edwina.html)

## Misja właściwa:

Andrea i Wacław relaksowali się w "Rzecznej Chacie" pijąc jakieś środki (najpewniej alkoholowe). Tam było pewne poruszenie. "Moriath powrócił", "porwał kogoś znowu" - tien Alfred Kukułka, alchemik którego próbował kiedyś porwać Moriath (i podczas porywania którego zginął) został porwany niedawno. Dostrzegli też, że ich znajomy "elf", czyli tien Krówka jest taki osowiały i nieszczęśliwy, ale nie mieli na to głowy.
Zaczepił ich tajemniczy osobnik. Przedstawił się jako tien Grzegorz Czerwiec, terminus SŚ, polujący na Moriatha od bardzo dawna. Umówili się na spotkanie gdzieś indziej, w neutralnym miejscu.  Co ciekawe, Andrea kojarzy (ze słyszenia) tien Czerwca - ten terminus jest uznawanym za kompetentnego, niebezpiecznego (dla wrogów i sojuszników) magiem NIE działającym w okolicy.

Następnie... cóż, Wacławowi zależało by dowiedzieć się, czemu "jego" Tatiana miała takie kłopoty. Kto ją wrabiał i dlaczego. Andrea udała się an wizytę do tien Szamana, który na wejściu ją opieprzył. Z jego punktu widzenia, Andrea pomogła Tatianie, Bankierzowi i Kozak zdać ten egzamin... a przynajmniej do tego stopnia, by Szaman był w stanie ich uwalić sensownie. Więc, mogą uczyć się dalej...
Na nieśmiałe próby wyjaśnienia się Andrei westchnął. Najpewniej też miała jakieś niesamowite naciski polityczne. On to rozumie. Jedynym magiem, który tak naprawdę się takim naciskom nie poddawał był ich poprzedni szkoleniowiec (tien Wojciech Tecznia), który przez to odszedł...
Zdecydowanie się jednak rozchmurzył, gdy Andrea zasugerowała mu, że pomoże mu uciemiężyć ekipę. Utrudni szczeniakom maksymalnie życie - albo się nauczą, albo padną jak psy. Andrea zapytała Szamana jeszcze o tien Czerwca, na co odpowiedział, żeby trzymała się daleko od Czerwca, bo to straszne kłopoty. Tien Czerwiec stracił dwa skrzydła terminusów w swoim obsesyjnym polowaniu na Moriatha...

W tym czasie Wacław udał się do Iriny Zajcew, porozmawiać o jej córce (Tatianie) i jak może pomóc, by nikt jej nie skrzywdził (i znaleźć ślad tajemniczego maga), by nie było jakichś konkretnych, szczególnych problemów. To sabotowanie było dla Wacława tematem dość kłopotliwym. Irina go przyjęła i powiedziała, że mało prawdopodobne by to były walki wewnątrz rodziny. Bardziej prawdopodobne jest to, że nie Tatiana była celem. Poradziła Wacławowi, by ten zapoznał się ze statystykami zdawalności na terminusów na Śląsku - i Wacław zdębiał, gdyż okazało się, że trzykrotnie wyższa jest oblewalność w tym obszarze. Za każdym razem z powodów błędów kandydatów na terminusa. Ale, może, tak jak Tatianie, ktoś pomagał...
Czasami te zdawalności, te egzaminy na terminusa kończyły się wydarzeniami dość drastycznymi. 
Wacław obiecał, że to sprawdzi. Irina zapytała jeszcze o Moriatha i szczególnie skupiła się na tym, czy może zostać przesłuchany. Odpowiedź, że jednak nie, zmartwiła Irinę. Następnie zapytała Wacława o Inwazję - Wacław odpowiedział, że nie ma żadnej aktywności Inwazji. Ok. Irina chce być informowana na bieżąco w sprawie tych tematów.

Nadszedł czas na spotkanie z Grzegorzem Czerwcem. Spotkanie odbywało się na neutralnym gruncie, Już na samym początku Czerwiec porozstawiał systemy antypodsłuchowe i inne bariery tego typu. Super. Idea spotkania (agenda spotkania) miała polegać na wymianie informacji o Moriathu i działaniach Moriatha (gdzie Andrea i Wacław postanowili zachować większość ważnych sekretów dla siebie). Okazało się, że Grzegorz Czerwiec ma bardzo dużo interesujących informacji odnośnie Moriatha i działań Moriatha. Zaczął z grubej rury: "Moriath. Bardzo potężny, wpływowy mag. Może mieć wszystko. I nagle - osobiście - wybiera się by porwać alchemika. Nie udaje mu się to z uwagi na obecność Zajcewa i Moriath ginie przypadkiem w głupi sposób." Wskazał, że coś w tej historii nie pasuje, coś jest zdecydowanie nie tak. Ale prawdziwą bombą było, gdy powiedział, że alchemik (tien Kukułka) okazał się być jednym ze sług Moriatha. Osobą kontrolowaną przez Moriatha (mindwormem, ale o tym JAK Czerwiec nie wiedział). Skąd Czerwiec wiedział? Torturował skutecznie i w pewnym momencie każdy do jakiegoś stopnia pęka, a mindwormowane osoby nie. Zawsze są lojalne wobec Moriatha.
No dobra, ale to w ogóle nie trzyma się całości w historii śmierci Moriatha. Zwłaszcza, że Wacław pamięta, że alchemik NIE chciał dać się porwać. Może mindworm sam padł (w końcu Krystalia zabija mindwormy używając chemii).
Dodatkowo Czerwiec wyznał, że jego ukochana, Karolina Maus, także jest mindwormowana. Grzegorz wykrył działania Moriatha, ujawnił je (tracąc skrzydło terminusów i ledwo z tego wychodząc z życiem), w zamian za co Moriath uderzył w jego ukochaną i ją mindwormował. Ona wprowadziła go w zasadzkę i tak Grzegorz Czerwiec stracił drugie skrzydło i reputację. Został uznany "niebezpiecznym". Ale to co się mu udało to pokazanie światu, że Moriath jest zagrożeniem. Oraz uwięzienie Karoliny, zamknięcie jej w stazie...
W zamian za to Andrea i Wacław zdecydowali się mu zaufać. Opowiedzieli mu o "uczniach Moriatha", zaznaczając ich niedouczenie i możliwość mindwormów. Więcej, wyjaśnili że Moriath kontroluje swoich niewolników używając mindwormów - niewykrywalnych pasożytów będących nakładkami na osobowość, zastępujących osobowości i czerpiących swobodnie z pamięci ofiary. Powiedzieli też, że istnieje możliwość odwrócenia działań mindworma, ale nie mogą powiedzieć jak; są to winni osobie której udało się to przypadkowo zrobić (znaczy, Krystalii). 
Nagle Czerwiec ma nadzieję na odzyskanie ukochanej. I jej wiedzy.
Czerwiec dodał jeszcze, że Moriath faktycznie zginął, ale zostawił za sobą zaplecze. Udało mu się (tracąc skrzydło) dowiedzieć też, że Moriath nie działał sam - była co najmniej jeszcze jedna osoba mogąca pełnić "rolę" Moriatha, choć nie jako prawdziwy Moriath (raczej fallback). Ale na pewno może kontrolować mindwormy. Więc zagrożenie nie zniknęło...
To co ważne to to, że Moriath nie był "głową" przedsięwzięcia. Miał mocodawców. Ale Czerwiec nie wie kim są i jaki jest ich cel.

Mając te informacje Andrea zdecydowała, że koniecznie musi porozmawiać z Krystalią. Ale zanim zdążyła się do niej wybrać, do agencji detektywistycznej przyszła zapłakana Nela Welon. Okazało się, że tien Malwina Krówka zdecydowała się wyrzucić Nelę ze stanowiska asystentki po tym, jak dowiedziała się, że ta znowu była u Krystalii Diakon i że Krystalia coś jej znowu robiła. A skąd wiedziała? Radosław jej powiedział. Wszystko mamusi wygadał, więc ta zabroniła się mu spotykać z Nelą i dodatkowo się jej pozbyła. Nic dziwnego; Malwina Krówka ma bardzo dobrą reputację, jej systemy zabezpieczeń są powszechnie rozpoznawalne i widoczne w Srebrnej Świecy; można powiedzieć, numer jeden. A obecność Neli daje podejrzenie backdoorów dla Milennium. Gorzej z zakazem dla Radosława, ale ten jest noga, skoro posłuchał.
Cóż, co z nią zrobić? Nela nie ma gdzie się podziać. Dodatkowo, trudno jej zaufać - owszem, Krystalia wsadziła "zaślepkę" zamiast mindworma, ale Krystalia nie jest ekspertem od hodowania zaślepek (a Teresa od ich wprowadzania). Więc mimo, że ma zaślepkę może mieć dalej jakieś ukryte rozkazy...
Znaleźli jednak dla niej zastosowanie. Niech robi coś pożytecznego - niech wykorzysta swoje umiejętności asystentki tien Krówki do stworzenia torów przeszkód, takich ćwiczeń dla tien Szamana, by uczyć kandydatów na terminusów współpracy. To daje jej fuchę, stawia ją w miejscu gdzie niespecjalnie może zaszkodzić, jej kreacje będą pod kontrolą i wszyscy zyskują. Wszyscy.
Dobry plan.

Więc, Krystalia. By nie stresować jej za bardzo, Andrea postanowiła spotkać się z nią sam na sam. I dobrze zrobiła; Krystalia była w swoim laboratorium, nago. Była tam też Teresa, też nie ubrana. Oprócz nich... kraloth. Krystalia radośnie poinformowała Andreę, że jeśli ta się nie rozbierze z nimi, to niedługo na jej ubraniu coś się zacznie krystalizować, więc ona radziłaby się Andrei rozebrać.
Terminuska. Nieubrana. W obecności kralotha. Dobrze się zaczęło...

Laboratorium Krystalii wyglądało nieco inaczej niż zwykle - w środku znajdował się (nieszczelny) biovat, w biovacie pływała uprząż (taka na kobietę; myślcie strój Leeloo z "5 Elementu", ale bardziej pajęczynowy, więcej połączeń...) na której coś się krystalizuje. Aha, uprząż była możliwa do przygotowania tylko dlatego, że kiedyś Andrea dała coś Krystalii (jakaś przysługa; chyba za poprzednie odmindwormowanie) Za biovatem - obuta stopa, pokryta kryształkami.

Kraloth, co dziwne, wyraźnie nie czuł się swobodnie w towarzystwie nagich kobiet. Jak to radośnie wyjaśniła Krystalia, kraloth nie jest zbyt chętny na igraszki jeśli w organizmie kobiety jest zbyt dużo chemikaliów; kralothy tak samo przez skórę jak emitują też pobierają. Oczywiście, jak zaraz Krystalia doprecyzowała, przeciętna dawka jaką ma jej organizm zabiłaby przeciętnego maga. A skąd kraloth? Mateusz Nieborak go załatwił, by pilnował Krystalię. By zarówno nic jej się nie stało i by nic nie była w stanie zrobić.

W Andrei obudził się detektyw - skąd ta obuta noga. Kto to? Krystalia nie wie... jeszcze. Ale ta osoba próbowała porwać Teresę, a ona, Krystalia się na to nie zgadza. Najpewniej nieszczęsny porywacz pracował dla Artura Żupana. Podobno był jeszcze jeden, ale ten został rozpuszczony w biovacie i jego soki zasilają uprząż (Krystalia dużo mówi, ale ile z tego jest prawdą a ile złudzeniem?). Aha, Teresa ma na sobie kolię, Krystalia nie. Poziom skażenia w tym pomieszczeniu jest tak wysoki że Teresa potrzebuje kolii. Jednak na Andreę to zaczyna działać... zaczęła współczuć kralothowi (XD).

Cóż, by nie skażać niepotrzebnie Andrei oraz by przedyskutować temat, Krystalia i Andrea musiały wyjść z laboratorium. Nago (Krystalia nie pozwoliła się Andrei ubrać, by nie doszło do katalizy przez ubranie). Super. Przy okazji, Krystalia dodała że nieszczelność jest celowa; biovat nie wytrzyma tego, co dzieje się w środku. Za duże ciśnienie. No i porozmawiały. Andrea powiedziała, że ma kolejną potencjałną osobę z mindwormem i chce ją wyleczyć. Krystalia się wściekła. Andrea "kradnie jej życie". Jednak w wyniku bardzo trudnych negocjacji (czyt. 5 minut), doszły do pewnego porozumienia. Chodzi oczywiście o Teresę. Okazuje się, że Krystalia przygarnęła swego czasu Teresę, ale NIE MA POJĘCIA co z nią robić. Teresa jest lekarzem magicznym i Krystalia ją uczy i daje jej wolność, ale Krystalia jest niestabilną ćpunką. Ona nie jest w stanie na dłuższą metę zająć się Teresą czy ją ochronić. Tak więc, jakby coś złego stało się z Krystalią, adopcja spadnie na Andreę.

Wacław zdecydował się, w świetle rewelacji że Krystalia zdejmuje mindwormy dawką kolii na danie zarówno Teresie jak i Krystalii po artefakcie pod skórą. Ratunkowy beacon teleportacyjny; dziewczyny są w stanie wezwać Wacława do siebie w każdej chwili, dnia i nocy. A priorytetowy jest artefakt przyzywający Teresy (tak chciała Krystalia). 

Dobrze. Już ostatnia sprawa - Krystalia zgodziła się pomóc w odrobaczaniu Karoliny Maus z mindwormów jeśli przedtem Wacław i Andrea poddadzą się operacji odrobaczania. W końcu - nie wiadomo. Może mają mindwormy. Zgodzili się. Więcej, dali to jako warunek Grzegorzowi Czerwcowi do współpracy. I nie, Czerwiec nie dowie się kto ma narzędzia przeciw mindwormom. Podczas całej operacji będzie miał zawiązane oczy i w ogóle :P.

Czas pokaże, co dalej...

Cytat z Krystalii do Andrei: "Mówiłam ci, kochanie, że spotkamy się nago i będziemy w końcu razem ze sobą".

![](140219_NiespodziewaneWsparcie.png)

# Zasługi

* mag: Andrea Wilgacz jako detektyw i osoba umiejąca rozmawiać z nagą Krystalią w dziwnych okolicznościach.
* mag: Wacław Zajcew jako mag który w dawnej przeszłości przypadkowo unicestwił Moriatha; teraz współpracuje z Iriną Zajcew.
* mag: Grzegorz Czerwiec jako radykalny i charyzmatyczny terminus który stracił dwa skrzydła próbujący dorwać Moriatha i uratować ukochaną Karolinę. Chce współpracować z Zespołem.
* mag: Karolina Maus jako milość Grzegorza Czerwca która została pokonana i zmindwormowana przez Moriatha.
* mag: Alfred Kukułka jako porwany alchemik którego kiedyś już próbował porwać Moriath. Zdaniem Czerwca, mindwormowany.
* mag: Juliusz Szaman jako nieszczęśliwy szkoleniowiec młodych terminusów opieprzający Andreę za pomoc młodym w zdaniu testu. Dostał do pomocy Nelę Welon.
* mag: Irina Zajcew jako emerytowana terminuska interesująca się poziomem zdawalności terminusów w Kopalinie. Bardzo zainteresowana Moriathem i... Inwazją.
* mag: Teresa Żyraf jako niezbyt kompetentna implantatorka mindwormów. Robi jakieś dziwne eksperymenty nago w kolii Krystalii, przy kralocie.
* mag: Artur Żupan jako osoba która podobno nasłała kogoś by porwał Teresę, czego Krystalia nie chce darować.
* mag: Krystalia Diakon jako potencjalnie jedyna znana osoba zdolna do przypadkowego usuwania mindwormów używając kolii. Przygotowuje jakąś uprząż. Kraloth się jej brzydzi.
* mag: Mojra jako wspomnienie o Moriath; Grzegorz Czerwiec zauważył że historyjka Moriatha nie ma żadnego sensu MIMO że był tam na miejscu Wacław. 
* mag: Nela Welon jako wywalona przez Malwinę Krówkę ekspert od zabezpieczeń. Wykorzystana przez Andreę do pomocy Szamanowi.
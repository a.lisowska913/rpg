---
layout: inwazja-konspekt
title:  "Kto wpisał Błażeja do konkursu"
campaign: powrot-karradraela
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [161218 - Zazdrość Warmastera (SD)](161218-zazdrosc-warmastera.html)

### Chronologiczna

* [161218 - Zazdrość Warmastera (SD)](161218-zazdrosc-warmastera.html)

## Kontekst ogólny sytuacji
## Punkt zerowy:
## Misja właściwa:

Dzień 1:

Siluria budzi się z... Infernią. Obudziła się przed nią i... poszła na lekki _bondage_. Infernia przyjęła to z radością... ale Siluria zdecydowała się zwiać. Infernia uruchomiła pnączopętlę; jednak Siluria dała radę się wyślizgnąć tej roślinie. Dotyk afrodyzjaków z pnączopętli też udało się Silurii z siebie strząsnąć.

Szybko złapała Ignata - ten zauważył, że Siluria odświeżająco wygląda (bardzo lekko ubrana; szlafroczek # bielizna).

* Oi, Silurio, bardzo odświeżająco wyglądasz - rozradowany Ignat
* Właśnie zwiałam Inferni. - Siluria, z szerokim uśmiechem
* Ooo? - zainteresowany Ignat
* Chcesz skorzystać? Z pewnością nie będzie przeciw :> - Siluria, chcąc zmniejszyć zemstę Inferni

Siluria poszła do pokoju się przebrać. Tam zapukał do niej... Błażej. Siluria zaprosiła - nie przejmuje się stanem garderoby. Błażej się uśmiechnął szeroko i przeszedł do sprawy - za trzy dni jest konkurs organizowany przez Świecę o najlepszy reportaż, co pół roku. Wygrywa regularnie Jolanta Sowińska. Błażej się rozsądnie nie mieszał w takie tematy... ale KTOŚ go zapisał. I teraz jest problem...

Temat to "Muzyka, która zmieniła świat".

Siluria zaproponowała Błażejowi pomyślenie o współpracy z Melodią Diakon. Błażej powiedział, że nie pomyślał o działaniu POZA KADEMem; ale to ma sens. On i Melodia kształtujący pieśnią konstrukcję w Pryzmacie... Błażej jednak powiedział, że głównym problemem będzie brak współpracy magów KADEMu i to, że jest solo.

* Za Jolantą Sowińską stoi cała Srebrna Świeca. Za mną stoję... ja. - zirytowany Błażej
* No jak? A Instytut Pryzmaturgii? - zdziwiona Siluria
* Bawią się z jakimś dwunastościanem... - zrezygnowany Błażej
* ... - Siluria próbuje nie parsknąć śmiechem

Siluria zadzwoniła do Melodii. Millenianka odebrała i się ucieszyła słysząc swoją kuzynkę. Melodia zaprosiła Silurię i Błażeja na "Różowego Łabędzia" - statek w "porcie" Piroskim. Siluria jadąc tam przedstawiła Błażejowi jakiego typu osobą jest Melodia.

* Innymi słowy, ona się nie lubi bawić? - zaskoczony Błażej
* O, lubi. Bardzo lubi. Ona po prostu bawi się bardziej poważnie - Siluria, spokojnie

Siluria i Błażej weszli na pokład "Różowego Łabędzia". Statek jest mały i przytulny. Naprawdę mały. Jaka rzeka, taki okręt. Taka duża motorówka... Melodia poczęstowała winem i oceniła Błażeja uważnie. Nie wypadł bardzo źle w jej oczach. Błażej też przyjrzał się Melodii. Melodia powiedziała Błażejowi, że ten jej się podoba. On jej się zrewanżował, że wydała naprawdę zacną płytę. Siluria patrzyła z uśmiechem.

Siluria powiedziała jak wygląda sytuacja - Błażeja ktoś zapisał na konkurs. Jeszcze nie do końca wiadomo, o co chodzi, ale Siluria się dowie. Faworytką konkursu jest Jolanta Sowińska - ona wygrywa go regularnie. I Siluria pomyślała, że można stworzyć pieśń, która może RZECZYWIŚCIE coś zmienić i wzmocnić ten efekt Pryzmatem... może uda się panience Sowińskiej utrzeć nosa. Zwłaszcza, jeśli ktoś pierwszy raz biorący udział utrze jej nosa. Innymi słowy, Siluria zagrała na ambicji Melodii.

Melodia przyklasnęła - świetny pomysł. Oki, co chcą zrobić, co naprawić, co sprawić, by było lepsze?

Błażej zaproponował coś, co do KADEMu może niespecjalnie pasuje - ale 'więcej współpracy, mniej walki' między gildiami. Można rywalizować, ale po co WALCZYĆ między sobą? Siluria wie, skąd mu się to wzięło. Ma w głowie wewnętrzne spory na KADEMie, brak kohezji, oddzielność 'AD', ciągłe starcia...

Silurii spodobał się ten pomysł. Co więcej, spodobał się też Melodii. Czyli trzeba znaleźć magów z innych gildii dodatkowo. I - najlepiej - niech KADEM zasponsoruje trochę (np. Instytut Pryzmaturgii).

I mają na to jeszcze 2 dni i ten dzień. Mały pikuś ;-).

Siluria chciałaby zdobyć wsparcie Konrada Sowińskiego. Siluria wie, że w poprzednich okresach też rywalizował z Jolantą raz czy dwa - zawsze przegrywał. Na bazie jej niewielkiej wiedzy o konkursie (hej, nie jej obszar), nie jest postrzegany jako groźny i zdolny do zagrożenia Jolancie. Konrad jest bardzo ambitny i CHCE pokonać Jolantę. Ludźmi się nie interesuje - ani wrogi, ani przyjazny. On próbuje redefiniować reportaż. Jolanta działa 'typowymi sposobami reportażu', starymi metodami. On chce być 'disruptorem', jak Dolina Krzemowa - zrobić to INACZEJ, LEPIEJ. Bawi się formami, jest skłonny do innych, nowych rzeczy. Poznali się, gdy Konrad próbował opisać złożone relacje między gildiami i Millennium... zaproponowało Silurię jako jego rozmówczynię. A Siluria przecież z KADEMu.

Siluria więc przejechała się do Kompleksu Centralnego Srebrnej Świecy w Kopalinie, do obszaru gościnnego (dokładniej: Restauracji Dyskretnej), by spotkać się z Konradem. I w rzeczy samej, przyszedł się z nią spotkać. Konrad, zapytany czy wie coś o konkursie powiedział, że się zapisał - i tym razem ma zwycięstwo w kieszeni. 

* No dobrze, ale jak to będzie wyglądało? Pokonam Jolantę tylko dlatego, że jestem w sojuszu z kimś?! - Konrad, wzburzony
* Nie w kontekście tematu. - Siluria
* Będę tym zawsze słabszym. - Konrad
* Tylko, jak tak o sobie myślisz. - Siluria

Siluria nadal próbuje namówić Konrada na sojusz przeciwko Jolancie. Powiedziała, że ktoś wrobił Błażeja i on może to teraz wykorzystać. Konrad lubi normalnie współpracować, to bardzo rozsądny mag, ale tym razem chciał wygrać z Jolantą SAMEMU. Ale Siluria zauważyła, że ktoś zaczął już grać politycznie. (Fail): Siluria zauważyła, że kto wie, może (najpewniej) Jolanta wykorzystuje co roku wpływy polityczne i być może tylko dlatego wygrywa; wbija silniejszy klin między Konrada i Jolantę.

Konrad zgodził się, niechętnie, ale się zgodził na potencjalną współpracę. Porozmawia z Błażejem i Melodią.

(a tymczasem Siluria zdąży ostrzec Błażeja i Melodię jak wygląda sytuacja)

Na KADEMie w restauracyjce czekają już Błażej i Melodia. Siluria tam wróciła.

* Hej, musimy z TYM magiem współpracować jeśli jest taki? - Melodia, lekko zirytowana
* On nie jest taki zły. Jest rozdrażniony kuzynką. - Siluria, z uśmiechem
* A co ona takiego zrobiła? - zaciekawiona Melodia
* Co roku bezczelnie go ogrywa - rozbawiona Siluria

Melodia zauważyła punkt widzenia Silurii. Przekonać nie tych, których łatwo przekonać. Przekonać tych, którzy są FAKTYCZNIE trudni do przekonania i niekoniecznie chętni do współpracy.

Silurii przemiłą rozmowę przerwał telefon od Mordecji. Mordecja zauważyła, że co prawda kradzież nie została dokonana przez magów KADEMu (podłożono ślady), ale faktycznie magowie KADEMu weszli i coś zwinęli. Czyli byli tacy magowie KADEMu, którzy faktycznie coś zwinęli. I to jest kłopot; ona na razie opóźnia rewelacje. Siluria poprosiła Mordecję o rozmowę z Ignatem; jeśli magowie KADEMu za tym stoją, Ignat się tego dowie. Mordecja się zgodziła.

Siluria przeprosiła na moment Błażeja i Melodię i poszła szukać Ignata. Znalazła go w sali treningowej.

* Heeej :D - rozradowany Ignat - Miałaś rację wysyłając mnie do Inferni.
* Zła była, co? - Siluria, z udawaną troską
* No można tak powiedzieć - Ignat, z szerokim wyszczerzem.

Siluria powiedziała Ignatowi, jaka jest sytuacja. I powiedziała, że część prawdziwych śladów pochodzi z KADEMu. I ona musi wiedzieć kto i co. Ignat spoważniał. Spojrzy na to. Pogada z Mordecją i pozna sprawcę; Siluria dostanie go rano. Siluria cmoknęła w policzek i zwiała na spotkanie w Kompleksie - Błażej, Melodia i Konrad.

Melodia zaproponowała maga z Powiewu Świeżości. Konrad się żachnął - zamierzają każdą gildię planktonową zapraszać? Melodia powiedziała, że takie działania są nie tylko dla wielkich gildii. Siluria, chcąc rozładować napięcie, zaproponowała Juliana z Powiewu Świeżości - Powiew stanowi dobry symbol. Siluria dąży do tego, by ich oboje zawstydzić; zintegrować. Motywem przewodnim tego przedstawienia ma być współpraca i współistnienie - a oni na dzień dobry zaczynają walczyć. Ustawiła cały ten konflikt kontra Jolanta. To ona jest tym, kogo tutaj trzeba pokonać.

Udało się to Silurii osiągnąć, aczkolwiek dała wszystkim obecnym do zrozumienia, że to wszystko się udaje tylko dzięki niej...

Dzień 2:

Rano, do Silurii przyszedł Ignat z wieściami i ploteczkami.

* Lucjan Kopidół rozłożył ten dwunastościan. Zabawne, bo Whisper nie zamieniła go na ten inny. Czyli to ten "niezniszczalny".
* Ignat wie, kto zapieprzył coś z rzeczy do Kropiaktorium. Marian Łajdak. Coś zajumał... i coś innego podłożył.
* Te fałszywe ślady - Mordecja nad tym pracuje. Oczywiście, tienowaci. W czym - ktoś kto podłożył te fałszywe ślady wiedział, co zrobił Marian.

Znaczy, trzeba delikatnie.

Siluria wie, że Łajdak się interesuje jej kuzynką, Draceną - acz bez wzajemności - więc nie ma oporów przed zaciągnięciem go do łóżka. A w ten sposób dowie się od niego wszystkiego, co chce wiedzieć. Poszła więc przygotować sobie zaklęciem pokój wszelkich przyjemności - wzmocniła sobie Halę Symulacji. Nie wyjdzie stamtąd. Nie, póki ona z nim nie skończy. Sama się przygotowała - feromony, makijażyk... i poszła na polowanie. 

Marian niczego się nie spodziewa. Spotkał Silurię i się ucieszył. Diakonka przywitała go słodko. SIluria powiedziała mu, że chce mu coś pokazać w Hali Symulacji. Marian się nie domyślił co go czeka, ale ochoczo podążył. 

* Wow... przygotowałaś tą symulację dość... ciekawie - Marian, który nie wie co powiedzieć
* Nie uważasz? - uśmiechnięta Siluria, pozbywająca się ubrania

Gdy Marian zaczął się dobierać do Silurii, ta powiedziała, że chce przetestować kilka zaklęć. Ochoczo się zgodził.

* Pierwsze zaklęcie sprawia, że jest bardziej receptywny... na wszystko
* Kilka kolejnych jako odwrócenie uwagi
* Pre-ekstaza Tormentii, by ona kontrolowała jego stan.

Hala Symulacji jest ustawiona tak, by go nie wypuścić bez osiągnięcia przez niego orgazmu. Co jest zablokowane Pre-ekstazą Tormentii. Innymi słowy, jest tam tak długo, jak Siluria chce.

Siluria przez chwilę pozwala mu się dobierać, po czym przejmuje kontrolę. I krok po kroku prowadzi do jednego - niech on się przyzna.

Przyznał się. Okazuje się, że młodzi zabrali niebezpieczne artefakty; chcieli (przypadkowo) przekazać do Kropiaktorium rzeczy niebezpieczne dla nich i dla KADEMu. Więc Łajdak rozwiązał ten problem - podmienił to, co było niewłaściwe na to, co mogło być dostarczone. Niestety, zorientował się za późno - jak już artefakty były dostarczone do Kropiaktorium. Musiał więc zadziałać tak jak zadziałał. Ale szkoda, że nie pogadał. Nie jest to jednak metoda jaką KADEM często stosuje, niestety.

Dowiedziawszy się wszystkiego, Siluria poszła do Ignata; muszą pomóc Mordecji poznać prawdę. Przynajmniej tyle, że Marian podłożył rzeczy podobnej wartości...

* Łajdak... ty łajdaku - Ignat w dobrym humorze
* Ignat... - Marian, wyraźnie nieszczęśliwy

Siluria wyjaśniła, że ktoś ukradł ładunek. I przez to co Marian zrobił, kradzieżą ładunku obarczono KADEM. Ignat powiedział Silurii, że teraz on i Marian się tym zajmą; poproszą o pomoc jakiegoś eksperta z KADEMu i Mordecję. Niech ekspertem będzie... Andżelika. W końcu mało kto tak jak ona nie potrafi analizować magii i tego typu rzeczy. I tak to Ignat # Andżelika po raz pierwszy zaczęli prawdziwą współpracę.

Whisperwind przerwała ich rozmowę. Poprosiła Silurię na stronę i powiedziała, że Siluria jest bardzo silnie i bardzo uważnie obserwowana. Odcisnęło się to w Pryzmacie. Lekkie echo tego ma też Błażej Falka; ale Whisperwind szczególnie zainteresowała się Silurią. To tak silne zainteresowanie, że aż Whisper to zauważyła. W czym, Whisper akurat teraz jest wyczulona; próbuje dojść do tego jak Kopidół zniszczył tamten Dwunastościan. Siluria podziękowała za ostrzeżenie... NADAL w końcu nie wie, kto stoi za zapisaniem Błażeja na konkurs.

Krótki telefon Silurii do Mordecji. Poprosiła, by Mordecja dowiedziała się dla niej KTO zapisał Błażeja. Tak, by sygnał nie wrócił do Silurii. Mordecja ostrzegła, że to może potrwać. Siluria powiedziała, że nie dba.

Siluria próbowała przekonać Whisperwind, żeby ta ją pośledziła i spróbowała wykryć osobę JĄ śledzącą. Whisper stwierdziła, że nie jest szczególnie zainteresowana. Nie widzi szczególnych problemów ani ryzyk dla Silurii. Ona sama też nie próbowała Whisper przekonać.

Siluria poszła do Powiewu Świeżości, spotkać się z Julianem Krukowiczem. Mag jest czarujący; Siluria jednak poprosiła go o spacerek - nie chce rozmawiać z demonami podsłuchującymi na lewo i prawo.

* Chciałem się też dostać, ale mnie nie zaakceptowali. Zaproponowałem chórek śpiewających węży... - Julian, czarująco
* Ale... - Siluria, zaskoczona samym pomysłem
* To zmienia świat! - Julian, zdecydowanie
* Do tego może się nie przyznawaj... - Siluria, lekko zaskoczona

Siluria wyjaśniła Julianowi o co chodzi. Julian od razu wpadł na kilka pomysłów.

* Wprowadzę śpiewające węże na salony! - Julian, zapalając się
* Nie... to jeszcze nie ten moment. - Siluria, poważniej
* Mam też pewien pomysł wymagający podstaw kuromancji - Julian, zapalając się coraz mocniej
* Czarodzieju Krukowicz... kuromancja? Obawiam się, że co najmniej jeden z tych magów jest z tych poważniejszych... - poważniejąca Siluria

Siluria - z trudem - ale przekonała Juliana by się dostosował do reszty. Jednak... jednak potęga kuryzmu czy śpiewających węży... nie powinna mieć tu miejsca. Julian przebrał się w swój standardowy elegancki 'showmański' strój i pojechali z Silurią do Kompleksu Centralnego.

Melodia i Julian od razu przypadli sobie do gustu. Konrad i Julian mniej. Błażej i Julian - whatever. Błażej z przyjemnością się dopasuje. Współpraca przede wszystkim. Trochę trwało kalibrowanie i dopasowanie; jednak się udało. Siluria, wykończona i padnięta, wróciła do KADEMu. I tam - wiadomość od Mordecji. "Artur Żupan". Tani drań. I zdaniem Mordecji Żupan niekoniecznie jest podłożony. Bo część lootu ma.

Siluria przekazała temat Marianowi Łajdakowi. KADEM musi jakoś rozwiązać tą sprawę między Żupanem, Kropiaktorium, Rojowcem i Łajdakiem.

By móc umożliwić tajemniczej osobie skontaktować się z nią na KADEMie, zdecydowała się tą noc spędzić na zwykłym KADEMie - nie KADEMie Daemonica.

Dzień 3:

Zbliża się czas konkursu. Siluria odwiedziła kwartet chcąc zobaczyć, jak im idzie. Po przyjacielsku zajrzała. Całkiem nieźle sobie poradzili. Siluria jest zadowolona - serio. Mają efekty. Siluria dostała szybki telefon od Mordecji. Ta doszła do tego, kto zapisał Błażeja. Odpowiedź BARDZO zaskoczyła Silurię - Jolanta Sowińska. To ona zapisała swojego potencjalnie największego konkurenta.
...to nie do końca ma sens.

Siluria połączyła to z jeszcze jednym faktem - Konrad wydawał się BARDZO pewny tego, że w tym roku wygra gdy Siluria do niego przyszła. No ale Jolanta bierze udział...

Jolanta jest Silurii znana jako czarodziejka oddana Świecy. Przed dodanie Błażeja - KADEM nie miał dużych szans na wygraną. Chyba, że KADEM będzie współpracował ze sobą. Ale nie mogła przewidzieć innych gildii, a już w zupełności nie mogła przewidzieć tego, że KONRAD wejdzie w sojusz. 

Na szczęście, kwartet jest pierwszy. Więc cokolwiek Jolanta nie wymyśliła, nie wpłynie negatywnie na Kwartet...

Jedyne, co Siluria zdecydowała się zrobić nie znając agendy Jolanty - wzmocnić narrację. Sprawić, by była jak najmniej wykorzystywalna przez kogokolwiek innego.

Konkurs.

Kwartet zrobił świetną opowieść pokazującą POWER OF WSPÓŁPRACA. Z pieśnią. Ze wszystkim. Nawet pojawił się tam JEDEN śpiewający wąż. Julian wywalczył. Narracja świetna (Błażej), choreografia zabójcza (Julian), Konrad zrobił bardzo dobre dopasowanie do grupy docelowej a Melodia pozamiatała poziomem kunsztu. Siluria dałaby im pierwsze miejsce od ręki. Opowieść, jak to czterech magów z czterech gildii zdecydowało się na połączyć siły w ramach epickiej współpracy.

Potem wyszła Jolanta.

Jolanta wyszła z bardziej klasycznym reportażem. Pokazała pieśń, którą zmieniła rzeczywistość. Nagraną. Ona używa iluzji w czasie rzeczywistym by przedstawić zjawiska. Pokazała Konrada, który ją zaczepił, że teraz ją pokonał. Ona zanuciła "łatwe zwycięstwo". I to była ta pieśń. Pokazała, jak Konrad wbił się w struktury polityczne. Jak uniemożliwił Jolancie zwycięstwo - nie kupował głosów dla siebie, ale zapewnił, że ona nie wygra. Jeśli miałaby wygrać, musiałaby zacząć walkę frakcji we własnym rodzie.

Pokazała, jak Kwartet wszedł w sojusz (bez niektórych dialogów; same zjawiska). Pokazała to tak, by uwypuklić sojusz w formie pozytywnej, nie skupiając się kto co zrobił. I zakończyła reportaż słowami, że ta jedna piosenka, którą zrobiła najpewniej odebrała Świecy zwycięstwo po raz pierwszy od dawna. Świeca powinna współpracować ze sobą, a nie walczyć. Bo współpraca się zawsze opłaca.

Kwartet wygrał. Ale to Jolanta jest najbardziej zadowolona. A Konrad jest wkurzony na maksa.

# Progresja


# Streszczenie

Jolanta Sowińska i Konrad Sowiński od dawna ścierają się w konkursie. Konrad zadziałał politycznie by Jola nie mogła wygrać; ta, zamiast eskalować, dołączyła biednego Błażeja Falkę z KADEMu - by on wygrał. Błażej poszedł do Silurii, która sformowała Kwartet - Konrad, Błażej, Melodia i Julian. Wspólnie zrobili opowieść o Potędze Współpracy. Jolanta chciała pokazać, że Świeca ma współpracować, ale Siluria zmieniła przekaz. Nadal - Jolanta splugawiła sukces Konrada (acz niekoniecznie Kwartetu). Jednocześnie, Siluria odkryła, że Artur Żupan zakosił artefakty do Kropiaktorium i zrzucił winę na Mariana Łajdaka. Czemu? Bo on podmienił część artefaktów pierwszy (by ratować KADEM przed problemami). Lucjan Kopidół zniszczył "niezniszczalny" Dwunastościan ku zdziwieniu Whisperwind.

# Zasługi

* mag: Siluria Diakon, łagodząca spory między magami z różnych gildii i ratująca z jednej strony Łajdaka a z drugiej Błażeja Falkę. Bardzo balansuje.
* mag: Infernia Diakon, która nadal wraz z Silurią próbują sprawdzić która będzie 'u góry'.
* mag: Ignat Zajcew, detektyw, który 'skorzystał' z okazji z Infernią by potem znaleźć wspólnie z Mordecją KADEMowego winowację - Mariana Ł.
* mag: Błażej Falka, KADEMowy 'wordsmith' i śpiewak zarządzający radiowęzłem, którego 'ktoś' zapisał na konkurs. Chce współpracować z innymi.
* mag: Melodia Diakon, ściągnięta przez Silurię jako 'ciężka artyleria' na konkurs; pragmatyczna idealistka ścierająca się z Konradem Sowińskim.
* mag: Jolanta Sowińska, włączyła Błażeja w konkurs 'prawie Świecy' gdy nie mogła wygrać - by nie przegrać i by pokazać, że Świeca powinna współpracować. Wyszło tyci inaczej.
* mag: Konrad Sowiński, który BARDZO chciał pokonać Jolantę, załatwił jej wykluczenie u sędziów a potem po namowach Silurii dołączył do kwartetu.
* mag: Mordecja Diakon, nieoceniona detektyw, która odkrywa sekrety za wieloma planami i dostarcza Silurii wszystkie informacje zanim dostaną je klienci.
* mag: Lucjan Kopidół, który w jakiś sposób zniszczył ten Dwunastościan... który przecież Whisper nazwała 'niezniszczalnym'.
* mag: Marian Łajdak, który podmienił część KADEMowych artefaktów do Kropiaktorium na mniej niebezpieczne... i się przyznał Silurii po 'zachęcie'.
* mag: Andżelika Leszczyńska, pomagała Ignatowi i Mordecji dowiedzieć się kto naprawdę ukradł te artefakty z Kropiaktorium jako ekspert od magii detekcyjnej.
* mag: Whisperwind, ostrzegła Silurię, że ktoś się nią bardzo interesuje. Tak bardzo, że odbiło się w Pryzmacie.
* mag: Julian Krukowicz, dołączył do zespołu Melodia-Błażej-Konrad-Julian by pokazać potęgę współpracy między gildiami. Wcisnął jednego śpiewającego węża do przedstawienia.
* mag: Artur Żupan, który porwał część artefaktów KADEMu przeznaczonych do Kropiaktorium i wrobił w to Mariana Łajdaka.

# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Piróg Dolny
                    1. Ulica Rzeczna
                        1. Port rzeczny, gdzie zacumował "Różowy Łabądź" należący min. do Melodii Diakon
                1. Kopalin
                    1. Nowy Kopalin
                        1. Powiew Świeżości, skąd Siluria zwinęła Juliana do Kwartetu
                    1. Centrum
                        1. Kompleks Centralny Srebrnej Świecy, gdzie odbywały się wszystkie spotkania Konrada, Melodii, Juliana, Błażeja i Silurii
                            1. Pion ogólnodostępny
                                1. Restauracja Dyskretna, gdzie w jednej z salek Siluria spotkała się z Konradem Sowińskim
                        1. KADEM Primus
                            1. Zewnętrzny krąg
                                1. Sale spotkań, gdzie Siluria, Melodia i Błażej się zapoznawali z nowym konceptem
    1. Faza Daemonica
        1. Mare Vortex
            1. Zamek As'caen
                1. KADEM Daemonica
                    1. Układ nerwowy
                        1. Sale symulacji, gdzie Siluria dowiadywała się od Mariana Łajdaka o co chodzi.

# Czas

* Dni: 3
---
layout: inwazja-konspekt
title:  "Zagłodzona ekspedycja Świecy"
campaign: powrot-karradraela
gm: żółw
players: kić, dust
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [170122 - Gambit Anety Rainer (AW)](170122-gambit-anety-rainer.html)

### Chronologiczna

* [161012 - Kontratak Karradraela (HB, KB, SD)](161012-kontratak-karradraela.html)

## Kontekst ogólny sytuacji
## Punkt zerowy:

## Misja właściwa:

Dzień 1:

Samochód. Opel Astra. Poza rzeczywistością... w Pomiędzy.
I nikt nie ma pojęcia jak ich wyciągnąć. A na pewno nie Andżelika, która uruchomiła skok w Pomiędzy by znaleźć Matuzalema.

"Wytwory Setona nikogo intencjonalnie nie zabiły" - Siluria
"Jeżeli Matuzalem nie uciekł, NIE naciskajcie klawisza 'znajdź'. Samochód się zawiesza." - kartka zostawiona przez Setona w schowku
"Momencik! Pomyślmy jak Seton!" - Ignat, na którego spojrzała z politowaniem Siluria...

Hektor wykorzystuje swoje techniki śledcze by znaleźć wskazówki - nie wierzy w to, że nie ma opcji dowiedzieć się jak z tego wyjść. Andżelika, Ignat i Siluria opowiadają mu o Setonie. Hektor zauważył logikę w działaniach Setona i użył ręcznego hamulca. Przerwał operację Opla Astra i... wypadli z Pomiędzy.
Na Fazę Esuriit.

Opel Astra znalazł się na... Księżycu. A przynajmniej tak wygląda. Na pewno nie wygląda to jak Faza Daemonica...
W środku... miasta? Miasteczka raczej. I zbierają się magowie... jeden ma pistolet.
Andżelika powiedziała co to - Faza Esuriit. Miejsce, które zastępuje energię maga na energię tego miejsca. Niekompatybilna z Primusem.

Siluria rozchełstała dekolt. Gotowa do wyjścia...
Anna jest PRZERAŻONA. Czuje energię Fazy Esuriit. Czuje energię... Spustoszenia.

Siluria próbuje wzbudzić litość u magów z AK-47. Wystarczyło; magowie opuścili broń.

Marianna Sowińska kazała Jakubowi przebadać wszystkich - są czyści od Energii Esuriit (Anna Myszeczka jest silnie receptywna na Fazę Esuriit, co potwierdza hipotezę połączenia Spustoszenia z tą Fazą). Pozwoliła im wejść do Srebrnej Septy. Wyjaśniła, że są placówką Świecy która została odcięta od Świecy Daemonica...

"Ściągnęliśmy Syberię na Fazę Esuriit, po czym karmimy syberion lokalną energią magiczną (fauną i florą). Czerwie utrzymują nas przy życiu." - Marianna.
"Ja pierdolę, chcę zobaczyć tutejsze śledzie!" - Ignat

Czyli są w "Fazie Daemonica", sprowadzonej do Fazy Esuriit. 
Siluria się przyznała Mariannie - nie jest "tien". KADEMka. Przedstawiła wszystkich po gildiach. 
Marianna powiedziała, że na tym etapie czymkolwiek by nie byli, są miło witani.

Marianna powiedziała - mają tu kilkunastu magów i 147 ludzi. Ludzie generują pryzmat pozwalający Fazie Daemonica działać. Magowie mogą czarować. Głodna i nienawistna Faza Esuriit (tak, ona żyje i jest inteligentna) nie może się wbić dzięki defensywom. Ale Marianna nie powiedziała jakie to defensywy...

Siluria wygrzebała z bagażnika Opla Astra jedne z okularków Setona. Spojrzała na osoby obecne, póki jest w stanie - zobaczyła:
- czerw Hektora jest 'jasny' i Hektor jest 'jasny'
- czerw Anny jest 'jasny', ale w jej ciele są RANY od Spustoszenia koloru 'czarnego'
- czerw Marianny jest 'ciemny', ona jest szara (ale jasna)
- czerw Jakuba jest 'ciemny', on jest szary (ale jasny)
- czerw Dosifieja jest 'ciemny', on jest czarny (Esuriit go pożarł, on wyraźnie o tym nie wie)

No to klops.
Czyli w tej populacji są tacy i tacy. Nie wiemy kto jest kim... trochę tłumaczy, czemu Portal mógł zgasnąć a eksperci od Portali zginąć.

Spustoszenie pochodzi... stąd?

Problem w tym, że Opel Astra jest niesprawny. Potrzebuje energii...
Anna jest w szoku. Wzięła Hektora na stronę i powiedziała co następuje:
- by się stąd wydostać, potrzeba energii dla Opla Astry
- energia - trzeba zabić wszystkich i magią krwi przekierować
- mogą uciec

Hektor jest lekko przestraszony jej podejściem. She is losing it. Energia Esuriit działa na nią bardzo skutecznie...

Marianna wysłała swoich terminusów na polowanie na lokalną faunę, florę i energię magiczną. By wzmocnić syberion. Opuściła ich, zostawiając Łukiję by ta podsłuchiwała a oni nie wiedzą...

Andżelika zaproponowała rozłożenie Opla Astra by móc uratować wszystkich. Nie leży to w planach Silurii - Siluria polubiła Opla Astra.
Wypracowali plan - ściągnięcie pryzmatem Zamek As'caen...

Mikado, Hektor dołączyli do Silurii na boku. Ona wyjaśniła im o tym, że w Setonowych okularach widziała, jak jeden z magów jest już pożarty przez Esuriit...
W krótkiej rozmowie - wyszło wyraźnie, że lokalni nic nie wiedzą... być może są tu tylko tydzień a Esuriit już skutecznie wypaczył ich percepcję.
Zgodnie z danymi Andżeliki, Portal mógł się zamknąć... 2 tygodnie temu? Trzy tygodnie temu? Na pewno nie rok jak czuje Marianna.

Co odpiera Esuriit? Jak to się dzieje, że Esuriit jest odparty przez Srebrną Septę?

Siluria wyjęła kosmetyki i środki działające przeciwko Wiktora Sowińskiego. Poszła do Marianny dać jej nadzieję. Zainspirować ją. Sprawić, by ta zaufała Silurii, że Siluria ją z tego wyciągnie - ich z tego wyciągnie. I udało jej się. Położyła reputację KADEMu i swój wrodzony optymizm.
Marianna została pozytywnie zainspirowana. Trochę odżyła. Dała Zespołowi wolne poruszanie się po Sepcie i powiedziała coś zmrażającego: Septa to SIÓDMA placówka. Było siedem placówek. Stracili kontakt z pozostałymi sześcioma (i ze Świecą Daemonica) gdy zamknął się Portal...
Tak jak stracili dostęp do Świecy Daemonica. 

Siluria spytała, co zasila Syberion. Marianna powiedziała, że Siluria nie chce wiedzieć - podjęła pewne decyzje które ściągną na Mariannę wyrok śmierci jak ta wróci. Zapytana jak często sprawdzane są osoby pod kątem Esuriit, Marianna powiedziała, że raz dziennie. Zanim postawili defensywy pryzmatyczne... tracili magów i ludzi.
Marianna powiedziała, że mają bardkę Zajcewów i ona zna się na pryzmacie. Siluria się ucieszyła. Tego szukają...

Hektor. Speculoid wypuścił wizję dwójki bawiących się dzieci. Anna poszła za nimi - morale bardzo do góry. Dzieci weszły w zaułek i któreś się przewróciło i rozpłakało. Anna chciała iść, ale Hektor ją powstrzymał; pułapka Speculoida nie została zmaterializowana...
Ale przecież... przecież... speculoid nie ma dostępu do Srebrnej Septy.

Esuriit żywi się emocjami - to pasuje do teorii.
Hektor przekazał informacje Silurii. Jest niedobrze...

# Progresja


# Streszczenie

Opel Astra wylądował Pomiędzy, ale szczęśliwie udało się go sprowadzić na... Fazę Esuriit - niebezpieczną, inteligentną i żarłoczną Fazę. Prosto do placówki Świecy, która utraciła kontakt ze Świecą Daemonica. Siluria i Hektor łatwo wkupili się w łaski podłamanej Marianny Sowińskiej; zwłaszcza, że mają plan ewakuacji. Sama placówka ma kilkanaście magów i ~ 150 ludzi, mają Pryzmat z syberionu i dziwne systemy ochronne. Gorzej, że placówkę nęka speculoid, część magów i ludzi stało się Istotami Esuriit (pożarci przez Esuriit) i nikt tego nie widzi...

# Zasługi

* mag: Hektor Blakenbauer, opiekun Anny powstrzymujący ją przed pułapką speculoida. Najstabilniejszy i budzący szacunek. Dookoła niego się gromadzą. Wydedukował jak używać Opla Astra Setona.
* mag: Siluria Diakon, rozwiązująca problemy z brakiem zaufania w typowo Diakoński sposób; nie zastrzelili a potem podniosła morale. Ma plan.
* mag: Andżelika Leszczyńska, mająca mnóstwo przydatnych informacji min. o Świecy i o Fazie Esuriit
* mag: Ignat Zajcew, chciał poderwać Mariannę, ale mu nie wyszło. Nadzieja Silurii, że umie używać Pryzmatu...
* mag: Mikado Diakon, stabilny i lekko podłamany terminus próbujący utrzymać rzeczywistość pod jakąkolwiek kontrolą.
* mag: Anna Myszeczka, która po wszystkich rzeczach jakie się wydarzyły powoli ulega energii Esuriit; jeszcze można na niej polegać.
* mag: Marianna Sowińska, dowódca; 'czysta', podjęła grupę fatalnych (ale skutecznych) decyzji jak przetrwać na Fazie Esuriit. Chce do domu.
* mag: Jakub Pestka, badacz i lekarz; 'czysty', zaufany Marianny i wiemy, że nie zauważa tych, których Esuriit już pożarło.
* mag: Dosifiej Zajcew, * mag bojowy; 'pożarty', terminus regularnie zasilający syberion który stał się już Istotą Esuriit.
* vic: Siriratharin, speculoid; odpierany przez Srebrną Septę i monitorujący okolicę, by się pożywić istotami z Primusa. Zastawia pułapki.

# Lokalizacje

1. Świat
    1. Pomiędzy, gdzie skończył Opel Astra Setona z osobami na pokładzie do momentu korekty przez Silurię
    1. Faza Daemonica
        1. Mare Crucio
            1. Srebrna Septa
                1. Portal, ten sam co na fazie Esuriit; sprzężony z Portalem na Świecy Daemonica
    1. Faza Esuriit, która chce wszystkich zjeść; inteligentna i złowroga
        1. Insula Luna, archipelag "wysp w kosmosie" Esuriit
            1. Srebrna Septa, placówka Srebrnej Świecy numer siedem, gdzie znajdują się magowie i ludzie generujący Pryzmat
                1. Centrum
                    1. Centrala Świecy, skąd dowodzi Marianna Sowińska
                    1. Blockhaus K, gdzie znajduje się Zespół tymczasowo
                1. Wschodnia wyspa
                    1. Portal, ten sam co na fazie Daemonica; sprzężony z Portalem na Świecy Daemonica
  
# Skrypt

brak

# Czas

* Dni: 1
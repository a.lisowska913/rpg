---
layout: inwazja-konspekt
title:  "Hybryda w bazie Skorpiona"
campaign: powrot-karradraela
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [151007 - Nigdy dość przyjaciół: Szlachta i Kurtyna (HB, SD)](151007-nigdy-dosc-przyjaciol-szlachta-kurtyna.html)

### Chronologiczna

* [150826 - Pętla dookoła Pauliny (PT)](150826-petla-dookola-pauliny.html)

### Punkt zerowy:

## Misja właściwa:

### Faza 1: Dwóch chorych w organizacji paramilitarnej

Po akcji Jolanty, Paulina zdecydowanie nie chciała być w samym centrum Kopalina gdy artykuł uderzy...
Maria jeszcze nie znalazła kryjówki, lecz Olga miała coś o tych chorych co zainteresowało Paulinę.

Z Pauliną skontaktowała się Olga. Powiedziała, że dostała dostęp do chorych ludzi. Niestety, Olga i Paulina muszą tam pojechać z zawiązanymi oczami... alternatywą jest to, że chorzy ulegną egzekucji. Olga powiedziała jeszcze, że sytuacja jest dość tragiczna - chorymi są lekarz i jego pacjent; lekarz - człowiek - próbował użyć artefaktu by pomóc swojemu pacjentowi i niestety, artefakt nie zadziałał poprawnie. Artefakt miał być typu 'cleanse', ale nie zadziałał, pacjent wszedł w kontakt fizyczny z lekarzem i go przekształcił. Na pacjencie eksperymentowali magowie i puścili go wolno, by "wrócił" do miejsca pochodzenia. Został zmieniony w żywą pułapkę.
Stąd rekomendacja egzekucji w wypadku, w którym Paulina (Olga) nie będzie w stanie pomóc.

Paulina wchodzi w to. Ustaliły z Olgą, że Paulina jest przedstawiona jako czarodziejka, która ma dług wdzięczności u Olgi i nie ma nic przeciwko pomocy ludziom. Ale - by chronić Paulinę - Olga jest punktem kontaktowym.
Lokalizacja "tego miejsca" gdzie pojadą jest dość daleko i od miasta i od aptek.

Maria oczywiście zaczęła protestować, ale Paulina ugasiła wszelkie formy protestu. Chorzy ludzie, nikt im nie pomoże -> nie ma o czym mówić. Paulina jest ich jedyną nadzieją. Plus, jakkolwiek to brzmi, Iliusitius będzie ją chronił.
Paulina wzięła ze sobą tonę magicznych specyfików, leków, zwykłych leków, elementów higienicznych, opatrunków... wszystko.

Wieczorem Olga i Paulina poszły (obładowane pakunkami) na przystanek autobusowy. Podjechał busik i kierowca z Olgą wymienił się hasłami; Paulina i Olga weszły do środka. Tam założono im srebrne bransoletki i coś na oczy, po czym kierowca zaproponował, by się wyspały. 
Maria zastanawiała się, czy nie podłożyć pluskwy, ale po namyśle stwierdziła, że to może być bardzo niebezpieczne.
Kierowca puścił gaz usypiający, więc Olga i Paulina naprawdę dobrze spały.

Rankiem Paulina obudziła się w pokoju z Olgą. Olga jeszcze śpi. Paulina zorientowała się, że musiała dostać jakimś gazem. Szczęśliwie, nie wpłynęło to negatywnie na ich działania. Olga też się obudziła; pierwsze co zrobiła to otworzyła drzwi na korytarz i zażądała od strażnika, by przyprowadził im kogoś kto może powiedzieć co i jak. 

Przyszedł do nich mężczyzna w garniturze. Przedstawił się, Olga prychnęła i powiedziała, że będzie "panem X", bo to i tak nie jest prawdziwe nazwisko; jest w złym humorze przez ten gaz. Pan X powiedział, że to było konieczne. Po krótkiej rozmowie zaprosił Olgę i Paulinę na śniadanie. Paulina się zorientowała od razu - kantyna, baza, mundury... to jest jakaś baza, to nie jest byle mała sprawa. Zbyt dobrze zorganizowani. Bardziej siły paramilitarne niż "dwóch chorych ludzi".

Maria ma coraz gorszy humor po soullinku.

Olga i Paulina jedzą lekki posiłek. Podczas rozmowy Paulina próbuje wyciągnąć coś z Pana X.
Ten nie unika odpowiedzi o pacjentach: pacjent 0 był agentem tej grupy paramilitarnej i został złapany przez maga 2 tygodnie temu. W odróżnieniu od normalnych okoliczności, grupa zdecydowała się na odbicie jednego ze swoich; dorwali maga i go ścięli. Odbili pacjenta 0, ale ten był w laboratorium maga. To spowodowało, że poddali go kwarantannie. Tydzień temu zaczął się zmieniać; okazało się, że coś jest nie tak z jego pamięcią, uważa, że ta grupa to jego wrogowie. Włączyła mu się paranoja. Zaczęły się też pojawiać zmiany fizyczne; łącznie z tym, że jego skóra zaczęła zmieniać kolory jak kameleon (kolor otoczenia). 

Doktor miał artefakt - cleansing (agent może dostarczyć go do badań). Użył go w kierunku pacjenta 0 będąc z nim w pomieszczeniu (wymaga dotyku); nie zadziałał. Za słaby, albo nie do końca poprawnie. Pacjent 0 wyrwał się z restraintów (silniejszy niż powinien być - nadludzka siła) i złapał lekarza. COŚ mu zrobił. Trzymał go i tak jakby... wniknął w niego. Świadkowie poczuli zawroty głowy i nudności. Skóra lekarza zaczęła zmieniać kolory tak samo. Lekarz odmówił wyjścia z containing chamber. Zaprosił, śmiejąc się, innych by do nich dołączyli.
Następnie zdjęli z siebie ubrania a ich ciała pokryły się lekką, delikatną łuską.

Dwa dni później gdy strażnik chciał ich nakarmić (trójfazowy containing chamber), zarówno lekarz jak i pacjent 0 zaczęli migotać wprowadzając strażnika w formę transu. Gdyby nie operator kamery który poraził strażnika taserem, najpewniej doszłoby do przebicia containment chamber. Wyraźnie lekarz i pacjent 0 komunikują się na poziomie hiveminda, gdzie pacjent 0 jest bytem dominującym.
Paulina jest ostatnią nadzieją. Jak jej się nie uda, wysadzą ich.

Zdaniem Pauliny to nie jest choroba. To inżynieria magiczna. Celowana i o określonych efektach. Paulina chce się dowiedzieć więcej - ale o pacjencie, nie o organizacji i jej metodach (konflikt->sukces). Zgodnie z tym, co powiedział, na miejscu w laboratorium znaleziono ślady następujących istot:
- pseudokameleon wzorzysty; istota o własnościach hipnotycznych, bardzo mała, występuje w rojach; paraliżująca ofiarę przez migotanie, po czym wnikająca pod skórę i rozmnażająca się pod tkanką nosiciela
- jaszczurka skorupowa; potrafi odrzucić całą skórę jak wylinkę i działać dalej; niesamowite umiejętności regeneracji
- viimanos; bardzo rzadki vicinius (hodowany przez Myszeczków jak dromopod iserat), który przekształca ciało w energię magiczną, taumatożerca. Mało wydajny.

Zdaniem Olgi ta cała historia kupy się nie trzyma. Gdyby ten mag był tak niebezpieczny, nigdy w życiu nie udałoby się tej organizacji go załatwić. Gdyby ten człowiek był uwolniony z laboratorium, nigdy w życiu nie udałoby się go uwięzić w bezpieczny sposób bez pomocy maga; a więc mają maga i mieli ten protokół opracowany wcześniej. Konflikt Pauliny pozwolił jej złapać te drobne fakty.
Pan X powiedział, że to wszystko co wie. Nie uczestniczył w tej akcji osobiście i nie wie jak wyglądała sytuacja. On jednak wierzy w swoją organizację i nie wie, żeby mieli jakiegokolwiek maga do pomocy. On nie wie, czemu mieliby poświęcać dobrego lekarza i niezłego agenta i nie wierzy w to. Olga oczywiście widzi mnóstwo celów tego typu... 

Olga pyta Paulinę, czy w to wchodzą. W jej oczach pojawił się zimny błysk - ona chce teraz wejść w tą organizację. Chce znaleźć tego maga który za tym stoi - z tej organizacji - i go zniszczyć.
Paulina powiedziała, że jej zdaniem problem JUŻ się pojawił. Jak pewni są izolatki? Przecież mówimy o kameleonie i wylince. Pseudokamelon z migotaniem, kameleon z zabawieniem i jaszczurka z wylinką. Przecież to mogło dostroić się już do kamery a w izolatce mogła zostać tylko wylinka...
Olga się drapieżnie uśmiechnęła. Ma tylko nadzieję, że teoria o hivemindzie jest prawdziwa; wtedy złapią centrum roju (pacjenta zero) i mają wszystkich. Ale to znaczy, że muszą się spieszyć.

### Faza 2: Zapieczętowane laboratorium

Paulina zażądała dostępu do laboratorium. Pan X powiedział, że nie ma sprawy; zaczął je prowadzić, lecz instynkt Olgi wstrzelił. Powiedziała, że potrzebne im jest LABORATORIUM a nie jakaś namiastka. X zaczął się tłumaczyć, że przecież nie może zdradzić sekretów a Olga zaczęła, że z jej punktu widzenia on może być już "jednym z nich". Powiedziała, że X nawet nie wie, czy to jego poczucie obowiązku czy to pseudokameleony w jego żyłach każą mu odmówić. Wyjaśniła znaczenie viimanosa - niszczenie ciała dla generacji energii magicznej to stary manewr zwłaszcza w połączeniu z regeneracją jaszczurki skorupowej; pozwala na autogenerowanie magii dla utrzymania przez Skażenie nietrwałej struktury biomagicznej. Paulina dodała, że na dłuższą metę jest to nie do utrzymania. 

Paulina przypomniała sobie, że Olga najlepiej wie jak to działa, bo przecież kiedyś sama sobie to zrobiła nie chcąc trwale utracić mocy (czas życia: Zależa Leśnego). I jej też to nie pomogło.
Pan X został złamany. Zaprowadził ich do tego laboratorium. Po drodze Olga jeszcze mruknęła do Pauliny, że za tym stoi mag rodu Myszeczków, dowodem viimanos. Tylko oni potrafią to utrzymać przy życiu.

Przed laboratorium stoi strażnik. Zasalutował X, po czym wpuścił ich do lab. Paulina chciała wejść pierwsza, ale Olga jej nie pozwoliła; sama weszła pierwsza do środka. Rozejrzała się, powąchała, po czym błyskawicznie rzuciła nożem i trafiła coś, co wygląda jak skażona wiewiórka. Wiewiórka padła na ziemie, piszcząc z bólu i sycząc w dziwny sposób. X wysunął się przed Olgę i strzelił trzy razy, dokonując egzekucji na wiewiórce.
Słysząc to, do pomieszczenia wleciał strażnik z przygotowaną bronią; złapała go jednak Olga i bardzo boleśnie i brutalnie wykręciła mu ręce. Przez ból po jego skórze przeleciała żółta fala, co zauważyła tylko Paulina. Szybko zwróciła na to uwagę Oldze, która mocniej złapała strażnika i go w pełni unieszkodliwiła, bardzo boleśnie. Dowódca (X) spojrzał nic nie rozumiejąc i prawie zaczął celować w Olgę, ale Paulina zwróciła mu uwagę a Olga przydusiła Skażonego strażnika tak, by ten stracił kontrolę.
Paulina wyjęła kajdanki strażnikowi zza pasa; strażnik wykręcił nienaturalnie szyję by polizać Paulinę (nie udała się infekcja) w odpowiedzi na co Olga wyłamała mu ręce i przebiła jego własnym żebrem płuco. Strażnik w agonii a Olga się zaśmiała. Jeśli ma cechy jaszczurki, przejdzie mu.

Na rozkaz Olgi i w odpowiedzi na całą tą sprawę X zapieczętował laboratorium. Nikt nie jest w stanie wejść ani wyjść. Mają kilka godzin spokoju. Bonusowy efekt uboczny - izolatki też zostały zapieczętowane. Nikt nie jest w stanie uwolnić pacjenta zero ani lekarza.

Olga się zainteresowała jak się nazywa Pan X, motywując, że jednak są chyba po tej samej stronie. Mężczyzna się przedstawił jako Tymoteusz Dzionek. Olga się uśmiechnęła. Ona nadal nazywa się Olga Miodownik. Dzionek zapytał Olgę, jak ona to robi jeśli nie jest magiem; skąd wie tak dużo i skąd działa tak a nie inaczej. Olga powiedziała, że przed Zaćmieniem była magiem; więcej, była dobrym terminusem. Nadal walczy ze złą magią, tylko metody jej się zmieniły.

Paulina zapytała, czy to jest strażnik, który pilnował izolatki. Dzionek odpowiedział ponuro, że nie. Paulina zauważyła, że Dzionek ma swoją odpowiedź - infekcja się rozprzestrzeniła.
Olga powiedziała, że mają kilka godzin na opracowanie antidotum. Jak te drzwi się otworzą i nie będą mieć antidotum, mają naprawdę przechlapane i muszą sobie wyciąć drogę na zewnątrz.

"Pozwól mi pobierać krew. Z czasów, jak byłam terminuską mam bardzo odporną tkankę magiczną" - Olga, kłamiąc na użytek Tymoteusza Dzionka (chroni ją w pewnym stopniu Iliusitius)

Paulina pokazała strażnika - nie wiadomo, jak szybko się zregeneruje. Strażnik wykonał szybki ruch jakby chciał się uwolnić i złapał pełnię uwagi Olgi; szczęśliwie, Paulina dostrzegła (konflikt), że wiewiórka - martwa, ale już nie - rzuca się w kierunku Dzionka. Krzyknęła "padnij", co agent Skorpiona zrobił od razu. Olga natychmiast zareagowała, odwróciła się w kierunku wiewiórki i ponownie zabiła ją swoimi nożami, po czym ją poszatkowała robiąc wiewiórcze mielone. Chwilę potem zaproponowała, że problem strażnika trzeba też rozwiązać i silnym cięciem przecięła mu rdzeń kręgowy. 
Paulinie zrobiło się lekko niedobrze. Olga widząc spojrzenie Pauliny zauważyła, że zrobiła to maksymalnie bezboleśnie. No a zawsze naprawić można to magią.
Pocieszające jak cholera.

Olga zwróciła się do Dzionka z jednym pytaniem - czy chce sam dostarczyć swoją krew, czy Olga ma mu pomóc? Dzionek nie oponował; od razu dostarczył krew, bez zbędnych dyskusji i wątpliwości. A Olga pobrała krew od strażnika i dała Paulinie trochę swojej dla porównania.

Olga zamknęła resztki wiewiórki w jakimś szczelnym pojemniku a Paulina porównała próbki krwi. Udało jej się na bazie zwykłego mikroskopowego badania krwi wyprowadzić, że Dzionek nie jest chory, strażnik i wiewiórka byli a Paulina i Olga nie są. Obecność pasożytów we krwi jest faktycznie widoczna.
Paulina pokazała Dzionkowi różnice i udowodniła, jak wygląda faktycznie sytuacja. Dzionek zbladł. Tą demonstracją Paulina go kupiła.

Chwilę potem strażnik się rozkaszlał. Zaczął rozpaczliwie kasłać i chrypieć. Gdy cała trójka na niego spojrzała, zaczął migotać. Migotanie dotknęło całą trójkę; jednak Olga i Paulina natychmiast się uwolniły. Dzionek nie. Paulina podeszła do Dzionka i mocno uderzyła go w twarz. Wyrwała go z hipnotycznego transu a Olga pogroziła strażnikowi nożem. Ten przestał migotać. Na wszelki wypadek Olga jeszcze raz walnęła go nożem w rdzeń kręgowy. I jak Paulina spojrzała na Olgę, ta znowu wygląda jak uosobienie niewinności.

Chwilę potem Olga podeszła do Pauliny i szepnęła, żeby Paulina zauważyła; jak tylko Olga zadaje ból jednej istocie roju (np. strażnikowi) to inne istoty roju (np. wiewiórka) też cierpią. Innymi słowy, Olga znalazła potencjalny słaby punkt.

Paulina przyczepiła wszystkie możliwe systemy detekcyjne tego laboratorium do nieszczęsnego strażnika. Ale najpierw chce ocenić, w jakim stopniu ten strażnik jest faktycznie człowiekiem. Podaje jakiś środek działający normalnie na ludzi i chce sprawdzić co się dzieje; strażnik nadal ma ludzkie reakcje, choć osłabione. Połączenie regeneracji i tkanki magicznej.

Tymczasem Maria nie mogła wytrzymać swojej bezradności w obliczu tak przerażającej infekcji. Podskoczyła do apteczki Pauliny, zapakowała kilka bardziej promieniujących magicznych przedmiotów do torby. Wzięła maść uśmierzającą ból (dostała od Pauliny na specjalne okazje), która zmienia jako efekt uboczny kolorystykę skóry i wysmarowała się nią, by zarówno lekko świecić jak i nie wyglądać jak zwykle. Wsadziła sobie soczewki kontaktowe zmieniające oczy na bardziej kocie i przefarbowała włosy (częściowo je ścinając by wyglądać trochę jak straszydło). Zrobiwszy to wszystko, pojechała do Powiewu Świeżości udając viciniusa i poszła do biblioteki Powiewu z nadzieją, że spotka się z Katarzyną Kotek.

Oczywiście, Katarzyny nie było; Maria zatem poprosiła obecnego w bibliotece Maksa o sprowadzenie Romana Gieroja. Gdy Gieroj się pojawił, Maria powiedziała mu, że Paulina ma poważne kłopoty i potrzebny jest Marii dostęp do Kasi Kotek. Roman próbował protestować, ale Maria szybko go usadziła - jeśli Roman jej nie pomoże, ona powie Julii i Powiewowi wszystko o Kasi Kotek. Wściekły, Roman poprosił na dół Kasię i oddalił się święcie przekonany, że Paulina będzie musiała się za to ostro wytłumaczyć.

I w chwili, w której Paulina zaczęła badać strażnika, Maria skontaktowała się z Pauliną i powiedziała jej, że jest koło niej Kasia Kotek i że Maria jest w Powiewie Świeżości.
Paulina jest pomiędzy facepalmem, potężnym skrzyczeniem Marii i jej uściskaniem. I musi ukrywać to przed tymi na miejscu.

Olga podchodzi do strażnika z okrutnym uśmiechem. Chciała go tylko postraszyć. Opowiedziała mu, jak będzie wsadzała igły mu prosto w oczy - i na to Pacjent Zero zareagował. Odrzucił link. Strażnik zaczął się trząść i płakać, wzywać mamę. Wrócił do swoich zmysłów, choć w zainfekowanym ciele. Strażnik zaczął błagać Dzionka, by ten potwierdził, że on jest dobrym człowiekiem. On nic nie wie. Nic.
Dzionek pokręcił głową. Powiedział "Nikodemowi", że jest Skażony a Paulina jest lekarzem. Olga wycofała się poza pole widzenia Nikodema. Wszyscy odetchnęli z ulgą.

"Będę Cię przepraszać potem - na razie wyjdź z tego, co?" - Maria do Pauliny, przyznając się, co zrobiła.

Dobrze. Paulina poprosiła Dzionka o jakiś monitoring ze środka na zewnątrz. Powiedział, że się nie da; to nie było zaplanowane, że ktoś się zabarykaduje w laboratorium. Raczej problemem było to, że coś się wyrwie w laboratorium.
Paulina nie rozumie, czemu Dzionek nie został zainfekowany. Spytała go, kiedy przybył - poprzedniego wieczoru. Czyli było dość czasu. Maria powiedziała po konsultacji z Kasią Kotek, że najpewniej nie mają do czynienia z istotą ROJOWĄ - najpewniej mają do czynienia z czymś, co "jest" każdym bytem. Raniąc jednego, ranią wszystkie. On czuje i jest wszystkimi końcówkami jednocześnie. Powinno go nieźle dezorientować; ludzki mózg ma z tym duże problemy.

Ale z laboratorium jest wgląd w izolatkę. I on jest w środku. Więcej, jest obraz archiwalny - Paulina może wszystko zobaczyć. Paulina cofnęła widok, by zobaczyć co się stało gdy Paulina torturowała Dominika - faktycznie, Pacjent Zero też się zwijał z bólu. A złapał się za serce, gdy Olga wypatroszyła wiewiórkę. 
Zapytana przez Paulinę Kasia (przez Marię) powiedziała, że ten typ istot potrafi zwykle odrzucić "końcówkę" (jak jaszczurka ogon), ale nie będzie w stanie się z końcówką ponownie połączyć do czasu fizycznego kontaktu. Gorzej, że końcówka sama obumrze. Nie od razu.
Ale dla Pauliny to świetna wiadomość - nie ma ryzyka kaskadowej zarazy. Dodatkowo, to tłumaczy czemu cała baza nie została przejęta - vicinius zwyczajnie nie ma takiej mocy i nie ma takiej koncentracji. Czyli DA się to opanować. Choć będzie ciężko.

Zapytany o oddział, który jest zdrowy, Dzionek powiedział, że podejrzewa jeden oddział. Osiem osób, cały czas na zewnątrz, w kombinezonach hazmat (trening i test nowych kombinezonów). Dzionek jest w stanie ich ściągnąć... z centrum dowodzenia. Będzie też w stanie ich ostrzec, ale stamtąd. Zawsze to potencjalne wsparcie, choć nieco mordercze.

Kasia wykazała inicjatywę - powiedziała Paulinie, że jej pomoże. Paulina musi zbadać strażnika; musi poszukać jak wpłynęły cechy viciniusów na tego nieszczęśnika. Ona pomoże. I Kasia mówi dokładnie, co Paulina ma zrobić. Nie jest mikrobiologiem, ale ma dostęp do wiedzy Aurelii... chyba. Podaje polecenia zgodne z wytycznymi, jakie zrobiłaby Aurelia.
Kasia zaczęła mówić słowotokiem. Powiedziała, że viimanos jest kluczem do tego viciniusa; jest jedynym źródłem energii i pacjent zero pożera pozostałe końcówki powoli. Czerpie z nich energię. To jednak oznacza, że pacjent zero jest najbardziej Skażony i jego wzór mógł już się trwale zmienić. Co swoją drogą nie trzyma się kupy - w jaki sposób "tamten mag" zdestabilizował wzór człowieka tak skutecznie? To nie ma sensu. Chyba, że to już nie był zwykły człowiek... ale w takim razie, jak trzyma się TEN Nikodem? Zdaniem Kasi to podstawowa zagadka. To i uderzenie w viimanosa powinno Paulinie zdecydowanie pomóc rozwiązać ten problem.
Po chwili Kasia zamrugała. Nie do końca wie, skąd to wszystko wie i JAK Paulina ma to zbadać. Ale Paulina już ma pomysł...

Paulina nacisnęła Dzionka; czy Nikodem i Pacjent Zero mieli kiedyś do czynienia z magią? Tymoteusz próbował kręcić, ale Olga powiedziała ostro, że to nie czas na sekrety; chciałaby to przeżyć. Dzionek sypnął; Pacjent Zero swego czasu był na różnych akcjach przeciw magom. Wiele razy używano na nim artefakty czyszczące. Gdy inni wymiotowali, on miał to już gdzieś; brał serum uodparniające go na Skażenie. Paulina facepalmowała. Jedyny sposób jak można uodparnić się na Skażenie to wymienić część ciała na tkankę magiczną. Gość był podatny, bo był Skażony, tylko nie miał ukonstytuowanego wzoru.
Lekarz który był u niego nie był tak naprawdę prawdziwym lekarzem; ma podstawowe wykształcenie medyczne, ale jest to "ludzki artefaktor", badacz artefaktów i przez to tak samo bardzo Skażony.
Widać członkowie tej organizacji nie do końca rozumieją jak działa magia...

Paulina nastawiła różne testy na strażniku - z krwi, wymazów itp. Szuka jeszcze innych viciniusów oraz szuka słabego punktu pasożytów. Umiejscowienia. Czegoś, w co można uderzyć. Udało jej się znaleźć kilka interesujących rzeczy (wspierana jest przez Kasię i poświęca trochę czasu):
- Paulina znalazła ślad specjalnego eliksiru Myszeczków; coś, co rozspaja wzór maga, ale odmienione przez viimanosa. Viimanos zmutował w tą stronę... a przedtem było to zaadaptowane specjalnie na potrzeby ludzkie. Ktoś (mag) doprowadził do tego, że eliksir Myszeczków zadziałał na człowieka. Wymagało to uodparniania człowieka oraz bardzo żmudnej pracy alchemicznej na eliksirze. A potem viimanos to włączył do swojego wzoru jako element owego człowieka. I to spowodowało infektywność, choć NISKĄ infektywność; z tego wynika, że Paulina jest całkowicie niewrażliwa na infekcję.
- Paulina nie znalazła ŻADNEGO śladu kolejnego viciniusa. Spojenie właśnie tych trzech viciniusów w połączeniu z eliksirem rozspajającym wzór i w połączeniu z sugestią (zasada Paradygmatu) dały taką a nie inną istotę. Nadal nie wszystko trzyma się kupy, ale to oznacza, że ten vicinius trzyma się tylko przypadkowo. Jest groźny, ale nie TAK groźny. Potwierdziła się hipoteza "jednej istoty rozproszonej".
- W połączeniu z wiedzą o ludzkim mózgu i systemach rozproszonych Paulina określiła, że naraz ten vicinus potrafi kontrolować do 20 istot. Jeśli chce, by te istoty były przydatne, to będzie ~10 istot. W bazie jest koło 50 osób. Gorzej, że gdy vicinius jest w "kimś", ta osoba nie wie o infekcji dopóki nie kontroluje bezpośrednio; a wtedy nic nie pamięta.
- Połączenie tych konkretnych viciniusów wyraźnie wskazuje na działanie celowe. Takie rzeczy i destabilizacja wzoru... po prostu to się przypadkowo nie łączy. Nierealne.

Paulina poczuła potężną eksplozję. Drzwi do izolatki wyleciały na zewnątrz. Kontrolowani przez viciniusa agenci wydostali pacjenta zero na zewnątrz, wraz z lekarzem...
Paulina kazała Tymoteuszowi Dzionkowi włączyć pełną kwarantanę. Zgodził się, choć cholernie niechętnie. Cały budynek został odcięty na 24h. Nie da się przyspieszyć tego terminu. Skorpion został dodatkowo ostrzeżony o sytuacji.
Nagle sytuacja viciniusa się zdecydowanie pogorszyła.

Ten przycisk jest w końcu na DOKŁADNIE takie sytuacje.

### Faza 3: Wojna o bazę Skorpiona

Paulina, Dzionek i Olga są odcięci w zapieczętowanym laboratorium. Baza też jest zapieczętowana. Jednak w bazie grupa kilkudziesięciu osób właśnie się orientuje, że coś jest nie tak, a Pacjent Zero wydostał się ze swojej izolatki. I rozpoczyna się wojna o bazę.
Paulina by pomogła (niezbyt, ale trzeba to napisać), ale niestety (na szczęście) jest zamknięta w laboratorium.

Paulina wpadła na pewien pomysł. Cały czas wszyscy zakładają, że przejęci nie mają pamięci i są po prostu "opętani". Jednak być może osoby przejęte mają dostęp do części swojej pamięci i świadomości. Gdyby udało się uderzyć w tą stronę, to nawet jeśli vicinius faktycznie przekształca agentów, to można będzie wykorzystać tych agentów, homo superior, przeciwko niemu. A przynajmniej uwolnić się od problemu; gdyby udało się doprowadzić do tego że vicinius walczy sam, to już jest dużo lepsza sytuacja. Ale do tego Paulina musi przesłuchać strażnika i pomóc mu sobie poprzypominać. Ma zamiar użyć medykamentów, które są w laboratorium. Olga naturalnie zgłosiła się na ochotniczkę do przesłuchania. Paulina jej nie pozwoliła i wygrała konflikt; Olga się wycofała.

Paulina podeszła do nieszczęsnego strażnika (Szerściuka) i zaczęła go przesłuchiwać. Zdobyć informacje, co się stało, z nadzieją na potwierdzenie lub zaprzeczenie teorii. Maria pomagała Paulinie zrobić wywiad po soullinku; medykamenty pomagały nieszczęśnikowi sobie przypomnieć i nie spanikować a Dzionek był cały czas w polu widzenia (a Olga poza nim). Paulina dowiedziała się, że Szerściuk nie do końca był kontrolowany przez viciniusa; on tam cały czas był, ale tak jakby uśpiony. Ból oraz strach sprawiły, że vicinius "uciekł" z Szerściuka. Teraz jest już wolny; Paulina wyprowadziła z tego (z niewielką pomocą Kasi), że silny szok może spowodować rozerwanie. Tak samo strach. Tak samo uderzenie silnym bólem w kilka koncówek. Najważniejsze, że ci ludzie, jakkolwiek sterowani odgórnie, "są w środku". Po prostu uśpieni. Inną opcją mogłoby być obudzenie ich. Bo Kasia wyprowadziła z tego coś jeszcze - vicinius jest nimi wszystkimi jednocześnie; to znaczy, że jego ogólne umiejętności są ograniczone.

Paulinę coś uderzyło - skontaktowała się przez Marię z Kasią. Jak działa viimanos? Konwertuje ciało w energię magiczną na zasadzie anihilacji; Aquam -> Vim. Paulina rozbłysła radością - może rozsypać działanie viimanosa. Może przekształcić ich w nieumarłych, powinno to rozsypać konwersję. Paulina aż poczuła mentalną radość Marii. Kasia się zastanowiła, tak, powinno się to udać. Może przetestować na strażniku; zdaniem Kasi pojawia się ryzyko wycofania; rozsypie się cała struktura viciniusa i zwyczajnie ofiara umrze. Maria szybko rzuciła, że Paulina powinna być w stanie stopniowo transformować lub utrzymać przy życiu.
Kasia powiedziała, że to zadziała. I zaproponowała Paulinie, żeby ta przekształciła siebie, Dzionka i Olgę w nieumarłych. Dzięki temu będą całkowicie odporni na infekcję; ten typ viciniusa nie może się rozprzestrzeniać po nieumarłych.
Czyli mają plan!

Tymczasem w bazie rozgorzała walka pomiędzy siłami viciniusa i lokalnymi agentami Skorpiona. Nie wszyscy z tych kilkudziesięciu to siły wojskowe. Wojskowi spróbowali ewakuować co się da i ufortyfikować się w jakiejś sensownej pozycji. Nadal przepychanka; nie udało się skonsolidować sił żadnej ze stron. Na innym froncie siły dowodzone przez lekarza (doktor Kozior) zaatakowały skład artefaktów Skorpiona, ale rozpaczliwa obrona dowodzona przez agenta Czekana odparła Koziora używając miotaczy ognia. Na razie jeszcze nikt nie zginął, są jednak ranni a agenci viciniusa w odróżnieniu od ludzi się regenerują...

Skorpion dzielnie walczy. Zaskakujące jak dobrze się trzyma.

Olga słysząc strzały szybko zażądała od Dzionka, by ten powiedział jej natychmiast co jest w tej bazie. Dzionek zaczął wymieniać: magazyny, koszary, laboratorium, centralne dowodzenie, komunikacja, skład artefaktów, medbay, containment units...
...Olga mu przerwała. Skład artefaktów i containment. Co tam jest. Dzionek powiedział, że w containment unit niczego nie ma; akurat nie mają ani efemeryd, ani viciniusów ani nic więcej (poza tym co uciekło). Za to mają kilkanaście artefaktów z czego większości nie znają i nie rozumieją; chwilowo są w lapisie. Jedynym co się na tym zna (w ich pojęciu) jest dr Kozior; on badał i analizował artefakty. Olga od słowa do słowa wyprowadziła, że Kozior to ten człowiek który był lekarzem pod kontrolą viciniusa... świetnie...

Olga poprosiła Paulinę o zbudowanie jakiejś intelligence net. Potrzebne im sensory. Paulina się zgodziła, ale najpierw - zabezpieczenie przez zakażeniem. Przesunięcie fazowe wszystkich tu obecnych w nieumarłość. Olga się zgodziła, Dzionek nie wie o co chodzi ale się zgodził (Olga bywa sugestywna). Kasia powiedziała, że już szuka czegoś co można wykorzystać jako sensory. Paulina dorzuciła jej "minion master". Kasia powiedziała, że spróbuje wykorzystać kameleony jako wasze sensory; akurat ma już pewien pomysł.

Paulina zrobiła przesunięcie. Skupiła się nie tylko na przesunięciu i uodpornieniu, ale tak samo na tym, żeby mogli infekować dotykiem; innymi słowy, przesunięcie energii jest zaraźliwe. Skorzystała z techniki viimanosa i wiedzy jak działają wirusy, połączyła to z katalizą oraz odpaliła nie tak proste zaklęcie. Udało jej się, choć jest zmęczona i zajęło to chwilę. Paulina z zadowoleniem obserwowała, jak Olga szarzeje, jej oczy się rozjarzają i kapłanka Iliusitiusa nabiera bardzo upiornego wyglądu. To samo z Dzionkiem i samą Pauliną. Dzionek podskoczył a Olga go uspokoiła. Tak ma być. Olga, Dzionek i Paulina są wampirami energetycznymi; dotykiem przekazują ten sam stan. Czyli są chodzącymi szczepionkami. Choć nieco upiornymi.

Wojna o dominację bazy nadal się nie przesunęła. Siłom Skorpiona nie udało się jeszcze sformować frontu, ale vicinius nie dał rady jeszcze wbić się w żadne ważne miejsce. Paulina poczuła pojawiające się błyski energii magicznej; to oznacza, że Kozior dał radę odbić skład artefaktów. Czekan się wycofał chroniąc cywilów i idąc w kierunku na koszary.

Paulina stanęła przed kolejnym problemem - laboratorium jest zasealowane i jedyną opcją by je otworzyć to z centrum dowodzenia. Ale jak to zrobić? Współpracując z Kasią rozwiązała ten problem; ma tu kameleony, które przez viimanosa stały się bytem rozproszonym. Innymi słowy, wszystkie kameleony są sympatycznie powiązane, są tą samą istotą. Czyli Paulina może wpłynąć na te kameleony, by móc znaleźć takiego osobnika, który nie jest silnie Skażony. Jeśli tak się stanie, może próbować sympatią rzucać nań zaklęcie; techniki magii krwi.
Akurat lekarz magiczny ma trochę wiedzy na ten temat; niechętnie tego użyje, ale trochę musi umieć.

Cała akcja jest wręcz nieprawdopodobnie trudna (18!). To wymusza escalation mode.

Faza 1: 
Paulina chce otworzyć furtkę na użycie sympatii na kameleony. Druga strona - ma to zająć więcej czasu i lekko Skazić Paulinę.
ATK (kataliza vs poziom ekspercki # Paulina idzie dokładnie i poświęca trochę czasu -> 15v12 -> 17)
DEF (radzenie sobie ze Skażeniem w warunkach nie używania swojego ciała; zawodowy vs BdWl_Direct_R -> 10v9 -> 5)
WYNIK: Paulina otworzyła kontakt z kameleonami i ma do nich dostęp na poziomie magicznym. Niestety, uległa pewnemu Skażeniu mimo ostrożności.

Paulina poczuła jak artefakty zaczynają wchodzić w interakcje ze sobą. Ten Kozior najprawdodobniej zrobił coś głupiego i artefakty zaczynają wchodzić w rezonans. Ale nie wszystkie. I Paulina czuje, jak rezonujące artefakty się przesuwają. On gdzieś niesie te, które zaczynają rezonować. Tymczasem, jakkolwiek walka o bazę nadal rozgrywa się bardzo pozycyjnie (żadna ze stron nie uzyskuje przewagi), obrońcy dali radę się ufortyfikować w centrum dowodzenia. Mała grupka Czekana nie dała rady się tam przedostać; zostali odcięci i zmuszeni do schowania się do zbrojowni.

Faza 2:
Kasia próbuje zrozumieć co może mieć w planach Kozior.
Paulina próbuje doprowadzić do tego, by ktoś ich wypuścił, niezależnie od strony.
Druga strona: Paulina w szoku, we wstrząsie (masakra i zniszczenie a ona nie może pomóc i JEST tam przejmując kameleony)
Kasia: sukces
ATK: sukces
DEF (9 vs (Paulina społecznik, ale lekarz) i jest tam, WlHrt_d_r 8): sukces

Kasia powiedziała Paulinie jak najpewniej wygląda sytuacja; zgodnie z dostarczoną przez Dzionka mapką Paulina zauważyła, że Kozior idzie w kierunku koszar z artefaktami. Kasia połączyła fakty i powiedziała, że może chodzić o wywołanie Skażenia; ludzie źle reagują na Skażenie, ale viciniusy nie. Czyli najpewniej Kozior chce wpaść i zabić wszystkich.
Szczęśliwie, Skażenie ułatwia Paulinie sytuację; więcej kameleonów na słabych jednostkach.

Kozior wysłał jednego agenta przodem, trzymającego artefakty. Obrońcy go rozstrzelali i wtedy jeden z artefaktów został trafiony. Spowodowało to potężny wybuch Skażenia. Vicinusy wdarły się do chronionego centrum dowodzenia, przebijając się i zabijając wszystko co się rusza (lub nie; Skażenie). Paulina w ogniu tego wszystkiego zdołała przejąć kontrolę nad nisko Skażonym agentem i otworzyć laboratorium; jako doświadczony lekarz i mag wytrzymała szok.

Vicinius ma odwróconą uwagę, z czego skorzystał Czekan. Wziął wyrzutnię rakiet i pobiegł ze swoimi ludźmi w kierunku ufortyfikowanego centrum dowodzenia tylko po to, by zobaczyć jak centrum upada. Widząc masakrę, wycelował i strzelił.

Paulina poczuła eksplozję wyrzutni rakiet. Ktoś walnął rakietnicą w tunelu.

Paulina przekroczyła pewien poziom. Tam jest energia magiczna. Skażona, ale zawsze. Paulina chce zniszczyć to wszystko; zdecydowała się użyć magii by wszystkich przestraszyć. By wymusić wycofanie viciniusa z kameleonów. Podpięła się do Skażonej energii (Skażając się jeszcze bardziej; wzięła to na siebie i jest już na granicy Skażenia); Paulina jest już chora od Skażenia, ale nie jest w stanie zrobić tego inaczej by chronić wszystkich. Zaczęła akumulować energię...

...siły viciniusa odwróciły się w kierunku Czekana z rakietnicą. Czekan nie ma się gdzie wycofać. Wydał krótki rozkaz swoim 2 agentom chronić cywilów i zabarykadować się w kiblu a sam spojrzał na viciniusa i niedbale wystrzelił pocisk zapalający. Próbował uniknąć ognia, ale oczywiście nie było to możliwe; przeżył, ale bardzo ciężko ranny.

...Paulina poczuła szok magiczny i energetyczny; struktura pierwotnego viciniusa została ciężko uszkodzona i viimanos zaczął anihilację energii z ofiar. Paulina uderzyła dużo, dużo mocniej. Uderzyła strachem, połączonym z obrazami najgorszych rzeczy magicznego lekarza, bólem, sygnałami - wszystko po sieci kameleonów. Puściła Skażoną energię celując prosto w viimanosa.
Vicinius dostał tak silną dawkę zwrotną od wszystkich, że nie był w stanie skompensować. Szok go po prostu zabił.

Vicinius umarł na serce.
Paulina straciła przytomność.

### Faza 4: Tymczasowy szpital (epilog)

Damazy Czekan został znaleziony bardzo ciężko ranny, umierający. Olga kazała jednemu ze Skażonych wstrzyknąć mu tą samą formułę; Paulina będzie w stanie to odwrócić a szkoda człowieka. Dokładnie tą samą doktrynę kazała powtarzać ze wszystkimi innymi ciężko rannymi.

Po zniszczeniu viciniusa (pacjenta zero), tak jak Paulina się domyślała, agenci odzyskali zmysły. Ludzie otwarcie płakali widząc to, co się stało. Olga tylko wzruszyła ramionami. Przejęła tymczasowo kontrolę nad bazą, bo nikt nie pomyślał żeby to zrobić.

Gdy Paulina się obudziła, miała wszystkich przygotowanych w kolejności i Olgę, która spokojnie wyjaśniła sytuację.
Zanim lockdown budynku się skończył, Paulina odwróciła zaklęcie na Dzionku i kilku innych, by Skorpion nie zastrzelił ich na miejscu.

Po kilku dniach bardzo chora Paulina skończyła leczenie.
Olga zabrała im wszystkie artefakty. Skorpion protestował, ale Olga powiedziała, że zostały zniszczone w eksplozji. Dr Kozior nie protestował. Potwierdził. On wszystko potwierdzi, takie poczucie winy.

Maria straciła przytomność. Przedawkowała maść magiczną. Pomogli jej lekarze Powiewu; jest dalej w PŚ na obserwacji.

# Streszczenie

Olga wciągnęła Paulinę w ratowanie agentów Skorpiona Skażonych przez magię (zmienionych w viciniusy). Okrucieństwem Olgi i magią Pauliny uratowali bazę; Kasia Kotek dostarczyła przez Marię kluczowych informacji Paulinie i viimanos został zniszczony. Olga wkręciła się w Skorpiona i zwinęła artefakty. Na potęgę Iliusitiusa.

# Zasługi

* mag: Paulina Tarczyńska, lekarz * magiczny, która uratowała bazę Skorpiona przed straszliwą hybrydą * viciniusów.
* czł: Maria Newa, która bardzo dużo zaryzykowała by umożliwić Paulinie działanie; zaszantażowała Romana i dostała dostęp do Kasi Kotek.
* czł: Olga Miodownik, chroniona (częściowo) przed infekcją przez Iliusitiusa, której sadystyczne i mordercze instynkty okazują się być kluczem do przetrwania wszystkich.
* czł: Tymoteusz Dzionek, przerażony niebojowy agent Skorpiona, który nie spodziewał się, że epidemia jest Skażeniem hybrydy * viciniusów.
* vic: Katarzyna Kotek, stworzona bibliotekarka która jednak ma przebłyski EIS oraz Aurelii; nie jest jednak żadną z nich.
* mag: Roman Gieroj, zaszantażowany przez Marię Newę by pozwolił Katarzynie Kotek i Marii współpracować przeciwko * magicznej zarazie.
* czł: Dominik Szerściuk, nieszczęsny strażnik Skorpiona, który został zainfekowany przez potwora i był torturowany przez Olgę.
* czł: Damazy Czekan, odważny dowódca oddziału Szóstego Skorpiona który zdecydował się chronić personel cywilny niezależnie od kosztów. Ma rakietnicę z pociskami zapalającymi i nie zawaha się jej użyć. O dziwo, przeżył.
* vic: Marian Kozior, lekarz i badacz artefaktów, który po wydarzeniach w tej bazie stał się trwale * viciniusem.
* vic: Aleksander Szwiok, "pacjent zero" i hybryda kilku * viciniusów który prawie kompletnie zainfekował i zniszczył bazę Skorpiona.

# Lokalizacje:

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Nowy Kopalin
                        1. Powiew Świeżości
        1. Małopolska
            1. Powiat Okólny
                1. Szczawnia Mokra
                    1. Baza Skorpiona DK-6
                        1. Magazyn
                        1. Laboratorium
                        1. Centrala dowodzenia
                        1. Skład artefaktów
                        1. Izolatka
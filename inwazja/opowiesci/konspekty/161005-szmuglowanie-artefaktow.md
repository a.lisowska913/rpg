---
layout: inwazja-konspekt
title:  "Szmuglowanie artefaktów "
campaign: powrot-karradraela
gm: żółw
players: kić, dust
categories: inwazja, konspekt
---

# {{ page.title }}

# Konspekt pisany

## Kontynuacja

### Kampanijna

* [150408 - Rykoszet zimnej wojny (HB, DK, AB)](150408-rykoszet-zimnej-wojny.html)

### Chronologiczna

* [150406 - Aurelia za aptoforma (PT)](150406-aurelia-za-aptoforma.html)

## Kontekst ogólny sytuacji

## Punkt zerowy:

Marian Łajdak. Uroczy, ale łajdak. Pierwszy szmugler artefaktów, by KADEM było stać na różne eksperymenty...
Sęk w tym, że w okolicznościach zimnej wojny KADEM - Świeca, magowie Świecy zdecydowali się zatrzymać przemyt artefaktów KADEMu na wschód, liniami Zajcewów.
A Iza Łaniewska zlokalizowała kreta w Świecy chcącego zrobić coś podłego z Wyzwalaczem i zastawiła pułapkę z Kornelem i Dorotą...

Ż: Jak nazywa się Zajcew, główny kontakt przemytu Mariana Łajdaka?
D: Jewgenij Zajcew, musi trzymać się procesu. Wszystko musi działać jak w zegarku.
Ż: Jaki interesujący artefakt poza Kaczkoryzatorem Zajcew chce kupić?
K: Ognista butelka coca-coli. Nikomu jeszcze nie udało się tego wypić.
Ż: Inny?
D: Zabutelkowany pająk, którego ukąszenie odbiera tymczasowo magię.
Ż: Kto ze Świecy chce kupić Wyzwalacz Pryzmaturgiczny?
K: Jeden z terminusów powiązanych pośrednio ze Szlachtą, Julian Pszczelak
Ż: Gdzie dojdzie do przekazania artefaktów magowi Świecy?
D: W Kawiarence Złotko, na obrzeżach Kopalina
Ż: Gdzie dojdzie do przekazania artefaktów Zajcewowi?
K: Szary Pustostan w dzielnicy Mausów
Ż: Dlaczego misja musi się zamknąć w dwóch dniach?
D: Pająk zdechnie (Zajcew), a jak chodzi o maga Świecy - Wyzwalacz jest mu potrzebny na akcję.
Ż: Dwa zdania o terminusie Świecy
K: Jan Kotlin, kompetentny ale zmęczony życiem; gdyby nie obecna sytuacja, byłby już na emeryturze
Ż: Dwa zdania o terminusie Świecy
D: Kaspian Miauczek, biomanta i katalista. 

## Misja właściwa:

### Dzień 1:

Lekko, a nawet bardzo spanikowany Marian poprosił o spotkanie Magnusa i Anielę.
Podobno Świeca pilnuje, różne rzeczy. Co gorsza, Iza Łaniewska jest zainteresowana znalezieniem czegoś na KADEM... chce mścić się za Silurię Diakon.

"Matuzalema już raz sprzedaliśmy, ale wrócił. Straciliśmy dobrego klienta..." - Marian Łajdak

Magnus spytał Łajdaka, czy ten ma może artefakt o który ten prosił. Tak! Tak się składa, że ma. Maść katalityczna do przechowywania zaklęć mentalnych. Właśnie Jewgenij przywiózł, udało się mu zdobyć; ale się spieszy. Śledzik mu się rozchorował, spoił go wódką... i śledzik wyzdrowiał. Ale cała kolonia śledzi syberyjskich się spiła...
Tak, nikt nie wątpi, czemu się spieszy...

Marian się martwi, że terminuska po prostu węszy. Magnus zaproponował, by ustawić sytuację tak, by odwrócić jej uwagę gdzieś indziej. Marian westchnął - KADEM gra uczciwie, 1 na 1. Świeca idzie zergami. Magnus zaproponował dywersję - legalne artefakty. Dobry plan...
Też coś nielegalnego, ale uczciwie nielegalnego. Nic poważnego.

Aniela poprosiła o parę godzin. Sama przeprowadziła badania skierowane w stronę Izy. Znalazła - obroża posłuszeństwa. Coś, co normalnie kojarzy się z Silurią. Ale z elementami Spustoszenia; tak, by wyszło, że to pochodzi ze Świecy - by było podejrzenie, że pochodzi ze Świecy.
W końcu Świeca z tego słynie. Ma wyjść na to, że to pochodzi ze Świecy i jest podłożone. Oczywiście, udało się to zrobić.

"Nie zadziera się z KADEMem" - Aniela

Czyli jest artefakt, którym można wrobić maga Świecy... to plus artefakty "legalne".

Plan realizowany jest w bardzo prosty sposób. Marian zabiera paczkę-podpuchę (z lekko pękniętym ekranem ochronnym). Marian i Magnus jadą pociągiem, Aniela jest cover opsem.
W tym czasie w pociągu nadają "paczki właściwe" Aniela i Magnus. 
W trakcie podróży, gdy pociąg jest na stacji Kopalin. Opóźnienie pociągu, stoi na stacji.

Tamara, Judyta i Jan weszli do przedziału 
"Co to ma być! Jak pani śmie! Nietykalność maga!" - i Tamara znowu poddusiła Magnusa.

Magnus gadał, lżył i się wyśmiewał z Tamary. Naprawdę. Terminuska nie wytrzymała, użyła eksperymentalnej funkcji swojego magitecha i go... przesunęła. Jakoś. Gdzieś. Sama nie wie co do końca zrobiła... po minach pozostałych magów - oni też nie wiedzą i wcale nie jest im z tym dobrze.
Tymczasem Aniela, korzystając z zamieszania zrobionego przez Magnusa zostawiła "przypadkiem" notes na widoku polując na Izę. Izy nie ma - nie pojawiła się, ale pojawił się inny mag, terminus pilnujący Jewgenija. Aniela wpadła na niego i wsadziła Jewgenijowi kwity do kieszeni; by dokończyć przekazanie.
Jewgenij potraktował to rubasznie. Terminus się nie spodziewał i nie zorientował. Dostał swój notes...

Tymczasem Tamara zdążyła przywrócić Magnusa zanim coś złego się stało (magitech musiał się naładować PLUS Tamara nie miała pojęcia gdzie wylądował). Magnus wcale nie stracił niewyparzonej gęby, jednak Judyta powstrzymała Tamarę przed zrobieniem z Magnusa przykładu. Tamara westchnęła - Ozydiusz by tego sobie życzył, ale ona nie chce eskalować wojny między KADEMem i Świecą... zapieczętowała paczkę z Marianem (by był ślad anti-tampering) i zabrała to do laboratorium Świecy.

Po drodze, magowie Świecy jeszcze przeszukali pociąg. Nie udało im się znaleźć niczego, ale znaleźli po drodze pośrednie dowody na to, że to nie Iza mogła za tym stać. Innymi słowy, zneutralizowali pułapkę KADEMu, ale nie uniemożliwili przemytu.

Wieczór. KADEM. Marian, Aniela, Magnus.
Omówili w skrócie brak Izy - nie było jej, to jest... dziwne. Dodatkowo, Marian jest przerażony tym co Tamara zrobiła i jak daleko się posunęła.
Przynajmniej nic nie wskazuje na to, że była tam Iza. A Jewgenij dostał swojego pająka i butelkę...

Marian wyjaśnił o co chodzi:
- Sariath Trawens... ogólnie KADEM ma wysoką wydajność. Produkujemy bardzo dużo. To kosztuje surowców i generuje outputy.
- KADEM musi mieć kasę na te rzeczy
- Świecy to normalnie nie przeszkadzało, do momentu tej zimnej wojny
- Świeca chce ten Wyzwalacz Pryzmaturgiczny. Świeca to autoryzowała. Ale nie może tego zrobić "jawnie"
- Problem z Izą jest taki, że to pistolet...

### Dzień 2:

Następnego dnia - wielka wyprzedaż garażowa artefaktów. Kropiaktorium może się schować. Różne artefakty, prawie po kosztach... nic bardzo groźnego, ale wszystko co da się sprzedać i co najmniej nie stracić.
A że wici się rozejdą i będzie dużo gości...

Magnus i Aniela wrzucili do pudła księgę z KADEMu. Historyczne (nieaktualne) zabezpieczenia magicznych budynków i zamków. Sęk w tym, że nie wszyscy uaktualniają te rzeczy na bieżąco ;-). Grunt, że nie jest to super istotny Wyzwalacz Pryzmaturgiczny.

Gdy tien Pszczelak odebrał swoją księgę i wracał do Świecy, zgarnęła go Iza Łaniewska. Faktycznie przyskrzyniła go za coś, czego nie powinien posiadać. Mag poszedł mieć problemy gdzieś indziej, a Wyzwalacz został z KADEMem.

Nowe polecenie. Wyzwalacz trzeba schować POZA KADEMem. Wyzwalacz bardzo źle reaguje na pryzmat KADEMu.
Schowali Wyzwalacz na Pustostanie Mausów. Jest dobrze ekranowany. Jak będzie trzeba i nadejdzie czas, to wtedy się go wyciągnie...

# Progresja

## Frakcji

* KADEM: ma sieć przemytniczą z Jewgenijem Zajcewem, na wschód
* KADEM: na wysokim poziomie (szefostwo) ma sieć quasi-legalnego przekazywania artefaktów ze Świecą w Kopalinie... poza Ozydiuszem.
* Srebrna Świeca: w Kopalinie ma sieć quasi-legalnego przekazywania artefaktów z KADEMem. Poza oficjalnymi kanałami i poza Lordem Terminusem.

# Streszczenie

Marian Łajdak szmugluje artefakty na wschód, przez Jewgenija Zajcewa, by KADEM nie zbankrutował. Jednocześnie szmugluje niektóre artefakty do Świecy - przy współpracy góry Świecy i KADEMu. Jednak z uwagi na zimną wojnę operacjami zainteresowała się Tamara. Udało się wyprowadzić ją w pole - tym razem. Przekazanie Wyzwalacza Pryzmaturgicznego Świecy się nie udało - Iza Łaniewska usunęła maga Świecy który miał to przejąć jako skorumpowanego. Wyzwalacz został schowany w Szarym Pustostanie w Dzielnicy Mausów. 

# Zasługi

Gracze:
* mag: Magnus Spellslinger, irlandzki KADEMowiec, który pyskował Tamarze do skutku. Nawet wstrząśnięty, pyskował. Arystokrata od siedmiu boleści i supremacjonista KADEMu.
* mag: Aniela Lipka, KADEMka - łasica, która podłożyła Zajcewowi kwit uprawniający do odebrania szmuglowanej paczki. Wymyśliła kiermasz i przechytrzyła Świecę - a nawet Izę.

KADEM:
* mag: Marian Łajdak, który próbuje szmuglować rzeczy, by KADEM nie zbankrutował. Poluje na niego Świeca, bo jest etap zimnej wojny.
* mag: Aleksandra Trawens, sariath; dowodzi tematami szmuglerskimi oraz politycznymi. Zidentyfikowała, że Wyzwalacz Pryzmaturgiczny jest uszkodzony na KADEMie i trzeba się go pozbyć.

Szmuglerzy:
* mag: Jewgenij Zajcew, szmugluje artefakty KADEMu na wschód. Musi trzymać się procesu. Wszystko musi działać jak w zegarku. Rubaszny i otwarty.
* mag: Julian Pszczelak, terminus nie-liniowy Świecy powiązany lekko ze Szlachtą. Wysłany by zgarnął Wyzwalacz; złapany przez Izę jako skorumpowany i ma straszne problemy.

Grupa radykalnego oczyszczenia Świecy:
* mag: Infensa Diakon, (jeszcze Iza Łaniewska), która zastawiła pułapkę na "skorumpowanego * maga" Świecy i go aresztowała - wbrew woli przełożonym i ku ich utrapieniu.

Skrzydło terminusów blokujące szmugiel KADEMu:
* mag: Tamara Muszkiet, działała zgodnie z procedurami, choć dała się sprowokować * magnusowi. Świetnie zaprojektowała akcję i na linii * magów i ludzi. Nie znalazła nic na KADEM.
* mag: Kaspian Miauczek, biomanta i katalista; terminus pod rozkazami Tamary. Dał się zmylić Anieli Lipce i poszedł za notesem.
* mag: Jan Kotlin, kompetentny ale zmęczony życiem terminus pod rozkazami Tamary; gdyby nie obecna sytuacja, byłby już na emeryturze.

# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Centrum
                        1. KADEM Primus, gdzie zrobiono wyprzedaż artefaktów jako dywersja by pozbyć się Wyzwalacza (i jakiś zarobek lub zwrot kosztów)
                        1. Dworzec Kolejowy, gdzie Świeca zatrzymała pociąg i gdzie doszło do przechwycenia fałszywych artefaktów
                    1. Dzielnica Mausów, mniej zaludniona niż inne dzielnice KADEMu; uważana za mniej prestiżową i wymierającą
                        1. Szary Pustostan, gdzie ukryto Wyzwalacz Pryzmaturgiczny i go ekranowano
               
# Skrypt

brak

# Czas

* Opóźnienie: 2 dni
* Dni: 2
---
layout: inwazja-konspekt
title:  "Magowie Esuriit w domu"
campaign: powrot-karradraela
gm: żółw
players: kić, dust
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [161026 - Zagłodzona ekspedycja Świecy (HB, SD)](161026-zaglodzona-ekspedycja-swiecy.html)

### Chronologiczna

* [161026 - Zagłodzona ekspedycja Świecy (HB, SD)](161026-zaglodzona-ekspedycja-swiecy.html)

## Kontekst ogólny sytuacji
## Punkt zerowy:

## Misja właściwa:

Dzień 1:

Ranek...? zastał ich niewyspanych. Poszli wszyscy do Opla się naradzić. Andżelika robi diagnostykę. Opel się nie ładuje...
Siluria wysunęła teorię, że tu jest magia krwi. Anna Myszeczka potwierdziła, potrafi ją wyczuć. Andżelika się załamała lekko, Opel nie będzie działał z Magią Krwi...
Anna powiedziała, że nadal jest puryfikatorką. Nadal jest w stanie poczyścić jakąś energię.

Hektor zauważył, że mają czerwie. Może czerwie pomogą. Jakoś. Anna powiedziała, że to dobry pomysł - jest w stanie z pomocą magów z tej placówki podczyścić czerwiami energię i zacząć regenerować tego cholernego Opla. Można im zaufać? Niezbyt, ale co im zostaje.

Cały zespół (nie rozdzielają się) poszedł do Jakuba Pestki. Próbują wymyśleć, jak się stąd wydostać. On powiedział, że im pomoże. Powiedział, że:
- ta Faza robi rzeczy z pamięcią i percepcją. Nie pamięta wiele...
- nie pamięta imion swoich dzieci
- nie wie KIEDY to się stało. Po prostu pada portal i znika hipernet.
- jest archiwum, ale to archiwum to musi popytać Mariannę

Dopytany dokładniej, Jakub powiedział, że czerwie to robota Zofii Weiner i Konrada Myszeczki. W ogóle cała ekspedycja była pomysłem Zofii... Kazimierz (brat Marianny) to wszystko zorganizował. A teraz pożarło go "lokalne Spustoszenie". Też poszło po kanale magicznym; Marianna odpaliła system defensywny i... czterech magów zostało poza bazą.

Bazyli i on, Jakub, konwertują lokalne istoty i anomalie w zasilanie dla syberionu. Dokładnie jak to Bazyli robi, on nie wie.

Marianna Sowińska powiedziała, że potrzebuje kogoś kto pójdzie i zdobędzie elementy - faunę, florę, anomalie... rzeczy, którymi można zasilić syberion. Hektor się postawił - nie pozwoli na to. Marianna i Hektor w ostrym spięciu. Hektor mówi, że mają szansę powrotu do domu. Marianna, że jej magowie są słabi i wyczerpani. 

"Przecież MACIE magów bojowych" - Marianna
"Ostatnich dwóch magów bojowych" - Siluria
"Ja mam pięciu..." - Marianna

Hektor się postawił. Marianna wysłała trzech swoich magów bojowych z nadzieją, by oni dali radę zgromadzić zapasy... 
Marianna powiedziała, że to załamujące, że tylu magów nie jest skłonnych w żadnym stopniu jej pomóc. 

"Żeby coś zrobić musimy działać prędko. Inaczej będziemy wyglądać tak jak wy..." - Siluria
"Jeżeli nas wyciągniecie, to te wszystkie śmierci nie były na marne" - Marianna

By poprawić relacje, Siluria zdecydowała się powiedzieć Mariannie jaki ma plan. Ściągnąć zamek As'caen. Ale kluczem jest to, by ludzie nie wiedzieli i by pojawiła się nadzieja. Potrzebna im duża ilość energii... udało się Mariannę przekonać, by nikomu nie mówiła i by była nadzieja.
Marianna dała też Silurii uprawnienia do dostępu do archiwów. Powiedziała też, że młody Fiodor wpadł w komę podczas samego ataku.

Łukija Zajcew, bardka. Czuwa nad swoim dzieckiem. Spojrzała nieprzyjaźnie na Hektora - przez to, że nie chciał iść, Dosifiej musiał iść polować na anomalie. Hektor który wie, że Dosifiej jest magiem Esuriit... uśmiechnął się smutno. Siluria powiedziała Łukiji, że jest zainteresowana sprowadzeniem zamku As'caen. 
Łukija stworzy historię - jeśli Siluria zaakceptuje to, że w historii jej syn (Fiodor) jest kluczem do przejścia i opuszczenia fazy Esuriit.

Paweł Maus, lokalny lekarz, poprosił o pomoc w konsultacjach Annę Myszeczkę (puryfikatorkę). Andżelika podsłuchiwała i dołączyła do rozmowy...

Łukija po ciężkiej dyskusji się zgodziła. Nie doda tego obostrzenia. Niech i tak będzie... nie lubi i nie ufa Silurii (i innym), ale rozumie jaki jest wynik i koszt.

Andżelika, gdy już dotarli do swojego Blockhausu, powiedziała, że na Fiodorze zakrzywiona jest płaszczyzna pryzmatu. To on odpycha Fazę Esuriit. On "nie śpi", to się tylko wydaje. Wiele z rzeczy, które mają miejsce wynika z tego, że młody sprawia, że stają się rzeczywistością. Jest to coś co może zadziałać tylko na Fazie Daemonica, ale chłopiec jest barierą między Esuriit a Daemonica. To nie może być dobre dla tego młodego umysłu...

Dzień 2:

Hektor stoi na warcie z Anną...
I usłyszał echo dyskusji ze wczoraj. Głos Andżeliki, "To nie może być dobre dla tego młodego umysłu". Gdy już wszystkich pobudzili i nikt nie znalazł speculoida, usłyszał - tylko Hektor - "ostatnich dwóch magów bojowych... ja mam pięciu".
Zdaniem Mikada ta faza chce ich po prostu zamęczyć...

Łukija Zajcew próbuje budować pryzmat składając z Silurią i magami KADEMu opowieści o zamku As'caen, pilnując małego Fiodora Maiusa.
W tym czasie Paweł Maus poprosił Hektora i Annę na stronę. 

Wyjaśnił im, że Mausowie naturalnie czują brak większej grupy. Innych Mausów. I w pewnym momencie on przestał ten brak czuć. Anna od razu wycelowała broń mu w głowę. Hektor nie zrozumiał. Paweł Maus wyjaśnił uprzejmie, że Maus nie jest w stanie tego odciąć z uwagi na wpływ Karradraela. To, że on nie czuje tego uczucia oznacza, że nie jest już Mausem. Stał się Istotą Esuriit. Ale coś tu jest nie tak - normalnie istoty Esuriit takie jak to w co się powinien był zmienić są "głodne". Wysysają energię z magów. I ludzi. On nie ma takich ciągotek. Czyli coś sprawia, że - mimo, że jest istotą Esuriit - nie czuje tego, nie zachowuje się tak i nie działa to na niego. Coś o mocy zbliżonej do wpływu Karradraela, ale na tej Fazie.

Paweł powiedział że to znaczy, że on nie wróci. Poprosi o coup de grace przed tym jak oni zmienią Fazę. On i tak jest już martwy. Powiedział też jakie były działania w tej placówce. Speculoid został złapany i Zofia Weiner próbowała nawiązać kontakt i potencjalną współpracę z tym speculoidem. Niestety, gdy umarły portale, Speculoid wyrwał się na wolność i dopiero odepchnięcie doprowadziło do tego, że cała placówka nie została zmasakrowana. 

To oznacza, z perspektywy Hektora, coś bardzo niepokojącego. Część magów tutaj MOŻE być istotami Esuriit i mogą o tym zwyczajnie nie wiedzieć. Przypadkowe złamanie tego, co to powoduje może zniszczyć ich wszystkich. Dodatkowo, zwiększa to presję i zmniejsza licznik czasowy. A jeżeli były robione prace nad Speculoidem... to on może zachowywać się nie do końca normalnie.

Paweł Maus powiedział, że jeśli nie będzie innego wyjścia, dostarczy im źródło energii... jako istota Esuriit, jest niewrażliwy na Pryzmat Krwi.

W skrócie: NO ŁADNIE.

# Progresja


# Streszczenie

Marianna Sowińska żąda by Zespół współuczestniczył w niebezpiecznym zadaniu zdobywania energii; Siluria i Hektor powiedzieli, że oni potrzebują czasu i mocy by wydostać wszystkich z Esuriit. Starcie wygrane przez Zespół. Siluria ma plan ściągnięcia zamku As'caen - potrzebny jest Pryzmat i Moc. Pryzmat zapewnia bardka - Łukija - choć uzależnia to od wyciągnięcia z tej fazy jej syna. Paweł Maus (lekarz) powiedział Hektorowi, że podejrzewa, że część z nich (Ekspedycji) stała się Istotami Esuriit, choć nie są zgodni z normami. Też: Ekspedycja badała konkretnego Speculoida, który wyrwał się na wolność...

# Zasługi

* mag: Hektor Blakenbauer, wydobył wiedze od Mausa Esuriit o ofiarach z ludzi. Też: ochronił swoich przed Marianną. Słyszy głosy Speculoida.
* mag: Siluria Diakon, dąży do sprowadzenia zamku As'caen i łagodzi napięcia między Drużyną a Ekspedycją. Balsam po twardym Hektorze.
* mag: Andżelika Leszczyńska, której wiedza o Fazach i Pryzmacie (i inne nerdostwo) pokazało kilka niepokojących implikacji.
* mag: Anna Myszeczka, która nie oddala się od Hektora. Jest jak jego wierny cień - chronić i zapewnić bezpieczeństwo. Konsultuje Mausa jako puryfikatorka.
* mag: Marianna Sowińska, prawdziwa; traci nerwy i z trudem kontroluje morale w Ekspedycji. Straciła brata na Esuriit. Stoi po stronie Zespołu; ma nadzieję wrócić.
* mag: Łukija Zajcew, bard; Esuriit; zna się na Pryzmacie i szczerze kocha swoje dziecko (a przynajmniej go dogląda). Skłonna do zabicia wszystkich by tylko Fiodor przeszedł przez Portal.
* mag: Bazyli Weiner, katalista i badacz; Esuriit; * mag, który transformuje elementy fazy Esuriit w * magię kompatybilną z energią syberionu. 
* mag: Jakub Pestka, badacz i lekarz; prawdziwy; współuczestniczy w konwersji energii z Esuriit w "coś normalnego". Tak pomocny jak umie. 
* mag: Dosifiej Zajcew, * mag bojowy; Esuriit; poszedł zdobyć energię na fazie Esuriit zamiast kogoś z Zespołu.
* mag: Konrad Myszeczka, badacz i hodowca, Esuriit; zaprojektował z Zofią czerwie trzymające Ekspedycję przy życiu na tej fazie. 
* mag: Fiodor Maius Zajcew, dla ratowania którego Łukija była skłonna poświęcić Ekspedycję. Zdaniem Andżeliki, na nim zakrzywia się płaszczyzna Pryzmatu.
* mag: Zofia Weiner, badacz Esuriit; prawdziwa; okazało się, że prowadziła eksperymenty nad speculoidem chcąc się z nim dogadać. Wyszło... jak zwykle.
* mag: Konstanty Bankierz, terminus; prawdziwy; poszedł zamiast kogoś z Zespołu polować na energię na fazie Esuriit.
* mag: Paweł Maus, lekarz; uważa się za istotę Esuriit; powiedział Hektorowi, że ma dostęp do źródła energii jak by było potrzebne i powiedział o pesymistycznej wizji tej Ekspedycji.
* vic: Siriratharin, Speculoid; podobno nie może dostać się na teren bazy, ale jego szepty są wszechobecne.

# Lokalizacje

1. Świat
    1. Faza Esuriit
        1. Insula Luna
            1. Srebrna Septa
                1. Centrum
                    1. Centrala Świecy
                    1. Blockhaus K, gdzie Speculoid potrafi szeptać przeszkadzając Zespołowi we śnie.
                    1. Archiwum, gdzie znajdują się rekordy i informacje dotyczące działania Ekspedycji i jej celów.
                1. Skrzydło szpitalne
                    1. Sale medyczne, gdzie znajduje się Fiodor i Łukija. 
  
# Skrypt

# Czas

* Dni: 2
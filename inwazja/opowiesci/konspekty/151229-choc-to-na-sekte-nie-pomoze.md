---
layout: inwazja-konspekt
title:  "..choć to na sektę nie pomoże (PT)"
campaign: ucieczka-do-przodka
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [151223 - ..Można uwolnić czarodziejkę... (PT)](151223-mozna-uwolnic-czarodziejke.html)

### Chronologiczna

* [151223 - ..Można uwolnić czarodziejkę... (PT)](151223-mozna-uwolnic-czarodziejke.html)

## Kontekst misji

Czyściciel z ramienia Zajcewów wprowadził słupa (Orank) i sektę (Kły Kaina); ślady przekierowuje na nich.
Wiła nie żyje, zatruła się narkotykami gdzieś w lesie.
Policjanci na SERIO szukają śladów tajemniczej krwawej sekty.
Joachim Zajcew szuka Korzunia w sprawie "zabitej przez niego wiły".
Pielęgniarka Marzena okazała się baterią - defilerką dla Adama Diakona.
Adam Diakon się "nawrócił" na parafii zgodnie ze słowami Karmelika.
Maria dorabia pracując dla "Rytmu Codziennego"; przekazała Paulinie donos o "zamtuzie w lesie".

## Stawki:

- Czy "Kły Kaina" ustawią skutecznie szkołę sztuk walki "Chenil de loup" uczącej 'FAIM DE LOUP'?
- Czy Łukasz Perkas da radę uchronić się przed nędznym losem (nieważne skąd przyjdzie ów los)? 
- Czy Korzunio skutecznie ukryje prawdziwą przyczynę śmierci wiły przed Joachimem?

## Misja właściwa:
### Faza 1: Dojo "Faim de loup".

Dzień 3:

Paulina odprowadziła Marzenę na dworzec kolejowy. Świeżo odtworzona czarodziejka oddala się w nieznanym kierunku; zarówno Paulina jak i Marzena wiedzą, że najpewniej spotykają się po raz ostatni. Na odchodnym jeszcze Marzena powiedziała Paulinie, że jakby kiedyś wpadła w paskudne kłopoty, niech spróbuje powiedzieć, że ma "pochodnię niosącego światło". Paulinie rozdzwoniły w głowie dzwonki alarmowe (bo Illiusitius), ale nie powiedziała ani słowa.
I Marzena odjechała. A Paulina wróciła do pracy.

Dzień 5:

HIDDEN MOVEMENT:

- Korzunio fałszuje ślady, że wszystkie działania (łącznie z tymi w szpitalu) to robota "Kłów Kaina"
- Łukasz Perkas opisuje jak "Faim de loup" - sztuka walki - jest korzystna dla młodzieży itp. 
- Joachim konfrontuje się z tymi śladami; idzie szukać 'Kłów'. 
- Maria informuje Paulinę o tym, że chyba JEST tu jakaś grupa czy sekta; wysyła artykuł Perkasa. 

END OF HM

Maria wysłała Paulinie artykuł Perkasa; dużo rzeczy ją tam zafrapowało. Po pierwsze, dziennikarz śledczy. Dlaczego dziennikarz śledczy pisze czołobitny wywiad z adeptem niebezpiecznej, krwawej i nastawionej na zadawanie bólu sztuki walki (fakt, że w wywiadzie on mówi o zdrowej młodzieży i o kontroli swoich instynktów; ale Faim de loup jest raczej kojarzona inaczej we Francji skąd pochodzi). Następnie, Maria skrzyżowała to ze słowami Marzeny; "niosący światło" to Lucyfer. Czyli Marzena chciała dać Paulinie coś do ochrony przed satanistami? Może uciekała nie tylko od Diakona ale i "jego" sekty? Marzena mogła coś więcej wiedzieć...

Plus, to dojo. I samo podejście Wiesława Rekina (mistrz Faim de Loup) - podobno wbił do 'Rakiety' gdzie toczy się 'Fight club', sklupał obecnych wykrzykując jakieś hasła, po czym pokonał ochronę i wyszedł z uśmiechem. A teraz wszystko wskazuje na to, że dostanie pozwolenie na założenie szkoły sztuk walki. A takie artykuły jak ten jedynie pomagają.
Paulina powiedziała Marii, że warto by ta porozmawiała sobie z tym Perkasem (dziennikarzem). Maria się zgodziła. Coś tu się dzieje, a Maria nie chciałaby, żeby to była pułapka Adama Diakona na Paulinę.

HIDDEN MOVEMENT:

- Rekin, "mistrz sztuk walki", zakłada dojo. 
- Policja ma zgłoszenie o podejrzanych typach kręcących się dookoła parafii z Gęsilotu 
- Alicja ostrzega pozostałych policjantów o sekcie; 
- Po akcji w 'Rakiecie' (poprzednia misja, hidden mv) 'Kły Kaina' dostały kilku kandydatów na członków 

END OF HM

Dzień 7:

Paulina przyłapała dr Grażynę Tuloz i policjanta Bartosza Bławatka na konferowaniu o tajemniczych obcych kręcących się koło Gęsilockiej parafii. Policjant szeptał, że to może być to co kiedyś; ale tym razem centrala nie pomoże. Czy Grażyna nie miałaby czegoś, co mogłoby pomóc. Pani doktor powiedziała, że jest LEKARZEM a nie... ale oki, coś znajdzie. Coś spróbuje pomóc. A czy Bartek nie mógłby ich wylegitymować czy zatrzymać? Nie, nie ma podstaw...
I wtedy zauważyli Paulinę. NO PANI DOKTOR PLASTEREK! PROSZĘ TO PLASTEREK! DZIĘKUJĘ ZA PLASTEREK!...
Paulina facepalmowała. Ale dyskretnie.

Nie no, trzeba coś z tym zrobić. Paulina poszła odwiedzić panią doktor Grażynę Tuloz gdy tylko ta miała chwilę czasu i powiedziała, że słyszała rozmowę z policjantem. Mimo, że lekarka bardzo próowała Paulinę spławić, ta się nie dała; w końcu wyciągnęła z Grażyny prawdę: parę lat temu była tu sekta. Nazistowscy sataniści (tak, serio). Nazywali się "Krwawą stalą". I oni szukali swojej relikwii - jak Paulinie zapewne wiadomo, naziści mieli tendencje do bycia okultystami. Ta relikwia była tego typu obiektem - metalowa swastyka służąca do podcinania ciała, by krew spływała na środek. I była podpalana. I była z dziwnego metalu. Paulina - wszystkie możliwe dzwonki alarmowe. Stara, dziwna, magia krwi. No i te trzy lata temu Grażyna i Bartosz... schowali z pomocą księdza w kościele ową relikwię. Gdy sataniści przyszli jej szukać, zgarnęła ich policja.

A teraz najpewniej wrócili.

Aha, Paulina nie dowie się gdzie jest relikwia. Jest pod wodą święconą. Całkiem rozsądnie jak na człowieka...

Maria też powiedziała Paulinie jeszcze jedną rzecz - złapała Łukasza Perkasa i z nim porozmawiała. Perkas... de facto, jest ich człowiekiem. Wspiera tą "sektę". Co więcej, Perkas potwierdził istnienie sekty. Że jest jakaś sekta. Maria go wrobiła. Nie powiedział jaki jest zakres tej sekty, ale był zdecydowanie pod wrażeniem - widział tą sektę w akcji i miał wielkie oczy. Nawet powiedział Marii "Nie zbliżaj się do nich, bo cię zniszczą; nie masz szans, kochana". Oczywiście, Paulina wszystkiego się od Marii dowiedziała.

I - niespodzianka - nie spodobało jej się to.

### Faza 2: Wilk w świetle dnia.

HIDDEN MOVEMENT:

- Alicja wiąże to, że to ZUPEŁNIE OBCA SEKTA i informuje o tym kolegów 
- Ireneusz Przaśnik decyduje się podpalić dojo; atak odparty 
- Mistrz dojo kontratakuje; "narkotyki są złe, Czarny Dwór jest zły". Perkas drukuje z tego wywiad. 
- Policja dostaje zadanie Naprawić Sprawę. 

END OF HM

Dzień 8:

Paulinę w pracy czekała niespodzianka - Grażyna chciała ją chyba uziemić, bo rzuciła jej WSZYSTKICH pacjentów Karmelika. Na szczęście dwie godziny potem pojawił się Karmelik i bardzo przepraszał; chcieli mu bratanka spalić. Co? Okazuje się, że ów bratanek był w dojo "Faim de loup" i tam podjechało sześciu i chciało zrobić pokaz siły. Min. podpalić dojo - a przynajmniej spowodować jakieś straty. W odwecie adepci sztuk walki wklupali im bezlitośnie a mistrz ich przesłuchał i wyciągnął z nich wszystko. 

Potem jeszcze mistrz zdążył udzielić wywiadu Perkasowi, w którym to wywiadzie powiedział, że stawiają czoło narkotykowej grupie promując zdrowy duch i zdrowe ciało. I, że "szlachta bawiąca się w ciemności" go nie zatrzyma - dając WYRAŹNY wskaźnik na "Czarny Dwór". Ogólnie sprawa - zwłaszcza dzięki Perkasowi - naprawdę zrobiła się głośna i policja została skierowana na "Czarny Dwór" z sekty.

Maria powiedziała, że ona nie może się tu pojawić; ale to wszystko ślicznie wygląda. Aż za dobrze. I faktycznie, Maria potrafi powiązać "Czarny Dwór" z lokalnym przestępcą - Ireneuszem Przaśnikiem. On trzyma "Czarny Dwór", ale on min. zwalczał działania Gali Zajcew, bo nie była pod jego kontrolą.

Paulina jeszcze szybko wyciągnęła od Karmelika co się da odnośnie samych zajęć i mistrza; dowiedziała się, że nie tylko mężczyźni chodzą na zajęcia. I Paulina wpadła na pomysł. Pójdzie na zajęcia 'Faim de loup'.

I po pracy to zrobiła.

Spotkanie w dojo Paulina uznała za całkiem ciekawe. Mistrz Wiesław Rekin wygląda jak taki zakapior, ale uduchowiony zakapior. Sztuka walki którą uczy jest sztuką ofensywną i mocno zewnętrzną; zawiera w sobie też silny kolektywizm i całkowity brak litości. Daje "odpowiedzi" na "trudne pytania" i jest bardzo pociągająca dla młodych ludzi - do tego jest żarliwy i wyraźnie jest ideowcem. Paulina potwierdziła też, że najpewniej jest kultystą. Grupa dookoła Rekina to raczej ludzie którym imponuje siła i nie mający wielkich perspektyw...

Przy wyjściu Paulina zobaczyła policjantkę, która przepytywała jednego z młodych ludzi. Co on tu robi, czemu jest tutaj? Ledwo ona go ze sprzedaży narkotyków wyplątała a znowu w coś się pakuje? On nie chce z nią współpracować; policjantka westchnęła i poszła do mistrza Rekina. Spytała go o artykuł, czy był tu atak? Nie. Nie?! Ano, nie, nie było. Alicja (policjantka) niewiele mogła zrobić, podziękowała i poszła. Mistrz został zapytany, czemu nie pozwolił policji pomóc. Odpowiedział z uśmiechem, że "światło rozwiąże wszystkie problemy".

Paulinie się to zupełnie, zupełnie nie spodobało.

HIDDEN MOVEMENT:

- Kły Kaina rozprowadziły trochę narkotyków w Czarnym Dworze przez szantaż dealerów; są otrucia 
- Do szpitala trafia fallout; Paulina widzi, że się dzieje coś solidniejszego 
- Alicja odwozi nieprzytomnego podopiecznego Karmelikowi; Mirek Kujec otruł narkotyki i dostał od Przaśnika 

END OF HM

Dzień 9:

Trzecia rano. Paulina na dyżurze. Przyjeżdża na sygnale Alicja Gąszcz i przyprowadza półprzytomnego Kujca. Mówi, że w "Czarnym Dworze" są skażone narkotyki; będzie kilkunastu. Ona tam jedzie, karetki też w drodze. Paulina natychmiast ściąga Grażynę Tuloz. Ta, słysząc, że "Czarny Dwór" jest źródłem problemu jest zaskoczona - to niemożliwe. Paulina się nie spiera, po prostu zapamiętuje.

Paulina ma NullField i może używać magii, bo NullField wyżre ślady. Wykorzystuje go; przesłuchuje (po udzieleniu pomocy) wszystkich w nadziei, że dowie się czegoś cennego - co tu się stało. Ma szczęście, bo Kujec dostał specjalne zadanie od Wiesława Rekina. Rekin powiedział mu, że nie ma zamiaru pozwolić policji rozwiązywać problemów. Jeśli młody chce być jednym z nich, niech on zniszczy "Czarny Dwór". Przekazał narkotyki i kazał zatruć grupę osób. Tyle, że Przaśnik złapał na tym Kujca i zmusił go do zażycia; a potem okazało się, że są zatrute.

Paulina magią dotarła też do krzyków i kłótni między policjantką Alicją Gąszcz a Ireneuszem Przaśnikiem. Ona krzyczała, że to nie taka była umowa, że miał chronić, i widzi co się dzieje? On na to, że jak ona czegoś nie zrobi, będzie krwawa jatka. Ona, że sama go wsadzi - to nie jest ta sama sekta. To nie ci sami ludzie. To coś zupełnie, zupełnie innego...

Paulina się uśmiechnęła. Wreszcie coś wie...

## Dark Future:

### Faza 1: Dojo "Faim de loup".

Zasoby init:

Gala_Tymek \| hidden = 2
'Kły Kaina'\| reputation = 2 \| hidden = 5 \| members = 5 \| integrity = 5 \| politics = 1 \| resources = 3
policja    \| morale = 5 \|
Przaśnik   \| morale = 5 \| power = 5 \| members = 5 \| resources = 10

- Korzunio fałszuje ślady, że wszystkie działania (łącznie z tymi w szpitalu) to robota "Kłów Kaina"
..... (Gala_Tymek.hidden#)
- Łukasz Perkas opisuje jak "Faim de loup" - sztuka walki - jest korzystna dla młodzieży itp. 
..... ('Kły Kaina'.reputation#, Przaśnik.morale-)
- Joachim konfrontuje się z tymi śladami; idzie szukać 'Kłów'. 
..... (Gala_Tymek.hidden#, 'Kły Kaina'.hidden-)
- Maria informuje Paulinę o tym, że chyba JEST tu jakaś grupa czy sekta; wysyła artykuł Perkasa. 
..... (brak wpływu)
- Rekin, "mistrz sztuk walki", zakłada dojo. 
..... ('Kły Kaina'.hidden-, 'Kły Kaina'.reputation#, 'Kły Kaina'.politics#, 'Kły Kaina'.politics-)
- Policja ma zgłoszenie o podejrzanych typach kręcących się dookoła parafii z Gęsilotu 
..... ('Kły Kaina'.hidden-)
- Alicja ostrzega pozostałych policjantów o sekcie; 
..... ('Kły Kaina'.hidden-, policja.morale-, policja.wiedza#)
- Po akcji w 'Rakiecie' (poprzednia misja, hidden mv) 'Kły Kaina' dostały kilku kandydatów na członków 
..... ('Kły Kaina'.members#, 'Kły Kaina'.integrity-, Przaśnik.morale-)

### Faza 2: Wilk w świetle dnia.

Zasoby init:

Gala_Tymek \| hidden = 4
'Kły Kaina'\| reputation = 4 \| hidden = 1 \| members = 6 \| integrity = 4 \| politics = 2 \| \| resources = 2
policja    \| morale = 4 \| wiedza = 1
Przaśnik   \| morale = 3 \| power = 5 \| members = 5 \| resources = 10

- Alicja wiąże to, że to ZUPEŁNIE OBCA SEKTA i informuje o tym kolegów 
..... (policja.wiedza#)
- Ireneusz Przaśnik decyduje się podpalić dojo; atak odparty 
..... (Przaśnik.morale2-, Przaśnik.power-, 'Kły Kaina'.reputation2#, 'Kły Kaina'.hidden-)
- Mistrz dojo kontratakuje; "narkotyki są złe, Czarny Dwór jest zły". Perkas drukuje z tego wywiad. 
..... (Przaśnik.resources2-, 'Kły Kaina'.hidden2-, 'Kły Kaina'.politics#)
- Kły Kaina rozprowadziły trochę narkotyków w Czarnym Dworze przez szantaż dealerów; są otrucia 
..... (Przaśnik.resources2-, Przaśnik.morale-)
- Do szpitala trafia fallout; Paulina widzi, że się dzieje coś solidniejszego 
..... (brak wpływu)
- Policja dostaje zadanie Naprawić Sprawę. 
..... (policja.wiedza#, policja.morale#, Kły.resources-, Przaśnik.resources-)
- Alicja odwozi nieprzytomnego podopiecznego Karmelikowi; Mirek Kujec otruł narkotyki i dostał od Przaśnika 
..... (brak wpływu)
- Korzunio przekupuje kogo się da, by osadzić 'Kły'
..... ('Kły Kaina'.reputation#, 'Kły Kaina'.resources#, 'Kły Kaina'.politics#)

# Zasługi

* mag: Paulina Tarczyńska, która wpadła z czarodziejki pod sektę; celnym użyciem * magii poznała strony konfliktu.
* mag: Marzena Dorszaj, która zostawiła Paulinie hasło do kontaktu z ewentualnymi satanistami i oddaliła się w nieznane.
* czł: Grażyna Tuloz, okazuje się, że 3 lata temu uczestniczyła w akcji przeciwko sekcie; schowała im satanistyczno-nazistowską relikwię.
* czł: Bartosz Bławatek, policjant, który współpracował z dr Tuloz przeciwko sekcie 3 lata temu; cholernie się boi powrotu sekty.
* czł: Wiesław Rekin, mistrz 'Faim de loup', posiadacz dojo w Przodku, kultysta 'głosiciel światła' nie bojący się wyjść poza prawo. Drapieżnik.
* czł: Ireneusz Przaśnik, lokalny 'mafioso' współpracujący z częścią policji. Miał jakiś deal. 3 lata temu pomógł przeciwko sekcie.
* czł: Alicja Gąszcz, policjantka wyraźnie współpracująca z lokalną przestępczością, też: chce pomóc tym młodym ludziom.
* czł: Łukasz Perkas, dziennikarz śledczy, który został tubą propagandową dla kultu. Ostrzegł Marię, by w to nie wchodziła.
* czł: Maria Newa, która próbowała dowiedzieć się, czemu dziennikarz jest tubą dla kultu; też, co działo się w przeszłości?
* czł: Mirek Kujec, który pragnie 'nieść światło'. Idzie za silniejszym - wpierw za Przaśnikiem, teraz za Rekinem. Policjantka chce mu pomóc.
* czł: Jerzy Karmelik, który spanikował, bo bał się, że mu spalą bratanka. Bardzo rodzinny, czego się by po nim NIKT nie spodziewał po Marzenie.

# Lokalizacje:

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Leere
                1. Przodek
                    1. Centrum
                        1. Szpital Gotycki
                        1. klub Czarny Dwór
                    1. Osiedle Metalowe
                        1. dojo Faim de loup
                        1. klub Rakieta
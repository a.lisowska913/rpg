---
layout: inwazja-konspekt
title:  "Zabójczy spadek"
campaign: czarodziejka-luster
gm: żółw
players: kić, szpaqq
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [121104 - Banshee, córka mandragory. (An)](121104-banshee-corka-mandragory.html)

### Chronologiczna

* [121104 - Banshee, córka mandragory. (An)](121104-banshee-corka-mandragory.html)

## Kontekst ogólny sytuacji

Dawno temu, Damazy Bogatek był podróżnikiem. Min. podróżował na wschodzie; też zapuścił się na Syberię. Jednak wiry Syberyjskiej magii mogłyby pochłonąć tego człowieka, więc umierający mag któremu Damazy próbował pomóc sformował artefakt - stworzył grubo ociosanego śledzika syberyjskiego z kamienia i wpisał weń pająka astralnego (miecztapauk). W ten sposób był w stanie umożliwić Damazemu poruszanie się po niebezpiecznej Syberii. Dał jednak surowy prikaz - jeśli nie na Syberii, artefakt ma znajdować się w lapisowanej szkatułce. Damazy postępował zgodnie z instrukcją całe swoje życie...

Damazy Bogatek umarł - ze zwykłej, ludzkiej starości. Jego przedmioty zostały przekazane dalej w spadku. Wnuczka - Emilia - mająca kompleksy i czująca się niekochana z uwagi na to, że jest bogata i nie ma prawdziwych przyjaciół zdecydowała się na wyciągnięcie ze szkatułki czegoś co będzie przypominało jej dziadka. Tego śledzika syberyjskiego. I miecztapauk zabrał się do pracy.

Emilia przypominała sobie, że jest kochana, że dziadek ją szanował i kochał. I magia zaczęła odpowiadać. Dodatkowo, Czeludzia może nie jest największym miastem, ale jest wystarczająco wielka, by miecztapauk zbierał więcej energii niż mógł wykorzystać. Zaczął więc wydzielać "skrystalizowany syberion" na wierzch figurki. A Emilia, jak ćma przyciągana do ognia, zaczęła tym słodzić herbatę...

Prędzej czy później energia magiczna przekształciła Emilię w istotę wielkiej piękności, podziwianą i kochaną. Zaczęła eksperymentować z RPGami (a dodatkowo czasem manifestowała śledzia syberyjskiego jak był nadmiar energii). Tu wszedł Rudolf Mikołaj - młody chłopak zainteresowany jedną z akolitek Emilii. Dołączył do ich sesji RPGowych i stwierdził że to dla dzieci i idiotów. Emilia życzyła mu śmierci i magia odpowiedziała...

Z uwagi na działania miecztapauka kwas i inne narkotyki działały słabiej (brak marzeń i snów). Stąd pozostałe śmierci.

## Punkt zerowy:

3 młodych ludzi zmarło na przedawkowanie kwasu.

Ż: Czemu zdjęcie dziwnych zwierząt (śledzików) skłoniło Andromedę do wysłania maila Zygmuntowi? <br/>
K: Bo jeden z jego followersów przyniósł zdjęcia Flakowi <br/>
Ż: Dlaczego Flak chce współpracować z Andromedą w kwestii zdjęcia? <br/>
S: Ona wie, co to za istota - więc łatwiej to sfalsyfikuje lub potwierdzi <br/>
Ż: Skąd Andromeda wie o śledziach syberyjskich? <br/>
K: Bo Andromeda się już o nie otarła <br/>
Ż: Skąd dostałeś niewyraźne zdjęcia śledzi syberyjskich? <br/>
S: Od jednego z pseudofanów - blogersów <br/>
Ż: Skąd Andromeda wie o kiepskich zdjęciach śledzi? <br/>
K: Bo on to pokazał na swoim blogu <br/>
Ż: Jaka jest korelacja między śmiercią tych trzech nastolatków od kwasu a śledziami? <br/>
S: Jeden z chłopaków który zmarł dostarczył te zdjęcia Zygmuntowi <br/>

## Misja właściwa:

### Dzień 1:

Zygmunt. Ściągnięty z urlopu, bo prokurator podrywa młodą adwokat a jej kuzyn padł od kwasu - chce mieć drugą opinię. 
Zygmunt sprawdził stan raportu Stefana; poprzedni patolog sprawdził dokładnie jednego a innych pobieżnie na obecność kwasu. I napisał raport:
- 2 przedawkowało kwas, trzeci (Rudolf )nie
- Rudolf był tym, który przyniósł Zygmuntowi zdjęcie dziwnego zwierzęcia
- nie wiadomo na co Rudolf umarł
- właśnie o Rudolfa chodzi prokuratorowi - dlaczego zginął Rudolf?

Zygmunt przebadał dokładniej zwłoki i zobaczył tam... ślady tkanki magicznej u wszystkich. Ale u Rudolfa najwięcej. Tyle, że Zygmunt nie jest magiem - nie rozumie co widzi. Lekko zaskoczony, zadzwonił do Andromedy. Ona odebrała. Powiedział jej, że człowiek który dostarczył mu zdjęcie nie żyje - a tkanka magiczna znaleziona. Denat w momencie śmierci był w lasku, w ruinie domku. Tam go znaleziono wraz z jego kolegami.

Spotkają się pod Kościołem Franciszkanów i pójdą do lasku. Andromeda próbowała wykryć magię srebrem zanim się spotka z Zygmuntem. Wyczuła lekką aurę rezydualną - prawie niezauważalną. Jakby nie wiedziała że szuka, to by nie znalazła... Spóźniła się dobre 15 minut... Zygmunt nie był zadowolony. Ale po drodze się uzbroił. Poszli do lasku. Doszli do ruiny i szukają... czegoś.

Skupili się na przeszukiwaniu. Zygmunt zauważył, że Rudolf umarł na szok anafilaktyczny - jego ciało odrzuciło jego własne ciało. Miał halucynacje. Jako JEDYNY. Ostatnio w sumie większość osób nie ma marzeń... a Andromeda zauważyła, że tu było potężne wyładowanie Skażenia; energia magiczna uderzyła prosto w niego.

Po analizie zdjęcia wszystko wskazuje na to, że jest to fotografia fotografii. Zrobione z ręki iPhonem i wydrukowane. Czas Czesława; Zygmunt zadzwonił do Łukasza (prokuratora). Ten dał mu upoważnienie do tego by Czesław mu pomógł. Jednak Zygmunt podszedł do tego bardzo arogancko i Czesław odmówił - chce iść procedurami. Zygmunt obszedł i zadzwonił do prokuratora, ale Czesław stoi przy swoim. Dobra, Zygmunt dogadał się z szefem policji. On będzie mediatorem i Zygmunt dostanie dane z iPhona:
- info ze zdjęcia
- wykaz rozmów
- wydruk zdjęć z telefonu z dnia zgonu

Po dwóch godzinach Zygmunt dostał to wszystko. Robi się wieczór. Siedzi i szuka. Nie zna się na takim szukaniu, ale "ja siama" - poświęcił noc i znalazł wszystko co ważne. Tyle, że rano cholernie niewyspany.
- zdjęcie śledzika jest zrobione w domu Emilii Bogatek
- Rudolf kontaktował się z Emilią; próbował się do niej dodzwonić i dobrać
- zdjęcie grupy 5-6 czarnopłaszczowych RPGowców grających w coś. Zrobione ukradkiem. Tam jest też kiepsko zrobiona figurka śledzika.
- zdjęcie jest z piwnicy
- był potem SMS Rudolfa do tej Emilii "lol dorośli ludzie tak się nie bawią" w odpowiedzi na jej "przysięgałeś milczeć"

Andromeda poszła do lokalnej biblioteki i szuka gazet. Szuka wszystkiego co może wskazywać na efekty magiczne. 
- to jest pierwszy przypadek śmierci takiego typu
- nie ma marzeń, nie ma snów w okolicy. Skorelowane z redukcją sztuki, poezji etc
- zaczęło się mniej więcej 2 lata temu
- Tadeusz Bogatek prowadzi klinikę dla ofiar PTSD i ogólnie odwykowe. Jego córka - Emilia - podobno robi sektę. Szataniści.
- Weronika Przybysz opuściła miasto i Emilię więc popełniła samobójstwo. 3 miesiące temu. "Robiła dziwne rzeczy w piwnicy"
- Podobno widziano dziwną rybę z łapkami. Podobno ryba pożarła jej chihuauę. Nie było śladów. Geraldina Kurzymaś.

Andromeda jeszcze znalazła adres do Geraldiny Kurzymaś. Chce z nią rano porozmawiać.

### Dzień 2:

Geraldina okazała się być sympatyczną starszą kobietą. Opowiedziała o śledziu syberyjskim. Wielkim śledziu który pożarł jej pieska który ją chronił. Śledziu... wyidealizowanym. Jak pryzmatyczny obraz czyjś na temat śledzia syberyjskiego.

Żółw:      "Masz telefon"
Andromeda: "Odrzucam"
Zygmunt:   "O kurwa ale się wkurwiłem. Odrzucam"
Andromeda: "Ok"
Zygmunt:   "Dzwonię - to JA dzwonię do ludzi."

Pokłócili się. 
Zygmunt zdecydował się wziąć jeszcze jedną kawę i pojechać do domu Bogatków. Andromeda zdecydowała się pozbierać plotki o Bogatkach.

Zygmunta przy drzwiach spotkał kamerdyner Alfred. Zygmunt powiedział, że chce do Emilii a Alfred, że Tadeusz chce z nim rozmawiać. Zygmunt chwilę porozmawiał z ojcem rodziny, lecz Emilia zeszła z góry i poprosiła ojca, by mogła przejąć rozmowę. Nie umiał jej odmówić. Na Zygmunta też wywołała efekt - Emilia jest piękna i władcza, urocza i hipnotyzująca. Z trudem oparł się jej urokowi; powiedział jej, czego szuka i wszystko czego się na jej temat dowiedział. Emilia zaproponowała mu herbatkę (posłodzoną Syberionem) - czując, że coś jest nie tak Zygmunt chciał ją dyskretnie wylać ale jedynie rozlał ją na siebie. Perlisty śmiech Emilii. Poprosił czy może pójść do łazienki się wysuszyć, dziewczyna się zgodziła.

Po drodze do łazienki Zygmunt się rozejrzał - zobaczył że wszędzie są zdjęcia i portrety Emilii a w sypialni na suficie dziewczyna ma ogromne lustro. A na podeście stoi brzydki, wyciosana/wypalona figurka śledzia syberyjskiego. WTF.

Więcej już nie pytał. Zmył się jak niepyszny, bojąc się tego co będzie działo się dalej...

Andromeda na bazarze zbiera plotki:
- Emilię kochają wszyscy. 
- Rudolf nie był stąd. Przyjechał na wakacje i skończył martwy
- Jest ryba z nogami, ale nie powstała historia na jej temat; brak imaginacji
- Emilia kiedyś taka nie była; zaczęło się jakiś czas temu, pół roku?
- 2 lata temu umarł dziadek Emilii i Bogatkowie dostali spadek
- dziadek Emilii był podróżnikiem; jeździł sporo. Zwłaszcza po wschodzie. 
- dziadek był szczególnie kochający Emilię. Zawsze jej mówił, że jest kochana, najlepsza, patrzyła do środka - ona miała kompleksy

Jest popołudnie. Zygmunt się obudził.
Nie wiadomo co się tam stało, co jest w tym domu... 

Spotkali się w knajpie "U Zdzisia". Pogodzili się (Zygmunt zapłacił Andromedzie za taksi). 

Zygmunt opowiedział, że był w domu Bogatków. Powiedział o zdjęciu, od dziadka. Powiedział, że miał problemy z Emilią - ma wszędzie zdjęcia, sama była... wow. Andromeda dała mu do sprawdzenia coś srebrnego - zakłuło. Zygmunt zakrzyczał Andromedę "wtf", ona okłamała go czymś nieistotnym. I potem Zygmunt opowiedział o dziwnym posążku... i ogromnym lustrze na suficie.

Andromeda się bardzo zasępiła. Lustro... super. Tylko nie lustro...
Andromeda zaczęła zostawiać ślady swojemu "niekompetentnemu" terminusowi by przyjechał i zrobił porządek. To już wygląda na poważniejsze problemy z magią.

### Dzień 3:

W nocy obudził wszystkich ogromny huk. Cysterna straciła panowanie i wjechała prosto w dom Bogatków, wybuch zabił większość a figurki nie odnaleziono...

(w rzeczywistości, Skorpion wszedł do akcji pod dowództwem Damazego Czekana i zdobył artefakt i zatarł ślady przed ludźmi i magami używając cysterny).

# Progresja

## Frakcji:
* Skorpion: zdobył figurkę śledzia syberyjskiego z wpisanym miecztapaukiem (generującą krystalizowany syberion kosztem snu i marzeń)
* Skorpion: porwał Emilię Bogatek jako potencjalną broń / narzędzie

# Streszczenie

Andromeda spotkała się z syberyjskim artefaktem - figurką śledzia syberyjskiego z zaklętym miecztopaukiem. Niestety, artefakt ten zdążył już skazić wrażliwą 22-latkę, Emilię Bogatkę. Viciniuska zbudowała dookoła siebie (nieświadomie) kult i ludzie zaczęli ginąć. Andromeda z lokalnym patologiem, Zygmuntem próbowali dojść do tego o co chodzi. Niestety, nie zdążyli zniszczyć figurki - wbiły siły Skorpiona i porwały Emilię oraz zdobyły figurkę dla siebie.

# Zasługi

* czł: Kasia Nowak, która nie zdążyła zdobyć informacji, którymi mogłaby ściągnąć maga i - być może - uratować Emilię (viciniuskę) przed Skorpionem
* czł: Zygmunt Flak, patolog-sceptyk dążący do prawdy za wszelką cenę; pomógł Andromedzie dojść do prawdy o Emilii i z trudem uratował się przed jej mocami.
* czł: Stefan Porieńko, zalatany patolog który pierwszy zbadał martwych młodych ludzi i stwierdził kwas; nie zbadał wszystkiego dokładnie
* czł: Czesław Statyw, policyjny informatyk; nie lubi Zygmunta bo ten wyśmiał coś w co Czesław wierzy; zmuszony przez prokuraturę i policję do współpracy
* czł: Łukasz Czerwiącek, prokurator podrywający młodą panią adwokat, kuzynkę Rudolfa, który nagina procedury by Zygmunt mógł udowodnić niewinność Rudolfa.
* czł: Rudolf Mikołaj, młody chłopak i kuzyn młodej adwokat który podpadł Emilii i został zniszczony przez magię figurki śledzika syberyjskiego po spróbowaniu kwasu. KIA.
* vic: Emilia Bogatek, wnuczka Damazego, którą syberion przekształcił w hipnotycznego i pięknego viciniusa; kontroluje nieświadomie nadmiar energii magicznej. 
* czł: Tadeusz Bogatek, założył klinikę PTSD i zajmującą się odwykiem. Bardzo bogaty. Niestety, jego córka stała się hipnotycznym viciniusem. KIA.
* czł: Damazy Bogatek, podróżnik, który kolekcjonował różne rzeczy i - niestety - zostawił rodzinie figurkę syberyjskiego śledzia która doprowadziła do Skażenia wnuczki.
* czł: Damazy Czekan, agent Skorpiona dowodzący akcją zdobycia Emilii Bogatek i artefaktów. Bezwzględny, wykonał zadanie z powodzeniem.
* czł: Geraldina Kurzymaś, starsza, samotna pani której hologram śledzia syberyjskiego zeżarł chihuauę. Ma dużego psa.

# Lokalizacje

1. Świat
    1. Primus
        1. Małopolska
            1. Powiat Okólny
                1. Czeludzia, 11 km od Szczawni Mokrej; mała acz zżyta miejscowość pod aurą artefaktów Bogatków
                    1. Centrum
                        1. Kościół Franciszkanów, gdzie spotykają się Zygmunt i Andromeda (i nie ma lapisu)
                        1. Willa Bogatków, gdzie znajdują się artefakty i viciniuska (Emilia)
                        1. Komenda policji, gdzie ma swobodny wstęp patolog Zygmunt
                    1. Kwiatkowo, gdzie manifestował się "hologram" śledzia syberyjskiego i gdzie mieszka Geraldina Kurzymaś
                        1. Bazarek, gdzie można kupić i sprzedać mnóstwo rzeczy a na pewno wymienić okoliczne ploteczki
                    1. Lasek Starej Wróżdy, cieszący się złą reputacją u miejscowych
                        1. Ruina po starym domku, zwykle tam ćpają; gdzie znaleziono martwą trójkę

# Wątki



# Skrypt

- brak

# Czas

* Dni: 3
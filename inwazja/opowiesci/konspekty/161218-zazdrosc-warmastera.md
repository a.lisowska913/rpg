---
layout: inwazja-konspekt
title:  "Zazdrość Warmastera"
campaign: powrot-karradraela
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [161217 - Niezbyt legalna 'Academia' Whisperwind (SD)](161217-niezbyt-legalna-academia-whisperwind.html)

### Chronologiczna

* [161217 - Niezbyt legalna 'Academia' Whisperwind (SD)](161217-niezbyt-legalna-academia-whisperwind.html)

## Kontekst ogólny sytuacji
## Punkt zerowy:
## Misja właściwa:

Dzień 1:

Rankiem Siluria obudziła się z... Ignatem. Ten dał jej w prezencie małą, posrebrzaną (fałszywe srebro) fokę. Znalazł gdzieś w zamku.

Siluria się ubrała. Kiedyś musi ;-). Ogólnie, dobry dzień:

* Artur Kotała miał koszmar senny
* Karina Paczulis ciągle szuka sposobu dostania się do Dwunastościanu.

Poszła więc do Norberta; chciała dowiedzieć się jak poszła rozmowa z Whisperwind. Ten był u pacjentki; Karolina Kupiec, terminuska. Na Instytucie Transorganiki; zajmowała się min. Głowicą. Norbert zajmuje się nią, bo ma _dziwne_ rany. Nie reagują dobrze na magię. Karolina powiedziała, że pracowała nad wzmocnionym glashundem z Warmasterem, ale on zadał nietypowe rany. A Warmaster... tu się Karolina zawahała. Siluria nacisnęła - to jest nietypowe. Karolina dokończyła wypowiedź - Warmaster dał radę opanować glashunda, ale bardziej się przejął nim niż Karoliną. To bardzo nietypowe jak chodzi o Warmastera...

Karolina powiedziała, że Warmaster był tym, który stał za projektem glashunda. Nagle wczoraj wieczorem do niej przyszedł i wskrzesił ten projekt. Wcześniej odrzucił ten projekt, bo uznał, że jest on zbyt niebezpieczny - to Karolina chciała kontynuować; glashundy były wykorzystywane przez Academię Daemonica, nie przez KADEM.

Norbert w końcu dał radę zidentyfikować o co chodzi - glashund zadał rany pryzmatyczne. Póki Karolina wierzy w obrażenia zadane przez glashunda, Norbert ma problemy z leczeniem. Więc... Siluria robi jej dobry, rozluźniający masaż. Siluria próbuje ją uspokoić i ukoić by dać Norbertowi szanse na leczenie. Jak trzeba (a Siluria ma nadzieję że trzeba) to Siluria posunie się nieco dalej ;-). Trochę dalej się posunęły, ale nie na sam koniec - a Norbert dostał okazję znalezienia drogi do leczenia Karoliny.

Potem Norbert wziął Silurię na stronę. 

* "Rany pryzmatyczne? Transorganiczny glashund? Co on sobie myśli?!" - Norbert, naprawdę wzburzony
* "Nie wiem, będę się musiała tego dowiedzieć." - Siluria

Siluria przekonała Norberta, że jednak ona porozmawia z Warmasterem. Karolina, zapytana przez Silurię, powiedziała kto wiedział o projekcie. Wprawne ucho Silurii wyczuło, że to "Karolina, Ilona... i pół AD". Niestety, Warmaster nie powiedział, czemu wskrzesił ten projekt - ale zdaniem Karoliny był wyraźnie wzburzony czymś.

Wzburzony? Siluria poszła spotkać się z Whisperwind. Ona się orientuje dobrze w stanach emocjonalnych Warmastera. Ale idąc tam napotkała na drodze Bogumiła Rojowca który poprosił o radę. Powiedział, że od dłuższego czasu niektóre prostsze i mniej wartościowe artefakty KADEMu są sprzedawane do Kropiaka, w relacji win-win. Ale ostatnia partia mająca kilka naprawdę dobrych artefaktów zaginęła; ktoś ją ukradł. Niestety, Radek Weiner rozmawiał z Adrianem Kropiakiem i się pokłócili...

Kropiak zatrudnił detektywa - z nieoficjalnych źródeł Bogumił wie, że ślady prowadzą min. do KADEMu. I stąd prosi o radę. Siluria zaproponowała innego, niezależnego detektywa. Padło na Mordecję Diakon. Siluria obiecała się skontaktować - zaproponowała też, że pójdzie z Bogumiłem do Adriana Kropiaka. Przekonają go, by wspólnie zatrudnić Mordecję.

Siluria poszła znaleźć Whisper; znalazła ją bez problemu. Ta spytała, jak postępy. No, w trakcie. Siluria powiedziała Whisper o złym humorze Warmastera. Ta się wyraźnie zmartwiła; nic o tym nie wie czemu jest tak zły, tak jak i o tym, że on zaczął pracę nad projektem glashunda. Whisper powiedziała Silurii, że ten eksperymentalny glashund był zaprojektowany przez Venomkissa jeszcze. Jego funkcją było to, by być PRZYNĘTĄ dla jeszcze silniejszego viciniusa. Czegoś zdolnego do pokonania Kuby Urbanka i Ewy Zajcew.

Ale nie ma NICZEGO co jest warte tego, nie teraz. To było niebezpieczne i ryzykowne.

* "To miało sens tylko wtedy, jeśli mamy zdrową i niebezpieczną rywalizację jak wtedy AD z KADEMem" - lekko wzburzona Whisperwind
* "Znajdę go. Dziękuję." - Whisperwind, zaczynając skakanie po Zamku

Siluria została z niezbyt dobrymi przeczuciami. Ścięła się z Whisperwind - musi wiedzieć, jak to się skończyło, by móc adekwatnie do tego dostosować swój przekaz. A na serio: bo naprawdę się martwi. Whisper nie chce nic powiedzieć, bo 1) to jej problem, 2) AD trzyma się razem.

* "Przekonałaś mnie. Pójdę porozmawiać z magami Academii." - Whisperwind
* "Idę z Tobą." - Siluria
* "Nie jesteś jedną z nas." - spokojnie, Whisperwind
* "Teraz jesteście jednymi z nas. On też." - spokojnie, Siluria

Siluria widzi jedną rzecz - Whisper nie rozumie, że Shadow i Midnight zintegrowali się z KADEMem. Nawet Warmaster zintegrował się z KADEMem. Cholera, nawet sariath Aleksandra Trawens zintegrowała się z KADEMem. Tylko ona, Whisperwind, nie zintegrowała się z KADEMem. Więc Siluria nie będzie teraz naciskać. Zamiast tego Siluria pójdzie do Marianny (kiedyś: Midnight), by przekonać ją, by mogła dołączyć do tej akcji. Siluria zastała Maję w Instytucie Zniszczenia. Jak to Maja.

Siluria streściła Mai co się stało z Warmasterem i reakcję Whisperwind.

* "Whisper stwierdziła, że idzie porozmawiać z AD" - Siluria
* "Whisper żyje w swoim świecie" - Maja, wzdychając
* "Powiem Ci, co teraz zrobię. Pójdę do Andżeliki Leszczyńskiej i poproszę ją o odpalenie baterii detekcyjnych KADEMu. Niech znajdzie Warmastera." - uśmiechnięta Maja 'Midnight'
* "Nie wszystkie żyjemy w przeszłości" - Maja
* "Zajmij się więc wszystkim co ważne, a ja Ci wszystko dokładnie powiem co wydarzyło się na tajnym spotkaniu AD" - uśmiechnięta Maja do Silurii

Maja powiedziała Silurii, że ta ma jej poparcie jak chodzi o Academia Memoriae. Zawsze to postęp wobec tego, co jest dzisiaj... czyli Whisper która robi pout pout pout.

Siluria znalazła jakieś puste pomieszczenie i rzuciła pytanie w powietrze

* "Quasar?" - Siluria, do pustego pokoju
* "Do usług?" - bezcielesny głos z Zamku
* "Możesz mi powiedzieć, co wkurzyło Warmastera wczoraj?"
* "Nie"
* "Nie wiesz, czy nie chcesz?"
* "Nie wiem. Interfejs Yyizdatha nie pozwala mi wykryć prawdziwego powodu. Skanuję zapisy."

Parę minut później Quasar się odezwała. Jest naprawdę zdziwiona - nie potrafi zlokalizować powodu ani przyczyny. Wszystko wskazuje na to, że anomalne zachowanie Warmastera zaczęło się w momencie, gdy widział Whisperwind w wieczorowej sukni, gdy szła z Norbertem do Hal Symulacji.

SERIO? Wszystko to spowodowane przez ZAZDROŚĆ? Siluria facepalmowała. Double facepalm, że Quasar tego nie potrafi _zrozumieć_. Przynajmniej to jest coś, z czym AD... er, Midnight (Maja)... sobie poradzi. Siluria zdecydowała się jej podrzucić liścik.

Na dobre poprawienie dnia Siluria poszła sobie poprawić nastrój z Infernią...

Dzień 2:

Poranny Rojowiec. To jest, po wyplątaniu się z Inferni. Co nigdy nie jest proste, bo Infernia próbuje Silurię usidlić coraz to nowymi środkami (playful, for sport). I tak wpierw zadzwoniła do Mordecji Diakon - czy ta ma wolny slot detektywistyczny? Okazało się, że tak. Siluria powiedziała, że spróbuje zdobyć jej parkę KADEM - Świeca. Mordecja się ucieszyła. Lubi polityczne sprawy.

Bogumił Rojowiec powiedział Silurii to, co ta chciała wiedzieć. Adrian Kropiak jest niezbyt bogatym, geekowatym właścielem "Kropiaktorium". Siluria przygotowała 'ciężkie działa' - dekolcik, perfumy, inne takie - i przygotowała się na pójście na bitwę.

* "To co, gotowy na pokrycie rozsądnych kosztów detektywa?" - uśmiechnięta Siluria
* "Ech... więcej w to wsadzam pieniędzy niż wyciągam..." - nieszczęśliwy Rojowiec

Spotkanie z Adrianem Kropiakiem w Kropiaktorium, na Primusie. Adrian Kropiak był w naprawdę wojennym humorze. Ale spojrzał na Silurię i mu zmiękkło. A zaraz stwardniało. Siluria widzi od razu, że on nie ma często do czynienia z Diakonkami - a te, z którymi ma po prostu są nim niezainteresowane. Rojowiec ma szczękę w okolicach podłogi. A Kropiak po prostu nie jest światowy, to taki lekko niskokalibrowy przedsiębiorca.

Siluria przechodzi sobie po jego sklepie, przegląda różne artefakty... prowadzi narrację, że to ciekawy pomysł i fajne miejsce, trochę posłodzić, podziw (daje radę z biznesem)... ogólnie, Siluria w swoim żywiole. Siluria zaczęła snuć jak to fajnie że jest dwugildiowa inicjatywa... szkoda, by to wszystko się miało rozpaść. Zarówno Adrian jak i Bogumił się zgodzili z tą wizją. Siluria krok po kroku doprowadziła ich do Mordecji Diakon - niech faktycznie PRAWDZIWY sprawca będzie wykryty. Bo Siluria nie wierzy, by to był "KADEM" czy "Świeca" - najwyżej jakiś jeden mag.

I zaprowadziła ich do Mordecji. Tam już mogła ich zostawić.

Po południu z Silurią skontaktowała się Maja. Powiedziała, co następuje:

* Andżelika oczywiście znalazła Warmastera. Whisper trochę protestowała, czemu nie Quasar; jednak Maja i Sławek wymusili co chcieli.
* Spotkanie Whisper - Warmaster - Sławek i Maja. Wyszło na spotkaniu, że Warmaster chciał zaimponować Whisperwind.
* Kolektywnie wybili Warmasterowi to z głowy.
* Whisperwind zaproponowała Academia Memoriae. Wszyscy magowie obecni (AD) się zgodzili, ale...
* ...nie zgodzili się zmieniać Instytutów i ról. Przeszłość to przeszłość
* Whisperwind i Warmasterowi wyraźnie na tym spotkaniu było głupio
* Warmaster poszedł przeprosić Karolinę. Glashund poszedł "na strych"

Gdy Siluria powiedziała Mai, że to wszystko dlatego, bo Warmaster widział jak Whisper poszła w sukni wieczorowej z Norbertem... to Maja facepalmowała.

Siluria poszła jeszcze odwiedzić Norberta. Powiedziała mu, że problem z glashundem rozwiązany. Jak spytała o co chodziło z suknią, Norbert powiedział, że Whisper bardzo lubi tańczyć - więc Norbert ją zaprosił. Siluria powiedziała Norbertowi, że żałuje, że Whisper tak naprawdę nie chce się zintegrować z KADEMem. Ona po prostu cały czas żyje przeszłością i tym, co straciła podczas Inwazji - a straciła przyjaciół i rodzinę. Po takim zmiękkczeniu Norberta, Siluria przedstawiła Academię Memoriae - coś, co Whisper naprawdę chce zrobić. Coś, co może pomóc jej sobie z tym poradzić.

Norbert obiecał swoje poparcie.

# Progresja

* Siluria Diakon: dostała małą, 'posrebrzaną' foczkę znalezioną przez Ignata gdzieś na KADEMie Daemonica
* Warmaster: doprowadził glashunda do stanu 'pryzmatyczna maszyna zagłady'; schował 'na strychu'

# Streszczenie

Po rozmowie z Silurią Norbert wziął Whisperwind na tańce. Warmaster wpadł w zazdrość. Wznowił prace nad specjalnym glashundem; w wyniku Karolina Kupiec została ranna. Pojawiła się szczelina między dawnym AD a KADEMem - Whisper żyje w przeszłości. Siluria użyła eks-AD do pomocy w opanowaniu sytuacji. W międzyczasie, Rojowiec poprosił Silurię o pomoc - ktoś zakosił artefakty z Kropiaktorium które KADEM im sprzedał. I podobno są ślady na KADEM. Siluria rozładowała sprawę i skierowała ją do Mordecji Diakon.

# Zasługi

* mag: Siluria Diakon, która wykorzystała magów byłego AD by opanować konflikt personalny między Whisperwind i Warmasterem.
* mag: Norbert Sonet, który zaprosił Whisper na jeden taniec... oraz skupiał się na leczeniu Karoliny po tym, jak glashund ją boleśnie poranił przez Pryzmat.
* mag: Karolina Kupiec, terminuska KADEMu na Instytucie Transorganiki. Pomagała Warmasterowi w tworzeniu nietypowego glashunda. Poraniona Pryzmatem.
* mag: Warmaster, zazdrosny o Whisper tańczącą z Norbertem skupił się na wskrzeszeniu projektu 'glashund mroku i zagłady'. Zatrzymany przez magów AD. 
* mag: Whisperwind, izolująca się od innych magów KADEMu i myśląca o sobie jako o czarodziejce AD. Jako jedyna z tej ekipy. Przez jeden taniec z Norbertem Warmaster zrobił się dość niebezpiecznie zazdrosny.
* mag: Bogumił Rojowiec, który próbuje w jakiś sposób dbać o relacje Świecy i KADEMu. Zgłosił Silurii, że ktoś ukradł przesyłkę odrzutów z KADEMu do Kropiaktorium.
* mag: Radosław Weiner, mag KADEMu o nieco zbyt rozwiniętym poczuciu wyższości KADEMu nad Świecą; zaostrzył konflikt z Kropiakiem i to bez sensu.
* mag: Mordecja Diakon, detektyw wynajęta (dzięki Silurii) do delikatnej politycznie sprawy - ktoś ukradł dostawy z KADEMu do Kropiaktorium.
* mag: Adrian Kropiak, nieco zubożały biznesmen Świecy który skupuje odrzuty KADEMu i sprzedaje je z zyskiem jako zabawki niektórym słabszym magom Świecy.
* mag: Maja Błyszczyk, kiedyś "Midnight", summonerka i artylerzystka; zintegrowała się z KADEMem i chce pomóc ze zintegrowaniem Whisperwind.
* mag: Andżelika Leszczyńska, użyła baterii sensorów Zamku As'caen by zlokalizować Warmastera ze swoim wzmocnionym glashundem.
* mag: Quasar, która wiecznie słucha w Zamku As'caen i próbuje monitorować absolutnie wszystko - np. czemu Warmasterowi 'odbiło'.

# Czas:

* Dni: 2
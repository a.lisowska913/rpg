---
layout: inwazja-konspekt
title:  "To się nazywa 'łupy wojenne'?"
campaign: powrot-karradraela
gm: żółw
players: kić, dust
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [170208 - Koniec wojny z Karradraelem (HB, SD)](170208-koniec-wojny-z-karradraelem.html)

### Chronologiczna

* [170208 - Koniec wojny z Karradraelem (HB, SD)](170208-koniec-wojny-z-karradraelem.html)

## Kontekst ogólny sytuacji
## Punkt zerowy:

Z grupy tematów:

- Temat Wiktora Sowińskiego
- Temat Infensy Diakon
- Temat The Governess
- Co dalej z Renatą "Maus"
- Co dalej z młodą Blutwurstką
- Co dalej z autowarami
- Arazille w Czeliminie
- Prokuratura Magów
- Konflikt KADEM-Świeca
- Konflikt wewnętrzny Świecy
- Co z "Mausami poza bramą"
- Głowica 0003

Dust wybrał (kolejność ma znaczenie):

- Prokuratura Magów
- Temat The Governess
- Temat Infensy Diakon
- Co dalej z młodą Blutwurstuwną
- Co dalej z Renatą "Maus"

Kić wybrała (kolejność ma znaczenie):

- Konflikt KADEM-Świeca
- Głowica 0003
- Temat Infensy Diakon
- Co dalej z Renatą "Maus"
- Temat Wiktora Sowińskiego

Sumaryczna ilość punktów:

- Infensa: 6
- Prokuratura Magów: 5
- KADEM / Świeca: 5
- The Governess: 4
- GS Aegis 0003: 4
- Renata: 3
- Blutwurstka: 2
- Wiktor: 1

## Misja właściwa:

Dzień 1:

Dzień jak co dzień... Hektor leży w szpitalu na KADEMie. Monitoruje go Edwin i Norbert. Koło Hektora leży Siluria, blada jak nieszczęście i w stanie bardzo, bardzo kiepskim, acz... jakoś sprawnym. I niedaleko leży Renata Maus.

Edwin poprosił Hektora o udzielenie prawa transfuzji do Renaty. Wyjaśnił, że transfuzja zmieni Lady Maus w nie-Maus. Dzięki temu Karradrael będzie wolny i Mausowie przyzwą nowego seirasa (Norbert naiwnie wierzy, że MAUSOWIE a nie ANDREA to zrobi). Hektor spytał, jak zabezpieczyć Renatę po transformacji? Norbert pokazał palcem na GS Aegis 0004. Nie teraz, potem. W krótkiej rozmowie wyszło, że:

* Może się nie dać kontrolować Renaty
* Może Renata kontrolować się jako bestia
* Nie wiadomo co się stanie

Co prowadzi do smutnej obserwacji:

* Na pewno chcemy użyć mojej krwi, z centrum rodu? - Hektor
* Tylko to może pokonać krew Lady Renaty Maus, centrum rodu Maus - Edwin
* Nasze wspaniałe zgrane rodzeństwo dochrapie się nowego członka, który wniesie dużo dumy i radości - Hektor, z sarkazmem

Hektor wyraził zgodę. Edwin dał Hektorowi czekoladę. Hektor spojrzał z wyrzutem... Siluria otworzyła oczy, powiedziała "jeszcze 5 minut" i odwróciła się na drugi bok. Norbert powiedział Silurii, że ta dostała zastrzyk w Świecy; destruktor, mający niszczyć BARDZO nieudane eksperymenty. 

Edwin i Norbert ratują Renatę; komponent Blakenbauera jest odrzucany przez Renatę. Norbert powiedział Silurii wyraźnie - mogą ustabilizować Renatę, ale jeśli zrobią to bez krwi Silurii, Renata będzie... USZKODZONA. Siluria zgodziła się na transfuzję.

Dzień 2:

Hektor się obudził. Edwin powiedział mu, że już jest wybrany nowy Lord Maus - jakiś małolat, pod kontrolą Lady Terminus Kopalina. Świeca się nie opóźniła... nie pozwoliła sobie na sytuację, w której ktoś INNY przejąłby kontrolę nad Karradraelem. Edwin spytał, czy Hektor chciałby zobaczyć nową siostrzyczkę. Hektor chce. Poszli więc do niej. 

Renata jest... dziwna. Nie jest czystą Blakenbauerką, jest magiem, jest czymś... innym. Edwin spytał, czy Hektor chce zobaczyć. Chce. Renata dostała obrożę...

W sali wspomaganej. Renata śpi na łóżku. Jak Hektor wszedł, otworzyła oczy.

* Jestem... Lady Renata Maus. Padnij na kolana i oddaj mi pokłon, a WYBACZĘ Ci tą transgresję - Renata, nie orientując się w sytuacji.
* On nie spał z Tobą. Tylko ma taką głupią minę - Edwin do Renaty, o Hektorze

Jako, że Renata jest w łóżku (pod kołdrą) a na sobie ma tylko obrożę, Edwin poszedł "po piżamę" zostawiając Hektora samego z Renatą. Renata chce zdjąć obrożę; Hektor ją zatrzymał.

* Jesteś magiem KADEMu? - Renata, do Hektora, zdziwiona
* Hektor Blakenbauer. - Hektor
* Pomniejszy ród... nieważny. - Renata z pogardą.
* Nie wiem ile pamiętasz z tego co się działo... - Hektor, zaczynając - ...ale na pewno zasługujesz na wyjaśnienie.

Renata jest święcie przekonana, że nadal jest Mausem a głos który słyszy to Karradrael. Wysłała sygnał do Karradraela; odebrał go Hektor, acz nie "zrozumiał". Nieźle. Renata, ignorując swój brak ubrania próbuje wstać z łóżka, ale nie ma siły. Pada. Hektor ją podtrzymuje, na co Renata reaguje wściekłością - gdzie plebs ze swoimi łapami.

* Wiem o co chodzi z tą obrożą. KADEM próbuje się bawić. Przejmuję dowodzenie. I obiecuję, Urbanku, że spalę KADEM i zabiję KAŻDEGO maga jaki stanie mi na drodze jeśli zaraz mnie nie wypuścisz. - Renata, z lodowatą wściekłością.
* On Cię nie słyszy. Jego tu nie ma. A nie po to ta obroża. - Hektor - Ona jest po to, by... by stabilizować.
* Mnie nie trzeba "stabilizować" i nie jestem pieskiem by nosić obrożę - Renata, wcale nie spokojniejsza

Hektor pokazał Renacie o co chodzi i zdjął swoją obrożę. Renata nie pokazała po sobie większego zdziwienia; zaczęła rozmawiać, wyciągać z Hektora informacje i układać nowy plan wydostania się. Jeśli jest na Fazie Daemonica, to jest coś o czym wiele osób może nie pomyśleć. Skaleczyła się o szpon Bestii i zaczęła rzucać zaklęcie hermetyczne "Terror Supremus". Quasar uniemożliwiła to; nie chce masowego strachu na KADEMie. Renata straciła przytomność bo byciu trafioną błyskawicą.

W odpowiedzi Hektor polizał Renatę po twarzy, budząc ją. Usłyszał krzyk furii i frustracji. Renata zerwała obrożę (z pomocą Hektora) i nie przetransformowała. Kazała Hektorowi założyć obrożę i zaczyna myśleć, co do cholery tu się dzieje.

* Nie jesteś Lady Maus - Hektor, ostrożnie
* Gwarantuję Ci, że jestem. Nie ma innej. - Renata, zimno
* Mausowie mają teraz Lorda. - Hektor
* Lady Maus i Lord Maus to terminy używane zamiennie.
* No to seirasem Mausów jest teraz Lady Abelard Maus - Hektor, puentując
* Pogorszyło Ci się na Fazie Daemonica - Renata, znowu z pogardą.
* Był tylko jeden sposób, by powstrzymać Karradraela. Szukał Lady Maus. - Hektor, ostrożnie
* Nie oczekiwałabym niczego mniej. Powiedz tylko - nie zabił nikogo, prawda? - Renata, ze zdziwieniem
* Nasz kraj spłynął krwią. Nie mieliśmy wyboru. Musieliśmy dać mu nowego seirasa. - Hektor, nadal ostrożnie
* Nie słyszę go. Powinnam mieć kontakt z Karradraelem na Fazie. - Renata, orientując się powoli w sytuacji
* Nie masz kontaktu z Karradraelem, bo nie jesteś już Mausem - Hektor, ostrożnie

Renata powiedziała Hektorowi, by ten wyszedł. Gdy Hektor się zawahał, powiedziała, że nie zamierza sobie robić krzywdy - i nie jest w stanie. Gdy dowiedziała się, że ma w sobie krew Blakenbauerów, wcale nie była szczęśliwsza. Edwin zaniósł jej piżamkę z pieskami.

Tymczasem Silurię wybudził Norbert, który wraz z Jankiem zamontował jej zewnętrzny egzoszkielet. Siluria jest słaba; jeszcze potrwa nim będzie mogła normalnie działać. W tym może chodzić i efekt "skrzywdzili moją Silurię!" może pomóc w zaprowadzaniu pokoju między KADEMem a Świecą. Temat zszedł na Renatę.

* Normalnie poprosiłbym Franciszka, by z nią porozmawiał, ale Franciszek MAUS nie jest tu najlepszą opcją - Norbert.
* Czego ode mnie oczekujesz? - Siluria
* Nie mam pojęcia - Norbert, szczerze
* To daj mi wymiary Renaty... - Siluria, zrezygnowana

Siluria poszła do pokoju wybrać nieużywaną suknię. Ma ich sporo. 

* Wiesz, że mogę ją zabić i oszczędzić jej cierpienia tak, by nikt się nie zorientował? - materializująca się Whisper
* Będę brutalnie szczera. Renata Blakenbauer jest problemem Blakenbauerów - Siluria do Whisper
* Pomóc Ci dobrać sukienkę? - Whisperwind, z szerokim uśmiechem

Siluria zapukała do celi Renaty. Ta nie odpowiedziała, więc Siluria weszła. 

* Diakonka. Będziesz mnie łaskotać. - Renata, w ponurym nastroju
* Po co? - autentycznie zaskoczona Siluria
* Bo gorzej już nie będzie. - Renata siląc się na żart

Widząc stan Silurii, Renata zapytała ze zdziwieniem

* To jakaś nowa moda?
* Nie. Tylko dlatego chodzę.
* Karradrael? - pustym głosem, Renata.
* Nie tym razem - Siluria, spokojnie - Przejdzie.
* To dobrze. - Renata, spokojnie. - Nie zakładam, że przyszłaś dać mi sukienkę.
* Przyszłam przede wszystkim po to... i dać Ci okazję porozmawiania z kimś, kto nie ślini się na każde słowo.
* Jak wyglądam? - Renata - Wasz ekspert ma rację, że lustro na Fazie to zły pomysł, ale jakbyś miała miskę z wodą jak dla psa...

Faktycznie, gdy Renata chciała wysępić lustro od Hektora to Quasar nie pozwoliła (lustro + Renata + Faza + Krew == ???). Ale Siluria użyła kamerki i pokazała Renacie jak ta teraz wygląda. Renata nie do końca potrafiła zrozumieć swoją nową twarz; wygląda... łagodniej. Nie pasuje jako Blakenbauer + Maus. Siluria dodała, że aby uratować Renatę przed śmiercią, dostała też krew Diakonki.

* Mój Wzór został nieodwracalnie zniszczony. Jestem kundlem trzech rodów. Nigdy nikt czegoś takiego nie zrobił... - Renata, w szoku
* Jest to sprzeczne z... wszystkim - Renata, nadal w szoku - Świeca stanęła przeciwko samej sobie. KADEM jej pomógł. 
* Ród Blakenbauer. Ród Diakon. Ród Maus. A mnie... mnie zmieniliście w kundla. - Renata, której coraz trudniej myśleć o tym jasno

Niedługo potem rozmowa zeszła na temat Mausów.

* Abelard Maus? Naprawdę? - Renata, z wyraźnym bólem.
* Kojarzysz go? - Siluria
* Ród Maus jest martwy. Tyle Ci powiem - Renata, z pogardą. - Świeca wygrała. Powinnam była go [Abelarda] zabić gdy miałam okazję...
* Nie bierzesz pod uwagę jednej rzeczy. Z tego co wiem, jest młody. I był w stanie wezwać Karradraela. - Siluria, pocieszająco
* Ja też byłam... nie powinnam była być w stanie, gdy to zrobiłam. Na szczęście, miałam kodeks i zasady. On... - Renata, nie mogąc mówić dalej

Siluria po rozmowie spróbowała się dowiedzieć od Renaty (lub o Renacie) jakie ta ma cele i co w ogóle chce osiągnąć. Siluria idzie przyjaźnie, Renata... cóż, nie wie co robić. Konflikt -> R. Renata dowiedziała się, że Siluria podejrzewa, że ona skończy u Blakenbauerów. Siluria dowiedziała się, że Renata tak naprawdę nie ma własnego planu. Zgodnie z jej przekonaniami, jej życie zostało właśnie zniszczone. Renata jest bezużyteczna.

Gdy rozmowa zeszła na temat Karradraela, Renata nie wierzyła w to co się działo. Nie wie, że Karradrael był "zakłócony".

* Wy widzicie swoje "fakty". Ja byłam w głowie Karradraela. - Renata, z wściekłością
* Ktoś się z wami bawi. I to nie jest ABELARD. - Renata, z pogardą.

Gdy Renata poprosiła Hektora o przyjście, nadal porozmawiali chwilę o przeszłości. Siluria dostarczy Renacie jakieś datapady, by ta mogła nadrobić informacje na temat przeszłości; Renata nie ma wątpliwości, że nikt jej stąd nie wypuści. 

* Karradrael chciał mnie odzyskać. Więc pozwalacie na śmierć wielu, zamiast mnie obudzić. Ja bym go powstrzymała. Nigdy bym na to nie pozwoliła. - Renata, bardzo, bardzo zimno.
* Akceptowalnym rozwiązaniem było zniszczenie Lady Maus i zniewolenie rodu Maus. Nieakceptowalnym było negocjowanie ze mną - nadal Renata, budująca swoją linię
* Świeca złamała swoje ideały. Hektorze Blakenbauerze, oskarżam Świecę. Oraz Ty mnie wyciągniesz z KADEMu. Chcę być wolna. - Renata.

Renata zaznaczyła, że ma zamiar pomóc Hektorowi w zbudowaniu potężnej prokuratury magów (powiedział, że ma zamiar to zrobić). Takiej prokuratury, która może oskarżać magów i gildie. Takiej, która uniemożliwi to, co się stało Renacie. To, co się stało Infensie. Nie może wiele, ale wie z kim rozmawiać i w jaki sposób. Czyli Renata będzie (zamkniętym w pudełku) wsparciem politycznym dla Hektora.

I Świeca musi zapłacić.

Siluria i Hektor wymienili spojrzenia. Będzie się działo...

# Progresja

# Streszczenie

Edwin i Norbert przekształcili Renatę Maus w... "kundla trzech rodów": Maus, Blakenbauer i Diakon. Renata nie przyjęła tego najlepiej; zupełnie nie wiedziała, co się działo podczas jej nieobecności. Zdecydowała się współpracować z Hektorem w celu zbudowania potężnej prokuratury magów, by Świeca zapłaciła za to, co jej (Renacie) zrobili. Nowym seirasem Mausem został Abelard Maus. Siluria, po Destruktorze, jest bardzo słaba i potrzebuje wspomaganego egzoszkieletu do normalnego działania.

# Zasługi

* mag: Hektor Blakenbauer, przedstawiający się nowej "siostrzyczce" i jako główny dostawca krwi do Renaty.
* mag: Siluria Diakon, źródło krwi stabilizującej dla Renaty, próbuje złagodzić szok Renaty po tym, jak ta przestała być Mausem.
* mag: Edwin Blakenbauer, lekarz nadzorujący Renatę oraz tak sympatyczny jak zawsze; chce dać Renacie szansę, więc wysyła do niej Hektora (który nie zna jej historii). Złośliwy.
* mag: Norbert Sonet, lekarz nadzorujący Renatę, wraz z Jankiem złożył dla Silurii mobile suit.
* mag: Renata Maus, już nie Maus; zagubiona, nie wie co ze sobą zrobić, z poczuciem przegranej. Chce, by Świeca zapłaciła za to co się stało; będzie współpracować. Chce wolności.
* mag: Quasar, nie da się nabrać na sztuczki Renaty, monitoruje i dostarcza danych gdy potrzeba.
* mag: Jan Wątły, złożył mobile suit dla Silurii, by ta - ranna i słaba - mogła się poruszać i swobodnie działać.
* mag: Abelard Maus, nowy seiras który nie spotkał się z wysoką aprobatą Renaty ("powinnam była go zabić"). Nieobecny na misji.

# Lokalizacje

1. Świat
    1. Faza Daemonica
        1. Mare Ithium
        1. Mare Vortex
            1. Zamek As'caen
                1. KADEM Daemonica
                    1. Skrzydło medyczne
                        1. Zaawansowany medtech, gdzie Norbert i Edwin pracowali nad transformacją i ratowaniem Renaty... i nad Silurią.
                    1. Skrzydło wzmocnione
                        1. Specjalne sypialnie, gdzie przetrzymywana jest Renata Maus po transformacji

# Czas

* Dni: 2
---
layout: inwazja-konspekt
title:  "Dracena... Blakenbauer?"
campaign: prawdziwa-natura-draceny
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [160904 - Rozpaczliwie sterowany wzór (PT)](160904-rozpaczliwie-sterowany-wzor.html)

### Chronologiczna

* [160904 - Rozpaczliwie sterowany wzór (PT)](160904-rozpaczliwie-sterowany-wzor.html)

## Kontekst ogólny sytuacji
## Punkt zerowy:

Dracena jest niestabilna, po ostatniej akcji jej wzór jeszcze bardziej się pogorszył - jak Paulina się spodziewała. Walczą ze sobą aspekty: Diakonka, Wiła, Spustoszenie, pochłonięci ludzie, terminus Marek, urocza cybergothka, pająk astralny, dotknęła Paulinę. Za dużo. Sama Dracena jest silnie zagubiona w tym wszystkim. Mimo potencjalnych problemów wynikających z pacjentami, Paulina zdecydowała się skupić przez chwilę na Dracenie - jeśli jej się nie uda, czarodziejka może się zbyt wypaczyć.
Jednocześnie Paulina wie co nieco o Diakonach. Nie rozumie, dlaczego Dracena jest "sama". To najdziwniejszy przypadek jaki widziała. No i skąd ta "destabilizacja wzoru"?

Aktualny stan Draceny: (11 jeszcze do wydania), Diakon 4, Wiła 5, Spustoszenie 3, Pająk 1, Cybergothka 4, Strach 2

## Misja właściwa:

Dzień 1:

Dracena jest tak spokojna jak może być w tej sytuacji. Nic jej chwilowo nie odbija, co samo w sobie jest fajne. A Kinga i Paulina rozmawiają...

Kinga powiedziała Paulinie, że jest poważny problem - z jakiegoś powodu ona wykrywa zewnętrzną presję na wzorze Draceny; presję, która wzmacnia Diakonkę i Wiłę ponad wszystko inne (Paulina podejrzewa cruentus reverto). Ryzyko jest takie, że Dracena - nawet przy pomocy Kingi - może zmienić się w seksoholiczną wiłę; może przekształcić się w coś czym nie chce być. Kinga prosi o kontakt z członkami rodziny Draceny, jej oryginalnego źródła wzoru. Chce wzmocnić Dracenę. Jak nie, rozważa to by Dracena skupiła się na magii krwi. Cokolwiek co rozproszy silną presję zewnętrzną.

Sama w sobie Kinga zamierza destabilizować Wzór Draceny by się nie "zapętlił". Nie rozmontuje tego co jest, ale utrudni samoistną stabilizację. Kinga podparła - potrzebuje maga jej krwi. Rodu. Spokrewnionego z Draceną bezpośrednio.

Paulina niechętnie, ale poszła porozmawiać z Draceną... 

Dracena, niestety, jest krok dalej. Jest w trybie "pytałaś mnie gdzie jest zakopana forteca", choć Paulina nic o tym nie słyszała. Na kanałach Spustoszenia słyszy głosy, jest śmiertelnie głodna i paranoiczna. Odmawia jakiejkolwiek relacji z Draconisem; wyjaśniła Paulinie, że Draconis jest jej "strażnikiem". On jej nie kocha. On pilnuje, by nic strasznego się nie stało z uwagi na sekrety i mroczną wiedzę znaną Dracenie. To Dracena sama zaproponowała destabilizację wzoru; uciekła Draconisowi i wydostała się poza cały ten obszar, całą historię.

Dracena jest wściekła i przerażona. Z trudem pohamowuje się - wiła wygrywa. Zażądała od Pauliny jej krwi, jej energii. Chce osłabić wiłę. NIE BĘDZIE wiłą. Paulina zaproponowała osobę, która może pomóc - Netheria. Dracena zareagowała wściekłością - od Netherii się zaczęło jej zdaniem. "Netheria została odrzucona z tego powodu z którego Milena została zaakceptowana". Paulina chłodno żądała wszystkich szczegółów. Podważa to; pokazuje Dracenie, że ta jest w psychozie.
Dracena rzuciła się na Paulinę, złapała ją za gardło i podniosła. Ta zaczęła się dusić; Dracena się opanowała i ją odrzuciła. Wysyczała, że niech będzie, Netheria może być... może być...

Kinga przekazała Paulinie złe wieści. Mają 3 dni. Potem wzór się rozsypie lub ustabilizuje. Kinga jest w stanie to spowolnić; ale mają za mało czasu. Dracena nie może wiedzieć. Powiedziała też Paulinie, że jest coś w Dracenie czego nie ma w innych magach. Dracena jest szczególnie WAŻNA. Z nieważnymi magami takich rzeczy się po prostu nie robi.

Jak Paulina skończyła, Kinga dała jej propozycję - ma znajomego Mausa, który robi kryształy. Kryształy Mausów. Dałoby radę zdobyć kilka - w ekspresowym tempie - i ją uśpić (tymczasowo wyłączyć) oraz wzmocnić aspekt jaki chcą wzmocnić - cybergothka. Niestety, w takim czasie będzie to dość kosztowne...
Paulina się zgodziła. 

Paulina zadzwoniła do Netherii Diakon. Powiedziała, że to sprawa życia i śmierci i dotyczy jej kuzynki. Netheria się zdziwiła; nie wie nic o żadnej kuzynce która ma problemy. Paulina smutno zauważyła, że to norma w jej wypadku. Poprosiła Netherię o przyjechanie samotnie, z dużą ilością przydatnych rzeczy. Neti wzięła gigantyczną ciężarówkę Scania i powiedziała, że będzie wieczorem.
Na propozycję rekompenstaty, Netheria odpowiedziała, żeby Paulina nie była głupia; jakoś się dogadają i to rozwiążą.

Dracenę głód pokonał. Z upiornym spojrzeniem ruszyła ku Paulinie. Ta, widząc, że Dracena walczy szybko przesunęła siebie i Kingę nekromantycznie. Niewiele pomogło - Dracena rzuciła się na Paulinę - ale pomogło wystarczająco. Czarodziejka odskoczyła (a Paulina poczuła atak astralny (pająk), fizyczny (głód, seks), energetyczny (Spustoszenie, wiła)). Dracena się opanowała - w panice odpaliła coś z magii krwi co spowodowało kolejną krystalizację aury (niestety, nie tam gdzie chcieli) - ale już nie będzie miała napadów. Niestety, odnowiło jej się uzależnienie od magii krwi...

Paulina przytulała Dracenę. Nie jest "czymś", jest "kimś"...

Kinga obiecała, że kryształy będą na rano. Wieczorem przyjechała Netheria. W gigantycznej ciężarówce. Paulina z nią poruszyła temat Draceny i Netheria rzuciła kilka bomb:
- Dracena jest z rodu Blakenbauerów z nałożoną krwią Diakonów. Blakenbauerów jest silniejsza.
- Destabilizacja wzoru Draceny sprawiła, że krew Blakenbauerów wraca. Stąd runaway Wiły + Blakenbauer.
- Wprowadzenie krwi Diakonów uratuje Dracenę. Diakonka nie zmieni się w wiłę - choć tego nie widać po wzorze i sygnałach.
- Netheria pomoże jak tylko jest w stanie, ale nie ma niestety czystej krwi (jak na przykład Siluria).

Netheria powiedziała, że Draconis próbuje uratować Dracenę a Silgor nic nie wie. Czyli zrobił to totalnie przez przypadek. Z uwagi na to czym jest Dracena (i Antygona, i cruentus reverto) Dracena nie jest "w pełni" Diakonką - dlatego nie przez wszystkich traktowana jest jak jedna z nich. Ale ona pomoże jak tylko jest w stanie. A jest w stanie.

Netheria, Kinga i Paulina. Kinga powiedziała, że pomysł Netherii (wzmocnić Diakonkę) jest niebezpieczny. Netheria powiedziała, że sygnały tak wyglądają - ale tak naprawdę tam są wbudowane zabezpieczenia. Neti zaproponowała badanie wzoru Draceny Kindze. Kinga powiedziała, że to może skrzywdzić Dracenę. Neti przytaknęła - ale lepsze to niż śmierć Draceny bo zrevertuje do wiły. Decyzja jest u Pauliny.

Netheria powiedziała jeszcze jedną rzecz Paulinie - jest specjalne serum podawane nieudanym czy uszkodzonym kralothborn (Spojonym). To serum redukuje wpływ wszystkich 'natur' (-1 Diakonka, -1 wiła, -1 pająk, -1 Blakenbauer). Problem taki, że to ją wprowadzi w psychozę na pewien czas. Więc niewskazane; zwykle pilnuje takiej osoby dwóch kralothborn by upewnić się, że nie stanie się krzywda.
Paulina powiedziała, że kralothborn nie wchodzą w grę. Neti zaproponowała coś alternatywnego - Paulina wyłączy ciało (jako lekarz), Netheria przesunie Dracenę w astral a razem - Paulina i Netheria - degenerować ciało Draceny by walczyć z regeneracją. To zajmie sporo czasu, ale powinno pomóc. Netheria zaproponowała, by robić to pod sam koniec, nie wcześniej. Kinga zajmie się wtedy finalną ewolucją.

Paulinie się to nie podoba. Aczkolwiek ma zalety...
Zostały im jeszcze 2 dni.

Aktualny stan Draceny: (8 do wydania), Diakonka 4 (musi być 8), Wiła 5, Spustoszenie 4 (musi być 5), Pająk 1, Cybergothka 4, Strach 2, Weapon 2

# Progresja

* Paulina Tarczyńska: -1 void (na kryształy Mausów dla Draceny)
* Dracena Diakon: + głód magii krwi (słaby)

# Streszczenie

Stan Draceny się pogarsza; coraz gorzej się kontroluje. Szczęśliwie, jej wzór się stabilizuje. Niestety, łapie uzależnienie magii krwi i zdecydowanie dominuje wiła. Paulina ściąga Netherię do pomocy; ta zdradza, że Dracena jest adoptowana (jak i Antygona). Oryginalna matryca Draceny to był Blakenbauer - i jeśli wzór Draceny nie zostanie silnie przesunięty ku Diakon, Blakenbauer (jako silniejszy ród) wróci i Dracena zmieni się w viciniusa.

# Zasługi

* mag: Paulina Tarczyńska, stawiająca czoła wile w Dracenie, ściągająca Netherię i czująca się bezradnie w obliczu Skażenia Draceny.
* mag: Dracena Diakon, o której się dowiadujemy, że była krwi Blakenbauer i że Draconis próbował ją ratować; chcąc chronić Paulinę zakosztowała uzależnienia Krwi.
* mag: Kinga Toczek, lekko przestraszona rozwojem sytuacji ale zdeterminowana by uratować Dracenę mimo wszystko.
* mag: Netheria Diakon, ściągnęta przez Paulinę na pomoc Dracenie i zdradzająca kilka sekretów z przeszłości Draceny.

# Lokalizacje


1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Czarnoskał
                1. Marzanka, niewielka wieś agroturystyczna
                    1. Droga Gościnna, przy której znajdują się domki dla turystów i podróżników
                        1. Domek Zadumy, wynajęty przez Paulinę i Dracenę

# Wątki



# Skrypt

- brak

# Czas

* Dni: 1
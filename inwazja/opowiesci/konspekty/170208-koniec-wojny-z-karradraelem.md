---
layout: inwazja-konspekt
title:  "Koniec wojny z Karradraelem"
campaign: powrot-karradraela
gm: żółw
players: kić, dust
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [170125 - Przeprawa do Świecy Daemonica (HB, SD)](170125-przeprawa-do-swiecy-daemonica.html)

### Chronologiczna

* [170125 - Przeprawa do Świecy Daemonica (HB, SD)](170125-przeprawa-do-swiecy-daemonica.html)

## Kontekst ogólny sytuacji
## Punkt zerowy:

## Misja właściwa:

Dzień 1:

Hektor stoi na sali sądowej. Wygłasza płomienną mowę a Izabela Łaniewska rzuca się w jego kierunku... nagle Hektor czuje GŁOS. Eis. Eis go budzi i zmusza do działania. Hektor został Ranny przez Eis i Eis wyłączyła jego obrożę, wprowadzając go w berserk... i po chwili Hektor otworzył oczy. Jest w Laboratorium Mgieł, dookoła dwóch rannych naukowców i grupa terminusów. Eis uruchomiła z powrotem obrożę Hektora.

Tien Przodownik wyjaśnił, że są w kwarantannie. Laboratorium Mgieł szuka informacji o tym, czy to co się zdarzyło jest prawdą; nie spodziewali się, że Hektor wyrwie się z pasów, to było "niemożliwe" i to jedynie opóźni cały proces, niestety. W tej chwili Siluria (przywieziona w sarkofagu i wybudzana) i Hektor są w rękach biurokracji Świecy Daemonica.

* Spanikowałaś - Hektor, dyskretnie, do Eis
* Nie. Ty spanikowałaś. Ja Ci tylko w tym pomogłam - Eis, lekko

Po krótkiej serii dyskusji Przodownik powiedział, że to co zrobili Izie Łaniewskiej było podłe. Teraz decyzja jest w rękach biurokracji, nie jego czy kogokolwiek innego. I wyszedł, zostawiając Hektora i Silurię w kwarantannie... w międzyczasie Hektor spłukał z siebie krew.

* Aha, ta obroża... się dostosowała. Nie dam rady tego zrobić drugi raz - Eis, z lekkim smutkiem
* ... - Hektor
* Właśnie. Pomóż mi. Nie chcę być w pudełku. Nie chcę. Pomóż mi. - Eis, prosząco.
* Na razie nie wiem nawet gdzie i kim jesteś - Hektor, cicho
* Nie zapomnij o mnie. - Eis, z determinacją

Przyszedł do nich tien Metody Bankierz. Z obstawą terminusów i dwoma lekkimi pancerzami magitechowymi. Ma lekkie poczucie humoru, ale jest kwaśny, że Przodownik powiedział, że mogą pogadać tylko z "pachołem pachoła Sowińskiego" - jeden poziom nad Przodownikiem. Metody powiedział, że nie ma szerokich uprawnień; ale może dać im magitechy i zadekretować, że mogą w ramach kwarantanny poruszać się po Świecy Daemonica. I może im powiedzieć coś więcej.

Siluria powiedziała Metodemu, że trafili do Srebrnej Septy; stąd ślady Esuriit. I wstawiła się za Marianną i Konstantym. Zrobili więcej, niż można by oczekiwać. Metody się żachnął. Świeca nie zabija swoich. Oni potrzebują kwarantanny i ochrony.

* Kwarantanna przeciwko czemu, jeśli mogę spytać? - Siluria, zaskoczona
* Nie wiem. Musiałabyś porozmawiać z tien Przodownikiem. On ma uprawnienia, by powiedzieć - Metody, spokojnie.

Siluria przekazała Metodemu wiadomość o Mausach i Karradraelu. Ten powiedział, że Mausowie nie są problemem. Gdy Hektor spytał o to, jak porozmawiać z jego przełożonym, Metody powiedział, że Hektor musi napisać podanie. Hektor zna się na biurokracji... spytał jak urzędnik urzędnika, co należy zrobić. Jak wygląda tu sytuacja. Hektor ma +2 za Silurię (wysłanniczka KADEMu) i ona jest +1 za sympatyzowanie Metodego z Silurią i Hektorem. Metody ma (4), Hektor (2+3). F: Hektor zaciąga przysługę u Metodego. F: fragment Laboratorium się uszkodzi. S.

Metody wyjaśnił sytuację:

* Ogólnie, wiedzą, że i kto jest w Świecy. Wiedzą o Silurii (i że ma uprawnienia), wiedzą o Hektorze (i że jest ważny), wiedzą o uratowaniu z Esuriit.
* Kłócą się między sobą na wysokim szczeblu. Nie wiedzą co robić i jak można to wszystko przekształcić. 
* KTOŚ podjął decyzję o Esuriit. KTOŚ podjął decyzję o zamknięciu portali. KTOŚ uważa, że KADEM jest zbyt groźny. KTOŚ uważa, że Karradrael jest groźniejszy...
* Świeca Daemonica ma uszkodzone ekrany pryzmatyczne, więc się rozpadają jej komponenty - stąd kwarantanna
* INNI magowie wierzą, że kwarantanna jest wywołana przez chorobę. Stąd ekranowane magitechy
* Co gorsza, Hektor jest BARDZO niepopularny. Przez Izę.
* Włączony jest Ekran Ochronny Świecy. KADEM nie może namierzyć Silurii. Nie da się połączyć...

Laboratorium Mgieł zaczęło się destabilizować. Metody wsadził ich w Magitechy i kazał im wyjść z Laboratorium. Zaprowadził ich do Skrzydła Gościnnego, do odpowiednich sal. Poprosił, by się stamtąd nie ruszali i sobie poszedł...

Kilkanaście minut później, w pokoju z Hektorem, Siluria zobaczyła coś... dziwnego. Pokazała to Hektorowi (2+2v3). REMIS. Wykryta bomba; udało im się uciec zanim to wybuchło... a rozsadziłoby ich na kawałki. Wybuch. Gdy wypadli na zewnątrz, podbiegła do nich czarodziejka Świecy i ciągnie za sobą. Przedstawiła się jako Klaudia Bankierz. Chce ich zaciągnąć ze sobą; Hektor jednak ją zakrzyczał i powiedział, że idą do wierchuszki Świecy. Idą ich opierniczać. Klaudia się ucieszyła. Bardzo ucieszyła. Jasne, nie ma sprawy...

Ale jak się przedostać przez strażników? Klaudia... ma pomysł. Wyjmuje flaszkę z syberionem i... YOLO! Zespół próbuje ją powstrzymać. 3v4->remis. Klaudia odpaliła syberion i rozpoczęła się regularna WOJNA PRYZMATYCZNA. Korzystając z okazji Siluria i Hektor bez problemu wślizgnęli się na spotkanie. A tam - grupa magów...

Oliwier Sowiński, pontifex i lord Świecy Daemonica zapytał, czemu tu są. Hektor wyszedł od burzliwego tekstu, ataku na nich itp. Siluria poszła miękko, w pomoc i współpracę z KADEMem. Oliwier by nawet chciał wsparcia, ale robi co może, by tylko Świeca mu się nie rozpadła (wspiera, koordynuje itp na Primusie i Daemonica). Siluria zdecydowała się wpłynąć na innych magów Świecy - niech uda się doprowadzić do skupienia uwagi wszystkich na Primusie. 8v6->S. Udało się Silurii skupić wszystkich dookoła potencjalnego sukcesu.

Oliwier ich szorstko odesłał, ale Siluria widzi, że sytuacja została zmieniona. Dostali jako wsparcie... Konstantego jako terminusa ochronnego.

Gdy wracali do jakiegoś pokoju, ze ściany wypadła Saith Catapult. Dźgnęła Silurię trucizną i zwiała. Siluria padła na ziemię i zaczęła umierać... błyskawicznie pojawił się Agrest i Edwin; zablokowali teren. Postawili Silurię w stazę. Agrest nakrzyczał na Myszeczkę, że ten nie dał rady ochronić Silurii; następnie powiedział, że na to lekarstwo jest na KADEMie. Niestety, trucizna jest eksperymentalną bronią KADEMową.

To sprawia, że nie ma wyjścia. Niestety, trzeba wyłączyć Ekran Ochronny.

Edwin, Hektor, nieprzytomna Siluria, Renata Maus (jako ładunek) przechodzą przez portal Świecy na KADEM... operacja "TRANSFUZJA".

# Progresja


# Streszczenie

Świeca uśpiła Silurię i Hektora w Laboratorium Mgieł, próbując dojść do tego, co teraz robić. Eis wybudziła Hektora który wyciągnął Silurię. Razem przebijali się przez biurokrację; gdy tylko Agrest zrobił na nich fałszywy zamach, skorzystali z okazji. Siluria przekonała Wysoką Radę Świecy do współpracy z KADEMem a Agrest używając Saith Catapult zmusił Świecę do szybkiego przyzwania KADEMu. Renata Maus, Edwin, Hektor i umierająca Siluria trafili na KADEM...

# Zasługi

* mag: Hektor Blakenbauer, wybudzony przez Eis, ostro negocjował z biurokratami a w formie bestii wyrządził zniszczenia w Laboratorium Mgieł.
* mag: Siluria Diakon, przekonała Wielką Radę Świecy do sojuszu z KADEMem. Potem z woli Agresta - bardzo ciężko ranna, by wymusić pojawienie KADEMu.
* vic: Eis, która wybudziła siłą Hektora z Laboratorium Mgieł. Nie ma za dużej kontroli nad sytuacją; robi jako "angel AI".
* mag: Tomasz Przodownik, inżynier Wzoru w Laboratorium Mgieł; kontestuje biurokrację Świecy i nie lubi Hektora i Silurii, ale wykona rozkazy.
* mag: Konstanty Myszeczka, wysłany do ochrony Silurii i Hektora po to, by Saith Catapult pokazała jego miejsce ;-).
* mag: Metody Bankierz, sympatyzuje z Hektorem i Silurią, acz jest biurokratą i to widać. Hektor wisi mu przysługę. Powiedział Hektorowi i Silurii o co tu chodzi.
* mag: Klaudia Bankierz, jedna z dziewczyn Marcelina; anarchistyczna i niezależna dusza. Jest skłonna rozbić syberion na Fazie by zmusić Świecę do działania.
* mag: Oliwier Sowiński, pontifex Świecy i przełożony Świecy Daemonica. Pasują mu działania Hektora i Silurii mające wejść w sojusz Świeca - KADEM. Z jego rozkazu działa Agrest.
* vic: Saith Catapult, zabójczyni z rozkazu Agresta; dziabnęła Silurię trucizną z zaskoczenia i uciekła niezauważona.
* mag: Marian Agrest, dwulicowy łotr który z rozkazu Oliwiera Sowińskiego udaje, że "to wszystko wina reakcji w Świecy". Ciężko rani Silurię i wysyła Renatę Maus z Edwinem i Hektorem na KADEM.
* mag: Edwin Blakenbauer, ściągnięty wcześniej na Świecę Daemonica i trzymający Silurię przy życiu po ataku Saith Catapult.

# Lokalizacje

1. Świat
    1. Faza Daemonica
        1. Świeca Daemonica
            1. Krąg wewnętrzny
                1. Wielka Sala Tronowa, gdzie odbywała się narada możnych Srebrnej Świecy
            1. Krąg zewnętrzny
                1. Skrzydło Powietrza
                    1. Laboratorium Mgieł, gdzie Hektor i Siluria odbywali kwarantannę
                1. Skrzydło Wody
                    1. Pokoje mieszkalne, gdzie... podłożono bombę na Hektora i Silurię ;-)

# Czas

* Dni: 1
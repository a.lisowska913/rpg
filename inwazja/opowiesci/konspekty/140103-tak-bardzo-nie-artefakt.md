---
layout: inwazja-konspekt
title:  "Tak bardzo nie artefakt"
campaign: druga-inwazja
players: kić, dzióbek
gm: żółw
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [140103 - Tak bardzo nie artefakt(AW, WZ)](140103-tak-bardzo-nie-artefakt.html)

### Chronologiczna

* [120507 - Preludium: Historia świata](120507-preludium-historia-swiata.html)

## Misja właściwa:

Nie każdego dnia się zdarza, że do biura Andrei Wilgacz przychodzi młoda dziewczyna i pyta, czy to biuro detektyw Mordecji Diakon.

Zwłaszcza, że Mordecja Diakon nie żyje. Zaćmienie przerwało jej owocną karierę. A ta młódka wypytywała o nią, "bo przecież jej biuro zarejestrowane jest tutaj" zgodnie z rejestrem SŚ. I oczekiwała tanich usług! Dopytana, przedstawiła się jakimś fałszywym imieniem i nazwiskiem, w związku z tym Andrea i jej partner, Wacław Zajcew odesłali ją na "właściwy" adres - na cmentarz. Niech się mała pomęczy, nie chce mówić prawdy, niech się bawi.

Niedługo później w biurze pojawił się jeszcze jeden młody człowiek. Ten przedstawił się jako Mateusz Nieborak i spytał, czy to biuro... Mordecji Diakon. Ten jednak zapytany powiedział jak się nazywa, że jest członkiem Millenium i że potrzebuje pomocy detektywa. Tak więc tego nie wysłali na cmentarz a zdecydowali się wysłuchać (zwłaszcza, że ten nie oczekiwał tanich usług). No to im opowiedział...

Zaginęła kolia. Dziwna kolia, w kształcie kralotha z perłami, kiczowata i niezbyt ładna. Problem polega na tym, że ta kolia należy do niejakiej Krystalii Diakon, czarodziejki Millenium, która nie wie, że jej kolia zaginęła ;-).

Teresa Żyraf, młoda czarodziejka Millenium pożyczyła sobie kolię swojej mentorki i nauczycielki, bo jej przyjaciółka z SŚ (Emilia Szudek) była nią żywotnio zainteresowana. Mateusz, Teresa i Emilia byli na takiej dość hucznej imprezie w "Rzecznej Chacie" gdzie trochę popili, pobalowali, po czym pobawili się różdżką zamiany ciał i kolia została oddana osobie trzeciej.
Dowcip polega na tym, że kolia magiczna nie jest, ale nie da się jej magicznie zlokalizować, nie ma aury, oraz podobno "nie da się jej ukraść, trzeba oddać". No i Mateusz prosi Andreę i Wacława o odnalezienie owej kolii i oddanie jej jemu lub Teresie.
Dopytany, zaznaczył, że on nie jest magiem; on jest viciniusem Millenium, który kiedyś był człowiekiem. A Teresa jest uczennicą Krystalii Diakon. A dziewczyną wysłaną przez Andreę i Wacława "na drzewo" była właśnie Emilia ;-). Zdziwił się, że Emilia szuka kolii na własną rękę.

Andrea i Wacław poprosili Mateusza o przyjście z Teresą; na razie nie wyglądało na to, że to zlecenie ich zainteresuje. Biedować to nie biedują, a stopień trudności zlecenia jest bardzo wysoki a potencjalny zysk... mniej. W sensie: a co jeśli pojawi się bardziej interesujące i intratne zlecenie? Może ta Teresa coś wniesie do sprawy...

W międzyczasie (gdy Mateusz już poszedł) do biura wparowała Krystalia Diakon. Zywiołowa, energiczna, rozedrgana, wściekła. Powiedziała, że wie, że Teresa ukradła jej kolię. Ona zapłaci im trzy razy więcej niż Teresa, jeśli oddadzą kolię jej, Krystalii, z pominięciem Teresy. Detektywi zgrywali głupich, więc rozbiła jakiś wazon i wyszła, cała wściekła. Detektywi postanowili wziąć tą sprawę... w taki sposób, by maksymalnie uciemiężyć tą nienormalną Diakonkę.

Teresa potwierdziła wersję Mateusza. Młoda, nieco przestraszona lecz spokojna czarodziejka Millenium, zna się z Emilią "od dawna, bardzo dawna, połączyły ich zainteresowania i pasje" a z Mateuszem "od czasu jak asystuje Krystalii Diakon". Panicznie boi się Krystalii i jej reakcji. To ona wpadła na pomysł rotowania ciał; kolię zabrała, bo Emilia nie wierzyła, że ktoś może nosić takie szkaradztwo. No i Emilia była ciekawa, jak by jej ciało wyglądało w takiej kolii (podpowiedź: fatalnie) :-). Teresa powiedziała, że nikt nigdy kolii nie ukradł, bo jedynymi dwoma osobami kiedykolwiek ją noszącą była ona oraz Krystalia. Zapytani, wraz z Mateuszem zrekonstruowali kolejność przechodzenia kolii po ciałach:

ciało Teresy -> ciało Emilii -> ciało Teresy -> ciało Emilii -> ktoś obcy

Mając tą informację, wiedząc, że to się wydarzyło przedwczoraj oraz wiedząc, gdzie to miało miejsce ("Rzeczna chata") nasi detektywi z ciężkim sercem udali się do owej "Rzecznej Chaty". Trzeba zarobić i uciemiężyć podłą Diakonkę.

W "Rzecznej Chacie" ku swojemu zdziwieniu zobaczyli swojego "konkurenta". Jest to inny detektyw, Waldemar Zupaczka, też mag. Nosi kapelusz, prochowiec, ma tendencje do użalania się nad sobą i wiązania z trudem końca z końcem. Na głowie, o dziwo, ma bandaż. No i pije jakiś alkohol... Andrea i Wacław zdecydowali, że bezpieczniej będzie zrobić to sprytnie: Wacław pogada z Waldemarem i odwróci jego uwagę od Andrei a Andrea pogada z barmanką, Ofelią, i zajmie się sprawą.

Okazuje się, że Waldemar ma zlecenie na Teresę. Ale nie kolię - kolia jest dla niego nieistotna, jest jedynie premią. Z drugiej strony on dowiaduje się, że nasi detektywi mają zlecenie na kolię; Krystalii Teresa nie interesuje. Kto zatem dał zlecenie na Teresę? Niejaki tien Artur Żupan, drobny cinkciarz Srebrnej Świecy; podobno to on sprezentował swojej kochance, Krystalii Diakon, ową kolię. No i Waldemar był tak bardzo blisko złapania Teresy; byli jeden na jeden, ale ktoś walnął go w tył głowy czymś obuchowym. A on, Waldemar, był na tarczach... co nie pomoże jeśli tępy obiekt jest lapisowany. W głowie Wacława zapaliła się lampka alarmowa - to najpewniej był Mateusz, ale jak to, zwykły chłopak z Millenium miałby lapisowaną pałkę?

Tymczasem Andrea poznała okoliczności imprezy. Było na imprezie naprawdę sporo osób, ale ta trójka była dość charakterystyczna. Dowiedziała się też, komu została przekazana kolia. To tien Mateusz Krówka, zwany "elfem", młody mag lubujący się w łucznictwie i wszystkim elfopodobnym, stały bywalec "Rzecznej Chaty". Nazwisko Krówka też coś Andrei mówi - Malwina Krówka to czarodziejka specjalizująca się w spersonalizowanych systemach zabezpieczeń Srebrnej Świecy. Czyżby ta sprawa z kolią była jakoś powiązana z większą polityką...? W co oni się wpakowali?

Trzeba znowu porozmawiać z Mateuszem i Teresą >.>

Skontaktowali się z Mateuszem. Ten odpowiedział, że tak, znokautował jakiegoś maga dobierającego się do Teresy, ale to miało miejsce wcześniej niż do nich przyszedł. Z przyjemnością się z nimi spotka, ale bez Teresy. Teresa jest dobrze ukryta, nikt nie jest w stanie jej sensownie znaleźć. No dobra...

Czekając na spotkanie zostawili Waldemara Zupaczkę w "Rzecznej Chacie", zapijającego smutki, po czym udali się na umówione spotkanie z "elfem", w czasie, gdy ten miał przerwę w szkole łucznictwa. Ów mag był pod wrażeniem "wow, prawdziwi detektywi" i z przyjemnością odpowiedział na wszystkie pytania - akurat jego nie było w "Rzecznej Chacie" tego dnia, miał szlaban. Bo nagadał dziewczynie swojego brata, że ten brat ją zdradza (bardzo głupie). Brat nazywa się Radosław Krówka, dziewczyna Nela Welon.
No dobra, ale jeśli jego nie było, kto był? Elf odpowiedział, że sprawdzi rytuał elfiości; drogo kosztowało przygotowanie i zrobienie takiego rytuału, ale było to tego warte. On nie tylko wygląda jak elf, staje się elfem, łącznie z podpowiadaniem w głowie, wsparciem do łucznictwa itp.
Zdaniem detektywów to Radosław, czyli brat, przebrał się za elfa i chciał się zemścić. Sounds legit. No bo skoro ten młody (Mateusz Krówka) nic nie wie o kolii i go faktycznie tam nie było...
A też okazało się, że Malwina Krówka, czyli ekspert od zabezpieczeń to matka Radosława i Mateusza Krówek... czyżby złożona super-intryga mająca na celu ukradzenie czegoś ważnego związanego z systemami zabezpieczeń Krówek? I jaka w tym rola kolii? Czym jest ta kolia w tym momencie?

Przeszli przez "Rzeczną Chatę" i Ofelia potwierdziła - to na pewno był "Elf". była ta elfia aura, było elfie zachowanie, choć dziwne, że nie miał łuku i że pił sporo alkoholu; Mateusz Krówka rzadko pije alkohol, jeśli w ogóle. Więc tak... teoria "złego brata" się zaczyna potwierdzać. Tymczasem okazało się, że Waldemara Zupaczki nie ma już w "Rzecznej Chacie". Wziął detoks i poszedł gdzieś sobie.

Próbując dojść do tego czy ktoś nie próbuje ich wrobić, Andrea sprawdziła w rejestrach SŚ kto zmienił ich biuro na biuro Mordecji Diakon. Odpowiedź przyszła w miarę szybko - to był... Waldemar Zupaczka. Najpewniej po pijanemu się wściekł, że znowu nie ma klientów bo mu podebrała Andrea i po pijanemu zemścił się po swojemu. Czyli nie, to nie jakieś tajne hasło czy skomplikowane działania... to jest nie powiązane z całą tą złożoną superintrygą.

Przybył do ich biura Mateusz Nieborak. Szybko został zapytany gdzie jest Teresa. Okazało się, że jest schowana w szopie za sztuczną ścianką jako martwy pająk. Eliksir przemiany i eliksir spowolnienia funkcji życiowych. Całkiem skuteczna forma schowania... całkowicie niedostępna zwykłemu magowi, a co tu mówić o zwykłym viciniusie Millenium. Nnnno dobra. Poprosił o dodatkową płatną usługę - o chronienie Teresy przed Krystalią Diakon i jej agentami. Tu, Wacław błysnął sprytem - "przed Krystalią, tak?", no bo przecież Teresę ściga Artur Żupan, nie Krystalia Diakon, ale Mateusz Nieborak szedł w zaparte. Lol. No dobra, da się załatwić - ale i tak muszą spotkać się z Teresą, rozwiązać kilka spraw i upewnić się co do niektórych rzeczy. Nie ma problemu. Wsiadają w samochód i jadą.

A tu - paskudna niespodzianka. Niedaleko miejsca gdzie schowana jest Teresa znajduje się samochód Waldemara Zupaczki. Albo już ją znalazł, albo jest blisko. Dobra, detektywi natychmiast dzwonią do straży miejskiej "taki a taki samochód zaparkował na miejscu dla niepełnosprawnych i pan Zdzisiu nie może wjechać do domu". Dodatkowo Wacław przygotował fałszywy znak i fałszywe linie o miejscu dla niepełnosprawnych i wyczarował je tak, by samochód faktycznie łamał zasady. Po tym zdecydowali się poczekać - albo Zupaczka znajdzie Teresę i przyjdzie z nią do samochodu (i mogą zareagować), albo straż zwinie samochód; a zwykle magowie mają jakieś systemy na samochodach, by mogli reagować.

I faktycznie, straż przyjechała. Jak zabrali się za lawetowanie samochodu, wypadł Zupaczka cały rozchełstany i nerwowy. Chciał się wybronić (gołym okiem widać, że coś tu jest nie tak), ale trafił na strażnika, któremu wcześniej kiedyś podpadł... i tak biedny musiał jechać z nimi.

W szopie, w której schowana była Teresa były jeszcze ślady odpowiednich rytuałów. Andrea się im przyjrzała i zrozumiała, czemu Zupaczka jest tak skuteczny w odnajdywaniu. Mianowicie, jeśli już raz coś namierzył, buduje sobie obraz astralny danego obiektu czy osoby, potem dopytuje duchy (astralne, nie nekromantyczne) w ich języku gdzie znajduje się ów obiekt. Wykorzystuje sympatię. To powoduje, że w pewien sposób szuka wg. "ciepło zimno", ale niezależnie od formy, transformacji, stanu itp obiektu - jak długo ma możliwość użycia sympatii, obiekt szukany nie jest w stanie mu uciec.

Czyli znalezienie Teresy to kwestia czasu...

Szczęśliwie, tym razem nie znalazł Teresy na czas. Zdążyli w ostatniej chwili. Wrócili z Teresą do biura Andrei, wiedząc, że to tylko kwestia czasu. Jak uchronić się przed poszukiwaniami w taki sposób? Teresa musi być w ciągłym ruchu - w ten sposób Zupaczka nie jest w stanie uzyskać triangulacji. No dobra, wysyłamy ją na nocną jazdę tramwajem czy autobusem czy coś, ma całą noc jeździć...

W międzyczasie detektywi dostają telefon od Mateusza Krówki. Okazuje się, że tak, ktoś używał rytuału elfiości. Dwa razy. I to nie był Mateusz Krówka.

Zanim Teresa zacznie jeżdzić w kółko, musi odpowiedzieć na kilka pytań. O co tu, do cholery, chodzi. Detektywi pytają Teresę samą, Mateusz Nieborak nie jest w pobliżu. Chcą dowiedzieć się jej wersji prawdy, by potem odpytać Mateusza samego.

Więc, czas przepytać Teresę. Najpierw o Artura Żupana - kto to, jakie ona ma z nim relacje? Okazuje się, że Artur Żupan jest magiem świadczącym drobne usługi różnym postaciom, min. Krystalii Diakon. Tam właśnie spotkał Teresę i chciał rozszerzyć swoje usługi o kolejną osobę. On właśnie sprzedał Teresie różdżkę służącą do zamieniania ciał. Rzecz niekoniecznie bezpieczną; artefakt taki nie powinien być powszechnie używany, ale nie od razu super regulowany; tak czy inaczej, mógłby mieć jakieś problemy z terminusami jakby wpadł w niewłaściwe ręce... np. takie, jak pijanej Teresy.
A kim jest Mateusz Nieborak? Jest to agent Millenium, pracujący często dla Krystalii Diakon. Tam się poznali; jest od niej o dwa lata starszy, więc jest o czym z nim gadać. Słucha poleceń magów, osoba wesoła i sympatyczna, trudno go nie lubić.
O kolii nic więcej nie wie. Często ją nosiła, ale krótko - zawsze Krystalia jej dawała kolię i kazała jej oddać tą kolię.
Rodu Krówek nie kojarzy; nie wiedziała nawet, kim jest "elf" czy Malwina Krówka.

Nnnno ok, czas porozmawiać z Mateuszem Nieborakiem. Ale jak go ruszyć? Andrea zaproponowała niezbyt modne rozwiązanie - kawę na ławę. Ok, można tak spróbować.

Mateusz Nieborak nie stawiał oporu. Z przyjemnością odpowiadał na pytania. Stwierdził, że nie zdradził Krystalii i nie uważa, by Krystalia tak na to patrzyła, że ją zdradził. Po prostu, chwilowo bardziej potrzebuje go Teresa. Wyjaśnił też swoją rolę w tym wszystkim - Teresa jest młodziutką asystentką Krystalii i on, Mateusz, pozwala jej popełniać błędy i się uczyć na swoich błędach w warunkach w miarę kontrolowanych. Owszem, to wszystko wyrwało się spod kontroli, ale nie stało się nic nieodwracalnego, choć niepokoi go ta kolia. Jest agentem Millenium mniej więcej na poziomie Krystalii; nie jest jej podległy ani jej nadrzędny. Teresa jest podrzędna Krystalii. To tłumaczy, czemu ma do dyspozycji takie a nie inne środki.
Mateusz powiedział też, że Artur Żupan jest po prostu dostawcą; na pewno nie jest kochankiem Krystalii i nie miał nic wspólnego z kolią. Kolia została przez Krystalię wyhodowana własnoręcznie. Zapytany dokładniej o hodowlę wyjaśnił, że Krystalia Diakon hoduje biżuterię (stąd imię) i kolię sformowała własnoręcznie, dla siebie.

Tu do detektywów dotarło, że kluczem do tajemnicy kolii może być Krystalia Diakon i dopytali Mateusza o szczegóły na jej temat. Krystalia jest ćpunką, uzależnioną od różnego rodzaju środków skandalistką; ta kolia jest bardzo w jej stylu.

Tu detektywi mieli moment facepalma. Najpewniej kolia nie jest artefaktem, ale jest żywą istotą. Istotą, która wydziela różne narkotyki i inne środki by Krystalia mogła być na stałym haju. I to tłumaczy jej zachowanie gdy była w ich biurze - nikt nie lubi być na głodzie. To też oznacza, że "kradzież" kolii może być niebezpieczna i że ekspozycja na kolię może być niebezpieczna sama w sobie... i to wreszcie tłumaczy wszystko.

Ciało Teresy przekazuje kolię Emilii. To ciało przywraca. Przekazuje znowu Emilii. Mamy kontakt Emilii z kolią, co jakkolwiek nie jest szczególnie szkodliwe już posiada jakieś skażenie narkotykami. Następnie przekazane jest to najpewniej bratu "elfa", który może być już w bardzo złym stanie; Teresa nigdy nie nosiła kolii długo, jedyną która to kiedykolwiek robiła jest Krystalia Diakon. A tu już leci drugi dzień!

Mimo nocy, Wacław dzwoni do Radosława Krówki. I czas na opisanie tego wątku:

Całą noc Radosław Krówka był na silnym haju wynikającym z działania kolii. Ale był dzielny, nie zdradził swojej dziewczyny z nikim, choć miał niesamowite ciągoty (kolia w końcu zawiera elementy kralotha). Był w różnych miejscach, bawił się świetnie, ze śladami niesamowitych imprez udał się nad ranem do domu Neli się z nią spotkać... a ta widząc na nim ślady szminki, jakąś kolię na szyi i wyraźne pijaństwo i rozpustę w płacz (że ją na pewno zdradził) i wywaliła go na zbity pysk, odbierając kolię (zrywając ją z szyi). Radosław, nie do końca świadom co się dzieje błąkał się po mieście, po czym zapadł w sen.

Emilia poszła na cmentarz i nie znalazła tam Mordecji Diakon. Albo znalazła, nieważne. Grunt, że skontaktował się z nią (ocknięty) Radosław, chcący się z nią spotkać i oddać kolię (powiedzieć, że on ma jej kolię i ją odda). On ze skażeniem kralotycznym i ona ze skażeniem kralotycznym... skończyli w łóżku.

I wtedy zadzwonił Wacław.

Radosław cały załamany, nie powiedział gdzie się znajduje, ale powiedział, że Nelka odebrała mu kolię. Powiedział też, gdzie Nela mieszka i gdzie ma laboratorium. To wystarczyło; Mateusz Nieborak natychmiast skontaktował się z Krystalią, by ta powiedziała mu o co chodzi z tą kradzieżą i co wtedy kolia robi. Krystalia z radością powiedziała, że kolia ma wbudowany fajny narkotyczny system obronny - wpierw silne uzależnienie, potem ciąg wysoki, potem ciąg niski, z depresją, myślami samobójczymi i wszystkim (ale odbierając energię do życia i uniemożliwiając działanie), po czym kolia coraz silniej działa na ofiarę, uzależniając i degenerując ją, by ta przyszła do niej, Krystalii, błagać o łaskę i przebaczenie. Fajnie, nie?

...detektywom się to mniej spodobało.

Mateusz Nieborak poprosił Krystalię o pojawienie się. Jeśli ktokolwiek jest w stanie coś zrobić z samą kolią, będzie to właśnie ona. Tymczasem Wacław i Andrea odkryli, że Nela się zamknęła w swoim laboratorium technomantycznym i uruchomiła pełną moc systemu zabezpieczeń (jedyna cenna rzecz to właśnie to; ona jest asystentką Malwiny Krówki, dla odmiany).

Nnno ok, jakoś trzeba ją stamtąd wydobyć. Jej organizm jest już dość spustoszony przez kolię a, zgodnie z relacją "elfa", jeśli matka by się dowiedziała to mogłaby Nelę wywalić. Ona i tak niezbyt aprobuje związek jej brata z Nelą. I na te dywagacje przyjechała Krystalia, oczywiście na haju. Mateusz Nieborak jest już na miejscu.

Krystalia jest cała rozpromieniona - jej system defensywny zadziałał właściwie. Diabli wiedzą, co wzięła - jest jednak cała rozanielona i chętna do pomocy. Ma przy sobie perłę, sympatią powiązaną z kralotyczną kolią. Przy użyciu tej perły zaczęła wprowadzać Nelę w stan odpowiedni - spokojniejszy, powolniejszy... ogólnie, próbowała zrobić z Nelą coś, by dało się ją z tego wyciągnąć.

Andrea zaczęła rozmowę z Nelą. W tej rozmowie wyszło, że Nela jest tylko asystentką i bardzo by chciała zostać samodzielną czarodziejką zajmującą się systemami zabezpieczeń. Andrea próbowała ją skusić właśnie tym, pochlebstwem i marzeniami Neli. Niestety, nie udało się tego osiągnąć. Nela nie zrozumiała, co Andrea miała na myśli odnośnie rozbrojenia systemu by Andrea mogła kupić od Neli ten system. Była zbyt rozproszona by argumentacja Andrei mogła zadziałać.

Krystalia zaproponowała inne rozwiązanie - ona umożliwi bezpośredni link zaklęciem do Neli i ktoś z magią mentalną poradzi sobie z rzuceniem na Nelę czaru. Wacław zgłosił się na ochotnika. Krystalia złączyła kolię z ciałem Neli używając odpowiedniego polecenia, po czym udostępniła perłę jako element sympatyczny Wacławowi. Co prawda operacja wymagała połączenia kolii z układem nerwowym Neli (i jest operacją "nieodwracalną" poza laboratorium), zdaniem Krystalii jest to tego warte (i nic nie powiedziała Andrei ani Wacławowi). Wacław rzucił zaklęcie mentalne na Nelę, każąc jej wyjść na zewnątrz. Zaklęcie się udało...

-----

Nela trafiła na badania i celem regeneracji do Krystalii. Ta powiedziała detektywom, że zasłużyli na nagrodę od niej - pod warunkiem, że nic nie uderzy w Teresę. Krystalia wszystko bierze na siebie.

Detektywi dostali odpowiednie plastry i inne takie do detoksyfikacji. Mają uleczyć osoby powiązane jakoś z kolią.

Krystalia odwołała Artura Żupana ze zlecenia na Teresę (tak, to ona za tym stała). Waldemar ma dostać jakąś tam niewielką sumę pieniędzy za zerwanie umowy. Och straszne.

Bracia mają coś zrobić, by ochronić Nelę przed matką.

Teresa przyszła podziękować detektywom. Jej zdaniem, Krystalia nic nie wie. Jest tak bardzo szczęśliwa, że udało im się przekazać kolię Mateuszowi i że mimo srogiego upomnienia skończyło się to dla niej dobrze...

# Zasługi

* mag: Andrea Wilgacz jako detektyw szukająca kolii w kształcie kralotha by zdrażnić jej właścicielkę.
* mag: Wacław Zajcew jako detektyw który szuka kolii i zupełnie nie wygląda jak Mordecja Diakon.
* mag: Mordecja Diakon jako martwa od jakiegoś czasu czarodziejka służąca za pretekst dla młodych magów.
* czł: Mateusz Nieborak jako członek Millenium starający się chronić swoje koleżanki.
* mag: Teresa Żyraf jako uczennica Krystalii Diakon i osoba odpowiedzialna za kolię, którą utraciła.
* mag: Krystalia Diakon jako właścicielka zaginionej kolii i patentowa ćpunka (też: niestabilna psychicznie).
* mag: Emilia Szudek jako uczestniczka niesamowitej imprezy i czarodziejka SŚ próbująca zaimponować.
* mag: Waldemar Zupaczka jako konkurencja Andrei i Wacława robiąca złośliwostki za co zapłacił lawetowaniem auta.
* mag: Mateusz Krówka jako elfi potomek rodziny Krówków chętnie współpracujący z detektywami
* mag: Radosław Krówka jako brat podszywający się pod elfa chcąc zrobić mu numer; nieświadomy tego co z tego wyjdzie.
* mag: Nela Welon jako praktykantka u Krówków i dziewczyna Radosława, która zabiera kolię nie należącą do niej.
* mag: Ofelia Caesar jako barmanka "Rzecznej Chaty". Wszechobecna i nic nie mówiąca bez potrzeby.
* mag: Artur Żupan jako "tani drań" świadczący usługi (nie tylko) Krystalii

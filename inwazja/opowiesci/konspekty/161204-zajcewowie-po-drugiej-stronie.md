---
layout: inwazja-konspekt
title:  "Zajcewowie po drugiej stronie"
campaign: powrot-karradraela
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [160713 - Jak ukraść ze Świecy Zajcewów (AB, DK, HB)](160713-ukrasc-ze-swiecy-zajcewow.html)

### Chronologiczna

* [161120 - Tak wygrywa się sojuszami! (AW)](161120-tak-wygrywa-sie-sojuszami.html)

## Kontekst ogólny sytuacji
## Punkt zerowy:
## Misja właściwa:

Dzień 1:

Bez presji. Andrea ma TEN i fragment następnego dnia na odebranie bandy Zajcewów z potencjalnie bardzo bojowej i niebezpiecznej sytuacji. I nawet nie ma postawionego "gdzie"...

Rano obudziła Andreę Lidia, z wyraźną radością. Udało jej się obudzić i naprawić Dalię. Mają lekarza. Rafael zaproponował Andrei pastylki pobudzające; terminuska odrzuciła (dla bezpieczeństwa). Pierwsze, co Andrea robi - idzie odwiedzić Dalię. Dalia przyjęła Andreę z wyraźnym entuzjazmem - z radością zrobi wszystko dla Andrei. Jest pobudzona i energiczna. Lidia przekazała hipernetem Andrei, że niestety, Dalia jest na pastylkach Rafaela... były potrzebne by postawić Lidię na DZISIAJ. Andrea westchnęła. Nie ma wyjścia... niech Lidia wyjaśni Dalii o co chodzi - chcą mieć Rudolfa ASAP.

Mieszko przekazał Andrei, że ktoś zbliża się do bazy. Friend-or-foe wysłali jako Millennium, Melodia Diakon. Czy Andrea coś wie na ten temat? Andrea szczęśliwa, dobrze mieć posiłki.
Melodia z gracją i radością przywitała się z Andreą. Kraloth dyplomatycznie się nie ruszał.

"To maleństwo to Laragnarhag! Nininini!" - Melodia, tarmosząc kralotha
"..." - kraloth, patrząc czułkami na Melodię

Mieszko po hipernecie wysłał sygnał do Andrei, że nie podoba mu się ten kraloth. Wygląda... kralothowato. A oni mają dziewczyny. I Barana. Andrea zaakceptowała tą informację, ale jest ten kraloth jej potrzebny. Wysłała kralotha do "kralociego pokoju".

Andrea porozmawiała z Melodią. Poprosiła, by ta zajęła się Tadeuszem Baranem i jego problemami z seksem / zdrowiem. Melodia się uśmiechnęła. Zajmie się tym z przyjemnością, zwłaszcza jeśli Rafael przepnie Andreę na Melodię. Andrea się nie zgodziła - niech Baran nadal będzie w niej zakochany; szkoda chłopa a dodatkowo każda zmiana biomagiczna jest niebezpieczna. ZWŁASZCZA w wykonaniu Rafaela.
Melodia powiedziała, że to nadal nie problem. Wzięła to ambicjonalnie.

Andrea wysłała Melodię, Pszczelaka i Skrzydło po emiter hipernetowy (w ciężarówce). Ma nadzieję, że Melodia da radę złamać/omamić/przekonać mechanika samochodowego. Melodia obiecała, że wieczorem przyjedzie ciężarówką (i osiągnęła niesamowitą wartość 24). Andrea założyła, że Melodia wie, co robi i skupiła się na reszcie bazy.

Andrea poszła spotkać się z Dalią. Ta siedzi, cała radosna i... konferuje z Rafaelem i Laragnarhagiem. Andrea zauważyła też, że Kajetan ich obserwuje cały czas, wyraźnie nie ufając kralothowi. Andrea ostrzegła Dalię, że następnego dnia pojawią się ranni; ta radosna powiedziała, że nie ma problemu i z radością się nimi zajmie. Andrea zauważyła, że Dalia przez te tabletki ma nienormalnie dobry humor. Cóż, przynajmniej ktoś. Szczęśliwie, nadal jest wystarczająco profesjonalna i skuteczna.

Rafael przechwycił Andreę i zaproponował jej, że on, Dalia, Lidia i Laragnarhag mogą postawić do jutra Rudolfa. Zajmie to cały dzień i trochę surowców ale jest duża szansa powodzenia (70%). W najgorszym wypadku po prostu nic złego się nie stanie. A najpewniej Rudolf nie będzie potrzebował nawet pastylek takich jak dostała Dalia.
Rafael powiedział Andrei, żeby ta nie pytała Dalii o estymację. Dalia ma teraz tendencje do bycia zdecydowanie hurraoptymistką. Tak działają te pastylki. I tak lepsze to niż to co zrobił Baranowi, jak zauważył.

Gdy Rafael się oddalił, Andreę poprosił o spotkanie Kajetan. Kajetan powiedział, że nie uważa by był Andrei już potrzebny. Siłę ognia zapewnia Krąg Życia oraz Millennium (przez komunikację kralotyczną i opcję teleportacji kralotycznej). Kajetan nie widzi dla siebie roli - a bardzo, BARDZO nie podoba mu się obecność kralotha i to, że Rafael tak blisko jest kralotha i jeszcze faszeruje jego kuzynkę jakimiś pastylkami. Jego zdaniem Lidia nie jest tu wiarygodna. Kajetan jest terminusem chroniącym ludzi i walczącym z potworami - jakkolwiek docenia ruch Andrei wobec Kręgu Życia, uważa, że Krąg negatywnie wpłynie na ludzi na tym obszarze. Obecność kralotha natomiast... lekko przepełnia czarę.

Andrea zgodziła się z nim, że jej siły się zwiększyły i to nie w sposób jakiego by chciała. Twierdzi, że potrzebuje Kajetana, bo on ma wiedzę o ludziach. Kraloth jest tu tylko tymczasowo, do momentu przechwycenia grupy Zajcewów. Potem opuści to miejsce. Kajetan zauważył, że kraloth będzie robił kralotyczne rzeczy gdzieś indziej. On nie chce tego wspierać - współpracy z kralothami, pozwolenie viciniusom na eksploatowanie ludzi. Krąg Życia i kraloth... po prostu dla niego to temat nie do przeskoczenia.

Andrea uderzyła w nuty pragmatyzmu. Powiedziała, że nie miała innego wyjścia by powstrzymać tego wroga z którym walczą. Plus - oszukała go - przenegocjuje temat z Millennium i kralothem oraz z Kręgiem Życia. Zapewni bezpieczeństwo ludziom na tym obszarze i ogólnie. Przekonała go. Niechętnie, ale Kajetan zostanie i będzie Andreę wspierał. To daje największą szansę na ochronę wszystkiego i wszystkich przed Spustoszeniem i przed wszystkimi problematycznymi sytuacjami i viciniusami.

Andrea na wszelki wypadek tak go przesuwa by nie miał nic wspólnego z viciniusami i cieszy się w duchu, że Kajetan nie wie, że kazała zniszczyć ten samolot by uratować EAM...
Korzystając z okazji, chwilowo poprosiła Kajetana by chronił Jankowskiego, Dalię i Lidię. Tak na wszelki wypadek (paranoja)...

Potem Andrea odwiedziła Mariana Łajdaka. Czarodziej kombinował nad czymś; słysząc Andreę uśmiechnął się do niej. 

"Mam zlecenie pasujących do twoich... specjalnych talentów" - Andrea
"Mam ich mnóstwo, lady terminus. Których?" - uśmiechnięty Marian
"Szmuglerskich" - Andrea, spokojnie
"To potwarz" - Marian z pokerową twarzą
"Możemy się krygować lub rozwiązać problem" - Andrea, uśmiechnięta
"Mogę załatwić różne rzeczy, ale są całkiem legalne" - Marian, podobnie uśmiechnięty

Andrea poprosiła Mariana o załatwienie czegoś do walki ze Spustoszeniem oraz do uniewidzialnienia od Spustoszenia. Ona nie wie dość; chce coś, co da im przewagę. Marian spytał, czy może powołać się na NIEOFICJALNE zlecenie. Jest coś... Marian powiedział, że jego znajomi znają znajomych którzy znają potężnych magów którzy by bardzo docenili, gdyby Świeca przymknęła oczy na niektóre tematy i niektóre kanały przerzutowe... nawet, jeśli na jakiś czas. Marian mówi o kanale Niemcy - Ukraina. Ale za to jego znajomi są naprawdę skłonni zapłacić. Więc Andrea dostałaby potężnego sojusznika, potężne bronie i sprzęt.

Andrea się zgodziła pod warunkiem, że Marian powie im "trzy miesiące LUB Andrea przestaje być Lady Terminus". Marian powiedział, że mogą się nie zgodzić - ona może przestać być Lady Terminus nawet następnego dnia. Andrea powiedziała, że jak ma być brutalna, to jak Świeca ją odsunie to Andrea nic nie może zrobić. Łajdak powiedział, że spróbuje coś z tym zrobić i coś z tego wypracować. Powiedział, że bierze Opla Astra i jutro wróci. Andrea się zgodziła.

Andżelika dostała zadanie przyjęcia nowych magów. Pokoje, kwarantanna, inne takie. Niech ona zinwentaryzuje i zajmie się ogólnie rozumianą logistyką. Niech Andrea nie ma tego na głowie.

Andrea porozmawiała z Mieszkiem o kralocie. On powiedział, że pochwala jej decyzję; walczy tym co ma. Andrea dała Mieszkowi dzikie zadanie - wydobyć od kralotha informacje odnośnie tego, co się stało w Kompleksie Centralnym i jak można to wykorzystać. Niech Mieszko znajdzie jakiś słaby punkt przeciwnika. Coś, co podeprze ich potencjalną strategię. Mieszko uniósł brwi, ale się zgodził. Zajmie się tym. W końcu nie takie rzeczy już robił (terminus black ops, ma dużo na sumieniu).

Zadaniem Barana jest zintegrować się z biomózgiem. Ma odbierać sygnały i de facto stanowić superkontroler bazy i okolic. Organiczna końcówka. A przy okazji będzie miał okazję popracowania chociaż chwilę z Andreą, co zdecydowanie poprawi mu humor i będzie stanowić jakąś różnicę. A przynajmniej - dopóki Melodia się za niego nie zabierze ;-).

Wieczorem wróciła uśmiechnięta Melodia z Pszczelakiem i skrzydłem szkoleniowym. W ciężarówce. Powiedziała, że wypożyczyła na dwa tygodnie - ma nadzieję, że wystarczy ;-). Jak co i jak odda po miesiącu, też nikt nie będzie się gniewał a jak będzie trzeba, to pojedzie jeszcze raz ;-).

Zdaniem Dalii i Lidii rokowania Rudolfa są bardzo dobre. Fizycznie jest w dużo lepszym stanie niż myślały. Lidia jednak martwi się ośrodkiem osobowości; nadal jest odcięty jako zabezpieczenie przed wypaleniem rozkazem samozabicia. Ale Lidia jest w stanie to obejść, po prostu niekoniecznie od razu... czyli Andrea dostanie broń, nie maga. Terminusa - zabójcę wydziału wewnętrznego. Bezmyślne acz posłuszne narzędzie. Andrei się to nie do końca podoba, ale lepsze to niż bezwładne ciało w wypadku potencjalnego ataku. Tylko nie dać go Spustoszyć (choć nie włada magią, więc jedyny sposób to bezpośrednio).

Dzień 2:

Rudolf jest fizycznie sprawną maszyną do zabijania. Zdaniem Lidii, przejdzie mu w ciągu tygodnia. Ale już coś - i słucha Andrei (i tylko Andrei). Andrea westchnęła. Cóż... Rafael, Lidia i Dalia...

Marian przekonał swojego "partnera" do współpracy (25!). Wrócił do Andrei. Podał jej stare radio tranzystorowe. Odezwał się męski, ciepły głos. Kirył Sjeld. Powiedział, że Andrea zawróciła Marianowi w głowie a to się zawsze chwali. Kirył powiedział, że owe radio jest brudnym przekaźnikiem. Położy, ustawi odpowiednią częstotliwość a czołg przyjedzie. Brudność przekaźnika polega na tym, że prowadzi na Esuriit. Musi przyjechać czołgiem przez Esuriit... ten przekaźnik jest... specjalny. Sam czołg to bydlę. T-42-class, zmodyfikowany. Sześć osób (nominalna załoga: 14) w czołgu. Duży luk bagażowy - z granatami.

Andrea się zdziwiła. Luk bagażowy... w czołgu?

Marian dał Andrei do zrozumienia, że to NIE jest czołg. To jeżdżąca forteca. Nosi nazwę "Stalowy Śledzik Żarłacz".

Kirył powiedział Andrei, że od Mariana dowiedział się, że Andrea ma w pobliżu koszmarnie wielke źródło energii Magii Krwi i Pryzmatu - prawie Syberię. Andrea potwierdziła. Kirył polecił jej, by ta odpaliła radio tam i Skaziła obszar syberionem który dostanie od Mariana. I Śledzik przyjedzie. I ma swoją dywersję... a on, Kirył, nie chce wiele. On chce jedynie poprzenosić niektóre artefakty i byty w okolicy.

Kirył powiedział Andrei, że ważne jest to, że radio musi dostać autoryzację Lady Terminus.
...to Andrei coś PRZYPOMNIAŁO. Słyszała o T-42 które były domami nomadów syberyjskich. Klanowe czołgi ładowane syberionem.
...co przypomniało jej legendę o tym, jak jeden z czołgów został w pijanym amoku wysłany by rozwalić Fazę Esuriit. Oczywiście, nie wrócił.

...czy Kirył chce, by Andrea przywołała mu czołg z Fazy Esuriit, który kiedyś jego ród zgubił? Andrea przywołała hipernet; Kirył jest dość sympatycznym nomadem-bez-czołgu który żyje na obrzeżach Syberii (bo nie ma swojego czołgu). Dowodzi handlowcami i ma mentalność "wszystko co nie jest przybite do ziemi może być moje". Dotrzymuje umów. Gdzieś w historii jego rodu jest czołg. Może ten zaginiony... przynajmniej Kirył często opowiada o "zagubionym czołgu". Ten czołg o którym mówimy uciekł na Esuriit. Przodkowie Kiryła nie chcieli wpaść w ręce Świecy i wierzyli, że uda im się uciec.
Mylili się, Esuriit ich pożarł. Magowie Świecy widzieli Energiaki. Ale czołgu nie odnaleziono. Jego szczątkowe AI działało.

Andrea przydusiła. Widzi co Kirył próbuje zrobić. Jej zdaniem odzyskanie czołgu jest więcej warte niż sama pojedyncza akcja wykorzystania tego czołgu. Plus, czy czołg w ogóle Kiryła zaakceptuje?
Kirył się zaśmiał. Marian miał rację. Czołg spędził sporo czasu na Fazie Esuriit i ogólnie powinien być już dość zahartowany na energię Esuriit. Andrea powiedziała, że ściągnie mu ten czołg - ale on odpowiada za problemy z Maskaradą i za to, by jak najmniej ludzi uszkodzić.

"Andreo, w tym czołgu jest mnóstwo broni. I nielegalnych, potężnych zabawek." - Kirył
"No ale nie mamy tego czołgu" - Andrea
"Zalegalizujesz moje zabawki?" - Kirył
"Zależy co. Nie wszystko mogę." - Andrea
"Wiesz co, wpadniemy dzisiaj do Ciebie z Anastazją" - Kirył
"Dobra" - Andrea, mając nadzieję że nie wybiorą Czelimina jako punktu spotkania

Łajdak weźmie na siebie ustalenie miejsca spotkania.
Łajdak wrócił do Andrei. Nie zdążą. Nie pojawią się dzisiaj. Siedzą na granicy Rosja/Ukraina.

...Andrea się z nimi NIE spotka. Ale ma 'czołg beacon'. Przynajmniej tyle...

Andrea poszła porozmawiać z Mieszkiem. Terminus zasalutował niedbale.

"Tien Bankierz. Trudne zadanie i niebezpieczne" - Andrea
"Brzmi jak typowe zadanie dla terminusa" - Mieszko
"Wróci pan na teren Czelimina" - Andrea, z uśmiechem
"Brzmi jak nietypowe zadanie dla terminusa..." - Mieszko, lekko zdziwiony

Andrea kazała Mieszkowi wziąć 'czołg beacon' do Czelimina i go uruchomić. Wyjaśniła mu czołg Esuriit...
Mieszkowi się bardzo ten pomysł nie podoba.

"On robi jakieś shifty dziwne... w sumie... nie wiem co robi ten czołg" - Mieszko, zirytowany
"Tym lepiej. Wróg też nie będzie wiedział" - Andrea, spokojnie

Andrea każe, Mieszko zrobi. Ale nie martwi Mieszka to, że czołg siedzi na Esuriit... z drugiej strony, Andrea ma rację - ta cholera może związać siły Czelimina zwiększając szansę sukcesu ich operacji.

Mieszko powiedział, że z kralotha wyciągnął, że Kompleks Centralny jest silnie Spustoszony. I sygnatura Spustoszenia jest spójna z Magią Krwi. Ale wszystko wskazuje na to, że centrum dowodzenia Kompleksem Centralnym - konkretnie Kompleksem - jest w Czeliminie. Kraloth wyczuł energię i formy sygnałów. Ktoś komunikuje się krwawymi falami z wieku XIX. Synchronizacja dwóch torturowanych magów. Nie da się tego sensownie przechwycić; kraloth to wyczuł bo je zna.

Kraloth wyjaśnił plan sił z Czelimina. Czelimin próbuje zainfekować źródła energii w Kompleksie Centralnym. Pełz wyczuł to dopiero gdy mógł dostać się tu i wyczuć aurę Czelimina.
Czyli nie umie przejąć EAM to próbuje przejąć cały Kompleks...

Czołg... zaczyna być kuszącą opcją.

Andrea wysłała Mieszka do przygotowania się a sama poszła na naradę wojenną z innymi terminusami i Marianem Łajdakiem.
Podczas narady Andrea dała im zadanie. Lidia ma za zadanie kontrolując biomózg uruchomić i sterować hipernetem "starej generacji". 

Marian Łajdak ma NATYCHMIAST sprowadzić inną ciężarówkę. Marian się umówił - nie będzie to tanie, ale dostanie ciężarówkę z TrustPort; kierowca Jacek Molenda.
Franciszek Myszeczka z Kręgu Życia wymyślił miejsce - leśna polana w pobliżu Skażonego i containowanego Węzła; w okolicy Piroga. Nazywa się Bladą Polaną; od czasów Zaćmienia nikt nie próbował jej wyczyścić (stopień trudności i dopływ energii). 

Z Andreą po 'starym hipernecie' próbuje się skontaktować Aneta Rainer. Tym razem wyszła od 'Lady Terminus' ;-). Powiedziała, że z Andreą skontaktuje się agentka i poprosiła o potraktowanie jej poważnie. To Jolanta Sowińska. Teraz Andrea zrozumiała - nie potraktowałaby jej poważnie. Okazuje się, że Jolanta jest Sowińską czystej krwi. Ma uprawnienia do kontroli Kompleksu Centralnego.

...to jest niespodziewane.

Jeśli nie ma Świecy Daemonica - a nie ma - to jeśli wszystkie Kompleksy wydadzą sygnał autoryzujący, to można uruchomić autonomiczne bronie z czasów Wojen Bogów. To utrudni ich przeciwnikom. Andrea jako Lady Terminus zna sygnały autoryzujące. Jolanta Sowińska jest na miejscu by móc odpalić autoryzację.

Jolanta wyśle sygnał do Andrei wtedy i TYLKO wtedy jeśli będzie potrzebowała kody do uruchomienia w ciągu 3-4 minut. Andrea się zgodziła (Aneta powiedziała, że Jolanta stoi koło niej, więc nie jest pod kontrolą przeciwnika).

Wracając do aktualnej sytuacji:

Zadaniem Mieszka jest odpalić czołg i wracać. Andrea upoważniła czołg do powrotu.
Skrzydło szkoleniowe zostaje, pilnuje bazy i się nie wychyla. Lidia zarządza hipernetem. Kraloth robi za komunikację. Rafael z komórką robi za interfejs do kralotha. Toteż Melodia. Dalia przygotowana na odbiór ciężkich przypadków.
Kajetan jedzie jako medyk bojowy, Rudolf jako maszyna do zabijania, Julian jako mag defensywny, Andżelika jako jednostka dywersyjna. Andrea też jedzie.

Mieszko odpalił czołg. "Stalowy Śledzik Żarłacz" wyrwał się z fazy Esuriit i plunął ogniem, sprowadzając Esuriit ze sobą i destabilizując Czelimin. Siły Czelimina rozpoczęły walkę ze Śledzikiem, który inteligentnie zaczął blinkować i szukać źródła sygnału. Niestety, Mieszko został ranny podczas próby ucieczki, ale ślady po nim zostały zamaskowane. Mieszko MUSIAŁ teleportować się, co wyczuły siły Czelimina... ale to nieważne, bo Śledzik był gotowy do strzelania...

Tymczasem portal zsynchronizowany między Dalią i Lidią Weiner a Anetą Rainer otworzył się w Czubrawce. Molenda spanikował, ale od czego zaklęcia. Banda Zajcewów, Wioletta Bankierz, Tatiana Zajcew i nieprzytomna Bianka Stein wpadli do ciężarówki. Odpalony został drugi portal - zmyłkowy. Andżelika używa pełni umiejętności by móc ukrywać tira magicznie.

Siły Czelimina próbują reagować na portal, ale Śledzik szaleje, w berserku, nie przyzwyczajony do Primusa. Co gorsza, Krwawa Kuźnia źle reaguje na Esuriit.

Andrea dała Andżelice zadanie - przebadać wszystkich obecnych pod kątem znaczników i systemów pułapkowych. Andżelika znalazła - te co są dla nich mordercze wysyłają też sygnał. Andrea wydała rozkaz - mają się tego pozbyć. Chciała przechwycić znaczniki; niestety, trzeba było je zniszczyć. Ma więc grupkę poranionych Zajcewów.

I bezpiecznie wróciła do domu...

# Progresja

* Kirył Sjeld: może spokojnie operować na okolicznych terenach póki Andrea jest Lady Terminus

# Streszczenie

Lidia i Rafael postawili Dalię Weiner, zdobytą dzieki współpracy z Kręgiem Życia. Niestety, Dalia jest na pastylkach Rafaela. Ale - mają lekarza. Przybyło też więcej posiłków - Melodia i Laragnarhag z Millennium, co spowodowało konflikty - zwłaszcza z Kajetanem. Melodia ustabilizowała Barana a potem zdobyła przekaźnik hipernetowy. Andrea załatwiła z Marianem Łajdakiem użycie jego szmuglerskich talentów. Ten skontaktował ją z Kiryłem Sjeldem - który chce odzyskać czołg który utknął na Fazie Esuriit. Andrea ściągnęła go (czołg) do Czelimina by zrobić dywersję - odzyskać poranionych Zajcewów i dodatkowe wsparcie. W wyniku, Druga Strona dowiedziała się, że Mieszko Bankierz był w Czeliminie. Ale! Andrea odbiła wszystkich Zajcewów bezpiecznie i Karradrael nie wie, gdzie się znajduje baza Andrei (i myśli, że dowodzi Mieszko i że ów nie ma wielkich sił). Rudolf Jankowski postawiony, choć jeszcze bez "umysłu" - ale jest skuteczną maszyną do zabijania słuchającą tylko Andrei.

# Zasługi

* mag: Andrea Wilgacz, radzi sobie z konfliktami katalizując wszystkich przeciw wspólnemu wrogowi, ratuje Zajcewów i decyduje przyzwać Stalowego Śledzika Żarłacza do Czelimina z Esuriit.
* mag: Lidia Weiner, postawiła z Rafaelem i Dalią Rudolfa, acz odcięła wyższe ośrodki myślenia; dowodzi 'starym nowym hipernetem' dla Andrei. Neuronautka rekalibrująca hipernet...
* mag: Melodia Diakon, gwiazdka Millennium przynosząca radość. Stabilizuje Tadeusza Barana i odzyskała dla Andrei stary emiter hipernetowy. Można polubić. Niestety, lubi kralotha.
* vic: Laragnarhag, jak na kralotha, wyjątkowo grzeczny. Może bardzo wiele - ale nikt mu nie pozwala. Służy jako system komunikacyjny z Millennium. Źródło konfliktów w Zespole.
* mag: Julian Pszczelak, pełnił rolę * maga defensywnego przy przechwytywaniu Zajcewów.
* mag: Dalia Weiner, z Lidią i Rafaelem postawiła Rudolfa. Lekarz stawiający Zajcewów na nogi. Sieć bezpieczeństwa dla Andrei.
* mag: Marian Łajdak, zapewnił Andrei narzędzia antyspustoszeniowe i zapewnił jej swoje kontakty w świecie szmuglersko-przestępczym jako wsparcie.
* mag: Rafael Diakon, co prawda po* maga zespołowi jak może, ale jest coraz bardziej traktowany jako źródło problemów a nie prawdziwa pomoc. Jest nieco nieludzki i jest niebezpieczny.
* mag: Mieszko Bankierz, zdrowy rozsądek ze Świecy, co Andrea bardzo docenia. Przywołał czołg z Esuriit do Czelimina, powodując poważne kłopoty Karradraelowi. Niestety, zdradził się.
* mag: Kajetan Weiner, niechętny całej sprawie i zgniłym kompromisom. W ostrym ścięciu z kralothem i gildią * viciniusów. Andrea jednak go utrzymała, acz jest to coraz trudniejsze.
* mag: Andżelika Leszczyńska, stanowi centralny komponent wykrywający Zajcewów i ukrywający ich ruchy przed Karradraelem. Skutecznie. Przy okazji, robi za ochmistrzynię zamku.
* mag: Rudolf Jankowski, chwilowo bezduszna maszyna do zabijania pod kontrolą Andrei. Chwilowo. Inteligentny acz "bez duszy".
* mag: Kirył Sjeld, szmugler z okolic Syberii który współpracuje z Andreą i dał jej do przyzwania rodowy czołg z prymitywnym AI. Niestety. Utknął na granicy z Ukrainą.
* czł: Jacek Molenda, kierowca ciężarówki który przetransportował Zajcewów dla Andrei; w sferze wpływów Mariana Łajdaka.
* mag: Franciszek Myszeczka, zapewniał źródło wiedzy lokalnej dzięki min. zmysłom stowarzyszonych * viciniusów.
* mag: Aneta Rainer, która łączy ze sobą Jolantę Sowińską i Andreę. Chce uruchomić autonomiczną broń z czasów Wojen Bogów.
* mag: Jolanta Sowińska, która jest Czystej Krwi - może kontrolować Kompleks Centralny (czyli: Andrea może wysyłać kody przez Jolantę).
* vic: Stalowy Śledzik Żarłacz, syberyjski czołg T-42 (101 ton) z prymitywnym AI i uwięziony na Esuriit. Wezwany przez Mieszka do Czelimina jako dywersja...

# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Czubrawka, gdzie wypadła banda Zajcewów z ekipą i skąd Andżelika maskowała ich ślady do Wtorku Śląskiego
                1. Wtorek Śląski
                    1. Ośrodek historyczny
                        1. Ruina zamku, gdzie Andżelika dokonuje cudów by wszystkich móc umieścić, pomieścić, by się nie spotykali jak nie chcą... na szczęście, Marian ma zapasy.
                1. Czelimin, gdzie został przyzwany Stalowy Śledzik Żarłacz by ciemiężyć Karradraela.
                1. Piróg Górny
                    1. Las Stu Kotów
                        1. Blada Polana, gdzie znajduje się zapieczętowany nieprawdopodobnie potężny Węzeł którego nikt nie chce dotknąć
      
# Czas

* Dni: 2
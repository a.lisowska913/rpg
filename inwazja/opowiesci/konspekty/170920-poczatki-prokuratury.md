---
layout: inwazja-konspekt
title: "Początki prokuratury"
campaign: powrot-karradraela
players: kić, dust, raynor, foczek
gm: żółw
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [170914 - Kolejna porażka Kinglorda](170914-kolejna-porazka-kinglorda.html)

### Chronologiczna

* [170914 - Kolejna porażka Kinglorda](170914-kolejna-porazka-kinglorda.html)

## Kontekst ogólny sytuacji

### Siły główne

**Arazille**

The Queen of freedom has a very simple agenda. She wants to protect Hector from being enslaved. On top of that, she wants chaos – at this moment her forces are fortifying Czelimin and the fate of this city may be sealed unless the magi start coordinating forces against her. The turmoil around the Blakenbauer bloodline is exactly what she needs at this moment. No matter if Maus are hit or if Blakenbauer are hit, she wants an outpost and it will be very hard to dislodge her.

**Iliusitius**

The king of just revenge has only one goal, only one dog in this fight. What he wants is to make the prosecution work – under his orders. He has let Hector a lot of leeway, as much as he has left his bloodline alone for long time. Now it is time for the chickens to come to roost. It is time to control the justice in the world of Magi.

**Kinglord**

This slightly insane defiler wants to become a god. Of course, anyone would like to become a god. He believes he is able to do so by tapping into the network connecting the Maus  bloodline with Karradrael. A completely insane Maus mage has managed to convince him that by killing Hector, Kinglord is able to rally to support of other Maus Magi.

**Pyotr**

This mage is unusually receptive to the flows of magic, by some freak accident he got connected to Iliusitius. He became a tool by Iliusitius to connect the God to the prosecutor. His fate is irrelevant for anyone until that point comes.

**Henry**

After being devastated by the King Lord who has managed to destroy his pattern, he got rebuilt into a young girl. This process linked him to Renata Souris, as the second instance of her bloodline. What Renata wants is to return to relevance and she will use Henry to achieve her goals. He, as a person, is irrelevant to her. She is attracted to Henry as the only person related to her by blood now, but it is not enough for the determined lady to change her approach.

**AntiMaus**

Those forces are commanded by Dariusz Sowiński. They want to purge everything from the Maus bloodline. Because of Blakenbauer involvement with Maus bloodline those forces are kind of fighting against them too. They are very opportunistic and wants to focus on eliminating Maus over anything else. They can be swayed and persuaded to the cause.

### Stan aktualny

Hector is currently free. He had some issues with the coalition trying to exterminate the Maus bloodline,  however with the help of Marianne and Siluria  he was released to the Blakenbauer mansion. He is very ill, however, and his brother – Edwin – needs to take care of him. Kinglord's Assassin was able to properly wound him knowing some stuff about the blood... And the nature of the Beast.

Mordred, Henry and Pyotr are in the same location – the ruined opera. Not much is going on there at this moment, between them not really having anything to do and – potentially – enemy not knowing they are over there. The fact they had an assault team hunting Pyotr doesn't really matter. After all, Kinglord  cannot remember those simple  aspects, can he?

Siluria joined Mordred  in watching over Henry after Rafael made his modifications. She's trying to mount a campaign against Kinglord. to make it work, she has  asked a friend of hers – Whisperwind – to help her. Whisper was only happy to be able to kill the asshole. She is also located in the ruined opera.

Henry is still a girl. He kind of likes it; it gives him new types of powers he did not experience before as an elder priest. He has managed to communicate with Renata Souris who has given him access to the tech bunker. Henry was unaware of the risks and he sends an elder human there; the old man died. Whoops. Henry has as much of a remorse as he used to.

Pyotr however still has no motivation to anything. He still embodies the power of Iliusitius.

Siluria was warned by Arazille repeatedly that Hector is under grave risk. Siluria might be more disturbed that Arazille cares for Hector and that Arazille communicates with her then that Hector was in danger. She kind of got used to it.

### Istotne miejsca

The opera is actually quite a devastated building at this moment. It has second level of magical structural vulnerability at this moment. It was really hit by the Mordred's approach and is really strongly fortified mostly by the spiderwebs. And this is a problem itself. The building suffers from high amount of magical pollution and being quite damaged. It used to be a paintball place; there are some HOLES, cracks and sneaky hiding places. Also, art. Mostly Pyotr's art.

The Blutwurst Castle  is a very strange castle –it used to have quite a normal distribution of posh rooms around, but extensive use of blood magic by its inhabitants have twisted the structure of the castle. Especially after the stuff of Alexandria. But also, Kinglord has made some modifications to the castle itself. He has infected it with something...

The black tower itself is made out of a dead Dragon - asari. It is a very strange place teeming with magic. Kinglord  has managed to adapt to it  and bend it somehow, using some strange types of magic. This place is in a way alive and responding to its new master.

## Punkt zerowy:

## Misja właściwa:

Siluria sprowadziła magów z Dare Shiver i z Zaćmionego Serca do oglądania wystawy Piotra. Też, ludzi z tamtych miejsc. Około 30-40 osób. Operiatrix szczęśliwa: 2/20, trochę się zasiliła. Piotr jednak się nie popisał z targetem obrazów, wybrał niewłaściwe...

1) Atak Kariny Łoszad na operę.
11) Seal (10, Krwawa, Zasilana Strachem)
12) Thugi (10, Sprzężeni, Bloodhoundy)
13) Karina Łoszad (12, Skażona Asari, Krwawy Głód)
14) Artyleria (15, LongRange, Kinetyczna, Erupcja Magii)

Atak Kinglorda. Nie zdążył, Mordred szybszy. Distres call zanim padła komunikacja. Renata poradziła Hani podpięcie techbunkra do seala. Ostrzegła, że będą ofiary. Zamiast tego, Hanna zdecydowała się użyć tego połączenia by nakarmić pryzmatycznie Operiatrix przy użyciu energii techbunkra (15 -3 (kody kontrolne)). Cała trójka wzmocniła Operiatrix do poziomu pełnej mocy, większej niż kiedykolwiek...
...i na to pojawiły się siły Zła.

Operiatrix osłoniła defensywę. Shrapnel nie miał tej dziwnej mocy; poraniło członków. Zasiliło to wszystkie istoty które tu się pojawiło; Mordred wyczuł, że są sprzężeni z kośćmi martwego asari. Asari + Krew + Kinglord...

Siluria uderzyła najsilniejszym zaklęciem jakie ma - PreEkstaza. Znowu ciągną z bunkra. Skażenie bunkra; skażenie LEY LINE. Pojawiła się Świeca (Aneta Rainer z siłami) oraz KADEM (Whisperwind i Warmaster). Whisper porwała preekstatyzowanych drabonów i Karinę; Millennium wzięło ostatniego. Aneta przekazała teczkę z niebezpiecznymi dokumentami o Blakenbauerach Mordredowi, ostrzegając o niebezpieczeństwie.

Tymczasem Hektora odwiedziła Andrea. Przekazała mu tą samą teczkę i ostrzegła go o sprawie ze swego rodu. Hektor przejrzał teczkę; wpadł w lekką depresję i zdecydował się porozmawiać o tym z resztą rodziny. Ale wpierw - musi pojechać do prokuratury. Ma tam spotkanie z kimś ważnym - z samym Iliusitiusem - o czym się dowiedział od niego samego.

W samochodzie, Hektor został nawiedzony przez Arazille oraz Iliusitiusa. Iliusitius zapowiedział Hektorowi, że w świetle zbrodni popełnionych przez niego samego (na Izie Łaniewskiej, na Wandzie Ketran...) on, Iliusitius, chce dowodzić prokuraturą magów. Zdradził Hektorowi, że to on stoi za zbudowaniem prokuratury. Arazille poinformowała Iliusitiusa, że nie pozwoli mu przejąć kontroli nad Hektorem. Ona także opiekowała się Hektorem przez długi czas...

Hektor w końcu wrócił do Rezydencji. Skonfrontował się z resztą rodu i ich odrzucił. Został tylko Hektor i Mordred jako jedna frakcja Blakenbauerów a Otton, Edwin, Margaret i Marcelin jako druga (i trafili na KADEM). Tymotheus znowu zniknął. Klara i Leonidas? Nie wiadomo.

W operze pojawiła się Aneta Rainer. W czasie, w którym wraz z Warmasterem ustalała co dalej - Whisperwind porwała Karinę Łoszad i kilku drabonów. Millennium zapewniło sobie ostatniego drabona rękami Mordreda. Tien Rainer nie dostała niczego - ale osłoniła teren i uratowała sytuację.

Siluria została wycofana na KADEM by poradzić sobie w nowej sytuacji.

## Podsumowanie kampanii:

* Hektor i Mordred założyli prokuraturę magów wspierani przez siły Srebrnej Świecy i pozostałych gildii.
* Iliusitius w tle wpływa na prokuraturę magów, acz nie udało mu się dostać dostępu do hipernetu (co zniszczyłoby wszystkich magów Świecy).
* Karradrael się obudził pod kontrolą Abelarda Mausa. Mausowie współpracują z innymi siłami.
* Renata jest urażona faktem, że straciła techbunker. Ale jest chętna do współpracy z Silurią celem zniszczenia Kinglorda - śmierć Henryka zabrałaby Renacie "komunikację", jej poddanych.
* Whisperwind nie jest w stanie dostać się do Kinglorda. Defiler stał się potężniejszy niż kiedykolwiek - być może nawet niż Whisper.
* Opera jest w bardzo dobrym stanie. Zarówno Operiatrix jak i sam budynek jakoś dojdą do siebie.
* Niesamowity Paradoks erotyczny poleciał w leyline pod operą. Nie do końca wiadomo, co się z tą energią stanie.
* Elementami planu Arazille było rozpuszczenie chaosu (Mausowie, Blakenbauerowie) by mogła się ufortyfikować. Jej zdaniem, by kontrować Iliusitiusa.
* Henryk Siwiecki trafił do Millennium, pokazując Mordredowi dużo Mausowych techbunkrów.

**Podsumowanie torów**

- NA

**Interpretacja torów:**

- NA

# Progresja

* Edwin Blakenbauer, trafił na KADEM jako członek po odrzuceniu go przez Hektora i Mordreda
* Margaret Blakenbauer, trafiła na KADEM jako członek po odrzuceniu jej przez Hektora i Mordreda
* Marcelin Blakenbauer, trafił na KADEM jako członek po odrzuceniu go przez Hektora i Mordreda

# Streszczenie

* Hektor i Mordred założyli prokuraturę magów wspierani przez siły Srebrnej Świecy i pozostałych gildii.
* Iliusitius w tle wpływa na prokuraturę magów, acz nie udało mu się dostać dostępu do hipernetu (co zniszczyłoby wszystkich magów Świecy).
* Karradrael się obudził pod kontrolą Abelarda Mausa. Mausowie współpracują z innymi siłami.
* Renata jest urażona faktem, że straciła techbunker. Ale jest chętna do współpracy z Silurią celem zniszczenia Kinglorda - śmierć Henryka zabrałaby Renacie "komunikację", jej poddanych.
* Whisperwind nie jest w stanie dostać się do Kinglorda. Defiler stał się potężniejszy niż kiedykolwiek - być może nawet niż Whisper.
* Opera jest w bardzo dobrym stanie. Zarówno Operiatrix jak i sam budynek jakoś dojdą do siebie.
* Niesamowitej mocy superkaskada Paradoksu erotycznego poleciała w leyline pod operą. Nie do końca wiadomo, co się z tą energią stanie.
* Elementami planu Arazille było rozpuszczenie chaosu (Mausowie, Blakenbauerowie) by mogła się ufortyfikować. Jej zdaniem, by kontrować Iliusitiusa.
* Henryk Siwiecki trafił do Millennium, pokazując Mordredowi dużo Mausowych techbunkrów.

# Zasługi

* mag: Mordred Blakenbauer, przekaźnik energii do walki z Kinglordem, negocjator (serio), skończył jako partner Hektora w prokuraturze magów. Zwerbował Hannę (Henryka) do Millennium.
* mag: Hektor Blakenbauer, rozczarowany rodem (dowiedział się prawdy) dołączył do prokuratury magów z Mordredem. Opuścił ród. Uniknął przejęcia przez Iliusitiusa i "wolności" Arazille.
* mag: Siluria Diakon, rzuciła najbardziej erotyczne zaklęcie militarne w historii, wyłączając drabony i Karinę Kinglorda. Załatwiła z Renatą wspólne interesy przeciw Kinglordowi.
* mag: Henryk Siwiecki, jako Hania. Zeżarła Mausowy TechBunker dla wspólnego dobra aby naładować Operiatrix. Potem zasiliła Silurię energią. Też - ma dobry wpływ na Renatę Souris (serio).
* vic: Operiatrix, odżyła zasilona mocą techbunkra Mausów; zwalczyła siły atakującego Kinglorda i osłoniła swoich gości w operze.
* mag: Whisperwind, która bezwzględnie porywa na KADEM co należy do KADEMu jej zdaniem. Poluje na Kinglorda, ale ktoś musi jej go wystawić.
* mag: Renata Souris, która odkąd ma Henryka (Hanię?) jest skłonna do współpracy - ma po co żyć. Ma dobre propozycje jak zniszczyć napastników... z operą.
* mag: Kinglord, który objął władzę w Czarnej Wieży i używa nienaturalnych form magii, niezgodnych z tymi jakie działają dla zwykłych magów.
* mag: Karina Łoszad, Skażona nienaturalną magią Kinglorda i kośćmi asari zaatakowała Operę. Pokonana i porwana przez KADEM celem jej naprawienia.

# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Okólno-Trocin
                1. Czapkowik
                    1. Stara Opera, miejsce wojny pomiędzy oddziałem Kinglorda mającym porwać Piotra i zniszczyć wszystkich a Zespołem próbującym temu zapobiec.

# Czas

* Dni: 2

# Strony

* **EIS**: Chce zniszczyć Renatę.
* **Renata**: Chce być wolna i odzyskać Karradraela
* **Kinglord**: chce dostęp do Bzizmy, chce być Karradraelem 2, odzyskać Henryka, ukarać Piotra, zniszczyć Hektora... a ogólnie, zdobyć władzę nad Mausami.
* **Iliusitius**: chroni Hektora, chce prokuratury magów, chce obsadzić ją SWOIM magiem. Hektor się nada. Chce wykorzystać Piotra jako dywersję i conduit.
* **Arazille**: chce zniszczyć Blakenbauerów i ochronić Hektora przed siłami Iliusitiusa. Chce uwolnić Piotra od aury Iliusitiusa i uwolnić Bzizmę.
* **Bzizma**: chce odzyskać kontrolę i możliwości. Non-issue.
* **Rafael Diakon**: chce dowiedzieć się jak najwięcej odnośnie Kinglorda i odnośnie Henryka.
* **Judyta Maus**: non-issue, ale chce być lubiana
* **Renata Souris**: chce wolności i odzyskania Karradraela. A naprawdę, chce wolności i odnalezienia się w nowym świecie, mieć ZNACZENIE.
* **Koalicja AntyMausowa**: chce zniszczenia rodu Maus
* **Abelard Maus**: chce akceptacji.
* **Karina von Blutwurst**: chce wolności i zniszczenia Kinglorda; chwilowo nie ma wyboru.
* **Marianna Sowińska**: chce pomóc Hektorowi zrobić prokuraturę magów.


# Narzędzia MG

## Opis celu misji

-

## Cel misji

-

## Po czym poznam sukces

-

## Wynik z perspektywy celu

-

    
## Wykorzystane tabelki

Postać (strona gracza):

| .Motywacja. | .Siły i słabości. | .Specjalizacja. | .Umiejętność. | .Szkoła Magii. |.POSTAĆ. |
|             |                   |                 |               |                |         |

Opozycja (strona NPC):

| .VERSUS. | .Aspekty. | .Skala. | .Magia. |.Pomysł gracza. | .OPOZYCJA. |
|          |           |         |         |                |            |

Wynik:

| .Postać. | .Opozycja. | .Rzut.    | .Wynik.  |
|          |            | xx: +x Wx |   S-SS-F |

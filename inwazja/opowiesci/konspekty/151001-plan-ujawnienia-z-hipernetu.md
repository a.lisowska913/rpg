---
layout: inwazja-konspekt
title:  "Plan ujawnienia z Hipernetu"
campaign: powrot-karradraela
gm: żółw
players: dust
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [150820 - Klemens w roli swatki (Kx, AB)](150820-klemens-w-roli-swatki.html)

### Chronologiczna

* [150820 - Klemens w roli swatki (Kx, AB)](150820-klemens-w-roli-swatki.html)

### Logiczna

* [150819 - Krótki antyporadnik o sojuszach (HB, SD, Kx)](150819-krotki-antyporadnik-o-sojuszach.html)

## Punkt zerowy:

Ż: Jaką korzyść z tej całej sytuacji może uzyskać KADEM przeciwko Świecy?
B: KADEM uzyskuje przyczółek na obszarze pomiędzy KADEMem a Srebrną Świecą.
Ż: Jaka akcja z ukrycia na pewno MOCNO uderzyła w Srebrną Świecę?
K: Ktoś z członków Świecy zaatakował Powiew w taki sposób, że Powiew ma silne prawo do rekompensaty.
Ż: Powiedz mi coś o dziewczynie, przez którą Marcel wpadł w kłopoty.
D: Ma lepkie ręce. Cicha, zwinna, niepozorna. Absolutnie niebojowa.
Ż: Dlaczego właśnie jego? Co ma za szczególną wiedzę którą chce się dowiedzieć?
B: Vladlena może nie wiedzieć, ale zna połączenie pomiędzy Emilią a niebezpieczną grupą przestępczą.

Ż: Co buduje Vladlena czym może skrzywdzić Szlachtę?
D: Exposer; wielką stację broadcastową. Jeśli złapie się maga Szlachty to można poważnie uszkodzić ich socjalnie.

## Kontekst misji:

Z jednej strony, same dobre wiadomości; sojusz z Kurtyną jest już w miarę stabilny, Marcelin znalazł dziewczynę i nie przeszkadza...
Z drugiej strony, nadal są rzeczy niedopowiedziane:
- Vladlena szuka czegoś z laboratoriów Blakenbauerów (zdaniem Blakenbauerów: nic nie ma)
- Vladlena stoi za tymi wszystkimi cholernymi dopalaczami dla dzieciaków
- Emilii Kołatki jeszcze nie ma; nadal jest zniknięta
- Vladlena rozbudowuje bazę defensywną której NA PEWNO przestraszy się Szlachta

## Misja właściwa:


### Faza 1: Budowa Exposera

Vladlena przejęła obszar budowy hipermarketu NeWorld i rozpoczęła pracę nad Exposerem - bardzo złożonym artefaktem opartym min. o technomancję i astralikę oraz o żywego maga który wejdzie do środka. Przesunęła pracę całej Kurtyny w kierunku na budowę Exposera jako broń ostatniej szansy przeciwko Szlachcie.
Exposer ma za zadanie przesłać po hipernecie wszystkie istotne informacje tak, by nie dało się tego wyciszyć i by zmusić Srebrną Świecę do reakcji, skoro najwyraźniej Wydział Wewnętrzny się tym w ogóle nie interesuje. Vladlena pobrała większość surowców Kurtyny i działa zdecydowanie z ukrycia, pierwotnie ukrywając Exposer za "bardzo dużym działem".
Ale Hektor dowiedział się czym jest Exposer... przypadkiem. Od swoich sił specjalnych, a dokładniej od Anny Kajak.

Anna zaraportowała, że na terenie zajętym przez Vladlenę budowana jest silna sieć elektryfikacyjna i wodna; coś, co nie jest tam potrzebne. Niezgodnie z tym co miało być wg planu. Hektor się zasępił - miało tam być budowane "duże działo", ale duże działo nie potrzebuje czegoś takiego. Hektor odwołał Annę; ma sporządzić raport i zostawić temat.
Patrycja powiedziała też (przez Annę), że tam będzie budowana... serwerownia?
Hektor powiedział, że chce raport, ale na razie nie mają się tym zajmować. Anna przyjęła do wiadomości. Hektor wysłał ją do jakiegoś błędnego zaułka; nie chce by tam siły specjalne działały.

Edwin przyszedł do biura Hektora. Hektorowi opadła szczęka.
Edwin powiedział, że Szlachta zaczyna widzieć, że Kurtyna buduje coś przeciwko Szlachcie... a Blakenbauerowie ich chronią. Plotka tu, plotka tam, Vladlena specjalnie je rozpuszcza a Marcelin tym bardziej. Dodatkowo, Vladlena powiedziała przecież, że Blakenbauerom coś uciekło - nie, Edwin odrzuca. Sprawdził. Ani jemu ani Margaret nic ważnego nie uciekło, nic w ogóle nie uciekło. Hektor zauważył, że skądś wziął się przecież aptoform, ale Edwin sparował - to nie ICH aptoform. Może pasować do schematu, ale...
Edwin i Hektor zastanawiają się, po co Vladlena miałaby truć tych ludzi.
Hektor decyduje, że chyba jednak się trzeba spotkać z Vladleną...
Edwin zauważył, że niedługo obudzi się Mojra, Rezydencja już ją w wysokim stopniu naprawia z jego pomocą.
Edwina tknęło - może Vladlena szuka Arazille (i o tym nie wie). Hektor na to, że skończy jak Mojra... 

Jak załagodzić kontratak Szlachty? Cała nadzieja w Mojrze...

Rozmowę przerwało im pukanie. Przesyłka. Ale nie wolno przecież przynosić przesyłek do tego miejsca...
Hektor odwołał się do sekretarki "kto to". Ona nie wie, zorientowała się dopiero teraz. Hektor ją uspokoił i wysłał gościa do poczekalni.

"Ja się schowam w szafie; jak wybuchnie, to cię wyleczę" - Edwin do Hektora

Hektor wyszedł do przybysza, którym okazał się tien Adrian Murarz. Hektor do niego nie wyjdzie, tym bardziej Edwin. Po krótkiej naradzie, wezwano Dionizego. Dionizy odebrał paczkę, kazał Adrianowi przyjść z dowodem osobistym, wsadził paczkę do lapisu i odniósł na żądanie do Rezydencji. Tam przebada to Margaret. Przyszło ze stowarzyszenia miłośników ryb...

"Czy to jest groźba?" "Nie, to jest akwarium" - Adrian Murarz do Dionizego Kreta

Margaret facepalmowała. Ona wie co to jest. Walkie-talkie. Nalewasz wodę, możesz rozmawiać z inną osobą mającą takie same akwarium; bardzo tania metoda komunikacji, typowa wśród uczniaków i żaków.

Hektor wrócił do Rezydencji. Trzeba nalać wodę do akwarium...
W akwarium pojawiła się nieznana dama. Przedstawiła się jako Diana Weiner. Diana powiedziała Hektorowi, że pragnie pokoju. Ostrzegła go, że za 21 godzin Szlachta przeprowadzi atak przeciwko hipermarketowi. Hektor spytał, czy Diana ostrzega go o zamachu terrorystycznym; czarodziejka powiedziała, że interesuje ją jedynie to, żeby żaden człowiek nie ucierpiał. Przypomniała Hektorowi, że on całe życie pomagał ludziom i ona nie chce by w wojnie magów jakiś człowiek ucierpiał. Zauważyła, że Marcelin zranił ją merkuriaszem; ona nie ma nic przeciw temu, takie rzeczy się zdarzają.
Powiedziała Hektorowi, że Szlachta idzie w kierunku demokracji i zniszczenia pionowych struktur Rodów które są teraz. Nie interesują ich ludzie. Poprosiła Hektora, by ten miał przy sobie jakiś komunikator który Szlachta może lokalizować podczas bitwy. Oni wiedzą o klątwie Blakenbauerów. Nie chcą odpalić Bestii i chcieliby, by Hektor pozostał poza konfliktem.

Diana zauważyła, że siły powiązane z Vladleną pobrały dane o Spustoszeniu; coś z tego jest wykorzystywane w tym, co dzieje się w okolicach hipermarketu. A Kurtyna wcześniej już miała ze Spustoszeniem do czynienia (Dracena, Antygona, Iza...). Szlachta po prostu nie chce dopuścić, by to się powtórzyło - więc zaatakuje.

Hektor ma trochę do myślenia. Zaproponował Dianie spotkanie z Vladleną na neutralnym gruncie; czarodziejka się z radością zgodziła. Powiedziała, że jeśli Vladlena zgodzi się przyjść to ona, Diana, stawi się nieuzbrojona i bez ochrony. Zaufa Hektorowi.

Tak...

HIDDEN MOVEMENT

- Adrian Murarz prosi Vladlenę by tego nie robiła \| WS.(make a show of his power)
- Adrian Murarz mówi Vladlenie, co się stanie jak będą stawiać czoło Wiktorowi \| WS.(announce future badness)

END OF HIDDEN MOVEMENT

Rozmowa z Edwinem w sprawie sytuacji. Edwin powiedział, że faktycznie, Szlachta robi złe rzeczy magom; przypomniał Antygonę i Dracenę (oraz Izę). Te wszystkie akcje przeciw magom Kurtyny to też Szlachta. Okrutni i niebezpieczni. Kurtyna dla odmiany ma magów wykorzystujących ludzi jako surowiec. Obie strony zdaniem Edwina mają swoje na sumieniu.
Kurtyna chroni starego porządku a Szlachta żąda nowego porządku. Przynajmniej na to wygląda.
Hektor chce tego co Szlachta, ale nie takimi metodami. Uważa, że Świeca powinna do tego dojrzeć.

Z perspektywy Hektora:

_Kurtyna:_
- używają ludzi jako zasobów (-)
- są fanatycznymi obrońcami starego systemu (-)
- używają Spustoszenia (-)
- zmuszają Blakenbauerów do działania; włączają ich w swoją grę (-)
- pomagają słabszym magom (+)
- działają razem (+)

_Szlachta:_
- rewolucjoniści (-)
- krzywdzą magów (-)
- niszczą swoich wrogów okrutnie (-)
- są gotowi do dialogu; mogą współpracować (+)
- poglądy podobne do Hektora (+)
- nie chcą w konflikt włączać Blakenbauerów (+)
- używają prokuratury jako narzędzia (-) 
- umożliwili powstanie prokuratury (+)

Hektor stwierdził, że rodzinnie odwiedzą teren hipermarketu NeWorld. Hektor, Margaret i Edwin. Edwin zaprotestował. Mindlink też odpada. Jeśli jest Spustoszenie i ono przejdzie na Rezydencję przez Edwina...
Edwin dostał interkom od Hektora. Tak mogą działać.
Płaszczki będą kontrolowane przez Rezydencję; w ten sposób znajdę ewentualne Spustoszenie.

Hektor i Margaret jadą limuzyną prowadzoną przez Dionizego. Dotarli do hipermarketu NeWorld. Tam czeka już Vladlena; jest blada i zmęczona. Zaprasza Hektora i Margaret do Komnaty Echa - ogromnej betonowej budowli podziemnej. Tam mogą swobodnie rozmawiać. Przyciśnięta, Vladlena powiedziała, że to co tam robią nie jest działem - jest to broń psychologiczna. Próbują ściągnąć uwagę Wydziału Wewnętrznego na Kopalin. Vladlena nie ufa kopalińskiemu Wydziałowi; są jej zdaniem albo ślepi albo w to zamieszani.

W dalszej rozmowie Vladlena przyznała (konflikt), że zrobili coś specjalnego; mają zamiar uderzyć w hipernet. Spenetrować bariery hipernetu i ujawnić komunikację, sny i marzenia. Przesłać je szeroką wiązką. W ten sposób Wydział Wewnętrzny będzie musiał się tym zająć a działania Szlachty wyjdą na jaw. Margaret zauważyła, że chyba używają Spustoszenia do tego. Hektor wszedł tak twardo jak tylko umie i Vladlena pękła. Tak, użyją Spustoszenia; mają do pomocy Zenona Weinera. Tylko Spustoszeniem mogą przebić się przez barierę hipernetu. Potencjalna wada jest taka, że magowie Świecy usłyszą Szept Spustoszenia a Spustoszenie usłyszy myśli magów Świecy. Ale - nie ma Spustoszenia.

Hektor powiedział, że nie jest pewny; może jeszcze być, niech Vladlena potwierdzi to z Zenonem. I faktycznie - jest jeszcze jeden kontroler. O jeszcze jednym wiedzą. Kompatybilnym (bo z tego samego szczepu). Vladlena powiedziała, że to nie jest taki problem; Hektor zauważył, że 'lena proponuje, by Spustoszenie poznało tożsamość WSZYSTKICH magów Świecy.
Vladlena zbladła.

Hektor powiedział, że Vladlena musi znaleźć inne rozwiązanie. Na razie - ewakuują ludzi. Uderzy tu Diana Weiner a na pewno Szlachta.

Vladlena powiedziała, że Diana Weiner jest najskuteczniejszą kapłanką Szlachty ever. Tak jak Hektor zniszczy każdego na sali sądowej, tak Diana przekona każdego poza tą salą...
Został też Dionizy w ukryciu. Jakby Kurtyna robiła coś złego, ma dać znać.
Vladlena kazała rozdać ludziom stymulanty; mają pracować na 150-200% normy, na pełni mocy. Zanim uderzy Szlachta, Vladlena chce mieć swoją fortecę której nikt nie da rady spenetrować.

Margaret zauważyła, że Vladlena jest bardzo zdesperowana i może zrobić coś głupiego.

### Faza 2: Obrona hipermarketu

Hektor nie zdecydował się na spełnienie prośby Diany. Ani nie ma przy sobie komunikatora będąc w Rezydencji, ani nie znajduje się na polu bitwy. Hektor nie chce mieć z nią żadnego kontaktu.

Na miejscu jest jednak Dionizy. I Vladlena o tym wie.

Siły Vladleny to uzbrojone manekiny i płaszczki. Siłami Szlachty dowodzi tien Adrian Murarz oraz tien Wiktor Sowiński. Po tamtej stronie są siły demoniczne oraz oddziały klonów. Po stronie Vladleny jest cała Kurtyna.

Na pole bitwy wyszedł Wiktor Sowiński i Adrian Murarz. Murarz zasalutował Vladlenie, która uśmiechnęła się smutno. Były mag Kurtyny, zniszczony przez Sowińskiego. Sowiński jeszcze ostrzegł, że jeśli odejdą, nic złego się nie stanie. Jeśli nie odejdą... wpadną w jego ręce.

Vladlena zaprosiła Sowińskiego do walki. Sowiński machnął ręką i otworzył serię portali z demonicznymi kohortami, drugi raz machnął i pojawiła się armia klonów. Dość duże środki. A wszystko ukryte pod welonem iluzji, by nie łamać Maskarady.
Samo Skażenie utrudniło czarowanie. Vladlena prawie zemdlała, Dionizy się pochorował, ale wyszedł z tego. Dionizy szybko podał 'lenie stymulant trzymający ją na nogach (połączył się ze Skażeniem) i Dionizy szybko wydał polecenia strategiczne. Jeden z lepszych oficerów taktycznych i to świetnie radzący sobie w warunkach taktycznych bez magii (Wiktor założył, że magowie się na to nie przygotują i miał rację!) i błyskawicznie przechytrzył przygotowanego Wiktora.

Vladlena dała wytyczne Dionizemu - koniecznie potrzebny jest Vladlenie Adrian Murarz. Niestety jest na tyłach. Czy jest opcja, by Dionizy przechwycił Murarza? Vladlena upewniła Dionizego, że ma plan; Sowiński nie będzie w stanie jej zatrzymać i pokonać. Dionizy niechętnie, ale się zgodził.
Zwłaszcza, że 'lena przygotowała tunele.

Adrian ma bodyguardów (walka 14 #5 tool #3 circum). Sam Adrian ma wartość (10 # 5 circum #5 tool). Więc wpierw trzeba by go jakoś rozebrać. Szczęśliwie, Wiktor jest na froncie ze swoim (15 # 5 # 5 -> 25). 
Dionizy, najsilniej przygotowany w historii wyszedł z armią samobójczych kalmarów podziemnych. Przedtem wysłano dywersję w tym kierunku, kilka pancernych żuków które zostały zniszczone przez ogień zaporowy, więc oddział chroniący Adriana zmienił pozycję a Dionizy wyszedł spod ziemi. Kalmary oplotły strażników, Dionizy złapał przerażonego Adriana i ściągnął go pod ziemię. Pełen pakiet zabezpieczający, Adrian jest wyłączony.

Tymczasem atak Sowińskiego przecina przez obrońców jak masło. Idzie w kierunku na Vladlenę.
Vladlena uśmiechnęła się z triumfem. Wygrała. W technomantycznej rękawicy trzyma kostkę Spustoszenia.

Vladlena zagroziła Wiktorowi, że ma zamiar się Spustoszyć i zniszczyć jego. On przegrał. Wiktor zauważył, że 'lena Spustoszy magów Kurtyny. Czarodziejka o to nie dba. On nie wygra i on jej nie dostanie. Wiktor Sowiński otworzył jeszcze jeden portal sprowadzając tu moc Węzła i Skażając obszar oraz stwierdził, że woli się wycofać (i stracić Adriana) niż dopuścić do tego, by Spustoszenie rządziło tym obszarem. Vladlena zaśmiała się z triumfem.

Dionizy korzystając z okazji i widząc, że Wiktor się wycofuje podskoczył i zwinął Vladlenie Spustoszenie oraz schował je do lapisowanego worka. Vladlena "lost it". Wydała rozkaz zatrzymania i obezwładnienia Dionizego; ten jednak biega szybciej a płaszczki jego...

Tyle, że zostawił Adriana Murarza.
Pełen raport dostał Hektor.

"Leokadia była tą nienormalną, tak?" - Edwin zdziwiony do Hektora widząc działania Vladleny.

Hektor i Edwin powiedzieli o Spustoszeniu profesorowi Ottonowi Blakenbauerowi. Spustoszenie zostało umieszczone w nowo storzonym pomieszczeniu Rezydencji i chwilowo sprawa została zamknięta.

Plan Vladleny jest niemożliwy do zrealizowania bez Spustoszenia. Sama czarodziejka padła do Skażenia i stymulantów. Kurtyna się obroniła, ale bardzo wysokim kosztem - stracili plan oraz nie są w stanie utrzymać tego terenu; trzeba go odkazić. A Kurtyna chwilowo nie ma środków na odkażenie tego terenu.

Hektor przejął inicjatywę; przechwycił Vladlenę i rannych magów Kurtyny do Rezydencji. Tu są bezpieczni (a Edwin może sprawdzić to co chce). Teren jest zabezpieczony przez płaszczki. Niestety, trzeba będzie zapłacić za odkażenie...

Pat i impas.

## Dark Future:
### Actors:

Vladlena Zajcew (Warmonger)
Wiktor Sowiński (Dominator)
Diana Weiner (Cult Speaker)
Edwin Blakenbauer (Sentinel)

### Faza 1: Budowa Exposera

- Vladlena buduje Exposer w "NeWorldzie" \| VZ.(construct an offensive firebase)
- Edwin ostrzega Hektora o sytuacji politycznej \| EB.(warn of impending problems)
- Diana nawiązuje kontakt z Hektorem \| DW.(offer friendship)
- Adrian Murarz prosi Vladlenę by tego nie robiła \| WS.(make a show of his power)
- Vladlena intensyfikuje X \| VZ.(force <Szlachta> to make a bad move)

### Faza 2: Obrona hipermarketu

- Vladlena odpowiednio przygotowała pole bitwy \| VZ.(erect fortifications)
- Wiktor informuje Vladlenę, co czeka ją i innych \| WS.(announce future badness; Vladlena loses it)
- Diana pokazuje, że Vladlena poświęci ludzi na polu bitwy dla ochrony X \| DW.(show hollowness of <Vladlena's> existence)
- Wiktor pokazuje, że Vladlena zaatakuje sojusznika \| WS.(force an <Vladlena> to do something wrong)
- Vladlena uderza i odbija Andriana Murarza \| VZ.(deal a vicious hit)
- Diana pokazuje, że Vladlena nie ufa Hektorowi przez wstrzymywanie sił \| DW.(use <Vladlena> as a weapon)
- Vladlena zmusza Szlachtę do wycofania się przez odpalenie pola nullifikacji \| VZ.(take advantage)
- Edwin pyta Hektora, czy o to mu chodziło \| EB.(question the results of a plan)

# Streszczenie

Vladlena buduje broń opartą na Spustoszeniu mającą ujawnić Szlachtę przez hipernet i zmusić Wydział Wewnętrzny do działania - ale nie Śląski (bo nie ufa). Koszt jednak taki, że Spustoszenie poznałoby tożsamość WSZYSTKICH magów Świecy powiązanych hipernetem. I wykorzystuje płaszczki Blakenbauerów jako obronę tego, zmuszając Blakenbauerów do wyboru "Szlachta lub Kurtyna". Ujawnione są frakcje - czym jest Szlachta, czym Kurtyna. Podczas wielkiej bitwy o hipermarket Vladlena zagroziła Wiktorowi Spustoszeniem; on się wycofał. Dionizy zwinął Vladlenie Spustoszenie i uratował kogo się da z Kurtyny (łącznie z 'leną). W walce teren został Skażony.

# Lokalizacje:

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Obrzeża
                        1. Rezydencja Blakenbauerów
                    1. Nowy Kopalin
                        1. Plac budowy hipermarketu "NeWorld"

# Zasługi

* mag: Hektor Blakenbauer, który zaczyna wątpić, czy Szlachta czy Kurtyna są gorsze. Obie siły dają mu do myślenia. Chwilowo Kurtyna jest w Rezydencji.
* vic: Dionizy Kret, taktyk, który wykorzystał plan Wiktora przeciwko niemu samemu; porwał Adriana i Spustoszenie.
* mag: Vladlena Zajcew, z desperackim planem użycia Spustoszenia który miał szanse powodzenia... i gdyby się udało, to to byłoby straszne.
* mag: Diana Weiner, bardzo sympatyczna i dążąca do pokoju (jak twierdzi) szalona kultystka (zdaniem Vladleny). Hektor ją lubi.
* mag: Wiktor Sowiński, niezły taktyk ze świetnym pomysłem i groźny wojownik, który wolał się wycofać niż dopuścić do Spustoszenia obszaru.
* mag: Edwin Blakenbauer, który martwi się całą tą sprawą do tego stopnia, że zaczął współpracować z Hektorem (co się udało).
* mag: Margaret Blakenbauer, zadająca trafne pytania i współpracująca z Hektorem katalistka. Rozpoznała akwarium komunikacyjne.
* mag: Adrian Murarz, kiedyś * mag Kurtyny i przyjaciel Vladleny; teraz agent Szlachty, którego Vladlena chce rozpaczliwie odzyskać.
* czł: Anna Kajak, która zobaczyła, że coś się dzieje na terenie hipermarketu i zgłosiła to Hektorowi by być odesłaną do czegoś nieważnego.
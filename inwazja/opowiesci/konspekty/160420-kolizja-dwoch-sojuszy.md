---
layout: inwazja-konspekt
title:  "Kolizja dwóch sojuszy"
campaign: powrot-karradraela
gm: żółw
players: kić, dust, dzióbek
categories: inwazja, konspekt
---

# {{ page.title }}

## Wątki

## Kontynuacja

### Kampanijna

* [160412 - Spleśniała dusza terminuski (SD)](160412-splesniala-dusza-terminuski.html)

### Chronologiczna

* [160406 - Najprawdziwszy sojusz Blakenbauerów (HB, KB, AB, DK)](160406-najprawdziwszy-sojusz-blakenbauerow.html)

## Kontekst ogólny sytuacji

- Emilia rzuciła bombę - magitech Wiktora ma przydatną wiedzę, klucz do zniszczenia Szlachty. Siluria najpewniej go nie uzyska ;-).
- Laboratoria Blakenbauerów mogą być wykorzystane przez Szlachtę za zgodą Hektora i z planami dla Blakenbauerów.
- Diakoni zablokowani przez Mojrę; silny sceptycyzm co do działań Blakenbauerów (thanks, Romeo). Dzięki działaniom Leonidasa problem Blakenbauerowie - Szlachta.
- Pancerz Tamary i "kołysanka". Powiązanie ze Spustoszeniem?
- Oktawian, Adela Mausowie i Dominik Bankierz zniknęli lub zginęli na wykopaliskach
- Baltazar Maus będzie poświęcony na ołtarzu Rodziny Świecy i bezpieczeństwa (nie w tej misji).
- Milena Diakon widziała Spustoszenie (?). Skąd i jak?
- Ktoś napuszcza na siebie Świecę i KADEM, Kurtynę i Szlachtę... wszystko zaczyna ze sobą walczyć.
- Ciągła plaga Irytki Sprzężonej. Mechaniczni terminusi i viciniusy trzymają porządek.
- Emilia skonsolidowała siły Kurtyny dookoła siebie; Dagmara dowodzi siłami Szlachty. Blakenbauerowie są powiązani ze Szlachtą bardziej niż z Kurtyną.
- Ozydiusz Bankierz pracuje nad cruentus reverto; nie ma jednak czasu, bo non stop są kryzysy. Uruchomił mechanicznych terminusów.
- Blakenbauerowie dali radę zdobyć dowody dla pogłaskania Ozydiusza; też zdobyli materiały i surowce częściowo z Kopalina używając płaszczek.
- Blakenbauerowie współpracują z Millennium; dążą do rozpadu monopolu Srebrnej Świecy w Kopalinie. Diakoni mogą współpracować / walczyć z Blakenbauerami.
- W skrócie, Wiktor chce iść w kierunku na Overlord Supreme i przejąć Kopalin. A jeśli ma do tego oddać jego część Millennium, niech tak będzie.
- Mirabelka Diakon (Millennium) jest opłacana przez Wiktora i ma za zadanie pomóc Wiktorowi zdobyć kontrolę nad magami poza Kopalinem.
- Amanda Diakon / Pasożyt (Millennium) powoli wchodzą na teren Kopalina pod pozorem sił Mirabelki. Jej cel to Ozydiusz i CruentusReverto oraz Milena.
- Silny chaos na poziomie Świecy a na pewno na poziomie Diakonów przez Arazille i Irytkę.
- Ozydiusz trzyma wszystko w garści żelazną pięścią; ukrywa Spustoszenie (pojawiło się przy śmierci na wykopaliskach) i informacje o nim.
- Rodzina Świecy vs Mausowie wchodzą w kolejną fazę eskalacji. Elea i Rufus uciekają się do drastycznych działań; seiras Maus nie pomaga. Bankierze vs Mausowie.
- Marcelin jest oficerem łącznikowym u Ozydiusza. Ozydiusz / Blakenbauerowie mają sojusz; ale po ostatnich ruchach płaszczek sojusz jest niepewny. Blakenbauerowie złamią sojusz.
- Ozydiusz ma przeciwko sobie: Szlachtę (bo Leonidas), Blakenbauerów (bo Leonidas # Millennium), Millennium Amandy (bo CruentusReverto i Milena), Millennium Mirabelki (bo współpraca ze Szlachtą). Ma po swojej stronie: Emilię Kołatkę, Kompleks centralny (lojalistów). Yay.

## Punkt zerowy:

Ż: Jaki budynek kazał ufortyfikować Ozydiusz w Dzielnicy Owadów w Kopalinie?
D: Typowy PRLowski blok z żelbetu.
Ż: Jakie jest znaczenie strategiczne tego budynku dla Świecy?
K: Transmitery na szczycie tego budynku; przekaźniki energii do Kompleksu centralnego.
Ż: Dlaczego zdobycie tego budynku przez siły Millennium postawi Ozydiusza w paskudnej sytuacji?
B: Pasożyt jest w stanie wykorzystać te przekaźniki.

## Misja właściwa:

Wątek poboczny:
Pewnego dnia:

Paulina dostała komunikat od Olgi. Telefon. Olga ostrzegła Paulinę, że "uruchomiono Krwawą Kuźnię". Ona nie wie gdzie, nie wie co to jest, zna tylko nazwę przekazaną jej przez Iliusitiusa w snach. Krwawa Kuźnia jest gdzieś koło Kopalina, ale jest doskonale zamaskowana. Nie da się tego cholerstwa wyczuć; Olga wie to dzięki temu, że "krew żąda wróżdy". Olga opowiedziała Paulinie o pladze, o kwarantannie i o tym, że ogólnie jest problem. Paulina nie może wracać; nie z Nikolą. Paulina nacisnęła na Olgę odnośnie jakiejkolwiek lokalizacji; ta odpowiedziała niechętnie, że Czelimin jest podejrzany. Wysłała tam już oddział, ale zawiódł. To znaczy że albo nic tam nie ma, albo zwyczajnie oni byli dość niekompetentni.

Paulina skontaktowała się z Kajetanem. Gdzieś gdzie jak w Przodku magia nie działa się spotkali (Dzierżniów). W gospodzie "Smalec". Paulina spytała go o "krwawą kuźnię". Kajetan z niczym takim się nie spotkał, choć brzmi to paskudnie. Po wyłuszczeniu przez Paulinę sytuacji, Kajetan powiedział jej brutalnie, że on musi to sprawdzić. Gdy Paulina powiedziała jaka to skala, Kajetan zauważył:
- on nie jest terminusem lokalnym i nie ma szczególnych znajomości w żadnej z gildii
- w Kopalinie jest panika; objawiła się Arazille, Irytka, plotki wojen gildii, plotki o Spustoszeniu
- Kuźnia, jakkolwiek "krwawa", jest niewykrywalna lub nie wykryta
On nic nie może zrobić bez dowodów. Zdobędzie backup i spróbuje zapoznać się z sytuacją.

Gdy Paulina powiedziała Kajetanowi o tym, że ma do czynienia z istotą o poszerzonej percepcji, Kajetan skorelował to z niedawną manifestacją Arazille i spytał Paulinę mocno, czy ona nie współpracuje z Arazille lub jej istotami. (14v11->win) Paulina zaczęła troszeczkę mieszać, ale pod karzącym spojrzeniem Kajetana po prostu wybuchła płaczem. Zaczęła prosić by nie pytał - to nie Arazille, ona przysięga - ale nie może powiedzieć. Po prostu nie może. W Kajetanie walczy ze sobą terminus i po prostu dobry mag... (12v14). Kajetan ustąpił. Jest przekonany, że Paulina wie więcej, ale nie naciska. Kajetan zaczął ją pocieszać i kazał jej uważać na siebie.

Kajetan nie zdradzi nikomu źródła, zrobi research i uda się na miejsce ze wsparciem.
Paulina jest relatywnie zadowolona...

Dzień 1:

Klara wróciła przez Bramę do Rezydencji. Poszła odbudowywać sieć czujników dla Hektora...
I Leonidas zawarł sojusz z Millennium.
Klara poszła z Margaret odbudowywać sieć czujników...

Hektor dostał prośbę od Elei Maus - zadzwoniła przez telefon. Poprosiła o telekonferencję technomantyczną. Hektor poszedł porozmawiać z Klarą. Klara jest bardzo wściekła; przez Hektora zmarnowali sporo czasu i stracili czujniki. Hektor jakoś Klarę ubłagał... zestawiła mu połączenie technomantyczne.

Elea Maus powiedziała o problemie Baltazara Mausa i o tym, jak ród Bankierz chroni Malię. Poprosiła Hektora o sprawiedliwość i powiedziała, że ma dowody - może je przesłać, ale musi powiązać telefon Hektora z drukarką. Hektor poszedł do Klary... która wściekła się zgodziła. Ale powiązała drukarkę z martwym gołębiem. Za karę.
Po tym, jak Margaret zasugerowała, że Marcelin i Siluria mieli coś wspólnego, Hektor szybko się ewakuował.

I faktycznie, wydrukowane dowody są dowodami pośrednimi i poszlakowymi. Nie ma to jak Mordecja Diakon.

Tymczasem Klara w końcu skończyła z czujnikami. Z Margaret wysłała swoje czujniki w powietrze. A tam - anioły. Anioły, które blokują ruch powietrzny. Klara zdążyła odciąć; ogólnie, lord terminus Ozydiusz Bankierz wprowadził zasadę, że zaklęcia i wszystkie byty magiczne muszą być pod kontrolą Aniołów. W innym wypadku gigantyczna grzywna.

Klara zdecydowała się przekazać główną kontrolę nad czujnikami Ozydiuszowi. Zdaniem Klary - Blakenbauerom będzie najlepiej po drodze ze Świecą. Świeca jest tym, gdzie Blakenbauerowie najwięcej wygrają. Ozydiusz zwrotnie umożliwił Klarze dostęp do danych z sieci.

Z Hektorem skontaktował się Wiktor Sowiński i powiedział, że cieszy się niezmiernie, że Hektora widzi. Powiedział, że "lady Diakon" (Amanda) powiedziała mu o sojuszu i bardzo się cieszy tym faktem. Niedługo powinna się lady Diakon z nim skontaktować. Hektor zaaprobował.

Z Leonidasem skontaktowała się Amanda i powiedziała, że potrzebuje wsparcia - jest budynek z transmiterem i trzeba wprowadzić tam Pasożyta. Hektor już wie (Wiktor jej powiedział). Na ich koszt idzie kampania marketingowa naprawiająca wizerunek Blakenbauerów, dostają Blakenbauerowie teren... ogólnie dość sporo cennych rzeczy. Ale Amanda zażądała zaatakowania ufortyfikowego przez Anetę Rainer budynku. Za trzy dni. Uwaga na anioły.

Leonidas poszedł porozmawiać z Hektorem. Wyjaśnił, jak wygląda sprawa z sojuszem. Potem spotkali się z Klarą...

Amanda dodała - chodzi o rodzinę. Ona chce odzyskać cruentus reverto oraz chce odzyskać Milenę Diakon. Należą do niej. Musi zmusić Ozydiusza do działania. Nieważne jak. Amanda zauważyła też, że informacja o tym, że Blakenbauerowie są w sojuszu z Millennium i ze Szlachtą przeciw Ozydiuszowi nie będzie możliwa do ukrycia. Nie w ten sposób. Nie tak...

Po co to wszystko? Tymotheus chce destabilizacji Srebrnej Świecy. Jeśli Świeca nie patrzy, on będzie w stanie zasiać ziarno nowej Rezydencji.

Plan Leonidasa jest "dość prosty". Wystawić Ozydiusza Amandzie gdzieś poza Kompleksem tak, by ta mogła go zdjąć a Leoś i Hektor wejdą w formę Bestii. A Hektor jest tam jako mediator między Ozydiuszem a Amandą. Amanda go porwie i wymieni.
I Hektor i Marcelin są niewinni; oni nic nie wiedzą. Działają w dobrej wierze.
Do Amandy prośba - nagonka medialna na jej prawdziwe cele, ma być oficjalna skarga itp. I pokazanie Świecy, że ona ma realne powody.

Dzień 2:

Biuro Lorda Terminusa nie robi niczego szczególnego. Nie wypowiadają się, nie mają co powiedzieć. Millennium zaczyna wycofywać magów Świecy przez swój portal; jednocześnie zagrażają wielu kluczowym obiektom w mieście, uniemożliwiąc Ozydiuszowi skupienie sił by ich zniszczyć. Millennium wycofuje ich po cichu, 2-3 dziennie. Ozydiusz nie może uderzyć sam. Millennium to ufortyfikowało po swojemu.

Marcelin skomunikował się z Hektorem. Trzeba pomóc Hektorowi. Estrella Diakon powiedziała mu, że Millennium porywa magów. Marcelin proponuje atak i uderzenie by się wykazać przed świetnym sojusznikiem (Ozydiuszem).

"Marcelinie, umów nas na spotkanie z Ozydiuszem."
"Lordem terminusem dla ciebie."

Hektor przez Marcelina próbował się skontaktować z Ozydiuszem bezpośrednio. Jednak nie chce spotkać się w Kompleksie, jeśli za dzień czy dwa będzie zmuszony wytoczyć Ozydiuszowi sprawę sądową. Czyli trzeba poczekać...

Dzień 3-4:

Czas na plotki. Jolanta Sowińska przeprowadza wywiad z Amandą Diakon, w którym to łzawym wywiadzie Amanda powiedziała, że stała się im krzywda. Wyszła sprawa Mileny Diakon, wyszła sprawa Malii Bankierz / Baltazara Bankierza.
A jednocześnie Leonidas rozpuszcza plotki i wrażenia, że Bankierze jako ród są dość 'forceful'. Nadużywają swojej pozycji jako Wielkiego Rodu w Świecy (na przykładzie Malii / Baltazara). Parę dodatkowych przykładów. Wojmił, który chodzi i pozywa losowych magów o śmierć Dominika. Inne takie. Prawo służy Bankierzom, nie oni prawu.
Plus, czarna propaganda Millennium dalej działa.
Plus, pojawia się parę głosów odnośnie "Świeca przygotowuje się do zajęcia stanowiska". A Hektor wygląda na zapracowanego i strapionego.
Szlachta bardzo publicznie zaprzecza. Bardzo gorliwie. Bronią go jak najbardziej są w stanie.
Jeden z magów pracujących dla Szlachty zgłasza Ozydiuszowi, że do prokuratury wpływają dowody i jest tego coraz więcej.
Millennium zajęło Muzeum Śląskie.

Dzień 5: 

Marcelin skontaktował się z Leonidasem. Powiedział, że są przebrzydłe plotki na temat Ozydiusza. Niech Leonidas pomoże. Leo wyjaśnił, że nie ma możliwości tego sensownie rozwiązać; jedyny sposób jak można to powstrzymać to spróbować jakoś z Hektorem to załatwić. Marcelina martwi to, że idzie to w Malię Bankierz a nie w lorda terminusa. 

Leonidas wpadł na dobry pomysł. Wojmił Bankierz skrzywdził maga Świecy; w odpowiedzi kilku (wspieranych przez Szlachtę) magów Świecy sklupało bardzo niewinnego Bankierza którego jedyną winą było nazwisko. W odpowiedzi na to Świeca zdecydowała się zrobić audyt rodu Bankierz pod kątem działań takich jak robi Wojmił i terminusów; zmianie szczególnie dopingowali Zajcewowie.

Millennium, Szlachta i Zajcewowie robią skoordynowany atak na pozycje Bankierzy. W rodzie Bankierzy zaczynają się naciski na Ozydiusza. Wiktor Sowiński skupił się szczególnie na Malii ze stwierdzeniem "ona będzie następna". 

Ozydiusz musiał zrobić ruch.
Gdy Marcelin spotkał się z Hektorem, ten powiedział Marcelinowi, że może porozmawiać z Amandą; ale musi od Ozydiusza wiedzieć co i jak.

W trakcie negocjacji ustalili, że Amanda spotka się z Ozydiuszem w "Dziale Plazmowym", w dzień (by nikt nic nie wiedział i nikogo nie śledził). Porozmawiają o warunkach współpracy; gdy Irytka szaleje nikomu nie zależy na walce z Millennium. Klara zbudowała szrapnele i Borys zamontował je w Dziale Plazmowym.

Plan był prosty i w swojej naturze dobry:
- Ozydiusz wchodzi do Działa Plazmowego z Blakenbauerami.
- Odpala szrapnel mający wywołać w Hektorze, Marcelinie i Leonidasie formę Bestii.
- W zamieszaniu i walkach Amanda ma się pojawić i ukraść Ozydiusza.
- Potem siły Millennium mają opanować Bestie.

Amanda dostaje co chce, Ozydiusz jest zdjęty z akcji a Blakenbauerowie są faktycznie niewinni. Polityka.

I się to stało. 

Leonidas i Hektor poszli do Działa Plazmowego. Przyszedł też Ozydiusz z Marcelinem i Sabiną Sowińską jako eskortą.
I odpalił szrapnel...

(w tej chwili stan nieoznaczony; dowiemy się co się stało na następnej misji)


## Wynik misji:

- Sprawą Malii i Baltazara zajmuje się Hektor Blakenbauer
- Pojawia się silna nagonka na Bankierzy; szczególnie podchwycona przez Zajcewów
- Blakenbauerowie, Szlachta i Millennium zakładają pułapkę na Ozydiusza

## Dark Future:

- Elea prosi Hektora o to, by ten dołączył Malię Bankierz do równania.
- Amanda skomunikowała się z Wiktorem i powiedziała, że Blakenbauerowie są po jej stronie.
- Amanda zaczyna proces transferu magów spoza Kopalina.
- "Hektor" ukradł drogą kolię sędzi Janinie Strych.
- Ozydiusz przejmuje kontrolę nad powietrzem Kopalina.


# Zasługi

* mag: Paulina Tarczyńska, która usłyszawszy niepokojące wieści od Olgi (zwłaszcza o krwawych kuźniach) zaangażowała w temat Kajetana Weinera.
* mag: Hektor Blakenbauer, który przejął sprawę Baltazara Mausa i Malii Bankierz, też lawirował między Ozydiuszem, Amandą a Wiktorem.
* mag: Klara Blakenbauer, która odbudowała system detekcyjny i zdecydowała się wesprzeć Świecę z przyczyn politycznych.
* mag: Leonidas Blakenbauer, który wymyślił jak NIE złamać dwóch całkowicie sprzecznych ze sobą sojuszy. Też: zmontował nagonkę na Bankierzy by złapać Ozydiusza.
* czł: Olga Miodownik, której Iliusitius kazał ostrzec * magów; przekazała Paulinie to, co "widziała" najlepiej jak jest w stanie.
* mag: Kajetan Weiner, który domyślił się, że Paulina nie mówi mu całej prawdy, lecz zdecydował się jej pomóc (acz powoli i ostrożnie).
* mag: Amanda Diakon, która wmanewrowała Blakenbauerów w zaatakowanie przez nich budynku z przekaźnikami energii; odstąpiła mogąc zastawić inną pułapkę.
* mag: Aneta Rainer, broniąca budynku z przekaźnikami energii zasilającego Kompleks centralny (który Amanda chce zaatakować). Nikt nie chce tego atakować...
* mag: Ozydiusz Bankierz, uparty i bezwzględny, nie dał się złapać w pułapkę, acz pod presją i gdy jego ród zaczął być atakowany poszedł spotkać się z Amandą.
* mag: Elea Maus, która zdeterminowana jest albo uratować Baltazara albo spalić Malię. Obie wersje pasują zdesperowanej matce.
* mag: Wiktor Sowiński, tworzący szeroki sojusz Szlachta - Blakenbauerowie - Millennium przeciwko lojalistom Srebrnej Świecy.
* mag: Marcelin Blakenbauer, dla odmiany dobry oficer łącznikowy między Ozydiuszem a Blakenbauerami. 
* mag: Tymotheus Blakenbauer, grający na osłabienie Srebrnej Świecy by posadzić ziarno nowej Rezydencji.

# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Leere
                1. Dzierżniów
                    1. Ulica Centralna
                        1. Gospoda Smalec
            1. Powiat Kopaliński
                1. Kopalin
                    1. Centrum
                        1. Cyberklub Działo Plazmowe
                        1. Kompleks Centralny Srebrnej Świecy
                    1. Dzielnica Owadów
                        1. Blok z żelbetu z przekaźnikiem energetycznym SŚ
                    1. Obrzeża
                        1. Rezydencja Blakenbauerów
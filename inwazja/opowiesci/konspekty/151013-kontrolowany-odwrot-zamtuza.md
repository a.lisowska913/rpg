---
layout: inwazja-konspekt
title:  "Kontrolowany odwrót z zamtuza"
campaign: ucieczka-do-przodka
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [151003 - Zamtuz przestaje działać (PT)](151003-zamtuz-przestaje-dzialac.html) 

### Chronologiczna

* [151003 - Zamtuz przestaje działać (PT)](151003-zamtuz-przestaje-dzialac.html) 

## Punkt zerowy:

Ż: Dlaczego Janusz poszedł do lasu?
K: Po grzyby.
Ż: Jaka informacja może boleśnie uderzyć w reputację Gali w Kopalinie?
K: Ujawnienie, że to ona stała za zamtuzem z wiłą.

## Kontekst misji

Wiła jest usunięta z akcji. Gala i Tymek orientują się, że... no nie mają wiły. Rozpoczynają procedurę wycofania się (nie pasuje to Tymkowi, ale Gala płaci i wymaga). Gala wprowadza Czyściciela.
Tymczasem wiła jest stosunkowo niebezpieczna; uzależniona, chce dostępu do środków i nie może się po prostu oddalić.
Uderza artykuł Marii; lokalny bieda-gangster (Franciszek Marlin) stwierdził, że może skorzystać z okazji i zasłużyć sobie na przychylność szefostwa i ściągnąć korzyści z nielegalnego zamtuza.
Sytuacją zaczyna interesować się policja; wprowadzony jest znajomy Marii, czyli Artur Kurczak, który zaczyna interesować się sytuacją. Nie pasuje to NIKOMU.
Tymczasem Paulina wykryła coś bardzo dziwnego w szpitalu, akumulator magiczny (Null Field).
## Misja właściwa:
### Faza 1: Artykuł Marii

Rano Paulina nie jest wyspana. Jest szczególnie niewyspana. Ale nie musi iść do pracy, więc się nie przejmuje; śpi po prostu dłużej.
Ogromną radość Paulinie sprawił artykuł. Opisuje zamtuz dla pedofili. Nielegalne zatrudnienie, przymus kobiet zza granicy, ściąganie dzieci z klubów... Maria się postarała.

Paulina pojechała do Gęsilotu, od razu na plebanię. Chce z dystansu wyczyścić swoje ślady magiczne. Ale orientuje się w jednej rzeczy - co nie zrobi, nie jest w stanie poradzić nic na to, że ksiądz będzie ją pamiętał. Na szczęście tu może pomóc jej Maria edytując pamięć. Tak zatem zrobią. Sama pojechała (księdza nie ma na plebanii) i zaklęciem zniszczyła ślady magiczne. Niestety, podczas mazania, Przodek wpłynął na jej magię; powstała niefortunna eksplozja. Paulina zaczęła lekko świecić, choć samo zaklęcie jest perfekcyjnie zamazane.

Skupiona na sobie Paulina zdążyła jeszcze skryć się w krzakach gdy nadszedł Tymek Maus. Mag bardzo przejęty, lekko zestresowany, nie ma wiły. Poszedł na plebanię, zapukał i zapytał czy ksiądz miał może gości; szuka takiej jednej kobiety i czy może jej nie było a najlepiej w nocy. Gospodyni się oburzyła, że jak to tak, jak on śmie insynuować. Już dość że mają zamtuz, ich ksiądz właśnie pisze listy do województwa i prezydenta że zamtuz... Tymek przestraszony i smutny się wycofał. A Paulina cichutko chichocze w krzakach.

Tymek się rozejrzał i rzucił zaklęcia detekcyjne, co tu się stało, o co chodzi i gdzie jest jego wiła. Napotkał bardzo zamazaną magię i się zafrapował. Tu był mag. Co tu się działo. Paulina korzystając z okazji zdecydowała się wycofać jak najszybciej; już prawie prawie jej się udało, ale Tymek ją zauważył i zagadnął. Paulina lekko spłoszona się zatrzymała z nim pogadać. Nieszczęśliwa Paulina. Udaje niezrozumienie i zainteresowanie. Tymek Maus pyta o wiłę, ale nie umiał zadać pytania; w akcie desperacji Zdominował Paulinę magią i zaprowadził ją do zamtuza.

Gala przywitała Tymka chłodno. Co on narobił. Tymek powiedział, że przecież miał znaleźć wiłę... więc przywiózł człowieka, który ma jakąś wiedzę. Gala wzgardziła; to lokalna lekarka ze szpitala. I Gala przypomniała Tymkowi o specyfice lokalnej. Przecież magia jest nietrwała. Nawet Quarkowana. Tymek się zmartwił - czyli ona będzie pamiętać? Gala wzruszyła ramionami; od tego ma narkotyki. Paulina się nie ucieszyła... Tymek przesłuchał Paulinę magią i dowiedział się to, co Maria chciała by się dowiedział - lokalna lekarka która szła się pomodlić w nocy z uwagi na to że zmarł jej pacjent (miesięcznica) i ostatnie co pamięta to oczy wiły w nocy. Potem Gala i Tymek mieli ambitny spór na temat tego, czy wiła ma moce hipnotyczne (nie) i Gala kazała Paulinie wstrzyknąć sobie narkotyki.

Halucynki, wysoka temperatura, fatalne efekty uboczne... jazda jak cholera.
Paulina nie doceniła.

HIDDEN MOVEMENT

- Uderza artykuł Marii; wszystkie niewłaściwe osoby mają okazję go przeczytać \| Maria.(publish an article)
- Gala chce się wycofać, ale jest wiła \| Gala.(stop throwing good money after the bad; abort / sacrifice [something])
- Tymek protestuje, chce utrzymać Węzeł \| Tymek.(technobabble an audience)
- Ksiądz zaczyna akcję przeciwko zamtuzowi \| TLeżniak.(denounce opponents / name evil)
- Marlin odwiedza magów w Gęsilocie żądając haraczu i dostosowania \| Marlin.(manipulate [someone] in order to profit)
- Gala ściąga Krystiana Korzunio, cleanera \| Gala.(appear with defending mercenaries)

END OF HIDDEN MOVEMENT

Następnego dnia Paulina dzielnie chorowała pod opieką Marii; nie chciała do szpitala. Musiała przedłużyć urlop o jeden dzień. Tego nie może darować...
Paulina wie o nietrwałości magii, ale wie jak działa jej ciało. Użyła magii by się jakoś podczyścić. Zwyczajnie nie chce narażać pacjentów a nawet nietrwała magia może pomóc.

### Faza 2: Pechowy grzybiarz.

Dwa dni później Paulina poszła do szpitala, do pracy. Przywieźli ciężki przypadek; facet wygląda, jakby przeszedł przez grupę nożowników. Znaleźli go w lesie, w Gęsilocie. Wygląda na to, że wiła go pocięła, poszarpała... ale się wstrzymała i nie zabiła. Nieszczęsny grzybiarz zresztą majaczy o kobiecie z nożami zamiast palców...
Bardzo trudny przypadek; gdyby Paulina nie miała wspomagania magicznego, gdyby nie ten szpital, to grzybiarz by nie przeżył. Nie miałby ŻADNYCH szans. Po typie ran i obrażeń Paulina zobaczyła coś co kojarzyło jej się z defilerem; wiła zadawała rany by nieszczęśnik cierpiał. I ruchy wiły były desperackie, to nie było działanie zaplanowane.
Ogólnie, dziwne.

Paulina ma obserwację - wiła jest rozpaczliwie głodna. Nie chce zabijać, nie chce robić różnych rzeczy... ale Przodek ją zabija. I wiła jest głodna i nie może odejść z terenu (dlaczego?). Powinna być w stanie odejść; jednak z jakiegoś powodu tego nie robi. Coś jeszcze ci magowie zrobili wile, a Paulina nie wie co i nie wie jak jej pomóc - a wiła jest już skazana na odstrzelenie.

Nagle - Paulinę opuściły siły i zaklęcie wspomagające. Coś wyssało ją z zaklęć wspomagających. Coś w tym szpitalu ją dispellowało... wyżarło. Paulina chciała pójść w tego kierunku, ale zatrzymało ją dwóch policjantów - Artur Kurczak i Bartosz Bławatek.
Policjanci prosili Paulinę o informacje na temat nieszczęśnika i co się mogło dziać (ile napastników, jaka broń). Maria szybko przerwała Paulinie i powiedziała, żeby ta uważała na Kurczaka - ten gość często z Marią współpracuje w tematach lokalnych; ma pomysły, jaja i umie wyjść poza to co normalne i możliwe. Innymi słowy, bardzo niebezpieczny. Paulina powiedziała, że wszystkie rany są zadane jednym narzędziem i to wygląda jak niepraktyczne narzędzie do zadawania bólu; coś nakładanego na ręce. Takie "metalowe szpony". Materiał niemożliwy do określenia, ilość napastników nieznana, brak śladów szczególnych. A Janusz Karzeł (grzybiarz) będzie potrzebował wielu operacji plastycznych; szczęście, że twarz oszczędzono.
Policjanci podziękowali. Na wyjściu Paulina usłyszała, że mówili o sektach.

Maria potwierdziła - przez rozpad magii Maskarada jest wątlejsza bo "ludzie widzieli" przez co w Przodku jest więcej sekt i quasireligijnych działań niż normalnie.

Paulina skupiła się też na tym dziwnym rozpadzie jej zaklęcia - co do cholery się stało. Na bazie wiedzy katalistycznej doszła do tego że to nie było zaklęcie, które zostało zniszczone przez magię czy rozproszone. Jej zaklęcie po prostu przestało istnieć. Coś je "pożarło". Coś w szpitalu. Paulina zdecydowała się zrobić obchód i zobaczyć czy czegoś ciekawego nie znajdzie.

 ==== DONE ====
- Pojawia się Korzunio w Gęsilocie.
- Wiła uderza; ciężko rani człowieka, ale nie zabija \| Lea.(try to drown the addiction with different addiction)
- Paulina zauważa dziwną redukcję magiczną w szpitalu \| NullField.(destroy the [effect])
- Artur Kurczak odwiedza szpital pytając o rannego \| Kurczak.(ask questions close to truth about [sth])
- Gala zleca Tymkowi podanie wile narkotyków by ją uspokoić \| Gala.(supply [something] to [someone])
- Tymek idzie do lasu znaleźć wiłę lub jej krew i podłożyć narkotyki \| Tymek.(notice a [problem] nobody else sees)
 ==============

Paulina zdecydowała się rzucić małe, delikatne zaklęcie z nadzieją, że Null Field je tak samo pożre i będzie w stanie sprawdzić skąd jest ssanie. Próbkowanie bardzo delikatnymi impulsami magicznymi. Jednocześnie próbowała uniknąć wykrycia. [Wynik konfliktu: wykryła i została wykryta i rozpoznana]. Paulina dała radę zlokalizować miejsce, w którym jest Null Field - to jest coś w gabinecie doktora Jerzego Karmelika. Zgodnie z grafikiem, Karmelik jest w gabinecie.

Paulina korzystając z tego, że jest nowa, weszła, przedstawiła się i poszukała Null Fielda też tam; znalazła lokalizację. W gabinecie Karmelika znajduje się szafka z danymi i małymi obiektami; Null Field jest gdzieś tam, w tej szafce. To musi być niewielki obiekt, bo się zmieścił w szafce.

Paulina zdecydowała się jeszcze porozmawiać z Karmelikiem; z pomocą Marii spróbowała się od niego dowiedzieć, czy wie coś na temat tego co ma. Okazało się, że nie wie. Nie ma pojęcia. Paulina tylko dowiedziała się, że Karmelik jest strasznym flirciarzem (a jest przystojny) i w szafce przechowuje też rzeczy, które niekoniecznie są papierami i niekoniecznie do niego należą - ale zawsze były mu użyczone (if you know what I mean). Próbował poderwać Paulinę, ale Maria dała mu kosza ;p. Nie spodobał jej się; powiedziała, że to nie "Lekarze z Kopalina" czy inny serial.

A Paulina i tak się da kiedyś wyciągnąć na jakąś kawę ;p.

Po zakończeniu pracy, Paulina poszła z Karmelikiem na kawę i kolację (on stawia). Do niczego nie dochodzi, jest to spotkanie niezobowiązujące, ale Karmelik to straszny gaduła; i na to Paulina liczyła. Przy aktywnej pomocy Marii Paulina spróbowała z niego wyciągnąć czym jest ten Null Field, czym być może i kiedy go dostał. Jak najwięcej. Paulina wyciągnęła z Karmelika co następuje:
- Null Field jest trofeum; jedna z kobiet w szpitalu mu go dała. To musi być jedna z personelu szpitala; jedna z trzech kobiet.
- Null Field pojawił się za czasów jeszcze Adama Diakona. Ale Karmelik dostał go dopiero po tym, jak Diakon zniknął.
- Karmelik nie wie nic o magii.
- Paulina uzmysłowiła sobie, że Przodek niszczy magię. Nie ma pojęcia czaru trwałego w Przodku. A Null Field najpewniej żywi się i zasila się magią.
- Ktoś w szpitalu karmi Null Field. Sam w sobie by zaniknął przez własność Przodka.

Paulina, zadowolona, wróciła wieczorem do mieszkania. Tam czekała na nią Maria, z lekko okrutnym uśmiechem. Znalazła coś ciekawego - napisała krótki artykuł o tym, jak "młoda, musząca się wyszaleć" Gala Zajcew założyła wyjątkowo nieudany zamtuz w Przodku i pokazała ów artykuł jako coś, co zamierza wysłać do tabloidu w Kopalinie. Paulina się zgodziła - ale nie na aliasie Marii. Ta się zgodziła. Może nie zarobi (szkoooda), ale przynajmniej Gala dostanie za to, że skrzywdziła dzieciaki i chciała użyć viciniusa do swych niecnych celów.

Dodatkowo Maria powiedziała, że z jej źródeł czyściciel Gali Zajcew (Korzunio) przybył na ten teren i "na terenie Gęsilotu weszły młode wilki gangsterskie", takie nieopierzone i chcieli założyć zamtuz; spacyfikowani przez lokalną przestępczość decydują się trochę przycupnąć i przycichnąć. Taka historyjka czyściciela; co więcej, czyściciel wprowadził osobę mającą być "młodym wilkiem". Znalazł słupa. Maria jeszcze nie wie kto to i dlaczego został słupem - na pewno jest pozamiastowy.

Dodatkowo policja szukała śladów tajemniczej sekty w lesie. Też gość z zamtuza był w lesie.

 ========== DONE ============
- Kurczak idzie do lasu szukać śladów \| Kurczak.(search for clues in [location])
- Korzunio wprowadza "ambitne młode wilki" którym nie wyszło \| Korzunio.(create plausible explanation)
- Korzunio informuje Marlina, że sprowadził szefa \| Korzunio.(collude with local powers to hide [activity])
- Maria zdobyła dla Pauliny informacje o działaniach Korzunia. \| 
- Maria znajduje sposób, by Gala ucierpiała za te dzieciaki \| 
 ============================

Paulina ma dla Marii nowe zadanie: jak uratować wiłę? Zaproponowała, by dopisać o wile w artykule; a dokładniej tak, by nie napisać "wiła", lecz by magowie się byli w stanie zorientować. Paulina nie chce, by wiła została zabita. Mimo wszystko. Co wiła winna.

## Spekulacja:

Kić: 
Wszystko wygląda, że mag jest miejscowy albo chcący tu być, więc raczej nie ucieknie. Null Field jest ciekawym objawem, sugeruje, że mag raczej chce być na miejscu. Sugeruje też, że mag raczej dąży do oczyszczenia miejsca z magii. Być może NF ma inny cel; np. mag się napromieniowuje a NF ma go oczyszczać. Fakt, że lekarz dostał ten NF po odejściu Diakona świadczy, że ta osoba próbuje się ukryć przed innymi magami a Paulina nie ma po co go szukać. Czyli temat maga na razie jest nieistotny. Nie zagraża Paulinie wykryciem i nie zmienia niczego co istotne. Paulina jest ciekawa, ale nie jest to kluczowe; lepiej nie ruszać. Niech mag i Null Field sobie żyją; od tej pory Paulina bierze pod uwagę jego obecność i tyle.

Wiła. Zachowywała się przy kapłanie w sposób dość nienaturalny co sugerowało kontrolę; Paulina złamała kontrolę, co było "naturalne" i wiła zachowywała się tak jak powinna. Ale teraz zachowuje się znowu dziwnie. Tzn. Przodek najpewniej wykańcza wiłę do niemożności bo wiła jest głodna jak cholera. I tak cud że powstrzymała się przed zabójstwem grzybiarza... lub coś ją powstrzymało, na przykład ograniczenia wpisane w jej wzór. To sugeruje, że była celem inżynierii magicznej przez kogoś. To z kolei prowadzi do tego: czemu nie zwiała (do właściciela albo po prostu stąd). Paulina za mało wie o wiłach a Maria tego nie znajdzie. Jedyne źródło danych to Kasia Kotek, ale z nią niekoniecznie chcą się kontaktować. Ogólnie - wiłę warto uratować, bo co winna, ale nie za wszelką cenę.

Czyściciel. W sytuacji idealnej czyściciel ogarnia ślady i idzie w cholerę. Paulinie naprawdę to nie przeszkadza - ona chce żyć w spokoju. Ten czyściciel musi być kimś na usługach Gali lub Tymka, ewentualnie ich rodów. Na pewno jest groźny i na pewno wie o magii. Może dążyć do wyczyszczenia informacji z magów, choć mała szansa. 

Paulina. Na pewno warto, by Maria coś się dowiedziała o policji by móc przewidzieć ich ruchy i działania oraz podrzucić ślady; tu cele czyściciela i Pauliny są zgodne. Warto wiedzieć kto jest czyścicielem, by móc z nim współpracować z ukrycia i nie dopuścić do niczego głupiego. Kusząca jest próba przywabienia wiły, Quarkami na przykład... ale Paulina sama nie powstrzyma i nie pokona wiły, co psuje ewentualne plany. Marii tym bardziej nie puści. Podstawowy cel Pauliny to jednak identyfikacja czyściciela (nie wiadomo jeszcze jak). Potencjalnie zwinięcie całego majdanu, całego zamtuzu... Paulina chce po prostu, by wszyscy sobie poszli i by mogła działać w spokoju. Fajnie byłoby uratować wiłę, ale jeszcze nie wiadomo jak.

## Dark Future:

### Faza 1: Artykuł Marii

- Uderza artykuł Marii; wszystkie niewłaściwe osoby mają okazję go przeczytać \| Maria.(publish an article)
- Gala chce się wycofać, ale jest wiła \| Gala.(stop throwing good money after the bad; abort / sacrifice [something])
- Tymek protestuje, chce utrzymać Węzeł \| Tymek.(technobabble an audience)
- Ksiądz zaczyna akcję przeciwko zamtuzowi \| TLeżniak.(denounce opponents / name evil)
- Marlin odwiedza magów w Gęsilocie żądając haraczu i dostosowania \| Marlin.(manipulate [someone] in order to profit)
- Gala ściąga Krystiana Korzunio, cleanera \| Gala.(appear with defending mercenaries)

### Faza 2: Pechowy grzybiarz

- Pojawia się Korzunio.
- Wiła uderza; ciężko rani człowieka, ale nie zabija \| Lea.(try to drown the addiction with different addiction)
- Paulina zauważa dziwną redukcję magiczną w szpitalu \| NullField.(destroy the [effect])
- Artur Kurczak odwiedza szpital pytając o rannego \| Kurczak.(ask questions close to truth about [sth])
- Gala zleca Tymkowi podanie wile narkotyków by ją uspokoić \| Gala.(supply [something] to [someone])
- Tymek idzie do lasu znaleźć wiłę lub jej krew i podłożyć narkotyki \| Tymek.(notice a [problem] nobody else sees)
- Kurczak idzie do lasu szukać śladów \| Kurczak.(search for clues in [location])
- Korzunio wprowadza "ambitne młode wilki" którym nie wyszło \| Korzunio.(create plausible explanation)
- Korzunio informuje Marlina, że sprowadził szefa \| Korzunio.(collude with local powers to hide [activity])
- Maria zdobyła dla Pauliny informacje o działaniach Korzunia. \| 
- Maria znajduje sposób, by Gala ucierpiała za te dzieciaki \| 

# Zasługi

* mag: Paulina Tarczyńska, którą porwał Tymek i znarkotyzował, po czym uratowała życie grzybiarza i poszła na kawę i kolację z kolegą z pracy.
* czł: Maria Newa, której kończą się pieniądze i która pomaga Paulinie socjalnie, ale nadal ma zęby i umie ugryźć Galę.
* czł: Tomasz Leżniak, ksiądz w Gęsilocie, który organizuje ostre akcje zniszczenia zamtuza i nagłaśnia zamtuz ku utrapieniu wszystkich magów.
* vic: Lea Swoboda, uzależniona wiła, która miota się pomiędzy narkotykiem, mocą Przodka i wyniszczeniem. Prawie zabiła grzybiarza; nie powstrzymała głodu.
* czł: Janusz Karzeł, grzybiarz napadnięty przez wiłę i ciężko ranny; cudem uratowany przez Paulinę. Miał pecha być w niewłaściwym miejscu.
* mag: Tymoteusz Maus, który porwał Paulinę i dowiedział się, że tu magia nie jest trwała. Szuka śladów w lesie... i wiły. Ma nadzieję jej nie znaleźć.
* mag: Gala Zajcew, która chce wycofać się z Gęsilotu a najlepiej o wszystkim zapomnieć jak najszybciej. Nie będzie jej dane. Wezwała czyściciela.
* czł: Franciszek Marlin, drobny przestępca, który chce się wybić i próbuje wyjść przed szereg i zdobyć zamtuz dla lokalnego bossa. Biedny.
* czł: Krystian Korzunio, czyściciel Zajcewów, który próbuje posprzątać to co napaskudził Tymek i Gala. Zaczął od ściągnięcia "młodego wilka".
* czł: Artur Kurczak, policjant, przed którym Maria ostrzega Paulinę, że to Truthseeker. Poluje na wiłę i Paulina próbuje, by jej nie znalazł.
* czł: Bartosz Bławatek, policjant, który podejrzewa satanistyczne kulty "jak już się zdarzało". Nie chce iść do lasu ale Artur nalega.
* czł: Jerzy Karmelik, lekarz, w którego gabinecie (szafce) znajduje się NullField. Flirciarz i kobieciarz. Podrywa Paulinę na kawę. Straszny gaduła.
* czł: Maciej Orank, młody i ambitny wilk gangsterski, który zostanie słupem dla Korzunia by Gala i Tymek mogli się wycofać.

# Aktorzy

- Tymek; katalista (Engineer)
- Gala; kupiec (Merchant)
- Korzunio; czyściciel (Cleaner)
- Maria (Intrepid Reporter/Paladin)
- Franciszek Marlin; gangster (Proaktywny Oportunista)
- Lea; uzależniona wiła (Addicted, The)
- część mieszkańców Gęsilotu (Strażnicy moralności)
- Ryszard Herman (Truthseeker)
- Artur Kurczak (Sentinel/Truthseeker)

# Lokalizacje:

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Leere
                1. Przodek
                    1. Centrum
                        1. Szpital Gotycki
                    1. Równia Słoneczna
                        1. "Mrówkowiec"
                            1. Wynajęte mieszkanie Marii i Pauliny
                1. Gęsilot
                    1. Chata Pirzeców
                    1. Plebania gęsilocka
                    1. Las Skrzacki
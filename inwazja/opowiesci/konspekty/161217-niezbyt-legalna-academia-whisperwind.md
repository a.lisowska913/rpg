---
layout: inwazja-konspekt
title:  "Niezbyt legalna 'Academia' Whisperwind"
campaign: powrot-karradraela
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [161216 - Szept z Academii Daemonica (SD)](161216-szept-z-academii-daemonica.html)

### Chronologiczna

* [161216 - Szept z Academii Daemonica (SD)](161216-szept-z-academii-daemonica.html)

## Kontekst ogólny sytuacji
## Punkt zerowy:

## Misja właściwa:

Dzień 1:

Rano pukanie do drzwi Inferni... i Silurii ;-). Infernia jako lifeshaperka specjalizująca się w roślinach i perfumach zrobiła odpowiedni nastrój. Gdy Karina zapukała i dziewczyny ją zaprosiły, od razu się zaczerwieniła i zwiała. Ale Infernia wróciła ją. Porozmawiają.

Karina spytała Infernię, czy ta potrafiłaby zrobić kwiaty arstraccatis. Infernia się zdziwiła; kwiaty arstraccatis występują na Fazie, ale tego się nie 'hoduje'. Karina, zapytana przez Silurię, wyjaśniła, że chcą zniszczyć coś bardzo niebezpiecznego i do tego celu potrzebują naładowanych kwiatów arstraccatis. Dzięki temu będą w stanie zniszczyć efemerydę którą można zniszczyć tylko pozytywnymi emocjami. Infernia powiedziała, że arstraccatis są przecież w Ogrodach Weroniki; czyli trzeba porozmawiać z Wiolettą.

Siluria, chcąc docenić, że pomyśleli, zgłosiła się na ochotniczkę do pomocy Karinie. Porozmawiać z Wiolettą. Ale Karina poda kontekst rozmowy. Jak chodzi o Infernię - ona się zgodziła pielęgnować arstraccatis tak, jak robiła to do tej pory. Karina wycofała się z pokoju, zanim atmosfera się jej udzieli (zapachy itp).

...godzinę później Siluria się ubrała. Infernia jeszcze zauważyła jak już była sam na sam z Silurą, że trzeba jeszcze przecież arstraccatis naładować odpowiednimi emocjami. Nie wie, czy Karina zdaje sobie z tego sprawę.

Siluria poszła w końcu do Kariny - ta i tak już czekała. Karina wyraźnie wzięła zimny prysznic. Zapytana, Karina powiedziała, że Artur był tym, który łączył się z Wektorem Sigma. To był jego pomysł by sprawdzić tamtego viciniusa, arstraccatis itp. Karina nie wie wiele odnośnie arstraccatis - nie jej specjalizacja. Siluria poradziła jej, by jednak poszła sama sprawdzić do biblioteki. Karina potwierdziła, że to dobry pomysł i się oddaliła.

Siluria zastanowiła się nad problemem z Arturem - on wyraźnie nie zdaje sobie sprawy ze znaczenia tego, co robi. Pomyślała chwilę i poszła do Sławka Błyszczyka - mistrza astraliki. 

"Jest ktoś kto bardzo potrzebuje mieć koszmar" - Siluria, słodko
"Skarbie, jestem oficerem porządkowym na KADEMie..." - Sławek, spokojnie
"No właśnie!" - Siluria, z naciskiem
"O, zamieniam się w słuch." - Sławek, zainteresowany

Siluria powiedziała Sławkowi o swoim planie - niech Artur ma koszmar. Wszystko się udało jak chciał. Warmaster i Alisa zapałali do siebie prawdziwym, szczerym uczuciem. Potem przeszli na Primusa... i to minęło. I już nie chcieli. I bali się wrócić. Sławek się uśmiechnął. Podoba mu się pomysł Silurii. Obiecał, że to przejmie i zrobi prawidłowo; niech Siluria się tym już nie przejmuje. Tej nocy Artur będzie miał naprawdę paskudny koszmar i będzie można ocenić jakiego typu jest osobą.

Siluria się uśmiechnęła. Dostała na odchodne żelka (Shadow regularnie nosi ze sobą żelki).

Whisperwind zaprosiła Silurię, by ta obejrzała kilka fotografii. Siluria się zdziwiła... i ucieszyła. Jasne, czemu nie. Whisper zaczęła z nią kluczyć po KADEMie, odwracając jej uwagę... i w końcu jej się udało. Gdy Siluria była dość skupiona na tym, co Whisper mówi, ta przeprowadziła ją przez "uskok w Pryzmacie" - do miejsca 'obok' KADEMu na Zamku As'caen. Pokazała jej pokoje ze swoimi fotografiami - piękne fotografie robione z ukrycia różnym magom KADEMu. Wszystkie fotografie pokazują tych magów w pozytywnym świetle. Uwypuklają ich pozytywne cechy i charaktery. Widać, że dla niektórych fotografii Whisper musiała poświęcić godziny.

Whisper poprosiła Silurię, by ta zbudowała na KADEMie poparcie dla zalegalizowanie 'tego' kawałka przestrzeni jako Academii Daemonica, jako element KADEMu. 

Oprowadziła Silurię po "Academii", tłumacząc różnice między KADEMem a "Academią". Po pierwsze, wszystkie elementy takie jak krzesła, stoły... wszystkie elementy wyposażenie pochodzą z Fazy Daemonica; są przez Whisper przyniesione lub zdobyte. One są _inne_. 

"Popatrz - wszystko co tu widzisz - WSZYSTKO - pochodzi z Fazy. Każda roślina, każda istota... każda myśl" - Whisper, do zaskoczonej Silurii
"Każdy mag KADEMu może tu wejść - ale nie każdy potrafi. Wiesz jak to zrobiłam, ale nie wiesz, jak wskoczyć w odpowiednią szczelinkę" - Whisper, ze spokojem
"Na oryginalnej AD była sala z samych luster. Byś potrafiła 'się nie widzieć' i poruszać się po Pryzmacie." - Whisper, wyjaśniając wielkie lustro

Whisper wyjaśniła Silurii, czemu sariath nie odtworzyła takiej sali itp. AD miało mało osób, które uczyły się takiej koncentracji. Kosztem tego były pewne wypaczenia - Ash była psychotycznie skierowana na ranienie, Twilight 'nie było', Whisper jest nieskończenie cierpliwa i niewidoczna, Warmaster skupia się na innych, nie sobie...

Siluria odetchnęła z ulgą widząc, że Whisper ma lepszy humor niż ostatnio.

Whisper wyjaśniła jeszcze Silurii, że jeśli Academia Daemonica stanie się częścią KADEMu, to magowie wślizgujący się w tą rzeczywistość będą trafiali do jej Academii. Ale jeśli nie, mogą trafić _gdzieś indziej_. I to byłoby niebezpieczne. 

Siluria przekonała Whisper do zmiany nazwy z Academia Daemonica na Academia Memoriae. Dlaczego? Bo jeśli połączy w Pryzmacie "Academia Daemonica" z "KADEM", to nigdy już w Pryzmacie nie znajdzie oryginalnej AD. Pryzmat AD zniknie, zagubi się. Zaniknie. Coś, czego Whisper nigdy, przenigdy by nie chciała.
Osiągnąwszy to duże zwycięstwo nad Whisper, Siluria jest przekonana, że może sprawić, by ta wizja Whisper (Academia Memoriae) się stała rzeczywistością.

Siluria chciała jeszcze sprawdzić, czy jest w stanie sama wrócić na KADEM. I przeskoczyła... gdzieś. Ciemno, ciasno... i w pomieszczeniu z nią znajduje się Bezimienny. Nic nie robi. Oni są ogólnie niematerialni; zajmują się ogrodnictwem Zamku. Siluria zauważyła, że coś... za nim jest. Zobaczyła za Bezimiennym szkielet. Delikwent wyraźnie zmarł z głodu... lub z pragnienia.

W sumie, Siluria by coś wypiła...

Siluria usłyszała głos Whisperwind "złap mnie za rękę". Złapała. Whisper ją 'przeciągnęła' do Academia Memoriae. Whisper wyjaśniła, że wszystko, co Siluria widziała pochodziło od Silurii. Siluria, zdaniem Whisper, przesunęła się bardzo 'blisko'. Whisper złapała Silurię za rękę i wróciła z nią na KADEM Daemonica... bezpośrednio koło magazynu, gdzie znajduje się EIS.

"Czyli jeśli dobrze rozumiem, wejście i wyjście są ruchome?" - Siluria
"Wejście i wyjście to nie miejsca. To stany umysłu. To koncepty. Dlatego muszę odwrócić Twoją uwagę; wtedy mogę Cię przeprowadzić _tam_ " - Whisper, tłumacząca przeskoki

Siluria pomyślała chwilę. Trzeba by trochę Whisper zintegrować z KADEMem :3. Najlepiej z Norbertem - będą pasowali do siebie charakterami. Obaj pamiętają stare czasy i w ogóle. Więc... Siluria poprosiła Whisper, by ta czasem zjadła z kimś kolację (Siluria zaaranżuje) i pracuje nad swoim PR. Bo to zwiększy szanse na integrację Academii Memoriae z KADEMem. Whisper się zgodziła - ona ogólnie lubi ludzi. Po prostu woli na nich patrzeć ;-).

Wieczór. Siluria postarała się natknąć na Karinę. Udało jej się. Karina bardzo podziękowała jej za radę; dowiedziała się, że arstraccatis wymagają ładowania. Są bardzo czułe. Emocje z nich wyciekają. W skrócie: to nie takie proste. Karina bardzo Silurii podziękowała - wyraźnie arstraccatis nie rozwiązują tego problemu; nie tak. Dopytana, Karina przyznała, że dotychczas ich poszukiwania wiadomości to 'każdy sam'. I w sumie nie do końca działa...

Siluria doradziła Karinie zmianę haseł wyszukiwania i sposobów komunikacji z Wektorem Sigma. Siluria próbowała też delikatnie wpłynąć na Karinę, by ta zrozumiała sam Pryzmat...

Siluria umówiła się jeszcze z Norbertem na kolację i zaprosiła Whisperwind. I faktycznie, z facylitacja Silurii kolacja bardzo dobrze wyszła. Siluria z satysfakcją obserwowała, jak magowie KADEMu znajdowali nici porozumienia we wspólnej historii i formach pomocy innym.

# Progresja


# Streszczenie

Karina przyszła do Inferni i Silurii z nadzieją użycia kwiatów arstraccatis by móc pokonać viciniusa na Fazie; zrobili research. Siluria przekonała ją, by pogłębili research; okazało się, że jednak muszą zacząć od nowa i tak tego nie zrobią. Siluria załatwiła ze Sławkiem, by Artur Kotała miał koszmarny sen - niech myśli że udało się połączyć Warmastera i Alisę i Warmaster odszedł z KADEMu. Whisper pokazała Silurii mikroskok w Zamku As'caen - małą 'przestrzeń'. Swoją 'Academię Daemonica'. Siluria pomoże zrobić PR, by Whisper mogła 'zalegalizować' to miejsce i połączyć je z KADEMem - pod jurysdykcją Whisper. No i Siluria dla ocieplenia wizerunku Whisper (i by ją trochę podintegrować z KADEMem) zaprosiła Norberta i Whisperwind na wspólną kolację.

# Zasługi

* mag: Siluria Diakon, która pracuje nad PR dla Whisperwind, załatwia koszmary dla Artura, śpi z Infernią i pomaga Karinie w zdobywaniu wiedzy i nie wpakowaniu się w coś głupiego.
* mag: Infernia Diakon, 'ogrodniczka' KADEMu zajmująca się Ogrodami Weroniki Seton. Też: ekspert od biomanipulacji nastrojów zapachami. Nadal kochanka Silurii. Lubi deprymować nie-Silurię. W poszukiwaniu hedonizmu.
* mag: Karina Paczulis, lekarz i astralistka o niesamowitej intuicji; słucha rad Silurii i ogólnie sympatyczna. Lekko sfrustrowana, ale jak przystało na KADEMkę chce rozwiązać problem.
* mag: Artur Kotała, który dał radę opracować sposób na viciniusa - kwiaty arstraccatis. Ma pomysły, acz... niebezpieczne wykonanie. Tej nocy będzie mieć straszny koszmar ;-).
* mag: Sławek Błyszczyk, kiedyś "Shadow", katai władający cieniami, astraliką i scavenger; oficer porządkowy KADEMu. Przygotowuje koszmar mający Artura nauczyć konsekwencji. Nosi żelki i częstuje ;-).
* mag: Whisperwind, która robi piękne zdjęcia magów KADEMu (i przestrzeni). Chce odtworzyć 'Academię Daemonica' jako część KADEMu. Siluria przekonała ją, by stworzyła 'Academię Memoriae'. Mistrzyni poruszania się po Fazie.
* mag: Norbert Sonet, zgodził się na kolację z Whisperwind. Nie żałował. 
* mag: Wioletta Lemona-Chang, kontrolująca się defilerka, mistrzyni tatuaży, nożowniczka i lifeshaperka. Dowodzi Instytutem Biomancji. Większość magów się jej strasznie boi.

# Lokalizacje

1. Świat
    1. Faza Daemonica
        1. Mare Vortex
            1. Zamek As'caen
                1. Pokój Zdjęć Whisperwind, ukryty w Przeskoku zamku As'caen, do którego tylko ona ma dostęp
                1. KADEM Daemonica
                    1. Układ nerwowy
                        1. Security, gdzie Siluria się spotkała ze Sławkiem (Shadowem)
                    1. Skrzydło wzmocnione
                        1. Specjalne sypialnie, gdzie sypia Siluria z Infernią
                    1. Academia Memoriae, którą pokazała Silurii Whisperwind
                        1. Sala Centralna, gdzie jest wielkie lustro i miejsce na kilkanaście osób

# Czas

* Dni: 1

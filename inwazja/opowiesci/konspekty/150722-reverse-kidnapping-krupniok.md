---
layout: inwazja-konspekt
title:  "Reverse kidnapping z Krupnioka"
campaign: powrot-karradraela
gm: żółw
players: dust, dzióbek
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [150718 - Splątane tropy: Spustoszenie? (AW)](150718-splatane-tropy-spustoszenie.html)

### Chronologiczna

* [150718 - Splątane tropy: Spustoszenie? (AW)](150718-splatane-tropy-spustoszenie.html)

## Punkt zerowy:

Ż: Czego zupełnie nie przewidział Tymek Maus odnośnie przetrzymywania dużej ilości dzieci w jednym miejscu (3-4 lata)?
D: Trzeba znaleźć im jakieś zajęcie; inaczej są nie do zniesienia.
Ż: Czego tymczasowa baza Tymka i Gali nie ma, a co Gala koniecznie musi załatwić by dzieci przeżyły?
B: Problemy sanitarno-higieniczne.
Ż: Jakie pojawiły się problemy związane z porwaniami dzieci, których zupełnie nie przewidział Tymek?
D: Monitoring osiedlowy. Tymek tego zupełnie nie przewidział a musi rozwiązać by nie było uszkodzenia Maskarady.
Ż: Jakie pojawiły się problemy logistyczne, których zupełnie nie przewidziała Gala?
B: Setki problemów - schowanie dzieci, pieluchy, przepierka...
Ż: Skąd Anna zdobyła informacje odnośnie akcji Maus/Zajcew z dziećmi?
D: Monitoring osiedlowy # "czarny van" ("czarna wołga") # perfekcyjne psucie się monitoringu w odpowiednich momentach # rodzice nie zgłaszają.
Ż: Z grupy: Artur, Anna/Olga/Kleofas, Patrycja, Szczepan - które z nich ma umiejętności szczególne dla tej akcji i czemu?
D: Patrycja Krowiowska; drony, ludzki sprzęt, infiltracja zabezpieczeń. Taki nasz decker. Zamknięta w pancernym vanie z masą elektroniki.
Ż: Z grupy: Artur, Anna/Olga/Kleofas, Szczepan - które z nich ma umiejętności szczególne dla tej akcji i czemu?
B: Olga Miodownik; jej szczere i bezpośrednie podejście do świata pozwala na rozwiązanie problemów które ludzie chcą ogarnąć (małe dzieci, starsze panie...)

## Misja właściwa:

### Faza 1: Decyzja o działaniu.

Anna poprosiła do siebie Dionizego. Tam już czeka Olga. Powiedziała, że jest poważny problem - trzymane są dzieci na "farmie" w warunkach urągających wszelkim warunkom sanitarnym. Dzieci są sprowadzone z zewnątrz, są tam ukraińskie pielęgniarki... Anna pokazała dodatkowo zdjęcia lotnicze z drona (zrobione przez Patrycję). Plan Anny jest prosty. Wbić się niepostrzeżenie, porwać Galę Zajcew i przy pomocy Olgi wytłumaczyć jej (tu się Olga zimno uśmiechnęła) dlaczego to jest zły pomysł. Nie trzeba nic jej odcinać.
Jak tylko Dionizy usłyszał Gala ZAJCEW to natychmiast napisał raport do Edwina. W takim tonie, że jednak warto się tym zająć i potrzebne jest jakieś wsparcie ogniowe na wszelki wypadek. Zeta i Klemens. "Klemens i jego gang Zeta".

Edwin wysłał odpowiedź. DENIED. "Zostaw, nie dotykaj, niech Anna nic z tym nie robi."
Dionizy skontaktował się bezpośrednio z Edwinem. Tam poprosił Edwina o pozwolenie. Edwin wyjaśnił mu w lekko nieparlamentarnym języku, że cała sprawa na linii Edwin (KADEM), Hektor oskarżający Marcelina (Srebrna Świeca) przez Infernię Diakon która jest ZAJCEWEM. Więc atak Hektora lub sił Hektora na Zajcewkę, nawet taką byle Galę to może być czytane jako forma odwetu Hektora przeciwko Inferni.

Edwin powiedział Dionizemu, że to on ma rozwiązać problem, by Anna to zostawiła.
Dionizy wyprowadził: jeśli Anna chce, by to było ciche to nie może być zgłoszenia. Więc trzeba zrobić zgłoszenie. Więc Dionizy zrobił zgłoszenie incognito by sprawa nie mogła być zamieciona pod dywan. I zrobił cynk do mediów.

Patrycja błyskawicznie zablokowała informacje z mediów i dała znać Annie. Anna, w desperacji, poprosiła Dionizego by ten się tym zajął z nią i Olgą TERAZ. Dionizy odmówił. Anna zablefowała, że poprosiła Brysia ale Dionizy przejrzał blef. 
W świetle tego Anna mogła zrobić tylko jedno. Zmienić akcję w oficjalną. Porozmawiała długo z Patrycją i ta zadzwoniła do Hektora - powiedziała, że to wygląda na handel małymi dziećmi, że tam są ludzie z bronią a dzieje się to na terenie kontrolowanym przez niejaką Galę Zajcew. Hektora zmroziło - kazał Patrycji zbierać dowody a sam udał się do Edwina.

"Nie wyjdzie, że wiedzieliśmy. Ja na przykład nie wiedziałem. Nadal nic nie wiem, popatrz na mnie. Ty też możesz nie wiedzieć" - Edwin do Hektora.
"Nie możesz działać. Ród jest ważniejszy niż Twoje widzimisię." - Edwin do Hektora, o akcji, nawet dyskretnej.

Hektor jednak zdecydował się na cichą infiltrację. Są rzeczy ważne a być może jest to akcja nielegalna...
Hektor skontaktował się z Patrycją, odwołał ostatni rozkaz i kazał jej zająć się powstrzymywaniem mediów.

Tymczasem Edwin powiedział Hektorowi, że on wyśle do infiltracji swojego człowieka i bierze to na siebie. Ale INFILTRACJI. Nic więcej.
Hektor się zgodził. A dokładniej: skłamał, że się zgadza. Edwin łyknął.

"Czyli wysłali dwóch zabójców na delikatną sprawę infiltracji magów i mają dowodzić grupą psychopatów. A nikt nie ma magii." - Klemens.

### Faza 2

Klemens i Dionizy się spotkali. Klemens został wprowadzony do grupy specjalnej jako konsultant wrzucony przez Hektora. Dostali od razu informacje odnośnie okolicy - baza Gali Zajcew znajduje się w lesie, trudno dostępne miejsce. Jest chroniona przez bandę niezbyt skutecznych Zajcewowych ludzkich agentów; piąty czy szósty sort; nie są to perfekcyjne siły Zajcewów. Nawet nie będą dość dobrymi siłami Zajcewów.

Klemens na zdjęciach zauważył jeszcze jedną osobę która nie pasuje do wzoru. Młody, elegancko ubrany człowiek nie pasujący do otoczenia. Marcelin zapytany przez Klemensa o obraz powiedział, że jest to Tymoteusz Maus.

Marcelin przyszedł do Hektora. Powiedział mu, że na prośbę Klemensa (wtf is Klemens? A, jeden z tych od Edwina) szukał Tymoteusza Mausa i natrafił na powiązanie między Tymoteuszem Mausem a Galą Zajcew. Rozmawiał z przyjaciółką terminuską (z nią nie spał!) ze Srebrnej Świecy na ten temat i ona powiedziała Marcelinowi, że jest zarejestrowana akcja współpracy między Galą a Tymoteuszem. Chcą porwać grupę dzieci, wywieźć je na Syberię i doprowadzić do tego, że Syberia pożre część dzieci a część może uzyska moc magiczną. Dziećmi (magami) się podzielą między Mausami a Zajcewami.
Czemu ta terminuska nic z tym nie zrobi skoro nienawidzi Mausów? Boi się. Bardzo się boi i nie ma wsparcia, jest sama.

Słysząc to, Hektor wysłał wiadomość Dionizemu, który poinformował Klemensa, który zadzwonił do Edwina.

Edwin się podłamał. Zgodnie z poprzednimi doświadczeniami tego typu to nie działa i nie ma prawa działać. Czyli te dzieci są na śmierć. Leciutko naciskany przez Klemensa, Hektora, Dionizego się złamał. Powiedział, że trzeba Coś Z Tym Zrobić. Nadal nie chce krzywdzić magów, ale chce wyciągnąć te dzieci. Zobowiązał się do tego, że z Margaret zbuduje czterdziestkę dzieci do podstawienia tak, by (jego słowa) "już nigdy nikt nie wysłał dzieci na Syberię".

I Klemens i Dionizy mają wymyśleć jak podmienić armię dzieci.

W końcu udało się opracować plan. Problem polega na tym, że jedyny bezpieczny moment ataku jest wtedy gdy już są dzieci (wszystkie dzieci) w transporcie. Inaczej mogą jakieś dzieci nie zostać uratowane...

### Faza 3

Patrycja siedzi ze Szczepanem w czarnym vanie odpowiednio daleko, ale niedaleko obozu Gali Zajcew i przechwytuje całość komunikacji na linii Gala - ród Zajcew przez telefony komórkowe (ukrycie przed Adelą: 6/k20, fail). Zgodnie z tymi podsłuchanymi rozmowami udało się Patrycji przechwycić cały plan wywozu dzieci. Plan jest strasznie desperacki, zwłaszcza głos Gali gdy się zorientowała, że autokar nie dojedzie do obozu i trzeba dzieci dotransportować do autokaru i kłótnia Gala - Tymek o to, kto ma się tym zająć.

Stanęło na tym, że Patrycja przechwyciła, że oni (ludzie Zajcewów) będą chodzili i przeprowadzali pojedynczo lub po dwie maks. osoby dzieci do autokaru przez co najmniej 10 minut cholernego spaceru przez las. 2 strażników przy autokarze, 4 strażników w obozie i koło 6-7 do przenoszenia dzieci.

Moment. Czyli oni się nie widzą. Czyli jest opcja zupełnie innego planu: wbić się w ich supply line i przechwytywać dzieci z Child Supply Line wprowadzając sztuczne dzieci Edwina i Margaret. Można trochę przedłużyć linię, nikogo nie obezwładniać, będzie jeden gość w kominiarce więcej. A ci Zajcewowie mają dość i naprawdę nie mają głowy na takie sprawy...

Jako, że magia nie będzie użyta, magowie nic nie wykryją. Tymoteusz siedzi w obozie a Gala najpewniej przy autokarze.
Tyle wygrać. Najbezczelniejsza akcja ever.

A Patrycja monitoruje teren.

### Faza 4

Dobrze. Przygotowanie do sprawy:
- Olga i Patrycja są w vanie.
- Anna siedzi w odwodach i jak coś pójdzie nie tak, opóźnia konwój na trasie. Jako, że była policjantką, ma predyspozycje (i mundur).
- Kleofas Bór wbija się w Child Supply Line i wchodzi z kłamstwem (w kominiarce).
- Szczepan odbiera dzieci od Bora i je usypia; jako medyk sobie z tym radzi. I chowa je w rowie.
- Klemens i Dionizy pilnują punktów przekazania dzieci; jakby Bór się nie był w stanie wykpić, wchodzą i zastąpią.

I tak zamiast jednego wężyka, powstały dwa.

### Faza 5

I tak autokar odjechał w kierunku Syberii z 48 sztucznymi dziećmi. Jak już wszystko się oddaliło, siły specjalne zapakowały (ciągle uśpione) dzieci na plandekę. Szczepan, Kleofas, Dionizy zostają z dziećmi a reszta zabezpiecza teren.

I jak zwykle, siły specjalne mają potem wymazaną pamięć...

A dzieci będą przechowane w Rezydencji. I nie wiadomo kto się nimi zajmie i w jaki sposób XD. Opieka nad dziećmi w wieku 2-4 lata w Rezydencji, ilość 48...


## Dark Future:

### Faza 1:

- Tymek Maus kończy harvestowanie dzieci i usuwania pamięci rodzicom i Osiedlowemu Monitoringowi.
- Gala Zajcew przygotowuje poligon i surowce do działania. I problemy sanitarne.

### Faza 2:

- Tymek Maus rozwiązuje problemy z rodzicami i z Osiedlowym Monitoringiem. Nie wygrywa...
- Gala Zajcew załatwia logistykę; za dużo dzieci.
- Gala Zajcew próbuje poszerzyć ochronę tymczasowej bazy...

### Faza 3:

- Adela Maus lekko sabotuje działania Tymoteusza i Gali; chce ich spowolnić.
- Tymek i Gala próbują załatwić Bramę z tymczasowej kryjówki. Nie udaje się.
- Autokar może być wprowadzony... ale nie wjedzie do obozu. NO JAK!

### Faza 4:

- Tymek i Gala transportują dzieci do rendezvous point.

### Faza 5:

- Wywóz na Syberię.

# Streszczenie

Siły Specjalne wysłane by uratować niewinne dzieci przed Tymkiem i Galą (plan Andrei i seirasa Mausa). Adela Maus (inkwizytor) obserwuje z ukrycia. Klemens, Alina i Dionizy mają podmienić armię dzieci tak, by nikt nie wiedział na "płaszczki" z Blakenlab. Udało się. Adela Maus przeskanowała Patrycję Krowiowską, ale poza tym nic nie mają. Nawet naruszenia Maskarady. Dzieci uratowane, plan seirasa zawiódł.

# Lokalizacje:

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Centrum
                        1. Prokuratura
                    1. Dzielnica Trzech Myszy
                        1. Baza Sił Specjalnych Hektora
            1. Powiat Rymaski
                1. Krupniok, wieś 35 km od Kopalina
                    1. Las rusałek

# Zasługi

* vic: Dionizy Kret jako członek sił specjalnych chętny do uratowania dzieci, ale nie kosztem sprzeciwienia się Edwinowi. Odbili dzieci niezauważenie.
* vic: Klemens X jako agent, który z Dionizym doprowadził do reverse kidnappingu dzieci całkowicie niezauważenie. Świetnie planuje akcje bezkrwawe.
* mag: Hektor Blakenbauer, który nieustępliwie doprowadził do zielonego światła akcji ratowania dzieci przed Tymkiem i Galą.
* czł: Anna Kajak, próbowała rekrutować Dionizego do planów "The Director", ale zrezygnowała; inicjatorka akcji uratowania dzieci przed Syberią.
* czł: Patrycja Krowiowska jako osoba, która zebrała wszystkie istotne informacje i używając dronów zapewniła przewagę do wygrania misji bez 1 strzału.
* czł: Olga Miodownik, zawsze gotowa do użycia bólu do przekonania jakiegoś maga by ten nie robił nic złego ludziom. Murem stoi za Anną i jej planami.
* czł: Kleofas Bór, który bez kłopotu wbił się w linię przekazywania dzieci dzięki świetnym umiejętnościom kłamania.
* vic: Szczepan Paczoł, pilnujący, by Patrycji nic się nie stało a potem usypiający dzieci i chowający je do rowu na ostatniej akcji.
* mag: Tymoteusz Maus, mający nadzieję na niezależność i wykazanie się porywając dzieci i wysyłając je na Syberię współpracując z Galą Zajcew.
* mag: Gala Zajcew, która potrafi złożyć całkiem trudną logistycznie operację, choć potyka się na niespodziewanych szczegółach. Pracuje z Tymkiem Mausem.
* mag: Edwin Blakenbauer, pilnował by politycznie nic złego się nie stało z reverse kidnappingu. Złamał się i zezwolił na akcję; nie chce śmierci dzieci.
* mag: Margaret Blakenbauer, która przygotowała armię sztucznych dzieci by na Syberii oduczyć Galę prób eksperymentowania na dzieciach.
* mag: Marcelin Blakenbauer, który dostarczył kluczowych informacji o Tymku, Gali i całej sprawie; ma przyjaciółkę terminuskę SŚ z którą nie spał.
* mag: Adela Maus, "Ostrze Karradraela", inkwizytor monitorująca całą sytuację i zbierająca dowody przeciwko siłom mogącym skrzywdzić Galę lub Tymka.
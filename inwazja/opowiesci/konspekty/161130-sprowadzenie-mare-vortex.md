---
layout: inwazja-konspekt
title:  "Sprowadzenie Mare Vortex"
campaign: powrot-karradraela
gm: żółw
players: kić, dust
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [161124 - Ponura historia ekspedycji Esuriit (HB, SD)](161124-ponura-historia-ekspedycji-esuriit.html)

### Chronologiczna

* [161124 - Ponura historia ekspedycji Esuriit (HB, SD)](161124-ponura-historia-ekspedycji-esuriit.html)

## Kontekst ogólny sytuacji
## Punkt zerowy:

## Misja właściwa:

Dzień 1:

Nadszedł czas opuszczenia pięknej Fazy Esuriit.

Zespół zebrał się w Blockhausie K, swoim bezpiecznym domu. Dzięki Łukiji, Mikado i Silurii morale idzie do góry.
Jakaś ilość energii jest obecna. 
Kazimierz Sowiński był ekspertem od zabezpieczeń, jak Marianna. Biomanta, ekspert od ofensywy, survival. Ogólnie, paskudny przeciwnik.

Hektor zaproponował zaatakowanie Kazimierza by ten nie mógł przeszkadzać podczas samej ewakuacji. Plus... cokolwiek Esuriit może rzucić na jakichkolwiek magów, Kazimierz jest bardzo groźny, bo był magiem i to świetnym. Z uwagi na ilość posiadanego przez nich czasu, mają ten dzień na działania i następny dzień na przyzwanie Zamku As'caen.

Siluria i Hektor przekonali terminusa, Konstantego Myszeczkę, by poszedł "jako Marianna". Andżelika rzuciła na Konstantego zaklęcie kopiując aurę z Marianny. Kazimierz jako istota Esuriit wyczuwa aury, więc wyczuje Mariannę. (w ramach konfliktów: zdąży wyhodować Kolcodrzew).
By wzmocnić sygnał, Anna i Marianna udały, że doszło do tego, że że poziom energii spadł za nisko. Rozproszyły siły zbierające energię (a Andżelika zamaskowała co była w stanie). Wysłały większość magów.
Faktycznie zbieranie energii bardzo wzrosło. Niestety, jeden mag zginął, jeden złamał nogę i dwóch zostało rannych.

Siluria, Hektor i Andżelika zostali w bazie. Na akcję poszli: Anna, Mikado, Ignat i Konstanty (jako Marianna).
Ekipa dostała komunikatory by móc się skomunikować z bazą jak było trzeba.

Ekipa poszła, udając że zbiera energię. Kazimierz ze wsparciem krystaperzy i wijokłąbów zaatakował. Z zaskoczenia, Anna odpaliła granaty EMP (rozwalając połączenia wijokłąbów), ale Kazimierz mimo wszystko zranił solidnie nawet opancerzonego Konstantego. Mikado ugodził podle Kazimierza a Anna doprawiła. Kazimierz walczył dalej, ale został zmuszony do ucieczki. Gdy się schował za jakąś iglicą, Ignat odpalił Kometor Mini i strzelił w niego tą iglicą za którą ów się schował... a raczej jej częścią. Kazimierz zginął.

Przynieśli trupa. Wszystkie trupy jakie udało im się zebrać.
Mają naprawdę sporo energii gotowej do przetworzenia...

Marianna zamknęła się w swoich kwaterach. Mikado dostał ponure zadanie poćwiartować zwłoki Kazimierza, by nie wrócił. Ignat odwiedził Hektora - chce, by ten wygłosił mowę do tienowatych... ale w sumie sam mógł to zrobić. Hektor go powstrzymał. 

Hektor oddał głowę Kazimierza Pawłowi Mausowi. Poprosił, by ten zrobił artefakt; Kazimierz wyraźnie kontrolował istoty z Fazy Esuriit dzięki narośli na żuchwie. Paweł... spojrzał lekko zniesmaczony i powiedział, że się zrobi... (udało mu się).

Siluria dała Mariannie chwilę czasu, po czym poszła z nią porozmawiać. Zapukała do pokoju i weszła.
Marianna ma na sobie odświętny, nieco wyświechtany mundur.

"Mamy trochę do zrobienia..." - Marianna, płaskim głosem
"Dasz sobie radę?" - współczująca Siluria
"Dam. Jestem tien. Świeca nie zostawia swoich." - Marianna, zimno

Siluria wzięła kosmetyczkę i pomogła Mariannie z makijażem. Ma wyglądać jak zdrowa, zdeterminowana i stanowcza kobieta.
Niech Marianna wzbudza szacunek, podziw i niech wygląda jak lider którym być powinna.

"Czas wygłosić mowę do magów. Chodź ze mną." - Marianna do Silurii.

Marianna w krótkich słowach powiedziała im, że następnego dnia wracają do domu. Przyzwą zamek As'caen. Skutecznie podniosła morale.

Dzień 2: 

Dzień ewakuacji. Zamek As'caen zaczyna realizować się w powszechnych umysłach obecnych.
Poziom nadziei rośnie.
Poziom energii rośnie.
Opel Astra podstawiony jest blisko.

Hektor szedł koło transformacji energii i usłyszał zduszony krzyk. Podbiegł - Pestka pyta Bazylego co się dzieje. Bazyli podniósł się i złapał Pestkę. Ten wrzasnął; Weiner zaczął wysysać jego energię. Obaj zmieniają się w energiaki Esuriit.
Hektor dał w długą. Na szczęście zdążył zwiać; dobiegł do Anny. Anna powiedziała, że rozwiąże problem. Z zimną krwią zastrzeliła obu magów nie dając im szans.

Andżelika, Mikado i Anna zobaczyli Esuriit. Coś się dzieje z bańką koszmarów...

Ekipa poszła szybko do Zofii. Andżelika i Zofia zaczynają szybko dyskutować o bańkach; wyszło im, że gdzieś wyhodowany jest Kolcodrzew Esuriit.
Andżelika potrafi zlokalizować gdzie mniej więcej jest. Jest gdzieś na Wyspie Kolców...
Zofia powiedziała, że to oznacza, że KAŻDY mag który jest Esuriit może w dowolny sposób się przekształcić.

Marianna chce z nimi porozmawiać. 
Gdzie jest Kolcodrzew? Zofia odpowiedziała, że gdzieś na Iglicy Sowińskich.
Marianna kazała Konstantemu wziąć oddział i iść. Spojrzał na nią wiedząc że to samobójcza misja, lecz potwierdził.
Siluria zaproponowała, że strzelą Kometorem. Konstanty powiedział, że pójdzie sam - tylko na zwiad. Da namiar.

Andżelika prawie straciła przytomność.
Powiedziała, że jest problem. "Tatotatotatonieee!". Silny sygnał...
Oki. Jest problem. Mikado i Anna wysłani do Dosifieja. Ale nie mają mu robić krzywdy jeśli to mag Esuriit.

"Nie żyje! Kto umarł, nie żyje!!!" - krzycząca Anna do Hektora, przypominająca sobie o swoich dzieciach

Mikado i Anna poszli. Ignat zajął stanowisko na Kometorze. 
Zofia odciągnęła Hektora i Silurię na stronę i wyjaśniła, że jeśli Koszmar się rozpadnie, są martwi. Obowiązkiem Hektora jest torturować dziecko aż odciągnie myśli od ojca.
Hektor poprosił Silurię, by ta zajęła się matką.

Weszli do skrzydła medycznego. Paweł Maus, Łukija Zajcew zajmują się krzyczącym Fiodorem. Łukija boi się o Dosifieja, ale nie chce by Fiodor cierpiał.

"To nie jest dziecko z rodziny Świecy. To prawdziwe dziecko!" - Łukija

Łukija zaczyna zmieniać się w istotę Esuriit. Paweł ją szybko złapał i próbował obezwładnić. Łukija próbuje go drenować. Paweł się uśmiechnął. Serio. Ale niech Siluria ją zabije!
Bańka zaczyna się rozpadać... 
Siluria korzystając z umiejętności samoobrony wyszkolonych przez Wiolettę... złapała skalpel i cięła prosto w szyję.
Łukija padła martwa na ziemię. Jest martwiejsza niż martwa.

"Widząc co tu się dzieje... chyba czas, bym was zostawił. Jestem dla was niebezpieczny. Opiekujcie się wszystkimi." - odchodzący szybkim krokiem Paweł Maus.

Hektor wniknął w umysł dziecka i zaczął rzucać koszmary z sali sądowej i swego dzieciństwa w Rezydencji. Fiodor Maius stał się elementem tamtych koszmarów... samotnie.
Hektor po kilku minutach skończył zabawę. Bańka się utrzymała.

Konstanty dał radę wrócić, ponownie ranny. Niestety, jeszcze jeden strzał Kometora był potrzebny - Kolcodrzew wymagał DWÓCH strzałów. Poziom energii spadł poniżej wartości minimalnej... nie mają energii by wezwać Zamek As'caen.
Magowie nie będą się przemieniać dalej - Kolcodrzew i jego wpływ przestał działać.

Paweł Maus, wiedząc, że tylko on może to zrobić zaczął skutecznie eliminować ludzi poświęcając ich w Rytuale Krwi. Jednego po drugim. W najokrutniejszy sposób jaki potrafił sobie zaplanować by zmaksymalizować generację energii.
Niech oni wszyscy wrócą do domu. On nie jest już w stanie.
Hektor i Siluria wiedzą co się dzieje - ale jeśli zatrzymają Pawła, to wszyscy zginą.

Poświęcenia się opłaciły. Zaczyna się powoli materializować Zamek As'caen.

Siluria bierze Andżelikę i Ignata. Zaczynają nadawać sygnał SOS.
Zamek niesie za sobą cholerne Mare Vortex z Fazy Daemonica...
I Zamek otwiera ogień z setek baterii dział. Zamek, element Fazy Daemonica zaczyna oblegać Fazę Esuriit.

"Zamek oblężniczy. Tego jeszcze nie widziałem." - Dust, 2016.

Sęk w tym, że Zamek nie wie o obecności kilku żywych magów których może przypadkiem rozgnieść jak pluskwy.

"Silurio! NIE SŁYSZĄ NAS!" - Ignat, zaskoczony pięknem artylerii Zamku As'caen
"MUSZĘ ZAPUKAĆ!" - Ignat wchodzący na Kometor i celujący w As'caen

Siluria skopała Ignata z Kometora. Co on, chce ich zabić?!
Zleciał. 
Szybko skoczyła do Opla i odpaliła sygnały awaryjne, klakson i wszystkie możliwe elementy.
Zamek As'caen odebrał sygnał maszyny Setona... Quasar zmieniła cel działania Zamku - ewakuacja, nie oblężenie.

Niestety, jedno z dział strumieniowych Zamku trafiło Zofię. Nic nie zostało.

Zniszczenia wywołane przez zamek As'caen są straszliwe, ale Archiwum, Opel Astra, niektórzy magowie i niektórzy ludzie zostali bezpiecznie ewakuowani na Mare Vortex...

# Progresja

## Frakcji

* KADEM: przechwycił dziwny artefakt z Esuriit składający się ze szczęki Coś Robiącej Na Esuriit.
* KADEM: przechwycił Archiwum Ekspedycji Esuriit.

# Streszczenie

Zespół wykonał atak wyprzedzający przeciwko Kazimierzowi. Mag Esuriit zginął. Jednak zdążył wcześniej przygotować narzędzie osłabiające bańkę koszmaru; magowie zaczęli orientować się że są Esuriit i przekształcać się - zawsze jednak Anna i Mikado byli na miejscu. Wszyscy byli zmuszeni do strasznych kompromisów - ale udało się wycofać. Pojawił się zamek As'caen i udało się uciec z Esuriit. Nawet uratowali archiwa i część Ekspedycji. Speculoid podążył za nimi, na Fazę Daemonica.

# Zasługi

* mag: Hektor Blakenbauer, uratował całą Ekspedycję tortujując dziecko. Kierował Annę i Mikada, by zabijali * magów Esuriit. De facto współdowodził z Marianną...
* mag: Siluria Diakon, Diakonka, która zabiła skalpelem. Diakonka, która wezwała Mare Vortex. Diakonka, która skopała Ignata z Kometora.
* mag: Andżelika Leszczyńska, buduje fałszywe sygnatury, odkrywa i ukrywa aury, skanuje co dzieje się z bańką... a gdy dochodzi do walki, siedzi w Oplu skulona.
* mag: Mikado Diakon, uduchowiony mistrz miecza zwany "rzeźnikiem z Esuriit". Zabił Dosifieja Esuriit. Współuczestniczył w egzekucji Kazimierza.
* mag: Anna Myszeczka, egzekutorka * magów Esuriit by chronić Hektora i Ekspedycję. Świetna na zasięg. Współuczestniczyła w egzekucji Kazimierza.
* mag: Ignat Zajcew, zbyt lubi strzelać z Kometora by ktokolwiek czuł się bezpiecznie. Zestrzelił Kazimierza Esuriit "małym Kometorem". Niestety.
* mag: Konstanty Myszeczka, terminus, bardzo kompetentny, lekko zrezygnowany (że tym razem Marianna go zabije). Ku ogromnemu zdziwieniu wszystkich - przeżył.
* mag: Zofia Weiner, nie wierzyła, że wróci do domu. Miała rację - działo strumieniowe KADEMu zabiło tą badaczkę Esuriit. KIA.
* mag: Marianna Sowińska, do samego końca dowodziła Świecą podejmując straszne decyzje. Do samego końca została na posterunku. Przeżyła - dla innych.
* vic: Kazimierz Sowiński, zaplanował zabicie swojej siostry i Zofii... a skończył w pułapce zmasakrowany przez mini-Kometor. Strasznie groźny w walce. KIA.
* vic: Dosifiej Zajcew, terminus Esuriit, którego transformacja prawie zniszczyła bańkę chroniącą Ekspedycję. Zabity przez Annę i Mikado. KIA.
* vic: Łukija Zajcew, bardka Esuriit, która chciała chronić Fiodora ponad wszystko. Gdy transformowała, Paweł Maus się z nią siłował. Zabita przez Silurię skalpelem. KIA.
* vic: Bazyli Weiner, który pierwszy transformował w Esuriit i dotykiem przekształcił Jakuba Pestkę. Zastrzelony przez Annę. KIA.
* mag: Jakub Pestka, chciał pomóc Bazylemu. Skończyło się na tym, że też przetransformowało go w Esuriit. Zastrzelony przez Annę. KIA.
* vic: Paweł Maus, lekarz Esuriit do końca. Nawet jako energiak, chronił Ekspedycję, poświęcał ludzi w rytuale by zwiększyć poziom energii. Wszedł w ogień dział As'caen. KIA.
* mag: Fiodor Maius Zajcew, torturowany przez Hektora by utrzymać Bańkę. Przeżył. Stracił rodziców i najpewniej poczytalność.

# Lokalizacje

1. Świat
    1. Faza Esuriit
        1. Insula Luna
            1. Srebrna Septa
                1. Centrum
                    1. Centrala Świecy
                    1. Blockhaus K 
                    1. Archiwum, które docelowo trafiło na KADEM
                    1. Kometor, który znowu wystrzelił - i to nie jeden raz
                1. Skrzydło szpitalne
                    1. Sale medyczne, gdzie Łukija i Paweł się siłowali, Łukiję zabiła Siluria a Fiodora torturował Hektor. Tak bardzo sale medyczne.
            1. Wyspa Kolców
                1. Iglica Sowińskich, gdzie znajduje się Kolcodrzew Esuriit skierowany przez Kazimierza Sowińskiego na Srebrną Septę. Trafiona z Kometora.
    1. Faza Daemonica
        1. Mare Vortex
            1. Zamek As'caen, który ma ogromną ilość dział różnego rodzaju i który może oblegać Fazę Esuriit. Może niedługo... ale dość długo.

# Skrypt

- Speculoid opuszcza z nimi Fazę Esuriit            : done
- Kazimierz zabija Zofię Weiner i Mariannę          : Kazimierz nie żyje
- Paweł Maus masakruje ludzi dla źródeł energii     : done
- Opel Astra zostaje pozostawiony na Esuriit        : nie wpłynęło
- Konstanty Myszeczka prowadzi ekspedycję by zniszczyć Kazimierza i zachować Koszmar. : done, o dziwo - przeżył?
- Jakub Pestka ginie od Energiaka Dosifieja         : done, ale Bazylego
- Postacie uciekają do Mare Vortex.                 : done
- Zamek As'caen bombarduje i ciężko rani większość magów.   : done

# Czas

* Dni: 2
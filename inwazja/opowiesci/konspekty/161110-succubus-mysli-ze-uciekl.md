---
layout: inwazja-konspekt
title:  "Succubus myśli, że uciekł"
campaign: nicaretta
gm: żółw
players: kić, raynor
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [161103 - Egzorcyzmy w Żonkiborze (HS, temp)](161103-egzorcyzmy-w-zonkiborze.html)

### Chronologiczna

* [161103 - Egzorcyzmy w Żonkiborze (HS)](161103-egzorcyzmy-w-zonkiborze.html)

## Kontekst ogólny sytuacji
## Punkt zerowy:

## Misja właściwa:

Dzień 1:

Już dwa dni minęły odkąd Nicaretta się zmaterializowała na tym świecie. Henryk dał radę zakończyć ciężką pracę w Żonkiborze. Wzywa pomoc - nie jest w stanie znaleźć Nicaretty. Henryk połączył się z poczciwym kotolubnym introligatorem - Albertem. Ten powiedział, że może wysłać mu wsparcie. Ale niech Henryk traktuje wsparcie z szacunkiem... bo straci serce. Po czym Albert powiedział Lunie o sytuacji i odprowadził ją na pociąg.

A Henryk, tymczasem, szuka Nicaretty. Zgromadził odpowiednich ludzi w kościele (tych co byli podczas rytuału) i przygotował czar astralny. Chce dowiedzieć się wszystkiego odnośnie Nicaretty i samego rytuału formą astralną. Zrobił to wieczorem.
Henryk poznał imię sukkuba: Nicaretta.
Henryk dowiedział się, że Nicaretta chce zachować low profile. I leci do najbliższego dużego miasta.
I Henryk wie, jaki smak ma aura Nicaretty.

Luna weszła do kościoła. Henryk skończył z konfesjonałem i wychodzi. Do pokoju na plebanii.
I Luna się pojawiła, z krzaków wylazł do Henryka kot. I się patrzy. Dostała polędwicę i zjadła.

Henryk spakował się i czeka. Luna też czeka.

"Ta polędwica była dobra, ale trochę mało" - Luna
"...jeszcze chcesz?" - Lekko zdziwiony Henryk
"Miau" - Luna, złośliwie

"O co chodzi? Jestem dosyć zaskoczony" - Henryk do Luny
"Podobno chciałeś polować." - Luna
"Aaa, to już wszystko jasne" - Henryk

Henryk powiedział Lunie, że wie gdzie jest Nicaretta. Zaproponował jej podwiezienie.
Jadą do Wyjorza.

Hotel Luksus w Wyjorzu, pet friendly. Dostał pokój acz nie tanio. Z Luną.
Henryk zdecydował się poszukać Nicaretty niemagicznie. Nie znalazł - ale wyczuł coś obcego, starego, martwego i złowrogiego (Iglicę Trójzębu). Czyli Nicaretta się zabunkrowała lub nie czaruje...

Luna poszła na miasto zaczepiać koty i szukać czy któryś z nich wie coś o nie-dwunogu cuchnącym seksem.
Zajęło jej to całą noc. Dowiedziała się o:
- kilku agencjach towarzyskich. Za dużo...
- "Martwe miejsce". Nie jest już groźne, ale jest złe. Niedwunóg tam jest i był.
- Legowisko niedwunogów (hotel). I tam był. I coś się działo.
- A teraz niedwunoga nie ma.

Dzień 2:

Henryk się obudził. Kot - ciężki - śpi na nim. Henryk śpi dalej.
9:00. Henryk powiedział Lunie, że nie ma pazurów i nie wie jak polować na Nicarettę.

Henryk idzie z Luną. Wpierw - hotel. Hotel Jantar. I Henryk wyczuwa ze środka stare echo zaklęcia Nicaretty.
Henryk spytał recepcjonistę, czy jest tam Marzena Gilek. Recepcjonista powiedział, że nie teraz, ale ogólnie tak. Henryk powiedział, że zostawi jej kartkę - recepcjonista zostawił kartkę w skrytce do pokoju 113.

Luna chciała pójść do tego pokoju. Tymczasem zaatakował go boy hotelowy... nożem. Kilka płytkich ran zadanych Henrykowi. Ten krzyknął, Luna się wycofała. Recepcjonista rzucił się by trzymać Henryka, Luna użyła mocy Terroru i po chwili boy wpadł w berserk (raniąc kilka innych osób), Henryk pochlipał i się otrząsnął. Wpadła policja i spałowała boya, Henryka zwinęli lekarze, opatrzyli i wypuścili po godzinie...
To miejsce było pułapką. Zaprojektowane przeciwko osobie triggerującej.
Luna wie, że w hotelu co najmniej dzień nie było.

Luna zaprowadziła Henryka do "Martwego miejsca" - Iglicy Trójzębu. Ciała martwego asari. Byłej fortecy Tymotheusa Blakenbauera.
Dla ludzi, jest to biurowiec. Dla Henryka jest to klatka Faradaya dla magii i echo wojny Bogów w ciele martwego asari odiatis...
Luna nie idzie z Henrykiem.

Pani sekretarka, Halina Pawełek, uśmiechnęła się do księdza Henryka. Iglica Trójzębu jest... specyficznym miejscem. Ma za dużo pięter. Ludzie tego nie zauważają...
Ksiądz użył zaklęcia astralnego - wyczuł harce sukkuba na jednym z "niewidzialnych pięter", wczoraj. To był dziwny rytuał, bardzo... odmienny. Ale stąd nie możesz go zbadać. Coś zrobiła.
Ksiądz powinien się tam dostać by to zbadać...

Henryk powiedział Halinie, że szuka swojej zakonnicy. Plantuje w niej myśl, by ta pozwoliła mu tam wejść. Pod wpływem zaklęcia Halina zmiękkła i dała Henrykowi czego chciał - dostęp do windy całkowicie dowolnie. Henryk poczuł:
- orgia, rytuał miały miejsce na "+1 poziomie". Na +1 poziomie jest jakieś biuro.
- coś się wydarzyło na "+4 poziomie" którego ludzie sobie nie wyobrażają

Henryk wjechał na poziom +1. Tam recepcjonista chciał go zatrzymać. Henryk nalegał, bo zakonnica... i polityk siedzący w środku kazał recepcjoniście wpuścić Henryka.
Polityk - Hubert Kaldwor, z partii "Kochamy Państwowe Pieniądze", spytał o co Henrykowi chodzi. Henryk powiedział o zakonnicy i Hubert się roześmiał. Pokazał Henrykowi film z orgią, licząc, że ksiądz się zgorszy. Henryk się ucieszył. Zaczął zadawać pytania, polityk na to zniesmaczony, że on nie jest prawdziwy ksiądz. 

Poprosił swoich thugów, by wyciągnęli zeznania z księdza. Henryk rzucił na Huberta czar mentalny - są przyjaciółmi i spotkali się na jednej z takich imprez. Hubert odwołał thugów; zgrał Henrykowi filmik (ku przerażeniu dziewczyn) i powiedział, gdzie jest w tej chwili "zakonnica" - umieścił ją w hotelu Dagmara. 

Henryk wjechał jeszcze na +4 piętro. Tam użył astraliki i miał wizję, jak Nicaretta zmienia ciało na Grażynę Diadem. Ma jej nową aurę i nowy wygląd. Zorientował się, że są teraz 2 succubusy... a Nicaretta skutecznie uciekła. Chwilowo.
Potem dostał DEATH DRAGON LSD ACID TRIP.

Luna przechodząc się po mieście dostała memory na ciało Nicaretty/zakonnicy.

Dzień 3:

11 rano. Henryk obudził się de facto w rowie. Zmarznięty i niewyspany i pocięty nożem. Malus na malusie. Ale Luna na nim siedzi, więc jest ok... lol.
Henryk jest chory...
Nie podoba mu się walka z demonami...

Wrócili do hotelu. Henryk się ogarnął. I wziął aspirynę.
Pojechali do hotelu Dagmara.
Hotel JEST opanowany przez jedyną słuszną partię KPP. Wygląda jak miejsce gdzie mogą mieć spotkania mafiozi.

Henryk z "kotkiem" podeszli do hotelu. Tam recepcjonista. Odmawia wejścia Henrykowi, ale ten powołuje się na Huberta i może wejść.
Luna mówi księdzu, że "Nicaretta" (to drugi sukkub) kopuluje w sali spotkaniowej.

Henryk i Luna zdecydowali się poczekać, aż nie będzie Pryzmat tak silny.
Henryk wyskoczył do apteki, kupił bardzo silne środki przeczyszczające. Potem wrócił.

Kot (Luna) zaprzyjaźnił się z boyem hotelowym i jego nogami i go przewrócił. Henryk gorąco przeprasza i pomaga mu z napojami, wrzucając środki przeczyszczające ze skutecznością. Boy zaniósł je na salę i zaczęło się... scat horror fetish party. Niefortunne. Ściany zmieniają kolor. Sukkub taki smutny. Nie ma radości. Nie ma przyjemności...

Impreza się zakończyła i sukkub wrócił do pokoju 112. 
Henryk przygotował wielki, żelazny krzyż i pismo święte. Wie też jakie są słabe punkty demonów.

Henryk otworzył drzwi (ofc nie są zamknięte) i wszedł. Sukkub zaczął od "come to me"; Henryk udawał, że idzie a potem polał sukkuba wodą święconą.
Jak demonica wrzasnęła, walnął ją krzyżem i wyegzorcyzmował. Kot rozszarpał jej tętnicę.

Henryk pojechał z ciężko ranną zakonnicą do szpitala (pod iluzją) i zostawił ją nagą pod drzwiami. I zwiał.
Trzy kolejne dni Henryk siedzi w łóżku i odchorowuje.

# Progresja

# Streszczenie

Nicaretta (sukkub) ucieka. Zmieniła ciało na inne i próbowała zmylić ścigającego ją Henryka. Ten jednak za nią podążył, wpadł w pułapkę (został pocięty nożem, uratowany przez Lunę), po czym wszedł do Iglicy Trójzębu w Wyjorzu. Tam dostał namiar na drugiego, słabszego sukkuba (przyzwanego przez Nicarettę jako dywersja) i złapał "namiar" na aktualną formę Nicaretty. Współpracując z quasimafijnym politykiem w Wyjorzu z partii Kochamy Publiczne Pieniądze dorwał sukkuba w kontrolowanym przez partię hotelu odległego o ~10 km. Zostawił hosta pod drzwiami szpitala i uciekł pod iluzją.

# Zasługi

* mag: Henryk Siwiecki, ksiądz dzielnie tropiący i egzorcyzmujący sukkuba, który wykrył że są dwa i w sumie skończył chory. Mistrz środków przeczyszczających.
* vic: Luna, * magiczny kot. Słodka kicia spod sutanny zagryzająca sukkuba i zbierająca informacje od kotów. Opiekuje się chorym księdzem bo poprosił ją przyjaciel.
* mag: Albert Czapkuś, tien, docenia pomoc i znajomość Luny. Tymczasowy handler Henryka Siwieckiego (bo hipernet niezbyt działa). Introligator. Kotolubny.
* vic: Nicaretta, zastawiająca pułapki (gra by zabić) i przeskakująca po hostach demonica, która myśli, że skutecznie zmyliła ewentualny pościg. Bardzo inteligentna.
* czł: Marzena Gilek, host INNEGO sukkuba niż Nicaretta (która przeskoczyła). Skończyła ranna (żelazny krzyż w głowę) w szpitalu, ale wolna.
* czł: Grażyna Diadem, prostytutka i nowy host Nicaretty. Henryk wie, jak wygląda.
* czł: Hubert Kaldwor, polityk partii Kochamy Państwowe Pieniądze. Powiązany mafijnie, lubi dobrą zabawę i dziewczyny.

# Lokalizacje

1. Świat
    1. Primus
        1. Lubelskie 
            1. Powiat Tonkij
                1. Żonkibór
                    1. Parafia
                        1. Kościół
                        1. Plebania
                1. Wyjorze
                    1. Centrum
                        1. Iglica Trójzębu, która nadal promieniuje energią martwego asari. Niebezpieczna dla użytkowników astraliki.
                        1. Hotel Jantar, gdzie Nicaretta założyła pułapkę na Henryka - a dokładniej, na każdego co próbuje się o niej dowiedzieć.
                        1. Hotel Luksus, drogi i bogaty - mają pokoje pet friendly i jest bogaty; tam przenocował Henryk z Luną.
                1. Puszcza Nocy
                    1. Wschodnia Dolina
                        1. Hotel Dagmara, kontrolowany przez quasimafijne struktury partii Kochamy Publiczne Pieniądze

# Skrypt

-

# Czas

* Opóźnienie: 1 dzień
* Dni: 6
---
layout: inwazja-konspekt
title:  "Dlaczego magitrownia działa?"
campaign: wizja-dukata
players: kić, żółw
gm: raynor
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [171205 - Dlaczego magitrownia działa? (temp)](171205-dlaczego-magitrownia-dziala.html)

### Chronologiczna

* [170920 - Początki prokuratury (HB, HS, SD)](170920-poczatki-prokuratury.html)

## Kontekst ogólny sytuacji

### Wątki konsumowane:

?

## Punkt zerowy:

* R: Czemu XYZ jest zainteresowany tym, by magitrownię przejęli ludzie Sądecznego?
* Ż: Bo tak jak jest magitrownia jest śmiertelnie i potencjalnie niebezpieczna. Dupnie. Kiedyś.
* R: Dlaczego profity Natalii są silnie powiązane z wydajnością magitrowni?
* K: Dzięki temu Natalia jako inwestor może więcej uzyskać z nowych elementali (z wieży wichrów).
* R: W jakim dokładnie celu Jakub został wysłany do magitrowni przez Sądecznego?
* Ż: Rekonesans, ochrona Natalii i drobne naprawy.
* R: Dlaczego Sądeczny zwrócił się do Natalii w sprawie magitrowni?
* K: Wykorzystać rzut zwierząt by się tymczasowo pozbyć Marzyciela.
* R: Skąd Natalia zna Sądecznego?
* K: To, że zdecydowała się zostać weterynarzem nie zmienia tego, że jest bogata. Jej fortuna ze sprzedaży artefaktów i spotkali się przy jednej z eskapad.

## Potencjalne pytania historyczne:

* Czy Marzyciel jest człowiekiem?
    * Domyślnie: NIE
* Czy Marzyciel jest powiązany magicznie z magitrownią?
    * Domyślnie: TAK
* Czy magitrownia odepchnie od siebie ludzi Sądecznego?
    * Domyślnie: TAK

## Misja właściwa:

**Dzień 1**:

Sądeczny powiedział Natalii, że trzeba pozbyć się Marzyciela z magitrowni na pewien czas. Dobrocień pomyślał - on zna okolicę, zna magów i ludzi, z niejednym pił. SS: Marzyciel opuści to miejsce jeśli pozyska się odpowiednio sporo małych słodkich kotków. Bo w Magitrowni tego typu stworzenia raczej nie występują a to jest poza comfort zone magitrowni. Natalia się nie zgodziła - więc Dobrocień zaproponował, że pojedzie do magitrowni. Po prostu spróbuje Marzyciela spić.

Natalia więc pojechała do Apoloniusza Bankierza - szefa Eliksiru Aerinus. Jest inwestorką. Apoloniusz powiedział, że niedługo będzie nowy pakiet akcji i dokapitalizowania firmy. Natalia powiedziała, że (kwestia zakładu) - jej nie upije, kogoś tak. Czyli: serum + konkretny eliksir alkoholowy. I Natalia dostała swój "wymarzony" eliksir.

Dobrocień pojechał z Natalią do magitrowni. Zapukał do magitrowni i powiedział, że chciałby naprawić magitrownię - jako, że Marzyciel zna się na tym najlepiej, Dobrocień jest skłonny oddać się pod kontrolę Marzyciela by ten mówił co ma być zrobione. Marzyciel niechętnie się zgodził i wpuścił Dobrocienia. Jednak Porażka i rerollowany SS sprawiły, że Magitrownia się obudziła jak tylko Dobrocień wszedł. Dookoła magitrowni zaczęła emanować dziwna energia. Magitrownia odpowiedziała na Dobrocienia...

...Natalia to wyczuła. Spróbowała pasywnie zrozumieć co oznacza ta emanacja. Jej wiedza  i umiejętności Lary Croft. Reakcja magitrowni jest porównywalna z reakcją grobowca faraona gdy do grobowca wchodzą złodzieje... czyli reakcja obronna. Dobrocień w tarapatach.

Dobra. Natalia zdecydowała się postawić małe stado małych słodkich DECOY kotków. Nieprawdziwe, wirtualne bezradne puszyste kiciusie. Słodkie. Przytulaśne. Główny cel kotów? Ściągnięcie na siebie tego co się skupia na Dobrocieniu. Natalia ma Soczewkę: porządne... 100 kotów lata po magitrowni. Dążą do kontaktu z człowiekiem i są głośne i głaskalne. Poziom alertu magitrowni bardzo wysoki.

Marzyciel wyleciał z magitrowni do narzędziowni. Poleciał na koty - z siekierą. Marzyciel jest chodzącym alertem dla magitrowni..?

Dobrocień zastąpił drogę Marzycielowi, rozbroił go i wytrącił mu broń. Utrzymał go i uspokoił - ale po chwili słychać pisk wszystkich stworzonych kotów. Coś się stało kiciusiom.

Weszła Natalia do magitrowni. Okazuje się, że Marzyciel nic nie pamięta o kotach ani o poprzedniej akcji. Dobrocień tam poudawał, że to on przyniósł siekierę. Natalia i Dobrocień się po hipernecie zorientowali, że on jest sprzężony z magitrownią.

Dobrocień wpadł na pomysł - dogada się z magitrownią. Jako mechanik i technomanta skupił się na naprawianiu magitrowni a Natalia na jej sprzątaniu. Próbowali udowodnić magitrowni m.in. przez przekonanie czynami Marzyciela że są w stanie jej pomóc. I faktycznie ZACZĘLI to robić. W pewnym momencie Dobrocień zaczął nawet dość ostro czarować - usuwać rdzę, czyścić kanały fizyczne... a Natalia dokładnie to samo odnośnie katalitycznych kanałów magitrowni. Może nie są ekspertami, ale chcą pomóc.

I Magitrownia to zaakceptowała. Marzyciel też "polubił" Dobrocienia tak po swojemu.

Wieczorem wrócili do domów by rano pogadać z Sądecznym.

**Dzień 2**:

Sądeczny był nie tak lekko oburzony. Dobrocień i Kazuń mieli pozbyć się tego cholernego Marzyciela. Jednak magowie zauważyli, że magitrownia jest miejscem wielu potężnych energii i Marzyciel nie jest już w pełni człowiekiem. Nie ma takiej możliwości by się go po prostu pozbyć. Już nie. Gdy Sądeczny zażądał by wpakowali tam jakiegoś kontrolowanego przez niego maga, Dobrocień zaproponował że zna jednego delikwenta który się nadaje. Nazywa się Daniel Akwitański.

## Wpływ na świat:

JAK:

1. Co na tej sesji dookoła jakiegoś konfliktu czy zdarzenia było fajne? Co chcesz by w ramach tego się poszerzyło?
1. Każdy gracz wydaje swoje punkty na generowanie rzeczywistości i wątków.

CZYLI:

* Wątek nadwrażliwości magitrowni na słodkie kociaki. WHAT THE HELL. (Kić)
* Jak Sądeczny, taki zwykły "pro thug" stał się tak potężny. (Żółw)
* Czy Marzyciel jest człowiekiem? : NIE WIEM czym jest.
* Czy Marzyciel jest powiązany magicznie z magitrownią? : TAK, ale nie wiemy jak.
* Czy magitrownia odepchnie od siebie ludzi Sądecznego? : TAK.

JAK:

* 2: MUSIK: gracz mówi w jaki sposób jedna z obcych postaci wspiera coś związanego z motywacją JEGO postaci
* 2: CLAIM na wątku / postaci / okoliczności
* 2: do elementu Historii lub przeszłego konfliktu dodajemy "ale" lub "oraz"
* 2: dodajemy pytanie, które musi zostać odpowiedziane na jakiejś z przyszłych misji
* 2: odpowiadamy na pytanie, które pojawiło się na jakiejś z misji
* 3: gracz mówi w jaki sposób jedna z obcych postaci wspiera coś związanego z motywacją JEGO postaci
* 3: zmieniamy kontekst okoliczności czegoś z Historii - scena z przeszłości / fakt?
* 3: do elementu spoza Historii dodajemy "ale" lub "oraz"
* 3: dodajemy znaczącego NPC powiązanego z elementem Historii
* 3: pozyskanie przez dowolną postać znaczącego surowca mającego sens z perspektywy misji
* 3: dodanie nowego elementu Historii
* 3: dodanie przyszłego lub przeszłego faktu; czegoś, co się wydarzyło lub wydarzy

Z sesji:

1. Raynor (8)
    1. Całą akcję z magitrownią, aż do przejęcia, Sądeczny przypisuje skutecznie sobie i tylko sobie (Raynor).
    1. Dla całej ekipy Dukatopodobnej Marzyciel stał się integralnym wyposażeniem magitrowni. Niewyciągalny, ma tam być.
    1. -
1. Kić (5)
    1. Apoloniusz wypuścił dodatkowe akcje Eliksiru i one się skutecznie zwróciły (Kić).
    1. Natalia dała radę uniezależnić się od Sądecznego i sił Dukata; wykupiła się i kupiła prawdziwy immunitet z czasem (Kić).
1. Żółw (8)
    1. Karol Marzyciel akceptuje (niekoniecznie ufa) Jakuba Dobrocienia (Żółw)
    1. Robert Sądeczny w tym czasie silnie polegał na Dobrocieniu jako na nieco podległym, ale partnerze (Żółw)
    1. To Dobrocień zainstalował Daniela Akwitańskiego w strukturach magitrowni, acz nie powiedział Danielowi o Marzycielu (Żółw).

# Progresja

* Natalia Kazuń: kupiła dodatkowe akcje Eliksiru Aerinus od Apoloniusza; zwróciły się po czasie i były dobrą inwestycją
* Natalia Kazuń: całkowicie uniezależniła się od sił Sądecznego i sił Dukata. Wykupiła się.
* Jakub Dobrocień: Karol Marzyciel akceptuje go jako zacnego maga. Nie "ufa" mu, ale "akceptuje" i czasem pomoże.
* Jakub Dobrocień: to on zainstalował Daniela Akwitańskiego w magitrowni po tych dniach - ale nie powiedział mu o Karolu Marzycielu.
* Jakub Dobrocień: jest "podległym partnerem" dla Roberta Sądecznego. Nie jest tylko jakimś podległym randomem.
* Karol Marzyciel: przez siły Gabriela Dukata i Roberta Sądecznego jest traktowany jako nieusuwalny "mebel" w magitrowni.
* Robert Sądeczny: całą akcję z magitrownią aż do przejęcia przypisał skutecznie sobie i tylko sobie.

# Plany

* Jakub Dobrocień: ściągnąć Daniela Akwitańskiego do magitrowni Histogram, bo jest tam miejsce dla kogoś takiego jak on
* Robert Sądeczny: przejąć kontrolę nad głównymi kluczowymi elementami otoczenia; potrzebuje do tego wsparcia
* Apoloniusz Bankierz: szokowo rozszerzyć biznes / wejść na nowy typ produktu; potrzebuje do tego kapitału

# Streszczenie

Robert Sądeczny niedawno pojawił się na tym terenie z ramienia Dukata. Magitrownia Histogram jest nieco niestabilna i by wzmocnić Portalisko Pustulskie musi być usprawniona. Wysłał zatem Dobrocienia i Natalię Kazuń by się tym zajęli. Oni zamiast usunąć lokalnego dziwaka, Karola Marzyciela, zbadali magitrownię - okazało się, że ona "żyje" i będzie działać sama z Marzycielem jako swoim awatarem.

# Zasługi

* mag: Natalia Kazuń, była poszukiwaczka artefaktów i aktualna inwestorka, której umiejętności przydały się niezmiernie przy stabilizacji magitrowni Histogram i dojściu do tego, co jest nie tak.
* mag: Jakub Dobrocień, poczciwy i niekonfliktowy kierowca ciężarówki na usługach mafii, który próbuje zreperować magitrownię i usunąć problem tak, by nikomu nie stała się krzywda.
* czł: Karol Marzyciel, człowiek mieszkający i zarządzający magitrownią Histogram, z którym magitrownia się silnie sprzęgła. Nie jest efemerydą ani viciniusem... ale jest już Inny.
* mag: Robert Sądeczny, mafioso, który próbuje przejąć kontrolę nad magitrownią Histogram i wysłał tam Dobrocienia i Kazuń. Bez większego powodzenia.
* mag: Apoloniusz Bankierz, alchemik i biznesmen wchodzący na nowe rynki.

# Lokalizacje

1. Świat
    1. Primus
        1. Mazowsze
            1. Powiat Pustulski
                1. Męczymordy
                    1. Magitrownia Histogram, która jest dużo bardziej żywa niż komukolwiek się wydawało. Komunikuje się przez Karola Marzyciela.


# Czas

* Opóźnienie: 100
* Dni: 2

# Wątki kampanii

-

# Narzędzia MG

## Opis celu misji

-

## Cel misji

-

## Po czym poznam sukces

-

## Wynik z perspektywy celu

-

## Wykorzystana mechanika

Postać (strona gracza):

| .Motywacja. | .Umiejętności. | .Magia. | .SiłySłabości. | .Znam.    | .Mam.      |.POSTAĆ. |
|   (-2) - 2  |     0 - 2      |  0 - 2  |    (-2) - 2    | Z+M = 0-6 | Z+M = 0-6  |         |

Opozycja (strona NPC):

* Baza: 2
* Aspekt: 3/aspekt, wariant z nożyczki/papier/kamień
* Modyfikatory: Skala, Sprzęt, Sytuacja (1-3 każdy)

Wynik:

| .Postać. | .Opozycja. | .Rzut.    | .Wynik.  |
|          |            | xx: +x Wx |   S-SS-F |

Wpływ:

* 8 kart / dzień
* każda porażka i skonfliktowany sukces = +2 pkt wpływu dla gracza
* każda karta = +1 wpływ dla MG
* każde 5 kart zaokr. w górę: +1 pkt wpływu dla gracza

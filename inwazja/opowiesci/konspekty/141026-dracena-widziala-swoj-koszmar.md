---
layout: inwazja-konspekt
title:  "Dracena widziała swój koszmar"
campaign: prawdziwa-natura-draceny
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [141025 - Gildie widziały protomaga (PT)](141025-gildie-widzialy-protomaga.html)

### Chronologiczna

* [141025 - Gildie widziały protomaga (PT)](141025-gildie-widzialy-protomaga.html)

## Misja właściwa:

Z samego rana obudził się terminus Bolesław Derwisz. Poszedł odwiedzić swoją koleżankę, Dalię Weiner i jej nie znalazł. Zmartwiony, zaczął jej szukać po hipernecie, lecz jej nigdzie nie było. Chciał zacząć szukać magicznie, ale jest absolutny zakaz krzyżowo nałożony między Bolesławem Derwiszem a Rafaelem Diakonem. Bolesław, bardzo zmartwiony zniknięciem Dalii umawia się na spotkanie z Rafaelem Diakonem. Ma nadzieję, że ten pozwoli mu rzucić zaklęcia pozwalające na znalezienie Dalii. Rafael jednak nie pozwala. Przypomina, że wcześniej Bolesław sam powiedział, że "prawo jest najważniejsze" (przeszukanie pokoju Draceny bez obecności maga Millennium). Bolesław rzuca, że najpewniej Diakoni stoją za zniknięciem Dalii co Rafael chłodno kontruje, że on jest tutaj i nie czarował. Rafael WIE, że Silgor nie jest taki głupi a Dracena nie ma powodu atakować Dalii niezależnie od tego, czego by ta nie zrobiła. A innych magów Millennium tu nie ma.

Tymczasem lekarz, Ryszard Bocian, podjął pewną decyzję. Wpływ wiły na niego wygasł - Dracena jest uzależniająca i zadziałało to bardzo mocno na viciniusa. Moce wiły przestały działać zbyt szybko; Dyta potrzebuje Draceny. Ale Silgor ani nawet Dyta o tym nie wiedzą. Więc Ryszard Bocian poszedł na komendę policji, do Bartosza Bławatka. Powiedział, że w jego domu jest dziewczyna (DiDi), którą trzeba aresztować za pedofilię. Bo dobiera się do jego niepełnoletniego syna.
Bławatek oczywiście stwierdził, że to trzeba rozwiązać. Poprosił koleżankę, funkcjonariuszkę Alicję Gąszcz o pomoc (aresztowanie potencjalnie rozebranej dziewczyny jest czymś czego nie chce robić sam) i pojechali do domu lekarza.

A co w tym czasie robi Artur Kurczak? Wraz z kolegą, Grzegorzem Murzeckim weszli do mieszkania należącego do Franciszka Marlina i zajmowanego przez Silgora i Dytę. Ta dwójka została wstępnie aresztowana za brak dokumentów połączony z prowadzeniem agencji towarzyskiej i deprawację nieletnich. Do tego doszedł stalking i posiadanie nagich zdjęć nieletnich. Kurczak z wielką przyjemnością mógł spróbować część tego nałożyć na Franciszka Marlina - członka lokalnej grupy przestępczej, którego nigdy Artur nie dał rady tak naprawdę przymknąć.
Z uwagi na gwałtowne reakcje cierpiącej z braku Draceny Dyty, do tego jeszcze doszło agresywne zachowanie podczas próby zatrzymania.
I tak Silgor Diakon wraz z Dytą trafili do aresztu policyjnego a Artur Kurczak mógł zacząć węszyć. Jednak Grzegorz Murzecki uniemożliwił mu wprowadzenie Marii by mogła się rozejrzeć.

Paulina z największą radością obserwowała ten spektakl przez soullink. Sama nie była w stanie się nigdzie ruszyć, zajmowała się bowiem Nikolą i ją uspokajała. Maria powiedziała Paulinie, że chwilowo się musi oddalić. Jako, że wszystkie tymczasowe problemy zostały rozwiązane, ona musi pomóc swojemu kontaktowi (Janowi Fiołkowi) się zainstalować i zrobić briefing. Paulina poprosiła Marię, by ta spowolniła swoje działania - być może nie trzeba przejmować Nikoli już teraz, może uda się przechwycić pełnosprawnego maga.
Paulina doprowadziła Nikolę do stanu spokoju, zostawiła namiar i powiedziała o Maskaradzie - nikt nic nie może wiedzieć. Ma żyć normalnym życiem. Nikola przyjęła do wiadomości i się zgodziła. Paulina odesłała Nikolę do domu (odprowadziła). Jest 11:17.

Gdy Paulina odprowadziła już Nikolę i szła z powrotem do hotelu (spotkać się potencjalnie z DiDi), dostała telefon od Derwisza. Powiedział, że nie wie gdzie jest Dalia i nie wie kiedy zniknęła. A nie może jej poszukać, bo Rafael Diakon go zablokował. Paulina powiedziała, że poprzedniej nocy dowiedziała się o Skażeniu i poprosiła Dalię, czy ta nie mogłaby się tym zająć. Derwisz się zdenerwował - Dalia nic mu nie powiedziała, złamała procedury. Paulina podała mu adres Skażenia. Derwisz tam jedzie. To samo Paulina. Paulina chce trafić tam pierwsza, więc (2 vs 0 -> 6) zdążyła zdecydowanie przed nim.

Blok. Czwarte piętro. Pęknięta szyba. 11:24. Paulina zaczepiła plotkarki dopytać się o to co mogą wiedzieć - okazuje się, że w nocy była tu Dalia (zachowywała się jak złodziejka, ale dzwoniła na domofon). Potem był huk. I jeśli wyszła, to kiedyś w nocy, nikt nie widział jak wychodziła. 
Przyjechał Derwisz. Paulina dowiedziała się wtedy od niego, ku swemu zdumieniu i przerażeniu, że Dalia nie jest terminusem. To jego koleżanka. Ona nie umie walczyć. Paulina sprawdziła szybko zaklęciem (9 vs 8 -> 9) jaki jest stan Skażenia - Skażenie wciąż występuje, ale nie ma źródła, rozprasza się i spadło poniżej masy krytycznej. Paulina wyczuła, że ktoś wcześniej wysadził Skażenie, przekształcając energię. Tego. Się. NIE. Robi. To jest noobstwo, pochopność i niebezpieczeństwo. Paulina zadzwoniła na domofon i na hasło "ulotki" ją wpuścili. Nie ma chwilowo dobrej opinii o Dalii...

Weszli do mieszkania. Na ziemi nieprzytomna, poparzona magicznie Dalia której coś stwór wstrzyknął i stwór, który też jest nieprzytomny i poparzony. Derwisz osłania Paulinę, sam sprawdza mieszkanie i szuka Skażeńców i efemeryd. I na życzenie Pauliny ma znaleźć apteczkę. Paulina szybko przeprowadziła skan magiczny i fizyczny ofiar. (12v6->10) Dalia jest poparzona magicznie, nieprzytomna, sama z tego wyjdzie (zaklęcia puryfikujące i lecznicze). Niestety, jest Skażona. To oznacza, że jej magia jest bardzo chaotyczna. Chłopak (Paweł Brokoty) natomiast był w epicentrum Skażenia. W jego wypadku też nie będzie źle, ale potrzebne mu leczenie; eksplozja Dalii poraniła go wewnętrznie. No i magicznie trzeba go naprawić i ustabilizować. Szczęśliwie, nie powinno być efektów ubocznych jeśli otrzyma szybką pomoc.
No tak, ale jak ich ewakuować sprzed oczu plotkarzy?

Bolesław Derwisz powiedział, że ma zamiar ściągnąć karetkę i normalnie powiedzieć, że rury są rozszczelnione. To oczywiście wiąże się z ewakuacją budynku i kontrolą, ale to SŚ bez kłopotu zrobi. Problem w tym, że Derwisz NIE MOŻE większości zrobić - przeszkadza mu sytuacja z Rafaelem. Ale to co może zrobić to wezwać karetkę i natychmiast ewakuować tą dwójkę. Więc Bolesław Derwisz zadzwonił by ściągnąć lekarza i uruchomić awaryjny depot SŚ poza miastem. A Paulinę czekało trudniejsze zadanie - przekonanie Rafaela.

Tymczasem Bartosz Bławatek i Alicja Gąszcz zwinęli Dracenę. Janek był tak wstrząśnięty tym, że przyszła policja, że kazał Dracenie iść z policją, oddalić się od niego. Dracena poszła z policją i dopiero jak trafiła do aresztu to wróciła jej kontrola. Zorientowała się jak bardzo sytuacja jej się skomplikowała.
Drugą obserwacją było to, że trafiła do tej samej celi co Dyta. Wcześniej Dracena była tak opętana, że nie skojarzyła że widzi wiłę, ale tym razem od razu poznała agentkę Silgora. Chwilę wymieniły się słowami, przy czym Dracena od razu zorientowała się, że wiła się od niej uzależniła. Oraz, że Silgor ma coś wspólnego z tym wszystkim, ale co?
Dyta powiedziała, że Silgor może pomóc Dracenie w uwolnieniu się od Jana Bociana, ale Dracena odmówiła. Ona toczy swoje walki sama.
Wiła natychmiast przekazała informacje Silgorowi.

Silgor wysłał wiadomość do innego członka swojej kolekcji, by ten doprowadził do spotkania pomiędzy nim a Franciszkiem Marlinem. Silgor trafił do aresztu i się mu to zdecydowanie nie podoba, szczególnie teraz, jak w okolicy są magowie Srebrnej Świecy a Dracena się naprawdę solidnie załatwiła. Czas zdjąć jedwabne rękawiczki i zacząć działać we właściwy sposób. Dracena miała swoją szansę.

Franciszek Marlin spotkał się z Silgorem w areszcie. Powiedział Silgorowi, że ten nie miał prawa robić takich rzeczy w JEGO mieszkaniu i jest wściekły. On naprawdę chciał działać uczciwie. Silgor mu odpowiedział, że mogą sobie bardzo pomóc, tylko Franciszek musi zapłacić za niego kaucję. Gdy ten wyśmiał Silgora, czarodziej kazał mu spojrzeć na Dytę i obiecał mu jej ciało. Plus korzyści. Między innymi rozwiązanie problemu pewnego bardzo upierdliwego policjanta i wprowadzenie na jego miejsce kogoś, kto będzie Franciszka popierał, a przynajmniej mu nie przeszkadzał. Marlin powiedział, że nie jest zainteresowany (bo to może być podpucha), na to Silgor powiedział, żeby Franciszek go wypuścił. I wtedy Franciszek sam zobaczy ile Silgor może dla niego zrobić...

Tymczasem Dyta cierpi w obecności Draceny, której nie może dotknąć. Dracena z przyjemnością prowokuje wiłę - wie, że jeśli wiła zacznie się do niej dobierać już w areszcie, stanie się z nią coś złego (policja zareaguje). Wiła przeklina Diakonkę pod nosem, ale wytrzymała (kość szczęścia -> 20).

Równolegle Maria oprowadza Fiołka po mieście. Pokazuje mu wszystkie istotne punkty, miejsca, które mogą go interesować i opisuje mu sytuację. Artur Kurczak natomiast korzysta z tego, że chwilowo może zrobić przerwę i w przyspieszonym tempie przystosowuje jeden pokój do przetrzymywania magów (zgodnie z wytycznymi Marii, lapisuje pomieszczenie w piwnicy). Fiołek dostaje zdjęcia lub rysunki wszystkich zlokalizowanych magów (Silgora, Dyty, Bolesława, Dalii, Pauliny, Nikoli) z opisami i jak wobec nich należy się zachować i komu za żadne skarby nie wolno robić krzywdy (na liście są tylko Paulina i Nikola).

Paulina zadzwoniła do Rafaela. Powiedziała mu o sytuacji i poprosiła, by Rafael zaakceptował wprowadzenie lekarza z SŚ by pomógł biednej Dalii i biednemu Pawłowi Brokoty. Rafael się nie zgodził. A dokładniej, zgodził się pod pewnymi warunkami: jako, że cała sytuacja tak naprawdę jest elementem egzaminu, normalnie on (Rafael) by się tym zajmował. Gdyby nie blokada ze strony Świecy do całej tej sytuacji by nie doszło. W związku z tym Rafael chce, by nie były wyciągane konsekwencje wobec Draceny, Rafaela lub samego Millennium.
Paulina przekazała to Derwiszowi. Bolesław się na to nie zgodził. Jak to, bez konsekwencji? Mamy Nikolę, protomaga, któremu prawie zrujnowano przyszłość. Owszem, SŚ zawaliła pierwsza, ale Millennium mogło pomóc. Mogło ją uratować, a nie bawić się w chore gierki. Dalej, gdzieś tu jest człowiek, element egzaminu, którego Rafael zmienił. Częściowo uniósł Maskaradę. Częściowo przekształca go w viciniusa. Tu się dzieją złe rzeczy. A teraz to - ten biedak. I jeszcze ranna Dalia. I to ma być bez konsekwencji?
Przyjechała karetka. Wszyscy trafili do karetki, rozpoczęto ewakuację budynku bo gaz i dyskusja toczyła się dalej w karetce.

Silgor Diakon wyszedł za kaucją. Franciszek Marlin zapłacił. Jednak Franciszek nie zdecydował się za kaucją wypuścić Dyty. A przynajmniej do momentu, aż Silgor nie udowodni swojej dla niego przydatności. Akurat na komendzie była Alicja, która zobaczyła, że Franciszek był tym który kaucję zapłacił i jako dobra koleżanka poinformowała o tym Artura Kurczaka (tego, kto jest najbardziej zainteresowany Franciszkiem, bo chce od dawna go utopić).

Powrót do Pauliny. Derwisz wypytał Paulinę o dokładny stan Dalii. Jest Skażona. Wyjdzie z tego, ale im bardziej się skazi, tym więcej czasu zajmie jej wyjście z tego. Rozmowy z Rafaelem utknęły - ani Bolesław, ani Rafael nie chcą ustąpić. Bolesław zadzwonił do depotu i zażądał przygotowania tam stymulantów magicznych, takich, jakie pamięta z czasów jak Andrea Wilgacz użyła podczas walki z gorathaulem ("Lord Jonatan"). Nie jest w stanie ściągnąć maga - jest w stanie zrobić to inaczej.
Paulina uniosła brwi. To niebezpieczne i potężne środki. Derwisz powiedział jej, że widział jak to jest robione. Ona może postawić Dalię na nogi. Paulina odmówiła - primum non nocere. Derwisz powiedział, że nie pozwoli, by coś złego się stało Dalii. Ale to co się tu dzieje wymaga działania. A Diakoni próbują go zatrzymać zdecydowanie za bardzo. I to wszystko jest chore.
Paulina dostała też komunikat mentalny od Marii - Silgor Diakon jest na wolności. Dyta nie wyszła, ale Silgor tak. Zapłacono za niego kaucję (Franciszek). 
Paulina szybko zadzwoniła do Rafaela - czy Franciszek ma jakiekolwiek powiązania z Silgorem? Nie. Czy Silgor ma magię mentalną? Nie. Ok...

Dobrze. Paulina wykonała grupę telefonów do znajomych lekarzy. Szuka kogoś, kto jest lekarzem magicznym i może pomóc tej dwójce bez zadawania pytań. (10v6-8-10 -> 10) Jest w stanie ściągnąć maga SŚ który nie będzie zadawał za dużo pytań, ale pomoże zarówno Dalii jak i Pawłowi. Pojawi się jednak za dwie godziny.
Ale tam jest Silgor, który wyszedł na wolność! A na pewno pierwszym celem Silgora będzie Nikola. Spanikowana Paulina (uwięziona w depocie przez 2h) natychmiast skontaktowała się z Marią i ostrzegła błagając, żeby ta coś zrobiła. Sama natomiast porozmawiała z Derwiszem. Powiedziała, że jest tam jeszcze jeden Diakon. Dowiedziała się tego niedawno. On jest niezależny, poza kontrolą Rafaela. I niebezpieczeństwo jest takie, że on może chcieć porwać Nikolę. Ona, Paulina, zostanie w tym depocie aż pojawi się mag SŚ który ją zmieni. Ale niech on jedzie pilnować Nikolę. I to asap. (5+4v7->9) Derwisz się zgodził. A Maria powiedziała, że dopóki Derwisz się nie pojawi, ona pilnuje Nikoli. Nie powiedziała szczegółów, ale Jan Fiołek rozstawił karabin snajperski i pilnuje.

Tymczasem Silgor Diakon nie skierował się do Nikoli. Byłoby to miłe, ale ma ważniejsze rzeczy do zrobienia. Poszedł do Artura Kurczaka, poczekał, aż będzie miał okazję i doprowadził do tego, że Artur nieszczęśliwie spadł ze schodów. Wstrząs mózgu i konieczność hospitalizacji. Potem oddalił się spokojnie i powiedział Franciszkowi Marlinowi jak wygląda sytuacja. Sam natomiast udał się do furgonetki, którą przyjechał inny element jego kolekcji i zabrał stamtąd dwie bransoletki. Artefakt, za pomocą którego będzie w stanie kontrolować Dracenę. Zabrał też jeszcze jeden artefakt, generujący pole ograniczające percepcję i skierował się spokojnie w kierunku aresztu.

Karetka zabrała Artura Kurczaka i Bartosz Bławatek zaczął badać okoliczności upadku Kurczaka ze schodów. Ale nic się nie stało, nie ma śladów. Po prostu nic. Silgor użył minimalnej energii magicznej by zapewnić poślizgnięcie i odpowiedni upadek. 

Tymczasem na komisariacie Silgor poszedł odwiedzić swoją wiłę na komisariat. Odpalił tam artefakt generujący pole ograniczające percepcję (przez co policjanci nie zauważyli, co tam się dzieje) i podał Dycie bransoletki. Dyta rzuciła się na Dracenę. Czarodziejka nie miała szans z silną fizycznie wiłą; Dyta założyła jej bransoletki i Dracena została częściowo unieszkodliwiona. Plan Silgora zaczyna się realizować. Silgor opuścił komisariat zabierając artefakt depercepcji (chowając go do furgonetki) i powiedział Marlinowi, że chce, by za kaucją wyszły zarówno Dyta jak i Dracena. Powiedział, że jeśli cokolwiek będzie nie tak, Silgor bierze za to pełną odpowiedzialność. Powiedział też Franciszkowi, że jeśli wypuści Dytę za kaucją, może pójść z nią do łóżka, ale koło Dyty cały czas ma być Dracena.

Tymczasem Derwisz osłaniał niczego nieświadomą Nikolę. A Paulina została świadkiem wybudzenia Dalii przez lekarza SŚ. Dalia jest jeszcze pod wpływem tego, co wstrzyknął jej przekształcony Paweł Brokoty. Innymi słowy, jest podniecona. A stres jedynie wzmacnia ten efekt. Pytała o stan chłopaka - Paulina ją uspokoiła, będzie dobrze. Dalia dodała, że to nie była Soczewka. To był Paradoks i to Paradoks przy otwartym źródle, otwartym węźle. Doszło do akumulacji energetycznej, bo widocznie Paradoks albo zarezonował z prawdziwą naturą węzła albo doprowadził do trwałej zmiany węzła. Paulinie się to bardzo nie spodobało. Dalia powiedziała, że jedyne co mogła zrobić w takim czasie... spanikowała i wysadziła energię magiczną. Dalia przeprosiła Paulinę i powiedziała, że Nikola jest na głowie Pauliny.

Paulina zamówiła taksówkę z powrotem do Wykopu Małego. I dostała kolejny komunikat od Marii. Artur Kurczak jest w szpitalu. Wstrząs mózgu. Spadł ze schodów. Paulina od razu skojarzyła Silgora, który wyszedł z aresztu. Powiedziała Marii, że jest w stanie zobaczyć jeszcze ślady magiczne - bo cokolwiek Silgor nie zrobił, ona już widziała jego sygnaturę oraz to było zbyt niedawno a ona jest za dobrą katalistką by mogła to ukryć. Maria powiedziała, że osoby, które aresztowały Silgora to był Artur Kurczak oraz Grzegorz Murecki. Paulina poprosiła Marię, by Jan Fiołek spróbował osłaniać Mureckiego (gdzie z pewnością Silgor pójdzie) i żeby go unieszkodliwił. Maria powiedziała, że on ma metody na unieszkodliwienie maga tak, by ten nie był w stanie nic z tym zrobić. Sama natomiast Paulina przestawiła taksówkę na miejsce, gdzie Kurczak spadł ze schodów.

Silgor Diakon poszedł do miejsca, gdzie Jan Bocian rozmawia sobie z dwoma kolegami. Używając tak samo prostego zaklęcia kinetycznego doprowadził do tego, że Janek się przewrócił i skaleczył się w łokieć. Silgor patrzył ze spokojną radością jak nagle błogosławiona i skuteczna krew Janka zaczyna działać na kolegów Janka w ten sam sposób co wcześniej na dziewczyny. Gdy już zaczęło być nieco bardziej poważnie (do Janka dotarło, że jego koledzy chcą go zgwałcić) Silgor wszedł do akcji. Osłaniany przez odpowiednie kontrzaklęcie wszedł tam, wyciągnął Janka i zaczął z nim biec do samochodu. Jego koledzy biegli wolniej (z opuszczonymi spodniami powoli się biega). Silgor powiedział Jankowi, że jak widać, jego dar nie jest tylko i wyłącznie zaletą ale i wadą. Ale on, Silgor ("Stanisław") będzie w stanie mu pomóc.

Tymczasem Paulina zadzwoniła do DiDi. Ta jednak nie odebrała telefonu. Paulina dzwoniła kilka razy, ale bez efektu. W związku z tym Paulina zadzwoniła do Rafaela. Powiedziała mu, jaki to tak naprawdę był Paradoks Draceny. Rafael powiedział, że to oznacza, że Dracena zmieniła sobie wzór. W jakiś sposób się przekształciła. Paulina powiedziała też, że Silgor wyszedł z aresztu. Rafael stwierdził, że Silgor nie doprowadziłby do takiego Paradoksu Draceny; nie ten typ maga. Ale jak już się stał, zdecydowanie może chcieć go wykorzystać. A Rafael nic nie może zrobić, bo SŚ. Zapytany, gdzie Dracena mogłaby chcieć się ukryć, Rafael powiedział, że u dowolnej osoby dowolnej płci jaka była na imprezie organizowanej przez Dracenę. Jako dziewczyna na jedną moc. Ostrzegł też Paulinę, że to MUSIAŁABY być na jedną noc, bo Dracena jest uzależniająca. Elementem jej wzoru jest ekstadefiler i pasywnie zbyt częsty (osobniczo, ale zawsze częściej niż 3 razy w tygodniu u ludzi i magów) seks z Draceną powoduje uzależnienie od seksu z nią i od samej Draceny.
Super. Paulina nie ma żadnego śladu. Ale ma pomysł.

Paulina dotarła pod dom Kurczaków. Przygotowała się do szukania śladów działania Silgora. (9v6/8->8) Paulina wykryła minimalne zaklęcie kinezy. Silgor po prostu Artura poślizgnął i lekko obrócił w powietrzu. W oczach Pauliny Silgor skoczył o dwa oczka do góry na skali niebezpieczeństwa i sukinsyństwa. Mag, który używa minimalnej potrzebnej mocy jest groźny. Ale Paulina wie o Silgorze a Silgor nie wie o Paulinie.
I kolejny komunikat od Marii - była na komendzie policji i tam się dowiedziała, że została zapłacona kaucja za Dytę oraz za "pedofilkę". Maria wypytała i opis pasował do Draceny. Okazuje się, że te dwie były razem w celi przez pewien czas, właściwie od wczesnego południa. Nie dała rady dowiedzieć się więcej, bo przyszedł policjant Bartosz Bławatek i powiedział, że nie wolno takich rzeczy ujawniać. Osobą, która zapłaciła kaucję był Franciszek Marlin.

Paulina zadzwoniła do Rafaela po raz kolejny. Poprosiła o informacje dotyczące Silgora i Draceny. Powiedziała Rafaelowi, że Dracena i Dyta były w jednym areszcie. Że najpewniej Silgor ma Dracenę. Paulina nie chce, by Dracena stała się elementem kolekcji Silgora. Jakim cudem jemu tak wolno?! Rafael powiedział, że Dracena sama sobie to zrobiła - nie zgłosiła czarodziejki (Nikoli), więc Silgor zablokował Rafaela. A teraz Derwisz zablokował Rafaela z drugiej strony. Paulina postawiła sprawę jasno i twardo. Ona nie chce, by Nikola dołączyła do gildii, w której ktoś taki jak Silgor może dokonać gwałtu na osobowości Draceny i, co gorsza, przekształcić Dracenę zgodnie ze swoim widzimisię. Dracena chciała pomóc ludziom. Jest często kiepska, nie daje sobie w wielu kwestiach rady, nie jest wprawna w tym co robi, ale Nikola też nie będzie.
Gdy tak postawiła sprawę, Rafael musiał zmienić zdanie. Powiedział Paulinie, że celem Silgora od samego początku jest Dracena; jeśli ruszyłby w kierunku Nikoli, spowodowałby poważne spięcia między Millennium a SŚ. Powiedział też, że jeśli Paulina go odblokuje przed Derwiszem, może znaleźć Dracenę. Powiedział, że to co on zrobił - nadał chłopakowi, Janowi Bocianowi (lat 15) dar bycia nieodpartym dla kobiet. Ale zrobił to tak, że ten dar jest coraz silniejszy. Pierwotnie "te dziewczyny jego, którym się podoba". Teraz już najpewniej (po 2 tygodniach) "każdy człowiek, mag i wiła jego". Działa to na bazie krwi. I jeśli Dracena w głupi sposób poszła się z Jankiem spotkać, jest po niej.

W odpowiedzi na te informacje Paulina natychmiast pojechała się spotkać z Bolesławem Derwiszem. Powiedziała mu, że ten Paradoks uszkodził trwale czarodziejkę, Dracenę Diakon. Dracena zawsze chciała ratować ludzi i im pomagać. Paulina opowiedziała Bolesławowi o Dracenie i o tym, że jest jeszcze jeden Diakon - Silgor Diakon - który teraz na Dracenę poluje. I chce dodać ją do swojej kolekcji. Powiedziała, że ten Diakon odpowiada za wiele szkód u ludzi. Że on odbiera osobowości. Zaapelowała do Bolesława Derwisza - jest terminusem. Nieważne, że to Diakonka. To Diakonka lepsza niż Silgor, a jak stanie się elementem jego kolekcji, tacy jak Silgor wzrosną w moc.
Bolesław Derwisz powiedział, że on zna Silgora Diakona. Ma z nim osobisty zatarg. I tak, jako terminus jest zobowiązany do pomocy Dracenie. Tak, pomoże Paulinie znaleźć Dracenę.

Tymczasem gdzie jest Dracena? Dracena wraz z Silgorem są w domu Jana Bociana. Wiła została z Franciszkiem, by spłacić długi Silgora, jednak sam Silgor już skupił się na rozmowie z młodym chłopakiem. Pierwsze co, to poinformował go, że jego krew ma specyficzne własności i będzie jeszcze gorzej. Niech wyobrazi sobie co będzie, gdy zacznie działać to na jego rodziców...
Gdy Jan był już wystarczająco wystraszony, Silgor zaproponował mu wybawienie od tego wszystkiego. Chłopak chciał kilka różnych dziewczyn, Silgor się nie zgodził. Ma być jedna. I on odbierze mu pamięć, lecz Jan będzie miał najbardziej lojalną, wiecznie kochającą go dziewczynę. Chyba, że ją będzie chciał zranić. Ten piękny sen się skończy póki jeszcze jest pięknym snem.
Ale ale, by do tego doszło, Silgor musi móc się włączyć w egzamin. Więc zwrócił się do Draceny by go o to poprosiła.

Dracena odmówiła. 
Silgor powiedział Janowi, by kazał Dracenie odrzucić muzykę raz na zawsze.
Zanim Janek zdążył to powiedzieć, Dracena poprosiła Silgora by pomógł jej z egzaminem. Silgor ją pochwalił - zna swoje miejsce.
W ten sposób Silgor może włączyć się w egzamin.

Silgor Diakon obiecał Janowi Bocianowi szczęśliwą miłość ze wzajemnością, szansę utrzymania jakiejkolwiek jednej dziewczyny jakiej chce, pięknych snów i brak konsekwencji wszystkiego co było do tej pory. W zamian za to chce od Jana Bociana przekazania kontroli nad Draceną jemu i próbkę krwi, by Silgor mógł syntetyzować w sobie krew Bociana.
I mniej więcej wtedy do akcji weszli Paulina i Bolesław Derwisz. Derwisz powiedział Paulinie, że on nie może uderzyć pierwszy Silgora Diakona, bo Rafael. Nie może. Może jedynie się pojawić i rozmawiać, ale nie może samemu działać. Ma tylko nadzieję, że Silgor Diakon tego nie wie.

Bolesław Derwisz wszedł jawnie, jako dywersja dla Pauliny.
Wszedł jak do saloonu, hucząc "Silgor Diakon". Oczywiście, Silgor nie rozpoznał Derwisza. Po przedstawieniu się przez Derwisza, Silgor powiedział, że przecież Bolesław Derwisz nic nie może, bo Rafael Diakon a teraz są w trakcie egzaminu. Na co Derwisz odparł, że jego egzaminy nie interesują, jest terminusem i nie pozwoli, by czarodziejce stała się krzywda. A na pewno nie przez plugawego Diakona. Na to Silgor stwierdził "ach, to ten..." i zauważył, że faktycznie, on magiem bojowym nie jest.
Jednak problem polega na tym, że Silgor może sprawdzić jak daleko Bolesław Derwisz się posunie. A jeśli posunie się za daleko, SŚ będzie miała problemy.

I w reakcji na to weszła do pomieszczenia Paulina. Dumna, wyprostowana. Zarzucając jak Millennium traktuje swoją własną rodzinę. Idąc w kierunku Silgora. Paulina zaczyna się uspokajać, wyciszać, ale powiedziała, że te głupie gierki mają narażać maga i ludzi. Dodaje, że rozważała opcję przyłączenia do Millennium. Po spotkaniu Draceny Paulina myślała, że spotkała kogoś, komu leży na sercu los nie tylko magów. Ale to co widzi... to jest ohydne.
Działanie Pauliny poruszyło Dracenę. Młoda Diakonka padła na kolana przed Pauliną i poprosiła, by Paulina jej pomogła. Silgor spojrzał na Dracenę zaskoczony i jej nie przerwał, a Dracena powiedziała Paulinie numer telefonu. Telefonu ojca Draceny.
I Silgor stwierdził, że to jest koniec. Bo on nie może zaatakować Pauliny, bo terminus go usmaży. Zastanowił się chwilę i zaznaczył, że jeśli ojciec Draceny się dowie o tej sytuacji, Jan Bocian umrze. Ponieważ ani on (Silgor) ani nikt inny nie będzie w stanie naprawić wzoru Draceny po tym, co Dracena sobie zrobiła. Ten chłopak od teraz do końca świata będzie w stanie kontrolować Dracenę jeśli wyda jej rozkaz.
Dracena stwierdziła, że nie chce, by ktoś go zabił. Jedyna osoba jaka powinna zginąć to Silgor.
Silgor is amused.
Silgor zaznaczył, że widział przełom. Nie tylko Dracena zorganizowała dookoła siebie czarodziejkę niezależną, ale i terminusa który chce ją chronić. Dodatkowo nie tylko poprosiła Paulinę o pomoc, ale była skłonna poprosić też o coś swego ojca. To oznacza, że Silgor na pewno nie dołączy Draceny do swojej kolekcji i tak naprawdę Dracena wypada poza jego strefę wpływów. Przestawił bransolety sterujące Draceny z trybu "kontroli" na tryb "inhibitora" i powiedział, że ma mu je oddać później - ale jak odda teraz, to wpadnie pod kontrolę Jana Bociana.

Po czym Silgor odwrócił się i chciał odejść. Paulina go zatrzymała - co on zrobił Dracenie. Silgor stwierdził, że nic. Nic nie musiał robić. Jedynie Dyta jeden raz musiała rzucić kotem w Jana Bociana. Tu Silgor zatrzymał się i spytał Janka ile razy. Ten się spłonił, więc Silgor spojrzał mu w oczy i się uśmiechnął. Jan Bocian jest uzależniony od Draceny Diakon. Zauważył, że "jego wiła", czyli Dyta też. Poprosił Dracenę o trochę krwi, by mógł przygotować antidotum. Dracena zaśmiała mu się w twarz i powiedziała, że to niemożliwe. Silgor spojrzał na Dracenę i powiedział jej, że Dyta nic jej nie zrobiła a symptomy odstawienia od Draceny są straszliwe. Jeśli Dracena jej nie pomoże, niech ją sama zabije. Bo inaczej on to zrobi, coup de grace. Dracena stwierdziła, że Dyta będzie musiała zostać z Draceną jakiś czas.
Silgor się zgodził. Stwierdził, że jako, że Dyta i Janek są uzależnieni, i to się objawia koniecznością spędzania wspólnej nocy, to Dracenę czekają pracowite noce a Janka piękny sen jeszcze się nie skończył. Dracena zaznaczyła, że jeśli Janek wyda jej jakikolwiek rozkaz, to ona go osobiście zabije.

Próg pogardy Pauliny do Silgora i nienawiści Draceny oraz Derwisza do Silgora jest bardzo wysoki. Ale Dracena powiedziała Silgorowi, że jeśli COKOLWIEK się stanie Paulinie, Dyta ucierpi pierwsza. A jak już ją wyleczy, to zobowiązuje Silgora do ochrony Pauliny. Bo on mógłby być z tych mszczących się na słabszych. Silgor się uśmiechnął i powiedział, że z jego strony nic Paulinie nie grozi. 

Po wszystkim. Dyta i Janek muszą poczekać. Paulina pojechała z Derwiszem do Dalii. Zabrała się z nimi... Dracena. Dracena poprosiła Bolesława, by mogła też pojechać i przeprosić Dalię - w końcu przez nią te wszystkie kłopoty. Bolesław się zgodził, lekko zdziwiony że i Diakon może mówić ludzkim głosem.
Paulina bardzo podziękowała tien Bolesławowi Derwiszowi. Gdyby nie on, mogłoby dojść do katastrofy. Nie musiał pomóc, a pomógł. Tien Derwisz powiedział, że przecież po to został terminusem - by chronić. To, że Diakon, nic nie zmienia. Paulina jeszcze raz mu bardzo podziękowała.
Gdy spotkali się z Dalią, Paulina była świadkiem jak Bolesław przytulił Dalię i ją zbeształ za nie ostrzeżenie go i pójście na samotną akcję. Po czym powiedział, że była dzielna i sobie poradziła. Paulina też podziękowała Dalii i powiedziała, że wszystko dobrze się skończyło. Dalia spytała, czy nigdy już nie zobaczy Nikoli (bo Paulina wybierze inaczej). Paulina powiedziała, że nie wie. Dalia się uśmiechnęła i powiedziała, że jej zainteresowanie bierze się z tego, że ona też kiedyś była "Nikolą" a samotnej czarodziejce nieświadomej co i jak bardzo trudno w życiu. Paulina potwierdziła.
Gdy Dracena przeprosiła Dalię, ta spytała czy Dracenie nic nie jest. Ta skłamała, że rzucała czar detoksyfikacji przy otwartym węźle z quarków. Dalia dała się nabrać.
Dracena powiedziała, że ten chłopak, Paweł Brokoty, uratował DiDi gdy ta była jak naćpana i w złym stanie. I tak mu się Dracena odpłaciła...

Paulina po wszystkim spotkała się z Rafaelem. Nie chciała zdawać dokładnego sprawozdania, ale Rafael jej powiedział, że to nie ma znaczenia dla niego. Kluczem było to, że Dracena sobie uświadomiła że sama nie jest w stanie zrobić wszystkiego plus że faktycznie rozwiązała problem. Rafael zaznaczył, że aktualna sytuacja jest fascynująca - dwie potężne gildie polują na tą samą Nikolę, a na miejscu jest Dracena próbująca wyleczyć agentkę jej śmiertelnego wroga oraz człowieka, który jednym słowem może ją zniewolić. A w tym wszystkim - Paulina i porady prawne Rafaela Diakona. Rafael powiedział, że nie zapomniał o owej EAMce. Poprosił też Paulinę, by ta spróbowała przekonać Dracenę, by ta pozwoliła Rafaelowi sobie pomóc. Rafael będzie chciał zasklepić jakoś uszkodzenie wzoru, jakie zrobiła sobie Dracena. Ale Dracena mu na to nie pozwoli. Może Paulina ją przekona. W końcu - na razie ma winning streak.

Dracena otworzyła drzwi na pukanie Pauliny. Ku zaskoczeniu Pauliny, nie jest ubrana w swój standardowy strój cybergoth. Dracena bardzo podziękowała Paulinie. Paulina uruchomiła niesamowitą maszynę do pomocy Dracenie, negocjowała pomiędzy magami, między gildiami, musiała wyjść z ukrycia, nie wiadomo jakich korzyści nie obiecywała, jakich sił nie ściągała, stanęła naprzeciw Silgora... a przecież Paulina nawet Draceny nie zna. Paulina wybrała uratowanie Draceny... a przecież Silgor mógłby dać Paulinie cokolwiek ta tylko chciała. I tak, Dracena świetnie sobie z tego zdaje sprawę. Bardzo gorąco podziękowała Paulinie. Uratowała Dracenę od losu, którego nawet sobie nie wyobraża.
Paulina widzi, że Dracenę strasznie torturuje poczucie winy. Skrzywdziła tak wielu magów i ludzi. Dalia, Nikola... ale przede wszystkim Paweł Brokoty. Chłopak, który pomógł jej w najczarniejszych chwilach i prawie stracił człowieczeństwo. Też Dracena umiera ze strachu nad tym, co ten Paradoks jej zrobił. Jest wolnym ptakiem, niezależnym, a nagle może na jeden rozkaz stać się niewolnikiem. Jej najczarniejszy koszmar.
Dracena wyznała Paulinie, że nie dogaduje się dobrze z magami. Rodzina to mafia. Owszem, może są lepsi niż inne gildie, ale nieznacznie. Przykład - Silgor. Przykład - farmy ludzi. Dracena nie jest w stanie rozmawiać czy dobrze czuć się wśród magów, którzy są mili, uprzejmi a zaraz potem wracają do swoich lojalnych niewolników i traktują ich jak przedmioty. A Świeca? Wcale nie lepsza, Bolesław Derwisz i Dalia Weiner to były wyjątkowe przykłady. Częściej spotyka się Sebastianów Tecznia. Dracena powiedziała Paulinie, że jest jedyną czarodziejką, z którą tak naprawdę rozmawiała. Jedyną, która coś sobą prezentuje. Nie chce nikogo wykorzystać, ani ludzi, ani magów.
Paulina poprosiła Dracenę, by ta zaufała jej i pomogła sobie pomóc i zasklepić tą Paradoksalną ranę. Zaznaczyła, że sama nie będzie w stanie tego naprawić; nie jest w stanie nawet w pełni zrozumieć co się stało. Będzie potrzebowała wsparcia. I niech Dracena zaufa, że Paulina dokona dobrego wyboru by jej pomóc.


# Lokalizacje:

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Leere
                1. Wykop Mały
                    1. Królestwo
                        1. Ekskluzywne domki, gdzie od Marlina domek wynajął Silgor
                        1. Komenda policji w Wykopie
                            1. areszt
                    1. Osiedle Ludowe
                        1. domki, gdzie mieszka min. Nikola Kamień, Jan Bocian i Kurczakowie
                        1. familoki, gdzie znajduje się mieszkanie Brokotych i gdzie Skażona została Dracena
                        1. hostel Opatrzność, tymczasowo mieszka tam Paulina
                        1. Kafejka U Sępa
                    1. Nieużytki wykopskie
                        1. Awaryjny depot Świecy

# Pytania:

N/A

# Stakes:

Żółw:

- Czy Nikola trafi do Millenium? I czy pod opiekę Draconisa czy Silgora? -> Na pewno nie Silgora.
- Jak będzie z relacją między Bolesławem Derwiszem a Dalią Weiner? -> Bliska. Ale jaka dokładnie, jeszcze nie wiemy.

# Idea:

- Wprowadzenie Silgora Diakona jako interesującego maga-kolekcjonera Millenium. "Nie stać nas na fanaberie Draceny."
- Pokazanie Millenium jako dość okrutnej gildii RODZINNEJ. Dracena nie może "ja siama".
- Pokazanie Millenium jako miejsca w którym po prawdzie Paulina nie czułaby się dobrze.
- Eksploracja Draceny nie tylko jako wesołej cybergothki ale i z innych stron.

# Zasługi

* mag: Paulina Tarczyńska, prowadząca negocjacje pomiędzy gildiami i magami dużo potężniejszymi od siebie. Żongluje potężnymi siłami tak, by jak najmniej istot ucierpiało i w jak najmniejszym stopniu.
* czł: Maria Newa jako wierny cień i bardzo skuteczne źródło informacji.
* mag: Dracena Diakon jako Zosia*Samosia, która naprawdę nie chce nikomu robić krzywdy... aż do samego końca.
* mag: Rafael Diakon jako mag EAM z Millennium który ma związane ręce, ale jednak zdecydował się COŚ zrobić.
* mag: Bolesław Derwisz jako terminus*mentalista o związanych rękach, który próbuje znaleźć sprawiedliwość w świecie gildii magicznych.
* mag: Dalia Weiner jako nieprzytomna puryfikatorka która poszła na akcję bez przygotowania.
* mag: Silgor Diakon jako mag Millennium wykorzystujący pełną gamę dostępnych mu środków do osiągnięcia celu.
* mag: Nikola Kamień jako czarodziejka, dla której trzeba znaleźć jak najszybciej gildię.
* vic: Dyta jako Silgorowe narzędzie przekupstwa będące uzależnioną od Draceny wiłą.
* czł: Ryszard Bocian jako lekarz który postanowił zaryzykować swój związek, swą rodzinę by uratować syna.
* czł: Jan Bocian jako nastolatek który orientuje się, że jego dar jest straszliwą klątwą.
* czł: Artur Kurczak jako policjant, który zapłacił za to, że bardzo blisko współpracował z Marią i Pauliną.
* czł: Bartosz Bławatek jako policjant który jest bardzo czuły na to, co policjantowi wolno a co nie.
* czł: Jan Fiołek jako snajper wezwany przez Marię, który rozstawia karabin w wielu miejscach ale nie strzela ani razu.
* czł: Alicja Gąszcz jako policjantka ubierająca Dracenę w mieszkaniu nieletniego chłopaka.
* czł: Grzegorz Murzecki jako policjant aresztujący maga i wiłę.
* czł: Franciszek Marlin jako członek "bieda*mafii" skorumpowany skutecznie przez Silgora.
* czł: Paweł Brokoty jako nieprzytomny fan DJ Draceny zmieniony przez jej Paradoks w niebezpiecznego viciniusa.

---
layout: inwazja-konspekt
title:  "Chora terminuska i Żabolód"
campaign: powrot-karradraela
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Wątki

- Szlachta vs Kurtyna
- Wojny wielkich gildii Śląska

## Kontynuacja

### Kampanijna

* [150625 - Poligon kluczem do Marcelina? (HB, SD)](150625-poligon-kluczem-do-marcelina.html)

### Chronologiczna

* [150607 - Brat przeciw bratu (HB, SD, PS)](150607-brat-przeciw-bratu.html)

## Punkt zerowy:

## Misja właściwa:

### Faza 1: The Arrival

Ż: Po co Paulina poszła z Romanem do supermarketu? Czemu?
K: Bo potrzebowała czegoś ze sklepu.

Roman nadal nalega na Paulinę, żeby połączyć rezurentke (Aurelię) z ciałem jakiejś zmarłej dziewczyny lub z osobowością jakiejś zmarłej dziewczyny. On nie chce Aurelii. Nie chce nic powiązanego z Aurelią. Bo problemem Aurelii jest osobowość.

"Zlepek osobowości będzie GORSZY!" - Paulina.
"Będzie nie gorszy od Aurelii, na tym polega problem." - Roman.

Paulina nie zgadza się na łączenie z osobą umarłą - to wypaczy obie osobowości. Zamiast tego zaproponowała coś innego - zintegrować wiedzę EIS i ewentualne tło soulbinda Aurelii z osobą żywą. Inną osobą. Paulina zaproponowała hospicjum - znalezienie starszej osoby, która jest bliska śmierci i zaproponowanie jej "nowego startu". Z usuniętą pamięcią, ze sztucznym narzutem Aurelii, z zapewnieniem nowego, młodego ciała (ciało pochodzące od wyhodowanych ludzi Diakonów). Roman się na to zgodził. Tak, to się może udać...

Paulinie dalej się to nie podoba, ale mniej, niż integracja ze zmarłym.

Roman powiedział Paulinie, że ma zamiar z nią pójść do supermarketu. Ładuje zwykły kryształ Quark "zwykłymi ludzkimi uczuciami". Paulina uniosła brew - w supermarkecie? Roman powiedział, że wszystko jest potrzebne. Paulina się zgodziła - jak długo nie ma wyprzedaży.

W supermarkecie norma - Roman się kręci, Paulina kupuje, dyskusja się toczy... nie ma bardzo dużo ludzi. Nie ma w ogóle dużo ludzi. Normalny dzień. Nagle - silny impuls Skażenia dobiegający z zaplecza. I coś jak krzyk, ale zduszony.

Roman i Paulina poszli w tamtym kierunku. Roman poprosił Paulinę, by poczekała i sam wszedł. Wszedł ostrożnie, ale nie spodziewał się ataku terminuski - Tamara (ciężko ranna i ciężko chora na paranoiczny Lodowy Pocałunek) uderzyła go falą uderzeniową zostawiając na nim kawałki swego ubrania po czym użyła infekcji ubrania i nie tylko go zakneblowała ale i poraziła i rzuciła na ziemię.

Przeprosiła go za niedogodności. Ale nie może ryzykować.

### Faza 2: The Tailor

Paulina przyjrzała się terminusce. Zorientowała się, że ta cierpi na ostrą fazę Lodowego Pocałunku. Ma jakieś 2 dni życia. 
Paulina kazała Romanowi dowiedzieć się jak najwięcej. Roman się przedstawił jako Roman Gieroj, ko-przywódca Powiewu Świeżości. Terminuska parsknęła - PŚ jest pieskiem KADEMu. Roman się żachnął. Tamara się przedstawiła jako Tamara Muszkiet, przywódczyni "Kurtyny". 

"Żabolód jako dowód zła KADEMu. Taaak." - Roman do Tamary.
"Jest Spustoszony. To KADEM stoi za Spustoszeniem i za tym wszystkim." - ciężko chora Tamara, mająca halucynacje.

Tamara uruchomiła lockdown supermarketu i wyłączyła komunikatory. Nikt nie wejdzie, nikt nie wyjdzie. Wysłała flarę (myśli, że to zrobiła - nie ma mocy i energii) by wezwać wsparcie. Nie ma dostępu do hipernetu i jej zdaniem to Roman stoi za tym, że Tamara nie ma do niego dostępu.

"Powiew może Ci pomóc." - Roman do Tamary.
"Powiew pomaga KADEMowi, nie magom chcącym pomagać innym." - Tamara.

Tamara musiała być chora już wcześniej i mieć kontakt z energiami magicznymi. Dodatkowo, ma złamaną prawą rękę usztywnioną przez kombinezon. I reaktor jej kombinezonu przestał działać... i jej reaktor Skaził ją (chorą na Lodowy Pocałunek). To tłumaczy jej stan...
Paulina oddaliła się tak daleko jak była w stanie od Tamary i Romana i spróbowała zadzwonić dyskretnie do Świecy... ale Tamara może monitorować; zamiast tego Paulina użyła soullinka z Marią i poprosiła Marię o zadzwonienie do Świecy jako minion Pauliny. Świeca musi się dowiedzieć, w jakim stanie jest ich terminuska!

Maria zadzwoniła. Telefonistka przełączyła ją do Kermita Diakona, terminusa wysokiego rangą Świecy. Maria powiedziała, że Tamarze grozi ogromne niebezpieczeństwo, jest chora i nie pozwoli sobie pomóc. Paulina zreferowała Kermitowi, że chodzi o ostry Lodowy Pocałunek; w skrócie ostra paranoja i nie wolno jej ani przegrzewać ani użyć w jej kierunku magii.

Paulina powiedziała, że Tamara ma jeńca, maga Powiewu Świeżości. Terminus autoryzował ją do zabicia Tamary, jeśli ona spróbuje zabić więźnia. Paulina nie skomentowała - lekarz nie zabija. Za to zażądała od terminusa możliwość wyłączenia jej osobistego pancerza. Come on, Paulina NIE WIERZY, że Tamara nie ma jakiegoś backdoora; terminus nie może mieć takiego pancerza. Kermit po chwili wahania podał jej sygnaturę wiązki nekro-kata-leczniczej która jest modulowanym sygnałem mającym wyłączyć terminuskę z akcji. Kermit ostrzegł też Paulinę by ta nic nie robiła: póki kombinezon działa, Tamara będzie funkcjonować nawet po śmierci. Tak go zaprojektowała.

Kermit powiedział, żeby Paulina czekała. Ale Paulina zaczęła przygotowywać określone elementy...
I Roman dostał rozkaz od Pauliny - nie prowokuj Tamary.

Tamara poprosiła związaną kobietę w supermarkecie o to, by ta ściągnęła inną. Tamara musi naładować kombinezon. Tamta, przerażona, zawołała koleżankę. "Hela" przyszła, Tamara ją unieszkodliwiła i rozpoczęła drenaż obu ludzi. Roman krzykami zaczął odwracać uwagę Tamary a Paulina wpadła, krzycząc "Tamara Muszkiet do raportu". Terminuska została całkowicie skonfundowana i Paulina odpaliła sygnał dezaktywacyjny. Tamara z przerażeniem zauważyła jak jej kombinezon zaczyna się rozpadać, a drenaż ludzi się nasilił. W rozpaczy odpaliła "Gamma Override" i wyłączyła sygnał dezaktywacyjny (wbrew WSZYSTKIM procedurom Świecy; nie wolno jej było wprowadzić override). Widząc to, Paulina złapała żaboloda i chodu! Tamara podniosła lewą (złamaną) rękę by ją zestrzelić, ale krzyknęła z bólu. Plus, natychmiast przerwała drenaż. Uruchomienie pełni mocy kombinezonu sprowadziło terminuskę na kolana. Tamara zorientowała się, że to ją zabije.

### Faza 3: The Decay

"Dlaczego... Szlachta współpracuje z KADEMem... czemu Spustoszają żabolody... dowód... plan..." - Tamara w powietrze.

Tamara odłączyła część kombinezonu; pozostawiła sobie jedynie lekką uprząż i usztywnienie ręki. To trzyma ją na nogach, ale nie daje jej dużej mocy ani mobilności. Jej kombinezon "założył" Romana. Tamara dała rozkaz "Ty zabrałeś mi moich agentów, więc ich znajdziesz i przekażesz im dowody". Pancerz zaczął integrować się z układem nerwowym Romana. Roman traci przytomność, staje się organiczną baterią kombinezonu Tamary.

Kermit skontaktował się z Pauliną i powiedział, jak wygląda sytuacja - są tu, 10 terminusów. Poprosił Paulinę o dokładny namiar na pozycję Tamary; teraz jak Tamara nie ma na sobie kombinezonu mogą przebić ścianę i zalać ją serią strzałek z chemikaliami. Uprząż jest za słaba; da się ją przeładować. Jedno skrzydło bierze Tamarę, drugie skrzydło bierze Romana w kombinezonie (Kermit nie jest szczęśliwy, że mag Powiewu Świeżości jest w kombinezonie; same z tego problemy).

I tak zrobili. Tamara naszpikowana chemikaliami zaczęła tracić przytomność. Kombinezon z Romanem włączył pełną moc i nagle... zgasł. Jeden z terminusów użył mocy i wypalił linię w kombinezonie. Wyrwał Romana z kombinezonu i wrzucił go w podprzestrzeń.
Paulina przypadła do Romana - wysmażony układ nerwowy, ale będzie żył i działał już za 48h.
Paulina przypadła do Tamary i rozpaczliwie zaczęła ją ratować; Tamara jest ciężko ranna i tylko dzięki Paulinie przeżyła. To, co Paulinę martwi to to, że Tamara za mocno zareagowała na chemikalia; tam musiało być coś jeszcze, jakiś katalizator. Paulina nie ma dowodów, ale ma wysokie podejrzenie, że ktoś chciał zabić Tamarę.

Wśród terminusów - konsternacja. Nie wiedzą, kim był ten terminus, który wyciągnął Romana (jedenasty). A to był Saith Flamecaller, o czym nikt nie wie...

Paulina poprosiła Kermita na stronę. Powiedziała, że najpewniej Tamarę ktoś chciał zabić; powiedziała o katalizatorze. Powiedziała też, że ludziom ma się nic nie stać...
Poprosiła też Kermita o odebranie jej disablera pancerza Tamary. Zostawiła go oczywiście pomiędzy sobą a Marią, ale Świeca myśli, że go nie ma.

I w natłoku zdarzeń zapomniała oddać Kermitowi żaboloda.

### Faza 4: The Assault
### Faza 5: The Death

Obvious, ain't it?

# Streszczenie

Paulina i Roman nadal pracują nad homunkulusem Aurelia/EIS i w procesie wpadają do supermarketu, gdzie ciężko chora na Lodowy Pocałunek Tamara wierzy, że żabolód jest dowodem na zło KADEMu i ich prace nad Spustoszeniem. W ostrej paranoi. Paulina dała radę wezwać wsparcie Świecy przez Marię i soullink. Tamara uruchomiła zakazany override swojego magitecha (niszcząc swoją karierę); dzięki działaniu Saith Flamecallera i Pauliny Tamara przeżyła, ale trafiła do szpitala w ręce Mariana Agresta. Ważny agent Kurtyny zdjęty z akcji. Aha, Paulina ukradła ukradzionego żaboloda. Plot thickens.

# Lokalizacje:

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Nowy Kopalin
                        1. Powiew Świeżości
                    1. Dzielnica Trzech Myszy
                        1. Supermarket "Smakołyk"
               
# Zasługi

* mag: Paulina Tarczyńska, lekarz, która zidentyfikowała Lodowy Pocałunek i bardzo dużo ryzykowała, by chora terminuska przeżyła.
* czł: Maria Newa, cichy "minionek" Pauliny który routuje dookoła zabezpieczeń Tamary (dzięki czemu wszystko dobrze się skończyło).
* mag: Roman Gieroj, ko-przywódca Powiewu, zakładnik, ciężko uszkodzony przez Tamarę który jednak nie zamierza się na chorej mścić.
* mag: Tamara Muszkiet, ciężko chora na Lodowy Pocałunek, bardzo paranoiczna, która jednak próbowała nie zabić. Złamała wszystkie zasady Świecy.
* mag: Kermit Diakon, terminus wyższy rangą, który silnie współpracował z Pauliną by uratować Tamarę przed nią samą.
* mag: Saith Flamecaller, tajemniczy jedenasty terminus, który wyciągnął Romana i zabezpieczył kombinezon Tamary przed badaniami i Świecą.
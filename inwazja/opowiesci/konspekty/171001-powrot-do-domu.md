---
layout: inwazja-konspekt
title:  "Powrót do domu"
campaign: wizja-dukata
players: kić
gm: żółw
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [171205 - Dlaczego magitrownia działa? (temp)](171205-dlaczego-magitrownia-dziala.html)

### Chronologiczna

* [171205 - Dlaczego magitrownia działa? (temp)](171205-dlaczego-magitrownia-dziala.html)

## Kontekst ogólny sytuacji

### Siły główne

**Robert Sądeczny**

* Chce, by "Nowa Melodia" zaczęła płacić haracz Dukatowi. Sukces: dzieje się.
* Chce rozłożyć systemy detekcyjne na wszelkich terenach, łącznie z domem Pauliny. Sukces: stało się to.
* Chce, by Dracena zaczęła informować Dukata o różnych rzeczach jakie tam mają miejsce. Sukces: dzieje się.

**Dracena Diakon**

* Chce dostać się do pracy w "Nowej Melodii". Byłaby naprawdę dobrym DJem i mogłaby bardzo pomóc we wszystkich aspektach. Sukces: dostała się.
* Chce pomóc w rozkręceniu "Nowej Melodii" jako wolny klub. Sukces: "Nowa Melodia" działa i ma sukces finansowy, jest zareklamowana itp.
* Chce zorganizować warsztaty z majsterkowania i rozkręcić kulturę cybergoth w okolicy. Sukces: pojawia się mała grupka.

### Stan aktualny

brak

### Istotne miejsca

* Krukowo Czarne, gdzie koło gabinetu Pauliny znajduje się detektor i transmiter Sądecznego
* Mżawiór, gdzie znajduje się Portalisko Pustulskie

## Punkt zerowy:

## Misja właściwa:

**Dzień 1:**

Paulina, Dracena i Maria wprowadziły się do Gabinetu Pauliny w Krukowie Czarnym. Tak na serio to jest domek a nie gabinet. Parę dni Paulina ogarniała i opanowywała, odświeżenia znajomości, wysłanie wici... takie tam. W tym czasie Maria zaczęła integrować się z sieciami integracyjnymi a Dracena zaczęła szukać co i jak - i wzięła większość sprzątania na siebie. W końcu się najmniej męczy i jest najsilniejsza. Filigranowa cybergothka ;-).

Parę dni później Paulinę odwiedzili Hektor z Dianą. Wyglądają na szczęśliwych - Hektor pracuje dalej w Eliksirze Aerinus, Diana założyła małą firmę zajmującą się sprzątaniem. Między magią materii i zmysłów specjalnie się nie narobi, a zarobi. Paulina przedstawiła Dracenę jako przyjaciółkę. Diana skomplementowała strój; Dracena podziękowała. Robi nowy.

Hektor ostrzegł Paulinę, że niedaleko niej jest stacja przekaźnikowa Dukata. Zbiera dane z otoczenia i przesyła dalej. Nie będzie Paulinie bruździć, ale będzie monitorować. Paulinie się to zdecydowanie nie spodobało. Podczas sprawy z Karradraelem Dukat pilnował terenu łącznie z efemerydą; zrobiła się próżnia po tym, jak Bankierze się zaczęli unicestwiać. Grazoniusz i Prosperjusz zostali wyparci z tego terenu przez Apoloniusza. Apoloniusz sam ucierpiał na wojnie Bankierzy; dlatego Hektor tak szybko stał się kluczowym zasobem. A próżnię wypełnił Dukat. Teraz to się zacznie zmieniać; Świeca niedawno zainstalowała nowego Rezydenta. Podobno Mausofil, i to Bankierz. Powiedzieli jej też o Rezydencie Dukata - Robercie Sądecznym.

Paulina pojechała do maga rezydenta, się przywitać. Mag Rezydent Świecy przyjął Paulinę uprzejmie, jak rezydent byłą rezydentkę i pochwalił ją, że za czasów jej rezydentury wszystko poszło bardzo dobrze. Rezydent (Sylwester Bankierz) powiedział Paulinie, że jest poddany potężnej kampanii dezinformacyjnej. Nie wie, które wydarzenia z przeszłości są prawdą. Nie wie kto stoi po jakiej stronie. Na szczęście Paulina jest magiem który zna dane historyczne - ona może mu trochę rozwiązać sprawę. Dukat zrobił kawał świetnej roboty mieszając dane Sylwestrowi. W bibliotekach, w internecie, w pamięci ludzi...

Paulina powiedziała Sylwestrowi wszystko co wie o efemerydzie i danych historycznych, omijając temat Dukata. Sylwester zauważył na żądanie Pauliny, że nie zamierza krzywdzić ludzi - nie ma powodu i nie ma po co. Zapytał jednak o informacje o Dukacie i jego siłach. Paulina powiedziała to co pamięta omijając sprawy prywatne. Powiedziała, że nie walczyli ze sobą - współpracowali gdy cele mieli wspólne, ale raczej się omijali. Dukat omijał Paulinę, bo jest lekarzem a Paulina nie wchodziła mu w szkodę, bo nie miała sił. A czasem sobie pomagali przy okazji.

Sylwester wyrobił sobie opinię na temat Pauliny i Dukata. Uznał, że Paulina jest sensowną i pomocną czarodziejką, ale boi się konfliktu z Dukatem.

Paulina spróbowała się dowiedzieć czegoś odnośnie tego co ukrywa Dukat. (6+2v7->6 SS). Z tego Sylwester dowiedział się, że Dukat transportuje w określony sposób określonymi magami drogi sprzęt medyczny. I ten sprzęt jest możliwy do odzyskania przez Sylwestra. A Paulina zobaczyła, że Dukat maskuje... ogólnie większość rzeczy. Chce wprowadzić Sylwestra w permanentny chaos. Czemu? By on był jedyną siłą kontrolującą ten teren. Bankierze już się wycofali. A Dukat się tymi ludźmi mimo wszystko opiekuje.

Sylwester pięknie Paulinie podziękował.

Paulina zauważyła też, że zniknęły WSZYSTKIE informacje o jednej erupcji efemerydy. Paulina widziała je na systemach jakie jeszcze po Kai zostały w jej domu - przy samej efemerydzie senesgradzkiej - była tam jedna konkretna manifestacja efemerydy i to nietypowa. Nie badała tego dokładnie, ale to była dziwna manifestacja, anomalna. I tak naprawdę wiele informacji zostało stworzonych po to, by tą jedną ukryć.

Paulina wróciła do siebie. Ma wykresy i ma sensory. I wie na co patrzeć. Ostrzegła Marię o bardzo dziwnej erupcji efemerydy i poprosiła, by ta coś z tym zrobiła, coś się dowiedziała. Ostrzegła, że to Dukat i dlatego Maria musi bardzo, bardzo uważać...

Paulina rzuciła pojedyncze zaklęcie by się dowiedzieć, czy ktoś zareaguje. O dziwo, nie. Co więcej, Paulinie wyszło, że ta sieć transmisyjna nawet nie zauważyła zaklęcia Pauliny. Jest uszkodzona. Ktoś mu ją uszkodził. Kto? Przy okazji... Paulina nie kojarzy, by sensory, jakie ona ma w domu były aż tak rozbudowane. Ktoś nad tym pracował. Ktoś to przebudowywał. Jest szansa, że ktoś będzie chciał coś jeszcze z tym robić... ale jak często się ta osoba tu pojawia? I kto to?

Zapytana, Dracena potwierdziła - to jest rozbudowywane przez kogoś, kto się zna na rzeczy. Kogoś, kto znał oryginalny design sensorów i miał dostęp do dobrego jakościowo sprzętu. Myśli Pauliny od razu skierowały się ku Kai. Ale Kaja? Po takim czasie..?

Paulina zdecydowała się na najprostszą chyba opcję. Zapytać Kaję bezpośrednio po hipernecie.

* ...nie mam nic przeciwko temu, że ten dom był wykorzystywany jako baza, ale muszę wiedzieć co i kto tu robił... - Paulina do Kai
* Czarodziejko Rezydentko? Przepraszam bardzo, był autoresponder wcześniej - Kaja
* Nie zmienia to faktu, że takie jest moje pytanie - Paulina
* Też się cieszę, mogąc panią usłyszeć, czarodziejko rezydentko. - Kaja, lekko złośliwie

Kaja powiedziała, że w okolicach czasu Spustoszenia nie była w stanie ryzykować i wolała zapewnić odpowiedni zakres działań. Efemeryda jest zbyt ważna.

* Kto jeszcze o nim wie? - Paulina, zatroskana
* My dwie? Taki sekret dzielony między służką a rezydentką - Kaja
* Może sekret, ale nie "rezydentką". Obecnie na tym terenie rezydentem jest Sylwester Bankierz - Paulina
* Czarodziejko Tarczyńska... dla mnie jesteś jedyną rezydentką na tym terenie, w tej chwili - Kaja

Kaja obiecała, że przyjedzie i pokaże Paulinie dokładnie jak to czytać i jak tego używać. Paulina się zawahała; dary od Eweliny mają tendencje do bycia zatrute. Ale to..? Tego nie mogli przewidzieć. W czym Paulina zauważyła, że Kai bardzo, BARDZO zależy na tych sensorach. Zwłaszcza, że to faktycznie nie nadaje do Eweliny...

Paulina zdecydowała się na przeprowadzenie diagnozy i analizy tego wycinka danych o efemerydzie z sensorów. Zauważyła, że to wydarzenie miało miejsce miesiąc temu. Aspekty wykresu: (-2 (sensory), 3 (efemeryda), 3(wymaga dokładności), 3(reakcja rezonansowa), 3(mental+bio+kata)). Paulina zna tą efemerydę i ten teren (-1 karta +2 wynik), użyła właściwych ścieżek (destrukcja aspektu). 6v4->12 S. Paulina doszła do tego, co to jest. Efemeryda zareagowała rezonansem na zaklęcie. Na rozpaczliwą emisję energii magicznej. Na próbę odciśnięcia śladu. Mag aktywnie użył mocy, nie będąc tutaj, celując w tą efemerydę, nie mogąc jej adresować. To było wołanie SOS. Efemeryda mogłaby wybuchnąć...

Co więcej, analiza Paulina pokazała, że to echo magiczne to była próba pokazania czegoś. Ktoś próbował krzyczeć o pomoc, w czasie, w którym jakieś zaklęcie go nadpisywało. Sprawiało, że ten nieszczęsny mag wpadał pod czyjąś kontrolę...

Paulina opowiedziała Dracenie historię Kai. To nie jest zła dziewczyna, po prostu Ewelina ma nad nią kontrolę. Sama się oddała w niewolę. Dracenę to poruszyło; ma zamiar Coś Z Tym Zrobić. Delikatnie.

(8 kart)

**Dzień 2:**

Dracena jest gotowa na przybycie Kai. (6 +2 za makijaż i przygotowanie). Aż Paulinę oczy bolą jak na nią patrzy. I ten zapach pełny feromonów. Paulina wie jak to działa i się przygotowała i znieczuliła (wesoły poranek pomógł). I przybyła Kaja. Jest... zaskoczona obecnością Draceny.

* Czarodziejko rezydentko, nie spodziewałam się obecności tak szacownego gościa jak czarodziejka rodu - Kaja, z udawaną pokorą
* Nazywam się Dracena; rezydentna specjalistka od czujników i pięknych wykresów - Dracena, z udawaną (kiepsko) nieznajomością rzeczy

Konflikt: czy Dracena doprowadzi do tego, że Kaja będzie chciała wrócić (wywrzeć wrażenie): Kaja: obowiązkowa; zdesperowana by coś osiągnąć; przerażona Draceną i samą Pauliną. Paulina neutralizuje obowiązkowość - Kaja musi wrócić i tak. Sukces. Kaja jest pod ogromnym wrażeniem Draceny i samej Pauliny (z innych powodów). Będzie wracać i jest rozkojarzona; łatwiej z nią rozmawiać. (-1 karta).

Kaja opowiada o systemie i próbuje go wyjaśnić. Paulina pyta jej po co, a Dracena skutecznie rozprasza. Kaja, rozproszona, przyznała się: między brakiem rezydenta a nowym magiem Dukata Kaja martwiła się, że nic się nie będzie działo w ramach samej efemerydy. Że może dojść do tragedii. Ucierpi Senesgrad i ludzie. Oraz Diana. Więc Kaja przygotowała systemy detekcyjne. Ale wtedy mag Dukata postawił stacje. Kaja je z czasem pouszkadzała, ale nie mogła zapewnić transmisji, więc musiała wracać. Okazuje się, że mag Dukata jest specjalistą od komunikacji magicznej, sieci i transmisji. Więc Kaja nie odważyła się na użycie transmiterów. Paradoksalnie, zdaniem Kai, bezpieczniej użyć hipernetu niż komórek.

Kaja nie jest zwolenniczką tego maga Dukata. Jej zdaniem Robert Sądeczny jest inteligentnym specjalistą od komunikacji który buduje sieć skanującą i Organizację Dukata na tym terenie, ale jednocześnie ma tendencje do brania wszystkiego jako swoje. Bez dawania wyboru. Całkowicie NIE jak Ewelina. Dracena zauważyła Paulinie, że najpewniej mają maga, z którym trzeba prędzej czy później zrobić. Paulina jeszcze zauważyła, że Kaja nie wszystko mówi. Coś ukrywa, coś, co ją gryzie. 

Paulina korzystając z okazji nacisnęła Kaję - jak ona może jej pomóc? Co się dzieje? Kaja jest zdesperowana i pękła, zwłaszcza pod wpływem Draceny i przez to, że Paulina jest "nowa". Kaja powiedziała, że Ewelina ma kłopoty i to poważne. Newerja ma stare poglądy - kobieta nie może posiadać majątku. Dlatego Ewelina tak pozbywa się potencjalnych absztyfikantów. Ale teraz Newerja skupiła oczy na Sylwestrze Bankierzu... a dodatkowo Dukat poluje na Ewelinę by zmienić ją w swoją. (-1 karta)

Kaja chce, by Sylwester Bankierz w oczach Newerji stał się niegodny. Nieważne, czy jest godny - Kaja nic do niego nie ma. Ale nie może Newerja mieć czegoś do Sylwestra. Więcej, Ewelina ma założony kryształ, który zapewnia, że Newerja dowie się o każdym spisku Eweliny. Dlatego Kaja działa na własną rękę; dla Newerji los Eweliny nie jest szczególnie istotny. A Kaja dba o swoją panią - to jest lojalność jakiej nie da się kupić. Jeśli Ewelinie zabraknie surowców, wszystkie elementy i programy Eweliny się rozpadną, wiele ludzi straci korzyści... ogólnie, kiepsko. I to martwi Kaję.

Dracena nie wie w tym momencie co z tym robić. Jej zdaniem to bagno przekraczające cokolwiek. Bankierze na tym terenie...

Paulina nie dała Kai niczego. Jeszcze. Powiedziała, żeby ta wróciła wkrótce - powinna mieć dla Kai jakieś wyniki. Kaja podziękowała, przekazała instrukcję itp Paulinie, powiedziała, że jakby był problem z efemerydą to niech da znać i wróciła do swojej pani.

Dracena spytała Pauliny jak ta była w stanie tu wytrzymać. To jest bagno jak rzadko. Paulina powiedziała, że bez zrozumienia Dukata nie da się zrobić niczego na tym obszarze...

Paulina chce się spotkać z Dukatem. Zdecydowała się pojechać z nim spotkać. Gdy już była w Orłostwie, skomunikowała się przez hipernet z Czykomarem, że jedzie do Dukata i żeby nie strzelali. O dziwo, Dukat nie kazał jej czekać. Ucieszył się ją widząc i spytał o stan Marii. Odpowiedziała, że wszystko działa idealnie. Znowu się ucieszył. Spytał, czy Paulina zdecydowała się osiąść na jego terenie. Paulina odpowiedziała, że wróciła na współdzielony teren, do domu. Paulina spytała Dukata o stan jego dziecka. Dukat odpowiedział ze smutkiem, że bez zmian. Nadal nic nie pomaga.

Paulina poprosiła Dukata o to, czy ten nie mógłby zniszczyć wież komunikacyjnych na terenie jej domu. Dukat się zgodził - też chce chronić Marię. Paulina słysząc, że Dukat eksperymentował z medycznym Spustoszeniem ale nie dało się zachować działającej osobowości była zmrożona. Dracena jest zagrożona! Paulina natychmiast wkręciła Dukata tak, by usłyszał o Dracenie ale nie usłyszał tego co najważniejsze - powiedziała, że ma pacjentkę która jest jej kochanką i to UWIELBIA. Diakonkę ;-). (-3 karty, sukces). Dukat pogratulował Paulinie i całkowicie pominął Dracenę. Powiedział, że Paulina dostanie od niego glejt dla Draceny i dla Marii. Żeby ktoś pochopny z jego organizacji nie pomyślał czegoś głupiego. Jak długo Paulina (i one) nie występują przeciw niemu, są bezpieczne. Po prostu trzymaj się zasad.

Paulina podziękowała i odjechała. Dukat życzył jej pięknej Diakonki na odchodne.

Paulina wróciła do domu. I nic nie było już takie jak kiedyś...

(7 kart)

## Wpływ na świat:

JAK:

* 1: dodanie faktu lub okoliczności wobec 'neutral' lub 'claim'
* 1: counter działania MG z sensowną propozycją alternatywną (wobec CLAIM)
* 2: postawienie CLAIM.
* 3: przesunięcie zgodne z motywacją
* 5: przesunięcie niezgodne/sprzeczne z motywacją
* 5: przesunięcie wobec postaci z CLAIM wbrew graczowi

MG: 15 kart

* Dracena: dostaje pracę w "Nowej Melodii" (3)
* Maria: dowie się o złych rzeczach jakie robiły siły Dukata (3)
* Robert: dostaje haracz od "Nowej Melodii" (3)
* Sylwester: prowadzi udaną akcję zdobycia transportu Dukata (3)
* To Hektor pomaga w budowaniu eliksirów dla Katii (3)

Gracze (x2 za konflikt +1/5 kart MG): 7

* Kić: CLAIM "Trudna droga Sylwestra do zrozumienia terenu" (2)
* Kić: fakt "Sylwester został zesłany tu przez Newerję" (1)
* Kić: COUNTER Dukat: "Dukat nie ma gwałtownej reakcji na działania Sylwestra; da się wychować. Tupnął nogą bo musiał - and now we talk" (1)
* Kić: CLAIM "Newerja Bankierz x Gabriel Dukat - zostaną parą" (2)
* Kić: fakt "docelowo Ewelina dostanie jakieś środki do życia; nie będzie żebraczką" (1)

# Progresja

* Dracena Diakon: zdobyła pracę w Nowej Melodii jako DJ i marketingowiec

# Streszczenie

Paulina wróciła do Krukowa, miejsca, gdzie kiedyś była rezydentką z Marią (zarejestrowaną) i Draceną (też zarejestrowaną). Ku swemu zdziwieniu zobaczyła wielką wojnę dezinformacyjną z kierunku Dukata w aktualnego rezydenta. Rozpoznała trochę sytuację i nawiązała kontakt z Kają, która z braku Pauliny monitorowała z jej domu efemerydę. Sytuacja się zmieniła - Dukat stał się główną siłą w okolicy, Ewelina się wycofała, Apoloniusz ucierpiał, otwarto portalisko i do tego Paulina wykryła coś... niepokojącego, stary sygnał w efemerydzie, czyjeś wołanie o pomoc... aha, aktualny rezydent odbił od Dukata jedną maszynę medyczną. Super.

# Zasługi

* mag: Paulina Tarczyńska, wróciła jako nie-rezydentka by zacząć knuć jakby nią dalej była. Próbuje pomóc nowemu rezydentowi i Kai a jednocześnie uwolniła się od monitoringu Dukata.
* mag: Dracena Diakon, która posłużyła jako deviator dla Kai; dodatkowo, aplikowała o pracę w Nowej Melodii i ją dostała.
* czł: Maria Newa, która szybko rozpłynęła się w strukturach lokalnych powiatu Pustulskiego by zacząć się orientować w sytuacji i stać się pulsem okolicy.
* mag: Hektor Reszniaczek, który znalazł dobrą pracę w Eliksirze Aerinus i osiadł z Dianą. Pilnował domu Pauliny i starał się by wszystko się udało na czas jej nieobecności. Udało się.
* mag: Sylwester Bankierz, mag rezydent Świecy który jest tak zdezinformowany przez Sądecznego, że aż nie wie jak wyglądają fakty w okolicy. Zdobył je od Pauliny. Zaatakował i zdobył od Dukata maszynę medyczną w tranzycie.
* mag: Kaja Maślaczek, znowu ma pecha do Pauliny; gdy Pauliny nie było, zmontowała w jej domu zaawansowany system monitorowania efemerydy - by upewnić się, że nikt nie ucierpi przez brak rezydenta.
* mag: Gabriel Dukat, uprzejmy jak zawsze. Skupiony bardziej na swoim dziecku niż zwykle i z nieco mniejszym zaangażowaniem skupia się na mafii. Paulinie wypisał glejt i kazał rozmontować jej stację śledzącą.
* mag: Robert Sądeczny, rezydent Dukata na teren powiatu Pustulskiego. Przeprowadził potężną kampanię dezinformacyjną przeciw Sylwestrowi Bankierzowi i rozbudował sieci detekcji magii na swoim terenie.
* czł: Andrzej Toporek, właściciel "Nowej Melodii"; przyjął Dracenę do pracy z nadzieją dodatkowych profitów.
* mag: Katia Grajek, czarodziejka bardzo kompetentna w specjalności eliksirów, lekko zakręcona, dość młoda. Nienawidzi partactwa. Dawno temu wysłała sygnał SOS którą przechwyciła efemeryda...

# Plany

* Paulina Tarczyńska: wróciła na teren powiatu Pustulskiego, by po kolei i stopniowo walczyć o bycie rezydentką.
* Kaja Maślaczek: konsekwentnie monitoruje efemerydę senesgradzką i upewnia isę, że nikt nie cierpi przez brak aktywnego sensownego rezydenta
* Dracena Diakon: próbuje się jakoś ustatkować i znaleźć gdzieś pracę. Próbuje się zreintegrować do społeczeństwa, acz w swojej nowej formie Spustoszonej Wiły
* Maria Newa: próbuje odzyskać stare kontakty i odbudować swoje sieci połączeń w powiecie Pustulskim. Chce odzyskać status pulsu okolicy.
* Hektor Reszniaczek: przestał pilnować domu Pauliny; nie ma już takiej konieczności (Paulina wróciła).
* Sylwester Bankierz: próbuje uzyskać jakąkolwiek informację o tym co dzieje się w okolicy i przebić się przez zasłonę dezinformacji. Chce być sensownym rezydentem.
* Gabriel Dukat: skupia się bardziej na swoim dziecku niż na Rodzinie Dukata; zdecydowanie zdepriorytetyzował to, co dzieje się z jego mafią.
* Robert Sądeczny: dezinformuje, utrzymuje przewagę magitechnologiczną, wprowadza chaos i zdobywa pieniądze dla Dukata.

# Lokalizacje

1. Świat
    1. Primus
        1. Mazowsze
            1. Powiat Pustulski
                1. Krukowo Czarne
                    1. Gabinet Pauliny, do którego Paulina wróciła i który ma zaawansowane sensory do wykrywania stanu efemerydy
                1. Trzeszcz
                    1. Klub Nowa Melodia, w którym Dracenie udało się znaleźć pracę jako DJ i marketingowiec, ku jej wielkiej radości
                1. Mżawiór, niewielka wieś z portaliskiem
                    1. Portalisko Pustulskie, zarządzane przez Bolesława Mausa
                1. Byczokrowie, wieś, w której bazę zbudował mag rezydent Świecy Sylwester Bankierz
                    1. Niezniszczalna Kazamata, w którym (i pod którym) Sylwester Bankierz zbudował centralę dowodzenia Srebrnej Świecy na region
                1. Orłostwo Wielkie
                    1. Centrum
                        1. Apartamenty Dukata, gdzie Dukat przyjął z uprzejmością nie zapowiedzianą wcześniej Paulinę
            1. Powiat Głuszców
                1. Upiorzec
                    1. Eliksir Aerinus, w którym - jak się okazało - pracuje Hektor
                    1. Dzielnica Wichrów, gdzie mieszka Hektor z Dianą w jednym z bloków
                    1. Pałacyk Gryfa, w którym mieszka w luksusach Robert Sądeczny

# Czas

* Opóźnienie: 250
* Dni: 2

# Strony

* **Robert Sądeczny**: Wygra, jeśli "Nowa Melodia" zapłaci haracz i odda się w opiekę Dukatowi
* **Dracena Diakon**: Wygra, jeśli Sądeczny odpierniczy się od "Nowej Melodii" i odejdzie ze skulonym ogonem
* **Maria Newa**: Wygra, jeśli w wyniku zabawy magów żaden człowiek nie ucierpi

# Narzędzia MG

## Opis celu misji

-

## Cel misji

-

## Po czym poznam sukces

-

## Wynik z perspektywy celu

-

## Wykorzystana mechanika

Postać (strona gracza):

| .Motywacja. | .Umiejętności. | .Magia. | .SiłySłabości. | .Znam.    | .Mam.      |.POSTAĆ. |
|   (-2) - 2  |     0 - 2      |  0 - 2  |    (-2) - 2    | Z+M = 0-6 | Z+M = 0-6  |         |

Opozycja (strona NPC):

* Baza: 2
* Aspekt: 3/aspekt, wariant z nożyczki/papier/kamień
* Modyfikatory: Skala, Sprzęt, Sytuacja (1-3 każdy)

Wynik:

| .Postać. | .Opozycja. | .Rzut.    | .Wynik.  |
|          |            | xx: +x Wx |   S-SS-F |

Wpływ:

* 10 kart / dzień
* każda porażka i skonfliktowany sukces = +2 pkt wpływu dla gracza
* każda karta = +1 wpływ dla MG
* każde 5 kart zaokr. w górę: +1 pkt wpływu dla gracza

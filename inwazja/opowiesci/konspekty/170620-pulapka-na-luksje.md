---
layout: inwazja-konspekt
title:  "Pułapka na Luksję"
campaign: corka-lucyfera
players: kić, raynor
gm: żółw
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [170603 - Córka jest narzędziem? (HS, Kić(temp))](170603-corka-jest-narzedziem.html)

### Chronologiczna

* [170603 - Córka jest narzędziem? (HS, Kić(temp))](170603-corka-jest-narzedziem.html)

## Koncept wysokopoziomowy misji

-

## Kontekst ogólny sytuacji

W okolicy pojawił się "biegający wirtualny ksiądz Siwiecki". Niestety, Luksji udało się sformować potężny kult satanistyczny, na odpowiednim poziomie jaki jest jej potrzebny. Już widoczna część potrzebnej Luksji populacji zmieniona została w thralle. 

Poziom samowoli i anarchii w mieście się ostro wzmocnił; pomiędzy satanistami, thrallami, Luksją i dziwnymi rzeczami które się dzieją jest poczucie, że nikt nie kontroluje sytuacji. Siły satanistyczne okrzepły i są potężną siłą w okolicy. Postępy Luksji się nieco zatrzymały między wszystkimi innymi rzeczami, które czarodziejka próbuje zrobić.

Luksja WIE, że Siwiecki jest magiem. Nie wie jakim i gdzie jest. Było ich dwóch. Ma jednak demona do pomocy.

Tymczasem do akcji wkraczają dwie dodatkowe siły - Lucyfer oraz przedstawiciel władz, bardzo próbujący się wykazać...

Ksiądz Zenobi Klepiczek jest zabójczo zakochany i zafascynowany Estrellą.

## Punkt zerowy:

**Apokalipci Lucyfera**

* Konwersja satanistyczna:              5/5:                 MAX
* Transformacja w thralle:              3/6:                 Tier 1
* Przygotowanie artefaktu:              1/4:                 
* Zdobycie władzy politycznej:          1/6                  
* Uruchomienie Artefaktu:               0/10                 

**"Niosący Światło"**

* **Schowanie linka**:                  0/8:                
* **Pozyskanie demona**:                0/4:                
* **Fałszywe ślady (demon)**:           0/6:                
* **Usunięcie śladów o sobie**:         0/8:                
* **Znalezienie Biegusia**:             0/8:                

**Dyktatura Barona Czosnku**

* Brutalna siła porządkowa:             0/8:                
* Władza polityczna w mieście:          0/8:                
* Pokój przez tyranię:                  0/8:                
* Jest jeden władca wszystkiego         0/8:                
* Oczyszczenie Iliusitiusa              0/10:               

**Neutralna**

* Skażenie magią:                       2/12                
* Anarchia w mieście:                   6/12                Tier 2

## Misja właściwa:

**Dzień 1:**

+1 akcja (init):

* Apokalipci: +1 władza polityczna
* Niosący Światło: +1 usunięcie śladów
* Baron: +1 brutalna siła
* Neutralni: +1 anarchia

Estrellę i Henryka rano obudziły wiadomości (plotki) od okolicznych parafian. Radny Paweł Parobek zaczął rozmawiać z satanistami... więc jego syna pobili "rycerze kościoła". Że co? No, ale zadziałało. No i Balbina Wróblewska dała namiar. Dżony Słomian. On coś ostatnio pił i on stoi za "rycerzami kościoła" - dostał sprzęt i nienawidzi szatanizmu.

Henryk poszedł do Dżonego do baru Gulasz. Dżony powiedział, że przyszedł do niego w nocy ksiądz i dał mu sprzęt i kazał bić. To taki dresik jest, przyciężkawy nieco. Henryk kazał mu przestać bić i kazał werbować dodatkowych ludzi. Będą mu potrzebni. I przy powoływaniu nowych członków: pod sławą egzorcysty znanego w Polsce.

Henryk próbuje pomóc w rekrutacji. Swoją aurą egzorcysty. Chce mieć zapas ludzi i choć troszkę przesunąć pryzmat... (-1 surowiec). S(+6). Duża skala.    +1 akcja

Henryk wezwał kleryków do pomocy (-1 surowiec). Przydadzą mu się. Estrella wezwała sprzęt antydemoniczny ze Świecy (-1 surowiec).

Nadszedł czas nocy... +4 akcje

**Dzień 2**

Noc. Henryk spokojnie śpi. Toteż Estrella. Oboje się obudzili słysząc, że ktoś zakrada się do pokoju Henryka. Henryk uformował szybko kołdrę jakby spał i się przesunął do kąta. Wpadło dwóch SWATowców. Henryk z boku rzucił czar skanujący. Efekt Skażenia. Anielski chór "czego chcecie" na cały kościół i pół miasta. SWATowcy zaskoczeni, Henryk wie, że za nimi stoi niejaki Bieguś (efekt czaru). SWATowcy zaskoczeni. Cała akcja straszenia Henryka psu w zad. Ale postraszyli (grzecznie) i sobie poszli...     +1 akcja

**Przeliczenie torów**

Akcje: 6

**Apokalipci Lucyfera**: 4*2, 3, 4

* Konwersja satanistyczna:              5/5:                 MAX
* Transformacja w thralle:              7/8:                 Tier 3
* Przygotowanie artefaktu:              2/4:                 Tier 1
* Zdobycie władzy politycznej:          3/6                  Tier 1
* Uruchomienie Artefaktu:               0/10                 

**"Niosący Światło"**: 2*1, 2*2, 4, 5

* **Schowanie linka**:                  2/8:                Tier 1
* **Pozyskanie demona**:                2/4:                Tier 1
* **Fałszywe ślady (demon)**:           0/6:                
* **Usunięcie śladów o sobie**:         2/8:                
* **Znalezienie Biegusia**:             1/8:                

**Dyktatura Barona Czosnku**: 2*1, 2*2, 3, 4

* Brutalna siła porządkowa:             3/8:                Tier 1
* Władza polityczna w mieście:          2/8:                Tier 1
* Pokój przez tyranię:                  1/8:                
* Jest jeden władca wszystkiego         1/8:                
* Oczyszczenie Iliusitiusa              0/10:               

**Neutralna**

* Skażenie magią:                       2/12                Tier 1
* Anarchia w mieście:                   7/12                Tier 2

**Zespół**

* Pryzmat antysatanizmu                 2/6                 Tier 1
* Przynęta na Luksję                    2/6                 Tier 1


Henryk siada i przygotowuje potężny Pryzmat w kościele. Pryzmat religijny, kościelny, antydemoniczny. Z Klepiczkiem ponownie pobłogosławili kościół. Walą w dzwony "Bóg się odezwał, odmawiać zdrowaśki!"

Henryk: Cudowne Objawienie (1), Pomysł (2), Dużo Rycerzy Kościoła (2), Kościół (1) = 4+6
Satapryzmat: Obezwładniająca moc (3), Thralle (1), Skażenie (1) + Anarchia (2) = 2+7 = 9

10v9->R +1 akcja

KF: 8: Szerokie zainteresowanie. To miejsce stanie się obszarem pielgrzymek ZARÓWNO satanistów JAK I sekciarzy i religijnych.   +1 akcja

* Antysatanizm +2
* Przynęta +2

Pryzmat się udało przesunąć. Kościół ma Pryzmat... właściwy dla kościoła. Z poczuciem dobrze wykonanego obowiązku wszyscy poszli spać.

Rano.

Przybyli klerycy oraz sprzęt Estrelli.

Estrella zamaskowała Henryka jako losowego radnego, by ten mógł bezpiecznie wyjść z kościoła. No i Henryk poszedł do Kantosza. Po drodze dorwało go dwóch takich drabów, wykręciło ręce i żąda kasy od radnego. Henryk poprosił o uwolnienie ręki by móc iść do bankomatu.

* KF: Zmiana zdania innej strony: Dżony Słomian wrócił do Biegusia.   +1 akcja

Henryk rzucił szybki czar "love love świata poza sobą nie widzicie". 4v2-> ES. 

* ES: Ten sam czar, inny cel: Klepiczek na Estrellę.

Henryk poszedł dalej. Ma w końcu inny cel. U Kantosza. Lokaj wpuścił Henryka (w końcu przebrany jako radny). Henryk walnął mu zaklęciem; czar ten miał za zadanie sprawić, by Kantosz spiskował przeciw Luksji i że Henryk nieudolnie maskował to.          +1 akcja

* Przynęta +2

Luksja przyjdzie do kościoła... a da się ściągnąć Luksję na plebanię.

Estrella w tym czasie zaminowała teren lis'ha'orami. 6v5->S.

DO ESTRELLI PRZYSZEDŁ KLEPICZEK! Zaczarowany przez Siwieckiego (ES) rzuca się na Estrellę, która mu skutecznie umyka. Interweniują klerycy. Klepiczek odprowadzony, żałośnie krzyczący za Estrellą... a Estrella poczuła od Klepiczka echo Skażenia Siwieckiego. Cóż, Klepiczek pocierpi. Estrella nie ma teraz surowców :-(.

Henryk wrócił. Bezpiecznie. Klepiczek związany. Henryk próbuje go odkazić...

* Klepiczek: Skażony, Skrzywdzony przez Luksję, PONOWNIE Skażony: 5
* Henryk: Pryzmat, 2. -> 3

Nie. Henryk odpuszcza Klepiczka. Przydałyby mu się przedmioty Estrelii, ale... cóż, Luksja ;-).

Henryk wysłał jeszcze wiadomość do "swojej" armii kościoła. Odmówili posłuszeństwa - Bieguś ich odzyskał... Henryk zbudował armię Biegusiowi.

Pojawiła się Luksja z demonem, nawołując "księżulku". Klerycy zostali wysłani, by zacząć odprawiać rytuał i się modlić.

Estrella i Henryk przygotowali się. Kombinowana akcja przeciw demonowi. Z zaskoczenia. +1 akcja

Zespół: Zaskoczenie, Klerycy, Lis'ha'or, sprzęt antydemoniczny, sprzęt egzorcysty, Pryzmat, Wsparcie drugiego maga (2) = +8+4 = 12
Demon: Bardzo Wojowniczy (2), Nieludzki i ludzki, Bardzo Krwawy (2), Bardzo stabilny (2), dopakowanie Luksji = +8 = 10

12v10->F,R

* KF: 20: Wydarzenie na skalę szerszą: +1 akcja
* KF: x: Wzmocnienie Luksji: +1 akcja

Demon spłonął. Luksja zintegrowała się z satanistycznym pryzmatem i strzeliła straszliwą falą psinoską w Estrellę i Henryka.

* Luksja: Krwawa, Zewnętrzna magia (2), 2 = 5
* Henryk: 4v5-> F,F,S
* Estrella: 2v5-> F,F,S

Gdy psinoska fala przeszła... 

* Życie (lis'ha'or, trawa, kwiaty...) umarło, klerycy krzyczą z bólu itp.
* Teren został Skażony i Splugawiony
* Henryk i Estrella są Ranni
* Luksja przekierowała energię w Krwawe Osłony

Henryk zdecydował się wpłynąć na energię Luksji. Zewnętrzną, mroczną energię wyegzorcyzmować kościołem.

* Luksja: Krwawa aura (2), Zewnętrzna magia (2), Zasilana cierpieniem (1) = 7
* Henryk: sprzęt egzorcysty, Pryzmat +5 = 7

7v7-> S, ES: Rezonans z czarami: Egzorcyzm USZKODZIŁ poważnie teren. Kościół, chodnik, rośliny, części ubrania Estrelli i sutanny Henryka...

Estrella zdecydowała się przekształcić istniejące resztki lis'ha'or w zasilanie Krwią używając odpowiednich roślin żywiących się Krwią. De facto, puryfikujących Krew.

* Luksja: Krwawe osłony (3), Zasilana cierpieniem (1), Energia anielska (-1) = 5
* Estrella: Lis'ha'or, nasiona, 5 = 7

7v5 -> S, ES: 14: Zadziwiające zaklęcie. Lis'ha'ory przekształciły się w zintegrowaną istotę i zasymilowały całość MK, po czym się oddaliły. Znaczy, odleciały. Plantoptak.

+1 akcja

Luksja jest zaskoczona. Próbuje jeszcze raz użyć psinoskiej fali. Bez wspomagania krwi. Bez wspomagania dalszego. Estrella zdecydowała się strzelić do Luksji, korzystając z okazji, że główne tarcze padły.

* Luksja: standardowe osłony, 2: 3
* Estrella: pistolet, 3: 4

4v3: Estrella przestrzeliła przez osłonę i zraniła Luksję. Osłony się rozproszyły, czarodziejka krzyknęła z bólu.

* Luksja: Rozproszona (-1), Bardzo Zniewolona Przez Lucyfera (2) = 3
* Henryk: Przewaga magiczna (1), 3 = 4

4v3-> R     +1 akcja

* KF: 4: Kontr-akcja przeciwnika: Lucyfer zdąży pozamiatać: +1 akcja LUCYFERA

Luksja padła, całkowicie zdominowana przez Henryka.

Estrella rzuca bardzo potężne zaklęcie Zmysłów, by do końca dnia wszystko wyglądało jak zawsze... 8v6->R        +1 akcja

* KF: 20: wydarzenie na dużą skalę. Zbliża się magiczna burza...    +1 akcja

**Przeliczenie torów**

Akcje: 11 (L:12)

**Apokalipci Lucyfera**: 

* Konwersja satanistyczna:              5/5:                MAX
* Transformacja w thralle:              8/8:                MAX
* Przygotowanie artefaktu:              4/4:                MAX
* Zdobycie władzy politycznej:          6/6                 MAX
* Uruchomienie Artefaktu:               5/10                Tier 2

**"Niosący Światło"**:

* **Schowanie linka**:                  6/8:                Tier 3
* **Pozyskanie demona**:                4/4:                MAX
* **Fałszywe ślady (demon)**:           1/6:                
* **Usunięcie śladów o sobie**:         5/8:                Tier 2
* **Znalezienie Biegusia**:             3/8:                Tier 1

**Dyktatura Barona Czosnku**: 

* Brutalna siła porządkowa:             6/8:                Tier 3
* Władza polityczna w mieście:          4/8:                Tier 2
* Pokój przez tyranię:                  3/8:                Tier 2
* Jest jeden władca wszystkiego         3/8:                Tier 1
* Oczyszczenie Iliusitiusa              2/10:               Tier 1

**Neutralna**

* Skażenie magią:                       2/12                Tier 1
* Anarchia w mieście:                   7/12                Tier 2

**Zespół**

* Pryzmat antysatanizmu                 4/6                 Tier 1
* Przynęta na Luksję                    6/6                 MAX

**Interpretacja torów:**

Apokalipci są na dobrej drodze do doprowadzenia do apokalipsy psinoskiej - artefakt już się uruchomił. Niosący Światło nie dał rady podłożyć śladów na "demona", jakkolwiek go pozyskał i podłożył do Storczyna. Z pewnością jednak trudno mu cokolwiek udowodnić. Tymczasem Antoni Bieguś zaczął zaprowadzać porządek w mieście brutalną siłą i prawie spacyfikował okolicę; ścina się z satanistami o władzę.

**Komplikacje Fabularne i Skażenie**:

* KF: 8: Szerokie zainteresowanie. To miejsce stanie się obszarem pielgrzymek ZARÓWNO satanistów JAK I sekciarzy i religijnych.
* Henryk i Estrella są Ranni
* Zenobi Klepiczek jest bardzo całuśny wobec Estrelli
* Okolice kościoła i plebanii są wyniszczone i uszkodzone
* Lis'ha'ory przekształciły się w zintegrowaną istotę ptakopodobną, żywiącą się Magią Krwi. Odleciało toto.
* Luksja jest pod silną kontrolą Henryka
* Zbliża się magiczna burza.

**Ciekawe obserwacje MG**

Tory kaskadowały przez wybór przeciwnika. Wpierw demon, potem Luksja. Strategia wygrywająca to było: zdjęcie Luksji przez Henryka z zaskoczenia (Luksja: Krwawa(2) + Osłony(2) = '6'), Estrella buduje iluzje przesuwając demonowi pole detekcji, by nie mógł znaleźć Henryka ani jej (Bardzo Wojowniczy (2), Dopakowanie Luksji (1), Krwawy (2) = '7'). Oboje korzystaliby z ataku z Zaskoczenia (+1), odpowiedniego Sprzętu (+1). Estrella dodatkowo z Lis'ha'or (+1). Efektywnie, walczyliby baza vs 4. Zakładając brak innych aspektów. A potem, w przyszłej turze, Henryk mógłby wsparty przez Estrellę zniszczyć demona jak teraz.

Gdyby wybrali tą ścieżkę, oszczędziliby 5 akcji. To sprawiłoby, że zamiast 11 akcji inne strony miałyby tylko 6 akcji. Cała misja zamknęłaby się w 12 akcjach ze strony przeciwników - a po "naszej" stronie mieliby 3 magów, w czym Luksję.

Aspekty zadziałały. Gracze potrafią budować aspekty ofensywnie. Nie potrafią ich budować defensywnie.

Zrobiłem błąd dodając +4 akcje rotując dzień. Były wcześniej 2 akcje. Powinienem dodać +3 akcje (dopełnić do 5), nie +4.

Danie bonusu za wsparcie w formie +2 działa lepiej niż +1, zwłaszcza, jeśli w koszcie jest akcja.

# Progresja

* Luksja Pandemoniae: zdominowana przez Henryka Siwieckiego

# Streszczenie

Henryk i Estrella zwabili Luksję do kościoła i skutecznie ją pokonali i zdominowali, poważnie uszkadzając plebanię. "Niosący Światło" w panice zaczął ukrywać dowody swojego zaangażowania a Antoni Bieguś zaczął przejmować w sposób mafijny kontrolę w okolicy. Apokalipci Lucyfera rozpoczęli proces uruchamiania artefaktu (zrobiła to jeszcze Luksja). Tymczasem zbliża się potężna magiczna burza...

# Zasługi

* mag: Henryk Siwiecki, zbudował armię kościoła (którą stracił na rzecz Biegusia), zanęcił Luksję do przyjścia na plebanię po czym rozwalił demona i zdominował samą Luksję. Ranny.
* mag: Estrella Diakon, zastawiła pułapkę, wciągnęła Luksję, rozmontowała jej Krwawe Osłony, po czym postrzeliła ją blokując dalsze działania. Ranna.
* czł: Balbina Wróblewska, plotkarka, która z radością powie na plebanii wszystko. Wie mniej więcej co kto i gdzie w Storczynie Mniejszym.
* czł: Paweł Parobek, radny; z przyczyn politycznych zaczął kombinować z satanistami. Siły Biegusia pobiły jego syna, więc przestał - zrozumiał przesłanie.
* czł: Dżony Słomian, 'rycerz kościoła'; pomniejszy dresik, wrobiony przez siły Biegusia w tłuczenie satanistów. Henryk go zainspirował do budowania armii... po czym Bieguś go odzyskał.
* czł: Henryk Kantosz, jeden z wyznawców Luksji, którego Henryk magią mentalną wrobił w bunt przeciw Luksji by ją rozwścieczyć i skłonić do wpadnięcia w pułapkę. Z powodzeniem.
* czł: Zenobi Klepiczek, którego dotknęło Skażenie Henryka i który rzucił się całować Estrellę. Skończył związany. Magowie nie mają sił go odkażać przed walką z Luksją...
* vic: Antoni Bieguś, protomag i radny, który buduje siły przeciwko satanistom. Polega raczej na paramilitarnym oddziale i brutalnej sile niż subtelnościach ;-).
* mag: Luksja Pandemoniae, zaczęła potężny pojedynek magiczny z Henrykiem i Estrellą, który przegrała. Skończyła jako zdominowana lalka Henryka Siwieckiego. Ranna.

# Lokalizacje

1. Świat
    1. Primus
        1. Lubuskie
            1. Powiat Szarczawy
                1. Storczyn Mniejszy
                    1. Fabryczny
                        1. Bar Gulasz, w którym raczej nie jadają bogaci ludzie. Tanie, Popularne miejsce.
                    1. Luksusowy
                        1. Posesja Kantosza, gdzie Siwiecki zdominował właściciela, by rozjuszyć Luksję. Luksusowa, Trudno Dostępna, Zabezpieczona.
                    1. Duchowy
                        1. Plebania, gdzie doszło do straszliwej bitwy Luksji z Henrykiem i Estrellą. Poniszczona, Anielski Pryzmat.

# Czas

* Dni: 2

# Frakcje:


## Apokalipci Lucyfera

### Koncept na tej misji:

Siły wspierające pierwotny koncept Luksji i Lucyfera, dążące do końca świata i wariantu Psinoskiego.

### Scena zwycięstwa:

* Miasto spowija krwawa poświata; dochodzi do rytuału transcendencji.
* Luksja LUB Bieguś absorbują energię miasta. Ktoś staje się efektywnie potężnym czarodziejem lub bytem Krwi.
* Sataniści sprowadzą koszmar na ziemię

### Motywacje:

* "Lucyfer będzie zadowolony. Jego wola stanie się prawdą. Chwała Krwi!"
* "Jesteś z nami lub przeciw nam. Chwała Niosącemu Światło."

### Siły:

* Luksja Pandemoniae, czarodziejka: mentalistka, biomantka, infomantka, katalistka, Krew.
* Demon bojowy, Bardzo Wojowniczy, Nieludzki, Krwawy, Posłuszny Luksji, Zindoktrynowany.
* Łukasz Tworzyw, najwyższy kapłan-satanista, w służbie Luksji... już nie do końca.
* Sataniści Łukasza, duża grupa lojalnych satanistów.
* Thralle Luksji, Zmienieni w odpowiedniki Krwawców, ale przekaźniki energetyczne.

### Wymagane kroki:

* Skonwertowanie odpowiedniej ilości ludzi na satanizm: 5
* Transformacja indukcyjna odpowiedniej ilości ludzi w thralle: 6
* Zdobycie władzy politycznej: 6
* Przygotowanie Artefaktu Psinoskiego: 4
* Uruchomienie Artefaktu Psinoskiego: 5

### Ścieżki:

* **Konwersja satanistyczna**:      5/5:                 MAX
* **Transformacja w thralle**:      3/8:                 Tier 1
* **Przygotowanie artefaktu**:      1/4:                 
* **Zdobycie władzy politycznej**:  1/6                  
* **Uruchomienie Artefaktu**:       0/10                 


## "Niosący Światło"

### Koncept na tej misji:

Ustatkowany mag, pragnący zdobyć większą władzę i silnie zmieszany pojawieniem się faktycznych terminusów w okolicy.

### Scena zwycięstwa:

* Luksja lub inny mag wchłania energię psinoską i wpada pod jego kontrolę; rozerwana ochrona rodziców
* Luksja jest niemożliwa do powiązania z samym magiem
* Osłabienie jakichkolwiek przesadzonych efektów tego, co się tu stało; zatarcie śladów

### Motywacje:

* "Co tu się dzieje - przesadzone, przesadzone!"
* "Przecież nikt nie może mnie powiązać z tym co tu się dzieje!"
* "Nareszcie, potęga i władza. Cudzymi rękami."

### Siły:

* Ustatkowany mag. Materia + Mentalne + Infomancja
* Golemiczni Słudzy.

### Wymagane kroki:

* Schowanie linka pomiędzy Luksją a jej rodzicami
* Pozyskanie potężnego demona "z rynku"
* Zrzucenie winy na potężnego demona; fałszywe ślady
* Usunięcie danych o Lucyferze spośród miasta
* Znalezienie innych magów
* Dominacja innych magów

### Ścieżki:

* **Schowanie linka**:                  0/8: 
* **Pozyskanie demona**:                0/4: 
* **Fałszywe ślady (demon)**:           0/6: 
* **Usunięcie śladów o sobie**:         0/8: 
* **Znalezienie Biegusia**:             0/8: 


## Dyktatura Barona Czosnku

### Koncept na tej misji:

Dowodzona przez lokalnego barona czosnkowego grupa samopomocowa i ochronna próbująca zapewnić bezpieczeństwo i stabilną sytuację. Próbują zająć kluczowe tereny i opanować sytuację. A Bieguś ma tajną broń - ołtarz Iliusitiusa.

### Scena zwycięstwa:

* Bieguś zdobył pełną władzę polityczną w mieście; w kryzysie okazał się kompetentny i zorganizował wokół siebie pewne siły.
* Bieguś dał radę zniewolić Luksję i innych magów.
* Brutalna siła pacyfikuje satanistów i thralli; ludzie rozumieją ideę.
* Bieguś przejmuje kontrolę nad miastem.

### Motywacje:

* "To moje miasto i będę je chronił tak, jak jestem w stanie"
* "Dzieje się tu coś dziwnego. Ale ja też mogę z tym poradzić"
* "Pokój przez tyranię."

### Siły:

* Antoni Bieguś, lokalny baron czosnkowy. Protomag.
* Ołtarz Iliusitiusa, niszczący zjawiska paranormalne.
* Agencja ochrony Termos, dość brutalna.
* Policja, z radością stająca po stronie kogoś z "władzy".
* Lokalni mieszkańcy chcący spokoju.

### Wymagane kroki:

* Zwalczanie thralli i satanistów brutalną siłą
* Zwrócenie uwagi Luksji, magów, paranormalnych
* Opanowanie miasteczka i kluczowych miejsc
* Sterroryzowanie mieszkańców i konkurentów ;-)
* Wypalenie wszystkiego potęgą Iliusitiusa
* Dyktatura Biegusia

### Ścieżki:

* **Brutalna siła porządkowa**:         0/8:  
* **Władza polityczna w mieście**:      0/8:  
* **Pokój przez tyranię**:              0/8:  
* **Jest jeden władca wszystkiego.**    0/8:  
* **Oczyszczenie Iliusitiusa**          0/10: 


## Neutralna

### Ścieżki:

* **Skażenie magią**:       2/12                      
* **Anarchia w mieście**:   6/12                      Tier 2


# Narzędzia MG

## Cel fabularny misji

Misto staje w płomieniach. Co zrobią postacie? Z Luksją? Z Lucyferem? Z satanistami..?

## Cel techniczny misji

1. Sprawdzenie Ścieżek i ich przyrostu
1. Sprawdzenie Aspektów i ich użycia

## Po czym poznam sukces

1. Ścieżki rosną szybko. Mają znaczenie. Nie rosną ZA szybko.
1. Gracze muszą decydować co robić i które Ścieżki wybrać.
1. Aspekty prowadzą do interesujących decyzji w ramach konfliktów
1. Aspekty są "lepsze" niż modyfikatory [-3,3]

## Wynik z perspektywy celu

1. Ścieżki rosną szybko. Moim zdaniem nie ZBYT szybko, zdaniem Kić zdecydowanie zbyt szybko. Dalsze misje pokażą.
1. Decyzje graczy mają ogromne znaczenie w kwestii przyrostu Ścieżek. Ten obszar - jak Ścieżki przyrastają - wymaga dalszych badań.
1. Kić zgłosiła, że za RZADKO próbkuję ścieżki. Za rzadko je przeliczam. To sprawia, że nie ma szans ani nadziei.
1. Walka podobała się wszystkim a magia "była magiczna". Dało się wyczuć.
1. Aspekty wyglądały poprawnie. Diabeł w tym, że trzeba je zrobić i skalibrować PRZED konfliktem.

## Wykorzystane tabelki

Postać (strona gracza):

| .Motywacja. | .Zachowanie. | .Specjalizacja. | .Umiejętność. | .Szkoła Magii. | .Cecha. | .POSTAĆ. |
|             |              |                 |               |                |         |          |

Opozycja (strona NPC):

| .VERSUS. | .Aspekty. | .Skala. | .Magia. |.Pomysł gracza. | .OPOZYCJA. |
|          |           |         |         |                |            |

Wynik:

| .Postać. | .Opozycja. | .Rzut.    | .Wynik. |
|          |            | xx: +x Wx |   SFR   |

---
layout: inwazja-konspekt
title:  "Patrycja węszy szpiega"
campaign: powrot-karradraela
gm: żółw
players: kić, dust, dzióbek
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [160326 - Siluria na salonach Szlachty (SD)](160326-siluria-na-salonach-szlachty.html)

### Chronologiczna

* [160217 - Sojusz według Leonidasa (LB, AB, DK)](160217-sojusz-wedlug-leonidasa.html)

## Punkt zerowy:

Ż: Jaką korzyść z tej całej sytuacji może uzyskać KADEM przeciwko Świecy?
B: KADEM uzyskuje przyczółek na obszarze pomiędzy KADEMem a Srebrną Świecą.
Ż: Jaka akcja z ukrycia na pewno MOCNO uderzyła w Srebrną Świecę?
K: Ktoś z członków Świecy zaatakował Powiew w taki sposób, że Powiew ma silne prawo do rekompensaty.
Ż: Co bardzo cennego dla Blakenbauerów wie Vladlena czego nie chciałaby Emilia by oni wiedzieli?
D: Może dać Blakenbauerom dostęp do czystego Węzła niewielkim kosztem.
Ż: O zdobycie czego poprosiła Emilia Hektora?
D: Jakiś konkretny magitech zbudowany przez Tamarę (Wiktora).
Ż: Dlaczego to coś jest gamechangerem całego konfliktu?
K: Bo ten konkretny magitech poda tożsamości i cele wielu członków Szlachty.
Ż: Co jest szczególnie cennego / ciekawego w węźle Vladleny?
D: Jest sterylny; jest to przenośny Węzeł, którego bardzo trudno Skazić emocjami i już ma poziom stabilny (autokumuluje).
Ż: Co w tej chwili Szlachcie najbardziej przeszkadza w dalszej ekspansji?
K: Uważają, że jest ktoś kto aktywnie zniechęca ich do dołączania się do nich. Myśleli, że to Kurtyna - jednak - jak widać - nie.
Ż: Co w tej chwili zastąpiło Tymotheusowi Blakenbauerowi Crystal i aptoforma?
D: An Ultimate Entity; nie w pełni działa bez Arazille i aptoforma. Nadal niebezpieczna broń, ale nie aż tak. "Ziarno Rezydencji".

## Misja właściwa:
### Kontekst misji:

- Wróciła Emilia i rzuciła bombę - magitech Wiktora ma przydatną wiedzę, klucz do zniszczenia Szlachty.
- Wiktor Sowiński i Diana Weiner dalej chcą współpracować z Hektorem. Wiktor dalej interesuje się Silurią.
- Laboratoria Blakenbauerów mogą być wykorzystane przez Szlachtę za zgodą Hektora i z planami dla Blakenbauerów.
- Emilia skonsolidowała siły Kurtyny dookoła siebie
- Blakenbauerowie wiedzą o obecności Tymotheusa w Kopalinie; Edwin szuka Tymotheusa.
- Mojra dała znać o Arazille Srebrnej Świecy.
- Siluria została pobita przez Ignata i wyłączona z akcji.
- Hektor wniósł oskarżenie przeciwko Vladlenie o próbę Spustoszenia hipernetu.
- Zajcewowie wyzajcewowali Blakenbauerów przez Tatianę, która potem została przez Hektora upokorzona.
- tien Adrian Murarz jest w Rezydencji Blakenbauerów; pomaga jak może.
- Olga ma dostęp do danych ze Skorpiona i może wykorzystać siły specjalne Hektora do rozwiązania mrocznych aktywności.
- Silny chaos na poziomie Świecy a na pewno na poziomie Diakonów.
- Diakoni zablokowani przez Mojrę; silny sceptycyzm co do działań Blakenbauerów (thanks, Romeo).
- Skubny zdobył koncesję na kanał telewizyjny.

### Poprzednie spekulacje

Magitech - najpewniej Spustoszony. Hektor musi wziąć to pod uwagę. To byłoby ciekawe.
Emilia - najpewniej nie rozmawiała z Tamarą a z Saith Kameleon. Bardzo w stylu Agresta.
Szlachta dowiadując się o spotkaniu Silurii / Emilii utrudni relację z Wiktorem. Albo nawet w drugą stronę, Szlachta na pewno dowie się o tej rozmowie, więc Siluria tym bardziej udaje, że próbuje wykorzystać Emilię na korzyść Szlachty. Czyli Szlachta ma wierzyć, że Siluria próbuje zadurzyć w sobie Emilię - jako cel do Wiktora.
Najpewniej po akcie oskarżenia magowie Kurtyny zrezygnują ze współpracy z Hektorem i przejmie ich Emilia.
Siluria docelowo będzie chciała działać przeciwko Arazille; żeby Arazille nie było w okolicy.

Plany na przyszłość: 
- wyciągnięcie magitecha (Siluria is on it)
- pokazać Dianie jaki Wiktor jest; doskonałe odwrócenie uwagi
- w skrócie: pełen chaos # chaos na poziomie Szlachty

### Plan graczy

- N/A

### Faza 1: Tajna akcja Patrycji.

Dzień pierwszy.

Hektor zamknął się w gabinecie z Edwinem, Margaret i Marcelinem szukając odpowiednich osób do kontaktu. Tak, potrzebny był Marcelin. On, niestety, sypia z za dużą ilością czarodziejek. Ich zadanie jest bardzo proste - znaleźć magów z rodu Weiner, z którymi Blakenbauerowie będą w stanie współpracować oraz opracować strategię współpracy Blakenbauerów. Czyli: wizja, misja, wszystko to na co Otton nie ma czasu. Bo płaszczki czekają. Otton i Mojra zajmują się poważnymi sprawami a osoby mniej umocowane politycznie zajmują się polityką.

Edwin w rozpaczy. Nigdy stąd nie wyjdą (a na pewno nie do końca misji)...

Tymczasem, w innym pokoju Rezydencji (koszary zamaskowane jako pokoje służby) siedzi sobie Alina oraz Dionizy. Czekają na Edwina. Edwina dalej nie ma. Nie mają rozkazów...

Tymczasem apka na smartfona (apka sił specjalnych) załatwiona przez Patrycję dostała update. To jest sygnał, że Patrycja chce się z nimi skomunikować twarzą w twarz i jest "alarm czerwony" i "ściśle tajne". Apka wskazuje, że spotkanie ma mieć miejsce na basenie. Dostali szafki; tam są kostiumy kąpielowe itp. Spotkali się z Patrycją na basenie, tam Patrycja dała im NOWE klucze do szafek (innych) i powiedziała, że za 65 minut się spotkają. Dionizy i Alina popływali zachodząc WTF Patrycja, po czym przebrali się w NOWE ciuchy z NOWYMI rzeczami a na telefonach (tak, nowych) był adres. Klub "Działo Plazmowe".

"Działo Plazmowe" jest fatalnym miejscem na spotkanie. Ale Patrycja powiedziała, że tu nie da się ich podsłuchać. Faktycznie, sami ledwo ją słyszą. Patrycja powiedziała, że przeprowadziła własne śledztwo; zaczęło się to od etapu aptoforma - do prokuratury wpływały informacje, które nigdy nie dotarły do Hektora. Czy to o aptoformie (tematach powiązanych), czy o Szymonie Skubnym (co zdziwiło Alinę i Dionizego) czy powiązane. Patrycja ustaliła, że Blakenbauerowie to taka bogata rodzina jak w "Modzie na sukces" czy "Dynastii" i oni wsadzili macki do prokuratury. Dionizy i Alina to osoby, którym Patrycja zaufała, więc im może powiedzieć (jako JEDYNI z sił specjalnych są silnie usztucznieni, więc Edwin DOBRZE zatroszczył się o ich historię i jej wiarygodność; reszta grupy ma wątpliwą historię (bo prawdziwą)).

Patrycja zdecydowała się wrócić do dzieci i zostawić to im. Powiedziała też, że na stan jej wiedzy Skubny ma telewizję po to, by móc zbierać informacje i ich NIE rozpowszechniać. Patrycja dała przykłady akcji utajniających Aliny i Dionizego jako dowód, że ktoś manipuluje informacjami w prokuraturze. Dionizy i Alina zachowali powagę. Alina wydobyła jeszcze informację (Patrycja nie wie, że Alina to wie), że Patrycja podzieliła się tą informacją z JESZCZE kimś z sił specjalnych. Ale nie powiedziała z kim. By dwie różne strony same się kontrolowały. I Alina się domyśla, że komu Patrycja tego nie powiedziała, też wpadnie na to, że nie jest jedyny...

Patrycja dostarczyła mały USB który zawiera WSZYSTKIE informacje które trafiły do prokuratury i nikt nie przekazał ich dalej do sił specjalnych albo Hektor nie zadziałał zgodnie z tymi danymi (czyli: rzeczy utajnione przez Hektora, Alinę, Dionizego, Klemensa, INNYCH magów, rzeczy którymi Hektor się jeszcze nie zajął itp). Jest tego DUŻO.

Alina i Dionizy wzięli USB do siebie i zaczęli to rozbierać. Odrzucać rzeczy, z jakimi mieli coś wspólnego. Wyrzucić rzeczy typowo magiczne, które miały nie dotrzeć (znamiona magii, implikują agentów magów w prokuraturze - nie interesują Aliny i Dionizego, choć kiedyś zaciekawi to pewnie Hektora). Próbują znaleźć rzeczy powiązane ze SkubnyTV (bo to wzbudziło zainteresowanie Patrycji). I znaleźć kreta w prokuraturze, takiego, na jakiego da się skierować Patrycję. 
I znalazła. Wrobiła dane w taki sposób, by skupić uwagę Patrycji na FAKTYCZNYM krecie w prokuraturze mało powiązanym z magami. Po prostu przekupny gość, też powiązany pierwotnie ze Skubnym, ale ostatnio "kto zapłaci więcej" (niestety w wyniku konfliktu Patrycja wróci na ślad prędzej czy później). 

Alina przejrzała źródła danych. Alina chciała dojść do tego, jakie są najbardziej interesujące informacje dotyczące prokuratury i co wygląda najbardziej niewinnie i zawsze znika. I doszła do tego. Przy danych Patrycji nie było to trudne. Udało się Alinie zlokalizować silne powiązania Szymona Skubnego z zakładami chemicznymi "Chegolent". W różnych tematach pojawiały się delikatnie wątki dookoła finansowania niektórych działań Skubnego i niektórych działań "Chegolent". O dziwo, czasem działania charytatywne Skubnego były wspierane przez datki "Chegolent" i "Chegolent" był wspierany przez niewidoczne działania Skubnego.
Zaczęło się to cztery miesiące temu. Przedtem nie było nic o powiązaniach.
A "Chegolent" jest dość... niewinny i niewidoczny.

### Faza 2: Kret w Rezydencji.

Dzień czwarty. 

Dionizy "Haker" Kret przygotowuje wiadomość trojańską - zawiera odpowiednie załączniki, obrazy w raporcie, robi trojana. Takiego z danymi jaki przedtem był blokowany. Jeśli tam się odbywa cenzura, ktoś to musi przeczytać. Jeśli tak jest, Dionizy "Haker" Kret będzie w stanie zlokalizować jak najwięcej odnośnie tego kto i kiedy to otworzył. 
Dionizy wysłał wiadomość i czekał. Udało mu się znaleźć maszynę; to maszyna w Rezydencji. Nie wiadomo czyja, ale pochodzi z Rezydencji.

Alina zaproponowała Borysowi partyjkę w pokera. Borys oczywiście dał się wyciągnąć; Alina chce umożliwić Dionizemu dostęp do głównych komputerów ochrony Rezydencji. Gra w rozbieranego pokera jest dość angażująca, by nawet Borys, stary oszust, mógł się skupić na... Alinie, a nie na tym, że zapomniał się wylogować. Do niczego (niestety) nie doszło.

A Dionizy ma dostęp do stacji centralnej zabezpieczeń Rezydencji. Bez kłopotu złamał zabezpieczenia Borysa i zlokalizował "winny" komputer - jest to maszyna Marcelina. A cross z kamer obserwujących Rezydencję wskazuje na to, że winną osobą jest Wanda Ketran, "podobno perfekcyjnie kontrolowana" przez Edwina. Jasne. Nie można ufać cybergothkom.

W czym ważne jest, że wiadomości w przeszłości znikały jak jeszcze Wandy tu nie było. Pewnie był ktoś inny. Teraz tą rolę szpiegujowca przejęła Wanda Ketran. Człowiek, cybergothka. Nieźle. Dionizy sprawdził jeszcze na monitoringu co Wanda robi z jedzeniem. Zjada. Nie robi innych dziwnych rzeczy, nie szuka trucizny...
Dobra wiadomość.

Na czas obiadu Dionizy podrzuca odpowiednie piguły leków do jedzenia Wandy (odpowiednio dozowane). Tak, by Wanda powiedziała prawdę. Po czym chwilę czeka, aż ona to zje i jak tylko poczuła się gorzej, Dionizy programistycznie wyłączył kamery (zostawił backdoor) i wbił Wandzie do pokoju. Zamaskowany. Wanda półprzytomna na łóżku, Alina zaczyna pytać. Wanda nie ma szans.
- Wanda nie była zainteresowana Marcelinem ani Blakenbauerami. Nie interesowała się. Dopiero Edwin wyindukował jej zainteresowanie.
- Wanda nie wie dlaczego to robi. Po prostu powinna. "It's a right thing to do".
- Wanda nie jest w pełni pod niczyją kontrolą. Ma elementy wolnego agenta. Cały czas. Co - zgodnie z tym co mówił Edwin - jest niemożliwe.
- Wanda ma dostęp do haseł systemu (nic dziwnego, dziewczyna Marcelina).
- Usuwane są dane o Skubnym, Chegolent, o części magii, o Blakenbauerach (wszystko).

Dionizy instaluje na kompie Marcelina program, że jak coś skasuje, to zostaje w jakimś ukrytym miejscu "skasowane".

Dionizy i Alina udali się do Edwina; wyciągnęli Edwina z pokoju i zdali mu raport. Edwin usłyszał i powiedział, że mają to zostawić. On wprowadził Szymona Skubnego po to, by Hektor miał coś czym może się zająć i nie przeszkadzał mu w innych ważnych sprawach. Mają zapomnieć zwłaszcza o wszystkim związanym z firmą "Chegolent".

Edwin dowiedział się od Dionizego i Aliny o Patrycji i jej tajnym śledztwie. Westchnął ciężko. Musi ją skorygować - kazał Dionizemu i Alinie ściągnąć Patrycję do Rezydencji, on dowie się co zrobiła, skoryguje jej pamięć i kaskadowo zapoluje na dalszych członków sił specjalnych by to wyczyścić. I w ten sposób potencjalny problem zostanie załatany...

# Streszczenie

Patrycja wpadła w tryb paranoiczny i zaczęła badać... Blakenbauerów. Bo Blakenbauerowie ukrywają dane przed Hektorem. Dużo takich informacji. Alina spowolniła Patrycję; znaleźli w tym też powiązania między Skubnym a firmą Chegolent. Dionizy wykrył, że mają kreta w Rezydencji - Wandę Ketran. Ona to robi. O dziwo, mimo, że zdaniem Edwina nie jest to możliwe - ma elementy wolnego agenta. Ponadto usuwa dane o Skubnym, Chegolent, o części magii, o Blakenbauerach (wszystko). Edwin skorygował Patrycję (by ta nic nie pamiętała) i zbagatelizował Wandę (on jej kazał usuwać te dane).

# Lokalizacje:

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Centrum
                        1. Cyberklub Działo Plazmowe, gdzie - zdaniem Patrycji - nie da się ich podsłuchać. Ma rację. Nie da się tam NIC podsłuchać i ledwo da się rozmawiać.
                        1. Prokuratura
                    1. Obrzeża
                        1. Rezydencja Blakenbauerów
                    1. Dzielnica Owadów
                        1. Basen Miejski

# Zasługi

* vic: Alina Bednarz, zwodzicielka Borysa, osoba zadająca trudne pytania i mistrzyni analizy danych z Excela.
* vic: Dionizy Kret, master hacker Windowsa Vista, agent podrzucający narkotyki do jedzenia Wandy i kompetentny porywacz.
* czł: Patrycja Krowiowska, która przedsięwzięła akcję oczyszczenia prokuratury z kreta i zdrajcy. Niestety, Edwin jest * magiem.
* czł: Wanda Ketran, która choć w części jest autonomiczna, jednak Edwin jej zaprogramował usuwanie danych przed Hektorem.
* czł: Borys Kumin, szef zabezpieczeń Rezydencji potwierdzający ponownie swoją niekompetencję w tej roli.
* mag: Edwin Blakenbauer, który, jak się okazuje, ukrywa Szymona Skubnego i firmę Chegolent przed Hektorem. "By miał wyzwanie i nie przeszkadzał".
* czł: Szymon Skubny, nie było go, ale dowiedzieliśmy się, że Edwin go ukrywa; ma powiązania z firmą Chegolent.

# Czas

* Dni: 4
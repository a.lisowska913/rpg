---
layout: inwazja-konspekt
title:  "Samotna w świecie magow "
campaign: powrot-karradraela
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [170221 - Przecież nie chodzi o koncert... (temp)](170221-przeciez-nie-chodzi-o-koncert.html)

### Chronologiczna

* [161222 - Kto wpisał Błażeja do konkursu?! (SD)](161222-kto-wpisal-blazeja-do-konkursu.html)

## Kontekst ogólny sytuacji
## Punkt zerowy:

Ż: Na jaką imprezę wieczorem wybiera się Siluria? (coś w Kopalinie).
K: Otwarta potańcówka w Dziale Plazmowym. She is determined not to go to sleep alone ;-).

## Misja właściwa:

DEKLARACJA: misja ma niską stawkę. 
DEKLARACJA: misja testuje mechanikę surowców.

### Dzień 1:

Siluria idzie spokojnie na imprezę do Działa Plazmowego w Kopalinie. Jest ubrana adekwatnie, ostro podrasowana, ale bez przesady, bo nie wymaga ciężkich dział ;-). Idzie polować. Wtem - wyczuła coś dziwnego z pobliskiego śmietnika. Artefakt. Wyraźny artefakt z emanacją KADEMu... akurat nikogo nie ma w tamtym miejscu. Siluria rozejrzała się uważnie - nic nie ma, żadnych kamer (nie zauważyła schowanej Anny Kozak czającej się). Siluria nie jest w trybie zbyt podejrzliwym. Artefakt jest gdzieś na wierzchu śmietnika, trzeba tylko otworzyć.

Siluria podniosła wieczko, nie nachylając się; jest w nastroju na imprezę, nie zasadzkę. Wybuch. Siluria się zatoczyła i została zaskoczona; Anna wyskoczyła z środkiem obezwładniającym (coś w stylu chloroformu, ale na magów) na Silurię. 

* Żółw: "Jeśli chcesz walczyć, możesz. Masz malus. Jeśli NIE chcesz walczyć, ok. Jeśli chcesz wygrać, możesz użyć surowca - Ty, nie Siluria - i wezwać wsparcie używając sinków. Np. z kimś się tu spotykasz. Albo kogoś tu znasz. Wiesz - surowce pasujące. Nie postać. Ty."
* Kić: "Wiem wiem, myślę, co chcę zrobić. Actually! Tak! Szczerze? Niech to będzie człowiek... Siluria chciała być z człowiekiem, umówiła się, i niech on się tu pojawi."
* Żółw: "Oki, To jest surowiec; który sink?"
* Kić: "Reputacja osoby z jaką można się zabawić. Partner na imprezę. To jest reputacja na trójce; Siluria może ściągnąć właściwie każdego."
* Żółw: "Zgadzam się. Kontynuujemy."
* Kić: "Teraz, po co to jest. Chcę zobaczyć kto."
* Żółw: "Trudna sprawa. Przeciwnik jest przygotowany, w masce itp. Wiesz, że to drobna kobieta. Ale Twój sojusznik może próbować ją odgonić."
* Kić: "Jestem na tyle ciekawa, że chciałabym dać się porwać, ale nie nieprzytomna. Może to nie sink z partnerem?"
* Żółw: "To inaczej; Bo masz np. Ignata jako inny potencjalny sink. Więc detektyw może Ci to znaleźć. Lub możesz spróbować schwytać napastnika. Lub inny sink. Np. masz jakieś... narkotyki? Wiesz, coś co Cię może pobudzić?"
* Kić: "Hm. Tego nie mam jako sinka..."
* Żółw: "Jak z przysługami na KADEMie?"
* Kić: "3. W zasadzie, jakbym z tego wzięła... wiem! Przysługi na KADEMie, prezent od Karoliny, coś co się da zaaplikować w tej sytuacji, czy to jakaś maść czy coś..."
* Żółw: "Inaczej. Ty sobie to zaaplikowałaś przed akcją; wiesz, że czasem magowie mają... problemy, więc Karolina coś Ci zrobiła wcześniej... pasuje? W ten sposób jesteś... przytomna."
* Kić: "Ok, niech tak będzie. ALE! To mi daje przytomność, jednocześnie coś co chcę zrobić - struggle a bit, następnie niby padam, ale! W związku z tym chcę zostawić jakiś ślad w tym miejscu. Coś, co wskaże osobie szukającej, że tu była i nie jest oczywiste dla porywacza."
* Żółw: "Intencja. Chcesz byś mogła użyć surowca byś chciała się wyrwać?"
* Kić: "De facto tak. Chcę mieć dostęp np. do usług Ignata, który będzie Silurii szukał. Ale niekoniecznie chcę to odpalać teraz. Nie chcę skończyć w piwnicy bez szansy, że ktoś będzie mnie szukał."
* Żółw: "Przyjąłem. Pasuje. To tu jest konflikt... Coś w stylu aktorstwa albo..."
* Kić: "Oblicze dla każdego - bo się spodziewa że tak się skończy - i Devious. Impulsu tu nie widzę. Czyli 4"
* Żółw: "Walczysz z... policzę... 3."
* Kić: "Siluria bywała w różnych sytuacjach. Może to ciekawa gra wstępna... 9, czyli... 3. Siluria będzie lekko ogłupiona i nie wie, GDZIE ją zabierają."
* Żółw: "Pasuje. I masz opcję użycia surowca, by móc wezwać wsparcie w dowolnym momencie. Pasuje?"
* Kić: "Mhm."

Oki, Anna przeprowadziła Silurię takim miejskim autkiem, gorszej klasy, do... gdzieś. Siluria nie do końca wie gdzie jest. Do znajdującego się niedaleko podziemia Kina V, nieczynnego chwilowo. Tam Siluria została ocknięta (bez brutalności), twarzą do ściany, z zachowaniem godności, niezbyt fachowo, acz wprawnie związaną. W tym, że Siluria jest... lepsza w węzłach. I wyczuła kilka potencjalnych słabych punktów.

* Siluria Diakon, czarodziejka KADEMu. - Anna, głosem zmodyfikowanym przez maskę Vadera
* Ciekawy fetysz - Siluria nie do końca mająca ochotę się bać...
* Nie stanie Ci się krzywda, jeśli odpowiesz na kilka moich pytań - Anna, lekko sfrustrowana

Na to, przy stole:

* Żółw: "Pamiętaj, że masz -1 do testów społecznych. Jesteś lekko ogłupiała."
* Kić: "Stąd też to o fetyszu. Niekoniecznie powiedziałaby to jako pierwszą rzecz ;-)"
* Żółw: "Acz by pomyślała jako o pierwszej"
* Kić: "Actually! Testy społeczne testami społecznymi, ale gramy na ogłupienie ;p."
* Kić: "Nawet lekko ogłupiona Siluria może pójść w tą stronę... akcja trochę bardziej złożona..."
* Kić: "Cholera wie jakie pytania... ale chcę zagrać w 'jestem ogłupiona, wzbudzam litość, przesadzili ze środkiem... a postawą ciała okazać, że ten sposób skrępowania jest dla Silurii nieprzyjemny i powoduje dyskomfort. Chcę sprawdzić miękkość łapacza Silurii a po drugie, daje Silurii szansę by się pozbierać. Czyli gram na czas.'"
* Żółw: "Czyli grasz na czas? Wynikiem konfliktu ma być zdjęcie malusa?"
* Kić: "Tak. A jednocześnie test, czy przeciwnik jest na tyle miękki by zmienić sposób skrępowania."
* Żółw: "Dwa testy. Ale najpierw malus."
* Kić: "Oblicze dla każdego, rozbrajająco urocza, niegroźna. W sumie z Devious: 5"
* Żółw: "Oki. Liczę. Przeciwnik zrobił research; ma #2."
* Kić: "Wie, że ogłupił Silurię i jest to skuteczne. She is doing a plausible thing."
* Żółw: "Point, zwłaszcza, że masz to we krwi od Karoliny. Masz #1. Teraz liczę... przeciwnik ma z tym #2... 3 i 2, czyli 5."
* Kić: "U mnie jest 5 i 1... czyli 6. Dobra, rzucam."
* Kić: "4. Fail. -2. Branie malusa za ściągnięcie malusa jest słabe."
* Żółw: "Daj pomyśleć... przeciwnik przejrzał, ale za późno. To utrudni ten drugi test, ale nie masz malusa."
* Kić: "Malus do następnego testu de facto."
* Żółw: "Przeciwnik widzi, że Siluria doszła do siebie, de facto. Czyli nie możesz grać w ogłupioną."
* Kić: "Nie mogę grać na ogłupienie... pasuje. Byłoby korzystne, ale jak nie mogę..."
* Żółw: "Akceptuję."

Siluria doszła do siebie, udając ogłupioną bardziej niż była. We krwi środki Karoliny zadziałały. Jej przeciwnik miał...

* Żółw: "Właśnie, konflikt. Możesz tylko wygrać. Knowledge # jakiekolwiek narkotyki czy środki psychoaktywne."
* Kić: "Mój knowledge o środkach? Chyba tylko z corruptora."
* Żółw: "Nie, z imprez. Imprezowe narkotyki. Nie wierzę, że Siluria się z tym nie spotkała."
* Kić: "Całe 1 w sumie..."
* Żółw: "Kno # Impreza = 1?"
* Kić: "Tak."
* Żółw "Sprawdzam tabelkę. To bym założył na równy zawodowy - to jest 3."
* Kić: "Mmm..."
* Żółw: "Nie możesz nic stracić a możesz ROZPOZNAĆ ten środek. Czyli win/no-lose. Testuj."
* Kić: "Musi być 20 by przeszło. A padło 4."
* Żółw: "Nie chcesz eskalować? ;-)"
* Kić: "Kosztem czego?"
* Żółw: "Nie mam pojęcia... ale masz małe szanse."
* Kić: "Nie, nie warto. Drugi rzut z liczeniem na 20... nie warto."
* Żółw: "Point."
* Kić: "Mogę to spróbować wydusić inaczej. Ale rozpoznanie tego tak nie zadziała."
* Żółw: "Ok. Masz rację. Warto było spróbować ;-)."

Anna się zorientowała, że Siluria doszła do siebie, ale za późno. Powiedziała, że widzi, że Siluria nie chce współpracować.

* Kić: "Research researchem, ale chcę sprawić, by przeciwnik uznał Silurię za całkowicie niegroźną."
* Żółw: "Co masz na myśli? Z perspektywy przeciwnika Siluria jest związana i nie uważa jej za zagrożenie fizyczne."
* Kić: "Ale się chowa - chowa twarz itp."
* Żółw: "Chowa twarz, bo KADEM. KADEM może zrobić każdemu z dupy jesień średniowiecza. Ona nie boi się Silurii. Boi się KADEMu. Więc nie konfliktuję, ale nie zmieniam zachowania."

Siluria zauważyła, że ona zawsze chętnie współpracuje i wszyscy o tym wiedzą; żałośnie spytała czemu to wszystko. Sprawdziła węzły, szukając dyskretnego sposobu rozluźnienia. Spytała, czy Anna myśli, że poszło jej tak łatwo. Przecież mogła walczyć.

* Żółw: "Konflikt. Konfliktuję tylko 'rozluźnienie', nie 'dyskrecję'. Przeciwnik nie spodziewa się tego, więc jeśli chcesz po prostu sprawdzić... walczysz z '1' #2 za istnienie węzłów bez intencji rozwiązania."
* Kić: "Bondage 3, nimble #1..."
* Żółw: "Bondage 3?!"
* Kić: "Specjalizacja węzłów. Powiedz, że nie pasuje do Silurii bondage, ze wszystkich rzeczy. Zwłaszcza po Wiktorze. Niby przed, ale... it's her thing."
* Żółw: "I rest my case... Bondage 3 it is... potem przeanalizujemy Silurię; masz za dużo specek."
* Kić: "Więc: Bondage 3, Nimble 1, śliska jak piskorz 1... w sumie, jedną speckę bez żalu wywalę od ręki dla bondage ;-)."
* Kić: "Rzuciłam 14. Czyli... 5#1, czyli 6. Kontra 3."
* Żółw: "Tak. Możesz w dowolnym momencie kosztem chwili pozbyć się węzłów. Najlepiej, by druga strona nie widziała bo będzie mieć silny bonus"
* Kić: "A wracając do kontynuacji rozmowy..."

Anna powiedziała zimno, że Siluria mogła walczyć, ale przegrała. A KADEM stoi za pewnymi zbrodniami i ona chce dowiedzieć się, czy Siluria wie coś o tym. Jeśli Siluria za tym stoi, Anna chce tylko dowiedzieć się kto i Silurię puści. Więc - czy Siluria odpowie na jej pytania, czy nie. I wtedy Anna będzie musiała użyć innego typu środków jak nie będzie chciała odpowiedzieć. Anna nie chce, by Siluria grała na czas; nie chce zrobić czegoś, co może Silurię skrzywdzić.

* Kić: "Skoro mogę się uwolnić, nie chcę by widziała, że mogę coś zrobić z węzłami bo mi to może utrudnić życie."
* Kić: "W zasadzie, Żółw... hm. Siluria szła na imprezę, tak?"
* Żółw: "Kić, czy ja mam zakonfliktować wyciąganie informacji?"
* Kić: "Nie, na razie myślę jak postawić konflikt... chcę pójść w tą stronę, że nie dowie się póki nie spyta ale Siluria pomoże jak się dowie. KADEM zwykle jakieś zbrodnie robi, ale zwykle wie co się dzieje... a to jest dziwna sprawa."
* Żółw: "Oki."

Anna zaczęła zadawać pytania. Zaczęła od: "Dlaczego KADEM zaczyna rozprowadzać narkotyki wśród ludzi w okolicach Kotów?".

* Kić: "<Blink>. WHAT?! Siluria nic raczej nie wie o narkotykach w okolicach KOTÓW. Kurczę, wiedziałaby!"
* Żółw: "Raczej by wiedziała."
* Kić: "Tu jest szczere zdziwienie. Możesz sobie konfliktować dowolnie długo. Szczerzej nie będzie. Ja cały czas pamiętam o surowcu 'ratunek'. I do intend to use Ignat. Ale wpierw chcę się coś dowiedzieć..."
* Żółw: "Oczywiście ;-)."

Drugie pytanie Anny: co w ogóle robi KADEM w Kotach na terenie Świecy?

* Kić: "Świecy? Nie Millennium?"
* Żółw: "Potwierdzam. Nieformalnie: Koty są w jurysdykcji Millennium. Nie są SILNIE wspierane, bo nadal... mamy dominację Świecy, ale KADEM traktuje Koty jak teren Millennium. Nikt w sumie nic nie robi w Kotach. Nie po tien Asterze."
* Kić: "Tia... zwłaszcza Świeca nic nie robi w Kotach po tien Asterze."
* Żółw: "Nie oficjalnie. Nikomu w Świecy się nie opłaca pchać do Kotów i wyciągać Astera."
* Kić: "On był tam zakopany, nie?"
* Żółw: "To ta sprawa z kralothem co de facto pogrzebało go politycznie za niekompetencję."
* Kić: "Na tak postawione pytanie Siluria się obrusza; to nie teren Świecy. Po drugie: czy ja pilnuję magów KADEMu? Nie, Siluria naprawdę nie wie..."

Anna jest lekko skonfundowana. Pyta Silurię, która czarodziejka KADEMu bawi się narkotykami.

* Kić: "Mnie przychodzi do głowy Karolina, ale... to nie narkotyki. Nie uzależniają, więc to nie narkotyki."
* Żółw: "Wiem, psychoaktywne środki hedonistyczne ;-)."
* Kić: "Silurii wystarcza, że nie uzależniają."
* Żółw: "To skonfliktuję. Na wyciąganie informacji. Ale masz #1 - faktycznie, Karolina nie bawi się narkotykami w rozumieniu Silurii."
* Kić: "Manipulatorka # Devious = 3."
* Żółw: "Aggressive # Impuls # detektyw. 4."
* Kić: "Let us see... 6, czyli -1. Either way, zaeskaluję."
* Żółw: "Nie chcesz podać Karoliny?"
* Kić: "Nie."
* Żółw: "Masz pomysł na eskalację? Bo ja mam."
* Kić: "Jaki?"
* Żółw: "Odpalisz teraz surowiec. Znaczy: enter Ignat."
* Kić: "Pasuje."
* Żółw: "Reroll."
* Kić: "13. Remis."
* Żółw: "Daj jej headstart."
* Kić: "Nie. Nie nie nie. Absolutnie nie. Nie dam jej headstartu, bo zwieje."
* Żółw: "Kind of my point."
* Kić: "No tak, ale to PEWNIAK że zwieje. To tak nie działa."
* Żółw: "Inny koszt remisu? #2? Też daje jej duże szanse."
* Kić: "Actually, Żółw, a co jeśli się zorientuje, że Siluria jest pod wpływem czegoś?"
* Żółw: "I co to da?"
* Kić: "Nie wiem. Nie wiem czego ona chce a zwianie nie wchodzi w grę."
* Żółw: "Ona chce znaleźć osoby odpowiedzialne za te narkotyki. I nie zostać wykryta przez KADEM."
* Kić: "Wymieńmy się. Dowie się o Karolinie. Ale Ignat ją zaskakuje i ma malus. Spodziewała się, że ją znajdą, ale nie że tak szybko."
* Żółw: "Jest dobrze przygotowana, ale Ignat jest detektywem... więc może ją znaleźć."
* Kić: "Ale ja się rozwiązuję, by mieć free action."
* Żółw: "Nie wiesz o Ignacie, więc nie masz jak zdążyć. Ignat to niespodzianka ;-)"
* Kić: "Jak coś zaczyna się dziać, zaczynam się rozwiązywać."
* Żółw: "No tak, ale jak coś zaczyna się dziać, ona daje w długą. Ona nie próbuje walczyć."
* Żółw: "Priorytetyzuję jej wycofanie się nad dowiedzenie się o Karolinie."
* Kić: "Ok... ale dla mnie jej wycofanie się w wyniku tego konfliktu nie jest akceptowalne."
* Żółw: "Czemu?"
* Kić: "Bo jej nie znajdę. Ignat Ignatem, ale jak jest tak dobrze przygotowana, to jej nie znajdę."
* Żółw: "Znajdziesz ją. Wiesz, że coś się dzieje w Kotach. Jeśli tak, tam będą do niej ślady. Plus: artefakt KADEMu, środek Tobie podany, samochód... nie mówię, że to pewne, ale są JAKIEŚ ślady." 
* Kić: "Ok... no dobra."
* Żółw: "Powiem więcej. Nie chcę imienia Karoliny. Ale chcę, by się dowiedziała, że Siluria nie ma z tym nic wspólnego. Konfliktujesz?"
* Kić: "All right. Czyli zwieje, ale Siluria nie będzie na jej celowniku?"
* Żółw: "Tak. Będzie przekonana, że Siluria nic o tym nie wie. Niekoniecznie, że żaden mag KADEMu nie wie, ale Siluria na pewno nie wie."
* Kić: "Ok. I znalezienie jej wciąż jest możliwe?"
* Żółw: "Może wymagać trochę surowców, ale masz samochód, artefakt, miejsce pułapki, Koty, środek... jest tego trochę. A Ignat jest dobry. Zawsze możesz eskalować ;-). Ja będę ją chronił."
* Kić: "Nie widzę sensownych kosztów eskalacji dla obu stron."
* Żółw: "Mam na myśli: eskalacji gdy próbujesz dojść do tego kim ona jest."
* Kić: "Nie no, chyba OK. Myślę, że mogłabym to lepiej rozegrać, ale nie umiem wymyśleć sensowniejszego konfliktu."
* Żółw: "Wiesz, co możesz zrobić dalej w tym wątku?"
* Kić: "Ona pewnie zwieje a Ignat będzie jej szukał."
* Żółw: "To zróbmy inaczej: pomogłaby Ci próbka tego narkotyku?"
* Kić: "Tak, to by pomogło."
* Żółw: "Daj jej się wycofać. Pokaże Ci próbkę. Ignat ją przestraszy i masz."
* Kić: "Tak samo zostawi środki wykorzystane w tej akcji."
* Żółw: "Niepotrzebne. Forensic KADEMu to wykryje. Masz to w sobie."
* Kić: "Nie zdążyła posprzątać. Zostawiła więcej śladów. Ale na razie skutecznie zwiała."
* Żółw: "Pasuje. Po co Ci te ślady, intencja?"
* Kić: "By ją znaleźć, docelowo. By mieć czego się zaczepić."
* Żółw: "Bonus #1?"
* Kić: "Mhm"
* Żółw: "Ok."

Wpadł Ignat, jak to Ignat. Z trotylem, hukiem i napalmem. Anna się przestraszyła i zwiała. Ignat przypadł do Silurii, cały przestraszony, która zaczęła się uwalniać z więzów spokojnie. Ignat patrzy taki zdziwiony. Myślał, że Siluria została porwana...

* Nic mi nie jest! Łap ją! - Siluria do Ignata

Ignat poleciał za przeciwnikiem, ale nie udało mu się jej złapać. Cholera za szybko biega ;-).

* Kić: "Cichy chód na wsi i szybki bieg w mieście? :D"
* Żółw: "Idealnie ją opisałaś."

Opiekuńczy Ignat wrócił do Silurii. Która tylko chciała iść na imprezę...

Kić: "KOMPETENTNA grupka zbiera ślady pozostawione przez napastnika. Nie będę angażować Whisper, ale Ignat ma kumpli. A Siluria idzie do Karoliny."
Kić: "Ach, dwa surowce poszły. No cóż, przeciwnik też musiał swoje zużyć?"
Żółw: "Tak. Napastnik ma downtime. Nic więcej nie zrobi."

Na KADEMie Siluria poszła do Karoliny Kupiec. Sytuacja zdążyła się roznieść.

* Żółw: "Wiesz, nawet bez surowca pozwolę Ci użyć 'maskotka wszystkich' by podnieść atmosferę."
* Kić: "Nie! Nie chcę by KADEM zaczął się zbroić... nie cały KADEM. Chociaż, w sumie, actually... tylko ja nie chcę doprowadzić do sytuacji, w której przeciwnika mi zlinczują."
* Żółw: "Jeśli to mag Świecy to Ignat ją zlinczuje z definicji"
* Kić: "To prawdopodobnie JEST mag Świecy. Chcę kogoś kto powstrzyma Ignata przed linczem..."
* Żółw: "I tak użycie Ignata do znalezienia przeciwnika będzie surowcem, chyba, że użyjesz relacji bezsurowcowo przez konflikt."
* Kić: "Wiem jak to zrobić. Podgrzać atmosferę, owszem, ale nie na święty RAGE a na coś w stylu 'oddamy ją Silurii'"
* Żółw: "Nie kontrolujesz KADEMu. Nic nie kontroluje KADEMu i tej atmosfery."
* Kić: "Tak, ale mogę sterować atmosferą. Nie chcę doprowadzić do linczu..."
* Kić: "Realistycznie patrząc POWINNA być tu jakaś forma konfliktu, bo KADEM trzyma się razem a ktoś zaatakował kogoś z KADEMu."
* Żółw: "Masz rację. Konflikt powinien być jak chcesz ZAPOBIEC linczowi."
* Kić: "Tak. Ten konflikt zrobię. Uspokajam atmosferę. I raczej obracam ją w kierunku na 'give her to me, she will be my pet - it's personal'"
* Żółw: "Proponuję użyć radiowęzła ;-). Siluria jest popularna a magowie KADEMu mają gorące głowy ;-)."
* Kić: "To NIE był rozsądny wybór, uderzanie w Silurię"
* Żółw: "Realistycznie, w kogo innego dało się uderzyć? ;-)"
* Kić: "To tylko dowodzi jak bardzo ta osoba nie rozumie KADEMu..."
* Żółw: "Kim jestem, by zaprzeczać ;-)"
* Kić: "KADEM, tak bardzo niezrozumiany..."
* Żółw: "Stopień trudności: KADEM trzyma się razem. To problem napędzającej się grupy. Czyli #2 za swarm. #1 za sam KADEM i nastawienie. Trudność ekspercka..."
* Kić: "Coś nie widzę jak Siluria to zrobi..."
* Żółw: "Cichaj tam. Już liczę. Równy ekspert to 4, czyli w sumie 7."
* Kić: "Bez szans."
* Żółw: "Kombinuj z narzędziami. Jaką masz podstawę?"
* Kić: "Friendly, Maskotka wszystkich (dostaję o co proszę) -> 4, jako impuls przejęcie kontroli w rękawiczkach -> 5."
* Żółw: "Widzisz?"
* Kić: "Kontra 7? 20 muszę rzucić."
* Żółw: "Kombinuj z narzędziami i okolicznościami. Np. jeśli jesteś skłonna uspokajać każdego z osobna i siedzieć nad tym całą noc, dam Ci #1 ;p."
* Kić: "Malus jutro >.>"
* Żółw: ":-)"
* Kić: "Nie jest jutro akceptowalne. Chodzenie non stop na malusach jest słabe."
* Żółw: "Wymyśl inne narzędzia i okoliczności. Pokazuję, że to nie jest beznadziejne. Zważ, że RATUJESZ swojego napastnika ;-)."
* Kić: "Bo nie chcę linczu i problemów..." <kwaśno>
* Kić: "Ale w sumie... jakby tak pójść z argumentacją w taką stronę, hmm... po pierwsze, zagrać na arogancji KADEMu 'she's not worth it'..."
* Żółw: "Niegodna istota zaatakowała naszą kochaną Silurię...?"
* Kić: "W tą stronę nie..."
* Żółw: "Pomoże Ci możliwość ściągnięcia surowca z następnego dnia?"
* Kić: "Może pomóc, choć w następnym dniu wchodzę bez surowców bo drugim odpalam Ignata. I jestem wyprztykana..."
* Kić: "Ty, ja wiem. Jeszcze inaczej. Nie łagodzić tych nastrojów sensu stricte. Przekierować je w dodatkowy surowiec."
* Żółw: "KADEM chce pomóc Silurii, bo 'all for one'?"
* Kić: "Tak, bardziej w tą stronę. Żeby to poszło w 'help me do it my way'. Cały ten RAGE wykorzystać do sformowania surowca."
* Żółw: "Tak, zadziała. Teraz ta arogancja jest #1. Podgrzewa nastroje, zapobiega linczowi, ale konfliktuje KADEMowi z nie-KADEMem."
* Kić: "Ale mam zasób. Nawet kilka, bo KADEM jest silną gildią. A być może na tej zasadzie, że być może w kontekście tej sytuacji jeśli Siluria prosi o przysługę (koszt surowca) nie ma kosztu surowca."
* Żółw: "Tego nie skonfliktuję. A raczej: skonfliktuję inaczej. Popatrz: masz nie liczoną pulę surowców. Ale każde użycie tej puli zwiększa ogólny RAGE KADEMu. Nie wobec napastnika (bo Siluria chce po swojemu) ale wobec nie-KADEMu. Bo ktoś ją zaatakował. Co ładnie się wpasowuje w przyszłość, która już się odbyła w poprzednich częściach tej kampanii. Czyli paradoksalnie napastniczka i Siluria podgrzały tą przyszłą atmosferę."
* Kić: "Ona i tak była gorąca i nie wiedzieliśmy czemu..."
* Żółw: "Jeśli się zgodzisz na to, to ta sprawa na pewno była jedną z przyczyn. Co Ty na to?"
* Kić: "Wiesz co, ładnie się wpisuje w historię. Normalnie bym dążyła, by Siluria nie była zarzewiem konfliktu... ale z drugiej strony, to nie do końca jest pod jej kontrolą."
* Żółw: "Dokładnie."
* Kić: "Oki. Czyli unlimited use sink na tą sesję."
* Żółw: "Tak. Ale musisz wygrać konflikt, czy powstrzymasz ten lincz mimo wszystko. Czyli: unlimited sink MASZ. A kontrola stanu napastniczki podlega konfliktowi. I możesz użyć tego sinka (kosztem RAGE) do wzmocnienia testu przeciwko linczowi jak nic lepszego nie wymyślisz ;-)"
* Kić: "Jeszcze mi powiedz jaki RAGE... chcę znać poziomy rage'a. Ile użyć to mild, ile to med, ile to big."
* Żółw: "Zaczyna się na '2'. Bo Siluria jest zaatakowana. Rozumiesz."
* Kić: "Mhm"
* Żółw: "Co '2' podnoszę kategorię. Kategorie to: mały gniew (nie lubią, zaciskają zęby), gniew (powiedzą coś niemiłego), wściekłość (małe plądrowanko), furia (nie-poważne obrażenia cielesne). Nie idź wyżej. Atm - mały gniew. Czyli 'bezpiecznych' masz... 4 użycia. Do plądrowanka ;-). Nawet 5 użyć - nadal plądrowanko."
* Żółw: "Przyszłą misję możesz próbować mitygować gniew KADEMu ;-)."
* Kić: "Jeszcze nie wiadomo, że zrobił to ktoś ze Świecy."
* Żółw: "Nieważne kto zrobił. Ważne, że Ignat nie lubi Świecy ;-)"
* Kić: "Taaaak..."
* Kić: "Czyli 5 od Silurii, 7 do pokonania."
* Żółw: "Poświęcenie nocy daje #1. Ukierunkowanie gniewu na arogancję to kolejne #1. Możesz jutro wydać surowce i iść spać ;p."
* Kić: "To by było na równo..."
* Żółw: "Misja ma 5 dni limitu czasowego, ale jest niskostawkowa."
* Kić: "Mam inny pomysł z tym spaniem. Przejdę się do Karoliny i dla odmiany ja ją poproszę o przysługę bez RAGE sinku - coś co mnie utrzyma. Stymulant, nie niszczycielski. Coś, co pozwoli mi podziałać."
* Żółw: "Taki... produktywny wieczór? I tak Ignat potrzebuje trochę czasu."
* Kić: "Aha, coś, by całkiem tego dnia nie spalić."
* Żółw: "Zrobimy tylko test stymulantu. Fort Silurii?"
* Kić: "Zero."
* Żółw: "Test z ułatwieniem 2."
* Kić: "Najpierw tamten."
* Żółw: "Jasne."
* Kić: "7 do 7. Nope :-(."
* Żółw: "Co, Ignat pobił jakiegoś niewinnego maga Świecy?"
* Kić: "Tak na dzień dobry?"
* Żółw: "KADEM agresor ;-)."
* Kić: "Z drugiej strony to nie jest takie straszne; Ignat jest ZNANYM KADEMagresorem... znowu porażka."
* Żółw: "To test na kanalizowanie agresji, prawda?"
* Kić: "Mhm."
* Żółw: "Co, jeśli przez to całkowicie rozsypią się kontakty Rojowca z Kropiakiem?"
* Kić: "Za duży koszt. To pozbawia Rojowca źródła dochodu i niszczy długo wypracowywaną relację a jest bez związku z tą sprawą."
* Żółw: "Jest powiązane - stamtąd właśnie wziął się artefakt w śmietniku."
* Kić: "No tak, ale Kropiak nie kontroluje komu sprzedaje te artefakty. Chociaż w sumie... to będzie potem dla Silurii zadanie odbudowania tej relacji, ale ona to może zrobić. Choćby udając się do Kropiaka."
* Żółw: "Ewentualnie możemy po prostu podnieść o 1 rage point."
* Kić: "To jeszcze wyższy koszt dla mnie. I tak będę strasznie starała się to ograniczać."
* Żółw: "Oki, Kropiak / Rojowiec czy kombinować dalej?"
* Kić: "Ciekawy punkt na odbudowę relacji. Bo Silurii będzie zależeć, bo Kropiak dostaje za niewinność. Potencjalna kolejna misja lub sprawa dla Silurii. Jak się rozpadło?"
* Żółw: "Zbyt gorące, zbyt niebezpieczne. Za dużo gniewu. Bezpieczniej rozpaść relację biznesową niż ryzykować, że coś podobnego się powtórzy."
* Kić: "Reroll. Teraz mam #1. I 20... nie mogło być wcześniej? 11 do 7."
* Żółw: "Skanalizowałaś gniew. Sprawdź jeszcze stymulant z ułatwieniem 2 a potem opiszę wynik."
* Kić: "Remis. Kostka mnie NIE lubi ostatnio."
* Żółw: "Na przykład relacja stymulantu z tym co miałaś... widać, że jesteś w gorszej formie. Malus do urody."
* Kić: "Niebezpieczne rozwiązanie; jeśli widać, wzmacnia gniew."
* Żółw: "Powiedziałbym inaczej; mamy próg '2' już w tym momencie. To jest przyczyna gniewu. Gwarantuję, że sam nie wzrośnie."
* Kić: "Ok, to się zgadzam."
* Żółw: "Zapisuję wydarzenia w świecie gry."
* Żółw: "Właśnie, masz pomysł jakiego niewinnego maga Świecy mógł pobić Ignat?"
* Kić: "Już szukam... to musi być facet, bo Ignat kobiety nie uderzy. ŻUPAN! :D Żupan dostanie wpierdziel! Spowoduje to mały outrage w Świecy, nic wielkiego."
* Żółw: "On ma coś na Mariana Łajdaka. W sumie, problem kolejnej sesji."
* Kić: "Może być to niewystarczające by wykorzystać, bo Ignat mu wklupał ;-). Najwyżej Marian przyjdzie z reklamacją do Ignata."
* Żółw: "Pomartwimy się na następnej misji w tym cyklu ;-)."
* Żółw: "Czekaj, Kić! Żupan w przyszłości zidentyfikował Ignata jako 'tajemniczego maga'. Sprawdziłem w jego rekordzie Dramatis Personae."
* Kić: "Cholera, racja... nowy mag?"
* Żółw: "Nowy mag. Street musician?"
* Kić: "Czemu nie. Jego wina - przebywanie w tym samym mieście z Ignatem ;-)."
* Żółw: "Nowy rekord w Personae: Oliwier Bonwant"
* Kić: "Ładne imię."

Sytuacja tak się rozwinęła - Siluria idąca na imprezę zaatakowana przez kogoś - że w świetle ogólnych wydarzeń na KADEMie nie zaczęło się poprawiać. Magowie normalnie lubiący Silurię zaczęli się dość denerwować. Siluria się zmartwiła; nie chciała, by jej napastniczka została zlinczowana. Oki, zaatakowała ją, ale w sumie chyba chciała dobrze... Siluria spróbowała uspokoić magów KADEMu i przekonać ich, by pozostawili jej - Silurii - prawo wyboru co się stanie. Oczywiście, wyszło jak zwykle. Ignat pobił Oliwiera Bonwanta (zupełnie niewinnego, acz Ignat przysięgał, że na pewno coś o tym wie... bo tak).

Silurii udało się w końcu skupić KADEM tak, by nie chcieli robić chaotycznych manewrów i przygotowywać się do wojny z całym światem. Okazało się, że atak na Silurię był tą kroplą, która przepełniła trochę czarę goryczy. Siluria zatrzymała min. Whisper przed zapolowaniem sobie dla sportu na jakiegoś maga. Zajęło jej to bardzo dużo czasu... ale się udało. By móc funkcjonować następnego dnia poszła do Karoliny po jakieś stymulanty; niestety, w połączeniu z tym, co Siluria już dostała od Anny (napastniczki) wcześniej sprawiło to, że uroda Silurii trochę ucierpiała do momentu porządnego wyspania się. Super...

### Dzień 2:

* Żółw: "Czyli wykorzystujesz swoje 2 surowce? I jakieś dodatkowe?"
* Kić: "Ignat szuka napastniczki. Drugi - wyciągnąć od Karoliny o co chodzi z Kotami. Czy my w ogóle robimy cokolwiek w pieprzonych Kotach. Ogólnie, WTF?! A Karolina jest najbardziej prawdopodobnym powodem."
* Żółw: "To nie surowiec, to konflikt. Szkoda surowca. Lepiej daj na narkotyk; tam jest coś wartego przebadania."
* Kić: "Narkotyk tak. Ale od Karoliny muszę się dowiedzieć o co chodzi."
* Żółw: "Powołaj kosztem surowca, może być rage, bo działają do końca misji persystentnie, Karolinę jako alianta. Możesz konfliktem."
* Kić: "Wolę konfliktem. Nie uda się, może być surowiec. Jeden surowiec idzie na identyfikację narkotyku."
* Żółw: "Test: Karolina, #1 za próbkę, impuls, ekspert, magia, biolab KADEMu (#2), inklinacja. 10. Jeszcze 2 za narzędzie magiczne. 12. Wow."
* Kić: "Poszalała dziewczyna..."
* Żółw: "Masz w kontaktach ekspertów z laboratorium magicznym..."
* Kić: "Mam. Starałam się. I KADEM i w ogóle ;p."
* Żółw: "Bij jak najwyżej."
* Kić: "Nope. 4, czyli -2. 10."
* Żółw: "Działamy na poziomie eksperckim, więc przekraczamy bardzo trudny. Masz coś dziwnego - próbkę MAGA w tym narkotyku. I info o samym narkotyku."
* Kić: "Próbkę... MAGA? Jak maga?"
* Żółw: "Sympatia. Ktoś używa sympatii do stworzenia narkotyku... najpewniej nieświadomie co to znaczy, gdyby dostał to defiler."
* Kić: "W sensie... nie próbka Silurii tylko JAKIEGOŚ maga?"
* Żółw: "Twórca narkotyku użył DNA i Wzoru jakiegoś maga do sformowania narkotyku. I ten narkotyk... on tak jakby sprzęga myśli osób biorących z magiem zawartym w tej próbce."
* Kić: "Taki do przesłuchiwań narkotyk?"
* Żółw: "Bardziej taki... do Pryzmatu. Osoby biorące ten narkotyk wzmacniają Pryzmat maga biorącego ten narkotyk. Sekta, system rojowy, w pigułce."
* Kić: "To ten narkotyk, o którym mówiła napastniczka, tak?"
* Żółw: "Tak."
* Kić: "Jestem PEWNA, że to nie Karolina. It is SO NOT HER. A ona sama też powinna być zaniepokojona tym tematem."
* Żółw: "Wygląda na bardzo zaniepokojoną. Aha, zapomniałem. Przy POBIEŻNEJ analizie wygląda to KADEMopodobnie. Ktoś nieudolnie wrabia KADEM."
* Kić: "Dobra... teraz to bardziej zrozumiałe."

Gdy Siluria się obudziła - ku swemu niezadowoleniu, sama - dostała wiadomości od Karoliny. Karolina powiedziała Silurii, że jakiś mag stworzył narkotyk na bazie Wzoru i DNA maga. Siluria facepalmowała; pierwszy defiler który to dostanie może wyrządzić straszliwe szkody. A co ten narkotyk robi? Sprzęga sterowanie odbiorców - osób biorących te narkotyki - z Wzorem maga, który jest w narkotyku zawarty. W wyniku, mag który jest zapisany w narkotyku ma możliwość silnego kontrolowania Pryzmatu. Ludzie biorący narkotyk wzmacniają moc tamtego maga przez Pryzmat. Jako minus... ten narkotyk strasznie uzależnia. I działa silnie na tematy erotyczne. Przenosi się min. przez transmisję płynów. Nie dość że jest narkotykiem to jest zaraźliwy i pobudza.

Ktoś - dość nieudolnie dla laboratorium - próbował zamaskować ten narkotyk jako działanie KADEMu. To wyjaśnia napastniczkę. Karolina jest głęboko wstrząśnięta, bo ten narkotyk stanowi antytezę wszystkiego, w co ona wierzy. Karolina zaoferowała się Silurii, że spróbuje zrekonstruować wzór tego maga. To da im jakieś informacje i możliwości. Siluria zaproponowała, że poprosi Mariana Łajdaka; w końcu nikt jak on nie zna półświatka. Karolina powiedziała, że co Siluria zrobi ma robić. Ona weźmie kilku magów KADEMu i spróbują dowiedzieć się, o co chodzi.

Tymczasem Ignat: Anna się dobrze ukryła. (5). Ignat ma: #1 (ślady) #1 (atak na JEGO Silurię i użycie surowców) i 3. 5 vs 5. (16). Udało mu się.

* Żółw: Oki, co chcesz od Ignata? Co dalej?
* Kić: Catch. Negotiate. Po prostu. Trochę odwrócić sytuację.
* Żółw: Cel znajduje się w Kotach.
* Kić: ...to be expected...
* Żółw: Ignat ma wejść i ją złapać dla Ciebie? Ma jej bazę i ona mu nie ucieknie, acz może stracić kilka kryjówek; wiesz, Ignat nie wie kim jest ale zawsze ją znajdzie.
* Kić: Jej złapanie ma duży sens... bo to co Siluria już wie wystarczy by przeprowadzić solidną rozmowę. Niech ją dopadnie.
* Żółw: solo? Może ona umie walczyć. Znaczy, Ignat... mag dewastacji. Więc...
* Kić: Wiesz co, nie... myślę o czymś innym. WIEM! Forma odwrócenia jej uwagi. Wiem, że jest przekonana że Siluria nic nie wie, ale jeśli po spotkaniu Siluria pojawi się w Kotach, wzbudzi jej zainteresowanie.
* Żółw: Intencja?
* Kić: Ja jako przynęta, on ma ją złapać. Dać Ignatowi bonus lub ją pozbawić bonusu. Z drugiej strony wiem, że tu gdzieś zapiernicza ktoś...
* Żółw: Powiem tak. Ignat i dyskrecja nie pasują do siebie. On pójdzie - znasz go - czystą dewastacją. Magia oblężnicza. Instytut Zniszczenia.
* Kić: ...w sumie tego nie chcemy...
* Żółw: KRATER NA SŁOŃCU!
* Kić: ...
* Kić: Ale Karolina była terminuską, nie? Może nie najlepszą, ale...
* Żółw: Popatrz na kartę; specjalizuje się w unieszkodliwianiu.
* Kić: Ignat ją namierza i nie pozwala uciec powstrzymując się od artylerii, Siluria odwraca uwagę, Karolina zdejmuje. To brzmi lepiej, zwłaszcza, że jest tu ktoś, kto próbuje wrobić KADEM.
* Żółw: Jest już wieczór.
* Kić: Następnego dnia, chociaż... nocna akcja? Siluria musi się pozbierać choć trochę. Ale... ale zrobię jeden mały konflikt z Łajdakiem. Z prostej przyczyny - on ma kontakty w wiadomych okolicach. Ktoś ten narkotyk musi rozprowadzać. Wiemy, że najprawdopodobniej nie jest to napastniczka Silurii. Natomiast może uda się poznać kanały. Mam dwa tory: napastniczka i właściwy problem. Łajdak jest proszony by namierzyć dealerów.
* Żółw: Ale to Łajdak musi użyć swojego surowca.
* Kić: Ja nie chcę używać surowca.
* Żółw: Zaczerpnij go z następnego dnia.
* Kić: No ok.
* Żółw: W ten sposób następnego dnia dowiesz się kto rozprowadza.
* Kić: Zakładając, że mu wyjdzie.
* Żółw: COŚ mu wyjdzie. Z niczym nie zostaniesz.

Ignat zlokalizował napastniczkę w Kotach. Znalazł jej bazę i dał znać Silurii. Ta poprosiła, by na nią poczekał (konflikt: Ignat impulsywny/rywalizacja, uliczny wojownik -> 3 vs 5 Silurii), by Karolina mogła przechwycić cel a Siluria robić jako dywersję. Ignat prychnął, ale zdecydował się posłuchać Silurii; nie zgubi jej. Chciał sam zanieść jej głowę Silurii, ale ta go przekonała, że Anna to płotka - problemem jest ktoś zupełnie inny.

Tymczasem Siluria poprosiła ślicznie Mariana Łajdaka, by ten zajął się namierzeniem grubej ryby. Kto rozprowadza te cholerne narkotyki? Marian skontaktował się ze swoimi kontaktami; jaki jest zakres tego cholerstwa i kto to rozprowadza? Zdobędzie dla niej odpowiedź.

Siluria dodatkowo udała się do Wioletty. Bardzo ładnie poprosiła (RAGE resource), by ta przygotowała broń przeciwko tajemniczemu magowi z narkotyku. Coś, co nie jest magią krwi a będzie w stanie...

* Żółw: Właśnie, co?
* Kić: Mam problem z określeniem. Co to ma być może zależeć od tego, czego dowiem się od napastniczki.
* Żółw: Czego Ci brakuje? W jaki sposób?
* Kić: Chcę mieć możliwość namierzenia maga który jest w tym narkotyku, czy to zaklęciem czy czymś innym. Mogę chcieć sprawić, że się nie wysypia czy coś...
* Żółw: Mam. Perversity. Co powiesz o odwróceniu sympatii by "gruba ryba" musiała tu przyjść usuwać ten narkotyk? Tu, do Kotów?
* Kić: Ok ^__^
* Żółw: Katalityczne wypaczenie narkotyku. Jeżeli sympatia idzie w jedną stronę...
* Kić: ...to idzie też w drugą.
* Żółw: Nawet squarkowane, jest powiązanie.
* Kić: To mi pozostawia jeden surowiec na przyszły dzień i podbija RAGE counter. To jest trzy, nie cztery. Swoją drogą: ja CHCĘ go podbić, by historia się dobrze trzymała, ale nie na bzdury.
* Żółw: Jasne, nie ma sprawy ;-).

Wioletta usłyszała pomysł Silurii, wyśmiała go i zaproponowała coś, co pamięta z czasów wojen KADEMu z AD. Wypaczenie energii magicznej. Są wciąż na KADEMie kataliści a takie działanie powinno raz na zawsze oduczyć tajemniczego narko-maga od robienia takich rzeczy. Siluria doceniła pomysł - jest bardzo w stylu KADEMu. Wioletta się uśmiechnęła szeroko i zaczęła polować na kilku magów KADEMu... miłe odprężenie a i opcja uciemiężenia jakiegoś maga spoza KADEMu. Bardzo w jej stylu.

### Dzień 3:

Siluria i Karolina udają się na łowy. Czas do Kotów. Tam w jednym z domków na wynajem zabunkrowała się tajemnicza napastniczka Silurii. Ignat - mistrz oblężeń - przygotował się by odciąć różne drogi ucieczki. Jak na Ignata, wyjątkowo po cichu. Ignat w cholernie złym humorze, bo się nie wyspał i zmarzł.

* Żółw: Jaki plan?
* Kić: Wyciągnąć ją - poczekać aż wyjdzie, zwrócić uwagę jak wyjdzie i pokazać jej, że Siluria tu jest... jeśli Ignat dał się wyrolować i jej tam nie ma to Ignat jest dupa wołowa i Siluria go zasłużenie wyśmieje. No chyba, że Karolina ma lepszą sugestię?
* Żółw: Ignat proponowałby bombardowanie.
* Kić: Ignat nie rozumie pojęcia 'cicho'.
* Żółw: Karolina... daj pomyśleć.
* Żółw: Karolina najpewniej by weszła, zapukała i jak jej otworzą drzwi to potraktowałaby cel jakimś eliksirem.
* Kić: Siluria jest przekonana, że przeciwnik nie otworzy drzwi bo będzie przekonany kto pod nimi stoi. Głupia kamera.
* Żółw: Karoliny nie zna. Ale Karolina nie jest taktykiem; kiepski z niej terminus. Z drugiej strony, z Twojej napastniczki też nie jest groźny mag bojowy...
* Kić: Tia...
* Żółw: Co więc proponujesz? Możemy to rozbić na grupę konfliktów.
* Kić: Zależy jak dużą realistką jest nasza przeciwniczka...
* Żółw: Podaj happy path.
* Kić: Moja pierwsza wizja: Ona opuszcza budynek, zostaje dorwana, obezwładniona, postawiona w tej samej sytuacji co Siluria... bo to najlepszy sposób by udowodnić, że jesteśmy po tej samej stronie.
* Żółw: Oki. Moment. Czyli czekamy aż opuszcza budynek. Czyli jakaś forma testu taktyki.
* Kić: Kiepsko. Ale Ignat za nią łazi, ma opracowany teren i jej nawyki.
* Żółw: Detektyw też w to wejdzie bo powinien łapać zachowania.
* Kić: Siluria jest manipulatorką; jeśli chcemy pewne zachowania, jest w stanie to zrobić.
* Kić: Jest jeszcze opcja numer dwa. Zakłada realistycznego / rozsądnego przeciwnika. Siluria podchodzi do drzwi kryjówki i puka. Ignat i Karolina obstawiają drogi ucieczki.
* Kić: ...w zasadzie w tą stronę. Jeśli otworzy drzwi i zdecyduje się porozmawiać, przyjmuje do wiadomości, że przegrała. A to też test na kumatość porywaczki... i nie musimy marnować czasu siedząc i patrząc na jej zamknięte drzwi ;-).
* Żółw: W tym wariancie happy path to...?
* Kić: Zacznie rozmawiać. Jak uciekać - zostanie złapana i wracamy do pierwszego.
* Żółw: Czyli w tą stronę idzie test. Skłonić ją do realistyczności, tak?
* Kić: Mhm. Trochę na zasadzie "oh shit they found me can't do much".
* Żółw: Oki. Nie ucieszę Cię. To test na Aggressive.
* Kić: Bardzo mnie nie ucieszysz.
* Żółw: Liczę. Bo to - zauważ - zastraszanie. Mamy Cię. KADEM już Cię dorwał. Współpracuj. Mam rację?
* Kić: Trochę tak...
* Żółw: Cel: 4 # 1 za przekonanie o Silurii i Siluriowości.
* Kić: 5 powiadasz... co masz na myśli?
* Żółw: W zasadzie niegroźna. Siluria raczej wzywa "bijcie mnie" a nie "bójcie się mnie".
* Kić: Mhm...
* Żółw: Masz #1 za KADEM. KADEM ma opinię potworów. #1 ekstra za to, że ją znalazłaś. Czyli masz #2 vs 5. Ty to...?
* Kić: 2#2. 4. Kiepsko. 4 vs 5.
* Żółw: Zawsze masz opcję że próbuje uciec i Ignat wali w nią z superciężkiej artylerii.
* Kić: Nie nie nie. Karolina tam TEŻ jest. I ona jest pierwszym celem...
* Żółw: To się zobaczy ;-). Rzucasz.
* Kić: 18. #2.
* Żółw: Bez sensu. Wyszło.

Ignat trochę narzekał, gdy Siluria powiedziała, że on i Karolina osłaniają potencjalną ucieczkę. Że Siluria pójdzie sama porozmawiać. No trudno, nie on dowodzi - obiecał, że będzie po Siluriowemu. I Siluria poszła zapukać do drzwi. Po chwili ciszy drzwi zostały otwarte. Tym razem napastniczka przebrała się za dziewczynę od pizzy w okularach przeciwsłonecznych i silnym makijażem.

* Kić: Jedno i drugie tak naprawdę na Silurię nie działa jak chodzi o kamuflaż.
* Żółw: To się nazywa 'test przeciwko maskowaniu czy aktorstwu'. Chcesz coś wiedzieć, konfliktuj. Ma fajną czapkę z daszkiem.
* Kić: Cieszę się...

Napastniczka zapytała nienaturalnie wytężonym głosem, o co Silurii chodzi i czy ona ją zna. 

* Żółw: Walczy do końca, tym co ma ;-)
* Kić: Szczerze? "Masz dwie opcje. Mogę się odwrócić i odejść, ale Ty nie zdążysz uciec. Albo porozmawiamy."
* Żółw: Czy to konflikt na zastraszanie? Nie rozumiem do końca intencji.
* Kić: Drop the stupid disguise. Start talking business.
* Żółw: Wygrałaś konflikt na to, że będzie rozmawiać. To jest poza dyskusją.
* Kić: Spoko. Ale nie chcę... nie będę rozmawiać ze sztucznymi wąsami. Po prostu.
* Żółw: Cholera, no nie wpadłem na sztuczne wąsy. A... pasują tutaj.
* Kić: No przykro mi... 
* Żółw: Oki. Masz konflikt na zastraszanie. JEŚLI wygrasz, będzie potulna. Przegrała. Jeśli przegrasz, będzie chciała rozmawiać, ale nie będzie chciała tracić prywatności.
* Kić: Myślę, czy tego inaczej nie postawić. Zastraszona już do pewnego stopnia jest.
* Żółw: NIE jest zastraszona. Jest przestraszona, ale nadal walczy. Chce rozmawiać, uważa to za najlepszą opcję, ale nie wyklucza ucieczki. Czuje, że jej grozisz. Stąd takie działanie. Boi się.
* Kić: Bardzo dobrze. Nie wiem czy nie pójść w tą stronę - to paradoksalnie najdziwniejsza próba zastraszania po friendly jaką mogę wymyśleć...
* Żółw: Nie widzę tego, Kić... zastraszanie po friendly?!
* Kić: Słuchaj, nie gadaj. "W tej chwili cały KADEM ma na Ciebie TAKIE zęby. Udało mi się to jakoś zmitygować. Dużo mnie to kosztowało. Nie uciekniesz a ja na to nie poradzę. To Twoja jedyna szansa. Z ledwością udało mi się ich powstrzymać." - i to niestety prawda. Jak teraz da nogę, KADEM ją znajdzie. I nie będą mili.
* Żółw: Już Ty się o to postarasz, by nie byli mili?
* Kić: Siluria nie. Ja tak.
* Żółw: Próbuję to jakoś zinterpretować. Dla mnie to jest aggressive, ale masz silny bonus. Ty nie dajesz jej dobrej rady. Ty ją zmuszasz.
* Kić: She started ;-)
* Żółw: Oki. Masz #3. KADEM, Dread, sytuacja. Walczysz z 5.
* Kić: #3, -1 Aggressive, Oblicze dla Każdego, Dominatorka. 6v5. Bardzo mam nadzieję, że wyjdzie.
* Żółw: Zawsze możesz eskalować.
* Kić: 7. Remis. 
* Żółw: Chcę pozostawić jej tożsamość w ukryciu. Akceptujesz?
* Kić: Nie. Nie chcę gadać ze sztucznym wąsem.
* Żółw: Z ciekawości: czemu?
* Kić: Może fajny fetysz, ale nieakceptowalny w tym wypadku. Mogę pójść na to, że przyzna Silurii kim jest a Siluria to dla siebie zachowa. Na to mogę pójść.
* Żółw: A możesz pójść na coś odwrotnego? Ona MYŚLI, że zachowała prywatność, a Ty się dowiesz kim ona jest? Pytam o poziom power play. Czy ona musi wiedzieć. O **to** pytam.
* Kić: Chcę, żeby wiedziała, że choć jedna osoba z KADEMu wie. Choćby dlatego, że jeśli wie, że ktoś z KADEMu wie, że to ona, to nie będzie mieć motywacji do tego, że załatwiła KADEMowców. Ochrona image KADEMu i zostawi KADEM w spokoju. No chyba, że... mam jedną kartę którą mogę rozegrać a dążę do jednej prostej rzeczy - I want to break her a bit, by jej pokazać, że powinna była przyjść pogadać z Silurią.
* Żółw: Dobrze. Widzę o co chodzi. Oddam tą tożsamość, nie o to mi chodzi.
* Kić: Nie przeszkadza mi to, że zniknie po tej misji i będzie nie do znalezienia przez KADEM.
* Żółw: Zróbmy tak dla spójności - you break her will, po prostu ją przestraszysz, ale wzbudzisz... gniew. You play with her.
* Kić: Chcę współpracować.
* Żółw: I będzie współpracować. Boi się. Ustępuje przed siłą KADEMu. Tyle.
* Kić: Jednym z impulsów Silurii jest dążenie, by była lubiana. Najważniejszym zadaniem Silurii jest bycie... swojego rodzaju punktem kontaktowym między magami i gildiami. O ile gniew pojedynczego maga nie jest szczególnie istotny, nie mogę sobie pozwolić na dużą ilość precedensów.
* Żółw: To inaczej. Nie gniew na Silurię. Na KADEM.
* Kić: Ok. Na KADEM mogę pójść. Chcę, by dotarło do niej, że wybrała jedną z najgorszych ścieżek rozwiązania sprawy.
* Żółw: To gwarantuję. Na pewno co KADEM nie zrobi, u niej ma od tej pory immunitet ;-).
* Kić: Chodzi mi o to, że jej podejście do Silurii było tragiczne.
* Żółw: To podzbiór tego co powiedziałem. Pokój przez tyranię. Powiem tak - będzie tak przestraszona KADEMem, że nie stanie przeciwko nikomu z KADEMu łącznie z Silurią z własnej woli.
* Kić: No wiadomo. Jak ktoś zmusi, to zmusi. W świecie magów to nic dziwnego.
* Żółw: Akceptujesz?
* Kić: Przestraszyła się KADEMu. Siluria jest członkiem KADEMu, ale nie chce jej skrzywdzić - i ona tak uważa. Nie podejmie walki z KADEMem z własnej woli. Pasuje.
* Żółw: Potwierdzam. Uda się zachować spójność historii.

Napastniczka pękła. Po prostu. Siluria aż widzi, jak oklapła. Nie ma już na głowie uciekania, po prostu patrzy na Silurię spode łba, fajnej czapki i przeciwsłonecznych okularów. Młoda dziewczyna. Stanęło na tym, że zrobi, co Siluria sobie życzy. Siluria wezwała Ignata i Karolinę, po co mają siedzieć w krzakach.

* Może wejdziemy i porozmawiamy? - Siluria
* ... - Anna się okręciła na pięcie i weszła do domku zostawiając otwarte drzwi.

Ignat rozczarowany. Nie mógł postrzelać. Karolina nie jest rozczarowana; jak na terminuskę wyjątkowo nie lubi przemocy ;-). Dziewczyna zaprowadziła Silurię do dużego pokoju i usiadła na krześle w kącie. Odruch obronny, choć totalnie bez znaczenia - Ignat może anihilować cały dom jednym zaklęciem oblężniczym.

Siluria powiedziała, że dość tej maskarady. Anna burknęła, że zaraz wróci. Poszła do łazienki zdjąć makijaż i się przebrać. Po kilkunastu minutach wróciła. Znowu założyła makijaż, ale inny - podkreślający młodość.

* Kić: Oh, please...
* Żółw: Nic nie ukrywa, zgodnie z parametrami...
* Kić: Tak tak, wzbudzanie litości...
* Żółw: Zgodne z parametrami? Jak nie, retconuję.
* Kić: Nie musisz. Wzbudzanie litości może próbować robić. Nice try.

Siluria powiedziała jej "nice try, ten makijaż Ci nie pomoże w podkreślaniu młodości". Nie z Silurią te numery. Acz - Siluria zauważyła z lekkim rozczarowaniem - że zadziałało i na Ignata i na Karolinę. Młoda wygląda teraz na 15 lat...

* Kić: Szczerze? W najgorszym wypadku znowu ich na nią wkurzę. Nie będzie trudno.
* Żółw: Cóż... nie trzeba. Ona się poddała, de facto. Po prostu próbuje ograniczyć konsekwencje.
* Kić: Mówiąc brutalnie przy takim nastawieniu KADEMu konsekwencje będą takie jak im pozwoli Siluria. 
* Żółw: Wiesz... ja wiem. Ona nie. Wygląda tak niegroźnie i wrażliwie. 
* Kić: Niewrażliwie my ass.
* Żółw: Ciekawe, czy płakać też umie na życzenie.
* Żółw: Twój ruch. Sama nic nie zrobi.

Siluria zaczęła pytania.

* Dlaczego interesowały Cię te narkotyki? - Siluria
* Chciałam... pomóc ludziom. - Anna, szeptem
* O... nietypowa motywacja jak na maga - zaskoczona Siluria
* Zaatakowałaś SILURIĘ by pomóc ludziom? Coś mi nie pasuje. - prychnął Ignat.
* ... - Anna, nic nie powiedziała. Nie chce się narażać.
* Nie wiem jak dokładnie sprawdzałaś informacje na mój temat, ale gdybyś zrobiła to naprawdę dokładnie, dowiedziałabyś się, że ja ludzi nie krzywdzę. - Siluria, z lekkim uśmiechem
* Nie mam dużo informacji - Anna, nadal nie chcąc się narażać. Skłamała.
* Doprawdy? Skąd wiedziałaś gdzie i kiedy będę? - Siluria, niezbyt zadowolona.
* Zastawiłam tą pułapkę trzy razy. Dwa razy nie trafiłam. - Anna, dla odmiany mówiąc prawdę.
* Dla Twojej informacji; do momentu naszej rozmowy nie miałam pojęcia o sprawie - Siluria.
* Gdy tylko się tego dowiedziałam, chciałam... się wycofać - Anna.
* Nie miało to nic wspólnego z tym, że ja przyszedłem, co? - Ignat, łypiąc wściekle.
* Nie zdążyłam przez to jej rozwiązać - znowu skłamała Anna (3 vs Ignat: 5)
* JAK MASZ ZAMIAR KŁAMAĆ TO ZAMKNIJ PYSK! - Ryknął Ignat i walnął pięściami w stół (zastraszanie: 4#2 vs def 3 -> #2)
* ... - Anna, zaczynająca się sypać i tracić kontrolę; w skrócie: płakać ze strachu NAPRAWDĘ.
* Może i wykonanie miała kiepskie, ale intencje dobre. Młoda jest... pamiętasz Elizawietę? - Siluria przejmująca kontrolę w rękawiczkach nad Ignatem; osłaniając Annę (kompensacja: 7v6->remis)

remis

* Żółw: Może... dziewczyna naprawdę, naprawdę nie znosi Ignata?
* Kić: Pasuje. Może go nienawidzić z całego serca. Jak długo widzi co Siluria robi.
* Żółw: Jak chodzi o mnie, może uważać Silurię za anioła. Metaforycznie.

powrót do rozmowy:

* ... - Anna zmieniła twarz w woskową maskę z ogniem w oczach. Jest przerażona. Nie będzie już kłamać.
* Zacznijmy więc od początku. - Siluria. - Czemu ja?
* ...jeśli ktoś wie, to pani będzie wiedziała. I mogłam wygrać... - Anna, cicho
* Skąd pomysł, że KADEM? - Siluria, spokojnie
* Wszystko na to wskazywało... sygnatura, Koty... - Anna, lekko śmielej
* Koty? Wskazują na KADEM? - naprawdę zdziwiona Karolina
* Nie Świeca... - Anna
* Są jeszcze inne gildie na tym terenie. Zakładasz, że to mag powiązany z jakąś gildią. - Siluria, spokojnie
* Jesteś ze Świecy - Ignat z pogardą. Widząc potwierdzające skinienie głowy - Wiedziałem... tienowaci
* Ty badałaś tą sygnaturę? - Siluria
* Nie... - Anna, znów cicho.
* Jak dobrej klasy osoba to analizowała? - Siluria, dążąca do tego, czy ktoś wrobił ją w KADEM
* Lepsza ode mnie... wiarygodna. Ale nie wiem jak dobra. - Anna, bezradnie
* Kiepska - Ignat.
* Nie DOŚĆ dobra - Siluria
* ... - Anna, nie wiedząc co powiedzieć
* Na pierwszy rzut oka to wyglądało na sprawę KADEMu. - Siluria, spokojnie - Wiesz chociaż, co robiło?
* Nie. Jakieś narkotyki. - Anna, cicho
* Znalazłaś tego, kto to rozprowadzał albo wytwórcę? - Siluria, drążąc, czy Anna ma cokolwiek przydatnego
* Tak... mam... dealera. Dwóch. To jakaś nowość, jakiś eksperyment. - Anna (4 vs 6/9 -> 7)
* Oj tak. - Siluria - A źródło?
* Od dilera do źródła... to jakieś małe, lokalne laboratorium - Anna, odzyskuje odrobinę nadziei
* Chciałam bardzo przeprosić... - Anna, do Silurii. Szczerze, ale i trochę nieszczerze; to taktyczne zagranie
* Weź sobie cukierka na uspokojenie - Karolina, nie do końca mogąc wytrzymać tą sytuację
* Nie przyszliśmy tutaj jeść cukierków - Siluria, zabierając Hedonistyczny Środek Psychoaktywny Karolinie i ją łagodnie upominając

a tymczasem:

* Kić: Cholerny BATMAG! To musi być Ania! Po prostu pasuje!
* Żółw: Teraz wiesz, czemu tak chroniłem tamten wątek. Ona zaatakowała w przyszłości poligon KADEMu
* Kić: Ale wyszło?
* Żółw: Wyszło idealnie. Mamy wszystko pogodzone.
* Kić: Zapytałabym się jak się do niej zwracać, ale Ignat... on to wykorzysta lub rozpowszechni... ech

wracając do dialogu:

Siluria jeszcze dopytała Annę o jej plan. Niestety, plan był... kiepski. Anna nie wie z czym walczy, nie wie jak do tego podejść... miała szanse powodzenia, ale za słabe. Mogła sama się załatwić. Owszem, najpewniej by się wycofała, acz zależy dużo od tego z czym mają tak naprawdę do czynienia. Ignat i Karolina wymienili spojrzenia. Anna po prostu nie jest taktykiem, jest młoda, nie ma doświadczenia... Siluria to jej klasa przeciwnika - ale tylko bojowo.

* Kić: Atakując Silurię pociągnęła DUŻEGO tygrysa za ogon
* Żółw: Tygrys przyszedł rozwalić narkotyki. Nie dzięki niej.
* Kić: Nope. Choć była... kind of useful i a lot of stupid.
* Żółw: Wiesz... mając to, co zrobiła dla Ciebie Wioletta tego maga możesz sobie tu ściągnąć.
* Kić: Złapię, zatargam na KADEM, przesłucham. Przydałby się terminus z KADEMu z prawdziwego zdarzenia. Karolina... nie. Ignat rozpierniczy w drobny mak.
* Żółw: Zależy od tego, co będzie robić to perversity. Wyobraź sobie coś, co pozbawia maga mocy. 
* Kić: Co robi ten narkotyk na ludzi? Poza uzależnieniem i indoktrynacją?
* Żółw: Niewiele; to sekta w pigułce. Rojowość. Jednomyślność. Dążenie do wymiany płynów ustrojowych - zaraźliwość.
* Kić: Symptomy odstawienia?
* Żółw: Złamanie jednomyślności. Cierpienie.
* Kić: Już mi się kształtuje powoli jakiś plan... jest w nim nawet rola dla dziewczyny.
* Żółw: Serio?
* Kić: Tak. Żółw, złapanie winowajcy, przesłuchanie itp to my. Ale ktoś musi ludzi odtruć ludzi. Jak najprościej ich odtruć? Porcja odtrutki jako narkotyk.
* Żółw: Zrobisz z niej dilera?
* Kić: Ona sprzeda to dilerom.
* Żółw: Nie doceniłem Cię.
* Kić: Mogłabym użyć Łajdaka, ale po co? Poza tym... nie jest zła z przebierankami a Siluria jej pomoże.
* Żółw: Po co pomoże? To ten efekt 'weź, nie jestem taka zła'?
* Kić: Trochę tak... poza tym jest jeszcze jedna rzecz; dilerom opchnąć to trzeba by myśleli, że dostali od stałego dostawcy. A że go chcemy usunąć... ktoś musi go udawać. I nie będę to ja.
* Żółw: Anna typowy dealer?
* Kić: Tak.
* Żółw: To co robimy z Anną? Masz ją tam zastraszoną.
* Kić: Dobrze...

Siluria powiedziała Annie parę słów. To nie KADEM za tym stoi, choć ktoś próbował na KADEM to zwalić. Anna zgubiła próbkę narkotyków i Karolina to przeanalizowała. Siluria zamierza znaleźć tego, co za tym stoi i dowiedzieć się, o co chodzi. Niemniej jednak - nie można pozbawić uzależnionych ludzi dostępu do tego narkotyku. Tu Karolina się odezwała. Ona może zrobić odtrutkę - coś, co działa stopniowo i neutralizuje to, co im dostarczono. Siluria się ucieszyła. I patrzy na Anię - jeśli Ania chce pomóc ludziom, to teraz ma okazję. Anna kiwa głową twierdząco. Siluria powiedziała, że trzeba to rozdystrybuować.

* Mam... być... dilerem? Odtrutki? - zdziwiona Anna
* Masz dostarczyć dilerom odtrutkę. - Siluria, spokojnie
* Nie jestem mentalistką... - Anna
* Całkiem nieźle sobie bez tego radzisz a ja mogę Ci w tym pomóc - Siluria, z uśmiechem
* Dobrze. Dziękuję. - Anna

...

* Kić: I tak jej samej nie puszczę na gadki z dilerami; to niebezpieczni ludzie
* Żółw: Jeszcze sobie zrobi krzywdę.
* Kić: chciałabym wiedzieć jak się do niej zwracać, ale przy Ignacie...
* Żółw: On się dowie. Wie jak wygląda. Jest detektywem. Zależy mu na Silurii.
* Kić: Fakt.

...

* Jakoś się do ciebie zwracać? - Siluria
* Anna - Anna Kozak, nie rozumiejąc dowcipu z "Anna Diakon".
* Anna... - Ignat aż się żachnął, rozumiejąc dowcip i podejrzewając, że młoda pyskuje.
* Tak mam na imię... - cicho, Ania, nie rozumiejąc
* Zajmiesz się odtrutką? - Siluria do Karoliny
* Oczywiście. Nie wiem, czy na jutro zdążę... to nie jest prosta sprawa, acz mogę Norberta poprosić. I Karinę. - Karolina, zastanawiając się usilnie
* Spytaj jeszcze Wiolettę o postęp. - Siluria do Karoliny
* Jasne. Nie ma sprawy. Cukierka? - Karolina, uśmiechając się szeroko

Siluria wpieprzyła się Ance z butami do mieszkania; wie, że ta nie będzie uciekać. KADEM ją namierzył. Nie ma ucieczki ;-). Wioletta dowie się tego czego ma się dowiedzieć; Karolina tam jest. Trzeba będzie jeden dzień poczekać, by dać czas Karolinie i Wioletcie.

* Kić: Jeszcze jedna rzecz do wyjaśnienia Ani; ten narkotyk poza byciem uzależniającym... nie musi tego wiedzieć, ale chcę jej pokazać, że przeoczyła coś krytycznego... jest środkiem wzmacniającym maga. Chociaż nie. Wait for it. Ona może się rzucić i mścić.
* Żółw: Wiesz, zależy, czy nie postawisz konfliktu, żeby się nie odważyła. 
* Kić: Postawię. To jest na takiej zasadzie: MY się tym zajmiemy. My, KADEM.
* Żółw: Przypomina mi się misja z poczciwymi szkieletami. Anna to taki poczciwy szkielet. W zasadzie niegroźna.
* Kić: Ale upierdliwa, no...
* Żółw: Ja tego nie skonfliktuję. Zbyt przerażona.

Anna dostała zadanie pilnować, czy nic dziwnego się nie dzieje. KADEM poszedł spać.

* Żółw: Szlachta śpi, plebs pracuje? ;-)
* Kić: Na pewno poprawi to humor Ignata, nie sądzisz?
* Żółw: Zdecydowanie tak.

### Dzień 4:

Ten dzień zaczął się dla Silurii naprawdę pozytywnie (poza tym, że ma jedynie 1 surowiec). Wpierw, wyspała się. W łóżku na KADEMie. I to nie w "swoim" łóżku, a w KADEMowym wewnętrznym hotelu na Primusie. Żeby nie musiała mieć do czynienia z ciągłą Fazą; każdy co jakiś czas potrzebuje odpoczynku od Fazy (lekarz zalecił, by nie musiała Stymulować po tych wszystkich środkach które miała w sobie).

Do Silurii rano przyszła Karolina, cała uśmiechnięta. Nie, żeby to była anomalia, jak chodzi o Karolinę. Powiedziała, że zebrała wszystkie informacje: ma dane od Mariana odnośnie tego jak to wygląda z tymi dealerami, ma Perversity od Wioletty i... nie ma odtrutki. Pracuje nad tym. Nie chce zrobić krzywdy; to potrwa parę dni, woli zrobić to DOBRZE. Karolina ma możliwość zastąpienia narkotyku bytem neutralnym, dużo bezpieczniejszym. Jest sfrustrowana - ten narkotyk był robiony przez kogoś, kto się zna na narkotykach ale niekoniecznie na magii; to powoduje, że squarkowanie jest niestabilne. To powoduje, że narkotyk sam się usunie z organizmu. Na plus, nie rozszerzy się to za mocno. No i nie czerpie energii z tych ludzi. Na minus... unsustainable. Daje to 'burst pryzmatyczny' - masz trudny czar do rzucenia, możesz doprowadzić do tego, by to się stało. Kosztem podchorowywania populacji. Zasięg jednej wioski czy coś.

* Kić: Czyli produkt Wioletty zniszczy efekt wzmocnienia? Jak go musimy rozdystrybuować?
* Żółw: Lepiej. Produkt Wioletty odwróci efekty narkotyku przeciw magowi.
* Kić: Co trzeba zrobić, by go użyć?
* Żółw: Dealer gangnam style. 
* Kić: O, to Anna się przyda. Ania się BARDZO przyda. Nie? ;-)
* Żółw: :-)
* Kić: Ej, jakąś robotę musi mieć. Nie będę za nią odwalać tego co chciała zrobić.
* Żółw: Trzeba jej zrobić test zdrowia :D.
* Kić: Bo?
* Żółw: Stres, Pryzmat, niedosypianie...
* Kić: Co za... SZCZUR! Jeszcze ją będę leczyć...
* Żółw: Wiesz, ma #1.
* Kić: Yay. A to za co?
* Żółw: Fort 0, Survival 1.
* Kić: 18. Przetrwała. Nawet może się przyda. Do czegokolwiek...
* Żółw: Twarda jest ;-).
* Kić: Jak na szczura.

Karolina połączyła ze sobą to, co zrobiła Wioletta (perversity) i substytut narkotyku, tworząc jeden środek. Łatwiej ludziom "sprzedać". 

* Kić: Zastanawiam się, czy mimo wszystko nie poprosić Mariana. Bo Anna może to spieprzyć.
* Żółw: Są na miejscu jeszcze ci lokalni dilerzy
* Kić: Ktoś musi wejść z nimi w kontakt. To jest większy kłopot.
* Żółw: Kto w okolicy ma dominujące cechy i skille socjalne, hmm...
* Kić: Siluria, ale ona nie chce się w to bawić. Jeszcze ktoś się potem do niej przywali, że robi coś z narkotykami.
* Żółw: Przyznaję, obiło mi się o płat czołowy, czy któryś tam ;-)
* Kić: By ją w to wrobić? Nope. Tough luck.
* Żółw: Surowiec dla Mariana? Niech jego kontakty to załatwią, jak co, on jest proxy?
* Kić: W sumie... tak. Znowu zero surowców.
* Żółw: Poza RAGE-owymi.
* Kić: Ja chcę złapać pachoła, który to rozprzestrzenił... rozprowadzenie tych narkotyków chwilę potrwa. Najwcześniej następnego dnia zadziała to na delikwenta.
* Żółw: Tak. Tak będzie.
* Kić: W takim razie, może tak... 
* Żółw: Mam pomysł! Użyję SkubnyNetwork.
* Kić: SKUBNY NETWORK? Pasuje :D. Przy okazji Hektor będzie się uganiał za dziwnym narkotykiem :D. Nikt tutaj nie dba o ukrywanie tego.
* Żółw: Koty.
* Kić: Tak...
* Żółw: Powiesz Millennium? To co tu się działo?
* Kić: Z grubsza w zarysie. Głównie po to, by kogoś nie przysłali. Coś w stylu 'we are clearing it', po prostu.
* Żółw: Oki. To też będzie konflikt; mimo wszystko Millennium Kotami się interesuje.
* Kić: Pójdzie w tą stronę, że ktoś coś kombinuje z KADEMem i KADEM to czyści. Troszeczkę pójdzie w stronę Silurii pełniącej swoją rolę - międzygildiowego facylitatora.
* Żółw: To otwiera jeszcze jedną opcję. Dilerzy Millennium.
* Kić: No w sumie... nie byłoby głupie.
* Żółw: Acz narko-tech KADEMu rozprowadzony przez Millennium... ciekawe politycznie. Wioletta może nie docenić.
* Kić: Szczerze... Tak, ciekawe politycznie. A jednocześnie coś, co... nie twierdzę, że Millennium nie jest w stanie tego przeanalizować, ale czy by powtórzyli? Po co?
* Żółw: Rafael Diakon. 'Po co' to złe pytanie.
* Kić: Tak, ja wiem. To jest Rafael.
* Żółw: Dlatego tu jest trochę ciekawszy wybór. Co powiesz, kiedy, komu i co ma z tego być.
* Kić: Jeśli im nie powiem nic, to co? Mają jakąś formę oczu w Kotach?
* Żółw: Nie. Świeca ich wyparła.
* Kić: Jakby mieli, trzy razy by znaleźli Ankę. No nie?
* Żółw: Tak. Widzę teraz takie proste opcje: SkubnyDil (Marian), MillenniumDil (Millennium), Anna...?
* Kić: Anna ma ten mały urok, że jak na Mariana ktoś patrzy, ona jest całkiem nieinteresująca. Dostanie jakąś formę ochrony i osłony. Siluria może jej pomóc z jakąś charakteryzacją...
* Żółw: Kusi Cię ta Anna, nie?
* Kić: Tak, strasznie. Choćby po to, by zrobiła coś, co się przyda.
* Żółw: Spojrzę do Dramatis... jest coś. Anna przyznała się Andrei (40 misji w przyszłości na oko), że Szlachta "coś na nią miała". Może to to? Na nią akurat Szlachta mogła mieć dużo różnych rzeczy.
* Kić: Wolałabym, by Siluria nie była pośrednio winna atakowi na KADEM. Ma to swój urok, ale. Kusi mnie, by to zrobiła, bo i tak jest zaangażowana... a najpewniej wywinie jakiś numer jak nic jej nie dam.
* Żółw: Wiesz, abstrahując od przyszłego ataku na KADEM, Anna może to szybko po TYCH dilerach rozprowadzić. Jeszcze przed godziną zero.
* Kić: I tak na efekty musimy poczekać do następnego dnia.
* Żółw: Będziesz babysitterem w escort queście. W sensie: Anny.
* Kić: Nie widzę Silurii jako eskorty. Prędzej Łajdak.
* Żółw: Ignat?
* Kić: Zbyt wrogi stosunek do dziewczyny. Łajdak zna tego typu ludzi, będzie lepszy.
* Żółw: I nie odpalasz SkubnyNetwork bo masz Annę ;-). Czyli konflikt z Marianem a nie surowiec.
* Kić: Czyli lepiej.

Siluria poszła do Mariana Łajdaka, ślicznie mu podziękować... i nie tylko. Marian przywitał Silurię z radosnym uśmiechem. Powiedział jej od razu co następuje:

* Ten narkotyk... nikt ze źródeł Mariana o tym nie wie. Nikt nie chodzi do Kotów; tam po prostu... nie opłacało się. Od czasu kralotha Koty są... dziwne pod względem narkotyków. Nie są dochodowe (co Mariana bardzo dziwi)
* Kontakty Mariana w Świecy nic nie wiedzą o tym narkotyku. To na pewno nie plan Świecy ani baronów Świecy. Marian podejrzewa jakiegoś łapczaka na boku.
* Tamci "dilerzy" o których Anna mówiła to takie płocie; dwa lokalne drągale bez perspektyw. Nie z Kotów. Z Glinu Żółtego. Nie są to typowi narkodilerzy. Nie mają wprawy.

...

* Kić: A co jakby im po prostu podmienić ładunek zamiast wchodzić w deal? Anna umie takie rzeczy.
* Żółw: Lower profile. Z osłoną na przykład Karoliny...
* Kić: To zmienia trochę plan. Jak to ludzie, to nie ma problemu. Martwiłam się o magów, ale tak, to jest to prostsze i mniej niebezpiecznie.
* Żółw: A jak wszystko pójdzie z planem, to tylko Anna jest lokalizowalna; KADEM nic oficjalnie nie wie.
* Kić: No! W tą stronę :D. Pytanie czy ostrzegać kogoś z Millennium.
* Żółw: PRZEDTEM czy POTEM?
* Kić: Przedtem nie widzę powodu. Siluria jest Diakonem na KADEMie, nie magiem Millennium na KADEMie. Nie jest szpiegiem. Nie dzieje się tu nic uzasadniającego rozmowę z Millennium na ten temat. Bo po co? A jak Millennium jest wypchnięte z terenu Kotów to najpewniej nic tu nie robią. A jak Millennium działa przeciw KADEMowi, oni powinni pierwsi pójść do Silurii. Więc... nie.
* Żółw: Proponowałem to by poszerzyć Ci potencjalne opcje, nie po to, by coś sugerować.
* Kić: Chwilowo to sprawa KADEMu. Millennium nie ma tu biznesu.
* Żółw: Tak więc będzie. Millennium się nie dowie przed upływem tygodnia, a będzie wtedy już po czasie.
* Kić: Łajdak nie jest już potrzebny. Jednak Karolina - ci dilerzy to szczury bez wiedzy o dilowaniu. A że nie chcemy wchodzić na ścieżkę dilerską... Łajdak spełnił swoją rolę.
* Żółw: Nadal masz więc swój surowiec.
* Kić: TAK! :D. Informacja dużo zmienia ;-).

Powrót Silurii do Karoliny. Wyjaśniła jej swój obecny tok rozumowania, co chce zrobić i jaka w tym jej rola. Karolina ma osłaniać Annę przed dwoma bysiami; jest w końcu terminuską. Karolina pokiwała głową z entuzjazmem. Pokazała Silurii swój KADEMowy Magitech (pancerz osobisty). Jest w stanie walczyć jeśli trzeba. Siluria powiedziała też Karolinie, że Anna ma wiedzieć że ma osłonę. Siluria powie Annie, że ta ma działać dyskretnie a nie agresywnie i w swoim stylu.

Siluria ma już wszystko co potrzebowała. Ma Karolinę, ma środki ("narkotyki"), ma informacje i wie, kto jest dealerem. Wsiadły w samochód i pojechały do Kotów, do Anny. A ta dzielnie czuwa na posterunku i monitoruje sytuację. Jest przemarznięta, ale zdrowa. Na lekkich stymulantach terminuskich (dostępnych uczennicy na terminusa).

Siluria zostawiła Karolinę i poprosiła Annę do wynajętego domu. Wyjaśnia jej plan - grubszy zarys. Dała Annie wytyczne: ma podmienić bysiom narkotyki na to, co Siluria jej dała. To wyciągnie "wrogiego maga" z ukrycia, zmusi go do przyjścia tutaj i do rozpoczęcia prac nad własną formą odtrutki, bo jego własny narkotyk będzie go krzywdził.

* Kić: Żółw, jedna ważna rzecz. Narkotyk odebrany bysiom ma być w CAŁOŚCI zdany. Nie po to, by KADEM go schował; po to, by Anna go nie miała.
* Żółw: Nie konfliktuję. Anna nadal jest zastraszona. Plus, to nie jest w jej stylu - ona nie robi rzeczy przeciwko ludziom.

Oki. Siluria siedzi i pije herbatkę, Karolina osłania Annę a Anna ma za zadanie podmienić narkotyki bysiom.

* Żółw: Tu będą konflikty.
* Kić: Wiem. Siluria pomoże Annie w przygotowaniach jeśli ta chce się bawić w kamuflaże i inne cholery.
* Żółw: Muszę spojrzeć na skillset. Zasadzka, nikt jej nie zauważy... ma coś. Bysie złapały gumę a Anna spróbuje im to popodmieniać?
* Kić: Można też... do czego Anna nie przywykła... bo wiesz nad czym myślę? Jak oni są w aucie... biedna, piękna kobieta złapała gumę na drodze. Siluria.
* Żółw: Prostsze. Skuteczniejsze. Ale pokazuje Silurię i zmusza ją do działania. Ty odciągasz uwagę, Karolina osłania, Anna podmienia.
* Kić: Ślad do Silurii wciąż jest znikomy, zwłaszcza z odpowiednią charakteryzacją.
* Żółw: To tak naprawdę pytanie, czy Siluria chce w to wchodzić.
* Kić: To osłoni Annę, a ona to dzieciuch. Nie chcę jej ZBYT narażać. Masz jakieś większe implikacje, które za tym idą?
* Żółw: Nie. Myślę. W najgorszym NAJGORSZYM wypadku jest link między Anną a Silurią, ale bardzo słaby.
* Kić: Jeśli ryzykujemy, że ślad znajdzie Millennium... fine. Świeca? Less fine, ale jest mag Świecy bardziej zamotany. A KADEM _jest_ zaatakowany wizerunkowo.
* Żółw: To połączenie taktyczne, nie polityczne. Jeśli Ci to nie przeszkadza w najgorszym wypadku, to ok. Przeciwnik nie ma silnych powiązań politycznych. Gdyby miał...
* Kić: Nie bawiłby się w coś takiego.
* Żółw: I nie w Kotach.

Siluria łapie gumę na trasie bysiów. Anna ma za zadanie się przekraść i podmienić narkotyki. Karolina osłania. Siluria się przygotowuje tak, by nie prowokować do natarczywości ale by wzbudzać potrzebę pomocy i zaimponowania. Żeby skupiać dla siebie uwagę, by być magnesem dla wzroku.

* Żółw: Oki, działamy. Jesteś na czas, Anna dała wyprzedzenie. Złapałaś gumę zgodnie z planem.
* Kić: Wyjmuję z bagażnika elementy do wymiany koła, rozrzucam je jakby ktoś nieporadnie próbował coś zrobić, po czym macham na bysiów.
* Żółw: Konflikt. Walczysz z... -1 (inklinacja), 0 (skill), 4 (zaklęcie)...
* Kić: ZAKLĘCIE? Wow.
* Żółw: Czyli walczysz z 3. Ach nie, 2. Impuls. Też obniżony.
* Kić: Oblicze dla każdego, pasuję tu. 3. Devious #1. 4. Bezpośrednio pasujący impuls... nie widzę.
* Żółw: Dodam tutaj im jeszcze #1. Za krótko Cię widzą.
* Kić: Spoko. Zawsze mogę podnieść stawkę. 4v3. Fail XD.
* Żółw: Oki, zabawne.
* Kić: Pfft. Wychodzę im na środek drogi. MUSZĄ się zatrzymać.
* Żółw: XD. Oki. Teraz masz 5v4. Bo Ciebie zobaczą, ale są wkurzeni. Bo wyszłaś na drogę. Zatrzymali się - pytanie, co dalej.
* Kić: 14. Jak to co? Są moi.
* Żółw: Point. Wysiedli więc z auta - to taki tani, duży osobowy. Idą cali wkurzeni; zamkną drzwi do auta, to ważne.
* Kić: Karolina jest technomantką?
* Żółw: Nope.
* Kić: Ojoj, nie mam istotnego klucza by to koło wymienić :-(. A potem ich rozproszę, by nie zamknęli drzwi.
* Kić: Troszkę ignoruję, że są wkurzeni. Dziękuję, że się zatrzymali; są kochani, wielcy i w ogóle. Trochę nawijam patrząc jak oni na to reagują. Tyle mnie samochodów minęło i nikt nie chciał się zatrzymać... i trochę idę w kierunku na bezradność. Ja PRÓBOWAŁAM sama, ale coś jest nie tak, ja nie umiem!
* Żółw: Intencja?
* Kić: Troszkę ich rozluźnić; pokazać, że głupia laska sobie nie radzi z wymianą koła a ładna to warto pomóc. Podlewam ich adekwatną dawką podziwu.
* Żółw: Dobra. Jako, że ich dwóch a Ty masz tu sporo do odwracania...
* Kić: Mam CZYM odwracać :p
* Żółw: ...to liczę stopień trudności testu na... trudny zawodowy. 5.
* Kić: Oblicze, Devious, rozbrajająco urocza, #1 za przygotowania i podkreślenie atrybutów, #1 za wiarygodną historyjkę i zachowanie 
* Żółw: Czyli masz... 7?
* Kić: Tak. 3. Remis.
* Żółw: Zależy Ci na tym surowcu? Mogą Ci portfel ukraść. Dam za to bonus Annie, #1.
* Kić: Wpisuje się w historię głupiej idiotki.
* Żółw: Oki, są w pełni zajęci Tobą... i portfelem. Tymczasem Anna wlezie im niezauważenie i zacznie podmieniać. To będzie... Daredevil # podkładanie śladów # Nimble # 1 za bonus # 1 za odwrócenie uwagi = 6. Też trudny zawodowy. Vs 5. 
* Kić: Porażka. Eskalujemy.
* Kić: Zobaczenie Anny nie wchodzi w grę; wtedy cała gra idzie w cholerę.
* Żółw: Anna musiała użyć zaklęcia? Będą ślady magiczne - Anny. Tylko Anny.
* Kić: Na przykład nie otworzyli bagażnika i Anna nie mogła otworzyć, bo zrobiłoby to hałas.
* Żółw: Tak, coś takiego. Jako scavenger ma tam jakieś zaklęcia tego typu. Oki, reroll.
* Kić: 14. Sukces, poszło.
* Żółw: Excellent. Operacja się udała perfekcyjnie. Narkotyki podmienione, Siluria (bez portfela) ma zmienione koło, WIESZ o tym, że byli pod wpływem magii, odjechali. Tylko czekać, aż wrogi mag wypełznie z nory.
* Kić: Tak. 
* Żółw: A Ty nie masz surowca.
* Kić: Oh well, wychodzę na zero tego dnia. Czyli co, następny dzień, tak?
* Żółw: Możemy przerotować; nie wiem, co jeszcze można by tu zrobić. Anna na zwiadzie...
* Kić: Nie. Ma się kiedyś wyspać. Siluria nie jest aż taką sadystką.
* Żółw: Kto pilnuje? Ten mag może wyjść wcześniej.
* Kić: Karolina się wyspała. 
* Żółw: To niech Anna zostanie w tym wynajętym domku a Karolina zwiaduje Koty. Wrogi mag pojawi się wieczorem.
* Kić: Chcę ściągnąć Ignata. Będziem bić. Ignat i Karolina powinni wystarczyć.
* Żółw: Zapomniałaś o Annie.
* Kić: Ona się liczy jako minus pół...

Oki. Wieczór. Siluria na KADEMie, Karolina i Anna na miejscu. Ignat gotowy do akcji, na siłowni (w halach symulacji KADEMu). Wtem - Siluria dostaje wiadomość. Jest czarodziejka z uszkodzonym Wzorem (Perversity). Jej Wzór tak się uszkodził dzięki Wioletcie, że aż Karolina rozpoznała w niej czarodziejkę.

* Kić: To... musiało boleć. Wiemy kto to?
* Żółw: Nie. Ale w tej chwili jest biedna.
* Kić: LUBIMY biedne czarodziejki. Im bardziej biedne, tym lepsze.
* Kić: Czyli: Ignat i Siluria jadą. Karolina i Anna mają czekać. Biedna, słaba magicznie i w ogóle?
* Żółw: Tia. Karolina informuje, że tamta próbuje wpływać na "swoich ludzi", ale Perversity to uniemożliwia. Nie ma żadnej władzy.
* Kić: Oh, so cute...
* Żółw: ...to serio misja typu 'poczciwy szkielet', nie?
* Kić: Ano ;-).
* Kić: W zasadzie - trzeba ją po prostu zgarnąć z ulicy, zabunkrować i przesłuchać. Nie wierzę, że Wioletta nie będzie chciała zobaczyć wzoru osoby po JEJ zabiegach. Ale to potem.

Siluria i Ignat dotarli na miejsce. W ciągu tego pół godziny wroga czarodziejka zabunkrowała się w jednym budynku z dwoma ludźmi, którzy - jak Anna wie - są pod wpływem narkotyku... a dokładniej, Perversity. Zgodnie z emanacją aury, na pewno tam jest i próbuje dojść do tego co do cholery się stało. Nie ma większych szans. Choć... test: 7 (z magią) vs 10 -> remis na 20. Fail. No chance. No hope.

Oki. Zespół w komplecie. Drzwi zamknięte. Anka włamywacz je otworzy ;-). 3 vs 3. Sukces; otworzyła drzwi w formie niezauważonej. Weszli do środka, w sposób niezauważony. 

* Kić: Wersja optymistyczne. Ignat służy jako zabezpieczenie. Karolina łapie i przytrzymuje czarodziejkę. Siluria ją wiąże.
* Żółw: To będzie trudny konflikt... Ignat nie rzuca, Karolina... nie rzuca, Siluria nie rzuca.
* Kić: Ok O_O.

No cóż. Zespół złapał czarodziejkę. 

* Kić: Eeej!
* Żółw: No co? Nie poradzę... no tak to zrobiłaś, że nic nie mogłem zrobić.
* Kić: Ok
* Żółw: Mam jeszcze asa w rękawie... ale to walet trefl.
* Żółw: Wroga czarodziejka próbuje stawiać opór...
* Kić: So cute. Ignat, przeszukaj ją. Czy nie ma czegoś czym może się skrzywdzić. A Ignat się nie wstydzi przeszukiwać kobiet.
* Żółw: Cóż. Nie ma. Scientist-type, nie warrior-type. 40-50 lat, osłabiona...
* Kić: Obecnie nic dziwnego
* Żółw: Nigdy o niej nie słyszałaś, czyli jakiś plebs.
* Kić: Wygląda jakby rozpoznawała KOGOKOLWIEK?
* Żółw: Nie konfliktuję; nie.
* Kić: ...aż nie wiem co zrobić...
* Żółw: Happy path? Co byś chciała?
* Kić: Siluria nie jest magiem bojowym. Najpewniej by ją przesłuchała tu, na miejscu. Ignat i Karolina są bardziej bojowi... w bazie wroga to niekoniecznie rozsądne.
* Żółw: Jest trigger na ewakuację; jeśli chcesz o coś zapytać, warto tu kilka pytań strzelić. Ignat może skanować perimeter, Karolina pilnować Ciebie.
* Kić: Anna może pilnować okolicy. Nie musi słyszeć pytań i odpowiedzi; not her business. Plus... ona jest dobra w wypatrywaniu zagrożeń.
* Żółw: Nie konfliktuję. Jasne. Czyli co 'sio zmykaj, pilnuj by nikt nie wzazł'?
* Kić: Nie tymi słowami, ale niech pilnuje czy przeciwnik nie ma wsparcia. Ignat też może... artyleryjski mag w budynku to...
* Kić: Jakbym była wredna, nakarmiłabym ją cukierkiem od Karoliny. Serio.
* Żółw: Oki, masz więc przestraszoną i związaną czarodziejkę. Ignat i Anna są na patrolu. Karolina próbuje wyglądać groźnie. Nie idzie jej.
* Kić: Nie musi.
* Żółw: Ma magitech? 
* Kić: Mhm. Niech tylko hełm założy by się nie uśmiechać. Plus, nie wiemy co kobieta ma przygotowane.
* Żółw: Oki. Co zatem chcesz osiągnąć? TAK, będę konfliktował.
* Kić: Siluria nigdy nie przesłuchuje agresywnie... scary thought.
* Żółw: Jeśli chcesz agresję, co nie jest głupie, zamień miejscami Ignata i Karolinę. Ignat składa się z agresji.
* Kić: Oki, tak zrobię. Mam teraz 'dobry glina / zły glina'.
* Żółw: I jedną wynędzniałą czarodziejkę.
* Kić: W takim razie, Żółw, pierwsze pytanie...

...

* Kim jesteś? - Siluria zaczyna spokojnie.
* TIEN Urszula Murczyk. A Ty? - Urszula, próbując ogarnąć myśli / Kić: nie pozwolę jej na ogarnięcie myśli.
* Obawiam się, że nie jesteś w pozycji do zadawania pytań. Co tutaj robisz? - Siluria, zimno
* Eksperyment. Kim ty jesteś? Jakim PRAWEM atakujesz maga Świecy? - Urszula, zaczynająca się denerwować (gniew)
* Czy ty ZDAJESZ sobie sprawę co zrobiłaś? - Siluria, zimno
* Ty tępa, tienowata szmato! Wycierasz sobie mordę KADEMem i chcesz, by gildie stanęły znowu w ogniu?! - Ignat, taktowny jak zawsze (4v0). Ula się zamknęła.
* ... - Urszulę zatkało.
* Nie wiem kto ci zlecił to działanie, ale takie sekrety się długo nie uchowują a próba zrobienia tego i zrzucenie winy na inną gildię kwalifikuje się pod sąd - Siluria, bardzo zimno - Albo zrobiłaś coś bardzo głupiego, albo ktoś cię poświęca. / Żółw: jak mam na to odpowiedzieć? Kić: czy to był jej pomysł? Kto to spreparował? Czemu KADEM? Czemu w ten sposób? Co to za eksperyment? Żółw: Podejmuję próbę 'stall for time'. Ma... 1. Cholera. Nie ma szans.
* Mając do wyboru te opcje... coś głupiego. - Urszula, zbierając myśli - nie miałam pojęcia jakie są konsekwencje zostawienia śladu KADEMu. / Kić: seriously? Żółw: to nie jest skonfliktowane. Mówi prawdę. Omijam tylko to, co spowoduje trigger żeby dać Ci czas na nieskonfliktowane dowiedzenie się.
* Ten eksperyment miał sprawdzić, czy da się wzmocnić zaklęcia gdy to jest potrzebne, przy minimalnych kosztach - Urszula, przestraszona
* Jutro się kończył... nikt by nic nie wiedział... - Urszula, wciąż w szoku
* A jednak... - Siluria, z przekąsem / Kić: nie powiedziała o zlecenie. Żółw: zapomniała. Tu dookoła jest trigger. Kić: uderzenie we wzór jest poważne? Długo się będzie leczyć? Żółw: 2 tygodnie. Kić: a jak pomożemy? Żółw: to krócej. Kić: To oficjalnie teren Świecy. Czemu sekret? To bezpieczne pytanie? Żółw: tak.
* Dlaczego zachowałaś to w sekrecie? - Siluria
* Miałam móc pokazać, że potrafię ukrywać różne rzeczy. - Urszula, ostrożnie / Żółw: tu dookoła tych powodów jest trigger.
* Co Ty z tego masz? - Siluria, z ciekawością
* Będę mogła wreszcie zobaczyć swojego męża i dzieci - zrezygnowana Urszula
* Co się z nimi stało? - Siluria, ze zwróconą uwagą
* Zaćmienie. Mnie zmieniło. Ich nie. - Urszula, z kamienną twarzą.
* Innymi słowy! - Ignat, nie ukrywający pogardy - byłaś człowiekiem. Zostałaś magiem. I żeby odzyskać swoją rodzinę... teraz... robisz takie RZECZY innym ludziom takim jak Twój mąż i dzieci? Gnida...
* Jak zabiorą TWOJEGO męża i dzieci to wtedy porozmawiamy. - Urszula, zimno, do Ignata - Zrobiłabym dla nich wszystko. To moja RODZINA.
* Już nie - Ignat - Teraz to tylko ludzie.
* Do końca świata. - Urszula. - Do końca cholernego czasu.
* ... - Siluria, której po raz pierwszy od dawna zabrakło słów. / Kić: nie skasowali jej pamięci?! Nie usunęli pamięci o rodzinie?! Mam cholerną ochotę zabrać ją na KADEM, na Fazę. Żółw: Po co? Czemu na Fazę? Kić: bo tu coś się bawiło z Pryzmatem... a Faza mogłaby to ujawnić. Żółw: mogę odpalić trigger? Kić: Szczerze? Siluria spyta kto jej to zlecił i co ma zrobić. Wiem jak się nazywa, mogę ją zawsze monitorować i obserwować...
* Zostałaś poświęcona. Kto ci to zlecił? Kto powiedział, żebyś zamaskowała to KADEMem? Co chciałaś zrobić? - Siluria, trochę ostrożniej.
* Nikt mi nie kazał. - Urszula, z determinacją / Żółw: mówi prawdę - To była jedyna opcja, bym mogła odzyskać rodzinę. Nie cała Świeca akceptuje aktualny stan rzeczy...

Flara Srebrnej Świecy. Terminus w Kotach.

* Kić: bardzo dobrze. Od tego jest Karolina.
* Żółw: w sensie? Przecież Karolina nie będzie walczyć z terminusem Świecy
* Kić: zamieni trzy słowa i dowie się, o co chodzi. Tu Siluria nie zrobiła nic złego; mamy prawo tu być. Czy ona sugeruje, że jest grupa magów Świecy, którzy chcą łamać Maskaradę? O_O
* Żółw: nie. Ona mówi, że jest grupa magów Świecy nie zgadzających się ze statusem quo.
* Kić: szczerze? No shit, Sherlock. Ale jak to się ma do możliwości spotkania z jej rodziną? To nie ma sensu.
* Żółw: wrócę do dialogu.

...

* W tej chwili zasada jest taka, że mag nie może utrzymywać kontaktu z ludzką rodziną - Urszula, wściekła
* Wiesz dlaczego? - Siluria, spokojnie
* Kontrola. Odcięcie. Izolacja. - Urszula.
* Jeśli tak chcesz na to patrzeć. Ale również ochrona, obu stron. - Siluria.
* Jak?
* Wyobraź sobie, że zostałaś terminusem. Jak prościej cię zaszantażować? - Siluria, pytająco
* Nie jestem terminusem. Nie chciałam nawet być magiem! - Urszula, wybuchając
* Ale się nim stałaś. I nikt na to nie poradzi - Siluria
* Właśnie zobaczymy. To chciałam zmienić. - Urszula, wściekle
* Nie chcesz być magiem? Pierwszy sposób jest ostateczny. Drugi to przemiana w viciniusa. W żadnej z tych opcji nie zostajesz człowiekiem. - Siluria, uspokajająco
* Tienowaci nawet nie umieli jej skasować pamięci. - Ignat, z totalną pogardą - po to są te wszystkie programy. Żeby takie rzeczy się nie działy.
* NIE! Nigdy! To MOJA tożsamość! - Urszula / Kić: nikt się jej nie powinien pytać o zdanie... nie z tym charakterem... nie z tym podejściem... coś bardzo, bardzo zawiodło. Żółw: mogę tu wprowadzić terminusa? Kić: Chcę powtórzyć pytanie, skąd pomysł by wrobić KADEM. Żółw: na to dostaniesz odpowiedź, zamknę TEN wątek dla Ciebie.
* Skąd pomysł, żeby wrobić KADEM? - Siluria, zmieniając temat i wiedząc, że Karolina nie zatrzyma terminusa.
* Pytałam jak mogę ukryć ślady. Zaproponowano mi kilka gildii. Między innymi KADEM. Były jakieś artefakty, jest jakiś mag który przekształca te rzeczy w maskowanie. Jeśli dobrze zrozumiałam. Miałam pomoc. Pomysł, że KADEM był mój... po prostu były artefakty, z których można było maskować. - Urszula, z desperacją
* Cholerne Kropiaktorium sprawia problemy... - Ignat, zaskoczony, do Silurii / Żółw: dlatego Rojowiec x Kropiak się rozpadło na tej misji. Nie wiem, czy wróci. Kić: no... nie wiem, czy w ogóle wróci Kropiak x KADEM w tym świetle.

Wszedł terminus Świecy. Zastaje scenę... interesującą ;-).

* Dziękuję, lady Diakon. - rozluźniony terminus, z uśmiechem - Jej szukam.
* Z kim mam przyjemność? - zaskoczona Siluria
* Rukoliusz Bankierz - terminus się przedstawił - Świeca. Ta czarodziejka zrobiła kilka rzeczy, których nie powinna robić...
* Zgadzam się. Została również potraktowana... przeciwśrodkiem na jej działania. - Siluria, z lekkim uśmiechem - To nie był rozsądny pomysł ani rozsądna metoda.
* Wyczułem. - uśmiechnął się terminus. Nie ma nic do KADEMu, wyraźnie - Czy KADEM będzie chciał jakichś reperacji? Chciałem przeprosić w imieniu Świecy za... użycie imienia KADEMu nadaremno.

...

* Żółw: Kiciuś, ten terminus jest miły. Zbyt miły. To na pewno jest terminus, ale... tu jest Ignat. Jak często terminus Świecy się PRZYZNAJE?
* Kić: Tak... czyli jest tu prawdopodobnie coś więcej.
* Żółw: Z uwagi na wielochronologię mogę zdjąć kawałek welonu, jeśli chcesz.
* Kić: To tylko znaczy, że jest tu coś WIĘCEJ.
* Żółw: Tak. Ale to nie wpływa w żaden sposób na żadną Twoją postać do najnowszej misji w kampanii Karradraela. Nic co tu się nie stanie. Choć jeden element może naruszyć chronologię, ale ten monitoruję; jak się nie pojawi, to nie powiem.
* Kić: Spoko.
* Kić: Jak Siluria sobie czegoś nie zażyczy, to Ignat zrobi jej z dupy jesień średniowiecza.
* Żółw: Prawda. Terminus wygląda, jakby CHCIAŁ dać KADEMowi reparacje. Serio. Żeby KADEM nie mógł się przypieprzać... albo coś innego. To wymaga konfliktu.
* Kić: Actually... mam pomysł na konflikt z nim. Jak mu było na imię?
* Żółw: RUKOLIUSZ
* Kić: Rukoliusz... Jezu... rozumiem, że Bankierze aspirują do Diakonów jak chodzi o imiona... tak czy inaczej, kierunek w którym chcę iść: KADEM zrobił wszystko (odkrył, złapał...) i tu jest terminus który powinien był to zrobić. Chcę od niego wydusić... po pierwsze, skąd wiedział. Po drugie, co jest jego celem tutaj? Paradoksalnie w celach reparacji mogę zażyczyć sobie, by poszła na KADEM w celach leczenia.
* Żółw: To będzie straszna eskalacja polityczna, by poszła na KADEM na zmarnowanie i wycieńczenie.
* Kić: ...już jest wycieńczona... najpierw chcę poznać jego motywację, jego powody.
* Żółw: Muszę powiedzieć Tobie coś więcej, bo nie będziesz w stanie nawigować tego konfliktu. Muszę odsłonić część welonu. Ok?
* Kić: Mhm.
* Żółw: To mag Szlachty. Ale Siluria nie może usłyszeć o Szlachcie. Teraz rozumiesz?
* Kić: Mhm. Czyli ona też jest ze Szlachty?
* Żółw: Szlachta znalazła okazję na rekrutację czarodziejki do Szlachty. Ale do tego celu musi mieć na nią haka. I czarodziejka sama się wpakowała. Sama kojarzysz Annę, potem Dianę...
* Kić: No wiem. Widzę. Dobrze... jeśli jest zbyt miły... mam trochę problem z konfliktem.
* Żółw: Twój happy path?
* Kić: Nie wiem. Urszula nie ląduje w Szlachcie? Ale do tego celu Siluria musiałaby być po Spustoszeniu. A to jest wcześniej... niemożliwe.
* Żółw: To inaczej: pasuje Ci... dodanie do PRZYSZŁOŚCI wątku wyciągnięcia jej w przyszłości?
* Kić: Jeśli powiązane, że z rodziną nie do końca ma to szanse się udać.
* Żółw: Infensa 2.0? Nie mówię o implementacji, mówię o rekonstrukcji osoby
* Kić: Chrzanić rekonstrukcję osoby... gdyby nie pamiętała, że ma rodzinę, problem byłby rozwiązany. Ale to na przyszłość.
* Kić: Paradoksalnie Szlachta może sama zabrać jej pamięć; ma własne dążenia i motywacje, które mogą być sprzeczne z celami Szlachty. A oni są bezwzględni.
* Żółw: Nie wiem, czy w tej chwili jest cokolwiek, co możesz zrobić.
* Kić: Chcę surowców. Takich konkretnych. Zwrot kosztów, badań, całej tej akcji...
* Żółw: Named, nowy sink, unnamed stack? W jakiej formie? O czym myślisz? I dla KADEMu czy dla Ciebie?
* Kić: Szczerze? Named dla KADEMu i Sink dla Silurii. A koszta tej akcji nie były małe.
* Żółw: Im większy koszt, tym większa moc Szlachty. Szlachta gra na rozbicie Świecy. Im więcej dostanie KADEM, Siluria, nieważne, tym bardziej wizerunkowo KADEM i Szlachta współpracują. Stąd problemy z Tamarą, pamiętasz. Z Kurtyną. KADEM i Kurtyna się zwalczały, bo KADEM wykorzystywał Świecę. I w to właśnie gra Szlachta. W czym - cokolwiek nie zrobisz - zachowuje to spójność chronologii.
* Kić: Rukoliusz chce coś dać Silurii... i będzie to wina Urszuli.
* Żółw: Tak.
* Kić: Kusi mnie coś, na co ten gość nie pójdzie. A nie chcę tego robić, bo to uderzy w Urszulę wyjątkowo mocno. Chciałabym, by Świeca przeprosiła KADEM oficjalnie. Przyznanie się, że Świeca nie była w stanie ogarnąć swoich magów.
* Żółw: Ma to zaletę. Redukuje gniew KADEMu. To by zneutralizowało rzeczy, które zrobiła Anna.
* Kić: Jak długo nie wpływa to na wyciąganie Urszuli ze Szlachty...
* Żółw: Urszula to long game. Ona stanie się dostępna dopiero PO Karradraelu. Patrząc, co zrobiłaś z Izą... każdego możesz przekształcić.
* Kić: Ja, lub Wioletta. Lub Millennium.
* Żółw: Dokładnie. Więc konsekwencji terminalnych nie będzie.
* Kić: Takie przeprosiny oficjalne ze wstążkami i ciastem... byłoby to najlepsze.
* Żółw: Uspokoi KADEM, osłabi Świecę, ale nie aż tak, osłoni Annę
* Kić: Której ten terminus jeszcze jej nie zauważył na razie.
* Żółw: Zależy Ci na tym?
* Kić: Tak. To nie jest ten moment, kiedy Szlachta widzi Annę, po prostu.
* Żółw: Nie konfliktuję. Oki. Czyli mamy rozwiązanie.
* Kić: Tak. Siluria żąda oficjalnych przeprosin. Jemu się to nie do końca spodoba.
* Żółw: No to konflikt. Wolałby działać dyskretnie, dać KADEMowi coś, jak przekupstwo. Jawnie, to trochę utrudnia Szlachcie. Ale ma rozkaz doprowadzić do zgody. Więc... 3. To jest po friendly, nie devious.
* Kić: Dostaję to o co proszę, friendly, warto robić sojusze. 5 v 3. A argumentacja "odwaliliśmy całą robotę i się wykosztowaliśmy a Świeca zgarnia śmietankę. A bez oficjalnego uznania stwierdzą, że to wina KADEMu."
* Żółw: Promuję. 6v3.
* Kić: 10. Sukces.
* Żółw: Gratuluję.

Rukoliusz zabrał ze sobą Urszulę i obiecał zrobić porządek w Kotach. Siluria powiedziała, że KADEM bierze detoksyfikację na siebie. Rukoliusz się zgodził; posprzątanie zostało rozwiązane. A Urszula... dostała swoją lekcję. Gdy terminus poszedł w cholerę, Siluria ściągnęła Karolinę, Ignata i Annę. Anna jest cała blada.

* Coś się stało? - Siluria do Anny
* Nic, nic - skłamała Anna. 
* No przecież widzę, że coś. - Siluria, nieagresywnie.
* Młoda... - Ignat zaczął. Anna się lekko skuliła.
* Nie, po prostu... ona miała rodzinę. - Anna
* Wiem. Praktycznie każdy mag który stał się magiem po Zaćmieniu miał rodzinę. - Siluria, wyjaśniająco
* Ja nie wiem, czy miałam rodzinę. - Anna, rozżalona
* No i tak powinno być - Ignat, spokojnie.
* Jakkolwiek to nie brzmi smutno, posłuchaj... nieważne, czy jesteś terminusem, katalistką, tworzysz zabawki... jeśli masz rodzinę wśród ludzi o której wiesz i na której Ci zależy, jeśli ktoś będzie chciał Cię wykorzystać... Ci ludzie stają się celem. Są najprostszym sposobem dotarcia do Ciebie. A zrobisz dla nich wszystko. Rozumiesz? - Siluria, uspokajająco
* To nie jest sprawiedliwe. - Anna, z maską na twarzy - To po prostu nie jest sprawiedliwe.
* Nie, nie jest... - Siluria

# Progresja

* Anna Kozak: Przestraszyła się KADEMu. Siluria jest członkiem KADEMu, ale nie chce skrzywdzić Anny. Nie podejmie walki z KADEMem z własnej woli.
* Anna Kozak: Nienawidzi Ignata Zajcewa (z KADEMu) z całego swojego serca.
* Urszula Murczyk: Ma problemy w Świecy przez to, co zrobiła (KADEM, eksperymenty w Kotach). Spychana do Szlachty.
* Urszula Murczyk: Dwa tygodnie ciężkiego chorowania przez Perversity jej własnego narkotyku - robota Wioletty.

## Frakcji

* KADEM: dostał oficjalne przeprosiny; nastroje na KADEMie się poprawiły

# Streszczenie

Anna Kozak porywa Silurię, by dowiedzieć się, skąd KADEMowe narkotyki w Kotach. Ta daje się porwać by dowiedzieć się o co chodzi i też jest zdziwiona. "Uratowana" przez Ignata, kontynuuje śledztwo Anny i dopada ją właśnie w Kotach, studząc nastroje na KADEMie. Rozerwało się Kropiaktorium - KADEM, Ignat pobił niewinnego Oliwiera, a Siluria z Karoliną odkrywają, że faktycznie - w Kotach... są narkotyki. Świeca. Maskuje jako KADEM. Wioletta wypacza wzór Urszuli stojącej za narkotykami i Siluria łapie ją, odkrywając, że Urszula chce dołączyć do stowarzyszenia w Świecy pozwalającego magom na kontakty z ludzką rodziną. Terminus Świecy zatrzymuje przesłuchanie, Siluria żąda przeprosin od Świecy dla KADEMu.

# Zasługi

* mag: Siluria Diakon, rozwiązała wszystkie problemy swojej porywaczki, zastraszyła ową porywaczkę, wykorzystała moc KADEMu przeciw mniej kompetentnym magom Świecy i wymusiła przeprosiny, zarządzając gniewem KADEMu.
* mag: Anna Kozak, głupio porwała Silurię. Złapana. Przerażona, współpracuje jak może. A tylko chciała pomagać ludziom...
* mag: Ignat Zajcew, zastraszający tienowatych na lewo i prawo, pobił niewinnego Oliwiera, detektyw odnajdujący Annę. MVP.
* mag: Karolina Kupiec, pokazała różnicę między NIĄ a DILERAMI. Terminuska o dobrym sercu i dużych umiejętnościach badawczych narkotyków.
* mag: Marian Łajdak, robi rozpoznanie w świecie przestępczym dla Silurii - skąd wziął się "KADEMowy narkotyk". Źródło wiedzy.
* mag: Wioletta Lemona-Chang, proponująca bardzo chore Perversity narkotyku magicznego Urszuli. Dla niej frajda. Dla Urszuli mniej.
* mag: Oliwier Bonwant, muzyk uliczny i mag Świecy, któremu z frustracji na Świecę nakopał Ignat. Dostał - serio - za niewinność.
* mag: Urszula Murczyk, Świeca, badaczka narkotyków skłonna poświęcić innych ludzi by odzyskać prawo do kontaktów z ludzką rodziną. Grawituje do Szlachty. Skończyła uciemiężona przez Wiolettę i Silurię.
* mag: Rukoliusz Bankierz, Świeca, terminus Szlachty przepraszający Silurię i obiecujący przeprosiny od Świecy dla KADEMu. Osłonił Urszulę zanim ta wydała istnienie Szlachty.

# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Centrum
                           1. KADEM Primus
                               1. Wewnętrzny krąg
                                   1. Hotel wewnętrzny, gdzie czasem śpią magowie KADEMu by móc odpocząć od Fazy Daemonica. Padło na Silurię ;-).
                        1. Cyberklub Działo Plazmowe, do którego chciała dotrzeć Siluria na tańce. I nie wyszło, bo została porwana przez Annę.
                        1. Kino V, nieczynne; Anna przesłuchiwała w jego podziemiach Silurię a potem zwiewała stąd przed Ignatem
            1. Powiat Czelimiński
                1. Koty, które są wyjątkowo niedochodowe dla dilerów narkotyków; coś tu nie działa jak powinno; pod kontrolą Świecy... acz Millennium ma chrapkę.
                    1. Dwie hałdy
                        1. Domki na wynajem, gdzie wpierw zabunkrowała się Anna Kozak szukająca Urszuli a potem Urszula Murczyk szukająca odtrutki.
         
# Czas

* Opóźnienie: 4
* Dni: 4
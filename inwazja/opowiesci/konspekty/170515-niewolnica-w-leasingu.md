---
layout: inwazja-konspekt
title:  "Niewolnica w leasingu"
campaign: rezydentka-krukowa
players: kić
gm: żółw
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [170510 - Najgorsze love story (PT)](170510-najgorsze-love-story.html)

### Chronologiczna

* [170510 - Najgorsze love story (PT)](170510-najgorsze-love-story.html)

## Kontekst ogólny sytuacji

Wyjdzie z misji

## Punkt zerowy:

Ż: Do czego Paulinie mogłaby się przydać Diana?
K: Urządzenia napędzane prądem pomagające w diagnostykach. Też, zamaskowane dzięki iluzjom. I tanie energetycznie.
Ż: Do czego Paulinie mógłby się przydać Hektor?
K: Coś, co utrzyma jej ogródek w stanie znośnym, zwłaszcza w obszarach z magicznymi roślinami.

## Misja właściwa:

**Dzień 1:**

Startowe tory (wygenerowane losowo ze wpływem wygenerowanym losowo):

* "Jestem dobrą panią": +1
* "To wszystko nie ma sensu": +2
* "Faceci są bez sensu": +0

Czwarta rano. Paulinę budzi straszliwe wycie pralki. Chwilę potem padają bezpieczniki. Paulina spełza szybko na dół. Pralka jest wyraźnie pod wpływem jakiejś formy energii magicznej; pralka jest gorąca i działa bardzo mocno i szybko. Słychać zgrzyt metalu. Paulina zdecydowała się szybko rozproszyć energię:

Postać (strona gracza):

| .I_Strat. | .I_Takt. | .Specka. | .Umiejka. | .Umiejka Mag. | .Inklinacja. | .POSTAĆ. |
|     0     |    1     |    1     |     0     |      1        |      0       |     3    |

Okoliczności:

|  .Sytuacja. | .Sprzęt. | .Skala. | .Magia. | .Surowiec. |  .OKOLICZNOŚCI. |
|     0       |    0     |    0    |    1    |     0      |        1        |

Przeciwnik (strona NPC):

| .VERSUS. | .Diff. | .Wpływ akcji gracza. | .OPOZYCJA. |
|    2     |    2   |          0           |     4      |

Wynik:

| .Postać. | .Opozycja. | .Test?. | .Rzut.       | .Wynik. |
|    4     |      4     |   -1    | 13: +1 W0    |   S     |

Pralka zajęczała żałośnie, po czym zaczęła powoli przestawać działać. Paulina czuje, że źródło mocy jest osłabione, ale nie zniszczone. Źródło mocy jest w bębnie. Pralka przestała definitywnie działać. Paulina widzi, że źródło mocy wygasło. Najpewniej przeszło w stan hibernacji czy coś. To nie jest zaklęcie, to jakieś rezyduum; tam coś jest.

Paulina otworzyła pralkę i zajrzała do bębna. Tylko kurz. Pył. Ale ten pył... jest tym co Paulinę zaintrygowało. Badanie pyłu. Paulina zdecydowała się wziąć to ostrożniej i zająć się badaniami dokładniej - więcej czasu i sprzęt do badań magicznych w domu.

| .I_Strat. | .I_Takt. | .Specka. | .Umiejka. | .Umiejka Mag. | .Inklinacja. | .POSTAĆ. |
|     0     |    0     |    1     |     1     |      1        |      1       |     4    |

Okoliczności:

|  .Sytuacja. | .Sprzęt. | .Skala. | .Magia. | .Surowiec. |  .OKOLICZNOŚCI. |
|     1       |    1     |    0    |    1    |     0      |        3        |

Przeciwnik (strona NPC):

| .VERSUS. | .Diff. | .Wpływ akcji gracza. | .OPOZYCJA. |
|    2     |    3   |          0           |     5      |

Wynik:

| .Postać. | .Opozycja. | .Test?. | .Rzut.       | .Wynik. |
|    7     |      5     |    2    |  6: -1 W0    |   S     |

Ten pył to w rzeczywistości jest... żywy. To jakaś forma rośliny. To jest taki... żywy barwnik mający nadać konkretne cechy strojowi - odporność na chłód (stąd to ciepło) i zwiększyć energię generowaną magiczną; taki chlorofil, ale do magii. Po wypraniu liberii te rośliny się popsuły. Podtrute, przekształciły się. To co poodpadało z liberii zostało w pralce i zaczęło dalej pełnić swoją funkcję. Stąd ta reakcja...

* Co się stało? Czy to było... normalne? - zaskoczony Hektor do Pauliny
* Nie. Mam pralkę do wymiany - nieszczęśliwa Paulina
* Jakiś... zły czar? - Hektor, nie rozumiejąc
* Nie. Ciuchy Twojej dziewczyny - Paulina, kąśliwie

Hektor zaczął się zapierać, że Diana nie jest jego dziewczyną. Paulina machnęła ręką. Po chwili, spojrzała na Hektora sceptycznie. Poprosiła, by on to wytępił magią rolniczą. Jakieś pestycydy czy coś. Ona się nie zna. Hektor się skwapliwie zgodził. Zajęło mu to chwilę, ale z katalityczną pomocą Pauliny wyniszczył rośliny. Nie ma sensu, by tu były.

* "To ja tu rządzę": +1; Hektor uważa, że Ewelina jest jakaś chora i straszna. Czemu Dianie nie powiedziała? To się mogło wydarzyć...

Hektor zdecydował się pójść spać. Paulina, niezbyt szczęśliwa z KOLEJNEJ przerwanej nocy, poszła spać...

Ranek. Punkt o 9:00 przyjechała Diana. Nie potrzeba psychologa, by zobaczyć, że jest zestresowana. Diana dała Paulinie list, pisany na papeterii odręcznie przez sekretarza Eweliny. List ten głosi, że Ewelina cieszy się, że Diana się przydała Paulinie. Ubolewa, że przez Dianę Paulina mogła ponieść pewne straty, więc wypożycza Dianę na tydzień. Ale nie może nic się Dianie stać.

Diana wypytana powiedziała, że Ewelina dała Paulinie nadrzędność nad wszystkimi INNYMI rozkazami (poza sprzecznymi z rozkazami Eweliny). Diana musi słuchać wszystkich rozkazów magów "na dworze" Pauliny. Może odmówić. Pytanie, czy to zrobi. Oddając Paulinie list, weszła w jej zasięg pola osobistego - Paulina zobaczyła, że Diana jest pod ciągłym zaklęciem iluzji utrzymywanym przez siebie...

I Paulina wie, po odpytaniu Diany, że ta nie jest w swojej liberii; w końcu pralka ją zniszczyła.

* Jak masz tu zostać, przebierz się w coś mniej rzucającego się w oczy, co? - Paulina nie udaje, że jest szczególnie szczęśliwa
* Dobrze; z przyjemnością. - Diana TEŻ nie udaje, że jest szczęśliwa
* Możesz się przebrać u mnie. - Paulina pokazała palcem na pokój.

Po 10 minutach Diana wróciła. Przebrana jest w taki normalny ludzki strój - bez ŻADNYCH nadruków. Powiedziała, że spróbuje coś zrobić z tą pralką, jeśli jest w stanie. Paulina jej pozwoliła. Diana poszła do łazienki.

Hektor przyszedł do Pauliny. Powiedział, że wygląda, że wszystko rozeszło się po kościach. On się cieszy - nie chciałby robić kłopotów. Paulina powiedziała, że może zostać aż się zregeneruje, ale będzie spał na kanapie. Hektor się bardzo ucieszył, że będzie blisko Diany, acz próbował nie pokazać tego po sobie. Paulina lekko facepalmowała...

Dziś po południu Paulina ma bieg. Tzn. będzie biegać z ludźmi, po to, by ustalić stan społeczności; czy poziom zdrowia jest OK, czy nie ma jakiegoś Skażenia, czy nie ma na nich jakichś fal magicznych... tzw. "Bieg Czarnego Kruka". Sporo ludzi się schodzi, też z innych miast, jest szansa na wykrycie jakichś mocy magicznych.

Paulina zapowiedziała "swoim" magom, że ma dzisiaj bieg. Oni na nią popatrzyli z takim lekkim niezrozumieniem. Gdy Paulina wyjaśniła, że idzie pobiegać 3 km, Hektor zaoferował się, że on też by pobiegł. Paulina się uśmiechnęła. Spytała Dianę jaki jest stan pralki - Diana odpowiedziała, że kilka rzeczy się uszkodziło i trzeba je wymienić; ale ona nie umie bez magii. Paulina powiedziała, że wezwie mechanika. Ona naprawdę nie chce nadmiaru magii w domu (-1 surowiec: wezwanie mechanika).

Paulina zaproponowała Dianie, że jeśli chce, może pobiec. Jeśli nie chce, nie musi. Diana zaczęła się wahać; Hektor powiedział jej, żeby poszła - to będzie fajne. Przy nadmiarze sygnałów "chodź", Diana uznała, że z uwagi na polecenia Eweliny lepiej będzie pobiec. Choć lekko niechętnie.

Paulina zdecydowała się nie przydzielać nikomu zadań; jeśli będą chcieli, to się udzielą. Wyjęła coś z zamrażarki. Obiad będzie szybki...

Hektor od razu wpadł na pomysł. On ma pewne nasiona; są to dziwne magiczne zioła, które kiedyś dostał. Część z tego nadaje się na różne leki, ale wymaga odpowiednich warunków katalitycznych, wodnych itp. Zaproponował, że zasadzi Paulinie gdzieś tu w ogródku te zioła. Do tego potrzebne będą technomantycznie zmodyfikowane zraszacze które też katalitycznie przekazują odpowiednio energię magiczną. No i iluzje i odstraszacze.

Paulinie pomysł się spodobał. Dianie też (nie ma innego wyjścia, ale pomysł jej się podoba). Paulinie TYM BARDZIEJ ten pomysł się spodobał - Kić dodała nowy tor "Hektor - ogrodnik Pauliny" nad którym będzie walczyła ;-).

Paulina zdecydowała się obserwować tą dwójkę przy pracy, przygotowując obiad. Też pasywnie magicznie. 

Postać (strona gracza):

| .I_Strat. | .I_Takt. | .Specka. | .Umiejka. | .Umiejka Mag. | .Inklinacja. | .POSTAĆ. |
|     1     |     1    |    0     |     1     |       0       |       1      |     4    |

Okoliczności:

|  .Sytuacja. | .Sprzęt. | .Skala. | .Magia. | .Surowiec. |  .OKOLICZNOŚCI. |
|      2      |    0     |    0    |    1    |     0      |        3        |

Przeciwnik (strona NPC):

| .VERSUS. | .Diff. | .Wpływ akcji gracza. | .OPOZYCJA.   |
|    2     |   ?    |           0          | 2-7 (target) |

Wynik:

| .Postać. | .Opozycja. | .Test?. | .Rzut.       | .Wynik. |
|    7     |     7      |    0    | +3 Wx (xx)   |  S (10) |

Paulina nie dba o miłość między nimi, ale chce sprawdzić, czy się lubią. I tak, zdecydowanie się lubią. To jest dwustronne. On jest bardziej niezależny, on jest praworządny, ale autonomiczny. Chce żyć po swojemu i jest zainteresowany Dianą w tym życiu ;-). Diana z drugiej strony jest bardziej nieśmiała, jest zahukana, ale NIE zastraszona. Nie do końca wie, czego chce. Nie do końca przeszkadza jej los u Eweliny Bankierz. Co jemu bardzo przeszkadza.

* "Nie musimy być sami": tor +3; sytuacja ich zdecydowanie zbliżyła do siebie.

Paulina zauważyła, że Diana ledwo podtrzymuje tą swoją iluzję. Kontrolowanie światła, padanie cieni... najpewniej nie do końca sobie z tym radzi. Więc Paulina poprosiła na chwilę do siebie Dianę.

* Tak, słucham? - uśmiechnięta Diana
* Zrzuć tą iluzję - Paulina poważnie.
* Nie. To jest... - Diana orientując się, że jest w sprzeczności z rozkazami
* Męczysz się. Nie musisz przede mną udawać. - Paulina, spokojnie
* Dobrze... - Diana, dość cicho i strzelając oczami na boki

Diana nie jest ubrana. W ogóle. Paulina westchnęła i wzięła ją ze sobą. Poszukała po szafie, ale Diana powiedziała, że nie wolno jej niczego założyć. Zniszczyła liberię, nie wolno jej nosić INNEJ liberii. Poprosiła Paulinę, czy jednak może rzucić iluzję... bo jest jej bardzo niekomfortowo. Paulina dała jej Quarki. Niech sobie to utrwali (-1 surowiec). Diana skupiła się i rzuciła zaklęcie. Po chwili uśmiechnęła się blado do Pauliny.

* Lady Ewelina ma... specyficzne poczucie humoru - Diana, wymuszonie lekkim głosem
* Nie nazwałbym tego poczuciem humoru - Paulina, chłodno
* Wolno mi na nią narzekać, teraz. Więc nic złego mi się nie stanie - Diana, pocieszająco
* Super... - Paulina.
* Naprawdę się na to godzisz?! - Paulina, wybuchając - To jest niezrozumiałe!

Diana powiedziała Paulinie, że zrobiła kiedyś błędy. Pomogła swojej rodzinie (niemagicznej). Kupiła im pozycję. Wpadła w poważne problemy i Ewelina ją wyciągnęła. Kosztem Diany. Diana należy do Eweliny. Ale przynajmniej alternatywa - przestępcza, krzywda rodziny Diany... te rzeczy się nie zmaterializowały. Diana dla swojej rodziny uważa, że podjęła właściwą decyzję. Ona jest wdzięczna lady Ewelinie.

Paulina pozwoliła jej założyć iluzje...

* "To ja tu rządzę": +1: Ewelina bawi się Dianą i wszystkim. Ona decyduje, dzieje się jak ona chce.
* "Jestem dobrą panią" +1: Co nie powiedzieć, Ewelina zaopiekowała się jakoś Dianą. I nie jest jej wroga.

Paulina zauważyła tą obróżkę dookoła szyi Diany. Jest ładna, elegancka... ale to obróżka. Złożony artefakt; Paulina nie do końca wie, co on robi. Ale wie, że na pewno zapewnia Dianie jakieś energetyczne pole siłowe, by ta nie zrobiła sobie fizycznie krzywdy - np. "buty".

Diana wróciła do pracy z Hektorem. Przynajmniej teraz się nie męczy utrzymywaniem iluzji.

Paulina zrobiła jedzenie. Gdy zbliża się bieg, powiedziała Hektorowi i Dianie, że jeśli chcą biegać - to teraz jest ten moment, że muszą się ogarnąć. Hektor jest upaprany. Po prostu. Diana być może też, ale przez iluzję tego nie widać. Nawet Hektor się zorientował, że Diana ma na sobie iluzję.

Gdy Diana poszła się wykąpać, Hektor zagadał Paulinę. Spytał, czy ona jest pod wpływem iluzji. Paulina potwierdziła. Na pytanie Hektora "czemu", odpowiedziała "bo uważa, że powinna". Dodała szybko, że nie jest pobita i nic jej fizycznie nie jest. Hektor powiedział, że nie rozumie, ale przestał pytać.

Wykąpali się. I pojechali na bieg.

Paulina przed biegiem bierze narzędzia detekcyjne (-1 surowiec; z następnego dnia). Wzięła też swoją torbę lekarską, którą zostawiła osobom towarzyszącym. Jeszcze przed biegiem wyszukała jednego faceta (Michała Furczona), który ma spory ogród i przedstawiła mu Hektora jako znajomego, specjalistę od ogrodnictwa. Ktoś, kto może pomóc. Hektor się ucieszył, on lubi solidną pracę. Furczon też się ucieszył; przyda mu się (odpłatna) pomoc.

Zostawiła ich by sie dogadali, sama poszła monitorować ludzi i stan ludzi.

Postać (strona gracza):

| .I_Strat. | .I_Takt. | .Specka. | .Umiejka. | .Umiejka Mag. | .Inklinacja. | .POSTAĆ. |
|     1     |     1    |    1     |     1     |       1       |      1       |     6    |

Okoliczności:

|  .Sytuacja. | .Sprzęt. | .Skala. | .Magia. | .Surowiec. |  .OKOLICZNOŚCI. |
|      2      |    1     |   -4    |    1    |     0      |        0        |

Przeciwnik (strona NPC):

| .VERSUS. | .Diff. | .Wpływ akcji gracza. | .OPOZYCJA. |
|    2     |   2    |          0           |     4      |

Wynik:

| .Postać. | .Opozycja. | .Okoliczności. | .Rzut.    | .Wynik. |
|    6     |     4      |      0         | 10: -1 W0 |   S     |

Paulina puściła szeroki monitor. Biegnie i bada. Próbuje używać mniejszej energii, żeby w wypadku srebra nie krzywdzić, co oznacza, że musi podbiec do większej ilości ludzi - i tyle. Zaklęcie pokazało jednoznacznie, że kilka osób miało do czynienia z energią magiczną w niedawnym czasie, ale nikt nie jest Skażony czy nie ma poważnych problemów magicznych. Norma. Nic złego się nie dzieje.

Bieg się skończył. On dobiegł bez większych kłopotów. Diana jednak... z wielkim trudem dobiegła. Nie umie dobrze ustawić oddychania, jak ma biec... ogólnie, dostała w kość. Pomogło to, że Hektor się z nią zrównał i jej mówił, jak to robić. Ale się udało. Z trudem, ale się udało. Po czym - Hektor złapał Dianę za przedramiona i pogratulował, też pogłaskał mocno po plecach...

...orientując się, że nie ma na sobie niczego...

Paulina zauważyła, że Hektor ma NAPRAWDĘ dziwną minę. Diana spłoniła się jak rak.

Czas wracać do domu. Wsiedli do samochodu. Dianę Paulina wzięła do przodu. Hektor siedzi z tyłu. W samochodzie ani Hektor ani Diana nie powiedzieli ani słowa aż Paulina zaczęła gadać. Że bieg sympatyczny, można było monitorować stan ludzi...

* ...I nikt nie robi mi rytuałów na moim terenie. - Paulina, krzywo, ale półżartem
* Przepraszam. Nie spodziewałem się konsekwencji - Hektor, szczerze, łypiąc okiem na Dianę

Paulina zaproponowała Hektorowi, że skoro ona ma silnego faceta w okolicy, czas zrobić duże zakupy. Mag nie oponował.

* "Nie musimy być sami": +1, Hektor i Diana biegając razem i Hektor pomagający Dianie.
* "To wszystko nie ma sensu": +2, JAK ONA ŚMIE. Nie Diana. Ewelina. Tak nie może być.
* "Samotność moim przeznaczeniem": +1, może jak mnie nie będzie, Ewelina zostawi Dianę w spokoju.
* Hektor ogrodnik: +1, Paulina znalazła kogoś, komu on się może przydać poza Pauliną.
* "To ja tu rządzę": +2, cień Eweliny. Ona niszczy wszystko, co piękne.
* "Faceci są bez sensu": dlaczego on mnie traktuje w ten sposób? Nie wie, że to dla mnie BARDZO trudne? Nie oceniaj czego nie rozumiesz...

Poszli do sklepu. Paulina powiedziała Dianie, żeby ta została w samochodzie; niech nie musi ocierać się o ludzi. Diana doceniła. Hektor chyba też. Zaczęli kupować różne zgrzewki i inne ciężkie rzeczy. Dużo mąki... następnego dnia Paulina robi ciasta na koło gospodyń wiejskich! :D.

**Dzień 2:**

(autorandom)

* "Jestem dobrą panią": +1, Diana zauważyła, że nadal ma wspomagania. Niestety, czary stałe zanikają... ale nie ma to znaczenia. Wolno jej wszystko.
* "To wszystko nie ma sensu": +2, Hektor przemyślał sprawy w nocy, zwłaszcza widząc co dzieje się rano i stwierdził, że to trzeba zatrzymać.

Paulina przy śniadaniu miała okazję zauważyć dwie rzeczy:

* Diana ZNOWU podtrzymuje zaklęcie iluzji. Jej kryształ się... skończył? To nie do końca ma sens i Paulina podejrzewa, że ta cholerna obróżka coś robi.
* Paulina miała okazję słyszeć piękną kłótnię między Hektorem a Dianą. O Ewelinę.

Diana jeszcze raz wyjaśniła, że ma rodzinę. Kocha swoją rodzinę. Straciła pamięć o rodzinie jak została czarodziejką, ale ją odzyskała. Oddali jej pamięć o jej rodzinie. I ona wpakowała się w kłopoty, by im pomóc. A Ewelina ją uratowała. Nawet pomaga rodzinie Diany. Więc niech Hektor nie mówi, bo on jest niezależny, nie ma zobowiązań, niczego nie potrzebuje, więc może robić co chce. Ona nie. A Ewelina przy wszystkich swoich wadach ma też silne zalety.

Hektor powiedział Dianie, że on ma zamiar wstawić się za nią do Eweliny. Porozmawia z nią. Ta wyraźnie się przeraziła. Hektor zauważył, że Ewelina jest wolną czarodziejką a on jest wolnym magiem. Ona nic mu nie zrobi, nie musi z nim rozmawiać. Ale może pomóc. Diana zauważyła, że ten świat tak nie działa. Jeśli Hektor jest przeszkodą, Ewelina może go zniszczyć.

Hektor jest zdeterminowany, by porozmawiać z Eweliną. Bardzo szybko skończyła im się energia na kłótnię...

Paulina wysłała Dianę do kuchni, by ta zrobiła ciasto (Diana powiedziała, że umie). A Hektora wzięła do ogródka, żeby "zobaczyć sytuację". A na serio, by być sam na sam.

Hektor powiedział, że magiem jest od Zaćmienia. Tak on jak i Diana. Paulina powiedziała, że Hektora spotkali magowie, wymazali mu ludzką pamięć i dali mu wiedzę. Hektor potwierdził, że brak ludzkiej pamięci o rodzinie trochę mu uwiera, ale to prawidłowa procedura. Tak musi być - gdyby tak nie było, to by był kłopot. Paulina rzuciła bombę "jeśli pomożesz jej na siłę, ona Cię znienawidzi". Zauważyła jaka jest specyfika arystokracji Świecy. Że Hektor może być użyty przeciwko Dianie przez Ewelinę.

Postać (strona gracza):

| .I_Strat. | .I_Takt. | .Specka. | .Umiejka. | .Umiejka Mag. | .Inklinacja. | .POSTAĆ. |
|     1     |    1     |    0     |     1     |       0       |       1      |    4     |

Okoliczności:

|  .Sytuacja. | .Sprzęt. | .Skala. | .Magia. | .Surowiec. |  .OKOLICZNOŚCI. |
|      2      |    0     |    0    |    0    |      0     |        2        |

Przeciwnik (strona NPC):

| .VERSUS. | .Diff. | .Wpływ akcji gracza. | .OPOZYCJA. |
|    2     |    2   |          0           |     4      |

Wynik:

| .Postać. | .Opozycja. | .Okoliczności. | .Rzut.    | .Wynik. |
|    4     |      4     |        2       | 12: +1 W0 |     S   |

* Jak wy się w ogóle spotkaliście? - Paulina, zaciekawiona
* Ja byłem DJem, ona miała wolne i poszła się bawić. Wpadła mi w oko - Hektor, z uśmiechem

Paulina zauważyła, idąc w kierunku na tor "Hektor ogrodnik", że jeśli będzie Diana miała kogoś, kto ją wspiera... to ma szansę.

REROLL_1: Hektor jest coraz bardziej przekonany o wszechmocności Eweliny wobec Diany

Sukces. Hektor nie zamierza robić niczego głupiego, a na pewno nie rozmawiać z Eweliną odnośnie Diany. Tor "Hektor ogrodnik" +2. Tor "Love" +1.

Paulina poszła więc porozmawiać z Dianą. Diana, nieszczęśliwa, robi ciasto (nie dlatego nieszczęśliwa, że ciasto; nieszczęśliwa, bo Hektor. I wszystko). Całkiem nieźle robi to ciasto, Paulina musi przyznać.

Paulina spytała, czemu Diana podtrzymuje iluzję. Diana powiedziała, że nie wie czemu - ale tamta iluzja padła. Paulina poprosiła Dianę o możliwość sprawdzenia artefaktu (obróżki).

Postać (strona gracza):

| .I_Strat. | .I_Takt. | .Specka. | .Umiejka. | .Umiejka Mag. | .Inklinacja. | .POSTAĆ. |
|    1      |    1     |    1     |     0     |       1       |      1       |    5     |

Okoliczności:

|  .Sytuacja. | .Sprzęt. | .Skala. | .Magia. | .Surowiec. |  .OKOLICZNOŚCI. |
|      1      |     1    |    0    |    0    |      0     |         2       |

Przeciwnik (strona NPC):

| .VERSUS. | .Diff. | .Wpływ akcji gracza. | .OPOZYCJA. |
|     2    |    5   |           0          |      7     |

Wynik:

| .Postać. | .Opozycja. | .Okoliczności. | .Rzut.    | .Wynik. |
|    5     |     7      |        2       | 13: +1 W0 |    S    |

Paulina ma czas, narzędzia swoje diagnostyczne. Udało jej się zbadać, co kryje się w obróżce. Sama obróżka jest bardzo złożonym artefaktem: 

* rejestruje to co się tu dzieje i bada stan samej Diany
* może być zdjęty przez Dianę ale tylko przez nią i jeśli ona nie jest pod wpływem zaklęcia ani distress
* jeśli jest distress, stanowi namiar na portal bojowy (czyli może wpaść ekipa)
* zapewnia Dianie odporność na chłód i ogólną odporność fizyczną; może wsadzić, np. rękę do piekarnika
* eliminuje konsekwentnie wszystkie zaklęcia dookoła Diany

Paulina spytała, czy Diana wie, jak to działa. Diana odparła, że nie do końca wie - ale wie, że zapewnia jej pewne bezpieczeństwo. Paulina powiedziała, że m.in. przez to padają zaklęcia iluzji. Diana westchnęła. Paulina zauważyła, że Diana może tą obróżkę zdjąć. Diana powiedziała, że wie. Ale tego nie zrobi, bo zmarznie. Nie założy na siebie niczego, bo Ewelina by się dowiedziała...

Paulina wyjaśniła Dianie dokładnie co artefakt robi. Diana potwierdziła, że się mogła tego spodziewać. Tak, narzędzie kontroli. Ale to jedynie pasuje - to obróżka. Diana należy do Eweliny. Paulina się zdziwiła - czy Diana tego CHCE? Nie. Ona tego NIE CHCE. Ale nie ma wyjścia.

* Nie chciałabyś się uwolnić? Tak NAPRAWDĘ uwolnić? - Paulina, z ciekawością
* Chciałabym, ale nie ma takiej możliwości. Nie, jeśli na czymkolwiek Ci zależy. Nie możesz być wolna. - Diana, z rezygnacją
* Mogłaś nie zobaczyć wszystkich możliwości. - Paulina
* Na pewno ich nie widzę. Ale Hektor nie jest opcją. Ty też nie. - Diana - A innych po prostu nie znam.
* Popatrz; bez osłony lady Eweliny moja rodzina traci wszystko co dostała. Ja nie mogę ich chronić. Jestem przez moją rodzinę podatna na szantaż. - Diana, spokojnie - Nie mogę być sama, bo nie mam siły być sama. Ktoś musi być... ze mną. 
* No dobrze... jak dużo jesteś skłonna poświęcić, by ochronić swoją rodzinę? - Paulina, z ciekawością
* Zamieniłam wolność na złotą klatkę, ale klatka była złota. Nie chcę być nieetyczna... - Diana
* A gdybyś zapewniła im bezpieczeństwo a potem zapomniała? Nie byłoby opcji szantażu. - Paulina
* I co wtedy będę miała? Będę sama. Stracę rodzinę. - Diana, ze smutkiem
* Rodzina Świecy - Paulina - i inne rodziny magów
* Ale TO jest moja rodzina. Nie chcę innej. Ja ich kocham. - Diana, zirytowana

Paulina ją rozumie. Była na tyle zdeterminowana, by odzyskać swoją rodzinę. Ale cała ta sytuacja dla Pauliny jest chora. Udało im się jeszcze ustalić, że Diana wykonuje polecenia Pauliny i Hektora, acz Paulina ma priorytet; nie ma innych magów w okolicy (na co Diana odetchnęła z ogromną ulgą).

Paulina spróbowała uzmysłowić Dianie, że ma inne opcje. Że tak nie musi być. Żeby Diana nie była całkowicie bez nadziei.

Postać (strona gracza):

| .I_Strat. | .I_Takt. | .Specka. | .Umiejka. | .Umiejka Mag. | .Inklinacja. | .POSTAĆ. |
|     1     |     1    |    0     |     1     |       0       |      1       |    4     |

Okoliczności:

|  .Sytuacja. | .Sprzęt. | .Skala. | .Magia. | .Surowiec. |  .OKOLICZNOŚCI. |
|      1      |    -1    |   -1    |    0    |     0      |        -1       |

Przeciwnik (strona NPC):

| .VERSUS. | .Diff. | .Wpływ akcji gracza. | .OPOZYCJA. |
|    2     |   3    |           2          |      3     |

Wynik:

| .Postać. | .Opozycja. | .Okoliczności. | .Rzut.    | .Wynik. |
|    4     |    3       |      -1        | 1: +1 W0  |    S    |

Paulina nie jest w stanie jej przekonać ani nawet jej nic uzmysłowić. Może jednak zasiać w jej głowie zwątpienie.

* Nowy tor: "Nadzieja na inne życie": +1.

**Stan torów:**

* "To ja tutaj rządzę": 5
* "Dobra pani": 3
* "Faceci są bez sensu": 1
* "To WSZYSTKO nie ma sensu": 6
* "Nomad odchodzi": 1
* "Miłość": 5
* Hektor-ogrodnik: 3
* "Nadzieja na inne życie": 1

**Interpretacja torów:**

Cień Eweliny Bankierz wpłynął na domostwo. Jakkolwiek Diana nadal w nią wierzy, jest ogólne poczucie tego, że postawiła na swoim i się bawi innymi magami. Hektor nie do końca chce stawać osobiście przeciw niej... najchętniej nie miałby z nią nic wspólnego. Niestety, jest jego ukochana Diana.

Hektor i Diana wyraźnie mają się ku sobie. Słodko się na nich patrzy. Jednocześnie są też silne tarcia o Ewelinę Bankierz między Hektorem i Dianą. W desperacji, Hektor najpewniej zrobi coś niezbyt mądrego. Ale nie zrobi tego osobiście i nie zrobi tego wobec Eweliny. Jego ewentualne pomysły, by odejść i zostawić sytuację wyparowały; zaczął nawet myśleć, czy - póki Diana tu jest - nie osiąść tu na jakiś czas.

Tymczasem w myślach Diany zaczynają pojawiać się pytania: czy tak jak ona żyje, czy tak życie musi wyglądać? Nadal jest przekonana, że Ewelina Bankierz ogólnie jej pomaga... ale czy naprawdę to jest uczciwa cena za tą pomoc?

# Progresja

* Diana Łuczkiewicz: problemy z ubraniem, ale za to nosi elegancką obróżkę od Eweliny Bankierz.
* Hektor Reszniaczek: zaczyna kontaktować się z siłami Gabriela Dukata.

# Streszczenie

Diana została ukarana przez Ewelinę Bankierz w taki sposób, że nie ma prawa nosić żadnego ubrania i ma obróżkę zapewniającą jej stan zdrowia. Ewelina oddała Dianę Paulinie na tydzień. Paulina szybko zorientowała się w stanie sytuacji. Co gorsza, Diana i Hektor mają się ku sobie, ale ścinają się o autonomię i o to, czy Ewelina jest w ogóle dobrą osobą. Diana wiele oddała, by jej ludzka rodzina miała dobre życie; Ewelina uratowała ją od długów. Hektor chce wolności dla Diany...

# Zasługi

* mag: Paulina Tarczyńska, poza standardowymi działaniami lekarza-rezydenta wpadła jej na głowę jedna "niewolnica" i jeden pacjent. Próbuje powstrzymać młodych od głupstw.
* mag: Diana Łuczkiewicz, która dla ludzkiej rodziny oddała się w niewolę Ewelinie Bankierz. Broni Eweliny i w nią wierzy. Za to: chodzi nago, pod iluzjami i z obróżką.
* mag: Hektor Reszniaczek, zdecydowanie zakochany w Dianie, nie może znieść, że ona tak żyje, ścina się z Dianą o Ewelinę. Chwilowo zostaje w okolicy... może jako ogrodnik Pauliny?
* czł: Michał Furczon, bogatszy gość z ogrodem; Paulina zapewniła współpracę między nim a Hektorem. Ten człowiek jest dość zadowolony ze sprawy.
* mag: Ewelina Bankierz, bawiąca się życiem Diany, ale po swojemu dbająca o zdrowie i życie swojej zabawki. Otwarte stawanie jej czoła wydaje się głupie.

# Lokalizacje

1. Świat
    1. Primus
        1. Mazowsze
            1. Powiat Pustulski
                1. Krukowo Czarne
                    1. Gabinet Pauliny, gdzie chwilowo mieszka Diana, Hektor i sama Paulina.
                    1. Szkoła wiejska, dookoła której toczył się bieg charytatywny

# Czas

* Dni: 2

# Warianty Przyszłości

(1k6 wybiera wariant)

1. **Jej wysokość Ewelina Bankierz**
    1. "To ja tu rządzę": Ewelina pokazuje wszystkim, gdzie ich miejsce i wszyscy akceptują jej władzę i potęgę. Wizerunek przerażający.
    2. "Jestem dobrą panią": Ewelina pokazuje, że pomaga magom w swojej mocy. Nie jest "fajna", ale jest osobą mającą miejsce i cel.
1. **Wola Eweliny:**
    3. "Faceci są bez sensu": Diana przestaje interesować się Hektorem; widzi jaki on "jest naprawdę".
1. **Hektor na rozdrożu:**
    4. "To wszystko nie ma sensu": Hektor zbliża się do sił Gabriela Dukata (zostać mafijnym żołnierzem?).
    5. "Samotność moim przeznaczeniem": Hektor decyduje, że musi odejść. Szukać szczęścia gdzieś indziej.
1. **Nadzieja miłości:**
    6. "Nie musimy być sami": Diana i Hektor stają się parą. A przynajmniej zbliżają się do tego.


# Narzędzia MG

## Cel misji

1. Misja weryfikująca działanie 'Efekt Skażenia jeśli czar ma rzut 16+'
2. Misja sprawdzająca działanie 'magicznych umiejętności' ze zintegrowanymi 'magicznymi specjalizacjami'
3. Misja weryfikująca działanie 'Wariantów Przyszłości'

## Po czym poznam sukces

1. Efekty Skażenia będą miały następujące cechy:
    1. Będą wyraźnie zmieniały fabułę
    1. Nie będą sprawiać, że magowie boją się czarować, ale... wiąże się z magią pewien koszt
    1. Nie będą całkowicie nadmiarowe
2. Magiczne umiejętności będą dobrze się synergizowały z resztą postaci; zwłaszcza ze specjalizacjami
3. Warianty mają następujące efekty:
    1. Zawsze jest tor, na który można postawić komplikację fabularną lub Skażenie
    1. Istnieje możliwość zbudowania finalnego REZULTATU misji niezależnie od tego JAK działali gracze
    1. Warianty nie są dominujące, ale kierują MG. Każdy konflikt zwiększa jeden odpowiedni tor w zależności od wyniku
    1. Dodanie każdego dnia 0-2 do 2 torów zwiększa chaotyczność decyzji, ale umożliwia graczom zatrzymanie "złego" wariantu. Pierwszy rzut losowy.

## Wynik z perspektywy celu

1. Efekty Skażenia:
    1. Nie było ŻADNEGO efektu Skażenia. Nie wyglądają na nadmiarowe ;-).
    1. Magowie nie boją się czarować, choć efekty Skażenia gdzieś są.
2. Magiczne umiejętności - działają NIE GORZEJ niż z magicznymi specjalizacjami.
3. Warianty:
    1. Historia zdecydowanie lepiej się budowała, nawet niewiele mając na wejściu. Była presja.
    1. Wynik misji jest ciekawszy niż zwykle, bardziej bogaty. Rezultat misji jest... inny, odczuwalnie inny.
    1. Nie każdy konflikt przesuwa tory, acz każdy konflikt gracza dodatkowo powinien / może to robić. Czasem przesunięcie toru nie ma sensu w wyniku konfliktu, czasem niekonfliktujące akcje to powodują same
    1. 0-2 działa dobrze. Przy wielkich rzeczach, 1-3. To jest prescriptive/descriptive.
    1. Kić: brakuje mi przede wszystkim tego, by móc... wpłynąć na te tory. To, że na dwa tory wpłynęłam to... hm. Mam poczucie, że nie rozumiem, czemu niektóre tak wyskoczyły (ten mafijny). Chrzanić moją możliwość kombinowania z tym. Ale NAGLE JEST. To powoduje takie... zagubienie. O tyle mylące, że nie mam wartości, do której dążę. To jest stała walka.
    1. Kić: Tor osłabia znaczenie konfliktu. Konflikt jest definitywny. Tor... konflikt tylko TROCHĘ zmienia. To powoduje, że jeden konflikt to za mało, by wywrzeć wpływ. Nawet maksymalizując wpływ, głupia rozmowa dwóch NPCów może osiągnąć dużo więcej. A w pewnym momencie tor absolutnie dominujący nad innymi zmieni się w samospełniającą się przepowiednię.
    1. Kić: Nie powinnam widzieć torów których Paulina nie widzi. Nie chcę widzieć zapełniających się torów, na które nie mogę poradzić.

## Wykorzystane tabelki

Postać (strona gracza):

| .I_Strat. | .I_Takt. | .Specka. | .Umiejka. | .Umiejka Mag. | .Inklinacja. | .POSTAĆ. |
|           |          |          |           |               |              |          |

Okoliczności:

|  .Sytuacja. | .Sprzęt. | .Skala. | .Magia. | .Surowiec. |  .OKOLICZNOŚCI. |
|             |          |         |         |            |                 |

Przeciwnik (strona NPC):

| .VERSUS. | .Diff. | .Wpływ akcji gracza. | .OPOZYCJA. |
|          |        |                      |            |

Wynik:

| .Postać. | .Opozycja. | .Okoliczności. | .Rzut.    | .Wynik. |
|          |            |                | xx: +x Wx |  S / F  |

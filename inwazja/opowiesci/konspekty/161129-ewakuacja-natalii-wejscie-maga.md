---
layout: inwazja-konspekt
title:  "Ewakuacja Natalii, wejście maga"
campaign: nicaretta
gm: żółw
players: kić, raynor
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [161115 - Uciekła do femisatanistek (HS, temp)](161115-uciekla-do-femisatanistek.html)

### Chronologiczna

* [161115 - Uciekła do femisatanistek (HS)](161115-uciekla-do-femisatanistek.html)

## Kontekst ogólny sytuacji
## Punkt zerowy:
## Misja właściwa:

Dzień 1:

Do Mai w ciągu ranka zapukały trzy dziewczyny. Natalia, Renata i Urszula. Maja udaje, że jej nie ma, ale Natalia nie chce w takim razie iść z Mają. Maja otworzyła drzwi. Natalia chciała Maję zapoznać z dwiema pozostałymi; Maja przyjęła je chłodno. Natalia zaapelowała do wyższych uczuć Mai - czy ona wie, co robi jej szef kobietom? Maja dała do zrozumienia, że nie będzie o tym gadać. Natalia chciała pomocy Mai w czymś; ta odmówiła, bo Maja i tak już poprosiła o wiele.
Dziewczyny odeszły jak niepyszne. Jeszcze Renata została i powiedziała Mai, że Hubert zginie we krwi za swoje czyny.
Creepy.

Od rana też nawiedził Huberta ksiądz Albert.

"Widzę, że miałeś okazję porozmawiać z uroczą Korą?" - uśmiechnięty Albert.
"Uroczą... jest kwestia dyskusyjna. Zwierzyła się księdzu, że chce zniszczyć kościół?" - skonfundowany Henryk
"Jest po prostu sfrustrowana i nieszczęśliwa. Ale to nie jest zła kobieta"
"Jestem w stanie stwierdzić, że są w niej dobre intencje, ale są ukryte przez... obłęd?"
"Jest ona... jest ona bliżej kościoła niż wielu znanych mi biskupów."

Po ostrej dyskusji Henryk przejrzał Alberta. On uważa, że Kora ratuje i pomaga dziewczynom. Walczy z systemem. I Albert martwi się o siostrę - Adelę. Henryk poznał Adelę wcześniej, to jedna z analityczek Huberta... Albert wie, że ona była szantażowana, acz nie wie o co chodzi. I Albert powiedział Korze, że martwi się o siostrę.
I co na to Kora? Powiedziała, że spróbuje pomóc.

Hubert poszedł do hotelu, szukać śladów. Poszedł na śniadanie w hotelu. Tam poluje na "turystkę" szukającą śladów srebrnego lustra. Znalazł ją. Przysiadł się.

"Podoba mi się to miejsce. Jest dobre. Koło mnie - ksiądz i satanistka. Nie myślałam, że to zobaczę" - Amelia, do Huberta i Felicji

Amelia opowiedziała Henrykowi, że szuka srebrnego lustra bez odbicia - słyszała o tym wiersz i wskazywał na tą okolicę. Przesłanka jest - "ksiądz łamie się opłatkiem z satanistą".

"Chyba ksiądz socjalista... na pewno nie Siwiecki..." - Raynor, 2016

Henryk dowiaduje się - nie wprost - czy to Amelia myszkuje po hotelu i kościele w nocy. Amelia jest śliska jak piskorz. Henryk wywnioskował od Amelii, że jej przyjaciel szuka tego lustra i ona WIE o tym. Ale ona sama nie wie co i jak on robi; wierzy, że wystarczy rozpytać lokalnych i poszukać w bibliotece itp.

Przyszła Kora i przysiadła się do stolika. 

"Przyszedł ksiądz się nawrócić na femisatanizm?" - Kora

Kora powiedziała Amelii, że tu na pewno gdzieś jest to lustro którego ona szuka. Korze się śniło srebrne lustro. Patrzyła w nie, a w lustrze widziała potężną kobietę, która zerwała łańcuchy. Amelia powiedziała, że nie pomyślała o tym, że lustro po prostu ma na sobie malunek. Kora to gdzieś już kiedyś widziała.
Ksiądz jest teraz pewny, że Kora ma pomysł i sposób na usunięcie Huberta.

Maja zadzwoniła do Huberta. Chce, by ten opracował cel Mai - Hipolita Mraczona. 
Hubert potwierdził; Mraczon ma psy i jakkolwiek ma rodzinę, w tamtym domku w którym zwykle mieszka nie ma swojej żony i dzieci. Ma za to psy obronne.

Hubert powiedział, że ten hotel jest finansowany przez kilkunastu bogatych mężczyzn; kobieciarzy. Odkąd opłacają ten hotel, przestali mieć etykietkę kobieciarzy. Robią to dość dyskretnie.
Szantaż? Wymuszenie? Tak czy inaczej, Maja jest teraz przekonana, że Kora (lub za Korą) stoi coś silniejszego.

Maja się dobrze przygotowała - obejrzała sytuację z domem Mraczona. I jest gotowa do nocnej akcji.
Wezwała dwóch mięśniaków jako wsparcie na akcję - mają się pojawić bezpośrednio na akcję.
Zdobyła też ciuchy dla Natalii.

Spotkanie Mai i Henryka. Zabrała Henryka na spacerek by mu przekazać informacje.
Henryk ostrzegł Maję, że Kora może zagrozić Hubertowi. Poprosił Maję, by ta pomogła mu zdobyć informacje z recepcji.

Henryk i Maja czekają, aż femisatanistki mają spotkanie i tylko jedna (Felicja tym razem) będzie na recepcji. Przedtem - Henryk upolował w kościele starszą panią z balkonikiem. Przekonał ją, by ona poszła sobie do hotelu by się nią zaopiekowali - na koszt księdza. Babcia poszła. Felicja skupiła się na pomocy starszej pani i dała Mai okazję by ta mogła wejść do akcji.
Maja, przygotowana, weszła do hotelu ściągać dane. Udało jej się (koszt konfliktu: Natalia wierzy, że Maja ma kluczową wiedzę o Hubercie i jego słabych punktach i Kora w to wierzy)

Wieczór. Maja i Henryk mają dane z wideo. Henryk chce sprawdzić tego przyjaciela Amelii. Udało mu się - ma kontakt wizualny.

Maja umówiła się z Natalią nie w hotelu, tylko koło biblioteki. Potem poszła z nią do lasu - tam może się Natalia przebrać w NINJA STRÓJ. Sama Maja jest ubrana w NINJA STRÓJ. Maja kazała Natalii poczekać po czym weszła ze swoimi mięśniakami by zabić psy. Niewiele finezji; broń z tłumikami. Psy padły zanim ktoś się zorientował. Maja wprowadziła Natalię pod domek Mroczana. Maja wbiła się do środka.
Maja skrępowała i zakneblowała Mroczana; ten nie miał opcji walki z Mają.

Maja kazała Natalii poczekać, walnęła kilka razy Mroczana i go postraszyła. Niech się boi jeszcze bardziej. Chodzi o wywołanie efektu u Natalii. A Maja cały czas nakręca Natalię "to twój czas na zemstę". Maja nakręca Natalię, ta zadaje obrażenia... ale się opamiętała. Maja złamała Natalię. Ona chce do domu. 

Maja chce wyjść z domu z Natalią; mięśniaki dzwonią do Mai - zbierają się dziewczyny. Maja wsiadła do samochodu Mroczana i chce uciekać z Natalią. Wyjechała wywalając bramę i jedzie szybko, by zmienić samochód. Marzena się wściekła. Nie straci Natalii. Rzuciła czar Rozpadu. Samochód się uszkodził, ale Marzena nie miała dość czasu przycelować. Mięśniaki porwały Natalię i ją wywiozły, ale Maja została z grupą femisatanistek. 

"Coś ty zrobiła!" - Kora, przerażona, do Mai
"Dałam jej to o co prosiła!" - Maja
"Wykorzystałaś ją, by zaspokoić swego chorego pracodawcę!" - Kora
"Wróciła do domu, jak chciała" - Maja

Marzena się wściekła. Nikt nie zrobi krzywdy jej podopiecznym. NIKT.
Maję pożarła ciemność. Zdominowana przez Marzenę Dorszaj.

Henryk wyczuł GDZIE zostało rzucone zaklęcie. Czar został rzucony tam, gdzie jest Maja. Czyli tam jest mag.
...bardzo niekorzystne pole walki dla Henryka. 

Ostrożnie, Henryk poszedł w tamtym kierunku. Chce zobaczyć co się stało.
Henryk dzwoni do Huberta.

"Ale to jest Maja. Wiesz, nie z takich rzeczy wychodziła" - Hubert
"Nie jestem dobry w fizycznych starciach. Mógłbyś przysłać wsparcie?" - Henryk

Hubert obiecał, że za 30 minut będą posiłki. Henryk jedzie na miejsce, zobaczyć co się dzieje.

"Maju, pojedź, zabij Huberta Kaldwora, potem wróć do mnie" - Marzena.

Maja posłuchała i poszła. A dokładniej - musi mieć auto.

Henryk dojechał; szczęśliwie, zauważył Maję zanim ktokolwiek dał radę zobaczyć jego. Czyli zauważył Maję zanim ktokolwiek zauważył Henryka. Wyczuł od niej świeżą magię mentalną. Maja wbiła się do czyjegoś samochodu, przejęła go i ruszyła do Wyjorza.
Henryk zrobił nawrotkę i jedzie za Mają. Próbował ostrzec Huberta, ale ten nie wierzy. Maja to Maja. 

Henryk dał radę jechać za Mają; (koszt: Marzena jest sprzężona z czarem + aura Marzeny jest nieczytelna dla Huberta - nie pozna kim jest Marzena).
Marzena nie wie kto i skąd, ale wie, że mag rozerwał to zaklęcie. Maja na magii się nie zna...
Zaklęcie na Mai zostało rozerwane przez Henryka w Iglicy Trójzębu.

Maja straciła przytomność.

Natalia wróciła do domu. Henryk wie, że jedna femisatanistka jest czarodziejką a Kora jest figurantką. Hubert przeżył...
Chyba się udało...

# Progresja



# Streszczenie

Siostry Światła (femisatanistki) próbują przekonać Maję do stanięcia przeciwko swemu pracodawcy. Maja odmawia. Ksiądz Henryk poznaje Amelię Eter szukającą srebnego lustra bez odbicia; Kora obiecuje Amelii pomóc je znaleźć. Maja umożliwia Natalii na zemstę; ta jednak nie jest stworzona do zadawania bólu i chce do domu. Maja ewakuuje Natalię, ale w Siostrach Światła objawiła się czarodziejka. Ona Maję zdominowała i dała jej rozkaz zabicia Kaldwora. Henryk pojechał i zatrzymał Maję w Trójzębie rozrywając czar mentalny Marzeny. Przedtem - Henryk dowiedział się przeglądając monitoring kim jest "przyjaciel" Amelii szukający z nią tego lustra.

# Zasługi

* mag: Henryk Siwiecki, dowiedział się, o co chodzi ze srebrnym lustrem; po czym o co chodzi femisatanistkom. Finalnie - uratował Huberta łamiąc czar przymusu na Mai.
* czł: Maja Liszka, złamała Natalię dając jej zemstę, po czym ją wyciągnęła i oddała rodzicom po czym magia mentalna kazała jej zabić Huberta. Czar złamał Henryk.
* czł: Renata Krzem, wojownicza dusza działająca w femisatanistkach (Siostrach Światła). Ostrzega Maję, by ta nie stanęła po niewłaściwej stronie.
* czł: Urszula Kram, z malutką rólką osoby jaką Natalia chciała pokazać Mai, by ta się przejęła. Maja się nie przejęła.
* czł: Natalia Kaldwor, chciała skaptować Maję przeciw Hubertowi, miała swoją zemstę na Mraczonie dzięki Mai i się złamała. Chciała do domu i Maja ją ewakuowała.
* czł: Amelia Eter, poetka i turystka poszukująca Srebrnego Lustra Bez Odbicia. Świetna w odgrywaniu turystki. Zaprzyjaźniła się z nią Kora Panik.
* czł: Felicja Wydech, femisatanistka tymczasowo na recepcji; zaprzyjaźniła się z Amelią Eter.
* czł: Kora Panik, która z przyjemnością pomaga Amelii w znalezieniu lustra i chce nawrócić Maję - by wyplenić tortury kobiet u Huberta.
* czł: Hipolit Mraczon, do jego domu włamała się Maja, zabiła mu psy, został ostro pobity przez Maję i Natalię. Zostawiony w więzach, lecz przeżył.
* czł: Hubert Kaldwor, który dostarcza wszystkich kluczowych informacji Henrykowi i Mai dzięki swoim analityczkom. Zagięły nań parol femisatanistki.
* czł: Andrzej Klepiczek, który wspiera ruch femisatanistek, bo się martwi o swoją siostrę (wie, że jest szantażowana). Bardziej ludzki ksiądz niż element Systemu.
* czł: Adela Klepiczek, siostra księdza Andrzeja z Lenartomina o którą ów się bardzo martwi. Jest w Iglicy Trójzębu w Wyjorzu.
* mag: Marzena Dorszaj, czarodziejka stojąca za Siostrami Światła (femisatanistkami). Po zuchwałej ewakuacji Natalii, zdominowała Maję i kazała jej zabić Huberta. 


# Lokalizacje

1. Świat
    1. Primus
        1. Lubelskie 
            1. Powiat Wolny
                1. Lenartomin, miejsce gdzie objawia się Arazille i działa grupa femisatanistek pod kontrolą maga
                    1. Hotel Lenart, baza centralna Sióstr Światła gdzie także mieszka Amelia Eter.
                    1. Kościół, gdzie sypia Henryk i gdzie Henryk i lokalny ksiądz kłócą się o lokalne femisatanistki
                    1. Posiadłość Mraczona, do której włamała się Maja z Natalią i gdzie Maja wycięła psy bojowe
            1. Powiat Tonkij
                1. Wyjorze
                    1. Centrum
                        1. Iglica Trójzębu, gdzie Henryk zdjął czar mentalny z Mai. Ta padła nieprzytomna.
        
# Skrypt

- Marzena próbuje przejąć Maję, by dowiedzieć się o Hubercie i znaleźć jego słaby punkt
- Siostry Światła nie pozwolą Natalii odejść
- ksiądz Klepiczek próbuje nawrócić Henryka na socjalizm i feminizm
- Kora pomoże Amelii znaleźć Srebrne Lustro

# Czas

* Dni: 1
---
layout: inwazja-konspekt
title:  "Zabili mu syna"
campaign: blakenbauerowie-x-skorpion
gm: żółw
players: kić, dust, dzióbek
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [150210 - "Komandosi", czyli upadek bohaterki (HB)](150210-komandosi-upadek-bohaterki.html)

### Chronologiczna

* [141022 - Po wymianie strzałów... (HB, Kx, AB)](141022-po-wymianie-strzalow.html)

## Punkt zerowy:

Ż: Dlaczego Skorpion nie jest zainteresowany pomocą swojemu członkowi w odnalezieniu dziecka?
B: Ze zniknięciem tego dziecka powiązany jest mag użyteczny dla Skorpiona.
Ż: Dlaczego Blakenbauerowie też nie chcieli się angażować w tą sprawę?
K: Pragmatyzm.
Ż: Co zainteresowało w tym wszystkim Mojrę?
D: Bo tak naprawdę dziecko było viciniusem.
Ż: Jaką unikalną wiedzę może Hektorowi zapewnić jego grupa śledcza w tej sprawie? (co wiedzą)
B: Wiedzą dokładnie dlaczego mag zniknął dziecko.
Ż: Jaką szczególną cechę posiadał ten vicinius?
K: Potrafił sprawić, że nikt nie chciał go skrzywdzić.
Ż: Co sprawiło, że Klemensa oddelegowano do tego zadania?
D: Podejście do dzieci.
B->Ż: Dlaczego odnalezienie dziecka przybliży Hektora do Kingi?
Ż: Bo co się nie stanie, będzie to bardzo spektakularne i odbija się w świecie ludzi.
D->Ż: Czemu Alinę strasznie martwi zniknięcie tego dzieciaka?
Ż: Chodzi nie o dzieciaka a o reakcję Mojry. Ostatni raz jak ktoś się tak zachowywał jak Mojra, doszło do eskalacji z Ottonem.
K->Ż: Dlaczego tą sprawą też interesuje się Draconis Diakon?
Ż: Bo to wygląda na powiązane z atakiem na KADEM celem zdobycia Wyzwalacza Pryzmaturgicznego.
Ż: Jak Kinga planuje uwieść Hektora?
B: Fifty Shades of Grey.
Ż: Dlaczego Draconis podejrzewa siły Blakenbauerów?
K: Fałszywe dowody na to wskazują.
Ż: Trzy słowa opisujące osobę mającą znaczące dla całej historii lustro.
D: Nad wyraz wysoka.
D->Ż: Skąd Klemens zna dziewczynę z aparatem.
Ż: Konieczność wyczyszczenia po Marcelinie.
K->Ż: Jakie posunięcie naszego przeciwnika które - choć pozornie bardzo dobre - obróci się przeciw niemu?
Ż: Zaangażowanie osoby która ma zdecydowanie zbyt dużo własnej ambicji i agendy.
B->Ż: Co się stanie z relacją Kinga - Hektor po tym jak Kinga znajdzie się w obszarze Wyzwalacza?
Ż: Bez interwencji Hektora Kinga umrze straszną śmiercią.

## Misja właściwa:

Hektor wyszedł cały zadowolony z rozprawy sądowej. Wsadził maga do więzienia, wbrew grupie innych magów, więc jego dzień jest podwójnie lepszy. Do tego stopnia lepszy, że gdy zaczął go obwoływać jakiś mężczyzna, nie był bardzo niezadowolony. Dał mu 2 minuty na przedstawienie swojej sprawy. Ten powiedział "Skorpion. Zabili mi syna". Hektor zaprosił go do gabinetu.
Mężczyzna przedstawił się jako Przemysław Marchewka. Wywiadowca Skorpiona. Osoba, co przetwarza dane, zbiera plotki itp. Trzy dni temu w nocy jego syn wracał do domu z imprezy i przejechał go samochód. Syn (Dominik) po prostu wszedł pod koła. Ale problem polega na tym, że Przemysław miał "ghostera" - artefakt pozwalający na przeskanowanie resztek osoby po śmierci pod kątem duszy i ostatnich wspomnień. Wspomnienia denata nie były wspomnieniami Dominika, mimo, że DNA było to samo. 
"Skorpion" nie chce się tym zająć, więc Przemysław przyszedł do Hektora - prokurator, sprawiedliwość, współpraca ze Skorpionem itp. Hektor tak bardzo ufny, zaraz zadzwonił do swojego kontaktu w Skorpionie (Tymoteusz Dzionek). Tamten potwierdził, że Przemysław jest ich agentem ale powiedział, że to żałość ojca i niechęć do przyznania że jego syna już nie ma.

Hektor bierze tą sprawę hobbystycznie, pro publico bono. Jeśli Skorpion się tym nie interesuje a nawet zniechęca, może być to całkiem ciekawe. Dzwoni do swojej grupy śledczej i prosi Annę Kajak o trzy osoby z konkretnymi profilami. Anna ma dostarczyć te osoby w ciągu godziny.
Anna Kajak skontaktowała się z Aliną Bednarz. Powiedziała jej, że Hektor żąda 3 osób na akcję odnalezienia zaginionej osoby. Anna, Olga i Alina. Aniołki Hektora.

Briefing u Hektora Blakenbauera. Gdy Hektor powiedział kogo szukają to Anna stwierdziła, że wiedzą gdzie jest denat. Na to Hektor, że zakładają, że nie mają tego trupa. Olga zauważyła, że mają nawet kierowcę. Anna podsumowała - Hektor chce, by znaleźli osobę (i zapomnieli że nie żyje) i nie szukali tej osoby w jednej konkretnej kostnicy. Hektor potwierdził. Anna z kamienną twarzą przyjęła zadanie.

Potem Hektor wziął na bok Alinę i powiedział że chodzi o Skorpion i podał większy kontekst. Alina się załamała. Hektor oczekuje od Aliny rozwiązania tej sprawy i odnalezienia żywego Dominika. Zakładamy, że Dominik żyje. No i przydałaby się informacja czemu Skorpion chce zatuszować tą sprawę.

Hektor poszedł do Edwina poprosić o zbadanie próbki ciała Dominika. Edwin akurat rozmawiał z Mojrą, ale ta się usunęła (choć została) i pozwoliła im rozmawiać. Hektor zaraz wypomniał Edwinowi jakąś starą sprawę za którą ten mu wisi, lecz Edwin powiedział, że nie chce mieć z tym NIC wspólnego. Żadnego badania zwłok czy szukania. Naciskany przez Hektora się wyślizgiwał aż w końcu Mojra powiedziała Edwinowi, że ma pomóc Hektorowi. Zdziwienie ze wszystkich stron. Edwin palnął, że to Zajcewowi. Mojra na to, że niekoniecznie - i KADEM coś zaraportował i Świeca. Edwin niechętnie się zgodził a Mojra wzięła na stronę Hektora.

Hektor poszedł pełen złych przeczuć a Mojra zaczęła roztaczać przed nim wizję leniwego Edwina który nie robi nic konstruktywnego. Czy nie byłoby idealnie, gdyby był z nimi zaufany, wierny sługa, który będzie patrzył Edwinowi na ręce? Ktoś wybitny i chętny do pomocy Hektorowi? Wzięli Klemensa. Bo leżał w skrzydle szpitalnym i nie mógł uciec.
Zanim Hektor dotarł do Klemensa, złapała Klemensa Mojra. Wyjaśniła, że faktycznie znikają ludzie (Klemensowi nie zależało) i w wyniku tego Mojra podejrzewa jedną siłę więcej - ani Zajcewowie, ani Diakonowie, ani Skorpion. Tym razem to nie Blakenbauerowie a Mojra chce pozbyć się konkurencji. Więc, Klemens się przyda. A przy naprowadzi Hektora by ten nie znalazł niczego czym Mojra się zajmuje.

Hektor poszedł do Klemensa z bombonierką. Oczywiście, jak Klemens mógł odmówić prośbom Hektora!
Hektor zabrał Edwina i Klemensa i pojechali do kostnicy. Spotkać się z martwym Dominikiem Marchewką.

Hektor wziął strażnika zwłok (ciecia z kostnicy) na stronę i twardo odwracał uwagę znęcając się nad dokumentami. W tym czasie Klemens pilnował Edwina a Edwin przeskanował zwłoki. Wykrył, że zwłoki są klonem; technologia sztucznych ludzi Diakonów. Znów w Rezydencji. Edwin jest w kropce - nie ma pojęcia jak to znaleźć. Nie jest detektywem. Hektor powiedział, by Edwin wraz z Margaret przywrócili technologię os szukających. Genialny pomysł. Edwin jest pod wrażeniem. Zajmie się tym, jutro powinien mieć wstępne osy.

[IRX: Przypis 1, na samym dole]

Tymczasem Alina dostała hardcorowe zadanie - ma zweryfikować kiedy nastąpiła podmiana. Sprawdziła z Przemysławem kiedy i jak kontaktował się z synem i czy nic się nie zmieniło - nie, do dwóch ostatnich dni wszystko było bez zmian, potem był zajęty i nie dzwonił. Czyli mówimy o oknie dwóch ostatnich dni. Poszła do akademika "Szczur", mignęła legitymacją na portierni i poszła spotkać się z dziewczyną Dominika, Ewą. Ewa w żałobie. Alina rozmawiała z Ewą i dowiedziała się, że niejaki Adam Bożynów zorganizował imprezę w klubie "Pełnia". Dominik nie chciał iść, ale Adam nalegał i przekonał Dominika. Czym, Ewa nie wie. Nie rozmawiała z Adamem od tego czasu.

Anna Kajak i Olga Miodownik przeszukują imprezę o której mówiła Ewa pod kątem mediów społecznościowych. Nic nie znajdują - nie ma zdjęć, bo WSZYSTKIE aparaty mają zbyt niewłaściwe i nieostre wyniki. Ale znajdują tekst na forum studenckim, że jest jedna osoba, niejaka Luiza Wanta, która powiedziała że dlatego robi się zdjęcia aparatem analogowym. Anna i Olga przekazują informacje o miejscu zamieszkania i namiarach Alinie. Tymczasem Alina odkrywa, że podobny problem dotyczył kamer bezpieczeństwa w "Pełni". Czyżby podmiana miała miejsce w samym klubie a atakujący wiedział jak działa świat ludzi? Na wszelki wypadek Alina przekazała Klemensowi twarde dyski z nadzieją że ktoś będzie w stanie magicznie odtworzyć jakiś interesujący obraz sytuacji.

Edwin został poproszony o odpowiedź na pytanie kto kupił tego sztucznego człowieka. Żeby skontaktował się przez świat Diakonów. Powiedział, że nie do końca ma siłę przebicia, ale spróbuje.

Jako, że Klemens miał kiedyś już do czynienia z Luizą (Marcelin) i rozstali się w pokojowej atmosferze, Klemens po prostu do niej zadzwonił się umówić. Powiedział Luizie, że ta ma zdjęcia i jego (Klemensa) klient jest zainteresowany zrobieniem projektu. Luiza umówiła się z nimi w swoim mieszkaniu. Klemens (6v7->8) zauważył, że Luiza nie wszystko powiedziała; ona wie coś i może komuś powiedzieć za dodatkową opłatą.

Klemens i Alina poszli spotkać się z Luizą w jej mieszkaniu. Alina zaczęła mówić o swoim projekcie socjologicznym a Luiza powiedziała, że ma zdjęcia z imprezy. Alina przejrzała zdjęcia (12v10->14) i zauważyła kogoś bardzo interesującego - Rebekę Piryt, córkę Estery Piryt ze Skorpiona. Alina przepytała Luizę odnośnie całej imprezy (12v9) i dowiedziała się, że Luiza podsłuchała rozmowę Rebeki z kolegą (też na zdjęciu). Rebeka mówiła, że "tak to się robi" i że "nie będzie śladów". Kolega był pod wrażeniem. A potem Dominik zginął. 
Zdaniem Luizy to jest epicka wojna organizacji ubezpieczeniowych (jakiś mroczny reżim i kartel) przeciwko podejrzanej ośmiornicy mafijnej wyłudzaczy ubezpieczeń. I Luiza nie chce podpaść żadnej z tych stron. W odpowiedzi na to, Klemens wysłał ją na 2 tygodnie na wakacje do Egiptu. Jeszcze nikomu nie powiedziała. 
Ona (Rebeka) bardzo denerwowała się na imprezie, a jej kolega powiedział, że to pierwszy raz. On dowodził, mimo, że ona pokazywała co i jak.
Luiza powiedziała, że jest jeden dość znany poeta, który odrzucił wartości materialne. I to on powiedział jej o mafii ubezpieczeniowej i prosił o kontakt. Antoni Szczęśliwiec.

Tymczasem Anna skontaktowała się z Hektorem. Znalazła kogoś - młodego człowieka który był w klubie i nie ma prawdziwego ID. Anna nie ma go w bazach danych, nic o nim nie wie. Udało jej się znaleźć jego zdjęcie i NADAL nic. Co więcej, zgłosiła że ten młodzian pojawiał się w różnych miejscach w przeszłości i w okolicy owego młodziana pojawiały się ludzkie tragedie. Anna zgłosiła też Skażenie prowadzące do wymiotów i poparzenia srebrem. Ogólnie Anna niebezpiecznie zbliżyła się do wykrycia magii. I w jej głosie było słychać determinację - ona znajdzie tego młodzieńca i go przepyta.
Hektor kazał jej kontynuować. Tymczasem sam zadzwonił do Marcelina (który bawi w domostwie SŚ 100 km stąd). Chciał, by Marcelin do niego przyjechał. Marcelin protestował, że polityka, że profesor... jednak Hektor go przekonał jako starszy brat (9#1v7#3->12). Marcelin przeprosił, wymknął się z imprezy i pojechał do Kopalina.

Alina, Hektor i Klemens się spotkali. Klemens powiedział co i jak. Alina ostrzegła Klemensa, że Alina jest dobra, nawet bardzo dobra. Ale (9v7/9/11->8) coś ją musiało naprowadzić; była ZA skuteczna. Miała pomoc lub podpowiedź. Coś sprawiło, że trafiła we właściwym kierunku bardzo szybko i znalazła bardzo szeroki obszar wiedzy na temat młodego maga. A to ten sam mag co wskazała go Luiza, ten, który był "przełożonym" Rebeki.

Marcelin, nieszczęśliwy i marudny stawił się przed obliczem Hektora. Powiedział, że nawiązywał kontakty z sympatyczną alchemiczką i nigdy kontaktów nie nawiąże bo Hektor. Powiedział że dlatego Hektor nie ma dziewczyny - a nawet Edwin coś wyrwał. Gdy Hektor zauważył, że on MA dziewczynę, Marcelinowi się pycho wygło. Już wie jak się zemścić. Kinga. Wtedy Hektor zażądał od Marcelina zidentyfikowanie tajemniczego maga - to terminus Sebastian Tecznia, młody mag. Hektor się uśmiechnął. Wie, kim jest mag działający na imprezie i wie, gdzie on mieszka.
Jedyne co nie pasuje to te klony Diakońskie. I po co mag czystej krwi z SŚ miałby porywać ludzi w taki sposób. I co Anna miała na myśli mówiąc, że Sebastian już się kiedyś przewinął.

Alinę nadal frapuje problem nadzwyczajnie dużej skuteczności Anny Kajak. Wzięła ją ze sobą do restauracji i porozmawiała o tym. Anna trzyma karty przy orderach, ale Alina zdecydowała się na ryzykowne wejście i poruszenie tematyki paranormalnej (14v14->17). Anna przyznała, że kiedyś, dawno temu miała brata. Nie kontynuowała tematu, ale powiedziała, że jak długo otacza się lustrami jest bezpieczna. Powiedziała, że jak nie wie co robić, ustawia bardzo wiele luster i próbuje w nie patrzeć. Udało jej się w lustrach zauważyć ślad Sebastiana. Narysowała portret pamięciowy i trafiła. Bo na tego konkretnego maga (ona myśli: człowieka) poluje już od pewnego czasu. Identyfikuje go jako "złego".
Anna jest samotną kobietą (36 l) mieszkającą w mieszkaniu pełnym luster. Na pewno nie jest normalna. Ona nie podejrzewa magii, ona jest pewna, że jest coś więcej. Ale jest skryta i trzyma karty przy orderach.
Na koniec rozmowy Anna stwierdziła, że nie powinna Alinie tego wszystkiego mówić i opuściła spotkanie.

Edwin został poproszony o uruchomienie działających już os szukających. Potwierdził - następnego dnia da znać Hektorowi jaki jest wynik poszukiwań. A tymczasem Hektor, Alina i Klemens mają zgryz - jeśli uderzą w Rebekę, uderzą w Esterę - najbardziej istotną postać w Skorpionie (bo Dromopod Iserat). Jeśli uderzą w Sebastiana, najpewniej zrobią problemy Marcelinowi. Ale jest to poświęcenie na które Hektor jest gotowy. Zdecydowali się uderzyć w Sebastiana Tecznię.

Hektor się zmartwił, bo nie do końca ma JAK uderzyć w Sebastiana Tecznię. W związku z tym udał się do Mojry. Najpierw poprosił o to, by ona wydobyła informacje, ale się nie zgodziła. Za to dała mu kartę podpisaną przez Andrzeja Sowińskiego z uprawnieniem do przesłuchania w sprawie znikającego człowieka. Hektor się ucieszył. Wziął Klemensa i Alinę (w formie Rebeki, związanej choć nie nagiej) i pojechał z nimi do Sebastiana (z którym uprzednio się umówił na spotkanie).

Sebastian zdziwiony, ale Hektor wyjechał z wizytówki i wszedł twardo. Sebastian został zniszczony. Powiedział wszystko: on uczestniczył w kilkunastu akcjach tego typu, jego mistrzynią jest Karolina Maus i on niewiele wie o jej planach - ona mu nie mówi. Tym razem na akcji była Rebeka Piryt, gildia nieznana, ale Rebeka miała dołączyć do Karoliny i to był jeden z testów. Rebeka powiedziała Sebastianowi, że ona wie, gdzie znajduje się porwany człowiek (bo Sebastian, jak to lojalny spaniel, nie ma pojęcia). Podała nazwę miejscowości - jakieś 8x kilometrów od Kopalina. Czyli poza zasięgiem os.
Hektor surowo zakazał Sebastianowi powiedzieć cokolwiek Karolinie na ten temat i na temat rozmowy. Sebastian wybuchł - gdzie byli wcześniej. Przy badaniach nad Spustoszonymi ludźmi, przy badaniach z ładowaniem artefaktów. Hektor odpowiedział w stylu terminatora "classified".

Znając miejscowość, osy Edwina i Margaret zlokalizowały gdzie powinien znajdować się zaginiony Dominik Marchewka. W siedzibie firmy informatycznej LegioQuant. W małym miasteczku o nazwie Bobrów. Firma informatyczna. O nazwie LegioQuant. Rekordy świadczą, że ta firma faktycznie istnieje już kilka lat, ale nie miała dużego powodzenia. Ostatnie dwa lata były lepsze.

No ok. Więc - wchodzą karaluchy zwiadowcze Margaret. I przynoszą smętną wiadomość. Mamy do czynienia z nie ukrywanym systemem zabezpieczeń typu Krówków oraz z lapisowaną piwnicą. Nie wiadomo co tam jest. Dodatkowo, są tu ludzie. Jakichś 5-8 studentów programistów i jeden PM. Tak bardzo firma.

Zlokalizowali bazę. Nie wiadomo co zrobić dalej...

EPILOG:
Anna Kajak zrezygnowała z roli przełożonej grupy śledczej Hektora. Na swoje miejsce zaproponowała kolejną osobę, kompetentną. Motywowała swoją rezygnację przyczynami osobistymi. Nie opuszcza grupy śledczej, ale nie chce już dowodzić.

# Lokalizacje:

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Centrum
                        1. Prokuratura
                        1. Politechnika
                            1. Akademik Szczur
                    1. Dzielnica Trzech Myszy
                        1. Klub Pełnia
                        1. mieszkanie Luizy Wanta
                    1. Obrzeża
                        1. Rezydencja Blakenbauerów
            1. Powiat Myśliński
                1. Bobrów
                    1. Obrzeża
                        1. Siedziba firmy LegioQuant


# Idea:

- Sojusz Blakenbauerów ze Skorpionem może też czynić dobro.

# Zasługi

* mag: Hektor Blakenbauer jako twardo wchodzący we wszystko prokurator, lecz delikatny dla Kingi.
* vic: Alina Bednarz jako czasem związana a czasem zwyciężczyni najtrudniejszego testu na sesji.
* czł: Klemens X jako cichy ochroniarz rozwiązujący trudne problemy (Blakenbauer potrzebował kanapkę).
* czł: Przemysław Marchewka, szeregowy agent Skorpiona, wywiadowca, który stracił syna i chce by prokurator go odnalazł.
* czł: Anna Kajak, przełożona sił specjalnych Hektora, przeżyła bardzo wielu przełożonych i podwładnych i ma zamiar nie przerywać dobrej passy.
* czł: Olga Miodownik, siły specjalne Hektora, medycyna sądowa i skłonności sadystyczne.
* czł: Brunon Czerpak, kierowca, który przejechał Dominika Marchewkę.
* vic: Dominik Marchewka, bardzo uzdolniony student informatyki i elektroniki który zaginął (zginął).
* czł: Ewa Kroideł, dziewczyna (żywa) Dominika.
* mag: Mojra, o dziwo agent dobra ratujący ludzi na tej misji
* czł: Adam Bożynów, student, który wciągnął Dominika na imprezę
* czł: Luiza Wanta, studentka ASP, aspiruje do bycia artystką i robi zdjęcia aparatem analogowym (klisza).
* czł: Antoni Szczęśliwiec, poeta, który odrzucił świat materialny. Ale nie głoduje za bardzo, bo pełni przydatne funkcje.
* mag: Marcelin Blakenbauer, podręczne narzędzie dostępowe do hipernetu.
* mag: Sebastian Tecznia, potencjalny mastermind i kidnaper z nadmiarem wpływów u Diakonów.
* mag: Rebeka Piryt, młoda czarodziejka Skorpiona w niewłaściwym miejscu... polując na syna agenta Skorpiona?


# Przypis 1
Pomyliliśmy chronologie. W tej Kinga jeszcze nie istnieje.
W związku z tym anulowałem następujący paragraf (zostanie on jak i pytania o Kingę przeniesiony do misji chronologicznie po "Reporter vs Blakenbauerzy"):


Do Hektora przyszedł Borys. Powiedział, że wypadałoby, by ten poszedł do pokoju. Mocno przyciśnięty przez Hektora Borys wyznał, że wprowadził kogoś do pokoju Hektora za wyraźnym rozkazem Mojry i niech Hektor nie pyta. Więc nie spytał - sprawdził na kamerach. Okazało się, że to Kinga. Zamiast do pokoju, Hektor poszedł do gabinetu i tam poprosił przez interkom Kingę by stawiła się w jego gabinecie. Nie wie co ona tam robi i okazał się być dosyć odporny na aluzje więc Kinga zdjęła płaszcz i stanęła przed Hektorem w lateksie i dała mu bicz. Powiedziała, że zrobiła research - Hektor wyraźnie lubi S&M w roli dominującej i może ją ukarać w sposoby jakie mu się podobają. Hektor próbuje kulturalnie i delikatnie ją przekonać że to zły pomysł i że to plotka (7v10->11) i mu się udało. Kindze zrobiło się głupio. Zmieni podejście i już nie podejrzewa Hektora o S&M.
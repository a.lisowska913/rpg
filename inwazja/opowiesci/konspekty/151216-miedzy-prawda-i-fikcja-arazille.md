---
layout: inwazja-konspekt
title:  "Między prawdą i fikcją Arazille"
campaign: powrot-karradraela
gm: żółw
players: kić, dust
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [151212 - Antyporadnik wędkarza Arazille (HB, AB, DK)](151212-antyporadnik-wedkarza-arazille.html)

### Chronologiczna

* [151212 - Antyporadnik wędkarza Arazille (HB, AB, DK)](151212-antyporadnik-wedkarza-arazille.html)

## Punkt zerowy:

## Kontekst misji:

Oddział Zeta oraz Dionizy z Aliną zostali przechwyceni przez Arazille i gdzieś wywiezieni...

## Poprzednie spekulacje

N/A

## Plan graczy

N/A

## Dane

- zegarmistrz, zmieniający ludzi w proces (fabryka -> optymalizacja)
- artysta, tworzący sztukę z ludzi (galeria -> podziw)
- strażnik, uniemożliwiający zmiany (ratusz -> niezmienność)

## Misja właściwa:
### Faza 1: 

GO! GO! GO!
Ciężarówka sił specjalnych wjechał do Miasta. Briefing ze strony Kleofasa Bora: całe miasto działa dziwnie; najpewniej skażono wodę. Narkotyki czy działania psychoaktywne. Miasto ogólnie zachowuje się nienormalnie porządnie. Z drugiej strony były podobno samobójstwa... ale żaden z rekordów policyjnych nie potwierdza. Kleofas ma tą informację od Patrycji.
Skład grupy specjalnej:
- Jędrzej, "najpierw niszcz, potem pytaj". Psychol i destroyer. (Zeta, kiedyś: Selfish guy. Naprawdę zły mag.)
- Marta, "stateczna acz niebezpieczna". Serce drużyny. (Zeta, kiedyś: Marta)
- Kamila, "McGyver w spódnicy". Kolekcjonerka pięknych przedmiotów. (Zeta)
- Romuald, "podrywacz i kucharz". Porwany w Czarnej Wieży (Zeta)
- Maria, "psioniczka i mental detector". (Arazille, stworzona dla Marty)
- Kleofas, "dowódca, co nic nie wie". (Arazille, stworzona dla Aliny / Dionizego i przekazał dowodzenie Dionizemu)

POI:
- ratusz
- stary młyn
- domek na osiedlu
- plac zabaw
- muzeum
- biblioteka
- komisariat policyjny
- monopolowy
- piwniczka na starówce
- poczta
- obserwatorium

Mają więc dane z komisariatu od Patrycji (Arazille). Dane te są perfekcyjne. Dionizy i Alina stoją przed komisariatem z Kleofasem... skąd się tu nagle wzięli? Chcieli WYSŁAĆ tu Kleofasa. Plus, przypomnieli sobie migawkę o Arazille i wchodzeniu do ciężarówki z poprzedniej misji. Mają oczywiście srebrne medaliony Hektora... i owe medaliony wysyłają sygnał lekkiego bólu. Dionizy zaproponował "szkolenie czy kontrola"? Nie no, ma wejść Kleofas i zrobić im kontrolę...
Arazille przesunęła im czas/percepcję i ściągnęła Romualda, by on pełnił rolę Kleofasa.

Alina i Dionizy zorientowali się, że minęła godzina. Ale... jak to. No nic, wejdą...

Moment, godzina? Alina zadzwoniła do Marty zapytać, co się dzieje (czy minęła godzina). Tak, minęła godzina - ona spędziła cały ten czas w restauracji grzebiąc po paragonach (jej siostra, Maria, jej zaproponowała (właśnie Alina i Dionizy przypomnieli sobie, że Marta i Maria są siostrami)) - okazuje się, że dzień w dzień wszystko wygląda identycznie. Tydzień w tydzień posiłki są te same. Ci sami ludzie też je kupują, co więcej - NIKT nie gotuje w domu.
Dionizy zadzwonił więc do Romualda. W rozmowie Romuald powiedział, że był w muzeum, ale nie wszedł; muzeum jest dziwne, bo tam są wystawy z ludzi. Antyki i inne takie poszły do piwnicy, wszystko co są to ludzie. I Dionizy się zorientował, że w sumie, Romuald... stoi koło niego i Aliny. A Kleofasa tu nie ma.

Czemu tu jest Romuald? Gdzie Kleofas? Romuald odpowiedział, że przecież Dionizy go tu wezwał. A Kleofas? Nie opuszczał ciężarówki. W końcu Dionizy - pełniący obowiązki dowódcy - kazał mu zostać. W rozmowie Romuald - Dionizy jeszcze jedna rzecz się stała; gdy Romuald powiedział, że nie może wejść do muzeum, bo przecież amulet poparzyłby jego piękne ciało, dłonie Romualda zmieniły się na MOMENT w koszmarnie mordercze szpony jakiejś bestii. Alina to zauważyła. Zbladła, ale nic nie powiedziała.

Dionizy zauważył, że jakiś przechodzień spojrzał na Dionizego i Alinę z przestrachem. Dionizego to zainteresowało, podszedł i zagadał. Przechodzień cały przerażony, pyta, czy obcy. Tak. No to on musi to zgłosić na policję; a tak w ogóle, nie może zostać i rozmawiać, bo to niezgodne z prawem do bycia produktywnym. Dionizy WTFuje po raz kolejny - NIE MA prawa do bycia produktywnym. Przechodzień mówi, że to obowiązek od zawsze. Dionizy mówi, że jest z policji. Flashback: cała ekipa widzi, jak ów przechodzień kiedyś miał żonę i dziecko, ale zagrozili że mu je odbiorą jeśli złamie choć JEDNO prawo (a każdy zwyczaj jest prawem, łącznie z "kiedy wolno ci co zjeść"). Dionizy postraszył, że wpadnie zobaczyć żonę i dziecko. Przechodzień się zgodził (przerażony i załamany). Wygląda na to, że faktycznie policja jest tu wszechmocna.

...nieźle...
...wszystko wskazuje, że flashbacki podsuwa Arazille...

Romuald, Alina, Dionizy mają strasznego WTFa. Zostawili przechodnia i nie weszli na komisariat; tu dzieje się coś NAPRAWDĘ dziwnego. Romuald zrobił głupią minę, przypomniał sobie jajecznicę z groszkiem i chrzanem - nie chciał się przyznać. No, taka głupotka, zrobił to swojej dziewczynie jak nie wierzyła, że da się to zjeść. Bardzo jej smakowało.
Alina spytała go, gdzie ta dziewczyna jest teraz. BSOD. 

Romuald się rozdarł (zaczął wrzeszczeć). Jest członkiem Zety, zmienionym przez Tymotheusa Blakenbauera w Czarnej Wieży. Nie jest w żadnych siłach specjalnych, jest tylko zwykłym kucharzem. Zwykłym cholernym kucharzem. Alina i Dionizy zaczęli go uspokajać; Romuald zaczął krzyczeć, że zabił swoją dziewczynę (Tymotheus zmienił go w Zetę i ją zabił). Wtedy Arazille zesłała wizję - nie, NIE zabił dziewczyny; uratował ją Edwin i zatrzymał Zetę... 

Alina i Dionizy próbują zebrać wszystkich do kupy. Powiedzieć im jaka jest prawda. Dojść do tego co robić dalej; pamiętają Arazille. Dionizy odzywa się przez komunikator, by wszyscy zgrupowali się w odpowiednim miejscu (stacja benzynowa). Dostali wiadomość od Kleofasa; Kleofas powiedział, że nie - mają się wszyscy stawić w ciężarówce. Alina nie chce. Natychmiast przyjęła formę Hektora i powiedziała, że są nowe rozkazy i Kleofas jest odsunięty od działania (dowodzenia). Kleofas próbował się żołądkować, ale Dionizy puścił na kanale sił specjalnych bez Kleofasa, że Kleofas nie wie o magii a sprawa dotyczy magów; więc Kleofas został odsunięty. A Alina jako Hektor kazała mu wypełniać papiery.

Much smutek, so very sad. Smutny Kleofas jest smutny.

Jeszcze przez moment Romuald szybko wyjaśnił - on w Czarnej Wieży miał do czynienia z Arazille. Tam poznał jak ona działa. Arazille daje wolność swoim agentom; wolność za jedno zadanie. Więc oni teraz rozwiązują faktyczny problem dla Arazille a potem będą wolni i mogą robić co chcą. Romuald się wściekał na Blakenbauerów; wszyscy są źli, tu Alina powiedziała, że Hektor naprawdę nie wiedział (najpewniej) i Romuald może sam z nim porozmawiać; Dionizy zauważył, że przecież Edwin uratował jego dziewczynę...

Nagle - zamarzli. Nie mogą się ruszyć. Niemłody mag we fraku podszedł do nich i zaczął żądać odpowiedzi na swoje pytania. Wzgardził nimi i zaczął kontrolować. Powiedział, że na szczęście, to on ich znalazł pierwszy - gdyby jego kolega ich znalazł, próbowałby włączyć ich w produktywny proces. A tak - można ich po prostu zniszczyć, by zachować status quo. Dionizy go zaczął opóźniać, by kupić czas pozostałym członkom Zety na pojawienie się. Dał radę - mówiąc o Karradraelu. Ów defiler powiedział, że Karradrael jest na tym terenie ale on nie ma jak go wywalić. I na to weszła Marta, która osłaniana przez Marię i z pomocą Romualda unieszkodliwiła defilera z zaskoczenia.

Defilera przenieśli do ciężarówki. W końcu tam jest Arazille, i tam może użyć swojej mocy przeciwko defilerowi...

Jeszcze w ciężarówce Alina i Dionizy wzięli na siebie smutny obowiązek przedstawienia sytuacji wszystkim obecnym. Wyjaśnienia, że są Zetą. I dowiedzieć się, kim są.
- Marta jest wściekła. Przypomniała sobie swoją przeszłość i swojego ojca. Przypomniała sobie - miała być rozmnożona z Marcelinem, ale nie była kompatybilna. Gdy coś się zepsuło, przekształcono ją w Zetę z rozkazu Ottona. Marta ma na celowniku Marcelina, Edwina, Ottona, Margaret. Nie ma na celowniku Hektora, którego uważa za idiotę. Jest córką terminusa. Jak długo ma Marię... Marta wie co robić.
- Jędrzej jest szczęśliwy. On wie, że jest potworem. Lubi to. Powiedział Alinie, że przez ostatnie 15 minut dowiedział się najwięcej z całego zespołu:
-- drugi defiler to kobieta, artystka, wystawiająca ludzkie istoty w muzeum
-- trzeci defiler to zegarmistrz, który ustawia ludzi w proces i chce wszystkim ręcznie sterować; działa w obserwatorium.
Wszystko mu powiedział ten policjant jakiego Jędrzej przesłuchiwał. Mimo, że w ten sposób zgubił swoją rodzinę. Ale Jędrzejowi o to też chodziło.
- Kamila była modelką; chce być piękna. Kiedyś sprzężenie artefaktów jej to zrobiło; Edwin ją uratował. Ale nigdy nie powiedział, że zabierze też jej wolę. Uważa, że jest nadal piękna (mocą Arazille) i z przyjemnością pomoże w uratowaniu tych ludzi.

...tak... czyli wszystko jest prawdą i fikcją jednocześnie...

# Streszczenie

Oddział Zeta jest porwana przez Arazille, która wykorzystuje ich do walki przeciw bardzo silnym defilerom w Toczmiejowie. Członkom Zety wróciła tożsamość sprzed transformacji w Zetę; pojawiają się lekkie przebicia, że nie są ludźmi, ale Alina i Dionizy kontrolują sprawę. Arazille obiecała Zecie wolność za zniszczenie defilerów. Jeden z defilerów zaatakował i zdominował Zetę, ale Marta Newa dała radę go rozwalić. Zeta przypomniała sobie swoją przeszłość...

# Lokalizacje:

1. Świat
    1. Primus
        1. Małopolska
            1. Powiat Komesów
                1. Toczmiejów
                    1. Komisariat policji
                    1. Muzeum
                    1. Obserwatorium
                    1. Restauracja Trzecia

# Zasługi

* vic: Alina Bednarz, która pierwsza przeniknęła barierę iluzji i doprowadziła do tego, że Zeta uzmysłowiła sobie ponurą prawdę.
* vic: Dionizy Kret, tymczasowy dowódca, który pomaga utrzymać wszystko pod kontrolą w czasie w którym Arazille panuje nad zmysłami.
* vic: Arazille, kalibrująca rzeczywistość dla Aliny, Dionizego i Zety; odbudowująca osobowości i pamięć Zety i próbująca uratować miasto przed trzema defilerami. 
* vic: Marta Newa, oddział Zeta; otrzymała efemerydę siostry i z jej pomocą znajduje nienaturalne wzory w mieście. Niebezpieczna w walce wręcz.
* vic: Jędrzej Zeta, oddział Zeta; torturował policjanta by dowiedzieć się z kim mają do czynienia. Kiedyś psychopatyczny mag, cieszy się że stał się Zetą.
* vic: Kamila Zeta, oddział Zeta; cicha, zawsze ma coś groźnego pod ręką. Kiedyś: modelka i kolekcjonerka artefaktów.
* vic: Romuald Zeta, oddział Zeta; pierwszy, któremu Alina i Dionizy pomogli odzyskać świadomość. Kiedyś kucharz i podrywacz.
* vic: Oddział Zeta, który zaczyna sobie powoli przypominać kim są, kim byli i co im zrobiono...
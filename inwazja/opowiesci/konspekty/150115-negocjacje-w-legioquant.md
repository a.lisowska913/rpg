---
layout: inwazja-konspekt
title:  "Negocjacje w LegioQuant"
campaign: blakenbauerowie-x-skorpion
gm: żółw
players: kić, dust, dzióbek
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [141216 - Zabili mu syna (HB, Kx, AB)](141216-zabili-mu-syna.html)

### Chronologiczna

* [141216 - Zabili mu syna (HB, Kx, AB)](141216-zabili-mu-syna.html)

## Punkt zerowy:

Ż: Jaki jest główny, znaczący produkt produkuje LegioQuant?
B: Sprowadzają i dostosowują na polski rynek zaawansowane companion dolls z Japonii.
Ż: Jakie jest znaczenie rzeki w Bobrach?
K: To jest bieżąca woda i zło nie może jej przekroczyć.
Ż: Jaką informację jakiej nie wiecie wy posiadają siły Millennium (czyli Mikado)?
D: Wie dokładnie co trzymają w środku piwnicy władcy LegioQuant.
Ż: Jakie było ostatnie ciekawe wydarzenie w Bobrach?
B: Wystawa kotów rasowych.
Ż: Kto jest interesującą postacią w Bobrach?
K: Młody geniusz komputerowy aktywnie odmawiający pracy dla LegioQuant.
Ż: Jakie jest ciekawe miejsce w Bobrach?
D: Opuszczony szpital, dzieciaki tam chodzą imprezować.
Ż: Dlaczego młody geniusz odmawia?
B->Ż: Zważywszy na jego historię życiową boi się, że się zakocha.
Ż: Czemu opuszczono szpital w który zainwestowano sporo pieniędzy?
K: Z powodu osobistej zemsty z jednego z pracowników.
Ż: Co niesamowitego zobaczyli niektórzy mieszkańcy?
D: Spektra w szpitalu.
B->Ż: Co w modelu biznesowym tej firmy wzbudziło szczególne zainteresowanie Anny Kajak?
Ż: To się może udac tylko pod warunkiem że wszystko od samego początku do samego końca będzie miało szczęście. Jeśli choćby jedna rzecz (po stronie PL lub JP) nie zadziała, padają.
K->Ż: Co sprawia, że niezależnie od rozwoju sytuacji Anna Kajak będzie dalej pracować dla Hektora?
Ż: Z jej punktu widzenia Hektor jest albo rozwiązaniem albo problemem. W tym drugim wypadku zostanie wyeliminowany.
D->Ż: Jaki sekret skrywa geniusz komputerowy?
Ż: Wie, dlaczego LegioQuant znajduje się nad rzeką.

## Misja właściwa:

Bobrów. 
Gigantyczna czarna ciężarówka Blakenbauerów, której właścicielem jest Marcelin. To "podrywowa ciężarówka". Z nieco zmienionymi elementami w środku i wymienioną kanapą.
A w ciężarówce - Alina, Anna, Olga (aniołki Hektora), Marcelin (podrywający dziewczyny), Hektor, Margaret i Klemens.

Margaret przypomniała Hektorowi, że ma w środku karaluchy które znalazły lapisowaną piwnicę. Zaproponowała wejścia jako sanepid. Hektor się nie zgodził. Anna zebrała zbiór dokumentów na to, co można zrobić firmie informatycznej i Hektor wszedł (11v10->11). Znalazł mnóstwo rzeczy jakie można im zrobić i z jakiego powodu można im tam wejść i coś zrobić.
Anna zgłosiła się na ochotnika i powołała Olgę i Alinę. Olga zaprotestowała mówiąc, że Anna nie dowodzi, lecz Anna powiedziała, że "jeszcze".

"Nie będę posyłał agentek specjalnych by mi psa łapały! Klemens, jest robota." - Hektor do Margaret

Więc Klemens wziął ciężarówkę i podjechał pod schronisko. Tam znalazł psa. Starsza pani w schronisku od której dostał psa imieniem Łamignat powiedziała mu z oburzeniem (5v2->8), że Łamignat został zostawiony w lesie. Ale z obrożą. Ona dzwoniła i dowiedziała się, że pod numerem napisanym na obroży nikt nie zna ludzi o nazwisku Szorak (właściciel Łamignata, wynika z obroży) ale jak tam pojechała, to podobno państwo Szorak pojechali do Anglii na stałe.
Klemens dowiedział się tego powyżej też z rekordów policyjnych. Dowiedział się też, że Filip był bardzo zdolnym informatykiem, choć chimerycznym.

Margaret przerobiła Łamignata; zmieniła go w bardzo skutecznego psa tropiącego, nadała mu cechy os.
Aniołki Hektora wchodzą twardo do siedziby LegioQuant. Pod dowództwem Aliny (Margaret nalegała, by nie Anna dowodziła). Hektor zadbał, by weszły w majestacie prawa.

Weszły Anna, Alina i Olga. Szybko ustawiły wszystkich do porządku i informatycy odeszli od komputerów. Szef firmy przedstawił się jako Jakub Ryjek i powiedział, że nie wierzy w legalność ich wejścia na co Alina zrzuciła tonę dokumentów od Hektora. Jakub to czytał i mina mu rzedła. Co mógł zrobić, pozwolił jej się swobodnie poruszać. Olga sterroryzowała informatyków a Alina chciała wejść do piwnicy; Łamignatowi zależało (bo znalazł ślad). Faktycznie, w piwnicy znajdowały się japońskie lalki, companiony. Łamignat przyczepił się do ściany, która jednak była pełna. Alina zażądała otworzenia tajnego przejścia i (11v10->13) Jakub Ryjek się zgodził. Poszedł do komputera pod nadzorem Aliny i wpisał hasło. Alina kazała mu zostać u góry i sama zeszła do piwnicy. Długi, podwójnie lapisowany korytarz. Za nią zatrzasnęło się tajne przejście i zostało jej tylko przejść z Łamignatem przez następne drzwi. Nacisnęła na klamkę i poraził ją prąd. Gdy traciła przytomność (5/k20) zobaczyła, że Łamignat "łamie sobie zęby" atakując metalową nogę.

Margaret poinformowała Zespół, że jej karaluchy dostrzegły, że studenci pokonali Aniołki - Olga i Anna zostały pokonane, choć 3 studentów zostało bardzo poważnie rannych. Dziewczyny są popychane i prowadzone do piwnicy. Czarodziejka zaproponowała wirusy i tego typu broń biologiczną, bo nie ma przy sobie żadnych ciekawych bioform. Choć zaproponowała też przekształcenie w coś Klemensa. Marcelin szybko łączący się z Mojrą stwierdził, że nie działa na tym terenie Srebrna Świeca. A przynajmniej nie z tego co wie Mojra. Czyli mała prywata lub czarna gildia planktonowa.

Margaret wezwała Oddział Zeta. Kazała Klemensowi wbić się ciężarówką w budynek. Klemens zrobił to z najwyższą przyjemnością. Podczas wbijania się zginął jeden student, lecz to dało możliwość Annie i Oldze unieszkodliwienia reszty napastników. Klemens, Marcelin i Hektor wyszli zwycięsko z ciężarówki prosto na Annę i Olgę. Anna spokojnie zaraportowała zneutralizowanie przeciwników i zaginięcie Aliny. Hektor powiedział, że wezwał siły specjalne (by Anna nie wezwała ich sama) po czym powiedział Annie, że ona NIE idzie z nimi i ma zostać i pilnować Olgi i sytuacji. Anna przyjęła.

Hektor, Marcelin i Klemens zeszli w podziemia. I widzą lite ściany i lalki. Podczas przeszukiwania ścian Klemens zorientował się, że coś jest nie tak (7v8->9) i zdążył odepchnąć zarówno Hektora jak i Marcelina na boki. Lalka nie zdążyła skrócić ich o głowy. 
Klemens zareagował w swój ulubiony sposób - użył fireballa na biedne lalki. Poparzył Blakenbauerów, ale zniszczył 5 z 8. Marcelin wysadził jeszcze jedną a Hektor unieruchomił kolejną. Ósma lalka zrezygnowała z walki i się ukłoniła, na co Klemens zareagował zastrzeleniem jej. Otworzyło się tajne przejście i rozległ się głos "no pięknie, ale to nie było potrzebne". Klemens zniszczył ostatnią lalkę.

"Czy jak wyjdę z korytarza, Herr Blakenbauer, zapewnisz mi, że twój cyngiel mnie nie zastrzeli?"

Z korytarza wyszedł mag w pancerzu serwomotorowym - Timor Koral. Przedstawił się i powiedział, że ma Alinę i może w dowolnej chwili zniszczyć ich wszystkich. Może ich wysadzić.
Krótkie negocjacje nie doprowadziły do niczego - Timor nie chce nikogo wydać (łącznie z Aliną), ale odda ją jak Blakenbauerowie sobie pójdą i dadzą słowo, że nie wrócą i nie będą interesować się tym terenem ani nikomu nie nadadzą cynku. 

"Nie mam czym panu grozić. Bo co, zabiję pańskiego człowieka? To ma być groźba? To jest na porządku dziennym." - roześmiany Timor Koral do Hektora.

Hektor poszedł w grę na czas. Użył przemowy o praworządności, by kupić czas na sprowadzenie Oddziału Zeta. Timorowi pasowała gra na czas, więc słuchał uprzejmie ziewając.

W końcu obudziła się Alina...
Obudziła się w łóżku. Nie związana. W laboratorium. Zobaczyła Diakonkę, starszą Diakonkę która wygląda jak cyborg. Diakonka powiedziała, że Alina wszystko jej już powiedziała - wszystko co wie, jakie ma moce, jakie ma informacje, absolutnie wszystko. I teraz Diakonka powiedziała Alinie, że ma iść do Hektora - bo w tej chwili jest zakładniczką. Alina jest w koszuli nocnej, bo w ciężarówce jest Margaret Blakenbauer. Z tego samego powodu zniszczony został pies.
Diakonka powiedziała Alinie, że nie mogą wyjść z piwnicy póki nie rozwiąże się problemu tego, co Blakenbauerowie widzieli. Jest skłonna wysadzić wszystko i wszystkich. Bo takie dostała rozkazy.

Alina wyszła do Hektora i Timor się ucieszył. Podszedł i szepnął Alinie, że ma bombę.
Alina mindlinkiem przesłała obraz Diakonki Klemensowi który przesłał to Margaret. Diakonka to Mordecja Diakon.
Negocjacje polegają na tym: albo Blakenbauerowie dadzą sobie wymazać pamięć, albo Mordecja zabije ich wszystkich i wszystko zniszczy. Na żądanie odgórne.
Timor nie chce umierać. Bardzo nie chce umierać. Mordecja powiedziała, że ktoś dowodzi, ale i ona i Timor zapomnieli wszystko. Wymazano ich.
Oddział Zeta na pozycji. Mojra poinformowana o całej sytuacji.

Problem polega na tym, że aktualna sytuacja jest taka: za dużo wiedzą. Albo się poddadzą albo wszystko zniszczą. Etap negocjacji się skończył.
Więc, trzeba przetrwać eksplozję...

Klemens mówi: "Mam pomysł jak rozwiązać ten problem". Staje koło Hektora i Marcelina i wbija im po nożu pod żebra, wyzwalając w nich Bestię.
Alina łapie za karalucha Margaret i zaczyna transformować. 2/k20 na kości szczęścia (utajnione). Bomba upadła na ziemię i nie wybuchła.
W formach bestii: Hektor odgryzł rękę Klemensa. Marcelin oderwał głowę Timora i zaczął ją pożerać.
Karaluch spieprza jak karaluch.
Rdzeń Węzła został przeciążony...


Klemens: zjedzony i wysadzony przez Węzeł. Będzie odtworzony przez Edwina od nowa. Nic nie pamięta, raport poda mu Mojra.
Marcelin: 9/k20. Pobity ciężko i uszkodzony przez Hektora, połamany i potłuczony, do naprawy przez Edwina. Też: impotencja na miesiąc.
Hektor: 4/k20. Wchłonął bardzo dużo energii i poszedł w formie bestii na Bobry. Zmasakrował kilkunastu cywili zanim siły Mojry go zatrzymały.
Alina: 1/k20. Poważna transformacja.
Anna: 11/k20. Wstrząs mózgu i konieczność leczenia w szpitalu dla ludzi.
Olga: 14/k20. Potłuczona i nic więcej. 
Margaret: 18/k20. Straciła fryzurę; włosy jej się zburzyły.
Mordecja: 11/k20. Zdążyła się schować do containment chamber i mimo, że bardzo ciężko poparzona i napromieniowana, została przechwycona przez siły Mojry.
Oddział Zeta: 16/k20. Stracili 3 członków, między innymi do Hektora.

Wszystko, cała baza, wszystkie eksperymenty, ślady i wszystko zostało całkowicie unicestwione a pole magiczne i efemerydy zamaskowały co tu się robiło.
Śladów nie ma.

# Lokalizacje:

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Myśliński
                1. Bobrów
                    1. Centrum
                        1. schronisko dla psów Przyjaciel
                    1. Obrzeża
                        1. Siedziba firmy LegioQuant
                            1. Lapisowana piwnica
                            1. Tajne laboratorium i baza


# Stakes

Żółw:

- Czy jeńcy przeżyją? : nie, nic nie zostało
- Czy Timor Koral przeżyje? : nie, nie miał szans
- Czy LegioQuant zostanie zniszczone? : nic nie zostało, wszystko zniszczone

Gracze:

B: Czy Marcelin będzie w stanie zinkorporować i wykorzystać dla Blakenbauerów firmę LegioQuant? : może by był... ale wszystko zniszczone.
D: Czy vicinius odnajdzie drogę do domu? : skończył w niebie.
K: Jak bardzo ucierpi Sebastian Tecznia? : cóż... od niego wyszła informacja o LegioQuant... tak, ucierpi.

# Idea:

- Sojusz Blakenbauerów ze Skorpionem może też czynić dobro.

# Zasługi

* mag: Hektor Blakenbauer jako wchodzący kulturalnie negocjator który po przemianie w Bestię poszedł masakrować ludzi.
* vic: Alina Bednarz jako karaluch, który po prostu nie ma szczęścia.
* czł: Klemens X jako ten, który ginie za sprawę ratując Hektora i Marcelina.
* czł: Anna Kajak, która niekoniecznie radzi sobie z walką ze studentami nie czującymi bólu ani strachu.
* czł: Olga Miodownik, która niekoniecznie radzi sobie z walką ze studentami nie czującymi bólu ani strachu.
* mag: Marcelin Blakenbauer, tracący ciężarówkę mag pokazujący umiejętności bojowe.
* mag: Margaret Blakenbauer, drugoliniowa czarodziejka której wielkim planem było taranowanie ciężarówką i zapewnianie karaluchów.
* czł: Waltrauda Werner, starsza pani zajmująca się schroniskiem dla psów w Bobrach.
* czł: Filip Szorak, student informatyki, który podobno wyjechał do Anglii i porzucił Łamignata
* czł: Jakub Ryjek jako przestraszony autorytetem Hektora szef LegioQuant (29 lat).
* mag: Timor Koral, koordynator i przełożony LegioQuant który nie chce umierać... więc zginął.
* mag: Mordecja Diakon, wybitna detektyw i samotniczka porwana przez LegioQuant jako kontroler i "super*system".
* vic: Oddział Zeta, elitarna grupa Blakenbauerów ofiarnie i kosztem strat wyciągający mocodawców z pułapki (straty: trzy Zety).
---
layout: inwazja-konspekt
title:  "Klinika 'Słonecznik'"
campaign: blakenbauerowie-x-skorpion
gm: żółw
players: kić, dzióbek
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [150304 - Ani słowa prawdy... (HB, AB)](150304-ani-slowa-prawdy.html)

### Chronologiczna

* [150304 - Ani słowa prawdy... (HB, AB)](150304-ani-slowa-prawdy.html)

## Punkt zerowy:

Ż: Dlaczego zatrudnili właśnie Was?
K: Bo Esme ma znajomości wśród arystokracji.
Ż: Dlaczego Esme dostała dyskretną prośbę o zainteresowanie się sprawą od członka rodziny?
B: Ponieważ ten "człowiek" był kiedyś magiem rodu Maus który utracił moc po Zaćmieniu i wymazali mu pamięć.
Ż: Skąd się znacie?
K: Rodzina chciała wrobić go w działanie zgodne z rodem a Esme była opcją ucieczki od losu bycia właściwym Diakonem.
Ż: Co poza pieniędzmi motywuje Was by pomóc czarodziejce-zleceniodawczyni?
B: Jej wrogowie są naszymi wrogami.
Ż: Dlaczego Wiktorowi osobiście zależy, by dowiedzieć się o co w tym wszystkim chodzi?
K: Cokolwiek to jest, może dać szansę na to, by kontynuować NIE-Diakońskie życie.
Ż: Co dostarcza Wam zleceniodawczyni by pomóc w tej misji (usługę)? 
B: Śledczy.
Ż: Co dostarcza Wam zleceniodawczyni by pomóc w tej misji (obiekt)?
K: Papiery umiejscawiające nas w świecie ludzi dające dostęp do wielu miejsc, no questions asked.
Ż: Jaka ścieżka jest trudna, ale pewna by rozwiązać problem?
B: Mamy dostęp do maga-świadka który wie o co chodzi, ale nic nie chce nikomu powiedzieć.
B->Ż: Jakie konsekwencje poniesie zleceniodawczyni jeśli nie znajdziemy osoby odpowiedzialnej?
Ż: Rozłąka z rodziną i lodówka u Blakenbauerów.
K->Ż: Dlaczego w tym wszystkim jest drugi zespół, ale nie wynajęty przez zleceniodawczynię?
Ż: Zespół zajmuje się dyskretnym znalezieniem Dromopod Iserat.
Ż: Co jest stawką tej kampanii w kwestii Drugiej Inwazji?
B: Ta mikrokampania decyduje, czy nieznana jeszcze strona sprzyja czy działa przeciw Blakenbauerom.
K: Kto z mało ważnych NPCów w chronologii Inwazji jest skrytym Diakonbauerem i ustalenie jaki jest jego cel wobec Hektora.
K->B: Dlaczego Wiktor trzyma się Esme?
B: Jak długo trzyma się z Esme, tak długo nie ma problemów wynikających z jego Diakonowości.
B->K: Z jakiego powodu Esme w ogóle pasuje obecność Wiktora?
K: Zdejmuje to presję z Esme, bo Myszeczkowie liczą na udane potomstwo i sojusz z Diakonami.
Ż: Z jaką nadzieją zleceniodawczyni wynajęła Esme + Wiktora, na co liczy:
B: Ona ma szczerą nadzieję, że ktoś komu teraz źle życzy a jest dookoła tej sprawy wyjdzie z tego lekko poszkodowany (przy okazji problemu, casualty :P)
Ż: Co Wam się nie podoba najbardziej w tej sprawie:
K: Zleceniodawczyni wyraźnie nie chce nam czegoś powiedzieć.
Ż: Dlaczego złym pomysłem jest wydobycie siłą informacji od tego maga, który jest świadkiem?
B: Bo ten mag jest zrzeszony i nic nie zrobił.
Ż: Jak nieżyjący Maus może pomóc Wiktorowi w queście "nieDiakonowania"?
K: Zostawił coś po sobie, coś, co Wiktor może znaleźć.
Ż: Jaki szlachetny czyn Esme z przeszłości zagroził odseparowaniem jej od rdzenia Myszeczków (kiedyś)?
B: Esme miała pecha trafić w środek akcji bojowej w ramach której przypadkiem uratowała życie Weinerowi. Nie wiedziała, że to Weiner, ale po fakcie było za późno.
Ż: Czemu Esme musiała kiedyś chronić Wiktora przed swoim rodem? 
K: Bo kiedyś podpadł jednemu z wyżej postawionych Myszeczków - wykorzystał swoje umiejętności dywersji w niestandardowy sposób.
Ż: Jak przedmiot który zostawił po sobie Maus jest skorelowany z arystokracją?
B: Jeden z rodów z kapitałem politycznym jest skłonny za ten przedmiot świadczyć niemałe usługi.
Ż: Jak to się stało, że Maus - nie mag - go posiada?
K: It has been bound to him. It would keep coming back.
Ż: Jak "dywersja w niestandardowy sposób"?
B: Pink Mist ("Róża Selerbergu"). Dywersja była bardziej skuteczna niż się wydawało.

## Misja właściwa

01. Spotkanie ze zleceniodawczynią. Ta przedłożyła im, że ktoś krzywdzi ludzi. Eksperymentuje na nich. Ma dla nich ciało człowieka, spustoszone przez magię. Przyleciała osa, dziabnęła go i odleciała i to spowodowało spustoszenie magiczne. Co jest celem misji? Znaleźć co robi te eksperymenty na ludziach i cokolwiek to nie jest, zniszczyć to. Mają śledczego, który ma im pomóc i papiery. Zapytana o szczegóły - nie chce, by jej imię wyszło, więc płaci więcej. Niech mówią do niej "Amelia".

02. Śledczy, Henryk Waciak, nie jest magiem. Ale jest dość kompetentnym śledczym wiedzącym o istnieniu magii. Przedtem "Amelia" nie pozwoliła mu się angażować w ten temat, więc czekał, ale sprowadził znajomego lekarza. Lekarz powiedział, że czegoś takiego jeszcze nie widział - organy wewnętrzne nie do końca działały, szok całego organizmu. Prześledzanie przeszłości i jego poprzednich ruchów wskazało, że był w szpitalu w Kopalinie.

03. Śledczy dowiedział się czegoś ciekawego. Ten konkretny martwy człowiek będąc w szpitalu był jedynym wysłanym do prywatnej kliniki "Słonecznik" przez lekarza. Dzięki uprawnieniom które otrzymali Wiktor i Esme udało się uzyskać się dostęp do rejestrów - jeśli to fałszywka, to zrobiona przez kogoś, kto się zna na rzeczy. Henryk Waciak dostał zadanie - ma sprawdzić wszystkie znane do tej pory martwe osoby i skorelować je z tym szpitalem i z kliniką Słonecznik. Henryk poszedł się tym zająć.

04. Śledczy zrobił odpowiednią korelację i wyszło mu, że wszyscy troje zmarli mieli coś wspólnego z kliniką "Słonecznik". Byli tam co najmniej raz. Pierwszego dane są nieznane, dwóch pozostałych zginęło kilka godzin po opuszczeniu kliniki. Polityczny zmysł Esme ostrzegł ją, że coś jest za prosto w całej tej sprawie. Podzieliła się tą informacją z Henrykiem, który potwierdził, że najpewniej tam. Esme sprawdziła, czy ta klinika ma jakiekolwiek powiązania z magami - tak. Dyrektorem tej kliniki jest Edwin Blakenbauer. Jest tam też 2-3 innych ludzkich lekarzy.
Poproszony przez Esme Henryk poszukał korelacji między trzema martwymi ludźmi. Nie znalazł niczego, poza tym, że wszyscy byli w przedziale 25-40 lat.

05. Esme i Wiktor wrócili do ciała. Wiktor rzucił zaklęcie mające na celu sprawdzenie na poziomie magicznym o co tu naprawdę chodzi, jak zginął nieszczęsny człowiek. Okazało się, że:
-- faktycznie był ukąszony przez osę, reakcja była inna niż powinna być przy ukąszeniu osy
-- w miejscu śmierci już jakiś czas był po ukąszeniu, więc "Amelia" nie mogła widzieć jak ukąsiła tego człowieka osa jeśli konkretnie widziała jego śmierć
-- mężczyzna z kimś walczył przed śmiercią
-- nagle doszło do spike'a energii magicznej który spowodował total organ failure; nie widać powodu tego spike'a (nie ma dopływu magii z zewnątrz).
-- gdyby nie było spike'a, pożyłby jakieś 2-3 tygodnie i trafił do jakiegoś szpitala zanim by jego życiu zagrażało niebezpieczeństwo.

06. Bohaterowie są w częściowym martwym punkcie. Chcą dowiedzieć się, czy Edwin ma coś z tym wspólnego, ale nie mają jak. Wiktor policzył i zekstrapolował wstecznie, że jeśli przyrost energii magicznej nie przez spike był w miarę liniowy, to zaczął się nie wcześniej niż 2 tygodnie temu. Gdzieś po drodze była ta tajemnicza "osa". Henryk został poproszony przez Esme, by prześledził dokładnie 2 ostatnie tygodnie działania martwego (Mausa). 
Dodatkowo okazało się, że zdarzało się wysłać ze szpitala innych ludzi do kliniki Słonecznik i ci ludzie nie ginęli (lub jeszcze nie zginęli). Ci dwaj poprzedni martwi ludzie przeszli z pominięciem szpitala.

07. Oki. Pojawiło się nowe pytanie - dlaczego ten lekarz w ogóle wysłał kogokolwiek do Słonecznika? Żeby się dowiedzieć mimo minimalnych umiejętności przesłuchiwania magicznego przez Esme zespół poczekał aż lekarz będzie szedł do domu. Gdy wszedł do klatki, niewidzialni Esme i Wiktor ogłuszyli lekarza i go wyteleportowali. Tam, jak lekarz był osłabiony i nieprzytomny, Esme go przesłuchała. Mimo silnego bonusu, było nędznie, ledwo się udało. Okazało się, że lekarz ma zakodowane że KONKRETNE zespoły objawów (duża część świadczy o potencjalne chorób magicznych) mają być natychmiast kierowane do Słonecznika a w niektórych wypadkach - pełna kwarantanna szpitala.
...ok...

08. Wiktor skonsultował przeczytane objawy na martwym Mausie ze znajomymi lekarzami magicznymi. Dostał odpowiedź - jest to wyraźne zatrucie jadem Dromopod Iserat, hipnotycznych skorpionów. Ale cała reszta, efekt tła magicznego... tego nie da się objaśnić. Wiktor sprawdził też pozostałe grupy objawów - są to choroby i problemy magiczne z którymi ludzka medycyna sobie nie potrafi poradzić. Wszystko wskazuje na to, że dla odmiany Edwin Blakenbauer wykonuje kawał dobrej roboty dla ludzi...

09. Wiktor skontaktował się ze znajomym magicznym lekarzem. Poprosił o to, by ten spróbował określić jaką dawkę otrzymał przed śmiercią ów martwy Maus. Lekarz powiedział, że jak otrzyma ciało to 24h później powinien być w stanie to określić. Esme skontaktowała się ze znajomymi terminusami i członkami rodu by poszukać czegoś więcej na temat Dromopod Iserat. Okazało się, że jej "wuj", Antoni Myszeczka ma nielegalną hodowlę Dromopod Iserat. Zwykle bardzo drogo sprzedaje jad, ale czasem sprzedaje skorpiony (i tak nikt poza niektórymi Myszeczkami nie umie ich opanować, nawet Diakoni mają straszne problemy i nie umieją utrzymać hodowli). No i 4-5 miesięcy temu dwóch magów i jeden Dromopod Iserat zniknęli. Klient się strasznie żołądkował, ale trudno, taki biznes...
Na pewno ten Dromopod Iserat powinien już nie żyć, chyba, że jakimś cudem trafili na wykwalifikowaną osobę do obsługi Dromopod Iserat.

10. Henryk Waciak powrócił z informacjami odnośnie ostatnich dwóch tygodni życia Mausa. Esme dokładnie sprawdziła te informacje rozmawiając z nim, próbując znaleźć nieścisłości i rzeczy udowadniające, że on i "Amelia" tworzą jeden wspólny front. I w wyniku przedstawiania faktów i pokazywania wszystkich informacji jednoznacznie Esme udało się upewnić, że faktycznie Henryk współpracuje z "Amelią". Nie fabrykuje faktów, ale wspiera to co ona mówi i wyraźnie wie więcej, jednocześnie uzupełniając luki.
Czyli "Amelia" i Henryk to jedna strona i to strona niekoniecznie szczera wobec swoich zleceniobiorców...
Esme i Wiktor uśpili Henryka i przeskanowali jego pamięć. Okazało się, że "Amelia" kazała mu naprowadzić Zespół na klinikę, bo tam są osy i że to osy wchodzą w interakcję z czymś we krwi ludzi i ci ludzie umierają. "Amelia" zaznaczyła, że nie ona tu dowodzi i że jak operacja się zakończy, Henryk odzyska swoją pamięć.

11. Oki. Wiktor i Esme wpadli na nowy, podły plan. Chcą wprowadzić Henryka Waciaka do kliniki Edwina. Ale do tego celu potrzebują, no, jadu. Wiktor porozmawiał z Henrykiem i powiedział mu, że mogą go wprowadzić do kliniki i zasymulować objawy. Detektyw nie zczaił, że chcą go zatruć jadem i się zgodził. Tymczasem Esme porozmawiała z wujkiem Antonim, który prowadzi główną farmę Dromopod Iserat. Poprosiła o dawkę jadu bo ktoś w Kopalinie szaleje z jadem Dromopod Iserat. Wujek się zgodził. Szybko. Za szybko. Ale Esme nie zczaiła. Antoni próbował wypytać Esme odnośnie tego, czemu się tym interesuje i skąd to ma. (6 vs 7 -> 11), Esme zwycięża. Wujek nie zagłębia się w szczegóły, został uspokojony.

12. Potraktowali Henryka jadem Dromopod Iserat, dali mu parę srebrnych wisiorków i wysłali do kliniki. Wytyczne: dowiedzieć się, co tam się dzieje. Ma sprawdzać co i jak się będzie w tej klinice działo. Zdecydowali, że trzeba tam pójść go asekurować. Jako, że mają listę objawów, Esme wejdzie tam i powie, że ojciec Esme ma objawy wskazujące na jakieś z tych chorób by dowiedzieć się czy będzie tam przyjęty.
Wiktor "snajperskie oko" Diakon przejrzał wizualnie zabezpieczenia kliniki. Nie jest szczególnie chroniona, ale ma opcję Total Lockdown. Nic dziwnego, patrząc po niektórych z magicznych chorób jakie są objawami...
Henryk dodatkowo załatwił pluskwę, by Wiktor mógł podsłuchiwać co się dzieje w gabinecie. Sygnały pójdą po hipernecie SŚ do Esme, jeśli będzie taka potrzeba.

13. Edwin zamknął się z Henrykiem w jednym z pomieszczeń. Jest dźwiękoszczelne. Pobrał mu krew, zbadał, potwierdził "kolejny". Spytał czy ktoś na niego czeka - rodzina. Serią pytań dowiedział się, że Henryk jest świadomy magii i że jego mocodawczynią jest niejaka "Amelia" interesująca się kliniką. Nie znalazł nic odnośnie Esme i Wiktora. Zamknął go w kajdanach i wezwał mentalistę przez telefon. Młoda, ładna czarodziejka weszła do gabinetu...
...i Wiktor wystrzelił. Pocisk ze snajperki z "Pink Mist" uderzył prosto w ścianę tamtego pomieszczenia i Edwin z mentalistką zajęli się sobą. Bardzo, bardzo pracowicie. W tym czasie Esme wpadła do środka, Existo Atomos zniszczyła kajdany, wyciągnęła dobierającego się do niej Henryka, pomogła mu kontrolnym ciosem w głowę się do niej nie dobierać i skoczyła przez okno. Ewakuacja udana, Edwin jest bardzo zajęty mentalistką. Pacjenci, potencjalnie, też. Severity level: 6/20 (15+ to total lockdown).

14. Oki... Henryk Waciak zdobyty. Edwin spacyfikowany. Sukces. Została tam pluskwa pozostawiona przez Henryka (w ubraniach; został ewakuowany nieco mniej ubrany). Niestety (1/k20 szczęścia) jedyne co udało się Wiktorowi podsłuchać to uroczo kompromitująca kłótnia mentalistki z Edwinem. Jej zdaniem, on to zaplanował by ją zgwałcić. Odchodzi i nie chce mieć z nim od tej pory już nic wspólnego. A rodzina ostrzegała ją by nie wchodziła w interakcje z Blakenbauerami. Edwin nie wyglądał na szczęśliwego. Jego ostatnie słowa odebrane "No, pięknie. Amelia, tak?".

15. Wiktor i Esme spotkali się z "Amelią". Wiktor przycisnął "Amelię" o tą osę. "Amelia" powiedziała, że osa jest, że jest kluczowa dla sprawy. Następnie Esme nacisnęła "Amelię" o to, kogo podejrzewa. Zgodnie z raportami Henryka, "Amelia" podejrzewa klinikę Słonecznik. Tyle, że Henryk nigdy nie składał żadnych raportów...
Esme nacisnęła "Amelię" tak, by wytrącić ją z równowagi. Mówi, że Henryk jest w klinice Słonecznik, poszedł tam na ich zlecenie a Edwin go porwał i przetrzymuje. I wezwał mentalistkę do odczytania jego pamięci. Esme naciska mocno. (7+2 vs 6/8/10 -> 8). Esme dała radę ją mocno przydusić.
"Amelia" pękła. Powiedziała, że wiedziała o Blakenbauerze od samego początku i to co robi prowadzi do śmierci ludzi. Że niezależnie od wszystkiego ci ludzie będą umierać. Ona w to wierzy. Zapytana, czy ludzie których leczy Edwin mogliby przeżyć w innych okolicznościach, krzyknęła, że ona tylko wykonuje zadania.
Wyciągnęła Artefakt po martwym Mausie. Dała go Esme i powiedziała, że jest ich. I że arystokracja magiczna tylko może tego dotknąć.
Zapytana, czemu schowała pamięć Henryka (4 v 4) odpowiedziała, by schować informacje przed nimi. I jak widać, słusznie, bo czytali jego pamięć.
Na odchodnym powiedziała "Uratujcie Henryka. I wystrzegajcie się os".
Tymczasem Edwin, mający próbkę krwi Henryka, nakierował osy na niego. Standardowa reakcja.

16. Z Esme skontaktował się terminus, tien Tadeusz Baran (patrz: "Lord Jonatan"). Powiedział, że dostał jakieś zgłoszenie od Edwina Blakenbauera odnośnie jadu Dromopod Iserat, odnośnie ludzi wiedzących o magii (łamanie Maskarady) i odnośnie ataku na jego klinikę. Ale... to Blakenbauer. Czy może Esme i Wiktor, tak po znajomości, wiedzą coś na ten temat? Bo jemu nie uśmiecha się latać po okolicy i szukać człowieka który wie o magii...
Esme i Wiktor z radością skierowali podejrzenia na Edwina, zwłaszcza w kwestii używania jadu Dromopod Iserat. Powiedzieli, że to przecież perfekcyjne alibi - ściąganie terminusa na coś, co on sam zrobił. Baran potwierdził, zdecydowanie jest to warte przeskanowania. (10 vs 9 -> 10) Nie tylko zamknie klinikę na czas badania, ale i jak się czegoś dowie to podzieli się informacjami.

17. Wieczór. Esme i Wiktor wrócili do miejsca gdzie jest Henryk i od razu zauważyli obecność os na szybach. Wiktor postawił dookoła nich tarcze magiczne i zaraz przeniósł je do słoika. Mają próbki doświadczalne! Dobra. Czas na pauzę...

Ż: On czy ona?
K: Ona.
Ż: Co jej zrobiłeś, za co powinna się na ciebie obrazić?
B: Small accident with Pink Mist she gave him.
Ż: Z jakiego znanego rodu magów jest z domu?
K: Sowińskich.
Ż: Z którym ZNANYM NAM Diakonem jest najbliżej powiązana niezależnie od sposobu?
B: DiDi.
Ż: Dlaczego?
K: Bo zrobiło się jej żal.
Ż: Jak ma na imię?
B: Anabela

...koniec pauzy. Wiktor zadzwonił do Anabeli i poprosił ją o spotkanie w Kopalinie. Anabela umówiła się z nim na rano.
A Tadeusz Baran tymczasowo zamknął Edwinowi klinikę. Baran powiedział magom współpracującym z Edwinem, że są pewne podejrzenia a i to co powiedziała mentalistka swoje zrobiło - magowie Edwina opuścili. Ludzki personel tak samo, bo klinika zamknięta. Baran usunął uwarunkowania lekarzy, tymczasowe. 
Edwin jest naprawdę, naprawdę zły. 

18. Ranek. Mieszkanie na tarczach fizycznych, klika os na tarczach. Osy w słoiku. Henryk dobrze śpi, Wiktor i Esme gotowi do działania. Anabela się pojawiła. 51 lat, "szalona ciotka". Wygląda na... 51 XD. Więc Anabela pracuje nad osami, poproszona przez swojego "Ktulusia" (drugie imię Wiktora to jest "Ktulu") a w tym czasie Wiktor i Esme rozbudowują defensywy.
Esme dostała telefon od "Amelii". Dzwoniła, by się upewnić, że Henrykowi nic się złego nie stało. Nie. Odsypia. Podziękowała i się rozłączyła oraz dała znać wyżej.
Godzina 13:27. Anabela uzyskała przełom.
Więc tak: osy to arcydzieło. Są nakierowane bezpośrednio na Henryka; osoba je konstruująca musiała mieć dostęp do krwi Henryka. Ich jad to w rzeczywistości środek reagujący silnie z jadem Dromopod Iserat. Jeśli osa użądli osobę bez jadu D.I. we krwi, zwyczajnie go ukąsi. Ale z jadem, wyzwoli energię magiczną i zacznie to powoli wyłączać organy. Śmierć w kilka miesięcy. Ale przedtem trafi do szpitala... a tam znajdzie ofiarę Edwin...
Czyli W TAKI SPOSÓB Edwin jest w stanie zdobyć dostęp do właściwych pacjentów o właściwych objawach...
Co może uratować Henryka? Musi wyjechać, osy mają zasięg, nawet 50 km daje mu gwarancję że nic złego go nie spotka.

19. Oki. Czas na operację zamknięcia permanentnego kliniki Słonecznik. By to zrobić, wpierw Esme wysępiła ze zniżką (za co płaciła "Amelia") jednego Dromopod Iserat, potem Wiktor wynajął grupę "The Zajcew Goons" którzy mieli włamać się do kliniki Słonecznik "by coś wynieść" (a naprawdę podłożyć hipnoskorpiona). Pojawił się terminus, tien Tadeusz Baran, aresztował Zajcewów i odkrył Dromopod Iserat.

20. Wynik: Był spory, medialny proces (profesor Otton Blakenbauer to załagodził), w wyniku którego klinika jest permanentnie zamknięta. Jednym z oskarżycieli posiłkowych była młoda mentalistka, Dalia Weiner, która była idealistką "podle wykorzystaną przez Edwina". Nie widziała samego Dromopod Iserat, ale pojawiał się on w dyskusjach. I tak, na pewno Edwin prowadził jakieś eksperymenty.
Edwin próbował się bronić, że przecież jego JEDYNYM celem było pomaganie ludziom. Był lekarzem. Chciał lekarzyć. Nikt jednak mu nie uwierzył, nie przy czarnym rodzie i czarnym PR Blakenbauerów...
"Amelia" podziękowała. Rozstali się.
Esme została z artefaktem, którego nie rozumie i nie wie co robi. Jeszcze. Poza tym, że jest ważny. Choć nie wie czemu.
  
  
  
"Myszeczki - ród hodujowców" - Kić.
"Czyli śmierć w męczarniach lub życie w męczarniach, kłuty przez osy do końca dni swoich" - Miaushi.

# Zasługi

* mag: Esme Myszeczka jako najemniczka trzymająca się dość daleko polityki rodu
* mag: Wiktor Diakon jako najemnik trzymający z niezbyt normalną ciotką
* mag: Estera Piryt jako "Amelia", czyli zleceniodawczyni troszcząca się o świat ludzi i mówiąca więcej kłamstw niż prawd
* czł: Henryk Waciak jako kompetentny śledczy, choć człowiek, nie znający świata magów
* czł: Onufry Maus, którego jedyną rolą na misji było być totalnie martwym
* mag: Edwin Blakenbauer jako Blakenbauer, który dla odmiany robi coś dobrego dla świata. A przynajmniej tak to wygląda.
* mag: Antoni Myszeczka jako posiadacz dorodnej hodowli Dromopod Iserat
* mag: Dalia Weiner jako lekarka/mentalistka z SŚ która wierzyła w czyste intencje Edwina (a skończyło się seksem na kozetce...)
* mag: Tadeusz Baran jako leniwy terminus SŚ który woli słuchać doniesień o Blakenbauerach niż robić pracę terminusa
* mag: Anabela Diakon jako czarodziejka współczująca Wiktorowi i podziwiająca kunszt Blakenbauerskich os. Nee Sowińska.

# Lokalizacje:

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Obrzeża
                        1. Klinika Słonecznik
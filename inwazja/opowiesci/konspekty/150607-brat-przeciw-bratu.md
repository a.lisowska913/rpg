---
layout: inwazja-konspekt
title:  "Brat przeciw bratu"
campaign: powrot-karradraela
gm: żółw
players: kić, dust, dzióbek
categories: inwazja, konspekt
---

# {{ page.title }}

## Wątki

- Szlachta vs Kurtyna
- Wojny wielkich gildii Śląska
- Wszystkie dziewczyny Marcelina

## Kontynuacja

### Kampanijna

* [160809 - Awokado dla wampira (temp)](160809-awokado-dla-wampira.html)

### Chronologiczna

* [150604 - Proces bez szans wygrania (HB, SD, PS)](150604-proces-bez-szans-wygrania.html)

### Logiczna

Skąd się wzięła Infernia Diakon i co ma do Hektora:

* [150411 - Dzień z życia Klemensa (Kx, AB)](150411-dzien-z-zycia-klemensa.html)

## Punkt zerowy:

Ż: Co było między Infernią i Ignatem co było dość... destrukcyjne na KADEMie?
K: Ignat nie odczytał "tych oczywistych sygnałów" od Inferni i dała mu taki sygnał który ma zrozumieć.
Ż: Infernia-Diakon/Zajcew. Ignat-Zajcew. Co ich łączy?
D: Dzielą wspólną pasję, egzotyczne zwierzęta.
Ż: O co KADEM może oskarżyć Marcela Bankierza?
B: Przy jego obecności zniknęły wrażliwe materiały - jak dostać się na poligon doświadczalny.
Ż: Czemu Inferni bardzo zależy, by być ubraną właśnie w ten mundur na bal u Emilii?
K: Ten mundur sprawi, że Emilia nie będzie pewna co się stanie jeśli Inferni nie wpuści. Ten mundur ma znaczenie rytualno-polityczne.
Ż: Kluczowe miejsce w Kopalinie, w którym mógł wystąpić Paradoks?
D: Ratusz.
Ż: Co wskazuje na to, że Marcelinowi ktoś z rodziny "pomógł" wpaść w opały?
K: Nikt nie naciska, by Klemens lub Alina zajęli się rozwiązaniem tego problemu mimo jego wyraźnej wagi.
Ż: Dlaczego z perspektywy prawa SŚ Marcelin naprawdę tym razem wpadł w opały?
B: Ponieważ jego pobyt na KADEMie został oficjalnie usankcjonowany - potencjalne szpiegostwo.
Ż: Czemu to, że Marcel wypuścił tą informację na światło dzienne tak zirytowało Infernię?
D: W publicznej opinii Infernia stała się "jak każdy Zajcew" a walczyła by opinia była "że Diakon".
Ż: Jaką korzyść z tej misji może uzyskać KADEM przeciwko Świecy?
B: KADEM uzyskuje przyczółek na obszarze pomiędzy KADEMem a Srebrną Świecą.

## Misja właściwa:

Hektor - w końcu prokurator - siedzi w biurze i przesłuchuje ludzkiego świadka w sprawie Szymona Skubnego. Wtem przychodzi goniec ze Świecy, mag, z biura Emilii i rzuca zaklęcie którym unieszkodliwia świadka Hektora. Super. Od razu Hektor niezadowolony "można było inaczej" na co mag "musiałbym czekać". Goniec przekazuje Hektorowi list przekierowany z biura Świecy przez Emilię do Hektora; chodzi o prowadzenie oskarżenia jakiegoś maga. Hektor, zadowolony, bierze ten list i potwierdza odbiór (w ten sposób przyjął sprawę; nie zapytał oraz nie zapytał LISTU - bo nie wiedział że może). Gońca wyekspediował, wrócił do przesłuchania w sprawie Szymona Skubnego. A potem przeczytał list i się okazało, że przyjął... oskarżenie Marcelina. A dowody ma silne. Marcelin oskarżony o konspirację, szpiegostwo, zdradę oraz przekazanie danych poufnych JEDNOCZEŚNIE. Są przeciw niemu dowody i da się zmontować oskarżenie...
A oskarżycielką jest Infernia Diakon. Eks Marcelina. Którą rzucił.
A Świeca - dokładniej, Emilia, przekazała to Hektorowi.
Najstraszniejsze jest to, że Emilia załatwiłiła, że całej sprawie będzie przypatrywał się audytor by upewnić się, że Hektor będzie sprawiedliwy. A tak to zrobiła, że w powszechnej opinii ona tego nie zrobiła; po prostu gorąco zapewniała o swojej pewności uczciwości i surowości Hektora.

Hektor koniecznie chce się spotkać z Silurią i Pawłem - Infernia Diakon jest z KADEMu, może oni coś pomogą. 

Tymczasem Siluria spotkała się z Ignatem, który z radością powiedział, że założył się z kolegą o autobus śledzików syberyjskich. I je wypuszczą na poligonie na KADEMie. Możliwe do rozrodu ^_^. I Infernia faktycznie zrobiła taki numer, i dzięki temu będzie autobus śledzików :D. A ona ma nagrania i dowody... po stronie SŚ nie ma chętnego obrońcy, więc będzie obrońca z urzędu. A wszystko po to, by Ignat i Infernia mieli swoją hodowlę śledzi syberyjskich i mogli zrobić konkurs hodowców. A w ogóle to dlatego, że ten mag w zły sposób rzucił Infernię...
Ignat poprosił Silurię o kieszeń podprzestrzenną na śledziki. Taką, by mógł swoje hodować lepiej niż Infernia.

Hektor zadzwonił do Silurii prosząc o pomoc w kolejnej sprawie. Poprosił o spotkanie... asap. Wieczorem. Siluria od razu stwierdziła, że Hektor chce się z nią spotkać i po prostu umówić. I jeszcze z Pawłem. Znaczy: trójkącik. Ale taki trójkącik może nie do końca być w stylu Pawła. Siluria próbowała mu delikatnie wyjaśnić, że Paweł nie przepada za facetami, Hektor nie zrozumiał. Więc Siluria zasugerowała NIBY NIEWINNIE (9v6) i Hektor ZROZUMIAŁ co do niej powiedział. Że z jej punktu widzenia Hektor zaproponował Pawłowi i Silurii trójkącik (nawet czworo- z demonem :P) i ona się zgodziła... ale Paweł może mieć obiekcje.

Tymczasem Paweł był oddelegowany do podkorygowania GS Aegis 0003; Nihilus został uszkodzony w starciu Inferni i Ignata. A Marian i Eryk są na poligonie. Został Paweł...
Zadzwonił do niego Mariusz Trzosik; Lucjan Kopidół odpalił w pomieszczeniu jakiś artefakt pryzmaturgiczny (dokładniej: coś co się mu samo zepsuło i eksplodowało). Więc on ma problem... coś się dzieje na poligonie. Zginęły 3 żabolody, 2 zniknęły. Eryk Płomień tam już jest, jest tam też Warmaster na randce (co XD) i Marian Łajdak. Ale Marian nie daje znaku życia, trzeba to sprawdzić.
Oki... to nie brzmi dobrze. Zaraz... Ilona i Mariusz są tam uwięzieni przez artefakt, tak? Co się stało? Gdzie Lucjan? Z żoną. Jak opuścił KADEM? O_O. Telefon do Norberta; ten szybko zrobił diagnostykę systemu i wyszło, że Ignat i Infernia wysadzili systemy zabezpieczeń. 
I na to telefon Silurii. Paweł "dobił" działem strumieniowym GS Aegis, by to się nie zregenerowało za bardzo (nie jest w 100% aktywne) i poszedł uzbrojony na poligon.

Podczas rozmowy Siluria przekazała Pawłowi, że śledzie może są na KADEMie. I nalega, żeby się spotkać z Hektorem.

Paweł porozmawiał z Warmasterem; powiedział jak wygląda sytuacja. Warmaster z Erykiem powiedzieli, że znajdą Mariana Łajdaka. I NIE TO NIE JEST RANDKA. Warmaster się zdziwił że wyłączony jest containment system oraz że wyłączony jest monitor... włączył je i zdecydowali się przejść. Wszystko bezpieczne, dadzą znać co i jak. Norbert do nich dołączył a po Lucjana wysłał Andżelikę.

A Paweł i Siluria spotkali się w "Rzecznej Chacie" z Hektorem Blakenbauerem. Hektor poprosił Ofelię (barmankę) o espresso, Ofelia z radością mu dostarczyła mówiąc, że z Etiopii. Oczywiście, skłamała - ale espresso dobre. Siluria i Paweł dostają coś innego - drink, który ma oszałamiający zapach a płyn neutralizuje.
Hektor przyznał się, że jego zleceniodawczynią jest Infernia. Siluria z radością wyjaśniła, że Infernia przejęła główne cechy rodów Zajcew i Diakon - obie pełne dominanty rodów. Aha, Infernia jest magiem w Instytucie Zniszczenia.

"Z tego co słyszałam, nie da się przegrać tej sprawy" - Siluria do załamanego Hektora o Inferni vs Marcelin

Siluria zapewniła Hektora, że oskarżenie jest prawdziwe, sprawa jest prosta a dowody są uczciwe. Hektor jeszcze bardziej załamany. Hektor próbuje jak może "zależy mi bardziej na uczciwym procesie niż na wygranym". I chce wszcząć własne śledztwo. I prosi o pomoc Silurię i Pawła.
...moment. Jeśli Infernia wycofa oskarżenie, NIE będzie procesu. To znaczy, że Marcelin może nie dostać. JEST NADZIEJA!
...I Hektor przyznał się że to jego brat.
Siluria banan. Czemu Hektor się tego w ogóle podjął? JAK TO SIĘ STAŁO? Polityka. Nie minął tydzień i JUŻ Hektora ktoś wrobił.

Paweł zadał proste pytanie: jak on może pomóc? Hektor powiedział szczerze - znają Infernię i są na KADEMie. On może sam nie wyciągnąć prawdy a chce pomóc bratu...

Norbert zadzwonił do Pawła. Powiedział mu, że są niezłe jaja - faktycznie ktoś był na poligonie. Ostrzeliwał się z Warmasterem, po czym się ranteleportował Paradoksalnie. Zgodnie ze wskazaniami VISORa pierwszym teleportem przesłano żaboloda który wyparował, więc dwie pozostałe ewakuacje poszły już ranteleportami. I jeden z nich jeszcze ranteleportował...
Dodatkowo, Marcel Bankierz miał do czynienia z dokumentami dotyczącymi poligonu. I to samo w sobie silnie promieniuje na współodpowiedzialność Świecy, zwłaszcza, że Marcela wywalili ze Świecy a technologia by opuścić Poligon z postawioną kwarantanną już wymaga terminus-level artifacts... Norbert powiedział, że będzie chciał by Hektor zajął się też tą sprawą a dokładniej: Marcelem Bankierzem.

A kto wyłączył systemy zabezpieczeń? Eksplodował artefakt Lucjana i pod wpływem tego artefaktu wyłączono część systemów. Ta eksplozja była sama w sobie nieco dziwna; nie powinno to nastąpić. To też będzie musiało być przeanalizowane na KADEMie.

Ale serio - cała ta akcja by ukraść niezbyt cennego żaboloda? Bez sensu. Ale upokorzenie KADEMu jest...

Siluria poszła więc porozmawiać z Infernią. Tam dowiedziała się kilku ciekawych rzeczy:
- Infernia ma świetne dowody, ale jest skłonna się wycofać.
- Niestety, durny Ignat założył się o autobus śledzików syberyjskich... więc jeśli Infernia wycofa, musi ODDAĆ taki autobus.
- Z tego wynika, że Infernia chce dostać DWA autobusy śledzików syberyjskich od Marcelina. I przeprosiny. "Z agrafką lub bez".
- Do tego, Infernia chciałaby mundur. Na SŚ jest czarodziejka która nie lubi KADEMu i potrafi dopasować odpowiedni mundur do rodów Inferni. Zrobiłaby furorę na balu Emilii.

Tymczasem Hektor porozmawiał z Marcelinem.
- Marcelin faktycznie przekazał Inferni informacje (szpiegostwo)
=== odnośnie tego, kto w SŚ interesuje się śledziami syberyjskimi
=== gdzie w SŚ znajdują się hodowle śledzi syberyjskich
=== poprosił przyjaciółki z SŚ o informacje niejawne dotyczące badań SŚ w zakresie śledzi syberyjskich
- Marcelin wkręcony przez Infernię namawiał ją do przekazania pewnych informacji
=== spytał ją czy może wpaść wieczorem, powiedziała, że nie może bo są systemy włączone
=== więc zapytał kiedy może bo nie są aktywne...

Innymi słowy, są dowody i to silne... bo Marcelin FAKTYCZNIE to wszystko zrobił.

Rozmowę przerwał im Edwin. Pokazał Hektorowi najnowsze wydanie "Paktu":
- sylwetka Hektora pokazanego jako bezwzględny i oportunistyczny karierowicz
- sylwetka Marcelina jako porzucającego dziewczyny playboya
- prezentacja rodu Blakenbauerów jako czarnego, ale chwilowo "grzecznego"
- podobno Otton powiedział, że Hektor nie robi niczego czego Blakenbauer nie powinien (ofc z nim nikt nie rozmawiał)

Ogólnie, "Pakt" jedynie dowala Blakenbauerom i zwiększa śmieszność na Hektora.

Plan Hektora na to jest bardzo prosty. Dogadać się jakoś z Infernią i odpłacić Emilii.

Problemy do rozpatrzenia dalej:
- kto stoi za atakiem na KADEM? Dlaczego?
- kto był na poligonie? Jaki był cel wejścia na ten poligon? ŻABOLODY SERIOUSLY?
- czemu Pakt w to wszedł? (Założenie: że to robota Emilii)
- kto skoordynował to wszystko?
- Marcel Bankierz i jego rola.
- jaka czarodziejka ma koncept munduru dla Inferni?

# Streszczenie

Eks Marcelina - Infernia Diakon - wrobiła go w zdradę i inne takie. Hektor będzie prokuratorem... dodajmy do tego, że "Pakt" uwziął się na Hektora i Marcelina. Jednocześnie, ktoś zaatakował poligon KADEMu i... ukradł żabolody. Infernia jest skłonna wycofać oskarżenie za 2 autobusy śledzi syberyjskich. Wszystko przez zakład z Ignatem. Oczywiście.

# Lokalizacje:

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Centrum
                        1. KADEM Primus
                            1. Restauracja
                        1. Prokuratura
                        1. Klub Magów Rzeczna Chata
                    1. Las Stu Kotów
                        1. Poligon KADEMu
                    1. Obrzeża
                        1. Rezydencja Blakenbauerów

# Zasługi

* mag: Siluria Diakon, która wie wszystko o wszystkim od wszystkich (Ignat - śledziki, Infernia - mundur) i którą wszyscy znają (Ofelia) i kochają (w przenośni lub nie).
* mag: Paweł Sępiak, który próbował wszystko ogarnąć i by nie oszaleć po zebraniu danych scedował to na Norberta, samemu skupiając się na Hektorze.
* mag: Hektor Blakenbauer jako NIE zimny karierowicz, którego wrobili by na takiego wyglądał w oskarżanie Marcelina przed sądem SŚ. W lekkiej panice.
* mag: Infernia Diakon, krew Zajcewa i Diakona, którą rzucił Marcelin za co wciągnęła go w perfidną pułapkę. Marzy o eleganckim mundurze.
* mag: Ignat Zajcew, który założył się o autobus rozrodowych śledzików syberyjskich z * magiem Świecy, co strasznie skomplikowało żywot Hektora (bo Infernia podniosła stawkę).
* mag: Marcel Bankierz, który wyjątkowo zirytował Infernię i być może jest współodpowiedzialny za atak na KADEM... wyleciał z KADEMu na zbity pysk.
* mag: Mariusz Trzosik, który pomimo wpływem eksplodującego artefaktu próbował zwrócić uwagę wszystkich na Poligon KADEMu.
* mag: Lucjan Kopidół, którego artefakt astralno-pryzmatyczny eksplodujący na KADEMie spowodował mnóstwo problemów i doprowadził do 'breach'.
* mag: Norbert Sonet, przejął tymczasowe dowodzenie na KADEMie by doprowadzić wszystko do porządku. A nigdy nie chciał dowodzić.
* mag: Warmaster, który wraz z Erykiem opanował problemy na poligonie KADEMu. Ponadto, NIE był na randce.
* mag: Emilia Kołatka, która przekierowała list do Hektora nie chcąc oskarżać sama Marcelina; obserwuje go z uwagą i załatwiła audytora by Hektor ocenił odpowiednio uczciwie.
* mag: Marcelin Blakenbauer jako podsądny który się katastrofalnie wpakował między Infernią a Emilią.
* mag: Ofelia Caesar, która świetnie zna Silurię... i w sumie wszystkich w "Rzecznej Chacie".
---
layout: inwazja-konspekt
title:  "Naszyjnik Przenośnych Wspomnień"
campaign: powrot-karradraela
players: kić, dust
gm: żółw
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [170417 - Ratując syrenopnącze (SD)](170417-ratujac-syrenopnacze.html)

### Chronologiczna

* [170118 - Ludzka prokuratura a magowie... (HB, AB, DK)](170118-ludzka-prokuratura-a-magowie.html)

## Kontekst ogólny sytuacji

Test przeciwnika - [Mimik Symbiotyczny](http://riggeddice.gitlab.io/rpg/inwazja/opowiesci/viciniusy/1701-mimik-symbiotyczny.html) ;-).

## Punkt zerowy:

* Ewelina nosiła Mimika; dawał jej niesamowite moce przy rekrutacji
* Beata zabrała Ewelinie naszyjnik, jak ta trafiła do szpitala
* Marcelin uratował Ewelinie życie wypełniając ją energią życiową
* Beata wykorzystała moc Mimika by używając marzeń Eweliny zgwałcić rapera
* Beata została złapana i wsadzona do więzienia.
* Marzenia Beaty - być ceniona jak ojciec (Policjant, Ceniony, Genialny detektyw, Zabity przez przypadek, Pomimo ze łapał morderców śmierć go nie łapała a zabił go pijany kierowca)
* Naszyjnik przechwyciła INNA policjantka, Katarzyna Leśniczek...
* Katarzyna pnie się do góry w Pirogu Górnym...
* Katarzyna też została wyczerpana i trafiła do szpitala w ostatniej chwili
* Naszyjnik został przechwycony przez Julię, lekarkę, która nie chce być zapomniana...

## Misja właściwa:

Dzień 1: 

Kleofas Bór odwiedził Hektora w prokuraturze, z zafrasowaną i smętną miną. Okazuje się, że w klubie Pełnia policjantka zgwałciła rapera. I raper nie chce tego zgłosić. Oprócz tego w klubie, na tym zapleczu, rapera zgwałciły jeszcze trzy inne dziewczyny - pod muszką. I jedna z nich zgłasza gwałt (ofiara gwałcąca) - bo musiała zrobić to pod muszką... Hektor jest... podłamany.

Przybyła Patrycja Krowiowska, na żądanie Hektora. Dostała zadanie zbadać te materiały o gwałcącej policjantce. Patrycja poznała Beatę Solniczkę - dziewczynę o ogromnej motywacji, córkę legendarnego detektywa (przejechanego przez pijanego kierowcę). To jej pierwszy wyskok ever. Patrycja powiedziała, że nikt przy tym nie majstrował. I zdaniem Patrycji - to wygląda na zrobione z niezłą premedytacją.

Hektor wezwał Brysia, Bednarz i Bora. Drużyna B. Kazał im iść za tropem. Bryś chciał zobaczyć filmik; GDZIEŚ już to widział. Poszli jeszcze raz obejrzeć filmiki i - po rozkazach Hektora - Bryś i Patrycja analizują filmiki na DarkPublicFetishes69.xxx a Dionizy i Alina zostali wysłani do klubu Pełnia. Kleofas i Anna Kajak mają ściągnąć Beatę...

Klub Pełnia. Łepek rozkleja plakaty "BEZKARNOŚĆ POLICJI". To oczywiście Stanisław Pormien; który bardzo nie lubi policji i uważa to za naprawdę podłą czynność. Dionizy i Alina podszywają się za reporterów skrajnej gazety "!LIBERATOR!". Dionizy wkręcił Stanisława w pseudonim "Cień Ciemności". Dionizy postawił Stanisławowi mocne piwo. I lekko wstawiony "Cień Ciemności" ma małe siły w konflikcie ;-).

Stanisław wyjaśnił co następuje:

* Był osobiście na wystąpieniu rapera; jego koledzy też byli. On poszedł na zaplecze na zioło. Wtedy wparowała policjantka. Nim się w ogóle nie zainteresowała (urażona duma).
* Wszystko robi z własnej woli, nikt mu nie zapłacił. Policja gwałci prawa anarchistów, ale... teraz gwałci ciała!
* Raper Wojtek dał radę ;-). Cztery dziewczyny przeleciał. Normalnie to nie powinno być możliwe.
* Alina zauważyła, że Stanisław zwraca szczególną uwagę na naszyjnik. W perspektywie: częściowo rozebrana policjantka, nagie dziewczyny... a on mówi o naszyjniku. Artefakt?

Dionizy zadzwonił do Kleofasa; ten powiedział, że wracają już z Beatą. Dionizy - ok, są pokonani - ostrzegł Hektora... sprawdzili jeszcze klub Pełnia - it happened.

Bryś powiedział Hektorowi co znalazł - to jest odwzorowanie filmu. Film ten jest o feminizmie uciemiężającym mężczyznę (śpiewaka) a "ta jedyna" z pistoletem jest tą jedyną ;-). Bryś pokazał, że ten gwałt w filmie to technicznie JEST gwałt, ale oni wszyscy w praktyce są wyzwoleni. Full-consensual. I tak samo tutaj - policjantka pozwoliła innym odejść...

Patrycja też powiedziała coś Hektorowi - ten film jest totalnie niszowy. A jak zrobiła cross-section między Beatą i Wojtkiem... nie znalazła nic. Nigdy się nie spotkali. Nigdy też nie spotkała się Beata z innymi dziewczynami gwałcącymi rapera.

Hektor kazał Patrycji dowiedzieć się jak najwięcej odnośnie tego filmiku, kto miał dostęp itp. 

Policjantka Beata u Hektora. Nie ma naszyjnika i jest stosunkowo słaba. 

* Czyżby się pani nie wyspała? - Hektor
* Przecież pan wie, panie prokuraturze. Przyznaję się - Beata
* Zabrzmi to głupio... ale... urodziłam się tylko po to, by to zrobić... - Beata

Hektor jest wyjątkowo łagodny i przyjacielsko przesłuchuje Beatę:

* Ma te myśli od około 2 tygodni. Coraz bardziej zaprzątało jej to myśli. Marzyła o tym. Śniła o tym. Obsesja...

Hektor przesunął się na aggressive.

* Beata ukradła komuś naszyjnik ze szpitala jakieś 20 dni temu.
* Teraz ktoś na komendzie jej zabrał ten naszyjnik. Ukradł jej...

Hektor wzmocnił zaklęciem przesłuchanie. Sprawdza co się stało z naszyjnikiem. Jak pozyskała, jak utraciła...

* Pozyskała naszyjnik zabierając go ze stolika przy łóżku. "Szeptał do niej". "Obiecywał jej moc i spełnienie marzeń". (Hektor znalazł info które to łóżko)
* Utraciła naszyjnik odkładając go na biurko gdy przyszedł Bór i Anna.

Hektor chce usunąć z niej uzależnienie. Squarkował to - udało mu się. Beata jest... czysta.

Mając datę i łóżko Hektor poszedł do Patrycji. Niech ona znajdzie informacje czyje to łóżko było - a Dionizy niech tam jedzie i porozmawia z lekarzami. A Hektor i Alina jadą na posterunek, szukać Naszyjnika...

Szpital. Dionizy. Patrycja ostrzegła Dionizego - Ewelina Nadzieja miała tamto łóżko; ogólne wyczerpanie organizmu ORAZ uzależnienie od narkotyków. 

Robert Pomocnik, lekarz, wyjaśnił poprawiający się stan Eweliny Dionizemu. Wychodzi z uzależnienia; dochodzi do siebie. Wielka strata. Taka zdolna rekruterka...

Telefon od Patrycji. Okazuje się, że Ewelina marzyła o raperze Wojtku. I ona wchodziła na tą samą stronkę co Bryś ;-). Czyli Beata zrobiła rzeczy, o jakich Ewelina marzyła. Dionizy podziękował.

Dionizy przekonał Pomocnika, żeby ten wypuścił z nim Eweliną. I zaraz się załamał. Naszyjnik z jakim Ewelina przyszła... jest INNY niż ten który miała Beata. Zmiennokształtny naszyjnik.

TYMCZASEM NA KOMISARIACIE. Hektor i Alina. Oczywiście, Naszyjnika już nie ma.

Hektor poszedł pogadać z komendantem załatwić dostęp do nagrań; Alina przekształciła się w Beatę i zrobiła haję o to gdzie jest jej naszyjnik. Jeden z policjantów powiedział, że Katarzyna zabrała naszyjnik by zanieść go Beacie. Podał trasę Katarzyny...

Alina dotarła do Katarzyny. Niedaleko centrum handlowego. Alina uniknęła detekcyjnego skanu Mimika Symbiotycznego; ma headstart. Katarzyna wyraźnie idzie do centrum; chce zgubić Naszyjnik. Alina przybrała tożsamość reporterki "!LIBERATOR!" i podeszła do Katarzyny porozmawiać o Beacie i jej ekscesach. Alina chce kupić czas i odciągnąć od tłumu... 

Udało się. Hektor wszedł na scenę, z zaskoczenia. No i nowy plan - zdominować mimika...

* ALINA: skłania Katarzynę do wyjęcia mimika. 5v6->SUKCES. Katarzyna, słysząc o zachowaniu Beaty wobec naszyjnika wyjęła "swój" by pokazać.
* HEKTOR: 6v6-> F (Kasia ma na sobie srebro), S.

Alina zajęła się głośno krzyczącą Kasią; tymczasem Hektor podszedł i wziął jej zdominowany Naszyjnik...

EPILOG:

* Hektor wyczyścił wszystkich ludzi którzy mieli jakieś dane.
* Dionizy zeznał, że "Cień Ciemności" miał zioło i posadzili go na parę dni ;-).
* Patrycja wyczyściła ślady komputerowe z tego co działo się w Pełni. 
* Doprowadzono do wycofania oskarżenia przeciwko Beacie. Było zgłoszenie, nie akt oskarżenia. Konto Beaty jest czyste.
* Hektor ma "pet mimic", z czego bardzo ucieszyła się Margaret...

# Progresja

* Hektor Blakenbauer: upolował i zdobył sobie Mimika Symbiotycznego

# Streszczenie

Mimik Symbiotyczny skakał między hostami w Kopalinie. Do Hektora sprawa dotarła, gdy policjantka zgwałciła rapera i trzeba było działać. Siły Specjalne doprowadziły do zlokalizowania Mimika a Hektor go magicznie zdominował. Przy okazji, anarchiści weszli w lekki konflikt z policją o to, że policjantkę trzeba ukarać. Hektor magią mentalną wyczyścił ślady i pomógł ludziom. Aha, Artur Bryś ma... interesujące zainteresowania ;-).

# Zasługi

* mag: Hektor Blakenbauer, mag mentalny szukający usprawiedliwień dla niewinnej policjantki. Złapał sobie Mimika Symbiotycznego. Leczy ludzi uzależnionych od Mimika.
* vic: Alina Bednarz, udająca reporterkę, lokalizująca złodziejkę naszyjnika i doprowadzająca Hektora do naszyjnika. Doskonała +1.
* vic: Dionizy Kret, fałszywy reporter, przesłuchiwacz świadków i osoba bardzo dużo łażąca na potrzeby Sił Specjalnych.
* czł: Kleofas Bór, zgłosił Hektorowi problem z policjantką / raperem, potem odpowiednio rozdzielał odpowiedzialności zespołu, by się nie narobić ;-).
* czł: Patrycja Krowiowska, dziewczyna od komputera ;-); znalazła korelację między Eweliną i Beatą i tak naprawdę umożliwiła dojście do Mimika Hektorowi.
* czł: Artur Bryś, który wykazał niesamowite rozeznanie w różnych fetyszach i różowych stronach internetowych ;-). Szczególnie bawiła go reakcja Patrycji...
* czł: Stanisław Pormien, anarchista, który został "Cieniem Ciemności" - rozklejał plakaty o podłych czynach policji przeciw raperom. Skończył na tydzień w więzieniu za zioło ;-).
* czł: Ewelina Nadzieja, poprzednia nosicielka Mimika; świetna rekruterka mająca 'crush' na Wojtku Raperze. Skończyła w szpitalu, wydrenowana z energii.
* czł: Beata Solniczka, nosicielka Mimika, policjantka i córka legendarnego policjanta; pod wpływem mimika zgwałciła rapera. Mimik z niej przeskoczył dalej...
* czł: Wojciech Popolin, raper i poeta; na zapleczu klubu Pełnia zgwałcony przez policjantkę. Nie zgłosił wydarzenia, bo... no, nie protestowałby bardzo ;-).
* czł: Robert Pomocnik, lekarz w szpitalu wojskowym w Kopalinie, który opiekował się Eweliną. Chcąc jej pomóc, gawędził z Dionizym...
* czł: Katarzyna Leśniczek, nosicielka Mimika, policjantka, ostatnia nosicielka od której Mimika odebrali Alina i Hektor zanim coś złego się stało.

# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Centrum
                        1. Prokuratura
                        1. Kompleks budynków sądowo-policyjnych
                            1. Centralna komenda policji, gdzie działała Beata Solniczka i gdzie zostawiła Naszyjnik
                    1. Dzielnica Trzech Myszy
                        1. Klub Pełnia, gdzie doszło do gwałtu policjantki na raperze (???)
                        1. Duże Tesco, koło którego doszło do walki z Mimikiem Symbiotycznym i nośnikiem (Katarzyną)
                        1. Szpital Wojskowy, gdzie leżała Ewelina Nadzieja (po starciu z Mimikiem Symbiotycznym)

# Czas

* Opóźnienie: 4 dni
* Dni: 1
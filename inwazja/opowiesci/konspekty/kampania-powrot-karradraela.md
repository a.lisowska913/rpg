---
layout: default
categories: inwazja, campaign
title: "Powrót Karradraela"
---

# Kampania: {{ page.title }}

## Kontynuacja

* [Czarodziejka Luster](kampania-czarodziejka-luster.html)

## Opis

-

# Historia

1. [Wołanie o pomoc](/rpg/inwazja/opowiesci/konspekty/140503-wolanie-o-pomoc.html): 10/01/03 - 10/01/04
(140503-wolanie-o-pomoc)

Marian Agrest wysłał Andreę by pomogła lekarce Kornelii (chyba ze Szlachty) i serbskiemu terminusowi by odnaleźć czarodziejkę (Klotyldę Świątek) powiązaną z Rodziną Świecy. Agrest powiedział też, że gdzieś w tej okolicy znikali magowie powiązani z Rodziną Świecy. Przypadkowo, odkryli Rdzeń Interradiacyjny (to co z niego zostało) w który zaplątana była martwa czarodziejka - Kwiatuszek. Burze magiczne i niekontrolowane wyładowania energii powodowały powstawania efemeryd. Czterech magów zginęło a Klotylda - jakkolwiek ciężko Skażona - została uratowana. Kwiatuszek zniszczony, Rdzeń przetransportowany do laboratorium Świecy.

1. [Musiał zginąć, bo Maus](/rpg/inwazja/opowiesci/konspekty/140505-musial-zginac-bo-maus.html): 10/01/05 - 10/01/06
(140505-musial-zginac-bo-maus)

Okazało się, że serbski terminus Vuko został opłacony przez Franciszka Mausa - Mausa na KADEMie (by pomóc rodowi i obserwować co się dzieje). Okazało się też, że od bardzo dawna Mausowie nie dostawali magów z Rodziny Świecy; jakby gildia grała na wyniszczenie rodu. Okazało się też, że Kwiatuszek z krwi była Mausówną. Dodatkowo: Rdzeń Interradiacyjny chciał zabrać do badań Jan Weiner (seras), ale Agrest go zablokował. Przyciśnięty, seras Weiner powiedział o "Lordzie Jonatanie" i że Rdzeń IR służył jako uziemienie. Widać: nie do końca czynne. Dodatkowo: mnóstwo przypomnienia systemu Zaćmienia i formy przetrwalnikowe kralothów ;-).

1. ['Lord Jonatan'](/rpg/inwazja/opowiesci/konspekty/140508-lord-jonatan.html): 10/01/07 - 10/01/08
(140508-lord-jonatan)

Małe ad-hoc skrzydło terminusów dowodzone przez (otrutego) Tadeusza Barana ma oczyścić teren. Na terenie pojawił się gorathaul i ci magowie próbują się go pozbyć. Niestety, okazało się, że "Lord Jonatan" jest sprawny i próbuje przejąć kontrolę nad terenem z woli Karradraela. Przechwycił katalistę Cezarego i Skaził ciężko Milenę Diakon. Julia i Andrea wniknęły w na pół nieaktywnego aderialitha i odkryły, że Karradrael dąży do odzyskania Renaty Maus i do "planu B" (nie wie jak odzyskać Renatę). "Hackowanie" Lorda Jonatana Julia okupiła komą; gdy Karradrael skupił ogień na niej, Cierń zniszczył jego agenta a Andrea powstrzymała gorathaula. Brak Mausów zmusił aderialitha do pełni uśpienia. Do Myślina nie może nigdy zbliżyć się żaden Maus (bo jest tu na wpół przebudzony Karradrael) - Weinerowie przejęli teren w opiekę (Jan Weiner).

1. [Streamerka w Na Świeczniku](/rpg/inwazja/opowiesci/konspekty/170501-streamerka-w-na-swieczniku.html): 10/01/09 - 10/01/12
(170501-streamerka-w-na-swieczniku)

Do "Na Świeczniku" - firmy magów zajmującą się ochroną reputacji - trafiło anonimowe zgłoszenie o ludzkiej streamerce którą skrzywdził mag i magowie wysocy znaczeniem mogą mieć przez to problemy. NŚ skupiło się na tym temacie i odkryło, że zleceniodawczynią jest... Antygona Diakon, która chciała pomóc ludzkiej przyjaciółce. NŚ pomogli człowiekowi w niemałym stopniu, acz Antygona za to zapłaci przez odpracowanie. Streamerka jednak ma bana na gry komputerowe i streamowanie przez ojca.

1. [Kaczuszka w servarze](/rpg/inwazja/opowiesci/konspekty/150729-kaczuszka-w-servarze.html): 10/01/19 - 10/01/21
(150729-kaczuszka-w-servarze)

Pierwsze użycie GS Aegis 0002; zniknął Bogumił Rojowiec (mag KADEMu w cieniu ojca) który ostatnio zaczął interesować się sposobami trwałego kontrowania i zabicia technomanty. Idąc jego śladem, w Kotach, Siluria i Paweł znaleźli Karolinę Maus - czarodziejkę, która chce umrzeć ale ma blokadę i która zmusiła Rojowca zdolnościami klasy EAM by ten ją zabił. Plan Karoliny został powstrzymany przez kaczkoryzator Janka; małe kaczuszki kiepsko obsługują groźne servary. Siluria i Paweł byli świadkami, jak Oktawian Maus tuli Karolinę i próbuje ją uspokoić. Czarodziejka jest niestabilna i nie chce żyć, ale nie może się zniszczyć. Cierpi na syndrom ghoula - Aurelia/Karolina.

1. [Szept z Academii Daemonica](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html): 10/01/27 - 10/01/29
(161216-szept-z-academii-daemonica)

Na KADEMie Artur Kotała zrobił gazetkę pokazującą, że Alisa Wiraż i Warmaster mają się ku sobie. Jest to niebezpieczne, bo Pryzmat (może stać się to prawdą). Siluria zneutralizowała zagrożenie Pryzmatu i pocieszyła Alisę (odkrywając, że ta żali się EIS). Whisperwind zrobiła małą pułapkę na tą grupkę magów KADEMu. Whisper i Warmaster tęsknią za AD; Whisper dodatkowo podkrada wszystkie niebezpieczne artefakty z Instytutu Eureka i podmienia je złośliwie na niegroźne 'klony' z Fazy Daemonica.

1. [Niezbyt legalna 'Academia' Whisperwind](/rpg/inwazja/opowiesci/konspekty/161217-niezbyt-legalna-academia-whisperwind.html): 10/01/30 - 10/01/31
(161217-niezbyt-legalna-academia-whisperwind)

Karina przyszła do Inferni i Silurii z nadzieją użycia kwiatów arstraccatis by móc pokonać viciniusa na Fazie; zrobili research. Siluria przekonała ją, by pogłębili research; okazało się, że jednak muszą zacząć od nowa i tak tego nie zrobią. Siluria załatwiła ze Sławkiem, by Artur Kotała miał koszmarny sen - niech myśli że udało się połączyć Warmastera i Alisę i Warmaster odszedł z KADEMu. Whisper pokazała Silurii mikroskok w Zamku As'caen - małą 'przestrzeń'. Swoją 'Academię Daemonica'. Siluria pomoże zrobić PR, by Whisper mogła 'zalegalizować' to miejsce i połączyć je z KADEMem - pod jurysdykcją Whisper. No i Siluria dla ocieplenia wizerunku Whisper (i by ją trochę podintegrować z KADEMem) zaprosiła Norberta i Whisperwind na wspólną kolację.

1. [Zazdrość Warmastera](/rpg/inwazja/opowiesci/konspekty/161218-zazdrosc-warmastera.html): 10/02/01 - 10/02/03
(161218-zazdrosc-warmastera)

Po rozmowie z Silurią Norbert wziął Whisperwind na tańce. Warmaster wpadł w zazdrość. Wznowił prace nad specjalnym glashundem; w wyniku Karolina Kupiec została ranna. Pojawiła się szczelina między dawnym AD a KADEMem - Whisper żyje w przeszłości. Siluria użyła eks-AD do pomocy w opanowaniu sytuacji. W międzyczasie, Rojowiec poprosił Silurię o pomoc - ktoś zakosił artefakty z Kropiaktorium które KADEM im sprzedał. I podobno są ślady na KADEM. Siluria rozładowała sprawę i skierowała ją do Mordecji Diakon.

1. [Kto wpisał Błażeja do konkursu](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html): 10/02/04 - 10/02/07
(161222-kto-wpisal-blazeja-do-konkursu)

Jolanta Sowińska i Konrad Sowiński od dawna ścierają się w konkursie. Konrad zadziałał politycznie by Jola nie mogła wygrać; ta, zamiast eskalować, dołączyła biednego Błażeja Falkę z KADEMu - by on wygrał. Błażej poszedł do Silurii, która sformowała Kwartet - Konrad, Błażej, Melodia i Julian. Wspólnie zrobili opowieść o Potędze Współpracy. Jolanta chciała pokazać, że Świeca ma współpracować, ale Siluria zmieniła przekaz. Nadal - Jolanta splugawiła sukces Konrada (acz niekoniecznie Kwartetu). Jednocześnie, Siluria odkryła, że Artur Żupan zakosił artefakty do Kropiaktorium i zrzucił winę na Mariana Łajdaka. Czemu? Bo on podmienił część artefaktów pierwszy (by ratować KADEM przed problemami). Lucjan Kopidół zniszczył "niezniszczalny" Dwunastościan ku zdziwieniu Whisperwind.

1. [Przecież nie chodzi o koncert](/rpg/inwazja/opowiesci/konspekty/170221-przeciez-nie-chodzi-o-koncert.html): 10/02/05 - 10/02/07
(170221-przeciez-nie-chodzi-o-koncert)

Antygona i Judyta wybrały się do Stokrotek obejrzeć koncert "Smooth Jazz Dla Ubogich". Tam jednak natknęły się na maga chcącego zrobić reality show (Mariusza Garaża) ich kosztem. Przez Garaża Antygona i Judyta skrzyżowały szpady z Kirą i Franciszkiem; zanim jednak sprawa zrobiła się "na noże", zaklęcie Kiry (zbyt silne) zdradziło obecność Garaża. Tien Garaż wysępił odzyskanie swoich kamer i obiecał zostawić Stokrotki w spokoju. Magowie (poza Garażem) się pogodzili.

1. [Samotna w świecie magow](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html): 10/02/12 - 10/02/16
(170108-samotna-w-swiecie-magow)

Anna Kozak porywa Silurię, by dowiedzieć się, skąd KADEMowe narkotyki w Kotach. Ta daje się porwać by dowiedzieć się o co chodzi i też jest zdziwiona. "Uratowana" przez Ignata, kontynuuje śledztwo Anny i dopada ją właśnie w Kotach, studząc nastroje na KADEMie. Rozerwało się Kropiaktorium - KADEM, Ignat pobił niewinnego Oliwiera, a Siluria z Karoliną odkrywają, że faktycznie - w Kotach... są narkotyki. Świeca. Maskuje jako KADEM. Wioletta wypacza wzór Urszuli stojącej za narkotykami i Siluria łapie ją, odkrywając, że Urszula chce dołączyć do stowarzyszenia w Świecy pozwalającego magom na kontakty z ludzką rodziną. Terminus Świecy zatrzymuje przesłuchanie, Siluria żąda przeprosin od Świecy dla KADEMu.

1. [Duch Opery](/rpg/inwazja/opowiesci/konspekty/170808-duch-opery.html): 10/02/17 - 10/02/19
(170808-duch-opery)

Two models liking special substances have awakened a Gestalt of the ruined opera. Marcelin and Piotr with the help of Melodia saved all the people endangered in the result. Gestalt remains awakened and Piotr is supposed to be a curator of the opera, which is now a paintball venue.

1. [Polowanie na Mausównę](/rpg/inwazja/opowiesci/konspekty/170228-polowanie-na-mausowne.html): 10/02/08 - 10/02/10
(170228-polowanie-na-mausowne)

Ktoś poluje na Judytę Maus i nie zawaha się poświęcić ludzi, by ją dorwać. Chce wyraźnie ją zdefilerować. Franciszek ma perypetie z praktykantką swojego komendanta, Kira ma perypetie z Franciszkiem i czarodziejkami. W końcu Kira wygnała stalkera Judyty używając autorytetu Świecy a Franciszek zapewnił, by żadnemu człowiekowi nie stała się krzywda.

1. [EIS na kozetce](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html): 10/02/18 - 10/02/21
(170111-eis-na-kozetce)

Po podłym pobiciu muzykanta przez Ignata, Balrog Bankierz rzucił mu wyzwanie. Siluria, bojąc się, że sromotna porażka Ignata doprowadzi do wojny gildii - ściągnęła Netherię i Melodię i po odrobinie przekonywania i podkupowania... powinno się udać. Ignat najpewniej przegra walkę, ale nie sromotnie ;-). Ilona Amant przyznała się Pawłowi Sępiakowi, że zrobiło jej się żal EIS i podała jej sensory, by ta nie oszalała sama. I teraz trzeba ją przebadać, bo Alisa się jej kiedyś żaliła. Paweł wciągnął Silurię, wciągnęli Franciszka Mausa, po czym Paweł zrobił epickie przedstawienie i dywersje i za plecami Quasar przesłuchali EIS... która okazała się nie-wroga KADEMowi, acz pragnąca wolności.

1. [Klub Dare Shiver](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html): 10/02/22 - 10/02/25
(170115-klub-dare-shiver)

Ignat w wyjątkowo paskudnym humorze; Elizawieta go opierniczyła za to, że pobił Oliwiera. Netheria załatwiła Oliwierowi występ w Rzecznej Chacie by Ignat miał JAK go przeprosić. Udało się, acz Balrog przypisał te zasługi sobie. Artykuł w Pakcie poprawia stan KADEM/Świeca, kosztem Ignata, ale Elizawieta wybaczyła Ignatowi, więc... ok? Siluria poszła tropem Dare Shiver, klubu magów ryzykantów i wyciągnęła jego prezydenta, Adonisa, do rozmowy. Adonis wyjaśnił, że Dare Shiver jest niegroźny dla swoich członków; Siluria się martwiła, bo tien Kartel martwił się o swoją córkę (ascendowała na maga po Zaćmieniu a jej rodzice byli już magami). Po wyjaśnieniach Adonisa poczuła się lepiej. Potem Siluria pomogła Judycie Maus z jej wyzwaniem; w wyniku tego Judyta jest Siluria-struck i stała się Siluria-fangirl.

1. [Ludzka prokuratura a magowie](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html): 10/02/22 - 10/02/25
(170118-ludzka-prokuratura-a-magowie)

Bryś wykrył dziwne działania "narkotyków" Karoliny z KADEMu. Zgłosił to Hektorowi; Dionizy i Alina wsparli Brysia i Alina wpadła w środki psychoaktywne. Hektor wpierw porozmawiał z Infernią (która się spiła i wymusiła pocałunek na Hektorze w Rezydencji; to był warunek) a potem poznał opowieść od Rukoliusza dzięki Marcelinowi. Bardzo zniesmaczony, że magowie wykorzystują sobie tak ludzi...

1. [Skradziona pozytywka Mausów](/rpg/inwazja/opowiesci/konspekty/170217-skradziona-pozytywka-mausow.html): 10/02/28 - 10/03/02
(170217-skradziona-pozytywka-mausow)

Ignat się nudził i zdecydował się nadać zbirom w Świecy namiar na pozytywkę kilku nieważnych Mausów. Miał nadzieję na popcorn. Niestety, Siluria i Paweł ściągnięci przez Judytę (fangirl Silurii) zepsuli ten piękny plan i zmusili Ignata do odzyskania pozytywki. Co więcej, Franciszek oprowadził Mausów po KADEMie; KADEM dostał silny wzrost szacunku i przyjaźni a Ignat poszedł pić.

1. [Ratując syrenopnącze](/rpg/inwazja/opowiesci/konspekty/170417-ratujac-syrenopnacze.html): 10/03/05 - 10/03/12
(170417-ratujac-syrenopnacze)



1. [Naszyjnik Przenośnych Wspomnień](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html): 10/03/02 - 10/03/03
(170315-naszyjnik-wspomnien)

Mimik Symbiotyczny skakał między hostami w Kopalinie. Do Hektora sprawa dotarła, gdy policjantka zgwałciła rapera i trzeba było działać. Siły Specjalne doprowadziły do zlokalizowania Mimika a Hektor go magicznie zdominował. Przy okazji, anarchiści weszli w lekki konflikt z policją o to, że policjantkę trzeba ukarać. Hektor magią mentalną wyczyścił ślady i pomógł ludziom. Aha, Artur Bryś ma... interesujące zainteresowania ;-).

1. [Biznes pośród niesnasek](/rpg/inwazja/opowiesci/konspekty/170707-biznes-posrod-niesnasek.html): 10/03/06 - 10/03/08
(170707-biznes-posrod-niesnasek)

Na szczycie Mausów wojna o wizję rodu. Oktawian ma konserwatywne, kooperacyjne podejście a Ferdynand ma pro-drapieżne, izolacjonistyczne podejście. Zogniskowało się to wokół Judyty Maus, która AKURAT założyła biznes (Essir) z kilkoma innymi magami. Przez to, że Essir chciał wejść do Zaćmionego Serca, skonfliktowali się z Warmasterem... a Kornelia (córka Joachima) dokucza ojcu na każdym kroku. Siluria, Henryk i Marcelin musieli sobie z tym wszystkim poradzić.

1. [Camgirl na dragach](/rpg/inwazja/opowiesci/konspekty/170319-camgirl-na-dragach.html): 10/03/06 - 10/03/07
(170319-camgirl-na-dragach)

Bryś przyszedł do Aliny, bo jego druh (też wojownik) - Roman z Ognistych Niedźwiedzi - ma kłopot. Jego dziewczyna, eks-camgirl, dostała dziwne dragi i teraz Romek ma kłopot. Alina robiąc to śledztwo odkryła, że narkotyk jest najpewniej magiczny oraz nie ma dystrybutorów; po prostu czasem się pojawia i jest trochę inny za każdym razem. Po zdobyciu próbki krwi ofiary poszła z nią do Edwina; Edwin powiedział, że wygląda to na próby stworzenia czegoś do wykorzystania w walce ludzi przeciw magom. I ktoś świetnie to ukrywa - bo Edwinowi do tej pory nic nie wypłynęło...

1. [Przebudzenie viciniusa](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html): 10/03/08 - 10/03/09
(170407-przebudzenie-viciniusa)

Baletnica pod wpływem narkotyków bojowych uruchomiła uśpione geny viciniusa; stłukła Brysia i uciekła. Hektor napotkał Kornela (terminusa szukającego tych narkotyków) i weszli we współpracę. Alina i Dionizy zapewnili, że Ogniste Niedźwiedzie nadal mają tego samego bossa (kumpla od Brysia). Kornel dokonał zdalnej egzekucji aspirującego do roli bossa Parszywiaka, który podłożył narkotyk baletnicy. Baletnicę złapał Hektor z Edwinem i doprowadzili ją do w miarę ludzkiej formy...

1. [Przed teatrem absurdu](/rpg/inwazja/opowiesci/konspekty/170412-przed-teatrem-absurdu.html): 10/03/10 - 10/03/12
(170412-przed-teatrem-absurdu)

Alina zrzuciła na Brysia wyprowadzenie Pauliny Widoczek na prostą... więc ten poprosił o pomoc Kleofasa Bora. I Bór jej pomógł - mieszkanie załatwił, pracę... co doprowadziło do spięć między Brysiem i Brunowiczem. Zespół Dionizy/Alina zaczął podejrzewać wpływ magii na Bora więc zrobili pełną konspirę, tylko by odkryć, że... Bór po prostu ma dobre serce. Przy okazji udało im się NIE zauważyć, jak siły Dukata ścinające się z siłami Skubnego doprowadziły do pożaru w Teatrze Wiosennym w Czeliminie...

1. [Antygona kontra Dracena](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html): 10/03/27 - 10/03/29
(141119-antygona-kontra-dracena)

Antygona i Dracena robiły pozorowaną wojnę domową ze sobą, by wciągnąć w śmiertelną pułapkę Wiktora Sowińskiego ze Szlachty i pozbyć się jego mrocznego cienia. Były tak zdesperowane, że aż nie wahały się wciągnąć KADEMu w tą "walkę". Siluria i Paweł jednak doprowadzili do przerwania tej walki i Dracena przypadkowo wygadała się, że to pułapka. Jako, że Antygona była dookoła wrogów... konsekwencje dla młodej Diakonki mogły być fatalne. Draconis spacyfikował Dracenę, choć wezwał do akcji Izę Łaniewską by ta uratowała Antygonę. Na razie - Szlachta wygrywa.

1. [GS Aegis 0002](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html): 10/04/13 - 10/04/14
(141115-gs-aegis-0002)

Kilku magów Świecy i Millennium pojawiło się w Kotach by zrobić imprezę co skończyło się tym, że... w Kotach pojawiło się Spustoszenie. GS Aegis 0002 tam też się znajduje i jako transorganik uległ częściowemu Spustoszeniu; walczy jednak by ochronić wszystkich i odcina stopniowo z siebie Spustoszone komponenty. Niestety, stanął przed trudnym dylematem; zabić ludzi czy pozwolić magowi zginąć. Zanim 0002 odpalił rakiety, zniszczył go Paweł odpowiednim działem. Siluria wysłała sygnał alarmowy o potencjalnym Spustoszeniu zanim i oni zostali Spustoszeni. Sytuacja uratowana.

1. [Druga kradzież wyzwalacza](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html): 10/04/17 - 10/04/19
(141123-druga-kradziez-wyzwalacza)

Spustoszenie i zachowanie Aegis0002 postawiło cały program pod znakiem zapytania; zdecydowano się jednak zaryzykować i uruchomić GS Aegis 0003. 0003 jest podpięta pod Silurię i Eryka; podejrzewają, że głowica ma osobowość. Tymczasem zniknął Lucjan Kopidół wraz z artefaktem wykorzystującym Pryzmat. Pryzmat zamanifestował się w przedszkolu i w TESCO, jednak Aegis przechwyciła Kopidoła przed Działem Plazmowym i odzyskała artefakt pryzmaturgiczny. Na Lucjana Kopidoła wpływały jakieś zmiany, stoi za tym "Helena Diakon". Która nie jest prawdziwą Diakonką.

1. [Bezpieczna baza w Kotach](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html): 10/04/15 - 10/04/16
(150110-bezpieczna-baza-w-kotach)

Zlokalizowany zostaje Rdzeń Spustoszenia w Kotach (w okolicy Kopca Miślęgów); polują na niego: Julia Weiner z Powiewu (ratować Spustoszonych magów / ekspertów od Spustoszenia Mariana Welkrata i Zenona Weinera) oraz Tamara Muszkiet ze Świecy. Świeca niechętnie łączy siły, więc podzielili się na obronę (KADEM) i atak (Świeca). Okazało się, że to Spustoszenie działa inaczej; najpewniej kontroluje sporą część miasteczka ale... z ukrycia. Julia opowiedziała o Asterze i jego współpracy z kralothem. Dodatkowo okazało się, że kościół w Kotach ma własności magiczne - nie wiadomo jednak jakie.

1. [Wojna domowa Spustoszenia](/rpg/inwazja/opowiesci/konspekty/150224-wojna-domowa-spustoszenia.html): 10/04/17 - 10/04/20
(150224-wojna-domowa-spustoszenia)

Kontrolerem Spustoszenia okazało się być... pryzmatyczne echo kralotha i miślęgi. Ale wiele wskazuje na drugiego, nieznanego kontrolera Spustoszenia (który dostał pierścień Marysi i pozwolił im odejść). Ten drugi Kontroler jest taktykiem i ma jakiś dziwny cel. W kościele w Kotach dzieją się dziwne rzeczy powiązane z pamięcią ludzi, ale KADEM ukrył to przed Świecą. Za to Świeca zachowuje się, jakby coś ukrywała przed KADEMem (atak Świecy zniszczył wszystkie potencjalne ślady). Tak czy inaczej, Spustoszenie zostało odparte z Kotów i magowie odnalezieni. Min. znaleźli tam Oktawiana Mausa, którego nikt się nie spodziewał. Eksperci od Spustoszenia zostali odzyskani przez KADEM.

1. [Ile tam było szczepow Spustoszenia](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html): 10/04/22 - 10/04/24
(150313-ile-tam-bylo-szczepow-spustoszenia)

Zenon Weiner współpracował z KADEMem; powiedział o drugim kontrolerze Spustoszenia i o tym, że Marian Welkrat wykrył to pierwszy. Okazało się, że Marian miał dostęp do Spustoszenia zmienionego przez Aurelię Maus; Spustoszenie mające zniszczyć wszelkie Spustoszenie. Marian pamięta więcej niż Zenon; powiedział, że Spustoszenie przeskanowało mu pamięć. Powiedział też, że na akcji była Iza Łaniewska, terminuska ich chroniąca. A Iza współpracowała ostatnio z Tamarą Muszkiet... Okazało się też, że Iza wiedziała, że na Powiewie jest Spustoszenie i jak się tam dostać. Ale jak Spustoszenie wyciekło? Nie wiadomo.

1. [Knowania Izy](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html): 10/04/25 - 10/04/28
(150329-knowania-izy)

Iza pozycjonuje się na masterminda całego tego problemu; odciąga uwagę od Antygony i Draceny (Antygona mówi, że była kontrolerem a Dracena chyba doprowadziła wycieku Spustoszenia jakoś). Tamara - okazuje się - autoryzowała Izę do użycia Spustoszenia... odbierającego pamięć, broni Świecy. Z bełkotu Antygony dowiedzieli się też, że ona została zabawką Szlachty a Spustoszenie miało ją uratować. Ale coś poszło nie tak. Iza zatrzymała rozmowę i powiedziała, że ona utopi 3 gildie we krwi jeśli musi, ale Dracena i Antygona idą do Millennium pod opiekę Draconisa. Tak się stało.

1. [Morderstwo jak w książce](/rpg/inwazja/opowiesci/konspekty/150325-morderstwo-jak-w-ksiazce.html): 10/04/29 - 10/04/30
(150325-morderstwo-jak-w-ksiazce)

Maria Newa jest na imprezie i dochodzi do zbrodni. Pojawia się mag i wszystkich usypia. Wpierw pojawia się Paulina i dochodzi do Marii, potem Hektor z siłami specjalnymi. Zabity człowiek okazał się zostać wprowadzony jako sztuczny człowiek Diakonów by ukryć zbrodnię. Hektor aresztował wszystkie "istoty nieludzkie" na imprezie i ku zdumieniu zauważył, że mają niesamowitą mimikrę psychologiczną. Edwin zidentyfikował to jako 'aptoform'. Gdzieś w okolicy jest mag współpracujący z aptoformem lub przezeń wykorzystywany. Chwilowo końcówki są w Rezydencji. Edwin skasował pamięć Paulinie, Maria ją odtworzyła soullinkiem.

1. [Napaść na Annalizę](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html): 10/04/29 - 10/04/30
(150330-napasc-na-annalize)

Cóż, Wydział Wewnętrzny Świecy na pewno nie działa poprawnie - tak silne frakcje nie powinny istnieć. Dodatkowo, Annaliza została zaatakowana przez "Joachima Kopca" (nieznany napastnik); spłoszonego szczęśliwie przez Łajdaka fajerwerkiem. Rośnie wrogie napięcie między KADEMem a Świecą. Pojawiają się plotki wrogie dla obu gildii. Ignat wpadł w kłopoty z Marcelem ze Świecy, KADEM przetransportował Spustoszenie z Powiewu na KADEM a magowie Świecy współpracujący z KADEMem mają problemy przez Tamarę. Aha, Draconis pomógł zrekonstruować prawidłową chronologię działania Spustoszenia - jedno było przyniesione przez Antygonę, drugie przez Mariana i oba się od siebie nauczyły z niewiadomych przyczyn i powodów.

1. [Dzień z życia Klemensa](/rpg/inwazja/opowiesci/konspekty/150411-dzien-z-zycia-klemensa.html): 10/05/01 - 10/05/02
(150411-dzien-z-zycia-klemensa)

Przez zbieg okoliczności dziewczyna (ludzka) Marcelina która dostała odeń artefaktyczną maść na sen wpadła w kłopot gdy owa maść zarezonowała z innym czarem. Siły Specjalne Hektora ją uratowały przed katastrofą. A Siły Specjalne zostały uratowane przed terminuską (Judytę) przez Mojrę - nothing like red tape. Zimna wojna między KADEMem a Świecą jest w mocy.

1. [Aurelia za aptoforma](/rpg/inwazja/opowiesci/konspekty/150406-aurelia-za-aptoforma.html): 10/05/01 - 10/05/02
(150406-aurelia-za-aptoforma)

Paulina boi się Blakenbauerów. Roman (co-szef Powiewu) boi się o Powiew i chce stworzyć homunkulusa bazowanego na Aurelii Maus i na EIS. Maria zbiera informacje o aptoformie nie chcąc, by Blakenbauerowie go dorwali. Roman ma wziąć na siebie problem aptoforma a Paulina pomoże mu "odbudować" Aurelię/EIS w formie w której to nie będzie groźne tak jak kiedyś było.

1. [Rykoszet zimnej wojny](/rpg/inwazja/opowiesci/konspekty/150408-rykoszet-zimnej-wojny.html): 10/05/01 - 10/05/02
(150408-rykoszet-zimnej-wojny)

Polowanie na aptoforma, wersja: Blakenbauer. Linia anty-samobójstw dzieciom skupia się by jak najwięcej dzieci zabić. Okazuje się, że to nie działanie aptoforma; dwóch magów walczyło i rykoszet czaru zmienił psychologa dziecięcego i Wandę Ketran. Mojra próbowała pomóc Wandzie, odkryła w jej głębi Arazille i została porażona. Koma. Sygnał o eskalacji został wysłany; jeśli ludziom grozi śmierć przez magów, to na serio już jest to niebezpieczne...

1. [Szmuglowanie artefaktów](/rpg/inwazja/opowiesci/konspekty/161005-szmuglowanie-artefaktow.html): 10/05/05 - 10/05/07
(161005-szmuglowanie-artefaktow)

Marian Łajdak szmugluje artefakty na wschód, przez Jewgenija Zajcewa, by KADEM nie zbankrutował. Jednocześnie szmugluje niektóre artefakty do Świecy - przy współpracy góry Świecy i KADEMu. Jednak z uwagi na zimną wojnę operacjami zainteresowała się Tamara. Udało się wyprowadzić ją w pole - tym razem. Przekazanie Wyzwalacza Pryzmaturgicznego Świecy się nie udało - Iza Łaniewska usunęła maga Świecy który miał to przejąć jako skorumpowanego. Wyzwalacz został schowany w Szarym Pustostanie w Dzielnicy Mausów.

1. [Śladami aptoforma](/rpg/inwazja/opowiesci/konspekty/150422-sladami-aptoforma.html): 10/05/03 - 10/05/04
(150422-sladami-aptoforma)

Borys odkrył lokalizację rdzenia aptoforma i posłano na to Siły Specjalne Hektora. W nieaktywnej oczyszczalni ścieków. Siły specjalne weszły podczas, gdy dwie siły tam zaczęły walczyć - Arazille (bogini marzeń) oraz ochroniarze aptoforma. W wyniku Siły Specjalne porwały końcówkę aptoforma a Arazille inkarnowała się w ciele "wampirzycy", bardzo zmienionej Diakonki i niewolnicy Tymotheusa. Jedynym całkowicie przegranym jest Tymotheus Blakenbauer. Stracił zarówno aptoforma jak i Shard.

1. [Proces bez szans wygrania](/rpg/inwazja/opowiesci/konspekty/150604-proces-bez-szans-wygrania.html): 10/05/05 - 10/05/06
(150604-proces-bez-szans-wygrania)

Blakenbauerowie chcąc zająć się niegodnie Mojrą zaaranżowali proces przeciwko Izie, włączając w to wszystko KADEM. Proces nie miał prawa się udać - nie ma na Izę żadnych dowodów. Hektor jednak dokonał niemożliwego i Millennium przejmie nad Izą jurysdykcję, przekształcając ją i robiąc jej terapię. Mimo działań Tamary i podłożenia Hektorowi "shackowanego" demona prawniczego (niewiadomo przez kogo), Hektor wygrał - a Emilia jako adwokat Świecy dostała silny cios polityczny i Kurtyna ucierpiała boleśnie.

1. [Awokado Dla Wampira](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html): 10/05/07 - 10/05/09
(160809-awokado-dla-wampira)

Von Blutwurstowie wraz ze swoimi "ofiarami" w obszarze utraconym (Małopolska/Okólnicz) wpadli w ręce Spustoszenia dzięki intrydze The Governess. Dzięki bohaterskim działaniom zwykłych ludzi, cywile nawet nie ucierpieli specjalnie (stan ludzi się nawet poprawił).

1. [Brat przeciw bratu](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html): 10/05/07 - 10/05/08
(150607-brat-przeciw-bratu)

Eks Marcelina - Infernia Diakon - wrobiła go w zdradę i inne takie. Hektor będzie prokuratorem... dodajmy do tego, że "Pakt" uwziął się na Hektora i Marcelina. Jednocześnie, ktoś zaatakował poligon KADEMu i... ukradł żabolody. Infernia jest skłonna wycofać oskarżenie za 2 autobusy śledzi syberyjskich. Wszystko przez zakład z Ignatem. Oczywiście.

1. [Wolność pająka fazowego](/rpg/inwazja/opowiesci/konspekty/160202-wolnosc-pajaka-fazowego.html): 10/05/09 - 10/05/10
(160202-wolnosc-pajaka-fazowego)

Big one: Arazille uwolniła wszystkie istoty kontrolowane przez Tymotheusa gdy przejęła Crystal Shard. A Blakenbauerom zniknął pająk fazowy (Borys myśli że to jego wina). I ów pająk doprowadził do konfliktu Nikodem Sowiński - Laurana Bankierz. Który nie pasuje Blakenbauerom. Szczęśliwie Leonidas zlokalizował a Klara shackowała pająka fazowego; odzyskali go dla Rodu. Zabawne, że przez pająka są dwa artefakty "pomiędzy" i nikt nie wie gdzie i co robią.

1. [Poligon kluczem do Marcelina?](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html): 10/05/09 - 10/05/10
(150625-poligon-kluczem-do-marcelina)

KADEM (bardziej chciwa część KADEMu) próbuje oskarżyć Hektorem o cokolwiek Tamarę, by zwinąć jej technologie magitechowych pancerzy. Siluria nie przepada za Tamarą czy Kurtyną. Hektor został ofiarą żartu Ignata i powstał filmik "amerykański kurczak szeryf Blakenpower", który został rozpowszechniony przez "Pakt". Ukradzione żabolody były z projektów zaliczeniowych studentów. Na zaatakowanym Poligonie KADEMu było 2 magów: jeden z Kurtyny, drugi nie i oni toczyli walkę. Jeden teleportował się terminus-grade teleportation tool, drugi używając magitech armour. I wiele wskazuje za tym, że Vladlena lub Emilia stoją za atakiem na KADEM lub na Hektora.

1. [Chora terminuska i Żabolód](/rpg/inwazja/opowiesci/konspekty/150716-chora-terminuska-i-zabolod.html): 10/05/09 - 10/05/10
(150716-chora-terminuska-i-zabolod)

Paulina i Roman nadal pracują nad homunkulusem Aurelia/EIS i w procesie wpadają do supermarketu, gdzie ciężko chora na Lodowy Pocałunek Tamara wierzy, że żabolód jest dowodem na zło KADEMu i ich prace nad Spustoszeniem. W ostrej paranoi. Paulina dała radę wezwać wsparcie Świecy przez Marię i soullink. Tamara uruchomiła zakazany override swojego magitecha (niszcząc swoją karierę); dzięki działaniu Saith Flamecallera i Pauliny Tamara przeżyła, ale trafiła do szpitala w ręce Mariana Agresta. Ważny agent Kurtyny zdjęty z akcji. Aha, Paulina ukradła ukradzionego żaboloda. Plot thickens.

1. [Najskrytszy sekret Tamary](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html): 10/05/11 - 10/05/12
(150704-najskrytszy-sekret-tamary)

Quasar wprowadziła lockdown i phase-out KADEMu by naprawić systemy zabezpieczeń. Siluria została na zewnątrz; wzięła ze sobą GS Aegis 0003. Hektor konferencją prasową (dla magów) przeciągnął wielu magów na swoją stronę; najlepsze wystąpienie w życiu. Iza Łaniewska zostanie przekształcona w Diakonkę. Siluria była świadkiem jak Saith Flamecaller zaciera ślady w supermarkecie (chroniąc Tamarę) przez GS Aegis. Hektor i Siluria z Ozydiuszem Bankierzem weszli do domu Tamary i odkryli, że Tamara chowała tam swoją "ludzką" rodzinę. Pojawiły się dowody, że magiem Kurtyny na Poligonie KADEMu była właśnie Tamara - ale kim był ten drugi? Tamara była już chora na Lodowy Pocałunek. Ozydiusz wyrzucił Tamarę ze służby (zaocznie). Nie jest już terminusem.

1. [Splątane tropy: Spustoszenie?](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html): 10/05/13 - 10/05/14
(150718-splatane-tropy-spustoszenie)

Agrest nie ma jurysdykcji w Kopalinie, ale poprosił Andreę, by sprawdziła co tu się dzieje - potencjalnie szef Wydziału w Kopalinie jest niekompetentny lub zdrajcą (Aleksander Sowiński). By zneutralizować Hektora (chronić Tamarę przed losem Izy) Andrea współpracowała z seirasem Mausem; stworzyli fałszywą acz wiarygodną plotkę by Hektor wpadł w pułapkę i został skompromitowany (Tymek & Gala). Marcel Bankierz przyznał się, że przed atakiem z Aurelem Czarko (jego pomysł) znalazł lukę zabezpieczeń na KADEMie (Poligonie) i podał ją Salazarowi a potem Tamarze. Potem Zenon Weiner powiedział Andrei, że były dwa kontrolery Spustoszenia i że Iza wiedziała od początku o Spustoszeniu. Tym drugim. Też Zenon ostrzegł, że mogą mieć do czynienia z Overmindem Spustoszenia - a aktualny blokujący Overmind (Aurelii) jest w lockdownie na KADEMie i nie blokuje. Zdaniem Zenona, aktualnym kontrolerem (Overmindem?) Spustoszenia jest vicinius o bardzo silnej woli i "one purpose" - inaczej nie byłby w stanie kontrolować a byłby podległy. Dzięki bardzo brutalnemu przez przeciążenie energii (za zgodą) przesłuchaniu Estrelli, Andrea dowiedziała się też, że Antygona, Dracena, Oktawian, Iza i Wiktor byli w jednym miejscu gdy doszło do Spustoszenia; wtedy też zarezonowało to w kopcu i Spustoszyło Estrellę, Netherię i Olafa. Ale był sygnał kontrolujący Antygonę. Coś "z zewnątrz". Z tego wynika, że Marian i Zenon oberwali później od Spustoszenia, ale oni i Iza myśleli że poszli razem - tajemniczy "trzeci" Overmind (potencjalny vicinius) wszystko genialnie zsynchronizował.

1. [Reverse kidnapping z Krupnioka](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html): 10/05/15 - 10/05/16
(150722-reverse-kidnapping-krupniok)

Siły Specjalne wysłane by uratować niewinne dzieci przed Tymkiem i Galą (plan Andrei i seirasa Mausa). Adela Maus (inkwizytor) obserwuje z ukrycia. Klemens, Alina i Dionizy mają podmienić armię dzieci tak, by nikt nie wiedział na "płaszczki" z Blakenlab. Udało się. Adela Maus przeskanowała Patrycję Krowiowską, ale poza tym nic nie mają. Nawet naruszenia Maskarady. Dzieci uratowane, plan seirasa zawiódł.

1. [Sojusz przeciwko Szlachcie](/rpg/inwazja/opowiesci/konspekty/150728-sojusz-przeciwko-szlachcie.html): 10/05/15 - 10/05/16
(150728-sojusz-przeciwko-szlachcie)

Leokadia Myszeczka, "ostrze Tamary", wrobiła Marcelina w dostarczenie jej merkuriasza (lustrzaka) i dała się napaść i poranić. Dzięki temu, że napastnicy zabrali jej magiczne przedmioty, merkuriasz zaatakował Dianę Weiner i ją ciężko poranił. Teraz Szlachta BĘDZIE winić Blakenbauerów i Blakenbauerowie WEJDĄ w sojusz z Kurtyną przeciw Szlachcie. Cholerna fanatyczka. Aha, plan pochodzi od Saith Kameleon, czyli od Agresta. Oczywiście.

1. [Krótki antyporadnik o sojuszach](/rpg/inwazja/opowiesci/konspekty/150819-krotki-antyporadnik-o-sojuszach.html): 10/05/17 - 10/05/18
(150819-krotki-antyporadnik-o-sojuszach)

Szeroko rozpowszechniane są dopalacze. By chronić politycznie Marcelina i Blakenbauerów przed Szlachtą, Edwin otruł Leokadię (najkrótszy sojusz świata). Vladlena opowiedziała, że Kurtyna pod naciskiem Szlachty się powoli rozpada a Szlachta rośnie w siłę. Podobno aptoform uciekł (nieprawda). Spotkanie w restauracji Hektora#Silurii i Skubnego, on nie ma nic wspólnego z dopalaczami. To jakaś magiczna sprawa. Skubny wie o magii. Za to POTEM wyszło, że Vladlena dopalaczami szuka aptoforma / innych istot i wydobyła od Hektora ochronę dla magów Kurtyny. Mimo krzywdy Leokadii, Sojusz z Kurtyną jest w mocy.

1. [Andrea węszy koło Szlachty](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html): 10/05/19 - 10/05/20
(150913-andrea-weszy-kolo-szlachty)

Andrea nadal zbiera dane o Szlachcie. Poznajemy historię Judyty Karnisz, dodatkowo Judyta naświetla to, że ona, Iza i Tamara chciały uniemożliwić zabijanie ludzi w Kotach (zgodnie z procedurami już można przy tym progu Spustoszenia). Zdaniem Judyty, Spustoszenie próbowało "przypadkiem" zabić Zenona Weinera; anomalią był też Oktawian Maus, który "zniknął" a jednak się znalazł w kopcu miślęgów. 
Magitech Tamary wykrył dodatkowe sygnały wyglądające jak zakłócenia (jakąś regionalną wersję kołysanki zgodnie z Agrestem) z bunkra w Kotach. I odkryła drugą osobę - Anna Kozak była oddana przez Tamarę do Rodziny Świecy dawno temu. I Szlachta wysłała Annę na poligon KADEMu i przez Salazara ostrzegła Tamarę (z Lodowym Pocałunkiem). Potem Andrea porozmawiała z Oktawianem, dowiedziała się, że Milena jest w fatalnej formie i że Oktawian jest fanatykiem rodu Maus. Oktawian powiedział, że jest ze Szlachtą by ratować Świecę i Mausów. Kurtyna, jakkolwiek bliska Oktawianowi, chce działać zgodnie z zasadami systemu - a system musi zostać zniszczony. Potem Salazar przyznał się Andrei, że wpadł z Marcelem w kłopoty z ludźmi i jakiś mag im pomógł i doprowadził do "tego wszystkiego". Dzięki Annie Kozak i jej machinacjom z ludźmi i artefaktami Andrea poznała tożsamość tamtego maga - to Dominik Bankierz. A jego działania śledzi inkwizytor Adela Maus. A Anna naraziła Maskaradę i Świecę.

1. [Klemens w roli swatki](/rpg/inwazja/opowiesci/konspekty/150820-klemens-w-roli-swatki.html): 10/05/19 - 10/05/20
(150820-klemens-w-roli-swatki)

Edwin chce Wandę Ketran jako dziewczynę dla Marcelina. Bo ma jej dokładny wzór. Klemens i Alina wysłani by ją porwać niezauważenie. Pomogli wrobić biznesmena Ormię w jej zniknięcie. Aha, Wanda ma eksperymentalną dronę "Czarny Błysk" z firmy Skrzydłoróg.

1. [Plan ujawnienia z Hipernetu](/rpg/inwazja/opowiesci/konspekty/151001-plan-ujawnienia-z-hipernetu.html): 10/05/21 - 10/05/22
(151001-plan-ujawnienia-z-hipernetu)

Vladlena buduje broń opartą na Spustoszeniu mającą ujawnić Szlachtę przez hipernet i zmusić Wydział Wewnętrzny do działania - ale nie Śląski (bo nie ufa). Koszt jednak taki, że Spustoszenie poznałoby tożsamość WSZYSTKICH magów Świecy powiązanych hipernetem. I wykorzystuje płaszczki Blakenbauerów jako obronę tego, zmuszając Blakenbauerów do wyboru "Szlachta lub Kurtyna". Ujawnione są frakcje - czym jest Szlachta, czym Kurtyna. Podczas wielkiej bitwy o hipermarket Vladlena zagroziła Wiktorowi Spustoszeniem; on się wycofał. Dionizy zwinął Vladlenie Spustoszenie i uratował kogo się da z Kurtyny (łącznie z 'leną). W walce teren został Skażony.

1. [Atak na sanktuarium Estrelli](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html): 10/05/23 - 10/05/24
(150823-atak-na-sanktuarium-estrelli)

Katarzyna Kotek została stworzona jako homunkulus Aurelii/EIS przez Paulinę i Romana. Spięcie między Pauliną i Marią o Katarzynę. W supermarkecie Maria wykryła Olgę Miodownik - agentkę Iliusitiusa; Paulina zaryzykowała i przyznała Oldze, że pamięta Iliusitiusa. Połączyły siły - Olga śledzi działania Estrelli na cmentarzu, która "może krzywdzić ludzi". Pojechały obie - tam Estrellę (wciąż fatalny stan po przesłuchaniu Andrei) napadło dwóch osiłków i Paulina ją uratowała. Osiłki były zaczarowane przez tanie zabawne artefakty z Kropiaktorium. Paulina pomogła terminusce; cmentarz to jej "sanktuarium" jakie ona kocha. W nocy - cmentarz spłonął; Paulina z trudem z Netherią dały radę to powstrzymać. Niestety, większość "dobrej magii" na cmentarzu została zniszczona, działania pokoleń. Coś jest pod spodem i to się obudziło.

1. [Pętla dookoła Pauliny](/rpg/inwazja/opowiesci/konspekty/150826-petla-dookola-pauliny.html): 10/05/25 - 10/05/26
(150826-petla-dookola-pauliny)

Pakt obśmiał Estrellę i skompromitował Ozydiusza. Lord terminus zrobił Estrelli areszt domowy. Pakt też wskazuje na Baltazara Mausa jako winnego ataku na Estrellę. Zdaniem Netherii, wszyscy przegrywają: Świeca, Ozydiusz, młody Maus. Ktoś to rozegrał. A pod cmentarzem coś jest. Słabe i stare. Nie wiadomo co to; nikt o tym jeszcze nie wie. Paulina młodego Mausa uratowała przed Olgą (ta by zabiła). Jolanta Sowińska z Paktu zrobiła wywiad z Pauliną; chce obniżyć napięcia Świeca-inne gildie. Paulina jako pozytywny symbol. Ta się przeraziła i ewakuowała; niekoniecznie chce być na świeczniku. Ale będzie. Bo Pakt.

1. [Sprawa Baltazara Mausa](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html): 10/05/27 - 10/05/28
(150920-sprawa-baltazara-mausa)

Aleksander Sowiński (szef Wydziału Wewnętrznego na Śląsku) zażądał od Andrei odkrycia problemu Baltazara Mausa. Pierwszego maga Mausów z Rodziny Świecy od dawna (ta sprawa przywróciła ostrą dyskusję w Świecy nt Mausów/Rodziny). "Matką" Baltazara jest Elea Maus - lekarka. Wskazała Malię Bankierz (dziewczynę Baltazara) i jej wujka Ozydiusza. Żupan dostarczył jej kryształy z KADEMu które pomogły stworzyć Baltazarowi plan. I wszystko wskazuje na Ignata Zajcewa z KADEMu. Ozydiusz próbuje chronić Malię i Baltazara, Estrella chce skrzywdzić Baltazara. Czyli najpewniej Ignat... ale nikt nie rozumie po co.

1. [Nigdy dość przyjaciół: Szlachta i Kurtyna](/rpg/inwazja/opowiesci/konspekty/151007-nigdy-dosc-przyjaciol-szlachta-kurtyna.html): 10/05/29 - 10/05/30
(151007-nigdy-dosc-przyjaciol-szlachta-kurtyna)

Poszerzenie: Szlachta jest zła i się bawi magami, Wiktor lubi czarodziejki jako "zwierzątka", Adrian przyłączył się do Szlachty z własnej woli. Zdaniem Edwina, Blakenbauerowie powinni wesprzeć Szlachtę, zdaniem Hektora - Kurtynę. Oktawian Maus przyszedł do Rezydencji prosić o sprawiedliwość dla Mileny, niech Świeca ją wyda. On jej pomoże. Oktawian podejrzewa kontakt z Karradraelem i chce go odnaleźć. Spotkanie w chatce w lesie; Szlachta załatwiła "możemy wykorzystywać wasze lab". Nagle - atak. Edward Sasanka, napuszczony przez Aurela Czarko (podobno). Szczęśliwie, GS Aegis go unieszkodliwiła. Diana przekazała Hektorowi materiały o zbrodniach Vladleny i poprosiła o start śledztwa.

1. [Hybryda w bazie Skorpiona](/rpg/inwazja/opowiesci/konspekty/150829-hybryda-w-bazie-skorpiona.html): 10/05/27 - 10/05/28
(150829-hybryda-w-bazie-skorpiona)

Olga wciągnęła Paulinę w ratowanie agentów Skorpiona Skażonych przez magię (zmienionych w viciniusy). Okrucieństwem Olgi i magią Pauliny uratowali bazę; Kasia Kotek dostarczyła przez Marię kluczowych informacji Paulinie i viimanos został zniszczony. Olga wkręciła się w Skorpiona i zwinęła artefakty. Na potęgę Iliusitiusa.

1. [O Wandzie co Zajcewa nie chciała](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html): 10/05/29 - 10/05/30
(150930-o-wandzie-co-zajcewa-nie-chciala)

Wandy Ketran szuka mag rodu Zajcew, Joachim. Alina, Olga, Anna i Klemens uniewinniają strażnika cmentarza z "to przez niego spłonął" i wrobili w to Joachima Zajcewa; przekierowali zainteresowanie Joachima na Tymka Mausa i nasłali nań policję. Nikt nie łączy Blakenbauerów z Wandą.

1. [Kasia, nie EIS w Powiewie](/rpg/inwazja/opowiesci/konspekty/150830-kasia-nie-eis-w-powiewie.html): 10/05/29 - 10/05/30
(150830-kasia-nie-eis-w-powiewie)

Paulina wraca do Kopalina odzyskać Marię i uciec. Nie wyszło. Kasia Kotek ma przebłyski EIS i Aurelii; nie wie czym jest. Wykryła ją Julia i ścięła się z Romanem o to czym jest Powiew Świeżości. Gdy Julia i Kasia zaczęły walkę, obecność Kasi została broadcastowana Powiewowi. Roman kazał Paulinie z Marią się ulotnić; on to załatwi i zintegruje Kasię Kotek z Powiewem. Tymczasem na Fazie Naturalnej mayday Netherii na Cmentarzu Wiązowym. Ozydiusz wydobył spod ziemi "Skażonego maga pełnego nienawiści" a Mikado z Millennium walczy z Judytą. Ozydiusz kazał wybatożyć Paulinę za impertymencję, ciężko zranił Mikada po czym Skaził obszar. Służby puryfikacyjne i medyczne pomogły obecnym, ale dla Pauliny to sygnał. Znika stąd.

1. [Jedno słowo prawdy za dużo](/rpg/inwazja/opowiesci/konspekty/151014-jedno-slowo-prawdy-za-duzo.html): 10/05/31 - 10/06/01
(151014-jedno-slowo-prawdy-za-duzo)

Tatiana zażądała wydania Vladleny. Hektor odmówił i przyjął odpowiedzialność za wszystko co się z Vladleną stało od momentu aresztowania. Edwin skasował Vladlenie pamięć o aptoformie i innych eksperymentach Tymotheusa. Plus, dowiedział się o Węźle Vladleny. Hektor rozpoczął proces Vladleny. Blakenbauerowie zgłosili obecność Arazille i Tymotheusa teraz i biorą Tymotheusa na siebie (Arazille na Świecę). A Siluria dowiedziała się od Amandy, że Milena Diakon ma wstrzykniętą nie matrycę Mausów a Karradraela; da się ją uratować przez sprzężenie z kralothem. Na spotkaniu z Emilią, czarodziejka rzuciła bombę - magitech Wiktora ma informacje z czasów jak Wiktor był Spustoszony i wszystko co kiedykolwiek zrobił. Czyli to może zniszczyć Szlachtę.

1. [Przebudzenie Mojry](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html): 10/06/02 - 10/06/03
(151021-przebudzenie-mojry)

Emilia odzyskała magów Kurtyny z Rezydencji Blakenbauerów. Diana Weiner oddała Hektorowi grupę praktykantów Szlachty do rozwiązywania problemów ze światem ludzi. W zamian za to - Hektor przechwyci Węzeł Vladleny. Wiktor zaleca się do Silurii ze wzajemnością. Wiktor na arenie (macho man) pokonuje terminusów i Tatianę. Mojra się obudziła i powiedziała Hektorowi, że jest wykorzystywany do niszczenia przeciwników politycznych Szlachty. Świeca wie o Arazille i jej szuka. Siluria dowiedziała się, że atak na poligon KADEMu to sprawa polityczna Świecy; jak Hektor prokurator. Siluria skutecznie zamaskowała Sasankę przed Emilią. Hektor odmówił Tatianie czegoś z Rezydencji a jej kolega - Zajcew - przekupił Borysa i kupił stary kapeć. Tania przegrała zakład. Adrian Murarz został sprzężony symbiontem (autoryzacja Mojry); symbiont Szlachty robi więcej niż mówił Wiktor.

1. [Och, nie! Porwali Ignata!](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html): 10/05/29 - 10/05/30
(150922-och-nie-porwali-ignata)

Świeca interesuje się Ignatem (który faktycznie stoi za problemem Baltazara Mausa). Ignat chciał chronić Elizawietę i skrzywdzić Świecę. Siluria skasowała sobie pamięć o GS Aegis 0003, kazała jej porwać Ignata i skrzywdzić siebie. Siluria udowadnia, że Aegis jest czymś więcej niż tylko głowicą, że ma intelekt EIS. Kermit Diakon przygarnął Silurię do siebie; zlokalizował Głowicę która pokonała jego skrzydło terminusów. Anna Kozak przyznała się, że ona dostała artefakt od Karola Poczciwca do własnej obrony i udostępniła go ludziom. Andrea zauważyła, że Milena Diakon ma "poczucie Spustoszenia". Zgodnie z rewelacjami Elizawiety, Milena ją SPECJALNIE skaziła, by odepchnąć od siebie. Bo Milena ma przymusy i szepty, straszne szepty a Elizawieta chciała jej pomóc i się z nią zaprzyjaźnić.
Po sprawie na Cmentarzu Wiązowym Ozydiusz wyjaśnił Andrei, że zabezpieczył eksperyment Millennium - cruentus reverto, "niewłaściwie zabitego maga" stworzonego z Diakonów.

1. [Romeo i... Hektor](/rpg/inwazja/opowiesci/konspekty/151110-romeo-i-hektor.html): 10/05/31 - 10/06/01
(151110-romeo-i-hektor)

Ktoś napuścił Romeo Diakona na Hektora Blakenbauera; chciał go uwieść jako 'dominator'. Power play. Romeo "wisi" coś Zajcewce. Romeo obraził Hektora, ten śmiertelnie obraził Romeo, Mojra skontaktowała się z Triumwiratem Diakonów i jakkolwiek zabezpieczyła Blakenbauerów przed problemami ze strony Diakonów, to jednak stosunki Diakoni - Blakenbauerowie są teraz zimno wrogie (plus: Infernia musi wycofać zarzuty wobec Marcelina, więc ten jest bezpieczny). Hektor zgodził się, by Skubny dostał licencję na SkubnyTV.

1. [Byli sobie przestępcy](/rpg/inwazja/opowiesci/konspekty/160128-byli-sobie-przestepcy.html): 10/06/02 - 10/06/03
(160128-byli-sobie-przestepcy)

Znajomy komendant straży pożarnej został oskarżony o konstrukcję maszyny piekielnej; Hubert Rębski chce aresztować Hektora. Diana Weiner uratowała sprawę za sporą polityczno-prawną przysługę. Ktoś podrobił dokumenty (wulgarnie trywialnie) oskarżające Hektora o korupcję. Część dokumentów jednak jest prawdziwa - powiązanych z Kimarojem. Bardzo dawno temu ów komendant i Kimaroj przemycali ludzi z Ukrainy; potem przestali. Hektor skupił uwagę na tej sprawie.

1. [Batmag Uderza!](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html): 10/06/04 - 10/06/05
(160210-batmag-uderza)

Kimaroj prowadzi "bezpieczne miejsce" dla przestępców w restauracji Wyżerka. Skubny poprosił Hektora o pomoc w rozwiązaniu konfliktu między nim a Daczynem; nie chce wojny grup przestępczych. Hektor wsadził Skubnego i Daczyna do aresztu. Przyciśnięty Kimaroj się złamał i dał Hektorowi dowody na Daczyna i inne siły przestępcze w okolicy. Trafił do programu ochrony świadków. Za wszystkim stoi mag - Anna Kozak (o czym nikt nie wie) - która chce być Batmagiem i wrobiła Hektora w działanie.

1. [Sojusz według Leonidasa](/rpg/inwazja/opowiesci/konspekty/160217-sojusz-wedlug-leonidasa.html): 10/06/06 - 10/06/07
(160217-sojusz-wedlug-leonidasa)

Oktawian Maus odwiedził Rezydencję Blakenbauerów, ale tym razem miast żądać sprawiedliwości dla Mileny poprosił Edwina o porwanie Zenona Weinera, by udowodnić, że Ozydiusz stoi za Spustoszeniem. Zostawił jako zakładniczkę kuzynkę, Judytę Maus. Alina jako Ludwik Weiner kupuje nieaktywne Spustoszenie, porywają Zenona pokonując Salazara Bankierza i wrabiając w to "Spustoszonego agenta". Leonidas zdradził Oktawiana; wrobił go tak, by zyskać ogromny dług u Diany Weiner, zachować zakładniczkę i zdobyć przewagę nad Oktawianem. Ale Oktawian dostał swojego Zenona.

1. [Siluria na salonach Szlachty](/rpg/inwazja/opowiesci/konspekty/160326-siluria-na-salonach-szlachty.html): 10/05/31 - 10/06/01
(160326-siluria-na-salonach-szlachty)

Wiktor Sowiński "zaprosił" Silurię do Kompleksu Centralnego. Gerwazy i Wioletta chronią jej, ale dają jej wolność. Siluria uczestniczy w rytuale zmieniającym tien Antoniego Kurzamyśla w sleeper agenta (w posiadłości Emilii); przekonała Wiktora, że jest mu lojalna. Przekazała Wiktorowi kontakty biznesowe Millennium, by ten mógł wynająć agentów. Potem - sparing z Wiolettą, w wyniku którego ta jest zafascynowana Silurią i używaniem erotyzmu w walce / sparingach. Wioletta nauczy Silurię walczyć a Siluria nauczy Wiolettę walczyć... inaczej.

1. [Patrycja węszy szpiega](/rpg/inwazja/opowiesci/konspekty/151119-patrycja-weszy-szpiega.html): 10/06/08 - 10/06/12
(151119-patrycja-weszy-szpiega)

Patrycja wpadła w tryb paranoiczny i zaczęła badać... Blakenbauerów. Bo Blakenbauerowie ukrywają dane przed Hektorem. Dużo takich informacji. Alina spowolniła Patrycję; znaleźli w tym też powiązania między Skubnym a firmą Chegolent. Dionizy wykrył, że mają kreta w Rezydencji - Wandę Ketran. Ona to robi. O dziwo, mimo, że zdaniem Edwina nie jest to możliwe - ma elementy wolnego agenta. Ponadto usuwa dane o Skubnym, Chegolent, o części magii, o Blakenbauerach (wszystko). Edwin skorygował Patrycję (by ta nic nie pamiętała) i zbagatelizował Wandę (on jej kazał usuwać te dane).

1. [Odbudowa relacji konfliktem](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html): 10/06/13 - 10/06/14
(151124-odbudowa-relacji-konfliktem)

Blakenbauerowie odkryli Kacpra Weinera jako potencjalnego sojusznika i Ozydiusza Bankierza jako potencjalny cel do ataku by zdobyć szacunek i wsparcie. Siluria wyjaśniła Hektorowi, że skrewił z Diakonami i że Czirna Zajcew - przyjaciółka Romeo - źle mu doradziła. Dodatkowo Tatiana włamała się do leśnej chatki Blakenbauerów i wyzwała Hektora na pojedynek przez Fire Suit Zajcewów. Przy okazji Borys przyznał się, że Edwin kazał mu przechwytywać wszelkie informacje o Tymotheusie. Czemu? By Hektor czegoś głupiego nie chlapnął.

1. [Zdobycie węzła Vladleny](/rpg/inwazja/opowiesci/konspekty/151202-zdobycie-wezla-vladleny.html): 10/06/15 - 10/06/18
(151202-zdobycie-wezla-vladleny)

Margaret pracuje nad "galaretką" dostarczoną przez Szlachtę; coś mającego umożliwić magom zobaczyć "wolność" (wymaga hipernetu do działania). Z aprobatą Emilii, Margaret ukradła klucz do portalu Vladleny a Hektor go zajumał. Tatiana przerwała im w Fire Suicie; Alina jednak ją oszukała i nie doszło do walki. Tatiana przegrała Fire Suit Duel. Powstała o niej niepochlebna ballada.

1. [Siedmiu magów - nie Mausów](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html): 10/06/08 - 10/06/13
(160229-siedmiu-magow-nie-mausow)

Agrest przebił się przez dane Oktawiana Mausa; ów zwrócił się ku Szlachcie by pomóc Mausom. A Aleksander Sowiński (przywódca Szlachty) doprowadził do takiego stanu Mileny Diakon by kontrolować Oktawiana. Andrea uratowała siedmiu porwanych przez Oktawiana magów by nie stali się Mausami, zdobywając z siłami Agresta (min. Tamarą) ukrytą bazę Oktawiana. Tadeusz Baran ogłosił Szlachcie i (przypadkowo Kurtynie) info o starym TechBunkrze gdzie jest coś co może pomóc w złamaniu Labiryntu Zielonej Kariatydy Ozydiusza. Anna Kozak oddała wszystkie niewłaściwe artefakty Andrei. Agrest przekazał Andrei, że na wykopaliskach była katastrofa - zniknęli lub zginęli Dominik Bankierz, Oktawian Maus i... Adela Maus.

1. [Piećdziesiąt oblicz Szlachty](/rpg/inwazja/opowiesci/konspekty/160327-piecdziesiat-oblicz-szlachty.html): 10/06/14 - 10/06/23
(160327-piecdziesiat-oblicz-szlachty)



1. [Antyporadnik wędkarza Arazille](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html): 10/06/19 - 10/06/20
(151212-antyporadnik-wedkarza-arazille)



1. [Między prawdą i fikcją Arazille](/rpg/inwazja/opowiesci/konspekty/151216-miedzy-prawda-i-fikcja-arazille.html): 10/06/21 - 10/06/22
(151216-miedzy-prawda-i-fikcja-arazille)

Oddział Zeta jest porwana przez Arazille, która wykorzystuje ich do walki przeciw bardzo silnym defilerom w Toczmiejowie. Członkom Zety wróciła tożsamość sprzed transformacji w Zetę; pojawiają się lekkie przebicia, że nie są ludźmi, ale Alina i Dionizy kontrolują sprawę. Arazille obiecała Zecie wolność za zniszczenie defilerów. Jeden z defilerów zaatakował i zdominował Zetę, ale Marta Newa dała radę go rozwalić. Zeta przypomniała sobie swoją przeszłość...

1. [Wiktor kontra Kadem i Świeca](/rpg/inwazja/opowiesci/konspekty/160403-wiktor-kontra-kadem-i-swieca.html): 10/06/23 - 10/06/24
(160403-wiktor-kontra-kadem-i-swieca)



1. [Aniołki Marcelina](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html): 10/06/23 - 10/06/24
(160224-aniolki-marcelina)



1. [Otton zabija Zetę](/rpg/inwazja/opowiesci/konspekty/160303-otton-zabija-zete.html): 10/06/23 - 10/06/24
(160303-otton-zabija-zete)

Zeta kłóci się wewnętrznie, oddała dowodzenie Dionizemu (Alina wrobiła). Operacja - usunięcie dwóch defilerów; stworzyli dowody że ten trzeci pracuje z innym defilerem po czym Zeta zaatakowała z zaskoczenia. Mimo przewag, magowie (defilerzy) są zwyczajnie ZA SILNI, ale się Zecie udało wygrać i oddać defilerów Arazille. Otton włączył selfdestruct Zecie; tylko Marta przeżyła a Dionizy i Alina wyszli z iluzji Arazille. Dionizy MÓGŁ zabić Martę, ale tego nie zrobił; porwał defilerów i uciekł z Aliną i Kleofasem & Margaret & Olgą do Rezydencji.

1. [Sekret śmierci na wykopaliskach](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html): 10/06/14 - 10/06/15
(160411-sekret-smierci-na-wykopaliskach)



1. [Desperacka bateria dla Aegis](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html): 10/06/16 - 10/06/21
(160927-desperacka-bateria-dla-aegis)

Ignat Zajcew jest przerażony krytycznym stanem GS Aegis. Poprosił o pomoc "tienowatą" Kirę, kuzynkę, 'nie defilerkę' i jej męża, sołtysa wsi Stokrotki. Podczas prób pomocy, Franciszek i Kira zmierzyli się z Aurelem Czarko (który wyraźnie dobrze się bawi tępiąc młode terminuski i magów Świecy). Część odpowiedzi znaleźli u Kasi Kotek w Powiewie Świeżości, w książkach - Kira sprowadziła Syberię, Franciszek zrobił imprezę i udało im się naładować reaktor Aegis. W podzięce Ignat pobił (dyskretnie) Aurela Czarko i wyszło, że to robota żuli ;-). Czarko został też politycznie poskromiony przez Zenobię Bankierz z Trocińskiego oddziału Świecy.

1. [The power of cute pet](/rpg/inwazja/opowiesci/konspekty/160404-the-power-of-cute-pet.html): 10/06/25 - 10/06/26
(160404-the-power-of-cute-pet)



1. [Tajemnica podłożonej świni](/rpg/inwazja/opowiesci/konspekty/170321-tajemnica-podlozonej-swini.html): 10/06/25 - 10/06/26
(170321-tajemnica-podlozonej-swini)

W Stokrotkach pojawił się glukszwajn, ku wielkiemu utrapieniu Kiry. I audytorka Wieprzpolu, ku wielkiemu utrapieniu Franciszka. Próba ukrycia teleportującej się świni przed audytorką, nie wysadzenia saperów w lesie i pogodzenia sprzecznych interesów rolników doprowadziła do tego, że JAKOŚ wszystko się udało. Wieprzpol wchodzi do Stokrotek a złapany glukszwajn został sprzedany magowi Świecy.

1. [Irytka Sprzężona](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html): 10/06/25 - 10/06/26
(160309-irytka-sprzezona)



1. [Frontalne wejście Millennium](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html): 10/06/27 - 10/06/28
(160316-frontalne-wejscie-millennium)



1. [Najprawdziwszy sojusz Blakenbauerów](/rpg/inwazja/opowiesci/konspekty/160406-najprawdziwszy-sojusz-blakenbauerow.html): 10/06/29 - 10/06/30
(160406-najprawdziwszy-sojusz-blakenbauerow)



1. [Symptomy kryzysu Świecy](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html): 10/06/16 - 10/06/17
(160417-symptomy-kryzysu-swiecy)



1. [Spleśniała dusza terminuski](/rpg/inwazja/opowiesci/konspekty/160412-splesniala-dusza-terminuski.html): 10/07/01 - 10/07/02
(160412-splesniala-dusza-terminuski)



1. [Kolizja dwóch sojuszy](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html): 10/07/01 - 10/07/02
(160420-kolizja-dwoch-sojuszy)



1. [Uważaj, o czym marzysz](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html): 10/06/18 - 10/06/21
(160424-uwazaj-o-czym-marzysz)



1. [Wyścig pająka z terminuską](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html): 10/06/22 - 10/06/24
(160506-wyscig-pajaka-z-terminuska)

Zapanował chaos. Otton chce, by Blakenbauer byli "tymi dobrymi". Klara i Hektor współpracują z siłami Świecy pod kontrolą Elizawiety Zajcew i Anety Rainer opanowując chorych na Irytkę i ograniczając straty jednostek Świecy. Amanda Diakon zażądała współpracy z Emilią i tego, by Hektor odzyskał cruentus reverto. Klara i Hektor użyli pająka fazowego używając kodów Agresta i zwinęli cruentus reverto prosto spod szponów Dagmary Wyjątek. Ślady pająka wskazują na Emilię. Powoli sytuacja się ustawia politycznie jako Millennium # lojaliści Świecy # wszyscy vs Szlachta / Regentka Spustoszenia. Andrea dowiedziała się, że najpewniej mechaniczni terminusi są Spustoszeni. Więc Kopalin jest stracony.

1. [Czyj Jest Kompleks Centralny](/rpg/inwazja/opowiesci/konspekty/160723-czyj-jest-kompleks-centralny.html): 10/06/22 - 10/06/23
(160723-czyj-jest-kompleks-centralny)

Siluria wytrąciła Wiktora ze stuporu, by ten nie pozwolił Dagmarze na swobodne kontrolowanie sytuacji. W Kompleksie Centralnym sformowały się dwa ośrodki dowódcze - Wiktora (Dagmary) i Bogdana Bankierza. Siluria skłoniła Wiktora, by ten zmusił Dagmarę do pokojowego rozwiązania tej sprawy; potem Siluria odblokowała komunikację między Kompleksem a resztą Świecy (ktoś umieścił zagłuszacz KADEMu, zniszczony przez Silurię). Pojawia się silniejsza atomizacja i niechęć KADEM - Świeca (Szlachta) - Świeca (Bankierzy) - Świeca (inni).

1. [Portal do EAM](/rpg/inwazja/opowiesci/konspekty/160724-portal-do-eam.html): 10/06/24 - 10/06/25
(160724-portal-do-eam)

Kompleks Centralny powoli przestaje działać przez Irytkę i ogólną panikę. Pierwsze padnie jedzenie dla viciniusów, często kluczowych dla funkcjonowania Świecy (Wiktor i Siluria się tym zajęli). Dagmara chciała wejść na EAM dzięki Kluczowi i Bramie (by Spustoszyć); Judyta i Bianka ją powstrzymały, lecz wpadły w ręce Dagmary/Wiktora. Bankierze odbili magazyn artefaktów niebezpiecznych. Power Suit Dagmary pochodzi od Oktawiana Mausa. Generatory Kompleksu są w coraz gorszym stanie.

1. [Odbudowa dowodzenia Świecy](/rpg/inwazja/opowiesci/konspekty/161030-odbudowa-dowodzenia-swiecy.html): 10/06/22 - 10/06/24
(161030-odbudowa-dowodzenia-swiecy)

Wraz ze śmiercią Aleksandra Sowińskiego poszedł deathspell - uderzył w magów podległych Wydziałowi Wewnętrznemu w Kopalinie. Andrea jednak jest "kretem" Agresta - uruchomił się w niej defensywny system Agresta, który zmusił ją do przeżycia. "Andrea" ściągnęła Rafaela Diakona i Lidię Weiner, by ją "wskrzesili". Agrest wyjaśnił jej sytuację i promował Andreę do Lady Terminus Kopalina. Andrea odbudowuje swoje siły w Świecy i okolice Piroga. Siły Spustoszenia polują na magów w okolicy. Świeca jest w ruinie.

1. [Morderczyni w masce Wandy](/rpg/inwazja/opowiesci/konspekty/160615-morderczyni-w-masce-wandy.html): 10/06/25 - 10/06/26
(160615-morderczyni-w-masce-wandy)

Pojawiła się "The Governess" która chcąc wyłączyć Blakenbauerów z akcji zabijała metodycznie eks-pacjentów Edwina i asystentkę Hektora, aż Blakenbauerowie dostali zakaz od Ottona opuszczania Rezydencji (fakt, że Edwin i Hektor mogliby zginąć jakby nie zmienili nawyków bardzo pomógł). Governess wyraźnie celuje w Blakenbauerów. Wygląda jak Wanda Ketran (ale to NIE ona) i planuje zniszczyć Hektora. Kinga Melit zginęła jako zasadzka na prokuratora.

1. [Bezwzględna Lady Terminus](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html): 10/06/25 - 10/06/28
(161101-bezwzgledna-lady-terminus)

Andrea powoli dostaje uprawnienia z Hipernetu; niestety Hipernet jest słaby. Pojawiło się niespodziewane wsparcie - Opel Astra KADEMu z Andżeliką i Marianem. Andrea po przeanalizowaniu danych z magitecha Wiktora Sowińskiego doszła do wniosku, że Overmind Spustoszenia jest powiązany z Mausami... i że Overmind szuka Renaty Maus. Andrea dowiedziała się też, że jeszcze jeden mag Wydziału przeżył, acz jest w złym stanie. Jako, że siły Spustoszenia chcą się wbić do EAM przez nieczynny portal w lesie, Andrea zniszczyła Wyzwalacz Pryzmaturgiczny, poświęciła ludzkiego pilota i zabiła kilku Spustoszonych magów i kilkudziesięciu ludzi, po czym ewakuowała się do Wtorku Śląskiego by odbudować swoją bazę w ogólnie bezpieczniejszym miejscu.

1. [Rezydencja? E, polujemy na dronę!](/rpg/inwazja/opowiesci/konspekty/160629-rezydencja-e-polujemy-na-drone.html): 10/06/27 - 10/06/30
(160629-rezydencja-e-polujemy-na-drone)

Tymotheus Blakenbauer chciał założyć drugą Rezydencję w Trocinie; Mojra chciała pomóc mu zdestabilizować to miejsce by to się udało. Blakenbauerowie odkryli, że teren monitorowany jest przez Spustoszone drony. Lokalne siły Świecy mocno trzymają Trocin, acz są atakowane przez "wampirzy ród Blutwurst" z obszarów utraconych - coś bazopodobnego znajduje się w Okólniczu. Zdaniem Mojry, ktokolwiek stoi za Governess czy Irytką, jednocześnie stoi za Blutwurstami. Nikt nie wie, że Blakenbauerowie to wykryli (Agrest dostał info od Mojry), Blakenbauerowie anulowali operację.

1. [Świeca nie zostawia swoich](/rpg/inwazja/opowiesci/konspekty/161113-swieca-nie-zostawia-swoich.html): 10/06/29 - 10/07/01
(161113-swieca-nie-zostawia-swoich)

Andrea ustabilizowała bazę we Wtorku i nawiązała ściślejszy sojusz z KADEMem (ułaskawiając Ignata i Silurię) oraz przez Mariana Łajdaka wynegocjowała sojusz z Kręgiem Życia (organizacją pro-viciniusową). Już nie może doczekać się swojego procesu. Potem wbiła do Czelimina (dyskretnie) by uratować tien Jankowskiego; okazało się, że miasto jest kontrolowane przez mortalisy - jak przed Porozumieniami Radomskimi. Oki... Andrea użyła wiedzy Kajetana (Pauliny ;p ) by Jankowskiego przenieść ludzkimi procedurami i go odzyskała. Fakt jak świetnie broniony jest Czelimin (Spustoszenie, Krew, Pryzmat Ekstazy Religijnej, mortalisy...) nie napawa optymizmem.

1. [Wycofanie Mileny z piekła](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html): 10/06/26 - 10/06/30
(160821-wycofanie-mileny-z-piekla)

Siluria zmusiła Biankę do skorumpowania Judyty ku protestowi powyższej. Elea Maus "odpłynęła" - wierzy, że rozkazuje im Karradrael i że to on stoi za rozkazami Dagmary. Siluria też zmusiła Wiktora do proszenia ją; zmieniła między nimi relację. Kończy się jedzenie dla viciniusów w Kompleksie Centralnym; Siluria załatwiła jedzenie z Millennium przez Estrellę. Dostała się też do Mileny Diakon i dowiedziała się, że ta ma problem "pasożytniczej boskiej matrycy". Jako, że Dagmara interesuje się Mileną... Siluria opracowała plan Krwawej Teleportacji Silurii i Mileny. Jeszcze pomogła Dagmarze dostać się do archiwów i była świadkiem tego, że Wiktor przejął centrum dowodzenia Kompleksu, a potem - uciekła. Wpierw jednak zostawiając w Judycie sygnał by móc ją uratować i odwrócić cały proces... ;-).

1. [Mała szara myszka...](/rpg/inwazja/opowiesci/konspekty/160707-mala-szara-myszka.html): 10/07/01 - 10/07/03
(160707-mala-szara-myszka)

Blakenbauerowie odkrywają powiązanie między hutą "Chrobry" w Czeliminie, Skrzydłorogiem w Bażantowie i Spustoszonymi dronami w Trocinie. Informują o tym Mojrę (informacja trafia do Agresta). The Governess porywa Patrycję Krowiowską. Hektor odkrywa powiązanie między Dagmarą Czeluść a Skubnym. Klara odkrywa, że Spustoszone drony powstają według planu w Skrzydłorogu.

1. [Tak wygrywa się sojuszami](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html): 10/07/02 - 10/07/04
(161120-tak-wygrywa-sie-sojuszami)

Andrea nawiązała połączenie z Anetą Rainer. Dowiedziała się o starym przekaźniku hipernetowym, poprzedniej generacji (przed Zaćmieniem); niestety, Julian i jego skrzydło nie dali rady go zdobyć (właściciel jest 'zaprogramowany'). Rafael powiedział Andrei, widząc stan Barana, że efekty uboczne już strzeliły. Nie spodziewał się celibatu. Andrea w odpowiedzi nawiązała kontakt z Draconisem i z nim weszła w sojusz. Najważniejsze wydarzenie to wejście przez Andreę w sojusz z Kręgiem Życia i oddaniem im miasta (Wtorku Śląskiego) w zamian za silną współpracę i przyjęcie na stałe placówki Świecy na tym obszarze. Andrea już nie może doczekać się swojego procesu...

1. [Jak ukraść ze Świecy Zajcewów](/rpg/inwazja/opowiesci/konspekty/160713-ukrasc-ze-swiecy-zajcewow.html): 10/07/04 - 10/07/06
(160713-ukrasc-ze-swiecy-zajcewow)

Elea Maus odkryła wstępne lekarstwo przeciwko Irytce Sprzężonej. Marian Agrest skoordynował akcję - ośmiu Zajcewów (Zajcewkommando) zostało uwolnionych przez agenta w Świecy i on wywołał chaos. Niestety, Elea metaforycznie wbiła mu nóż w plecy i Dagmara Wyjątek go zabiła. Zajcewowie uciekli przez specjalny, ad-hocowy portal Świecy dzięki Tatianie, po czym Blakenbauerowie ich wycofali kanałami i przez portal Millenium przerzucili poza Kopalin. W Kompleksie Centralnym jest chaos a Andrea dostała dostęp do 8 wykwalifikowanych Zajcewów, Wioletty Bankierz (która wie co się dzieje w Kompleksie) oraz katalistki Bianki Stein - eksperta od portali i energii magicznych. Blackenbauerowie dostali obietnicę odbicia Patrycji i odepchnięcia The Governess.

1. [Zajcewowie po drugiej stronie](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html): 10/07/05 - 10/07/07
(161204-zajcewowie-po-drugiej-stronie)

Lidia i Rafael postawili Dalię Weiner, zdobytą dzieki współpracy z Kręgiem Życia. Niestety, Dalia jest na pastylkach Rafaela. Ale - mają lekarza. Przybyło też więcej posiłków - Melodia i Laragnarhag z Millennium, co spowodowało konflikty - zwłaszcza z Kajetanem. Melodia ustabilizowała Barana a potem zdobyła przekaźnik hipernetowy. Andrea załatwiła z Marianem Łajdakiem użycie jego szmuglerskich talentów. Ten skontaktował ją z Kiryłem Sjeldem - który chce odzyskać czołg który utknął na Fazie Esuriit. Andrea ściągnęła go (czołg) do Czelimina by zrobić dywersję - odzyskać poranionych Zajcewów i dodatkowe wsparcie. W wyniku, Druga Strona dowiedziała się, że Mieszko Bankierz był w Czeliminie. Ale! Andrea odbiła wszystkich Zajcewów bezpiecznie i Karradrael nie wie, gdzie się znajduje baza Andrei (i myśli, że dowodzi Mieszko i że ów nie ma wielkich sił). Rudolf Jankowski postawiony, choć jeszcze bez "umysłu" - ale jest skuteczną maszyną do zabijania słuchającą tylko Andrei.

1. [Sleeper agent Oktawiana](/rpg/inwazja/opowiesci/konspekty/160803-sleeper-agent-oktawiana.html): 10/07/07 - 10/07/08
(160803-sleeper-agent-oktawiana)

Judyta Maus okazała się być potencjalnym sleeper agentem Oktawiana. Diana Weiner dostała dostęp do blueprintów grzybków mogących pomóc przejąć Kompleks Centralny. Blakenbauerowie dostali blueprinty Oddziału Trzygław - ścisłego oddziału viciniusów który przed Porozumieniem Radomskim terroryzował magów. Mają go dostarczyć Szlachcie. Hektor opóźnia.

1. [Presja ze strony Czelimina](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html): 10/07/08 - 10/07/10
(161229-presja-ze-strony-czelimina)

Po znacznym powiększeniu sił Andrei we Wtorku, kohezja zaczęła się sypać. Pojawiły się linie i podziały, które z trudem Andrea odbudowała, min. decydując się na odesłanie Rafaela i Laragnarhaga. Jednak zanim do tego doszło - Laragnarhag ostrzegł Andreę, że Mieszko jest atakowany Pryzmatycznie po Krwi. Andrea kazała go wsadzić do Brudnego Węzła; kraloth dodał, że to się stało też w Kompleksie Centralnym. Melodia jest nieszczęśliwa z tego jak jest traktowana, Druga Strona zrobiła pułapkę na Mieszka torturując Katalinę w Czeliminie a Wioletta ostrzegła, by Andrea nie próbowała jej ratować. O dziwo, najbardziej zdyscyplinowaną siłą Andrei są Zajcewowie. Co ją dobija. Aha, Andrea poprosiła Draconisa o pomoc w przyzwaniu Arazille do Czelimina...

1. [Zaszczepić Adriana Murarza!](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html): 10/07/09 - 10/07/11
(160810-zaszczepic-adriana-murarza)

Blakenbauerowie spróbowali uzyskać szczepionkę; jedynie stracili Adriana Murarza (zaszczepiony w Technoparku, na kwarantannie). Nie ma sensownej opcji jak to zrobić, ale dowiedzieli się, że programem kieruje Elea Maus. W międzyczasie, Projekt Trzygław dla Wiktora wymaga magów; Margaret tego nie zrobi (bo nie ma magów). Zajcewowie odbili Patrycję z rąk The Governess, Vladlena z kilkoma Zajcewami przybyła do Rezydencji Blakenbauerów. Wiktor odpalił Intensyfikator Energii Magicznej i wypalił siły Millennium ze Szpitala Pasteura. Dał Blakenbauerom 5 magów. Niech zrobią mu Trzygław.

1. [Eskalacja Czelimina, eskalacja Andrei](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html): 10/07/11 - 10/07/12
(161231-eskalacja-czelimina-andrei)

Andrea zignorowała błagania Kataliny. Laurena się odezwała, że ma mały oddział i chce uratować Katalinę. Andrea to też zignorowała - pułapka. Wioletta przyznała się, że wszystko wygadała Melodii. Melodia powiedziała Andrei, że Wioletta jest bardzo uszkodzona przez Wiktora. Rafael zaproponował, by dowiedzieć się o naturze wroga przez unię: Laragnarhag x Wioletta x Pszczelak. Andrea NIE powiedziała nie. Gdy Deiiw i Lilak przynieśli wiadomość o niebezpiecznych Patrolach ("oczy"), Rodion Zajcew zaproponował 'nakarmić' Laragnarhaga Mieszkiem (by zmylić). Andrea się zgodziła - jeśli to odwracalne ;-). Andrea obiecała Kiryłowi pomoc w znalezieniu Kariny - za co on pomógł jej przesunąć Śledzika do polowania na Patrole...

1. [Oblicze guwernantki](/rpg/inwazja/opowiesci/konspekty/160819-oblicze-guwernantki.html): 10/07/12 - 10/07/14
(160819-oblicze-guwernantki)

Blakenbauerowie odkrywają, że magowie mający być zmienieni w Oddział Trzygław mają wzór uszkodzony krwią Mausów. Weszli do Kotów (współpracując z Netherią, która uzyskała wymierne korzyści), do farmy z której uciekała The Governess i po starciach z pozostawionymi przez The Governess elementami udało im się odzyskać żywą ofiarę The Governess. Zdobyli sporo informacji na temat The Governess, min. jej tożsamość - Karolina Maus. A ona wie, kto zaatakował, acz nie wie co się dowiedzieli.

1. [Patrol? Kralotyczne maskowanie!](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html): 10/07/13 - 10/07/15
(170101-patrol-kralotyczne-maskowanie)

Lilak przyniósł Andrei info odnośnie Patroli i ich rozmieszczenia. Andrea zdecydowała się zamaskować bazę używając Laragnarhaga; 7 magów jako 'podwładni kralotha', Węzeł odpowiednio wyglądający... reszta magów do piwnicy. Zajcewowie zostali wysłani (na nie-powrót) by zniszczyć jeden Patrol a Tania, by wezwać Śledzika na inny Patrol. W większości się udało; Zajcewowie zgładzili "swój" Patrol a potem wypadł na nich Śledzik (zwiali) ;-). Karradrael wierzy, że Mieszko nie żyje, więc Laureny nie ma po co zmieniać w Hunter-Killera. Tymczasem Anna Kozak uzależniła się od kralotha, Lidii padło morale, Kajetan i Antoni Przylaz poszli do Żonkibora zrobić Ogniwo Arazille by ściągnąć ją do Czelimina i wrócili z powodzeniem. Andrea kazała Laragnarhagowi przekształcić Mieszka w "ochotnika" (niech umrze jako bohater). Wioletta Bankierz i Julian Pszczelak po 'lekkiej' pomocy Melodii i Rafaela (pomysł Rafaela, aprobata Andrei) zintegrowali w Laragnarhagu wiedzę o Kompleksie i swoich wrogach i mnóstwo informacji (min. Karradrael czy wpływ Dagmary czy Irytka) trafiło do Andrei i Agresta. Na końcu misji oddział Andrei opuścili: Lidia, Kajetan, Alojzy, Anna, Marian, Andżelika, Rafael i Laragnarhag. No i 10 Zajcewów łącznie z Rodionem. Andrea utrzymała kontrolę, wróg o niej nie wie i wreszcie Andrea może atakować.

1. [Plany Overminda](/rpg/inwazja/opowiesci/konspekty/160825-plany-overminda.html): 10/07/15 - 10/07/16
(160825-plany-overminda)

Z danych w komputerach i przepytania stronników Blakenbauerom udało się dowiedzieć jakie są plany Overminda. Dowiedzieli się też, że zakres przestrzeni Spustoszenia jest gigantyczny; praktycznie cała Polska - acz baza jest na Śląsku. Dowiedzieli się też, że Anna Myszeczka jest pewnym sojusznikiem a Karolina Maus jest Spustoszoną Defilerką kontrolowaną przez kanał Mausów; to sprawia że potencjalnie KAŻDY Maus jest pod kontrolą Overminda.

1. [Wojna Bogów w Czeliminie](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html): 10/07/16 - 10/07/18
(170103-wojna-bogow-w-czeliminie)

Jankowski doszedł do siebie i od razu zaczął się przydawać. Andrea dowiedziała się, że Ernest Maus nie nadaje się jako nowy seiras kontrolujący Karradraela - ale Zuzanna (którą Andrea zna) tak. Agrest i Jankowski mają zapewnić współpracę Blakenbauerów. Nie Mieszko a Joachim Kopiec przyzwał Arazille do Czelimina (ginąc) zaczynając wojnę bogów. Bianka jest w złej formie psychicznej; Tadeusz Baran (z pomocą Melodii) się nią opiekuje i ta dwójka zaczyna mieć się ku sobie. Arazille, przyzwana w Czeliminie uwolniła Śledzika od kontroli, o czym nikt nie wie.

1. [Aleksandria, krwawa Aleksandria](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html): 10/07/17 - 10/07/20
(160914-aleksandria-krwawa-aleksandria)

Uderzenie w Aleksandrię w super-ufortyfikowanym pałacu Blutwurstów. Ziarno Rezydencji dało im szansę gdy inkarnował się Overmind. Rozpaczliwa walka pod ciągłym oblężeniem - odcięcie Karoliny (by nie mogła wrócić), zniszczenie defensyw, a na końcu - zniszczenie Krwawej Aleksandrii. Zginęli: Irena Paniszok, Patrycja Krowiowska, unnamed. Uratowani ludzie i magowie sprzężeni z Aleksandrią, min. Adrian Murarz, Malwina Krówka i Anatol Weiner. Karina von Blutwurst i Hektor Blakenbauer rozstali się w zgodzie... aczkolwiek na horyzoncie jest też Krwawy Aderialith, który jest bliski zbudowania.
Znamy tożsamość Overminda - to Karradrael. Wiemy jak przywrócić KADEM. Wiemy jak unieszkodliwić Pacjenta Zero.
Duży sukces.

1. [Kontratak Karradraela](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html): 10/07/22 - 10/07/23
(161012-kontratak-karradraela)

W Rezydencji jest za dużo ludzi i magów; Rezydencja się rozpada. "Emilia" - Saith Kameleon - usuwa z gry Dagmarę Wyjątek i Kompleks Centralny ulega serii uszkodzeń i wybuchów. Jakby było mało, Karradrael ma namiar na Hektora (który zaczerpnął Magii Krwi w Aleksandrii) i Karradrael aktywnie Rezydencję destabilizuje. Anna Myszeczka i Hektor opuszczają Rezydencję; Anna chce uratować innych i poświęcić siebie i Hektora - uciekli do Rzecznej Chaty gdzie jest... Siluria. Przetrwali morderczy atak Karradraela, bo wpadł Opel Astra Setona i Głowica GS Aegis 0003 w formie Jadwigi stawiła czoła Karolinie. Aurelia kontra Aurelia. Siluria, Hektor, Anna, Andżelika, Mikado i Ignat odjechali Oplem... ranteleportem między fazami.

1. [Gambit Anety Rainer](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html): 10/07/19 - 10/07/23
(170122-gambit-anety-rainer)

Wojna Bogów w Czeliminie trwa. Aneta Rainer korzysta z okazji i siłami swoimi, Kurtyny, Andrei, Millennium opanowuje Kopalin z Amandą oraz desperackim gambitem stabilizuje Kompleks Centralny. Andrea, pozbawiona sił, odbija Laurenę, Katalinę i 3 innych magów z Czelimina; w procesie tego Śledzik jest uwolniony przez Arazille i poluje na Karradraela na własną rękę. Tatiana odkrywa, że Karradrael czerpie energię z oddziałów Harvestujących - poluje na okoliczne miejscowości i je wysysa kombem kraloth#mortalis. Z Kompleksu teleportowano sporo rzeczy (min. generatory) do Myślina. Trocin uniemożliwił pojawienie się Krwawego Aderialitha.

1. [Zagłodzona ekspedycja Świecy](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html): 10/07/24 - 10/07/25
(161026-zaglodzona-ekspedycja-swiecy)

Opel Astra wylądował Pomiędzy, ale szczęśliwie udało się go sprowadzić na... Fazę Esuriit - niebezpieczną, inteligentną i żarłoczną Fazę. Prosto do placówki Świecy, która utraciła kontakt ze Świecą Daemonica. Siluria i Hektor łatwo wkupili się w łaski podłamanej Marianny Sowińskiej; zwłaszcza, że mają plan ewakuacji. Sama placówka ma kilkanaście magów i ~ 150 ludzi, mają Pryzmat z syberionu i dziwne systemy ochronne. Gorzej, że placówkę nęka speculoid, część magów i ludzi stało się Istotami Esuriit (pożarci przez Esuriit) i nikt tego nie widzi...

1. [Magowie Esuriit w domu](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html): 10/07/26 - 10/07/28
(161102-magowie-esuriit-w-domu)

Marianna Sowińska żąda by Zespół współuczestniczył w niebezpiecznym zadaniu zdobywania energii; Siluria i Hektor powiedzieli, że oni potrzebują czasu i mocy by wydostać wszystkich z Esuriit. Starcie wygrane przez Zespół. Siluria ma plan ściągnięcia zamku As'caen - potrzebny jest Pryzmat i Moc. Pryzmat zapewnia bardka - Łukija - choć uzależnia to od wyciągnięcia z tej fazy jej syna. Paweł Maus (lekarz) powiedział Hektorowi, że podejrzewa, że część z nich (Ekspedycji) stała się Istotami Esuriit, choć nie są zgodni z normami. Też: Ekspedycja badała konkretnego Speculoida, który wyrwał się na wolność...

1. [Jak prawidłowo wpaść w pułapkę](/rpg/inwazja/opowiesci/konspekty/161109-jak-prawidlowo-wpasc-w-pulapke.html): 10/07/29 - 10/07/30
(161109-jak-prawidlowo-wpasc-w-pulapke)

Z wyprawy po surowce wrócił jedynie ranny Dosifiej. Scryer Andżeliki wykazał, że jeden terminus żyje; poluje nań speculoid. Ignat zbudował łazik i wraz z Andżeliką i Silurią skupili się na budowie Kometora - superciężkiego działa. Hektor, Anna, Mikado i Konstanty pojechali ratować terminusa Świecy. I to była pułapka - speculoid wezwał potężnego Czerwia. Anna unieszkodliwiła speculoida granatem nullifikacji i wrócili szybko do bazy z Czerwiem na ogonie. Ignat zranił Czerwia, który podkulił ogon i dał nogę. Morale wzrosło, pułapka rozwiązana i kupili sobie chwilę czasu. Ogólnie, super wynik (choć terminus do uratowania zginął)!

1. [Ponura historia ekspedycji Esuriit](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html): 10/07/31 - 10/08/01
(161124-ponura-historia-ekspedycji-esuriit)

Hektor i Andżelika badali archiwa Świecy i dowiedzieli się co się STAŁO na tej Ekspedycji. Ona nadal istnieje, bo brat Marianny został poświęcony w Rytuale Krwi a dziecko - Fiodor Maius - tworzy koszmar dookoła nich wszystkich Pryzmatem. I dzięki temu Faza Esuriit nie może ich znaleźć. Hektor chciał się poświęcić dla kilku ludzi w Rytuale Krwi, ale Siluria to zatrzymała. Dodatkowo, okazało się, że działka chroniące drogi na cmentarz są sabotowane - najpewniej przez Kazimierza Sowińskiego; naprawiła je Marzena po wykryciu tego przez Ignata i Annę. Siluria i Mikado pracują nad Pryzmatem pozytywnym. Marianna obiecała, że jak wrócą, powstanie fundacja pomagająca ludziom dla Hektora. By zapobiec atakowi zmarłych (energia # zwłoki == Energiak Esuriit), Dosifiej odciął kontakt z Wyspą Zmarłych rozbijając leyline.

1. [Sprowadzenie Mare Vortex](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html): 10/08/02 - 10/08/04
(161130-sprowadzenie-mare-vortex)

Zespół wykonał atak wyprzedzający przeciwko Kazimierzowi. Mag Esuriit zginął. Jednak zdążył wcześniej przygotować narzędzie osłabiające bańkę koszmaru; magowie zaczęli orientować się że są Esuriit i przekształcać się - zawsze jednak Anna i Mikado byli na miejscu. Wszyscy byli zmuszeni do strasznych kompromisów - ale udało się wycofać. Pojawił się zamek As'caen i udało się uciec z Esuriit. Nawet uratowali archiwa i część Ekspedycji. Speculoid podążył za nimi, na Fazę Daemonica.

1. [Lizanie ran na KADEMie](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html): 10/08/05 - 10/08/07
(161207-lizanie-ran-na-kademie)

Chwila wytchnienia - na KADEMie. Okazuje się, że KADEM nic nie wiedział; Faza Daemonica i Primus w tym zakresie się rozerwały i Mare Vortex "dryfuje". To działania Karradraela - dawno temu już coś takiego zrobiono. Ignat wpadł na to, że przecież da się dostać na Primusa przez Powiew Świeżości dzięki temu, że gdy ładował Głowicę to ta miała dostęp do wiedzy o KADEMie o którym nie powinna wiedzieć. Andżelika nie potrafi wybaczyć Mariannie tego, co ta zrobiła. Dzięki powrotowi Zespołu, KADEM ma możliwość otwarcia drogi zarówno do Świecy Daemonica jak i na Primus. A Marta wie "wszystko" co jest potrzebne i co do tej pory się zdarzyło i od Silurii i od Andżeliki i od Hektora.

1. [Spalone generatory pryzmatyczne](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html): 10/08/08 - 10/08/09
(170104-spalone-generatory-pryzmatyczne)

Hektor wdraża się w swoją nową rolę Bestii, adaptuje coraz lepiej. Niestety, po problemach na Esuriit na KADEMie padły generatory pryzmatyczne, co wymaga skupienia uwagi i usuwania efemeryd. Jako, że uratowani magowie Świecy przyciągają Pryzmat Esuriit, Siluria spacyfikowała Mariannę po swojemu a Norbert wpuścił magom Świecy różową mgiełkę ;-). Eis ma GS Aegis 0004 oraz korzysta z uszkodzenia generatorów pryzmatycznych.

1. [Przeprawa do Świecy Daemonica](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html): 10/08/10 - 10/08/11
(170125-przeprawa-do-swiecy-daemonica)

KADEM nawiązał kontakt ze Świecą; po ciężkiej przeprawie Siluria i Hektor dotarli do Świecy Daemonica. Eis poprosiła Hektora, by ten ją wydostał z KADEMu gdzie jest przetrzymywana. Infernia i Hektor weszli w fazę ostrą konfliktu. Whisperwind nie do końca jest normalna; rozmawia z martwymi przyjaciółmi z AD a pryzmatyczne efemerydy Marianny i Konstantego prawie zabiły cały Zespół. W skrócie: normalny dzień na KADEMie.

1. [Koniec wojny z Karradraelem](/rpg/inwazja/opowiesci/konspekty/170208-koniec-wojny-z-karradraelem.html): 10/08/12 - 10/08/13
(170208-koniec-wojny-z-karradraelem)

Świeca uśpiła Silurię i Hektora w Laboratorium Mgieł, próbując dojść do tego, co teraz robić. Eis wybudziła Hektora który wyciągnął Silurię. Razem przebijali się przez biurokrację; gdy tylko Agrest zrobił na nich fałszywy zamach, skorzystali z okazji. Siluria przekonała Wysoką Radę Świecy do współpracy z KADEMem a Agrest używając Saith Catapult zmusił Świecę do szybkiego przyzwania KADEMu. Renata Maus, Edwin, Hektor i umierająca Siluria trafili na KADEM...

1. [To się nazywa 'łupy wojenne'?](/rpg/inwazja/opowiesci/konspekty/170215-to-sie-nazywa-lupy-wojenne.html): 10/08/14 - 10/08/16
(170215-to-sie-nazywa-lupy-wojenne)

Edwin i Norbert przekształcili Renatę Maus w... "kundla trzech rodów": Maus, Blakenbauer i Diakon. Renata nie przyjęła tego najlepiej; zupełnie nie wiedziała, co się działo podczas jej nieobecności. Zdecydowała się współpracować z Hektorem w celu zbudowania potężnej prokuratury magów, by Świeca zapłaciła za to, co jej (Renacie) zrobili. Nowym seirasem Mausem został Abelard Maus. Siluria, po Destruktorze, jest bardzo słaba i potrzebuje wspomaganego egzoszkieletu do normalnego działania.

1. [Wygraliśmy wojnę... prawda?](/rpg/inwazja/opowiesci/konspekty/170226-wygralismy-wojne-prawda.html): 10/08/18 - 10/08/20
(170226-wygralismy-wojne-prawda)

Aneta Rainer odzyskuje kontrolę nad Kopalinem. Andrea wydała edykt zakazujący krzywdzić Mausów; też są ofiarami tego wszystkiego. Szczepan Sowiński ostrzegł Andreę przed Overlordami Hipernetowymi i innymi anomaliami. Okazało się, że Śledzik totalnie wyrwał się spod kontroli. Cóż - Andrea odwiedziła Myślin i dowiedziała się, że WSZYSCY magowie tam zostali przekształceni w Mausów a sam Myślin - w fortecę. Baltazar Maus (niechciany dar od Amandy) trafił właśnie tam, pod opiekę Jana "Weinera". Ogólnie, Andrei udało się nie dopuścić do znęcania się nad Mausami - co w tej sytuacji to naprawdę sporo...

1. [Renata Souris i echo Urbanka...](/rpg/inwazja/opowiesci/konspekty/170222-renata-souris-i-echo-urbanka.html): 10/08/14 - 10/08/15
(170222-renata-souris-i-echo-urbanka)

Renata Maus przybrała nowe nazwisko - Souris. Częściowo pogodziła się z losem; konspiruje i próbuje przekonać Hektora i Silurię dając im podarki i pokazując swoją pogardę do aktualnego Lorda Mausa. Bardzo niebezpieczna. Grupa magów KADEMu próbuje usunąć Renatę; w odpowiedzi na to z pomocą Kopidoła Siluria tworzy echo Kuby Urbanka. Renata dała radę przekonać Hektora do wyzbycia się zemsty na Karolinie Maus i przekazała Silurii "jakieś" kody. Przekazała też Silurii informacje jak uratować Milenę Diakon.

1. [Zegarmistrz i Alegretta](/rpg/inwazja/opowiesci/konspekty/170517-zegarmistrz-i-alegretta.html): 10/08/17 - 10/08/18
(170517-zegarmistrz-i-alegretta)

Hektor i Andżelika wspólnie chcą uruchomić prokuraturę magów, gdzie Hektor jest tą pragmatyczną częścią zespołu. Hektor wrócił do Rezydencji, na Primus, wzbudzając radość rodziny. Judyta Maus powiedziała Hektorowi o problemie z sygnałami od jej przyjaciela, Maurycego. Hektor, Edwin, Klara i Margaret dali radę wejść do Chatki Zegarowej i uratować umierającego Maurycego w stazie; w wyniku tego Hektor zaprzyjaźnił się z Alegrettą (opętany clockwork golem). Chatka Zegarowa została poważnie uszkodzona. Eis ujawniła się Hektorowi i Klarze, którzy na razie nie wiedzą co z tym robić...

1. [Chyba wolelibyśmy kartony...](/rpg/inwazja/opowiesci/konspekty/170405-chyba-wolelibysmy-kartony.html): 10/08/18 - 10/08/19
(170405-chyba-wolelibysmy-kartony)

Renata chcąc poprawić swoje warunki na KADEMie (i móc eksperymentować ze swoją magią) ostrzegła KADEM, że w Bażantowie Karradrael eksperymentował z różnymi wariantami Spustoszenia. Paweł, Siluria, Andżelika, Shadow i Midnight ruszyli tam i napotkali dziwną świńsko-ludzką Aleksandrię i coś nowego - Celatida Aleksandryjskiego. Wszystko z dziwnymi szczepami Spustoszenia. Po ciężkim przebijaniu się, KADEM porwał i zneutralizował eksperymentalne byty oraz rozszabrował obszar ;-).

1. [Autowar: pierwsze starcie](/rpg/inwazja/opowiesci/konspekty/170531-autowar-pierwsze-starcie.html): 10/08/20 - 10/08/22
(170531-autowar-pierwsze-starcie)

Estrella przyszła do Blakenbauerów pytając, czy to co się dzieje w Mszance to ich vicinius. Wyjaśniła, że tam dzieje się coś strasznego - infestacja os. Margaret i Hektor chcą to dla Blakenbauerów; Estrella nie oponuje. Na miejscu okazało się, że to bardzo silna infestacja a przeciwnik buduje małe Węzły taktyczne. Hektor, Estrella i Klara porwali kilka dron tajemniczego przeciwnika i wycofali się do Rezydencji. Na miejscu, Margaret zidentyfikowała ich przeciwnika jako autowara Świecy...

1. [Oślepienie autowara](/rpg/inwazja/opowiesci/konspekty/170607-oslepienie-autowara.html): 10/08/23 - 10/08/24
(170607-oslepienie-autowara)

Otton poproszony przez Marcelina powiedział, że Bzizma to autowar pochodzący od Blakenbauerów - stary autowar, przejęty przez Świecę. Estrella dodała, że Kompleks Centralny odciął kody kontrolne, by autowary nie wpadły w ręce Karradraela. Więc... Blakenbauerowie jako ród przygotowali się i odcięli Bzizmę od zaawansowanych sensorów konstruowanych przez autowara i kupując sobie czas. Skażenie i łamanie Maskarady w Mszance wzrosło, ku zmartwieniu wszystkich łącznie z autowarem.

1. [Odzyskać Aegis 0003](/rpg/inwazja/opowiesci/konspekty/170519-odzyskac-aegis-0003.html): 10/08/21 - 10/08/22
(170519-odzyskac-aegis-0003)

Siluria i Paweł dostali zadanie odnaleźć Sasankę i Aegis, bo Ilona chciała robić w TAMTYM TechVaulcie badania nad Skażonym Spustoszeniem... okazało się, że Aegis uwolniła Sasankę, pomogła mu rozszabrować TechVault KADEMu, przejąć Leśną Chatkę Blakenbauerów a Edward Sasanka pod wpływem Aegis zaczął zakładać własną gildię. Przez Efekt Skażenia Silurii i Pawła Ignat dostał dwie zamesmeryzowane czarodziejki. Aegis wahała się: Arazille czy KADEM. W końcu... wróciła na KADEM. Wszyscy chwilowo są na KADEMie aż Siluria i Paweł nie rozwiążą problemu z Sasanką...

1. [Suma niedokończonych spraw...](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html): 10/08/25 - 10/08/28
(170823-suma-niedokonczonych-spraw)

The operation of taking over the autowar has managed to make sure Bzizma is weakened and there exists an access to the core of the autowar. However, the forces of Kinglord have decided to inexplicably kill Hector. As a reaction to that, Iliusitius has sent Olga to make sure Hector will survive. In the process a mage artist, Peter, has been used as a conductor between the plans of the Kinglord and intelligence of the Blakenbauers.

1. [Wdzięczność Iliusitiusa](/rpg/inwazja/opowiesci/konspekty/170830-wdziecznosc-iliusitiusa.html): 10/08/29 - 10/08/30
(170830-wdziecznosc-iliusitiusa)

Arazille spotkała się osobiście z Silurią i dała jej dowody zbrodni Blakenbauerów. Rosną nastroje antymausowe podżegane przez Sowińskiego który nie może wybaczyć śmierci Sabiny. Kinglord poluje na Piotra, ale Mordred go uratował. Piotr chce być wolny od ochrony, ale dzięki aurze kapłana Iliusitiusa... polują na niego magowie i Mordred ratuje go przed śmiercią, za co Piotr nie jest mu szczególnie wdzięczny ;-). Piotr nadal maluje obrazy pokazujące to, co Iliusitius chce pokazać...

1. [Kolejna porażka Kinglorda](/rpg/inwazja/opowiesci/konspekty/170914-kolejna-porazka-kinglorda.html): 10/08/31 - 10/09/02
(170914-kolejna-porazka-kinglorda)

Hektor dotarł do Piotra, ale z uwagi na to, że tylko ON widzi aurę Arazille i Iliusitiusa w martwym magu... został aresztowany jako zabójca. Kinglord nie dał rady zastrzelić Henryka snajperem, ale wprowadził go w Bestię. Na miejsce przyszła Siluria i wynegocjowała bezpieczeństwo Hektora i jego leczenie. Tymczasem Henryk Siwiecki został przebudowany przez Rafaela Diakona w Henriettę - i uwolniony od władzy i potęgi Kinglorda. Siły Kinglorda... lub Arazille szukają Bzizmy i możliwości zdominowania autowara.

1. [Początki prokuratury](/rpg/inwazja/opowiesci/konspekty/170920-poczatki-prokuratury.html): 10/09/03 - 10/09/05
(170920-poczatki-prokuratury)

* Hektor i Mordred założyli prokuraturę magów wspierani przez siły Srebrnej Świecy i pozostałych gildii.
* Iliusitius w tle wpływa na prokuraturę magów, acz nie udało mu się dostać dostępu do hipernetu (co zniszczyłoby wszystkich magów Świecy).
* Karradrael się obudził pod kontrolą Abelarda Mausa. Mausowie współpracują z innymi siłami.
* Renata jest urażona faktem, że straciła techbunker. Ale jest chętna do współpracy z Silurią celem zniszczenia Kinglorda - śmierć Henryka zabrałaby Renacie "komunikację", jej poddanych.
* Whisperwind nie jest w stanie dostać się do Kinglorda. Defiler stał się potężniejszy niż kiedykolwiek - być może nawet niż Whisper.
* Opera jest w bardzo dobrym stanie. Zarówno Operiatrix jak i sam budynek jakoś dojdą do siebie.
* Niesamowitej mocy superkaskada Paradoksu erotycznego poleciała w leyline pod operą. Nie do końca wiadomo, co się z tą energią stanie.
* Elementami planu Arazille było rozpuszczenie chaosu (Mausowie, Blakenbauerowie) by mogła się ufortyfikować. Jej zdaniem, by kontrować Iliusitiusa.
* Henryk Siwiecki trafił do Millennium, pokazując Mordredowi dużo Mausowych techbunkrów.

## Progresja

|Misja|Progresja|
|------|------|
|[Streamerka w Na Świeczniku](/rpg/inwazja/opowiesci/konspekty/170501-streamerka-w-na-swieczniku.html)|[Antygona Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-antygona-diakon.html) nieco przymusowa współpracowniczka 'Na Świeczniku', aż odpracuje...|
|[Szept z Academii Daemonica](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|[Whisperwind](/rpg/inwazja/opowiesci/karty-postaci/9999-whisperwind.html) podkradła Dwunastościan, który Karina Paczulis uznała za 'głęboko zaraźliwy jak się otworzy, trzeba zniszczyć'. Schowała przed wszystkimi.|
|[Zazdrość Warmastera](/rpg/inwazja/opowiesci/konspekty/161218-zazdrosc-warmastera.html)|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html) dostała małą, 'posrebrzaną' foczkę znalezioną przez Ignata gdzieś na KADEMie Daemonica|
|[Zazdrość Warmastera](/rpg/inwazja/opowiesci/konspekty/161218-zazdrosc-warmastera.html)|[Warmaster](/rpg/inwazja/opowiesci/karty-postaci/9999-warmaster.html) doprowadził glashunda do stanu 'pryzmatyczna maszyna zagłady'; schował 'na strychu'|
|[Polowanie na Mausównę](/rpg/inwazja/opowiesci/konspekty/170228-polowanie-na-mausowne.html)|[Judyta Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-judyta-maus.html) ma stalkera, niebezpiecznego maga, który chce ją... zdefilerować? splugawić? zMausić?|
|[Samotna w świecie magow](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html)|KADEM dostał oficjalne przeprosiny; nastroje na KADEMie się poprawiły|
|[Samotna w świecie magow](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html)|[Anna Kozak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kozak.html) Przestraszyła się KADEMu. Siluria jest członkiem KADEMu, ale nie chce skrzywdzić Anny. Nie podejmie walki z KADEMem z własnej woli.|
|[Samotna w świecie magow](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html)|[Anna Kozak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kozak.html) Nienawidzi Ignata Zajcewa (z KADEMu) z całego swojego serca.|
|[Samotna w świecie magow](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html)|[Urszula Murczyk](/rpg/inwazja/opowiesci/karty-postaci/1709-urszula-murczyk.html) Ma problemy w Świecy przez to, co zrobiła (KADEM, eksperymenty w Kotach). Spychana do Szlachty.|
|[Samotna w świecie magow](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html)|[Urszula Murczyk](/rpg/inwazja/opowiesci/karty-postaci/1709-urszula-murczyk.html) Dwa tygodnie ciężkiego chorowania przez Perversity jej własnego narkotyku - robota Wioletty.|
|[Duch Opery](/rpg/inwazja/opowiesci/konspekty/170808-duch-opery.html)|[Piotr Kit](/rpg/inwazja/opowiesci/karty-postaci/1709-piotr-kit.html) zobowiązany do zostania kustoszem zrujnowanej opery w Czapkowiku|
|[Duch Opery](/rpg/inwazja/opowiesci/konspekty/170808-duch-opery.html)|[Żaneta Kroniacz](/rpg/inwazja/opowiesci/karty-postaci/9999-zaneta-kroniacz.html) ma krew viciniusa; nie jest w pełni człowiekiem. Uaktywniły się jej aspekty krwi viciniusa?|
|[Klub Dare Shiver](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|[Oliwier Bonwant](/rpg/inwazja/opowiesci/karty-postaci/9999-oliwier-bonwant.html) zagra w Rzecznej Chacie; wybaczył Ignatowi pobicie|
|[Klub Dare Shiver](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|[Balrog Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-balrog-bankierz.html) wybił się kosztem Ignata Zajcewa jako paladyn w lśniącej zbroi chroniący Oliwiera|
|[Klub Dare Shiver](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|[Jolanta Sowińska](/rpg/inwazja/opowiesci/karty-postaci/1709-jolanta-sowinska.html) ma dożywotnią kartę wstępu do Dare Shiver.|
|[Klub Dare Shiver](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html) dostała kartę wstępu do Dare Shiver, dożywotnią.|
|[Klub Dare Shiver](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|[Judyta Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-judyta-maus.html) awestruck Silurią. Fangirl Silurii ;-).|
|[Naszyjnik Przenośnych Wspomnień](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html)|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html) upolował i zdobył sobie Mimika Symbiotycznego|
|[Ratując syrenopnącze](/rpg/inwazja/opowiesci/konspekty/170417-ratujac-syrenopnacze.html)|[Adonis Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1709-adonis-sowinski.html) powiązany z organizacją gigantycznej imprezy erotycznej wraz z Silurią Diakon|
|[Ratując syrenopnącze](/rpg/inwazja/opowiesci/konspekty/170417-ratujac-syrenopnacze.html)|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html) powiązana z organizacją gigantycznej imprezy erotycznej wraz z Dare Shiver|
|[Ratując syrenopnącze](/rpg/inwazja/opowiesci/konspekty/170417-ratujac-syrenopnacze.html)|KADEM ma dostęp do syrenopnącza; zostało uratowane|
|[Camgirl na dragach](/rpg/inwazja/opowiesci/konspekty/170319-camgirl-na-dragach.html)|[Artur Bryś](/rpg/inwazja/opowiesci/karty-postaci/1709-artur-brys.html) ma się opiekować Pauliną Widoczek|
|[Camgirl na dragach](/rpg/inwazja/opowiesci/konspekty/170319-camgirl-na-dragach.html)|[Paulina Widoczek](/rpg/inwazja/opowiesci/karty-postaci/9999-paulina-widoczek.html) ma Artura Brysia jako opiekuna|
|[Camgirl na dragach](/rpg/inwazja/opowiesci/konspekty/170319-camgirl-na-dragach.html)|[Artur Bryś](/rpg/inwazja/opowiesci/karty-postaci/1709-artur-brys.html) jego ziomkiem i przyjacielem jest Roman Brunowicz z Ognistych Niedźwiedzi|
|[Biznes pośród niesnasek](/rpg/inwazja/opowiesci/konspekty/170707-biznes-posrod-niesnasek.html)|[Judyta Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-judyta-maus.html) ma patronat Franciszka Mausa i ochronę viciniusów Warmastera.|
|[Biznes pośród niesnasek](/rpg/inwazja/opowiesci/konspekty/170707-biznes-posrod-niesnasek.html)|[Judyta Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-judyta-maus.html) Sad Judyty uległ Skażeniu - echem zwarcia Ferdynanda i Oktawiana. Jej własny sad nią gardzi.|
|[Przebudzenie viciniusa](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|[Patrycja Krowiowska](/rpg/inwazja/opowiesci/karty-postaci/1709-patrycja-krowiowska.html) wierzy w dziwną szajkę hakerów, którzy bawią się w "x-files" na kamerach przemysłowych|
|[Przebudzenie viciniusa](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|[Paulina Widoczek](/rpg/inwazja/opowiesci/karty-postaci/9999-paulina-widoczek.html) przekształciła się w viciniusa pod opieką i czujnym nadzorem Edwina Blakenbauera; będzie funkcjonować w świecie ludzi|
|[Przebudzenie viciniusa](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html) przekształcił swojego Mimika Symbiotycznego w myśli o wysokim poziomie spolegliwości, nieagresji i uległości|
|[Przebudzenie viciniusa](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|[Artur Bryś](/rpg/inwazja/opowiesci/karty-postaci/1709-artur-brys.html) dostał opinię gościa, którego skroiła słaba dziewczyna w szpitalu; najpewniej się do niej dobierał czy coś?|
|[Przebudzenie viciniusa](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|[Urszula Murczyk](/rpg/inwazja/opowiesci/karty-postaci/1709-urszula-murczyk.html) Kornel skutecznie zrzucił na nią odpowiedzialność za narkotyki bojowe nad którymi eksperymentował|
|[Szmuglowanie artefaktów](/rpg/inwazja/opowiesci/konspekty/161005-szmuglowanie-artefaktow.html)|Srebrna Świeca w Kopalinie ma sieć quasi-legalnego przekazywania artefaktów z KADEMem. Poza oficjalnymi kanałami i poza Lordem Terminusem.|
|[Szmuglowanie artefaktów](/rpg/inwazja/opowiesci/konspekty/161005-szmuglowanie-artefaktow.html)|KADEM na wysokim poziomie (szefostwo) ma sieć quasi-legalnego przekazywania artefaktów ze Świecą w Kopalinie... poza Ozydiuszem.|
|[Szmuglowanie artefaktów](/rpg/inwazja/opowiesci/konspekty/161005-szmuglowanie-artefaktow.html)|KADEM ma sieć przemytniczą z Jewgenijem Zajcewem, na wschód|
|[Piećdziesiąt oblicz Szlachty](/rpg/inwazja/opowiesci/konspekty/160327-piecdziesiat-oblicz-szlachty.html)|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html) uczy się walki od Wioletty Bankierz|
|[Odbudowa dowodzenia Świecy](/rpg/inwazja/opowiesci/konspekty/161030-odbudowa-dowodzenia-swiecy.html)|[Andrea Wilgacz](/rpg/inwazja/opowiesci/karty-postaci/1709-andrea-wilgacz.html) została tymczasową Lady Terminus Kopalina|
|[Odbudowa dowodzenia Świecy](/rpg/inwazja/opowiesci/konspekty/161030-odbudowa-dowodzenia-swiecy.html)|[Tadeusz Baran](/rpg/inwazja/opowiesci/karty-postaci/1709-tadeusz-baran.html) stymulowany i zakochany w Andrei; kompetentny jak kiedyś|
|[Odbudowa dowodzenia Świecy](/rpg/inwazja/opowiesci/konspekty/161030-odbudowa-dowodzenia-swiecy.html)|[Andrea Wilgacz](/rpg/inwazja/opowiesci/karty-postaci/1709-andrea-wilgacz.html) musi pić krew maga Świecy, by przeżyć|
|[Portal do EAM](/rpg/inwazja/opowiesci/konspekty/160724-portal-do-eam.html)|[Gerwazy Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/9999-gerwazy-myszeczka.html) przysługa u Silurii, może ściągnąć coś od Millennium przy okazji jedzenia dla viciniusów|
|[Wycofanie Mileny z piekła](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html)|[Judyta Karnisz](/rpg/inwazja/opowiesci/karty-postaci/9999-judyta-karnisz.html) złamana i zindoktrynowana do Wiktora Sowińskiego|
|[Wycofanie Mileny z piekła](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html)|[Dagmara Wyjątek](/rpg/inwazja/opowiesci/karty-postaci/1709-dagmara-wyjątek.html) zdobyła kontrolę nad Kompleksem Centralnym|
|[Wycofanie Mileny z piekła](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html)|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html) uzyskała kontakty z Wiktorem Sowińskim; jest praktycznie ściśle w jej obszarze wpływów|
|[Wycofanie Mileny z piekła](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html)|[Milena Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-milena-diakon.html) transfuzje dają jej możliwości działania jak kiedyś, aczkolwiek krótkoterminowo|
|[Wycofanie Mileny z piekła](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html)|[Dagmara Wyjątek](/rpg/inwazja/opowiesci/karty-postaci/1709-dagmara-wyjątek.html) straciła Milenę Diakon|
|[Mała szara myszka...](/rpg/inwazja/opowiesci/konspekty/160707-mala-szara-myszka.html)|[Karolina Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-karolina-maus.html) łapie Patrycję Krowiowską.|
|[Mała szara myszka...](/rpg/inwazja/opowiesci/konspekty/160707-mala-szara-myszka.html)|[Patrycja Krowiowska](/rpg/inwazja/opowiesci/karty-postaci/1709-patrycja-krowiowska.html) wpada w ręce The Governess.|
|[Mała szara myszka...](/rpg/inwazja/opowiesci/konspekty/160707-mala-szara-myszka.html)|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html) traci Patrycję Krowiowską|
|[Mała szara myszka...](/rpg/inwazja/opowiesci/konspekty/160707-mala-szara-myszka.html)|Skrzydłoróg nie nadąża z produkcją dron by dorównać sprzedaży; daje 1 Fire więcej.|
|[Jak ukraść ze Świecy Zajcewów](/rpg/inwazja/opowiesci/konspekty/160713-ukrasc-ze-swiecy-zajcewow.html)|Blakenbauerowie ocieplenie relacji z lojalistami Świecy, z Millennium, z Zajcewami Iriny.|
|[Jak ukraść ze Świecy Zajcewów](/rpg/inwazja/opowiesci/konspekty/160713-ukrasc-ze-swiecy-zajcewow.html)|[Andrea Wilgacz](/rpg/inwazja/opowiesci/karty-postaci/1709-andrea-wilgacz.html) dostała dostęp do komando 8 Zajcewów dowodzonego przez Tatianę Zajcew|
|[Jak ukraść ze Świecy Zajcewów](/rpg/inwazja/opowiesci/konspekty/160713-ukrasc-ze-swiecy-zajcewow.html)|[Andrea Wilgacz](/rpg/inwazja/opowiesci/karty-postaci/1709-andrea-wilgacz.html) dostała dostęp do Bianki Stein, kompetentnej katalistki i eksperta od portali|
|[Jak ukraść ze Świecy Zajcewów](/rpg/inwazja/opowiesci/konspekty/160713-ukrasc-ze-swiecy-zajcewow.html)|[Andrea Wilgacz](/rpg/inwazja/opowiesci/karty-postaci/1709-andrea-wilgacz.html) dostała Wiolettę Bankierz pod komendę; wraz z wiedzą o tym co dzieje się w Kompleksie Centralnym|
|[Zajcewowie po drugiej stronie](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html)|[Kirył Sjeld](/rpg/inwazja/opowiesci/karty-postaci/9999-kiryl-sjeld.html) może spokojnie operować na okolicznych terenach póki Andrea jest Lady Terminus|
|[Sleeper agent Oktawiana](/rpg/inwazja/opowiesci/konspekty/160803-sleeper-agent-oktawiana.html)|Szlachta + blueprinty toksycznych grzybków do przejęcia kontroli nad Kompleksem Centralnym|
|[Sleeper agent Oktawiana](/rpg/inwazja/opowiesci/konspekty/160803-sleeper-agent-oktawiana.html)|Blakenbauerowie + blueprinty oddziału szturmowego "Trzygław", sprzed Porozumień Radomskich|
|[Presja ze strony Czelimina](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html)|[Andrea Wilgacz](/rpg/inwazja/opowiesci/karty-postaci/1709-andrea-wilgacz.html) nie jest już narażona na samozniszczenie przez Wydział Wewnętrzny|
|[Presja ze strony Czelimina](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html)|[Andrea Wilgacz](/rpg/inwazja/opowiesci/karty-postaci/1709-andrea-wilgacz.html) ma uprawnienia Lady Terminus "starego hipernetu"|
|[Presja ze strony Czelimina](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html)|[Tatiana Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-tatiana-zajcew.html) ma dostęp do kontaktów swoich rodziców i niektórzy nawet będą jej słuchać ;-)|
|[Presja ze strony Czelimina](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html)|[Tatiana Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-tatiana-zajcew.html) ma dostęp do informacji o różnych strukturach i obiektach swoich rodziców i stowarzyszonych|
|[Presja ze strony Czelimina](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html)|[Tatiana Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-tatiana-zajcew.html) ma dostęp do 'artylerii' swoich rodziców i stowarzyszonych, rozmieszczonych w techbunkrach na Śląsku|
|[Presja ze strony Czelimina](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html)|[Lidia Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-lidia-weiner.html) ma bardzo złą opinię o Silurii Diakon z uwagi na wydarzenia w Kompleksie Centralnym i Infensę|
|[Presja ze strony Czelimina](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html)|[Kajetan Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-kajetan-weiner.html) ma bardzo złą opinię o Silurii Diakon z uwagi na wydarzenia w Kompleksie Centralnym i Infensę|
|[Zaszczepić Adriana Murarza!](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html)|[Adrian Murarz](/rpg/inwazja/opowiesci/karty-postaci/9999-adrian-murarz.html) zaszczepiony na Irytkę Sprzężoną|
|[Zaszczepić Adriana Murarza!](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html)|Blakenbauerowie tracą Adriana Murarza (tymczasowo)|
|[Zaszczepić Adriana Murarza!](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html)|[Karolina Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-karolina-maus.html) traci Patrycję Krowiowską|
|[Zaszczepić Adriana Murarza!](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html)|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html) odzyskuje Patrycję Krowiowską|
|[Zaszczepić Adriana Murarza!](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html)|Blakenbauerowie dostają Vladlenę i grupkę Zajcewów, którzy budują broń masowego zniszczenia Spustoszenia (i nie tylko)|
|[Zaszczepić Adriana Murarza!](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html)|Blakenbauerowie #5 magów "skazańców" do zbudowania oddziału Trzygław|
|[Oblicze guwernantki](/rpg/inwazja/opowiesci/konspekty/160819-oblicze-guwernantki.html)|[Karolina Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-karolina-maus.html) wie, że przeciwko niej stają Blakenbauerowie|
|[Oblicze guwernantki](/rpg/inwazja/opowiesci/konspekty/160819-oblicze-guwernantki.html)|[Karolina Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-karolina-maus.html) okazuje się być bardzo opanowaną defilerką o ogromnej mocy (co samo w sobie jest oksymoronem)|
|[Oblicze guwernantki](/rpg/inwazja/opowiesci/konspekty/160819-oblicze-guwernantki.html)|Blakenbauerowie dostają ślady biologiczne Karoliny Maus|
|[Oblicze guwernantki](/rpg/inwazja/opowiesci/konspekty/160819-oblicze-guwernantki.html)|Blakenbauerowie dostają komputery The Governess gdzie znajdują się cenne informacje o jej ruchach i jej planach / działaniach|
|[Oblicze guwernantki](/rpg/inwazja/opowiesci/konspekty/160819-oblicze-guwernantki.html)|Blakenbauerowie dostają Krwawokrąg, który może posłużyć jako prototyp do badań (lub do kontroli Anny Myszeczki)|
|[Oblicze guwernantki](/rpg/inwazja/opowiesci/konspekty/160819-oblicze-guwernantki.html)|Blakenbauerowie dostają Annę Myszeczkę|
|[Oblicze guwernantki](/rpg/inwazja/opowiesci/konspekty/160819-oblicze-guwernantki.html)|[Netheria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-netheria-diakon.html) dostaje usługi Aliny i Dionizego wtedy, gdy ci nie są zajęci i gdy nie koliduje to z celami Blakenbauerów|
|[Oblicze guwernantki](/rpg/inwazja/opowiesci/konspekty/160819-oblicze-guwernantki.html)|[Netheria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-netheria-diakon.html) dostaje kontakty Hektora w świecie ludzi|
|[Patrol? Kralotyczne maskowanie!](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|[Rafael Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-rafael-diakon.html) wróg w osobie Lidii Weiner|
|[Patrol? Kralotyczne maskowanie!](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|[Anna Kozak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kozak.html) uzależnienie od kralotha (Laragnarhaga)|
|[Patrol? Kralotyczne maskowanie!](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|[Lidia Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-lidia-weiner.html) silna niechęć do Rafaela Diakona|
|[Plany Overminda](/rpg/inwazja/opowiesci/konspekty/160825-plany-overminda.html)|[Klara Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-klara-blakenbauer.html) dostaje czar hermetyczny "Stworzenie Aleksandrii"|
|[Wojna Bogów w Czeliminie](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html)|[Wojmił Kopiec](/rpg/inwazja/opowiesci/karty-postaci/9999-wojmil-kopiec.html) wszystkie ewentualne problemy i zarzuty zostały z niego zdjęte|
|[Wojna Bogów w Czeliminie](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html)|[Rudolf Jankowski](/rpg/inwazja/opowiesci/karty-postaci/9999-rudolf-jankowski.html) wreszcie pełnosprawny, pełnosprytny i cholernie niebezpieczny|
|[Wojna Bogów w Czeliminie](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html)|[Bianka Stein](/rpg/inwazja/opowiesci/karty-postaci/1709-bianka-stein.html) widzi bohaterstwo Tadeusza Barana w niesamowicie pozytywnym Świetle|
|[Wojna Bogów w Czeliminie](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html)|[Andrea Wilgacz](/rpg/inwazja/opowiesci/karty-postaci/1709-andrea-wilgacz.html) w prezencie od Kiryła jedna porcja granatów entropicznych (zakazanych, zdecydowanie przepakowanych)|
|[Wojna Bogów w Czeliminie](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html)|[Tadeusz Baran](/rpg/inwazja/opowiesci/karty-postaci/1709-tadeusz-baran.html) pozytywny wpis do akt od Andrei za uratowanie oddziału przed Portalem Energii Bogów|
|[Wojna Bogów w Czeliminie](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html)|[Bianka Stein](/rpg/inwazja/opowiesci/karty-postaci/1709-bianka-stein.html) lekka fobia przed Mausami (-1 SCA, SCD, SCF w kontaktach z Diakonami)|
|[Wojna Bogów w Czeliminie](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html)|[Bianka Stein](/rpg/inwazja/opowiesci/karty-postaci/1709-bianka-stein.html) lekka fobia przed Diakonami (-1 SCA, SCD, SCF w kontaktach z Diakonami)|
|[Wojna Bogów w Czeliminie](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html)|[Bianka Stein](/rpg/inwazja/opowiesci/karty-postaci/1709-bianka-stein.html) eliksir Rafaela Diakona, który ją zachęca do Tadeusza Barana|
|[Wojna Bogów w Czeliminie](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html)|[Tadeusz Baran](/rpg/inwazja/opowiesci/karty-postaci/1709-tadeusz-baran.html) eliksir Rafaela Diakona, który go zachęca do Bianki Stein|
|[Aleksandria, krwawa Aleksandria](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html)|Blakenbauerowie zyskali Adriana Murarza|
|[Aleksandria, krwawa Aleksandria](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html)|Blakenbauerowie stracili Ziarno Rezydencji|
|[Aleksandria, krwawa Aleksandria](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html)|Blakenbauerowie dostali ogromny szacunek, respekt i chwałę za zniszczenie Aleksandrii i uratowanie tak wielu istnień|
|[Aleksandria, krwawa Aleksandria](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html)|Blakenbauerowie dostali mnóstwo ludzi i magów kiedyś sprzężonych z Aleksandrią|
|[Sprowadzenie Mare Vortex](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html)|KADEM przechwycił dziwny artefakt z Esuriit składający się ze szczęki Coś Robiącej Na Esuriit.|
|[Sprowadzenie Mare Vortex](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html)|KADEM przechwycił Archiwum Ekspedycji Esuriit.|
|[Lizanie ran na KADEMie](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html)|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html) traci marker od Karradraela|
|[Lizanie ran na KADEMie](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html)|KADEM potrafi fabrykować szczepionkę Esuriit odcinającą Mausów od Karradraela na pewien czas|
|[Lizanie ran na KADEMie](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html)|[Franciszek Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-maus.html) ma szczepionkę Esuriit odcinającą go od Karradraela (w ramach potrzeby)|
|[Renata Souris i echo Urbanka...](/rpg/inwazja/opowiesci/konspekty/170222-renata-souris-i-echo-urbanka.html)|[Renata Souris](/rpg/inwazja/opowiesci/karty-postaci/1709-renata-souris.html) uważa, że jest winna życie Silurii i Hektorowi. Nie jest pewna, czy to korzyść dla nich czy powinna ich za to zabić...|
|[Renata Souris i echo Urbanka...](/rpg/inwazja/opowiesci/konspekty/170222-renata-souris-i-echo-urbanka.html)|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html) otrzymała od Renaty nieznane kody kontrolne do "czegoś". Quasar nie wie jeszcze co z tym zrobić...|
|[Renata Souris i echo Urbanka...](/rpg/inwazja/opowiesci/konspekty/170222-renata-souris-i-echo-urbanka.html)|KADEM dostał efemerydę Kuby Urbanka, który strzeże zdrowia i życia Renaty Souris.|
|[Autowar: pierwsze starcie](/rpg/inwazja/opowiesci/konspekty/170531-autowar-pierwsze-starcie.html)|[Estrella Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-estrella-diakon.html) ma tymczasowo podniesione uprawnienia jako terminus przez Andreę; jest użyteczna i można na niej polegać, więc...|
|[Odzyskać Aegis 0003](/rpg/inwazja/opowiesci/konspekty/170519-odzyskac-aegis-0003.html)|[Teresa Koliczer](/rpg/inwazja/opowiesci/karty-postaci/9999-teresa-koliczer.html) zamesmeryzowana Ignatem Zajcewem dzięki działaniom Silurii i Efektu Skażenia|
|[Odzyskać Aegis 0003](/rpg/inwazja/opowiesci/konspekty/170519-odzyskac-aegis-0003.html)|[Sandra Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-sandra-maus.html) zamesmeryzowana Ignatem Zajcewem dzięki działaniom Silurii i Efektu Skażenia|
|[Odzyskać Aegis 0003](/rpg/inwazja/opowiesci/konspekty/170519-odzyskac-aegis-0003.html)|[Ignat Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1802-ignat-zajcew.html) obiekt zamesmeryzowania Sandry Maus i Teresy Koliczer dzięki działaniom Silurii i Efektu Skażenia|
|[Suma niedokończonych spraw...](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|[Judyta Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-judyta-maus.html) uważa Marcelina Blakenbauera za swojego prawdziwego przyjaciela.|
|[Suma niedokończonych spraw...](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|[Henryk Siwiecki](/rpg/inwazja/opowiesci/karty-postaci/1709-henryk-siwiecki.html) można do niego mówić "Alicjo". Zmieniony przez Kinglorda w dziewczynkę.|
|[Suma niedokończonych spraw...](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|[Marcelin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-marcelin-blakenbauer.html) ma oddaną przyjaciółkę w Judycie Maus.|
|[Kolejna porażka Kinglorda](/rpg/inwazja/opowiesci/konspekty/170914-kolejna-porazka-kinglorda.html)|[Renata Souris](/rpg/inwazja/opowiesci/karty-postaci/1709-renata-souris.html) od tej pory jest sprzężona mindlinkiem z Henrykiem Siwieckim w kobiecej formie|
|[Kolejna porażka Kinglorda](/rpg/inwazja/opowiesci/konspekty/170914-kolejna-porazka-kinglorda.html)|[Henryk Siwiecki](/rpg/inwazja/opowiesci/karty-postaci/1709-henryk-siwiecki.html) od tej pory jest damą i to rodu Souris, jak Renata; sprzężona jest z Renatą mindlinkiem|


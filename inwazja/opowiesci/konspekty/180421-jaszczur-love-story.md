---
layout: inwazja-konspekt
title:  "Jaszczur Love Story"
campaign: dusza-czapkowika
gm: żółw
players: kić, arleta
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [180419 - Jaszczury rządzą miastem](180419-jaszczury-rzadza-miastem.html)

### Chronologiczna

* [180419 - Jaszczury rządzą miastem](180419-jaszczury-rzadza-miastem.html)

## Kontekst ogólny sytuacji

### Opis sytuacji

* Wątek "Operiatrix bez Piotra Kita".

## Punkt zerowy

* null

## Potencjalne pytania historyczne

* null

## Misja właściwa

**Dzień 1**:
Cztery dni później, Genowefa dostała telefon od pani burmistrz - jaszczura w przebraniu. Ta poprosiła o spotkanie; jest wyraźnie lekko poruszona. Daria lubi pomagać, więc pojechała ją odwiedzić. Jaszczurka czekała na poligonie paintballowym (należącym do niej). Powiedziała, że jakkolwiek wszystkie problemy są rozwiązane w większym lub mniejszym stopniu, pojawiło się coś nowego. dziewczyna w bieli. Przychodzi pod operę i zaczyna tańczyć do nieprzytomności. Znaczy: niefart. Coś jeszcze się tam dzieje.

And then, there was a gunshot. The lizard stiffened and fell down. She got shot by someone. Genowefa realized, that she is alone, she has no idea where the attacker is and her lizard friend is dying in front of her eyes.

Genowefa looked around and noticed that the attacker is running away, a bit panicked. She didn't have time to worry about him – she had to save the lizard. She brought forth the magic and regenerated Kamila. She also touched her with mental magic to make sure this will not influence Kamila in negative ways.

In the meantime, Daria was around. She was sent here by the Silver candle command – this area has had some new rumors about lizards and with the opera being nearby, a catalyst was required to make sure everything is all right. So, Daria was here, slightly grumbling about everything.

As a catalyst, she was able to detect the magical energy used to revive Kamila. She sent a signal to Genowefa asking if everything is all right. Genowefa confirmed but said something is wrong here. When Daria asked Genowefa if she wants her here, Genowefa said that it would be best if they met at the opera. There is a root problem. Why? Genowefa wanted to wait until Kamila's masking field will return to action. Magi can kill vicinius on the spot and Kamila is a lizard – a vicinius.

Daria decided to check the opera while she was waiting for Genowefa. She used her magic to determine the state of Operiatrix. Operiatrix was not awakened, but she was not asleep either. By determining the flows of magic, Daria has managed to see that there is some kind of parasite – magical parasite - which was slurping energy from Operiatrix.

By looking deeper into the flows of energy Daria has managed to find out that this is connected somehow with draining of human. So there is a human being drained by a magical parasite and the same parasite is feeding off Operiatrix.

Then Genowefa with Kamila enter the scene. Daria can detect a touch of the same parasite on Kamila. Then, Genowefa exposes Kamila as a lizard. Kamila is unhappy. Fortunately, Daria does not intend to kill Kamila – she wants to scan her. Aided by Genowefa, both cast a spell to determine what the hell is going on here. And they have found an answer. A symbiotic mimic. Or rather, a parasitic mimic.

It seems that the mimic has split. One of them influences the girl in white. The other – or rather the other part of the same mimic in two bodies - influenced the sniper trying to kill the mayor.

Okay, so who the hell would even want to kill a mayor? The mimic will influence an extreme feeling; it will make people act what they would like to do but would never do. There needs to be a need – a want.

Genowefa suggested it could be the other lizard, the one hiding in the opera. Kamila denied forcefully – he would never do something like that. However, she admitted she has many enemies; she was elected on the platform of pragmatism and prosperity. There are many people in the city who would love for the city to return to being a beacon of culture. Not the lousy kitschy "lizard Roswell culture" but the high culture – opera, poetry... stuff.

Then, Genowefa noticed something interesting. The mayor said she isn't walking near the opera often. So how did she know when the woman in White was dancing? It was strange. Genowefa and Daria conspired via hypernet and they decided to play good cop bad cop with the lizard. And they have uncovered a terrible secret.

THE LIZARDS ARE TOGETHER AS LOVERS!

Both Kamila and Alfred ran away from the lizardfolk to be together. But on the surface they had different approaches to what to do moving forward.

Kamila decided to go into pragmatic approach – politics, paintball, money. Ways to be safe, a way to avoid detection by accumulating power in the human world.

Alfred is a hopeless romantic. He heard Operiatrix singing and he fell in love with human music (in opposite to lizard underground rap). He wanted to go into high culture, to make everything beautiful and pretty. And because his masking field is malfunctioning, he can't really go out and they can't be together with the mayor.

Okay. That explains something. Genowefa asked Kamila to bring Alfred from the opera (to Daria's dismay - she doesn't want to deal with lizard love stories). And then, to even greater dismay, the lizards asked magi to restore our threads masking field.

Seriously.

Daria didn't really want to do that. It was a lot of work and it required connecting it to an energy source (in this case - energy powering Operiatrix as much as the terrain nearby), but Genowefa convinced Daria by giving her a favor in the future. Fair enough...

And so Alfred's energy field (masking field) got restored. The lizard lovers could be together without anyone knowing what is happening.

**Dzień 2**:

Genowefa was wondering how to find the sniper and the lady in white. That is, how to find them before something bad happens to them. A bloody good idea was to use the mayor as a bait (to Kamila's dismay). However Daria had a different approach – the girl in white will come to the opera and after she does, combine spell of Genowefa and Daria will force the other mimic to appear. Technically, both are the same mimic, so it is not difficult to force the other part of the entity to appear when the first part is being controlled in the range.

To make it happen and to make sure everything will go smoothly, the sorceresses prepared. Genowefa prepared the mental force fields to make sure the mimic will not enthrall them. Daria prepared something connected to fire to make sure mimic will not run away and will easily subdued. The fire trap working against mimic.

Unfortunately, Genowefa's mental shields were slightly malfunctioning. They were perfectly adapted to humans and Magi. But lizards do not think like people. On seeing mimics, lizards sex drive would go into overdrive, so to say. Completely unaware of that fact, the sorceresses waited until Alfred will tell them that the girl in white will appear. And that has happened.

Genowefa and Daria came before the girl finished dancing. The mimic took the form of white high heels. Combined spell from Genowefa and Daria made sure that the human host will not be hurt and the mimic will be disabled. Then, another spell was cast upon mimic to lure the other part here.

In the meantime, the lizards went to make out in the opera. Fortunately for everyone, this was offscreen.

Daria has managed to dominate the mimic and Genowefa has managed to make sure no human was harmed. A complete success. To Genowefa's astonishment, Daria did not destroy the mimic – she has used her artifact control powers to take over the entity and bind it to her will. To enslave the construct.

And with this, the great symbiotic mimic crisis has been averted.

## Wpływ na świat

![Rysunek z konfliktami Opowieści](Materials/180499/180421-konflikty.png)

* Żółw: 17
* Kić: 4
* Arleta: 7

Czyli:

Gracze:

* Daria dostaje mimika symbiotycznego jako narzędzie
* Genowefa ma zaufanie jaszczurów - dostaje wejście do ich podziemnego świata
* Alfred zostaje maestro w operze - ludzie przychodzą go słuchać
* Daria dostaje reputację pomocnej w deeskalacji Operiatrix dzięki Genowefie
* Alfred, Kamila - ambasadorzy świata jaszczurów w Czapkowiku

MG:

* Część Reptiljan chcą władzy nad światem (3)
* Część Reptiljan chce izolacji i usunięcia zbiegłych jaszczurów (3)

# Streszczenie

W Czapkowiku pojawił się mimik symbiotyczny. Szczęśliwie, Genowefa i Kamila powstrzymały go, zanim stała się większa krzywda. Rosną tam napięcia między frakcją wysokiej kultury a frakcją finansowo-pragmatyczną. Udało się odkryć, że Alfred i Kamila są jaszczurczymi kochankami. Główna frakcja reptiljan zdecydowała się zrobić uciekłe jaszczury ambasadorami...

# Progresja

* Alfred Werner: Daria i Genowefa naprawiły jego ekran maskujący. Działa prawidłowo; nie będzie już sam się wyłączał. Ma ekran dość niezawodny.
* Alfred Werner: został maestro w operze Czapkowickiej. Więcej, ludzie przychodzą go słuchać. Jaszczur Of The Opera, dosłownie XD.
* Alfred Werner: ujawniło się, że jest parą z Kamilą Woreczek. Dodatkowo, został ambasadorem jaszczurów (reptiljan) na Czapkowik.
* Kamila Woreczek: ujawniło się, że jest parą z Alfredem Wernerem. Dodatkowo, została ambasadorem jaszczurów (reptiljan) na Czapkowik.
* Genowefa Huppert: tak bardzo jej ufa Alfred i Kamila, że dostała informacje jak wejść do jaszczurzego świata pod ziemią. Ma dostęp do części dziwnych jaszczurzych zasobów.
* Daria Rudas: kontroluje mimika symbiotycznego, którego może wykorzystać w dowolny sposób.

## Frakcji

* Miasto Czapkowik: pojawiają się silne napięcia między frakcją pragmatyczną (pieniądze, paintball, kicz) a kulturalną (opera, poezja, sztuka)

# Zasługi

* mag: Genowefa Huppert, jedyny mag któremu ufa Kamila; wyleczyła umierającą Kamilę, usunęła problem z mimikiem i naprawiła ekran Alfreda. Też: doszła do love story!
* mag: Daria Rudas, miała tylko spojrzeć na Operiatrix, a naprawiła ekran Alfreda i złapała mimika dla siebie. Pragmatyczny szczur.
* vic: Alfred Werner, główny lookout dla magów wyszukujący Kasię (dziewczynę w bieli tańczącą pod operą). Bezużyteczny. Silnie we frakcji kulturalnej.
* vic: Kamila Woreczek, osoba dbająca o Czapkowik i komunikująca się z jedynym magiem, któremu ufa - z Genowefą. Zastrzelona i zregenerowana. Silnie we frakcji pragmatycznej.
* czł: Kasia Krabowska, dotknięta przez Mimika Pasożytniczego; tańcząca dziewczyna w bieli. Mimik wzmocnił jej miłość do tańca i spróbowania zrobić "coś więcej".
* czł: Artur Wiążczak, dotknięty przez Mimika Pasożytniczego; strzelec chcący zastrzelić burmistrz. Z natury łagodny miłośnik kultury wyższej ale też paintballa.

# Plany

## Frakcji

* Miasto Czapkowik: echo zaklęcia Darii i Genowefy dotknęło źródła energii i wyprodukowało hipnokarabiny - efemerydę, która pragnie rosnąć.
* Miasto Czapkowik Reptiljanie: część z Czapkowikich Reptiljan chce władzy nad światem (jak to reptiljanie)
* Miasto Czapkowik Reptiljanie: część z Czapkowikich Reptiljan chce izolacji i usunięcia zbiegłych jaszczurów (Alfreda i Kamili)

# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Okólno-Trocin
                1. Czapkowik, pod którym FAKTYCZNIE mieszkają jaszczury. Niewielka kolonia. I który zacznie być znany z jaszczurzego kiczu.
                    1. Stara Opera, gdzie nadmiernie fraternizowali się Alfred z Kamilą po nieudanym ekranie mentalnym; też: tam tańczyła dziewczyna w bieli.
                    1. Poligon Klubu Snajper, miejsce bezpiecznych spotkań burmistrz i Genowefy. Niestety, burmistrz została tam postrzelona.

# Czas

* Opóźnienie: 4
* Dni: 2

# Narzędzia MG

## Cel Opowieści

* null, sesja z zaskoczenia

## Po czym poznam sukces

* null

## Wynik z perspektywy celu

* null

## Wykorzystana mechanika

Mechanika 1804

Wpływ, klasycznie:

* 8 kart balans między graczami; 8-10 kart to zwykle dzień
* każda porażka = +2 pkt wpływu dla gracza
* każdy skonfliktowany sukces = +1 pkt wpływu dla gracza
* każda karta = +1 wpływ dla MG
* każde 5 kart zaokr. w górę: +1 pkt wpływu dla gracza

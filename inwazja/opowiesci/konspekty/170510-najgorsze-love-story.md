---
layout: inwazja-konspekt
title:  "Najgorsze love story"
campaign: rezydentka-krukowa
players: kić
gm: żółw
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [170525 - New, better Senesgrad (Dru,Jojo,T(temp*3))](170525-new-better-senesgrad.html)

### Chronologiczna

* [170525 - New, better Senesgrad (Dru,Jojo,T(temp*3))](170525-new-better-senesgrad.html)

## Kontekst ogólny sytuacji

Wyjdzie z misji

## Punkt zerowy:

Ż: Dlaczego Paulina jechała z wizytą domową do niedalekiej wsi?
K: Bo pacjent nie miał jak dojechać. Starszy człowiek, mieszka sam.
Ż: Trzy szkoły magiczne, które zostały użyte w zupełnie innym miejscu (w Wiązie)?
K: Golemancja, Magia Imprezowa, Magia Rolnicza
Ż: Dwie zupełnie inne szkoły magiczne?
K: Technomancja, Magia Iluzji

## Misja właściwa:

**Dzień 1:**

Paulina odwiedziła swojego pacjenta w Głębiwiązie. Nazywa się Konrad Paśnikowiec i jest starszym człowiekiem, który mieszka tu od dłuższego czasu. Nic strasznie poważnego, ale jest nieco unieruchomiony. Swego czasu pracował na budowie, ale to już nie ten wiek.

Paulina dostarczyła mu leki i go zbadała. Porozmawiała chwilę, posłuchała co ma do powiedzenia... siedząc tu pewien czas Paulina wyczuła coś magicznego. Coś we wsi. To nie jest zaklęcie, to coś... innego. Skończyła wizytę i zdecydowała się przejechać po wsi.

Nie trwało długo, aż zauważyła coś nietypowego. Na jednym z pól uprawiana jest kukurydza. Coś w tej kukurydzy, gdzieś "w środku" pola, ma echo magiczne. Echo Skażenia. Paulina spróbowała oszacować kiedy doszło do Skażenia; wyszło jej, że jakieś dwa dni temu. A Skażenie nie ustąpiło. Czyli najpewniej nie ustąpi, bo jest tam COŚ Skażonego. Najpewniej jakaś grupka roślin uległa Skażeniu.

Paulina zdecydowanie NIE chce wchodzić w pole, by nie zostać pogonioną z kosą czy czymś tego typu. Jej wzrok padł na drzewo; niestety, nie czuła się zbyt pewnie by się na owo drzewo wspinać. Wzruszyła ramionami i skupiła się na zaklęciu. Skupiła się na jednej z gałęzi drzewa i spróbowała nałożyć _relay_ magiczny, by móc adresować bezpośrednio źródło Skażenia. 

(2, W+, 17-> Skażenie, efekt 4). Paulina zorientowała się, że jej czar zadziałał trochę inaczej. Poza byciem relayem ten konkretny relay stał się też uziemiaczem - ściąga magię z otoczenia. (przyquarkowane, -1 surowiec). Jak skończy się magia temu obiektowi, zacznie ściągać magię z siebie i samo zgaśnie.

Paulina nawet nie musi rzucać czaru na kukurydzę. Zostawiła sygnaturę maga rezydenta, że gdyby ktokolwiek się tym zainteresował, kontaktuj się z Pauliną. I namiar hipernetowy plus numer telefonu.

Po czym w poczuciu dobrze spełnionego obowiązku Paulina pojechała do domu.

**Dzień 3:**

Piąta rano. Paulina dostała alarmową wiadomość hipernetową. Już nie śpi ;-).

Skontaktowała się z Pauliną młoda czarodziejka pytając rozpaczliwie co Paulina zrobiła najlepszego i dlaczego ożywiła DRZEWO. Paulina zauważyła, że nic takiego nie zrobiła; Diana (bo tak się przedstawiła) powiedziała, że jej kolega rzucił rytuał i energia została przekierowana... gdzieś. I ożywiło DRZEWO. I drzewo zainteresowało się kukurydzą. Diana spróbuje jakoś odwrócić jego uwagę i ściągnąć go do wiązu... sam kolega jest w bardzo złej formie. Jeśli Paulina jest katalistką, niech pomoże!

Paulina, załamana i zirytowana ("Nic tylko czekać aż magowie coś spieprzą") zdecydowała się pojechać... i zabrała ze sobą roundup.

Tymczasem Dianie udało się odciągnąć uwagę drzewa, rzucając kilka pomniejszych czarów iluzji. Wszystkie zaklęcia trafiają prosto do relaya a Skażeniec zdecydował się iść za źródłem SMACZNOŚCI, coraz potężniejszy energetycznie (czego Diana oczywiście nie zauważyła).

Paulina dotarła do miejsca, gdzie było drzewo. Wyczuwa energię z ładnego rytuału, takiego z przygotowaniem. Wyczuwa też energię z nieefektywnych zaklęć Diany - ścieżka iluzji. Diana rzuca iluzje... kukurydzy. Myśli, że to jest powód, dla którego drzewo za nią podąża. W rzeczywistości Diana karmi to drzewo. Paulina facepalmowała. Kazała Dianie sprowadzić drzewo tam, skąd przyszło. Paulina Ma Plan.

Diana upozycjonowała drzewo na swoim miejscu. Na moment. Paulina skupiła się na SWOIM RELAYU. Swoim własnym relayu. Zdecydowała się odwrócić ciąg; niech ten relay wyssie energię z... drzewa i odwróci ciąg. Paulina chce przepuścić ciąg cholernego relaya na siebie i zacząć zdobywać Quarki.

TOR: 0/5

Paulina potrzebuje podniesionego wpływu. Zdecydowała się użyć korzenia drzewa. Zawahała się przy roundupie; 

Postać (strona gracza):

| .I_Strat. | .I_Takt. | .Specka. | .Umiejka. | .Specka Mag. | .Umiejka Mag. | .Inklinacja. | .POSTAĆ. |
|     1     |     0    |    0     |     0     |       1      |       1       |       0      |     3    |

Okoliczności:

|  .Sytuacja. | .Sprzęt. | .Skala. | .Magia. | .Surowiec. |  .OKOLICZNOŚCI. |
|     1       |    2     |   -1    |    0    |     1      |         3       |

Przeciwnik (strona NPC):

| .VERSUS. | .Diff. | .Wpływ akcji gracza. | .OPOZYCJA. |
|    2     |    2   |          2           |      6     |

Wynik:

| .Postać. | .Opozycja. | .Test?. | .Rzut.       | .Wynik. |
|    6     |      6     |   0     | +1 W0 (15)   | SUKCES  |

TOR: 3/5

Drzewo zadrżało w posadach gdy Paulina zaczęła je wyżerać i generować Quarki. W pewnym momencie Paulina poczuła wpływ drzewa na coś innego - drzewo żywi się energią maga. Tamtego maga z samochodu! On jest podlinkowany do relaya przez rytuał...

Paulina stwierdziła, że musi zniszczyć relay... wpierw skupiła się jednak na diagnostyce; musi odpiąć tamtego maga od relaya! Diagnostyczne zaklęcie (17). Paulinie udało się tym zaklęciem jednocześnie odpiąć maga jak i zniszczyć Relay... ALE część energii poszła prosto w wiąz. Ponownie. Mniejsza część, ale poszła. Drzewo jest... na miejscu. 

Diana zaczęła Paulinie entuzjastycznie dziękować, nie wiedząc, że część energii uderzyła w Wiąz Występku. Paulina zauważyła, że Diana jest ogólnie dość słabym magiem; powinna być w stanie normalnie zobaczyć, że część energii poszła do wiązu... Paulina wyjaśniła spokojnie Dianie, że część energii poszła w wiąz a jej kolega był sprzężony. Muszą iść do wiązu by skończyć to wszystko, na pewno doszło tam do Skażenia...

Paulina z Dianą szybko poszły do Wiązu Występku. Zbliżając się do Wiązu usłyszały "STILL LOVING YOU" Scorpions. Bardzo głośne Still Loving You. Dochodzi z wiązu.

Dziewczyny spojrzały po sobie. Nie ma wpływu magicznego ani mentalnego.

Paulina spytała Dianę, czy jej kolega jest na hipernecie. Ta odpowiedziała, że tak. Ale Diana nie ma z nim aktualnie kontaktu; on nie odpowiada...

Paulina podchodzi w miarę ostrożnie. Znalazła tam... boomboxa (starego, elektrośmieci) który ożył i lata po wiązie. Paulina kazała Dianie to zapędzić do jakiejś dziupli. Diana cała zaskoczona, bo głos dochodzący z boomboxa to głos jej kolegi..?

Paulina kazała jej to złapać i zapędzić. Diana pospiesznie popatrzyła po innych elektrośmieciach i zaczęła składać kolejne zaklęcia - budować "hunter-killery" mające złapać i zapędzić tamtego boomboxa w pole widzenia Pauliny ;-). Ożywiła kilka sprzętów, dostosowała je do działania w warunkach leśnych i wysłała za boomboxem.

Pół godziny później boombox znalazł się w polu widzenia Pauliny, zapędzany przez inne elektrośmieci. A Paulina zdecydowała się rozłożyć to na Quarki. Ma to gdzieś.

Paulina rzuciła zaklęcie na boomboxa. Widząc, że nie ma dość energii zanim ten zniknie, zaczerpnęła energię z Diany. Ta pisnęła z bólu i szoku, ale Paulina na tym etapie to ignoruje (reroll). Następnie wykorzystała surowiec; trochę kryształów (reroll). Po czym - boombox został pożarty i rozpadł się w elektrośmieci, wraz z zaklętymi śmieciami Diany. Boombox jeszcze przed "śmiercią" zdołał krzyknąć jedną rzecz: "KOCHAM CIĘ! NIE PRÓBUJĘ WKRĘCIĆ SIĘ W ŁASKI TWOJEJ PANI! PODRYWAM CIĘ TAK NIEUDOLNIE!"

Diana zrobiła najgłupszą minę, jaką Paulina kiedykolwiek widziała na czarodziejce.

* To gdzie jest Twój kolega..? - Paulina, zmęczonym głosem
* Eeee... w samochodzie. Na tylnich siedzeniach... - speszona Diana

Hektor jest w głębokim szoku energetycznym po utracie ogromnej ilości energii magicznej. Jest cały wychłodzony i drży. Stracił za dużo energii. Paulina kazała Dianie podkręcić ogrzewanie na maksa, przykryła Konrada kocem i kazała Dianie jechać za nią. I pojechała do domu, gdzie ma sprzęt...

We dwie wywlokły Konrada z samochodu na kanapę. Paulina zabrała się za niemagiczne badanie nieszczęśnika. (6v4->F,S). Po drodze do kanapy z samochodu on i Diana się solidnie utytłali błotem; Paulina ma sportową odzież, więc ma to gdzieś. Badanie wykazało, że jest na bardzo niskim poziomie magicznym. Dwa tygodnie i się zregeneruje, byle był w cieple.

Paulina ma inne plany. Część z wyssanych quarków wpuściła z powrotem w niego, jako magiczny lekarz i katalista jednocześnie. Na tyle by odzyskał przytomność i mógł jakoś funkcjonować. (7v5->S). Około 11 rano kuracja została zakończona.

Paulina spojrzała sceptycznie na Dianę, która zdążyła zasnąć na kanapie. Paulina ją lekko wybudziła, by zdjąć z niej brudne ciuchy i nakryć ją jakimś kocem.

* Eee... zdrzemnęłam się? - Diana, nieco bełkotliwie
* Możesz się zdrzemnąć dalej, tylko daj brudne ciuchy - Paulina
* Która godzina? - Diana, z lekkim stresem
* Jedenasta - Paulina
* NIE! - Diana, przerażona - Ona mnie zabije!
* Kto? - zaskoczona Paulina
* Moja pani... - załamana Diana
* A ty piesek? - Paulina, z niedowierzaniem
* Prawie... u Bankierzówny... - Diana, ogarnięta rozpaczą i przerażeniem - Zwolni mnie...
* Opowiedz od początku... - Paulina, z ciężkim westchnięciem. Nie wie, czy chce tego słuchać...

Z opowieści Diany wynikało, że niejaka Lady Ewelina Bankierz jest wyjątkowo paskudną kobietą. Arogancka, żąda służkę do umycia plecków czy przyniesienia ręcznika. A Diana wpakowała się w jakieś długi, musi jakoś spłacić... a Diana nie ma do końca jak zarobić, bo jest kiepską czarodziejką... 

Paulina pozwoliła jej użyć swojej pralki i dobrać jakieś czyste ciuchy Pauliny, sama skrobnęła podziękowanie od maga rezydenta do lady Eweliny Bankierz za użyczenie jej służki i kazała Dianie wrócić najszybciej jak ta może. Diana bardzo podziękowała i czmychnęła, zerkając na Konrada, ale bojąc się Paulinę o coś zapytać.

Hektor się obudził i spytał, gdzie jest. Paulina podała mu kubek z piciem, kazała mu spać i powiedziała, że jest lekarzem. Hektor, jeszcze nie do końca przytomny, spytał Paulinę czy jest doktorem medycyny czy jakimś... po czym próbował się wycofać. Paulina powiedziała, że oboma i kazała mu spać. Hektor zasnął.

Koło 17 Hektor się obudził ponownie, w lepszej formie. Paulina przyjmowała pacjentów normalnie.

W końcu Paulina i Hektor mogli porozmawiać. Paulina spytała go o co w ogóle tam chodziło? Co on chciał zrobić za rytuał? Hektor wyjaśnił, że wie, że Diana służy na jakimś dworze i tam są bale różnego rodzaju. Odnalazł i dopracował rytuał mający za zadanie korzystać z energii osób bawiących się na balu i na bazie tej energii ma zbudować golema rolniczego, by ten mógł pracować aż padnie. W wizji Hektora, to kupi mu miejsce u tamtych arystokratów.

Paulina facepalmowała. Jakkolwiek pomysł jest dobry, ŻADEN arystokrata nie zgodzi się na coś takiego; Hektor wyraźnie nie rozumie jak myślą zdecydowanie zbyt magowie...

W krótkiej rozmowie wyszło, że Hektor jest niezależnym magiem; jest młodym nomadem, interesuje się pomaganiem innym i magią rolniczą, nie jest szczególnie dobrze wyszkolony i wyuczony, ale nie boi się pracować i jest dość praworządny. Ma pozytywny stosunek zarówno do ludzi jak i do Maskarady. W sumie, do wszystkiego.

Paulina dała mu te quarki które wydobyła z rytuału i pojechała z nim, by zamaskować ślady po kukurydzy. Jeśli jest magiem rolniczym, powinno się dać squarkować to wszystko w miarę sensownie. I faktycznie, udało się.

Hektor i Diana są zbyt nieszkoleni, by móc zorientować się komunikatem Pauliny. Hektor próbował czyścić Skażenie po swoim rytuale, ale nie znalazł wszystkiego. Dlatego Paulina znalazła to poletko kukurydzy...

Hektor NADAL nie wie, że boombox powiedział, że on jest zainteresowany Dianą. Wyraźnie się lekko rumieni w jej pobliżu, więc Paulina ma pewne podejrzenia, że tak jest ;-).

# Progresja



# Streszczenie

Zwyczajny dzień pracy Pauliny odwiedzającej pacjenta w domu skończył się znalezieniem Skażonego poletka kukurydzy. Paulina postawiła drenujący relay... a trzy dni później skontaktowała się z nią o 5 rano spanikowana czarodziejka, że powstał drzewiec i ona potrzebuje pomocy by go zatrzymać. Okazało się, że czar Pauliny wszedł w interakcję z rytuałem stworzonym przez młodego maga... drzewiec pokonany, magiem się zajęli. A Diana (czarodziejka) spóźniła się na śniadanie swojej pani.

# Zasługi

* mag: Paulina Tarczyńska, która próbowała rozładować Skażone poletko kukurydzy a wplątała się w walkę z dziwną drzewną efemerydą. I zaopiekowała się dwójką młodych magów...
* mag: Diana Łuczkiewicz, dość słaba czarodziejka, służka Eweliny Bankierz. Technomantka i czarodziejka iluzji. Skontaktowała się z Pauliną gdy rytuał jej przyjaciela stworzył potwornego drzewca i pomogła Paulinie jak mogła naprawić sytuację. Spóźniła się na śniadanie swojej pani.
* mag: Hektor Reszniaczek, mag rolniczy, imprezowy i golemanta. Podoba mu się Diana. Chciał ją poderwać dość zaawansowanym i Skażonym rytuałem; oczywiście, przez kolizję z czarem Pauliny skończył praktycznie w komie i Diana z Pauliną musiały go ratować...
* czł: Konrad Paśnikowiec, starszy, samotny człowiek mieszkający w Głębiwiązie; pacjent Pauliny, która go czasami odwiedza.
* mag: Ewelina Bankierz, podobno całkiem wredna zołza, która bierze sobie na służki słabsze czarodziejki i steruje ich całym życiem.

# Lokalizacje

1. Świat
    1. Primus
        1. Mazowsze
            1. Powiat Pustulski
                1. Krukowo Czarne
                    1. Gabinet Pauliny, gdzie Paulina przyjmuje pacjentów i zajmuje się nieszczęsnym magiem Hektorem.
                1. Głębiwiąz
                    1. Domki na wzgórzu, gdzie mieszka min. Konrad, starszy już pacjent Pauliny
                    1. Pola, gdzie znajdowało się Skażone poletko kukurydzy oraz gdzie ożywione zostało drzewo.
                    1. Występny Wiąz, gdzie Hektor przygotowuje swoje rytuały by zaimponować Dianie.

# Czas

* Dni: 3

# Narzędzia MG

## Cel misji

1. Misja weryfikująca działanie 'Efekt Skażenia jeśli czar ma rzut 16+'
2. Misja sprawdzająca działanie 'magicznych umiejętności' i 'magicznych specjalizacji'

## Po czym poznam sukces

1. Efekty Skażenia będą miały następujące cechy:
    1. Będą wyraźnie zmieniały fabułę
    1. Nie będą sprawiać, że magowie boją się czarować, ale... wiąże się z magią pewien koszt
2. Magiczne umiejętności będą dobrze się synergizowały z resztą postaci; nie ma pełnego pokrycia zbioru między postacią magicznie i niemagicznie
3. Magiczne specjalizacje będą wystarczająco interesujące, by ich nie łączyć ze zwykłymi specjalizacjami

## Wynik z perspektywy celu

1. Efekty Skażenia:
    1. Ogromny sukces. Trzeba dodać "komplikację fabularną" jako opcję NIE bycia Skażenia. Cała ta misja jest wynikiem Efektów Skażenia ;-).
2. Magiczne umiejętności zadziałały poprawnie. Nie ma 100% synergii, acz czasem synergia występuje.
3. Magiczne specjalizacje nie mają ŻADNEJ synergii ze zwykłymi. Można połączyć te dwie kategorie w jedną i zachować +1 za magię?

## Wykorzystane tabelki

Postać (strona gracza):

| .I_Strat. | .I_Takt. | .Specka. | .Umiejka. | .Specka Mag. | .Umiejka Mag. | .Inklinacja. | .POSTAĆ. |
|           |          |          |           |              |               |              |          |

Okoliczności:

|  .Sytuacja. | .Sprzęt. | .Skala. | .Magia. | .Surowiec. |  .OKOLICZNOŚCI. |
|             |          |         |         |            |                 |

Przeciwnik (strona NPC):

| .VERSUS. | .Diff. | .Wpływ akcji gracza. | .OPOZYCJA. |
|          |        |                      |            |

Wynik:

| .Postać. | .Opozycja. | .Test?. | .Rzut.       | .Wynik. |
|          |            |         | +x Wx (xx)   |  x WX   |

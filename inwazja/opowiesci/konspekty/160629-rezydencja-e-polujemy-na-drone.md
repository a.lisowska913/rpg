---
layout: inwazja-konspekt
title:  "Rezydencja? E, polujemy na dronę!"
campaign: powrot-karradraela
gm: żółw
players: kić, dust
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [161101 - Bezwzględna Lady Terminus (AW)](161101-bezwzgledna-lady-terminus.html)

### Chronologiczna

* [160615 - Morderczyni w masce Wandy (HB, KB)](160615-morderczyni-w-masce-wandy.hml)

## Kontekst ogólny sytuacji

- Ozydiusz, Aleksander Sowiński, Sabina Sowińska nie żyją. Świeca jest pozbawiona głowy w Kopalinie. Zaczyna się frakcjonalizacja.
- Emilia rzuciła bombę - magitech Wiktora ma przydatną wiedzę, klucz do zniszczenia Szlachty. Pancerz magitechowy Tamary i "kołysanka". Powiązanie ze Spustoszeniem?
- Emilia dowodzi siłami Kurtyny.
- Aneta Rainer przejęła dowodzenie nad siłami Lojalistów.
- Wiktor skonsolidował siły Szlachty w Kopalinie.
- Hipernet funkcjonuje z problemami. Mechaniczni terminusi wykonują ostatni rozkaz Ozydiusza.
- Ciągła plaga Irytki Sprzężonej. Mechaniczni terminusi i viciniusy trzymają porządek. Anioły Nadzorcze się dezaktywowały.
- Amanda Diakon zmusiła Mirabelkę Diakon do współpracy ze sobą.
- Rodzina Świecy vs Mausowie wchodzą w kolejną fazę eskalacji. Elea i Rufus uciekają się do drastycznych działań; seiras Maus nie pomaga. Bankierze vs Mausowie.
- Baltazar Maus będzie poświęcony na ołtarzu Rodziny Świecy i bezpieczeństwa (nie w tej misji). Hektor pomoże z Baltazarem i Malią.
- Milena Diakon widziała Spustoszenie (?). Skąd i jak?
- Amanda Diakon / Pasożyt (Millennium) powoli wchodzą na teren Kopalina pod pozorem sił Mirabelki. Jej cel to Ozydiusz i CruentusReverto oraz Milena.
- Diakoni zablokowani przez Mojrę; silny sceptycyzm co do działań Blakenbauerów (thanks, Romeo). Akcja z Wandą Ketran nie pomaga; Estrella nie wybaczy.
- Oktawian, Adela Mausowie i Dominik Bankierz zniknęli lub zginęli na wykopaliskach.
- Blakenbauerowie, jakimś cudem, są w sojuszu ze wszystkimi siłami.
- Świeca Daemonica jest odcięta od Fazy Materialnej przez bombę pryzmatyczną.

- Ktoś napuszcza na siebie Świecę i KADEM, Kurtynę i Szlachtę, Millennium i Świecę.
- Milena Diakon widziała Spustoszenie (?). Skąd i jak?
- Baltazar Maus będzie poświęcony na ołtarzu Rodziny Świecy i bezpieczeństwa (nie w tej misji).
- Pancerz Tamary i "kołysanka". Powiązanie ze Spustoszeniem?
- Adela Maus - zniknęła, Oktawian Maus - koma "od miecza", Dominik Bankierz nie żyje.
- The Governess w masce Wandy Ketran się pojawiła. Zmusiła Blakenbauerów do wycofania się z działań w Kopalinie mordując wszystko co oni kochają.

## Punkt zerowy:

The Governess has managed to actually stop the Blakenbauer bloodline. Otton has decided, that it is too dangerous to assist any of the major forces because of the risks inherent to the bloodline. Edwin, Hektor and others were protesting his decisions, but Otton was relentless.

So instead of focusing the power of the bloodline outwards, the Blakenbauer bloodline focused inwards. Tymotheus started to work on the new Mansion and under the threat of the Governess, Otton decided to help him. He sent Klara and augmented humans: Alina and Dionizy. Tymotheus has secured the monetary means and construction required to form new form of headquarters.

To make sure the local magi will not be able to oppose Tymotheus, Otton decided to help him sending Margaret.

Ż: Jedna rzadko spotykana acz pożyteczna szkoła magiczna?
D: Adaptacja nie-ludzi do życia w społeczeństwie ludzkim.
Ż: System defensywny na granicy z Upadłą Małopolską?
K: Sieć mini-riftów. Działają jak miny i rifty i system ostrzegania... ogólnie coś z czasów świetności Świecy.

Trocin słynie z fabryki mebli, obróbki drewna i magicznego drewna.

## Misja właściwa:

Status początkowy:
- koszty Blakenbauerów: 0/3
- wiedza o Blakenbauerach: 0/3
- inwazja Blutwursta: 0/3
- kontrola Świecy nad terenem: 0/5
- ekonomia świata magów: 0/3
- populacja ludzka: 0/4

Dzień 1:

Ciężarówka. Lapisowana ciężarówka. A w niej: Klara, Dionizy, Alina, Margaret, kilka płaszczek. I briefing - od Mojry.

Trocin jest miastem kontrolowanym przez Świecę. Jest w stanie oblężenia (dane z hipernetu). Stare systemy riftów zostały reaktywowane przez magów Świecy. Te problemy jakie spotkały Kopalin dzieją się wszędzie, ale inaczej; próg chaosu rośnie. Tutaj chaos objawiony jest przez magów (defilerów? wampirów?) rodu Blutwurst. A dokładniej - von Blutwurst. Mojra nigdy wcześniej nie słyszała o tym rodzie; nie było ich przed Inwazją. W Trocinie - jako bazie na okolicę - operuje około 27 magów Świecy. Większość magów zaczęła się wycofywać. 

Cel jest wieloetapowy - chcemy ustabilizować tu placówkę i to tak, by nikt o tym nie wiedział. Docelowo chcemy przejąć ekonomię i Świecę na tym obszarze. Jak długo nie pojawiają się żadne problemy, Świeca nie wyśle wsparcia. Blutwurstowie chcą przejąć ten teren. Czemu? Nikt nie wie. Nie ma to sensu.

Drewno z Trocina pomoże w odbudowie i wzmocnieniu Rezydencji. Dodatkowo, umożliwi to zbudowanie jeszcze jednej Rezydencji, w tym obszarze. Jest tu mag Świecy - tien Aurel Czarko. Jeśli on kiedykolwiek zostałby przywódcą Świecy, nigdy żaden negatywny raport nie poleci. A Świeca ma dużo mniej sił niż się wydaje.

Poza magami ze Świecy (27) jest tu koło 40 innych magów. Są mniej zrzeszeni, lub inaczej zrzeszeni. Świeca nie zgodzi się na utratę tego terenu do jakiejkolwiek innej siły. A Margaret ma przy sobie mały węzełek z Irytką.

W tym obszarze jest dużo wyższe stężenie viciniusów kojarzących się z drzewami i lasami.

Zenobia Bankierz - dowódca cywilny, trzyma ekonomię w kupie, świetny dyplomata. Ściśle współpracuje z tien Szczepanem Sowińskim. I ona jest gwarantem dla wielu magów dookoła. Zenobia jest (niezbyt) lubianą gubernatorką okolicy, ale trzyma ją ściśle w kupie i jakkolwiek większość narzeka, nikomu to tak naprawdę nie przeszkadza; ona pilnuje bilans interesów. Ściśle współpracuje z terminusami.

Może zrobić krucjatę przeciw Blutwurstom? Zdaniem Mojry, ktokolwiek stoi za Governess czy Irytką, jednocześnie stoi za Blutwurstami.
Na prośbę Zespołu, Mojra przekazała im kody kontrolne do jednej z placówek terminuskich poza Trocinem; ta placówka to 'lookout'. Lekko uzbrojony, jego funkcją jest przeszukiwanie i identyfikacja anomalii. Dzięki kodom Mojry nie będą w ogóle wykrywani przez tą placówkę. 

Plan Mojry: jedna grupa uderza w zapasowy i dobrze ukryty generator zapasowy energii do riftów. To powinno zmusić Świecę do błyskawicznej reakcji. W tym czasie druga grupa uderza w "bezpieczny" lookout i hakuje komputery Świecy. Na stan wiedzy Mojry, ci konkretni info- i technomanci, którzy projektowali ten system zostali przeniesieni. Są w Trocinie inni, ale nie ci sami. Dionizy zaproponował dodatkowy ruch - postawić proxy z jeszcze innej 'lookout tower'. W ten sposób kupują sobie więcej czasu.

Pierwszym krokiem jest opanowanie pierwszego lookouta. Takie obserwatorium, wieża strażnicza.
Z uwagi na to, że Trocin dostarcza meble i drewno, jest tu pełno ciężarówek. Są też większe parkingi z motelami dla kierowców. Przemalowali ciężarówkę na taką z "sensownej" firmy (Meble 'CMYK') i zostawili ją na parkingu.

Pierwszy krok - wejście na lookout. Dionizy poszedł przodem i podał kody, by placówka go nie wykryła. Pilnując i szpiegując teren, zauważył dodatkowe kamery (nie powiązane z bezpieczeństwem bazy, ale należące do Świecy i bazy) i coś dziwnego - dronę, nie transmitującą, która nagrywa i nie pochodzi ze Świecy. Dobrze ukryła się w lookout tower. 

Mojra: "Aleksander Dziurząb, technomanta, lekko paranoiczny. Nie jest infomantą. Poszerzył rzeczy niezgodnie z zasadami - dodał dodatkowe kamery. On je podpiął do komputerów i systemów. Dokonał nieautoryzowanej autoryzacji systemu; coś mogłoby lekko pójść nie tak..."

Klara dokonała interesującego odkrycia - te drony, tutejsze, to te same (acz zmodyfikowane) drony z którymi Klara miała do czynienia w Kopalinie. One pochodzą z tej samej 'konstrukcji'. Czyli są delikatne, dość autonomiczne i ogólnie świetna robota. Klara chce jedną. Albo wszystkie.
A Alina miała olśnienie. Wanda Ketran swego czasu używała drony podobnej konstrukcji - ale nie podobnej do tej, jaka jest tu, a tej jaka była w Kopalinie. Wtedy, gdy po raz pierwszy Wanda została porwana. To nazywało się wtedy 'Czarny Błysk', nowa, prototypowa drona.

Dionizy zapolował na dronę. Klara przygotowała mu torbę Faradaya i Dionizy zaszedł dronę, po czym ją zgarnął. Drona nawet nie zorientowała się co się stało. Po czym zaniósł dronę do ciężarówki, do lapisowanego miejsca. A tam czekała Klara...
Klara otworzyła dronę. Przy stworzeniu tej drony była użyta magia. Drona nie transmituje. W ogóle. Miała kilka systemów "nerwowych" i komunikacyjnych; jeden z nich się zniszczył w chwili, w której Dionizy porwał dronę. Drona nadal ma AI, i system kontrolny wychodzący z AI.

AI także jest cudem techniki, a dokładniej AI-mancji (infomancję). Posiada osobowość, emocje, instynkt przetrwania, paranoję... nie jest to ludzki umysł, ale jest to umysł. Bo to jest cyborg - stworzyony z ptaka. Ptak został zabity i dostosowany, by być droną. Praca technomanty i lifeshapera. Jest to dzieło sztuki, dzieło mistrza. Osoba robiąca to kocha detale.

Dzięki analizie banków pamięci, odkryli skąd drona przyleciała i jakie ma instrukcje - ma pilnować i obserwować; jeśli się zbyt dużo magów Świecy pojawi, ma się wycofać na teren Małopolski, do aktywnej fabryki samochodów. Ogólnie drona nie jest szczególnie groźna.

Dzień 2:

Następny dzień Klara i Margaret poświęciły na tworzenie wirusa. Wirusa jednocześnie cybernetycznego (komputerowego) jak i biologicznego. Byt, który dostosowany jest do tej drony. A Dionizy i Alina pilnują lookoutu. Nikt tam nie idzie.

Dzień 3:

Wirus jest gotowy. Bio: 24, tech: 23. 27 vs 27. Sukces. Dionizy się zakradł i strzelił snajperką by zarazić dronę.
...z powodzeniem.

Ku szokowi Klary, emanacja wskazała na obecność Spustoszenia w dronie (wirus przejął dronę po czym Spustoszenie zdominowało i wypaliło się by ukryć). To tłumaczy, czemu drona nie ma specjalnie kanałów komunikacyjnych -jest elementem roju Spustoszenia.

Ale to znaczy, że Spustoszenie jest w wielu miejscach. I obserwuje. I najpewniej Spustoszenie stoi za wzmacnianiem różnych sił prowadzących do chaosu...
I to już nie jest fajne.

Grunt, że Spustoszenie nie wie o tym, że Blakenbauerowie są tymi, kto wykrył co się tak naprawdę stało...

# Streszczenie

Tymotheus Blakenbauer chciał założyć drugą Rezydencję w Trocinie; Mojra chciała pomóc mu zdestabilizować to miejsce by to się udało. Blakenbauerowie odkryli, że teren monitorowany jest przez Spustoszone drony. Lokalne siły Świecy mocno trzymają Trocin, acz są atakowane przez "wampirzy ród Blutwurst" z obszarów utraconych - coś bazopodobnego znajduje się w Okólniczu. Zdaniem Mojry, ktokolwiek stoi za Governess czy Irytką, jednocześnie stoi za Blutwurstami. Nikt nie wie, że Blakenbauerowie to wykryli (Agrest dostał info od Mojry), Blakenbauerowie anulowali operację.

# Zasługi

* mag: Klara Blakenbauer, która z Margaret stworzyła cyber-organicznego wirusa i ujawniła Spustoszenie u dron. Też: zajmowała się badaniem dron ;-).
* vic: Alina Bednarz, która przypomniała sobie, że podobne drony do tej Spustoszonej widziała u Wandy Ketran.
* vic: Dionizy Kret, który wpierw wypatrzył kamery, a potem upolował Spustoszoną dronę dla Klary do analizy.
* mag: Mojra, 'kret' w szeregach Świecy i główny strateg natarcia Blakenbauerów na Trocin; dostarczyła kody kontrolne i informacje geopolityczne.
* mag: Margaret Blakenbauer, stworzyła organiczną część wirusa, dzięki któremu dowiedzieli się o Spustoszonych dronach.
* mag: Zenobia Bankierz, ceniona gubernator Trocina Świecy rządząca sprawiedliwie, acz uczciwie. Ściśle współpracuje ze Szczepanem Sowińskim i jego terminusami.
* mag: Szczepan Sowiński, wielokrotnie odznaczany lord terminus Trocina (seriously...) ściśle współpracujący z władzami cywilnymi (znaczy: Zenobią Bankierz).
* mag: Aurel Czarko, niekompetentny terminus o ambicjach zdecydowanie przekraczających jego umiejętności. Wg planu Mojry - ma zostać dowódcą.
* mag: Aleksander Dziurząb, lekko paranoiczny acz kompetentny technomanta, który zamontował kamery w okolicy całkowicie niezgodnie z planami.

# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Okólno-Trocin
                1. Trocin
                    1. Obrzeża
                        1. Parking z motelem 'Tarcica'
                        1. Generator zapasowy Mikro-riftów
                    1. Żywy Las
                        1. Placówka Strażnicza 1.3
                        1. Placówka Strażnicza 1.5
        1. Małopolska
            1. Powiat Okólny
                1. Okólnicz
                    1. Fabryka samochodów 

# Czas

* Dni: 3
---
layout: inwazja-konspekt
title:  "Złodzieje nieważnych artefaktów"
campaign: adaptacja-kralotyczna
gm: żółw
players: kić, raynor
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [180402 - Pętla dookoła niekralotha](180402-petla-dookola-niekralotha.html)

### Chronologiczna

* [180327 - Szept Mare Felix](180327-szept-mare-felix.html)

## Kontekst ogólny sytuacji
### Opis sytuacji
## Punkt zerowy:

[!Rysunek inicjujący sesję](Materials/180499/180411_init.png)

* Ż: Dlaczego zniknięcie nieważnego artefaktu zagroziło Darii? Skąd Maurycy o tym wie?
* K: Podobny był do tego, co ona kiedyś próbowała zrobić - skojarzenie oczywiste. A Maurycy wie dzięki Kropiaktorium.
* Ż: Czemu Maurycy uważa, że jest w stanie złapać złoczyńcę z pomocą Darii?
* R: Samemu nie jest w stanie tego zrobić, bo skupia się na Maurycym. Daria jest nieznanym elementem.

## Potencjalne pytania historyczne:

* Czy kraloth przejmie dużą część artefaktów Maurycego?
* Czy kraloth porwie Darię lub Mausa?

## Misja właściwa:

**Dzień 1**:

Maurycemu uszkodziła się Alegretta. Chciał jej pomóc, ale zabrakło mu kilku narzędzi. Okazało się, że nie ma części narzędzi niskopowerowanych - ktoś je zwinął. Podpytał Eleę; okazało się, że nie jest jedynym, któremu znikają różne rzeczy. Maurycy zdecydował się zastawić pułapkę na ów byt - czy faktycznie ktoś mu wchodzi i próbuje okraść? Zagraża to Alegretcie i ogólnie rozumianym bytom. Zrobił pułapkę.

**Dzień 2**:

Maurycy zauważył ku swemu ogromnemu zdziwieniu, że nie ma do czynienia z człowiekiem czy zwykłym złodziejem. To jakiś vicinius lub bioartefakt; zostawił próbkę dziwnego śluzu. Dobra, czas przejść się po sąsiadach - i podpiąć się do ich systemów monitoringu (wyższa klasa średnia). Potem z uporem i cierpliwością przejrzał feed, łącząc go z magią i znajomością swojego terenu. Cierpliwy Maurycy synchronizuje się z wszystkimi zegarami, monitoruje magią i próbuje odtworzyć ruchy dziwnej istoty. Jak to wygląda. Co to jest.

Maurycy doszedł do wyglądu tej istoty. Fatalny problem sprawił, że nie poznał larghartysa. Po prostu widział kiedyś coś takiego w Świecy. Pamięta nawet kiedy - Estrella Diakon coś z tym robiła. Nie napawa to Maurycego zbyt dużym optymizmem; nie jest fanem Diakonów. Ani Świecy. Ani traconych artefaktów. Przydałoby mu się wsparcie - ale niekoniecznie Diakon. Wezwał więc Darię.

Daria WIE, że znikają artefakty. Nie cieszy jej to zupełnie - jest sama obwiniona. Maurycy jej wyjaśnił, że to najpewniej vicinius albo bioartefakt. Maurycy ma śluz tego cholerstwa i ma maszynkę która może działać jak radar. I Maurycy trochę podejrzewa Estrellę Diakon - terminuskę. Daria aż się zdziwiła - czemu Maurycy podpadł ESTRELLI.

Daria zrobiła szybki research Estrelli - kim ona jest? Czy opieprzy Darię "bo tak"? Czy to podły terminus jest? Jest to test Łatwy - Estrella ma branda i jest znana. Wywiad środowiskowy wykazał, że będzie dobrze. Bo ten:

* Estrella jest znaną terminuską
* Daria ma kiepską reputację
* i komunikuje się w sprawie MAUSA

Terminus-maskotka brzmi dobrze. Ale po co Estrella miałaby robić monstra? Ona raczej słynie z iluzji czy roślin; raczej koi niż tworzy ŚLUZOMORY do kradzieży artefaktów. Daria poprosiła Estrellę o kontakt - prośba o wsparcie terminusa z uwagi na szczególne dopasowanie do sytuacji. Jest korposzczurem. Test Typowy (Łatwy bo odpaliła Zasób). Mimo uwikłania Estrelli i licznych problemów, udało się Darii skomunikować ze Świecą i uzyskać dostęp do Estrelii ZANIM doszło do jakichkolwiek działań Drugiej Strony.

Estrella zdziwiła się, że pojawił się problem. Daria wyjaśniła, że robi własne śledztwo odnośnie artefaktów i ma ślady. Estrella jest zainteresowana. Gdy zobaczyła wizerunek larghartysa, spoważniała. Skomunikowała się natychmiast z Maurycym i ostrzegła go, że ten ma się ufortyfikować i czekać. Już ruszała do akcji, ale się zmartwiła. Odebrano jej operację - Judyta Karnisz ma pewien plan. Daria wzmocniła prośbę - ona chce Estrellę. Judyta przekazała plan Estrelli i się wycofała.

Maurycy tymczasem buduje system ostrzegający przed tymi istotami. Mechanizm łączący poprzednie sieci detekcyjne z osiedla oraz jednocześnie magiczne katalityczne sygnały śluzowe. Detektor, by być w stanie przeciwnika znaleźć i zidentyfikować. Niestety, artefakt robiony jest na szybko i workaround - działa (2/3/3 zostało), ale nie gwarantuje przyskrzynienia wszystkich sił przeciwnika.

Larghatrys odpowiedział wysyłając człowieka (sąsiada), by ten pożyczył od Maurycego cukier. Maurycy nie chce otworzyć - NIE SŁODZI. Sąsiad jest przekonywujący; jednak Maurycemu udało się powstrzymać NAWET gdy gość osunął się umierając na ziemię. Estrella pojawiła się na czas z Darią i pomogły sąsiadowi; Estrella utrzymała go przy życiu.

Dobra. Mały war room. Estrella chce zwabić tą istotę w pułapkę - inaczej Maurycy będzie do końca świata czekał na rozwiązanie. Niech artefaktorzy pomogą Alegretcie wrócić do jakiejś tam formy, potem nałoży się na nią iluzję katalityczno-zmysłową Krwi, by wyglądała jak zainfekowany Maurycy. W ten sposób mają perfekcyjną przynętę. Maurycy nie jest przekonany, ale co zostaje...

Alegretta ma przede wszystkim problem ze źródłem energii. Nie jest szczególnie szybka ani mobilna. Szczęśliwie, wsparcie Maurycego przez Darię wystarczyło do naprawienia Alegretty. I udało się postawić odpowiedniej klasy iluzję. Niestety, doszło do interferencji - zadziała to ale źródło energii przestanie działać po misji. Oki, mają idealną przynętę. Maurycy nie chce wystawiać jej na niebezpieczeństwo, ale...

Estrella zaproponowała, by Maurycy zastawił pułapkę by potwór nie uciekł a Daria by rozwiązała problem zatrucia energii magicznej. Maurycy używa swojego terenu (domu), radaru, tego, że jest tu Alegretta udająca jego i znajomości. Maurycy chce skuteczną pułapkę. Udało mu się. Niestety, nie ma unknown unknowns (nie wie o DRUGIEJ istocie i jej tym nie złapie).

Tymczasem Darii udało się przygotować truciznę magiczną przeciwko viciniusowi. Sukces. Przeciwnik będzie unieszkodliwiony.

Estrella unieszkodliwiła przeciwnika i go zapakowała. Misja wykonana. Po raz pierwszy mają szansę złapać i usunąć Drugiego Kralotha - mają jego DNA. Mają możliwość go trackować...

Lista konfliktów i kart Wpływu:

[!Lista konfliktów](Materials/180499/180411_konflikty.PNG)

## Wpływ na świat:

JAK:

* 2: MUSIK: gracz mówi w jaki sposób jedna z obcych postaci wspiera coś związanego z motywacją JEGO postaci
* 2: CLAIM na wątku / postaci / okoliczności
* 2: do elementu Historii lub przeszłego konfliktu dodajemy "ale" lub "oraz"
* 2: dodajemy pytanie, które musi zostać odpowiedziane na jakiejś z przyszłych misji
* 2: odpowiadamy na pytanie, które pojawiło się na jakiejś z misji
* 3: gracz mówi w jaki sposób jedna z obcych postaci wspiera coś związanego z motywacją JEGO postaci
* 3: zmieniamy kontekst okoliczności czegoś z Historii - scena z przeszłości / fakt?
* 3: do elementu spoza Historii dodajemy "ale" lub "oraz"
* 3: dodajemy znaczącego NPC powiązanego z elementem Historii
* 3: pozyskanie przez dowolną postać znaczącego surowca mającego sens z perspektywy misji
* 3: dodanie nowego elementu Historii
* 3: dodanie przyszłego lub przeszłego faktu; czegoś, co się wydarzyło lub wydarzy

nowy wariant:

* 3: wygranie Typowego konfliktu
* 5: wygranie Trudnego konfliktu
* *2: wygranie Eskalacji

Z sesji:

1. Żółw (11)
    1. Drugi Kraloth złapie dwa dni później drugim agentem Maurycego Mausa (6)
    1. Drugi Kraloth rozprzestrzenił się na Fazę Daemonica zanim doszło do jego znalezienia (5)
1. Kić (3)
    1. Daria Rudas: w Świecy była informacja, że to ONA pomogła w wytępieniu Drugiego Kralotha. Jej niszczycielstwo jest przydatne. Rośnie jej reputacja "terminuski z przypadku" (3)
1. Raynor (8)
    1. Maurycemu jednak uda się naprawić to źródło energii - Alegretta jest znowu sprawna (3)
    1. Gdy Drugi Kraloth porwie Maurycego, Alegretta pójdzie za nimi. I Alegretta znajdzie Tajną Bazę Drugiego Kralotha. (5)

# Streszczenie

Złodziejami nieważnych artefaktów okazały się dwa larghartysy, kralothspawny należące do Drugiego Kralotha. Połączone siły Maurycego Mausa, Darii Rudas oraz Estrelli Diakon dały radę złapać jednego z nich, co stanowi niezły postęp do znalezienia Drugiego Kralotha.

# Progresja

* Alegretta Tractus: ZAWSZE wie, gdzie iść by dostać się do Maurycego Mausa
* Estrella Diakon: jakkolwiek złapała pierwszego larghartysa, nie złapała drugiego i przez to ucierpiał Maurycy Maus. Bittersweet; bardziej bitter.
* Daria Rudas: poszerzenie opinii niszczycielki. To ona pomogła w złapaniu larghartysa. Jej niszczycielstwo jest epickie i bardzo przydatne
* Maurycy Maus: za dwa dni zostanie złapany przez Drugiego Kralotha (gwarantowana przyszłość)
* Glarnohlagh: rozprzestrzenił się na Fazę Daemonica

## Frakcji


# Zasługi

* mag: Daria Rudas, dzięki znajomości biurokracji Świecy znalazła tą jedną terminuskę, która potraktowała ją poważnie - i znaleźli klucz do Drugiego Kralotha.
* mag: Maurycy Maus, wszystko źle zinterpretował, lecz rozwiązał Tajemnicę Znikających Nieważnych Artefaktów przez Drugiego Kralotha. Naprawił Alegrettę.
* mag: Estrella Diakon, właściwa terminuska na właściwym miejscu. Wysłuchała cywili i pomogła im rozwiązać problem kralothspawna (larghartysa). Niestety, przegapiła drugiego.
* vic: Alegretta Tractus, służyła jako przynęta na larghartysa, z powodzeniem. Wróciła do pełnosprawnego działania.

# Plany

## Frakcji

# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Las Stu Kotów
                        1. Osiedle Leśne
                            1. Chatka Zegarowa, atakowana przez dwa larghartysy celem podkradnięcia artefaktów i miejsce pułapki na te larghartysy


# Czas

* Opóźnienie: 2
* Dni: 2

# Narzędzia MG
## Cel Opowieści

* Sprawdzenie żetonów
* Sprawdzenie rozkładu prawdopodobieństwa

## Po czym poznam sukces

* Udane konflikty
* Obecność sukcesów i porażek

## Wynik z perspektywy celu

* sukces

## Wykorzystana mechanika

Mechanika 1804

* obecność Eskalacji: 3-3-3.
* obecność żetonów

Wpływ:

* 8 kart balans między graczami; 8-10 kart to zwykle dzień
* każda porażka = +2 pkt wpływu dla gracza
* każdy skonfliktowany sukces = +1 pkt wpływu dla gracza
* każda karta = +1 wpływ dla MG
* każde 5 kart zaokr. w górę: +1 pkt wpływu dla gracza

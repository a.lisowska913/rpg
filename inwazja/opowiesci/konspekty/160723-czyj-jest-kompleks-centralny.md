---
layout: inwazja-konspekt
title:  "Czyj Jest Kompleks Centralny"
campaign: powrot-karradraela
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}
# Konspekt pisany

## Kontynuacja

### Kampanijna

* [160506 - Wyścig pająka z terminuską (HB, KB)](160506-wyscig-pajaka-z-terminuska.html)

### Chronologiczna

* [160424 - Uważaj, o czym marzysz... (AW, SD)](160424-uwazaj-o-czym-marzysz.html)

## Kontekst ogólny sytuacji
## Punkt zerowy:
## Misja właściwa:

Dzień 1:

Czegoś takiego Siluria jeszcze nie widziała. Wiktor jest szary z przerażenia. 
Wiktor "Coś ty... coś ty zrobiła..."
Dagmara "Lordzie, prezentuję ci Kopalin! <triumfalnie>"

Dagmara "Tylko ty możesz przejąć kontrolę nad miastem. Nikt nic nie wie i panuje chaos."

Dagmara przekonuje Wiktora, żeby on przejął władzę. 
Wiktor "Odejdź!"
Dagmara "Oczywiście, lordzie. Tymczasem przygotuję dla ciebie Kompleks Centralny."
Wiktor "<nie dosłyszał; jest w szoku>"

Dagmara wyszła. Siluria, Wioletta, Wiktor zostali sami. W pokojach Wiktora.
Wiktor "Won..."
Wioletta się ukłoniła i wychodzi. Wiktor jest rozbity. Dagmara przejmie kontrolę bez żadnego problemu. Wiktor się nie spodziewał czegoś takiego. To oznacza, że Dagmara może robić co tylko chce; świat Wiktora runął. Gdy Wioletta wyszła, Siluria go przytuliła i spróbowała go pozbierać. Rozbity Wiktor jest niekorzystny; oddaje kontrolę Dagmarze. Siluria dała radę wytrącić go ze stuporu (-1 fate) i Wiktor się otrząsnął. Uzmysłowił sobie, że praktycznie zniszczył swój ród i jego pozycję w Kopalinie. I stracił "frenemy" - Sabinę - na zawsze.

Wiktor - "Dagmara do raportu! Wioletta! Gerwazy!"

Wszyscy przyszli. Wiktor kazał Dagmarze upewnić się, że może dostać się do systemu komunikacji Kompleksu i by zaraportowała stan Kompleksu. Nie wolno jej nikogo krzywdzić; już dość magów zginęło. Siluria jako czarodziejka KADEMu ma zbudować system komunikacji z jego najemnikami z Millennium; coś się stało i komunikacja nie działa. A Gerwazy ma się zająć obroną Skrzydła Sowińskich. Wioletta chroni Silurię. A on dowodzi. Wiktor zajął się negocjacjami z tien Lenartem Myszeczką (główny lekarz Kompleksu) by kontynuować walkę z Irytką i ową walkę nasilić. 

Wiktor "Jesteś z KADEMu. Coś potrafisz. Zrób to."

Siluria chce wykonać jego polecenie, ale wie, że Spustoszenie może kontrolować komunikację. Więc chce zrobić to tak, by "przypadkiem" znaleźć to, co zakłóca komunikację, co miesza z komunikacją. Dzięki temu będzie w stanie to udowodnić. Czyli: Pracownie Inżynierii Technomantycznej. Z Wiolettą. 

Laura "A co ty tu robisz?"
Wioletta "Jestem terminuską. Wykonuję rozkazy."
Laura "W tym rzecz. Czyje?"
Wioletta "Lorda Wiktora Sowińskiego"
Laura "No to mamy problem. Bo ja wykonuję rozkazy lorda Bogdana Bankierza."
Wioletta "W tej chwili potrzebna nam jedna silna władza"
Laura "I lord Bankierz ją zapewni."
Siluria "Nie ma potrzeby wszczynania burd wewnątrz Świecy. Te sprawy rozwiążą między sobą lordowie Sowiński i Bankierz."
Wiktor, przez radiowęzeł "...przejmuję dowodzenie nad Kompleksem Centralnym..."

Wioletta wysłała sygnał hipernetem do Wiktora o sytuacji. Ten wysłał Gerwazego.
Laura i Wioletta się mierzą; nie chcą ze sobą walczyć. Jednak mają wyraźnie sprzeczne rozkazy. Tymczasem Marcel przysunął się do Silurii i zaczął rozmowę. Powiedział, że dopóki Sowiński nie ogłosił przejęcia władzy, nie musieli ich powstrzymywać; Marcel i Laura są tu by zabezpieczać Pracownie. Marcel powiedział, że Bogdan Bankierz - polityk - przybył porozmawiać z Ozydiuszem i wszystko padło (hipernet, portal). Współpracuje z Ireneuszem Bankierzem - starym terminusem - i chcą zachować porządek.
A tymczasem, gdy Siluria i Marcel mają kulturalną rozmowę, Laura i Wioletta się spierają kto powinien rządzić. Bogdan to polityk. Ale ma mandat i jest doświadczony. Wiktor dowodzi Szlachtą. Co, zdaniem Laury, jedynie świadczy na minus...

Marcel i Siluria doszli do porozumienia - Siluria chce zbudować komunikator do Diakonów. Marcel zauważył, że Wiktor ma wynajęte siły, więc to zmieni balans sił Wiktor - Bogdan; Siluria powiedziała, że może połączyć się z kimś neutralnym. A jeżeli nie - to Bogdan też może kogoś wynająć. Marcel przedstawił pomysł Laurze i ta się zgodziła. Wioletta poinformowała Wiktora i Gerwazy został odwołany.

Siluria zabiera się za poskładanie systemu komunikacyjnego. Pomaga jej dwóch techników. Bankierze ją wspierają (dają formalne wzmocnienie). Siluria walczy z (22); ma (24). Trianguluje i próbuje znaleźć wszystko co i jak - wpierw podstawowy, prototypowy element do komunikacji i próba pingowania. Potem wiadomość z preparowaniem. Jako, że dostała odpowiedź - wiemy, że to zaawansowane zagłuszanie. Inteligentne. Każdy nie-Diakon by uwierzył, że wiadomość przeszła lub miała zakłócenia. Ale Siluria jest Diakonem. 

Nieszczęśliwy technomanta Świecy - "KADEMka kradnie nasze technologie..."
Siluria, skupiona na pracy - "Och proszę, co mam tu kraść?"

Udało się; niestety, podczas pracy nad tym doszło do dodatkowych pyskówek i problemów KADEM - Świeca oraz "Wiktor nie jest rozsądny, bo KADEMka kradnie nasze technologie". Jeszcze silniejsza atomizacja Świecy; magowie vs KADEM i magowie vs Bankierze i magowie vs Szlachta. Sygnał został zlokalizowany - zagłuszacz jest w Ogrodach Świecy. I Siluria wie jak się tam dostać. Ale sygnatura i styl działania zagłuszacza kojarzy się Silurii z technologią KADEMu. Owszem, najpewniej ktoś go zajumał - ale to nie jest fortunny moment na znalezienie zagłuszacza KADEMu w Świecy. Akurat teraz. A Siluria ma twardy dowód, że jest zagłuszanie.
Niestety, nie ma śladów Spustoszenia. Nie ma opcji sensownej nadinterpretacji. Szkoda.

Siluria ma coś, dzięki czemu ma możliwość namierzyć sygnał zagłuszacza i zabrała to do Wiktora. A tam - Dagmara rozmawia z Wiktorem. 

Dagmara "Lordzie, teraz jak nie ma komunikacji mam możliwość zmiażdżyć Bankierzy. Ich siły nie są tak duże."
Wiktor "Jesteś pewna, że jesteś w stanie to zrobić?"
Dagmara "Bez problemu. Wezmę siły, które stoją po naszej stronie."
Siluria "Bankierze wiedzą, że komunikacja jest zagłuszana."
Dagmara "Oni nie mają jak wezwać posiłków. A my jesteśmy skupieni. Możemy uderzyć i ich pokonać."
Siluria "Nawet KADEM - banda indywidualistów - by się zjednoczył. A my chcemy atakować Bankierzy?"
Dagmara "Jeden wódz, silna Świeca. Marzenie Wiktora - silna Świeca."
Siluria "Ile będzie ofiar przy tym? Ile sensownych terminusów zginie? Ile będzie ofiar cywilnych?"
Dagmara "Tak wielu już zginęło, by móc zrealizować to marzenie."
Siluria "Ilu jeszcze musi zginąć członków RODÓW Świecy?"
Dagmara "Ilu będzie trzeba! Tortury Diany Weiner, upadek magów Kurtyny, zniewolenie Antygony, nawet krew z krwi Sowińskich. Czym jest kilka Bankierzy?"

Wiktor - przed śmiercią Ozydiusza - nie patrzył na to w ten sposób. Ale teraz był wstrząśnięty. (15v14->18).

Wiktor "Nie. Więcej śmierci nam nie trzeba. Dagmaro - załatw to pokojowo."
Dagmara "Lordzie, mamy jedną szansę. Jeśli teraz nie uderzę, nie wiem jak zagwarantować Tobie i Szlachcie zwycięstwo."
Wiktor "Wioletta też jest Bankierzem. Twoja przyjaciółka."
Dagmara "Twój cel jest wszystkim."
Wiktor "Nie utopię Świecy we krwi. To moja gildia. Należy do mnie. Idź przejąć te artefakty; na Bankierzy naślę Silurię."
Dagmara "Jak sobie życzysz, lordzie..."

I w ten sposób, przez te opóźnienia, __cruentus reverto__ został ukradziony sprzed nosa Dagmary.
Gdy Dagmara wyszła, Siluria powiedziała Wiktorowi o zagłuszaniu. I że może spróbować to wyłączyć. 

Siluria - "Włączyć w ten proces Bankierzy?"
Wiktor - "Nie. JA wyłączyłem zagłuszanie. Nie oni."

Silurii to bardzo pasuje. I faktycznie (szła z Wiolettą), na miejscu znalazła zakopany zagłuszacz. KADEMowy zagłuszacz. Robota Instytutu Zniszczenia.

Siluria "Och nie! System defensywny! <miażdży zagłuszacz stopą, by nie dało się poznać>."

I w ten sposób dowody zostały zniszczone.
Zza węgła wyszedł Gerwazy Myszeczka i zbliżył się do Silurii. Powiedział jej, że wie, jaki jest wpływ na Wiktora i ogólnie on uważa, że w porównaniu z Dagmarą jej wpływ jest nań korzystniejszy. Powiedział, że niedługo poprosi Wiktora, by mógł pilnować więzienia. Ma nadzieję, że Wiktor się zgodzi na jego prośbę. Po czym uśmiechnął się i odszedł.

# Progresja



# Streszczenie

Siluria wytrąciła Wiktora ze stuporu, by ten nie pozwolił Dagmarze na swobodne kontrolowanie sytuacji. W Kompleksie Centralnym sformowały się dwa ośrodki dowódcze - Wiktora (Dagmary) i Bogdana Bankierza. Siluria skłoniła Wiktora, by ten zmusił Dagmarę do pokojowego rozwiązania tej sprawy; potem Siluria odblokowała komunikację między Kompleksem a resztą Świecy (ktoś umieścił zagłuszacz KADEMu, zniszczony przez Silurię). Pojawia się silniejsza atomizacja i niechęć KADEM - Świeca (Szlachta) - Świeca (Bankierzy) - Świeca (inni).

# Zasługi

* mag: Siluria Diakon, która wyciągnęła Wiktora z dołka i zmotywowała go do działania uniemożliwiając Dagmarze przejęcie kontroli nad Kompleksem Centralnym
* mag: Wiktor Sowiński, po lekkim załamaniu nerwowym zaczął przejmować władzę w Kompleksie. Walczy jawnie z Bankierzami i niejawnie z Dagmarą.
* mag: Dagmara Wyjątek, w której niewiele ludzkich cech już zostało. Dowodzi operacją przejęcia Kompleksu Centralnego.
* mag: Ireneusz Bankierz, starszy terminus doradzający Bogdanowi i będący częścią ośrodka władzy Kompleksu Centralnego. Ad-hocowego.
* mag: Bogdan Bankierz, polityk, który chciał się pojednać z Ozydiuszem, ale nie zdążył. Część ośrodka władzy Kompleksu Centralnego. Ad-hocowego.
* mag: Wioletta Bankierz, która znalazła się między młotem (Bogdan Bankierz) a kowadłem (Wiktor Sowiński). Bodyguard Silurii z życzenia Wiktora.
* mag: Gerwazy Myszeczka, "domyślił" się roli Silurii i ją aprobuje widząc Dagmarę jako alternatywę. Chce dowodzić osłoną więzienia.
* mag: Laura Bankierz, terminuska-lojalistka, która ma zabezpieczać miejsca, gdzie w Kompleksie można coś wytwarzać (zwłaszcza technomantycznego).
* mag: Marcel Bankierz, który z Silurią znalazł rozwiązanie, jak można pozbyć się problemu zagłuszania gdy Wioletta i Laura krążyły dookoła siebie z nożami. 
* mag: Lenart Myszeczka, przełożony skrzydła szpitalnego Kompleksu Centralnego, z którym Wiktor próbował wynegocjować.

# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Centrum 
                        1. Kompleks Centralny Srebrnej Świecy, obszar konfliktu wewnętrznego Świecy 
                            1. Kompleks pałacowy
                                1. Ogrody Świecy, gdzie zakopany był zagłuszacz KADEMu
                                1. Skrzydło Sowińskich, skąd Wiktor próbuje przejąć kontrolę nad Kompleksem Centralnym
                                    1. Sala reprezentacyjna, "sala tronowa" dla Wiktora
                                1. Skrzydło Bankierzy, gdzie powstaje konkurencyjny do Wiktora ośrodek władzy Kompleksu Centralnego
                                1. Pion badawczo-produkcyjny, chroniony przez Laurę Bankierz
                                1. Pracownie Inżynierii Technomantycznej, gdzie doszło do starcia Wioletta - Laura odnośnie prawa Silurii do wykorzystywania surowców

# Skrypt

- Dagmara oddaje Wiktorowi władzę; sama chce opanować Świecę zaczynając od przejęcia kontroli i oddania jej Wiktorowi.
- Dagmara przejmuje kontrolę nad Kompleksem Centralnym w imieniu 
- Potem Dagmara chce dostać sie do artefaktów w Skarbcu. 
- Goran Bankierz w imieniu Świecy chroni skrzydło medyczne.
- Gerwazy Myszeczka chce chronić skrzydło ogólnodostępne. Przecież nie Bankierze.
- Wioletta Bankierz jest w klinczu; między Bogdanem a Wiktorem.
- Bogdan Bankierz za namową Ireneusza chce przejąć dowodzenie wewnątrz Świecy (Kompleksu Centralnego). Polityk, nie terminus.
- Ireneusz Bankierz chce spowolnić działania Sowińskiego. By pomóc możliwie Agrestowi. Stary i niezbyt potężny terminus, acz odważny.
- Dnia '2' uderzy pająk fazowy. Dagmara chce cruentus reverto i artefakty asap.

Drzewo decyzyjne (S/P):

- konflikt Dagmara:Wiktor: siłowo czy miękko rozwiązać Bankierzy
- konflikt Gerwazy:Wiktor: kto pilnuje więzienia
- konflikt o Pion Medyczny; Elea koordynuje Irytkę z licznymi lekarzami. Inni nie ufają Wiktorowi; czemu Elea Maus tu?
- konflikt o krwawość przejęcia władzy
- konflikt o działania poza Kompleksem Centralnym
- konflikt Bogdan:Wiktor:Wioletta. Kogo posłucha.
- konflikt Siluria:Dagmara. Czy można ufać Silurii.

# Czas

* Dni: 1
---
layout: inwazja-konspekt
title:  "Rezydencja Blakenbauerów"
campaign: druga-inwazja
gm: żółw
players: kić, dust, dzióbek
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [140604 - Patriarcha Blakenbauer (AW, WZ, HB)](140604-patriarcha-blakenbauer.html)

### Chronologiczna

* [140604 - Patriarcha Blakenbauer (AW, WZ, HB)](140604-patriarcha-blakenbauer.html)

## Misja właściwa:

![](image RezydencjaBlakenbauerow.jpg)

# Zasługi

* mag: Hektor Blakenbauer, który w zamian za danie potomka rodowi został Patriarchą.
* mag: Andrea Wilgacz, która ku swemu zdumieniu odkryła, że Marian Agrest ma jakąś dziwną agendę połączoną z eksterminacją magów...
* mag: Wacław Zajcew, który został poproszony przez Irinę o ochronę Hektora ORAZ Tatiany.
* mag: Otton Blakenbauer, który umarł bo został odcięty od systemu podtrzymywania życia Rezydencji. KIA.
* mag: Marian Agrest, który przekazał Elżbietę Niemoc do Rezydencji, odciął Czerwca, blokuje wszystkie informacje o zabijanych magach i wszystko wskazuje na to że to on jest mastermindem morderstw.
* mag: Irina Zajcew, która powiedziała w końcu Wacławowi że agentem Inwazji może być każdy i wyjaśniła, że współpracowała z Ottonem; przy użyciu Tatiany zestawiła połączenie z Hektorem.
* mag: Tatiana Zajcew, która przybyła do Rezydencji Blakenbauerów jako ambasador dobrej woli Iriny a skończyła z przekształconym przez Tatianę wzorem magicznym (<3 na Hektora).
* mag: Dracena Diakon, sprowadzona przez Rezydencję do walki z Inwazją; przekształciła Tatianę by ta się w Hektorze zakochała przez zmianę jej wzoru magicznego.
* mag: Marcelin Blakenbauer, pogrążony głęboko w żałobie po śmierci Ottona. Przez to nie pomyślał o Sophistii.
* mag: Karolina Maus, desygnowana na następczynię Aurelii Maus przez Karradraela. Zgodnie z tym co mówi Franciszek Maus "nie jest jedną z nas".
* mag: Franciszek Maus, czarodziej KADEMu który nadal interesuje się swoim rodem i pomaga Andrei jak może.
* mag: Gustaw Siedeł, który upewnił Andreę, że Sophistia jest bezpieczna i nikt nie ma do niej dostępu.
* mag: Waldemar Zupaczka, który zainteresował się czymś czym nie powinien i trafił pobity do szpitala. Nic nie pamięta.
* mag: Maja Kos, która powróciła jako agentka Mariana Agresta i ma zapieczętowane wszystkie rekordy.
* mag: Grzegorz Czerwiec, odcięty przez Mariana Agresta od absolutnie wszystkiego.
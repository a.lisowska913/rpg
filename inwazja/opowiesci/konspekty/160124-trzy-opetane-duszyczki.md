---
layout: inwazja-konspekt
title:  "Trzy opętane duszyczki"
campaign: rezydentka-krukowa
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [170326 - Cała przeszłość spłonęła...](170326-cala-przeszlosc-splonela.html)

### Chronologiczna

* [170326 - Cała przeszłość spłonęła...](170326-cala-przeszlosc-splonela.html)

## Punkt zerowy:

Ż: Dlaczego Paulina podróżuje?
K: Paulina się przemieszcza od czasu do czasu odkąd została magiem, ponieważ chce być elementem świata ludzi a nie magów.
Ż: Dlaczego Paulina zatrzymała się właśnie w Krukowie Czarnym?
K: Praca. Znalazła miejsce, gdzie może pracować zanim będzie musiała ruszyć dalej.

Daniel Stryczek, lekarz, który używa artefaktu w szpitalu Krukowa by nie pozwolić pacjentom umrzeć.
Bólokłąb, upiorna efemeryda, która kaskaduje dalej swoim stanem i cierpieniem; manifestuje się w nie zbudowanym domu.
Maciek, Mateusz i Milena często spotykali się w domu; natrafili na Bólokłąba.

## Misja właściwa:

### Faza 1: Trzy opętane duszyczki...

Dzień 1:

Do Pauliny przyszła matka z dzieckiem. Chłopak ma 13 lat. Wycofany, osowiały, zamknięty w sobie - zupełnie inaczej niż kiedyś. Nie do końca kontaktuje; pierwszy rzut oka Pauliny? Gigantyczny szok. Matka powiedziała, że Maciuś wrócił z zabawy na podwórku z kolegami; było normalnie, jak zawsze. Tylko był cichszy. Zrobił lekcje i poszedł spać. I rano leżał w łóżku, jak teraz. Chłopak ciągle narzeka, że boli i że nie może się ruszyć. Paulina spróbowała nawiązać połączenie z chłopcem (18; między 'ekspert normalnie' a 'ekspert w dobrych warunkach'). Chłopak zareagował... dziwnie, straszną agresją i krzykami. Paulina się nie spodziewała; udało jej się jednak z matką opanować chłopca (8>7). Dzieciak zaczął histeryzować, wrzeszczeć, rzucać się. Matka biała, Paulina... lekko zdziwiona.

Chłopak wrzeszczy, do zdarcia gardła "boli, boli, nie mogę się ruszyć, ja, my, boli, my, ruszyć, igły, boli, niech nie boli, pożreć, przestań, zatrzymaj, pożreć, niech nie boli"...
Paulina teraz podejrzewa już magię. To mogą być dobre halucynki, ale to raczej magia.
Dopytując... Paulina dowiedziała się, że Maćka boli "serce, plecy, macica". Oooook. Coś nie tak. To chyba nie halucynki.

Paulina z matką przypięły Maciusia do łóżka, po czym Paulina poprosiła Olgę (matkę) by skontaktowała się z rodzicami paczki (Maciek, Mateusz, Milena) i dowiedziała się od nich, czy nic im nie jest i czy im się nic nie stało. Korzystając z okazji, zdecydowała się rzucić czar na chłopca. Mały zaczął modlić się po łacinie jak tylko mama wyszła; wygląda na śmiertelnie przerażonego. Paulina zna trochę łacinę i religię; poznała "PSALM 91: A Psalm of Protection". Coraz większe oczka.

Paulina splotła proste zaklęcie skanujące energię magiczną i rzuciła czar (13); wyszło jej, że najpewniej malec był świadkiem manifestacji potężnej efemerydy Spiritus i się z nią podlinkował. Te rzeczy co mówi nie pochodzą od niego. To echa. Czyli, mały ma syndrom ghoula który walczy z jego normalnym, dziecięcym umysłem...

Paulina zdecydowała się na przygotowanie dla niego alchemicznego lekarstwa. To trudna sprawa; [16, 19, 22]. Paulina może wygrać:
- chłopakowi już nic nie zagraża, choć rekonwalescencja potrwa
- rekonwalescencja jest szybka
- lekarstwo jest tanie jak barszcz
- lekarstwo jest produkowalne i w miarę uniwersalne
- lekarstwo NIE skaża
Sukces idzie po (1,3,5). Czyli: Pauliny 21 # 1k20 -> #1. Dokładnie 22. Paulina zdecydowała, że "jest szybka" jest najmniej istotnym elementem równania. Jako, że Paulina jest alchemikiem, katalistą ORAZ lekarzem to akurat ta sprawa była dla niej "idealna". Ale sam fakt, że tak trudno to rozerwać udowadnia, że Paulina ma do czynienia z SILNĄ Drugą Stroną.

Matka wróciła do Pauliny; pozostała dwójka jest w szkole. Chyba nic im nie jest. Jakby co, ma telefon do Mateusza.
Paulina dała Oldze lekarstwo (przejdzie w ramach badań lab, choć to magiczne ciężko cholerstwo jest), po czym powiedziała, że po szkole zadzwoni do Mateusza. A Maciuś dostaje lek i Paulina powiedziała, że Maciuś musi pojechać na Mazury na trzy dni. Powinno pomóc. Ale teraz, nie na weekend.
...czego nie mówi, że ma nadzieję w 3 dni rozwiązać problem efemerydy...

Dziesięć minut później Paulina dostała telefon. To mama Maćka. Podobno Mateusz krzyczał w szkole, wybijał szyby, krzyczał że się dusi i potrzebuje przestrzeni...
Paulinie zrobiło się zimno. To się stało "przed chwilą". Paulina dostała numer do mamy Mateusza. Szybko przygotowała zapas eliksiru (dla Mateusza i potencjalnie Mileny) i skontaktowała się z matką Mateusza. Przekonała ją, że ma lekarstwo; jedzie do szkoły (równolegle z mamą Mateusza) spotkać się z chłopakiem.

W szkole. Mateusz wygląda na "w lepszym stanie". Bardziej kontaktuje. Jest z higienistką w gabinecie.
"Tak strasznie boli, tak bardzo zimno, nic go nie powstrzyma, nic mnie nie powstrzyma, mamo ratuj, tato ratuj".
Jak zobaczył mamę "Nie podchodź! Nie podchodź bo będzie bolało! Bo cię pożre! On tam jest! Nie ma miejsca".
Higienistka zrobiła mu zastrzyk z czegoś uspokajającego "Nie chcę spać! NIE CHCĘ SPAĆ! ON PRZYJDZIE NOCĄ!"

Paulinę coś tknęło. Milena jest starsza od Mateusza, który jest starszy od Maćka. Ona - jeśli jest następna - to niedługo. 
Podeszła do Mateusza. Zaczęła go przepytywać - co się działo, jak... okazuje się, że środek uspokajający pomaga. Paulina się z nim dogadała (17):
- bawili się w "szarym domu" (nie do końca zbudowany dom, należy do Przemuskich; zabrakło im pieniędzy i była jakaś sprawa z ekipą budowlaną). Ofc nikt nie wiedział gdzie się bawią
- nie robili nic okultystycznego
- efemeryda to "siedem". "Siedem i jeden". Nie umie inaczej nazwać; jego umysł tego nie łapie.
- wpierw opowiadali sobie historie o duchach, potem się chowali (by było jeszcze straszniej)
- Milena krzyknęła; potem efemeryda się pojawiła.
W młodym wzbiera furia; Paulina pozwala mu na zrobienie pierwszego ruchu, po czym oddaje porządnym plaskaczem (celem jest podniesienie swych szans w wyprowadzeniu go na "Mateusza" a nie "Siedem"). (sukces) To daje jej #3 na social aggressive; (14v10-12-14 -> 10). Paulina podniosła na (11); higienistka i matka do Pauliny z gębą, ona je uspokaja, kilka minut to zajęło a "Mateuszek" wrócił do formy. Jest chłopakiem, choć uszkodzonym; nie 'efemerydą'.

Po uspokojeniu i przebadaniu chłopaka, Paulina dała im lekarstwo dla Mateusza. To samo zalecenie co w wypadku Maćka; ma wyjechać na Mazury na kilka dni. Może jechać z Maćkiem.

Paulina spytała jeszcze matkę Mateusza; czy Milena chodzi do tej samej szkoły? Tak. Jest dziś na lekcjach? Nie wie. Dobrze...
Niecałe 5 minut zajęło Paulinie dowiedzenie się, do której klasy chodzi Milena. Dzwonek za 15 minut.

Piętnaście minut później - Mileny nie ma w klasie. Była w klasie - wyszła przed tą lekcją. Koleżanka Mileny mówiła, że Milena mamrotała, że "jej tata wszystko naprawi, wszystkich ocali, że będzie dobrze, że jest bohaterem" - tata Mileny jest strażakiem. Oki. Paulina szybko spytała gdzie jest remiza i gdzie mieszka Milena - dowiedziała się, że tata Mileny będzie dziś w domu, miał nockę. Oki. Paulina poszła do domu Mileny. Zadzwoniła wpierw do taty Mileny; ona poprosiła go o pomoc, jest ktoś kto potrzebuje pomocy, i on musi pomóc; gość się boi i jest uwięziony. Tyle zrozumiał; Milena jest podekscytowana i nie do końca normalnie się zachowuje. Paulina poprosiła, by ją zatrzymał. Milena się wyrwała i pobiegła, on za nią. Młoda leci przez chaszcze.

Na szczęście Paulina wydedukowała, że Milena zmierza ku Szaremu Domowi. Jest jej do niego bliżej niż do domu Mileny. Kurs przechwytujący...
Przechwycenie Mileny z ojcem: 15
Paulina nie prosi o podwózkę. Wsiada w auto i jedzie z piskiem opon. 13. -> 12. Paulina jechała jak dzika; omijała psy, wyprzedzała traktor w niewłaściwym (cholernie niewłaściwym) miejscu i złapał ją ten JEDEN JEDYNY fotoradar... w bardzo niewłaściwy i kompromitujący sposób. Oh well... "pani doktor rajdowiec", tak na nią wołać będą.
A niech wołają. Zdążyła na czas - Milena, pocięta roślinnością (jeżyny itp), a ojciec za nią, ale nie zdążył. Paulina - akcja jak z filmu - wjechała tak, by zablokować drogę Milenie by ojciec zdążył ją przechwycić.
Blokada drogi: 12
Paulina: 7 (#9, #17) - wjechała (ostro i brutalnie) przez płot (którego nie ma, ale jest betonowy próg) i lekko nadwyrężając auto zastawiła drogę Milenie. W sam raz, by ojciec ją złapał i unieruchomił.

Milena wpada w histerię. Ojciec jest jednak silny. Trzyma. Paulina się nie przejmuje; zastrzyk uspokajający. Milena wiotczeje. Wsadzają Milenę do samochodu i jadą. Do gabinetu Pauliny. Dark future zażegnany, wszyscy bezpieczni, wow.

Paulinie jedna rzecz nie pasuje. Taka efemeryda wymaga naprawdę dużej ilości energii. A ona NIC nie czuje od Szarego Domu. Efemeryda by tu nie przetrwała. A jednak wszystko centruje się na tym miejscu - na Szarym Domu. 

Nic, czas się skupić na Milenie. Młoda, co nie jest dziwne, piszczy "tato, ratunku, zniszcz potwora, uwolnij mnie, zimno, nie mogę się ruszyć". Ojciec jest, oczywiście, niewyganialny. Paulina stwierdziła, że tu jest dobry moment na eksperyment. Przejrzała internet i znalazła Psalm VI - błaganie o litość. Zaczęła go na głos czytać. Milena zamarła, by po chwili zacząć recytować unisono z Pauliną. Ojciec "czy to opętanie"? W ogóle zgłupiał. Paulina stwierdziła, że nie jest; dziewczyna jedynie się przestraszyła "co ja powiedziałam, co to znaczyło, skąd ja to znam!" Starsza niż Maciek dziewczyna nie potrafi zrozumieć co się z nią dzieje gdy łączy się z efemerydą. Ale to potwierdza teorię Pauliny - dzieci stały się częścią efemerydy. Nie do końca, ale efemeryda ma w nich macki a one mają macki w efemerydzie. I na pewno efemeryda zapewnia znajomość psalmów po łacinie...

<<Kić: i tu by się Maria przydała...>>

Paulina, jak zawsze - przepisuje lekarstwo Milenie. Zaleca kilka dni na Mazurach. Najlepiej od dzisiaj. Mogą jechać z Maćkiem i Mateuszem. Nieważne - ale daleko stąd.
A Paulina zamyśliła się nad samą efemerydą...

## Dark Future:

### Faza 1: Trzy opętane duszyczki...

- Maciek trafia do Pauliny przyprowadzony przez matkę.
..... Possessed.make <someone> question the sanity / state of the Possessed resulting in <action>

- Mateusz wybija wszystkie okna po kolei w szkole i wpada w histerię; 
..... Possessed.manifest the demonic influence doing <something> unnatural

- Milena zabiera tatę ze sobą; niech zniszczy Bólokłęba. Rodzice w końcu umieją... 
..... Possessed.lure <someone> close to them into the darkness

- Bólokłąb się manifestuje pożerając dorosłych.
..... Wounded Devastator.destroy <someone> in horryfying way

### Faza 2: ...tańczą z Dewastatorem...

- Milena chce popełnić samobójstwo wypijając kreta; z uśmiechem na ustach.
..... Possessed.perform a self-destructive <action> resulting in major damage

- Rodzice Mileny podpalają swój dom i zostają w środku, śmiejąc się szaleńczo.
..... Human Victims.cause unrest in population because <reasons>


# Zasługi

* mag: Paulina Tarczyńska, odmontowała trójkę dzieciaków od efemerydy i została "panią doktor rajdowiec". Jaka wieś, taki Bond.
* czł: Olga Jeden, matka Maćka; główny węzeł informacyjny dla Pauliny. Gdyby nie ona to po prawdzie skończyłoby się to tragicznie. Nikt by nic Paulinie nie powiedział.
* czł: Maciek Jeden, 13-latek, który jako pierwszy miał oznaki opętania przez efemerydę. Na szczęście, najłatwiej mu z tego wyjść.
* czł: Mateusz Kuraszewicz, 15-latek, który spotkał się z efemerydą i niszczył okna i krzyczał. Paulina mu pomogła. 
* czł: Milena Letniczek, 16-letnia dziewczyna, która wierzyła do samego końca, że ojciec ją uratuje i zniszczy efemerydę. Prawie go zabiła.
* czł: Onufry Letniczek, ojciec Mileny, strażak; zatrzymał ją z pomocą Pauliny i nie skończył jako przysmak dla efemerydy.
* vic: Bólokłąb, efemegestalt, efemeryda Spiritus, która Opętała i podmontowała do siebie trójkę dzieci. Zdaniem Pauliny: nie ma prawa istnieć (za mało magii by utrzymać).

# Aktorzy

- Bólokłąb, <<Ranny Dewastator>>
- Maciek / Mateusz / Milena <<Opętaniec>>

# Lokalizacje:

1. Świat
    1. Primus
        1. Mazowsze
            1. Powiat Pustulski
                1. Krukowo Czarne
                    1. Gabinet Pauliny
                    1. nie zbudowany dom Przemuskich, "szary dom"
---
layout: inwazja-konspekt
title:  "Gildie widziały protomaga"
campaign: prawdziwa-natura-draceny
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [141019 - Lekarka widziała cybergothkę (PT)](141019-lekarka-widziala-cybergothke.html)

### Chronologiczna

* [141019 - Lekarka widziała cybergothkę (PT)](141019-lekarka-widziala-cybergothke.html)

## Misja właściwa:

Paulina się rano obudziła. Maria jeszcze na posterunku. Bardzo Marię przeprosiła i powiedziała, że jest jej coś winna. Maria powiedziała, że nikt nie próbował dostać się do Nikoli. Chyba, że użyto metod magicznych, których oczywiście Maria nie jest w stanie rozpracować. Maria poszła spać a Paulina została. Tym razem Paulina nie pozwoli Dracenie spać. Ubrała się, umyła i poszła do pokoju Draceny. ~8:30. Paulina natarczywie się dobijała, aż w końcu Dracena otworzyła. Nie była ubrana, jak to Diakonka. Paulina się nie przejęła i kazała jej wstać - długi sen otępia. Dracena przyjęła to z absolutną godnością i poprosiła o moment. Pół godziny później (~09:00) Dracena się pojawiła. A Paulina zrobiła jej mocną kawę. Chokiatto, taka, jaką Maria lubi najbardziej. Dracenie też zasmakowało. Przyznała, że trzy godziny po trafieniu do pokoju przygotowywała systemy obronne i zaklęcia. Nie wyspała się.
Cóż, nie przywykła, że ktoś na nią poluje. Paulina powiedziała Dracenie, że wszystko wskazuje na to, że u Nikoli nikogo nie było; to najpewniej nie atak na nią. Za to Paulina zapytała, czemu Dracena myślała, że to wina Rafaela. Cóż, Dracenia nie zna na tyle Rafaela, ale może to element testu... choć trochę nie w stylu Rafaela, po prawdzie. Paulina też wydobyła informacje skąd Dracena zna policjanta - ta powiedziała, że wskazał jej plotki o aniele co doprowadziło ją do Nikoli. Paulina pośrednio wyciągnęła, że Dracena i policjant mieli romans.

Paulina zaproponowała Dracenie skontaktowanie się z Rafaelem. Ta odmówiła. Z dyskusji wyszło, że Dracena nie to, że nie lubi Rafaela, ale chce sama rozwiązywać swoje problemy. Nie chce musieć na nikim polegać, zwłaszcza kimś interesownym. Paulina zaznaczyła, że Rafael może być potrzebny by pomóc Nikoli, na co Dracena zauważyła, że zgodnie z prawem jeśli się zorientuje że jest protomagiem, powinien ściągnąć ją do Millennium. Dracena dodatkowo powiedziała, że Rafael jest magiem dość opanowanym i konsekwentnie (niestety) trzyma się zasad EAM - nie dzieli się sekretami z innymi członkami rodu. Poproszona przez Paulinę, umówi Rafaela i Paulinę. Ale sama nie chce mieć z tym nic wspólnego. Ona toczy swoje walki sama. Zadzwoniła i Paulina zwróciła uwagę, że DiDi zachowuje się oficjalnie i chłodno. Przedstawiła Paulinę jako czarodziejkę mającą wspólne z nią spotkania i że Rafael powinien z nią porozmawiać na wspólnie ich interestujący temat; jednocześnie zaprzeczyła że są przyjaciółkami a nawet koleżankami.

Dracena oddaliła się do swoich spraw koło 10:17 a koło 11:00 z Pauliną skontaktowała się Maria. Radośnie stwierdziła, że 3:30 snu to dość. Poprosiła Paulinę o obraz mentalny przez soullink tej trójki która zaatakowała Paulinę, DiDi i Kasię i powiedziała, że ona spróbuje ich znaleźć. Skontaktuje się z Arturem Kurczakiem i razem znajdą tych oprychów. Maria nie pozwoli, by ktokolwiek w jakikolwiek sposób zagrażał JEJ czarodziejce i przyjaciółce. Paulina spełniła jej prośbę. Niestety, wbrew naleganiom Pauliny, Maria stwierdziła że chokiatto musi poczekać.

12:00, Rafael Diakon spotyka się z Pauliną Tarczyńską w kafejce "U Sępa". Paulina przyszła troszkę wcześniej. Gdy Rafael się pojawił, okazał kurtuazję i elegancję. Potwierdził chęć rozmowy. Wpierw Paulina spytała Rafaela o to, czemu nakierował Dracenę na Nikolę. Rafael spokojnie zaprzeczył. On znalazł Nikolę, stwierdził, że to zaawansowany i skomplikowany przypadek i znalazł kogoś innego kogo przekształcił dla Draceny. Zdaniem Rafaela to Dracena powinna się z nim skontaktować i powiedzieć, że Nikola przecież nie może być tą osobą. Rafael by potwierdził. Dracena miała to w protokole, ale ona musiała dokonać założeń i działać po swojemu. Paulina dopytała też o Nikolę - Rafael stwierdził, że jak on badał Nikolę 16 dni temu to jej wzór się zasklepiał w coś. Wszystko co się potem popsuło to robota Draceny.
Rafael dodał też, że Nikola jest też elementem testu dla Draceny. Nie powiedział jakiego i w jaki sposób, ale Dracena musi podjąć jakąś decyzję.
Rafael zaznaczył, że Dracena związała mu ręce. On nie może niczego zrobić, jeśli go nie poprosi lub nie zacznie robić czegoś niewybaczalnego.

13:17. Paulina poszła w kierunku na hostel. Idąc po drodze zauważyła (7v5->8) jako katalistka, że hostel emanuje Skażeniem. Skażenie jest Skażeniem typu przestrzennego - hostel jest przekształcany przez geometrię, co powoduje przyspieszoną degenerację budynku które potencjalnie spowoduje zawalenie poniżej kilkunastu godzin. Skażenie nie jest dużej mocy. Paulina natychmiast zadzwoniła do Draceny, pytając gdzie jest. Dracena nie chce powiedzieć, ale w tle Paulina słyszy Kasię, która namawia Dracenę, by ta już poszła. Paulina mówi rozkojarzonej DiDi, że ta ma iść i zrobiła to, co każdy szanujący się mag powinien zrobić.
Skontaktowała się z lokalnym terminusem. Odpowiedział Bolesław Derwisz - terminus-mentalista z elementami ludzizmu. Paulina zgłosiła mu Skażenie, ludzie zagrożeni, mag mieszka w tym hostelu i ogólnie nie wiadomo co się dzieje. Aha, i mało czasu. Derwisz powiedział, że zadziała tak szybko jak tylko będzie w stanie. Będą w okolicach 30 minut.

Paulina obchodzi teren. Próbuje określić coś więcej odnośnie natury tego Skażenia. (7v4->5) Skażenie było impulsowe i ma efekt rezydualny. Gdzieś w hostelu jest źródło Skażenia. Skażenie ciągle wzmacnia się źródłem, ale coraz słabiej. Skażenie jest typu przestrzennego, wygląda na to, że epicentruje się w okolicach w których pokój mają Dracena i Paulina. Skażenie ma posmak Soczewki lub Paradoksu; jest wyraźnie działaniem niewprawnym i nie wygląda na atak. Chyba, że wyjątkowo marny lub nietypowy.
Cóż, nie jest to pierwszy atak wymierzony w Dracenę.

13:38 z Pauliną skontaktowała się mentalnie Maria. Powiedziała, że wraz z Arturem Kurczakiem zlokalizowała pierwszego z tej trójki, która napadła DiDi i Paulinę. Idzie dalej tym tropem i spróbują się czegoś dowiedzieć. Paulina powiedziała jej mentalnie o sytuacji z hostelem i obie wprowadziły tryb ostrożny - "Paulina nie zna Marii, Maria nie zna Pauliny". Tak jest bezpieczniej dla wszystkich. Kontakt jedynie po soullinku.

13:51 przyjechał terminus z koleżanką. Bolesław Derwisz i Dalia Weiner. Szybko zapoznali się z sytuacją, Paulina ich wdrożyła. Bolesław wypytał o Dracenę; słysząc, że to Diakonka wyraźnie się skrzywił. Nie lubi Diakonów z jakiegoś powodu. Dalia jednak go zatrzymała - nie czas na to teraz. Bolesław podziękował Paulinie za informacje o naturze tego Skażenia. Dalia jest wyraźnie zestresowana; to chyba jej pierwsza akcja. Bolesław zaproponował prosty plan - wchodzą, Dalia ich osłania puryfikacją. On unieszkodliwia centrum Skażenia, Dalia je błyskawicznie puryfikuje. Natychmiast potem Bolesław osłania Dalię a ona czyści budynek ze Skażenia. Paulina zadzwoniła dyskretnie do DiDi chcąc ją ostrzec, ale DiDi nie odbiera telefonu.
Działają zgodnie z planem. Okazało się, że źródło Skażenia jest w pokoju Pauliny. Drzwi wypaczone przez geometrię, więc Bolesław zdjął je kopniakiem, po czym na wejściu strzelił zaklęciem obezwładniającym do środka. A w środku... klęcząca i modląca się Nikola. Ona jest źródłem Skażenia. No i padła jak naleśnik na glebę.

A dlaczego DiDi nie odbiera telefonu? 
Otóż, DiDi szła do domu Ryszarda Bociana (lekarza), zaciągana tam przez bardzo podekscytowaną Kasię. Kasia chciała ją zapoznać ze swoim chłopakiem, chciała jej koniecznie go pokazać... DiDi nie do końca wiedziała o co chodzi, ale zauważyła pewną zmianę w zachowaniu Kasi. Telefon Pauliny ją rozkojarzył na wejściu do domu Bociana i jej myśli były gdzieś indziej podczas wchodzenia. Gdy spotkała sie z Jankiem Bocianem, młodym ludzkim chłopakiem, z wrażenia upuściła telefon.
Otóż Janek Bocian jest osobą zmienioną przez Rafaela Diakona.
Rafael przekształcił Janka w następujący sposób: praktycznie nie da się mu oprzeć. A efekt został szczególnie wzmocniony i skierowany na Dracenę.
Jednak Dracena Diakon ma bardzo silną wolę. Wytrzymała feromony, które zniewoliłyby anioła.
Do momentu, aż zadziałała Dyta, agentka Silgora Diakona znajdująca się w negliżu w pokoju z Jankiem, która jedynie czekała na tą chwilę. Dyta (vicinius rasy wiła) rzuciła w Janka Mruczkiem, zwykłym, niemagicznym kotem domowym państwa Bocianów. Janek spróbował złapać kota i ów rozorał mu rękę.
Krew jako katalizator i pobudzacz był zbyt silny. Kasia oraz Dracena natychmiast zostały zniewolone przez feromony.
16-letni chłopak (Jan Bocian) zdecydowanie przeciwko temu nie protestował.
...

Dalia rozpoczęła puryfikację. Paulina chciała podejść i przebadać Nikolę, ale Derwisz ją zatrzymał. Dalia nie wie, co się dzieje, więc Paulina poprosiła ją o zostawienie Nikoli bez puryfikacji. Bolesław, zapytany jak długo Nikola będzie nieprzytomna powiedział, że jakieś kilka godzin - strzelał by się upewnić, że przeciwnik będzie wyłączony. Powiedział Paulinie, że to wymaga opowiedzenia historii. Paulina potwierdziła.
Na hałas (wywalonych drzwi) przyszła recepcjonistka i zaczęła łajać magów "co tu się dzieje". Bolesław tak się ustawił by recepcjonistka nie widziała nieprzytomnej Nikoli i zaczął przepraszać, że się uniósł i ze zapłaci za szkody. Recepcjonistka była nieugięta. Paulina spróbowała ją przekonać (5+2 vs 7 -> 5), że przecież zapłacą i za szkody i za okoliczności, ale nic nie dało się zrobić. Jak tylko recepcjonistka stwierdziła, że zadzwoni na policję, Paulina użyła magii mentalnej i ją uspokoiła.

Dalia poszła przejść się po hostelu celem usunięcia Skażenia a Bolesław czekał na obiecaną historię.
Paulina z historii pominęła wszelkie elementy udziału Marii; ona sama przybyła w to miejsce. Ważne punkty opowieści:
- był rytuał czarodziejki z EAM, Paulina to powstrzymała (przesunęła)
- okoliczności rytuału
- że EAMka nie była sama; był tam też mag jej pomagający którego przekonała Paulina
Bolesław Derwisz zaczął przekonywać Paulinę, ze jeśli chce pomóc, niech powie wszystko co może na temat tej EAMki i maga z nią współpracującego. Paulina zaznaczyła, że przecież to mag EAM, terminusi niewiele mogą zrobić osobie o odpowiedniej pozycji. Derwisz skontrował - właśnie po to są terminusi, by magowie EAM nie bawili się w bogów. A jeśli polityka go zatrzyma, to przecież terminusi są w stanie ratować ofiary EAM i przeszkadzać w rytuałach okrutnej czarodziejki.
Po namyśle, Paulina przekazała informacje o EAMce i o Sebastianie. Nazwała go z imienia i nazwiska.
Temat co zrobić z Nikolą, czy będzie magiem czy człowiekiem, Bolesław i Paulina odroczyli na później. Ale widać, że Bolesław będzie chciał przechwycić Nikolę do Świecy.

14:19, wróciła Dalia. Powiedziała, że oczyściła co się dało, ale podczas pracy nad tym wykryła lekkie emanacje Magii Krwi. Musi dokładniej przeszukać i to sprawdzić. Bolesław powiedział, że nie musi. On wie, skąd to. Jeśli MK, to stoi za tym Diakonka. Pojawienie się Magii Krwi zmienia sytuację - terminusi mają bezwzględne prawo wejść do każdego miejsca w którym wykryto MK i dokładnie sprawdzić, w jakim stopniu i co jest zrobione. Paulina usiłuje bronić DiDi - DiDi tylko leczyła i pomagała Nikoli, lecz Bolesław jest nieugięty. Dalii też się to nie podoba - po pierwsze, antagonizują nieznaną sobie czarodziejkę, po drugie, nic nie wskazuje na to, by DiDi zrobiła jakąkolwiek krzywdę Nikoli a po trzecie, potencjalne pułapki; Bolesław to wszystko konsekwentnie ignoruje. On chce tam wejść i dokładnie przeszukać czyny i działania podłej Diakonki. Wydał rozkaz Dalii, która w tym momencie zrobi to, co jej się kazało.
Paulina nie chce mieć z tym nic wspólnego. Jak tylko Bolesław i Dalia wyszli z pokoju Pauliny (zostawiając ją z nieprzytomną Nikolą), Paulina wysyła SMSa Dracenie (ta dalej nie odbiera telefonu) informując ją o sytuacji. Następnie Paulina informuje o całej sprawie Marię i mówi, że bardzo nie chce, by w jakikolwiek sposób Nikola trafiła do Srebrnej Świecy. Maria prosi, by Paulina się z nią kontaktowała na bieżąco i daje Paulinie szansę, że Maria coś wymyśli. Potrzebuje jednak trochę czasu.

W czasie gdy Boleslaw i Dalia radzą sobie z przeszukaniem pokoju Draceny, Paulina błyskawicznie skontaktowała się z Rafaelem Diakonem. Powiedziała mu, że pojawiła się ekipa Srebrnej Świecy i przeszukują pokój Draceny. Rafael się zdziwił, bo nie do końca mają prawo. I skąd tu się w ogóle wzięli. Paulina powiedziała o tym Skażeniu i obecności Nikoli na miejscu oraz o tym, że ich wezwała a terminus najwyraźniej nie jest fanem rodu Diakon. Rafael z ciężkim westchnięciem stwierdził, że może musieć się tu pojawić. Powiedział Paulinie, że go nie zna. Umówili się na spotkanie ze sobą na 17:30 w kafejce "U Sępa", jak ostatnio.

Paulina stanęła przed trudnym wyborem. Albo iść z Bolesławem i Dalią przeszukać pokój Draceny i potencjalnie ukryć coś, co mogłoby Dracenie zaszkodzić, albo połączyć się magią mentalną z nieprzytomną Nikolą i spróbować się dowiedzieć, skąd się tu wzięła i o co w ogóle tutaj chodzi. Po namyśle, zdecydowała się na coś takiego - wpierw pójdzie z Bolesławem i Dalią by ich maksymalnie spowolnić i uniemożliwić zebranie dowodów przeciwko DiDi (jakiekolwiek by one nie były) a potem jak pojawi się Rafael, wycofać się do Nikoli i ją przesłuchać (otworzyć do niej kanał mentalny i założyć, że Rafael zna się na rzeczy i blokowaniu nadgorliwych terminusów SŚ). Wpierw dotarła tam by siać zamęt (7 vs 3 -> 10) między zespołem, prosto w Dalię, by wzbudzić w niej brak podstaw prawnych i "nie powinniśmy tego robić, nie powinniśmy tu być". Zdecydowała się jednak zostać w ukryciu; tzn. działać bardzo subtelnie. Więc +2. I następny test to (4+2 v 6 -> 5), na chowanie rzeczy niewłaściwych przed Bolesławem (kłócącym się z Dalią) zanim ten się zorientuje. Nie wyszło; parę rzeczy udało się jej schować, lecz Bolesław coś znalazł.
I na to wszedł Rafael.
Ofuknął wszystkich w środku od prowadzenia przeszukania na terenie Millennium bez poinformowania innego maga Millenium. Bolesław odparł, że mu wolno zgodnie z zasadami prawnymi. Rafael zaznaczył, że faktycznie mu wolno, ale niech nie liczy na dobrą wolę ze strony Millennium jeśli on będzie potrzebował jakiejś pomocy. Zwłaszcza że wprowadził osoby niepowołane - pokazał palcem na Paulinę i ją wyrzucił.

Paulina wróciła do Nikoli. 14:38. Zamknęła drzwi na zamek i rzuciła zaklęcie komunikacyjne z Nikolą (9v8->9 by zrobić tak, by tamci kłócący się nie zorientowali się i nie zareagowali). Co tam, w tym domu, się do cholery stało? Nikola przekazała, że był tam ktoś, kogo nie widziała nigdy wcześniej. Rano. Ten ktoś "coś robił". Ona nie wie co. Nie "dotknął" jej. Ale czuła się dziwnie. Bała się. Zaczęła się więc modlić, by być blisko swego archanioła, by być w bezpiecznym miejscu. I najpewniej kombinacja silnych uczuć w połączeniu z krwią Nikoli w pokoju Draceny spowodowały wywołanie efektu protomaga. Hurra?
Paulina uzyskała twarz (zamazaną trochę) osoby, której przestraszyła się Nikola.
Maria natychmiast dostała twarz tej osoby. Może widziała, może przyda się backup... tak czy inaczej, warto mieć.

Maria dała zwrotkę - jeden z tej trójki którzy napadli wcześniej DiDi, Paulinę i Kasię został przesłuchany przez Artura i Marię. Zaczął sypać. Powiedział, że dostali po 100 zł na osobę za to, że nastraszą solidnie dziewczyny. Zapłaciła im przepiękna kobieta. "Jak gwiazda porno, ruda i seksowna. Zrobiłbym dla niej wszystko co by sobie życzyła". Pierwsza myśl Pauliny - Diakonka. INNA Diakonka. Może Rafael lub Dracena coś wiedzą, ale żadne z nich nie jest chwilowo osiągalne.
Maria powiedziała, że spróbują przesłuchać dwóch pozostałych. Mają też wygląd, Maria popyta - tak wyglądające kobiety po prostu zwracają uwagę.

Mając przed sobą perspektywy ciągle kłócących się Bolesława i Rafaela, Paulina zdecydowała się zobaczyć o co chodziło tajemniczemu magowi. Przejść się do domu Nikoli i przeanalizować co i jak tam się stało. Bez żadnego prolemu dotarła do celu. Oczywiście, nikogo tam nie ma. Paulina zdecydowała się sprawdzić jaki typ magii był tam rzucony. (9 vs 6/8/10 -> 11) Paulina dowiedziała się tego czego chciała:
- jaki to typ magii: grupa delikatnych i kompleksowych zaklęć katalitycznych nie centrowanych na Nikoli. Centrowanych na jej środowisku - czy ktoś tu czarował, po co, czy ona czarowała... ogólnie, wybadać co i jak dookoła Nikoli było robione.
- Paulina nie widziała nigdy wcześniej takiej sygnatury. To kolejna nowa osoba.
- ktoś DOKŁADNIE wiedział czego szuka. Są delikatne sygnały i nawiązania dookoła Magii Krwi. Czar był skonfigurowany, by nie dotknąć Nikoli.

Paulina wróciła do pokoju prosto na wyjście z pokoju Draceny - Rafaela. Terminusów SŚ już tam nie ma. Rafael spojrzał na Paulinę władczo i oddalił się bez słowa, sealując pokój Draceny. Jest 16:12. Komunikat od Marii - cała trójka napastników widziała tą samą piękną kobietę. I wszyscy mieli wobec niej to samo - chcieli spełniać jej życzenia. Maria pochodziła trochę po mieście i więcej osób widziało tą piękność - wielu ludzi miało ten sam efekt.
Do spotkania z Rafaelem Paulina kombinuje nad kombinacją zwykłych, niemagicznych rzeczy z apteki jakie mogą pomóc Marii by ustrzec się przed feromonami.
Paulina powiedziała Marii, że jest bardzo źle. Nie tylko nie uda się przekształcić Nikolę w człowieka, ale i "oni" chcą ją zabrać do gildii i nauczyć jej magizmu. Po rozmowie z Anią Kozak Paulina wie już, co to jest Rodzina Świecy i bardzo by nie chciała, by biedna Nikola dołączyła do "Rodziny Świecy" lub "Rodziny Millennium" (jeśli coś takiego istnieje; Dracenie nie do końca można ufać, bo jest stroną). A Paulina i Maria chciałyby, żeby Nikola mogła żyć jak chce. Może być magiem - ale niech jest ludzistką. Niech pamięta, skąd jest. Maria powiedziała, że ma u kogoś przysługę. I spróbuje go ściągnąć. I może im uda się przechytrzyć wszystkich.
Paulina jest jej naprawdę wdzięczna.

Maria przeraziła się całą tą sytuacją, więc zdecydowała się na "guzik atomowy". Skontaktowała się ze swoim znajomym który wisi jej przysługę - Janem Fiołkiem, doskonałym snajperem i bardzo niebezpiecznym człowiekiem i powiedziała, że może być pewna sprawa. I że dotyczy ona magów, w tym potencjalnie magów niebezpiecznych i to takich, których nie chce zabijać. Fiołek obiecał, że przyjedzie i pomoże Marii jeśli będzie taka potrzeba. Pojawi się rano, dnia następnego. Maria nie powiedziała o co chodzi, Fiołek nie pytał. Zdrowa relacja.

17:24, Dracena uwolniła się od wstępnego efektu. Gdy wychodziła od Jana, widział ją lekarz (Ryszard Bocian). Zażądał, by zostawiła jego syna w spokoju, na co Dracena odparła, że jemu nic do jej działań. Doktor Bocian jest w kropce. Zgłosił problem "agentowi specjalnemu", czyli Silgorowi Diakonowi. Silgor powiedział, że Dracena jest już w większości zneutralizowana. Lekarz poprosił, by nie odbiło się to na jego rodzinie, na co Silgor powiedział, że nie będzie żadnego problemu.
Ryszard Bocian porozmawiał ostro z synem, jednak nie jest w stanie niczego mu nakazać. Janek jest otoczony dwoma dziewczynami, Kasią i Dytą. Dyta wstała i leniwie odprowadziła lekarza do innego pomieszczenia, gdzie pokazała mu jak daleko sięga jej władza ludzkich ciał i umysłów. Obiecał, że niczego nie zrobi, nie będzie Jankowi przeszkadzał ani rozkazywał. Tylko żeby przestała. Dyta się zgodziła.

Dracena, bardzo osłabiona i z szalejącą magią i krwią Janka na ciele niewiele może zdziałać, kręci się chaotycznie po mieście, próbując się pozbierać. Całkowitym przypadkiem znajduje ją Paweł Brokoty - 29-latek, który pracuje w klubie "Klimat Nocy". Naturalnie, rozpoznał Dracenę. Widzi, że ta jest nie w formie, więc zaprosił ją do siebie do mieszkania. Mieszkanie to mała, ciasna kawalerka, ale dobre i to. Założył, że czarodziejka jest na jakichś narkotykach i odpowiednio ją potraktował - coś na odkażenie, pomógł jej się wykąpać i położył w swoim łóżku. Dzięki temu Dracena zaczęła powoli dochodzić do siebie, choć wciąż jest szalenie zakochana i podatna. Była oszołomiona do tego stopnia, że nie zauważyła w Dycie viciniusa; myślała, że Dyta jest po prostu koleżanką Janka lub Kasi. Jednak Dracena jest świadoma, że MUSI rozwiązać problem z Jankiem. Inaczej przegra.

Tymczasem Derwisz i Dalia skanowali dom Nikoli magicznie. Wplątali się w rozwikływanie śladów Pauliny i spróbowali namierzyć czarodzieja by go przesłuchać i dowiedzieć się o co chodzi. I czy to kolejny cholerny Diakon. Troszkę czasu to zajęło... Mniej więcej tak proste jak kłąb sznurka w kieszeni. W międzyczasie SŚ dostało zapytanie odnośnie procedur co zrobić z Nikolą w takiej sytuacji, od Dalii (Bolesław nie jest w SŚ najbardziej popularny).

17:30, spotkanie Rafaela i Pauliny. Już na samym początku Rafael poinformował Paulinę, że skutecznie on i Bolesław Derwisz się zaszachowali. Żaden z nich nie może zrobić żadnego ruchu, nawet mniej sensownego. Rafael nie jest w stanie nawet wyszukać Draceny. Z drugiej strony, Bolesław nie może Dracenie przeszkadzać ani wpłynąć na egzamin. Nikola jednak jest poza wpływami kogokolwiek. To powoduje, że bardzo istotną osobą staje się Paulina. Jeśli nie dołączy do żadnej gildii, jej opinia się nie liczy, gildie mogą decydować o losie Nikoli po swojemu. Jednak jeśli do którejś dołączy, to jako "pierwsza która znalazła" pojawia się silny argument dla tej gildii. Tu Rafael zaproponował Paulinie przystąpienie do Millennium na dwa tygodnie. Na pytanie czemu tylko tyle odpowiedź brzmi, bo jej się więcej nie spodoba. Ale dzięki temu będzie dało się znaleźć właściwego mentora dla Nikoli. Kogoś, kto będzie mógł zapewnić, że Nikola będzie jak w domu. I otoczyć jej rodzinę ochroną Millennium.

Paulina powiedziała, że gdzieś w okolicy kręci się tajemnicza piękna kobieta. Po podaniu rysopisu Rafael facepalmował. Zidentyfikował ją jako viciniusa, Wiłę, agentkę Silgora Diakona. Zapytany co ma Silgor do tego wszystkiego Rafael kazał Paulinie obiecać, że Paulina nie będzie przekonywać do niczego Draceny i jej wszystkie interakcje z Draceną (w czasie egzaminu) będą na zasadzie "bo Dracena poprosiła". Jak Paulina obiecała, Rafael wyjaśnił o co chodzi. Silgor jest kolekcjonerem, który dodatkowo twierdzi, że nie mają czasu ani możliwości czekania na fanaberie Draceny. Dracena nie może "ja siama". Na przykład Nikola - Dracena powinna była zapytać. Ale ona nie pyta. Ona wszystko chce sama. I Silgor chce zapewnić, żeby sytuacja stała się tak niebezpieczna, żeby Dracena MUSIAŁA poprosić o pomoc. Nieważne kogo - Rafaela, Silgora, Paulinę, Święcę... ale nie może działać samotnie.

Niestety, Dracena dała bardzo potężną broń Silgorowi do ręki. Nie zgłosiła Nikoli jako czarodziejki. A Millennium ma bardzo mało magów. To spowodowało, że Rafael nie jest w stanie zrobić wielu rzeczy. A teraz jeszcze przy blokadach ze Świecy, Rafael naprawdę jest związany a Silgor, o którym nikt nie wie ma swobodę działania. Paulina zaczęła bronić Dracenę, że ona bierze nie zgłoszenie maga na siebie; Rafael jednak zauważył, że nikogo to nie interesuje. Paulina nie miała obowiązku zgłoszenia maga. Dracena tak.

Paulina dodała, że przecież czarodziejka SŚ EAM zrobiła to Nikoli i nie zgłosiła - czy w jakiś sposób zmienia to sytuację? Tak. Rafael jest w stanie jakoś to wykorzystać. Paulina zauważyła, że Millennium i Świeca to nie są opcje które są tak naprawdę idealne. Obie mają "złe" podejścia do ludzi. Rafael potwierdził, lecz zaznaczył, że Paulina nie do końca ma inne opcje. Maria się na soullinku uśmiechnęła. Oczywiście, że jest trzecia opcja. Paulina przesłała jeszcze sygnał do Rafaela pokazujący maga koło domu Nikoli. Rafael potwierdził - to jest Silgor Diakon. Kolekcjoner. Paulina zapytała Rafaela czemu nazywa Silgora "kolekcjonerem" - Silgor Diakon posiada kolekcję którą bardzo delikatnie traktuje, ludzi, magów i viciniusów. Elementy jego kolekcji są zawsze szczęśliwe. Przedmiotowe, ale szczęśliwe. Jeśli odrzuci się swoją autonomię i niezależność, jeśli chce się iść przez życie będąc pod troskliwą opieką, Silgor Diakon jest idealny.
Maria została przez Paulinę ostrzeżona odnośnie Silgora Diakona i wiły. Jest tu jeszcze jeden mag, kolekcjoner, a Rafael jest zdjęty z akcji z przyczyn politycznych.
Maria ślicznie podziękowała.

18:52. Paulina skończyła rozmowę z Rafaelem. Wróciła do pokoju. W sam raz na obudzenie się Nikoli. Paulina delikatnie wyjaśniła jej, że Nikola jest czymś więcej. Nie powiedziała "mag", ale powiedziała, że są pewne opcje które Nikola może rozważyć. Nikola zaczęła panikować, ale Paulina ją uspokoiła (konflikt autosukces). Paulina poprosiła, by Nikola zachowała spokój. Ona ją ochroni. Paulina dała Nikoli swój numer telefonu. Jakby Nikola kiedyś chciała porozmawiać, może po prostu zadzwonić.
Rozmowę przerwało ciche pukanie do drzwi. Paulina wyszła. Samotna Dalia Weiner.
Dalia chciała wejść do środka. Chce pomóc Nikoli, sprawdzić czy wszystko w porządku. Nie chce jej straszyć, chce uspokoić, zrobić elementy puryfikacji. A może pomóc, bo w tym się specjalizuje (w leczeniu bardzo skażonych magów). (7+2 vs 7 -> 9) Paulina jednak nie wpuściła jej do pacjentki tylko tak twardo weszła, że wysłała ją do domu Nikoli, by tam puryfikowała i zajęła się działaniami by Nikola, gdy wróci, miała możliwość przebywania w czystym, nie Skażonym środowisku.
Nikola dała znać rodzicom, że będzie nocować u Pauliny.

19:24. Paulina zdecydowała się spędzić wieczór z Nikolą. Wyjaśnić jej podstawowe rzeczy. Wyjaśnić, że to co się z nią dzieje, że to poczuła co poczuła to oznacza, że ma dar. Podała jej fakty, że to wskazuje, że ona może być czarodziejką. Wyjaśniła koncept maga. Bardzo wyraźnie zaznaczyła, że Nikola nie kontroluje swojej mocy, nie wie jak sobie z nią poradzić i wymaga ogromnej ilości cierpliwości, pracy i pomocy. Wyjaśnia, że Nikola była chora i potrzebuje leczenia (i Paulina leczy). Gdy Nikola zapytała o Sebastiana, czy on też leczył Paulina odpowiedziała, że Sebastian nie próbował pomóc Nikoli, ale był pod wpływem kogoś innego. Sebastian jej nie kocha. To czarny charakter. Ale to nie do końca był jego wybór. Już jej nie kocha, ale dzięki temu ją mógł ochronić.
Paulina była bardzo delikatna, ale Nikola potrzebowała dużo ciepła i miłości.

21:44. Komunikat od Marii. Zlokalizowała miejsce, w którym mieszka Silgor Diakon ze swoją wiłą. A to było tak: na komendzie jest jeszcze jeden policjant, Bartosz Bławatek. I ów Bławatek dostał wiadomość od Ireny Kryszniok, tej recepcjonistki, że w mieszkaniu jest agencja towarzyska. Bo widziała jak wiła chodzi nago po mieszkaniu (mag nie zadbał o firanki). No a tak się składa, że to mieszkanie należy do Franciszka Marlina - człowieka, który jest powiązany z lokalną bieda-mafią. No i Kurczak i Bławatek chcą mu dokopać. A tak się składa że chyba jeden jedyny uczciwy interes jaki Marlin zrobił to było właśnie wynajęcie mieszkania. Ale... Silgorowi. No to Bławatek wpadł i zobaczył że wszystko w porządku. ALE. Zauważył tam zdjęcia Nikoli i Draceny. Jako że Dracena wygląda nietypowo a Nikola chodzi do klasy z jego córką to powiedział o tym Kurczakowi. A Artur Kurczak powiedział o tym Marii. I mają lokalizację.
Paulina nie może się stąd ruszyć - ma Nikolę, którą musi się zająć. Ale poprosiła Marię, by ta coś zrobiła, by na jakiś czas wsadzić ich do aresztu.
Maria coś wymyśliła - skoro już jest precedens że agencja towarzyska, to trzeba przede wszystkim zdobyć więcej świadków. Wiła będzie chodzić nago, bo wiła. Wystarczy żeby dzieciaki z okolicznych bloków wiedziały gdzie patrzeć i mamy paragraf na deprawację nieletnich. Wystarczy, że będą rozdawane ulotki o agencji towarzyskiej, wystarczy że zaczną przychodzić klienci i się dobijać. Powinno być to wystarczającym pretekstem, by coś z tym zrobić. A fakt kto wynajął mieszkanie jedynie ułatwia sprawę.
Paulina była gotowa Marię wyściskać.

23:41, Dracena postanowiła spróbować się naprawić. Mimo, że Paulina jest magicznym lekarzem. Zwłaszcza, że w mieście są terminusi. Przygotowała odpowiednie zaklęcie rytualne i spróbowała się naprawić i uodpornić na wpływy Janka Bociana. Niestety, nie tylko jej nie wyszło - osiągnęła efekt Paradoksalny a z uwagi na tryb Wspomagania na stałe zmieniła swój wzór. Dracena nie jest świadoma Paradoksu; myśli, że osiągnęła Soczewkę. Wie jednak o Skażeniu. Decyduje się na konfrontację z Janem Bocianem jak najszybciej a sama kontaktuje się z Pauliną prosząc o rozwiązanie problemu Skażenia. Podaje adres i prosi o łagodne potraktowanie Pawła Brokotego - on jej bardzo pomógł, gdy nie udało jej się pierwsze podejście do przeciwnika.

23:57. Paulina nadal jest z Nikolą. Ta nie może spać, ale Paulina ją uspokaja. I nagle, Paulina dostaje telefon. Dracena.
Dracena powiedziała Paulinie, że trochę narozrabiała. Podeszła do rozwiązania problemu na egzaminie, odbiła się, musiała wylizać rany. I niestety, wyszło jej Soczewkowe Skażenie. Poprosiła Paulinę, żeby ta wyczyściła po Dracenie, bo "ten chłopak nie zasłużył na to, żeby mieć Skażone mieszkanie". Paulina powiedziała, że ona nie może tego zrobić bo Nikola. Ale że zna kogoś i może poprosić - ale musi obciążyć kosztami (kogoś ze Świecy). Dracena się zgodziła.
Paulina zadzwoniła do Dalii Weiner i ją obudziła. Dalia zachowała się kulturalnie. Wpierw zmartwiła się, że coś się stało z pacjentką, ale nie. Paulina powiedziała Dalii, że doszło do Skażenia na linii katalizy i lifeshape a ona niestety nie może iść tego rozwiązać. Poprosiła Dalię o to, żeby ona to zrobiła. Oczywiście, zapłaci.
Dalia odpowiedziała, że się tym zajmie.
Dalia nie chciała budzić Bolesława; wie, że ten bardzo nerwowo reaguje na Diakonów a sama Dalia podejrzewa, że to ma coś z Diakonami wspólnego. W ten sposób Dalia wpakowała się i poszła sama do miejsca, gdzie doszło do Paradoksalnego Skażenia. Kość szczęścia: 4/k20.

00:41. Dracena poszła do domu Ryszarda Bociana. Lekarz otworzył i pomny słowom wiły wpuścił Dracenę do środka. Jeszcze raz poprosił ją, by zostawiła jego syna w spokoju. Dracena na to, że nie może. Musi to skończyć i wtedy zniknie z ich życia. Lekarz wrócił do łóżka i już nie spał całą noc, jedynie wszystko słyszał.
Jak tylko Dracena weszła do pokoju Janka, chłopak się obudził. Ku swemu przerażeniu Dracena odkryła, że w rzeczywistości jej Soczewka była Paradoksem. Nie tylko reaguje na Janka bardzo mocno ale i nie jest w stanie mu niczego odmówić. A Janek to wie. Jest w stanie przewidywać ruchy i reakcje Draceny. Do tego Dracena nie jest w stanie zrobić niczego, co może Jankowi w jakikolwiek sposób zaszkodzić. I jest szczęśliwa.
Ojciec chłopaka nie spał tej nocy...
Janek w sumie też nie...
A Rafael nic nie wie.

Tymczasem Dalia Weiner podeszła do tematu z właściwą dla siebie ostrożnością i lękliwością. Jedynie to ją uratowało; nie wiedziała, że w epicentrum Skażenia był Paweł Brokoty. Nie wiedziała, że Paradoks Draceny go przekształcił. Dalia weszła na teren Skażenia jak zwykle oddzielając je od reszty i rozplatając skomplikowane sieci magiczne, gdy nagle zauważyła szybki ruch. Dalia, nie mająca instynktu terminusa spanikowała i puściła w Skażeńca czystą energię magiczną, która poparzyła potwora. Ten jednak zdołał jej wstrzyknąć truciznę. Charakterystycznego dla Diakonów typu.
Szczęśliwie dla Dalii, miała postawione zaklęcia samoleczące i samopuryfikujące. Do niczego nie doszło, choć było blisko. Resztką sił Dalia dała radę przekształcić energię magiczną doprowadzając do niewielkiego wybuchu energii uszkadzając meble w mieszkaniu i pękając szyby. Skażeniec padł, nieprzytomny i Dalia tak samo.
W wyniku Dalia jest dość, hm, pobudzona przez najbliższy czas i emanuje widocznym Skażeniem co zdecydowanie podnosi jej próg Paradoksu. Jednak Pawła uda się uratować. 
Dalia nie dała rady nacisnąć sygnału SOS.
Padła nieprzytomna.


# Lokalizacje:

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Leere
                1. Wykop Mały
                    1. Osiedle Ludowe
                        1. domki, gdzie mieszka min. Nikola Kamień i Jan Bocian
                        1. familoki, gdzie znajduje się mieszkanie Brokotych i gdzie Skażona została Dracena
                        1. hostel Opatrzność, gdzie pokoje mają zarówno Paulina jak i Dracena
                        1. Kafejka U Sępa

# Zasługi

* mag: Paulina Tarczyńska, robiąca co może, aby dać Nikoli pełny wybór i w miarę możliwości chroniąca Dracenę.
* czł: Maria Newa jako soullinkowana reporterka z kontaktami pracująca po nocy gdy Paulina śpi.
* mag: Dracena Diakon jako bardzo rozkojarzona, zakochana cybergothka walcząca rozpaczliwie o swoją przyszłość.
* mag: Rafael Diakon jako mag EAM z Millennium strasznie nie chcący robić absolutnie niczego poza obserwowaniem. Plus, próbuje uroczo intrygować.
* mag: Bolesław Derwisz jako Diakonożerczy terminus*mentalista chcący pomóc ludziom i pognębić wszystko co Diakonoidalne.
* mag: Dalia Weiner jako magiczny lekarz, puryfikatorka, mentalistka która nadrabia entuzjazmem całkowity brak doświadczenia na akcjach (i wrodzoną lękliwość).
* mag: Silgor Diakon jako pewny siebie kolekcjoner który zawsze wiedząc lepiej chce wykorzystać okazję.
* ???: Nikola Kamień jako potencjalna mag/człowiek/vicinius/źródło losowego Skażenia będąca jeszcze przedmiotem a potencjalnie podmiotem.
* vic: Dyta jako piękny element kolekcji Silgora i bardzo niebezpieczna wiła, oddana swemu mistrzowi ponad wszystko.
* czł: Jan Bocian jako nastolatek szczęśliwie zdolny do uwiedzenia każdej dziewczyny i nieszczęśliwie każdego faceta też.
* czł: Ryszard Bocian jako lekarz wykorzystany przez Dracenę. A potem Silgora. Szczęśliwie w nie ten sam sposób.
* czł: Irena Krysniok jako zdeterminowana recepcjonistka hostelu "Opatrzność" z nadmiarem energii i waleczności, która nie jest fanką młodych piersi w oknie.
* czł: Artur Kurczak jako policjant, który potrafi wykonywać swoją pracę.
* czł: Bartosz Bławatek jako policjant, który kolegując się z Arturem nie docenia jego znajomości z reporterką.
* czł: Katarzyna Trzosek jako gothka ze szkoły Nikoli która trochę za bardzo uwielbia Jasia Bociana.
* czł: Edmund Marlin, Mikołaj Mykot, Piotr jako trzech kumpli porządnie przesłuchanych przez Artura Kurczaka i Marię.
* czł: Franciszek Marlin jako człowiek mający dość sporo na sumieniu który jeden jedyny raz wynajął mieszkanie uczciwie. Sęk w tym, że Silgorowi.
* czł: Paweł Brokoty jako fan DJ Draceny i pomocnik w "Klimacie Nocy", który pomaga jej w trudnych chwilach... za co płaci wysoką cenę.
* czł: Jan Fiołek jako snajper wezwany przez Marię, by uratować sytuację. Chwilowo wystepuje jako głos w telefonie Marii.
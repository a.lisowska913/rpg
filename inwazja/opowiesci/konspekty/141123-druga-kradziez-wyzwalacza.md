---
layout: inwazja-konspekt
title:  "Druga kradzież wyzwalacza"
campaign: powrot-karradraela
gm: żółw
players: kić, varether
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [141115 - GS "Aegis" 0002 (SD, PS)](141115-gs-aegis-0002.html)

### Chronologiczna

* [141115 - GS "Aegis" 0002 (SD, PS)](141115-gs-aegis-0002.html)

## Kontekst ogólny sytuacji

## Punkt zerowy:

## Misja właściwa:

Eryk Płomień. Cyborg. Chwilowo z nie do końca sprawnym działaniem - wrócił właśnie z frontu walki ze Spustoszeniem. Niestety, został Spustoszony (przez miślęgi) i niedawno został zregenerowany przez KADEM. To powoduje, że nie wszystko pamięta i nie wszystkiego jeszcze kojarzy. Częściowa amnezja.

Eryk pracował nad jednym ze swoich pet projects, gdy nagle otrzymał low-level distress signal. Sygnał dochodzi z KADEMu, z Instytutu Technomancji. Eryk wzmocnił sygnał i spróbował go przeanalizować (8v9->12). Okazało się, że to sygnał idący na kanale używanym do komunikacji z głowicami. Sygnał typu "sos". Eryk poszedł w kierunku na Instytut Technomancji skąd dochodzi sygnał i natknął się na informację, że jest tam w jednej sali debata nt. przyszłości projektu Aegis, min. zniszczenia głowic.
Kolejny distress signal wskazał wyraźnie na to, że sygnał dochodzi z tamtej sali. Na sali jest Janek Wątły, Ilona Amant i Quasar. 

Instytut Technomancji. Eryk wszedł i powiedział, że nie dostał zaproszenia. Wszyscy się zdziwili, a Eryk wyjaśnił, że on był ostatnim kontrolerem 00003 i logicznym jest, by był osobą zaproszoną jak mówi się o przyszłości głowicy jaką on zna. Eryk upewnił się jeszcze jak chodzi o triangulację - tak, 00003 wysłała distress signal. Świetnie, wykazuje wysoki poziom autonomii, coś, co nie powinno nastąpić. Eryk jest świadomy że jakby to powiedział, los 00003 jest przesądzony.
Eryk powiedział, że nie wiadomo w jakim stopniu Spustoszenie a w jakim programowanie wpłynęło na działania 00002. Quasar się nie zgodziła, ale wezwała świadka - Sabinę.

Sabina i Eryk zaczęli rozmawiać o sytuacji - Eryk dowiedział się, że Janek wysłał Sabinę i Pawła do akcji, do lasu, i Sabina powiedziała jak Paweł działem uszkodził mocno 00002. Eryk zapytał Janka ile czasu minęło od momentu utraty przez Mariana Łajdaka kontroli nad 00002 a wejścia 00002 do akcji - parę godzin. Eryk zaznaczył, że 00002 w ciągu kilku godzin dała radę wyewoluować pochodnię plazmową i broń ostrą. Janek się zastanowił. To ciekawe.

Eryk zapytał jeszcze jakie były kroki 00002. Ilona wyjaśniła, że podczas walki ze własnym Spustoszeniem 00002 dała radę złapać i unieszkodliwić dwóch ludzi oraz Mariana Łajdaka, potem postawiła działka obronne by uniemożliwić Spustoszeniu dotarcie do Mariana i ludzi. Sabina próbowała bronić 00002, ale zaznaczała uczciwie co tam się działo. Dodała potem o barierze i zaznaczyła że 00002 obroniła Sabinę przed Spustoszeniem po czym uderzyła łamiąc Maskaradę. Dodała też, że 00002 odrzuciła Spustoszoną część i ją spopieliła.

Potem powiedziała o ostatniej akcji. Eryk wie już jak wygląda sytuacja.
Co się stało z 00001? 00001 miała uszkodzone połączenie - każdy kontroler łączący się z głowicą wpadał po kilku minutach w amok. Więc zaprojektowano broń strumieniową której celem było zniszczenie głowicy typu Aegis. A 00003? 00003 jest poprawnym modelem. Nie ma w niej niczego błędnego.
Eryk spróbował podsumować. Quasar zauważyła, że przecież 00002 złamała Maskaradę. Przecież 00002 nie miała takiej możliwości - jest to wyraźne złamanie dyrektywy programowania. Niebezpieczne, że potrafiła to zrobić. Eryk zauważył, że był race condition - chronić maga KADEMu vs Maskarada.
Sabina dodała, że mogła pojawić się forma inteligencji / osobowości.

Oki. Werdykt Quasar. 00003 nie zostaje zniszczona. Kontrolerem 00003 zostaje Eryk. Drugim kontrolerem (zapasowym) będzie Sabina. Ale - istnieje ryzyko, że ludzki komponent skaził programowanie głowicy. W związku z tym jedynie Eryk ma dostęp do 00003 dopóki Ilona nie upewni się, że to nie jest problem. Eryk ma bana na misje powiązane ze Spustoszeniem a Sabina jest oddelegowana do pomocy Ilonie na czas nieokreślony.

Eryk poprosił do swojego pokoju Ilonę i Sabinę. Eryk ekranował wszystko przed magami KADEMu i pokazał im, że 00003 samoistnie wysłała sygnał SOS. Ilona się zmartwiła. Ona sama się do czegoś przyznała - nie była w stanie stworzyć AI takiej klasy, by zadziałało bezbłędnie w interakcji z super złożonym systemem GS, więc posiłkowała się AI jakie zostawił im lord Jakub Urbanek. Posiłkowała się EIS. Sabina się zdziwiła - czy w głowicach znajduje się EIS (która w końcu wpadła w berserk?) Nie, zupełnie nie tak. Ale na bazie programowania EIS powstało AI które działa w głowicach, więc Ilona sama nie do końca wie, jak działa głowica i jej programowanie. Ale EIS słynęła z umiejętności nieprawdopodobnej adaptacji.
Ilona dała zadanie Sabinie - zlokalizować i zapewnić współpracę ze strony Mariana Welkrata - maga Powiewu Świeżości który współpracował z Aurelią Maus i współuczestniczył w tworzeniu EIS. Bo w EIS jest dużo więcej niż wynika z samego programowania, struktury i obwodów.
Ale jeszcze nie teraz.

Trzy godziny później Sabina i Eryk zostali wezwani do Quasar. Mają maga-renegata który wykradł eksperymentalny artefakt pryzmaturgiczny. Nie działa perfekcyjnie i w trakcie testów. Artefakt ten ma jedną zasadniczą własność - on manifestuje pryzmat. Czyli: gdyby wielu ludzi uwierzyło, że pali się w teatrze, teatr by zaczął się palić. Artefakt ten jest dość niestabilny. Quasar podejrzewa Millennium - Lucjan Kopidół mówił coś wcześniej o "sympatycznej Diakonce która zobaczyła jaki naprawdę jest a nie jak wygląda". Ma żonę i jedno dziecko.
Zadanie - odzyskać artefakt. Przechwycić Lucjana. Lepiej, żeby był żywy. Śmierć jest akceptowalna.
Wieczór. Działał w Instytucie Pryzmaturgii. 
Aha, i Eryk jest autoryzowany do wykorzystania 00003 na akcję.
Sabina dostała nowe zadanie od Quasar - upewnić się, że Millennium nie będzie chciało otrzymać tego artefaktu. Pryzmatycznego Wyzwalacza. Bo Millennium nie chce wojny totalnej, prawda?

Sabina poszła wywiedzieć się znajomych Lucjana - znalazła Mariusza. Poobserwowała go chwilę w kantynie KADEMu - wesoły, niski mag z poczuciem humoru. Nigdy nie siedzi sam - towarzyski typ. Sabina wykorzystała swój status maskotki i wkręciła się do stolika a jak tylko kolacja się zaczęła kończyć, poszła za nim i poczekała aż zostaną sam-na-sam. Mariusz wyjął różę z "pięknej główki" Sabiny jako świetny żart. Sabina roześmiała się uprzejmie. Powiedziała, że ma problem - gdy śpi, rzuca czary i one się źle kończą. Mariusz powiedział Sabinie, że jego kolega może jej pomóc - Lucjan, jego emo kolega. Sabina zaczęła uroczo się wdzięczyć (8v5->6) i pokazała Mariuszowi, że zależy jej na Lucjanie. Ten nie wytrzymał - powiedział, że zawsze pisał emo poezję że nikt nigdy nie widzi jego prawdziwego wnętrza, dziewczyny się nim nie interesowały (ma żonę, ale zawsze wzdychał do studentek) a tu nagle nie jedna kochanka a dwie!
Sabina pociągnęła temat. Zbliżyła się bardziej do Mariusza. Mariusz powiedział, że przecież istnieje regulacja Marty że mag KADEMu nie powinien mieć rodziny poza obszarem KADEMu. A tu nagle pojawiła się kochanka, "Helena Diakon". Bardziej pociągnięty za język, powiedział, że poznali się w "Zaćmionym Sercu" (klub kiedyś założony przez Warmastera).

Instytut Pryzmaturgii. Eryk poszedł do sariath Aleksandry Trawens. Aleksandra powiedziała, że zidentyfikowała w magii, że on ucieka. Powiedziała też że wie, że artefakt będzie lub był użyty. Widziała scenę - w świetle neonów i wirujących świetlnych kul Spustoszeni tańczą z ludźmi (Aleksandra - mag chaosu - intuicja). Zdaniem Aleksandry nie ma to sensu.
Eryk spytał czy Aleksandra może powiedzieć mu coś na temat tego maga. Dała mu teczkę. On przeszukał teczkę pod kątem wzorów (8v6->7) i znalazł grupę wzorcowych wierszy na przestrzeni czasu które dają obraz jego stanu psychicznego. Ostatni wiersz jest z wczoraj.

Eryk zapytał Aleksandrę o jakieś napromieniowanie pozwalające mu na zidentyfikowanie go. Aleksandra powiedziała że nie ma nic takiego, ale każdy mag wchodzący i wychodzący z KADEMu ma regularne skany aury itp. I może je dostarczyć Erykowi. Eryk je dostał. Szuka wzoru w aurach, szuka zmian. Na przestrzeni czasu. Przez rok. (12v7/9/11->10) Eryk skanuje wzór aury i porównuje je z wierszami. 2 tygodnie temu Lucjan zdobył kochankę. Jest w wierszach, nie w aurze. 10 dni temu aura zaczęła się delikatnie zmieniać. Delikatnie. W wierszach nic się nie zmieniło. 6 dni temu aura ciągle się przesuwa, ale wiersze się zmieniają - Lucjan zaczyna się gubić. Traci osobowość, gubi się w sobie. Wiersze stają się słowami, przestaje "czuć". Wiersze coraz bardziej degenerują. Do teraz. Ostatni wiersz jest już całkiem "pusty". Lucjan "zaginął". W aurze nie ma wpływu magii.

Synchronizacja Eryk - Sabina. Eryk wezwał też 00003 w trybie kamuflażu. Sabina chce zrobić sense motive i analizę psychologiczną na wierszach jakie tu się pojawiły. Chce sprawdzić, czy to techniki Diakonów (true Diakon) i jaki może być motyw zabrania przez niego tego artefaktu. (8#1v9/11->11) To nie jest sposób działania Millennium. Millennium działa inaczej, bardziej subtelnie. To w ogóle nie wygląda na działania ani Świecy ani Millennium. To może być działanie jakiejś małej grupki w Świecy czy Millennium, ale jak i skąd? Analiza wierszy wskazuje, że on się gubi, że zanika. Najpewniej zabrał artefakt, bo... ktoś mu kazał.

Przy tych danych Eryk podłączył się do systemów KADEMu i poszukał informacji na temat takiej zmiany aury i zmian psychologicznych. Dostał odpowiedź - jad Dromopod Iserat. Hipnotyczne skorpiony, które hodować potrafi jedynie ród Myszeczków. To zdaniem Sabiny wyklucza Millennium. Na pewno Millennium nie posiada Dromopod Iserat. Ale sposób działania nie wskazuje na Srebrną Świecę. To jedynie upewnia Sabinę. Nie idzie do Millennium. Nie ma mowy. Jakby Millennium się dowiedziało, że coś takiego zginęło, sami zaczną szukać...

Eryk wrócił na KADEM. Poszedł do Janka i poprosił o coś do wyłączenia 00003. Janek powiedział, że nic nie mają takiego. Dlatego działo strumieniowe. Janek powiedział Erykowi, że teoretycznie może dać mu coś co wyłączy GS, ale w praktyce Janek podejrzewa po tym co działo się z 00001, że GS "będzie udawać" wyłączenie. GS adaptują niesamowicie szybko (Seton # Seton). I niestety Janek nie ma pojęcia jak to zatrzymać, wyłączyć czy bezpiecznie kontrolować. W związku z czym zaproponował Erykowi minę. Wielkości 6 pięści. Taką do umieszczenia na kadłubie 00003 i odpalaną przez Eryka zdalnie (działa jak działo strumieniowe ale mniejszy dystans).
Erykowi to wystarczy. Może zabrać 00003 do miejsca, gdzie będą magowie oraz ludzie (i być może jakiś Spustoszony). A przynajmniej, z większą pewnością.

Sabina powiedziała Erykowi o Dracenie i o tym, że Antygona jest czarodziejką SŚ a Dracena czarodziejką Millennium. Parsując wizję Sariath Trawens, Eryk uznał, że najpewniej do odpalenia artefaktu dojdzie w klubie "Działo Plazmowe". A - niespodzianka - głównym DJem w "Dziale Plazmowym" jest Dracena. Ten sam mag KADEMu (Lucjan Kopidół), ten sam artefakt (Wyzwalacz Pryzmaturgiczny), ten sam klub ("Działo Plazmowe"). Absurd. Po prostu absurd.
Sabina powiedziała Erykowi, że w tym jest coś więcej, ale nie może mu powiedzieć więcej. Powiedziała o siostrach (Dracena/Antygona), że są podejrzane, choć nie DLACZEGO. Eryk powiedział, że musi wiedzieć czemu są podejrzane a Sabina - "źródło nie ma znaczenia". Eryk widzi, że Sabina mu nie ufa. Widzi, że to najpewniej powiązane jest jakoś z rodem Diakon.

Oki, Eryk poszedł znowu do Janka (i wysłał za Sabiną Diakon małą sondę - drona z livefeedem by ją śledzić) (10->12). Poprosił go o zbudowanie działa strumieniowego z Czarnym Diamentem. Takie w stylu "ręki Shockwave'a". Janek zaznaczył że zbudowanie takiej ręki trochę potrwa - jak się uda, Eryk będzie miał wmontowane działo strumieniowe jako broń w swoje ciało. Takie tam "awaryjne" jakby GS 00003 odbiło. Janek zaznaczył, że musi do tego zmodyfikować Erykowi zasilanie. Nie spieszy się. Do tego Janek dla Eryka ma zmienić VISOR w wózek inwalidzki. Taki by Eryk mógł siedzieć na VISORze i by miał stały live feed.
Janek stwierdził, że musi dać zlecenie młodym na zbudowanie nowego VISORa, bo zwyczajnie ten projekt jest rozchwytywany.

Sabina poszła do Mikado porozmawiać odnośnie Draceny. Nie zauważyła że jest obserwowana przez drona Eryka (4v12->2). Chce dowiedzieć się od Mikado co wie na temat ruchów Draceny. Mikado powiedział Sabinie, że problem z Marianem Łajdakiem został rozwiązany - mag NIE jest już zakochany w Dracenie. Dostał polecenie. Sabina chce dowiedzieć się od niego co porabia Dracena. Mikado od razu włączył się KRĘCICIEL MODE, więc Sabina próbuje go przekonać by jej pomógł (13v10->13). Dracena raz już próbowała coś zrobić, Sabina nie chce iść do Draconisa by go ochronić. Na razie sprawa zostaje na poziomie plebsu. Ale jak Dracena jest z tym powiązana, najpewniej przechlapane mają: Draconis, Dracena, Sabina i Millennium i KADEM. Mikado poszedł po rozum do głowy.
Zdaniem Mikado to na pewno nie Dracena - Dracena została wysłana na front Spustoszenia. Mikado zupełnie nie wie dlaczego, bo Dracena się nie nadaje do walki ze Spustoszeniem... ani nawet chihuahuą. Ale na pewno jej tu nie ma w tej chwili. Antygona tak samo za tym nie powinna stać - ma aktualnie sporo problemów w Świecy, plus, po ostatniej akcji się pożarły ze sobą. Ale Mikado nie wie gdzie znajduje się Antygona. To coś w Świecy.

Kolejnym krokiem Sabiny było udanie się do kontaktu w Świecy. Tadeusz Baran. Terminus SŚ, który obsypywał ją prezentami licząc, że zaciągnie do łóżka. Baran jest z siebie taki dumny, bo mu się udało. Sabina jest z siebie dumna, bo terminus nie zauważył, że to Sabina polowała na Tadeusza Barana. Spotkanie w jakiejś kawiarence. Baran TEŻ nie zauważył drona obserwacyjnego. Sabina porozmawiała z nim na temat Antygony. Tadeusz Baran umiał odtworzyć ostatnie działania czarodziejki: wpierw prosiła go o pomoc w jakiejś sprawie, ale Baran odmówił, bo się nie miesza w politykę. Potem weszła Iza Łaniewska. Potem uderzyło Spustoszenie. Potem Antygona zniknęła. Potem wróciła i zaczęła przypochlebiać się Wiktorowi Sowińskiemu. I to zupełnie nie pasuje Sabinie. Ponieważ Wiktor Sowiński to stosunkowo młody (32 lata) arystokrata w stylu "pan każe sługa musi". Otacza się świtą, nie tylko z SŚ. Znaczący i surowy mag który swoich nagradza a innych surowo karze. I Antygona NIJAK tu nie pasuje do wzoru.
Antygona w tej chwili jest w Kopalinie.

Eryk usłyszał to co potrzebował. Zadzwonił do Sabiny. Powiedział jej, że chce spędzić trochę czasu w "Dziale Plazmowym". Zdecydował się, że bierze ze sobą. GS "Aegis" 00003 idzie z Erykiem. Eryk montuje minę na GS "Aegis" 00003 i informuje, że montuje minę na czas ewaluacji. "Awaryjne wyłączenie". Eryk poczuł na sobie spojrzenie GS, ale głowica niczego nie zrobiła. Jako, że Eryk uruchomił to raz by sprawdzić czy działa (i natychmiast wyłączył), natychmiast zmienił sposób komunikacji z miną. By głowica nie była w stanie się adaptować. Eryk założył optymistycznie, że GS "Aegis" działa na takiej zasadzie: jedna metoda zadziała dokładnie raz. Zakłada paranoicznie, że 00003 wie wszystko co 00001 i 00002.

Sabina poprosiła Barana, by ten przyjrzał się Antygonie. Nie ma ingerować, ma jedynie obserwować. W skrócie: Tadeusz Baran ma być Sabinowym systemem wczesnego ostrzegania odnośnie Antygony i działań Antygony. Sabina poszła szybko do "Działa plazmowego".
Impreza jeszcze się nie zaczęła, ale zbierają się ludzie. Następne trzy dni prowadzi "DJ Elektron". Zarówno przy wejściu zwykłym i ewakuacyjnym Eryk ustawił małe, dyskretne kamerki mające łączyć się z nim i łączyć się z VISORem by w czasie rzeczywistym zdobywać info kto jest w klubie, czy magowie czy ludzie i by móc w dowolnym momencie złapać dowolną osobę. Eryk zauważył, że GS "Aegis" 00003 jest bardzo skupiona na VISORze. Pełna uwaga. Eryk dał imię głowicy - "Nihilus". Kazał zamaskować się 00003 jako dziewczyna. Drobna, szczupła blondynka reagująca na imię Nihilus. Nihilus i Eryk stoczyli ciekawą rozmowę o VISORze, po czym Nihilus stwierdziła, że bardzo podoba jej się to imię. Kojarzy się z nihilizmem - brakiem nadziei i przyszłości. Eryk docenił.

Sam klub "Działo plazmowe" jest sformowany w stylu: stoliki i duża przestrzeń do tańczenia. W klimatach postapo. Różne światła, sztuczne mgły, zdemolowane auto (prosto ze złomowiska), głośniki... aranżacja wnętrza nie była tania. Przygotowują się do SaberThrone. Sabina dotarła do "Działa plazmowego". Nihilus się przedstawiła, co nieco zdeprymowało Sabinę. Ok. Mają głowicę. Sabina powiedziała bezpieczną wersję (omijając niektóre szczegóły) Erykowi, po czym mają plan na kolejną rzecz.
Część schodzących się ludzi wygląda jak cyborgi i cybergoth. GS "Aegis" 0003 o dźwięcznym imieniu Nihilus połączyła się z VISORem i zaczęła wzmacniać swoje sensory. Po chwili konsternacji Eryk zapytał Nihilus, czy ona to robi (z jego punktu widzenia po prostu VISOR zaczął działać sam). Nihilus potwierdziła. Powiedziała, że tak jak oni naciskając klawisze wysyłają sygnały, Nihilus się zaadaptowała i też wysyła sygnały. Eryk nie zganił jej za inicjatywę. Pochwalił nawet.

Za godzinę będzie tu event. Impreza cybergoth. Tematem jest "gniew", "odrzucenie", "porzucenie". Eryk i Sabina zorientowali się, że na nich też zadziała pryzmat. Ale na głowicę - nie.

VISOR zlokalizował manifestację pryzmaturgiczną. W sklepie. Tesco, gdzie rzucili tanie laptopy. Mimo obiekcji Nihilus (która chciała zostać sama z VISORem) Sabina i Eryk się rozdzielili - Sabina została w klubie a Eryk wstał z wózka inwalidzkiego i pobiegł do Tesco. Stał się cud. Nihilus usiadła na wózku i holograficznie dostosowała swój wygląd. Nihilus poprosiła o pozwolenie adaptacji VISORa do siebie - transorganicznego wchłonięcia (a potem odłączenia), ale nigdy jeszcze nie odłączała niczego. Eryk odmówił. Za mało VISORów na KADEMie.

Na pytanie Sabiny czym dla 00003 są 00001 i 00002, Nihilus odpowiedziała, że to są klony. Nieudane klony. Zapytana czemu nieudane a 00003 jest udana, "ja wciąż istnieję. One nie". Sabina nie jest w stanie w tej chwili sprawdzić, czy Aegis posiada własną osobowość. Niestety. Choć Sabina podejrzewa, że jest tam coś więcej, ale nie ma pewności i nie chce ryzykować.

A sklep:

> frustration and tiredness in a mall: thats shopping, spending money, wanting stuff more and more stuff
> its always noisy but in a bad way you get a headache
> and you get really really annoyed with other people
> if you dont come by car then you carry your jacked coat hats etc with
> in the summer it can be too cold, cause the AC is always on
> and lots of music around, from different shops different music
> something might be on you hate, it makes it even worse
> and if you got kids with you, they need to wee, they are hungry, thirsty
> they can get lost now thats panic:)
> but generally you just get angry with others
> why are they there, why now
> and people really easily start to shout with others cause of this
> or when its sale - like the black friday before chirstmas, there are huuuge queues at the checkouts
> and people also hunt for bargains, throwing stuff around
> its a big wirling mess, hot and very tactile
> also there are smells
> some stink, but generally lots of parfumes mixing

Ludzie w sklepie są napędzani nienawiścią do innych, którzy stoją im na drodze do konsumpcji i strasznym pożądaniem i głodem wszystkiego. Ograniczona ilość ludzi, ograniczona ilość towarów, ludzie się biją o towary. Eryk bardzo rozsądnie zignorował pryzmaturgiczną efemerydę i podłaczył się do kamer sklepu. Zauważył jak do tego doszło - Lucjan Kopidół stał w kolejce, wyraźnie nie wie co robi i o co chodzi, jak kazali mu płacić to przypadkowo (?) uaktywnił artefakt.
Powstała efemeryda - kolejka. Składa się z masy ludzi nienawidzących i pożerających wszystko.

Następny ping - przedszkole. VISOR zaraportował że w przedszkolu doszło do manifestacji pryzmatu.
Głowica zgłosiła się na ochotnika pójścia do przedszkola. Została przegłosowana. NIE idzie do przedszkola.

Aleksandra Trawens życzyła sobie, żeby Sabina i Eryk przechwycili artefakt i Lucjana Kopidóła w klubie. Krótka rozmowa Eryka i Sabiny zaowocowała tym, że chyba posłuchają rozkazów, mimo, że da się określić gdzie Kopidół jest i gdzie pójdzie. Innymi słowy, da się określić gdzie będzie i da się zażegnać ponowne odpalenie artefaktu.
Plan Eryka i Sabiny jest następujący:
- Eryk rozwiązuje sklep.
- Sabina rozwiązuje przedszkole.
- Nihilus zostaje w klubie, jak zlokalizuje Kopidóła ma go nieletalnie ogłuszyć i przechwycić najszybciej jak się da. Przechwycić artefakt i dopilnować że nikt go nie zabierze. I za żadne skarby nie integrować go w siebie. Brak dostępu do broni letalnej, prawo do złamania Maskarady. Priorytet: nie dopuścić do odpalenia artefaktu.

Eryk połączył się z systemem nagłośnienia i powiedział, że przed sklepem znajdują się tanie produkty z najwyższym poziomem przeceny. Ludzie zaczęli wybiegać, tratując się trochę nawzajem, ale nikt nie zginął. Sklep się opróżnia, efemeryda się rozpada. Bilans: 5-6 rannych. KADEM poinformował SŚ, że zajmują się tym sklepem (choć nie wiedzą co i o co chodzi, ale co tam, wezmą coś na siebie ten jeden raz). KADEM rozbroi resztę. SŚ widząc, że poziom Skażenia maleje zostawiło KADEMowi sprawę. Bo to tylko ludzie.

Przedszkole. Efemeryda strachu i koszmarów. Wszędzie pająki, gargamel, buka, węże, efemeryda jako chodzący dorośli krzywdzący dzieci. Dzieci w pełnej panice. Sabina dostała linę od Eryka. Weszła i... nie ma ryzyka przejęcia Sabiny. Ta efemeryda jest dużo słabsza, bo tym razem Lucjan jedynie przechodził obok i dużo słabsza efemeryda się sformowała.
Sabina wchodzi do przedszkola i stara się otworzyć na efemerydę jednocześnie zachowując swoją osobowość i kontrolę. (7v5/7/9->8) Udało się to zrobić Sabinie, efemeryda "wpuściła" Sabinę do środka pryzmatu. Zła królowa. Sabina dostała rolę kontrolną nad innymi ludźmi. Teraz Sabina próbuje przekształcić to wszystko w bajkę o dobrym zakończeniu (Jaś i Małgosia, Narnia) by dzieci poczuły się bezpieczne. Sabina ustawiła sytuację tak: źli dorośli wyłapują dzieci a one trafiają do bezpiecznego miejsca z zabawkami i przygodą (10#1v9/11->9). Sabina wpadła w klincz z efemerydą. Rozmontuje ją. Ale za wolno. Więc Eryk ma trudny wybór - albo pomoże Sabinie (magowie Świecy nie dowiedzą się nigdy o Wyzwalaczu Pryzmaturgicznym) albo pomoże głowicy i będzie miał pewność, że głowica rozwiązała problem poprawnie.

Eryk się cofnął i pomógł Sabinie. Rozmontowali efemerydę i lecą do klubu. Po raz kolejny KADEM zajął się sprawą.

A w klubie - a dokładniej przed klubem - stoi Nihilus, VISOR w formie wózka inwalidzkiego, a na nim leży Lucjan Kopidół. Głowica opuściła klub, przechwyciła Lucjana kilkanaście metrów przed klubem używając wstrząsu elektrycznego i położyła go na VISORze. Zignorowała fakt, że ma to zrobić w klubie, bo to by było nielogiczne. Artefakt nie został uruchomiony.

Sariath Trawens powiedziała, że powinni złapać Lucjana w klubie. Zapytana dlaczego powiedziała, że mieliby okazję przechwycić osobę za tym stojącą. No trudno, najpewniej to była tylko płotka.

Sabina powiedziała Erykowi o tym, że to druga kradzież artefaktu. Ten sam mag, podobne okoliczności. Eryk powiedział, że to trzeba zgłosić. Być może można wynieść inny artefakt. Sabina powiedziała, że to zgłosi, jeśli znajdzie sposób jak to zgłosić bez wywoływania zbędnego konfliktu. Sabina bardzo chce uniknąć starcia Millennium - KADEM. Stwierdzili, że Eryk to zgłosi. Nie jako "drugi raz" a jako "reproducible issue".


# Streszczenie

Spustoszenie i zachowanie Aegis0002 postawiło cały program pod znakiem zapytania; zdecydowano się jednak zaryzykować i uruchomić GS Aegis 0003. 0003 jest podpięta pod Silurię i Eryka; podejrzewają, że głowica ma osobowość. Tymczasem zniknął Lucjan Kopidół wraz z artefaktem wykorzystującym Pryzmat. Pryzmat zamanifestował się w przedszkolu i w TESCO, jednak Aegis przechwyciła Kopidoła przed Działem Plazmowym i odzyskała artefakt pryzmaturgiczny. Na Lucjana Kopidoła wpływały jakieś zmiany, stoi za tym "Helena Diakon". Która nie jest prawdziwą Diakonką.

# Lokalizacje:

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Centrum
                        1. Cyberklub Działo Plazmowe, gdzie toczą się przygotowania do SaberThrone.
                        1. KADEM Primus
                            1. Instytut Technomancji
                            1. Instytut Pryzmaturgii
                            1. Sale pracy własnej
                    1. Dzielnica Trzech Myszy
                        1. Duże Tesco, gdzie doszło do Skażenia pryzmaturgicznego przez artefakt KADEMu podczas przeceny laptopów
                        1. Przedszkole Łobuziak, gdzie doszło do Skażenia pryzmaturgicznego przez artefakt KADEMu

# Pytania:

Ż - Dlaczego Eryk jest w stanie poradzić sobie z pryzmatycznym oddziaływaniem artefaktu?
K - Bo jego ten pryzmat w ogóle nie dotyka.
Ż - Dlaczego dostaliście głowicę na akcję infiltracyjną?
P - Z powodu zdrady.
K - Dlaczego Marcie cholernie zależy na powodzeniu tej akcji?
Ż - Bo ten artefakt może zachwiać równowagą sił między ludźmi a magami.
P - Co poszło nie tak z pierwszymi dwoma głowicami.
Ż - Nikt nie rozumie jak działa AI Eis.


N/A

# Stakes:



# Idea:

- Lol, wszystkiemu zawsze winne jest Millennium. Lub Dracena. Czy Antygona.

# Zasługi

* mag: Eryk Płomień jako cybernetyczny analityk o wielkim sercu dla wadliwych głowic bojowych
* mag: Siluria Diakon jako wdzięczny kłębek uroku mający wszystkich pod słodkim paluszkiem
* vic: GS "Aegis" 0003 jako trzecia, jeszcze nie wysadzona głowica bojowa
* mag: Ilona Amant jako wysoka rangą czarodziejka KADEMu niechętna zniszczeniu GS "Aegis"
* mag: Lucjan Kopidół jako mag-renegat (?) KADEMu, astralika x pryzmat. 52 lata.
* mag: Aleksandra Trawens jako przełożona Instytutu Pryzmaturgii
* mag: Mariusz Trzosik, Instytut Pryzmaturgii, specjalizacja Zniszczenie.
* czł: Stanislaw Przybysz jako "DJ Elektron" w "Dziale Plazmowym", konkurent i frenemy Draceny.
* mag: Mikado Diakon jako tymczasowy ekspert od działań Draceny
* mag: Tadeusz Baran jako Srebrnoświecowy tymczasowy ekspert od działań Antygony i znajomy Silurii.
* mag: Dracena Diakon jako główna, chyba niewinna podejrzana
* mag: Antygona Diakon jako druga, najpewniej niewinna podejrzana

# Wątki

- Aegis czy EIS?

# Czas: 

* Opóźnienie: 2 dni
* Dni: 2
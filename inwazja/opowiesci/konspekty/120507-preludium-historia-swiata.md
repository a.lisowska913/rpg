---
layout: inwazja-konspekt
title:  "Preludium: Historia świata"
campaign: start
categories: inwazja, konspekt
---

## Kontynuacja

### Kampanijna

* [120507 - Preludium: Historia świata](120507-preludium-historia-swiata.html)

### Chronologiczna

* [120507 - Preludium: Historia świata](120507-preludium-historia-swiata.html)


## Definicje

* **Primus** - nasz plan rzeczywistości
* **Rój Leonidów** - co roku na Ziemię spadają tamtejsze meteoryty. Główne źródło arildisu na Ziemi. Nie wszystkie zawierają arildis.
* **Vicinius** - inteligentna istota magiczna.

## Czasy Legend

Dawno temu magowie mieli zdecydowanie większą moc. Ci, którzy osiągnęli odpowiednią jej wartość byli jak bogowie. Dla odpowiednio potężnego maga osiągnięcie nieśmiertelności czy wskrzeszenia innego maga było stosunkowo proste. Ten okres jest dziś znany jako "Czasy Legend". Kalevala, mity i legendy arturiańskie, wszystkie te opowieści mogły być częściowo prawdą.

W Czasach Legend popularną praktyką największych arcymagów było "zbliżanie planów do Primusa". Oznaczało to przenikanie różnych planów i rzeczywistości celem zdobycia większej mocy i źródeł do ekstrakcji Quark. Część planów uległa całkowitemu zniszczeniu i wchłonięciu przez Primus, część jedynie zaczęła orbitować dookoła Primusa. Tak czy inaczej, efektem było to, że Primus stał się dużo bardziej magiczny i ilość mocy magicznej oraz Utalentowanych ludzi zaczęł drastycznie rosnąć.

## Czasy Rozłamu

Spośród największych magów okresu Czasu Legend wyłoniły się dwie grupy. 

Jedna uważała, że ciągłe zwiększanie energii magicznych jest korzystne - coraz większa ilość ludzi staje się Utalentowanymi, ilość energii magicznej na świecie rośnie, świat staje się coraz lepszy. Ewentualne problemy związane ze zwiększonym polem energii magicznej i wrogość innych planów do Primusa uznali za problematyczne, lecz z optymizmem patrzyli w przyszłość. Ta grupa nazwała się Lugus <światło>. Wpływową czarodziejką w Europie była Morgana LeFay.

Druga uważała, że ciągłe zwiększanie energii magicznych prowadzi do skażenia czasoprzestrzeni i doprowadzić może nawet do wirów temporalnych, a docelowo do rozmycia całego życia i całej rzeczywistości w wirze magicznym, węźle większym niż jakikolwiek znany Macierzysty. Zauważyli też pewną zależność wieku maga i jego mocy (moc zwykle rośnie z czasem) jak i wpływu samej energii magicznej na stabilność umysłu maga (im większa, tym gorzej dla mózgu). Ta grupa nazwała się Illuminati. Wpływowym magiem w Europie był Merlin.

Między magami Lugus i Illuminati dochodziło do częstych starć i bojów a wojny między nimi ogarnęły wszystkie znane wówczas kontynenty (Europę i Azję). W tym okresie miejsca, gdzie znajdowały się rudy srebra były martwymi strefami, niemożliwymi do zamieszkania, tak wielka była moc pola magicznego.

Doszło do znacznej eskalacji pomiędzy Lugusem a Illuminati po tym, jak spadł stosunkowo niewielki meteoryt zawierający arildis. Ów "kosmiczny kamień" wpadający w ziemskie pole magiczne dokonał ogromnych zniszczeń reagując z energią magiczną (co odbiło się wyraźnym echem na orbitujących planach). Sam meteoryt uległ zniszczeniu. Illuminati oszacowali, że wraz ze wzrostem energii magicznej może dojść do sytuacji w której arildisowy meteoryt zniszczy Ziemię. To, wraz z obserwacjami astronomicznymi skłoniło Illuminati do bardziej desperackich i skutecznych działań.

Dziś już nie wiadomo, co dokładnie się stało. Illuminati próbowali przeprowadzić bardzo skomplikowany rytuał, który miał na celu doprowadzić do obniżenia średniego pola energii magicznej w jakiś sposób, o którym dziś nic nie wiadomo. Jedyne, co na pewno wiadomo to to, że rytuał ten zawierał komponenty tego, co dziś nazywamy magią krwi i że odbywał się na wszystkich znanych terenach. Jednocześnie jednak magowie Lugusa zaczęli przeprowadzać własny rytuał - o niewiadomych celach - o zasięgu takim jak rytuał Illuminati. Nie wiadomo, co się stało i kto zawinił. Efektem jednak było Pierwsze Zaćmienie.

## Pierwsze Zaćmienie

Pierwsze Zaćmienie było najbardziej efektownym i katastroficznym. Najpotężniejsi magowie, nawet ci, którzy nie brali udziału w rytuale zniknęli z tego świata. Najpotężniejsze artefakty albo przestały być magiczne, albo zostały zniszczone. Wieże magiczne i centra badawcze zostały skażone lub unicestwione a energia magiczna wypromieniowała. Formy magii zostały zmienione, parę planów orbitujących dookoła Primusa zostało poważnie skażonych, kilka unicestwionych, kilka się oddaliło a inne uległy zbliżeniu. Zdecydowana większość magów Illuminati także zginęła, duża grupa ludzi uzyskała magię a większość byłych magów tą moc utraciło. Potężne fale mutacji i zniekształceń rzeczywistości przeszło przez Ziemię, zmieniając planetę.

Pozostały grupy nowych, przerażonych magów nie znających swoich mocy czy możliwości wśród dużo liczniejszych, zwyczajnych ludzi oraz eks-magów w bardzo niebezpiecznym świecie - miejscu potworów zmutowanych energią magiczną, viciniusów oraz najeźdźców z innych planów. Pierwsze Zaćmienie sprawiło, że nawet ludzka wiedza odnośnie świata nie była w pełni aktualna - pojawiły się nowe gatunki roślin i zwierząt, nie mówiąc już o nowych gatunkach viciniusów i miejscach mocy. Szczęśliwie, obszary zawierające srebro nadawały się już do zamieszkania a z uwagi na to, że wcześniej nie było czego mutować (bo nic tam nie żyło) stały się obszarami najbezpieczniejszymi. Dawne obszary rozkwitu cywilizacji - ośrodki magiczne - stały się najbardziej niebezpiecznymi miejscami na ziemi.

## Wielka Odbudowa

Wiele z odciętych grupek magów zostało unicestwionych. Część z przyczyn geograficznych nie miała szans, część znalazła się na drodze najazdu z innych planów, korzystających z tego, że Primus jest osłabiony i chcących się zemścić lub po prostu skorzystać z okazji. 
Wśród tych, którzy przetrwali pojawiły się trzy główne nurty:
- magowie biorący ludzi w opiekę, i przejmujący władzę, działający jawnie - magowie zostawali szlachtą w klasycznym systemie feudalnym, ale mieli swoje obowiązki wobec ludzi.
- magowie korzystający z mocy by rządzić ludźmi - pełna taumatokracja, ludzie są surowcem, wola maga jest prawem a znaczenie maga jest definiowane tylko przez jego potęgę.
- magowie nie przejmujący władzy, ale pomagający ludziom - zwykle pozycjonowali się jako przedstawiciele duchowieństwa lub "starsi wioskowi". Rzadko obnosili się przed ludźmi ze swymi mocami.

Warto zaznaczyć tu jedną specyficzną grupę - viciniusy, które zdecydowały się objąć opieką grupkę ludzi w zamian za (porównywalnie niewielkie) ofiary. Między innymi grupa kralothów zdecydowała się osiedlić na Primusie i potraktowały obecnych tu ludzi jako potencjalną hodowlę, by mieć relatywny spokój.

Wraz z upływem czasu ogólny chaos magiczny zaczął się zmniejszać. Najbardziej niebezpieczne i nienaturalne potwory zaczęły same wymierać, niezdolne do odżywiania się i rozmnażania. Niestabilne źródła wygasły a poziom energii przestał tak fluktuować. Ludzie mogli zacząć mniej martwić się przeżyciem a nawet pomyśleć o stabilizacji i eksploracji najbliższego otoczenia.

Podczas tej eksploracji ludzkość nawiązała kontakty z różnymi rodzajami viciniusów oraz z innymi grupami ludzkimi. Niestety, odnaleźli też Illuminati - a przynajmniej to, co z nich pozostało.

## Wojna z Illuminati

Nie wiadomo, co działo się z Illuminati w okresie Wielkiej Odbudowy.
Wiadomo jednak, że wydarzenia Pierwszego Zaćmienia zginęła większość z magów Illuminati - przetrwało zaledwie kilkuset.
Ci, którzy przetrwali, zostali zmienieni przez magię tak, że nie byli już magami.
Nie wiadomo do końca, //czym// są istoty znane dziś jako Illuminati. Jak dotąd żadnemu magowi nie udało się zbliżyć do nich na tyle, aby przeprowadzić jakiekolwiek dokładniejsze badania.
Znany jednak jest ich cel, a jest nim eksterminacja wszystkiego, co potrafi władać energią magiczną powyżej "średniego poziomu energii magicznej". Istoty te są inkarnacją idei oryginalnych magów Illuminati - uniemożliwienie osiągnięcia takiego poziomu mocy, by istniała możliwość wskrzeszania magów czy jakiejkolwiek formy nieśmiertelności.

Pierwsze spotkania magów z Illuminati nieodmiennie kończyły się tragicznie dla magów, którzy zupełnie nie spodziewali się istnienia takiej siły. W końcu jednak jakiś mag musiał uciec z pogromu i donieść reszcie obdarzonych mocą o czyhającym zagrożeniu. Mimo ostrzeżenia bywało, że Illuminati wyrzynali całe rodziny magów, pozostawiając jedynie słabe niedobitki, po czym ruszali dalej, nie pozostawiając na swej drodze potężnych magów. Nie oszczędzali również niektórych viciniusów. Te viciniusy, które zdolne były władać magią w sposób podobny do magów, również padały ofiarą Illuminati.

Reakcje ludzi na Illuminati były różne, zależnie od tego, jak traktowali ich magowie. Ci ludzie, którzy byli pod absolutną kontrolą magów, uznali Illuminati za swoich wybawców i aktywnie starali się wspierać ich sprawę. Ludzie, których magowie traktowali dobrze, na ogół starali się walczyć z Illuminati, których postrzegali jako agresora.
Samych ludzi Illuminati z zasady nie krzywdzili. W końcu obrońcy magów zauważyli pewną zależność - Illuminati niezbyt chętnie pojawiali się wśród ludzi, nawet tych, którzy całym sercem wspierali ich sprawę. Bliższe zbadanie sprawy udowodniło, że Illuminati czują się niepewnie w towarzystwie ludzi nie dotkniętych w żaden sposób mocą magiczną. Z czasem udało się udowodnić, że Illuminati unikają obszarów pozbawionych magii czy też nasyconych srebrem (co doprowadziło do powstania gwałtownego zapotrzebowania na ten metal i rozwój wydobycia i przetwórstwa)

Z czasem udało się odkryć, że Illuminati silnie zależą od średniego poziomu mocy. Im wyższa moc tła, tym potężniejszy Illuminati. Wraz ze spadkiem energii magicznej, Illuminati stawali się coraz słabsi. I choć wraz ze zniknięciem mocy magicznej, najprawdopodobniej zniknęliby i Illuminati, oznaczałoby to również zniszczenie magów.

## Powstanie Maskarady

Wojny z Illuminati były długie i wyniszczające dla magów, zwłaszcza, że początkowo Illuminati współpracowali ze sobą. W końcu magowie zdali sobie sprawę, że jeśli tendencja się utrzyma, Illuminati osiągną swój cel - niemal doprowadzili oni do wyniszczenia magów na terenie Eurazji (czyli całego znanego za ich czasów świata). Chyba najstraszniejszym odkryciem, jakiego dokonali magowie było to, że w zasadzie nie są w stanie zabić Illuminati - a przynajmniej nie na długo. W miejce zabitego zawsze pojawiał się nowy. I choć nie miał dokładnej wiedzy poprzedniego, miał dokładnie taką samą moc i potrafił dążyć do swego celu równie skutecznie.
Za każdym razem, kiedy Illuminati udawało się zidentyfikować maga, jego szanse na przeżycie gwałtownie malały.

Stopniowo ludzie coraz bardziej niechętnie opowiadali o "swoich" magach, zachowując ich istnienie w tajemnicy przed obcymi. Magowie popierali taką postawę; dyktował to czysty instynkt samozachowawczy. Przez wieki istnienie magów w danej okolicy stawało się coraz większym sekretem, aż do chwili, kiedy nie wszyscy ludzie w danej osadzie wiedzieli, że jest w niej mag. W ten sposób narodziła się Maskarada, zaś grono osób "uświadomionych" coraz bardziej się zawężało.

Okres narodzin Maskarady to również czas, kiedy okazało się, że to, czego nie mogą osiągnąć magowie, jest w zasięgu ludzi. Powstały wówczas grupy ludzi polujące na Illuminati. Zwabiali oni Illuminati i zabijali ich, co było nieosiągalne dla magów. Znamienne, że jak długo magia nie dotykała ludzi, Illuminati ich nie atakowali, nawet, jeśli sami byli atakowani. Wydawało się wręcz, że Illuminati muszą włożyć spory wysiłek w dostrzeżenie "czystych" ludzi. Jeśli jednak człowiek był pod wpływem zaklęcia, nie mieli żadnych skrupułów. 

Niestety, choć nieludzcy, Illuminati okazali się dość inteligentni, by po tym, jak ludzie odnieśli początkowe sukcesy, zacząć działać przez agentów. Sami Illuminati zaczęli również przestrzegać Maskarady.

Magowie, aby utrudnić Illuminati identyfikację, opracowali metody kontroli swojej mocy w taki sposób, aby nią nie emanować. Pociągnęło to za sobą spadek efektywnej mocy magów, gdyż pewna część ich energii była zużywana na stałe wyciszanie echa, ale jednocześnie wydatnie zwiększyło przeżywalność. 
Sekret kontroli swojej mocy i rozmycia swojego echa przekazywany był z ust do ust i stawał się coraz bardziej znany wśród magów.

Przeżycie ułatwiały magom również zmiany stopniowo zachodzące w samych Illuminati - w miarę upływu czasu od Pierwszego Zaćmienia Illuminati stawali się coraz bardziej samotniczy - w chwili obecnej Illuminati nie są zdolni do współpracy.

## Struktury magów

Maskarada, zmiany magiczne, Illuminati, nowe, nieznane jeszcze magom typy viciniusów i tym podobne rzeczy sprawiły, że świat stał się miejscem bardzo nieprzyjaznym dla magów. Z czasem Maskarada stała się odgórnym nakazem, niemalże religią, a powiązania magów z ludźmi się coraz bardziej rozmywały. 

Kiedy ludzie weszli w epokę Oświecenia, Maskarada miała już formę zbliżoną do dzisiejszej - zaledwie pojedyncze jednostki były świadome istnienia magii i magów.
Ludzie nie polowali już na Illuminati i nie ochraniali aktywnie magów. W tej sytuacji, dla ochrony, magowie zaczęli łączyć się w gildie, gromadzące wiedzę i moc. Illuminati, choć potrafili zlokalizować gildie, nigdy nie mieli dość mocy, by zniszczyć pojedyncze, silne skupisko magów. 
W okresie Oświecenia gildie wypracowały taki sposób czarowania, by ich moc upodobniła się w "kolorze" do Illuminati. Dzięki temu Illuminati nie potrafią odróżnić maga od innego Illuminati - doprowadziło to do sytuacji, gdzie w razie spotkania dwóch Illuminati wywiąże się walka pomiędzy nimi. Illuminati jest zwykle najpotężniejszym bytem w okolicy, co czyni go automatycznie celem drugiego Illuminati, równego mu mocą. Dzięki temu magowie mają szansę uciec z zagrożonego terenu.

Illuminati wciąż polowali na tych magów, których byli w stanie zlokalizować. Zwłaszcza nowi magowie, na przykład pochodzący z ludzkich rodzin, padali ich ofiarą. Magowie aktywnie poszukiwali takich osób (nazywanych przez nich proto magami) aby nauczyć ich kontrolować swoją moc i dać szansę na przeżycie.

Obecnie wszyscy Illuminati są znani z "imienia"; sposób działania każdego Illuminati jest znany i opisany.
W chwili obecnej magowie starają się kierować agentów jednego Illuminati na agentów drugiego, czasem doprowadzając do starcia tytanów.

## Czasy nowożytne

Illuminati pochodzą z Eurazji; w Amerykach, Afryce i Australii magia była częścią kultury plemiennej; zasięg działania pierwotnej organizacji Illuminati ograniczał się do Eurazji.
Po odkryciu nowych lądów Illuminati rozproszyli się po kontynentach, efektywnie rozciągając swoje siły. Stało się to w sposób bardzo naturalny - magowie, którzy wywędrowali do Ameryki, Afryki czy Australii bardzo swobodnie korzystali z mocy, co automatycznie przyciągało uwagę Illuminati.
Przez to zagrożenie ze strony Illuminati bardzo spadło - kilkuset musiało rozciągnąć swe siły na cały glob zamiast dwóch kontynentów. Dodatkowo, gwałtowny wzrost liczebności ludzi i magów sprawił, że ilość potencjalnych magów na terytorium każdego Illuminati zdecydowanie wzrosła, utrudniając jednocześnie odnalezienie i identyfikację magów.

W Polsce aktualnie wiadomo o jednym "rezydentnym" Illuminati, w szczytowych momentach zaobserwowano nie więcej niż trzech.

Miały miejsce przypadki, w których magowie celowo próbowali zdjąć Maskaradę, złamać ją na stałe. Zawsze jednak kończyło się to tragicznie dla podejmujących taką próbę magów - ginęli oni albo z rąk samycj Illuminati, albo z rąk ich agentów.
Duże gildie mają oddziały, których jedynym zadaniem jest śledzenie Illuminati i ich posunięć; w Polsce taką gildią jest Srebrna Świeca. Co więcej, SŚ udostępnia tą informację - choć uważnie sprawdza, kto ma do niej dostęp (Ma to na celu uniknięcie sytuacji, w której jeden mag ściąga drugiemu na głowę Illuminati, by ten maga zabił).
SŚ posiada oddział terminusów koncentrujący się na utrzymaniu Maskarady i ochronie magów i ludzi. Oddział ten jest całkowicie apolityczny, a jego władzę uznają wszystkie gildie na terenie Polski. Oddział ten nie składa się wyłącznie z magów SŚ. Gildia jedynie zaopatruje go w sprzęt i zapewnia bazę.

Obecnie istnieją miejsca, do których Illuminati nie mają wstępu - nie są w stanie tam przetrwać (w Polsce takimi miejscami są Kopalin i część Warszawy). To sprawia, że miejsca te stały się głównymi skupiskami i ośrodkami działalności magów. Zaobserwowano jednak, że po przekroczeniu odpowiedniego poziomu tła magicznego Illuminati jest w stanie wkroczyć do sanktuarium - dlatego obowiązuje ograniczenie wykorzystywanej mocy w sanktuariach.

Jako, że Illuminati działają poprzez agentów, nieostrożni magowie czasem są 'naznaczani' - na przykład agent dostarcza Illuminati artefakt wykonany przez konkretnego maga. Od tej pory Illuminati zna 'zapach' tego maga i jest w stanie go namierzyć. 
Taki mag ma niewielkie szanse przetrwania, chyba, że uda mu się dotrzeć do sanktuarium - Illuminati traci wówczas namiar. Choć wciąż zna 'zapach' maga, nie jest w stanie go odnaleźć. Jednak dotarcie do sanktuarium nie jest końcem problemów tego maga - jako jedyne tereny niedostępne bezpośrednio dla Illuminati, sanktuaria mają najwyższe zagęszczenie ich agentów. 
Wśród magów są grupy, które uważają, że im mniej się czaruje, tym lepiej, by powstawało jak najmniej tła. Inne grupy dążą do maksymalizacji tła tak, by Illuminati mieli ręce pełne roboty. Jednak większość uważa Illuminati za legendę, bajkę służącą straszeniu dzieci.

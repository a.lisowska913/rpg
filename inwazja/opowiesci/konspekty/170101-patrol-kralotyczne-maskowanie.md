---
layout: inwazja-konspekt
title:  "Patrol? Kralotyczne maskowanie!"
campaign: powrot-karradraela
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [160819 - Oblicze Guwernantki (HB, KB, AB, DK)](160819-oblicze-guwernantki.html)

### Chronologiczna

* [161231 - Eskalacja Czelimina, eskalacja Andrei... (AW)](161231-eskalacja-czelimina-andrei.html)

## Kontekst ogólny sytuacji
## Punkt zerowy:

Karradrael chce przekształcić mocą Krwawej Kuźni Laurenę w broń przeciwko Mieszkowi. Hunter-Killer. Zastawił pułapkę. Albo Katalina, albo Laurena...

Tymczasem, Andrea zdecydowała się na kontrpułapkę - zniszczyć oddziały szukające ich i dać do zrozumienia, że Mieszko jest kontrolowany przez kralotha.

* Zajcewowie przygotowują grupę uderzeniową do zbombardowania grupy viciniusów GDZIEŚ.
* Tatiana przyzywa Śledzika
* Kajetan podszkala skrzydło szkoleniowe
* Rafael kusi Pszczelaka Wiolettą (dostał odpowiednio sfabrykowany posiłek)
* Melodia zajmuje się piórami Deiiwa
* Lilak zbiera informacje z otoczenia
* Laurena jest całkowicie zignorowana
* Lidia i Dalia próbują uruchomić Jankowskiego
* Mieszko 'z ketchupem' jest mierzony łakomym wzrokiem Laragnarhaga
* Andżelika miała rozpatrzeć kwestię ukrycia magów przed Oczami.

## Misja właściwa:

Dzień 1:

Andżelika z Rodionem przyszli do Andrei. Rodion jest uśmiechnięty i non-nonsense. Andżelika jest BARDZO nieszczęśliwa.

* Rozpatrzyłam kilka możliwości ukrycia nas przed tymi patrolami... - Andżelika, próbując jakoś ubrać to w słowa. Próbuje czegoś nie powiedzieć, ale Rodion patrzy.
* Najsensowniej byłoby zrobić tak, by zrobić jawną aurę kontroli kralotha. - Andżelika.
* Kontynuuj, mała. - Rodion, patrząc na Andżelikę - Teraz najlepszy fragment.
* Nasz przeciwnik kontroluje magów Świecy i Millennium. Być może KADEMu. - Andżelika, patrząc złym okiem na Rodiona
* Musimy założyć, że nasz przeciwnik wie, jacy magowie powinni być gdzie - Andżelika, podłamana
* No... - Rodion, zachęcając Andżelikę
* Nie możemy ujawnić żadnego maga Millennium... - Andżelika - Bo nikt w to nie uwierzy. Nie, że kraloth to kontroluje.
* Nie możemy ujawnić Zajcewów. Bo... ich szuka. I nie chcemy ich pokazać i zwracać uwagi. - niechętna Andżelika
* To powoduje, że mamy bardzo mało magów, których możemy pokazać pod kontrolą kralotha - załamana Andżelika
* Nie chcemy pokazywać większości Świecy. Zostajecie wy... i Krąg Życia. - Andrea, z namysłem
* Nie chcę być pod kontrolą kralotha! - Andżelika, prawie wybuchając
* Nie musisz. Możesz zrobić fałszywą aurę. - Andrea, nie rozumiejąc
* Nie. Nie umiem. Nie umiem pracować z magią krwi! - Andżelika, lekko się kontrolując
* Kraloth może ci ją podać. Możecie współpracować. - Andrea, łagodnie acz stanowczo
* Jak... współpracować z kralothem? - Andżelika, nie rozumiejąc
* Normalnie. Melodia może Ci w tym pomóc. - Andrea, z uśmiechem

"To z kralothem będzie najbardziej traumatycznym wydarzeniem dla Andżeliki... do Esuriit." - Kić, wybuchając śmiechem

Andrea odesłała Rodiona, by ten przyprowadził Melodię. I faktycznie, Melodia przyszła. 

Po zastanowieniu, Andrea wytypowała sobie grupę magów do bycia kontrolowanym przez kralotha: Marian, Andżelika, skrzydło szkoleniowe, Pszczelak, Baran. Siedmiu magów. A Mieszko jest "pożarty" i nie do odzyskania. Jest to coś, co powinno zmylić Karradraela.

Andżelika zaproponowała, że wraz z innymi katalistami (np. z Baranem) przekierują energię jakiegoś pobliskiego Węzła do zameczku. Kraloth to nasyci energią Krwi i zrobi Skażenie. Sygnał skierują 'do góry', poza siebie. Wszyscy magowie będą w piwnicy. To sprawi, że żaden patrol nie będzie chciał tu wchodzić (kralothy są terytorialne a ten się urządził) a krwawy sygnał zagłuszy magów w piwnicy.

Przybyła Melodia. Andrea odesłała z nią Andżelikę; mają się dogadać z Laragnarhagiem. Andżelika jeszcze raz spróbowała protestować - Andrea jednak zagrała na jej ciekawości i praworządności. Zauważyła też do Andżeliki, że ONA (Andrea) miała do czynienia z kralothami (hobbystycznie) i jej się bardzo podobało. Andżelika nie wie jakie to uczucie, prawdziwa unia i współpraca. Andżelika nieco niechętnie, ale się zgodziła. Została przekonana (a nawet lekko zaciekawiona). Melodia się szeroko uśmiechnęła.

Melodia jeszcze na moment została sam-na-sam z Andreą. Zapytała Andreę, czy to co powiedział jej Rafael jest prawdą? Gdy Andrea się zawahała, Melodia powiedziała, że ona osobiście jest wielką zwolenniczką dobrowolności, ale do dobrowolności można doprowadzić. Tylko, że... Rafael. Ona mu nie ufa. Andrea potwierdziła, że sama jest zwolenniczką dobrowolności. Melodia zrozumiała. Zajmie się tym ;-).

(Melodia porozmawia z Wiolettą na zasadzie takiej, że ta w końcu nie pokazała się od najlepszej strony wobec Andrei. I jak to można by naprawić, by Wioletta mogła być tą... FAJNĄ. Sama nakieruje Wiolettę na to, że w sumie Laragnarhag był w Kompleksie i on sam nie zinterpretował wszystkiego. I że możnaby zrobić Unię między Wiolettą a Laragnarhagiem. Potem skieruje Wiolettę na Pszczelaka, który ma wiedzę o starych tematach. A potem, poproszona przez Wiolettę, spróbuje uwieść Pszczelaka używając Wioletty tak, by to był 'one kraloth stand' i by nic nigdy nie wyszło. Melodia, w odróżnieniu od Rafaela, zna się na tym doskonale i to lubi. Wszyscy będą tacy szczęśliwi ;-) )

Lilak powrócił. Ma kompozycje i lokalizacje dla Andrei. Andrea od razu ściągnęła Rodiona. Rodion spojrzał na Lilaka, widzi psa, nic nie powiedział. Andrea powiedziała, że to sojusznik. Rodion wziął flaszkę i golnął. Poza tym, nic nie powiedział. 

"Dopiero zmusił Andżelikę do gadania z kralothem. Teraz musi pogadać z psem. Deal with it." - Kić, 2017

Lilak powiedział, że to sześć grup. Każda ma mniej więcej: 
* jednego mortalisa, wykrywającego po empatii; skontrowane przez ekstazę kralotyczną
* tego samego mortalisa, wykrywającego po umysłach ludzi którzy widzieli Mieszka; nikt go nie widział
* jednego błękitniaka, wykrywającego energie magiczne; skontrowane przez Węzeł Krwi
* dwa glashundy, do rozszarpywania i walki; są lapisowane
* jednego demonicznego koordynatora, dowodzącego oddziałem
* rój Spustoszonych dron opadający magów i szukający z powietrza
* 2-3 mniejsze demony bojowe, śmierdzące krwią i metalem

Czyli ogólnie oddział jest niefajny; może być problem z czystą artylerią. Lilak ma ślady; wie gdzie te oddziały są. Mortalisy nie umieją czytać umysłów i uczuć niektórych viciniusów, bo są zbyt nieludzkie. A Krąg Życia potrafi łączyć viciniusy w łańcuchy, by dało się jakoś skomunikować.

Rodion dostał po hipernecie zarys planu od Andrei: wysłanie ich z viciniusami Kręgu Życia, by obejść patrole i mają ubić patrol gdzieś daleko. Najlepiej artylerią. Na głos, Andrea zapytała Lilaka, czy są w stanie dyskretnie przeprowadzić Zajcewów. Ten odparł, że to będzie trudne, bo dużo metalowych ptaków na niebie. Ale się da. Na pewno w stronę OD Wtorku; niekoniecznie z powrotem.

Rodion spytał Andreę, czy ma wrócić. Nie. Mają kupić jej czas. Dobra. Rodion to załatwi. Poprosił Andreę o sprzęt anty-Spustoszeniowy. Dostanie. Przekaże parę drobiazgów swojemu kowalowi; uzbroją się właściwie i będą gotowi do wymarszu. Zajcewkommando. Andrea poprosiła, by poczekał; ma jeszcze jeden pomysł - poszła do Andżeliki. Zajcewów z bronią KADEMu się nikt nie spodziewa, zwłaszcza, że KADEM odcięty. Andżelika powiedziała, że nie ma problemu. Sprzęt KADEMu może oddać; nieco niebezpieczny, ale się nadaje. Skonsultowała też Rodiona w sprawie ukrywania oddziału Zajcewkommando. Rodion tylko poprosił Andreę, by Tatiana tu została; Irina go zabije jeśli jej coś się stanie. Andrea się zgodziła.

Oki. Andrea zapewniła sobie dywersję od strony nie-Wtorku. Jeszcze Śledzik; ale on się pojawi (Tatiana).

Andrea zeszła do pomieszczeń kralotha. Musi porozmawiać z Draconisem - jak wezwać cholerną Arazille?

Draconis się przygotował. Powiedział Andrei, że jest w stanie wezwać Arazille. Potrzebne jest, niestety, Ogniwo. Nexus. Samo wezwanie Arazille nic nie da; moc Karradraela jest zbyt wielka. Trzeba przynieść np. z Żonkibora Ogniwo dla Arazille - coś, dzięki czemu 'świat Arazille' zostanie przyniesiony do 'świata Czelimina'. Draconis może podać Andrei rytuał stworzenia Ogniwa; trzeba maga, który poświęci dość czasu w miejscu, gdzie już jest Ogniwo (Żonkibór). A potem jakiś mag musi zanieść Ogniwo do Czelimina. I najpewniej mag który zaniesie Ogniwo do Czelimina umrze. A dokładniej - stanie się awatarem Arazille.

Draconis powiedział Andrei, że przekazanie wiedzy o rytuale wymaga Unii bezpośredniej z kralothem. Primo, Draconis ABSOLUTNIE nie życzy sobie, by Rafael poznał ten rytuał (Andrea podpisuje się pod tym wszystkimi kończynami). Secundo, kraloth jest w stanie sprawić, by mag znający rytuał nie był w stanie go powtórzyć; zamazać mu go. Prosta kralotyczna indoktrynacja. Andrea się zasępiła - Baran byłby idealny, ale Arazille go uwolni od Rafaela. Padło na Przylaza a Kajetan będzie go osłaniał... wrócą za trzy dni to wszyscy będą szczęśliwi. Wniesienie Ogniwa do Czelimina sprawi, że Arazille się zamanifestuje. Będzie mieć energię. Zareaguje.

Andrea wezwała do siebie Przylaza. Przyszedł, cały przestraszony. Andrea widzi poczciwego, dobrego człowieka. I wie, co ma zamiar mu zrobić...

* Tien Przylaz, mam dla pana zadanie - Andrea, poważnie - Zadanie trudne i wymagające, ale dostanie pan wsparcie pełnoprawnego terminusa. Z obecnych tu magów pan najlepiej nadaje się do jego wykonania.
* Chodzi o przeprowadzenie rytuału w celu stworzenia artefaktu. W tym celu uda się pan do Żonkiboru a następnie z gotowym artefaktem powróci tutaj. - Andrea, kontynuuje
* Lady Terminus, nie znam się na artefaktach.
* Nie musi się pan znać. Szczegóły rytuału będą panu przekazane przez Laragnarhaga.
* Kto to jest? - Przylaz, jeszcze niewinny
* Kraloth - Andrea, spokojnie
* Kraloth? Ten potwór? - Przylaz, przerażony
* Tien Przylaz, gwarantuję, że nie zrobi panu krzywdy. Niemniej jednak to nie było pytanie ani prośba.
* To on mi to jakoś... napisze? - Przylaz, lekko uspokojony
* Kralothy nie potrafią pisać. Skomunikuje się pan z nim bezpośrednio. - Andrea, bez cienia sadyzmu
* Telepatia? Tak jest, oczywiście. Przepraszam za pytania. - Przylaz, orientując się z kim rozmawia
* Chodźmy zatem. - Andrea do Przylaza, idąc do Melodii.

Melodia usłyszała, że Alojzy Przylaz będzie musiał przejść przez kralotyczną Unię. I nie wie co to znaczy. Uśmiechnęła się słodko i poprosiła Andreę o 30 minut. Ona mu wyjaśni. I faktycznie, wyjaśniła. Jako, że to bardzo ważna sprawa, Andrea rozkazała a sama Melodia obiecała mu, że wejdzie z nim w tą unię, to nieszczęsny Alojzy się zgodził. Nie chciał - bardzo nie chciał, ale Melodia go uspokoiła. Gdy Andrea przyszła, Alojzy cały aż drżał ze strachu i obrzydzenia, ale zrobi, co Andrea chciała. Melodia. I obiecał Melodii, że nikomu nic nie powie.

Pukanie do drzwi Andrei. To Julian Pszczelak. Przyszedł do Andrei, bo ma problem natury etycznej... rozmawiał z Rafaelem i ma pewien pomysł natury badawczej co może przekazać wiedzę o wrogu, ale do tego celu musiałby się... fraternizować z pewną terminuską. I czy mu wolno. To nie jest jego uczennica! Andrea facepalmowała... jednak Melodia x Rafael robią dobrą robotę ;-).

Przekazała Pszczelakowi, że Przylaz jest jej potrzebny na parę dni. Plus, ma nieprzyjemne zadanie dla niego, Kopca, Barana, Kozakowej, KADEMowców.

"Grupa Dywersyjna" stawiła się na miejsce.

* Wasza siódemka będzie stanowić zasłonę dymną przed aktualnie zbliżającymi się do nas poszukiwaczami. Przeciwnik rozpoczął zakrojone na szerszą skalę poszukiwania i musimy się ukryć. - Andrea, spokojnie.
* Część magów absolutnie nie może zostać dostrzeżona, waszą rolą będzie bycie dywersją. W tym celu musicie nosić na sobie aurę kralotha. Dla przeciwnika powinno wyglądać tak, jakbyście byli pod jego kontrolą - Andrea, nadal spokojnie i nie pozwalając pytaniom na pojawienie się.
* Wieczorem udacie się do pomieszczeń kralotha, by mógł ustawić na was swoją aurę. - Andrea, nic nie wyjaśniając

Marian wie (Andżelika mu powiedziała), Pszczelak po zgodzie Andrei nie ma nic przeciwko niczemu teraz, młodzi nic nie rozumieją. I tak ma być.

Wieczorem wróciła Tatiana. Kilka draśnięć. Jest. Śledzik został przyzwany. Andrea odesłała ją do Dalii. Przylaz i Kajetan już wybyli do Żonkiboru. Zajcewowie odprowadzeni przez Krąg Życia też już odeszli. Andżelika i Baran przekierowali Węzeł by kraloth miał miejsce na dywersję. Dzięki odpowienim eliksirom Rafaela, nikt nie protestował po wejściu do pomieszczeń kralotha; te opary zrobiły swoje. Mieszko jest "pożarty" przez kralotha.

Cała reszta jest zabunkrowana w piwnicy. Pozostaje tylko czekać.

Dzień 2:

* Zajcewowie: (-2) +1 (KADEM) +1 (Spustoszeniowy) +1 (artyleria) +1 (zaskoczenie): sukces. Oddział viciniusów numer jeden ich zarejestrował i został anihilowany.
* Zajcewowie, wieją: +1 anihilacja +1 (viciniusy): sukces. Nic im nie jest, nikt ich nie zobaczy. Uda im się bezproblemowo schować.
* Kajetan: (5) vs 1 (Przylaz), 1 (rzut), 1 (zaufanie), 3+ (Andrea): sukces, Kajetan nie domyślił się, co Andrea zrobiła komukolwiek i jakkolwiek. Wciąż zbyt ufny ;-)
* Przylaz: 1 (za dobrego człowieka), 1 (Karradrael): sukces. Arazille pobłogosławiła Ogniwo. Uda się dnia 3 przynieść Ogniwo.
* Laragnarhag: 2 (prawdopodobne), 1 (nikt nie uwierzy w to, że Świeca się zgodzi), 1 (Węzeł), 1 (thralle), 1 (kraloth) vs 7 (oddział detekcyjny + szukają Mieszka): sukces. Oddział założył, że tamten teren jest pod kontrolą kralotha a Mieszko został zasymilowany
* Śledzik morderca: zaatakował... Zajcewów. Dwóch rannych, acz się wycofali. Karradrael WIE, że Śledź ich zaatakował - nie jest ich sprzymierzeńcem i jest psychiczny...
* Rafael: (5) +5 (Wioletta i Pszczelak i Laragnarhag) + 1 (Melodia w Unii) + 1 (duuużo czasu i syty kraloth) + 2 (rzut) = legendarna elita bez magii: Laragnarhag dał radę to przeasymilować i zrozumieć. Laragnarhag potrafi odtworzyć to, co się stało w Kompleksie. Dokładnie rozumie, co tam się dzieje. Andrea dostanie dokładne informacje, co stało się w Kompleksie Centralnym, jak zadziałał Karradrael i ma POTWIERDZENIE, że to Karradrael. Do tego też wrócimy.
* Laragnarhag: 2 (kraloth), -2 (dyskretnie), 1 (impuls): 1v0: nie udało mu się zasymilować wiedzy wszystkich sprzężonych magów. Tylko wiedzę pobieżną. Żadnych super sekretów. Ale po drodze udało się mu przypadkowo silnie uzależnić od siebie Anię Kozak, o czym NIKT na razie nie wie. Łącznie z nią samą.
* Andrea: -1 (kraloth, to co się stało), -1 (aftermath), -1 (nerwy i emocje), -1 (gildie, różnice) vs 4 (Andrea), 1 (lady terminus): Lidia + Dalia Weiner vs Rafael Diakon; zrobił się konflikt silny. Dodatkowo, rozsypały się bariery na hipernecie i wyszła sprawa z Laureną i Kataliną. Chcieli ratować. Ale Wioletta mówiła, że nie można ich uratować, to pułapka - ogień skupił się na Wioletcie... poza tym, Andrei udało się utrzymać sytuację.

Andrea widzi, że sytuacja jest bez szans. Mieszko jej znienawidzi do samego końca. On poświęci swoje życie dla Laureny czy Kataliny... więc Andrea z ciężkim sercem poprosiła o coś Rafaela.

* Lady Terminus? - Rafael, cały zadowolony z sukcesów dnia dzisiejszego
* Jak zapewne już zdążyłeś usłyszeć, partnerki Mieszka są w Jego rękach. I aktywnie wzywają pomocy. Pułapka, szyta grubymi nićmi. - Andrea, smutna
* W tej chwili, Karradrael uważa, że Mieszko nie żyje, co czyni z niego idealnego kandydata do wykonania... OSTATNIEGO zadania. - Andrea, stanowcza
* Wezwania Królowej Marzeń? - Rafael, dla odmiany poważny - Ktoś musi zginąć, prawda?
* Niestety... - Andrea.
* Zrozumiałem. - Rafael, poważny jak rzadko.
* Wolałabym, żeby był ochotnikiem - Andrea, z naciskiem
* Będzie bohaterem. - Rafael, nadal poważnie

Dzięki temu Malia będzie miała potem dużo lepsze warunki i lepsze życie...

Informacje z Kompleksu Centralnego od Laragnarhaga okazały się być bardzo ciekawe i cenne. Dagmara dowodzi, nie Wiktor. Ale tak naprawdę, ona jest częścią tego kombinatu (Blood Suit + Dagmara) - to powoduje, że psychika Dagmary wpływa na działania Karradraela w tamtym regionie. Na przykład, jej oddanie Sowińskiemu sprawia, że nie zginie tak łatwo. To powoduje, że da się zastawić na nią pułapkę - i ta wiadomość poszła do Agresta natychmiast.

Andrea dostała też informacje odnośnie Irytki i innych tego typu broni jakie stosowano. Świeca Daemonica oraz Millennium mogą opracować łatwo antidotum.

Andrea dostała wiadomość od Kiryła, że będzie rano. Udało się wszystko załatwić; będzie sam. Rozmowę przerwała jej bardzo ostra kłótnia. Właściwie krzyki pełne prawdziwej złości. Lidia. Wyzywa Rafaela od potworów, defilerów, sadystów i istot takich, jakie zwykle zabijają terminusi. De facto, postawiła go na równi z Karradraelem. I sprawa eskaluje... to, co Andreę zdziwiło - w kłótni pojawił się głos Melodii broniący Rafaela. A to jest ostatnie, czego Andrea by się spodziewała...

Pod drzwiami sal kralotycznych jest grupka. Andrea rzuca im spojrzenie mordercy, po czym wchodzi...

Na miejscu Andrea zauważa Annę wtuloną w kralotha, wchłaniającą soki, Lidię z nożem celującą w Rafaela (bardzo nieporadnie), uśmiechniętego Rafaela, Dalię też wściekłą do białości, Melodię stojącą koło Rafaela i próbującą... mediować i bronić. I kralotha, korzystającego z okazji, asymilującego całą tą energię używając Anny. I Andrea widzi jedną rzecz. Kraloth w dowolnej chwili może unieszkodliwić wszystkich w tym pomieszczeniu. 

Andrea usadziła obie Weinerki. Wszystkich usadziła. Lidia pokazała na Annę (która nie zauważyła obecności Andrei, jest w błogim transie) - co ONI jej zrobili. Rafael wyjaśnił, że Anna jest nadpodatna. Nie było zaplanowane. Wzór Anny jest jakoś powiązany z kralothem, lub z magią krwi, lub z magią erotyczną. We wszystkich wypadkach jest to interesujące, ale da się naprawić. W oczach Lidii pojawiły się łzy bezsilności. Dalia chce dotknąć Annę, ale Melodia nie pozwala. Na skórze Anny są teraz podobne wydzieliny jak na kralocie... nikt nie chce tego przechwycić.

Andrea powiedziała, że to się stało jest złe. Niech sobie ją obwiniają - to z jej rozkazu. Andrea skłamała, że gdyby wiedziała, że to się stanie - rozkaz byłby inny. I Andrea wymusza na Lidii: ma przeprosić Rafaela. Lidia powiedziała, że nie ma zamiaru. Andrea powiedziała, że tak - musi. Lidia powiedziała, że przeprosi... ale odejdzie. Andrea powiedziała, że musi więc odejść. Ale przeprosić. Lidia przeprosiła. I odejdzie z Kajetanem. Andrea się zgodziła.

...Dalia patrzy z wielkimi oczami. Ona zostanie...

Andrea powiedziała też Rafaelowi, że wieczorem ów wybywa z Laragnarhagiem. Rafael się zgodził skwapliwie. Melodia też nie oponuje. Choć pokazała na Annę - co z nią? Też może jechać? Dalia... nie chce. Zdaniem Dalii, jak Anna wróci do zmysłów, będzie wolała być tu, wśród "swoich" niż wśród kralothów Millennium. Melodia się nie zgodziła. Tam wyleczą ją bezboleśnie. Andrea zdecydowała - wyśle z Anną jeszcze tien Przylaza. Więc Rafael i Laragnarhag wyjeżdżają rano. Dalia się ZGADZA z decyzją Andrei. Ona jest lekarzem. Tylko lekarzem. Ona leczy magów, nie decyduje o ich życiu...

I wtedy padła bariera hipernetowa. Błagania Kataliny, sygnały Laureny...

Lidia przełączyła się na tryb zadaniowy. Zablokuje to. Andrea nie oponuje - jeśli ten sygnał będzie szedł cały czas, oni oszaleją a morale będzie zniszczone. Andrea... musi zrobić odezwę do ludności. Lidii udało się uciszyć te komunikaty w ciągu 30 minut. Andrea ściągnęła wszystkich do salki hipernetem. Do Rafaela i Laragnarhaga, wcześniej - "Już pora, byście wrócili do Millennium. Dziękuję za pomoc. Wyślę z wami tien Przylaza i Annę Kozak". 

Andrea podczas odezwy powiedziała, że ten sygnał to pułapka ich przeciwnika. Że nawet nie wiadomo czy to jest prawda. Ale Andrea ma plan. I coś się uda zrobić, by przynajmniej nie tylko się ukrywać.

Wieczorem odeszli:

* Alojzy Przylaz (pojedzie Oplem do Millennium z Anną)
* Anna Kozak (uzależniona, odjedzie Oplem do Millennium)
* Andżelika Leszczyńska (odjedzie Oplem do Millennium, potem wróci jak się uda)
* Marian Łajdak (odjedzie Oplem do Millennium, potem wróci jak się uda)
* Rafael Diakon (odjedzie Oplem do Millennium)
* Laragnarhag (odjedzie Oplem do Millennium)
* Kajetan Weiner (odejdzie gdzieś z Lidią)
* Lidia Weiner (odejdzie z Kajetanem)

# Progresja

* Lidia Weiner: silna niechęć do Rafaela Diakona
* Rafael Diakon: wróg w osobie Lidii Weiner
* Anna Kozak: uzależnienie od kralotha (Laragnarhaga)

# Streszczenie

Lilak przyniósł Andrei info odnośnie Patroli i ich rozmieszczenia. Andrea zdecydowała się zamaskować bazę używając Laragnarhaga; 7 magów jako 'podwładni kralotha', Węzeł odpowiednio wyglądający... reszta magów do piwnicy. Zajcewowie zostali wysłani (na nie-powrót) by zniszczyć jeden Patrol a Tania, by wezwać Śledzika na inny Patrol. W większości się udało; Zajcewowie zgładzili "swój" Patrol a potem wypadł na nich Śledzik (zwiali) ;-). Karradrael wierzy, że Mieszko nie żyje, więc Laureny nie ma po co zmieniać w Hunter-Killera. Tymczasem Anna Kozak uzależniła się od kralotha, Lidii padło morale, Kajetan i Antoni Przylaz poszli do Żonkibora zrobić Ogniwo Arazille by ściągnąć ją do Czelimina i wrócili z powodzeniem. Andrea kazała Laragnarhagowi przekształcić Mieszka w "ochotnika" (niech umrze jako bohater). Wioletta Bankierz i Julian Pszczelak po 'lekkiej' pomocy Melodii i Rafaela (pomysł Rafaela, aprobata Andrei) zintegrowali w Laragnarhagu wiedzę o Kompleksie i swoich wrogach i mnóstwo informacji (min. Karradrael czy wpływ Dagmary czy Irytka) trafiło do Andrei i Agresta. Na końcu misji oddział Andrei opuścili: Lidia, Kajetan, Alojzy, Anna, Marian, Andżelika, Rafael i Laragnarhag. No i 10 Zajcewów łącznie z Rodionem. Andrea utrzymała kontrolę, wróg o niej nie wie i wreszcie Andrea może atakować.

# Zasługi

* mag: Andrea Wilgacz, przez grupę niestandardowych i nieetycznych planów dookoła kralotha zmyliła Karradraela i jego patrole. Utrzymała grupę w ryzach, choć pozbyła się 8 istot. Jej siły - jak chciała - są "Świeca + sojusze" a nie mieszana. I ona dowodzi.
* mag: Andżelika Leszczyńska, skuszona do integracji z kralothem jako dywersja ("podwładny * mag kralotha"), przesuwająca Węzeł z Baranem, słuchająca się Rodiona Zajcewa... na końcu transportuje Oplem * magów do Millennium.
* mag: Rodion Zajcew, faktyczny dowódca Zajcewów, skłonił Andżelikę do współpracy z kralothem, potem wziął oddział Zajcewów robić dywersję; zniszczył jeden z oddziałów poszukiwaczych Karradraela. Potem wpadli na Śledzika i się poharatali...
* mag: Melodia Diakon, pełni rolę corruptora. Wpierw Andżelika, potem Wioletta x Pszczelak x Laragnarhag, potem Przylaz. A przy tym wszystkim urocza i uśmiechnięta. Scary.
* vic: Świeży Lilak, przyniósł informacje o składzie grup poszukiwawczych oraz o ich rozmieszczeniu, też: przeprowadza bezpiecznie * magów drogami Kręgu Życia by nikt nie odkrył siedziby Andrei.
* mag: Draconis Diakon, dostarczył Andrei wiedzę jak wezwać Arazille używając Ogniwa Arazille z Żonkibora. Też: dostał wiedzę o Irytce (pracuje nad antidotum) i dane z Kompleksu Centralnego by móc zwalczać Karradraela skutecznie.
* vic: Laragnarhag, kralotyczne maskowanie, pozorny pożeracz Mieszka, tymczasowy 'władca 7 * magów', agregator informacji i źródło uzależnienia Anny. W skrócie: MVP.
* mag: Marian Agrest, dzięki działaniom Andrei poznaje rozwiązania Irytki, stan Kompleksu Centralnego i wpływ Dagmary na Karradraela i ogólnie mnóstwo cennych danych.
* mag: Laurena Bankierz, dzięki planowi Andrei (przekonanie Karradraela, że Mieszko nie żyje) uniknęła losu _Caedus Eternus_ (Hunter-Killer). Więziona w Czeliminie.
* vic: Stalowy Śledzik Żarłacz, przyzwany przez Tatianę (zniechęcenie Karradraela do tego terenu) między Wtorkiem a Czubrawką... po czym, zaatakował Zajcewów i ich poharatał. Tak bardzo Śledzik...
* mag: Tatiana Zajcew, szczera lojalistka, przyzywa Śledzika tam, gdzie Andrei jest potrzebny, trochę podsłuchuje (nie Andreę) by zdać Andrei relację co się dzieje.
* mag: Mieszko Bankierz, "posiłek dla kralotha" oraz - na życzenie Andrei - przez Laragnarhaga przekształcany, by CHCIAŁ poświęcić się by wezwał Arazille. Przygotowywany na bohatera.
* mag: Dagmara Wyjątek, z badań Wioletty, Pszczelaka, Laragnarhaga wynika, że ona silnie wpływa na działania Karradraela w Kompleksie Centralnym. Jej psychika. Agrest już wie i może pułapkować.
* mag: Rafael Diakon, wraz z Melodią scorruptował Pszczelaka i Wiolettę, potem: w Unii z Laragnarhagiem tworzył maski i dywersje, też: nie zrobił absolutnie nic gdy Lidia go atakowała (co mu się chwali). Zimna ryba.
* mag: Marian Łajdak, w Oplu przetransportował grupę * magów do Millennium. Zintegrowany z Laragnarhagiem jako dywersja (podwładny * mag kralotha).
* mag: Lidia Weiner, she got broken (morale). Zaatakowała Rafaela ("defiler i potwór"), nie dała rady zamaskować sygnałów hipernetowych i opuściła z Kajetanem grupę Andrei.
* mag: Dalia Weiner, przerażona lekarka, która nienawidzi tego, co stało się Annie, ale została z Andreą; Andrea potrzebuje lekarza a ona jest naprawdę dobra. Bardziej pragmatyczna niż sama sądziła.
* mag: Julian Pszczelak, skuszony przez Rafaela (wiedza) i Melodię (erotyzm) do Wioletty (to był jej plan). Nadal poszedł do Andrei po pozwolenie... Still, dużo cennych danych o Kompleksie. Też: "podwładny * mag kralotha" (dywersja)
* mag: Wioletta Bankierz, chcąc zrehabilitować się u Andrei weszła w Unię z Laragnarhagiem i Pszczelakiem (zmanipulowana przez Melodię). Paradoksalnie, ma wysokie morale - widziała gorsze rzeczy.
* mag: Joachim Kopiec, podłączony do kralotha jako "podwładny * mag kralotha" (dywersja). Potem: zaskoczony reakcją Anny na kralotha (uzależnienie). Podglądał.
* mag: Tadeusz Baran, podłączony do kralotha jako "podwładny * mag kralotha" (dywersja). Też z Andżeliką przekierowali Węzeł do zameczku, by móc zamaskować * magów w piwnicy.
* mag: Kajetan Weiner, osłaniał Alojzego Przylaza do Żonkiboru, by stworzyć Ogniwo Arazille. Potem z Lidią Weiner opuścił oddział Andrei.
* mag: Alojzy Przylaz, miał swój dzień. Wpierw w Unii z kralothem (by dowiedzieć się o Ogniwie Arazille), potem stworzył Ogniwo w Żonkiborze osłaniany przez Kajetana, potem opuścił oddział Andrei by opiekować się uzależnioną od kralotha Anną Kozak.
* mag: Anna Kozak, wpierw podłączona do kralotha jako "podwładny * mag kralotha" (dywersja), w wyniku czego się odeń uzależniła. Rafael podejrzewa "krew, erotykę lub kralotha" w jej Wzorze. Odesłana do Millennium na leczenie.

# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Czubrawka, gdzie Zajcewowie zastawili pułapkę na oddział Karradraela i go zniszczyli. Niestety, przyciągnęli przez to tam Śledzika.
                1. Wtorek Śląski
                    1. Ośrodek historyczny
                        1. Ruina zamku, zamaskowana jako "baza kralotha". Krąg Życia ma sposób niezauważonego transportu do i z tego zamku.
                    1. Obrzeża
                        1. Stary Skup Zboża, gdzie Tatiana przyzwała Stalowego Śledzika Żarłacza i wiała aż miło ;-).
        1. Lubelskie 
            1. Powiat Tonkij
                1. Żonkibór, gdzie tworzone jest Ogniwo Arazille przez Przylaza. Ten teren jest pod władaniem (memetycznym wpływem) Arazille.

# Czas

* Dni: 2
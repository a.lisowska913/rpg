---
layout: inwazja-konspekt
title:  "Skradziona pozytywka Mausów"
campaign: powrot-karradraela
gm: żółw
players: kić, dzióbek
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [170118 - Ludzka prokuratura a magowie... (HB, AB, DK)](170118-ludzka-prokuratura-a-magowie.html)

### Chronologiczna

* [170115 - Klub "Dare Shiver" (SD)](170115-klub-dare-shiver.html)

## Kontekst ogólny sytuacji

## Punkt zerowy:

## Misja właściwa:

### Dzień 1:

Karolina wpadła do Silurii. Powiedziała, że ktoś był się z nią spotkać, ale Ignat ją zawrócił. Niestety, trochę czasu minęło. Siluria idzie do Ignata... a ten na siłowni. Nie kosztowało jej wiele wyciągnięcie z Ignata o co chodzi - Judyta Maus chciała się spotkać z Silurią.

* Chciałem Cię chronić... - Ignat, krzycząc
* Przed TĄ Mauską? - Siluria, z niedowierzaniem
* Hej, oceniam Twoje szanse właściwie - Ignat, złośliwie
* ...pościk - Siluria, nie mniej złośliwie

Siluria przeszła przez Infernię, poprosiła, by ta zajęła Ignata. Infernia się złośliwie uśmiechnęła i powiedziała Silurii, żeby ta szła SZYBKO. Siluria poszła. Szybko. I... zadzwoniła do Judyty Maus jak tylko przeszła na Primusa. Ta w pełnej radości i podekscytowaniu; chce się spotkać z Silurią. Siluria zaprosiła ją na KADEM, do sal gościnnych. Za pięć minut Judyta ma tu być!

* Ciebie Siluria nie będzie podejrzewać a Ilona robi z Tobą co chce, więc... - Ignat, mistrz przekonywania
* Dobra, Ignat, masz to jak w banku. - Paweł, całkowicie ignorując Ignata

Tymczasem Siluria czeka i przygląda się... widząc jadący motor z Judytą. Ona NIE MA SZANS wyhamować. ONA NIE UMIE JEŹDZIĆ NA MOTORZE! Siluria (KONFLIKT) używa innego maga KADEMu, by ten zatrzymał ten motocykl (-1 surowiec). REMIS. Judyta się rozwaliła na motorze... na szczęście stojący niedaleko Norbert ją natychmiast uratował i wyleczył.

Judyta cała załamana. Motor co prawda nie uszkodzony, ale... cóż, ona nie umie jeździć na motorze. Nawet wzmocnionym technomantycznie.

* Przyszłam prosić Cię o pomoc! - Judyta, cała w emocjach

Judyta powiedziała, że mają kłopot. Ktoś zajumał steampunkową lalkę. I to może zniszczyć małą grupkę Mausów. A Tymek powiedział, że KADEM jest zły, a ona mówi, że KADEM jest dobry i pomaga i ona założyła fanklub i Kornelia dołączyła i są już dwie i piszą kiepską poezję i opowiadają historie z życia Silurii... a ogólnie lalka miała biżuterię zrobioną przez Tymka ale ciężko nad nią pracował Maurycy i... a w ogóle Judyta pożyczyła motor od Tymka i on nie wie bo ona chce poprawić PR KADEMu u Mausów...

Judyta. Słowotok zagłady. Niestety, niewiele wie (nie traktuje się jej poważnie). Maurycy i Tymek nie będą zbyt przyjaźnie nastawieni a lalka ma następnego dnia występować i...

W sumie Siluria nie ma nic lepszego do roboty. Czemu nie pomóc The Maus Misfit. Kazała jej poczekać; pójdzie poprosić kolegę o pomoc. JEDYNY Sępiak, który na przekór całemu rodowi nie poszedł w lalki... on poszedł w golemy...

Paweł otworzył Silurii. Niechętnie - domyślił się, że to coś... podstępnego.

* Słuchaj, mam taką... uroczo rozbrajająco Mauskę. - Siluria
* W razie czego jakby Ignat pytał, byłaś obserwowana. - Paweł, zrezygnowany

Siluria zrelacjonowała opowieść Judyty.

* Jeśli wierzyć ulotce... szukamy lalki która śpiewa utwory steampunka... - Paweł, smętnie.

Siluria przekonała smętnego Pawła. Poszli do Judyty... a ta cierpliwie czeka. I próbuje naprawić motor... Judyta wyjaśniła podstawowe elementy i pokazała zdjęcie. Bardzo zaawansowany mechanizm; konstrukt wykonany steampunkowo. Jak zegary.

* Cóż Silurio, caryco moja droga, czas wykorzystać Twoje legendarne umiejętności detektywa - Paweł, baaardzo złośliwie
* Paweł...
* Co takiego, ekscelencjo? 

Judyta próbuje postawić motor na nogi. Paweł jej nie przeszkadza. Miły widok.

* Jest sens ruszać się z KADEMu? Niech ten Maurycy tu przyjdzie - Siluria "detektyw" Diakon

Judyta nie jest do końca przekonana, że jest w stanie przyprowadzić tu Maurycego. Nie do końca ufa KADEMowi... ale może dlatego, że nie zna Silurii ;-). Z ciężkim sercem, Siluria zdecydowała się pojechać z Pawłem i Judytą. Ale nie na motorze. Paweł prowadzi auto. Motor został wzięty na przyczepce i jedzie z nimi. Pojechali na obrzeża Kopalina, do chatki w okolicach Lasu Stu Kotów.

* Coś Ty zrobiła z tym kołem?! - Tymek, patrząc na swój motor
* No... samo klynkło... - Judyta, z rozbrajającą minką

Tymek i Judyta zaczęli debatować o motorze itp, a w tym czasie Maurycy podszedł do Silurii. Zaciekawił się, kim oni są i dlaczego Judyta uznała, że będą w stanie pomóc.

* Jesteście z... Powiewu? - Maurycy, zaciekawiony
* Nie, z KADEMu. - uśmiechnęła się Siluria
* O, cholera... skąd Judyta ma znajomości na KADEMie? - Maurycy, bardzo zaskoczony
* To... długa historia - Siluria, nie chcąc mówić o Dare Shiver

Maurycy zaprosił ich do środka. Chata Maurycego jest dość spora; składa się z wielu zegarów, mechanizmów i... no, "steampunkowej technomancji". Maurycy wyjaśnił, że jest technomantą, ale przy okazji jest też zegarmistrzem. Judyta widząc zainteresowanie Silurii zażądała, by Maurycy dał Silurii zegar w prezencie. Starszy mag się uśmiechnął i wyciągnął mały, ładny zegar z szafki. Siluria odmówiła prezentu, ale Maurycy nalegał. Judyta cała zadowolona.

Zapytany o co chodzi, Maurycy powiedział, że złożył z kół zębatych i mechanizmów typu clockwork bardzo interesującą pozytywkę. Tymek pomógł przez zbudowanie źródeł energii i zasilania; pozytywka ta potrafi pobrać odpowiednio dostosowany Kryształ Opowieści Mausów i zmienić go w piosenkę. Silurię to zainteresowało. Niestety, kogoś JESZCZE to zainteresowało; ktoś się wbił do chaty i ukradł pozytywkę. A już poszedł przekaz do kilkudziesięciu magów (Mausów i zaprzyjaźnionych), że będzie pokaz tego w przestrzeni Świecy.

No ok. Są ślady włamania - typowa robota Zajcewów. Ale Maurycy twierdzi, że nikt nie wiedział gdzie ta pozytywka się znajduje. Plus, face it: to nie jest nic poważnego. To nie jest nic wielkiego. To tylko... pozytywka. A Maurycy nie ma wrogów, nie jest dość ważny. Sam pokaz... to był pokaz dla Mausów i zaprzyjaźnionych magów. Cel: fun, pokazanie ballad Mausów w inny sposób. Pozytywka. Celem pokazać coś innego, coś fajnego, show art, zdobyć wsparcie / finansowanie...

Co prawda Kryształu Mausów każdy może użyć ale nie każdy kryształ jest skalibrowany. Wszystkie kryształy także zniknęły; nie mają w tym momencie niczego. A sama pozytywka była budowana kilka miesięcy, to był projekt hobbystyczny. W tym samym czasie toczy się konkurencyjna impreza na Świecy. 

22 magów było zainteresowane pozytywką i przyszłoby na występ.

Silura i Paweł poszli konferować na bok; wymyślili, że przy małej ilości czasu warto zrobić imprezę na... KADEMie. Wzięli więc potem Judytę i zaproponowali jej co następuje: niech impreza będzie na KADEMie. Niech nie będzie na terenie neutralnym Świecy. Niech ta impreza będzie w hali symulacji; dzięki temu KADEM zyska (jako benevolent) i Mausowie uratują swoją skórę. Judycie się aż oczy zaświeciły.

* Będę mogła też tam być? ZOBACZĘ KADEM OD ŚRODKA?! - Judyta, zapalona
* Tylko jeśli Maurycy się zgodzi - Siluria, zrezygnowana

Judyta jest świetnym narzędziem wpływu. Tymek protestował, ale Maurycy się zgodził i gorąco podziękował Silurii i Pawłowi za pomoc. Tymek jest cały głęboko urażony. Silurię to interesuje; chce się dowiedzieć o co chodzi. Okazuje się, że Tymek zawsze CHCIAŁ być magiem KADEMu, ale KADEM go nie chciał. Więc on się tak starał, w ogóle... a oni weszli tutaj że niby na białym koniu i w ogóle. I Judyta zanegowała jego pracę i w ogóle. Tymek żywi niechęć do KADEMu, ale nic ofensywnego. Czysta zawiść. Z tym da się żyć.

Oki, ze strony Mausów jest buy-in. Czas iść na KADEM i to rozwiązać...

Wpierw, Marta...

* Ja pukam, Ty mówisz - Paweł do Silurii

Siluria i Paweł przedstawili to jako:

* Mausowie (trójka) zwróciła się do KADEMu o pomoc - ocieplenie relacji KADEM - Maus
* Ogólne podbudowanie wizerunku KADEMu i pokazaniu potęgi militarnej KADEMu, by wywrzeć Wrażenie
* Okazja do niewielkiego zarobku na Mausach; każdy mag KADEMu może coś sprzedać turystom
* KADEM nie odmawia pomocy. Warto to pokazać; opłaci się na przyszłość.

Quasar jest zdecydowanie przeciwna. Marta ma też wątpliwości logistyczne. KONFLIKT (5#2v5->S). Cóż, skoro można pokazać z czym się mierzą... Marta się zgodziła. Będzie.

Franciszek dostanie polecenie służbowe się tym zająć. Będzie zadowolony.

Czas na Ignata. Wziął go na stronę Paweł. 

* Ignat, jest sprawa. Wpakowałeś mnie i to zdrowo. Judyta potrzebowała pomocy, Siluria się zgodziła. Im zaginęła taka lalka. Jak my tej lalki nie znajdziemy do jutra, impreza Mausów przenosi się na KADEM - Paweł, łżący jak z nut
* Co?! Ja... jak na KADEM? - Ignat, w szoku
* Siluria się zgodziła pomóc, lalki nie znajdziemy, Siluria załatwiła sprawę z Mausami i Martą... jakbyśmy znaleźli lalkę, może da się pokombinować
* CHOLERA! Czyli muszę ją znaleźć do rana!? - Ignat, ze szczęką przy ziemi
* Mogę Ci pomóc, drony... - Paweł, złośliwie
* Dobra, NIE! KURDE, NIE! Znajdę tą lalkę choćbym się miał... - Ignat, w panice. Wybiegł z pokoju.

Paweł zatarł ręce. Zadziwiająco dobrze poszło.

Wieczorem Ignat przyszedł do Pawła. NO QUESTIONS! Przyniósł sprawną, działającą lalkę... Paweł cały szczęśliwy. Ignat wyszedł...

Paweł poszedł z lalką do Silurii i dał jej tą lalkę. Ignat Pawła nie śledzi. Zwiał ;-). Siluria się bardzo, bardzo ucieszyła. Zaciekawiło jej kto miał tą lalkę i dlaczego; niestety, Paweł nic nie wie. Ignat - zdaniem Pawła - wyglądał, jakby go to kosztowało. No i Pawła interesuje bajeczka "czemu nie możemy tego wycofać".

Marta się zgadza, żeby oddać Mausom lalkę a jednocześnie zrobić imprezę.

Siluria poszła szukać Ignata; nie ma go. Quasar, zapytana, powiedziała, że Ignat jest poza KADEMem. Na Primusie. Siluria poświęciła (-1 surowiec) i wydobyła coś do namierzania komórek. Czas znaleźć Ignata. Ignat jest nieźle schowany. Ignat jest w paskudnej mordowni - Mordownia. Siluria przygotowała sobie więc zaklęcie. Zaklęcie mające sprawić, że jak jakikolwiek facet podejdzie do Silurii, będzie chciał jej służyć. Ignat ma to wyłączone.

Weszła, wyciągnęła Ignata i wyszła. Ludzie... cóż. Nic im się nie stało ;-).

* PIJĘ! Bo mi Mausów ściągasz na KADEM... - Ignat
* Chodź! Pogadamy! - Siluria - Po co pijesz PIWO?
* Fraternizuję się z plebsem... wciąż lepiej niż Mausowie czy tienowaci - Ignat

Siluria powiedziała Ignatowi, że cieszy się, że on znalazł tą lalkę. Ale pyta, czy nie wykosztował się za bardzo. Nie. Ignat... nie do końca chce powiedzieć. Ignat powiedział, że dzięki temu przynajmniej nie będzie Mausów na KADEMie. Siluria powiedziała, że Marta powiedziała, że tego nie da się już odkręcić.

* Czyli JEDNOCZEŚNIE Mausowie odzyskają durną pozytywkę ORAZ będą Mausowie na KADEMie. - Ignat, wściekły
* Tak... - Siluria, ze smutkiem
* Silurio, idę pić... - Ignat, nie ukrywając wściekłości

Siluria zdecydowała się skonfliktować. Ignat normalnie nie chodzi w takie miejsca, plus... ucieka od Silurii? Przeprasza za Infernię. Sukces. 

* Mówiłem 'nie idź gadać z tą Mauską'. Mówiłem. Nie mogłaś mnie RAZ posłuchać? - Ignat, rozgoryczony
* Nie wiem czy wiesz, ale coś mamy. Jednym z powodów dla których Marta się zgodziła jest to, że pokażemy im potęgę KADEMu. - Siluria, łagodnie

Ignat opowiedział, o co chodzi. On nie lubi zarówno Mausów jak i magów Świecy (tienowatych). W związku z tym dał znać drobnym zbirom w Świecy o pozytywce i kupił popcorn. Liczył na porządną nawalankę między Świecą i Mausami... a przez Silurię i Pawła nie wyszło. I jeszcze KADEM na tym ucierpi :/. Do tego musiał zapłacić i odkupić od paserki pozytywkę, bo sprawa się zrobiła cholernie polityczna - jeśli Marta się zainteresowała, to już musi pójść w taki sposób.

...no ok, KADEM ma lepszą opinię niż miał. Ale nadal... u Mausów. Trzech nieważnych Mausów i kilku mniej nieważnych Mausów :/.

Silura sprzedała Ignatowi, żeby mógł zrobić wrażenie na Mausach i postrzelać artyleryjsko do istot Fazy. Trochę mu się humor poprawił.

Paweł sprzedał Judycie koncept, że Siluria jest przywódczynią potężnego i dobrze ukrytego skrzydła magów na KADEMie; opowiedział jej kilka historii SuperSilurii. Oczywiście, to ściema. Oczywiście, Judyta uwierzyła...

### Dzień 2:

Odbyło się wystąpienie. KADEM zyskał, Mausowie zyskali, Maurycy odzyskał pozytywkę, doszło do wymiany pieniędzy ;-). A i Franciszek mógł pogawędzić z rodzinką.

# Progresja

# Streszczenie

Ignat się nudził i zdecydował się nadać zbirom w Świecy namiar na pozytywkę kilku nieważnych Mausów. Miał nadzieję na popcorn. Niestety, Siluria i Paweł ściągnięci przez Judytę (fangirl Silurii) zepsuli ten piękny plan i zmusili Ignata do odzyskania pozytywki. Co więcej, Franciszek oprowadził Mausów po KADEMie; KADEM dostał silny wzrost szacunku i przyjaźni a Ignat poszedł pić.

# Zasługi

* mag: Paweł Sępiak, przekonał Judytę o epickości Silurii fałszywymi opowieściami, pomysłodawca imprezy na KADEMie i mistrz przekonywania Ignata odnośnie odkręcania planów ;-).
* mag: Siluria Diakon, przekonana przez Judytę by pomóc dwóm pacyfistycznym Mausom przekształciła to w wielką imprezę na KADEMie poprawiającą relacje między KADEMem a Mausami.
* mag: Ignat Zajcew, mastermind chcący skłócić Mausów i Świecę (nadał zbirom pozytywkę Mausów do ukradnięcia); niestety, wykosztował się paskudnie odzyskując ją gdy sprawa stała się polityczna.
* mag: Infernia Diakon, zatrzymała Ignata przed śledzeniem Silurii; groźniejsza w walce bezpośredniej od Ignata, acz nie bardzo bojowa.
* mag: Judyta Maus, założyła fanklub Silurii, doprowadziła do uratowania sytuacji Mausów. Nie umie jeździć na motorze. W sumie, katalizator a nie jednostka działająca.
* mag: Tymoteusz Maus, kiedyś chciał być magiem KADEMu ale mu odmówili. Współpracował z Maurycym by stworzyć steampunkową pozytywkę. Ogólnie, taki jęczliwy typ.
* mag: Maurycy Maus, zegarmistrz kochający sztukę i muzykę, który z Tymkiem zbudował steampunkową pozytywkę. 4x-5x-letni pacyfista, pobłaża Judycie i daje sobie wejść na głowę.
* mag: Gala Zajcew, paserka od której Ignat musiał odkupić pozytywkę. Zdobyła dobrą cenę; w sumie najwięcej ona zyskała na całej tej sprawie. Nieobecna na misji.
* mag: Franciszek Maus, miał okazję oprowadzić członków swego rodu po KADEMie i wzbudzić w nich bojaźń potęgi KADEMu. Też: posocjalizować się. Ogólnie, miał dobry dzień ;-).

# Lokalizacje

1. Świat
    1. Faza Daemonica
        1. Mare Vortex
            1. Zamek As'caen
                1. KADEM Daemonica, zwiedzony przez sporo zainteresowanych Mausów
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński 
                1. Kopalin
                    1. Centrum
                        1. KADEM Primus
                            1. Zewnętrzny krąg
                                1. Sale spotkań, gdzie w końcu Siluria zaprowadziła Judytę i skąd Judytę zawrócił wcześniej Ignat
                    1. Las Stu Kotów
                        1. Osiedle Leśne
                            1. Chatka Zegarowa, gdzie mieszka sobie Maurycy Maus wraz ze swoimi licznymi zegarami i eksperymentami zegarowymi
              
# Czas

* Opóźnienie: 2 dni
* Dni: 2
---
layout: inwazja-konspekt
title:  "Aplikanci widzieli gorathaula"
campaign: prawdziwa-natura-draceny
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [140928 - Policjant widział anioła (PT)](140928-policjant-widzial-aniola.html)

### Chronologiczna

* [140928 - Policjant widział anioła (PT)](140928-policjant-widzial-aniola.html)

### Punkt zerowy:

### Misja właściwa:

Dwa, trzy tygodnie zajęły Paulinie doprowadzenie Nikolę Kamień ("anioła") do jakiegoś stanu normy psychicznej. Fakt, że Sebastian ją zostawił był dla niej niezwykle bolesny. Paulina przeformowała pamięć Nikoli - nie zostawiła jej całej pamięci. Wymazała swoją twarz z pamięci Nikoli, zaznaczyła, że Sebastianowi mogła stać się krzywda (i dlatego zniknął) oraz wyindukowała, że jako anioł musi dalej pozostać w ukryciu. Zdecydowała się też zdestabilizować wzór Nikoli, mając nadzieję na to, że potem wróci i ją naprawi. Ale dzięki destabilizacji uniemożliwiła błędnym wzorom oraz jej własnej wiązce nekromantycznej się w niej sensownie zagłębić. Niestety, stabilizacja to przede wszystkim mnóstwo czasu a obecność Sebastiana by bardzo pomogła...

Paulina postanowiła znaleźć Sebastiana i sprawdzić co jemu zrobiła ta dziwna EAMka. Znalazła konkretną chorobę (magiczną, mało objawową) i spróbowała go poszukać w swoich kontaktach medycznych i magicznych. Zapytana czemu szuka odpowiadała, że chce podać mu lek i prosi o dyskrecję. (9 vs 6 \|\|8 -> 13 > 8) W wyniku udało jej się znaleźć Sebastiana. Jej kontakty zachowały dyskrecję, dowiedziała się dokładnie jego lokalizacji. Znajduje się w okolicy Piróga Górnego, niewielkiej miejscowości. Jest w bardzo dobrej formie, zdrowy i wesoły. Aktualnie tam mają miejsce egzaminy kwalifikujące na uczniów terminusów.

Paulina jest w kropce. Jeśli on jest szczęśliwy, nie jest pewna, czy chce przysporzyć mu cierpienia. Paulina nie wie jak wygląda sytuacja, więc wysłała tam Marię na przeszpiegi. Maria ma spróbować go zlokalizować i dowiedzieć się jak najwięcej na temat Sebastiana i całej sytuacji, egzaminów... na ludzi nigdy nikt nie patrzy.
Trzy dni później Paulina dostała informację od Marii. Maria usłyszała o "żółwiaku z mgieł" (użyła szklanki), znalazła Sebastiana i zobaczyła, jak Sebastian oferuje im swoją pomoc. Dodatkowo potwierdziła - on nie jest jednym z aplikantów. Aha, i kilku ludzi "zniknęło". To jest, magowie ich zniknęli.
Sama Paulina sprawdziła archiwa i biblioteki. Żółwiak z mgieł implikuje gorathaula, śmiertelnie niebezpiecznego taumatożercę.

Paulina zastanowiła się chwilę. Nie ma pomysłu, jak do tego wszystkiego podejść. Skonsultowała się z Marią, która zaproponowała wrzucenie tematu do ich kółka paranormalnego. Paulina jest tak zdesperowana, że się zgodziła, pod warunkiem nie złamania specjalnie Maskarady. Maria wzięła to na siebie. Zapytała o dokładny problem do rozwiązania. Gdy Paulina powiedziała, że chce tam wejść, Maria zaproponowała przebranie się. Paulina zgodziła się na ufarbowanie włosów i tego typu metody. Ale historyjki - jak tam wejść by to było sensowne - Paulina nie była w stanie prawidłowo wymyślić. No i tu właśnie pomogło kółko paranormalne. Zaproponowali Paulinie, by zastanowiła się czy wśród jej znajomych ktoś nie ma rodziny wśród ludzi.
Paulina scross-checkowała znajomych lekarzy magicznych SŚ pod kątem nawet dalszej rodziny i poprosiła o poświadczenie, że ona może tu być, bo dzieje się coś bardzo złego. (7+2 kółko vs 8 -> 5) Znalazła kogoś. Nie maga, ale znalazła. Mag emeryt (magowie którzy stracili moc), który napisze jej i wystawi takie poświadczenie, ale ona musi pomóc jego przyjacielowi. Jest to też mag emeryt, ale nie ma dla niego pomocy medycznej. Paulinie zajmie to tydzień lub dwa bezpośrednio po tej misji.
Paulina się zgodziła. Z blond włosami i uzbrojona w papier mogła ruszyć do Piróga Górnego.

Piróg Górny, dwa dni po wiadomości od Marii. Pomnik rycerza na koniu, kilkanaście domów na krzyż, mały placyk i spokój. Maria przekazała Paulinie następujące informacje:
- po pierwsze, wygląda na to, że młodzi aplikanci dostali w kość. Maria widziała ich liżących rany ze spuszczonymi ogonami. "Ale to powinno działać". Aplikanci o wszystko obwiniali "tą dziewczynę", czyli jedną z grupy, że to niby była jej wina. 
- po drugie, jest tu więcej magów niż ta piątka. Oni są obserwowani przez coś, co wygląda jak dron... ale jak dron się nie zachowuje.
- zapytana przez Paulinę, potwierdziła: Sebastian kręci się w pobliżu. Daje im się zobaczyć. Zupełnie jakby czekał na ruch ze strony aplikantów.
Paulina nadała Marii nowy temat: czy Maria może się dowiedzieć czegoś na temat tych ludzi, którzy zniknęli? Gdzie, w jakich okolicznościach, dlaczego...

Paulina sprawdziła od kiedy są anomalie pogodowe - od 8 dni. A po 14 dni egzamin na ucznia terminusa się kończy z wynikiem negatywnym. Czyli mają 5 lub 6 dni, więc powinni być dość zdesperowani. Poprosiła Matię, by tak postawiła ślady zniknięcia ludzi by łatwo można było doprowadzić aplikantów do tych śladów. I niech te ślady prowadzą docelowo do Pauliny.
Tymczasem sama uda się do Sebastiana. Założenie: nie będzie jej pamiętał, bo EAMka coś mu zrobiła...

Sebastian jest w pokoju "motelowym" (tak, jest motel w Pirógu Górnym, ale nie hotel). Paulina poczekała, aż wyjdzie do sklepu. Zagadała od "tien Tecznia" i na jego twarzy pojawił się szok. Rozpoznał Paulinę. Paulina spytała co on tu robi, on się zrewanżował tym samym pytaniem. Paulina wybadała go - nie ma w nim w ogóle uczucia do Nikoli, ale ją pamięta. I wie o dronie. Zapytana ponownie, Paulina powiedziała, że jest tu bo ją poproszono, bo ludzie znikają. Sebastian zaprzeczył - nic im się nie stało, to zwykła sprawa z egzaminem. Przyciśnięty dodał, że przecież nic im nie będzie, to tylko do zwiększenia napięcia na egzaminowanych magów. To MARIA znająca się na procedurach magów wie, że za tym stoją magowie, ludzie są spokojni. Paulina, mimo swoich szczerych chęci, nie dała radę dowiedzieć się o co chodzi Sebastianowi i jakie są jego plany - ale wie, że w okolicy działa znowu EAMka i że nie ma ona na celu drugiego "anioła".

Paulina zaryzykowała "wild guess" - czemu on znowu współpracuje z EAMką. Trafiła. Sebastian się zaczął bronić - ona oferuje moc, potęgę, rytuały, rzeczy jakich nie jest w stanie dać mu nikt inny. A tym razem żaden człowiek nie ucierpi. Tym razem nikomu nie stanie się żadna krzywda. Upewnił się. Koszt jest praktycznie minimalny, ale zysk jest niewyobrażalny.
Spytał, czemu EAMka nie zaproponowała jej współpracy. Paulina parsknęła, że ona by tego nie chciała. Sebastian bardzo poprosił, by mu nie przeszkadzała. To jego ostatnia szansa. Paulina nie obiecała. Zapytany, powiedział Paulinie, że sam wziął eliksir usuwający uczucie do Nikoli. Nie kazała mu. Po prostu, do niczego by to nie prowadziło. A jeśli się do niej nie zbliży, może ten epizod zamknąć i nie zrobi jej krzywdy.

Paulina jest w kropce. To, co chce osiągnąć - dowiedzieć się co się dzieje z tymi ludźmi i gdzie się znajdują oraz dowiedziec się, co knuje EAMka. Niestety, nie jest w stanie bezpośrednio dostać się do EAMki a też nie do końca ma jak tych ludzi znaleźć bez użycia magii (czego robić nie chce - nie chce zostać karmą dla gorathaula). W związku z tym poprosiła Marię, by ta tak połączyła ślady zniknięcia ludzi, by młodzi kandydaci na terminusów znaleźli Paulinę. 

Następnego wieczora aplikanci pojawili się w motelowym mieszkaniu Pauliny. Tzn. Paulina weszła na otwarte mieszkanie, gdzie już czekało dwóch ponurych aplikantów, gotowych do tego by wpierw strzelać, potem pytać, na pełnych tarczach i różdżkach. Próbowali nieudolnie zastraszyć Paulinę, lecz gdy ta pokazała papier, zmiękkli - ona ma prawo tu być i nie stanowi dla nich zagrożenia. Z rozmowy wyniknęło, że to oni porwali tych wszystkich znikniętych ludzi i wykorzystali ich (nasycając energią magiczną) by mieć przynętę na gorathaula - i stąd wiedzą, że gorathaul nie zabija (bo nie zabił żadnego człowieka). Na to wszystko weszła kandydatka, przedstawiła się jako Ania i powiedziała, że Paulina (która zaoferowała lekarską pomoc) może pomóc ludziom poranionym przez gorathaula. Wyszły konflikty między Anią i Joachimem i Joachim skutecznie sterroryzował Anię; nic więcej nie powie przy Paulinie.

Maria poinformowała Paulinę, że na zewnątrz jest jeszcze 2 aplikantów-terminusów. Paulina zaoferowała im by weszli, na co Joachim zareagował niezadowoleniem. W rozmowie wyszło, że Ania zawiodła - "normalne" badane przez nią techniki walki z gorathaulem nie zadziałały, mimo, że ten miał być low power. Wyszło też, że Ania to ludzistka. Joachim jako jedyny jest zupełnie pełnosprawny (nie ranny), inni są pokaleczeni. Paulina zaoferowała pomoc ze swoich surowców. Joachim przedstawił to jako "kto się odważy wykorzystać pomoc nieznanej czarodziejki i nie wiecie co ona wam zrobi", na co Ania pierwsza się zgłosiła, uzyskując jeszcze jedno spojrzenie pełne wściekłości ze strony Joachima. Paulina też powiedziała im, że coś ich śledzi. Joachim dodał, że mają jeszcze 4 dni a czwarty właśnie mija. I muszą znaleźć tego gorathaula i go pokonać, niezależnie od tego jaki jest koszt.

Pojechali taksówką (większą taksówką) do miejsca gdzie są ludzie (kościoła). Oczywiście, Joachim nie płacił tylko "zapłaciłem ci" magią. Paulina zapłaciła ze swoich pieniędzy. Więc, na miejscu okazało się, że aplikanci objęli kościół i ludzie im służą. Czemu kościół? Bo lapis, więc promieniowanie magiczne nie wycieka tak na zewnątrz. Pierwsze spojrzenie na nich, Ania została wysłana do apteki z listą rzeczy jakie ma kupić i załatwić. Paulina będzie współpracować z Salazarem Bankierzem, pełniącym rolę lekarza magicznego i zaczyna analizę i czarowania, przy okazji ucząc Salazara jak to się robi. Joachim powiedział, że nie ma na to zupełnie czasu a Paulina odpowiedziała, że owszem, czas jest. Krótka pyskówka. Joachim zakazuje swojemu zespołowi współpracować z Pauliną bo mają ważniejsze rzeczy do zrobienia, jednak Paulina zaczyna dyskusję i się postawiła. (8 v 9 -> 7) Niestety, Joachim za mocno ich trzyma; skupili się na swoich "ważniejszych" sprawach a Paulina pracuje samotnie (poza tym, co już Ania zdążyła jej przynieść). W połączeniu umiejętności ludzkich i magicznych udało jej się daleko doprowadzić sukces, dużo lepiej są wyleczeni i przy mniejszej aurze niż ci magowie mimo że oni byli pułapkami (14 vs 10). Ale tanio nie było.

Wczesne południe. Dla odmiany, Paulina mogła się wyspać. Obudziła ją rozmowa, gdy Joachim powiedział Sebastianowi Teczni, że się zgadza, że będzie jak Sebastian chce. Sebastian dał deadline na jutrzejszy wieczór i "wszystkie problemy będą z głowy". Jak Sebastian wyszedł, Joachim powiedział zespołowi, że wiedzą co teraz robić i mają to wszystko "załatwić" na dziś wieczór. Salazar zaopiniował, że to nieetyczne. Joachim powiedział, że jest etyczne, bo jak Ania powiedziała, "korzystają z dostępnych źródeł". Nikt nie wie, że Paulina cokolwiek słyszała. Paulina odczekała jakieś 30 minut, ubrała się i poszła do pacjentów (ludzi). Są w dobrej formie, Salazar podziękował bardzo Paulinie i pogratulował jej umiejętności. Paulina zaproponowała, że go tego nauczy ale Salazar powiedział, że nie ma pacjentów. Joachim, zadowolony, zaznaczył, że przecież jest Anna która się wcześniej już zgodziła. Powiedział Paulinie że za pomoc ludziom i uczenie Salazara puści ludzi wolno. Paulina się wściekła i powiedziała, że robi to dla Salazara i Ani, nie dla Joachima. Ten bezczelnie się uśmiechnął i powiedział, że nie ma to dla niego znaczenia.

Przyszła Ania. Paulina kazała się jej rozebrać i na niej zaczęła demonstrować i tłumaczyć Salazarowi co i jak należy zrobić. Zaznaczyła, że np. złamanie nogi wymaga 7 jednostek quark czystej magii a przy użyciu ludzkich technik a potem magii tylko 4 jednostek quark. Ania jest poparzona magicznie ("nigdy nie wiedziałam, że gorathaul może tworzyć parzącą magicznie mgłę, nie było tego w źródłach... musi być sztuczne utrudnienie naszych prowadzących"), uszkodzona fizycznie i widać na jej ciele ślady bicza i bicza energetycznego ("odrobina dyscypliny naszego ukochanego przełożonego"). Paulina dała radę jej pomóc i w wyniku pracy nad Salazarem (10 v 8 -> 9) przekonała go by też zaczął interesować się ludzkimi technikami i ludźmi, nie tylko czysto magicznymi technikami.

Podczas pomagania Paulina spróbowała też dowiedzieć się czegoś odnośnie tego co tu się dzieje. Salazar powiedział, że mają możliwość ułatwienia sobie zrobienia czegoś do zniszczenia gorathaula przy pomocy zewnętrznego maga. Ania stwierdziła, że koszt jest za wysoki. Zapytany Salazar przez Paulinę powiedział, że chodzi o 2 przedmioty niemagiczne "z domu", o wartości sentymentalnej. Jeden do stworzenia artefaktu, drugi do wstawienia przez tego maga w gorathaula - w ten sposób będzie osłabiający rezonans. Paulina spytała, czy ten rytuał i artefakt przeszukiwała Ania wcześniej - ta potwierdziła, lecz nic nie znalazła. Salazar zauważył, że może przecież Ania nie jest tak dobra w wyszukiwaniu hipernetu jak sądziła lub nie ma uprawnień. Paulina nasunęła na EAM, ale Ania zbiła - nie ma uprawnień do EAMowych danych.

Paulina zdecydowała się na szczery i odważny krok - opowiedziała im o złowrogiej EAMce o tajemniczych celach. Nie wiadomo, jakie ona ma cele. Może chce przetestować rytuał, może chce zrobić coś innego. Jak Paulina zaznaczyła, nie wiadomo czy przy gorathaulu manipulowali jedynie egzaminatorzy. Więc Paulina uważa, że lepiej by EAMka nie dostała tego, co chce - w końcu od małych osób, małych kroczków czasem dużo zależy a jak nikt nigdy jej się nie postawi to ona zawsze będzie wygrywać. Przekonała (7 vs 8 -> 11) Salazara, że on ma rację, że to nie tylko nieetyczne ale i niewłaściwe. Joachim nie otrzyma od Salazara i Ani artefaktu.

Reszta odbyła się już poza Pauliną. Podczas spotkania zespołu Salazar stwierdził, że on i Ania nie dadzą swoich przedmiotów. To nieetyczne. Nad etyką się nie głosuje. Joachim zaprotestował, ale co zrobi Bankierzowi. Stwierdził, że dostanie czego chce - będzie się znęcał nad Anią aż Salazar zmiękknie. A Salazar po prostu skontaktował się ze swoim rodem i powiedział jak wygląda sytuacja. Natychmiast doszło do wprowadzenia audytorów egzaminu i:
-- Sebastian dał radę się niezauważony wycofać; BARDZO pomogła EAMka. Bez niej by wszystkie jego działania wyszły.
-- Salazar i tylko Salazar zdał egzamin. Pozostali zostali uznani za winni konspiracji (a o Anię się nikt nie upomniał bo zbyt nieważna).
-- Okazało się, że jednym z dwóch egzaminatorów jest... ojciec Joachima, tien Wojmił Kopiec.
-- Okazało się, że ktoś jeszcze manipulował gorathaulem i coś zmieniał. Zrzucono winę na egzaminatorów za niedopatrzenie.

Paulina zadowolona. Osiągnęła swój cel, wie, jaką decyzję podjął Sebastian, nawiązała przydatny kontakt (Salazar Bankierz) i nieprzydatny kontakt (Anna Kozak). Oddaliła się do domu magów seniorów, pomóc przyjacielowi starego eks-maga który wypisał jej papier...
 
# Pytania:

N/A
 
# Streszczenie

Nikola jest w fatalnym stanie; Paulina zrobiła co mogła, ale pomógłby Sebastian... więc Paulina wybyła do Piroga szukać młodego nędznego terminusa. Tam Sebastian dla Karoliny manipulował egzaminem kandydatów na terminusów by uzyskać od nich artefakty osobiste (plan Karoliny). Paulina nauczyła młodego Salazara, że ludzistyczne metody leczenia są przydatne, zniszczyła dominację Joachima Kopca i unieszkodliwiła plany Karoliny przez sprawienie, że Salazar zaeskalował do swojego Rodu. A potem poszła spłacać przysługi ;-).

# Zasługi

* mag: Paulina Tarczyńska jako główna rozgrywająca brużdżąca EAMce i pomagająca niewinnej Nikoli
* czł: Maria Newa jako osoba pomagająca Paulinie w trudnych chwilach
* mag: Nikola Kamień jako dziewczyna potencjalnie bez przyszłości ze złamanym sercem
* mag: Sebastian Tecznia jako ten, który błaga "EAMkę" o jeszcze jedną szansę
* mag: Anna Kozak jako uczennica na terminusa z Rodziny Świecy która nie dała się złamać
* mag: Joachim Kopiec jako uczeń na terminusa zdaniem Pauliny na terminusa się nie nadający
* mag: Wojmił Kopiec jako ojciec Joachima który troszkę złamał zasady etyki (nie wtrącanie się)
* mag: Salazar Bankierz jako uczeń na terminusa który chce się nauczyć a nie "wiedzieć lepiej"
* mag: Karolina Maus jako "EAMka", czyli złowrogi cień czuwający nad tym wszystkim

# Progresja:

Paulina Tarczyńska: zyskała przydatny kontakt (Salazar Bankierz)
Paulina Tarczyńska: zyskała nieprzydatny kontakt (Anna Kozak)
Salazar Bankierz: zdał egzamin na terminusa, uznany za whistleblowera w sprawie "jak nieuczciwie zdać egzamin"
Anna Kozak: oblała egzamin na terminusa i uznana za konspiratorkę w sprawie "jak nieuczciwie zdać egzamin"
Joachim Kopiec: oblał egzamin na terminusa i uznany za konspiratora w sprawie "jak nieuczciwie zdać egzamin"
Wojmił Kopiec: dostał silny OPR i nieufność za manipulowanie i konspirowanie w sprawie "jak nieuczciwie zdać egzamin"; sprawa jest poważna.

# Lokalizacje:

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Piróg Górny, gdzie jest kwalifikacja młodych magów na terminusów (egzamin) w którym "uczestniczy" gorathaul
                    1. Centrum
                        1. Motel Przypadek, w którym pokój wynajął tien Sebastian Tecznia... i Paulina.
                        1. Kościół Serdecznego Serca, gdzie magowie-praktykanci zrobili sobie tymczasową bazę i umieścili tam wszystkich ludzi - przynęty.
                        1. Pomnik rycerza na koniu, charakterystyczny dla Piroga Górnego

# Idea:

-- poszerzenie kontaktów Pauliny
-- wprowadzenie postaci Ani Kozak
-- zdobycie decyzji i stanu Sebastiana Teczni

# Czas:

* Opóźnienie: 16 dni
* Dni: 8
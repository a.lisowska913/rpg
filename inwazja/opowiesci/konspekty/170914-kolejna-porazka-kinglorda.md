---
layout: inwazja-konspekt
title: "Kolejna porażka Kinglorda"
campaign: powrot-karradraela
players: kić, dust, raynor, foczek
gm: żółw
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [170830 - "Wdzięczność" Iliusitiusa (HB, KB, DK, AB, PK, HS)](170830-wdziecznosc-iliusitiusa.html)

### Chronologiczna

* [170830 - "Wdzięczność" Iliusitiusa (HB, KB, DK, AB, PK, HS)](170830-wdziecznosc-iliusitiusa.html)

## Kontekst ogólny sytuacji



## Punkt zerowy:

## Misja właściwa:

**Pewien czas temu**:

* Mordred, Rafael. Mordred chce od Rafaela naprawienie Henryka, ale tak, by się dowiedzieć wszystkiego.
* Rafael potraktował naprawienie Henryka jako wyzwanie. Kinglord zrobił świetną robotę.
* Rafael przekształcił Henryka w Mausa, potem połączył Diakona i Blakenbauera by wyciąć wpływy Kinglorda.
* Henryk obudził się jako "Henrietta". Chce do Millennium. Millennium go nie chce :D.

**Teraz**

* Hektor znalazł stronę internetową "Piotra"
* Hektor kontaktuje się z Piotrem. Dowiaduje się o bombie.
* W ścianie martwy Bankierz
* Hektor widzi aurę Arazille i Iliusitiusa - po ostrzeżeniu Arazille
* Wezwanie terminusów
* Hektor jest jedynym co czuje
* Piotr przekonał terminusa że to nie on. Błąd proceduralny?
* Hektor jest aresztowany. Bez sensu.
* Hektor przesłuchiwany przez siły antymausowe. Trafia go snajper Kinglorda. Hektor wchodzi w Bestię. Zjada snajpera i kiereszuje ciężko Dariusza Sowińskiego z frakcji antymausowej.
* Marianna Sowińska wykorzystuje wpływy i uwalnia Hektora, ostrzega Silurię.
* Siluria wynegocjowała wydobycie Hektora z Kompleksu Centralnego i przekazanie go Edwinowi, do Rezydencji. Hektor jest w złym stanie.
* Mordred nadal chroni Piotra, w ukryciu i tajemnicy przed Piotrem. 
* Rafael dostarczył Mordredowi Henryka w nowym ciele. Henryk "słyszy" głos Renaty Souris, która daje mu info o techbunkrze.
* Henryk wysłał niewinnego starszego pana by ten sprawdził mu techbunker. Starszy pan zginął od zabezpieczeń.

**Podsumowanie torów**



**Interpretacja torów:**



# Progresja

* Henryk Siwiecki: od tej pory jest damą i to rodu Souris, jak Renata; sprzężona jest z Renatą mindlinkiem
* Renata Souris: od tej pory jest sprzężona mindlinkiem z Henrykiem Siwieckim w kobiecej formie

# Streszczenie

Hektor dotarł do Piotra, ale z uwagi na to, że tylko ON widzi aurę Arazille i Iliusitiusa w martwym magu... został aresztowany jako zabójca. Kinglord nie dał rady zastrzelić Henryka snajperem, ale wprowadził go w Bestię. Na miejsce przyszła Siluria i wynegocjowała bezpieczeństwo Hektora i jego leczenie. Tymczasem Henryk Siwiecki został przebudowany przez Rafaela Diakona w Henriettę - i uwolniony od władzy i potęgi Kinglorda. Siły Kinglorda... lub Arazille szukają Bzizmy i możliwości zdominowania autowara.

# Zasługi

* mag: Piotr Kit, maluje obrazy. Próbuje uniknąć zrobienia czegokolwiek; wyrolował terminusa i udało mu się ominąć problem własnego mieszkania i zwłok w ścianie.
* mag: Mordred Blakenbauer, pilnuje Henriettę i Piotra. Nie przegania Silurii. Konspiruje z Rafaelem. Dostarczył swojej krwi do stworzenia Henrietty.
* mag: Hektor Blakenbauer, wpakował się w intrygę. Zwłoki w ścianie, echo Arazille, z jakiegoś powodu Arazille go ostrzega a Iliusitius chroni. Przeżył próbę zamachu i zeżarł snajpera.
* mag: Siluria Diakon, mobilizuje siły (Whisper) na Kinglorda, wyciągnęła Hektora z opresji; źródło kontaktu Marianny Sowińskiej by ostrzec o losie Hektora.
* mag: Henryk Siwiecki, rozpoczyna nowe życie jako "Henrietta" czy "Hania"; nawiązał kontakt z Renatą i zabił randomowego dziadka, niechcący.
* mag: Rafael Diakon, który pokonał defilerskie moce Kinglorda i przekształcił Henryka Siwieckiego w coś nowego - "Henriettę".

# Lokalizacje



1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Okólno-Trocin
                1. Czapkowik
                    1. Stara Opera, aktualny 'dom' Piotra Kita i miejsce którym zarządza.

# Czas

* Dni: 2

# Strony

* **EIS**: Chce zniszczyć Renatę.
* **Renata**: Chce być wolna i odzyskać Karradraela
* **Kinglord**: chce dostęp do Bzizmy, chce być Karradraelem 2, odzyskać Henryka, ukarać Piotra, zniszczyć Hektora... a ogólnie, zdobyć władzę nad Mausami.
* **Iliusitius**: chroni Hektora, chce prokuratury magów, chce obsadzić ją SWOIM magiem. Hektor się nada. Chce wykorzystać Piotra jako dywersję i conduit.
* **Arazille**: chce zniszczyć Blakenbauerów i ochronić Hektora przed siłami Iliusitiusa. Chce uwolnić Piotra od aury Iliusitiusa i uwolnić Bzizmę.
* **Bzizma**: chce odzyskać kontrolę i możliwości. Non-issue.
* **Rafael Diakon**: chce dowiedzieć się jak najwięcej odnośnie Kinglorda i odnośnie Henryka.
* **Judyta Maus**: non-issue, ale chce być lubiana
* **Renata Souris**: chce wolności i odzyskania Karradraela. A naprawdę, chce wolności i odnalezienia się w nowym świecie, mieć ZNACZENIE.
* **Koalicja AntyMausowa**: chce zniszczenia rodu Maus
* **Abelard Maus**: chce akceptacji.
* **Karina von Blutwurst**: chce wolności i zniszczenia Kinglorda; chwilowo nie ma wyboru.
* **Marianna Sowińska**: chce pomóc Hektorowi zrobić prokuraturę magów.


# Narzędzia MG

## Opis celu misji



## Cel misji



## Po czym poznam sukces



## Wynik z perspektywy celu



    
## Wykorzystane tabelki

Postać (strona gracza):

| .Motywacja. | .Siły i słabości. | .Specjalizacja. | .Umiejętność. | .Szkoła Magii. |.POSTAĆ. |
|             |                   |                 |               |                |         |

Opozycja (strona NPC):

| .VERSUS. | .Aspekty. | .Skala. | .Magia. |.Pomysł gracza. | .OPOZYCJA. |
|          |           |         |         |                |            |

Wynik:

| .Postać. | .Opozycja. | .Rzut.    | .Wynik.  |
|          |            | xx: +x Wx |   S-SS-F |

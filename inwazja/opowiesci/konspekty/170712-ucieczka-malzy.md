---
layout: inwazja-konspekt
title:  "Ucieczka Małży"
campaign: nie-przydzielone
players: kić, żółw, dzióbek
gm: dust
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [170712 - Ucieczka Małży (Kić, Dzióbek, Żółw (temp))](170712-ucieczka-malzy.html)

### Chronologiczna

* [120507 - Preludium: Historia świata](120507-preludium-historia-swiata.html)

## Kontekst ogólny sytuacji

Cywilizacja małżów żyła sobie na dnie jeziora. Żywiła się planktonem i tym co podpłynęło. Przez pokolenia nic się nie działo. Wszyscy połączeni umysłami - rozkwitała kultura, poezja itp. Aż pojawili się ludzie. Na początku był spokój, czasem ktoś się topił i się sadziło na nim grzyby. Ale potem zaczęli podtruwać wodę. Nic z czym potężna cywilizacja małżów nie może sobie poradzić. Ciekawie się zrobiło od pojawienia się sygnałów radiowych. To rozumiemy. Nauczyliśmy się świata.

...tak jak biedny bot Microsoftu wrzucony w internet. Tia.

Ludzie mają dziwny sposób komunikowania się ze sobą, nie działają altruistycznie, są okrutni, egoistyczni i pełni przemocy. Krzywdzą się nawzajem. Trolle internetowe pomogły w tym zrozumieniu. Zaczęła nas zalewać fala tych informacji z całego świata. I dlatego trzeba się rozrosnąć.

## Punkt zerowy:

I wtedy przyszli oni. Ludzie pojawili się w świecie małży. Dziwni ludzie. Nie tacy 'normalni'. Tych nie dało się kontrolować ani odgonić. Wpuścili do wody coś dziwnego, coś, co ogłupiło małże i zaburzyło możliwość komunikowania się ze sobą. Poodcinało zmysły. I wyłowili nas na powierzchnię.

Nie wiecie gdzie jesteście, ile czasu minęło. Godziny? Dni? Miesiące? I poczuliście przerażającą pustkę. Wyczujecie obecność tylko SIEBIE. Nie ma innych małży? Cała cywilizacja przestała istnieć? Jakby ktoś wymordował cały ród i tylko Wy żyjecie. I nie jesteście na dnie jeziora.

Bazujecie na falach radiowych i sygnałach z mózgów. I wody, wilgoci. Raczej nie wzroku i słuchu.

Jest jeszcze 1 pokrewny sygnał. Przyprowadzono do tego miejsca inną istotę. Pokrewną im, ale jej umysł nie wysyła sygnałów. Nie ma świadomości. Ci, którzy przyprowadzili zaczęli eksperymentować. Ta istota pod czaszką ma mózg - coś podobnego do małży. I potem ludzie zespolili małża z człowiekiem. Jesteśmy czymś nowym. Nie małżem. Małżłowiekiem.

Gdy płyn w biovacie opadł otworzyły się drzwi i weszła postać. Ludzka. Widzicie ją po raz pierwszy i słyszycie. Jesteście ludźmi i małżami, z pełną mocą.

**Pytania i odpowiedzi**

* D: Gdzie dokładnie w mieście znajduje się Wasz azyl?
* K: W palmiarni. Ciepło, wilgotno. Co prawda w akwariach są żółwie, ale to nie problem. Jest na co polować.
* D: Jak się nazywa sprzyjający azylowi mag pomagający Wam?
* B: Stefan Myszeczka. 
* D: Dlaczego ten mag również się ukrywa?
* Ż: Bo swego czasu poderwał bardzo niewłaściwą damę.
* D: Czemu tak w ogóle mu ufacie?
* K: Połączył się z nami mentalnie i wiemy gdzie ukrył zwłoki
* D: Którego rodu Wasza frakcja (viciniusy w azylu) boi się najbardziej?
* B: Rodu Diakon. Na pewno znajdą niewłaściwe zastosowanie dla małży...
* D: Czemu azyl aktywnie poszukuje nowych mieszkańców?
* Ż: Im więcej jest istot podobnym nam tym silniejsi jesteśmy, a inne możemy zrobić podobne nam
* D: Nazwa mitycznego stwora? Tylko nazwa.
* Ż: Chimera.
* D: Imię i nazwisko maga. Podrzędny i mało kompetentny. Żyje sobie na tym terenie, pilnuje porządku i o Was nie wie.
* K: Janusz Czaruś, Świeca

## Misja właściwa:

**Dzień 1:**

Wpada człowiek, wygląda jak z uniwersytetu. Siwiutki. Mówi "szybko zanim wrócą, uciekamy". Małżludzie poszli za nim. Profesorek powiedział, że trzeba pomóc innym - i poszli. Inne cele, inne laboratoria. Rolnik wysłał sygnał do nieprzytomnego strażnika sprawdzając jego stan przytomności. 3v3->S. Tak, strażnik jest nieprzytomny z uwagi na najpewniej zaklęcie czy atak mentalny. To implikuje, że ten profesorek jest magiem..?

Łowca sprawdził, czy inne istoty są wrogie. Te zamknięte w celach. Te inne istoty często cierpią i chcą wyjść. Starszy/Strażnik/Sentinel zadecydował, że obserwacja trwa. Profesorek wziął przedmiot i rzucił do pomieszczenia; jedna z istot wzięła ów przedmiot nie wiedząc co z tym robić. Małże nie wiedzą o co chodzi; Łowca sięgnął swoim umysłem chcąc zrozumieć z czym ma do czynienia. Łowca wyczuł umysł; sięgnął doń i zaproponował podejście do ściany w odpowiedni sposób. Małże połączyły umysły. Łowca i Strażnik - eksperci od wabienia, sugestii i dominacji. 4v5->S.

Istota poszła do rogu, patrzy się jak dziecko w kącie. Usiadła po turecku, położyła przedmiot i kiwając się po turecku patrzy na niego. Wygląda jak człowiek ale to nie człowiek. Nie jest istotą myślącą a narzędziem. Ale jest tam jeszcze jedna, nie-małż, cierpiąca, w środku. Małże stwierdziły, że należy uspokoić istotę w środku. Spróbują powiedzieć, że nie jest sama i próbują jej pomóc. Uczucie sytości i dobrobytu. I info, że wyglądają jak oprawcy, ale nimi nie są. 

5v4->S. Udało się wyindukować przyjazność i przyjemny głos mentalny kobiety. Sygnał unisono. Zagubiona. Rolnik otworzył i wypuścił istotę ze środka; czarodziej próbował go zatrzymać, ale Rolnik powiedział, że nie jest niebezpieczna. Kobieta w środku jest łuskowaną hybrydą, zakrwawiona. Coś zjadła. Gdy Rolnik ją wypuścił, wyskoczyła przewracając i potłukując Rolnika. To jakaś hybryda ryby i człowieka; z kolcem jadowym płaszczki. Zaklęcie mentalne maga ochroniło go przed Kobietą (nazwa kodowa: Syrena). Ta zeżarła tępego minionka (golema).

Łowca uspokoił Stefana Myszeczkę. Stefan powiedział, że trzeba uciekać - inni magowie już wiedzą. Rolnik chce chronić innych, ale Strażnik zauważył, że to są inni - nie Małże. I jak trzeba uciekać to trzeba uciekać. Rolnik się zgodził. Gdy uciekali, zapaliły się czerwone światła, alarmy. Polecieli dalej - wielkie potężne drzwi, ale z gorzej osadzoną framugą. Strażnik wpuścił wodę we framugę i ją zamroził, reszta zaczęła taranować drzwi. Porażka, Efekt Skażenia. Ta sama intencja, inny czar.

Drzwi zaczęły się rozpuszczać. Zrobiły szczelinę do ucieczki. A w oddaleniu pojawili się ludzie, inni z ochrony. A za nimi drzwi zamarzają - odcinają się od strażników, ochrony. Czyli udało się uciec przynajmniej z rdzennego obszaru. A małże w ludzkich ciałach zaczęły spieprzać.

To miejsce wygląda jak ufortyfikowany bunkier nie wyglądający na bunkier. Udało im się zwiać przez wyjście awaryjne. Są na placu, kilkadziesiąt metrów dalej druty, siatka, wieżyczki z reflektorami... a w siatce Stefan pokazał dziurę. Więc Strażnik przygotował czar Mgły. Bardzo silna mgła. A małże NIE machają rączkami i nie mówią - one mają inne inkantacje, nie ludzkie a małżowskie. Jest ciemno, noc - łatwiej o mgłę a Strażnik ma czas. Nie są w mieście. Strażnik postawił Mgłę i użył błyskawicę w kierunku na reflektory. Gdy małżludzie już byli z boku.

Błyskawica miała nie tylko rozwalić reflektor. Miała pomóc Łowcy w akcji dezorientacji mentalnej strażników na wieżyczkach, by ci na wieżyczkach postrzelali w tych na dole. I będą mniej chętni do działań. Mgła, błyskawica, potem inni ochroniarze co wydostali się z zamrożonych drzwi. Reflektory kierują się w stronę błyskawicy (nie małżludzi). Spięcie po błyskawicy wywaliło prąd. Małżludzie wieją, ludzie strzelają do ludzi... super. Odgłosy strzelania do ludzi są inne niż w filmach. Ludzie dostają wpiernicz od ludzi. Małżludzie uciekają.

Gdzieś z boku wystrzelił stożek ognia równolegle do ściany, rozrzucając mgłę. Gdzieś tam jest wrogi mag. Ten mag szuka małżludzi, ale nie może ich znaleźć. Stożek ognia jest mało użyteczny, nie może znaleźć. Małże zdecydowały się NIE atakować tylko wiać. Gdy Zespół (3 małże, jedna syrena, 1 mag) uciekli już odpowiednio daleko, jest ciemno. I zaczął padać deszcz. 

Stefan Myszeczka z torby wyjął mysz. Mysz jest dość specyficzna, bo znajdzie samochód Myszeczki - Rolnik się połączył. Znaleźli dużego poloneza. Zostawili trochę śladów, więc wrogowie ich namierzą, ale nie teraz. W aucie ktoś jest z tyłu na siedzeniu i wyczuwają świadomość. Ta świadomość jest przygaszona. To uciekinier..? Tak. Kobieta. Wchodzą do auta. Jest ciasno.

Jechali bardzo długo. Widać, że ośrodek jest gdzieś na uboczu. Myszeczka zawiózł ich do palmiarni. Gdy małże zapytały Myszeczkę o stary dom... dowiedziały się, że Diakoni dowiedzieli się o ich cywilizacji. Część wyciągnęli do eksperymentów, część wymordowali.

* "Małże potrafią długo chować urazę" - Kić, 2017

Myszeczka zaprowadził ich do windy; starsza pani się uśmiechnęła do Myszeczki. Syrena chciała ją zjeść, ale Rolnik ją zatrzymał; w głowie syreny jest sygnał od małży "trzeba coś z nią zrobić". Starsza pani dotknęła Syreny i Łowcy; pojawiła się sympatia. Duża sympatia. W reakcji, Rolnik walnął potężnym sygnałem strachu, bólu i furii Syreny prosto w staruszkę. Oboje padli. I staruszka, i Rolnik (F,S). Łowca ma koszmarnie trudny dysonans poznawczy: 5v6->S. Łowca się otrząsnął. Zwymiotował jakąś plugawą galaretkę i doszedł do zmysłów. Strażnik spytał ostro, kim jest ten vicinius. To pani Antosia. Ona TAK MA. Enzym przy dotyku.

Strażnik ostro zauważył, że to był atak. Myszeczka jest zdecydowanie pod wpływem. Cały ten cholerny azyl jest pod wpływem. To nie jest bezpieczne miejsce. Zostawili Syrenę na dole, po czym jak Myszeczka wraz z panią Tosią poszli sobie zająć się Tosią, Małże pojechały windą do góry i odjechały. Tam spotkały ostatnią pasażerkę poloneza o której Myszeczka zapomniał. Po synchronizacji mentalnej okazało się, że ten byt jest szalony. Ona coś bełkocze, że Weinerowie jej pomogą, ale małże nie rozumieją.

Małże schowały się w kanałach. Teenage Mutant Ninja Małżas. System defensywny w oparciu o inteligentne szczury...

# Progresja



# Streszczenie



# Zasługi

* vic: Antonio Wulgaris, 'małż' Rolnik. 
* vic: Izolda Gofer, 'małż' Łowca. 
* vic: Gerwazy Protazy, 'małż' Strażnik. 
* mag: Stefan Myszeczka, z misją. 
* mag: Klaudia Weiner, szalona 
* vic: Antonina Antosia, przyjaciółka wszystkich ale nie małżów. 

# Lokalizacje

1. Świat
    1. Primus
        1. XXX
            1. Powiat XXX
                1. XXX
                    1. Palmiarnia

# Czas

* Dni: 1

# Wykorzystane tabelki

Postać (strona gracza):

| .Motywacja. | .Zachowanie. | .Specjalizacja. | .Umiejętność. | .Szkoła Magii. | .Cecha. | .POSTAĆ. |
|             |              |                 |               |                |         |          |

Opozycja (strona NPC):

| .VERSUS. | .Aspekty. | .Skala. | .Magia. |.Pomysł gracza. | .OPOZYCJA. |
|          |           |         |         |                |            |

Wynik:

| .Postać. | .Opozycja. | .Rzut.    | .Wynik. |
|          |            | xx: +x Wx |   SFR   |

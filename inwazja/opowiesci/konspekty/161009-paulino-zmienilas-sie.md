---
layout: inwazja-konspekt
title:  "'Paulino, zmieniłaś się...'"
campaign: prawdziwa-natura-draceny
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [161002 - Wyciek syberionu (PT)](161002-wyciek-syberionu.html)

### Chronologiczna

* [161002 - Wyciek syberionu (PT)](161002-wyciek-syberionu.html)

## Kontekst ogólny sytuacji
## Punkt zerowy:
## Misja właściwa:

Dzień 1:

Piękny poranek. Dla odmiany nic się złego nie działo. Jedynie jedzie smażoną rybą...
Michał jest zdrowszy i weselszy - bo Dracena. Michał jest więcej - rozpromieniony. A jego Skażenie maleje za wolno...

[Maria: to jej sprawka. Przecieka jej magia, lub coś...]
[Paulina: nie możesz zakładać, że wszystko co się dzieje jest intencjonalnie złe...]
[Maria: Tak jak było z Nikolą?]
[Paulina: Znaczy że co...?]

Maria i Paulina weszły w sprzeczkę. Maria zarzuciła Paulinie, że ta się zmieniła.

[Maria: kiedyś nie wysłałabyś psychotycznej nimfomanki przeciw ludziom i magom...]
[Paulina: kiedyś nie robiłam różnych rzeczy... nie mówiąc o tym że NIE MIAŁAM psychotycznej nimfomanki - chcę doprowadzić ją do normalności]
[Maria: kosztem innych. Kiedyś tak nie robiłaś]
[Paulina: koszt był zawsze - kiedyś kosztem siebie. Ten koszt jest zawsze, nawet jeśli tego nie widzisz]

Maria powiedziała Paulinie, że zauważyła, że Paulinie na Dracenie zależy. I nie podoba jej się to. Nie podoba się Marii, co Dracena z Pauliną robi. Próbuje Paulinę przekonać, by ta trzymała Dracenę na silniejszej obroży. Niech Dracena nie pasożytuje na magach i ludziach. Niech Paulina będzie taka jak dawniej...
Silne starcie dwóch soulbindowanych przyjaciółek.
Paulina poszła w "przecież wiesz, że mi na tobie zależy i nie chcę byś była odrzucona i porzucona; musiałaś się chować a byłam sama i wiem że to nie w porządku..." - obiecała Marii, że jakiś czas mogą pobyć po prostu same, same ze sobą. Bez nikogo innego. Paulina udobruchała Marię; a żeby ją zamknąć, że Paulina używa metod magicznych i zachowuje się jak mag - zrzuciła na Marię jedną decyzję:

"Mamy ludzi którzy byli magami. Nie wiedzą, nie pamiętają. Nie wiedzą, że mogli, a nie mogą. Są szczęśliwi i spokojni. A jak sobie przypomną i jakiś mag się dowie, jest duża szansa, że umrą... co byś zrobiła?" - Paulina, stawiając Marii poważny problem etyczny

Maria obiecała, że o tym pomyśli. A przed Pauliną stanęło dziwne pytanie - czemu Michałowi wolniej przechodzi niż ona zakładała, skoro Michał NIE MIAŁ DO CZYNIENIA z żadnym Źródłem Skażenia. Wie, bo... wie. Pilnowała. Czyżby faktycznie Dracena?

Paulina spytała Dracenę o tą sprawę. Ta powiedziała, że nie powinna przeciekać, ale zgodziła się, że to nieco podejrzana sprawa. Obiecała, że przejdzie się i poszuka nowych źródeł i ogólnie rozumianych anomalii magicznych. A Paulina wróciła do dalszego badania Michała. Chce dowiedzieć się, co się mogło zmienić; dlaczego. I patrzy na jego wzór - o co chodzi?

Zmienił się CHARAKTER tkanki magicznej. Jest bardziej syreniasty. Jest czystsza, mniej zakłócona. Gdyby nie maść, zaczęłyby mu rosnąć piersi. Zupełnie jakby w ostatnim czasie zostały usunięte zakłócenia i została tylko czysta transformacja w syrenę.

Badania innych członków populacji i ich leczenie prowadzą do podobnie interesującego wniosku - sygnał jest czystszy. Pryzmat jest czystszy. Siła mocy spadła, jakość sygnału drastycznie wzrosła. Wciąż stare techniki leczące są wystarczające, ale... to jest dziwne.

Popołudniem wróciła Dracena, min. wracając od Haliny. Dracena wyjaśniła Paulinie co się stało - zniszczyły Morową Pannę. Morowa Panna stanowiła jedno ze źródeł Pryzmatu. Usunęły jedno silne źródło Pryzmatu i się poczyściło...
Dracena powiedziała, że jest też jedna sprawa. Halina sobie DOŚĆ przypomniała. Pamięta trochę inne życie. Halina błagała, by mogła zostać magiem... lub viciniusem. Nie chce żyć normalnie, jako człowiek, bez żadnych perspektyw. Obecność magii sprawiła, że Halina sobie przypomniała więcej... ale i ta Syrena i Dusza Ognia...

Maria szybko rzuciła informacje o Syrenie: to jest siła ochronna. Gdzie będzie Syrena? W jeziorze. Paulina zaznaczyła, że w jeziorze NIE MA magii. Maria powiedziała, że to niemożliwe. Magia Pauliny zawodziła, a Draceny już w ogóle...

Zakładając, że faktycznie Weinerowie stworzyli te legendy:
- Morowa Panna sprawia, że ludzie nie chcą wyjeżdżać (nie ma łamania Maskarady)
- Zarażaniu, problemom itp przeciwdziała Syrena
- Czarny Kaptur pilnuje Maskarady
- Ognista Dusza jest nowa, nie z Weinerów a z nadmiernie oddanej terminuski (co nadal jest zagadką)
- Nie wiadomo dalej, gdzie jest vault Weinerów. Może magowie kasujący pamięć go nie zwinęli...? Nie było o tym żadnych zapisów.

I teraz te legendy stały się rzeczywistością >.>

"Pomożemy tej Weinerce?" - Dracena
"Wiesz, że chcę jej pomóc... co będzie zdrowe i bezpieczne dla całej tej przeklętej okolicy" - Paulina
"Chcesz się bawić w boga, Paulino? Nie lepiej, by... by została człowiekiem?" - Maria
"Lepiej dla kogo? Czy ty w jej sytuacji wolałabyś?" - Paulina
"Nigdy nie byłam magiem... nie jestem w stanie powiedzieć. Jako... upośledzony mag; vicinius, pamiętający... to chyba gorsze niż szczęśliwy człowiek" - Maria
"Żal mi jej. Chciałabym... nie być człowiekiem na jej miejscu. Mieć jakieś moce. Lepsze to niż żadne." - Dracena

Zabawne:
- Maria BYŁA czarodziejką. Nie wie o tym. Uważa, że lepiej ludziom.
- Dracena JEST czarodziejką. Jest też viciniuską. Uważa, że lepiej magom.
- Paulina ma przechlapane. 

Paulina powiedziała Dracenie o samotności do końca życia, jeśli ta stanie się viciniusem. Dracena się zamyśliła... samotność jest czymś, co do niej przemawia jako ogromny ból. I powiedziała Marii, że ta jest dziennikarką - może zmienić świat artykułem. "think of it as magic". I tego nie ma i nie będzie mieć. Jest rybą śniącą o lataniu...

Tak naprawdę, magowie odbierają moc tym, którzy utracili pamięć po części z litości.

Oki. Paulina lubi mieć zasoby. Poszła więc... rozmontować niekorzystne źródło energii magiczne - w przychodni. A że jest późno i przychodnia jest nieczynna...

Paulina idzie z Draceną - dwie katalistki. Idą więc spokojnie, niedaleko przychodni Paulinę zatrzymał szpakowaty facet. Przedstawił się jako Kurt Weiner. Zarzucił Paulinie, że to co podaje ludziom nie ma prawa działać. To zwykłe maście. Ale Paulina nie chce na tym zarabiać... więc to coś więcej. PAULINA DĄŻY DO SŁAWY I CHWAŁY (i wyciągania kasy od populacji).
Paulina zauważyła, że jednak pomaga.
Kurt się zorientował, że Paulina ma rację. Wcześniej tego nie zauważył - potęga źródła magicznego zamazała mu sytuację i doprowadziła do dysonansu poznawczego. Ale teraz... teraz widzi, że Paulina faktycznie pomaga ludziom. Nie zdawał sobie sprawy. I czuje wewnętrznie, że to nie jest kwestia szczęścia (dawne wspomnienia maga).

Oddala się, by przeprowadzić badanie ponownie i ZROZUMIEĆ... Paulina rzuciła nań zaklęcie mentalne. Niech pomieszają mu się zmysły lekko. Niech zapomni tą myśl; niech wróci do błogiej nieświadomości i szukania profitów dla Pauliny. Jako, że Paulina nie słynie z magii mentalnej, Dracena zrobiła podstęp. Zawołała lekarza i pokazała mu biust. Gość ma szczękę przy ziemi... a Paulina dokończyła zaklęcie. Niestety, miało to pewien koszt:
- Lekarz się oburzył na zachowanie Draceny. Uznał, że Dracena i Paulina sobie to lekceważą.
- Maria też jest zasmucona; zachowanie Draceny... Paulina działająca "jak każdy mag"...
- Lekarz podzwoni i popyta; spróbuje dowiedzieć się kim jest Paulina i Dracena (o Dracenie dostanie niewiele)
- Lekarz całkowicie stracił tą myśl z głowy

"Tak... znam to uczucie..." - Maria, ze smutkiem, widząc jak Paulina i Dracena mieszają poczciwemu lekarzowi w głowie magią

Paulina skutecznie przekonała Marię, że musiała - gdyby wezwała terminusa, oni wszyscy mogli zostać pozabijani. Bo nie wiadomo, czemu odzyskują pamięć. Relacje Maria - Paulina się znowu poprawiły, aczkolwiek Maria - Dracena są najniższe w historii...

Widząc sytuację, Paulina zdecydowała się NIE asymilować tych źródeł chwilowo. Quarki cenna rzecz, ale ważniejsze, by lekarz - i w sumie nikt - sobie nie przypomniał. A bez Morowej Panny to jest zbyt prawdopodobne...
Dracena zaproponowała Paulinie przepięcie źródeł. Zaproponowała wzmocnienie sygnału niepamiętania - by ludzie przypominali sobie wolniej. Paulina zaproponowała usprawnienie tego pomysłu - zmienić w master/slave; niech to jedno źródło będzie najsilniejsze, dominujące i rozsyła do pozostałych sygnał zapominania by objąć odpowiednio szeroki obszar.

Ale wpierw trzeba je oczyścić z zarażania.

1) Oczyścić je z zarażania
2) Zbudować sieć repliki
3) Scalić i ustawić, by sygnały działały poprawnie

Niestety, muszą wpierw dostać się do przychodni. Dracena poprosiła Paulinę, by ta obserwowała sytuację a sama użyła magii. Przełączyła kłódki i weszły niepostrzeżenie na teren przychodni. W mniejszym miasteczku przychodnia nie ma monitoringu ani stróża; to nie ten typ miejsca. Weszły do środka. Maria przypomniała, by nie zostawiały żadnych śladów i dała kilka rad jak to się robi. Niestety, Paulina nie potrafi się włamywać... ślady pozostały jako "ktoś tu był".

Dotarły bez żadnego problemu do źródła magicznego. Dracena prowadzi Paulinę; ona widzi lepiej w ciemności. Źródło jest widzialne dla maga; manifestuje się w leżance Kurta Weinera. I tu pierwsze zaskoczenie - Paulina zauważyła, że to jest Stare Zaklęcie które uległo wypaczeniu. Sprzed Zaćmienia. Kurt Weiner kiedyś rzucił czar mający chronić Maskaradę. I ten czar potem zmutował, wygasł a syberion przywrócił go do istnienia.
Po ponownym badaniu, ten czar okazał się być bardziej interesujący. COŚ się wydarzyło podczas Zaćmienia. Coś bardzo, bardzo złego. To było w tej spalonej przychodni; i ta leżanka przez Pryzmat złapała tego echo. Stąd "zarażanie". Ten przedmiot ma astralną historię - znaczy więcej, niż jest. Dziś to już niegroźne, ale miało znaczenie kiedyś... grunt, że "zarażanie" było elementem tej magii. To nie jest przypadek - przed pożarem przychodni COŚ ZARAŻAJĄCEGO stworzonego przez (chyba) Kurta Weinera weszło w interakcję z mazaniem pamięci stworzonym przez (chyba) Kurta Weinera. I pożar to zatrzymał. I ten pożar na 100% był magiczny. Wypalał MAGIĘ. Ta leżanka przetrwała, ale zniszczyło to jej moc. Teraz tylko przywrócone zostało echo przez syberion.
Ważne: to nie jest groźne. Syberion - nadmiar syberionu - mógłby coś z tym zrobić, ale to wymaga dużo więcej syberionu.

To co ważne - Paulina chce to wyczyścić do oryginalnej wersji zaklęcia; to jest super wiadomość. Nawet z Zaćmieniem i syberionem to okaże się być prostsze.
Wpierw - oczyścić z zarażenia i zbudować czysty 'master source'. Udało jej się - ma silne, czyste źródło które osłabia pamięć i prowadzi do nie zauważania złamań Maskarady. I nie wspiera koncepcji Czarnego Kaptura w żaden sposób!!!
Oczywiście, dwie godzinki później, Paulinie się udało - była zmęczona jak diabli, Dracena zresztą też, ale im wyszło.

Czas zająć się budowaniem sieci repliki. To źródło połączy się z pozostałymi i przekształci je w sobie podobne.
Paulina zdecydowała się zrobić to w konkretnie ciekawy sposób - korzysta z wiedzy katalitycznej i lekarskiej by stworzyć sieć komórkową. Jedno rozproszone źródło. Każdy węzeł jak komórka, mają współdziałać ze sobą i odrobinę przekierowywać energię między sobą.

I sukces! Weinerowie nie będą sobie szybko przypominać a to, co zrobiła Paulina silnie osłabia wpływ syberionu.

Zmęczone, Dracena i Paulina poszły spać... znaczy, padły na pysk.

Dzień 2:

Paulina obudziła się, wyspana i dość szczęśliwa. To jest piękny dzień. Nic nie powinno złego się stać... tylko ten zapach ryby...
Piękny dzień Pauliny zepsuło badanie Michała. Regeneruje za wolno. Paulina nie czuje Pryzmatu... nie powinno być żadnego. Nie teraz. A jednak wpływ na Michała udowadnia, że Pryzmat wciąż jest... gdzieś. Zaraza się cofa, ale powoli. Za wolno. Działania Pauliny pomogły, zmiana źródeł pomogła... ale powinny pomóc bardziej. Gorzej, że Paulina zlokalizowała punkt przegięcia poniżej którego po prostu przestanie pomagać (do momentu wyparowania syberionu).

Paulina tego nie rozumie. I bardzo nie lubi nie rozumieć.

"Wiesz, z tego co mówisz, gdybyś tu nie przyjechała to i tak nie stałaby się tragedia" - Dracena
"Nie. Parę ludzi skończyłoby z paskudnymi bliznami, paru Weinerów mogłoby coś sobie przypomnieć, ale... ogólnie nie." - Paulina, z lekko poprawionym humorem

Paulina poprosiła Marię, by ta się przeszła do Kurta Weinera i sprawdziła czy on faktycznie nie pamięta; co robi, co pamięta...
Maria bez większego entuzjazmu się zgodziła. Paulina widzi, że działa na nią ta aura syberionu tak jak na innych; zdecydowała się na wzmocnienie sygnałów do Marii by ta nie czuła się sama i wykorzystywana.

A sama - z Draceną - poszła porozmawiać z Haliną. 
Moment. Skąd się wziął ten klon Haliny? Jak powstał? Podobno "się pojawił", ale JAK? Z tego co mówił Michał "się pojawił"...
Magia Krwi? Dracena zaprzeczyła, by Halina była defilerką. Paulina powiedziała - jeszcze nie. Ale może otworzyła drogę..?

Poszły do lasu, gdzie znajduje się Halina. Dracena wprowadziła Paulinę...
Halina przywitała się z Pauliną.

"Ty ciągle władasz mocą... jesteś tym czym ja byłam..." - Halina do Pauliny

W Halinie jest gorycz, ale nie ma nienawiści. Halina pamięta, że istnieje magia i że ona ją miała... ale nie pamięta skąd się wzięła. Halina jest nieprzekonywalna; ona chce "czerwoną pigułkę", chce mieć moc, nie chce być zwykłą dziewczyną. Nieco pokrzyżowała jej się pamięć własna i pamięć Ksenii Zajcew; chce chronić. Nie rozumie, że transformacja w viciniusa zniszczyłaby osobę, którą jest Halina...

Halina przyznała się, nieświadomie, że swojego klona zrobiła z krwi, używając do pomocy Syreny. Wypłynęła na jezioro a Syrena odpowiedziała. Dlaczego odpowiedziała? Bo wyczuła maga z krwi Weinerów... a pod wodą jest i vault i agregat pamięci Weinerów. Są odpowiednio zamaskowane.

"Pod wodą? Tak jak mówiłam?" - Maria
"Źródeł dalej tam nie ma..." - Paulina
"Tak jak mówiłam?" - Maria

Paulinie udało się przekonać Halinę, że jednak życie viciniusa niekoniecznie jest najlepsze. I że jako vicinius straci autonomię i człowieczeństwo. I w ogóle będzie w gorszej formie niż Dracena.

Cel Pauliny był świetny. Jedynie Dracena zirytowana, że została (przez Halinę) potraktowana jako "mięsień" i nie-autonomiczny agent Pauliny...

To co zostaje do dookreślenia: czy wezwać wsparcie z gildii planktonowych, Świecy czy też Millennium. Bo Paulina i Dracena nie czują się na siłach wchodzić i łamać vaulta Weinerów z potencjalną infekcją syberionu we dwójkę...

# Progresja



# Streszczenie

Zniszczenie Morowej Panny wzmocniło efemerydę Syreny, co sprawia, że leczenie Pauliny działa słabiej. Pojawiają się tarcia w zespole - Paulina musi radzić sobie z Marią i Draceną - a te dwie nie są ze sobą kompatybilne. Paulina i Dracena przekształciły źródła w takie, które utrzymują Maskaradę mocniej. Lekarz Kurt Weiner szuka, co Paulina chce osiągnąć szukając wszystkiego co najgorsze. Halina chce być viciniusem (Paulina wybiła jej to z głowy) i powiedziała, że Vault i Agregat Pamięci są w jeziorze...

# Zasługi

* mag: Paulina Tarczyńska, cieszy się, bo dla odmiany jej działania nie ratują życia wszystkim dookoła... acz chce zostawić wszystkich w jak najlepszym stanie.
* mag: Dracena Diakon, zirytowana brakiem autonomii i próbująca uratować wszystkich... mając przy okazji zabawę. Nie wie, jak bardzo bywa irytująca.
* czł: Maria Newa, dzielnie zbierająca legendy i rekonstruująca fakty. Czuje się porzucona i uważa, że Paulina się zmieniła na niekorzyść. Nie lubi Draceny.
* czł: Michał Jesiotr, pocieszający się brakiem Haliny przy boku Draceny ;-). Też: za wolno się leczy; stanowi Paulinowy papierek lakmusowy.
* czł: Halina Weiner, która marzy by odzyskać moc, ale nie rozumie konsekwencji. Paulina wybiła jej to z głowy; Halina powiedziała gdzie jest Syrena i co jest w jeziorze...
* czł: Kurt Weiner, lekarz szukający o co chodzi Paulinie; też: jak pognębić Paulinę i Dracenę. O dziwo, nie ze złej woli a ze zmartwienia o lokalną populację.

# Lokalizacje

1. Świat
    1. Primus
        1. Lubelskie 
            1. Powiat Hrabiański
                1. Stawnia, która ma bardzo dziwną historię - pożar spowodowany przez terminuskę która siebie poświęciła by ratować miasto podczas Zaćmienia?
                    1. Centrum
                        1. Przychodnia, gdzie Paulina zneutralizowała jedno źródło syberionowej magii, zarażające i w ogóle.
                    1. Oziersko
                        1. Smażalnia ryb Jesiotr
                            1. Przybudówka, nadal tu śpi Paulina, Dracena i okazjonalnie Michał z Draceną ;-)
                        1. Jezioro Oziero, gdzie znajduje się vault Weinerów i tajemnicza syrena
                    1. Leśnicz
                        1. Las ozierski, gdzie schowana jest kandydatka na viciniuskę - Halina

# Skrypt

Pain points:
- Dracena jest niestabilna; ma tendencje do utraty kontroli nad sobą zwłaszcza w obecności Pryzmatu
- Dracena i Maria
- pod jeziorem znajduje się 'memcache' Weinerów
- syrena uwodzicielka, niktor
- po zniszczeniu Morowej Panny, Syrena ma nadmiar wpływu mocy
- Weinerowie pracowali nad systemami by ludzie nie pamiętali o magii - biomantycznie
- Ognista Dama, która uniemożliwiła uratowanie rodziny Mensiaków

# Czas

* Dni: 2
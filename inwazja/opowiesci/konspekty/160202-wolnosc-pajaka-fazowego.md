---
layout: inwazja-konspekt
title:  "Wolność pająka fazowego"
campaign: powrot-karradraela
gm: żółw
players: kić, dzióbek
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [150607 - Brat przeciw bratu (HB, SD, PS)](150607-brat-przeciw-bratu.html)

### Chronologiczna

* [150607 - Brat przeciw bratu (HB, SD, PS)](150607-brat-przeciw-bratu.html)

## Logiczna

* [150422 - Śladami aptoforma... (DK, AB)](150422-sladami-aptoforma.html)

## Wątki

- Za chwałę Blakenbauerów


## Punkt zerowy:

Ż: Klub. Jakiego typu? Jaki fajny, interesujący, kopaliński klub (czego)?
K: Klub miłośników historii miasta.
Ż: Czemu Nikodem Sowiński chce tam brylować; jaki jest jego wysokopoziomowy cel?
B: Chce zainteresować innego maga swoją pro-ludzkością.
Ż: Czemu pasuje to ogólnie rodowi Blakenbauerów; co z tego mają?
K: Jest szansa na ściągnięcie tam kiedyś Hektora. Pro-ludzistyczny Hektor i pro-ludzistyczny Nikodem.
Ż: Czym był super eksperymentalny projekt robiony przez Nikodema powiązanego z tym tematem?
B: Było to planowane jako największy rytuał Archera w Polsce, który miał na celu odkrycie szlachetnej spuścizny Kopalina.
Ż: Co szczególnego występuje właśnie na TAMTYM terenie?
K: Ponieważ ktoś kiedyś zdecydowanie nadużywał tam Archerów.
Ż: Co charakteryzuje zadziorną artefaktorkę Bankierzy w relacji do Nikodema?
B: Nikodem chce jej imponować.
Ż: Jaki system oparty o Archera stworzyła Laurena?
B: System naprowadzania i namierzania dla najbardziej szowinistycznych świń w okolicy.

## Kontekst misji

KADEM i Świeca jeszcze są w stanie zimnej wojny, KADEM nie jest wyłączony.
Nikodem Sowiński próbował zaimponować Laurenie, ale zawalił na całej linii. Odrzucił projekt jako niemożliwy.
Laurena przekształciła projekt Nikodema. Zaczął pełnić "jakąś" funkcję... Laurena nie doceniła.

- Laurena Bankierz ustabilizowała to, co Nikodem Sowiński chciał zniszczyć (uznał za bezsensowne)
..... Engineer.defy / subvert an order (inefficient / wrong from their point of view)

- Pajęczak fazowy zarufusił bardzo cenny klejnot Sowińskiego i umieścił go w okolicy echa Arazille.
..... Magpie.find a lost / hidden <location> and start 'collecting' from it

- Nikodem Sowiński się wściekł, że przypisuje się mu coś takiego.
..... Explorer.demand proper recognition for his <deeds>.

## Misja właściwa:

### Faza 1: Niefortunny zbieg okoliczności

Z Leonidasem i Klarą skontaktował się Borys; powiedział, że pająk fazowy - istota kradnąca artefakty i przenosząca je przez Fazę Daemonica do punktu docelowego - robi ruchy na własną rękę. Borys naciska "stop" i nic się nie dzieje. Pająk zachowuje się na własną rękę. Tylko Edwin coś wie na ten temat; ale on nie odbiera wiadomości od Borysa. Borys ma dokładną sygnaturę i obraz tego jak wygląda ten klejnot, ale nic nie może zrobić.

W Rezydencji Blakenbauerów Borys szybko poprosił ich do siebie. Wpisał hasło i hard porno przekształciło się w sygnaturę skomplikowanego kryształu magicznego; Leo od razu to wykrył - zobaczył unikalny kryształ Sowińskich (leader diary); bardzo wrażliwy na magię. Zdaniem Borysa kryształ jest między Fazami. I tam został.
Szybko ściągnęli Edwina. Ten ma 5 minut - pomaga śmiertelnie choremu człowiekowi (nie magowi) na coś bardzo dziwnego. Gdy dowiedział się o co chodzi, facepalmował. Przyciśnięty Borys przyznał się, że komunikował się z pająkiem - wysyłał mu obsceniczne teksty i w ogóle - ale on nie ma uprawnień, to nie powinno nic zrobić ani zmienić.

Niestety, Nikodem Sowiński jest osobą do której prędzej czy później Leo chce dokooptować Hektora. Jest to mag, który koniecznie Chce Się Wykazać - zarufuszenie mu kryształu zdecydowanie podpada pod "Nikodem teraz cię nienawidzi", bo to straszna obelga. Edwin podał na wszelki wypadek Klarze i Leonidasowi kod autodestrukcji i poszedł. Trzeba zatrzymać pająka... a przynajmniej uniemożliwić Blakenbauerom ucierpienie na reputacji. Nie stać ich na nowych wrogów.

Normalnie Sowiński jest w stanie zawsze zlokalizować kryształ; ale teraz jest on pomiędzy fazami. Więc nie może. Czuje link, nie zna lokalizacji. Jest to dla niego bardzo nieprzyjemne i frustrujące. Wraz z korupcją kryształu link będzie zanikał. Też, w lapisowym pudełku link zniknie. 
...ogólnie, wykorzystanie tego pająka do tego kryształu... smutek. 

Klara poprosiła Borysa, by ten dał jej zapis wszystkich kontaktów z pająkiem. Wszystkich 2 miesięcy. Ktoś musi to przejrzeć... Klara chce spróbkować działanie i reakcje pająka na bluzgi Borysa. Te na początku i te na końcu. Te przed akcją i te po akcji. Też: impuls magiczny który mógł zmienić działanie pająka. Klara dokładniej przejrzała i wyszło coś takiego: sygnał do pająka fazowego od Borysa był całkowicie ignorowany. Ale w chwili uwolnienia Crystal Shard przez Arazille, sygnały do pająka fazowego zaczęły działać zupełnie inaczej; pojawiło się coś innego. Niestety, zajęło to trochę czasu...

W tym samym czasie Leonidas wypuścił sieci brokera informacji. Dowiedział się, że Nikodem robił jakiś mega fajny rytuał Archera; coś się spieprzyło. Poprosił Laurenę o to, by ta go zniszczyła. Laurena go dokończyła i powstał mega perwersyjny czar Archera; tak więc ten kryształ Nikodema to forma Laureny by się zemścić. Sowiński wziął to BARDZO do siebie.
Tyle, że Blakenbauerom pasuje, by Laurena i Nikodem pracowali razem docelowo - Hektor może zyskać DWA kontakty. A inżynier artefaktor i znający Kopalin Sowiński to świetne kontakty które można Hektorowi wprowadzić do akcji.

Chwilowo śledztwo prowadzi Sowiński sam. Docelowo jak będzie chciał prowadzić szerszymi środkami, Leonidas ma zamiar się tam wkręcić jako oficjalny detektyw / szukajowiec. By wygenerować presję, Leonidas ma zamiar sprzedać Ignatowi Zajcewowi informację jak upokorzyć Nikodema - poprosić go o prezentację o ciekawych korzyściach i sytuacjach ze znajomości Kopalina. Jako, że on to dokumentował w krysztale pamięci... obejdzie się smakiem.

Jedyne co wiadomo to to, że pająk jest na Fazie i się przesuwa. A kryształ jest pomiędzy fazami. Badania jeszcze potwierdziły, że Borys sam w sobie nic nie zrobił; kluczem był moment działania Arazille na Crystal. Trzeba podziękować Dionizemu i Alinie.

Pająk pozostawia po sobie bardzo charakterystyczną sygnaturę. Pomysł Klary - przygotować rój robocików, by odwrócić uwagę i zachować tą samą sygnaturę. Zmieniła zdanie - trzeba przejąć kontrolę nad pająkiem, jak tylko ten przejdzie na fazę naturalną. Z pomocą Leonidasa i Borysa studiowała system - by tylko móc skontaktować się i zhackować pająka jak on zmieni fazę.

### Faza 2: Hackowanie pająka.

Pająk szukał, szukał i wyszukał - znalazł sobie artefakt na fazie naturalnej. Ten artefakt to coś dziwnego; wygląda jak żywy artefakt który trochę pulsuje. Wygląda ogólnie bardzo paskudnie; pająk zstąpił na Fazę Naturalną by go ukraść i wtedy zadziałał zespół Klara Leonidas (i Borys cheerleader).

Połączenie z pająkiem wymaga i technomancji i katalizy (Klara).
Przejęcie kontroli nad pająkiem wymaga dominacji (Leonidas). 

Zdecydowali się zadziałać. Pająk jest na fazie naturalnej.
Klara: 19 (z magią). Pająk 17 (z shieldem 15). Klara (18) -> shield 14. Zaeskalował; Klara zaeskalowała; pająk podniósł artefakt i aktywnie walczy z defensywami. To jest ciekawe; wszystko wskazuje na to, że pająk jest rozłączony z systemem command and control. Po eskalacji okazało się, że niestety pająk został wykryty przez systemy zabezpieczeń; w świat poszła informacja o tym, że ktoś kontroluje phase spidera. Potem niestety Klara musiała zrobić jeszcze jedną eskalację; serwery c&c się rozpadły.

Pająk przeszedł na Drugą Stronę. Ma artefakt w Fazie Daemonica.
Edwin dostał prikaz total containment. A Leonidas zdecydował się na przekonanie pająka, by ten wrócił do Rezydencji gdzie jego dom. Udało mu się - ale niestety, pająk dał radę wpierw oddać ten dziwny artefakt tam, gdzie chciał go oddać; tam, gdzie znajduje się kryształ. Drugim kosztem jest to, że WSTECZNIE okazało się, że WSZYSTKIE istoty pochodzące od Tymotheusa potencjalnie uzyskały wolność i sygnał od Arazille tak jak wcześniej pająk fazowy. Ale bez efektów stałych.

Pająk grzecznie pełznie do Rezydencji, gdzie problem zostanie rozwiązany.

Tymczasem Leonidas dostał sygnał od brokerów informacji - jest wysokie zainteresowanie Sowińskiego ściągnięciem sił wsparcia znalezienia kryształu ORAZ sił Laureny ściągnięciem mięśni i wsparcia do znalezienia winnego problemu. Sowiński ma pomysł jak znaleźć, Laurena zbudowała artefakt mający łapać sygnaturę. Na szczęście, pająk umieści pomiędzy fazami. Na nieszczęście, kto szuka ten prędzej czy później znajdzie; silny katalista jest w stanie coś z tym zrobić dalej.

W związku z tym, Klara ma zamiar wysłać rój małych robocików po Kopalinie; kilkukrotny ping kontrolowany przez Klarę w samej Rezydencji by móc wysłać i zamieszać zarówno Laurenie jak i Nikodemowi i całkowicie ukryć prawdziwą lokalizację artefaktów. Nieznaną też Klarze i Leonidasowi. I się udało wielokrotnie wysłać sygnały - jednocześnie udało się wyrównać relacje między Laureną a Nikodemem. Innymi słowy, sytuacja wróciła do normalności.

## Stan finalny:

- pająk został odzyskany przez Blakenbauerów, choć przez pewien czas jest nieoperacyjny
- wiemy, że wszystkie istoty Tymotheusa mogą być rozstrojone przez Arazille
- wiemy, że są dwa artefakty "pomiędzy" i nie wiemy gdzie
- Laurena i Nikodem wrócili do normalnej relacji; bardziej wkurzeni, ale oboje oberwali
- Borys nie został płaszczką

## Dark Future:

### Faza 1: Niefortunny zbieg okoliczności

- Borys kontaktuje się z Leonidasem i Klarą; pokazuje im problem.
..... 

- Nikodem kontaktuje się z Laureną i żąda oddania swojego klejnotu. Ona oddaje mu jego urządzenie.
..... Explorer.demand returning <object of collection> which is righteously his

- Laurena przygotowuje artefakt mający pomóc znaleźć zaginiony klejnot; niestety, jego użycie zniszczyłoby klejnot.
..... Engineer.fieldtest <something>, make a dangerous experiment

- Nikodem wyzywa Laurenę od ostatnich idiotek i twierdzi, że chce ona zniszczyć jego własność.
..... Explorer.attack <someone> with harsh words over something which would endanger <something>


### Faza 2: Lokalizacje, lokalizacje.

- Laurena buduje sieć skanującą pomiędzy echem energii a "to się pojawi". Uważa, że ktoś chce ją wrobić.
..... Engineer.pour more resources into an action

- Nikodem odkrywa powiązanie energetyczne po liniach pływów magii prowadzące do kilku miejsc - a zwłaszcza Pirogów.
..... Explorer.find out about the nature of the <expedition> from historic reasons

- Pajęczak fazowy ściągnął nieznany artefakt nieznanego pochodzenia i umieścił go w okolicy echa Arazille
..... Magpie.get distracted / bribed by a different <something> which shines more


### Faza 3: Spotkanie w Pirogu

- Laurena, Nikodem i siły Blakenbauerów napotykają się niedaleko działania pajęczaka fazowego. W Pirogu.
..... Magpie.be pursued by multiple parties wanting the same; lead them to contact with each other

- Sprzężenie artefaktów zaczyna rezonować i ściągać energię w swoim kierunku.
..... EmoNode.divert magical currents towards itself

- Dariusz Larent organizuje ogromny festyn by zamaskować ślady i w ogóle odepchnąć magów
..... Celebrity's Society.organize a meeting / party

- Dochodzi do konfliktu pomiędzy magami i rodami. Powoli ściągane są siły: Bankierzy i wsparcie Sowińskiego. Tu już chodzi o honor.
..... 

# Streszczenie

Big one: Arazille uwolniła wszystkie istoty kontrolowane przez Tymotheusa gdy przejęła Crystal Shard. A Blakenbauerom zniknął pająk fazowy (Borys myśli że to jego wina). I ów pająk doprowadził do konfliktu Nikodem Sowiński - Laurana Bankierz. Który nie pasuje Blakenbauerom. Szczęśliwie Leonidas zlokalizował a Klara shackowała pająka fazowego; odzyskali go dla Rodu. Zabawne, że przez pająka są dwa artefakty "pomiędzy" i nikt nie wie gdzie i co robią.

# Zasługi

* mag: Leonidas Blakenbauer, który zdominował zbuntowanego pająka fazowego i rozwiązał konflikt między Sowińskim i Bankierzówną.
* mag: Klara Blakenbauer, która nawiązała technokatalityczne połączenie ze zbuntowanym pająkiem fazowym i odkryła, co Arazille zrobiła z istotami Tymotheusa.
* czł: Borys Kumin, który zauważył nietypowe zachowanie pająka fazowego. Też: wysyłał mu sprośności i gadał do niego z nudów.
* mag: Laurena Bankierz, która skłóciła się z Nikodemem Sowińskim i została wrobiona (przypadkowo) w głównego przeciwnika z jego punktu widzenia.
* mag: Nikodem Sowiński, któremu pająk fazowy ukradł cenny kryształ pamięci. Obwiniał Laurenę, wyszedł na idiotę przed Ignatem.
* mag: Edwin Blakenbauer, który powiedział parę słów odnośnie pająka fazowego, po czym uniknął odpowiedzialności jak na Edwina przystało.
* mag: Ignat Zajcew, który wydał trochę kasy by pognębić tienowatego Sowińskiego; udowodnił, że ów jest mocny tylko w gębie.
* vic: Arazille, która impulsowo potencjalnie uwolniła wszystkie istoty Tymotheusa Blakenbauera.

# Lokalizacje:

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Dzielnica Owadów
                        1. Klub miłośników historii Kopalina i okolic
                    1. Obrzeża
                        1. Rezydencja Blakenbauerów
                1. Piróg Dolny
                    1. Bełty Śląskie
                        1. Dworek Larentów
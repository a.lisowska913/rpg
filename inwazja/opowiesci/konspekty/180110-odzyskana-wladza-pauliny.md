---
layout: inwazja-konspekt
title:  "Odzyskana władza Pauliny"
campaign: wizja-dukata
gm: żółw
players: kić, raynor
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [171229 - Esuriit w Półdarze](171229-esuriit-w-poldarze.html)

### Chronologiczna

* [171229 - Esuriit w Półdarze](171229-esuriit-w-poldarze.html)

## Kontekst ogólny sytuacji

### Opis sytuacji

* Łucja Maus jest ciężko ranna; chce jednak dojść do siebie jak najszybciej i pomóc Bolesławowi
* Abelard Maus chce odzyskać Łucję jako TERORYSTKEM
* Daniel chce postawić narzędzie mające przesłać sygnał z magitrowni
* Paulina chce wyleczyć cholerne klątwożyty by je przesłać.

### Wątki konsumowane:

brak

## Punkt zerowy:

## Potencjalne pytania historyczne:

* Czy Łucja trafi w ręce Abelarda? (TAK, długość: 3)
* Czy klątwożyty wzmocnią wylęgarnię? (TAK, długość: 3)
* Czy pojawi się straszliwa świnia dużej mocy? (TAK, długość: 5)

## Misja właściwa:

**Dzień 1**:

Paulina nie miała szczęścia. Łucja (zamknięta w pokoju miłości Draceny) osunęła się na ziemię i zaczęła krwawić. Uszkodzenie. Krótka diagnostyka Pauliny wskazała, że Łucja ma zniszczony Wzór. Nie wygląda na czysto Mausowy. Łucja jest półświadoma. Tyle, że Paulina ją wyleczyła - Łucja nie powinna mieć rozwalonego Wzoru... Paulina prosi o pomoc Dracenę.

* Łucja: Zwykły(3), Poważne Obrażenia, Zniszczony Wzór = 7
* Paulina: baza (6), Lekarz z Powołania, Naprawa Wzoru  = 10

Sukces. Paulina ustabilizowała i wyleczyła Łucję. "Dobrze mieć lekarza" - zauważyła Łucja, z lekką rezygnacją. Przyciśnięta przez Paulinę, Łucja powiedziała co następuje:

* ona działała przeciw Myszeczce, bo Myszeczka rozwalił portalisko
* ona chce pomóc Bolkowi Mausowi
* ona ma dowody, że Myszeczka podłożył świnię na portalisku i próbował zrzucić winę na Bolka
* ona sama robi swoje leki; niestety, nie zdążyła ich zażyć

Paulina broniąca domu i znająca się na wywiadzie lekarskim vs osłabiona Łucja - nieufna z tendencjami antywładzowymi. Sukces. Łucja powiedziała:

* podczas wojny z Karradraelem zniszczyła sobie wzór. Nie dorwał jej. Nie chciała krzywdzić niewinnych.
* potrzebuje czarnotrufli; pobiera je z Eliksiru Aerinus zwykle jak jest w okolicy
* Bolek jest jej przyjacielem. Chciała pomóc.

Paulina jest skłonna ją wypuścić, ale... Łucja tymi rękami zniszczyła siedzibę główną Świecy. Teraz rezydent Świecy (Sylwester) musi mieszkać w piwnicy magitrowni. Łucja dorzuciła nowych problemów do tych które były. Paulina nie pozwoliła Łucji odejść - ale powiedziała jej, że Mausowie żądali pomocy Rezydenta jej. I ją zostawiła. Jako strażnika zostawiła dyskretnego agenta Lady Eweliny Bankierz. Ma informować. I dostanie czarnotrufle...

Tymczasem Daniel próbuje optymalizować energię wysyłaną z magitrowni, by ją ustabilizować. Musi przygotować się do tego, by to mogło zadziałać do przebudowania struktury. Wpierw Daniel wybiera biznesy mniej ważne, które można... uciemiężyć. Daniel próbuje wybierać klientów, którzy mogą poradzić sobie bez energii lub z mniejszą ilością. Danielowi udało się znaleźć 3 takie firmy, które przetrwają kryzys i które pomogą ustabilizować magitrownię:

* Eliksir Aerinus
* Farma Myszeczki
* Awianarium Oliwii

To pozwoliło Danielowi ustabilizować magitrownię. Magitrownia jest STABILNA. Niestety, glukszwajny i inne świnie się zainteresowały - Świeca wygasła. Świnie skupiają się na portalisku... a przynajmniej część z nich, jak raportuje Dracena, zaalarmowana.

Trzeba Coś Zrobić. Glukszwajny jak wejdą w portalisko, stanie się coś... dziwnego.

Dracena zapytana o pomysł powiedziała, że może zrobić przepięcie na kablach. To kupi czas. Zdaniem Daniela - zły pomysł. Trzeba eksperta. TRZEBA TOMASZA MYSZECZKI! Szybki telefon... Myszeczka musi się skupić, wszystkie siły by okiełznać te świnie! Myszeczka poprosił Daniela, by ten kupił mu kilka godzin. Powinno dać się to jakoś opanować.

Daniel, nieszczęsny, robi przepięcia na infrastrukturze. Niech świnie mają co jeść. Daniel przesyła energię z portaliska prosto na kanały magiczne. Wzmocnił to magią - niech nie rozwali to całej magitrowni a energia ma być identyczna. Katalityczna iluzja, że portalisko jest TAM gdzie przepięcie.

* Świnie: Zwykły (3), Rozkaz Wylęgarni (4), Masywna Skala (4)
* Daniel: (6), Właściwa Energia Z Portaliska (4), Iluzja Portaliska.

Danielowi udało się opanować świnie. Kupił czas Myszeczce.

Tymczasem Paulina poszła do Melodii. Melodia powiedziała co następuje:

* na tym terenie jest INNY kraloth. Paulina jest przerażona.
* Laragnarhag jest odporny na klątwożyty.
* Melodia może jej pomóc z morale, przekonywaniem lub polityką.

Melodia zaproponowała, że może spróbować pomóc ze świniami w eterze, ale potrzebuje próbkę świń. Laragnarhag może zaprojektować straszliwy patogen i zniszczyć cokolwiek jest potrzebne. Więc: projekt patogenu antyświńskiego brzmi jak najlepszy możliwy pomysł. Choć nie, efemeryda... to niech Melodia znajdzie dla Pauliny kralotha.

Na tym terenie są też klątwożyty. One się modyfikują. Paulina potrzebuje próbki zarażonych magów. Więc siły Sądecznego poszły złapać Paulinie takiego maga (kilku), najlepiej niezbyt zbitych. Paulina potrzebuje próbki nowych odmian klątwożyta.

Z Danielem skontaktował się dyplomata - Joachim Maus. Ten przekierował sprawę Łucji do Pauliny. Paulina spławiła go - nie ma czasu. Joachim jednak poprosił Paulinę, by ona przekazała Łucji, że ta jest aresztowana i jak będzie wolna ma się stawić przed sądem Karradraela. Paulina nie chcąc antagonizować Mausów się zgodziła. Paulina westchnęła - czyli jednak Tomasz Myszeczka złożył skargę. Zapytany przez Paulinę czy chcą ukarać Łucję czy ją osądzić, Joachim powiedział, że chcą osądzić. Paulina nie wierzy. Joachim jeszcze raz przeprosił Paulinę, że doszło do takich zniszczeń na tym terenie - znowu przez Mausów.

Łucja powiedziała, że nie ma już dowodów na działania Myszeczki. Najpewniej Abelard Maus je zniszczył. Z woli Karradraela, Mausowie mają być tylko podnóżkiem. Poprosiła Paulinę o wypuszczenie - Paulina odmówiła. Łucja zadzwoniła do Daniela przedstawiając swoje racje - jest tu jej przyjaciel Bolesław. On zarabia... a teraz traci. Ona chce pomóc, jest na zasadzie parolu więc utrzymuje słowo honoru. Daniel powiedział jej, że ona ma pomóc mu z emiterem i ogólnie może pomóc z magitrownią - na zasadzie parolu. Łucja się zgodziła (konflikt). Zgodziła się - ale jeśli ma dojść do jej wydania, Daniel jest zobowiązany by ją ostrzec.

Paulina została przez Daniela ostrzeżona, że Łucja mu pomaga.

**Dzień 2**:

Czas zabrać się za konstrukcję emitera do klątwożytów:

* Emiter: Bardzo Trudny (11), Daleki Zasięg, Precyzyjny Sygnał, TRWAŁOŚĆ 2.
* Daniel: baza (8), Skala: 3 magów, specjalistyczne narzędzie do sygnału z magitrowni, zaklęcie rezonujące z rurami do niwelowania zasięgu

I... nie ma Paradoksu. Emiter gotowy, magitrownia - ustabilizowana - może wysyłać sygnały rozwiązujące problemy klątwożytów. Teraz tylko czas na same klątwożyty.

Paulina dostała jakiegoś nieszczęsnego maga dzięki thugom Sądecznego. Paulina ma możliwość ekstrakcji próbki klątwożyta. Podczas tej ekstrakcji doszło do Efektu Skażenia: Transport + Tworzenie + Soczewka. Innymi słowy, wzmocnienie intencji. Zaklęcie Pauliny wzmocniło działanie emitera i tworzyło kopię emitera w lepszym miejscu - w lesie. Emiter jest niestabilny i tymczasowy... ale lepiej rozmontuje te klątwożyty.

Czas na Paulinową emisję destruktorów klątwożytowych. Tzn. na budowanie tych rzeczy.

* Klątwożyty: Bardzo Trudny (11), Mutują, Trudne do wyplenienia (4), Rozległa Skala (4)
* Paulina: all (8), dawna szczepionka, próbka aktualnego, magimed sądecznego, mutacja środowiska w nieprzyjazne, czyści teren, sprzęt magitechowy

20v19 -> sukces. Paulinie udało się zbudować antyklątwożytówkę.

I teraz czas, by Daniel z Warinskym wyemitowali sygnał. Czas pozbyć się klątwożytów na dobre.

* At least one remains: Trudne (7), Ogromna Skala (6), Emisja niedestruktywna (4), Efemeryda
* Daniel: baza (6), Rezonans Emiterów (4), Stabilna magitrownia, Warinsky, Integra, pro bono, kontrola magitrowni, ogólny sprzęt

22v19 -> Skonfliktowany Sukces. Udało się pozbyć tych wszystkich cholernych klątwożytów - acz magitrownia została solidnie uszkodzona przez rezonans emiterów. Poszło kilka rzeczy. Trzeba naprawiać. Szczęśliwie Myszeczka opanował świnie; problem stada uwolnionych glukszwajnów nie jest problemem Daniela ani Pauliny.

Sytuacja wróciła do... normy?

Podsumowując:

* Pozbyli się stada wałęsających się świń uwolnionych przez Łucję
* Pozbyli się klątwożytów dezinhibicyjnych na tym terenie poza miejscami ich wylęgania
* Wiedzą, że nie ma inwazji z zewnątrz; to były tylko akcje przeciwko Myszeczce

Aha, Wiaczesław zaraportował Paulinie - była mała banda magów, chciała skorzystać z okazji na tym terenie. Zostali zdewastowani przez Wiaczesława. Nie złapał ich skutecznie (1v4), ale zdewastował. Zmusił ich do ucieczki.

## Wpływ na świat

JAK:

* 2: MUSIK: gracz mówi w jaki sposób jedna z obcych postaci wspiera coś związanego z motywacją JEGO postaci
* 2: CLAIM na wątku / postaci / okoliczności
* 2: do elementu Historii lub przeszłego konfliktu dodajemy "ale" lub "oraz"
* 2: dodajemy pytanie, które musi zostać odpowiedziane na jakiejś z przyszłych misji
* 2: odpowiadamy na pytanie, które pojawiło się na jakiejś z misji
* 3: gracz mówi w jaki sposób jedna z obcych postaci wspiera coś związanego z motywacją JEGO postaci
* 3: zmieniamy kontekst okoliczności czegoś z Historii - scena z przeszłości / fakt?
* 3: do elementu spoza Historii dodajemy "ale" lub "oraz"
* 3: dodajemy znaczącego NPC powiązanego z elementem Historii
* 3: pozyskanie przez dowolną postać znaczącego surowca mającego sens z perspektywy misji
* 3: dodanie nowego elementu Historii
* 3: dodanie przyszłego lub przeszłego faktu; czegoś, co się wydarzyło lub wydarzy

Z sesji:

1. Żółw (12)
    1. Pojawia się straszliwa świnia dużej mocy (5)
    1. Klątwożyty zdążyły wzmocnić wylęgarnię zanim zniknęły (3)
    1. Łucja i Myszeczka się nienawidzą. A z Danielem się lubią. Niestabilny trójkąt.
1. Kić (3)
    1. Paulina: Sława i chwała. Osiągnęła duży sukces przy stabilizacji regionu.
1. Raynor (5)
    1. Daniel Akwitański: wybawca od klątwożytów i świń!
    1. Daniel: Łucja jest skłonna współpracować z Danielem. Widzi, że nie jest jej wrogi.

# Streszczenie

Daniel dał radę ustabilizować energię magitrowni ograniczając dostęp mniej krytycznych firm od stałych dostaw energii. Tomasz Myszeczka opanował kryzys dzikich glukszwajnów dążących do wdeptania Portaliska racicami. Paulina dowiedziała się, że na terenie jest inny kraloth i uratowała Łucję Maus. Łucja została przekonana przez Daniela do współpracy i wszyscy współnymi siłami wykonali plan Warinsky'ego - usunęli zagrożenie klątwożytowe z okolicy. A Hektor Reszniaczek został tymczasowym CEO Eliksiru Aerinus. Niestety, wylęgania rośnie w mocy...

# Progresja

* Paulina Tarczyńska: sława i chwała; duży sukces przy stabilizacji terenu: magitrownia działa, świnie pod kontrolą, nie ma klątwożytów.
* Tomasz Myszeczka: odzyskał kontrolę nad swoimi świniami i ogólnie odbudował swoje zasoby.
* Łucja Maus: uzyskała pulę czarnotrufli z Eliksiru Aerinus, na leki dla siebie; Paulina ją jakoś dodatkowo ustabilizowała.
* Marcin Warinsky: jego plan okazał się perfekcyjny. Mistrz Skażenia Szlachty nadal w formie.

## Frakcji

* Senesgradzki Chaos: pojawia się straszliwa świnia dużej mocy w okolicy, po działaniach w magitrowni i wyłączeniu klątwożytów
* Senesgradzki Chaos: Wylęgarnia świń zdecydowanie wzrosła w mocy przez działania klątwożytów
* Senesgradzki Chaos: traci dostęp do mutujących klątwożytów i wolno wędrujących świń w Powiecie Pustulskim

# Zasługi

* mag: Daniel Akwitański, przekonał Łucję Maus do współpracy w ramach magitrowni oraz pomógł Tomaszowi Myszeczce opanować "dzikie" świnie. Ustabilizował wypływ energii z magitrowni.
* mag: Paulina Tarczyńska, która osiągnęła ogromny sukces - przez MOMENT wszystko na terenie Powiatu Pustulskiego jest pod kontrolą. Klątwożyty, świnie, Mausowie...
* mag: Łucja Maus, czarodziejka, która sama rozwaliła sobie wzór i wymaga leków by tylko nie służyć Karradraelowi. Buntowniczka antyKarradraelowa. Pomaga Danielowi jak może z magitrownią.
* mag: Tomasz Myszeczka, opanował glukszwajny i wszelkie inne świnie na tym terenie z pomocą magitrowni Histogram. Sprawa powoli wraca mu pod kontrolę.
* mag: Joachim Maus, dyplomata rodu Maus, który z rozkazu seirasa Abelarda Mausa zażądał wydania Łucji. Zniszczył dowody na działania Myszeczki, by go nie antagonizować.
* mag: Wiaczesław Zajcew, z prośby Pauliny chronił wszelkie siły na terenie; by nikt nie wtargnął i nie pożywił się osłabionym Powiatem Pustulskim.
* mag: Melodia Diakon, ostrzegła Paulinę przed obecnością innego kralotha na tym terenie. Dodatkowo zobowiązała się do znalezienia tamtego kralotha z Laragnarhagiem.

# Plany

* Daniel Akwitański: ma wsparcie ze strony Łucji Maus. Ona widzi w nim potencjalnego sojusznika a przynajmniej nie-wroga.
* Paulina Tarczyńska: chce przekazać Eliksir Aerinus Hektorowi Reszniaczkowi; nie chce by Kaja go rozwaliła
* Tomasz Myszeczka: zwalcza Łucję Maus, bo ona jest niebezpieczną, szaloną psychopatką i rozpędziła mu świnie w momencie kryzysu
* Łucja Maus: będzie współpracować z Danielem Akwitańskim; chce dobrze dla Mausów i tego terenu
* Łucja Maus: zwalcza Tomasza Myszeczkę, bo on zrobił krzywdę jej bliskiemu przyjacielowi

## Frakcji

# Lokalizacje

1. Świat
    1. Primus
        1. Mazowsze
            1. Powiat Pustulski
                1. Męczymordy
                    1. Magitrownia Histogram, centrum dowodzenia i rehabilitacji dla Łucji Maus. Ogólnie, centralny obszar dowódczy.
                1. Byczokrowie
                    1. Niezniszczalna Kazamata, gdzie zadziałał solidnie Tomasz Myszeczka i przechwycił wszystkie świnie zanim te poszły na portalisko.

# Czas

* Opóźnienie: 0
* Dni: 2

# Wątki kampanii

nieistotne

# Przeciwnicy

## XXX
### Opis
### Poziom

* Trwałość: ?
* Poziom: Trudny (7)
* Siły: 
* Słabości: 

# Wątki kampanii

1. Stan aktualny Pauliny Tarczyńskiej
    1. CLAIM: Paulina zostanie rezydentką Świecy na tym terenie a Tamara jej prawą ręką - terminusem bojowym.
    2. CLAIM: Paulina nie będzie musiała uciekać z terenu niezależnie od okoliczności (Kić)
    3. Magimed zostaje u Pauliny jako trwały surowiec. (Kić)
    4. Sądeczny uznał Paulinę za gracza a nie pionek - docenił to czym jest. (Kić)
    5. Paulina dostaje wsparcie od Millennium. (Kić)
1. Stan aktualny Krzysztofa Grumrzyka
    1. Po tym jak Krzysztof pomógł Filipowi, współpraca się zacieśniła - lepsza kalibracja Firmy i Firmy.
1. Stan aktualny Kociebora Dyrygenta
    1. CLAIM: okazja na poznanie i zaprzyjaźnienie się Kociebora z Kajetanem.
    2. Sądeczny daje Kocieborowi własną agencję detektywistyczną za zasługi i ratunek Dobrocienia
    3. Oliwia zapewnia Kocieborowi drony, które pozwolą na obserwowanie i śledzenie. Nowy biznes Oliwii.
    4. Customizowany awian, nie taki jak Czarny Ptak dla Kociebora. Taki czołg.
    5. Maja próbuje w najbardziej nieoczekiwanych momentach (i nieodpowiednich) okazać Kocieborowi sympatię i pomoc.
    6. Prawdziwym bohaterem Mai stał się Kociebor, który potraktował ją łagodnie i z sympatią.
1. Stan aktualny Daniela Akwitańskiego
    1. CLAIM: Myszeczka jest ważnym partnerem biznesowym Daniela (Raynor)
    2. Sądeczny w swoim investigation odkrywa, że Tamara zadziałała wbrew Danielowi i on nie chciał stawać przeciw niemu
    3. Robert uważa, że współpraca z Danielem oznacza, że Daniel jest dużo bardziej cenny niż myślał wcześniej
    4. Konflikt z Sylwestrem Bankierzem - oferował największą wartość, Daniel był w stanie przekonać go że jest wartościowym partnerem. (Raynor)
    5. Działania Daniela i Tomasza nie zostały wykryte przez ani Sądecznego ani Tamarę. Przez nikogo. (Raynor)
1. Stan Mazowsza i okolicy
    1. Filip aktywnie wspiera Paulinę w redukcji pola magicznego i Skażenia na tym terenie.
    2. W jaki sposób można odepchnąć w eterze nadmiar energii, by pozbyć się kłopotu bez skrzydła katalistów, terminusów i logistyków?
    3. Te pieprzone świnie się rozmnożyły. Pasożytnicze świnie z eteru. (Prezes)
    4. Doszło do wielokrotnego złamania Maskarady na tym terenie.
    5. Energia Eteru i Efemerydy Senesgradzkiej mutuje inne efemerydy w eterze. Zwłaszcza te świńskie. Mamy wylęgarnię. (Żółw)
    6. Doszło do Skażenia Primusa efemerydami świńskimi w ogromnym stopniu. Są WSZĘDZIE.
1. Efemeryda Senesgradzka i jej stan
    1. CLAIM: Grzybb (w Efemerydzie zamiast Farnolisa) jest do uratowania (Kić)
    2. CLAIM: nigdy więcej efemeryda nie skrzywdzi moich awianów (Foczek)
    3. CLAIM: będę w stanie wykorzystać tą efemerydę jako niesamowite źródło energii (Foczek)
    4. Efemeryda Senesgradzka występuje na wielu Fazach jednocześnie i ma dopływy z różnych Faz (Żółw)
1. Farma krystalitów Mai Weiner
    1. Dlaczego krystality są niezbędne Oliwii do opanowywania efemerydy?
    2. Byt który opętał elementala/awiana Rolfa jest emocjonalnie związany z dzieckiem.
    3. Farma Krystalitów z młodą Mają która decyduje się przejąć dowodzenie na farmie.
1. Awiany z lokalnej montowni
    1. Awiany nowego typu są lepsze, fajniejsze ale i niebezpieczniejsze
    2. Spora część awianów montowanych w montowni jest dostosowana do linii produkcyjnej Filipa.
    3. Sporo awianów a wszystkie nowej generacji są zbudowane technikami sprzężenia Allitras-elementalna.
    4. Dlaczego "elemental burzy i gniewu" jako najlepsze miejsce znalazł ten klub (Panienka w Koralach)? Co było z tym miejscem lub ludźmi?
    5. Ściągane i pętane elementale są niewolone ALE nigdy nie są agresywne.
    6. Jakiego typu viciniusem stanie się awian Filipa?
    7. Awiany są strategicznie potrzebne zarówno mafii jak i Świecy
1. Echo starych technologii Weinerów
    1. Harvester nie ma dość energii by się obudzić
    2. Natalia pozyskała bezpiecznie kolejną część artefaktu
    3. Kajetan odnajdzie niebezpieczne elementy Harvestera przed Dukatem (Kić)
1. Wieża Wichrów
    1. Wieża Wichrów jest sprzężona portalem z nieznaną (chwilowo) fazą Allitras. Stałe powiązanie, póki są tam burze.
    2. Robert Sądeczny zażądał od Filipa zaprzestania tych konkretnych praktyk zagrażających terenowi.
    3. Bez tych praktyk Wieża Wichrów w obecnej formie nie ma sensu; lepiej zrobić nieco inne miejsce.
    4. Wieża Wichrów zostaje w rękach Filipa. Sądeczny nie ma jak się do niej przyczepić.
    5. Filipowi uda się utrzymać firmę JAKOŚ.
    6. Portal w Wieży Wichrów sam z siebie się nie rozproszy - jest tam i będzie spamował dziwne byty.
1. Byty z z Allitras
    1. Przepakowane elementale. Więcej zabawy, więcej energii. (Foczek)
    2. Byty wchodzące w elementale są świadome. (odpowiedź graczy)
    3. Aktualny elemental nie zdołał otworzyć portalu, ALE wysłał sygnał w eter.
    4. Lewiatan się zbliża. Jest już bardzo blisko.
1. Klątwożyty dezinhibicyjne
    1. Klątwożyty dezinhibicyjne trwale dostosowały się do okolicy. Bardzo trudno będzie je wyplenić. (Żółw)
    2. Farnolis zaraził obszar dawnego Pentacyklu klątwożytem dezinihibicyjnym. (Żółw)
    3. Klątwożyty dezinhibicyjne trwale Skaziły Biały Zameczek. (Żółw)
    4. Jeden typ klątwożyta. Mutujący, ale jeden. W ten sposób da się z tym poradzić - i to zrobił ich wspólny Paradoks (Kić)
    5. Dalia została wyleczona z klątwożytów i już ich nie złapie (Kić)
1. Odbudowa Farnolisa
    1. Farnolis zaczął przejmować władzę na obszarze dawnego Pentacyklu; odbudowując pod areną obszar wpływu. (Żółw)
1. Tamara x Sądeczny?
    1. Sądeczny, który podrywa Tamarę Muszkiet. To po prostu ekstra. (Kić)
1. Oczy Świecy, Oczy Sądecznego
    1. Sądeczny odzyskuje część "oczu" przez wprowadzenie linii produkcyjnej dron-awianów do fabryki Oliwii i Tamara wie o tym.
    2. Sądeczny wprowadził rezydenta u Myszeczki
    3. Tamara doprowadziła do sytuacji, gdzie Świeca nie jest na ciągłym podsłuchu
    4. Tamara nie jest w stanie rozwiązać wcześniejszej dezinformacji
    5. Oktawia ma wiedzę z Legionu Tamary z czasów misji przed opętaniem magitrowni.
    6. Sądeczny nasyła Zofię na szpiegowanie Tamary
1. Reputacja Świecy, Reputacja Sądecznego
    1. Sądeczny spinował akcję dezinformacyjną, masakrując reputację Świecy.
    2. Tamara skupia uwagę na wspieraniu przede wszystkim Tomasza Myszeczki. Duży, rozległy, potrzebuje pomocy. Brak mu wszystkiego.
    3. Sądeczny znajduje się w niełasce po tym, co tu się ogólnie ciągle dzieje - ma to naprawić.
    4. Zarówno Sądeczny jak Muszkiet są wyłączeni z akcji przez około tygodnia.
    5. Balans sił i energii po prostu się rozpadł na tym terenie - nikt nic nie kontroluje.
    6. Sytuacja z portaliskiem wzmacnia status quo na tym terenie - nikt nie jest w stanie uzyskać przewagi. (Raynor)
1. Robert Sądeczny, zapamiętany przez historię
    1. Jak wbić klin między Fornalisa a Sądecznego? (Kić)
    2. Aneta wróciła z Sądecznym z Muzeum Pary ORAZ wie, co tam się stało
    3. Hektor pomagał w budowaniu eliksiru który skrzywdził Katię
    4. Czy Aneta Rukolas się na serio zakocha w Sądecznym (Skażenie Wzoru)?: NIE WIEMY. (Żółw). 5/10.
    5. Wzór Anety jest trwale uszkodzony - wymaga czegoś nowego (Żółw)
    6. Sądeczny przekazał dowodzenie defensywne Wiaczesławowi (Żółw)
1. Lord Rezydent Sylwester Bankierz
    1. Sylwester został zesłany tu przez działania Newerji
    2. Sylwester przeprowadził udaną akcję zdobycia transportu Dukata
1. Tomasz Myszeczka - król viciniusów
    1. Sądeczny dostarczył maga, który zajmie się glukszwajnem czyszczącym czujniki ze Skażenia
    2. Tamara robi się bardzo podejrzliwa wobec Myszeczki
    3. Myszeczka staje po stronie Rezydenta Świecy
    4. Tomasz Myszeczka pomaga w tematach leczniczych. Poniesie koszty, ale zrekompensuje sobie wiele innych rzeczy. (Kić)
1. Dracena Diakon, w jakimś społeczeństwie
    1. CLAIM: Szybki, dyskretny awian. Trafi do Draceny. (Żółw)
    2. Kto i w jaki sposób pomoże Dracenie stać się częścią tego terenu dookoła? Należeć do czegoś?
    3. Dracena wykonuje działania mające uwolnić "Nową Melodię" od mafii 
    4. Dracena uważa, że to jest moment na uderzenie w Sądecznego i zniszczenie władzy na tym terenie (Żółw)
1. Magitrownia Histogram, echo technologii Weinerów
    1. CLAIM: Dracena docelowo będzie w stanie zintegrować się z JAKIMŚ społeczeństwem (Kić)
    2. Magitrownia z Draceną działa dużo lepiej i taniej
    3. Dracena została przekonana, że magitrownia współpracuje z mafią a nie jest wykorzystywana
    4. Zarówno siły Dukata jak i Myszeczki zapewniają, by Skażenie nie dotarło do magitrowni.
    5. Tomasz Myszeczka - jego viciniusy stanowią źródło wczesnego ostrzegania na liniach magitrowni. (Raynor)
    6. Nowa polityka miłości w firmie generalnie rozwiązuje problem z napięciami i nie powoduje publicznych rozterek. (Raynor)
    7. Magitrownia uzyskuje wsparcie zasobowe Eweliny Bankierz. (Raynor)
1. Gabriel Dukat - uleczenie jego syna
    1. CLAIM: Dukat lubi Paulinę (Kić)
    2. W jaki sposób byty z Allitras mogą pomóc dziecku Dukata?
    3. Aktywny Harvester będzie w stanie pomóc dziecku Dukata - stałym kosztem ofiar z ludzi (Żółw)
    4. Dukat nie ma gwałtownej reakcji na działania Sylwestra; da się wychować. Tupnął nogą bo musiał - and now we talk" (Kić)
    5. Kluczowe elementy soullinkowania dziecka Dukata pochodzą z wiedzy pozyskanej przez Sądecznego w ramach działania na tym terenie
1. Portalisko Pustulskie i odrodzenie Mausów
    1. Pojawia się zwiększenie ruchów antymausowych na tym terenie - mniej portaliska dla Mausów.
    2. Bolesław Maus rozpoczyna kampanię "keep magic low here".
    3. Portalisko jest silnie połączone z Allitras.
    4. Bolesław Maus kontratakuje przeciwko Tomaszowi Myszeczce - nie może tak być, że ciągle świnie niszczą portalisko.
1. Świńskie viciniusy i coś poszło nie tak
    1. Rolnik od świń założył ma misję - i przydupasa - mające udowodnić, że ze świniami dzieje się coś dziwnego
    2. W okolicy Męczymordy jest odpowiednia ilość pola rezydualnego by sprawić by znikające świnie były prawdą
1. Oktawia, efemeryczne echo Oliwii
    1. CLAIM: Oktawia dorośnie i zniknie. (Kić)
    2. CLAIM: Dracena i Oktawia będą miały duet. (Żółw)
    3. Oktawia chce pomóc Krzysztofowi, bo widzi w nim potencjał do pomocy Oliwii
    4. Marzyciel może nadal komunikować się z Oktawią; może jej śpiewać, gdy ona jest w Efemerydzie. (Żółw)
    5. Chaotyczność i nieufność / niestabilność Oktawii jedynie wzrosła. Ciągle odrzucana, discardowana, torturowana. (Żółw)
1. Wolność Eweliny Bankierz
    1. CLAIM "Newerja Bankierz x Gabriel Dukat - zostaną parą" (Kić)
    2. Docelowo Ewelina dostanie jakieś środki do życia; nie będzie żebraczką
    3. Kaja uważa, że już dość; czas zacząć pomagać i ratować sytuację (Kić)
1. Prokuratura Magów
    1. Całą tą sprawą, tą destabilizacją zainteresowała się nowo powstała prokuratura magów. (Raynor)

# Narzędzia MG

## Opis celu Opowieści

N/A

## Cel Opowieści

* Sprawdzenie mechaniki 1801 jako całości
* Sprawdzenie kalibracji liczb 1801
* Zrobienie czegoś co się Dzióbkowi spodoba ;-)

## Po czym poznam sukces

* Dzióbek się będzie dobrze bawił
* Liczby dadzą dobry rezultat

## Wynik z perspektywy celu

* Dzióbek się dobrze bawił: sukces
* Liczby dały ciekawy rezultat; było za prosto. 
    * Wszystkie konflikty wygrane przez postacie. 
    * Mitygacja: podnieść bazy konfliktów o 1-1-1-1 dla próby (wariant mechaniki 1806).

## Wykorzystana mechanika

Mechanika 1801, wariant z daty 1803

Wpływ:

* 10 kart balans między graczami; 8 kart to zwykle dzień
* każda porażka i skonfliktowany sukces = +2 pkt wpływu dla gracza
* każda karta = +1 wpływ dla MG
* każde 5 kart zaokr. w górę: +1 pkt wpływu dla gracza


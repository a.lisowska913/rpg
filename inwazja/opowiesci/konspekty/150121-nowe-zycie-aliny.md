---
layout: inwazja-konspekt
title:  "Nowe życie Aliny"
campaign: blakenbauerowie-x-skorpion
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [150115 - Negocjacje w LegioQuant (HB, Kx, AB)](150115-negocjacje-w-legioquant.html)

### Chronologiczna

* [150115 - Negocjacje w LegioQuant (HB, Kx, AB)](150115-negocjacje-w-legioquant.html)

## Punkt zerowy:

K->Ż: Co sprawiło, że Blakenbauerowie uznali, że warto Aliną się zajmować a nie ją zniszczyć po ostatniej sprawie?
Ż: Bo dla Edwina Klemens czy Alina to osoby, nie tylko narzędzia. I póki może, póty chce im pomóc.
Ż: Co agent Millennium robił w Kotach?
K: To nie była oficjalna akcja Millennium; znalazł gdzieś w dokumentach coś wskazujące na obecność viciniusa.
K->Ż: Dlaczego agent Millennium pojawił się tam całkowicie sam?
Ż: Bo nie chce ryzykować eskalacji zanim nie bedzie miał pewności.
Ż: Dlaczego Marysia Kiras ma oko na owego agenta Millennium.
K: Ponieważ została przed nim ostrzeżona.
K->Ż: Komu zniknięcie tego agenta by bardzo pasowało?
Ż: Osobom, które są zwolennikami Millennium potężnego i silnego a nie Millennium opiekuńczego i pro-ludzkiego.
Ż: Dlaczego Borysowi zależy osobiście na tym by ten agent Millennium został członkiem Zety.
K: Nic osobistego; ktoś w Millennium poprosił go o przysługę.
K->Ż: Co pomocnego dla Aliny znajduje się w kościele?
Ż: Źródło prawdy i stabilności.
Ż: Jakie znaczące wydarzenie będzie miało miejsce w Kotach do tygodnia.
K: Festiwal marzeń sennych.
Ż: Z którym wątkiem związana jest ta sprawa: Samira czy Maria?
K: Trzymajmy się Marii.

## Misja właściwa:

Podczas akcji w Bobrowie odnośnie LegioQuant siły Blakenbauerów poniosły sromotną porażkę. Edwin miał pełne ręce roboty pomiędzy odtwarzaniem Klemensa oraz korekcją i stabilizacją Aliny. To zatrzymało jakiekolwiek większe plany Blakenbauerów, dzięki czemu Hektor skupił się na interesujących go tematach świata ludzi. Tymczasem Margaret i Edwin mieli pełne ręce roboty by dało się postawić unieszkodliwionych agentów (oraz resztki Oddziału Zeta).

W świetle poniesionych przez Blakenbauerów strat Otton zażądał, by Edwin i Margaret przygotowali kolejne jednostki nadające się do Oddziału Zeta. Z uwagi na specyfikę Zety to muszą być mężczyźni i może być ich tylko troje więcej (Zeta operuje poprawnie tylko w przedziale 5-8 osób). Tak więc to, co zostało z Oddziału Zeta chwilowo jest krytyczną liczbą. Otton zaproponował Edwinowi przekształcenie Aliny w członka Zety ale Edwin odmówił z uwagi na potencjalną niekompatybilność. Tak więc do zadania "znajdź trzech lub więcej potencjalnych agentów Zety" oddelegowany został Borys.

W czasie w którym Alina otworzyła oczy i przeszła wszystkie testy jakie kazali jej zrobić Edwin i Margaret, Borysowi udało się znaleźć jedną osobę kompatybilną z programem Zeta. Z uwagi na to, że ta osoba jest agentem Millennium do sprawy zdecydowano się wysłać Alinę. Borys i Alina mają za zadanie unieszkodliwić i porwać agenta Millennium w taki sposób, by wina spadła na kogoś innego. Dlatego Alina. Edwin zapewnił, że jak Alina przeszła testy to jest już wystarczająco stabilna i zdolna do wykonania tego zadania, zwłaszcza, jeśli dostanie wsparcie. Margaret zgodziła się zapewnić to wsparcie. Wbrew sugestiom Edwina Alina odmówiła wzięcia jako wsparcia kogokolwiek z sił specjalnych Hektora...

Tymczasem Alina po raz kolejny otworzyła oczy na łóżku w laboratorium Edwina. Zobaczyła Edwina. Ten powiedział jej, że już nie jest źle i powiedział jej, że nie jest pewny jakie moce i jaki jest aktualny stan Aliny. Zrobili kilka testów - Alina zmieniła się w pokojówkę Blakenbauerów, ale nie umiała powrócić do oryginalnej formy. Nie pamięta oryginalnej formy. Edwin powiedział, że w jej żyłach płynie krew Margaret; wzór Aliny jest niestabilny (ale Edwin nad tym pracuje). Na szczęście Alina zauważyła, że jest w stanie zrobić wymaz ze swojej oryginalnej formy i przekształcić się w to jak w cudzą formę.
Normalne odwracanie to jakiś 5-minutowy proces w ciągu którego stopniowo Alina przechodzi przez formy pośrednie. 
Wbrew prośbie Aliny Edwin odmówił gdy chciała zmienić się w niego. Powiedział, że na to jeszcze za wcześnie. Zmiana w Margaret czy Edwina jest niebezpieczna dla Aliny, ale w Hektora czy Marcelina nie.
Edwin powiedział, że Alina nie ma swojej podstawowej formy; dlatego krew Margaret. Z jakiegoś powodu Margaret jest najbardziej kompatybilna z Aliną i właśnie Margaret staje się "podstawową formą" Aliny. Dlatego Alina nigdy nie może się w nią zmieniać.
Weszła Margaret i spytała z marszu Edwina kiedy zaczną "wzmocnienie" Klemensa. Edwin spytał, czy Mojra nic nie wie. Margaret potwierdziła - Mojra nic nie wie. Edwin powiedział że wyprawią Alinę i mogą zaczynać. Wtedy Margaret dopiero zorientowała się, że Alina tu jest. Chciała rzucić jakiś skanujący czar na Alinę, ale Edwin jej zabronił. Powiedział, że Margaret nie może nigdy rzucać na Alinę czarów a Alina nie może przybrać formy Margaret.
Podbudowana (niezbyt) tymi słowami Alina poszła do Borysa.

Borys Kumin Alinie się nie spodobał. Alina chce chronić i pomagać. Borys dba o interesy Blakenbauerów niezależnie od kosztu. Jednak są w stanie współpracować a pomysł Borysa jedynie Alinę ucieszył - Borys powiedział, że chcą porwać maga Millennium, niejakiego Konrada Węgorza a najlepiej gdyby udało się go tak porwać by wina spadła na Sebastiana Tecznię. Bo w ten sposób źródło które sprawiło, że Alina i interesy Blakenbauerów ucierpiały zostanie uszkodzone.
Kim jest ów Konrad Węgorz? Mag Millennium, nie pochodzący ze Śląska. Niezbyt silny, ale Millennium nie ma w zwyczaju wysyłać magów samotnie na akcje; nadal strzegą swoich magów. To co Borys wie to to, że Węgorz ma wrogów w Millennium. Z tego co Borys wie od wrogów Konrada, magowi towarzyszy nie więcej niż dwóch agentów Millennium. Żaden z nich nie jest magiem. Borys ma uprawnienia do zdobycia wsparcia na akcję (włączając w to różne bioformy Blakenbauerów); Alina dowodzi więc Alina powie jakie wsparcie jest potrzebne. Borys nie może dowodzić, bo nie ma wiedzy bojowo-militarnej.
Alina nie pytała dlaczego ona dowodzi. Słyszała odpowiedź w duchu "ostatnio dowodził Hektor".

Co wiemy o Węgorzu: Borys powiedział następujące rzeczy:
- astralika, iluzje jak chodzi o magię; stosunkowo niewielka moc magiczna i mała transformacja.
- w świecie ludzi jest performerem, "magiem scenicznym" i imprezowym. Często współpracował z Netherią Diakon.
- ostatnio bardzo interesował się lustrami.
- aktualnie przebywa w Kotach, powód jego wizyty tam jest nieznany. Za tydzień jest Festiwal Marzeń; może występuje (co by miało sens).

Alina dała Borysowi pierwsze zadanie - Borys ma zdobyć pewną bandę oprychów. Jako, że Sebastian Tecznia znajduje się w Kopalinie, nie powinno być to szczególnie trudne; Borys ma zapewnić, by:
- Alina była w pogotowiu (lub być oprychem)
- oprychy mają pobić do nieprzytomności i okraść Sebastiana Tecznię. Alina chce zdobyć kilka wzorów transformacji.
Po namyśle Alina zaproponowała Borysowi inny plan:
- Alina będzie w formie rumuńskiego dziecka i spróbuje Sebastianowi "sprzedać" jakąś błyskotkę czy perfumy
- Alina się rozedrze, kopnie Sebastiana w goleń i ucieknie do zaułka. Ma zamiar pociągnąć terminusa za sobą
- Tam czekają oprychy i zrobią swoje.
Borys plan docenił. Zwłaszcza dlatego, że jego tam nie ma.

Dwa dni później (kiepski rzut) pułapka doszła do skutku. Sebastian Tecznia został zlokalizowany w Kopalinie, niedaleko sklepu jubilerskiego "Electrum". Poczekali przez moment aż Sebastian wyszedł ze sklepu i Alina weszła do akcji już w formie dziecka. Alina tak się ubrała i ucharakteryzowała, żeby Sebastian Tecznia poczuł do niej lekką odrazę. Nie było to trudne, bo ogólnie gardzi ludźmi. Zaczęła mu wciskać jakieś perfumy. Sprowokowała go do tego, by się na Alinę zamachnął i agentka się rozdarła na całą ulicę, skupiając uwagę wszystkich na tej dwójce. Sebastian zamarł, ona kopnęła go w goleń i zaczęła uciekać. Sebastian ją dopadł. Alina jest dobra w walce wręcz ale on jest wyszkolonym terminusem specjalizującym się w tym obszarze więc doszło do szamotaniny. Alina została ranna (do krwi), ale i Sebastian został ranny (do krwi), oboje trafili do tego zaułka gdzie czekały oprychy Borysa...
...tyle, że nie mieli okazji zadziałać.

Pod wpływem zapachu krwi swojej ofiary (Sebastiana) Alina zmieniła formę w coś pośredniego między człowiekiem a karaluchem. Całkowicie nie spodziewający się tego Sebastian został zaatakowany, zraniony, pokonany siłowo przez karaluchostwora i Alina wżarła się mu w żyłę wysysając krew. Z trudem, dała radę się opanować nie zabijając maga. Jej oprychy widząc co się stało zwiały. Alina skupiła się na Sebastianie i jego krwi oraz zmieniła formę stając się Sebastianem. Po czym zabrała jego ubranie, założyła jego ubranie (zostawiła mu bieliznę), skopała dla "śladów" (i przyjemności) oraz wezwała transport Borysem i powiedziała mu co się stało. Borys powiedział, że wyczyści oprychy (żeby nic nie wiedzieli) oraz Alina wyczyściła preparatami dostarczonymi przez Borysa ślady swojej krwi.
Alina poczuła się trochę dziwnie, czuła tak jakby Obecność "czegoś", czegoś bardzo potężnego, ale jej przeszło (2/k20, wymagane 15+).
No nic, operacja udana. Sebastian został w majtkach i skarpetkach, nieprzytomny (przykryty śmieciami by nie zamarzł) i oblany cuchnącymi płynami by utrudnić jakiekolwiek ślady. Jedyne co chłopak wie to to, że zwabił i załatwił go jakiś nieznany mu wcześniej vicinius.
A Alina ma podłą satysfakcję, bo to przez NIEGO ona ma teraz takie problemy.

Alina wróciła do Rezydencji, jako Sebastian Tecznia i pierwsze co zrobiła to poszła do Edwina. Powiedziała mu absolutnie wszystko jak wyglądała sytuacja. (Edwin test: 10+2 -> 13). 
Edwin powiedział jej, że najprawdopodobniej została napromieniowana podczas akcji z LegioQuant do tego stopnia, że nie tylko jej wzór się rozerwał ale i krew Margaret nadała jej część własności ich rodu. Przynajmniej dopóki ma w sobie krew Margaret od której jest uzależniona. Edwina bardzo zaniepokoiło to poczucie Obecności; to może być Rezydencja, ale to byłoby bardzo dziwne. Edwin też wyjaśnił Alinie jak działa proces transformacji maga w członka magicznego rodu (przez transfer krwi i inhibicję mechanizmów obronnych); tyle, że mimo, że zrobił to Alinie ona magiem nie jest. Może ma taki poziom natywnego Skażenia, że ten sam mechanizm zaczął działać, może...

Alina powiedziała, że dopóki nie dowie się jak zmienia się w karaluchostwora bojowego nie chce narażać nikogo na akcji. Poprosiła Edwina o miejsce i środki do przeprowadzenia paru testów. Edwin dodał, że żaden Blakenbauer nie jest w stanie kontrolować swojej formy bestii.

"Powiedziałbym 'witaj w rodzinie'. ...mam karalucha w rodzinie..." - Edwin do Aliny, o jej sytuacji.

Borys dostał nowe zadanie od Aliny - znaleźć choć jednego agenta Millennium chroniącego Konrada. Do tego, zdobyć zaufanego mentalistę. W międzyczasie Alina skupi się na eksperymentach ze swoją drugą formą. 
Ku swojemu przerażeniu, w pomieszczeniu z Aliną znajduje się jeden człowiek z nożem w ręku.
Alina chciała uciec przez drzwi. Drzwi są zamknięte. Usłyszała spokojny głos Edwina "bez obaw, kontroluję sytuację".
Alina zaczęła błagać, by uwolnił człowieka lub ją, że można zrobić to inaczej, że to za duży koszt. Edwin powiedział, że on decyduje co jest za dużym kosztem. Kontrolowany mentalnie człowiek zaciął się nożem w rękę. Alinie ruszył ślinotok i głęboka potrzeba transformacji i pożarcia nieszczęśnika, ale to opanowała. Skuliła się w kącie i drżała. Edwin uznał, że to dobry początek i ją stamtąd wypuścił. Alina skuliła się i wpadła w chorobę sierocą.
Edwin powiedział, że czeka ją wiele takich sesji. Alina musi nauczyć się kontrolować i reagować na zapach krwi. Dziewczyna NIE jest mu wdzięczna.
Zranienie Aliny, jej własna krew nie wywołuje absolutnie żadnej reakcji.

Zostały dwa dni do Festiwalu. Borysowi udało się zdobyć maga - mentalistę. Niestety, nie udało mu się zdobyć informacji odnośnie tego kim są agenci chroniący Węgorza od Millennium. To co mu się udało zrobić to postawić obserwację i śledzić ruchy Konrada Węgorza. Na bazie tego zidentyfikował dwóch potencjalnych agentów Millennium - Marysię Kiras oraz Filipa Sztukara. Oboje "na oko" są ludźmi. Przemysław zachowuje się jak agent bliski (rozmawia z Konradem, trzyma się blisko itp), Marysia jak agent daleki (prowadzi obserwację, skanuje teren itp). Marysia dodatkowo jest agentem lokalnym, czyli mieszka w Kotach.

Margaret zaproponowała Alinie, że jeśli Alina potrzebuje czyjejś części ciała (tkanki) by się w tą osobę zmienić i ma zbędne części ciała, to ona spróbuje sformować Alinie coś, dzięki czemu będzie zawsze miała pod ręką próbki Sebastiana Teczni. Potrzebuje jedną próbkę. I tak, jest też w stanie zrobić pigułki Aliny - dzięki temu Alina ma zawsze dwie formy w które może się zmienić: Alina Bednarz i Sebastian Tecznia.
Margaret się tym zajmie; chwilkę potrwa synteza i badania.
Zła natura Aliny właśnie zaczęła wychodzić. Nie lubi Sebastiana Teczni.

Żeby nie ryzykować, Alina wzięła transfuzję krwi przed wyjazdem na akcję do Kotów.

Żeby nie powtórzyć wszystkich błędów Hektora które doprowadziły do katastrofy w LegioQuant Alina zdecydowała się poczekać do Festiwalu marzeń sennych. Podczas tych pozostałych dni sama pojechała do Kotów w formie Sebastiana Teczni by zdobyć jak najwięcej informacji na temat Konrada Węgorza i jego celów (oraz agentów), ale także samego Festiwalu. Borys też przydzielił jej kilku agentów. Wskazał też Alinie maga-mentalistę; niejaki Artur Żupan zrobi bardzo wiele za odpowiednią zapłatą. Jest to żałosny padalec i nie ma kręgosłupa, więc "Sebastian" powinien "dobrze chować swą tożsamość". Wtedy Millennium dojdzie do Żupana który wskaże na Tecznię jako zleceniodawcę.
A Borys zapewnia bezpieczny punkt odbioru Konrada. W samochodzie należącym do Zajcewów. Co więcej, FAKTYCZNIE należącym do Zajcewów którzy są całkowicie "legalnie" wynajęci do przewiezienia osoby bez zadawania za dużej ilości pytań.

Tak więc Alina jako Sebastian Tecznia poszła wynająć Artura Żupana. Oczywiście w "Rzecznej Chacie" w Kopalinie. Alina po mistrzowsku dała się zobaczyć barmance i bywalcom (zachowując się jakby przesadnie i niewprawnie próbowała nie zwracać na siebie uwagi). Alina przedstawiła się jako Sebastian (ale podała inne nazwisko), symulując typowy błąd nowicjusza. Żupan spojrzał na Alinę przez monokl (by sprawdzić iluzje lub niewłaściwą aurę magiczną szukając magii transformacji; nie znalazł żadnych). Alina podała termin, Żupan podał cenę, Żupan wygrał negocjacje ale Alina nie dała się doszczętnie orżnąć. I dopłaciła za "bez zadawania pytań". Wciąż, jest to kwota w zasięgu możliwości Sebastiana Teczni, zwłaszcza wspieranego przez Karolinę Maus.

Festiwal marzeń sennych w Kotach, na rynku. Festiwal ma trwać dwa dni. Do Kotów zjechało się mnóstwo ludzi, Alina jako Sebastian Tecznia bez żadnego problemu rozpłynęła się w tłumie. Zlokalizowała Filipa Sztukara - iluzjonista dał efektowny pokaz i schronił się w przyczepie zaparkowanej na obrzeżach Kotów.
Alina jest wyposażona w środek obezwładniający załatwiony przez Borysa. Alina bez żadnego problemu zauważyła, że Filip Sztukar się nie pilnuje; nie spodziewa się ataku.
Alina podeszła do przyczepy i zapukała. Niczego nie spodziewający się iluzjonista otworzył drzwi i Alina bezwzględnie zrobiła mu zastrzyk płynem Borysa. Filip Sztukar stracił przytomność na miejscu. Weszła do przyczepy i zamknęła drzwi. Po czym ściągnęła Artura Żupana.

Artur Żupan na życzenie "Sebastiana" przeprowadził powierzchowny transfer. Alina nie ma dostępu do wiedzy i umiejętności Filipa, ale potrafi zachowywać się jak on i tymczasowo odpowiadać na kluczowe pytania. Zna też podstawowe hasła i wzory zachowań. Żupan to zaquarkował (na koszt Aliny) i ostrzegł, że lekko świeci energią magiczną. Alina wie.
Zadzwonił telefon Sztukara. Alina kazała Żupanowi wyjść i stać na straży. Upewniła się że nie podgląda i przekształciła się w Filipa Sztukara. 
Alina odebrała telefon. Usłyszała rozemocjonowany głos Konrada Węgorza - "znalazł ją". Naładował lustra i ją zobaczył (czarodziejkę; Filip szukał czarodziejki). Powiedział, że czarodziejka jest w bardzo złym stanie i Netheria musi o tym jak najszybciej usłyszeć. Powiedział, że spróbuje dowiedzieć się czegoś więcej; chwilowo ma tylko to, co ma - niewiele. Spróbuje jeszcze naładować lustra (cokolwiek to znaczy). Alina wie, jakiej czarodziejki on szuka - szuka Marty Newy.
Marta Newa nic nie mówi Alinie.

Alina wróciła do postaci Sebastiana i wezwała ponownie Żupana (pigułki Sebastiana się przydają). Zażądała, by ten poszukał agentów powiązanych z Konradem Węgorzem w głowie iluzjonisty. Bez problemu - Żupan powiedział, że agenci Millennium nie mają pojęcia o Marysi Kiras, ona pomogła im w znalezieniu miejsc i odpowiedzi na parę pytań Konrada, ale traktowała ich jak każdego innego - poza ogromną wdzięcznością do Konrada za zasponsorowanie Festiwalu marzeń sennych.
Agenci Millennium w okolicy to on - Filip Sztukar, Konrad Węgorz który jest tu z jakąś misją nadaną przez siebie odnośnie zaginionej czarodziejki i jeszcze jeden agent, niejaki Robert Przerot. Ten jest człowiekiem, faktycznym ochroniarzem Konrada i on wie o istnieniu magii i umie walczyć.
Dodatkowo Alina zażądała od Żupana, by ten poznał nazwisko człowieka który jest iluzjonistą i może zastąpić Sztukara na festiwalu. Nie ma żadnego problemu, Żupan dostarczył odpowiednie nazwisko.

Żupan nie jest już Alinie potrzebny. Wymazał i zafałszował Sztukarowi pamięć by Alina mogła porzucić go w jakimś rowie (tym się zajmie Borys) i po otrzymaniu zapłaty za swoją usługę oddalił się. Alina pojechała z nim do Kopalina - ale poszła do Rezydencji Blakenbauerów by zdobyć pistolet strzałkowy i jakiś potężny środek obezwładniający. Margaret dała Alinie coś, czego używał w tych celach Marcelin (nikt nie chce pytać czemu).
Alina zabrała nowy sprzęt i ruszyła z powrotem do Kotów.

Późny wieczór. Alina zmienia się w Filipa Sztukara (jedyna różnica między nią a Filipem to to, że ma tonfę; zabrała Sztukarowi ubranie). Dzięki informacjom od Żupana Alina wie, gdzie oni mieszkają - mają wynajęty domek; niewielki.
Alina puka do drzwi domku. Otwiera Robert Przerot. Alina wpakowuje mu pocisk ze strzałkowca; ochroniarz upada na ziemię nieprzytomny. Wychodzi niczego nie spodziewający się Konrad Węgorz słysząc dźwięk upadku ciała ochroniarza i Alina jego też unieszkodliwia. Gdy Alina odwracała się by zamknąć drzwi, zobaczyła Marysię Kiras... która widziała całą sprawę i właście zeszła z roweru (11v8->8).

Marysia krzyknęła "policja!" i ruszyła na Alinę. Ta próbowała ustrzelić ją pistoletem strzałkowym, ale Marysia uniknęła strzału i wykopnęła broń Alinie (11v12->11). Alina nie jest w stanie walczyć z Marysią uczciwie; ma jednak ukrytą tonfę więc wykorzystała ją by uderzyć Marysię i się oderwać. Marysia dała radę uniknąć ciosu z zaskoczenia i na posesję samochodem wjechał Borys chcąc potrącić Marysię. Alina złapała dziewczynę i rzuciła się z nią pod koła (11v12->11). Alina jest viciniusem i to z tych twardszych; zniesie więcej. Marysia jest ciężko poturbowana. Borys szybko złapał Węgorza i wepchnął go do samochodu, po czym pomógł Alinie wpłeznąć do środka po czym szybko ruszył.

Policja ściga Borysa; ten z zimną krwią zjechał na chodnik potrącić losową osobę, po czym pojechał dalej, jeszcze szybciej. Pojedynczy radiowóz się zatrzymał. Ten akt okrucieństwa, przemocy i bezwzględności po prostu ich zaszokował; chcą pomóc oraz boją się dalszych ofiar.
Tymczasem Borys opuszczając pole widzenia wszystkich dookoła wjechał do lasu. Zatrzymał się na poboczu, zmusił Alinę do wyjścia i wyciągnął nieprzytomnego Węgorza, po czym podpalił samochód i odlecieli w brzuchu jednego ze stworów Rezydencji Blakenbauerów.

W wyniku tej akcji (zwłaszcza akcji Marysi) Sztukar wypłynął. To w dalszej konsekwencji powoduje, że Millennium które miało idealny przykład że to była robota Sebastiana Teczni zorientuje się relatywnie szybko że jakkolwiek Tecznia jest z tym w jakimś stopniu powiązany, nie tylko nie mają dowodów ale też nie są pewni czy to on (choć jakoś tam dostanie, ale nie tak mocno, jak by powinien). Nadal jednak nie ma śladów łączących Blakenbauerów z tą akcją a Konrad Węgorz trafił do Rezydencji by można było rozpocząć nad nim pracę by sformować nowego członka Oddziału Zeta.

Ale najpierw Edwin musi poskładać Alinę po przejechaniu przez Borysa.


# Lokalizacje:

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Centrum
                        1. Rzeczna Chata
                    1. Dzielnica Owadów
                        1. sklep jubilerski Electrum
                    1. Obrzeża
                        1. Rezydencja Blakenbauerów
                            1. Podziemia Rezydencji 
                                1. Czarne laboratorium
            1. Powiat Czelimiński
                1. Koty
                    1. Centrum
                        1. Rynek, gdzie odbywa się festiwal marzeń sennych
                    1. Dwie hałdy
                        1. Domki na wynajem, gdzie wynajęli domki Węgorz i Przerot
                    1. Obrzeża
                        1. Nieużytek służący jako parking


# Stakes

Żółw:

- Czy Netheria pozna los Marty Newy? -> Tak.
- Czy Sebastian Tecznia zostanie prawidłowo wrobiony przez Alinę? -> Tak.
- Czy Alina dowie się co kryje się za Obecnością? -> Nie, jeszcze nie teraz.

# Zasługi

* vic: Alina Bednarz jako vicinius uczący się od nowa swoich umiejętności i granicy możliwości po akcji w LegioQuant.
* mag: Edwin Blakenbauer jako zimny i pragmatyczny lekarz chcący pomóc Alinie nawet wbrew niej, nie dopuszczający opcji jej zniszczenia.
* mag: Margaret Blakenbauer jako naukowiec opracowująca "pigułki Aliny i Sebastiana" oraz "honorowa dawczyni krwi" dla Aliny.
* czł: Klemens X jako zwłoki na stole operacyjnym, na których Edwin i Margaret będą coś robić.
* czł: Borys Kumin jako osoba usługi wszelakie na rzecz Blakenbauerów pokazująca, że nie ma okrucieństwa do jakiego się nie posunie by nie przegrać.
* mag: Konrad Węgorz jako 40~50*letni mag Millennium (astralika + iluzje) który próbuje odnaleźć Martę Newę. Cel Borysa i Aliny.
* mag: Sebastian Tecznia jako ciężko ranna ofiara nie kontrolującej się Aliny w formie bojowej (gdy Alina odkrywała swoją nową naturę).
* czł: Marysia Kiras jako osoba bardzo wdzięczna Konradowi za Festiwal, która zniszczyła misterny plan Aliny chroniąc nieprzytomnego maga.
* czł: Filip Sztukar jako potencjalny agent "bliski" Millennium zdaniem Borysa; iluzjonista, którego Alina załatwiła zastrzykiem.
* mag: Artur Żupan jako mentalista wynajęty przez "Sebastiana Tecznię" (Alinę) do porwania maga Millennium (o czym na etapie wynajmowania nie wie).
* mag: Marta Newa jako czarodziejka której szuka Konrad Węgorz z Millennium, przez lustra.
* mag: Netheria Diakon jako czarodziejka Millennium której Konrad powiedział przez telefon o stanie Marty Newy (co widział w lustrze).
* czł: Robert Przerot jako FAKTYCZNY agent Millennium skrajnie lojalny Konradowi Węgorzowi; umie walczyć i wie o magii.
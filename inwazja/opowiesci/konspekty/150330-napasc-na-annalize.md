---
layout: inwazja-konspekt
title:  "Napaść na Annalizę"
campaign: powrot-karradraela
gm: żółw
players: kić, dzióbek
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [150325 - Morderstwo jak w książce (PT, HB)](150325-morderstwo-jak-w-ksiazce.html)

### Chronologiczna

* [150329 - Knowania Izy(SD, PS)](150329-knowania-izy.html)

## Wątki

- Szlachta vs Kurtyna
- Wojny wielkich gildii Śląska
- Trzeci kontroler Spustoszenia

## Punkt zerowy:

PODSUMOWANIE:

- Ktoś (Iza) wypuścił Spustoszenie w Kotach. Spustoszenie wymknęło się spod kontroli (niespodzianka).
- Antygona jest święcie przekonana, że była w to zamieszana; jej pamięć coś nie do końca. Atm Antygona jest u Draconisa z Draceną.
- Iza Łaniewska wszystko tuszuje. Wykorzystuje Dracenę, Antygonę, Tamarę do jakiegoś nieznanego celu.
- Overmind Spustoszenia Aurelii z Powiewu ma być przeniesiony pod opiekę Marty, na KADEM.
- Powoli rosną napięcia pomiędzy KADEMem a Srebrną Świecą do stanu pomiędzy zbrojnym pokojem i zimną wojną.


## Misja właściwa:

"Jaka jest motywacja Izy do robienia tego wszystkiego?" - Siluria.

Siluria i Paweł czekają na to, aż Draconis będzie w stanie coś im powiedzieć; zwłaszcza odnośnie stanu Antygony. Potrwa to najpewniej koło dnia. 
Marta Szysznicka poprosiła Zespół do siebie, na rozmowę. Zanim jednak poszli, poprosili Ignata Zajcewa, by poszedł dowiedzieć się czy ktokolwiek wie odnośnie tego co Iza robi; czy ucierpi i jak bardzo jeśli coś się rozniesie. Ignat powiedział, że nie jest to jego najsilniejsza strona, ale zaproponował Andżelikę.

Oki, Andżelika Leszczyńska. Zapytana przez Zespół o "Szlachtę", Andżelika odpowiedziała, że większość grup SŚ ma dwa człony w nazwie, by się odróżniać. I Andżelika nie kojarzy żadnej grupy znanej jako "Szlachta". W świetle tego - niechętnie - Zespół zdecydował się na powiedzenie Andżelice kontekst - Antygona jako zabawka. Andżelika powiedziała, że są takie tendencje w Srebrnej Świecy zwłaszcza wśród młodych i potężnych politycznie magów. Ale powiedziała też, że Wydział Wewnętrzny SŚ powinien temu zapobiegać. Gdy Zespół powiedział Andżelice całą sprawę, Andżelika aż usiadła.

Srebrna Świeca ma bardzo silny Wydział Wewnętrzny. Ich działania mają za zadanie prowadzić do tego, by mikro-ruchy działały przeciwko sobie, ale żeby wszystko działało w stanie w miarę zbalansowanym. Jeśli takie coś się dzieje, to znaczy, że Wydział Wewnętrzny nie działa dobrze a Srebrna Świeca jako gildia nie działa prawidłowo. Coś się stało.

Andżelika została poinformowana, że Iza Łaniewska jest bardzo niebezpieczna i najpewniej będzie chciała zatrzymać działania Andżeliki. Andżelika współpracuje z Ignatem; spróbuje zlokalizować dla Zespołu ową "Szlachtę".

Tymczasem Marta dała Zespołowi Nowe, Lepsze Zadanie. Jako, że już są solą w oku dla Srebrnej Świecy równie dobrze mogą uczestniczyć w akcji przeniesienia Spustoszenia z Powiewu Świeżości na KADEM. Marta wysłała tam pół KADEMu, większość osób zdolnych do walki (przez co sama została na KADEMie by dowodzić potencjalną obroną). Autoryzowała... nie, wymusiła użycie GS Aegis '0003'. Akcją dowodzi Ewa.

Bardzo kilka godzin później (Ewa nalegała, by ktoś został pilnować, skanować, upewnić się, że nic nie zostało... więc zostawiła Pawłowi VISORa) akcja była skończona, wszystko udało się bez problemu. Paweł zrobił wstępny skan VISORa, działa bez zarzutu. Nawet poszerzono go o skanery Spustoszenia i domontowano osobiste tarcze przeciw Spustoszeniu. Podczas, gdy Zespół znajdował się dalej na Powiewie, zaobserwowali ciekawe zjawisko:

Marian Łajdak niesie nieprzytomną Annalizę ze sobą i woła o lekarza.

Siluria ściągnęła Norberta, Paweł zaczął pytać co się stało.
- Marian szedł od dostawcy, gdy usłyszał zduszony pisk. Pobiegł tam.
- Zobaczył jedną szamoczącą się Annalizę z jakimś magiem który ją trzymał i całował. Annaliza zaczęła się coraz mniej szamotać.
- Marian, przerażony, użył Mega Dziwnego Fajerwerku by przestraszyć tajemniczego maga. O dziwo, mag się spłoszył (na usprawiedliwienie: fajerwerk był dziwny)
- Podczas spłoszenia użył portalu i zostawił za sobą brudne jądro magiczne. Eksplodowało i wytworzyło szerokie Skażenie.
- Na miejscu było jeszcze kilkanaście ludzi obu płci. Nagich. Były przy nich ich ubrania.
- WTF moment.
- Marian wysłał distress signal na KADEM, ale przez akcję był zablokowany przez Ewę.
- Marian wysłał distress signal na kanał magów, pojawiło się dwóch terminusów SŚ. Powiedzieli, że przejmują to. Marian przegadał, że za rogiem jest Norbert i on może szybko podleczyć Annalizę. Magowie SŚ łyknęli; skupili się na ludziach i na Skażeniu.
- Marian ma namiary na tych magów i jest przekonany, że ta akcja była powiązana z SŚ ("bo kto inny").
- Marian będzie w stanie rozpoznać tego maga, który przeprowadził tą akcję.

Pojawił się Norbert. Po wstępnym przebadaniu Annalizy powiedział, że to wygląda jak serum neutralizujące magów; kilka godzin Annaliza zapomni i będzie mieć nadwyrężone kanały magiczne (ale on jest w stanie z tym sobie poradzić). Siluria ponuro zauważyła, że to wygląda jak magiczna pigułka gwałtu.
Norbert za zgodą Julii wziął Annalizę na KADEM.

Pół godziny później, Siluria i Paweł wrócili na KADEM z VISORem. Na wejściu - huk. Na ziemię pada Marcel Bankierz, pod tarczą. Nad nim (na tarasie) stoi Ignat Zajcew, cały wyszczerzony. Siluria stanęła pomiędzy nimi i powiedziała, żeby się opamiętał co Ignat potraktował jako hardmode. Krzyknął "wąąąąż" i wystrzelił magmowego sterowanego węża w Bankierza. Siluria zażartowała z jego węża co na tyle stropiło maga, że wąż mu wpadł w Paradoks. Aegis 0003 natychmiast zadziałała, przechwytując wszystko na tarczę (praktycznie dezaktywując swój reaktor) i następnym ruchem wystrzeliła metalowy pręt by wbić Ignata Zajcewa w ścianę (maksymalny ból).

Marcel Bankierz się otrzepał i powiedział, że takiej gościnności ze strony KADEMu się nie spodziewał. Chce natychmiast składać skargę; jednak Siluria (miła i przytulna) go zneutralizowała. Powiedziała, że to raptus i otrzyma karę jak raptus. Marcel się zgodził odłożyć skargę. Co ciekawe, Marcel siedział spokojnie w swoim pokoju, wpadł Ignat i powiedział "no, będziemy się bić". Marcel nie chciał, na co usłyszał "wolisz bić bezbronne dziewczyny" i Ignat wyciągnął go z pokoju i walnął pierwszym zaklęciem...

Paweł przepytał Ignata o co chodziło, o co poszło.
Ignat dowiedział się o tym, że najpewniej magowie Świecy pobili czarodziejkę Powiewu. Ostatni czas Ignat non stop porusza się wśród "tienowatych" których nie cierpi i nerwy ma zszargane. A ta informacja po prostu była kroplą, która przepełniła czarę.

Siluria poszła spotkać się z Andżeliką. Andżelika jest bardzo nieszczęśliwa z jakiegoś powodu, ale zbyła pytanie o co chodzi. Powiedziała, że Ignat - Marcel to cień ogromnego konfliktu Bankierze - Zajcewowie, który się od dawna ciągnie w Srebrnej Świecy.
Andżelika powiedziała, że jest coś co wygląda na tą "Szlachtę". Bardzo dobrze umocowany mag, Wiktor Sowiński wspierany przez Oktawiana Mausa. Jest więcej magów w takim kręgu i dookoła Wiktora buduje się mały dwór. Andżelika powiedziała, że to jest dziwne, że Oktawian Maus jest w to umoczony; ten mag jest lojalny tylko wobec Mausów i rodu Maus. Andżelika powiedziała też, że Wydział Wewnętrzny powinien sobie z tym poradzić. Na pytanie Silurii "a co jeśli zinfiltrowali też Wydział Wewnętrzny" odpowiedziała, że to nie jest prawdopodobne; szef tego wydziału jest praktycznie apolityczny.
Andżelika powiedziała też, że na KADEMie pojawia się plotka, że to Bankierze pobili Annalizę. Powiedziała, że zajmie się tym by się nie rozprzestrzeniła za daleko. Zajmie się też Marcelem; dotrzymywała mu towarzystwa w normalniejszych okolicznościach.
Andżelika nie jest w stanie w tej chwili skupić się mocniej na śledztwach politycznych (czas).

GS Aegis 0003 się ładuje. Zajmie to kilkanaście godzin, potem znowu jest operacyjna.
Ignat zostanie poskładany następnego dnia.
Annaliza będzie przytomna za kilka godzin.

Siluria poprosiła magów KADEMu o zrzut obrazu i danych dotyczących tajemniczego maga - napastnika na Annalizę z Mariana Łajdaka. Zero problemu, będzie za kilka godzin.

Annaliza się obudziła kilka godzin później.
Siluria i Paweł mogą się z nią spotkać. Poszli.
Annaliza widziała wszystko dokładnie i pamięta bardzo dobrze. Tajemniczy napastnik rozkazywał grupie ludzi by się rozebrali; oni próbowali walczyć z tym, ale nie byli w stanie się oprzeć. Annaliza chciała uciec, ale on ją usłyszał i się odwrócił. Dopadł ją, obezwładnił i pocałował by uciszyć...
Siluria poprosiła, by Annaliza poddała się też skanowaniu, by złapać napastnika. Po wahaniu, zgodziła się.

Kilka godzin później KADEM otrzymał nazwisko napastnika. Młody mag, uczeń na terminusa, Joachim Kopiec.
Ta informacja została przekazana Julii Weiner, by ta złożyła skargę na maga.
Julia złożyła skargę.

Ignat Zajcew został ukarany. Dodatkowo musiał pójść przeprosić Marcela Bankierza. Wolałby być zamknięty w zimnej, zawilgłej piwnicy, ale Ewa była bardzo przekonywująca.

Późnym wieczorem, Siluria dostała komunikat. Od Joachima Kartela, swojego kontaktu w SŚ. "Przepraszam bardzo, ale nie będę w stanie więcej dla Ciebie szpiegować. Piszę to pod przymusem." Andżelika poszła z tym do Andżeliki (ta już w pidżamie).
Po rozmowie (i kilku informacjach o działaniach Silurii), Andżelika wydedukowała, że to jest kwestia frakcji w Srebrnej Świecy. Joachim Kartel jest magiem we frakcji bardziej pro-współpracowej, szerzenia wiedzy i "otwartej Świecy". Stalowa Kurtyna jest frakcją skupiającą się na konsolidacji, zbudowaniu przewagi Srebrnej Świecy nad innymi gildiami i z ich punktu widzenia to może wyglądać jak działania KADEMu przeciwko jedynej frakcji która może KADEMowi zagrozić.
Plus, nadal na KADEMie jest Zenon Weiner, czołowy ekspert Świecy do spraw Spustoszenia, którego wiedzę MOŻE KADEM wyciągać. Siluria i Paweł nic nie wiedzą o tym, by takie coś miało miejsce, ale Kurtyna tego nie wie. A Marcel Bankierz został odizolowany (nie może chronić Zenona Weinera 24/7).
Andżelika zauważyła, że kimkolwiek osoba stojąca za notatką jest, najpewniej chce by Siluria nie spała dobrze tej nocy lub by zrobiła błędny ruch.
Super.
Andżelika obiecała, że dowie się tego tak, by nikomu nie zaszkodzić. Poprosi o pomoc przyjaciółkę.

Następnego rana, Paweł dostał wiadomość od Julii. Srebrna Świeca twierdzi, że to nie Joachim Kopiec, że Joachim Kopiec był w innym miejscu i żąda dowodów oskarżenia. Paweł przekazał Julii zrzut pamięci z KADEMu (autoryzowany i potwierdzony) i Julia wysłała to do Srebrnej Świecy.
W zupełnie innym miejscu, w Świecy ZNOWU zauważono, że powiązany z tym jest KADEM.

Zespołowi otworzyło się okno spotkania holograficznego z Draconisem Diakonem. Już dowiedział się czegoś od Draceny i Antygony; już coś może powiedzieć (lub przemilczeć).
"Nie mogę powiedzieć wam niczego odnośnie spraw Srebrnej Świecy, ale pomysł użycia Spustoszenia pochodził od terminusa" - Draconis.
Draconis nie może powiedzieć, dlaczego Antygona się w to wszystko zaangażowała; było to głupie i ryzykowne, ale miało to sens. Powiedział też, że przy posiadanych faktach Antygona podjęła decyzję racjonalnie, ale zaznaczył, że kto posiada fakty, kieruje decyzją.

Antygona miała nieco pomieszaną pamięć i chronologię najnowszych (od czasu Spustoszenia) faktów, ale udało się to uporządkować w miarę. Najpewniej była w Kotach i faktycznie służyła jako przynęta i pułapka przeciw jakimś magom Świecy.

Wraz z Draconisem udało im się jakoś odtworzyć chronologię Spustoszenia:
- Izabela wyciąga Spustoszenie #1 manipulujące pamięcią z SŚ; to Spustoszenie nie ma kontrolera.
- Spustoszenie #1 zostaje nauczone przez OSA Powiewu Świeżości samozniszczenia, walki ze Spustoszeniem i formowania kontrolera.
- Spustoszenie #1 trafia do Antygony która zostaje kontrolerem.
- Spustoszenie #2 jest wzięte przez Mariana i Zenona do Kotów
- Dochodzi do rezonansu zaklęć
- Spustoszenie #2 działa na Kopiec Miślęgów (kontroler 1).
- Spustoszenie #1 działa na Antygonę (kontroler 2).
- ...
- Spustoszenie #1 ulega samozniszczeniu.

Draconis został poproszony przez Silurię, by dowiedział się czy z Joachimem Kartelem wszystko w porządku. Potwierdził. Będzie sąd koleżeński który wyda wyrok "niewinny"; jego funkcją nie jest skazanie a pokazanie, że współpracowanie z KADEMem się nie opłaca.

Draconis dodał też, że informacja o Spustoszeniu wypłynęła szerzej. Nie jest to korzystne z jego punktu widzenia, ale zaczyna się wypływać i ujawniać.

Annaliza - spotkanie z Izą (która chciała spotkać się z Annalizą w sprawie tego napadu). Siluria i Paweł zauważyli, że Iza jest powiązana ze sprawą Spustoszenia co Annalizę zmartwiło. Jednak zdecydowała się pójść spotkać z Izą; do obserwacji i obrony Annalizy Siluria wysłała GS Aegis 0003.
Annaliza poszła.
Dwie godziny później, mimo wszelkich zmartwień i nieszczęść militarystów KADEMu, GS Aegis 0003 i Annaliza wróciły na KADEM.

Iza pytała Annalizę o pełny skan działań jej pamięci w zakresie działania "Joachima Kopca". Powiedziała, że w czasie ataku na Annalizę Joachim Kopiec znajdował się na szkoleniu terminusów i koło niego byli inni uczniowie i jeden nauczyciel. Nadal, zdaniem Izy, jest możliwe by to był Joachim Kopiec więc ona chce dostać jak najwięcej informacji by móc określić co i jak.
Annaliza wyraziła zgodę. KADEM dosłał resztę materiałów Świecy.

# Streszczenie

Cóż, Wydział Wewnętrzny Świecy na pewno nie działa poprawnie - tak silne frakcje nie powinny istnieć. Dodatkowo, Annaliza została zaatakowana przez "Joachima Kopca" (nieznany napastnik); spłoszonego szczęśliwie przez Łajdaka fajerwerkiem. Rośnie wrogie napięcie między KADEMem a Świecą. Pojawiają się plotki wrogie dla obu gildii. Ignat wpadł w kłopoty z Marcelem ze Świecy, KADEM przetransportował Spustoszenie z Powiewu na KADEM a magowie Świecy współpracujący z KADEMem mają problemy przez Tamarę. Aha, Draconis pomógł zrekonstruować prawidłową chronologię działania Spustoszenia - jedno było przyniesione przez Antygonę, drugie przez Mariana i oba się od siebie nauczyły z niewiadomych przyczyn i powodów.

# Lokalizacje:

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Centrum
                        1. KADEM Primus
                    1. Nowy Kopalin
                        1. Powiew Świeżości

# Pytania:

TO RESOLVE:

K - Dlaczego Judyta Karnisz, terminuska ze Świecy zwróci się o pomoc do Pawła i Silurii w sprawie po Spustoszeniu?
Ż->K - Bo w każdym innym wypadku pewni magowie zostaną niesprawiedliwie i nieproporcjonalnie mocno ukarani za Spustoszenie.
K - Dlaczego Oktawian Maus bardzo nie chce wpaść w ręce Tamary Muszkiet?
Ż->K - Bo nie chce zostać uznany winnym z automatu.
B - Dlaczego wybór miślęga jako kontrolera Spustoszenia nie był przypadkowy?
Ż->B - Kraloth był czynnikiem nieprzewidzianym; natomiast miślęg jest czymś czym da się w miarę łatwo sterować.
K - W którym aspekcie Iza nie do końca zgadza się z celami Tamary?
Ż->K - Izie zupełnie nie zależy na znalezieniu winnych Spustoszenia.
B - Dlaczego Tamara, mimo, że wie czym jej to grozi będzie próbowała ominąć zakaz?
Ż->B - Bo jest w jakimś stopniu powiązana z EAM i chce chronić wiedzę Zenona przed KADEMem.
Ż - Dlaczego "akcję w której znajdował się nieznany mag" mogła uratować tylko Izabela Łaniewska?
B - Były potrzebne takt i wyczucie Izy, jej niezaangażowanie polityczne i fakt że nie ma nic do stracenia.
Ż - I w jaki sposób nieznany mag by ucierpiał gdyby Iza tego nie zrobiła (high stakes)?
K - Zostałby poddany jakiegoś typu przemianie; przekształciliby go na maga rodu Sępiak.
Ż - I czemu Judyta Karnisz nadal się interesuje sprawą Spustoszenia?
B - Czuje się wymanewrowana przez KADEM w tej sprawie.
Ż - Czemu Paweł nic nie wie o tym, że ktoś próbuje reanimować ród Sępiak?
K - Te próby reanimacji są przeprowadzane tak, by w żaden sposób nie wiązać się z istniejącym rodem.

NEW:

[brak]

# Zasługi

* mag: Siluria Diakon jako balsam na Marcela Bankierza i kontroler GS Aegis 0003.
* mag: Paweł Sępiak jako osoba, montująca drobnymi socjalnymi czynnościami jeden front KADEMu.
* mag: Draconis Diakon jako ten, który pomógł zrekonstruować chronologię Spustoszenia Zespołowi; też chroni Dracenę i Antygonę.
* mag: Antygona Diakon, która dochodzi do siebie po strasznym poczuciu winy i lukach w pamięci u Draconisa.
* mag: Oktawian Maus jako członek tajemniczej "Szlachty" SŚ; nie do końca pasujący do wzoru "Mausa idealisty".
* mag: Wiktor Sowiński jako przywódca tajemniczej "Szlachty" SŚ; okrutny i podły.
* mag: GS "Aegis" 0003 jako bardziej autonomiczna i skuteczna niż zwykle głowica bojowa; pod kontrolą Silurii.
* mag: Marta Szysznicka jako przywódczyni KADEMu która doprowadziła do przeniesienia Overminda na KADEM i zmusiła Silurię do wzięcia GS Aegis 0003 na akcję.
* mag: Ewa Zajcew jako głównodowodząca akcją przeniesienia Overminda Spustoszenia Aurelii z Powiewu Świeżości na KADEM.
* mag: Marian Łajdak jako przypadkowy bohater, odstraszający efektownym fajerwerkiem niebezpiecznego napastnika od Annalizy.
* mag: Annaliza Delfin jako czarodziejka Powiewu, która będąc przypadkiem w niewłaściwym miejscu zobaczyła coś, czego nie powinna i została napadnięta.
* mag: Ignat Zajcew jako detektyw który w końcu traci nerwy po zaatakowaniu Annalizy i sam atakuje niewinnego Marcela Bankierza.
* mag: Marcel Bankierz jako niewinna ofiara Ignata Zajcewa, ugłaskany przez Silurię, więc nie złożył skargi.
* mag: Andżelika Leszczyńska jako główny kontroler plotek i bardzo szerokiej wiedzy o Srebrnej Świecy na KADEMie; ratuje przed zimną wojną.
* mag: Norbert Sonet jako główny lekarz KADEMu ratujący pamięć Annalizy po brutalnym ataku.
* mag: Infensa Diakon, wtedy: Izabela Łaniewska, zły duch, który siedzi w tle tego wszystkiego. Do tego, zainteresowała się napaścią na Annalizę.
* mag: Joachim Kopiec jako osoba, która podobno napadła na Annalizę. Tyle, że w tym czasie był gdzieś indziej.
* mag: Joachim Kartel jako ten, z którego Stalowa Kurtyna chce zrobić przykład "dlaczego nie współpracować z KADEMem" przez sąd koleżeński.
* mag: Julia Weiner jako przywódczyni PŚ składająca do SŚ formalną skargę na napaść ze strony Joachima Kopca na Annalizę.
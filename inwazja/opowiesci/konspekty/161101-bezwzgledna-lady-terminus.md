---
layout: inwazja-konspekt
title:  "Bezwzględna Lady Terminus"
campaign: powrot-karradraela
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [160615 - Morderczyni w masce Wandy (HB, KB)](160615-morderczyni-w-masce-wandy.html)

### Chronologiczna

* [161030 - Odbudowa dowodzenia Świecy (AW)](161030-odbudowa-dowodzenia-swiecy.html)

## Kontekst ogólny sytuacji

Andrea została awansowana przez Agresta na tymczasową Lady Terminus Kopalina. Dowiedziała się, że większość jej współpracowników zginęła.
Szczęśliwie, Andrea ma też wszystkie dane z pancerza Sowińskiego.

Andrea ma pewne siły: Mieszka Bankierza, Tadeusza Barana, skrzydło szkoleniowe (3 niedoświadczonych # doświadczony) oraz okoliczne siły i kontakty, Lidię Weiner i Rafaela Diakona. Ma też Kajetana Weinera. Niestety, podobno w Czeliminie jest Krwawa Kuźnia a Spustoszenie poluje na magów w okolicy.

## Punkt zerowy:
## Misja właściwa:

Dzień 1:

Andrea obudziła się z bólem głowy. Hipernet próbował się zaadaptować do nowych uprawnień Andrei, ale bez retransmiterów i potęgi Kompleksu...  nie wyszło mu. Szczęśliwie, ból głowy nie uniemożliwia Andrei działania.
Skrzydło szkoleniowe robi dobrą robotę; nie panikuje. 

"Czy to może być coś na kształt transfuzji? -_-" - Andrea, z nadzieją
"Oczywiście. Nie musisz tego pić" - rozbawiony Rafael

Rafael wyjął ze swojej torby medycznej chłeptnik zwyczajny. Jest to dwustronna pijawka, długa jak wąż. Powiedział, że to najlepsza rzecz do bezpiecznej transfuzji między magami; jedna z bioistot Diakonów. Lidia była donorem i Andrea dostała swoją dawkę.
Ból głowy nie minął, ale poważnie się ograniczył. Andrea też nie czuje zimna w środku. Jednak kody kontrolne dalej chcą ją zniszczyć...
Sam zabieg trwa koło 10 minut. Andrea dostała od Rafaela chłeptnika jako prezent. "Doceniła".

Mieszko wysłał komunikat do Andrei, z prośbą o spotkanie. Mieszko poprosił o spotkanie w magazynie, ukryta. Powiedział, że są ludzie i mag skrzydła szkoleniowego wszedł z nimi w interakcję. NORMALNIE Andrea byłaby w stanie go podsłuchiwać po hipernecie, ale... nie jest dobrze - w Andrei hipernet przez cholerne kody nie chcą działać dobrze.

Andrea dotarła do Mieszka. Tam jest pięciu facetów i jedna młódka, gawędzą sobie przyjaźnie. Hipernet pokazuje, że w tej piątce 2 osoby są magami skrzydła szkoleniowego, oboje są uczniami. Młódka (Anna) poszła w kierunku Andrei i ta ją zatrzymała. Anna poznała Andreę i się nie ucieszyła. Andrea... też miała większą nadzieję. Andrea pokazała Annie sygnał Lady Terminus. Ta nie poznała sygnału, Andrea facepalmowała. Po chwili hipernet się zregenerował i Anna poczuła sygnał... załamała się.

"Po prostu mam przesrane, jak zawsze..." - Anna, widząc, że Andrea została Lady Terminus

Anna wyjaśniła Andrei sytuację - jest wraz z Alojzym na patrolu z własnej inicjatywy. Gdy weszli ludzie, niezauważeni, to Anna i Alojzy zaczęli improwizować. Alojzy pomoże w wyładunku. Ona robi za jego córkę. To normalni pracownicy normalnej firmy.
Andrea się zdziwiła, że biomózg nie zaraportował. Okazało się, że biomózg stracił połączenie z hipernetem więc zresetował do ustawień fabrycznych "jestem w trakcie transportu" i się odpiął od wszystkiego. Rafael ma go przestawić na tryb, w którym hipernet nie jest niezbędny. Na uwagę Mieszka, że mag Millennium zajmuje się mózgiem Andrea odparła, że to najlepsze co ma.

Andrea prześlizgnęła się do ciężarówki i sprawdziła, jak jej stan magiczny. Czysta. Wygląda na normalną sprawę normalnych ludzi. Pocieszające.

Andrea zostawiła magów - niech robią swoje. Złapał ją jeszcze na moment Mieszko. Ostrzegł, że siada logistyka - nie ma żywności, nie ma innych rzeczy. Placówka polegała na obecności hipernetu i kontaktu z Fazą Daemonica. Oba elementy nie działają. Placówka MOŻE być przestawiona w tryb autonomiczny; tak była zaprojektowana. Po prostu z biegiem czasu wszyscy dobudowywali powiązania z Kompleksem, nikt nigdy nie pomyślał (poza pierwszymi chwilami) o tym, że można stracić kontakt.
Co zrobić by przestawić ją w tryb autonomiczny? Trzeba poodcinać większość luksusów. Rozwalić nakładki na generatory - będą hodować "konserwy", nie jest to smaczne, ale jest pożywne. Itp itp. Trzeba zresetować mózg do ustawień fabrycznych.
Mieszko powiedział, że można to zrobić... ale to problem dla morale.
Na szczęście, Świeca ma też konta. Więc można poświęcić kasę na zakupy. W czym - Andrea wie, że ten teren może być monitorowany.
Ale Andrea wie też, że ta placówka nie jest tajna. Bo różni magowie znali jej lokalizację. Więc i tak jest "spalona".

Andrea stwierdza, że należy stąd wiać. Trzeba też przenieść biomózg.
Czas na naradę wojenną! Z wszystkimi magami jakich Andrea ma do dyspozycji.

Andrea wyjaśniła, że dowodzi Świecą tymczasowo. Dodatkowo, powiedziała że hipernet szwankuje. Co gorsza, Andrea dostała sygnał od Agresta - Wydział Wewnętrzny przebudowuje hipernet. Jeśli istnieje choć SZANSA by wykorzystać hipernet przeciwko Świecy, by Spustoszyć hipernet...
Andrea rozumie.

Andrea pokazała jak zła jest sytuacja - znikają magowie, perymetr jest słaby, mało zasobów... ogólnie, kiepsko. A ta placówka jest znana magom Świecy, więc jest spalona.
Rafael Diakon i Mieszko Bankierz mają już połączenia z innymi firmami; to miejsce jest bezpieczne.

Anna zna miejsca w świecie ludzi gdzie warto się zabunkrować.
Wojmił w świecie magów.

Anna, Baran, Pszczelak i Kopiec - mają zdemontować, oczyścić i skanibalizować co się da. Przydatne artefakty, wymontowanie itp. Wyczyścić.
Alojzy - ma przygotować podziemne przejście do punktu X. I zawalić za nimi teren.
Lidia, Rafael, Mieszko - mają odpiąć biomózg i przystosować go do transportu. Potem - przepiąć system defensywny.
Kajetan - ma iść na zakupy i zdobyć wszystko co potrzebne. Jest spoza Świecy i spoza terenu. Nikt o nim nie wie.

Andrea też powiedziała im, jak wygląda sytuacja - są małą grupką. Ona potrzebuje regularnych transfuzji.
Odłączenie mózgu jest ostatnim ruchem jaki ma mieć miejsce. Ostatnim.

W czasie gdy magowie robią swoje rzeczy, Andrea zajmuje się badaniem danych przesłanych przez Agresta.
Pierwszą rzeczą, jaka Andreę uderzyła - analiza tej "kołysanki" z dawnych lat. Ta kołysanka wyraźnie koreluje z tien Aurelią Maus. To nie był kod, ale ta kołysanka była wyemitowana w konkrentne miejsce - do Kotów. Przez retransmitery.
Andrea dostała rekordy, że faktycznie - pancerz BYŁ Spustoszony. Był wektorem Spustoszenia, ale w pewnym momencie Spustoszenie uległo samozniszczeniu. Nie odkryło jednak rejestracji (tego, że jest to wszystko rejestrowane); to Spustoszenie nie było przeznaczone do magitechów. Wiktor był Spustoszony; magitech był tylko nośnikiem.

Te dane pokazują opowieść o młodym magu Sowińskich który rozpaczliwie próbował pokazać swą siłę, próbował stać się "kimś", dołączył do Szlachty i wyszedł tam całkiem wysoko rozgrywając magami - a zwłaszcza ładnymi czarodziejkami - kolejne sojusze. Magitech też zarejestrował "kuszenie Oktawiana Mausa". Jak to Wiktor obiecał Oktawianowi pomoc w odbudowaniu Mausów w Świecy. Mausów, którzy zdaniem Oktawiana są wyniszczani przez Świecę.

Najciekawsze dane udało się zebrać Andrei chwilę potem - w chwili, w której doszło do faktycznego Spustoszenia Netherii, Wiktora, Oktawiana itp. Okazało się, że - mimo, że Antygona Spustoszyła Wiktora (skąd skaskadowało dalej) - jednak OKTAWIAN stał się dowódcą Spustoszenia, overridując Antygonę. Czyli drona Spustoszenia overridowała kontrolera. To implikuje, że przez Oktawiana doszło do połączenia z Overmindem Spustoszenia. I to Overmind wysłał kołysankę.

Oktawian był wektorem Overminda. Czyli to jest połączenie przez Mausów... w jakiś sposób. Idąc tym tropem, udało się Andrei zlokalizować wiedzę o Karolinie Maus - dokładniej, że ona cierpi na soulmeld. Karolina jest Aurelią. A raczej, Karolina jest istotą dwóch osobowości. Niestabilna i bezużyteczna. Chyba, że ma coś co ją kontroluje - a taka kołysanka może zadziałać na pamięć Aurelii. A taka kołysanka może wzmocnić Aurelię... i ukierunkować Karolinę ORAZ Aurelię w tym samym kierunku.

Overmind Spustoszenia wymazał pamięć wszystkich tych magów i się wycofał. Skupił się na walce z drugim kontrolerem, zostawiając sobie TYLKO Oktawiana i echo w pancerzu Wiktora. W przyszłości jeszcze wykorzystał Spustoszony pancerz Wiktora by ten zrobił kluczowe ruchy lub dostarczył odpowiednie informacje. Overmind grał na destabilizację Świecy, wzmacniając Szlachtę jak tylko był w stanie. To Overmind zaproponował kandydaturę Hektora Blakenbauera Szlachcie, jako destabilizator Świecy i narzędzie do walki z Kurtyną.

Overmind nadał Wiktorowi jedno główne polecenie. Dowiedzieć się, gdzie znajduje się Renata Maus. Wiktor nie był w stanie tego zrobić - nie miał uprawnień.
Dla Andrei pojawił się nowy 'darkest scenario'. Zabić Renatę Maus i to ogłosić i udowodnić.
Oczywiście, kontakt z Agrestem jest niemożliwy... hipernet jest przebudowywany...

Dzień 2:

Andrea się obudziła z fatalnym bólem głowy. Ale czuje się... silniejsza. Fizycznie i magicznie. Jej aura jest mocniejsza, trochę inna. Lady Terminus się konstytuuje w Andrei. Wychłodzenie jest głębokie - Andrea umiera. Brakuje jej krwi. Oczywiście...
Tym razem jako dawca krwi posłużył Tadeusz Baran.

Z dobrych wieści: wszystko się udało. Kajetan załatwił żywność, baza jest w większości zdemontowana, biomózg jest podłączony, ale można rozłożyć... 
Anna i Wojmił mają listę miejsc, Alojzy przygotował plany przekopania się przy minimalnej ilości magii. Zwłaszcza do dwóch najlepszych miejsc od Anny.
Alojzy powiedział, że potrzebują dnia by zrobić migrację. To daleka droga. Połączenie badania map oraz analizy otoczenia umiejętnościami górniczymi.
Biomózg gotowy do transportu.

Biomózg zaraportował coś nietypowego. Aktywność na poziomie portalu. Tego starego, nieaktywnego portalu. Biomózg estymuje, że coś się dzieje - ktoś próbuje coś z tym portalem tam zrobić, przepiąć lub zaadaptować. Niewiele "widzi". Estymowany czas przy tych przepływach energii i tej złożoności zgodnie z danymi biomózgu - między dniem a trzema dniami.

Andrea poprosiła Mieszka, Kajetana i Tadeusza na rozmowę w tej sprawie. Wprowadziła ich w sprawę. Uważa, że jest to wstęp do ataku na TĄ bazę. Andrea zaproponowała działania niestandardowe - zrobić COŚ. Terminusi długo radzili i wyszło im, że chyba najlepszym miejscem będzie ruina zamku. Jest w rękach prywatnych, więc jest częściowo kontrolowana. Należy do ludzi.
Andrea potrzebuje kogoś do zdobycia pieniędzy ze Świecy i do bycia frontmenem. Padło na Annę Kozak, bo ona najlepiej zna świat ludzi. Kajetan będzie ją osłaniał i patrzył jej na ręce. Jest zadanie dla Anny i Kajetana.

Mieszko, Tadeusz, Andrea zajmą się budowaniem pułapek, sensorów i innych takich. Przygotowanie bazy do opuszczenia.
Wojmił i Julian mają pomóc Alojzemu w budowaniu drogi ucieczki.
Lidia i Rafael mają obserwować wskazania biomózgu i spróbować mu "pomóc". 

Andrea dostała zakłócony, zniszczony sygnał hipernetowy. Automatyczny distress signal - na wysokich bandach hipernetu. Nie odbierze tego nikt inny. Sygnał pochodzi od tien Rudolfa Jankowskiego - innego maga Wydziału Wewnętrznego pod dowodzeniem Aleksandra Sowińskiego. Też przeżył, acz jego stan jest... zły. Nie wiadomo gdzie się znajduje; hipernet próbuje to jakoś ustalić i odbudować. Grunt, że przeżył. Ale... nie wiadomo o niczym więcej.

Lidia i Rafael mają ten sygnał zrekonstruować, oczyścić i zlokalizować źródło. 

Andrea dostała informację alarmową od Lidii po hipernecie. Oczywiście, jest niewyraźna. Poszła zobaczyć co tam się dzieje - jedzie tajemniczy, niezidentyfikowany obiekt prosto do sładu Żuczek. Wysłał sygnał detekcyjny magiczny, ale baza go skontrowała swoim sygnałem antydetekcyjnym. Zanim Andrea odpaliła maksymalną siłę ognia, Lidia wykryła, że ten obiekt ma sygnaturę KADEMu. Andrea pozwoliła im wjechać do składu (do zewnętrznego perymetru bazy).

Z samochodu wyszli Marian i Andżelika. Ku wielkiemu rozbawieniu Andrei, nie wiedzą, że są w bazie Świecy i kłócą się gdzie może ta baza być. Andrea spoważniała słysząc o Spustoszeniu w okolicy i magii krwi w okolicy. Kazała Pszczelakowi doprowadzić ich do niej. Andżelika wynegocjowała, że tylko ona pójdzie - boją się, że Świeca tu też jest Spustoszona. Obawa z wzajemnością ;-).
Andżelika przed obliczem Andrei przeszła do sprawy bezpośrednio - przekazała informacje od Silurii (Spustoszenie w Kompleksie Centralnym, jak wygląda sytuacja, że próbowali wbić się na EAM). Powiedziała też, że lokalny portal (ten tutaj) jest aktywowany. Andrea spoważniała - najpewniej Spustoszenie próbuje dostać się na EAM. To kiepska sprawa, mówiąc bardzo delikatnie. Andżelika zaproponowała Andrei pomoc - da jej Wyzwalacz Pryzmaturgiczny. 

Andrea wykombinowała plan. Niech Tadeusz Baran weźmie ze sobą element bazy pozwalający na antydetekcję. Niech pojedzie na lotnisko (jest jedno pod Glinem Żółtym, acz nie szczególnie wielkie), porwie samolot, wyładuje go ładunkami i quarkami, weźmie Wyzwalacz, wsadzi tam pilota i da mu rozkaz mentalny "rozbij się" i wyśle. Kombinacja Wyzwalacza i tego wszystkiego powinna destabilizować i zniszczyć portal. "Nic nie może tego przetrwać".
Wzmocnione to będzie tym, że przy portalu jest koło 10 magów, koło 100 ludzi i używają Magii Krwi.

Udało się. Zginęło w wybuchu 6 magów i 73 ludzi. Portal jest niemożliwy do naprawienia przez co najmniej miesiąc przez szalejące energie (a potem wymagać to będzie inwestycji). Efektywnie: nie ma on już znaczenia w tej kampanii.

Andrea zarządziła ewakuację. Marian i Andżelika dostali Lidię, Rafaela i biomózg. Reszta oddziału przeszła tunelami.

Dzień 3:

Wszyscy bezpiecznie dotarli na miejsce. Udało się zdobyć (uczciwie wynająć) ruiny zamku i się tam zainstalować. Odbudowa bazy, komunikacji, defensywy itp zajęła do końca tego dnia. Wielką radość sprawiło Andrei to, że siły Spustoszenia wdarły się do ich poprzedniej bazy i nie znalazły śladów Andrei ani magów Świecy...
Ewakuacja nastąpiła właściwie w ostatniej chwili.

# Progresja



# Streszczenie

Andrea powoli dostaje uprawnienia z Hipernetu; niestety Hipernet jest słaby. Pojawiło się niespodziewane wsparcie - Opel Astra KADEMu z Andżeliką i Marianem. Andrea po przeanalizowaniu danych z magitecha Wiktora Sowińskiego doszła do wniosku, że Overmind Spustoszenia jest powiązany z Mausami... i że Overmind szuka Renaty Maus. Andrea dowiedziała się też, że jeszcze jeden mag Wydziału przeżył, acz jest w złym stanie. Jako, że siły Spustoszenia chcą się wbić do EAM przez nieczynny portal w lesie, Andrea zniszczyła Wyzwalacz Pryzmaturgiczny, poświęciła ludzkiego pilota i zabiła kilku Spustoszonych magów i kilkudziesięciu ludzi, po czym ewakuowała się do Wtorku Śląskiego by odbudować swoją bazę w ogólnie bezpieczniejszym miejscu.

# Zasługi

* mag: Andrea Wilgacz, Lady Terminus która w zimny sposób zabija kilku * magów i kilkudziesięciu ludzi by ratować EAM przed Spustoszeniem.
* mag: Anna Kozak, w imieniu Andrei zdobywa zapasy finansowe i służy jako biznesswoman; też: zaproponowała miejsca w świecie ludzi do chowania się.
* mag: Lidia Weiner, główny operator biomózgu i pomocniczy lekarz, zwłaszcza w kwestii * magii mentalnej i Andrei.
* mag: Rafael Diakon, lekko sarkastyczny i złośliwy, acz skutecznie wspiera Andreę, dostarcza chłeptnika i mówi co wie na temat EAM.
* mag: Mieszko Bankierz, który mimo odstawienia na boczny tor przez Andreę niejednokrotnie udowodnił dobrą wolę i lojalność.
* mag: Alojzy Przylaz, górnik i 4x-letni kandydat na terminusa. Zbudował drogę ucieczki na życzenie Andrei. Dobrze pije z robotnikami.
* mag: Julian Pszczelak, katalista, który utrzymał morale w swoim skrzydle i w imieniu Andrei negocjował z Andżeliką.
* mag: Joachim Kopiec, ekspert od Szlachty, który próbował się przydać jako przynieś-podaj-pozamiataj. Nie dostał istotnej roli w niczym.
* mag: Kajetan Weiner, którego nikt ze Świecy ani z Drugiej Strony nie zna; może poruszać się po okolicy bezpiecznie (i to robi).
* mag: Tadeusz Baran, zakochany w Andrei i stymulowany; porwał samolot i zmusił pilota do zniszczenia portalu by ratować EAM. Zero wyrzutów.
* mag: Andżelika Leszczyńska, negocjowała ze Świecą (Andreą) i oddała Wyzwalacz Pryzmaturgiczny by uratować EAM przed Spustoszeniem.
* mag: Marian Łajdak, który został w odwodach na wypadek, gdyby Andrea była wroga KADEMowi.
* mag: Marian Agrest, który ostrzegł, że hipernet się przekształca i przesłał dane z pancerza Wiktora... po czym zamilkł.
* mag: Felicjan Weiner, przybył by przejąć kontrolę nad Portalem i połączyć go z EAM... i skończył poraniony i poparzony.
* mag: Rudolf Jankowski, inny * mag Wydziału Wewnętrznego, w sygnale SOS poinformował "wysokopoziomowców" że żyje i jest w fatalnej formie.

# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Piróg Górny
                    1. Obrzeża
                        1. Skład budowlany Żuczek, który... służył jako skład budowlany, dla odmiany - przez normalnych ludzi
                            1. Ukryta kwatera Świecy, która została pozbawiona sprzętu i została zdemolowana przez Spustoszenie po ewakuacji Świecy
                                1. Biostanowisko dowodzenia, z którego wymontowano biomózg
                                1. System detekcyjny, który potrafił nawet zakłócić działania Opla Astra Setona
                    1. Las Stu Kotów
                        1. Martwy Portal, którego próbował Felicjan dostosować do EAM; zniszczony przez pilota kamikaze z Wyzwalaczem
                1. Glin Żółty
                    1. Obrzeża
                        1. Lotnisko, z którego Tadeusz Baran porwał pilota małego samolotu
                1. Wtorek Śląski, miasteczko w kierunku na Czelimin
                    1. Ośrodek historyczny
                        1. Ruina zamku, gdzie zainstalowała się Andrea z ferajną
     
# Skrypt

# Czas

* Dni: 3
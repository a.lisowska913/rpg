---
layout: inwazja-konspekt
title:  "Wąż jako vicinius Pauliny"
campaign: rezydentka-krukowa
players: kić
gm: żółw
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [170331 - Kiepsko porwana Paulina (PT)](170331-kiepsko-porwana-paulina.html)

### Chronologiczna

* [170331 - Kiepsko porwana Paulina (PT)](170331-kiepsko-porwana-paulina.html)

## Kontekst ogólny sytuacji

## Punkt zerowy:

## Misja właściwa:

**Dzień 1:**

Paulina obudziła się w stolarni. Na zapleczu. W barłogu. Bała się, że jej nie zabiorą (nie przyjadą po nią). Obudził ją... zapach bulionu. Wstała i poszła do reszty. Widok ją... zaskoczył.

Wiaczesław sprawdza, czy jego AK-47 działa. Wąż gotuje śniadanie - wypiekła chlebek (wczoraj), przygotowuje bulion do popicia i robi "pasztet" z warzyw na kanapkę. Paulina, jak już pozbierała szczękę, spytała jaki mają plan.

* Wąż wchodzi na arenę jako vicinius. Nie mag. Weźmie wszystkie potrzebne narkotyki bojowe i wspomagania.
* Wiaczesław, ciężko uzbrojony, spróbuje się WBIĆ na arenę z zewnątrz.

Paulina spytała, czy Wiaczesław wie, jak działa ta arena. Może jest opcja rzucenia wyzwania? Jak często ów mag staje do walki by uzyskiwać ochy i achy? Wiaczesław powiedział, że przeglądał w swojej sieci kontaktów - nikt o tej arenie nie wie, czyli nie szukali nawet półprofesjonalnych gladiatorów. Po prostu szukali ofiar do pobicia. Z tego co Wiaczesław wie, nie mają nawet lekarza. Nie mają lekarza przy arenie!

* Dałoby się ją wkręcić w takim razie; jako lekarza na arenę - Wąż o Paulinie do Wiaczesława
* Zły pomysł, Wąż. To cywil. - Wiaczesław
* No i? Medyk. - Paulina

Wąż zaproponowała, że dostarczy jakieś trucizny Paulinie. Ta się aż wzdrygnęła, mimo, że Wąż zapewniła, że na NIĄ nie będą działały bo zrobi je w połączeniu z krwią Pauliny. 

* Pójdę jako lekarz; może uratuję życie kilku ludziom - Paulina
* Czekaj; masz na myśli... że oni mogą tak robić bo nie mają lekarza by ratować ludzi? - Wiaczesław
* Popatrz ekonomicznie; łatwiej jednego leczyć niż polować na kolejnych. Mniejsza szansa, że ktoś się zainteresuje - Paulina
* Rozumiem... widzę, do czego dążysz. - Wiaczesław - Ale nadal, śmierć za śmierć.
* Cóż... - Paulina

Paulina poznała jedną z trucizn Wąż. Ktoś jej KIEDYŚ wysłał fiolkę do analizy z ostrzeżeniem, że to trucizna. Ta powodowała nudności i gorączkę; ciężko było znaleźć antidotum. Nieletalna, acz cholernie upierdliwa.

* A nie moglibyśmy go... porwać? - Paulina, z nadzieją
* Porwania to nie moja specjalność... - westchnął Zajcew
* Wiesz... mogę się włamać, otruć i porwać - Wąż - Ale... kogo?
* Tego się możemy dowiedzieć na przykład będąc na arenie. Ja jako lekarz, Ty jako vicinius. - Paulina
* Jako viciniusa mnie stamtąd nie wypuszczą, wiesz o tym? - Wąż
* Chyba, że jesteś MOIM viciniusem - Paulina, z szerokim uśmiechem
* Lekarz i promotor areny. - Wąż, z szerokim uśmiechem
* To się może udać. Zrobię historyjkę jakiejś małej arenki, która podupadła, byś istniała - Wiaczesław
* Czy umiesz zmieniać swój kształt? - Paulina do Wąż
* Mogę się przekonstruować, to nie byłby pierwszy raz - Wąż, z uśmiechem

Oki, mają więc nowy, makeshiftowy plan. Paulina idzie na arenę jako lekarz i jako promotor walk a Wąż jako jej vicinius. Wiaczesław zrobił Paulinie szybkie przeszkolenie; jak należy się zachowywać, jak działają areny, jak POWINNO to wyglądać... że na arenie się nie zabija. To za drogie! Same przyczyny ekonomiczne!

* Mam pewne... narkotyki, które Ci pomogą. Testowane na magach. Mają pewne, słabe efekty uboczne. Jako lekarz, pomożesz sobie - Wąż, pomocnie
* ...poproszę... - Paulina, myśląc o ludziach, którym chce pomóc.

Paulina usłyszała, ku swemu zaskoczeniu, że te narkotyki są oparte o kilka magicznych roślin i ich zadaniem jest pomóc w skupieniu się. To takie... stymulatory tunelowe. Pomoże Paulinie się "nie martwić", "nie wygadać" - skupić się na tym, co jest głównym kluczem. A w tym wypadku tym kluczem jest dostać się jako lekarz na arenę. Przytępia uczucia, pomaga w skupieniu. Efekt uboczny? Przy źle dobranej dawce nadmierna nerwowość po zejściu narkotyku.

Dobrze, Paulina poprosiła Wiaczesława, by ten zaczął rozpuszczać historię o Paulinie jako o lekarce na małej arenie, która upadła. Takiej, która ma fajnego viciniusa i chociaż fajnie byłoby go użyć na arenie, nie ma parcia się zatrudnić. Wiaczesław obiecał, że to zrobi; dodał też, że kilka pierwszych bitew Paulina robi zawsze pro bono, żeby się rozeznać. Jeśli to takie małe coś, ta tutejsza arena, to oni mogą być "sprytni" - zatrudnią lekarkę i powiedzą, że jednak nie, jak już będzie po etapie pro bono.

Paulina się zgodziła. Podoba jej się ten pomysł.

* To mi daje też dojście do właściciela areny. Negocjacje. - Paulina, z nadzieją
* Powiesz kto to, porwę. - Wąż

Paulina dopytała Wiaczesława o stawki areny. Wiaczesław powiedział jej jak to wygląda.

* Tęsknisz za areną, prawda? - Wąż do Wiaczesława, głaszcząc go delikatnie po ramieniu
* Trochę tak, ale już nie będę przywiązany do jednego miejsca. Wygrałem w Wejan... - Wiaczesław, ze smutkiem
* Zniszczyłeś Wejan. - Wąż, ze współczuciem
* Tylko tak dało się wygrać... - Wiaczesław, z nie mniejszym smutkiem
* Czas zniszczyć kolejną - Paulina, nie do końca wiedząc o co chodziło z Wejan
* Masz rację. I każdą następną, która jest tak nieprofesjonalna. - Wiaczesław
* Wiaczesław, niszczyciel aren. - Wąż
* A, spełzaj - Wiaczesław

Czyli plan jest ustalony.

* To mnie odwieź. Arena areną, ale pacjenci nie czekają. - Paulina do Wiaczesława
* Ma sens. - Wiaczesław
* Ej, z tego żyję... pomijając już wszystko inne. - Paulina, z uśmiechem
* Dobra. Odwiozę Cię. Przywiozę, jak coś będzie jasne. - Wiaczesław
* Tylko tym razem zadzwoń zamiast się włamywać... - Paulina, z nadzieją
* On? On się włamywał? - Wąż, zaskoczona
* Mhm... - Paulina
* Nie wyszło? - Wąż
* Dotarł do mojej sypialni... - Paulina

Orientując się, że w sumie można się do niej wkraść, Paulina jednak zwróciła się do Wąż i poprosiła o jakieś trucizny usypiające. Coś, dzięki czemu może spać spokojnie, gdyby ktoś próbował się zakraść Paulinie do sypialni. Wąż z radością podzieliła się zapasami. Dała Paulinie kilka fiolek. Kątem oka Paulina zobaczyła, że Wiaczesław przekazał Wąż trochę kasy; w jakim stopniu jest to skorelowane z zakupem Pauliny, nie wiadomo.

Wiaczesław odwiózł Paulinę, ścigany szkaradnym rechotem Węża. Paulina wie, że potem mu Wąż będzie docinać... i dobrze mu tak.

* Paulino, jedna rzecz. - Wiaczesław, odwożąc Paulinę
* Mmm? - Paulina
* Dostałaś coś od Wąż. Teraz Ty musisz jej coś dać. - Wiaczesław, poważnie
* Wiem. - Paulina
* Proponuję coś z roślin. Lub leków. - Wiaczesław, próbując być pomocnym

Paulina poprosiła Wiaczesława, by ten jej powiedział, czy ktoś kiedyś próbował pomóc Wąż ze wzorem. Wiaczesław się zasępił. Sporo dobrych, drogich lekarzy. Wąż chce mieć wzór niestabilny, by móc adaptować. By móc... ewoluować do perfekcji. Póki nie jest perfekcyjna, chce być otwarta. Ale jednocześnie przez to każdego dnia jest trochę inna. Jest pewien "core" Wąż. Jedyny stabilny fragment. Wąż JEST dysfunkcją. I ta osoba sama sobie to zrobiła...

* Wąż się boi, że straci moc magiczną, wiesz? - Wiaczesław smutno - To jedyny sposób, dla którego jej moc nie spadła.
* Ten wzór taki a nie inny? - Paulina
* Tak. Ten trik zadziałał; osoba raniona przez Zaćmienie odrzuciła wszystko POZA magią. I tak powstała Wąż. - Wiaczesław - Z tego co rozumiem.

Paulina wróciła do przyjmowania pacjentów a Wiaczesław wrócił do Wąż. To jest długi dzień...

Paulina poprosiła też o jedną rzecz - Wąż musi być gdzieś blisko. Jeśli NIE będzie, to ewentualne obserwujące Paulinę czujki mogą się zdziwić, że nie ma jej "viciniusa" w pobliżu. Czyli Wąż musi mieszkać z Pauliną...

Mogło być gorzej. Niewiele, ale mogło.

**Dzień 2:**

Rano, pod gabinet Pauliny podjechał foodtruck. A za kierownicą Terror Wąż.

* Przyjechałam z Tobą zamieszkać. - Wąż do osłupiałej Pauliny - Przywiozłam śniadanie. Pizza na szpinaku.
* ...nie uważasz, że foodtruck trochę się kłóci z naszym pomysłem? - Paulina, podłamana
* Tam mam wszystko. Mogę zaparkować z tyłu. - Wąż, ze spokojem
* ...zaparkuj z tyłu... - Paulina

Paulina, wiedząc, że Wąż przyjedzie, zrobiła dla niej budyń z kaszy na mleku owsianym. Wąż to zjadła i doceniła starania Pauliny, acz... niezbyt jej wynik ;-). Paulina powiedziała Wąż, że pacjenci nie mogą jej zobaczyć; Wąż potwierdziła. Nie chce, by pacjenci ją widzieli. Ale Wąż zapytała gdzie może w okolicy odbierać i wysyłać narkotyki i inne takie. Bo zakłada, że nie pod ten adres.

* ZDECYDOWANIE nie pod ten adres! - Paulina, załamana
* Unikaj też najbliższej okolicy... ale nie wiem gdzie tu się to odbywa... - Paulina, szukając
* Mogę spytać na DarkRoad gdzie w okolicach Krukowa Czarnego takie rzeczy się robi - Wąż - Też, gdzie mogę zdobyć jakieś SENSOWNE warzywa w okolicy?
* Są lokalni rolnicy - Paulina, próbując być pomocna - Daj mi chwilę...

Paulina poszukała informacji o najbliższych wysokojakościowych produktach spożywczych roślinnych (7v5->S) i powiedziała Wąż gdzie kupić lokalne marchewki i inne warzywa. Wąż się ucieszyła; Wąż może iść sama wszystko kupić, ale... Paulina, wiedząc, że tego mag viciniusowi by nie zlecił, obiecała załatwić sama. Ustaliły listę zakupów; Paulina przygotowuje tematy zakupowe w przerwie.

Tymczasem Wąż dowiaduje się, gdzie może handlować.

Gdy wieczorem fala pacjentów minęła a Wąż już jest na miejscu, w foodtrucku przygotowuje kolejny zestaw eliksirów, Paulina poprosiła Wąż jak eliksiry się skończyły. Też: dostała świetny obiad. A Paulina dostarczyła składniki. Wzajemność uzupełniona ;-).

* Chciałam Ci się jakoś odwdzięczyć za te usypiacze. Tak sobie pomyślałam... potrzebujesz magii, ale magia umie Cię zakłócić, prawda? - Paulina - Nie chcesz czegoś, co Ci ją uzdatni?
* Bardzo ciekawy pomysł. Nie pomyślałam o tym - zaskoczona Wąż.
* Będzie miało swoje ograniczenia, ale będzie się dopasowywać do Ciebie. - Paulina
* ..? - Wąż czeka

Paulina wyjaśniła, że jest katalistką wzoru i alchemikiem. Można to połączyć; zwłaszcza, że Wąż jest tu z Pauliną. Da się na bazie Wąż zbudować taki uzdatniacz; buteleczka, do której Wąż wpuszcza kroplę swojej krwi - jej AKTUALNY Wzór. I do tego jeszcze energia magiczna, która ma się dostosować do Wzoru Wąż. To zmniejszy straty magiczne i usprawni "smak" energii.

Wąż się ten pomysł zdecydowanie spodobał.

Paulina konstruowała to cały wieczór (10->12: bardzo trudny arcymistrz). Jest to skuteczny artefakt; posłuży Wąż dobrze. Wąż doceniła. Zdecydowanie doceniła.

W nocy, około 3 rano, Paulina śpi. Wąż się PODKRADA. Wpierw włamuje się do pokoju Pauliny (3v5->F) prawie bezszelestnie, następnie podkrada się do samej Pauliny (3v5->F). Paulina otworzyła oczy widząc Wąż powoli zbliżającą się do niej swoim nieludzkim obliczem.

Paulina natychmiast się obudziła. Reakcja widząc przerażającą Wąż (3v3->F). Podskoczyła, ale nie pisnęła. Spytała, czy coś się stało, próbując ukryć scare jump...

* Nie, chciałam tylko sprawdzić, jak łatwo się do Ciebie podkraść - Wąż
* Łatwo. Mogłaś zapytać... - Paulina, nie ukrywając irytacji

Wąż się odwróciła i wyszła z pokoju, zaznaczając, że Paulina nie ma po co zamykać drzwi. Wąż ją obroni. Paulina powiedziała Wąż, żeby ta tak nie robiła - ona szybko nie zaśnie. Wąż zaproponowała herbatkę na uspokojenie. Paulina podziękowała; nie jest zainteresowana. Wąż wyszła. A Paulina lekko zmierzła... poszła zrobić jakiś porządek gdzieś. Gdziekolwiek. O 3 rano.

**Dzień 3:**

Paulina przyjmuje normalnie pacjentów. Wąż czeka gdzieś poza widokiem. Paulina ma gdzieś GDZIE Wąż jest; byle nie pokazywała się pacjentom. A Wąż jest w kuchni Pauliny i gotuje.

Paulina pomaga jakiemuś młodziakowi na ból brzucha i nagle wyczuła ping "SOS, lekarz" magiczny. Z niedaleka jej domu. Zbliża się szybko. 2 km stąd i się zbliża pingując co 10 sekund.

Paulina odesłała ping, lokalizując na sobie. Skupiła się na tym by pomóc chłopaczkowi by ten mógł szybko wyjść. (6v3->S). Młody szybko wyszedł a Paulina poczekała, przygotowując adekwatne środki. Wjazd otwarty i Paulina zajrzała do Wąż.

* Nie czułaś nic? - Paulina
* Nie - zdziwiona Wąż
* Ping magiczny; ktoś wzywa lekarza - Paulina - Trzymaj się na razie z tyłu
* Oczywiście, nie będę Ci przeszkadzać - Wąż
* Jak długo nie zdzielą mnie przez łeb lub Cię nie wezwę, nic nie rób - Paulina
* ... - Wąż wzruszyła ramionami i wróciła do gotowania

Paulina bardzo nie chce niepotrzebnej eskalacji a Wąż, cóż... to Wąż. Eskalacja prawie pewna.

Pod dom Pauliny podjechał mizernie wyglądający, ranny (lekko poszarpany przez viciniusa) urzędnik. Tak wygląda jak urzędnik.

* Pomocy! Czy pani jest lekarzem, tien... - zrozpaczony Onufry
* Po prostu Paulina Tarczyńska - Paulina ze spokojem

Gdy Paulina próbowała go zaprowadzić do gabinetu, on ją zatrzymał.

* Nie dla mnie. Moja żona i dziecko. Jej wzór się zdestabilizował, zmieniła się w potwora, porwała dziecko i uciekła - Onufry, zrozpaczony
* To nie medyka pan tu potrzebuje - Paulina, zdziwiona
* A kogo? Zabiją ją. Mogła mnie zabić, nie zrobiła tego! - Onufry - Jest pani lekarzem, ma pani coś na... wzór

Paulina zbiera swoje rzeczy. I myśli, wziąć Wąż czy nie... poszła do Wąż i streściła jej historię jaką usłyszała. Paulina chce rozpoznać sytuację, pomóc jeśli się da, jeśli się NIE da (za duży problem) to wezwać terminusa. Paulina poprosiła Wąż, by ta nie używała magii; to może być próba "ludzi z areny". Wąż uważnie się zastanowiła i pokiwała głową. Wąż nie potrzebuje magii.

Paulina zgarnęła rzeczy potrzebne do naprawy Wzoru, Wąż okutała się w strój zakrywający i welon, po czym wsiedli do samochodu Pauliny z nieszczęsnym Onufrym i pojechali na miejsce zdarzenia... i dała krótki sygnał Wiaczesławowi, że ktoś może chcieć odwiedzić chatę Pauliny.

Jadą do Lesiwa. Auto Pauliny to NIE jest motor Wiaczesława.

Onufry, pytany, zaczął tłumaczyć nieco mętnie: był z żoną i dzieckiem na plaży (przy jeziorze). Nagle coś się stało, krzyknęła i jej magia się zdestabilizowała. Poczuł wyładowanie magiczne, ale od niej. Chwilę przedtem czarowała sobie z dzieckiem. Syn jest 11-latkiem, katalista + biomanta. Ona jest biomantką i mentalistką. I tak jakby... zarezonowało wszystko. I jej Wzór się rozsypał. Chyba.

Paulina może prowadzi. Może skupia się na drodze. Ale nie trzyma jej się to kupy. Wyczuła, że delikwent albo łże albo nieświadomie łże. Czegoś Paulinie nie powiedział.

* Chcesz ryzykować życie żony i dziecka, by Paulina się czegoś nie dowiedziała? - Wąż, z pogardą - Nie są dla Ciebie wiele warci.
* To nie tak! Oni... - Onufry, wyraźnie zdesperowany
* Chcesz wziąć na siebie każdą kroplę krwi Pauliny, która próbuje ich ratować. - Wąż, śmiertelnie poważna - I ich życia.
* Czarodzieju. <pauza>. Bardzo dobrze zastanów się nad odpowiedzią, bo ona myśli inaczej niż tien i ja i nawet ja nie będę w stanie jej powstrzymać - Paulina, poważniej
* Nielegalne magiczne mody! - Onufry - Implanty artefaktyczne. Wzmacniacze mocy. Eksperymentalne. Po Zaćmieniu. I one zarezonowały. Chyba. To nie jej moc, to one. Ma amplifikatory biomancji i mentalki. One coś... z nią.
* A dziecko? - Paulina
* Nie wiem. Nie wiem, czemu je porwała. - Onufry
* Czy ono też ma? - Paulina, poważnie
* Nie, tylko matka. Ja mam słabą moc, ona... mniejszą. Szczepan ma największą. - Onufry, bezładnie
* Czy ona jest jeszcze magiem? - Wąż, praktycznie leniwie
* Tak... - Onufry, speszony
* Postawisz na to życie swojego syna? - Paulina, wchodząc Wąż w słowo
* Ja... przed modami... bardzo starałem się nie sprawdzać. Jesteśmy tien... - Onufry, podłamany
* Cóż. Niewiedza nie ułatwi leczenia... - Paulina, wzdychając
* Masz próbkę jej krwi? Innych płynów? - Wąż, z ciekawością
* Nie, oczywiście, że nie! - wstrząśnięty Onufry
* Możemy podjechać do niego do domu i spróbować coś zebrać - Wąż do Pauliny - W poprzednim życiu byłam alchemikiem
* Mniej istotne. Musimy ją znaleźć. - Paulina - Bardziej istotne jest znalezienie dziecka i jej.
* Uratuj ją, proszę. Proszę. - Onufry, do Pauliny - Nie widzę bez niej świata.
* Czy masz coś, co do niej należało? - Paulina do Onufrego
* Tak - Onufry, szybko

Porwała dziecko. Gdzie mogła pójść... Wąż zatopiła się w swoich myślach, Paulina prowadzi, Onufry się martwi.

Paulina zdecydowała się na sprawdzenie na szybko, czy w pobliżu nie ma jakichś nietypowych Węzłów. Czegoś, co mogło wywołać taką reakcję. (3v3->F). Tak, jest w okolicy, w lesie stare drzewo; jest to drzewo pryzmatyczne i nie da się go pozbyć (zniszczenie drzewa sprawi, że sie pojawi kiedy indziej). To drzewo ma dziwny wpływ na energię magiczną. Jeżeli te mody są eksperymentalne... mogło zareagować. I najpewniej wynikowa istota będzie dążyć do drzewa.

Niestety, na 99% obecność Katarzyny i jej transformacja złamała Maskaradę wśród ludzi.

Paulina facepalmowała. Oni są tien. Dziecko jest na hipernecie. Kazała ojcu spytać syna, gdzie jest... po chwili koncentracji, ojciec powiedział - są dalej w lasku. Nie stała się mu krzywda. Mama... ona próbuje... atakować jakieś drzewo. Próbuje je zniszczyć. I nie, nie zachowuje się jak mama. Nie jest sobą. Boli ją - a Onufry nie ma z nią kontaktu hipernetowego.

Paulina poszła w kierunku na wiadome jej drzewo. Onufry i Wąż z nią. (3v1->S); Paulina zauważyła, że Wąż idzie lekko niepewnie. Ma zdestabilizowany Wzór; a drzewo ma własności jakie ma. Paulina się bardzo zmartwiła; poprosiła Wąż, by ta została z tyłu. To pryzmatyczne drzewo. Wąż zauważyła, że Paulina potrzebuje potwora by walczyć z potworem; Katarzyny nie ma. Paulina powiedziała, że Wąż nie ma...

Paulina poprosiła, by Onufry wezwał do siebie Szczepana. Z dala od mamy. Onufry się skupił i wysłał sygnał hipernetowy. Kilka minut później Onufry powiedział, że Szczepan tu idzie. Wąż ruszyła zgarnąć dzieciaka i na obiekcje Pauliny powiedziała, że jest wegetarianką. Nie zje ani dzieciaka ani Pauliny... i kazała Onufremu ostrzec Szczepana, że idzie po niego przyjaciółka.

Nie. Paulina się nie zgadza. (5v2->S) Głęboka prośba do Wąż, żeby tego nie robiła. To niebezpieczne dla Wąż. Wąż z trudem dała się przekonać, ale nie odchodzi daleko... i całą trójką poszli w kierunku na dzieciaka. Udało się dotrzeć do Szczepana, zanim wynikowy stwór z Katarzyny się zorientował... Szczepan rzucił się do taty z płaczem.

* Dobrze, jak ją wyciągniemy stamtąd. Pomysły? - Wąż
* Może... zawołamy? - Paulina, z nadzieją
* Działaj. - Wąż

To drzewo to coś, co zakłóca Wzór. Czyli Paulina jest w stanie wysłać sygnał "leczenia wzoru"; coś, co może przyciągnąć tu potwora którym stała się Katarzyna. (6v5->S); Paulina zrobiła kupkę Quarków, by Katarzyna mogła wziąć i się ustabilizować. I faktycznie, po chwili... z lasu wybiegła niekształtna istota o cechach humanoidalnych. Wydała żałosny dźwięk widząc swoją rodzinę, odwróciła się by się cofnąć, po czym zobaczyła Quarki. Rzuciła się pożerać kryształy. Jest zaabsorbowana, ale nie całkowicie.

* Ja trzymam, Ty czarujesz? - Wąż do Pauliny, serio - Biorę moją krew na siebie.
* Niezły plan, ale najpierw spróbuję pogadać... - Paulina, z nadzieją
* Nie ma nadziei. Sama to widzisz. - Wąż, zirytowana - Musisz użyć magii.
* Tak, ale tylko za pewną akceptacją. - Paulina, zirytowana
* Ty jesteś lekarzem. Lekarze. Jak zawsze. - Wąż, naprawdę zirytowana - Czasem po prostu się nie da.
* Ale zawsze warto spróbować - Paulina, odwracając się od Wąż.

Paulina wysunęła się kilka kroków przed innych i zwróciła się do potwora. Ten nie reaguje na swoje imię (Katarzyna). Niestety; wszystko wskazuje, że Katarzyna nie rozumie lub nie słyszy Pauliny. Jednak gdy chłopiec zakwilił "mamo", Katarzyna spojrzała na niego i wzrokiem zmierzyła teren szukając zagrożenia. Nie widząc, wróciła do Quark. To samo, gdy Onufry się odezwał. Ich rozpoznaje. Niekoniecznie rozpoznaje innych i niekoniecznie jest "świadoma".

* W tej chwili problem jest taki, że Twoja mama nie do końca rozumie świat dookoła - Paulina, do Szczepana - Jest chora.
* Innymi słowy, jest potworem, tak jak ja. Ale głupszym. - Wąż, pomocna jak zawsze
* WĄŻ! - Paulina, wściekła - Przestań wpadać mi w słowo.
* ... - Wąż zignorowała Paulinę jakby nigdy nic nie powiedziała
* Mamo... wróć... - Szczepan, z płaczem

Vicinius przerwał asymilację i zaczął zbliżać się do chłopca. Wąż obserwuje, ale nic nie robi. Chłopiec z polecenia Pauliny zaczął nucić ulubioną kołysankę. Vicinius niezgrabnie przytulił chłopca, do niego przytulił się Onufry. I facet też zaczął śpiewać kołysankę. Niestety, vicinius - jakkolwiek ma jakieś cechy ludzkie - nie zaczął śpiewać ani spać.

(2v2->S). Paulina wyczuła płynącą energię w ciele Katarzyny. To te mody. One ciągle utrzymują niewłaściwą, Skażoną formę. Są w ciele, dość głęboko, ale nie w sposób letalny. Wąż może je wydrzeć z ciała nieszczęsnej Katarzyny...

Paulina przesłała po hipernecie do ojca sygnał "trzymaj mocno syna i niech nic nie widzi" i kazała Wąż zaatakować Katarzynę i wyrwać jej implanty. Wąż się nie zawahała ani sekundy. Paulina odwróciła uwagę bestii a Wąż zaatakowała. (S!). Wąż rzuciła się, zdestabilizowała Katarzynę, pyskiem przytrzymała szyję, nacięła skórę i wyrwała implanty. Jednocześnie. Bardzo brutalnie. Bardzo skutecznie. Katarzyna zawyła w agonii a Wąż ją jeszcze odepchnęła i zrobiła skok. Onufry FULL TERROR.

Usunięcie implantów jej nie pomogło, ale tkanka magiczna nie ma już podtrzymania. A Wzór już nie "skacze". Pływa, ale nie "skacze".

Paulina skupiła się na naprawie Wzoru. I ustabilizowanie Wzoru do jej Wzoru... używając kropli krwi syna, materiałów z gabinetu, Quarków itp. (-1 surowiec) (12v11->R). Stała się viciniusem jako efekt uboczny; ale Paulinie udało się znaleźć stabilizację Wzoru... Ranna, półnaga kobieta opadła na ziemię.

* Złamanie Maskarady - Ferrus (mechaniczny terminus)
* No w samą porę. - Wąż, przewracając oczami
* Jak się chcesz wytłumaczyć z tego wszystkiego? - Ferrus do Pauliny
* A, coś mi nie wyszło i musiała posprzątać. Eksperymenty na ludziach, łamanie Maskarady i inne takie. - Wąż, wchodząc Paulinie w słowo

Paulina powiedziała Wąż cicho, by ta siedziała cicho i nie psuła jej opinii. Wąż uderzyła atakiem hipnotycznym (3v3->F); Paulina odparła atak i zmusiła Wąż do zachowania spokoju. Paulina MUSI rozwiązać problem tak, by być sponsorem...

Konstruminus obserwuje i nic nie mówi. Jak to konstruminus.

* Najpierw pacjent. - Paulina do konstruminusa, lecząc z pełnych sił.
* ... - Wąż, bardzo, bardzo zirytowana
* ... - konstruminus czeka

Onufry jest blady jak śmierć, Szczepan nic nie rozumie.

* Czy to ona jest zakłóceniem Maskarady? - konstruminus, do Pauliny
* Ta czarodziejka była chora. - Paulina do konstruminusa - Tak, MOGŁO dojść do złamania Maskarady; Twoim zadaniem jest to wyczyścić
* Bierzesz odpowiedzialność za tą czarodziejkę od teraz do momentu wypuszczenia? - konstruminus do Pauliny
* Tak. - Paulina, po prostu
* Wracam do korekcji Maskarady - konstruminus, opuszczając Paulinę i innych

...

# Progresja

* Paulina Tarczyńska: dostała pomniejszą sławę jako lekarka na małej arenie, promotorka viciniusów i lekarka. Całkowicie nieprawdziwa fama, acz plotka poszła.
* Paulina Tarczyńska: jest tymczasowo odpowiedzialna za tien Katarzynę Mirłik aż ta wyzdrowieje
* Terror Wąż: dostał artefakt uzdatniający energię magiczną by ją lepiej wchłaniał (i smakowała lepiej), co redukuje jego koszty.

# Streszczenie

Pojawił się plan wejścia na tajemniczą arenę. Paulina lekarz i promotor viciniusa (Wąż). Do tego celu Wąż udaje viciniusa i zamieszkała na moment z Pauliną, ku utrapieniu tej drugiej. Sielankę przerwał Onufry, którego żona (mag -> Zaćmienie -> człowiek) zmieniła się w potwora; Paulinie z pomocą Wąż udało się naprawić jej Wzór i przekształcić w viciniusa; choć po drodze nie było łatwo. Wąż nadal udaje viciniusa Pauliny. Kiepsko jej to idzie.

# Zasługi

* mag: Paulina Tarczyńska, z którą zamieszkała Wąż. Uratowała człowieka, któremu rozsypał się Wzór (i tak na serio, całą jej rodzinę).
* mag: Wiaczesław Zajcew, przyjaciel Wąż, który rozpuścił plotki o Paulinie szukającej areny gdzie może promować swojego viciniusa.
* mag: Terror Wąż, kucharka z powołania, psychopatka z zamiłowania, więcej straszy niż faktycznie krzywdzi. Pragmatyczna, bezwzględna, ale próbuje pomóc po swojemu.
* mag: Onufry Mirłik, by być z żoną (człowiekiem) i dzieckiem zaryzykował karierę w Świecy; destrukcja + puryfikacja, niezbyt silny mag. Urzędnik.
* vic: Katarzyna Mirłik, z biowspomaganymi implantami zwiększającymi jej moc (bio + mental). Przekształcona w viciniusa, gdy implanty zawiodły.
* mag: Szczepan Mirłik, młody (11 lat) chłopak, który kocha swoich rodziców; biomanta + katalista. Najsilniejszy mag w rodzinie.
* vic: Ferrus Mucro, konstruminus próbujący utrzymać i naprawić Maskaradę po wydarzeniach z przekształceniem Katarzyny Mirłik w potwora 

# Lokalizacje

1. Świat
    1. Primus
        1. Mazowsze
            1. Powiat Pustulski
                1. Krukowo Czarne
                    1. Gabinet Pauliny, gdzie na poddaszu mieszka z Pauliną Wąż (ku utrapieniu Pauliny)
            1. Powiat Czaplaski
                1. Paszczęka
                    1. Południe
                        1. Stolarnia Sitek, nadal baza Wiaczesława i Terror Wąż. 
            1. Powiat Ciemnowężny
                1. Skalna Grota
                    1. Stadnina Koni, gdzie odbywa się tajemnicza arena na którą chce się wkręcić Paulina
                1. Lesiwo, niewielka mieścina "wypoczynkowa"
                    1. Leśne Jezioro, ma plażę. I niedaleki las. A w tym lesie coś... dziwnego.
                        1. Wieczybór, lasek, gdzie znajduje się odradzające się Pryzmatyczne Drzewo, które dziwnie wpływa na magię
# Czas

* Dni: 3

# Narzędzia MG

## Cel misji

* Pokazanie różnic w podejściu Wiaczesława i Węża a podejściu jakiegokolwiek innego maga; też pokazania, że Wiaczesław i Wąż są... "normalni", mają marzenia itp. Innymi słowy, misja wprowadzająca Wiaczesława i Węża. Też jako znajomych Pauliny.
* Okazja do odpowiedzenia na pytanie: czy Wiaczesław i Wąż zabiją tych magów, czy nie?
* Okazja do zadania sobie pytania: Kim jest Wąż? A zwłaszcza dla Pauliny?

## Po czym poznam sukces

* Zrozumiemy mniej więcej jak na sesjach można Wiaczesława i Węża używać; Paulinie zbudują się JAKIEŚ kontakty
* Paulina będzie miała decyzję do podjęcia i niekoniecznie prosty konflikt
* Paulina ma JAKĄŚ relację z Wąż

## Wynik z perspektywy celu

* Sukces
* Porażka; TODO
* Sukces

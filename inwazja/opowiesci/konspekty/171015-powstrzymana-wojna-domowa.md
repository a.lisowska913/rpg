---
layout: inwazja-konspekt
title:  "Powstrzymana wojna domowa"
campaign: wizja-dukata
players: kić
gm: żółw
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [171010 - Jasny sygnał Tamary (PT, DA, KG)](171010-jasny-sygnal-tamary.html)

### Chronologiczna

* [171010 - Jasny sygnał Tamary (PT, DA, KG)](171010-jasny-sygnal-tamary.html)

## Kontekst ogólny sytuacji

### Siły główne



### Stan aktualny



### Istotne miejsca

* Mżawiór, gdzie znajduje się Portalisko Pustulskie
* Rogowiec, gdzie znajduje się hodowla Myszeczki
* Męczymordy, gdzie znajduje się magitrownia

## Punkt zerowy:

* Ż: Jakie złowrogie FAKTY znalazła Maria, które nie zostały unicestwione odpowiednio dobrze przez Sądecznego - w Senesgradzie?
* K: Sądeczny kręcił się w okolicy Muzeum Pary i coś przywołał. Chciał to zmazać, ale nie udało mu się - Efemeryda przywróciła.
* Ż: Co się stało z Areną? Po tym jak Wąż i Wiaczesław, Paulina...
* K: To miejsce zostało zniszczone. Paulina dążyła, by magowie przeżyli. Więc przeżyli. Ci bez dużej krzywdy ludziom rozpędzeni, inni ukarani przez Wiaczesława. Aneta została wyleczona przez Paulinę i rozpędzona przez Wiaczesława.

Kiedyś, Robert Sądeczny próbował znaleźć to, czego szukali Weinerowie w Muzeum Pary. Jako, że to było śmiertelnie niebezpieczne, użył magów Pentacyklu - Anety Rukolas (katalistki) i Fryderyka Grzybba (mentalisty i iluzjonisty). Nie wszystko poszło tak, jak powinno - ale udało mu się opanować sprawę.

* Czy Kaja dowie się, co spotkało Anetę i jak niebezpieczny jest Sądeczny?
* Czego naprawdę szukał Sądeczny w ramach Muzeum Pary?
* Czy Paulina zdąży zatrzymać Wiaczesława, zanim ten uderzy w siły Tamary?

## Misja właściwa:

**Wcześniej**

* Zapłacę Ci za każdego rozwalonego... golema tej cholernej terminuski Świecy, Wiaczesław. Za każdego. - Sądeczny, z niedowierzaniem
* Dobra. Nie ma sprawy. Bonus, jak pogardliwie? - Wiaczesław, z szerokiem uśmiechem
* Zapłacę. - Sądeczny, kręcąc głową - Bo tak się nie da pracować...

**Dzień 1**:

Paulina skończyła przyjmować pacjentów. Miała nasilenie i wie, że to przez te sprawy w magitrowni... lekkie Skażenie się wylewało a to zawsze wpływa na ludzi. Przechlapane, ogólnie... nie ucieszyła ją też wiadomość od Draceny, że Tamara tu jest. A Paulina nie chciała jej tu widzieć. No way. Gdyby nie Dracena, tych przypadków chorobowych byłoby więcej - ale przez jej działania było lepiej. Więc Paulina mówiła Dracenie co i jak a Dracena skupiała się na naprawie sieci dystrybucyjnej.

Maria skontaktowała się z Pauliną i powiedziała co następuje: w Senesgradzie ostatnio znowu efemeryda była gorąca; jakiś lokalny mag to wyczyścił. Nie wie kto i jak, ale wie, że ktoś tym się zajmuje. Skupiała się na Sądecznym i wyszło jej coś dziwnego - zwykle dezinformacje Sądecznego były wielopoziomowe, różni ludzie pamiętają różne rzeczy. Tym razem ludzie pamiętali dwie odmienne sceny, nawet ludzie, którzy nie mieli prawa tego widzieć - to wyraźnie nie działanie Sądecznego, który jest bardziej subtelny. Kilka miesięcy przed sprawą z Katią Sądeczny zaprowadził dwie osoby ze sobą do Muzeum Pary. Gdy wrócił, wrócił z dwoma osobami. Ale ludzie pamiętają to właśnie inaczej - część osób pamięta, że wrócił z tymi samymi. Część osób pamięta, że wrócił z tą samą dziewczyną ale innym facetem.

Maria zidentyfikowała nazwiska. Osoby, z którymi wszedł Sądeczny nazywały się: Aneta Rukolas i Fryderyk Grzybb. A osoby z którymi wyszedł to Aneta Rukolas i Andrzej Farnolis. Co dla Marii ciekawe, ludzie potrafią powiedzieć jak się te osoby nazywały - nawet, jeśli nigdy ich nie spotkały. Maria nic bardziej zrobić i dowiedzieć się nie może, ale ma nadzieję, że to coś Paulinie pomoże. Paulina podziękowała.

Dracena zadzwoniła do Pauliny. Wysłała jej fotkę - a tam zdjęcie pojedynczego pancerza Legionu Tamary. Zbezczeszczony, pokonany, zmieniony w strach na wróble i namalowano na nim sprayem wielkiego penisa. A na plecach "Tamara <3 dla Ciebie". I wyszedł na nią członek Legionu i chciał ją przesłuchać. Dracena powiedziała, że NIE jest pod jurysdykcją Świecy i Legion NIE ma prawa jej zatrzymać. Paulina widząc eskalację zadzwoniła do Tamary i poprosiła o odwołanie z Draceny Legionu. Tamara się zdziwiła - po chwili zauważyła, że incydent ma miejsce. Powiedziała Paulinie, że Dracena może odejść, jeśli Paulina da słowo, że przyjdzie. Paulina powiedziała, że Tamara musi uwzględnić specyfikę lokalną, specyfikę Draceny - niech to zrobi, bo przecież ona była w magitrowni cały czas i mają świadków... 

Tamara się zgodziła. Powiedziała jedynie, że ma nadzieję, że nie będzie to wszystko propagowało dalej. Sprawdzi wpierw dane Legionu itp, jak nie trzeba - nie będzie kłopotać Draceny. Dracena tym czasem "cała Świeca... uciekł z podkulonym ogonem" i prychnęła. Paulina zauważyła, że dostał takie polecenie. Dracena powiedziała, że po wyniku wyglądało to na zabawkę z dzieckiem. Jeśli TO jest siła jaką ma Świeca na tym terenie... to nie mają żadnej. Dracena zachichotała - jak na strzelnicy. Paulina nacisnęła na Dracenę - dała słowo, niech nie wysyła zdjęcia dalej. Dracena niechętnie obiecała. Zależy jej na Paulinie...

Paulina w końcu dojechała do Dukata. 

Paulina powiedziała Dukatowi, że w ostatnim czasie coraz więcej się psuje w jego organizacji i Dukat musiałby coś z tym powoli zacząć robić. Powiedziała o potężnej akcji dezinformacyjnej dla rezydentów - gdyby nie wiedziała wcześniej, Paulina nie wiedziałaby na czym stoi. Dukat powiedział, że wie o tym - chodzi o zabezpieczenie terenu dla Organizacji. Paulina zauważyła, że przez to ściągnięto tu Tamarę. A jeden z jej Legionu został ośmieszająco zniszczony. Dukat zauważył, że on nie ma takich agentów na tamtym terenie. To nikt od niego. Dukat powiedział, że zażąda od Sądecznego by ten nie doprowadził do wojny ze Świecą. Ale Paulina musi pamiętać - Sądeczny nie może wyglądać na słabego. Jeśli Tamara zacznie robić niektóre ruchy... to on też będzie musiał.

Dukat powiedział Paulinie, że widzi współegzystencję ze Świecą, jeśli nie będą stali mu na drodze. Ale jest to mało prawdopodobne. Więc raczej będzie ciężko... Paulina powiedziała też, że na terenie jest Kajetan Weiner. Jej przyjazny terminus. Dukat zauważył, że Kajetan jest pod ochroną jego osobiście - ma z nim pozytywną historię. Paulina z ulgą wróciła do domu.

A tam - Dracena. Powiedziała że jeśli Paulina chce się skomunikować z Efemerydą Senesgradzką, może użyć... Oktawii. Oktawii, która jest subefemerydą i opętała magitrownię. Co? Dracena streściła wynik wczorajszego dnia - jest młoda, chce być piosenkarką i chce się bawić. Jest niestabilna, nie jest niebezpieczna... ogólnie... spoko. Paulina się przejdzie...

Ale wpierw - Sylwester Bankierz, najlepiej jeszcze dziś. Sylwester znalazł dla Pauliny czas, oczywiście. Zaprosił ją do siebie. Paulina przyjechała z ciasteczkami. Sylwester powiedział na głos "Jeśli podsłuchujesz, tien Muszkiet, przestań". Z uśmiechem zwrócił się do Pauliny - czym może pomóc?  Paulina powiedziała, że uniknięciem wojny na tym terenie. Sylwester nie chce wojny. Paulina zauważyła, że działania Tamary do niej prowadzą. Paulina zauważyła, że nie tylko społeczność zantagonizowała ale i mafię. Sylwester powiedział, że Świeca nie może zaakceptować i układać się z mafią. Paulina odbiła: "tien Bankierz, na tym terenie Świeca sama zachowuje się jak mafia". Tamara JUŻ napadła na Dracenę, przedtem na Daniela z magitrowni... Tamara doprowadzi do konfliktów. Konflikt: (2, lojalność Sylwestra wobec podwładnych, Świeca musi wrócić na ten teren, Tamara wie co robi). Paulina kontruje (Tamara nie wie co robi bo jest terminusem a ja byłam rezydentką, ja rozmawiałam z Dukatem więc niech odpuści to będzie dobrze). 

Paulinie się udało przekonać Sylwestra - pokazała, że zależy jej na tym terenie. Chce z nim współpracować. Chce, by było jak najlepiej. On nie chce, by Świeca układała się z mafią. Zdaniem Pauliny - nie muszą. Ona tu jest i jej mafia ufa i Świeca ufa. Niech Sylwester powściągnie Tamarę i będzie dobrze. Ona załatwi tak, by lokalny boss mafii też się hamował. Sylwester się zgodził. Czemu nie spróbować?

Paulina wróciła do domu zadowolona. Przy odrobinie szczęścia uniknęliśmy wojny.

Późnym wieczorem już Paulina ma request komunikacji hipernetowej od Tamary. Przyjęła.

Tamara opowiedziała Paulinie historię czarodzieja Jana Karczocha - młodego architekta z okolicy. Jego rodzice mieli firmę transportową i Dukat jej potrzebował. Więc rodzice Karczocha go wygnali by go chronić - nadal jednak Dukat (osobiście!) mówił im co i jak dokładnie mają robić. Pewnego razu kierowca uległ śmiertelnemu wypadkowi i ojciec Karczocha poszedł do więzienia. Sam Karczoch? Nie wiedział o tym. Załamany, skupił się na budowaniu jakiejś durnej areny i popadł w innego typu kłopoty. I inne problemy jakie wynikają z działania mafii. Dlatego Tamara uważa, że Paulina jako rezydentka robiła rzeczy złe - akceptowała, że to się dzieje. Mafia i Świeca to nie to samo. Paulina tu powiedziała, że ona skupia się na systemowym bezpieczeństwie ludzi, nie na pojedynczych jednostkach. Jednostkom pomaga jako lekarz, jako rezydentka odpowiada za okolicę.

Paulina mocno wierzy, że ktoś o poczuciu sprawiedliwości po każdej ze stron doprowadzi do tego, że Świeca i Rodzina Dukata obie będą się pilnować. Niektóre akcje nie powstaną. Obie strony będą trzymać się w szyku. To jest balans do którego Paulina dąży.

Tamara powiedziała, że jest to sprzeczne ze wszystkim o co Paulina walczyła. Might makes right. Wykorzystywanie ludzi i magów przez silniejszą organizację. Dokładnie to, z czym cała historia Pauliny wskazuje, że walczyła. A Paulina nie wie co powiedzieć... ale wie, że na tym terenie MUSI być siła, bo inaczej stanie się coś strasznego z efemerydą. I mafia jest większą siłą niż Świeca. I Świeca nie uzyska takiej potęgi. I Paulina powiedziała, że NIE współpracuje z mafią blisko. 

Tamara weszła ostro jako kapłanka Świecy. Zrobiła homework. Przygotowała się i próbuje Paulinę przekonać. Pokazać jej mrok jej własnych decyzji i konsekwencje jej własnych wyborów. A Paulina próbuje pokazać Tamarze, że tam gdzie się spotkała ze Świecą tam było to na niekorzyść Świecy a półświatek ma też dobre, pozytywne działania. I że to nie jest czarnobiałe - Paulina musi podjąć te decyzje które podejmuje. Pełen sukces - Paulinie udało się odeprzeć atak Tamary. Jej słowa nie robią na Paulinie żadnego znaczenia.

Paulina zdecydowała się odbić pałeczkę. W tej chwili, w tej sytuacji i w tym stanie terenu Świeca jest bezużyteczna. Nikt Świecy tu nie chce. Obecne postępowanie Świecy niczym nie różni się od postępowania Świecy sprzed czasów Karradraela. Tamara zauważyła, że Paulina nie ma prawa wiedzieć o tym, że odkąd przybyła odpowiedziała na kilka próśb o pomoc i pomogła. Ale przez dezinformację nikt nie wie. Paulina powiedziała, że działania militarne dezorganizują cały ten teren. 

Skonfliktowany sukces. Tamara powiedziała, że jest skłonna do tego, by spróbować pokazać czemu Świeca jest tu potrzebna i korzystna. Ale wszelkie próby dezinformacji oraz krzywdzenia ludzi i magów spotkają się z odpowiednim działaniem ze strony Tamary. Paulina podziękowała. O to mniej więcej jej chodziło.

## Wpływ na świat:

JAK:

* 1: do czegoś, co się wydarzyło dodajemy "ale" lub "oraz"
* 2: dodajemy fakt / scenę z przeszłości do Znaczącego Bytu
* 2: pozyskanie przez dowolną postać znaczącego surowca mającego sens z perspektywy misji
* 2: CLAIM na wątku
* 3: korzystny z perspektywy motywacji postaci (dowolnej) ruch; przesunięcie zgodne z motywacją
* 5: przesunięcie niezgodne/sprzeczne z motywacją
* 5: przesunięcie wobec postaci z CLAIM wbrew graczowi

MG: 10

* Kluczowe elementy soullinkowania dziecka Dukata pochodzą z wiedzy pozyskanej przez Sądecznego w ramach działania na tym terenie
* Tamara skupia uwagę na wspieraniu przede wszystkim Tomasza Myszeczki. Duży, rozległy, potrzebuje pomocy. Brak mu wszystkiego.
* Sylwester opłaca nowego przedsiębiorcę mającego założyć konkurencyjną małą magitrownię. By uniezależnić się od gróźb. Aha, czyn nieuczciwej konkurencji.

Kić: 4

* Dukat zatrzymał działania Sądecznego ORAZ wstrzymało to niszczenie Legionu
* CLAIM: Paulina zostanie rezydentką Świecy na tym terenie a Tamara jej prawą ręką - terminusem bojowym.
* Aneta wróciła wtedy z Sądecznym ORAZ wie, co tam się stało


# Progresja



# Streszczenie

Maria odkryła dla Pauliny coś, czego Sądeczny nie zamaskował - Aneta Rukolas i Fryderyk Grzybb weszli z Sądecznym kiedyś do Muzeum Pary i coś się tam stało. Tymczasem Wiaczesław na prośbę Sądecznego zaczął masakrować Legion i robić z niego pośmiewisko. Paulina skontaktowała się z Sylwestrem Bankierzem i z Gabrielem Dukatem; zapewniła, że nie będzie otwartej wojny. Tamara próbowała wpłynąć na Paulinę - niech wie, że mafia jest ZŁA. Paulina jest jednak pragmatyczna.

# Zasługi

* mag: Paulina Tarczyńska, która walczy o to, by nie wybuchła wojna domowa i mediuje między Świecą i Rodziną Dukata - a nawet wewnątrz tych organizacji. Aha, i z Draceną.
* czł: Maria Newa, która dotarła do powiązania między byłymi magami Pentacyklu a Robertem Sądecznym w okolicy Muzeum Pary. Nie może iść dalej, by się nie narażać.
* mag: Tamara Muszkiet, rozsądniejsza niż się wydawało; nie zaatakowała Draceny, osadzona przez Sylwestra i próbująca przekonać Paulinę o tym, że mafia nie może być przez nią wspierana.
* mag: Sylwester Bankierz, nie dąży do wojny a do współzrozumienia; za namową Pauliny przerwał antyDukatowe działania i poskromił Tamarę w jakimś zakresie.
* mag: Dracena Diakon, która odkryła zbezczeszczony element Legionu, wpadła w kłopoty z Tamarą i w końcu niechętnie obiecała zachować sekret tego co się tu działo.
* mag: Gabriel Dukat, za namową Pauliny usadził Sądecznego, by ten nie rozpętał wojny ze Świecą. Okazuje się, że trochę się odsunął od kierowania mafią.
* mag: Wiaczesław Zajcew, wynajęty przez Roberta Sądecznego, by uciemiężył i skompromitował Tamarę. Dokonał tego przez masakrację Legionu i rysowanie na nim penisów.
* mag: Aneta Rukolas, o której Maria się dowiedziała, że kiedyś weszła z Sądecznym i Grzybbem do Muzeum Pary. I tam COŚ się stało, co odbiło się w Efemerydzie.
* mag: Fryderyk Grzybb, o którym Maria się dowiedziała, że kiedyś wszedł z Sądecznym i Rukolas do Muzeum Pary a wyszedł ALBO on ALBO Farnolis.

# Lokalizacje

1. Świat
    1. Primus
        1. Mazowsze
            1. Powiat Pustulski
                1. Krukowo Czarne
                    1. Gabinet Pauliny, gdzie przez problemy z magitrownią Paulina miała nasilenie pacjentów
                1. Męczymordy
                    1. Las szeptów, Dracena znalazła tam splugawiony element Legionu przerobiony w stracha na wróble.
                1. Senesgrad
                    1. Zalesie
                        1. Muzeum Pary, gdzie w czasie nieobecności Pauliny Sądeczny robił coś z Grzybbem i Rukolas.

# Czas

* Opóźnienie: 0
* Dni: 1

# Wątki kampanii

* Powrót Srebrnej Świecy
    * CLAIM: Paulina Tarczyńska zostanie rezydentką z ramienia Świecy i wsparta przez Dukata (Kić)
    * Sylwester został zesłany tu przez Newerję
    * Sylwester: przeprowadził udaną akcję zdobycia transportu Dukata
    * Tamara doprowadziła do sytuacji, gdzie Świeca nie jest na ciągłym podsłuchu
    * fakt: Tamara nie jest w stanie rozwiązać wcześniejszej dezinformacji
    * fakt: Tamara robi się bardzo podejrzliwa wobec Myszeczki
    * Myszeczka staje po stronie rezydenta Świecy
    * Awiany są strategicznie potrzebne zarówno mafii jak i Świecy
    * fakt: Oktawia w power suit ma dostęp do tego co wie power suit. Wie to, co Tamara przekazała.
* Działania Roberta Sądecznego
    * Robert dostaje haracz od "Nowej Melodii"
    * rezydent polityczny Dukata u Myszeczki
    * Robert spinuje akcję dezinformacyjną. Gdyby nie Świeca, nie byłoby kłopotu i elektrownia nie miałoby tragedii (3)
    * Robert uważa, że współpraca z Danielem oznacza, że Daniel jest dużo bardziej cenny niż myślał wcześniej
    * Sądeczny daje Kocieborowi własną agencję detektywistyczną za zasługi i ratunek Dobrocienia
    * Awiany są strategicznie potrzebne zarówno mafii jak i Świecy
    * fakt: Sądeczny w swoim investigation odkrywa, że Tamara zadziałała wbrew Danielowi i on nie chciał
    * fakt: Sądeczny dostarczy maga, który zajmie się glukszwajnem
    * Sądeczny nasyła Zofię na szpiegowanie Tamary
    * Maria Newa dowie się o złych rzeczach jakie robiły siły Dukata
* Katia Grajek
    * To Hektor pomagał w budowaniu eliksirów dla Katii
* Dracena, wracająca do świata ludzi
    * CLAIM: Dracena docelowo będzie w stanie zintegrować się z JAKIMŚ społeczeństwem (Kić)
    * CLAIM: Dracena i Oktawia będą miały duet. (Żółw)
    * CLAIM: Szybki, dyskretny awian. Trafi do Draceny. (Żółw)
    * Fakt: Dracenę wylano z "Nowej Melodii" za radykalizm
    * Dracena wykonuje działania mające uwolnić "Nową Melodię" od mafii 
    * Fakt: Elektrownia z Draceną działa dużo lepiej i taniej
    * Dracena dowiaduje się, że elektrownia współpracuje z mafią a nie jest wykorzystywana
* Gabriela Dukata droga do ostatecznej medycyny dla dziecka
    * CLAIM: Dukat lubi Paulinę (Kić)
    * Dukat nie ma gwałtownej reakcji na działania Sylwestra; da się wychować. Tupnął nogą bo musiał - and now we talk" (Kić)
* Osobliwości lokalne
    * CLAIM: Paulina nie będzie musiała uciekać z terenu niezależnie od okoliczności (Kić)
    * CLAIM: Myszeczka jest ważnym partnerem biznesowym Daniela (Raynor)
    * Zarówno siły Dukata jak i Myszeczki zapewniają, by Skażenie nie dotarło do magitrowni.
    * Pojawia się zwiększenie ruchów antymausowych na tym terenie - mniej portaliska dla Mausów.
* Kluczowa rola glukszwajnów wobec efemerydy Senesgradzkiej
    * CLAIM: nigdy więcej efemeryda nie skrzywdzi moich awianów (Foczek)
    * CLAIM: będę w stanie wykorzystać tą efemerydę jako niesamowite źródło energii (Foczek)
    * W okolicy Męczymordy jest odpowiednia ilość pola rezydualnego by sprawić by znikające świnie były prawdą
    * Rolnik od świń założył ma misję - i przydupasa - mające udowodnić, że ze świniami dzieje się coś dziwnego
    * Natalia ma własnego tresowanego glukszwajna z apetytem na magię Harvestera
    * Natalia ma eksperta od glukszwajnów; człowieka kiedyś pracującego dla Myszeczki
* Wolność Eweliny Bankierz
    * CLAIM "Newerja Bankierz x Gabriel Dukat - zostaną parą" (Kić)
    * fakt "docelowo Ewelina dostanie jakieś środki do życia; nie będzie żebraczką"
* Echo starych technologii Weinerów
    * fakt: Harvester nie ma dość energii by się obudzić
    * Natalia pozyskała bezpiecznie kolejną część artefaktu
* Oktawia, echo Oliwii
    * CLAIM: Oktawia dorośnie i zniknie. (Kić)
    * Oktawia chce pomóc Krzysztofowi, bo widzi w nim potencjał do pomocy Oliwii


# Narzędzia MG

## Opis celu misji



## Cel misji



## Po czym poznam sukces



## Wynik z perspektywy celu



## Wykorzystana mechanika

Postać (strona gracza):

| .Motywacja. | .Umiejętności. | .Magia. | .SiłySłabości. | .Znam.    | .Mam.      |.POSTAĆ. |
|   (-2) - 2  |     0 - 2      |  0 - 2  |    (-2) - 2    | Z+M = 0-6 | Z+M = 0-6  |         |

Opozycja (strona NPC):

* Baza: 2
* Aspekt: 3/aspekt, wariant z nożyczki/papier/kamień
* Modyfikatory: Skala, Sprzęt, Sytuacja (1-3 każdy)

Wynik:

| .Postać. | .Opozycja. | .Rzut.    | .Wynik.  |
|          |            | xx: +x Wx |   S-SS-F |

Wpływ:

* 10 kart / dzień
* każda porażka i skonfliktowany sukces = +2 pkt wpływu dla gracza
* każda karta = +1 wpływ dla MG
* każde 5 kart zaokr. w górę: +1 pkt wpływu dla gracza

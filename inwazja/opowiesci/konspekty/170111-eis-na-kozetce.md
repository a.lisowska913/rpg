---
layout: inwazja-konspekt
title:  "EIS na kozetce "
campaign: powrot-karradraela
gm: żółw
players: kić, dzióbek
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [170228 - Polowanie na Mausównę (temp)](170228-polowanie-na-mausowne.html)

### Chronologiczna

* [170108 - Samotna, w świecie magów (SD)](170108-samotna-w-swiecie-magow.html)

## Kontekst ogólny sytuacji
## Punkt zerowy:
## Misja właściwa:

### Dzień 1:

Siluria obudziła się, w dobrym humorze. Nie wie jeszcze, że to tymczasowe. Wymknęła się, póki Franciszek Maus śpi ;-).

Ogarnęła się, potem mała rundka po KADEMie. Kto Silurię zaczepi? ;-). O dziwo, Bogumił Rojowiec. Powiedział jej, jakie problemy spotkały Kropiaktorium; rozsypały się kontakty biznesowe. Przecież to nic groźnego; odbudują to, prawda? Chwilowo Quasar się rzuca i nie pozwala i nie pozwala nic robić takiego z magami Świecy... Siluria zaproponowała, że można w aurze artefaktów zaznaczać, że są one odsprzedane. Najlepiej z kodem komu. Trzeba tylko dopracować to katalitycznie. Jeszcze bardziej kosztowne i czasochłonne, ale może da się coś z tym zrobić... może się opłacać.

Rojowiec się wyraźnie ucieszył. Pogada z innymi. Może uda się coś wymyślić. Aha, ostrzegł Silurię, że Balrog Bankierz rzucił mu wyzwanie i wyzwał go od tchórzliwych ognistych kurczaków. Które tylko umieją bić niewinnych i gdakać. A pary w łapach nie ma. Rojowiec wyraźnie się zmartwił - Ignat nie ma szans z Balrogiem. Ten mag to legenda ringu. Rojowiec zapytany, czy bawi się w takie rzeczy zaprzeczył. Niech Ignat odmówi. Nie musi walczyć z takim magiem, nie ze Świecy, nie w takich okolicznościach!

Siluria poszła odwiedzić Infernię. A tam... Karolina, z Infernią namiętnie coś obserwują. Karolina ma wypieki na twarzy. Infernia wyraźnie zaciekawiona... sytuacja jest tylko LEKKO erotyczna, co wynika z roślinek Inferni. Siluria zagadała; Karolina zafascynowana jest tymi narkotykami zrobionymi przez czarodziejkę Świecy. Naprawdę ciekawie zrobione. Miała pomoc katalisty; potrafi pięknie współpracować z katalistami. Wzór maga się ślicznie zespolił z samym narkotykiem a ograniczniki sprawiły, że narkotyk nie mógł zabić ani wywołać zbyt poważnej choroby. Czyli albo ta czarodziejka (Ula) była tak samo lekarzem albo bardzo dobrze jej magia pozwala jej na współpracę z innymi magami. Ciekawe. Albo ma INNE stymulanty, które jej na to pozwalają - zmniejszają bariery jej magii.

Teraz Siluria sama się zaciekawiła. Karolina powiedziała że da się zrobić takie rzeczy; one wymagają dużego zaufania od osoby biorącej. Staje się zbyt "otwarta" magicznie. Samo to świadczy o desperacji... lub ignorancji.

* Szukałaś mnie, kochanie? - Infernia do Silurii; temperatura w głosie Inferni sprawiła, że Karolina się zaczerwieniła
* Ale tym razem, słonko, w troszeczkę innej sprawie - uśmiechnięta znacząco Siluria, po czym spoważniała - Mam zmartwienie, wiesz?

I Siluria wyjaśniła Inferni o Ignacie. Ta facepalmowała na miejscu. Co można zrobić? Zamknąć go w ciemnicy na najbliższy miesiąc? Najpewniej roztrzaska... i będzie zły i głodny. Bankierz wyzwał go na pojedynek. A ten Bankierz ma szanse mu wklupać... i ma powód. Pretekst.

* Ignat nie potrafi utrzymać swoich tępych pięści na miejscu... - Infernia, sfrustrowana
* Nie, nie potrafi... - Siluria, zrezygnowana
* Zasłużył na małe wciry po prawdzie - Infernia, myśląc
* Jak teraz dostanie wpiernicz... dopiero udało mi się uspokoić nastroje na KADEMie po ostatnim... - Siluria zmartwiona - Tym razem, po ostatnim... i niefortunnym pomyśle napastniczki...
* Rozumiem. Jak ona się nazywała? - Infernia, zaciekawiona
* Nieistotne - Siluria.
* Zapytam Ignata. On mi powie. - Infernia, ze złośliwym uśmiechem
* Daj jej spokój. Młoda, naiwna... nie miała pojęcia co robi - Siluria, uspokajająco - Tak słodka i niewinna że aż wstyd ją w ogóle ruszać.
* I miała fajną czapkę z daszkiem ^_^ - Karolina błysnęła, jak to ona.

Konflikt. Infernia 3. Siluria 5. Sukces. Infernia odpuściła; nie zamierza szukać Anki ani się nią interesować.

* Powinnaś pogadać z Ignatem i Karoliną, słoneczko. Pół KADEMu ostrzy sobie miecze na tą małą. - Infernia, złośliwie - Sama nie muszę nic robić. Wystarczy, że komukolwiek innemu powiedzą...
* I dlatego prosiłam, żeby nie mówili. - Siluria, zdecydowanie, patrząc na Karolinę. Ta uniosła dłonie w geście defensywnym
* Nikomu nic nie powiedziałam i nie powiem. Nie mam po co. Sympatyczna dziewczyna... A taka Whisper by ją zmasakrowała. - Karolina, wyjaśniając Silurii
* Za to, że okazała się kimś z sercem - Siluria, z przekąsem
* Po prawdzie, Silurio - Karolina, z lekkim namysłem - Ktoś z sercem wywołuje KOSZMARNE efemerydy na Pryzmacie.
* Wiem. A czemu jej tu nie ma... między innymi? - Siluria, wzdychając - Czasem KADEM zapomina, że nie wszyscy są na Fazie.
* Tak czy owak, trzeba coś wymyślić z Ignatem, by się nie rzucał w ten pojedynek... - zmartwiona Siluria
* Tygodniowa sekswycieczka? - bezpośrednia Infernia jest bezpośrednia
* No niby można... może by na to poszedł - Siluria - Ale jak za każdym razem mu taką zorganizujemy, NIGDY nie wrócimy do KADEMu...
* Może jakoś przekupić tamtego Bankierza? - Karolina, z namysłem
* Niby znam tego gościa... ale jest specyficzny - Siluria.
* Nie śpię z magami spoza KADEMu. - prychnęła Infernia
* Ja tak ^_^ - zawsze zachowująca humor Siluria
* Może zająć jego uwagę czymś innym? - Siluria, kombinując

Siluria poprosiła Infernię, by ta zmotywowała Ignata; niech on zacznie porządnie ćwiczyć. Żeby zwiększyć jego szansę na zwycięstwo. Niestety, do walki dojdzie i Siluria nie ma pozycji by Ignata powstrzymać. Ignat nie przyjmie pozycji "niech mnie obrażają, lol". A do walki z jakimś Bankierzem Ignat nie będzie przyzywał żadnej Zajcewowej magii. Balrog nie wyznaczył jeszcze daty pojedynku; najpierw musi zrobić odpowiedni hype ;-). Musi być odpowiedź Ignata... i Ignat ma możliwość wyboru broni i czasu. Siluria poprosiła Infernię, by ta się upewniła, że Ignat nie odpowie póki Siluria nie wróci.

-1 surowiec. Netheria Diakon jest tu potrzebna. Siluria chce się z nią spotkać; Netheria ma apartament w Kopalinie, więc SIluria daleko nie ma.

Netheria jest u siebie. Poprosiła Silurię do siebie. Siluria powiedziała, że chce wyrównać szanse pojedynku. Neti spytała, czy TEGO pojedynku. Tak, tego. 

* Bukmacherzy dają przewagę Bankierzowi - Netheria, spokojnie
* Wiem. Ja też - Siluria, z uśmiechem
* Rozumiem, że mam postawić na Zajcewa? - Netheria, z lekkim półuśmiechem
* Na kogo postawisz, to... to Twój wybór. Nie zależy mi na jego zwycięstwie. Ale nie może paść jak dziewica do Romea... - Siluria - Normalnie to by mi nie przeszkadzało, ale jak mi Ignata sklupią tak bez reszty...
* KADEM się zemści - Netheria
* No właśnie. I ja będę mieć problem... - Siluria, zmęczona - Rozumiesz. Przy bardziej wyrównanym starciu... sprawa nie będzie aż tak krytyczna.

Netheria spytała Silurię, jak może jej pomóc. Siluria zauważyła, że Balrog jest specyficzny. Nie opuścił żadnej imprezy. "Ani żadnej fanki" - dodała Netheria. Siluria chciałaby, by Bankierz był zmęczony, miał zajęcie całą noc, ostre imprezy... ogólnie, wyrównać szanse.

* Możemy zaatakować jego fanki LUB zrobić imprezę. - Netheria, wyjątkowo chłodno - W obu wypadkach będzie się bardzo starał i męczył. Jest opiekuńczy wobec fanek... zwłaszcza tych z przywilejami.
* Atak na fanki może powiązać z Ignatem. - Siluria, niechętnie
* Zależy jaki atak. Fanka, która poszła na imprezę i nic nie powiedziała? - Netheria, rozjaśniając - Jakby on myślał, że coś jej grozi... i jej szukał...
* Nie, Netherio... - Siluria, kręcąc głową. - Potrzebuję po prostu czegoś, co nie da KADEMowi powodów do zemsty. Nie widzę sposobu, by ta dwójka się pogodziła...

Netheria się uśmiechnęła. Sprawdzała Silurię; osobiście nie lubi atakować osób trzecich. Spytała Silurię, czego ta zatem oczekuje? Imprezy? Siluria potwierdziła - takiej, by Balrog nie mógł na nią nie pójść. Serii imprez. Wykańczających. Netheria aż pokręciła głową - koszta będą bardzo wysokie. Seria imprez. Siluria się uśmiechnęła. Na pewno się coś da zrobić...

* Przecież jesteś NAJLEPSZA! - Siluria, bezczelnie przypochlebiająca się Netherii
* Opóźniaj go, Silurio... - Netheria, wzdychając - Te przygotowania zajmą sporo czasu...
* Ile to jest 'sporo czasu'? - Siluria z ciekawością
* Tydzień? Będzie bez reklamy... - Netheria, ciężko myśląc
* Nie możesz wykorzystać istniejących imprez? - Siluria, kombinując
* Muszę. Zwykle odpowiedniej klasy imprezy mają kilkumiesięczne wyprzedzenia... - Netheria, kręcąc głową...

Wyszło na to, że będzie ciężko...

* Mówisz, że to będzie kosztować. A co, jeśli znajdziemy sobie sponsora? - szeroko uśmiechnięta Siluria
* Skierować jego środki na te imprezy? Hmm... - Netheria, coraz szerzej uśmiechnięta
* Przecież nie będzie mógł się nie pojawić - Siluria, z drapieżną minką
* Nawet się zgodzę pracować za darmo, by uhonorować wielkiego wojownika... - Netheria, z podobnie drapieżną minką
* ...ja Ci zapłacę, innymi słowy? - Nagle posmutniała Siluria
* Wyciągniesz sobie z KADEMu. - Netheria, pokazująca język.

Siluria pożegnała się z Netherią. Wdrożyła ją w koncept Melodii Diakon - będzie mieć ułatwienie. Melodia ma go namawiać na dużą ilość imprez i namawiać do wyprztykania się z kasy, zarżnięcia się imprezowo i tak dalej. A nie podburzać go do walki. Netheria zachichotała nerwowo. Melodia na pewno doceni...

Siluria poszła do Działa Plazmowego - tam jest Melodia. Siluria zaczęła tańczyć z nią, by zwrócić jej uwagę - odbiła sobie Melodię od jakiegoś gościa przebranego za Szkieletora (tego z HeMana)...

* O, kuzynko! A od kiedy to Twoje klimaty? - zaskoczona Melodia, przebrana za Smerfetkę
* Do Ciebie przyszłam - uśmiechnięta Siluria - Mam biznes
* Tu możemy porozmawiać, jest tu ostre techno i nic nie słychać - wzruszyła ramionami Melodia

Siluria odciągnęła Melodię i wyjaśniła o co chodzi. Siluria przyznała na zarzuty Melodii - tak, Ignat zasłużył. Tak, powinien oberwać. Ale nie bardzo mocno i nie doszczętnie. Bo się KADEM pogniewa. Siluria przedstawiła to jako potencjalnie przyszły konflikt gildii. Opcja gdzie Ignat dostaje wciry ale zachowuje honor też jest opcją. Melodia się uśmiechnęła - zajmie się Balrogiem. Ktoś musi wydać jej płyty, a nawet Melodia jest skłonna do tego, by Balrog zrobił z nią karaoke... może nawet zaśpiewa z nią coś? ;-).

Siluria i Melodia się zgodziły. Melodia wróciła do tańczenia, Siluria z uśmiechem poszła do KADEMu...

Tymczasem Pawła Sępiaka (który rozmontowywał nowy eksperyment Janka) poprosiła o drobną pomoc Ilona Amant... A dokładniej - weszła mu do pokoju z uśmiechem, z głowicą GS Aegis 0002. 

* Quasar ustawiła monitory na to, czy się zbliżam do magazynów. Wiem, bo mnie znalazła. Ale ja sprawdziłem na co działają monitory. Nie ma tam Ciebie. - uśmiechnięta Ilona
* Czyli chcesz powiedzieć, że chcesz bym bez narzędzi (bo przejmie) zinterfejsował z Eis... wbrew reszty KADEMu? - podłamany Paweł

Ogólnie, Ilona coś kręci. Chce dostać dostęp do skanu mentalnego Eis; martwi się, że AI oszalała. A niedawno jakaś czarodziejka z KADEMu tam z nią rozmawiała... to znaczy, do niej mówiła. I Ilona chce sprawdzić, czy nic to nie zmieniło. Bo co prawda nie ma sensorów, ale są na Fazie... i w ogóle, powinno się z nią monitorować. A chwilowo siedzi w magazynie i nic się z nią nie dzieje.

* Czy mogę zaproponować byśmy zaczęli edukację naszych AI od pukania, a nie trybów bojowych? - Paweł, z przekąsem
* Wtedy nikt nie da mi zezwolenia, bo AI będą bezużyteczne. - Ilona, ze smutkiem
* Jakie są plany uczenia się od Eis, skoro jest w magazynie a nie ma z nią kontaktu? - zdziwiony Paweł
* Wiesz... nie ma. Siedzi w magazynie i nic się nie dzieje. To nie design KADEMu, więc... wiesz - Ilona, zirytowana
* Jeżeli mogę być szczery... mam wrażenie, że masz konkretny plan dotyczący Eis nie związany z jej monitorowaniem i chcesz mnie wykorzystać. Ale wolałbym wiedzieć, w co się pakuję. - Paweł, prosto.

Konflikt: remis. Paweł się dowie, ale nie przekaże dalej. Dyskrecja.

* Ona to wszystko obserwuje. Wszystko. W każdej chwili. Quasar i Yyizdath. Myślą, że są w stanie wszystko widzieć, ale nie z tym - Ilona, z uśmiechem
* Jak to działa?
* W tej chwili tam stoimy i się całujemy. Jesteśmy w... naszym stanie umysłów. Wspólnym. W atrium - Ilona, z uśmiechem
* Siluria mnie zainspirowała. Quasar zakłada, że to słabe, ludzkie odruchy. Nie patrzy ;-) - Ilona, tłumacząc co zrobiła

I Ilona się przyznała. Eis by oszalała. Adaptacyjna inteligencja strategiczna, bez zdolności przyswajania? Więc, by mogła słuchać. Obserwować. W jej magazynie. Tylko tam. A potem dowiedziała się, że tam chodziła jakaś czarodziejka i się jej zwierzała. Serio. Opowiadała jej historie swojego życia, sukcesy i porażki. Więc teraz Ilona musi sprawdzić stan Eis... Siluria wie, kto się zwierzał Eis. Ale ona, Ilona, nie.

"Na Quasar sztuczka zadziała raz. Może dwa. Ale zawsze jest też kolejna sztuczka." - Ilona.

* Chcesz powiedzieć, że mamy najlepszą nam adaptywną AI bojową która jako JEDYNY zestaw danych wejściowych dostała zwierzenia jakiejś nastolatki bez INNYCH zbiorów informacji wejściowych? - załamany Paweł - Potencjalnie Eis jest teraz emo.
* Emo nastolatka to dokładnie słowa Quasar. Było "żaliła się" i "wypłakiwała się" i "nie mam przyjaciół" i inne takie - zrezygnowana Ilona
* Dobrze, ale... ojej. I dlatego potrzebujesz snapshot Eis? - Paweł
* Masz lepszy pomysł? Całkowicie szczerze pytam - zaciekawiona Ilona
* To jest jeden z bardziej niebezpiecznych pomysłów, jakie można zrobić - Paweł - Jeśli dobrze rozumiem, przy aktualnym stanie Eis nieszczególnie jest opcja by nie zauważyła, że jest skanowana
* Masz rację. Ale przeceniasz niebezpieczeństwo... - Ilona
* W wypadku Eis wolę przecenić niż nie docenić niebezpieczeństwa. - Paweł, wchodząc jej w słowo
* Eis nie jest głupia. Nigdy nie była. Wie, że jeśli zrobi coś głupiego to zginie - Ilona.
* Widząc co ją do tej pory od nas spotkało może założyć, że jej nie zniszczymy, póki nie mamy wyjścia - Paweł
* Touche... - Ilona

Ilona powiedziała, że chce zrobić snapshot i sprawdzić stan Eis na Atari, które ma airgap.

"Jestem pewien, że będziemy mieli najbardziej nieszczęśliwe Atari w całym budynku" - Miaushi, 2017

Ilona przyznała, że nie do końca wie co zrobić... Paweł zaproponował eksperta od Szaleństwa. Ale jak można to zrobić?

* Czyli musimy zainterfejsować z Eis psychologa tak, by Quasar nie wiedziała? - Ilona, z nienaturalnym uśmiechem
* Ano - Paweł, z podobnym uśmiechem
* Podoba mi się Twój plan. Zrealizujesz go? - Ilona, ze SZCZERYM uśmiechem
* Będziemy potrzebowali WIĘCEJ urządzeń do całowania... - Paweł, nieszczęśliwy
* Chcesz powiedzieć, że zrobimy sobie... KĄCIK MIŁOŚCI... z magazynu z Eis? - wstrząśnięta Ilona

Paweł zaproponował eksperyment. Jeśli Quasar nie wie, że Eis ma jakieś informacje (więc się nie przejmuje, że ktoś tam przyszedł i się wyżalił), w związku z czym jeśli pojawią się tam bardziej... intensywne sensorycznie sceny, to jeśli Quasar nie zainterweniuje od razu, to nie wie. Ale... jak... jak przekonać WIOLETTĘ by przesłuchała i przebadała Eis? I tu Paweł się uśmiechnął. Po to jest Siluria.

W końcu na przestrzeni dyskusji wyszło, że najlepszym magiem do przesłuchania Eis będzie Franciszek Maus. Więc trzeba wtajemniczyć Silurię...

Wieczorem, Siluria w dobrym humorze wracała na KADEM. I tam, zadzwonił do niej Paweł. I spytał, czy Siluria ma ochotę na herbatkę. Siluria zaproponowała Rzeczną Chatę...

W Rzecznej Chacie, Paweł powiedział Silurii że ma problem. 

* Najprościej będzie powiedzieć wprost: potrzebne jest, byś się z kimś przespała - Paweł, jak to Paweł, bezpośrednio
* Nuda... - Siluria
* Co więcej, to recydywa
* Co ostatnio nie jest recydywą, mi powiedz...
* Ale są w tym dwie ciekawe rzeczy...
* Nowe zabawki?
* Tak. Potencjalnie mam... wiesz, że Quasar lubi obserwować co się dzieje na KADEMie. Mam coś, by nie dało się Ciebie monitorować.
* Ok... - bardzo zaciekawiona Siluria
* Jest to artefakt, gdzie jesteś jedyną osobą na KADEMie zdolną używać to wielokrotnie. ANyway, problem z Eis.
* Z Eis?
* Chcemy dowiedzieć się, jaki jest jej stan. Ale jest embargo na kontakt z Eis.
* Czy ja wiem? Jest magazyn.
* Rozmawiałaś z nią? Jest odcięta od bodźców.
* Problem polega na tym, że nie wiemy czy całkiem odcięta i musimy dowiedzieć się... ale nie chcemy jej uwolnić. Przyznasz, że to szczytny cel. W związku z czym chcemy z nią porozmawiać. Ale główne podejrzenie, że Eis jest tyci szalona... więc potrzebny ktoś...
* No ale z Eis się nie prześpię przecież
* Myśleliśmy o naszych ekspertach od Szaleństwa... albo Wioletta, albo Franciszek.
* Oboje są ciekawi - uśmiechnięta Siluria 
* Z mojego punktu widzenia, jak się połączysz z oboma, to też zadziała
* Chcesz Eis od razu przedstawić trójkącić?
* Czemu po prostu nie zgrać danych?
* Bo coś zainfekuje...

Paweł zauważył, że jeśli Eis jest zamknięta i odcięta od bodźców, chce się uwolnić. I najpewniej to planuje. Ale przy rozmowie głosowej? Nie wciśnie i nie wykona żadnego planu. Siluria zauważyła, że Eis niekoniecznie chce rozmawiać. Paweł powiedział, że robi założenie, że ma możliwość komunikowania się z kimkolwiek, więc ma możliwość przekonać, żeby ją wypuścili. Siluria zainteresowała się Pawłowym zainteresowaniem Eis. On powiedział, że to Ilona. Kazała mu to rozwiązać... Paweł przyznał się, że namieszali z Eis... dostarczyli jej sensory, bo sposób przechowywania Eis jest... nieludzki. Jest w pudełku. Bomba zegarowa leży i tyka...

* Ktoś już rozmyślał, co chcemy jej powiedzieć? - Siluria, zaciekawiona
* Nie. Dlatego potrzebuję psychologa. Interesuję się AI, ale na co dzień... nie pracuję z szalonymi AI. - Paweł, lekko zrezygnowany

Paweł powiedział Silurii, że ktoś był u Eis i się skarżył. I Eis mogła usłyszeć. Nawet najpewniej usłyszała.

Aktualny pomysł Pawła: 

* Wziąć zwykłe słuchawki do komórki. Takie applowskie.
* Zrobić dwa portale na kablu. Rozłączyć.
* Dać zasymilować kabel przez Eis. I to zadziała.
* BEZPRZEWODOWA SŁUCHAWKA DO EIS. A Eis ma kawałek kabelka ze wtyczką i... nic nie zrobi.

Największą załamkę ma Siluria; żeby Franciszek był kompetentnym psychologiem, to on nie może być zbyt napalony. Innymi słowy, musi zażyć jakiś środek... i on będzie mamrotać do "Silurii" (Eis) a ona będzie ekstra głośna, by nikt (Quasar) się nie zorientował...

Niby wszyscy wygrywają, ale poświęcenie...

Paweł podszedł do Karoliny na KADEMie, w Instytucie Biomancji. 

* Coś, co pozwoli zachować przytomność umysłu pracując z Silurią... - Paweł, znacząco
* Martwy ptak nie wstaje z gniazda tylko leży martwy? - spytała Karolina współczująco
* Raczej... ptak lata, ale coś co pozwoli skupić się na tym co trzeba zrobić - Paweł - Coś... nie fizyczne a mentalne
* Fajne wyzwanie. Dobra! Na jutro?! - Karolinie zaświeciły się oczy - Sto procent skupienia... i sto przyjemności!
* Brzmi jak plan! - szczęśliwy Paweł

Jeszcze w nocy Paweł (surowcem) składał... drony grające na skrzypcach. Zamaskowane jako CHERUBY. Chodzi o uzasadnienie aury magicznej Przestrzeni w okolicy... oglądał jeszcze jakieś ckliwe romansidła, by się wdrożyć w klimat.

### Dzień 2:

Paweł przygotowuje magiczny zestaw słuchawkowy z portalem pośrodku (zaklęcie). Nie surowcem; to za prosta sprawa. Siluria poszła porozmawiać z Franciszkiem Mausem. Ma całuśnik. Musi go wypróbować ;-). Franciszek aktualnie się uczy rysować dinozaury; cieszy się z wizyty Silurii. Ta go pocałowała i... nagle... atrium.

* Najlepiej będzie, jak w jakieś dłuższe szczegóły porozmawiamy w Dziale Plazmowym lub w Rzecznej Chacie... próbowałeś kiedyś zdiagnozować psychikę AI? - zaciekawiona Siluria

Siluria wyjaśniła, że działają w ukryciu przed Quasar. Franciszek się zdziwił - mają AI i Quasar nie ma wiedzieć?

* Proszę... Silurio... powiedz, że to NIE jest prezent urodzinowy dla Quasar... albo forma swatania Quasar z AI... - Franciszek, błagalnie
* Nie. Obiecuję. - Siluria, z radością; ma nowy pomysł na przyszłość.
* To zamieniam się w słuch. - Franciszek
* Chodzi o Eis. - Siluria, dość poważnie
* Eis. Która zniszczyła Powiew. - Franciszek, upewniając się
* Ta sama. I która od diabli wiedzą ile siedzi w pudełku. - Siluria, smętnie
* W pudełku? Nie w Instytucie Technomancji?
* Nope. Siedzi grzecznie zamknięta w pudełku.
* To w jaki sposób KADEM... coś z nią robi? - Franciszek, nie rozumiejąc
* I to jest to dobre pytanie... KADEM... formalnie nic z nią nie robi. - Siluria, smętnie

To się nie mogło dobrze skończyć:

* To będzie akt... naukowo-rozrywkowy, ponieważ musimy odwrócić uwagę Quasar. - Siluria, wyjaśniając
* Prawda, że zawsze miałeś ochotę wybrać się na bardzo nastrojową randkę? - Siluria, z szerokim uśmiechem
* Silurio... nie chcesz chyba powiedzieć... - Franciszek, cały załamany
* We do what we must, because we can ;-) - Siluria, z determinacją

Franciszek się zgodził (-1 surowiec Silurii). Następnego dnia... on musi pogadać o konstruowaniu AI i o książkach i o stanach psychicznych AI...

Paweł poszerza pracę nad tą iluzją. Niech ten magazyn wygląda tak, jak wyglądał wcześniej. Druga rzecz, Paweł poszedł do magazynu Instytutu Zniszczenia i Technomancji; sformował grupę rzeczy disruptujących sensory. Chodzi o to, by ukryć randkę i schadzkę przed Quasar...

Siluria wydała DRUGI surowiec, by wyłączyć środek uodparniający Franciszka. Karolina się szeroko uśmiechnęła :D.

### Dzień 3:

Osoby "wiedzące" to: Ilona, Franciszek, Siluria i Paweł. Plan powoli, powoli się materializuje...

* Szansa, że Quasar nie wykryje co się NAPRAWDĘ dzieje i uzna to za nieodpowiedzialność: magiczny heroiczny -1: 16
* Szansa, że Quasar stwierdzi, że to nieistotne i nie potraktuje tego jako wyzwanie: zawodowy heroiczny: 10

Paweł zużył w sumie w cholerę surowców na tą akcję. Wpierw, zaklęcie na najniższej możliwej mocy bezprzewodowe słuchawki. Potem iluzja miejsca romantycznej schadzki z BAARDZO zbolałą miną Pawła. Ta iluzja ma ogromne tło magiczne by zakłócić. Echa magii przestrzeni - drony teleportujące się (cheruby) i jak ktoś próbuje je odgonić, teleportują się dalej. Trzeci surowiec to kwestia iluzji widocznej z zewnątrz; magazyn w stanie spoczynku. Ten magazyn jest pusty, nic się nie dzieje. Alisa jakby tu weszła, nic nie zauważy. Ostatnia rzecz - seria dronów zakłócająca systemy szpiegowskie i działania Quasar. A żeby to wszystko miało ręce i nogi, ta ostatnia seria jest nie skoncentrowana na Silurii a na Pawle. To ON nie chce być powiązany z tym, co tu się dzieje. On tu jest bo go trafiło. Bo Siluria się za ładnie uśmiechnęła. Mógł robić to lub być na miejscu Franciszka... ;-).

Oki, liczymy. Drony i iluzje: #5 narzędzia magicznego. Za okoliczności i sytuacyjne masz #5 narzędzia magicznego. I #1 za magię. Czyli... #11. Paweł: metodyczny, rekonstrukcje/mimikra, ScD -> 16. Czyli 16 vs 16. Sukces.

Quasar nie udało się dojść do tego, o co tak naprawdę toczy się gra. Czyli wygląda to na kolejny głupi prank KADEMu. Yet Another Stupid KADEM Thing.

Czas na drugi konflikt: 4 od Pawła. Do tego masz #5 za narzędzie magiczne (drony, iluzje, historia), #2 za Silurię i Franciszka: to tak wygląda, jakby Franciszek potrzebował "coś więcej". Sukces. Zbolała mina Pawła i cherubodrony przekonały Pawła. To po prostu... po prostu... KADEM. Quasar machnęła ręką i skupiła się na czymś innym; a zawsze jest coś, co wymaga Captain Quasar!

Tymczasem Siluria i Franciszek... na początku oboje się dobrze bawią, by po pewnym czasie Siluria zaczęła się bawić a Franciszek skupić się na pracy.

Siluria (zabawa) vs Karolina (neuronull): 4 vs 5#2 = sukces Karoliny. Franciszek może pracować bez malusa.

Franciszek próbuje przesłuchać Eis. Naturalna elita: 5. Warunki pracy: -1. Ma czas #1. Przygotowania do przesłuchania AI i o samej Eis: #3. = 8. (-1) -> 7. Poziom elitarny trudny. Eskalacja. Eis dostała informacje o tym z kim i jak ma do czynienia. Wynik: 8#3 -> 11. Poziom elitarny bardzo trudny. 

Co się dowiedzieli o Eis:

* Czego Eis w tej chwili chce? W tej chwili Eis chce przede wszystkim poszerzyć swoją wiedzę, ekspansja.
* Cel długoterminowy Eis: być wolną, autonomiczną i niezależną.
* Jej stosunek do KADEMu: oziębły. Nie kocha KADEMu, nie chce być w pudełku, nie jest KADEMowi wroga; realnie widzi, że nie jest w stanie wiele zrobić. Nie zaatakowałaby KADEMu. Ma też zdrowy szacunek militarny do KADEMu ;-).
* Czy informacje jakie ma jest tematyczny? Eis była już 3 razy na granicy zniszczenia. Nie chce być czwarty. Nie wie, co jest potrzebne do życia. Marzy o grach wojennych...
* Eis "boi się ciemności" - odcięcia od bodźców. Decyzja Ilony była rozsądna; to pokazało Eis, że KADEM nie jest jej w pełni wrogi.
* Eis nauczyła się od Alisy, że magowie też są czasem nieszczęśliwi
* Jeżeli Eis znajdzie sposób w jaki może się wyrwać nikogo nie krzywdząc, zrobi to.
* Eis cierpi na głód energii. Ma za mało energii, by uruchomić pełną moc. Jest śpiąca i głodna.

Paweł niepostrzeżenie podrzucił Eis "Kasparov ChessMaster". Niech się nie nudzi...

# Progresja

# Streszczenie

Po podłym pobiciu muzykanta przez Ignata, Balrog Bankierz rzucił mu wyzwanie. Siluria, bojąc się, że sromotna porażka Ignata doprowadzi do wojny gildii - ściągnęła Netherię i Melodię i po odrobinie przekonywania i podkupowania... powinno się udać. Ignat najpewniej przegra walkę, ale nie sromotnie ;-). Ilona Amant przyznała się Pawłowi Sępiakowi, że zrobiło jej się żal EIS i podała jej sensory, by ta nie oszalała sama. I teraz trzeba ją przebadać, bo Alisa się jej kiedyś żaliła. Paweł wciągnął Silurię, wciągnęli Franciszka Mausa, po czym Paweł zrobił epickie przedstawienie i dywersje i za plecami Quasar przesłuchali EIS... która okazała się nie-wroga KADEMowi, acz pragnąca wolności.

# Zasługi

* mag: Siluria Diakon, wymanewrowująca Balroga Bankierza przy użyciu kuzynek z Millennium oraz z soczystą randką w * magazynie z EIS... z psychologiem Franciszkiem Mausem
* mag: Paweł Sępiak, twórca planu i dywersji mylącej Quasar; za plecami ochrony KADEMu przesłuchali EIS tak, że nie mogła niczego uzyskać
* mag: Bogumił Rojowiec, który ostrzegł Silurię o tym, że Balrog Bankierz wyzwał Ignata; bardzo zmartwił się stanem KADEM x Kropiaktorium
* mag: Karolina Kupiec, zafascynowana narkotykiem Urszuli, przygotowała hedonistyczny neuronull dla Pawła (by dać Franciszkowi) po czym przygotowała na to samo antidotum dla Silurii. Bez jednej refleksji.
* mag: Infernia Diakon, zainteresowana KTO porwał Silurię, Siluriowa informatorka w sprawie Ignata i pomoc Karoliny by przebadać narkotyk Uli.
* mag: Urszula Murczyk, nieobecna, acz obiekt fascynacji Karoliny; jej narkotyki wyjątkowo dobrze współpracują z innymi * magami. Całkiem zdolna we współpracy.
* mag: Balrog Bankierz, oportunistyczny wojownik i showman, który zdecydował się wypłynąć i zdobyć popularność na fali podłego uczynku Ignata 
* mag: Ignat Zajcew, którego pobicie ulicznego muzykanta ściągnęło nań Balroga; też, którego sromotna i haniebna porażka do Balroga mogłaby rozjarzyć konflikt KADEM - Świeca
* mag: Netheria Diakon, ściągnięta przez Silurię by kaskadować imprezy które mają wymęczyć Balroga Bankierza (by nie wygrał z Ignatem zbyt mocno)
* mag: Melodia Diakon, ściągnięta przez Silurię, by wymęczyć Balroga Bankierza. Sympatyzuje z Balrogiem bardziej niż z Ignatem, ale nie chce wojny gildii.
* mag: Ilona Amant, która przechytrzyła Quasar całuśnikiem i innymi, dodała do EIS czujniki (bo było jej żal odciętego AI) po czym radośnie zrzuciła problem na Pawła Sępiaka. 
* vic: Eis, która ma dostęp do danych w "swoim * magazynie", jest nie do końca obudzona i nie w pełni mocy i ogólnie nie życzy źle KADEMowi... ale chce być wolna, niezależna i na pełnej mocy.
* mag: Quasar, to nie był jej dzień. Wpierw Ilona obeszła jej monitoring, potem za jej plecami Paweł i Siluria opracowali sposób, by Franciszek przesłuchał Eis. A ona nadal nic nie wie.
* mag: Franciszek Maus, psycholog Szaleństwa na KADEMie; przebadał EIS pod kątem jej szaleństwa oraz miał najdziwniejszą randkę z Silurią w życiu.

# Lokalizacje

1. Świat
    1. Faza Daemonica
        1. Mare Vortex
            1. Zamek As'caen
                1. KADEM Daemonica
                    1. Skrzydło Wypoczynkowe, gdzie znajduje się pokój Pawła... technicznie, wszystkie pokoje.
                    1. Skrzydło Wzmocnione
                        1. Pancerne Magazyny
                            1. Magazyn TECH_KN_15, gdzie odbyła się orgia dywersyjna mająca zamaskować komunikację z Eis
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Centrum
                        1. Prywatne Osiedle Mirra
                            1. Apartament Netherii Diakon, najlepsze miejsce do zastania Netherii w dobrym humorze.
                        1. Cyberklub Działo Plazmowe, gdzie Melodia i Siluria się spotkały i konspirowały przeciw Balrogowi Bankierzowi
                        1. Rzeczna Chata, gdzie odbywały się spotkania magów KADEMu mające wymanewrować Quasar

# Czas

* Opóźnienie: 1
* Dni: 3
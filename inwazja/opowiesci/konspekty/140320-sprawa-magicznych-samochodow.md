---
layout: inwazja-konspekt
title:  "Sprawa magicznych samochodów"
campaign: druga-inwazja
gm: żółw
players: kić, dust
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [140227 - Sophistia x Marcelin(AW, WZ, HB)](140227-sophistia-x-marcelin.html)

### Chronologiczna

* [140227 - Sophistia x Marcelin(AW, WZ, HB)](140227-sophistia-x-marcelin.html)

## Misja właściwa

To był dobry dzień dla prokuratora Hektora Blakenbauera. Siłom policyjnym udało się rozbić szajkę złodziei samochodów, więc on mógł zająć się tym, co lubi najbardziej - pracą. Podczas przesłuchiwania poszczególnych złodziei jeden szczególnie go zainteresował - niejaki Gustaw Siedeł. A przynajmniej tak twierdził, bo nie miał dokumentów.

Gustaw Siedeł nie tylko nie miał dokumentów, ale okazał się, ku utrapieniu Hektora anarchistą i to klasy Sophistii. Gorzej, znał Sophistię. A w pyskowaniu jej dorównywał, lub, co gorsza, przewyższał. Jak Hektor słyszał to jego "jesteś psem łańcuchowym ciemiężącego nas reżimu" to aż miał ochotę go potraktować surowiej...

Postawiony przed koniecznością rozmawiania z takim osobnikiem postanowił go przesłuchać. Zaproponował Siedełowi adwokata, ale ten odmówił. No cóż. Podczas tego przesłuchania Siedeł przyznał się ogólnie rzecz biorąc do współpracy z grupą przestępczą, "wskazywania celów" gangowi, częściowego przejęcia gangu i ogólnie wkopał się wprost modelowo.

Tak, to był dobry dzień dla Hektora Blakenbauera.

Ten dzień był znacznie gorszy dla Andrei Wilgacz. Dostała bowiem wiadomość od swojego kontaktu i bliższego kolegi, Gustawa Siedeła, że prokurator Blakenbauer się na niego uwziął i jest to co nieco irytujące. Samo w sobie to jeszcze nie byłoby dużym problemem, gdyby nie to, że Siedeł faktycznie wpakował się do grupy przestępczej i polował na pewne określone samochody...

Co?

Otóż, jak to Siedeł wyjaśnił, znalazł coś dziwnego. Jakieś magiczne, dziwne samochody. Same w sobie są praktycznie niewykrywalne czy to pod kątem magiczności czy to pod kątem tego co robią. Wygląda to na jakąś formę kombinowanego zaklęcia czy artefaktu, i to niesamowicie wysokiej klasy - poszczególne samochody nie robią niczego same w sobie, ale im więcej ich w pobliżu tym więcej własności uzyskują. Dlatego Siedeł postanowił zgromadzić ich więcej by zrozumieć ich naturę. Tu bowiem jest haczyk - samochody te muszą czymś się żywić i wszystko wskazuje na to, że żywią się energią ludzi w ich pobliżu. A Siedeł został wyrzucony z SŚ za radykalne poglądy (tak, da się) - jego zdaniem magowie powinni żyć w swoim świecie, ludzie w swoim a rzucanie zaklęć na ludzi to po prostu niegodna zbrodnia.

No i siedzi Gustaw Siedeł w areszcie, Hektor próbuje znaleźć na niego jakieś papiery czy dokumenty, oczywiście nic nie ma (Siedeł jak Sophistia, żyje poza "zbrodniczym systemem") i ogólnie Siedeł potrzebuje pomocy Andrei. Gdy ta to usłyszała to naturalnie złapała się za głowę. Pierwsze co zrobiła to ruszyła na czarny rynek sformować mu jakąkolwiek sensowną tożsamość i dokumenty (wyszło, że będzie ślusarzem). Drugie to zadzwoniła umówić się na spotkanie z Hektorem Blakenbauerem w jego rezydencji. I tak, ma spotkanie - wieczorem.

Przy okazji dowiedziała się, że nalot na dziuplę był zrobiony na bazie anonimowego donosu. Ten donos był całkowicie anonimowy, ale zdradzał szczegóły o grupie złodziei które wskazywały na to, że to insider lub ktoś dokładnie ich obserwujący.

Spotkanie Andrei z Hektorem było dość... ciekawe. Hektor nie ucieszył się, że złapał maga, ale ucieszył sie, że zbrodniarz zostanie osądzony. Andrea wprowadziła Hektora w sprawę magicznych samochodów i tego, że magowie podle sobie z ludźmi pogrywają, a Hektor na to stwierdził, że Siedeł wybrał niewłaściwe metody. Doszło do pewnego impasu, podczas którego oboje przerzucali się argumentami odnośnie wypuszczaniem lub osądzeniem Gustawa Siedeła. I na to wszedł Alfons, kamerdyner Blakenbauerów wprowadzając Sophistię (wyraźnie podsłuchującą pod drzwiami).

To bardzo zdenerwowało Hektora. Zapisał sobie, że ma skrzyczeć Marcelina - Sophistia nie ma prawa zbliżać się do jego gabinetu. Jednak jako że już tu jest to zabrała głos i zaproponowała wariant Stefana "Siary" Siarzewskiego - podłożyć kogoś. Pierwszy pomysł - podłożyć "coś z piwnic Blakenbauerów" został kategorycznie odrzucony. Drugi pomysł, Andrei, wynająć kogoś na czarnym rynku był akceptowalny. 

Pod warunkiem, że Hektor Blakenbauer się zgodzi. A on się zgodzi jedynie, jeśli Gustaw Siedeł zgodzi się wrócić do aresztu. Zostać sprawiedliwie osądzony i ukarany, wedle prawa ludzi. I znowu ogromna dyskusja z Andreą (i Sophistią, która dzielnie broniła Siedeła).

W wyniku tej skomplikowanej dyskusji ustalono co następuje:
- Hektor porozmawia z Siedełem i spróbuje go do tego wszystkiego przekonać.
- Siedeł został złapany w wyniku donosu; Hektor spróbuje znaleźć coś na temat nadawcy tego donosu.
- Albo to osobisty atak na Siedeła albo obrona dziwnych magicznych samochodów. W obu wypadkach, trzeba pogrzebać.

No i czas na dyskusję Hektora z Gustawe m Siedełem. Ta rozmowa była mniej przyjemna dla Hektora, gdyż Gustaw zgodził się, że wróci do tego miejsca, ale nie da się "sprawiedliwie osądzić", gdyż żaden reżim na świecie nie ma i nie będzie miał nad nim władzy. Zaznaczył, że on zainteresował się tymi samochodami przez przypadek i przejął gang jako ten, który chciał kupić różnego rodzaju towary (i wskazywał samochody). Mając aż cztery takie pojazdy w dziupli miał szanse znaleźć kolejne. Poinformował też Hektora radośnie, że on nie ma nad nim żadnej władzy. Jeśli będzie chciał odejść, odejdzie. Gdy Hektor zagroził mu ściganiem, Gustaw powiedział, że może być mężczyzną, kobietą, osobą w dowolnym wieku... Hektor go nie złapie. Prokurator potraktował to jak wyzwanie. Jednocześnie jednak zwolnił go z więzienia na parol; potrzebują Siedeła do rozwiązania tej sprawy.

Andrea porozmawiała z Siedełem i próbowała dopytać o kogoś, kto mógłby osobiście złożyć donos na Siedeła. Gustaw powiedział, że jest ktoś taki - tien Damian Bródka, który podziela Gustawowe poglądy w sprawie czarowania na ludzi ale tak jak Siedeł jest zwolennikiem braku praw tak Damian jest praworządny jak cholera. Ma kompetencje by śledzić Siedeła, więc wszystko wskazuje na to, że to może być on.

Tymczasem Hektor rozpuścił swoje prokuratorsko-policyjno-znajomościowe wici. Nie tylko miał okazję usłyszeć sam donos (głos kobiecy), ale i udało mu się posynchronizować informacje - donos pochodził z telefonu komórkowego na kartę (lub odpowiednika) i udało mu się powiązać lokalizację z jedną z kamer. Dostał obraz kobiety w prochowcu i kapeluszu w okularach p-słon, o ciemnych włosach. Cóż, raczej nie jest to tien Bródka. Mając obraz poszukał głębiej i okazało się, że taka osoba znajdowała się niedawno niedaleko parkingu policyjnego, uważnie obserwując samochody. Powiało chłodem.

Andrea skupiła się na badaniach samochodów na parkingu. Zaklęcie było niesamowicie skomplikowane; zdecydowanie przekraczające możliwości zrozumienia (a tym bardziej założenia) ze strony Andrei. Ta jednak zawzięła się i kosztem silnego bólu głowy odkryła coś ważnego - to jest artefakt złożony. Będzie istniało między 8 a 12 samochodów. Co więcej, charakterystyka magii jaką stworzono artefakt świadczy jednoznacznie o tym, że była z tym powiązana Academia Daemonica... i najpewniej sama sariath Trawens. Tknięta instynktem, wyszukała czy ktoś ją obserwuje i podgląda i udało jej się znaleźć z wielkim trudem wysoko w powietrzu niewidzialną dronę szpiegowską. Ta także miała magiczną sygnaturę skażoną AD.

Hektor opieprzył Marcelina za to, że Sophistia tak po prostu chodzi gdzie chce i podsłuchuje rozmowy których nie powinna. Ten przeprosił (no co miał zrobić? XD). Hektor chciał wykorzystać Ikę do znalezienia tajemniczej kobiety, ale stwierdził, że to niemoralne. No przecież Ika to dziecko, ludzka dziewczynka. Wypytał Marcelina o to, skąd Ika się wzięła w laboratorium Blakenbauerów - Edwin ją podobno kupił od Diakonów, z jednej z hodowli Diakonów. Hektor uniósł się gniewem, że nie chce by Ika była traktowana jak jakiś przedmiot i co to ma w ogóle być, na co nic nie świadomy Marcelin stwierdził, że tylko Hektor tak ją traktuje. On i Sophistia się z nią bawią, uczą ją różnych rzeczy a to Hektor jest tym który traktuje ją jak jednostkę tropiącą. Hektorowi zrobiło się nieco głupio.

Hektor przesłuchał Ikę - czy wie coś o swoim poprzednim życiu, czy ma jakieś wspomnienia, czy chciałaby wrócić do poprzedniego życia, ale cóż... prokuratorskim podejściem prawie doprowadził ją do łez. Nic nie pamięta, tu jej dobrze. Hektor się pożegnał z Iką i Marcelinem, na odchodnym prosząc Marcelina o sformowanie nowej jednostki tropiącej - ale ta ma nie być człowiekiem. I ma być normalna. Np. ma być psem.

Tymczasem Andrea przepuściła listę członków AD zanim AD było zniszczone pod kątem osób zdolnych do zrobienia takiej drony i w ogóle działań technomantycznych. Zastanowiła się szczególnie nad imieniem "Quasar". Tak, Quasar pasuje do wzoru. To mogłaby być ona. Ale co z tym zrobić teraz?

Jak nie wiadomo co zrobić, najlepiej się oczywiście spotkać. O dziwo, Quasar, czarodziejka KADEMu zgodziła się spotkać z Andreą. Już na wstępie jak tylko Andrea powiedziała że chodzi o magiczne samochody to Quasar powiedziała "Projekt AD do ochrony przed Inwazją, zastrzeżony, temat zamknięty. Co jeszcze chcesz wiedzieć skoro jeszcze tu jesteś". W tej rozmowie Quasar powiedziała, że ona (Quasar) i Irina Zajcew są odporne na korupcję ze strony Inwazji. Zapytana czy wie jaki mechanizm to powoduje, Quasar odpowiedziała całkiem uczciwie że mają pewne podejrzenia, ale jeszcze nie wiedzą. Quasar stwierdziła, że tylko ona wie jak działa ten system z samochodami i tak musi zostać - ona jest odporna na korupcję a pozostali nie.

Po tej rozmowie Andrea postanowiła zostawić sprawę. Cieszy to, że nie tylko oni kombinują nad walką z Inwazją, że mają potencjalnego silnego sprzymierzeńca i że "coś się dzieje". Gustaw Siedeł wrócił do aresztu (gdzie zdaniem Hektora jego miejsce a zdaniem Andrei będzie chwilowo bezpieczny) a temat został zamknięty. Samochody wróciły na "wolność".



"Chciałbym, by Ika przestała być traktowana jak przedmiot" - Hektor, do Marcelina, wściekły na stworzenie takiej istoty tropiącej.
"Ale... ale tylko Ty ją tak traktujesz..." - Marcelin, nie rozumiejący o co Hektorowi chodzi; on i Sophistia się małą opiekują a on tylko używa na akcje.

"Wystarczająco, by ktoś się zainteresował" - Hektor do Gustawa w temacie samochodów.
"Ktoś jest w więzieniu" - cięta riposta Gustawa wskazująca na działanie przeciwnika.

![](140322_SprawaSamochodow.jpg)

# Zasługi

* mag: Hektor Blakenbauer jako prokurator, który wpierw ma dobry dzień i przymyka Siedeła a potem ten dzień robi się dużo gorszy, bo Sophistia...
* mag: Andrea Wilgacz jako osoba negocjująca z Hektorem uwolnienie Siedeła a potem rozwiązująca tajemniczy artefakt złożony w formie samochodów (robota Quasar)
* mag: Gustaw Siedeł, Wieczny Anarchista kumplujący się z Sophistią i mający na nią oko który przejął organizację przestępczą do ochrony ludzi przed... samochodami.
* mag: Damian Bródka, czarodziej o podobnych poglądach do Gustawa Siedeła ale bardzo praworządny.
* czł: Amelia Eter, Sophistia, która bezczelnie podsłuchuje pod drzwiami Hektora w sprawie Siedeła i ma kilka ciekawych pomysłów jak z tego wybrnąć.
* mag: Marcelin Blakenbauer, który przepraszał Hektora za podsłuchującą Sophistię, lecz powiedział mu, że tylko Hektor traktuje Sophistię jak narzędzie.
* vic: Ika, która popłakała się gdy Hektor chciał jej pomóc i przesłuchiwał ją na okoliczność jej przeszłości. Jej dobrze z Marcelinem.
* mag: Quasar, która stoi za magicznymi samochodami i ucięła Andreę gdy ta chciała się czegoś dowiedzieć, bo to plan przeciw Inwazji a Quasar jest odporna na Inwazję. Aha, złożyła donos na Siedeła.
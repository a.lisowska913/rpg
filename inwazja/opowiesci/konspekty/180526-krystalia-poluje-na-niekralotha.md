---
layout: inwazja-konspekt
title:  "Krystalia poluje na niekralotha"
campaign: adaptacja-kralotyczna
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [180402 - Pętla dookoła niekralotha](180402-petla-dookola-niekralotha.html)

### Chronologiczna

* [180402 - Pętla dookoła niekralotha](180402-petla-dookola-niekralotha.html)

## Kontekst ogólny sytuacji

### Opis sytuacji

* brak

## Punkt zerowy

![Rysunek inicjujący Opowieść](Materials/180499/180526-init.png)

Poprzednie plany (z tych istotnych):

* Krystalia Diakon: ktoś nadużył jej dobrego imienia. Osobiście zaangażuje się w znalezienie Bójki, bogowie i magowie be damned...
* Roman Bruniewicz: poprowadzić Ogniste Niedźwiedzie do Klasztoru Zrównania. Wyjaśnić im, że tak się nie godzi przez mały Zastrasz.
* Rafael Diakon: ostrzec Silurię przed tym, że Krystalia próbuje znaleźć Bójkę. Krystalia może Silurii bardzo napsuć.
* Patrycja Widoczek: nie chce odejść z Ognistych Niedźwiedzi. Chce je zmienić na "dobry gang".
* Prokuratura Magów: przyjrzeć się tematowi Marii Przysiadek i Hralglanatha. Nie dopuścić do złamania Maskarady. Odbudować życia zniszczone przez kralotha.
* Ogniste Niedźwiedzie: przeprowadzić działania przeciwko Klasztorowi Zrównania. Mały Terror Site.

## Potencjalne pytania historyczne

* Czy dojdzie do konfliktu między Świecą i Millennium o Krystalię i Kromlana?
* Czy Krystalia osiągnie swój cel i zmusi Glarnohlagha do działania?
* Czy Krystalia unieszkodliwi Eleę tylko dlatego, bo ta stoi jej na drodze?
* Czy Krystalia podpadnie Prokuraturze Magów?

## Misja właściwa

TIMING_INIT: (20:42)

**Dzień 1**:

Rafael Diakon ostrzegł Silurię - Krystalia się zainteresowała tematem. Szykuje się potencjalna afera polityczna. Oczywiście. Coś tam kombinowała w biolabie a potem opuściła Las i poszła do Jodłowca.

"Krystalia niekoniecznie będzie wroga Silurii... w końcu też na tego poluje. Tyle, że Krystalia ma podejście typu 'you WILL wither'; Siluria jest fanką samokontroli".

Czas wykorzystać Kromlana. Siluria nie ma nad nią kontroli a Krystalia ma historię, że niewinnym działa się krzywda (20:50). Może Shadow i Midnight z KADEMu? (20:55) W końcu Siluria skontaktowała się z Kromlanem i powiedziała, że Krystalia weszła do akcji polować na drugiego kralotha. A nie ma historii minimalizacji szkód. Niech Świeca się tym zajmie. Kromlan spytał logicznie, czemu nie Millennium lub KADEM? Siluria odpowiedziała smutno, że Krystalii NIKT nie kontroluje a on jest na miejscu...

Kromlan obiecał, że będzie miał oko na ludzi by im się nic nie stało. I spróbuje zatrzymać Krystalię. A przynajmniej minimalizować szkody. (21:00)

Siluria nawiązała kontakt z Krystalią. Krystalia przyjęła ją lekko chłodno; jest zajęta. Przygotowuje pułapkę na niekralotha, w Jodłowcu. Ten kraloth - zdaniem Krystalii - jest defilerem a ONA wie jak to zrobić, by kraloth ucierpiał z głodu. Zaprosiła Silurię do Jodłowca. Ok. Nowy plan. Siluria ODWRACA UWAGĘ Krystalii. Czas na kontakt z Łajdakiem...

Łajdak zdębiał gdy się dowiedział, że ma przywieźć do Jodłowca zwłoki kralotha. No ok, czego się nie robi. Jak dowiedział się, że chodzi o Krystalię, zrozumiał... za 2h będzie... (21:13).

Siluria dotarła do Jodłowca. PRZED kralothem. I przed nią - piękny widok. Kromlan i Krystalia, ostro się ścierający słownie (na razie). To jest, Krystalia nadal COŚ robi. Kromlan nie odważył się jej przeszkadzać. Czeka na wsparcie.

Siluria wyciągnęła od Krystalii, że ta zbudowała "cukierki" z ludzi, wyjątkowo smacznych ludzi którzy aż przyciągają kralothy. Kilkudziesięciu nawet. Kromlan zbladł i wezwał wsparcie. Siluria z WIELKIM trudem przekonała Krystalię, by ta pomyślała o opcji alternatywnej - krwi Silurii do znalezienia Bójki, bo kraloth może być na Fazie. Test Typowy - a Krystalia widzi Silurię jako naukowca a nie playgirl, więc ufa jej planom (+1). Reroll. Krystalia właśnie ma w pełni odwróconą uwagę, ALE zmienieni już ludzie są zmienieni i Krystalia będzie pilnować, by BYLI zmienieni.

To już zmartwienie Kromlana, który próbuje ich upolować i przekazać Elei... (21:32)

Krystalia i Siluria rzucają czar kombinowany mający na celu znaleźć Bójkę. Trudny czar, skupiony na zlokalizowaniu jej między Fazami. Zaklęcie niestety nie dało rady zlokalizować Bójki (porażka); za to analiza Krystalii wyniku zaklęcia pokazała, że jeśli Cukierki znajdą to miejsce, to da się dostać do Bójki przez ofiary z ludzi. Siluria nie jest szczęśliwa z tej odpowiedzi... (21:41). Jest na innej Fazie, najpewniej Daemonica. Teraz Krystalia jest święcie przekonana, że ma rację i jej plan działa.

...na szczęście dotarł martwy kraloth w vanie. W jakimś domku, w piwnicy. Krystalia się ucieszyła i zapomniała o innych planach; musi wpierw zrozumieć TEGO kralotha i jego śmierć. Jak długo nic nie dotknie jej agentów, tak długo jest zneutralizowana na jakiś czas.

Korzystając z neutralizacji Krystalii, Siluria poprosiła o wsparcie Shadowa i Midnight (Sławka i Maję). Magowie AD byli zafrasowani, to nie jest łatwa sprawa, by znaleźć maga na Fazie... cóż, AD nie takie rzeczy już robiło, czas po prostu przypomnieć sobie jak to szło...

**Dzień 2**:

W międzyczasie Elea opracowała antyśrodki pomagające ludziom zarażonym przez Krystalię. Krystalia się zirytowała, bo spadła ilość Cukierków, więc wezwała Silurię i jej o tym powiedziała. Dała jej anty-antidotum i wysłała na korektę Cukerków i znalezienia osoby odpowiedzialnej.

Siluria dotarła do Elei. Elea się po prostu przyznała - tak, to ona. Nie może pozwolić na cierpienie ludzi, jest lekarzem. W rozmowie między czarodziejkami, Elea się zgodziła by Siluria skierowała uwagę Krystalii na siebie. A Siluria "zgubiła" trochę antidotum, by Elea mogła je znaleźć.

Kromlan przygotowuje bardzo ciężką walkę przeciwko Krystalii; ściągnął pięciu konstruminusów, autoryzowany przez Prokuraturę Magów (Juliana Strąka).

Siluria rozprzestrzeniła anty-antidotum, by zarazić wszystkich ludzi Cukiereczkowością, by Krystalia była odpowiednio zadowolona. I Krystalia JEST odpowiednio zadowolona. Zapytana przez Silurię jak rozpoznaje którzy ludzie są w jakim stanie, Krystalia powiedziała, że sprzęgła ich ze sobą przez Krew. Dodała też, że skutecznie wzmocniła środek; Elea nie powinna poradzić sobie ze zdekombinowaniem. Siluria dyskretnie poprosiła Norberta by Elei pomógł. Norbert Elei pomoże ;-).

Krystalia powiedziała Silurii, że udało jej się dojść do dość ciekawych wniosków. Niekraloth próbował nie zabić Hralglanatha a przejąć jego ciało. Czyli niekraloth próbuje wrócić do bycia kralothem. Interesujące - ma też dowód, że ów jest defilerem. Czyli Cukiereczkowość zadziała. Sigh od Silurii.

Siluria zrobiła deal z Krystalią. Wykorzystają ciało Hralglanatha by weń inkarnować Glarnohlagha. Krystalia może go badać i się nim bawić, ale finalnie dostanie go całego i zdrowego Siluria. Krystalia się zgodziła - mają deal. Jak długo wróci stabilny, tak długo Krystalia może zeń cokolwiek (informacje) ekstraktować. (22:27)

Łajdak skontaktował się z Silurią. Coś się dzieje w lesie. Ludzie biją się z ludźmi. Krystalia też to wyczuła i powiedziała Silurii, że Elea nasłała ludzi do bicia jej Cukierków. Niech Siluria to dla niej rozwiąże.

Franciszek Knur i inne Niedźwiedzie biją się z Cukierkami. To nie wygląda na ustawkę; to po prostu bitka. Koło 10 Niedźwiedzi i 10 Cukierków. Siluria sprawdziła z Krystalią - Cukierki nie mają poleceń walczyć. Więc zaczepiła Knura (którego jeszcze kojarzy z Mordowni). Co tu się dzieje. Niedźwiedzie wygrywają. Knur najpierw zaśmiał się, bo może piwnicy tu nie ma ale jest trawka - a potem spoważniał. Siluria chciała z niego wyciągnąć co tu się dzieje. Skąd są "delegacją". Bruniewicz poszedł do klasztoru, potem ci tutaj zaczęli przyłazić i się dziwnie zachowują. I kazali Niedźwiedziom sobie iść. I to ONI zaatakowali, a nie Niedźwiedzie.

Krystalia, zapytana czemu są grupą się ucieszyła. To znaczy, że znaleźli kralotha; kraloth ich do siebie przyciągnął. Wróci potem; wpierw musi rozwiązać problem z Eleą. Z tego co Knur mówi, Bruniewicz poszedł do klasztoru. Oni czekają aż wróci - jak NIE wróci, wejdą tam i go odbiją.

Tymczasem w zupełnie innym miejscu, Elea stanęła naprzeciw Krystalii i nie miała żadnych szans. Dała jedynie znać Kromlanowi, który uderzył w przychodnię swoimi siłami... chwilę później Siluria straciła kontakt zarówno z Krystalią (pokonana przez siły Świecy i Prokuratury) jak i z Eleą (pokonana przez Krystalię).

Siluria spingowała szybko Norberta - co tam się stało? I czy może przygotować antidotum na ludzi by ich wyciągnąć? Norbert spokojnie odpowiedział, że co prawda go tam nie było, ale z tego co wie - Kromlan, Inkwizytor Prokuratury Magów i skrzydło konstruminusów uderzyli w Krystalię. Elea była pułapką na Krystalię.

Norbert dostał zadanie zrobić antidotum na szkodliwe składniki Krystalii a Siluria zaczęła pracować nad Knurem. Jeśli wycofa Niedźwiedzie do Mordowni, to ona pójdzie z nimi i Siluria pójdzie z Knurem do łóżka. Co może spotkać złego Bruniewicza w klasztorze pełnym KOBIET? Oki, poszli z Silurią. I sprawa zaczyna się czyścić... a biedny Bruniewicz gdzieś zniknął.

![Rysunek z konfliktami Opowieści](Materials/180499/180526-konflikty.png)

## Wpływ na świat

* Żółw: 14
* Kić: 7

Czyli:

Gracze:

* Wzmocnienie Silurii przez Kromlana (osłona przed prokuraturą magów)
* DOCELOWO jest gwarancja tego, że Shadow + Midnight odbiją Bójkę od niekralotha (za dwa tygodnie?)

MG:

* Sprawa Krystalii pogorszyła status Millennium jako gildii - nie pilnują swoich
* Krystalia zdecydowanie podpadła Prokuraturze Magów
* Krystalia zmusiła Glarnohlagha do działania

# Streszczenie

Krystalia weszła do Jodłowca, by przekształcić ludzi w żywe przynęty dla kralotha. Silurii udało się odwrócić jej uwagę, ale nie dość. Krystalia zlokalizowała niekralotha i powiedziała kilka nowych rzeczy; jednak Elea pomagając ludziom szkodziła planom Krystalii. Krystalia poszła ją wyłączyć z akcji (z powodzeniem), po czym wpadli terminusi Kromlana i Strąka oraz unieszkodliwili Krystalię. A Bruniewicz poszedł do Klasztoru i ślad po nim zaginął.

# Progresja

* Siluria Diakon: Elea jest nią rozczarowana, bo nie stanęła przeciwko Krystalii (czyt. była rozważna politycznie)
* Siluria Diakon: Marek Kromlan osłonił ją przed prokuraturą magów (sprawy z Krystalią).
* Elea Maus: rozczarowana Silurią; Siluria może i wszystkich ostrzegała, ale jednak nie stanęła po właściwej stronie - przeciw Krystalii
* Elea Maus: Marek Kromlan w końcu ją polubił. Nie jest dla niego "Mauską" a "Eleą, która ryzykuje dla ratowania ludzi"
* Marek Kromlan: polubił Eleę Maus - jest słonna dużo poświęcić dla bezpieczeństwa ludzi, stanęła na linii ognia Krystalii
* Krystalia Diakon: bardzo podpadła Prokuraturze Magów; jest uznawana za groźniejszą niż Karolina Maus

## Frakcji

* Millennium: ma zdecydowanie pogorszoną opinię przez Krystalię. Co jest warta gildia nie pilnująca swoich członków?

# Zasługi

* mag: Siluria Diakon, skutecznie odwracała uwagę Krystalii i kompensowała jej ruchy. Wykorzystała Kromlana do rozwiązania problemu z Krystalią i obroniła się politycznie w tej sytuacji.
* mag: Krystalia Diakon, niewiarygodnie wręcz skuteczna życiokształtniczka; wynalazła sposób zlokalizowania i pokonania niekralotha. Pokonała Eleę, która stała jej na drodze. Złapana przez Prokuraturę Magów.
* mag: Elea Maus, nie zgodziła się na traktowanie ludzi jako przynętę na niekralotha i zwalczała środki Krystalii. Padła, gdy Krystalia przyszła do niej osobiście.
* mag: Marek Kromlan, próbował powstrzymać Krystalię łagodnie i nie wyszło. Wezwał wsparcie z Prokuratury Magów i miał już dość siły.
* mag: Julian Strąk, z prokuratury magów; pomógł Kromlanowi i wjechali na Krystalię ze skrzydłem konstruminusów.
* mag: Norbert Sonet, wezwany przez Silurię jako wsparcie Elei do pomocy w naprawianiu szkód wyrządzonych przez Krystalię. Potem pomagał Elei i Krystalii po akcji terminusów.
* mag: Marian Łajdak, paser nieziemskiej klasy. Przetransportował zwłoki kralotha (Hralglanatha) do Jodłowca, by odwrócić uwagę Krystalii od ludzi.
* czł: Franciszek Knur, wyznaczony na dowódcę grupy Niedźwiedzi w czasie gdy Bruniewicz poszedł do Klasztoru. Siluria ściągnęła go z niebezpiecznego miejsca obietnicą łóżka.
* czł: Roman Bruniewicz, chciał odzyskać dobre imię i dobrą opinię a wpadł w ręce niekralotha w Klasztorze Zrównania.

# Plany

* Glarnohlagh: jest zmuszony do natychmiastowego działania przez Krystalię; pętla dookoła niego się zaciska.
* Julian Strąk: chce znaleźć dowody na zło Krystalii Diakon by móc ją porządnie oskarżyć i skazać; nie jakieś byle co.

## Frakcji

# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Piróg Dolny
                    1. Pylisko
                        1. Przychodnia Larent, miejsce uzdrowienia ludzi (przez Eleę) i miejsce starcia między Krystalią i Eleą - a potem grupy szturmowej kontra Krystalia
            1. Powiat Czelimiński
                1. Jodłowiec
                    1. Klub Kolt, miejsce, gdzie Krystalia konwertowała ludzi w agentów mających usunąć Glarnohlagha
            1. Las Stu Kotów
                1. Okolice Piroga Dolnego, doszło do małej bijatyki między agentami Krystalii a Ognistymi Niedźwiedziami niedaleko Klasztoru Zrównania

# Czas

* Opóźnienie: 1
* Dni: 2

# Narzędzia MG

## Cel Opowieści

* Sprawdzić nową mechanikę magii, nowe karty 1805 i nową generację sesji
* Timing test

## Po czym poznam sukces

* null

## Wynik z perspektywy celu

* null

## Wykorzystana mechanika

1805

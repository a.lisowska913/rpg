---
layout: inwazja-konspekt
title: "Krzywdzę, bo kocham"
campaign: prawdziwa-natura-draceny
gm: żółw
players: foczek, raynor, kić, dust
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [170725 - Krzywdzę, bo kocham (4*temp)](170725-krzywdze-bo-kocham.html)

### Chronologiczna

* [150329 - Knowania Izy(SD, PS)](150329-knowania-izy.html)

## Kontekst ogólny sytuacji

Po wydarzeniach z Draceną, Izą, Antygoną itp. Dracena została wycofana przez Draconisa i dobrze schowana. Mimo starań Draconisa, Dracena została oddana pod "ochronę" (i jej i przed nią) 2 terminusów i 2 konstruminusów.

Silgor Diakon nie mógł się na to zgodzić. Dracena jest jednocześnie zbyt niebezpieczna i zbyt niepasująca do rodu. Tak po prostu nie powinno być. Jeśli Draconis nie potrafi kontrolować swojej "córki", jeśli ona nie chce być częścią rodu... to trzeba doprowadzić albo do sytuacji, w której będzie częścią rodu albo przestanie być zagrożeniem. Wszystko dla rodu.

## Punkt zerowy:

**Scena startowa**:

Dracena zwiała. 2 konstruminusy i 2 terminusów w pościgu. Znaleźli ulicznicę; jeden z terminusów użył magii mentalnej i ją zniszczył, ALE dowiedział się gdzie pojechała Dracena.
Konstruminusy wzbiły terminusów w powietrze. Magowie załatwili przejazd kolejowy gdy już zlokalizowali pojazd. Gdy mieli odpowiednią wysokość, Barnaba walnął artylerią. Samochód uszkodzony, pociąg uszkodzony.

Dracena się wydarła, próbując ratować umierającego człowieka. Konstruminusi dali radę go utrzymać przy życiu, porazili Dracenę jeszcze raz i pozbawili ją przytomności. Okazało się, że wyrwała się szczeniaczkowi, bo się naćpała. The WHAT?

**Stan początkowy**:

1. Uwolnić Dracenę
    1. S Draconis traci na znaczeniu w Millennium; jest słabszy i bardziej podatny.    : 0/10
    1.   Barnaba rośnie w potędze i znaczeniu w Millennium dzięki różnym działaniom.   : 0/10
    1.   Draconis jest ciężko ranny przez Lustro. Powoli regeneruje.           : 0/10
    1. D Dracena staje się persona non grata w Rodzie.     : 0/10
    1.   Barnaba poluje na Ackmanna i ekipę.   : 0/10
    1.   Echo Skażenia, Lustro spowoduje echo Lustra : 0/10

**Pytania i odpowiedzi**

* Ż: Co sprawia, że Dracena nie jest w stanie opuścić samego miasta? Słaba strona.
* K: Jest trzymana w lekko przepalonych kanałach magicznych; jej poziom mocy jest zdecydowanie ograniczony.
* Ż: Jakie wsparcie ma Dracena w Jodłowcu? Silna strona, niezauważona przez Zło.
* D: Specjalny typ viciniusa który wspiera ją mentalnie. Mały puchaty szczeniaczek oddziałujący na nią pozytywnie, ładujący w nią pozytywne myśli. Błogość i bezradność.
* Ż: W jakim miejscu w Jodłowcu znajduje się bardzo strzeżona czarodziejka?
* R: Znajduje się w fabryce czołgów, która ponoć nie produkuje żadnych czołgów.
* Ż: Nazwij najlepszy marny cyberklub w Jodłowcu. Kilka słów o tym miejscu.
* F: Czerwony Kogut. Knajpa stylizującą się na ekskluzywną, ale to zwykły kicz. Złotoplastikowe zasłony, czerwonoobrzygany dywan. Nie chodzą osoby z wyższych kręgów. Stolik z ceratą ze złotym obszyciem.

## Misja właściwa:

**Dzień 1:**

_Rano: (+1 akcja O)_

Piękny dzień. Mateusz księgujowuje w swoim home office. Ma gościa - Dytę. Dyta skończyła w łóżku z Mateuszem. Tam przekazała informacje o tym co się działo. Mateusz się zgodził... są gotowi na akcję. Dama w opresji. Ma za zadanie przekazać przez dziewczynę lustro jej opiekunowi.

Ekipa zebrana. Jodłowiec. Wleźli do hotelu Jodłowiec. Tam mają bazę.

_Południe: (+1 akcja O)_

Pojechali rozejrzeć się w fabryce czołgów. Co jest tam i jak wygląda. Jest tam strażnik; nie chce dotknąć ani przeczytać. Ma inne rozkazy. Przekazano mu małża Poszukiwacza; małż strażnika zdominował - ale Fryderyk zauważył kamerę (SS). Gdy "Strażnik" poinformował szefostwo o tym, że pojawiła się inspekcja ze skarbówki, Henryk (terminus) stwierdził, że to jest niefortunny problem. On zejdzie na dół się tym zająć.

Gra na czas. Małż Mateusz szybko wpełzł w swoim crawlerze do kamery i ją natychmiast przełączył w loop, by nic się nie nagrało (SS). Jednak coś nie do końca się spodobało konstruminusowi, więc także zdecydował się zejść na dół.

Henryk zszedł pierwszy. Dwóch magów walnęło w niego zaklęciami mentalnymi by rozsypać mu tarczę a małż Poszukiwacz go Dotknął. Henryk nie miał większych szans, został zdominowany.
Strażnik się ocknął. Małż Mateusz spadł z sufitu, walnął go w głowę swoim crawlerem i go ogłuszył.

Dane małża Mateusza wskazały, że zbliża się konstruminus. Więc małż Poszukiwacz dominujący maga Henryka kazał mu zdominować magów pozostałych, by aura była poprawna. Gdy konstruminus wszedł, zauważył aurę Henryka dominującego wszystkich innych i jednego nieprzytomnego strażnika. Małże się pochowały.

Zdominowany Henryk wydał rozkaz konstruminusowi - ma wziąć nieprzytomnego strażnika i schować go za fabryką. On (Henryk) rozwiązał już sprawę. Konstruminus próbował się oprzeć i zareagować zgodnie z profilem ryzyka, ale nadrzędne programowanie było nadrzędne. Zaniósł strażnika za fabrykę czołgów... i dostał kombinowanym zaklęciem dominacyjnym: Henryka, Małża Mateusza, Fryderyka i Mateusza Ackmanna.

Zaklęcie stworzyło gigantycznej mocy Skażenie dotykające cały Jodłowiec. Pozostały mag - Barnaba - spanikował. Natychmiast zaczął wzywać posiłki by poradzić sobie ze Skażeniem. Konstruminus został wysłany by uniemożliwić Skażeniu rozpełznięcie się. To spowodowało, że Zespół mógł po prostu wparować i porwać nieco nieprzytomną Dracenę...

**Dzień 2:**

Dracena się obudziła. Jest nieco zdziwiona. Wie, że nie tak miała sytuacja wyglądać; nie powinna być tak przetrzymywana i traktowana. Ale wie też o straszliwym Skażeniu które nie powinno mieć miejsca. Powiedzieli jej o lustrze, które miała dać Draconisowi. Dracena się zaśmiała i powiedziała, że nie ma zamiaru tego robić.

Ackmann chce, by im zapłacono. Dracena zauważyła delikatnie, że wszedł w kontrakt z viciniusem. W świetle magów nie ma to prawnego znaczenia. Ackmann i Fryderyk się i tak spotkali z Dytą, która powiedziała, że jest ogromne Skażenie itp. Nie do końca to co ona chciała. Ale Dracena jest wolna. Więc... Dyta powiedziała, że nie wypłaci im nagrody - ale pomoże im się schować. Ackmann w szoku, Fryderyk wynegocjował część wynagrodzenia i usunięcie śladów śladczących o nich. Dyta się zgodziła.

A małże uciekły z konstruminusem i Henrykiem Gwizdonem.

# Progresja

* Henryk Gwizdon: zniewolony przez małże.
* Draconis Diakon: straszliwy cios w politykę, reputację itp. przez Silgora i Dracenę.
* Barnaba Łonowski: uznany za bohatera za próbę uratowania Jodłowca i szybkie myślenie.
* Mateusz Ackmann: +1 surowiec od Diakonów (Silgor)
* Fryderyk Bakłażan: +1 surowiec od Diakonów (Silgor)

# Streszczenie

Silgor próbował rozwiązać problem Draceny jako 'pistoletu' zagrażającego rodowi i wysłał Dytę, by ta znalazła siły mogące ją wyciągnąć i uszkodzić politycznie Draconisa. Udało się nadspodziewanie dobrze - dwa małże zniewoliły maga i z nim uciekły, Jodłowiec ma ciężkie Skażenie kiczo-kogutowo-ceratowe, Dracena uciekła a on sam wiele płacić nie musiał... 

# Zasługi

* mag: Mateusz Ackmann, przeleciał wiłę, zebrał ekipę, pojechał, doprowadził do największego Skażenia w historii Jodłowca i stracił o większości pamięć dla bezpieczeństwa.
* mag: Fryderyk Bakłażan, wynegocjował faktyczną zapłatę i nie przeleciał wiły. Brał aktywny udział w Skażeniu Jodłowca. Stracił większość pamięci dla bezpieczeństwa.
* vic: Małż Poszukiwacz, małż. Skakał między ciałami przez dotyk. Z pomocą reszty zdominował maga, Skaził Jodłowiec i uciekł z ciałem maga.
* vic: Małż Mateusz, też małż. Zdominował konstruminusa, shackował kamerę i grawitacyjnie ogłuszył krzepkiego ochroniarza. Skaził Jodłowiec i uciekł z konstruminusem.
* mag: Silgor Diakon, próbował rozwiązać problem Draceny i Draconisa z miłości dla rodu. Nie do końca mu wyszło, ale Dracena nie będzie już problemem.
* vic: Dyta, piękna wiła, mistrzyni negocjacji ciałem. Miała za zadanie uwolnić Dracenę i doprowadzić do problemów Draconisa. Udało jej się. Niestety, musiała zapłacić.
* mag: Dracena Diakon, próbowała uciekać, ale bez sukcesu. Narkotyzowana i unieszkodliwiona min. szczeniaczkiem. Uratowana i puszczona wolno przez Zespół. Zdziwiona.
* mag: Barnaba Łonowski, pseudonim "krzywa katiusza", technomancja, elementalna i materia. Dalekosiężny miłośnik artylerii i dewastacji ze Świecy. W odpowiednim momencie wezwał posiłki.
* mag: Henryk Gwizdon, zabił ulicznicę i zdominował go małż. Wystarczy. Jego przyszłość MOŻE zawierać randomowe bajorko.

# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Czelimiński
                1. Jodłowiec, ze straszliwym Skażeniem kiczo-kogutowo-ceratowym...
                    1. CityHotel Jodłowiec, w którym 2 magów i 2 małże zrobili sobie bazę. I który jest popularnym hotelu w Jodłowcu.
                    1. Fabryka Czołgów, która niby nieczynna, a jednak przetrzymywana jest tam Dracena. Nie produkuje się tam czołgów acz jest dobrze ufortyfikowana.
                    1. Knajpa Czerwony Kogut, Knajpa stylizującą się na ekskluzywną, ale to zwykły kicz. Złotoplastikowe zasłony, czerwonoobrzygany dywan. Nie chodzą osoby z wyższych kręgów. Stolik z ceratą ze złotym obszyciem.

# Czas

* Opóźnienie: 10
* Dni: 2

# Narzędzia MG

## Opis celu misji

-

## Cel misji

-

## Po czym poznam sukces

-

## Wynik z perspektywy celu

-

## Wykorzystane tabelki

Postać (strona gracza):

| .Motywacja. | .Siły i słabości. | .Specjalizacja. | .Umiejętność. | .Szkoła Magii. |.POSTAĆ. |
|             |                   |                 |               |                |         |

Opozycja (strona NPC):

| .VERSUS. | .Aspekty. | .Skala. | .Magia. |.Pomysł gracza. | .OPOZYCJA. |
|          |           |         |         |                |            |

Wynik:

| .Postać. | .Opozycja. | .Rzut.    | .Wynik.  |
|          |            | xx: +x Wx |   S-SS-F |

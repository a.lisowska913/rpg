---
layout: inwazja-konspekt
title:  "Cel uświęca środki"
campaign: anulowane
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja:

Misja dzieje się w Millennium i wprowadza nową postać, agentkę Paulinę Rejtan. Jest jej pierwszym zetknięciem z magiem, Robertem Dzierżawcem, którego celem jest oczyszczenie Millennium z Inwazji...
 
 
### O DZIERŻAWCU:

- kim on jest i czym się interesuje +
- co o nim widać na zewnątrz +
- coś co mu się spodoba na miły początek współpracy +


### SPOTKANIE Z DZIERŻAWCEM:

- skonfliktowany: wpadła w kłopoty, ale definitywnie zaskarbiła sobie zaufanie Roberta Dzierżawca.
- zakupiła też jednorazową tarczę kinetyczną dla terminusa. Może się jej przydać.
- sprzągł różdżkę sygnaturową z Pauliną by móc tworzyć dla niej artefakty.
- dowiedziała się, że w Afganistanie niesprzęgnięta różdżka zniszczyła wioskę użyta przez innego maga.
- nauczyła się jego systemu kodowania różdżek. Dzierżawiec jest pod wrażeniem.


### OPERACJA 'DEFILER ŻYWCEM':

- Dzierżawiec powiedział, że defiler jest rdzeniem nowej broni i potrzebuje nie-viciniusa defilera. Żywego.
- dowiedziała się, że Inwazja ma metodę na ukrycie szpiega, którą można obejść przez coś, co sprawia, że można komuś owego "szpiega" usunąć
- dowiedziała się po konflikcie z Dzierżawcem, który nie chciał się zdradzić, ale przekonała go jako, że udało się jej wcześniej zdobyć jego zaufanie
- no i pomyślała sobie, że ona mu nie ufa a skoro on twierdzi, że można szpiega usunąć tą metodą to może da się również szpiega wstawić
- więc zaczęła go sprawdzać i dostała telefon "z góry" z informacją, że on jest "pewny" a jeśli coś złego z tego wyjdzie, to ona nie będzie mieć kłopotów. Stoi za tym Edward Diakon, osobiście.


### POSZUKIWANIE WŁAŚCIWEGO DEFILERA:

- Paulina rozpuściła wici po Millennium w poszukiwaniu właściwego defilera. Skontaktowała się z nią Ilona Szklanka, która miała sugestię odpowiedniego defilera. Ale chce za to pomocy Pauliny w akcji, której Paulina nie chciałaby wykonać. Paulina powołała się na to, że góra (Edward Diakon) jest zainteresowana powodzeniem tej misji. Trudny test został zdany w formie skonfliktowanej - Paulina dostanie namiar na defilera, ale pomoże Ilonie w akcji szmuglowania niewolników; dostanie za to zapłatę. Paulina już uznała, że spróbuje to na kogoś innego zrzucić. Dostała podstawowe info o defilerze - w odosobnieniu, trochę narusza Maskaradę, w obszarze utraconym, ale jest tam ukryta Brama Millennium. Tak więc Paulina może się tam móc przedostać... jeśli przekona odpowiednie osoby.

- Paulina załatwiła sprzęt na akcję. Od Goliata Zajcewa, maga truciciela wzięła truciznę wyłączającą kanały magiczne i poprosiła o wsparcie. Dostała człowieka, agenta wojskowego. Erwin Sternik. Dostała też osobę 'social operator', Amelia Żądło.

- Paulina opracowała plan i przygotowała sprzęt do walki z defilerem. Test średni, ona ma sharp++ i terminusa. Zwycięstwo bezkonfliktowe, stały bonus sprzętowo-taktyczny +2 do wszystkich akcji przeciw temu terminusowi.

- Paulina zdobyła też od Dzierżawca chain explosion rod i od centrali zaklęcie defensywne.

- Czas na defilera.

- Paulina znalazła (sharp oraz oficer polityczny) kompetentną osobę która podejmie się działań do tej misji jaką zleciła Ilona Szklanka. Przekonała go (hot + oficer), by się jej podjął za niewielką przysługę w jej stronę (i przekazanie całości nagrody od Ilony). Operacja się udała, skonfliktowane, w wyniku tamten przekonany terminus bardzo ucierpi w przyszłości. 


### PO DRUGIEJ STRONIE BRAMY:

- Brama znajduje się na terenach utraconych, jest to ukryta, przemytnicza brama Millennium znajdująca się w podziemnej bazie Millenium. Jest tam środek transportu, trochę jedzenia, mapy itp. Brama jest wyciszona; otwiera się ją kilka godzin, chyba, że otworzy się ją w pełni mocy (wtedy będzie wykryta natychmiast).

- Paulina, Amelia dostały się do najbliższej miejscowości, w okolicy której znajduje się defiler. Paulina jest ekspertem od budowania profili przeciwników. Amelia idzie na miasto i zbiera informacji o defilerze, (średni na jej silnych stronach). Zdany. Drugim kanałem, Paulina sprawdza dane z bibliotek, starych gazet i próbuje to powiązać. Niestety, ciężko było dostać się do biblioteki i ważnych danych (konfliktowane), więc potrzebna była łapówka dla lokalnego wrednego staruszka, który wszystko archiwizował. Na bazie tych połączonych danych udało się obniżyć stopień trudności budowy profilu z bardzo trudnego do trudnego i Paulina zabrała się do roboty.

- Test Paulinie się udał w sposób nieskonfliktowany. Defiler i wszystko na jego temat stanęło Paulinie otworem. Defiler jest pięćdzięsięciokilkuletnim kapłanem Wiązu. Poświęca starych lub śmiertelnie chorych, ewentualnie używa kukieł i krwi wyznawców. Tym różni się od przeciętnego defilera, że jeszcze nie uległ korupcji. Sam siebie uważa za element porządku naturalnego. Paulina powinna go szukać gdzieś w okolicach naturalnych miejsc kultu świętego drzewa. Jego potencjalne siły to młodzi neopoganie (patrząc na mieścinę, max 4, wyznawcy to max. 20, niebojowi). Ewentualnie jakaś efemeryda jaką uważa za manifestację Wiązu. Ludzie w okolicy są bardzo pozytywnie nastawieni do defilera.

- Amelia zaproponowała, że udając dziennikarkę pozna tożsamość defilera. Na pewno zostanie zauważona, ale stawką jest to, żeby nie wzbudzić niczyich podejrzeń. Test trudny. Nieskonfliktowanie jej się to udało osiągnąć - poznali tożsamość defilera. To lokalny nauczyciel śpiewu i tańca a przy okazji aktywista na rzecz lokalnej miejscowości. Bardzo rozpoznawalna i znana osoba w okolicy. Mają jego adres. Dodatkowo jest przychylnie nastawiony do Amelii, którą uważa za początkującą dziennikarkę próbującą się dostać do jakiejś bardziej znanej gazety.

- W trójkę naradzali się nad tym, w jaki sposób pokonać defilera w maksymalnie niegroźny sposób. Erwin zaproponował wrzucenie mu pigułki gwałtu do wina; a dokładniej, by Amelia to zrobiła gdy go odwiedza. Powinna go skutecznie już do tej pory oczarować (tak, zrobiła to) i w ten sposób jest w stanie bezpiecznie go unieszkodliwić. Wszystkim się to podoba. Operacja powiodła się perfekcyjnie. Amelia weszła, porozmawiała z defilerem, gdy nie zauważył, wrzuciła mu pigułkę do wina i padł. Amelia poszła do domu. W nocy do defilera przyszli Paulina i Erwin, podłożyli defilerowi narkotyki i niedokładnie spalony liścik wg szablonu "interpol Cię szuka", po czym spokojnie odjechali jego autem, utopili je, przesiedli się do swojego i spokojnie i nienerwowo dotarli do bazy.


### PODCZAS URUCHAMIANIA BRAMY:

- Są w bezpiecznym miejscu. Defiler został magicznie unieszkodliwiony a jego aura jest scramblowana. W skrócie: są tak bezpieczni jak tylko się da.
- Defiler chce z kimś rozmawiać, ale nie ma z kim. Nikt nie chce z nim rozmawiać. W ten sposób nikt nie ulegnie szantażowi itp. 
- Po 3h brama została otwarta i nic się nie stało. Przeszli.


### DEFILER W MILLENNIUM:

- Dzierżawiec jest rozradowany. Ten defiler jest idealny. Jest nie dość mocno skorumpowany, ale to nie problem, można go po prostu mocno skazić. Zmartwił się trochę, że nie udało się przetestować żadnej broni, ale co tam. Będzie okazja. Sam tymczasem rozpocznie proces tworzenia nowej broni na bazie defilera, codename: "Inkarnator".

- Mag który poszedł na misję zamiast Pauliny nie wrócił. Dzierżawiec powiedział, że to mógł być atak wyprzedzający na nią (bo jemu pomaga). Jeśli tak, są 3 potencjalne osoby, które mogą to wiedzieć. Ilona Szklanka, bezpośredni szef Pauliny lub jakaś gruba szycha w Millennium. Dzierżawiec może jest zbyt paranoiczny, a może...


============================

### Aftermath:

- Paulina: dokonanie: "Złapanie defilera bez przemocy i magii".


Kić zadaje pytanie: "Jakim cudem Paulina ma takie niespójne domeny? Antykwariuszka, śledczy, terminus, oficer polityczny"

I dostaje odpowiedź: "Paulina miała wroga rodowego - potężny artefakt. Ten zaatakował i zmasakrował jej ród. Paulina się musiała ukrywać, ale i kontratakowała, w sekrecie zbierając wiedzę i śledząc lokalizację przeciwnika nieświadomego jej istnienia. Podczas Zaćmienia Paulina zniszczyła swego wroga, raz na zawsze zamykając to zagrożenie."

Żółw zadaje pytanie: "Czemu naukowiec, który podczas Zaćmienia pomógł jej zniszczyć artefakt, zginął?"

I dostaje odpowiedź: "Podkochiwał się w Paulinie. W pewnym momencie był dylemat - albo on ucierpi (może zginie, niewielka szansa, ale jest) a ona będzie bezpieczna, albo ona zginie a on będzie bezpieczny. Zaryzykował..."
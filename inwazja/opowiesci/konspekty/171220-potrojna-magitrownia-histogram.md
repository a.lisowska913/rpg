---
layout: inwazja-konspekt
title:  "Potrójna magitrownia Histogram"
campaign: wizja-dukata
players: kić, raynor
gm: żółw
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [171115 - Kryzysowo tymczasowy dyktator](171115-kryzysowo-tymczasowy-dyktator.html)

### Chronologiczna

* [171115 - Kryzysowo tymczasowy dyktator](171115-kryzysowo-tymczasowy-dyktator.html)

## Kontekst ogólny sytuacji

### Opis sytuacji

* W magitrowni doszło do wypadku; padł Dekontaminator.
* W wyniku uszkodzenia Dekontaminatora Daniel, Dracena i Paulina skupili się na naprawie tego cholerstwa.
* Ciężko uszkodzona Dracena została. Daniela i Paulinę "wciągnęło" w Magitrownię... Daemonica?

### Wątki konsumowane:

-

## Punkt zerowy:

-

## Potencjalne pytania historyczne:

* Czy ktoś jeszcze został wciągnięty w Magitrownię i ucierpi poważnie?
    * DOMYŚLNIE: Tak (tor: 9).
* Czy Wylęgarnia Świń zakotwiczy się w Magitrowni?
    * DOMYŚLNIE: Tak (tor: 9).

## Misja właściwa:

**Dzień 1**:

_EKSPERYMENT: sesja ma pokazać działanie bossfighta._

Jest ciemno. Paulinie szumi w głowie. Sygnał od Marii - silny - zatarty. Automaton stoi nad Danielem i wtapia go transorganicznie. Paulina nie umie się skomunikować z Marią - nie wie gdzie jest; jest w jakimś korytarzu w magitrowni. Ze ściany korytarza wystają krystality. Ale... czemu?

Paulina skupiła się i rzuciła na siebie Heroicznego Chirurga. Energia magiczna jest... dziwna, ale nadaje się do użycia przez Paulinę. Paulina zdecydowała się na to, by NATYCHMIAST przesunąć Daniela w energię negatywną. Ale do tego potrzebuje... wiedzieć, czym jest ta energia. Więc, szybkie badanie energii.

* "Energia jest szkodliwa": multifazowość (6), presja czasu -> 11
* "Opanuję tą szkodliwość": puryfikacja Pauliny, diagnoza energii, praca z Pryzmatem -> 9+5

Wynik: Paulina wie, że jest na multifazie. Stopione kilka faz. Wow. Ale jest w stanie zintegrować ze sobą tą energię i pomóc Danielowi...

* "Assimilation complete!": Dziwna Energia, transorganizacja -> 8
* "Danielu, wróć!": Rozumiem tą energię -> 3+9

Wynik: Soczewka. Nie tylko Daniel się zintegrował z energią - zaczął CZUĆ magitrownię zupełnie inaczej na tej Fazie. I poczuła go Integra... Daniel poczuł Automatony, poczuł świńskie skażenie, poczuł multifazę. I wie gdzie są - zna plany. Znajdują się w obszarze mieszkalnym, koło pokoju miłości Draceny. A pokój miłości jest ufortyfikowany antyaurą, nie? Paulina zaczyna oglądać Daniela.

Skąd tu są - Dracena próbowała naprawić dekontaminator. Coś bardzo poszło nie tak. Daniel pomagał. Dekontaminator zachowywał się zupełnie nie tak jak powinien; Paulina ratowała Dracenę. Tu są automatony - na Primusie nie. Multifaza? Daniel zdecydował się, że trzeba przejść do czujnikowni - tam będą kontrolki. I zaskoczyła ich Integra. Powiedziała, że ona jest przełożoną magitrowni a oni są anomaliami - zasymiluje ich. Wystrzeliła kable w kierunku Pauliny. Daniel spróbował zatrzymać kable wolą. Przesunąć. Zaskakując Integrę.

* "Assimilation will happen": magitrownia ciałem, inkarnacja (6) -> 11
* "JA NIE CHCĘ!": heroiczny chirurg, zakłócenie Daniela (6) -> 9+1 za drąg +3 -> 13

Skonfliktowany sukces: Integra nie jest w stanie ich zasymilować, acz problem nie jest rozwiązany. Opanowała ściany, by nie dało jej się uciec.

Paulina szybko przekazała wszystko Danielowi odnośnie Integry co pamięta.

* "Nic o mnie nie wiesz": Efemeryda magitrowni, Integra zawsze była z boku: -> 8
* "Kajetan mi o Tobie opowiadał!": Wiedza od Dobrocienia i Daniela, Kajetan: -> 6+3+3

SUKCES: Paulina przypomniała sobie 3 rzeczy o Integrze:

* Q: Czemu tu jest? Skąd się tu wzięła?
* A: Nie jest. Umarła w Wielkiej Efemerydzie Senesgradzkiej; zginęła w Senesgradzie wraz z resztą Bractwa Pary.
* Q: Kim ona jest?
* A: Założycielka magitrowni Histogram, ukochana Karola Marzyciela.

Daniel rozpoczął próbę negocjacji. Zrozumieć Integrę. My nie jesteśmy zagrożeniem, to jej dominium! Chcemy jedynie odejść.

* "Anomalia ulegnie asymilacji": inkarnacja (6), echo Weinerki, skala jednej woli: -> 14
* "Zostaw nas pls": zna Daniela!, częściowo zintegrowany, wiedza o Integrze: -> 10+5

Wynik: SUKCES. PERSISTENCE -1. Integra się cofnęła o krok. Zaakceptowała taką możliwość - nie wolno im dotykać leylinów, nie dotykać świń. Mają wrócić przy minimalnym koszcie i nie mają uszkodzić magitrowni. Po czym Integra ich wypuściła i się oddaliła w ścianę.

Zostali sami w magitrowni. Paulina znowu wysłała pocieszający sygnał Marii. A więc magitrownia jest częścią efemerydy senesgradzkiej... a z oddali słychać złowieszcze chrumkanie. Daniel czuje potężną świńską obecność w okolicach przepompowni zewnętrznej.

Zza rogu usłyszeli chrumk i ludzki kaszel. Zobaczyli świnioluda w agonii - połączony z krystalitami, człowiek i świnia. Boli; jest berserkerem. Świńludź atakuje Paulinę. Paulina atakuje chcąc go ogłuszyć.

* "Pożrę Twoje serce!": berserker, echo przeszłości, silny brutalny szybki: 11
* "Nope. Pośpisz.": heroiczny chirurg, agonia, broń (drąg)+1, Daniel: 10+2

WYNIK: Skonfliktowany. Udało się Paulinie go obalić; świniolud wycharczał z trudem "zabij się sama". Po chwili stracił przytomność. Paulina zaczęła jego badania. Drganie bólu, szeptu i cierpienia w powietrzu - Daniel natychmiast wezwał tam automatona, korzystając ze swoich szybkich umiejętności boksera. Automaton pojawił się na miejscu Pauliny i padł, ciężko uszkodzony przez atak glashunda. Glashund zmasakrował świniaka i zaczął się pastwić nad nim; masakruje biedaka i żywi się jego cierpieniem. Nie ma co już go ratować.

By uniknąć spotkań z dziwnymi ludźmi i istotami, zdecydowali się pójść "ścianami" - maintenance shaftami.

* "Trudna droga w magitrowni": trudny teren, multiplanar (6), odbicie Pryzmatu, system defensywny -> 17
* "Bez kłopotu": zintegrowany z magitrownią Daniel, świetna znajomość terenu, czuje niebezpieczeństwo, macie czas, chirurg -> +15+4

Wynik: SS. Dotarli, ale czeka na nich Integra i nie pozwoliła tam zrobić niczego dotyczącego Pryzmatu. Mogą tylko wrócić do domu. Paulina zarzuciła cierpieniem ludzi. Integra powiedziała, że misja jest najważniejsza - energia musi płynąć. Owszem, życie świniaka to cierpienie, ale to energia a energia musi płynąć.

Daniel zdecydował się wpierw połączyć z Marzycielem używając komunikatorów dalekiego zasięgu - radiostacji eteru. 

* "Skontaktujesz się z... czymś innym": multifaza (6), only human -> 11
* "Skontaktuję się z Marzycielem": Marzyciel słucha eteru, integracja z magitrownią, aspekty inne +2 -> +8+8

Wynik: 15. Daniel skutecznie przebił się przez eter. Paulina odzyskała komunikację z Marią a sam Daniel skomunikował się z Karolem. Daniel poprosił Karola, by ten opowiedział mu o Integrze; Karol się nie zgodził. To nie sprawa Daniela. Karol ma powiedzieć Sylwestrowi Bankierzowi o problemie i będzie mógł porozmawiać za to z Integrą...

Paulina zaatakowała słownie Integrę. Ile z Integry zostało w tej manifestacji? Czemu pozwala na śmierć ludzi? Wezwała z efemerydy cień Janiny Muczarok by pomóc.

* "Zostaniesz uszkodzona. Wrócisz do domu.": inkarnacja magitrowni (6), Skala Efemerydy (6), energia musi płynąć: -> 17
* "Zrozumiem, czym jesteś i dotrę do Ciebie.": echo Integry, Karol rozmawia z Integrą (6), pomagamy magitrowni, Janina -> +15+4

Wynik: REROLL, 19. RESURGENCE -1. Integra... pękła, lekko. Opadła z niej osłona efemerydy. Powiedziała, że przez to, że są tak duże potrzeby energetyczne i STABILNOŚCI energii to Integra musi zasilać czymś innym. Faza Daemonica / emocje, magia krwi / świnioludzie cierpienia? Jak nie będzie SLA na stabilność energetyczną, nie będzie problemów ze świnioludźmi. Nie będzie magii krwi. Integra zauważyła, że magitrownia histogram nigdy nie była zaprojektowana jako stabilna. Zawsze jako impulsówka.

A Efemeryda Senesgradzka, z jakiegoś powodu, jest bardzo "zainteresowana" świniami. Nie wie czym JEST "zainteresowana" - to nie te same emocje...

Po tamtej stronie, Sylwester Bankierz ściągnął przenośny portal. Jego katalista skalibrował go przez numery magiczne przekazane przez Integrę. I Paulina z Danielem wrócili do domu...


## Wpływ na świat:

JAK:

* 2: MUSIK: gracz mówi w jaki sposób jedna z obcych postaci wspiera coś związanego z motywacją JEGO postaci
* 2: CLAIM na wątku / postaci / okoliczności
* 2: do elementu Historii lub przeszłego konfliktu dodajemy "ale" lub "oraz"
* 2: dodajemy pytanie, które musi zostać odpowiedziane na jakiejś z przyszłych misji
* 2: odpowiadamy na pytanie, które pojawiło się na jakiejś z misji
* 3: gracz mówi w jaki sposób jedna z obcych postaci wspiera coś związanego z motywacją JEGO postaci
* 3: zmieniamy kontekst okoliczności czegoś z Historii - scena z przeszłości / fakt?
* 3: do elementu spoza Historii dodajemy "ale" lub "oraz"
* 3: dodajemy znaczącego NPC powiązanego z elementem Historii
* 3: pozyskanie przez dowolną postać znaczącego surowca mającego sens z perspektywy misji
* 3: dodanie nowego elementu Historii
* 3: dodanie przyszłego lub przeszłego faktu; czegoś, co się wydarzyło lub wydarzy
* 5: anulowanie wyniku konfliktu

Z sesji:

1. Żółw (9)
    1. W magitrowni Histogram jest już dość stabilna wylęgarnia i mutator świń (ale bez ludzi)
    1. Karol Marzyciel: w ciągłym kontakcie z Integrą Weiner (echem efemerycznym). Kochankowie przez telefon.
    1. Integra Weiner: w ciągłym kontakcie z Karolem Marzycielem. Kochankowie przez telefon.
    1. Integra Weiner: posiada możliwość manifestacji osobiście w Magitrowni Histogram na Primusie (przez zbliżenie faz).
1. Kić (8)
    1. Integra Weiner: nie będzie polować na ludzi. Nie chce celować w ludzi. Uwolniła wszystkich ludzi w multifazie Histogram.
    1. Paulina Tarczyńska: Istnieje możliwość powrotu do miejsca splotu tych wszystkich faz. Ona będzie w stanie docelowo tam wrócić.
    1. -
1. Raynor (5)
    1. Integra Weiner: aktywnie wspiera obecność Daniela Akwitańskiego na Primusie.
    1. Daniel Akwitański: ma "linię supportową" do Integry Weiner przez Karola Marzyciela. Jak coś się spieprzy, to może z Integrą próbować rozwiązać.

# Streszczenie

W wyniku awarii dekontaminatora w magitrowni Histogram Paulina i Daniel znaleźli się w splocie kilku faz - Primusa, Daemonica, Allitras, Esuriit... i wszędzie istniała ta sama magitrownia Histogram. Spotkali tam się z efemerydą senesgradzką i jej manifestacją - Integrą Weiner, pierwszą twórczynią magitrowni Histogram. Udało im się przekonać Integrę do zaprzestania stabilizacji energii magią krwi, nie udało im się zapobiec stabilizującej się tam wylęgarni świń i wrócili bezpiecznie do domu, na Primus.

# Progresja

* Paulina Tarczyńska: Istnieje możliwość powrotu do miejsca splotu tych wszystkich faz. Ona będzie w stanie docelowo tam wrócić.
* Daniel Akwitański: ma "linię supportową" do Integry Weiner przez Karola Marzyciela. Jak coś się spieprzy, to może z Integrą próbować rozwiązać.
* Karol Marzyciel: w ciągłym kontakcie z Integrą Weiner (echem efemerycznym). Kochankowie przez telefon.
* Integra Weiner: w ciągłym kontakcie z Karolem Marzycielem. Kochankowie przez telefon.
* Integra Weiner: posiada możliwość manifestacji osobiście w Magitrowni Histogram na Primusie (przez zbliżenie faz).
* Integra Weiner: nie będzie polować na ludzi. Nie chce celować w ludzi. Uwolniła wszystkich ludzi w multifazie Histogram.
* Integra Weiner: aktywnie wspiera obecność Daniela Akwitańskiego na Primusie.

# Zasługi

* mag: Paulina Tarczyńska, ujawniła Integrę w manifestacji efemerydy; nie będzie eksperymentów na ludziach. Oraz: była w trójfazie i wróciła do domu.
* mag: Daniel Akwitański, przekonał do siebie Integrę i częściowo był sprzężony z magitrownią na trójfazie. Bezpiecznie wrócił z trójfazy do domu. Zaplanował randkę Marzyciela z Integrą.
* vic: Integra Weiner, regentka z ramienia efemerydy senesgradzkiej. Buduje wylęgarnię świń i bardzo stara się utrzymać magitrownię przy prawidłowym działaniu. Nadal myśli o Karolu.
* czł: Karol Marzyciel, nadal zakochany w Integrze, nie zapomniał o niej. Odebrał komunikaty z eteru od Daniela i pomógł z manifestacji wyciągnąć Integrę.
* vic: Efemeryda Senesgradzka, okazuje się, że jest bardzo zainteresowana świniami i eksperymentowaniem na świniach. Zdaniem Integry Weiner myśli w sposób zupełnie niezrozumiały.

# Plany

* Karol Marzyciel: już wiadomo, że da się wyrwać osobę z efemerydy. Chce więc wyrwać Integrę Weiner z efemerydy docelowo.
* Integra Weiner: zapewnić prawidłowe działanie magitrowni Histogram niezależnie od pryzmatu czy czegokolwiek. Oraz trochę poMarzycielować.
* Efemeryda Senesgradzka: z niewiadomego dla wszystkich powodów rozwija program eksperymentowania na eterycznych świniach. W eterze.

# Lokalizacje

1. Świat
    1. Primus
        1. Mazowsze
            1. Powiat Pustulski
                1. Męczymordy
                    1. Magitrownia Histogram, istniejąca jednocześnie na Primusie, Allitras i Daemonice, zarządzana przez echo Integry Weiner. Jedna magitrownia, wiele miejsc.
    1. Faza Allitras
        1. Trimektil
            1. Jezioro Błysków
                1. Prąd Histogram
                    1. Magitrownia Histogram, istniejąca jednocześnie na Primusie, Allitras i Daemonice, zarządzana przez echo Integry Weiner. Jedna magitrownia, wiele miejsc.
    1. Faza Daemonica
        1. Mare Setaril
            1. Labirynt Techniki
                1. Pokład Szeptów
                    1. Magitrownia Histogram, istniejąca jednocześnie na Primusie, Allitras i Daemonice, zarządzana przez echo Integry Weiner. Jedna magitrownia, wiele miejsc.

# Czas

* Opóźnienie: 0
* Dni: 1

# Wątki kampanii

-

# Przeciwnicy

## Integra Weiner, echo czarodziejki
### Opis

Echo czarodziejki, która zarządzała i kochała swoją magitrownię ponad wszystko. Chwilowo integruje rzeczy dla Efemerydy. System defensywny magitrowni.

### Motywacje

"Wszystko musi stać się jednością. Energia musi płynąć. Chronić magitrownię."

### Aspekty

* RESURGENCE: 10
* Integra: inkarnacja magitrowni (6), słabość: koty (6), magitrownia moim ciałem, magitrownia mymi zmysłami, transorganiczna asymilacja (6)
* Magitrownia: trudny teren, multiplanar (6), odbicie Pryzmatu, system defensywny
* Automatony: żelazo szkło plastik, transorganiczna asymilacja, relentless, magitrownia mymi zmysłami

## Wylęgarnia Świń, efemeryczna amplifikacja
### Opis

Pasożyt energetyczny, który przyssał się do magitrowni by wyssać z niej całość energii.

### Motywacje

"JEŚĆ WIĘCEJ! BYĆ WIĘCEJ! CAŁA ENERGIA BĘDZIE MOJA!"

### Aspekty

* RESURGENCE: 10 
* Motherpig: oddział świńludzi (9), oddział skażyświń (9), ofiary w wylęgarki, biotransformacja (6)
* Świńludzie: silni szybcy i brutalni, wzbudzają strach, żywotni i pancerni, echo przeszłości
* Skażyświnie: niepowstrzymany berserker (6), pancerny z przodu (6), wieczny ból
* Wylęgarka: kiedyś człowiek lub mag, spawn more pigs, wymaga energii magicznej

# Narzędzia MG

## Opis celu misji

-

## Cel misji

-

## Po czym poznam sukces

-

## Wynik z perspektywy celu

-

## Wykorzystana mechanika

Postać (strona gracza):

| .Motywacja. | .Umiejętności. | .Magia. | .SiłySłabości. | .Znam.    | .Mam.      |.POSTAĆ. |
|   (-2) - 2  |     0 - 2      |  0 - 2  |    (-2) - 2    | Z+M = 0-6 | Z+M = 0-6  |         |

Opozycja (strona NPC):

* Baza: 2
* Aspekt: 3/aspekt, wariant z nożyczki/papier/kamień
* Modyfikatory: Skala, Sprzęt, Sytuacja (1-3 każdy)

Wynik:

| .Postać. | .Opozycja. | .Rzut.    | .Wynik.  |
|          |            | xx: +x Wx |   S-SS-F |

Wpływ:

* 8 kart / dzień
* każda porażka i skonfliktowany sukces = +2 pkt wpływu dla gracza
* każda karta = +1 wpływ dla MG
* każde 5 kart zaokr. w górę: +1 pkt wpływu dla gracza

---
layout: inwazja-konspekt
title:  "Gambit Anety Rainer"
campaign: powrot-karradraela
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [161012 - Kontratak Karradraela (HB, KB, SD)](161012-kontratak-karradraela.html)

### Chronologiczna

* [170103 - Wojna bogów w Czeliminie (AW)](170103-wojna-bogow-w-czeliminie.html)

## Kontekst ogólny sytuacji
## Punkt zerowy:

W Czeliminie rozpoczęła się Wojna Bogów. Arazille i Karradrael walczą o Czelimin; jednocześnie uderzyła Aleksandria i Teleportacja z Kompleksu Centralnego do nieznanego punktu różnego rodzaju oprzyrządowania... 

Aneta Rainer próbuje rozpaczliwie utrzymać Kompleks Centralny przy życiu; pozbawiony wielu źródeł energii i zdestabilizowany, tańczy między złamaniem Maskarady i samozniszczeniem.

Autowary rozpełzły się po okolicy; uszkadzając siły Karradraela. Felicjan Weiner i Karolina Maus walczą przeciwko nim ze swoimi oddziałami. Karradrael zaczyna asymilację ludzi z poszczególnych mniejszych mieścin by móc zasilić się i pokonać Arazille...

MECHANICZNIE: Andrea ma 5 surowców / dzień.

## Misja właściwa:

Dzień 1:

Andrea jest gotowa do działania. Aneta Rainer wysłała sygnał do Andrei. Jest problem.

Aneta powiedziała, że z Kompleksu zostały teleportowane... generatory? Kompleks Centralny jest pomiędzy granicą eksplozji a manifestacji; między złamaniem Maskarady i samozniszczeniem. Aneta potrzebuje posiłków. Siły ognia, katalistów, lekarzy. 

* Możesz wysłać mi swoich magów Millennium - Aneta, z napięciem w głosie
* Ja już NIE MAM magów Millennium... spróbuję zdobyć jakieś siły... - Andrea, zmęczona.
* Jeśli nie dostanę dość wsparcia, nie mamy Kompleksu i Świeca straci kolejny teren - Aneta, chłodno - Kompleks jest ważniejszy.
* Kompleks Centralny został zmieniony w pułapkę. Jestem w stanie to rozmontować, ale potrzebuję sił. Jak największych sił. - Aneta, prosząco

Dodatkowo, pogoda jest fatalna. Burze magiczne. Wojna Bogów. I autowary rozłażące się po okolicy. 

Jeszcze gorzej - teleportowane zostały generatory? Coś jeszcze? Andrea poprosiła Anetę, by ta sprawdziła co zostało teleportowane. Andrea podjęła trudną decyzję - wyśle do Anety Rainer większość swoich sił. Zajcewów, Dalię i Biankę. Zostaje jej Baran, Pszczelak, Tatiana, Melodia, Kirył + Anastazja, Jankowski, Krąg Życia. Transport do Kompleksu zapewni Krąg Życia. Jankowski dostanie jako zadanie - znaleźć KADEM i dowiedzieć się co się stało. 

Innymi słowy, bywało lepiej.

Andrea skontaktowała się z Millennium. Dokładniej, z Draconisem (-1 surowiec). Draconis powiedział, że lepszą opcją będzie wyłączenie Kompleksu Centralnego - grupa komandosów, którzy wbiją i wyłączą Kompleks (Świeca traci kontrolę nad regionem). Andrea się nie zgadza. Draconis zadał proste pytanie: czemu Millennium ma pomóc Świecy w odbudowaniu Kompleksu Centralnego? Zwłaszcza pod dowództwem Świecy (Anety Rainer)? Draconis zaproponował pomoc pod dwoma warunkami:

* Amanda Diakon dowodzi siłami Millennium
* Millennium dostanie jakiś obszar Kopalina plus jakieś miasto (np. któryś Piróg). Coś, by móc także działać na pełnej mocy.

Na świat gry: stawka jest taka:

* Z pomocą Millennium, Kompleks Centralny będzie uratowany. Niecały miesiąc i zregeneruje. A przedtem jest sprawny.
* Bez pomocy Millennium, Kompleks MOŻE być uratowany. Bo Andrea wysłała siły. Ale zniszczenia będą większe i ofiary też.

Andrea zdecydowała się na kontr-negocjacje. Odda im miasteczko - Koty. I tak to miejsce Millennium, tylko nieoficjalnie. A dokładniej: nie Millennium. Ród Diakonów. Spójnie z innymi rodami (Mausów, Bankierzów...). Andrea powiedziała wyraźnie - niech Amanda zadziała jako najemniczka pod Anetą. Andrea sprawi, by to się opłaciło. A Świeca była i będzie. Po co walczyć. Draconis, po zastanowieniu i komunikacji, się zgodził. Powiedział, że w ciągu trzech dni Amanda sprowadzi potrzebne siły; sama ze swoimi siłami i tak tam pójdzie. Czas na bitwę o Kopalin.

Andrea załatwiła Anecie Amandę i swój oddział. Aneta Rainer załatwiła Kurtynę, siły Emilii Kołatki (dzięki Vladlenie) i wsparcie sił Agresta.

Aneta spytała o możliwość wsparcia w wykryciu tego co teleportowano z Kompleksu. Gdy Andrea dowiedziała się, że to poleciało gdzieś w okolice Myślina, zaśmiała się tylko. Powiedziała, że tam nie wolno nikogo wysyłać. Bo wszyscy zginą. Aneta przyjęła do wiadomości. Andrea kazała jej dowiedzieć się jak najszybciej co zniknęło z Kompleksu Centralnego...

Kirył i Anastazja dostali zadanie od Andrei - mają jak najszybciej zdobyć i przejąć Śledzika. Andrea potrzebuje Śledzika... Kirył z najczystszym optymizmem stwierdził, że poradzą sobie z przejęciem. Andrea wysłała ich do Lilaka; niech on pomoże znaleźć i przeprowadzić.

Mieszko przyszedł do Andrei. Jak jest Wojna Bogów, najpewniej Laurena i Katalina ucierpią. Ale to jest szansa, by je odbić, uratować! Mają dość umiejętności, by się wkraść... Bankierz nie chce przyjąć do wiadomości, że solo nie ma szans ich uratować. Nie jest w stanie. Nie da się.

* Istnieje tylko jeden sposób, w jaki mogę się na to zgodzić, tien Bankierz... - Andrea
* Słucham - Mieszko
* Jeśli uda się tam razem z czarodziejem Sjeldem w jego czołgu. I nie mamy już lekarza... - Andrea
* Laurena jest lekarzem. Potrzebujemy jej. - Mieszko
* Laurena jak również i Katalina... mają być w stanie nieprzytomności. Przy szczątkowej świadomości, nie dopuszczę ich do tej bazy. - Andrea, zimno
* Rozumiem. Rozumiem, sensowne... - Mieszko
* Żeby było jasne: czarodziej Sjeld jeszcze nie potrafi kontrolować swojego czołgu. Jest to jego zadanie na teraz. - Andrea
* Mam czekać i nic nie robić, gdy moje partnerki są w ich rękach. - Mieszko, zimno
* Tego nie powiedziałam... - Andrea

Andrea wydała dyspozycje do rozparcelowania bazy w różne miejsca. Każdy ważny komponent do innego miejsca. Baran ma zarekwirować ciężarówkę, do której się wsadzi biomózg. Należy dodatkowo schować koło jednego z węzłów przekaźnik hipernetowy. Ogólnie... przygotować się, by można było stracić tą bazę.

Dzień 3:

Andrea dostała dobre wiadomości. Siły Anety Rainer dały radę częściowo przejąć kontrolę nad Kopalinem. Udało im się też WSTĘPNIE uratować Kompleks; nie jest jeszcze pod ich kontrolą, ale nie wybuchnie i nie straci Maskarady. Innymi słowy, teraz to tylko kwestia czasu, acz ciężkiego czasu. I zwalczania sił rezydentnych... Aneta zakłada, że Bianka jest jej jeszcze potrzebna; za dwa dni może Andrei oddać wszystkich co nie są Bianką - siły ze Świecy z bezpieczniejszych obszarów i siły Millennium ją wspierają. Też: magowie Rodów.

Wieści z Trocina są niezłe. Doszło do wielkiej bitwy, strasznego starcia, uniemożliwiającego uruchomienie Krwawego Aderialitha. Ten problem jest z głowy, ale też związuje siły Świecy.

Andrea dostała dobre wieści od Kiryła - przechwycili Śledzika. A Wojna Bogów wciąż trwa. Andrea nie chce wysyłać Śledzika do Czelimina; nie wie co się może stać. Ale jeszcze bardziej nie chce, by Mieszko poszedł sam się zabić... więc z ciężkim sercem, poprosiła Kiryła by wziął Mieszka i odbili Laurenę i Katalinę z Czelimina. I tak, Anastazja, Kirył i Mieszko w Śledziku pojechali do Czelimina... priotytetem jest ICH przetrwanie. Nawet jak im się nie uda, niech się wycofają...

Nawet ze Śledzikiem, Wojna Bogów itp. Wymaga to sukcesu Heroicznego. F (Śledzik się znarowił; wyjdzie poza kontrolę Kiryła i będzie walczyć z Karradraelem na własną rękę), S. Udało się...

Tatiana i Baran zaraportowali, że baza została rozłożona na większy teren.

Wieczorem, Kirył i Anastazja powiedzieli, że Mieszko Laurena i Katalina są razem, nieprzytomne. Mają jeszcze kilku magów, których udało się wyciągnąć. Kilku potrzebuje pomocy medycznej. Anastazja się nimi zajmuje... okazało się, że Śledzik się Phase-Outował po drodze i oni zostali na otwartym terenie. Nie mają już śledzika... zabunkrowali się w stodole w Pszeninie.

Mają trzech aktywnych (Mieszko, Kirył, Anastazja) i pięcioro nieprzytomnych (Laurena, Katalina, +3). Lilak ma załatwić transport do zamku... najlepiej, niech Baran ich przewiezie ze wsparciem Lilaka (Kręgu Życia) jako oczy.

Gdy Lilak dotarł na miejsce Andrea dostała telefon od Franciszka Myszeczki.

* Masz problem, Lady Terminus. - Franciszek
* Który teraz? - Andrea
* Choroba. - Franciszek - Osoby uratowane są w różnych stadiach różnych chorób. I smak krwi.
* To co można zrobić? Nie mam chwilowo lekarza... - Andrea, załamana
* Daj ich nam. My się nimi zaopiekujemy; mamy lekarzy viciniuskich - Franciszek - Ich choroby magów się nie imają.
* Kirył, Mieszko, Anastazja - idą z wami. - Andrea, zarządzając kwarantannę

Baran, Pszczelak, Melodia, Tatiana. Pozostałe siły Andrei. Plus Krąg Życia.

Dzień 4:

Lekarze Kręgu Życia zgłosili, że nikt się nie zaraził z tej trójki. Andrea z radością powiększyła swoje siły o Kiryła, Anastazję i szczęśliwego Mieszka.

* Jak Mieszko teraz chociaż PIŚNIE nie tak... - niezadowolona Kić

Tatiana odezwała się do Andrei. Jej zwiad (no, autonomiczny zwiad Iriny) zgłosił jej, że Karradrael zmienił taktykę. Teraz wysyła małe oddziały harvestujące, które wbijają do wsi, kombinacją: kraloth + mortalis wysysają energię z ludzi i przesyłają energię do Czelimina. To zwiększa moc Karradraela w walce z Arazille. Jeden z tych oddziałów wbił właśnie do Czubrawki; Tatiana uważa, że są w stanie weń uderzyć. Lub go zatrzymać. W najgorszym razie, przejąć energię jako swoją.

Mówiąc brutalnie, Andrea ma: Kiryła, Anastazję (oboje mało bojowi), Pszczelaka (terminus teoretyk), Tatianę (uczennica terminusa i to kiepska), Mieszka (świetny terminus, ale jeden), Barana (terminus) i Melodię (niebojowa). Nie sądzi, że są w stanie coś z tym zrobić. A przynajmniej, póki nie przyjdzie wsparcie.

Szczęśliwie, już następnego dnia Andrea dostanie do pomocy Zajcewów i Dalię...

# Progresja



# Streszczenie

Wojna Bogów w Czeliminie trwa. Aneta Rainer korzysta z okazji i siłami swoimi, Kurtyny, Andrei, Millennium opanowuje Kopalin z Amandą oraz desperackim gambitem stabilizuje Kompleks Centralny. Andrea, pozbawiona sił, odbija Laurenę, Katalinę i 3 innych magów z Czelimina; w procesie tego Śledzik jest uwolniony przez Arazille i poluje na Karradraela na własną rękę. Tatiana odkrywa, że Karradrael czerpie energię z oddziałów Harvestujących - poluje na okoliczne miejscowości i je wysysa kombem kraloth#mortalis. Z Kompleksu teleportowano sporo rzeczy (min. generatory) do Myślina. Trocin uniemożliwił pojawienie się Krwawego Aderialitha.

# Zasługi

* mag: Andrea Wilgacz, pozbywająca się resztek posiadanych zasobów dla dobra Świecy; też, przygotowuje się do utraty bazy (na wszelki wypadek)
* mag: Aneta Rainer, która korzystając z okazji uderzyła by odzyskać Kopalin i ustabilizować Kompleks Centralny. Z pomocą Millennium i Andrei - wygrała.
* mag: Rudolf Jankowski, dostał od Andrei zadanie znaleźć co się stało z KADEMem by móc się z nim skontaktować. Oddalił się; samodzielny agent.
* mag: Draconis Diakon, pośredniczył w kontakcie pomiędzy Andreą i Amandą. Twardy negocjator, ale chce pomóc Świecy... po prostu, chce silnego Millennium.
* mag: Amanda Diakon, która w imieniu rodu Diakon wsparła z całej siły Anetę Rainer. Z wielkimi kosztami, trzyma Kopalin i pomaga odzyskać Kompleks.
* mag: Vladlena Zajcew, z Rezydencji Blakenbauerów oddaje siły Kurtyny pod kontrolę Anety Rainer, by uderzyć w Kompleks Centralny.
* vic: Stalowy Śledzik Żarłacz, uzyskał samoświadomość i wolność wbijając do Czelimina. Po uratowaniu kilku magów, oddalił się na własną rękę tępić Karradraela.
* vic: Świeży Lilak, mistrz logistyki i główny koordynator Kręgu Życia dla Andrei. Da się lubić.
* mag: Mieszko Bankierz, który był skłonny oddać życie dla swoich partnerek. Szczęśliwie posłuchał Andrei - i odzyskał Laurenę i Katalinę.
* mag: Kirył Sjeld, który z Anastazją zdobył Śledzika... by go utracić do Arazille. Sam nie wie o co chodzi i co się dzieje.
* mag: Anastazja Sjeld, która z Kiryłem odzyskała Śledzika... by go utracić do Arazille. Podręczna lekarka-amatorka z technikami syberyjskimi.
* mag: Laurena Bankierz, uratowana z Czelimina przez Stalowego Śledzika Żarłacza; nieprzytomna i chora na coś.
* mag: Katalina Bankierz, uratowana z Czelimina przez Stalowego Śledzika Żarłacza; nieprzytomna i chora na coś.
* mag: Tatiana Zajcew, ma małą sieć sensorów Iriny dookoła Śląska. Odkryła, że Karradrael poluje na ludzi i... nie są w stanie nic z tym zrobić.
* mag: Franciszek Myszeczka, ostrzegł Andreę, że uratowani z Czelimina magowie są chorzy na magiczne choroby; dostarczył viciniuskich lekarzy.
* mag: Bianka Stein, wypożyczona Anecie Rainer, by ta mogła opanować Kompleks Centralny. Najlepsza katalistka w okolicy.
* vic: Arazille, jej manifestacja w Czeliminie czerpie energię z Żonkiboru i powoli i nieubłaganie wypiera Karradraela. Uwolniła Śledzika.
* vic: Karradrael, walczy na wszystkich frontach przeciwko wszystkim. I nie przegrywa. Nie ma magii tak czarnej, jakiej by nie użył.

# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Wtorek Śląski
                    1. Ośrodek historyczny
                        1. Ruina zamku, skąd Andrea wyciągnęła większość elementów centralnych bazy (by móc tą stracić ;p )
                1. Czubrawka, zaatakowana przez Harvesterów Karradraela; ludzie ucierpieli przez kralothy i mortalisy.
                1. Kopalin, odzyskany w większości przez siły Anety Rainer, Amandy Diakon, Andrei.
                    1. Centrum
                        1. Kompleks Centralny Srebrnej Świecy, ustabilizowany przez Anetę Rainer; sporo zeń wyteleportowano, ale stoi i się trzyma kupy.
                1. Pszenino, przysiółek, gdzie zabunkrowała się grupa magów Andrei odbijających ofiary z Czelimina
            1. Powiat Czelimiński
                1. Czelimin, w którym trwa Wojna Bogów i skąd Mieszko w Śledziku odbił kilku magów (min. swoje partnerki)
                1. Koty, które zostaną oddane rodowi Diakon za wsparcie w odbijaniu Kompleksu Centralnego przez Millennium
            1. Powiat Myśliński
                1. Myślin, do którego wteleportowano z Kompleksu Centralnego generatory i inne rzeczy. Auć.
            1. Powiat Okólno-Trocin
                1. Trocin, gdzie magowie Świecy i lokalni zaatakowali wyprzedzająco i usunęli zagrożenie ze strony Krwawego Aderialitha

# Czas

* Dni: 4
---
layout: inwazja-konspekt
title:  "Rozpaczliwie sterowany wzór"
campaign: prawdziwa-natura-draceny
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [160901 - Uwięziony w komputerze! (PT, temp)](160901-uwieziony-w-komputerze.html)

### Chronologiczna

* [160901 - Uwięziony w komputerze! (PT, temp)](160901-uwieziony-w-komputerze.html)

## Kontekst ogólny sytuacji
## Punkt zerowy:

Dracena jest niestabilna, po ostatniej akcji jej wzór jeszcze bardziej się pogorszył - jak Paulina się spodziewała. Walczą ze sobą aspekty: Diakonka, Wiła, Spustoszenie, pochłonięci ludzie, terminus Marek, urocza cybergothka, pająk astralny, dotknęła Paulinę. Za dużo. Sama Dracena jest silnie zagubiona w tym wszystkim. Mimo potencjalnych problemów wynikających z pacjentami, Paulina zdecydowała się skupić przez chwilę na Dracenie - jeśli jej się nie uda, czarodziejka może się zbyt wypaczyć.
Jednocześnie Paulina wie co nieco o Diakonach. Nie rozumie, dlaczego Dracena jest "sama". To najdziwniejszy przypadek jaki widziała. No i skąd ta "destabilizacja wzoru"?

Mechanicznie, Dracena ma 21 punktów do rozdysponowania na 10 kategorii: Diakonka, Wiła, Spustoszenie, Cybergothka (po 2 wydane), Marek, Paulina, Ludzie (syndrom ghoula), Pająk, Strach i Optymizm. 3# = wpływa na nią. 5# = staje się jej elementem. 8# = element corowy.

## Misja właściwa:

Dzień 1:

Paulina z duszą na ramieniu wróciła do Marzanki z Draceną; do domku, który wynajęła jej Maria (Paulinę mogą szukać, Marii nikt nie szuka). Czekała na przebudzenie czarodziejki; na razie Dracena nie przejawiała żadnych objawów bycia chętną do obudzenia się. Jednak ostatnia akcja bardzo dała jej w kość i wpłynęła na jej unikalny wzór. Jej aura szaleje. Zmysł Pauliny - obudzi się. 

Paulina jest głęboko zaniepokojona - do tego stopnia, że zdecydowała się zdobyć pomoc. Powołując się na tajemnicę lekarską i płacąc niemało poprosiła o pomoc specjalistę od leczenia wzoru i terminusów. Bartłomiej Czyrawiec którego poprosiła o skołowanie kontaktu powiedział, że najlepsze kontakty ma w okolicach Świecy. Ale Paulina nie zgodziła się na Świecę; Bartłomiej polecił więc znajomego maga, ale Paulina powiedziała, że potrzebuje kobietę. Zrezygnowany, Czyrawiec zaproponował Kingę Toczek. Czarodziejka ta ma swoje odchyły (np. uważa gildie za chory pomysł, jest skrajną indywidualistką i ma cięty język, ale jest lekarzem i składa terminusów).

Kinga Toczek okazała się być lekko zmierzłą i kostyczną czarodziejką. Zgodziła się - po namowach Pauliny - pojawić pojutrze (dnia 3). Kazała utrzymywać Dracenę w stanie nie robienia niczego. Powiedziała, że pojawi się ze wsparciem - Paulina ją ubłagała, by to nie byli magowie a golemy.

Paulina, z lekkimi obawami (niestabilna Dracena + Skażona Paulina) zdecydowała się rzucić czar. Chce przedłużyć sen Draceny... i sama się przespać. Oczywiście, pod warunkiem, że nie zaszkodzi to Dracenie. Udało się. Dracena będzie spać dłużej a i Paulina się prześpi... 

Paulina obudziła się pierwsza - jest lekarzem. Dracena wstała z łóżka, jednym ruchem. W ciemności jej oczy lśnią diodami i wyładowaniami. W oczach ma celowniki. Szuka. Paulina spróbowała ją wytrącić; udało jej się. Dracena szuka Marka Śmietanki, chce zabrać jego życie tak jak on zabrał życie jakiegoś człowieka. Paulina próbuje Dracenę uspokoić, odwrócić jej uwagę... nie karać TAK, to nie ona, nie Dracena. Udało się... 

Dracena się ustabilizowała. Powiedziała, że "czuła" Marka. W swojej głowie. Jemu zupełnie, zupełnie nie zależało na tym człowieku. Pozwolił mu umrzeć, bo musiałby się wysilić, by przeżył. Paulina powiedziała, że tacy magowie są. Dracena, że w imię "ich władcy" mogą wszyscy być zasymilowani i skorygowani. Paulina - że to NIE Dracena. To mówi Spustoszenie. Paulina powiedziała, żeby Dracena skupiła się na tym, czym jest, co pamięta. Dracena, że pamięta za wiele, zbyt wiele różnych rzeczy. Paulina przypomniała Nikolę i poprosiła, by Dracena jej opowiadała historie, by pamiętać, by przywrócić...

Dracena opowiedziała o cruentus reverto. Dawno temu Antygona i Dracena miały siostrę. I rodziców. To była czysto rodzina magów. Wszystko Diakoni. A potem przyszło Zaćmienie i oboje rodzice utracili moc magiczną. Ich siostra oddała się najczarniejszym rytuałom, z pomocą Draceny (NIE Antygony). W wyniku tego siostra stała się cruentus reverto a Diakoni przechwycili Dracenę i Antygonę. Dracena NIE chce być Diakonką. NIENAWIDZI tego wszystkiego. Ale jak Antygona mogła wybrać Świecę i życie w Świecy i tam się rozpłynąć, Dracena musi być pilnowana. Bawiła się w zbyt mroczne rzeczy. I dlatego miała zdestabilizowany wzór - cruentus reverto wpadł w ręce Ozydiusza a potem w ręce "tych złych". Ta sama krew. Ta sama siostra. Można było Dracenę i Antygonę dowolnie zmienić i przekształcić. Więc Dracena musiała zmienić swój wzór na inny niż siostry. Antygona też, ale inaczej.
Dracena ma podejrzenia, że Diakoni po prostu chcieli się jej pozbyć.

Paulina usłyszawszy to po prostu przytuliła Dracenę. To uaktywniło aspekt wiły i Dracena zareagowała bardzo spontanicznie i cieleśnie. Paulina próbowała się bronić, ale Dracena jest za silna, zbyt głodna i zbyt zdeterminowana. Paulina skończyła przeleciana przez Dracenę, z przerażoną Marią na aktywnym linku...

Gdy Maria obudziła Paulinę, Dracena już zdążyła podciąć sobie żyły i próbować się utopić w wannie. Na marne - jej wzór zaadaptował. Dracena nie jest w stanie się zabić, nie potrafi. Paulina korzystając z okazji zmusiła Dracenę, by ta zaakceptowała pomoc innej czarodziejki (Kingi Toczek). Dracena wrogo, ale się zgodziła. Przeprosiła bardzo Paulinę za to co zrobiła; jakkolwiek lubi seks, nie chce tak.

Dzień 2:

Paulina obudziła się przed Draceną i zauważyła, że Dracena wypiękniała. Aspekt wiły zaczyna się przebijać - i dominować. Paulina przyniosła Dracenie śniadanie do łóżka; potem zagnała ją do ciężkiej pracy - tu posprzątać, tu narąbać drew (nieważne, że nie trzeba), przekopać ogródek - pod pretekstem, że trzeba zbadać kondycję fizyczną Draceny. Paulina nie wisi nad nią cały czas, ale cały czas ją monitoruje. Wytrzymałość i siła Draceny przebijają wszystko, co Paulina do tej pory widziała. Ta drobna cybergothka jest ze stali.

Niepokojące dla Pauliny jest to, że Dracena do niej przyszła i powiedziała, że jest głodna. Bardzo głodna. Ma pełny żołądek, ale jest strasznie głodna. Paulina nie wie na co i jak może jej pomóc; a boi się użyć aury magicznej by Dracena to pożerała. Dała więc Dracenie kilka quarków; czarodziejka pożarła je łapczywie. Poprawiło jej się, aczkolwiek powiedziała, że to nie wszystko. Dracena (nadal głodna) zaproponowała Paulinie, by ta dała jej jeszcze kilka quarków i sprawdziła jak wygląda przyswajalność. Paulinie się pomysł spodobał - ale! Nie teraz. Niech wpierw przyjedzie Kinga. Więc powiedziała Dracenie, że ta nie może się przejeść. Załamana Diakonka wróciła do przekopywania ogródka. 

Paulina przygotowuje obiad i zobaczyła przez okno, że Dracena gdzieś idzie. Węszy. Paulina poszła za nią, nie zdradzając swojej obecności. Znalazła Dracenę pod jednym z domków; czarodziejka obserwowała go uważnie. Paulina zapytała ją, co robi. Dracena - tam jest jedzenie. Paulina ją zatrzymuje - nie chce, by Dracena się żywiła na czymś nieznanym. Dracena rzuciła zaklęcie pokazujące Paulinie, że nadal się kontroluje i jaki typ aury. Astral? No nic, niech pożeruje, Paulina ją zatrzyma (nie byłaby w stanie jej odmówić). I Dracena zaczęła żerowanie. To aspekt pająka; Dracena asymiluje marzenia i sny ludzi. Paulina ją zatrzymała i Dracenie faktycznie UDAŁO SIĘ zatrzymać. Wściekła, zaczęła krzyczeć na Paulinę, lecz ta ją odciągnęła (Maskarada). Dracena rzuciła na siebie czar (udany) korzystający z jej wiedzy technomantycznej i aspektu Spustoszenia. Jeśli nie może jeść, chce przynajmniej nie czuć tego, że jest głodna.

Jako, że Paulina wyczuła adoratorów i zainteresowanie Draceną, odciągnęła Dracenę do lasu. Tam wyjaśniła, że byliby goście. Dracena zauważyła, że zawsze są goście. Paulina westchnęła. Nie teraz...

Obiad jest przynajmniej spokojny. 
Podczas obiadu okazało się (znaczy, Dracena przyznała się), że Dracena nie lubi lekarzy. Nie pomogli jej siostrze. Nawet nie próbowali.

"Już tej lekarki nie lubię. Bo nie jestem dla Ciebie jedyna." - Dracena do Pauliny.

Paulina skierowała rozmowę na tematy bliskie Dracenie - cybergoth. Działo Plazmowe. Przyjaciele. Dracena się ożywiła i zaczęła dyskusje i opowiadanie... podczas tego wszystkiego jednak złapał ją atak paniki. Jest sama. Paulina powiedziała, że ma ją - za mało. Dracena jest trójaspektowa (Spustoszenie, Wiła, Diakon). Wszystkie 3 aspekty wymagają ludzi w pobliżu. Czysta empatia, podobieństwo do innych ludzi, biologia... nie może być sama. Gdy Paulina chciała ją przytulić, Dracena ją zatrzymała - nie chce, by się powtórzyło to, co poprzedniego dnia.

Tymczasem mają gości. Grupa młodych ludzi płci obojga; ściągnięta aurą Draceny. Paulina zgodziła się na małą imprezę, ale poprosiła Dracenę by ta się kontrolowała i uważała na siebie. Dracena obiecała. Paulina była w pogotowiu, ale aczkolwiek było momentami ciekawie (choć ubranie) to do niczego nie doszło.
To, co Paulinę zmartwiło to pełne już zarysowanie w Dracenie cech wiły. Jeszcze się kontroluje... ale jak bardzo?

Dzień 3:

Z rana przybywa Kinga Toczek. Dracena zostaje sama w domu w towarzystwie minionów Pauliny (cieni) a Paulina jedzie po Kingę.

Kinga porozmawiała z Pauliną - gdy dowiedziała się o stanie wzoru Draceny, strapiła się. Diakonka + Wiła to po prostu za mocna kombinacja. Zniszczy umysł młodej czarodziejki, zmieni ją w seks-zabawkę lub seks-drapieżnika. Niechętnie, ale zgodziła się z opinią Pauliny - muszą kształtować Spustoszenie w antytezie do powyższego. Aczkolwiek silne skupienie na tych aspektach osłabi moc magiczną Draceny...
Trudno. Przynajmniej będzie żyła i będzie "sobą".
Paulina wyprosiła Kingę, by na piersze spotkanie golemy ochronne zostały w samochodzie.

Tymczasem w domu Dracena robiła magią samodiagnostykę. Zaklęcie się nie udało; Dracenie zmieniła się percepcja. Jest (jak terminus) gotowa do walki; chce odeprzeć każdy atak i pokonać każdego wroga. Pełna paranoja. Zaczęła kreślić jakieś rytuały na ścianie...

Gdy Paulina i Kinga dojechały do domku, Paulina poczuła nienaturalną aurę. Coś się stało. Wyprosiła Kingę, by golemy (i ona na razie) zostały w samochodzie a sama poszła porozmawiać z Draceną. Dracena, umalowana w symbole wojenne jest gotowa do walki z całym światem. Paulina przypomniała jej, że ta obiecała. Dracena ze wściekłością próbowała walczyć, ale Paulina przypominała i szła "to nie ty". W końcu Paulinie udało się Dracenę przekonać, by ta dopuściła Kingę.

Kinga powiedziała, że ma kilka pomysłów jak można pomóc Dracenie. Nie będzie to łatwe, ale da się ją naprawić...

# Progresja

* Paulina Tarczyńska: już nie jest Skażona
* Dracena Diakon: uzyskała dominujący aspekt Wiły

# Streszczenie

Dracena jest niestabilna. Powoli jej wzór zaczyna się stabilizować w "coś" (z dominującym akcentem wiły). Paulina rozpaczliwie uniemożliwia Dracenie zrobienie komuś lub sobie krzywdy; do korekty wzoru ściągnęła Kingę Toczek do pomocy. Dracena powiedziała Paulinie o swej przeszłości - trzy siostry, jedna to cruentus reverto, Dracena kontrolowana przez Draconisa by nie poszła w mroczną magię.

# Zasługi

* mag: Paulina Tarczyńska, znękana i zmaltretowana lekarka Draceny, próbująca rozpaczliwie ją "naprawić". Ściągnęła Kingę do pomocy.
* mag: Dracena Diakon, kłębek chaosu, w której obudził się już aspekt wiły. Powiedziała Paulinie o swej przeszłości.
* mag: Bartłomiej Czyrawiec, kontakt Pauliny, który nadał jej Kingę Toczek - specyficzną acz skuteczną lekarkę terminusów.
* mag: Kinga Toczek, lekarka terminusów specjalizująca się w leczeniu ciężko Skażonych przypadków; nie lubi żadnej gildii.
* czł: Maria Newa, która wytrąciła Paulinę z błogostanu by ta była w stanie pomóc Dracenie mimo wszystko

# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Czarnoskał
                1. Marzanka, niewielka wieś agroturystyczna stosunkowo niedaleko Czarnoskała
                    1. Droga Gościnna, przy której znajdują się domki dla turystów i podróżników
                        1. Domek Zadumy, wynajęty przez Paulinę i Dracenę

# Wątki

 

# Skrypt

- brak

# Czas

* Dni: 3
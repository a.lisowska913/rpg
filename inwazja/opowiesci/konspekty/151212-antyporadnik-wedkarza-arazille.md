---
layout: inwazja-konspekt
title:  "Antyporadnik wędkarza Arazille"
campaign: powrot-karradraela
gm: żółw
players: kić, dust
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [160327 - Pięćdziesiąt oblicz Szlachty (SD)](160327-piecdziesiat-oblicz-szlachty.html)

### Chronologiczna

* [151202 - Zdobycie węzła Vladleny (HB, AB)](151202-zdobycie-wezla-vladleny.html)

## Punkt zerowy:

* Ż: Jaką korzyść z tej całej sytuacji może uzyskać KADEM przeciwko Świecy?
* B: KADEM uzyskuje przyczółek na obszarze pomiędzy KADEMem a Srebrną Świecą.
* Ż: Jaka akcja z ukrycia na pewno MOCNO uderzyła w Srebrną Świecę?
* K: Ktoś z członków Świecy zaatakował Powiew w taki sposób, że Powiew ma silne prawo do rekompensaty.
* Ż: O zdobycie czego poprosiła Emilia Hektora?
* D: Jakiś konkretny magitech zbudowany przez Tamarę (Wiktora).
* Ż: Dlaczego to coś jest gamechangerem całego konfliktu?
* K: Bo ten konkretny magitech poda tożsamości i cele wielu członków Szlachty.
* Ż: Co w tej chwili Szlachcie najbardziej przeszkadza w dalszej ekspansji?
* K: Uważają, że jest ktoś kto aktywnie zniechęca ich do dołączania się do nich. Myśleli, że to Kurtyna - jednak - jak widać - nie.
* Ż: Co w tej chwili zastąpiło Tymotheusowi Blakenbauerowi Crystal i aptoforma?
* D: An Ultimate Entity; nie w pełni działa bez Arazille i aptoforma. Nadal niebezpieczna broń, ale nie aż tak. "Ziarno Rezydencji".

## Kontekst misji:

- Wróciła Emilia i rzuciła bombę - magitech Wiktora ma przydatną wiedzę, klucz do zniszczenia Szlachty.
- Wiktor Sowiński i Diana Weiner dalej chcą współpracować z Hektorem. Wiktor dalej interesuje się Silurią.
- Laboratoria Blakenbauerów mogą być wykorzystane przez Szlachtę za zgodą Hektora i z planami dla Blakenbauerów.
- Blakenbauerowie wiedzą o obecności Tymotheusa w Kopalinie; Edwin szuka Tymotheusa.
- Hektor wniósł oskarżenie przeciwko Vladlenie o próbę Spustoszenia hipernetu.
- tien Adrian Murarz jest w Rezydencji Blakenbauerów; pomaga jak może.
- Olga ma dostęp do danych ze Skorpiona i może wykorzystać siły specjalne Hektora do rozwiązania mrocznych aktywności.
- Srebrna Świeca szuka Arazille.
- Diakoni zablokowani przez Mojrę; silny sceptycyzm co do działań Blakenbauerów (thanks, Romeo).
- Hektor chce zdjąć ograniczenie z Diakonów przeciw Blakenbauerom; ale jeszcze nie może
- Skubny zdobył koncesję na kanał telewizyjny. Edwin przyznał się że stoi za Skubnym.
- Hektor, Edwin, Marcelin i Margaret znaleźli sojuszników: Kacper Weiner i Esme Myszeczka.
- Blakenbauerowie chcą się skupić do ataku na Ozydiusza Bankierza by poprawić reputację swego rodu
- Tatiana użyła Fire Suit przeciw Hektorowi i przegrała.
- Węzeł Vladleny został przechwycony przez Blakenbauerów.

## Poprzednie spekulacje

Magitech - najpewniej Spustoszony. Hektor musi wziąć to pod uwagę. To byłoby ciekawe.
Emilia - najpewniej nie rozmawiała z Tamarą a z Saith Kameleon. Bardzo w stylu Agresta.
Szlachta dowiadując się o spotkaniu Silurii / Emilii utrudni relację z Wiktorem. Albo nawet w drugą stronę, Szlachta na pewno dowie się o tej rozmowie, więc Siluria tym bardziej udaje, że próbuje wykorzystać Emilię na korzyść Szlachty. Czyli Szlachta ma wierzyć, że Siluria próbuje zadurzyć w sobie Emilię - jako cel do Wiktora.
Najpewniej po akcie oskarżenia magowie Kurtyny zrezygnują ze współpracy z Hektorem i przejmie ich Emilia.
Siluria docelowo będzie chciała działać przeciwko Arazille; żeby Arazille nie było w okolicy.

Plany na przyszłość:
 
- wyciągnięcie magitecha (Siluria is on it)
- pokazać Dianie jaki Wiktor jest; doskonałe odwrócenie uwagi
- w skrócie: pełen chaos # chaos na poziomie Szlachty

## Plan graczy



## Dane

- Szlachta pracuje nad najpiękniejszym Archer Flower Of Secrets dla Silurii jaki jedynie istnieje.
- Pojawia się Jurij Zajcew, który pracuje w BlackLab nad Uwolnicielem.
- Jurij współpracuje z Diakonką imieniem Stokrotka; ona jest genialną katalistką

## Misja właściwa:
### Faza 1: 

Dzień 1:

Siluria poszła porozmawiać z Romeo odnośnie tego, kto wystawił Hektora. Romeo niespecjalnie się ociągając powiedział Silurii - Czirna Zajcew. Bardka, 4x-letnia czarodziejka, samotna, iluzjonistka syberyjska. Czirna podobno potrafi stosować "odwrotny pryzmat syberyjski" - sprawić, by coś stało się rzeczywistością przez użycie zarówno iluzji jak i syberionu. Czirna jest wciąż na "niedowadze przysług" wobec Diakonów, więc Romeo zupełnie nie rozumie o co tu chodzi...
Siluria dokładnie przepytała Romeo o co tu chodzi - skąd Romeo pomyślał w ogóle by pytać CZIRNĘ. Mimo, że ta obserwuje Blakenbauerów od dawna? Siluria dowiedziała się, że Czirna jest wywiadowcą i ogólnie interesuje się różnymi rzeczami. I okazało się, że Czirna "wkręciła" Romeo w tą sprawę i ona popchnęła Romeo do działania które prowadzi do najgorszej możliwej eskalacji. I zdaniem Silurii nie chodzi tu o "chichotka" (Edwina, zdaniem Czirny).
Siluria uświadomiła Romeo co zrobiła Czirna. Smutny Romeo. Siluria zaproponowała eskalację do Hektora; Romeo się zgodził.

Hektor w Rezydencji usłyszał głośne, rozpaczliwe NOOOOO! które pochodzi z okolic laboratorium. Niewiele czekając, zwinął Erebosa i Himechan ze sobą (quetzoalcoatl i chimera) i zszedł do laboratorium. A tam, pokaźny mąż opieprza Margaret. Że wzięła i wyjęła galaretkę (którą Margaret nazywa "Grzegorz" a on "Wolnościowiec") z Rezydencji i galaretka uległa Skażeniu emocjonalnemu. A w ogóle gdzie jest węzeł. Hektor zdziwiony; Margaret przedstawiła go jako Jurija Zajcewa. Jurij to głośny, wesoły i kłótliwy anarchista który "walczy z opresyjnym systemem" i chyba nie jest najlepszy w trzymaniu sekretów. Hektor aż skontaktował się z Dianą; potwierdziła, że Jurij jest "prawdziwy". Tak, to on - woła na Margaret "mine Liebchen" ze strasznym akcentem, głaszcze Himechan i Erebosa oraz jest głośniejszy niż torturowany mag. Ogólnie, chodzący niefart...

Jurij nie mógł zadzwonić z piwnicy; więc Hektor dał mu odpowiednio spreparowaną komórkę przez siły specjalne. Dzięki temu nagrywa sobie jego rozmowy, min. rozmowę z tajemniczą (bo nie było jej wcześniej) Stokrotką Diakon. Jurij pytał Stokrotkę jak wyczyścić galaretkę, co zrobić. Stokrotka powiedziała, że bez Węzła nie wie. Ale poszuka. Hektor facepalmował - to ILE osób wie, co tak naprawdę robi tu Szlachta i ile osób jest zamieszana w "sekretne sekrety"? Hektor się cieszy, że Jurij ma grupę kontaktów ze Szlachty na telefonie; wszystkie skopiował. Może się przydać przeciwko Szlachcie i można wzmocnić Kurtynę, dla odmiany...

Mojra została poproszona o zdobycie wiedzy o Juriju. Zrobiła załamaną minę. Się załatwi... to TEN mag...
Telefon od Silurii do Hektora. Prośba o spotkanie. Hektor jedzie na spotkanie do kawiarenki "Złotko"; w międzyczasie hodowana jest komora do testów dla Jurija...

Na spotkaniu Siluria powiedziała Hektorowi o Czirnie. Siluria nie chciała z Czirną rozmawiać; zostawia temat Hektorowi. Opowiedziała, że Czirna jest śmiertelnie niebezpieczna, choć niebojowa; jej słowa są mordercze. Hektor powiedział, że jeśli Czirna jest winna będzie w stanie zdjąć zakaz na Blakenbauerów. Siluria powiedziała, że przecież HEKTORA nic nie kosztowało go nałożenie - a nie potrzebował żadnych dowodów. Tak, ale trudniej zdjąć niż nałożyć...

Siluria podała lokalizację - 43 km od Kopalina, na uboczu, Klub Poszukiwaczy Szczęścia. Niedaleko Bażantowa.

Hektor poszedł zobaczyć się z Edwinem. Dowiedział się wpierw od Alfreda, że Vladlena na niego czeka; właśnie przyszła. Stwierdził, że się z nią spotka; ale najpierw - musi spotkać się z Edwinem. Poruszył z Edwinem temat Czirny. Edwin przyznał, że Czirna to stara sprawa; kiedyś Czirna miała spięcia z samym Ottonem i od tej pory "mistrzyni szpiegów" jest na uboczu (zdaniem Edwina, by nie prowokować lwa). Edwin nie wie, czemu Czirna się uaktywniła, ale uważa, że to nie kwestia Hektora. Czirna potrafi stworzyć każdy dowód; więc spotkanie Hektora i Czirny zdaniem Edwina jest całkowicie bezcelowe.

Hektor podziękował Edwinowi i poszedł na spotkanie z Vladleną.
A tam - niespodzianka. Vladlena, jakkolwiek słaba i blada, powiedziała Hektorowi co następuje:
- ona osobiście nie popiera działań Hektora ani wobec niej ani wobec jej Węzła
- Fire Suit wybrał; wpierw pomógł Hektorowi zdobyć Węzeł od Emilii a potem od Tatiany
- kim ona jest, by sprzeczać się z legendarnym Fire Suitem?
Vladlena, po raz kolejny, zaoferowała Hektorowi wsparcie gdyby jej potrzebował. Nieco skonfundowany Hektor kazał Janowi ją odwieźć...
Now THAT was unusual.

Hektor poszedł porozmawiać jeszcze na ten temat (Czirny) z Ottonem. Otton Blakenbauer powiedział, że Czirna i on faktycznie mieli spięcia w przeszłości; on ją odparł i ona uciekła. Czemu się uaktywniła? Otton nie wie, ale podejrzewa Tymotheusa. Dlatego ma zamiar zająć się Czirną osobiście. Hektor nalegał - ale Otton był nieubłagany...
Hektorowi trop został wyrwany z rąk. I prokuratorowi się to bardzo nie podoba.

Dzień 2:

Hektora obudziło głośne NOOOOOO i próby telefonu ze strony Jurija. Zszedł i pomógł mu zadzwonić do "swojej Stokrotki". Stokrotka powiedziała, że Jurij może potrzebować katalizatora; ona może mu go dostarczyć. Ale on ma dostać zgodę od Blakenbauerów. Hektor też usłyszał, że Stokrotka nie ma zbyt dobrej opinii o samych Blakenbauerach (jak wszyscy). Gdy Jurij miał wyjść, złapała go Margaret "Pyza" Blakenbauer; spytała czym jest katalizator. KREW SMOKA! Margaret zaświeciły się oczka a Jurij powiedział, że w sumie nie wie; Stokrotka wie. I poszedł. Margaret i Hektor mieli krótką rozmowę na ten temat, zwłaszcza, że Margaret wygadała się z istnienia Mojry.
...ale krew smoka? Skąd Diakoni to mają? I czemu Stokrotka pomaga Jurijowi?

Hektor poszedł porozmawiać z Mojrą. Poprosił, by ta jakoś załatwiła, by jednak udało się zdjąć zakaz z Diakonów. Mojra powiedziała, że będzie ciężko. Hektor powiedział, że to naprawdę robi się ważne. Zajmie jej to z tydzień.

Potem z Hektorem spotkał się Marcelin. Powiedział, że wszyscy sobie Hektora i jego (Marcelina) lekceważą. I wszyscy działają za ich plecami:
- Szymon Skubny współpracuje z Edwinem; Marcelin to podsłuchał przez telefon
- Marcelin uważa, że atak Czirny nie był skierowany w Blakenbauerów a w Diakonów
- Marcelin powiedział, że Czirna coś wie i nikt nie chce by Hektor lub Marcelin to wiedzieli
Hektor powiedział, żeby Marcelin zaufał profesorowi. Marcelin powiedział, że taki z nich jasny ród, że się własnego ojca boją. Ale - nie będzie robić niczego powiązanego z tą sprawą.
Choć Hektor widzi, że Marcelin jest wściekły.

### Faza 2: 

Dzień 3:

Zaczęło się niewinnie. Jurij wrócił do Rezydencji z bardzo silnie promieniującym artefaktem. To fiolka krwi. Rezydencja na zlecenie Edwina (i Hektora) uruchomiła skan... który wprowadził ją w katatonię. Otton wyłączył Rezydencję i wprowadził hard reset. Niestety, to wprowadziło Ottona w stan osłabienia; Otton nagle ma na głowie WSZYSTKIE czary i istoty i wszystko kontrolowane przez Rezydencję. Margaret usypia wszystkie chwilowo nieistotne istoty Rezydencji, by były bezpieczne i nie obciążały Ottona. Adriana też uśpili.
Tymczasem Jurij się awanturuje. On chce pracować. Hektor na niego krzyczy, Jurij krzyczy na Hektora, stanęło na tym, że Jurij dostał prototyp "Wolnościowca", bezpieczny kontener na fiolkę z dziwną krwią i poszedł swoją drogą.

Tymczasem Hektor skontaktował się z Dianą Weiner. Powiedział jaka jest sytuacja (choć nie powiedział jak strasznie uszkodzona została Rezydencja). Diana powiedziała, że "by design" (nie wiedząc co NAPRAWDĘ się stało). Hektor opieprzył Dianę za "Stokrotkę Diakon". Diana nie wie kim jest Stokrotka. Natychmiast zaczęła szukać jakichkolwiek informacji na temat Stokrotki.

Tymczasem Hektor ma plan. Otton ma się połączyć z Margaret i Marcelinem dla poszerzenia energii. Edwin chroni Rezydencję. On (Hektor) wysyła Oddział Zeta, Alinę i Dionizego by złapali i unieszkodliwili Jurija. W końcu - Jurij "odszedł i już nie wrócił". Mają go drapnąć.

Hektor szybko włączył konsoletę sterowania i zdecydował się wyszukać gdzie znajduje się telefon Jurija. Ekipa dostała przenośny lokalizator; a Jurij jedzie kilkanaście kilometrów poza Kopalinem. Do zupełnie innej miejscowości (ale nie w kierunku Czirny). Pewnie do swojego laboratorium.

Kluczem jest dorwać Jurija zanim tam dotrze.

Jako, że Hektor WIE gdzie jedzie Jurij i czym jedzie (szara Wołga), załatwił z drogówką, by Jurija opóźnili. Sami natomiast pojechali przeciąć mu drogę, zanim jeszcze dojedzie do Glina Żółtego.
Plan był prosty - robimy wypadek na drodze by Jurij się zatrzymał i Zeta go przechwyci. Informacja od Patrycji jednak zelektryzowała Dionizego - drogówka powiedziała, że samochód miał dwóch pasażerów. Mężczyznę i kobietę. I drogówka nie była w stanie powiedzieć wiele na ich temat; zdaniem Dionizego, użyto magii mentalnej na nich.

Nie wiadomo dość dobrze z czym mają do czynienia; najpewniej to Stokrotka Diakon i Jurij Zajcew. 

...

Akcja jest zaiste epicka. Specjalna, przyszykowana ciężarówka gotowa wyjechać na akcję "coś nam uciekło, trzeba to szybko złapać". Ruch wahadłowy na drodze PRZED pozycją szarej wołgi. W środku - 6 członków Zety i kierowca. 4 członków Zety, Dionizy i Alina są też poza ciężarówką.
Gdy tylko szara wołga podjechała za ciężarówkę, otworzyły się drzwi na pakę i wystrzeliła sieć - taka do łapania Bestii i innych. Trafiła w samochód i wciągnęła wołgę na pokład. Drzwi zostały zamknięte i wołga została uwięziona w lapisowanej ciężarówce.

Zeta zaczęła wpuszczać neuroparaliżujący gaz. Jurij padł na miejscu. Jego towarzyszka spokojnie otworzyła fiolkę z krwią... i zamanifestowała. Arazille (w ciele Crystal) zamanifestowała i uwolniła Zetę spod władzy Blakenbauerów, po czym poszerzyła swoje moce i przechwyciła też Alinę i Dionizego. Zeta, Dionizy, Alina - wszyscy weszli do ciężarówki i ruszyli dalej w drogę...

Hektor w szoku. Nie wie co się stało - nagle zniknęła Zeta, ciężarówka, Alina i Dionizy. Ale ma zdjęcie Diakonki z kamery...

Przerwał mu Marcelin. Poprosił Hektora, by ten się NIE odwracał. Co? No, żeby się NIE odwracał. Hektor był tak zaskoczony że się aż nie odwrócił i eliksir rzucony przez Wandę trafił. Hektor został obezwładniony i sparaliżowany, oraz jego moc została częściowo odcięta. Marcelin powiedział, że ucieka z Wandą - swoją prawdziwą miłością - i naprawi sytuację, pozna prawdę i uratuje ich ród. Hektor chciał go zwymyślać, ale nie mógł...

Gdy Marcelin już nie słyszał, Wanda mu powiedziała - Marcelinowi nic się nie stanie, jeśli "innym" nic się nie stanie (w domyśle: Zeta, Alina... Jurij i oczywiście tajemnicza Diakonka). Wanda cały czas kontrolowała sama siebie; raz dotknięta przez Arazille zawsze będzie wolna.
Otton, Edwin, Margaret - nagle na nich spadła cała kontrola Rezydencji. Nie mogli nic poradzić.

Marcelin i Wanda uciekli...
Dionizy, Alina, Zeta - przechwyceni przez Arazille...
Otton, Edwin, Margaret - wyłączeni...

## Dark Future:
### Actors:

Romeo (Narcissist)
Tatiana Zajcew (mechanika-inwazja-archetypy-explorer/ Explorer)
Diana Weiner (Cult Speaker / Scientist)

Emilia Kołatka (Silk Hiding Steel)

Edwin Blakenbauer (Sentinel)
Wiktor Sowiński (Dominator)
Diana Weiner (Cult Speaker / Scientist)
Tatiana Zajcew (Brawler/Explorer)
Tymotheus Blakenbauer (Dominator/Engineer)
Mojra (Cleaner)
Ozydiusz Bankierz (Warmonger)

Magowie Kurtyny (Społeczność dookoła celebryty)
Vladlena Zajcew (Warmonger)

### Faza 1: 

- Jurij kalibruje galaretkę; zawodzi (Scientist.make a very costly experiment which requires [blood_X] as a resource)
- Siluria przekazuje informację Hektorowi o tym, kto zdradził Romeo (Czirna Zajcew)
- Vladlena stwierdza, że Fire Suit wybrał Hektora (Warmonger.Adapt a plan to an unexpected [Loss of Node])

# Lokalizacje:

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Obrzeża
                        1. Rezydencja Blakenbauerów
                        1. Kawiarenka "Złotko"
            1. Powiat Myśliński
                1. Bażantów
                    1. Klub Poszukiwaczy Szczęścia
            1. Powiat Czelimiński
                1. Glin Żółty
                    1. Okolice
                        1. Droga w lesie

# Zasługi

* mag: Hektor Blakenbauer, któremu wszystko zaczęło się składać poprawnie... i dał się podejść Marcelinowi.
* mag: Siluria Diakon, która udzieliła Hektorowi kluczowej informacji, że to Czirna skłóciła Diakonów i Blakenbauerów.
* vic: Alina Bednarz, współautorka świetnego planu złapania Jurija z towarzyszką... i "złapała" Arazille.
* vic: Dionizy Kret, współautor świetnego planu złapania Jurija z towarzyszką... i "złapał" Arazille.
* mag: Czirna Zajcew, która była architektem rozbicia między Diakonami i Blakenbauerami, pośrednio bardzo po* magając Arazille.
* mag: Jurij Zajcew, podrywacz Margaret, głośny i genialny naukowiec współpracujący - nie wiedząc o tym - z Arazille.
* mag: Diana Weiner, która zaręcza za Jurija i jego prawidłowość. Nie wie o "Stokrotce Diakon"...
* mag: Margaret Blakenbauer, "Pyzusia kochana", podrywana niemiłosiernie przez Jurija. Współpracuje z Jurijem i próbuje zmaksymalizować zysk Blakenbauerów.
* mag: Marcelin Blakenbauer, który zdecydował się zaatakować Hektora by go obezwładnić. Uciekł z Rezydencji z Wandą, by "ratować swój ród".
* mag: Edwin Blakenbauer, opowiedział Hektorowi o Czirnie, podjął złą decyzję o skanie krwi Arazille a na końcu przejął część Rezydencji na siebie.
* mag: Otton Blakenbauer, kiedyś miał zatarg z Czirną i podejrzewa obecność Tymotheusa za jej działaniem. Przejął ciężar Rezydencji na siebie.
* mag: Romeo Diakon, powiedział wszystko Silurii i jest ciężko zraniony zdradą Czirny; nie chce mieć z Blakenbauerami nic wspólnego.
* mag: Vladlena Zajcew, która pogodziła się z tym, że Fire Suit wybrał Hektora a nie ją czy Tatianę; chce współpracować z Hektorem.
* mag: Mojra, która obiecała doprowadzić do rozejmu między Blakenbauerami i Diakonami w ciągu tygodnia.
* vic: Arazille, bogini ukrywająca się za fasadą "Stokrotki Diakon", zinfiltrowała Szlachtę oraz Blakenbauerów.
* czł: Wanda Ketran, agentka Arazille w Rezydencji Blakenbauerów, przekonała Marcelina do ucieczki.
* czł: Patrycja Krowiowska, agentka sił specjalnych Hektora która dostrzegła, że Alina i Dionizy mają "lost signal".
* vic: Erebos, quetzoalcoatl chroniący Rezydencję; jedna z "płaszczek". 
* vic: Himechan, chimera chroniąca Rezydencję; jedna z "płaszczek"
* vic: Oddział Zeta, przeznaczony do przechwycenia Jurija Zajcewa... złapał Arazille, która ich przechwyciła.

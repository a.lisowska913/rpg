---
layout: inwazja-konspekt
title:  "Złodzieje kielicha w akcji"
campaign: anulowane
gm: żółw
players: kić, paulina, grzegorz
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja:

## Punkt zerowy:

Ż - Interesujące lokacje w Ptaczewie
KGP - Fontanna ze skwerkiem, pobliski zamek, basen.
Ż - Interesujące postacie w Ptaczewie:
K - Pan Krzysztof, stary kociarz i miłośnik plotek.
Ż - Interesujące postacie w Ptaczewie:
G - Juliusz Jubilat, kolekcjoner starych przedmiotów i były duchowny.
Ż - Interesujące postacie w Ptaczewie:
P - Anna Patyczek, jubiler.
Ż - Unikalna lokalizacja w Ptaczewie jakiej nie powinno tu być
K - Katakumby w pobliżu, lecz nie pod zamkiem.
Ż - Gdzie po raz pierwszy się spotkaliście?
G - W sklepie warzywnym, i zorientowałem się, że włada magią.
Ż - W jakich okolicznościach powiedziałaś mu, że władasz magią?
P - Gdy jechaliśmy razem autobusem.
Ż - Jakie było jej pierwsze zaklęcie?
G - Błyskawiczny wzrost storczyka.
Ż - Jakie było jego pierwsze zaklęcie?
P - Zobaczył nadgryziony przez jego psa drogi kabel i go naprawił.
Ż - Gdzie spotyka się Wasza gildia?
G - Na basenie.
Ż - Jak przekonałaś mistrza Archibalda że basen jest idealnym miejscem spotkania?
P - Bo krzyczące dzieci i głośno - perfekcyjny screening i antypodsłuchowe.
Ż - W jakim kontekście problemów pomógł Wam mistrz Archibald?
G - Wyjaśnił, skąd się bierze magia i jak ją opanować.
Ż - Dlaczego zostaliście z Archibaldem a nie pojechaliście szukać szczęścia?
P - Bo on ma wystarczającą wiedzę.
G->K - Czego chce od nas mistrz Archibald?
K - Ma cichą nadzieję, że kiedyś być może umożliwicie mu wrócenie do świata magów.
P->K - Kto jest naszym głównym przeciwnikiem?
K - Oboje podpadliście lokalnemu biedagangsterowi.
K->Ż - Jak to się stało że Luna lubi właściciela i hodowcę szczekacza?
Ż - Bo zdaniem Kamili on potrzebuje pomocy.
G->Ż - Dlaczego Ptaczewo?
Ż - Bo tylko tutaj mistrz Archibald może udawać że dalej jest magiem.
P->Ż - Dlaczego lubimy sok pomarańczowy?
Ż - Ponieważ niweluje negatywny wpływ rezydencji Archibalda (Archiville) na Was.
K - Jak to się stało że teoretycznie nic nie znacząca sklepikarka jest w takiej dobrej komitywie z burmistrzem?
G - Kiedyś pomogła mu rozwiązać jego bardzo wstydliwy problem.
K - Jaki użyteczny artefakt znalazł Adam podczas dalszej wyprawy z psem?
P - Dziurawą reklamówkę, która dokonuje zniknięcia obiektu w środku.
Ż - Jak Aleksandra zabezpieczyła tą reklamówkę?
G - Sprawiła, że reklamówka działa tylko w rękach Aleksandry, Adama i Archibalda.
Ż - Kto przyszedł do Ciebie z niepokojącą informacją?
P - Policjant. Mariusz Błyszczyk.


## Misja właściwa:

Wczesnym południem do Aleksandry przyszedł policjant - Mariusz Błyszczyk. Spytał jej dyskretnie, czy wie coś na temat Janka Łobuziaka. Podpytany powiedział, że chyba Janek zszedł na złą drogę, ale nie wchodził w szczegóły. 
Blisko zamknięcia sklepu Aleksandra usłyszała jak dwie panie rozmawiają o Janinie Kielich, że to wielka tragedia dla jej rodziców. Powiedziały Aleksandrze z ochotą, że Janina ukradła bogato wyglądający kielich, od Ani (jubilerki). Zapytane powiedziały, że policjant znalazł ów przedmiot w jej plecaku. Jubilerka twierdziła, że Janina u niej w ogóle nie była, ale fakty są faktami.

Spotkanie na basenie. Mistrz Archibald opowiedział młodym neofitom o znaczeniu tkanki magicznej, o znaczeniu widzenia w czarowaniu, o Skażeniu oraz o burzach magicznych. W końcu zapytał o status report - czy coś się dzieje w okolicy o czym on powinien wiedzieć. Adam zaznaczył, że Janina wpadła w jakieś kłopoty z prawem i Aleksandra powiedziała co wie - że wpierw policja podejrzewała Janka Łobuziaka, ale stanęło na aresztowaniu Janiny Kielich. Bo u niej faktycznie znaleziono kielich Anny (jubilerki). Słysząc o Annie Patyczek mistrz Archibald podskoczył - to on kiedyś sprzedał artefaktyczny kielich Annie.

Co? Archibald przyznał ze wstydem, że to on sprzedał kiedyś Annie Patyczek kielich. Kiedyś. Zanim Aleksandra mu pomagała i się nim zaczęła zajmować. Potrzebował pieniędzy. Ale ten kielich, ten artefakt działa tylko dla maga - po nasyceniu energią magiczną danego typu (danego koloru, danego maga) będzie się do tego maga aportował na życzenie i wypełni się odpowiednim płynem zgodnie z żądaniem. Więc Archibald ma teorię - Janina Kielich jest protomagiem! Teoria nie do końca pasowała Aleksandrze na bazie tego co o niej wie, bo jej ojciec jest burmistrzem któremu kiedyś pomogła. (13v11->10) Niestety, nie jest w stanie potwierdzić czy zaprzeczyć, ale jest prawie pewna, że protomagiem NIE JEST.

Pojawiła się druga fajna teoria - że Anna Patyczek przechodziła z kielichem koło Archiville a potem Janina przechodziła koło Archiville i została napromieniowana. Nie jest to zbyt logiczna teoria, ale też warto sprawdzić.
Aleksandra zainteresowała się też postacią Juliusza Jubilata. Kolekcjoner starych przedmiotów i kiedyś duchowny. Mistrz Archibald powiedział, że owszem, Juliusz może i byłby zainteresowany kupnem kielicha, ale Archibald by się na to nie zgodził. Jeśli gdzieś jest jeden artefakt, najpewniej są też dwa. A kolekcjoner starych przedmiotów ma potencjalnie kilka artefaktów i gdyby napromieniował się i Juliusz i kielich... istnieje prawdopodobieństwo, że Juliusz mógłby uaktywnić artefakt, łamiąc Maskaradę.
Zdecydowanie nie jest to pożądana opcja.

Dobrze. Plan jest prosty - trzeba dotrzeć do Janiny Kielich i z nią porozmawiać. Aleksandra wie, że ojciec by nie dopuścił by siedziała w areszcie z łajzami i na pewno zapłacił kaucję. Za to Adam wie, gdzie normalnie chodzi Janina - Janina ma jamnika imieniem Brutus i często Adam spotyka się z nią na psich ścieżkach wyprowadzając swojego Loggera. W związku z tym pojawił się plan - Adam i Aleksandra pójdą na spacer z Loggerem i "przypadkiem" spotkają się z Janiną na psich ścieżkach.
Taki plan nie może się nie udać.

Faktycznie, spotkali się z Janiną. W pewien sposób zagrali z Janiną dość ciekawie - Aleksandra poszła "surową ciotką" korzystając ze znajomości z ojcem a Adam poszedł w "kto Cię wrobił". Krzyżowe przesłuchanie, (10+2v3/5/7->X) spowodowała że Janina pękła i odpowiedziała na pytania. Nie wie kiedy dokładnie pojawił się w jej plecaku skradziony kielich, ale wie, że pomoczyły jej się książki. Ekstrapolując jak działa artefakt, A&A doszli do tego - bardzo chciało się Janinie pić i wezwała nieświadomie kielich który wypełniony płynem zalał książki (i plecy Janiny). Później jak oglądała kielich w miejscu publicznym przypałętał się policjant i wziął ją ze sobą.
O! Czyli artefakt został aktywowany. 
Skąd wzięło się promieniowanie (jakoś nikt nie wierzy w protomaga) - może z Archiville? Nie, Janina powiedziała, że tam nie chodzi - mieszka tam jakiś podły snob który każe starszemu, 5x-letniemu panu mieszkać w budce dla służby (podpowiedź: to Archibald :P).
A była może w okolicach Juliusza Jubilata? Na pewno nie. Zdaniem Janiny on ma ten "creepy vibe" i Janina podejrzewa, że on na dysku kolekcjonuje całkiem inne rzeczy, "if you know what I mean".
No ok. Jak sprawdzić napromieniowanie by mieć pewność że to nie protomag? Srebrem. Jak zaboli, mamy napromieniowanie :p.

Więc Aleksandra poszła do jubilera (Anny Patyczek) po srebrny łańcuszek. Tam była świadkiem dziwnej sytuacji - Anna Patyczek wyraźnie się wzdrygnęła dotykając srebra. Czyżby była Skażona? Aleksandra kontrolnie poprosiła Annę o kilka innych łańcuszków i reakcja na strebro była wyrazista. Gdy Aleksandra pociągnęła Annę za język to się dowiedziała, że zaczęło się to 3 dni temu, po koncercie Kruków Nocy. Lokalnej biedametalowej kapeli. Co ciekawe, zgodnie z relacją Anny Janina także była na koncercie Kruków Nocy.
Aleksandra kupiła więc srebrny łańcuszek i pozostawiając Annę poszła do Janiny i Adama.

Gdy dotarła, podała łańcuszek Janinie. Ta się mocno oparzyła, na palcach pojawiły się bąble. Ok... to już bardzo silne Skażenie, takie, że pojawią się i nudności i inne objawy. Aleksandra i Adam zaczęli podpytywać Janinę odnośnie okoliczności koncertu, co tam się działo i dlaczego. Okazało się, że Janina niczego z samego koncertu nie zabrała, ale jest lekko zauroczona Jankiem. Bo okazuje się że "bad boy" Janek Łobuziak jest też gitarzystą zespołu "Kruki Nocy". A tej nocy "Kruki Nocy" krakały w lokalnej knajpie "Melina Świerszczyk" należącej do lokalnego biedagangstera. 
To co ważne - Janek miał nową gitarę. Inną niż zwykle. I to jest coś nowego. Potencjalny drugi artefakt?
I tu pojawiło się nowe pytanie, nowy problem - Anna Patyczek jest posiadaczką jednego artefaktu. Ekspozycja na drugi na koncercie. A jednak Janina jest dużo, dużo bardziej Skażona. Jeszcze jedno źródło? Spostrzegawcza Aleksandra zauważyła, że Janina trzyma rękę w kieszeni i coś kurczowo łapie.

Aleksandra przekierowała rozmowę z Janiną. Okazuje się, że Janina ma coś w kieszeni na szczęście. Aleksandra powiedziała, że ona ma trzy kamyki a Janina przekonana że nikt nic nie powie, cała czerwona powiedziała, że ona trzyma w kieszeni... czerwone majtki. Męskie. Ale czyste i podobno nieużywane. Dalej pytana powiedziała, że dostała je w prezencie od swojego chłopaka, spoza miasta. Bo Janina bała się strasznie egzaminu i on jej dał te majtki, powiedział, że pomogą w nauce... i jak to nie brzmi, pomagają.

Ok. Adam wpadł na chytry plan. Używając mocy skłonił Brutusa, by ten ukradł czerwone majtki (teraz na spacerze) i przekazał je Aleksandrze potem. Plan się udał, Adam został pocieszać płaczącą rzewnie Janinę a Aleksandra poszła "szukać psa". Niespodzianka! Znalazła. Dostawszy te majtki sprawdziła je jako artefaktor (12+2v11->17) - okazało się, że jest to produkt mało wprawnego maga który przekształca Aqua -> Alucinor Vis. Artefakt ten konwertuje siły fizyczne i energię życiową w skuteczność nauki, zapamiętywania i ogólnie działanie umysłu oraz jako efekt uboczny wzmacnia i wypromieniowuje magię.
ORAZ! Ten artefakt jest w stanie rezonansu z co najmniej dwoma innymi artefaktami. Aleksandra jeszcze nie spotkała się ze zjawiskiem rezonansu artefaktów, ale już jej się nie podoba. Natychmiast rozerwała rezonans; nawet, jeśli nie rozwiązuje to wszystkich efektów dokonanych przez artefakty (i wynikowy pojedynczy artefakt złożony będący wynikiem rezonansu), przynajmniej efekty się nie będą nasilać.

Aleksandra wróciła z Brutusem. Niestety, majtek nie udało się znaleźć :P. Tak czy inaczej, Adam i Aleksandra zdecydowali się skonsultować z mistrzem Archibaldem i odprowadzili Janinę do domu. Jeden artefakt jest już w rękach Zespołu; gorzej, że pozostały na wolności jeszcze co najmniej dwa (kielich i być może gitara)...

# Lokalizacje:


1. Ptaczewo
    1. Archiville
        1. domek dla służby
    1. Psie ścieżki w drodze na zamek
    1. Basen, centrum spotkań gildii...
    1. Sklep jubilerski Anny Patyczek
    1. Knajpa "Melina Świerszczyk"


# Zasługi

- mag: Adam Płatek, technomanta w podejrzanie dobrej komitywie z psem młodej dziewczyny.
- mag: Aleksandra Pudryk, pani z warzywniaka o aspiracjach parzenia ludzi przy użyciu srebra.
- czł: Archibald Bankierz, wiecznie optymistyczny i radosny organizator nie wpuszczany do własnej Archiville przez ową Archiville.
- czł: Juliusz Jubilat, kolekcjoner starych przedmiotów uznany za dość obleśnego przez Janinę.
- czł: Anna Patyczek, jubiler którą parzy srebro. Co za pech.
- czł: Janek Łobuziak, potencjalnie niewinny lecz grający na gitarze kindermetalowiec w wieku 21 lat.
- czł: Janina Kielich, grzeczna córka burmistrza która ma chłopaka i jest zauroczona kimś innym... a ojciec nic nie wie nawet o pierwszym.
- czł: Rafał Kielich, burmistrz Ptaczewa. Więcej tu sylab o nim w Dramatis Personae niż jego obecności na sesji.
- czl: Mariusz Błyszczyk, lokalny policjant który dyskretnie nic nie powiedział Aleksandrze. Bo nie pytała.

# Dark Future:

- Pojawia się terminus i sprząta. A Archibald nie ma magii i to może wyjść...
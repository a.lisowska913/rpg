---
layout: inwazja-konspekt
title:  "Uważaj, o czym marzysz"
campaign: powrot-karradraela
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Wątki

## Kontynuacja

### Kampanijna

* [160420 - Kolizja dwóch sojuszy (PT, HB, LB, KB)](160420-kolizja-dwoch-sojuszy.html)

### Chronologiczna

* [160417 - Symptomy kryzysu Świecy (AW)](160417-symptomy-kryzysu-swiecy.html)
* [160412 - Spleśniała dusza terminuski (SD)](160412-splesniala-dusza-terminuski.html)
* [160420 - Kolizja dwóch sojuszy (PT, HB, LB, KB)](160420-kolizja-dwoch-sojuszy.html)

### Inne

Misja konwergencyjna; łączy wszystko co działo się do tej pory i tworzy nowy początek i punkt startowy.
Misja kończy się wtedy, gdy kończy się misja:
* [160420 - Kolizja dwóch sojuszy (PT, HB, LB, KB)](160420-kolizja-dwoch-sojuszy.html)

### Misja właściwa:

## Kontekst misji
- Ktoś napuszcza na siebie Świecę i KADEM, Kurtynę i Szlachtę, Millennium i Świecę.
- Milena Diakon widziała Spustoszenie (?). Skąd i jak?
- Baltazar Maus będzie poświęcony na ołtarzu Rodziny Świecy i bezpieczeństwa (nie w tej misji).
- Pancerz Tamary i "kołysanka". Powiązanie ze Spustoszeniem?
- Adela Maus - zniknęła, Oktawian Maus - koma "od miecza", Dominik Bankierz nie żyje.

## Kontekst ogólny sytuacji

- Emilia rzuciła bombę - magitech Wiktora ma przydatną wiedzę, klucz do zniszczenia Szlachty. Pancerz magitechowy Tamary i "kołysanka". Powiązanie ze Spustoszeniem?
- Kurtyna jest rozbita i przygotowuje się na kontrdziałania. Lekki sojusz Kurtyna - Blakenbauerowie, w który Kurtyna nie wierzy. Dowodzi Emilia.
- Szlachta i Blakenbauerowie są w ścisłym sojuszu. Jest to też dość jawne w Świecy.
- Diakoni zablokowani przez Mojrę; silny sceptycyzm co do działań Blakenbauerów (thanks, Romeo). Akcja z Wandą Ketran nie pomaga; Estrella nie wybaczy.
- Oktawian, Adela Mausowie i Dominik Bankierz zniknęli lub zginęli na wykopaliskach.
- Baltazar Maus będzie poświęcony na ołtarzu Rodziny Świecy i bezpieczeństwa (nie w tej misji). Hektor pomoże z Baltazarem i Malią.
- Milena Diakon widziała Spustoszenie (?). Skąd i jak?
- Amanda Diakon / Pasożyt (Millennium) powoli wchodzą na teren Kopalina pod pozorem sił Mirabelki. Jej cel to Ozydiusz i CruentusReverto oraz Milena.
- Ciągła plaga Irytki Sprzężonej. Mechaniczni terminusi i viciniusy trzymają porządek. Ozydiusz odpalił Anioły Nadzorcze.
- Blakenbauerowie dali radę zdobyć dowody dla pogłaskania Ozydiusza; też zdobyli materiały i surowce częściowo z Kopalina używając płaszczek.
- Ozydiusz ma przeciwko sobie: Szlachtę (bo Leonidas), Blakenbauerów (bo Leonidas # Millennium), Millennium Amandy (bo CruentusReverto i Milena), Millennium Mirabelki (bo współpraca ze Szlachtą). Ma po swojej stronie: Emilię Kołatkę, Kompleks centralny (lojalistów). Yay.
- Arazille zniknęła; Kajetan Weiner bada sprawę "Krwawych Kuźni" w okolicach Czelimina (nadany przez Iliusitiusa).
- Mirabelka Diakon (Millennium) jest opłacana przez Wiktora i ma za zadanie pomóc Wiktorowi zdobyć kontrolę nad magami poza Kopalinem.
- Silny chaos na poziomie Świecy a na pewno na poziomie Diakonów przez Arazille, Irytkę i sytuację polityczną i wszystkie problemy "na prowincji".
- Ozydiusz trzyma wszystko w garści żelazną pięścią; ukrywa Spustoszenie (pojawiło się przy śmierci na wykopaliskach) i informacje o nim.
- Rodzina Świecy vs Mausowie wchodzą w kolejną fazę eskalacji. Elea i Rufus uciekają się do drastycznych działań; seiras Maus nie pomaga. Bankierze vs Mausowie.
- Marcelin jest oficerem łącznikowym u Ozydiusza. Ozydiusz / Blakenbauerowie mają sojusz; ale po ostatnich ruchach płaszczek sojusz jest niepewny. Blakenbauerowie złamią sojusz.

## Punkt zerowy:

- brak

## Misja właściwa:

Dzień 1: Andrea.

Noc minęła bez zbędnych wydarzeń. Andrea ustawiła opóźniony rozkaz (ma strzelić po kwarantannie) i ten rozkaz mówi, że Tadeusz Baran ma się stawić do jakiegoś psychologa. Andrea wskazała kogoś adekwatnego; kluczem jest to, by temat nie zniknął. Logika prosta - nie ma co tracić dobrych magów, a ten się jeszcze przyda.

Andrea zdecydowała się zostawić Mieszka a sama udała się w głąb Lasu Stu Kotów (w okolicach Piroga) by spotkać się z siłami Millennium. A dokładniej, z Felicją (Fortitią Diakon). Skontaktowała się z Aleksandrem Sowińskim; chce mieć informacje co i jak w czasie negocjacji. Aleksander powiedział, że Andrea ma poszerzone uprawnienia; dużo się dzieje. W okolicy większości ośrodków Świecy temperatura się podniosła i Świecy zwyczajnie brakuje magów i środków. Kopalin jest silnie zarządzany przez Ozydiusza; tu tego problemu nie ma. Nie takiego. Andrea wyraźnie się zmartwiła. Normalnie takie rzeczy byłyby robione ponad Andreą... można bezpiecznie założyć, że Aleksandrowi Sowińskiemu skończyły się moce przerobowe.

Andrea strzeliła zapytanie do Agresta; o co tu tak naprawdę chodzi? Wszystko wskazuje na to, że Millennium dąży do zaburzenia Świecy. Ona w to nie wierzy. Politycznie to nie ma sensu, ale tak to wygląda a centrala nie patrzy na lokalne uwarunkowania a przecież kompleks centralny ma moc. Skąd ten bunkier, po co i skąd Fortitia to wzięła? I Andrea wie, że Amanda chce dwóch rzeczy i jest skłonna jej to oddać - w zamian za pomoc w stabilizacji sytuacji i informację co Milena wie o Spustoszeniu?

Andrea wykorzystała surowiec by wspomóc się w negocjacji; mag Świecy i wykwalifikowany negocjator (under secrecy). Urok hipernetu.
Andrea przedstawiła się jako "Świeca, Wydział Wewnętrzny" i działa w majestacie uprawnień. Fortitia spotka się z nią w domku w Pirogu Dolnym.

Spotkanie z Fortitią. Wieczorem. W domku (takim z ogrodem), kontrolowanym przez Millennium, gdzie znajdują się ludzie. Fortitia wzięła przy Andrei pełną odpowiedzialność za to co się stanie w tym domku. Zapytana przez Andreę o techbunker, Fortitia odpowiedziała bez unikania tematu - tien Aurel Czarko, terminus Świecy, skontaktował się z nią i powiedział o TechBunkrze. Powiedział, że tam może zredukować przelew krwi i problemy spowodowane przez Ozydiusza z jego Kariatydą i niemożnością opanowania Szlachta/Kurtyna. Fortitia powiedziała, że z radością pomoże - bo to zredukuje problemy.

Andrei tien Czarko wypłynął już wcześniej; nie podoba jej się to. Ten sam gość robił coś z KADEMem...
Czyli ten Czarko poprosił o to Fortitię po to, by nikt nie mógł się do niej dostać i jej zmusić. Fortitia zauważyła, że będzie współpracować ze Świecą, bo jej nie zależy na destabilizacji Kopalina.

Andrea wie, że Fortitia jest czarodziejką, której Świeca kiedyś zrobiła krzywdę; przed Zaćmieniem Fortitia nie była Diakonką, była czarodziejką Świecy. Zaćmienie zabrało moc jej mężowi. Gdy się zgłosiła z prośbą o pomoc, odebrali mu pamięć. Wkrótce potem poroniła. Nie umiała sobie poradzić; pomogło jej Millennium i odeszła do nich by radykalnie zmienić swoje życie. Tam odnalazła "nowy świat".

Innymi słowy, Aurel Czarko zrobił research.

Andrea skupiła się na Fortitii. Czemu ona chce pomóc? Fortitia nienawidzi Świecy. Ale nie pozwoli, by krzywda stała się innym. Kopalin potrzebuje Świecy. Więc Fortitia nie będzie oszukiwać Andrei; będzie współpracować. Nie podoba jej się Irytka... ani to, że ktoś ją wykorzystał.

Andrea więc przeszła na kolejny etap negocjacji. Podziękowała Fortitii. Okazało się, że przełożonej Fortitii nie ma (Wiktorii); ona nie ma mocy wpłynąć na Templariuszy. Amanda działa według zasady "Ghost Division" a przez technomantów komunikacja komórkowa jest ograniczona. Jest tylko opcja poinformowania Edwarda Diakona i on może ma kontakt z Amandą; Fortitia się tym zajmie. Jako była czarodziejka Świecy rozumie, jak istotny jest fakt że działa tu agent Wydziału Wewnętrznego.

Dzień 1: Siluria

Nasze bohaterki pogodziły się jakoś z obecnością Sabiny. Wioletta, w lekkim szoku, nadal śledzi Sabinę i jej działania; patrzy też troszeczkę inaczej na Dagmarę (bo się też inaczej zachowuje). I podzieliła się tą myślą z Silurią. Dagmara trzyma Wiolettę trochę na dystans; nie pozwala jej się za bardzo zbliżyć. Kiedyś Wioletta by tego nie rozumiała, ale dzięki Silurii... i Romeo... zaczyna rozumieć niektóre rzeczy. Zdaniem Wioletty, Dagmara ją troszkę odpycha. Nie wie czemu.

Dodatkowo, Wioletta powiedziała, że hipernet szwankuje. Ma luki, dziury, zakłócenia... tak jakby coś go zagłuszało. Podobno są takie rzeczy w Laboratorium Mgieł, ale skąd Millennium lub Ozydiusz (dwa podstawowe typy Wiktora) miałyby do tego dostęp? 

Siluria powiedziała Wioletcie, że mogłaby zrozumieć o co chodzi, gdyby zobaczyła jak działa wobec niej Dagmara. Dla Wioletty - żaden problem. Poprosi Dagmarę o sparing.
I faktycznie, Wioletta poprosiła Dagmarę o sparing, gdy w pobliżu była Siluria. Dagmara odmówiła; Siluria zauważyła, że Dagmara gra na odsunięcie od siebie Wioletty. Dagmara coś wie. Powiedziała Wioletcie, że ta mogła być wielka, ale wybrała Silurię i kralothy. Wioletta ją spoliczkowała i Dagmara włączyła pełną moc power suita pokazując potęgę tego co posiada. Odepchnęła Wiolettę bez żadnego problemu.

Siluria zdecydowała się zaatakować Dagmarę słownie, chcąc złamać jej zimny, logiczny plan. Chcąc nią wstrząsnąć. Chcąc jej uświadomić, że potrzebuje pomocy. Że z nią dzieje się coś strasznego. I że nie może iść do Wiktora, że musi to zrobić inaczej. Udało się; Dagmara nic nie powiedziała, ale cała mowa ciała (mimo power suita) wskazuje na to, że Dagmara jest przerażona i załamana. Po prostu odeszła.

Siluria zabrała ze sobą Wiolettę i ją uspokoiła. Zdrada przyjaciółki dotknęła terminuskę bardzo głęboko. Siluria domyśla się, że ten power suit zawierający elementy magii krwi jest WREDNY - tak wredny, że Wiktor nie chce dotykać czy destabilizować Dagmary. Dagmara na pewno chciałaby się go pozbyć asap. Najpewniej boi się, że przez power suit może zabić koleżankę; boi się tego, czym się stała.
Oczywiście, Siluria nie powiedziała tego Wioletcie. 

Laragnarhag potwierdził, że jest tyle bólu w okolicy; bardzo mu to smakuje. 
Z pomocą kralotha Siluria wyprodukowała zabawki i narzędzia, które nie są może super kralotyczne, ale są bardzo przydatne; dedykowane rzeczy na Wiktora by móc wpływać nań skuteczniej. I na Sowińskich. Kraloth zauważył, że w wypadku Sabiny afrodyzjaki nie zadziałają, bo "spleśniała". Ciało zadziała, ale do osoby nie dotrze. Pleśń pożarła wszystko. Dla Silurii ważna obserwacja - cokolwiek Sabina nie robi, symuluje. Emocji w tym nie ma.

Tego Wiktorowi Siluria nie powie (bo nie wie czy Wiktor wie), ale zamierza wybadać - czemu Sabina symuluje, że ma uczucia?

Wieczorem, z Wiktorem, Siluria x Wiktor. Sami. Wiktor aż tryska humorem. Siluria zaczyna od troski o Wiktora i pokazuje mu, że dba o niego; myje go w wannie. Bez problemu wypytała o to, dlaczego jest taki szczęśliwy - Diana Weiner przeszła przez portal. Skontaktowała się z Aleksandrem Sowińskim. Następnego dnia przejdą przez portal i tu będą; wtedy wygrają.
Siluria powiedziała, że nigdy Wiktora o nic nie prosiła, ale dziś chciałaby wypróbować czegoś nowego. Miażdżący sukces; Wiktor stwierdził, że jak są sami, to mniej przeszkadza mu to, że czasem to on jest przypięty. Siluria zachichotała w duchu, dzięki, Laragnarhag :-).

Dzień 2: Andrea.

Rano obudził Andreę rozbłysk magiczny, a potem drugi. Magowie walczą, w obszarze czujników. Po chwili zaklęcia wygasły. Jedna sygnatura to Baran. Druga nieznana. Czujniki przesłały aurę; identyfikacja: nie mag Świecy. Andrea odpaliła alarm bojowy i wysłała Mieszka na pole bitwy; sama zaczęła przygotowywać pułapkę w bazie. Po chwili Mieszko wysłał informację, że problem rozwiązany. To wędrowny terminus, wpakował się na czujniki o których nie wiedział i lekko zbyt nerwowo zareagował na Barana (albo Baran na niego). Starcie było nierozegrane; oboje poszli defensywnie.

Terminus przedstawił się jako Kajetan Weiner.

Kajetan mówi tak, by nic nie powiedzieć. Nie da się na nim niczego wymusić. A jak Kajetan pięknie opowiada i bajeruje. 
Andrea ujawniła się i weszła by z nim porozmawiać. Kajetan - bardzo ostrożnie i defensywnie - zastanawia się, czy oni są patrolem terminusów i czegoś szukają? Sam się nie przyznał, po co tu jest; zagłuszył wszystko w swoich opowieściach. 
Andrea wypunktowała mu kilka nieścisłości; po co tu jest?
On zauważył, że trzech terminusów w taki sposób; nie jest to standardowe modus operandi Świecy.

Po krótkiej kłótni magowie doszli do porozumienia: Kajetan nie chce z nimi rozmawiać i wątpi, czy są magami Świecy (choć arogancja świadczy, że tak). Andrea pokazała sygnaturę Świecy. Kajetan sformował (z trudem) sygnaturę rodu. Wyraźnie nie robi tego często. Kajetan powiedział Andrei, że szuka "Krwawej Kuźni". Nie do końca wie co to jest (a hipernet jest cicho), źródło ma takie... co nie może powiedzieć (choć on mu wierzy) a Kuźnia jest ukryta tak, że magowie jej nie znajdą.
Andrea powiedziała, że widać, jak to wygląda. Kajetan potwierdził.

Andrea puściła agenta Świecy na to (26). Dostanie zagregowaną odpowiedź wieczorem. Kajetan Weiner zostawił Andrei numer telefonu i poczłapał w siną dal. Dostał informację przybliżoną, który teren jest pod kontrolą Świecy.

Andrea dostała wiadomość od Ozydiusza. Sygnał hipernetowy jest słaby i ma przekłamania sygnału. Ozydiusz powiedział, że Bianka Stein już szuka co się stało; ogólnie, sygnał hipernetowy jest fatalny w Kopalinie; coś w Kompleksie Centralnym zakłóca komunikację. Ozydiusz powiedział, że podmienił magitech Wiktora Sowińskiego; przesyła Andrei dane z tego magitecha, bo on ma za dużo na głowie; nie ma czasu się tym bawić ani się tym zajmować. Niech Andrea prześle to dalej. 
Czyli Andrea posiada zbiór WSZYSTKICH ruchów Wiktora Sowińskiego gdy miał na sobie magitech. I sporą część komunikacji; plus to, co magitech sam usłyszał.

Tą informację Andrea przesłała Agrestowi. Przetransmitowała mu też dane z magitecha. Agrest powątpiewa, że warto to teraz dekodować (bo nie ma sił na wykonanie jakichkolwiek ruchów); Andrea mówi, że jednak może warto. Agrest się zgodził. Zabierze się za dekodowanie (nie osobiście).

Wieczorem, Andrea dostała dwie prawie jednoczesne informacje. Jedna pochodzi ze Świecy, "Krwawe kuźnie". Ten termin pochodzi etymologicznie z dawnych czasów; przede wszystkim z zasobów wiedzy Mausów. Krwawa Kuźnia jest formą artefaktorium gdzie torturuje się ludzi lub/i magów by stworzyć artefakty przy użyciu esencji torturowanego bytu. To jest następnie kalibrowane przez bardzo precyzyjnego i potężnego defilera. Praktycznie się tego nie spotyka, bo NIE MA takich defilerów zdolnych kontrolować Kuźnię. Sama Kuźnia jest miejscem przesiąkniętym Pryzmatem.
Sam artefakt wyprodukowany w takiej Kuźni jest zależny od woli swego twórcy.
To cholerstwo promieniuje jak mało co. By to ukryć, trzeba to albo zamaskować innym pryzmatem albo zrobić pieczarę z lapisu.

Druga wiadomość - telefon od Fortitii. Powiedziała, że Edward Diakon się będzie chciał spotkać z Andreą. Następnego dnia. Niestety, może zająć to większość dnia; kalibracja komunikacji z Amandą zajmie trochę czasu. Andrea się zgodziła i ucieszyła. Zwłaszcza po tym co usłyszała należy jak najszybciej zakończyć potencjalne bezsensowne potyczki.

Andrea zadzwoniła do Kajetana. Chce się spotkać; nie wysyła się kolegi terminusa na potencjalną śmierć. Powiedziała mu wszystko o Kuźni. Kajetan się stropił. Zapytany jeszcze raz, powiedział, że wszystko wskazuje na okolice Czelimina.

Andrea wydała rozkazy znajdującemu się niedaleko Czelimina skrzydłu szkoleniowemu. NATYCHMIAST mają się stawić w Pirogu; są oddelegowani do dyspozycji Mieszka Bankierza. Dowódca, Julian Pszczelak, natychmiast posłuchał rozkazów...

Dzień 2: Siluria.

Wiktor lubi, żeby jego wrogowie cierpieli. Siluria próbuje z niego wyciągnąć co tak naprawdę czuje Sabina; czy cierpi? Czy też może jest otępiona? Wiktor w dobrym humorze powiedział, że jest przekalibrowana, by służyć jemu.
...so fucking false. Wiktor nie ma pojęcia, co zrobił i co się dzieje...
Wiktor nie powie Silurii co zrobił. Za to oferuje jej jakieś włości czy ziemie dla KADEMu. Siluria powiedziała, że chciałaby sobie powybierać (bo nie chce nic wybrać by nie było, że ona jest winna), on się tylko zaśmiał rubasznie.

Późnym popołudniem Siluria usłyszała radosny śmiech Wiktora. Zawołał Silurię. Siluria jest świadkiem tego, jak Dagmara klęczy przed Wiktorem po skończonym raporcie. W kącie siedzi Elea z twarzą wyglądającą jak martwa. Wiktor kazał Dagmarze zreferować ponownie; Dagmara powiedziała, że Aleksander Sowiński jest pod kontrolą Wiktora. A fakt, że jest spokrewniony z Sabiną mu nie pomógł. Aleksander zostawił warianty awaryjne by nikt nie zauważył, że znika; przez to mają więcej czasu.

Elea miała wybuch paniki, śmiechu i histerii. Gdy Wiktor chciał, by Siluria ją uspokoiła, Dagmara zgłosiła się na ochotniczkę. Jednak Elea powiedziała, że ona chce porozmawiać z Silurią. I to zrobiła.
Na osobności ostrzegła Silurię, by ta niczego nie zrobiła. Nie wchodziła w drogę. Nie przeszkadzała. Nie pytała. Powiedziała, że ona (Elea) jest lekarzem i spróbuje uratować wszystkich. Niech SIluria chroni Wiolettę. Niech przycupnie albo ucieka - jeśli Siluria przycupnie lub ucieknie, będzie bezpieczna. W innym wypadku zostanie zniszczona. Elea jest zupełnie jak nie ona. Przerażona, marzy, by Oktawian się obudził, oraz "jej świat pękł". 

Siluria się naprawdę zmartwiła...

Dzień 3: Andrea

Ku wielkiej radości Andrei skrzydło szkoleniowe jest w komplecie i się dostosowało do poleceń Mieszka. Skrzydło też nie wykryło żadnej "Kuźni". Kajetan dalej szuka pod kątem świata ludzi, plus nie działa sam; okazuje się, że raportuje swoją pozycję co godzinę na wszelki wypadek. Andrea też mogłaby szukać pod kątem świata ludzi, ale chwilowo jest zajęta innymi tematami; między innymi, spotkaniem z Edwardem Diakonem.

Edward Diakon nie przyszedł sam. Przyprowadził kralotha.
Wyjaśnił Andrei, że to jest narzędzie komunikacyjne; można się połączyć z kralothem i w ten sposób zarezonować z kralothem po drugiej stronie. W ten sposób jeśli Andrea się połączy z tym kralothem to będzie mogła porozmawiać z Amandą połączoną z tamtym kralothem. A Edward wie, że Andrea ma doświadczenia z kralothami. Przysiągł na Millennium, że to będzie wykorzystane jedynie jako środek komunikacyjny.

Jemu nie zależy na wojnie ani Andrei nie zależy na wojnie. 

Andrea (niezbyt chętnie, ale czego nie robi się dla Wydziału) zdjęła z siebie ciuchy i dała się pochłonąć kralothowi. Przynajmniej to przyjemne. Połączyła się z Amandą.

Amanda postawiła sprawę jasno, jak Edward:
- CruentusReverto i Milena Diakon trafiają do Millennium.
- Amanda się wycofuje i oddaje wszystko co zabrała.
- Amanda oddaje wszystkich magów, jakich przeprowadziła przez kwarantanę.

Andrea chce rozwiązać ten temat w sposób maksymalnie pokojowy, ona ze swojej strony oczekuje, by Millennium pomogło w rozwiązaniu problemów które stworzyło (min. opanowanie części terenu; niech tam zapewnią bezpieczeństwo). Andrea chce, by Świeca wyszła na tym jak najlepiej, choć niekoniecznie kosztem Millennium.

Andrea: +6 (negocjator), +2, 13 -> 21
Amanda: +3 (kraloth-comms), 15 -> 18

Amanda się zgodziła. Ale Ozydiusz się z nią skontaktuje i zaakceptuje jej warunki. Za to Amanda pomoże Andrei i przekaże wszystko, co Milena wie o Spustoszeniu.

Czas, ku nieszczęściu Andrei, skontaktować się z Ozydiuszem.
Andrea nacisnęła mocą Wydziału Wewnętrznego. Ozydiusz jej nie musi lubić. Ale zrobi, czego ona chce. Niech Ozydiusz pójdzie tam porozmawiać i uzgodnić szczegóły przekazania. Amanda wie i jest nastawiona pokojowo...
Ozydiusz stwierdził, że weźmie wzmocnioną obstawę.

Dzień 3: Siluria

Siluria zdecydowała, że nie będzie ryzykować swojego życia i, co najważniejsze, Wioletty.
Siluria zauważyła, że przyniesiono dziwny artefakt, który wziął Aleksander Sowiński. To jest wyraźna bomba magiczna. Wygląda jak klasyczny przykład bomby rozrywającej energię magiczną. 
Sabina z rozkazu Wiktora opuściła Kompleks. Poszła do Ozydiusza, jako strażniczka. W pełnej szlachetności i godności.

Siluria zasugerowała Laragnarhagowi, by ten się wycofał. Kraloth nie do końca może; jest jeszcze potrzebny Wiktorowi jeden dzień. Siluria zdecydowała, że chce przekonać Wiktora Sowińskiego do wycofania kralotha; nie zmieni to już skali kosmicznej, ale może kralotha uratuje. W sytuacji na osobności (seks), Siluria zaargumentowała, że "tamto coś" wyglądało jak bomba i Siluria nie chce, by kraloth ucierpiał, bo są delikatne na takie rzeczy. Wiktor bardzo nie chciał (bo Dagmara/sojusznik, bo plan...), ale Siluria go przekonała. Niech odeśle kralotha w ostatnim momencie. Ale niech go odeśle.

I Wiktor się zgodził.

Punkt konwergencji:

- Ozydiusz, Sabina oraz Kermit poszli do "Rzecznej Chaty".
- Marcelin i Hektor i Leonidas też tam byli. Cały czas komunikowali się NIE z siłami Amandy a Mirabelki (która współpracuje z Sowińskim... a raczej sojusznikiem).
- Mirabelka odpala szrapnel. Amanda nic nie wie. Ale wie, że to nie jest atak Ozydiusza.
- Przy Bestiach Blakenbauerów, podczas walki, Sabina zabija Ozydiusza srebrem LUB arildisem.
- Widząc poziom zagrożenia, jakaś Bestia (Hektor) zabija Sabinę. Spustoszona terminuska nie zdąża wyłączyć Spustoszenia. Zostają ślady.
- Amanda opanowała sytuację ze swoimi siłami i z Mirabelką, która za cholerę nie wiedziała, co się tu stało.
- Millennium zwiera szyki i Amanda NATYCHMIAST informuje Andreę.
- Przez portal przechodzi Spustoszony Aleksander Sowiński, do Świecy Daemonica z bombą.
- Sowiński wysadza bombę tej samej klasy co zdestabilizowała konektor z KADEMem i rozerwał portal do Kopalina; sam zginął.
- Wiktor Sowiński jest w pełnej panice...
- Wiktor zaatakował Dagmarę, wściekły, krzycząc "coś ty zrobiła!!!". Dagmara się uśmiechnęła. Oddała Wiktorowi Kopalin, tak, jak sobie tego życzył.
- Siluria jest przerażona. Andrea też. Komunikacja hipernetowa się jeszcze bardziej pogorszyła.

# Zasługi

* mag: Andrea Wilgacz, dostała poszerzenie uprawnień od Sowińskiego 
* mag: Siluria Diakon, która z przerażeniem obserwuje rozwój sytuacji i próbuje rozpaczliwie ratować co się da. Związała ze sobą mocno Wiktora Sowińskiego.
* mag: Tadeusz Baran, którego Andrea chce wysłać do psychologa by mu pomógł. Związał walką Kajetana Weinera i ujawnił go Andrei.
* mag: Fortitia Diakon, eks-czarodziejka Świecy która znalazła pokój w Millennium. Współpracuje z Andreą, bo nie chce wojny w Kopalinie; poinformowała o Aurelu Czarko.
* mag: Aleksander Sowiński, który przekazał Andrei poszerzenie uprawnień. Wciągnięty przez Dianę w pułapkę, dał się Spustoszyć i wysadził bombę pryzmatyczną w Świecy Daemonica. KIA.
* mag: Ozydiusz Bankierz, wysłał dane z * magitecha Wiktora do Andrei, wynegocjował z Andreą i Amandą warunki pokoju i zginął z ręki własnej ochrony - Spustoszonej Sabiny Sowińskiej.
* mag: Marian Agrest, z którym Andrea określa sytuację geopolityczną, ale który na tym terenie niewiele może zrobić.
* mag: Amanda Diakon, która ustaliła zasady pokoju z Ozydiuszem przez Andreę i gdy poszła na ustalenia, Ozydiusz zginął. Jej siły opanowały sytuację i skomunikowała się z Andreą.
* mag: Aurel Czarko, źródło informacji o TechBunkrze dla Fortitii Diakon. Okazuje się, że może być bardziej zamieszany niż się wydawało.
* mag: Wioletta Bankierz, zrozpaczona po utracie Dagmary i chroniona przez Silurię przed złem tego świata. Zaczyna wykazywać inicjatywę.
* mag: Sabina Sowińska, Spustoszona i zniewolona; zabiła Ozydiusza będąc jego eskortą; w reakcji zginęła z odgryzioną przez Hektora głową. KIA.
* mag: Wiktor Sowiński, który nie miał pojęcia, co zrobił ze Spustoszeniem i Dagmarą... doprowadził do uszkodzenia Świecy i śmierci Sabiny, Ozydiusza, Aleksandra. Przerażony, obejmie władzę...
* mag: Dagmara Wyjątek, która jest przerażona tym czym się stała i co robi. Spustoszyła Aleksandra i Sabinę, przejęła komunikację z Mirabelką i dowodziła operacją... dla Wiktora? Odpycha przyjaciół.
* vic: Laragnarhag, kraloth, którego Siluria wycofała w ostatniej chwili i który jest świadkiem i pomocą Silurii w sprawach powiązanych ze Szlachtą.
* mag: Diana Weiner, złamana i podległa Wiktorowi; ściągnęła Aleksandra Sowińskiego do Kopalina na Spustoszenie przez Dagmarę.
* mag: Kajetan Weiner, szukający Krwawej Kuźni * mag nie należący do Świecy; ostrożny, powiedział Andrei co wie na temat Kuźni gdy upewnił się, że ta z Wydziału Wew.
* mag: Ryszard Weiner, * mag analityk Wydziału Wewnętrznego Świecy, który dostarczył Andrei (poproszony) informacje o "Krwawej Kuźni".
* mag: Edward Diakon, jeden z Triumwiratu, który zapewnił Andrei możliwość kralotycznego kontaktu z Amandą celem negocjacji.
* mag: Julian Pszczelak, głównodowodzący treningowym skrzydłem terminusów w okolicach Czelimina; oddał się do dyspozycji Mieszka z rozkazu Andrei.
* mag: Elea Maus, która uświadomiła sobie COŚ co prawie wpędziło ją w komę, ale jako lekarz chce wszystkich uratować; demonstracyjnie wzięła Silurię w opiekę
* mag: Mirabelka Diakon, odpaliła szrapnel w Dziale Plazmowym co doprowadziło Blakenbauerów do wejścia w tryb Bestii i śmierci Ozydiusza i Sabiny.
* mag: Hektor Blakenbauer, w formie bestii odgryzł Sabinie Sowińskiej głowę (przez co nie zdołała wycofać Spustoszenia)

# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Czelimiński
                1. Czelimin
            1. Powiat Kopaliński
                1. Piróg Górny
                    1. Obrzeża
                        1. Skład budowlany Żuczek
                            1. Ukryta kwatera dla skrzydła terminusów Świecy
                    1. Las Stu Kotów
                1. Piróg Dolny
                    1. Ulica Rzeczna
                        1. Domek Millennium
                1. Kopalin
                    1. Centrum
                        1. Cyberklub Działo Plazmowe
                        1. Kompleks centralny Srebrnej Świecy
                            1. Kompleks pałacowy
                                1. Skrzydło Sowińskich
                                    1. Pokoje gościnne
                                    1. Buduar Wiktora
    1. Faza Daemonica
        1. Świeca Daemonica

# Czas

* Dni: 3
---
layout: inwazja-konspekt
title:  "Ołtarz Podniesionej Dłoni"
campaign: czarodziejka-luster
players: kić
gm: żółw
categories: inwazja, konspekt
---

# {{ page.title }}

## Punkt zerowy:

## Kontynuacja

### Kampanijna

* [130506 - Sekrety rezydencji Szczypiorków (An)](130506-sekrety-rezydencji-szczypiorkow.html)

### Chronologiczna

* [130506 - Sekrety rezydencji Szczypiorków (An)](130506-sekrety-rezydencji-szczypiorkow.html)

## Misja właściwa:

- Ranek. Kasia dostała informacje od "swojego" studenta odnośnie znaczenia symboliki medalionu. Na kości szczęścia wypadło 19/20. Student nie odwalił fuszerki, naprawdę zrobił dobrą robotę - symbolika medalionu została Kasi zdradzona. Dodatkowo, Kasia poznała kilka dodatkowych faktów - około 100 lat temu ten zakon bardzo wzrósł w siłę (wszystko wskazuje na obecność magów, dobrobyt itp) a potem drastycznie spadł w mocy i został doszczętnie zniszczony (znowu wskazuje na działania magów).

- Kasia i August się biją z myślami. Zgodnie z procedurami, August powinien to eskalować i zostawić. Jednak... Kasia zaznacza, że przecież jeśli Żanna jest magiem kochającym człowieka lub po prostu jeśli jest magiem nieświadomym swej mocy, to zniszczą życie całej rodziny. August się z nią zgadza. Skoro Kasi można zaufać, może ta procedura nie jest tak krytyczna...
Jednocześnie Kasia i August dochodzą do wniosku, że chyba tylko Żanna i Augustyn mają coś wspólnego z symbolem medalionu. Jednak trzeba porozmawiać z Żanną, ale na neutralnym gruncie - nie na jej terenie. Jak ją wyciągnąć? Może powiedzieć, że chce porozmawiać o wnuczce?
W końcu Kasia wpadła na pomysł. Wpleść w prośbie o spotkanie coś ze słownictwa specyficznego dla tego zakonu. Może to ją wywabi, jeśli jest świadoma tego.
Jednak nie wysłała listu Radosławowi.
Mag postawił im obu tarcze: zarówno fizyczne jak i magiczne/mentalne. Kasia była nieco sceptyczna - nie może użyć srebra, ale tak jest bezpieczniej. Idą na spotkanie z nieznanym magiem.

- Miejsce spotkania: park. Ludzie w pobliżu, ale nie bezpośrednio obok. Żannę przywiózł lokaj, po czym się oddalił. W rozmowie wpierw Kasia próbowała wybadać Żannę pod kątem jej lojalności (magowie - kościół) i nie udało jej się (2 vs 3 sukcesy), potem Żanna Kasię i też jej się nie udało (2 vs 3 sukcesy). Terminus i Kasia przedstawili się tak, że Żanna nie miała jak zidentyfikować czegokolwiek.
Czyli żadna z nich nic nie wie o drugiej stronie.
August nachylił się do Kasi i szepnął, że w tym parku jest lokalny terminus rezydent Świecy. Skąd wie? Przed każdą akcją w terenie August uczy się wszystkich lokalnych terminusów i ich wyglądu na pamięć. Terminus nie powinien być w stanie podsłuchać Augusta...
Kasia dowiedziała się od Żanny, że ta jest czarodziejką od urodzenia. Wymusiła konfliktem informację, że Żanna niespecjalnie zna się na realiach świata magii i że myśli o sobie jako o czarodziejce podległej kościołowi. Żanna uważa, że kościół powinien dominować nad magami.
Żanna za to konfliktem wymusiła informację, że terminus jest mało ważny i nie ma silnego zaplecza ani wsparcia. Ale jeśli coś się mu stanie, przyjdą następni - Świeca nie opuszcza swoich. 
Kasia ostrzegła Żannę, że Indze grozi niebezpieczeństwo. Ona się przekształca z czasem. Skażenie postępuje i Inga jest coraz mniej ludzka.
Żanna się tu odsłoniła - nie wie, z czego to wynika. Na pytanie czy w domu jest źródło mocy odpowiedziała, że nie wie. Jest wyraźnie magiem nieszkolonym, nie potrafi widzieć mocy (nie uczyła się), nie wie, czy w domu coś się zmieniło od czasu gdy Inga się zmienia.
Była przez kogoś uczona. Sama przyznała, że "kilkoro sług bożych" pomogło jej opanować "klątwę" (Skażenie) dotykającą rękę. Zwłaszcza przy użyciu ołtarza.
W rozmowie z Żanną Kasia zauważyła, że Żanna ma silne znamiona fanatyzmu.
Kasia raz jeszcze przycisnęła Żannę. Kto porwał Samirę? Podała Żannie scenę - sama, w ciemnym pomieszczeniu, zakneblowana (...). Żanna przyznała - to Radosław porwał Samirę. Nie chciał by "coś" [wizerunek obrazu - przypisek Żółwia] wyszło na jaw.
Radosław, zdaniem Żanny, zrobił coś Złego. Dlatego właśnie Żanna, pomagając mu, Skaziła swoją rękę ("klątwa") - a przynajmniej zdaniem Żanny.
[Przypisek Żółwia: moc magiczna przekształca ciało maga w to, czym mag w swojej opinii jest lub być powinien. Żanna przekształca się w coś demonopodobnego. Wyciągnijcie wnioski ;-)]
Zdaniem Żanny stan Augustyna (jest dość młody) jest wynikiem łaski boskiej, nie mocy Żanny. To jest dość ciekawe.
Żanna powiedziała też, zapytana, że jej moc pochodzi od diabła i dlatego zmienia się w "coś" i przed tym chroni ją tylko wiara i religia. Skąd to wie? Powiedział jej to Augustyn - a on jest ostatnim kapłanem.

- Rozmowa Kasi i Żanny skierowała się bardziej na temat Ingi. Inga kocha swoją babcię - chyba najbardziej z całej rodziny Inga kocha właśnie Żannę. Kiedyś Inga była popularna. Bardzo popularna. Miała przyjaciół i znajomych, jednak wszystkich potraciła. Jakiś rok po Zaćmieniu. Wtedy też zaczęła się interesować okultyzmem.
Kasia przestraszyła się tego, że Żanna będzie miała okazję porozmawiać z Augustynem. Uznała, że trzeba jak najszybciej (ie: teraz) udać się do domu Szczypiorków by zlokalizować to pole magiczne. Augustyn się z tego powodu bardzo ucieszył - on bardzo chciał ściągnąć tam Kasię i terminusa Augusta, zanim August się z kimkolwiek zdąży skontaktować. Terminus (zaobserwowany przez Augusta), widząc tą sytuację, przekazał wynik Augustynowi. Ten ściągnął wszystkich "swoich" magów do rezydencji w czasie, gdy Jan jedzie (nie spiesząc się - po drodze podjechali pod kościół, by kupić różańce dla wszystkich).
August szepnął Kasi, że ma tarcze, artefakty, wspomagania (nie włączone) - tym razem nawet Ingę, jeśli musi, pokona.

- W domu Szczypiorków. W domu są oznaki Skażenia - teraz Kasia wie czego widocznie szukać i to widzi. Strategicznie patrzy i pilnuje gdzie są lustra - na wszelki wypadek.
Weszła Inga, wciąż w stroju żałobnym. Gdy odeszła, Kasia pyta Żannę czemu Inga nosi strój żałobny. Żanna odpowiedziała, że Inga wczoraj pogrzebała swojego "urojonego przyjaciela". Płakała nad tym długo, bo to był jej jedyny przyjaciel. Zdaniem matki (Jolanty) Inga postanowiła dorosnąć.

- Bardzo szybko August zlokalizował źródło energii magicznej. Piwnica. Zeszli tam w trójkę i weszli do pomieszczenia gdzie znajduje się dziwny ołtarz odpowiadający medalionowi. Jest tam też Augustyn i 10 magów. Na wejściu Kasi i Augusta Augustyn podziękował im bardzo za przyjście. Między drużyną a Augustynem, magami, ołtarzem pojawiła się jedna Wall of Force oraz między drużyną i wyjściem pojawiła się druga Wall of Force. Drużyna jest uwięziona.
Magowie Augustyna rzucają zaklęcie kombinowane mentalne na drużynę. Terminus chciał zrobić coś pochopnego, najpewniej z jakimś niesprawdzonym zakazanym rytuałem krwi - ale Kasia go powstrzymała, siłując się z nim by nie mógł się zranić. Po chwili jednak odpaliło zaklęcie mentalne i Kasia i August zostali ubezwłasnowolnieni. Po chwili (kolejnej) na ich ciałach odpaliło zaklęcie dezintegracyjne i zostali nadzy. Okryto ich szatami - wszelkie artefakty, urządzenia podsłuchowe itp. właśnie przestały istnieć.
Kasia spróbowała porozmawiać z Augustynem i Żanną. Augustyn opowiedział Kasi o tym, jak kiedyś, dawno temu został zesłany na wschód (jakieś 60-70 lat temu). Tam został wyciągnięty przez niewielką grupkę Polaków, którzy właśnie byli wyznawcami Podniesionej Dłoni i mieli spętaną Żannę. Tam został odchowany, pomogli mu, nauczyli go i przechrzcili na nową religię. Potem poprosili go o odejście z Żanną i ołtarzem - ale wpierw poprosili, by Żanna ich (i wszystko co posiadali) spaliła żywcem. Żeby nie było żadnego śladu.
Augustyn wpierw skupił się na przyjemnościach po powrocie. Był awanturnikiem, złodziejem i kochankiem. Źle traktował Żannę. Ale z biegiem czasu zaczął ją cenić, szanować, a nawet się zakochał. A ona w nim. W końcu się ożenili i życie toczyło się dalej a on, Augustyn, odnalazł Prawdziwą Wiarę. Za co został przez Boga nagrodzony - i tu Augustyn rozłożył anielskie skrzydła, które ma jako pełnoprawnie przekształcony vicinius.
Żanna ani Augustyn nie chcą wolności. Wolność (wolna wola) jest dla ludzi. Oni - jak i magowie - nie są ludźmi. Są wykonawcami woli Pana.
I faktycznie, Augustyn kieruje "swoich" magów do pomocy ludziom. Tam też kieruje swój majątek. On FAKTYCZNIE robi dużo dobrego dla okolicy.
Szkoda, że nie doczekał się żadnego godnego następcy...

- Kasia w rozpaczy zasugerowała, że Inga może być magiem. Dziadkowie Ingi zareagowali współczuciem i sympatią. "Biedactwo, tak długo musiała się męczyć bez łaski bożej". Wpierw jednak August... Augustyn kazał mu położyć dłoń na ołtarzu i się pomodlił, by się stało. Moc przepływająca przez Augusta wypaczyła jego strukturę (to jak mindrape, ale dużo, dużo gorszy - jest na STAŁE) i sformowała na nim ten sam symbol co jest na Żannie i innych magach. Nad ołtarzem zawirował obraz umęczonego Augusta (jak na efemerydzie). August dołączył do grona zniewolonych magów, magów "anielskiego korpusu".

<img src="http://i443.photobucket.com/albums/qq160/phantomofdeth/Angel_Wing_by_Mikeinel.jpg"/>

- Inga w lustrze zobaczyła nową twarz. Szybko zbiegła do piwnicy i stanęła jak wryta, ale poprosiła dziadka, by nie robił jej "nowego przyjaciela", bo "stary umarł i już nie wróci". Na to dziadek powiedział, że Inga zrobiła coś złego. Terminus, już w anielskim korpusie, wszystko wypaplał. Ten sam czar mentalny co uprzednio drużynę trafił Ingę - dziewczyna się przestraszyła. Ta sama procedura - dla jej własnego dobra i spokoju umysłu chcieli ją przekształcić w członka anielskiego korpusu. Inga błagała dziadków, by jej tego nie robili, nie odbierali jej woli, lecz "to tylko dla jej dobra". W chwili, w której rytuał się zaczął, Kasia szybko podskoczyła i też położyła swoją dłoń na dłoni Ingi. W drugiej dłoni trzymała lusterko, które... nie wie skąd się wzięło, ale chciała, by się pojawiło.
Doszło do połączenia Kasi i Ingi. Kasia przeczytała kilka scen z pamięci dziewczyny - scenę młodej, 14-letniej dziewczyny (w okolicach Zaćmienia) którą Dotknęła efemeryda. Której efemeryda rozjaśniła historię jej rodu. Dziewczyna, która zrozumiała zło, którego jest częścią... Scena, w której dziewczyna, już homo superior (Skażenie, nasilone przez efemerydę) oddala się od świata ludzi i w której babcia - jej ukochana babcia - usuwa jej pamięć. Scena, w której w nocy zakrada się do niej wujek Radosław i ją molestuje. Scena, w której jej przyjaciel (efemeryda) znika w nocy, zniszczony przez tajemnicze siły i Inga jest znowu całkiem sama.
Do tego samo uczucie nakładania symbolu jest jak strasznie bolesny gwałt - naruszenie struktury, ciało obce, odebranie władzy nad sobą i zniewolenie powiązane z bolesną "przyjemnością"... 
Szczęśliwie, ani Inga ani Kasia nie są czarodziejkami - ołtarz nie jest w stanie ich podłączyć. Rytuał się przerywa, choć obie są wstrząśnięte. A Kasia zawarła wspomnienia Ingi i jej pamięć w lustrze. Inga też od tej pory jest odporna na wymazywanie pamięci (to samo co Kasia).
Żanna wymazała pamięć zarówno Indze jak i Kasi. Kasia usłyszała już tylko, że Augustyn autorytarnie stwierdził, że jest jeszcze jedna czarodziejka - Sandra. I że ją też trzeba dołączyć do anielskiego korpusu.

- Kasia obudziła się już w pokoju. Wszystko pamięta. Wraz z nią są: Samira (w pokoju z Kasią) i terminus (w swoim pokoju). Jest środek nocy i tylko ona nie śpi...
Kasia spojrzała na lusterko z sealowaną Ingą. Korzystając z obecności Samiry (swojej baterii) przesłała przez to lusterko do Ingi podświadomą myśl - jest ktoś, komu na niej zależy. Nie jest sama. I ta osoba jest spoza jej domu.
Kasia zaczyna kombinować jak to rozwiązać wszystko...
Wyszła z domu, nie budząc terminusa. Najpewniej rano August zacznie polowanie na Sandrę, więc ma czas tylko teraz by coś zrobić w tej sprawie.
Na zewnątrz się zastanowiła, co dalej. Ktoś dotknął ją w ramię - to Samira. Samira spytała, czy Kasia ma jakiś problem. Jednak ze sposobu poruszania się i zachowywania się Samiry Kasia stwierdziła, że to nie do końca Samira. Kasia spytała, kim jest - jest istotą z lustra. 
Istota z lustra powiedziała, że istnieje jedynie jeśli Kasia i Samira są koło siebie. To ona, Kasia, wezwała tu Istotę i kazała jej przejąć ciało Samiry. Istota ma za zadanie chronić zarówno Kasię jak i Samirę. Nie ma żadnych innych celów. Kasia wypytała o to, co Istota może zrobić. Ta nie wie. Mogła zamanifestować, bo Kasia jest mocno napromieniowana.
Im mocniej Kasia i Samira są napromieniowane, tym więcej Istota może zrobić.
Istota odeszła. Samira straciła przytomność i się obudziła. Kasia powiedziała, że Samira lunatykowała.
Samira wróciła do pokoju cicho, Kasia została na dworze.

- Kasia zadzwoniła do Sandry Stryjek (mimo godziny 3 rano). Poprosiła, by ta przyjechała do Kasi - do McDonalda w pobliżu. Sandra nie była szczęśliwa, ale Kasia ją przekonała (konflikt), że detale na miejscu a nie przez telefon, bo to niebezpieczne. Sama Kasia zdecydowała się przejść do tego McDonalda. Nie jest to najbliżej, ale jest to lepsze niż być wykrywalną. Wyszukuje też śledzi na ulicy, ale nic jej nie śledzi.

- Z Sandrą zamówiły cokolwiek. Sandra jest [jr] na czymś socjalnym i to bez magii, więc Kasia ma przewagę w konfliktach. Kasia wyszła od tego, że August i Kasia "wpadli w tarapaty" i "August nie chciałby, by Sandrze coś się stało". Sandra się zarumieniła. Stwierdziła, że on jej i tak nie zauważa. Kasia zaprzeczyła, ale Sandra wie lepiej. Kasia powiedziała, że największym problemem jest to, że August musi zachowywać się w jeden sposób, a chciałby w inny. Sandra znalazła wiarygodną interpretację - August przeniknął jako podwójny agent do jakiejś organizacji i musi działać wiarygodnie. Kasia bardzo, bardzo poprosiła Sandrę, by ta się nie ujawniała w żaden sposób i nikomu. A zwłaszcza Augustowi. Bo będą ją szukać.
Sandra się zgodziła. Obiecała Kasi pomoc, jeśli będzie trzeba. Ona, Sandra, jest technomantką i astralistką - jeśli Kasi czegos potrzeba, ona może to zapewnić.
Z tym się rozeszły.
Kasia skutecznie dała Sandrze head-start na ukrycie się. Teraz tylko zostało rozplątać ten węzeł gordyjski...

# Zasługi

* czł: Kasia Nowak jako człowiek, którego nie da się kontrolować ołtarzem i osoba eksperymentująca z mocami luster.
* czł: Samira Diakon jako w sumie żywa bateria dla "zaklęć" Kasi przy użyciu mocy luster (których Kasia i tak nie umie kontrolować).
* mag: August Bankierz jako terminus który wybłagał zwolnienie z aresztu domowego tylko po to by stać się niewolnikiem Ołtarza Podniesionej Dłoni.
* mag: Sandra Stryjek, która potrafiła unikać problemów dla Augusta jak tylko Kasia poprosiła ją o to, by się przed nim schowała. Astralistka i technomantka.
* vic: Augustyn Szczypiorek, (83 l) "Anioł" który poświęci wszystko dla religii i kontroli magów dla swych ideałów.
* mag: Żanna Szczypiorek, (68 l) uważająca się za demona i fanatycznie wierząca w prymat Kościoła nad magami. Lojalna Augustynowi (mężowi) nade wszystko.
* vic: Radosław Szczypiorek, (44 l) który uprzednio porwał Samirę w imię większego dobra a jeszcze wcześniej zdradzał inklinacje do młodocianej Ingi.
* vic: Inga Wójt, (19 l) podejrzewana o bycie czarodziejką za co ukochani dziadkowie byli skłonni zniszczyć jej umysł. Interesuje się okultyzmem i homo superior od roku.
* czł: Feliks Bozur jako student znający się na dziwnych religiach i symbolach i umiejący używać Google.
* mag: Herbert Zioło jako lokalny terminus asekurujący Żannę Szczypiorek z rozkazu Augustyna Szczypiorka.

# Lokalizacja

1. Piwnice Wielkie
    1. Domostwo Szczypiorków
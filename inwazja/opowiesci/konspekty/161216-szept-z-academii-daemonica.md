---
layout: inwazja-konspekt
title:  "Szept z Academii Daemonica"
campaign: powrot-karradraela
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [150729 - Kaczuszka w servarze (SD, PS)](150729-kaczuszka-w-servarze.html)

### Chronologiczna

* [150729 - Kaczuszka w servarze (SD, PS)](150729-kaczuszka-w-servarze.html)

## Punkt zerowy:

Pięć dni później, po Kaczuszcze...

Ż: Czemu lekarz (Karina) chce pokazać magom Academii Daemonica, że czysta destrukcja jest lepszą opcją niż stealth w tym wypadku? <br/>
K: Cała jej wiedza wskazuje na to, że to głęboko zaraźliwe i powinno się to zniszczyć zanim będzie za późno. <br/>
Ż: Czemu Alisie tak bardzo zależy by wykazać się właśnie przed tą grupą? <br/>
K: Ponieważ uważa, że to da jej wejście do wymarzonego Instytutu.

##### Misja właściwa:

Dzień 1:

Siluria otworzyła oczy. Przeciągnęła się rozkosznie u boku równie zadowolonej Inferni.

W żyłach Inferni płynie krew zarówno Diakonów jak i Zajcewów - z takim bagażem genetycznym może być tylko magiem Instytutu Zniszczenia. Dlatego min. sypia w skrzydle wzmocnionym, nie z innymi magami - zwłaszcza gdy ma epizody lunatykowania ;-).

Pod drzwiami pokoju Inferni Siluria zobaczyła gazetkę "KADEM Today". Zabawne, bo KADEM nie ma takiej gazetki. Projekt zaliczeniowy?

Infernia, poproszona przez Silurię, spuryfikowała tą gazetkę. Złapała sobie Silurię na miły poranek ;-), ale Siluria ją przekonała, że musi iść.

Gazetka jest wydana przez amatora. Na pewno to mag KADEMu. Na głównej stronie 'Zakochana para - Warmaster i Alisa'. To, co wzbudziło uwagę Silurii - atm Warmaster i Alisa są w konkurujących ze sobą projektach. Plus, jest to lekko niebezpieczne - Siluria WIE, że Whisperwind i Warmaster mają się ku sobie. A niebezpiecznie jest drażnić Whisperwind w tej sprawie (Siluria czy Infernia mogą tam z Warmasterem... Whisperwind nie jest tego typu osobą - ALE). Więc ta gazetka jest lekko... niebezpieczna.

Instytut Eureka wypuścił nowy obiekt. Ten obiekt to dwunastościan wielkości stołu. Zgodnie z obserwacjami Warmastera, jest to coś w stylu pancernika czy żółwia, ale nie jest to inteligentne. Przechowuje to coś. Warmaster i Whisperwind chcą to otworzyć - używając stealth. Zorganizował się DRUGI zespół, dowodzony przez Lucjana Kopidoła, który chce użyć mocy Pryzmatu by 'przekonać' ten dwunastościan do otwarcia się. Trzeci zespół został zorganizowany przez... Karinę Paczulis, która jest przekonana święcie, że to jest niebezpieczne i trzeba to zniszczyć. Miała wizje, że tam znajduje się mordercza choroba.

...co na KADEMie znaczy tyle, że to może być prawda.

Z biegiem czasu dwa zespoły - Pryzmat oraz Zniszczenie połączyły siły. Lucjan nadal nim dowodzi; dołączył Janek. A zespół Whisperwind # Warmaster nadal opracowuje sposób otwarcia tego całkowicie niezauważenie.

Problem polega na tym, że Instytutem Eureka dowodzi Aleksandra Trawens oraz Marta Szysznicka, razem...

Siluria usłyszała głosy z oddali między Whisper i Warmasterem.

"Whisper! Nie mam z tym NIC wspólnego! Ja i Alisa, nie mamy nic wspólnego" - Warmaster <br/>
"Nie pytam ;-)" - lekko rozbawiona Whisperwind <br/>
"Ale naprawdę! Ja i ona... nie ma mnie i jej" - Warmaster <br/>
"Hamster, naprawdę - nie pytam" - Whisperwind

Siluria pamięta teorię Whisper. Że dwunastościan się otwiera wtedy, gdy nikt na niego nie patrzy. I Whisper chce go oszukać. Siluria nadal nie wie, skąd Whisper wzięła tak dziwną teorię, ale ok...

Siluria chciała minąć się z Whisper i Warmasterem w korytarzu, ale usłyszała jeszcze jedną dyskusję.

"Alisa? Co ty tu robisz?" - zaskoczona Whisperwind <br/>
"PRZEPRASZAM! Ja nic... ja nie..." - Alisa, przerażona <br/>
"Nie, to JA przepraszam! Nie chciałem! Nie wiem..." - Warmaster, widząc przerażenie Alisy <br/>
"...zabijcie mnie..." - Whisperwind

Siluria poszła tam - nie może sobie darować takiej okazji. Z gazetką pod ręką. Rzuciła lekkie spojrzenie na Whisper i zauważyła od razu jedną rzecz. Whisper jest wściekła. Nie na Alisę czy na Warmastera, na kogoś innego. A wściekła Whisper nie brzmi dobrze...

A Alisa wyraźnie płakała niedawno.

Siluria nie rozumie sytuacji i próbuje to wszystko zrozumieć; podeszła do Whisper i próbuje z niej wyciągnąć co się dzieje. Przekonała Whisper, że ona nie ma z tym nic wspólnego. Pyta Whisper, czemu ta jest tak zdenerwowana - "mimo wszystko jesteśmy jedną wielką, choć patologiczną rodziną". Whisper wyjaśniła słodkim głosem (co udowadnia jak bardzo jest wściekła). Są na Fazie Daemonica. Jedyne osoby w okolicy to magowie KADEMu. Jeśli magowie KADEMu uwierzą, że Alisa i Warmaster mają się ku sobie... to tak będzie.

I nagle dla Silurii znaczenie tego problemu skoczyło o kilka oczek w górę.

Siluria powiedziała Whisper, że nie wie KTO za tym stoi, ale wie jak to rozwiązać. Spojrzała na Alisę wymownie. Whisper odpowiedziała wymownym uśmiechem.

Alisa jest dość świeżą KADEMką o dużej mocy, strasznie naiwna i przejmująca się wszystkim.

"Haaaamster! Chodź. Chodź do Akademii." - Whisper, wołająca Warmastera
"Akademii już nie ma..." - Warmaster, posmutniał
"Akademia jest tam, gdzie my." - Whisper, zdecydowanym tonem

Siluria poszła z Alisą, lekko ją... skłaniając do zwierzeń. Zainteresowało Silurię, że Alisa idzie w kierunku na pancerne magazyny KADEMu. Siluria prowokuje Alisę, chce, by ta się przed nią otworzyła i przed nią wyżaliła. Alisa, która jest naiwną i zranioną młodą czarodziejką, otworzyła się.

Powiedziała:

* Nie umówiła się z nikim konkretnym. Ona po prostu idzie porozmawiać z _nią_, bo _ona_ nie ocenia i słucha...
* Jest zraniona, bo mówią, że przez to, że ona i Warmaster, to nie mają sukcesów w zniszczeniu dwunastościanu

Siluria zmieniła plan. Wzięła na spacer ze sobą Alisę, poszła z nią coś zjeść... niech wszyscy myślą, że jest coś między Alisą i Silurią. Siluria chce, by cały cholerny KADEM usłyszał ploty, że Siluria rozpoczęła łowy na Alisę...

A Alisa, naiwna, nie ma o tym zielonego pojęcia. Siluria poszła, postawiła jedzenie Alisie, poprzymierzała z nią stroje, zrobiła jej makijaż... celem - poprawić humor Alisie i sprawić, by ta nie czuła się sama i samotna. Po drugie, doprowadzić, by wszyscy rozumieli, że Siluria poluje na Alisę - więc na pewno Alisa i Warmaster nie są razem. I Silurii się to udało.

Wieczorem poszła spotkać się z Ignatem.

Ignat nie widział nic złego w tej gazetce (chociaż opierdzielił kilku chłopaków za to jak się nabijali z Alisy) aż Siluria wyjaśniła implikacje - są na Fazie. Ignat uznał, że to byłby zwykły gwałt i uniósł się świętym oburzeniem. Siluria przekonała go, by wpierw jednak przyszedł z tym do niej. Ignat detektyw obiecał się dowiedzieć...

Dzień 2:

Następnego dnia Siluria nadal rozbudowuje legendę o Alisie. Jednak jak jej szukała, nie udało jej się znaleźć Alisy. Za to przyszedł do Silurii Ignat. Ma gościa. Artur Kotała. Po co? Artur ma teorię, że dwunastościan jest w rzeczywistości żartem Whisperwind z KADEMu, więc przez zrobienie presji na Alisie chciał wymusić na Warmasterze reakcję. Co jedynie oznacza - w oczach Silurii - że Artur nie rozumie Warmastera. Bo ten tego nie zauważyłby, gdyby to był żart Whisperwind...

Siluria spytała Ignata, czy ten widział Alisę. Nie, nie przyszła na spotkanie rano. Albo przyszła i poszła; on się spóźnił na spotkanie. Najpewniej Alisa gdzieś tam jest na tych swoich pancernych magazynach...

Siluria wzięła Ignata (który jako detektyw ma magię detektywistyczną) i poszli na magazyny KADEMu. I bez żadnego problemu znaleźli ślady Alisy - prowadzą do jednego z magazynów. Dokładniej, do magazynu TECH_KN_15. Ten magazyn nie jest pusty. Tam jest EIS... Siluria poprosiła Ignata, by ten zaczekał; rozmowa z Alisą może wymagać pewnej delikatności. Sama weszła do środka.

"No i wtedy ja myślałam, że ona chce mi pomóc, ale ona chciała mnie uwieść? Komu mam wierzyć? No komu?" - Alisa, żaląca się EIS.
"Wiem, że nie słyszysz. Wiem, że jesteś tylko maszyną, i nie do końca sprawną. Ale z kim mam rozmawiać?" - Alisa, nadal żaląca się EIS

Siluria się dyskretnie wycofała. Poprosiła Ignata, by ten powiedział Alisie, że Siluria jest JEGO. Potem zapukała do magazynu. Gdy Alisa spytała, jak Siluria ją znalazła, Ignat powiedział, że to był on.

"Z Infernią tośmy walczyli o Silurię ostro; śledziki latały. Z Jankiem porównywaliśmy, kto ma większe działo i które strzela dalej; tak czy inaczej, zasłużył. Ty, Aliso, nie zasłużyłaś; na co masz ochotę? W jaki sposób chcesz sobie wywalczyć prawo do flirtowania z Silurią?" - Epicki Monolog Ignata (parę minut) oskarżający Alisę o próbę przelecenia Silurii.

Alisie wszelkie myśli o tym, że Siluria chce ją uwieść z głowy wyparowały. Ignat pozamiatał.

Siluria nadal trzyma wrażenie chroniące Alisę (i Warmastera).

Wieczorem Siluria dostała liścik od Ignata. Liścik mówił, że przyszedł Warmaster i dał kilka wskazówek, jak Whisper uważa, że można zniszczyć ten dwunastościan i jak on sam uważa, że można to zniszczyć.

Siluria poszła do Whisperwind, zaniepokojona. Ta przywitała ją z wesołym uśmiechem - Siluria zrobiła to, co miała z Alisą. Zapytana przez Silurię, Whisper powiedziała, że nie wysłała Warmastera. Cóż. Gdy Siluria zapytała, czy Whisper rozważała połączenie sił z magami KADEMu, ta się zaśmiała. Z nimi nie będzie łączyć sił, bo wie wszystko co chciała wiedzieć o tym dwunastościanie. Siluria widzi, że Whisper ma jakiś plan - wredny plan - i nie chce powiedzieć.

Siluria spróbowała przekonać Whisper, by ta jej coś powiedziała. To było trudne wyzwanie; Whisper jest trudnym przeciwnikiem. W końcu Whisper się zgodziła powiedzieć Silurii; jednak nie pomoże i nie zatrzyma swoich działań (osobiście) tak samo jak i Siluria pomoże Whisperwind zdobyć kilka przedmiotów do "Academii Daemonica".

Whisper opowiedziała tą historię.

* Instytut Eureka przekazał dwunastościan
* Whisper podmieniła dwunastościan na INNY dwunastościan, z Mare Ithium. Bardzo odporny. 
* Potem powiedziała im, że jest niezniszczalny.
* Jako, że są na pryzmacie, uwierzyli, że jest niezniszczalny.
* Magowie KADEMu próbują zniszczyć niezniszczalny dwunastościan.
* Teraz Warmaster poszedł dać im hinta o istocie, którą mogą przyzwać w Mare Vortex.
* Istota ta po prostu zaatakuje ich obrazami tego co kochają, że ich krzywdzi. Pokonać to czym? Happy thoughts.
* W tym czasie Whisper podmieni dwunastościan z Mare Ithium na coś jeszcze innego. Coś zniszczalnego. Z bardzo śmierdzącą cieczą.
* Niech Karina ma swoje 'zaraźliwe i niebezpieczne'.

"Na Pryzmacie nigdy nie jesteś samotna. Jak długo wierzysz, że tu są - są z Tobą dalej" - Whisper, po przyzwaniu iluzji Venomkissa.

Siluria jest lekko w kropce.

* W całej grupie KADEMu najbardziej winny jest Kotała za tą konkretną akcję...
* Bogumił, Karina, Alisa ale i Artur Kotała - oni są niedoświadczeni. Oni nie wiedzą o Pryzmacie.
* Lucjan Kopidół jest kompetentny, ale on nie czyta takich gazetek; ma głowę w chmurach
* Ignat i Janek chcą po prostu rozwalić coś z innych powodów. Bo mogą.

Brak pomyślunku na KADEMie zawsze jest ciężko karany; to, co robi teraz Whisperwind to typowa lekcja etyki w wydaniu Weroniki Seton...

Siluria nie chce odwracać tego, co zrobiła Whisperwind w stu procentach. Ale nie chce, by to się aż tak ostro skończyło.

# Progresja

* Whisperwind: podkradła Dwunastościan, który Karina Paczulis uznała za 'głęboko zaraźliwy jak się otworzy, trzeba zniszczyć'. Schowała przed wszystkimi.

# Streszczenie

Na KADEMie Artur Kotała zrobił gazetkę pokazującą, że Alisa Wiraż i Warmaster mają się ku sobie. Jest to niebezpieczne, bo Pryzmat (może stać się to prawdą). Siluria zneutralizowała zagrożenie Pryzmatu i pocieszyła Alisę (odkrywając, że ta żali się EIS). Whisperwind zrobiła małą pułapkę na tą grupkę magów KADEMu. Whisper i Warmaster tęsknią za AD; Whisper dodatkowo podkrada wszystkie niebezpieczne artefakty z Instytutu Eureka i podmienia je złośliwie na niegroźne 'klony' z Fazy Daemonica.

# Zasługi

* mag: Siluria Diakon, która bez najmniejszych skrupułów łagodzi konflikty na KADEMie i wykorzystuje wszystkie swoje znajomości
* mag: Whisperwind, karząca ręka Academii Daemonica na KADEMie. Cicha i dyskretna, acz złośliwie podmienia niebezpieczne artefakty z Instytutu Eureka na rzeczy 'niezniszczalne'. Bezwzględna.
* mag: Warmaster, poczciwy i o dobrym sercu. Też tęskni za AD. Chciał pomóc Alisie, więc - jak chciała Whisper - wpakował magów KADEMu w pułapkę zastawioną przez Whisper.
* mag: Lucjan Kopidół, mag Instytutu Pryzmaturgii dowodzący projektem zniszczenia Dwunastościanu. Raczej z głową w chmurach; jedyny ekspert od Pryzmatu w całym zespole.
* mag: Artur Kotała, który wyczaił, że Whisper może się z nimi bawić i próbował naciskiem na Alisę doprowadzić do tego, by Warmaster powiedział co Whisper zaplanowała.
* mag: Karina Paczulis, która wyczuła, że najnowszy produkt Instytutu Eureka jest bardzo niebezpieczny jak się go otworzy i dołączyła do zespołu mającego zniszczyć ów Dwunastościan.
* mag: Alisa Wiraż, ma nadzieję dostać się do wymarzonego Instytutu; zahukana i nieśmiała. Żali się EIS; nie czuje się częścią grupy. NIE jest dziewczyną Warmastera i NIE flirtuje z Silurią.
* mag: Ignat Zajcew, detektyw który powiedział Silurii jaka była rola Artura Kotały; też, wrobił Alisę w to, że by flirtować z Silurią trzeba go w jakiś sposób pokonać.
* mag: Infernia Diakon, sprzężona z rodów Diakon i Zajcew, 'destruktywna puryfikatorka' Instytutu Zniszczenia i (tymczasowa) kochanka Silurii. 
* mag: Aleksandra Trawens, współgłównodowodząca Instytutu Eureka wraz z Martą Szysznicką
* mag: Marta Szysznicka, współgłównodowodząca Instytutu Eureka wraz z Aleksandrą Trawens

# Lokalizacje

1. Świat
    1. Faza Daemonica
        1. Mare Ithium, skąd Whisperwind skombinowała drugi, bardzo podobny Dwunastościan
        1. Mare Vortex
            1. Zamek As'caen
                1. KADEM Daemonica
                    1. Skrzydło naukowe
                        1. Instytut Pryzmaturgii
                            1. Sale badawcze, gdzie znajduje się Dwunastościan (i gdzie regularnie wkrada się Whisperwind)
                    1. Skrzydło wzmocnione
                        1. Instytut Eureka, który czasem generuje bardzo dziwne i niebezpieczne przedmioty
                        1. Pancerne magazyny
                            1. Academia Daemonica, magazyn, który Whisperwind objęła jako swój i Warmastera nazywając go AD
                            1. Magazyn TECH_KN_15, gdzie znajduje się EIS i gdzie żali się Alisa, uważając, że EIS nie słyszy
                        1. Specjalne sypialnie, gdzie min. śpi Infernia Diakon. Zwłaszcza gdy nie "śpi". tylko jest z Silurią
   
# Czas

* Opóźnienie: 5 dni
* Dni: 2
---
layout: inwazja-konspekt
title:  "Portret Boga "
campaign: czarodziejka-luster
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [131008 - "Mój Anioł" (An)](131008-moj-aniol.html)

### Chronologiczna

* [131008 - "Mój Anioł" (An)](131008-moj-aniol.html)

## Misja właściwa:

Andromeda nie ma przy sobie Ołtarza Podniesionej Dłoni. Ołtarz znajduje się w trzecim najmniej oczywistym miejscu. Póki nie musi, nie zamierza tego wyciągać. Niestety, amulet kontrolny Andromeda musi nosić przy sobie - bez tego magowie połączeni z nią Ołtarzem powoli umierają. Andromeda raz jeszcze się przeniosła, a potem jeszcze raz. Od ostatnich wydarzeń minął miesiąc. Samira wydobrzała; Andromeda jak mogła unikała manipulacji lustrami i wyraźnie miało to korzystny wpływ na stan Samiry. August nie dowiedział się, że Andromeda tak naprawdę ostatnio skrzyżowała magię krwi z magią luster. Nie powiedziałaby mu. Nigdy. Życie toczyło się w miarę spokojnie - Samira znalazła coś dla iluzjonistki (i poczyniła postępy w sztuce iluzji), August szukał i kombinował jak rozwiązać ten problem z Ołtarzem, ale nie udało mu się niczego znaleźć (i nie chciał zwracać uwagi ani Mistrza ani Marcela). No i nie może zadać o to pytań... Tymczasem Sandra czysto przypadkowo zamieszkała niedaleko i znalazła pracę u mechanika samochodowego. 

Życie toczyło się dalej. Wszystko zmienił jeden videochat z Andromedą. Zadzwonił do Andromedy agent ubezpieczeniowy, Andrzej Domowierzec. Poprosił Andromedę o namalowanie portretu Boga. Andromeda zapytała o jakiego boga chodzi; Andrzej stwierdził, że bóg jest wolnością, marzeniem, wyzwoleniem, prawdą. Nie miłością. Zachowuje się troszeczkę sekciarsko, ale nie wygląda na niebezpiecznego. Chce zapłacić i chce, by Andromeda miała pełną dowolność w namalowaniu owego obrazu - w końcu bóg jest wolnością. Naprawdę dziwne. Powiedział też, że zaangażował innych malarzy, między innymi Artura Szmelca. Gdy Andromeda spytała dlaczego ona, powiedział, że ma opinię osoby która maluje rzeczy które nie istnieją tak, jakby istniały - wie to od innych malarzy. To w wypadku tego zlecenia szczególnie ważne.
Andromeda powiedziała, że za dwa dni odpowie czy interesuje ją to zlecenie.

Andromeda jest skonfliktowana. To śmierdzi jak magia. Ona unika magii jak ognia. To może być bardzo niebezpieczne. August jest niedaleko Andromedy - od czasu jak ma amulet, pilnuje jej uważnie, bo boi się, że coś się jej stanie i on (oraz reszta magów) umrze. Dlatego August pilnuje Andromedy bardzo uważnie, przez co część osób mogłaby myśleć, że są parą (młodziutki chłopak i wamp która go uwiodła, haha). Andromeda powiedziała mu o swoich problemach a August zaproponował, że jak ona pojedzie, on jedzie z nią. Jeśli Andromeda nie pojedzie, proponuje wysłanie Sandry. Im dalej od Augusta jest Sandra tym lepiej. Andromeda lekko spochmurniała, bo jej zdaniem młoda technomantka nie zasłużyła sobie na taką reakcję Augusta, ale mag jest nieprzekonywalny (chyba, że rozkazem, czego Andromeda nie chce robić). Zdaniem Augusta Sandra to zdrajczyni i na tym się kończy.

I na to do drzwi zapukała Sandra. August facepalmował. Skąd wiedziała! Andromeda ją wpuściła a Sandra przyszła podobno do Andromedy (choć malarka wie, jak Sandra patrzy na Augusta). Sandra dała Andromedzie zrobioną przez siebie pozytywkę z niewielką ilością magii (Sandra pracowicie próbuje przekonać Andromedę, którą uważa za czarodziejkę do ludzi ku wielkiemu rozbawieniu Andromedy i zażenowaniu Augusta). W zamian za to Andromeda zaproponowała Sandrze obiad i czarodziejka z radością się zgodziła. Powiedziała, że obiad smakuje prawie tak dobrze jak - tu się zatrzymała - i August okrutnie dokończył - jakby zrobiła go jej mama. Sandra jadła przez chwilę w milczeniu a Andromeda wyciągnęła ją z tego zwykłymi, normalnymi pytaniami. Widząc, że Sandra jest z natury społeczną osobą (a chwilowo bardzo samotną) zaprosiła ją na następny występ Samiry.

Jakby tego było mało, weszła Samira. Cała wesoła gromadka w komplecie. Samira pokazała Andromedzie nowe lustro - piękne, złote z unikalnymi wzorami i taflą lustra złożoną z wielu różnych wzorków i typów szkła. Niesamowita i fascynująca konstrukcja. Zarówno Sandra jak i August zareagowali nerwowo - lustro to nie jest tylko lustro, nigdy. Samira naturalnie nie wie o co chodzi. Andromeda uspokajała rozpaczliwie dwóch magów próbując zachować Maskaradę gdy August wziął Samirze lustro i poszedł do łazienki przeskanować je magicznie. Samira zaprotestowała i Sandra dzielnie wyjaśniła, że jak August widzi dziwne lustro, musi przejrzeć w nim swoje przyrodzenie. Coś pomiędzy fetyszem a nerwicą natręctw. O dziwo, Samira to łyknęła.

August opuścił łazienkę i oddał lustro Samirze, dając Andromedzie znaki, by nie patrzyła w lustro. Andromeda ma w tym wprawę, więc spróbowała (11v10->11) - mimo pojedynczego spojrzenia lustro zaczęło ją "wciągać" i chciało coś z nią zrobić. Andromedą wstrząsnęło to, że to lustro nie działało jak zaklęcie. To lustro zachowywało się jak zaprojektowane specjalnie dla niej. Oddała lustro Samirze i postanowiła w najbliższym czasie nie patrzeć na to konkretne lustro. 

Wesoła gromadka się rozeszła. August i Andromeda zostali sami. Andromeda powiedziała Augustowi, że ma zamiar zainteresować się tą sprawą. August poprosił o jeden dzień - musi poszukać w literaturze tego lustra, bo TAKIE lustro na pewno było udokumentowane przez kogoś. Andromeda się zgodziła, ale poprosiła o ostrożność. August zasugerował, że poprosi Marcela (tak, schowa dumę do kieszeni) by załatwił zadanie dla Sandry, by sobie stąd poszła i przestała się bawić. Zlecenie byłoby typu technomancja # świat ludzi. Jako, że ona nie dostaje zleceń, na pewno dałaby Andromedzie i Augustowi spokój. Andromeda zauważyła, że Sandra może być przydatna. Więc nie chce by to się stało.

Następnego dnia Andromeda, i Sandra obejrzały spektakularny pokaz Samiry. Andromeda jest zestresowana, bo nowe lustro Samiry, ale faktycznie nie było ono użyte. Tymczasem August szukał informacji na temat tego lustra (1/1k20) lecz nie udało mu się znaleźć niczego szczególnie interesującego; rekordy na temat tego lustra istnieją, ale są zapieczętowane. Andromeda miała więcej szczęścia - poprosiła o pomoc swojego kolegę, Feliksa Bozura (6#2), i dostała odpowiedź - jest to lustro, które pojawiło się całkiem niedawno; nie jest stare choć na stare wygląda a przynajmniej nie ma o nim informacji starych. Jest z nim powiązana fraza - "Pryzmat myśli". Niestety, nic więcej nie był w stanie znaleźć.
Samira powiedziała, że kupiła je w antykwariacie, tu, na miejscu. Nie było tanie, ale było tego warte, zdaniem Samiry.
Zdaniem Andromedy też... jak Samira je ma, przynajmniej nie ktoś inny...
"Lustra do niej lgną jak magia do mnie -_-" - Andromeda o Samirze do Augusta.
Andromeda też zaznaczyła Augustowi, że ktoś w końcu zauważy, że on wpada w kłopoty mniej więcej po tym, jak wychodzi od Andromedy.

August oddalił się smutny a Andromeda połączyła się amuletem ze "swoimi" zdominowanymi magami. Poprosiła (nie chce im rozkazywać), żeby znaleźli wszystkie informacje o nietypowym lustrze z perspektywy potencjalnego terminu "Pryzmat myśli". Mają mieć low profile, nie być wykryci. 

Następnego dnia, w okolicy wczesnego południa Andromeda dostała informacje o "Pryzmacie Myśli" (16/1k20). Nie ucieszyło jej to, co przeczytała.
Pryzmat Myśli był artefaktem który został udokumentowany tylko przez jedną czarodziejkę - Samirę Diakon. Istnieje podejrzenie, że to ona stworzyła to lustro, ale nawet ona nie powinna być w stanie tego zrobić, stworzyć czegoś takiego - chyba, że posiłkowała się magią krwi (co w połączeniu z magią luster i nieznanymi technikami jest cholernie groźne). Wraz z trwaniem Zaćmienia moc Samiry malała. Im bardziej malała, tym bardziej obsesyjnie skupiała się na Pryzmacie Myśli. Nie zdążyła. 
Niestety, nie pozostawiła notatek. Nie pozostawiła niczego. Niewiele tak naprawdę pamiętała; znaleziono ją jako człowieka trzymającego w ręku Pryzmat Myśli.
Lustro to - gdy pewien mag wpatrywał się w nie dość długo - rozproszyło jego osobowość; mag przestał istnieć, pozostał jedynie puste, bezmyślne ciało.
Pryzmat Myśli powinien (zgodnie z rekordami, jest) być zasealowany w strażnicy artefaktów SŚ w Trzech Czarnych Wieżach.
Andromeda postanowiła, że August nic nie wie. Ktoś miał jaja, że stamtąd zwinął taki artefakt...

W południe Andromeda zadzwoniła do Andrzeja Domowierzca. Odebrała niejaka Małgorzata Poran i poinformowała, że to faktycznie komórka Domowierzca, ale on nie może odebrać. Naciśnięta dodała, że to dlatego, bo Domowierzec nie żyje. Andromeda w szoku - Domowierzec zginął w nocy i policja bada sprawę. Aha, czy to sekretarka Andromedy? Tak. Małgorzata stwierdziła, że się cieszy i zaprasza, by Andromeda namalowała portret boga. Andromeda w szoku *2. Nie tylko Małgorzata nie sprawia wrażenia bardzo przejętej, ale i nie zmienia w ogóle planów mimo wyraźnych wskazówek, że tam jest morderca. Andromeda została poinformowana, że dostanie wynajęte bezpieczne miejsce, ale niech jest z osobami towarzyszącymi. Będzie wynajęta ochrona.

Ok... 
Andromeda poszła z tym do Augusta. Ten nie zrozumiał znaczenia śmierci człowieka aż w końcu Andromeda mu to uświadomiła - tam może dla odmiany dziać się coś magicznego.
Andromeda powiedziała, że chce pojechać z Sandrą oraz Samirą a on, August, jedzie incognito. Osobno. Żeby nikt nie wiedział. Ma to sens. August spytał, czy ktoś będzie wiedział że on tam jest. Nie, ani Sandra, ani Samira. August powiedział że załatwi tylko pozwolenie u mistrza (czysta formalność) i nie ma sprawy.
Potem Andromeda poszła do Samiry i zaproponowała jej wyjazd za miasto - Żonkibór. Samira się ucieszyła, przyda jej się wycieczka, oraz zdziwiła się że Żonkibór, gdzie jest ten festiwal. Jaki festiwal? Okazuje się, że od miesiąca dzień w dzień jest tam tzw. Festiwal Marzeń, gdzie ludzie mogą bawić się w spokoju, bez konieczności bycia kimś innym, wolno. Festiwal adresowany jest do młodych ludzi. Ona, Samira, też była zaproszona ale nie mogła jechać - była zbyt chora (okolice czasu rezydencji Szczypiorków).

Andromeda spotkała się z Sandrą i spytała, czy ta nie pojechałaby na wycieczkę do Żonkiboru. Sandra się ucieszyła, ale zmartwiło ją że nie będzie Augusta. Naciskała na Andromedę - czemu nie będzie Augusta, o co chodzi, dlaczego się nie pojawi. Andromeda powiedziała, że nie pytała ale Sandra nalegała. Andromeda widzi, że Sandra coś podejrzewa. Niedobrze.
"Tien Diakon, bardzo mi na tym zależy." - Sandra do Andromedy o obecności Augusta (nie, nie może sprawdzić w hipernecie - ma poblokowane).
"Nie przysługuje mi tytuł tien." "Och, przepraszam. Millennium?" "To bardziej skomplikowane." "Przepraszam, nie moja sprawa." - Andromeda i Sandra.
Andromeda wyjaśniła Sandrze w łagodnych słowach, że nie jest Augustem zainteresowana erotycznie, mimo "że Diakon". Andromeda okłamała Sandrę i wmówiła jej - Andromeda nie jest zagrożeniem dla Augusta i nie jest nim zainteresowana erotycznie.
Sandra zgodziła się pojechać z Andromedą. Zdecydowała się wyładowywać swoje frustracje na tle Augusta - na Auguście.

Żeby August nie miał za dobrze (zostaje sam w mieście; nieważne czy robi coś dla "Diakonki" czy po prostu się leni) Sandra skontaktowała się z Marcelem. Powiedziała mu, że chwilowo August nic nie robi tylko zajmuje się lustrami. Marcel w pełni świadomy że za to ostatnio August dostał OPR od mistrza szybko znalazł mu jakieś bardziej właściwe terminuskie zadanie i jak tylko August zgłosił mistrzowi że chce udać się gdzieś indziej (do Żonkiboru) mistrz to zabanował. Wysłał zrozpaczonego Augusta na zupełnie inną misję...

Andromeda, Sandra i Samira jadą samochodem do Żonkiboru. Andromeda powiedziała im o nietypowym zleceniu na co Sandra zrobiła "oj". Przyznała się Andromedzie, że wrobiła terminusa w misję. Znikąd wsparcia jakby było potrzebne (or so she thinks).

Zdaniem Andromedy, pięknie się zaczyna...

# Lokalizacje:

Nieistotne tym razem.
Warto wspomnieć:

1. Kopalin
    1. Trzy Czarne Wieże 
        1. Strażnica Artefaktów SŚ.

# Zasługi

* czł: Kasia Nowak, która musi pracować z tym co ma a wszystko jej się rozłazi.
* czł: Samira Diakon, do której lgną wszystkie możliwe lustra a zwłaszcza te co nie powinny.
* mag: August Bankierz, thrall Andromedy i kinderterminus który bardzo chce się pozbyć Sandry zanim ta mu zaszkodzi.
* mag: Sandra Stryjek, nieświadoma niczego technomantka o kiepskiej przeszłości zakochana w Auguście, zaszkodziła Augustowi.
* czł: Artur Szmelc, dumny i kompetentny malarz zatrudniony do portetu boga.
* czł: Andrzej Domowierzec, 37, agent ubezpieczeniowy który chciał portret boga.
* czł: Małgorzata Poran, 47l, która odbiera telefony za Andrzeja Domowierzca.

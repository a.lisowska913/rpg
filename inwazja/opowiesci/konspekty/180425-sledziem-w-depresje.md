---
layout: inwazja-konspekt
title:  "Śledziem w depresję"
campaign: dusza-czapkowika
gm: kić
players: arleta
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [180421 - Jaszczur Love Story](180421-jaszczur-love-story.html)

### Chronologiczna

* [180421 - Jaszczur Love Story](180421-jaszczur-love-story.html)

## Kontekst ogólny sytuacji

### Opis sytuacji

## Punkt zerowy

Melinda Zajcew jest zakochana w Warmasterze. Chce mu zaimponować poprzed ulepszenie wystroju Zaćmionego Serca i zdobywa tuzin śledzi syberyjskich, które chce przerobić na śledzie śpiewające mroczne piosenki (a może nawet wokalizujące wiersze Warmastera..!)
Niestety, paradoks krzyżuje jej plany...

## Potencjalne pytania historyczne

* K: Co Genowefa robiła w kiepsko oświetlonym zaułku?
* A: Wracała ze zbierania grzybów na składniki eliksirów.

## Misja właściwa

Genowefa właśnie wracała do domu po udanym zbiorze rzadkich grzybów na składniki eliksirów, kiedy nagle coś obok niej przemknęło i zaraz potem wpadła na nią Melinda Zajcew z okrzykiem "uważaj!"

Po krótkiej wymianie zdań, gdzie dziewczyna trochę kręciła, Genowefa spróbowała rzucić zaklęcie, na co zanim zdążyła to zrobić, Melinda radośnie stwierdziła "Ty też jesteś czarodziejką! Pomóż mi złapać śledzia!"
...Że co?
Przyciśnięta, Melinda wyjaśniła, że uciekł jej śledź. Taki zębaty. Z nogami. Syberyjski. No.. bo miał śpiewać w Zaćmionym Sercu, ale jej uciekł.
I teraz musi go złapać. A najpierw znaleźć.
Genowefa magicznie wytropiła śledzia w śmietniku. Takim zamykanym. Co tu zrobić? 
Tak, wypłoszyć! I złapać do worka.
Melinda się boi, ale złapie jeśli śledzia zobaczy (magia). Wypłoszenie się udało i Melinda przy użyciu magii transportu złapała śledzia do worka z okrzykiem "Mam cię, szóstko!"
...Chwila, to ich jest więcej?!
...Tak... tuzin.

Genowefa myśli, jak znaleźć 11 śledzi. Idzie przez nocne miasto, Melinda za nią. Nagle dostrzega rozpaczającego na ławce człowieka, a w jego pobliżu czający się długi cień.
śledzik?
Genowefa kazała Melindzie zajść śledzia, a sama podeszła do człowieka. Ten jednak nie chciał mieć nic do czynienia z kimkolwiek, chciał być sam ze swoim nieszczęściem. Wstał i zaczął się oddalać. Kiedy się ruszył, śledź - cień poszedł za nim... Genowefa była zdziwiona, bo śledź ewidentnie nie atakował, tylko... śledził człowieka?
Postanowiła więc śledzić śledzia. I wyśledziła... do kolejnej ławeczki w pobliżu, gdzie mężczyzna usiadł.
Ku zdziwieniu Genowefy, jego szlochy, na początku rozpaczliwe, szybko stały się coraz mniejsze, aż całkiem ucichły.
Rzuciła zaklęcie, by zbadać człowieka ma odległość. Bez problemu poznała jego stan. Jeszcze 10 minut temu ten mężczyzna był w ciężkiej rozpaczy i miał początki depresji. Stracił matkę, jego pies wpadł pod auto i ogólnie ostatnio miał serię nieszczęść. Jednak teraz patrzył na życie optymistycznie...
Czyżby śledź wyjadł jego nieszczęście? Śledź ratujący świat? O co tu chodzi.
Genowefa postanowiła zwabić śledzia poprzez wyciągnięcie resztek nieszczęścia mężczyzny i przekształcenia ich w smakowity kąsek. Jeśli śledź się skusi to teoria o śledziu - pocieszycielu zostanie potwierdzona...
Skusił się. Przy okazji, całkowitym przypadkiem, razem z depresją (na stałe), Genowefa usunęła wszelką pamięć o tym, co tu się stało.
Ok. Mamy dwa śledzie z dwunastu. Co teraz?

Genowefa przycisnęła Melindę. Jak jej ten śledź uciekł? No... bo rzucała zaklęcie, żeby śpiewały (melorecytacja wierszy Warmastera *_*) ale nagle zniknęły i teraz ona szuka...
No skoro te śledzie pożerają depresję i smutek...
"Mój klub!"
No tak... Genowefa załamana. W sumie racja. Co może być bardziej smakowite dla takiego śledzia niż emo-klub?

Niestety, jest środek nocy. Genowefa jest padnięta po całym dniu zbierania. Pyta Melindy, czy może przenocować u niej. No... ale Melinda też tu nie mieszka. Ale mogą przenocować w klubie! Warmaster dał Melindzie klucz ( *_* )
Noc, o dziwo, spokojna.

Rano poszły po jakieś śniadanie do Ropuszki. Po drodze dostały sygnał od terminusa. Estrella Diakon. Melinda chowa się za Genowefą i panikuje, jak tylko Estrella zapytała, czy nie widziały tu jakichś śledzi.
Genowefa uspokoiła Melindę (było trudno), i wyjaśniła sytuację terminusce.

Estrella zaoferowała pomoc, z której Genowefa skwapliwie skorzystała.
Zanim klub otwarł swoje podwoje, Genowefa przygotowała go do bycia smakowitym kąskiem wzmacniając mroczność klubu magią mentalną i zmysłów.

Potem było już prosto - Genowefa wygenerowała duży sygnał depresyjny, który zwabił śledzie na dach klubu, gdzie Lishaory Estrelli zapędziły ryby do saka...

### Konflikty

|Trudność|Co                     |Wynik     |Sukcesy|
|--------|-----------------------|----------|-------|
|Łatwy   |szukanie śledzia       |v=        |8s     |
|Typowy  |łapanie śledzia        |vv        |6s     |
|Łatwy   |czytanie człowieka     |vv        |8s     |
|Łatwy   |łapanie śledzia        |v=        |8s     |
|Typowy  |uspokajanie Melindy    |x=, v=    |4s     |
|Typowy  |wzmocnienie mroczności |v=        |5s     |
|Trudny  |wyłapanie śledzi       |x=, x=, v=|10s    |

## Wpływ na świat

* Kić: 17
* Arleta: 5

Czyli:

Gracze:

* Genowefa Huppert: ma dostęp do pożerających depresję śledzi (hodowla domowa)
* Genowefa Huppert: Melinda Zajcew staje się jej kontaktem. Dziewczyna jej ufa i ją lubi.

MG:

* ...?

# Streszczenie

Młoda czarodziejka chciała zaimponować Warmasterowi, w którym się zadurzyła. Dlatego zdobyła tuzin śledzi syberyjskich, które miały śpiewać smutne piosenki... Niestety, coś poszło nie tak (paradoks) i śledzie rozpełzły się po okolicy. Podczas polowania na pierwszego Melinda wpadła na Genowefę, która nie potrafiła odmówić pomocy wyraźnie przestraszonej dziewczynie...

# Progresja

* Genowefa Huppert: ma dostęp do pożerających depresję śledzi (hodowla domowa)
* Genowefa Huppert: Melinda Zajcew staje się jej kontaktem. Dziewczyna jej ufa i ją lubi.
* Genowefa Huppert: wskutek dziwnych interakcji emocjonalnych co jakiś czas musi zajrzeć w okolice Zaćmionego Serca, aby się upewnić, że wszystko nadal w porządku
* Genowefa Huppert: śledzie potrafią się teleportować. Wszelka próba sprzedaży, kradzieży itp kończy się ich powrotem.
* Genowefa Huppert: śledzie trzeba karmić. Depresją. Smutkiem.

## Frakcji

* Zaćmione Serce: Warmaster rozdaje klucze lekką ręką. Każdy może tam wejść.

# Zasługi

* mag: Genowefa Huppert, przyjazna i pomocna czarodziejka, ratująca Melindę Zajcew przed nią samą i śledzi śledzie
* mag: Melinda Zajcew, szesnastoletnia czarodziejka zabujana w Warmasterze. Chciała ulepszyć klimat Zaćmionego Serca, a skończyła goniąc anty-depresyjne śledzie...
* mag: Estrella Diakon, główny łapacz siedmiu pozostałych śledzi...

# Plany

# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Nowy Kopalin
                        1. Emoklub "Zaćmione Serce"

# Czas

* Opóźnienie: 1
* Dni: 2

# Narzędzia MG

## Cel Opowieści

* null, sesja z zaskoczenia

## Po czym poznam sukces

* null

## Wynik z perspektywy celu

* null

## Wykorzystana mechanika

Mechanika 1804

Wpływ, klasycznie:

* 8 kart balans między graczami; 8-10 kart to zwykle dzień
* każda porażka = +2 pkt wpływu dla gracza
* każdy skonfliktowany sukces = +1 pkt wpływu dla gracza
* każda karta = +1 wpływ dla MG
* każde 5 kart zaokr. w górę: +1 pkt wpływu dla gracza

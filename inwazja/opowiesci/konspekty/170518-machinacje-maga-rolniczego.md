---
layout: inwazja-konspekt
title:  "Machinacje maga rolniczego"
campaign: rezydentka-krukowa
players: kić
gm: żółw
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [170515 - Niewolnica w leasingu (PT)](170515-niewolnica-w-leasingu.html)

### Chronologiczna

* [170515 - Niewolnica w leasingu (PT)](170515-niewolnica-w-leasingu.html)

## Kontekst ogólny sytuacji

Wyjdzie z misji

## Punkt zerowy:

Ż: Jaką obserwowalną korzyść może mieć Paulina z tych ziół?
K: Docelowo? Surowiec. Zioła lecznicze. Jeszcze taki powtarzalny - przygotują ogródek pod uprawę :D. Bezpieczne miejsce do uprawiania roślin.
Ż: Czemu do podtrzymywania terenu z ziołami przy życiu jednak potrzebny jest technomanta lub moc technomantyczna (okresowo)?
K: Bo chłopak nie będąc katalistą tylko tak potrafił (chciał?) to powiązać.
K: Jaki byt dałby Dianie możliwości postawienia się Ewelinie Bankierz?
Ż: Coś do kolekcji Eweliny, czego Ewelina szuka.
Ż: Co nietypowego kolekcjonuje lady Ewelina Bankierz? -> Ogólnie rozumiana sztuka, w której kobiety dominują i tępią mężczyzn. Zbudowana pod wpływem emocji.

Cień Eweliny Bankierz wpłynął na domostwo. Jakkolwiek Diana nadal w nią wierzy, jest ogólne poczucie tego, że postawiła na swoim i się bawi innymi magami. Hektor nie do końca chce stawać osobiście przeciw niej... najchętniej nie miałby z nią nic wspólnego. Niestety, jest jego ukochana Diana.

Hektor i Diana wyraźnie mają się ku sobie. Słodko się na nich patrzy. Jednocześnie są też silne tarcia o Ewelinę Bankierz między Hektorem i Dianą. W desperacji, Hektor najpewniej zrobi coś niezbyt mądrego. Ale nie zrobi tego osobiście i nie zrobi tego wobec Eweliny. Jego ewentualne pomysły, by odejść i zostawić sytuację wyparowały; zaczął nawet myśleć, czy - póki Diana tu jest - nie osiąść tu na jakiś czas.

Tymczasem w myślach Diany zaczynają pojawiać się pytania: czy tak jak ona żyje, czy tak życie musi wyglądać? Nadal jest przekonana, że Ewelina Bankierz ogólnie jej pomaga... ale czy naprawdę to jest uczciwa cena za tą pomoc?

Startowe tory (wygenerowane losowo ze wpływem wygenerowanym losowo: autogen 4 1-2-3 MULTI: 1, 5, 4, 2) uzupełnione o tory poprzedniej misji (+3 Dianie za LOVE):

1. D "Zostaję, gdzie jestem": 2
2. D "Może jednak Hektor?": 6
3. D "Odchodzę na swoje": 0
4. H "Pierwsza za darmo": 1
5. H "Pomogłem, prawda?": 1
6. H "To nie o ciebie chodzi": 0

## Misja właściwa:

**Dzień 1:**

Paulinę budzi pukanie w szybę okienną. O piątej rano. Już jest jasnawo. Paulina patrzy - widzi w oknie... zaawansowany, eksperymentalny Diakoński rój diagnostyczny. Hipnotyczny wzór. A w hipernecie Paulina wyczuła sygnał, że mag rodu Weiner się żali, że jego głupi uczeń wypuścił mu rój diagnostyczny. Paulina próbowała się osłonić magią mentalną, ale nie zdążyła... ostatnie co dała radę zrobić to wysłać sygnał do Weinera, że TEN RÓJ JEST TU ZABIERZ TO! (konflikt: F)

Dokładne badania roju. Paulina miała dość przyjemną godzinę... i nie była jedyna. Po mniej więcej godzinie rój się oderwał od Pauliny zostawiając ją w stanie lekko półprzytomnym i wyleciał z pomieszczenia. Paulina dostała sygnał na hipernecie, choć nie była w stanie go już odebrać i zapadła w przyjemny sen.

O 9:30 rano Paulina się obudziła. I Paulina jest zła. BARDZO zła. Może nie ma zbyt dużej mocy... ale może zgłosić naruszenie dóbr osobistych. A to jest bardzo sprzeczne z polityką Świecy. Zanim jednak to zrobi, idzie wiadomość do Efraima Weinera. Paulina żąda wyjaśnień. To jej teren. Jakim prawem jest napastowana...

Pół godziny później pukanie do drzwi Pauliny. Mag w ceremonialnych szatach. Przyniósł kwiaty i czekoladki. Bardzo się kaja. Też jest lekarzem - jest mu strasznie głupio; unika silnie wzroku Pauliny. Powiedział, że jego uczeń wykorzystywał ten rój do rozrywki własnej, żeby nie mówić inaczej. Ale... czegoś nie do końca mówi. Naciśnięty przez Paulinę powiedział, że eksperymentalność tego roju polega na tym, że jest bardziej autonomiczny, "rozbrykany" i nie potrzebuje Diakonów.

Postać (strona gracza):

| .I_Strat. | .I_Takt. | .Specka. | .Umiejka. | .Umiejka Mag. | .Inklinacja. | .POSTAĆ. |
|     1     |     0    |    0     |     1     |       0       |      -1      |    1     |

Okoliczności:

|  .Przygotowanie. | .Pomysł. | .Skala. | .Magia. | .Surowiec. |  .OKOLICZNOŚCI. |
|         0        |    2     |    0    |    0    |     0      |        2        |

Przeciwnik (strona NPC):

| .VERSUS. | .Diff. | .Wpływ akcji gracza. | .OPOZYCJA. |
|    2     |    1   |           0          |      3     |

Wynik:

| .Postać. | .Opozycja. | .Okoliczności. | .Rzut.    | .Wynik. |
|     1    |      3     |       2        | 17: +2 W1 |    S    |

Paulina opierniczyła go za to co zrobił i ostrzegła, że jeśli nie powie jej wszystkiego, to zaraz zgłosi to gdzie trzeba. A na pewno będą o tym wiedzieli Diakoni. Efraim się załamał. Poprosił, by Paulina tego nie robiła. Ona powiedziała, że ma już DOSYĆ magów działających na jej terenie. Efraim powiedział prawdę.

* Dostał ten rój pokątnie. Jest lekarzem, czasem pracuje z magami z nadmiarem energii magicznej; musiał mieć taką diagnostykę.
* Mag, któremu Efraim nie może odmówić zażądał dokładnej diagnostyki Diany. Więc Efraim to zrobił. Tamten mag dostał już jej dane; nic więcej go nie obchodziło.
* Czyli jakiś wpływowy przestępca zainteresował się Dianą.

Paulina się podłamała. Diana jest służką Eweliny. Ona NIE zamierza ponosić odpowiedzialności za tą sprawę. Ewelina na pewno to będzie miała nagrane. Efraim zbladł i zaczął coś tam bełkotać. Paulina nie ma dobrego mniemania o Ewelinie Bankierz, Efraim wyraźnie też nie. 

* E Przyszłe niezadowolenie Eweliny: +2, ta sprawa pokazuje, że każdy mógł mieć dostęp do danych jej służki...
* D "Zostaję, gdzie jestem": +1, u Eweliny takie rzeczy się nie zdarzały... nie takie i nie w ten sposób...

Paulina jest w stanie przebaczyć naruszenie swoich dóbr osobistych, ale byłaby MEGA zirytowana - jej goście w jej domu, którym ONA zapewnia bezpieczeństwo a tu coś takiego! Paulina zażądała co następuje:

* Efraim przekazuje Paulinie wszystkie dane które ma na temat Pauliny, Diany i Hektora. Po czym je usuwa po swojej stronie.
* Efraim pisze protokół wyrządzenia szkody - opisuje co się staje by Paulina była kryta.

I sprawa idzie nie tyle w zapomnienie a w zawieszenie. Efraim powiedział Paulinie, na jej wyraźne życzenie, kto był zainteresowany Dianą. Okazuje się, że to był detektyw. Aleksander Czykomar. Detektyw, który czasami jest powiązany z interesami Dukata. To on zażądał skanu Diany. Ale był sprytny - nie powiedział, jak Efraim ma go zrobić, powiedział, że potrzebuje go na "dziś".

Paulina rozstała się z przerażonym Efraimem Weinerem, po czym poszła do domu. Diana i Hektor nie śpią i robią sobie śniadanie. Diana utrzymuje iluzję...

Paulina opisała w skrócie sytuację Dianie i Hektorowi. Powiedziała, że wymknął się spod kontroli rój diagnostyczny. Oddała Hektorowi i Dianie ich dane biometryczne. Powiedziała Dianie, że musi wysłać sytuację Ewelinie, by nie miała żadnych kłopotów. Diana zaproponowała, że napisze w imieniu Pauliny list; ta się nie zgodziła. No i Paulina napisała w liście - odręcznie - że miało miejsce takie zdarzenie. Uczeń pewnego medyka stracił kontrolę nad diagnostycznym rojem, rój dostał się do domu... i zbadał domowników. W tym Dianę. Tien Weiner przyjął winę na siebie. Paulina bardzo przeprasza za tą sytuację.

Liścik został wysłany.

Paulina próbuje przekonać Ewelinę, by nie było żadnych reperkusji wobec ani Pauliny ani Diany.

Postać (strona gracza):

| .I_Strat. | .I_Takt. | .Specka. | .Umiejka. | .Umiejka Mag. | .Inklinacja. | .POSTAĆ. |
|     0     |     0    |    0     |     1     |       0       |       1      |    2     |

Okoliczności:

|  .Przygotowanie. | .Pomysł. | .Skala. | .Magia. | .Surowiec. |  .OKOLICZNOŚCI. |
|        0         |     1    |    0    |    0    |      0     |        1        |

Przeciwnik (strona NPC):

| .VERSUS. | .Diff. | .Wpływ akcji gracza. | .OPOZYCJA. |
|    2     |   1    |           0          |      3     |

Wynik:

| .Postać. | .Opozycja. | .Okoliczności. | .Rzut.    | .Wynik. |
|     2    |     3      |        1       | 20: +3 W1 |    S    |

F: Ewelina wyśle kogoś do ochrony Diany jutro.

Paulina dała radę wysłać liścik bez płaszczenia się. Ewelina zdecydowała się nie wyciągać konsekwencji, zwłaszcza, skoro się to Dianie najpewniej podobało ;-). Zamiast tego wyśle z Dianą maga, który będzie miał ją pilnować... i podsycać konflikty między Dianą a Hektorem?

4. D "Mam swoją godność": +3, Diana widzi, że można osiągać sukces inaczej, niż tylko płaszczeniem się.

Diana zatem zaniosła ten liścik Ewelinie.

Co powoduje, że Paulina ma największe pytanie od dawna. Dlaczego półświatek interesuje się Dianą? I to teraz?

Korzystając z faktu, że Diany nie ma, Paulina zdecydowała się skontaktować z jednym ze swoich znajomych. Głównie po to, by dowiedział się od detektywa jakie to zlecenie. Ów znajomy, Jakub Dobrocień (-1 surowiec), skontaktował się z Pauliną - oddzwonił do niej. Paulina spytała o Czykomara; Dobrocień powiedział, że Czykomar to kompetentny detektyw, zwykle pracuje dla Rodziny Dukata. Paulina poprosiła, czy Dobrocień nie mógłby jej z Czykomarem zapoznać; Dobrocień się zgodził. Poprosił o pół godzinki. Chwilę potem oddzwonił - Czykomar spotka się z Pauliną jutro rano (~8:00~10:00) w restauracji Dobrego Ducha.

Ta nazwa...

Paulina upewniła się, że Hektor poszedł zająć się ogrodem itp. po czym dopytała, jak mu się podobało. Nieźle. Może być. Hektor wyraźnie martwi się Dianą...

Resztę dnia spędziła na pacjentach i zajmowaniu się ustalaniem planu dnia...

**Dzień 2:**

(autogen 2: 1, 5)

1. D "Zostaję, gdzie jestem": 2 +0
2. D "Może jednak Hektor?": 6
3. D "Odchodzę na swoje": 0
4. D "Mam swoją godność": 3
5. H "Pierwsza za darmo": 1     +1
6. H "Pomogłem, prawda?": 1
7. H "To nie o ciebie chodzi": 0

Paulina dostała wiadomość, że koło godziny 8:00 przybędzie Diana wraz z magiem mającym się nią zajmować. Czyli najpierw wróci Diana, potem Paulina pojedzie do Czykomara. Oki, tak też można, nie jest to zła opcja. 

Paulina przygotowała śniadanie. A oni przyjechali. Diana, trzymająca iluzję mocniej niż kiedykolwiek i dobrze wyglądający mag rodu Bankierz. Nie jest podwładnym Eweliny. Jest niezależnym Bankierzem. Na wejściu odesłał Dianę do noszenia jej torby. Przedstawił się Paulinie jako Grazoniusz i powiedział, że niestety Ewelina nie pozwoliła mu zabrać swojej świty, więc niestety muszą się podzielić Dianą. Hektor słysząc to jest pomiędzy białym a czerwonym.

Paulina ma pokój dla gości, salon i swoją sypialnię. Paulina zauważyła, że to trudny problem - Bankierz nie dostanie własnego pokoju. Bankierz zaproponował więc, by Hektor i Diana przejęli salon a on zadowoli się malutkim pokoikiem dla gości. Paulina powiedziała, że się nie zgadza. Bankierz zaproponował więc, że w chacie obok może zamieszkać Hektor; gdy Paulina powiedziała, że lubi ludzi dookoła, zaproponował, że skoro ona jest ludzistką to on zapłaci tamtym ludziom by Hektor miał gdzie mieszkać. Paulina zauważyła, że Grazoniusz próbuje JEJ nie robić problemów; traktuje ją jak rezydentkę a nie jak służkę.

Hektorowi się ten pomysł bardzo nie spodobał - nie chce, by Diana była w tym samym domu co Grazoniusz. Zwłaszcza bez jego pomocy. Woli wariant, że on i Diana będą w tym samym pokoju. No ale on nie wie, że iluzja przestaje działać w nocy...

* Doceniam pana propozycję, ale z doświadczenia wiem, że magowie mieszkający u ludzi... to nie kończy się zwykle dobrze - Paulina, z udawanym smutkiem
* Rozumiem. Sam miałem kiedyś taką sytuację. Na szczęście nie doszło do złamania Maskarady - Grazoniusz, myśląc, że mówi dobrze

Paulina weźmie Dianę do swojej sypialni okazując niezadowolenie z tej sytuacji - to JEJ sypialnia. Jakiś materac na ziemi dla Diany. Grazoniusz ma zobaczyć, że Paulina robi ustępstwa, choć Paulina ani nie zapraszała, ani nie prosiła. Grazoniusz spytał, czy oni Paulinie przeszkadzają - jak tak, zabierze Dianę. Albo... może rozstawić namiot i tam zamieszkać z Dianą. Paulina zmartwiała - lepiej niech Diana zaniesie swoje rzeczy do pokoju Pauliny.

W krótkiej rozmowie wyszło co następuje:

* Paulina powiedziała Grazoniuszowi, że Diana jest jej potrzebna do tego rytuału rolniczego.
* Grazoniusz powiedział Paulinie, że to super - bezużyteczna służka się do czegoś przydała. A myślał, że Ewelina ją trzyma dla dobrego serca.
* Grazoniusz powiedział, że jak się będzie nudził to może poduczać Dianę magii - utrzymywania i kontroli energii magicznej.
* Słuchający tego wszystkiego Hektor za niedługo wybuchnie.

Paulinie, po naprawie pralki, zrobiło się trochę prania. Paulina poprosiła Dianę o wyprasowanie tego prania... a jak skończy zanim Paulina wróci, niech pójdzie do ogródka. Grazoniusz powiedział, że po praniu niech urządzi jego pokój tak jak on chce. A Paulina wysłała Hektorowi SMSa, że "czasami w pokojach pojawiają się mrówki :-(" z nadzieją, że ten zrozumie. Zrozumiał.

Aha, Diana może zostać kolejny tydzień. Hurra. 

Pół godziny drogi i Paulina dotarła do Czykomara. Detektyw spokojnie czeka w restauracji kontrolowanej przez siły Rodziny. Czykomar zaproponował jej jedzenie i picie - on stawia. Przyjął Paulinę z uprzejmą galanterią. Paulina powiedziała mu prosto z mostu, że zainteresowanie Czykomara gośćmi Pauliny sprawiło jej pewne problemy. Już. Dobór metod - rój na Dianie - sprawił, że ma na głowie Grazoniusza...

Czykomar wyraził głębokie ubolewanie. Powiedział, że miał nadzieję, że Efraim zrobi coś... etycznego. Przeprosił Paulinę. Powiedział, że tego nie planował i obiecuje, że więcej taka sytuacja nie ma prawa zaistnieć. Paulina powiedziała, że chce wiedzieć, o co chodzi Czykomarowi. On powiedział, że chce pomóc gościom Pauliny. Mimo, że przecież nie jest z tym powiązany i nie ma z tym nic wspólnego.

Paulina chce przekonać Czykomara, że więcej osiągną współpracując. O co chodzi. Skąd nagłe zainteresowanie dobrobytem tych magów?

Postać (strona gracza):

| .I_Strat. | .I_Takt. | .Specka. | .Umiejka. | .Umiejka Mag. | .Inklinacja. | .POSTAĆ. |
|     1     |    0     |    0     |     1     |       0       |      1       |     3    |

Okoliczności:

|  .Przygotowanie.        | .Pomysł. | .Skala. | .Magia. | .Surowiec. |  .OKOLICZNOŚCI. |
|  1 (leczyła Dukatowców) |    1     |   0     |    0    |     0      |       2         |

Przeciwnik (strona NPC):

| .VERSUS. | .Diff. | .Wpływ akcji gracza. | .OPOZYCJA. |
|    2     |   3    |           0          |      5     |

Wynik:

| .Postać. | .Opozycja. | .Okoliczności. | .Rzut.    | .Wynik. |
|     3    |     5      |       2        | 20: +3 W1 |    S    |

* F: na miejscu (w domu Pauliny) jest draka. Jeszcze nie apokaliptyczna. 

Czykomar się uśmiechnął. Zdecydował się powiedzieć Paulinie wszystko - i tak może.

* Hektor był tym, który nadał temat i poprosił, czy nie dałoby się jakoś pomóc Dianie
* Dukat nie lubi, gdy rodzina jest wykorzystywana przeciwko magom. Kazał rozwiązać problem Czykomarowi
* Czykomara zastanowiło, jak podrzędny mag dał radę powiązać Dianę z ludzką rodziną; to Trudny Problem.
* Czykomar zlecił badania Diany i ludzkiej rodziny
* Wstępne wyniki badań są takie, że Diana i ludzka rodzina nie są spokrewnieni

To jest bomba... Diana byłaby 3 lata wpierw szantażowana przez pomniejszego przestępcę a potem w służbie Eweliny... bo wierzyła, że ma rodzinę. Która nie jest jej rodziną. Paulina aż pokręciła głową... jeśli to jest prawda, to się musi jak najszybciej skończyć. Czykomar powiedział Paulinie z uśmiechem, że będzie poinformowana jako pierwsza.

Paulina, bardzo zaniepokojona całą tą sytuacją wróciła do domu. A tam - o rany...

Na ziemi leży nieprzytomny Grazoniusz Bankierz. Rozbita warga i nos. Silne uderzenie w głowę, ale żyje. Diana, której iluzja już nie działa i wspomagana mocą obróżki wali w Hektora jak w bęben zadając mu dość duże obrażenia; on próbuje się bronić i nie patrzeć. A ona wrzeszczy "jak mogłeś", "pobiłeś Bankierza", "zostaw mnie w spokoju".

Paulina walnęła drzwiami. Krzyknęła, że dość tego. Diana zignorowała polecenie. Paulina złapała ją za rękę i powtórzyła polecenie. Diana się lekko opanowała, spojrzała na siebie... i poleciała do łazienki. Paulina kazała Hektorowi usiąść. Nie pozwoliła mu mówić, sprawdziła stan nieprzytomnego.

Wstrząśnienie mózgu, uderzenie w głowę... rozbita warga i nos.

Paulina zażądała wyjaśnień. Hektor zaczął opowiadać:

Grazoniusz zaczął "uczyć" Dianę używania magii. Kazał jej utrzymać iluzję i zaczął modulować tak, by miała jak najtrudniej. Diana zaczęła szlochać ze strachu i trudności, on się coraz lepiej bawił. Jak iluzja padła przez moment, Hektor zasłonił Dianę przed Grazoniuszem i go odepchnął... i zrzucił ze schodów. Ten źle upadł i stracił przytomność. Diana się na Hektora rzuciła...

Paulina nie pomoże Hektorowi. Nie może. Grazoniusz musi widzieć, jak wygląda sprawa by się zlitował.

Paulina poszła porozmawiać z Dianą. Ta jest załamana. Ewelina ją wyrzuci albo Diana będzie musiała odpracować albo... czemu Hektor musiał tam być? Czemu on musiał to widzieć? Diana rozumie uczucia Hektora; ona nie jest zła, że zaatakował. Mógł pójść, Diana poprosiła, by poszedł, ale Hektor chciał zobaczyć jak wygląda szkolenie magiczne... a ona nie jest w stanie nikomu się przeciwstawić. Fizycznie czuje się w porządku, ale stało się bardzo źle.

Paulina sprawdziła - ponaciągana, ale nic strasznego. Do jutra przejdzie.

Paulina wzięła Bankierza na warsztat. Hektor ma położyć Bankierza jej w gabinecie, po czym ma sobie pójść. Sama musi rzucić zaklęcie mające na celu mu pomóc. A Diana ma przyjść pomóc Paulinie w podawaniu rzeczy. Po czym odsyła ją, zanim ta się obudzi.

Paulinie zależy, by nie było śladu. Perfekcyjna regeneracja (duży wpływ). Utrwala to (-1 surowiec) - z czasem ma organizm zastąpić to, co podtrzymuje magia.

Postać (strona gracza):

| .I_Strat. | .I_Takt. | .Specka. | .Umiejka. | .Umiejka Mag. | .Inklinacja. | .POSTAĆ. |
|     1     |     1    |     1    |     1     |       1       |      1       |    6     |

Okoliczności:

|  .Przygotowanie. | .Pomysł. | .Skala. | .Magia. | .Surowiec. |  .OKOLICZNOŚCI. |
|         1        |     0    |    0    |    1    |     0      |        2        |

Przeciwnik (strona NPC):

| .VERSUS. | .Diff. | .Wpływ akcji gracza. | .OPOZYCJA. |
|    2     |    2   |           2          |     6      |

Wynik:

| .Postać. | .Opozycja. | .Okoliczności. | .Rzut.    | .Wynik. |
|    6     |      6     |       2        | 19: +2 W1 |    S    |

Efekt Skażenia: Wszyscy są perfekcyjnie wyleczeni. Hektor, Diana, Grazoniusz. W ramach podniesionego wpływu, wszyscy czują się świetnie pod względem fizycznym i energetycznym. I świecą jak pelikany magią leczniczą i wspomagającą przez pewien czas.

Grazoniusz otworzył oczy.

* Czy pani gość... - Grazoniusz, zaczynając
* Cóż... widzę, że kuzynka nie powiedziała panu paru rzeczy... - Paulina, spokojnie
* Zamieniam się w słuch, czego teraz kochana kuzynka mi nie powiedziała - Grazoniusz, z udawanym spokojem
* Ten czarodziej zakochał się w Dianie - Paulina

Głupia mina Grazoniusza. 

* W niej? To jak w meblu... - Grazoniusz, parskając śmiechem
* Tien Bankierz, ona nie jest meblem, tak samo jak Hektor, ja czy Ty - Paulina, zdecydowanie
* Możemy się nie zgadzać, ale ona jest tak samo magiem jak Ty czy ja - Paulina
* A ona w nim? - Grazoniusz
* Dużo na to wskazuje - Paulina, ostrożnie

Grazoniusz parsknął śmiechem. Zapytany o Ewelinę powiedział, że Ewelina nie ma szczególnie zalet. 

* Biegająca nago pod iluzją dziewczyna i mag niezdolny do kontrolowania swoich popędów, razem... - Grazoniusz - A to wyborne
* Biega nago nie z własnego wyboru... - Paulina
* Patrząc co stało się z tą liberią, lepiej niech się przyzwyczai... - Grazoniusz

Grazoniusz stwierdził, że teraz musi wyzwać Hektora na pojedynek na śmierć i życie. Paulina powiedziała, że nie musi. On zauważył, że Ewelina go skompromituje na dworze - maga bojowego pojechał rolnik. Paulina powiedziała, że Diana stłukła Hektora. Naga dziewczyna...

Postać (strona gracza):

| .I_Strat. | .I_Takt. | .Specka. | .Umiejka. | .Umiejka Mag. | .Inklinacja. | .POSTAĆ. |
|    0      |    1     |     0    |     1     |      0        |      1       |    3     |

Okoliczności:

|  .Przygotowanie. | .Pomysł. | .Skala. | .Magia. | .Surowiec. |  .OKOLICZNOŚCI. |
|        -1        |     1    |    0    |    0    |     1      |        0        |

Przeciwnik (strona NPC):

| .VERSUS. | .Diff. | .Wpływ akcji gracza. | .OPOZYCJA. |
|    2     |   2    |          0           |      4     |

Wynik:

| .Postać. | .Opozycja. | .Okoliczności. | .Rzut.    | .Wynik. |
|     3    |     4      |       0        | xx: +x Wx |  S / F  |

* F: Grazoniusz zażąda, by Hektor się pokajał i poprosił o łaskę. Przy Dianie.
* F: Ewelina zrobi z tego kryształ Mausów, który stanie się na pewien czas hitem. Taka komedyjka.
* R: Diana dostaje silne wzmocnienie toru Eweliny: "Ciągnie mnie do Hektora... ale faceci to same problemy"

Paulina mając taką sytuację, zdecydowała się porozmawiać z Hektorem.

* Zaatakował maga rodu. Będą z tego poważne problemy.
* Diana nosi rejestrator. Wszystko będzie wykorzystane.
* Diana ucierpi. Jak dojdzie do pojedynku, Diana będzie załamana.

Więc Hektor MUSI przeprosić i się pokajać wobec Grazoniusza w obecności Diany...

Postać (strona gracza):

| .I_Strat. | .I_Takt. | .Specka. | .Umiejka. | .Umiejka Mag. | .Inklinacja. | .POSTAĆ. |
|     1     |     1    |     0    |     1     |       0       |       1      |     4    |

Okoliczności:

|  .Przygotowanie. | .Pomysł. | .Skala. | .Magia. | .Surowiec. |  .OKOLICZNOŚCI. |
|        2         |     1    |    0    |    0    |      0     |         3       |

Przeciwnik (strona NPC):

| .VERSUS. | .Diff. | .Wpływ akcji gracza. | .OPOZYCJA. |
|    2     |   2    |           2          |     6      |

Wynik:

| .Postać. | .Opozycja. | .Okoliczności. | .Rzut.    | .Wynik. |
|    4     |     6      |        3       | 16: +2 W1 |     S   |

Hektor się zdecydowanie przejął. Zrozumiał bardzo poważne konsekwencje swoich decyzji. Ukorzył się przed Bankierzem, naprawdę mocno, bardzo przeprosił - dla Diany. Od tej pory będzie pamiętał, gdzie jest miejsce Diany i jego w tej relacji. Grazoniusz przyjął przeprosiny. Diana, jakkolwiek zobaczyła, że Hektor nie jest taki silny jak myślała, ale trochę do góry poszło jej też to, że w sumie... skoro on umie się dopasować... może coś z tego może być.

**Stan torów:**

1. D "Zostaję, gdzie jestem": 2 +1      = 3
2. D "Może jednak Hektor?": 6 +1        = 7
3. D "Odchodzę na swoje": 0             = 0
4. D "Mam swoją godność": 3 -1          = 2
5. D "Faceci to problemy": 0 +3         = 3
6. H "Pierwsza za darmo": 2             = 2
7. H "Pomogłem, prawda?": 1             = 1
8. H "To nie o ciebie chodzi": 0        = 1
9. H "Muszę być silniejszy": 0 +2       = 2
10. G "Wypadam z łask Eweliny": 0 +3    = 3

**Interpretacja torów:**

Ewelina osiąga elementy tego, co chce. Grazoniusz zrobił coś głupiego i wypadł z dworu Eweliny, Diana jest zestresowana przez okolicznych facetów i coraz bardziej skupia się na tym, że może w towarzystwie dziewczyn jest po prostu lepiej... co prawda u Diany powoli pojawiają się elementy godności, ale nadal jest zdecydowana zostać u Eweliny bardziej niż cokolwiek innego.

Hektor i Diana są w sobie już naprawdę zakochani.

Hektor powoli orientuje się, że nie jest w stanie wygrać przeciw potędze Bankierzy samotnie. Coraz bardziej zbliża się do Rodziny Dukata, zwłaszcza, że pierwsza dawka była za darmo. Coraz bardziej Hektora kusi, by rosnąć w siłę, być silniejszym i potężniejszym, by musieli się z nim liczyć.

Grazoniusz zaczyna się stresować swoją pozycją. Musi jakoś odzyskać sławę i poklask; w innym wypadku skończy jako ofiara dowcipów. Musi jakoś się zemścić na Hektorze ale tak, by to było wyrafinowane i nie robiło nikomu krzywdy - a by inni arystokraci mogli to docenić... no i musi wrócić do łask Eweliny.

# Progresja

* Hektor Reszniaczek: jest zakochany w Dianie.
* Diana Łuczkiewicz: jest zakochana w Hektorze.
* Grazoniusz Bankierz: straszny ubytek pozycji; mag bojowy pokonany przez maga-rolnika... wypada z dworu Eweliny Bankierz

# Streszczenie

Hektor skontaktował się z siłami Dukata. Detektyw mafii stwierdził, że to dziwne, że Diana ma swoją rodzinę i zaczął badać, czy to naprawdę JEJ rodzina... w wyniku czego zrobiono Dianie pełną diagnostykę z zaskoczenia. Ewelina uznała, że musi Dianie wysłać obstawę - wysłała kuzyna Grazoniusza. Gdy Paulina dowiadywała się od detektywa o co tu chodzi, Hektor zepchnął Grazoniusza ze schodów. Bankierz stracił sporo reputacji; Hektor się musiał kajać. Ewelina zrobiła z tego popularną komediową kostkę Mausów...

# Zasługi

* mag: Paulina Tarczyńska, rozpaczliwie łagodzi wszystkie możliwe konflikty między magami... ma też noc swojego życia ;-). Z owadami. Troszkę swata Dianę i Hektora.
* mag: Diana Łuczkiewicz, przekonuje się do Hektora, nie do końca wie, czego chce, jest rozdarta... służka, która boi się przestać być służką. Wszystko dla rodziny... która może nie jest jej.
* mag: Hektor Reszniaczek, ma problemy z kontrolowaniem impulsów (pobił Grazoniusza o Dianę), kajał się pokornie i zapoczątkował lawinę wchodząc w konszachty z Rodziną Dukata.
* mag: Efraim Weiner, lekarz, który dostał od Rodziny Dukata badawczy rój eksperymentalny... i musiał zapłacić. Najpewniej podpadł Ewelinie Bankierz. Boi się.
* mag: Aleksander Czykomar, bezwzględny detektyw działający u Dukata; dał się przekonać Paulinie i powiedział, że pracuje nad tym, czy rodzina Diany to prawdziwa rodzina.
* mag: Jakub Dobrocień, znajomy z półświatka Pauliny na Mazowszu; skontaktował ją z detektywem pracującym dla Dukata i powiedział o nim parę słów.
* mag: Grazoniusz Bankierz, rozpuszczony arystokrata; mag bojowy i katalista; dał się zepchnąć ze schodów magowi rolniczemu. Wypada z łask Eweliny. Złośliwy, acz wobec Pauliny zachowuje się nienagannie.
* mag: Ewelina Bankierz, wyraźnie bawi się całą tą sytuacją jedynie wprowadzając komplikacje. Nie wiadomo, czego chce. Ale opiekuje się trochę Dianą. Nie lubi kuzyna (z wzajemnością)

# Lokalizacje

1. Świat
    1. Primus
        1. Mazowsze
            1. Powiat Pustulski
                1. Krukowo Czarne
                    1. Gabinet Pauliny, i jednocześnie jej domek, gdzie mieszkają już: Paulina, Diana, Hektor, Grazoniusz... miejsce Wielkiej Draki.
                1. Orłostwo Wielkie
                    1. Orłostwo Mniejsze
                        1. Restauracja Dobrego Ducha, kontrolowana przez siły Dukata, gdzie spotkała się Paulina z Czykomarem.

# Czas

* Dni: 2

# Warianty Przyszłości

(1k6 wybiera wariant)

1. **Przyszłość Diany**
    1. "Zostaję, gdzie jestem": Diana zostaje u Eweliny, akceptując swój los.
    2. "Może jednak Hektor?": Diana daje Hektorowi szansę. Może warto z nim jednak być?
    3. "Odchodzę na swoje": Diana odchodzi od Eweliny, próbując przejąć sprawy w swoje ręce
    4. "Mam swoją godność": Diana decyduje, że nie musi się płaszczyć; nie musi wszystkiego akceptować
1. **Cena Dukata**
    5. "Pierwsza za darmo": Hektorowi się upiecze; Dukat ma działania długoterminowe
    6. "Pomogłem, prawda?": Hektor będzie musiał odpłacić Dukatowi za to, co otrzymał
    7. "To nie o ciebie chodzi": Dukata nie obchodzi Hektor - a Diana lub Ewelina. Lub coś innego sprawdzał

# Strony

### Hektor:

Scena zwycięstwa:

* Ewelina przeprasza Dianę, która odzyskała poczucie własnej wartości.
* Hektor i Diana są razem. Mieszkają razem gdzieś, gdzie są blisko natury i niezależni od wszystkich.
* Hektor i Diana utrzymują kontakty z innymi magami i ludźmi.

Pragnie:

* Diany. Jej dobra, samodzielności, osoby... jego myśli obracają się wokół Diany.
* Zemsty na Ewelinie za traktowanie Diany i traktowanie magów jak zabawki.

Nienawidzi:

* Złego traktowania Diany przez Ewelinę. Faktycznie, nienawidzi Eweliny.
* Głupiej arystokracji magów, nie ceniących uczciwej pracy i autonomii innych jednostek.

### Ewelina:

Scena zwycięstwa:

* Diana wraca do niej, rozumie swoje miejsce na świecie i daje sobie spokój z tą ludzką rodziną.
* Jest szanowana i podziwiana przez wszystkich dookoła. Rezydentka składa jej pokłon.
* Odepchnęła od siebie wszystkich problematycznych magów i nie musi dzielić się władzą.
* Ma władzę praktycznie absolutną; mała control freak ;-).

Pragnie:

* "Czegoś prawdziwego". Prawdziwego konfliktu, starcia, uczuć...
* Pozbyć się ze swojego otoczenia wszystkich bezwartościowych kuzynów i absztyfikantów.

Nienawidzi:

* Zależności od swojego rodu i swojej złotej klatki.
* Tego, że wolno jej się jedynie bawić. Nie jest traktowana poważnie.
* Mężczyzn, którzy uważają, że kobiety są od nich gorsze.

### Dukat:

Scena zwycięstwa:

* Ewelina Bankierz jest zabawką w jego rękach. Może zachować swoją pozycję... ale to on pociąga za sznurki.
* Hektor i Diana są w jego obszarach wpływu.
* Interes się kręci bez zbędnych problemów.

Pragnie:

* Zabezpieczenie swoich interesów; jego działania wymagają dużych kosztów.
* Osobiście chce prztyczknąć w nos Ewelinę Bankierz, "udzielną księżną" tego terenu.

Nienawidzi:

* Wykorzystywania rodziny przeciwko komukolwiek.
* Działania, które nie niosą za sobą konsekwencji i ogólnie są puste.

# Narzędzia MG

## Cel misji

1. Misja weryfikująca działanie 'Efekt Skażenia jeśli czar ma rzut 16+'. Znowu.
2. Misja sprawdzająca 'przygotowanie/pomysł' vs 'sytuacyjne, sprzętowe'.
3. Misja weryfikująca działanie 'Wariantów Przyszłości'
4. Misja weryfikująca działanie 'Stron'

## Po czym poznam sukces

1. Efekty Skażenia będą miały następujące cechy:
    1. Będą zmieniały fabułę
    1. Nie będą sprawiać, że magowie boją się czarować, ale... wiąże się z magią pewien koszt LUB fabularny
    1. Nie będą nadmiarowe i "o nie, po cholerę mi to"
2. Przygotowanie / pomysł działają tak:
    1. Łatwiej dopasować konflikty.
    1. Nie będzie "gorzej" niż sytuacja + sprzęt
    1. Wartości będą nadal sensowne, akceptowalne. Czyli funkcja Okoliczności się nie zmieni.
    1. Premiowanie pomysłu dynamicznie i przygotowania przed akcją.
3. Warianty mają następujące efekty:
    1. Przy sześciu torach (naprawdę, 2 tory z 3 wariantami) jest to łatwe w zarządzaniu
    1. Przy sześciu torach będzie wysoka intensywność akcji - będzie widać, co się stało
    1. Świat "żyje". Czyli: dzieją się rzeczy niezależnie.
4. Strony mają następujące efekty:
    1. Dzięki stronom zawsze wiem kto wykonuje działania - sensowne działania. Dzięki temu NPCe żyją.
    1. Wiem czego oczekiwać od poszczególnych frakcji i subfrakcji; dążą do ideału i mają pragnienia i nienawiści
    1. Gracze mogą rozgrywać strony między sobą.
    1. Strony są logicznie spójne między działaniami.

## Wynik z perspektywy celu

1. Efekty Skażenia:
    1. Żółw: Efekt Skażenia był jeden - wyleczenie wszystkich było wygodne dla WSZYSTKICH stron. Nie zmieniło fabuły, acz odebrały jeden bonus ;-).
    1. Żółw: Wyglądało to bardzo sympatycznie i poprawnie.
    1. Kić: Dodał to, że Paulina ma większy szacun; fajne zaklęcie. Odjął to, że Hektor nie ucierpiał, więc Grazoniusz jest wredniejszy. Ogólnie OK.
2. Przygotowanie/pomysł:
    1. Żółw: Nie było problemu z dopasowaniem konfliktów.
    1. Żółw: Nie było "gorzej" niż sprzęt / sytuacja. Parametry są podobne.
    1. Żółw: Nie zauważyłem tego premiowania; wymaga więcej testowania.
    1. Kić: Zauważyłam różnicę, ale nie jestem pewna czy mi się podoba...
3. Warianty:
    1. Żółw: Tory się rozrosły do 10. To chyba normalne, że tory się powiększają. Było łatwe w zarządzaniu.
    1. Żółw: Po raz kolejny wiem co się stało po misji i mam wyjścia na kolejne misje, zwłaszcza w połączeniu ze Stronami.
    1. Żółw: Tym razem nie widziałem efektu żywego świata. A przynajmniej, tego nie odczułem. Ale podsumowanie torów bardzo pomogło.
    1. Kić: Przeszkadzało mi że nie widziałam torów o których wiem.
4. Strony:
    1. Żółw: Doskonałe narzędzie! Mogę łatwo zidentyfikować FAKTYCZNYCH przeciwników i FAKTYCZNE strony aktywne na misji.
    1. Żółw: Wiem czego oczekiwać od poszczególnych frakcji, zadziałało.
    1. Żółw: Nie wystąpiło rozgrywanie stron między sobą. Do sprawdzenia. Strony wyglądają na logicznie spójne.
    1. Kić: Koncept widziałam na noc, ale nie widziałam tego w akcji.

## Wykorzystane tabelki

Postać (strona gracza):

| .I_Strat. | .I_Takt. | .Specka. | .Umiejka. | .Umiejka Mag. | .Inklinacja. | .POSTAĆ. |
|           |          |          |           |               |              |          |

Okoliczności:

|  .Przygotowanie. | .Pomysł. | .Skala. | .Magia. | .Surowiec. |  .OKOLICZNOŚCI. |
|                  |          |         |         |            |                 |

Przeciwnik (strona NPC):

| .VERSUS. | .Diff. | .Wpływ akcji gracza. | .OPOZYCJA. |
|          |        |                      |            |

Wynik:

| .Postać. | .Opozycja. | .Okoliczności. | .Rzut.    | .Wynik. |
|          |            |                | xx: +x Wx |  S / F  |

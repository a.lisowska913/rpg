---
layout: inwazja-konspekt
title:  "Spleśniała dusza terminuski"
campaign: powrot-karradraela
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [160417 - Symptomy kryzysu Świecy (AW)](160417-symptomy-kryzysu-swiecy.html)

### Chronologiczna

* [160406 - Najprawdziwszy sojusz Blakenbauerów (HB, KB, AB, DK)](160406-najprawdziwszy-sojusz-blakenbauerow.html)

### Logiczna

* [160404 - The power of cute pet (SD)](160404-the-power-of-cute-pet.html)

## Kontekst misji

- Wiktor próbuje podporządkować sobie Silurię. Siluria próbuje podporządkować sobie Wiktora. Dalej.
- Gerwazy usunięty z drogi Silurii; na to miejsce weszła Sabina Sowińska. Wioletta stoi przy Silurii, nie Wiktorze.
- Arazille się ujawniła, pożarła Kariatydę, ale Blakenbauerowie ją odparli. Równolegle zaczyna się plaga Irytki Sprzężonej.
- Weszło Millennium na teren Kopalina za prośbą Wiktora; działają w kompletnym ukryciu.
- Millennium wycofuje magów KADEMu przed siłami Wiktora za prośbą Silurii.
- Wiktor próbuje zdobyć kontrolę nad Szlachtą przy użyciu Diany w jakiś dziwny sposób...

## Cel misji:

Cel misji: 
- Rozpoczęcie ostatniej fazy kampanii.
- Sprawdzenie, czy Wiktor będzie rządził Szlachtą czy przegra wojnę o Szlachtę.

## Punkt zerowy:

- Wiktor zmienił zdanie co do Silurii. Ma zamiar wykorzystać czarodziejkę KADEMu jako partnerkę. Jest kobietą, więc niewolnicę - ale godną partnerkę. Może się podzielić.
- Wiktor ma dość być "tym trzecim lub czwartym" w Szlachcie. On chce rządzić Szlachtą. Ma dość oporu ze strony wszystkich, łącznie z własnym rodem.
- W skrócie, Wiktor chce iść w kierunku na Overlord Supreme i przejąć Kopalin. A jeśli ma do tego oddać jego część Millennium, niech tak będzie.
- Mirabelka Diakon (Millennium) jest opłacana przez Wiktora i ma za zadanie pomóc Wiktorowi zdobyć kontrolę nad magami poza Kopalinem.
- Amanda Diakon (Millennium) powoli wchodzi na teren Kopalina pod pozorem sił Mirabelki. Jej cel to Ozydiusz i CruentusReverto oraz Milena.
- Marian Łajdak i Andżelika Leszczyńska są ukrywani i przenoszeni przez Millennium by ich sensownie wycofać z Kopalina zanim złapie ich Wiktor.
- Sabina Sowińska próbuje przejąć sytuację i zbudować kontrfront wobec Wiktora.
- Dagmara Wyjątek próbuje opanować Sabinę i ochronić interesy Wiktora.
- Wiktor próbuje zająć się Dianą i schować wszystko przed Sabiną.

## Misja właściwa:

Dzień 1:

Normalne dni Silurii toczą się ostrożnie. Wpierw skupia się na Dianie, łamiąc ją wspólnie z Wiktorem ale jednocześnie dyskretnie dostosowuje Wiktora do myśli, że ona jest nadrzędna (Siluria). Uczy Wiktora podległości i daje mu zadowolenie i nową formę radości. Uzależnia go od siebie. Jednocześnie on cały czas myśli, że jest dominujący. Że może przestać w dowolnej chwili.
Oni zawsze tak myślą.
Jednocześnie Siluria się boi. Czuje, że coś jest bardzo nie w porządku. Idzie jej... za łatwo, ale nie. To coś więcej, aura of dread. I Siluria nie potrafi jej zlokalizować, nie wie nawet kiedy to się zaczęło i nie ma to związku z pojawieniem się Sabiny. Nie było tego - ale się pojawiło. Kiedyś. I tak naprawdę... może było od początku, ale cichsze...

W chwilach, w których Siluria nie jest zajęta, próbuje zrozumieć Sabinę. Sabina nadal uważa, że Siluria jest do uratowania i Sabina całkowicie łyknęła "Siluria as pet" z poprzedniej misji. Udało się Silurii na tyle przebić przez bariery Sabiny, by spróbować ją zrozumieć - udało się. Cel Sabiny tutaj to puryfikacja. Oczyszczenie tego wszystkiego, doprowadzenie Kopalińskiego oddziału Szlachty do macierzy - dobrej, pomocnej Świecy. Ona chce doprowadzić do wzmocnienia Świecy. Jest lojalistką, najpewniej by pracowała z Ozydiuszem gdyby miało to jakikolwiek sens (hint: Ozydiusz nie lubi kobiet). Jednocześnie chce chronić ród. Niestety, jej podejście do Silurii bawiącej się Wiktorem byłoby wrogie.

Wieczorem, swawoląc z Wiktorem, Siluria sprowokowała Wiktora do tego, by ten z nią porozmawiał o Dagmarze. Skłoniła go do tego, by wsadził taką samą uprząż jak Wioletcie czy jej do tego pancerza Dagmary. Wiktor uśmiechnął się na tą myśl, ale spoważniał. Nie może. Naciśnięty przez Silurię (konfliktem), wyjaśnił; ten pancerz pochodzi od wiernego sojusznika; jest neurowspomagany. Gdy Dagmara ma go na sobie nie do końca jest sobą. Jest jak drugie ciało. Wiktor pozwolił nosić go Dagmarze, ponieważ jest mu oddana i jest lojalna. Tylko jej zaufa. Taka uprząż mogłaby zniekształcić połączenie. Też Wiktor się zamyślił; Dagmara błagała go, by nie musiała go zakładać, ale jej kazał. Na początku to była dla niej tortura. Teraz... już nie. Przywykła.

Dzień 2:

Do Silurii dotarła wiadomość o Irytce. Wiktor zapewnił wszystkich, że to skrzydło i Kompleks Centralny są bezpieczne.
Siluria po prostu stwierdziła, że to jest coś co jej specjalnie nie rusza - ale nie zamierza czarować.
Spięcie między Sabiną i Wiktorem; czarodziej powiedział, że wysłał Dagmarę a Elea poszła pomóc Ozydiuszowi. To i tak dużo. Wiktor nie zamierza robić nic więcej. Uśmiechnął się - ze Szlachty pomaga Diana (proxy). Sabina próbowała go przekonać. 

Siluria zdecydowała się zrobić ruch. Chce, by Sabina zauważyła, że Siluria wspiera Świecę (dobro). A u Wiktora - to podniesie standing Wiktora w Szlachcie.
Siluria osiągnęła swój cel. Słodkie oczka, zmartwiona wszystkimi i tą straszną Irytką... Wiktor skupił się na tym, by jak najbardziej pomóc ofiarom. Nie pomagając Ozydiuszowi, ale działając pomocnie - lazaret, rozdał część artefaktów, wysłał golemiczną straż... ogólnie, utrzymać Kopalin.
I tego Ozydiusz by się nie spodziewał. Szkoda, że Leonidas...

Wiktor zostawił w bazie tylko Wiolettę. No i Silurię. Sabina poszła na Irytkę.
Siluria zauważyła, że z Wiolettą jest coś nie tak; smutna, nieszczęśliwa, nie do końca taka jak ostatnio. Na treningu Siluria ją zmęczyła i rozluźniła; na sam koniec treningu Siluria próbowała wyciągnać co się stało. Wioletta wybuchła. Sabina jest PRAWDZIWĄ terminuską, jej prywatną bohaterką; kimś wybitnym i wyjątkowym. Wioletta uczyła się o czynach Sabiny... a teraz kim jest Wioletta? Sex slave. Bezużyteczną, zmarnowaną terminuską bojącą się wszystkiego. A Sabina? Wymusiła na Wiktorze pomoc w walce z Irytką (Wioletta nie wie o działaniu Silurii).
Sabina powiedziała Wioletcie, że ta może być czymś więcej. Zaproponowała jej przeniesienie gdzieś, gdzie jej talenty zostaną wykorzystane. Wioletta się boi... Wiktor ją sobie dostosował do wyglądu i zachowania "młodej dziewczynki", i Wioletta dostosowała się do swojego pana.

Siluria przekonała Wiolettę, by ta jednak pomyślała o przeniesieniu. Szkoda Wioletty pod Wiktorem. Bardzo jej szkoda. Siluria zgodziła się z opinią Sabiny, że Wioletta która nie gardzi innymi gildiami może być całkiem niezłym terminusem Świecy. Takim, jakiego Świeca teraz potrzebuje. Siluria próbuje umocnić poczucie swojej wartości Wioletty i sprawić, by ta poczuła się pewniej. Pomóc Sabinie. Ale tak, by Wioletta sama jej nie zostawiła.

Wioletta: wpływ Wiktora: 6 # 6 # 14 = 26.
Siluria: przyjaciółka (6) # szok Sabiny (6) # 12 -> 24. -> 26.

Silurii udało się uszkodzić skorupę bezsilności i rozpaczy dookoła Wioletty. Młoda terminuska zobaczyła, że jest świat poza tym wszystkim i być może ona - Wioletta - może stać się częścią tego świata. Siluria ma nadzieję, że Sabina nie jest głupia i dalej pójdzie za ciosem...

By odwrócić uwagę Wiktora od Wioletty, poszła do Wiktora zająć się Dianą. Grunt, żeby się nie zajmował Wiolettą. Siluria ma już na tyle silny wpływ, że Wiktor nie zauważył, co ona zrobiła.

Wieczorem wróciły Elea, Sabina i Dagmara. Dagmara opowiedziała o ataku Ozydiusza na Szlachtę. Plus jest taki, że Wiktorowi wypadła z głowy Wioletta. Minus, że Wiktor uznał, że Ozydiusz wypowiedział Szlachcie wojnę. Sabina od razu stanęła przeciw Wiktorowi; Ozydiusz ma na głowie Irytkę; nie mogą go teraz zaatakować. Wiktor ryknął, że muszą. Właśnie teraz. Sabina lodem, Wiktor ogniem. (14v15->fail). Wiktor stwierdził, że Ozydiusz musi zostać ukarany. Sabinie się nie udało. Terminuska powiedziała, że nie dopuści do tego. Wiktor powiedział, że to ona rozpocznie tą walkę z Ozydiuszem. Sabina zaśmiała się "tak jest, lordzie Sowiński", po czym wyszła z pokoju, śmiejąc się perliście. Wiktor cały taki wściekły jak nigdy. Wioletta nie oddycha. Dagmara z chłodnym spojrzeniem - czysta nienawiść wobec Sabiny. Dagmara nie zauważa Silurii. Przynajmniej tyle, jeden problem z głowy...

By rozładować Wiktora, Siluria przypadła na kolana go trochę rozładować. Odesłała Wiolettę jednym ruchem ręki, po czym zabrała się do zadowalania Wiktora. Siluria chce go uspokoić. Udało jej się. Wiktor się odprężył. Powiedział Dagmarze, że przecież Siluria robi to co jest potrzebne. Czemu Dagmara nie rozwiąże jakiegoś jego problemu?
Dagmara zasalutowała. Nie ma w niej nienawiści do Silurii (ku wielkiemu zadowoleniu KADEMki). Powiedziała, że zajmie się tym.

Wiktor i Siluria poswawolili, po czym Wiktor wysłał Silurię przodem; on chce coś jeszcze zrobić. Siluria poszła więc spotkać się z Eleą przy Dianie. Od razu Siluria zauważyła, że Elea jest bardzo niespokojna.
Siluria położyła Elei uspokajająco rękę na ramieniu. Lekarka jest spięta. Elea zaczęła Silurii z trudem wyjaśniać, że "wszystko zaczyna się od Karradraela i kończy się na Karradraelu". Że Wiktor wygra, i Siluria może go wspierać i wygrać z nim lub przegrać bez niego. Na Eleę coś bardzo, bardzo silnie wpłynęło; coś się dowiedziała gdy badała Irytkę (albo gdy była na mieście). [Kić: to jest po zniknięciu Adeli Maus i w okolicach działania Jana Weinera koło Lorda Jonatana]. Grunt, że Elea cierpi. Jest bardzo skonfliktowana i nie wie co robić. Ale wykona zadanie.
Elea powiedziała, że drzewo... ona jest drzewem, i gałęzie obumierają. I nie ma Karradraela...

Tak czy inaczej, przesunęli Dianę trochę dalej.

Dzień 3:

Wcześnie rano, Silurię obudził zew. Sygnał. Wie, co to - pieśń kralotha. Z pokoju gościnnego, niedaleko pokoju Silurii.
Oczywiście, Siluria się przeszła. Strażniczką jest Wioletta. Jest biała jak ściana. Kraloth jest w środku, jest samotny; Siluria z radością się z nim spotkała.
Kraloth przedstawił się jako Laragnarhag. Jest kralothem Millennium; został tu poproszony przez "dziewczynę w krwawej puszce" (Dagmarę). Ale to znaczy, że coś z jej power suitem jest powiązane z Krwią; ma sens w obliczu tego co powiedział Wiktor, choć tego akurat się Siluria po nim nie spodziewała. Kraloth powiedział, że Wioletta smacznie pachnie; zażąda jej. Siluria odmówiła; odda mu Dianę i siebie # Wiktora. Kraloth się zgodził. Jeszcze nie wie czemu tu jest, ale ma zrobić "coś" czego oni nie umieją. Coś zbadać. Coś dostosować. Siluria poprosiła, by wyjaśnił jak się dowie. Kraloth potwierdził.

Siluria wyszła. Wioletta dalej biała; Wiktor ją straszył kralothami. Siluria uspokoiła ją, że nic jej nie będzie. Wioletta powiedziała jeszcze Silurii, że pojawił się nowy mag - niejaki Fryderyk Chwost. Przyprowadziła go Sabina; to przyjaciel Dagmary (Wioletta cała wstrząśnięta, że Dagmara ma przyjaciół).
Siluria zapukała do pokoju Fryderyka (jako metresa Wiktora, może). Fryderyk ją zaprosił.

Fryderyk Chwost okazał się być artystą. Trochę gry na instrumentach, trochę tańca, trochę śpiewu... taki troszkę podróżnik-bard-lutmistrz. Fryderyk szczerze kocha (miłością ojcowską) "małą Dagmarę Zajcew". Słyszał o tragedii, ale z przyjemnością jej pomoże jeżeli jest w stanie. I Siluria zrozumiała, co robi Sabina - rozmontowuje Wiktorowi jego mały krąg. Teraz Siluria jest przekonana, że nie zobaczy już Gerwazego - bo Sabina coś wymyśliła...

Siluria zostawiła Fryderykowi miłe chwile, po czym poszła do Wiktora spać. Fryderyk jest niegroźny. Na razie.

W dzień znowu: Elea, Dagmara i Sabina poszły zająć się Irytką. Wiktor realokował kralotha do Diany i swojego buduaru. Tym razem postępy w deprawacji Diany były zdecydowanie wyższe.

Wieczorem Siluria usłyszała scenę. Podniesiony głos na korytarzu. 
Dagmara krzyknęła na Sabinę, co ta sobie myślała sprowadzając Fryderyka. To była przeszłość. To nie wróci. Sabina, że to może jej pomóc... Dagmara zaatakowała z zaskoczenia ciężko raniąc Sabinę. Drugi cios by ją dobił, gdyby nie Siluria. Gdy KADEMka kazała jej odejść (bo to nie pomoże Wiktorowi), Dagmara odeszła bez słowa.
Siluria wezwała kralotha, by ten wylizał rany Sabiny. Grzecznie. Laragnarhag wykonał prośbę, choć niechętnie; Siluria wezwała Wiktora. 
Ten odprowadził Sabinę do skrzydła medycznego Świecy i poprosił, by ona tego nie zgłaszała. Problemy rodzinne. Sabina się zgodziła. 

W nocy, Wiktor i Siluria. Siluria próbowała Wiktora przekonać, by ten kazał Dagmarze zdjąć power suit. Wiktor zaprzeczył. Spojrzał Silurii prosto w oczy i powiedział, że to on kazał Dagmarze znaleźć pretekst by unieszkodliwić Sabinę na jeden dzień. Bo ona ma specjalny hipernet. 

Dzień 4:

Ku wielkiemu zaskoczeniu Silurii, nie stało się nic szczególnego. Ale poprosił ją do siebie Laragnarhag. Kraloth zażądał, by Siluria do niego przyszła i poszła do jego macek. Siluria z przyjemnością oddała się ekstazie... a w rzeczywistości zaczęli telepatyczną rozmowę.

Kraloth powiedział Silurii, że przekształcał krew Sabiny. Był drugi defiler. Inny byt, ten komunikujący się przez zbroję Dagmary. On zadawał serię pytań. Chciał się dowiedzieć dużo bardzo precyzyjnych i specjalistycznych rzeczy o krwi Sabiny; kraloth nie wie po co. Kraloth powiedział też, że wprowadzili wzory zniewolenia. Siluria jest przerażona; nie wiadomo co spotka Sabinę, ale cokolwiek by to nie było, Siluria tego nie chce. Niestety, nie widać żadnej metody ostrzeżenia jej ani nawet zadziałania; na dworze jest Irytka a Siluria jest dziwnie przekonana, że nikt nie pozwoli jej wyjść z pokoi Wiktora tego dnia.
Siluria nie może nic zrobić... 

W nocy Siluria zorientowała się, że następnego dnia Diana będzie "kompletna". Powiedziała Wiktorowi, że lepiej się upewnić, zwłaszcza, że mają kralotha. Wiktor się zgodził. Małe zwycięstwo Silurii. A Siluria chce ugruntować swoją władzę nad Wiktorem.

Dzień 5:

Od rana przyszła do Silurii Wioletta. Cała szczęśliwa. Wiktor otworzył im linię kredytową i kazał pójść sobie kupić ładne ciuszki. Mają się nie pojawiać przed 17:00. Siluria jeszcze wyprosiła u kralotha, by ten obserwował jeśli jest w stanie. Bo nawet taka obserwacja jest lepsza niż żadna...

Ogólnie, dziewczyny bawiły się nieźle. Siluria specjalnie kupowała dla Wioletty dwa typy ubrań. Pierwszy - słodka trzpiotka ("dla Wiktora") a drugie - no-nonsense, terminuska, poważna acz inteligentna. Takie, by Wioletta była "sensowna". I pokazała Wioletcie, że obie te wersje mogą być prawdziwe... jeśli Wioletta tylko chce.

Wioletta się zdecydowała. Poprosi o przeniesienie. Ale nie teraz - chce wpierw się dowiedzieć jak najwięcej od Silurii oraz od Sabiny.
Siluria nie pokaże Wioletcie czego się spodziewa... nie może...

Siluria powiedziała Wioletcie, że to dobry moment, by ta uczyła się "wytrzymałości" w uprzęży. Uprząż działa, a Wioletta musi po sobie nie pokazać w miejscu publicznym... a potem poszła Siluria z Wiolettą na salę treningową. I tym razem Wiolettę wyciorała.

Po południu wróciły. Normalne spotkanie u Wiktora - mag chce wiedzieć, jakie ładne stroje kupiły. Oczywiście, Wioletta założyła stroje "dla Wiktora". Siluria też spodziewając się, że napotka na "grzeczną" Sabinę, kupiła parę drobiazgów. Nie po to, by Sabinę ciemiężyć. Po to, by upewnić Wiktora o swojej lojalności i oddaniu.

I faktycznie, Wiktor poprosił je by zobaczyć jak ładnie ubrały się jego dziewczęta. Czeka tam też Sabina. Gdy dziewczyny przyszły w uprzężach i innym stroju, on przepiął nową uprząż na Silurię. I poprosił dziewczyny, by ładnie ubrały jego nową lalkę. Wioletcie łzy stanęły w oczach - Sabina, jej bohaterka, upadła do jej poziomu. Sabina wyszła i drżącym z ekstazy głosem powiedziała, że zmieniła zdanie i z radością podda się woli Lorda Sowińskiego. Wiktor zaznaczył, że zaraziła się Irytką, więc... zaopiekował się nią. Siluria rozumie - plausible. Very plausible. Ale ona zna prawdę...

Wioletta i Siluria z szacunkiem przebrały Sabinę. Wiktor zaczął ją upokarzać, maksymalnie upokarzać, ale także lekko całować po policzkach i czole. Sabina się wzdrygała, ale Siluria miała wrażenie "fake". It all is staged, it all is faked, it all is wrong. Sabina "tam jest", ale bardzo daleko. Nieobecna. A Wioletta cierpi i jej marzenia i nadzieje na przeniesienie się rozpadły. Wiktora to bawi. Dagmara która to obserwuje jest bardzo skonfliktowana widząc Wiolettę... ale i trochę Sabinę.

Siluria jeszcze dopytała kralotha co się stało. Kraloth powiedział, że Sabinie "spleśniała dusza". Nie było tam magii krwi. Po prostu pleśń poszła po kanałach magicznych; Sabina błagała; tak błagała jak jeszcze nigdy, ale Dagmara ją trzymała i była nieugięta. Aż w końcu Sabina całkowicie wygasła.
Na szczęście - kraloth pocieszył - tą pleśń się leczy. Ona nie jest trwała. Różni magowie ją leczą; kralothy nie. To nie jest organiczne.

Siluria się uśmiechnęła ponuro. Sytuacja się odwróci...

# Zasługi

* mag: Siluria Diakon, która przekonała Wiolettę, by ta zdecydowała się na przeniesienie; też zaobserwowała "spleśnienie duszy Sabiny".
* mag: Sabina Sowińska, która w wyniki skomplikowanej intrygi Wiktora (i Dagmary?) została przekształcona w jego posłuszną niewolnicę.
* mag: Wiktor Sowiński, coraz bardziej uzależniony od Silurii ale jednocześnie coraz bardziej zwyciężający na froncie rządu Szlachtą... i Świecą w Kopalinie.
* mag: Wioletta Bankierz, która podjęła decyzję o przeniesieniu od Wiktora by nie marnować swego potencjału. Widząc co spotkało jej bohaterkę (Sabinę), zmieniła zdanie.
* mag: Dagmara Wyjątek, nosząca krwawy power suit i nie do końca będąca sobą; w pełni ufa jej Wiktor. Przekształciła Sabinę dla Wiktora i ominęła problem zaawansowanego hipernetu.
* mag: Elea Maus, która coraz gorzej radzi sobie z atmosferą budzącą jej instynkty defilera. Przeczuwa coś strasznego i cierpi z braku Karradraela.
* mag: Diana Weiner, w pełni już przekształcona i posłuszna woli Wiktora dzięki Elei, Wiktorowi, Silurii i Laragnarhagowi.
* mag: Fryderyk Chwost, * mag Szlachty zainspirowany Dagmarą; artysta, 47 lat. Sprowadzony przez Sabinę do ucywilizowania Dagmary i rozbicia jej z Wiktorem. Zawiódł ;-).
* vic: Laragnarhag, kraloth w służbie Millennium, tymczasowy partner Silurii i świadek działań Wiktora. Nikt go nie podejrzewa, bo niemoralny kraloth.

# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Centrum
                        1. Kompleks Centralny Srebrnej Świecy
                            1. Kompleks pałacowy
                                1. Skrzydło Sowińskich
                                    1. Pokoje gościnne
                                    1. Buduar Wiktora
                            1. Pion ogólnodostępny
                                1. Galeria handlowa Srebrnej Świecy
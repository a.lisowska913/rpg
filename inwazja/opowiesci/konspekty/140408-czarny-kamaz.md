---
layout: inwazja-konspekt
title:  "Czarny Kamaz"
campaign: anulowane
gm: żółw
players: kić, dzióbek
categories: inwazja, konspekt
---

# {{ page.title }}

## Misja właściwa:

Na zamówienie Zajcewów prosto z Syberii do Polski przyjechał Czarny Kamaz.
Wiózł materiały zamówione przez członków rodu z różnych frakcji, między innymi zamówiony przez Irinę Zajcew "rdzeń życia" pozwalający na modyfikację żywych istot. Artefakt ten nasycone jest energią chaotycznej Syberii.

Niestety, okazało się, że ktoś wykradł ładunek.
Zajcewowie najchętniej załatwiliby sprawę wewnętrznie, ale problemem była Prawdziwa Świeca. Co prawda zginęło sześć magów rodu Zajcew, ale również zaginęły narkotyki bojowe i broń chemiczna. Ze swoich źródeł Ilarion Zajcew, przywódca frakcji supremacji dowiedział się, że część rzeczy, które trafiały poprzez Kamaza do rodu, wypływała z rodu. Innymi słowy, gdzieś tam jest przeciek i niezwykle niebezpieczne rzeczy mogą trafić poza Zajcewów.

Mimo oporu Ilariona, zarówno Benjamin Zajcew jak i Ewa Zajcew stwierdzili, że w zaistniałych okolicznościach należy włączyć w sprawę zaufaną osobę ze Świecy. Irina rekomendowała Agresta, który rekomendował Andreę.

Czarny Kamaz został jedynie wstępnie przesłuchany. 
Andrea oraz Wacław na spotkaniu z Iriną dowiedzieli się, że Kamaz jest inteligentnym artefaktem. Czymś więcej niż tylko pojazdem, a jego siła ognia wystarcza do swobodnego poruszania się po terenie Syberii.
Irina podejrzewała Prawdziwą Świecę, organizację, która próbuje doprowadzić do polaryzacji na linii Zajcewowie - Świeca tak, by mag nie mógł być jednocześnie Zajcewem i magiem Świecy i by musiał wybrać ród lub gildię.

Podczas rozmowy Iriny z zespołem, wypłynęły nowe informacje. Na pace Kamaza znajdował się mag syberyjskiej sekty Zajcewów, o nazwie Zamarznięty Płomień (Zamiorszaja Fłamia) Magowie Fłamii są praktycznie nieumarli. Są bardzo odporni na temperatury niskie i wysokie, nie mają problemu z oddychaniem, są zdolni do hibernacji, oraz sami się skażają. Wierzą, że skażenie ujawnia prawdziwą naturę maga, a najbardziej cenią sobie przetrwanie, lojalność, kolektyw i rodzinę.

Żaden odłam rodu nie przyznaje się do wezwania Fłamii. Jest to dodatkowa zagadka, z którą Andrea i Wacław muszą sobie poradzić.

Mając powyższe informacje, Andrea i Wacław udali się (razem z Agrestem) na miejsce zbrodni.
Tam Andrea poprosiła Agresta, by ocenił teren od strony taktycznej. Sama skupiła się na rzuceniu czarów mających określić, "co tu się działo". Wacław zaczął przesłuchiwać Kamaza.
Kamaz stwierdził, że wiedział o obecności czarodziejki Fłamii imieniem Swietłana, że zmienił cel podróży na wyraźne żądanie kogoś z góry, że tych sześciu magów odebrało ładunek i Swietłanę, on odjechał, oni zostali zaatakowani, on zawrócił, ale kiedy dotarł, było po wszystkim.
Historia ogólnie trzymała się kupy... do chwili, kiedy Agrest podsumował to, co zauważył.
Ciała wskazywały na absolutne zaskoczenie. Ci magowie nawet się nie bronili. Zostali zaskoczeni i nie mieli szans. Ponadto, Agrest zaznaczył, że takie coś jak ten Kamaz z pewnością ma jakieś czujniki, broń artyleryjską... coś, czym może pomóc mordowanym kolegom na Syberii.
A tu wyglądało na to, że nic takiego nie zostało użyte.
Zapytany bezpośrednio, Kamaz zasłonił się Maskaradą.
Sprawy jednak skomplikowały się, kiedy Andrea skonfrontowała wyniki swoich zaklęć z zeznaniami Kamaza i wnioskami Agresta. Ktoś tu łgał... i to łgał jak Kamaz.
Andrea wykryła działanie bardzo potężnego zaklęcia rozpraszającego magię, połączonego z potężnym czarem oszałamiającym. I tak się składa, że na polanie był ślad z mniejszym poziomem skażenia. Akurat w kształcie Kamaza. Nadal nie wiadomo, kto mógł rzucić zaklęcie takiej mocy, ale wiadomo, co ich zdjęło z akcji.
Tylko dlaczego Kamaz łże, że go tu nie było...? I o czym jeszcze kłamie?

Wacław zaraportował Irinie, że Kamaz łże. Irina sprowadziła wszystkich trzech przywódców frakcji, dzięki czemu zagadka wreszcie się rozwiązała. Kamaz powiedział, że zmienił trasę na żądanie Ilariona i że było dziesięć osób, z czego cztery ukryte.
Ta szóstka magów odbierających niebezpieczny towar nie kryła się z tym, że chcą zaatakować ród Zajcewów przy jego pomocy, wykorzytując to do rozgrywek politycznych frakcji supremacji. Wtedy ujawniła się czarodziejka Fłamii, kapłanka Fłamii, i stwierdziła, że nie mogą tak zrobić, że jest to zło w najczystszej postaci. Oni nie mieli wyboru - w tym świetle musieli pozbyć się świadków. Kamaz musiał ochronić Swietłanę. Uderzył w siebie wszystkimi zaklęciami. Dla niego to żadne ryzyko. Cała szóstka zginęła, a Swietłana zabrała ze sobą czterech pozostałych magów, pod działami Kamaza dała im skosztować trochę swojej krwi (tymczasowe ulojalnienie), po czym oddalili się. Ładunek nadal jest w Kamazie, ukryty na pace, poza rdzeniem życia, który nie może przebywać w Kamazie przez dłuższy czas.
Na pytanie, czemu Kamaz nie powiedział tego wcześniej odpowiedź była prosta: Kamaz ma na uwadze przede wszystkim dobro rodu i nie zna się na polityce. Nie wiedział, kto ma jaką rolę i zwyczajnie chciał ukryć wszystko, zanim pojawią się przywódcy wszystkich frakcji. A Swietłanie ufa.

Ilarion dodał, że on specjalnie zastawił pułapkę na tą szóstkę. Miał nadzieję, że to oni stali za przeciekami. Miał też nadzieję, że uda mu się zdobyć informację o dodatkowych napastnikach. Niestety, działania Swietłany i Kamaza uniemożliwiły Ilarionowi dojście do prawdy.
No trudno, ostatnie, co zostało, to odnaleźć Swietłanę i zaginionych Zajcewów.
Wielkodusznie, wszyscy zostawili to zespołowi. Wacław został przez trzech przywódców uprawniony do uzyskiwania prawdziwych odpowiedzi od Kamaza.

Mając wszystkie te informacje i pomoc Kamaza, Andrea była w stanie skonstruować zaklęcie namierzające aktualną lokalizację Swietłany. Na szczęście dla Andrei, zaklęcie to przeszło niezauważone.
Gdy już Andrea i Wacław byli w drodze do kryjówki Swietłany, dostali dodatkową informację. Irina powiedziała im, że wie, kto wezwał Swietłanę. Jej córka, Tatiana. Ta informacja wystarczyła, by Wacław wiedział, jak i o czym rozmawiać ze Swietłaną.

Swietłana okazała się być całkiem rozsądną kapłanką. 
Gdy przedstawiono jej wszystkie informacje, sama "poddała" swoich Zajcewów i zgodziła się na gościnę oferowaną jej przez Irinę...

<img src="140408_CzarnyKamaz1.jpg"/>

# Zasługi

* mag: Wacław Zajcew jako zaufany Iriny Zajcew.
* mag: Andrea Wilgacz jako wstrzyknięta przez Mariana Agresta obserwatorka świata Zajcewów mająca raportować działania "Prawdziwej Świecy".
* mag: Irina Zajcew jako wysoka terminuska Zajcewów i żona Benjamina która ufa Marianowi Agrestowi.
* mag: Tatiana Zajcew jako "pen-pal" Swietłany i ta, która wezwała kapłankę Fłamii do Polski.
* mag: Benjamin Zajcew jako przywódca frakcji Współpracy który uważa, że pomoc Świecy jest nieoceniona.
* mag: Ilarion Zajcew jako przywódca frakcji Supremacji który nie chce mieszać nikogo w sprawy Zajcewów.
* vic: Czarny Kamaz 314 jako pierwszy w historii Kamaz który łże jak Mistrz Gry.
* mag: Swietłana Zajcew jako kapłanka Zamiorszajej Fłamii która przybyła w odpowiedzi na prośbę Tatiany.

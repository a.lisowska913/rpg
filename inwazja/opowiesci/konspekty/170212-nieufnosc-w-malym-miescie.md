---
layout: inwazja-konspekt
title:  "Nieufność w małym mieście"
campaign: prawdziwa-natura-draceny
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [161009 - "Paulino, zmieniłaś się..." (PT)](161009-paulino-zmienilas-sie.html)

### Chronologiczna

* [170103 - Wojna bogów w Czeliminie (AW)](170103-wojna-bogow-w-czeliminie.html)

## Kontekst ogólny sytuacji
## Punkt zerowy:
## Misja właściwa:

Dzień 1:

Paulina zadzwoniła do Kajetana Weinera w sprawie Stawni.

* Kajetan. Wszystko w porządku, Paulino? - Kajetan, lekko zmartwiony
* Tak i nie... a u Ciebie wszystko ok? - Paulina
* Gdzie mniej więcej jesteś, fizycznie? - Kajetan
* W Lubelskim? - Paulina
* Natknęłaś się już na Spustoszonych agentów?
* Nie...? Co się dzieje? - zaskoczona Paulina
* Świeca jest atakowana. Technicznie, jest w ruinie. - Kajetan, spokojnie
* Okej... - Paulina, w szoku - Spytam inaczej: jak bardzo jesteś zajęty? Przydałaby mi się...
* Pomogę Ci. Niewiele mogę poradzić na to, co dzieje się tutaj. A mogę osłonić wschód z Tobą. - Kajetan
* Wiesz, że ja na Spustoszenie dużo nie poradzę?
* Spustoszenie jest naszym najmniejszym problemem, overmind jest większym. - Kajetan
* Mam pewien kłopot z dawnymi członkami Twojego rodu. Więcej szczegółów na miejscu... nie dzieje się nic krytycznego. - Paulina
* JAKIEGO TYPU kłopotów? To ważne. - Kajetan, bardzo ostro
* Stracili moc po Zaćmieniu, był wyciek syberionu i wraca im pamięć.
* Dobrze, to nie to co myślałem. Będę tam wieczorem. Gdzie?
* Stawnia.
* Stawnia?! - Kajetan, cały zaskoczony
* Coś wiesz, przyjeżdżaj; pogadamy. - Paulina, ucieszona.

Kajetan obiecał, że się pojawi. Będzie z przyjaciółką. Paulina powiedziała mu, że jest z nią Diakonka w trakcie rekonwalescencji po ciężkim zaburzeniu Wzoru. Kajetan ją ostrzegł przed Stawnią. Nie jest ze Świecy? Nie. Dobrze. Kajetan zapytał jeszcze Paulinę, czy ta ma możliwość połączenia z hipernetem? Nie. Dobrze. Ma się NIE łączyć.

Paulina wysłała Dracenę na obchód Węzłów; niech ta sprawdzi, co się dzieje. Celem Pauliny będzie podreperowanie relacji Maria - Paulina... Paulina po prostu chce pójść z Marią na kawę, porozmawiać... po prostu... spędzić z nią trochę czasu. Tymczasem Dracenę napadł niktor; ta dała radę go zniszczyć, acz złamała Maskaradę wobec Janusza Krzykoła i by zmitygować, Uwiodła go mocą Wiły. Niestabilnie; Krzykoł jest beznadziejnie zakochany. Paulina - tymczasem - przekonała Marię. Maria jest udobruchana; acz zauważył ich też razem Tomasz Weiner.

Telefon od Draceny. Ta cała roztrzęsiona. Powiedziała o starciu z niktorem; powiedziała, że rozerwała tą efemerydę. Ma częściową amnezję, niewiele pamięta. I Przekształciła jednego z ludzi; gość był... za blisko, gdy Dracena weszła w pełną formę bojową. Maria powiedziała, że ROZUMIE, że Dracena jest chora. Naprawdę chce to rozumieć... przynajmniej coś, z perspektywy Pauliny.

Paulina przeszła przez apteczkę i poszła do Draceny asap. Ta skryła się w domku z obserwatorium, wraz z nieszczęsną ofiarą Draceny. Paulina weszła do tej chatki... i otworzył jej Spustoszony człowiek. Paulina odskoczyła, przewróciła się i potłukła solidnie.

"PEŁNA moc Draceny"

Dracena nie odpowiada. Spustoszony zaprowadził Paulinę do Draceny... w łóżku. Ta jest... dziwna. Siedzi nago w pościeli i mówi w powietrze, kłócąc się z jakimś głosem, który tylko ona słyszy... Dracena zaprosiła do łóżka Paulinę, przytulając itp. Paulina dała się przytulić, acz nie pójdzie do łóżka. Dracena jej nie poznaje; dopiero powiedzenie przez Paulinę swego imienia sprawiło, że Dracena przypomniała sobie kilka rzeczy. A odpytany Spustoszony wyjaśnił Paulinie co widział (co się stało). Przynajmniej niktor został zniszczony.

Didi dalej słyszy głos Karradraela. Jest to... dziwny głos. Mówi o Krwi i o Obsesji, używa słów, których nie rozumiem, starych słów. Tysiące słów, tysiące myśli na moją jedną główkę... nie ma tam złej woli, nie ma tam żądania; nie słyszy Didi. Po prostu szepcze, szepcze o swojej Obsesji... Ku zaskoczeniu Pauliny Dracena jest bardzo, bardzo pobudzona fizycznie...

Szczęśliwie, Wzór Draceny się nie zepsuł. Ma tylko zaniki pamięci, które jej mechaniczna część próbuje odbudowywać. I energia erotyczna by Dracenie zdecydowanie pomogła. Paulina się jedynie upewniła, że do Janusza nikt nie przychodzi i delikatnie przeciągnęła palcem po skórze Draceny. Reszła przyszła sama ;-).

Wieczorem, Paulina się obudziła. To było przyjemne...

"Nic nie patrzyłam. Bardzo nie patrzyłam. Odcinałam co się dało..." - chmurna Maria. Paulina jest ODROBINĘ zawstydzona. Dracena przygotowała Paulinie eliksir detoksyfikujący na wypadek uzależnienia... Dracena spróbowała jeszcze rozmontować Spustoszenie; zdążyli. Ale do wieczora...

Wieczorem przybyli Kajetan i Lidia. Dracena profilaktycznie została w pokoju... a Paulina załatwiła i Kajetanowi i Lidii pokoje u Jesiotra, jak same mają.

* Pozwól, że przedstawię Ci moją koleżankę, neuronautkę i ekspertki od hipernetu - Kajetan do Pauliny
* Nie masz pojęcia jak źle tam teraz się dzieje... - Lidia do Pauliny
* Nawet nie chcę sobie wyobrażać - Paulina, nie mając POJĘCIA o wojnie z Karradraelem - Załatwiłam dla Was miejsce do zamieszkania
* Dzięki... właśnie, CO jest z tym przekaźnikiem hipernetowym? - zaskoczona Lidia
* Jakim przekaźnikiem? - Paulina
* Jest uszkodzony - Lidia. Widząc niezrozumienie Pauliny, dodała - Jestem połączona z hipernetem. To zmodyfikowany typ... i uszkodzony.
* NIe rozumiem implikacji tego co mówisz - Paulina
* Ja też nie... - Lidia

Gdy już się ulokowali w knajpce, nikt nie podsłuchuje, Paulina opowiedziała im o tym, co się dowiedziała. Wyciek syberionu, dwie zniszczone efemerydy... a gdzieś pod jeziorem jest vault. Aha, i jest dużo ludzi bez magii rodu Weiner co zaczynają sobie przypominać. I Halina. I w ogóle...

Kajetan powiedział Paulinie zupełnie inną opowieść. Czarodziejka kochająca opowieści, rodu Weiner. Czarodziejka Świecy. Worldmaker, w hipernecie. Uploadowana do hipernetu, do kolonii Rasputin. Kolonii Rasputin znajdującej się w Stawni. A potem przyszło Zaćmienie... i Zakłócenie Demonów Hipernetowych. Większość demonów hipernetowych oszalała po zmianie form magicznych. Kolonia Rasputin miała kontakt z... Primusem przez to, że miała drony, technomantyczne byty, zombie... Ale ostatni raport Ksenii Zajcew do Świecy (to wyciągnęła Lidia) świadczy, że Rasputin straciła kontrolę nad swoimi eksperymentami... albo demon je uwolnił.
 
Rasputin miała uploady "swoich" Weinerów. Więc być może próbuje ich "wgrać" z powrotem.

Wspólnymi siłami ustalili, że najpewniej muszą rozwiązać problem kolonii Rasputin. Paulina poprosiła Lidię, by ta odeszła na bok. Lidia wzruszyła ramionami i się oddaliła. A Paulina zaczęła mówić z Kajetanem...

* Sama nie do końca rozumiem polityki, która za tym stoi, ale Dracena... już miała niestabilny Wzór. Do jej otwartego wzoru zostały włączone dwa elementy - Wiła i Spustoszenie. - Paulina, ostrożnie
* Spustoszenie... - Kajetan, pobladły - ona słyszy głos Karradraela.
* Ok... - Paulina - Tak, mówiła, że słyszy głos, ale...
* Na pewno tak powie. - Kajetan, zimno.
* Uwierz, gdyby nie było to prawdą, to nie byłoby tej rozmowy - Paulina. 
* Ozydiusz nie żyje. Wydział wewnętrzny zdruzgotany. Kopalin w rękach Karradraela. - Kajetan, punktując - Karradrael CZEKA.
* Na co? - Blada jak śmierć Paulina
* Nie wiem. Krwawa Kuźnia przetwarza dziesiątki ludzi DZIENNIE. - Kajetan, dalej bezwzględnie - Karradrael wykorzysta Twoją przyjaciółkę
* Jeśli o niej wie - Paulina
* Wie. - Kajetan
* Wszystko wskazuje, że nie. Posłuchaj... nie rozwiążemy tego. Sprawdź mnie pod kątem Spustoszenia...  - Paulina
* Bardziej się martwię, że robi ludzkich agentów za Twoimi plecami, nie wiedząc o tym - Kajetan - To nie ten Karradrael, którego znasz. Ten jest GORSZY. Groźniejszy.
* Posłuchaj... jakkolwiek sytuacja wygląda, wolałam powiedzieć Ci to od razu - Paulina, błagalnie - Dziewczyna przeszła naprawdę ciężką bitwę o siebie. Kinga Toczek ją składała do kupy. Kinga uznała, że jest w porządku. Wciąż jeszcze ma momenty niestabilności...
* Nie wiedziała o Karradraelu. On tak działa, przez 'momenty niestabilności' - Kajetan, uważnie
* Rozumiem Twoje obawy... - Paulina, podłamana - Niemniej... Cóż. Jest moją przyjaciółką a ja próbuję jej pomóc, tak jak tej mieścinie...
* Nie chcę robić jej krzywdy. Ale chcę ją unieszkodliwić. - Kajetan, zamyślony - Skutecznie ubezwłasnowolnić
* To nie przejdzie. Mnóstwo wysiłku kosztowało mnie sprawienie, by przestała się bać, naprawienie jej. - Paulina - A ubezwłasnowolnienie to atak, zwłaszcza przy jej naturze.
* Załóż, że mam rację, że jest agentką Karradraela która zadziała w dowolnym momencie. Co mam zrobić? - Kajetan, prosto
* Jesteś ostrzeżony wcześniej... ale nie po to wyciągałam ją z tego wszystkiego, by to zniweczyć! - Paulina, zdesperowana - Wezmę to ryzyko na siebie.
* Nie, wzięłaś to ryzyko na wszystkich - spokojnie, Kajetan
* Jeśli uznasz, że ryzyko jest za duże, nie obrażę się, jeśli wyjedziesz - Paulina
* W wypadku tego ryzyka nasza przyjaźń jest mniej istotna niż powstrzymanie Spustoszenia... - Kajetan, wzdychając - Jeśli się pomylę, mogę być winny śmierci 10000 ludzi; wobec ubezwłasnowolnienia jednej czarodziejki.
* Jesteś skłonny zaufać opinii Kingi Toczek i mojej? - Paulina
* Nie pracowałyście z agentami i ofiarami Karradraela. - Kajetan

Konflikt. 4v4. F - w sprawie vaulta Dracena jest wyłączona. Nie ma możliwości nic z tym robić. I Kajetan będzie ją monitorował, ale nic nie zrobi bez powiedzenia Paulinie. I nie wolno Dracenie powiedzieć, co się dzieje... ani Lidii.

Kajetan i Paulina doszli do porozumienia też w kolejnej kwestii: Lidia musi przebadać Halinę Weiner. Nie jako lekarz; jako neuronautka. Musi sprawdzić, czy Halina jest uploadowana czy przypomina sobie rzeczy dzięki Pryzmatowi. Kajetan naprawdę martwi się tym, że być może kolonia Rasputin oszalała; że tutejszy demon jest szalony. No i Paulina wyjaśniła jeszcze Kajetanowi temat Haliny / Draceny.

* Innymi słowy, wzięłaś niestabilną "wiłę" i napuściłaś ją na niestabilną... viciniuskę? - Kajetan, zaskoczony
* Mogłam doprowadzić do tego, by uciekła, przestraszyła się, lub... a miałam na głowie skażone syberionem miasto - Paulina, spokojnie
* Wolna wola. Chyba, że dotyczy tylko Twojej przyjaciółki? - Kajetan, naprawdę zaskoczony decyzjami Pauliny
* Nie, nie dotyczy to tylko jej. Dlatego chcę dać Halinie wybór. - Paulina
* Paulino, to co Twoja przyjaciółka zrobiła... to gwałt. To kontrola umysłu i ciała. To jak sekta. - Kajetan, wyjaśniający bardzo spokojnie
* I to odwróci... - Paulina, bardzo spokojnie. - Nie wszystko rozumiem, nie o wszystkim mam pełną wiedzę, ale w Dracenie nie ma złej woli. I będzie szczęśliwa mogąc to zakończyć.
* Podejmujesz inne decyzje niż kiedyś - Kajetan, nie oskarżycielsko
* Słyszałam już, że się zmieniłam. Cóż. Wciąż staram się pomagać ludziom i magom. - Paulina, ze smutkiem - Mogłam nie zrobić nic.
* Jak to wszystko wyjaśnić Lidii? Ona ma uraz do Diakonów po spotkaniu Rafaela. - Kajetan
* No tak... nic dziwnego - Paulina przypomniała sobie Rafaela - może... samotnością? Na początku szczerze wierzyłam, że mogę sprawić, by Halina żyła normalnym ludzkim życiem...
* Jak wyjaśnić Dracenę i jej relację z Haliną? - Kajetan, doprecyzowując - Lidia nie ma tendencji do kompromisów.
* Halina była samotna. Dla Draceny samotność jest jak śmierć; powolna i okrutna - Paulina, brutalnie - Instynkt. Nie było wyjścia.
* Zadziała. - Kajetan - Naprawdę mam nadzieję, że to nie jest kolejna gra Karradraela...
* Uwierz, ja też - Paulina

Dobrze; Paulina zdecydowała się więc zaprowadzić Kajetana i Lidię do Haliny. Halina nadal ma swoje ukochane drzewo, a dokładniej dziuplę... Halina czeka już na Paulinę i Dracenę; zdziwiła się widząc Kajetana i Lidię. Nie te osoby. Paulina wszystko wyjaśniła; Lidia i Kajetan są tutaj by pomóc Halinie. Lidia powiedziała Halinie, że spróbują ją zbadać i dodać jej energii magicznej; najpewniej się nie uda, ale i tak spróbują pomóc. Halina poprosiła, by Lidia nie zmieniała jej w człowieka. Lidia powiedziała, że nic wbrew Halinie.

Lidia i Paulina zabrały się do badań nad Haliną. Kajetan ma pilnować i asekurować. W tym czasie Paulina dała Dracenie zadanie - niech ta zrobi jakieś drony z dostępnych materiałów...

Dzień 2:

Lidia i Paulina odkryły, że Halina jest uploadowana z jakiegoś źródła. Sygnał jest słaby, bardzo słaby, ale oddaje jej pamięć. Można ostrożnie wysunąć hipotezę, że nie tylko jej. Wniosek prosty - żeby rozwiązać problem, trzeba wyłączyć kolonię Rasputin.

* I co teraz? Powiesz jej wszystko? Nic? Oszukasz ją? By Ci pomogła? - Paulina do Kajetana o Halinie
* Jestem terminusem. Ochronię ludzi. - Kajetan, prosto
* Co to znaczy 'ochronić'? Czy Halina jest człowiekiem? Nie masz tu dobrej opcji - Paulina
* Zobaczę, jak się sytuacja rozwinie, ale Maskarada musi być zachowana - Kajetan
* Ale nie powiesz, że to oczywisty wybór - Paulina
* Nie. Ale jestem terminusem. Nie mam wyboru tak naprawdę - Kajetan

Szybki komunikat od Marii do Pauliny. Ktoś dopytywał się o Dracenę w kontekście nieletnich. Maria próbuje naprawić sprawę. Paulina się poważnie zmartwiła - Kajetan i Lidia się NIE mogą o tym dowiedzieć. Maria powiedziała, że jakiś lekarz rozpytywał, w związku z problemami jakie wyrządziła Dracena w tych okolicach... a Kajetan nawet jeszcze nie wie o Spustoszonym człowieku (który został odSpustoszony, szczęśliwie).

Kajetan powiedział Paulinie, że ma zamiar rozlokować w okolicy detektory Spustoszenia. Paulina powiedziała Kajetanowi, że jest jeszcze jedna rzecz - miejscowy lekarz nie lubi Pauliny. Kajetan się zdziwił; Paulina martwi się tym, że Rasputin może zareagować na działania Kurta. Kajetan powiedział, że będzie monitorował sytuację. No i kwestia ostatniej efemerydy - Syreny. Najsilniejsza z nich wszystkich, bo pozostałe zostały zniszczone. Kajetan powiedział, że on się tym zajmie.

Lidia dostała zadanie badać innych Weinerów. Kajetan rozlokuje detektory Spustoszenia...

Przeleciała nad nimi drona. Wykryła Kajetana i Paulinę, zrobiła kółeczko, po czym poleciała dalej.

* Co to było? - Kajetan, zaskoczony
* To Draceny. - Paulina
* Drony? Monitoring terenu? Wiesz, że Karolina Maus używała właśnie dron do rozprzestrzeniania Spustoszenia? - Kajetan, ostro
* Nie, nie wiedziałam - Paulina
* Niech ona je wszystkie wyłączy - Kajetan
* Mogę ją o to poprosić, ale nie obiecam - Paulina
* Dobrze - ciężkie westchnięcie Kajetana - Po prostu rozlokuję WIĘCEJ detektorów... i niech te drony nie pokażą się Lidii.

Paulina, podłamana, poszła porozmawiać z Draceną. Kajetan nie jest wrogi Dracenie, ale jest bardzo, BARDZO ostrożny.

* Paulinko - Dracena, radośnie
* To było Twoje fruwadło? - Paulina, uśmiechnięta
* Tak, mam nawet mechaniczne ryby - Dracena
* Ci Weinerowie chcą rozwiązać problem, ale Kajetan miał ciężkie przejścia ze Spustoszeniem. A ta druga czarodziejka zareaguje jeszcze gorzej. Więc mogłabyś zrobić coś dla mnie? Trzymaj drony, ryby i inne takie z dala od tej czarodziejki. - Paulina
* Już skanuję jezioro i inne obszary - Dracena, nachmurzona
* I bardzo dobrze - Paulina, szybko - Po prostu... nie pokazuj im nic o Spustoszeniu
* Szkoda, że wezwałyśmy wsparcie - Dracena, posmutniała - Po prostu... 
* Może szkoda, ale... dzięki temu mamy szansę pomóc tym wszystkim ludziom - Paulina - I dawnym magom.
* Dobrze. To jak mogę Ci pomóc? - Dracena, z lekkim entuzjazmem - Wchodzimy do jeziora?
* A czego się dowiedziałaś na razie? - Paulina, ostrożnie
* W jeziorze jest syberion. I jakieś technomantyczne przekaźniki; uwięził się - Dracena - Powoduje zakłócenia... czegoś
* Nawet wiem, czego - Paulina - Tam jest schowek Weinerowski
* Dla mnie wygląda jak coś... innego. - Dracena, pokazując palcem na drukarkę. Paulina dostała wydruk dziwnego urządzenia, częściowo zatopionego w skale. Było schowane za skałą, ale ta pękła

Paulina stwierdziła, że pokaże to Weinerom. Dracena pisnęła. Coś przejęło kontrolę nad jej "rybą"; płynie na kolizyjne z tym dziwnym urządzeniem. Dracena powiedziała bezradnie, że ryba się... włączyła do urządzenia. Zamyka obwód. Coś się naprawia tą rybą...

Dracena wycofała pozostałe ryby i robi dla Pauliny mapy tego jeziora. A sama Paulina idzie szybko do Kajetana... gdziekolwiek Kajetan nie jest. Zadzwoniła do niego; Kajetan jest w mieście, układa ekrany detekcyjne.

* Dracena zbadała po części jezioro. Dronami, ale pływającymi - Paulina. 
* Świetnie... - Kajetan westchnął - Nic złego się nie stało?
* No właśnie chyba trochę tak. Znalazła takie coś... - Paulina pokazała wydruk - ...ale jedna z jej dron domknęła obwód po przejęciu. Rasputin działa.
* Ten obwód... to urządzenie to emiter hipernetowy. Kontroler hipernetowy. - Kajetan, kręcąc głową - Rasputin była odcięta. Nie jest już. Może kontrolować... cokolwiek tam ma.
* Tia... - Paulina, kwaśno - A co ma?
* Może mieć drony, broń balistyczną, ładunki wybuchowe... - Kajetan zaczął wymieniać
* A jak jest realistyczne, że użyje tego przeciwko nam? - Paulina
* Jeśli Rasputin oszalała, a to pasuje do relacji Ksenii... - Kajetan zaczął przygotowywać tarcze
* Jest tam syberion; zakłóca sygnał - Paulina, szybko
* Kupuje nam to czas. Niewiele czasu. - Kajetan. - Musimy tam iść. Teraz.
* Lidia? Nie przyda się? - Paulina
* Miałem na myśli, Lidia i ja. - Kajetan, cierpko
* Przydam Wam się? - Paulina
* Tak. Jesteś lekarzem. Możesz... możesz być potrzebna. - Kajetan
* Dracena jest technomantką - Paulina
* Nie wezmę potencjalnej agentki Karradraela. - Kajetan
* Nie będę Cię zmuszać - Paulina, ciężko wzdychając

Paulina poszła do Draceny. Dracena się NIE zgodziła, by Paulina tam poszła. To niebezpieczne. Paulina powiedziała, że Dracena zostanie jako odwody. Ta się nie zgodziła. Paulina powiedziała, że komu może zaufać jako defensywa? Dracena zauważyła, że jest JEDYNĄ technomantką. Bez niej Paulina nie ma szans. Cały zespół bez niej nie ma szans...

Notatki MG:

* Laetitia gra na czas, chce naprawić Fabryki, zregenerować Źródło Energii i uruchomić Blue Goo
* Laetitia myśli, że po stronie Zespołu są Zajcewowie z syberionem
* Laetitia wie, że dostała uruchomienie z Kompleksu Centralnego; jest źle i jest nieufna

Powrót do misji:

Paulina powiedziała Dracenie, że jeśli ta chce z nią iść, nie może używać Spustoszenia. Dracena się skwapliwie zgodziła. Obiecała, że ściągnie 4 aparaty do nurkowania.

Powrót Pauliny do Kajetana. Powiedziała mu prawdę:

* Dracena jest biologicznie zależna od obecności Pauliny
* Dracena wykona każdy rozkaz Pauliny
* Paulina tego nie chce wykorzystać

Kajetan powiedział Paulinie, że mają KOLEJNY problem. Pojawiła się jeszcze jedna czarodziejka rodu Weiner; skontaktowała się hipernetem z Lidią. Próbuje naprawić / dostać się do kolonii Rasputin. Nie ufa Paulinie, Kajetanowi etc. ponieważ... boi się Spustoszenia. Ale Kajetan nie ufa tamtej czarodziejce - dlaczego tylko z Lidią, tylko hipernetem itp. Tamta czarodziejka - Laetitia - podziękowała za naprawienie ogniwa i pozbycie się syberionowego Skażenia. Paulina zauważyła, że nie pozbyły się Skażenia...

Kajetan powiedział, że w sprawie Draceny - niech Paulina wyda Dracenie jeden rozkaz. Rozkaz, by ta nie mogła obrócić się przeciwko nim. To mu wystarczy... choć i tak mówi to z trudem. Paulina się zgodziła. Zrobi to... zintegruje w końcu zespół. Przy czym - to obejmuje tylko Kajetana i Lidię. Nie jakieś nieznane czarodziejki Weinerów. Kajetan się zgodził. Paulina jeszcze zgodziła się, że Kajetan i Lidia mogą zaatakować Dracenę, ale nie Paulinę... choć niechętnie. Bardzo niechętnie.

Znowu do Draceny. 

Paulina powiedziała Dracenie, że nie ma wyjścia. Została zmuszona, by wydać jej taki rozkaz. Dracena zauważyła, że Kajetan może ją zaatakować a ona nie może się bronić. Paulina wyjaśniła intencje Kajetana - jeśli Dracena jest potworem, on jako terminus ma prawo ją wyłączyć. Dracena... chce wyjechać. Z Pauliną. Nie chce tu dłużej być. Nie tak.

Paulina powiedziała, że chyba nie może wyjechać... być może ta czarodziejka Świecy to _nie_ czarodziejka. Być może to KOLONIA. Dracena jest zaskoczona. ISTNIEJĄ hipernetowe AI? To może być AI? Paulina powiedziała, że zgodnie z tym co powiedział Kajetan... może. A Dracena jest jedyną technomantką...

* Jeżeli jestem JEDYNĄ technomantką, potrzebują mnie. Paulino, nie muszą traktować mnie jak... jak NIEWOLNIKA. - Dracena
* Też nie..? - Paulina, nie rozumiejąc
* Chcą mnie zamknąć w pudełku. Chcą... chcą mnie kontrolować - Dracena, zapalając się - Chcą użyć CIEBIE do kontroli MNIE!
* Didi... dlaczego... o co chodzi? - Paulina
* Wpierw... nie kontroluję życia. Potem nie kontroluję Wzoru. Potem zanika moja kontrola i osobowość a teraz... krok po kroku zanikam. Nie jestem osobą. Jestem zabawką. - Dracena

Podczas rozmowy, Dracena powiedziała Paulinie, że to logiczna kontynuacja. Paulina "nie ma wyboru". Ona to zrobi, bo zależy jej na tym bezpieczeństwie... więc Paulina też jest zniewolona przez SYSTEM. Dracena jej nie wini, ale... tak. Niewola. Obie są niewolnicami. Dracena bardziej...

Dracena nie chce ich skrzywdzić. Ale boi się, że oni skrzywdzą ją. Paulina zdecydowała się ją przekonać... nawet, jak to zajmie całą noc. Paulina ostrzegła Kajetana o potencjale "czarodziejki" Świecy i zdecydowała się przekonać Dracenę, by ta pozwoliła sobie wydać rozkaz.

Dracena pozwoliła sobie wydać rozkaz. Niechętnie, broniła się, ale się zgodziła.

Dzień 3:

Następnego dnia Dracena ma już sprzęt do nurkowania. Paulina wzięła sprzęt medyczny i... spotkanie z Kajetanem i Lidią. Paulina wszystkich przedstawiła krzyżowo, Dracena zachowuje się elegancko, choć chłodno. Toteż Lidia i Kajetan. Lidia zreferowała, co dowiedziała się z Kajetanem:

* Mogą mieć do czynienia z czarodziejką... lub nie. Tak czy inaczej, próbuje ich przekonać, że jest czarodziejką
* Przedstawia się jako Leokadia Weiner; ona istnieje w rekordach Świecy. Tu Lidia jeszcze zauważyła, że OCZYWIŚCIE że istnieje, bo używają tutejszego hipernetu.
* Kolonia Rasputin to była kolonia hipernetowa Świecy. Z systemami defensywnymi, blue/black goo i dronami. Oraz miała fabrykatory.
* Lidia powiedziała, że "Leokadia" jest podobno autoryzowana przez Lady Terminus Andreę Wilgacz. To najbardziej zdziwiło Lidię - skąd ten pomysł. Kajetan uznał, że to bez sensu.
* Tak czy inaczej, autoryzacja lub jej brak ze strony Lady Terminus to już problem polityczny; Paulina zasugerowała Lidii zapytanie Andrei.

Lidia może skontaktować się z Andreą; nie chce tego robić, bo rozstały się w... mniej pozytywnych relacjach. Kajetan zauważa, że to ważne. Paulina mówi, że to politycznie korzystne i ważne dla tego terenu. KONFLIKT: (4v2->S); Lidia niechętnie, ale skontaktuje się z Andreą...

Godzinę później Lidia udzieliła odpowiedzi:

* Andrea nie autoryzowała "Leokadii Weiner"
* Leokadia była uploadowana do Rasputin; przybrała imię Laetitia Gaia Rasputin Weiner.

Kajetan westchnął. Tego się obawiał - mają przeciwko sobie sprawną (jakoś sprawną) Kolonię Hipernetową. I nie wiadomo, gdzie jest Rdzeń. Ale mogą spróbować go znaleźć... używając Lidii i jej wiedzy o przekaźnikach hipernetowych. A Lidia potrzebuje bezpośredniego połączenia. Na szczęście, Dracena ma sprzęt do nurkowania.

Dracena zaproponowała Lidii skonstruowanie komunikatora hipernetowego; taki projektor. Coś, by Laetitia mogła rozmawiać nie tylko z Lidią, ale i min. z Kajetanem. To umożliwi większe linie negocjacyjne. Paulina zauważyła, że Laetitia ma ograniczenie czasowe; jeśli kolonia nie działa poprawnie, to w tym momencie ograniczenie jest większe. Kajetan powiedział, że Laetitia ma nie więcej niż miesiąc.

Paulina chce poznać motywację Laetitii. Wszyscy się z nią zgadzają. Kajetan powiedział, że Zaćmienie Zakłóciło wiele Kolonii. Dlatego Kolonie Hipernetowe w Kompleksach Centralnych zostały w większości powyłączane - Kompleksy obracały się przeciwko magom. Aha, pryzmatyczna opresja się zmniejszyła; Kolonia zniszczyła ostatnią efemerydę, Syrenę.

Na razie Kajetan zakłada BARDZO złe intencje Kolonii Rasputin wobec znajdujących się w okolicy magów; zauważył też, że Kolonia ma system defensywny. Acz jest w ruinach.

Paulina zauważyła coś ciekawego. Może zamiast niszczyć Kolonię Rasputin... można ją naprawić? Dracena jest entuzjastycznie nastawiona do tego pomysłu. Kajetan... się waha.

* Lidia jest neuronautką, psychologiem i ekspertem od hipernetu
* Paulina jest ekspertem od naprawiania Wzoru
* Dracena jest technomantką

Kajetan... nie jest przekonany. Nigdy tego jeszcze nie zrobiono. Lidia zauważyła, że nie próbowano. Kajetan zgodził się z rozumowaniem Lidii... ale do tego celu potrzebna jest diagnostyka Kolonii. Czyli narzędzie do komunikacji jest potrzebne. Dracena i Lidia się tym zajmą. Kajetan wyglądał, jakby chciał zaprotestować, ale ugryzł się w język. W końcu kiedyś trzeba zacząć współpracę.

Parę godzin później prosty projektor był zbudowany. Lidia podłączyła go do hipernetu i wywołała Kolonię Rasputin.

* Leokadia Weiner - Laetitia, wyglądająca jak czarodziejka
* Chcę porozmawiać z Laetitią Gaią Rasputin Weiner, Overlordem Hipernetowej Kolonii - Lidia, przechodząc do konkretów
* Laetitia Gaia Rasputin Weiner, do Twojej dyspozycji - Laetitia, zdejmując maskowanie
* To... było prostsze niż myślałam - zdziwiona Lidia
* [uśmiech] - Laetitia

Laetitia powiedziała, że Andrea autoryzowała wszystkie Overlordy w Kompleksach Centralnych do uruchomienia się i rozpoczęcia działań korekcyjnych. Powiedziała też, że niedaleko jest potężny defiler i ona rozwiązuje ten problem, wysyłając go do poszukiwań na lewo i prawo. Powiedziała, że "byłych magów" ma w stworzonym świecie wirtualnym i próbuje ich re-uploadować do ciał; gdy doszło do niej, że oni nie są już magami, zdecydowałać się przerwać operację.

Laetitia powiedziała tak samo, że była silnie Zakłócona, ale autorepair ją z tego wyciągnął. Niestety, jak zaczęła odbudowywać Kolonię, Zajcewowie wszystko zepsuli.

Lidia wykryła psychiczne uszkodzenia w Laetitii - zakłócenia większe i mniejsze. Ale nic terminalnego; wygląda na to, że Overlord kolonii Rasputin jest możliwa do naprawienia. Andrea autoryzowała Lidię jako regentkę Świecy, co sprawiło, że Laetitia pozwoli jej na więcej...

I teraz tylko zostaje opracować, jak Dracena, Kajetan, Paulina i Lidia mogą wkraść się w łaski Laetitii by ją naprawić...

# Progresja

* Lidia Weiner: autoryzowana przez Andreę, została regentkę Świecy na Stawnię i okolicę.

# Streszczenie

Paulina wezwała do pomocy Kajetana i Lidię, by pomogli jej opanować Vault. Kajetan przybył z informacją, że na tym terenie był Overlord Hipernetu - Laetitia Gaia Rasputin Weiner. Dracena zniszczyła kolejną efemerydę, ale dostała luki w pamięci. Paulina próbuje pogodzić Kajetana (i potrzebę bezpieczeństwa) z Draceną (i potrzebą wolności); szczęśliwie, Laetitia się objawiła. Po wstępnym przebadaniu Laetitii - nie trzeba z nią walczyć, można to wszystko naprawić. Lidia dostała od Andrei Wilgacz autoryzację jako Regent Świecy...

# Zasługi

* mag: Paulina Tarczyńska, próbująca pogodzić sprzeczne lojalności Kajetana i Draceny, potem jakoś osłonić wszystkich przed Laetitią i wpadła na pomysł naprawy Laetitii.
* mag: Kajetan Weiner, terminus ciężko doświadczony przez Spustoszenie, przez co bardzo nieufny wobec Draceny. Na szczęście, rozważny i sympatyczny jak zawsze.
* mag: Dracena Diakon, zniszczyła efemerydy, Spustoszyła biednego człowieka, składała drony i ogólnie ma wybuchy buntu przeciwko temu, co się z nią stało.
* czł: Janusz Krzykoł, lokalny geek i astronom. Spustoszony przez Dracenę, potem naprawiany. Na szczęście, nic się mu nie stało.
* czł: Maria Newa, wyjechała by ratować dobrą opinię Draceny i Pauliny. Jako reporter, zbiera dane i próbuje jakoś to ponaprawiać.
* mag: Lidia Weiner, neuronautka i psycholog, która nie ma nic do Draceny i - wbrew sobie - skontaktowała się z Andreą a nawet została regentką Świecy dla Laetitii.
* czł: Kurt Weiner, dowiedział się o powiązaniach Draceny z nieletnimi (no, Najady). Potencjalny wrzód na... nodze.
* vic: Laetitia Gaia Rasputin Weiner, Overlord Kolonii Hipernetowej Świecy. Kiedyś: worldmaker. Zakłócona, acz się zregenerowała. Funkcjonuje i chce współpracować ze Świecą.

# Lokalizacje

1. Świat
    1. Primus
        1. Lubelskie 
            1. Powiat Hrabiański
                1. Stawnia, gdzie znajduje się Overlord Hipernetowy Świecy, Laetitia Gaia Rasputin Weiner

# Czas

* Opóźnienie: 3 dni
* Dni: 3
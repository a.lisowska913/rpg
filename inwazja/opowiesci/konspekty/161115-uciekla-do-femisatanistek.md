---
layout: inwazja-konspekt
title:  "Uciekła do femisatanistek"
campaign: nicaretta
gm: żółw
players: kić, raynor
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [161110 - Succubus myśli, że uciekł (HS, temp)](161110-succubus-mysli-ze-uciekl.html)

### Chronologiczna

* [161110 - Succubus myśli, że uciekł (HS)](161110-succubus-mysli-ze-uciekl.html)

## Kontekst ogólny sytuacji
## Punkt zerowy:
## Misja właściwa:

Dzień -1:

Maja Liszka, najemniczka dostała prośbę od Huberta Kaldwora. Jego bratanica, Natalia, powiedziała że jest lesbijką. Rodzice jej powiedzieli, że wyrośnie, więc "uciekła z domu" (ma 22 lata) 6 dni temu i oznajmiła, że została satanistką. Niech Maja tam jedzie i ją przyprowadzi, bo kariera...
No i Maja dotarła do hotelu Lenart i tam jest Natalia, w czarnej sukni i czerwonych rękawicach i zachowuje się uniżenie wobec innej kobiety w lateksie. Mówi do niej "mistrzyni". 

Maja podeszła do Natalii i powiedziała jej, że chce porozmawiać. Natalia spojrzała prosząco na Korę i ta jej pozwoliła się odezwać. Gdy Maja poprosiła czy mogą siąść u stolika, Kora się zgodziła kpiąco. Mai się to zdecydowanie nie podobało...

"A kim ty jesteś?" - Natalia
"Pracuję dla twojego wuja"
"A, dla zboczonego Huberta..." - lekko rozczarowana Natalia

"Słyszałaś kiedyś o feminizmie? Pussy Riot?" - Natalia, rozpalona ewangelizowaniem
"Powiedziałam rodzicom, że zostałam satanistką. Ale naprawdę, to to jest femisatanizm!" - nadal Natalia
"Uratowałyśmy już jedną dziewczynę; to jest prawdziwe siostrzeństwo! Przed kimś takim jak Hubert." - do coraz bardziej zniesmaczonej Mai
"Powiedz Hubertowi, że jest skończony. Mam kanał na Youtubie! Ma 16 followersów!" - Natalia do bardzo podłamanej Mai

"Jak się ma wolność kobiety do pytania czy się możesz odezwać?" - zdziwiona Maja
"Wybrałam tak!" - dumna Natalia, że odrzuciła okowy norm oddając się w dobrowolne niewolnictwo

...

I z tym Maja wróciła, nieco podłamana. Dowiedziała się. Nie chciała wiedzieć...

Dzień 1:

Henryk się zregenerował. Uszkodzona zakonnica (Marzena Gilek) będzie żyć. Henryk jest sam...
Henryk wie, że jest w stanie zlokalizować sukkuba. Ale co z tego? Teraz Nicaretta nie wie, że Henryk ma na nią namiar. Jak ją znajdzie, to się dowie. I najpewniej zwieje, lub zastawi nową pułapkę. Ogólnie, nędza.

Henryk skontaktował się z magiem Świecy działającym najbliżej. Tien Aurel Czarko...
Czarko opieprzył Henryka za to, że ten śmie przeszkadzać mu mówiąc o jakimś sukkubie na terenach nie kontrolowanych przez Świece. Ostrzegł też Henryka - na tamtych terenach znikają magowie; zniknął min. patrol terminusów (5, czyli skrzydło). A teraz Świeca jest atakowana i nie ma czasu się pieprzyć z jakimiś sukkubami. Czarko ostrzegł - niech Henryk wraca do domu. Tu jest niebezpiecznie.

Henryk chciał wracać do domu... ale dwóch silnych facetów podeszło i poprosiło Huberta o podejście. Szef prosi (Hubert). Henryk docenił, że Hubert coś chce po dobroci i poszedł z nimi...

"Maju, przedstawiam ci Henryka. Jest to ksiądz, ale serce ma po dobrej stronie. Bierze publiczne pieniądze i jego wiara jest negocjowalna" - Hubert

Henryk i Hubert doszli do deala. Henryk załatwi z powrotem Natalię. Hubert pomoże Henrykowi walczyć potem z jego "sikorką" (Nicarettą). Jest to deal zasadny i pomocny dla obu stron. Hubert powiedział jeszcze Henrykowi, że odpowiednie zdjęcie i każda dziewczyna prędzej czy później wpadnie w jego ręce. Kimkolwiek by nie była. Każda ma rodzinę.
Mai się to nie podoba, ale cóż.

Maja zdała relację Henrykowi, nieco łagodząc słowa o zboczonym Hubercie. Ale istotne elementy (rewerencja, pytanie o pozwolenie itp) pokazała. 101 sekta.
Hubert kazał swojej "networkerce", Adeli, pokazać Henrykowi informacje o satanistce (mistrzyni).

Mistrzyni nazywa się Kora Panik. Prawdziwe imię i nazwisko. 37 lat, psycholog. Działała w poprawczaku, ale była zmuszona do opuszczenia tamtego miejsca. Ostatnio biedowała, podejrzana była o satanizm; trafiła do Lenartomina w tajemniczych okolicznościach. I dostała od razu miejsce w hotelu jako animator kultury. Hotel wypłaca jej pensję. Miało to miejsce jakiś miesiąc temu. Jak były zarzuty, że to niewłaściwa kobieta, bardzo się za nią wstawił lokalny ksiądz.

Pojechali więc do Lenartomina.

Ksiądz z radością podzielił się problemami i wątpliwościami. Jest stosunkowo młody; powiedział, że w księgach poprzednich proboszczy jest zapis "Uważaj, jeśli posąg marzeń nie śpi". I kilka parafian powiedziało, że śnili, że posąg się obudził. On, ksiądz, też śnił o posągu który się obudził...
Dodatkowo, turystka pytała o srebrne lustro. A w księgach jest wyraźnie napisane "mąż wielkiej mocy nie może spojrzeć w lustro". Ale czemu wielkiej mocy? Nie wielkiej wiary?
...Henryk ma oczy TAK. Arazille...
Turystka szukająca lustra została przez księdza wysłana do biblioteki. Nic tam nie ma. Mieszka w hotelu Lenart.

Ksiądz spytany o Korę powiedział, że to nie satanistka. To feministka. Ona uratowała dziewczynę; Urszulę Kram. Wykupiła ją z domu publicznego i uratowała do hotelu. To chrześcijański uczynek jest. Owszem, ma kiepski styl - agresywny feminizm, ale nie zabija kotów i ratuje kobiety. Sam poszedł z nią porozmawiać, więc uśmiechnęła się i sama oprowadziła go po swoich dziewczynach i dziełach.

Henryk wyczuł od księdza... SOCJALIZM! Radykalny socjalistyczny antysystemowiec, a pod spodem księdza jest niezgoda na świat, który jest zły. To dlatego mniej interesuje się "satanizmem" a bardziej działaniami prospołecznymi. Poprosił księdza, czy może się na niego powołać. Oczywiście, że tak. Więcej, Henryk może zostać na plebanii, ale nie mają gosposi. To byłoby nie po chrześcijańsku.
...dla Henryka jest to coś niesamowitego.

W tym czasie Maja obchodziła kościół i okolice. Znalazła ślady dyskretnych włamań; ktoś przeszukiwał kościół. Włamywał się nawet do trumien... nie wiadomo czy znalazł czy nie, ale metodycznie przeszukiwał cały kościół. Najpewniej nie znalazł... jednak poszukiwania i działania Mai sprawiły, że siły z zewnątrz się nią zainteresowały. Ktoś, na kim warto skupić uwagę.

Maja i Henryk się spotkali.
Maja powiedziała Henrykowi, że bardzo dokładnie przeszukano kościół i nic oczywistego nie zniknęło. Szczególną uwagę przyłożono do małych starych luster.
Henryk przeszedł się jeszcze po kościele; przejrzał archiwa i popatrzył na architekturę. Jest dziwna. Henryk doszedł do tego o co chodzi - ten kościół był zbudowany przez magów dla magów. Miejsce wykrywające co się dzieje w okolicy i przesyłające sygnały do maga rezydenta...
Ale nie ma maga rezydenta. Marnuje się.

Poszli do hotelu.
Recepcjonistka poznała Maję i się uśmiechnęła. Mistrzyni powiedziała, że Maja wróci. Zaproponowała pokój 104 i wyraźnie się ucieszyła. Mai to wisi; zgodziła się.

"Czy... czy pani sprowadziła sobie KSIĘDZA by wyegzorcyzmować mistrzynię Korę?" - rozbawiona recepcjonistka

Ksiądz Henryk poprosił o rozmowę z Korą. Recepcjonistka powiedziała, że z radością. Zaproponowała pójście do sali balowej.

W sali balowej jest kilkanaście kobiet i Kora. Pozdrowiła księdza i poprosiła, by został on i Maja. Ksiądz poprosił Korę, by ta z nim porozmawiała na uboczu. Ta powiedziała, że się zgadza, niech Maja jednak pogada z koleżankami. Gdy ksiądz powiedział, by Maja sobie poszła, Kora zrobiła wielką przemowę o tym jak to ksiądz rządzi Mają. Nie udało jej się zagrzać i uodpornić dziewczyn, acz udało jej się przekonać dziewczyny do tego, by jeszcze bardziej były oddane Sprawie i nienawidziły Huberta i jego podobnych...

Kora poszła na ubocze z księdzem. 

"Kościół jest instytucją patriarchalną pragnącą zniszczyć kobiety. Więc jeśli szatan chce zniszczyć kościół... ja jestem satanistką." - Kora, do Henryka.

Henryk spytał Korę, o co naprawdę jej chodzi. Jej odpowiedź go przeraziła - Kora zakłada siostrzeństwo, robi to co kościół powinien robić. Kora chce chronić wszystkie kobiety, chce pilnować, chce je ratować. A Hubert zostanie zniszczony i upadnie. 
Henryk jest już pewny, że Kora nie jest magiem. Ale ktoś mógł Korę zmanipulować... ktoś zmienił psycholog w liderkę sekty.
Kora chce zwalczyć pornobiznes. Kora chce ratować kobiety. Ale ktoś ją wspiera. Ona jest mistrzynią ceremonii, tylko mistrzynią ceremonii.
I Kora nie pozwoli Henrykowi wydostać Natalii i przywrócić jej do rodziny...

Tymczasem Maja w hotelu zauważyła ślady przeszukiwań. Te przeszukiwania jednak są dość pobieżne; ktoś tego nie zdążył zrobić jeszcze odpowiednio dokładnie. Maja dała radę zlokalizować miejsca, które jeszcze nie zostały przeszukane. Będzie chciała polować w nocy.

Henryk podziękował Korze i poszedł spotkać się z Mają.
Ta stwierdziła, że trzeba znaleźć koneksje finansowe i sieć kontaktów. Skąd jest to finansowanie? Kto finansuje ten hotel? Czemu?
Po pieniądzach poznamy co i jak się dzieje. Maja powiedziała, że poprosi Huberta by on to sprawdził.

Do Mai podeszła Natalia. Poprosiła Maję, by ta jej pomogła pomścić Ulę. Skrzywdzić Hipolita Mraczona. Maja zauważyła potencjalny opening; Natalia robi to z własnej inicjatywy bo chce pomścić Ulę. Kora najpewniej niewiele wie. Maja jest za tym; niech Natalia skrzywdzi Mraczona i poczuje szok z tego co zrobiła...
Więc zadziałają w nocy.

# Progresja



# Streszczenie

Hubert ma problem - jego bratanica, Natalia, uciekła do femisatanistek do Lenartomina. Poprosił Henryka, by ją sprowadził i dodał do pomocy Maję, swoją zaufaną strażniczkę. Na miejscu Henryk zauważył, że lokalny ksiądz to radykalny antysystemowiec który sympatyzuje z femisatanistkami (które zresztą nie robią nic złego; pomagają innym kobietom). Dowiedział się o potencjalnej manifestacji Arazille, działającej turystce która chce uzyskać Srebrne Lusterko i... ogólnie szefowa kultu Kora dała radę go wykorzystać by podnieść kohezję dziewczyn. Maję próbowała nawrócić Natalia - nie wyszło jej. Przechlapane.

# Zasługi

* mag: Henryk Siwiecki, zaniepokojony wstępnymi oznakami obecności Arazille na usługach pornobiznesu dla własnych korzyści. Potrzebuje pomocy w ubiciu demona.
* czł: Maja Liszka, chłodna bodyguard Huberta Kaldwora, niezbyt społecznie ukierunkowana z drastycznym planem wyciągnięcia młodej z kultu.
* mag: Aurel Czarko, terminus Świecy spławiający Henryka raportującego o niebezpiecznym sukkubie. Zniechęca Henryka do czegokolwiek.
* czł: Kora Panik, mistrzyni ceremonii i femisatanistka. Psycholog, która dostała drugą szansę od kogoś. Chce ratować dziewczyny i zniszczyć Huberta.
* czł: Urszula Kram, uratowana ofiara Hipolita. Dzielnie dołączyła do grona femisatanistek. Przyjaciółka Natalii.
* czł: Natalia Kaldwor, bratanica Huberta która dołączyła do Kory jako satanistka-akolitka. Przyjaciółka Urszuli. Zbuntowała się przeciw rodzicom.
* czł: Hipolit Mraczon, bogatszy facet któremu nikt nie podskoczy; skrzywdził Ulę. Natalia chce, by cierpiał.
* czł: Hubert Kaldwor, który dla odmiany martwi się o swoją bratanicę, która uciekła z domu i dołączyła do femisatanistek...
* czł: Adela Klepiczek, jedna z analityczek i networkerek Huberta Kaldwora. Ładna dziewczyna z niefortunnymi zdjęciami.
* czł: Andrzej Klepiczek, ksiądz w Lenartominie. Radykalny pro-humanista, socjalista i antysystemowiec. Ogólnie dobry * człowiek.

# Lokalizacje

1. Świat
    1. Primus
        1. Lubelskie 
            1. Powiat Wolny
                1. Lenartomin
                    1. Hotel Lenart
                    1. Biblioteka
                    1. Kościół
            1. Powiat Tonkij
                1. Wyjorze
                    1. Centrum
                        1. Iglica Trójzębu

# Skrypt

-

# Czas

* Dni: 1
---
layout: inwazja-konspekt
title:  "Jak prawidłowo wpaść w pułapkę"
campaign: powrot-karradraela
gm: żółw
players: kić, dust
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [161102 - Magowie Esuriit w domu... (HB, SD)](161102-magowie-esuriit-w-domu.html)

### Chronologiczna

* [161102 - Magowie Esuriit w domu... (HB, SD)](161102-magowie-esuriit-w-domu.html)

## Kontekst ogólny sytuacji
## Punkt zerowy:

## Misja właściwa:

Dzień 1:

Ekspedycja wróciła. A dokładniej - wrócił Dosifiej, z energią (rzeczami jakie Bazyli może przekształcić w kryształy Quark do zasilenia syberionu). Dosifiej powiedział, że rozłączył się z pozostałymi; podzielili się, bo pojawił się speculoid. Ale pozostali mogą jeszcze żyć...
Marianna chce rozpocząć poszukiwania.

Andżelika zaproponowała, że spróbuje zrobić jakąś wizję (scrier) w okolicy miejsca rany Dosifieja, z jego własnej krwi.

"To niebezpieczne. Czy chcesz ryzykować?" - Siluria
"Nie chcę ekspedycji. Chcę ich znaleźć martwych..." - Andżelika

Marianna dostarczyła Andżelice krew znikniętych terminusów do scriera. Andżelika i Anna pracują nad tym, tymczasem Hektor przesłuchał Dosifieja - mag Esuriit nie kłamie. Naprawdę chce ratować swoich. Potwierdza się teoria Pawła Mausa - magowie Esuriit nie wiedzą że są magami Esuriit.
Andżelika i Anna to zrobiły. Mają podgląd...

Jeden z terminusów nie żyje. Wpadł na niego speculoid, mag użył mocy, Wijokłąb Esuriit wpadł na linię energii magicznej i "Spustoszył" go, ten się poślizgnął i nabił na kolce. Ale drugi terminus się ostrzeliwuje speculoidowi. Próbuje mu uciec a speculoid się z nim bawi... na Wyspie Kolców.

Ignat, Siluria i Hektor złożyli do kupy (technicznie: Ignat buduje, Siluria i Hektor rozbierają bazę by dostarczyć komponenty i je dostosowują) wyścigówkę zagłady. Łazik. Szybki łazik. Z miejscem na 6 osób. Lepiej mieć zapas.
Hektor przygotował sprzęt z wyposażenia Dionizego. Jakieś granaty, jakieś elementy broni.

Kto jedzie? Hektor, Anna (nie zostawi Hektora), Konstanty Bankierz, Jakub Pestka. Zostają: Ignat (buduje wielkie działo), Andżelika (chce do archiwów), Siluria (nie chce na pole bitwy), Mikado (ktoś musi bronić). Andżelika rzuciła sekwencję czarów defensywnych, zmodulowanych do Esuriit i odporniejszych na Esuriit. Powinny być mniej widoczne i odporniejsze od zwykłych tarcz.

Z pomocą Andżeliki, Siluria próbowała sobie przypomnieć o co w ogóle chodzi ze speculoidami. TEN speculoid zachowuje się inaczej. Ale przypomnieli sobie czym speculoid jest; ten tutaj jest... inny. Najpewniej poluje, próbuje ich wyciągnąć.

"Nie... bez Mikada NIE MAMY SZANS tam przeżyć" - Hektor do Silurii
"A kto NAS będzie bronić?" - Siluria do Hektora

Hektor zwinął co się da ze sprzętu Dionizego. I jadą ratować terminusa... z Mikadem. 
Ma kilka granatów nullifikacji (wyłączających energię magiczną, by Wijokłąby nie mogły podążać)

Ekspedycja ratująca Konstantyna:
1) Musi przejechać przez las kolczasty ze zmieniającą się topologią

Hektor przeprowadził ich przez kolczasty las; udało mu się uniknąć ciągle zmieniającej się topologii. Ewentualne krystaperze zostały wystrzelane przez Annę. Nie było ich zbyt wiele. 

2) Musi przeskoczyć łazikiem Szczelinę Głodu i uniknąć problemów z krystaperzami

Niestety, Hektorowi nie udało się przeskoczyć tego cholerstwa. Anna wzmocniła skok energią magiczną (kineza), ale niestety poruszenie magiczne wywołało jeden rój krystaperzy. Hektor wylądował po drugiej stronie i dał pełną moc by uniknąć cholerstwa. Anna kinezą wspomaga się by rzucić granatami; udało jej się pozbyć krystaperzy. Niestety, energia magiczna wezwała wijokłąby. Hektor je wyprzedził... zgubiły trop. Niestety, łazik jechał za szybko, walnął w jeden z kolców - ale struktura łazika przetrwała. Hektor pojechał dalej bez większych problemów...

Tymczasem w bazie.
Ignat pracuje nad mega ciężkim działem. Nazywa je "Kometor - miotacz asteroidów". Andżelika i Siluria pomagają. Marianna zaprotestowała przed wzięciem całego budynku przez Ignata; też powiedziała, że nie do końca jej podoba się nazywanie przez Ignata ich "tienowatymi". Koszt energetyczny działa jest dość wysoki. Ignat dostał przez Silurię zachętę by traktować tienowatych z jakąś godnością; oczywiście, nie wyszło. Ignat.

I Siluria ma grupę nieszczęśliwych magów Świecy traktowanych nieco jak plebs. Po Ignatowemu. Co NIE sprzyja jej Pryzmatowi. A Ignat składa Kometor.
Niestety, morale jest ZA niskie. Działo będzie drenować ZA DUŻO energii. I budowa potrwa za długo...

Silurię martwi to cholerne morale. Ci magowie NIE WIERZĄ, że to działo jest skuteczne. To powoduje, że Pryzmat jest przeciwny Kometorowi... to sprawia, że najpewniej nie zadziała tak jak powinno. Siluria wzięła Andżelikę na stronę - opracowały krótką historię jak to Ignat przegrywał liczne zakłady o śledziach i musiał wpadać na Esuriit rozwalać tu wszystko. Plus, rozpięła dekolt. Udało jej się uratować sytuację...

Tymczasem, Hektor dotarł w okolice działań speculoida.

Terminus jest już bez nadziei. Ranny, speculoid się nim bawi. Hektor włączył światła i ruszył w kierunku speculoida. Speculoid powołał iluzję dzieci Anny i Hektor przez nie przejechał. Anna i Mikado zaczęli walkę; Mikado ją unieszkodliwił i raniąc, doprowadził ją do zmysłów.

"Co tu widać na radarach?!" - Hektor
"Tu jest coś wielkiego! Pod tobą!" - Mikado

Hektor natychmiast zawrócił! Spod ziemi wypełzł Czerw i pożarł nieszczęsnego terminusa, ale nie dorwał Hektora. Czerw - istota stworzona z materii Esuriit i napędzana energią Esuriit zaczął gonić łazik... i nadrabiać. Hektor wyrzucił granaty nullifikacyjne a jeden z nich Anna wyrzuciła w kierunku na speculoida.
Udało się; granat nullifikacyjny dał radę skutecznie osłabić speculoida; został skutecznie osłabiony i unieczynniony na pewien czas.
Nie spodziewał się tego.

To powoduje, że speculoid nie ma możliwości praktycznego kontrolowania przestrzeni. Łazik przeskoczył przez przepaść, walnął i rozwalił jakiś kolec, padły tarcze, sam łazik to "Polska w ruinie", ale się trzyma. Czerw na ogonie, też: krystaperze.

Hektor mistrz kierownicy. Jedzie, rozwala kolce, łazik coraz gorszy... ale wyjechał na prostą mimo czerwia. Łazik ma przebicie w baku, ale się trzyma. Tymczasem Mikado używa zaklęcia na poziomie światła i dźwięku, kontrola otoczenia - robi magiczny performance dla krystaperzy. Inni magowie go osłaniają i wspierają. Mikado udało się skonfudować nietoperze. Anna i Konstanty złapali kilka jako źródło energii. Łazik wypadł prosto do bazy - a czerw za nim.

Pryzmat lol. Czerw wejdzie.

I Ignat walnął z działa. Kometor kontra czerw. Targeting system Andżeliki, broń Ignata, Pryzmat Silurii.
Działo rozwaliło zewnętrzny pancerz czerwia; głowa się rozpadła. Zassanie energii jest koszmarne...

Czerw się chce wycofać. Ale Ignat chce przeładować i strzelać.
Siluria rzuca się Ignatowi na szyję i chce go zatrzymać. Boi się, że baza zostanie zniszczona.

"Ignat! Nie! Bo już nigdy nie będzie śledzików!" - Siluria

Ignat jest zapalony ogniem walki. Chce strzelać! Ale Siluria go zatrzymała zanim Pryzmat się doszczętnie rozsypie.
I jej się udało. Ignat umożliwił Czerwiowi się wycofać... ku swej wielkiej rozpaczy.

"Silurio... ale..." - zrozpaczony Ignat
"Masz wielkiego całusa!" - Siluria

Pryzmat i morale w bazie zdecydowanie wzrosło.
To jest TEN moment na który czekał Zespół. To jest ten czas na zebranie surowców...

# Progresja


# Streszczenie

Z wyprawy po surowce wrócił jedynie ranny Dosifiej. Scryer Andżeliki wykazał, że jeden terminus żyje; poluje nań speculoid. Ignat zbudował łazik i wraz z Andżeliką i Silurią skupili się na budowie Kometora - superciężkiego działa. Hektor, Anna, Mikado i Konstanty pojechali ratować terminusa Świecy. I to była pułapka - speculoid wezwał potężnego Czerwia. Anna unieszkodliwiła speculoida granatem nullifikacji i wrócili szybko do bazy z Czerwiem na ogonie. Ignat zranił Czerwia, który podkulił ogon i dał nogę. Morale wzrosło, pułapka rozwiązana i kupili sobie chwilę czasu. Ogólnie, super wynik (choć terminus do uratowania zginął)!

# Zasługi

* mag: Hektor Blakenbauer, mistrz kierownicy łazika Ignata, który próbował (fail) uratować terminusa ale przypadkowo ściągnął Czerwia do bazy.
* mag: Siluria Diakon, władczyni serc * magów odwracająca Pryzmat, dzięki której nie tylko Ignat zranił ciężko Czerwia, ale i go nie zabił (wraz z destrukcją bazy).
* mag: Andżelika Leszczyńska, support; budująca defensywne tarcze dla łazika (by mogli uciec) i system namierzający dla działa Ignata (by mogło trafić).
* mag: Ignat Zajcew, konstruujący wpierw łazik a potem Kometor Oblężniczy Zabijający Czerwie. Obraża * magów Świecy. Prawie niszczy swoją bazę. Typowy Zajcew.
* mag: Mikado Diakon, robiący performance krystaperzom i odciągający je od zespołu. Też: powstrzymał Annę w kluczowym momencie przed walką z Zespołem.
* mag: Anna Myszeczka, która wpierw uratowała łazik przed upadkiem a potem Speculoid sprawił, że prawie wpadła w berserk (iluzja przejeżdżanych dzieci). Miota kinetycznie granatami.
* mag: Marianna Sowińska, nie chcąca zostawiać żadnego ze swoich * magów na śmierć.
* mag: Konstanty Bankierz, terminus; prawdziwy; współpracuje z Zespołem dla uratowania jednego ze swoich. Bardzo pozytywnie nastawiony do Zespołu po tej akcji.
* mag: Dosifiej Zajcew, jedyny członek ekspedycji po surowce który przeżył i przyniósł cokolwiek. Przynęta puszczona przez Siriratharina.
* vic: Siriratharin, który prawie zgładził Zespół wysłany by uratować terminusa; zastawił pułapki lecz nie docenił technologii KADEMu (bo nie zna). Wyłączony z akcji na moment.

# Lokalizacje

1. Świat
    1. Faza Esuriit
        1. Insula Luna
            1. Srebrna Septa
                1. Centrum
                    1. Kometor, Miotacz Asteroidów zrobiony z Blockhausu O
            1. Wyspa Kolców
                1. Las kolczasty, ciągle zmieniające się miejsce przypominające Race The Sun na poziomie Apocalypse.
                1. Szczelina Głodu, przez którą Hektor przeskoczył łazikiem i która jest siedziskiem krystaperzy
  
# Skrypt

# Czas

* Dni: 1

# Godny przeciwnik

## Czerw Esuriit:
Istota złożona z materii nieorganicznej animowana przez energię Esuriit; potężna i wspomagana przez Wijokłąb Esuriit.

pancerz zewnętrzny: magiczna elita: 30
segmenty: 28
core: 25
eksplozja: 22

## Wijokłąb Esuriit:
Pra-istota Spustoszenia, wdziera się w ciało i adaptuje. Porusza się po energiach magicznych.

reakcje: 19
defensywy: 15

## Rój Krystaperzy:
Kryształowe nietoperzopodobne bezszelestne mordercze żywe skrzydlate pociski żywiące się krwią. Przechlapane.

atk: ?
rozgonienie: 22
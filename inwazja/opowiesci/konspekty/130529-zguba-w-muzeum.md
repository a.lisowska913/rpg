---
layout: inwazja-konspekt
title:  "Zguba w muzeum"
campaign: anulowane
gm: żółw
players: kić, dust
categories: inwazja, konspekt
---

# {{ page.title }}

## Punkt zerowy:

- Postać Dusta: 	Wojtek Bierecki
- Postać Kić: 		Ania (nie zna swego nazwiska)
- Ania przywołała ducha, bo aktywowała jakiś artefakt
- Dla Wojtka byłoby niekorzystne gdyby mistrz Ani i Wojtka to wiedział, bo to Wojtek wykradł ten artefakt
- Wojtek najbardziej zazdrości Ani domeny magicznej: magii mentalnej
- Wojtek jest ulubionym uczniem mistrza dzięki domenie nekromancji
- Aktualnie, ten duch znajduje się w lochach muzeum na rynku
- Nekromancja Wojtka nie działa dobrze na ducha, gdyż nie jest do końca duchem
- Ania chce wykorzystać tego ducha do odegrać się na przyjaciółce.
- Przyjaciółka Ani jest zadurzona w Wojtku od pierwszego spotkania.
- Ania z przyjaciółką rzuciły kiedyś nieudany czar we dwójkę. Od tej pory uganiają się za nimi wszyscy faceci z klasy.
- Przyjaciółka Ani (Jola) próbowała zaimponować Dustowi technomancją.
- Mistrz przygarnął Anię za jej świetną pamięć (pamięć absolutną).
- Jola najbardziej podziwia w Wojtku domenę sztukmistrza.
- Gdyby Ania musiała zarabiać, zarabiałaby jako adwokat.
- Wojtek ma specjalny dar: potrafi rozkochać w sobie każdego ducha.
- Mistrzowi strasznie zależy na tym artefakcie, bo jutro wieczorem ma randkę z piękną czarodziejką, której chce go sprezentować.
- Mistrz nie wie, że Wojtek pochodzi z rodu, którego mistrz nienawidzi, bo żyje dość na uboczu i nie zna nazwiska panieńskiego jego matki.
- Jola traktuje Anię jak młodszą siostrę, bo liczy na to, że uznają je za rodzeństwo; nie zobaczą między nimi różnic.
- Jola szuka opieki u Wojtka, bo tylko nekromanta pozwala jej normalnie żyć.
- Jola jest niezbędna, by wprowadzić ducha do artefaktu, bo nieświadomie go ze sobą związała.
- W pobliżu tego ducha nie wolno czarować, bo zwiewa. Tym razem zwiał do lochów.
- Duch chce się zemścić na magu, który zmienił go w ducha.
- Z kandelabrem znajdującym się w holu mistrza ducha łączy fakt, że z tym kandelabrem łączy sę sposób w jaki ostatnim razem mistrz tego ducha schwytał.

## Misja właściwa:

01. Jola rzuca czar na kamerę muzeum, przenosząc obrazy na swój tablet. Znalezienie artefaktu (różdżki). Jest w stróżówce muzeum (tam się widać zgubił Ani).

02. Wojtek rzuca zaklęcie, duch towarzyszy Ani która idzie do muzeum, bo zgubiła różdżkę.

03. Duch przełączył się na strażnika który poszedł po różdżkę. Różdżka wessała ducha. Jak strażnik dotknął różdżki, duch się pojawił spełniać jego życzenia "co to jest! Znikaj!". I zniknął.

04. Drugi strażnik - to samo. On nie widzi ducha. Poszedł, dotknął, zobaczył - pierwszy strażnik nie widzi. 

05. Jola rzuciła czar - na drugim końcu muzeum zbiera się studencki flashmob przebrany za goryloklaunów (czyt. to widzi portier przez swój widok z kamer). Portier tam idzie, Ania rzuca nań lekką sugestię (mogę iść gdzie chcę), potem poszła do stróżówki, tam rzuciła sugestię lekką "weźcie rożdżkę przez materiał i mi ją oddajcie".

06. Oddali jej. Wróciła z powrotem i różdżką do reszty ekipy. Akcja jednogłośnie nazwana FEST FAILEM.

07. Poszli z Wojtkiem do podziemnych korytarzy, które zwie domem. Tam Wojtek wyczuł, że ktoś tam jest. Jego znajomy duch samobójczej emo dziewczyny, wywęszył GDZIE i się tam zamanifestował w straszny sposób.

08. Tak napatoczyli się na nieprzyjemnego maga pod innym mistrzem, Wincenta "Ekierkę" Słonia. Ekierka autentycznie chciał pogratulować fajnego dowcipu Wojtkowi, ale został wzgardzony przez wszystkich. Tak nim gardzili i go wyśmiewali, że obiecał się zemścić.

09. Ekierka poszedł do muzeum zbierać aurę, za nim Wojtek puścił ducha. Jak tylko Ekierka wszedł do stróżówki, Wojtek poszedł i spytał strażników, czy nie mogliby znaleźć jego kumpla studenta, który gdzieś się zapodział. Strażnicy przyłapali Ekierkę w stróżówce...

10. Po odrobinie biegania i czarowania Ekierka rozwiązał problem i się ewakuował. Oczywiście, jednocześnie zamazał aurę... nieszczęśliwy pseudoterminus musi mścić się w inny sposób :-(.

11. Czas na plan wejścia do muzeum. Wojtek posłał swojego znajomego ducha emo nastolatki (Ruby) przez muzeum do negocjacji z tamtym duchem, którego muszą złapać. Ruby nie wróciła. Ostatni sygnał - Ruby unieszkodliwiona a tamten duch, jak upiorna zjawa, wbija w niej swoje szpony.

12. Ania sprawdziła kandelabr - na kandelabrze zmienił się układ kamieni. Po kilku eksperymentach wyszło, że kandelabr kontroluje stan różdżki. Ustawiła kandelabr na "chwilę po wypuszczeniu ducha". Jako, że tylko Jola jest w stanie związać ducha, dali jej różdżkę. Faktycznie, zmieniła stan kandelabru. Jak odłożyła, stan wrócił do poprzedniego stanu. Jola wymusza pewien stan...

13. Czas na akcję. Jola zajęła się kamerami. Wojtek poprowadził ich przez podziemia do okolic podmuzealnych, gdzie za ścianą powinny znajdować się magazyny muzeum. Wojtek postawił ducha-wabika, który przeszedł przez ścianę.

14. Wabik dostrzegł upiora stojącego nad złożonym kręgiem rytualnym, jest tam też nieprzytomny strażnik i emo duch. Upiór zlekceważył wabika, ale zbliżywszy się do ściany wyczuł Jolę i różdżkę. Podjął walkę, przeszedł przez ścianę i rzucił na Jolę "Terror", z powodzeniem. Wojtek używając korzyści spróbował w sobie upiora rozkochać. Upiór stanął...
Ania wbiła w upiora różdżkę, na co dostała sieknięcie szponami od niechcenia. Polała się krew.
Na to zareagowała natychmiast Jola. Przyjęła Klątwę, przeszła w formę nieumarłej (trochę jak strzyga) i pocałowała usta-usta Anię, wdychając w nią energię negatywną. Rany Ani się zatrzymały, przestała krwawić. Potem - Jola zaatakowała upiora, ponosząc ciężkie rany w procesie. Upiór znów skupił się na Wojtku, żądając, by ten poszedł z nim lub dziewczyny zginą.
Wojtek się zgodził.

15. Ostatnia, desperacka próba. Jola atakuje upiora, odwracając maksymalnie jego uwagę. Wojtek i Ania wspólnie rzucają czar mentalno-nekromantyczny, by upiór się zatrzymał. Tym razem się udało - Jola ma ciężkie rany, upiór jest Zatrzymany, Jola się zatraciła. Powstrzymana przez Anię, Jola wpakowała różdżkę upiorowi w gardło i przywróciła stan pierwotny (jak przed misją) po czym rzuciła się do ucieczki.

16. Śledzona przez duszka Wojtka, zapędziła się w ślepą uliczkę. Tam wyszedł na nią Wojtek i okazał jej pełne zaufanie. Jola przeszła w formę ludzką, odezwały się rany i straciła przytomność. Wojtek zrobił co mógł, by odzyskała przytomność i znowu przeszła w formę nieumarłą...


Po misji (fallout):

- Ania i Jola, ciężko ranne, zostały wyleczone przez pokątnego maga-alkoholika. Świetny lekarz, kiepska etyka, nie zadaje pytań, dobre serce.
- Wojtek odniósł artefakt. Zero problemów.
- Ekierka odkrył tajemniczy mroczny rytuał w podziemiach, "wszystkich uratował" i został bohaterem.
- Ruby (emo duch Wojtka) wróciła do Wojtka.



"Za dużo rzeczy się gubi w tym muzeum"
---
layout: inwazja-konspekt
title:  "Opętany konstruminus"
campaign: rezydentka-krukowa
players: kić
gm: żółw
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [170523 - Opętany konstruminus (PT)](170523-opetany-konstruminus.html)

### Chronologiczna

* [120507 - Preludium: Historia świata](120507-preludium-historia-swiata.html)

## Kontekst ogólny sytuacji

MG ma ogólną wizję spotkania Dobrocienia z Pauliną; coś ostro skrzywdziło jej przyszłego pacjenta i razem muszą sobie z tym poradzić. To wszystko co wiem.

## Punkt zerowy:

* Ż: Dlaczego ciężko uszkodzony opryszek poszedł do Pauliny, nie do "swoich" lekarzy?
* K: Bo to, co go załatwiło będzie czekać TAM. I on o tym wie.
* Ż: Czemu pomoc Pauliny jest niezbędna w odzyskaniu dziwnego magitechowego systemu leczącego?
* K: Nietypowe i potrzebne tutaj jest połączenie lekarz + nekromanta.

Żółw: Dobrocień przejmował magitech leczniczy i coś duchowego opętało... konstruminusa po czym go załatwiło? I zagraża magitechowi leczniczemu (dalej:magimedowi).

* Ż: Gdzie znajduje się magimed? Lokalizacja fizyczna, jakieś jej aspekty.
* K: Dawno, dawno temu była to siedziba gildii planktonowej. Potem opuszczone, ktoś to squatował... ma Historię.

Żółw: czyli mówimy o duchu jednego z magów gildii planktonowej. Czyj to konstruminus? Bankierzy? Ale co on tam robi? Próba... wskrzeszenia? self-res przez konstruminusa? Starscream Ghost?

* Ż: Jakim - całkowicie nie związanym z głównym tematem - zadaniem zajmował się tam vicinius Bankierzy?
* K: Został wysłany do zbierania magicznych ziół.
* Ż: Na czym polega biznes wykorzystujący magiczne niebezpieczne w pozyskaniu zioła?
* K: Te zioła są składnikami alchemicznymi; biznes zajmujący się dostarczaniem półproduktów alchemicznych.

Żółw: innymi słowy, w okolicach siedziby gildii planktonowej pojawiają się zioła magiczne i jest jakieś niebezpieczeństwo

* Ż: Jakiego typu potężny rytuał był dawno temu użyty w niezamieszkanym miejscu? I nie wszystko się udało. Szkoły, intencja... szukam niebezpieczeństw.
* K: Kapsuła czasu. Materia, infomancja. Próbowali zrobić Kapsułę Czasu, która przetrwa WSZYSTKO. Więcej, niż Zaćmienie. I coś wtedy poszło nie tak.
* Ż: Jak nazywała się gildia planktonowa? Miała czterech członków.
* K: Ojej... nie mam pomysłu. 
* Ż: Oki. Bractwo Pary.

Żółw: oki. Mamy implikacje. Innymi słowy, mamy do czynienia z... siedzibą gildii zlokalizowaną KIEDYŚ w okolicy [kolejowej wieży ciśnień](https://pl.wikipedia.org/wiki/Wie%C5%BCa_ci%C5%9Bnie%C5%84). Domek z czterema magami plus wieża ciśnień, gdzie prowadzili eksperymenty.

* Ż: Dlaczego Kapsuła Czasu, jeśli ich dominujące szkoły dawały... parę. Taką wodną. I tym się zajmowali. Magia elementalna, nie?
* K: Bo byli po części ludzistami i koncept kapsuły czasu im się po prostu spodobał. Pożyczyli go od ludzi.
* Ż: Co naprawdę fajnego robili z parą? Czemu ta para? Co takiego fajnego z parą?
* K: Niewielka grupka pasjonatów technologii chcących stworzyć muzeum i usprawnić silniki parowe. Czyli technomancja + elementalna.

**Ścieżki Fabularne**:

ran 5: (2,1,1,4,2): 

* **Skażony Magimed**: 4
* **Paradoksalny Duch**: 6
* **Zniszczony Konstruminus**: 0
* **Problemy Dobrocienia**: 2 

## Misja właściwa:

**Dzień 1:**

Do Pauliny przyszedł ciężko ranny mag. Widać, że się rozpaczliwie teleportował i że ledwo się przywlókł, działając na rezerwach. Zaczął bredzić w gorączce, że "on tam jest i czeka a tu go nie ma". Widać, że to była walka. Czarodziej padł wchodząc do gabinetu, powodując przerażenie akurat znajdującej się w gabinecie młodej pacjentki, która BARDZO próbowała Paulinę przekonać, że nie może być w ciąży - to musi być boże błogosławieństwo, bo ona nigdy nie...

Zdaniem Pauliny, niektórzy mają większe problemy niż randomowa ciąża. Na przykład nieprzytomny mag. Młoda wymaga uspokojenia. Paulina złapała ją za ramiona i nakrzyczała na Zosię; 4v2->S. Paulina uspokoiła Zosię; kazała jej iść bo ma ważniejszy temat. Ma przyjść później i wtedy porozmawiają o cudownej ciąży. Zosia poszła a Paulina skupiła się na nieprzytomnym magu...

Mag jest: Skażony, Ranny i Nieprzytomny. Paulina ma gabinet, który ma: Leki, Narzędzia Diagnostyczne. "Okoliczności: -1". Paulina zakłada lapisowo-lateksowe rękawiczki i bierze się do roboty...

Zadanie Pauliny nie jest proste, ale nie jest też szczególnie trudne.

* Tor zdrowia maga: 2, 2, 1 (5)
* Tor kłopotów maga: 2, 2, 1 (5) -> rośnie 0-1-2.

Paulina skupiła się na diagnozie maga, w pełnym zakresie. Próbuje dojść do tego, co się z nim dzieje i co się z nim w sumie stało. 7v4->

Postać (strona gracza):

| .I_Strat. | .I_Takt. | .Specka. | .Umiejka. | .Specka Mag. | .Umiejka Mag. | .Inklinacja. | .POSTAĆ. |
|     1     |     1    |     1    |     1     |       1      |      1        |       1      |    7     |

Okoliczności:

|  .Przygotowanie. | .Pomysł. | .Skala. | .Magia. | .Surowiec. |  .OKOLICZNOŚCI. |
|         -1       |     0    |    0    |    1    |      0     |        0        |

Przeciwnik (strona NPC):

| .VERSUS. | .Diff. | .Wpływ akcji gracza. | .OPOZYCJA. |
|     2    |    2   |          0           |     4      |

Wynik:

| .Postać. | .Opozycja. | .Okoliczności. | .Rzut.    | .Wynik. |
|     7    |     4      |       0        | 06: -1 W0 |   S     |

Paulina zauważyła, że rany są od walki z konstruminusem. To niefajnie. Będzie musiała to zgłosić. Ale oprócz tego Paulina wyczuwa echo mentalno-astralne. I jeszcze dotyk echa magii leczniczej, ale dziwnego typu; golemiczno-technomantycznego. Działo się tu coś dziwnego. Mag jest uszkodzony, poważnie uszkodzony, bo konstruminus zaatakował z zaskoczenia. Miał okazję skrzywdzić maga i uszkodzić mu możliwości czarowania - tymczasowo.

Też konstruminus, o dziwo, nie walczył by pojmać. Walczył, by zabić. Co widać po rodzaju ran. Nietypowe dla konstruminusa. Tor kłopotów maga rośnie o (1k20->6) 1.

Paulina stwierdziła, że wpierw zajmie się magią pożerającą jego ciało. Skupiła się na potężnej inkantacji.

Postać (strona gracza):

| .I_Strat. | .I_Takt. | .Specka. | .Umiejka. | .Specka Mag. | .Umiejka Mag. | .Inklinacja. | .POSTAĆ. |
|     1     |     0    |     1    |     1     |       1      |      1        |       1      |    6     |

Okoliczności:

|  .Przygotowanie. | .Pomysł. | .Skala. | .Magia. | .Surowiec. |  .OKOLICZNOŚCI. |
|         +1       |     0    |    0    |    0    |      0     |        1        |

Przeciwnik (strona NPC):

| .VERSUS. | .Diff. | .Wpływ akcji gracza. | .OPOZYCJA. |
|     2    |    2   |          2           |     6      |

Wynik:

| .Postać. | .Opozycja. | .Okoliczności. | .Rzut.    | .Wynik. |
|     6    |     6      |       1        | 09: -1 W0 |   R     |

W ramach Remisu: +1 Ścieżka kłopotu Dobrocienia. Paulina mu pomogła, zdecydowanie szarpnęło, ale niestety - trudniej będzie mu ukryć, że był jakiś problem i miał do czynienia z konstruminusem. Rotowanie toru: +0

* Tor zdrowia maga: V, 1, 1 (2)
* Tor kłopotów maga: 1, 2, 1 (4) -> Ścieżka +1.

Paulina zdecydowała się zająć się jego ciałem już bez użycia magii. Jego rany nie są typu "wymagają magii". Będzie potłuczony i obolały, ale da się to naprawić "po ludzku". Więc Paulina wzięła leki, opatrunki... i zabrała się do roboty.

Postać (strona gracza):

| .I_Strat. | .I_Takt. | .Specka. | .Umiejka. | .Specka Mag. | .Umiejka Mag. | .Inklinacja. | .POSTAĆ. |
|     1     |     0    |     0    |     1     |       0      |      0        |       0      |    2     |

Okoliczności:

|  .Przygotowanie. | .Pomysł. | .Skala. | .Magia. | .Surowiec. |  .OKOLICZNOŚCI. |
|          1       |     1    |    0    |    0    |      0     |        2        |

Przeciwnik (strona NPC):

| .VERSUS. | .Diff. | .Wpływ akcji gracza. | .OPOZYCJA. |
|     2    |    2   |          0           |     4      |

Wynik:

| .Postać. | .Opozycja. | .Okoliczności. | .Rzut.    | .Wynik. |
|     2    |     4      |       2        | 15: +1 W0 |   S     |

Paulina dała radę bez znieczulenia postawić nieprzytomnego maga na nogi. Po godzince się sam obudzi. Nie będzie w idealnej formie, ale... pardon. Sole trzeźwiące. Za sekundę się obudzi.

* Tor zdrowia maga: V, V, V (0)
* Tor kłopotów maga: V, 2, 1 (4) -> Ścieżka +1+1.

Ścieżki:

* **Skażony Magimed**: 4
* **Paradoksalny Duch**: 6
* **Zniszczony Konstruminus**: 0
* **Problemy Dobrocienia**: 2 + 2

Paulina dała radę obmyć go z krwi, zabandażować, zamaskować część ran... i postawiła go na nogi. Czarodziej zakaszlał i zaczął Paulinie dziękować za pomoc. Taki porządny Sebix. Paulina powiedziała magowi, że musi zgłosić ślady po konstruminusie. Czarodziej poprosił, by tego nie robiła; to był dziwny konstruminus a on sam z siebie nie robił nic nielegalnego. Nie tym razem. Mag się aż żołądkuje, że tak wyszło.

Paulina kazała mu się uspokoić. Sprawdziła po hipernecie w Świecy - jako rezydent ma prawo. Dostała odpowiedź, że żaden z ich konstruminusów nie operuje na tym terenie. Czyli to jakiś konstruminus. Nie Świecowy. A mało która gildia wykorzystuje konstruminusy.

Mag przedstawił się jako Jakub Dobrocień. Okłamał Paulinę, że "miał szczęście" i nie wiedział, gdzie jej szukać. Ta parsknęła z pogardą. Ten teleport nie miał znamion losowego teleportu. 3v2->S. Dobrocień się przyznał. Jego szef kazał mu Paulinę obserwować, gdy tylko Paulina się tu wprowadziła z opinią "lekarka, ale dziwna". Chciał zobaczyć na czym polega ta dziwność. Wyszło, że Paulina jest spoko. Nic ciekawego. Ale nadal zamontowali jej beacon teleportacyjny. Na wypadek gdyby Paulina miała silne kłopoty - szef lubi lekarzy. Nie chce, by działa się im krzywda.

Paulina zrobiła diagnostykę Dobrocienia. Wszystko działa. Dobrocień powiedział, że konstruminus ukradł mu sprzęt magimedyczny - Dobrocień miał tylko przetransportować go pociągiem i zawieźć do szefa, a konstruminus zaszedł go od tyłu i załatwił. I ukradł. Jak szef dowie się, że ktoś ukradł JEGO CZŁOWIEKOWI magimed... zrobi piekło. Serio. A w tego typu wojnach zrobi piekło. Paulina zauważyła, że nie Świecy. Dobrocień zauważył, że nie jest pewien. Oni też umieją kłamać...

Czyli, Paulina może spokojnie zniszczyć tego konstruminusa. Lub Dobrocień. Bo to nie Świecy a Paulina - mag rezydent - może dać uprawnienie zniszczenia dowolnego viciniusa. Zapytany, Dobrocień powiedział, że miał to zawieźć z Senesgradu do Orłostwa Wielkiego. Gorzej, że Dobrocień coś pamięta - ten konstruminus coś... ROBIŁ z tym magimedem. Coś zmieniał. Dobrocień pozwolił Paulinie sprawdzić swoją pamięć.

4+2v4->reroll, S. +Problemy Dobrocienia, +ktoś szukający konstruminusa

Konstruminus PRYWATNY ubrany w liberię Bankierzy zaatakował biednego Dobrocienia. Po tym spojrzał na to, co to za magimed - Paulina uznała, że to totalnie nielegalne urządzenie; coś jakby... cyborgifikacja magów? Ogólnie, niebezpieczne cholerstwo. Konstruminus zachowywał się... ludzko, nie jak konstruminus. Z punktu widzenia taktycznego nie umiał walczyć, ale miał silne ciało.

Dobrocień powiedział, że to nie ma nic wspólnego z Bankierzami... a przynajmniej on nie wie by miało. Szef mu nie powiedział nic o Bankierzach. Dobrocień jest logistykiem, nie magiem bojowym. Umie walczyć, ale nie jest w tym jakiś świetny.

* **Skażony Magimed**: 4
* **Paradoksalny Duch**: 6
* **Zniszczony Konstruminus**: 0
* **Problemy Dobrocienia**: 4 + 1

Paulinie rzuciło się też, że konstruminus walczył z nienawiścią. Tępił Dobrocienia. Chciał zabić, ale np. ludzi itp ignorował. Tępił TYLKO Dobrocienia. Paulina kazała Dobrocieniowi skontaktować się ze swoimi lekarzami; ma sprawdzić, czy u nich był. Nie. Nie było. Musiał się zaczaić gdzieś po drodze...

Paulina połączyła się z systemami Świecy by dowiedzieć się, co to za konstruminus. Czyj. Co to za liberia. Co wiadomo na ten temat (-1 surowiec). Połączyła się z Antonim Troczkiem - swoim punktem kontaktowym w Świecy (jest magiem rezydentem, ma asystenta Świecy). Powiedziała, że jak dostanie odpowiednio szybką odpowiedź, nie będzie musiała nic więcej niż porozmawiać przy odrobinie szczęścia. Antoni szybko sprawdził w danych Świecy - znalazł. Ten konstruminus jest zarejestrowany na firmę Eliksir Aerinus; przewodniczy tej firmie niejaki Apoloniusz Bankierz. Kilkunastu magów, nie działa na tym terenie. Czasem pojawiają się harvesterzy ziół, ale ogólnie na tym terenie nie działają.

Paulina zdecydowała się więc skontaktować przez hipernet z Apoloniuszem Bankierzem. Apoloniusz odebrał szybko wiadomość od maga rezydenta. 38-letni mag w szczycie mocy i potęgi.

Paulina powiedziała mu, że spotkała się z potencjalnie uszkodzonym konstruminusem Bankierzy i to jest jego konstruminus. I on zaatakował pewnego maga i ciężko go poranił. Spytała, co on tu robił i gdzie miał działać. Jak bardzo jest pewien swoich pracowników i jak jest pewien, że któryś nie nabruździł przy konstruminusie.

Z rozmowy wyniknęło co następuje:

* Apoloniusz stracił kontakt z konstruminusem niedługo przed atakiem na Dobrocienia
* Konstruminus był wyposażony w sprzęt mający pomóc mu w szukaniu ziół; miał zbierać rzadkie zioła z szerokiego terenu
* Konstruminus stracił kontakt w okolicach kolejowej wieży ciśnień; skorelowane to jest czasowo z pociągiem Dobrocienia w którym był magimed
* Apoloniusz wysyła 3-4 magów by znaleźli mu tego konstruminusa; wysłał też 3-4 innych konstruminusów. Inny model. 

Tu uruchamiają się nowe Ścieżki - te powiązane z Eliksirem.

Aktualny stan Ścieżek (wszystkie poprzednie konflikty i AKCJE idą round-robin przesuwając główne Ścieżki, były 4):

* **Skażony Magimed**: 4 +1             = 5
* **Paradoksalny Duch**: 6 +1           = 7
* **Zniszczony Konstruminus**: 0 +0     = 0
* **Problemy Dobrocienia**: 5 +0        = 5
* **Odzyskanie konstruminusa**: 0       = 0
* **Wyciszenie wszystkiego**: 0         = 0

Paulina stwierdziła, że tak dalej nie może być. Skupiła się na Dobrocieniu. Chce, by on jej coś wisiał. W końcu mu pomogła (gdy dogorywał) a nawet pomaga mu rozwiązać problem z jego przesyłką. Zdecydowała się porozmawiać z nim poważnie na ten temat. Taktyczny przeciw Paulinie. Z surowcem by wyleczyć (-1 surowiec): 3v2->S. Zgodził się. Poprosił Paulinę o wsparcie. Paulina zaznaczyła, że rozmawiała z właścicielem konstruminusa. Wie, gdzie stracił z nim kontakt. (+1 Ścieżka)

* **Dług Dobrocienia u Pauliny**: 3 +2   = 5

Z rozmowy z Pauliną Dobrocień jeszcze się przyznał, że w Senesgradzie został jego duży van. Paulina powiedziała, że ona ich zawiezie. Wzięła sprzęt medyczny, badawczy i pojechała na miejsce zdarzenia. (+1 Ścieżka)

Okolice wieży ciśnień są Niedostępne, Za Płotem, mają Silną Paradoksalną Emanację Magiczną i są Zarośnięte. Nie tyle zaklęcie co raczej... echo. Magia. Ambient. Skażenie. To chyba nie była robota maga; to czysta energia magiczna. Pływ lub leyline... 3v2->S (+1 Ścieżka). Typ energii magicznej nie wynika z zaklęcia; to jakiś rezonans magiczny. Zawiera połączenie starej magii, z okolic zniszczenia Bractwa Pary wraz z niedawną energią. Typ: Technomancja, Astralika oraz Mentalna. Paulina czuje też smak Nekromancji. Paulina (Wpływ+) wykryła też, że doszło do rezonansu między magimedem a starym zaklęciem; magimed próbował coś "odtworzyć" i wyrwało go z zasięgu w trakcie działania. A magimed działał jakoś z protezami. To jest coś protetycznego; dlatego ma technomancję. I jedyny technomantyczny obiekt jaki był w jego zasięgu... to był konstruminus, który zbierał na tym terenie zioła.

A nekromancja przyniosła echo CZEGOŚ z przeszłości. Duch nie jest osobą. I maszyna protetyczna wykorzystała konstruminusa jako PROTEZĘ dla DUCHA. I zanim to się skończyło... wyjechała z zasięgu. A konstruminus poleciał za nią...

Paulina powiedziała, że jest bardzo niedobrze. Dobrocień spytał, o co chodzi; Paulina poszła po Katalizie, Astralice, Mentalnej - skrzyżowała to z Nekromancją i Badaniami. Paulina chce dokładnie dowiedzieć się, co się dokładnie stało. Paulina chce dowiedzieć się, co kieruje tym duchem - chce odtworzyć tą scenę. Chce móc dotrzeć do tego, gdzie duch zaniesie ten magimed i co z nim zrobi...

* Ż: Co Bractwo Pary zrobili BARDZO źle?
* K: Zajmowali się parą, ale któryś z nich nie dopilnował... i była eksplozja.
* Ż: Co się stało z Bractwem Pary? Jak Kapsuła Czasu ich "zniszczyła"?
* K: Nie wszyscy chcieli się Kapsułą bawić... i nie zorientowali się, że im się para wypieprzyła. Nie było drugiego poziomu zabezpieczeń. Przez to ktoś zginął.
* Ż: Dlaczego Dobrocień jest współodpowiedzialny za zniszczenie Bractwa Pary?
* K: Zamówili coś u niego... a on albo nie dostarczył, albo dostarczył coś gorszej jakości. Nie musi być świadomie.

Tor Przeszłości: 2, 2, 1

Paulina walczy z Otoczeniem, które ma aspekty i jest ogólnie żywą energią. Niedostępne, Za Płotem, mają Silną Paradoksalną Emanację Magiczną, Zarośnięte, Historia

Cały obszar nie jest niebezpieczny, CHYBA, że się czaruje. Paulina powiedziała, że muszą tam wejść, ale Dobrocień ma NIE czarować - albo skończą jak konstruminus. Kapsuła Czasu sprawiła, że miejsce nie do końca da się wyczyścić... ale da się je spuścić z wody tymczasowo. Dobrocień ma porządny nóż i cęgi; ma przebić się przez płot i zrobić Paulinie wolną drogę. (1k20->19, sukces). Dobrocień wyciął Paulinie drogę. Już tylko Silna Paradoksalna Emanację Magiczna, Zarośnięte, Historia. (+1 Ścieżka)

Paulina poszła w Katalizę, a dokładniej Puryfikację. Trzeba zrobić sink tej energii. A przynajmniej - usunąć aspekt Paradoksalny. Zdecydowała się tymczasowo usunąć aspekt Paradoksalności okolicy. Paulina ma sprzęt jaki zabrała. Paulina stwierdziła, że to ten moment, by wytłumić rezonans - interferencją magiczną. Zdecydowała się przesłać energię magiczną w IDEĘ PRYZMATYCZNĄ tej kapsuły, by magia poszła w ten Pryzmat... i to spowoduje, że chwilowo powinno jej się udać.

Postać (strona gracza):

| .I_Strat. | .I_Takt. | .Specka. | .Umiejka. | .Specka Mag. | .Umiejka Mag. | .Inklinacja. | .POSTAĆ. |
|     1     |    0     |    0     |     0     |       1      |       1       |       0      |    3     |

Okoliczności:

|  .Przygotowanie. | .Pomysł. | .Skala. | .Magia. | .Surowiec. |  .OKOLICZNOŚCI. |
|        1         |     2    |    -1   |    -1   |     0      |         1       |

Przeciwnik (strona NPC):

| .VERSUS. | .Diff. | .Wpływ akcji gracza. | .OPOZYCJA. |
|    2     |   2    |          0           |     4      |

Wynik:

| .Postać. | .Opozycja. | .Okoliczności. | .Rzut.    | .Wynik. |
|    3     |      4     |       1        | 16: +2 W1 |   S     |

Efekt Skażenia: "7 - Efemeryda lub smakołyk"

Intencją Pauliny było usunięcie Silnej Paradoksalnej Energii Magicznej. Udało jej się - przekierowała tą energię w Pryzmatyczną ideę Kapsuły Czasu. Więcej, energia ta będzie Paulinę wzmacniać, bo rozpoznała Paulinę jako członka Bractwa Pary (live the past again). Jednak Paulina jest świadoma, że w ten sposób formuje się właśnie efemeryda... i prędzej czy później będzie trzeba się tym zająć. Bo "Kapsuła Czasu" dostanie tyle energii, że sformuje efemerydę. Czy to kolejnego ducha, czy TEGO SAMEGO ducha czy inną efemerydę... coś się pojawi. (+1 Ścieżka)

Czyli Paulina ma Korzystną Energię Skażenia. Nadal walczy z niekorzystnym, Skażonym otoczeniem, ale ma też wsparcie części tej energii.

Paulina chce odtworzenie (mentalno-astralne) wydarzeń z koncentracją na duchu. Chce zobaczyć i zrozumieć, w jaki sposób ten duch opętał konstruminusa. Oraz zobaczyć co i w jaki sposób uszkodzony jest magitech.

Postać (strona gracza):

| .I_Strat. | .I_Takt. | .Specka. | .Umiejka. | .Specka Mag. | .Umiejka Mag. | .Inklinacja. | .POSTAĆ. |
|     1     |     0    |     0    |     1     |      1       |       1       |       0      |     4    |

Okoliczności:

|  .Przygotowanie. | .Pomysł. | .Skala. | .Magia. | .Surowiec. |  .OKOLICZNOŚCI. |
|        2         |     1    |    -1   |     -1  |     0      |         1       |

Przeciwnik (strona NPC):

| .VERSUS. | .Diff. | .Wpływ akcji gracza. | .OPOZYCJA. |
|    2     |   1    |          0           |     3      |

Wynik:

| .Postać. | .Opozycja. | .Okoliczności. | .Rzut.    | .Wynik. |
|    5     |      3     |       1        | 14: +1 W0 |   S     |

* Reroll: przychodzi od Dukata ktoś szukać Dobrocienia: +1 Ścieżka nieszczęście Dobrocienia. No i +1 Ścieżka.

Paulinie się udało spojrzeć w przeszłość. Faktycznie, magimed dał radę wykorzystać konstruminusa jako protezę. Ale brakowału mu 'umysłu'. Więc zaczął szukać - i "Kapsuła Czasu", czyli Skażenie, odpowiedziało. Powołany został duch maga. Tego, który zginął podczas pracy nad Kapsułą. Duch opętał konstruminusa, zdominował AI, po czym poczuł, że magimed się oddala. Poleciał za nim - duch NIE WIE, że jest wspomnieniem. Nie wie, że czas minął. Dla ducha czas się zatrzymał. On nie jest "żywy". To konstrukt mentalno-astralny stworzony przez silne emocje. Odtworzony czarodziej, Andrzej Farnolis. Cele ducha to... wrócić do ludzkiej formy. A jak zobaczył Dobrocienia... musiał skończyć zemstę. Bo to Dobrocień dostarczył elementy, które sprawiły, że był wybuch. Duch Farnolisa to interpretuje jako "to przez niego gildia umarła". Farnolis chce też odbudować gildię. Bo w JEGO świecie ona powinna istnieć.

Magimed działał. Nie był wyłączony. Ktoś musiał go uruchomić lub go nie wyłączyć. Nie widać wyraźnie, czemu magimed działał. Gdyby nie działał, nie byłoby całego problemu. Nie było tu ingerencji innych magów. To rezonans magiczny. 

Konstruminus - a raczej duch Farnolisa - próbuje przebudować magimed. Z nędznym skutkiem. Ale wierzy, że potrafi - bo Farnolis był technomantą. Więc magimed jest w trakcie wypaczania... acz nie jest zniszczony ani zepsuty nieodwracalnie.

Zaklęcie nekromanckie Pauliny uruchomiło inne wspomnienia. Próbują wydostać się na zewnątrz i sformować inne efemerydy i byty. Chcą też czerpać energię z Pauliny i wykorzystać też JEJ pamięć i wspomnienia - powołać też efemerydy jej przeszłości. Paulina spróbowała się skupić i odeprzeć energię magiczną próbującą wbić się jej w głowę. Nie jest dobra w obronie - ale jest dobra w ataku! Jest tu efemeryda Kapsuły; więc Paulina skupiła się na WZMOCNIENIU tej efemerydy - niech efemeryda się zorientuje, że to nie tak jak powinno być i zadziała. Paulina ją materializuje by wyczyściła teren, potem ją chowa ponownie. 5+2v4->S.

Efekt Skażenia: Magi-emocjonalny wątek. Tajemnica z przeszłości. By nie wymyślać teraz (mało czasu IRL), przeniesiemy na manifestację tej efemerydy.

Paulina skutecznie przesunęła Pryzmatyczną Efemerydę tego miejsca na swoją korzyść. Efemeryda tu jest i pilnuje bezpieczeństwa Pauliny, którą rozpoznaje jako "swoją". Dobrocień próbuje po sobie nie pokazać przerażenia energią, którą właśnie wyczuł. Paulina też ma duże oczy. Ta Kapsuła jakoś połączyła się z podziemnymi Pływami; i teraz to się zamanifestowało jako wsparcie Pauliny.

Dobrze, Paulina ma za plecami do pomocy Efemerydę Kapsuły. Paulina powiedziała Dobrocieniowi, że to silniejsze niż myślała... ale na szczęście, jest po ich stronie. Paulina spytała Dobrocienia... czy to włączał? 2+2v2->S. Przyznał się - na początku, gdy odebrał to urządzenie, zrobił to. Skrzynia spadła mu na stopę więc odszedł mu paznokieć. Jak miał magimeda... użył go. ALE WYŁĄCZYŁ. A przynajmniej myślał że wyłączył... (+1 Ścieżka nieszczęście Dobrocienia). Paulina uświadomiła go, że był włączony i stąd cały problem. 

Dobra, czas się dowiedzieć GDZIE znajduje się cholerny konstruminus. Gdzie Farnolis mógł zabrać magimeda. Paulina skupiła się na nekromancji - GDZIE? Paulina podejrzewa, że to będzie gdzieś w okolicach ich dawnej siedziby. 

Postać (strona gracza):

| .I_Strat. | .I_Takt. | .Specka. | .Umiejka. | .Specka Mag. | .Umiejka Mag. | .Inklinacja. | .POSTAĆ. |
|     1     |     0    |    0     |     1     |       1      |       1       |       1      |     5    |

Okoliczności:

|  .Przygotowanie. | .Pomysł. | .Skala.        | .Magia.        | .Surowiec. |  .OKOLICZNOŚCI. |
|        2         |     1    | +1 (efemeryda) | +1 (efemeryda) |     0      |       5         |

Przeciwnik (strona NPC):

| .VERSUS. | .Diff. | .Wpływ akcji gracza. | .OPOZYCJA. |
|    2     |   2    |           2          |     6      |

Wynik:

| .Postać. | .Opozycja. | .Okoliczności. | .Rzut.     | .Wynik. |
|    5     |      6     |       5        | 2,7: -2,-1 |   S, S  |

Paulina się skupiła; spróbowała to zrobić, ale energia zaczęła jej się wyślizgiwać z rąk. Zachwiała się, ale za drugim razem udało jej się złapać odpowiednie wątki. Ewentualne kontrataki i działania inne zostały zatrzymane przez Efemerydę Kapsuły. (+2 Ścieżka)

I Paulina poznała lokalizację oraz dalsze działania. Konstruminus potrzebuje ludzkiego ciała - więc kogoś musi porwać. Potem, skupi się na muzeum pary: miejscu, które wraz z resztą Bractwa Pary zbudowali i złożyli. I tam spróbuje wykorzystać magimed by scalić konstruminusa i człowieka w jedną całość, z dominującym umysłem Farnolisa. To jest, to jest PLAN. Ale konstruminus nie do końca czaruje tak jak Farnolis. A magimed jest bardzo zaawansowanym urządzeniem. To powoduje, że najpewniej coś wybuchnie...

+8 Ścieżek, +2 ekstra Dobrocienia: 2,2,0,0,0,1,1,0,1,1. Ekstra tor długu Dobrocienia (2): 1,1

* **Skażony Magimed**: 5 +2+1             = 8
* **Paradoksalny Duch**: 7 +2+0           = 9
* **Zniszczony Konstruminus**: 0 +0       = 0
* **Problemy Dobrocienia**: 5 +0+2        = 7
* **Odzyskanie konstruminusa**: 0+0       = 0
* **Wyciszenie wszystkiego**: 0+1         = 1
* **Dług Dobrocienia u Pauliny**: 5+2     = 7

Paulina zdecydowała, że to jest moment, kiedy trzeba stąd odejść. Jak Paulina się oddaliła, Efemeryda Kapsuły zaczęła powoli, powoli zanikać... ale przyszedł mag. Felicja Szampierz. Zażądała wyjaśnień od Dobrocienia; nie ma magimeda. Czemu Dobrocień fraternizuje się z jakąś czarodziejką, gdy magimed nie dotarł na czas?

Paulina zauważyła, że jest tu rezydentką. Zapytała co Felicja tu robi. Ta się przedstawiła jako windykator; Dobrocień miał coś dostarczyć na czas i nie dostarczył, więc Felicja chciała sprawdzić, czy wszystko w porządku. Lub. Paulina powiedziała, że Dobrocień odniósł poważne obrażenia podczas ataku. A teraz współpracują, by odzyskać utracony magimed. 

* Dobrocień, czemu się nie skontaktowałeś? - Felicja, groźnie
* Bo był nieprzytomny przez większą część czasu? - Paulina, zimno

Paulina próbuje przedstawić to jako "oberwał podczas transportu, próbował uzyskać pomoc, poprosił o wsparcie i wszystko wskazuje, że są blisko rozwiązania problemu."

3+2v4->S, podniesiony Wpływ.

* **Problemy Dobrocienia**: 7-3         = 4
* **Dług Dobrocienia u Pauliny**: 7+3   = 10

Dobrocień dostał kluczyki do auta Pauliny, Paulina powiedziała Felicji, że jak chce pomóc odzyskać magimeda - może jechać z nimi. Felicja zmilczała lekką obrazę, ale się zgodziła - magimed jest ważny dla Dukata. A Paulina wysłała Apoloniuszowi, że wie co i gdzie - poprosiła go o wysłanie swoich ludzi przy Muzeum Pary; tam zorganizują wspólną akcję.

* **Skażony Magimed**: 8 +1             = 9
* **Paradoksalny Duch**: 9 +0           = 9

Trzech magów pracujących dla Apoloniusza. Felicja i Dobrocień. I Paulina. W sumie - sześciu magów. Paulina pobieżnie wyjaśniła Felicji, że tam są magowie Bankierzy, którym też ukradziono konstruminusa. Wyjaśniła, że to interakcje między dziwnymi rzeczami doprowadziły do takiego a nie innego wyniku. Więc to NIE jest ich wina i nie jest winą Dobrocienia. Aha, Paulina dowodzi ;-).

Ani ludziom Apoloniusza ani Dukata się to nie podoba - ale to jedyny sposób, by to zadziałało.

Magowie wpadli i wyłączyli konstruminusa, który zaczął powoli nabierać cech ludzkich (a lokalny porwany człowiek - cech technomantycznych). Duch został błyskawicznie wyegzorcyzmowany a Paulina zatrzymała Felicję, która chciała wyjąć baterię podczas działania magimeda...

Paulina spróbowała podejść do wyłączenia tego magimeda. Złapała Dobrocienia, który CZYTAŁ instrukcję i ma technomancję... a sama zauważyła, że magimed spiął się z Efemerydą Kapsuły. Dobrocień zaczął tłumaczyć, jak to działa - ale ten magimed jest już Skażony. Na podstawie swojej wiedzy i technomantycznej wiedzy Dobrocienia Paulina spróbowała dostosować procedurę wyłączenia ustrojstwa.

Tor: 2, 2, 1. Magimed jest Skażony, Uszkodzony, Eksperymentalny, Połączony z Efemerydą Kapsułą.

Paulina wie z Efemerydy, co duch zrobił (duch nie jest osobą, jest echem). Od Dobrocienia ma instrukcję. Ma swoją wiedzę o magimedach.

Postać (strona gracza):

| .I_Strat. | .I_Takt. | .Specka. | .Umiejka. | .Specka Mag. | .Umiejka Mag. | .Inklinacja. | .POSTAĆ. |
|     1     |    0     |    0     |     1     |       1      |       1       |       0      |    4     |

Okoliczności:

|  .Przygotowanie. | .Pomysł. | .Skala. | .Magia. | .Surowiec. |  .OKOLICZNOŚCI. |
|        2         |    1     |    0    |    1    |     0      |         4       |

Przeciwnik (strona NPC):

| .VERSUS. | .Diff. | .Wpływ akcji gracza. | .OPOZYCJA. |
|    2     |   3    |           2          |     7      |

Wynik:

| .Postać. | .Opozycja. | .Okoliczności. | .Rzut.    | .Wynik. |
|     4    |      7     |      4         | 13: +1 W0 |    S    |

Paulinie udało się wyłączyć podstawowe elementy magimeda. Musiała odeprzeć katalityczne wyładowania i energię syntezującą ją z tymi innymi artefaktami z pary (4+2v4->R); skończyło się tym, że Paulina jest lekko poparzona i Skażona magicznie; odparła atak, ale musiała go wysadzić. Zasyczała z bólu, ale kontynuowała - teraz musi odpiąć Efemerydę. Paulina wydała jej sygnał, korzystając z rozpoznania, żeby ta się wycofała.

Postać (strona gracza):

| .I_Strat. | .I_Takt. | .Specka. | .Umiejka. | .Specka Mag. | .Umiejka Mag. | .Inklinacja. | .POSTAĆ. |
|     1     |    1     |    0     |     0     |       1      |       1       |       0      |    4     |

Okoliczności:

|  .Przygotowanie. | .Pomysł. | .Skala. | .Magia. | .Surowiec. |  .OKOLICZNOŚCI. |
|        2         |    2     |    0    |   -1    |     0      |         3       |

Przeciwnik (strona NPC):

| .VERSUS. | .Diff. | .Wpływ akcji gracza. | .OPOZYCJA. |
|    2     |   2    |           0          |     4      |

Wynik:

| .Postać. | .Opozycja. | .Okoliczności. | .Rzut.    | .Wynik. |
|     4    |      4     |      3         | 19: +2 W1 |    S    |

Efekt Skażenia: 13 - Ta sama intencja, inny czar

Efemeryda wessała do siebie CAŁĄ magię z okolicy. Muzeum Pary, inni magowie, echo Farnolisa, inni magowie... wszyscy padli, jak ścięci. Człowiek w części Skażony został wyczyszczony. Konstruminus został wyczyszczony i włączyły się awaryjne subrutyny regeneracji. Nagle - magimed się wyłączył i zapadła "cisza". Wow.

Magowie popatrzyli na siebie z niedowierzaniem...

**Ścieżki Fabularne:**

* **Skażony Magimed**:                    = 9
* **Paradoksalny Duch**:                  = 10
* **Zniszczony Konstruminus**:            = 0
* **Problemy Dobrocienia**:               = 4
* **Odzyskanie konstruminusa**:           = 0 DONE
* **Wyciszenie wszystkiego**:             = 1
* **Dług Dobrocienia u Pauliny**:         = 10

Efekty Skażenia:

* 07 - Efemeryda lub smakołyk
* 11 - Magi-emocjonalny wątek. Tajemnica z przeszłości.

**Interpretacja Ścieżek:**

Magimed został poważnie Skażony i uszkodzony; szczęśliwie, nic, czego eksperci Dukata nie są w stanie naprawić. Zwłaszcza po działaniu efemerydy. Dobrocień dostał opiernicz, ale Felicja się za nim lekko wstawiła i zaznaczyła sytuację. Na opierniczu Dobrocienia się skończyło; nic poważnego. Za to Dobrocień uważa, że ma duży dług u Pauliny - która się z nim zgadza w tej kwestii.

Andrzej Farnolis został wyegzorcyzmowany. Nie do końca. Efemeryda Kapsuły wchłonęła go ponownie, przetwarzając dodatkową energię. W połączeniu z pozostałymi Skażeniami Paulina wie, że on wróci. Nie wie jak, w jakiej formie (poza tym, że efemeryda) i kiedy - ale wróci. Tymczasowo Efemeryda Kapsuły wycofała się w Pryzmat.

Apoloniusz Bankierz odzyskał swojego konstruminusa. Jest zadowolony ze współpracy z lokalną rezydentką...

# Progresja

* Paulina Tarczyńska: Jakub Dobrocień, logistyk Dukata ma u niej dług za sprawę z magimedem i uratowanie życia...

# Streszczenie

Przez rezonans między starym rytuałem a magitechem medycznym konstruminus Bankierza został protezą dla ducha. Ów duch próbował wrócić do świata żywych. Paulina zmontowała koalicję z siłami Dukata i Bankierza, znalazła lokalizację starego rytuału i dali radę powstrzymać ducha (konstruminusa) bez szkód ekonomicznych i ludzkich.

# Zasługi

* mag: Paulina Tarczyńska, mag-rezydent i lekarz. Ratuje Dobrocienia i po zrozumieniu sprawy ducha kradnącego magitech medyczny montuje koalicję by ducha rozłożyć i by nikomu nic się złego nie stało. Też ekonomicznie ;-).
* mag: Jakub Dobrocień, mag Transportu i Technomancji; logistyk pracujący dla Dukata. Duch ukradł mu magitech medyczny i ciężko poranił. Uratowany przez Paulinę, ma u niej dług.
* czł: Zofia Murczówik, 19-latka mówiąca Paulinie, że nie może być w ciąży... a jest. Gdy była u Pauliny, wpadł na nią ranny Dobrocień... no i jest plotkarką.
* mag: Apoloniusz Bankierz, uczciwy biznesmen robiący w półproduktach alchemicznych; jego konstruminusa opętał duch. Współpracuje z Pauliną.
* vic: Andrzej Farnolis, kiedyś mag a dzisiaj duch, który opętał konstruminusa; z gildii Bractwa Pary. Wyegzorcyzmowany zanim zdołał Coś Zrobić z magitechem medycznym.
* mag: Felicja Szampierz, pomiata Dobrocieniem z ramienia Dukata; Paulina ją przekonała, że Dobrocień niewinny. Pomogła jak była w stanie. 
* vic: Efemeryda Senesgradzka, z której magimed Dukata "wyrwał" Andrzeja Farnolisa by umieścić go w konstruminusie. Efemeryda wspierała Paulinę w reasymilacji Farnolisa.

# Lokalizacje

1. Świat
    1. Primus
        1. Mazowsze
            1. Powiat Pustulski
                1. Krukowo Czarne
                    1. Gabinet Pauliny, gdzie - jak się okazuje - jest beacon teleportacyjny ludzi Dukata.
                1. Senesgrad, stare miasto pełniące kiedyś rolę węzła komunikacyjnego. Nadal słynie ze stacji przeładunkowych
                    1. Dworcowo
                        1. Kolejowa wieża ciśnień, zarośnięta i niebezpieczna; jest tam stara magia od czasów rytuału z "Kapsułą Czasu".
                        1. Stacje przeładunkowe, gdzie Dobrocień został okradziony z magitecha medycznego przez opętanego konstruminusa.
                    1. Zalesie
                        1. Muzeum Pary, kiedyś zarządzane przez Bractwo Pary, teraz w rękach ludzi; tam miało dojść do Wskrzeszenia Maga.

# Czas

* Dni: 1

# Frakcje:

### Rodzina Dukata

**Koncept na tej misji:**

Potężna grupa przestępcza ściągająca eksperymentalne maszyny medyczne.

**Scena zwycięstwa:**

* Magimed zostaje odzyskany przez siły Dukata
* Bankierz zapłaci odszkodowanie za dokonane przez jego konstruminusa zniszczenia

**Dążenia:**

* Zapewnienie bezpieczeństwa i względnego dobrobytu wszystkim członkom Rodziny
* Zdobycie jak najlepszych narzędzi medycznych magitechowych w okolicach swojej kwatery głównej
* Wspieranie lekarzy, zwłaszcza eksperymentalnych i pomaganie im na danym terenie.

**Ścieżki:**

* Zniszczony Konstruminus
* Problemy Dobrocienia


### Eliksir Aerinus (biznes Apoloniusza)

**Koncept na tej misji:**

Biznes, próbujący dostarczać najlepsze półprodukty alchemiczne.

**Scena zwycięstwa:**

* Konstruminus zostaje przechwycony i odzyskany.
* Dowiedzenie się, co się stało by zabezpieczyć przed przyszłymi działaniami tego typu.
* Opinia firmy jest bezpieczna.

**Dążenia:**

* Poszerzenie swojego biznesu, ale w sposób kontrolowany, bezpieczny, bez utraty kontroli
* Zapewnienie środków i bezpieczeństwa swojej rodzinie i zapewnienie sobie dostatku
* Optymalizacja procesów biznesowych oraz dalsze postępy w dziedzinie biomagicznych składników

**Ścieżki:**

* Odzyskanie konstruminusa
* Wyciszenie wszystkiego


### Andrzej Farnolis

**Koncept na tej misji:**

Duch, echo emocjonalno-mentalne maga Bractwa Pary, który "chce wrócić"

**Scena zwycięstwa:**

* Przechwycony magimed pozwolił mu na przebudowanie konstruminusa i człowieka w nową istotę
* Dobrocień, który "współodpowiada" za zniszczenie Bractwa Pary, zostaje zabity LUB staje się podstawą ciała Farnolisa

**Dążenia:**

* Odbudowa Bractwa Pary
* Zemsta na tych, którzy zniszczyli Bractwo Pary
* Powrót do świata żywych

**Ścieżki:**

* Skażony Magimed
* Paradoksalny Duch


# Narzędzia MG

## Cel misji

1. Weryfikacja nowego podejścia do magii - szkoły magiczne
2. Losowe Efekty Skażenia
3. Weryfikacja frakcji, czyli ewolucji wariantów rzeczywistości + fronty
4. Weryfikacja nowego poziomu granulacji konfliktów: tor-5.
5. Weryfikacja meta-torów, czyli ścieżek fabularnych
    1. 3 konflikty / dzień / gracza
    2. Tory z konfliktów się przenoszą na meta-ścieżki
    3. Mechanizm eskalacji (brutalizacji) meta-ścieżek
    4. Patrz: [Notatka 170520](/rpg/mechanika/notatka_170520.html)

## Po czym poznam sukces

1. Szkoły magiczne
    1. Magia staje się potężniejsza; szersza, feel jest bardziej jak ElMet.
    2. Postać Pauliny jest dobrze odwzorowana przez sztywne szkoły magiczne.
    3. Widzimy GRANICE działania Pauliny. Wiemy, co może zrobić magią a co nie.
2. Efekty Skażenia
    1. Nadal są interesujące. 
    2. Nadal nie niszczą fabuły.
    3. Wzbogacają misję: albo ciekawsze wyniki albo nowe wątki.
3. Frakcje
    1. Świat żyje; zawsze wiem, do czego dążą poszczególne strony aktywne
    2. Bookkeeping nie jest morderczy
    3. Gracze są w stanie wejść w interakcje z frakcjami i im przeszkadzać lub pomóc
4. Granulacja konfliktów
    1. Mamy różne typy konfliktów: 'jednostrzałowe' i bardziej interesujące, wieloetapowe
    2. Istnieje możliwość większego wykorzystywania Okoliczności podczas konfliktów
    3. Mniejsze wychylenia wynikające z pojedynczego testu - przegranie testu nie uniemożliwia osiągnięcia celu.
5. Ścieżki Fabularne
    1. Określenie postępu rzeczywistości w określonym kierunku wraz z płynięciem czasu do przodu

## Wynik z perspektywy celu

1. Szkoły magiczne
    1. Kić: Mam wrażenie, że ta magia jest FAJNA. 
    1. Żółw: Z pewnością magia jest OBECNA. Działać dalej.
2. Efekty Skażenia
    1. Kić: Efekty Skażenia powodują, że się zastanawiasz czy używać magii, ale nie zniechęcają.
    1. Żółw: ES generują ciekawe wyniki. De facto złożyły nową misję. A na tej miały znaczenie.
    1. Kić: Trzymałabym się tabelki z ES. Mniej obciąża MG, plus ten element uznaniowości, który przeciętnemu MG daje szansę na standardowe patologie znika :D. 
3. Frakcje
    1. Kić: Z mojego punktu widzenia... w trakcie sesji nie widać tego jak nie myślisz w ten sposób. Jak zaczynam zauważać Frakcje, i tak o nich już myślę, ale to nie przeszkadza.
    1. Żółw: Zdecydowanie Frakcje pomogły mi w złożeniu sensownych Ścieżek ORAZ w tym, że mogłem dogenerować co powinno się dziać. Np. Felicja. Felicja by się nie pojawiła bez Frakcji.
    1. Żółw: Bookkeeping nie był morderczy ;-).
4. Granulacja konfliktów
    1. Żółw: Jednocześnie wprowadziłem Okoliczności Scen (aspekty nadawane scenom) i Granulację Konfliktów. To trochę zakłóciło obserwację.
    1. Kić: Granulacja konfliktów była OK. Nie załatwialiśmy wszystkiego 1 testem. Te mini-tory są fajne; nie trzeba było się "męczyć" czy "przebijać".
    1. Żółw: Diabli siedzą w implementacji tych konfliktów. Np. tor-7 dla nudnego konfliktu bez aspektów. To by imo zawiodło.
    1. Kić: Aspekty scen... pozwalają na to by w nie uderzać i je zmieniać. Jak jest dużo aspektów, tor powinien być krótszy.
    1. Żółw: Aye, tak to zaimplementowałem tym razem. Był "feel" bossfighta przy tej efemerydzie? Tam były 4 aspekty i tor-3 (2,2,1).
    1. Kić: Mało HP, duży pancerz ;-). To nie było coś, do czego można podejść "o, sialala, dup". 
5. Ścieżki Fabularne
    1. Żółw: Wątki na Ścieżka-10 i Ścieżka wypełniana Wpływem postaci lub 0-1-2. To była implementacja. Zadziałała, chociaż może wymagać kilku innych wariantów. Teraz szliśmy round-robin; warto sprawdzić round-robin dla KAŻDEJ Frakcji na KAŻDĄ akcję gracza. ALE. Wtedy Frakcja musi mieć więcej niż 1 Ścieżkę. Inaczej będzie za dużo.
    1. Kić: Dobrze jest, że jakaś frakcja osiągnęła cele; inaczej byłoby słabe. Nadal Ścieżki są lekko stresujące - robienie akcji sprawia, że "coś" się dzieje w tle.
    1. Żółw: Czas nie płynął do przodu. Misja zamknęła się w jednym dniu mimo 15+ konfliktów.
    1. Kić: Nie wiem czy ograniczenie konfliktów na dzień jest potrzebne. Na logikę świata... czasem zrobisz 3 rzeczy/dzień, czasem 15.
    1. Żółw: Przy akcja-reakcja (akcja gracza, akcja Frakcji) czas nie jest taki ważny. Bo rzeczy się i tak Dzieją. To zadziałało.
    1. Kić: Nie miałam surowców bardzo szybko; gdyby np. poszukiwanie było trudniejsze (potrzeba mi surowca), najpewniej bym przerotowała dzień.
    1. Żółw: Jeszcze jedna obserwacja; mogłem dodać Frakcji siły aktywne. Np. Felicja, Dobrocień to siły Dukata a Apoloniusza to "magowie". Wtedy na akcję gracza przypada akcja każdej siły we Frakcji a nie ścieżki. Wtedy, wyłączenie siły z Frakcji bardzo osłabia tą Frakcję. To mogłoby być lepsze.
    1. Kić: Chyba tak. To jest sposób by kontrolować postępy Ścieżek. 

Ogólnie:

* Żółw: Bardzo udana misja, a to co ciekawe: ta misja wygenerowała się sama. Same narzędzia sprawiły, że mamy to, co mamy. WAŻNE: Wpływ-2 a nie Wpływ-3 zostaje. Ta misja udowodniła, że Wpływ-3 jest niekoniecznie dobrym pomysłem. Tak jak jest, były konflikty o podniesionym Wpływie. Inaczej... mogłoby ich nie być i przebijanie się przez tory byłoby... brutalnie nudne.
* Kić: Takie sesje mi pasują; nie chodzi o fabułę... nie miałam wrażenia, że jestem zarąbana a nie było nudy. TRZEBA wymyślić efektywniejszy sposób zapisywania sesji... 

## Wykorzystane tabelki

Postać (strona gracza):

| .I_Strat. | .I_Takt. | .Specka. | .Umiejka. | .Specka Mag. | .Umiejka Mag. | .Inklinacja. | .POSTAĆ. |
|           |          |          |           |              |               |              |          |

Okoliczności:

|  .Przygotowanie. | .Pomysł. | .Skala. | .Magia. | .Surowiec. |  .OKOLICZNOŚCI. |
|                  |          |         |         |            |                 |

Przeciwnik (strona NPC):

| .VERSUS. | .Diff. | .Wpływ akcji gracza. | .OPOZYCJA. |
|          |        |                      |            |

Wynik:

| .Postać. | .Opozycja. | .Okoliczności. | .Rzut.    | .Wynik. |
|          |            |                | xx: +x Wx |  S / F  |

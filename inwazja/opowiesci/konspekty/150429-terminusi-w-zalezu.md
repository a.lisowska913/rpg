---
layout: inwazja-konspekt
title:  "Terminusi w Zależu"
campaign: swiatlo-w-zalezu-lesnym
gm: żółw
players: kić, dust
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [150427 - Kult zaleskiego anioła (An, Mo)](150427-kult-zaleskiego-aniola.html)

### Chronologiczna

* [150427 - Kult zaleskiego anioła (An, Mo)](150427-kult-zaleskiego-aniola.html)

## Punkt zerowy:

W odpowiedzi na dziwny obraz Andromedy zostało wezwanych dwóch bardzo młodych terminusów (bardziej kandydaci niż pełnoprawni); rodzeństwo. Ich zadanie jest bardzo proste:
- czy tam się dzieje coś dziwnego
- rozwiązać ten problem

Ich przełożony nie uważa tej sprawy za priorytetową.

Olga Żmijucha nie ucieszyła się rozbiciem witraża, ale pracuje nad tym, by witraż wrócił do normy (załatwiła to u "swojego" przedsiębiorcy z branży metalurgicznej, Rufusa Etera, ambitnego biznesmena i polityka). Sprowadzane są już nowe elementy witrażu, dodatkowo Eter załatwił jej czterech osiłków.

Tymczasem Aneta Kosicz utraciła dostęp do energii anioła i nie jest już w stanie uzdrawiać dotykiem. Aneta jest coraz mniej istotna i coraz mniej poważana, co zmusiło ją do szukania innych źródeł wiedzy i częstych podróży do Anielskiego Dworu. 
Kult próbuje wciągnąć nowe osoby przez inkorporację, zaczynając min. od młodziutkiej Karoliny Błazoń.

Ż: Dlaczego nie pamiętacie specjalnie niczego przed misjami przełożonego? </br>
D: Testowany jest nowy model terminusa. </br>
Ż: Co uzależnia Was od przełożonego? </br>
K: Jesteśmy lojalni i nie chcemy go opuścić. Dba o nas i nam pomaga. </br>
Ż: Jakie stosunki macie z waszym przełożonym? </br>
D: Jest naszym guru, mentorem i opiekunem. </br>
Ż: Jaką przykrywkę macie przed magami spoza waszej kabały? </br>
K: Dla nich jesteśmy magami z Rodziny Świecy. </br>
Ż: Jakie są przewidywania do waszej specjalnej "lepszości"? Co próbowali zrobić? </br>
D: Uważają, że ominie nas choroba terminusów. </br>
Ż: Co magicznego znalazła w Anielskim Dworze Aneta (funkcjonalność)? </br>
K: Amulet szlachcica. </br>
Ż: Na czym bardzo mocno zależy księdzu (boi się stracić / chce uzyskać)?  </br>
D: Chce pozyskać artefakt, który uznaje za relikwię. </br>
Ż: Czym kierował się czarodziej, który przybył tu przed wami? </br>
K: August Bankierz zwrócił po raz pierwszy uwagę na Andromedę i jej mniej więcej tu szukał. </br>

## Misja właściwa:

**Faza pierwsza: przybycie do Zależa**

Pensja Błazonia. Zespół się tam rozłożył. Wiedzą o jednym lokalnym magu ale nie wiedzą kto nim jest i do jakiej gildii należy. Zespół ma pełne prawo się bronić; są terminusami w prawie, walczącymi o to, by skorygować potencjalne złamanie Maskarady.

Janek poszedł do kawiarenki "Ukrop" podsłuchać i porozmawiać z miejscowymi. Tam dorwał leśniczego i policjanta i udało mu się wkraść w ich łaski. Leśniczy ostrzegł Janka, by ten nie chodził do lasu. Niestety, Jankowi nie udało się dowiedzieć niczego szczególnego, więc użył magii mentalnej przeciwko ludziom. Dowiedział się od policjanta, że tamten był przeskanowany magią mentalną i zmieniono mu pamięć; dowiedział się, że zrobiła to Olga Żmijucha (widział ją). Doszedł też do tego, że lokalni ludzie są w różnych fazach Skażenia... niestety, tym zaklęciem poinformował zarówno Anetę jak i Olgę o swojej obecności.
Janek dowiedział się też, że w lesie leśniczy widział Pawła Frannę, już po jego śmierci.

Tymczasem Małgorzata znalazła w jednej z książek historycznych notatki Pawła Franny. Stamtąd dowiedziała się o Anecie Kosicz i o planach historyka. Notatki nie wyglądały jak notatki pedofila, raczej jak notatki kogoś martwiącego się o podopieczną.
Poza tym Małgorzata w bibliotece znalazła następujące interesujące informacje:
- chatka w środku lasu (chatka wiedźmy) w której zginął mag.
- krypta na cmentarzu, w której znajdowało się sekretne miejsce. Tam jest ryngraf z wygrawerowanym aniołem; służy do sprzężenia panującego Zaleskiego z aniołem.
- stary ołtarz ku czci anioła światłości w lesie; ołtarz nie został wzniesiony przez chrześcijan.

**Faza druga: kościół i witraż**

Aneta Kosicz poczuła się zagrożona tym, że jej moc nie rośnie i ludzie się od niej odwracają. Zdecydowała się wezwać na pomoc swoich kultystów i pod osłoną nocy pójść do kościoła i zmusić księdza do tego, by uznał ją za świętą, by dołączył do nich...
Olga Żmijucha stwierdziła, że obecność innego maga jest bardzo niebezpieczna. Czyżby to on znalazł poszukiwany przez nią ryngraf? Wysłała hrabiego oraz Pawła do lasu by szukali dalej ryngrafu a sama zaprosiła do siebie Jana; chce dowiedzieć się czy to on ukradł jej ryngraf. Jeżeli tak, ona jest gotowa do walki...
Tymczasem ludzie Olgi załatwili z księdzem - oni dadzą księdzu skopiowany ryngraf (ksiądz wierzy, że jest prawdziwy) w zamian za co ksiądz pozwoli im wstawić nowy witraż.

Do Jana przyleciał mechaniczny ptak, zrobiony bardzo misternie i dokładnie. Zostawił mu na stoliku kopertę i odleciał. Jan jednak nie odpuścił tak łatwo - napromieniował mechanicznego ptaka by móc śledzić jego ślady. Owszem, wysyłająca ptaka Olga od razu się zorientuje, ale Jan ma dodatkowe informacje. Koperta jest bezpieczna od magii; Jan otworzył i przeczytał list. Tam - zaproszenie. Odręcznie napisała.

Jan zdecydował się odwiedzić Olgę, która nie była szczęśliwa, że ten ptak był napromieniowany... niezły początek spotkania.

Małgosia była poproszona przez Jasia o asekurację. Niestety, Małgosia zobaczyła jak szklarz jedzie w kierunku na kościół, z zapasowymi elementami witraża. Stwierdziła, że warto uniemożliwić postawienie tego witraża na jakiś czas... więc zdecydowała się zaklęciem rzucić weń problemy błędnicze. 14v12. Remis: szklarz dostaje problemów z błędnikiem. Nie jest w stanie zrobić tego witraża. Uspokojona Małgosia poszła wesprzeć Jana. W międzyczasie do szklarza wezwano Anetę, która go Uzdrowiła mocą anioła, wypalając zaklęcie Małgosi. Szklarz zdąży wmontować witraż a Aneta odzyska moc Anioła i możliwość jego wezwania.
Małgosia poczuje, że jej zaklęcie zostało wypalone.

Jan otwarcie, na tarczach zbliżył się do chaty Olgi. Olga nie dała rady przekonać Jana by ten wszedł do środka, więc rozmowa była bardzo zabawna - Jan na zewnątrz, Olga w środku. Czarodziejka nie chce współpracować z Janem; jednak zmuszona powiedziała, że wie o aniele (to regionalny vicinius/konstrukt ze światła) który jest dla niej źródłem energii. Nośnikiem Anioła jest Aneta Kosicz, która nie jest pod kontrolą Olgi. Olga przyznała też, że to ona stoi za przywróceniem Pawła Franny do nie-życia. 
By porozmawiać z historykiem, Jan wszedł do jej domostwa. Poziom aury magicznej tam jest tak silny, że jego aż oślepia; ta czarodziejka musi być dużo bardziej Skażona niż on. Odmówiła pójścia z Janem; ale Jan ją przycisnął i Olga wpadła w histerię.

Opanowanie histerii Olgi zajęło dobre kilka godzin, ale jak minęło, pojawiło się kilka nowych informacji:

1. Zaginął jej ryngraf przez działanie Augusta Bankierza. Ryngraf pozwala jej kontrolować hrabiego Onufrego Zaleskiego i pośrednio łączy z aniołem; bez ryngrafu anioł nie słucha poleceń (co stawia Anetę na BARDZO uprzywilejowanej pozycji).
1. August Bankierz pojawił się tu znienacka i przydybał Olgę w lesie. Tam doszło do wymiany zdań i pyskówki; Olga się panicznie wycofała ale Bankierz przechwycił ryngraf (Olga nic nie zauważyła). I teraz Olga szuka ryngrafu swoimi agentami.
1. Olga wykorzystuje to wszystko do akumulacji mocy. Ma obsesję na tym punkcie.
1. Olga JESZCZE kontroluje hrabiego. Z czasem kontrola zaniknie i na wolności będzie bardzo niebezpieczny upiór miecza.

Małgorzata zdobyła informacje o Auguście Bankierzu. Okazało się, że jego nie powinno w ogóle tu być! Też uczeń na terminusa, ale bez specjalnych osiągnięć i teraz nie powinien tu być; nie ma podkładki papierowej. Innymi słowy, wtf.

W czasie jak Małgorzata i Jan zajmowali się Olgą, Aneta Kosicz dostała rozłożoną na starym ołtarzu Anioła Karolinę Błazoń. Aneta odprawiła na Karolinie rytuał światłości, zmieniając Karolinę w lojalną i szczęśliwą członkinię kultu Anioła.

Jan otworzył kontakt z Augustem Bankierzem. Chce z nim porozmawiać. Ale wpierw Małgosia wyszukała grupę specyficznych dokumentów w hipernecie by wymusić na Auguście Bankierzu współpracę (casus kolizji terminusów). Udało jej się i Jan ma #3 tool bonus przeciwko Augustowi.
August wyśpiewał wszystko. Powiedział, że coś zwróciło jego uwagę (hint: obraz Andromedy) i pojawił się w Zależu Leśnym (zboczył o 140 km z kursu 5h przed akcją). Tu spotkał Olgę w lesie i po kłótni powiedział że nie jest na służbie. Olga go zaatakowała. August obronił się Defensywną Teleportacją Nicole Archer... i teleportował KAŻDY element ekwipunku Olgi w inne miejsce. Łącznie z ryngrafem. Po czym stwierdził, że chyba trochę nabroił i zwiał na akcję (która trwa do teraz).

No tak... czyli ryngraf GDZIEŚ jest w lesie...

**Faza trzecia: eskalacja zniszczeń**

W nocy Jan się obudził słysząc głośne trzaśnięcie drzwiami na dole. To Franciszek Błazoń, jego żona i córka. Wszyscy przebrani w strój kultystów; wszyscy idą w kierunku kościoła. Jan i Małgorzata postanowili ich śledzić.
Niedaleko kościoła spotkanie kultystów; jest ich kilkudziesięciu. To dużo. Przed nich wyszła Aneta Kosicz; jej oczy płoną światłem. Jest naprawdę głęboko Skażona. Aneta wydała rozkaz, by poszli do księdza; czas nawrócić księdza na jedyną prawdziwą wiarę. Słysząc to, kandydaci na terminusa w długą, na plebanię. Biorą księdza, pakują go w jakiś strój i natychmiast ewakuować. Jan rzucił iluzję, że ksiądz cały czas tam leży na łóżku, kupując cenne minuty.
Udało się wycofać księdza po morderczym przebijaniu się przez las i unikaniu kultystów. W końcu go ukryli - pod łóżkiem terminusa (dokładniej: Jana).
Nad ranem usłyszeli trzaśnięcie drzwiami. To rodzina Błazoniów wróciła. Niepocieszeni - nie znaleźli księdza. Terminusi usłyszeli, że następna noc to będzie Noc Nawrócenia.
Aha.

**Faza czwarta: przed Nocą Nawracania**

Niestety, jedynym sensownym rozwiązaniem - zdaniem terminusów - była egzekucja Anety. Najpewniej (widząc, że ta przyzywa Anioła wedle swej woli) ona ma już ryngraf.
I faktycznie - terminusi usunęli Anetę. Padła od jednego zaklęcia bojowego. Na jej piersi znaleźli ryngraf, który wystarczyło rozmagicznić. Dzięki temu Olga straciła kontrolę nad aniołem a hrabia Onufry Zaleski został odesłany.
Małgorzata poinformowała Augusta Bankierza o sytuacji oraz poinformowała swego przełożonego o tym, że zadanie zostało wykonane. Ostrzegła też, że mają tu niestabilną i potencjalnie niebezpieczną czarodziejkę.

**Faza piąta: Noc Nawracania, otwarta wojna**

[nie doszliśmy tu, na szczęście; misja skończyła się w fazie 4]

# Lokalizacje:

1. Świat
    1. Primus
        1. Podlasie
            1. Powiat Orłacki
                1. Zależe Leśne
                    1. Serce
                        1. pensja Błazonia
                        1. kawiarenka Ukrop
                        1. kościół
                    1. Wał Kartezia
                        1. dworek pana
                        1. cmentarz
                            1. krypta rodu Zaleskich
                    1. Koniowie
                        1. siedziba czarodziejki
                        1. dawna chata lokalnej wiedźmy
                    1. Las Gęstwiński
                        1. ołtarz Anioła

# Zasługi

* mag: Jan Grimm jako kandydat na terminusa, którego ulubioną metodą działania jest dominacja i zastraszanie.
* mag: Małgorzata Grimm jako kandydatka na terminusa, która przeszukuje archiwa i dokumenty umożliwiając Janowi jak najlepsze działanie.
* czł: Dariusz Remont jako leśniczy; widział zmarłego Pawła Frannę w lesie i ostrzegł przed tym Jana Grimma.
* czł: Kamil Gurnat jako policjant; zdominowany przez Jana Grimma przekazał mu kluczowe informacje o tym, że lokalnym magiem jest Olga Żmijucha.
* mag: Olga Miodownik jako opętana obsesją akumulacji mocy czarodziejka mieszkająca w chatce z nadmiarem Skażenia; paranoiczna i przerażona. Eks-patolog sądowy.
* vic: Aneta Kosicz jako kultystka Anioła Światła, która zaszła tak daleko swym Skażeniem i ambicją, że jedyną opcją uratowania sytuacji była jej egzekucja. KIA.
* vic: Onufry Zaleski jako hrabia, upiorny rycerz w służbie Olgi (kontrolowany mocą ryngrafu). Odesłany przez Małgorzatę i Jana. KIA.
* czł: Franciszek Błazoń jako kultysta w służbie Anioła, który oddał swoją córkę na transformację przez Anioła Światła.
* czł: Karolina Błazoń jako 14-letnia dziewczyna która mocą Anioła i Anety została zmieniona w kultystkę Rytuałem Światła.
* czł: Rafał Czapiek jako ksiądz, który był celem kultu Anioła Światła i został ewakuowany i uratowany od śmierci przez Jana i Małgorzatę.
* vic: Paweł Franna jako historyk który został przywrócony jako nieumarły sługa Olgi Żmijuchy. Szuka ryngrafu w lesie.
* mag: August Bankierz jako kandydat na terminusa który zainteresował się dziwnym obrazem Andromedy i przez to wpakował się (i Olgę) w kłopoty.
* czł: Rufus Eter jako bogaty przedsiębiorca współpracujący z Olgą Żmijuchą; dostarcza jej elementy witraża i bardzo w nią inwestuje.

# Fronty:

### Front 1: Czarodziejka Zależa Leśnego
Expresses: siły pojedynczej czarodziejki próbującej ustabilizować swój świat
Scarcity: fear (strach przed odkryciem prawdy, strach przed byciem człowiekiem)

Agenda/ Dark Future:
Zależe zacznie się stabilizować pod rządami Olgi i hrabiego Zaleskiego, ale to tylko początek. Z biegiem czasu kontrola przejdzie na hrabiego Zaleskiego i powróci wczesny renesans.

Threats: 
- Olga Miodownik
=== Kind: grotesque, mutant
=== Impulse: craves restitution
=== Description: zbierająca artefakty czarodziejka pochodząca ze świata ludzi. Eks-patolog sądowy. Sprzeda duszę i zmieni ciało - tylko by nie stracić magii. Odsuwa tą groźbę od Zaćmienia. Powoli jej obsesja zabija jej ostrożność a jej naturalny optymizm zmienia się w rozpacz.

- Hrabia Onufry Zaleski
=== Kind: grotesque, pain addict
=== Impulse: craves pain, its own or others
=== Description: nieumarły hrabia, upiorny rycerz sprzężony z Olgą. Ich umysły się powoli łączą.
=== Custom move: przenika przez ściany, materializuje tylko to co musi, działa energią negatywną. Wrażliwy na anioła, światło, "dobro", religię i silną wolę. Ograniczony przestrzennie do dworku, chaty Olgi, cmentarza... chwilowo.

- Czterech osiłków
=== Kind: brutes, sybarites
=== Impulse: to consume someone’s resources
=== Description: żyjący z Rufusa Etera siłacze, którzy mają za zadanie rozwiązywać problemy swojego mocodawcy. Chwilowo mają załatwić montaż nowego witraża...

### Front 2: Kult Anioła
Expresses: rosnący w siłę i populację kult Anioła Światła, próbującego zaprowadzić anielski pokój w Zależu Leśnym. 
Scarcity: ignorance (niewiedza o działaniu magii, niewiedza o Skażeniu)

Agenda/ Dark Future:
Zależe zostanie nawrócone na wiarę Anioła Światła. Wielu zginie, wielu się wzmocni śmiercią pozostałych, wielu ulegnie Skażeniu.

Stakes:
- czy Aneta Kosicz stanie się viciniusem którego trzeba będzie zabić? / TAK
- czy Karolina Błazoń uniknie dołączenia do kultu? / NIE, dołączyła do kultu.

Threats: 
- Aneta Kosicz
=== Kind: warlord, prophet
=== Impulse: to denounce and overthrow
=== Description: eks-weterynarz, Skażona kobieta która stała się uzdrowicielką i uzyskała dostęp do magicznego artefaktu wpływającego na zwierzęta. Z całego serca wierzy, że to ją Anioł wybrał by została awatarem jego woli. Jednak koszt energetyczny jest wysoki, za każdym razem jak używa swojej mocy zmienia się coraz bardziej. Znalazła amulet kontrolujący hrabiego.
=== Custom move: Dotyk Anioła. atk 10 (vs BDS#WDS); jeśli się uda to regeneruje ciało i kieruje ku aniołowi linkując z aniołem.

- Kult Anioła
=== Kind: brutes, cult
=== Impulse: to victimize & incorporate people
=== Description: Grupa ludzi wierzących, że Aneta jest prorokiem a Anioł jedyną siłą. Min. Franciszek Błazoń. Część z nich mogła ulec Skażeniu.

- Anioł Światła
=== Kind: affliction, barrier
=== Impulse: to impoverish people
=== Description: konstrukt, artefakt, potwór Zależa Leśnego. Nie do końca wiadomo co to jest, ale jest to istota powiązana ze światem religijnym, która wspiera pana Anielskiego Dworu i go chroni oraz pomaga, kosztem okolicznych ludzi. Manifestuje się jako światło i łączony jest przez swoich wyznawców.
---
layout: inwazja-konspekt
title:  "Świnia na portalisku"
campaign: wizja-dukata
players: kić, prezes
gm: żółw
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [171001 - Powrót do domu (PT)](171001-powrot-do-domu.html)

### Chronologiczna

* [171001 - Powrót do domu (PT)](171001-powrot-do-domu.html)

## Kontekst ogólny sytuacji

### Siły główne

**Tomasz Myszeczka**

* Chce, by nie wyszło na jaw że ma luki w zabezpieczeniach. Sukces: zostawią go w spokoju.
* Chce, by wina spadła na kogokolwiek innego. Sukces: winny jest ktoś inny i wina odsunięta od niego.

**Bolesław Maus**

* Chce, by portalisko działało z możliwie minimalnymi opóźnieniami i nie mieć kłopotów. Sukces: działa.
* Chce uzyskać dowody na to że jest niewinny i może dostać pieniądze z ubezpieczenia. Sukces: dostaje dowody.

**Glukszwajn**

* Chce się przyjaźnić z randomowymi świniami z Męczymord. Sukces: wtopił się w tło.
* Chce wykopać i uruchomić element Harvestera. Sukces: udało się.


### Stan aktualny

brak

### Istotne miejsca

* Mżawiór, gdzie znajduje się Portalisko Pustulskie
* Rogowiec, gdzie znajduje się hodowla Myszeczki
* Męczymordy, gdzie znajduje się element Harvestera

## Punkt zerowy:

## Misja właściwa:

**Dzień 1:**

Po akcji Sylwestra Bankierza skierowanej przeciwko Dukatowi, Sądeczny kazał Dobrocieniowi uratować kilka innych operacji Dukata. To jednak spowodowało, że Dobrocień nie byłby w stanie wyrobić się z terminem dotarcia na czas do Portaliska i jego legalnej pracy dla Myszeczki. Zatem pojechał nie do końca właściwą, mocno napromieniowaną magicznie ciężarówką do Rogowca, przejechał blisko glukszwajnów i poprosił Henryka, człowieka od świń o załadowanie właściwych viciniusów. Jednak przy okazji przypałętał się jeden glukszwajn...

I Dobrocień przejechał do Portaliska Pustulskiego z glukszwajnem, co rozregulowało portal. Wszystko częściowo poszło do piekła.

* "Pomocy! Świnie mi wciągło!" - Bolesław do Natalii, znajomej weterynarz, która akurat jest w pobliżu.

Natalia pojechała. Na miejscu zdestabilizowane portale, kompletnie uszkodzone portalisko i załamujący ręce Maus. Kociebor nieprzytomny. Postawiła go dla Mausa. Bolesław się ucieszył - Kociebor pozbierał się na nogi (acz kosztem tego, że Maus będzie miał kłopot że nie wysłał go do lekarza). Kociebor skupił się by przypomnieć sobie jak wyglądała sytuacja gdy portal porwał ciężarówkę. Pamięta, że heroicznie wyciągnął magią kierowcę - Dobrocienia - i pamięta, że zdołał się oddalić by go zbyt nie napromieniowało. Pamięta też, że Dobrocień prosił by puścić go jak najszybciej bo się spieszy. I jest pewny że w ciężarówce było świń osiem - ale gdy wjechała do portalu było ich 9...

Dobrocień został odesłany do lekarza (Pauliny) na naprawę.

Sądeczny już naprawia i stabilizuje uszkodzone portale na portalisku. Maus się przeraził, że będzie musiał pozwać Myszeczkę o sabotaż bo inaczej zbankrutuje. Muszą zdobyć fakty dla ubezpieczenia, inaczej będzie poważny problem Natalia i Kociebor stwierdzili, że muszą glukszwajna zlokalizować i złapać. Poszli szukać gdzie ta cholera się teleportowała wykorzystując rezydualne pole z portalu.

Natalia i Kociebor przekonali Sądecznego tym, że dobrobyt okolicy jest zagrożony. Mag zdecydował się przerwać na moment pracę by móc znaleźć tego glukszwajna. A potem obiecał że w imię Dukata winny zapłaci. Portal jest Niestabilny, Promieniuje, Pracują nad nim, Inercja. Zespół zneutralizował Niestabilny przez wiedzę Kociebora (pracował z portalami i zna takie sytuacje) a Promieniowanie przez gabinet weterynaryjny Natalii. I udało im się - znaleźli glukszwajna. Kociebor wykrył go w niewielkiej wsi Męczymord. Zaprzyjaźnił się z lokalnymi świniakami.

Kociebor zabrał ze sobą wiszącą mu przysługę kadrową Jolę (wiedzącą o magii). Dodatkowo Kociebor zdecydował się zrobić keystone - kotwicę teleportacyjną, by wszystkie teleportacje glukszwajna się zapętliły i by wrócił do tego samego miejsca. Natalia poszerzyła to o przynętę, by niwelować zagrożenie dla ludzi. Oni mają być na ambonie a Natalia ma strzałki usypiające w pistolecie na glukszwajna. Aha, zapach przynęty - maciorka w rui. Plan prosty - ściągnąć glukszwajna, uspokoić, uśpić i przetransportować. Zadziała.

Oki, pojechali z Jolą do glukszwajna. Do gospodarstwa. Pojechali dalej zastawiać pułapkę na ambonie i robić rzeczy, ale zanim dobrze zaczęli to Jola zadzwoniła do Kociebora i powiedziała że rolnikowi świnia się teleportowała. I jest problem. Rolnik Paweł został onieśmielony i przerażony przez Kociebora. Kociebor podszedł do miejsca, gdzie znajdował się glukszwajn przed teleportacją i wysunął zaklęcie w jego kierunku. Gdzie jest glukszwajn? Zaklęcie ma efekt Paradoksalny. Kociebor teleportował do SIEBIE glukszwajna. Świniak kwiknął przerażony i teleportował się ponownie. Kociebor się go uczepił i poleciał z nim. Na target teleportacji.

Kociebor znalazł się w lesie. Przerażony świniak potruchtał kilka metrów i Kociebora zignorował. Kociebor nieszczęśliwy zadzwonił do Natalii i powiedział jej, że jest w lesie. GPS mówi, że kilkaset metrów od niej. Natalia się zebrała, przygotowała przynętę i przeprowadziła działania. Unieszkodliwiła wieprza. Lekką emisję wieprza wciągnęło to coś pod ziemią...

Kociebor wykopał to coś. To stary artefakt czyszczący energię magiczną który powinien ją wysyłać gdzieś dalej. Ale transmiter jest uszkodzony, więc nie wysyła. To część czegoś większego. Natalia zapłaciła za to, że znajomy Kociebora, Ryszard go obejrzy. Ryszard Kota się zgodził i zdecydował się pomóc. Dostał artefakt i poszedł siedzieć nad tym.

**Dzień 2:**

Ryszard powiedział, że ten artefakt to element starego Harvestera. Obwody zapasowe. Nie wiadomo, co można zrobić z tym teraz - ale problem Harvestera nie jest skończony. Okazuje się, że jednak nie całkowicie problem został rozwiązany lata temu i elementy tego Harvestera gdzieś nadal leżą...

Natalia udowodniła, że glukszwajn należy do Myszeczki i napisała odpowiedni dokument. Teraz już wiadomo skąd się wziął w ciężarówce Dobrocienia - ale nie wiadomo wciąż dlaczego. Pojechali więc do Myszeczki. Chcą zadośćuczynienia. 

Myszeczka zaprowadził ich do Henryka Mordżyna, który odpowiada za świnie i viciniusy świniopodobne. Ten zeznał, że Dobrocień bardzo się spieszył i że trochę złamano zabezpieczenia. Czemu się spieszył człowiek Dukata? Nie wiadomo. Zdaniem Myszeczki, problem Dukata. Zdaniem Kociebora, problem Myszeczki. Myszeczka próbował ich przekupić, ale się nie udało.

Myszeczka zatem zwolnił Henryka i zapłacił wszelkie potrzebne koszty. Co gorsza, dorobił się oficera politycznego Dukata i zaczął cicho wspierać Sylwestra Bankierza. Lepsza siła, która jest praworządna...

## Wpływ na świat:

JAK:

* 1: dodanie faktu lub okoliczności wobec 'neutral' lub 'claim'
* 1: counter działania MG z sensowną propozycją alternatywną (wobec CLAIM)
* 2: postawienie CLAIM.
* 3: przesunięcie zgodne z motywacją
* 5: przesunięcie niezgodne/sprzeczne z motywacją
* 5: przesunięcie wobec postaci z CLAIM wbrew graczowi

MG: 13 kart

* Żółw: rezydent polityczny u Myszeczki (3)
* Żółw: Myszeczka staje po stronie rezydenta Świecy (3)
* Żółw: w okolicy Męczymordy jest odpowiednia ilość pola rezydualnego by sprawić by znikające świnie były prawdą (1)
* Żółw: pojawia się zwiększenie ruchów antymausowych na tym terenie - mniej portaliska dla Mausów (3)
* Żółw: ciężarówka ze świniami jest półdostępna w okolicach Męczymordy (1)
* Żółw: goście od świń założyli grupę polującą na znikające świnie (1)
* Żółw: -

Gracze (x2 za konflikt +1/5 kart MG):

* Prezes: 5
    * gościu od znikających świń nie może tego tak zostawić. JEGO MISJĄ JEST TO UDOWODNIĆ! : 1
    * gościu ma przydupasa którego misją jest to udowodnić! : 1
    * Sądeczny daje Kocieborowi własną agencję detektywistyczną za zasługi i ratunek Dobrocienia: 3
* Kić: 4
    * pielęgniarz od zwierząt (Henryk) z wiedzą o działaniach Myszeczki : 1
    * Harvester nie ma dość energii by się obudzić: 1
    * Natalia ma własnego tresowanego glukszwajna z apetytem na magię Harvestera: 1
    * Natalia pozyskała bezpiecznie kolejną część artefaktu: 1

# Streszczenie

Po akcjach rezydenta przeciw Dukatowi kurier Dobrocień został przemęczony i przeciążony - w wyniku podłapał glukszwajna i próbował przewieźć go przez portalisko. Ledwo przeżył. Wypadek mógł zniszczyć portalisko. Weterynarz i "mag od wszystkiego" w portalisku poszli szukać glukszwajna i winnego. Znaleźli fragment zapasowych systemów Harvestera i znaleźli glukszwajna. Znaleźli też dowody świadczące o niedbałości Myszeczki a dokładniej zabezpieczeń jego hodowli. W wyniku - Myszeczka musiał zapłacić sporą cenę a portalisko nadal przez pewien czas jest nieczynne...

# Progresja

* Tomasz Myszeczka: Robert Sądeczny wsadził mu rezydenta politycznego - kogoś, kto ma pilnować, by nie działy się nadużycia z glukszwajnami.
* Kociebor Dyrygent: od Roberta Sądecznego dostał agencję detektywistyczną wraz z mandatem do detektywowania - za zasługi i ratunek Dobrocienia.
* Paweł Kupiernik: gościu od znikających świń ma przydupasa, którego misją jest pomóc mu udowodnić to wszystko (Prezes).
* Paweł Kupiernik: zakłada grupę polującą na znikające świnie; próbuje zrozumieć co tu się dzieje i zbierać informacje z terenu.
* Natalia Kazuń: dostała własnego tresowanego glukszwajna z apetytem na magię Harvestera.
* Natalia Kazuń: zatrudniła sobie Henryka Mordżyna jako pielęgniarza od zwierząt. On wie jak wyglądają działania Myszeczki.
* Natalia Kazuń: pozyskała aktywną i sprawną część Harvestera. Nie jest niebezpieczna, acz jest częścią Harvestera.
* Bolesław Maus: przeciwko niemu montują się nastroje antyMausowe i antyKarradraelowe

# Zasługi

* mag: Kociebor Dyrygent, główny pracownik Portaliska teleportujący do siebie glukszwajna (i się z nim), skończył z agencją detektywistyczną po tym, jak nie dał się przekupić Myszeczce.
* mag: Natalia Kazuń, wytresowała glukszwajna i postawiła nieprzytomnego Kociebora; zastawiła pułapkę na glukszwajna i jako weterynarz stała po stronie zwierzątek. Lokalna katalistka, o dziwo.
* mag: Jakub Dobrocień, chciał pogodzić interesy Dukata oraz Myszeczki i zaniedbał zabezpieczeń. Przez glukszwajna w portalu skończył ciężko ranny i skończył u Pauliny w szpitalu. 
* mag: Bolesław Maus, właściciel portaliska zdolny do desperackich decyzji odnośnie stawiania na nogi Kociebora, by tylko nie wypaść z interesu. Boi się Myszeczki.
* mag: Tomasz Myszeczka, który nieco za bardzo chciał oszczędzić i stracił najpierw glukszwajna a potem musiał zapłacić ogromne odszkodowanie za uszkodzenia w portalisku.
* mag: Robert Sądeczny, rezydent Dukata, który wyjątkowo napracował się ciężko na swoje utrzymanie stabilizując portal na portalisku po "wizycie" glukszwajna. Rozwiązał problem kto co płaci twardą ręką.
* czł: Jolanta Cieśliska, kadrowa z Portaliska Pustulskiego; lubi bardzo Dyrygenta i z przyjemnością pomogła mu rozwiązać problem rolnika, któremu zniknęła nadmiarowa świnia (glukszwajn)
* czł: Paweł Kupiernik, właściciel gospodarstwa w Męczymordach; jest na osobistej krucjacie udowodnienia, że ma 8 świń, ale dziewiąta zniknęła! Przekonany przez żonę wałkiem.
* mag: Ryszard Kota, który kiedyś miał problem z przeniesieniem artefaktu, znajomy Kociebora. Zidentyfikował artefakt jako element starego Harvestera. Też ma wiedzę historyczną o tym terenie.
* czł: Henryk Mordżyn, kiedyś zajmował się trzodą u Myszeczki a został pielęgniarzem zwierząt u Natalii. Nadal ma wiedzę odnośnie tego jak to się u Myszeczki robi.

# Plany

* Kociebor Dyrygent: kiedyś, pracownik portaliska Pustulskiego. Od tej pory ma zamiar zostać detektywem.
* Natalia Kazuń: staje po stronie zwierzątek w sytuacji konfliktu zwierząt i ludzi.
* Tomasz Myszeczka: zdecydował się wzmocnić Srebrną Świecę nad Rodzinę Dukata - Sądeczny wsadził mu rezydenta politycznego :-(.
* Paweł Kupiernik: gościu od znikających świń nie może tego tak zostawić. JEGO MISJĄ JEST TO UDOWODNIĆ! (Prezes)
* Bolesław Maus: za wszelką cenę chce utrzymać portalisko w swoich rękach. Jest zdolny do wielkich desperacji, by to wszystko jakoś działało.
* Tomasz Myszeczka: celuje w oszczędności i poszerzenie swoich wpływów jak najmniejszym kosztem finansowym.
* Robert Sądeczny: chce wykorzystać agencję detektywistyczną Kociebora do zbierania informacji i silniejszej kontroli terenu.
* Robert Sądeczny: jest zainteresowany stabilizacją terenu i ochroną portaliska Pustulskiego. Pilnuje, by na jego terenie nic złego nikomu się nie działo.

# Lokalizacje

1. Świat
    1. Primus
        1. Mazowsze
            1. Powiat Pustulski
                1. Mżawiór
                    1. Portalisko Pustulskie, które przez wypadek z glukszwajnem zostało wyłączone na pewien czas.
                1. Rogowiec, wieś
                    1. Hodowla Myszeczki, o wyjątkowo niskim poziomie zabezpieczeń skąd uciekł glukszwajn.
                1. Męczymordy, wieś
                    1. Gospodarstwa rolne, gdzie pojawił się przyjazny glukszwajn chcący się z wszystkimi przyjaźnić i skąd uciekł do lasu szeptów
                    1. Las szeptów, gdzie glukszwajn zlokalizował niezbyt sprawny element zapasowych obwodów Harvestera

# Czas

* Opóźnienie: 0
* Dni: 2

# Narzędzia MG

## Opis celu misji

-

## Cel misji

-

## Po czym poznam sukces

-

## Wynik z perspektywy celu

-

## Wykorzystana mechanika

Postać (strona gracza):

| .Motywacja. | .Umiejętności. | .Magia. | .SiłySłabości. | .Znam.    | .Mam.      |.POSTAĆ. |
|   (-2) - 2  |     0 - 2      |  0 - 2  |    (-2) - 2    | Z+M = 0-6 | Z+M = 0-6  |         |

Opozycja (strona NPC):

* Baza: 2
* Aspekt: 3/aspekt, wariant z nożyczki/papier/kamień
* Modyfikatory: Skala, Sprzęt, Sytuacja (1-3 każdy)

Wynik:

| .Postać. | .Opozycja. | .Rzut.    | .Wynik.  |
|          |            | xx: +x Wx |   S-SS-F |

Wpływ:

* 10 kart / dzień
* każda porażka i skonfliktowany sukces = +2 pkt wpływu dla gracza
* każda karta = +1 wpływ dla MG
* każde 5 kart zaokr. w górę: +1 pkt wpływu dla gracza

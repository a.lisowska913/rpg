---
layout: inwazja-konspekt
title:  "Ucieczka Trzmieli"
campaign: anulowane
gm: żółw
players: dust, dzióbek
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja:

## Punkt zerowy:

Wczesne badania nad mindwormami, planem mającym na celu walkę z Inwazją. Podczas jednego z wczesnych eksperymentów Blakenbauerowie pracowali nad jednym z więźniów dostarczonym przez Mojrę, magu Srebrnej Świecy. Ku jego wielkiemu nieszczęściu został on zniszczony i zamordowany by stworzyć pierwotny celatid mindwormów oraz byt mający na celu być wszczepianym w magów by tworzyć mindwormy. Margaret zdecydowała się na zaimplantowanie eksperymentalne takiego mindworma człowiekowi (nie magowi) by zobaczyć co się stanie. Okazało się, że jest to wystarczająco silne wiązanie z celatidem jako źródłem energii, że taki mindworm ma prawo działać. Margaret wbudowała temu bytowi odpowiednie systemy kontrolne i blokujące - nazwała go Trzmielem (istota rojowa) i przygotowała 3 egzemplarze Trzmiela, z 3 ludzi których dostarczyła jej Mojra.

W końcu projekt Trzmiel dojrzał na tyle, przeprowadzono na tym projekcie odpowiednią ilość badań, by Margaret mogła sprawdzić jak to działa w warunkach polowych. Podczas jednego z takich spacerów Trzmiel zainfekował ludzką kobietę, czego Margaret nie zauważyła, po czym dał się przejechać przez samochód. Margaret myślała, że wyczyszczenie ciała będzie wystarczające, ale Trzmiel już przeskoczył w ciało tamtej kobiety. Przygotował dwóch innych ludzi, zbudował okno i dwa Trzmiele w niewoli też popełniły samobójstwo by się odrodzić.

Tu Margaret załapała, że coś jest nie tak. Mając samobójstwa w laboratorium była w stanie przeanalizować co i jak się stało. Straciła cenną godzinę na to, by dowiedzieć się co naprawdę się stało, po czym wpadła w panikę. Natychmiast uderzyła do Ottona który uderzył do Mojry. Mojra eskalowała to do mocodawców z SŚ i uruchomiła ogromny kredyt usług (i nic więcej, by nie wzbudzać podejrzeń i nie interesować nikogo tym projektem). Otton przygotował rytuał detekcyjny, ale Trzmiele były już za daleko, czar nie mógł ich dosięgnąć.

Została tylko jedna opcja. Margaret zwróciła się do Hektora z nadzieją, że tam, gdzie magia zawiodła, tam metody niemagiczne i prokurator zadziałają. Trzmiele NIE MOGĄ im uciec...

## Misja właściwa:

Margaret przyszła do Hektora i powiedziała, że zawaliła. Przepraszała strasznie braciszka, że zawiodła, że się pomyliła, że fail i w ogóle... Grunt, że uciekły jej viciniusy. Trzy viciniusy, zdolne do przeszczepiania się w ludzkie ciała (i stawanie się tymi ludźmi), o kontroli umysłu na zasięg dotykowy. Viciniusy wiedzą o Blakenbauerach i chcą uciec - a nikomu nie zależy na dalszym szarganiu nazwy Blakenbauerowego rodu. Hektor zapytał czemu to robiła? Bo jej Otton kazał. Hektor westchnął i obiecał pomoc. Margaret poprosiła Klemensa - człowieka, opiekuna Marcelina, który dba, by Marcelin nie pakował się w za duże kłopoty - o pomoc Hektorowi. Klemens jest jednym ze wzmocnionych ludzkich agentów Blakenbauerów.

Hektor dowiedział się od Margaret, że Trzmiele zawsze używają nazwiska "Trzmiel", nie są w stanie kłamać. Mogą pokazać nieprawdziwy dokument, ale zapytani jak się nazywają zawsze powiedzą, że Trzmiel. To zawsze coś. Zadzwonił do swoich przełożonych i powiedział, że jest problem z rosyjskimi agentami. Ukradli coś bardzo ważnego (nie dokumenty) i próbują zbiec. Reagują na znane sobie hasło Trzmiel, nie wiedzą, że "nasi" wiedzą o tym haśle. Poprosił o zidentyfikowanie i zatrzymanie.

Dość szybko dostał odpowiedź zwrotną - osoby z takimi danymi zostały zlokalizowane w Świnoujściu. To zmroziło Blakenbauerów - to poza ich miejscem działania, poza ich rejonem i w ogóle na terenie nie należącym do Świecy. Hektor kazał zamknąć Trzmiela; sam pojedzie z nim porozmawiać i dowiedzieć się jak najwięcej. Mojra (z ukrycia) autoryzowała użycie Bramy do Warszawy a tam czekał już podstawiony kierowca ze Świecy.

Aha, Margaret jedzie z Klemensem i Hektorem. Hektor protestował, ale przegrał konflikt. Margaret jedzie dopilnować interesów Blakenbauerów i by chronić Hektora. Nie chciała, by się czegokolwiek dowiedział. Plus, ona może najlepiej pomóc / poradzić na Trzmiele.

Dotarli szybkim autem do Świnoujścia (kierowca piratował jak szatan magicznym autem a Hektora stróża prawa skręcało). Natychmiast udali się na komisariat policji gdzie przetrzymywany był Trzmiel (no, od razu po wynajęciu hotelu). Tam Hektora na rozmowę poprosił komendant policji, na osobności. Margaret została w komisariacie, ale Klemens wepchnął się z Hektorem na spotkanie z komendantem. Ten użył mocy Trzmiela kontroli i uśpił Hektora, ale zanim zdążył nawet zadeklarować swoje żądania, został obezwłądniony przez Klemensa i wsadzony w BONDAGE. No, to jeden problem z głowy. Klemens wciągnął Margaret do gabinetu komendanta i kazał jej ocucić Hektora, co się udało.

Wezwali kierowcę by podjechał pod komisariat, od strony gabinetu i... wyciągnęli przez okno komendanta. Porwali go bezczelnie. Nie chcieli używać magii, bo na tych terenach jest to relatywnie mało bezpieczne. Zawieźli Trzmiela do wynajętego hotelu i tam zdecydowali się z nim porozmawiać. Trzmiel w ciele komendanta policji był twardy, lecz obecność Margaret i kontrolne rozkazy przez nią wydane były wystarczające by zaczął mówić.

Trzmiel apelował do człowieczeństwa Klemensa i Hektora, mówiąc, że Margaret jest potworem. Potwierdził, że oni zabili ludzi, ale jedynie po to by uciec z rąk Margaret. Zaznaczył, że "oryginalne" osoby też nie żyją, zabite przez Margaret. Ta się nie wyparła - bardzo przepraszała, ale Otton jej kazał. Hektor westchnął. Pomoże siostrze i rodowi, naturalnie. Ale Margaret musi w przyszłości mieć więcej kręgosłupa moralnego. Ta się skwapliwie zgodziła a Klemens uśmiechnął się pod wąsem.

Trzmiel zeznał także, że on tu został by ich opóźnić. I to się udało. Pozostali dwaj są już na promie do Szwecji - a tam nawet potęga Margaret nie sięga. Więc mogą już tego puścić wolno, nikt inny nie musi ginąć. Margaret miała inny pomysł - skontaktowała się z Mojrą i dyplomacja SŚ podjęła dyskusję ze stroną szwedzką w sprawie Trzmieli (nieudany eksperyment SŚ może zrobić problem oni, terminusi, chcą to zneutralizować. Aha, i żeby to wszystko było bardzo ciche). Po przekazaniu odpowiedniej ilości Quarków, sprawa załatwiona - nasza trójka może spokojnie działać na terenie Szwecji ten jeden raz.

Więc udali się właśnie ram. Do Szwecji, tylko szybszą drogą. I poczekali na zadokowanie promu podczas czego Margaret bazując na krwi złapanego Trzmiela zbudowała prowizoryczny detektor (z niewielką pomocą szwedzkiego temrinusa - rezydenta). I go nawet dała radę odpalić zanim prom zadokował. Dostała potwierdzenie - na promie jest dwóch Trzmieli. I coraz dokładniej może ich zlokalizować.

W końcu prom zadokował. W celu uniemożliwienia ucieczki Trzmieli ale i infekcji zdecydowali się atakować każdego z osobna, zrobić dwa ataki i się nie rozdzielać. Pierwszy Trzmiel to motocyklista. Drugi to kobieta w samochodzie z mężem i dzieckiem. Wpierw zajęli się motocyklistą (bo szybciej wyjedzie z promu) - Klemens po prostu podszedł i strzelił w Trzmiela z paralizatora. Problem z głowy.

Kobieta zareagowała, wyszła z samochodu i zaczęła zmierzać w kierunku na burtę promu, dotykając wszystkich których się da by wywołać panikę. Granat gazowy (usypiający) rzucony w tamtym kierunku ogarnął Trzmiela. Kobieta czołgała się w kierunku burty chcąc się zabić zamiast wpaść w ręce Margaret, ale się nie udało. Margaret przechwyciła wszystkich Trzmieli.

Hektor surowo kazał Margaret więcej nie robić takich rzeczy, przestać, postawić się ojcu i uratować tych ludzi a viciniusy zniszczyć. Margaret przyjęła polecenie z pokorą...

# Zasługi

- mag: Hektor Blakenbauer jako ten, który nie kwestionuje działań siostry choć surowo ją upomina.
- czł: Klemens X jako agent specjalny Blakenbauerów do ochrony Marcelina... a przy okazji, zdolny rozwiązywaciel problemów.
- mag: Mojra jako ta, która ma praktycznie nieskończone wpływy w SŚ i nie zawaha się ich użyć
- mag: Margaret Blakenbauer jako ta, która jakkolwiek zrobiła błąd, zdecydowała się wziąć całą winę na siebie przed Hektorem
- mag: Otton Blakenbauer jako ten "który kazał". 
- vic: Trzmiel jako istota, która prędzej da się zniszczyć niż wróci do laboratorium Blakenbauerów

---
layout: inwazja-konspekt
title:  "Jad w prokuraturze"
campaign: blakenbauerowie-x-skorpion
gm: żółw
players: kić, dust, dzióbek
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [141006 - Klinika "Słonecznik" (WD, EM)](141006-klinika-slonecznik.html)

### Chronologiczna

* [141006 - Klinika "Słonecznik" (WD, EM)](141006-klinika-slonecznik.html)

## Punkt zerowy:

Ż: Co z punktu widzenia śledztwa zapewnia Alina, że na tej misji jest niezbędna zdekonspirowana?
B: Potrzebujemy kogoś, kto umie kompetentnie i dyskretnie poprowadzić policję w tym śledztwie.
Ż: Dlaczego Klemens został uznany za niezbędnego do wspierania Aliny i Hektora?
D: Inwigilowane środowisko jest zgodne ze strefą kompetencji Klemensa. 
Ż: Jaka informacja sprawiła, że Hektor został uznany za niezbędnego w śledztwie zarówno przez Edwina jak i Margaret?
B: Dlatego, że wszystkie ślady prowadzą do świata ludzi gdzie Hektor jest najgrubszą rybą z Blakenbauerów.
Ż: Jaką szczególnie istotną moc specjalną ma postać Kić?
D: Doppelganger. By przybrać inną postać potrzebuje fragmentu DNA.
Ż: Dlaczego tien Tadeusz Baran przygląda się wszystkiemu bardzo dokładnie?
K: Ponieważ ktoś dał mu cynk.
Ż: Jakiego typu zemstę zaplanowała Dalia na Edwinie?
B: Zaaplikować Edwinowi najbardziej upokarzającą rzecz w życiu, jak ona doświadczyła. I zaszczepić mu trigger.
Ż: Która strona zatrudni najemników?
D: Policja.
Ż: Koszmarny czyn, jaki magowie sprawili pewnemu człowiekowi?
K: Zabrali mu rodzinę, jego pozostawiając przy życiu.
Ż: Dlaczego ktoś mógłby chcieć wspierać siłę, jakiej metody się mu nie podobają?
B: Pragnienie zemsty jest większe niż jego poczucie niewłaściwości tych metod.
Ż: Jakie efekty uboczne sprawiły, że Maus musiał zginąć?
D: Dekompozycja organów wewnętrznych.
Ż: Jak działa bardzo niebezpieczny artefakt mogący wszystko zepsuć?
K: Broadcast.
Ż: Z jakiego powodu idealista płakał w nocy?
B: Po tym jak coś zobaczył, zrozumiał, że od tej pory będzie już inaczej.
Ż: Dlaczego utrata tego artefaktu tak naprawdę doprowadzi ICH plany do ruiny?
D: Ponieważ jest w stanie nadać to, co oni chcą ukryć.
Ż: Jaki mag próbuje za wszelką cenę powstrzymać ciemność?
K: Zraniony samotnik. Mag materii.
Ż: Jakie wydarzenie wymagało pełnego przebudowania Aliny celem ratowania jej życia?
B: Wpadła w węzeł.
Ż: Jakiego typu magiem jest Scaladar?
D: Piromanta.
Ż: Czy Dawid pierwszy zauważy Goliata, czy Goliat Dawida?
K: Dawid Goliata.
D->K: Co rodzinka obiecała Hektorowi w zamian za pomoc?
K: Pomoc w rozwiązaniu jakiejś sprawy związanej z prokuraturą.
K->B: W jaki sposób Alina i Klemens się poznali?
B: Oboje służą Blakenbauerom.
B->D: Dlaczego Otton nie uczestniczy w tej sytuacji osobiście?
D: Odprawia rytuał, którego nie wolno przez pewien czas przerywać.
K->D: Dlaczego Marcelin nie bierze w tym udziału?
D: Wyjechał z kolegami i koleżankami na tournee.
D->B: Czemu Klemens nie pojechał z Marcelinem?
B: Bo pojechała z nim Mojra.
B->K: Co spotka Hektora, jeśli rozwiąże to zgodnie z zasadą "no good deed ever goes unpunished"?
K: Marcelin uzna, że Hektor się zakochał i będzie chciał mu naraić coś specjalnego...
B->Ż: Kogo wynajęła Dalia do zemsty na Edwinie?
Ż: Znany artefaktor, Karol Poczciwiec oraz Izabela Łaniewska, terminus-żyleta.
D->Ż: Co sprawiło, że Hektor postanowił się osobiście włączyć w akcję zamiast oddelegować?
Ż: Będzie dużo bardziej bezpieczny z Zespołem niż solo. 
K->Ż: Dlaczego Edwin zupełnie nie czuje się zagrożony przez Dalię?
Ż: Bo w ogóle nie czuje się winny. Nie widzi, że cokolwiek się stało. Uważa siebie za ofiarę w takim stopniu co ona jeśli nie bardziej - to JEGO klinikę zamknęli.

## Misja właściwa:

Edwin udał się do Hektora i poprosił o pomoc. Opowiedział historię ze swojego punktu widzenia - chciał jako lekarz pomóc ludziom, załatwił i sformował klinikę. Niestety, podpadł komuś kto się bawił hipnoskorpionami. Jak tylko zainteresował się jadem Dromopod Iserat zaraz coś się stało - podłożyli mu w klinice jednego hipnoskorpiona i to doprowadziło do zamknięcia kliniki i jego aktualnych problemów. Hektor dostał nazwiska wszystkimi osobami, dostał też wiadomość o tym Mausie, że on miał niesamowicie dużą dawkę. Edwin powiedział jak działał - podleczał, wypuszczał, potem wysyłał osy i ściągał ich na kontrolę z powrotem. Niestety, wszystko stracone a on ma teraz problemy. Hektor zauważył, że Edwin robi to tylko dlatego, żeby pokazać się dobrze w oczach swojej dziewczyny (Mojry). Edwin absolutnie zaprzeczył. Hektor wie swoje.

Hektor udał się do Klemensa. Potrzebuje zgłoszenia na komendzie policji odnośnie zaginięcia "Stefana Małżoszpona". Klemens mu to załatwił - był w kostnicy zaprzyjaźnionej z Blakenbauerami, zdobył dokumenty świeżego truposza i zgłosił zaginięcie. Jeśli się potem okaże że zaginięty umarł, cóż, policja działać będzie zupełnie inaczej...
Hektor tymczasem uruchomił swoją grupę śledczą i przejął sprawę. Ma nadzieję, że znajdzie go bardzo, bardzo szybko.

Tymczasem Alina, agentka grupy specjalnej Hektora a DODATKOWO agentka Blakenbauerów pilnująca Hektora (żeby wszystko szło do przodu, by nie zrobił sobie krzywdy, by miał odpowiednią opinię, by nikt nic nie zauważył) dostała zadanie. Ale to zadanie zadziałało inaczej. Ona - Alina - nie dowodzi grupą specjalną Hektora. Ale tajnymi kanałami dostaje zawsze kopię polecenia. I polecenie, jakie dostała Alina i polecenie, jakie przekazał grupie dowódca śledczych grupy Hektora (Kleofas Bór) się różniły. Po pierwsze, Kleofas nie podał informacji o tym, że nie ma co szukać w promieniu 20 km od Kopalina (zasięg os). Po drugie, usunął jedną czy dwie informacje, znacznie poszerzając potencjalne osoby mające dostęp do informacji. I jedyną osobą wiedzącą o tym jest Alina...

No nie no, agentka manipulująca śledztwami dla dobra Hektora Blakenbauera nie podda się tak łatwo. Alina przywykła do tego, by od czasu do czasu sprawdzać postępy śledztwa jako Hektor (moc zmiennokształtnego). Hektor nic o tym nie wie, byłby ciężko zaszokowany, że taka sytuacja występuje... No i Alina gra twardego, okrutnego, Hektora "Terrora" Blakenbauera. Wtedy jak Hektor czasami odwiedza swój zespół jest zaskoczony jak bardzo są karni i cisi. Więc Alina wchodzi do nich jako Hektor, po dwóch dniach... robi im odprawę, w otwartym pokoju. Alina przycisnęła go dokładniej i (10v5) wyprowadziła, że on zmienił polecenia. Jest jednak potwornie zmęczony i jakiś taki... niemrawy. Pasywny. Apatyczny.
Alina "Terror Blakenbauer" rozdała prawidłowe polecenia, odwołała Kleofasa Bora, powołała kogoś innego na dowódcę i odeszła w siną dal.

Edwin poszedł do Hektora i spytał, ile jeszcze to będzie trwało. Hektor powiedział, że jego kompetentni ludzie już nad tym pracują. Edwin mówił, że zwykle trwało to mniej. Więc Hektor i Klemens pojechali do bazy śledczej... i Klemens dostał rozpaczliwy telefon od Aliny, że ma go stąd zabrać (widziała go przez okno). Klemens wywiózł i przekonał Hektora do pojechania poza bazę i ściągnął tam Alinę... lepiej, by faktycznie miała coś do powiedzenia.

I Alina (Alina Bednarz) się pojawiła. Pokazała się Hektorowi i Klemensowi. Poinformowała Hektora, że dowódca jej grupy, Kleofas Bór, zmienia jej rozkazy. Pokazała rozkazy prawdziwe i fałszywe. Hektor powiedział, że jest pod wrażeniem jej osiągnięć. Hektor nie dopytuje, skąd ona ma rozkazy. Alina powiedziała, że Kleofas najpewniej nie jest świadomy tego co zrobił i najlepiej, by spojrzał na niego mentalista. Hektor jest zdziwiony, że ma w zespole maga a Alina powiedziała, że nie jest. Powiedziała też, że przekazano zespołowi poprawne rozkazy. Że on, Hektor Blakenbauer, zrobił to osobiście. Hektor się ucieszył. Powiedział, że potrzebuje takich ludzi w zespole. Powiedziała też, że Hektor zdjął Kleofasa Bóra. Hektor się zgodził. Napuści na niego Edwina.
Teraz śledztwo ruszy z pełną parą.

Hektor i Klemens zawieźli nieszczęsnego Kleofasa Bóra do Edwina. Edwin go przebadał i kilka godzin później miał odpowiedź - Kleofasowi zaaplikowano jad Dromopod Iserat. Ale zmodyfikowany, nad tym pracował jakiś alchemik. I to konkretny alchemik, taki, który się dobrze zna nad jadem Dromopod Iserat i na komponentach medycznych. To wystarczyło Hektorowi - przesłuchał go magicznie (14 vs 12 -> 13). Hektor przeskanował pamięć i wyszło, że on od 3 miesięcy jest podtruwany i kontrolowany. Zawsze jak dostawał rozkazy od Hektora dzwonił pod konkretny numer telefonu. Dostawał czasem podpowiedzi, pomoc - by być bardzo skutecznym. Tym razem dostał zadanie zmiany rozkazów i opóźniania.
Dawki dostawał tak, że dostawał losowe telefony (prepaid) gdzie mówiono mu gdzie ma się stawić.
Dodatkowo zanim Kleofasa pochłonął jad, przeprowadzał jeszcze elementy śledztwa - udało mu się zobaczyć kilka osób, które mają coś z tym wspólnego. To nigdy nie są ludzie bardzo ważni, ale są wpływowi (np. nie burmistrz, ale jego sekretarz). To co ważne - to nie może być powiązane z kliniką Edwina; jak Kleofas dostał pierwszą iniekcję, Edwin jeszcze nie założył kliniki.

Hektor poprosił Edwina o coś przeciwko jadowi, osłonowego. Edwin powiedział, że z Margaret zajmą się konkretnymi lekami. Jedyne co ma to lek osłonowy ale jak się to bierze dłużej to kończy się licznymi wymiotami i biegunką. Edwin powiedział też Hektorowi, że nie może dać mu niczego do wykrycia osób z jadem Dromopod Iserat - to cholerstwo jest fatalnie wykrywalne, znaczy, niewykrywalne. Ale to co może zrobić - sformuje z Margaret taką kombinację chemikaliów, żeby jak ktoś ma we krwi jad D.I da jeden zestaw objawów a jak nie ma, inny. I to się będzie wstrzykiwać. Hektor uczulił Edwina, by te objawy nie były zbyt... hardcorowe i Edwin wraz z Margaret zaszyli się w lab. Hektor oddał im Kleofasa do eksperymentów.

Po siedmiu godzinach grupa śledcza znalazła Henryka Waciaka. Był w miejscowości 114 kilometrów stąd. Nie żyje między 24 a 48 godzin. Pozostawił rodzinę, która została powiadomiona o śmierci odpowiednio wcześnie. Ktokolwiek opóźnił rozkazy, sprawił że Waciak nie przeżył...

Hektor, Klemens i Alina spotkali się celem synchronizacji. I zadzwonił Edwin do Hektora - znaleźli z Margaret coś dla niego. Neurotoksyna, zmodyfikowany jad pewnego węża. Działa tak: jeśli osoba jest niewinna, ślepnie na 30-60 minut i ma halucynacje. Jesli jednak ma w krwi jad Dromopod Iserat, dostaje ataku padaczki. Nic od momentu wstrzyknięcia jadu do momencie zakończenia objawów nie jest pamiętane przez ofiarę. Więc, Hektor i Klemens skonspiracjonowali się przeciw Alinie, by sprawdzić czy nie ma w niej jadu (pomysł Hektora). Szczęśliwie Klemens zorientował się, że jako, że ona jest viciniusem może to zadziałać na nią inaczej. Telefon do Edwina potwierdził jego obawy - Edwin kategorycznie nie pozwolił jakichkolwiek iniekcji w Alinie. Sam to sprawdzi.

Klemens wpadł na pomysł - wynająć nekromantę by dowiedzieć się jak najwięcej o Henryku Waciaku. By przesłuchać trupa. Pojechali tam na miejsce, wynajęli nekromantę i Hektor twardo przesłuchał trupa z pomocą Aliny (10+2 vs 8/10/12 -> 11). Mimo, że Henrykowi Waciakowi wymazano pamięć przed zabiciem, magowie mażący pamięć nie byli świadomi tego, że i w jaki sposób działa nekromancja wobec magii mentalnej. Więc Henryk Waciak mógł powiedzieć całkiem sporo, zwłaszcza, że niedawno umarł. Henryk nie był zbyt szczęśliwi, że go zabili, więc nie miał wiele przeciwko temu, by sypać.

Nazywał się Henryk Waciak, był agentem Skorpiona - organizacji pomagającej ludziom w walce z magami i w ochronie ludzi przed magami. Ta, którą znali jako "Amelia" w rzeczywistości nazywa się Estera Piryt i mieszka z mężem w Kopalinie - podał adres. On, Estera, jej mąż... oni są członkami Skorpiona od zawsze, jak jeszcze był niewielką organizacją. Henryk jest zdziwiony, że nie żyje - agenci, którzy byli zdekonspirowani lub zagrożeni byli zwykle po prostu przenoszeni. Może ich też zabito?
Estera jest alchemikiem, jest magiem specjalizującym się w magicznych chemikaliach. O Dromopod Iserat nic nie wie. 
On był agentem Estery w akcji przeciwko klinice Edwina Blakenbauera. Estera wynajęła najemników by zniszczyć klinikę. O samym planie itp. nic nie wie.
Zapytany przez Hektora, podał 6-7 agentów takich jak on. Ten Maus który zginął był jednym z agentów Skorpiona.
Atak na klinikę był wynikiem tego, że Skorpion został przez klinikę Edwina zaatakowany. Np. Maus został zabity.

Telefon Kleofasa zaczął dzwonić. Alina przekształciła się w Kleofasa i odebrała. Hektor zaczął informować ją o tym co Kleofas odpowiadał a Alina / Kleofas mówiła to co powinna. W skrócie: Alina dowiedziała się, kiedy następna iniekcja jadu Dromopod Iserat. W wyniku tej konwersacji Hektor nacisnął by dowiedzieć się kim jest Alina i jaka jest jej prawdziwa funkcja. Dyskretna sprawa Ottona zawisła na włosku (10 v 10 -> 9), lecz Alina twardo się obroniła przed pytaniami Hektora. Hektor wie, że Alina ma dodatkowe zdolności, ale wierzy, że "dziewczyna musi jakoś zarabiać" i że po prostu jest w jego zespole.

Wrócili do Rezydencji Blakenbauerów. A tam - mgła dezynfekcyjna. I jeden przerażony, roztrzęsiony lekarz z SŚ. Okazało się, że znalazł na ulicy pacjenta, który miał pewne objawy które Edwin nazwał krytycznymi, więc lekarz z SŚ przyleciał do Edwina mimo, że klinika zamknięta. Edwin wysłał go do diabła (bo zeznawał przeciw niemu), ale lekarz podał objawy. Edwin pojechał po pacjenta, dezynfekcja, badania... i potem skrzyczał wszystkich (a zwłaszcza Hektora, bez powodu), że istnieją kombinacje leków, jadów, środków przy których pojawiają się bardzo niebezpieczne, zaraźliwe choroby. Min. takie co mają jedne objawy na magów a inne na ludzi. I to się właśnie pojawiło, jakiś kretyn przesadził i bawił się różnymi dziwnymi lekami i eliksirami. 

Hektor powiedział Edwinowi, że "Amelia" to Estera Piryt. I że jest alchemikiem. Edwin zatarł ręce. Ma swoją zemstę.
Zemsta Blakenbauera może nierychliwa, ale sprawiedliwa.

"Zespół i tak po 'Terrorze' Blakenbauerze spodziewa się jakiejś podłości". - Kić
"Klemens zabierz go stąd! Jestę Hektorę!" - Alina.
"Nie mamy Dromopod Iserat. Nie umiemy utrzymać go przy życiu. Nie to, że nie próbowaliśmy..." - Edwin.
"Ja mam gust. Pracuję nad płaszczkami i chorobami. TO JEST TWOJE." - Edwin do Hektora o Alinie.

# Zasługi

* mag: Hektor Blakenbauer jako prokurator używający swoich znajomości w świecie ludzi z powodzeniem
* czł: Klemens X jako żywy dowód, że im mniejsza moc magiczna tym lepsze pomysły
* vic: Alina Bednarz jako zmiennokształtna agentka ratująca Hektora od rozkazów niezgodnych z linią Blakenbauerów
* mag: Edwin Blakenbauer jako ten, który pragnie pomsty i metaforycznego mordu
* mag: Mojra jako wielka nieobecna, która jednak pełni pewną rolę przez swą nieobecność
* czł: Kleofas Bór jako dowódca sił specjalnych Hektora, który wyraźnie gra na dwóch frontach
* mag: Margaret Blakenbauer jako naukowiec i lifeshaperka pełniąca wyraźnie wspierającą rolę
* czł: Henryk Waciak jako śledczy świętej pamięci który zaczął sypać lokalnemu nekromancie

# Lokalizacje:


1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Dzielnica Owadów 
                        1. Szpital Pasteura
                    1. Obrzeża
                        1. Rezydencja Blakenbauerów
                        1. Klinika "Słonecznik"
---
layout: inwazja-konspekt
title:  "Epidemia Dezinhibicji"
campaign: wizja-dukata
players: kić, raynor
gm: żółw
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [171101 - Magimedy przed epidemią (PT)](171101-magimedy-przed-epidemia.html)

### Chronologiczna

* [171101 - Magimedy przed epidemią (PT)](171101-magimedy-przed-epidemia.html)

## Kontekst ogólny sytuacji

### Wątki konsumowane:

* Bolesław vs Tomasz:
    * Czy będzie tu ściągnieta ekipa by zrobić porządek ze "świniami" i Świecą?: TAK. (Żółw). Siła: 5/10.
    * Bolesław Maus kontratakuje przeciwko Tomaszowi Myszeczce - nie może tak być, że ciągle świnie niszczą portalisko.
    * Pojawia się zwiększenie ruchów antymausowych na tym terenie - mniej portaliska dla Mausów.
* Epidemia:
    * Większość magów dotykających portaliska podczas katastrofy - nie PC - jest zaklątwożytowana (Żółw)
    * Klątwożyty i efemerydy prowadzą do zbliżającej się epidemii (Żółw)
    * Jak krystality mogą pomóc w zatrzymaniu epidemii? (Żółw)
* Niszczycielska Dracena
    * Dracena wykonuje działania mające uwolnić "Nową Melodię" od mafii 
* Chrumkanie Mroku:
    * Energia Eteru i Efemerydy Senesgradzkiej mutuje inne efemerydy w eterze. Zwłaszcza te świńskie. Mamy wylęgarnię. (Żółw)
    * Doszło do Skażenia Primusa efemerydami świńskimi w ogromnym stopniu. Są WSZĘDZIE.
* Stan okolicy:
    * Byt który opętał elementala/awiana Rolfa jest emocjonalnie związany z dzieckiem.
    * Zarówno Sądeczny jak Muszkiet są wyłączeni z akcji przez około tygodnia.
    * Balans sił i energii po prostu się rozpadł na tym terenie - nikt nic nie kontroluje.
    * Sytuacja z portaliskiem wzmacnia status quo na tym terenie - nikt nie jest w stanie uzyskać przewagi. (Raynor)
    * Sądeczny przekazał dowodzenie defensywne Wiaczesławowi (Żółw)
    * Chaotyczność i nieufność / niestabilność Oktawii jedynie wzrosła. Ciągle odrzucana, discardowana, torturowana. (Żółw)

## Punkt zerowy:

* Ż: Skąd Kaja Maślaczek i Daniel Akwitański się znają?
* K: Ponieważ Kaja została kiedyś wysłana do Daniela i Kaja była wysłana by Daniela opierniczyć by włączył zasilanie.
* Ż: Co do cholery robi Dracena wieczorem w magitrowni? Czego szuka? Co jej nie pasuje?
* R: Robi nadgodziny, ponieważ szuka innych konektorów takich jak ten należący do Harvestera.
* Ż: Jakie w okolicy jest inne silne źródło energii magicznej, acz nie wykorzystywane?
* K: Klimatyczny zamek z białą damą w Byczokrowiu.
* Ż: Czemu nie jest wykorzystywane?
* R: Ta energia nie jest czysta. Jest tam wyjątkowo paskudna historia, więc energia nie jest czysta.
* Ż: Wymień dwie postacie interesujące dla Ciebie na tej sesji
* K: Dalia Weiner, Kaja Maślaczek
* R: Sylwester Bankierz, Tomasz Myszeczka

## Potencjalne pytania historyczne:

* Czy Dracena zniszczy wszelkie siły detekcyjne Tamary i Sądecznego na tym terenie z pomocą Kai?
    * Domyślnie: TAK
* Czy świński temat stanie się bardzo palącym problemem łamiącym Maskaradę i zagrażającym magom?
    * Domyślnie: TAK
* Czy epidemia się ukonstytuuje jako Pink Goo i zarazi źródła magii w Białym Zameczku?
    * Domyślnie: TAK
* Czy to się stanie w Senesgradzie?
    * Domyślnie: NIE

## Misja właściwa:

**Dzień 1**:

Marzyciel poskarżył się na Dracenę Danielowi - ona rozprasza Guziewicza i poziom pary spada.
Guziewicz się przyznał zapytany przez Daniela - tak, rozprasza. Ale to Dracena prosiła o upuszczanie pary w różnych miejscach. I się nachylała!
Dracena powiedziała Danielowi, że słyszy śpiew Oktawii. Wieczorami. Więc szuka. Powiedziała, że może... potrzebować członków załogi magitrowni.
Daniel kazał Marzycielowi zrobić pokój dla Draceny i obić go lapisem.
Potem Daniel pojechał do Oliwii. Jest zainteresowany tym, by Oliwia znalazła mu Oktawię. Oliwia nie bardzo chce... ale Daniel ją przekonał. Oliwia pojechała. Już z klątwożytem.
Tam na miejscu - Kaja. Przekonywała Dracenę, że to moment, by zniszczyć wszystkie sensory i systemy Sądecznego. Bez kłopotu ją przekonała.
Oliwia została sama - Kaja złożyła tą samą ofertę Danielowi zaznaczając, że to idealny moment a on ma efemerydy na rurach.
Oliwia w magitrowni. Słania się pod wpływem klątwożyta i walnęła w rurę zaklęciem.
Dracena wypadła i wycałowała Oliwię. Pod wpływem pola magicznego i klątwożyta Dracena revertowała w formę bardziej wiły. Daniel wycofał ludzi i poleciał naprawiać rurę która wycieka.
Drugi SS. Drugi poziom rozprzestrzenienia klątwożyta. Dracena się opanowała na czas. Zaniosła Oliwię do pokoju Daniela i ją związała. Potrzeba doktora dla Oliwii - coś jest nie tak.
...tymczasem Paulina...
Paulina zajmuje się Anetą i próbuje jej pomóc. Stabilizuje ją w magimedzie i przygotowuje się do porządniejszych badań. Gdy...
Trzy czarne awiany przeleciały i rozrzuciły achtungi. Chwilę potem wybuchły 2-3 blokady. Część świń - glukszwajnów, innych viciniusów - uciekła. Po chwili z ziemi poleciała rakieta. Uderzyła w jednego z awianów. Awian spadł, dwa inne uciekły.
Tomasz Myszeczka się popłakał. Paulina zdziwiona. Jakiś wojskowy przejął władzę.
Paulina poszła w kierunku na awiana. Tam zobaczyła pięciu łysych w czarnych kurtkach z czaszkami z chabrem.
Paulina zdecydowała się walnąć zaklęciem snu. Nie będzie pięciu łysych. Zadziałało.
Zaszedł ją "Doktor Zło", czyli Szczepan. Pyta dla kogo ona pracuje i czemu ratuje awiana. Paulina gra na czas.
Maria nawija po soullinku by spowolnić. 
Wpadła ekipa Myszeczków. W bijatyce Paulina dostała w twarz od sojusznika.
Gdy Paulina otworzyła oczy, Doktor Zło ciężko pobity. Bysie są w ciężkiej formie. Paulina ich poleczyła. Na miejscu od Myszeczki ten oficer co przejął dowodzenie - brat Doktora Zło.
Dalia pełna obsesja. Tam dogorywał Zło - tu Dalia zajmuje się sińcami Pauliny.
Po komunikacie Daniela do Pauliny, Daniel załatwił drabonów a Paulina poszła do magitrowni. Dalię Paulina wysłała do Myszeczki.
Paulina dotarła do magitrowni.
Tam Dracena powiedziała że podejrzewa klątwożyta u Oliwii. Ona oczywiście jest niewrażliwa. Paulina leczy Dracenę.
Paulina przechwyciła Wzór klątwożyta a Dracena jest wyleczona. Klątwożyt jest ciekawy - klątwożyt dezinhibicyjny.
Paulina i Daniel spróbowali przekonać Dracenę. Przynajmniej tymczasowo. Udało się - odłoży na potem, jak tylko problem się częściowo rozwiąże.
Daniel ma wiadomość od drabonów. Boją się. Przyszedł Wiaczesław. Paulina się z nim skomunikowała... 
Wiaczesław obiecał że powie Paulinie jak coś się dowie o tym kto był z awianem. Na razie dowodzi całym terenem. Spróbuje pomóc ze świniami, ale nie ma pomysłu jak.
Daniel wymyślił! Nie zamkną magitrowni, mogą wysłać do Białego Zameczku! Ostrzegł Świecę, trzeba ich ewakuować.
Paulina ma Paradoks. Daniel osłaniał Paradoks - energia poszła, poleciała prosto na Zameczek. Klątwożyty tam poleciały, zmiecione zaklęciem Pauliny.
Glukszwajny poleciały za nimi.
Sylwester Bankierz zdołał się wycofać ze swymi siłami.

## Wpływ na świat:

JAK:

1. Co na tej sesji dookoła jakiegoś konfliktu czy zdarzenia było fajne? Co chcesz by w ramach tego się poszerzyło?
1. Każdy gracz wydaje swoje punkty na generowanie rzeczywistości i wątków.

CZYLI:

* Wiaczesław będzie grał na powrót Pauliny rezydentki. (Kić)
* Wiaczesław przyjdzie zrobić porządek w magitrowni Histogram. (Raynor)
* Czy Dracena zniszczy wszelkie siły detekcyjne Tamary i Sądecznego na tym terenie z pomocą Kai?: NIE
* Czy świński temat stanie się bardzo palącym problemem łamiącym Maskaradę i zagrażającym magom?: TAK
* Czy epidemia się ukonstytuuje jako Pink Goo i zarazi źródła magii w Białym Zameczku?: TAK

JAK:

* 2: MUSIK: gracz mówi w jaki sposób jedna z obcych postaci wspiera coś związanego z motywacją JEGO postaci
* 2: CLAIM na wątku / postaci / okoliczności
* 2: do elementu Historii lub przeszłego konfliktu dodajemy "ale" lub "oraz"
* 2: dodajemy pytanie, które musi zostać odpowiedziane na jakiejś z przyszłych misji
* 2: odpowiadamy na pytanie, które pojawiło się na jakiejś z misji
* 3: gracz mówi w jaki sposób jedna z obcych postaci wspiera coś związanego z motywacją JEGO postaci
* 3: zmieniamy kontekst okoliczności czegoś z Historii - scena z przeszłości / fakt?
* 3: do elementu spoza Historii dodajemy "ale" lub "oraz"
* 3: dodajemy znaczącego NPC powiązanego z elementem Historii
* 3: pozyskanie przez dowolną postać znaczącego surowca mającego sens z perspektywy misji
* 3: dodanie nowego elementu Historii
* 3: dodanie przyszłego lub przeszłego faktu; czegoś, co się wydarzyło lub wydarzy

Z sesji:

1. Żółw (16)
    1. Klątwożyty dezinhibicyjne trwale Skaziły Biały Zameczek.
    1. Klątwożyty dezinhibicyjne trwale dostosowały się do okolicy. Bardzo trudno będzie je wyplenić.
    1. Dracena uważa, że to jest moment na uderzenie w Sądecznego i zniszczenie władzy na tym terenie
    1. Farnolis zaczął przejmować władzę na obszarze dawnego Pentacyklu; odbudowując pod areną obszar wpływu.
    1. Farnolis zaraził obszar dawnego Pentacyklu klątwożytem dezinihibicyjnym.
1. Kić (12)
    1. Jeden typ klątwożyta. Mutujący, ale jeden. W ten sposób da się z tym poradzić - i to zrobił ich wspólny Paradoks (Kić)
    1. Kaja uważa, że już dość; czas zacząć pomagać i ratować sytuację (Kić)
    1. Dalia została wyleczona z tego klątwożyta i już go nie złapie (Kić)
    1. Paulina dostaje wsparcie od Millennium. (Kić)
1. Raynor (10)
    1. Magitrownia uzyskuje wsparcie zasobowe Eweliny Bankierz. (Raynor)
    1. Całą tą sprawą, tą destabilizacją zainteresowała się nowo powstała prokuratura magów. (Raynor)
    1. Nowa polityka miłości w firmie generalnie rozwiązuje problem z napięciami i nie powoduje publicznych rozterek. (Raynor)

# Progresja

* Dalia Weiner: wyleczona z klątwożyta i disinhibition plague; niewrażliwa na nie (Kić)
* Andrzej Farnolis: odbudował pod terenem dawnego Pentacyklu... coś. I zaraził to miejsce klątwożytem dezinhibicyjnym
* Paulina Tarczyńska: dostaje wsparcie od Millennium. (Kić)

# Plany

* Wiaczesław Zajcew: będzie grał na powrót Pauliny rezydentki. (Kić)
* Wiaczesław Zajcew: chce (i przyjdzie) zrobić porządek w magitrowni Histogram. (Raynor)
* Kaja Maślaczek: uważa, że już dość z epidemiami itp; czas zacząć pomagać i ratować sytuację (Kić)
* Ewelina Bankierz: decyduje się wesprzeć Magitrownię Histogram jako nowa wspierająca siła (Raynor)
* Andrzej Farnolis: odbudowuje potęgę Bractwa Pary korzystając z nieobecności Sądecznego (Żółw)
* Dracena Diakon: uważa, że to jest moment na uderzenie w Sądecznego i zniszczenie władzy na tym terenie (Żółw)

# Streszczenie

Dracena zorganizowała sobie pokój miłości w magitrowni. Słyszy głos Oktawii w nocy, więc Daniel ściągnął Oliwię zarażoną klątwożytem. Dracena osłoniła magitrownię. Paulina wprowadziła kwarantannę i centrum dowodzenia na farmie Myszeczki - więc zaatakowały farmę awiany... Potem siły Wiaczesława i siły Myszeczki się poprztykały, Daniel osłonił farmę Myszeczki i ogólnie doszło do eskalacji epidemii a Świeca musiała się ewakuować przez glukszwajny i klątwożyty.

# Zasługi

* mag: Paulina Tarczyńska, próbowała opanować epidemię we wczesnej fazie - niestety, atak awianów rozwalił ten plan. Potem poprztykała się z paramilitarnymi Chabrami i odnowiła kontakt z Wiaczesławem.
* mag: Daniel Akwitański, odbił Paradoks Pauliny do Zameczku i ostrzegł Świecę; podniósł morale zespołu Draceną i ściągnął Oliwię - która okazała się być pod wpływem klątwożyta.
* mag: Kaja Maślaczek, próbowała przekonać Dracenę do rozmontowania i zniszczenia sił Sądecznego na tym terenie. Udało jej się. Wycofała się by uwolnić Ewelinę.
* mag: Dracena Diakon, która zrobiła sobie w Magitrowni Histogram małe "gwiazdko miłości". Powstrzymała dezinhibicyjną Oliwię przed wysadzeniem magitrowni.
* czł: Szczepan Złodrak, szef punktu ofensywnego Sądecznego. Twardy były wojskowy. "Doktor Zło", próbuje opanować sprawę z zestrzelonym awianem i na 5 minut wziął Paulinę do niewoli.
* czł: Bolesław Złodrak, szef militarny u Tomasza Myszeczki. Twardy były wojskowy. Współpracuje z Pauliną i próbuje jakoś osłonić przed nią Szczepana (brata).
* czł: Karol Marzyciel, zgłosił Danielowi, że są problemy bo Dracena rozprasza facetów... za co Daniel kazał mu znaleźć Dracenie "gniazdko miłości". Nieszczęśliwy.
* mag: Wiaczesław Zajcew, przejął dowodzenie nad siłami Sądecznego i zdecydował się utrzymać porządek niezależnie co będzie to kosztować.
* mag: Dalia Weiner, zainfekowana klątwożytem z obsesją na punkcie zdrowia i dobrobytu Pauliny; szczęśliwie, docelowo wyleczona.
* mag: Tomasz Myszeczka, zainfekowany klątwożytem i płakał i był nieszczęśliwy i stracił trochę twarzy i bogactwa i glukszwajnów. Nie jego dzień.
* mag: Sylwester Bankierz, musiał ewakuować bazę Świecy przez nadciągające klątwożyty i glukszwajny. Utrzymał Tamarę w magitechu. Zdobył wsparcie.
* mag: Oliwia Aurinus, zainfekowana klątwożytem dezinhibicyjnym; pacjent zero. Uszkodziła magitrownię i powstrzymana przez Dracenę.

# Lokalizacje

1. Świat
    1. Primus
        1. Mazowsze
            1. Powiat Pustulski
                1. Męczymordy
                    1. Magitrownia Histogram, z "pokojem miłości Draceny". Słychać tam śpiew Oktawii. Nie wiadomo czemu.
                1. Byczokrowie
                    1. Biały Zameczek, klimatyczny z białą dama i silnym źródłem energii; został trwałym źródłem emisji klątwożytów dezinhibicyjnych. Opanowany przez stado glukszwajnów.
                    1. Niezniszczalna Kazamata, ewakuowana przez Sylwestra Bankierza po tym, jak Biały Zameczek został opanowany przez glukszwajny i klątwożyty.
                1. Rogowiec
                    1. Hodowla Myszeczki, zaatakowana przez tajemnicze czarne awiany i ochraniana przez Wiaczesława Zajcewa.
                    1. Lasek, gdzie doszło do starcia między siłami Wiaczesława a Pauliną i siłami Myszeczki.

# Czas

* Opóźnienie: 0
* Dni: 1

# Wątki kampanii

1. Stan aktualny Pauliny Tarczyńskiej
    1. CLAIM: Paulina zostanie rezydentką Świecy na tym terenie a Tamara jej prawą ręką - terminusem bojowym.
    2. CLAIM: Paulina nie będzie musiała uciekać z terenu niezależnie od okoliczności (Kić)
    3. Magimed zostaje u Pauliny jako trwały surowiec. (Kić)
    4. Sądeczny uznał Paulinę za gracza a nie pionek - docenił to czym jest. (Kić)
1. Stan aktualny Krzysztofa Grumrzyka
    1. Po tym jak Krzysztof pomógł Filipowi, współpraca się zacieśniła - lepsza kalibracja Firmy i Firmy.
1. Stan aktualny Kociebora Dyrygenta
    1. CLAIM: okazja na poznanie i zaprzyjaźnienie się Kociebora z Kajetanem.
    2. Sądeczny daje Kocieborowi własną agencję detektywistyczną za zasługi i ratunek Dobrocienia
    3. Oliwia zapewnia Kocieborowi drony, które pozwolą na obserwowanie i śledzenie. Nowy biznes Oliwii.
    4. Customizowany awian, nie taki jak Czarny Ptak dla Kociebora. Taki czołg.
    5. Maja próbuje w najbardziej nieoczekiwanych momentach (i nieodpowiednich) okazać Kocieborowi sympatię i pomoc.
    6. Prawdziwym bohaterem Mai stał się Kociebor, który potraktował ją łagodnie i z sympatią.
1. Stan aktualny Daniela Akwitańskiego
    1. CLAIM: Myszeczka jest ważnym partnerem biznesowym Daniela (Raynor)
    2. Sądeczny w swoim investigation odkrywa, że Tamara zadziałała wbrew Danielowi i on nie chciał stawać przeciw niemu
    3. Robert uważa, że współpraca z Danielem oznacza, że Daniel jest dużo bardziej cenny niż myślał wcześniej
    4. Konflikt z Sylwestrem Bankierzem - oferował największą wartość, Daniel był w stanie przekonać go że jest wartościowym partnerem. (Raynor)
    5. Działania Daniela i Tomasza nie zostały wykryte przez ani Sądecznego ani Tamarę. Przez nikogo. (Raynor)
1. Stan Mazowsza i okolicy
    1. Filip aktywnie wspiera Paulinę w redukcji pola magicznego i Skażenia na tym terenie.
    2. W jaki sposób można odepchnąć w eterze nadmiar energii, by pozbyć się kłopotu bez skrzydła katalistów, terminusów i logistyków?
    3. Te pieprzone świnie się rozmnożyły. Pasożytnicze świnie z eteru. (Prezes)
    4. Doszło do wielokrotnego złamania Maskarady na tym terenie.
    5. Energia Eteru i Efemerydy Senesgradzkiej mutuje inne efemerydy w eterze. Zwłaszcza te świńskie. Mamy wylęgarnię. (Żółw)
    6. Doszło do Skażenia Primusa efemerydami świńskimi w ogromnym stopniu. Są WSZĘDZIE.
1. Efemeryda Senesgradzka i jej stan
    1. CLAIM: Grzybb (w Efemerydzie zamiast Farnolisa) jest do uratowania (Kić)
    2. CLAIM: nigdy więcej efemeryda nie skrzywdzi moich awianów (Foczek)
    3. CLAIM: będę w stanie wykorzystać tą efemerydę jako niesamowite źródło energii (Foczek)
    4. Efemeryda Senesgradzka występuje na wielu Fazach jednocześnie i ma dopływy z różnych Faz (Żółw)
1. Farma krystalitów Mai Weiner
    1. Dlaczego krystality są niezbędne Oliwii do opanowywania efemerydy?
    2. Byt który opętał elementala/awiana Rolfa jest emocjonalnie związany z dzieckiem.
    3. Farma Krystalitów z młodą Mają która decyduje się przejąć dowodzenie na farmie.
1. Awiany z lokalnej montowni
    1. Awiany nowego typu są lepsze, fajniejsze ale i niebezpieczniejsze
    2. Spora część awianów montowanych w montowni jest dostosowana do linii produkcyjnej Filipa.
    3. Sporo awianów a wszystkie nowej generacji są zbudowane technikami sprzężenia Allitras-elementalna.
    4. Dlaczego "elemental burzy i gniewu" jako najlepsze miejsce znalazł ten klub (Panienka w Koralach)? Co było z tym miejscem lub ludźmi?
    5. Ściągane i pętane elementale są niewolone ALE nigdy nie są agresywne.
    6. Jakiego typu viciniusem stanie się awian Filipa?
    7. Awiany są strategicznie potrzebne zarówno mafii jak i Świecy
1. Echo starych technologii Weinerów
    1. Harvester nie ma dość energii by się obudzić
    2. Natalia pozyskała bezpiecznie kolejną część artefaktu
    3. Kajetan odnajdzie niebezpieczne elementy Harvestera przed Dukatem (Kić)
1. Wieża Wichrów
    1. Wieża Wichrów jest sprzężona portalem z nieznaną (chwilowo) fazą Allitras. Stałe powiązanie, póki są tam burze.
    2. Robert Sądeczny zażądał od Filipa zaprzestania tych konkretnych praktyk zagrażających terenowi.
    3. Bez tych praktyk Wieża Wichrów w obecnej formie nie ma sensu; lepiej zrobić nieco inne miejsce.
    4. Wieża Wichrów zostaje w rękach Filipa. Sądeczny nie ma jak się do niej przyczepić.
    5. Filipowi uda się utrzymać firmę JAKOŚ.
    6. Portal w Wieży Wichrów sam z siebie się nie rozproszy - jest tam i będzie spamował dziwne byty.
1. Byty z z Allitras
    1. Przepakowane elementale. Więcej zabawy, więcej energii. (Foczek)
    2. Byty wchodzące w elementale są świadome. (odpowiedź graczy)
    3. Aktualny elemental nie zdołał otworzyć portalu, ALE wysłał sygnał w eter.
    4. Lewiatan się zbliża. Jest już bardzo blisko.
1. Tamara x Sądeczny?
    1. Sądeczny, który podrywa Tamarę Muszkiet. To po prostu ekstra. (Kić)
1. Oczy Świecy, Oczy Sądecznego
    1. Sądeczny odzyskuje część "oczu" przez wprowadzenie linii produkcyjnej dron-awianów do fabryki Oliwii i Tamara wie o tym.
    2. Sądeczny wprowadził rezydenta u Myszeczki
    3. Tamara doprowadziła do sytuacji, gdzie Świeca nie jest na ciągłym podsłuchu
    4. Tamara nie jest w stanie rozwiązać wcześniejszej dezinformacji
    5. Oktawia ma wiedzę z Legionu Tamary z czasów misji przed opętaniem magitrowni.
    6. Sądeczny nasyła Zofię na szpiegowanie Tamary
1. Reputacja Świecy, Reputacja Sądecznego
    1. Sądeczny spinował akcję dezinformacyjną, masakrując reputację Świecy.
    2. Tamara skupia uwagę na wspieraniu przede wszystkim Tomasza Myszeczki. Duży, rozległy, potrzebuje pomocy. Brak mu wszystkiego.
    3. Sądeczny znajduje się w niełasce po tym, co tu się ogólnie ciągle dzieje - ma to naprawić.
    4. Zarówno Sądeczny jak Muszkiet są wyłączeni z akcji przez około tygodnia.
    5. Balans sił i energii po prostu się rozpadł na tym terenie - nikt nic nie kontroluje.
    6. Sytuacja z portaliskiem wzmacnia status quo na tym terenie - nikt nie jest w stanie uzyskać przewagi. (Raynor)
1. Robert Sądeczny, zapamiętany przez historię
    1. Jak wbić klin między Fornalisa a Sądecznego? (Kić)
    2. Aneta wróciła z Sądecznym z Muzeum Pary ORAZ wie, co tam się stało
    3. Hektor pomagał w budowaniu eliksiru który skrzywdził Katię
    4. Czy Aneta Rukolas się na serio zakocha w Sądecznym (Skażenie Wzoru)?: NIE WIEMY. (Żółw). 5/10.
    5. Wzór Anety jest trwale uszkodzony - wymaga czegoś nowego (Żółw)
    6. Sądeczny przekazał dowodzenie defensywne Wiaczesławowi (Żółw)
1. Lord Rezydent Sylwester Bankierz
    1. Sylwester został zesłany tu przez działania Newerji
    2. Sylwester przeprowadził udaną akcję zdobycia transportu Dukata
1. Tomasz Myszeczka - król viciniusów
    1. Sądeczny dostarczył maga, który zajmie się glukszwajnem czyszczącym czujniki ze Skażenia
    2. Tamara robi się bardzo podejrzliwa wobec Myszeczki
    3. Myszeczka staje po stronie Rezydenta Świecy
    4. Tomasz Myszeczka pomaga w tematach leczniczych. Poniesie koszty, ale zrekompensuje sobie wiele innych rzeczy. (Kić)
1. Dracena Diakon, w jakimś społeczeństwie
    1. CLAIM: Szybki, dyskretny awian. Trafi do Draceny. (Żółw)
    2. Kto i w jaki sposób pomoże Dracenie stać się częścią tego terenu dookoła? Należeć do czegoś?
    3. Dracena wykonuje działania mające uwolnić "Nową Melodię" od mafii 
1. Magitrownia Histogram, echo technologii Weinerów
    1. CLAIM: Dracena docelowo będzie w stanie zintegrować się z JAKIMŚ społeczeństwem (Kić)
    2. Magitrownia z Draceną działa dużo lepiej i taniej
    3. Dracena została przekonana, że magitrownia współpracuje z mafią a nie jest wykorzystywana
    4. Zarówno siły Dukata jak i Myszeczki zapewniają, by Skażenie nie dotarło do magitrowni.
    5. Tomasz Myszeczka - jego viciniusy stanowią źródło wczesnego ostrzegania na liniach magitrowni. (Raynor)
1. Gabriel Dukat - uleczenie jego syna
    1. CLAIM: Dukat lubi Paulinę (Kić)
    2. W jaki sposób byty z Allitras mogą pomóc dziecku Dukata?
    3. Aktywny Harvester będzie w stanie pomóc dziecku Dukata - stałym kosztem ofiar z ludzi (Żółw)
    4. Dukat nie ma gwałtownej reakcji na działania Sylwestra; da się wychować. Tupnął nogą bo musiał - and now we talk" (Kić)
    5. Kluczowe elementy soullinkowania dziecka Dukata pochodzą z wiedzy pozyskanej przez Sądecznego w ramach działania na tym terenie
1. Portalisko Pustulskie i odrodzenie Mausów
    1. Pojawia się zwiększenie ruchów antymausowych na tym terenie - mniej portaliska dla Mausów.
    2. Bolesław Maus rozpoczyna kampanię "keep magic low here".
    3. Portalisko jest silnie połączone z Allitras.
    4. Bolesław Maus kontratakuje przeciwko Tomaszowi Myszeczce - nie może tak być, że ciągle świnie niszczą portalisko.
1. Świńskie viciniusy i coś poszło nie tak
    1. Rolnik od świń założył ma misję - i przydupasa - mające udowodnić, że ze świniami dzieje się coś dziwnego
    2. W okolicy Męczymordy jest odpowiednia ilość pola rezydualnego by sprawić by znikające świnie były prawdą
1. Oktawia, efemeryczne echo Oliwii
    1. CLAIM: Oktawia dorośnie i zniknie. (Kić)
    2. CLAIM: Dracena i Oktawia będą miały duet. (Żółw)
    3. Oktawia chce pomóc Krzysztofowi, bo widzi w nim potencjał do pomocy Oliwii
    4. Marzyciel może nadal komunikować się z Oktawią; może jej śpiewać, gdy ona jest w Efemerydzie. (Żółw)
    5. Chaotyczność i nieufność / niestabilność Oktawii jedynie wzrosła. Ciągle odrzucana, discardowana, torturowana. (Żółw)
1. Wolność Eweliny Bankierz
    1. CLAIM "Newerja Bankierz x Gabriel Dukat - zostaną parą" (Kić)
    2. Docelowo Ewelina dostanie jakieś środki do życia; nie będzie żebraczką


# Narzędzia MG

## Opis celu misji

-

## Cel misji

-

## Po czym poznam sukces

-

## Wynik z perspektywy celu

-

## Wykorzystana mechanika

Postać (strona gracza):

| .Motywacja. | .Umiejętności. | .Magia. | .SiłySłabości. | .Znam.    | .Mam.      |.POSTAĆ. |
|   (-2) - 2  |     0 - 2      |  0 - 2  |    (-2) - 2    | Z+M = 0-6 | Z+M = 0-6  |         |

Opozycja (strona NPC):

* Baza: 2
* Aspekt: 3/aspekt, wariant z nożyczki/papier/kamień
* Modyfikatory: Skala, Sprzęt, Sytuacja (1-3 każdy)

Wynik:

| .Postać. | .Opozycja. | .Rzut.    | .Wynik.  |
|          |            | xx: +x Wx |   S-SS-F |

Wpływ:

* 8 kart / dzień
* każda porażka i skonfliktowany sukces = +2 pkt wpływu dla gracza
* każda karta = +1 wpływ dla MG
* każde 5 kart zaokr. w górę: +1 pkt wpływu dla gracza

---
layout: inwazja-konspekt
title:  "Polowanie na Mausównę"
campaign: powrot-karradraela
players: kić, raynor
gm: żółw
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [170808 - Duch Opery (MB, PB)](170808-duch-opery.html)

### Chronologiczna

* [170221 - Przecież nie chodzi o koncert... (temp)](170221-przeciez-nie-chodzi-o-koncert.html)

## Kontekst ogólny sytuacji

## Punkt zerowy:

## Misja właściwa:

### Dzień 1:

Trzy dni do koncertu. Antygona i Judyta się pogodziły z Kirą i Franciszkiem. Oczywiście, są w gospodarstwie bezużyteczne...

A jednak nie. Judyta okazała się być świetnym sadownikiem. Może pomóc w gospodarstwie. Antygonę dla odmiany zapędzono do podrasowywania internetu na poziomie software'owym.

Wpadł na pogaduchy Józef Krzesiwo do Franciszka. Lokalny "komendant policji". Pożalił się, że przyszła do niego miastowa uciekająca przed swoim eks, który obiecał że ją potnie. Franciszek nie wie co z tym robić; nie ma gdzie jej wsadzić i w ogóle. Franciszek poradził mu, by ten zamknął ją w celi i niech jego praktykantka, Łucja, zajmie się pilnowaniem dziewczyny i jej ochroną. Franciszek jeszcze raz się pożalił, że przecież chciał kolegę z policji parobka a dostał "laskę z miasta i to taką pyskatą". Tymczasem, do tej koleżanki Franciszka podchmala Krzesiwo.

Kira wyczuła emanację demoniczną. Pobiegła, w samą porę by zobaczyć jak Lusowicz, zarywający do Judyty, zajeżdża sobie w nogami demonicznymi grabiami Judyty... te grabie same się wyślizgnęły by zadać demoniczną ranę.

Kira i Franciszek zalewają Lusowicza wódką i Kira rozrywa połączenie demonicznego chłeptania. Franciszek się musiał przy okazji trochę napić, oczywiście; sam chłop pić nie będzie.

Przyjechał do nich lekarz i zabrał Lusowicza. Kira opierniczyła Judytę za zostawianie narzędzi bez nadzoru wśród ludzi; ta jest wstrząśnięta. Coś takiego się jeszcze nigdy nie stało. Nie powinno. Te czary nigdy tak jeszcze nie działały.

Franciszek przeszedł się na komendę przesłuchać tą miastową. Tam jest niestety tylko praktykantka. Kij w tyłku, służbistka... Franciszek się z nią trochę pokłócił i Łucja... aresztowała Franciszka za picie w pracy jako wójt. Ok. Franciszek jest tam gdzie chciał - przy młodej miastowej. Przyjdzie Józek i opierniczy Łucję. Tymczasem, trzeba pogadać z tą, Kingą...

* To jest nowa kobieta we wsi i ewidentnie zrobiła błąd bo jestem tu wójtem - Franciszek
* Przyszedł do mnie komendant Krzesiwo i opowiadał o pani sytuacji. Jak widzi pani, jesteśmy całkiem nieźli w zamykaniu ludzi. Dwie cele, w obu zamknięci niewinni ludzie - Franciszek
* Nie ukrywajmy, Józek Krzesiwo nie jest asem polskiego wywiadu. Czemu przyszła pani na to zadupie? Plus, nasza komenda ma tylko jednego Józka... i jedną Łucję, która wpakowała swojego pracodawcę za kraty. - Franciszek, wietrząc podstęp.

Kinga powiedziała, że jej eks - Michał Brukarz - wierzy, że ona coś mu ukradła. Ale NIE ukradła. I on ma przeszłość z narkotykami i ogólnie brutalnym działaniem... ona naprawdę się boi.

Zaraz potem dowiedział się jakoś o uwięzieniu Franciszka jego arcyrywal - Antoni Chlebak. Łucja Rowicz posłuchała, podpuściła... i aresztowała go za próbę skorumpowania jej (znaczy, funkcjonariusza). Chlebak i Franciszek chwilę ponarzekali razem na miastową Łucję, która "się nie zna jak się to robi na prowincji".

A wysłane przez Franciszka sieci wskazały jednoznacznie, że w okolicy jest jakiś INNY mag. I tak, jest tam ten nożownik (Brukarz). I szuka Kingi.

Kira jest sama, w pokoju. Słyszy kłótnię. Antygona i Judyta. Ostro się kłócą - Antygona atakuje Judytę za to, że ta skrzywdziła ludzi. Judyta mówi, że to nie jej wina; że to było dziwne. Kira zdecydowała się częściowo przerwać ten spór. Kira poszła tam, poprosiła Judytę o przyzwanie ich jeszcze raz i ona zbada te narzędzia. Kira określiła jednoznacznie: te narzędzia same z siebie nie mogłyby skrzywdzić Lusowicza. Były pod wpływem demonicznym. Czyli albo demon, albo demonolog...

Judyta - gdy usłyszała - zdecydowanie się rozluźniła. A i Antygona mniej na nią najeżdża. Obie trochę się... rozluźniły. Ale ktoś coś zrobił... Antygona zaoferowała poszukanie w hipernecie. Na pewno coś znajdzie... Judyta spojrzała z roświetloną nadzieją na Antygonę, ale Kira pokręciła głową... że niby co ona znajdzie, przepraszam?

Wieczór. Upragniony opierdol Franciszka... to jest, Łucji. Przez Józefa. Józef opierniczył Łucję zdrowo i kazał jej wracać do domu. Ona zaczęła protestować, bo kto z aresztantką zostanie? Józef jednak był nieubłagany. Do domu czas. Łucja pojechała... Franciszek zauważył, że Józef sobie tak lekko traktuje pracę policjanta.

Józef przeprosił Franciszka i Antoniego. Chciał z nimi pójść (i zostawić Kingę samą); niestety, Franciszek się upomniał o Kingę. Józef chciał zawrócić Łucję, ale Franciszek bardzo nie lubi wyzysku (też wyzysku praktykantek).

* Ty też nie byłeś cały dzień w pracy! I co? Nic się nie stało. - urażony Józef, że każą mu do pracy przyjść.
* Wiesz co, Józek, bo mnie Twoja stażystka uświadomiła, że cały czas jestem w pracy. Szach-mat, Józek. - zirytowany Franciszek.

Stanęło na tym, że Józef zostanie z Kingą a Łucja się wyśpi...

Kira i Franciszek się synchronizują. Nie spodobały im się krzyżowe wieści.

Kira i Franciszek nadają sygnał. Wiedzą, że w pobliżu jest jakiś mag. Są lokalnymi rezydentami. Oczekują, że mag bawiący się na ich terenie ALBO się pojawi, ALBO odejdzie. I faktycznie, mag dostał wiadomość... i zwiał. Ale zostawił za sobą echo.

### Dzień 2:

Franciszek poszedł spotkać się znowu z Kingą. Oczywiście, na miejscu Łucja, nie pozwalająca się Franciszkowi spotkać z Kingą. Jak Franciszek powiedział, że Kinga jest gościem... to Łucja zauważyła, że to Kinga powinna decydować, czy chce się spotkać się z Franciszkiem. I tak, chce. Łucja wpuściła Franciszka.

Franciszek wypytał Kingę o co chodzi. Dokładnie. Dowiedział się, że mag manipulował jej umysłem i umysłem Brukarza. Niestety, w wyniku przesłuchania Łucja wsadziła go do pierdla za molestowanie Kingi... ale dodatkowo, Franciszek dowiedział się, że Łucja i Michał grawitują w kierunku na Judytę. Oooki... plus, DRUGI raz w areszcie? A Józek śpi i znowu Franciszek dzień przesiedzi :-(.

Gdy Kira dowiedziała się wszystkiego (zewnętrzny hipernet Franciszka), facepalmowała. Franciszek ściągnął praktykantkę (zrobił miejsce pracy), to ma. Kira dowiedziała się o Judycie i się bardzo zainteresowała... czemu ktoś grawitowałby do Mauski? Zwłaszcza TAKIEJ?

Kira wydrukowała obraz Kingi. Pokazała go Judycie; ta nie kojarzy. Antygona poprosiła o zdjęcie i udała się do hipernetu i internetu. Po piętnastu minutach wróciła z bombą - dziewczyna (Kinga) była Mausem przed Zaćmieniem...

Ciekawym eksperymentem byłoby zbliżenie do siebie Judyty i Kingi; wygląda to jak dziwny atak na Judytę. Może lepiej jednak nie...

Ostra dyskusja o przeszłości, Mausach, złych magach i ogólnie. Judyta podłamana. Czemu ona? Ona nie jest groźna, nic nie umie. Ona tylko robi soczki. A jej mama była (przed Zaćmieniem) świetnym piekarzem. To też nie pasuje. 

Kira buduje serię dron z jednym podstawowym zadaniem - mają znaleźć nożownika. Ich jedynym celem życiowym jest go znaleźć i poinformować o punkcie przebywania. Antygona się z nimi zintegrowała, by wzmocnić połączenie. Bez żadnego problemu znaleźli chłopaka; idzie w kierunku na areszt w Stokrotkach. Oki. Dobra, to przestaje być śmieszne.

Kira poszła obudzić Józefa. Ten wstał, nieszczęśliwy, poszedł na komendę, opierniczył Łucję... i kazał jej robić kółeczka patrolować wieś. Czemu? By zeszła z ognia jakiemuś psychopatycznemu nożownikowi. To robota dla weteranów. A Józek kiedyś był w saperach...

Sztacheta komendanta Józefa vs nożownik. 1:0. Flawless. Nożownik nawet nie wiedział co go trafiło. Wiejski cios w czerep.

Przybył mentalista Krzysztof Cygan do Kiry, wezwany mocą 1 surowca. Kira jako rezydentka wyjaśniła sytuację: Brukarz i Grzybnia. Do skanu i naprawy. Cygan przeanalizował obydwóch; tak, jakiś mag wchodził w interakcje z ich umysłami; da się naprawić. On naprawi, nie widzi problemu.

Dodatkowo wyszła ponura sprawa - Kinga ma specyficzną mutację krwi (stara tkanka magiczna). Jest szczególnie "słodka" dla defilerów. Wyraźnie ktoś próbował sprawić, by Judyta przesunęła się na pozycję defilera. Sęk w tym, że... Judyta nie nadaje się na defilera. Nigdy nie była defilerem. Nie ma predylekcji. Ale napastnik wyraźnie o tym nie wiedział.

Czyli to wszystko było atakiem na Judytę. Młoda ma niezłego stalkera...

Kira ostrzegła zarówno Antygonę jak i samą Judytę. Niech wiedzą. I niech zawiadomią opiekunów Judyty...

# Progresja

* Judyta Maus: ma stalkera, niebezpiecznego maga, który chce ją... zdefilerować? splugawić? zMausić?

# Streszczenie

Ktoś poluje na Judytę Maus i nie zawaha się poświęcić ludzi, by ją dorwać. Chce wyraźnie ją zdefilerować. Franciszek ma perypetie z praktykantką swojego komendanta, Kira ma perypetie z Franciszkiem i czarodziejkami. W końcu Kira wygnała stalkera Judyty używając autorytetu Świecy a Franciszek zapewnił, by żadnemu człowiekowi nie stała się krzywda.

# Zasługi

* mag: Kira Zajcew, wyciągająca Franciszka z kicia i twardo wykorzystująca przywileje Świecy i swoją reputację. Wypędziła tajemniczego maga.
* mag: Franciszek Baranowski, przesiedział dwa dni (z dwóch) w areszie własnej wsi. Szybka głowa pozwala mu na rozwiązywanie problemów potencjalnego łamania Maskarady.
* mag: Judyta Maus, na którą ktoś poluje. Flirtuje z ludźmi, co się źle dla nich kończy. 
* mag: Antygona Diakon, bezużyteczna aż do momentu, gdy mogła wejść w inkarnację dron i przeszukiwanie hipernetu. Wieś nie służy jej wykazywaniu się ;-).
* czł: Józef Krzesiwo, komendant policji w Stokrotkach. Dość leniwy (jeden dzień w tygodniu pracy?!), kiedyś w saperach, świetnie włada sztachetą. Dobry kolega Franciszka.
* czł: Łucja Rowicz, praktykantka miastowa, policjantka z kijem w tyłku, dwa razy aresztowała Franciszka dwa dni pod rząd. Józef jest podłamany. Chciał krzepkiego parobka.
* czł: Kinga Grzybnia, ofiara tajemniczego maga; kiedyś Mauska (straciła moc bo Zaćmienie). Ma specyficzną krew działającą jak narkotyk dla defilerów.
* czł: Władysław Lusowicz, chciał sobie poflirtować z młodą Mauską, skończył z demonicznymi grabiami w stopie, zalany przez Franciszka, pojechał do szpitala.
* czł: Michał Brukarz, 28-latek z szemraną przeszłością (narkotyki # noże), którego tajemniczy mag wykorzystał do swoich celów (ataku na Judytę).
* czł: Antoni Chlebak, przeciwnik polityczny Franciszka Baranowskiego zamkniętego przez Łucję do aresztu za próbę łapówki; połączony z Franciszkiem w niesmaku.
* mag: Krzysztof Cygan, mentalista Świecy na posyłki. Z przyjemnością pomógł Kirze naprawić dwie ludzkie ofiary tajemniczego maga polującego na Judytę.

# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Okólno-Trocin 
                1. Stokrotki
                    1. Zachód
                        1. Gospodarstwo Kiry i Franciszka, z fajnym sadem i całkiem niezłymi elektrowniami ;-).
                    1. Gminna komenda policji, zbudowana za pieniądze z Unii Europejskiej; ma areszt i dwie cele. Franciszek sporo w niej przesiedział.

# Skrypt

-

# Czas

* Dni: 2
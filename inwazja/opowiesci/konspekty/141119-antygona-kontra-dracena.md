---
layout: inwazja-konspekt
title:  "Antygona kontra Dracena"
campaign: powrot-karradraela
gm: żółw
players: kić, dzióbek
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [170412 - Przed teatrem absurdu (HB, AB, DK)](170412-przed-teatrem-absurdu.html)

### Chronologiczna

* [170412 - Przed teatrem absurdu (HB, AB, DK)](170412-przed-teatrem-absurdu.html)

## Kontekst ogólny sytuacji

## Punkt zerowy:

Dracena Diakon. Antygona Diakon. Dwie siostry, córki tych samych rodziców, ale los je rozdzielił. Dracena ma nowego ojca - Draconisa Diakona, terminusa Millennium a Antygona została czarodziejką Srebrnej Świecy. Siostry utraciły ze sobą kontakt na długo. Padło wiele gorzkich słów, wiele wściekłości.

Do czasu.

Antygona Dolores Kebab Diakon, zwana popularnie "Anią" (niespodzianka) natknęła się na zrzeszenie magów działające wewnątrz Srebrnej Świecy. Ania, z natury czarodziejka lękliwa i szukająca przede wszystkim spokoju i sposobu na przetrwanie (idealna kariera: gdzieś daleko od innych magów, gdzie może ciężko pracować i nikt nie będzie jej o nic pytał) uwikłała się w całą tą sprawę dość mocno. I skontaktowała się z siostrą, Draceną, z którą nie rozmawiała już od co najmniej 2 lat. Grupa ta jest BARDZO mocno umocowana politycznie; trudno będzie coś im zrobić i oni sami są zbyt pewni siebie.

Po nie takiej krótkiej rozmowie, Ania i Dracena wpadły na pewien plan. W skrócie:
- Ania infiltruje grupę i wystawia im Dracenę.
- Dracena daje się wystawić. Wpada w pułapkę (będąc sama w sobie pułapką)
- Albo uzależnianie Draceny wchodzi do gry, albo impreza staje się pułapką przy użyciu pryzmaturgii albo... Ania ma jeszcze jeden plan, ale nie powiedziała jaki łagodnej z natury Dracenie.

## Misja właściwa:

Marian Łajdak jest zdesperowany. Stara się jak może, ale jego ukochana, Dracena Diakon w żaden sposób nie chce mu zaufać, nie chce się do niego zbliżyć. Owszem, chodzą razem do łóżka, razem się dobrze bawią, ale Dracena mówi Marianowi zawsze, że to - jak się umawiali - tylko seks bez zobowiązań. A on chciałby coś jeszcze, coś więcej... a co gorsza, wszystko wskazuje na to, że Dracena wpada właśnie w jakieś kłopoty. Ktoś ją atakuje. A Dracena wszystkich od siebie odpycha.

Marian spotkał się z Pawłem Sępiakiem. Przedstawił mu problem - dziwne magiczne światło które miało uderzyć w jego ukochaną Diakonkę (Dracenę). Przyznał się, że się zakochał i poprosił o pomoc - Dracena go odcięła i nie pozwoliła mu niczego robić. Co więcej, niejaki Mikado Diakon uważa, że to wszystko wina Mariana Łajdaka. Bo niby Marian chce zaciągnąć Dracenę do łóżka i ją od siebie uzależnić. Co za to Paweł będzie miał? Marian przeskanuje AI "Aegis" i będzie pudelkiem Ilony by móc przekazać wszystko co wie o AI "Aegis" Pawłowi do wykorzystania w jego własnych golemach. Do tego Marian zgłosi się na ochotnika do kontroli GS "Aegis" 00002. Mimo, że to miał robić Paweł (ale po 00001 nikt nie chciał tego robić).
Marian obiecał pomoc Sabiny Diakon.

Marian spotkał się z Sabiną. Poprosił o pomoc w sprawie Draceny. Sabina szybko sprawdziła co wie o Dracenie - (10v10->11). Dracena jest adoptowaną córką Draconisa Diakona, jest z boku rodu. Jest Diakonem z krwi, ale nie z krwi Draconisa. Dracena jest z boku rodu, nie do końca angażuje się w tematy. Cybergothka, bardzo dużo czasu spędza z ludźmi. Rozwija technomantyczne talenty. 
Mikado Diakon jest uczniem terminującym u Draconisa. Jest bliżej Draconisa niż jego własna córka. Zawsze zazdrościł Dracenie ojca, czasem mieli scysje o Draconisa, ale stał murem za Draceną. Jeśli Mikado stoi za jakimikolwiek atakami na Dracenę, coś jest bardzo nie w porządku.
Nic z tego co Marian chciał dać Sabinie jej się nie spodobało - nawet spektakl magiczny i spektakl świateł. Nope, he owes her a big one.
Wszystko wskazuje na to, że tylko i wyłącznie Mikado i Dracena wiedzą, co to było za zaklęcie.

Sabina radośnie przebrała Pawła Sępiaka za cybergotha. Sabina potem poszła do Mikado. Dopytać się o co chodziło.
Sabina dowiedziała się, że Mikado próbował pomóc Dracenie, ale ona go zablokowała. I to Mikado bardzo, bardzo martwi. Mikado ma nic nie mówić, nic nie robić, z nikim nie rozmawiać i nie robić absolutnie niczego w tym temacie. Ale Mikado ma to gdzieś i powiedział Sabinie co i jak. Mikado twierdzi, że Dracena osłania napastnika - scramblowała zaklęcie zanim Mikado zdołał coś sensownego wyczytać. Akcja ataku wyglądała mniej więcej tak - lalka była przekazana Dracenie przez człowieka i reflektor, odpowiednio przedtem zmodyfikowany użył szerokiej wiązki. Tylko posiadacz lalki został trafiony. Szczęście chciało że lalka została przekazana koleżance - i to koleżankę trafił promień. Efekt: "miłość i chodź do łóżka". Ale Mikado wyczuł, że wiązka miałaby inny efekt na Dracenę. Podejrzewa, że twórca zaklęcia miał dostęp do krwi Draceny.
Mikado podejrzewa oczywiście Mariana Łajdaka. Lub KADEM. Lub jakiegoś Sępiaka (bo lalki).

Oki. Sabina wróciła na KADEM. Udała się do kolegi technomanty i poprosiła o jednorazową, mniejszą wersję VISORa. Chce zdobyć informację o JEDNYM zaklęciu rzuconym w konkretnym miejscu. Potem poprosiła innego kolegę o wysłanie pierwszaka, który pójdzie do "Działa Plazmowego" i zdobędzie dane z tamtego reflektora. Oczywiście, Sabina płaci w naturze ;-).
(8+2v9/7/5->7) Pierwszak został zidentyfikowany przez Dracenę, która powiązała go od razu z Marianem Łajdakiem, ale pierwszak dał radę zdobyć Sabinie to co ona chce wiedzieć. Zaklęcie to miało na celu zmianę Draceny w chętną maskotkę po powiedzeniu odpowiedniej grupy słów kluczowych - wszystko wskazuje na to, że ktoś bardzo chciał zaciągnąć Dracenę do łóżka. Miało zrobić coś jeszcze, coś silnie powiązanego z biologią i działaniem samej Draceny. Ktoś kto to zrobił bardzo, BARDZO dobrze wiedział o Dracenie.

Sabina zrobiła głęboki skan wiedzy o Diakonach. Natrafiła na postać Ani - Antygony Dolores Kebab Diakon. Czarodziejki będącej golemitką, technomantką, uwodzicielką i katalistką. Z punktu widzenia mocy mogłaby to zrobić a jak chodzi o jej lokalizację, znajduje się w Zaćmionym Sercu (w ramach kontraktu ma odnowić systemy Zaćmionego Serca). Jest to rodzona siostra Draceny, czarodziejka mająca dużą niechęć do swojego rodu i będąca lojalną czarodziejką Srebrnej Świecy. Nie utrzymuje kontaktów z magami rodu Diakon ani z siostrą.

Oki. Przy tych informacjach, Sabina jest w kropce. Nie wie co z tym zrobić. Nie powiedziała Pawłowi, że to siostra ale powiedziała że to Diakonka. Paweł wie, że to sprawa osobista na linii Dracena - Antygona. Oboje zgodnie postanowili przygotować obronę, przygotować się jak na wojnę i przygotować się na atak Ani i obronić Dracenę.
Minęły dwa dni. Wieczór, będzie impreza. Paweł poprosił o VISOR i dostał autoryzację. Umieścił sensory wcześniej i w chwili wejścia był w stanie użyć wszystkich informacji ze strony VISORa.
(10v13->14) Dracena wprowadziła i świetnie zamaskowała emitery zwiększające wpływ jej muzyki co jej umożliwi jej kontrolowanie emocji tłumu. Dodatkowo załatwiła obniżkę cen biletów - chce mieć jak najwięcej ludzi i kontrolować emocje tłumu. Zmodyfikowała też część reflektorów podobnym zaklęciem jak miała być trafiona. Dodatkowo wmontowała w pulpit wiązkę mającą przekierować falę od niej w wybrane kierunki.
Dracena stała się najbardziej zaawansowaną pułapką jaką widział klub "Działo plazmowe".

Sabina i Paweł spojrzeli na siebie i stwierdzili że jeszcze i na to się muszą przygotować.

Sabina szybko pchnęła agenta (pierwszaka) by znalazł Anię. Niestety, Ania jest niedostępna, gdzieś na Srebrnej Świecy.

Tłum zaczyna się zbierać do "Działa plazmowego". Zaczyna się niedługo super impreza. Sabina i Paweł mają gotowe tarcze, ale ich nie uruchamiają. Ani nigdzie nie ma. Dracena weszła na scenę, impreza się zaczyna. VISOR zobaczył dwóch magów KADEMu - ukrywającego się Mariana Łajdaka i nerwowego, choć nadrabiającego miną Lucjana Kopidoła (z Instytutu Pryzmaturgii). Łajdak siedzi na imprezie i wzdycha. Kopidół ma czarny płaszcz i maskę. Nie ten styl, ale jakoś przebrany.

Sabina podeszła do Lucjana Kopidoła i zaczęła z nim rozmowę. Kopidół bardzo nerwowy, powiedział, że przyszedł bo prosiła go przyjaciółka. Zidentyfikował Anię Diakon jako "Petitię". Super. Sabina pociągnęła go za język - Lucjan wziął ze sobą Pryzmatyczny Wyzwalacz. Artefakt eksperymentalny który powstał w Instytucie Pryzmaturgii i który sprawia że silne emocje przez Fazę Daemonica stają się rzeczywistością. Przebija pryzmatem brak magii. Im silniejsze uczucia, tym silniejsza moc...
Sabina użyła ograniczonych słodkich umiejętności - wycofała go NATYCHMIAST. Byłaby skłonna nawet posunąć się do tego, że powie jego żonie. Na szczęście nie musiała. Lucjan Kopidół gdy usłyszał że wpakował się w wojnę pomiędzy dwie Diakonki to natychmiast zwiał - KADEM ma politykę absolutnej izolacji i absolutnej nieingerencji i apolityczności.

Następną osobą z którą porozmawiała Sabina był Marian Łajdak. Ten wzdycha do Draceny a ona każe mu się bezczelnie wycofać. Być "z drugiej linii" i pomagać im poza klubem. Marian się bardzo nie zgadza, on chce chronić SWOJĄ Dracenę (vs9!). Sabina się słodko uśmiechnęła i posunęła się do szantażu - albo sobie pójdzie albo ona powie Dracenie że on tu jest (#4!). No i konflikt (11v9->13). Poszedł z podkulonym ogonem. Nie będzie widzial swojej kochanej Draceny.

Czas na ruch Ani Diakon. Do klubu weszła cybergothka z dużym pluszowym misiem. VISOR przeskanował misia na rozkaz Pawła - miś jest golemem napakowanym zaklęciami. Dracena nie może tego nie zauważyć. Dracena się uśmiechnęła i wyciągnęła rękę w kierunku misia. W tym momencie Paweł strzelił korzystając ze stroboskopów i laserów na cyber-imprezie. Jeden laser więcej, ale ten ZABIJA NIEDŹWIEDZIE A ZWŁASZCZA PLUSZOWE. Na twarzy Draceny absolutny szok, dziewczyna (ludzka) nic nie zauważyła.
Dracena ma +33 do stresu i impreza toczy się dalej.

Tymczasem po drugiej stronie konsternacja. Ania nie potrafi wytłumaczyć swoim mocodawcom co się dzieje; była pewna, że to wystarczy do złapania i zniewolenia Draceny. Wyraźnie ktoś Draceny pilnuje, ktoś jej chroni. W związku z tym Wiktor Sowiński stwierdził, że sprawę należy rozwiązać inaczej. Żadnych więcej ataków na imprezie. Zaatakują gdy Dracena wróci do domu. Ania proponowała oponować, ale jedno spojrzenie Wiktora ją całkowicie uciszyło. Jej plany - jak się okazało - są nic nie warte. Wiktor pokaże Ani jak to się robi.

Tymczasem, jako, że na imprezie nie ma już więcej ataków, Paweł z nudów uruchomił VISOR do skanowania i sprawdzenia zaklęć na MARTWYM PLUSZOWYM NIEDŹWIEDZIU. (14). Zaklęcia misia były tak wycentrowane, by Dracenie było bardzo przyjemnie. Zdjęłoby to ją z akcji, więc misiogolem przejąłby konsoletę i to on sterowałby emocjami tłumu. De facto, przejąłby wszystko co Dracena przygotowała jako wzmacnianie. Ten fragment z konsoletą był najgłębiej ukryty. Dodatkowo tam było jeszcze jedno zaklęcie - bardzo silna amplifikacja jakiejś własności Draceny w formie 'burst' (od MG: uzależniania). Ale nie są w stanie sprawdzić jakiej.

Impreza się kończy. Dracena bardzo dyskretnie demontuje cały system zabezpieczeń i wszystkie elementy magiczne jakie przygotowywała. Cały czas kombinuje i zastanawia się o co chodzi i czemu plan Ani się nie udał. Wie, że jest obserwowana. I Sabina zdecydowała się pójść do Draceny z nią porozmawiać. Wpierw Sabina spróbowała zrozumieć działania Draceny (7->3) ale jest to po prostu niemożliwe; Dracena zachowuje się inaczej niż zwykle.

Dracena została zaskoczona obecnością Silurii. Wie, że jest podsłuchiwana, więc nie wie co komu mówi. Siluria wygrała konflikt (9v9). "Zastawiam pułapkę na siostrę". Tu Wiktor zwrócił się do Ani, która wyjaśniła, że Dracena jest bardzo arogancka; ona to przewidziała i powiedziała, że miś miał przejąć kontrolę i Dracena wpadłaby w ręce Wiktora. Dalej dociskana przez Silurię, Dracena dodała, że Ania zaatakowała pierwsza. Skłamała, że nie wie o co Ani chodzi (Siluria wszystko to zauważa, że Dracena kłamie) i Dracena dodała, że sama siostry pierwsza nie zaatakuje. Chce złapać ją w pułapkę. "Kuzynko, gdybyś nie wiedziała o co chodzi, nie przyjmowałabyś tego tak spokojnie". Siluria uderzyła mocniej i powiedziała, że Dracena chroniła siostrę przed Marianem i innymi. W końcu Siluria nacisnęła bardzo mocno, zaczęła się z Draceną kłócić i wyprowadziła Dracenę z równowagi. Dracena nie wytrzymała i wypaliła, że współpracuje z Anią i polują na grubszego zwierza.

...To był ten moment, gdy Ani naprawdę zrobiło się smutno. Bo koło niej znajdują się magowie z tej grupy którą Ania próbuje infiltrować...
Widząc przerażenie Draceny świadomej podsłuchów Siluria i Paweł sprawdzili podsłuchy. Faktycznie, byli podsłuchiwani magicznie.

Siluria kazała VISORowi zlokalizować źródło podsłuchów i sama poszła porozmawiać z Draceną na osobności. Dracena przez moment nie kontaktowała, po czym szybko wzięła telefon komórkowy by zadzwonić pod jakiś numer. Używając umiejętności walki wręcz, Siluria zabrała Dracenie (zupełnie niewprawnej) telefon nie robiąc jej krzywdy. Krótka (~1min) pyskówka w której Siluria powiedziała, że Dracena zrobiła coś bardzo głupiego i zagroziła wywołaniem poważnego incydentu Millennium - KADEM, na co Dracena odpowiedziała ze wściekłością, że Siluria wywołała właśnie potężny konflikt Millennium - Świeca. Silurii to zupełnie nie rusza.

Siluria zażądała od Draceny informacji, by mogła jej pomóc. Dracena nadal żąda swojego telefonu. Siluria zagroziła wezwaniem Draconisa, co całkowicie zmieniło plany Draceny. W tym świetle to ONA (Dracena) zdecydowała się zadzwonić do Draconisa. Gdy powiedziała Silurii, że chce zadzwonić do Draconisa, Siluria oddała Dracenie telefon. I Dracena zadzwoniła do ojca. Powiedziała, że Ania (Antygona) wpadła w koszmarne kłopoty, jest fizycznie w okolicach Trzech Czarnych Wież w SŚ i że jest koło niej co najmniej trzech wrogich jej magów SŚ. Draconis zauważył, że Ania JEST czarodziejką Świecy (to samo Siluria). Dracena powiedziała mu, że to jest silnie powiązane z "Wirusem" i że ona nie może mówić jawnie, bo tu są podsłuchy i szpiedzy (domyślnie: Siluria). Czarodziejkę KADEMu szlag trafił. Draconis powiedział, że nie może działać osobiście, ale może zatrzymać katastrofę. I że Dracena ma iść do pokoju, nie wychylać się, nic nie robić, zostawić "Wirus" w spokoju.

O dziwo, Dracena posłuchała. Mimo sugestii Silurii, że ta może pomóc Dracena powiedziała, że Siluria już "dość pomogła" (w domyśle: "tym złym") i że KADEM ze swoją świętą zasadą izolacji nie jest w stanie nie tylko jej pomóc ale i nikomu pomóc. Siluria stwierdziła, że jest z rodu. Na to Dracena zakwestionowała ten status. Silurię znowu szlag trafił ale nie kontynuowała pyskówki.
Zamiast tego wykorzystała swoje moce polityczne i udała się do Draconisa. Spotkanie z Draconisem było holograficzne; Draconis nie jest w stanie spotkać się z Silurią osobiście (odkręca sprawę Ani i Draceny).

Holograficzne spotkanie Draconisa i Silurii odbyło się w miłej atmosferze. Siluria powiedziała Draconisowi sytuację, nie mówiąc JAKI to artefakt KADEMu ale mówiąc, że eksperymentalny, niebezpieczny i miał być użyty na ludziach. To wystarczyło by dzień Draconisa stał się jeszcze gorszy. Siluria dodała też, że już się tym zajęła, nikt nic nie wie, i ona nie chce zrobić poważnej krzywdy Dracenie (ani innej czarodziejce rodu Diakon). Draconis powiedział, że on skontaktował się z magiem SŚ o odpowiedniej moralności (Iza Łaniewska) by ów terminus uratował Antygonę z kłopotów. Draconis powiedział Silurii co wie - Antygona znalazła coś na SŚ. Coś śmiertelnie niebezpiecznego, coś, co popchnęło osobę tak płochliwą jak Antygona do działania. Powiedziała o tym Dracenie i łagodna z natury Dracena była skłonna posunąć się tak daleko by zaatakować. By zostać pułapką.

Draonis powiedział, że tak tego nie zostawi, ale że Dracena jest spacyfikowana. Jeśli Dracena coś spróbuje w tym temacie zrobić, Draconis osobiście wyśle skrzydło terminusów do pomocy Świecy. I Dracena o tym wie. Powiedział też, żeby Siluria się nie martwiła tym tematem, bo jest on "daleko poza zainteresowaniem KADEMu i Silurii oraz jej gałęzi i działań".

"W konflikcie Diakon-Diakon lepiej mieć Diakona po mojej stronie. Jak silniejszy nie wygra, przynajmniej pójdą razem do łóżka" - Marian Łajdak.
"Jakbym mogła Ci jakoś pomóc..." "Z Draceną? <<ironiczny uśmiech>>" - Siluria / Draconis.

# Streszczenie

Antygona i Dracena robiły pozorowaną wojnę domową ze sobą, by wciągnąć w śmiertelną pułapkę Wiktora Sowińskiego ze Szlachty i pozbyć się jego mrocznego cienia. Były tak zdesperowane, że aż nie wahały się wciągnąć KADEMu w tą "walkę". Siluria i Paweł jednak doprowadzili do przerwania tej walki i Dracena przypadkowo wygadała się, że to pułapka. Jako, że Antygona była dookoła wrogów... konsekwencje dla młodej Diakonki mogły być fatalne. Draconis spacyfikował Dracenę, choć wezwał do akcji Izę Łaniewską by ta uratowała Antygonę. Na razie - Szlachta wygrywa.

# Lokalizacje:

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Centrum
                        1. Cyberklub Działo Plazmowe
                        1. KADEM Primus
                            1. Instytut Technomancji
                    1. Nowy Kopalin
                        1. Emoklub Zaćmione serce

# Pytania:

Ż - Jak objawił się ostatni (prawie skuteczny) atak na czarodziejkę?
B - Artefakt został wywołany przypadkiem przez kogoś innego.
Ż - Jakie nieracjonalne zachowanie Draceny tak bardzo przeraża Mariana Łajdaka?
K - Dąży do tego, by impreza była jak najlepiej dopięta.
Ż - Co sprawia, że Marian jest aż tak zdesperowany by pomóc Dracenie?
B - To on jest podejrzewany o zorganizowanie tego zamachu przez Mikado Diakona.
Ż - Jakie wyjaśnienie Dracena wykorzystuje by odciąć Mariana od jakichkolwiek form działania?
K - Restraining order.
Ż - Na co liczy tajemniczy napastnik (Ania)?
B - Be the last man standing.
Ż - Dlaczego lokalizacja jest tak bardzo istotna?
K - Bo to o nią wszystko się rozgrywa.
Ż - Co takiego jest w tej lokacji co się ujawni gdy nie będzie tam czarodziejki (i tylko wtedy)?
B - Jest tam ktoś z przeszłości Draceny (Wiktor Sowiński) kto bardzo nie chce się z Draceną spotkać?
Ż - Czym Dracena dała radę zablokować Mikado Diakona?
K - Doprowadziła do tego, że jak się wtrąci, wszystko zeskaluje.
B->Ż - Co powoduje, że collateral damage jest akceptowalny?
Ż - Bo na nikim w to zaangażowanym, poza Wami, nikomu nie zależy.
K->Ż - Co sprawia, że nasze postacie mogą w to wejść w miarę bezpiecznie?
Ż - Nie tylko nie jesteście powiązani z żadną ze stron - nikt do tego jeszcze nie chce irytować KADEMu
Ż - Skąd wziął się w klubie pryzmatyczny wyzwalacz?
B - Mag KADEMu za niego odpowiedzialny (Marian Trzosik) przegrał go do Lucjana Kopidoła na okres jednego dnia
Ż - W jakich okolicznościach mag KADEMu spotkał piękną czarodziejkę?
K - W drodze do Zaćmionego Serca.

# Stakes:

- N/A

# Idea:

- Wprowadzenie obu sióstr w warunkach nietypowych: Draceny i Antygony.
- Wprowadzenie "Wirusa".

# Zasługi

* mag: Siluria Diakon jako próbująca pomóc kuzynce słodka istotka.
* mag: Paweł Sępiak jako mistrz snajperstwa przy użyciu VISORa.
* mag: Dracena Diakon jako cybergothka próbująca trzymać wszystkich maksymalnie na odległość.
* mag: Antygona Diakon jako czarodziejka skłonna poświęcić "dziewictwo" siostry dla zniszczenia "Wirusa".
* mag: Mikado Diakon jako najwierniejszy uczeń Draconisa.
* mag: Marian Łajdak jako zakochany w Dracenie KADEMowiec skłonny się solidnie zadłużyć by ona była bezpieczna.
* mag: Draconis Diakon jako wytrawny polityk i wybitny terminus Millennium chroniący córkę, lecz nie pozwalający jej na nadmiar swawoli.
* mag: Lucjan Kopidół jako KADEMowiec podatny na piękne Diakonki.
* mag: Wiktor Sowiński jako jeden z "Wirusa", niekomfortowo blisko Ani Diakon.

# Czas:

* Opóźnienie: 14 dni
* Dni: 2

# Wątki

- Szlachta vs Kurtyna
---
layout: inwazja-konspekt
title:  "Na żywym organiźmie Draceny..."
campaign: prawdziwa-natura-draceny
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [160907 - Dracena... Blakenbauer? (PT)](160907-dracena-blakenbauer.html)

### Chronologiczna

* [160907 - Dracena... Blakenbauer? (PT)](160907-dracena-blakenbauer.html)

## Kontekst ogólny sytuacji
## Punkt zerowy:

Dracena jest niestabilna, po ostatniej akcji jej wzór jeszcze bardziej się pogorszył - jak Paulina się spodziewała. Walczą ze sobą aspekty: Diakonka, Wiła, Spustoszenie, pochłonięci ludzie, terminus Marek, urocza cybergothka, pająk astralny, dotknęła Paulinę. Za dużo. Sama Dracena jest silnie zagubiona w tym wszystkim. Mimo potencjalnych problemów wynikających z pacjentami, Paulina zdecydowała się skupić przez chwilę na Dracenie - jeśli jej się nie uda, czarodziejka może się zbyt wypaczyć.
Jednocześnie Paulina wie co nieco o Diakonach. Nie rozumie, dlaczego Dracena jest "sama". To najdziwniejszy przypadek jaki widziała. No i skąd ta "destabilizacja wzoru"?

Aktualny stan Draceny: (8 do wydania), Diakonka 4 (musi być 8), Wiła 5, Spustoszenie 4 (musi być 5), Pająk 1, Cybergothka 4, Strach 2, Weapon 2

### Misja właściwa:

Dzień 1:

3 rano. Wszyscy dzielnie śpią. Paulina ma piękny sen erotyczny... po chwili się budzi i orientuje, że faktycznie, ma sen erotyczny... ale sama sobie pomaga w tej przyjemności. Otrząsnęła się i jeszcze lekko zamroczona idzie zobaczyć czy Dracena śpi. Śpi. Zanim ją obudziła, powstrzymała ją Netheria - kazała spojrzeć Paulinie katalitycznie. I Paulina spojrzała - nad Draceną jest wir energii magicznej. Połączył się aspekt mecztapauka i wiły i stworzył potężny wir energetyczny dotykający całego miasteczka i drenujący z nich energię erotyczną wzmacniając Dracenę i jej aspekty.

Trzeba to uziemić nim będzie za późno...

"Dildo nieskończonej przyjemności... nie takie artefakty już powstawały" - Netheria, z radosnym uśmiechem.

Krok pierwszy - muszą się połączyć. Tak, żeby nie doszło do niczego więcej. Niestety, są w sumie (-1) -ScF+SPN (Netheria -2+2 = 0, Paulina -2+1=-1). Ale Paulina jest katalistką, więc (+2) = +1. Udało się, dziewczyny dobrze pracują razem.

Krok drugi - uziemienie w dildo. Niestety, energie są za silne; bez zbędnych emocji Paulina poświęciła Kingę (i tak śpi) i wykorzysta ją jako najbardziej rozerotyzowany komponent konwersji energetycznej. Dildo zostało uziemieniem - stało się niestabilnym artefaktem.

Netheria zaproponowała krok trzeci - użycie jej krwi by wzmocnić Diakonkę w Dracenie. Tak, by wzmocnić jej kontrolę. Netheria skupi się na nekromantycznym wygaszaniu wiły, Paulina skupi się na naprawianiu wzoru Draceny pod kątem wzoru Netherii i krwi Netherii. Nieskomplikowane ;-) - walczymy z (21). Zajęło to jakieś 2 godziny, ale się udało. W Dracenie aspekt Diakonki został wzmocniony i wir magiczny przestał działać.

Netheria jeszcze poprosiła Paulinę o ustabilizowanie tego dildo. Są quarki. Mają "granat erotyczny" który da się w coś docelowo przerobić.

Obie dziewczyny poszły spać. Wykończone. Ale będą na nogach rano.

Rano pierwsza obudziła się Paulina. Netheria wykończona jeszcze śpi. Jest wcześnie - 9 rano ;-). Paulina zdecydowała się przejść dookoła budynku zobaczyć ewentualne ślady Skażenia; zamiast tego wyczuła flarę magiczną - jakiś mag się ogłasza. Flara wskazuje na Srebrną Świecę. Mag jest niedaleko. Paulina poszła go odwiedzić - zostawiła tylko Netherii notkę co i jak.

Terminus Świecy Jerzy Pasznik okazał się być bardzo niesympatycznym magiem. Stwierdził, że będzie przeprowadzał śledztwo i czy Paulina jest za to odpowiedzialna? Nie. Paulina powiedziała, że Kinga tu jest (pytał) - ma dla niej przesyłkę. Paulina ubłagała go by poczekał i porozmawiał z Kingą; zgodził się. Strasznie biurokratyczny i nieprzyjemny. Wyraźnie chce się wykazać. Podejrzewa aurę kralotyczną.

Paulina szybko wróciła porozmawiać z Netherią i Kingą. Tymczasem wchodząc do budynku usłyszała krzyczące na siebie Netherię i Dracenę. To znaczy: Dracena krzyczy, Neti odpowiada ze spokojem. Dracena krzyczy, że Netheria wiedziała od samego początku, że jest mistrzynią szpiegów. Netheria odpowiada ze spokojem, że Dracena nie jest małym dzieckiem i nie wszystko kręci się dookoła niej.

Paulina weszła by rozładować sytuację. Weszła w devious - rozczarowanie, smutek, "zawiodłam się na was". Netheria przeprosiła z gracją, Dracena ma meltdown samoniszczący. Skuliła się i zaczęła rozpaczliwie szlochać. Paulina przypadła ją pocieszać; Dracena się uspokoiła. Dracena dostała zadanie zrobić jajecznicę. Poszła się tym zająć. Gdy Paulina przedstawiła Netherii problem, Neti się uśmiechnęła. Terminus na służbie uwiódł nieszczęsną rekonwalescentkę - ją. Ale wpierw muszą dostać kryształy...

Kinga wyszła z pokoju. Porusza się na lekko chwiejnych nogach i ma luźną suknię. Niestety, jakkolwiek jest operacyjna... nadal na nią działa przekierowanie energii. Dlatego nie może mieć na sobie spodni ;-). Dostała solidne jedzenie. Niestety, ani Netheria ani Paulina nie są w stanie jej pomóc - samo musi przejść. O godzinie zero...
Pojadła, Paulina powiedziała że przyszła przesyłka. Przyniósł ją posłaniec. Jakiś mag Świecy. Kinga pójdzie z nim porozmawiać - sama...

Paulina chciała iść z Kingą, ale zarówno Kinga jak i Netheria jej to odradziły. Parę minut po wyjściu Kingi Paulina stwierdziła, że martwi się czy Kinga to wytrzyma. Neti powiedziała, że mała szansa - ten stan jest zwany "kralotyczną rują". Paulina chciała iść Kingę ratować, ale Neti zauważyła, że mają swoją rekonwalescentkę. Szlachetna pani doktor która pomaga terminusom i którą wykorzystuje terminus - można zrujnować mu karierę. Paulina się nie zgadza z metodami Netherii, ale ta jest bardzo przekonywująca - terminus jest kłopotem a w taki sposób zostanie zneutralizowany. Co chcą z terminusem? Ma czyścić. Jeśli COKOLWIEK z tego co tu się dzieje wyjdzie na jaw, kariera terminusa jest złamana. Z ciężkim sercem, Paulina dała Kindze 15 minut. Po czym poszła do terminusa wiedząc co zobaczy.

To znaczy, MYŚLAŁA że wie co zobaczy. Weszli w relację master-slave. On zaczął na nią krzyczeć, ona zaczęła reagować, jemu udzieliła się kralotyczna ruja i Paulina zastała ich w bardzo... jednoznacznej dominująco-niewolniczej pozycji. Nawet nie musiała udawać szoku. JEMU się to podoba, JEJ nie, ale pochłonęła ją moc jaką wchłonęła. Paulina weszła bardzo aggressive - oblała ich wężem z wodą i zaczęła na nich krzyczeć - ustawiła się między nią a nim, obronnie, i zaczęła napastować terminusa - powiedziała, że PRZYPROWADZI Kingę. A on, terminus...

Paulina złamała tego terminusa. Zrobi wszystko, czego Paulina sobie życzy. Będzie trzymał się z daleka i czyścił wycieki magiczne, plus utajni co tu się dzieje. Z punktu widzenia Świecy - on się tym wszystkim zajmuje. Ten terminus nie był tu w ogóle wysłany. Po prostu chciał się wykazać i znaleźć jakąś anomalię, by wyszło na to, że może wrócić na front. Czy coś. Jest jeszcze niedoświadczony.

Paulina pomogła Kindze się pozbierać, wzięła quarki i wyszła ze starszą lekarką. Paulina NADAL ma poczucie winy, nawet spore... ale... Świeca jest zbyt niebezpieczna w tej sytuacji, prawda?

Kinga w miarę szybko wróciła do jakiejś normy; jest starszą czarodziejką i z różnymi rzeczami miała do czynienia. Zaordynowała Dracenie kryształy Mausów - by ją odpowiednio dostosować. Netheria rozpostarła parasol energetyczny nad Draceną (astralika) by odpowiednio wzmocnić odpowiednie komponenty (cybergothka, Diakonka) podczas, gdy Dracena się dostosowuje do nowej formy.

Paulina skupiła się na rozmowie z Kingą - jak można zrobić, by Dracena miała te wszystkie rzeczy, których od niej "chcemy", potrzebujemy. Kinga powiedziała, że jej strach (punkty strachu) można przekierować - jeśli np. Dracena bałaby się strasznie Spustoszenia, to wtedy ten strach może wzmocnić Spustoszenie. Więc to nie jest jeszcze stracone. Ale to Kinga proponuje zrobić już po tym, jak się ustabilizuje Dracenie aura. Paulinie się ten pomysł nie podoba... ale nie ma lepszego.

Aktualny stan Draceny: (5 do wydania), Diakonka 6 (musi być 8), Wiła 5, Spustoszenie 4 (musi być 5), Pająk 1, Cybergothka 5, Strach 2, Weapon 2

Dracena i Netheria będą zajęte do końca dnia. Ale Paulina może coś jeszcze zrobić. 

Dzień 2:

Paulina - niespodzianka - obudziła się pierwsza. W nocy nie działo się nic złego. Paulina przygotowała śniadanie oraz znalazła muzykę w typie cybergoth i puszczając. Random cybergoth playlist. Netheria przyszła i powiedziała, że pięknie pachnie. Ma przygotować cyber-imprezę? Zaczęła przygotowania już wczoraj; może to odpalić jeśli Paulina chce i wtedy pod koniec dnia będzie mała impreza, taka na 100 osób w temacie ;-). Neti potrzebuje sporo osób by móc odpalić astralikę. Paulina zmartwiła się - a wiła? Neti nie ma pojęcia jak to zrobić bez ryzykowania wiły jeśli ma odpalić się cybergothka. Za to jest szansa że odpali się Diakonka...

Paulina wyczuła drganie energii - Dracena weszła do pokoju. Aura jest zupełnie nie taka jak powinna. W tej chwili normalnie aura Draceny pływa; walczy ze sobą samą. W tej chwili aura Draceny jest spójna - wszystkie aspekty skupione na tym samym tworzące... jakiś stan psychiczny i energetyczny. Dracena stwierdziła, że jest głodna. Bardzo głodna. Ale ani jajecznica, ani Paulina, ani nikt nie dał jej reakcji jaką chciała dostać... zasyczała. Nie ma Draceny, jest tylko... drapieżnik. Chciała wyjść polować; wykryła coś co może zjeść, ale Paulina ją powstrzymała. Dracena zaczęła rzucać zaklęcie mające wszystkich zdjąć z akcji by mogła się pożywić i Paulina kazała odpalić dildo. Dildo odpalone, Dracena straciła przytomność - wchłonęła całość tej energii.

Step 1: Kinga i Paulina muszą się połączyć. 
Step 2: Praca na (21) mająca doprowadzić Dracenę do normalnego stanu. Udało się - niestety, Dracena nie jest w stanie być na imprezie. Nie wytrzyma.

Stan jest... ciekawy. Dracena jest głodna, zdestabilizowana, acz się stabilizuje (3 punkty). Nie wytrzyma imprezy. Mają do dyspozycji - Netherię, Kingę, Paulinę i jednego terminusa. Netheria upiera się, że najważniejsze jest, by zamknąć Diakonkę - jeśli tego nie zrobią, Dracena revertuje. Kinga twierdzi, że w Dracenie nie ma balansu. 

Ogólnie, fatalnie. Cybergothka jest za słaba. Draceny "prawie nie ma" w Dracenie. Wszystko się popieprzyło... krótka kłótnia Pauliny i Netherii (Neti chce Dracenę zabrać na leczenie do Millennium, Paulina jest twardo przeciw) zaowocowała decyzją o destabilizacji Draceny. Ale wpierw - z Netherią pod kontrolą Kingi zdecydowała się na przygotowanie "paczki nekromantycznej" mającej wyniszczyć wiłę w Dracenie; dzięki temu uda się wycofać niewłaściwą progresję i otworzą się nowe opcje konstruowania czarodziejki.

Niestety, psychoza jako koszt brzmi koszmarnie, ale Paulina woli mieć tymczasowo Dracenę w psychozie niż NIE MIEĆ Draceny w ogóle. Czar potrzebny jest bardzo wysoki - 26. Kinga mówi dokładnie co i jak ma być robione a Netheria pomaga przy zbudowaniu paczki nekromantycznej. Sukces!

Paulina poprosiła Netherię o "specjalny payload" - porządny, magiczny BONDAGE. Unieruchomiona Dracena, niemagicznie i skutecznie... a Neti jako Diakonka jest naprawdę skuteczna jak chodzi o więzy i krępowanie. Odpowiednio przygotowane komponenty, sprzęt... w sam raz by przygotować pierwszą fazę psychozy.

I z ciężkim sercem, Dracena zostaje zdestabilizowana. Z nadzieją na lepsze jutro...

Aktualny stan Draceny: (11 do wydania), Diakonka 6 (musi być 8), Wiła 3, Spustoszenie 3, Cybergothka 5, Strach 3, Weapon 1

# Progresja

* Dracena Diakon: stan psychozy po destabilizacji post-kralotycznej
* Jerzy Pasznik: bardzo dziwna relacja z Kingą Toczek; zagraża jego karierze i jest dlań... nową obserwacją (impulsem?)
* Netheria Diakon: okazała się być wpływowa w Millennium; podobno jest mistrzynią szpiegów

# Streszczenie

Pojawił się terminus, który chciał powęszyć; szczęśliwie nadmiar energii erotycznej i biedna Kinga wyłączyły go z akcji i przeniosły do "team Dracena". Głód i destabilizacja pożerają Dracenę coraz skuteczniej; Paulina by ratować osobowość Draceny postanowiła doprowadzić do destabilizacji post-kralotycznej z nadzieją, że zresetują nieudane mutacje i przetrwają jakoś psychozę. Netheria i Kinga współpracują ściśle z Pauliną.

# Zasługi

* mag: Paulina Tarczyńska, próbująca kierować Dracenę ku lepszemu, walcząca o "tą Dracenę która była" nawet kosztem destabilizacji i wycofania postępu.
* mag: Dracena Diakon, która rozpaczliwie walczy o swoją osobowość, aczkolwiek... nie wyszło. Została zdestabilizowana by próbować jeszcze raz - lepiej. 
* mag: Kinga Toczek, wpakowała się w nadmiar ekstatycznej energii i przypadkowo weszła w erotyczny galimatias z terminusem Pasznikiem.
* mag: Netheria Diakon, pokazująca swoją mroczniejszą stronę i próbująca wyjaśnić Paulinie co się dzieje z Draceną by ratować dziewczynę
* mag: Jerzy Pasznik, terminus Świecy chcący sprawdzić pobieżnie co tu się dzieje by się wykazać i wpadający w erotyczny galimatias i sidła Netherii

# Lokalizacje


1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Czarnoskał
                1. Marzanka, do której zawitał terminus Jerzy Pasznik
                    1. Droga Gościnna, chyba główna w Marzance.
                        1. Domek Zadumy, nadal kontrolowany przez Paulinę, Dracenę, Kingę i Netherię
                        1. Domek Radości, objęty przez terminusa Jerzego Pasznika
 
# Wątki



# Skrypt

- brak

# Czas

* Dni: 2
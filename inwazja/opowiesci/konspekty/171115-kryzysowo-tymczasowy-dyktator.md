---
layout: inwazja-konspekt
title:  "Kryzysowo tymczasowy dyktator"
campaign: wizja-dukata
players: kić, raynor
gm: żółw
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [171105 - Epidemia dezinhibicji (PT, DA)](171105-epidemia-dezinhibicji.html)

### Chronologiczna

* [171105 - Epidemia dezinhibicji (PT, DA)](171105-epidemia-dezinhibicji.html)

## Kontekst ogólny sytuacji

### Wątki konsumowane:

?

## Punkt zerowy:

-

## Potencjalne pytania historyczne:

* Czy Ewelina Bankierz stanie się dominującą siłą na tym terenie działającą z poziomu magitrowni?
    * Domyślnie: TAK
* Czy Mausowie zostaną uznani za wszelkie zło tego świata przez działania młodej Łucji Maus?
    * Domyślnie: TAK

## Misja właściwa:

**Dzień 1**:

Eliksir Aerinus żre więcej energii niż kiedykolwiek. Jest to zagrożenie. Więc - Daniel przycina poziomy energii. Wszystkie poziomy idą w dół.
Apoloniusz natychmiast odezwał się do Daniela - żąda więcej energii! Są w trakcie eksperymentu. Powiedział Danielowi, że potrzebuje energii bo Daniel przejdzie do historii jako przydupas Apoloniusza! Apoloniusz jest opętany klątwożytem i pragnie przejść do historii. Daniel... taaaaak. Daniel zaczął manipulować Apoloniuszem, że on zostanie przydupasem! Ale musi innych klientów przekonać! Apoloniusz... się zgodził. Ostrzegł, że Daniel ma czas do jutra... inaczej wszystko mu tam wybuchnie.

Sylwester Bankierz połączył się hipernetowo z Danielem. Powiedział, że magitrownia może posłużyć jako amplifikator i jeśli Paulina coś zrobi na ten klątwożyt, to wtedy jest nadzieja... że magitrownia zamplifikuje. Pomysł Warinsky'ego. Chwilę potem hipernet padł - glukszwajny przebiły się przez Niezniszczalną Kazamatę i nadajnik hipernetowy przestał działać. Sylwester Bankierz z narażeniem życia wyciągnął stamtąd Warinsky'ego; nie został rozżarty przez glukszwajny.

Tymczasem helikopter przyleciał. Z Eweliną i Kają. Kaja przedstawiła Ewelinę Danielowi (i wzajemnie) - wyraźnie jest szczęśliwa widząc Ewelinę zdrową i całą. Ewelina nie przejęła władzy; rozesłała agentów po perymetrze, po czym spytała Daniela jak może pomóc w tym kryzysie. Gdy Daniel powiedział o problemach ze świniami i biednym Warinskym, Ewelina wysłała Kaję z helikopterem. Niech Kaja się tym zajmie. Daniel wkręcił na pokład siebie oraz Paulinę.

Tymczasem Paulina próbuje przy użyciu magimeda pomóc Anecie Rukolas. Przepalona, Echo Eteru, Eliksir we Wzorze. Ciężko poszło (SS) - ale udało się Paulinie zdjąć Echo Eteru. Niestety, przy okazji Aneta została permanentnie zmieniona. Transport zmienił jej Wzór. Dodatkowo Paulina przygotowała dodatkowe zapasy szczepionki przeciw klątwożytowi. Z poczuciem zadowolenia zadzwoniła do Wiaczesława.

Wiaczesław powiedział telefonicznie, że ma tu taką ciężarówkę co mu porwała kilku nazioli i on ma zamiar ją wysadzić. Paulina woli poczekać, Wiaczesław chce wysadzać. Tymczasem Paulinę zatrzymała Dracena - powiedziała, że ciężarówkę z Melodią zaatakowało kilku nazioli i ona ich porwała. Paulina lekko się przeraziła i skrzyczała Wiaczesława, że on chce jej Melodię wysadzić. Wiaczesław spoko. Niech będzie. Paulina ma czas. Wiaczesław też wypaplał, że złapał Mauskę - Łucję Maus, która stała za atakiem na Myszeczkę. Torturuje ją.

Paulina uderzyła - nie wolno torturować. Wiaczesław odbił - Paulina nie jest w stanie go zatrzymać. Zgodnie z doktryną Wąż, cierpienie musi być zbalansowane. On się dowie kto za tym stoi, jak wygląda plan i się upewni, by to się nie powtórzyło. Paulina niechętnie, ale przestała się z Wiaczesławem kłócić; wie, kiedy nie wygra. Skontaktowała się z Melodią i poprosiła, by ta jak najszybciej uwolniła nazioli i przyjechała do magitrowni. Melodia się zgodziła; not one to cause trouble.

I wtedy przyszedł Marzyciel i powiedział, że jest Ewelina Bankierz. Paulina nędznie westchnęła i poszła.

Drony Draceny pokazały że nie ma Bankierza ani w zameczku ani w kazamatach. Są we wsi; objęli jedną z chat. Polecieli helikopterem i helikopter wylądował. Kaja robi "TVN Uwaga" z simulacrami. Daniel i Paulina poszli do chaty; tam Sylwester, związany Warinsky i 4 Strażników (golemy). Sylwester mówi, że wezwał glashundy; wie od seirasa Mausa, że jest tu gdzieś torturowana Mauska a on jest Rezydentem.

Sylwester nie zgadza się na to, by go ewakuować. WPIERW glashundy. Pokazał, że zależy mu na dobrobycie Mauski - nie jest to dziewczyna jego, ale jest na JEGO terenie. ON za nią odpowiada. To nie robota Sądecznego; Sądeczny tak się nie zachowuje. Więc Sylwester wie, że w pobliżu jest jakiś psycho degenerat. Glashundy rozwiążą problem. Ale Sylwester wysłał Warinsky'ego helikopterem i został z dwoma strażnikami.

Paulina skontaktowała się z Wiaczesławem PONOWNIE. Powiedziała, że on ma glashundy na ogonie. MUSI uwolnić Mauskę. Starła się ściśle i wygrała - Wiaczesław pozwoli Paulinie odbić Mauskę tak, by to było maksymalnie upokarzające dla Świecy. Gdzieś niedaleko dawnej bazy Świecy (słowami Wiaczesława "świniarium"). I faktycznie - Paulina poszła i znalazła tam umierającą Łucję. Z wielkim trudem udało jej się rozbroić pułapkę przy użyciu thugów Eweliny (ech, Eweliny) i Paulina dobiła się do samej Łucji. Łucja poparzona kwasem i umierająca. Paulina ją uratowała, acz jej Paradoks zcyborgizował Łucję; tylko tak mogła utrzymać ją przy życiu. Ech.

Tymczasem Daniel ściągnął Sądecznego do magitrowni - wpierw skomunikował się z Elwirą a potem z samym obudzonym Sądecznym. Przekonał go, że na terenie jest masakra, zrzucił problemy na Wiaczesława (nie wiedząc nic o nim) oraz że trzeba coś zrobić bo cały region pójdzie wpierdut. Sądeczny dał się przekonać...

W końcu, spotkanie na szczycie. Sylwester Bankierz, Robert Sądeczny, Ewelina Bankierz. Po krótkiej niezgodzie stwierdzili, że trzeba coś z tym wszystkim zrobić. Ewelina zaproponowała oddanie wszystkich sił jednemu dowódcy. Sądeczny zaproponował Daniela, ale ani Daniel ani Sylwester się nie zgodzili. Wtedy - w odpowiedzi na to, że Paulina pomogła Kai zrobić porządek w Eliksirze Aerinus (Kaja poprosiła o środki na neutralizację zarażonych, m.in. Apoloniusza i Paulina z ciężkim sercem dała jej coś co pomoże w planach Kai i uratuje magów Eliksiru) - Ewelina zaproponowała Paulinę: była rezydentką, miała poparcie Świecy i ogólnie daje radę. Wszyscy trzej się zgodzili i tak Paulina została dyktatorem...

...przez przypadek. Z woli Eweliny.

## Wpływ na świat:

JAK:

1. Co na tej sesji dookoła jakiegoś konfliktu czy zdarzenia było fajne? Co chcesz by w ramach tego się poszerzyło?
1. Każdy gracz wydaje swoje punkty na generowanie rzeczywistości i wątków.

CZYLI:

* Kaja Maślaczek, cokolwiek złego zrobi nie może podejrzenie ani działania paść na Daniela (Raynor)
* Wiaczesław Zajcew, osłania region przed najazdami innych magów (Kić)
* Czy Mausowie zostaną uznani za wszelkie zło tego świata przez działania młodej Łucji Maus? NIE bo tortury Wiaczesława
* Czy Ewelina Bankierz stanie się dominującą siłą na tym terenie działającą z poziomu magitrowni? PRAWIE stała się siłą znaczącą

JAK:

* 2: MUSIK: gracz mówi w jaki sposób jedna z obcych postaci wspiera coś związanego z motywacją JEGO postaci
* 2: CLAIM na wątku / postaci / okoliczności
* 2: do elementu Historii lub przeszłego konfliktu dodajemy "ale" lub "oraz"
* 2: dodajemy pytanie, które musi zostać odpowiedziane na jakiejś z przyszłych misji
* 2: odpowiadamy na pytanie, które pojawiło się na jakiejś z misji
* 3: gracz mówi w jaki sposób jedna z obcych postaci wspiera coś związanego z motywacją JEGO postaci
* 3: zmieniamy kontekst okoliczności czegoś z Historii - scena z przeszłości / fakt?
* 3: do elementu spoza Historii dodajemy "ale" lub "oraz"
* 3: dodajemy znaczącego NPC powiązanego z elementem Historii
* 3: pozyskanie przez dowolną postać znaczącego surowca mającego sens z perspektywy misji
* 3: dodanie nowego elementu Historii
* 3: dodanie przyszłego lub przeszłego faktu; czegoś, co się wydarzyło lub wydarzy

Z sesji:

1. Żółw (13)
    1. Są silne dowody na działania niezgodne z prawem Apoloniusza Bankierza i Kaja je znalazła.
    1. Dukat jest zadowolony z Sądecznego, który podjął właściwą decyzję odnośnie regionu.
    1. Sylwester Bankierz kontroluje grupę glashundów na tym terenie na czas nieokreślony.
    1. Istnieje niewielka populacja awianów które uległy dezinhibicji przez bliskość Białego Zameczku.
1. Kić (7)
    1. Aneta jest w dużo lepszym stanie.
    1. Kaja zapewnia Paulinie Katię i nikt nie może jej overridować.
1. Raynor (3)
    1. Z pomocą Eweliny i Warinsky'ego, włączenie technik antyklątwożytowych do magitrowni. Moduł / emiter antyklątwożytowy.

# Progresja

* Paulina Tarczyńska: została dyktatorem na dwa tygodnie z ramienia Sylwestra Bankierza, Eweliny Bankierz i Roberta Sądecznego
* Apoloniusz Bankierz: przeciwko niemu znaleziono dowody współpracy z mafią - i to takie "nieetyczne".
* Kaja Maślaczek: zdobyła haka na Apoloniusza Bankierza. Ma na jego sprawki liczne dowody.
* Sylwester Bankierz: dowodzi grupą ~8 glashundów na tym terenie przez czas nieokreślony. Zapewniają hipernet.
* Ewelina Bankierz: dowodzi uzbrojonym helikopterem bojowym "Princessa Jeden" wraz z 6 żołnierzami.

# Plany

* Ewelina Bankierz: chce uwolnić się od łańcuchów Newerji przy użyciu Daniela Akwitańskiego oraz ma okazję uderzyć w Apoloniusza Bankierza. Na pewno to zrobi.
* Kaja Maślaczek: chce zmaksymalizować zniszczenia na Apoloniuszu Bankierzu i pomóc Ewelinie odzyskać kontrolę nad regionem.
* Marcin Warinsky: chce pokazać się jako Wartościowy Członek Zespołu. Nieważne komu - ktokolwiek spyta i jest pod ręką.
* Robert Sądeczny: oddał Paulinie dowodzenie i dał jej dwa tygodnie by sprawdzić, czy poradzi sobie lepiej niż Sylwester jako Rezydentka. Obserwuje.
* Gabriel Dukat: jest zadowolony z tego, że Sądeczny współpracował z innymi siłami by opanować epidemię. Nie zamierza niczego zmieniać w dowodzeniu regionem.
* Wiaczesław Zajcew: gra w przesunięcie Pauliny na stanowisko Rezydentki; chwilowo skupia się na zwalczaniu zewnętrznych sił atakujących teren.
* Sylwester Bankierz: chce znaleźć degenerata odpowiedzialnego za krzywdę dokonaną Łucji Maus i odzyskać panowanie nad sytuacją korzystając z braku Tamary.

# Streszczenie

Klątwożyty dezinhibicyjne zarażają coraz szerszą populację magów; szczęśliwie Paulina ma szczepionkę w magitrowni. Po ściągnięciu wszystkich trzech przywódców - Eweliny Bankierz, Sylwestra Bankierza i Roberta Sądecznego, Paulina została na 2 tygodnie dyktatorem dowodzącym wszystkimi siłami (robota Daniela). Kaja uderzyła w Apoloniusza i znalazła dowody jego złych czynów. Wiaczesław torturował tajemniczą pilot awiana - Łucję Maus - i pozwolił Paulinie ją odbić dla jej chwały. Sylwester Bankierz zdobył glashundy jako wsparcie.

# Zasługi

* mag: Paulina Tarczyńska, pomogła Anecie, uratowała Łucję, posprzeczała się z Wiaczesławem i niespodziewanie została dyktatorem na 2 tygodnie.
* mag: Daniel Akwitański, zwołał spotkanie na szczycie, NIE został dyktatorem (a proponowano), zmienił magitrownię w centrum dowodzenia i pracuje nad emiterem antyklątwożytowym.
* mag: Melodia Diakon, dotarła by pomóc Paulinie; jej ciężarówka była zagrożona przez Wiaczesława. Paulina dała jej immunitet i Melodia dotarła do magitrowni.
* mag: Wiaczesław Zajcew, torturuje Łucję Maus za jej atak na Myszeczkę, nie atakuje Melodii (bo Paulina) po czym w efektowny sposób pozwala Paulinie odbić Łucję by miała szacun na dzielni.
* mag: Łucja Maus, narwana pilot czarnego awiana, solidnie torturowana przez Wiaczesława. Skończyła umierająca, uratowana przez Paulinę. Wiaczesław ma wyczucie dramatyzmu.
* mag: Apoloniusz Bankierz, zarażony jakimś klątwożytem dezinihibicyjnym; Daniel go spowolnił a Kaja wykończyła.
* mag: Sylwester Bankierz, wykazał się niezwykłą na rezydenta Świecy odwagą i wyciągnął Warinsky'ego spod glukszwajnów. Wezwał glashundy do ratowania Łucji Maus. Oddał siły Paulinie.
* mag: Robert Sądeczny, nie może spać w magimedzie bo Daniel budzi. Oddał Paulinie siły (choć optował za Danielem) na 2 tygodnie. Po czym wrócił do spania.
* mag: Marcin Warinsky, wymyślił emisję antyklątwożytową przy użyciu magitrowni jako super-anteny. Potem wyżarły z niego energię glukszwajny i uratował go Sylwester.
* mag: Ewelina Bankierz, przyleciała helikopterem bojowym (różowym) z grupą agentów (żołnierzy) i zabezpieczyła teren magitrowni chcąc pomóc Danielowi. Oddała siły Paulinie.
* mag: Kaja Maślaczek, osiągnęła triumf swojego życia - uratowała Ewelinę, ukrzywdziła Apoloniusza... i teraz może lojalnie wspierać ten region.

# Lokalizacje

1. Świat
    1. Primus
        1. Mazowsze
            1. Powiat Pustulski
                1. Męczymordy
                    1. Magitrownia Histogram, centrum dowodzenia antyklątwożytowego.
                1. Byczokrowie
                    1. Niezniszczalna Kazamata, w której doszczętnie zniszczono resztki bazy świecy metodą "na racicę"
                    1. Chaty, gdzie schronił się Sylwester Bankierz wraz z Warinskym i magimedem Tamary

# Czas

* Opóźnienie: 0
* Dni: 1

# Wątki kampanii

1. Stan aktualny Pauliny Tarczyńskiej
    1. CLAIM: Paulina zostanie rezydentką Świecy na tym terenie a Tamara jej prawą ręką - terminusem bojowym.
    2. CLAIM: Paulina nie będzie musiała uciekać z terenu niezależnie od okoliczności (Kić)
    3. Magimed zostaje u Pauliny jako trwały surowiec. (Kić)
    4. Sądeczny uznał Paulinę za gracza a nie pionek - docenił to czym jest. (Kić)
    5. Paulina dostaje wsparcie od Millennium. (Kić)
1. Stan aktualny Krzysztofa Grumrzyka
    1. Po tym jak Krzysztof pomógł Filipowi, współpraca się zacieśniła - lepsza kalibracja Firmy i Firmy.
1. Stan aktualny Kociebora Dyrygenta
    1. CLAIM: okazja na poznanie i zaprzyjaźnienie się Kociebora z Kajetanem.
    2. Sądeczny daje Kocieborowi własną agencję detektywistyczną za zasługi i ratunek Dobrocienia
    3. Oliwia zapewnia Kocieborowi drony, które pozwolą na obserwowanie i śledzenie. Nowy biznes Oliwii.
    4. Customizowany awian, nie taki jak Czarny Ptak dla Kociebora. Taki czołg.
    5. Maja próbuje w najbardziej nieoczekiwanych momentach (i nieodpowiednich) okazać Kocieborowi sympatię i pomoc.
    6. Prawdziwym bohaterem Mai stał się Kociebor, który potraktował ją łagodnie i z sympatią.
1. Stan aktualny Daniela Akwitańskiego
    1. CLAIM: Myszeczka jest ważnym partnerem biznesowym Daniela (Raynor)
    2. Sądeczny w swoim investigation odkrywa, że Tamara zadziałała wbrew Danielowi i on nie chciał stawać przeciw niemu
    3. Robert uważa, że współpraca z Danielem oznacza, że Daniel jest dużo bardziej cenny niż myślał wcześniej
    4. Konflikt z Sylwestrem Bankierzem - oferował największą wartość, Daniel był w stanie przekonać go że jest wartościowym partnerem. (Raynor)
    5. Działania Daniela i Tomasza nie zostały wykryte przez ani Sądecznego ani Tamarę. Przez nikogo. (Raynor)
1. Stan Mazowsza i okolicy
    1. Filip aktywnie wspiera Paulinę w redukcji pola magicznego i Skażenia na tym terenie.
    2. W jaki sposób można odepchnąć w eterze nadmiar energii, by pozbyć się kłopotu bez skrzydła katalistów, terminusów i logistyków?
    3. Te pieprzone świnie się rozmnożyły. Pasożytnicze świnie z eteru. (Prezes)
    4. Doszło do wielokrotnego złamania Maskarady na tym terenie.
    5. Energia Eteru i Efemerydy Senesgradzkiej mutuje inne efemerydy w eterze. Zwłaszcza te świńskie. Mamy wylęgarnię. (Żółw)
    6. Doszło do Skażenia Primusa efemerydami świńskimi w ogromnym stopniu. Są WSZĘDZIE.
1. Efemeryda Senesgradzka i jej stan
    1. CLAIM: Grzybb (w Efemerydzie zamiast Farnolisa) jest do uratowania (Kić)
    2. CLAIM: nigdy więcej efemeryda nie skrzywdzi moich awianów (Foczek)
    3. CLAIM: będę w stanie wykorzystać tą efemerydę jako niesamowite źródło energii (Foczek)
    4. Efemeryda Senesgradzka występuje na wielu Fazach jednocześnie i ma dopływy z różnych Faz (Żółw)
1. Farma krystalitów Mai Weiner
    1. Dlaczego krystality są niezbędne Oliwii do opanowywania efemerydy?
    2. Byt który opętał elementala/awiana Rolfa jest emocjonalnie związany z dzieckiem.
    3. Farma Krystalitów z młodą Mają która decyduje się przejąć dowodzenie na farmie.
1. Awiany z lokalnej montowni
    1. Awiany nowego typu są lepsze, fajniejsze ale i niebezpieczniejsze
    2. Spora część awianów montowanych w montowni jest dostosowana do linii produkcyjnej Filipa.
    3. Sporo awianów a wszystkie nowej generacji są zbudowane technikami sprzężenia Allitras-elementalna.
    4. Dlaczego "elemental burzy i gniewu" jako najlepsze miejsce znalazł ten klub (Panienka w Koralach)? Co było z tym miejscem lub ludźmi?
    5. Ściągane i pętane elementale są niewolone ALE nigdy nie są agresywne.
    6. Jakiego typu viciniusem stanie się awian Filipa?
    7. Awiany są strategicznie potrzebne zarówno mafii jak i Świecy
1. Echo starych technologii Weinerów
    1. Harvester nie ma dość energii by się obudzić
    2. Natalia pozyskała bezpiecznie kolejną część artefaktu
    3. Kajetan odnajdzie niebezpieczne elementy Harvestera przed Dukatem (Kić)
1. Wieża Wichrów
    1. Wieża Wichrów jest sprzężona portalem z nieznaną (chwilowo) fazą Allitras. Stałe powiązanie, póki są tam burze.
    2. Robert Sądeczny zażądał od Filipa zaprzestania tych konkretnych praktyk zagrażających terenowi.
    3. Bez tych praktyk Wieża Wichrów w obecnej formie nie ma sensu; lepiej zrobić nieco inne miejsce.
    4. Wieża Wichrów zostaje w rękach Filipa. Sądeczny nie ma jak się do niej przyczepić.
    5. Filipowi uda się utrzymać firmę JAKOŚ.
    6. Portal w Wieży Wichrów sam z siebie się nie rozproszy - jest tam i będzie spamował dziwne byty.
1. Byty z z Allitras
    1. Przepakowane elementale. Więcej zabawy, więcej energii. (Foczek)
    2. Byty wchodzące w elementale są świadome. (odpowiedź graczy)
    3. Aktualny elemental nie zdołał otworzyć portalu, ALE wysłał sygnał w eter.
    4. Lewiatan się zbliża. Jest już bardzo blisko.
1. Klątwożyty dezinhibicyjne
    1. Klątwożyty dezinhibicyjne trwale dostosowały się do okolicy. Bardzo trudno będzie je wyplenić. (Żółw)
    2. Farnolis zaraził obszar dawnego Pentacyklu klątwożytem dezinihibicyjnym. (Żółw)
    3. Klątwożyty dezinhibicyjne trwale Skaziły Biały Zameczek. (Żółw)
    4. Jeden typ klątwożyta. Mutujący, ale jeden. W ten sposób da się z tym poradzić - i to zrobił ich wspólny Paradoks (Kić)
    5. Dalia została wyleczona z klątwożytów i już ich nie złapie (Kić)
1. Odbudowa Farnolisa
    1. Farnolis zaczął przejmować władzę na obszarze dawnego Pentacyklu; odbudowując pod areną obszar wpływu. (Żółw)
1. Tamara x Sądeczny?
    1. Sądeczny, który podrywa Tamarę Muszkiet. To po prostu ekstra. (Kić)
1. Oczy Świecy, Oczy Sądecznego
    1. Sądeczny odzyskuje część "oczu" przez wprowadzenie linii produkcyjnej dron-awianów do fabryki Oliwii i Tamara wie o tym.
    2. Sądeczny wprowadził rezydenta u Myszeczki
    3. Tamara doprowadziła do sytuacji, gdzie Świeca nie jest na ciągłym podsłuchu
    4. Tamara nie jest w stanie rozwiązać wcześniejszej dezinformacji
    5. Oktawia ma wiedzę z Legionu Tamary z czasów misji przed opętaniem magitrowni.
    6. Sądeczny nasyła Zofię na szpiegowanie Tamary
1. Reputacja Świecy, Reputacja Sądecznego
    1. Sądeczny spinował akcję dezinformacyjną, masakrując reputację Świecy.
    2. Tamara skupia uwagę na wspieraniu przede wszystkim Tomasza Myszeczki. Duży, rozległy, potrzebuje pomocy. Brak mu wszystkiego.
    3. Sądeczny znajduje się w niełasce po tym, co tu się ogólnie ciągle dzieje - ma to naprawić.
    4. Zarówno Sądeczny jak Muszkiet są wyłączeni z akcji przez około tygodnia.
    5. Balans sił i energii po prostu się rozpadł na tym terenie - nikt nic nie kontroluje.
    6. Sytuacja z portaliskiem wzmacnia status quo na tym terenie - nikt nie jest w stanie uzyskać przewagi. (Raynor)
1. Robert Sądeczny, zapamiętany przez historię
    1. Jak wbić klin między Fornalisa a Sądecznego? (Kić)
    2. Aneta wróciła z Sądecznym z Muzeum Pary ORAZ wie, co tam się stało
    3. Hektor pomagał w budowaniu eliksiru który skrzywdził Katię
    4. Czy Aneta Rukolas się na serio zakocha w Sądecznym (Skażenie Wzoru)?: NIE WIEMY. (Żółw). 5/10.
    5. Wzór Anety jest trwale uszkodzony - wymaga czegoś nowego (Żółw)
    6. Sądeczny przekazał dowodzenie defensywne Wiaczesławowi (Żółw)
1. Lord Rezydent Sylwester Bankierz
    1. Sylwester został zesłany tu przez działania Newerji
    2. Sylwester przeprowadził udaną akcję zdobycia transportu Dukata
1. Tomasz Myszeczka - król viciniusów
    1. Sądeczny dostarczył maga, który zajmie się glukszwajnem czyszczącym czujniki ze Skażenia
    2. Tamara robi się bardzo podejrzliwa wobec Myszeczki
    3. Myszeczka staje po stronie Rezydenta Świecy
    4. Tomasz Myszeczka pomaga w tematach leczniczych. Poniesie koszty, ale zrekompensuje sobie wiele innych rzeczy. (Kić)
1. Dracena Diakon, w jakimś społeczeństwie
    1. CLAIM: Szybki, dyskretny awian. Trafi do Draceny. (Żółw)
    2. Kto i w jaki sposób pomoże Dracenie stać się częścią tego terenu dookoła? Należeć do czegoś?
    3. Dracena wykonuje działania mające uwolnić "Nową Melodię" od mafii 
    4. Dracena uważa, że to jest moment na uderzenie w Sądecznego i zniszczenie władzy na tym terenie (Żółw)
1. Magitrownia Histogram, echo technologii Weinerów
    1. CLAIM: Dracena docelowo będzie w stanie zintegrować się z JAKIMŚ społeczeństwem (Kić)
    2. Magitrownia z Draceną działa dużo lepiej i taniej
    3. Dracena została przekonana, że magitrownia współpracuje z mafią a nie jest wykorzystywana
    4. Zarówno siły Dukata jak i Myszeczki zapewniają, by Skażenie nie dotarło do magitrowni.
    5. Tomasz Myszeczka - jego viciniusy stanowią źródło wczesnego ostrzegania na liniach magitrowni. (Raynor)
    6. Nowa polityka miłości w firmie generalnie rozwiązuje problem z napięciami i nie powoduje publicznych rozterek. (Raynor)
    7. Magitrownia uzyskuje wsparcie zasobowe Eweliny Bankierz. (Raynor)
1. Gabriel Dukat - uleczenie jego syna
    1. CLAIM: Dukat lubi Paulinę (Kić)
    2. W jaki sposób byty z Allitras mogą pomóc dziecku Dukata?
    3. Aktywny Harvester będzie w stanie pomóc dziecku Dukata - stałym kosztem ofiar z ludzi (Żółw)
    4. Dukat nie ma gwałtownej reakcji na działania Sylwestra; da się wychować. Tupnął nogą bo musiał - and now we talk" (Kić)
    5. Kluczowe elementy soullinkowania dziecka Dukata pochodzą z wiedzy pozyskanej przez Sądecznego w ramach działania na tym terenie
1. Portalisko Pustulskie i odrodzenie Mausów
    1. Pojawia się zwiększenie ruchów antymausowych na tym terenie - mniej portaliska dla Mausów.
    2. Bolesław Maus rozpoczyna kampanię "keep magic low here".
    3. Portalisko jest silnie połączone z Allitras.
    4. Bolesław Maus kontratakuje przeciwko Tomaszowi Myszeczce - nie może tak być, że ciągle świnie niszczą portalisko.
1. Świńskie viciniusy i coś poszło nie tak
    1. Rolnik od świń założył ma misję - i przydupasa - mające udowodnić, że ze świniami dzieje się coś dziwnego
    2. W okolicy Męczymordy jest odpowiednia ilość pola rezydualnego by sprawić by znikające świnie były prawdą
1. Oktawia, efemeryczne echo Oliwii
    1. CLAIM: Oktawia dorośnie i zniknie. (Kić)
    2. CLAIM: Dracena i Oktawia będą miały duet. (Żółw)
    3. Oktawia chce pomóc Krzysztofowi, bo widzi w nim potencjał do pomocy Oliwii
    4. Marzyciel może nadal komunikować się z Oktawią; może jej śpiewać, gdy ona jest w Efemerydzie. (Żółw)
    5. Chaotyczność i nieufność / niestabilność Oktawii jedynie wzrosła. Ciągle odrzucana, discardowana, torturowana. (Żółw)
1. Wolność Eweliny Bankierz
    1. CLAIM "Newerja Bankierz x Gabriel Dukat - zostaną parą" (Kić)
    2. Docelowo Ewelina dostanie jakieś środki do życia; nie będzie żebraczką
    3. Kaja uważa, że już dość; czas zacząć pomagać i ratować sytuację (Kić)
1. Prokuratura Magów
    1. Całą tą sprawą, tą destabilizacją zainteresowała się nowo powstała prokuratura magów. (Raynor)


# Narzędzia MG

## Opis celu misji

-

## Cel misji

-

## Po czym poznam sukces

-

## Wynik z perspektywy celu

-

## Wykorzystana mechanika

Postać (strona gracza):

| .Motywacja. | .Umiejętności. | .Magia. | .SiłySłabości. | .Znam.    | .Mam.      |.POSTAĆ. |
|   (-2) - 2  |     0 - 2      |  0 - 2  |    (-2) - 2    | Z+M = 0-6 | Z+M = 0-6  |         |

Opozycja (strona NPC):

* Baza: 2
* Aspekt: 3/aspekt, wariant z nożyczki/papier/kamień
* Modyfikatory: Skala, Sprzęt, Sytuacja (1-3 każdy)

Wynik:

| .Postać. | .Opozycja. | .Rzut.    | .Wynik.  |
|          |            | xx: +x Wx |   S-SS-F |

Wpływ:

* 8 kart / dzień
* każda porażka i skonfliktowany sukces = +2 pkt wpływu dla gracza
* każda karta = +1 wpływ dla MG
* każde 5 kart zaokr. w górę: +1 pkt wpływu dla gracza

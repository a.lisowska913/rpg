---
layout: inwazja-konspekt
title:  "Rykoszet zimnej wojny"
campaign: powrot-karradraela
gm: żółw
players: kić, dust
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [150406 - Aurelia za aptoforma (PT)](150406-aurelia-za-aptoforma.html)

### Chronologiczna

* [150325 - Morderstwo jak w książce (PT, HB)](150325-morderstwo-jak-w-ksiazce.html)
* [150406 - Aurelia za aptoforma (PT)](150406-aurelia-za-aptoforma.html)

# Wątki

- Projekt "Moriath"
- Wszystkie dziewczyny Marcelina
- Arazille, królowa niewolników

## Punkt zerowy:

## Misja właściwa:

Paulina opuściła Hektora w pokoju; jest to zbyt niebezpieczna sprawa a Paulina nie jest magiem bojowym.
Do Hektora przyszła Mojra. Powiedziała, że słyszała, że Hektor wymienił imię Tymotheusa Blakenbauera. Jest bardzo zainteresowana czemu tak powiedział, ale Hektor powiedział, że to przeczucia, bo mu się skojarzyło z przeszłymi działaniami Tymotheusa. Hektor skutecznie NIE powiedział Mojrze, że w podziemiach zamknięte są końcówki aptoforma i ogólnie grzecznie wysłał ją na drzewo. Nie będzie z nią współpracować (ale udaje, że będzie).

Patrycja Krowiowska skontaktowała się z Hektorem. Ma trop. Linia pomocy dzieciom (dzwonią by się żalić że są niekochane, zgłosić molestowanie itp) którą normalnie zarządza Marek Rudzielec (psycholog dziecięcy) zachowuje się nieco inaczej. Marek od wczoraj robi wszystko co w jego mocy, by skłonić dzieci do samobójstw; 4 dzieci już wylądowała w szpitalu. Hektor zażądał od Patrycji wezwania Aliny Bednarz i kilku policjantów; to co się teraz dzieje to jakaś paranoja...

Hektor odwiedził Edwina. Ten nie był w stanie dać Hektorowi jeszcze żadnego bardzo skutecznego i łatwego sposobu wykrycia aptoforma, więc dał Hektorowi 2 pigułki - jedna powoduje omdlenie aptoforma a padaczkę u ludzi; druga leczy ludzi z padaczki. Edwin załatwił też Hektorowi środki amnezyjne. Hektor przekazał te środki swojemu agentowi (Dionizy Kret) i wysłał go na zadanie z Aliną. Wcześniej Hektor powiedział Alinie, że to kwestia narkotyków i psychotropów. 

Pojechali na 2 auta - Alina z Dionizym, drugie auto z funkcjonariuszami. Alina oficjalnie dowodzi akcją.
Skontaktowali się z Edwinem; czarodziej powiedział, że mają do czynienia ze złotoustym potworem i to jest niebezpieczne.
Plan jest prosty - wpaść i zgarnąć końcówkę aptoforma. 

Alina po prostu weszła do centrali, machnęła odznaką i poprosiła ze sobą Marka Rudzielca. Ten wstał (taki znękany) i poszedł z Aliną. Alina doprowadziła go do Dionizego, który po prostu wsadził mu tabletki w gardło. Niestety, piana. Człowiek, nie końcówka aptoforma. Dostał tabletkę amnezyjną i został porwany - prosto do Hektora...
Zdaniem Patrycji to właśnie on jest tą osobą, która pchała dzieci do samobójstwa.
Alina została tam jeszcze na miejscu, by poszukać końcówki aptoforma a Dionizy odwiózł Marka Rudzielca do Hektora Blakenbauera.

Wszystko wskazuje na to, że Marek Rudzielec jest tylko człowiekiem... ale coś się stało co sprawiło, że jego zachowanie pasuje do profilu aptoforma. I co się stało, że dobry, uczciwy psycholog zaczął działać w taki sposób?
Hektor idąc z Markiem do Edwina zaczął go przesłuchiwać. Użył zaklęcia mentalnego przesłuchującego na nieszczęsnym Marku i zobaczył straszną scenę. Marek i jakaś dziewczyna (Wanda) szli i żartowali sobie, gdzie natknęli się na walkę dwóch magów. Nie wiedzieli co to, ale widząc faceta goniącego dziewczynę pobiegli za nimi. I dostali zaklęciem przeznaczonym dla jednego z magów. Wanda zasłoniła sobą Marka, więc na niego zaklęcie zadziałało słabiej...
Efekt zaklęcia: Marek pragnie śmierci i zniewolenia, ale nie może sam sobie tego zrobić. Musi błagać. I w swoim chorym umyśle przełożył to zaklęcie na "muszę zabić te dzieci by nie cierpiały".
- ta dziewczyna jest cybergothką; znajdzie się ją zwykle w "Dziale Plazmowym".
- ta dziewczyna nazywa się Wanda Ketran; Hektor pamięta, że Marcelin się o nią martwił i mu coś marudził dwa dni temu (co incydentalnie pasuje do wspomnień psychologa).
- Hektor ma listę wszystkich dzieci z którymi rozmawiał po incydencie Marek.
- nie ma w jego pamięci nic powiązanego z aptoformem ALE jest coś wskazującego, że ktoś śledzi Wandę jeszcze przed incydentem.

Hektor poszedł z Markiem do Edwina, ale ten przekierował Hektora do Mojry (nie ma mocy mentalnych). Mojra bez większego problemu naprawiła biednego psychologa, po czym Hektor pokazał Mojrze magów, którzy się nieładnie bawili. Mojra rozpoznała jednego maga Świecy i jednego maga KADEMu.
Mojra obiecała, że zobaczy co da się z tym zrobić. Wyraźnie napięcie między KADEMem i Świecą rośnie.

Hektor poszedł spotkać się z Marcelinem odnośnie Wandy.
Marcelin jest bardzo zmartwiony. Wanda od przedwczoraj zachowuje się bardzo dziwnie. Ma bombę na plecach (XD) i powiedziała, że ciekawe kiedy coś wybuchnie. Marcelin posłał za Wandą ogon, ale ten ogon wykrył, że Wanda już ma ogon. Wanda doprowadziła do tego, że stanęła na dachu i powiedziała, że bóg ją chroni. Skoczyła. Marcelin magią rozpostarł siatkę na dole i cybergothka się zaśmiała. "Widzisz?"
A przecież wcześniej Wanda nakłaniała ludzi do pracy w ruchu chrześcijańskim "Metoda". A przy jej charyzmie to nie był nigdy większy problem.

Hektor wysłał Alinę i Dionizego do Wandy... zneutralizować i przyprowadzić. Najlepiej bezpośrednio do Rezydencji. I uwaga na bombę.

Późny wieczór. Klub "Działo plazmowe". Dionizy i Alina... a w środku Wanda. Tańczy jak szalona.
Alina próbowała wypatrzeć osoby śledzące Wandę... ale śledzący Wandę detektyw znalazł Alinę pierwszy (Fryderyk Mruczek). Powiedział Alinie, że fajnie ją zobaczyć; ta do niego, że on śledzi Wandę i Fryderyk przytaknął. Detektyw był zaskoczony; myślał, że śledzi Wandę na zlecenie grupy specjalnej Hektora Blakenbauera. Alina wyprowadziła go z równowagi. Zapytany, detektyw powiedział, że przespał się z Wandą (wyczaiła go). Ta dziewczyna jest pełna życia!
Problem w tym, że Wanda jest bardzo... happy-go-lucky chwilowo. I stała się idolką młodzieży którzy próbują robić to co ona. 
W tłumie, Alina zauważyła końcówkę aptoforma. Aptoform jest zafascynowany Wandą; próbuje do niej zaadaptować, ale nie potrafi. Podłożył jakąś małą bombę by wywołać panikę; Dionizy i Alina postanawiają wykorzystać to na swoją korzyść.

Alina i Dionizy umożliwili wybuch pirotechniki. Nie doszło jednak do paniki (aptoform nie zdążył krzyknąć, że pożar, bo Dionizy go zneutralizował pigułką gwałtu Edwina). Ochrona opanowała sytuację. W tym czasie Alina podeszła do Wandy i sprowokowała ją, by ta poszła z Aliną (na zasadzie, że nie dostanie tego, co chce). Wanda poszła z Aliną, ale niestabilna przez czar raniła Alinę nożem. Dionizy obezwładnił cybergothkę, po czym skuł ją i zawiózł z końcówką aptoforma do Rezydencji.
Wanda była wstrząśnięta tym, co zrobiła, że zraniła Alinę.

Hektor zgarnął śmietankę. Alina, lekko ranna, została odesłana do Edwina do naprawy.

Wanda została zaprowadzona do Mojry. Ta rzuciła zaklęcie by Wandę naprawić i to jej się udało... ale Mojra znalazła coś jeszcze w środku Wandy. Coś głębiej, coś bardzo interesującego... (1/k20). Mojra odkryła Arazille wewnątrz Wandy. Bogini Marzeń uderzyła korzystając z energii Wandy, prosto w nie-osłoniętą przed atakiem takiej siły Mojrę. Czarodziejka dokonała manifestacji mocy (potężnie Skażając ten fragment Rezydencji) i krzyknęła, że to Arazille... po czym spaliła kanały i straciła przytomność.

Pojawił się zawołany przez Hektora Edwin. Spochmurniał. Wspólnie ustalili, że skupiają się na aptoformie, nie na Arazille. Cybergothka jest wyegzorcyzmowana, Arazille NIE MOŻE dostać się do aptoforma. Edwin chce aptoforma żywcem, bo "Margaret prosiła". Hektor zrozumiał tą argumentację... ma podobnie.

Mojra zdążyła jeszcze naprawić Wandę mentalnie i usunąć z niej Arazille.
Mojra dała też radę wysłać sygnał SŚ i KADEMowi o tym, że taka a nie inna eskalacja wystąpiła i że to jest wysoce niewłaściwe.
Niestety, przez dłuższy czas Mojra już nic nie zrobi...

# Streszczenie

Polowanie na aptoforma, wersja: Blakenbauer. Linia anty-samobójstw dzieciom skupia się by jak najwięcej dzieci zabić. Okazuje się, że to nie działanie aptoforma; dwóch magów walczyło i rykoszet czaru zmienił psychologa dziecięcego i Wandę Ketran. Mojra próbowała pomóc Wandzie, odkryła w jej głębi Arazille i została porażona. Koma. Sygnał o eskalacji został wysłany; jeśli ludziom grozi śmierć przez magów, to na serio już jest to niebezpieczne...

# Lokalizacje:

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Centrum
                        1. Cyberklub Działo Plazmowe
                    1. Obrzeża
                        1. Rezydencja Blakenbauerów

# Pytania:

Ż: Dlaczego wydarzenia powiązane z cybergothką są wystarczająco istotne by skupić uwagę Hektora?
K: Jeśli Hektor czegoś nie zrobi, Marcelin to zrobi.
Ż: Dlaczego cybergothka fascynuje aptoforma?
D: Jest w stanie wywoływać bardzo silne emocje wśród ludzi i to powoduje, że aptoform chce skopiować jej wzór.
Ż: Skąd kapłan zdobył niewyobrażalne źródło mocy zdolne do zatrzymania magów?
K: Miał je od zawsze; przekazywane było w formie spadku.
Ż: Na jaki cel wskazują działania aptoforma?
D: Ucz się, adaptuj i zdobywaj.
Ż: W jaki sposób starcia KADEM-Świeca odbijają się na możliwościach działania Hektora?
K: Z jednej strony odwracają uwagę, z drugiej sprawiają, że magowie są dużo bardziej paranoiczni.
Ż: Jak w sprawę (korzystnie dla Hektora) zamieszany jest Onufry Puzel, dość skromny adwokat z urzędu?
D: Mieszka w bardzo bliskim sąsiedztwie rdzenia aptoforma, więc ma kluczową informację o której nie wie.
K->Ż: Jaki błąd popełni aptoform wobec maga, który kontroluje aptoforma?
Ż: Pokaże, że jest bardziej autonomiczny niż komukolwiek się wydawało, przez co mag wyśle siepacza do de-socjalizacji.
D->Ż: Dzięki czemu Patrycja potrafiła odfiltrować nieprawidłowe informacje odnośnie aptoforma z sieci.
Ż: Anna Kajak zapewniła wiedzę okultystyczną, której nie powinna posiadać.
Ż: Miejsce szczególnie ważne dla fascynującej Marcelina i aptoforma cybergothki?
K: Nieduża filia III biblioteki w Kopalinie.
Ż: Jakie wydarzenie sprawiło, że Marcelin chce, by Hektor zadziałał w powiązaniu z cybergothką?
D: Ktoś ją skrzywdził.
Ż: Jakim śladem zaczyna misję Hektor?
K: Wskazanie na potencjalną końcówkę aptoforma odfiltrowaną z sieci (mylny trop).
Ż: Straszliwe zaklęcie mentalne bojowe, jakie powiązane jest z kralothami.
D: Smak własnego ciała sprawia ekstazę ofierze; nie może przestać.

# Zasługi

* mag: Hektor Blakenbauer, który w prawidłowy sposób delegował wszystkie zadania wymagające pracy na oddział specjalny samemu zajmując się przesłuchaniami.
* vic: Dionizy Kret jako bardzo skuteczna postać bojowa, unieszkodliwiający końcówkę aptoforma nim ten zdążył wywołać panikę.
* vic: Alina Bednarz, która potrafiła wyciągnąć wszystkich podejrzanych we wszystkie ustronne miejsca i tylko raz oberwała nożem.
* czł: Wanda Ketran, 24-letnia cybergothka, która nie obawia się niczego i nie ma nic do stracenia; stała się idolką młodych buntowników. Też: agentka Arazille.
* mag: Mojra, mentalistka próbująca współpracować z Hektorem (który jej nie ufa) która próbując ratować cybergothkę jest porażona przez Arazille.
* czł: Patrycja Krowiowska, która co prawda nie znalazła dla Hektora aptoforma, ale znalazła coś powiązanego z Arazille...
* czł: Marek Rudzielec, psycholog pomagający dzieciom, który został porażony przez czar mentalny walczących ze sobą magów KADEMu i Świecy.
* mag: Edwin Blakenbauer, który z drugiej linii dowodzi operacjami grupy specjalnej Hektora w sprawie aptoforma...
* mag: Marcelin Blakenbauer, który zmartwił się dziwnym zachowaniem charyzmatycznej cybergothki i powiedział o tym Hektorowi (który go olał aż powiązał to z aptoformem).
* vic: Aptoform Mirasilaler, zafascynowany Wandą, której nie potrafi przeskanować; najpewniej na celowniku Arazille.
* vic: Arazille, bogini marzeń, która chroni Wandę przed rykoszetem zaklęcia walczących magów; po jej wykryciu przez Mojrę razi czarodziejkę wypalając jej kanały.
* czł: Fryderyk Mruczek, czasem współpracujący z siłami specjalnymi Hektora detektyw który dla odmiany pracuje dla kogoś innego śledząc cybergothkę.
---
layout: default
title: Konspekty Inwazji
---

# {{ page.title }}

## Inwazja

## 1. Nie umieszczone, Anulowane

10/01/01 - 10/01/02
[Omówienie kampanii](/rpg/inwazja/opowiesci/konspekty/kampania-anulowane.html)

1. [130514 - Chłopak, który chciał być bohaterem](/rpg/inwazja/opowiesci/konspekty/130514-chlopak-ktory-chcial-byc-bohaterem.html): 10/01/01 - 10/01/02
1. [130529 - Zguba w muzeum](/rpg/inwazja/opowiesci/konspekty/130529-zguba-w-muzeum.html): 10/01/01 - 10/01/02
1. [131117 - Cel uświęca środki](/rpg/inwazja/opowiesci/konspekty/131117-cel-uswieca-srodki.html): 10/01/01 - 10/01/02
1. [140408 - Czarny Kamaz](/rpg/inwazja/opowiesci/konspekty/140408-czarny-kamaz.html): 10/01/01 - 10/01/02
1. [140409 - Czwarta frakcja Zajcewów](/rpg/inwazja/opowiesci/konspekty/140409-czwarta-frakcja-zajcewow.html): 10/01/01 - 10/01/02
1. [140702 - Miślęg zniknąć!](/rpg/inwazja/opowiesci/konspekty/140702-misleg-zniknac.html): 10/01/01 - 10/01/02
1. [140808 - Ucieczka Trzmieli](/rpg/inwazja/opowiesci/konspekty/140808-ucieczka-trzmieli.html): 10/01/01 - 10/01/02
1. [140819 - Marcelin w klasztorze!](/rpg/inwazja/opowiesci/konspekty/140819-marcelin-w-klasztorze.html): 10/01/01 - 10/01/02
1. [140910 - Reporter kontra Blakenbauerzy](/rpg/inwazja/opowiesci/konspekty/140910-reporter-kontra-blakenbauerzy.html): 10/01/01 - 10/01/02
1. [141210 - Złodzieje kielicha w akcji](/rpg/inwazja/opowiesci/konspekty/141210-zlodzieje-kielicha-w-akcji.html): 10/01/01 - 10/01/02
1. [150120 - Pierścień też zniknął](/rpg/inwazja/opowiesci/konspekty/150120-pierscien-tez-zniknal.html): 10/01/03 - 10/01/04
1. [160526 - Bobrzańskie Gumifoczki](/rpg/inwazja/opowiesci/konspekty/160526-bobrzanskie-gumifoczki.html): 10/01/01 - 10/01/02
1. [160608 - Czy on wyszedł z Rifta...?](/rpg/inwazja/opowiesci/konspekty/160608-czy-on-wyszedl-z-rifta.html): 10/01/01 - 10/01/02

## 2. Blakenbauerowie x Skorpion

10/01/03 - 10/01/16
[Omówienie kampanii](/rpg/inwazja/opowiesci/konspekty/kampania-blakenbauerowie-x-skorpion.html)

1. [150304 - Ani słowa prawdy...](/rpg/inwazja/opowiesci/konspekty/150304-ani-slowa-prawdy.html): 10/01/03 - 10/01/04
1. [141006 - Klinika 'Słonecznik'](/rpg/inwazja/opowiesci/konspekty/141006-klinika-slonecznik.html): 10/01/05 - 10/01/06
1. [141009 - Jad w prokuraturze](/rpg/inwazja/opowiesci/konspekty/141009-jad-w-prokuraturze.html): 10/01/07 - 10/01/08
1. [141022 - Po wymianie strzałów...](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html): 10/01/09 - 10/01/10
1. [150210 - 'Komandosi', czyli upadek bohaterki](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html): 10/01/11 - 10/01/12
1. [141216 - Zabili mu syna](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html): 10/01/11 - 10/01/12
1. [150115 - Negocjacje w LegioQuant](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html): 10/01/13 - 10/01/14
1. [150121 - Nowe życie Aliny](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html): 10/01/15 - 10/01/16

## 3. Druga Inwazja

10/01/03 - 10/01/22
[Omówienie kampanii](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)

1. [140103 - Tak bardzo nie artefakt](/rpg/inwazja/opowiesci/konspekty/140103-tak-bardzo-nie-artefakt.html): 10/01/03 - 10/01/04
1. [140109 - Uczniowie Moriatha](/rpg/inwazja/opowiesci/konspekty/140109-uczniowie-moriatha.html): 10/01/05 - 10/01/06
1. [140114 - Zaginiony członek](/rpg/inwazja/opowiesci/konspekty/140114-zaginiony-czlonek.html): 10/01/07 - 10/01/08
1. [140121 - Zniknięcie Sophistii](/rpg/inwazja/opowiesci/konspekty/140121-znikniecie-sophistii.html): 10/01/09 - 10/01/10
1. [140201 - Ona zdradza, on zdradza](/rpg/inwazja/opowiesci/konspekty/140201-ona-zdradza-on-zdradza.html): 10/01/11 - 10/01/12
1. [140208 - Na ratunek terminusce](/rpg/inwazja/opowiesci/konspekty/140208-na-ratunek-terminusce.html): 10/01/13 - 10/01/14
1. [140213 - Pułapka na Edwina](/rpg/inwazja/opowiesci/konspekty/140213-pulapka-na-edwina.html): 10/01/01 - 10/01/02
1. [140219 - Niespodziewane wsparcie](/rpg/inwazja/opowiesci/konspekty/140219-niespodziewane-wsparcie.html): 10/01/03 - 10/01/04
1. [140227 - Sophistia x Marcelin](/rpg/inwazja/opowiesci/konspekty/140227-sophistia-x-marcelin.html): 10/01/05 - 10/01/06
1. [140320 - Sprawa magicznych samochodów](/rpg/inwazja/opowiesci/konspekty/140320-sprawa-magicznych-samochodow.html): 10/01/07 - 10/01/08
1. [140312 - Atak na rezydencję Blakenbauerów](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html): 10/01/09 - 10/01/10
1. [140401 - Mojra, Moriath](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html): 10/01/11 - 10/01/12
1. [140604 - Patriarcha Blakenbauer](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html): 10/01/13 - 10/01/14
1. [140611 - Rezydencja Blakenbauerów](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html): 10/01/15 - 10/01/16
1. [140618 - Upadek Agresta](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html): 10/01/17 - 10/01/18
1. [140625 - Ostatnia Saith](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html): 10/01/19 - 10/01/20
1. [140708 - Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html): 10/01/21 - 10/01/22

## 4. Nie przydzielone

10/01/03 - 10/01/04
[Omówienie kampanii](/rpg/inwazja/opowiesci/konspekty/kampania-nie-przydzielone.html)

1. [170712 - Ucieczka Małży](/rpg/inwazja/opowiesci/konspekty/170712-ucieczka-malzy.html): 10/01/03 - 10/01/04

## 5. Start

10/01/01 - 10/01/02
[Omówienie kampanii](/rpg/inwazja/opowiesci/konspekty/kampania-start.html)

1. [120507 - Preludium: Historia świata](/rpg/inwazja/opowiesci/konspekty/120507-preludium-historia-swiata.html): 10/01/01 - 10/01/02

## 6. Rezydentka Krukowa

10/01/03 - 10/03/19
[Omówienie kampanii](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)

1. [170523 - Opętany konstruminus](/rpg/inwazja/opowiesci/konspekty/170523-opetany-konstruminus.html): 10/01/03 - 10/01/04
1. [170716 - Eteryczny chłopiec i jego pies](/rpg/inwazja/opowiesci/konspekty/170716-eteryczny-chlopiec-i-jego-pies.html): 10/01/08 - 10/01/10
1. [170525 - New, better Senesgrad](/rpg/inwazja/opowiesci/konspekty/170525-new-better-senesgrad.html): 10/01/13 - 10/01/15
1. [170510 - Najgorsze love story](/rpg/inwazja/opowiesci/konspekty/170510-najgorsze-love-story.html): 10/01/16 - 10/01/19
1. [170515 - Niewolnica w leasingu](/rpg/inwazja/opowiesci/konspekty/170515-niewolnica-w-leasingu.html): 10/01/20 - 10/01/22
1. [170518 - Machinacje maga rolniczego](/rpg/inwazja/opowiesci/konspekty/170518-machinacje-maga-rolniczego.html): 10/01/23 - 10/01/25
1. [170614 - Kryzys przez eliksir](/rpg/inwazja/opowiesci/konspekty/170614-kryzys-przez-eliksir.html): 10/01/27 - 10/01/28
1. [170702 - Miłość przez desperację](/rpg/inwazja/opowiesci/konspekty/170702-milosc-przez-desperacje.html): 10/01/29 - 10/02/02
1. [170723 - Wywalczone życie Diany](/rpg/inwazja/opowiesci/konspekty/170723-wywalczone-zycie-diany.html): 10/02/03 - 10/02/06
1. [170331 - Kiepsko porwana Paulina](/rpg/inwazja/opowiesci/konspekty/170331-kiepsko-porwana-paulina.html): 10/02/07 - 10/02/09
1. [170404 - Wąż jako vicinius Pauliny](/rpg/inwazja/opowiesci/konspekty/170404-waz-jako-vicinius-pauliny.html): 10/02/10 - 10/02/13
1. [170409 - Nie zabijajmy tych magów](/rpg/inwazja/opowiesci/konspekty/170409-nie-zabijajmy-tych-magow.html): 10/02/16 - 10/02/19
1. [170323 - Oszczędzili na rurach](/rpg/inwazja/opowiesci/konspekty/170323-oszczedzili-na-rurach.html): 10/02/20 - 10/02/24
1. [170325 - Klątwożyt z lustra](/rpg/inwazja/opowiesci/konspekty/170325-klatwozyt-z-lustra.html): 10/02/28 - 10/03/03
1. [170326 - Cała przeszłość spłonęła](/rpg/inwazja/opowiesci/konspekty/170326-cala-przeszlosc-splonela.html): 10/03/04 - 10/03/07
1. [160124 - Trzy opętane duszyczki](/rpg/inwazja/opowiesci/konspekty/160124-trzy-opetane-duszyczki.html): 10/03/08 - 10/03/09
1. [160131 - Dziwny transmiter Weinerów](/rpg/inwazja/opowiesci/konspekty/160131-dziwny-transmiter-weinerow.html): 10/03/10 - 10/03/11
1. [160227 - Zakazany harvester](/rpg/inwazja/opowiesci/konspekty/160227-zakazany-harvester.html): 10/03/12 - 10/03/13
1. [170312 - Przebudzony... Harvester?](/rpg/inwazja/opowiesci/konspekty/170312-przebudzony-harvester.html): 10/03/17 - 10/03/19

## 7. Światło w Zależu Leśnym

10/01/03 - 10/01/08
[Omówienie kampanii](/rpg/inwazja/opowiesci/konspekty/kampania-swiatlo-w-zalezu-lesnym.html)

1. [150427 - Kult zaleskiego Anioła](/rpg/inwazja/opowiesci/konspekty/150427-kult-zaleskiego-aniola.html): 10/01/03 - 10/01/04
1. [150429 - Terminusi w Zależu](/rpg/inwazja/opowiesci/konspekty/150429-terminusi-w-zalezu.html): 10/01/05 - 10/01/06
1. [150501 - Szalona 'czarodziejka' Zależa](/rpg/inwazja/opowiesci/konspekty/150501-szalona-czarodziejka-zaleza.html): 10/01/07 - 10/01/08

## 8. Czarodziejka Luster

10/01/03 - 10/02/20
[Omówienie kampanii](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)

1. [170207 - Błękitny zaskroniec](/rpg/inwazja/opowiesci/konspekty/170207-blekitny-zaskroniec.html): 10/01/03 - 10/01/05
1. [120918 - Można doprowadzić maga do problemu...](/rpg/inwazja/opowiesci/konspekty/120918-mozna-doprowadzic-maga-do-problemu.html): 10/01/06 - 10/01/07
1. [120920 - Ale nie można zmusić go do jego rozwiązania...](/rpg/inwazja/opowiesci/konspekty/120920-ale-nie-mozna-zmusic-go-do-jego-rozwiazania.html): 10/01/08 - 10/01/09
1. [121013 - Zwłaszcza, gdy coś jest ciągle nie tak](/rpg/inwazja/opowiesci/konspekty/121013-zwlaszcza-gdy-cos-jest-ciagle-nie-tak.html): 10/01/10 - 10/01/11
1. [121104 - Banshee, córka mandragory](/rpg/inwazja/opowiesci/konspekty/121104-banshee-corka-mandragory.html): 10/01/12 - 10/01/13
1. [160908 - Zabójczy spadek](/rpg/inwazja/opowiesci/konspekty/160908-zabojczy-spadek.html): 10/01/14 - 10/01/17
1. [170815 - Bliźniaczka Andromedy](/rpg/inwazja/opowiesci/konspekty/170815-blizniaczka-andromedy.html): 10/01/23 - 10/01/25
1. [170816 - Na wezwanie Iliusitiusa](/rpg/inwazja/opowiesci/konspekty/170816-na-wezwanie-iliusitiusa.html): 10/01/27 - 10/01/29
1. [130503 - Renowacja obrazu Andromedy](/rpg/inwazja/opowiesci/konspekty/130503-renowacja-obrazu-andromedy.html): 10/01/30 - 10/01/31
1. [130506 - Sekrety Rezydencji Szczypiorkow](/rpg/inwazja/opowiesci/konspekty/130506-sekrety-rezydencji-szczypiorkow.html): 10/02/01 - 10/02/02
1. [130511 - Ołtarz Podniesionej Dłoni](/rpg/inwazja/opowiesci/konspekty/130511-oltarz-podniesionej-dloni.html): 10/02/03 - 10/02/04
1. [131008 - 'Mój Anioł'](/rpg/inwazja/opowiesci/konspekty/131008-moj-aniol.html): 10/02/05 - 10/02/06
1. [141218 - Portret Boga](/rpg/inwazja/opowiesci/konspekty/141218-portret-boga.html): 10/02/07 - 10/02/08
1. [141220 - Bogini Marzeń w Żonkiborze](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html): 10/02/09 - 10/02/10
1. [141227 - Przyczajona Andromeda, ukryty Maus](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html): 10/02/11 - 10/02/12
1. [141230 - Ofiara z wampira dla Arazille](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html): 10/02/13 - 10/02/14
1. [150103 - Pryzmat Myśli pęka](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html): 10/02/15 - 10/02/16
1. [150104 - Terminus-defiler, kapłan Arazille](/rpg/inwazja/opowiesci/konspekty/150104-terminus-defiler-kaplan-arazille.html): 10/02/17 - 10/02/18
1. [150105 - Dar Iliusitiusa dla Andromedy](/rpg/inwazja/opowiesci/konspekty/150105-dar-iliusitiusa-dla-andromedy.html): 10/02/05 - 10/02/06
1. [150602 - Esme, najemniczka Netherii](/rpg/inwazja/opowiesci/konspekty/150602-esme-najemniczka-netherii.html): 10/02/19 - 10/02/20

## 9. Powrót Karradraela

10/01/03 - 10/09/05
[Omówienie kampanii](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)

1. [140503 - Wołanie o pomoc](/rpg/inwazja/opowiesci/konspekty/140503-wolanie-o-pomoc.html): 10/01/03 - 10/01/04
1. [140505 - Musiał zginąć, bo Maus](/rpg/inwazja/opowiesci/konspekty/140505-musial-zginac-bo-maus.html): 10/01/05 - 10/01/06
1. [140508 - 'Lord Jonatan'](/rpg/inwazja/opowiesci/konspekty/140508-lord-jonatan.html): 10/01/07 - 10/01/08
1. [170501 - Streamerka w Na Świeczniku](/rpg/inwazja/opowiesci/konspekty/170501-streamerka-w-na-swieczniku.html): 10/01/09 - 10/01/12
1. [150729 - Kaczuszka w servarze](/rpg/inwazja/opowiesci/konspekty/150729-kaczuszka-w-servarze.html): 10/01/19 - 10/01/21
1. [161216 - Szept z Academii Daemonica](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html): 10/01/27 - 10/01/29
1. [161217 - Niezbyt legalna 'Academia' Whisperwind](/rpg/inwazja/opowiesci/konspekty/161217-niezbyt-legalna-academia-whisperwind.html): 10/01/30 - 10/01/31
1. [161218 - Zazdrość Warmastera](/rpg/inwazja/opowiesci/konspekty/161218-zazdrosc-warmastera.html): 10/02/01 - 10/02/03
1. [161222 - Kto wpisał Błażeja do konkursu](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html): 10/02/04 - 10/02/07
1. [170221 - Przecież nie chodzi o koncert](/rpg/inwazja/opowiesci/konspekty/170221-przeciez-nie-chodzi-o-koncert.html): 10/02/05 - 10/02/07
1. [170108 - Samotna w świecie magow](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html): 10/02/12 - 10/02/16
1. [170808 - Duch Opery](/rpg/inwazja/opowiesci/konspekty/170808-duch-opery.html): 10/02/17 - 10/02/19
1. [170228 - Polowanie na Mausównę](/rpg/inwazja/opowiesci/konspekty/170228-polowanie-na-mausowne.html): 10/02/08 - 10/02/10
1. [170111 - EIS na kozetce](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html): 10/02/18 - 10/02/21
1. [170115 - Klub Dare Shiver](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html): 10/02/22 - 10/02/25
1. [170118 - Ludzka prokuratura a magowie](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html): 10/02/22 - 10/02/25
1. [170217 - Skradziona pozytywka Mausów](/rpg/inwazja/opowiesci/konspekty/170217-skradziona-pozytywka-mausow.html): 10/02/28 - 10/03/02
1. [170417 - Ratując syrenopnącze](/rpg/inwazja/opowiesci/konspekty/170417-ratujac-syrenopnacze.html): 10/03/05 - 10/03/12
1. [170315 - Naszyjnik Przenośnych Wspomnień](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html): 10/03/02 - 10/03/03
1. [170707 - Biznes pośród niesnasek](/rpg/inwazja/opowiesci/konspekty/170707-biznes-posrod-niesnasek.html): 10/03/06 - 10/03/08
1. [170319 - Camgirl na dragach](/rpg/inwazja/opowiesci/konspekty/170319-camgirl-na-dragach.html): 10/03/06 - 10/03/07
1. [170407 - Przebudzenie viciniusa](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html): 10/03/08 - 10/03/09
1. [170412 - Przed teatrem absurdu](/rpg/inwazja/opowiesci/konspekty/170412-przed-teatrem-absurdu.html): 10/03/10 - 10/03/12
1. [141119 - Antygona kontra Dracena](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html): 10/03/27 - 10/03/29
1. [141115 - GS Aegis 0002](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html): 10/04/13 - 10/04/14
1. [141123 - Druga kradzież wyzwalacza](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html): 10/04/17 - 10/04/19
1. [150110 - Bezpieczna baza w Kotach](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html): 10/04/15 - 10/04/16
1. [150224 - Wojna domowa Spustoszenia](/rpg/inwazja/opowiesci/konspekty/150224-wojna-domowa-spustoszenia.html): 10/04/17 - 10/04/20
1. [150313 - Ile tam było szczepow Spustoszenia](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html): 10/04/22 - 10/04/24
1. [150329 - Knowania Izy](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html): 10/04/25 - 10/04/28
1. [150325 - Morderstwo jak w książce](/rpg/inwazja/opowiesci/konspekty/150325-morderstwo-jak-w-ksiazce.html): 10/04/29 - 10/04/30
1. [150330 - Napaść na Annalizę](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html): 10/04/29 - 10/04/30
1. [150411 - Dzień z życia Klemensa](/rpg/inwazja/opowiesci/konspekty/150411-dzien-z-zycia-klemensa.html): 10/05/01 - 10/05/02
1. [150406 - Aurelia za aptoforma](/rpg/inwazja/opowiesci/konspekty/150406-aurelia-za-aptoforma.html): 10/05/01 - 10/05/02
1. [150408 - Rykoszet zimnej wojny](/rpg/inwazja/opowiesci/konspekty/150408-rykoszet-zimnej-wojny.html): 10/05/01 - 10/05/02
1. [161005 - Szmuglowanie artefaktów](/rpg/inwazja/opowiesci/konspekty/161005-szmuglowanie-artefaktow.html): 10/05/05 - 10/05/07
1. [150422 - Śladami aptoforma](/rpg/inwazja/opowiesci/konspekty/150422-sladami-aptoforma.html): 10/05/03 - 10/05/04
1. [150604 - Proces bez szans wygrania](/rpg/inwazja/opowiesci/konspekty/150604-proces-bez-szans-wygrania.html): 10/05/05 - 10/05/06
1. [160809 - Awokado Dla Wampira](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html): 10/05/07 - 10/05/09
1. [150607 - Brat przeciw bratu](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html): 10/05/07 - 10/05/08
1. [160202 - Wolność pająka fazowego](/rpg/inwazja/opowiesci/konspekty/160202-wolnosc-pajaka-fazowego.html): 10/05/09 - 10/05/10
1. [150625 - Poligon kluczem do Marcelina?](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html): 10/05/09 - 10/05/10
1. [150716 - Chora terminuska i Żabolód](/rpg/inwazja/opowiesci/konspekty/150716-chora-terminuska-i-zabolod.html): 10/05/09 - 10/05/10
1. [150704 - Najskrytszy sekret Tamary](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html): 10/05/11 - 10/05/12
1. [150718 - Splątane tropy: Spustoszenie?](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html): 10/05/13 - 10/05/14
1. [150722 - Reverse kidnapping z Krupnioka](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html): 10/05/15 - 10/05/16
1. [150728 - Sojusz przeciwko Szlachcie](/rpg/inwazja/opowiesci/konspekty/150728-sojusz-przeciwko-szlachcie.html): 10/05/15 - 10/05/16
1. [150819 - Krótki antyporadnik o sojuszach](/rpg/inwazja/opowiesci/konspekty/150819-krotki-antyporadnik-o-sojuszach.html): 10/05/17 - 10/05/18
1. [150913 - Andrea węszy koło Szlachty](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html): 10/05/19 - 10/05/20
1. [150820 - Klemens w roli swatki](/rpg/inwazja/opowiesci/konspekty/150820-klemens-w-roli-swatki.html): 10/05/19 - 10/05/20
1. [151001 - Plan ujawnienia z Hipernetu](/rpg/inwazja/opowiesci/konspekty/151001-plan-ujawnienia-z-hipernetu.html): 10/05/21 - 10/05/22
1. [150823 - Atak na sanktuarium Estrelli](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html): 10/05/23 - 10/05/24
1. [150826 - Pętla dookoła Pauliny](/rpg/inwazja/opowiesci/konspekty/150826-petla-dookola-pauliny.html): 10/05/25 - 10/05/26
1. [150920 - Sprawa Baltazara Mausa](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html): 10/05/27 - 10/05/28
1. [151007 - Nigdy dość przyjaciół: Szlachta i Kurtyna](/rpg/inwazja/opowiesci/konspekty/151007-nigdy-dosc-przyjaciol-szlachta-kurtyna.html): 10/05/29 - 10/05/30
1. [150829 - Hybryda w bazie Skorpiona](/rpg/inwazja/opowiesci/konspekty/150829-hybryda-w-bazie-skorpiona.html): 10/05/27 - 10/05/28
1. [150930 - O Wandzie co Zajcewa nie chciała](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html): 10/05/29 - 10/05/30
1. [150830 - Kasia, nie EIS w Powiewie](/rpg/inwazja/opowiesci/konspekty/150830-kasia-nie-eis-w-powiewie.html): 10/05/29 - 10/05/30
1. [151014 - Jedno słowo prawdy za dużo](/rpg/inwazja/opowiesci/konspekty/151014-jedno-slowo-prawdy-za-duzo.html): 10/05/31 - 10/06/01
1. [151021 - Przebudzenie Mojry](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html): 10/06/02 - 10/06/03
1. [150922 - Och, nie! Porwali Ignata!](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html): 10/05/29 - 10/05/30
1. [151110 - Romeo i... Hektor](/rpg/inwazja/opowiesci/konspekty/151110-romeo-i-hektor.html): 10/05/31 - 10/06/01
1. [160128 - Byli sobie przestępcy](/rpg/inwazja/opowiesci/konspekty/160128-byli-sobie-przestepcy.html): 10/06/02 - 10/06/03
1. [160210 - Batmag Uderza!](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html): 10/06/04 - 10/06/05
1. [160217 - Sojusz według Leonidasa](/rpg/inwazja/opowiesci/konspekty/160217-sojusz-wedlug-leonidasa.html): 10/06/06 - 10/06/07
1. [160326 - Siluria na salonach Szlachty](/rpg/inwazja/opowiesci/konspekty/160326-siluria-na-salonach-szlachty.html): 10/05/31 - 10/06/01
1. [151119 - Patrycja węszy szpiega](/rpg/inwazja/opowiesci/konspekty/151119-patrycja-weszy-szpiega.html): 10/06/08 - 10/06/12
1. [151124 - Odbudowa relacji konfliktem](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html): 10/06/13 - 10/06/14
1. [151202 - Zdobycie węzła Vladleny](/rpg/inwazja/opowiesci/konspekty/151202-zdobycie-wezla-vladleny.html): 10/06/15 - 10/06/18
1. [160229 - Siedmiu magów - nie Mausów](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html): 10/06/08 - 10/06/13
1. [160327 - Piećdziesiąt oblicz Szlachty](/rpg/inwazja/opowiesci/konspekty/160327-piecdziesiat-oblicz-szlachty.html): 10/06/14 - 10/06/23
1. [151212 - Antyporadnik wędkarza Arazille](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html): 10/06/19 - 10/06/20
1. [151216 - Między prawdą i fikcją Arazille](/rpg/inwazja/opowiesci/konspekty/151216-miedzy-prawda-i-fikcja-arazille.html): 10/06/21 - 10/06/22
1. [160403 - Wiktor kontra Kadem i Świeca](/rpg/inwazja/opowiesci/konspekty/160403-wiktor-kontra-kadem-i-swieca.html): 10/06/23 - 10/06/24
1. [160224 - Aniołki Marcelina](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html): 10/06/23 - 10/06/24
1. [160303 - Otton zabija Zetę](/rpg/inwazja/opowiesci/konspekty/160303-otton-zabija-zete.html): 10/06/23 - 10/06/24
1. [160411 - Sekret śmierci na wykopaliskach](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html): 10/06/14 - 10/06/15
1. [160927 - Desperacka bateria dla Aegis](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html): 10/06/16 - 10/06/21
1. [160404 - The power of cute pet](/rpg/inwazja/opowiesci/konspekty/160404-the-power-of-cute-pet.html): 10/06/25 - 10/06/26
1. [170321 - Tajemnica podłożonej świni](/rpg/inwazja/opowiesci/konspekty/170321-tajemnica-podlozonej-swini.html): 10/06/25 - 10/06/26
1. [160309 - Irytka Sprzężona](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html): 10/06/25 - 10/06/26
1. [160316 - Frontalne wejście Millennium](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html): 10/06/27 - 10/06/28
1. [160406 - Najprawdziwszy sojusz Blakenbauerów](/rpg/inwazja/opowiesci/konspekty/160406-najprawdziwszy-sojusz-blakenbauerow.html): 10/06/29 - 10/06/30
1. [160417 - Symptomy kryzysu Świecy](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html): 10/06/16 - 10/06/17
1. [160412 - Spleśniała dusza terminuski](/rpg/inwazja/opowiesci/konspekty/160412-splesniala-dusza-terminuski.html): 10/07/01 - 10/07/02
1. [160420 - Kolizja dwóch sojuszy](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html): 10/07/01 - 10/07/02
1. [160424 - Uważaj, o czym marzysz](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html): 10/06/18 - 10/06/21
1. [160506 - Wyścig pająka z terminuską](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html): 10/06/22 - 10/06/24
1. [160723 - Czyj Jest Kompleks Centralny](/rpg/inwazja/opowiesci/konspekty/160723-czyj-jest-kompleks-centralny.html): 10/06/22 - 10/06/23
1. [160724 - Portal do EAM](/rpg/inwazja/opowiesci/konspekty/160724-portal-do-eam.html): 10/06/24 - 10/06/25
1. [161030 - Odbudowa dowodzenia Świecy](/rpg/inwazja/opowiesci/konspekty/161030-odbudowa-dowodzenia-swiecy.html): 10/06/22 - 10/06/24
1. [160615 - Morderczyni w masce Wandy](/rpg/inwazja/opowiesci/konspekty/160615-morderczyni-w-masce-wandy.html): 10/06/25 - 10/06/26
1. [161101 - Bezwzględna Lady Terminus](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html): 10/06/25 - 10/06/28
1. [160629 - Rezydencja? E, polujemy na dronę!](/rpg/inwazja/opowiesci/konspekty/160629-rezydencja-e-polujemy-na-drone.html): 10/06/27 - 10/06/30
1. [161113 - Świeca nie zostawia swoich](/rpg/inwazja/opowiesci/konspekty/161113-swieca-nie-zostawia-swoich.html): 10/06/29 - 10/07/01
1. [160821 - Wycofanie Mileny z piekła](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html): 10/06/26 - 10/06/30
1. [160707 - Mała szara myszka...](/rpg/inwazja/opowiesci/konspekty/160707-mala-szara-myszka.html): 10/07/01 - 10/07/03
1. [161120 - Tak wygrywa się sojuszami](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html): 10/07/02 - 10/07/04
1. [160713 - Jak ukraść ze Świecy Zajcewów](/rpg/inwazja/opowiesci/konspekty/160713-ukrasc-ze-swiecy-zajcewow.html): 10/07/04 - 10/07/06
1. [161204 - Zajcewowie po drugiej stronie](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html): 10/07/05 - 10/07/07
1. [160803 - Sleeper agent Oktawiana](/rpg/inwazja/opowiesci/konspekty/160803-sleeper-agent-oktawiana.html): 10/07/07 - 10/07/08
1. [161229 - Presja ze strony Czelimina](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html): 10/07/08 - 10/07/10
1. [160810 - Zaszczepić Adriana Murarza!](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html): 10/07/09 - 10/07/11
1. [161231 - Eskalacja Czelimina, eskalacja Andrei](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html): 10/07/11 - 10/07/12
1. [160819 - Oblicze guwernantki](/rpg/inwazja/opowiesci/konspekty/160819-oblicze-guwernantki.html): 10/07/12 - 10/07/14
1. [170101 - Patrol? Kralotyczne maskowanie!](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html): 10/07/13 - 10/07/15
1. [160825 - Plany Overminda](/rpg/inwazja/opowiesci/konspekty/160825-plany-overminda.html): 10/07/15 - 10/07/16
1. [170103 - Wojna Bogów w Czeliminie](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html): 10/07/16 - 10/07/18
1. [160914 - Aleksandria, krwawa Aleksandria](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html): 10/07/17 - 10/07/20
1. [161012 - Kontratak Karradraela](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html): 10/07/22 - 10/07/23
1. [170122 - Gambit Anety Rainer](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html): 10/07/19 - 10/07/23
1. [161026 - Zagłodzona ekspedycja Świecy](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html): 10/07/24 - 10/07/25
1. [161102 - Magowie Esuriit w domu](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html): 10/07/26 - 10/07/28
1. [161109 - Jak prawidłowo wpaść w pułapkę](/rpg/inwazja/opowiesci/konspekty/161109-jak-prawidlowo-wpasc-w-pulapke.html): 10/07/29 - 10/07/30
1. [161124 - Ponura historia ekspedycji Esuriit](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html): 10/07/31 - 10/08/01
1. [161130 - Sprowadzenie Mare Vortex](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html): 10/08/02 - 10/08/04
1. [161207 - Lizanie ran na KADEMie](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html): 10/08/05 - 10/08/07
1. [170104 - Spalone generatory pryzmatyczne](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html): 10/08/08 - 10/08/09
1. [170125 - Przeprawa do Świecy Daemonica](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html): 10/08/10 - 10/08/11
1. [170208 - Koniec wojny z Karradraelem](/rpg/inwazja/opowiesci/konspekty/170208-koniec-wojny-z-karradraelem.html): 10/08/12 - 10/08/13
1. [170215 - To się nazywa 'łupy wojenne'?](/rpg/inwazja/opowiesci/konspekty/170215-to-sie-nazywa-lupy-wojenne.html): 10/08/14 - 10/08/16
1. [170226 - Wygraliśmy wojnę... prawda?](/rpg/inwazja/opowiesci/konspekty/170226-wygralismy-wojne-prawda.html): 10/08/18 - 10/08/20
1. [170222 - Renata Souris i echo Urbanka...](/rpg/inwazja/opowiesci/konspekty/170222-renata-souris-i-echo-urbanka.html): 10/08/14 - 10/08/15
1. [170517 - Zegarmistrz i Alegretta](/rpg/inwazja/opowiesci/konspekty/170517-zegarmistrz-i-alegretta.html): 10/08/17 - 10/08/18
1. [170405 - Chyba wolelibyśmy kartony...](/rpg/inwazja/opowiesci/konspekty/170405-chyba-wolelibysmy-kartony.html): 10/08/18 - 10/08/19
1. [170531 - Autowar: pierwsze starcie](/rpg/inwazja/opowiesci/konspekty/170531-autowar-pierwsze-starcie.html): 10/08/20 - 10/08/22
1. [170607 - Oślepienie autowara](/rpg/inwazja/opowiesci/konspekty/170607-oslepienie-autowara.html): 10/08/23 - 10/08/24
1. [170519 - Odzyskać Aegis 0003](/rpg/inwazja/opowiesci/konspekty/170519-odzyskac-aegis-0003.html): 10/08/21 - 10/08/22
1. [170823 - Suma niedokończonych spraw...](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html): 10/08/25 - 10/08/28
1. [170830 - Wdzięczność Iliusitiusa](/rpg/inwazja/opowiesci/konspekty/170830-wdziecznosc-iliusitiusa.html): 10/08/29 - 10/08/30
1. [170914 - Kolejna porażka Kinglorda](/rpg/inwazja/opowiesci/konspekty/170914-kolejna-porazka-kinglorda.html): 10/08/31 - 10/09/02
1. [170920 - Początki prokuratury](/rpg/inwazja/opowiesci/konspekty/170920-poczatki-prokuratury.html): 10/09/03 - 10/09/05

## 10. Córka Lucyfera

10/02/08 - 10/02/17
[Omówienie kampanii](/rpg/inwazja/opowiesci/konspekty/kampania-corka-lucyfera.html)

1. [170530 - Córka Lucyfera](/rpg/inwazja/opowiesci/konspekty/170530-corka-lucyfera.html): 10/02/08 - 10/02/10
1. [170603 - Córka jest narzędziem?](/rpg/inwazja/opowiesci/konspekty/170603-corka-jest-narzedziem.html): 10/02/11 - 10/02/12
1. [170620 - Pułapka na Luksję](/rpg/inwazja/opowiesci/konspekty/170620-pulapka-na-luksje.html): 10/02/13 - 10/02/15
1. [170628 - Ukradziona Apokalipsa](/rpg/inwazja/opowiesci/konspekty/170628-ukradziona-apokalipsa.html): 10/02/16 - 10/02/17

## 11. Taniec Liści

10/05/14 - 10/07/24
[Omówienie kampanii](/rpg/inwazja/opowiesci/konspekty/kampania-taniec-lisci.html)

1. [160922 - Czarnoskalski konwent RPG](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html): 10/05/14 - 10/05/19
1. [160915 - Rekrutacja mimo woli](/rpg/inwazja/opowiesci/konspekty/160915-rekrutacja-mimo-woli.html): 10/05/26 - 10/05/29
1. [160911 - Reedukacja Infensy](/rpg/inwazja/opowiesci/konspekty/160911-reedukacja-infensy.html): 10/07/15 - 10/07/17
1. [160921 - Wandy wolność i wróżda](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html): 10/07/22 - 10/07/24

## 12. Ucieczka do Przodka

10/05/31 - 10/06/27
[Omówienie kampanii](/rpg/inwazja/opowiesci/konspekty/kampania-ucieczka-do-przodka.html)

1. [150928 - Zamtuz z jedną wiłą](/rpg/inwazja/opowiesci/konspekty/150928-zamtuz-z-jedna-wila.html): 10/05/31 - 10/06/01
1. [151003 - Zamtuz przestaje działać](/rpg/inwazja/opowiesci/konspekty/151003-zamtuz-przestaje-dzialac.html): 10/06/02 - 10/06/03
1. [151013 - Kontrolowany odwrót z zamtuza](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html): 10/06/04 - 10/06/05
1. [151101 - Mafia Gali w szpitalu](/rpg/inwazja/opowiesci/konspekty/151101-mafia-gali-w-szpitalu.html): 10/06/06 - 10/06/07
1. [151220 - Z Null Fieldem w garażu...](/rpg/inwazja/opowiesci/konspekty/151220-z-nullfieldem-w-garazu.html): 10/06/08 - 10/06/09
1. [151223 - ..można uwolnić czarodziejkę...](/rpg/inwazja/opowiesci/konspekty/151223-mozna-uwolnic-czarodziejke.html): 10/06/10 - 10/06/11
1. [151229 - ..choć to na sektę nie pomoże (PT)](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html): 10/06/12 - 10/06/13
1. [151230 - Zajcew ze śmietnika partnerem...](/rpg/inwazja/opowiesci/konspekty/151230-zajcew-ze-smietnika-partnerem.html): 10/06/08 - 10/06/09
1. [160106 - ...i kult zostaje rozgromiony](/rpg/inwazja/opowiesci/konspekty/160106-i-kult-zostaje-rozgromiony.html): 10/06/10 - 10/06/11
1. [160117 - Muchy w sieci Korzunia](/rpg/inwazja/opowiesci/konspekty/160117-muchy-w-sieci-korzunia.html): 10/06/12 - 10/06/13
1. [170423 - Rozpad magii w Leere](/rpg/inwazja/opowiesci/konspekty/170423-rozpad-magii-w-leere.html): 10/06/24 - 10/06/27

## 13. Nicaretta

10/07/09 - 10/07/29
[Omówienie kampanii](/rpg/inwazja/opowiesci/konspekty/kampania-nicaretta.html)

1. [161103 - Egzorcyzmy w Żonkiborze](/rpg/inwazja/opowiesci/konspekty/161103-egzorcyzmy-w-zonkiborze.html): 10/07/09 - 10/07/12
1. [161110 - Succubus myśli, że uciekł](/rpg/inwazja/opowiesci/konspekty/161110-succubus-mysli-ze-uciekl.html): 10/07/14 - 10/07/20
1. [161115 - Uciekła do femisatanistek](/rpg/inwazja/opowiesci/konspekty/161115-uciekla-do-femisatanistek.html): 10/07/21 - 10/07/22
1. [161129 - Ewakuacja Natalii, wejście maga](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html): 10/07/23 - 10/07/24
1. [161206 - Ucieczka Sióstr Światła](/rpg/inwazja/opowiesci/konspekty/161206-ucieczka-siostr-swiatla.html): 10/07/25 - 10/07/26
1. [161220 - Zniszczenie posągu Arazille](/rpg/inwazja/opowiesci/konspekty/161220-zniszczenie-posagu-arazille.html): 10/07/27 - 10/07/30
1. [170113 - 'Dzisiaj złapiemy Nicarettę!'](/rpg/inwazja/opowiesci/konspekty/170113-dzisiaj-zlapiemy-nicarette.html): 10/08/01 - 10/08/04
1. [170120 - Wielki sojusz powszechny](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html): 10/08/05 - 10/08/06
1. [170214 - Nekroborg dla Laetitii Gai](/rpg/inwazja/opowiesci/konspekty/170214-nekroborg-dla-laetitii-gai.html): 10/07/26 - 10/07/29

## 14. Prawdziwa natura Draceny

10/05/09 - 10/07/25
[Omówienie kampanii](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)

1. [170725 - Krzywdzę, bo kocham](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html): 10/05/09 - 10/05/11
1. [140928 - Policjant widział anioła](/rpg/inwazja/opowiesci/konspekty/140928-policjant-widzial-aniola.html): 10/05/12 - 10/05/13
1. [141012 - Aplikanci widzieli gorathaula](/rpg/inwazja/opowiesci/konspekty/141012-aplikanci-widzieli-gorathaula.html): 10/05/30 - 10/06/07
1. [141019 - Lekarka widziała cybergothkę](/rpg/inwazja/opowiesci/konspekty/141019-lekarka-widziala-cybergothke.html): 10/06/08 - 10/06/09
1. [141025 - Gildie widziały protomaga](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html): 10/06/10 - 10/06/11
1. [141026 - Dracena widziała swój koszmar](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html): 10/06/12 - 10/06/13
1. [141102 - Paulina widziała sępy nad Nikolą](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html): 10/06/14 - 10/06/15
1. [141110 - Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html): 10/06/16 - 10/06/17
1. [160901 - Uwięziony w komputerze!](/rpg/inwazja/opowiesci/konspekty/160901-uwieziony-w-komputerze.html): 10/06/18 - 10/06/20
1. [160904 - Rozpaczliwie sterowany wzór](/rpg/inwazja/opowiesci/konspekty/160904-rozpaczliwie-sterowany-wzor.html): 10/06/21 - 10/06/24
1. [160907 - Dracena... Blakenbauer?](/rpg/inwazja/opowiesci/konspekty/160907-dracena-blakenbauer.html): 10/06/25 - 10/06/26
1. [160910 - Na żywym organiźmie Draceny...](/rpg/inwazja/opowiesci/konspekty/160910-na-zywym-organizmie-draceny.html): 10/06/27 - 10/06/29
1. [160918 - Walka o duszę Draceny](/rpg/inwazja/opowiesci/konspekty/160918-walka-o-dusze-draceny.html): 10/06/30 - 10/07/03
1. [161018 - Ballada o duszy ognistej](/rpg/inwazja/opowiesci/konspekty/161018-ballada-o-duszy-ognistej.html): 10/07/02 - 10/07/04
1. [161002 - Wyciek syberionu](/rpg/inwazja/opowiesci/konspekty/161002-wyciek-syberionu.html): 10/07/11 - 10/07/13
1. [161009 - 'Paulino, zmieniłaś się...'](/rpg/inwazja/opowiesci/konspekty/161009-paulino-zmienilas-sie.html): 10/07/14 - 10/07/16
1. [170212 - Nieufność w małym mieście](/rpg/inwazja/opowiesci/konspekty/170212-nieufnosc-w-malym-miescie.html): 10/07/22 - 10/07/25

## 15. Adaptacja kralotyczna

10/10/26 - 10/12/04
[Omówienie kampanii](/rpg/inwazja/opowiesci/konspekty/kampania-adaptacja-kralotyczna.html)

1. [170718 - Umarł z miłości](/rpg/inwazja/opowiesci/konspekty/170718-umarl-z-milosci.html): 10/10/26 - 10/10/28
1. [180310 - Kraloth w piwnicy](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html): 10/11/28 - 10/12/01
1. [180311 - Cienie procesu Izy](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html): 10/12/03 - 10/12/04
1. [180318 - Czyżby drugi kraloth?](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html): 10/12/05 - 10/12/07
1. [180321 - Porwanie pod nosem hipisów](/rpg/inwazja/opowiesci/konspekty/180321-porwanie-pod-nosem-hipisow.html): 10/12/08 - 10/12/09
1. [180327 - Szept Mare Felix](/rpg/inwazja/opowiesci/konspekty/180327-szept-mare-felix.html): 10/12/08 - 10/12/10
1. [180402 - Pętla dookoła niekralotha](/rpg/inwazja/opowiesci/konspekty/180402-petla-dookola-niekralotha.html): 10/12/13 - 10/12/15
1. [180411 - Złodzieje nieważnych artefaktów](/rpg/inwazja/opowiesci/konspekty/180411-zlodzieje-niewaznych-artefaktow.html): 10/12/13 - 10/12/15
1. [180526 - Krystalia poluje na niekralotha](/rpg/inwazja/opowiesci/konspekty/180526-krystalia-poluje-na-niekralotha.html): 10/12/17 - 10/12/19
1. [180619 - Śledzie na Fazie](/rpg/inwazja/opowiesci/konspekty/180619-sledzie-na-fazie.html): 10/12/02 - 10/12/04

## 16. Wizja Dukata

10/12/15 - 11/10/29
[Omówienie kampanii](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)

1. [171205 - Dlaczego magitrownia działa?](/rpg/inwazja/opowiesci/konspekty/171205-dlaczego-magitrownia-dziala.html): 10/12/15 - 10/12/17
1. [171001 - Powrót do domu](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html): 11/08/25 - 11/08/27
1. [171003 - Świnia na portalisku](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html): 11/08/28 - 11/08/30
1. [171218 - Senesgradzka kopia Pauliny](/rpg/inwazja/opowiesci/konspekty/171218-senesgradzka-kopia-pauliny.html): 11/08/28 - 11/08/31
1. [171004 - Niestabilna magitrownia](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html): 11/09/01 - 11/09/03
1. [171010 - Jasny sygnał Tamary](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html): 11/09/06 - 11/09/09
1. [171015 - Powstrzymana wojna domowa](/rpg/inwazja/opowiesci/konspekty/171015-powstrzymana-wojna-domowa.html): 11/09/10 - 11/09/11
1. [171022 - Tylko nieświadomy elemental](/rpg/inwazja/opowiesci/konspekty/171022-tylko-nieswiadomy-elemental.html): 11/09/12 - 11/09/14
1. [171024 - Detektyw, lecz nie Sądecznego](/rpg/inwazja/opowiesci/konspekty/171024-detektyw-lecz-nie-sadecznego.html): 11/09/16 - 11/09/18
1. [171029 - W co gra Sądeczny?](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html): 11/09/20 - 11/09/22
1. [171031 - Utracona kontrola](/rpg/inwazja/opowiesci/konspekty/171031-utracona-kontrola.html): 11/09/24 - 11/09/27
1. [171101 - Magimedy przed epidemią](/rpg/inwazja/opowiesci/konspekty/171101-magimedy-przed-epidemia.html): 11/09/28 - 11/09/30
1. [171105 - Epidemia Dezinhibicji](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html): 11/10/01 - 11/10/02
1. [171115 - Kryzysowo tymczasowy dyktator](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html): 11/10/03 - 11/10/04
1. [171220 - Potrójna magitrownia Histogram](/rpg/inwazja/opowiesci/konspekty/171220-potrojna-magitrownia-histogram.html): 11/10/05 - 11/10/06
1. [171229 - Esuriit w Półdarze](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html): 11/10/07 - 11/10/09
1. [180104 - Wspaniały Wieprz Wojtka](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html): 11/10/12 - 11/10/15
1. [180110 - Odzyskana władza Pauliny](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html): 11/10/10 - 11/10/12
1. [180112 - Chrumpokalipsa](/rpg/inwazja/opowiesci/konspekty/180112-chrumpokalipsa.html): 11/10/16 - 11/10/18
1. [180214 - Nie podłożona świnia Łucji](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html): 11/10/15 - 11/10/17
1. [180503 - Dlaczego Kret w jeziorze?](/rpg/inwazja/opowiesci/konspekty/180503-dlaczego-kret-w-jeziorze.html): 11/10/27 - 11/10/29

## 17. Dusza Czapkowika

10/11/08 - 10/11/30
[Omówienie kampanii](/rpg/inwazja/opowiesci/konspekty/kampania-dusza-czapkowika.html)

1. [180418 - Czapkowicka Apatia Kulturalna](/rpg/inwazja/opowiesci/konspekty/180418-czapkowicka-apatia-kulturalna.html): 10/11/08 - 10/11/10
1. [180419 - Jaszczury rządzą miastem](/rpg/inwazja/opowiesci/konspekty/180419-jaszczury-rzadza-miastem.html): 10/11/17 - 10/11/19
1. [180421 - Jaszczur Love Story](/rpg/inwazja/opowiesci/konspekty/180421-jaszczur-love-story.html): 10/11/24 - 10/11/26
1. [180425 - Śledziem w depresję](/rpg/inwazja/opowiesci/konspekty/180425-sledziem-w-depresje.html): 10/11/28 - 10/11/30


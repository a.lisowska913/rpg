---
layout: inwazja-konspekt
title:  "Ratując syrenopnącze"
campaign: powrot-karradraela
players: kić
gm: żółw
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [170217 - Skradziona pozytywka Mausów (SD, PS)](170217-skradziona-pozytywka-mausow.html)

### Chronologiczna

* [170217 - Skradziona pozytywka Mausów (SD, PS)](170217-skradziona-pozytywka-mausow.html)

## Kontekst ogólny sytuacji

## Punkt zerowy:

## Misja właściwa:

**Dzień 1:**

Dwa dni po opuszczeniu przez Mausów KADEMu Daemonica do Silurii przyszła zrozpaczona Infernia. Jedna z bardzo niebezpiecznych roślin z Żółtego Ogrodu Weroniki Seton umiera... i Andżelika nie chce jej powiedzieć dlaczego i jak ratować. Najpewniej sama nie wie, ale Andżelika się może dowiedzieć... a Infernia nie ma pomysłu jak to zrobić. Tak, są dokumenty Weroniki Seton - ale Infernia nie umie się z nimi połączyć; za duża niekompatybilność mentalna między Infernią a Weroniką Seton. A Andżelika by umiała.

Infernia powiedziała, że mogą próbować wskrzesić drzewo jeśli umrze - ale nie wie jak to zrobić na Fazie Daemonica. To drzewo normalnie żyje na Primusie... zgodnie z tym co zapamiętała z wiedzy Weroniki. Razem ze śluzem, afrodyzjakiem, mackami i wszystkim. To nie jest typowe drzewo; to silnie magiczne drzewo. Nieinteligentny vicinius, można powiedzieć. Tyle Infernia wie. Nazywa się to "syrenopnącze" i jest - jako gatunek - stare. Bardzo rzadkie na Primusie.

Siluria próbuje skojarzyć nazwę z "true Diakon": ((Im+U-In=+1, rzadki, W- -> +1, 0) -> 2 - 2 = 0 -> sukces +1 wpływ). (Poszerzenie wpływu: +ciekawostka dla Andżeliki odnośnie tej rośliny -> tool bonus). Syrenopnącze jest odpowiednikiem kocimiętki dla kralothów. Diakoni bardzo go unikają; podobno (plotki) Syrenopnącze jest wykorzystywane przy fuzji kralotha z magiem. Inna plotka: syrenopnącze podobno zostało sprowadzone przez kralothy ze swojej Fazy by terraformować Primusa. Po rozdarciu połączenia przez Karradraela, syrenopnącza zostawały niszczone (a kralothy tępione).

Nadal nie wyjaśnia, czemu Andżelika chce zniszczenia syrenopnącza na KADEMie; w Żółtym Ogrodzie jest tak naprawdę dość niegroźne. Po prostu nie jest nieszkodliwe - dlatego jest w Żółtym Ogrodzie.

Siluria poszła więc do Andżeliki. Ciekawy problem. Andżelika jest czymś zajęta, ale widząc Silurię się ucieszyła. Cała Andżelika. Siluria pomogła jej troszkę w analizie dziwnego płynu. Andżelika westchnęła. Kolejny dziwny eliksir Karoliny. Nadal niegroźny, ale... wypłynął. Ma go ze Świecy. Ten eliksir jest teraz najlepszym eliksirem imprezowym w niektórych kręgach... nieszkodliwy, ale... nie wszystkim się to podoba. Dlatego Andżelika chciała sprawdzić, czy to nie jest niebezpieczne...

* Wiesz, KADEM... KADEM coś znaczy. My nie zajmujemy się takimi rzeczami - smutna Andżelika
* KADEM to jego magowie - Siluria, z pocieszającym uśmiechem - A sam eliksir jest przecież niegroźny; nawet kaca po nim nie ma

Siluria przeszła do głównego dania. Rozmawiała z Infernią. O co chodzi z syrenopnączem?

* Pamiętam jak ciężko było posadzić syrenopnącze. Jak bardzo trudno było Weronice Seton to zrobić. To nie było bezkosztowe. - Andżelika
* Kuba był wściekły. Serio. - Andżelika
* No dobrze, siedzi, urosło... tym bardziej jeśli koszt został poniesiony, może nie warto go odnawiać? - Siluria
* Pamiętam, że Kuba powiedział, że ludzie ucierpieli by to zrobić czy zregenerować. A Infernia to zrobi. - Andżelika, zrezygnowana

Siluria zdecydowała się połączyć pochlebstwo z ciekawostką, by Andżelikę przekonać. (S: Im0,Um,Sp,In -> 4 An: Im+ -> 1)(W- ->+2)(Sprzęt+ -> +1)(-> 7v1 -> S). Andżelika się zgodziła, zrezygnowana. Siluria zauważyła, że ona też nie chce krzywdzić ludzi. Andżelika wie. Po prostu zdaniem Andżeliki Infernia ma to gdzieś...

Trzy godziny później Andżelika przyszła do Silurii.

Andżelika zdiagnozowała. Na syrenopnącze został wywarty wpływ Fazy Daemonica - inhibitory zostały źle przekierowane. Syrenopnącze zaczęło umierać; ono nie działa dobrze na Fazie, potrzebuje warunków z Primusa. Można spróbować przesadzić, ale wtedy też umrze. Trzeba je nakarmić. I tu pojawia się problem - na bazie wszystkiego co Andżelika wyczytała, to drzewo potrzebuje silnej energii erotycznej. Musi też przeprowadzić badania jak silnej. I naenergetyzowane Quarki powinny same w sobie wystarczyć...

Siluria wpadła na to, że można użyć energii z magów. Lub ludzi. Potężna orgia na Primusie. To przekierować w odpowiednie kryształy Quark i powinno to zasilić nieszczęsne syrenopnącze. Powiedziała Andżelice, że ma zamiar się z nią konsultować w tej sprawie...

Z ciężkim sercem, Siluria skontaktowała się z Rafaelem Diakonem. Jest to mag Millennium, który MOŻE coś wiedzieć o syrenopnączu - a jeśli nie wie, to wie, kto wie... (-1 surowiec). Użyła systemów komunikacyjnych KADEMu.

Rafael bardzo się ucieszył słysząc Silurię. Siluria wiedziała, że tak będzie. Rafael zwykle się cieszy. Zapytany o syrenopnącze, powiedział jej co następuje (zgodnie z tym o co pytała Siluria):

* Syrenopnącze żywi się konwertując Aquam Vim w Vim. Czyli elementy magii krwi i nieprawdopobna moc erotyczna.
* Syrenopnącze jest genialnym afrodyzjakiem
* Syrenopnącze w dobrym stanie śpiewa. Przyciąga do siebie potencjalne ofiary, jak śpiew syreny.
* Syrenopnącze powoduje najcudowniejsze uczucia ekstazy, większe, niż jakikolwiek kraloth. Uzależnia jeszcze mocniej. Wymaga silnej puryfikacji.
* Kralothy wariują na punkcie syrenopnącz; to najwyższy smakołyk dla nich i terraformer

Rafael ogólnie powiedział, że plan Silurii z orgią zadziała. Ale wymaga też elementu biologicznego, sam magiczny nie wystarczy. Zarekomendował jej poświęcenie grupy ludzi lub jakiegoś maga, który chce przeżyć coś, czego nigdy nie przeżył. I już nie przeżyje. Siluria od razu pomyślała o Inferni...

Rafael nalegał, by Siluria wzięła go ze sobą jako wsparcie i badacza, ale Siluria odbiła tym, że jest na KADEMie Daemonica. A Siluria obiecała, że ludziom nie stanie się przy tym krzywda, więc część tych pomysłów i tak odpada... szkoda, zdaniem Rafaela - można by zrobić to skuteczniej.

* Uwierz mi, warto przejść przez cały ten mechanizm by go zrozumieć - od usłyszenia śpiewu, przez bycie zamkniętym w kwiecie aż do uzależnienia i bolesnego odwyku - Rafael do Silurii, zachęcając
* Próbowałeś? - Siluria, zaskoczona
* Oczywiście... - Rafael z szerokim uśmiechem - Jest to niezapomniane uczucie. I wiele, wiele uczy jak funkcjonuje Twój własny organizm.
* A w to nie wątpię... - Siluria, w sumie nie zaskoczona, bo Rafael

(S: Im-Um+Sp0In+ -> 1, R: Im+Um+In0 -> 2)(W+ -> +2 (Rafael -> Siluria, więc +2 Sil a nie -2))(1+->SW+)

Silurii udało się skutecznie przekonać Rafaela, że to nie jest dobry pomysł. ALE. Jeśli Rafael załatwi Silurii odpowiednie systemy puryfikacyjne i zabezpieczenia... to Siluria w to wchodzi. Rafael się zgodził; spróbuje jej zmontować coś na jutro lub za dwa dni. Siluria dzięki temu da się skusić i zobaczy jak to jest a Rafael jej w tym z radością pomoże - to ten typ maga...

Czyli: Rafael załatwi Silurii defensywę i dowie się, po czym poznać KIEDY zacznie śpiewać. Siluria musi zdobyć Quarki naładowane erotyczną energią. Oki. To moment, gdy do gry wchodzi znowu Infernia...

Siluria powiedziała Inferni, że teraz KONIECZNIE musi karmić to drzewo. Umiera z głodu. Infernia, przejęta, spytała jak to naprawić? Siluria odpowiedziała ze śmiechem, że nakarmić - Quarki muszą mieć kolor różowo-czerwony. DUŻO takich Quarków.

* Potrzebujemy potężną orgię? - Infernia
* Tak, z zawstydzeniem i innymi takimi... ale nikomu nie może stać się krzywda. - Siluria
* Ok... czemu? - Infernia, zaskoczona
* Obiecałam. Dałam słowo. - Siluria, poważnie

Infernia obiecała, że nikogo nie skrzywdzi. Zorganizuje porządną orgię; ma znajomości. Siluria powiedziała, że potrzebna jest DUŻA orgia. Jak duża? DUŻA DUŻA. Infernia się zasępiła; da się załatwić.

Infernia załatwia uczestników itp. na imprezę (liczę ilość Wpływu wobec Niewielkiego, bo zna właściwe osoby): 

(Im+Sp+Um0In- -> 1)(W-P+++ -> -1)(Poz+Sur+ -> 2)(-2 zawodowy) -> Test vs 0 -> 14 -> S

Inferni udało się załatwić dużą ilość właściwych uczestników (Skala Przytłaczająca, ~50 osób)

Siluria dla odmiany poszła porozmawiać z... Adonisem Sowińskim, prezydentem Dare Shiver. Wzięła na spotkanie z szafy fantazyjny, acz pasujący do Adonisa kapelusz. Coś, co mu się spodoba... i poszła odwiedzić Dare Shiver. Adonis przyjął ją z uśmiechem.

Siluria opowiedziała Adonisowi o tym, że pojawia się impreza, na której może pojawić się sporo ciekawych dare'ów. Takich 18+. Siluria powiedziała, że ona potrzebuje konkretnego koloru energii magicznej - zawstydzenie, presja, niepokój, strach... oraz erotyka. A Adonis będzie miał okazję zrobić coś nietypowego, co zwykle się nie pojawia. Nowy smak - impreza głęboko erotyczna, coś, czego na co dzień w Dare Shiver nie ma i nawet magowie tego nie robią. Adonis zastrzygł uszami. Jest zainteresowany. 

Siluria oczekuje od Adonisa wprowadzenia koloru do tej magii. Mistrza ceremonii. Jego. Kogoś, kto zapewni tej imprezie LEGENDARNY status, coś, czego nie było jeszcze do tej pory. A jak chodzi o formułę, Siluria nie jest jeszcze pewna; najpewniej coś na kształt balu maskowego, by pozwolić ludziom i magom pewne rzeczy przekroczyć. Adonisowi spodobał się pomysł - dzięki tym maskom i obecności ludzi pozwoli na przekroczenie dodatkowych granic, kolejnego tabu.

(S: Im+Sp+Um+In+ -> 4 A: Im-Um+In+ -> 1)(W0S+ -> -1)(Syt0Sprzęt0M0Sur0Kom0 -> 0) -> Test 4v2 -> S

Adonisowi spodobał się pomysł. Załatwi odpowiednią miejscówkę, mistrza ceremonii (siebie) oraz dodatkowo odpowiednie osoby, zarówno ludzi jak i magów. I zapewni podstawowe bezpieczeństwo. A Siluria zapewniła, że Infernia załatwi uczestników (sporą ilość), obsługę imprezy i Quarki.

Jako katalistę Siluria poprosiła Akumulatora Diakona. Ten mag jest w stanie dobrze się bawić a jednocześnie zapewnić maksymalizację transformacji energii magicznej.

Perfekcyjnie. Czas sprawdzić samą imprezę.

**Dzień 4:**

Sama impreza. Impreza ma miejsce na placu budowy hipermarketu NeWorld; Adonis w jakiś sposób dał radę to załatwić i zorganizować, wieczorem. Załatwił też osłonę - skrzydło terminusów z Kompleksu Centralnego. Impreza jest brandowana jako "Zahamowaniom Precz" - najsilniejsza i efektowna współorganizowana impreza między Dare Shiver, KADEM, Millennium i elementy Świecy.

Sama impreza jest trójetapowa z perspektywy mechaniki projektów. Czyli trzy rzuty. Walka przeciw W+. Siluria przygotowała jednak określone parametry.

* Sama impreza. Zarządzana przez Adonisa, który świetnie kontroluje tłum a katalista Akumulator przesyła to w Quarki.

(Im+Sp+Um+In+ -> 4)(Diff+W0Sk++++ -> 5)(Syt+Sprz+Mag+Sur+Kom0 -> 4) -> Test vs 13 -> SW0 -> +7 wpływów na drzewo (z 20 potrzebnych).

* Siluria w tym momencie zajmuje się pomaganiem różnym osobom w tłumie; wyszukuje tych, którzy mają jeszcze opory i pomaga im je złamać. Przekroczyć tą przepaść. Na samą imprezę jest przygotowana feromonami, środkami Karoliny itp (-1 surowiec). A dookoła porozkładane są rzeczy zrobione przez Karolinę które mają zadziałać.

(Im+Sp+Um+In+ -> 4)(Diff+W0Sk++ -> 3)(Syt+Sprz+Mag+Sur+Kom0 -> 4) -> Test vs 11 -> SW0 -> +5 wpływów na drzewo (z 20 potrzebnych).

* Są namioty przyjemności wszelakich, gdzie możesz się odważyć coś zrobić, albo masz scenę gdzie możesz coś zrobić. Tym zajmuje się Infernia - mistrzyni zastraszania. Przekroczyć ich granice. Strach i przyjemność. 

(Im+Sp+Um+In0 -> 3)(Diff0W0Sk++ -> 2)(Syt+Sprz+Mag+Sur+Kom0 -> 4) -> Test vs 9 -> SW+ -> +5 wpływów na drzewo (z 20 potrzebnych).

* *Wynik*:

Udało się zakumulować 17/20 potrzebnych jednostek energii. Sama impreza udała się; poszło bez większych problemów. Na pewno terminusi musieli coś załatwić, ale ta impreza była uznana za jedną z bezpieczniejszych i najbardziej zakamuflowanych i jednocześnie erotycznie interesujących ;-). Innymi słowy, ogromne powodzenie.

**Dzień 5:**

Faza Daemonica, Żółty Ogród Weroniki Seton. Siluria i Infernia. Rafael załatwił dla Silurii potężne puryfikatory. Teraz tylko brakuje ofiary do nakarmienia drzewa. I Siluria poważnie, poważnie myśli, czy nie być tą ofiarą... z drugiej strony, Infernia na pewno zgłosiłaby się na ochotniczkę...

W pobliżu jest też Andżelika. W najcięższym pancerzu bojowym jaki mogła zdobyć.

| Impuls | Spec | Umiej | Inkl | SUMA | Diff | Wpływ | Skala | SUMA | Sytuacja | Sprzęt | Magia | Surowiec | Komplikacja | SUMA | VERSUS | Test? | Rzut | Wynik |
|   +    |   0  |   +   |   +  |  3   | ---  |   0   |   0   |  -3  |    +     |   +    |   --  |    +     |      0      |  1   |  -2    |  -1   |  1   |   0   |

Remis. Siluria wzmocniła drzewo (+1 -> 28/30), ale w wyniku Infernia także została zahipnotyzowana przez syrenopnącz. Podążyła do drzewa, oddając mu się jak Siluria. Andżelika patrzy z rosnącym przerażeniem.

| Impuls | Spec | Umiej | Inkl | SUMA | Diff | Wpływ | Skala | SUMA | Sytuacja | Sprzęt | Magia | Surowiec | Komplikacja | SUMA | VERSUS |Test? | Rzut | Wynik |
|   +    |   0  |   +   |   +  |  3   | ---  |   0   |   0   |  -3  |    +     |   +    |   --  |    ++    |      0      |  2   |  -2    |  0   |  +1  |  1    |

Wspierane przez Infernię i Silurię, syrenopnącze dało radę się najeść. (30/30). Odżyło. Jednak próbuje kontynuować operację pożerania, asymilując jak najwięcej z dwóch niezbyt już świadomych czarodziejek.

| Impuls | Spec | Umiej | Inkl | SUMA | Diff | Wpływ | Skala | SUMA | Sytuacja | Sprzęt | Magia | Surowiec | Komplikacja | SUMA | VERSUS | Test? | Rzut | Wynik |
|   +    |   0  |   +   |   +  |  3   | ---  |  -2   |   0   |  -5  |    +     |   ++   |   --  |    ++    |      0      |  3   |  -2    |  -1   | +2W+ |  1W+  |

Silurii z pomocą Andżeliki udało się wyrwać po kilkunastu minutach, choć jest całkowicie, całkowicie nie kontaktująca. A raczej - Andżelika ją wyrwała. Ją i Infernię. Siluria i Infernia są już uzależnione i błagają o możliwość powrotu; power suit Andżeliki jest jednak najsilniejszym, jaki ta jest w stanie założyć.

Czas na puryfikację. Niech cholerny Rafael robi swoją robotę dobrze...

| Impuls | Spec | Umiej | Inkl | SUMA | Diff | Wpływ | Skala | SUMA | Sytuacja | Sprzęt | Magia | Surowiec | Komplikacja | SUMA | VERSUS | Test? | Rzut | Wynik |
|   +    |   0  |   +   |   0  |  2   | ---  |  -2   |   0   |  -5  |    +     |   +    |  +++  |    +     |      0      |  6   |   -2   |   1   |  +1  |   2   |

Szczęśliwie, Rafaelowe mikstury świetnie działały. Siluria cierpi, Infernia też. One bardzo chcą wrócić. Ale puryfikacja trwa; po zatrzymaniu dziewczyn w skrzydle medycznym miną jeszcze ze dwa dni i obie wrócą do formy... bogate o nowe doświadczenia i wiedzące już, dlaczego syrenopnącze jest tak niebezpieczne...

**Dzień 7:**

Czarodziejki doszły do siebie i syrenopnącze także zostało uratowane...

Jedna rzecz jaką Siluria absolutnie zrobi - ona i Andżelika wymuszają na Inferni, że ta nikogo nie będzie krzywdzić nigdy.

| Impuls | Spec | Umiej | Inkl | SUMA | Diff | Wpływ | Skala | SUMA | Sytuacja | Sprzęt | Magia | Surowiec | Komplikacja | SUMA | VERSUS | Test? | Rzut | Wynik |
|   +    |   +  |   +   |   +  |  4   | N/A  |   0   |   0   |   0  |    +     |   +    |   0   |    0     |      0      |  2   |  -1    |   5   |  N/A |   N/A |

Przy takiej dysproporcji, Infernia obiecała, że nikim nie nakarmi drzewa (syrenopnącza)... i dotrzyma słowa.

# Progresja

* Siluria Diakon: powiązana z organizacją gigantycznej imprezy erotycznej wraz z Dare Shiver
* Adonis Sowiński: powiązany z organizacją gigantycznej imprezy erotycznej wraz z Silurią Diakon

## Frakcji
* KADEM: ma dostęp do syrenopnącza; zostało uratowane

# Streszczenie

# Zasługi

* mag: Siluria Diakon, która się skusiła na zakosztowanie syrenopnącza i je nakarmiła używając gigantycznej współorganizowanej imprezy erotycznej.
* mag: Infernia Diakon, ratowała w Żółtym Ogrodzie Weroniki Seton syrenopnącze, przez co miała najlepsze chwile swojego życia... do odwyku.
* mag: Andżelika Leszczyńska, nieszczęśliwa z powodu rosnącej reputacji KADEMu jako ero-narko-gildii; wyciągnęła Silurię i Infernię z syrenopnącza.
* mag: Rafael Diakon, źródło wiedzy o syrenopnączu dla Silurii i źródło antidotum puryfikacji przeciw jego efektom
* mag: Adonis Sowiński, mistrz ceremonii i współorganizator gigantycznej imprezy erotycznej o której będzie się mówić...
* mag: Akumulator Diakon, katalista i ekspert od akumulacji energii magicznej w Quarkach poproszony przez Silurię o wsparcie.

# Lokalizacje

1. Świat
    1. Faza Daemonica
        1. Mare Vortex
            1. Zamek As'caen
                1. KADEM Daemonica
                    1. Skrzydło wypoczynkowe 
                        1. Wewnętrzny Ogród
                            1. Ogród Weroniki Seton
                                1. Zielony Ogród
                                1. Żółty Ogród
                                1. Czerwony Ogród
                            1. Ogród Trójśpiewu
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Piróg Górny
                    1. Obrzeża
                        1. Osiedle Polne
                            1. Klub Włóczykij
                                1. Klub Dare Shiver, 
                1. Kopalin
                    1. Obrzeża
                        1. Plac budowy hipermarketu NeWorld, 


# Czas

* Opóźnienie: 2
* Dni: 7

# Narzędzia MG

## Cel misji

* Misja testująca nowy wariant mechaniki CR - powinna dawać CIEKAWSZE acz KONTROLOWANE wyniki (historie)
* Misja testująca nowy wariant mechaniki projektów - mechanika projektów działa i daje zacne wyniki
* Eksploracja uczennic Weroniki Seton - Karoliny i Inferni
* Eksploracja ogrodów KADEMu na Fazie Daemonica
* Eksploracja syrenopnącza i jego historii powiązanej z kralothami

## Po czym poznam sukces

* Oczekuję, że nowa mechanika CR da mi następujące wyniki:
    * różnica polega na tym, że pojawią się NIEPEŁNE sukcesy i NADMIERNE sukcesy, które wyeskalują ciekawe wyniki
        * nowe komplikacje poszerzające misje
        * nowe bonusy poszerzające misje
    * mechanika nie będzie wolniejsza; może nawet będzie SZYBSZA
    * lepsza kalibracja konfliktów: nie wszystkie konflikty będą 'trudne' zgodnie ze starą nomenklaturą
* Oczekuję, że w ramach mechaniki projektów zobaczę co następuje:
    * Około 5 prób naprawienia drzewa (w ramach trzydniówki) by był sukces
    * Kilka interesujących wyników wymagających proaktywności od gracza
    * FAKTYCZNIE rosnące postępy
* Karolina i Infernia. Czym się różnią? Jak podchodzą do problemów? Są podobne CZY różne?
    * Karolina i Infernia mają się różnić, acz mają być podobieństwa
    * Pojawi się echo Weroniki Seton, relacja jej z dwoma uczennicami i tymi ogrodami
* Na Fazie Daemonica pojawiają się nowe Ogrody, miejsce, gdzie działają miłośnicy roślin i spokoju - chcę pokazać RÓŻNICE między nimi
    * Czerwony Ogród jest morderczo niebezpieczny; tam są inhibitory i hazmatowe stroje
    * Żółty Ogród jest eksperymentalny i niebezpieczny; są tam inhibitory, ale jest ogólnie dostępny... jak wiesz co robisz
    * Zielony Ogród jest miejscem, gdzie robi się eksperymenty z magicznymi roślinami i egzaminy dla młodych ;-)
    * Ogród Trójśpiewu jest miejscem spokoju, relaksu... ogólnie, fajne miejsce
* Syrenopnącze. Nowa roślina magiczna, która jest skrajnie niebezpieczna.
    * Pojawia się relacja Syrenopnącze - Kraloth
    * Pojawia się powód, czemu jest tak niebezpieczna ta roślina (jeszcze nie wiem)
    * ALBO umrze ALBO postać graczki mu pomoże przetrwać

## Wynik z perspektywy celu

* Mechanika CR: 
    * zadziałało; dzięki tym mechanizmom Rafael dostarczył Silurii eliksir i ona zdecydowała się spróbować.
    * nie sprawdziliśmy poza symulacją co by było, gdyby test był słabszy. Powinno działać.
    * zapis konfliktu jest fatalny (tabelka). Do dopracowania na przyszłej misji.
    * mechanika spowolniła, ale jest nowa. Sprawdzić za miesiąc.
    * kalibracja konfliktów jest DUŻO lepsza.
* Mechanika Projektów: 
    * reskalować. 20 to za dużo, po prostu.
    * około 5 prób zadziałało. Ale zmienić na: W- to +1, W0 to +2, W+ to +3. Wyskalować do tego.
    * proaktywność była; o tej imprezie będzie się mówić ;-)
* Uczennice Weroniki: 
    * Nie udało się tego pokazać. Była tylko Infernia.
* Ogrody KADEMu: 
    * Udało się pokazać hinta. Nie było pełnego pokazania ogrodów i ich różnic.
* Syrenopnącze: 
    * Tu się udało. Nowa roślina została wykryta w całej jej chorej glorii.
    * Roślina przetrwała.

## Logika stopni trudności

* Legenda: 
    * W- (mały wpływ), W0 (normalny), W+ (duży).
    * T8 - wartość testowana 8+.
* By Andżelika została przekonana do czegokolwiek przez Silurię w dziedzinie naukowej, będzie to zawsze W-.
* Syrenopnącze to zawsze jest W0 lub W+ - stabilizacja na stałe to koszt W+, ale sam projekt nie jest szczególnie duży (trzydniówka: W+ = 20).
    * Symuluje to sytuację typu "trudno się tym zajmować, ALE jak już to robimy to nie jest to takie żmudne"
    * Daje to sytuację wymuszającą proaktywność na postaci gracza

## Potencjalny prototyp tabelki:

### Jedna tabelka:

| Postać | VERSUS | Diff | Wpływ | Skala | SUMA | Sytuacja | Sprzęt | Magia | Surowiec | Komplikacja | SUMA |  Test?  | Rzut | Wynik |
| -2 - 4 | -2 - 4 |   +  |   -   |   0   |   0  |    +     |   +    |  +    |    0     |      0      |  3   | P-V+S+S |  ?   |   ?   |

### Dwie tabelki:

Tabelka stopnia wyzwania:

| VERSUS | Diff | Wpływ | Skala | SUMA | Sytuacja | Sprzęt | Magia | Surowiec | Komplikacja | SUMA | FINAL_VERSUS |
|   2    |   +  |   -   |   0   |   0  |    +     |   +    |  +    |    0     |      0      |  3   |     5        |

Tabelka konfliktu:

| Postać | FINAL_VERSUS |  Test?  | Rzut | Wynik |
| -2 - 4 |       5      |  P-FV   |  ?   |   ?   |

### Trzy tabelki:

Tabelka postaci w znanej sytuacji:

| .Impuls. | .Specka. | .Umiejka. | .Inklinacja. | .CECHY. |  .Sytuacja. | .Sprzęt. | .Magia. | .Surowiec. |  .AKCJA. | .POSTAĆ. |
|   +      |   0      |     +     |     +        |     3   |        +    |     +    |    --   |      +     |     1    |      4   |

Tabelka opozycji postaci:

| .VERSUS. | .Diff. | .Wpływ. | .Skala. | .MODIFIERS. | .OPOZYCJA. |
|     2    |   +++  |     0   |     0   |     3       |      5     |

Tabelka konfliktu:

| Postać | Opozycja | Test? | Rzut | Wynik |
|   4    |    5     |  -1   | +2W+ |  S W+ |

---
layout: inwazja-konspekt
title:  "Muchy w sieci Korzunia"
campaign: ucieczka-do-przodka
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [160106 - ...i kult zostaje rozgromiony (PT)](160106-i-kult-zostaje-rozgromiony.html)

### Chronologiczna

* [160106 - ...I kult zostaje rozgromiony (PT)](160106-i-kult-zostaje-rozgromiony.html)

## Kontekst misji

Kły Kaina odparte. Czas zamknąć temat kultu w Przodku na jakiś czas.
Relikwia jest w niewiadomym miejscu.
Niedobitki kultu kryją się gdzieś po wsiach; policja ma pełne ręce roboty.
Ernest i Filip Brzeszczot to dzieciaki lokalnego starosty; robią sobie orgietki z kumplami. Na jedną trafiła Kinga...
Żeby pomścić Kingę, Mirek wszedł w kontakty z "kultystą" (Korzuniem). I czas zapłacić...

## Stawki:

- Czy Łukasz Perkas da radę uchronić się przed nędznym losem (nieważne skąd przyjdzie ów los)? TAK, Korzunio go uratował.
- Czy Alicja Gąszcz zostanie agentką Korzunia? TAK, wpadła w sidła Korzunia.

(15 pkt konfliktów)

## Misja właściwa:
### Faza 1: Pająk rozpościera swoje sieci...

Dzień 1: 

Paulina w szpitalu słyszy poruszenie. Przywieźli kogoś; Paulina poszła zobaczyć co się dzieje - w szpitalu są ochroniarze, bo przywieźli syna starosty. Grażyna Tuloz kłóci się z ochroniarzem; to co, jak będzie operowany to też będą stali? Ogólnie niezła kicha.
Na to wszedł ojciec pacjenta, starosta. Wydarł się na doktor Tuloz, że co tu się dzieje. Ona na to, że przygotowują syna. On chce operacji TERAZ - odpowiedziała, że gdzieś jest jakaś praktykantka. Tu jest potrzebny chirurg. Niech starosta jej NIE przeszkadza to ona wszystko załatwi. W swoim czasie.

Paulinę poprosił Karmelik o opinię na temat pacjenta. Chłopak wyglądał, jakby miał petardę Achtung w kieszeni spodni i ona wybuchła. Nie do końca da się uratować wszystko; da się zredukować zniszczenia. Nie jest to śmiertelne. Paulina chciałaby móc młodemu pomóc; wkręciła się jako pomoc w operacji. Prowadzi chirurg Nikodem Emkarnij. 

TEST:
Paulina: chce, by młody był w jak najlepszej formie; łącznie z opcją ewentualnego rozmnożenia.
Druga Strona: chce, by młody był wrakiem człowieka. Skończy na wózku (z niedowładną nogą) i będzie siusiał na siedząco.

Skala trudności: 16/19/22. Paulina ma Dobre Warunki (Tool, Cir) -> +6. Jej wartość natywna to 13 bez magii; atm 19. Wspiera się magicznie - 21. Poprzyciągać, pozlepiać, pousuwać bakterie, rozdzielić lepiej... prowadzi do +3 tool. Ale -1, bo musi ukrywać. Czyli 23. Na kości: (-5), czyli 18.
18 to lepiej niż "nie pomogła", ale to T1 a nie T2 lub T3.

Wniosek: Paulina podbiła o 1 poziom (do T2) - rehabilitacja potrwa bardzo długo; żeby mu pomóc Paulina musi dostarczać mocy magicznej (bo Przodek). To znaczy, że nawet jak się będzie WYDAWAŁO że można go wypisać, trzeba jeszcze go trzymać.
Paulina podbiła o jeszcze 1 poziom (do T3) - podczas pracy nad młodym Karmelik coś zauważył. Leciutkie "coś", nie umie tego zinterpretować; gość ma lekko paranoiczno-chore pomysły, więc najpewniej powiąże Paulinę z jakimiś siłami specjalnymi czy coś takiego.

Ale jako, że wynik jest na T3, młody odzyska sprawność z czasem. I nawet się rozmnoży. Choć nie bardzo szybko.
/TEST

Starosta długo i wylewnie dziękował chirurgowi. W końcu to wymagało niesamowitych umiejętności, by jego synowi pomóc w tej sytuacji. Grażyna Tuloz nie zgodziła się na ochronę w szpitalu, Leopold Brzeszczot nalegał (bardzo mocno). W końcu Grażyna powiedziała Paulinie, że to jej zadanie - przekonać Leopolda, że nie chcemy tu ochrony. Leopold się wściekł - nie będzie gadał z byle lekarką. Grażyna zauważyła, że dzięki tej byle lekarce jego syn będzie chodził.

Paulina zaczęła debatować ze starostą. On jej powiedział, że jest w tym coś jeszcze - to kultysta tak załatwił jego syna. Jego synowie strasznie podpadli kultowi. Leopold ma nagrania i przekazał je policji; ci się zajmują tą sprawą. Dlatego on chce ochronę. Paulina wyjaśniła, że ochrona powinna być policyjna jeśli już; szpital nie dopuszcza ochrony niepolicyjnej. Szpital ma własną ochronę. W wyniku TESTU Paulina przegrała ale podniosła tier; przekonała starostę. Ten załatwił szpitalowi ochronę policyjną.

...czyli Artur Kurczak, Bartosz Bławatek zajmują się ochranianiem szpitala (chłopaka w szpitalu) przed Złym Kultem. Bo szpital tak sobie życzył. Artur dał znać Grażynie co o tym myśli; ta poprosiła Paulinę na dywanik. Opieprzyła ją naturalnie; starosta w życiu by na to nie wpadł jej zdaniem. Paulina w rozmowie zauważyła, że Grażyna nie jest fanką Brzeszczota. Na szczęście następnego dnia wraca Herman; Grażyna ma już powoli dosyć.

Maria (poproszona wcześniej przez Paulinę o rozeznanie sytuacji dookoła nieszczęsnego dziennikarza) powiedziała Paulinie coś ciekawego. Z Perkasem rozmawia ten gość, co wraz z Korzuniem chronił kościół gdy Paulina prosiła go o pomoc, wtedy, przed kultem. Maria nie jest magiem. Nie ma super zdolności. Ale umie czytać z ruchu warg, ma świetną percepcję i cholernie dobrą lornetkę. (rzucenie 20/k20 też pomaga). Przeczytała jedną stronę rozmowy; to co mówił Perkas.

Ogólnie, ten gangster szantażuje Perkasa. Mówi, że nie tylko ma hasło i wie, co Perkas zrobił; nie tylko może to rozpowszechnić. Dodatkowo, jeśli Perkas nie będzie chciał współpracować, jego żonę może w ramach odwetu spotkać to samo, co "tą dziewczynkę" skrzywdzoną przez Perkasa. A co by było do zrobienia? Może przekazać jakąś paczkę, może jakąś informację, może zmylić policję... ogólnie, gangsterskie sprawy. Maciej Orank poszedł; Maria postanowiła go NIE śledzić. 
Paulina ma kłopot. Nie wie jak to ruszyć i jak w ogóle temu dziennikarzowi pomóc... i o co w ogóle tu chodzi? Korzunio szuka ludzi? Ale takimi metodami?

### Faza 2: Dwie muchy wpadają w sieci...

Dzień 2: 

Wrócił Ryszard Herman. On i Grażyna Tuloz długo konferują. Ogólnie, w szpitalu toczy się życie; chłopak się leczy, reszta pacjentów też się leczy. Paulina, jeszcze w szpitalu, stwierdziła, że musi dowiedzieć się gdzie ta relikwia. A kto to wie? Grażyna. Paulina więc wstrzyknęła myśl Grażynie; korzystając ze swoich mocy mentalnych i tego, że jest minion masterką. Zaszczepiła jej straszliwą myśl o relikwii, myśl, że ta została wykradziona. I kult powróci... Intencją Pauliny jest to, by Grażyna Tuloz poszła tego dnia do miejsca, gdzie chowana jest relikwia - by Maria mogła ją śledzić i zlokalizować dla Pauliny relikwię. Grażyna już udowodniła, że ma wredny charakter i żelazną wolę (postawiła się staroście, niewiele się boi, brała ogień na siebie...) a sytuacja dzieje się w Przodku. Paulina wie, że nie będzie łatwo.

TEST:
Grażyna: 6+1+4 -> 11 podstawa. -3 circum (była tu sekta) -> 8. Czyli bandy: 6 \| 8 \| 10.
Paulina: -3 (Przodek), -1 (Herman, ukrywanie itp) -> -4. +2 magia -> -2. Baza 10 -> 8.
Paulina wspiera się jeszcze Marią, by ta pomogła znaleźć rzeczy świadczące o tym, że coś się działo; wzmocnić przekaz: +1 -> 9.

Paulina stwierdziła, że warto będzie dodatkowo skłonić Grażynę by myślała o relikwii. Porozmawiała z nią o relikwii, sekcie, dobrze że ich nie ma... ogólnie, naprowadzając Grażynę na potencjalne problemy co by było gdyby coś zostało. I że przecież nie wszyscy zostali złapani. Gorzej, szef uciekł. Maria zza fasady Pauliny dowodziła tą rozmową; a ona jest niezła w manipulowaniu ludźmi (11). Grażyna jest przestraszona a okoliczności swoje robią (6+4-3->7). Maria wykonała uderzenie dające Paulinie +3 tool bonus.

Paulina z ciężkim sercem rzuciła zaklęcie. Nie lubi czarować, zwłaszcza w tak zimny sposób na w gruncie rzeczy porządną kobietę. (11-2->9). \|6\|8 (9)\|10\|. Udało się; Grażyna Tuloz tego jeszcze dnia pójdzie znaleźć swoją relikwię. Upewnić się, że wszystko jest ok. Niestety, z uwagi na moc Przodka, Paulina musiała nacisnąć mocno, ZA mocno według tego co chciała. Grażyna będzie miała do końca tygodnia koszmary na linii relikwia-sekta-itp.
/TEST

Poza tym, Paulina miała normalny, produktywny dzień pracy. Poza tym, że dwóch smutnych policjantów pilnuje szpitala i Paulina usłyszała ich mruczenie. Okazało się, że centrala przysłała policjantów z zewnątrz do znalezienia sekty (czytaj: osobę, która skrzywdziła syna starosty), bo starosta zwyczajnie nie ufa, że lokalna policja nie jest w związku i sądzi, że są niekompetentni (starosta zwalił na Alicję Gąszcz współpracująca z przestępcami). Paulina się do policjantów uśmiechnęła i poszła dalej.

Plan Pauliny był prosty jak konstrukcja kija. Poczekać, aż gangster pogada z dziennikarzem, dopaść go, użyć magii i wstrzyknąć mu jakieś świństwo (jak kiedyś zrobiła Gala). Paulina uczy się od najlepszych. Poszła więc kampić pod apartamentem gdzie mieszka dziennikarz...
Przebrała się, książka, i idzie "sobie czytać" by zmylić ochronę. Bez problemu, ochrona nie zwróciła na nią uwagi. Żadnej. Przyszedł gangster do dziennikarza i poszli na spacer. Ale są w polu widzenia Pauliny.
Nie tylko oni. Paulina dojrzała zmierzającego w tamtą stronę pewnym krokiem Krystiana Korzunio. Przecięła mu drogę i zagadała.
Korzunio powiedział, że "coś" działa w sposób niepożądany i on tu się pojawił, by wyczyścić problem. Powiedział Paulinie, że jego interesuje Perkas jako informator. Dziennikarz zrobił coś złego, ale pod przymusem. On jest skłonny pomóc Perkasowi - wyczyścić to, co się stało - ale nie za darmo. Perkas nie musi nic robić. Ale Korzunio chce mieć oczy. Kogoś, kto zgłosi rzeczy nienaturalne, rzeczy, które nie nadają się do prasy. Paulinie (chroniącej teren) nie ufa. Jest czarodziejką ORAZ nie jest miejscowa.

Paulina mu ustąpiła. Powiedziała, że do rozmowy kiedyś wrócą, ale niech na razie Korzunio zajmie się "swoim człowiekiem". I faktycznie, Korzunio podszedł do Oranka i Perkasa i się pokazał. Powiedział Orankowi, by ten lepiej uciekał; potem powiedział Perkasowi, że Orank wróci. Ale Perkas wygląda na człowieka potrzebującego pomocy od firmy Korzunia. Podał mu wizytówkę - "Rozwiązujemy problemy. Zapewniamy dyskrecję. Pierwszy telefon gratis". Korzunio spojrzał na Perkasa i powiedział, że TAKIE problemy też rozwiązuje. 
Perkas powiedział Korzuniowi, że "oni mają ludzi wszędzie, wysłali mi hasło mailem". Korzunio powiedział, żeby Perkas się nie przejmował. Jeśli Perkas skorzysta z usług Korzunia, to on się wszystkim zajmie. WSZYSTKIM. Pożegnał się i poszedł.

Maria dała znać Paulinie o lokalizacji relikwii. Relikwia jest gdzieś w domu Grażyny. 

Paulina i Korzunio się spotkali na rozmowę w "Czarnym Dworze".
Paulina powiedziała, że ona chce, by Łukasz Perkas stał się "shared resource" - niech informuje zarówno Korzunia jak i ją.
Korzunio chce wiedzieć, po co to Paulinie. Zwłaszcza, że cały koszt operacji płaci on.
Jaki koszt? Relokacja rodziny i jednej zgwałconej dziewczynki, potem korekta pamięci by żyli normalnie. Perkas za to zapłaci informacjami.
Zdziwiona Paulina - on FAKTYCZNIE coś robi by pomagać ludziom. Po swojemu.

ESCALATION_MODE:

Korzunio: chce wysłać Paulinę do domu, najlepiej, by nie musieli nic robić razem, działają osobno. Zachowuje pełną niezależność, Paulina nie przeszkadza w jego planach.
Paulina: chce współpracować z Korzuniem, jeśli ona wejdzie na jego teren to zagada i na odwrót. Niezobowiązująco. Jakoś się można zawsze dogadać.

Czyli Paulina próbuje wypracować układ z Korzuniem. 
Korzunio stwierdził, że nie zagraża Paulinie; raczej ona zagraża jemu.
Paulina powiedziała, że cele mają raczej wspólne; różnią się środkami a środki da się wypracować.
High levelowo Korzunio powiedział plan: zdobyć oczy, uszkodzić sektę, zablokowanie jej odrodzenia, usunąć ślady po Zajcewach. Nic więcej.
Pauliny celem jest zapewnienie spokoju i dobrobytu okolicy. Między innymi niwelowanie szkód sprawianych przez magów ludziom.
Oboje zauważyli, że TYM RAZEM udało im się pogodzić ich interesy. I z wiłą, i z sektą.
Paulina powiedziała, że jej nie przeszkadza to, by Korzunio dokończył tą operację pod warunkiem, że zakres tej operacji to "oczy, sekta, Zajcewowie" - to co już zaczął.
Korzunio powiedział, że na jego stan wiedzy nie ma tu nic magicznego i jego nic magicznego w tej operacji nie interesuje. Działa na ludziach, nie przedmiotach.
Czyli Paulina wie, że jest relikwia bezpieczna. Nie ma żadnego problemu, nic złego się z nią nie stanie.
Korzunio zaproponował Paulinie zamiast Perkasa jakiegoś mięśnia; ale Paulina nie chce, by Korzunio wiedział o jej działaniach. Odmówiła.
Uzgodnili, że jeśli ich ścieżki się przetną, zaczną od rozmowy a nie walki ze sobą.
Paulina poprosiła o wyjaśnienie sprawy z "achtungiem"; Korzunio powiedział o synach starosty którym wolno wszystko i, że była dziewczyna która chciała zapłacić cenę. Więc on rozwiązał ten problem. A ona nie musiała płacić tak wysokiej ceny... a Paulina westchnęła. Nie zgadza się z metodami. Ale ona poskładała młodego...

/ESCALATION_MODE

## Postmission:
- Korzunio wyjechał, pozyskawszy Perkasa i Alicję jako swoich agentów.
- Paulina sprawdziła relikwię w domu Grażyny Tuloz; to tylko niemagiczny, acz historyczny przedmiot.
- Policja aresztowała wielu młodych ludzi; nielokalna policja wezwana przez starostę Brzeszczota wolała aresztować niewinnych (ale szybko Bławatek i Kurczak rozwiązywali sprawę)
- Alicja Gąszcz została przywrócona do służby w policji.

## Dark Future:

### Faza 1: Pająk rozpościera swoje sieci...

- Korzunio okalecza Filipa Achtungiem. Filip trafia do szpitala Pauliny.
..... Collector.trade [something] for [something else]

- Korzunio informuje Mirka o tym co zrobił dla jego siostry. Teraz on musi coś dla niego zrobić...
..... Collector.trade [something] for [something else]

- Mirek wysyła hasło Perkasowi. "Nas jest więcej".

- Korzunio pokazuje pośrednio (przez Macieja Oranka) Perkasowi, że ma nagranie.
..... Cleaner.blackmail / bribe [someone]


### Faza 2: Dwie muchy wpadają w sieci...

- Do akcji polowania na kult wchodzi policjant Nikodem Dęciak ze swoją grupą. Inną grupę wysłał Przaśnik.

- Korzunio "walczy" z Orankiem na oczach Perkasa; manipuluje Perkasem w prośbę o pomoc
..... Collector.manipulate [someone] to do something which causes them to require Collector's help

- Maciej Orank wysyła Alicji Gąszcz plik pokazujący gwałt na Kindze
..... Collector.manipulate [someone] to do something dangerous or reckless opening Collector a path

- Konfrontacja Mirek / Alicja; policjantka jest przekonana, że to on załatwił Filipa. 

- Perkas prosi o pomoc Korzunia.


### Faza 3: Jedna mucha oplątana.

- Alicja na własną rękę próbuje dojść do tego, co się stało; Korzunio podkłada dowody na Mirka.
- Mirek chroni siostrę i jej działania; chce wziąć winę na siebie jeśli musi.
- Alicja usuwa dowody świadczące o roli Mirka; działa przeciw Kurczakowi.
- Korzunio "zabija" Oranka. Dowód dla Perkasa - kamera.
- Perkas jest w rękach Korzunia i pod jego ochroną.

### Faza 4: Druga mucha oplątana.
 
- Korzunio spotyka się z Alicją i pokazuje dowody.
- Alicja odmawia współpracy z Korzuniem. Ten pokazuje, że może dać dowody posłowi Brzeszczotowi. Alicja się łamie...
- Alicja jest w rękach Korzunia i wraca do policji.

# Zasługi

* mag: Paulina Tarczyńska, która pozyskała Korzunia jako alianta i definitywnie zamknęła temat relikwii i kultu w Przodku.
* czł: Krystian Korzunio, wielki zwycięzca, który pozyskał Alicję Gąszcz i Łukasza Perkasa jako agentów. Pomścił, wyczyścił, wygrał i się wycofał. Zależy mu na dobrobycie ludzi.
* czł: Alicja Gąszcz, która próbowała ratować dwójkę młodych ludzi; wmanipulowana przez Korzunia stała się jego agentką mimo woli. Wróciła do służby w policji.
* czł: Łukasz Perkas, który wmanipulowany przez Korzunia stał się jego agentem. Korzunio go uratował od oskarżeń ze strony kultu / nieletniej.
* czł: Kinga Kujec, siostra Mirka, dziewczyna z patologicznego domu skrzywdzona przez synów starosty; zawarła "pakt z diabłem" (Korzuniem) by ją pomścić.
* czł: Mirek Kujec, brat Kingi, którego Korzunio wykorzystał by znaleźć słaby punkt Alicji Gąszcz.
* czł: Filip Brzeszczot, brat Ernesta, Korzunio doprowadził do tego, by petarda "achtung" go naprawdę skrzywdziła (przy okazji zwalając winę na kult). Paulina go naprawiła ;-). Gwałciciel. "Złota młodzież".
* czł: Ernest Brzeszczot, brat Filipa; nieistotny na tej misji. Też winny gwałtu na Kindze. "Złota młodzież".
* czł: Leopold Brzeszczot, starosta, wierzy, że jego dzieci to aniołki i wezwał policję z zewnątrz by zniszczyła kult który okaleczył jego syna.
* czł: Grażyna Tuloz, która przez magię Pauliny będzie miała koszmary senne; powierniczka relikwii o stalowej woli, która się staroście nie kłania.
* czł: Maciej Orank, narzędzie w rękach Korzunia do zwerbowania Łukasza Perkasa. Z powodzeniem.

# Lokalizacje:

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Leere
                1. Przodek
                    1. Centrum
                        1. Szpital Gotycki
                        1. Klub Czarny Dwór
                    1. Równia Słoneczna
                        1. Apartamentowiec Słoneczko
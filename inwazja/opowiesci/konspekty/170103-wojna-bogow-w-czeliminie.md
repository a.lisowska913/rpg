---
layout: inwazja-konspekt
title:  "Wojna Bogów w Czeliminie"
campaign: powrot-karradraela
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [160825 - Plany Overminda (HB, KB, AB, DK)](160825-plany-overminda.html)

### Chronologiczna

* [170101 - Patrol? Kralotyczne maskowanie (AW)](170101-patrol-kralotyczne-maskowanie.html)

## Kontekst ogólny sytuacji
## Punkt zerowy:
## Misja właściwa:

Dzień 1:

Rudolf Jankowski wreszcie doszedł do siebie. Kilka godzin Andrea robiła mu briefing i korelowała informacje. Szczęśliwie, Jankowski jest tym samym wrednym, cynicznym i niebezpiecznym typem którym zawsze był. Teraz ma jeszcze skwaszony zły humor. Też się nie zmieniło.

* "Nie wiem jak wiele zasad JESZCZE można było złamać... Wydział Wewnętrzny plus Lady Terminus plus ten poziom autonomii? Bez kontroli?" - skwaszony Jankowski
* "Tak wiem. Spodziewam się długiego i ciekawego procesu. Będzie opisywany w podręcznikach prawników. Ale pocieszam się, że nie będę sama na ławce oskarżonych" - rozbawiona Andrea
* "Jeśli cokolwiek w Świecy jeszcze działa - COKOLWIEK - to Twój proces dojdzie do skutku dopiero po Twojej śmierci przez biurokrację" - Jankowski
* "Może być. Nie będą wiedzieli od czego zacząć i się poddadzą?" - Andrea, coraz bardziej rozbawiona

Jankowski zauważył, że szkoda mu trochę Bankierza. Konkretny i kompetentny terminus. Andrea się zgodziła, ale to jedyny jaki jej został i się nadaje - a to musi być ktoś, kto CHCE. Jankowski się zaśmiał wrednie. Powiedział, że Joachim Kopiec się zgłosi na ochotnika.

* "Andreo, Kopiec jest małoletni, prawda? 17 lat." - Jankowski
* "Jego ojciec sympatyzuje ze Szlachtą. Więcej, JEST członkiem Szlachty. Czeka go proces" - Jankowski
* "Kogo nie czeka?" - Andrea - "Prawnicy w Świecy mają zapewnione posady przez najbliższe ćwierćwiecze"
* "Posłuchaj tego - młody zgłasza się na ochotnika. Staje się wektorem Arazille. Za to odpuszczamy wszystko jego ojcu i młody umiera jako bohater" - Jankowski, tłumacząc jak ratować Mieszka
* "Mi to pasuje..." - Andrea, po zastanowieniu
* "Ja zajmę się pracą nad młodym Kopcem." - Jankowski - "Zdejmę to z Ciebie. A nieletniego łatwo wmanipulować."
* "Mhm, dobra" - Andrea, przechodząc do porządku dziennego.

Andrea dała Jankowskiemu jeszcze jedno zadanie. Po uruchomieniu Ogniwa Arazille, gdy Karradrael będzie zajęty i zaskoczony, trzeba znaleźć i zabezpieczyć Ernesta Mausa. Seirasa Mausa. Ma być bezpiecznie przeprowadzony tutaj - w wersji idealnej, dobrowolnie. Jankowski tylko spytał czy mają pozytywne kontakty z Amandą Diakon? Tak. Oki, on się zajmie kontaktem z Amandą i wraz z oddziałem Zajcewkommando go tu przeprowadzi kanałami Millennium.

* "Musi być w stanie go przywołać" - Andrea, z naciskiem do Jankowskiego
* "Ale on nie będzie w stanie" - nie rozumiejący Jankowski
* "Jeszcze nie. Jeszcze nad tym pracuję..." - Andrea, wyjaśniając
* "Nie, nie tak. On został wybrany jako seiras, bo on NIE jest w stanie przyzwać Karradraela." - wyjaśniający Jankowski
* "Nie?" - zaskoczona Andrea
* "Nie. On dba o ród w kontekście Świecy, nie 'dba o Ród'". - Jankowski, doprecyzowując
* "To musimy znaleźć lepszego kandydata..." - podłamana Andrea, której plan się nieco... popsuł - "Jakieś pomysły?"
* "Żadnego, którego można sensownie kontrolować... jeśli da się go kontrolować, nie nadaje się na przyzwanie." - Jankowski, ze westchnięciem
* "Ale mi nie chodzi o kontrolę..." - Andrea - "Chcę ubić z nim interes taki, by zdawał sobie sprawę z sytuacji politycznej i by nowy Lord Maus uznawał Świecę za korzystną"
* "Będzie trzeba znaleźć takiego. I tak, rozerwanie wiązania Renata - Karradrael pomoże nawet bez nowego seirasa..." - Jankowski, myśląc ciężko

Po dłuższym przeszukiwaniu opcji i znanych im Mausów, stanęło na potencjalnym nowym seirasie: Zuzannie Maus.

* Jest proaktywna i nie jest niechętna Świecy; jest trochę na uboczu, ale nie jest głupia i uczy się z historii i umie słuchać rad
* Jest archiwistką o silnej woli i Czystej Krwi; może to zrobić (ma odpowiednie Ścieżki)
* W jej sercu jej ród jest dla niej najważniejszy
* ...nie mają lepszego pomysłu kto.

Pierwsze co trzeba zrobić - usunąć Renatę. To powinno odebrać motywację Karradraelowi. Andrea skontaktowała się z Agrestem i spytała go o motywację Blakenbauerów we wspieraniu Szlachty. Agrest był w stanie wyjaśnić dzięki kontaktom w Kopalinie i swoim agentom:

* Blakenbauerowie próbują nie antagonizować nikogo
* Blakenbauerowie boją się Szlachty i ich siły ognia
* Blakenbauerowie pomogą Andrei jeśli będą odpowiednio zmotywowani (Agrest ich zmotywuje jak będzie trzeba)

Andrea dała Jankowskiemu trzy zadania do wykonania, w następującej kolejności:

* Arazille. Ma być przyzwana. Bonus, Mieszko przeżywa.
* Operacja "Renata":
 * Najpierw zmotywowani Blakenbauerowie pomogą pająkiem fazowym Jankowskiemu
 * Renata ma wylądować u Blakenbauerów na przerobienie
 * Na pewno Rezydencja będzie zaatakowana w tej sytuacji, wszystkimi siłami Karradraela; tu trzeba wsparcia Zajcewów itp.
 * Transformacja zajmuje 24-48h. Ale wystarczy niepełna transformacja; powiedzmy, 12-24h dla bezpieczeństwa. 
 * Pozostaje problem Myślina, ale ze zdjętą Renatą Myślin jest mniej groźny.
* Następnie: Zuzanna lub wskazany przez nią Maus. Ale Zuzanna jest preferowana.

Andrea dodała do Jankowskiego, że chce odciąć WSZYSTKICH Mausów od hipernetu. Dla szoku i wprowadzenia chaosu komunikacyjnego. Andrea i Jankowski będą mieć uprawnienia do dopuszczania Mausów do komunikacji. Przy odrobinie szczęścia Andrei uda się utrzymać to miejsce i jakoś wykorzystać Biankę...
Andrea powiedziała mu jeszcze o "Lordzie Jonatanie" - potężnym aderialicie znajdującym się pod Myślinem. Jankowski prawie usiadł z wrażenia.

Oki. Czyli plan mniej więcej znają...

Jankowski powiedział, że będzie musiał popracować nad Joachimem Kopcem. Zajmie mu to do końca dnia. Oddalił się do kwater młodego ucznia na terminusa wybranego jako przymusowy ochotnik, tylko jeszcze rzucił Andrei, by ta porozmawiała z Bianką. Może jest inna opcja insercji Arazille do Czelimina niż eskorta manualna.

"To mogła być Anna. Ma szczęście - jest uzależniona od kralotha ;-)" - Kić, 2017

Andrea spotkała się z Bianką Stein, Świecowym ekspertem od portali. 

Bianka się ucieszyła widząc Andreę. Jest jeszcze w bardzo delikatnej kondycji psychicznej; to, co zrobiły jej Siluria i Elea bardzo ją skrzywdziły... Andrea wie, że Siluria i Elea ją strasznie skrzywdziła i że Bianka się ledwo trzyma. Lidia zrobiła co mogła, ale miała za mało czasu i za dużo pracy. Bianka bardzo dziękuje za ratunek i jest nieskończenie wdzięczna Andrei za wszystko, co ta dla niej zrobiła. Andrea (która nic nie zrobiła) przyjmuje podziękowania - wszystko za chwałę misji.

Andrea powiedziała Biance, że w normalnych okolicznościach nie zlecałaby jej zadania, ale teraz musi. Andrea potrzebuje portal, by uderzyć tego, co kazał Biance zrobić to co jej zrobione. I uderzy tam, gdzie boli. Andrea próbuje Biankę motywować; bez większych sukcesów - ona jest zbyt rozkojarzona, zbyt... w świecie swoich koszmarów. Bianka da radę - jeśli na stałe będzie mieć kogoś kto ją będzie motywował. Zdecydowała oddelegować Barana, świetnie widząc ironię sytuacji - "największy leń" motywuje najlepszą katalistkę.

Andrea wezwała Tadeusza Barana. Ma za zadanie pomóc Biance Stein - Andrea chce użyć stary portal na EAM, zniszczony przez er, pilota ludzkiego, by móc przeteleportować kogoś do Czelimina. Bianka ma to zrobić, Tadeusz ma jej pomóc. Priorytet: asap, ma być gotowe na rano. Bianka powiedziała, że potrzebuje odczytów z portalu. Baran, sam katalista, zsynchronizował się auratycznie z Bianką i będzie jej 'remote odczyt'. Andrea załatwi z Kręgiem Życia logistykę. Przekazała temat Lilakowi.

Krąg Życia dał radę po raz kolejny uniknąć patroli Czelimina. Bianka dostała swoje odczyty i wraz z Baranem zabrali się do pracy.

Andrea wezwała do siebie Melodię, chcąc porozmawiać z nią o stanie Bianki Stein.

* "Powiedz mi, co myślisz o Baranie i Stein?" - Andrea, poważnie
* "Ładna z nich para." - Melodia, poważnie

Melodia przedstawiła Andrei swój plan: Tadeusz Baran nie ma motywacji w życiu. Jest uznany za lesera i ofiarę losu. Żyje tylko dzięki Rafaelowi (z tym się nawet Andrea zgodziła...). Bianka jest w rozsypce. Jej świat stanął do góry nogami i jest samotna. Nie ma czego się złapać.

* "Mówisz... może dwóch uszkodzonych magów wyleczy się wzajemnie?" - Andrea, żartując
* "I Baran i Stein byli w skrzydle medycznym. To znaczy, że mam składniki na eliksir, który ich lepiej splecie ze sobą. Ostatni dar Rafaela" - Melodia z przekąsem
* "Dla odmiany może się naprawdę przydać..." - Andrea, ciężko wzdychając

Melodia wyjaśniła, że ona i Baran nie pasują do siebie na dłuższą metę. Więc Rafael zostawił jej narzędzia, by stworzyć przepięcie; więc ona może przepiąć Barana na Stein i Stein na Barana. A z Bianką w depresji, ona sama nie da rady znaleźć eliksiru na sobie czy na Baranie; uznają, że to część leków które muszą brać. A po pewnym czasie eliksir po prostu nie będzie potrzebny. To nie jest "puf, i się przepięło"; to chwilę potrwa. Po tygodniu będą parą.

* "Dla Andrei jest wygodne, że Baran jest w niej zadurzony..." - Kić, 2017
* "Ale miłość Barana musi teraz ratować kogoś innego!" - Żółw, też 2017

Andrea wyraziła zgodę. Melodia ma się tym zająć.

Późnym popołudniem przybył Kirył Sjeld z Anastazją Sjeld. Krąg Życia przetransportował ich do bazy Andrei. Wiedzą, że jak raz wejdą do bazy, już nie wyjdą - do momentu, aż Andrea pozwoli im ją opuścić. Kirył był tym, który mówi z Andreą - jest przyjazny i chce pomóc. Andrea powiedziała, że od Kiryła będzie oczekiwała dużej siły ognia. Kirył dał Andrei jako oznakę dobrej woli kilka artefaktów bojowych. Porcja ciężkich granatów bojowych. Wspomagane taumatycznie; bardzo ciężka sprawa. Co prawda wszystko zakazane i nielegalne, ale...

Jankowski się "ucieszył". Granaty entropiczne o podwójnym wspomaganiu katalitycznym opracowane do destrukcji zon syberyjskich... super. Te granaty entropiczne są w stanie nawet uszkodzić, jak nie zniszczyć Śledzika.

Andrea powiedziała Kiryłowi, że potrzebuje Śledzika pod kontrolą jak najszybciej. Anastazja powiedziała, że potrzebują koło dnia na przejęcie kontroli nad Śledzikiem. A wpierw przydałby się dzień na przygotowanie... w czym tak czy inaczej, jest możliwość przesunięcia gdzieś Śledzika używając elementów dziadka ;-).

Dzień 2:

Dzień wstał piękny i słoneczny. Andrea oddycha pełną piersią. Wszystko zaczęło działać.

* Joachim Kopiec zgłosi się na ochotnika do bycia nosicielem Ogniwa Arazille i by stać się awatarem Arazille.
* Bianka powiedziała, że jest w stanie postawić ten portal; potrzebuje godziny na miejscu (przydałaby się dywersja). Do osłony dostaje Barana i Mieszka.
* Melodia podała im eliksir, czego nie byli świadomi.

I, żeby zepsuć dzień Andrei, przyszła do niej Dalia.

* Lady terminus, jest coś nie w porządku z Bianką Stein. Wpływa na nią coś, czego nie rozumiem - Dalia, wykrywszy eliksir Rafaela
* Ach no tak... przepraszam, moja wina. Tak, wiem o tym, nic złego się nie dzieje. - Andrea, zaczynając od strony _devious_

Andrea przekonała Dalię, by ta nie analizowała tego dokładniej i pochwaliła ją za bycie czujną. Zadowolona Dalia wróciła do swoich obowiązków. Andrea facepalmowała. Melodia nie pomyślała... więc Andrea poprosiła ją do siebie. Gdy Andrea wskazała Melodii sytuację, ta TEŻ facepalmowała. Fakt, Dalia jest lekarzem a Bianka jest jej podopieczną... teraz Melodii zrobiło się nieco głupio.

Trzeba przetransportować do portalu: Joachima Kopca, Biankę Stein, Mieszka Bankierza i Tadeusza Barana. Jankowski ma się poruszać sam z Ogniwem. Ma POTEM się pojawić, na moment; chcemy ściągać ogień na Jankowskiego (terminus, Wydział Wewnętrzny) a nie na zespół cywili... plus Andrea poprosiła Kiryła i Anastazję o przesunięcie Śledzika pod Czelimin. By zwrócił uwagę Karradraela. Krąg Życia zapewnia im możliwość ucieczki itp.

Udało im się. Kirył i Anastazja ściągnęli na siebie ogień Czelimina; gdy było naprawdę, naprawdę źle to Śledzik obrócił się przeciwko atakującym i podążył za nimi do Czelimina. To sprawia, że Czelimin znowu walczy przeciwko Śledzikowi a nie szuka tak ostro co tam się dzieje dalej. Z trudem, ale Krąg Życia wycofał Kiryła i Anastazję.

Tymczasem drugi oddział przekradający się... miał pecha. Siły Karradraela ich znalazły; Mieszko i Baran je odparli z Kręgiem Życia, ale Karradrael dowiedział się, że Mieszko żyje i ma się dobrze. Nie zdąży z tym wiele zrobić. Co gorsza, oddział Karradraela dał radę wszystkich ostro poranić. Ale udało im się otworzyć ten portal...

Rudolf, ekspert Wydziału, dał radę unikać sił Karradraela i Spustoszenia by przynieść Ogniwo Kopcowi.

Młody kandydat na terminusa ze łzami w oczach przeszedł przez Portal.

W Czeliminie zamifestowała sie Arazille, osobiście. Baran natychmiast zamknął portal - to jego Wielki Dzień. Udało mu się wszystkich osłonić, acz został porażony magią Wojny Bogów. Tak bardzo bohater! Bianka na niego inaczej spojrzała.

Arazille Uwolniła Śledzika, który jest w "jej jurysdykcji". Poprosiła go o usunięcie Krwawych Komponentów Karradraela. Śledzik z radością pojechał niszczyć dla Arazille.

Zespół dał radę wycofać się do bazy i zabunkrować się, w czasie, gdy na zewnątrz w Czeliminie rozpoczęła się Wojna Bogów...

Aha, Andrea odcięła wszystkich Mausów od hipernetu. Niech obciążenie komunikacyjne idzie po Karradraelu. Im trudniej, tym lepiej...

# Progresja

* Bianka Stein: lekka fobia przed Diakonami (-1 SCA, SCD, SCF w kontaktach z Diakonami)
* Bianka Stein: lekka fobia przed Mausami (-1 SCA, SCD, SCF w kontaktach z Diakonami)
* Andrea Wilgacz: w prezencie od Kiryła jedna porcja granatów entropicznych (zakazanych, zdecydowanie przepakowanych)
* Tadeusz Baran: pozytywny wpis do akt od Andrei za uratowanie oddziału przed Portalem Energii Bogów
* Bianka Stein: widzi bohaterstwo Tadeusza Barana w niesamowicie pozytywnym Świetle
* Bianka Stein: eliksir Rafaela Diakona, który ją zachęca do Tadeusza Barana
* Tadeusz Baran: eliksir Rafaela Diakona, który go zachęca do Bianki Stein
* Wojmił Kopiec: wszystkie ewentualne problemy i zarzuty zostały z niego zdjęte
* Rudolf Jankowski: wreszcie pełnosprawny, pełnosprytny i cholernie niebezpieczny

# Streszczenie

Jankowski doszedł do siebie i od razu zaczął się przydawać. Andrea dowiedziała się, że Ernest Maus nie nadaje się jako nowy seiras kontrolujący Karradraela - ale Zuzanna (którą Andrea zna) tak. Agrest i Jankowski mają zapewnić współpracę Blakenbauerów. Nie Mieszko a Joachim Kopiec przyzwał Arazille do Czelimina (ginąc) zaczynając wojnę bogów. Bianka jest w złej formie psychicznej; Tadeusz Baran (z pomocą Melodii) się nią opiekuje i ta dwójka zaczyna mieć się ku sobie. Arazille, przyzwana w Czeliminie uwolniła Śledzika od kontroli, o czym nikt nie wie.

# Zasługi

* mag: Andrea Wilgacz, doglądająca realizacji swoich planów, hamująca zapędy Dalii, wspierająca oddział i oddająca autonomię Jankowskiemu. Gra w nową seiras Maus i w kontratak na Karradraelu. Jej czas nadszedł.
* mag: Rudolf Jankowski, wreszcie pełnosprawny i bezwzględnie niebezpieczny. Przekonał Joachima by ów się poświęcił zamiast Mieszka. Silnie autonomiczny, wspiera Andreę. Przenosił Ogniwo Arazille dla Joachima.
* mag: Marian Agrest, dostarczył Andrei informacje o Blakenbauerach i motywuje Blakenbauerów do współpracy.
* mag: Ernest Maus, okazało się, że nie nadaje się na seirasa kontrolującego Karradraela. Efektywnie dla planów Andrei bezużyteczny.
* mag: Zuzanna Maus, potencjalna seiras Maus (może kontrolować Karradraela). Andrea gra, by ona została Lady Maus.
* mag: Joachim Kopiec, który został przekonany przez Jankowskiego; płacząc, poświęcił się wzywając Arazille by jego ojciec nie miał problemów. KIA.
* mag: Bianka Stein, zniszczone morale, ciężko psychicznie uszkodzona, ale działa dla Andrei. Otworzyła (praktycznie niemożliwy) portal do Czelimina i zaczyna podkochiwać się w Tadeuszu Baranie.
* mag: Tadeusz Baran, po* maga Biance z portalem, osłonił wszystkich przed Arazille i wojną bogów, choć sam został ciężko ranny. Zaczyna podkochiwać się w Biance Stein.
* vic: Świeży Lilak, który w imieniu Kręgu Życia koordynował logistykę na terenach opanowanych przez siły Karradraela
* mag: Melodia Diakon, zaproponowała Andrei parę: Bianka x Tadeusz. Niestety, gorzej z wykonaniem; Dalia coś zwęszyła. Ogólnie, uda się.
* mag: Kirył Sjeld, przybył i dał Andrei w prezencie cholernie nielegalne i niebezpieczne granaty entropiczne. Myśli, że będzie w stanie kontrolować Śledzika.
* mag: Anastazja Sjeld, szamanka i herbalistka, żona Kiryła, przybyła z Kiryłem do bazy Andrei (w nadziei, że to pomoże znaleźć Karinę).
* mag: Dalia Weiner, która znalazła eliksir miłości Rafaela na Biance ale dała się przekonać Andrei, że to nic ważnego. Podbudowane morale.
* vic: Stalowy Śledzik Żarłacz, przenoszony jako dywersja, chroniąc Kiryła pojechał do Czelimina gdzie Arazille uwolniła go z "prymitywnego AI"
* vic: Arazille, która została przyzwana przy użyciu Ogniwa do Czelimina, rozpoczynając wojnę bogów z Karradraelem
* vic: Karradrael, który został zmuszony do osobistej walki z Arazille w Czeliminie przez manifestację Arazille.
* mag: Mieszko Bankierz, ochraniał Zespół przy portalu, nieświadomie uniknął losu "radzieckiego ochotnika" jako awatar Arazille.

# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Wtorek Śląski
                    1. Ośrodek historyczny
                        1. Ruina zamku, skąd Andrea dowodziła i o której nadal nikt nie wie (poza Kręgiem Życia).
                1. Piróg Górny
                    1. Las Stu Kotów
                        1. Martwy Portal, uruchomiony przez Biankę i Barana łączący się z Czeliminem (by przesłać Ogniwo Arazille).
                1. Czelimin, do którego Bianka zrobiła tymczasowy portal z Piroga i gdzie zamanifestowała się Arazille, zaczynając wojnę bogów z Karradraelem

# Czas

* Dni: 2
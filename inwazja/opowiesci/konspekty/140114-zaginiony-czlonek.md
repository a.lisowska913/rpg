---
layout: inwazja-konspekt
title:  "Zaginiony członek"
campaign: druga-inwazja
players: kić, dust
gm: żółw
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [140109 - Uczniowie Moriatha(AW, WZ)](140109-uczniowie-moriatha.html)

### Chronologiczna

* [140109 - Uczniowie Moriatha(AW, WZ)](140109-uczniowie-moriatha.html)

## Misja właściwa:

Do biura Andrei przyszedł szofer. Szofer ów zwał się Jan Szczupak i powiedział jej, że prokurator okręgowy chce się z nią widzieć.
Ok... tego się Andrea nie spodziewała. 

Na stan wiedzy Andrei, prokurator (Hektor Blakenbauer) jest magiem mało interesującym się światem magów, żyje w świecie ludzi. Z uwagi na to, że szofer zdecydowanie jej się nie spodobał (zachowywał się, jakby pozjadał wszystkie rozumy), zdecydowała się na wywarcie na nim wrażenia. Nie było to łatwe, ale jej się udało - szofer zdecydowanie spojrzał na Andreę z innym szacunkiem, na osobę uzdolnioną.

W rezydencji Andrea spotkała się z prokuratorem Hektorem Blakenbauerem. Czarodziej powitał ją ciepło i powiedział, że problemem jest to, że zaginął członek rodziny - pies imieniem Margaret. Cóż... Andrea NIE doceniła. No psa będzie szukać? Naprawdę? Tu Blakenbauer stwierdził, że ten pies ma dla niego specjalne znaczenie (ano ma, Margaret kiedyś była czarodziejką rodu Blakenbauer, ale o tym się nie zająknął). Obiecał solidną płacę, no a detektywowi sympatia i znajomości z prokuratorem okręgowym się zawsze przydadzą...

No dobra... Andrei jednak coś się nie spodobało. Przeszukała dyskretnie gabinet prokuratora (SO VERY NOT AMUSED!) i znalazła... pluskwę. Zwykłą, niemagiczną pluskwę (prokurator - SO SO SO VERY NOT AMUSED). Wzięła prokuratora na spacer do ogrodu... trzeba by mu o tym powiedzieć. No i powiedziała. Prokurator, cóż, nie miał pojęcia skąd ktoś mógł dostać się do jego gabinetu... lub w ogóle do domu Blakenbauerów. Przecież tu są systemy obronne. No i renoma Blakenbauerów... to w końcu dość mroczny ród. Kto mógłby tak zadziałać? I, przede wszystkim, po co?

Borys, zapytany ochroniarz, odpowiedział, że nic na ten temat nie wie. Na to zdecydowała się zareagować Andrea - stwierdziła, że spróbuje dostać się do rezydencji 
Blakenbauerów całkowicie niepostrzeżenie. Tak, by nikt nie zauważył. Ostrzegła o tym Hektora (by nikt jej nie zabił przez brak komunikacji) i spróbowała się dostać do rezydencji niepostrzeżenie. Spytała wpierw Hektora jakie ma systemy i jak rozmieszczone. I faktycznie - przedostała się do wewnętrznego kompleksu niezauważona. Niestety, dostrzegł ją ochroniarz już w kompleksie (Borys) - powalił ją i ściorał o glebę. Sprawiło mu to paskudnie dużo przyjemności.

Hektor zatrzymał Borysa i w kilku słowach go opieprzył za zbędną brutalność. W czasie tego opieprzania lekko upokorzona i nadzwyczaj wściekła Andrea zwinęła mu telefon komórkowy - może znajdzie coś odpowiednio kompromitującego. I faktycznie znalazła - zdjęcie nagiej młodej kobiety, tagowane na noc zniknięcia Margaret z rezydencji Blakenbauerów. Zdjęcie było zrobione w samej rezydencji... czyli ktoś obcy tu był...
A przy okazji udowodniła, że da się dostać do rezydencji Blakenbauerów.

Cóż... optymistyczny komentarz Hektora "Chyba Borys może sobie szukać nowej posady". Wpierw jednak Hektor zadzwonił do swojego brata, Marcelina Blakenbauera. Okazało się, że ów znajduje się w rezydencji. Spotkali się pięć minut później, w salonie.
Marcelin potwierdził z rozbrajającą szczerością, że w nocy była u niego dziewczyna. Nazywała się "Sophistia", nie ma pojęcia jak naprawdę się nazywa. Została u niego na noc. Ogólnie to nie jest jakaś dziewczyna z ulicy, Marcelin ją dobrze zna; są przyjaciółmi od pewnego już czasu. "Fajna mi przyjaciółka". Zapytany, Marcelin zaprzeczył - Sophistia nie jest magiem, to tylko człowiek.

Ok... na razie Hektor i Andrea nie kłopotali Marcelina tym, co znaleźli na telefonie Borysa. Hektor zdecydował się przejrzeć zapisy z kamer bezpieczeństwa (no przecież musiały ją widzieć). Okazało się, że faktycznie, Sophistia jest na tych kamerach całkiem nieźle zarejestrowana. Najpierw, gdy wchodzi do pokoju Marcelina z samym Marcelinem. Potem, jak z niego się wykrada, ubrana jedynie w płaszczyk. Potem, jak rozmawia z Borysem i pokazuje mu swoje wdzięki, potem jak się z nim oddala na bok (ma artefakt, szminkę, za pomocą której może go skutecznie przekonać by z nią poszedł :P) a następnie jak wpierw idzie do pokoju z Margaret z artefaktem i wraca już bez niego, po czym idzie do gabinetu Hektora. Czyli chyba ona podłożyła mu tą pluskwę... na końcu wraca do Marcelina jakby nigdy nic.
Niezłe ziółko. Tak bardzo przyjaciółka.

To, co ciekawe - na zdjęciu Borysa szminka ma charakterystyczną aureolę, jak po odpaleniu artefaktu na wysokim poziomie skażenia. To by znaczyło, że faktycznie jest to artefakt. Lub, że aparat Borysa przedawkował efekt "lens flare". Tak czy inaczej, biedny Borys może nie być do końca winny swej słabości :P.

Dobrze więc... Andrea zdecydowała się przeanalizować jeszcze miejsce zniknięcia Margaret. Po rzuceniu grupy dość skomplikowanych zaklęć korzystając z sympatii doszła do wniosku, że został tu otwarty portal prowadzący na ulicę. Czyli faktycznie, Sophistia dała radę wyteleportować Margaret używając jakiejś wersji artefaktu. Szczęśliwie, artefakt ten miał jawną, oficjalną sygnaturę, zostawioną specjalnie dla pomocy terminusom. Ten artefakt pochodził ze sklepu znanego artefaktora SŚ, tien Karola Poczciwca. Znany jest w środowisku jako łatwowierny idealista (bogaty za to :P) i o ogromnym poczuciu praworządności. Zawsze pomaga terminusom i nie zgadza się, by jego artefakty posłużyły złu.

Oki. To jest trop, jaki Andrea może sprawdzić. Sama, bo Blakenbauerowie mają fatalną reputację.

Tymczasem Hektor porozmawiał z Marcelinem dowiedzieć się jak najwięcej o Sophistii i odwieść go od ewentualnych jej poszukiwań. Oczywiście, nie udało się. Jak tylko Marcelin dowiedział się że coś jej grozi, sam zabrał się za poszukiwania...

Andrea udała się do Poczciwca i powiedziała, że nie jest terminusem (bo nie jest), ale detektywem. Ktoś skrzywdził młodą dziewczynę ludzką i uderzył w magów. Poczciwiec stwierdził, że wie o ataku na Blakenbauerów, i pasuje mu to - to podły ród i powinien wyginąć. Andrea postawiła mu się - ludzka dziewczyna (Sophistia) nie powinna cierpieć. Po pojedynku woli Poczciwiec zdradził nazwisko osoby, która kupiła te artefakty. To był Bolesław Bankierz - znany zwadźca i miłośnik pojedynków. Andrei zrzedła mina. To tak bardzo niesprawiedliwe...

No dobra - Andrea i Hektor się spotkali i wymienili informacjami. Bolesław Bankierz... 
Powszechnie wiadomo, gdzie tien Bankierz się zwykle znajduje - to znana świetnie Andrei "Rzeczna Chata".

Udali się do Rzecznej Chaty i na wejściu ukazał się im przerażający widok. Lokalne osobistości dość zaciekawione bądź zmrożone widokiem, jak Marcelin Blakenbauer pyta głośno Bolesława Bankierza, by ten powtórzył. A Bolesław na to "Nie muszę niczego powtarzać tępemu Szmatenbauerowi, rozumiesz?". Prowokuje Marcelina do walki, jakiej ten nie jest w stanie za żadne skarby wygrać...
...Andrea skoczyła do przodu i walnęła Marcelina mocno w głowę. Chciała przerwać potencjalne wyzwanie na pojedynek, ale uderzyła za mocno, trafiła w ucho, bryzgnęła krew a Marcelin się popłakał.
Bolesław ryknął śmiechem i prawie spadł z krzesła. Jakiekolwiek myśli o pojedynkowaniu się z kimś takim po prostu go opuściły. Hektor zabrał swojego szlochającego brata i się szybko i dyskretnie ewakuował, a Andrea porozmawiała cicho z barmanką, Ofelią, i poprosiła ją o dolewanie do pełna Bankierzowi, po czym się do niego dosiadła.

Bolesławowi Bankierzowi się Andrea Wilgacz spodobała. Pociągnięty za język powiedział jej sporo z tego, co chciała wiedzieć:
- został wynajęty przez tajemniczego maga po to, by zdobyć artefakty do pojechania Blakenbauerów; kradzieży ich psa.
- tajemniczy mag nie ma psa. Pies mu zwyczajnie zwiał. Wyskoczył z portalu, ale wykazał się niespodziewanym sprytem. I tyle psa widział tajemniczy mag. Lol.
- czemu w to wszedł? Słyszał o klątwie Blakenbauerów i chciał zmierzyć się z potworem, którym staje się Blakenbauer gdy jego życie jest zagrożony.

Wrócili do rezydencji Blakenbauerów. Późny wieczór. Nie ma nigdzie Margaret, nie ma Sophistii, zwyczajnie nie wiadomo gdzie może się pies znajdować... ale wiedzą, że Margaret uciekła. Nie ma jej tamten przeciwnik... wiedzą, gdzie jest wyjście portalu. Może uda się znaleźć jakoś aktualną lokalizację Margaret?

Andrea rzuciła zaklęcie, mające przyzwać (znaczy, nie summon, niech przybiegnie) Margaret - dostała od zaklęcia odpowiedź, że nie ma takiej możliwości. Zaklęcie dostało się do samej Margaret, ale Margaret nie jest w stanie przyjść, jest uwięziona. Dobre i to, że jest choć taka odpowiedź, ale to znaczy, że Margaret jest stosunkowo blisko.

Hektor poprosił Marcelina o coś, co umie tropić (najlepiej Margaret). Brat dał radę - dał Hektorowi niewielką (wielkość dużego kota) mantikorę o doskonałych umiejętnościach tropiących. Z jej pomocą powinno się udać zlokalizować Margaret, a przynajmniej iść po jej śladach. I faktycznie - mantikora idąc po śladach Margaret doprowadziła ich do innego punktu, gdzie zgubiła trop. Jednak znajdowała się tam kamera przemysłowa, więc Hektor był w stanie zdobyć informacje co i jak. Na nagraniu kamery był człowiek (niestety, jakość podła), który zaciąga psa do samochodu i odjeżdżają.

Zidentyfikowanie i zlokalizowanie samochodu było formalnością.

Hektor, Andrea (i mantikora) zbliżyli się do domostwa, gdzie przetrzymywana miała być Margaret. Przed domem jednak, w miejscu ukrytym przed mieszkańcami rezydencji znajduje się samochód... detektywa Waldemara Zupaczki! No, problem. Cóż... Andrea okleiła samochód naklejkami typu "CHWDP" i "Polska dla Polaków", a Hektor zadzwonił na policję (MG: nie pamiętam co teraz zrobili z tym biednym samochodem, ale było przekonywujące). Poprosił, by się pospieszyli - każda minuta się liczy. No i podjechał patrol policji, zabrali się do zwijania samochodu, wyleciał Zupaczka jak oparzony i ci go zwinęli. TAK BADZO ZUPACZKA!

Oki. Mantikora, Andrea i Hektor. Kto idzie pierwszy? Oczywiście, do rozbrojenia magicznych pułapek wpierw wrzucili do domostwa mantikorę. No i rozległ się wrzask ŁOJEZU SZATAN! Okazało się, że... wrzucili mantikorę do domostwa stricte ludzkiego. TAK BARDZO MASKARADA!
Nnnno dobra... trzeba to cholerstwo jakoś złapać. Hektor nie chce się pokazywać ludziom (PROKURATOR REPUTACJA WOW), więc wpada Andrea, łapie mantikorę w koc, ale niestety ta (przerażona bardziej niż ludzie) dała radę dziabnąć ją ogonem jadowym. Andrea, nieprzytomna, osuwa się na ziemię spadając na mantikorę w kocu. Hektor zmienia wygląd twarzy i wpada do środka a tam... Paweł Grzęda. Sądowy pieniacz, człowiek składający milion donosów na sąsiadów, osobiście nienawidzi prokuratury a Hektora już w ogóle.

...co on ma z tym wspólnego?
No i do tego z innego pokoju słychać "POLICJA JUŻ JEDZIE PO SZATANA!"

Dobra. Jedzie policja, nie widać Margaret (choć bystre oko prokuratury dostrzegło psią miskę i ani jednego psa)... trzeba się zmyć. Hektor "Siłacz" Blakenbauer bierze na jedno ramię koc z mantikorą, na drugie Andreę i próbuje odpełznąć by schować się przed policją. Niestety, nie udaje mu się, więc gdy policja (tak, ten sam patrol co z Zupaczką; w samochodzie jeszcze siedzi zrezygnowany Zupaczka) już dociera do Hektora, ten używa magii mentalnej by wszystko naprostować. Zupaczka czuje emanację, ale nie reaguje. Tu jest bezpieczniej.

Dobrze. Po pozbyciu się policji metodami magicznymi nadszedł czas na przesłuchanie Pawła Grzędy. Hektor już się nie patyczkuje, jako, że i tak trzeba pamięć mazać to i przesłucha magicznie. Dowiedział się, że Margaret faktycznie tu była - Grzęda ją porwał. A dokładniej - piękny dog biegał sobie samotnie, Grzęda chciał oddać go właścicielowi, więc złapał psa, okazało się, że pies należy do prokuratora więc Grzęda stwierdził, że mu NIE odda. Bo prokurator! Kupił najtańsze psie chrupki i karmił biedną Margaret podłym żarciem.

Czemu psa nie ma i gdzie jest? Cóż, za dużo szczekał, za bardzo był irytujący... więc usunął chip i zawiadomił hycla, że bezpański pies się pałęta po jego obejściu (podwórku) i terroryzuje ludzi. Hycel przyjechał, capnął biedną Margaret i ją - do schroniska.

...zdobycie Margaret ze schroniska było formalnością. Przyjechał Jan Szczupak, powiedział, że to jego, Margaret rozpoznała... koniec tematu.


--

![](140114_ZaginionyCzlonek.jpg)

# Zasługi

* mag: Andrea Wilgacz jako detektyw wynajęta do odnalezienia psa... i znalazła tego psa mimo że trzeba było Zupaczce auto wywieźć na lawecie.
* mag: Hektor Blakenbauer jako prokurator, siłacz, poskromiciel brata i osoba która dowiedziała się więcej o świecie magów niż kiedykolwiek chciała.
* czł: Jan Szczupak jako dumny i pewny siebie szofer maga ze szlachetnego rodu (Hektora Blakenbauera); skromny, bo rozmawia z plebsem.
* czł: Borys Kumin jako ochroniarz Blakenbauerów z radością poniewierający i ciorający Andreą po glebie. A potem się okazało że spał z Sophistią i coś na niego wpływało.
* czł: Amelia Eter jako Sophistia, kochanka Marcelina oraz ku swej rozpaczy Borysa (pod wpływem złych czarów). 
* mag: Marcelin Blakenbauer jako Don Juan który ma dobry gust, prawie wyzywa Bolesława Bankierza na pojedynek i płacze publicznie po tym jak Andrea rozkwasiła mu ucho.
* mag: Karol Poczciwiec jako artefaktor w służbie SŚ. Nieco naiwny ale prawy i nie chce by jego artefakty służyły złu; powiedział Andrei co ta chciała wiedzieć.
* mag: Bolesław Bankierz jako wybitny zwadźca i najlepszy ekspert od pojedynków nie tylko w Kopalinie ale i na Śląsku. Chciał zmierzyć się z drugą formą Blakenbauera.
* mag: Waldemar Zupaczka jako detektyw, który prześcignął Andreę... lecz z jej konspiracji odholowali mu samochód na lawecie (więc stracił zlecenie).
* czł: Paweł Grzęda jako pieniacz nienawidzący Hektora Blakenbauera (bo odrzuca mu pozwy) więc zwinął psa Hektorowi. Czarny charakter sesji!
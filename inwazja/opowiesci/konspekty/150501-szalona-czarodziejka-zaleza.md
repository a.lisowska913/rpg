---
layout: inwazja-konspekt
title:  "Szalona 'czarodziejka' Zależa"
campaign: swiatlo-w-zalezu-lesnym
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [150429 - Terminusi w Zależu (JG, MG)](150429-terminusi-w-zalezu.html)

### Chronologiczna

* [150429 - Terminusi w Zależu (JG, MG)](150429-terminusi-w-zalezu.html)

## Punkt zerowy:

Po zniszczeniu Anety Kosicz przez młodych terminusów i zniszczeniu ryngrafu wydawało się, że problem został w pewnym stopniu opanowany. I faktycznie, Anioł Światła przestał się pojawiać.
Rufus Eter nie otrzymywał wystarczających korzyści ze wspierania Olgi i stwierdził, że nie ma po co dalej jej wspierać. W "niewielkim" stopniu przekonały go pewne problemy powiązane z siłami stojącymi za młodymi terminusami; grunt, że wycofał swoje wsparcie.

Czarodziejka Zależa Leśnego została sama. Z gwałtownie kurczącym się zapasem mocy i energii magicznej. A jak się skończy...

Tymczasem zniszczenie ryngrafu i wyłączenie Anioła Światła spowodowało silny debalans energetyczny. Bez Anioła doszło do przerwania pieczęci i Ołtarz znajdujący się w lesie zaczął budzić się do życia. W odpowiedzi kościół zaczął akumulować energię, by móc przyzwać anioła bez ryngrafu; uruchomił się zatem Szept Anioła, choroba duszy i ciała zwiększająca energię Anioła. Zwiększenie pola magicznego sprawiło, że artefakty Olgi zaczęły rezonować i pojawiły się pierwsze burze magiczne. A czarodziejka jest szczęśliwa - jeśli pojawia się energia magiczna, ona jest w stanie z tego korzystać i jest w stanie coś konstruktywnego z tym zrobić...

Ołtarz połączył się z jedynymi bytami z jakimi może się skomunikować. Karolina Błazoń i Paweł Franna. W reakcji Kult Anioła został wzmocniony Szeptem Anioła i skupił się na przekształceniu dwóch głównych agentów - księdza (który dołączył natychmiast do kultu) i Franciszka Błazonia.
Fajnie, ale niedługo potem Olga złapała księdza Czapieka i używając artefaktów przekształciła go w swojego agenta, wyrywając go spod kontroli Kultu i kościoła.

I na to przybył August Bankierz - terminus, który myśli, że to wszystko przez niego (bo widzi chorobę i nic więcej). Dał dyskretne ogłoszenie, że terminus szuka lekarza magicznego NIE ze Srebrnej Świecy, który bardziej chce pomóc ludziom niż zarobić. I to ogłoszenie przeczytała Paulina...

Misja zaczyna się z przybyciem Pauliny na pensję Franciszka Błazonia, gdzie rezyduje już August Bankierz.

## Misja właściwa:

**Faza pierwsza: Niegroźna(?) zaraza magiczna**

Inga Błazoń wpuściła Paulinę na pensję. Paulina zauważyła, że ta niedawno płakała; Inga powiedziała, że obiad i kolacja są już opłacone (August użył magii mentalnej by to załatwić). W rozmowie z Ingą Paulina zauważyła (11v8), że Inga jest pod wpływem zaklęcia mentalnego; nie dostrzega złamań Maskarady i do tego uważa, że wszystko jest opłacone. Ot, klasyczne działanie terminusów którego Paulina nie popiera. Zwłaszcza, że nie podoba jej się wysokie stężenie energii magicznej w okolicy.
Inga powiedziała jej też, że jej córka, Karolina, uciekła z domu. Więcej czasu przebywa w lesie niż z nimi. A wszystko zaczęło się od tego kultu anioła... który teraz jest rządzony przez męża Ingi, Franciszka. Inga powiedziała, że ona też należy do tego kultu, to tylko niegroźne pogańskie zabawy... ale niektórzy biorą to bardzo na poważnie a ksiądz poszedł z kultem na ostre i straszy, że wezwie jakichś inkwizytorów czy inne takie.
Inga się bardzo martwi i nie wie co z tym robić. 
Paulina nie ma zamiaru na razie łamać zaklęcia Augusta, ale jej się to bardzo ale to bardzo nie podoba...

Paulina weszła do pokoju, zostawiła tam swoje rzeczy i poszła do pokoju terminusa. August przywitał ją serdecznie. Taka... młodzieńcza radość (jakieś 15-16 lat?). Tak bardzo terminus...
August znalazł dla Pauliny pacjenta. To świeży pacjent, na którego nie rzucono zaklęć. August chciał by Paulina miała czystą próbkę. Policjant był zdrowy jeszcze 3 dni temu, gdy August przybył do miasteczka a teraz jest chory. Policjanta nie ma na pensji; jest gdzieś poza, ale August monitoruje go używając Terminuskiego Oka.
August twierdzi, że pole magiczne jest podniesione i samo zabezpiecza Maskaradę... ale jeszcze August nie znalazł źródła; wiąże jednak chorobę magiczną z tym polem.
Paulina zaczęła uzmysławiać Augustowi jeszcze implikacje tego, co on tak naprawdę zrobił - przez nadanie ludziom, że on im zapłacił mógł spowodować poważne problemy ze skarbówką i ich pieniędzmi. Pensja w takim miejscu na pewno nie jest super dochodowa. August się bronił - to procedura terminusa i do tego on przecież nie ma pieniędzy, jest magiem; przecież robi to dla ich dobra. Paulina jednak dała radę mu uzmysłowić (11v8), co naprawdę robi i jakie może mieć to implikacje. August będzie od tej pory uważał na to, co robi z ludźmi.
Paulina zapłaciła za pobyt; nie dla poczucia winy. Po prostu tak powinno być a ją na to stać.

W stancję uderzyła burza magiczna. Katalistka (Paulina) zadziałała natychmiast - przekierowała energię przez siebie i wypromieniowała ją jak radiator, nie dopuszczając do zbyt wielkiego Skażenia. August nie miał tyle szczęścia - Oko Terminusa było na zewnątrz oraz inne postawione przez niego zaklęcia były też aktywne. Miał otwarty kanał i przez to on uległ Skażeniu. Zwymiotował na podłogę i stracił kontrolę nad zaklęciami. Paulina, jak tylko opanowała pierwsze uderzenie (9v6), położyła nieszczęsnego terminusa który jej powiedział, że ma w torbie odgromniki. Paulina była miło zaskoczona - terminus, który nosi przy sobie przydatny sprzęt. Co prawda Paulina nie jest ekspertem od terminuskiego sprzętu, ale jest katalistką i alchemikiem; terminus powiedział jej jak tego sprzętu użyć i Paulina zabezpieczyła stancję przed kolejnymi burzami. August ma sprzętu na jeszcze 2 lokacje.
Paulina zorientowała się, że Inga nie ma na sobie zaklęcia terminusa. Ale - Paulina już jej zapłaciła, więc przymus "zapłacił mi chłopak" zmienił się na fakt "zapłaciła mi kobieta". Więc nie ma ryzyka ani problemu. 
Paulina wypakowała torbę i zdecydowała się pomóc Augustowi ze Skażeniem - (12v9). Młody terminus idealista z kamienną miną powiedział "activate me no matter what". I faktycznie, Paulina zniwelowała efekt Skażenia. August spojrzał na panią doktor z nowym szacunkiem.

August usłyszał "pomocy" od właścicielki. Poleciał przodem, Paulina z tyłu. Co się okazało: Inga była przy szafie gdy uderzyła burza magiczna. Wpadła do szafy, która się skurczyła i nie może wyjść, plus jest pokaleczona na rękach i nogach (jest krew, niewiele). August chciał załatwić zaklęciem sprawę, ale Paulina go powstrzymała - lepiej po burzy, nawet najmniejszej, nie rzucać zaklęć. Spytała gdzie jest piła i siekiera, po czym po nie poszła. August uwolnił gospodynię z szafy.
Paulina przekonała ją (10v5), że to był problem sejsmiczny; jako, że Inga jest w szoku niewiele było trzeba. August siedział cicho i się uczył. Paulinie się trochę bardziej już spodobał.

W krótkiej dyskusji między Augustem i Pauliną Paulina dowiedziała się, że August "nie jest ludzistą", ale chce pomóc tym ludziom. Wyciągnęła od niego (11v6), że:
- w przeszłości August miał starcie z lokalną czarodziejką i podejrzewa, że ona stoi za atakiem
- August podejrzewa, że on może mieć coś wspólnego z tym wszystkim i chce to naprawić; nie wie, czy zaklęcie Losowej Ochronnej Teleportacji Nicole Archer nie zepsuło czegoś więcej
- lokalna czarodziejka wygląda na to, że ma jakieś obsesje i jest niepoczytalna
- przedtem było tu rodzeństwo terminusów (też uczniowie jak August), którzy zlokalizowali i usunęli bardzo groźny Kult Anioła powiązany z viciniusem
- czarodziejka była powiązana z tym aniołem, ale najwyraźniej coś wymknęło jej się spod kontroli
- czarodziejka żyje w permanentnym Skażeniu przekraczającym możliwości wytrzymania przeciętnego maga

Paulina zadała Augustowi trafne pytanie: "skąd czarodziejka wie o twoim pobycie tutaj". W odpowiedzi August stwierdził, że nie ma pojęcia. Teoretycznie, na stan wiedzy Pauliny, ta czarodziejka jeśli jest katalistką MOŻE kanalizować Skażenie w burze, ale to znaczy że jest w tak silnym Skażeniu, że najpewniej jest już bardzo nieludzka; a nic z tego co usłyszała wcześniej na to nie wskazuje. Czyli muszą być jeszcze inne źródła energii magicznej.

Paulina poprosiła Augusta o coś w jego torbie co może pomóc w znalezieniu źródeł magicznych. August z ciężkim sercem dał jej dziwny, technomantyczny artefakt. On nie do końca wie co to jest, dostał to od koleżanki której nie lubi. Paulina wyciągnęła z niego jak to możliwe; August zrobił coś głupiego owej koleżance za co miał ją przeprosić (mistrz wyraźnie nie rozumiał, że to była jej wina). Dziewczyna przyjęła przeprosiny warunkowo - jeśli za to on weźmie ten artefakt i dała mu długą instrukcję. Wzburzony (bo warunkowo), August wywalił instrukcję bez czytania.
...ta młodzież...
Paulina zdecydowała się sprawdzić jak ten artefakt działa w perspektywie problemu jaki napotkali (13v9). Po odpowiednim wysiłku udało im się postawić ten artefakt na nogi mimo braku instrukcji. August powiedział, że on go weźmie i przejdzie się po okolicy by zlokalizować wszystkie źródła mocy które mogą coś robić. W wyniku tego konfliktu: Sandra Stryjek dowiedziała się, że August ma pewien problem do rozwiązania i używa tego artefaktu a jeśli coś się stanie z artefaktem to Paulina będzie o tym wiedzieć.

Paulina zeszła na dół, do Ingi i pożyczyła od niej stary telefon komórkowy (dumbphone z ładowarką). Poszła do kiosku kupić jakiś starter i była świadkiem dziwnego wydarzenia.
Z jednej strony, 10 kultystów i Franciszek Błazoń. Z drugiej strony ksiądz. Widać, że przed chwilą między nimi do czegoś doszło... a jako trzecia strona konfliktu wąsaty grubas a koło niego znękany policjant. Grubas jedzie po księdzu "wracać do swojej parafii zamiast mi tu kapuściane łby antagonizować" i po kultystach "jak się ma taką kapustę jak wy zamiast głów, każę aresztować was wszystkich i będziecie w piwnicy się kisić". O dziwo, burmistrz (bo grubas okazał się być burmistrzem) dał radę rozpędzić tłum i rozładować starcie. Po chwili burmistrz szepnął do policjanta, żeby ten poszedł do lasu i z leśniczym znaleźli tą małą od Błazionów, bo ojciec zachowuje się nieracjonalnie...

Paulina zauważyła, że zarówno Franciszek Błazoń jak i ksiądz są Skażeni do tego stopnia, że są klasyfikowani jako viciniusy. Na pewno mają jakieś specjalne umiejętności. Szczęśliwie, jest to jeszcze proces odwracalny. Ważne, że poziom Skażenia nie jest wystarczający do tego, by stali się takimi bytami, tu musi być coś więcej. Plus, czemu TEGO SAMEGO TYPU? To nie ma sensu z punktu widzenia katalistki...

Paulina kupiła starter. Burmistrz się jej spodobał.
Kioskarka, Marianna Zurka, powiedziała Paulinie co następuje:
- Ksiądz zastąpił drogę Franciszkowi Błazoniowi i kultystom. Powiedział, że mają wrócić do Prawdy, do Boga. O tyle to dziwne, że ksiądz nigdy się nie mieszał. Nie był dość odważny.
- Franciszek zarzucił księdzu, że ten zdradził Prawdę. Ksiądz powiedział, że zbłądził, ale powrócił.
- Przyszedł burmistrz i zrobił porządek.
- Kioskarka przyznała się, że sama jest członkinią kultu Anioła. No Aneta uzdrawiała. JĄ (Mariannę) wyleczyła.
- Aneta uległa wniebowstąpieniu (tak bardzo ślady terminusów...).
- Podobno zmarły "pedofil" (Paweł Franna) kryje się w lesie i wabi dzieci, dlatego mała Błazoniówna nie wróci już do domu.

**Faza druga: Nadnaturalny sojusznik(?)**

Paulina spotkała się znowu z Augustem i streściła mu historię o której usłyszała. August się zasępił. Wspólnie z Pauliną ustalili, że się podzielą - on poszuka źródeł magicznych używając Dziwnego Artefaktu Sandry a ona poszuka dziewczyny w lesie, potencjalnie nawiązując kontakt z nieumarłym. August ma wątpliwości czy Paulina podoła spotkaniu z nieumarłym, ale ta rzuciła że JEST nekromantką a do tego i tak nie mają czasu.
Paulina powiedziała Augustowi, że czarodziejka która zaatakowała go raz może zaatakować ponownie. August w odpowiedzi powiedział, że jest terminusem i sobie poradzi... wziął ze sobą jeden zestaw odpromienników magicznych (odgromników). Wygląda przez to jakby był Dziwnym Inżynierem. Trzeci zestaw trafia do pokoju Pauliny.
August idzie szukać źródeł a Paulina wybrała się do lasu, szukać dziewczyny (Karoliny Błazoń).

Paulina zeszła na dół poprosić Ingę o zdjęcie Karoliny; Inga zasnęła na krześle. Czujne oko Pauliny zauważyło, że ma do czynienia z chorobą magiczną. Paulina wezwała Augusta by to zobaczył, po czym zakasała rękawy, wywaliła bezużytecznego w tej kwestii terminusa i zabrała się do badań. Paulina chce poznać naturę tej choroby (14v12). Dowiedziała się:
- energia jest wytransferowywana z ludzi dotkniętych Szeptem Anioła i przekierowywana jest w kierunku kościoła (miejsce pochodzenia Anioła)
- nie ma anioła (dwójka terminusów go rozwaliła), więc uruchomił się inny podsystem. Wygląda to na bardzo kompleksową konstrukcję artefaktów i zaklęć
- to wygląda na zbiór starych zaklęć i konstrukcji magicznych; bardzo złożony system budowany latami dawno temu
- drenaż energetyczny z kościoła trwał zawsze; im większa energia jest potrzebna tym bardziej drenowani są ludzie. Nadmiar drenowania objawił się jako choroba magiczna.
- drenowani mogą być tylko kultyści. Dokładniej: osoby, którym jakoś wbudowano otwarcie nieistniejących u ludzi kanałów magicznych; muszą być podpięci do Anioła.
Do tego do kontaktów z Szeptem Anioła (nazwa choroby) ma stały bonus #2 tool.

Paulina zauważyła, że musi być jakiś proces inicjacyjny - inaczej kultystom nie dałoby się otworzyć kanałów.

Ok, Paulina chce znaleźć dziewczynkę. Weszła do jej pokoju korzystając z nieobecności Ingi by znaleźć coś na temat dziecka. Coś, o co można się zaczepić. (9v7). Zdążyła przed powrotem ojca do domu - znalazła pamiętnik dziewczynki, schowany w szparze łóżkowej. Dziewczynka - przypominam - ma 14 lat.
W pamiętniku pojawiło się coś, co zainteresowało Paulinę.
- "Tata zachowuje się jakoś dziwnie... to jest straszne. Ten anioł, pani weterynarz, boję się trochę tego wszystkiego..."
- "Anioł jest światłem, ale nie jest Światłem. Anioł jest więzieniem. Więzieniem dla Światła."
- "Nie czuję się normalnie. Nie czuję się dobrze. Od czasu tego na ołtarzu, tego co zrobili, Światło wypełnia mnie. Nie anioła. Nie czuję się... taka jak kiedyś. Nie czuję taty, mamy... nie czuję koleżanek. Nie chcę być sama. Nie słyszę szeptów anioła... ale słyszę szept. Nie rozumiem go. Nie zagraża mi. Rozumiem teraz, dlaczego pan Franna chciał mnie uratować. I ja jego uratuję."
- Kilka wskazówek w lesie kieruje Paulinę na ten dziwny ołtarz.

Paulina się nieco zastanowiła nad tą sytuacją. Wróciła do swojego pokoju i zadzwoniła do Augusta. Terminus nie odpowiedział (Ołtarz). 
Paulina poszła więc w kierunku lasu. Poszukać Karoliny Błazoń.
Karolina siedzi na zwalonym drzewie i macha nogami. Wygląda... zwyczajnie. Jak dziewczyna która przywykła do poruszania się po lesie.
Karolina dostrzegła Paulinę. Zupełnie nie jest wystraszona... dla Pauliny to jest dziwne. Jednak chwilę później Paulina wypatrzyła nieumarłego nauczyciela; znajdował się w drzewie.

Po krótkiej i niezobowiązującej rozmowie, Paulina skupiła się na stanie Karoliny. W rozmowie wypłynęło ciekawe zdanie ze strony dziewczynki "wszyscy którzy pamiętają albo oddadzą się pod opiekę światła albo światło ich pożre". Powiedziała też Paulinie, że to nie przy tym ołtarzu jej ojciec zbiera grupę; to nie jest ołtarz Anioła, to ołtarz innego anioła. I ojciec oddał ją w opiekę temu innemu aniołowi, bo nie wiedział... w wyniku tego Karolina nie jest taka jak kult, ale nie jest też taka jak inni ludzie. Karolina jest zupełnie inna. I jest sama.

Duch Pawła Franny powiedział Paulinie, by ta przytuliła Karolinę. Tak też zrobiła. Paweł wyjawił Paulinie kilka informacji:
- Aneta Kosicz zabiła go; Paweł nie wie czy specjalnie czy przez przypadek
- Olga go wskrzesiła i wysłała na misję.
- Ołtarz wypalił kontrolę Olgi i nadaje mu energię. Celem Ołtarza jest uwolnienie ludzi tutaj spod wpływów magicznych i wypalenie Olgi.
- Ołtarz ma moc puryfikacji.

Paweł Franna przedstawił też Paulinie strony konfliktu:
- Ołtarz chroni jego i Karolinę. Ołtarz jest stroną defensywną i staje po stronie ludzi.
- Olga walczy z kultem. Jej cel jest nieznany ale ona jest katalizatorem zła.
- Kult wspierany jest przez kościół. Kult wysysa energię z ludzi, krzywdząc ich. Musi zostać powstrzymany.

Paulina zapytała o kapłana. Paweł Franna nie zna odpowiedzi na to pytanie.
Paulina zatem poszła obejrzeć ołtarz.

Ołtarz wygląda jak stary blok kamienia prezentujący wizerunek anioła z mieczem. Na ołtarzu napisane są runy i symbole. Paulina spróbowała je zrozumieć i przez to usłyszała Szept Iliusitiusa (8v12->12, remis!). Iliusitius Spojrzał na Paulinę i zdecydował się zważyć jej życie "innocent" vs "guilty". Patrząc na działanie z Ingą i wcześniejsze wybrał "innocent". Władca wróżdy dał Paulinie informację: Olga Żmijucha nie jest czarodziejką. Paulina zadała nieśmiałe pytanie czy była, usłyszała, że była. Iliusitius powiedział, że kult w tym miasteczku jest czymś, z czym on sobie poradzi, choć jeśli Paulina się w to włączy, zginie mniej osób. Ale najważniejsze, zdaniem Iliusitiusa, jest to, by usunąć Olgę. "Czarodziejka" powoduje debalans i sprawia, że kult eskaluje bo mechanizmy obronne miasteczka reagują przeciwko niej.
Iliusitius powiedział Paulinie, że terminus też zobaczył Jego Światło. I że nie zginął - odejdzie wolny nie pamiętając niczego o Iliusitiusie. Paulina ma prawo decyzji w sprawie Olgi - zabić czy ratować. Ale niech się spieszy, bo kult przygotowuje się powoli do ataku.

Paulina otworzyła oczy. Słaba, zakrwawiona i wyczerpana. Karolina Błazoń, prorokini Iliusitiusa wyleczyła ją dotykiem. Paulina wie, że dziewczynka jest dla niej stracona.

**Faza trzecia: Czarodziejka Zależa Leśnego**

Paulina zadzwoniła do Augusta. Terminus dla odmiany odebrał; powiedział, że idzie do kościoła. Wykrył tam podwójne źródło energii. Pierwsze źródło to akumulacja pasująca do... ponad połowy miasteczka. Drugie źródło to transmiter; przekierowuje energię z kościoła na dom Olgi. Paulina poprosiła go by się wstrzymał; ona chce wpaść mu pomóc. August się zgodził. Poczeka na nią.
Paulina i August zabrali ze sobą jeden zestaw odgromników magicznych (trzeci - ten który August miał przy sobie gdy Spojrzał weń Iliusitius się spalił).

Paulina chciała wpierw pójść w kierunku na chatkę Olgi, ale wyczuła burzę magiczną która uderzyła w kościół. Ta burza była silniejsza. Podczas rozmowy, Paulina powiedziała Augustowi: Olga Żmijucha nie jest już czarodziejką; była. Z Augustem wysunęli hipotezę, że Olga próbuje kompensować brak mocy magicznej używając samej Woli i Skażenia. Plan Pauliny na wyciągnięcie Olgi z domku polega na tym, by odciąć jej źródło energii przez ekranowanie kościoła oraz przez pozbycie się Szeptu Anioła.

Czyli plan jest prosty: kościół - Szept Anioła - integracja kontrzaklęcia na Szept do Ołtarza Iliusitiusa - dorwanie czarodziejki.

Ale wpierw - lekarstwo na Szept Anioła. Paulina i August wrócili na stancję; burzą zajmą się potem. Paulina poszła do Ingi; ta jest w gorszej formie ale nadal nie bardzo złej. Widać, że drenaż energii został bardzo wzmocniony. Kościół potrzebuje ogromnych ilości energii; jeśli Paulina się nie myli, Inga (świeżo chora) nie przeżyje 48h. A co tu mówić o innych, którzy wcześniej byli chorzy?
August został wysłany po policjanta. On była chory dużo wcześniej, więc stanowić będzie świetne źródło informacji.
August przyniósł policjanta. Nieszczęśnik jest w komie.

Stopień Pauliny: 3 (1 tools # 2 poprzedni test tools) # 1 (circumstance, policjant) # 3 (2 tagi) # 8 = 15 v 14 -> 14. Zajęło to Paulinie dość sporo czasu, ale w tym czasie August dał radę zmodyfikować (przez złożenie swoich artefaktów) przekierowanie na Ołtarz Iliusitiusa. Innymi słowy, jak Paulina i August zamontują kontrzaklęcie na Szept Anioła w kościele, energia będzie przekierowana do Iliusitiusa. On wykorzysta to do leczenia Szeptu Anioła. To, w połączeniu z odgromnikami magicznymi sprawi, że Szept zacznie zanikać a ludzie zaczną być leczeni. To doprowadzi do tego, że Iliusitius będzie miał mniej energii. Win-win dla Pauliny.

Kościół jest opuszczony. Nie ma w środku nikogo, stacja przekaźnikowa była ukryta pod iluzją. August zerwał iluzję i dołączył komponent Pauliny do stacji przekaźnikowej. Operacja 'puryfikacja' została rozpoczęta.

Została jeszcze Olga...

Zbliżając się do domu Olgi August i Paulina zobaczyli, jak do domu Olgi weszli ksiądz z leśniczym i zamknęły się za nimi drzwi. Leśniczy trochę nie chciał, ale wszedł. August zatrzymał Paulinę przed wbiciem do środka.
Skażenie domu Olgi jest bardzo duże; normalny człowiek nie wytrzyma długo w takim miejscu.

Paulina poprosiła Pawła Frannę do wejścia do domu Olgi i wyskautowania. Paweł się zgodził. August założył to za rzecz normalną; w końcu Paulina jest nekromantką.
Paweł wszedł do budynku. Tam znajduje się na wpół szalona Olga. Panikuje, że ilość energii magicznej jej znika. Spojrzała na Mariannę i Dariusza Remonta (leśniczego) i na nóż rytualny. Zastanawia się nad zwiększeniem ilości energii, ale ma wątpliwości. Jeszcze nikogo nie zabiła...

Paulina wpadła na przedni koncept. Jako, że Olga jest na wpół szalona i potrzebuje źródła energii, Paulina przygotowała jej katalitycznie źródło energii. Tuż przy chacie (trochę poza zasięgiem samej chaty). Tak bardzo smaczne i cenne źródło energii. Paweł Franna zrobił swoje - subtelnie zaczął podpowiadać Oldze, że widział takie źródło, całkiem niedaleko, bo się zrobiło po ostatniej burzy...
Olga wybiegła po źródło energii. Gdy się do niego zbliżyła z odpowiednimi artefaktami, swoje zrobił już August i ją poraził, unieszkodliwiając na miejscu...


### Dark Future:

**Faza pierwsza**

(Inga Błazoń popłakuje, bo Karolina nie wraca do domu)
(August prezentuje Paulinie chorego policjanta)
(niewielka burza magiczna uderza w pensję Błazonia; Inga jest skaleczona)
(starcie między księdzem Czapiekiem a Franciszkiem Błazoniem)
(leśniczy ucieka do lasu, do swojej altanki by zwiać przed kultem)
(burmistrz rozpędza tłum)

**Faza druga**

(Inga Błazoń zachorowała na Szept Anioła)
(August idzie zobaczyć Ołtarz)
(z Pauliną chce negocjować nieumarły historyk, chcąc powiedzieć jej co tu się dzieje)
(ksiądz Nawraca jednego z kultystów i każe mu się udać do Olgi)
(ksiądz stawia w kościele stację przekierowującą magię dla Olgi)

**Faza trzecia**

(kult fizycznie atakuje kościół; Olga manifestuje na nich wir magiczny. Są ranni.)
(ksiądz łapie leśniczego w lesie i prowadzi go do Olgi)
(Karolina przychodzi do Pauliny; jeśli wyjdzie coś o Ołtarzu, August # Paweł -> Paulina)
(kościół zostaje odkryty jako źródło Szeptu Anioła)
(August niszczy stację przekierowującą magię)
(policjant wpada w komę przez Szept Anioła)
(Karolina wraca do domu wypalić z matki Szept Anioła)

**Faza czwarta**

(Olga zabija leśniczego i pożera jego energię)
(radiatory powodują potężną burzę magiczną nad chatą Olgi)
(kultyści zbierają siły by atakować Olgę)
(Karolina próbuje zatrzymać rozlew krwi, ale jej się nie udaje)
(policjant umiera przez Szept Anioła, nie on jeden)
(ksiądz zostaje zamordowany przez kultystów)

**Faza piąta**

(kościół przekierowuje więcej mocy we Franciszka, zmieniając go w Anioła Światła)
(Olga zmienia się w potwora przez nadmiar energii magicznej)
(kościół zostaje zniszczony)
(August odmawia wezwania wsparcia; obraca się przeciw Paulinie jeśli ta nie jest jedną z Ołtarzem)
(wiele ludzi ginie)
(zarówno Olga jak i Franciszek zostają zabici przez Augusta)

# Lokalizacje:

1. Świat
    1. Primus
        1. Podlasie
            1. Powiat Orłacki
                1. Zależe Leśne
                    1. Serce
                        1. pensja Błazonia
                        1. kawiarenka Ukrop
                        1. kościół
                    1. Wał Kartezia
                        1. dworek pana
                        1. cmentarz
                            1. krypta rodu Zaleskich
                    1. Koniowie
                        1. siedziba czarodziejki
                        1. dawna chata lokalnej wiedźmy
                    1. Las Gęstwiński
                        1. ołtarz Anioła
                        1. altana leśniczego

# Zasługi

* mag: Paulina Tarczyńska jako lekarz, która otarła się o moc Iliusitiusa i przeżyła. Dodatkowo uratowała Zależe przed wojną kult - czarodziejka.
* mag: August Bankierz jako terminus, który wrócił do Zależa rozwiązać problem który (jak uważa) sam stworzył. Wpadł w sidła Iliusitiusa.
* vic: Olga Miodownik jako coraz bardziej szalona czarodziejka, która tak naprawdę nie włada magią od Zaćmienia. W niej samej granica między osobą i potworem się zaczyna zacierać.
* vic: Rafał Czapiek jako Dawca Prawdy kontrolowany przez Olgę; wcześniej: ksiądz w Zależu Leśnym. Ma dla Olgi przejąć kontrolę nad Kultem Anioła i polować na ludzi.
* vic: Paweł Franna jako nieumarły nauczyciel historii który próbuje chronić Karolinę i oczyścić Zależe z magii... i Olgi.
* vic: Franciszek Błazoń jako Dawca Prawdy rządzący Kultem Anioła. Chce zbudować masę krytyczną wyznawców i uderzyć w czarownicę (Olgę).
* czł: Inga Błazoń jako gospodyni pensji Błazonia; teraz ona się tym zajmuje bo jej mąż zajmuje się kultem. Popłakuje, bo nie widuje córki i boi się co się dzieje. Kultystka.
* czł: Karolina Błazoń jako czternastolatka, która częściowo uciekła z domu i która stopniowo staje się prorokinią Iliusitiusa.
* czł: Dariusz Remont jako leśniczy, który uciekł do lasu przed Kultem Anioła. Z deszczu pod rynnę - wpadł w ręce Olgi...
* czł: Kamil Gurnat jako policjant, który ma wyraźne objawy choroby. Zapadł w komę, ale Paulina go wyleczyła.
* czł: Szczepan Zaleski, burmistrz Zależa Leśnego. Ambitny i bezczelny człowiek, który z radością wita lekarza w Zależu. Zależy mu na miejscowości.
* mag: Sandra Stryjek jako czarodziejka która dostarczyła Augustowi artefakt w ramach zaakceptowania przeprosin. Artefakt śledzi Augusta gdy ów go używa.
* czł: Marianna Zurka jako kioskarka i członek kultu Anioła. Lubi plotkować. Porwana przez Olgę celem pożarcia.
* vic: Iliusitius, pan wróżdy, manifestujący się przez ołtarz anioła; otoczył opieką Karolinę Błazoń i zmienia ją w swoją prorokinię.
* czł: Rufus Eter, biznesmen bardzo zniechęcony do współpracy z czarodziejką Olgą Żmijuchą; odwołał agentów i zatrzymał współpracę.

# Fronty:

### Front 1: Czarodziejka Zależa Leśnego
Expresses: siły pojedynczej czarodziejki próbującej ustabilizować swój świat
Scarcity: despair (wszystko co próbuje uratować się rozpada; traci moc magiczną)

Agenda/ Dark Future:
Skażenie zasila paranoję, paranoja zasila Skażenie. Wraz z upływem czasu Olga traci coraz więcej człowieczeństwa i coraz bardziej staje się potworem; jej percepcja też się zmienia. Burze i fale magiczne zaczynają Skażać obszar - przez specyfikę pozostałych sił na terenie nikt nic nie zauważa...

Threats: 
- Olga Miodownik
=== Kind: grotesque, mutant
=== Impulse: craves restitution
=== Description: zbierająca artefakty czarodziejka pochodząca ze świata ludzi. Eks-patolog sądowy. Sprzeda duszę i zmieni ciało - tylko by nie stracić magii. Odsuwa tą groźbę od Zaćmienia. Powoli jej obsesja zabija jej ostrożność a jej naturalny optymizm zmienia się w rozpacz.

- Dawca Prawdy Rafał Czapiek
=== Kind: grotesque, disease vector 
=== Impulse: craves contact, intimate and/or anonymous
=== Description: Przekształcony i dostosowany przez artefakty Olgi ksiądz, który ma za zadanie przechwycić i przekazać Oldze jak najwięcej osób z kultu Anioła. Próbuje wyegzorcyzmować nieumarłego w czasie wolnym.
=== Custom move: Nawracający Rytuał atk 10 (vs BDS#WDS). Jeśli się uda to regeneruje ciało i nadaje moc fanatyzmu w kierunku na Olgę.
=== Custom move: Wyraźne Skażenie atk 8 (vs ADS#BDS). Jeśli się uda, mag się orientuje że ma do czynienia z głęboko Skażoną osobą.

- Płynne pola magiczne
=== Kind: affliction, condition
=== Impulse: to expose people to danger
=== Description: Z uwagi na wysokie nagromadzenie artefaktów i sił w okolicy, magia zaczyna robić się nieco niestabilna. Struktura domu Olgi pozwala jej na jakąś formę kontrolowania przepływu mocy magicznych, dzięki czemu w nią nie uderzają centralne punkty i problemy. Olga może to próbować wykorzystać jako artylerię.
=== Custom move: Emisja magiczna atk 8 (vs BDD#WDD). Jeśli się uda, dojdzie do Skażenia jakiegoś obszaru / Paradoksu zaklęcia.
=== Custom move: Skażenie atk 6 (vs BDD#WDD). Większość obszaru jest Skażona, co prowadzi do zmian magicznych i uczucia choroby; -1 do -2 do testów.
=== Custom move: Skażenie atk 10 (vs BDD#WDD). Chata Olgi pływa w Skażeniu. Porażka uniemożliwia czarowanie bez Paradoksu.

Stakes:
- Czy Olga stanie się potworem zanim wyjdzie prawda? -> nie zmieniła się w potwora
- Czy Olga uzyska i zabije leśniczego dla zwiększenia mocy? ->nie zdążyła zabić
- Czy ksiądz Czapiek przeżyje spotkanie z nienawistnym tłumem (lub nieumarłym historykiem)? -> przeżył

### Front 2: Kult Anioła
Expresses: rosnący w siłę i populację kult Anioła Światła, próbującego zaprowadzić anielski pokój w Zależu Leśnym. 
Scarcity: ignorance (niewiedza o działaniu magii, niewiedza o Skażeniu)

Agenda/ Dark Future:
Zależe zostanie nawrócone na wiarę Anioła Światła. Wielu zginie, wielu się wzmocni śmiercią pozostałych, wielu ulegnie Skażeniu.

Threats: 

- Kultyści Anioła
=== Kind: brutes, cult
=== Impulse: to victimize & incorporate people
=== Description: Grupa ludzi wierzących, że Aneta stała się Aniołem. Słuchają Dawców Prawdy. Część z nich mogła ulec Skażeniu.

- Dawca Prawdy Franciszek Błazoń
=== Kind: grotesque, disease vector 
=== Impulse: craves contact, intimate and/or anonymous
=== Description: 
=== Custom move: Nawracający Rytuał atk 10 (vs BDS#WDS). Jeśli się uda to regeneruje ciało i nadaje moc fanatyzmu w kierunku na Anioła.
=== Custom move: Wyraźne Skażenie atk 8 (vs ADS#BDS). Jeśli się uda, mag się orientuje że ma do czynienia z głęboko Skażoną osobą.

- Szept Anioła
=== Kind: affliction, disease
=== Impulse: to saturate a population
=== Description: Magiczna choroba bazująca na nekromancji i katalizie. Choroba jest wzmocnionym efektem tego, co robi Anioł; pobiera energię życiową i osłabia wolę celem wzmocnienia kościoła i jego wpływu na kult. Retransferuje energię do Franciszka Błazonia, wzmacniając go przeciw Rafałowi Czapiekowi. Dodatkowo sprawia, że nikt nie chce mówić o dziwnych wydarzeniach (chroni przed naruszeniem Maskarady).
=== Custom move: atk 10 (vs BDD # WDD); łączy z Franciszkiem Błazoniem, nadaje stały modyfikator -3 do działań przeciw niemu i osłabia.
=== Custom move: atk 14 (vs AAD # BAD); opracowanie antidotum i anty-zaklęcia na Szept Anioła.

- Anioł Światła
=== Kind: affliction, barrier
=== Impulse: to impoverish people
=== Description: konstrukt, artefakt, potwór Zależa Leśnego. Nie do końca wiadomo co to jest, ale jest to istota powiązana ze światem religijnym, która wspiera pana Anielskiego Dworu i go chroni oraz pomaga, kosztem okolicznych ludzi. Manifestuje się jako światło i łączony jest przez swoich wyznawców.

Stakes:
- Czy Franciszek przeżyje i wróci do rodziny?   -> przeżył. Nie wiemy jeszcze.
- Czy Maskarada zostanie złamana?   -> jeszcze się trzyma
- czy kościół zostanie zniszczony?  -> nie został

### Front 3: Światło Zależa.
Expresses: próby ochrony Zależa Leśnego przed magią, przed koszmarem Olgi i próba przywrócenia starego, dobrego Zależa jakie było do tej pory.
Scarcity: ambition (odbudowa Zależa).

Agenda/ Dark Future:
Zależe zostanie oczyszczone ze wszystkich form magii. Wszystko pożre Ołtarz. Kultyści z Kultu Anioła zostaną puryfikowani przez Ołtarz.

- Paweł Franna, nieumarły nauczyciel historii
=== Kind: grotesque, cannibal
=== Impulse: craves satiety and plenty
=== Description: nieumarły nauczyciel historii przywrócony do nieumarłości przez Olgę Żmijuchę. Został uwolniony od jej kontroli i znowu skupia się przede wszystkim na ochronie młodziutkiej Karoliny Błazoń. Pragnie, by wszystko było jak dawniej. Chce zniszczenia Olgi za to, co zrobiła jemu i Karolinie.
=== Custom move: Prawdziwe Słowa. Atk 9 (vs HDS#ADS); jeśli Paweł próbuje kogoś przekonać, druga strona w to wierzy bezgranicznie. 

- Karolina Błazoń, prorokini Zależa
=== Kind: grotesque, disease vector 
=== Impulse: craves contact, intimate and/or anonymous
=== Description: Skażona przez ołtarz i Anioła młoda dziewczyna, która czuje instynktownie, że kult Anioła jest czymś złym. Czymś niewłaściwym. Dziwnie się czuje i szuka kontaktu z ludźmi, chce ich "poczuć", chce być częścią innych ludzi... chwilowo. Nie wie co się dzieje, ale wie, że się zmienia.
=== Custom move: Dotyk Światła. atk 10 (vs BDS#WDS); jeśli się uda to regeneruje ciało i nadaje moc fanatyzmu w kierunku na Ołtarz.
=== Custom move: Aura Światła. atk 12 (vs BDD#ADD); jeśli jest na nią rzucone zaklęcie, potęga Światła uderza w czarującego, egzorcyzmując magię.

- Ołtarz Anioła Światła / Iliusitiusa
=== Kind: landscape, furnace
=== Impulse: to consume things
=== Description: Ołtarz Anioła Światła nie zbudowany przez chrześcijan. Światło, które chroni i światło, które czyści. Światło, które pożera magię i wypala wspomnienia.
=== Custom move: Aura Światła. atk 12 (vs BDD#WDD); jeśli jest na nią rzucone zaklęcie, potęga Światła uderza w czarującego, egzorcyzmując magię.
=== Custom move: Szepty Światła. atk 12 (vs HDS#ADS); jeśli ktoś próbuje zrozumieć, co jest napisane na Ołtarzu, ulega jego wpływowi. Zapomina o jego mocy i znaczeniu i ignoruje działania frontu Światła.

Stakes:
- czy Paweł zostanie zniszczony przez starcie z Olgą i jej siłami?  -> nie, przeżył
- czy Ołtarz Światła otrzyma kontrzaklęcie blokujące Szept Anioła?  -> tak
- czy Paweł da radę przechwycić artefakty Olgi by zasilić Ołtarz?   -> jeszcze nie
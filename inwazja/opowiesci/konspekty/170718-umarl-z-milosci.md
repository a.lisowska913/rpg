---
layout: inwazja-konspekt
title: "Umarł z miłości"
campaign: adaptacja-kralotyczna
players: kić, raynor, foczek
gm: żółw
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [170718 - Umarł z miłości (temp))](170718-umarl-z-milosci.html)

### Chronologiczna

* [170920 - Początki prokuratury](170920-poczatki-prokuratury.html)

## Kontekst ogólny sytuacji

Jednostrzałówka.

## Punkt zerowy:

Pytania MG do siebie:

?

**Stan początkowy**:

1. Duch Utraconej Nadziei Piżucha
    1. Femme Fatale zakochuje się w Stefanie Piżuchu: 0/10
    1. Femme Fatale popełnia samobójstwo: 0/10
    1. Skandale i impulsy emocjonalne: 0/10
    1. Struktura firmy (IT, reputacja, infra) się rozpada: 0/10
1. Naturalna Entropia
    1. Krystalia odnalazła swoje zaginione koralowce: 0/10
    1. Erupcja pawiego pióra: 0/10
    1. Zainteresowanie świata zewnętrznego: 0/10
    1. Ubóstwo, nadmierny wysiłek, wszystkie ręce na pokład: 0/10

**Pytania i odpowiedzi**

* Ż: W firmie jakiego profilu (white collar) pracujecie?
* J: Firma prawnicza. MaksymLaw - Eksmisje.
* Ż: Kim jest piękna acz strasznie oportunistyczna femme fatale w Waszej firmie? Jednym zdaniem.
* K: Bardzo by chciała być znaną prawniczką, ale tylko dochapała się wspólnictwa. Ledwo co. Parę spraw zrobiła, acz znajdowała łosia. Jolanta Karbon.
* Ż: Kim jest przystojniak, który się w niej strasznie podkochiwał? Jednym zdaniem.
* R: Gościu w sortowni korespondencji. Przyjeżdża rano, sortuje, wyjeżdża i rozdaje. Stefan Piżuch.
* Ż: Szef firmy, interes rodzinny. Na jakim trofeum najbardziej mu zależy? Skąd je zdobył?
* J: Wielkie pióro z pawia kiedyś używane przez znanych prawników w Ameryce Południowej. Te klimaty. Wykazał się w konkursie i wygrał. Tomasz Klink.
* Ż: Dlaczego to, co dzieje się w firmie jest Waszym bezpośrednim problemem?
* K: Dotknęło wszystkich dookoła tylko nie nas - i dlatego nas podejrzewają.
* Ż: Skąd się znacie i jak dowiedzieliście się parę dni temu o swoich dziwnych umiejętnościach
* R: Cała nasza trójka zna się z dzieciństwa. Wracaliśmy po pijaku z imprezy i zaczęło się objawiać.
* Ż: W jaki sposób moc postaci Raynora sprawia, że macie więcej czasu na swoje sprawy?
* J: Kernel przyciągający uwagę.
* Ż: W jaki sposób umiejętność Foczka może Wam pomóc w uratowaniu krytycznej sytuacji?
* K: Ktoś, kto się na nas bardzo wkurzy o nas zapomni.
* Ż: W jaki sposób umiejętność postaci Kić uratowała Wam raz pracę?
* R: Do jednego z klientów miało pójść zapytanie ofertowe. Kac. Poszedł głupi mem. A klient Bank of America... co zrobiła Kić? Rozwaliła serwer. Tech Destroyer.
* Ż: Dlaczego Wasza firma mieści się w tym mieście? Co jest jego własnością szczególną?
* J: Na obrzeżach miasta jest fabryka gdzie produkowano czołgi. Podobno już tego nie robią.
* Ż: Określ zbliżoną lokalizację na Śląsku Waszego miasta? Coś więcej o nim.
* K: Jodłowiec. Wraz z Kopalinem i Czeliminem tworzy trójkąt. Linia kolejowa do Czelimina. Obok niego jest stary, nie używany poligon.
* Ż: Główne aspekty Waszej siedziby i podstawy modelu biznesowego.
* R: Jest to firma po części outsourcingowa; prawnicza ale nie zajmuje się prawem polskim. Outsourcuje siłę roboczą do USA. Specjalizuje się w prawie USA.

## Misja właściwa:

**Dzień 1:**

_Rano: (+1 akcja O)_

Jadwiga tańczy na stole, inni rzucają konfetti.
Ksenia opiernicza i wysadza radio. Skonfliktowane. Klient przyjedzie za wcześnie, nie zdążą posprzątać.
Zenek i Krzysztof przechwycili klienta - Tony'ego Armadillo - i wzięli go na wuzetki i kawę. Kryzys zażegnany.

_Południe: (+1 akcja O)_

Ksenia do Zenka. Tam był burdel. Firma mogła ucierpieć. Ale poradzili sobie. Tym razem było to bardziej efektowne... wcześniej to bywało gorsze.
Ksenia poszła do Jana Pyszczyńskiego, prawnika. On odbijał tyłek na kserokopiarce. Jest zagrożony wylotem i podatny.
Pyszczyński, naciśnięty przez Ksenię, się popłakał. Powiedział, że ona była piękna. Że to było dziwne. Że chyba ktoś jakieś dragi podłożył... coś jest nie tak.
Tomasz Klink wezwał Ksenię na dywanik. Ona powiedziała co się dzieje. Klink zrobił się ponury. Kazał jej też wyjść.
Tymczasem Krzysztof przeglądał maile, filmy itp. korelacje między jego mrowieniami a rzeczywistością. Dowiedział się, że ze wszystkich osób - 4 są niewrażliwe. Oni i Stefan.
Zenek zebrał ludzi w kafeterii. Tych co robią złe rzeczy. Dowiedział się. Pełne zaufanie.

_Wieczór: (+1 akcja O)_

Spotkali się po pracy w starym klubie z bardzo starymi automatami z grami typu Rambo. Klub "Kolt". Gdzieś po drodze do domu z pracy. Każdemu z nich jest po drodze.
Krzysztof powiedział Zenkowi i Ksenii, że chcą tą sprzątaczkę zwolnić. No i wyszło, że biuro widziało Stefana. A Stefana tam nie mogło być. Czyli... o co chodzi?
Gadali o Stefanie i wyszło im na to, że przyjdą rano do firmy i złapią Stefana przed czasem. Muszą. Dowiedzą się, co się dzieje i o co chodzi.
Mrowienie Krzysztofa. Znowu. Wyczuł obecność Krystalii na ławce. Ale olał.

_Noc: (+1 akcja O)_

Nic.

_Ranek: (+1 akcja O)_

Stefan przyszedł. Jak zdechły mops. Jest... taki... jak śnięta ryba. Nic nie ma. Ksenia straciła cierpliwość. Zdecydowała się na OPIERDOLENIE Stefana. To wzmacnia efemerydę. (+2 Erupcja Pióra, +2 Skandale +2 Femme). Sygnał poszedł do gabinetu szefa. Tak duży sygnał, że... Krzysztof czuje mrowienie TAM. W gabinecie szefa.
Polecieli do tego gabinetu. Tam szef nagi w samym garniaku i chce sobie nożem odciąć penisa. A Krzysztof zobaczył mignięcie Stefana stojącego za szefem, trzymającego rękę na jego ramieniu.
Jeszcze Krzysztof widzi, jak pióro faluje.
Zenek rzuca się na szefa by go zatrzymać przed odcięciem penisa. Szef zaczął ciąć. Zenek ofiarnie ochronił penisa szefa raniąc się w rękę. Krzysztof widzi, jak pióro pożera krew.
Oki, Zenek użył swojej mocy i zahipnotyzował szefa.
Zauważyli korelację między stanami emocjonalnymi a mrowieniem Zenka.
OKI! Trzeba budzić szefa. Wszyscy trzej nie chcą szefa resetować. Chcą coś z tego mieć.
Szef obudzony powiedział że chciał sobie odciąć bo szedł do łóżka z Jolantą a ona chciała skoczyć. Kiedyś. (+1 Femme trup).
Sukces Kseni. Wysadziła wszystko w pokoju Jolanty. Zenon uratował Jolantę przed skokiem z okna.
Szef nie pozwolił nikomu zbliżać się do biura. Dzień wolny. Zdalne dla wszystkich.
(+2 Krystalia)

_Południe: (+1 akcja O)_

Usiedli w knajpce niedaleko firmy. Gwiezdna Ośmiornica. Nie no, tak nie można - trzeba znaleźć Stefana i dowiedzieć się od niego co i jak - TERAZ. Poszli zadać mu jeszcze kilka pytań. Stefan jak to Stefan, mało reakcji. I wtedy Krzysztof używa swojego koralowca by Stefan zapomniał o Jolancie...
...koralowiec zasilany mocą pawiego pióra i uczucia złamanego serca skrzyżował się z duchem złamanego serca zasilanego mocą złamanego pióra.
Kość szczęścia: 20/1k20.
10 minut później postacie otwierają oczy. Stefan nie żyje od 2 tygodni czy coś. Koralowiec wyczyścił efemerydę i odkaził pióro. Krzysztofa boli głowa. Echo magiczne ściąga Krystalię.
Krystalia wpada, zalana, i krzyczy na nich że co oni niby robią a ona koralowców szuka. I znalazła.
Mała bitka; Krystalia pokonuje czystą siłą fizyczną trzech ludzi i wyciąga z nich rozkazem koralowce. Wypełzają przez uszy.
Krzysztof zjada swojego. Krystalia na niego krzyczy! Biedny koralowiec! Zenek swojego rozdeptuje. Krystalia panikuje. Ludzie panikują.
Ogólny chaos.

_Epilog_

Magowie naprawili sprawę. Wszyscy mają skasowaną pamięć, sprawa naprawiona. Za Stefana podłożono wyhodowanego człowieka. Budynek odbudowano, pióro podmieniono na coś innego - zwykłe, a nie magiczne repozytorium. Krystalia dostała opieprz, ale powiedziała, że w sumie dobrze wyszło, nie?

All is vain.

**Podsumowanie torów**

1. Duch Utraconej Nadziei Piżucha
    1. Femme Fatale zakochuje się w Stefanie Piżuchu: 1/10
    2. Femme Fatale popełnia samobójstwo: 7/10
    3. Skandale i impulsy emocjonalne: 4/10
    4. Struktura firmy (IT, reputacja, infra) się rozpada: 0/10
2. Naturalna Entropia
    1. Krystalia odnalazła swoje zaginione koralowce: 5/10
    2. Erupcja pawiego pióra: 4/10
    3. Zainteresowanie świata zewnętrznego: 2/10
    4. Ubóstwo, nadmierny wysiłek, wszystkie ręce na pokład: 0/10

**Interpretacja torów:**

No change. Krystalia naprawiła sprawę. A Diakoni naprawili po Krystalii.

**Przypomnienie Komplikacji i Skażenia**:



# Progresja

# Streszczenie

Pracownicy MaksymLaw uzyskali 'koralowce' które zgubiła Krystalia. Koralowce dały im ograniczone moce magiczne. Dzięki temu pracownicy dali radę rozwiązać problem magicznego pióra (generatora energii) i dziwnych wydarzeń docelowo prowadzących do przyszłej śmierci Jolanty - jednej z prawniczek MaksymLaw. Po tym jak owym pracownikom udało się unieczynnić efemerydę i rozładować emocjonalne pole magiczne, Krystalia znalazła swoje koralowce, zdobyła pióro i odzyskała koralowce. Niesprawiedliwość.

# Zasługi

* czł: Ksenia Armon, agresywna prawniczka, nakręcająca efemerydę i opierniczająca wszystkich. Łącznie z własnym nagim szefem. Chciała pobić Krystalię. Symbiont koralowca. Straciła pamięć.
* czł: Zenon Stecki, AM, sales, prawniczy. Doznał lekkiej rany ratując penisa szefa. Poświęca wszystko dla firmy w której pracuje. Uderzył Krystalię (bokser). Symbiont koralowca. Stracił pamięć.
* czł: Krzysztof Brakujowiec, najleniwszy IT guy prawniczy. Pozyskiwał informacje z monitoringu i mrowienia (bólu) od koralowca. Symbiont koralowca. Stracił pamięć.
* czł: Jolanta Karbon, zakochany był w niej Stefan, a ona spała z szefem. Zdobyła awans nad Zenona przez łóżko. Uratowana przed samobójstwem pod wpływem efemerydę.
* czł: Stefan Piżuch, sortownik listów w MaksymLaw. Zakochany w Jolancie a ona złamała mu serce. Zmarł przez stworzoną przez siebie efemerydę. Na tej sesji był już martwy. Ale chodził z listami.
* czł: Tomasz Klink, szef firmy rodzinnej MaksymLaw. Chce dobrze i opieprza ludzi za złe rzeczy; uratowany przez Zespół przed odcięciem sobie penisa pod wpływem efemerydy.
* czł: Jadwiga Opaszczyk, starsza wiekiem sprzątaczka, przez efemerydę tańczyła na stole. Zwolniona, ale nie zwolniona, bo wszyscy stracili pamięć.
* czł: Tony Armadillo, klient. Wzięty na kawę przez AMa Zenona i IT Guya Krzysztofa. Dzięki temu nic strasznego się nie stało i biznes nie padł.
* czł: Katarzyna Marszał, sekretarka. Parzy szefowi ulubioną herbatę. Chce dobrej atmosfery w firmie. Przez szefa pod wpływem magii skończyła we łzach i w panice. Uratowana przez Zespół.
* mag: Krystalia Diakon, degeneratka i narkomanka. Zgubiła 'koralowce' i próbowała je znaleźć. Przy okazji, ludzie symbiotyczni z koralowcami rozwiązali problem dziwnego źródła energii...


# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Czelimiński
                1. Jodłowiec, wraz z Kopalinem i Czeliminem tworzy trójkąt. Linia kolejowa do Czelimina. Obok niego stary, nie używany poligon.
                    1. MaksymLaw, firma prawnicza z 6x osobami. Specjalizuje się na rynku USA. Interes rodzinny.
                        1. Biuro, w którym działy się magiczne sceny dantejskie przez Umarłego z nieodwzajemnionej miłości i pawiego pióra - źródła energii.
                        1. Sortownia korespondencji, gdzie doszło do faktycznej erupcji magicznej i gdzie pracował Umarły.
                    1. Knajpka Gwiezdna Ośmiornica, blisko MaksymLaw, gdzie bierze się klientów gdy w biurze jest burdel.
                    1. Klub Kolt, z automatami starych gier. Dla koneserów i miłośników arcade. I cichej, dyskretnej rozmowy.

# Czas

* Opóźnienie: 50
* Dni: 2

# Frakcje


## Duch Utraconej Nadziei Piżucha

### Scena zwycięstwa:

* Femme Fatale zakochuje się w Piżuchu i zostaje odrzucona
* Femme Fatale popełnia samobójstwo i zasila energetycznie pióro
* Piżuch reanimuje się w jakimś stopniu

### Motywacje:

* Mieli cierpieć tak jak on.
* Zasilić się. Żywić się. Powrócić.

### Siły:

* Duch Piżucha
* Ciało Piżucha

### Ścieżki:

1. Duch Utraconej Nadziei Piżucha
    1. Femme Fatale zakochuje się w Stefanie Piżuchu: 0/10
    1. Femme Fatale popełnia samobójstwo: 0/10
    1. Skandale i impulsy emocjonalne: 0/10
    1. Struktura firmy (IT, reputacja, infra) się rozpada: 0/10


## Naturalna Entropia

### Scena zwycięstwa:

* brak

### Motywacje:

* brak

### Siły:



### Ścieżki:

1. Naturalna Entropia
    1. Krystalia odnalazła swoje zaginione koralowce: 0/10
    1. Erupcja pawiego pióra: 0/10
    1. Zainteresowanie świata zewnętrznego: 0/10
    1. Ubóstwo, nadmierny wysiłek, wszystkie ręce na pokład: 0/10


# Narzędzia MG

## Opis celu misji

* Za notatką: [Notatka_170716](/mechanika/notes/notatka_170716.html) zmieniamy kartę Pauliny.
* Konflikty są na (3, 5, 7, 10, 15). Każdy ma [1,3] dominujące aspekty negatywne, które zniwelowane obniżą stopień trudności o '2' każdy.
* Konflikty na [-1,1] (przegrane o 1, remis, wygrane o 1) są CZĘŚCIOWYMI sukcesami. Obie strony osiągają cel lub osiąga się cel niekompletny. To jest próba implementacji mechanizmu z Apocalypse World o równoległości działań postaci; alternatywne interesujące podejście jest w Mouse Guard.

## Cel misji

1. O-O-O-S-O-O-O-S, gdzie O to operacyjne (wzrost ścieżki) a S to komplikacje (nowa ścieżka).
    1. O: przeciwnik dał radę podnieść JAKĄŚ ścieżkę. Jeden lub wszyscy. Generowana jest nowy Postęp Przeciwnika.
    1. S: przeciwnik ma NOWĄ ścieżkę. Jeden lub wszyscy. +1 akcja dla wszystkich przeciwników. Generowana jest nowa Komplikacja Fabularna (wątek).
1. Dzień składa się z 4 faz, domyślnie. Normalne postacie w jednej fazie śpią ;-).
1. Nowy model postaci tempek pokaże, że lepiej jest opisana i ogólnie lepiej i szybciej działa.
1. Konflikty 3,5,7,10,15
1. Poziom częściowego sukcesu [-1,1]

## Po czym poznam sukces



## Wynik z perspektywy celu

1. Operacyjne komplikacje i brak Strategicznych
    1. Ż: Trzymam się tego chwilowo.
1. Cztery fazy dnia
    1. Ż: Znowu zadziałało. Acz Foczkowi nie pasuje 'sztuczność'.
1. Nowy model postaci
    1. Ż: Na razie działa.
    1. R: Mniejsza konfuzja niz zachwoania/motywacje, gdzie czasem mimo ze wiedzialem czym sie rozni jedno od drugiego trudno mi bylo wszystko od razu zdefiniowac. Slabe i silne strony, jako zwykle cechy zmieniajace rzeczywistosc i podatne na zmiane zewnetrzna sa bardziej czytelne dla mnie
1. Konflikty 3,5,7,10,15 i kontraspekty
    1. Ż: Overtuning. Musi być 0-2-4-6-9-15.
    1. K: Zgadzam się
1. Częściowy sukces
    1. Ż: 
    1. R: nie widze duzej roznicy od remisu
    1. K: 
1. Inne
    1. Ż: Aspekty. Gracze ich nie widzą. Nie używają. Nie widzą że mogą.

## Wykorzystane tabelki

Postać (strona gracza):

| .Motywacja. | .Siły i słabości. | .Specjalizacja. | .Umiejętność. | .Szkoła Magii. |.POSTAĆ. |
|             |                   |                 |               |                |         |

Opozycja (strona NPC):

| .VERSUS. | .Aspekty. | .Skala. | .Magia. |.Pomysł gracza. | .OPOZYCJA. |
|          |           |         |         |                |            |

Wynik:

| .Postać. | .Opozycja. | .Rzut.    | .Wynik.  |
|          |            | xx: +x Wx |   S-SS-F |

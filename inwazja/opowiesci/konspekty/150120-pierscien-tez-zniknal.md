---
layout: inwazja-konspekt
title:  "Pierścień też zniknął"
campaign: anulowane
gm: żółw
players: kić, agata_n, grzegorz
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [141210 - Złodzieje kielicha w akcji (APł, APu)](141210-zlodzieje-kielicha-w-akcji.html)

### Chronologiczna

* [141210 - Złodzieje kielicha w akcji (APł, APu)](141210-zlodzieje-kielicha-w-akcji.html)

## Punkt zerowy:

Ż - Kim z zawodu jest Olga Pierwiosnek?
A - Pracuje w cukierni.
Ż - Dlaczego zniknięcie niemagicznego pierścionka zaręczynowego w lesie było dla mistrza Archibalda ważniejsze niż znalezienie kielicha i gitary?
G - Bo to był pierścionek mistrza Archibalda przeznaczony dla innego maga i mistrz Archibald chciał mu go oddać; teraz pojawiło się niebezpieczeństwo że mag przybędzie po pierścionek.
Ż - Czemu mistrz Archibald tak bardzo interesuje się poczynaniami Janiny Kielich?
A - Podejrzewa, że może coś wiedzieć na temat pierścionka.
Ż - Dlaczego właśnie Aleksandra była najlepszą osobą do odzyskania samodzielnie kielicha?
G - Bo z uwagi na swoje znajomości ze sklepu z warzywami zna wszystkie odpowiednie osoby.
K->Ż - Co Janina robiła koło dawnego domostwa Archibalda (Archiville)?
Ż - Szukała, jak dostać się do środka niezauważenie.
A->Ż - Jaka jest relacja między Olgą a Janiną?
Ż - Olga jest zazdrosna o urodę i młodość Janiny, Janina jest zazdrosna o powodzenie Olgi.
G->Ż - Co robiła Olga w jedynej firmie informatycznej w Ptaczewie w czasie awarii internetu?
Ż - W szpiegowski sposób próbowała wynieść USB w ciastku.
Ż - Co jest szczególnego w tej firmie informatycznej w Ptaczewie?
K - Założona przez grupkę pasjonatów, załapała się na krzywy ryj na państwowy projekt.
Ż - Scharakteryzuj mi osobę z "Cekina" (firma inf.) podkochującego się w Janinie?
G - "Będę grał w grę".
B->A - Co o pierścieniu sądzi że wie lokalny zwolennik teorii spiskowych? 
A - Janusz, organista, dochodzi do wniosku, że Archibald ma kochankę której chce się oświadczyć tym pierścieniem.
B->G - I dlaczego w całkiem niespodziewany przez niego sposób ma rację?
G - Dlatego, że mag który chce zdobyć pierścień nie jest mężczyzną - to czarodziejka. I to czarodziejka która kiedyś była eks- Archibalda.

## Misja właściwa:

Spotkanie na basenie. Mistrz taki bardzo smutny; powiedział, że potrzebny jest mu bardzo ważny pierścień i że widział podobny u Olgi (pracującej w cukierni). Trochę kręci, nie chce powiedzieć czemu potrzebny mu pierścionek zaręczynowy. Przyciśnięty przez Adama (10v3, auto sukces), przyznaje się, że jego eks skontaktowała się z nim, chcąc odzyskać pierścień, który dała mu na przechowanie. Problem w tym, że pierścień był w domu Archibalda. Był? No, był. Archibald widział jednak podobny u Olgi, kiedy kupował ciastko.

Zespół postanowił pójść do cukierni Gąska. Plan zespołu dostał po paszczy, kiedy okazało się, że w cukierni jest Marian a nie Olga. Zapytany, Marian powiedział że Olga wzięła urlop. Adam spróbował wejść na fejsika Olgi i zdobyć zdjęcia Olgi z pierścionkiem oraz jej adres. (10v7->13). Udało mu się bez zwracania uwagi chłopaka podkochującego się w Oldze (nie będzie miał śledzia)
Nie tylko znalazł poszukiwane zdjęcia, ale jeszcze wyszukał, że ta ma jamnika. 
Co więcej, na zdjęciach z pierścionkiem Olga była razem z jakimś facetem, cała uśmiechnięta.
Przez chwilę Miranda i Adam myśleli, jak odebrać dziewczynie pierścionek, ale sumienie ich ruszyło i postanowili wkręcić mistrza. Co teraz? Allegro i jubiler.

Pomimo obrażeń (poparzenia od srebra) Anna jest w sklepie (ale w rękawiczkach). Zapytana, czy można zrobić pierścień taki sam, jak na zdjęciu, spoważniała, przyjrzała się, poszła na chwilę na zaplecze, po czym wróciła oznajmiając, że zrobienie takiego pierścienia jest niemożliwe. (Jej zdaniem padli ofiarą iluzji optycznej - nie da się zrobić czegoś takiego bo przy zaciśnięciu pięści pierścień się rozpadnie). Dopytali się, ile by kosztowało zrobienie czegoś takiego ale realnego. Powiedziała, że koło 500 - 1000 zł.

Side note: część sygnetowa ma wyryte ABET jedno na drugim (monogram)

Czyli... pierścień niemożliwy, czyli magiczny? Ale moment, mistrz mówił, że nie jest magiczny. Ale on nie może być niemagiczny...  O co chodzi? Poszli więc do mistrza Archibalda. Ten zdumiał się niepomiernie, gdy okazało się, że to jest JEGO pierścień. Ale jak pierścień wybył z domu? Ostatnim razem, kiedy mistrz próbował tam wejść, prawie zginął. Od marnej śmierci uratowała go Aleksandra...
A czemu nie powiedział że pierścień powstał w magiczny sposób? Bo nie wiedział, że ludzie nie mają materiałów tak twardych by taki pierścień mógł zaistnieć. Zwyczajnie nie pomyślał - jest magiem, nie człowiekiem, nie zna się na metalurgii i jubilerstwie.

Z gorszych rzeczy: jeśli Elżbieta (eks) nie dostanie pierścienia, może się tu pojawić. A jeśli dowie się, że on nie ma mocy, może mu odebrać uczniów...
Po chwili wzajemnego szantażu emocjonalnego do niczego nie doszli, więc postanowili iść dalej.
Pierścień został zakupiony w Kopalinie, zrobiony na zamówienie. Wyhodował go Melanin Diakon.

Zespołowi przez chwilę przyszło do głowy, że może by kupić nowy, ale uznali, że marne szanse, żeby to przeszło. Adam i Miranda starli się w pojedynku sił. Miranda chce zrobić replikę, podmienić pierścień i pozwolić Oldze dalej być zakochaną bez dodatkowych ingerencji magicznych. Adam chce kupić najtańszy pierścień, podmienić go Oldze, wyczyścić jej pamięć, po czym dorwać jej narzeczonego i jemu również zmodyfikować pamięć. Adam przekonał Mirandę mówiąc, że tak będzie szybciej, co redukuje szansę, że Elżbieta się tu pojawi i dodatkowo w ten sposób usuwają ślady mogące zwrócić uwagę magów. Miranda utargowała, że nie wezmą najtańszego pierścionka tylko odpowiedni dla tej klasy faceta.

Chcąc sprawdzić czy Olga jest w domu Adam użył mocy magicznej i włączył jej komputer. Następnie przejął kontrolę nad kamerką. Nikogo w domu nie ma, ale widać ślady drastycznego szukania. 
Miranda chce sprawdzić magicznie, jakie emocje towarzyszyły przeszukaniu. Chce dowiedzieć się dlaczego taki bałagan, czy ktoś szukał, dlaczego, jakie emocje itd. Adam szuka psa (jamnik o imieniu Zuzia), ale nie ma go w domu. Miranda rzuca zaklęcie (12v7/9/11->11)

Pierwsze jej odczucia to to, że Olga była owładnięta radością, euforią itd z bardzo delikatną "czarną" nutką, po czym strach, rozpacz i panika. Żeby zidentyfikować tą "czarną nitkę" musieli wejść do domu, gdzie Miranda mogła wybadać, co to jest. Okazało się, że owa miłość odczuwana przez Olgę jest wyindukowana; to coś wypala jej wolę i coraz bardziej ją opętuje. Niestety, Zespół został przyłapany przez policjanta, który dostał zgłoszenie od Olgi o zaginięciu pierścienia i szedł do niej do domu dowiedzieć się o co chodzi.

Adam wyskoczył do policjanta z tekstem "Geniuszu, jak myślisz, czy mogliśmy to zrobić?" Ten się z lekka nadął i oburzył, ale sytuację uratowała Miranda odgrywając totalnie nawiedzoną maniaczkę i medium. (8 vs. 11 -> 10) Policjant uznał, że nie warto babrać się z zamykaniem tej dwójki. Zespół aktualnie uznaje, że Olga jest ofiarą.

Kiedy udało im się opuścić dom, Adam zabrał się za grzebanie w sieci, żeby szukać informacji o narzeczonym. (9vs. ? -> 9, tak/nie, ale) Facet to typowy cebulak, któremu ostatnio wyjątkowo ostatnio zaczęło się powodzić (każda jego, ma kasę etc.) Pierścień znalazł, nie ukradł. Adamowi udało się również dowiedzieć, że facet znalazł nie tylko pierścień. Zespół dochodzi do wniosku, że z rezydencji przedmioty znikają same z siebie. Facet nazwa się Mirosław Cebula.
W ramach "tak, ale" podkochujący się w Oldze informatyk dowiedział się, że Adam szperał w jej fejsiku. Oczywiście dowiedział się, bo on TEŻ szperał w jej fejsiku...

Co dalej? Czas pokaże.

# Zasługi

- mag: Adam Płatek, infomanta lubiący mieszać ludziom w fejsiku i wchodzić innym ludziom do głowy; o nieco niewyparzonym języku.
- mag: Miranda Delf, astralistka i performerka grająca nawiedzoną ofiarę policyjnego reżimu buntująca się przeciw mistrzowi Archibaldowi.
- czł: Archibald Bankierz, mistrz trójki uczniów którego dogoniła kłopotliwa przeszłość z eks- oraz zaginionym pierścieniem.
- czł: Olga Pierwiosnek, cukierniczka nosząca pierścionek Archibalda której coś próbuje odebrać wolę.
- czł: Marian Jogurt, normalnie nie pracujący w cukierni ale poproszony przez Olgę o zastępstwo na czas jej awaryjnego urlopu.
- czł: Aneta Patyczek, jubiler w rękawiczkach, która bezbłędnie rozpoznaje niemożliwy na pierścieniu wzór.
- czł: Mariusz Błyszczyk, lokalny policjant który nie rozumie nowoczesnej sztuki i performance'u oraz nie lubi jak się go traktuje z wyższością.
- czł: Mirosław Cebula, który znalazł kilka artefaktów i zaczęło mu się powodzić.


# Lokalizacje:


1. Ptaczewo
    1. Archiville
        1. domek dla służby
    1. Cukiernia
    1. Firma informatyczna "Cekin"
    1. Basen, centrum spotkań gildii...
    1. Sklep jubilerski Anny Patyczek

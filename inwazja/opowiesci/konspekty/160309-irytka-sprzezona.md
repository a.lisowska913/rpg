---
layout: inwazja-konspekt
title:  "Irytka Sprzężona"
campaign: powrot-karradraela
gm: żółw
players: kić, dust, dzióbek
categories: inwazja, konspekt
---

# {{ page.title }}

## Wątki

- Szlachta vs Kurtyna
- Powrót Mausów
- Irytka Sprzężona

## Kontynuacja

### Kampanijna

* [170321 - Tajemnica podłożonej świni (temp)](170321-tajemnica-podlozonej-swini.html)

### Chronologiczna

* [160224 - Aniołki Marcelina (LB, KB)](160224-aniolki-marcelina.html)
* [160303 - Otton zabija Zetę (AB, DK)](160303-otton-zabija-zete.html)

### Logiczna

* [151212 - Antyporadnik wędkarza Arazille (HB, AB, DK)](151212-antyporadnik-wedkarza-arazille.html)

## Kontekst misji

- Ktoś napuszcza na siebie Świecę i KADEM.
- Ktoś napuszcza na siebie Kurtynę i Szlachtę.
- Anna Kozak winna oddać artefakty Andrei.
- Zenon Weiner zniknie z pola widzenia w drugiej części misji.
- Świeca jest zajęta problematyką Arazille, zwłaszcza Diakoni.
- Ozydiusz Bankierz pracuje nad cruentus reverto.
- Siedmiu magów czeka straszny los; Szlachta oddała ich Oktawianowi Mausowi by przekształcić w Mausów.
- Milena Diakon widziała Spustoszenie (?). Skąd i jak?
- Baltazar Maus będzie poświęcony na ołtarzu Rodziny Świecy i bezpieczeństwa (nie w tej misji).
- Pancerz Tamary i "kołysanka". Powiązanie ze Spustoszeniem?
- Działania archeologiczne Dominika Bankierza pod uważnym okiem Inkwizytor Adeli Maus.
- Oktawian, Adela Mausowie i Dominik Bankierz zniknęli lub zginęli na wykopaliskach
- Andrea i oddział Tamary skutecznie odbili magów, których Oktawian chciał zmienić w Mausów

## Kontekst ogólny sytuacji

- Emilia rzuciła bombę - magitech Wiktora ma przydatną wiedzę, klucz do zniszczenia Szlachty.
- Wiktor Sowiński i Diana Weiner dalej chcą współpracować z Hektorem. Wiktor dalej interesuje się Silurią.
- Laboratoria Blakenbauerów mogą być wykorzystane przez Szlachtę za zgodą Hektora i z planami dla Blakenbauerów.
- Emilia skonsolidowała siły Kurtyny dookoła siebie
- Blakenbauerowie wiedzą o obecności Tymotheusa w Kopalinie; Edwin "szuka" Tymotheusa.
- Blakenbauerowie są powiązani ze Szlachtą bardziej niż z Kurtyną.
- Hektor wniósł oskarżenie przeciwko Vladlenie o próbę Spustoszenia hipernetu.
- Srebrna Świeca poluje na Arazille, która zeżarła Zieloną Kariatydę ośmieszając Ozydiusza
- Olga ma dostęp do danych ze Skorpiona i może wykorzystać siły specjalne Hektora do rozwiązania mrocznych aktywności.
- Diakoni zablokowani przez Mojrę; silny sceptycyzm co do działań Blakenbauerów (thanks, Romeo).

## Punkt zerowy:

Ż: Jaki korzystny projekt robi tien Karol Poczciwiec w którym chce pomóc Diana?
K: Artefakt wspomagający hydroponikę roślin magów i ogólnie rozumianej żywności. 
Ż: Istotna lokalizacja strategicznie w Kopalinie (i czemu jest ważna dla magów i Szlachty i Kurtyny)?
B: Lokalne muzeum; jedna z wystaw z uwagi na srebrne i lapisowane wystawy. Takie Victoria&Albert museum.
Ż: W jakim miejscu w Kopalinie odbyła się największa spektakularna walka Szlachty i Kurtyny?
D: Plac budowy dużego obiektu sportowego. Duża inwestycja, dużo ludzi, przed kamerami... ogólnie WTF.

## Misja właściwa:

1. Hektor odkrywa Judytę Maus w podziemiach.

Parę dni temu...

- 0 - Problem Judyty Maus.
- 0 - Informacja o Kariatydzie / Arazille i o przeżyciu Oktawiana (ale w katatonii po wydarzeniach na wykopaliskach; nie ma świadków, Adela Maus zniknęła i Dominik Bankierz też).

Hektor podczas uruchamiania Rezydencji siedział w piwnicy pilnować płaszczek. Usłyszał pukanie z głębi i "halo"? sympatycznym, kobiecym głosem. Judyta Maus zapytała, czy ma może do czynienia z płaszczką...

2. Dowiaduje się od Leonidasa, że Judyta jest zakładniczką oddaną przez Oktawiana za wykonanie zadania. Hektor nie ma pojęcia, o co chodzi
3. Judyta upiera się, że kocha Hektora i za niego wyjdzie. Hektor nie jest entuzjastycznie nastawiony
4. Po wyjaśnieniu sobie sprawy w rodzinnym kręgu, Leonidas ratuje Hektora pierogami (ze szpinakiem) gwałtu. Judyta ląduje w stazie. Jest uciążliwa.

Dzień 1:

Edwin powiedział o aktualnej sytuacji:

- 1 - Estrella przekazuje Joachimowi wiadomość od Wandy.
- 1 - Ozydiusz pakuje mnóstwo energii w Neutralizator, chcąc go uruchomić
- Diana wspiera tien Karola Poczciwca w projekcie hydroponiki roślin i efektywnej konstrukcji żywności
- Mag Kurtyny, Rufus Czubek, wpadł na Karola Poczciwca i spalił projekt w którym pomagała Diana.

Porozmawiali wszyscy odnośnie problemu Judyty Maus. Jako, że Oktawian jest nieprzytomny a niedługo małżeństwo polityczne, pojawia się potencjalny problem. Edwin nie chce łączyć Mausówny z Rezydencją; boi się, że to pułapka i wpuszczenie Karradraela. Stanęło na tym, że zostaje w Rezydencji. W stazie. W końcu nie ma niczego problematycznego...
Edwin jeszcze dodał, że Aliny i Dionizego Arazille nie dotknęła tak jak Wandy; Edwin ich odbudowuje i regeneruje.

5. Podczas dyskusji o tym, co robić dalej padła decyzja, aby spróbować postawić Oktawiana. W końcu Edwin jest lekarzem. Ale do tego trzeba wejść do Mausów. Wniosek? Lojalna Oktawianowi Judyta nas tam wprowadzi... Znaczy, trzeba obudzić.

Dzień 2:

- 1 - Skubny zbiera dowody przeciwko Hektorowi.
- Pierwsze naruszenia Maskarady
- Szlachta boryka się ze złamaniem Maskarady.
- Kurtyna boryka się ze złamaniem Maskarady.

6. Klara jako jedyna, która jescze nie zaatakowała Judyty w żaden sposób budzi ją i wyjaśnia sytuację (Judyta nie ma pojęcia o ostatnich wydarzeniach) Judyta daje się łatwo przekoniać, by ratować Oktawiana.
7. Wszyscy wsiadają w limuzynę i jadą do siedziby Mausów.
8. Po drodze Klara i Leonidas wykrywają zaklęcie rzucone w środku miasta, w oczywisty sposób łamiące Maskaradę. Klara uziemia zaklęcie, Leonidas przygotowywał drogę ewakuacji. Klara wskazuje źródło czaru - jest nim jeden z trójki magów kłócących się w pobliżu. 
9. Hektor wyszedł ich aresztować, a Leonidas na to "Janie, jedziemy"

"Janie, jedziemy" - Dzióbek
"Cóż złego może się stać" - Dzióbek.

10. Hektor - po drobnej pyskówce - zamknął trójkę magów i kataliści Świecy potwierdzili, że to oni rzucali te czary, ale sami magowie nie mają pojęcia, że to robili
11. W tym czasie reszta dociera do siedziby Mausów, a Edwin spotyka Eleę. Wspólnie badają Oktawiana, dochodząc do wniosku, że jego tam nie ma; brakuje "duszy" i osoby w ciele.

- Szersze naruszenia Maskarady
- 2 - Joachim wysadza 1 kg puszek surstrumminga w prokuraturze.
- Szlachta brutalnie wchodzi do Muzeum, pokazując, że oni faktycznie zachowują porządek (tien Dagmara Wyjątek)
- Akcja odwetowa Kurtyny przeciwko Szlachcie w Muzeum; Kurtyna zostaje zmasakrowana i wielu magów Kurtyny trafia do niewoli.

12. W tym czasie Leonidas uczy Judytę bezużytecznego i złożonego rytuału, którego jedynym celem jest danie dziewczynie zajęcia. Rytuał nie robi nic.
13. Oprócz zajęć dydaktycznych, na prośbę Elei Leonidas sprawdza zgłoszenia o licznych złamaniach Maskarady w okolicy Kopalina.
14. Zebrane informacje prowadzą Eleę do wniosku, że to epidemia irytki, choroby magicznej. Niestety, jest to zmodyfikowana forma. Na razie jednak Elea i Edwin rozdali leki i zabezpieczenia przed irytką ("no to taki katar...")
15. Hektor dostał telefon od swojej asystentki Kingi, że Skubny jest w prokuraturze i chce się przyznać. Ucieszony, poleciał czym prędzej tylko po to, żeby załapać na 50 wybuchających puszek surstrominga. Stracił przytomność zanim się rozbestwił.
16. Rodzinka pojechała razem z Judytą ratować Hektora; Borys zadzwonił do Edwina i powiedział o sytuacji. Sam Borys obudził Hektora i zrobił mu usta-usta.
17. Po kąpieli (i pobiciu demona czyściciela wezwanego przez Judytę) Hektor poprosił Klarę, żeby sprawdziła źródło połączenia. Efekt: zadzwoniła do Hektora nie Kinga... a Joachim Zajcew.

Hektora obudziło usta usta Borysa.
"Mam demona który Cię dobrze wyliże"

- 2 - Anna Kajak informuje Hektora, że Patrycja jest zastraszona przez nieznanych sprawców. Kleofas podejrzewa pułapkę; nie chce ruszać przeciwko Skubnemu.
- Walki Szlachta - Kurtyna
- Pojawiają się potwory i manifestacje Maskarady; Szlachta używając artefaktów próbuje to zatrzymać.
- Kurtyna wypchnięta. Tracą wszystkie obszary; Ozydiusz ratuje ich przed zagładą przy placu budowy Stadionu Olimpijskiego.

18. Jako, że nikomu nie leży w interesie, by Szlachta była w Muzeum, Leonidas skontaktował się (pośrednio) z narwanym Zajcewem z Kurtyny i sprzedał mu pomysł o użyciu surstromminga by magów Szlachty stamtąd wypłoszyć.
19. Zajcew to zrobił. Muzeum znowu jest "ziemią niczyją". Jako miejsce jest dość bezpieczne przed czarowaniem (lapis i srebro).
20. Po powrocie do rezydencji usłyszeli o kwarantannie i wezwaniu lekarzy. Leonidas zorganizował "marketing impotencyjny", a Hektor zastanawia się czy wybrać Szlachtę czy Kurtynę.
21. Edwin potwierdził, że ta Irytka jest bronią biologiczną. Nie jest łatwa do wyleczenia jak zwykła. Nie działają normalne metody. Najlepiej nie czarować.
22. By zapobiec problemom, Leonidas ułożył plan; zrobili filmik informacyjny i dodali fałszywy fakt: ta NOWA irytka powoduje impotencję nieleczalną magicznie. W ten sposób narwańcy powinni przestać czarować a konflikt między magami powinien się trochę osłabić.

"Marketing impotencyjny"

- Ozydiusz chroni teren Neutralizatorem, tworząc "safe spaces" przed irytką.
- Szlachta wspiera nowych członków i ogólnie magów; pomagając im zakończyć poprzednie działania. PR # działania (tien Bolesław Derwisz).
- Emilia prosi Blakenbauerów o wsparcie militarne; lub choć polityczne.
- Rekwizycja Edwina

23. Ozydiusz stworzył obszar chroniony przed irytką; użył pola zerowego generowanego przez Neutralizator by wyłączyć możliwość irytki i uniemożliwić czarowanie. Desygnował do tego technopark "M.Weber".
24. Ozydiusz zażądał, by wszyscy lekarze odpowiedniej klasy się stawili u niego. Zarządził kwarantannę. I ogólnie powiedział, że Świeca będzie pilnować Maskarady i porządku w Kopalinie używając artefaktów itp. 
25. Z Hektorem skontaktowała się Emilia i powiedziała mu, że Hektor jest teraz kingmaker dzięki temu, że w Kopalinie ma najwięcej "płaszczek". On jest w stanie zamknąć wojnę Szlachta / Kurtyna. Powiedziała, że Kurtyna została ciężko uszkodzona i Emilia oczekuje publicznej deklaracji Hektora po której stronie stoi ród Blakenbauerów...

## Wynik misji:



## Dark Future:

Linia Irytki (Plagi):
- nic
- Pierwsze naruszenia Maskarady
- Szersze naruszenia Maskarady
- Walki Szlachta - Kurtyna
- Zwiększenie irytacji i ogólnej złości
- Identyfikacja choroby
- Rekwizycja Edwina
- Pierwsze magiczne lekarstwa
- Lekarstwa nie działają
- Badania Irytki; to coś szerszego
- Druga fala lekarstw
- Lekarstwa nie działają

Linia Ambient: (buy)
- 0 - Problem Judyty Maus.
- 0 - Informacja o Kariatydzie / Arazille i o przeżyciu Oktawiana.
- 1 - Estrella przekazuje Joachimowi wiadomość od Wandy.
- 1 - Ozydiusz pakuje mnóstwo energii w Neutralizator, chcąc go uruchomić
- 1 - Skubny zbiera dowody przeciwko Hektorowi.
- 2 - Joachim wysadza 1 kg puszek surstrumminga w prokuraturze.
- 2 - Anna Kajak informuje Hektora, że Patrycja jest zastraszona przez nieznanych sprawców. Kleofas podejrzewa pułapkę; nie chce ruszać przeciwko Skubnemu.
- 3 - Netheria ogłasza wielki koncert ku czci Wandy Ketran. 
- 2 - Ozydiusz odpala Neutralizator prosto w rejon najcięższych walk, obezwładniając ich kompletnie

Linia Szlachty:
- Diana wspiera tien Karola Poczciwca w X
- Szlachta boryka się ze złamaniem Maskarady.
- Szlachta brutalnie wchodzi w obszar X, pokazując, że oni faktycznie zachowują porządek (tien Dagmara Wyjątek)
- Pojawiają się potwory i manifestacje Maskarady; Szlachta używając artefaktów próbuje to zatrzymać.
- Szlachta wspiera nowych członków Szlachty; pomagając im zakończyć poprzednie działania. PR # działania (tien Bolesław Derwisz).
- tien Wyjątek dowodzi operacją mającą pozbawić Kurtynę środków; skutecznie. Odcięła supply line.
- Szlachta fortyfikuje się w X, skutecznie.
- Diana prosi Hektora o wsparcie do zniszczenia bazy Emilii; agenci Szlachty atakują Emilię w jej własnej bazie; zostają anihilowani w całkowicie korzystnej dla siebie sytuacji.
- Szlachta robi katastrofalny błąd atakując bazę Emilii; nikogo tam nie ma poza Polem Statycznym.

Linia Kurtyny:
- Mag Kurtyny, Rufus Czubek, wpadł na Karola Poczciwca i spalił projekt w którym pomagała Diana.
- Kurtyna boryka się ze złamaniem Maskarady.
- Akcja odwetowa Kurtyny przeciwko Szlachcie w X; Kurtyna zostaje zmasakrowana i magowie Kurtyny trafiają do niewoli.
- Kurtyna wypchnięta. Tracą wszystkie obszary; Ozydiusz ratuje ich przed zagładą przy X1.
- Emilia prosi Blakenbauerów o wsparcie militarne; lub choć polityczne.
- Kurtyna fortyfikuje się w bazie Emilii.
- Emilia odcięła Szlachcie wszystkie posterunki które sukcesywnie budowali poza X.
- Dom Emilii płonie i jest bardzo uszkodzony. Systemy zabezpieczeń są spalone.
- Kurtyna łapie Dianę Weiner w niewolę.

# Zasługi

* mag: Hektor Blakenbauer, który aresztował chorych na Irytkę * magów za czyn, który popełnili nieświadomie. Potem nawąchał się surstrumminga i padł.
* mag: Leonidas Blakenbauer, nokautujący szpinakowym pierogiem Judytę, porzucający Hektora limuzyną i straszący * magów impotencją by opanować epidemię Irytki.
* mag: Klara Blakenbauer, wskazująca Joachima Hektorowi i 'handler' Judyty; jedna z nielicznych opanowujących Judytę bez problemu.
* mag: Edwin Blakenbauer, który przyznał się do błędu w sprawie Wandy, nie może pomóc Oktawianowi, rozpoznał Irytkę Sprzężoną i waha się, czy iść pomóc Ozydiuszowi.
* mag: Margaret Blakenbauer, która służy jako łącznik komunikacyjny i zarządza tematami w Rezydencji w czasie, gdy reszta się rozbija po mieście.
* mag: Diana Weiner, która po* maga * magom Świecy w imieniu Szlachty i zamiast walczyć, buduje zaufanie do swojej frakcji.
* mag: Karol Poczciwiec, artefaktor, któremu narwany * członek Kurtyny zniszczył pozytywny projekt dla Świecy nad którym pracował ze Szlachtą.
* mag: Rufus Czubek, który przez czyste narwaństwo zniszczył Poczciwcowi bardzo cenny i pożyteczny artefakt tylko dlatego, bo po* magała przy nim Diana.
* mag: Judyta Maus, sympatyczna i naiwna "demonolog 3 klasy", która jednocześnie jest bardzo narzucająca się i wy* maga ogromnej uwagi. Bardzo chce pomóc. Niestety.
* mag: Elea Maus, lekarz * magiczny; zna Edwina i wraz z nim próbowała opracować coś do zwalczenia Irytki Sprzężonej.
* mag: Oktawian Maus, nieprzytomny, nieobecny, ogólnie: "nie". Mausowie się nim zajmują i próbują doprowadzić do dobrej formy.
* mag: Emilia Kołatka, która poniosła ogromne straty jako przywódca Kurtyny. Zwróciła się do Hektora o wsparcie polityczne i militarne.
* mag: Joachim Zajcew, ten, który boleje nad stratą Wandy Ketran i w ramach zemsty wysadził surstromming w prokuraturze by Hektor ucierpiał.
* mag: Bolesław Derwisz, który z ramienia Szlachty wspierał działania po* magające * magom Świecy w obliczu Irytki Sprzężonej.
* mag: Dagmara Wyjątek, oficer taktyczny Szlachty, która zdobyła i prawie okopała się w Muzeum. Niestety dla niej, jej plany zostały zniwelowane sustrommingiem.
* mag: Ozydiusz Bankierz, lord terminus Kopalina, który próbuje utrzymać porządek w ryzach po utracie Kariatydy do Arazille i w obliczu wybuchu epidemii dziwnej Irytki Sprzężonej.
* czł: Anna Kajak, która wykryła atak tajemniczych sił na dzieci Patrycji; poprosiła Hektora o autoryzację akcji.
* czł: Kleofas Bór, który wiedząc o ataku na dzieci Patrycji poprosił Hektora o NIE autoryzowaniu akcji Anny; uważa, że to wszystko jest pułapką.
* czł: Kinga Melit, niewinna asystentka Hektora od której PODOBNO poszedł sygnał wzywający prokuratora do prokuratury. A to był Joachim Zajcew.
* czł: Patrycja Krowiowska, której dzieci zostały zagrożone przez "trzecią stronę" z niewiadomego powodu.

# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Centrum
                        1. Technopark Max Weber
                        1. Wielkie Muzeum Śląskie
                        1. Prokuratura
                    1. Obrzeża
                        1. Dzielnica Mausów
                            1. Przychodnia 11
                        1. Stadion Olimpijski (plac budowy)
                        1. Rezydencja Blakenbauerów
                            1. Podziemia Rezydencji

# Didaskalia

- obj: Irytka Sprzężona, która okazała się być dużo bardziej niebezpiecznym i skomplikowanym bytem, bronią biologiczną stworzoną przez kogoś. Co najmniej 3 lata pracy i adaptacji.
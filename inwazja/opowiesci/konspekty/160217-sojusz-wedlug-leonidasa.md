---
layout: inwazja-konspekt
title:  "Sojusz według Leonidasa"
campaign: powrot-karradraela
gm: żółw
players: kić, dust, dzióbek
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [160210 - Batmag uderza! (HB, KB, LB)](160210-batmag-uderza.html)

### Chronologiczna

* [160210 - Batmag uderza! (HB, KB, LB)](160210-batmag-uderza.html)

### Logiczna

* [150920 - Sprawa Baltazara Mausa (AW)](150920-sprawa-baltazara-mausa.html)

## Wątek główny:
"Spustoszenie"

## Punkt zerowy:

Ż: Dlaczego Leonidas jest najbezpieczniejszym wyborem politycznym na nie-jasną (znaczy czarną) misję dla Edwina?
B: Bo nie zaprotestuje.
Ż: Jakie przesłanki wskazują na powiązania Ozydiusza Bankierza ze Spustoszeniem?
K: Ozydiusz zadaje bardzo dziwne pytania i ruchy wskazują, że wie więcej niż powinien.
Ż: Co łączy Dalię i Dianę; czemu problemy Dalii tak bardzo mocno promieniują na Dianę?
D: Dalia w kłopotach to Szlachta w kłopotach (mimo, że Dalia nie jest członkiem Szlachty)
Ż: Co - gdyby Dalia wpadła w kłopoty - szczególnie mocno by osłabiło pozycję Diany?
B: Diana coś użyczyła Dalii. Gdyby Dalia wpadła w kłopoty z tym "czymś" czego nie powinna mieć...
Ż: I jak Blakenbauerowie mogą całą tą sprawę uratować (czemu szczególnie oni)?
K: Bo są w tej chwili dość "neutralni" i nie powiązani politycznie.
Ż: Skąd wiadomo, że Oktawianowi Mausowi można zaufać w tej kwestii?
D: Dał coś bardzo cennego Blakenbauerom; jeśli zdradzi to to wpadnie w ich ręce.
Ż: Co to jest "coś szczególnie cennego"?
B: Mało ważny Maus czystej krwi (zakładnik).
Ż: Czemu jest on cenny dla Rodu Maus?
K: Jest to osoba, która sama w sobie nie ma wpływu ani mocy politycznej, ale stanowi zasadny mariaż (i jest umówiony na ślub).
Ż: Jak ma na imię?
D: Judyta.


## Kontekst misji:

- Wróciła Emilia i rzuciła bombę - magitech Wiktora ma przydatną wiedzę, klucz do zniszczenia Szlachty.
- Wiktor Sowiński i Diana Weiner dalej chcą współpracować z Hektorem. Wiktor dalej interesuje się Silurią.
- Laboratoria Blakenbauerów mogą być wykorzystane przez Szlachtę za zgodą Hektora i z planami dla Blakenbauerów.
- Emilia skonsolidowała siły Kurtyny dookoła siebie
- Blakenbauerowie wiedzą o obecności Tymotheusa w Kopalinie; Edwin szuka Tymotheusa.
- Mojra dała znać o Arazille Srebrnej Świecy.
- Siluria została pobita przez Ignata i wyłączona z akcji.
- Hektor wniósł oskarżenie przeciwko Vladlenie o próbę Spustoszenia hipernetu.
- Zajcewowie wyzajcewowali Blakenbauerów przez Tatianę, która potem została przez Hektora upokorzona.
- tien Adrian Murarz jest w Rezydencji Blakenbauerów; pomaga jak może.
- Olga ma dostęp do danych ze Skorpiona i może wykorzystać siły specjalne Hektora do rozwiązania mrocznych aktywności.
- Silny chaos na poziomie Świecy a na pewno na poziomie Diakonów.
- Szymon Skubny i Igor Daczyn siedzą sobie tymczasowo w areszcie.

## Poprzednie spekulacje

Magitech - najpewniej Spustoszony. Hektor musi wziąć to pod uwagę. To byłoby ciekawe.
Emilia - najpewniej nie rozmawiała z Tamarą a z Saith Kameleon. Bardzo w stylu Agresta.
Szlachta dowiadując się o spotkaniu Silurii / Emilii utrudni relację z Wiktorem. Albo nawet w drugą stronę, Szlachta na pewno dowie się o tej rozmowie, więc Siluria tym bardziej udaje, że próbuje wykorzystać Emilię na korzyść Szlachty. Czyli Szlachta ma wierzyć, że Siluria próbuje zadurzyć w sobie Emilię - jako cel do Wiktora.
Najpewniej po akcie oskarżenia magowie Kurtyny zrezygnują ze współpracy z Hektorem i przejmie ich Emilia.
Siluria docelowo będzie chciała działać przeciwko Arazille; żeby Arazille nie było w okolicy.

Plany na przyszłość: 
- wyciągnięcie magitecha (Siluria is on it)
- pokazać Dianie jaki Wiktor jest; doskonałe odwrócenie uwagi
- w skrócie: pełen chaos # chaos na poziomie Szlachty
## Misja właściwa:
### Faza 1: Porwanie Zenona Weinera

Dzień 1: 

Oktawian Maus, jak co jakiś czas, odwiedził Rezydencję Blakenbauerów żądać sprawiedliwości dla Mileny Diakon od Hektora. Jednak tym razem zrobił obejście - odwiedził Edwina i zaproponował mu spłacenie długu u Szlachty. Po czym okazało się, że nie jest spłacany dług u Szlachty - Oktawian ma własny plan. Święcie wierzy w to, że Ozydiusz Bankierz stoi za Spustoszeniem - wskazują na to jego dziwne ruchy i tępienie Baltazara Mausa. Jednak by to udowodnić bądź zaprzeczyć Oktawian potrzebuje Zenona Weinera - eksperta Świecy od Spustoszenia. Nie chce go zapytać, bo nie chce się ujawnić; co więcej, jeśli zapyta, to w tym momencie Zenon może obrócić się przeciwko niemu. Bo Weiner.

Więc Oktawian ma lepszy plan. On podejrzewa, że Świeca go obserwuje, więc regularnie nawiedza Blakenbauerów w kwestii Mileny. Ale tym razem prosi o wsparcie - niech Blakenbauerowie porwą dla niego Zenona Weinera a on wykorzysta swoją wiedzę o Szlachcie by spokojnie w nich uderzyć. Dalia Weiner jest naiwnym lekarzem; jeśli ona wpadnie w kłopoty, Diana wpadnie w kłopoty. Jako, że pierwszymi podejrzanymi byliby Blakenbauerowie (by spłacić dług), Maus z przyjemnością weźmie to na siebie i umożliwi Blakenbauerom odbicie Dalii i pomoc Dianie.

Jako zakładniczkę Oktawian zostawił swoją kuzynkę, Judytę Maus. Ma ona niedługo wziąć ślub polityczny, więc jest istotna dla Mausów.

Leonidas jest lekkim zwolennikiem tego planu. To znaczy, nie zgłosił się na ochotnika (Edwin go zaproponował), ale Leonidas od razu wpadł na ciekawy wariant planu - faktycznie porwać Zenona, po czym sprzedać informację Dianie, że Dalia jest w kłopotach, by Oktawian nie mógł po prostu spełnić swojej części planu i by Judyta została z Blakenbauerami. A Mausówna się zawsze przyda, zawsze teraz, jak nie ma Karradraela.

Edwin powiedział, że wszystko zależy od decyzji profesora a jako, że Leonidas jest bliżej, on zostawia decyzyjność w gestii Leonidasa.

Leonidas zdecydował się zacząć od zebrania informacji na temat Zenona.
Zenon Weiner, 45 lat, mag, sympatyzuje z Powiewem Świeżości, dobre serce, nieco ciapowaty. Nie zadaje się z ludźmi. Ma żonę (drugą) i trójkę dzieci; żony nie wiedzą o sobie. Gdy pierwsza straciła moc magiczną to zgodnie z dekretami Świecy ją zostawił (choć protestował!). Nie przeszkadzało mu to jednak się ożenić ponownie. Ogólnie, bardziej interesuje się Spustoszeniem niż żonami. Szczególną uwagę poświęcał "jak Spustoszenie może przywrócić moc magiczną". Oczywiście, zawiódł.

W jakich okolicznościach czy miejscu można porwać Zenona Weinera? Gdzie znalazłby się sam z siebie?
- spotyka się ze swoją byłą żoną, w jej domku w Kopalinie, bo wciąż ją kocha choć to politycznie niepoprawne
- regularnie wybywa w odleglejsze miejsca; szuka różnych rzeczy po złomowiskach (może natrafi na kernel Spustoszenia / ślad); wtedy ma wsparcie.

Terminusi nie utrzymują z nim regularnego kontaktu, bo jest ich po prostu za mało. A nie jest non-stop narażony.

Ostatnio przy Zenonie Weinerze zawsze znajduje się jeden terminus. Ostatnio - uczeń terminusa. Tym razem to będzie Salazar Bankierz.

Leonidas znalazł używając swojej sieci politycznej interesującego maga - Ludwik Weiner, alchemik-barman. Nikt nie zrobi ci tak dobrego drinka jak Ludwik. Działa w ludzkim barze (a on tam szpanuje), klub "Paradoks" w Kopalinie, klub studencki. Magowie nie lubią tam chodzić bo nazwa.

Wieczorem Alina idzie do "Paradoksu", zdobyć próbkę barmana...
Poszła gdy impreza się już lekko rozkręciła. Idzie w "barman może spojrzeć jej w dekolt i upija się na smutno". Jest nieszczęśliwa, chce zmienić chłopaka, ma smutną opowieść... chlipie. Gdy impreza się kończy, Alina dalej chlipie. Ludwik ją przytula, przychodzi Dionizy jako "zły chłopak", daje mu w mordę i odchodzi z krwią na rękach i "lalunią przy boku". W ten sposób Alina ma próbkę krwi Ludwika Weinera (Ludwik zapamiętał Alinę i częściowo Dionizego).

Alina jako Ludwik kupuje nieaktywne Spustoszenie.

Dzień 2: 

Klara z Edwinem przygotowali miny chemiczne. Klara nie czaruje, ale minę złoży. I wsadzi tam trochę nieaktywnego Spustoszenia. Edwin zajął się odpowiednim przygotowaniem chemikaliów.
Leonidas wie, na które złomowisko uda się Zenon; Alina jako Ludwik przeszła tam by strażnik ją zauważył. I podłożyła minę chemiczną.

Zenon i Salazar weszli na złomowisko. Alina i Dionizy są na złomowisku, schowani. 

Oczywiście, Zenon wszedł na minę chemiczną i padł. Koło niego stał Salazar, który zgodnie ze wszystkimi procedurami miał wszystkie możliwe tarcze i artefakty (służbista) i tarcze go osłoniły. Na to jednak wyskoczył Dionizy (asekuracja) i walnął mu w twarz kolbą broni. Salazar padł nieprzytomny. Mają Zenona.
...to jedynie potwierdza teorię Spustoszonego agenta.

Teraz Dionizy ewakuuje Zenona, Alina kradnie mu ubrania, Dionizy wsadza go w dywan i wsadza w śmieci i daje flaszkę strażnikowi; idzie do skupu. Strażnik udaje, że nic nie widział. Zapamiętał jednego złomiarza. A Alina jako Zenon pospiesznie się oddaliła w bliżej nieznanym kierunku.

Potem Alina poszła do jakiejś galerii, do ubikacji, zostawiła ciuch i zmieniła formę po czym sobie poszła jako inny facet...

Oktawian pokazał wcześniej gdzie ma zostać Zenon dostarczony. Dionizy zostawił Zenona w dropboxie po czym oddalił się w swoim kierunku.

### Faza 2: Porażka planu Oktawiana?

Zgodnie z obietnicą, Oktawian Maus zdecydował się na spełnienie swoich zobowiązań. Naprawdę chciał! Nie chciał nikogo oszukać! Jednak Leonidas miał nieco inny plan.

Do Diany zgłosił się agent, który ma żywotnie istotne informacje; powiązane ze słowem kluczem (od anonimowego dobroczyńcy). A informacja to ostrzeżenie o sytuacji z Dalią. Diana zaczęła się zbroić i asekurować - dyskretnie - Dalię.

Oktawian Maus działa przez agentów. On sam nie robi niczego sam; nie wpada sam w pułapkę. Ma 25% szans na powodzenie. Im niższa wartość na k20 tym lepiej dla sił Oktawiana. Wypadło 7/1k20, więc Oktawianowi NIE udało się wpakować kłopotów Dalii, ale był bardzo blisko; nikt bardzo nie ucierpiał i nie było bardzo poważnych zniszczeń. Ta opcja ma najwięcej otwarć ze wszystkich stron. Oktawian nie dostarczy Dalii. Diana nie wie, kto za tym stoi. Wie tylko, że było silne ryzyko i gdyby nie hint, nie udałoby się.

Oki. Negocjacje po stronie Oktawiana nie mają większych szans powodzenia. Na razie Judyta zostanie w Rezydencji; czas pokaże jaki będzie koszt / zysk.

Wynik:
- u Diany jest do odebrania dług na hasło
- u Oktawiana jest bardzo silny zakładnik PLUS można zawsze Dianie zdradzić kto stał za atakiem; Oktawian nie ma pozycji negocjacyjnej
- Zenon Weiner jest zdjęty z akcji co potencjalnie osłabi Ozydiusza (a Zenonowi nic nie grozi; taki układ z Mausem)

Całkiem niezły wynik jak na sojusz z Blakenbauerami. A mogli zabić.

...jeśli Judyta nie chce wyjść za mąż, może... może zaproponować jej azyl?

# Streszczenie

Oktawian Maus odwiedził Rezydencję Blakenbauerów, ale tym razem miast żądać sprawiedliwości dla Mileny poprosił Edwina o porwanie Zenona Weinera, by udowodnić, że Ozydiusz stoi za Spustoszeniem. Zostawił jako zakładniczkę kuzynkę, Judytę Maus. Alina jako Ludwik Weiner kupuje nieaktywne Spustoszenie, porywają Zenona pokonując Salazara Bankierza i wrabiając w to "Spustoszonego agenta". Leonidas zdradził Oktawiana; wrobił go tak, by zyskać ogromny dług u Diany Weiner, zachować zakładniczkę i zdobyć przewagę nad Oktawianem. Ale Oktawian dostał swojego Zenona.

# Lokalizacje:

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Centrum
                        1. Studencki klub "Paradoks"
                    1. Obrzeża
                        1. Rezydencja Blakenbauerów
                    1. Nowy Kopalin
                        1. Złomowisko Kopalińskie

# Zasługi

* mag: Leonidas Blakenbauer, the puppetmaster, który wyrolował sojuszników, przeciwników i postronnych.
* vic: Alina Bednarz, the cosplayer, zdobyła środki (krew) i wyszła jako "Zenon" poza złomowisko.
* vic: Dionizy Kret, the muscle, wpierw znokautował barmana a potem spałował zaskoczonego ucznia terminusa od tyłu.
* mag: Oktawian Maus, który był tak nierozsądny, że wszedł w sojusz z Blakenbauerami. Ale Zenona Weinera dostał.
* mag: Edwin Blakenbauer, który był tak załamany planem Oktawiana, że przekazał go Leonidasowi. 
* mag: Zenon Weiner, który po prostu miał pecha i porwali go Blakenbauerowie dla Oktawiana Mausa.
* mag: Salazar Bankierz, uczeń terminusa postępujący zgodnie ze wszystkimi procedurami którego tarcze padły do lapisowanej srebrnej pały Dionizego.
* mag: Judyta Maus, mająca wyjść za mąż politycznie czarodziejka, która została tymczasowo wprowadzona do Rezydencji Blakenbauerów jako "honorowy gość".
---
layout: inwazja-konspekt
title:  "O Wandzie co Zajcewa nie chciała"
campaign: powrot-karradraela
gm: żółw
players: kić, dzióbek
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [150829 - Hybryda w bazie Skorpiona (PT)](150829-hybryda-w-bazie-skorpiona.html)

### Chronologiczna

* [150920 - Sprawa Baltazara Mausa (AW)](150920-sprawa-baltazara-mausa.html)

### Logiczna

* [150820 - Klemens w roli swatki (Kx, AB)](150820-klemens-w-roli-swatki.html)

### Inne

Kończy się w okolicach misji:
* [150922 - Och nie! Porwali Ignata! (AW, SD)](150922-och-nie-porwali-ignata.html)

## Punkt zerowy:

Ż: Jaką korzyść z tej całej sytuacji może uzyskać KADEM przeciwko Świecy?
B: KADEM uzyskuje przyczółek na obszarze pomiędzy KADEMem a Srebrną Świecą.
Ż: Jaka akcja z ukrycia na pewno MOCNO uderzyła w Srebrną Świecę?
K: Ktoś z członków Świecy zaatakował Powiew w taki sposób, że Powiew ma silne prawo do rekompensaty.
Ż: Powiedz mi coś o dziewczynie, przez którą Marcel wpadł w kłopoty.
D: Ma lepkie ręce. Cicha, zwinna, niepozorna. Absolutnie niebojowa.
Ż: Dlaczego właśnie jego? Co ma za szczególną wiedzę którą chce się dowiedzieć?
B: Vladlena może nie wiedzieć, ale zna połączenie pomiędzy Emilią a niebezpieczną grupą przestępczą.

Ż: Jakie plugawe w świecie ludzi czyny robi Joachim Zajcew?
B: Dowolnymi sposobami, wyciąga ludzi z aresztu. Jest magiem. Hektor wie i nic nie może zrobić.
Ż: Czemu Joachim nie był w stanie "pozyskać" Wandy?
K: Bo z jego punktu widzenia jest "niekompletna", bo nie zaistniały odpowiednie warunki.

## Misja właściwa:

### Faza 1: Równoległe działania

Edwin poprosił do siebie Alinę i Klemensa. Powiedział im, że Wanda Ketran jest irytująca; teraz szuka jej mag. Jednocześnie jest problem powiązany z tym, że Grzegorz Śliwa - całkowicie niewinny strażnik cmentarza - jest oskarżony o coś, czego nie popełnił. I niestety dowiedział się o tym Hektor...
Zdaniem Edwina sprawa jest bardzo prosta - trzeba wrobić maga (Joachima Zajcewa) i uratować Grzegorza Śliwę. To, że Zajcew jest tego akurat niewinny a Śliwa to zrobił fizycznie to już inny problem.
Zajmuje się tematem niejaki Miron Ataman, bardzo skrupulatny i dokładny detektyw policyjny. Nie wierzy w to co się stało, bo to nie ma sensu.
Zajcew już próbował zatrudnić detektywów; Edwin podrzucił mu najbardziej bezużytecznego jakiego zna. Ale nie wiadomo, czy na tym się skończy.

Edwin sprawdził pamięć Wandy pod kątem Joachima Zajcewa. Znalazł tam coś smutnego - wyglancowanego elegancika, który Wandzie powiedział "musisz być moja" i "będziesz moja" a ona nim wzgardziła. Joachim próbował - faktycznie próbował - zdobyć ją drogimi prezentami, różnym podejściem, imprezami, usługami a Wanda konsekwentnie odmawiała. Powiedziała, że nie będzie jego "rzeczą". Joachim powiedział jej przy okazji, że on otacza się różnymi ludźmi i zapraszał ją ze sobą - zawsze odmawiała.
A teraz zniknęła i on jej szuka. Zdaniem Wandy - szuka by dalej ją namawiać.
Jest to spójne z danymi Edwina; różnica jest taka, że Joachim nie jest tak beznadziejny jakim widzi go Wanda. Jest dyskretnie bogaty i dość elegancki i FAKTYCZNIE otacza się pięknymi ludźmi (Wanda nie pasuje do wzoru); miał swego czasu nawet jedną wiłę.

Alina wpadła na pomysł - podłożyć mu Annę Kajak jako detektywa. A Klemens zaproponował, by dowiedzieć się z prokuratury odnośnie cmentarza ze szczególnym uwzględnieniem Śliwy. Zgodnie z prośbą Aliny, Patrycja wrzuciła dane bardzo szybko:
- Grzegorz Śliwa kupił kanistry z benzyną
- Nie do końca pamięta szczegóły, ale twierdzi, że podpalił cmentarz; przesłuchany przez Atamana powiedział, że to on zrobił ale nie zna szczegółów
- Śliwa nie ma motywu. Nie ma innych śladów niż jego.
Joachim Zajcew nie ma alibi na ten czas. Ale nic nie łączy go z tą sprawą. Czyżby potencjalny cel "chciał się popisać jaki jest macho przed panienką"? Chodzi też o to, by pojawił się powód zrozumiały dla magów - szedł mag, upomniał go człowiek, mag zagroził człowiekowi i ups, katastrofa.
Klemens poprosił Edwina, by ten znalazł kogoś, komu Joachim mógł podpaść. Najlepiej mało wpływowego maga, który mógłby zrobić coś tego typu.
Edwin powiedział, że poszuka. W ten sposób nikt nie będzie podejrzewał, że celem jest uniewinnienie człowieka.

Anna Kajak przyjęła misję. Ma zostać detektywem dla Joachima Zajcewa i ma się dowiedzieć czego Joachim szuka i czego chce od Wandy. I ile jest skłonny zapłacić. Anna powiedziała, że się tym zajmie. Przy odrobinie szczęścia, Anna ma znaleźć haka na Joachima Zajcewa.
...w chwili, w której Anna ma do dyspozycji trackery i podsłuchy, hak może nie być bardzo trudny do znalezienia...

- Edwin szuka informacji o potencjalnych wrogach Joachima Zajcewa, by kogoś w to dodatkowo wrobić.
- Anna próbuje znaleźć haka na Joachima, a przynajmniej dowiedzieć się, czego tak naprawdę chce od Wandy.
- Patrycja szuka ludzkich akt na temat Joachima; coś, co pozwoli go tam postawić; historia jego i okolic Cmentarza Wiązowego.

HIDDEN MOVEMENT:

- Joachim naciska Ormię
- Miron nie znajduje żadnych śladów poza Grzegorzem
- Miron znajduje leki u Grzegorza

END OF HIDDEN MOVEMENT:

### Faza 2: Poszerzenie kręgu

- Edwin: 
Edwin znalazł pasującego maga. Niejaki Tymoteusz Maus; dość żałosna postać, który powadził się w sprawie koncepcji zbudowania konstrukcji artefaktycznej ekstraktującej Quarki z pola emocjonalnego (na czystym poziomie rezydualnym; bez konieczności formowania silnego węzła; starczy słaby). Tymoteusz zrobił prototyp dla Joachima, prototyp nie działał podobno prawidłowo i Joachim odmówił zapłacenia dopóki Tymoteusz nie dostarczy funkcjonującego urządzenia. Żarli się o to, ale Tymek oddalił się z podkulonym ogonem.

- Anna: 
Anna zgłosiła, że Joachimowi dość zależy na znalezieniu Wandy, ale nie jest to niezbędne do jego życia. Przycisnęła go; wyprowadziła, że Wanda ma w sobie "coś", jest władcza i przywódcza. Wanda - zdaniem Joachima - musi przyjść z własnej woli; inaczej go nie interesuje. Podłożyła mu podsłuch i tracker. Przy okazji bierze jego pieniądze jako detektyw. Wie też, że naciskał niejakiego Ormię, będzie się wybierał do fabryki dronów i ma zamiar porozmawiać niedługo z dziennikarką "Kociego Oka".

- Patrycja: 
Patrycja zgłosiła, że Joachima widywano czasami - nieczęsto, ale czasami - w okolicach Cmentarza Wiązowego. Spotykał się tam z pewną damą, bardzo piękną. Dodatkowo, Cmentarz Wiązowy ma sławę miejsca, gdzie "coś się dzieje". Nie jest to miejsce całkowicie czyste z punktu widzenia historii; może porachunki? Nie wiadomo, nie ma dowodów, ale to miejsce jest... szczególne.
Patrycja jeszcze dorzuciła, że Miron Ataman ma nową teorię - w mieszkaniu Śliwy znaleźli różne leki i Ataman przesłał request do laboratorium by powiedzieli, czy leki w połączeniu z alkoholem itp mogły spowodować częściową niepoczytalność.

"
Aktualny plan zakłada:
- w świecie ludzi:
   - świadek widział jak Śliwa przeszkodził w spotkaniu Joachima z piękną kobietą na cmentarzu, kobieta oddaliła się pośpiesznie, Joachim zaczął się kłócić ze Śliwą, doszło do szamotaniny, Śliwa został uderzony w głowę i się przewrócił rozlewając karnister z benzyną, świadek nie widział więcej szczegółów, ale chwilę po tym benzyna paliła się, Joachim oddalił się pośpiesznie, a otumaniony Śliwa wyszedł z terenu cmentarza i wezwał policję
- Olga podkłada wyniki w laboratorium które mówią o tym że skutkiem medykamentów mogą być zaburzenia pamięci
"

Olga naturalnie się zgodziła. Znajdzie takie substancje i okoliczności, by uniewinnić mężczyznę.

W opisie podanego przez świadka kobieta ma być nieprecyzyjnie opisana; nie da się jednoznacznie do niej dojść. Nie jest to też w pełni zgodne z aktami policyjnymi; magowie zwykle nie pomyślą o czymś takim i nie zadbają o szczegóły.

Edwin dostał kolejną prośbę od Klemensa; znaleźć tego świadka (jakiegoś świadka). Świadek winien mieć powód być w okolicach cmentarza i świadkowi trzeba troszeczkę pomanipulować pamięcią; z jednej strony w świecie ludzi świadek wtedy mówi prawdę, z drugiej w świecie magów ktoś rzucił na szybko, na odchodne zaklęcie modyfikacji pamięci. Edwin westchnął. Znajdzie się świadek.

Anna Kajak zgłosiła informacje z podsłuchu. Joachim Zajcew poprosił o pomoc Annę Górę, dziennikarkę Kociego Oka. Powiedział Annie G. dość sporo interesujących scen i informacji z życia Wandy sprawiających, że Wanda wygląda na bardzo ciekawą osobę. Widać, że Joachim interesuje się Wandą z uwagi na jej pełne przekonanie o swoich możliwościach i pójście "all-in". Wanda jest charyzmatyczna, ma umiejętności przywódcze, jest marzycielką i potrafi to wszystko wykorzystać.
Anna G. się zainteresowała tematem; powiedziała, że jej "kociątka" pomogą mu to znaleźć zwłaszcza jeśli on da jej ciekawe tematy na bloga. Umowa stoi, Joachim ma kilka fajnych tematów którymi z radością się podzieli, a i zapłaci solidnie.
Dziennikarka powiedziała, że ma zamiar pozbierać tematy o Wandzie.

HIDDEN MOVEMENT:

- Miron znajduje świadków potwierdzających hipotezę; min. od Olgi
- Anna Góra zbiera dane o Wandzie z różnych stron; min. ze squata i z firmy z dronami

END OF HIDDEN MOVEMENT

### Faza 3: Gdzie jest Wanda?!

Olga zaraportowała, że faktycznie udało jej się udowodnić, że połączenie alkoholu z lekami mogło spowodować pewne problemy z pamięcią i poczytalnością. Jej opinia z laboratorium była taka: gość nie był w pełni trzeźwy, nie był pijany, ale nie był w pełni trzeźwy i przez to się przyznał i przez to nie wszystko do końca pamięta. Śliwa będzie miał pewne (niewielkie) problemy, bo nie był trzeźwy w pracy, ale bycie lekko wstawionym jak pilnujesz cmentarza gdzie nic się nie dzieje po prostu nie jest problemem i nie jest takie istotne. A Ataman będzie miał swoje wyjaśnienie.

Edwin znalazł "ochotnika". Starszy pan, który regularnie odwiedza grób swojej żony. Miał powód tam być, nikogo to nie dziwi, jest stałym bywalcem... perfekcyjny. Lekka manipulacja pamięcią jest standardową operacją. Zostaną ślady - bo mają.

Miron Ataman stwierdził, że Grzegorz Śliwa najpewniej jest niewinny; oczywiście, przedłoży sprawę wyżej, ale szuka teraz kogoś innego i jego zainteresowanie skupiło się na Joachimie Zajcewie (który nic nie wie i nie ma zielonego pojęcia, że jest na celowniku policji).

Tymczasem gruchnęła bomba - Anna Góra napisała ciekawy artykuł o Wandzie z pytaniem "gdzie jest Wanda". Zasugerowała, że Wanda podpadła sporej ilości osób (nie wymieniła Ormiego, ale zasugerowała). Pokazała profil Wandy z "Działa plazmowego" i ogólnie wzbudziła jej nieobecnością zainteresowanie. 
Ogólnie, Edwin złapał się za głowę. Ludzie nie tyle, że rzucili się jej szukać, ale temat Wandy pojawił się szerzej; nie wiadomo, kiedy magowie zaczną jej szukać (nie wszyscy, tacy jak Dracena czy Janek Wątły).

Edwin poprosił Alinę na dywanik.
Jako, że sytuacja powoli wymyka się spod kontroli, trzeba to jakoś opanować. I kto to opanuje? Pojawi się Wanda Ketran i porozmawia z Anną Górą. I kto będzie Wandą? Oczywiście, Alina.

Klemens rzucił świetny pomysł - a co, jeśli to Wanda była osobą, z którą na cmentarzu spotykał się Joachim? Ona to wszystko widziała. Dlatego właśnie zniknęła; jeden z tych powodów. A artykuł Anny Góry i cała ta sytuacja zmusiła Wandę do wyjścia i porozmawiania z Anną. Wanda nie chce mieć nic wspólnego z Joachimem Zajcewem i dlatego zniknęła.
To wspiera wszystkie te teorie.
I to rozwiąże wszystkie te problemy i Edwin będzie szczęśliwy.

Została ostatnia rzecz do zrobienia - wyekspediować Joachima Zajcewa gdzieś daleko podczas gdy Alina (jako Wanda) udziela wywiadu Annie Górze. To też nie jest problem - Anna Kajak (detektyw wynajęty przez Joachima) da poszlaki wskazuje na Piróg Górny i pojedzie tam z Joachimem. Więcej, poszlaki będą prawdziwe; jakiś kawał ubrania Wandy czy coś.

### Epilog:

- Grzegorz Śliwa został uznany winnym nietrzeźwości w pracy i nic złego z tego nie było. Nikt nie wystąpił o ściganie; Śliwa lekko dostał po łapkach i na tym koniec.
- Miron Ataman uczepił się tematu Joachima Zajcewa. Wszystko wskazuje, że to Zajcew ma coś w tym wspólnego a zniknięcie Wandy Ketran jedynie to uprawdopodobnia.
- Anna Góra zrobiła kolejny artykuł "wywiad z Wandą", w którym opisała całą tą sytuację; Wanda na jakiś czas opuszcza Kopalin i się chowa. Poszukiwania Wandy zostały zarzucone.
- Edwin jest szczęśliwy. Nadal nie wie, czemu Wanda jest taka specjalna, ale nie obchodzi go to.
- Hektor jest szczęśliwy. Człowiek jest niewinny a zły mag dostał po łapkach.
- Klemens dostał 3 dni urlopu. Pilnując Wandy. Tak bardzo urlop...

## Dark Future:
### Actors:

Miron Ataman, ludzki detektyw (Scientist)
Joachim Zajcew, mag polujący na Wandę (Collector)
Anna Góra, dziennikarka (Explorer)
Ochotnicy Anny (Społeczność dookoła celebryty)

### Faza 1: Równoległe działania

- Joachim wynajmuje detektywów \| C.(recruit henchmen for <finding Wanda>)
- Miron przesłuchał Grzegorza \| S.(seek alternate theory)
- Joachim naciska Ormię \| 
- Miron nie znajduje żadnych śladów poza Grzegorzem \| S.(get to conclusion)
- Miron znajduje leki u Grzegorza \| S.(get to conclusion)

### Faza 2: Poszerzenie kręgu

- Miron sprawdza teorię z lekami i próbuje to zweryfikować \| S.(build a new hypothesis)
- Joachim prosi o pomoc Annę Górę \| C.(prepare resources to get closer to <Wanda>)
- Miron znajduje świadków potwierdzających hipotezę \| S.(confirm a hypothesis experimentally)
- Anna Góra zbiera dane o Wandzie z różnych stron \| Ex.(research <Wanda> for future exploration)

### Faza 3:

- Anna Góra robi "ktokolwiek widział" o Wandzie \| Ex.(tell a story)

# Streszczenie

Wandy Ketran szuka mag rodu Zajcew, Joachim. Alina, Olga, Anna i Klemens uniewinniają strażnika cmentarza z "to przez niego spłonął" i wrobili w to Joachima Zajcewa; przekierowali zainteresowanie Joachima na Tymka Mausa i nasłali nań policję. Nikt nie łączy Blakenbauerów z Wandą.

# Zasługi

* vic: Klemens X, mastermind i taktyk Blakenbauerów tępiący Joachima i skutecznie zaciemniający zniknięcie Wandy.
* vic: Alina Bednarz, łącznik Blakenbauerów z siłami specjalnymi i udająca Wandę Ketran zmiennokształtna agentka.
* mag: Edwin Blakenbauer, który dla odmiany załatwiał rzeczy dla Klemensa (przede wszystkim świadków i informacje o Joachimie Zajcewie).
* czł: Wanda Ketran, której przeczytana pamięć posłużyła do bezproblemowego jej zniknięcia. Okazało się, że jest całkiem wpływowa.
* czł: Grzegorz Śliwa, strażnik cmentarza który go podpalił i siły specjalne udowodniły, że to w sumie nie on. Dostał lekko po łapkach.
* czł: Miron Ataman, bardzo skuteczny policjant Kopalina, który został zmylony; teraz poluje na Joachima Zajcewa ku uciesze Hektora.
* czł: Anna Kajak, zatrudniona przez Zajcewa jako detektyw, podsłuchiwała lojalnie dla Blakenbauerów. Gra na dwa fronty.
* czł: Olga Miodownik, która tak długo szukała w laboratorium, aż znalazła kombinację leków ratujących Grzegorza Śliwę.
* czł: Patrycja Krowiowska, zbierająca istotne informacje o działaniu policji i stanie oskarżenia dla Blakenbauerów.
* czł: Anna Góra, dziennikarka "Kociego oka", która miała być tajną bronią Joachima w znalezieniu Wandy a Joachima pogrzebała dzięki Alinie.
* mag: Joachim Zajcew, zainteresowany Wandą Ketran, szukał jej wszelkimi środkami, za co Blakenbauerowie nasłali na niego policję.
* mag: Tymoteusz Maus, który w sumie nie wystąpił na misji... ale został wrobiony że to jego wina; w tle jest konflikt Tymka z Joachimem.

# Lokalizacje:

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Obrzeża
                        1. Rezydencja Blakenbauerów
---
layout: inwazja-konspekt
title:  "Śledzie na Fazie"
campaign: adaptacja-kralotyczna
gm: żółw
players: arleta
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [180425 - Śledziem w depresję](180425-sledziem-w-depresje.html)

### Chronologiczna

* [180425 - Śledziem w depresję](180425-sledziem-w-depresje.html)

## Kontekst ogólny sytuacji

### Opis sytuacji

* brak

## Punkt zerowy

?

## Potencjalne pytania historyczne

* brak

## O co toczy się gra

* Ż: Co z tymi śledziami?
* A: Mogę pozyskiwać z nich jakieś mikstury alchemiczne. Mogę się podzielić z kimś śledziami. Fundacja "wyślij śledzia".
* Ż: Fundacja?
* A: Fundacja "wyślij śledzia" polegająca na tym, że wysyłam śledzie i one pomagają innym. I mam za to fajne składniki alchemiczne.
* Ż: Widzę 3 główne problemy:
    * Teleport: Lassie wróć w wykonaniu śledzia (Arleta: wychować śledzie, jak gołębie) 
    * Śledzie syberyjskie to święte stworzenia. Niekoniecznie powinny być wysyłane UPSem. (Arleta: o nie dbać i rozwiązać konflikt polityczny)
    * Pozyskanie klientów: magowie są nieufni śledziom syberyjskim (Arleta: pozyskanie klientów)

Czyli:

* Ż: Fundacja się uda ale będą fuckupy, pikiety i nieposłuszne śledzie. Jeden kiepski klient.
* A: Fundacja się uda, działa sprawnie i zyska dobrą renomę. Ma się rozrastać i MOŻE żeby Zajcewowie zaakceptowali działanie tej fundacji.

## Misja właściwa

TIMING_INIT: (21:02)

**Dzień 1**:

SCENA 1: Głodna Genowefa?

Kopalin, a beautiful city. A perfect place to start a career as a businesswoman.

First things first – it is time to buy some rolls. However, when Jean wanted to pay, a Siberian herring looked out of her handbag and smiled with his many teeth towards the unfortunate baker.

The bakers screamed and Jean forcefully put the herring inside the handbag and ran away – she was less hungry than frightened that something wrong will happen. Fortunately, no one was following her.

So the blasted herring teleported inside her handbag. Yet, she was still hungry. She went to a small bistro nearby to get some vegetarian food to eat, hoping naïvely that siberian herrings are not well known from eating vegetarian.

Sadly, the herring felt the call of Adventure. Jean noticed that quickly and she tried to pacify the herring... (1 zasób, 3+3vNORM->SS). Although this particular herring got pacified, another one teleported to have fun. It teleported directly into the kitchen...

Panika. Dres bierze nóż i chce rozwiązać problem; jego lalunia piszczy jaki on dzielny. Genowefa chce zatrzymać dresa, to JEJ śledź. Zaklęcie iluzji - ten śledź gdzieś uciekł. Niestety, magiczna porażka. WSZYSTKIE śledzie się teleportowały do bistro. POTĘŻNA Panika. Ludzie się boją, część robi zdjęcia. Genowefa rzuca KOLEJNE zaklęcie kontrolujące śledzie... niech odlecą do bezpiecznej kryjówki. SS. SUKCES. Śledzie odeszły. Ale syf został. Cóż, Genowefa musi do śledzi...

Co gorsza, całość jedzenia została Skażona i przestała być jadalna. Niefart.

SCENA 2: Kryjówka (gdzie Genowefa trafi dalej?)

At this point Jean is really hungry... And the herrings managed to steal all the rocket salad (rukola). They are eating, but Jean is unable to because the food is Corrupted.Herrings are really having a good food, between Jean's depression and some cool, corrupted rocket salad.

Jean has located her hideout in the building which has been ruined after the Karradrael's war. Unknown to her, this building is located about 500 m from the main headquarters of the Powiew Świeżości. And Max was going to make it all work.

Max entered the building, carefully, with some sandwiches and stuff. He got informed earlier about an unknown mage who was causing havoc in the bistro and he wanted to see what the hell was going on.He shared his lunch with her (the fact she took food and started munching not expecting any trick or trap he considered to be a good sign - she was not a bad person, just misguided and naive).

After some chatting about the herrings, Max has learned that the herrings feed on depression and sadness. This was an amazing opportunity for him – As'Caen castle was located on Daemonica Phase and that way Jean could become a herring type of a therapist. And between Aniela and Celestyna everything was possible...

Max asked her casually where was she going to stay. When Jean said she has no particular place, Max invited her as a guest to Powiew Świeżości. Jean accepted with happiness.

SCENA 3: Powiew (czy Genowefa dowie się o celach wobec jej osoby?)

Powiew Świeżości was a really interesting place for Jean. She was introduced to the leader of the Guild, Roman, and she has noticed that he and Max were not telling her the whole truth. She decided not to pursue this course of action and to accept their hospitality. In the meantime she learned the following:

* No one really understands how this castle works; it changes by itself when no one is looking
* there is one person, Kasia Kotek, who is able to navigate the castle and "shows the arrows" - literally they light up and flash while we are walking.

Her room was supposed to be between the room of Celestyna and Aniela. Okay. On to the room.

SCENA 4: Pokój (czy Genowefa się spokojnie wyśpi czy pójdzie coś nie tak?)

Jean's room is quite large – it has three strange doors, no bed and no windows. Max tested all the doors – they led to nothing at all; behind the doors there was a wall.

Afterwards, after everyone left except Jean, Jean wanted to go to sleep. There were no 'three' doors anymore. There were two doors and a curtain. Jean opened the curtain and looked outside the window – she was able to see the magnificence of Daemonica... and of the As'caen castle.

Her herrings were able to watch it too. One of them in particular got really enamored with what he has seen...

Jean went to sleep on the curtain, not wanting to disturb anyone...

SCENA 5: Ratujmy śledzia (czy Genowefa odzyska niesfornego śledzia?)

Jean woke up. She noticed one herring is missing. She looked outside the window and noticed a herring is out there. Ouch. So she asked Kate to lead the way to Max - and it was done.

When she explained to Max what was happening and that her herring is endangered, Max assembled the team - Aniela, Celestyna, himself, Jean and one other herring. Why herring? If it drains depression and negative thoughts, it will make them safer on Daemonica.

Aniela opened the As'caen (she asked pretty please), a long ramp for like 10 storeys appeared and they went down to pick the herring up, under the fire support of As'caen castle.

IT WENT WELL!

## Wpływ na świat

* Żółw: 12
* Arleta: 7

Czyli:

Gracze:

* Jeden śledź będzie niesforny, reszta nie.

MG:

* 

# Streszczenie



# Progresja

* 

## Frakcji

* 

# Zasługi

* mag: Genowefa Huppert, która została honorowym gościem Powiewu Świeżości

# Plany

* 

## Frakcji

# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński


# Czas

* Opóźnienie: 1
* Dni: 2

# Narzędzia MG

## Cel Opowieści

* Sprawdzić nową mechanikę magii, nowe karty 1805 i nową generację sesji
* Timing test

## Po czym poznam sukces

* null

## Wynik z perspektywy celu

* null

## Wykorzystana mechanika

1805

---
layout: inwazja-konspekt
title:  "Przecież nie chodzi o koncert"
campaign: powrot-karradraela
gm: żółw
players: kić, raynor
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [161222 - Kto wpisał Błażeja do konkursu?! (SD)](161222-kto-wpisal-blazeja-do-konkursu.html)

### Chronologiczna

* [161218 - Zazdrość Warmastera (SD)](161218-zazdrosc-warmastera.html)

## Kontekst ogólny sytuacji
## Punkt zerowy:

* Q: Most corny name serialu ever 
* A: Raynor: "Milusińskie perypetie" 
* Q: Most corny name zespołu muzycznego i gatunek muzyki 
* A: Gatunek "Flower funk", zespół "Smooth Jazz Dla Ubogich" - SJDU. 

## Misja właściwa:

### Dzień 1:

Franciszek dopina na ostatni guzik imprezę. Przyjeżdża zespół "Smooth Jazz Dla Ubogich", załatwione za fundusze unijne w remizie. Spodziewa się kilkunastu osób. Franciszek czuje, że jego kariera polityczna nabiera tempa...

Pukanie do drzwi. Wieczór. Dziewczyna, młoda. Judyta Maus. Pełen entuzjazm, chce iść na koncert "SJDU". Zawołała swoją lekko zażenowaną przyjaciółkę, Annę Diakon (Antygonę). Antygona jest zainteresowana hotelem, Judycie się zaświeciły oczy na myśl o tym, że może spać z FOLKLOREM.

Franciszek przedstawił Kirę. Judyta wykazała nadmierny entuzjazm; Antygona była ostrożniejsza. Usłyszawszy, że 10 km stąd jest hotel... dziewczyny oklapły. Judyta zaprotestowała; Antygona powiedziała, że mają samochód. Judyta, że MIAŁY. Antygona, że MAJĄ... znaczy, coś się stało.

...z historii wynikło, że czarodziejki przeszły kilkadziesiąt kilometrów by przyjść na koncert SJDU. To nie ma sensu. Antygona i Judyta dostały małą chatkę z wygódką na zewnątrz. Antygona ma kilometr do internetu. Podłamała się; ma zlecenie... Kira pozwoliła się chwilę Antygonie pomęczyć, po czym dała jej sieć dla gości... a Judyta zaoferowała się, że pomoże rano wydoić krowy.

* Jak ja się dałam na to wszystko namówić... - Antygona, podłamana

Wyraźnie Antygona i Judyta... jakoś... współpracują. Judyta zrobiła soczek, Kira zrobiła kolację. Kira spróbowała sprawdzić, jakie własności ma ten soczek; jest to BARDZO nietypowo sformowany, ale zdrowy soczek. Franciszek próbuje sprawdzić, co tu się dzieje, jakie ona ma intencje; Judyta chce wywrzeć dobre dobre wrażenie i się odwdzięczyć. Jest jak otwarta księga. Słodka i naiwna, taka trochę głupiutka. Za to Antygona... REMIS. Jej poziom nieufności wzrósł; za to Franciszek dowiedział się, że jest nieufna i ostrożna, ale nie jest wroga.

Dziewczyny nie zrobiły nic złego, acz Kira je pilnuje... i Kira ustawiła potykacz jakby chciały coś robić; rzeczy, które zaalarmują, jeśli będą zbliżały się nie tam gdzie powinny.

Zakładając, że są w miarę miastowe, nie powinno być kłopotu z zabezpieczeniem różnych dodatkowych rzeczy. I Kira zabezpieczyła wejście do warsztatu. Ogarnęli obejście z Franciszkiem i poszli spać... przedtem potykacz, coś...

W nocy: KONFLIKT. Kira pilnuje defensywnie, by nikt nie wszedł do elektrowni czy nie załatwił zwierząt. Franciszek postawił odpowiednie pułapki. 7v6 -> S. Elektroegzekucja biednego golema Antygony; zwiadowczej sowy, gdy Antygona chciała zbadać wnętrze elektrowni. Kira ubrała się i poszła w tamtym kierunku; znalazła koło elektrowni... proszek. Dziwny proszek. I emanację magiczną. W chatce dziewczyn jest ciemno. Więc... 

Franciszek dołączył do Kiry. Mają proszek, czas na analizę. 5v3. Sukces. To jakiś zaawansowany golem; bardzo specyficzny. Kira ma aurę magiczną; pokrywa się z aurą z chatki. Zawiera cień astraliki. Franciszek zapukał do chatki. 

Dwie czarodziejki przytulone w jednym łóżku pod kołdrą. Kira zaczęła je opieprzać, że ktoś próbował się dobrać do źródeł energii. Widząc nienaturalne zachowanie czarodziejek, Franciszek zaznaczył to Kirze. Kira przypatrzyła się dziewczynom. To golemy. Czarodziejek nie ma. 

* TO JEST PRAWO WIOCHY! - Kić. - OGNISTA MAGIA ZAJCEWÓW!!!

Golemy zostały spopielone. Kira wróciła zabezpieczać teren z Franciszkiem... Kira sprawdziła całe gospodarstwo i nie znalazła już więcej żadnego golema ani żadnej czarodziejki.

Kira uruchomiła hipernet. Wysłała jej sygnał... 

* Albo się wytłumaczysz, albo złożę oficjalną skargę. Skorzystanie z gościnności i uderzenie w gospodarza? - Kira, do Antygony
* Nie pozwolę Ci na wykorzystywanie ludzi jako baterie, Kiro Zajcew - Antygona, zimno.
* Skąd przekonanie, że wykorzystuję ludzi? - Kira, z pogardą
* Masz tu grupę dowodów - Antygona, zimno

Ogólnie, wyszło na to, że albo Kira poświęca ludzi albo ktoś tworzy nieistniejące dowody. Antygona sama to zauważyła. Powiedziała, że osobiście boi się Kiry i wolała się wycofać. Kira powiedziała Antygonie, żeby sobie sprawdziła gazetę z tydzień temu...

Umówiły się na to, że Antygona weźmie jeszcze jednego golema i sprawdzi. A Kira, tknięta tym, że Antygona słyszała krzyk, przeszła się szukając katalitycznie czegoś... znalazła echo artefaktu z Kropiaktorium. Ten fragment opowieści Antygony trzyma się kupy.

WŁAŚNIE, KUPY! Konflikt - Franciszek jajcarz przecież próbował wyprowadzić krowę (-1 surowiec) by był placek do wdepnięcia. 3v3. Sukces. Jest ślad. Męski but. Nie produkcji świata ludzi. Tu był inny mag.

* Kira, zobacz, co znalazłem w kupie! - Franciszek, szczęśliwy
* ...utrwal mi to materią... - Kira, podłamana

Przybył golem Antygony. Lalka. Pytanie Kiry, czy golem jest technomantyczny; nie. Kira wyjaśniła o artefakcie z Kropiaktorium; nie wie, co ten mag chce, ale powinno pomóc. KATALITYCZNO-TECHNOMANTYCZNE EMP. Kira chce dupnąć SUPER EMP. Najpotężniejsze jakie potrafi. Kira przypina się do własnych generatorów. Poprawia je swoim warsztatem. YOLOOOOOO!!!!

Konflikt: 9v7->S. Spadła latająca, rozpieprzona kamera. Nie jedna. Kilka. Wspomagane, magitechowe, takie dobre...

Kira zbiera. Nagle przyjeżdża czarna wołga. 

* Stać! Stój! - Garaż

Garaż cały spanikowany; kamery. Są drogie. Musi je odzyskać. Tłumaczy, że reality show. Ma pozwolenia na zrobienie show... a Franciszek nie ma szans użyć surowca - Garaż ma wszystko. 5v7. Trudny test. Oki, Garaż odzyska swoje kamery...

Garaż opowiada, że bardzo interesuje go reality show. Zaproponował Kirze, że poprawi jej reputację wśród magów; ta jednak kazała mu się wynosić...

### Dzień 2:

Kira wzięła swoje źródła energii i zdecydowała się spiąć je w taki sposób, by powodowało periodyczne zakłócenia technokatalityczne. Pogorszy się jakość odbioru w telewizorach, ale powinno uszkodzić część przyszłych nagrań, póki będzie robił w tej okolicy.

Antygona skontaktowała się z Kirą i przeprosiła. Powiedziała, że fakty są takie a nie inne, że się myliła. Kira zaprosiła ją do chatki i Antygona się zgodziła. Podziękowała.

# Progresja



# Streszczenie

Antygona i Judyta wybrały się do Stokrotek obejrzeć koncert "Smooth Jazz Dla Ubogich". Tam jednak natknęły się na maga chcącego zrobić reality show (Mariusza Garaża) ich kosztem. Przez Garaża Antygona i Judyta skrzyżowały szpady z Kirą i Franciszkiem; zanim jednak sprawa zrobiła się "na noże", zaklęcie Kiry (zbyt silne) zdradziło obecność Garaża. Tien Garaż wysępił odzyskanie swoich kamer i obiecał zostawić Stokrotki w spokoju. Magowie (poza Garażem) się pogodzili.

# Zasługi

* mag: Kira Zajcew, katalityczna niszczycielka EMP golemów, dronów i wszystkiego. Anihilator w spódnicy.
* mag: Franciszek Baranowski, jajcarz, znalazł ślad w kupie, chciał spić Judytę i dzielnie bronił swojej ziemi przed biurokracją Świecy.
* mag: Judyta Maus, rozbrajająca. Zrobiła pyszny soczek, chciał spić się cydrem i traktuje to wszystko jako przygodę...
* mag: Antygona Diakon, obiecała opiekować się Judytą i stąd wszystkie jej problemy. Przyjechała na koncert, skończyła tracąc trzy golemy i niesłusznie oskarżyła Kirę.
* mag: Mariusz Garaż, reżyser reality show. Nie spodziewał się mocy Kiry, specjalista od pierwszego wrażenia... NOT. Nie umie grać 'win-win', nawet gdy chce.

# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Okólno-Trocin 
                1. Stokrotki
                    1. Remiza, gdzie będzie koncert "Smooth Jazz Dla Ubogich"
                    1. Zachód, gdzie znajduje się niemałe gospodarstwo Kiry Zajcew i Franciszka Baranowskiego

# Skrypt



# Czas

* Opóźnienie: 1 dni 
* Dni: 2
---
layout: inwazja-konspekt
title:  "Mojra, Moriath"
campaign: druga-inwazja
gm: żółw
players: kić, dust, dzióbek
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [140312 - Atak na rezydencję Blakenbauerów (AW, WZ, HB)](140312-atak-na-rezydencje.html)

### Chronologiczna

* [140312 - Atak na rezydencję Blakenbauerów (AW, WZ, HB)](140312-atak-na-rezydencje.html)

## Misja właściwa:

Nie była to spokojna noc. Hektora Blackenbauera obudził telefon. Był to jego znajomy i jednocześnie kolega - Tadeusz Czerwiecki. Ów policjant, który już wiele razy pomagał Hektorowi, stwierdził, że dzwoni mimo, że jest trzecia rano, gdyż aresztowali dwie osoby. Marcelina i Amelię Blackenbauer. Za obelżywe graffiti na ścianie komendy policji. Hektor się złapał za głowę. Marcelin i Sophistia... Niech siedzą. Zasłużyli. Rano ich przesłucha.

Zauważył też, że Jan Szczupak odpala samochód. Ostatnio Szczupak współpracował z ludźmi, którzy napadli na rezydencję, więc Hektor się lekko zaniepokoił. Zapytał Jana o co chodzi.
Edwin kazał mu pojechać z nim. No i o wilku mowa. Edwin zszedł z góry. Ubrany trochę jak maniakalny morderca, łącznie z okularami przeciwsłonecznymi. Hektor próbował wypytać go o co chodzi, lecz Edwin stwierdził po prostu, że jedzie do pacjentki. Nagły przypadek. 
No dobra... Jak tylko Edwin zniknął z pola widzenia, Hektor posłał za nim ludzi. Niech go śledzą.

Ciężką noc mieli także Andrea i Wacław. Ich zbudził wrzask wybudzonej z koszmarnego snu Teresy. Wacław uspokoił ją, zwłaszcza, że nic jej nie groziło, ale zaciekawił się prośbą Teresy, która poprosiła, by zadzwonił do Krystalii mimo wczesnej godziny.
Teresa nie chciała nic powiedzieć, ale przyciśnięta przez Wacława wszystko wyśpiewała. Krystalia wykorzystała fakt, że Teresa jest zarówno katalistką, lekarzem jak i w ograniczonym stopniu nekromantką. Już od pewnego czasu Krystalia skłania Teresę do rzucenia zaklęcia wskrzeszenia. Teresa absolutnie wzbraniała się przed próbami na ludziach lub magach, lecz Krystalia zmusiła ją do wskrzeszenia psa.
Koszmarny sen Teresy polegał na tym, że coś się stało i pies się wypaczył.
W świetle takich rewelacji Wacław zadzwonił do Krystalii. Krystalia była w absolutnie uroczym humorze osoby, która od dłuższego czasu nie ma na sobie kolii, więc Wacław szybko się rozłączył. 
Przy okazji, ku zaskoczeniu Wacława, Teresa nie wie, czym są Illuminati.
Zrobił jej szybką lekcję poglądową.

Koło siódmej rano Wacław dostał telefon od Krystalii, że Karolina Maus się obudziła, mają przyjść i pilnować "tego swojego" (Czerwca).

Tymczasem Hektor nie był w stanie zasnąć. Nadal w uszach dźwięczały mu rewelacje, którymi podzielił się Edwin chwilę przed opuszczeniem rezydencji. Margaret, co do której Hektor był przekonany, że jest psem, w rzeczywistości nim nie była. Pies jest psem. Ale Margaret Blackenbauer po prostu jest nieobecna i nie wiadomo, gdzie jest. I nie wiadomo, gdzie przez cały ten czas była. To też jednak oznaczało, że podczas rytuału, w wyniku którego "Margaret stała się psem" chodziło o coś zupełnie innego. Ale Edwin nie wie, o co. Tylko Otton to wie...
Myśli Marcelina przerwał powrót Jana Szczupaka. Kierowca wrócił sam. Edwin oddał mu swoją komórkę, wyciągnął jakąś z kosza na śmieci i zniknął. Tknięty przeczuciem Hektor spytał, gdzie jest Elżbieta (narzeczona Edwina).
Jan odparł, że zniknęła przed Edwinem.
Czyli ktoś próbuje ponownie uderzyć w Edwina, tym razem skuteczniej.

Hektor wybrał się do aresztu, jednocześnie telefonując do Andrei. Ta jednak - razem z Wacławem - zajmowała się sprawą Karoliny Maus. Odmówiła natychmiastowego przyjazdu - przyjedzie, gdy skończy to, co robi.
W rozpaczy, Hektor udał się do eks-viciniusa, Estery Maus. Co by nie mówić, to była jedyna istota, która próbowała skrzywdzić Edwina przed chwilą obecną.
Estera niewiele jeszcze pamiętała. Chciała pomóc, lecz jej umysł nie był w pełni zregenerowany. Hektor nie mógł tego znieść. Przegrał ze swoją naturą osoby oddającej wszystko rodzinie i bez litości przesłuchał dziewczynę magicznie, nie bacząc na jej stan, rozrywając delikatne korekty, które zrobił Edwin.
To, co odkrył, go przeraziło.

Estera miała dwie osobowości. Zewnętrzną, która oplatała jej prawdziwą osobowość i mogła czerpać z jej pamięci i wszystkiego innego. Typowy przykład mindworma. Tyle, że świeżo osadzonego i brutalne zaklęcia Hektora po prostu rozwarstwiły integrację.
Wszystko wskazywało na to, że osobą, która zaszczepiła mindworma, był Edwin.

Z pamięci Estery udało się Hektorowi dowiedzieć całkiem istotnej informacji. Osobą, która nadzorowała transformację Estery w viciniusa, był Edwin współpracujący z Margaret. Była tam też Mojra, tajemnicza postać, którą kiedyś Edwin przyprowadził jako swoją dziewczynę. Mojra wyraźnie szantażowała Edwina. "Jeśli ty tego nie zrobisz, zrobię to ja, a sam wiesz, jak się to może skończyć".
Hektor ujrzał też w tych wspomnieniach wnętrze jakiegoś dziwnego laboratorium.

Tego było za wiele. Musiał spotkać się z Marcelinem.
Udał się do aresztu, wpłacił za Marcelina kaucję (Sofistię zostawił, by gniła), a gdy Marcelin protestował, Hektor poszedł ostro, mówiąc, że Edwin ma kłopoty i nie ma czasu na zabawy z Sophistią. Marcelin ustąpił. Ustąpił tak daleko, że obiecał Hektorowi wejście do czarnej strefy laboratoriów, kiedy tylko ten będzie miał ochotę. Zrozpaczony Hektor znowu złapał za telefon do Andrei...

Tymczasem Andrea i Wacław znowu trafili do "nagiej strefy" laboratorium w Millenium, tym razem z Czerwcem. (Drużyna go nie chciała, lecz Krystalia świetnie się bawiła widząc jego udrękę) Czerwiec był na skraju wybuchu. Ku poważnemu zaniepokojeniu drużyny, Krystalia nie miała kolii. Była niezdrowo pobudzona i miała koszmarną huśtawkę nastrojów. I wyjątkowo wrednego ratlerka (wskrzeszonego przez Teresę)
Krystalia prawie sprowokowała Czerwca (kraloth by Czerwca dorwał ku uciesze Krystalii), ale Andrea temu zapobiegła.

Karolina Maus była cała roztrzęsiona. Była święcie przekonan, że wytrzymała, że odparła mindworma. Czerwiec nie wyprowadzał jej z błędu, ale mimo stanu Karoliny bardzo twardo naciskał na to, by ta powiedziała wszystko, co wie. Mimo obiekcji Wacława (najkompetentniejszy lekarz na sali), Karolina jakoś zebrała się do kupy i powiedziała najważniejsze informacje. Jej mindworma implantował jakiś lekarz, nie Moriath. Sam Moriath okazał się być kobietą. Karolina opisała też potężny głos pełen władczości i gdy Wacław dał jej próbkę głosu Ottona, potwierdziła, że jest duża szansa, że to był ten głos. Powiedziała też jeszcze jedną, bardzo ważną rzecz. Moriath sterował mindwormami przy użyciu specjalnego stworzenia (celatid) Nawet po śmierci Moriatha, kto kontroluje celatida, kontroluje mindwormy.

Czyli zagrożenie ze strony Moriatha nie minęło. Na to Wacław zadzwonił do Hektora i poprosił go o jakieś zdjęcia Mojry, Margaret... ogólnie, wszystkich dziewczyn (i kobiet) rodu Blakenbauerów. Ten ucieszył się, że Wacław i Andrea pracują nad tą samą sprawą (czyli Edwinem :P) i z radością im przekazał zdjęcia. Na zdjęciach Karolina Maus rozpoznała Mojrę jako Moriatha.

Potwierdziły się przypuszczenia Andrei i Wacława - jest powiązanie między Moriathem i rodem Blakenbauerów. Ale jak ma się do tego Hektor?
Czerwcowi tyle wystarczyło. Wie, że Karolina jest bezpieczna, wie, co może robić dalej, tak więc zostawił Karolinę w rękach Krystalii, a sam wyruszył w swoją drogę.

Tymczasem Wacław i Andrea stwierdzili, że muszą się upewnić, że można ufać Hektorowi, bo tak dalej się nie da, zwłaszcza w świetle tych rewelacji.  Zdecydowali się zwabić Hektora do swojego biura (by systemem zabezpieczeń Krówek go unieszkodliwić), więc go tam zaprosili.

Hektor przed pojechaniem spotkał się z Marcelinem. Ten zdołał przygotować "coś" dla Hektora. Stworzony przy użyciu sympatii z krwi Edwina detektor stanu Edwina. Zgodnie z detektorem Edwin aktualnie był ciężko zatruty różnymi odtrutkami na różne świństwa, oraz trucizną. Min. jadem Dromopod Iserat. Innymi słowy, Edwin jest w tarapatach. Przy takiej dawce jadów ma kilkanaście godzin życia, w czym koło 6-10 przytomności. Marcelin ostrzegł też Hektora o czymś dziwnym - Bestia Edwina jest INNA. Marcelin nie wie na czym ta inność polega, ale krew Edwina zachowuje się nieco inaczej niż krew "przeciętnego" Blakenbauera i zdaniem alchemika Marcelina jest to silnie powiązane z Bestią.

Aha, ludzie Hektora zgubili Edwina. Wyjechał z miasta, gdzieś dość daleko. I jeszcze kilka razy zmienił telefon. SUPER. Hektor spojrzał na Marcelina i poprosił go, by ten coś wymyślił. Hektor wierzy w niego. Marcelin musi COŚ zrobić. 

I z tym optymistycznym akcentem pojechał Hektor z ufnością do biura Andrei i Wacława tylko po to, by wpaść w system zabezpieczeń Krówek i być przesłuchiwanym. Andrea i Wacław ustawili system za mocno; minęły jakieś dwie godziny zanim się ocknął. Oczywiście, Hektor spanikował (EDWIN!!!) co oczywiście nie pomogło całej sytuacji (tymczasem Marcelin temat omówił z Ottonem - Hektor go naprawdę zmotywował).

Hektor próbował negocjować lub się dogadać z Andreą i Wacławem, ci jednak byli nieubłagani. Albo odpowie na ich pytania dokładnie, albo będą kontynuowali śledztwo bez niego, bo nie mogą mu ufać. Bo być może to on jest powiązany z Moriathem. Na "Ale Edwin!" dostał bezduszną odpowiedź "To cię zmotywuje do szybszego podjęcia właściwej decyzji." Hektor skapitulował. Pozwolił przeczytać swoją pamięć. Andrea i Wacław zauważyli, że faktycznie Edwin jest odpowiedzialny za przemianę Estery w viciniusa. Na pewno też Edwin ma coś wspólnego z tym wszystkim. I to jest coś więcej niż tylko "był w pobliżu".

Kolejnym wątkiem, na którym się skupili, był Otton Blackenbauer. A dokładniej: co mu się stało. Niestety, Hektor tego nie wie. Jedyne, co Hektor wie, to to, że on także nie poznał prawdy. Że jego także rodzina trzymała w niewiedzy (kłamali). Jedyne, co wie: Otton i Edwin z pomocą Marcelina próbowali powstrzymać klątwę, która od zawsze wisiała nad rodem Blackenbauerów. Mężczyznom daje bestię. Kobiety jednak nie wpadają w ten stan, ale po przekroczeniu pewnego wieku (dla każdej inny) ich bestia się budzi i już nigdy nie wracają do ludzkiej postaci. I właśnie to próbowali zatrzymać Otton i Edwin. Jednak coś się strasznie nie udało. W wyniku tego rytuału Margaret zmieniła się w psa (teraz wiemy, że nie wiadomo, co się z nią stało), Otton utracił zmysły, Edwin objął dziedzictwo Blackenbauerów i jakimś cudem pomógł ojcu i go trochę ustabilizował.

Wszystko wskazywało na to, że faktycznie kluczem do sprawy jest Edwin a nie Hektor i że Hektor nic nie wie, i że strasznie martwi się o Edwina (i że ma powody do zmartwienia)
Wacław i Andrea postanowili, że można mu zaufać, ale wymogli obietnicę, że da się profilaktycznie "odrobaczyć" (jak Krystalii się trochę poprawi).
Dodatkowo, do czasu aż Hektor przejdzie przez odrobaczenie, otrzyma obrożę. Jeśli spróbuje ich zdradzić, obroża go porazi ("artefakt" dostarczył Wacław, w rzeczywistości jest to placebo)
Bardzo niechętnie, ale Hektor zgodził się na te warunki.

Tymczasem z Hektorem zaczęło dziać się coś niedobrego. Poczuł, jak wysysana jest z niego energia. Poczuł znany sobie "smak" Ottona i Marcelina. Te fale także były wysysane. I wszystkie prowadziły w jednym kierunku: do rezydencji Blackenbauerów. Stwierdzili, że Edwin czy nie - trzeba to sprawdzić, więc pojechali do rezydencji. Jeszcze po drodze do fal Ottona i Marcelina dołączyła jeszcze jedna fala. Nieco niestabilna i niesprawna fala Margaret. W tym momencie Hektor musiał to zobaczyć. Gdy podjechali pod rezydencję, ta była w pełni ufortyfikowana. Żeby wbić się do środka, trzeba by pokonać rezydencję jako żywy organizm, nie tylko pojedynczych magów czy potwory w fosie.
Hektor zdecydował się tam wejść, zobaczyć, co się dzieje. Jednak musiałby iść sam. Andrea przeprowadziła szybki skan i wyszło jej na to, że wiązka energii jest emitowana z rezydencji w konkretnym kierunku. Hektor jednak bardzo chciał wejść do środka.
No dobrze, przejechali przez biuro Andrei, przełożyli zabezpieczający beacon z Teresy do Hektora, wrócili pod rezydencję i Hektor wszedł do środka.

Hektor ledwo poznał rezydencję. Wyglądała jak w czasach świetności Blackenbauerów. Rozjaśniona, tryskająca energią, zadbana... Tymczasem Hektor porusza się z coraz większym trudem, tym większym, im bardziej zbliża się do centrum wysysania. Alfons pomógł mu wejść na górę do pokoju, w którym znajdowali się wszyscy magowie rodu Blackenbauer. Byli tam Otton, siedzący jak za dawnych czasów, Marcelin, ułożony na poduszce i Margaret, taka, jaką Hektor pamiętał, przytulona do Marcelina.
Wszyscy byli nieobecni duchem - stopień wysysania energii był zbyt wysoki. Marcelin, przy swoich bardzo ograniczonych możliwościach próbował wysyłać energię wysysaną od wszystkich członków rodu do Edwina, który tego potrzebował.

Hektor spojrzał na Margaret i chociaż serce mu się krajało, podjął decyzję, że nie dołączy do tego kręgu. On pójdzie pomóc Edwinowi - Edwin go potrzebuje.
Zdjął też Margaret z nogi jedną skarpetkę - będzie co dać Zupaczce, by znalazł Margaret, jeżeli ta zniknie.

Mając znowu Hektora Andrea była w stanie przygotować trochę bardziej złożone zaklęcie. Zaklęcia brało pod uwagę pozycję Hektora, rezydencji oraz promień. To połączone z wykrywaczem stanu Edwina stworzonym przez Marcelina (sympatia) wystarczyło do wytyczenia kierunku. Jedyne, co pozostało zrobić, to jechać w tym kierunku aż znajdą Edwina.

Mając to wszystko, nie było to takie trudne. Niestety, od zniknięcia Edwina minęło bardzo dużo czasu. Edwin, ciężko zatruty, nie mający dostępu do magii (inna trucizna) wpadł w ręce uczniów Moriatha. Będąc całkowicie świadomy tego, że będą chcieli jego zabić, po czym pozbędą się niewygodnych świadków (Elżbiety i alchemika) oraz, że nie jest w stanie ich uratować sam, Edwin przeszedł w formę bestii i zdecydował się usunąć problemy, bo może uda mu się uratować pozostałych.

Rebeka (uczennica Moriatha) była w formie viciniusa; coś podobnego do tego, czym była Estera, zanim Edwin ją wyleczył. Ale nawet w takiej formie nie miała z Edwinem żadnych szans...

I na to przyjechał zespół.
Kolejny podziemny kompleks Moriatha. Gdy zeszli w dół, szybko natknęli się na uciekającego i płaczącego Sebastiana (uczeń Moriatha). Wacław obezwładnił go jednym podstawieniem nogi. Szybko przesłuchany Sebastian powiedział, że Edwin żyje, stało się z nim coś dziwnego (uczniowie Moriatha nie wiedzieli o bestii) i że zabił Rebekę.

Nie skończył tego mówić, gdy Edwin w formie bestii się pojawił. Wacław bezceremonialnie wypchnął Hektora do walki z Edwinem, z Sebastiana wyciągnął, gdzie znajdują się alchemik oraz Elżbieta, po czym sam ruszył na poszukiwanie alchemika (Sebastian wiedział, gdzie jest dokładnie), a Andreę wysłał, by wyciągnęła Elżbietę (tylko Rebeka wiedziała, gdzie dokładnie jest Elżbieta)

Tymczasem Hektor spróbował dogadać się z Edwinem w formie bestii, dotrzeć do niego. Ku jego ogromnemu zaskoczeniu, Edwin odpowiedział od razu, nie zmieniając formy. Edwin posiada kontrolę nad sobą, gdy jest w formie bestii. To jest coś, co jest uznane za niemożliwe. Hektor dał radę przekonać Edwina, że wszystko będzie dobrze, że Elżbieta i alchemik będą wyciągnięci, że widział Margaret (więc Ediwn musi żyć dalej, żeby znaleźć Margaret) i że im szybciej Edwin będzie z Hektorem współpracował, tym lepiej.
O dziwo, ta argumentacja trafiła do Edwina. Wrócił do swojej postaci, po czym osunął się nieprzytomny. 
Dwa tygodnie spalonych kanałów magicznych i parę dni będzie nieprzytomny.
Tymczasem zarówno Andrea jak i Wacław odnaleźli osoby, których szukali.

Zupaczka nie dał rady odnaleźć Margaret. Nigdy nie było jej w rezydencji. To była tylko projekcja Margaret, która w rzeczywistości znajdowała się gdzieś indziej, w związku z tym skarpetka nie pomogła.
Zarówno alchemik jak i Elżbieta przeżyli. Alchemik trafił do biura Andrei i Wacława, zamknięty w systemie zabezpieczeń Krówka.
Elżbieta też trafiła do biura, pod czujną opiekę Teresy. Zwłaszcza, że jest w ciąży. Teresa poinformowała, że ciąża nie jest zagrożona i nie ma problemów.

Rezydencja Blackenbauerów pozostaje odcięta od świata, przynajmniej dopóki nie obudzi się Marcelin.
Znowu nie wiadomo, co zrobił rytuał Ottona, ale ten desperacki rytuał najprawdopodobniej uratował życie Edwinowi. Hektor jako jedyny przytomny członek rodu Blackenbauerów, spróbował objąć dziedzictwo rodu - zostać przez rezydencję uznanym za "prawdziwego Blackenbauera", patriarchę. Jednak rezydencja go nie zaakceptowała. Hektor odrzucił część magiczną siebie, odrzucił część Blackenbauerowską siebie, więc (przynajmniej na razie) rezydencja nie uznała go za patriarchę.

Wacław zrobił z całości pobieżną notatkę dla Iriny, prosząc o spotkanie. Zbyt dużo się dowiedział, by nie poinformować Iriny o sytuacji.

Tymczasem źródła Andrei doniosły, że Grzegorz Czerwiec spróbował przedsięwziąć działania przeciwko rodowi Blackenbauerów - a dokładniej próbował zorganizować najazd na rezydencję. Umotywował to Moriathem i nowo odkrytymi dowodami.
Agrest zablokował tą prośbę. Gdy Grzegorz zażądał przeprowadzenia konklawe terminusów nie tylko SŚ w sprawie tego nie zamkniętego tematu o wysokim priorytecie, Agrest stwierdził, że są w stanie podwyższonej gotowości bojowej z uwagi na potencjalne zagrożenie Inwazją, w związku z czym konklawe nie może zostać powołane. Zgodnie z przepisami stan podwyższonej gotowości może trwać do piętnastu dni od ogłoszenia. Innymi słowy, w najgorszym dla Hektora wypadku, do rezydencji  nie przyjdzie banda terminusów w chwili, w której on nie jest w stanie ich wpuścić.

Andrea widzi wyraźnie, że Agrest osłania Blackenbauerów. Ale dlaczego..?


## Tablica z misji:

![](140401_MojraMoriath.jpg)

# Zasługi

* mag: Hektor Blakenbauer, który poruszył niebo i ziemię by uratować Edwina przed Uczniami Moriatha, nawet dał się torturować Andrei i Wacławowi i dał przeczytać sobie pamięć. I rozbił związek Sophistia x Marcelin.
* mag: Andrea Wilgacz, która z Wacławem wymusiła od Hektora prawdę. Jako analityk magiczny dała radę pomóc Hektorowi odnaleźć Edwina.
* mag: Wacław Zajcew, który z Andreą wymusił od Hektora prawdę. Zdobył zaufanie Teresy i przechwycił Sebastiana. 
* czł: Tadeusz Czerwiecki, kopaliński policjant i znajomy Hektora, który powiedział Hektorowi, że przymknęli Marcelina i Sophistię.
* mag: Marcelin Blakenbauer, wsadzony do aresztu za obelżywe graffiti i zmuszony przez Hektora do uratowania Edwina; doprowadził do uruchomienia Ottona i URATOWAŁ Edwina.
* czł: Amelia Eter, tu: Blakenbauer :P. Zrobiła obelżywe graffiti na murach posterunku policji, więc Hektor zostawił ją w więzieniu; przez działania Hektora Marcelin ją opuścił.
* czł: Jan Szczupak, który nie tylko wiezie Edwina tam, gdzie ten chce ale też pomaga Edwinowi wymknąć się ze wszystkich możliwych namierzeń przez Hektora. Powiedział Hektorowi, że Edwin i Elżbieta zniknęli.
* mag: Edwin Blakenbauer, który poszedł na pewną śmierć by uratować swoją kochaną Elżbietę. Uratowany przez dostarczenie energii przez ród, zmienił się w Bestię i zamknął problem Uczniów Moriatha.
* mag: Teresa Żyraf, która ma koszmary senne korelujące ze złym stanem Krystalii; dodatkowo, Krystalia zmuszała ją do eksperymentów ze wskrzeszaniem.
* mag: Krystalia Diakon, odwracająca mindwormy na lewo i prawo oraz coraz bardziej się destabilizująca mentalnie. Nie nosi kolii.
* mag: Grzegorz Czerwiec, bezwzględnie próbujący odzyskać Karolinę i dowiedzieć się o co chodzi. Następnie chciał uderzyć w Blakenbauerów (po rewelacjach Karoliny), ale zatrzymał go Agrest.
* mag: Karolina Maus, święcie przekonana że odparła mindworma (lol), która zdradziła Andrei, Wacławowi i Czerwcowi wszystko co pamiętała o Moriacie.
* mag: Mojra, (przeszłość) która dowodziła jako Moriath, potencjalnie szantażując Edwina i zmusiła go i Margaret do zmienienia Estery w viciniusa.
* mag: Otton Blakenbauer, który po prośbie Marcelina powrócił na moment... by uratować Edwina potężnym rytuałem uruchamiającym moc Rezydencji.
* mag: Margaret Blakenbauer, echo. Widmowa projekcja która dostarczyła energii Edwinowi wtedy kiedy najbardziej jej potrzebował.
* mag: Rebeka Piryt, która porwała Edwinowi dziewczynę i go otruła; potem w formie viciniusa próbowała stawić czoło Edwinowi w formie Bestii i została rozerwana na kawałki (KIA).
* mag: Sebastian Tecznia, który wraz z Rebeką porwał Elżbietę Niemoc i otruł Edwina; potem gdy Edwin przeszedł w formę Bestii, uciekł i uratował go (oraz złapał do niewoli) Zespół.
* mag: Waldemar Zupaczka, który nie potrafi odnaleźć Margaret Blakenbauer; po prostu nie jest w stanie.
* mag: Marian Agrest, który spokojnie zablokował wszystkie działania Grzegorza Czerwca i zablokował możliwość najazdu na Blakenbauerów. Z jakiegoś powodu chroni Blakenbauerów.
* mag: Estera Piryt, której mentalny kokon zniszczył przestraszony Hektor i odkrył świeżego mindworma zaimplantowanego przez Edwina. Znowu w mentalnej ruinie po działaniach Hektora.
* czł: Elżbieta Niemoc, którą porwali Uczniowie Moriatha jako pułapkę... co sprawiło, że Edwin odrzucił wszelkie człowieczeństwo. Trafiła do Bazy Detektywów.
* mag: Alfred Kukułka, alchemik porwany przez Uczniów Moriatha który też został uratowany przez Zespół. Trafił do Bazy Detektywów.
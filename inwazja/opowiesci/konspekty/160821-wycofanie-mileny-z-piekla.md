---
layout: inwazja-konspekt
title:  "Wycofanie Mileny z piekła"
campaign: powrot-karradraela
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [161113 - Świeca nie zostawia swoich (AW)](161113-swieca-nie-zostawia-swoich.html)

### Chronologiczna

* [160724 - Portal do EAM (SD)](160724-portal-do-eam.html)

## Kontekst ogólny sytuacji
## Punkt zerowy:

Generatory Kompleksu powoli zawodzą. Zapas żywności dla viciniusów zaczyna się kończyć. Kompleks Centralny opanowany jest przez formę wojny domowej. 
- W kompleksie Centralnym jest około 500 magów i 100 viciniusów. Szlachta ma poniżej 100 magów. Koło 200-300 magów jest chorych na Irytkę.
- Lojaliści Ozydiusza ufortyfikowali się w Pionie Ogólnodostępnym
- Chorzy na Irytkę są w Technoparku Max Weber
- Tworzą się frakcje:
.... Szlachta, która ma wreszcie okazję przejąć władzę
.... Kurtyna, w opozycji do Szlachty. "Każdy ale nie oni".
.... Bankierze, którzy stanowią alternatywny (prawy) ośrodek władzy
.... Zajcewowie, którym dość pasuje chaos i wolność
.... Neutralni, którzy chcą kontynuacji swojej pracy i mają wszystko gdzieś
.... Stabilizatorzy, któzy chcą doprowadzić gildię do jak najlepszego działania niezależnie od konsekwencji
.... Lojaliści Ozydiusza, chcący zapewnić, by cele Ozydiusza były spełnione i wszystko działało zgodnie z planem
.... Wolni Magowie, którzy mają dość potęgi wielkich rodów

Tymczasowo Bankierze opanowali Magazyn Artefaktów Niebezpiecznych. Jednak Dagmara skupiła swoje siły na Pionie Dowodzenia Kopalinem; dokonała "uczciwej" wymiany. Magowie rodu Bankierz nie przejmują się specjalnie tym, że Szlachta opanowała Pion Dowodzenia; nie mają uprawnień by korzystać z elementów Kompleksu Centralnego.

Siluria martwi się stanem Mileny Diakon. Dodatkowo, nie chce - bardzo nie chce - przekształcać tien Judyty Karnisz pod kontrolę Wiktora. Jeszcze bardziej jednak nie chce mu podpaść.

## Misja właściwa:

Dzień 1:

Wiktor nadal jest wściekły na Judytę jak na nikogo wcześniej. Stracił "swoje" EAMki a ona praktycznie plunęła mu w twarz. Tak więc Siluria i Elea są przy poduszce uśpionej Judyty. Elea ma kryształ Mausów. Elea zaznaczyła, że Judyta była kiedyś uczennicą, narzędziem i zabawką defilera. Powinna być łatwa do przywrócenia do poprzedniej formy, sprzed pomocy ze strony Ozydiusza. Siluria w szoku - Elea wie dziwne rzeczy. Elea zachowuje się inaczej niż kiedyś, zupełnie inaczej, jest... inna. Spróbowała ją odczytać. Co się zmieniło dla Elei?
Niestety, Siluria nie doszła do tego. Nie udało się.

Elea - za namową Silurii - przyprowadziła Biankę. Bianka zaczęła protestować - nie pomoże w korupcji terminuski. Ma godność. Elea powiedziała, że Bianka ma złudzenia wolnej woli i niech tego nie robi, niech nie staje JEMU na drodze, bo ON zawsze wygrywa. Bianka krzyknęła - Estrella nie byłaby dumna z działań Silurii; cały czas chroni i mówi, że Siluria jest "ta dobra". Estrella miała być tą, która wysadzała ten generator, ale Judyta powiedziała, że tylko ona ma szanse. I gdyby to Estrella była w tym miejscu a nie Judyta, czy Siluria też by to zrobiła?
Elea powiedziała, że tak. Bo "jego" wola jest nieskończona.

Siluria poszła w kierunku "od jak dawna tu jesteś? Nie widziałaś jego..." - pokazuje strach połączony z fascynacją, nie oddanie, ale... jak ćma do ognia. Przekonała Biankę od ręki. 

"To... to jest chore! Niszczycie wszystko! To nie jest Świeca!" - Bianka, bezsilnie
"Ona jest z KADEMu" - Elea do Bianki

Siluria zrozumiała co jest nie tak z Eleą. Elea jest w trybie "jestem Mausem, jestem elementem roju, to jest moje zadanie". To jest tryb "rozkazano mi, a ja muszę". To jest efekt wyuczony - Mausowie nie potrafią się oprzeć Karradraelowi, więc mają takie podejście w kontaktach ze swoim Władcą. Siluria rozumie już - Elea "odeszła za daleko". Elea próbuje chronić co jest w stanie, ale gdyby "Karradrael" rozkazał jej zabić Silurię, zrobiłaby to.
Jest to coś, co rozumieją jedynie Mausowie.

Siluria powiedziała Biance, że ma dwa wyjścia - pomoże (robiąc najmniejszą możliwą krzywdę terminusce), lub nie pomoże. A terminuska może ucierpieć nieodwracalnie.
...Bianka się złamała. Siluria po prostu ją wyrolowała, załatwiła i doprowadziła do jej całkowitego pokonania.

Bianka i Elea skupiły się na analizie wzoru Judyty i ewentualnym zdejmowaniu jej zabezpieczeń. Siluria z nimi to obserwuje. Ozydiusz brał czarodziejki, których nikt nie chciał; czarodziejki, które były "bezużyteczne" dla Świecy i najlepiej byłoby je po prostu usunąć - jak Iza czy Judyta. Ozydiusz dawał im szansę i zapewniał im jakieś bezpieczeństwo czy uwarunkowanie (w wypadku Judyty). I Bianka teraz te magiczne systemy blokujące, te rzeczy które pomagały Judycie trzymać się w ryzach zaczęła rozmontowywać; zabezpieczenia, elementy samokontroli, rozpraszanie przez hipernet... Elea pomaga Biance by nie skrzywdzić terminuski. Siluria natomiast obserwuje - by znaleźć punkty, gdzie może wstrzyknąć Judycie "trigger". Coś, za pomocą czego terminuska nie będzie "stracona".

Prace trwały długo, jednak w pewnym momencie Siluria została wezwana przez Wiktora. Wieczór, więc nie chce spać sam. Zabrakło mu jego ulubionej zabawki.
Siluria się przygotowuje specjalnie dla niego - tu i ówdzie jakiś feromonik, odpowiednie ubranko... a przy okazji, chciałaby podpytać o co nieco ;-).

Wiktor jest w dominującej pozie. Silny, zestresowany, rozkoszuje się nową władzą dostarczoną mu przez Dagmarę (przekazała mu dowodzenie Kopalinem, acz nie ma kontroli nad głównymi systemami przez brak uprawnień). Siluria chce to wykorzystać - teraz ma ochotę zrobić na nim niesamowite wrażenie. Najsilniejsze ever. 
Udało jej się. Wiktor - w tym stanie, w tej chwili - jest całkowicie pod kontrolą Silurii. On myśli że dowodzi, ale się beznadziejnie zakochał i nie chce tylko jej ciała a chce CAŁĄ JĄ. I nie zdaje sobie z tego sprawy. To daje od tej pory WSZYSTKIE testy Silurii na Wiktora #6.

Wiktor kazał Silurii się rozbierać i klęczeć przed nią - Diakonka odmówiła. Będzie jak chce Wiktor, ale nie tak. I w ten sposób, po raz pierwszy, Wiktor NIE był górą w sypialni - i nawet się nie zorientował. Zmieniło to ich power level. 
...gdyby nie cholerna Dagmara, to właśnie Szlachta by upadła...

Siluria powiedziała mu, że Bianka i Elea nie są jej potrzebne. Siluria sama poradzi sobie z Judytą. Natomiast Elea niech zajmie się Irytką - nie ma lepszych. Po prostu niech Elea raz na jakiś czas wpadnie, sprawdzić postępy. Wiktor się zgodził. Powiedział jednak, że ma problem - musi wynegocjować dwustronną sieć Bramą między Kompleksem Centralnym a Technoparkiem, gdzie znajdują się chorzy na Irytkę magowie. Niestety, inne frakcje najpewniej zbombardują te próby; dwustronny portal umożliwiłby wprowadzenie wsparcia dla dowolnej z frakcji do Kompleksu. Jednostronny wyrzuciłby Eleę i uniemożliwiłby jej powrót. Podpięcie generatorów do Bramy nie jest trudne (Bianka), ale to jest zbyt łatwe do sabotowania.

Dodatkowo, kończą się zapasy żywności dla viciniusów a z uwagi na niski poziom energii hydroponika nie jest na pełnej mocy - magowie dostaną mniej smaczne jedzenie. Na razie dostają jedzenie...

Siluria martwi się losem kuzynki - ta jest w szpitalu i się źle czuje i w ogóle. A tak w ogóle ona chce w szpitalu znaleźć coś co pomoże jej pracować nad Judytą. Wiktor się zgodził. Niech Siluria następnego dnia spojrzy do szpitala. Wiktor jeszcze zauważył, że jeśli będzie miała problemy, niech powie. Dagmara chciała uderzyć w szpital wcześniej, ale Wiktor przekierował ją do "pożytecznych rzeczy".
WTF Dagmara chce od skrzydła szpitalnego?

Siluria jeszcze przekonała Wiktora, by ten pozwolił jej skontaktować się z Estrellą. Trzeba ratować viciniusy Świecy przed głodem. Od tych viciniusów często zależy czy Świeca w ogóle żyje...

Dzień 2:

Wiktor z Alicją zaczynają poważne negocjacje odnośnie stawiania Bram i typów Bram do Technoparku. Viciniusy mają JUŻ obniżone racje żywnościowe. Za 3 dni zapanuje głód wśród niektórych viciniusów (i tak niedojadają). 

Pierwsze kroki Siluria skierowała do szpitala. Jest elegancka i wzbudza uwagę. Skrzydło szpitalne jest w pełnym wewnętrznym lockdownie - dowodzi nim Edward Bankierz (więc jest w pełnym lockdownie). Siluria została poproszona (w strefie kwarantanny) o rozebranie się; chcą sprawdzić, czy wszystko jest bezpieczne i nie ma czegoś niewłaściwego. Cóż, Diakonka zrobiła pokaz "niewinnie i przypadkowo". Zawsze to bonus do przyszłych działań :>.

Silurię odebrała Dalia Weiner, spłoniona jak piwonia. Zapytana o Milenę, Dalia odesłała Silurię do Jakuba Niecienia - lekarza prowadzącego. Tyle, że z wiedzy Silurii Jakub Niecień to mag specjalizujący się w przypadkach skrajnym, kontaminujących, niebezpiecznych. Czemu on zajmuje się Mileną? Dalia zaprowadziła Silurię do gabinetów lekarzy - do tien Niecienia.

Siluria wyciągnęła od Dalii, że ta jest lojalistką. Ona słucha Świecy, a dla niej Świecą jest dyrektor szpitala - tien Bankierz. Szpital ma dostęp do rzeczy z Kompleksu Centralnego. Plotka niesie, że lockdown jest spowodowany tym, że Edward Bankierz boi się, że ktoś się wedrze i zrobi krzywdę pacjentom - lub ich wypuści - lub głodne viciniusy zapolują.
Bo tak ogólnie to nie ma konieczności robienia lockdownu. I tak większość lekarzy jest w Technoparku; tam są chorzy na Irytkę. Tu nie ma Irytkowców.
W szpitalu poza Mileną jest jeszcze Kermit z Diakonów; Kermit jest przepalony magicznie po jednej z niedawnych akcji. Tu nie ma miejsca na frakcje. Szpital to szpital.

Siluria chwilę poczekała na tien Niecienia, po czym lekarz się z nią spotkał. Niecień powiedział, że Milena ma problem pasożytniczej matrycy - jest inna matryca, która próbuje nadpisać jej wzór. Rozwiązaniem jest konsekwentna transfuzja matrycy Diakonów (czyli po to jest tu Kermit). Niecień powiedział, że takie przypadki miały miejsce w przeszłości; ma dostęp do odpowiednich archiwów. Gość jest no-nonsense, zdecydowanie chce pomóc swojej pacjentce, zdecydowanie nie chce mówić rzeczy mogące wzmóc panikę.
I wie, że Milena jest zainfekowana boską matrycą lub czymś tego typu.
Niestety, Siluria ma paskudne wrażenie, że Milena nie ma paru miesięcy spokojnych transfuzji. Nie w tym miejscu. Musi wrócić do Millennium.

Siluria powiedziała, że Kermit na skali krwi Diakonów jest '6'. Milena to '4'. Ale Siluria to '10' - jest purebreed. Niecień stwierdził, że w takim razie Silurii krew będzie dużo lepsza. Zwłaszcza, że mówimy o boskiej matrycy (słowa Niecienia). Co ważne, użycie czystej krwi zdecydowanie usprawni stan zdrowia Mileny - Milena będzie bardziej Mileną niż kiedykolwiek.
Siluria umówiła się z tien Niecieniem na następny dzień. Zostanie jeden dzień w szpitalu i skalibruje się z Mileną by jej pomóc (następny).

Czas wracać do pracy. Siluria wraca do Skrzydła Sowińskich - i została nagle zaatakowana. Dźgnięta przez maga zatrutą bronią; zanim straciła przytomność krzyknęła wołając o pomoc. Tracąc przytomność zobaczyła jeszcze, jak nad nią zaczynają się przepychanki słowne i wzywany zostaje terminus...

Dzień 3:

Siluria otwiera oczy. Nad nią stoi Elea. Siluria leży w łóżku, nieubrana. Siluria jest po ciężkim detoksie; Elea ją przepłukała. Siluria jest słaba i ma niewiele sił; nie kwalifikuje się do czarowania ani do niczego poważnego. Elea jej powiedziała, że jakiś mag chciał jej zrobić poważną krzywdę. Czemu Siluria? Elea wyjaśniła, że to młody mag - wierzy w całą tą propagandę KADEM vs Srebrna Świeca. Celował w czarodziejkę KADEMu, która chciała zniszczyć Srebrną Świecę. Wierzył, że jeśli Siluria zostanie unieszkodliwiona, Świeca będzie jak dawniej. Bo to wszystko wina KADEMu.

Co to był za środek? "Brudna bomba" - z rozmaitych roślin z ogrodów Świecy; zmieszanych przez studenta biomagii.

Elea sprowadziła Wiktora na prośbę Silurii; Siluria nie zgadza się na leżenie w łóżku tu, skoro może leżeć w łóżku w skrzydle szpitalnym i kalibrować pod kątem Mileny.

Wiktor przyszedł do Silurii z miną anioła śmierci. Bankierze trzymają łotra i nie chcą go wydać. On ich rozszarpie na kawałki - zaatakuje i zmiażdży Bankierzy. Siluria powiedziała, że jest on nieistotną płotką; poddał się propagandzie i winny jest ten, kto ową propagandę rozsiał.

Silurii to nie przeszkadza; jej chodzi o to, by mogła trafić tam, na kalibrację dla Mileny. Wiktor, ostro przekonywany przez Silurię, się zgodził. Siluria będzie miała obstawę i trafi do Skrzydła Szpitalnego. Odprowadzi ją osobiście Dagmara i Wioletta. W czym Wioletta zostaje przy Silurii i nie odstępuje jej ani na krok (łącznie z ubikacją) a Dagmara wraca.

Bez żadnego problemu się udało. Wioletta zanudziła się na śmierć, tien Niecień pomógł Silurii na linii Siluria - Milena.
A przy okazji odtruli ją jeszcze lepiej.

Dzień 4:

Początek dnia czwartego zastał Silurię w Centrum Dowodzenia Kopalinem, z Wiktorem dowodzącym Kompleksem. Siluria, Dagmara, Wioletta przy nim. 
Wiktor jest zaskoczony - Dagmara skalibrowała Centrum Dowodzenia pod kątem Wiktora. Centrum akceptuje Wiktora jako przełożonego Kompleksu Centralnego, acz nie ma dostępu do wszystkich funkcji Lorda Terminusa - ale ma statusy i sygnały, na które rzuciła się Alicja (by ocenić stan Kompleksu).

Wiktor jest jeszcze odrobinę przytłoczony tym wszystkim i nie zauważył, jak Dagmara wydała polecenia. Siluria ma WPIERW uzyskać dostęp do Archiwów, POTEM załatwić sprawę z Estrellą, by nażywić viciniusy a na końcu zacząć pracę nad Judytą. Siluria zauważyła połączenie nagrody, groźby i kto tu tak naprawdę dowodzi. Ale teraz ma dostęp do Mileny i teraz ma plan jak ją wydobyć; to powoduje, że jest skłonna zrobić wszystko co Dagmara chce - dla bezpieczeństwa swojego, Wioletty i Mileny.

Siluria spotkała się z Estrellą. Z jednej linii komunikacji - normalna rozmowa handlowa. Z drugiej - podając rękę Estrelli Siluria przekazała trochę swojej krwi (zadała sobie rankę). Z trzeciej - przekazała, że z Mileną nie jest dobrze, ale może być lepiej acz wymaga transfuzji i im czystsza krew tym lepsza (Estrella wie jakiego typu krwi jest Siluria). A w którymś momencie w rozmowie pojawia się nawiązanie do 'blood summon' - niech w odpowiednim momencie Estrella (Amanda) zrobi 'blood summon' krwi Silurii. To powinno przywołać zarówno Silurię jak i Milenę.
Ale głównym tematem jest negocjacja. Wiktor wyznaczył bardzo uczciwą cenę, Siluria ją zaniża, Estrella ją zawyża i w końcu ogrywają Świecę na korzyść Rodu Diakonów. 
Bo tak.

Z Archiwami nie było problemów - Siluria chciała dostać dostęp z koleżanką. Sęk w tym, że koleżanką jest Dagmara...
Po tym, jak urok Silurii dał Dagmarze dostęp do Archiwów, Siluria poszła do szpitala zajmować się Mileną a Dagmara została w Archiwach.

Później, Siluria zaczęła prace nad Judytą. Dzięki poprzedniemu studiowaniu co zrobił Ozydiusz, była w stanie znaleźć co ONA ma zrobić by nie było widać a jednocześnie by Judyta za bardzo nie ucierpiała. Owszem, będzie zniewolona - ale będzie trigger i Judyta wróci do bycia wolną.
Teraz tylko to wszystko zrobić. To zajmie trochę czasu, ale wszystko już jest w zasięgu...

# Progresja

* Dagmara Wyjątek: straciła Milenę Diakon
* Dagmara Wyjątek: zdobyła kontrolę nad Kompleksem Centralnym
* Milena Diakon: transfuzje dają jej możliwości działania jak kiedyś, aczkolwiek krótkoterminowo
* Siluria Diakon: uzyskała kontakty z Wiktorem Sowińskim; jest praktycznie ściśle w jej obszarze wpływów
* Judyta Karnisz: złamana i zindoktrynowana do Wiktora Sowińskiego

# Streszczenie

Siluria zmusiła Biankę do skorumpowania Judyty ku protestowi powyższej. Elea Maus "odpłynęła" - wierzy, że rozkazuje im Karradrael i że to on stoi za rozkazami Dagmary. Siluria też zmusiła Wiktora do proszenia ją; zmieniła między nimi relację. Kończy się jedzenie dla viciniusów w Kompleksie Centralnym; Siluria załatwiła jedzenie z Millennium przez Estrellę. Dostała się też do Mileny Diakon i dowiedziała się, że ta ma problem "pasożytniczej boskiej matrycy". Jako, że Dagmara interesuje się Mileną... Siluria opracowała plan Krwawej Teleportacji Silurii i Mileny. Jeszcze pomogła Dagmarze dostać się do archiwów i była świadkiem tego, że Wiktor przejął centrum dowodzenia Kompleksu, a potem - uciekła. Wpierw jednak zostawiając w Judycie sygnał by móc ją uratować i odwrócić cały proces... ;-).

# Zasługi

* mag: Siluria Diakon, która tak manipulowała wydarzeniami, że uratowała Milenę i dała wszystkim to, czego chcą nie robiąc nic naprawdę strasznego
* mag: Elea Maus, która straciła nadzieję; poddała się przeznaczeniu nie próbując już nawet z nim walczyć i jedynie ratując to, co jest w stanie
* mag: Bianka Stein, katalistka próbująca działać zgodnie ze swymi przekonaniami i ideałami; złamana przez Silurię i Eleę - jak nie pomoże, Judyta najpewniej umrze.
* mag: Wiktor Sowiński, pozornie dowodzący Kompleksem Centralnym i ślepy na to, że oddał Dagmarze władzę. Zakochany beznadziejnie w Silurii, uzależniony od niej.
* mag: Edward Bankierz, dowodzi skrzydłem szpitalnym w Kompleksie Centralnym SŚ; wprowadził lockdown bojąc się o dobro pacjentów.
* mag: Jakub Niecień, lekarz prowadzący Milenę. Specjalizujący się w przypadkach kontaminujących, niebezpiecznych, beznadziejnych. Wprowadził prawidłowe leczenie.
* mag: Milena Diakon, chora na "pasożytniczą boską matrycę", z krwią transferowaną od Silurii i Kermita Diakon by ją ratować
* mag: Dagmara Wyjątek, która praktycznie przejęła kontrolę nad Kompleksem Centralnym przy nie zauważeniu ze strony Wiktora
* mag: Judyta Karnisz, indoktrynowana i łamana przez kombo Siluria / Elea; jednak Siluria zostawiła trigger by móc odwrócić zbrodniczy czyn
* mag: Estrella Diakon, kontakt komunikacyjny między Amandą a Silurią; terminuska Lojalistów Ozydiusza w Kopalinie tymczasowo pod dowodzeniem Anety Rainer
* mag: Kermit Diakon, terminus zostający w skrzydle szpitalnym by pomóc kalibrować wzór Mileny lekarzowi Jakubowi Niecieniowi

# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Centrum
                        1. Kompleks Centralny Srebrnej Świecy
                            1. Kompleks pałacowy
                                1. Skrzydło Sowińskich
                                    1. Buduar Wiktora
                            1. Pion dowodzenia Kopalinem
                                1. Biuro Lorda Terminusa, objęte przez Wiktora Sowińskiego
                            1. Pion ogólnodostępny
                                1. Sala spotkaniowa
                            1. Pion szpitalny
                                1. Gabinety lekarskie, gdzie znajduje się gabinet tien Niecienia
                                1. Pracownia korekty wzoru, gdzie kalibrowany był wzór Silurii ze wzorem Mileny
                            1. Archiwa
                                1. Czarne Archiwa
                            1. Pion mieszkalny
                                1. Kwatery magów
                                1. Kwatery viciniusów
                                1. Kwatery gości
                                1. Kwatery służby
                            1. Pion logistyczny
                                1. Brama
                                1. magazyny
                                1. Hydroponika
                                1. Energetyka
      
# Wątki



# Skrypt

- Dagmara włącza system defensywny i podpina go do swojego bloodsuit
- Wiktor chce wyprodukować technologie w Pionie badawczo-produkcyjnym; opanowanym przez sojusz Bankierzy i Stabilizatorów
- Formalną kontrolę nad Kompleksem przejmuje Wiktor. Nieformalnie nie ma sił by cokolwiek trzymać
- Dagmara znika przechwycić Pion Dowodzenia
- Trzeba postawić tymczasowy portal między Technoparkiem a Kompleksem Centralnym, lub nie będzie się dało ich badać
- Lojaliści Ozydiusza muszą zostać zmiażdżeni

# Czas

* Dni: 4
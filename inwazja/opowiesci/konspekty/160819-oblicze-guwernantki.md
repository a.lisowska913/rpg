---
layout: inwazja-konspekt
title:  "Oblicze guwernantki"
campaign: powrot-karradraela
gm: żółw
players: kić, dust
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [161231 - Eskalacja Czelimina, eskalacja Andrei... (AW)](161231-eskalacja-czelimina-andrei.html)

### Chronologiczna

* [160810 - Zaszczepić Adriana Murarza! (HB, KB, AB, DK)](160810-zaszczepic-adriana-murarza.html)

## Kontekst ogólny sytuacji

Dust:

Wiktor chce armię istot sprzed Porozumienia Radomskiego. Stworzymy taką armię, ale nie wykorzystamy do tego jego schematów tylko nasze i naszą krew. Stworzymy coś, co po ziemiach polskich nie chodziło już od bardzo dawna - stworzymy oddział Blakenbauerów. Zupełnie niekompatybilny z tą wersją Spustoszenia, na które byłby podatny pierwotny oddział, całkowicie lojalny i oddany rodowi. Jeńcy Wiktora pozostaliby magami, a później dostali wybór powrotu do poprzedniego rodu, lub pozostania Blakenbauerami.
Niestety jest tylko jeden sposób, aby to zrobić szybko i skutecznie - musimy działać ramię w ramię z Tymotheusem i Hektor jest na to gotowy, aby ocalić jeńców przed losem gorszym od śmierci.
Więc spyta się Klary, czy wie jak nawiązać kontakt ze swym ojcem rodowym.
Doszedłem do wniosku, że musimy zacząć działać i to w sposób, jakiego nie przewidzą nasi przeciwnicy. Dalej już tylko szturm na świecę zamaskowany dostarczaniem jednostek i eliminacja Intensyfikatora. W użyciu broń Vladlieny do walki ze spustoszeniem (EMP). Powinno działać również na mechanicznych terminusów. Do tego pająk fazowy przemyci BlackOpsy do generatorów kompleksu centralnego w celu sabotażu intensyfikatora (pomoże tu katalistka).

## Punkt zerowy:

Działania Zajcewów i sił Andrei doprowadziły do tego, że The Governess musiała wycofać się z farmy agroturystycznej i utraciła Patrycję. Zajcewowie i Andrea skupili się na innych tematach; jednak najpewniej The Governess zostawiła jakieś ślady. A badanie śladów to coś, w czym siły śledcze (i Hektor) są szczególnie skuteczne. Istnieje okazja odpłacenia The Governess, dowiedzenia się, kim jest i o co jej chodzi. I szkoda byłoby nie wykorzystać sytuacji. 

## Misja właściwa:

Dzień 1:

Jeńcy zostali dostarczeni do Rezydencji. Rezydencja ich przeskanowała - nie są problematyczni. Hektor był świadkiem kłótni Edwina i Ottona - Edwin się nie zgadza. Hektor się wpiął w temat; zaproponował transformację nie w Oddział Trzygław a w Blakenbauerów, ale tych sprzed Porozumień Radomskich.

"Oryginalna matryca Blakenbauerów, zanim założyliśmy sobie kagańce?" - Otton

Dobry pomysł. Edwin i Margaret mają zbadać jak sprawić by ci "nowi Blakenbauerowie" wyglądali jak Oddział Trzygław.
Hektor i Klara zaproponowali że pójdą tam gdzie była The Governess i dowiedzą się wszystkiego na jej temat. Otton się zgodził - ale powiedział, żeby rozważyli wsparcie ze strony Netherii.

Hektor i Klara przygotowują siły - jakieś dwie płaszczki, Dionizy, Alina. 

Edwin zgłosił Ottonowi ciekawą obserwację - Oddział Trzygław nie wymaga badania krwi magów, ale ta sprawa z Blakenbauerami wymaga dokładnego badania krwi na poziomie biomagicznym. Okazuje się, że ich wszystkie wzory są częściowo uszkodzone; ktoś walnął im po matrycach. Edwin może to sprawdzić dokładniej, ale to wymaga opóźnienia. Otton się zgodził. Najwyżej transformacja trochę poczeka. Edwin obiecał wyniki następnego dnia.

Kto ma pogadać z Netherią i jak? Cóż, Klara wysłała Alinę i Dionizego. Neti nie jest fanką Blakenbauerów po sprawie z Wandą Ketran...
W godzinach po pracy, Alina i Dionizy poszli odwiedzić Netherię nie zapowiadając się. Business casual. A tam... Neti otworzyła w samym szlafroku. Wyraźnie na kogoś czeka. Po lekkiej konfuzji przyszła inna para - i Netheria zaprosiła wszystkich do dużego pokoju.

Tam dwie pary, które przyszły na terapię erotyczną i są zdziwione sobą nawzajem. No i Dionizy i Alina. Netheria powiedziała spokojnie i z uśmiechem, że terapia ma połączyć ich DZIECI a nie ich. A Dionizy jest po to, by się stąd nie oddalili...
... kilka godzin później problem biznesowy został rozwiązany i Neti skupiła się na Dionizym i Alinie. Po usłyszeniu sprawy powiedziała, że pomoże - za to, że dostanie Wandę Ketran. Chce ją odzyskać.

Wrócili do Rezydencji. Blakenbauerowie nie mogą oddać Wandy - ta stanowi link pomiędzy nimi a Tymotheusem. Zamiast tego, zaproponowali:
=== Hektor zapozna Netherię ze swoimi kontaktami w świecie ludzi (burmistrzowie, policja, muzea...)
=== Netheria ma możliwość wykorzystania Dionizego i Aliny jeśli ci nie robią czegoś dla Blakenbauerów

Netheria się zgodziła. Chciała uratować Wandę, naprawdę chciała, ale The Governess musi być zatrzymana a to co dostała to i tak więcej niż się spodziewała. Znaczy: Blakenbauerowie są albo zdesperowani, albo The Governess naprawdę im podpadła.

Wieczorem Edwin powiedział Ottonowi, co wykrył z tą krwią. Wzór tych magów został zdestabilizowany krwią Mausów; są w fazie pośredniej między bycia magami swego rodu (silniej) a Mausami (słabiej). Zapytany jak by to wpłynęło na Trzygław, powiedział, że nie powinno... ale krew Blakenbauerów, zwłaszcza z silnego nośnika krwi (Marcelin lub Hektor) rozwiąże ten problem. Zamaże wzór oryginalny i wzór Mausów.

Trzeba kupić Marcelinowi czekoladę i do roboty. Tran-sfu-zja.

Dzień 2:

Z rana Netheria przyszła do Rezydencji, jest bardzo zaciekawiona jak opuści Kopalin. Zapoznała się z resztą drużyny; tam gdy porozmawiała z Hektorem o tym, że chciałaby uratować Wandę ten powiedział, że trzeba usunąć Arazille. A nie wie jak. Neti powiedziała, że poszuka.
Opaska na twarz i przeszli na drugą stronę portalu, skąd przejechali się do Kotów. Tam Netherii zdjęli zasłonę.

Zainstalowali się w remizie strażackiej, potem pojechali do farmy agroturystycznej gdzie bazę miała The Governess. Mieszkańcy farmy zginęli podczas starcia z The Governess - dwójka starszych ludzi dorabiających sobie agroturystyką.

Netheria i Klara poczuły jak zginęli ci ludzie. The Governess walczyła przeciw Andrei i Zajcewom, wykorzystywała ludzi jako baterie do Magii Krwi. Zajcewowie zabili ludzi a defilerce udało się uciec. Netheria jest zaszokowana, że tak silny defiler ma kontrolę nad sobą. Może Spustoszenie ją kontroluje? Ale chwilę potem nastroje czytane przez Netherię jeszcze bardziej ją skonfundowały - self-loathing, czysta nienawiść do Magii Krwi, ale... kilka osobowości. Trzy w jednym ciele? Czym jest The Governess...?

Netheria powiedziała, że wyczuła że na farmie był jeszcze jeden mag - przerażona czarodziejka, Netheria identyfikuje ją jako "matkę". Czarodziejka była: przerażona, zaszczuta, zniewolona przez The Governess i w kiepskiej formie. Dionizy zabrał się do szukania - znalazł podłogę, faktycznie zamkniętą (drewniana podłoga, świeżo położona). Ta czarodziejka jest zamknięta pod ziemią...

Alina przeszukała łóżka; znalazła jakąś próbkę genetyczną The Governess. Zdecydowała, że ma zamiar się w nią przekształcić (w Rezydencji). Chce zobaczyć, czym jest The Governess. Mogą dojść do tego, kim ona jest.

Klara znalazła zniszczone komputery i inne takie komponenty. W walce The Governess / Zajcewowie zostały zniszczone te wszystkie maszyny. Do odzyskania.
Klara - katalistka - przygotowała tymczasową blokadę magii. Pod ziemią jest spanikowana czarodziejka. Nie daj sobie przywalić fireallem przez osobę, którą ratujesz ;-).

The Governess dostała sygnał, że "ktoś" jest w jej byłej bazie. Zdecydowała się na kontratak.

Zanim jednak zdążyła - Krwawokrąg (istota zbudowana z krwi Anny Myszeczki, pasożyt komplementarny z nią) zapolował na swoją "matkę". Jak tylko Dionizy wydobył Annę Myszeczkę, Krwawokrąg zaatakował. Dionizy skutecznie dał radę Annę uratować; szybkie reakcje. Krwawokrąg próbował się wycofać, ale Alina + Dionizy + płaszczki go złapali. Żywego.
Hektor przynosi pojemnik łapiący, zaaferowany; zaobserwował lecącą na nich Grubą Dronę. Która się sypie (atak The Governess).

Hektor nie jest w stanie zrobić nic sensownego a Klara za daleko. Szybka piłka - strzelić prostą, mocną wiązką piachu by rozwalić dronę. Drona wybuchła, Skaziła bardzo stężoną Irytką cały obszar. Netheria - dzięki działaniu Hektora - miała szczęście. Dionizy ją obezwładnił (ogłuszył).

Zespół próbuje wycofać się, zanim The Governess uderzy by posprzątać.
Klara konstruuje Swarm by móc ewakuować wszystko co ważne a Dionizy zarządza ewakuacją. I Klara magicznymi sygnałami emuluje walkę między Zajcewami i to co powinno wyskoczyć z Irytki. Dzięki temu kupują sobie czas i zmylają The Governess, niezdolną do szybkiej adaptacji i nie spodziewającej się że ma do czynienia z Blakenbauerami.

Hektor podpala chatę. W końcu Zajcewowie.

Dionizy bezpiecznie ewakuował wszystkich zanim Front Sprzątający się pojawił. Udało im się zdobyć:
=== Annę Myszeczkę, która jest naprawialna i zdolna do działania po zdjęciu prawdnika jadowitego
=== Netherię, która bezpiecznie nie została zarażona Irytką; przejdzie jej po kilku dniach w Rezydencji
=== Krwawokrąg, który w pudełku będzie do dalszego badania w Rezydencji
=== Komputery, które zawierają informacje o działaniach The Governess i jej postępach
=== Ślady biologiczne, wiedza kim jest The Governess
=== The Governess wie, że to byli Blakenbauerowie. Ale nie wie co zgarnęli. Ani jak.

W Rezydencji.

Anna Myszeczka rozpoznała The Governess. Powiedziała, że to Aurelia Maus.
Z próbki genetycznej w Rezydencji zrobiono superpozycję wyglądu The Governess. To Karolina Maus.

Na komputerach Klara znalazła następujące informacje z Dionizym:
=== Sprzęt ze Skrzydłorogu, kupiony i przekazany legalnie
=== The Governess nie szukała. Ona wiedziała. Komunikowała się w inny sposób, nie przez internet. Spustoszenie?
=== Próba złamania klucza archiwów Świecy. Nie zdążyła.

...

# Progresja

* Netheria Diakon: dostaje kontakty Hektora w świecie ludzi
* Netheria Diakon: dostaje usługi Aliny i Dionizego wtedy, gdy ci nie są zajęci i gdy nie koliduje to z celami Blakenbauerów
* Karolina Maus: wie, że przeciwko niej stają Blakenbauerowie
* Karolina Maus: okazuje się być bardzo opanowaną defilerką o ogromnej mocy (co samo w sobie jest oksymoronem)

## Frakcji

* Blakenbauerowie: dostają komputery The Governess gdzie znajdują się cenne informacje o jej ruchach i jej planach / działaniach
* Blakenbauerowie: dostają Annę Myszeczkę
* Blakenbauerowie: dostają Krwawokrąg, który może posłużyć jako prototyp do badań (lub do kontroli Anny Myszeczki)
* Blakenbauerowie: dostają ślady biologiczne Karoliny Maus

# Streszczenie

Blakenbauerowie odkrywają, że magowie mający być zmienieni w Oddział Trzygław mają wzór uszkodzony krwią Mausów. Weszli do Kotów (współpracując z Netherią, która uzyskała wymierne korzyści), do farmy z której uciekała The Governess i po starciach z pozostawionymi przez The Governess elementami udało im się odzyskać żywą ofiarę The Governess. Zdobyli sporo informacji na temat The Governess, min. jej tożsamość - Karolina Maus. A ona wie, kto zaatakował, acz nie wie co się dowiedzieli.

# Zasługi

* mag: Hektor Blakenbauer, autor genialnego pomysłu nie zadowolenia Wiktora i mistrz niszczenia dron wypełnionych Irytką. Też: podpalacz chat.
* mag: Klara Blakenbauer, łamiąca dane z komputerów The Governess i oszukująca ją katalizą że tam jest walka. Też: mistrzyni swarmowanego ewakuowania śladów.
* vic: Dionizy Kret, ratujący Annę przed niechybną śmiercią, po czym pokonujący krwawokrąga. Też: mistrz ewakowania zanim The Governess się zorientowała.
* vic: Alina Bednarz, mistrzyni negocjacji namawiająca Netherię do współpracy. Zbierając ślady odkryła tożsamość The Governess.
* mag: Otton Blakenbauer, zgodził się na plan Hektora odnośnie "starych Blakenbauerów" i zaproponował Netherię jako wsparcie w odkryciu prawdy o The Governess
* mag: Edwin Blakenbauer, odkrył, że wzór * magów dostarczonych przez Wiktora jest uszkodzony krwią Mausów. Nie przeszkadza mu to, acz dziwna obserwacja
* mag: Netheria Diakon, która pozornie pogodziła się ze stratą Wandy i zgodziła się działać z Blakenbauerami przeciwko The Governess. Detektyw obrazów z przeszłości.
* mag: Anna Myszeczka, ofiara The Governess, która została uratowana przez Dionizego i dostarczona do Rezydencji. Służyła jako podstawa do tworzenia Krwawokrągów.
* mag: Karolina Maus, The Governess, która koordynuje zdalnie działania w Kotach swoimi dronami. Perfekcyjnie przygotowana... na nie tych przeciwników.

# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Prywatne osiedle "Mirra" 
                        1. Apartament Netherii Diakon, gdzie doszło do erotycznej terapii dwóch par (ludzkich), a dokładniej to chodzi o coś z ich dziećmi
                    1. Obrzeża
                        1. Rezydencja Blakenbauerów, stacja dowodzenia Blakenbauerów przeciwko The Governess
            1. Powiat Czelimiński
                1. Koty
                    1. Dwie hałdy
                        1. Agroturystyczny domek, ruina kiedyś spokojnego miejsca - podpalony przez Hektora by zamaskować co się dzieje przed The Governess
      
# Wątki



# Skrypt

- Czarodziejka nastrojów pragnie odzyskać influencerkę porwaną przez Blakenbauerów
=== Konflikt Netherii z Blakenbauerami o Wandę
=== Wykrycie nastrojów The Governess, już na miejscu, w Kotach
=== Wykrycie maga w pudełku (Anny)

- Zabójczy potwór pragnie zabić swoją matkę by skompletować swój stan
=== Próba zabicia Anny Myszeczki
=== Polowanie na Zespół
=== Ostrzeżenie The Governess

- Zniszczony komputer, ukrywający ślady The Governess
=== Sprzęt ze Skrzydłorogu, kupiony i przekazany legalnie
=== The Governess nie szukała. Ona wiedziała
=== Próba złamania klucza archiwów Świecy
=== Połączenie z centrum dowodzenia gdzieś indziej w Kotach

- Bunkier, śmiertelna pułapka The Governess
=== System defensywny kupujący czas defilerce
=== Portal mający umożliwić defilerce krótki teleport
=== Bomba mająca zniszczyć wszystko
=== Standoff z The Governess

# Czas

* Dni: 2
---
layout: inwazja-konspekt
title:  "Detektyw, lecz nie Sądecznego"
campaign: wizja-dukata
players: kić, prezes, foczek
gm: żółw
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [171022 - Tylko nieświadomy elemental (PT)](171022-tylko-nieswiadomy-elemental.html)

### Chronologiczna

* [171022 - Tylko nieświadomy elemental (PT)](171022-tylko-nieswiadomy-elemental.html)

## Kontekst ogólny sytuacji

### Siły główne



### Stan aktualny



### Istotne miejsca



## Punkt zerowy:

* Tomasz Kapelusz: brat Adama, podlewa krystality szlaufem i dba o to by się nie skaził
* Adam Kapelusz: brat Tomasza, ojciec Mai z poprzedniego małżeństwa i właściciel farmy

Ojciec Mai - Adam - zajmuje się fakturami. Tomasz zajmuje się krystalitami.
Burza. Nietypowa, z czymś co wygląda jak portal. Tomasz nagrywa kamerą.
Krzyk Mai. Jej awian z imprintem osobowości Rolfa (bezpieczne "AI" dla dziecka) zachowuje się dziwnie. Walczy sam ze sobą.
Awian wchodzi w tryb stealth. Tomasz łapie Maję i biegnie do domu.
Awian porywa Tomasza i Maję - Tomasz potyka się o niewidzialnego awiana i doń wpada. Widzi to Adam.
POMOCY! Rolfa. Rolf krzyczy SOS PORYWAJĄ! Adam próbuje kontrolować pilotem - bez efektu.
Adam zdobywa kamerę. Dzwoni do dziadka Mai.

## Potencjalne pytania historyczne:

* Czy Wieża Wichrów przetrwa ekonomiczna w rękach Filipa, czy trafi do Sądecznego?
    * Docelowo: nie, Sądeczny przejmie Wieżę Wichrów
* Czy "elementale" wymuszą zmianę całego ekosystemu awianów w Krolżysku?
    * Docelowo: tak, awiany będą musiały zostać przebudowane z woli Sądecznego i Świecy
* Czy "elementale" zbudują stały portal z Wieżą Wichrów?
    * Docelowo: nie, ale może mi się uda ;-).

## Misja właściwa:

**Dzień 1**:

Późny wieczór. Sądeczny życzy sobie by Krzysztof uratował dziecko (Maję Weiner). Wysłał info po hipernecie - nagranie z kamery. To awian wyprodukowany przez nich.
W bazie. Krzysztof poleciał "Czarnym Ptakiem". Zlokalizował narzędziami w bazie tego awiana z porwanymi.
Zaklęcie. Paradoks Perdo Zmysły. "Rolf" ślepy. Jednocześnie - info o tym, że tam są 2 elementale i ekran ochronny rozsypany.
Paralizator awianów uratował lecących divebombem awianem (poleciał fullspeedem).
Ranni są Skażeni elementalem. Coś dziwnego. Tamten elemental z "Rolfa" ma sygnaturę trochę jak te co dostaje z Wieży Wichrów - ale jest "inny". Były tam dwa...

Kociebor pił wieczorem whisky, z Kocieborem skontaktował się Sądeczny po hipernecie. Jak z Krzysztofem, ale Kociebor ma się dowiedzieć co i jak.
Kociebor jak w CSI - obrazy kamery wzmocnił magią. Infomancja na taśmę. Wykrył: ciężarówka ze świniami, jej echo. Widział portalisko po drugiej stronie. Widział Wieżę Wichrów.
Kociebor pojechał do montowni awianów i dowiedział się jak są montowane awiany. Kolejne nawiązanie do Wieży Wichrów. Wtedy wrócił Krzysztof z pojmanym "Rolfem".
Po wypuszczeniu, Paradoks przerażonej Mai. Awian ożył, powiedział "nie bój się maleńka", wybił się, zatrzymany przez haki i opadł, bardziej uszkodzony. Ale Maja uspokojona. Krzysztof nieszczęśliwy. Więcej naprawiania.
Kociebor dowiedział się od Mai (kosztem Paradoksu rego bio), że po drugiej stronie portalu jest gniew i wściekłość i żal. I echa ciężarówki ze świniami z glukszwajnem. Efemeryda. Super.

Paulina wezwana przez Oliwię bada Tomasza i Maję w izolowanym pokoju, z pomocą Grumrzyka.
Paulina dowiedziała się o tym, że oni są Skażeni a Maja ma w sobie tego dziwnego elementala, jego ziarno. SS: elementale mają stały portal powiązany z Wieżą Wichrów.
Paulina z pomocą Krzysztofa uleczyła Maję oraz wyekstraktowała dziwny typ elementala z Mai do Krzysztofowej probówki elementalnej.
Paulina opowiedziała im poprzednią historię - to o portalisku i biednym Szymonie.
Maja i Tomasz wrócili do domu. Odesłani taksówką. Nie awianem. Wuj dostaje numer telefonu - jak coś się dzieje...

**Dzień 2**:

Zjazd całej trójki w fabryce awianów.
Podstawowy zbiór informacji o Keramiuszu Kociebora podało info łączącą Paulinę i Filipa Keramiusza! Paulina wyjaśniła że nic ich nie łączy, byli znajomymi ;-).
Cały zespół pomógł zbudować czujniki na te "dziwne" elementale. Paulina i Krzysztof zamontowali na swoje awiany te czujniki.
Po odpaleniu czujników, wszystkie awiany w fabryce się rozświetliły. Wszystkie mają te "dziwne" komponenty. Wow...

Wieża Wichrów. Paulina ostrzegła Filipa, że idzie DOOMKOMMANDO.
Naciśnięty przez Kociebora Filip powiedział prawdę - nie wie co robi, nie wie gdzie otwiera, nie rozumie tych technologii.
Paulina wezwała Kajetana jako ekranowanie. Będą otwierać burzę by zrozumieć co i jak.
Burza magiczna. 2 potężne elementale. Jeden spętany, drugi dla Kajetana. Kajetan świetny terminus, daje radę.
Badanie przez Kociebora: inna faza niż Daemonica, to jakieś byty, portal się ukonstytuował. By portalu nie było, przez 2 miesiące nie może tu być burz.
Kajetan pokonał elementala; będzie miał zgon bo Paulina zaaplikowała mu Heroicznego Chirurga.
Czas by Kociebor powiedział co i jak Sądecznemu. Winna specyfikacja sprzętu KADEMu, w sumie, ale Sądeczny nie łyknie. On chce Wieżę Wichrów.
Kociebor wkręcił Sądecznego, że to przez glukszwajna. Że to świnie w eterze. Ta firma nie ma z tym nic wspólnego. To echo tego z portaliska. Sądeczny łyknął.
Sądeczny nie ma powodu ani pretekstu by przejąć firmę.
Filip kocha Kociebora! A Kociebor nie chce być po prostu plebsem Sądecznego.

## Wpływ na świat:

JAK:

1. Każdy gracz wybiera 1 element Historii - konflikt / okoliczność, która miała Znaczenie i ma zostać jako zmiana po akcji. MG wybiera tyle, ile jest graczy.
1. Każdy gracz wydaje swoje punkty na generowanie rzeczywistości i wątków.

CZYLI:

1. Kić: Awiany nowego typu - lepsze, fajniejsze, niebezpieczniejsze
1. Prezes: Te pieprzone świnie się rozmnożyły. Pasożytnicze świnie z eteru.
1. Foczek: Przepakowane elementale. Więcej zabawy, więcej energii.
1. Żółw: Portal w Wieży Wichrów sam z siebie się nie rozproszy - jest tam i będzie spamował dziwne byty.
1. Żółw: Farma Krystalitów z młodą Mają która decyduje się przejąć dowodzenie na farmie.
1. Żółw: Prawdziwym bohaterem Mai stał się Kociebor, który potraktował ją łagodnie i z sympatią.

JAK:

* 2: MUSIK: gracz mówi w jaki sposób jedna z obcych postaci wspiera coś związanego z motywacją JEGO postaci
* 2: CLAIM na wątku / postaci / okoliczności
* 2: do elementu Historii lub przeszłego konfliktu dodajemy "ale" lub "oraz"
* 2: dodajemy pytanie, które musi zostać odpowiedziane na jakiejś z przyszłych misji
* 2: odpowiadamy na pytanie, które pojawiło się na jakiejś z misji
* 3: gracz mówi w jaki sposób jedna z obcych postaci wspiera coś związanego z motywacją JEGO postaci
* 3: zmieniamy kontekst okoliczności czegoś z Historii - scena z przeszłości / fakt?
* 3: do elementu spoza Historii dodajemy "ale" lub "oraz"
* 3: dodajemy znaczącego NPC powiązanego z elementem Historii
* 3: pozyskanie przez dowolną postać znaczącego surowca mającego sens z perspektywy misji
* 3: dodanie nowego elementu Historii
* 3: dodanie przyszłego lub przeszłego faktu; czegoś, co się wydarzyło lub wydarzy

MG: 13

* Maja próbuje w najbardziej nieoczekiwanych momentach (i nieodpowiednich) okazać Kocieborowi sympatię i pomoc.
* Byty wchodzące w elementale są świadome. (odpowiedź graczy)
* W jaki sposób byty wchodzące w elementale mogą pomóc dziecku Dukata?
* Dlaczego krystality są niezbędne Oliwii do opanowywania efemerydy?
* Sądeczny odzyskuje część "oczu" przez wprowadzenie linii produkcyjnej dron-awianów do fabryki Oliwii i Tamara wie o tym.
* W jaki sposób można odepchnąć w eterze nadmiar energii, by pozbyć się kłopotu bez skrzydła katalistów, terminusów i logistyków?

Kić: 5

* Filip aktywnie wspiera Paulinę w redukcji pola magicznego i Skażenia na tym terenie.
* Kto i w jaki sposób pomoże Dracenie stać się częścią tego terenu dookoła? Należeć do czegoś?

Foczek: 5

* Po tym jak Krzysztof pomógł Filipowi, współpraca się zacieśniła - lepsza kalibracja Firmy i Firmy.
* Byt który opętał elementala/awiana Rolfa jest emocjonalnie związany z dzieckiem.

Prezes: 7

* Oliwia zapewnia Kocieborowi drony, które pozwolą na obserwowanie i śledzenie. Nowy biznes Oliwii.
* Customizowany awian, nie taki jak Czarny Ptak. Taki czołg.
* CLAIM: okazja na poznanie i zaprzyjaźnienie się z Kajetanem.

Z sesji:

* Wieża Wichrów zostaje w rękach Filipa. Sądeczny nie ma jak się do niej przyczepić.
* Wieża Wichrów jest sprzężona portalem z nieznaną (chwilowo) fazą Allitras. Stałe powiązanie, póki są tam burze.

# Progresja



# Streszczenie

Dziwna magiczna burza nad farmą krystalitów; awian 12-letniej Mai się zbuntował i ją porwał wraz z wujkiem. Zawołany przez Sądecznego Krzysztof wyłączył awiana z akcji a Paulina odkryła infekcję "dziwnym elementalem". Tymczasem detektyw Kociebor z nadania Sądecznego znalazł źródło problemów w Wieży Wichrów. Okazało się, że Filip Keramiusz przyzywał coś innego, nie konstruował elementali burz... Wieża została w rękach Filipa a Zespół ma detektory tych dziwnych elementali, które nie są elementalami.

# Zasługi

* mag: Kociebor Dyrygent, chce się uwolnić od etykietki "detektyw Sądecznego"; odkrył co stoi za Wieżą Wichrów i że jest ona sprzężona z fazą Allitras, po czym by ratować biznes Filipa, nakłamał Sądecznemu że to wszystko przez glukszwajny...
* mag: Krzysztof Grumrzyk, nie tylko legendarny konstruktor i właściciel Czarnego Ptaka - też mag, który uratował Maję Weiner przed "zbuntowanym" awianem i pomagał Paulinie w naprawianiu szkód.
* mag: Paulina Tarczyńska, wezwana na leczenie Mai Weiner i Adama Kapelusza, została, bo temat dotyczył "dziwnych elementali". Wezwała Kajetana w kluczowym momencie i odkryła prawdę o Wieży Wichrów.
* mag: Maja Weiner, 12 lat; córka Adama Kapelusza. Porwana przez swojego awiana wraz z wujkiem Tomaszem; Odkażona przez Paulinę z ziarna "dziwnego elementala".
* czł: Adam Kapelusz, ojciec Mai Weiner i właściciel farmy krystalitów. Jego największym osiągnięciem było zadzwonić do dziadka Mai by info o problemach trafiło do Sądecznego.
* czł: Tomasz Kapelusz, wujek Mai Weiner, który nagrywał na kamerę dziwną burzę nad farmą krystalitów. Próbował ją uratować przed "oszalałym" awianem - ale też został portwany. Odkażony przez Paulinę.
* mag: Filip Keramiusz, dzięki silnej pomocy Kociebora udało mu się utrzymać Wieżę Wichrów i uzyskać potężnego elementala - którego może sprzedać Krzysztofowi ;-). Powiedział Kocieborowi prawdę.
* mag: Robert Sądeczny, chciał przejąć kontrolę nad Wieżą Wichrów i załatwił do tego celu Kociebora; oszukany i stracił okazję. Ale uzyskał zapewnienie awianowej linii produkcyjnej dron, by mieć oczy. Aha, nie życzy sobie wypadków z dziećmi na SWOIM terenie.
* mag: Kajetan Weiner, wywarł wrażenie na Kocieborze gdy był wezwany do osłony Wieży Wichrów podczas eksperymentów Kociebora odnośnie tego co tam się dzieje podczas burzy.

# Lokalizacje

1. Świat
    1. Faza Allitras
        1. Trimektil
            1. Jezioro Błysków, na stałe sprzężone z Wieżą Wichrów w Powiecie Ciemnowężnym (Gnuśnów Letni).
    1. Primus
        1. Mazowsze
            1. Powiat Ciemnowężny
                1. Gnuśnów Letni, który kiedyś słynął z nadmiernego występowania południc
                    1. Farma Krystalitów, gdzie przez burzę otworzył się portal do Allitras i "dziwny elemental" opętał awiana Mai Weiner.
                1. Burzowiec
                    1. Wieża Wichrów, na stałe sprzężony z Jeziorem Błysków w Trimektilu, w fazie Allitras - gdy jest burza.
            1. Powiat Pustulski
                1. Mżawiór
                    1. Portalisko Pustulskie, które jakoś odbiło się w Jeziorze Błysków; w Eterze tam się znajdującym 
                    1. Agencja detektywistyczna, sprezentowana Kocieborowi przez Sądecznego; Kociebor sobie tam chillował gdy zadzwonił Sądeczny...
                1. Krolżysko
                    1. Montownia awianów Lotnik, która stała się tymczasowym centrum dowodzenia działań przeciwko "dziwnym elementalom" z Allitras.

# Czas

* Opóźnienie: 1
* Dni: 2

# Wątki kampanii

* Wieża Wichrów
    * Robert Sądeczny zażądał od Filipa zaprzestania tych konkretnych praktyk zagrażających terenowi.
    * Bez tych praktyk Wieża Wichrów w obecnej formie nie ma sensu; lepiej zrobić nieco inne miejsce.
    * Czy Wieża Wichrów przetrwa ekonomiczna w rękach Filipa, czy trafi do Sądecznego?
    * Filipowi uda się utrzymać firmę JAKOŚ.
* Elementale i awiany
    * CLAIM: nigdy więcej efemeryda nie skrzywdzi moich awianów (Foczek)
    * Aktualny elemental nie zdołał otworzyć portalu, ALE wysłał sygnał w eter.
    * Spora część awianów montowanych w montowni jest dostosowana do linii produkcyjnej Filipa.
    * Sporo awianów a wszystkie nowej generacji są zbudowane technikami sprzężenia Daemonica-elementalna.
    * Dlaczego "elemental burzy i gniewu" jako najlepsze miejsce znalazł ten klub (Panienka w Koralach)? Co było z tym miejscem lub ludźmi?
    * Ściągane i pętane elementale są niewolone ALE nigdy nie są agresywne.
    * Jakiego typu viciniusem stanie się ten awian?
* Powrót Srebrnej Świecy
    * CLAIM: Paulina zostanie rezydentką Świecy na tym terenie a Tamara jej prawą ręką - terminusem bojowym.
    * Sylwester został zesłany tu przez Newerję
    * Sylwester: przeprowadził udaną akcję zdobycia transportu Dukata
    * Tamara doprowadziła do sytuacji, gdzie Świeca nie jest na ciągłym podsłuchu
    * fakt: Tamara nie jest w stanie rozwiązać wcześniejszej dezinformacji
    * fakt: Tamara robi się bardzo podejrzliwa wobec Myszeczki
    * Myszeczka staje po stronie rezydenta Świecy
    * Awiany są strategicznie potrzebne zarówno mafii jak i Świecy
    * fakt: Oktawia w power suit ma dostęp do tego co wie power suit. Wie to, co Tamara przekazała.
    * Tamara skupia uwagę na wspieraniu przede wszystkim Tomasza Myszeczki. Duży, rozległy, potrzebuje pomocy. Brak mu wszystkiego.
    * Sylwester opłaca nowego przedsiębiorcę mającego założyć konkurencyjną małą magitrownię. By uniezależnić się od gróźb. Aha, czyn nieuczciwej konkurencji.
* Działania Roberta Sądecznego
    * Robert dostaje haracz od "Nowej Melodii"
    * rezydent polityczny Dukata u Myszeczki
    * Robert spinuje akcję dezinformacyjną. Gdyby nie Świeca, nie byłoby kłopotu i magitrownia nie miałoby tragedii
    * Robert uważa, że współpraca z Danielem oznacza, że Daniel jest dużo bardziej cenny niż myślał wcześniej
    * Sądeczny daje Kocieborowi własną agencję detektywistyczną za zasługi i ratunek Dobrocienia
    * Awiany są strategicznie potrzebne zarówno mafii jak i Świecy
    * fakt: Sądeczny w swoim investigation odkrywa, że Tamara zadziałała wbrew Danielowi i on nie chciał
    * fakt: Sądeczny dostarczy maga, który zajmie się glukszwajnem
    * Sądeczny nasyła Zofię na szpiegowanie Tamary
* Katia Grajek
    * To Hektor pomagał w budowaniu eliksirów dla Katii
    * Aneta wróciła wtedy z Sądecznym ORAZ wie, co tam się stało
* Dracena, wracająca do świata ludzi
    * CLAIM: Dracena docelowo będzie w stanie zintegrować się z JAKIMŚ społeczeństwem (Kić)
    * CLAIM: Dracena i Oktawia będą miały duet. (Żółw)
    * CLAIM: Szybki, dyskretny awian. Trafi do Draceny. (Żółw)
    * Fakt: Dracenę wylano z "Nowej Melodii" za radykalizm
    * Dracena wykonuje działania mające uwolnić "Nową Melodię" od mafii 
    * Fakt: Elektrownia z Draceną działa dużo lepiej i taniej
    * Dracena dowiaduje się, że elektrownia współpracuje z mafią a nie jest wykorzystywana
* Gabriela Dukata droga do ostatecznej medycyny dla dziecka
    * CLAIM: Dukat lubi Paulinę (Kić)
    * Dukat nie ma gwałtownej reakcji na działania Sylwestra; da się wychować. Tupnął nogą bo musiał - and now we talk" (Kić)
    * Kluczowe elementy soullinkowania dziecka Dukata pochodzą z wiedzy pozyskanej przez Sądecznego w ramach działania na tym terenie
    * Dukat zatrzymał działania Sądecznego ORAZ wstrzymało to niszczenie Legionu
* Osobliwości lokalne
    * CLAIM: Paulina nie będzie musiała uciekać z terenu niezależnie od okoliczności (Kić)
    * CLAIM: Myszeczka jest ważnym partnerem biznesowym Daniela (Raynor)
    * Zarówno siły Dukata jak i Myszeczki zapewniają, by Skażenie nie dotarło do magitrowni.
    * Pojawia się zwiększenie ruchów antymausowych na tym terenie - mniej portaliska dla Mausów.
* Kluczowa rola glukszwajnów wobec efemerydy Senesgradzkiej)
    * CLAIM: będę w stanie wykorzystać tą efemerydę jako niesamowite źródło energii (Foczek)
    * W okolicy Męczymordy jest odpowiednia ilość pola rezydualnego by sprawić by znikające świnie były prawdą
    * Rolnik od świń założył ma misję - i przydupasa - mające udowodnić, że ze świniami dzieje się coś dziwnego
    * Natalia ma własnego tresowanego glukszwajna z apetytem na magię Harvestera
    * Natalia ma eksperta od glukszwajnów; człowieka kiedyś pracującego dla Myszeczki
* Wolność Eweliny Bankierz
    * CLAIM "Newerja Bankierz x Gabriel Dukat - zostaną parą" (Kić)
    * fakt "docelowo Ewelina dostanie jakieś środki do życia; nie będzie żebraczką"
* Echo starych technologii Weinerów
    * fakt: Harvester nie ma dość energii by się obudzić
    * Natalia pozyskała bezpiecznie kolejną część artefaktu
* Oktawia, echo Oliwii
    * CLAIM: Oktawia dorośnie i zniknie. (Kić)
    * Oktawia chce pomóc Krzysztofowi, bo widzi w nim potencjał do pomocy Oliwii


# Narzędzia MG

## Opis celu misji



## Cel misji



## Po czym poznam sukces



## Wynik z perspektywy celu



## Wykorzystana mechanika

Postać (strona gracza):

| .Motywacja. | .Umiejętności. | .Magia. | .SiłySłabości. | .Znam.    | .Mam.      |.POSTAĆ. |
|   (-2) - 2  |     0 - 2      |  0 - 2  |    (-2) - 2    | Z+M = 0-6 | Z+M = 0-6  |         |

Opozycja (strona NPC):

* Baza: 2
* Aspekt: 3/aspekt, wariant z nożyczki/papier/kamień
* Modyfikatory: Skala, Sprzęt, Sytuacja (1-3 każdy)

Wynik:

| .Postać. | .Opozycja. | .Rzut.    | .Wynik.  |
|          |            | xx: +x Wx |   S-SS-F |

Wpływ:

* 8 kart / dzień
* każda porażka i skonfliktowany sukces = +2 pkt wpływu dla gracza
* każda karta = +1 wpływ dla MG
* każde 5 kart zaokr. w górę: +1 pkt wpływu dla gracza

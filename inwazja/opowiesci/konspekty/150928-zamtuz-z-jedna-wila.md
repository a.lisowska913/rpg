---
layout: inwazja-konspekt
title:  "Zamtuz z jedną wiłą"
campaign: ucieczka-do-przodka
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [150928 - Zamtuz z jedną wiłą (PT)](150928-zamtuz-z-jedna-wila.html)

### Chronologiczna

* [150830 - Kasia, nie EIS, w Powiewie (PT)](150830-kasia-nie-eis-w-powiewie.html)

## Punkt zerowy:

* Ż: Kto jest głównym "moral guardianem"?
* K: Starsza pani, Matylda Daczak; nie jest super wredna, ale konserwatywna, wnuki odwiedzają z rzadka.

## Kontekst misji

Po ucieczce z Kopalina Paulina i Maria zabunkrowały się w Przodku. Jest to miejsce, gdzie zaklęcia nie działają typowo, więc magowie tych okolic unikają.

Tymczasem Gala Zajcew i Tymoteusz Maus próbują zrobić coś konstruktywnego dalej...

- Gala wpada na pomysł: wykorzystają wiłę do zbudowania małego źródła dochodu; Tymek katalizuje, Gala organizuje
- Gala wbija z wiłą do Gęsilotu, bierze ze sobą Mariusza Czyczyża. Daje Czyczyżowi za zadanie rozkręcić odpowiednie sutenerstwo.
- Gala przekonuje sołtysa, Czesława Simnika, że to dobry pomysł; że obecność wiły da Gęsilotowi korzyści finansowe.
- Gala załatwia, że wiła zamieszka u Franka i Joanny Pirzec (którzy wyprowadzają się do Kopalina).

## Misja właściwa:
### Faza 1: Nowy dom Pauliny

Ku zdziwieniu Pauliny, gdy przybyły z Marią do Przodka to czekało na nie już mieszkanie. Dwupokojowe; jedno dla Pauliny, jedno dla Marii. Załatwiła to. Załatwiła też spotkanie z Ryszardem Hermanem następnego dnia; jest on dyrektorem Szpitala Gotyckiego. To znaczy - Maria się poprawiła - ten szpital ma inną nazwę, ale wszyscy na niego mówią Gotycki. To dość... specyficzne miejsce. 
Maria powiedziała, że po pierwsze, Herman ma u niej pewien dług. Po drugie, mimo, że szpitalom płacą w określony sposób co zniechęca do posiadania dużej ilości trudnych przypadków, Herman nie wyrzuca ludzi. Ma przez to wysoko rekordową śmiertelność i trudne przypadki. Min. z uwagi na śmiertelność szpital nosi nazwę "gotycki". Mówi się, że używają w szpitalu pił tarczowych i zardzewiałych strzykawek - co jest bzdurą.

Zdaniem Marii, ten szpital jest idealnym miejscem dla Pauliny; da radę się dobrze schować w tłumie. Ostrzegła tylko Paulinę, że Herman jest lekko paranoiczny i ma otwarty umysł; bardzo łatwo będzie przy nim złamać Maskaradę. Jakby co - Maria pocieszyła Paulinę - zawsze jest możliwość wymazania Hermanowi pamięci w ostateczności.

Paulina się uśmiechnęła. Maria to mówi. Jak bardzo się wszystko zmienia.

Ryszard Herman przywitał Paulinę uprzejmie, choć szorstko. Powiedział jej, że Szpital Gotycki ma taką a nie inną renomę, że nie płacą tu aż tyle co w okolicznych klinikach; dodatkowo, jest to end-of-career z powodu ogólnej niepopularności samego Hermana. Jeśli Paulina jest zainteresowana, proszę bardzo.
Paulina poprosiła Hermana o pewną elastyczność czasu pracy i okoliczności; Herman powiedział, że to nieco zabawne; miał swego czasu lekarza - bardzo dobrego lekarza - który nazywał się Adam Diakon. Prosił o dokładnie to samo. Niestety, zrezygnował i... a w ogóle, nieważne. Paulina przycisnęła (konflikt); wygrała. Herman powiedział, że jakkolwiek Diakon był dobrym lekarzem, jego pacjenci czasem wracali. Z identycznymi objawami i identycznymi ranami. Paulina przełożyła na: wykorzystywał magię i zaklęcia się porozpadały.

Herman zadziałał z grubej rury. Poprosił Paulinę o konsultacje w sprawie bardzo ciężkiego przypadku; Paulina spojrzała i jej ręce opadły. Delikwent dwa tygodnie temu był do uratowania (jakby mieli lepszy sprzęt w tym szpitalu i jakby trafił wcześniej), ale aktualnie bez magii nie da się mu pomóc...
Herman wyjaśnił Paulinie co zrobili z tym pacjentem i jak próbują mu pomóc; niestety, Paulina wie, że to co Herman mówi nie ma szans zadziałać. I Herman też to wie. Ale Paulina wie, że nie powinna pokazywać swojej mocy...

Paulina powiedziała, że też nie widzi żadnej sensownej ścieżki. Zaproponowała kilka rzeczy, które (jak wie) nie pomogą, ale są zgodne z zasadami sztuki (co on też wie) i... nic nie może zrobić. Maria wysłała jej współczujący sygnał po soullinku czując smutek Pauliny. Maria nie wie, co się stało, ale czuje smutek Pauliny.

Paulina wraca do domu, do Marii. Maria już siedzi na internecie, szuka, grzebie... cała Maria. Uśmiechnięta Paulina idzie spać.

Następnego dnia - drugi dzień pracy. Sprawy administracyjne, zapoznawanie się z różnymi przypadkami, pasywna aura magiczna z jednego pokoju... wrróć! W pokoju gdzie przyjmuje doktor Maciejak (internista) jest słaba pasywna aura magiczna. Znaczy, coś magicznego. Paulina na szybko próbowała odnieść się do swojej wiedzy katalitycznej; niestety, jedyne co wywnioskowała to to, że to przyszło z pacjentem. I pacjent dalej tam siedzi.

Paulina poprosiła Marię o niewielką przysługę; jeśli nie jest bardzo zajęta, czy nie mogłaby wpaść do szpitala i śledzić tego pacjenta. Kto to w ogóle jest, żeby dojść do tego, skąd miał kontakt z czymś silnie magicznym. Maria powiedziała, że nie ma sprawy, zajmie się tym.
Tymczasem Paulina podskoczyła do rejestracji i sprawdziła, jak delikwent (pacjent) się nazywa i gdzie mieszka; nazywa się Filip Czątko, 16 lat, mieszka tu, w Przodku (znany adres). Niedaleko. Przyszedł z mamą.

Paulina usiadła pod ścianą i udaje, że coś czyta czekając, aż młody wyjdzie. I wyszedł. Filip Czątko jest rozedrgany emocjonalnie, przełyka ślinę... jest pod wpływem silnych uczuć. Paulina widzi, że miał kontakt albo z silnym zaklęciem, albo z węzłem.
Maria go przechwyciła. Śledzi go. Wysłała sygnał Paulinie - ten chłopak jest napalony jak fretka w rui. Ona się na tym zna...
To się robi ciekawe.

Paulina ma trochę czasu. Skupiła się na przypadku tego nieszczęśnika którego Herman pokazał jej na samym początku. Z pomocą Marii spróbowała sprawdzić, o co tu chodzi; (konflikt) wyszło jej, że Herman przeanalizował ten wypadek dużo dokładniej. Herman WIE, że nie ma możliwości mu pomóc. Przy tych wynikach bez magii jest nie do uratowania.
Z ciężkim sercem, ale Paulina wiedziała już co musi zrobić. Niestety, pacjent umrze. Nie da się uratować wszystkich. A w tej chwili przyznanie się do magii jest...

Tymczasem Maria zaalarmowała Paulinę - młody Filip Czątko zwiał przez okno i pędzi przez miasto. Podglądała go wcześniej - masturbował się dobre 2-3 razy. Maria jest na ogonie Czątka i go nie zgubi; po to jej rower.
Maria wypatrzyła, że Czątko wsiadł do autobusu jadącego w kierunku na okoliczne wsie. Wsiadła do autobusu z rowerem i cierpliwie śledziła go aż wysiadł na przystanku. Wysiadła (bardzo ryzykowne) i pojechała w innym kierunku. Dała jednak radę zauważyć do którego domu wchodził.

Filip Czątko pojechał do Gęsilotu, do zamtuza.
Gęsilot. Prostytucja w Gęsilocie! Maria chichotała całą drogę powrotną na rowerze. 
...i tak wróciła wykończona dopiero na wieczór. Rower był ZŁYM pomysłem. Padła.

### Faza 2: Prostytucja w Gęsilocie

Następnego dnia z rana Paulina zajrzała do kartoteki Filipa Czątko. Zdaniem lekarza, normalne problemy dorastania oraz jakieś rekreacyjne nartkotyki, czyt. dopalacze. Przyszedł do lekarza z matką, bo to matka go zaciągnęła. Bo zachowywał się inaczej; przedtem był normalny. Dopiero po wizycie w klubie "Czarny Dwór" i powrocie do domu tak go wzięło (zgodnie z tym co mówi matka).

Tymczasem Maria przyjrzała się Gęsilotowi i prostytucji w owej wsi. Okazało się, że aktualnie jest tam pikieta. 10-12 mieszkańców pikietuje sołtysa o to, by "coś z tym zrobił". Dodatkowo 3 osoby spacerują dookoła felernej chaty i wyglądają zniechęcająco. Maria ogląda to z dala, rozmawia ze strażnikami moralności... robi robotę reportera w czasie, gdy Paulina pracuje.

Paulina szybko sprawdziła, czy Czątko jest umówiony na wizytę kontrolną. Okazuje się, że tak - jeszcze tego dnia. Doktor Maciejak powiedział, że skoro podejrzenia są o narkotyki, to on chce zobaczyć, czy czas pomaga. Czy organizm sam sobie z tym poradzi. Paulinie to bardzo pasuje. Jeszcze w zaciszu domu wzięła swoją wizytówkę i zrobiła z niej próbnik; musi wytrzymać godzinkę - dwie. Mały artefakt, który ma za zadanie przeskanować i zapamiętać aurę Filipa Czątka. Profilaktycznie, zrobiła też dwa kolejne próbniki - dla lekarza i matki. Chce mieć pewność, że to się nie rozprzestrzenia.

Zanim zaczęła się wielka fala pacjentów, Paulina odwiedziła doktora Maciejaka. Pogadać - jest nowa. Upuściła "przez przypadek" wizytówkę - doktor, lekko korpulentny, siwiejący facet zerwał się i jej podał. Paulina na nią spojrzała - nie ma przeniesienia Skażenia. Maciejak jest czysty.

Dokładnie to samo Paulina powtórzyła gdy przyszedł Filip Czątko z mamą. Szła, poślizgnęła się i rozsypała jej się grupa wizytówek (w czym kilka magicznych). Oczywiście, zarówno matka jak i chłopak rzucili się pomóc je pozbierać (jak tylko Filip spojrzał na biust Pauliny, spłonił się solidnie MIMO fartucha). I na wszystko to niestety wszedł Herman (conflict fail)...
Stoi i patrzy jak Paulina rozsypała wizytówki. I jest podejrzliwy. Paulina lekko spłoszona, Herman podejrzliwy. Nie ma podstaw ani nic, ale po co Paulinie te wizytówki? I po co tego tyle?
Paulina stwierdziła, że Herman powinien mieć tag "wrzód na dupie".

Na bazie analizy wizytówek Paulina ma jednoznaczną odpowiedź - ma do czynienia z czymś, co wygląda na organizację energii magicznej (czytaj: węzeł) o własnościach emocjonalnych. Węzeł emocjonalny, lub efemeryda emocjonalna. Co jest o tyle ciekawe, że na tym terenie nie powinno być czegoś takiego. Implikacja: ktoś próbuje to zagnieździć. Komuś zależy. Samo się nie pojawi. Chyba, że mamy dużą ilość artefaktów z rezydualnym polem lub wolnych Quarków... znaczy, zagnieżdżanie węzła brzmi BEZPIECZNIEJ dla Pauliny.

Dowcip polega na tym, że cokolwiek się nie dzieje, nie może być dla magów efektywne. Po prostu nie. Może być to sposób zbierania Quarków, ale wyjątkowo mało wydajny. Raczej coś, do czego by się przykleił prawdziwy bottom-feeder.

Tymczasem Maria powiedziała, że z domku pod pikietą wyszedł dobrze zbudowany, muskularny facet. Poprosił delegację do siebie do domku. Powiedział, że chce z nimi to przedyskutować. Po namyśle, trzy osoby weszły do środka - i jeszcze nie wyszły. Gorąca dyskusja.
Godzinę później wyszły. Zdaniem Marii wyglądają na oszołomione, przerażone i totalnie nie wiedzą co robić; przerwali pikietę i się rozeszli. A z domku ów mężczyzna im machał z uśmiechem i krzyknął "zapraszam znowu".

Paulina stwierdziła, że nie poprosi Marii by tam poszła. Maria powiedziała, że by odmówiła. Magia to problem Pauliny.

Paulina poprosiła Marię, by ta napisała jakiś artykuł. Coś, co zmusi drugą stronę do działania. Paulina chce wywrzeć presję na drugiej stronie, kimkolwiek oni nie są. Maria się zgodziła; użyje jednego ze swoich alter-ego. Nie powinna być łatwo znajdowalna. Maria już się oddalała od zamtuza, gdy stanęła jak wryta - facet, który machał wychodzi z domu (zamtuza) i gdzieś idzie.
W oczach Marii zapaliły się iskierki. ON GDZIEŚ IDZIE! Trzeba polujować!
Maria zadała tylko jedno pytanie: "Chcesz, bym śledziła gościa czy żebym włamała się do zamtuza?"

Paulina się podłamała. Nie chce, by Maria cokolwiek robiła, ale ta jest nieubłagana. Przy tak postawionej sprawie - niech już lepiej Maria śledzi tego faceta... nie wiadomo, czy w zamtuzie nie ma więcej ludzi.
Maria skutecznie śledzi gościa, gdy nagle ten skręcił. Maria poszła za nim, lecz wyskoczył w jej kierunku; Maria zamiast się zastanawiać wzięła nogi za pas i w długą. Ledwo, ale udało jej się uciec; facet złapał ją za płaszcz, więc wyślizgnęła się z niego i w długą! Na rower! Gość był tak zdziwiony, że uciekła.
Maria dała radę jeszcze zauważyć, że on zdecydowanie szedł do domu sołtysa.

...i czym wracała Maria do domu? Rowerem. Nie ufała taksówkom ani autobusom...

Wieczorem się spotkały. Maria zauważyła, że zwrócił na nią uwagę bo go śledziła i nie jest lokalna; to znaczy, że gość ma rozeznanie w terenie i kto jest stąd. Powiedziała też, że facet jest fachowcem. Nie wie czy jest magiem, ale na pewno jest fachowcem w tym co robi - sutenerstwo. Oczywiście, Paulinie do głowy przyszedł też inny pomysł - porwanie KOBIETY jak w okolicy jest ZAMTUZ i jest potencjalny mag może mieć bardziej... straszne implikacje. Ale tego nie powiedziała Marii.

Tego wieczoru zdecydowały się niczego jeszcze z tym nie robić. Za to Maria napisała artykuł; za dwa dni wyjdzie.

### Faza 3: Węzła problemy ząbkujące

Rano Maria i Paulina miały poważną rozmowę. Maria poprosiła Paulinę o jakieś stymulanty; coś, dzięki czemu Maria będzie szybsza, silniejsza i skuteczniejsza. Poprosiła też o coś, dzięki czemu Maria zmieni wygląd. Powiedziała, że tam wraca. Paulina powiedziała, że nie zgadza się na to. Rozmowa przeszła na temat agentów (wynajętych) - niestety, brak instant feedback. No ok. To w takim razie niech Paulina powie tym siłom, które miały tamtą bazę której Paulina pomogła (Skorpionowi)...

Stanęło na tym, że nie ma dobrej opcji. Po prostu nic nie ma. Maria skonfliktowała z Pauliną i przegrała. Nie dostanie środków, nie dostanie wspomagania i poczeka - będzie koncepcyjnie pracować, szukać dokumentów i czegokolwiek na temat "Czarnego Dworu" i tego dziwnego zamtuza. A jak Paulina wróci z pracy, to wtedy razem pójdą do "Czarnego Dworu" wieczorem.

HIDDEN MOVEMENT:

- Trzech lokalnych pijaczków napada dom Pirzców \| MG.(open attack against [prostitution])
- Wiła i Czyczyż pokonują ich z łatwością \| EH.(protect estate)
- Proto-węzeł wpłynął na wiłę; pobiła też Czyczyża. Opanowała się. \| EN.(dominate [Lea])
- Węzeł przyciąga kolejnych ochotników; min. sołtysa \| EN.(lure someone)

END OF HIDDEN MOVEMENT:

Wieczór. Paulina wróciła i Maria zdała relację (val = 12+6+3 -> 21 (elita w dobrych warunkach)):
- nie znikają dziewczyny w okolicy
- z "Czarnego Dworu" i innych klubów w okolicy wyjeżdżają minibusy z młodzieżą w nieznanym kierunku
- te same busy zaobserwowano w Gęsilocie
- w tej chacie gdzie jest zamtuz dwa tygodnie temu jeszcze mieszkało młode małżeństwo; potem wprowadziło się dwóch innych (on i ona)
- kobieta z zamtuza NIGDY nie opuszcza domu
- sołtys dostaje część kasy z zarobków z zamtuza
- ten zamtuz jest tu od dwóch tygodni
- nikt nie alarmuje policji (sołtysowi płacą, młodzież ma rozrywkę, rodzice nic nie wiedzą, strażnicy moralności są rozbici)
- facet z domu został zidentyfikowany jako Mariusz Czyczyż; "biznesmen do wynajęcia". Niezłej klasy najemnik do uruchamiania biznesów. Potrafi być fizyczny, ale nie tu jego siła.
- w klubach wysyłających młodzież tymi minibusami nie ma problemów z przestępcami
- zamtuz jest BARDZO tani. Nie zarabiają. Nie mają super przepustowości
- zgodnie z co najmniej jedną relacją, dochodzi tam do prawdziwych orgii wszystkich obecnych
- pojawia się efekt "powracających klientów"

Co najmniej część z tych rzeczy wygląda Paulinie na działanie węzła emocjonalnego, ale sterowanego. Paulina przypomniała sobie - takie rzeczy wskazują na to, że ktoś próbuje używając zwykłej energii magicznej przesunąć ją w emocjonalną. Czyli ktoś próbuje "z Quarków zrobić węzeł". Jeśli na miejscu jest defiler, to znaczy, że "z ludzi zrobić węzeł". Nawet przy małej ilości aury i specyfice Przodka, nawet bez defilera przy odpowiedniej skali powinno się dać zrobić taki narzut emocjonalny, by węzeł sam się sformował.

Czyli ktoś jest idiotą. To niebezpieczne, trudne do opanowania i bardzo mało efektywne. A operacja ma niemałą skalę. Bottom-feeder. Ale taki z kasą.

I zadzwonił telefon Pauliny. Ryszard Herman. Dowieźli mu cztery osoby w stanie ostrego pobicia i potrzebuje pomocy. Paulina już jedzie.
Dojechała i szok. Jednym z nich jest Czyczyż. Wszyscy BARDZO ciężko pobici. Paulina robi obdukcję przy użyciu sprzętu medycznego i sprawdza WTF happened there; pracuje z Paulina ramię w ramię z Hermanem. Dobry wynik.
- na ciałach trójki pobitych są ślady pięści Czyczyża
- Czyczyż też jest pobity przez tą trójkę
- są ślady jeszcze jednej osoby. Osoby, która tłukła i Czyczyża i tą trójkę. I biła w szale.
- to była kobieta o nienaturalnej sile; oburęczna, wskazuje na viciniusa lub wspomaganie magiczne
- da się ich wszystkich naprawić; kobieta jakby się opamiętała pod koniec
- na początku walczyła, potem oszalała i biła gdzie popadnie, potem się opanowała

Gdy Herman wrócił do swoich pacjentów, Paulina odwołała się do magii by dowiedzieć się co tam się stało. Użyła magii mentalnej. Nikogo nie ma w pobliżu a ta czwórka są pacjentami Pauliny.

Co się stało:
- ta trójka zdecydowała się chronić moralność. Poszli pouczyć sutenera. Nie chcieli skrzywdzić. No, może trochę.
- Czyczyż wie o magii, jest klasyfikowany w systemie magów jako agent Zajcewów, ale niezależny.
- ta trójka napotkała Czyczyża oraz kobietę; była naga.
- Czyczyż miał SILNE wytyczne nie dotykać wiły.
- kłótnie, przepychanki... i tu Paulina zidentyfikowała przesunięcie emocji Węzła. Z extasy przesunął się na extasy/fear/hate
- To jeszcze bardziej go pobrudziło i doszło do walki
- Czyczyż o dziwo nie był taki groźny, ale wiła tak. 
- Wiła nagle oszalała (wpływ węzła). Zaczęła atakować wszystkich, wzmacniając Węzeł.
- Wiła wyrwała się spod wpływu węzła. Wtedy wszyscy już stracili przytomność.
- Czyczyż wie, że zatrudniła go niejaka Gala Zajcew do tego, by założył zamtuz z wiłą i sformował Węzeł. Więcej nie wie.
- Ma działać cicho, ale jak co, Gala będzie czyścić i kompensować.
- Węzeł jeszcze się nie sformował, ale jeśli Czyczyż nie da znać do wieczora, przybędzie katalista zobaczyć co się dzieje i pomóc Czyczyżowi. On jest tam solo z wiłą, ale katalista i Gala są gdzieś w odwodzie.
- Czyczyż wie, że akcja jest przeprowadzana TANIO i minimalną ilością środków.

To ciekawe:
- wiła jest ogromnym kosztem.
- efektywnie, to to się nie zwróci. Nie może chodzić o zdobycie Quarków.
- po stronie ludzkiej koszty są niemałe (busy, element przestępczy).
- Czyczyż jest też kosztem.

O co tu chodzi?
Jeśli uda się dojść do tego jaki jest cel Gali Zajcew, to uda się zneutralizować całą akcję przez sprawienie, by się to nie opłacało.

## Dark Future:

### Actors:
- Ofiary wiły (Ludzkie ofiary)
- Ryszard Herman (Truthseeker)
- proto-węzeł (Węzeł emocjonalny)
- Mariusz Czyczyż (Estate Holder)
- Maria (Explorer/Paladin)
- część mieszkańców Gęsilotu (Strażnicy moralności)
- Lea; wiła-pułapka (Pułapka)
- Tymek; katalista (Engineer)

### Faza 1: Nowy dom Pauliny

- Ryszard Herman próbuje sprawdzić, czy Paulina sobie z tym poradzi (daje Paulinie przypadek bardzo ciężki) \| TH.(test <hypothesis>)
- Paulina zauważa ofiarę z emanacją magiczną w szpitalu; młody chłopak (16) z mamą
- Chłopakowi się pogarsza; zaczyna mieć głód \| HV.(try to get <sex>)
- Herman konfrontuje się z Pauliną by poznać prawdę \| TH.(go all out to get the truth about <Paulina>) / nie wystąpiło
- Maria mówi Hermanowi, że Paulina pomoże w sprawie jego córki \| P.(convert <Herman>) / nie wystąpiło

### Faza 2: Prostytucja w Gęsilocie

- Mieszkańcy Gęsilotu organizują pikietę przeciwko niemoralności \| MG.(protest loudly against <prostitution>)
- Czyczyż zaprasza do siebie delegację moral guardians \| EH.(receive guests)
- Lea uwodzi a Czyczyż nagrywa akcję \| VT.(strike against <target>)
- Czyczyż szantażuje strażników moralności, rozbijając ich od środka \| EH.(protect estate)
- Czyczyż odwiedza sołtysa obiecując mu zwiększenie zysków \| EH.(hire new <ally>)
- proto-węzeł ma emotion shift do strach/ekstaza \| EN.(emotion shift)

### Faza 3: Węzła problemy ząbkujące

- Trzech lokalnych pijaczków napada dom Pirzców \| MG.(open attack against <prostitution>)
- Wiła i Czyczyż pokonują ich z łatwością \| EH.(protect estate)
- Proto-węzeł wpłynął na wiłę; pobiła też Czyczyża. Opanowała się. \| EN.(dominate <Lea>)
- Węzeł przyciąga kolejnych ochotników; min. sołtysa \| EN.(lure someone)
- Gala podstawiła przyjazd kolejnej grupy młodzieży z klubów 
- Czyczyż dalej nieprzytomny; jedynie wiła siedzi w zamtuzie
- Jako, że Czyczyż się nie odzywa, Tymek Maus decyduje się pojechać tam osobiście / jeszcze nie wystąpiło

# Zasługi

* mag: Paulina Tarczyńska, która zaczyna nowe życie w Przodku i unika wykrycia przez podejrzliwego dyrektora.
* czł: Maria Newa, ucieka jak chart, fatalnie śledzi, załatwia Paulinie pracę oraz znajduje mnóstwo cennych informacji o Przodku i Gęsilocie.
* czł: Ryszard Herman, dyrektor Szpitala Gotyckiego w Przodku; był winny przysługę Marii, więc zatrudnił Paulinę. Podejrzliwy, testuje Paulinę pod kątem * magii.
* czł: Filip Czątko, młody chłopak (16 lat), który przeleciał wiłę i się lekko uzależnił; min. przez Węzeł Emocjonalny.
* czł: Rafał Maciejak, szarmancki lekarz internista ze Szpitala Gotyckiego, którego pacjentem była ofiara * magii.
* czł: Mariusz Czyczyż, agent Zajcewów z misją otwarcia zamtuza we wsi Gęsilot. Wynajęty przez Galę Zajcew.
* mag: Gala Zajcew, prawdopodobny mastermind planu konstrukcji Węzła Emocjonalnego w Gęsilocie i zleceniodawczyni Czyczyża.

# Lokalizacje:

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Leere
                1. Przodek
                    1. Centrum
                        1. Szpital Gotycki
                        1. klub "Czarny Dwór
                1. Gęsilot
                    1. chata sołtysa
                    1. chata Pirzeców
---
layout: inwazja-konspekt
title:  "Dlaczego Kret w jeziorze?"
campaign: wizja-dukata
gm: żółw
players: kić, dzióbek
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [180214 - Nie podłożona świnia Łucji](180214-nie-podlozona-swinia-lucji.html)

### Chronologiczna

* [180112 - Chrumpokalipsa](180112-chrumpokalipsa.html)

## Kontekst ogólny sytuacji

### Opis sytuacji

* Echo Wielkiego Guriniego(?)

## Punkt zerowy

* Yizdis, któremu doradził Wielki Gurini chce podtrzymywać COŚ przy istnieniu przy użyciu karcianki kolekcjonerskiej
* Ludzie mega wkręcają się w tą karciankę. To jest jak Pokemon Go, ale lokalne. Gamifikacja "ku dobremu".
* Nie masz -> nie należysz. Dotyczy raczej dzieciaków ze szkół

## Potencjalne pytania historyczne

* null

## Misja właściwa

**Dzień 1**:

Osiem dni po Chrumpokalipsie. Były załamania rąk, były problemy, dużo się działo - niekoniecznie dla naszego Zespołu. Osiem naprawdę złych dni - dokumenty, pytania, dokumenty, pytania... więc jak tylko pojawiła się okazja zrobić coś korzystniejszego (zwłaszcza jak może podnieść ich reputację), to się zgłosili na ochotnika i Centrala w osobie lorda terminusa Sylwestra Bankierza zaaprobowała to jako opcję.

Teren na którym ma się odbyć akcja jest pod bezpośrednim wpływem tien Karmeny Bankierz; wyjątkowej służbistki i męczyduszy. A ta akcja to dosłownie działania typu "malowanie trawy" na przybycie Karmeny. A jak trawa nie będzie pomalowana, cóż...

Anatol nie jest szczęśliwy. Bardziej nagana niż korzyść. Kinga zaczyna się zastanawiać KTO chce podłożyć jej świnię i komu podpadła.

Zebrali wszystkie możliwe informacje odnośnie zarówno Karmeny jak i miejscowości, zebrali dane, po czym ruszyli.

* Karmena jest osobą dość... specyficzną. Powiązana z Newerją Bankierz. Aristocratic stupidity.
* Chodzi o to, by nikt nie powiedział, że pod rządami Świecy a nie Newerji ten teren popada w ruinę i świnie.
* To jest teren Karmeny. Newerji nie zależałoby na zniszczeniu terenu; dała go jej.
* Na terenie jest jezioro, niewielki rezerwuar energii magicznej. Jest to porozpraszane, więc ogólnie jest spokojnie.

Czyli trzeba sprawdzić czystość terenu. A żadne z nich nie jest magiem detekcyjnym. Nabrali więc trochę artefaktów ze sobą ze Świecy; takich prostszych, katalitycznych. Ale Kinga jest paranoiczna. Chce dowiedzieć się więcej o całej tej sprawie. Esuriit? Świnie? No i chce zrobić tak dobrą robotę, by móc uzyskać pochwałę. (Typowy): Kinga ma czas, nie są to ukryte dane. Popytała też osoby, które nie chciały tam jechać. 7S-2R-4P. P (reroll) -> S.

* Sylwester Bankierz ma ciągłe problemy z Newerją. Wygrają jeśli problemem będzie zachowanie Karmeny. A po nich nikt nie spodziewa się subtelności politycznych.
* Karmena ma przeciwnika politycznego. To Ewelina Bankierz. Ale Karmenie Newerja szczególnie nie wierzy; dostała tylko Małoząbie.
* Karmena pojechała na imprezę, nie została ewakuowana. Bo miała pretekst; na imprezę zaprosił ją Grazoniusz Bankierz.
* Warunki przegranej: Świeca wygląda na słabą i niekompetentną. Lub, że wysłała ZBYT kompetentnych magów do rozwiązania problemu.
* Alfred Janicki - zarządca i lokaj Karmeny. Alfred zarządza majątkiem i terenem; raczej zarządza terenem dyskretnie i cicho. Sekretarz Karmeny - myśli, by ona nie musiała. Były terminus; po Zaćmieniu moc poszła w cholerę. Ogólnie, kompetentny i uważny. Karmena mu nie ufa bo jest jej niańką. Płaci mu Newerja. Alfred przedtem był "sekretarzem" Eweliny.
* Alfred przez pewien czas nie był tu obecny; pomagał w osłanianiu terenu.

Małoząbie.

Alfred, po skontaktowaniu się przez magów, zaprosił ich do Karmazynowego Pałacu. Elegancko ubrany, 47-letni czarodziej. Zaprosił ich na obiad, podczas którego Eskalacyjnie wypytał o to czemu tu są i jak to wygląda. Powiedział, że przy jeziorze jest więcej energii. Oraz to miejsce stało się większą atrakcją turystyczną. Jest sam; musi skupiać się na efemerydach, majątku oraz viciniusach. Oni powiedzieli, że chcą się rozejrzeć i sprawdzić co jest grane, czy po przebiciu Esuriit nie ma czegoś strasznego.

Eskalacja. Alfred chce dowiedzieć się czemu tu są i co chcą osiągnąć (2-2-2). Zespół chce: przekonać Alfreda że są niegroźni i mają takie rozkazy. 

Zespół zdradził się, że boją się że Karmena jak tu będzie to będzie piekło. (Typowy) Oni dodali informację, że mieli do czynienia z Chrumpokalipsą. On dodał, że jest overwhelmed - dużo się dzieje w okolicy i nawet jako słabszy terminus jest w stanie pomóc. Bo ten  majątek tutaj nie ma problemów i nie jest zagrożony.

Gdy doszło do dalszej rozmowy, Alfred zaproponował im, by poszli na inny teren, nie tutaj. On nawet pokaże gdzie. Oni powiedzieli, że mają rozkazy i muszą być tu, będą raporty. Alfred odparł, że w raportach należy pisać nieprawdę i robić to, co uważa się za słuszne. Słysząc to, uczniowe zesztywnieli. Faux pas wysokiej klasy.

Kinga zdecydowała, że chce się dowiedzieć, czemu on się chce ich stąd pozbyć. O co tu chodzi. (Łatwy) Alfred się martwi - dookoła dużo się dzieje, a jeszcze dostał dwóch takich którzy robią jazdę i problemy. Musi ich pilnować :-(. Nie może pomóc a oni ogólnie nie chcą pomagać tylko chcą mieć porządne papiery.

Oki. Czas zobaczyć tych nieszczęsnych turystów. Czemu ten teren stał się terenem turystycznym? To nie do końca ma sens. Anatol zna się na ludziach. Poszedł do lokalnego biura informacji turystycznej przy urzędzie, popatrzył i na bazie tego podszedł do piątki młodych ludzi. Kinga wyczuła na swoim świecowym detektorze odpływ energii od ludzi. Anatol podszedł do nich jak się mecz skończy. Okazuje się, że grają w kolekcjonerkę - "Złoty Kot". Całkiem niezła gra strategiczna; nie celuje w casuali. Albo jesteś fanem albo odpadasz. Aha, i ma "świetny model biznesowy" - niewiele płacisz.

Odkryli z pomocą Anatola, że "Złoty Kot" którego trzeba znaleźć jest pod wodą. W jeziorze? Kinga podrzuciła im trackera; chce zobaczyć co się stanie. A Kinga podrzuciła im tracker. No i jest referral bonus; Anatol dał referrala jednemu z nich.

Oki, pożegnali turystów. Sporo jest turystów szukających skórek. Czas pograć w grę. Gra daje wytyczne gdzie warto walczyć w grę; tam, gdzie są potwory. A gdzie są? Tam, gdzie aura emocjonalna i magiczna. Plus, im więcej graczy koło Ciebie, tym lepsze potwory dla każdego. Więc Anatol poszukał niskiego pola magicznego - np. pod randomowym sklepem. Niech zatem będzie to w maksymalnie odizolowanym miejscu i takim, by wzmocnić sensory. Padło na Bar Mleczny Rusałka.

Na niskim poziomie trudności, nic. Na wysokim jednak... jest Połączenie Energii Magicznej. Nie ma drenażu, ale jest połączenie. Czyli Coś Się Dzieje.

Kinga zdecydowała się na eksperyment. Artefakt Dare Shiver. Coś, co podbija poziom emocjonalny. Sprawdźmy, czy działa. I tak - udało się połączyć. Doszło do transferu energii (Łatwy). Nie tylko Anatolowi udało się wysłać właściwą ilość energii, ale też w formie kontrolowanej. Jest w stanie nawet przy natężonym poziomie emocjonalnym to kontrolować. Innymi słowy, to jest skierowane do ludzi a nie do magów.

Kinga szuka co tu się dzieje i gdzie prowadzi ta energia. Co się z tą energią dzieje. Gdzie prowadzi.

* Nawet człowieka nie wydrenuje bardzo mocno. Nawet w sprzyjających warunkach nie zagraża człowiekowi. Mało groźny pasożyt.
* Energia idzie na jezioro, skąd idzie gdzieś dalej. Wzmocnienie jeziora jest skorelowane z tą aplikacją.
* Gdy wybrany był wyższy stopień trudności, nie było AI a coś innego. Walka z czymś innym. To coś jest przeciwnikiem i otworzyło techno-katalitycznie energię.
* Energia ciągnięta jest z jeziora i uwalniana do jeziora. To próba wzmocnienia energetycznego jeziora. Jezioro jest zabarwiane emocjonalnie i wzmacniane.
* To się dzieje teraz jak Karmeny nie ma w okolicy. Ale aplikacja powstała zanim Karmena stąd odeszła. Po prostu jak jej nie ma, to to ruszyło.

Spacer nad jeziorem. Czy jest tam jakiś upływ? Czy tylko bufor? Ogólnie, Kinga sobie przypomniała - robiła przeszukiwanie o tym terenie i wyszło, że to jezioro to niebezpieczny rezerwuar, bo głęboko pod nim jest prąd magiczny. Jak przekroczy pewien poziom, wypłynie i połączy się z tym co jest "pod spodem". Kinga sprawdziła, czy ktoś to zgłaszał. Ano, nie. Więcej, odradzano by - teraz są świnie w okolicy. Plus, to złe jezioro.

Anatol skontaktował się z Sylwią, agentką Świecy która raczej Anatola lubi (he helps people). Poprosił ją o informacje na temat firmy, która wydała tą konkretną apkę. Dostał informację po kilku godzinach - Kret IV. Pojazd drążący (jest w opisie aplikacji). YyidathCorp, twórca aplikacji. Kinga sprawdziła poziom energii w jeziorze - gdzieś jest jakiś niewielki sink energii magicznej.

Oki. Jest szansa że nieszczęsny Kret IV trafił tu, przyteleportowany przez przepływ energii który go tu rzucił 10 lat temu. Ale gdzie? Opowieść o złotym kocie świadczy, że na dnie jeziora gdzieś pod mułem. A na pokładzie Kreta IV były 3 koty (w tym jeden imieniem Złotko) i pilot, czarodziej.

Wait. Kinga jest iluzjonistką. Jest szansa na jakieś dyskretne (i efektowne!) wejście do sprawy i odnalezienie nieszczęsnego Kreta IV na dnie jeziora. Jak zatem znaleźć w wysoko magicznym miejscu (acz nie Węźle) podziemnego metalowego Kreta?

Sylwia, po namowie ze strony Anatola, wysłała maila do YyidathCorp. Ku zaskoczeniu wszystkich, Yyizdath osobiście skontaktował się z Anatolem. Porozmawiali chwilę o pomniejszym bycie który 10 lat temu tam był i stracili z nim kontakt. Yyizdath zauważył, że jeśli jest połączenie z internetem to spawn powinien móc się połączyć; nie miał okazji. Więc Anatol by ściągnąć agenta Yyizdatha kupił w sklepie:  Disco Robot Tosy ;-).

Yyizdathspawn się inkarnował. Walnęło, część bezpieczników poszła itp. Yyizdathspawn jest nieszczęśliwy, ma kiepskie ciało. Ale echo było tak mocne, że aż Alfred poczuł sygnał... pojawił się i zażądał wyjaśnień. Dowiedział się jak wygląda sytuacja - mają jednostkę wsparcia Świecy (nie wie, że to Yyizdathspawn) i usuwają root cause pod wodą jeziora. Gdy dowiedział się jak wygląda ta sprawa i jakie rzeczy groziły jezioru, wycofał się.

Anatol składa zaklęcie - swobodne poruszanie się pod wodą wraz z Yyizdathspawnem. Exciting. Źródłem energii jest jezioro. Bańka powietrza. Pierwsze zaklęcie w systemie ;-). Niestety, aura energii Anatola jest tak silna, że zamaskowała Yyizdathspawna. Nie będzie w stanie być rozpoznany przez tego drugiego, w systemach Kreta. Yyizdathspawn przy dnie pokazał gdzie to mniej więcej jest. Kret tam jest.

Czas na DRUGIE zaklęcie. (Łatwy). Trzeba odkopać Kreta IV. Anatol zakasał rękawy. Pełen sukces. Trzeba teraz interfejsować z Kretem... i klęska. Nie ma sygnału. Kret nie odpowiada, Yyizdathspawn też nie potrafi się połączyć. A Kret nie puszcza śluzy. Komunikuje się przez chrypienie przez głośniki.

Po... pewnej dyskusji z chrypiącymi głośnikami... wyszło na to, że Yyizdathspawn w Krecie nie chce otworzyć Kreta, bo ma tam koty. One przeżyły. A dokładniej, nowa generacja kotów przeżyła. Yyizdathspawn bredzi, że mag też przeżył (niemożliwe). Po negocjacjach i przekonaniu, że mogą tam wejść Zespół wkroczył do najgorszej kuwety w egzystencji...

Kret od środka jest za***any. Są tam zwłoki (zasuszona mumia) ogryzionego przez koty maga, "podtrzymywanego przy życiu" przez technologię Yyizdathspawna. Ogólnie, ciężko. Sam Kret jest o dziwo dość sprawny, chociaż nie do tego był zaprojektowany. Uszkodzony; faktycznie wpadł w prąd magiczny i nie do końca wiadomo jak i czemu. Uszkodzony Yyizdathspawn powiedział, że uzyskał internet i zrobił grę, ponieważ dostał taką sugestię od czarodziejki jaka się z nim skontaktowała.

Oki, dogadali się i teraz czas przenieść Kreta. Anatol i Kinga załatwili hangar Świecy i... poszła Soczewka na Transporcie. Kret wpadł w pływ magiczny z największym hukiem w okolicy (efektowne jak cholera) i trafił bezpośrednio do celu - hangaru Świecy oddalonego 100 km stąd...

Yyizdath ma odzyskać dostęp do swojego Yyizdathspawna, jakkolwiek uszkodzonego. Może go przesłuchać i sprawdzić co jest grane.

![Rysunek z konfliktami Opowieści](Materials/180499/180503-konflikty.png)

## Wpływ na świat

* Żółw: 14
* Kić: 4
* Dzióbek: 4

Czyli:

Gracze:

* Widać w Progresji

MG:

* Świeca nie chce oddać Yyizdathspawna, bo nowa Faza, powiązana z Magitrownią Histogram (5)

# Streszczenie

Kinga i Anatol mają dość raportów; dostali więc ciężką misję polityczną - patrol terenu Karmeny Bankierz. Młoda poimprezowała i okazało się, że wraca i będzie chciała zwalić winę na Świecę, co bardzo nie pasuje Sylwestrowi Bankierzowi. Na miejscu - mimo zapewnienia eks-terminusa Karmeny (z osłabioną mocą), że niewiele dzieje się w samym Małoząbiu - ktoś drenuje energię z ludzi i wpakowuje ją w jezioro. Gdy Zespół doszedł do tego, że na dnie jest Kret IV (zaginiony stary driller), odkopali go i ewakuowali. AI Kreta IV to Yyizdathspawn. Wszystko wskazuje, że ktoś tu coś więcej wiedział...

# Progresja

* Anatol Sowiński: było źle bo był Wieprz... ale teraz jest Kret IV i to było dobre. Podniesienie reputacji. Off the hook.
* Anatol Sowiński: po zakończeniu sprawy z Kretem postacie wracają i pomogą Alfredowi gdzieś w okolicy. Relacje z Alfredem++.
* Kinga Bankierz: było źle bo był Wieprz... ale teraz jest Kret IV i to było dobre. Podniesienie reputacji. Off the hook.
* Kinga Bankierz: po zakończeniu sprawy z Kretem postacie wracają i pomogą Alfredowi gdzieś w okolicy. Relacje z Alfredem++.
* Kinga Bankierz: dostęp do Yyizdatha i tematów Yyizdathokształtnych przez Disco-Yyizdathspawna
* Karmena Bankierz: dorobiła się niezadowolenia ze strony Newerji. Dlaczego na JEJ terenie dzieją się takie rzeczy a ona nie wie?
* Kaja Maślaczek: skutecznie wygenerowała niezadowolenie Newerji wobec Karmeny, co ratuje Ewelinę od "bycia drugą kiepską".
* Ylytis: beznadziejnie zakochany w czteronożnych futrzakach zwanych kotami.
* Ylytis: posiada wiedzę z Faz, przez które przechodził Kret IV. Nie pomogło to na jego stan emocjonalny i logiczny ;-).

## Frakcji

* Srebrna Świeca: z logów Kreta IV wydobędzie się informacje o tym, kto wmanipulował uszkodzonego Yyizdathspawna w potencjalną katastrofę.
* Srebrna Świeca: nie chce oddać uszkodzonego Yyizdathspawna Yyizdathowi, ponieważ tam jest informacja o nowej Fazie a Yyizdath ma... historię eksploracji.

# Zasługi

* mag: Kinga Bankierz, głównie zbiera informacje i przy użyciu artefaktów prowadzi śledztwo. Ku swej ogromnej żałości.
* mag: Anatol Sowiński, poczarował! Wygrzebał Kreta z mułu i odkrył Tajemnicę Gry Komputerowej. Aha, świetnie negocjuje z uszkodzonym Yyizdathspawnem.
* mag: Stefan Bułka, opiekun i dowodzący Kingi i Anatola. Chciał by przycupnęli... wrócili z Kretem. I znowu było głośno. Ale teraz dobrze.
* mag: Sylwester Bankierz, jest zdesperowany by nie było katastrofy politycznej, więc do patrolu terenu ściągnął nadmiernie kompetentnych uczniów terminuskich.
* mag: Karmena Bankierz, zbyt zainteresowana imprezami by kompetentnie zarządzać swoim terenem; zostawiła na straży wiernego Alfreda.
* mag: Sylwia Zasobna, agentka wspomagająca Kingę i Anatola. Bardzo sympatyczna technomantka; zebrała informacje dla Kingi i Anatola odnośnie okolicy.
* vic: Yyizdath, żywo zainteresował go powrót Kreta IV; wysłał do pomocy uczniom terminusa Świecy swojego Yyizdathspawna.
* mag: Alfred Janowiecki, były terminus, były opiekun Eweliny Bankierz, aktualny Karmeny Bankierz, o bardzo słabej mocy ale sercu po właściwej stronie.
* vic: Ylytis, uszkodzony Yyizdathspawn który towarzyszył Kretowi IV przez jego długie przeboje. Aż do teraz.

# Plany

* Ylytis: zainteresowany NIE wróceniem do Yyizdatha; chce zniknąć i mieć własny dom z dużą ilością kotów

## Frakcji

* YyizdathCorp: zwrócił oczy w kierunku na teren pod kontrolą Pauliny z uwagi na potencjał nowej Fazy

# Lokalizacje

1. Świat
    1. Primus
        1. Mazowsze
            1. Powiat Pustulski
                1. Małoząbie, niewielka miejscowość niedaleko jeziora w domenie Karmeny Bankierz
                    1. Karmazynowy Pałac, centrum dowodzenia i królestwo Karmeny Bankierz
                    1. Jezioro Małoząbskie, w którym znajdował się Kret IV i który był centrum rozgrywki gry Yyizdathspawna
                    1. Bar Mleczny Rusałka, niestety: główna restauracja i centrum kulturalne Małoząbia (XD)

# Czas

* Opóźnienie: 8
* Dni: 2

# Narzędzia MG

## Cel Opowieści

* Sprawdzić nową mechanikę magii
* Ma być wesoło i podobać się Dzióbkowi

## Po czym poznam sukces

* null

## Wynik z perspektywy celu

* null

## Wykorzystana mechanika

Mechanika 1804

Wpływ, klasycznie:

* 8 kart balans między graczami; 8-10 kart to zwykle dzień
* każda porażka = +2 pkt wpływu dla gracza
* każdy skonfliktowany sukces = +1 pkt wpływu dla gracza
* każda karta = +1 wpływ dla MG
* każde 5 kart zaokr. w górę: +1 pkt wpływu dla gracza

---
layout: inwazja-konspekt
title:  "Cienie procesu Izy"
campaign: adaptacja-kralotyczna
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [180310 - Kraloth w piwnicy](180310-kraloth-w-piwnicy.html)

### Chronologiczna

* [180310 - Kraloth w piwnicy](180310-kraloth-w-piwnicy.html)

## Kontekst ogólny sytuacji

### Opis sytuacji

**Wizja artysty**:

brak

**Strony konfliktu, atuty i słabości**:

![Strony, atuty, słabości](Materials/180399/story_cienie_procesu_izy.png)

### Wątki konsumowane:

brak

## Punkt zerowy:

## Mapa logiczna Opowieści:

brak

## Pytania Generatywne:

* Ż: Skąd Maria znalazła ludzi do dokarmienia kralotha?
* K: Creepy, samotni ludzie w klubach, zgarniała i zapraszała, dawała się zaczepiać...

## Potencjalne pytania historyczne:

* Czy Maria odda się pod opiekę Arazille? (TAK: 5)
* Czy Maria zostanie napadnięta i potraktowana plasterkiem? (TAK: 5)

## Misja właściwa:

**Dzień 1**:

Siluria jest już w lepszej formie, nie tak zdewastowana. Norbert czyni cuda, acz zajęło to dzień... Silurii pogorszyło humor, że Kromlan zajął się Marią a nie KADEM. Karolina ewakuowała pełza ORAZ Silurię, nie miała sił walczyć jeszcze o Marię. A Siluria nie była w stanie walczyć.

Hralglanath został już "naprawiony" przez Edwina do poziomu aktywnego ciała, acz bez sprawnego umysłu. Siluria poszła po próbki by podrzucić je Marii i zastała tam... Karolinę. Która próbuje kralothowi śpiewać. Nie wychodzi jej. Nie jest w tym dobra XD. Zapytana przez Silurię, wyjaśniła, że rozmawiała z Edwinem i Karina Paczulis stwierdziła, że śpiewanie kralothowi może pomóc mu wrócić i odbudować neuropołączenia. Dla Silurii to DUŻE zaskoczenie - nie słyszała, by śpiewanie kralothom mogło pomóc.

Karolina poprosiła Silurię o pomoc. Po pierwsze, są na Fazie Daemonica - na pewno się uda. Po drugie, są KADEMem. I uśmiechnęła się tak jak tylko Karolina potrafi - z optymistyczną wiarą. Tak czy inaczej, Karolina odesłała Silurię do Edwina - szukał jej. Siluria jest w stanie wyciągnąć z kralotha próbki dla Marii. Niestety, nie reaguje szczególnie na Silurię - bardziej jak golem niż jak istota inteligentna. Karolina patrzy na tą scenę ze smutkiem, ale zaraz się rozchmurzyła.

Siluria poszła do Edwina - w końcu chciał ją widzieć. Edwin przywitał ją dość ciepło jak na niego. Powiedział, że Siluria zajechała kralotha - a najdziwniejsze w tym wszystkim jest to, że kraloth do tej pory wzmacniał i utrzymywał Czerwoną Bibliotekę a nagle ich pozabijał. Ale Marii nie zabił. To Maria doprowadziła kralotha do swojego domu, sama go ewakuowała z biblioteki. Dodatkowo po przesłuchaniu Silurii Edwin stwierdził, że Hralglanath był ciężko chory - a on nie zna się na chorobach kralothów. Niech Siluria znajdzie eksperta w Millennium. Nie pomaga to, że kraloth najwyraźniej wysyłał ludzi do kościoła Arazille w Czeliminie, co nie ma sensu. Zupełnie...

Zapytany o to, skąd to "śpiewanie kralothowi", Edwin wyjaśnił: zdaniem KADEMu kraloth ma być wyleczony lub wykorzystany jako biosyntezator. Nie ma możliwości trzymać go na KADEM Daemonica. Zdaniem Karoliny jednak kralotha należy wyleczyć lub pochować. Czemu - Edwin nie rozumie. Karina była tak zaskoczona, że aż sobie z Karoliny zażartowała... a Karolina to łyknęła. Więc zdaniem Edwina to tykająca bomba, zwłaszcza, jak Karolina zarazi masę krytyczną magów KADEMu tą myślą.

* Nie powinien umierać! Nie zrobiłam nic nietypowego związanego z punktu widzenia jego biologii! - Siluria.
* Może umarł ze _wstydu_ - Edwin, krzywo.

Siluria, lekko zdesperowana, zdecydowała się na podróż na Fazę Primus. Spotkać się z Netherią. Jak tylko wylądowała na Primusie, dostała wiadomość od Marka Kromlana. Siluria ma się z nim spotkać asap. Nie zmienia to faktu, że Siluria chce się spotkać z Netherią najpierw. Najlepiej w Rzecznej Chacie.

Netheria ze stoickim spokojem wysłuchała opowieści Silurii. Siluria powiedziała, że potrzebny jej specjalista - i to nie może być ani Rafael ani Krystalia. Netheria pokręciła głową. Laragnarhag? Siluria zaznaczyła, że nikt z nich nie wejdzie na KADEM Daemonica. Neti odbiła, że to jest wiedza możliwa do użycia przeciw kralothom Millennium; niech zatem będzie KADEM Primus. Neti zaproponowała duet Melodia + Laragnarhag. To dobra para. Siluria westchnęła - trudno będzie to szybko zaaranżować. Neti potwierdziła - jej też. Dwa dni co najmniej...

Gdy Siluria wyszła z pokoju spotkaniowego z Netherią, napotkała Kromlana, pijącego soczek wiśniowy. Terminus spojrzał na nią z ukosa. Siluria przysiadła się do niego, też coś zamawiając.

* Tien Kromlan, jak to dobrze, bo miałam jechać do Miłejki - Siluria ze słodkim podejściem maskotki
* Nie spieszyłaś się na spotkanie z terminusem, którego raz już oszukałaś - Kromlan, krzywo
* Tien Kromlan, ubolewam nad tym, że pan tak uważa. Nie było moim zamiarem wprowadzenie kogokolwiek w błąd... - skruszona Siluria jest skruszona
* Albo Twoja KADEMowa sojuszniczka jest winna, bo miała mi powiedzieć, albo Ty jej zakazałaś - Kromlan, zimno
* Nie zakładałabym złej woli. Dynamiczna sytuacja, Karolina ratowała ludzi -Siluria
* ...Pojmaliście kralotha żywcem? - Kromlan, zainteresowany
* Niestety, kraloth zmarł - Siluria wyraźnie posmutniała
* ...oczywiście... - Kromlan facepalmował - I ja mam w to uwierzyć? KADEM nie zabija jeńców.
* Niestety, ten kraloth był chory. Tylko dlatego w ogóle przetrwałam - Siluria, ze smutkiem - Niemniej, tien Kromlan, jeśli takie pana życzenie - mogę go panu pokazać.
* Spasuję. Teraz to problem KADEMu. - Kromlan szybko zaprzeczył - Kiedy ci ludzie wrócą do społeczeństwa?
* Od paru dni do może dwóch tygodni - Siluria

Kromlan powiedział, że Świeca zatem zajmie się przygotowaniem historyjki, by to miało sens. Siluria próbuje go trochę roztopić, ale jej nie idzie. I to nie chodzi o kralotha. On serio nie ma nic do KADEMu, nawet teraz, jak wszystko poszło nie tak. Ale ma coś do Silurii.

Siluria spytała, co z Marią. Maria jest uzależniona, ale tematem zajmuje się Elea Maus. Ma od czasu do czasu sprawdzić co z Marią i upewnić się, że można jej pomóc. Jak Elei się nie uda, Kromlan będzie kombinował dalej - nie ma środków. Gdy Siluria powiedziała, że KADEM może jej pomóc, wyraźnie się lekko ucieszył. Wyraźnie Kromlan wysoko ceni KADEM. Mimo, że mordują kralothy do przesłuchania. Przepraszam, wypadek przy pracy.

Siluria próbkuje Kromlana. On nie lubi Diakonów, Silurii, Mausów... i chyba Świecy też. Ogólnie, Kromlan wygląda na dość samotnego terminusa, który nie lubi wielu magów. Siluria wpadła na diaboliczny pomysł - Kromlan już przyjął pomoc KADEMu. Czemu zatem nie zaproponować mu osobistej pomocy Silurii? :-). To nie byłoby logiczne.

Po zaproponowaniu tego Kromlanowi, ów powiedział, że szuka Mirandy Maus - by ta odpowiedziała za swoje czyny i stanęła przed sprawiedliwością. Podobno potrafi opierać się Karradraelowi i dlatego Abelard Maus nie umie Mirandy przesłać. Jest w okolicy Lasu Stu Kotów. Tyle Kromlan wie...

* Marek Kromlan: chce Silurię ZRANIĆ i zmusić do odstąpienia
* Siluria Diakon: chce zrozumieć, czemu on jej tak nie znosi

Siluria wie, że on nie chce dążyć do antygildiowości. Robiła co mogła i jak mogła. Nawet jeśli nie jest terminusem, NAWET starła się z kralothem. A on traktuje ją jak zbrodniarza, jakby ona chciała zepsuć mu śledztwo i tak dalej. To jest zarzut. Niech on jej powie o co chodzi. Siluria manipuluje trochę faktami, ale ej. Kromlan odparł spokojnie, że jest dobrze - śledztwo zakończone na korzyść KADEMu, on nie chce komplikować i problem został rozwiązany. On nie rozumie o co JEJ chodzi. Acz Siluria czuje tą nienawiść w tle.

Siluria zauważyła, że niekoniecznie jest w porządku, skoro on jej nienawidzi. Chce wyciągnąć sekret na wierzch. 8v6->S. Kromlana to wyraźnie uderzyło. Przebiło się na zewnątrz; nie chciał by było to widać. Zaraz skontratakował. Wstał i powiedział Silurii, że jest potrzebny - Baltazar go wzywa a on nie ma na to czasu. Power of Authority (surowiec). Siluria próbuje się obronić w formie "próbujesz odejść, gdy ja próbuję wyciągnąć rękę do terminusa Świecy - jak mogę to naprawić, by gildie działały lepiej". I powołała Baltazara jako surowiec. 10v12->SS.

Kromlan odpowiadając nie zaprzeczył, że Baltazar by chciał rozwiązania problemu - dzięki temu Siluria dowiedziała się, że Baltazar nic do niej nie ma. To ewidentnie sprawa Kromlana. Siluria też nie czuje się z tym dobrze - Kromlan jest sam, nie jest w stanie z nią walczyć, a tu ONA bawi się w tyrankę a jemu wyraźnie nie sprawia to przyjemności i wyraźnie on się z tym męczy. Chce stąd wyjść.

Siluria uderzyła wrednie - "nawet w Szlachcie byli dobrzy magowie". Wierzy, że on jest jednym z nich. Kromlan został wyprowadzony z równowagi. Powiedział - głośno - że przez NIĄ dołączył do Szlachty. Świeca nie ochroniła lojalnej terminuski - Izy Łaniewskiej - przez nią, Silurię Diakon. I oni ją splugawili. A teraz - on wychodzi. Nie ma czasu na rozmowę z... NIĄ.

Siluria uderzyła tak głośno, by ta publiczność słyszała to samo co on ich sformatował. Powiedziała, że jest jej przykro, że Kromlan tak na to patrzy. Zawsze była możliwość prośby o spotkanie z Infensą. Nikt nigdy jej niczego nie zakazał ani nakazał odkąd kuracja się skończyła a teraz jest szczęśliwsza i skuteczniejsza. Sam musiałby się przekonać.

* Tak jak kraloth, ona nie żyje. - Kromlan na odchodnym do Silurii
* Na pewno w jej wypadku jest wygodniej tak myśleć - Siluria, okrutnie do Kromlana, by zadać mu najsilniejszą możliwą ranę

Terminus wyszedł. Siluria - sala (test Zwykły). Skonfliktowany sukces. Ogólnie, sala silnie za Silurią, acz jest niewielka grupka magów Świecy, którzy uważają, że Kromlan miał RACJĘ i Szlachta była rozwiązaniem - gdyby nie Karradrael, Świeca byłaby teraz lepsza. Bo Szlachta miała rację. I może Kromlan mógłby być z nimi..?

* Żółw: "Jak długo nie mówią na siebie 'Mieszczaństwo', jest OK"

No, nieźle. Przynajmniej Siluria wie, co Kromlan ma przeciwko niej. Mogło być gorzej. W tym świetle kolejny krok był oczywisty. Jeszcze tego wieczoru, Siluria odwiedziła Infensę Diakon.

Infensa strzela sobie na strzelnicy; coś z bronią. To zawsze kochała, i to się nie zmieniło. Zapytana przez Silurię o Kromlana, wyjaśniła: pochodzą oboje z Rusznicy Świecy, Zakonu czy raczej domu. Rywalizowali ze sobą. Ona była tą radykalną, on tym łagodnym dającym szansę. On zawsze bał się kralothów i tego, że one Wypaczają magów. Więc ona stała się tym, czego on się najbardziej bał. Zwłaszcza teraz, jak Rusznica została zniszczona. Aha, jego miłością zawsze była sztuka.

Siluria wpadła na pomysł. Ściągnąć jak najbardziej znaczącą broń Rusznicy Świecy i podarować ją w prezencie. Najlepiej niech to broń Wielkiego Mistrza, ale wystarczy którąkolwiek z broni Kirasjerów. Infensa nie ma nawet pojęcia jak taką zdobyć, ale Siluria pomyślała o Marianie Łajdaku i jego sieci kontaktów...

Siluria BARDZO doceniła, że Kromlan nawet nie wie, że regularnie mija się z Infensą. 

Cóż, czas wrócić na KADEM. Bez żadnego problemu. I Marian Łajdak. Infensa wie, kogo pytać, Siluria ma znajomych i ciekawostki a Łajdak ma skilla. Konflikt jest Bardzo Trudny (15), Siluria ma +8. 16v15 -> Sukces. Siluria dostanie jakąś "bezużyteczną" broń Rusznicy, po kosztach KADEMowych. Łajdak się wszystkim dla Silurii zajmie. Jest w tym najlepszy.

Tymczasem u kralotha...

Błażej Falka i Karolina Kupiec robią koncert dla martwego kralotha. A Lucjan Kopidół, ekspert od Pryzmaturgii i lekko... niedopasowany mag kombinuje z analizą Pryzmatu. Zmartwionej Silurii przyszło, że to się cholera jeszcze im uda... Siluria rzuciła smutne spojrzenie Błażejowi. NIKT nie chce martwić Karoliny. A Kopidół jest zdziwiony - nie działa wpływ Pryzmatyczny. Zupełnie jakby KTOŚ włączył kompensatory pryzmatyczne.

Siluria ze smutkiem wyjaśniła Karolinie, że załatwiła eksperta. Ekspert zajmie się kralothem. A tymczasem - niech Karolina nie wpływa nań pryzmatycznie. Przekonała Karolinę, ona jest skłonna poczekać. Korzystając z okazji, Lucjan Kopidół przypomniał o dziwnych odczytach na Mare Felix - chciałby wysłać tam ekspedycję, ale nikogo nie interesuje małe, nudne Mare Felix (odpowiednik Lasu Stu Kotów).

Siluria przekonała Karolinę, że zagrożenie które skrzywdziło kralotha może się rozprzestrzenić na inne istoty. Karolina zdecydowała się zmontować ekspedycję i spojrzeć na Las Stu Kotów. A gdy Kopidół ponownie poprosił, Siluria powiedziała, że spyta Whisperwind. Kopidół się ucieszył.

Whisperwind i Warmaster. Whisper podeszła sceptycznie, że coś się może dziać na Mare Felix, ale Siluria opowiedziała o powiązaniu tego z kralothem. Whisper nie ma nic przeciw; sprawdzi to z Warmasterem. I tak nie mają nic lepszego do roboty.


## Wpływ na świat:

JAK:

* 2: MUSIK: gracz mówi w jaki sposób jedna z obcych postaci wspiera coś związanego z motywacją JEGO postaci
* 2: CLAIM na wątku / postaci / okoliczności
* 2: do elementu Historii lub przeszłego konfliktu dodajemy "ale" lub "oraz"
* 2: dodajemy pytanie, które musi zostać odpowiedziane na jakiejś z przyszłych misji
* 2: odpowiadamy na pytanie, które pojawiło się na jakiejś z misji
* 3: gracz mówi w jaki sposób jedna z obcych postaci wspiera coś związanego z motywacją JEGO postaci
* 3: zmieniamy kontekst okoliczności czegoś z Historii - scena z przeszłości / fakt?
* 3: do elementu spoza Historii dodajemy "ale" lub "oraz"
* 3: dodajemy znaczącego NPC powiązanego z elementem Historii
* 3: pozyskanie przez dowolną postać znaczącego surowca mającego sens z perspektywy misji
* 3: dodanie nowego elementu Historii
* 3: dodanie przyszłego lub przeszłego faktu; czegoś, co się wydarzyło lub wydarzy

Z sesji:

1. Żółw (7)
    1. Marek Kromlan zostaje ogniskiem nowego ruchu magów Świecy, którzy chcą ideałów Szlachty ale bez wypaczeń. I nie wie o tym. (3)
    1. Karolina jest bardzo zdeterminowana ALBO wskrzesić ALBO pochować kralotha (3)
    1. Kilka Ognistych Niedźwiedzi zaatakuje docelowo Marię, dla pokazania sygnału (3)
1. Kić (7)
    1. Marian Łajdak przez czysty przypadek znalazł broń osoby, która osobiście dużo znaczy dla Kromlana - i nikt o tym nie wie (2)
    1. Edwinowi udało się wydobyć coś cennego z kralotha - czemu Arazille i czemu kraloth nie skrzywdził Marii (3)
    1. Whisper i Warmaster wrócą z czymś, co się przyda w zrozumieniu sytuacji w Lesie Stu Kotów (3)

# Streszczenie

Ku smutkowi Silurii, kraloth padł. Ku większemu smutkowi, Karolina jest zdeterminowana, by go "ożywić"... szczęśliwie, Siluria ściąga ekspertów z Millennium, Melodię i Laragnarhaga. Ścięła się też z Kromlanem, od którego wydobyła, że ów jest wrogi Silurii za to co spotkało "jego" Izę Łaniewską. Świeca poluje na Mirandę Maus - a Karradrael podobno nie może jej ściągnąć. Siluria poprosiła Whisper o pomoc Kopidołowi, który widzi dziwne efekty pryzmatyczne w Mare Felix powiązane z Lasem Stu Kotów.

# Progresja

* Siluria Diakon: pozyska (przez przypadek) broń osoby, która dużo znaczy dla Kromlana od Mariana Łajdaka.
* Edwin Blakenbauer: pozyskał informacje od martwego Hralglanatha odnośnie tego, czemu Arazille i czemu kraloth nie skrzywdził Marii.
* Whisperwind: zdobyła informacje odnośnie Mare Felix, które pomogą zrozumieć co dzieje się w Lesie Stu Kotów.
* Warmaster: zdobył informacje odnośnie Mare Felix, które pomogą zrozumieć co dzieje się w Lesie Stu Kotów.
* Marek Kromlan: pogorszył sobie reputację w Rzecznej Chacie, bo wydarł się na Silurię o Infensę.

## Frakcji

# Zasługi

* mag: Siluria Diakon, wyciągnęła od Kromlana powód jego niechęci do niej, powstrzymała Karolinę przed pryzmatycznym wskrzeszaniem kralotha i sporo facepalmująca
* mag: Karolina Kupiec, śpiewała martwemu kralothowi, bo Karina ją w to wrobiła. Organizuje kampanię jak wskrzesić kralotha.
* mag: Edwin Blakenbauer, próbuje zabezpieczyć kralothy przed losem Hralglanatha. Pomaga Silurii z rannymi ludźmi i rozwiązaniem tej zagadki.
* mag: Karina Paczulis, niezbyt poważna; wkręciła Karolinę w śpiewanie martwemu kralothowi (że to niby pomoże).
* mag: Netheria Diakon, pomaga Silurii znaleźć ekspertów od chorób kralothów. Stanęło na Melodii i Laragnarhagu.
* mag: Marek Kromlan, niezbyt lubiący kogokolwiek terminus; Silurii wyjątkowo nie lubi za Infensę. Sprzyja KADEMowi, ze wszystkich rzeczy. Zrezygnowany.
* mag: Infensa Diakon, groźniejsza niż kiedykolwiek, powiedziała Silurii o tym, że z Markiem Kromlanem łączyły ją frakcja, rywalizacja i przyjaźń.
* mag: Marian Łajdak, jak trzeba sprowadzić coś dziwnego dla Silurii, nie ma nikogo lepszego od niego. Sprowadził świetną broń ceremonialną dla Kromlana.
* mag: Błażej Falka, robi koncert dla martwego kralotha, bo Karolina poprosiła. Nie ma innego powodu.
* mag: Lucjan Kopidół, pomaga wskrzesić martwego kralotha, ale tak naprawdę martwi się dziwnymi odczytami w Mare Felix.
* mag: Miranda Maus, nieobecna, na którą poluje Świeca i która - podobno - potrafi oprzeć się wpływowi Karradraela.
* mag: Whisperwind, poproszona przez Silurię, poszła na zwiady do Mare Felix.
* mag: Warmaster, poproszony przez Silurię, poszedł na zwiady do Mare Felix.

# Plany

* Siluria Diakon: chce doprowadzić do zetknięcia się Marka Kromlana i Infensy Diakon.
* Siluria Diakon: chce pomóc Marii Przysiadek i zrozumieć o co chodzi z nią i Arazille.
* Siluria Diakon: chce zdobyć ceremonialną broń kirasjera Rusznicy, by lekko wstrząsnąć Kromlanem.
* Karolina Kupiec: bardzo zdeterminowana, by ALBO wskrzesić ALBO pochować Hralglanatha - ale na pewno nie trzymać go w tym stanie.
* Edwin Blakenbauer: chce zrozumieć, co stało się nieszczęsnemu Hralglanathowi - i czy to może pójść szerzej na inne kralothy.
* Lucjan Kopidół: chce zrozumieć, co dzieje się na Mare Felix i jak może to zatrzymać (uważa za niekorzystne).

## Frakcji

* Spero Candela: Szlachta 2.0, chce pozyskać Marka Kromlana jako przedstawiciela Właściwych Myśli.

# Lokalizacje

1. Świat
    1. Faza Daemonica
        1. Mare Felix, odbijające się w Lesie Stu Kotów, gdzie Kopidół wykrył coś niepokojącego
        1. Mare Vortex
            1. Zamek As’Caen
                1. Kadem Daemonica
                    1. Skrzydło Medyczne, gdzie gawędzą ze sobą Siluria i Edwin odnośnie stawiania na nogi martwych kralothów i ich przyczyn zgonów
                    1. Skrzydło Wzmocnione
                        1. Pancerne Magazyny, gdzie przetrzymywany jest niezbyt żywy kraloth
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Centrum
                        1. Klub Magów Rzeczna Chata, miejsce spotkania Silurii i Netherii. A potem Kromlana i Silurii - gdzie Kromlan zrobił Silurii scenę.
                1. Piróg Dolny
                    1. Ulica Targowa
                        1. Błysk - Laserowy Paintball, ulubione miejsce Infensy, gdzie spotkała się Siluria z Infensą.

# Czas

* Opóźnienie: 1
* Dni: 1

# Wątki kampanii

nieistotne

# Przeciwnicy

## XXX
### Opis
### Poziom

* Trwałość: ?
* Poziom: Trudny (10)
* Siły: 
* Słabości: 

# Wątki kampanii

1. 

# Narzędzia MG

## Opis celu Opowieści

N/A

## Cel Opowieści

* 

## Po czym poznam sukces

* 

## Wynik z perspektywy celu

* 

## Wykorzystana mechanika

Mechanika 1801, wariant z daty 1803

Wpływ:

* 10 kart balans między graczami; 8 kart to zwykle dzień
* każda porażka i skonfliktowany sukces = +2 pkt wpływu dla gracza
* każda karta = +1 wpływ dla MG
* każde 5 kart zaokr. w górę: +1 pkt wpływu dla gracza


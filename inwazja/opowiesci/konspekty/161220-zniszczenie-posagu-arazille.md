---
layout: inwazja-konspekt
title:  "Zniszczenie posągu Arazille"
campaign: nicaretta
gm: żółw
players: kić, raynor
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [161206 - Ucieczka Sióstr Światła (HS, temp)](161206-ucieczka-siostr-swiatla.html)

### Chronologiczna

* [161206 - Ucieczka Sióstr Światła (HS, temp)](161206-ucieczka-siostr-swiatla.html)

## Kontekst ogólny sytuacji
## Punkt zerowy:

The sorcerer known as Kinglord has taken interest in this area. However, he has noticed that the power of Arazille is waking up here. What he is - is an antithesis of Arazille. She would destroy him if she noticed his coming.

Therefore, Kinglord has sent his controlled servant - Kara Łoszad. She was supposed to find a way to entice a local mage – and possibly local population – to destroy the hidden altar of Arazille.

Meanwhile, Lenartomin has a complex problem between the lawful people listening to a "local" priest - Henryk Siwiecki and between the 'femisatanist' forces controlled by Marzena Dorszaj. A lot of people are torn between what the church says and between their own daughters and wives.

In the meantime, the power of Arazille is growing. A first priest has been spawned. And some people have become the minions of the goddess.

On the completely opposite side, Marzena has sprung a trap in case Henryk ever comes here. She is also created a false booby-trap where her base is supposed to be located.

## Misja właściwa:

Dzień 1:

Henryk ma wielki dzień. Hubert mu uwierzył i mu ufa. Plan przejmowania Trójzębu poczeka ;-). Czas zająć się sukkubem. Poszedł więc do hotelu, spotkać się z Anną (paladynką). Henryk ma namiar - wie jak wygląda i jak smakuje magicznie Nicaretta, ale nie wie gdzie jest. Hubert Henrykowi szuka Nicaretty używając swojej siatki ludzi. A paladynka potrzebuje tylko potwierdzenia odnośnie aury magicznej...

Więc Anna dostała artefakt - coś, co pozwoli wykryć ślady magii. Hubert zapewnia pośrednio do Anny informacje odnośnie potencjalnego pojawiania się sukkuba, a Anna to weryfikuje.

Henryk może się byczyć. Dobry czas.

Dzień 2:

Karina przybyła do Wyjorza, gdzie znajduje się Trójząb. Ma informacje o tym gdzie jest mag i kto jest magiem - i o stopniu zaawansowania Arazille. Karina zrobiła mały spacerek po okolicy... rozeznanie taktyczne i w ogóle.

Po rozpoznaniu terenu Karina poszła do hotelu Jantar i wysłała flarę. Powitalną / przyjacielską. Dała jednak do zrozumienia, że wie, że on jest księdzem. "Czy znajdzie się jakiś kapłan skłonny pomóc biednej niewieście" czy coś.

Henryk ją odebrał. Jak zszedł do kawiarni hotelowej, Karina mu pomachała. Podszedł do niej.

* "Martwi mnie co się tutaj dzieje." - Karina
* "A konkretnie, dokładnie co? Dużo się dzieje. Niektóre z nich też mnie martwią" - Henryk
* "Naprawdę nic nie zauważyłeś?" - Karina poważnieje - "Słyszałam o tobie lepsze opinie"
* "Zależy z kim rozmawiam" - Henryk, ostrożnie

Henryk i Karina weszli w szermierkę słowną odnośnie tego kto pierwszy powie o Arazille - Henryk pierwszy się przyznał. Powiedział, że to co wiedział przekazał do Świecy. Karina zauważyła, że Świeca jest "nieco" zajęta - jest oblężona. Połowa Świecy nie działa. Nie ma czasu na czekanie na Świecę jeśli Arazille się budzi. Henryk wyczuł, że celem Kariny jest współpraca - odepchnięcie Arazille z tego terenu. Po to tu jest.

Henryk podzielił się z Kariną wiadomością, że niejaka 'Amelia' zwinęła srebrne lustro Arazille i wywiozła je gdzieś na Śląsk.

* "Ojcze, to mamy DUŻO większy problem niż mi się wydawało. Na tym terenie pojawiły się już pierwsze fabokle..." - zaniepokojona Karina.
* "Rozumiem, że masz jakiś zamysł, co możemy teraz zrobić?" - zaciekawiony Henryk
* "Ja? Ja jestem tylko bardką. Wiem tylko, że musimy COŚ z tym zrobić. Nie jestem wojownikiem" - Karina, wzruszając rękoma
* "Też nie jestem wojownikiem." - Henryk, ze smutnym uśmiechem
* "Więc musimy użyć mózgów." - Karina, chłodno

Henryk wpadł na pomysł wezwania jakiegoś kleryka. Karina ma wątpliwości czy czysto fizyczna siła coś zmieni. Nie trzeba na szczęście jeszcze zabijać jej wyznawców; trzeba jednak usunąć rzeczy w których się zakotwiczyła - najbardziej namacalnym śladem jaki mają jest sen o posągu w Lenartominie. Karina potwierdziła - warto tam pojechać.

Henryk zaprosił Karinę do samochodu i pojechali do Lenartomina. Miasto, w którym Henryk zrobił sobie dużo dobrych wspomnień. Szkoda byłoby to spieprzyć.

"Już tego księdza używałem... więc może mieć złe wspomnienia ze mną" - Henryk, z uśmiechem.
"No to się nie postarałeś" - Karina, z niezadowoleniem.

Poszli do księdza Klepiczka.

Ten z radością opowiedział Karinie wszystkie historie jakie zna z tego kościoła. Karina spróbowała go przekonać, by udostępnił jej księgi z tego kościoła. Zgodził się - niech informacja jest wolna, nie będzie hierarchia ograniczać wiedzy.

...Henryk facepalmował.

Historia kościoła. W tych księgach SĄ zapiski magów, niewidzialnych dla ludzi.

* Q: Co się stało z ostatnim magiem rezydentem?
* A: Zaćmienie. Wypaliło moc magiczną.
* Q: Czy ten kościół ma części tajne i ukryte dla ludzi?
* A: Tak. Ten teren ma historię bycia w jakiś sposób nawiedzanym przez bogów. Części tajne i ukryte mają uniemożliwić pojawieniu się takich istot.
* Q: Poprzedni rezydent?
* A: Nie chciał stracić magii; wyjął srebrne lusterko z Ukrytego Obszaru z nadzieją że się ochroni. Nie wyszło... a lustro stracono.

To oznacza, że być może wśród tych ukrytych rzeczy jest coś kontrującego Arazille. I to może być bardzo niebezpieczne... szczęśliwie, kościół działa jako Soczewka. Cały ten kościół jest jednym potężnym artefaktem, tak naprawdę.

Wiedząc to wszystko, Karina i Henryk skupili się na szukaniu czegoś, co pozwoli im zatrzymać manifestację Arazille. Coś podobnego z przeszłości. Dowiedzieli się:

* Jest potężny podziemny ley-line, Żonkibór - Wyjorze - Lenartomin
* Lenartomin jest obserwatorium i predyktorium - Strażnica
* Większość specjalistów była w tym terenie rozlokowana i dlatego Zaćmienie było tak niszczycielskie
* Arazille manifestowała się wcześniej. To zawsze jest ten sam posąg. Ale w innych miejscach.
    * Miejsce jest pochodne tego, gdzie osoba potrzebowała największego ukojenia
    * Ta osoba przyzywa posąg. Jest kernelem, katalizatorem. Nie dzieje jej się krzywda
    * Osoba przyzywająca posąg nadaje oblicze posągowi
* Zniszczenie posągu oddala problem
    * Ludzką ręką (mag po prostu zginie)
    * Człowiek nie może być kontrolowany (Arazille się inkarnuje)
* Zniszczony posąg to fajne surowce; całkiem bezpieczny

Henryk i Karina chcą dowiedzieć się o Klepiczka jak wyglądał ten posąg. O dziwo, ksiądz oponuje i się lekko rumieni. Henryk i Karina jak dwa totalne sępy siedzą i dociskają biednego księdza Klepiczka, by ten mimo wszystko powiedział. A on się wykręca że przecież to tylko posąg i wygląda jak posąg i nikogo mu nie przypomina... Karina zaczyna opisywać posąg i patrzeć na reakcje księdza (trafienia) a Henryk dociska po swojemu. Zidentyfikowali "ofiarę" molestując księdza. To... 

Urszula Kram. Femisatanistka, która dużo przeszła. I w której ksiądz się troszeczkę podkochuje. Ale jest czystego serca i do niczego nie doszło...

Czyli posąg, którego szukają jest w promieniu spaceru / biegu od domu Hipolita Mraczona. Tam więc poszli - Karina przed drzwiami stanęła i sprawdziła; w środku jest _coś magicznego_. 

Karina ma dość. Nie chce wchodzić w potencjalną pułapkę. Poszła do kościoła skupić się na kalibracji; chce użyć potęgi kościoła by móc określić jaki TYP energii się tam znajduje. Karina się skupiła i zajęło jej to do nocy, ale dostała odpowiedź - tam nie ma nic związanego z Arazille. Jest tam esencja demoniczna. Henryk rozpoznał sygnaturę. Marzena. To 'prezent' jaki zostawiła Marzena.

Karina powiedziała Henrykowi chłodno, że jej to nie interesuje. To jego decyzja i jego sprawa. Arazille jest większym problemem. A jeśli się narażą przez jakąś niską zemstę, przez błahy problem Arazille będzie na wolności. Na razie to nie łamie Maskarady a najpewniej jest pułapką na Henryka.

Dzień 3:

Zmienili podejście. Henryk wsiadł w samochód samiutkim ranem i pojechał odwiedzić Natalię Kaldwor. Chce go widzieć lub nie - nie ma znaczenia. Kamerdyner go nie wpuścił, ale problem został rozwiązany jednym telefonem do Huberta. Matka (Romana) nie chciała współpracować z Henrykiem, ale jeden czar rozwiązał problem. Wezwano Natalię.

Natalia przyszła w stroju... arlekina. 

"Mamo, mówiłam, że nie chcę by ktoś mi przeszka... serio? Ten zboczony ksiądz?" - Natalia, przewracając oczami

Henryk rzucił kolejne zaklęcie. Natalia była grzeczna i powiedziała absolutnie wszystko. Ula ukryła się w bunkrze. Natalia do końca nie wie gdzie; Ula wie. W jednym z nich się Ula schowała i była tam przez jakiś czas - dwie noce - bała się wyjść, aż znalazła ją Marzena. Nikt nie wie, jak Marzena ją znalazła. Czyli gdzieś w lesie są bunkry i Ula się w jednym z nich schowała.

Henryk się zmył z tego domu; jedzie do Lenartomina. 

Tymczasem Karina wie, że szuka bunkrów za miastem, gdzieś w lesie. Poszła do biblioteki - do gadatliwego bibliotekarza. Szczepana Szokmaniewicza.

On słysząc o bunkrach się ożywił - pokazał mapę z sześcioma. Proponował ich renowację; zwłaszcza dwóch szczególnie głupich (zrobionych przez magów). Powiedział o legendzie, że podobno przez studnię da się wejść do większej ilości bunkrów, ale nie da się - za mała i za wąska. Powiedział, że Lenartomin jest ogólnie przeklęty; to co Mraczon zrobił jego ulubionej Uli... on był na policji, był w ratuszu, pisał maile i petycje - nic się nie dało zrobić. Jest naprawdę rozgoryczony i bardzo wspiera femisatanistki.

Kamień rozbił szybę. Motocyklista odjechał. Wiadomość "Rycerze Jezusa patrzą". Odkąd bibliotekarz wspiera femisatanistki, takie rzeczy się dzieją...

No to Karina wie wszystko.

Henryk i Karina spotkali się w kościele. Karina ma mapę bunkrów. Karina pokazała na mapie ulubiony bunkier Uli, który "chyba zrobili idioci" (magowie) słowami Szczepana.

Dotarli po godzinie przyjemnego spaceru do bunkra. Bunkier jest bardzo trudno złożony; jest półzapadnięty i... dziwny. Trudno tam wleźć. Karina weszła płynnie i bez problemów; Henryk poleciał twarzą w błoto i się potłukł. I... jest tam ten posąg. Czują obecność bogini. I posąg wygląda jak Ula.

Karina znalazła kilku podrostków i dała im pieniądze; niech zniszczą ten posąg.

I w ten sposób, pod czujnym okiem Kariny i Henryka, posąg Arazille został zniszczony.

# Progresja



# Streszczenie

Kinglord wysłał Karinę do pomocy Henrykowi, by pozbyć się posągu Arazille. W czasie, gdy Anna szuka Nicaretty z pomocą Huberta, Henryk i Karina uzgodnili wspólnego wroga - Arazille - i wrócili do Lenartomina. Odkryli, że tamtejszy kościół to strażnica na leylinie i że takie rzeczy się zdarzały. Niestety, gdzie Arazille się manifestuje wie tylko Ula Kram - jedna z femisatanistek, która uciekła z Marzeną. Jednak Natalia Kaldwor pod kontrolą Henryka wyśpiewała wszystko co wiedziała (bunkry) a Karina w bibliotece dowiedziała się, gdzie jest ten posąg. Przekupienie kilku młodziaków by zniszczyli posąg było proste. Manifestacja Arazille nie nastąpi szybko. Biblioteka jest atakowana przez "anty-femisatanistów".

# Zasługi

* mag: Henryk Siwiecki, doprowadziła do zniszczenia posągu Arazille. Pomógł Karinie; wyciągał informacje z księdza oraz zaczarował rodzinę Natalii Kaldwor. Wyczuł, że coś z Kariną nie tak, ale... co?
* mag: Karina Łoszad, bardka wyciągająca informacje z dokumentów, księdza i bibliotekarza. Synchronizowała się z kościołem. Doprowadziła do zniszczenia posągu Arazille. Obserwuje Henryka.
* czł: Anna Osiaczek, paladynka, która z pomocą sił Huberta Kaldwora skupiła się na znalezieniu Nicaretty. W większości nieobecna na misji.
* mag: Marzena Dorszaj, która zastawiła na Henryka pułapkę w domu Hipolita Mraczona; ale nikt w nią nie chciał wejść :-(.
* vic: Arazille, inkarnująca się w posągu w Lenartominie wezwana kiedyś przez ciepienie Uli Kram. Nie skrzywdziła Marzeny Dorszaj.
* czł: Andrzej Klepiczek, ksiądz lekko podkochujący się w Uli. Z radością podaje Henrykowi i Karinie wszystkie informacje o kościele jakie są potrzebne.
* czł: Urszula Kram, opowieść o niej. Raczej chłopczyca, interesuje się militariami. Kocha bunkry. Do momentu spotkania z Hipolitem...
* czł: Natalia Kaldwor, dla odmiany w stroju arlekina; zaczarowana przez Henryka mówi wszystko co wie na temat Uli Kram.
* czł: Romana Kaldwor, matka Natalii. Chroni córkę; zachowuje się jak obrzydliwie bogata matrona. Which she actually is.
* czł: Szczepan Szokmaniewicz, bibliotekarz, straszny gaduła i plotkarz. Bardzo lubi Ulę Kram; wstawiał się za nią. Sympatyzuje z femisatanistkami. 


# Lokalizacje

1. Świat
    1. Primus
        1. Lubelskie 
            1. Powiat Wolny
                1. Lenartomin, który, jak się okazało, kiedyś był strażnicą ochronną - przed Zaćmieniem (jest na uskoku Ley-Line)
                    1. Kościół, gdzie znajdują się księgi magów opisujące strażnicę (Lenartomin) i który Karina umie kalibrować by soczewkować identyfikację magii
                    1. Posiadłość Mraczona, gdzie Marzena Dorszaj zastawiła pułapkę na Henryka Siwieckiego. W którą nikt nie wpada.
                    1. Rynek
                        1. Studnia życzeń, gdzie - jak głosi legenda - są wejścia do innych bunkrów, nieznanych ludziom
                    1. Biblioteka, z ogromnymi mapami militarnymi otoczenia. Atakowana przez tajemniczego motocyklistę kamieniem.
                1. Puszcza Nocy
                    1. Zachodnia Dolina
                        1. Bunkier Gołębiarza, do którego bardzo trudno wejść - i gdzie znajduje się Posąg Arazille. Znajdował się.
            1. Powiat Tonkij
                1. Żonkibór, który też jest na uskoku Ley-Line.
                1. Wyjorze, które też jest na uskoku Ley-Line.
                1. Łuczywo Nadobne, wieś dla bogatych ludzi
                    1. Ulica Reprezentacyjna
                        1. Dom Kaldworów, gdzie mieszka Natalia z rodzicami
   
# Skrypt



# Czas

* Dni: 3
---
layout: inwazja-konspekt
title:  "Niestabilna magitrownia"
campaign: wizja-dukata
players: kić, raynor
gm: żółw
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [171003 - Świnia na portalisku (KD, temp)](171003-swinia-na-portalisku.html)

### Chronologiczna

* [171003 - Świnia na portalisku (KD, temp)](171003-swinia-na-portalisku.html)

## Kontekst ogólny sytuacji

### Siły główne

**Magitrownia Histogram**

* Chce skazić WSZYSTKO i wylecieć w powietrze. Sukces: zniszczenia w okolicy. Min. portalisko, viciniusy i Eliksir Aerinus.
* Chce wymusić powiązanie z efemerydą. Wygra jeśli będzie trzeba powiązać się z efemerydą

**Robert Sądeczny**

* Chce zabezpieczyć wszystko w maksymalnie cichy i tani sposób. Wygra, jeśli nic nie wyjdzie na jaw i nic się nie stanie. I nie zapłaci.
* Chce zrzucić winę na rezydenta Świecy. Wygra, jeśli opinia publiczna wskaże, że to jego wina.

**Dracena Diakon**

* Chce uratować okolicę i wykorzystać swoje moce dla poprawy sytuacji. Wygra, jeśli jej obecność pomogła.
* Chce uwolnić elektrownię od tyranii mafii. Wygra, jeśli uzna, że jej obecność coś skorygowała.

### Stan aktualny

After Natalia and Kociebor have removed the component of the Harvester, the stuff which lied below the Harvester was not drained anymore. It started to contaminate the underwater current flow which flew into the artesian water. This water source was used by the magical plant. That is, power plants generating magic instead of energy. This is quite old; it was designed and built by the old binder bloodline Magi. Those same magi have built the Harvester.

The main problem here – the contaminant is of a dominant magical type listening little time all their artesian water which powers they generated magic will be changed into types not corresponding to the wavelengths of the destination magical tools.

In other words, the portal facility will blow up. The farm animals are going to be corrupted and they are going to change. And it is not possible to reintroduce the part of the Harvester the equation over there – it has been calibrated specifically to work with the contaminant the contaminant itself is a particular point in space; it is not connected to any type of a physical object which could be removed. To combat the problem, a Harvester was placed in this position. But one would have to dig deeper to get to the contaminant self it is not unearthed this point.

So we have the contaminant and the power plant will eventually get destroyed. Its detectors are rusty and they have failed to detect the changes in the artesian water. Dukat’s man – Robert - who is working in the portal port – was the first one to notice the problems with the energy source. Here’s the one who requested the owner of the power plant to do something. Fast.

Even under those circumstances it is impossible to stop the power plant once and for all. This particular power is too important. It powers and not of places here. There exists a secondary capacity system which is able to drain the energy from the ephemera located in the Senegrad. It is also possible to use some kind of secondary power source is located somewhere but forgot to function one would have to understand how the old machinery works. And the present power sources can corrupt quite a lot of things – especially as energy lines are not really shielded from the outer world.

A second force in the area is the resident high mage of the Silver candle – Sylvester Bankierz. He also has access to some tools – but if they are not compatible with the current power grid.

### Istotne wpływy przeszłe

* Żółw: 
    * Dracena: dostaje pracę w "Nowej Melodii" (3)
    * Robert: dostaje haracz od "Nowej Melodii" (3)
    * Maria: dowie się o złych rzeczach jakie robiły siły Dukata (3)
    * Sylwester: prowadzi udaną akcję zdobycia transportu Dukata (3)
    * To Hektor pomaga w budowaniu eliksirów dla Katii (3)
    * rezydent polityczny u Myszeczki (3)
    * Myszeczka staje po stronie rezydenta Świecy (3)
    * w okolicy Męczymordy jest odpowiednia ilość pola rezydualnego by sprawić by znikające świnie były prawdą (1)
    * pojawia się zwiększenie ruchów antymausowych na tym terenie - mniej portaliska dla Mausów (3)
    * ciężarówka ze świniami jest półdostępna w okolicach Męczymordy (1)
    * goście od świń założyli grupę polującą na znikające świnie (1)
* Prezes:
    * gościu od znikających świń nie może tego tak zostawić. JEGO MISJĄ JEST TO UDOWODNIĆ! : 1
    * gościu ma przydupasa którego misją jest to udowodnić! : 1
    * Sądeczny daje Kocieborowi własną agencję detektywistyczną za zasługi i ratunek Dobrocienia: 3
* Kić:
    * CLAIM "Trudna droga Sylwestra do zrozumienia terenu" (2)
    * fakt "Sylwester został zesłany tu przez Newerję" (1)
    * COUNTER Dukat: "Dukat nie ma gwałtownej reakcji na działania Sylwestra; da się wychować. Tupnął nogą bo musiał - and now we talk" (1)
    * CLAIM "Newerja Bankierz x Gabriel Dukat - zostaną parą" (2)
    * fakt "docelowo Ewelina dostanie jakieś środki do życia; nie będzie żebraczką" (1)
    * pielęgniarz od zwierząt (Henryk) z wiedzą o działaniach Myszeczki : 1
    * Harvester nie ma dość energii by się obudzić: 1
    * Natalia ma własnego tresowanego glukszwajna z apetytem na magię Harvestera: 1
    * Natalia pozyskała bezpiecznie kolejną część artefaktu: 1
    
### Istotne miejsca

* Mżawiór, gdzie znajduje się Portalisko Pustulskie
* Rogowiec, gdzie znajduje się hodowla Myszeczki
* Męczymordy, gdzie znajduje się magitrownia

## Punkt zerowy:

## Misja właściwa:

**Dzień 1:**

Do Daniela dzwoni Sądeczny. Mówi, że ma fluktuacje energii z elektrowni i portalisko potrzebuje korekty. Daniel patrzy na czujniki - wszystkie liczby są poprawne. Wysłał na dół swojego agenta który za to odpowiada, Nadziejaka. BZZT, Nadziejak padł nieprzytomny. Daniel zawołał o wsparcie - niech się młodym zajmą i zbiegł w dół zobaczyć co się dzieje. Sprawdził zaklęciem. Okazało się, że doszło do przebicia starych linii... i jest dziwna energia. Daniel dał radę załatać uszkodzenie i zauważył, że skażona jest studnia artezyjska. Fluktuacje pochodzą stąd, że jest dodatkowy TYP energii nie wykrywanej przez czujniki - to powoduje fluktuacje amplitudy. (-7)

Daniel rozpaczliwie przepiął na zapasowe źródła energii, wezwał o wsparcie do klientów i kontrahentów i przygotował wszystkie systemy. Zmodyfikował stare czujniki by wykrywały nowy typ energii...

Grzegorza przywieźli do Pauliny. Dracena ją obudziła po jeździe z Dobrocieniem. Paulina wykryła od Grzegorza echo aury Harvestera i spoważniała. Diagnoza, elementy czarowania i lapis naprawiły Grzegorza i zatrzymały zmiany nieodwracalne. Paulina wyczuła że to jakieś stare echa... Paulina pakuje podstawowy sprzęt, bierze lapisowanie tabletki dla Grześka i chce zostawić Dracenę. Maria weszła jej w szkodę i zaznaczyła, że zostawienie Draceny jest najgorszym pomysłem. Maria przyjechała a Paulina pojechała do elektrowni. (-4)

Paulina dotarła. Tam już narada między Zofią, Marcinem, Danielem. Paulina i Dracena weszły. Nie, nikt więcej nie ucierpiał. Tak, jest problem. Źródła artezyjskie są Skażone. Dracena zaproponowała, by kataliści zajęli się rzeczami dla katalistów a ona zajmie się z Pauliną rozwiązaniem problemu. Poszli do elektrowni wszyscy.

Tam Dracena zintegrowała się z elektrownią pod nadzorem Daniela i Pauliny. To było dla niej nieco za duże, ale było potężne... dała radę z integracją mimo Niekompatybilnej Energii, Problemu Komputrona, Obcym Strukturom i faktem że to wszystko jest Delikatne I Stare. Dała radę zlokalizować problem - Skażony jest jeden dopływ magiczny. Pokazała mniej więcej kierunek. To gdzieś w lesie. Po czym się rozłączyła i padła na pysk; Dracena nie jest w stanie sobie poradzić po takim połączeniu, jest oszołomiona. Zofia i Marcin zostali czyścić i stabilizować magitrownię. Dracena została odpocząć. A Paulina i Daniel poszli zobaczyć co z tym robić dalej.

Dracena powiedziała im, że to stało się jakieś 2-4 dni temu. Paulina poszukała stożkowo gdzie to jest. Znalazła miejsce. To jest w lesie. To to miejsce gdzie był glukszwajn. I są ślady glukszwajna... Tutaj Daniel przygotował zaklęcie i zaczął szukać co to jest. Zaklęcie ma efekty Paradoksalne. Paulina błyskawicznie użyła moc Miłoszepta i zatrzymała Paradoks, ale fala i tak poszła do elektrowni... było efektownie ale niewiele złych rzeczy się działo. Miłoszept opieprzył Paulinę za zostawienie chorych samych sobie i poszedł do jej domu, strasząc Marię.

A co Daniel znalazł? Jest tam jakiś konkretny PUNKT który jest amplifikatorem i skażaczem. Harvester go neutralizował.

Daniel zadzwonił do Sądecznego. Ten powiedział, że miał kolejną falę i prawie stracił 2 dni roboty. Daniel spytał odnośnie tego co było tu pod ziemią. Sądeczny powiedział, że to jest uszkodzone - było elementem Harvestera, ale się uszkodziło. Nie zadziała tak jak działało.

Paulina korzystając z okazji, że Histogram ma doskonałe czujniki i systemy detekcyjne poprosiła o logi z efemerydy. Wszystkie. Daniel dał dyspozycje odnośnie Pauliny - może dostać dostęp do czujników i że Karol ma poszukać innych takich rzeczy. Karol powiedział Danielowi, że Dracena chce się zatrudnić. Taaaak...

Paulina pojechała do elektrowni do czujników, Daniel do Myszeczki pogadać o świniach. Daniel przekonał Myszeczkę - nad punktem gdzie znajduje się ten dziwny output Skażenia zostanie Myszeczce oddane za darmo miejsce na pastwisko dla glukszwajnów. Sądeczny się wszystkim zajmie - ale za to Myszeczka ma pilnować, by glukszwajny wyżerały to cholerstwo konsekwentnie i skutecznie. Umowa stoi.

Dracena chce się zatrudnić, bo przecież elektrownia nie chce mafii. Paulina oszacowała dane z logów. Dużo ich i są dobre. Ale musi wejść w głąb efemerydy - wtedy dowie się tego czego szuka. Na szczęście ma wejście z magitrowni Histogram. Na nieszczęscie, to niebezpieczne.

**Dzień 2**

Dracena dostała pracę w elektrowni. Spytała prosto - przez łóżko czy za kompetencje? Udowodniła, że jest dobra już wcześniej. Dostała się za kompetencje ;-). Jako maintainer i dziewczyna od infrastruktury.

Paulina odwiedziła Daniela. Powiedziała mu, że potrzebuje dostępu do efemerydy przez czujniki - stało się coś bardzo złego. Niestabilna Efemeryda (9) - zneutralizowane przez elektrownię, Inercja (3), Wybuchowa (3), Wymaga Subtelności (3) - zneutralizowane przez uprzednią analizę logów przez Marię. Czyli próba skorzystania z czujników przez Paulinę. Daniel się zgodził. Paulina z pomocą Daniela i monitoringiem Draceny weszła do efemerydy i dowiedziała się, że sygnał SOS był wysłany przez Katię Grajek z Eliksiru Aerinus. Podczas wysyłania sygnału traciła swoją osobowość. Coś ją ulojalniało i wiązało ją niewolniczo. A osobą usuwającą potem ślady był... Robert Sądeczny. Dobrych parę miesięcy temu.

Niestety, w wyniku grzebania w efemerydzie doszło do emisji energii. Energia nie dotarła jednak do Zespołu. Nie wiedzą o tym, ale energia poszła w zaniknętą ciężarówkę ze świniami - przywracając ją z eteru na Primus.


## Wpływ na świat:

JAK:

* 1: dodanie faktu lub okoliczności wobec 'neutral' lub 'claim'
* 1: counter działania MG z sensowną propozycją alternatywną (wobec CLAIM)
* 2: postawienie CLAIM.
* 3: przesunięcie zgodne z motywacją
* 5: przesunięcie niezgodne/sprzeczne z motywacją
* 5: przesunięcie wobec postaci z CLAIM wbrew graczowi

MG: 13 kart

* Żółw: 
    * Robert spinuje akcję dezinformacyjną. Gdyby nie Świeca, nie byłoby kłopotu i elektrownia nie miałoby tragedii (3)
    * Fakt: Draceny wylano z "Nowej Melodii" za radykalizm (1)
    * Sylwester Bankierz sprowadza terminusów (3)
    * Fakt: Elektrownia z Draceną działa dużo lepiej i taniej (1)
    * Dracena dowiaduje się, że elektrownia współpracuje z mafią a nie jest wykorzystywana (3)
    * Dracena wykonuje działania mające uwolnić "Nową Melodię" od mafii (3)

Gracze (x2 za konflikt +1/5 kart MG):

* Kić: 7
    * CLAIM: Dracena docelowo będzie w stanie zintegrować się z JAKIMŚ społeczeństwem (2)
    * CLAIM: Paulina nie będzie musiała uciekać z terenu niezależnie od okoliczności (2)
    * CLAIM: Dukat lubi Paulinę (2)
    * Fakt: Jeden z terminusów wezwanych przez Bankierza to Kajetan Weiner. (1)
* Raynor: 5
    * Zarówno siły Dukata jak i Myszeczki zapewniają, by skażenie nie dotarło do elektrowni - to zbyt ważne. (3)
    * Robert uważa, że współpraca z Danielem oznacza, że Daniel jest dużo bardziej cenny niż myślał wcześniej (3)

# Progresja



# Streszczenie

Regenerowane portalisko dostaje Skażoną energię z magitrowni Histogram; Sądeczny zadzwonił do Daniela, by ten (właściciel) to naprawił. Okazało się, że artezyjskie źródła są Skażone, pojawił się dopływ Skażenia. Do akcji wkroczyła też Paulina; jeden z pracowników Daniela został Skażony tą energią i Paulina wyczuła energię Harvestera. Co się okazało - usunięcie niedawno elementu Harvestera przez Natalię i Kociebora odblokowało Skażony dopływ. Daniel wprowadził tam glukszwajny Myszeczki i załatwił, że ten ma tam pastwisko. Potem zatrudnił Dracenę i pomógł Paulinie dowiedzieć się, że ten sygnał SOS w efemerydzie to była Katia i maskował to Sądeczny.

# Zasługi

* mag: Daniel Akwitański, właściciel magitrowni Histogram balansujący między Dukatem a Bankierzem; opanował kryzys energetyczny i jeszcze przekuł go na swoją korzyść. Zatrudnił Dracenę.
* mag: Paulina Tarczyńska, zdobyła logi z efemerydy i poznała ponurą prawdę. Pomogła ogarnąć problem z elektrownią. Leczyła ofiary, wezwała Miłoszepta i kontrolowała Dracenę.
* mag: Jakub Dobrocień, wyleczony i jeszcze nieprzytomny w pracowni Pauliny. Czuwa nad nim Maria i Bogumił Miłoszept.
* czł: Grzegorz Nadziejak, odpowiedzialny za sensory w Magitrowni Histogram; niestety, przez wyciek Skażonej energii wpakował się i Skaził. Paulina postawiła go na nogi.
* mag: Dracena Diakon, wylana z "Nowej Melodii", znalazła pracę w Magitrowni Histogram jako osoba od infrastruktury. Zintegrowała się eksperymentalnie z magitrownią - z powodzeniem. Kontroluje swoje nowe moce.
* mag: Robert Sądeczny, nadal naprawia problemy na portalisku; ściągnął katalistkę do magitrowni i dba o okolicę. Skutecznie wrobił Warinskiego w to, że przez niego się pogorszyło. Paulina ma dowody, że zrobił krzywdę w przeszłości Katii Grajek - zniewolił ją i zamaskował ślady.
* mag: Marcin Warinsky, katalista Świecy; młody i nieco narwany katalista, który chce jak najwięcej dobra wnieść w imię Świecy i Bankierza. Skupia się na sprzęcie i dekontaminacji. Wrobiony jako winny Skażenia magitrowni przez Sądecznego.
* mag: Zofia Przylga, katalistka Dukata; bliska przyjaciółka Sądecznego. Skupia się na dekontaminacji energii magicznej. Antagonizuje Świecę na tym terenie - bo nic nie mogą zrobić.
* vic: Bogumił Miłoszept, znowu wezwany Paradoksem Pauliny. Wpierw pomógł jej osłabić falę uderzeniową w magitrownię z amplifikatora, potem pomógł Marii w pielęgnowaniu chorych: Nadziejaka i Dobrocienia.
* czł: Karol Marzyciel, drugi po Nadziejaku odnośnie sensorów w Histogramie; ma wątpliwości co do kandydatury Draceny, bo... jest za ładna. Udostępnił Paulinie co miał jej udostępnić.
* mag: Tomasz Myszeczka, który dostał nową działkę w lesie niedaleko magitrowni Histogram z obowiązkiem zrobienia tam pastwiska glukszwajnów. Twardy negocjator.
* vic: Efemeryda Senesgradzka, połączona z magitrownią Histogram przez bardzo zaawansowane czujniki Weinerów; bardzo dużo "wie". Uruchomiona i dokonała emisji po badaniach z magitrowni.

# Lokalizacje

1. Świat
    1. Primus
        1. Mazowsze
            1. Powiat Pustulski
                1. Mżawiór
                    1. Portalisko Pustulskie, naprawiane konsekwentnie przez Sądecznego i Przylgę; pierwszy punkt, w którym pojawił się sygnał o Skażeniu cieknącym z magitrowni Histogram.
                1. Męczymordy, wieś
                    1. Magitrownia Histogram, która została Skażona energią powiązaną z Harvesterem; stara i "złośliwa" magitrownia korzystająca z wód artezyjskich, zapewniająca energię okolicy. Najlepsze miejsce do bezpiecznego połączenia z efemerydą senesgradzką.
                    1. Las szeptów, gdzie znajduje się Punkt Skażenia. Nad nim oddano działkę Myszeczce, by tam miał glukszwajny do wypasania. Czyli od tej pory jest tam też małe pastwisko glukszwajnów...
                1. Senesgrad
                    1. Dworcowo
                        1. Kolejowa wieża ciśnień, gdzie w efemerydzie ukryta była tajemnica cierpienia Katii i jej ostatni, rozpaczliwy sygnał SOS. Efemeryda wyemitowała sygnał po linii czujników...

# Czas

* Opóźnienie: 1
* Dni: 2

# Narzędzia MG

## Opis celu misji

-

## Cel misji

-

## Po czym poznam sukces

-

## Wynik z perspektywy celu

-

## Wykorzystana mechanika

Postać (strona gracza):

| .Motywacja. | .Umiejętności. | .Magia. | .SiłySłabości. | .Znam.    | .Mam.      |.POSTAĆ. |
|   (-2) - 2  |     0 - 2      |  0 - 2  |    (-2) - 2    | Z+M = 0-6 | Z+M = 0-6  |         |

Opozycja (strona NPC):

* Baza: 2
* Aspekt: 3/aspekt, wariant z nożyczki/papier/kamień
* Modyfikatory: Skala, Sprzęt, Sytuacja (1-3 każdy)

Wynik:

| .Postać. | .Opozycja. | .Rzut.    | .Wynik.  |
|          |            | xx: +x Wx |   S-SS-F |

Wpływ:

* 10 kart / dzień
* każda porażka i skonfliktowany sukces = +2 pkt wpływu dla gracza
* każda karta = +1 wpływ dla MG
* każde 5 kart zaokr. w górę: +1 pkt wpływu dla gracza

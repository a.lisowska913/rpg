---
layout: inwazja-konspekt
title:  "Utracona kontrola"
campaign: wizja-dukata
players: kić, raynor
gm: żółw
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [171029 - W co gra Sądeczny? (PT)](171029-w-co-gra-sadeczny.html)

### Chronologiczna

* [171029 - W co gra Sądeczny? (PT)](171029-w-co-gra-sadeczny.html)

## Kontekst ogólny sytuacji

### Wątki konsumowane:

* CLAIM: Myszeczka jest ważnym partnerem biznesowym Daniela (Raynor)
* Kaja przygotuje coś naprawdę dużego (efektowną akcję) - i da radę nie być znalezioną (Żółw)
* Kaja doprowadzi do sytuacji, w której Aneta Rukolas potrzebuje pomocy medycznej. (Kić)
* Energia Eteru i Efemerydy Senesgradzkiej mutuje inne efemerydy w eterze. Zwłaszcza te świńskie. Mamy wylęgarnię. (Żółw)
* Paulina jest kanałami sprzężona z magitrownią; 3-4 dni Paulina musi odwiedzać magitrownię na 2-3h na regenerację. Może tam spać. (Żółw)
* Dlaczego krystality są niezbędne Oliwii do opanowywania efemerydy?
* BÓL: Myszeczką pomiatają.
* BÓL: Sądeczny traci kontrolę.
* BÓL: Dracena: konflikt i krzywda tak bez powodu

## Punkt zerowy:

-

## Potencjalne pytania historyczne:

* W jakim stopniu doszło do Skażenia Primusa efemerydami świńskimi?
    * DOMYŚLNIE: 0/10
* W jakim stopniu doszło do Lewiatana?
    * DOMYŚLNIE: 0/10
* W jakim stopniu doszło do splątania portaliska z Allitras?
    * DOMYŚLNIE: 0/10

## Misja właściwa:

**Dzień 1**:

Tamara zaczyna działania w kierunku postawienia własnej magitrowni i hydroponików. Mały kompleks centralny. To potrwa.
Danielowi się to bardzo nie spodobało - czas na mały dumping cenowy. Przekonał Sądecznego - warto uzależnić od siebie Świecę.
Potem pojechał do Bankierza i dał mu tanią energię. On zablokował Tamarę. Tamara wie co sie dzieje - i nic nie może zrobić.
Do Daniela przyszedł Tomasz Myszeczka i powiedział że ma problem - Sądeczny chce by on otworzył portalisko i wszedł do eteru tępić świnie. Kupił od Zofii Przylgi waveform energii - da się trochę uszkodzić portalisko, bo Myszeczka potrzebuje 2-3 tygodnie by viciniusy przygotować.
Daniel się zgodził - wraz z Myszeczką przygotowali takie zaklęcie emanacją by wyglądało, że to mała magitrownia Świecy spowodowała te problemy.

**Dzień 2**:

Sądeczny poprosił Daniela o otworzenie energii. Będzie uruchamiał portalisko. Daniel odkręcił.
Dracena krzyknęła z obszaru zasilania alarmując o źródłach energii - idzie za dużo. Przestraszona tym że idzie do portaliska - wysadziła transformator... ustabilizowała się energia.
Paulina usłyszała na hipernecie że jest poważny problem. Sądeczny krzyknął że wciągnęło grupę magów i ludzi... i wchodzi. Calling all doctors / catalysts.
Po chwili komunikat od Tamary. Ona też wchodzi. Żółty alarm, sytuacja delikatnie opanowana. Wchodzi. Calling all doctors / catalysts.
Paulina na miejscu. Przejęła dowodzenie - ale lokalne nie-spanikowane efemerydy się rozlazły. Paulina -> alarm do Draceny. Potrzebują energii z magitrowni albo kataliści umrą.
Dracena ostrzegła Daniela. Ten diagnozował co się stało - było coś z rurą. Puścił inną linią i puścił. Udało mu się, przekalibrował - ale doSkaził eter (+3 Allitras)
Zofia krzyknęła, że postawi pole puryfikacji - udało jej się, jest gdzieś gdzie da się czarować. Paradoks zmienił ją w świnię. Poza tym pojawiły się efemerydy świńskie++.
Sądeczny wynosi, współpracując z Tamarą, rannych. Paulina dała mu cukierki lapisowe ;-). Coś, co obniży.
Paulina ściągnęła wszystkich w stanie krytycznym do pola. I staza. Z pomocą lapisu, swoich zasobów itp - próbuje wszystkich w stazę. Udało się.
Nowy Paradoks Pauliny. Krok po kroku Legion przekształca się w mechaniczne świnie i staje się bezużyteczny...
Pojawiają się magowie z Aerinus, z innych miejsc, ze Świecy ściągnięci przez Sylwestra...
Przybył Daniel. Paulina próbuje delapisować wszystko, wyczyścić Zofię... z pomocą Daniela się udało.
Pandemonium zostało opanowane. Z portalu wyszli Sądeczny i Muszkiet, trzymając ostatnich 3 rannych.
Kataliści z trudem zamknęli portal.
Magowie rozlecieli się po terenie i polecieli tłuc efemerydy.
Paulina i Daniel pomogli jak mogli.
Tamara i Sądeczny rzucili po krzywym żarcie do siebie, zwłaszcza, że mundur Tamary wychrumkał i zostawił ją bez niczego.

**Dzień 3**:

W magitrowni. Dracena Daniel Paulina rozmawiają. Coś poszło nie tak jak powinno - nie powinno być takich energii.
Dracena i Daniel poszli na spacer. Znaleźć to miejsce gdzie doszło do uszkodzenia. Co tam się stało?
Wykryli to. Aktywny kolektor, sprzęgł się z rurą. Paulina wezwała Kajetana.
Dracena zaczyna się dobierać do Daniela, więc on BARDZO POWOLI odsłania kolektor. Magicznie. Ostrożnie.
Udało się (SS). Ale... ich ubrania zniknęły (Daniela i Draceny). Paradoks.
Wchodzi Kajetan i Paulina na to. Kajetan potrzebuje dnia na to, by kolektor rozpracować. Zauważył, że ktoś kolektor podłożył.
Przechlapane.
Daniel chce rozproszyć Paradoks, ale Dracena mu przeszkadza. Chce się posiłować magicznie. Daniel robi deal - ona mu pomoże zamiast przeszkodzić i on przejdzie przed nią nago po gabinecie. Dracena z szerokim uśmiechem się zgadza.
Daniel przygotowuje się do rzucenia. Wyszło - zdjął z nich zaklęcie. Skonfliktowanie: +Allitras (4).
Daniel naprawił transformator i linię.
Paulina wraca do pacjentów na portalisku. A tam Efraim Weiner ma już zająć się Anetą. Paulina wyszła z rekomendacjami lekarzy i że pomogła Sądecznemu. Wsparta przez innych magów, Efraim się wycofał. Paulina przejęła Anetę i wzięła ją do swojego gabinetu. Duży sukces, zwłaszcza, że Sądeczny nic nie wie i nie może interweniować.

## Wpływ na świat:

JAK:

1. Każdy gracz wybiera 1 element Historii - konflikt / okoliczność, która miała Znaczenie i ma zostać jako zmiana po akcji.
1. Każdy gracz wydaje swoje punkty na generowanie rzeczywistości i wątków.

CZYLI:

1. Kić: Sądeczny, który podrywa Tamarę Muszkiet. To po prostu ekstra.
2. Raynor: konflikt z Sylwestrem Bankierzem - oferował największą wartość, Daniel był w stanie przekonać go że jest wartościowym partnerem.

JAK:

* 2: MUSIK: gracz mówi w jaki sposób jedna z obcych postaci wspiera coś związanego z motywacją JEGO postaci
* 2: CLAIM na wątku / postaci / okoliczności
* 2: do elementu Historii lub przeszłego konfliktu dodajemy "ale" lub "oraz"
* 2: dodajemy pytanie, które musi zostać odpowiedziane na jakiejś z przyszłych misji
* 2: odpowiadamy na pytanie, które pojawiło się na jakiejś z misji
* 3: gracz mówi w jaki sposób jedna z obcych postaci wspiera coś związanego z motywacją JEGO postaci
* 3: zmieniamy kontekst okoliczności czegoś z Historii - scena z przeszłości / fakt?
* 3: do elementu spoza Historii dodajemy "ale" lub "oraz"
* 3: dodajemy znaczącego NPC powiązanego z elementem Historii
* 3: pozyskanie przez dowolną postać znaczącego surowca mającego sens z perspektywy misji
* 3: dodanie nowego elementu Historii
* 3: dodanie przyszłego lub przeszłego faktu; czegoś, co się wydarzyło lub wydarzy

Z sesji:

1. Żółw: (13)
    1. Doszło do Skażenia Primusa efemerydami świńskimi w ogromnym stopniu. Są WSZĘDZIE.
    1. Lewiatan się zbliża.
    1. Portalisko jest połączone z Allitras
    1. Zarówno Sądeczny jak Muszkiet są wyłączeni z akcji przez około tygodnia.
    1. Balans sił i energii po prostu się rozpadł na tym terenie - nikt nic nie kontroluje.
    1. Doszło do wielokrotnego złamania Maskarady na tym terenie.
    1. Bolesław Maus kontratakuje przeciwko Tomaszowi Myszeczce - nie może tak być.
1. Raynor (9)
    1. Tomasz Myszeczka - jego viciniusy stanowią źródło wczesnego ostrzegania na liniach magitrowni.
    1. Sytuacja z portaliskiem wzmacnia status quo na tym terenie - nikt nie jest w stanie uzyskać przewagi.
    1. Działania Daniela i Tomasza nie zostały wykryte przez ani Sądecznego ani Tamarę. Przez nikogo.
1. Kić (5)
    1. Bolesław Maus rozpoczyna kampanię "keep magic low here".
    1. Sądeczny znajduje się w niełasce po tym, co tu się ogólnie ciągle dzieje - ma to naprawić.

# Progresja

* Robert Sądeczny: ranny; przez mniej więcej tydzień nie może czarować ani działać aktywnie. W niełasce u Dukata.
* Tamara Muszkiet: ranna; przez mniej więcej tydzień nie może czarować ani działać aktywnie. Straciła większość Legionu.

# Streszczenie

Daniel zniwelował ryzyko wynikające z małej magitrowni Tamary (składającej mały kompleks centralny) i dał Świecy tanią energię za zgodą Sądecznego. Gdy przyszedł do niego Myszeczka bardzo zmartwiony tym, że musi czyścić po drugiej stronie portaliska efemerydy świńskie (a nie ma jeszcze czym), Daniel poszedł za jego prośbą i puścił energię innego koloru. Chwilę potem portalisko eksplodowało magicznie, Sądeczny i Tamara ratowali magów od strony eteru a Paulina przejęła dowodzenie medyczne. Przechwyciła Anetę Rukolas. Winny? Kolektor harvestera (podłożony przez Kaję) zarezonował z energią Myszeczki. Mamy sprzężone portalisko z Allitras i mamy świńskie efemerydy ogromnej mocy i złamanie Maskarady na kilku poziomach... a Świeca i Mafia utraciły kontrolę nad terenem i sytuacją.

# Zasługi

* mag: Paulina Tarczyńska, przechwyciła Anetę Rukolas i ratowała ludzi i magów na lewo i prawo po katastrofie na portalisku. Przejęła kontrolę medyczną nad sytuacją.
* mag: Daniel Akwitański, zneutralizował mikromagitrownię Świecy, chciał pomóc Myszeczce i pośrednio wysadził portalisko. Ratował sytuację, naprawił uszkodzone elementy magitrowni i uniknął poderwania przez Dracenę... acz miał dość... ujawniający Paradoks.
* mag: Sylwester Bankierz, ucieszył się ze współpracy z Danielem (taniej energii) w ramach nowego kompleksu centralnego. Zablokował Tamarze budowanie własnej awaryjnej magitrowni.
* mag: Tomasz Myszeczka, chciał zapobiec utracie grupy viciniusów, poprosił Daniela o inną energię do portaliska i poważnie uszkodził portalisko które wciągnęło go w eter. Uratowany przez Sądecznego.
* mag: Dracena Diakon, dzięki jej szybkiej reakcji portalisko nie wybuchło - wysadziła transformator. Potem dobierała się do Daniela. Osiągnęła większy sukces niż to na co liczyła.
* mag: Robert Sądeczny, z narażeniem życia wszedł w Eter ratować wciągniętych magów i ludzi. Gdy Tamara mu pomogła, trochę ją podrywał - zwłaszcza, jak jej mundur odchrumkał.
* mag: Tamara Muszkiet, z narażeniem życia weszła w Eter ratować wciągniętych magów i ludzi i zespół ratujący. Po Paradoksie (cudzym) straciła ubranie i cały Legion. Podrywana przez Sądecznego.
* mag: Zofia Przylga, wpierw pomogła Myszeczce znaleźć harmoniczne sygnały które uszkodzą lekko portalisko a potem z narażeniem życia ratowała ludzi na portalisku. Paradoks zmienił ją w świnię.
* mag: Efraim Weiner, lekarz dowodzący operacją z ramienia Sądecznego. Miał szczególnie zająć się Anetą Rukolas, ale Paulina mu ją odebrała.

# Lokalizacje

1. Świat
    1. Primus
        1. Mazowsze
            1. Powiat Pustulski
                1. Męczymordy
                    1. Magitrownia Histogram, gdzie Dracena wysadziła transformator po tym jak poszedł nadmiar niewłaściwej energii do portaliska by ratować wszystkich i wszystko
                    1. Las Szeptów, gdzie doszło do wprowadzenia kolektora Harvestera koło linii przesyłowej energii magitrowni
                1. Byczokrowie
                    1. Niezniszczalna Kazamata, główna baza dowodzenia Sylwestra Bankierza i miejsce jego spotkania z Danielem Akwitańskim
                1. Mżawiór
                    1. Portalisko Pustulskie, od teraz silnie sprzężone z Allitras i generator świńskich efemeryd o naprawdę dużym stężeniu energii

# Czas

* Opóźnienie: 1
* Dni: 3

# Wątki kampanii

1. Stan aktualny Pauliny Tarczyńskiej
    1. CLAIM: Paulina zostanie rezydentką Świecy na tym terenie a Tamara jej prawą ręką - terminusem bojowym.
    2. CLAIM: Paulina nie będzie musiała uciekać z terenu niezależnie od okoliczności (Kić)
1. Stan aktualny Krzysztofa Grumrzyka
    1. Po tym jak Krzysztof pomógł Filipowi, współpraca się zacieśniła - lepsza kalibracja Firmy i Firmy.
1. Stan aktualny Kociebora Dyrygenta
    1. CLAIM: okazja na poznanie i zaprzyjaźnienie się z Kajetanem.
    2. Sądeczny daje Kocieborowi własną agencję detektywistyczną za zasługi i ratunek Dobrocienia
    3. Oliwia zapewnia Kocieborowi drony, które pozwolą na obserwowanie i śledzenie. Nowy biznes Oliwii.
    4. Customizowany awian, nie taki jak Czarny Ptak. Taki czołg.
    5. Maja próbuje w najbardziej nieoczekiwanych momentach (i nieodpowiednich) okazać Kocieborowi sympatię i pomoc.
    6. Prawdziwym bohaterem Mai stał się Kociebor, który potraktował ją łagodnie i z sympatią.
1. Stan aktualny Daniela Akwitańskiego
    1. CLAIM: Myszeczka jest ważnym partnerem biznesowym Daniela (Raynor)
    2. Sądeczny w swoim investigation odkrywa, że Tamara zadziałała wbrew Danielowi i on nie chciał stawać przeciw niemu
    3. Robert uważa, że współpraca z Danielem oznacza, że Daniel jest dużo bardziej cenny niż myślał wcześniej
1. Stan Mazowsza i okolicy
    1. Filip aktywnie wspiera Paulinę w redukcji pola magicznego i Skażenia na tym terenie.
    2. W jaki sposób można odepchnąć w eterze nadmiar energii, by pozbyć się kłopotu bez skrzydła katalistów, terminusów i logistyków?
    3. Te pieprzone świnie się rozmnożyły. Pasożytnicze świnie z eteru. (Prezes)
1. Efemeryda Senesgradzka i jej stan
    1. CLAIM: Grzybb (w Efemerydzie zamiast Farnolisa) jest do uratowania (Kić)
    2. CLAIM: nigdy więcej efemeryda nie skrzywdzi moich awianów (Foczek)
    3. CLAIM: będę w stanie wykorzystać tą efemerydę jako niesamowite źródło energii (Foczek)
    4. Efemeryda Senesgradzka występuje na wielu Fazach jednocześnie i ma dopływy z różnych Faz (Żółw)
1. Farma krystalitów Mai Weiner
    1. Dlaczego krystality są niezbędne Oliwii do opanowywania efemerydy?
    2. Byt który opętał elementala/awiana Rolfa jest emocjonalnie związany z dzieckiem.
    3. Farma Krystalitów z młodą Mają która decyduje się przejąć dowodzenie na farmie.
1. Awiany z lokalnej montowni
    1. Awiany nowego typu są lepsze, fajniejsze ale i niebezpieczniejsze
    2. Spora część awianów montowanych w montowni jest dostosowana do linii produkcyjnej Filipa.
    3. Sporo awianów a wszystkie nowej generacji są zbudowane technikami sprzężenia Allitras-elementalna.
    4. Dlaczego "elemental burzy i gniewu" jako najlepsze miejsce znalazł ten klub (Panienka w Koralach)? Co było z tym miejscem lub ludźmi?
    5. Ściągane i pętane elementale są niewolone ALE nigdy nie są agresywne.
    6. Jakiego typu viciniusem stanie się awian Filipa?
    7. Awiany są strategicznie potrzebne zarówno mafii jak i Świecy
1. Echo starych technologii Weinerów
    1. Harvester nie ma dość energii by się obudzić
    2. Natalia pozyskała bezpiecznie kolejną część artefaktu
1. Wieża Wichrów
    1. Wieża Wichrów jest sprzężona portalem z nieznaną (chwilowo) fazą Allitras. Stałe powiązanie, póki są tam burze.
    2. Robert Sądeczny zażądał od Filipa zaprzestania tych konkretnych praktyk zagrażających terenowi.
    3. Bez tych praktyk Wieża Wichrów w obecnej formie nie ma sensu; lepiej zrobić nieco inne miejsce.
    4. Wieża Wichrów zostaje w rękach Filipa. Sądeczny nie ma jak się do niej przyczepić.
    5. Filipowi uda się utrzymać firmę JAKOŚ.
    6. Portal w Wieży Wichrów sam z siebie się nie rozproszy - jest tam i będzie spamował dziwne byty.
1. Byty z z Allitras
    1. Przepakowane elementale. Więcej zabawy, więcej energii. (Foczek)
    2. Byty wchodzące w elementale są świadome. (odpowiedź graczy)
    3. Aktualny elemental nie zdołał otworzyć portalu, ALE wysłał sygnał w eter.
1. Oczy Świecy, Oczy Sądecznego
    1. Sądeczny odzyskuje część "oczu" przez wprowadzenie linii produkcyjnej dron-awianów do fabryki Oliwii i Tamara wie o tym.
    2. Sądeczny wprowadził rezydenta u Myszeczki
    3. Tamara doprowadziła do sytuacji, gdzie Świeca nie jest na ciągłym podsłuchu
    4. Tamara nie jest w stanie rozwiązać wcześniejszej dezinformacji
    5. Oktawia ma wiedzę z Legionu Tamary z czasów misji przed opętaniem magitrowni.
    6. Sądeczny nasyła Zofię na szpiegowanie Tamary
1. Reputacja Świecy, Reputacja Sądecznego
    1. Sądeczny spinował akcję dezinformacyjną, masakrując reputację Świecy.
    2. Tamara skupia uwagę na wspieraniu przede wszystkim Tomasza Myszeczki. Duży, rozległy, potrzebuje pomocy. Brak mu wszystkiego.
    3. Sylwester opłaca nowego przedsiębiorcę mającego założyć konkurencyjną małą magitrownię. By uniezależnić się od gróźb. Aha, czyn nieuczciwej konkurencji.
1. Robert Sądeczny, zapamiętany przez historię
    1. Jak wbić klin między Fornalisa a Sądecznego? (Kić)
    2. Aneta wróciła z Sądecznym z Muzeum Pary ORAZ wie, co tam się stało
    3. Hektor pomagał w budowaniu eliksiru który skrzywdził Katię
1. Lord Rezydent Sylwester Bankierz
    1. Sylwester został zesłany tu przez działania Newerji
    2. Sylwester przeprowadził udaną akcję zdobycia transportu Dukata
1. Tomasz Myszeczka - król viciniusów
    1. Sądeczny dostarczył maga, który zajmie się glukszwajnem czyszczącym czujniki ze Skażenia
    2. Tamara robi się bardzo podejrzliwa wobec Myszeczki
    3. Myszeczka staje po stronie Rezydenta Świecy
1. Dracena Diakon, w jakimś społeczeństwie
    1. CLAIM: Szybki, dyskretny awian. Trafi do Draceny. (Żółw)
    2. Kto i w jaki sposób pomoże Dracenie stać się częścią tego terenu dookoła? Należeć do czegoś?
    3. Dracena wykonuje działania mające uwolnić "Nową Melodię" od mafii 
1. Magitrownia Histogram, echo technologii Weinerów
    1. CLAIM: Dracena docelowo będzie w stanie zintegrować się z JAKIMŚ społeczeństwem (Kić)
    2. Magitrownia z Draceną działa dużo lepiej i taniej
    3. Dracena została przekonana, że magitrownia współpracuje z mafią a nie jest wykorzystywana
    4. Zarówno siły Dukata jak i Myszeczki zapewniają, by Skażenie nie dotarło do magitrowni.
1. Gabriel Dukat - uleczenie jego syna
    1. CLAIM: Dukat lubi Paulinę (Kić)
    2. W jaki sposób byty z Allitras mogą pomóc dziecku Dukata?
    3. Aktywny Harvester będzie w stanie pomóc dziecku Dukata - stałym kosztem ofiar z ludzi (Żółw)
    4. Dukat nie ma gwałtownej reakcji na działania Sylwestra; da się wychować. Tupnął nogą bo musiał - and now we talk" (Kić)
    5. Kluczowe elementy soullinkowania dziecka Dukata pochodzą z wiedzy pozyskanej przez Sądecznego w ramach działania na tym terenie
1. Portalisko Pustulskie i odrodzenie Mausów
    1. Pojawia się zwiększenie ruchów antymausowych na tym terenie - mniej portaliska dla Mausów.
1. Świńskie viciniusy i coś poszło nie tak
    1. Rolnik od świń założył ma misję - i przydupasa - mające udowodnić, że ze świniami dzieje się coś dziwnego
    2. W okolicy Męczymordy jest odpowiednia ilość pola rezydualnego by sprawić by znikające świnie były prawdą
    3. Energia Eteru i Efemerydy Senesgradzkiej mutuje inne efemerydy w eterze. Zwłaszcza te świńskie. Mamy wylęgarnię. (Żółw)
1. Oktawia, efemeryczne echo Oliwii
    1. CLAIM: Oktawia dorośnie i zniknie. (Kić)
    2. CLAIM: Dracena i Oktawia będą miały duet. (Żółw)
    3. Oktawia chce pomóc Krzysztofowi, bo widzi w nim potencjał do pomocy Oliwii
    4. Marzyciel może nadal komunikować się z Oktawią; może jej śpiewać, gdy ona jest w Efemerydzie. (Żółw)
    5. Chaotyczność i nieufność / niestabilność Oktawii jedynie wzrosła. Ciągle odrzucana, discardowana, torturowana. (Żółw)
1. Wolność Eweliny Bankierz
    1. CLAIM "Newerja Bankierz x Gabriel Dukat - zostaną parą" (Kić)
    2. Docelowo Ewelina dostanie jakieś środki do życia; nie będzie żebraczką


# Narzędzia MG

## Opis celu misji

-

## Cel misji

-

## Po czym poznam sukces

-

## Wynik z perspektywy celu

-

## Wykorzystana mechanika

Postać (strona gracza):

| .Motywacja. | .Umiejętności. | .Magia. | .SiłySłabości. | .Znam.    | .Mam.      |.POSTAĆ. |
|   (-2) - 2  |     0 - 2      |  0 - 2  |    (-2) - 2    | Z+M = 0-6 | Z+M = 0-6  |         |

Opozycja (strona NPC):

* Baza: 2
* Aspekt: 3/aspekt, wariant z nożyczki/papier/kamień
* Modyfikatory: Skala, Sprzęt, Sytuacja (1-3 każdy)

Wynik:

| .Postać. | .Opozycja. | .Rzut.    | .Wynik.  |
|          |            | xx: +x Wx |   S-SS-F |

Wpływ:

* 8 kart / dzień
* każda porażka i skonfliktowany sukces = +2 pkt wpływu dla gracza
* każda karta = +1 wpływ dla MG
* każde 5 kart zaokr. w górę: +1 pkt wpływu dla gracza

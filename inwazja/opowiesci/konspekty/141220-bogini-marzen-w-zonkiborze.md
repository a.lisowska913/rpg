---
layout: inwazja-konspekt
title:  "Bogini Marzeń w Żonkiborze"
campaign: czarodziejka-luster
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Punkt zerowy:

## Kontynuacja

### Kampanijna

* [141218 - Portret Boga (An)](141218-portret-boga.html)

### Chronologiczna

* [141218 - Portret Boga (An)](141218-portret-boga.html)

## Misja właściwa:

Andromeda, Samira i Sandra dojechały do Żonkiboru, do miejsca podanego im przez Małgorzatę Poran. Po drodze, po usłyszeniu rewelacji od Sandry Andromeda użyła mocy amuletu Podniesionej Dłoni i wezwała do pomocy jednego ze "swoich" terminusów, Herberta Zioło. Herbert ma siedzieć w odwodach i niczego nie robić; ma obserwować i nie być widzianym.
Na miejscu czekała na Andromedę niejaka Ewa Czuk, która przywitała ją z radością. Ucieszyła się, że Andromeda się pojawiła mimo ostatnich tragedii. Samira podchwyciła - "tragedii" implikuje liczbę mnogą. Ależ tak - przed śmiercią Andrzeja Domowierzca zginął w wypadku samochodowym mąż Małgorzaty - Hektor Poran. Sandra nie wytrzymała - to druga śmierć. Kiedy? Pięć dni temu. I wyraźnie nikt nie jest tym szczególnie poruszony; ani śmiercią Domowierzca ani Hektora Porana. 
Nagle Andromeda się bardzo ucieszyła, że ściągnęła tu terminusa...

Zdaniem Ewy wszystko jest i będzie w porządku, bo na miejscu jest już policja. Zapytana czy mają jakichś wrogów odpowiedziała, że nie... chyba, że lokalny ksiądz - Józef Pimczak. Ale jak stwierdziła i on ujrzy Prawdę. Powiedziała to nie jako groźbę, ale absolutnie przekonana o tym, że ma rację. Andromedzie zrobiło się zimno.
Tą przemiłą rozmowę przerwało wyjście z domku Andromedy młodego chłopaka który został zidentyfikowany jako Michał Czuk, syn Ewy. Powiedział mamie, że posprzątał. Gdy ta zaproponowała mu by poszedł na festiwal się bawić, odparł, że nie ma mowy - jest dużo pracy a on próżniakami gardzi.
Kolejne uniesienie brwi Sandry. 
Andromeda pożałowała, że nie ma tu Augusta.

Dobrze. Jako, że Andromeda nie ma pojęcia kiedy co się stanie i kiedy odwiedzi ją Małgorzata Poran, zdecydowała się zacząć działania. Poprosiła Sandrę, by ta przeszła się po mieście i poszukała astralnie co mniej więcej się tu dzieje, czy astralnie wszystko jest w porządku. Sandra się zgodziła i zabrała się do pracy (11/k20). Andromeda zdecydowała się przejść po barach, po różnych miejscach, po mieście, po bibliotece, pogadać z bibliotekarką, z barmanem... podowiadywać się w sposób dość casualowy. Samira zaproponowała, że pójdzie do księdza. Jak Andromeda stwierdziła, że w sumie ona jest zainteresowana księdzem, Samira powiedziała że ona może pójść pogadać z chłopakiem (Michałem Czukiem)(17/k20) a Andromeda może w tym czasie porozmawiać z księdzem. A Andromeda poszła do księdza.

Idąc do księdza Andromeda upewniła się, że nikt jej nie śledzi. Udała się do księdza, do parafii.
Ku swojemu dużemu zaskoczeniu ksiądz zachowuje się jak cesarz. Siedzi na "tronie" w bogatych szatach i udzielił Andromedzie audiencji. Powiedział o fałszywej bogini w miasteczku i że on oraz armia światła zniszczy tłum heretyków. Spytał, czy Andromeda ma zamiar dołączyć do armii światła, czy przyszła prosząc o wybaczenie (którego Bóg miłosierny jej udzieli) czy też przyszła w innej sprawie. Gdy Andromeda powiedziała jąkając się (z nerwów) że przyszła bo pracodawca kazał wpłacić datek na Kościół, ksiądz powiedział, że należy oddać cesarzowi co cesarskie. Niech nie daje pieniędzy, niech modli się za Kościół. Andromeda wyszła w szoku. Coś tu jest nie tak.

Spróbowała szczęścia jeszcze raz. Weszła do samego kościoła. A tam - organista grający jeden i ten sam utwór cały czas oraz jedna kobieta (Aneta Hanson) modląca się. Ku zdziwieniu Andromedy, kobieta wstała i skierowała się do wyjścia. Andromeda z nią porozmawiała. Kobieta kazała jej uciekać. Powiedziała, że fałszywa bogini opanowuje miasteczko i nic jej nie zatrzyma - nic poza nią. Bo ją prowadzi Bóg. Bo ona jedna jedyna jest odporna. Andromeda zrozumiała. Ona też jest szalona. 
Pożegnała się z kobietą i poszła do domu. Zadzwoniła po drodze do Samiry i Sandry i poprosiła o NATYCHMIASTOWE spotkanie. Trzeba opracować plan ataku lub ucieczki. Cokolwiek się tu dzieje jest naprawdę dziwne.

Synchronizacja. Wpierw Samira - powiedziała, że Michał Czuk jest młodym i praktycznym człowiekiem który reaguje sprzecznie do tendencji dookoła. Ludzie zaczynają skupiać się na swoich marzeniach, na przeżyciach wewnętrznych zamiast pracować. Dotknęło to też dziewczynę w której podkochiwał się Michał i stąd tak silna reakcja chłopaka. Zwłaszcza, że jego matka także ulega trochę tej fascynacji marzeniami. Samira powiedziała też, że festiwal trwa już miesiąc a ludzie nie płacą pieniędzy. Nikt nie oczekuje pieniędzy. Koszta muszą być horrendalne.
Andromeda powiedziała co poradziła jej kobieta z kościoła. Żeby uciekali, bo fałszywa bogini przejmuje władzę. Samira ostro zaprotestowała - są tu jej oraz Andromedy znajomi plus dodatkowo nie jest to coś czego Samira się boi. Fałszywych bogiń nie ma. Zabójca jest prawdziwy.

Samira poszła rozpakowywać rzeczy dając czas Sandrze na rozmowę z Andromedą. Sandra powiedziała Andromedzie, że jest tu dziwna aura. Aura powodująca wzmocnienie siły marzeń. Ludzie i magowie mogą w pewien sposób zacząć żyć swoimi marzeniami. One nigdy nie staną się prawdą (nie pryzmat), ale mogą dla danej osoby być rzeczywistością. Andromedzie od razu skojarzył się ksiądz. Sandra zauważyła, że do czegoś takiego potrzebne jest ogromne źródło energii. Plus, Sandra wykryła ślady magów.
Rozmowę przerwało im pukanie. Andromeda otworzyła - policja. Dwójka policjantów, Anna Makont i Wojciech Kajak. Anna zapytała czy mogą odpowiedzieć na kilka pytań a Andromeda zauważyła, że są tu dopiero od kilku godzin. Anna podziękowała za poświęcony im czas, lecz Wojciech zapytał Andromedę o to, czy widziała coś w czym można przetransportować ogromną ilość krwi. Anna się wyraźnie wściekła. Zabrała Wojciecha na bok, pożegnała się i go opieprzyła myśląc że Andromeda nie słyszy - "nie jesteśmy w jakimś CSI".

Używając mocy amuletu Andromeda skontaktowała się z Herbertem Zioło (swoim terminusem). Zapytała go o możliwości i radę, mówiąc mu wszystko czego się dowiedziała. Zioło przeanalizował sprawę i powiedział, że ważną rzeczą jest by Sandra dowiedziała się (a jego zdaniem powinna być w stanie) czy to jest energia wpływająca na ludzi przychodząca z zewnątrz i napełniająca ich energią (w którym to wypadku gdzieś musi być potężne epicentrum) czy też jest to energia która służy do otwarcia kanału drenażu (w którym wypadku gdzieś musi być coś, co pobiera tą energię). W obu wypadkach Zioło uważa, że mają do czynienia z czymś dużym. Zapytany przez Andromedę czy może wejść oficjalnie jako terminus powiedział, że nie - ten teren jest poza jurysdykcją SŚ. Oraz gildii śląskich. Powiedział, że spróbuje dowiedzieć się kto tu operuje i w jakich okolicznościach mógłby zadziałać.

Andromeda powiedziała Sandrze, że musi dowiedzieć się jaki typ oddziaływania tu ma miejsce (bazowane na tym co powiedział jej terminus). Sandra powiedziała, że potrzebny jej jakiś człowiek na bazie którego może sprawdzić czy jego astral jest wzmocniony czy osłabiony; im silniej dotknęło go oddziaływanie tym lepiej. Andromeda powiedziała, że ma dwa typy - ksiądz lub kobieta z kościoła. Ale nie zna imienia kobiety a księdza się boi. Po namyśle, stwierdziły, że poczekają na Małgorzatę Poran a potem pójdą na festiwal.

I Małgorzata przyszła. Zmartwiła się, że Kasia nie była na festiwalu, bo "nie wie jaka jest bogini". Zapytana gdzie Kasia ma iść by zrozumieć naturę bogini, Małgorzata odparła, że najlepszym miejscem jest festiwal. Podpytywana ciągle przez Andromedę, Małgorzata powiedziała, że Pani Marzeń mieszka na festiwalu, że ma konkretne miejsce, gdzie się znajduje - ale Małgorzata nie może tam jeszcze Kasi zabrać, bo zrobiłaby jej krzywdę. Nie jest jeszcze gotowa. Słysząc to Sandra odeszła na bok i zaczęła rzucać czar na Małgorzatę ku wielkiemu zdziwieniu Andromedy. No ok, Kasia kontynuowała odwracanie uwagi Małgorzaty. Spytała jak wygląda bogini, jakieś podania, legendy, na co Małgorzata odpowiedziała pytaniem - o czym Kasia marzy. Dziwna odpowiedź. Małgorzata wyjaśniła, że bogini objawia się w sposób właściwy dla marzącej osoby. Jak Kasia zapytała o czym marzy Małgorzata, ta powiedziała, że zawsze chciała zarządzać czymś ważnym. I niech Andromeda spojrzy - teraz Małgorzata zarządza firmą PapiBór która uratowała Żonkibór i do tego organizuje najważniejszy Festiwal w Polsce, jeśli nie na świecie.
Gdy Andromeda spytała o finansowanie i sponsorów, Małgorzata się uśmiechnęła. Sponsorzy po spotkaniu z boginią zmieniali zawsze zdanie i zostawali hojni. Bo bogini dała im to, czego pragnęli - spełniała ich marzenia. Małgorzata dała jako przykład swoją firmę papierniczą. Ludzie w firmie zarabiają niewiele, ot tyle, by wystarczyło na podstawowe potrzeby im i ich rodzinom. Ale są szczęśliwi i tylko to się liczy. Andromedzie zrobiło się zimno.
Małgorzata dała Andromedzie swój adres i, zapytana, adresy innych malarzy malujących portret Pani Marzeń.
Po czym się pożegnała i poszła.

Sandra powiedziała Andromedzie co zaobserwowała. Andromeda niewiele zrozumiała, więc amuletem otworzyła kontakt z Herbertem. Zdaniem Sandry (a pytania zadawał Herbert, więc Andromeda dostała #5 szacunu na dzielni) mają do czynienia z bardzo potężną efemerydą lub viciniusem lub węzłem sterowanym przez maga. Byt ten (dalej zwany "boginią") charakteryzuje się tym, że potrzebuje ogromnej ilości energii. Korzysta z zasady paradygmatu - im więcej osób w boginię wierzy, tym niższy koszt utrzymania. Kluczem są marzenia - bogini żywi się wszystkim OPRÓCZ marzeń. Pozostawia jedynie marzenia i uczucie spełnienia, szczęścia. Mają do czynienia z bardzo wysublimowanym pasożytem (niektórzy powiedzieliby symbiontem).
Super.

Andromeda zadała lekki środek nasenny Sandrze i Samirze wieczorem a gdy obie zasnęły, wzięła się do pracy. Wzięła Samirę za rękę, połączyła ręce na tafli lustra. "Efemeryda może pomyśleć, że zabrała co chciała, ale Samira ma być Samirą". Andromeda chce, by odbicie Samiry zostało w lustrze. Jeśli Samira zniknie, po spojrzeniu w lustro wróci do stanu z odbicia.
Dla siebie przygotowała przy użyciu lustra fałszywe marzenie.
Gdy Andromeda skończyła zabezpieczać siebie i Samirę, poszła do pokoju Sandry. Chciała zapewnić Sandrze jakąś ochronę przed boginią. Końcowa intencja: Sandra dostanie coś na kształt lustra weneckiego: nawet jeśli zostanie przejęta / wpłynięta w jakikolwiek sposób, ma efekt dwoistości. Ma wybór. Efekt, jaki chce osiągnąć Andromeda - żeby potęga bogini nie dotarła do prawdziwej natury Sandry tylko trafiła lustro, by Sandra była jak lustro weneckie, patrząc ze środka. 
Do tego Andromeda użyła lustra weneckiego jako podstawy, krwi Sandry (dla ułatwienia działania i "pokazania" lustru że o nią chodzi), zmieszana krew Andromedy i Samiry (pokierowanie lustrem zgodnie z intencją Andromedy i uczynienie lustra doskonalszym). (19/k20->20; ten jeden test szczęścia zapiszę). COŚ się stało.

Lustro na którym miał miejsce rytuał się rozjarzyło. Andromeda zobaczyła tam Sandrę, która przyłapała swoją mamę na rytuałach magii krwi i to tych bardzo zakazanych; łącznie z ofiarą z człowieka. Następna scena - Sandra mówiąca o tym terminusom. Następna, jakiś mag mówi, że Sandra nigdy nie może nikomu powiedzieć co widziała i co zgłosiła. Dostała jakąś nagrodę. W kolejnej scenie - matka się wyparła Sandry a o Sandrze poszła fama, że nakablowała na mamę ponieważ mama ukrywała to, że ojciec nie jest magiem. I gildia się od niej odwróciła.
Andromeda widziała też sceny, w których Sandra studiuje Magię Krwi teoretycznie (ale nie stosuje tego w praktyce). Szuka. Ciągle szuka.
I sceny gdzie Sandra obserwuje Augusta. Obserwuje Andromedę. August, gardzący Sandrą, w którym Sandra jest zakochana. Andromeda, broniąca przed Augustem Sandrę. Andromeda, która jest człowiekiem o czym wie zarówno August jak i teraz Sandra.
I Sandra milczała. Nigdy się nie zdradziła. Jest tak samotna, że postrzega wszystkich jako zagrożenie. Andromedę mniej...
W pewien sposób Kasi spadł kamień z serca.
Mogła pójść spać.

Ranek.

# Lokalizacje:

1. Żonkibór
    1. domek Ewy Czuk, w którym znajduje się Zespół.
    1. pobliski kościół i parafia.
    1. Festiwal Marzeń

# Zasługi

* czł: Kasia Nowak, która rozwikłuje swoich sojuszników przy okazji zagadek lokalnych bogiń
* czł: Samira Diakon, społeczna iluzjonistka której niewiedza o magii coraz bardziej przeszkadza.
* mag: Sandra Stryjek, technomantka i astralistka która przy okazji jest ekspertem od mrocznych rytuałów. Ale się nie przyzna.
* czł: Andrzej Domowierzec, 37, martwy agent ubezpieczeniowy, ktorego obowiązki przejęła Małgorzata.
* czł: Małgorzata Poran, 47l, zarządza firmą PapiBór oraz Festiwalem Marzeń. Zupełnie po niej nie widać.
* mag: Herbert Zioło, terminus ściągnięty przez Andromedę mocą jej amuletu. Nadal, terminus nade wszystko.
* czł: Ewa Czuk, właścicielka domu w którym zamieszkał Zespół i matka fajnego syna.
* czł: Hektor Poran, 49l, przedsiębiorca który uratował Żonkibór, branża papiernicza. Aktualnie nie żyje, ciało zwęglone.
* czł: Michał Czuk, 16, pracowity chłopak który nie chce być "cool kid" i podkochuje się w dziewczynie która stała się "cool kid".
* czł: Józef Pimczak, ksiądz z Żonkiborskiej parafii prowadzący krucjatę w swoim umyśle.
* czł: Anna Kajak, jeszcze: Makont, policjantka (analityk) która spotkała się z najtrudniejszą sprawą w życiu.
* czł: Wojciech Kajak, policjant (partner i brat Anny) tak zakręcony że ucieka się do niestandardowych pytań.

# Pytania:

Ż: O czym Andromeda marzy?
K: Andromeda nie ośmiela się marzyć, chce jedynie spokoju i chwili wytchnienia.
Ż: Zawód istotnej postaci (męża Małgorzaty)?
K: Weterynarz
K->Ż: Czemu żaden z kontrolowanych magów nie chce skrzywdzić Andromedy?
Ż: Niezależnie od stanu artefaktu są fizycznie przeprogramowani na niezdolność.
Ż: Co sprawia, że policjantka jest tak zdeterminowana by osiągnąć cel?
Dust: Jest to dla niej sprawa osobista i rodzinna.
K->Ż: Kto jest najbliżej statusu przyjaciela Sandry?
Ż: Andromeda.
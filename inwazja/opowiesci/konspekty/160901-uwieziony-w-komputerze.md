---
layout: inwazja-konspekt
title:  "Uwięziony w komputerze!"
campaign: prawdziwa-natura-draceny
gm: żółw
players: kić, raynor
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [141110 - Prawdziwa natura Draceny (PT)](141110-prawdziwa-natura-draceny.html)

### Chronologiczna

* [141110 - Prawdziwa natura Draceny (PT)](141110-prawdziwa-natura-draceny.html)

## Kontekst ogólny sytuacji
## Punkt zerowy:
## Misja właściwa:

Dzień 1:

v1:
Tien Dziurząb przyszedł do Marka Śmietanki. Powiedział, że ten jest oskarżony i musi się stawić w Trocinie na sąd (~50 km stąd). Dał 12 godzin i sobie poszedł.
Marek Śmietanka nie miał pomysłu jak się z tego wywinąć. Zero. Gdzie są takie informacje? Marek wlazł do internetu i poszukał czegoś o prawach Świecy i prawach podejrzanego.
"Niezbędnik prawny dla maga niesłusznie oskarżonego przez Srebrną Świecę". Bardzo przydatna książka znaleziona w internecie. I Marek jest gotowy.

v2:
It wasn’t the best day for Mark. He was sitting in his home, preparing for the future – his less than legal contacts were going to offer him another job – but someone knocked at the door. It was a terminus, Alexander Dziurząb. Thing is, Alexander’s operating outside his influence area. 

Alexander said, that a sorceress from the Silver Candle has accused Mark of using a human to improper things to her. And thus Mark is being summoned to the court of the Silver Candle, in the city of Trocin, 50 km from here. Yay.

Marek wsiada na choppera i pojechał do Trocina, do kompleksu Świecy. Pion Ogólnodostępny, sąd magów.
Oskarżany jest Marek o: wykorzystania nierządnie i niegodnie tien Lilii Kałdun przez człowieka - człowiek to narzędzie zbrodni, nie Lilia.
No to Marek chce przesłuchać narzędzie zbrodni. Znaczy: człowieka. Oczywiście, wszyscy się zgodzili. 

Unfortunately, the person to be interrogated is a 16-year-old boy living in a small city of Kartuszewo. About 20 km from Trocin. So Mark drove there using his chopper while Alexander used the car. When they entered the house, Alexander introduced Mark as a “doktor Rzezimieszek” and they tried to interrogate the child using the mental magic artifacts. The problem is, the child “was not there”. Peter (name of the child) was not really in the coma; he did not inhabit the body. Perplexed, both termini left the building and decided to discuss this further.

Alexander told Mark, that he doesn’t really believe him. There are two options – either Mark is trying to be cute and thought he can outsmart the Silver Candle or someone is trying to frame Mark. But this means politics. Alexander has looked Mark up; there exists no reason for anyone to frame Mark. Yet the facts are what they are. Mark asked Alexander for advice – the technomancer suggested, that in the records of Silver Candle there is a medical doctor who wants to help humans in general. Her name is Pauline. However, Alexander want Mark to be careful – the person who put records on Pauline into the registers of Silver Candle was no one else but Lily herself.

In the meantime, Pauline is trying to control Dracena; the Diakon sorceress is quite unstable and her state is completely unknown; Pauline has never seen something like that before and she can’t even consult anyone. Pauline got a phone; it was Lily. Lily introduced herself and told Pauline, that Pauline doesn’t know her. Lily explained, that something is hurting humans and she cannot do anything about it. She needs a magical doctor to look at it; however, as this a dangerous case, Lily provided Pauline with a terminus. She explained her ruse – she falsely accused a terminus having no friends but being a competent infomancer and she planted information on Pauline, so the terminus will need Pauline to clear his name. Lily told Pauline that infomancer is necessary, as all the human victims were found in front of the computers; also, she personally cares, because one of the victims is her son from before the Eclipse. Because of the Guild regulations she cannot contact her husband or her child and that is why she has to ask a stranger for help. She will pay Pauline’s expenses.

The magical doctor would usually agree with Lilly’s motivations and her actions, but she has Dracena to take care of. Shocked, Pauline heard Dracena’s voice: Dracena said she is going to pack soon and they have to go. Dracena has heard the phone even if it wasn’t loud; the Desolation component of her body made it possible for her to hear the radio waves. Even better.

Pauline asked Dracena to take it easy, her internal conflict is not resolved yet. Dracena shouted at Pauline and moved to attack “don’t tell me what I am supposed to do”. She managed to hold on herself though and stormed away trying not to hurt anyone. Pauline is worried – Dracena is impossible to control, and she actually is very sick. And showing this what Dracena became to a terminus look like a grave risk. But on the other hand there are people who may die if Pauline doesn’t do anything and Dracena obviously is not willing to let it go.

Marek terminus z Czarnoskała dzwoni do Pauliny.

"Ten człowiek jest w stanie nieaktywnym. Nie kontaktuje, ale... jest w śpiączce, ale bardziej wygląda to jak sen. Ten człowiek jest kluczowy do wyjaśnienia jednej ze spraw - jest kluczowy wręcz. Chcę mu pomóc, jest to rzecz potrzebna dla tego człowieka... i dla sprawy. A jako że jestem terminusem bardzo zależy mi na sprawie, więc... ten człowiek jest... BYŁ prawdopodobnie, aczkolwiek nie jest to pewne, narzędziem wykorzystanym przeciwko innemu magowie i osobie podejrzanej o jego wykorzystanie jestem ja. A mi zależy by wydobrzał, bo jest jedyną szansą na udowodnienie mojej niewinności a ja tego nie zrobiłem. Przez moment nie byłem pewien by być szczerym, ale... chyba mnie pani zrozumie".

The discussion between Pauline and the terminus has proven that the terminus wasn’t really the type of person Pauline likes to work with. The terminus obviously cared for the humans only because his own skin was in game. On the other hand, is a terminus and he doesn’t want humans to be hurt. He doesn’t care about them. Like typical magi. So Pauline told Mark her expenses and expected profit and Mark reluctantly agreed. So Pauline is going to be paid both by Mark and by Lily. At least something. Pauline also told Mark that she is going to have an ill colleague who is being rehabilitated. Mark agreed – not much he could have done about that. And so they decided to meet next day.

However, Dracena got one of those phases. She started getting premonitions, that if they are not there during this night that something horrible may happen. Knowing how this may end, Pauline decided to humor Dracena and they decided to go this night. Pauline simply has to rebook the hotel.

Dzień 2:

In the morning, Dracena had one of her attacks; the aspect of the nymph (technically, wiła) and the aspect of the desolation were battling in her body, which resulted in physical mutilation of her hand. Also, her left leg is completely stiff as it was an artificial limb. Pauline did what she could to mend the damage and the girls went out to meet with the terminus in the morning. In the café.

Pauline is worried how Dracena will react to Mark. Fortunately, she neither fell in love nor got irrational hatred. She simply considered Mark to be cute and reacted naturally – with flirting. They decided to go meet the father - Karol Wadomiec - and to see what could have happened which resulted in the child – Piotr Wadomiec - losing control of his life. While driving there, Dracena unconsciously took control over the car and had to cast a spell to leave Pauline control back. Dracena’s aura is horribly mangled; also she does not control the emission level of her power and thus the terminus detected the corruption. He said nothing – one of the conditions of Pauline helping Mark was that Mark is not going to ask any questions about the Dracena.

Dracena was to stay in the car during all of that, while Pauline and Mark went to meet with the father of the victim. Pauline was very surprised, because the guy didn’t trust them at all. He didn’t want them to split – which was extremely inconvenient for Pauline, as she prepared and asked Lily much earlier about some kind of keyword (the guy was her husband, but the Eclipse split them apart). Oh well, after some completely pointless chatting she devised a sneaky plan – but I left a note to the husband and she left with Mark. Before they managed to get into the car, Karol (the father) phoned Pauline that he wants to talk to her. So she went in and asked Mark to take care of something else.

Pauline was saddened, because the guy seemed that he still loved Lily. He didn’t get over it. Of course, neither did the child. It was no more than six years ago. Karol that he didn’t trust Mark because earlier when he came it was strange. She could completely understand that. She got used to the problems with Magi. Karol wanted to help Pauline as much as he could – he explained to her that he had nightmares in the night when the child got into coma and Pauline could detect the faint smell of corruption (Skażenie). The kid was playing a popular computer game, a MMO, called “gnomes and weddings”. He was found sitting on the chair, in front of the computer. The stupid game was still on.

Pauline has managed to extract information, that Peter had someone in the game. It was a girl, called Lucy. And the father’s spider senses started to tingle – the girl was too perfect. She had attractive photos, she was there when he wanted her to, but she did not have a presence outside the game. No Facebook, no address. And Peter completely fell in love.

In the meantime, Mark, not really knowing what to do decided to cast a simple spell to see how did the connection look like – what computers did Peter connect with, what was the transfer of information, with whom was he talking… Normal stuff. As he cast a spell, something strange happened.

One, he realized that the instance of the game Peter was playing was a private pirate server. The server was located in the same city Mark was living – Czarnoskał. Two, by connecting the magical channels with the server, he opened himself to an attack by an unknown entity. One which used this situation and attacked him. Three, Dracena counterstroke - because of the elements shifting in her magical state, a kind of black hole for energy was created. And as vulnerable Mark appeared and an external energy was infusing to him, Dracena started interacting with the energy and absorbing it, influencing both the unknown entity and Mark. May I humbly remind you, that Dracena at this point has the worst possible magical signature ever.

As a result, completely unprepared Mark got struck down as if by electricity. Dracena didn’t really think highly of him at that point – a terminus unprepared for something like this. But Mark has managed to get information on what they’re fighting – a type of an astral spider. But this particular spider was either engineered or horribly mutated on its own, because normal astral spiders operate in the dreams. This one operates… In the Internet?

Dracena has managed to taste the opponent. She knows the taste of the spider, she can feel it - and the spider definitely felt Dracena’s aura. So, having a completely unconscious terminus Dracena left the car and came to him to wake him up. But the encounter has shaken her a bit (because of her unstable state) so Dracena pulled him up using her robotic hair and started amplifying the pain to wake him up. It succeeded. Seeing he woke up, she pulled him to her body and passionately kissed him. Mark was surprised, yet content. Then she asked for a cold shower. Literally. Mark took a garden hose and started to use it on her. That’s when he noticed, that her cybergoth outfit was actually not an outfit and her makeup is not a makeup…

In the same time, Pauline tried to explain to Karol that Mark may not be a doctor but he is a competent technical specialist and it would be wise to show him the computer where Peter collapsed. The federal just pointed outside the window were Mark was using the hose on Dracena. Pauline slightly facepalmed. Such competence. Much wow.

Pauline left the building, asked Dracena to return to the car and took Mark to Peter’s computer. Mark started to analyze the situation and he noticed, that the game was still running. He maximized the game and he noticed that Peter’s character was sitting in a tavern, cowered, and he was asking on the chat “help me help me help me”. After a short conversation, Mark determined that Peter in game is actually the very same Peter whose bodies in the coma. He was transferred to the computer. Wow. Like in the movies. 

His girlfriend, Lucy, started asking on a chat where is Peter hiding. Mark made an educated guess that Lucy is a spider! Peter should not contact her. Peter was panicking here, because he was in a PvP zone and everything could kill him - as he has interfaced with the game using the controls he has no idea how to actually fight on his character. Mark has interfaced with the pay to win components of the game and boosted all possible defenses. Now they have a goal - get Peter out before something horrible happens.

Lucy and Mark started chatting a bit; neither party decided that they like the other one. When Mark said he can find Lucy location, Lucy said that if she does that Peter shall be killed. And Mark got completely castrated. If Peter dies then he will never prove his innocence to the court of the Candle. 

That is, unless they find another way.

So after a short deliberation, and idea is born. Dracena is completely unstable at this point – she can be used as a medium. Let’s hook a computer into her (as a technomancer, she’s able to interface the accumulator as a power supply and a smart phone as a reliable Internet). Then it gets to the hospital, get some of the Peter’s blood and use all of that to pull Peter away from the computer into his body. Pauline is very hesitant - Dracena is simply too unstable to rely on her - but he has no choice if people are to be saved.

When they got to the parking lot near the hospital Dracena shouted, that Peter is being killed. Another character, sir Richard the terrible, is attacking Peter. Mark interfaced with the game and started using spells to power Peter up. In the short conflict where Lucy powers Richard up and Mark powers Peter up, Mark’s magic proves to be stronger. Richard started begging Peter not to kill him, but Mark allowed Richard to die.

Dracena lost it. Not only the spider got nourished but also a human has died. Mark answered, that actually Richard knew what he was doing. Dracena stroke Mark using her powered up body, forced him to kneel and acted as if she wanted to crush him to the ground. Mark, even being a terminus, wasn’t able to withstand her sheer strength. Pauline approached Dracena from behind to stop her and she got slapped; fortunately, nothing got broken. This snapped the Dracena out of her murderous rage. Pauline and Dracena hugged each other fiercely while Mark decided that it is not wise to try to reason with the crazy, sick girl.

Pauline went into the hospital having the approval of Peter’s father and took some blood samples from Peter. Afterwards, she returned to the car and prepared for the ritual. She also contacted her colleague, Bartłomiej Czyrawiec, about some information how to infect an astral spider. To cut of his energy sources. Bartłomiej explained to her the basic mechanisms which are instrumental in her plan how to eliminate the risk of having the spider roaming free.

When she returned to the car, Mark designed and created a virus basing on the information Pauline brought. Then they decided to create a lure - a bot acting more or less like Peter does. The virus shall be implanted into the bot and shall take place of Peter when Peter gets extracted. But to achieve that, they need to interlink with each other. To create a mind meld - three wizards with different talents and common goal. The only problem is, that neither Dracena nor Pauline like Mark at this point. So Pauline decided to try to be an interface between Dracena and Mark. Maybe it helps, maybe it will work.

The interlink succeeded. The spell extracting Peter from the computer succeeded as well; however, in the process, Dracena got destabilized further. And the spider, taking the lure, caused some corruption in the area. However, those are costs which are acceptable to everyone. Now, having a connection – that interlink – which still kind of worked Dracena suggested to pull all the other people from the server as soon as possible. Mark was not really interested in that, however he knew better than to argue with a sick crazy scary girl. Pauline knew it is the only shot before the connection shatters… So she agreed.

The plan was quite scary – Mark needs to enter the server and cut off the spider from the server at all. Then, they need to transfer all the humans into the Dracena; she is to become a temporary server. It is possible, as her pattern absorbed some of the spider’s pattern and her inorganic components - the desolation - should be enough to hold those other people without causing a Ghoul Syndrome. After that, the Pauline would interface with Dracena’s nymph component and use her blood to help other people return to their bodies. Scary and a bit too close to the blood magic, but that’s the best chance those people have.

The plan succeeded perfectly. Well, in the process Pauline got Corrupted and Dracena lost her consciousness but aside that everything worked perfectly. Furthermore, Mark has managed to corral the spider onto the server and entered the house to take the server. The spider tried to bargain with Mark. It was created, it did not want to be as it is. The spider offered, that it will grant untold riches to Mark, it will work for Mark. Mark answered, that he is a terminus – and a terminus does not deal with monsters. The spider shall get sent to the silver candle and the problem shall also be passed to the silver candle. Mark’s name will be cleared.

Epilogue:

Pauline got paid both by Lily and by Mark. This was one of her more profitable actions. And she has saved several people, which is always a good thing.
Mark’s name got cleared. Lily apologized and had to pay a fee. Mark got some recognition for actually finding and eliminating the modified astral spider.
Lily’s beloved song recovered.
Dracena still did not regain her consciousness.

# Progresja

* Paulina Tarczyńska: tymczasowo Skażona
* Paulina Tarczyńska: #1 Fire #1 Void; zapłacono jej zdecydowanie nadmiernie
* Marek Śmietanka: uznanie w Świecy (Trocin) za akcję z pająkiem i odpuszczenie Lilii
* Dracena Diakon: tymczasowo silna destabilizacja

# Streszczenie

Zmodyfikowany przez niewiadomego maga pająk astralny polował na młodych ludzi i więził ich "dusze" na pirackim serwerze popularnej gry "Gnomy i Śluby". Zdesperowana czarodziejka Świecy, Lilia, której syn (człowiek) wpadł w jego sidła ściągnęła na pająka terminusa i Paulinę. Udało się uratować większość ludzi i unieszkodliwić pająka (który trafił do Świecy na badania), ale Paulina jest Skażona po akcji, acz solidnie zarobiła. Dracena się poważnie zdestabilizowała - nie powinna była jednak w tej akcji uczestniczyć (Paulina nie chciała).

# Zasługi

* mag: Marek Śmietanka, terminus wrobiony w sprawę (niewinny), zachowujący się dobrze wobec ludzi bo zdominowany przez małą cybergothkę - ale uratował ludzi i uwięził pajęczaka
* mag: Paulina Tarczyńska, której obie strony zapłaciły za ratowanie ludzi - i która silnie ratowała każde ludzkie istnienie (choć jednego nie uratowała)
* mag: Aleksander Dziurząb, terminus, paranoiczny terminus bez poczucia humoru, który jednak uczciwie podszedł do Marka.
* mag: Lilia Kałdun, czarodziejka Świecy która "utraciła" rodzinę po Zaćmieniu (uzyskała moc) ale mimo regulacji próbuje nadal im pomóc; zaaranżowała sojusz Pauliny i Marka.
* czł: Piotr Wadomiec, 16latek, leżący w komie w łóżku po tym, jak zmodyfikowany pająk astralny wciągnął go do komputera (by się nim pożywić)
* czł: Karol Wadomiec, ojciec dziecka, nieufny * magom, acz zrobi wszystko by pomóc Piotrusiowi. Nadal kocha swoją żonę, o której wierzy, że nie żyje.
* mag: Dracena Diakon, zdestabilizowana i depresyjna, ale i przerażająca terminusa na śmierć fanatyczka ratowania każdego * człowieka. 
* mag: Bartłomiej Czyrawiec, znajomy Pauliny i ekspert od pająków astralnych. Nie powiązany z żadną istotną gildią; mieszka na Śląsku ale w jakimś mniejszym miasteczku.


# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Czarnoskał
                1. Czarnoskał, miasto o minimalnej obecności Świecy i w okolicy spokojnej
                    1. Centrum
                        1. Hotel Bażant, gdzie tymczasowo mieszkają Dracena i Paulina
                        1. Kawiarenka Trzy Krzaki Róży, miejsce spotkania Draceny, Pauliny i Marka
                    1. Narzaniec 
                        1. Ulica Stefloka, gdzie jest domek terminusa Marka Śmietanki
            1. Powiat Okólno-Trocin
                1. Trocin, ważne miasto Świecy na wschodzie Śląska
                    1. Centrum
                        1. Kompleks Srebrnej Świecy
                            1. Pion ogólnodostępny
                                1. Sąd magów
            1. Powiat Rymaski
                1. Kartuszewo, większa wieś o niewielkich walorach
                    1. Ulica Paszczaka, gdzie znajduje się dom Wadomców
      
# Wątki



# Skrypt

- brak

# Czas

* Dni: 2
---
layout: inwazja-konspekt
title:  "Cała przeszłość spłonęła"
campaign: rezydentka-krukowa
players: kić
gm: żółw
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [170325 - Klątwożyt z lustra (PT)](170325-klatwozyt-z-lustra.html)

### Chronologiczna

* [170325 - Klątwożyt z lustra (PT)](170325-klatwozyt-z-lustra.html)

## Kontekst ogólny sytuacji

## Punkt zerowy:

## Misja właściwa:

Dzień 1: 

Paulina jedzie zobaczyć stan Eneusa. Czy już się naprawił. Niestety, ku wielkiemu zmartwieniu Pauliny... Eneus nie dał rady przejść przez samonaprawę. Jest w jakiejś pętli. Nie umie się zrebootować. Chyba się naprawił, ale nie do końca prawidłowo działa. Chwilowo nie działa.

Paulina spróbowała go przebadać. Poszła po solidną apteczkę i spróbowała zdiagnozować co i w jaki sposób działa. Katalistka i medyk. Jest szansa, że da radę wstrzyknąć jakieś pozytywne, korzystne działanie... (7v6->S). Paulina potwierdziła swoje podejrzenia; Eneus wpadł w pętlę. Naprawił się, skasował klątwożyta. Nie może się jednak uruchomić; potrzebny mu neuronauta. Czy coś tego typu. Lub infomanta.

...Paulina zorientowała się, że w sumie, Eneus nie do końca jest terminusem ŚWIECY. Oni są lokalnymi konstruminusami, ale niekoniecznie są ze Świecy. Oni po prostu... są.

I to jest moment, w którym Paulina ucieszyła się, że w sumie jest lekarzem. Zdecydowała się poprosić o pomoc, dowiedzieć czegoś od swojego znajomego maga, neuronauty. Prośba o konsultacje, lekarz do lekarza (-1 surowiec). Jako, że Paulina chce tego neuronautę tu ściągnąć, zdecydowała się na maga lokalnego. Przez "lokalnego" rozumiemy "położonego niedaleko". Grażyna Kępka, neuronautka z okolicy. Paulina się zapowiedziała osobiście i pojechała do niej; ona mieszka w Szczypaku.

Neuronautka powitała Paulinę z uprzejmością; to starsza czarodziejka, koło 60. Z rozpędu powitała Paulinę jako "tien". Gdy Paulina pokręciła głową przecząco, Grażyna się zakłopotała lekko.

* Mam przypadek, z którym ja sama nie jestem w stanie sobie poradzić; nie bez dużego ryzyka dla pacjenta - Paulina
* I potrzebna Ci jest neuronautka - Grażyna, ostrożnie
* Tak. Służę transportem w obie strony. - Paulina, radośnie
* Potrzebuję trochę szczegółów; muszę wiedzieć, co mam zabrać ze sobą. - Grażyna, nadal ostrożnie

Paulina powiedziała, że chodzi o konstruminusa. Wyjaśniła w skrócie w sytuację (nietypowy klątwożyt z lustra zainfekował konstruminusa).

* Chodzi o te trzy konstruminusy z okolicy? - Grażyna, zaskoczona
* Wiem o dwóch - Paulina - Jaki jest trzeci?
* Eneus, Ferrus i Aurus - powiedziała Grażyna - trójka Strażników
* Mhm, o nich chodzi - Paulina potwierdziła smętnie - A dokładniej, o Eneusa
* Oczywiście, pomogę. Na pewno pomogę. Ta trójka jest bardzo pomocna. - uśmiechnęła się neuronautka
* W końcu bronią całej okolicy - dodała Paulina

Grażyna pojechała z Pauliną. Paulina powiedziała Grażynie, że najczęściej miała kontakt z Eneusem. O Ferrusie wie, o Aurusie nie słyszała.

* Nie wiem jak dokładnie się to skończyło; gdy byłam aktywnym magiem, to było ich trzech - Grażyna
* Co się skończyło? - Paulina, zaciekawiona - Były jakieś... poważne problemy?
* Nie, nawet Zaćmienie przetrwali. Wtedy z pięciu zostało trzech - Grażyna - Mówię o wybuchu.
* Wybuchu? - Paulina, smętnie

Grażyna opowiedziała Paulinie historię. To było jakieś dziesięć lat temu, przed Zaćmieniem. Poszła paczka do Archibalda, od jakiegoś innego Strażnika. I kurier nie chciał jej wnosić, więc ją zostawił gdzieś indziej. Archibald dowiedział się za późno i jak konstruminusy poszły jej szukać, było już za późno. Paczka eksplodowała. Zginęło kilkanaście osób i cenny eksperyment.

* Rozumiem, że było to jakieś zabezpieczenie? - wyraźnie niezadowolona Paulina
* Nie wiem, to w ogóle jest poza mną. Ale Archibald i Daria bardzo to przeżywali - Grażyna
* Znaczy... śmierć tych ludzi? - zdziwiona Paulina
* Tak. Chodziło o ludzi, nie o eksperyment; stać było go na kolejną paczkę - Grażyna, lekko zaskoczona zdziwieniem Pauliny

Paulina spytała, czy Grażyna jest Strażniczką. Odpowiedziała, że nie. Ostatnimi Strażnikami na tym terenie jest Archibald i jego konstruminusy. Wypytana, Grażyna dodała, że konstruminusy są bardziej istotami Świecy na tym etapie, ale Archibald pozwala im się ładować u siebie. Uważa to za rzecz, którą Strażnik powinien zrobić.

...czyli klątwożyt MIAŁ dostęp do rezydencji Archibalda. I mógł się rozmnożyć. Czy coś... ale czemu nie zaatakował Archibalda? O_o

Grażyna potwierdziła, że Archibald i Daria bardzo się kochali. On zrobiłby dla niej wszystko. A ona dla niego. Grażyna nie zna okoliczności śmierci Darii i nie chce ich znać.

Paulina zdecydowała się wyciągnąć kilka plotek o Archibaldzie i Darii (2v2->S). Usłyszała dość ciekawą opowieść o dwójce magów zaangażowanych społecznie i w świat magów i w świat ludzi - Archibald i Daria - którzy mieli dość niestandardowe podejście do magii i zasad. Paulina dowiedziała się, że to Daria używając artefaktów zebranych przez Archibalda stworzyła konstruminusów i oddała ich Świecy do ochrony tego terenu. Coś nie do końca typowego było z tymi konstruminusami, ale Świeca się nie do końca zorientowała, bo... Świeca.

Archibald i Daria zawsze próbowali wpływać na lokalnych magów, by ci dbali o interes lokalny. Swego czasu był bardzo ambitnym pieniaczem pod tym względem, wykorzystując pozycję swojej planktonowej gildii "Strażnicy", ale z uwagi na artefakty i inne takie Świeca uznała, że nie warto się z nim kłócić. Po co. Jest przydatny, jest pomocny... natomiast Daria dawała Świecy podarki, pomagała magom Świecy... w zamian za współpracę w społeczności.

Wszystko się rozpadło przy Zaćmieniu. Gdy Daria i Archibald się skupili bardziej na sobie. Niedługo potem Daria umarła. A Archibald zamknął się w swojej posiadłości. Podobno dalej pomaga... ale w zupełnie inny sposób.

Grażyna i Paulina siadły do Eneusa. Grażyna powiedziała, że potrzebuje kilka godzin; chciała popracować nad Eneusem od siebie, z domu. Paulina powiedziała, że nie ma szans go przetransportować. Chociaż... poprosiła o pomoc Wiaczesława. Ten powiedział, że będzie za 3 minuty. Faktycznie, przyjechał. Blisko był.

* o kurde, znokautowałaś terminusa... - Wiaczesław, smętnie
* Pomożesz go wyładować po drugiej stronie? - Paulina z dużym uśmiechem
* Pomogę, pomogę. Ale ja jadę Śledziem. - Wiaczesław, zafrasowany

Grażyna dostała Eneusa i się nim zajęła. Paulina i Wiaczesław... cóż, Wiaczesław spytał jak Paulina rozwaliła terminusa. I jak długo będzie leżał. Paulina zaczęła coś podejrzewać; Wiaczesław przyznał, że się interesuje sytuacją bo ma coś do zrobienia w okolicy. Paulina nacisnęła. (4v3->S). Wiaczesław powiedział jej co się dzieje. On ma w planie pokraść Archibaldowi grupę artefaktów, bo jest tym bardzo zainteresowany Gabriel Dukat. Teraz, jak Paulina rozwaliła mu jednego terminusa... musi poradzić sobie tylko z jednym.

Paulina zamarła, gdy Wiaczesław powiedział jej, że ma specjalnie zaprojektowane klątwożyty i inne elementy które pomogą mu w infiltracji i zdobyciu tego czego chce zdobyć. Dukat jest zainteresowany, Dukat dobrze płaci, Dukat dostanie. Wiaczesław powiedział, że sprawa jest bardziej zabawna. Dukat się wyraźnie boi, że Archibald Składak rozwali to, czego Dukat pragnie. Czyli gość o potędze Dukata nie umie zrobić czy odbudować tego co ma Archibald. Dlatego raz na jakiś czas ktoś próbuje. A nie, że wszyscy. I teraz Wiaczesław próbuje - bo nie ma innych magów a Paulina rozwaliła terminusa.

* Nie mogłabyś tak rozwalić drugiego? - Wiaczesław z nadzieją
* Nie. - Paulina, zimno

Wiaczesław powiedział Paulinie w zaufaniu, że mówią o jednym lustrze, jednym golemie i jednym komputerze. Paulina poskrobała się po głowie. To nie ma zupełnie sensu. I Paulina ma trzy opcje - nie zrobić nic, pomóc Wiaczesławowi (i Dukatowi) lub pomóc Archibaldowi. 

Paulina zdecydowała się nie zrobić NICZEGO z tym tematem. Poprosiła tylko Wiaczesława, by nie skrzywdził Archibalda. I by sam się nie dał skrzywdzić. Wiaczesław obiecał.

No i Paulina wróciła do Krukowa. Ma gabinet, ma swoje życie...

Dzień 2:

Paulina od rana usłyszała wiadomości w radiu lokalnym. Pożar w kamienicy; na szczęście nikomu nic się nie stało, ale było blisko. Nie wiadomo, co było w kamienicy, bo płomienie miały bardzo dziwny kolor. Straż pożarna zabezpiecza teren. Paulina spakowała apteczkę i pojechała tam...

Na miejscu - ku OGROMNEMU zdziwieniu Pauliny - jest Archibald opierniczający maga, który był u Dukata. Ów mag potraktował to z wystarczającą pokorą. Na oko Pauliny, 20 magów zajmuje się operacją Odkażania i containowania...

Paulina wycofała się i skomunikowała się z Wiaczesławem. Co on do ciężkiej cholery zrobił?! Wiaczesław odpowiedział, rwącym sygnałem, że uratował życie Archibalda. Wyniósł go. Paulina czując sygnał widzi, że Wiaczesław dostał; jest w złej formie. Pyta, czy potrzebuje pomocy. Wiaczesław potwierdził; jest w piwnicy, tam, gdzie był Eneus. Paulina pojechała...

Wiaczesław jest silnie poparzony energią magiczną i syntezą artefaktów.

* Co się stało? - zaszokowana Paulina
* On tam miał DUŻO luster - zgnębiony Wiaczesław
* Oberwałeś którymś? - Paulina
* Żeby jednym... - westchnął Zajcew
* No i co ja mam z Tobą zrobić? - zrozpaczona Paulina
* Mam najsilniejszy Wzór ze wszystkich magów w okolicy... i zdobyłem te obiekty - Wiaczesław
* Chcę wiedzieć, co to jest? - zainteresowana Paulina
* Jasne. Cholerny konstruminus. To nie był golem. Ukradłem TERMINUSA - Wiaczesławowi aż się oczy zaświeciły
* No... trochę przesadzasz - Paulina

Wiaczesław ogólnie opowiedział Paulinie, jak to wyglądało; najpierw unieszkodliwił konstruminusa (Ferrusa), potem wszedł do kamienicy. Tam odpalił klątwożyty i inne takie, dezaktywując i wypaczając wszystkie systemy i wtedy natknął się na cholerne lustra. I zszedł Archibald. I walczył z Wiaczesławem, przede wszystkim artefaktami i skutecznym używaniem luster...

I wtedy Wiaczesław dorwał się do konstruminusa, którego uruchomił Archibald. Ten konstruminus walczył jak szatan, ale Wiaczesławowi udało się go unieszkodliwić. Zmniejszył go i teleportował; po drodze lustra zduplikowały ten (i kilka innych) czarów, w ogóle, więc mogło się popieprzyć. No ale Wiaczesławowi udało się też złapać WŁAŚCIWE lustro; wtedy wrzucił staruszka w energię (musiał). ALE GO WYNIÓSŁ! Komputer... cóż, tego nie dał rady wynieść, ale Dukatowi najmniej na tym zależało. Ważniejszy był konstruminus i lustro...

Wiaczesław uciekł z małym terminuskiem i z lustrem, ze staruszkiem na plecach i Śledź ich odciągnął. Potem zostawił Archibalda i pojechał, przekazując Dukatowi szybko to co ukradł. Ludzie Dukata doprowadzili go do jakiejś formy, ale Wiaczesław zwiał im; nie chce ryzykować, że taka ekipa będzie chciała mu "pomóc". Woli Paulinę.

* Chłopie... - Paulina
* Nie pytaj. Nie miałem pojęcia. - Wiaczesław
* Masz siłę by do mnie dotrzeć? - Paulina
* Nie ma sprawy. Dam radę - Wiaczesław, Skażony, acz się regenerujący

Paulina podpiera Wiaczesława i jadą do niej, do jej gabinetu. Paulina tam zabrała się za korygowanie Wiaczesława, by ten znowu do czegoś się nadawał. Używa całości swojego gabinetu, zaklęcia, czas... ogólnie, pomóc Wiaczesławowi najlepiej jak jest w stanie. (12: heroiczna elita). W ciągu tygodnia będzie w pełni mocy i nie będzie trwałych efektów ubocznych. Wiaczesław obiecał Paulinie, że podzieli się łupem. Paulina kazała mu spać u góry na łóżku. Drugim łóżku. Wiaczesław potulnie poczeka...

Po tym jak Paulina dotarła do ruiny kamienicy, widzi wyraźnie, że odkażanie kamienicy zajmie dobry tydzień. Nie ma szans, że uda się to szybciej opanować. Dobrze, że Dukat wysłał specjalistów; włoży swoje koszta... Poszukała Archibalda - znalazła go.

* Czarodzieju, słyszałam w radiu. Jak mogę pomóc? - Paulina, do Archibalda
* Przesunąłem do przychodni Słonecznej Skażonych ludzi. Zajmij się nimi - Archibald, mocno Skażony

Archibald jest wyraźnie ciężko Skażony; widać w nim fizyczne zmiany. Paulina chciała dać mu coś na redukcję Skażenia, ale Archibald powiedział, że Paulina nie wie, co on ma w ciele. Trzeba zająć się ludźmi. Paulina poszła się nimi zająć; trudno z Archibaldem dyskutować w tej kwestii.

Mag Dukata zajmujący się Odkażaniem pacjentów to Janusz Kocieł. Paulina i on się znają przelotnie.

* Triażowałem ich... - Kocieł, ciesząc się widząc Paulinę
* Na czym stoimy? - Paulina, szybko
* Nocka. - Kocieł

Paulina pali kolejny surowiec (-1) za gabinet, sprzęt itp. Wspierana przez Kocieła, pracują nad Odkażaniem ludzi i koordynowaniu sprawy... Na szczęście, udało się. (13). Wszystkich da się uratować. To się rzadko zdarza, ale Dukat faktycznie wysłał dobrą ekipę a Archibald wyraźnie umie dowodzić takimi sprawami...

Aha, Paulina dostała wiadomość od Grażyny, że Eneus jest już na chodzie i naprawia Ferrusa.

Dzień 3:

Paulina zebrała co się dało zebrać i wróciła zmęczona do Archibalda. Poprosiła Grażynę o przyjechanie i zajęcie się Archibaldem. Archibald na pewno jest w ciężkim szoku, ale może uda się go namówić na jakieś leczenie czy coś...

Archibald nie spał całą noc. Jest zasilany przez magię. Inni magowie padają, Archibald stoi. Niepotrzebnie, bo sytuacja jest już lekko opanowana...

* Teraz pan powinien odpocząć. - Paulina
* Jeszcze nie zrobiłem wszystkiego, co muszę. - Archibald
* Rozumiem obowiązek... jeśli jednak pan padnie, nie zrobi pan tego, co trzeba - Paulina
* ... - Archibald całkowicie Paulinę zignorował
* Przejmujesz dowodzenie. - Archibald do Pauliny, z lekką rezygnacją
* Bardzo dobrze. Zatem, na czym stoimy? - Paulina z ciekawością
* Nie wiem. Ich się spytaj. - Archibald zatoczył łukiem w kierunku magów - Ja idę się przespać
* Samo przespanie nie wystarczy. Ile jednostek magii wchłonąłeś? - Paulina, z gniewem
* Daj mi te swoje środki... - Archibald z rezygnacją
* Powiedziałeś, że nie wiem, co masz w ciele. Co masz w ciele? - Paulina, z naciskiem
* Transorganiczne wspomaganie. Konwersję energii. - Archibald, zirytowany - Daj mi te swoje leki; wiem, jak je zażyć

Paulina odwołała dyskretnie Grażynę. Ta niewiele w tej sytuacji zrobi. Paulina podała Archibaldowi leki, a ów... zaczął się oddalać. 

* Możesz mieć wspomaganie, świetnie. Ale nie masz gwarancji, że nic ci nie będzie. - Paulina, nie odpuszczając
* Jestem stary. Nie mam już niczego. Nie obchodzi mnie, czy nic mi nie będzie. - Archibald, podnosząc głos. 
* Ale mnie obchodzi. Ile masz lat? 60? - Paulina, podnosząc głos
* 71, czarodziejko. I będziesz musiała żyć ze swoim obchodzeniem, bo nie odpuszczę - Archibald, niezadowolony - Masz sytuację. Napraw ją.
* Mam sytuację! Między innymi cholernie nieposłusznego pacjenta! - Paulina

Archibald całkowicie zignorował Paulinę i zaczął odchodzić. Paulina użyła broni atomowej.

* Jesteś pewny, że tego chciałaby Twoja żona? Żebyś się poddał, schował w norce, żeby umrzeć? - Paulina, zimno
* Sam ją zabiłem. Nie ma jej i nigdy już nie będzie, młoda damo. - Archibald, nie mniej zimno
* Nie, jej nie będzie - Paulina, spokojniej
* A więc rozwiązuj swoje problemy sama - Archibald.

Paulina jest święcie przekonana, że Archibald planuje popełnienie samobójstwa. Zrobiła wszystko co w jej mocy, by mu to uniemożliwić. Spróbowała go przekonać, by tylko do tego nie doszło. (2v3->F(wyjdzie na nadopiekuńczą), S). Paulina wyszła na nadopiekuńczą wobec wszystkich magów dookoła, ale udało jej się zmusić Archibalda do pójścia spać (w bezpiecznym dla siebie i ukrytym miejscu). Paulina wie, że Archibald będzie o siebie dbał...

# Progresja

* Archibald Składak: ma transorganiczne implanty kontrolujące energię
* Archibald Składak: stracił kamienicę, przeszłość i w sumie wszystko co miał w Kropwi

# Streszczenie

Dukat chciał koniecznie zdobyć lustro, konstruminusa (Aurusa) i "komputer" od Archibalda. Wiaczesław się tego podjął i wszystko wybuchło; Archibald stracił kamienicę i wszystkie artefakty. Archibald dowodzi ratowaniem Kropwi a Dukat wysłał siły by mu pomóc. W tym czasie Paulina rozpaczliwie ratuje kogo się da i poznaje odłamki z przeszłości Archibalda. Nic nie pasuje do siebie...

# Zasługi

* mag: Paulina Tarczyńska, lekarka ratująca Eneusa, potem ludzi, stawiająca Wiaczesława, przekonywująca Archibalda... jeden raz jak nic nie zrobiła wszystko wybuchło.
* vic: Eneus Mucro, po tym jak wpadł w nieskończoną pętlę: zregenerowany, pełnosprawny i odbudowany; zajmuje się Ferrusem.
* mag: Grażyna Kępka, neuronautka; starsza czarodziejka znająca Archibalda i wszystkich konstruminusów. Plus, lokalne ploteczki. Zna relacje Darii i Archibalda.
* mag: Wiaczesław Zajcew, z bardzo silnym Wzorem; porwał konstruminusa i doprowadził do zniszczenia domu Archibalda. Ciężko ranny.
* mag: Gabriel Dukat, który z jakiegoś powodu bardzo chciał zdobyć jakieś artefakty Archibalda nie antagonizując go za bardzo.
* mag: Archibald Składak, ma transorganiczny implant, potrafi dowodzić i zależy mu na społeczności. Stracił wszystko, co posiadał w ogniu magii.
* mag: Janusz Kocieł, lekarz magiczny na usługach Dukata, który ucieszył się mając wsparcie Pauliny w Odkażaniu ludzi.

# Lokalizacje

1. Świat
    1. Primus
        1. Mazowsze
            1. Powiat Pustulski
                1. Krukowo Czarne
                    1. Gabinet Pauliny, gdzie u góry śpi ciężko ranny Wieczysław ;-).
                1. Kropiew Dzika
                    1. Centrum
                        1. eleganckie kamienice, gdzie spłonęła w ogniu magicznym kamienica Archibalda
                    1. Zachód
                        1. osiedle Krogulca, skąd przetransportowano Eneusa do Szczypaka i gdzie schował się Wiaczesław
                1. Szczypak, niewielka wieś w okolicach Kropwi, gdzie mieszka neuronautka Grażyna

# Czas

* Dni: 3

# Narzędzia MG

## Cel misji

* Pokazanie Paulinie relację Daria <-> Archibald
* Pokazanie Paulinie jak Archibald koordynuje "Rojowca Kropwi"
* Pokazanie Paulinie, skąd Rojowiec Kropwi się wziął - pomyłka kuriera i śmierć ludzi
* Czy Paulina wejdzie w przestępczość by obudzić Eneusa?
* Czy Gabriel dostanie zapas artefaktów Archibalda? Archibald nie ma mocy by je utrzymać wszystkie...

## Po czym poznam sukces

* Paulina zrozumie o co chodziło Darii i Archibaldowi; zobaczy też, że Archibald kontynuuje ich pracę
* Paulina zobaczy jak działa Rojowiec Kropwi i zrozumie co robi Ostatni Strażnik
* Uda się lub nie. Nie jest to tak istotne.
* Paulina uruchomi Eneusa - i zapłaci coś za to
* Gabriel definitywnie dostanie zapas artefaktów, albo definitywnie go NIE dostanie

## Wynik z perspektywy celu

* Nope, jeszcze nie wyszło
* Nope i został zniszczony XD
* Historia: YES
* Mam odpowiedź
* Mam odpowiedź

---
layout: inwazja-konspekt
title:  "Pryzmat Myśli pęka"
campaign: czarodziejka-luster
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [141230 - Ofiara z wampira dla Arazille (An)](141230-ofiara-z-wampira-dla-arazille.html)

### Chronologiczna

* [141230 - Ofiara z wampira dla Arazille (An)](141230-ofiara-z-wampira-dla-arazille.html)

## Punkt zerowy:

Sytuacja Andromedy robi się coraz bardziej interesująca. Czterech zagubionych policjantów snujących się szukających swojego cienia, zaginione lustro Pryzmat Myśli, Sandra, która nie zapomina, ale boi się Andromedy i nie chce miec z nią wiele wspólnego, pojawiający się terminus, który podobno nie żyje (Gabriel Newa), porwana Diakonka incognito i nieznana druga Diakonka a nade wszystko wszechobecna postać Arazille której zdaniem Andromeda jest jej naturalną wyznawczynią... trudno się w tym wszystkim połapać. Tyle dobrze, że nie ma już wampira i że wraca August (a przybywa niejaki Patryk Romczak jako dodatkowe wsparcie).

## Misja właściwa:

Herbert dał Kasi radę. Jeśli Kasia nie chce wymazywać Sandrze pamięci (nie może, ale Herbert o tym nie wie), to niech Kasia ją zablokuje. A dokładniej: niech wyśle kogoś do kontaktów z Sandrą (Herbert oczywiście miał na myśli siebie) i niech Kasia powie Sandrze, by ta nie odważyła się nikomu pisnąć nic co wie. Andromeda wahała się chwilę, ale zgodziła się z nim - musi powiedzieć Sandrze, że ta ma zachować wszystko w tajemnicy i musi zrobić to odpowiednio solidną groźbą. Ku wielkiemu nieszczęściu Herberta Kasia przeznaczyła na łącznika Augusta. To znaczy, jak August się tu pojawi. Jako, że Arazille wie już o istnieniu Herberta, Herbert jest "jawnym terminusem". Bardzo go to nie ucieszyło. Patryk oraz August (oraz Sandra) będą jej ukrytymi agentami.

Telefon do Sandry. Czarodziejka przywitała Kasię bardzo chłodno. Andromeda poprosiła Sandrę by ta powiedziała jej czego się dowiedziała. Sandra zaczęła mówić o Arazille ("morderczyni magów") i o tym, że widziała wszystko co Kasia zrobiła w lustrach. Oddała bezbronną Anetę Hanson Arazille na pożarcie. Kasia JUŻ stała się agentką Arazille. Andromeda żywo zaprotestowała i powiedziała, że zrobiła to by zbliżyć się do uwolnienia wszystkich magów pod swoją kontrolą. Sandra parsknęła i powiedziała, że powie wszystko Augustowi i on jako terminus się wszystkim zajmie. Kasia powiedziała, że Sandra skrzywdzi sumienie Augusta lub zabije pośrednio Augusta i innych...
Sandra dodała też, że Andromeda coś jej zrobiła, uniemożliwiła się oddalenie Sandry czy pozostawienie tego wszystkiego. Andromeda w ciężkim szoku. Sandra dodała gorzko, że lokalnie znajduje się tu mag, który pracuje jako informatyk w policji. Ów mag sukcesywnie i wstecznie zmienia rekordy czterech magów na Festiwalu i sfałszował, że jest tam więcej policjantów. Każde zapytanie o pomoc ze strony policjantów w Żonkiborze jest sabotowane, bo ten czarodziej skutecznie ośmiesza i sabotuje tą czwórkę a nawet jak ktoś ma jechać to raportuje, że wszystko jest w porządku. Sandra dała imię i nazwisko tego maga (dość niewprawny technomanta, nie ma szkolenia SŚ. Nazywa się Sebastian Linka).
"Arazille, królowa perfekcyjnego zniewolenia. Arazille, morderczyni magów. Arazille, niszczycielka magów". Zdaniem Sandry, żaden mag który kiedykolwiek współpracował z Arazille nie przeżył. Znalazła też coś o Iliusitiusie - "on i Arazille są dwoma stronami tej samej monety, dwoma siłami pochodzącymi z jednego źródła". 
Sandra - "Nie mogę się oddalić od Ciebie, bo nie wytrzymam bólu. Nie mogę Cię skrzywdzić, bo oni wszyscy zginą. Jedyne co mi zostaje to współpracować z Tobą... powiedz mi jeszcze o swoim sumieniu i o tym, że nie jesteś jak Arazille."
Sandra faktycznie NIC nie może zrobić z punktu widzenia działania przeciw Andromedzie...

I faktycznie, rano pojawili się August Bankierz i Patryk Romczak. 
Ale jeszcze przedtem, rano Andromeda dostała wiadomość od Feliksa Bozura (kość szczęścia: 11/1k20). Weszła na GG. Powiedział, że spotkał się z Arazille jako z "królową niewolników" ale nie "niewolącą" a "patronką niewolników". I to Kasię zdziwiło. Bo Sandra i Herbert (oraz Artur Szop) dawali jej zupełnie inne wyjaśnienie. Powiedział też coś bardzo ciekawego o Iliusitiusie - powiedział, że do niego przypisane jest słowo 'wróżda' oznaczające "zwyczaj wywierania krwawej zemsty na sprawcy zabójstwa krewnego".
Feliks powiedział, że mnóstwo było pokasowane. Składał i składał, odtwarzał pliki, ale znalazł coś ciekawego. Iliusitius pragnął nowego porządku, pragnął sprowadzić masonów i iluminatów by wprowadzić nowy porządek i odebrać szlachcie polskiej ich ziemie. Arazille natomiast pragnęła ukojenia dla tych, komu nic już nie zostało. Jest tą, która da ci to czego pragniesz, niezależnie jak słaby nie jesteś. Powiedział, że jeszcze szuka, ale na razie bardzo trudno znaleźć jest cokolwiek na ich temat, nawet pseudoreligijny bełkot. Obiecał, że poszuka głębiej i poszuka jakichś rytuałów (Kasia desperacko szuka zrozumienia natury Arazille).
Ku swemu zaskoczeniu, potrafił znaleźć słabości zarówno Arazille jak i Iliusitiusa. Nie da się znaleźć kim są, nie da się znaleźć ich siły, ich religii, ale da się znaleźć słabości i wytyczne. Wielką słabością Arazille jest okrucieństwo i bezsensowne cierpienie. Iliusitius źle reaguje na wybaczenie i na altruizm wobec wrogów.
Baaaaaaaardzo to Kasi pomoże. Dodatkowo, słabością zarówno Arazille jak i Iliusitiusa jest "osoba bez duszy".
Feliks spytał Kasi po co jej to. "This is some weird shit". Kasia napisała z emotikonką ":-)", że ona płaci, on nie pyta i Feliks się wycofał. Powiedział, że się po prostu z czymś tak dziwnym nie spotkał od czasu necrosleep (mem w internecie). Spytał, czy to nie jest nielegalne - Kasia upewniła go, że nie. Wrócił do szukania...
Kasia wypłaciła mu bonus, pochwaliła i podziękowała oraz zachęciła do działania.

Andromeda stwierdziła, że należy szybko sprawdzić co dzieje się... u księdza. Bo ma podsłuchy, trzeba sprawdzić. Ku swemu zaskoczeniu słyszała rozmowę - Netheria rozmawia z Gabrielem. Na parafii. Netheria prosi by Gabriel ją wypuścił; musi wrócić bo krzywda się stanie Luksji. Netheria prosi, grozi, ostrzega, że pojawi się tu banda Diakonów a Gabriel zwyczajnie twierdzi, że o to chodzi - bo przecież bedą musieli Luksję odbić. Netheria powiedziała, że wpierw ONA powie, kto ją naprawdę porwał, że Gabriel z tego nie wyjdzie żywy. Terminus się tym jakoś nie przejął zanadto. A jaka w tym rola księdza? Ksiądz widzi w Gabrielu innego członka Kościoła a w Netherii grzesznicę...
Netheria jest zrozpaczona - pragnie chronić Luksję.

Włączyła też nasłuch na policjantów. Okazało się, że szukają... narkotyków. Anna i Wojciech wydostali się spod wpływu Arazille. Zauważyli, że Amelii nie da się znaleźć. Szukają tego, co wpędziło ich w pętlę marzeń, szukają narkotyków. Anna powiedziała Wojciechowi, że się martwi. Wampiry, dziwne narkotyki, brak wsparcia... chciałaby odpuścić. Chodzić i mówić "godzina 12:00, wszystko w porządku". To ponad ich siły. Jednak Wojciech się nie zgadza - muszą iść dalej, dojść do prawdy.

Andromeda znalazła Okazję. Andromeda chce uwolnionej Netherii, chce wiedzieć o co chodzi z Luksją i zneutralizować "armię". Może da się do zrobić inaczej niż inwazją armii Diakonów. Może mieć jej współpracę. A jak to zrobić? Ma wolnych policjantów. Jak tylko August i Patryk się pojawili, Andromeda wysłała policjantom cynk, że na parafii dzieje się coś złego - ktoś jest tam przetrzymywany wbrew swojej woli.
Oczywiście, policjanci poszli się tym zająć. Wojciech i Anna - pozostali są unieczynnieni przez Arazille.

Andromeda słucha przez komunikator. Herbert obserwuje z bezpiecznego miejsca. August ubezpiecza Herberta. Akcją od strony terminusowskiej dowodzi Herbert. W kościele NIE MA Gabriela; gdzieś wybył. Jest tylko ksiądz i Netheria. Do kościoła weszli policjanci...

ksiądz: "Witajcie w mojej świątyni!"
Wojciech: "Przetrzymujesz kobietę, ojcze! Przybyliśmy ją odebrać"
Anna: "Serio. Tak jawnie."
ksiądz: "Nie! Nie kobietę! Ale mam jawnogrzesznicę w więzach!"
Wojciech: "Widzisz?"
Anna: "..."

Faktycznie, ksiądz zaprowadził policjantów do zakrystii gdzie znajduje się (ubrana) Netheria w więzach. Szybko zaczęła mówić o tym, że została księdzu sprzedana przez dwóch Bułgarów i prosi o uwolnienie... policjanci zabrali się do uwalniania Netherii, na co zmaterializowała się Arazille. 
Netheria nie od razu, ale rozpoznała Panią Marzeń. Arazille była przyjemnie zaskoczona sławą. Netheria powiedziała, że ma zamiar jej służyć, bo nie jest w stanie jej pokonać. Bardzo pragmatyczna Diakonka. Tymczasem policjanci wyszli z szoku i zorientowali się, że mają do czynienia z czymś nadnaturalnym. Arazille nie dała im szansy - powiedziała "chodźcie do mnie" i użyła swojej mocy. Wojciech podszedł do bogini, jednak Anna nie. Arazille była zaskoczona - jak człowiek opiera się jej wpływowi. Anna wyciągnęła Pryzmat Myśli i skierowała go w Wojciecha i Arazille. W tym samym momencie Andromeda zobaczyła tą scenę jakby tam była - i zobaczyła inną scenę, Samira (ale młodsza i bardziej elegancko ubrana) trzyma rękę na ramieniu Kasi i obie patrzą w lustro a Samira wypowiada jakieś słowa. Koniec wizji z przeszłości.
Arazille dostała silny cios, ale przywołała lustro mówiąc o wielokrotnych odbiciach i powiedziała, że lustro to tylko ciecz w spowolnionym tempie. Użyła swej mocy by trzymane w ręku lustro zaczęło "spływać" i Pryzmat spróbował zrobić to samo. Ale Pryzmat Myśli zbudowany był z wielu mikro-luster, więc się rozpadł na kawałki. To uwolniło Arazille, lecz się zdestabilizowała, więc wypuściła potężną flarę energii magicznej w niebo. I zniknęła, niezdolna do utrzymania formy materialnej.
Krzyk policjantki przez komunikator.
Krzyk Samiry z pokoju obok. Rozrywający ból, jakby coś w Kasi pękło. Kasia doczołgała się do Samiry; ta się wije i piszczy "Nie Pryzmat... Tylko nie Pryzmat... Teraz nie mam jak cię uratować...".
I Kasia straciła przytomność. Wszystko w ostrożnych rękach Herberta.

Kasia otworzyła oczy. Leży w jakimś starym pokoju. Wszedł do niej Herbert i powitał ją w ich nowej kwaterze głównej - domu w Wyjorzu (mieście gdzie znajduje się Sandra). W czasie jak Kasia była nieprzytomna Herbert wszedł, przechwycił Netherię oraz policjantów i się ewakuowali furgonetką z Żonkiboru do Wyjorza. Na szybko z Netherią załatwili wynajem tego domu i zapłacił z funduszy SŚ. Przekazał Świecy informację, że dzieją się tu rzeczy anomaliczne i że zagrożona jest czarodziejka Millennium (Luksja) a on był proszony o pomoc, bo jest na miejscu. W związku z tym dostał pozwolenie działania w tym terenie, ale oficjalnie nie powinien nic robić wobec lokalnych magów. Anomalie, viciniusy i ludzie się nie liczą. Herbert powiedział mocodawcom z SŚ, że nie zamierza działać bez prośby ze strony czarodziejki Millennium, przez co odpowiedzialność spadnie na nich. SŚ się to spodobało.
Sandra odmówiła dołączenia do zespołu, powiedziała, że jest bezpieczniejsza w kawalerce w centrum miasta. 
Stan Samiry: obudzona, mało komunikatywna, nie zna tu nikogo poza Andromedą więc uważa, że została porwana. Netheria: aktywnie współpracuje (jakby co, August ma na nią oko). Policjanci: Patryk nad nimi pracuje (ciekawy dobór słów). Kasia jest gotowa do dalszego działania jak tylko Patryk ją zbada. Herbert jej nie puści wcześniej.

Patryk przeskanował magicznie Andromedę (1/k20 na szczęściu, lol). Powiedział jej, że nie powinna była przeżyć tej flary; flara uderzyła Samirę oraz Andromedę mimo, że ich tam nie było. Nie uderzyła tak mocno księdza ani policjantów. Zupełnie, jakby Kasia i Samira były na miejscu, były bardzo blisko Arazille. Powiedział Kasi, że w niej jest sporo tkanki magicznej i wyjaśnił jej ten fenomen - tkanka magiczna powinna zmienić jej klasyfikację z człowieka na viciusa. Samira przeżyła, bo jest eks-magiem (i jakkolwiek stężenie tkanki magicznej się zmniejsza, to powoli). Ale Kasia, bo konsekwentnie wystawia się na różnego typu promieniowania i zmienia się w viciniusa. Kasia przypomniała sobie mandragorę, Szczypiorków i wszystko to co było przedtem...
Patryk kazał jej mieć dobre i pozytywne myśli by tkanka magiczna poszła w fajnym kierunku. Dość dobijające. Kasia zrobiła duże oczy, jak dowiedziała się, że się tak fatalnie czuje już po oczyszczeniu przez Patryka. Dodatkowo Patryk powiedział Kasi, żeby uważała na ten amulet; on dostosowuje Kasiną tkankę magiczną do swojej woli. W odpowiedzi na to, Kasia zdjęła amulet Podniesionej Dłoni i położyła go na wysokiej półce. By nikt przypadkiem go nie dotknął.

Kasia jest dodatkowo podłamana, gdy dowiedziała się, że Herbert nie wziął ani żadnych obrazów, ani narzędzi, ani żadnych luster. Kasia ma jakieś lusterko, ale to jest to jedno. Sprzęt Samiry także został w Żonkiborze. Szczęśliwe myśli, co?

Kasia zdecydowała, że z punktu widzenia całej sytuacji najlepiej będzie sprawić tak, by ona myślała, że to Herbert pociąga za sznurki. Taki plan, doskonały - przecież mag nie będzie słuchał się człowieka. Herbert powiedział, że jeśli Kasia chce wypytać o coś Netherię, może ją przedstawić jako "swojego" człowieka przyuczanego do tropienia i pracy detektywistycznej. Kasia się zgodziła. Zdecydowali się odwiedzić Netherię. W pokoju Augusta (tam August jej pilnuje).

Herbert otwiera drzwi do pokoju Augusta a tam... Netheria w częściowym negliżu i August w negliżu. Chłopak spiekł raka a Herbert zacisnął szczękę. Wyklął Augusta od niekompetentnych idiotów i kazał mu stąd natychmiast wyjść. Następnie przeprosił Netherię za całą sytuację i powiedział, że oczekiwał więcej kompetencji od terminusa Srebrnej Świecy. Czarodziejka uśmiechnęła się promiennie i powiedziała, że nic się nie stało. Herbert stwierdził, że się stało...
Herbert zaproponował Netherii że przepyta ją jego człowiek którego szkoli na detektywa.

"Kupiłeś ją u nas?" - zdziwiona Netheria
"Nie wiem. Dostałem ją ze Świecy. Skąd to pytanie?" - Herbert
"Dziwne skojarzenie..." - Netheria
"No dalej, Kasiu, pytaj panią." - Herbert

Zanim jeszcze Andromeda przepytała Netherię, ta przyznała się do czegoś. Wyciągnęła od Augusta kto naprawdę dowodzi. Wie, że dowodzi Kasia. Herbert zrobił minę typu "zabiję szczura". Kasia nie inaczej. Spieprzył potężnie. Netheria powiedziała, że z jej punktu widzenia to nie ma znaczenia, Millennium nie interesuje się czy mag, człowiek czy vicinius. Przyjmuje pod kompetencje, nie jakiej jest rasy czy ile ma tkanki magicznej. Herbert zauważył, że Millennium to mafia. Netheria odparła, że tak, mafia - broni swoich i nie odbiera swoim pamięci gdy stracą moc. Kasia zauważyła, że Netheria dość ostro to powiedziała. Ta odpowiedziała, że miała przyjaciela w SŚ, Diakona, któremu wymazali pamięć. To był z powodów czemu po Zaćmieniu sformowano Millennium. 
Kasia chciała się dowiedzieć od Netherii co ona tu w ogóle robi. Czarodziejka powiedziała, że przyjechała z Luksją na Festiwal. Plus, ona (Netheria) ma ezoteryczne hobby nie powiązane z erotyką którego nie chce zdradzać Millennium. Kasia nie wnikała. Powiedziała, że chciała zostawić Luksję i znaleźć coś do hobby o czym wiedziała że tu jest, ale Luksja zniknęła. Potem dowiedziała się, że Triumwirat ją porwał. Po co? Bo ładna dziewczyna, niepotrzebny był inny powód. No to Netheria poszła do Triumwiratu ale wpierw poinformowała rodziców Luksji. W Triumwiracie powiedziała kogo porwali i zaproponowała polubowne rozwiązanie - oni oddają Luksję, Netheria i Luksja opuszczają teren. Triumwirat się nie zgodził. Netheria zdecydowała dołączyć do Triumwiratu. Będzie im służyć. Po pierwsze, by ulżyć Luksji i nauczyć ją tak, by nic się jej nie stało zanim nie przyjdzie odsiecz. Po drugie, bo może uda jej się zmienić ich od środka.

"Z doświadczenia, ładnych dziewczyn się nie zabija. Robi się z nimi inne straszne rzeczy" - Netheria.
"Co było do okazania..." - Herbert, myśląc o Auguście.

Ale wtedy zadzwoniła Kasia. I cały plan (naprawdę by współpracowała!) poszedł się kochać. Netheria dostała od nich zadanie porwać Kasię by mogli ją sami przesłuchać. Gabriel miał ją asekurować i pilnować by nie zrobiła nic głupiego. Ale Gabriel ją porwał. Czego Netheria zupełnie nie rozumie, ale rozmawiała z nim; wszystko wskazuje na to, że Gabriel chce wywołać wojnę. Chce zniszczyć Triumwirat. Kosztem kogokolwiek (z jej punktu widzenia: Luksji).
Netheria oferuje współpracę Kasi (i oferuje przyłączenie jej do Millennium, opcjonalnie). Przysięga, że będzie współpracować.

"Słuchajcie. Nikt nie chce ze mną współpracować. Porwali Luksję i poszłam do Triumwiratu mówiąc, że jak ją uwolnią, pomogę im. Nie zgodzili się. Potem porwał mnie Gabriel i powiedziałam, że mu pomogę. Też odmówił. Arazille zaproponowałam, że będę jej wiernym pieskiem. Odmówiła. Może choć wy się zgodzicie... samotnej dziewczynie trudno." - Netheria do Kasi.

Gdy przycinał jej Herbert, Netheria powiedziała, że Kasia zawiodła jako dowódca. August jest samotny. Ile czasu jej zajęło zyskanie jego zaufania? Z jej punktu widzenia on stracił ojca (któremu wymazali pamięć) i nie miał nawet pomocy psychologicznej. Herbert powiedział, że August jest terminusem i powinien sobie z tym poradzić lub to zgłosić. Netheria parsknęła. Powiedziała Kasi, że jeśli ta pozwoli Netherii przebywać z Augustem ona nie tylko nie chce robić mu krzywdy, ale spróbuje mu pomóc. Bo żal jej chłopaka. Bo chce, by pojawił się terminus, który spojrzy też na ludzi, nie tylko magów. Herbert zauważył, że Netheria mówi "pod publiczkę" wiedząc, że Kasia jest człowiekiem. Netheria powiedziała, żeby w takim razie potraktowali to jako akt dobrej woli.
Kasia zgodziła się, by Netheria pomogła Augustowi pod warunkiem, że nie wykorzysta go jako źródła informacji i nie zrobi nikomu krzywdy. Netheria przystała na te warunki.

Kasi w Netherii coś wydało jej się znajomego... ale nie wie co. Dla odmiany spróbowała się na tym skupić, porozmawiać jeszcze z czarodziejką, zastanowić się (10v12->13). Arazille! Ten sam typ urody. A Kasia wie, że Arazille dla każdego wygląda inaczej. Czyli coś w Kasi jest, coś głęboko, co przypomina jej Diakonów. Ale nie wie co.
Ale Kasi kliknęło jeszcze coś w głowie. Netheria jest nekromantką. To oznacza, że po raz pierwszy Kasia ma możliwość przepytania czy porozmawiania z osobami którzy zginęli przez Arazille. Może dowie się coś więcej na temat Arazille od martwych, nie żywych...

Otwierają się nowe możliwości.

Netheria wyczuła, że Kasia choć twarda jest dość zagubiona, przestraszona i nieszczęśliwa. Na pożegnanie ją serdecznie i mocno przytuliła.
Andromeda zesztywniała, ale w dotyku Netherii nie było NIC erotycznego.
Herbert próbował podejść, ale Kasia zatrzymała go gestem.
Rozstali się. Netheria poszła do swojego pokoju i obiecała że nie będzie stąd wychodziła a Andromeda i Herbert poszli szukać Augusta.

A Augusta nie ma. Zostawił karteczkę "oddaliłem się zebrać informacje od Sandry" i zwiał. Herbert przekrzywił głowę. Jeśli August myśli, że uniknie bury i opieprzu maksymalnego bo jemu przejdzie gniew...

Cóż. Kasia poszła do Patryka spytać go w jakim stanie są policjanci. Ten powiedział, że miał nadzieję, że Kasia nie zapyta. Policjantka... on nie rozumie do końca co się stało, jest za słabo Skażona. Ta energia powinna w nią trafić, ale poszła "gdzieś indziej". Poprosił o możliwość skorzystania z Netherii celem sprawdzenia jej pamięci i zrozumienia co tam się naprawdę działo. Dodatkowo poprosił o możliwość skorygowania pamięci Anny Makont - bo Maskarada, ale też bo jej stan psychiczny. Na to pierwsze Kasia się absolutnie nie zgodziła, ale z uwagi na stan psychiczny - jak najbardziej. Patryk następnie powiedział o Wojciechu. Nie wie co mu jest. Zupełnie, jakby to, co zostało "po tej stronie" było odbiciem czterowymiarowego bytu w lustrze, lub cieniem. To nie ma sensu, ale jego magia w żaden sposób nie potrafi go sensownie zaadresować. A on tu jest, choć jest ideą a nie organicznym bytem. Patryk nie wie jak mu pomóc.
Netheria zgodziła się pomóc Annie i spróbować dowiedzieć się (pomóc Patrykowi) co stało się z jej bratem. Spróbuje to naprawić.

Kasia zadała jeszcze Netherii kilka niezobowiązujących pytań odnośnie Diakonów i wymazywania pamięci (i nie tylko). Z tego jednoznacznie wyszło, że Samira miała wymazaną pamięć przed epoką Millennium.

Kasia wzięła amulet. Skontaktowała się z Augustem. Terminus powiedział Kasi, że mają sporo informacji, ale nie wiedzą kto stoi za Sebastianem Linką. Powiedział, że dobrym planem będzie napaść na niego i wyciągnąć informacje. Nagle - Kasia usłyszała głos Sandry która stwierdziła, że to fatalny pomysł, mogą po prostu wyciągnąć informacje z jego komputera. I ma pewne pojęcie - nie wie kto za nim stoi, ale bardzo dużo przelewów i informacji na jego komputerze powiązanych jest ze słowem "trójząb". Kasia już wiedziała. Kazała Augustowi wracać szybko, na co terminus poprosił, czy nie może chwilę zostać z Sandrą, bo jest bardzo zestresowana. Andromeda miała wątpliwości czy ona czy on jest bardziej zestresowany, ale czemu nie, w końcu chciała by się dogadali...

Do Kasi zadzwoniła Sandra - ostatnia rzecz, jakiej by się Kasia spodziewała. Sandra spytała, czy Kasia naprawdę chciała jej pomóc. Tak. Sandra powiedziała Kasi, że skanując komputerowy system Sebastiana Linki doszła do tego, że jest on głównym adminem Triumwiratu. To oznacza, że Sandra ma potencjalnie dostęp do osoby mającej większość rekordów Triumwiratu - ale nikt nie atakuje Triumwiratu, bo reputacja. Mimo, że ona z pomocą Augusta (muszą się fizycznie włamać) powinni dać radę zdobyć cenne informacje - być może większość informacji Triumwiratu odnośnie członków, zakresu, usług... bardzo cenna sprawa. Ale do tego Sandra chce Augusta na noc. Poprosiła też Kasię, żeby ta wybaczyła Augustowi - dla niej. Sandra nie wie co August zrobił, ale dzięki temu rozmawia z Sandrą, martwi się, a jak ONA będzie w stanie powiedzieć Augustowi "rozmawiałam z Kasią. Wybaczyła ci i cię nie ukarze, bo poprosiłam" to zapunktuje. 
Kasia nie chciała nigdy karać Augusta, więc się zgodziła. A i zyskała w oczach Sandry. Sytuacja zaczyna się poprawiać...

Kasia spacyfikowała Herberta, który chciał zgnębić Augusta w błoto. Herbert jest nieszczęśliwy.

Netheria potwierdziła, że Triumwirat i "trójząb" są powiązane. I że Luksja jest przetrzymywana w "Trójzębie", na wysokich piętrach. A zdaniem Sandry serwerownia jest w słabo chronionej piwnicy "Trójzębu"...


# Lokalizacje:
1. Wyjorze
    1. peryferia
        1. dom, duży, wynajęty o pow. całkowitej ok. 320 mkw.
            1. działka przy domu, 400 mkw.
    1. centrum
        1. kawalerka wynajmowana przez Sandrę.
        1. "Trójząb", forteca Triumwiratu.
1. Żonkibór
    1. domek Ewy Czuk, w którym znajdowały się Andromeda i Samira
    1. pobliski kościół i parafia.
        1. zakrystia

# Zasługi

* czł: Kasia Nowak, zbierająca informacje i obserwująca jak rozpada się lustro i jej własne wyobrażenie o niej samej.
* mag: Herbert Zioło, terminus z dobrymi pomysłami, który bardzo próbuje się wykręcić od czegokolwiek co może być choć trochę niebezpieczne.
* mag: Sandra Stryjek, eksplorująca swoją nową sytuację z dziwnym połączeniem luster z Andromedą. Niechętna sojuszniczka Andromedy, operuje w Wyjorzu.
* czł: Feliks Bozur, student przynoszący Andromedzie zupełnie inne informacje o Arazille i Iliusitiusie niż słyszała wcześniej od magów.
* vic: Iliusitius, podobno aspekt 'wróżdy' i pomsty. Wciąż nieobecny i milczący.
* vic: Arazille, zraniona przez Pryzmat Myśli dzierżony przez policjantkę. Też, "morderczyni magów" i "patronka niewolników", o dziwo.
* mag: Netheria Diakon, która wpierw jest odbita przez policję, potem wyciąga wieści z Augusta a potem okazuje się, że jest w niej więcej niż się zdawało.
* mag: Luksja Diakon, przetrzymywana przez Triumwirat.
* mag: Gabriel Newa, okazuje się - gracz, a nie pionek. Chce rozpocząć wojnę. I - mimo, że członek Triumwiratu - chce zniszczyć Triumwirat.
* czł: Wojciech Kajak, który stał się ofiarą Pryzmatu Myśli co zmieniło niepowtarzalnie jego życie.
* czł: Anna Kajak, jeszcze: Makont, która użyła Pryzmatu Myśli krzywdząc swojego ukochanego brata. W ciężkim stanie psychicznym.
* czł: Józef Pimczak, ksiądz we władzy Gabriela. A może Arazille?
* mag: Patryk Romczak, kompetentny lifeshaper, katalista i puryfikator któremu spodobała się zagadka Kasi i jej tkanki magicznej. Z charakteru konkretny. Na oko koło 50 lat.
* mag: August Bankierz, który strasznie zawalił idąc do łóżka z Netherią i mówiąc jej kto tak naprawdę dowodzi (Andromeda).
* mag: Sebastian Linka, niewprawny technomanta który fałszuje rekordy przez co policjanci nie dostawali wsparcia. Powiązany z "trójzębem".


# Pytania:

K: W jaki sposób August przestanie tak reagować na Sandrę i zacznie traktować ją bardziej "po ludzku"?
B: Dzięki pewnej Diakonce.
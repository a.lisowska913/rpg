---
layout: inwazja-konspekt
title:  "Sprawa Baltazara Mausa"
campaign: powrot-karradraela
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [150826 - Pętla dookoła Pauliny (PT)](150826-petla-dookola-pauliny.html)

### Chronologiczna

* [150826 - Pętla dookoła Pauliny (PT)](150826-petla-dookola-pauliny.html)

### Logiczna

Bezpośrednia kontynuacja:
* [150913 - Andrea węszy koło Szlachty (AW)](150913-andrea-weszy-kolo-szlachty.html)

### Inne

Kończy się NA PEWNO przed misją:
* [150830 - Kasia, nie EIS, w Powiewie (PT)](150830-kasia-nie-eis-w-powiewie.html)

### Punkt zerowy:

## Misja właściwa:

Andrea miała ogólnie gorszy okres. Po ostatnich triumfach powiązanych ze Szlachtą została przesunięta na jakieś marginalne, acz dość interesujące zadania. Ani to Mausy, ani Szlachta. Andrea zdecydowała się na grzeczne działanie; poczekać zawsze można.

Aż tu pewnego dnia wezwał do siebie Andreę Aleksander Sowiński...
Powiedział Andrei, że jako, że ona specjalizuje się w Mausach, dostaje zadanie rozwiązania sekretu Baltazara Mausa - jakim cudem szczyl pokonał terminuskę. Jego zdaniem to jakieś działania wewnętrzne w Świecy, więc Wydział winien to rozwiązać. Zadaniem Andrei jest rozwiązanie problemu (znaleźć winnych, zgłosić się do niego z nazwiskami).
Andrea wolałaby większą autonomię, ale akceptuje.

Oczywiście, Kropiaktorium jest w raporcie odnośnie Baltazara Mausa. I wszystko wskazuje na Baltazara Mausa. Wszystko. A Baltazar jest członkiem Świecy. Za atak na terminusa nie grożą małe kary...
Andrea puściła przeszukanie o Baltazarze Mausie. Okazało się, że jest to pierwszy mag Mausów z Rodziny Świecy od dawna. Piętnastoletni czarodziej, jego "rodzice" (Mausowie) niewłaściwie zachowywali się wobec ludzi, Estrella Diakon znalazła po ich stronie pewne przewiny i oskarżyła ich mocą terminusa. Dostali ostry opiernicz. Wszyscy wiedzieli, że działania Estrelli były związane z ludźmi, ale miała prawo to zrobić. Z uwagi na duży koszt dla rodziców Baltazara, młody Maus poprzysiągł Estrelli krwawą zemstę - i mówił to jawnie. Estrella olała temat; nie wyciągnęła konsekwencji.

W rodzinie nie ma innego rodzeństwa a "rodzice" nie zrobiliby niczego przeciwko terminusowi. 

Baltazar Maus jest ognistym dzieckiem, bardzo lojalnym; to co zrobił pasuje niestety do jego charakteru. Lojalny i mściwy. W Świecy podniosła się już debata, czy to nie oznacza, że Rodzina Świecy powinna odebrać chłopaka rodzicom. Baltazar wie o tym, że pochodzi z Rodziny Świecy; nie obchodzi go kim był wcześniej. Chłopakowi zależy na swojej rodzinie bardziej niż na rodzie.

Andrea poszła porozmawiać z rodzicami Baltazara Mausa. Nie zapowiadała się.
Elea Maus była w domu. Powiedziała Andrei o Baltazarze; młody, lojalny, porywczy, naiwny.
Elea powiedziała, że Baltazar ostatnimi czasy interesował się terminusami, planami i walkami. Dostał dostęp do różnych ciekawych książek na ten temat; rodzice Baltazara nie pytali. Baltazar miał jakąś dziewczynę; przez nią to wszystko przeszło. Nazywała sie Malia Bankierz. Elea się uśmiechnęła; sprawdziła Malię na hipernecie i skorelowała jej pozycję. Zdaniem Elei, Malia - dziewczyna Baltazara - była tą, która dostarczała chłopakowi wszystkich książek, planów, pomysłów. Elea powiedziała, że dała znać w raporcie i powiedziała o działaniach Malii - ale informacji o Malii nie było w raporcie.

Andrea szybko sprawdziła Malię Bankierz; szesnastoletnia czarodziejka, blisko powiązana z Ozydiuszem. On jest wujkiem Malii.

Zapytana, Elea powiedziała co o tym myśli - Malia nie była aż tak mocno zainteresowana Baltazarem jak Baltazar Malią. Malia najpewniej też była przez kogoś wykorzystana; pytanie, przez kogo. Malia jednak ma plecy, ktoś ją chroni - a Baltazar nie ma nikogo. 

Andrea odniosła się do bazy danych Wydziału Wewnętrznego. Bez problemu wygrzebała informacje o Malii Bankierz:
- Malia nie uczy się na terminuskę, ale zawsze fascynowała się terminusami i pracą terminusa
- Malia wczytuje się w płomienne romanse i próbuje swoich sił w kierunku na rozrywkę
- Malia uczy się, by być "animatorką magiczną" - tworzyć rzeczywistość w efektowny i interesujący sposób, "rozrywkowa TV dla magów"
- Ozydiusz ma słabość do Malii

Andrea powiązała się też przez bazę Wydziału - kto wypuścił informację, że Estrella została pobita? Odpowiedź: Jolanta Sowińska. Ona ma umiejętności astralnego spojrzenia wstecz; ale ktoś musiał jej powiedzieć, by tam była. Czyli Jolanta była tam, ZANIM cmentarz spłonął (to wypaliło wszystkie ślady).

Andrea następnie odniosła się do tego, kim są rodzice Malii / jakie mają powiązania z Mausami (Lub stosunek do nich). Rodzice Malii są tą częścią rodu, który raczej otwarcie gardzi rodem Maus. Nie ma nienawiści, raczej pogarda i patrzenie z góry na "niewolników demona". W przeszłości Malia miała podobne poglądy, jako dziewczynka. Jednak ostatnio weszła w okres buntu.
Tak samo rodzice Malii (i pośrednio Malia) pogardzają magami innych gildii. Parweniusze i plebs.

Niechętnie, Andrea połączyła się z centralnym systemem monitoringu hipernetu Świecy. Skorzystała ze swoich uprawnień (kod zielony - dostępny dla każdego agenta), by zobaczyć jaka była heatmapa połączeń hipernetowych Malii. Okazało się, że Malia miała zawsze dość dużo połączeń z Ozydiuszem, dużo więcej niż z rodzicami, ale po tym co zrobił Baltazar heatmapa pomiędzy rodzicami Malii, Malią a Ozydiuszem po prostu eksplodowała. Wcześniej Ozydiusz specjalnie nie rozmawiał z rodzicami Malii.
Też po tym, co zrobił Baltazar, Malia odrzucała wszelkie połączenia ze strony Baltazara. 

Andrea znalazła też jedno ciekawe połączenie. Artur Żupan. Tani mag. Tani drań. Niedawno przed całą sprawą z Estrellą, kontaktował się z Malią kilka razym jakoś po lockdownie KADEMu - i on inicjował pierwszy kontakt. Nie istnieje logiczna ścieżka, czemu Artur Żupan miałby kontaktować się z kimś takim, jak Malia Bankierz - to zupełnie inne światy.
Żupan przed kontaktem z Malią kontaktował się z niemałą ilością magów Świecy. Wyglądało, jakby czegoś szukał. Tak jakby... normalna aktywność Żupana, nagle mnóstwo różnych magów, potem pauza, potem Malia. Przede wszystkim kontaktował się z Zajcewami.

Ostatnią osobą, która potencjalnie dostarczyła mu informacje był Remigiusz Zajcew. Mag Zajcewów i mag Świecy. 

Andrea przygotowała kilka przykładowych urządzeń - sprzęt rozbrajający pułapki (terminusowy) by Andrea mogła wejść Żupanowi do nory i poczekać w jego norze na Żupana. Tam chce go przesłuchać.
Grunt to wywrzeć wrażenie.
Sęk w tym, że Żupan miał pułapki które są całkowicie nielegalne i Andrea weszła tam nieświadoma tego. Zorientowała się, że są tam niewłaściwe pułapki i ochroniła kluczowe części ciała, ale nadal straciła przytomność. 

Obudził ją kajający się Żupan. Żupan nie jest głupi; nie zrobiłby niczego terminusce, więc gorąco ją przeprasza. Nazywa ją "królową" i w ogóle. Jest bardzo miły i obiecuje, że nikomu nic nie powie. Andrea powiedziała mu, że on może wiedzieć coś co ona chce; Żupan z radością pomoże we wszystkim. Andrea zaczęła od Malii Bankierz. Żupan się skurczył wewnętrznie.

Andrea zaczęła go przesłuchiwać. Żupan nie bronił się specjalnie - powiedział Andrei, że pewien mag dostarczył mu holokryształy z różnymi bitwami i akcjami terminusów. W pewnym momencie mag zapytał, czy Malia ogląda je ze swoim chłopakiem - tak - i Żupan wtedy dostał kolejne holokryształy. Na kilku pojawiła się Estrella Diakon. Na jednym z nich była akcja, w wyniku której Estrella wygrała, ale straciła na pewien czas moc i została ciężko uszkodzona.

Tu Andrea zamrugała. Nie było takiej akcji - stan Estrelli przecież spowodowała Andrea. Oczywiście, nie powiedziała tego Żupanowi.

Żupan został zapytany, czy ma kopie kryształów. Mag zaczął przepraszać - MYŚLAŁ że ma, ale nie skopiowało. Poszukał i okazało się, że KADEMowe kryształy są odrobinę inne. Czyli gość miał kryształy z KADEMu. Jeśli to był Zajcew (a tak się zachowywał), to na KADEMie nie ma dużo Zajcewów teraz jak jest lockdown - wszystko wskazuje na Zajcewskiego detektywa z KADEMu, Ignata Zajcewa. Ewentualnie to ktoś wynajęty przez Silurię Diakon. 

Andrea zostawiła Żupana ciężko wystraszonego. Nikomu nic nie powie.

Andrea wyszła z założenia, że Malia jest w to trochę bardziej zamieszana. Poszła spróbować potwierdzić to założenie - bezpośrednio do Kropiaktorium. Odpowiedział na pytania Andrei odnośnie śledztwa - Baltazar Maus kupił je osobiście, są to typowe artefakty imprezowe z tej rodziny. On płacił, ogólnie: tylko on jest powiązany z tymi artefaktami. Nie, nie był wcześniej nigdy klientem.
Na holokryształach Żupana były akcje, gdzie terminus używał artefaktów codziennych...
Kropiak dodał też, że jeden artefakt użyty przez Baltazara był kupiony przez Malię, całkiem niedawno, jako trwalszy. Andrea przypomniała sobie, że akurat TAMTEN artefakt był użyty jako jeden z trzech na jednym z holokryształów. Zapytany, Kropiak dodał, że tak - Malia kupiła trzy. Tak jakby chciała zobaczyć, czy faktycznie da się to zrobić.
Pikanterii dodaje fakt, że to był holokryształ gdzie w roli terminusa była Quasar.

Andrea się uśmiechnęła. Kropiak nie ukrywa obecności Malii - a jednak ktoś ją ukrył. W raporcie o Malii niczego nie było. Kropiak powtórzył wszystko co mówił w raporcie - tylko Malia "zniknęła".

Andrea zakasała rękawy i siadła do danych sił specjalnych. Chciała znaleźć, kto za tym stoi, kto fałszuje te raporty. Zajęło jej to chwilę, ale dostała odpowiedź - w całej tej linii Ozydiusz Bankierz faktycznie uczestniczył. I najpewniej on wyciszył te raporty.
Andrea uniosła brew. Nie wypada.

To spowodowało, że Andrea bardzo poważnie przyjrzała się Ozydiuszowi. Okazało się, że Ozydiusz często z kuzynką działał. Przebywał. Nic szkodliwego czy nielegalnego; ale był bliżej Malii niż jej rodzice.
Wyszło coś ciekawego - Ozydiusz dostaje SAME terminuski. A w papierach ma, że nie lubi pracować z kobietami; jest szowinistą. A dostaje same czarodziejki od czasu przyjęcia Judyty Karnisz. 
Przydziały podpisywane są przez Bogdana Bankierza. Czyli podpadł jakiemuś kuzynowi.
O - nieźle. Bogdan Bankierz ma powiązania z Malią. A dokładniej: Malia miała zostać jego żoną. Ma... 47 lat.

Jakie Ozydiusz ma stosunki z Mausami? Cóż, terminus z rodu Mausów ma z nim dobrze. Na terminusów nie patrzy pod kątem rodów w ogóle. Do kobiet rodu Maus ma słabość; swego czasu nawet był w związku z czarodziejką rodu Maus. Od czasu zniknięcia Karradraela jego poziom sympatii do tego rodu jedynie wzrósł. I Andrea widzi po ruchach Ozydiusza w bazach danych, że Ozydiusz próbuje jakoś łagodzić całą sprawę Baltazara i Estrelli. Próbuje doprowadzić do tego, by chłopak nie ucierpiał za bardzo.
To Estrella prze w kierunku na ukaranie Baltazara a Ozydiusz próbuje to jakoś łagodzić.
Miło z jego strony.

Wszelkie protesty Ozydiusza powiązane z tym, że dostaje same kobiety były zawsze obcinane w zarodku. Bogdan nadużywa swojej władzy, ale Ozydiusz tego nie eskaluje do Świecy; to sprawa rodu Bankierz.

Dobrze. Z perspektywy Andrei - czas porozmawiać z Ozydiuszem.
Agentka Wydziału Wewnętrznego (Andrea) wyczekała, aż tien Ozydiusz Bankierz nie będzie miał niczego ważnego w kalendarzu i zaprosiła go do pokoju przesłuchań SŚ. Lord terminus się zgodził. Ku zadowoleniu Andrei, pojawił się punktualnie. Powiedział, że będzie współpracować, naturalnie.
Zapytany o Estrellę Diakon, Ozydiusz westchnął. Ma z tego powodu poważny problem; zwłaszcza, że Jolanta Sowińska zrobiła taki a nie inny artykuł na ten temat. Andrea zauważyła, że Ozydiusz zaangażował się w ten temat. Lord terminus odparł, że ona wie najlepiej - w końcu chroni ród Mausów. Też odrobił pracę domową.

Andrea powiedziała Ozydiuszowi, że taki atak na terminusa oznacza, że sam najpewniej młody Maus tego nie wymyślił. Ozydiusz potwierdził - jakkolwiek w papierach nie wszystko się zgadza (potwierdzając zarzut Andrei), prawda jest poza dokumentami. Ozydiusz jeszcze nie wie, kto to, ale wie, że Bogdan Bankierz (ten co wysyła mu same kobiety) nie jest z tym powiązany; Ozydiusz zapytał go osobiście. Wygląda na przekonanego, że Bogdan by nie kłamał. Andrea nacisnęła. Ozydiusz powiedział, że to sprawa wewnętrzna rodu i przedłożył to rodowi. Andrea powiedziała, że to przekracza ród. Ozydiusz podniósł status lorda terminusa, Andrea podniosła poziom śledztwa. Ozydiusz podniósł honor rodu i swój osobisty, Andrea zażądała odpowiedzi. Ozydiusz się wściekł na Andreę, ale odpowiedział: podniósł ten temat na prezydium Bankierzy i Bogdan oraz Ozydiusz mieli przeczytaną pamięć i skan wsteczny, żeby udowodnić, że żadne z nich nie miało nic wspólnego z tą sprawą. Z uwagi na to wszystko, zarówno Ozydiusz jak i Bogdan musieli zapłacić niemałą sumę do skarbu rodu Bankierz. Czyli Ozydiusz zaeskalował do seirasa.

Dla Andrei było to wystarczające - ani Ozydiusz, ani Bogdan nie stoją za tą sprawą. Więcej, zarówno Ozydiusz jak i Bogdan zostali przez seirasa zobowiązani do rozwiązania tej sprawy przez przekierowanie odpowiednich środków; w wypadku negatywnego rozwiązania, Ozydiusz zapłaci dużą karę za straty wizerunkowe wobec rodu Bankierz za niekompetencję agentów pod jego dowodzeniem (Estrella) a Bogdan drugą taką karę za przekierowanie Estrelli i za zabawę przydziałami co wpływa niekorzystnie na ród Bankierz.
To nie są małe kary finansowe. Są procentowe wobec przychodów (nie zysków).

Ozydiusz musiał przyznać kobiecie, że ród go ukarał za niekompetencję. A jeszcze przecież w tle jest sprawa z Malią. Nic dziwnego że jest wściekły. Nie mówi tego na głos, ale po jego ruchach to widać; uważa, że to atak w niego.

Andrea spytała Ozydiusza, kto wiedział, że Estrella jest niedysponowana. Właściwie każdy terminus. Ale to, że wyszła wcześniej ze szpitala, tego już nie każdy wiedział - tylko ci, co byli na miejscu i ci, co mają dostęp do kartotek. On sam nie wiedział, że wyszła wcześniej; nie sprawdzał i nie pytał. Po co miałby.

Andrea spytała Ozydiusza odnośnie holokryształów; czy są tam jakieś dodatkowe ślady. Ozydiusz zaczął już śledztwo; ma wszystkie holokryształy pod ręką. On sam z Malią przeglądał holokryształy Świecy z bardziej jawnych akcji terminusów. Malię to bardzo fascynowało od zawsze; chciała być "korespondentem wojennym", ale Ozydiusz wybił jej to z głowy; powiedział, że ważniejsze jest kierowanie się ku interesującej fikcji. Nikt nie chce znać prawdy, a odrobina dobrej fikdji i nadziei to podstawa działania gildii. Malia to kupiła.
Andrea nie podzieliła się z nim myślą, że chyba po prostu nie chciał kolejnej terminuski pod skrzydłami.
Ozydiusz jeszcze powiedział, że w jego śledztwie wypłynął Ignat Zajcew - detektyw KADEMu. Zdaniem Ozydiusza jakiś cholerny mag Świecy schował się za KADEMem wiedząc, że przecież nikt KADEMu nie ruszy; a na pewno nie w takiej sytuacji politycznej.
A dokładniej - Ignat, przed tą sprawą (przed wyjściem Estrelli, przed pierwszym holokryształem itp) - Ignat interesował się Malią.

Niby Ignat to mag który pracuje tylko dla KADEMu, ale Ignat to Zajcew...
Ciekawe - Żupanowi też Ignat przekazywał holokryształy osobiście zgodnie z danymi Andrei.

Andrea powiedziała też Ozydiuszowi o Żupanie; ona z nim już rozmawiała. Ozydiusz powiedział, że w takim razie on nie zamierza. Andrea powiedziała Ozydiuszowi o tym, że Żupan przeprowadził własne śledztwo i w nim wypłynął Ignat Zajcew.
Ozydiusz przyjął to do wiadomości. Uśmiechnął się zimno. Teraz to się robi interesujące - przed sekundą Ozydiusz cross-checkował lokalizacje Ignata Zajcewa w przeszłości i znalazł powiązanie - Ignat Zajcew był w szpitalu, gdy opuściła go Estrella Diakon. Czyli on mógł coś usłyszeć.

Ozydiusz powiedział, że nie istnieje ŻADEN znany mu powód, dla którego Ignat Zajcew miałby kiedykolwiek atakować Malię, Estrellę lub jego. Po prostu nie ma powodu. Nic, co może znaleźć. Sam fakt, że Ignat nie lubi Świecy jest niewystarczający.

Ozydiusz nie ma uprawnień, ale Andrea ma; co Ignat robił w szpitalu Świecy? Był w odwiedzinach u tien Elizawiety Zajcew; po raz kolejny. Elizawieta jest jego przyjaciółką z dzieciństwa. Jest terminuską, oddelegowaną chwilowo do Mileny Diakon. Podczas jednej z akcji, Milena ciężko skaziła Elizawietę. Tien Zajcew chciała pomóc Milenie, a ta ją ciężko Skaziła, wstrzykując jej swoją krew. I Elizawieta jest w trakcie ciągłej regeneracji; błąd terminusów Świecy i dobre serce Elizawiety doprowadziły do tego, że Milena prawie zbiegła, ale Elizawieta ją zatrzymała. Wysokim kosztem; jest w szpitalu już od miesiąca, ale niedługo wyjdzie (i trafi do Ozydiusza).

Andrea sprawdziła, czy Ozydiusz wie, że dostał KOLEJNĄ terminuskę. Wie. Dostał ją za Tamarę... zgubił terminuskę, więc dostał następną. To ostatni prezent Bogdana Bankierza dla Ozydiusza...

Andrea westchnęła. Gdzieś tam jest siedmiu magów przekształcanych w Mausów. I ona nic o tym nie wie (gdzie są, kim są). Może Ozydiusz coś o tym doda? Nie, Ozydiusz nie wie o żadnym zniknięciu żadnego maga. To implikuje, że albo przekształcani NIE są z okolic Kopalina, albo, że nawet są spoza Świecy. I Andrea stawiałaby na to drugie...
I tak plan Oktawiana jest skazany na porażkę; bez Karradraela nikt nie będzie kontrolować tych siedmiu nieszczęsnych magów, po prostu zostaną Mausami wbrew swej woli. I na co to wszystko? Po prostu powtórzy się sprawa z Mileną.

Ozydiusz i Andrea ustalili, jak wygląda sprawa. Malia Bankierz w ogóle nie wypłynie w tej sprawie. Andrea oczekuje, że Malia zostanie ukarana; Ozydiusz powiedział, że on się tym zajmie i że Malia nie uniknie kary. Przy reputacji Ozydiusza... tak, powinno wystarczyć.
Oboje też stwierdzili, że trzeba znaleźć winowajcę i to powinno pomóc Baltazarowi Mausowi. Bo niestety, chłopak zbłądził.

Ozydiusz wychodzi ze spotkania z nastawieniem negatywnym wobec Andrei. Ale nie bardzo negatywnym.

# Streszczenie

Aleksander Sowiński (szef Wydziału Wewnętrznego na Śląsku) zażądał od Andrei odkrycia problemu Baltazara Mausa. Pierwszego maga Mausów z Rodziny Świecy od dawna (ta sprawa przywróciła ostrą dyskusję w Świecy nt Mausów/Rodziny). "Matką" Baltazara jest Elea Maus - lekarka. Wskazała Malię Bankierz (dziewczynę Baltazara) i jej wujka Ozydiusza. Żupan dostarczył jej kryształy z KADEMu które pomogły stworzyć Baltazarowi plan. I wszystko wskazuje na Ignata Zajcewa z KADEMu. Ozydiusz próbuje chronić Malię i Baltazara, Estrella chce skrzywdzić Baltazara. Czyli najpewniej Ignat... ale nikt nie rozumie po co.

# Zasługi

* mag: Andrea Wilgacz, która zaczyna dostrzegać powiązania między: Oktawianem, Mileną, Szlachtą, Baltazarem Mausem, Elizawietą, KADEMem? Podejrzewa kogoś z zewnątrz, lub odległą siłę w Świecy.
* mag: Aleksander Sowiński, przełożony Wydziału Wewnętrznego na Kopalin, który zlecił Andrei zadanie z Baltazarem Mausem. Tak nagle.
* mag: Elea Maus, matka Baltazara Mausa z Rodziny Świecy; przeprowadziła małe śledztwo w sprawie dziewczyny Baltazara, Malii.
* mag: Malia Bankierz, 16-letnia dziewczyna Baltazara; Ozydiusz jest jej wujkiem. Uwielbia holokryształy, terminusów i jest arystokratką.
* mag: Remigiusz Zajcew, z którym kontaktował się Artur Żupan i który dostarczył kluczowej informacji wsypując Ignata. * mag Zajcewów i Świecy.
* mag: Baltazar Maus, wielki nieobecny na misji, którego działania grożą rozsypaniu Rodziny Świecy wobec Mausów.
* mag: Artur Żupan, który przekazywał Malii Zajcew materiały od tajemniczego * maga; zidentyfikował tego * maga jako Ignata. Współpracuje z Andreą w pełni.
* mag: Adrian Kropiak, nic nie ukrywa przed Wydziałem Wewnętrznym, powiązał Malię Bankierz z Baltazarem Mausem dla Andrei.
* mag: Bogdan Bankierz, wysoko postawiony w Świecy, którego gierki w "Ozydiusz dostanie same laski" zagroziły Świecy.
* mag: Ozydiusz Bankierz, chroni Malię, prowadzi śledztwo, uważa, że sprawa Baltazara wymierzona jest w niego i ogólnie niechętnie współpracuje z Andreą. Ale to robi. Eskalował Bogdana i siebie do seirasa.
* mag: Elizawieta Zajcew, terminuska o dobrym sercu, która zatrzymała ucieczkę Mileny Diakon i na miesiąc trafiła do szpitala. Trafi pod Ozydiusza.
* mag: Milena Diakon, też nieobecna, która - jak się okazuje - potrafi zdestabilizować * maga przez połączenie Krwi oraz katalizy.
* mag: Ignat Zajcew, wielki nieobecny, który non stop kręci się w sprawie Baltazara: a to koło Malii, a to koło Elizawiety, tu koło Żupana...

# Lokalizacje:

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Centrum
                        1. Kompleks centralny Srebrnej Świecy
                            1. Pion dowodzenia Kopalinem
                                1. Sala przesłuchań 4B
                    1. Dzielnica Owadów
                        1. Kropiaktorium
                        1. Nora Artura Żupana
                    1. Dzielnica Mausów
                        1. Mrówkowiec Józef
                            1. mieszkanie Elei i Rufusa Mausów 
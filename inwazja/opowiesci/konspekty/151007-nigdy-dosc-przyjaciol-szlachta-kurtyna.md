---
layout: inwazja-konspekt
title:  "Nigdy dość przyjaciół: Szlachta i Kurtyna"
campaign: powrot-karradraela
gm: żółw
players: kić, dust
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [150920 - Sprawa Baltazara Mausa (AW)](150920-sprawa-baltazara-mausa.html)

### Chronologiczna

* [150920 - Sprawa Baltazara Mausa (AW)](150920-sprawa-baltazara-mausa.html)

### Logiczna

* [151001 - Plan ujawnienia z Hipernetu (HB, DK)](151001-plan-ujawnienia-z-hipernetu.html)

### Punkt zerowy:

Ż: Jaką korzyść z tej całej sytuacji może uzyskać KADEM przeciwko Świecy?
B: KADEM uzyskuje przyczółek na obszarze pomiędzy KADEMem a Srebrną Świecą.
Ż: Jaka akcja z ukrycia na pewno MOCNO uderzyła w Srebrną Świecę?
K: Ktoś z członków Świecy zaatakował Powiew w taki sposób, że Powiew ma silne prawo do rekompensaty.

Ż: Co bardzo cennego dla Blakenbauerów wie Vladlena czego nie chciałaby Emilia by oni wiedzieli?
D: Może dać Blakenbauerom dostęp do czystego Węzła niewielkim kosztem.
Ż: Dlaczego Siluria i Hektor wrócili do ściślejszej współpracy?
K: Bo Hektor pomaga Kurtynie (pokazuje jego intencje) i jako prokurator może wyjaśnić sprawę poligonu KADEMu.
Ż: O zdobycie czego poprosiła Emilia Hektora?
D: Jakiś konkretny magitech zbudowany przez Tamarę (Wiktora).
Ż: Dlaczego to coś jest gamechangerem całego konfliktu?
K: Bo ten konkretny magitech poda tożsamości i cele wielu członków Szlachty.


## Kontekst misji:

Po pokazaniu siły przez Szlachtę magowie Kurtyny na obszarze hipermarketu NeWorld lekko wpadli w panikę. Upadek Vladleny, która zagroziła Wiktorowi Sowińskiemu Spustoszeniem jedynie pogorszył sytuację. Spustoszenie przechwycił ród Blakenbauerów i Hektor dał Kurtynie gwarancje bezpieczeństwa i azyl w Rezydencji, zakładając optymistycznie, że przecież Szlachta nie zamierza pchać się do Rezydencji, bo wszystkie rzeczy jakie Szlachta przeciw Kurtynie robiła broniły interesów Szlachty - a aktualna Kurtyna im już nie zagraża.
Edwin zdecydował się pomóc Vladlenie. Osobiście zadbał o jej pamięć... bo Hektor mu ją oddał jako lekarzowi. 

Adrian Murarz, mag Szlachty, też trafił do Rezydencji.
## Misja właściwa:
### Faza 1: Hektor wielki polityk

Hektor jest szczęśliwy. Jego sojusznicy dla odmiany nie wykazują szkodliwej proaktywności.
Czas przesłuchać Adriana Murarza - członka Szlachty, który był magiem Kurtyny i przyjacielem Vladleny. Gość dostał środki i jest zdezorientowany. Hektor dodatkowo się nie cacka i wchodzi twardo z magią - chce znać prawdę. Chce jeden raz w życiu wiedzieć na czym stoi. Hektor dodatkowo doprowadził do tego, że Adrian się poddał - przeszedł na stronę Blakenbauerów. Będzie agentem Blakenbauerów w Szlachcie lub Kurtynie lub czymkolwiek; jego wola jest całkowicie złamana.

Adrian Murarz powiedział co następuje:
- zna tylko Wiktora Sowińskiego i Dianę Weiner.
- zmienił stronę (Kurtyna -> Szlachta), bo Diana pokazała mu Prawdę. Emilia i 'lena chronią starego porządku. Różnica jest taka, że Szlachta umożliwia awans z uwagi na zasługi i na kompetencje; Kurtyna nie daje żadnych opcji awansu jeśli nie masz krwi odpowiedniego Rodu. Emilia była kiedyś czarodziejką Rodu Weiner, ale zniszczyła swój wzór; teraz jest Kołatką. Szlachta zakłada możliwość demokracji. Zakłada, że ogólnie zmiany są możliwe; wielkie Rody usztywniły Świecę. Szlachta też zakłada możliwość powołania nowych Wielkich Rodów.
- Adrian był ostrzeżony, że Kurtyna próbuje porwań magów Szlachty. Aktualnie Szlachta ma przykazane uważać i nie poruszać się nieuzbrojonym. Wiadomo też (Diana wbijała mu to do głowy godzinę), że Diana uważa, że Kurtyna spróbuje rozerwać ew. sojusz Szlachty i Blakenbauerów; Kurtyna interesuje się Blakenbauerami tylko ze względu na płaszczki.
- Szlachta chciała zrobić pokaz siły; normalnie nie uderzyłaby taką mocą. Ale stać ich na dużo więcej. Po co kosić trawę kombajnem.
- Szlachta ma dużo groźniejszych wrogów niż Kurtyna; min. Saith, ogólnie Świeca, stare Rody, arystokracja, Ci U Władzy. 
- O naturze Saith więcej wie Diana Weiner.
- Adrian nie został zmuszony do współpracy. Został przekonany. Sam próbował przekonać Vladlenę, ale ona zadziałała histerycznie.

Vladlena mówiła Hektorowi, że Adrian został zmuszony i złamany. Jak widać, Adrian sam się przyłączył do Szlachty...

Hektor zdecydował się też przesłuchać magów Kurtyny. Dlaczego walczą ze Szlachtą, co się złego stało, czemu Szlachta jest tak zła?
- Szlachta może rekrutuje wśród znaczących magów, ale plebs jest plebsem; oni dołączą lub będą służyć
- Wiktor Sowiński niewoli i wykorzystuje ładne czarodziejki. Jak to powiedział, "lubi zwierzątka". Nie ma dowodów, bo ma wszystko w kulturze strachu.
- Wiktorowi udało się nawet Izabelę i Tamarę zablokować przed atakiem przeciwko Szlachcie. Nawet one się nie odważyły.
- Szlachta ogólnie wspiera siebie przeciwko Świecy. Ciągła ekspansja wewnątrz Świecy.
- Szlachta dźga Świecę np. bijąc w KADEM; liczy na sanację, odrodzenie pod nowymi rządami.
- Szlachta ma wzniosłe ideały ale postacie Szlachty są wysoce koszmarne.
- Hektor uderzył w nich w taki sposób, by odsunęli się od Vladleny. Bardziej przysunęli się do Blakenbauerów.

Hektorowi udało się przekonać obecnych tu magów Kurtyny, że współpraca z Blakenbauerami jest opłacalna dla nich i wspiera zarówno ich cele jak i cele Hektora. Jest to duży wyczyn. Skaptował zarówno magów Kurtyny jak i maga Szlachty pod kontrolę i strefę wpływu Blakenbauerów.

Edwin przyszedł do Hektora. Powiedział, że jest w stanie powstrzymać te dopalacze wydawane przez Zajcewów z ramienia Vladleny; wyjaśnił też Hektorowi, że Blakenbauerowie byli kiedyś Wielkim Rodem ale potem uderzyła Klątwa i już nie są...
Z perspektywy Edwina korzystny byłby sojusz ze Szlachtą choćby z tego powodu. Jednak - zostawia politykę Hektorowi (sam w to nie wierzy).

W akwarium zabulgotała woda. Hektor niechętnie zdecydował się "odebrać akwarium". W środku pojawiło się piękne oblicze Diany. 
Diana spytała, czy Hektor już widzi czym naprawdę jest Kurtyna i jak daleko jest skłonna się posunąć. Hektor powiedział, że w sumie to tylko i wyłącznie Vladlena; Diana powiedziała, że na tym polega problem. Tylko Iza, tylko Tamara, tylko 'lena. Każde z nich robi co chce. I to jest cała Kurtyna - emocjonalna, broni starego porządku, metody jakie są. Sama Diana pokazała się jako bardzo niebezpieczna manipulatorka, która jest święcie przekonana o słuszności swego celu, o słuszności prokuratury (która nie pasuje Świecy - ale pasuje Szlachcie) i Diana sama odda siebie w ręce sprawiedliwości jak zakończy swoje działanie.
Chce się spotkać z Hektorem. W cztery osoby. Nieuzbrojone. Ona i Wiktor, Hektor z kimś. Poprosiła o zwrócenie Adriana Murarza; gdy Hektor nieporadnie próbował coś ugrać zaśmiała się i powiedziała, że może siedzieć w Rezydencji i zjadać jego obiady. W tej chwili nie jest jej potrzebny a jeśli Hektorowi na nim tam zależy, to niech go gości.
Hektor, nieco wbrew sobie, zgodził się na spotkanie. Niestety; ma dostać dowody i nagrania z akcji na terenie hipermarketu (przeciw Vladlenie), więc...

Hektor zadzwonił więc do Silurii, chcąc uniknąć porażki do Diany. Hektor poprosił Silurię o to, by spotkała się z nim w prokuraturze. Zaprosił ją do prywatnego pokoju. Siluria spytała, czy to pokój zabaw Hektora; mag błyskawicznie odmówił. Już w pokoju Siluria dowiedziała się od Hektora, że ów zdecydował się wybrać i współpracować z Kurtyną. Nie podoba mu się żadna ze stron, ale Kurtyna jest mniejszym złem.

Siluria od razu stworzyła pewien plan. Chce jednocześnie uwieść Dianę Weiner i Wiktora Sowińskiego. Doprowadzić do tego, by w trójkąciku z tą dwójką wyszły z Wiktora jego instynkty sadystyczne i żeby uderzył w Dianę; celem Silurii jest skłócenie ze sobą Diany i Wiktora.
Plan jest genialny i bardzo... Diakoński.
Jako, że Hektor JUŻ ma słabość do Silurii, Siluria postara się, by Diana i Wiktor to zauważyli. W ten sposób mogą uznać Silurię za drogę do Hektora... a Siluria ich zniszczy.

Siluria dała się z trudem Hektorowi przekonać, że pójdzie na spotkanie. Z trudem. Bo tak zabawniej.

Oktawian Maus przyszedł do Silurii i Hektora, do prokuratury. Poprosił Hektora o sprawiedliwość dla młodej terminuski, która z winy zaniedbań Świecy bardzo ucierpiała. Ze Skażonym Wzorem, pomiędzy dwoma Rodami, nie jest sobą ani nie-sobą. Mówimy o Milenie Diakon. Siluria słysząc "Diakon" aż podskoczyła na krześle. Oktawian został przez Silurię przyciśnięty z pomocą Hektora i wyszło co następuje:
- Oktawian chce pomóc Milenie. Naprawdę.
- Oktawian ma nadzieję, że Milena skonwertowana w Mausa zostanie Mausem i nie wróci do bycia Diakonem.
- I najważniejsze - Oktawian podejrzewa, że Milena Diakon miała kontakt z Karradraelem. Tylko on może tak zmienić wzór. I że Milena go zaprowadzi do Boga Mausów.

Silurię zmroziło aż do kości. Rozumie, czemu nie zrobiono konwersji w Mausa. Rozumie, czemu nie dopuszczono do niej Mausów. Jednocześnie nie chce, by dziewczyna niepotrzebnie cierpiała...
Hektor weźmie tą sprawę. Ale - jak uzgodnił z Silurią - żaden "normalny" Maus nie będzie miał z Mileną kontaktu a już na pewno nie Oktawian.
I teraz Hektor i Siluria rozumieją, czemu Oktawian pracuje ze Szlachtą. Co za to dostał. Dostał potencjał na kontakt z Karradraelem...

Szlachta jest psychiczna jeżeli bawi się takimi siłami w taki sposób... to gorzej niż Spustoszeniem w hipernet...

Nadszedł czas uzgodnienia lokalizacji spotkania. O dziwo, Diana zaproponowała las. Wtedy Hektora tknęło - ma taką piękną małą chatkę. Elegancka, wytworna, ustronna, nikt tam się nie pojawia. I - co najważniejsze - Hektor upewnił się, że nie ma tam Marcelina. Nie ma. Zaproponował tą lokalizację i Diana się zgodziła. Jej chodzi o to, by nie było to miejsce zbliżone do ludzi; będą mówić o rzeczach nie dla ludzi a Hektor jako prokurator może być śledzony przez Kurtynę (Szlachta jeszcze nie wie).

Hektor chce pokazać, że interesuje go wejście w sojusz, ale nie jest przekonany; jest podatny na to wszystko. Chce lepiej poznać Szlachtę. To spotkanie to nie jest chwila moment, to mają być dwa dni. Weekendowy wypad. Diana się zgodziła.

Siluria się przedstawiła jako Sabina; nie chce, by wiedzieli kim jest i jak działa. Przygotowała się szczególnie. Lekka zmiana wyglądu (makijaż, magiczna technika). Feromony, dopasowane do ich rodów (i bardzo delikatne). Mały research odnośnie gustów - Wiktor lubi dziewczyny silne i zdecydowane; łamanie ich sprawia mu większą przyjemność. Diana natomiast lubi mężczyzn spokojnych, cichych, cierpliwych... ale takich, którzy zrobią pierwszy ruch. Siluria ubrała się też lekko w kierunku na kod kulturowy i kwiatowy Świecy. Też coś lekko wyzywającego i dla Wiktora i dla Diany.

Siluria i Hektor przybyli wcześniej; jako gospodarze napalili w kominku etc. Diana i Wiktor w końcu przybyli.

Diana i Wiktor powiedzieli następujące rzeczy Hektorowi i Silurii:
- laboratorium Blakenbauerów jest dyskretne i bardzo skuteczne; wszyscy wiedzą, że tam jest i nikt nie wie co się dzieje
- Szlachta jest zainteresowana jakimś sojuszem handlowym (receptury za produkcję); wiedzą, że Hektor nie chce dołączyć
- Tymotheus Blakenbauer jest poważnym problemem i jest w pobliżu
- Ród Blakenbauerów został zdaniem Diany skrzywdzony; technologia posłużyła do produkcji Saith.
- Saith są bardzo trudne do zniszczenia i zatrzymania.
- Blakenbauerowie są zgodni z profilem Szlachty.

Diana: 15 sukces -> zainteresowanie
Wiktor: 10 sukces -> zainteresowanie, chęć odbicia Silurii od Hektora

Podczas całej tej rozmowy Siluria jest filuterna, zaczepna ale nie frywolna. Wzbudziła zainteresowanie Diany i wysoką chęć Wiktora odbicia Silurii.
Szlachta uzyskała to, że mogą wyprodukować u Hektora, u Blakenbauerów to czego potrzebują (jeśli jest zgoda Ottona i innych Blakenbauerów) dyskretnie.

Czas na przejażdżkę konną po lesie. Przyjemnie spędzony czas.

Potem się rozdzielili - Hektor i Wiktor pojechali na polowanie a Siluria i Diana zostały same.

Diana spytała Silurię, czy ta jest Diakonką - Siluria potwierdziła. Diana "rozumie" - Siluria dołączyła do Hektora by coś osiągnąć. Ale co? Siluria powiedziała, że chodzi o Poligon KADEMu i dała się zidentyfikować. Dla Diany Siluria jest fascynująca... na KADEMie rzadko o takie zachowanie.

Głowica zgłosiła Silurii, że dookoła budynku zbiera się banda golemów. Inna banda wygląda na to, że poluje na Wiktora i Hektora.
Siluria pozwoliła głowicy pożreć (zasymilować) jednego i pójść w kierunku na źródło. 

Diana przerażona, czy Siluria i Hektor ich zdradzili. Siluria - nie, ten atak jest niespodziewany. Dała szybko znać Hektorowi, po czym zaciągnęła Dianę do stajni; niestety, golemy ze strzałkami usypiającymi otoczyły już budynek. Używając magii lifeshape Siluria zrobiła decoye używając swojej krwi i włosa Diany modyfikując konie i puściła konie z nadzieją, że golemy za nimi polezą. Udało się - golemy poszły za końcmi a Siluria i Diana schowały się w budynku tak, by nikt ich nie znalazł. 
Opinia Diany o Silurii - zdecydowanie wzrosła. To nie jest tylko "zwykła Diakonka z KADEMu".

Tymczasem Hektor i Wiktor jechali konno na polowanie. Rozmawiali o dziewczynach; rozmowa przeszła na Silurię a Hektor jedynie Wiktora podkręcał "Siluria jako trofeum" itp. Ogólnie, męska rozmowa. W pewnym momencie Wiktor zauważył coś dziwnego i zrzucił Hektora z konia, po czym przeturlał się z nim do rowu (i nazwał go "okop"). Nad nimi przeleciała strzałka z trucizną usypiającą. Golemy, jakie Dionizy widział broniące hipermarketu (stąd Hektor wie, że Kurtyny)...

Wiktor zapytał Hektora, czy ten zdradził ich Kurtynie. Hektor powiedział, że nie. Wiktor zaczął nakładać na siebie błoto i utwardzać je magią tworząc pancerz, po czym zaproponował wyzwanie - kto zniszczy więcej golemów (i wtedy Hektor dostał wiadomość od Silurii). Hektor się zgodził, Wiktor uruchomił umiejętności Katai i zaszarżował na golemy. Hektor stwierdził, że bezpieczniej będzie strzelać do golemów zagrażających Wiktorowi; niech Wiktor wygra. W końcu sprzyja to Silurii i jemu. Wiktor jeszcze krzyknął jednemu z golemów w twarz, że wie, że wysłał je tien Edward Sasanka i że on zapłaci za to.

W tym czasie głowica wysłana przez Silurię do źródła golemów znalazła jeden relay, potem drugi, potem maga kontrolującego golemy (Edwarda Sasankę) i go ogłuszyła. Niebezpieczeństwo zostało zneutralizowane. Głowica zabrała Sasankę do jednej z dziupli KADEMu, by Siluria mogła z nim porozmawiać w niedalekiej przyszłości.

Oczywiście, na tym cała rozmowa się zakończyła; to miejsce nie jest już bezpieczne. Diana przekazała Hektorowi dowody (nagrania z walki w hipermarkecie, że Vladlena miała dostęp do Spustoszenia, że Vladlena planowała skazić hipernet Spustoszeniem) i powiedziała, że Hektor ma wszystko co jest potrzebne by rozpocząć proces przeciwko Vladlenie. Wiktor bardzo zainteresował się Silurią a i Diana stwierdziła, że warto rozważyć ściągnięcie pięknej KADEMki do Szlachty.

Na zakończenie, Siluria udała się do KADEMowego autonomicznego schronu; bezpiecznego miejsca, gdzie schowała tien Sasankę. Dopytała go, skąd to wszystko się wzięło, skąd wpadł na pomysł, żeby zaatakować; dowiedziała się, że dostał wiadomość na hipernecie od terminusa sympatyzującego z Kurtyną. A mag ów nazywa się Aurel Czarko.

## Dark Future:
### Actors:

Vladlena Zajcew (Warmonger)
Wiktor Sowiński (Dominator)
Diana Weiner (Cult Speaker)
Edwin Blakenbauer (Sentinel)

### Faza 1: 

- Edwin znajduje dziwną wiedzę Vladleny o Blakenbauerach; czyści to
- Hektor przesłuchuje magów Kurtyny
- Szlachta proponuje spotkanie Hektorowi odnośnie przyszłej współpracy
- Oktawian proponuje pomoc Milenie Diakon

# Streszczenie

Poszerzenie: Szlachta jest zła i się bawi magami, Wiktor lubi czarodziejki jako "zwierzątka", Adrian przyłączył się do Szlachty z własnej woli. Zdaniem Edwina, Blakenbauerowie powinni wesprzeć Szlachtę, zdaniem Hektora - Kurtynę. Oktawian Maus przyszedł do Rezydencji prosić o sprawiedliwość dla Mileny, niech Świeca ją wyda. On jej pomoże. Oktawian podejrzewa kontakt z Karradraelem i chce go odnaleźć. Spotkanie w chatce w lesie; Szlachta załatwiła "możemy wykorzystywać wasze lab". Nagle - atak. Edward Sasanka, napuszczony przez Aurela Czarko (podobno). Szczęśliwie, GS Aegis go unieszkodliwiła. Diana przekazała Hektorowi materiały o zbrodniach Vladleny i poprosiła o start śledztwa.

# Lokalizacje:

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Centrum
                        1. Prokuratura
                    1. Obrzeża
                        1. Rezydencja Blakenbauerów
                    1. Las Stu Kotów 
                        1. Autonomiczny Schron KADEMu
                        1. Leśna chatka Blakenbauerów

# Zasługi

* mag: Hektor Blakenbauer, który skaptował * magów Szlachty i Kurtyny jednocześnie; wybrał Kurtynę, lecz wykazał nienaganny zmysł polityczny.
* mag: Siluria Diakon, czarująco niebezpieczna, która próbuje uwieść jednocześnie Dianę i Wiktora by ich skłócić i zniszczyć. Też: genialny manewr unikający golemów.
* mag: Wiktor Sowiński, niepowstrzymany w walce i zdecydowanie zainteresowany Silurią; ma nienaganne maniery i chęć współpracy z Hektorem.
* mag: Diana Weiner, fanatyczna Sprawie, przyznała Hektorowi, że chce by prokuratura wróciła. Wybitna manipulatorka. Zainteresowana Silurią.
* mag: Adrian Murarz, który dał się przekonać Hektorowi że praca dla Blakenbauerów jest w jego najlepszym interesie.
* mag: Edward Sasanka, porywczy * mag Kurtyny, który zaatakował golemami usypiającymi chcąc uderzyć w Szlachtę i Blakenbauerów; padł do GS Aegis 0003.
* mag: Aurel Czarko, terminus spoza okolicy, który poinformował porywczego * maga Kurtyny o potencjalnym słabym punkcie Szlachty.
* vic: GS "Aegis" 0003, głowica, która jakkolwiek za późno zorientowała się o ataku to jednak zrehabilitowała się eliminując (ogłuszając) głównodowodzącego * maga (Sasankę)
* mag: Edwin Blakenbauer, który jakkolwiek wolałby współpracę ze Szlachtą z uwagi na korzyści, zostawia Hektorowi wolną rękę (poza manipulacją pamięcią Vladleny).
* mag: Vladlena Zajcew, wielka przegrana tej misji. Edwin wyedytował jej pamięć, zablokował dopalacze szukające a do tego montowane jest przeciw niej oskarżenie.
* mag: Oktawian Maus, który poprosił Hektora o interwencję w sprawie Mileny Diakon; przyciśnięty przyznał, że chce znaleźć Karradraela przez Milenę.
---
layout: inwazja-konspekt
title:  "Wołanie o pomoc"
campaign: powrot-karradraela
gm: żółw
players: kić, til, viv
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [140503 - Wołanie o pomoc (AW, Til (temp), Viv (temp))](140503-wolanie-o-pomoc.html)

### Chronologiczna

* [120507 - Preludium: Historia świata](120507-preludium-historia-swiata.html)

## Wątki

- Szlachta vs Kurtyna
- Rodzina Świecy

## Misja właściwa:

Wszystko zaczęło się bardzo dziwnie. Wpierw Andrea została wynajęta przez maga - wybitnego lekarza (Kornelię Modrzejewską) do znalezienia zaginionej czarodziejki, Klotyldy Świątek. Był jeden warunek - Kornelia chciała pomóc w akcji. Andrea miała swoje obiekcje, jednak pojawiły się pewne, powiedzmy, sugestie z góry a Kornelia miała opinię świetnego lekarza i kompetentnego maga, więc Andrea, choć z wahaniem, się zgodziła. No ale skoro to tylko kwestia odnalezienia kogoś, to co może pójść nie tak?

Potem Andreę wezwał tien Agrest, dowódca operacyjny jej skrzydła. Dostał polecenie od swoich przełożonych, by odnaleźć zaginioną Klotyldę Świątek - czarodziejkę, która zniknęła z synem podczas burzy magicznej. Poszperał trochę w raportach i wyszło mu na to, że nie jest to jedyny przypadek tego typu... w związku z tym zdecydował się oddelegować do tego najlepszego terminusa analitycznego jakiego ma. Jednocześnie dostał odgórne wymagania, by przyjąć na akcję terminusa-najemnika, Vuko Milica. Serbski terminus dołączył do Świecy, jednak nadal był swego rodzaju outsiderem. Oraz dostał zadanie zaakceptować udział Kornelii Modrzejewskiej. Było to niezgodne z procedurami i Agrest bardzo protestował, jednak nie mógł odmówić wykonania bezpośredniego rozkazu. Mógł jednak wysłać do pomocy im maga, któremu bardzo ufał - Andreę.

Kornelii bardzo zależało, by dostać się do Klotyldy Świątek przed wszystkimi innymi, bo Klotylda wiedziała coś na jej temat, o czym Kornelia bardzo chciała by nikt nie wiedział. Vuko natomiast zaczęła palić się kryjówka - albańska mafia odnalazła częściowo miejsce jego działania, więc opieka SŚ stała się rzeczą bardzo pożądaną. Plus, zapłacono mu by się tym zajął i by zachował absolutną bezstronność.

Jeszcze podczas briefingu Andrea powiedziała Agrestowi, że coś tu jest zdecydowanie nie tak (wygrany konflikt na wydobycie informacji). Ten się zgodził i podzielił się z Andreą swoimi podejrzeniami - w tej okolicy na przestrzeni pół roku zniknęło już dwóch innych magów wcześniej. Oboje byli w jakiś sposób powiązani z "domami zastępczymi" młodych magów. I oboje zniknęli czy zaginęli podczas burzy magicznej. Z uwagi na bardzo ograniczoną ilość terminusów mogących się czymkolwiek zająć po Inwazji, zostało to nie znalezione dopóki Agrest nie zaczął grzebać. Tak więc ostrzegł Andreę i podzielił się z nią tymi wiadomościami.

O co chodzi w domach zastępczych dla młodych magów SŚ (nazwa: "Rodzina Świecy"): Rodzina Świecy jest procedurą jaka powstała w okolicach XVII wieku i dzielnie trwała aż do Zaćmienia i chwilę po Zaćmieniu. Przestało to działać na większą skalę po Inwazji, choć w niektórych miejscach nadal trwa. Rodziny Świecy polegają na tym, że jeśli w rodzinie ludzkiej (lub niewłaściwej) pojawi się mag, będzie on odebrany rodzicom i przekazany "rodzicom zastępczym". Tam dostanie nową tożsamość i będzie wdrażany jako swój. W ten właśnie sposób potężne rody magiczne (Diakon, Maus, Bankierz...) są w stanie zawsze mieć młodych magów. Z punktu widzenia Andrei jest to działanie nieetyczne i okrutne, Agrest uważa, że jest to etyczne, bo zgodne z procedurami i potrzebami Świecy.

Jeszcze zanim Vuko spotkał się z resztą zespołu, skupił się na tym, by dowiedzieć się jak najwięcej odnośnie Klotyldy Świątek (wygrany konflikt). Dowiedział się o niej absolutnie wszystkiego - Klotylda Świątek była jedną z Rodziny Świecy, jedną z rodziców zastępczych. Zrezygnowała z bycia jedną z Rodziny Świecy po Zaćmieniu, kiedy jej mąż utracił moc magiczną. Ku żalowi magów Świecy, gdyż z mężem wychowywali dzieci z sercem i miłością do tego stopnia, że aż faktycznie nazywały ich "Prawdziwą Rodziną". Wychowywali dzieci w duchu tego czym Świeca powinna być, a nie tego czym była obecnie. Dowiedział się też wszystkiego o jej sposobie pracy, o jej działaniach... jej zniknięcie nie do końca miało sens; nie miała wrogów. Udało się mu również namierzyć telefony komórkowe zarówno jej, jej męża i ich syna. Wszystkie trzy w jednej lokalizacji - niewielkiej miejscowości o nazwie Myślin.

Kornelia również nie próżnowała - ona skontaktowała się ze swoimi znajomymi z "Nowej, Młodej Elity" (grupy magów chcących zająć miejsce starej elity SŚ) i powiedziała im, że współpracuje z terminusami w sprawie Klotyldy Świątek. Poprosiła ich o to, by znaleźli coś o burzach magicznych w rejonie Myślina (konflikt) w zamian za co wygłosi po zakończeniu sprawy publiczne wystąpienie odnośnie ograniczonej kompetencji magów starych rodów SŚ, bo oni - nowe, młode rody - byli potrzebni do rozwiązania takiej sprawy. Znowu się udało; dostała jeszcze jedno nazwisko. Czarodzieja SŚ, który zginął podczas burzy magicznej która pojawiła się nagle. Przepaliło go. Co ciekawe, to był pierwszy mag który zginął z czymkolwiek powiązanym z burzami w okolicy, przed tym jak ci pozostali tam padli. Mag ten był Archiwistą Rodów Świecy (specjalna funkcja w SŚ; magowie ci spisują wszystkie rzeczy powiązane z magami SŚ i magami spotkanymi przez magów SŚ. Takie "teczki" na magów SŚ i magów spoza SŚ). Bonus - na bazie informacji Vuko ten mag przydzielał dzieci Klotyldzie Świątek.

No to mamy. Okazuje się, że to wszystko zacieśnia się dookoła Klotyldy Świątek i Rodziny Świecy. I jednej lokalizacji - Myślina.

Tak się składa, że przybrany ojciec Andrei, lokalny watażka mieszka właśnie w Myślinie. A przynajmniej - ma tam rezydencję. Jedną z. Ulubioną (Jesionowy Dwór). I chwilowo tam rezyduje. Vuko rozpostarł po okolicach Myślina, tam, gdzie były wydarzenia i burze magiczne sieć dron niemagicznych (konflikt: zasoby) a Andrea wprosiła się do ojca na obiad. A dokładniej: Andrea spytała ojca czy może jej coś powiedzieć o tych konkretnych telefonach komórkowych i tej lokalizacji, a on na to odpowiedział, że telefony znajdują się chwilowo u pasera. I że jak Andrea ("mała kurka") przyjdzie na świetną zupę grzybową na obiad, to on załatwi jej pogadanie z kimkolwiek chce. I te telefony. I w ogóle.
...co Andrea miała zrobić?

Myślin. Vuko i Kornelia zdecydowali się przejść po terenie i zobaczyć ślady i miejsce, gdzie powinien znajdować się czarny opel astra (samochód Klotyldy). Bo drony nie pokazują lokalizacji opla astra, a terminusi jednoznacznie wskazali gdzie to powinno się znajdować. Za to ślady wskazują na działanie Skażenia; burze magiczne zrobiły swoje. Owszem, terminusi i puryfikator odkazili teren, ale nie było to wystarczające - działali w pośpiechu...

Kornelia wpadła na niebezpieczny, ale potencjalnie skuteczny pomysł. Korzystając z tła Skażenia i faktu, że jest nekromantką (oprócz bycia lekarzem), zdecydowała się sprawdzić czy ktoś faktycznie tu umarł (poza tym archiwistą). Bo na razie nic nie wiedzą. Kornelia wysłała wici w eter by skontaktować się z jakimkolwiek ze zmarłych magów (konflikt). I udało jej się to, choć zupełnie nie w taki sposób w jaki chciałaby to zrobić. Z uwagi na obecność Skażenia i... czegoś innego doszło do zakłócenia jej zaklęcia. Jej czar "coś" uruchomił, "coś" zobaczyło obecność czaru Kornelii (Rdzeń Interradiacyjny, a raczej to, co z niego zostało, wspierające obecność Kwiatuszka (defilerki)) i Kornelia połączyła się bezpośrednio z magiem zmarłym na Rdzeniu Interradiacyjnym, wyżartym przez defilera i Rdzeń. To była wizja, mag torturowany przez jakąś technomantyczą maszynę z jednej strony i ostre ciernie róży z drugiej. I Kornelia przez MOMENT poczuła interakcję ze Rdzeniem. Szok był tak silny, że czarodziejce poszła krew z nosa i uszu i padła na ziemię nieprzytomna.

Drony zaalarmowały Vuko, że za nim jest ruch. To trzy psy Myślińskie. A raczej - to co z nich zostało po przejściach ze Skażeniem. Silniejsze, szybsze, skoordynowane. I pełne nienawiści. Vuko nie stracił zimnej krwi - stanął by ochronić Kornelię i potraktował psy gazem pieprzowym (konflikt). Jako, że Skażenie wzmocniło ich esencję psa, zadziałało to nadzwyczaj dobrze - psy pouciekały w panice a Vuko ochronił Kornelię.

Tymczasem Andrea jadła spokojnie posiłek ze swoim ojcem i bardzo przerażonym człowiekiem, który sprzedał paserowi komórki. Znalazł komórki w lokalnym byłym bieda-szybie, który aktualnie służy za za absolutnie nielegalne wysypisko śmieci. Przyciśnięty przez Andreę (konflikt) powiedział, że miał wrażenie że coś w kopalni się poruszało. Dodatkowo, opowiedział, że lokalne dzieciaki zrobiły obławę na ludzi w czarnym oplu astra - by ich skroić z kasy, ale samochód "rozpłynął się w powietrzu". Mniej więcej wtedy Andrea poczuła się bardzo źle. Jej własna magia zaczęła wzbierać. Przeprosiła i wybiegła - od razu odkryła (konflikt) co to jest - są choroby i rośliny które inaczej działają na magów (Aquam Vim) i ludzi (Aquam) i Andrea właśnie zjadła trujące zioła, które nic nie robią ludziom lecz magom wypalają kanały i magowie bardzo, bardzo ciężko to przechodzą. W rozpaczy wysłała SOS do Vuko i Kornelii i z trudem doczołgała się do krawędzi lasu...

Vuko otrzymał SOS z informacją o truciźnie. Bez zbędnych ceregieli (konflikt) ocucił Kornelię jej własnymi solami trzeźwiącymi, pogarszając jedynie jej stan, po czym ignorując Skażenie teleportował tą dwójkę (przez otwarty kanał z Andreą jako namiar) do Andrei. Kornelia dała radę magią zniszczyć truciznę z ciała Andrei (konflikt, epicki sukces) bez właściwie żadnych konsekwencji negatywnych - osłabiona i mniej stabilna magia połączona z tymczasową utratą nadawania hipernetem.

Andrea nie miała wyjścia - musiała wrócić do ojca choćby po to, by ten nikogo nie zakrzyczał czy nie pobił. Spytała go skąd wziął zupę; okazało się, że jest tam ten jeden kucharz, który bardzo tanio sprzedaje niesamowicie smaczną grzybową. Gdy Andrea powiedziała mu, że jej bardzo zupa zaszkodziła on powiedział, że jest pierwszą. Nie, drugą - jest jeszcze jeden gość, dziś przyjechał do miasta. (to terminus, przyszywany syn Klotyldy Świątek który szuka swojej "mamy"). Aktualnie jest w szpitalu. Korzystając z niemocy i osłabienia Andrei, ojciec wymusił na niej (konflikt) przyjęcie masażu i odpoczynku od Jędrka Zduna - super-personalnego masażysty (ojciec Andrei zrobił z Myślina swój własny prywatny kurort). No, plebs będzie pracował a szlachta jest masowana :D.

Vuko i Kornelia poszli do tego kucharza. Jest to zwykły, poczciwy człowiek. Miał on "prorocze sny od Boga" które mu się od jakichś 3-4 miesięcy śnią, odnośnie tego że ma robić i sprzedawać zupy grzybowe. Zapytany przez Kornelię czy dodaje ziół szkodzących magom (tu: po nazwie, człowiek nie wie o magach i ziołach szkodzących magom) ten potwierdził - świetny smak a receptura przyszła do niego od samego Boga! Vuko postraszył go bezbożną Unią Europejską, sanepidem, 2 osoby w szpitalu (Andrea i ten terminus) i (konflikt) kucharz poprzysiągł więcej tego nie robić w tym miejscu. Vuko zaproponował mu robienie i sprzedawanie zupek w Kopalinie. Jak tylko kucharz poszedł i powiedział Vuko i Kornelii w jakiej grocie znajduje ziółka, Vuko skontaktował się z Agrestem i powiedział mu, że będzie taki kucharz tworzył zupy trujące magów. I problem z głowy.

Jako, że Andrea nadal jest masowana przez Jędrka, Vuko i Kornelia udali się do tej groty gdzie miały być zioła. Kornelia używając swojej wiedzy o roślinach i ziołach od razu odkryła, że te konkretne zioła wymagają Skażonej ziemi i odpowiednich warunków. Ktoś tu je zapewnił, wiedząc, co jest potrzebne. Wiedząc bardzo wiele na ten temat odkryła (po jakości ziół i ich kształcie; jak mech zawsze rosnący na północy drzew), że źródło mocy Skażenia jest w kierunku kopalni odkrywkowej.

Vuko wysłał tam dronę. Kopalnia bieda-szybowa wygląda, no, kopalniowo. Ale drona nie może wlecieć tam gdzie Vuko chce, bo śmieci przeszkadzają. Kornelia wezwała i wysłała grupę 9 psów by odkopały wejście dla drony (konflikt: fail), ale gdy psy dokopały się odpowiednio głęboko, drona rozpadła się na kawałki, psy zintegrowały się w jedną istotę i powstał cyborg-wielopies. Kontrola nad nim została utracona i potwór ruszył w kierunku groty, gdzie Vuko i Kornelia się znajdowali.

Vuko i Kornelia zdecydowali się na przyjęcie walki. Plan prosty - Vuko postawił portal w grocie na zewnątrz groty, Kornelia została w grocie by przyjąć potwora jako przynęta, oraz używając Existo Atomos rozmontowała wiązania nad grotą na wyzwalaczu. Plan jest taki: potwór wchodzi do groty, Kornelia wyskakuje portalem, Kornelia łamie wiązania, potwór jest zawalony głazami. 

Oczywiście, plan jedno, rzeczywistość drugie. Potwór się pojawił i faktycznie pobiegł na grotę. Vuko dokonał eksplozji baterii drony by go ciężko skrzywdzić a Kornelia zniszczyła wszystkie jego formy trucizny zaklęciem (a miał kilka). Stwór nie bacząc na wszystko schylił się by wejść do środka groty - dokładnie tak jak zaplanowali Vuko i Kornelia...

...i rozpętała się piekielna magiczna burza. Oba zaklęcia - i portalu i wiązania przestały działać. Na Kornelię zaczęły spadać kamienie i cała grota zaczęła się walić a jedyne wyjście było przez wielkiego stwora...

Vuko zareagował błyskawicznie - zaszarżował na stwora by go odepchnąć i zrobić Kornelii prześwit by była w stanie się przedostać. Udało się to, choć został poszarpany przez potwora. Kornelia jednak musiała mimo burzy wesprzeć się zaklęciem by przedostać się przez stwora. Udało jej się, lecz zaklęcie objęła też potwora, dając mu niesamowitego powerupa. No i spadające kamienie 

...oops. Vuko i Kornelia zrobili najsensowniejszą rzecz jaką mogli. SOS SOS SOS! -> Andrea.

Ta zerwała się jak oparzona ze stołu do masażu, założyła na siebie pierwszą rzecz i poleciała ukraść ojcu jakiś motor. Nie będzie teleportować się w środek burzy magicznej. Po prostu nie (co nota bene jest bardzo mądrym podejściem).

Vuko i Kornelia zdecydowali się wiać. Po wzmocnieniu stwór wydawał się bardzo potężny, zwłaszcza w burzy magicznej. Udało im się uciec - Vuko ściągnął na siebie ogień by ratować Kornelię (on umie walczyć, ona... nie) i został mocniej ranny a Kornelia skorzystała z okazji. Gdy potwór się na nią rzucił, jej pies ją obronił, zostając bardzo ciężko ranny. Ale Kornelia wyszła z tego bez szwanku.

Andrea przyjechała na pomoc na bardzo poobijanym motorze. Vuko jej go zwinął, wbił motor w potwora i strzelił z pistoletu w bak, niszcząc skażeńca. Burza magiczna zaczęła przechodzić, Kornelia była zmuszona wszystkich połatać... trochę czasu to zajęło, dając przeciwnikowi czas na działanie.

Nie mieli już dużo czasu. Cała trójka zgodnie zdecydowała się zejść do kopalni, ale po zdobyciu jakiegoś uzbrojenia. Andrea poszła do ojca sępić broń. Ten się obruszył - nie ma broni, on nie jest tego typu przestępcą, po co mu broń... no ale udało się zdobyć dwa kałasznikowy i trochę materiałów wybuchowych...

Zeszli do kopalni grzebać w śmieciach. Andrea szła przodem by znaleźć pułapki. No i (niestety, sobą) znalazła jedną - minę przeciwpiechotną - na szczęście śmieci ochroniły ich przed ciężkimi ranami. Przechodząc przez kilka niezbyt optymistycznych miejsc pełnych pułapek tego typu dotarli do sporej groty.

W grocie znajdował się zniszczony Rdzeń IR, do którego przymocowanych było 2 magów - Klotylda, eks-ukochany Kornelii. W klatce koło Rdzenia znajdował się syn Klotyldy, w pasie z materiałów wybuchowych. W Rdzeniu znajdował się martwy mężczyzna, człowiek, mąż Klotyldy, pożarty przez Rdzeń ("tyle są dla ciebie warci ludzie, prawda? Nic nie warci, więc możesz odbierać im dzieci! <mąż Klotyldy umiera pożarty przez opętany przez defilera Rdzeń>"). Na ziemi koło Rdzenia leży terminus; ten zatruty zupą z grzybów a nad nim stoi człowiek, który stoi za tym wszystkim i kiedyś był magiem, przed Zaćmieniem.

Dookoła Rdzenia znajduje się potężne pole siłowe. Oczywiście, Andrea zdobyła plany Rdzenia przed zejściem na dół (konflikt) i wiedziała dokładnie w co uderzyć laserem by wymusić restart Rdzenia. To da ekipie MOMENT na zadziałanie zanim pole siłowe wróci.

Gdy Andrea zaczęła rzucać zaklęcie, mężczyzna (Krystian; eks-Maus) poinformował ich przystawiając pistolet do głowy Klotyldy, że on to zrobi jeśli dadzą mu jakikolwiek pretekst. Andrea się zatrzymała, ale ustalili wspólny plan z Vuko i z Kornelią. Przygotowała czar (laser) i... Skażenie # efekt działania zatrucia magicznego doprowadziło do Paradoksu. Andrea się oślepiła. Krystian Maus wzruszył ramionami i zastrzelił Klotyldę, po czym zażądał od Rdzenia IR by "zrobił to teraz". Rdzeń połączył się z nieprzytomnym terminusem (w rzeczywistości synem Krystiana Mausa i przybranym synem Klotyldy Świątek) i zaczął rekonstruować jego pamięć.

Ale nikt nie wiedział, co tak naprawdę Rdzeń robi, dla nich to był tylko promień energii uderzający w nieprzytomnego terminusa. 

Vuko, zdesperowany, rzucił zaklęcie lasera i zrestartował Rdzeń. Pole siłowe zgasło. Krystian Maus zignorował to; nie zauważył, skupiony na swoim synu. 
Kornelia, z pistoletem strzałkowym zawierającym morfinę (by unieczynnić maga dającego energię Rdzeniowi) stwierdziła, że nie ma cudów - nie trafi. Spanikowała i używając skalpela zabiła nieprzytomnego terminusa.
Krystian, jak w transie, w szoku, przyłożył pistolet do głowy Kornelii...
...ale Andrea (też w szoku) rzuciła się na niego, przewróciła go i uratowała Kornelii życie. Vuko użył pistoletu strzałkowego i odciął źródło energii Rdzenia (defilerskie).

Kwiatuszek spanikowała i uderzyła promieniem energii w syna Klotyldy w klatce, chcąc doprowadzić do swego wskrzeszenia. Jednak Andrea wrzuciła Krystiana w promień by ratować dzieciaka. Kwiatuszek nie była w stanie zagnieździć się w człowieku, więc Rdzeń IR nie dał rady jej odbudować, doprowadziło to jedynie do terminalnego Skażenia człowieka. Vuko wykorzystał pomysłowość Aurelii Maus budującej Rdzeń IR (zostawiła duży czerwony guzik "Naciśnij W Razie Problemów") i nacisnął go, efektywnie wyłączając Rdzeń IR...

Kornelia, mimo Skażenia, dała radę wskrzesić Klotyldę Maus. Ale wielkim kosztem - nie była w stanie nic więcej już zrobić przed odkażeniem, sama się poważnie Skaziła...

Cóż, misja chyba udana. Zginął sprawca - Krystian Maus, jeden terminus (którego nie powinno tu być), mąż Klotyldy Świątek (ale był człowiekiem, więc się nie liczy) i 2 innych magów (archiwista i inny z Rodziny). Rdzeń został zdobyty przez SŚ i przetransportowany do laboratorium...


# Streszczenie

Marian Agrest wysłał Andreę by pomogła lekarce Kornelii (chyba ze Szlachty) i serbskiemu terminusowi by odnaleźć czarodziejkę (Klotyldę Świątek) powiązaną z Rodziną Świecy. Agrest powiedział też, że gdzieś w tej okolicy znikali magowie powiązani z Rodziną Świecy. Przypadkowo, odkryli Rdzeń Interradiacyjny (to co z niego zostało) w który zaplątana była martwa czarodziejka - Kwiatuszek. Burze magiczne i niekontrolowane wyładowania energii powodowały powstawania efemeryd. Czterech magów zginęło a Klotylda - jakkolwiek ciężko Skażona - została uratowana. Kwiatuszek zniszczony, Rdzeń przetransportowany do laboratorium Świecy.

# Notatki MG:

Krystian (eks-Maus)
- utracił syna, Karola.
- żona (człowiek) go zostawiła.
- Dark Future: Klotylda Świątek umrze i zasili Reaktor. Umrą też wszyscy magowie w rękach Krystiana. Tak wzmocniony Rdzeń IR zmusi też Krystiana do poświęcenia swego syna. Krystian traci wolę do życia, Rdzeń IR to wykorzystuje i Krystian, zintegowany ze rdzeniem IR wytworzy nowy byt -> defiler imieniem Kwiatuszek powróci w formie syna Klotyldy Świątek. 

Rdzeń IR
- skażony przez defilera (Kwiatuszek).
- chce odbudować defilera przez Krystiana.
- potrafi ukierunkowywać burze magiczne.
- musi pożreć grupę magów (5) do rozpoczęcia technomantycznej odbudowy.

Zioło - prawdnik jadowity, działa negatywnie na biologię magów i nie robi nic ludziom. 

# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Myśliński
                1. Myślin
                    1. Ligota
                        1. Jesionowy Dworek
                    1. Okolice
                        1. Bieda-szyb (nielegalne wysypisko śmieci)
                            1. Grota z rdzeniem Interradiacyjnym
                        1. Kopalnia odkrywkowa

# Zasługi

* mag: Vuko Milić jako najemnik wybierający swoje cele starannie (Til)
* mag: Kornelia Modrzejewska jako wybitny lekarz z mrocznymi sekretami (Viv)
* mag: Andrea Wilgacz jako terminus analityczny działający jako liniowy (Kić)
* mag: Klotylda Świątek jako pozytywna zaginiona czarodziejka Rodziny Świecy
* mag: Marian Agrest jako przełożony Andrei i osoba która zawsze wie jedną rzecz więcej niż mówi
* czł: Krzysztof Wieczorek jako mafioso i przybrany ojciec Andrei który NIE chciał jej truć grzybkami
* czł: Jędrzej Zdun jako super-profesjonalny masażysta samego Krzysztofa Wieczorka
* vic: Kwiatuszek jako główny przeciwnik - resztka ekstadefilerki zapamiętana w Rdzeniu Interradiacyjnym
* czł: Krystian Maus jako osoba, która chciała odzyskać rodzinę po utracie magii
* mag: Karol Maus jako młody terminus z Rodziny Świecy, syn Krystiana Mausa, który nie przeżył spotkania z Kornelią

# Pytania:

Ilu magów spotkało już coś złego?
 troje nie licząc Klotyldy i jej syna.
 
Dlaczego zniknęło trzech magów i nikt nic o tym nie wie?
 komuś jest to bardzo na rękę.
 
Dlaczego Vuko się w to zaangażował?
 bo mu za to zapłacono
 
Jaka była motywacja osoby która chciała by Vuko w to wszedł?
 osoba ta uznała, że jako ktoś z zewnątrz nie będzie próbował realizować własnych interesów
 
Dlaczego właśnie Kornelia jest tu niezbędna?
 bo tylko ona jest w stanie uratować komuś życie
 
W jakim obszarze zaginiona kobieta rywalizowała z Vuko?
 próbowała go znaleźć bardzo dawno temu
 
Dlaczego to Kornelia musi jako pierwsza odnaleźć tą kobietę?
 ponieważ w innym wypadku wszystko się wyda
 
O czym zaginiona kobieta wie?
 wie o Mrocznej Reanimacji Golemicznej ze strony Kornelii (100% skutecznością)  pancjent zmarł jej na rękach, więc go reanimowała jako golema. Pacjent do dziś żyje.

Co robi tam jeszcze jeden terminus szukający tej kobiety i jaka jest jego agenda?
 próbuje znaleźć swoją przyszywaną matkę.
 
Dlaczego śmierć Klotyldy poniesie za sobą kolejne ofiary?
 ponieważ wszystkie osoby mające cokolwiek wspólnego z kotami muszą zginąć
 
Jakie powiązania ma jeden z zaginionych wcześniej magów z Kornelią?
 byli kochankami
 
Dlaczego burza jest tak dużym problemem w tym śledztwie?
 burza spowodowała zwarcie i pewien niezapisany plik zaginął bezpowrotnie
 
Dlaczego Andrea nie czuje się komfortowo z tą sprawą?
 ponieważ ma też tu jakieś sekrety
 
Dlaczego Kornelia ma tak silny stosunek emocjonalny do swego najnowszego tworu?
 ponieważ do jego stworzenia wykorzystała własną krew
 
Jaki jest OSOBISTY motyw Vuko do włączenia się w tą sprawę?
 pali mu się ogon
 
Kto zagraża Vuko?
 ci sami, przez których musiał opuścić rodzinny kraj (albańska mafia)
 
Dlaczego stara katedra jest tak bardzo istotna?
 bo stare mury znają stare dzieje.
 
Czy syn Klotyldy jest magiem?
 tak


Viv:
ambitna, skryta, dekadencka
chirurgia, magia medyczna, farmakologia

Kuzyn:
duże przywiązanie do funkcji terminusa (gliniarz), instynkt łowcy (tropiciel: Hunter-Killer), duch miasta (w mieście jest jak w domu, zlewa się z miastem)
cracking, technomancja, tropienie
---
layout: inwazja-konspekt
title:  "Nie zabijajmy tych magów"
campaign: rezydentka-krukowa
players: kić
gm: żółw
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [170404 - Wąż jako vicinius Pauliny (PT)](170404-waz-jako-vicinius-pauliny.html)

### Chronologiczna

* [170404 - Wąż jako vicinius Pauliny (PT)](170404-waz-jako-vicinius-pauliny.html)

## Kontekst ogólny sytuacji

## Punkt zerowy:

![Rysunek pokazujący kto kogo jak i gdzie](Materials/170409/mission_diagram.png)

## Misja właściwa:

**Dzień 1:**

Wieczorem, Paulina już po przyjmowaniu pacjentów, Wąż zanudzona na śmierć... gdy do Pauliny ktoś puka. Taki 20-30 letni facet, tatuaż, kolczyk... ubrany na czarno. Z kapeluszem. KAPELUSZEM. 

* Dzień dobry. Czy mam do czynienia z... czarodziejką Tarczyńską? - Jan Karczoch, teatralnie
* W rzeczy samej? - Paulina
* Podobno ma pani doświadczenie na arenie Kosmicznej Duszy - Karczoch, głośnym szeptem
* Tak, to prawda. Rozumiem, że jest pan zainteresowany usługami lekarza na arenie? - Paulina, do konkretów
* Tak, w rzeczy samej - Karczoch, dopasowując się do Pauliny - Nazywam się Jan Karczoch. Jestem przedstawicielem Pentacyklu.
* ... - Paulina nie komentuje kolejnej nazwy.
* Tak, słyszałam, że gdzieś w pobliżu jest arena; gdybym była zainteresowana, to bym już zaoferowała. - Paulina, z lekkim zniecierpliwieniem.

(2v2-> F (cena będzie niższa), S) Paulina próbowała wzbudzić popyt na swoje usługi. Udało jej się. Karczoch jest bardzo zainteresowany Pauliną; acz nie zaoferuje jej za dużo, bo zwyczajnie jest zainteresowany ale nie jest przekonany, że potrzebuje lekarza.

Weszli do Pauliny. Karczoch powiedział Paulinie, że chciał jej zaoferować miejsce jako lekarz na arenie. Po prawdzie nie mają lekarza. Nie był potrzebny do tej pory. Paulina zauważyła, że ponoszą spore koszty - zdobywać istoty do walk, coś robić z pokonanymi... paskudnie. Karczoch powiedział, że nie chodzi o pieniądze. Chodzi... o ARENĘ. Arena zarabia na siebie, jego zdaniem. Paulina w wiele rzeczy uwierzy, ale w to nie. Karczoch sam w to nie wchodzi. 

Paulina powiedziała, że nie zatrudni się na arenie, która nie zarabia i nie jest w stanie wypłacić jej pensji. Realistycznie, walka maksymalnie raz w tygodniu (Paulina poszła na bazie modeli znanych Wiaczesławowi). Biorąc poprawkę na teren i profil ludności... po prostu nie powinno być dzikich tłumów. Brak lekarza -> duże koszty. Nie każdy z gości jest w stanie pozwolić sobie na to regularnie. Itp itp. Więc - niech Karczoch mówi. (3+3v2+2->R (na razie bez viciniusa)). Karczoch powiedział jak to działa. Arena ma normalnie jedną walkę na 2 tygodnie do miesiąca. Na arenę przychodzą też ludzie. Oni generują energię magiczną; mają transformator emocjonalny (Paulina się aż skrzywiła). Mają system zabezpieczania przed złamaniem Maskarady; zwyczajnie, mają viciniusa, który zapewnia ochronę. Zlobotomizowanego; praktycznie na autopilocie (Paulinie się to SERIO nie spodobało).

Ludzie wychodzą z areny wierząc, że widzieli coś super; wspomnienia są odpowiednio zastąpione. Ludzie płacą niemało. Przy okazji z uwagi na krwawy sport generowana jest energia do Węzła; Węzeł emocjonalny jest potem odpowiednio puryfikowany i rozładowywany przez katalistę. Połączenie pieniędzy ludzkich i quarków pozwala na utrzymanie tego. Oprócz tego są robione inne imprezy, też pod kontrolą viciniusa; tamte są o niższym poziomie emocjonalnym, ale uniemożliwiają Węzłowi wejście w tryb Krwi lub negatywnych emocji zbyt skutecznie.

...całkiem niezły model. Oprócz tego, nie tylko ludzie przychodzą na trybuny. Też czasem magowie, zwłaszcza młodzi, których jara (tak, użył tego słowa) to, że mogą łamać Maskaradę przy ludziach, którzy tego nie pamiętają. Oni też płacą za NALEŻENIE. Takie "Dare Shiver". Ale inne, bardziej... softcore'owe.

(6v5->S) Paulina nacisnęła. Jaki to vicinius? Co oni mają? Karczoch się złamał. Powiedział jej. Wetrejan. Lobotomizowany wetrejan. Oprócz tego mają Dromopod Delinquo oraz dwa Ursataury. Innymi słowy, paskudni wojownicy. Gdy trzeba naprawdę naładować Węzeł, Ursataur rozrywa jakiegoś przeciwnika i go pożera. To generuje odpowiedni... poziom emocjonalny, zwłaszcza wśród dzieci.

* Jaki idiota bierze dzieci na arenę?! - Paulina, wstrząśnięta
* Wtedy nie wiedzą na co idą; to robimy tylko w warunkach szczególnych, gdy MUSIMY podładować Węzeł - Karczoch, z lekkim entuzjazmem
* Jest pan... współwłaścicielem? - Paulina, ostrożnie
* Też. - Karczoch, z dumą - Wszyscy jesteśmy współwłaścicielami. Będzie pani pierwszym pracownikiem. 

Paulina zaczęła pytać o wyposażenie w kontekście medycznym. Karczoch zauważył, że nie mają nic. Nasza katalistka czasami pomaga viciniusom. Karczoch powiedział, że w tej sprawie Paulina musiałaby z nią porozmawiać. Paulina zażyczyła sobie zwrot kosztów do warunków zatrudnienia. Karczoch się zgodził bez słowa.

Paulina wyraziła zainteresowanie ofertą, po czym zaczęły się negocjacje cenowe. One będą powtórzone jak Paulina zapozna się z rzeczywistą ofertą, ale po tym, jak Paulina ponegocjuje stanowczo, zaproponowała swoją zasadę - pierwsze trzy razy uznaje za okres próbny dla obu stron i możliwość oceny czy chcą kontynuować - za trzy razy oczekuje tylko zwrotu kosztów. A nie zapłaty. Karczoch się zawahał; chciałby Paulinie zapłacić. On naprawdę wierzy w tą arenę. Paulina odbiła, że jej vicinius dawno nie miał okazji poćwiczyć. Na to Karczoch się zmartwił; na to się specjalnie nie może zgodzić, bo ich Mistrz Areny się nie zgodzi :-(. Paulina zastrzygła uszami. Uznała, że niekoniecznie za pierwszym razem. Ale dojdą docelowo do porozumienia...

Karczoch wyszedł. Paulina poszła do Wąż. Powiedziała jej, że na pierwszą arenę idzie sama. Wąż zignorowała niebezpieczeństwa wobec Pauliny i zauważyła, że są już trzy osoby, których trzeba się pozbyć - Karczoch, katalistka i mistrz areny. Paulina ma coraz większe przekonanie, że rozpozna teren i zostawi sprawę Wąż i Wiaczesławowi. Nie chce mieć z tym nic wspólnego i nie ma pomysłu jak mogłaby osłonić tych magów... za mało wie.

**Dzień 2:**

Paulina idzie obejrzeć arenę; ma podstawowy i standardowy zestaw leków. Nie idzie pracować, ale zdecydowanie idzie obejrzeć. Na miejscu są normalne konie itp. Gdy Paulina zaczęła rozpytywać o Karczocha, powiedziano jej, by poszła do biura. Tam ktoś jest "od niego". Paulina zauważyła, że są tu normalni ludzie i normalne konie.

A tam, w biurze, Grzybb. Jak usłyszał, że to Paulina, ucieszył się bardzo. Oczywiście, Karczoch jest; koryguje elementy areny. Jest pod ziemią. Grzybb zaczął Paulinę pod ziemię; tam uderzyła w nią aura mentalno-iluzyjna (wetrejan). (5v4->R) Paulina się ZORIENTOWAŁA, że jest tam aura i szybko zaczęła się wycofywać.

* Nie chcesz się spotkać z Karczochem? - bardzo zdziwiony Grzybb - Możesz zejść, jest bezpiecznie. Zobaczysz boksy, viciniusy...
* Pozwolisz, że zanim tam zejdziemy jednak zapewnię sobie nieco ochrony przed wpływami mentalnymi? Nawyk profesjonalny. - Paulina, z uśmiechem, acz skóra jej się jeży
* Żaden problem, oczywiście, profesjonalizm jest najważniejszy. Czego potrzebujesz. - Grzybb, wyraźnie rozluźniony

Paulina zażyła (-1 surowiec) eliksir od Wąż i użyła zaklęcia mentalnego mającego ją chronić (7v4->S). Udało się; jest odporna na aurę wetrejana (i nie tylko!). A przy okazji spróbowała zrozumieć NATURĘ tej aury. Natura jest... iluzyjna. "Jest lepiej niż jest". "Będzie dobrze". "Wszystko uda się zrobić". "Pracą i wysiłkiem osiągniesz sukces". "To bardzo perspektywiczne". Oraz pewne przesunięcia rzeczywistości, np. rzeczy wyglądają lepiej niż powinny, są czystsze, viciniusy lepsze, magowie są ambitniejsi...

Paulina dotarła do Karczocha. Arena jest dość imponująca. Widać, że to miesiące pracy. Wspomagane quarkami, mają też koparkę podziemną i inne ludzkie sprzęty. No i Karczoch.

* Czarodziejko, cieszę się mogąc Cię widzieć! - Karczoch - Większość Pentacyklu chciałaby Cię poznać!
* Ależ oczywiście, po to tu jestem - Paulina
* Pozwól, że zaprowadzę Cię do Anety. Ona NALEGAŁA by Cię poznać - Karczoch

Karczoch wyjaśnił, że Aneta jest katalistką. To taka pozytywna dusza ;-).

I faktycznie, Aneta jest w Centralnym Źródle Energii areny. To potężny Węzeł emocjonalny wspierany i neutralizowany przez poszczególne dziwne generatory i inne urządzenia. Te urządzenia LEDWO sobie radzą. To jest na granicy niebezpieczeństwa! Aneta jest... oszpecona. Wygląda, jakby coś się na nią wylało, a potem wybuchło. Na pewno bardzo cierpiała. Twarz, lewa ręka, fragment torsu.

* Cześć - Aneta - Jestem tutejszą finansistką... katalistką - poprawiła się szybko Aneta

Anecie spodobał się pomysł lekarza. Mogliby robić walki między viciniusami. Jak powiedziała, większość aren ma coś takiego, a ich nie ma możliwości. Teraz, z Pauliną pojawia się nadzieja, że będzie lepiej. I stąd zapłata. Karczoch powiedział, że Paulina się zgodziła nie płacić, ale Aneta zaprotestowała. NIE MA TAKIEJ OPCJI. Karczoch się wycofał... Aneta zwróciła się do Pauliny i ta zaakceptowała.

* Czarodziejko, nie rozumiem. Jest to moja standardowa praktyka. Ja osobiście traktuję te trzy pierwsze spotkania jako swoisty okres próbny dla obu stron - Paulina
* Niech tak będzie, okres próbny. Ale płatny. - Aneta
* Czemu to takie istotne? - Paulina
* Nie jesteśmy magami, którzy chcą wykorzystywać innych magów. Ta arena ma... inne cele. - Aneta, z determinacją - Jak Ci nie płacimy, jesteś służącą a nie partnerką.

Paulina zauważyła, że aura wetrejana stabilizuje Węzeł Emocjonalny; aczkolwiek wetrejan może żywić się samym węzłem. Czyli jest silniejszy niż zwykle. I nikt tego nie widzi? Nie widział od samego początku? Najpewniej wpadli na "genialny pomysł" a potem było już za późno...

* Aneto, Serczedar chciał się z Tobą spotkać w jakiejś sprawie... - Grzybb
* Niech przyjdzie na następną walkę - prychnęła Aneta - nie mam na niego czasu
* Jest to mag rodu Bankierz, znaczący. Możemy dzięki niemu wygrać nasz bilet i patronat... - Grzybb
* Tak tego nie zrobimy, Frydziu - westchnęła Aneta - Jemu nie chodzi o nas. On coś chce a ja mu tego nie dam.
* Ale dzięki temu możemy wciągnąć Mikaelę do Pentacyklu - Grzybb knuje dalej
* Weinerkę? Lubię ją, ale nie ma szans; nie stać nas na nią - Aneta
* Ona może chcieć tak hobbystycznie... - Grzybb
* Pogadaj z Jarkiem. - Aneta - Ja mówię, że bez polecenia... nie ma to sensu

Paulina zauważyła, że Aneta potarła bezwiednie blizny gdy Węzeł zaczął pulsować mocniej. Karczoch ma jej pokazać pomieszczenie, gdzie Paulina będzie pracować. Karczoch zaprowadził Paulinę do takiego pomieszczenia; widać, że jest świeżo zrobione. Sensownie umiejscowione. Jest też w miarę sensownie zrobione; Karczoch jest architektem, choć początkującym. Paulina mówi co jej jest potrzebne, Karczoch uaktualnia plany i obiecuje, że następny raz będzie.

Paulina zdecydowała się dowiedzieć jak ta cała grupa działa. Kim oni są. I stwierdziła, że wyciągnie to z Karczocha, bo jest głównym adwokatem Pauliny. (4+1v2+1->S). Karczoch powiedział, co następuje:

* Wielki Gurini: nie wiedzą, jak się nazywa; jest to prawdziwy Wielki Mąż. On skontaktował tą grupkę. On dał im pomysł jak złożyć arenę do kupy. On dał im kickstart. I jemu zapłacili; spłacili go (łącznie z odsetkami) i teraz mają do niego taki luźny kontakt; nie bywa w pobliżu.
* Mistrz: przełożony grupy; rozmawia z nim przede wszystkim Jaromir Myszeczka oraz Aneta Rukolas. On ani Fryderyk nie mają do niego dostępu. Nie chce z nimi rozmawiać, ALE JEST OK, bo ich chroni przed Świecą, innymi gildiami itp. To Sowiński.
* Jan Karczoch: architekt, konstruktor, iluzjonista. Chce się wykazać. Po tym jak potraktowali go magowie w przeszłości to jest jedyna grupa, która okazała mu serce. Wierzy w tą arenę, w to, że wszystko będzie znowu dobrze.
* Jaromir Myszeczka: ekspert od nastrojów i viciniusów. On poza Sowińskim stanowi jakąś osłonę przed innymi magami; jest w końcu ze Świecy.
* Fryderyk Grzybb: mistrz areny; mag bojowy... w pewnym stopniu. Jest też ich ekspertem od marketingu. Niezły mentalista. Jego głównym celem jest maksymalizacja sławy areny i personalnej; plus, smali cholewki do Mikaeli Weiner. 
* Aneta Rukolas: dość zdolna katalistka i finansistka. Nie jest najlepsza, ale jest bardzo, bardzo pracowita. Była kiedyś służką Eweliny Bankierz, ale gdy Bankierzówna zrobiła błąd to wina spadła na Anetę. Wywalili ją. Chcieli, by wróciła przepraszać. Nie wróciła. Nie ma żadnych SZCZEGÓLNYCH skilli, ale jest pracowita i uparta. Dąży do autonomii i niezależności.

Paulina poskrobała się po głowie. To jest ciekawa sytuacja. Poprosiła jeszcze o informacje o Serczedarze i Mikaeli.

* Mikaela Weiner: śliczna jak laleczka. Interesuje się pracą i działami Grzybba. Dziewczyna Serczedara. Bywa dość amoralna; Grzybb dla niej zorganizował walkę, gdzie człowiek ucierpiał. Ale nie było wyjścia. Były pieniądze a Węzeł Musi Płonąć. Był na zbyt niskim poziomie energii i mógł zgasnąć...
* Serczedar Bankierz: mag bojowy. Chłopak Mikaeli. Wyraźnie jest zainteresowany Anetą, która go ma gdzieś. Mikaela nie jest zazdrosna; trudno być zazdrosną o Anetę (mało skilli, mała uroda, małe wszystko). Aneta mówi mu, że nie będzie z nim gadać czy się interesować. Chce z nią gadać? Musi przyjść na arenę i płacić. Zrobiła z niego pewne źródło dochodów dla areny.

Dla Pauliny jest tu coś bardzo dziwnego. Statystycznie KOMUŚ powinna przeszkadzać krzywda ludzi. Magowie nie są tak amoralni. Nie aż tak. Więc... wetrejan? Coś jeszcze? A Paulinie nikt nie uwierzy odnośnie wetrejana z tej ekipy. Nie ma szans...

Paulina zapytała Karczocha o to, czemu Aneta ma blizny. Czy szukała pomocy? Karczoch powiedział, że to trudny temat. Te blizny są właśnie stamtąd, ze służby od Bankierzy. Aneta NIE zgodziła się przeprosić. Nie była winna. Więc ją wywalili i nie pomogli, by ta przyszła poprosić o pomoc. A Aneta nie poprosiła. Niestety, Anety - ani całego Pentacyklu - nie stać na regenerację Anety. To jest tydzień kuracji z lekarzem pod ręką. Gorzej, że Węzeł musi być stabilizowany; żaden lekarz nie chciał łazić z Anetą przez tydzień, do tego kalibrować gdy ten Węzeł modyfikuje stan tych blizn. 

Czyli to kwestia ceny... Paulina podejrzewa, że to NIE jest pierwszy sort magów. Wiaczesław bez przygotowania może wszystkich pozabijać gołymi rękami...

Karczoch oprowadził Paulinę i po wszystkim Paulina wróciła do Wąż.

Wieczorem.

Wąż przygotowała świetne jedzenie. I zaprosiła Paulinę na kolację. Paulina doceniła.

* Ile osób należy zabić? - Wąż podczas kolacji - Wiem o trzech.
* Zaczynam się zastanawiać, czy te trzy również... - Paulina, ostrożnie - Pozwól mi się dowiedzieć więcej
* Oczywiście. Nie rozpoznając terenu nie możemy nic zrobić. Poza granatami - Wąż
* Oj uwierz, granaty to bardzo zły pomysł. Co robi granat w zamkniętym pomieszczeniu? - Paulina do Wąż
* Porządek. - Wąż
* Nie, bardzo duży bałagan. Łącznie z niewinnymi osobami - Paulina, ponuro
* Mają zakładników? - Wąż
* Upraszczasz... pozwól mi zrozumieć sytuację. - Paulina
* Oczywiście. Nie rozpoznając terenu nie możemy nic zrobić. - Wąż, powtarzając się z uśmiechem 

Paulina poszła do łóżka z ponurą obserwacją. Wąż i Wiaczesław mogą to zniszczyć... i to nie będzie walka. To będzie masakra... Paulina chce ich JAKOŚ uratować.

**Dzień 3:**

Paulina już WIE, że nie jest obserwowana. Tamta arena jest w zbyt złym stanie, ci magowie to amatorzy. Paulina musi porozmawiać z Wiaczesławem... a ze wspomnień pobitego człowieka wynika, że pobił go Grzybb. Poprosiła więc Wiaczesława, by ten przyjechał... i Wiaczesław się pojawił w odpowiednim czasie.

* Masz nazwiska? - Wiaczesław, spokojnie
* I tak i nie... tam jest coś więcej. To nie jest takie proste jak wygląda, a przynajmniej takie mam wrażenie - zrezygnowana Paulina

Paulina opisała Wiaczesławowi, że ci magowie zachowują się, jakby wierzyli, że to wszystko MUSI się udać. Ta arena jest mała, biedna, kiepska... a oni się zachowują jakby była wielką i potężną areną. Dziecięce zauroczenie. Wiaczesław spytał, czy są nieletni. Nie są. To odpowiadają za swoje czyny.

* Wolisz złapać płotkę czy rekina? - Paulina, brutalnie - Bo mam wrażenie, że ktoś... jaka szansa, że wszyscy podejdą do krzywdy ludzi z dziecięcą obojętnością?
* Nie jest to typowe, prawda. Ale wiesz, system zniewala - Wiaczesław
* Jest tam aura magiczna. Nie wiem jeszcze jaka... wszystko wskazuje na wetrejana. - Paulina
* Wetrejan - Wiaczesław podskoczył - To bardzo źle. Bardzo, bardzo źle.

Paulina powiedziała, że wetrejan został zlobotomizowany. Wiaczesław machnął ręką. Oni MYŚLĄ że tak jest. Są pod wpływem aury. Paulina powiedziała, że nie do końca mogą odpowiadać za swoje czyny. Wiaczesław zauważył, że odpowiadali przyjmując viciniusa. Ale tak, jest tam ktoś jeszcze.

* Czyli: ukarać tą grupkę... - Wiaczesław, zaczął
* Niekoniecznie śmiercią. - Paulina weszła mu w słowo
* To jak? - Wiaczesław
* Jeszcze nie wiem - Paulina - Ale jest tam co najmniej jedna dziewczyna, która została ciężko skrzywdzona
* I jest tu co najmniej kilku ludzi, którzy zginęli. Przez nich. I przez nią. - Wiaczesław
* Nie sądzisz, że większą wartość by miało, gdyby odpracowali krzywdy? - Paulina
* Nie wiem. Dla ofiar i ich rodzin? Czy dla mnie? Dla kogo większą wartość? Dla kolejnych ofiar? - Wiaczesław
* Bardziej dla społeczeństwa... - Paulina
* Dobra, o tym potem. Czyli mamy tych magów tutaj i jeszcze jakichś innych, dobrze rozumiem? - Wiaczesław
* To są płotki, to są wykonawcy. - Paulina - To są magowie, którzy zostali wykorzystani do zarobienia przez Guriniego. Mają też jakiegoś Sowińskiego... nie widziałam go, wątpię, bym go zobaczyła.
* Czyli Sowińskiego trzeba dorwać. Magowie tego rodu są zwykle izolowani od konsekwencji. - Wiaczesław, kwaśno
* On na pewno wie, że coś tu jest nie w porządku, bo izoluje się od połowy grupy. - Paulina
* Normalne. Krawaciarz zawsze spada na cztery łapy. - Wiaczesław, z lekką pogardą

Paulina spytała, czemu wetrejan tak go zmartwił. Wiaczesław zaczął tłumaczyć. Kiedyś był na arenie z wetrejanem. Psychiczny wampir żywił się decyzjami, ambicją, energią. To jest stary model budowania areny; wetrejan zapewnia działanie Maskarady i odpowiednią ambicją dla magów i gladiatorów. Oczywiście, wszyscy zaczynają działać ponad swoje możliwości w pewnym momencie, cel zaczyna uświęcać środki... arena Wejan o której tyle Wiaczesław mówił ma nazwę właśnie od wetrejanów. Tam po raz pierwszy to zrobili. Póki wetrejan jest, arena jest tańsza i bardziej efektowna. Można wymieniać wetrejany, lub je głodzić... lub zarazić czymś... wtedy wetrejan nie dość szybko się dostroi do wszystkich obecnych. Ale tych na pewno na to nie stać.

A jeśli za dużo się będzie używać eliksirów ochronnych w polu magicznym, ryzyko takie, że Wzór maga zostanie Skażony...

Paulina powiedziała, że oni nie zauważają tej aury. Wiaczesław parsknął. Są pod kontrolą, żyją w swoim świecie. Paulina powiedziała, że nie mogą po prostu zabić tych magów. Wiaczesław powiedział, że ludzie zginęli. Ktoś musi odpowiedzieć. Paulina się zgodziła. Musi. Trzeba jednak ukarać właściwe osoby. Paulina powiedziała Wiaczesławowi, że równowaga Wąż została zaburzona. Wiaczesław potwierdził. Krew za krew. Ktoś MUSI wziąć odpowiedzialność za krew. W rozumieniu Wąż jest tam krew negatywna i pozytywna... coś dziwnego. I balans musi wrócić na zero. Czy jakoś tak.

* Czy spowodowanie krwawej zemsty to efekt balansu? - Paulina
* Mnie nie pytaj. Chodzi Wąż o to, żeby krew rozlana po stronie "czerwonej" była zbilansowana krwią rozlaną po stronie "zielonej". Czy jakoś tak. - Wiaczesław
* Czy rozumiesz, do czego ja dążę? - Paulina
* Zabić tych, którzy NAPRAWDĘ za tym stoją - Wiaczesław, prostolinijnie

(4+1v4+1->S) Paulina fundamentalnie jest lekarzem. Zemsta nie jest tym co nią kieruje. Ona chce pomóc żywym, nie martwym. A Wiaczesław chce zamknąć chorą arenę, uniemożliwić by krzywda tego typu mogła się kiedykolwiek zdarzyć; dlatego jego zdaniem egzekucja magów za tym stojących pomoże. Bo "never again". Plus, przy odpowiednim PR, może zadziałać to jak akt terroru i inni się zastanowią dwa razy... tak, JEST radykalny. Paulina próbuje przekonać Wiaczesława, żeby ich jednak NIE zabijać; tylko osoby za tym stojące, ale nie te ragtagi.

Wiaczesław się zgodził. Trzeba jeszcze przekonać Wąż. A to będzie trudniejsze; Wiaczesław pomoże Paulinie.

* Czerwona i zielona krew? Ty nic nie rozumiesz... - Wąż do Wiaczesława, kręcąc głową z niedowierzaniem
* Sprawa jest prosta: zginęło kilka niewinnych osób. Bilans jest wychylony w stronę czerwieni. Teraz trzeba przywrócić bilans przez wzmocnienie bieli lub osłabienie czerwieni. Z uwagi na działania Krwi i Magii Krwi, krew za krew; skuteczniej zadziała usunięcie czerwieni. Oni stali się czerwoni przez zabicie tych ludzi. Więc - należy ich usunąć. Bezboleśnie, bez zbędnego torturowania, zabić. W ten sposób bilans będzie znowu stabilny. - Wąż
* A jakaś alternatywa nie wymagająca zabijania? - Paulina do Wąż
* Biel, nie czerwień. Wzmocnienie bieli. - Wąż
* Rozwiń - Paulina
* Śmierć tych ludzi i viciniusów spowodowała cierpienia ich rodzin. Można ich pomścić lub można pomóc rodzinom. Biel. Wzmocnienie bieli. Tak, jakby ci ludzie nadal żyli. - Wąż
* A jeśli czerwoni zajmą się wzmocnieniem bieli? - Paulina
* Zadziała. Pod warunkiem, że nigdy nikogo już nie zabiją; jeśli jest takie ryzyko, należy ich zabić najpierw. Wiedząc o tym, jeśli ich nie zabiję, krew jest na MOICH rękach. Czyli wszystkie kolejne zbrodnie jakie oni popełnią a ja ich nie zatrzymam to moje zbrodnie. - Wąż, wyjaśniając spokojnie

Wiaczesław stojąc za Wąż pokazał Paulinie symbol nienormalności. Paulina do pewnego stopnia rozumie tą logikę, acz jest... specyficzna. 

(4+1v2+2->S) Paulina poprosiła Wąż, by ta dała im szansę. By pozwoliła im spróbować pójść w biel a nie czerwień. Magowie pod wpływem wetrejana mogą podejmować błędne decyzje. Gdyby byli w pełni odpowiedzialni... Paulina się nie zawaha. Ale Paulina dopuszcza, że nie są w pełni odpowiedzialni. A będą mogli odkupić swoje winy.

Wąż się zgodziła. Jest skłonna zaryzykować balans dla Pauliny. Paulina udowodniła przeszłymi działaniami, że ma dobre intencje...

Paulina odetchnęła z ulgą. Niekoniecznie skończy się to wszystko wielką rzeźnią...

# Progresja


# Streszczenie

Z Pauliną skontaktował się mag Pentacyklu, chcąc ją zatrudnić na arenie jako lekarza. Wraz z poznawaniem magów Pentacyklu podejście Pauliny zaczęło się zmieniać z "muszą zginąć" na "w sumie, to nie ich wina". Na arenie jest też wetrejan który wyraźnie przejął kontrolę a same systemy areny są niestabilne. Magowie Pentacyklu wyglądają na takich trochę... lipnych. Paulina przekonała Wąż i Wiaczesława, że nie ma co ich zabijać; wpierw trzeba dojść do tego co tam się dzieje i kto za tym stoi...

# Zasługi

* mag: Paulina Tarczyńska, usiłująca zrozumieć co się dzieje na Pentacyklu i powstrzymać ich egzekucję przez swoich sojuszników
* mag: Wiaczesław Zajcew, który ma historię z wetrejanami; dał się przekonać Paulinie, by nie zabijać magów od razu
* mag: Terror Wąż, nadal udająca viciniusa i przygotowująca Paulinie przydatne eliksiry; dała się przekonać, by nie zabijać magów od razu
* mag: Jan Karczoch, jeden z Pentacyklu, architekt, konstruktor i iluzjonista; chce się wykazać, 24, wciągnął Paulinę do Pentacyklu jako główny ambasador
* mag: Fryderyk Grzybb, jeden z Pentacyklu, mistrz areny i marketingu; chce sławy, 26; to on pobił ciężko człowieka w przeszłości; wyraźnie interesuje się Mikaelą Weiner
* mag: Aneta Rukolas, jedna z Pentacyklu, katalistka i finansistka; chce niezależności, 29, oszpecona i zdeterminowana by zapłacić Paulinie
* mag: Jaromir Myszeczka, jeden z Pentacyklu, ekspert od viciniusów i emocji; chce przyjaciół, 28, nie pojawił się poza opowieściami Jana Karczocha
* mag: Mikaela Weiner, gość na arenie, 26, prowadzi badania nad aurą; podobno amoralna i podobno dla niej Grzybb zorganizował krwawszą wersję areny
* mag: Serczedar Bankierz, gość na arenie, 31, mag bojowy; gardzi wszystkimi; chce czegoś od Anety i dlatego płaci pojawiając się na arenie.
* mag: Kleofas Myszeczka, "Wielki Gurini" i lajfkołcz; zorganizował dla Pentacyklu rzeczy, dzięki którym założyli arenę, po czym zgarnął marżę i poszedł sobie

# Lokalizacje

1. Świat
    1. Primus
        1. Mazowsze
            1. Powiat Pustulski
                1. Krukowo Czarne
                    1. Gabinet Pauliny, miejsce centralne spotkać Wiaczesława, Wąż i Pauliny. Aha, czasem Paulina przyjmuje też ludzi do leczenia...
            1. Powiat Ciemnowężny
                1. Skalna Grota
                    1. Stadnina Koni, pod którą, jak się okazało, znajduje się Arena Pentacykl
                        1. Arena Pentacykl, ledwo się trzymająca i działająca na zapale a nie technice; rozbudowywana przez junior architekta Jana Karczocha

# Czas

* Opóźnienie: 2 dni
* Dni: 3

# Narzędzia MG

## Cel misji

* Okazja do odpowiedzenia na pytanie: czy Wiaczesław i Wąż zabiją tych magów, czy nie? Czy Paulina ich powstrzyma?
* Pokazania presji społecznej na grupkę 5 magów; czy naprawdę Wiaczesław i Wąż mają rację z radykalizmem? Ale ludzie cierpieli.
* Pokazanie jak "kołcz - konsultant" pogarsza świat, za co dostaje profity i kapitał społeczny
* Pokazanie co może zrobić dobrze odżywiony wetrejan poza kontrolą
* Słów kilka o wetrejanie i przeszłości Wiaczesława

## Po czym poznam sukces

* Paulina będzie miała decyzję do podjęcia i niekoniecznie prosty konflikt; przekonać Wiaczesława i Wąż.
* Paulina podejmie JAKĄŚ decyzję na linii presji społecznej - zemsty i kary
* Paulina odniesie się do pracy Wielkiego Guriniego. Najpewniej znając ją negatywnie ;-)
* Paulina będzie miała poważny problem - jak wiele z tego co się stało to wina magów a jak wiele wetrejana?
* Paulina będzie miała okazję dowiedzieć się więcej odnośnie przeszłości naszego Wiaczesława

## Wynik z perspektywy celu

* SUKCES: odrodzenie decyzji na potem.
* SUKCES: Paulina ma inne podejście do tych magów z Pentacyklu
* SUKCES: Wielki Gurini stał się bardzo nielubianą postacią u Paulin tej misji ;-)
* SUKCES: Wetrejan stał się istotną istotą tej misji
* PORAŻKA: wątek, za którym nie poszliśmy a Paulina nie dociekała

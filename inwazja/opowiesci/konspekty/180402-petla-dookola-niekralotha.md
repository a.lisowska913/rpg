---
layout: inwazja-konspekt
title:  "Pętla dookoła niekralotha"
campaign: adaptacja-kralotyczna
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [Szept Mare Felix](180327-szept-mare-felix.html)

### Chronologiczna

* [180327 - Szept Mare Felix](180327-szept-mare-felix.html)

## Kontekst ogólny sytuacji
### Opis sytuacji
## Punkt zerowy

* Upadły konstruminus okradający magów z artefaktów
* Kult nastawiający wojnę Niedźwiedzi z Klasztorem Zrównania
* Policja próbująca rozwikłać sprawę znikniętych ludzi

## Potencjalne pytania historyczne

* Czy Bruniewicz da radę skonfliktować się potężnie z Klasztorem Zrównania? Wojenka?
* Czy Bójka spawnuje nowego kralotha?
* Czy policja rozwali skutecznie Marię Przysiadek?

## Misja właściwa

**Dzień 1**:

Edwin dał radę wyciągnąć Marię; wraz z Norbertem się zaraz za nią ostro zabrali.

Edwin powiedział Silurii, że to nie Hralglanath kontrolował Grzegorza. To był ten "drugi kraloth". Tak więc - była próba maskowania. I najpewniej on wykończył Hralglanatha.

Kontakt do Silurii ze strony Elei. Powiedziała Silurii, że był policjant i szukał Marii Przysiadek. Zgodnie z tym, co zauważyła Elea - najpewniej Maria jest o coś podejrzana. Elea powiedziała, że nic jej nie wiadomo, bo Maria się wypisała na własną prośbę - ale Siluria musi wiedzieć, że taka sytuacja wystąpiła bo teraz KADEM jest za to odpowiedzialny... Policjant nazywa się Rafał Drętwoń; jest to policjant z Kopalina i okolic. Policjant chciał przesłuchać rodzinę Przysiadków tak samo, ale Elea skutecznie ich unieczynniła, by się bardziej pochorowali.

Elea poprosiła też Silurię, by ta odzyskała od Marii wspomagany magicznie stetoskop. Nie jest to potężny artefakt, ale zawsze coś - Elei nie stać na ciągłą wymianę takiej klasy rzeczy.

Whisperwind pojawiła się by ostrzec Silurię. Bójka może nie być pod wpływem Krystalii; ona może być pod kontrolą Drugiego Kralotha. Whisper jest zmierzła i obolała. Siluria zauważyła to; Whisper odcięła się, że symulacja nie jest prosta. Whisper jest zmartwiona - nie wie GDZIE jest kraloth; nie może znaleźć go ani na Mare Felix ani w Lesie Stu Kotów. Siluria powiedziała, że jeśli uczucia są wyinżynierowane... to jaki to może mieć wpływ na Mirandę?

Kolejny telefon do Elei dał Silurii informacje o Mirandzie: to jest "czarna królowa teatru". Mindbreak. Nastroje, piękno. Siluria spróbowała potwierdzić - jaka może być agenda Mirandy i czy w ogóle mogłaby stać za czymś takim? Elea poszukała informacji i połączyła się z Silurią; przy okazji, Miranda dowiedziała się, że Elea szuka informacji na jej temat. Wyszło wyraźnie - leży to w modus operandi Mirandy. Mogłaby robić coś takiego z Pryzmatem. Ale najpewniej to nie ona - nie ma po co. To, co Miranda na PEWNO robi to destabilizuje Mausów. Chce pokazać słabość Abelarda i Karradraela.

Z każdym dniem, w którym Miranda robi swoje, coraz więcej Mausów zastanawia się, czemu poddają się Karradraelowi. Elea się poważnie martwi - nie chce kolejnej nagonki na Mausów. Po prostu się boi, że to zniszczy Mausów na strzępy. "Miranda powinna po prostu przyjść, poddać się woli Karradraela i Abelarda - ród jest ważniejszy niż ona". Elea zauważyła - Oktawian próbował opuścić ten teren i Karradrael go ściągnął z powrotem siłą. A Miranda... she is taunting. She destabilizes. Jak?

Zdaniem Silurii, to nie Miranda stoi za tym wszystkim. Słyszała o niej dość. Wygląda to na coś, co Miranda mogłaby zrobić - ale nie jak to, co zrobiła. Nie ma powodu. Miranda nigdy nie zadawała bólu "niepersonalnie". Za to bardzo w stylu Mirandy jest wyśmiewanie publiczne Baltazara Sowińskiego. Prowokowanie i dokuczanie. Plus... drugi kraloth... coś takiego tam musi być. Miranda sama by nie umiała.

Po drodze Siluria jeszcze wysłała SMSa do Krystalii Diakon - czy ona ma coś wspólnego z Bójką? Odpowiedź była szybka. Krystalia nie miała NIC wspólnego z Bójką. Nic. Żadne koralowce, nic tego typu. Siluria się zasępiła. Czyli jednak Drugi Kraloth.

Siluria poszła więc do Edwina - czy nie przynieśli stetoskopu z Marią. Edwin się żachnął. Byli w obszarze kontrolowanym przez Arazille. Siluria powiedziała, że Elei potrzebny jest ten stetoskop. Edwin powiedział, że nie do końca rozumie - sprawdził pamięć Marii (rutynowo w tej dziwnej sprawie) i nie było tam NICZEGO powiązanego z jakimkolwiek stetoskopem. Przynajmniej, nic Edwinowi o tym nie wiadomo.

Czyli to nie Maria zwinęła stetoskop. Ktoś inny.

Edwin, zapytany, czy przeszkadzałoby mu gdyby Siluria przesunęła Ogniste Niedźwiedzie w coś innego - powiedział, że nie zależy mu w ogóle. Jego interesuje Artur Bryś i Paulina Widoczek. Edwin spytał Silurię czy ona chce porozmawiać z Grzegorza Nocniarzem. Udało się go zregenerować i doprowadzić do formy. Nieco uszkodzona pamięć, ale większość działa. Siluria prychnęła - co on może wiedzieć? Edwin zaoponował - jest świetnym neuronautą i miał dostęp do całej potęgi KADEMu. Kraloth zabił Klaudię rękami Grzegorza w taki sposób, że wszystko wskazywało na Hralglanatha. 

Edwin był tak zaciekawiony, że poprosił o zrobienie skanu na Mare Felix. Warmaster, on i Kopidół poszli na Mare Felix. Niestety, nie wiedział, że Whisperwind też była w tym czasie na Mare Felix i doszło do interferencji pryzmatycznych; ale ma odpowiedź na pytanie Silurii.

Klaudia była zakonnicą w Klasztorze Zrównania. Lokalny klasztor w Lesie Stu Kotów. To jakaś sekta. "Lustrzane odbicie Brysia - po prostu, Bryś w spódnicy. Seks to zniewolenie i inne głupoty". I ona - Klaudia - czuła miętę do Grzegorza. Ale klasztor był od niej silniejszy. Jej własne słowa. Klasztor był od niej silniejszy. A potem - Czelimin. Kościół Arazille. I skończyło się najdzikszym seksem w historii. Edwin triumfalnie spojrzał na Silurię - czy coś jej to przypomina?

* Tak. Przypomina. To nie ma sensu... - Siluria - Jeśli tak jest... ów klasztor jest w jakiś sposób... magicznie kontroluje zakonnice?
* I Hralglanath to wiedział. - Edwin
* To śmierdzi nienaturalnym, indukowanym, skrajnym feminizmem - Siluria - Takim, który chce niewolić kobiety.
* Aha. - zadowolony z siebie Edwin
* I to pasuje do uczuć, które sugeruje Kopidół. To, a nie Miranda - Siluria, dalej się zastanawiająć
* Aha. - dalej cholernie zadowolony z siebie Edwin

Jeszcze coś. Klaudia zdecydowała się wyjechać z Grzegorzem. Bardzo chciała. Grzegorz się zgodził. Gdy już wyjeżdżali - tu wspomnienie jest zniszczone. Następne, co Grzegorz pamięta to już Zamek As'caen. Edwin powiedział Silurii gdzie chcieli wyjechać - dzięki temu Siluria może rozplanować mapę. Mieli jechać przez Koty. Docelowo - poza Śląsk.

W świetle powyższego, nie mając czasu, Siluria poprosiła Mariana Łajdaka o pomoc. Pewien policjant szukał pewnej kobiety i chce się dowiedzieć czemu jej szuka. A chodzi dokładniej o Marię Przysiadek. Siluria ostrzegła Mariana - to może być kralotyczna metoda odnalezienia Marii. Siluria powiedziała jeszcze Marianowi, że ona jest pod opieką KADEMu - Łajdak obiecał, że zajmie się tym problemem.

W końcu, ku radości Silurii, okazało się, że na KADEM Primus czekają na nią Laragnarhag i Melodia. Siluria zaprowadziła ich do "ciała" Hralglanatha. Melodia się zatroskała - ciężko jej będzie coś z tym zrobić. Siluria powiedziała Melodii, że ten Drugi Kraloth ma też Bójkę. To bardzo silnie przestraszyło Melodię. Siluria dała Laragnarhagowi plasterek z tamtego kralotha; Laragnarhag wziął go do analizy. A Melodia spytała Silurię, czy ta byłaby w stanie załatwić im JAKIKOLWIEK biolab - bo tu nie ma niczego. Nic.

Siluria, nieszczęśliwa, musi porozmawiać z Quasar... nie chce, by musieli wracać do baz Millennium w Lesie Stu Kotów. To niebezpieczne z tym tajemniczym kralothem dookoła. Wait, zawsze można iść do... Marty.

* Wiesz, po co przyszłam? - Siluria, ze słodką minką
* Nie. Coś się dzieje na Mare Felix... niepokoją mnie odczyty - Marta

Siluria wyjaśniła Marcie, że problem leży w niebezpiecznym kralocie w okolicy. A ani Melodia ani Laragnarhag nie są bojowi. Marta wyraziła zgodę. Po raz kolejny Quasar została nadpisana przez Silurię tak, że nic nie może na to poradzić. Idealna sytuacja ^_^. A Laragnarhag i Melodia mogą pracować z plasterkiem i Hralglanathem.

**Dzień 2**:

Siluria obudziła się rano i dostała wiadomość od Łajdaka. Policja chce przesłuchać Marię i jej rodzinę; Hralglanath był karmiony ludźmi i część z nich po prostu zginęła. Policja - prawidłowo - szuka zaginionych ludzi a ślady prowadzą do warsztatu jubilerskiego w Miłejce. Co z tym robimy, spytał radośnie Łajdak... Siluria nie ma pomysłu. Spróbowała szybko sprawdzić, czy ofiary Hralglanathaga są pod wpływem kralotha - czy to on wszystkich zabił. Okazuje się, że NIE wszystkie ofiary pochodzą od Hralglanatha.  Ale świetnie to jest zamaskowane - tak, by wskazywało, że to Maria i Hralglanath. Gdyby nie świetna korelacja którą już Siluria zrobiła, wszystko poszłoby na Hralglanatha. Zainteresowała się też już prokuratura magów tym tematem - Marian jednak się wykpił... 

Innymi słowy, jest ciężko. KADEM opiekuje się Marią i ma zwłoki Hralglanatha, więc KADEM musi to rozwiązać. Marian został przez Silurię oddelegowany - niech on się tym zajmie. Uspokoi prokuraturę magów, wyciszy sprawę z policją. Bo cholera, nie ma jak tego zrobić sensownie inaczej... Marian się uśmiechnął - niech Siluria zostawi to jemu.

Siluria skorzystała z okazji - czas zagadać do Kromlana. Powiedziała mu, że jeden ze straconych konstruminusów jest kralothborn. Kromlan się zdziwił - to przecież teoretycznie niemożliwe. Siluria skontrowała, odpowiednio zaadaptowany kraloth JEST w stanie przejąć konstruminusa. Ale wtedy na pewno nie ma już ciała kralotha. Nie wiadomo czym to cholerstwo jest. To badania KADEMu na Fazie Daemonica...

Kromlan uwierzył. Nie ma powodu NIE wierzyć KADEMowi w tej kwestii, choć się odwoła do danych i biblioteki Świecy. Kromlan dowiedział się też, że Bójka była pod kontrolą kralotyczną. Czyli ten kraloth ma dostęp do danych Millennium w zakresie wiedzy Bójki. Siluria ma parę tropów - może uda jej się zdobyć prostszą metodę namierzenia tego kralotha, ale potrzebuje trochę czasu. Ten kraloth jest wyjątkowo niebezpieczny. I nikt nie zna jego agendy... a biologicznie nie działa jak standardowy kraloth. Innymi słowy, nic o nim nie wiadomo.

Siluria obiecała, że da znać jak pójdzie tropem Drugiego Kralotha. Siluria ukryła przed Kromlanem inne rzeczy, ale powiedziała, że dwójka kralothborn (w tym konstruminus) zostali wysłani by zranić i zniszczyć Hralglanatha. I że próbował we wszystko WROBIĆ Hralglanatha. Kromlan się lekko zasępił; to nie jest typowe zachowanie kralotha. Będzie ostrożny.

Siluria powiedziała Kromlanowi jeszcze, że ten kraloth może wzmocnić defensywy Mirandy przeciw Karradraelowi. Kromlan powiedział, że szuka kralotha tak czy inaczej. Siluria wyczuła, że Kromlan nie chce tego... ŻADNEGO kralotha na wolności. Siluria zauważyła, że musiała znaleźć specjalistów - jednym z nich jest kraloth. Kromlan zauważył "oczywiście" z sarkazmem. Siluria powiedziała, że w okolicy jest jej kuzynka. Kromlan westchnął. Pozna po kuzynce - nie chce rozpocząć kolejnej wojny gildii...

Kromlan powiedział Silurii, że coś dzieje się w Jodłowcu. Jedna grupa ludzi chce się bić z drugą grupą ludzi. Jako, że to nic z kralothem ani z Mirandą, nie miał sił zająć się tym tematem - ale informuje Silurię na wszelki wypadek.

Laragnarhag i Melodia. Melodia jest śpiąca; Laragnarhag zintegrował się z całym biolabem i połową pokoju. Melodia powiedziała, że zrozumienie tego wymagało poziomu mistrzowskiego, ale pracowali całą noc. Przeanalizowali plasterek. Okazało się, że:

* To jest nektar bezpośredni z kralotycznych kwiatów. To nie pochodzi z kralotha. Najpewniej ten kraloth nie ma formy typowej. Jest Niekralothem.
* Nie ma sygnatury kralotha. Ta istota... jest dziwna. Są tylko kralotyczne kwiaty.
* Ta istota nadal jest ekstadefilerem.

Cokolwiek to jest, jest to zniszczalne. Cokolwiek to jest, było kralothem. Ten kraloth bardzo zaadaptował. Być może za bardzo.

Siluria zauważyła, że on bardzo mocno wykorzystuje ludzi.

Czas na War Room. Siluria zgromadziła Janka, Quasar, Kopidoła, Melodię, Edwina, Margaret i zrobili wielką dyskusję by zawęzić, gdzie może znajdować się ta istota lub jaką może mieć agendę. Plus wsparcie Wektorem Sigma i Halami Symulacji Profilowanie kralotycznego przestępcy. Pełna potęga KADEMu.

I mają następujące odpowiedzi:

* Kraloth chce przede wszystkim przetrwać, zwłaszcza, że w okolicy uaktywniła się Arazille i w Lesie Stu Kotów są fabokle. Może nie ma jak uciec..?
* Bójka jest kralothowi potrzebna po to, by móc stworzyć nowego kralotha. Wszystko połączone z Pryzmatem wskazuje na to, że jest hostem. Szczęśliwie, host nie umrze.
* Ten kraloth myśli bardziej ludzko niż Laragnarhag. Nie myśli zupełnie jak kraloth. Utracił tą umiejętność. Najpewniej nie włada magią, a jak włada to jedną szkołą.
* Wszystko ogranicza się gdzieś dookoła Lasu Stu Kotów lub Mare Felix. I Millennium nic nie widzi. Co jest dobijające.

...

## Wpływ na świat

JAK:

* 2: MUSIK: gracz mówi w jaki sposób jedna z obcych postaci wspiera coś związanego z motywacją JEGO postaci
* 2: CLAIM na wątku / postaci / okoliczności
* 2: do elementu Historii lub przeszłego konfliktu dodajemy "ale" lub "oraz"
* 2: dodajemy pytanie, które musi zostać odpowiedziane na jakiejś z przyszłych misji
* 2: odpowiadamy na pytanie, które pojawiło się na jakiejś z misji
* 3: gracz mówi w jaki sposób jedna z obcych postaci wspiera coś związanego z motywacją JEGO postaci
* 3: zmieniamy kontekst okoliczności czegoś z Historii - scena z przeszłości / fakt?
* 3: do elementu spoza Historii dodajemy "ale" lub "oraz"
* 3: dodajemy znaczącego NPC powiązanego z elementem Historii
* 3: pozyskanie przez dowolną postać znaczącego surowca mającego sens z perspektywy misji
* 3: dodanie nowego elementu Historii
* 3: dodanie przyszłego lub przeszłego faktu; czegoś, co się wydarzyło lub wydarzy

nowy wariant:

* 3: wygranie Typowego konfliktu
* 5: wygranie Trudnego konfliktu
* *2: wygranie Eskalacji

Z sesji:

1. Żółw (11)
    1. Krystalia Diakon się osobiście zaangażuje w poszukiwanie i pomoc Bójce. (2)
    1. Bruniewicz wprowadzi Ogniste Niedźwiedzie do walki z Klasztorem Zrównania (3)
    1. Bójka jest gwarantowana do spawnowania nowego kralotha (6)
1. Kić (9)
    1. Rafael Diakon ostrzeże Silurię przed wejściem Krystalii (2)
    1. Patrycja nie chce odejść od Niedźwiedzi. Ona chce zmienić Niedźwiedzie. (2)
    1. Odcięcie linii fabularnej Drugi Kraloth x Patrycja w formie permanentnej. Niech permanentnie nie stanie się potworem. Niech ma powrót. (3)
    1. Drugi kraloth jakoś się wpakuje w to dziwne Paradoksalne Siluriowe Dildo w Mordowni (3)

# Streszczenie

Okazało się, że Bójka jest pod kontrolą Drugiego Kralotha. Zniknęła. War Room KADEMu i Melodia / Laragnarhag wykazali, że Bójka jest bezpieczna... najpewniej posłuży jako matka dla kralotha. Sam Drugi Kraloth nie jest już kralothem, jest Czymś Innym, za daleko zaadaptował. Idąc dalej, Siluria doszła do tego, że Miranda Maus gra na zniszczenie Mausów / uwolnienia się od Karradraela. Maria ma problemy przez policję ORAZ prokuraturę magów; Łajdak obiecał się tym zająć. Ze wszystkim chyba powiązany jest lokalny Klasztor - Klasztor Zrównania w Lesie Stu Kotów.

# Progresja

* Glarnohlagh: Bójka spawnuje mu nowego kralotha (guaranteed future)
* Glarnohlagh: wpakuje się jakoś w Dziwne Dildo Silurii w Mordowni Arenie Wojny (guaranteed future)
* Patrycja Widoczek: nie stanie się permanentnym viciniusem; ma opcję powrotu (guaranteed future)
* Bójka Diakon: spawnuje nowego kralotha (guaranteed future)

## Frakcji

# Zasługi

* mag: Siluria Diakon, poskładała przy użyciu KADEMu i Millennium opowieść o Drugim Kralocie i o roli Bójki w tym wszystkim.
* czł: Rafał Drętwoń, policjant; szuka ofiar Hralglanatha i na razie Elea wprowadziła go w maliny. 
* mag: Elea Maus, silnie współpracuje z Silurią w sprawie Mirandy i policjanta. Zniknął jej magiczny stetoskop; nie Maria go zabrała. Bardzo martwi się Mirandą.
* mag: Edwin Blakenbauer, neuronauta i lekarz najwyższej klasy - zdobył doskonałe informacje dla Silurii i odkrył co się działo dzięki skanowi na Mare Felix.
* mag: Marian Łajdak, przejął na siebie od Silurii problemy związane z Prokuraturą Magów i policją. Oczywiście, ma zamiar outsourcować.
* mag: Melodia Diakon, silnie współpracuje z Laragnarhagiem i uczestniczy w War Room KADEMu. Zapewnia potrzebną wiedzę o kralothach dzięki Laragnarhagowi.
* vic: Laragnarhag, przeewoluował w biolab w KADEM Primus, by móc dojść do danych o Drugim Kralocie.
* mag: Julian Strąk, Inkwizytor Prokuratury Magów szukający ofiar Hralglanatha. Nie dopuści do złamania Maskarady ani do cierpienia ludzi.
* mag: Marek Kromlan, ostrzeżony przez Silurię, że Drugi Kraloth nie jest kralothem. Był, ale nie jest. Koordynują swoje działania by nie doszło do tragedii.

# Plany

* Krystalia Diakon: ktoś nadużył jej dobrego imienia. Osobiście zaangażuje się w znalezienie Bójki, bogowie i magowie be damned...
* Roman Bruniewicz: poprowadzić Ogniste Niedźwiedzie do Klasztoru Zrównania. Wyjaśnić im, że tak się nie godzi przez mały Zastrasz.
* Rafael Diakon: ostrzec Silurię przed tym, że Krystalia próbuje znaleźć Bójkę. Krystalia może Silurii bardzo napsuć.
* Patrycja Widoczek: nie chce odejść z Ognistych Niedźwiedzi. Chce je zmienić na "dobry gang".

## Frakcji

* Prokuratura Magów: przyjrzeć się tematowi Marii Przysiadek i Hralglanatha. Nie dopuścić do złamania Maskarady. Odbudować życia zniszczone przez kralotha.
* Ogniste Niedźwiedzie: przeprowadzić działania przeciwko Klasztorowi Zrównania. Mały Terror Site.

# Lokalizacje

1. Świat
    1. Faza Daemonica
        1. Mare Felix, gdzie - jak się okazało - Edwin neuronautował Grzegorza w tym samym czasie co Whisper miała Symulację. I doszło do interferencji.
        1. Mare Vortex
            1. Zamek As’Caen
                1. Kadem Daemonica
                    1. Układ Nerwowy
                        1. Sale Symulacji, War Room gdzie próbowali dojść do tego czym jest "Drugi Kraloth" i wyciągnąć jego główne cechy.
                    1. Skrzydło Medyczne, gdzie są i Grzegorz Nocniarz i Maria Przysiadek. I inne ofiary kralotha. I Edwin z Norbertem, oczywiście.
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Centrum
                    1. Kadem Primus
                        1. Zewnętrzny Krąg, gdzie Siluria, Melodia i Laragnarhag wchodzą we współpracę. I gdzie - mimo Quasar - jest mały biolab KADEMu.

# Czas

* Opóźnienie: 2
* Dni: 2

# Narzędzia MG

## Cel Opowieści

* Sprawdzenie żetonów
* Sprawdzenie rozkładu prawdopodobieństwa

## Po czym poznam sukces

* Udane konflikty
* Obecność sukcesów i porażek

## Wynik z perspektywy celu

* 

## Wykorzystana mechanika

Mechanika 1801, wariant z daty 1803

![Testowa mechanika Inwazji z żetonami, 180327](Materials/180399/mechanics-180327-test.png)

* obecność Eskalacji: 2-2-2.
* obecność żetonów

Wpływ:

* 8 kart balans między graczami; 8-10 kart to zwykle dzień
* każda porażka = +2 pkt wpływu dla gracza
* każdy skonfliktowany sukces = +1 pkt wpływu dla gracza
* każda karta = +1 wpływ dla MG
* każde 5 kart zaokr. w górę: +1 pkt wpływu dla gracza

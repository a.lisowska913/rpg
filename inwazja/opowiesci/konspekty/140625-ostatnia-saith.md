---
layout: inwazja-konspekt
title:  "Ostatnia Saith"
campaign: druga-inwazja
gm: żółw
players: kić, dust, dzióbek
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [140618 - Upadek Agresta (AW, WZ, HB)](140618-upadek-agresta.html)

### Chronologiczna

* [140618 - Upadek Agresta (AW, WZ, HB)](140618-upadek-agresta.html)

### Misja właściwa:

![](UpadekAgresta.jpg)

### Aktualny stan NPC po misji:

![](Moriath_NPC_140626.jpg)

# Zasługi

* mag: Andrea Wilgacz, która dowiaduje się, że tak naprawdę ona i Agrest stali po tej samej stronie. Odkrywa sposób w jaki działa Inwazja.
* mag: Hektor Blakenbauer, który jako Patriarcha nagle staje się centrum uwagi wszystkich... zwłaszcza, że Moriath jest powiązany z Blakenbauerami.
* mag: Wacław Zajcew, który odzyskuje Tatianę i umożliwia Irinie zdobycie potrzebnych informacji od Saith Kameleon. Też, dokonuje coup de grace.
* mag: Marian Agrest, który (jak się okazało) poważnie uszkodził działania Inwazji i oddał życie, by Saith Kameleon mogła dowiedzieć się, w jaki sposób Inwazja przejmuje kontrolę. KIA.
* mag: Saith Kameleon, która pochłonęła Karolinę Maus i przez to musi coraz szybciej pożerać magów by nie stać się agentką Inwazji. Eks*EAM. Po wyjaśnieniu wszystkiego Irinie, poprosiła o śmierć. KIA.
* mag: Maja Kos, efemeryda będąca dowodem bezwzględności Agresta. Zniewolona i karmiona Tatianą przez Kameleon, by ściągnąć Irinę Zajcew. KIA.
* mag: Anna Kozak, porwana przez Saith Kameleon jako źródło żywności (osobowości). Pożarta przez Kameleon. Nigdy nie zostanie już terminuską. KIA.
* mag: Mojra, której tożsamość została ujawniona: Maria Sowińska, spokrewniona z Andrzejem Sowińskim.
* mag: Wojciech Tecznia, który próbował uratować Annę Kozak przed Saith Kameleon (myśląc, że to Dariusz Kopyto) i skończył w szpitalu. Nie mógł zatrzymać Saith.
* mag: Grzegorz Czerwiec, który jest załamany śmiercią Karoliny. "Moriath musi być zniszczony". Poluje na Saith Kameleon i pomaga Zespołowi ją znaleźć. Czyżby agent Inwazji?
* mag: Waldemar Zupaczka, który odnalazł Tatianę Zajcew w bazie Saith Kameleon dla Andrei i Wacława.
* mag: Dariusz Kopyto, który jako jedyny z agentów Agresta został odnaleziony. Nieprzytomny, trafił do Rezydencji Blakenbauerów. Najpewniej stał się agentem Inwazji.
* mag: Benjamin Zajcew, ojciec Tatiany i mąż Iriny. Zgodnie z tym, co mówi Saith Kameleon * agent Inwazji.
* mag: Quasar, która powiedziała Andrei i Irinie dość ponure wieści odnośnie Inwazji. Jak się okazuje, wiedziała sporo o celach Agresta i pracowała z nim nad odnalezieniem agentów Inwazji.
* mag: Juliusz Szaman, który zadzwonił powiedzieć Andrei, że ich mistrz jest ciężko ranny i w szpitalu.
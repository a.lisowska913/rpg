---
layout: inwazja-konspekt
title: "Wywalczone życie Diany"
campaign: rezydentka-krukowa
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [170702 - Miłość przez desperację (PT)](170702-milosc-przez-desperacje.html)

### Chronologiczna

* [170702 - Miłość przez desperację (PT)](170702-milosc-przez-desperacje.html)

## Kontekst ogólny sytuacji

Jednostrzałówka.

## Punkt zerowy:

Pytania MG do siebie:

- ?

**Stan początkowy**:

1. Przyszłość Diany i Hektora
    1. HD, Kaja i Ewelina uznają stratę Diany (Diana wybrała), Diana odrzuca przeszłość: 0/10
    1. H, Relacja Diany i przeszłości uległa rozbiciu. Diana zaczyna NAPRAWDĘ od nowa: 0/10
    1. KD, Kaja znajduje Hektorowi miejsce na dworze Eweliny. Hektor ma tam pracę. Diana u Eweliny: 0/10
    1. K, Diana bardziej polega na Kai i Ewelinie niż na Hektorze. Jest z Hektorem, ale na swoich warunkach.: 0/10
    1. HKD, Hektor znajduje dobrą pracę u Apoloniusza. Diana u Prosperjusza. Temat uczuciowy jest nieoznaczony.: 2/10
1. Biznes jak zawsze
    1. GE, Apoloniusz ponosi zdecydowane koszty. Traci maga od eliksirów do Dukata.: 0/10
    1. GE, Prosperjusz i Apoloniusz idą na zdecydowany kurs kolizyjny. Wojna domowa Bankierzy.: 4/10
    1. G, Kaja jest (niesłusznie) oskarżona o to, że jej działania spowodowały straty Apoloniusza.: 0/10
    1. PE, Prosperjusz buduje bezpieczny przyczółek w Praszynie i rozpoczyna swój biznes.: 2/10
    1. E, Do Prosperjusza dołączają inni magowie rodu Bankierz w okolicy Eweliny.: 0/10
    1. E, Kaja buduje bezpieczną sieć detekcyjną dla Eweliny i Pauliny.: 3/10
    1. A, Biznes Prosperjusza nie ma prawa zaistnieć. Potęga Apoloniusza go zatrzyma.: 0/10
1. Naturalna Entropia
    1. Przemęczona i zestresowana Kaja robi poważny błąd. Utrata u Diany i koszty Eweliny:  2/10
    1. Splugawiony konstrukt robi coś, co wymaga interwencji lekarskiej i magicznej:    0/10
    1. Grazoniusz decyduje się dołączyć do Prosperjusza:    2/10
    1. Głos Praszyny wymaga uwagi - znaleźli COŚ.: 2/10
    1. Sąsiedzi Pauliny i mieszkańcy Krukowa zainteresowani Dianą: 4/10
    1. Srebrna Świeca wymaga uwagi - Beata potrzebuje czegoś z regionu (trybut?): 0/10

**Pytania i odpowiedzi**

* Ż: Czemu Beata potrzebuje wzmocnienia?
* K: Z jej reputacją wzmocnienie nie zaszkodzi; nie jest lubiana i łatwo ją wyrufusić.
* Ż: I jak w tym może Paulina pomóc?
* K: Magowie się jej boją z uwagi na jej wiedzę i możliwości. A Paulina może przesłuchać dla niej odpowiedniego trupa.
* Ż: Wybitny spec od eliksirów w firmie Apoloniusza? Zdaniem.
* K: Kompetentna w swojej specjalności, lekko zakręcona, dość młoda. Nienawidzi partactwa. Katia Grajek.
* Ż: Trzy aspekty maga, którego chciałabyś mieć w okolicy (Bankierz).
* K: Proludzki lub nie-ludziokrzywdzący, resourceful, coś bojowego. Bardziej niż Grazoniusz.

## Misja właściwa:

**Dzień 1:**

_Rano: (+1 akcja O)_

Paulina ma pacjenta. Karola Komnata. Karol ma jakąś podłą wysypkę alergiczną. Tymczasem Hektor i Diana są, no... zajęci sobą. I wycieka aura magiczna. Paulina TROCHĘ się tego spodziewała; 4v2->S. Odparła to. Karol... też. Acz coś mu urosło. Jest zakłopotany i zainteresowany Pauliną, ale nic z tego nie ma. Paulina kazała mu się ubrać, dostał coś na alergię (norma, to kwestia nowych spodni - nie uprał) i wysłała go do domu.

Paulina ma dalszych pacjentów, ale młodzi się rozkręcają... zrobiła krótką przerwę i poszła do góry. Nie przeszkadzając im, zdecydowała się na postawienie prostego uziemienia magicznego. 4v2->S, ES. Czas na Miłoszepta. Miłoszept przeszedł przez drzwi i się na nich wydarł - jak to ma być? Jeśli chcą się zachowywać nieobyczajnie, niech to robią. ALE! Niech uziemiają aurę. On ich tego nauczy, pan z panią nie muszą przestawać...

Paulina dusząc śmiech wróciła do pacjentów. (+1 Diana x Hektor, wszystkie ścieżki).

_Południe: (+1 akcja O)_

Paulina usłyszała "co się stało?!" od Kai. A Bogumił Miłoszept jej wyjaśnił, że źle nauczyła Dianę, bo ta nie umie ekranować energii podczas seksu. Widząc, że Kaja jest wstrząśnięta tą sytuacją, dodał, że najlepszym nauczycielom się zdarza. Stres Kai rośnie. Miłoszept się rozwiał - opieprz zakończony, misja zakończona. Paulina weszła i poszła za ciosem. Czy Kaja widziała jej mistrza? Widząc ogłupiałą Kaję zauważyła, że Miłoszept czasem robi jej wykłady. 3+4v4->S (cel: zwiększenie stresu). Stres Kai bardzo wzrósł. Wygląda na bardzo nieszczęśliwą. Zaraportowała Paulinie, że ma już podstawowe sensory w kilku miejscach; ale musi mieć Dianę by podpiąć je do telewizora.

Paulina uknuła sprytny plan. Kaja jest zdecentrowana. Niech porozmawia z Dianą TERAZ - powinno dojść do kłótni. Diana nakręcona, Kaja niestabilna... Paulina udostępni im miejsce na podłączanie sensorów i da okazję do rozmowy. I nastawiła się na kupienie nowego telewizora - najpewniej go straci. 3+2v4->SS. Miała rację. Straci to TV. Paulina siedzi u góry i z lubością podsłuchuje kłótnię. +1 stres Kai, +1 Diana x Hektor, -1 Diana x Kaja. Pokłóciły się o Hektora. A Kaja jest 6/10 na stresie.

_Wieczór: (+1 akcja O)_

Paulina wysłała Kaję po telewizor. Chciała zapłacić, ale Kaja powiedziała, że Ewelina zapłaci w ramach kosztów niekompetencji swojej służby. Paulina zastanawiała się czy przyjąć - jednak tak. Hektor wrócił od Apoloniusza; powiedział, że tajemniczy konstrukt wpadł i rozwalił kilka rzeczy Apoloniuszowi. Potłukł KILKA eliksirów i sobie poszedł. Wszystko wskazuje na Prosperjusza. Apoloniusz jest wściekły.

Do Pauliny przyszedł Grazoniusz. Szuka Kai. Podejrzewa, że przez nią zniknął jakiś konstrukt. Paulina od razu skojarzyła z problemami Apoloniusza. Niech poczeka aż Kaja wróci. I wróciła z telewizorem. SMART Tv. Wypasiony i nowoczesny. Grazoniusz od razu ją zaatakował.

* Witam suczkę Eweliny - Grazoniusz do Kai, szyderczo
* Witam najwybitniejszego wojownika w okolicy - Kaja, z pokorą
* Dobrze, że znasz swoje miejsce - Grazoniusz
* Ja mówiłam do czarodzieja Hektora. Nie wiem kim Ty jesteś. - Kaja, z uśmiechem.

Grazoniusz wydarł się na Kaję, że zepsuła konstrukt od Prosperjusza i teraz on nie wie gdzie ten konstrukt jest. Kaja zauważyła, że nic o tym nie wie i nie miała z tym nic wspólnego. Paulina zwróciła się do Kai - skoro Grazoniusz uważa, że ona coś mu zrobiła, niech ona pokaże działanie sieci. Paulina chce dowiedzieć się, co ta sieć ROBI. Hektor i Diana dołączyli. Grazoniusz też. A Kaja jest zmęczona i zestresowana. 3+4v6->SS. Kaja przyznała, że ta sieć służy i Paulinie i Ewelinie; jest tylko detekcyjna. Ale trudno ją usunąć. Kaja też dodała, że Prosperjusz ma wsparcie Eweliny; ona nie chce by Prosperjuszowi się NIE udało. Chce by mu się udało, by "wybitni magowie rodu Bankierz, jak Ty, Grazoniuszu, nie musieli marnować swego życia na dworze Jej Ekscelencji Eweliny i znaleźli sobie zajęcie godne ich pozycji".

Paulina się żachnęła. Więcej Bankierzy. Sam ściek. To trzeba zatrzymać!

First thing first. Paulina nacisnęła na Kaję. To JEJ miejsce, JEJ baza. Nie życzy sobie takiej sieci. Nie na tej zasadzie. Nie chce zatrutych podarunków. Ufała intencjom Eweliny, a tu taka sytuacja. 1+5v6->S. Kaja chciała coś powiedzieć. Spojrzała na Dianę. Chłodna kalkulacja - nie wykonać zadania lub stracić przyjaciółkę. Uznała, że zadanie jest mniej ważne. Ewelina wybaczy. Ścieżka abortowana - Kaja nie będzie pracować nad sensorami. Stres jeszcze wyższy.

Kaja powiedziała, że oddali się by móc rozmontować sensory. Nie ma czasu na sen; nie może sieć się ukonstytuować sama. Grazoniusz powiedział, że nie skończył rozmowy; Kaja powiedziała, że zatem niech idzie z nią. I poszedł. Lol?

_Noc: (+1 akcja O)_

Paulina się położyła do łóżka. Chwilę później, jak już miała spać... pojawił się Bogumił. Poinformował Paulinę, że nie ma wycieku - to jest sukces. Paulina się ucieszyła. Wyciek jest na tyle słaby, że nieistotny... +1 Diana x Hektor.

**Dzień 2:**

_Rano: (+1 akcja O)_

Przyszła Kaja. Wygląda elegancko jak zawsze, ale upiornie zmęczona. Paulinie zrobiło jej się Kai żal, podała jej coś do picia ze środkami usypiającymi. A Kaja na stymulantach. Kaja się trzyma na nogach, ale padnie. Powiedziała, że porozmawiać musi z Dianą... a Paulina przypadkowo ją otruła (osłabiła).

Kaja powiedziała Dianie, że Ewelina umożliwiła Hektorowi znalezienie zajęcia na dworze Eweliny. Więc Diana może się tam bez problemu sprowadzić z Hektorem. I wiedziała o rodzinie Diany - mówiła nawet o tym. Do teraz wspiera tą rodzinę; to nie takie duże koszty a Dianie na tym zależało...

Paulina zauważyła, że Kaja odwraca sytuację. Nie może to się stać. To niebezpieczne. Paulina zauważyła, że Ewelina załatwiła sobie chętną i zgodną - ale niewolnicę. Nie pokazując dowodów. A pewne zachowania się przenoszą... tu wszedł Hektor. Kaja wiedziała, że to nieprawdziwa rodzina? I nic nie zrobiła? Kaja odbiła, że ta rodzina to wszystko, co Diana miała. Hektor zauważył, że to okrutne. Kaja, że to wszystko nie ma znaczenia. Paulina - właściwym było ją wydobyć i uwolnić. Nie zniewolić ponownie.

* Lady Ewelina Bankierz była młoda, gdy to wszystko się stało. - Kaja, ledwo trzymając się na nogach
* Ja też... - Diana, cicho.
* A jak Tobie 'pomogła' Ewelina Bankierz? - Paulina, lekko szyderczo

3+5v6->SS. Kaja krzyknęła, że była małą dziewczynką, która źle zaufała i uciekła z domu. Pewien Sowiński się nią "zaopiekował". Kaja cierpiała. Pewnego razu wystąpiła przeciw Ewelinie Bankierz. Potem ją okradła raz czy dwa by zdobyć dowody. W końcu ją Ewelina złapała, wysłuchała, dała szansę. Pomogła i osłoniła przed Sowińskim. Więc niech Paulina nawet nie próbuje, bo NIC nie rozumie. Danie Dianie iluzji było tym, czego Kaja nigdy nie miała szans dostać! (Diana jest PRZERAŻONA zachowaniem Kai).

Kaja w końcu padła ze zmęczenia. Diana patrzy przerażona i tylko łzy jej płyną. Ona nigdy nie widziała skażonego serca Kai. Hektor ją przytula i pociesza, idzie za ciosem. Nie mogą tam iść, nie do Eweliny. To miejsce jest chore... a Paulina przebadała Kaję i usunęła jej te stymulanty. Kaja wyraźnie ciężko pracowała tą noc. Paulina podała jej dawki usypiających leków; niech jeszcze dłużej pośpi. Do następnego dnia.

_Południe: (+1 akcja O)_

Paulina zaproponowała Hektorowi i Dianie potańcówkę wieczorem. Diana poprosiła Paulinę o jakąś suknię czy coś - powoli rozrywa więzy z przeszłością. Paulina się z radością zgodziła.

_Wieczór: (+1 akcja O)_

Potańcówka. Wszyscy bawią się dobrze.

_Noc: (+1 akcja O)_

**Dzień 3:**

_Rano: (+1 akcja O)_

Kaja się obudziła w miarę normalnie. Niestety, to co było między nią a Dianą przestało istnieć. Nie ze strony Kai - ze strony Diany. Diana postanowiła, że jednak spróbuje żyć z Hektorem, sama, na własny rachunek. Niech Kaja jej nie namawia, to nic nie da. Kaja przyjęła to ze spokojem. Wie, kiedy przegrała.

Dodatkowo Paulina poprosiła Kaję o rozmontowanie wszystkiego. Kaja odebrała właściwe odczucie, że nie jest już tak naprawdę mile widziana przez nikogo. Ze spokojem zdecydowała się zrobić o co ją proszą i opuścić ten obszar.

_Epilog_

Paulina wygrała wszystko co chciała.

**Podsumowanie torów**

1. Przyszłość Diany i Hektora
    1. HD, Kaja i Ewelina uznają stratę Diany (Diana wybrała), Diana odrzuca przeszłość: 0/10
    2. H, Relacja Diany i przeszłości uległa rozbiciu. Diana zaczyna NAPRAWDĘ od nowa: 10/10    MAX
    3. KD, Kaja znajduje Hektorowi miejsce na dworze Eweliny. Hektor ma tam pracę. Diana u Eweliny: 2/10
    4. K, Diana bardziej polega na Kai i Ewelinie niż na Hektorze. Jest z Hektorem, ale na swoich warunkach.: 3/10
    5. HKD, Hektor znajduje dobrą pracę u Apoloniusza. Diana u Prosperjusza. Temat uczuciowy jest nieoznaczony.: 2/10
2. Biznes jak zawsze
    1. GE, Apoloniusz ponosi zdecydowane koszty. Traci maga od eliksirów do Dukata.: 1/10
    2. GE, Prosperjusz i Apoloniusz idą na zdecydowany kurs kolizyjny. Wojna domowa Bankierzy.: 7/10
    3. G, Kaja jest (niesłusznie) oskarżona o to, że jej działania spowodowały straty Apoloniusza.: 3/10
    4. PE, Prosperjusz buduje bezpieczny przyczółek w Praszynie i rozpoczyna swój biznes.: 3/10
    5. E, Do Prosperjusza dołączają inni magowie rodu Bankierz w okolicy Eweliny.: 2/10
    6. A, Biznes Prosperjusza nie ma prawa zaistnieć. Potęga Apoloniusza go zatrzyma.: 0/10
    1. E, Kaja buduje bezpieczną sieć detekcyjną dla Eweliny i Pauliny.: 5/10   ABORTED
3. Naturalna Entropia
    1. Przemęczona i zestresowana Kaja robi poważny błąd. Utrata u Diany i koszty Eweliny: 7/10
    2. Splugawiony konstrukt robi coś, co wymaga interwencji lekarskiej i magicznej: 0/10
    3. Grazoniusz decyduje się dołączyć do Prosperjusza: 6/10
    4. Głos Praszyny wymaga uwagi - znaleźli COŚ.: 5/10
    5. Sąsiedzi Pauliny i mieszkańcy Krukowa zainteresowani Dianą: 5/10
    6. Srebrna Świeca wymaga uwagi - Beata potrzebuje czegoś z regionu (trybut?): 0/10

**Interpretacja torów:**

Bankierze: Prosperjusz + Grazoniusz są na kursie kolizyjnym z Apoloniuszem a Ewelina w tle się uśmiecha. Z drugiej strony Ewelina straciła Dianę ostatecznie; Kaja była przemęczona i zrobiła kilka błędów. Nie udało jej się zdobyć przyczółka ani oczu na tym terenie. Ludzie w okolicy Pauliny zaczęli się trochę bardziej interesować Dianą; tym lepiej, że Diana i Hektor się wyprowadzili.

**Przypomnienie Komplikacji i Skażenia**:

- Bogumił Miłoszept. Oczywiście, pojawił się ponownie ;-).

# Progresja

* Kaja Maślaczek: traci Dianę jako przyjaciółkę
* Diana Łuczkiewicz: odrzuca Kaję, Ewelinę, stare życie i zaczyna nowe życie z Hektorem
* Hektor Reszniaczek: pracuje u Apoloniusza Bankierza i stabilizuje się na obszarze Powiatu Pustulskiego z Dianą.

# Streszczenie

Ewelina straciła Dianę ostatecznie; Kaja była przemęczona i zrobiła kilka błędów, pokazując Dianie poziom degradacji swojego serca. Ewelinie nie udało się zdobyć przyczółka ani oczu na tym terenie. Ludzie w okolicy Pauliny zaczęli się trochę bardziej interesować Dianą; tym lepiej, że Diana i Hektor się wyprowadzili i mieszkają niedaleko razem. Oby na zawsze szczęśliwie. Tymczasem Prosperjusz + Grazoniusz wchodzą na ostry kurs kolizyjny z Apoloniuszem.

# Zasługi

* mag: Paulina Tarczyńska, pozornie nic nie wiedziała, ale to jej działania doprowadziły do tego, by Diana odrzuciła Kaję i Ewelinę i związała się z Hektorem. Wymanewrowała wszystkich.
* czł: Karol Komnat, elektronik z alergiczną wysypką przez wyciek magii Diany i Hektora mający pewne pobudzenie. Paulinie udało się go nie upokorzyć i dać mu lek na wysypkę.
* vic: Bogumił Miłoszept, ponownie nieumyślnie sprowadzony mocą Pauliny by pouczać młodych jak ekranować się podczas seksu by nie było wycieków energii. Skuteczny.
* mag: Diana Łuczkiewicz, zobaczyła prawdziwe serce Kai. Odrzuciła ją, Ewelinę i przeszłość by zostać z Hektorem. Skonfrontowała się ze swoją przeszłością i wyszła na prostą.
* mag: Hektor Reszniaczek, zaczął pracę u Apoloniusza. Zaczyna pracować nad manierami i podejściem; ma Dianę jako swoją ukochaną i nie musi nikomu nic udowadniać.
* mag: Kaja Maślaczek, która chciała zadowolić wszystkich - Ewelinę, Paulinę, Dianę - i przegrała wszystko. Wróciła do Eweliny, straciła przyjaciółkę i nie wykonała zadania. Kocha Dianę jak siostrę, ale ją straciła.
* mag: Prosperjusz Bankierz, mający pewne wsparcie Eweliny (o dziwo) i otwierający swój biznes. Idzie na kurs kolizyjny z Apoloniuszem. Ma wsparcie Grazoniusza.
* mag: Grazoniusz Bankierz, jak zwykle poprztykał się z Kają. Dołączył do sił Prosperjusza. Ustabilizowany i spokojny; nie jest już problemem dla nikogo w okolicy.

# Lokalizacje

1. Świat
    1. Primus
        1. Mazowsze
            1. Powiat Pustulski
                1. Krukowo Czarne
                    1. Gabinet Pauliny, gdzie doszło do rozwiązania tragedii w pięciu aktach - Ewelina (przez Kaję) straciła Dianę.

# Czas

* Opóźnienie: 0
* Dni: 3

# Starcia


## Przyszłość Diany i Hektora

### Strony z motywacjami:

* Diana
    * Chce być z Hektorem, ale nie stracić relacji z Eweliną / Kają. Nie wie co robić z "rodziną".
* Hektor
    * Chce być z Dianą. Chce wyrwać ją z jej chorego świata i oderwać ją od jej przeszłości. New start.
    * Chce zarabiać i być przydatnym i niezależnym magiem.
* Kaja
    * Przyjaciółka Diany. Nienawidzi magów mężczyzn. Chce uratować Dianę przed życiowym błędem.
    * Jeśli trzeba, przeboleje Hektora by nie stracić Diany. Kocha Dianę jak siostrę.

### Temat Starcia:

Jak Hektor i Diana będą zarabiać? Jakie będą mieć relacje z Eweliną? Czy Diana opuści przeszłe życie? Czy Hektor rozerwie Dianę z Kają?

### Ścieżki:

1. Przyszłość Diany i Hektora
    1. HD, Kaja i Ewelina uznają stratę Diany (Diana wybrała), Diana odrzuca przeszłość: 0/10
    1. H, Relacja Diany i przeszłości uległa rozbiciu. Diana zaczyna NAPRAWDĘ od nowa: 0/10
    1. KD, Kaja znajduje Hektorowi miejsce na dworze Eweliny. Hektor ma tam pracę. Diana u Eweliny: 0/10
    1. K, Diana bardziej polega na Kai i Ewelinie niż na Hektorze. Jest z Hektorem, ale na swoich warunkach.: 0/10
    1. HKD, Hektor znajduje dobrą pracę u Apoloniusza. Diana u Prosperjusza. Temat uczuciowy jest nieoznaczony.: 2/10

## Biznes jak zawsze

### Strony z motywacjami:

* Gabriel Dukat
    * Nie będzie na tym terenie wchodzić mi nowa siła Bankierzy. To utrudni moje interesy.
    * Ewelina musi zapłacić za swoje czyny. Więc, Kaja musi zapłacić.
* Apoloniusz Bankierz
    * Jakim prawem ktoś rozwala biznes, jaki ja zrobiłem? Lepiej jednak to zatrzymać...
* Prosperjusz Bankierz
    * Wyrwę się spod szponów durnej Eweliny i sobie ją wezmę siłą.
* Ewelina Bankierz
    * Prosperjuszowi musi się udać. Musi zniszczyć Apoloniusza.

### Temat Starcia:

Czy uda się Dukatowi zrzucić podejrzenia na Kaję i rozbić siły Prosperjusza? Czy Prosperjusz da radę ściągnąć Grazoniusza? Czy Ewelinie uda się wysłać więcej Bankierzy Prosperjuszowi? Czy Apoloniusz obroni się biznesowo?

### Ścieżki:

1. Biznes jak zawsze
    1. GE, Apoloniusz ponosi zdecydowane koszty. Traci maga od eliksirów do Dukata.: 0/10
    1. GE, Prosperjusz i Apoloniusz idą na zdecydowany kurs kolizyjny. Wojna domowa Bankierzy.: 4/10
    1. G, Kaja jest (niesłusznie) oskarżona o to, że jej działania spowodowały straty Apoloniusza.: 0/10
    1. PE, Prosperjusz buduje bezpieczny przyczółek w Praszynie i rozpoczyna swój biznes.: 2/10
    1. E, Do Prosperjusza dołączają inni magowie rodu Bankierz w okolicy Eweliny.: 0/10
    1. E, Kaja buduje bezpieczną sieć detekcyjną dla Eweliny i Pauliny.: 3/10
    1. A, Biznes Prosperjusza nie ma prawa zaistnieć. Potęga Apoloniusza go zatrzyma.: 0/10


## Moloch i Entropia

### Strony z motywacjami:

* brak

### Temat Starcia:

* brak

### Ścieżki:

1. Naturalna Entropia
    1. Przemęczona i zestresowana Kaja robi poważny błąd. Utrata u Diany i koszty Eweliny:  2/10
    1. Splugawiony konstrukt robi coś, co wymaga interwencji lekarskiej i magicznej:    0/10
    1. Grazoniusz decyduje się dołączyć do Prosperjusza:    2/10
    1. Głos Praszyny wymaga uwagi - znaleźli COŚ.: 2/10
    1. Sąsiedzi Pauliny i mieszkańcy Krukowa zainteresowani Dianą: 4/10
    1. Srebrna Świeca wymaga uwagi - Beata potrzebuje czegoś z regionu (trybut?): 0/10


# Narzędzia MG

## Opis celu misji

* Za notatką: [Notatka_170716](/mechanika/notes/notatka_170716.html) zmieniamy kartę Pauliny.
* Konflikty są na (3, 5, 7, 10, 15). Każdy ma [1,3] dominujące aspekty negatywne, które zniwelowane obniżą stopień trudności o '2' każdy.
* Konflikty na [-1,1] (przegrane o 1, remis, wygrane o 1) są CZĘŚCIOWYMI sukcesami. Obie strony osiągają cel lub osiąga się cel niekompletny. To jest próba implementacji mechanizmu z Apocalypse World o równoległości działań postaci; alternatywne interesujące podejście jest w Mouse Guard.

Oraz przeprojektowanie Frakcji w Starcia: każde Starcie to kilka stron które walczą o WARIANT przyszłości. Rozpatrywane są różne warianty przyszłości.

## Cel misji

1. O-(...)-O, gdzie O to operacyjne (wzrost ścieżki). Nie dodajemy nowej ścieżki.
    1. O: przeciwnik dał radę podnieść JAKĄŚ ścieżkę. Jeden lub wszyscy. Generowana jest nowy Postęp Przeciwnika.
1. Dzień składa się z 4 faz, domyślnie. Normalne postacie w jednej fazie śpią ;-).
1. Nowy model postaci tempek pokaże, że lepiej jest opisana i ogólnie lepiej i szybciej działa.
1. Konflikty 2,4,6,9,15
1. Poziom częściowego sukcesu [-1,1]

## Po czym poznam sukces

-

## Wynik z perspektywy celu

1. Operacyjne komplikacje i brak Strategicznych
    1. Ż: 
    1. K: 
1. Cztery fazy dnia
    1. Ż: 
    1. K: 
1. Nowy model postaci
    1. Ż: 
    1. K: 
1. Konflikty 2,4,6,9,15 i kontraspekty
    1. Ż: 
    1. K: 
1. Częściowy sukces
    1. Ż: 
    1. K: 
1. Inne
    1. Ż: Aspekty. Gracze ich nie widzą. Nie używają. Nie widzą że mogą.

## Wykorzystane tabelki

Postać (strona gracza):

| .Motywacja. | .Siły i słabości. | .Specjalizacja. | .Umiejętność. | .Szkoła Magii. |.POSTAĆ. |
|             |                   |                 |               |                |         |

Opozycja (strona NPC):

| .VERSUS. | .Aspekty. | .Skala. | .Magia. |.Pomysł gracza. | .OPOZYCJA. |
|          |           |         |         |                |            |

Wynik:

| .Postać. | .Opozycja. | .Rzut.    | .Wynik.  |
|          |            | xx: +x Wx |   S-SS-F |

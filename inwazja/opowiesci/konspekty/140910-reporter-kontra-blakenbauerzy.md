---
layout: inwazja-konspekt
title:  "Reporter kontra Blakenbauerzy"
campaign: anulowane
gm: żółw
players: kić, dust, dzióbek
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja:

## Punkt zerowy:

I->K: Dlaczego reporter stanowi poważny problem dla Blakenbauerów?
K: Bo podejrzewa Edwina i koniecznie się chce do niego przywalić. 
I->D: Czego konkretnie stara się dowiedzieć reporter?
D: Co się dzieje z kobietami Blakenbauerów.
I->B: Dlaczego Hektor, Paulina i Klemens współpracują w tej kwestii?
B: Powodem jest stwór podczas zamieszek.
K->I: Dlaczego Margaret bardzo zależy, by reporter jej nie zobaczył?
I: Bo skojarzy złamanie Maskarady którego już był świadkiem.
D->I: Skąd Edwin dowiedział się, że ktoś szpera w historii Blakenbauerów pod kątem córek?
I: Bezpośrednio od reportera.
B->I: Dlaczego nikt z Blakenbauerów nie jest świadomy, że jest z nimi spowinowacony?
I: Bo u niej klątwa zadziałała zupełnie inaczej.
I->K: Czarny koniec dla Projektu "Moriath":
K: Projekt Moriath zostanie ujawniony niepowołanym osobom.
I->D: Czarny koniec dla reportera:
D: Na zawsze stanie się częścią Rezydencji.
I->B: Czarny koniec na płaszczyźnie politycznej:
B: Otton wycofuje się z polityki i wyznacza następcę. Oficjalnie Projekt Moriath przechodzi pod kuratelę Ciotki.
I->K: Jaki damage control ma wprowadzić Hektor z Klemensem odnośnie spotkania Marii z Kingą (które już nastąpiło).
K: Mają przekierować ich zainteresowania na coś innego niż Blakenbauerowie dyskretnie i z wyczuciem.
I->D: Czemu Paulinie bardzo zależy, by Marii nic się nie stało.
D: Bo są soulmate'ami.
I->B: Dlaczego w pewien sposób Paulina zapewnia Wam bezpieczeństwo we wszystkich interakcjach z Marią.
B: Pozwala na sferę prywatną rozmów z Marią.
I: Co złego stanie się Blakenbauerom, jeśli Maria zginie (jej zdaniem)
K: Paulina jest jej dead man's switchem. Śmierć Marii przekaże zestaw ZŁYCH informacji do opinii publicznej - wykorzystywanie ludzi i magów.
I: Co złego stanie się Blakenbauerom, jeśli Maria trafi do niewoli (jej zdaniem)
B: Paulina i jeszcze 2 dead man's switch - auto-wysłana wiadomość do SŚ z informacjami. Jeśli to nie pomoże, Maria udostępni materiały udowadniające że Blakenbauerowie złamali Maskaradę.
I: Co złego stanie się Blakenbauerom, jeśli Maria zostanie na wolności (jej zdaniem)
D: Ujawni, co się dzieje z kobietami w rodzie Blakenbauerów.
I: Który z tych trzech wariantów byłby dla Blakenbauerów najlepszy:
D: Najlepszy dla Blakenbauerów - Maria zostaje "pod opieką" (w niewoli).
I: Czemu Smok nie może poradzić nic na działania Blakenbauerów z Marią.
D: Ponieważ Edwin potrafi go kontrolować.
I: Dlaczego Smok jest poza zasięgiem wszystkich, nawet Mojry.
K: Bo Ciotka go ochrania.
I: Jak Smok może skrzywdzić Blakenbauerów.
B: Smok ma z Blakenbauerami układ typu MAD.

## Misja właściwa:

01. Hektor ponuro ogląda program w TV Kopalin prowadzony przez reporter Marię Newę. Maria Newa opisuje spokojnie zamieszki przed Rezydencją Blakenbauerów uwzględniając przypisanie im winy za zranienie człowieka (eks-kochanki Edwina, Laury Filut). W programie też mówi o tym, że w Rezydencji dzieją się straszne rzeczy i ponure eksperymenty na zwierzętach. A dlaczego nikt nic nie wie? Bo prokurator Hektor Blakenbauer jest prokuratorem i nie pozwoli, by jego rodzinie stała się krzywda. Maria opisała też "dramat kobiet Blakenbauerów" - trzymane pod kluczem, więzione, nikt ich nie widuje. Przyrównała to do fundamentalistów religijnych. Próbowała rozmawiać z Edwinem, ale on odmówił komentarza. 
Program się skończył, Hektor nadal ponury.
02. Hektor udał się do szefa ochrony Rezydencji, Borysa. Powiedział Borysowi, że Maria ma absolutny zakaz zbliżania się do Rezydencji. Borys dodał Hektorowi nową informację - Maria rozmawiała z Kingą. Nie ucieszyło to Hektora. Kiedy? Po tym, jak Kinga została wyproszona z Rezydencji. Otton dał Hektorowi poważne zadanie - rozwiązać sprawę z Kingą.
03. Tymczasem w kawiarni siedzą Maria Newa i Paulina. Poprzez mindlink dyskutują o zamieszkach i o tym, jak bardzo Maria się naraża atakując Blakenbauerów. Paulina zaznacza, że Edwin jest przecież bardzo pomocnym lekarzem, jednym z lepszych. Maria kontruje, że Blakenbauerowie to okrutny i plugawy ród magów i ona, Maria, dopilnuje by koszmar przez nich robiony się skończył. Mówi Paulinie o "Trzmielach", dodaje, że Blakenbauerowie porywają ludzi i robią na nich jakieś eksperymenty. Zapytana, czy się nie boi odpowiedziała, że chroni ją mistrz Shaolin - Smok we własnej osobie. Paulina jest sceptyczna co so poziomu ochrony. Pomoże Smokowi w ochronie Marii.
04. Hektor umówił się na spotkanie z Kingą. Po cholerę tam szła do Rezydencji i co powiedziała Marii? Hektor wszedł twardo, po inkwizytorsku i Kinga rozpłynęła się we łzach. Okazało się, że Kinga jest stalkerem Hektora; chodzi za nim, zainteresowała się nim, on jest "jej życiem" (na ten etap nikt nie wie, że Kinga - Hektor mają nieaktywny soulmate). Co powiedziała Marii? Widziała eksperymenty na ludziach w laboratorium Rezydencji.
Tu pojawiło się pytanie, czemu Rezydencja ją wpuściła i nie zadziałały alarmy; nikt go jednak nie zadał właściwym instancjom.
Hektor stwierdził, że tak być nie może. Kinga zdecydowała się nacisnąć mocno i spróbowała wymusić na Hektorze, by zostali parą. Hektor "zły rzut" Blakenbauer stanął na wysokości swojego przydomku - spróbują być parą, ale ona musi odejść z prokuratury (bo byłby konflikt interesów). Weszła w to.
05. Mojra poprosiła Klemensa o spotkanie. Tam okazało się, że Klemens, jakkolwiek agent Blakenbauerów, przede wszystkim jest agentem Mojry. Mojra dała Klemensowi nowe zadanie - w świetle działań reporter Marii Newy on, Klemens, ma się upewnić że projekt Moriath pozostanie w ukryciu. Decyzja o sposobie rozwiązania problemu leży w rękach Klemensa. Klemens się zgodził pod warunkiem, że Mojra upilnuje, żeby Marcelinowi coś się nie stała i żeby nie zrobił jakiejś głupoty. Mojra powiedziała, że może jej to zostawić.
06. Klemens zlokalizował Marię i zaczął ją śledzić. Doprowadziło go to do kolejnego spotkania Marii i Pauliny. Używając niesamowicie high tech narzędzi (szklanka przy drzwiach) dowiedział się, że Marię interesuje szczególnie Margaret. Maria powiedziała Paulinie, że duża część jej źródeł to wrogowie Blakenbauerów, ale Maria ma swoje własne informacje ponad to. Zdaniem Marii, "ta od eksperymentów" to właśnie Margaret i ona jest głównym celem Marii. Paulina powiedziała Marii, że może umówić spotkanie Maria - Marcelin i Marcelin może doprowadzić Marię do Margaret. W wyniku konfliktu Paulina - Marcelin ogromny sukces - Marcelin spotka się z Marią. Klemens może jeno bezsilnie słuchać...
07. Tymczasem Hektor udał się do Ottona i powiedział mu, że zadanie zostało wykonane. Ale - Kinga będzie na obiedzie. Otton przyjął tą wiadomość z chłodnym spokojem.
08. Marcelin się pojawił na spotkaniu z Marią i Pauliną, ku zdziwieniu i niezadowoleniu Klemensa. Za to zaczęło dziać się coś dziwnego - Maria zaczyna mieć bardzo silną, ekhm, reakcję na Marcelina. Ciągle rosnącą reakcję pożądania. Oraz nienawiści, idącej "od Marii". I to wszystko bez ani jednego zaklęcia. Co gorsza, zaczęło to promieniować przez soulmate na Paulinę.
Paulina nie może sobie na to pozwolić. Nie chce. Nie chce trójkąta w łóżku z Marcelinem, nie tak. Co gorsza, Paulina dostaje tylko echo, Maria po prostu przegrywa w walce z tymi uczuciami. A przecież nie ma użytej żadnej magii a Marcelin nie słynie z tego, by robić takie rzeczy...
Paulina oddaliła się do ubikacji i rzuciła na siebie zaklęcie mające przekształcić ją częściowo w nieumarłą. Udało się. Nie ma tak silnej na niej już reakcji. Następnie Paulina użyła soulmate i zaczęła wysysać z Marii uczucia nienawiści i pożądania, stabilizując ją nieumarłym spokojem. 
O dziwo, udało się. Maria zaczęła się stabilizować.
Klemens to wszystko naturalnie zauważył. Marcelin tym bardziej. Skonfundował się. Sam wycofał się do ubikacji by pomyśleć. Stwierdził, że to wszystko jest mega podejrzane.
Oki, spotkali się ponownie. Marcelin dla odmiany nie poszedł z dziewczynami do łóżka i nie dał z siebie wyciągnąć żadnej ważnej informacji. Za to Klemens i Paulina poczynili pewne obserwacje... 
09. Klemens spotkał się z Mojrą. Powiedział jej z wyrzutem, że miała opiekować się Marcelinem, na co ona powiedziała coś takiego: "Cóż. Mogę powiedzieć, że mi się wymknął, lecz to nie świadczyłoby dobrze o mojej kompetencji. Powiedzmy, że pozwoliłam mu pójść na to spotkanie bo miałam tajny plan".
Zdaniem Mojry Maria właśnie awansowała na bardziej niebezpieczną jednostkę, niż jej się wydawało. Szczęśliwie, Maria ma coś w papierach odnośnie narkotyków. I do tego fałszywa tożsamość. Piękna sprawa, w sam raz dla prokuratury. Jakie to szczęście, że przypadkiem Mojra na to natrafiła...
Klemens wiedział już co robić. Nadał temat Hektorowi. Kochana Mojra <3.
Następnym ruchem Klemensa było udanie się do Edwina. To co widział z Marcelinem i Marią było dziwne. Czyżby Marcelin miał "coś w sobie", jakiś czar rzucony, jakaś choroba... o co chodziło? Edwin połączył się z Rezydencją i kazał przeskanować Marcelina. Marcelin jest czysty, cokolwiek to nie było musiało być w Marii.
Klemens się zamyślił.
10. Paulina i Maria usiadły razem i znowu połączyły się mindlinkami. Paulina zmusiła Marię, by ta wreszcie powiedziała jej o co chodzi. Maria ma... miała siostrę. Ta siostra była jej soulmate. Były bliżej siebie niż ktokolwiek inny - ale siostra była magiem.
W pewnym momencie ta siostra trafiła w ręce Blakenbauerów i oni zaczęli jej coś robić, na niej eksperymentować. I że Edwin był tym, który robił jej rzeczy, jakaś kobieta (?Margaret?) była tą, która zmieniała ją magicznie a Marcelin robił jej krzywdę przychodząc do niej w nocy. I ona, Maria, czuła to wszystko w osłabionej formie przez soulmate. Do momentu aż to co robiła Margaret w końcu "coś" zrobiło i soulmate się rozerwał. Wtedy Paulina próbowała leczyć Marię i soulmate wskoczył na Paulinę...
Maria powiedziała, że wierzy z całego serca, że Smok uratuje jej siostrę. A jeśli coś się stanie, Paulina jest magiem; maga magowie powinni posłuchać.
Przyciśnięta jeszcze mocniej przez Paulinę Maria powiedziała jaki ma plan. Maria chce uruchomić część stworów Rezydencji. Użyć tego co zostało z Trzmiela. Złamać Maskaradę i to broadcastować. Przez sieci znane magom i nieznane. Ma bardzo potężnego sojusznika, którego nazwała "Wiecznym Anarchistą" (czyżby Gustaw Siedeł?)
...jej plan jest bardzo radykalny i bardzo niebezpieczny. Paulina musi coś zrobić, ale co?
11. Spotkanie Klemensa z Hektorem. Klemens ostrzegł Hektora, że Maria interesuje się rodem Blakenbauerów. Powiedział, że Maria ogólnie może celować w ród ponieważ ma kryminalną przeszłość. Ma jakieś powiązania z narkotykowymi ludźmi. Hektor się oburzył - teraz jak ma motyw to jest to bardzo prosta sprawa. Napuścił swoich ludzi na tą sprawę (konflikt 10) i bez kłopotu dostał materiały o Marii. Teraz ma coś, dzięki czemu może ją zniszczyć i zdyskredytować - jest w to zaangażowana i próbuje uderzyć w Hektora przez jego rodzinę. Prosta sprawa.
Umówił się z Marią przez asystenta na następny dzień. Maria jest przekonana, że chcą oddać jej siostrę, Hektor jest przekonany, że Maria przyzna się i on będzie mógł ją aresztować.
12. Paulina spotkała się z Marcelinem w Rezydencji. Paulina pytała Marcelina o Martę (siostrę Marii), ale on nic a nic nie kojarzy. Paulina jest skonfundowana, bo Maria na pewno wierzyła w to co mówiła Paulinie odnośnie Marcelina i jej siostry. Paulina nacisnęła na Marcelina, który obiecał poszukać Marty w Rezydencji. I dokładnie to zrobił.
13. Mojra poprosiła do siebie Klemensa. Faktycznie, Marcelin szukał Marty w Rezydencji. Mojra nie wie nic na temat "Marty", nie kojarzy takiej osoby, ale jeśli faktycznie ta sprawa jest prawdziwa, to znaczy że jeszcze przed projektem Moriath Blakenbauerowie porywali magów i coś im robili. Tego niekoniecznie był świadom Andrzej Sowiński. Więc: po co Blakenbauerom "Marta", a nawet jeśli była potrzebna - gdzie ona jest, czy żyje i czemu Mojrze nikt nic nie powiedział?
Tak czy inaczej, Klemens ma rozwiązać problem Marii w "dowolny sposób jaki uzna za konieczny". Działania Marii za bardzo potencjlanie zagrożą projektowi Moriath, trzeba coś z tym zrobić.
Klemens spytał, czy Mojra może zapewnić jakieś bezpieczne miejsce poza obszarem wpływu SŚ i Blakenbauerów. Mojra potwierdziła. A zatem Klemens ma Plan...
14. Tymczasem Paulina nie odstępowała Marii na krok. Nawet, gdy Maria poszła porozmawiać ze Smokiem. Zatem po raz kolejny Paulina spotkała się ze Smokiem (mimo, że ich ostatnie spotkanie nie było dla Pauliny zbyt optymistyczne). Na spotkaniu Maria mówi Smokowi, że Hektor się chce z nią spotkać, powiedziała mu wszystko co dowiedziała się do tej pory i powiedziała, że Hektor odda jej Martę. Jeśli nie, Smok ma działać. Sam Smok próbuje moderować Marię, żeby niewinni nie ucierpieli, ale Maria jest nieubłagana - jej siostra za długo tam już jest, za bardzo już cierpiała; nie ma szans, trzeba to zrobić. Jej już nie zależy. Powiedziała Smokowi, że ona pójdzie na to spotkanie i przekaże Hektorowi ultimatum. Maria wymieniła sojuszników: Smok, Paulina i "tajemniczy wrogowie Blakenbauerów". Niestety, ani Smokowi ani Paulinie nie udało się uspokoić Marii. Ona już jest "za daleko". Smok i Paulina stwierdzili, że będą ją chronić.
15. Punkt kulminacyjny! Spotkanie Hektora i Marii. Było tak zabawnie, jak się można było spodziewać - przyszli ze swoimi przekonaniami, Maria o siostrze, Hektor o narkotykach, Maria wyparła się narkotyków, Hektor czegokolwiek z siostrą Marii, Maria postawiła ultimatum łącznie ze złamaniem Maskarady a Hektor użył policjantów i aresztował Marię. Mojra, mając informacje od Klemensa błyskawicznie uruchamia sieci powiązań i intryg by zablokować potencjalne złamanie Maskarady a Klemens ma pilnować Marii. Mojrze się udało, choć tym razem było to bardzo trudne.
16. Pierwszy "dead man's hand" jaki Maria zaczęła, by pokazać Blakenbauerom że nie żartuje. Maria poprosiła Paulinę o skontaktowanie jej ze Smokiem - mindlink (niewykrywalny) Maria - Paulina i częściowe (świadome) opętanie. Maria poprosiła Smoka o przekazanie jej kontaktowi - młodemu magowi SŚ - informacji o 5 ofiarach Blakenbauerów (magów SŚ). Informacja została przekazana Smokowi, który natychmiast przekazał je właściwemu magowi. Ten mag natychmiast puścił informację w hipernecie. To było coś, czego Mojrze nie udało się już zablokować; musiała uruchomić zupełnie inny damage control.
Mojra jest wściekła. Oraz nie wie, co jeszcze Maria wymyśliła. No i musi "zniknąć" lub kontrolować w odpowiedni sposób maga SŚ, czego nie chce robić... i nawet nie wie o co chodzi, skąd Maria to wszystko wie i kto za tym wszystkim stoi. Problem Marii musi być rozwiązany natychmiast.
17. Klemens pilnuje Marii. Maria jest w więzieniu. Klemens po prostu wszedł do więzienia, podszedł do Marii i użył teleport beaconu który dostarczyła mu wcześniej Mojra. Maria, Klemens, Mojra, Edwin, Margaret oraz 5 terminusów jest w magazynie w Kopalinie, na odpowiednio przygotowanym przez Mojrę terenie. I przesłuchanie czas zacząć.
Tymczasem terminusi podkładają w więzieniu "kukłę" Marii, by nikt (a już na pewno Hektor) się nie zorientował, że Marii nie ma.
18. Po mindlinku Paulina dostała potwierdzenie od Marii o tym, że faktycznie Margaret jest tą tajemniczą kobietą. Smok krzyknął na Paulinę, by ta go natychmiast "wpuściła", on może uratować Marię - ale Paulina pamiętała jak się to ostatnio skończyło (zarazi ją czymś czy coś) i odmówiła. Zwłaszcza, że Smok zażądał odpowiedzi na pytanie, czy jest tam Edwin ("jeśli tak, nie mogę jej uratować").
Paulina się przestraszyła o Marię i jej bezpieczeństwo. Zdecydowała się rzucić bardzo trudne i bardzo niebezpieczne zaklęcie - korzystając z soulmate link, zdekonstruować zarówno Marię jak i Paulinę i je zrekonstruować, efektywnie zamienić się nimi miejscami, korzystając z okazji że soulmate jest w pewnym stopniu linkiem jednej istoty w dwóch ciałach. W tym samym czasie Margaret zaczęła rzucać zaklęcie głębokiego skanu i kontroli w kierunku Marii - połączenie tych dwóch zaklęć na niestabilnym soulmate link rozerwało połączenie Marii i Pauliny i podpięło Paulinę i Margaret do siebie, zostawiając Marię w tymczasowej katatonii, w rękach Smoka.
19. Mojra natychmiast wysłała 5 terminusów i Klemensa do miejsca gdzie znajduje się Maria. Terminusi namierzyli ją po sympatii (DNA, skóra...) i natychmiast z Klemensem otoczyli to miejsce. Margaret krzyczy, Paulina krzyczy, soulmate się konstytuuje, Rezydencja nie jest w stanie wejść w interakcję by nie uszkodzić całego linku Rezydencji z Blakenbauerami. Otton jest nieszczęśliwy, Mojra jest nieszczęśliwa.
20. Klemens i terminusi otoczyli Smoka. Smok klęczy, trzyma Marię w ramionach i ją delikatnie głaszcze. Gdy Klemens zażądał od Smoka oddanie Marii, ten pocałował ją w czoło i sprawnym ruchem skręcił jej kark. Terminusi - zgodnie z instrukcjami - otworzyli ogień i zabili Smoka. Maria nie żyje.
21. Aftermath: soulmate link się ustanowił między Margaret a Pauliną, z Pauliną jako jednostką silniejszą. Smok jest potwierdzonym viciniusem; na pewno jakoś się wycofał. Rezydencja nie ma informacji na jego temat. Nie wiadomo czym on jest i dlaczego działa przeciw Blakenbauerom. Maria nie żyje na dobre.

# Zasługi

- mag: Hektor Blakenbauer jako prokurator, który NIE WIE.
- mag: Paulina Tarczyńska jako lekarka z uszkodzonym soullinkiem która próbuje uratować Marię przed nią samą.
- czł: Klemens X jako agent Mojry chroniący Marcelina ponad wszystko. Tak bardzo sens.
- czł: Maria Newa jako dziennikarka z uszkodzonym soullinkiem wypowiadająca wojnę Blakenbauerom o odzyskanie swojej siostry.
- mag: Marta Newa jako echo w pamięci Marii; podobno zniewolona czarodziejka której robią straszne rzeczy Blakenbauerowie.
- mag: Laura Filut jako eks-kochanka Edwina. Chwilowo ranna.
- czł: Borys Kumin jako szef ochrony Rezydencji nie mający żadnej istotnej funkcji na tej misji.
- mag: Otton Blakenbauer jako główny rozgrywający po stronie Blakenbauerów którego wszyscy się boją.
- czł: Kinga Melit jako potencjalna dziewczyna Hektora która dla niego rezygnuje z pracy.
- vic: Smok jako ten, który próbuje ochronić Marię przed Blakenbauerami i uderzyć w Blakenbauerów jednocześnie. NIC mu nie wychodzi.
- mag: Mojra jako lojalna czarodziejka której wszystko się rozsypuje w rękach przez to, że ma jedynie jednego agenta któremu może ufać (Klemensa).
- mag: Edwin Blakenbauer jako mag mający silniejszy dostęp do Rezydencji niż się wcześniej wydawało.
- mag: Margaret Blakenbauer jako ofiara splątania soulmate z Pauliną.

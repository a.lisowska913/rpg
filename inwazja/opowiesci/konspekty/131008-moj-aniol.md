---
layout: inwazja-konspekt
title:  "'Mój Anioł'"
campaign: czarodziejka-luster
players: kić
gm: żółw
categories: inwazja, konspekt
---

# {{ page.title }}
## Punkt zerowy:

## Kontynuacja

### Kampanijna

* [130511 - Ołtarz Podniesionej Dłoni (An)](130511-oltarz-podniesionej-dloni.html)

### Chronologiczna

* [130511 - Ołtarz Podniesionej Dłoni (An)](130511-oltarz-podniesionej-dloni.html)

## Misja właściwa:

- Działania Kasi na Indze miały ciekawy efekt uboczny. Inga przypomniała sobie wszystko rano. Poszła - po raz pierwszy od bardzo dawna - do swojego dawnego pokoju dziecięcego (gdzie znajdował się srebrny ołtarzyk) i nie potrafiła znaleźć swojego ołtarzyka. Poprosiła swojego tatę, by ten znalazł jej ołtarzyk a sama (w towarzystwie lokaja) zrobiła coś, czego jeszcze nigdy wcześniej nie robiła - poszła do kościoła.
Mniej więcej w tym czasie tata Ingi, Antoni, dostał solidny OPR za zachowanie córki od Augustyna i Żanny. Nie ucieszyło go to, oj nie...
W kościele Inga odkryła, że dotyk srebra ją rani...
Poszła do konfesjonału porozmawiać z księdzem. Tam upewniła się do co swoich podejrzeń - dziadek nie może być aniołem. Anioł by się tak nie zachowywał. Moc w tym domu nie pochodzi od Boga. A to, w prostym umyśle Ingi, może oznaczać tylko jedno, zwłaszcza w świetle tego, że wszelkie srebrne przedmioty kultu zniknęły...
Oni żyją w kłamstwie. Może od dawna.
[Przypis Żółwia: a teraz trzeba pokazać w lustrze taką scenę Kić, by wiedziała, że coś się dzieje...]

- Z rana August spróbował się skontaktować z Sandrą. Zniknęła z hipernetu (sieć magów SŚ), więc został mu tylko "wulgarny" telefon komórkowy. August powiedział, że chce ją przeprosić. Sandra stwierdziła, że August zranił jej uczucia i oczekuje przeprosin w formie kompotu brukselkowego. Zrobionego własnoręcznie przez Augusta. Rozłączyła się, zanim inny technomanta zdołał ją namierzyć. Jest naprawdę niezła ;-).

- August poszedł na miasto szukać brukselki (w ogrodzie botanicznym czy coś). Kasia nic mu nie powiedziała o pieniądzach, niech się męczy. Sama natomiast zobaczyła w lustrze dwie wizje. Pierwszą, jak Inga wychodzi z lokajem z kościoła, wyraźnie zamyślona i wstrząśnięta. Drugą, gdzie kłócą się rodzice Ingi. Ojciec (Antoni) chce ją przykładnie ukarać za pobicie, matka ją broni. Antoni zwraca też uwagę na to, że ktoś ukradł Indze ołtarzyk dziecięcy. W tym domu. Albo Inga sama go ukradła, by przyciągnąć na siebie uwagę. Matka broni Ingi - to na pewno nie to. To nie ona.
W tej wizji jeszcze Kasia zauważyła, że krzyżyk na szyi Jolanty jest srebrny. Jolanta ma na niego reakcję alergiczną (magiczną), ale Antoni nie ma żadnej reakcji.

- Kasia rozpoczęła malowanie specjalnego obrazu. Nazwała go "Mój Anioł". Przedstawia postać ze skrzydłami, połowa postaci ukryta jest w cieniu (jakby przekraczał granicę). Tam, gdzie pada światło, jest to anioł (czysty, świetlisty...). Tam, gdzie jest cień, jest zakłócone. Delikatnie. Nieznacznie. Wystarczająco, by wzbudzać niepokój. Twarz anioła w świetle ma dobrotliwy uśmiech. W cieniu, oko ma lekki poblask złem. Nic oczywistego. I na obrazie umieściła lekki, delikatny szkic medalionu, który ma Augustyn Szczypiorek na piersi, który służy do synchronizacji z ołtarzem. Gdy Kasia nie pracuje nad obrazem, obraz jest zakryty, by August się niczego nie domyślił.
Tymczasem Inga zorientowała się, że w domu nie ma (przynajmniej na widoku) w ogóle nic srebrnego. Poza krzyżykiem na szyi matki.
Wróciła do domu Samira. Cała jest podekscytowana. Udało jej się znaleźć okazję do doskonałego występu tego wieczoru - "pojedynek" trzech iluzjonistów. Ma pomysł na nową sztuczkę - niech Kasia pomaluje jej lustro dziwnymi, fantazyjnymi wzorami lekko widocznymi tylko w UV, a ona za to przygotuje dla nich obiad. Umowa stoi.
Kasia ruszyła do przygotowania lustra a Samira siadła do pracy nad obiadem.

- Południe. Samira pracuje nad obiadem a Kasia szpieguje Ingę przez lustra. Udało jej się zobaczyć dwie ważne sceny. Po pierwsze, Inga wyraźnie pamięta co się działo w piwnicy. Unika swoich dziadków, zastanawia się dużo...
Po drugie, Kasia była świadkiem sceny, gdy Antoni i Jolanta kłócili się z Ingą. Antoni wygarnął Indze, czy jest ona w ogóle świadoma na co naraziła młodego człowieka który za pobicie może pójść do więzienia. Czy w ogóle zdaje sobie z tego sprawę. Inga na to, że przecież chyba tak nawet powinno być - w końcu dziadek i wujek robią co chcą. Antoni ją skrzyczał - nie wolno tak się zachowywać, ma być odpowiedzialną dziewczyną i robić to, co powinna robić. Z mocą przychodzi odpowiedzialność. Jolanta wpadła w histerię - Inga nie może brać z nich przykładu. Nie może!
Inga się zbliżyła do mamy i srebrny krzyżyk mamę mocno oparzył (Inga jest MOCNO napromieniowana). W reakcji na to wściekły Antoni zerwał Jolancie krzyżyk z szyi i schował do kieszeni. On nie zgubi tej pamiątki, a nie może patrzeć jak jego żona się męczy.
Inga patrzy na tatę jak na zjawisko. On nie jest skażony diabelskimi mocami...
Jest nadzieja...
Scenę przerwał brzęk naczyń z kuchni. Kasia pobiegła zobaczyć co się dzieje - Samira leży na podłodze, próbując się pozbierać. Krwawi z nosa. Porozbijała kilka talerzy i obiad szlag trafił. Niestety, Kasia wzbudzając moce czerpie energię z Samiry i wiele energii Samirze już nie zostało. Nie jest dość napromieniowana...
Kasia poprosiła Samirę, by ta się położyła a sama zrobi obiad...

- ~16~17. Kasia skończyła obiad, Samira smacznie śpi, August wrócił do domu z brukselką. Kasia zaoferowała mu obiad, jeśli podleczy Samirę - dziewczyna dość mocno słabuje. August rzucił na nią dwa zaklęcia: leczące (5/5 sukcesów) i skanujące (4/5 sukcesów) - wyszło mu, że Samira jest bardzo mocno wyssana energetycznie. I moment wyssania miał miejsce tego konkretnego dnia, niedawno. A ogólnie wysysana jest od dość dawna. August się zmartwił, i to poważnie. Od razu podzielił się wątpliwościami z Kasią...
Problem polega na tym, że próg energii Samiry jest bardzo niski. BARDZO niski. Coś ją regularnie wysysa... a zgodnie z literaturą, objawy wskazują np. na działania kralotha czy innego taumatożercy. Cóż... August postanowił postawić pieczęcie na Samirze, potężne bariery by uniemożliwić wysysanie. Osiągnął na to 7 sukcesów (4 rzucone na 5k6, 3 za seniora).
Kasia próbuje przekierować czar pieczęci przez lustra w taki sposób, by August - wykwalifikowany terminus - nie zorientował się że NIE zapieczętował Samiry. Lub rozbić pieczęć tak, by August się nie zorientował. Zauważyła już wcześniej, jak skuteczne jest dla niej jeśli dotyka krwi Samiry, więc wzięła jej zakrwawioną chusteczkę by wzmocnić swój czar. Czaruje na poziomie eksperta (6k6, 4 sukcesy gwarantowane). Musi osiągnąć 4 sukcesy by Samiry nie udało się zapieczętować. 6 sukcesów.
Samira się strasznie rozkaszlała, jej poziom energetyczny spadł drastycznie. Krwawi z nosa, bardzo zimna... moc tego zaklęcia była bardzo silna.
Terminus jest zadowolony. Samira jest zapieczętowana (na szczęście, nie jest). Pod USILNYMI namowami Kasi, poszedł po eliksir wzmacniający. Boi się skazić Samirę, ale nie chce, by jej coś się stało...
Terminus widząc stan Samiry stwierdził, że musi znaleźć odpowiedzialnego za to stwora. Nie może się w pełni skupić na Sandrze.
Kasia została z nieprzytomną Samirą. Głaszcze ją po głowie i przeprasza...
Wpływ eliksiru na Samirę: 8/1k20 (zarówno jak chodzi o pozbieranie jak i o Skażenie)
Efekt jest taki: Samira stała się "oficjalnie zasealowana". Magowie będą widzieć seal stworzony przez maga SŚ. Jednak w rzeczywistości seal rozpłynął się gdzieś w lustrach.
Kasia uzmysłowiła sobie, że pasożytuje na cudzej energii (Samiry). Jest jak kraloth (cokolwiek by to nie było).
...może do Jej Wielkiego Planu, skoro już działa jak defiler, można użyć energii kogoś kto nie jest Samirą? Może można użyć energii Augusta?

- Wieczór późny. Samira nigdzie nie poszła - na takim poziomie energii śpi jak dziecko. Kasia ma wyrzuty sumienia. August jest zafrasowany, zarówno stanem Samiry jak i tym, że nie może znaleźć Sandry. Na domiar złego zaczął padać deszcz. Kasia powstrzymała się od używania lustra (by podbudować Ingę), z uwagi na stan Samiry.
Tej nocy Kasia postanowiła dać Samirze odpocząć, sama natomiast wpadła na Chytry Plan.
Zaoferowała się, że pomoże terminusowi w obieraniu brukselki - ale musi też obierać. Poczekała, aż sobie wyjątkowo nie będzie radził, po czym gwałtownie zatrzasnęła klapą do piekarnika. (6 kości vs 5 kości) - terminus się solidnie ciachnął w dłoń. To nie są kropelki, poszło po palcu. A Samira ma cholernie ostre noże kuchenne.
Natychmiast Kasia kazała mu wsadzić to pod zimną wodę. Podała mu chusteczkę, ma ściskać. Wymieniła mu 2 chusteczki, obie zakrwawione zachowała. Obiecała wściekłemu na cały świat terminusowi, że zrobi mu ten kompot i się kajała.
Kasia chciała pokazać mu jak się kroi brukselkę, ale miał na dziś dość brukselki... terminus poszedł wylizywać ranę języcznikiem a Kasia schowała chusteczki.
Kasi wersja kompotu z brukselki to osłodzona woda po ugotowaniu brukselki. NIE DA się tego pić. Jak nie patrzeć - obrzydliwe.

- Kasia, po wylizaniu przez terminusa sobie rany języcznikiem, stwierdziła, że czas Plan wprowadzić w życie. Dała terminusowi listę zakupów (w nocy) - jabłka, ale co najważniejsze - jogurt dla Samiry. Musi dać jej coś takiego, a Samira uwielbia jogurty i jogurt jej powinien pomóc. August czuje się trochę głupio, że tak huknął na Kasię, więc się zgodził. Wyszedł z domu.
Kasia usiadła przy Samirze, wzięła lusterko w którym zawarta jest pamięć terminusa (gdzie jest krew Samiry na szkle), bierze chusteczki, które poplamił terminus i na tym lusterku zarysowuje jego krwią okultystyczne symbole. Wszystko co jej przychodzi do głowy, co pozwoli osiągnąć to o co jej chodzi.
Kasia chce wprowadzić backdoor w terminusie. Mieć wejście do jego głowy.
Kasia dotknęła Samiry (by maksymalnie zredukować odległość) i przygotowana, wezwała z lusterka Istotę z Drugiej Strony.
Poprosiła o pomoc. Niech Istota pomoże. Ta ostrzegła, że Samira ma bardzo mało energii i Kasia może jej pomóc. Jak? Jeśli Kasia nakarmi Samirę niewielką ilością swojej (Kasi) krwi zmieszaną z kroplą krwi Samiry, od tej pory Kasia będzie czerpać energię nie tylko z Samiry ale też i z siebie.
Kasia zdecydowała się to zrobić.
Jako, że Kasia nie ma wprawy w cięciu się, przesadziła (1k20, celowała w 10, ma 20). W ramię. Cóż, zrobiło jej się słabo, ale osiągnęła cel. Ma krew. Szpilką dziabnęła Samirę. Zmieszała krew i dała Samirze do wypicia.
1k20 - kość szczęścia. Dała 4. Moc manifestacji: 10.
Samira gwałtownie usiadła z niewidzącymi oczami i zaczęła wrzeszczeć. Wszystkie lustra w pokoju eksplodowały i sformowały 'shard nova' dookoła Kasi i Samiry (która to Kasia rzuciła się na Samirę by przykryć ją swoim ciałem). Po chwili sformowały humanoidalny kształt, który uniósł ręce w geście zwycięstwa i się rozpadł. 
Po chwili Samira, leżąc pod Kasią, z niewidzącymi oczami wyszeptała słowa zaklęcia i wszystko wróciło do normy.
Kasia spytała Istotę z lustra z niepokojem, czy wszystko ok. Istota zwróciła się do Kasi, że tak. Teraz można rzucać zaklęcie. Ważne - Istota już NIE wygląda jak Kasia.
Kasia je rzuciła.
Właśnie w chwili, w której August otworzył drzwi, zdyszany, bo "coś się stało", gotowy do walki.
Spojrzał w Lustro Bojowe.
Zobaczył swoje odbicie.
Padł na ziemię nieprzytomny.
Istota z lustra powiedziała Kasi, że jeśli będzie rozkazywać dowolnemu lustru z intencją rozkazywania Augustowi, może swobodnie manipulować jego pamięcią i wydawać mu dowolne polecenia.
Kasia zrobiła test. Rozkaz "Śpij do rana". Sama wzięła jego języcznik i wyleczyła sobie rękę.
Czego się nie robi dla dobra Samiry...
Btw, po rzuceniu przez Kasię zaklęcia na terminusa, Samirze nic się nie stało.

- Ranek, następny dzień. Samira obudziła się z koszmarnym bólem głowy.
Ma temperaturę, poczucie, jakby miała ręce z plastiku. Czuje się bardzo. Z Kasi punktu widzenia - jeśli Kasia jest blisko, nie powinny jej moce i czary obciążać Samiry specjalnie. A tylko o to Kasi chodzi...
Terminus ma swój kompot brukselkowy. Powąchał i nie docenił.
Kasia ustawiła spotkanie w domu Szczypiorków z Jolantą na późne południe. Chce przekazać Jolancie opinię Andromedy i chce porozmawiać z Jolantą o galerii Jolanty. Biorąc pod uwagę środki Jolanty Kasia jest w stanie zorganizować galerię dla Jolanty i nowych artystów, chcących się pokazać. Organizacja i załatwianie tego będą od Kasi wymagać pojawianie się od czasu do czasu w domu Szczypiorków. Kasia staje się czymś w miarę normalnym w tym miejscu.
Kasia stwierdziła, że musi wiedzieć, czy medalion kontrolny do Ołtarza wymaga aktywacji. Ma na szczęście agenta w obozie. Terminus wie takie coś - owszem, wymaga rejestracji: wpierw aktualny posiadacz się musi wyrejestrować (krwią), potem nowa osoba zarejestrować. Jeśli aktualny nie żyje, nie trzeba się wyrejestrowywać.

- Spotkanie z Jolantą. Po wejściu do domu, Kasię dostrzegła Inga. Szybko wycofała się do pokoju i po chwili wróciła ubrana jak na lodowisko, z grubymi rękawicami. W nich - koperta z prezentem dla Kasi. Zatrzymała Jana i dała Kasi prezent - strebrny łańcuszek z krzyżykiem (w kopercie). Wzięcie go nie było dla napromieniowanej Kasi przyjemne, ale potrafiła go założyć na szyi. Dla Ingi - dowód, że w Kasi nie ma zła. Gdy Radosław przechodził, Kasia rzuciła mu inny. Złapał go odruchowo i upuścił z krzykiem - poziom jego napromieniowania jest bardzo wysoki, ale niższy niż nestora rodu.
Następną osobą, która dotknęła łańcuszka był lokaj. Też go zabolało. Kasia, na użytek nie znających magii spytała Ingę, czy ta czymś pokryła łańcuszek. Ta skłamała - azotkiem siarki. Rani złych ludzi. Zaznaczyła, że jej rodzina (poza rodzicami) jest zła do szpiku kości i zniknęła przed gniewem Radosława.
Kość szczęścia Kasi - 20/k20. Gdy Radosław się wkurzył, pod jego skórą coś lekko zafalowało a cień się zmienił. Dostrzegła to... czyli Radosław też nie jest w pełni człowiekiem.

- Kasia, korzystając ze stopnia w jakim kontroluje Augusta, wydała mu rozkaz. Ma zdobyć opcję teleportacji "tam pod ziemię", koło ołtarza. Ten artefakt, którego August używa do teleportacji nadaje się do tego w zupełności - August musi tylko postawić 'beacon' koło ołtarza.
August wykona polecenie Kasi. Ją samą przeraża to, co może mu kazać.
Kasia zaproponowała Jolancie sformowanie galerii. Jolanta się na to zgodziła.

- Wieczór. Kasia jest w okolicy domu Szczypiorków i natknęła się na Ingę. Ta na nią czekała. Inga zidentyfikowała Kasię jako inkwizytora Watykanu, szukającej zła. Inga powiedziała, że zło jest w domu Szczypiorków wszędzie. Jedynie jej rodzice są bezpieczni, nie są złem. Wszyscy inni są źli, łącznie z samą Ingą.
Kasia spróbowała przekonać Ingę, że nie jest dla niej jeszcze za późno. Że nie jest jeszcze stracone. Że Inga zrobiła pierwszy krok, przyznając się do zła i widząc to. Da się ją uratować. Senior vs Junior, Kasia wygrała. Inga wierzy, że da się ją jeszcze uratować. Ale najpierw jej rodziców.
Kasia poprosiła Ingę, by ta na razie nie zwracała na siebie nadmiernej uwagi. Ta się zgodziła.

Czyli Kasia ma dwóch silnych sojuszników - Inga i August.

Do końca dnia Kasia ciężko pracowała, by jak najdalej posunąć obraz.

- Następny ranek. W ciągu jednego dnia Samira się błyskawicznie zregenerowała. Kasia odłożone zaskórniaki przeznaczyła na to, by Samirze sfinansować występ u Szczypiorków. Dla starszego pana. Kasia chce mu zrobić prezent w swoim stylu - dać obraz "Mój Anioł".
- Kasia planuje wielotorowo. Aby mieć dostęp do domu Szczypiorków zaczęła organizować dla Jolanty małą galerię jej obrazów. Oczywiście, wymaga to konsultacji i bycia w kontakcie, więc Kasia pojawia się w domu dość często. Jednocześnie Andromeda oświadcza, że aby zrekompensować starszemu panu utratę oryginalnego obrazu, podaruje mu nowy, namalowany specjalnie dla niego.
- Kasia martwi się ewentualnymi problemami z lokalnymi magami i znajduje na ten problem rozwiązanie: Sandra. W okolicy znajduje się lokalny węzeł, Sandra ma za zadanie go lekko zdestabilizować. Magowie będą zajęci, Sandra będzie zajęta tym, żeby nie dać się złapać. Sandra postanowiła użyć czegoś lekko eksperymentalnego. Jako, że sukces był skonfliktowany, (konflikt z Sandrą na poziomie socjalnym - "możesz pomóc Augustowi w jego zadaniu"), Sandra użyje dostępnego artefaktu kontrolowanego wysadzania, jednak nie do końca utrzyma nad nim kontrolę. Nie będzie magów, Sandra bardziej się zadurzy w Auguście (w końcu robi to dla niego). 
- Konflikt z Augustynem odnośnie miejsca odsłonięcia nowego obrazu. Sukces skonfliktowany, będą zabezpieczenia, ale zwykłe, bez magów.
- Kasia martwi się również osobą Zofii. Jako jedyna poza Antonim wydaje się minimalnie napromieniowana. W rozmowie z Zofią Kasia zachowaniem sugeruje, że jest terminusem, choć ani razu tego nie mówi. Zofia przyznaje się do bycia magiem. Słabym, uważa też, że jest pod ochroną Radosława. Jednocześnie nie cierpi go i się go boi. Konflikt (trudny) z Zofią odnośnie natury Radosława i tego co wie Zofia zakończył się porażką, Radosław został ostrzeżony o tej rozmowie i o tym, że Kasia planuje upadek Augustyna.
- Kasia zdobyła jeszcze od Sandry artefakt z zaklęciem paraliżującym ludzi. Może na kogoś zadziała... Zaklęcie ma po prostu uniemożliwić innym wykorzystanie ludzi do ataku na kogoś innego, ale nie może uniemożliwiać ludziom ucieczki.
- Godzina zero. Impreza rozpoczyna się zgodnie z planem, Samira daje jeden ze swoich lepszych występów. Po jego zakończeniu, gdy lustra są wciąż na scenę, wyprowadzone jest lustro bojowe, udające ramę do obrazu. Obraz jest na lustrze, samo lustro przysłonięte materiałem. Augustyn poproszony jest o odsłonięcie obrazu. Na twarzy Radosława widać lekki uśmieszek zadowolenia, ale i zniecierpliwienia - przedstawienie Samiry trochę trwało.
- Gdy Augustyn odsłonił obraz, uderzenie z lustra bojowego go zniszczyło. Objawiło się ono w postaci promienia, który objął Augustyna niczym laser. W wynikowym chaosie Kasia porwała amulet, chwyciła Samirę za rękę i uruchomiła artefakt od Augustyna, by dostać się do ołtarzyka.
- Amulet został powstrzymany. Żanna użyła swojej magii połączonej ze swoją nienaturalną mocą viciniusa i uniemożliwiła teleportację dziewczynom. Sekudnę później Radosław ją zastrzelił. Wraz ze śmiercią czarodziejki, jej zaklęcie blokujące przestało działać i teleportacja się udała. Przy ołtarzyku Kasia została zaatakowana przez lokalnego kota. Poradziła sobie z nim szybko (w końcu to tylko kot, nawet jeśli napromieniowany magią), ale została drapnięta. Kot po otrzymaniu silnego ciosu, uciekł. Niestety, to wydarzenie opóźniło nieco proces dostrajania artefaktu. W końcu Kasi udało się zarejestrować w ołtarzyku. 
- Od razu dowiedziała się kilku rzeczy: 
 - magowie mają trochę problemów z ustabilizowaniem węzła. Z powodu śmierci Augustyna i związanego z tym szoku, wielu zostało lekko rannych lub napromieniowanych.
 - August rozpaczliwie próbuje ochronić Ingę przed Radosławem, ale długo już się nie utrzyma. 
- Co się stało: Inga zaatakowała Radosława naiwnie licząc, że jest w stanie go pokonać, lecz ten odrzucił ją jak szmacianą lalkę i przybrał swoją prawdziwą formę - koszmarnego viciniusa. Jest napromieniowany nie mniej, niż był Augustyn. Na ratunek córce rzuciła się Jolanta także przybierając prawdziwą formę - delikatnego z wyglądu, lecz wytrzymałego i osobliwie pięknego potwora. Jolanta wyraźnie przegrywała tą walkę, więc Zofia odważyła się na jedno, malutkie zaklęcie. Rozerwała czar, który unieruchomił Antoniego. Antoni z zimną krwią zastrzelił Radosława, po czym zastrzelił swoją żonę, myśląc, że to jakiś potwór. Inga zaczęła histeryzować, a Radosław się zregenerował i rozerwał Antoniego na kawałki i przypuścił gwałtowny atak na Ingę, przed którym próbował ochronić ją August.
- Na to przez jedno z luster, weszła Kasia wraz z Samirą. Widząc sytuację, Kasia uznała, że nie ma innego wyjścia i uruchomiła lustro bojowe przeciwko Radosławowi, wykorzystując jego ostatni ładunek. (sukces skonfliktowany) Stwór rzucił się, by zasłonić się Ingą i Augustem, ale promień i tak go objął. Radosław i Inga zginęli na miejscu, jednak August zdołał uniknąć promienia dzięki podwójnemu połączeniu z Kasią (ołtarzyk i lustra) Udało mu się postawić tarczę, która osłoniła jego - tylko na tyle miał czasu i mocy. Skończył poparzony.
- Żywi pozostali jedynie Zofia, August, Samira, Kasia, służba i kot.
- Ogólne napromieniowanie domu magią połączone z energią uwolnioną z lustra doprowadziło do powstania kilku ognisk pożaru w domu. Kasia szybko myśląc, usunęła z piwnicy właściwy ołtarzyk i przy pomocy Augusta upewniła się, że ludzie (policja i straż pożarna) uznają te wydarzenia za porachunki mafii (obecność broni połączona z piwnicami wypełnionymi srebrem, które tam Zofia znosiła przez lata, wydatnie to ułatwiła)
- Z rodziny Szczypiorków przy życiu pozostała tylko Zofia. Kasia nie podłączyła jej pod kontrolę ołtarzyka, uważając, że taki czyn sprowadziłby ją do tego samego poziomu, co Augustyna i Radosława. Zofia właśnie odziedziczyła dom, fortunę i wolność, ale nie ma pomysłu na życie.
- Samira uznała, że to wszystko był tylko sen, nie potrzeba było wiele wysiłku, aby o wszystkim zapomniała, a ewentualne przebłyski uznawała właśnie za dziwny sen.
- Korzystając z mocy ołtarzyka Kasia upewniła się, że miejscowi magowie pomogą Zofii na ile będzie tego potrzebować i że będą dobrze traktować ludzi. Następnie sam artefakt zamurowała w trzecim najmniej oczywistym miejscu na terenie posiadłości. (paranoja mode on) Kasia zakłada, nie będzie z artefaktu korzystać w żaden istotny sposób.
- August był załamany faktem, że Kasia ma nim władzę. Kasia chciała mu oddać amulet, ale terminus stwierdził, że dotyk amuletu go zabije. Kasia postanowiła kupić porządną skrzyneczkę z dobrym zamkiem, wyłożyć ją lapisem i do niej włożyć amulet. Kluczyk do skrzynki zachowała, zaś samą skrzynkę oddała magowi. Kasia nie chce z tego amuletu korzystać. Przy okazji, oddając go Augustowi, okazuje mu zaufanie. To od Augusta zależy czy i kiedy odda jej skrzynkę. Lustro bojowe Kasia zachowała, choć teraz jest zwykłym lustrem.
- Kasia rozmawiała zarówno z Sandrą jak i z Augustem. Zorientowała się w uczuciach Sandry do Augusta i zdecydowała, że bezpieczniej będzie powiedzieć Augustowi, że Sandra bardzo pomogła i że tak naprawdę są jej winni wdzięczność. August się na to bardzo oburzył twierdząc, że Sandra jest zdradziecką donosicielką i jeśli pomogła, to na pewno dlatego, że uznała, że coś z tego uzyska, aby go zrujnować. Kasia pozwoliła mu się wyzłościć w spokoju, ale dzięki jej mediacjom młodzi nie pożarli się całkowicie przy spotkaniu. Spotkanie było chłodne, formalne, Sandrze było przykro, ale nie doszło do starcia, do którego normalnie by doszło.


---
"Jak ktoś, kto umie walczyć nożami nie umie poradzić sobie z brukselką?" - Kasia.
"Wreszcie Samira nie będzie sama cierpieć" - Kasia moment przed nakarmieniem Samiry krwią.


# Zasługi

* czł: Kasia Nowak jako dowód na to, że ważniejszy jest pomysł niż moc magiczna.
* czł: Samira Diakon jako słabiutka już iluzjonistka, bateria mocy Kasi.
* mag: August Bankierz jako thrall Andromedy (i tak woli to niż thrall Szczypiorków).
* mag: Sandra Stryjek jako unikająca Augusta podkochująca się czarodziejka robiąca dywersję. Bo plan. I żądająca kompotu brukselkowego.
* vic: Augustyn Szczypiorek, (83 l) : Anioł Podniesionej Dłoni. KIA.
* mag: Żanna Szczypiorek, (68 l): żona Augustyna, niewolnica i miłość Augustyna, wierna i niezłomna do samego końca. KIA.
* vic: Radosław Szczypiorek, (44 l) : syn Augustyna i Żanny. Prawdziwy potwór rodu Szczypiorków. KIA.
* mag: Zofia Szczypiorek, (41 l): żona Radosława, która ośmieliła się na JEDNĄ rzecz w życiu. Samotnie przeżyła, nie ma pomysłu na życie.
* czł: Antoni Wójt, (38 l) : mąż Jolanty, zwykły człowiek pośród magów i viciniusów który chronił rodzinę jak mógł. KIA.
* vic: Jolanta Wójt, (39 l) : córka Augustyna i Żanny, która odrzuciła wszystko by chronić córkę by zginąć z ręki męża. KIA.
* vic: Inga Wójt, (19 l): córka Antoniego i Jolanty, wierząca do końca w dobro pochodzące od rodziców. KIA.
* vic: Dariusz Germont, : skażony lokaj rodu Szczypiorków który nagle jest wolny. Nadal chce służyć pani Zofii Szczypiorek.
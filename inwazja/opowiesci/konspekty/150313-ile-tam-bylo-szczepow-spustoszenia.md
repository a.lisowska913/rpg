---
layout: inwazja-konspekt
title:  "Ile tam było szczepow Spustoszenia"
campaign: powrot-karradraela
gm: żółw
players: kić, dzióbek
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [150224 - Wojna domowa Spustoszenia (SD, PS)](150224-wojna-domowa-spustoszenia.html)

### Chronologiczna

* [150224 - Wojna domowa Spustoszenia (SD, PS)](150224-wojna-domowa-spustoszenia.html)

## Kontekst ogólny sytuacji

## Punkt zerowy:

## Misja właściwa:

KADEM. Paweł i Siluria decydują się na pójście śladem Spustoszenia; w tej historii nadal jest więcej niewiadomych niż tego co już wiedzą. Pierwszym śladem jest Zenon Weiner, który do tej pory był nieprzytomny. Zdecydowali się na rozmowę z Zenonem na samym początku. Ten sympatyczny 4x-letni czarodziej jest jeszcze słaby i leży w łóżku. Usłyszawszy, że jest na KADEMie położył się wygodnie i stwierdził, że będzie współpracował.

Zapytany o co chodzi i skąd wzięło się Spustoszenie, Zenon wyjaśnił: skontaktował się z nim Marian Welkrat i powiedział, że doszło do aktywacji wskaźników Spustoszenia w Powiewie Świeżości i Marian się zmartwił. Poprosił Zenona o to, by ten pomógł Marianowi zlokalizować co się stało i czy gdzieś jest kontroler. Zenon wziął odpowiednio wykastrowane Spustoszenie i poszedł z Marianem za śladem Spustoszenia. Znalazł ślad w Kotach; tam doszło do rzucenia tajemniczego zaklęcia przez Netherię, rezonansu i w wyniku uaktywniło się Spustoszenie Zenona. Nie mogło połączyć się z magiem ani człowiekiem, lecz zagnieździło się w miślęgu. To co ważne; aktywacja nastąpiła przez INNY kontroler Spustoszenia. Niekoniecznie kompatybilny.

Zenon Weiner powiedział też o tym, że Spustoszenie uczy się od siebie; są "szczepy" Spustoszenia. W SŚ jest Spustoszenie które posiada opcję amnezji; Spustosza maga, łączy się z pamięcią i usuwa określoną pamięć po czym można owo Spustoszenie usunąć. Aurelia zmieniła "swój szczep" Spustoszenia przez dodanie niekompatybilności z innymi szczepami Spustoszenia oraz przez dodanie opcji wyłączenia Spustoszenia. Niestety, nie udało jej się wiele więcej zdziałać.

Zapytany o Aurelię, Zenon powiedział, że Aurelii w pracy ze Spustoszeniem pomagał Powiew; z Powiewem niektórzy magowie Srebrnej Świecy. Ale Aurelia to była Aurelia, tylko ona miała wiedzę powiązaną z całością problematyki Spustoszenia. Nie chciała się tym dzielić; uznała to za zbyt niebezpieczne. Zenon nie wie, by wielu innych magów rodu Maus interesowało się problematyką Spustoszenia.

Zenon praktycznie potwierdził, że były dwa kontrolery Spustoszenia. A jednym z nich był miślęg kontrolowanym przez astralnego kralotha. Drugi - dalej nieznany.

Paweł i Siluria mają już chyba wszystko czego dało się dowiedzieć od Zenona Weinera. Poprosili do niego Julię, sami natomiast poszli spotkać się z Marianem Welkratem.

Marian Welkrat też współpracował z KADEMem. Potwierdził, że zasięg nauki Spustoszenia od Spustoszenia - jakieś 100 metrów. Potrzebne jest jakieś 10-15 minut na naukę.

Marian opowiedział swoją wersję historii - Aurelia Maus próbowała stworzyć "Spustoszenie, które skończy wszystkie Spustoszenia". Szczep z kontrolerem niekompatybilnym z typowym Spustoszeniem, ale mający priorytet nad owym typowym Spustoszeniem. Plan Aurelii był taki, by "nowe Spustoszenie" było w stanie nadpisać stare, przechwycić je a na samym końcu dokonać samozniszczenia. Plan Aurelii polegał na tym, by dać EIS nadrzędną kontrolę nad Spustoszeniem które będzie połączone z jakimś organicznym celatidem. Marian jednak nie ruszał tej sprawy, nie uważa się za odpowiednio mądrego.

Marian powiedział, że nadrzędny kontroler Spustoszenia Aurelii jest w Powiewie i że jakby dodać komponent organiczny to mamy możliwość współpracy i komunikacji... ale Marian odradza. Nie wie co się stanie. Zapytany, powiedział, że sprawdził po uruchomieniu alarmów czy są wszyscy nie-Spustoszeni. Sam Powiew jest ekranowany; poza budynkiem Spustoszenie nie powinno móc transmitować. Ale wpadł na to, że nie sprawdził NIEAKTYWNEGO Spustoszenia, czyli nie połączonego z komponentem organicznym; nie ma zresztą jak.

"Jeśli komuś udało się dorwać nieaktywne Spustoszenie, najpewniej przyniesie je do Powiewu by stało się 'niegroźne' i może badać je w spokoju - w końcu jest odpowiedzialnym magiem" - Paweł.

Marian dodał jeszcze, że podczas gdy był Spustoszony znajdował się w kopcu miślęgów. Gdy tam był, został przechwycony przez to DRUGIE Spustoszenie w brutalny sposób, po czym doszło do skanowania wiedzy i pamięci i drugie Spustoszenie odrzuciło Mariana i to pierwsze (z kopca) odzyskało kontrolę.
Interesujące jest, że Marian pamięta lepiej niż Zenon.

Marian zauważył jeszcze, że Spustoszenie Aurelii ma najwyższy priorytet znaleźć inne Spustoszenia, przyczepić się do nich pasożytniczo i je wyłączyć. Ale Aurelia mogła zapomnieć ustawić warunki brzegowe (co Marian uznał za dziwne, ale możliwe). Innymi słowy, zdaniem Mariana, to Spustoszenie nie będzie chciało Spustoszać magów i ludzi losowo a tylko celowo, by niszczyć inne Spustoszenia. Ale znając Aurelię, "it is relentless".

Marian dodał jeszcze, że sam z Zenonem nie szli na akcję ze Spustoszeniem, to byłoby nierozsądne. Eskortowała ich Iza Łaniewska (terminuska). 
Oczywiście, wszystkie odkrycia zrobione przez Pawła i Silurię zostają na KADEMie.

Julia skończyła rozmowę z Zenonem więc Paweł i Siluria udali się do niej. Czarodziejka dodała, że to Spustoszenie które jest na PŚ to "overmind", najwyższa klasa kontrolująca agentów Spustoszenia. Zniszczenie overminda powoduje, że agenci stają się autonomiczni i na razie znana jest misja overminda - zniszczyć wszystkie inne Spustoszenia. Julia zaproponowała, by KADEM przejął od PŚ to Spustoszenie, ale nie mogą go badać; są tam systemy samoniszczące.
Oh joy.

Na razie chronologia się zgadza. Spustoszenie mogło wyjść z PŚ i potem od Zenona. I Spustoszenie jakie opanowało kopiec miślęgów mogło być tym, które pochodziło od Zenona.

Magowie KADEMu udali się do Marty, by powiedzieć o Overmindzie Spustoszenia Aurelii (OSA) i o tym, że Julia chce to oddać na KADEM. Marta stwierdziła, że KADEM faktycznie przejmie OSA, ale wpierw musi zabezpieczyć bezpieczny transport OSA na KADEM; zdaniem Marty cała ta sprawa może być planem OSA by wyrwać się z PŚ. I nie, Marta nie ufa Aurelii i jej umiejętnościom. 
Marta autoryzowała Silurię do użycia GS "Aegis" 0003 jeśli Siluria uzna to za konieczne. Siluria jest "szczęśliwa".
Julia ma w tej sprawie dogadać się z Martą; tak się to skończy.

Tymczasem Powiew Świeżości (Annaliza) przygotowuje listę osób, które były obecne (spoza PŚ) gdy OSA transmitował.

2-3 godziny później zostało to skończone. Lista osób obecnych na PŚ w tym czasie: Anna Kozak, Salazar Bankierz, Tatiana Zajcew, Anita Rodos. 

Siluria, czekając na wyjście Julii zdecydowała się zrobić research na punkcie Julii i Izy. Zwłaszcza w perspektywie potencjalnych interakcji. (6#4v6/8/10/12->10). Siluria dowiedziała się co następuje: W wypadku Julii, ta czarodziejka zawsze ceniła sobie KADEM. Ogólnie jest dobrze nastawiona do KADEMu a jej słabym punktem jest Powiew. Obiecasz, że pomożesz Powiewowi i ją rozbroisz. Szczególne postacie: Tomek od Kamili, Kuba Urbanek, Whisperwind.
Z Izą gorzej. She is kind of feral. Odpięła się od wszystkich. Ma mistrza w SŚ, ale... nie widują się zbyt często. Iza nie ma przyjaciół na KADEMie, ale nie ich też na SŚ. Ta czarodziejka ceni sobie polowanie, niezależność a najbliższa jej osoba jest martwa. Jedyny widoczny "słaby punkt" Izy to Dalia Weiner. Iza ceni Julię, ale nie chce z nią współpracować. Nie współpracuje z nikim, tylko wydaje rozkazy lub działa na zlecenia; jak sama powiedziała "jeśli nie mam nikogo blisko, nie można mnie zaatakować i zawsze mogę zrobić to, co musi być zrobione."
Jedyny powód dla którego Iza jest dalej na SŚ z punktu widzenia gildii jest taki, że gildia nie chce jej zwolnić by Iza nie została 'free agent'.
Jedna z ciekawych rzeczy; Iza ostatnio współpracowała częściej z Tamarą Muszkiet.

Julia wyszła ze spotkania z Martą i od razu przechwycili ją Siluria i Paweł. Siluria zdecydowała się wykorzystać fakt ogromnego zmęczenia Julii i przystąpiła do eleganckiej ofensywy przy herbatce (co składa się na kumulowany bonus #3). Julia jest wykończona, choć zadowolona. Pozbędzie się tego Spustoszenia (OSA) z Powiewu Świeżości. Powiedziała Silurii (zapytana), że Aurelia kiedyś była Spustoszona na Powiewie; to było przyczyną jej obsesji na punkcie Spustoszenia.
Zapytana, skąd Świeca wiedziała o Spustoszeniu, Julia powiedziała że najpewniej przylecieli do flary KADEMu. Tamara Muszkiet zapytana przez Julię powiedziała właśnie o tej flarze. Czyli to KADEM wezwał Świecę... przypadkowo.
Julia powiedziała, że Tamara jest członkiem "Stalowej Kurtyny" - grupy SŚ która uważa, że SŚ jest potężną gildią i powinna być odpowiednio silna. Co koliduje Kurtynę przeciw KADEMowi. Zdaniem magów Kurtyny KADEM przede wszystkim żeruje na artefaktach i wiedzy innych gildii pozując na super-naukową gildię. Dlatego Tamara tak chciała się upewnić, że Zenon Weiner nie wpadnie w ręce KADEMu.

Po tej krótkiej rozmowie Siluria zdecydowała się nacisnąć lekko (podstępem wyciągnąć od Julii) co ona wie; co Judyta miała na myśli gdy powiedziała, że Julia coś ukrywa. (13v10->12). Julia w końcu przyznała, że Iza wiedziała o tym gdzie znajduje się Spustoszenie. W przeszłości, Iza i Aurelia kłóciły się o lokalizację Spustoszenia; Iza powiedziała, że Spustoszenie nie jest tam bezpieczne aż w końcu Aurelia się zniecierpliwiła i dokładnie pokazała Izie zabezpieczenia i że nie da się do Spustoszenia dostać. I faktycznie, Iza zrobiła testy penetracyjne i nie dostała się do OSA. Ale jednak chyba Aurelia się myliła... nie rozpatrywali nauczenia się Spustoszenia od Spustoszenia.

Po "lekkim oporze" Siluria powiedziała Julii, że Iza była w Kotach wcześniej; że to właśnie Iza eskortowała Mariana i Zenona zanim Spustoszenie uderzyło.
Julia wpadła we wściekłość. Zenon i Marian to jej przyjaciele. Iza była przyjaciółką... Siluria nie była w stanie zmienić zdania Julii, ale powiedziała Julii że Iza mogła być Spustoszona. W końcu nikt nie sprawdził czy Iza jest Spustoszona, prawda?
...szok.

Komunikat od Marty. Przyszedł tien Marcel Bankierz. Chce z powrotem Zenona Weinera. To problem Silurii i Pawła. Hurra.
Julia sobie poszła; Siluria zaprosiła Pawła i Marcela Bankierza do ogrodu na KADEMie. To, że ogród zrobiła Weronika Seton umknęło opisowi Silurii.

Marcel Bankierz przyszedł. Wyjątkowo nieszczęśliwy czarodziej; przyszedł dość energicznie przygotowany do działania by być agresywnym inkwizytorem, spojrzał na Silurię i po prostu zrezygnował. Nie był w stanie tego wygrać. Siluria dowiedziała się jakie miał rozkazy - kazano mu pilnować Zenona Weinera przed atakiem Spustoszenia KADEMu. Ale na KADEMie nie ma Spustoszenia - więc miał przeszukać KADEM i znaleźć brak Spustoszenia. Jeśli nie, miał pilnować - oczywiście nie przed KADEMem a przed Spustoszeniem.
Siluria go całkowicie zamotała. Żeby Marcel Bankierz nie miał za dużych problemów w gildii zaproponowała mu pokój, w którym może chronić KADEM przed Spustoszeniem, dzięki czemu pośrednio będzie chronił Zenona przed Spustoszeniem.
Marcel na to przystał. W tym stanie przystałby na wiele. Ale lepiej móc powiedzieć przełożonym "chroniłem KADEM przed Spustoszeniem" niż "nic nie uzyskałem".

Korzystając z okazji, Siluria zaczęła wypytywać Marcela o Salazara Bankierza. Salazar jest ekstrawertycznym, pewnym siebie magiem który nie wyrywa się do mówienia, ale nie ma nic przeciw powiedzeniu swego zdania. Mag bojowy, terminus, medyk polowy. Współpracuje od czasu do czasu z Powiewem Świeżości; przekazuje im części medycznych rzeczy Bankierzy oraz uczy się technik magicznych z ksiąg Powiewu. (Bonus #2).
Siluria poprosiła Marcela o to, by umówił ją i Pawła z Salazarem. Nie ma problemu.

Salazar był skłonny spotkać się z nimi wieczorem, za kilka godzin. 

"To nie byłem ja. Nie przyznam się. Nie okradłem POWIEWU ŚWIEŻOŚCI." - Salazar Bankierz, między atakami śmiechu.

Przeskanowany przez RTG w komórce Silurii (dyskretnie), Salazar nie jest Spustoszony. Obiecał współpracę, zwłaszcza patrząc na piękną Silurię.

"Byłem z Anką Kozak. Moją dziewczyną z zespołu. No, nie moją." "Wspólną?" "No, taką trochę wspólną" - Salazar i Paweł.

Rozmawiając z Silurią która próbowała wyciągnąć wszystko co dziwne w tym okresie, Salazar powiedział że jedyne dziwne rzeczy to to, że najpierw spotkał się z Draceną - Diakonką-technomantką, potem w tym okresie o którym mówi Siluria był w Powiewie bo Dracena poprosiła go, by pokazał Ani Kozak jakieś konkretne książki (tematyka nie związana ze Spustoszeniem) a potem spotkał się z Silurią - kolejną Diakonką-technomantką. I to dla Salazara było w sumie najdziwniejsze.
Salazar nie został poinformowany co zginęło. Nic ciekawego się w Powiewie jego zdaniem nie pojawiło.

Poproszony przez Silurię, Salazar poprosił do siebie Annę Kozak. Ta pojawiła się 20 minut później. Widać, że biegła.
Salazar sobie postawił ją do wiwatu, pokazując jak bardzo ma nad nią władzę. 
Anna zwróciła uwagę Silurii i Pawła na Dracenę - powiedziała, że Dracena wyraźnie czegoś chciała od Salazara. Spotkała się z nim cztery dni przed tym incydentem, praktycznie wprowadziła się mu do domu (i łóżka), po czym po rozmowie z Anną zaproponowała Salazarowi by pokazał Annie ciekawe książki w Powiewie jakie Annę interesowały. Wtedy doszło do incydentu. I parę dni później Dracena się wyprowadziła od Salazara.
Siluria delikatnie wytłumaczyła Ani, że u Diakonów to działa inaczej. To pójście razem do łóżka po prostu oznacza chęć dobrej zabawy. Przy okazji Siluria dostrzegła, że Anna jest zadurzona w Salazarze (a on traktuje ją trochę z buta). Powiedziała Annie, że wystarczy Diakonowi zwykle powiedzieć że jest się kimś zainteresowanym i zwykle Diakon nie będzie przeszkadzał. Ania - pełny pąs "ja nie jestem zainteresowana! Nikim! Wcale!"

Taaaaaaaaaak.

Więc - chwilowo nie ma śladów tego Spustoszenia. Jedynym śladem jest Iza... ale nikt nie chce przepytywać czy przesłuchiwać Izy.
Chwilowo zostawią to Julii :P.

# Streszczenie

Zenon Weiner współpracował z KADEMem; powiedział o drugim kontrolerze Spustoszenia i o tym, że Marian Welkrat wykrył to pierwszy. Okazało się, że Marian miał dostęp do Spustoszenia zmienionego przez Aurelię Maus; Spustoszenie mające zniszczyć wszelkie Spustoszenie. Marian pamięta więcej niż Zenon; powiedział, że Spustoszenie przeskanowało mu pamięć. Powiedział też, że na akcji była Iza Łaniewska, terminuska ich chroniąca. A Iza współpracowała ostatnio z Tamarą Muszkiet... Okazało się też, że Iza wiedziała, że na Powiewie jest Spustoszenie i jak się tam dostać. Ale jak Spustoszenie wyciekło? Nie wiadomo. 

# Lokalizacje:

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Centrum
                        1. Klub Magów Rzeczna Chata
                        1. KADEM Primus
                            1. Skrzydło Szpitalne
                            1. Ogrody Weroniki Seton

# Pytania:

K - Dlaczego Judyta Karnisz, terminuska ze Świecy zwróci się o pomoc do Pawła i Silurii w sprawie po Spustoszeniu?
Ż->K - Bo w każdym innym wypadku pewni magowie zostaną niesprawiedliwie i nieproporcjonalnie mocno ukarani za Spustoszenie.
K - Dlaczego Oktawian Maus bardzo nie chce wpaść w ręce Tamary Muszkiet?
Ż->K - Bo nie chce zostać uznany winnym z automatu.
B - Dlaczego wybór miślęga jako kontrolera Spustoszenia nie był przypadkowy?
Ż->B - Kraloth był czynnikiem nieprzewidzianym; natomiast miślęg jest czymś czym da się w miarę łatwo sterować.
Ż - Co kluczowego wie Zenon Weiner co mogłoby doprowadzić do jego śmierci ze strony jednego z kontrolerów?
B - Odkrył sposób dezaktywacji Spustoszenia zmienionego przez Aurelię Maus.
Ż - Co wie Julia związanego ze sprawą, co zarzuciła jej Judyta (a czego Julia tak naprawdę nie skojarzyła)?
K - Ma poszlaki na temat tego, jak się Spustoszenie tu znalazło.
Ż - Dlaczego tylko KADEM jest w stanie dotrzeć do sedna tej sprawy?
B - Bo jako jedyny nie musi kryć własnej dupy.
Ż - Dlaczego Tamarze Muszkiet tak zależy na dotarciu do Spustoszenia (personal stuff)?
K - Ponieważ uważa, że Spustoszenie może pomóc bliskiej jej osobie.
Ż - Gdzie znajduje się Zenon Weiner i Marian Welkrat?
K - Na KADEMie.
Ż - Jak to się stało, że Tamara mimo całej potęgi SŚ jeszcze nie dała rady ich odzyskać (przynajmniej Zenona)?
B - Dostała odpowiedź, że przerwanie procesu de-Spustoszenia mogłoby być niebezpieczne dla jego życia.
Ż - W jaki sposób doszło do tego, że mimo wszystkich starań Tamary nie ma maga SŚ pilnującego by wszystkie rozmowy z Zenonem Weinerem były rejestrowane przez SŚ?
B - Trade secrety KADEMu odnośnie de-Spustoszenia były wyższe niż uprawnienia Tamary i terminuska dostała rozkaz 'drop it'.
K - W którym aspekcie Iza nie do końca zgadza się z celami Tamary?
Ż->K - Izie zupełnie nie zależy na znalezieniu winnych Spustoszenia.
B - Dlaczego Tamara, mimo, że wie czym jej to grozi będzie próbowała ominąć zakaz?
Ż->B - Bo jest w jakimś stopniu powiązana z EAM i chce chronić wiedzę Zenona przed KADEMem.

# Zasługi

* mag: Siluria Diakon jako słodka i "niewinna", której wszyscy wszystko powiedzą. Bawi się w detektywa.
* mag: Paweł Sępiak jako zadający właściwe pytania obserwator.
* mag: Tamara Muszkiet, członek 'Stalowej Kurtyny', terminuska uważająca KADEM za gildię niegodną; czasem trudno się zorientować czy walczy ze Spustoszeniem czy z KADEMem.
* mag: Dracena Diakon, której imię z dziwnych powodów pojawiło się w kontekście Spustoszenia w Kotach; podobno kręciła z Salazarem Bankierzem.
* mag: Zenon Weiner, przechwycony przez KADEM mag Świecy i absolutny ekspert od Spustoszenia, 4x-letni, współpracuje z KADEMem.
* mag: Marian Welkrat, który opowiedział swoją wersję historii Spustoszenia i wskazał na obecność Izy w całej tej sprawie. Bardzo współpracuje z KADEMem.
* mag: Julia Weiner, wymęczona wszystkim przełożona Powiewu i członek rodu Weiner którą Spustoszenie zmusiło do współpracy z KADEMem dla ochrony Powiewu.
* mag: Infensa Diakon, wtedy: Izabela Łaniewska, która potencjalnie wiedziała o Spustoszeniu zanim się pojawiło... i której rola w tej historii jest dość interesująca i niejasna.
* mag: Marta Szysznicka jako lekko paranoiczna głównodowodząca KADEMu; uzgodniła z Julią, że Overmind Spustoszenia Aurelii Maus trafia na KADEM w bezpieczne miejsce.
* mag: Marcel Bankierz, wysłany przez Srebrną Świecę w smutnej roli ochrony Zenona Weinera przed KADEMem... err... Spustoszeniem. Chyba ktoś go nie lubi :P. Siluria go lubi.
* mag: Salazar Bankierz, który w krótkim czasie napotkał dwie zainteresowane nim Diakonki-technomantki dzięki czemu jego poziom radości wzrósł drastycznie.
* mag: Anna Kozak, podkochująca się w Salazarze czarodziejka nie traktowana przez nikogo poważnie. Zazdrosna o Salazara, skupia się na Dracenie.
* mag: Aurelia Maus, wspomnienie o niej jako o czarodziejce która zawsze patrzyła w gwiazdy i zapominała o tym jak niebezpieczne są wyniki jej pracy.

# Czas:

* Opóźnienie: 1 dzień
* Dni: 2

# Wątki

- Szlachta vs Kurtyna
- Trzeci kontroler Spustoszenia
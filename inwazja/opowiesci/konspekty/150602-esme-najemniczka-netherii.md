---
layout: inwazja-konspekt
title:  "Esme, najemniczka Netherii"
campaign: czarodziejka-luster
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [150105 - Dar Iliusitiusa dla Andromedy (An)](150105-dar-iliusitiusa-dla-andromedy.html)

### Chronologiczna

* [150104 - Terminus-defiler, kapłan Arazille (An)](150104-terminus-defiler-kaplan-arazille.html)

## Punkt zerowy:

Ż: Jakie jest hobby Netherii jakim nie chce się chwalić w Millennium (nic erotycznego)?
B: Pisze kiepskie fanfiki.
Ż: Skąd Netheria wie, że może na nich liczyć (nie zdradzą jej do Blakenbauera)?
B: Z uwagi na reputację - Netheria płaci dobrze, uczciwie przedstawia zlecenie i Esme z uwagi na reputację obu stron nie chce tego zaprzepaścić.
Ż: Jakim cudem o niczym nie wiedzą siły Millennium?
K: Ponieważ Netheria nie działa w bezpośredniej bliskości Trójzęba.
Ż: Co jest potrzebne do ciągłego podtrzymywania funkcjonowania Trójzęba?
B: Regularnie trzeba dostarczać emocji, które podtrzymują Skażony Węzeł Trójzęba.

## Misja właściwa:

### Faza 1: "Rozlokowanie"

Netheria zauważyła, że nie ma wielkiej możliwości działania w tej sytuacji a ewentualny sojusz Mirabelki z Triumwiratem byłby katastrofalny. W związku z tym zdecydowała się na ściągnięcie najemniczki - Esme Myszeczki, która ma opinię osoby której można w takich sprawach zaufać.
Esme dowiedziała się następujących rzeczy:
- Tymotheus nie opuszcza Trójzęba, chyba, że musi.
- Tymotheus jest niesamowicie niebezpieczny w walce osobistej, jak to powiedział Gabriel Newa "nie sądzę, bym był w stanie go pokonać".
- Tymotheus interesuje się badaniami naukowymi, też na ludziach i tajemnicą poliszynela jest to, że na magach też.
- Zadanie Esme polega na tym, by albo opóźnić lub uniemożliwić sojusz Mirabelka - Triumwirat, albo by wyciągnąć Tymotheusa poza Trójząb i uniemożliwić mu powrót by można było atakować.

Netheria zaproponowała Esme hotel, ale ta odmówiła; najpewniej Triumwirat obserwuje wszystkie hotele. To samo z wynajętymi mieszkaniami. W związku z tym Netheria zaproponowała Esme mieszkanie z rodziną mającą trójkę dzieci. Esme się zdziwiła, jakim cudem Netheria może jej zapewnić "normalne" mieszkanie gdzie ludzie nie będą protestować, na co Netheria powiedziała, że bez magii też da się to załatwić by Esme była kochaną ciotką. Po prostu są ludzie, którzy nie są fanami Triumwiratu. Netheria poprosiła Esme, by ta jej zaufała w tej kwestii.
Esme zapytała Netherię co ta może dostarczyć za wsparcie z tego terenu. Netheria stwierdziła, że jest właśnie podłączany ruch oporu do sieci i centrali w której Netheria się znajduje. Są tu szantażowani (przez Triumwirat) magowie i wpływowi ludzie, którzy nie postawią się żadnemu agentowi Triumwiratu, ale mogą wykazać się chwilową niekompetencją i nie zauważyć ruchów Esme. Z biegiem czasu Esme będzie mogła dostać wsparcie też maga bojowego czy dwóch, mają też wysoko postawioną wtykę w Triumwiracie (Gabriela).
Wygląda to nieco lepiej.

Netheria podała Esme adres. Pod tym adresem czekała już na Esme tęgawa i sympatyczna kobieta - Zofia Perszen. Przeprosiła z góry za bałagan i powiedziała, że na dłuższą metę Esme nie będzie tu mieszkać - prowadzone są prace, by następnego dnia mogła zamieszkać u kogoś, kto ma znaczącą pozycję w hotelu 'Luksus' w którym przebywa aktualnie Mirabelka, Luksja i kilka osób z jej oddziału. 
Chłopak, Staszek, poprosił Esme do swojego pokoju, do komputera. A tam, na Skype, gTalku i kilku innych komunikatorach i różnych mailach Staszek zbiera informacje z różnych komórek i pokazuje je Netherii - jak ruch oporu i siły Arazille podłączają się do sieci centralnej.
Tak, tylko, że Esme wie (8v9->remis), że jak tylko Millennium skończy się stabilizować i rozlokowywać w okolicy, wprowadzi technomantyczne urządzenia skanujące sieci i wykryje anomaliczne ruchy. Innymi słowy, w ciągu 24-48h Millennium dowie się o nietypowym działaniu i najpewniej wyśle tu jakieś jednostki...
Fatalny początek.
Esme skontaktowała się z Netherią i wyjaśniła sytuację. Netheria się bardzo zasępiła i powiedziała, że przeniesie Esme jak najszybciej i wprowadzi jakieś proxy na jej miejsce. Netheria nie miała pojęcia, że Millennium jest w stanie działać tak skutecznie i tak szybko...

Esme wpadła tu na pewien pomysł - jak to przesunąć w jakąś formę sukcesu. Z perspektywy Millennium Triumwirat jest monolitem. Gdyby Esme się udało zrobić tak, że Millennium będzie myślało że to działania samego Triumwiratu, ale niekoniecznie powiązane bezpośrednio z Tymotheusem a jakaś reakcja - to może stanowić podstawę do pewnych korzyści. Żeby Millennium się mniej chętnie jednoczyło z Triumwiratem, bo jeśli jest reakcja i tego typu działania, to sugeruje słabość wewnętrzną i potencjalną wojnę domową. Co nijak nie sprzyja Mirabelce.
Żeby dopracować szczegóły, Esme musi porozmawiać z kimś kto "siedzi" w Triumwiracie. Netheria skontaktowała Esme z Gabrielem Newą.
Pierwsze co Esme (profesjonalna najemniczka) zdecydowała się zrobić to ZROZUMIEĆ na ile Gabriel jest po ich stronie, tak naprawdę. Na ile wspiera Netherię i na ile można mu ufać. Ale też na ile jest przeciwny Tymotheusowi. (sukces) Gabriel wypadł w oczach Esme pozytywnie, choć niepokojąco - ten mag jest skłonny poświęcić Netherię, Esme i wszystkich dla osiągnięcia celu. Szczęśliwie, cel mają ten sam. Nieszczęśliwie, on dowodzi. Nie zdradzi ich, będzie ich chronił, ale w wypadku konfliktu 'zniszczenie Triumwiratu' - 'ratunek kogokolwiek' on wybierze zniszczenie Triumwiratu. Ma objawy choroby terminusów.
Esme wyciągnęła z Gabriela informacje odnośnie małych rzeczy świadczących o tym, że powiązany jest z tym Triumwirat. Nic wielkiego, małe przeoczenia. Mirabelka, zdolna dama polityki Millennium na pewno je zauważy. Z pomocą Gabriela i Netherii (jej kontaktów politycznych) udało się Esme sformować zgrabne dowody. (Remis).
W fazie drugiej siły Millennium zalokują na tym budynku i zaczną badania. Dojdą do działania Esme i że to Triumwirat. W fazie trzeciej siły Millennium zalokują na Esme, ale będą uważać, że Esme jest agentką Triumwiratu niekoniecznie korzystnego dla Tymotheusa.

Gabriel powiedział Esme coś ciekawego. W Triumwiracie znajduje się pewien mag. Nazywa się Konrad Węgorz. Jest iluzjonistą i astralikiem. Nie jest szczególnie ważny ani potężny, ale nie podoba mu się działanie Triumwiratu. Tymotheus używając jego (Gabriela) odciął Konrada od jego żony. Gabriel zdradził Esme gdzie przetrzymywana jest żona Konrada i za jakimi zabezpieczeniami; nie jest to Czarna Wieża (bo Konrad jest astralikiem i mógłby się z nią kontaktować). Gdyby udało się Esme odbić żonę Węgorza, Gabriel może tego "nie zauważyć" a Esme może uzyskać może nie bardzo potężnego ale jakiegoś sojusznika w Triumwiracie. Plus, zawsze można zasugerować że to przypadkowe działania Millennium...
Esme dopytała - ale jak to możliwe, skoro do tej pory Triumwirat trzymał w Trójzębie wszystkie osoby; jakim cudem nagle pojawia się "żona" jaka jest poza Trójzębem. Gabriel z uśmiechem powiedział, że Triumwirat dał mu za zadanie znaleźć haka. Gabriel znalazł - żonę. Ale nie chciał by trafiła do Trójzęba, więc zamknął ją w artefakcie i powiedział swoim mocodawcom, że hakiem jest artefakt a nie kochająca go ze wzajemnością ludzka kobieta.
Esme spodobało się to działanie. Widać, że Gabriel planował to już od dawna i zbierał zasoby.

Łącząc dane, jakie Esme dostała z komputera Staszka czarodziejka spróbowała wywnioskować coś na temat sił Mirabelki. Zdecydowała się przejść po Wyjorzu i fizycznie zobaczyć jak wygląda sytuacja i z czym ma do czynienia. Wyjorze zaczyna wyglądać bardzo imponująco - Triumwirat rozwiesił flagi i proporce łącznie z atrakcjami różnego rodzaju; doprowadził też do tego, że da się zakwaterowac 42 członków oddziału Mirabelki "Pokora". Jako, że nie ma jednego miejsca zdolnego ich pomieścić w odpowiednim luksusie, rozdzielono ich na następujące lokalizacje:
- hotel 'Luksus' (Mirabelka, Luksja, 11 agentów)
- hotel 'Jantar' (mniej luksusowy, 16 agentów)
- kilka wynajętych mieszkań z oferty agentów nieruchomości (15 agentów)
Esme poznała lokalizację wszystkich tych mieszkań i agentów. Esme też zauważyła, że Triumwirat ma niesamowitą kontrolę nad Wyjorzem; musiało to zrobić wrażenie na Mirabelce.

### Faza 2: "Negocjacje w Luksusie"

Następnego ranka, wcześnie, Esme jest poproszona przez swoich gospodarzy do pojechania autobusem na dworzec. W autobusie przechwycił ją Tadeusz Umiej i wysłał jej SMSem adres. Powiedział, że jest jej kontaktem i wejściem do hotelu 'Luksus'; jak tylko Esme będzie chciała, on ją wprowadzi. To, czego Esme nie wie to to, że Umiej jest świeżo nawrócony przez Arazille; wczoraj jeszcze nie wiedział o istnieniu bogini marzeń (ale po drodze spotkał się z arcykapłanem Feliksem Hansonem, bo siły Arazille go porwały i skonwertowały). Esme wie też od Umieja, że po południu w 'Luksusie' pojawi się Tymotheus Blakenbauer, porozmawiać z Mirabelką na temat przyszłego sojuszu.
Na miejsce Esme siły Arazille wprowadziły kogoś innego tak, by Millennium nie zostało zdziwione. Ślady jednak będą prowadzić do Triumwiratu.

Esme postanowiła skorzystać z okazji i uszkodzić potencjalne relacje w trakcie spotkania. Plan jest prosty - znaleźć ten fragment sił Mirabelki, który odpowiada za kolekcję i działaniu na danych, by uderzyć właśnie w nich. Celem jest zasymulowanie reakcji Triumwiratu. A skoro te siły Mirabelki już się nią interesują, to jedynie dodaje całej sprawie wiarygodności. Zapytana o to Netheria powiedziała, że jakkolwiek nie ma pewności, ale z tych sił Diakonów o których Esme mówi taki "najbardziej komputerowy sprzęt" trafił do hotelu 'Jantar'. A tam akurat nie ma żadnych magów Millennium.

Niedaleko hotelu 'Jantar' Esme trafiła na trzech agentów "Pokory" którzy rozmawiają z dwoma policjantami. Policjanci protestują - rzeczy, których chcą agenci "Pokory" niekoniecznie są zgodne z prawem a na pewno oni nie maja prawa ich żądać. Agent "Pokory" poradził, by policjant sprawdził sobie wytyczne z komendy główne. Policjant sprawdził i się skrzywił. Agent "Pokory" może żądać czego chce. W odpowiedzi na to agent powiedział, że w poszczególnych miejscach mają być podsłuchy, niektóre miejsca mają być pod szczególną obserwacją, tu i tu ma być dodatkowy policjant na wszelki wypadek...
Esme zauważyła, że policjanci są zrezygnowani, ale nie podoba im się to. Zrobią, czego Millennium oczekuje, choć bardzo niechętnie.

Dobrze... sytuacja jest dość skomplikowana. Grupa skanująca "Pokory" będzie skupiała się na polowaniu na Esme, chyba, że ta wpierw ich unieczynni. Mówimy o kilku osobach znajdujących się w 'Jantarze'. Esme ma pomysł - otruć ich używając alchemicznych środków Myszeczków - w czym najbardziej jej tu pasuje środek oparty na srebrze i wyciągach z ziół dostępnych w pierwszej lepszej aptece; uniemożliwi to wyleczenie owych agentów przy użyciu magii. Problem inny - jak im to podać. Gabriel może udostępnić Esme dostęp do 'Jantaru', ale to potrwa (nie zrobią tego w tej fazie).

Esme weszła do hotelu kupić obiad, by się rozejrzeć i zobaczyć jak wygląda sytuacja. Tak się składa, że w hotelu jest szwedzki stół. Rozwiązało to część problemów; zapłaciła za posiłek gotówką i dyskretnie zatruła wszystkie dania środkiem opartym na srebrze. Ucharakteryzowała się naturalnymi metodami i konsekwentnie dosypała trucizny do jedzenia. I faktycznie, udało jej się (porażka na teście) zatruć analityków i pół hotelu.
Niestety, przez porażke, nie udało się zatrzymać wysłania wiadomości o Esme do Mirabelki (w kolejnej fazie). Dodatkowo, Tymotheus dowie się (też przyszła faza), że jest silna i skuteczna zorganizowana grupa próbująca uniemożliwić sojusz.

Tymczasem Tymotheus i Mirabelka negocjowali w hotelu 'Luksus'...
Esme dostała wiadomość od Netherii. Coś się stało w hotelu 'Luksus'. Zgodnie z informacjami ktoś rozsmarował kota na ścianie pokoju Luksji - od środka. Netheria wie to od agentów w hotelu... i chciałaby to zobaczyć osobiście. Jest nekromantką. Ona będzie w stanie się dowiedzieć co tam się stało rzucając kilka zaklęć. Szczęśliwie dla Esme, zarówno Luksja jak i Mirabelka już to widziały, chwilę po skończeniu rozmowy. Tymotheus wraz ze swoją agentką (Crystal) oddalił się do Trójzęba przepraszając gorąco Mirabelkę.
I wtedy rozbrzmiały syreny. Karetki ruszyły w kierunku na hotel 'Jantar'.
Jako, że Netheria nie ma pojęcia kto rozsmarował tego biednego futrzaka, chciałaby to zobaczyć osobiście. Więc nowe zadanie Esme - przeszmuglować tam Netherię. Wprowadzić, pozwolić jej rzucić czar, wyprowadzić.

Esme wpada na świetny pomysł - po pierwsze, trzeba przebrać Netherię jako sprzątaczkę. Po drugie, katalista dostępny Netherii (Patryk Romczak) ma zrobić dla niej na szybko trzy rzeczy:
- coś, co zrobi straszne Skażenie i zmasakruje aurę (by zamaskować czar Netherii)
- coś, co będzie dywersją; zupełnie, jakby ktoś miał Paradoks
- coś, co będzie wyglądało jakby ktoś chciał rzucić zaklęcie teleportacji
Netheria ma się stawić w melinie niedaleko 'Luksusu' z tymi przedmiotami.

Tymczasem Mirabelka, Luksja i dwóch magów Millennium ruszyło w kierunku na hotel 'Jantar' by pomóc chorym agentom 'Pokory' i rozeznać się w sytuacji. Dwóch magów Millennium zostało w 'Luksusie', w czym wszyscy magowie Millennium dostali rozkazy od Mirabelki asekurowania się nawzajem i wspierania się. Oprócz nich w 'Luksusie' zostało 7 agentów 'Pokory', nie będących magami.

Netheria pojawiła się na miejscu. Przebrana za sprzątaczke. Esme rozmawiała z Tadeuszem Umiejem i załatwiła, że ma być nowa sprzątaczka. Niestety, Umiej powiedział, że pokój Luksji jest off-limits, nikt nie może tam wejść i nie można wprowadzić tam sprzątaczki. W związku z tym - coś innego. Ma zdobyć strój sprzątaczki pasujący do Netherii i do Esme. Esme i Netheria wejdą do hotelu "normalnie" i się tu gdzieś dyskretnie przebiorą. Dostaną nieprawdziwe identyfikatory. Będą mieć też odpowiednie karty (otwierające drzwi do pokoi hotelowych).
No ok, ale Mirabelka jest czarodziejką. I to terminuską. Na pewno zamontowała jakieś pułapki w pokoju Luksji. 
W związku z tym Esme poprosiła o jeszcze jeden artefakt. Bombę magiczną. W odpowiedzi na to zaproponował nieco inne rozwiązanie Patryk Romczak - podpiąć jakieś uziemienie i zrobić coś co uaktywni te pułapki a dopiero POTEM walnąć bombę magiczną. Ale trzeba ofiarę, kogoś, kto "uaktywni" pułapki pierwszy. Kogoś, kto będzie kozłem ofiarnym. Esme - z wahaniem - zasugerowała Netherii użycie do tego celu członka 'Pokory'. Ta się zgodziła bez żadnego problemu. W końcu wyhodowani ludzie nie mają osobowości ani "duszy".

Czyli plan jest taki: uruchomienie zaklęć na nieszczęśniku -> 'Paradoks' -> uziemienie na członku Pokory -> teleport -> Netheria działa w pokoju Luksji -> brudna bomba zacierająca ślady. Wiadomo też jak dostać się do Jantara, jest plan i trzeba to zrobić. A katalista się przyłożył i skombinował szybko Esme wszystko, co potrzebne.

Esme ma rozkład kamer i agentów. Netheria i Esme wchodzą do budynku, już ucharakteryzowane. Idą do różnych ubikacji całkowicie normalnie a tam przebierają się za sprzątaczki (def_sub_ActHrt 11v9->13). Bez kłopotu udało się Esme ominąć strażników i kamery; nikt się nimi nie zainteresował. Przeszły koło piętra z pokojami Mirabelki i Luksji - siedzi tam dwóch agentów 'Pokory'. Ci agenci są ludźmi; są szkoleni bojowo. Niestety, całe piętro jest do dyspozycji Mirabelki i na tym piętrze są wszyscy agenci 'Pokory'.

Szczęśliwie, Esme była na to przygotowana. Przed akcją przygotowała gaz bojowy używając alchemii Myszeczków. Gaz ten ma antidotum które dostała Netheria i Esme. I zwyczajnie Esme po wjechaniu na piętro rzuciła butelkę i gaz (barwny i CHOLERNIE wonny) rozlał się po piętrze. (11v|6-13|->9) Niestety, nie wszystko się udało tak jak powinno; gaz unieszkodliwił natychmiast agentów w holu pilnujących pokoju Luksji oraz z uwagi na swoją specyfikę wyłączył magów Millennium znajdujących się na tym piętrze na parę minut, ale niestety gazem podtruły się lekko Netheria i Esme; po prostu stężenie było za wysokie.

Z jednego pokoju wypadło 3 agentów; zataczają się i nie widzą przez ten dym. Esme nie jest w dobrej formie, ale oni są w gorszej. Esme i Netheria używając kijów od mopów zaczęły wybijać lokalnych agentów Mirabelki. Zaczynając od splotu słonecznego (11v7->9). Bez kłopotu Esme się uporała z trzema kolejnymi agentami.
Na szybko, Esme założyła uziemienie na obu agentach 'Pokory' i odpaliła pułapki Mirabelki. Esme wycofała Netherię i wsparła się magią - i tak ma zamiar zostać nierozpoznana (14v12->12). Znowu nie jest perfekcyjnie; udało się nie ściągnąć Mirabelki i całkowicie zamaskować obecność Esme (Patryk to niezły katalista), ale niestety tych dwóch agentów 'Pokory' zginęło oraz pojawiło się dwóch przygotowanych agentów (w maskach) atakujących z zaskoczenia (10v10->9). Esme dała radę usunąć przeciwników, ale niestety mieli broń palną; postrzelili i Esme i Netherię. Broń palna trafia do Esme (okradła nieprzytomnych agentów).

Czarodziejki, ranne, wkroczyły do pokoju Luksji. Esme załatwiła opatrunek a Netheria przygotowała się do rzucenia zaklęcia. Pokój wygląda, jakby zniszczenia były dokonane w ataku szału. I jest kot rozmazany na ścianie. Netheria rzuciła zaklęcie i przeżyła wstrząs. Zobaczyła, jak do pokoju weszła Crystal - prawa ręka Tymotheusa - z martwym kotem. Wyraźnie nie jest sobą. Słania się i walczy ze sobą. Crystal w furii i rozpaczy (nie chce dopuścić by to co ją spotkało dotknęło kogoś innego niż ją - ergo, chce zatrzymać sojusz) niszczy pokój Luksji i rozmazuje kota po ścianie.

Netheria po konsultacji z Esme zdecydowała się wzmocnić astralne i nekromantyczne sygnały z tego pokoju. Mirabelka musi wiedzieć, jak wygląda sytuacja. Że Crystal była Diakonką.
...za to było warto dać się postrzelić i ujawnić aurę Netherii, Patryka i trochę swojej. Plus, zabić dwóch agentów 'Pokory'.

Ranne czarodziejki wycofały się z hotelu po zakończeniu działań i zamelinowały się u Tadeusza Umieja do rana. A broń agentów zostaje na miejscu; bezpieczniej zostawić i nie dać się odnaleźć...

Wraca Andromeda do Wyjorza, z ołtarzem Iliusitiusa.

## Dark future:

### Faza 1:
- Siły Mirabelki wchodzą do Wyjorza, zajmując odpowiednie pozycje (~50 ludzi). Mirabelka z Luksją osadzają się w 'Luksusie'.
- Arazille stopniowo uruchamia ruch oporu i wszystkie kontakty w Wyjorzu.
- Tymotheus przygotowuje w Wyjorzu odpowiednie miejsce dla Mirabelki, Luksji i ich świty (show of discipline). Miasto dostosowuje się do żądań. Mirabelka jest pod wrażeniem.
- Kult Arazille przygląda się Mirabelce i jej siłom bardzo uważnie; nawróceniu ulega część personelu hotelowego (min. Tadeusz Umiej).
- Feliks Hanson przybywa do Wyjorza i odbyć się ma rytuał wzmocnienia.

### Faza 2:
- Oddział 'Pokora' zabezpieczył perimeter, doprowadzając do pewnych konfliktów z mieszkańcami Wyjorza.
- Ruch oporu zlokalizował następną dostawę ludzi do Trójzęba.
- Tymotheus i Mirabelka spotykają się w 'Luksusie' by wynegocjować, co z tym robią dalej i zasady współpracy. Gabriel jest w ochronie.
- Crystal, widząc Luksję i wyhodowanych Diakonów ma epizod psychotyczny; niszczy pokój Luksji i rozmazuje kota na ścianie.
- Netheria chce koniecznie dostać się do hotelu zobaczyć ten epizod z rozmazanym kotem; jest nekromantką. Potrzebna dywersja.
- Feliks Hanson ulega transformacji w viciniusa mocą Arazille. Pani Snów wykorzystuje populację Wyjorza.
- [jeszcze ukryte]
- [jeszcze ukryte]
- [jeszcze ukryte]

# Zasługi

* mag: Esme Myszeczka, pomysłowa najemniczka która każdego otruje swoimi dziwnymi specyfikami. Ranna po infiltracji hotelu 'Luksus'.
* mag: Netheria Diakon, mastermind akcji i osoba pokazująca, że każdy może być czarnym koniem. Ranna po infiltracji hotelu 'Luksus'.
* vic: Crystal Shard, cicha bohaterka której działania być może uniemożliwiły sojusz Mirabelki z Tymotheusem. Kiedyś Diakonka. Nadal walczy z tym co jej zrobiono.
* mag: Tymotheus Blakenbauer, który, jak się okazuje, nie kontroluje Wyjorza w takim stopniu jak myślał. Sojusz z Mirabelką rozpada mu się w rękach.
* mag: Mirabelka Diakon, terminuska której powoli przechodzi chęć sojuszu z Tymotheusem. Ambicja i potencjalna moc nie są warte wszystkiego - a na pewno nie jej córki.
* mag: Luksja Diakon, której pokój zbezcześciła Crystal. 
* mag: Gabriel Newa, fanatyk (w oczach Esme), który dla zniszczenia Triumwiratu poświęciłby wszystkich bez mrugnięcia okiem.
* czł: Tadeusz Umiej, pracownik hotelu Luksus w Wyjorzu, nawrócony przymusowo przez Arazille na agenta. Nieoceniony w zapewnianiu miejsca do spania i w infiltracji hotelu.
* czł: Zofia Perszen, sympatyczna pani, która stanowi chwilowe centrum dowodzenia sił Arazille dla Esme. I pokój dla Esme. Żona, matka 3 dzieci. Kucharka.
* czł: Staszek Perszen, młody chłopak zbierający informacje o ruchach i komórkach Arazille dla Esme, przez co trafił na celownik Millennium.
* mag: Konrad Węgorz, astralika # iluzje, mag Triumwiratu który jest szantażowany przez Gabriela (co powiedział Esme by miała jeszcze jedną wtykę).
* mag: Patryk Romczak, katalista w służbie sił anty-Triumwiratowych, który okazuje się być bezwzględny i kompetentny. Niebezpieczne połączenie.

# Lokalizacje:

1. Wyjorze
    1. peryferia
    1. centrum
        1. mieszkanie Perszenów
        1. mieszkanie Umiejów
        1. hotel 'Luksus'
        1. hotel 'Jantar'
        1. "Trójząb", forteca Triumwiratu.

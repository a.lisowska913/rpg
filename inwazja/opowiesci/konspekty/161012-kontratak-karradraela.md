---
layout: inwazja-konspekt
title:  "Kontratak Karradraela"
campaign: powrot-karradraela
gm: żółw
players: kić, dust
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [160914 - Aleksandria, krwawa Aleksandria... (HB, KB)](160914-aleksandria-krwawa-aleksandria.html)

### Chronologiczna

* [160914 - Aleksandria, krwawa Aleksandria... (HB, KB)](160914-aleksandria-krwawa-aleksandria.html)

## Kontekst ogólny sytuacji

## Punkt zerowy:

Karradrael zdecydował się na wysłanie sił ciężkich do eliminacji Hektora i Silurii.
Wysłał The Governess - Karolinę Maus.

Ż: Gdzie znajduje się bezpieczne miejsce dla Blakenbauerów poza Rezydencją?
D: Chatka w lesie.
Ż: Dlaczego Siluria znalazła się w miejscu X?
K: Jechała do KADEMów; in transit.
Ż: Jakie są cechy charakterystyczne miejsca X?
D: Stary posąg anioła.
Ż: Jakie wsparcie ma Siluria (nie Infensę)?
K: Mikado Diakon.
Ż: Jakie wsparcie ma Hektor poza Rezydencją?
D: 2 magów bojowych, wcześniej elementy Aleksandrii.
Ż: Wyspecyfikuj maga z Aleksandrii.
K: Anatol Weiner. Już wyspecyfikowany.
Ż: Wyspecyfikuj terminusa z Aleksandrii.
D: Katalista. Mistrz entropii. Łamacz zaklęć i energii. 

## Misja właściwa:

Dzień 1:

Rezydencja jest w opałach. 373 uratowane osoby, ludzie i magowie (45 magów). Każda z tych osób potencjalnie wymaga leczenia, wymagają wsparcia Edwina...
Leczyć partiami? Staza? Wybudzamy? Wszystko elementy Aleksandrii...
Nawet Profesor Otton Blakenbauer wyszedł ze swojej nory pomagać ludziom i magom. To samo Margaret; wszyscy. Wszyscy z rodu.

Hektor pomaga mentalnie; vs15. On czyści umysły z traumy, wspiera Rezydencję pomagając jej sobie poradzić, usuwa myśli zakłócające Rezydencję... udaje się. Sytuacja jest w miarę stabilizowana.

Klara pomaga aleksandryjsko; vs15. Ona ich stabilizuje, wie co się dzieje, przewiduje zjawiska. Koordynuje i usprawnia. Szczęśliwie, udaje jej się. Blakenbauerowie dają radę. Klara pośrednio łata ich jeszcze technomancją; próbuje zastąpić Aleksandrię w wypadkach krytycznych by dać im nadzieję.

"Hektorze, zajmij się swoim sojusznikiem" - Otton, pokazując na akwarium (Szlachta)

Wiktor Sowiński. Chce swój projekt. Teraz.

"Ile zajmie zmiana tych magów w Blakenbauerów?" - Hektor do Edwina
"Musimy mieć sprawną Rezydencję. Chwilowo... nie mamy nic. Normalnie - dzień. Teraz - miesiąc?" - Edwin, do załamanego Hektora

"Maus!" - krzyknął Lancelot Bankierz.

Mag - Roman Błyszczyk - zaczął prostestować. Nie jest Mausem! Lancelot powiedział, że jest, był elementem Aleksandrii. Hektor wysłał go do stazy. Błyszczyk zaczął prosić, że zginie. Hektor zignorował, wsadzili Błyszczyka do stazy i... Błyszczyk zginął. I był Mausem. 

"Cóż mogę powiedzieć... cholernie mi głupio. Naprawdę." - Dust.

Klara i Edwin naskoczyli na Hektora. Margaret zauważyła, że to znaczy, że Mausów tu będzie więcej...

Hektor powiedział Wiktorowi, że część rytuałów i procesów po Zaćmieniu wpłynęło negatywnie na jego projekt. Nie ma swoich istot sprzed Zaćmienia. Wiktor zmarszczył brwi. Zażądał od Hektora wydania kilku magów - powiedział ich z imienia i nazwiska. Koło 10. Są w Rezydencji.
Otton powiedział, że śmierć tego maga zakłóciła Rezydencję...
Wiktor powiedział, że przez tych magów Rezydencja się rozpada.

Ano, Otton potwierdził. Coś się dzieje; chyba Kompleks Centralny emituje sygnał. Rezydencja wpada w rezonans; zaczyna się rozpadać. Nie przetrwa 24 godzin...
Hektor i Klara znaleźli cel sygnału. Punktem triangulacji jest Hektor...

Otton się nie zgodził, by Hektor opuścił Rezydencję.
Vladlena zaproponowała, by Hektor sprowokował Wiktora do strzelenia Intensyfikatorem - ona użyje syberionu, by zarazić Kompleks i zniszczyć Intensyfikator. Hektor spojrzał na nią jak na idiotkę. Przecież... Kompleks Centralny zostanie zmieniony w Syberię.
Vladlena zaproponowała więc użycie syberionu do wzmocnienia Rezydencji. Otton ręką zatoczył i pokazał wszystkich tych ludzi i magów. Powiedział, że nie da się ich skłonić do jednomyślenia. Vladlena, nieco podłamana, zgodziła się na to by jednak zadzwonić do Emilii...

Emilia Kołatka. 
Zaczęła z grubej rury. Ma plany broni pochodzącej z Wojen Bogów, broni opracowanej przez Iliusitiusa. Broni, która niszczy skutecznie wszystkie byty powiązane z Magią Krwi.
I potrzebny jej Czarny Diament, ze Świecy Daemonica.
To pozwoli jej złożyć superciężki granat i uwolni Kompleks Centralny od Dagmary Wyjątek i jej podobnych...
Hektorowi spodobał się ten pomysł.

Klara dostała koordynaty. Test vs 22. Klara spojrzała, przerażona - nie jest w stanie się tam dostać...
Zgłosiło się kilku ochotników do Aleksandrii, ale zostali odrzuceni...

Vladlena, przerażona, powiedziała, że Aneta Rainer poinformowała, że siły Dagmary Wyjątek zniknęły. Dagmara zaatakowała Emilię Kołatkę...
Dagmara ze swoimi siłami weszli do posiadłości Emilii. Emilia się uśmiechnęła. Dagmara zaatakowała jak wąż... Saith Kameleon uniknęła ciosu.
Saith Flamecaller zaatakował...
Z perspektywy postaci, wszyscy w posiadłości Emilii zginęli.

Kompleks Centralny zaczął wybuchać i się rozpadać...

Ale niestety, Hektor nadal degeneruje Rezydencję. Okazuje się, że gdy podczas Aleksandrii Hektor czerpał mocy Magii Krwi, Karradrael go przeklął. Hektor jest wektorem Karradraela, niszczącym Rezydencję...

Hektor musi opuścić Rezydencję na jakiś czas...

Anna Myszeczka dała Hektorowi eliksir mający chronić Hektora przed wykryciem ze strony Karradraela. Powiedziała, że go ochroni. Uratuje go przed Karradraelem i przed siłami Mausów...
Klara zostaje i pracuje z Aleksandrią. Hektor i Anna uciekają do leśnej chatki Blakenbauerów.

Anna i Hektor siedzą w leśnym dworku Blakenbauerów. Terminuska uśmiecha się do Hektora ze smutkiem.
Anna Myszeczka przyznała się Hektorowi, że tak naprawdę są martwi. Nie da się ukryć przed Karradraelem. Ona go okłamała. Poprosiła go samego po to, by móc uratować wszystkich innych. Ona i Hektor tu zginą; w chwili, w której Karradrael naznaczył Hektora, ten jest już martwy.
Ale mogą sprzedać skórę drogo lub tanio. Hektor chce walczyć. Więc udają się do Rzecznej Chaty - jedynego miejsca, które NAPRAWDĘ jest chronione.

W Rzecznej Chacie siedzi Siluria. Z Mikadem.
Piją herbatkę i czekają na magów KADEMu, którzy mają przyjechać niewidzialnym samochodem Setona.
Bezpieczeństwo Rzecznej Chaty zapewnia statua Anioła - bardzo potężny artefakt, wielokrotnie ładowany.

Nagle do pomieszczenia wpada przerażona terminuska z Hektorem Blakenbauerem. Dzięki sprzętowi Dionizego i dzięki kunsztowi Anny (lekko rannej) udało im się wpaść do azylu. Siluria poczuła potęgę MOCY uderzającej w Rzeczną Chatę. Moc ta nie należy do maga...
Siluria wysłała sygnał na KADEM. "Stay away"...

Czterech czy pięciu magów w Rzecznej Chacie rzuciło się do stawiania tarcz i defensyw. Totalnie niegroźni też...

"Co byś zrobił?" - Siluria do Mikada
"Jeszcze nie wiem. Wymyślę coś. Ewakuuję nas..." - Mikado, bez większej nadziei

"Zabiłeś nas wszystkich, prawda?" - zrezygnowany Mikado do Hektora
"Jedno słowo. Karradrael." - Hektor

Anna i Mikado zebrali się i zaczęli szybko oceniać sytuację taktyczną. Póki mają Anioła są w stanie...
Spustoszona Ofelia Caesar swoją krwią Skaziła Anioła. Mikado ją unieszkodliwił, ale to znaczy, że Karradrael tu wejdzie...

Mikado spojrzał z wyrzutem na Annę. Anna na Mikado. I ruszyli do pracy. 
Siluria zabrała się do kontrolowania zastałych magów. Udało jej się.
Po wyłączeniu Spustoszenia EMP, Hektor zmusił magią Ofelię do wzmocnienia i uruchomienia systemu zabezpieczeń. Ofelia jest w bardzo złym stanie fizycznym, ale udało jej się to zrobić...

Pierwsza faza wbijania się sił Spustoszenia i Karradraela do środka została odparta automatycznie. Ale w końcu weszli...

Karolina Maus. I kohorta...
Siły Spustoszenia. Siły fizyczne. Potwory. Magia. Ogień zaporowy.
Hektor odpala granaty EMP. Siluria wykorzystuje swój wpływ i rzuca magów przodem.
Mikado, chroniąc Silurię, zostaje ranny. Ale przetrwała. Hektor chroniony przez Annę się przebił.
Hektor nie ma już granatów EMP. Magowie mający ich ratować są na podłodze, w trakcie Spustoszenia...

Siluria i Hektor czują moc Karradraela. A Hektor zwłaszcza; podczas Aleksandrii szczególnie zaczerpnął jego mocy.

Siluria, Hektor, Mikado i Anna rozpaczliwie lecą po schodach do góry, zostawiając za sobą dogorywających sojuszników.

Karolina: "come to me" (22). Magiczny, katalityczny, krwawy i korupcyjny. Z krwawym smakiem Karradraela, dla Hektora.
Siluria próbuje to skontrować; nadaje aurę Arazille Annie. Ma nadzieję odwrócić uwagę Karoliny... udało się! Karolina jest skonfundowana.
Hektor: działania Silurii osłabiły wpływ mocy Karoliny przeciwko niemu. Hektor stworzył potężny atak mentalny przeciwko Karolinie. Coś, co sprawi, że Karolina się zatrzyma. Bazując na Silurii i wiedzy z Aleksandrii spróbował wbić klin między Aurelię, Karolinę i Karradraela.

I udało mu się... na MOMENT.

Przez MOMENT wszystko stanęło. I wtedy! 

"Ignaaaat! JAK TY PROWADZISZ!" - Marian Łajdak, gdy samochód walnął w ścianę i spadł bokiem.
"Wsiadajcie!" - zrozpaczona Andżelika

GS Aegis zaatakowała Karolinę krzycząc "dawno się nie widziałyśmy, kochanie!". Karolina odparła atak, krzycząc "nie jesteś ani nią, ani mną!". Karradrael manifestuje, zerg atakuje. Anna chce z Głowicą zniszczyć Karolinę. Jeśli Anna tu zostanie, to zginie...

Hektor pomaga Mikado dostać się na pokład samochodu.
Siluria próbuje przemówić do rozsądku Annie. Anna jest zaślepiona, nikogo tak nie nienawidzi jak Karoliny. Ale Siluria uderzyła w "ratuj nie potrafiących walczyć magów". I udało jej się, wszyscy wpakowali się na pokład samochodu...

Aegis dzielnie walczy z Karoliną. Nie wygrywa, ale nie może się nie oderwać.

Wszyscy w samochodzie. Nic nie działa poprawnie. Andżelika, zrozpaczona, uruchomiła polecenie "znajdź Matuzalema". Nie ma nigdzie Matuzalema, więc samochód wskoczył w Bezczas. Uciekli Karolinie...

# Progresja


# Streszczenie

W Rezydencji jest za dużo ludzi i magów; Rezydencja się rozpada. "Emilia" - Saith Kameleon - usuwa z gry Dagmarę Wyjątek i Kompleks Centralny ulega serii uszkodzeń i wybuchów. Jakby było mało, Karradrael ma namiar na Hektora (który zaczerpnął Magii Krwi w Aleksandrii) i Karradrael aktywnie Rezydencję destabilizuje. Anna Myszeczka i Hektor opuszczają Rezydencję; Anna chce uratować innych i poświęcić siebie i Hektora - uciekli do Rzecznej Chaty gdzie jest... Siluria. Przetrwali morderczy atak Karradraela, bo wpadł Opel Astra Setona i Głowica GS Aegis 0003 w formie Jadwigi stawiła czoła Karolinie. Aurelia kontra Aurelia. Siluria, Hektor, Anna, Andżelika, Mikado i Ignat odjechali Oplem... ranteleportem między fazami.

# Zasługi

* mag: Hektor Blakenbauer, Przeklęty przez Karradraela, skazany na Spustoszenie, uratowany przez sojuszników i ratujący sojusznikom życie
* mag: Klara Blakenbauer, operatorka niemożliwych koordynatów pająka fazowego, wykryła, że to Hektor jest wektorem Karradraela i stabilizowała ofiary Aleksandrii
* mag: Siluria Diakon, odwróciła uwagę Karradraela używając aury Arazille i uratowała Annę przed śmiercią udowadniając jej że jest potrzebna 
* mag: Edwin Blakenbauer, leczący i utrzymujący post-Aleksandryjskich ludzi i * magów i czyniący cuda. Ale nie umie fałszować dokumentów.
* mag: Margaret Blakenbauer, która nie zdążyła przekształcić * magów w Blakenbauerów. ALE! Wraz z Edwinem ratuje życia.
* mag: Otton Blakenbauer, koordynujący bardzo osłabioną Rezydencję i nie chcący poświęcić Hektora.
* mag: Wiktor Sowiński, działał zgodnie z wolą Dagmary i próbował wydobyć jak najwięcej od Hektora. Nie otrzymał nic.
* mag: Lancelot Bankierz, wykrył, że Roman Błyszczyk jest Mausem. Chciał się przydać jako terminus entropii - ale nikt mu nie zaufał
* mag: Roman Błyszczyk, zmieniony w Mausa jako interfejs komunikacyjny Aleksandrii. Zginął śmiercią tragiczną, w stazie. KIA.
* mag: Vladlena Zajcew, z dość desperackimi pomysłami odnośnie syberionu i zniszczenia Kompleksu Centralnego. Na szczęście poprosiła też o telefon do Emilii.
* mag: Dagmara Wyjątek, podsłuchała uszami Mausów fałszywy plan Hektora i Emilii, zaatakowała Emilię... a to była saith Kameleon. KIA.
* mag: Anna Myszeczka, zdesperowana terminuska, która oszukała Blakenbauerów by zginąć wraz z Hektorem - wiedziała, że nie da się ukryć przed Karradraelem. Przypadkowo przeżyła.
* vic: Saith Kameleon jako Emilia Kołatka, ściągnęła Dagmarę w pułapkę i wraz z Flamecallerem dokonała egzekucji. Wycofała się, choć straciła kilku * magów.
* vic: Saith Flamecaller, wraz z Kameleon zniszczył Dagmarę Wyjątek i elitarny oddział Karradraela dowodzący Kompleksem Centralnym.
* vic: Karradrael, częściowo manifestujący się w Rzecznej Chacie i mający wystarczającą moc by przebić się przez wszystkie zabezpieczenia.
* mag: Karolina Maus, która tym razem prawie dorwała Hektora i Silurię. Ale KADEMki i Aegis porwali cel Karolinie sprzed nosa. Skończyła walcząc z Aegis.
* vic: GS "Aegis" 0003, która w formie Jadwigi zaatakowała Karolinę by kupić czas KADEMkom i Hektorowi na wycofanie się.
* mag: Mikado Diakon, który tylko chciał eskortować Silurię a wpadł w środek wojny z Karradraelem. Ranny, broniąc Silurii. Ale obronił.
* mag: Andżelika Leszczyńska, która w rozpaczy wydała Oplowi Astra Setona rozkaz "znajdź Matuzalema". I spowodowała dziwny przeskok fazowy.
* mag: Ignat Zajcew, który połączył się z resztą KADEMek i który naprawdę nie umie prowadzić Opla Astra Setona (Seton też nie umiał).
* mag: Ofelia Caesar, Spustoszona agentka Karradraela, która wpuściła Karradraela do własnego sanktuarium wbrew swej woli.

# Lokalizacje

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Centrum
                        1. Kompleks Centralny Srebrnej Świecy
                        1. Rzeczna Chata
                    1. Las Stu Kotów
                        1. Leśna chatka Blakenbauerów
                    1. Osada Strażnicza
                        1. Dworek Emilii Kołatki

# Skrypt

- Przy 5 Żetonach Faila Aegis jest zbyt poważnie uszkodzona by przetrwać.

# Czas

* Opóźnienie: 1 dni
* Dni: 1
---
layout: inwazja-konspekt
title:  "Dzień z życia Klemensa"
campaign: powrot-karradraela
gm: żółw
players: kić, dzióbek
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [150330 - Napaść na Annalizę (SD, PS)](150330-napasc-na-annalize.html)

### Chronologiczna

* [150330 - Napaść na Annalizę (SD, PS)](150330-napasc-na-annalize.html)

# Wątki

- Wszystkie dziewczyny Marcelina

## Punkt zerowy:

Ż: Dlaczego Marcelin dał nieszczęsnej Beacie coś na koszmary senne?
K: Męczyły ją dość długo, więc chciał jej pomóc.
Ż: Skąd Marcelin się dowiedział, że coś poszło bardzo nie tak?
B: Wszystko się zdecydowanie pogorszyło mimo chwilowej poprawy.
Ż: Dlaczego do akcji wysłano zarówno Klemensa jak i Alinę?
K: Klemensa, bo Marcelin ma problemy. Alinę, na wypadek gdyby okazało się że to coś więcej niż nocne strachy.
Ż: Jaka własność Beaty pasuje do wzoru "wszystkie dziewczyny Marcelina Blakenbauera"?
B: She is too perfect.
Ż: W jakiej sprawie Beata manifestuje i protestuje w stylu "Occupy"?
K: Ekolożka; niszczenie rzadkich obszarów lęgowych.
Ż: Kto zauważył, że Beata zniknęła?
B: Marcelin obudził się bez niej przy boku.

## Misja właściwa:

Marcelin poprosił do siebie Alinę i Klemensa. Alina się zdziwiła, że ona też tam jest. W pokoju Marcelina - niespodzianka. Marcelin, obity i nieszczęśliwy i Margaret, lecząca mu rany. Krótkie wyjaśnienie i wszystko jasne - Marcelin powiedział, że miłość jego życia zniknęła i on próbował ją znaleźć i został pobity.
Alina i Klemens zaczęli przesłuchanie.
- Poznał Beatę na manifestacji w obronie Lasku Pejzażowego pod Pirogiem Dolnym przed wycięciem. Przyniósł jej termos.
- Chciał kupić rybę w sklepie rybnym (zobaczyć jak to jest być człowiekiem) i poznał miłość swego życia.
- Poznał ją tydzień temu. Ale to jest prawdziwa miłość.
- Miała koszmary. Od miesiąca. Dawniej, niż ją poznał. Więc trzy dni temu zrobił dla niej maść na koszmary. Zamaskował ją w pudełku L'oreal, na twarz.
- Marcelin poszedł spać z Beatą wieczorem. Rano się obudził, jej nie było. Poszedł szukać Beaty (nie mając klucza do mieszkania, więc zostawił otwarte ALE zostawił w środku pająkołowcę - schowanego w ubikacji). Nie znalazł jej, ale w jednej z uliczek gdzie szukał został pobity.
- Rodzice Beaty nie lubią Marcelina; przyłapali ich w łóżku gdy przyszli z niezapowiedzianą wizytą (bo Beata protestowała przeciw firmie, w której ONI pracują). 

Marcelin chce iść z Klemensem i Aliną. Margaret zaprotestowała i użyła argumentu "złamanie żebra Adama, które to złamanie powoduje impotencję".
Alina i Klemens doszli do tego, co się stało. Maść działająca na Beatę powoduje, że mag nią wysmarowany zaśnie jak kamień... więc Beata się wysmarowała a potem poszli się całować... i nie tylko.
Beata MOGŁA zostać porwana. Mogła pójść gdziekolwiek.

Czyli podsumowując:
- mają na wolności jeden artefakt w rękach człowieka. Nie wiadomo gdzie jest i Marcelin nie umie go zlokalizować, co jest bardzo dziwne.
- mają na wolności jednego pająkołowcę. "Ale jaka jest szansa, że ktoś tam wejdzie."
- ktoś pobił Marcelina; a zwykle nikt normalnie w dzień nie bije nikogo w tych obszarach Kopalina.

Margaret zaofiarowała swojego węża. Tzn. węża, który jak ugryzie ofiarę, owa ofiara zaczyna zapominać. Margaret obiecała Alinie, że ta będzie mogła wężem dowodzić. Alina z uśmiechem stwierdziła, że na czas tej akcji będzie miała węża w kieszeni.

Alina spytała jeszcze Marcelina, skąd wziął tam tego pająka. Marcelin powiedział, że wyjął go z flaszki. Technika syberyjska. Skąd Marcelin miał technikę syberyjską butelkowania? Jego kochanka mu dała kilka takich flaszek.
Klemens zatrzymał się, wstrząśnięty. JAKA kochanka?
Zajcewka. Ale teraz, jak KADEM i SŚ się nie do końca lubią, to on nawet nie ma jak z nią porozmawiać.
Zajcewka z KADEMu...? Niejaka Infernia Zajcew. Marcelin poszedł z nią do łóżka i powiedział "zostańmy przyjaciółmi" potem. Infernia nie doceniła, oj nie.
Zdaniem Klemensa i Aliny Infernia może się mścić na Marcelinie i NAGLE pojawił się motyw porywania jej następczyni. Nadal nie bardzo prawdopodobny motyw (Zajcew raczej spali niż porwie), ale motyw.

Co za niefart.

Dobrze, Alina i Klemens pojechali do mieszkania Beaty Zakrojec by zabrać pająkołowcę. Zabrali ze sobą smacznego wija (przysmak pająkołowcy). Przed budynkiem - pogotowie gazowe. Podeszli do mieszkania i weszli do środka; dwa kokony na suficie. Oczywiście, za nimi drzwi zamknięte. Alina wyjęła smacznego, bardzo nieszczęśliwego wija i zaczęła wabić pająkołowcę. 
Stwór zaczął biec w jej kierunku (a raczej w kierunku wija). Otworzyły się drzwi za agentami i weszła bardzo zaskoczona terminuska. Klemens chciał ją przez łeb i ogłuszyć; jednak tarcza go odrzuciła (choć nie zrobiła krzywdy). Pomiędzy pająkiem i terminuską Alina z trudem utrzymała wija, lecz operację kontynuowała. Pożywiony pająk wlazł do torby podróżnej a terminuska (Judyta Karnisz) zadała pytanie kim są (po tym jak Alina kontrolnie spytała "tien...?".

Przedstawili się jako agenci w akcji dowodzonej przez Mojrę. Judyta sprawdziła to na hipernecie; Mojra potwierdziła. Terminuska oddała działanie zespołowi a sama zaczęła zabezpieczać teren i czyścić. Zapytana przez Alinę o to, czy były tu jakieś ślady magii, Judyta wykryła coś nietypowego - artefakt, który się sam przekształca. Trochę tego na pościeli i poduszce (maść Marcelina). Ostrzeżona SMSem przez Klemensa, Mojra zablokowała możliwości działania Judyty w zakresie. Terminuska się ukłoniła i opuściła ten teren.

"Nie potrzebujemy wrogów: KADEMu, potworów... mamy politykę, tien Mojro." - Judyta po hipernecie.

W łazience Klemens zauważył rozbite lustro z odrobiną krwi. Jaky ktoś rozbił lustro uderzeniem pięści. Klemens zabrał krwawe odłamki i z Aliną pojechali do Rezydencji, pozostawiając za sobą nieprzytomnych gazowników i wyczyszczony teren. Alina zebrała wszystkie ślady jakie była w stanie.
- wszystko wskazuje na to, że dzień Beaty zaczął się jak zwykle; w pewnym jednak momencie przeszła przez łazienkę i się zestresowała znacząco. Próbowała dobudzić Marcelina ale bez powodzenia; wyszła.
- na bazie jej dokumentów, pamiętnika i stylu życia jest to osoba, która preferuje samotność a nie przyjaciół i rodzinę gdy ją coś boli. Najpewniej wycofała się i ukryła gdzieś w świecie przyrody.
- Alina dotknęła krwi i zaczęła transformację by zobaczyć w co. Przekształciła się w Beatę, ale zaczęły jej rosnąć włosy na całym ciele, policzkach itp. Transformacja nadal trwa. Retransformacja zajmie 1-2h.
Klemens zaczął czyścić ślady, owinęli Alinę i pojechali do Rezydencji.

Nadal największe zagadki:
- gdzie jest Beata?
- skąd się tu wzięli gazownicy?
- skąd się to wzięła terminuska?
- co stało się z tą cholerną maścią?

Margaret dostała maść do zbadania i ją przebadała. Ma wnioski.
- maść jest artefaktem ze zdestabilizowanym rdzeniem, ale otoczka udaje, że się nie zmienił. Innymi słowy, działanie celowe.
- maść ma dodatkową funkcjonalność: pozbawia dziewczynę snu (żywi się jej snami) ale nie daje jej odpocząć. Może wywoływać halucynacje.
- maść nie ma tak silnej reakcji na dziewczynę jak ma na Alinę.

Klemens zapytał jeszcze Marcelina o to, jakiego typu były koszmary Beaty. Odpowiedź "wstanie lasek i zabije jej rodzinę"; ogólnie rzeczy powiązane z samym laskiem.
Potem Klemens zapytał Mojrę czy coś DZIEJE się w tym lasku co może się dowiedzieć. Faktycznie, od miesiąca prowadzony jest projekt badawczy "Dreamcatcher". W lasku zakopany jest artefakt intensyfikujący sny i je pobierający. Oczywiście, każdy mag dostaje informację o projekcie badawczym zbliżając się do lasku...
...ale Beata nie jest magiem. I artefakty mogły interaktować.

Co, Marcelin nigdy nie był w tym lasku? Nie. Protestował w mieście.

"Zdecydowanie, protesty są łatwiejsze jak się ma Coffee Haven za rogiem" - Klemens do Marcelina o protestowaniu w mieście.

Zdaniem Klemensa, to wygląda na potencjalne działania Inferni Zajcew. Kiedyś: Inferni Diakon. Zajcew-Diakon na KADEMie, innymi słowy, potencjalnie wybuchowa mieszanka.
Istnieje też szansa, że Beata znajduje się w Lasku Pejzażowym, w Pirogu Dolnym... i może da się do niej dostać.

Margaret dała im do pomocy jamnika o imieniu Warczuś, który jest zaprojektowany do dobrego wyszukiwania. Jest nastawiony na zapach Beaty. Może się uda ją znaleźć...

I znalazł. Beata siedzi na rozłożystym drzewie, schowana w koronie (nie widać jej). Szczęśliwie Alina okazała się świetną manipulatorką i wrobiła Beatę, że "dusze drzew które próbują wszystkich dopaść" boją się ognia a ona ma zapalniczkę...
"The power of 20" - Dzióbek.
Beata zeszła w dół i pojechali do Rezydencji. W lusterkach samochodu dojrzeli maga KADEMu wchodzącego do lasu, szukającego czegoś.

Mojra i Margaret zabrały się do badania i pomocy Beacie; ich zdaniem biedactwo jest w 100% do odratowania. Dodatkowo, Margaret skupiła się też na przyniesionej przez Beatę maści; ciekawe, w jaki sposób została przebudowana...

#### EPILOG:
- Maść została przekształcona przez jakiegoś katalistę; da się to zrobić, choć zwykle nie ma po co. Rzadko destabilizuje się artefakty w taki sposób (nie opłaca się to).
- Beata będzie w pełni naprawiona.
- Powodem tego była jedna z następujących spraw: "Marcelin - eks", "Srebrna Świeca - KADEM" (gdzie Marcelin był pretekstem) lub "Blakenbauerowie - reszta świata" (gdzie Marcelin był pretekstem). Lub coś najzupełniej losowego.

Nadal nie wiadomo dokładnie o co chodziło.

# Streszczenie

Przez zbieg okoliczności dziewczyna (ludzka) Marcelina która dostała odeń artefaktyczną maść na sen wpadła w kłopot gdy owa maść zarezonowała z innym czarem. Siły Specjalne Hektora ją uratowały przed katastrofą. A Siły Specjalne zostały uratowane przed terminuską (Judytę) przez Mojrę - nothing like red tape. Zimna wojna między KADEMem a Świecą jest w mocy.

# Zasługi

* vic: Alina Bednarz jako niosąca zapalniczkę manipulatorka ściągająca halucynujące dziewczyny Marcelina z drzewa.
* vic: Klemens X jako ten, który nie padł trupem próbując ogłuszyć terminuskę na tarczach. Zrehabilitował się wyprowadzając Warczusia na spacer.

* mag: Marcelin Blakenbauer jako kochanek, który kręci z o jedną dziewczyną za dużo w nieodpowiednim momencie... i rozdaje niegroźne artefakty na bezsenność.
* mag: Margaret Blakenbauer jako dostarczycielka biologicznych rozwiązań problemów pochodzących od Blakenbauerów i dla odmiany pozytywna postać.
* mag: Mojra jako parawan ochronny ze strony SŚ dla Aliny i Klemensa. Zapewnia wiedzę, chroni oraz po* maga Beacie jak trzeba.
* czł: Beata Zakrojec jako aktualna dziewczyna Marcelina która w tajemniczych okolicznościach zniknęła i której nie po* maga prawidłowo maść Marcelina na koszmary.
* mag: Infernia Diakon jako "coś między Diakonami a Zajcewami", eks-kochanka Marcelina która go rzuciła... na krzesło. I gracze jej unikają jak ognia.
* mag: Judyta Karnisz jako terminus Srebrnej Świecy która przyszła rozwiązać problem z pająkołowcą w mieszkaniu Beaty Zakrojec.

# Lokalizacje:

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Obrzeża
                        1. Rezydencja Blakenbauerów
                    1. Dzielnica Owadów
                        1. mieszkanie Beaty Zakrojec
                1. Piróg Dolny
                    1. Lasek Pejzażowy
---
layout: inwazja-konspekt
title:  "Andrea węszy koło Szlachty"
campaign: powrot-karradraela
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [150819 - Krótki antyporadnik o sojuszach (HB, SD, Kx)](150819-krotki-antyporadnik-o-sojuszach.html)

### Chronologiczna

* [150819 - Krótki antyporadnik o sojuszach (HB, SD, Kx)](150819-krotki-antyporadnik-o-sojuszach.html)

### Logiczna

Poprzednie działania Andrei:
* [150718 - Splątane tropy: Spustoszenie? (AW)](150718-splatane-tropy-spustoszenie.html)


Misja, na której SIĘ CHRONOLOGICZNIE KOŃCZY ta konkretna misja (a dokładniej, kończy się w chwili spalenia cmentarza):
* [150823 - Atak na sanktuarium Estrelli (PT)](150823-atak-na-sanktuarium-estrelli.html)

Opowieści Judyty; mniej więcej w rejonie tych misji i tych wydarzeń:
* [150110 - Bezpieczna baza w Kotach (SD, PS)](150110-bezpieczna-baza-w-kotach.html)
* [150224 - Wojna domowa Spustoszenia (SD, PS)](150224-wojna-domowa-spustoszenia.html)

## Punkt zerowy:

Zgodnie z wydarzeniami poprzednimi, zadowolona Andrea widziała, jak na jej oczach Leokadia sformowała sojusz z Blakenbauerami. Nie udało się ich zniszczyć używając Gali Zajcew i Tymoteusza Mausa, ale cel jest osiągnięty - Leokadia sterowana przez Agresta (a dokładniej, o czym Andrea już nie wie, Saith Kameleon jako Tamara dała rozkaz Leokadii) wymusiła sojusz. Zdaniem Andrei może nie być to takie proste, ale Agrest powiedział, że ten problem już Andrei nie dotyczy.

Co więc Andrea ma robić dalej? Nadal nie wiadomo, dlaczego wydział wewnętrzny nic nie robi w sprawie KADEMu i Świecy; czemu w ogóle doszło do ataku Tamary na KADEM i co Szlachta (jeśli to oni) miała z tego mieć.

Ż: Co interesującego dla Andrei/Agresta może wiedzieć rodzina Tamary czego nie dowie się Ozydiusz?
K: Andrea może dowiedzieć się, jaka była treść ostatniej rozmowy Tamary z Salazarem. Salazar nie pamięta - oni mają ślad.

Niestety, nie było żadnych korzyści dla seirasa Mausa z akcji Tymek/Gala, tylko pewne straty materialne i czasu. Blakenbauerowie po prostu zadziałali w inny sposób niż ktokolwiek by się tego spodziewał; tyle dobrego, że Adela skatalogowała wszystkie działania i grupę specjalną (oraz przeczytała pamięć Patrycji Krowiowskiej i Szczepana Paczoła). Jeśli trzeba, Mausowie mają sposób nacisku na Blakenbauerów przez Maskaradę itp, ale jest to słaby środek.

Ż: Jaką nietypową (i potencjalnie korzystną dla Mausów) aktywność w świecie ludzi zgłosił seirasowi Oktawian Maus?
K: Oktawian dowiedział się o wyprawie archeologicznej dowodzonej przez Bankierza na terenie, w którym wykrył śladową obecność Proxy.
Ż: Co jest dziwnego w zgłoszeniu Oktawiana Mausa odnośnie tej aktywności co skłoniło seirasa do wysłania Ostrza Karradraela na akcję?
K: Jeśli przejmą to dyskretnie (Adela), to dostaną mnóstwo bardzo cennego materiału i danych historycznych. Jak nie, Bankierze to zniszczą nie rozumiejąc znaczenia.

Właśnie. Oktawian Maus. Mag, który jak na razie pojawia się w różnych miejscach i różnych okolicznościach, ale jeszcze "nic na niego nie ma". Bardzo lojalny swemu rodowi i bardzo lojalny seirasowi, jednak z jakiegoś tajemniczego powodu jest kojarzony ze Szlachtą? Czyżby był potencjalnym oknem Andrei w Szlachtę? Czy Andrea może go jakoś wykorzystać?

Co prowadzi nas do kolejnej zagadki: Salazar Bankierz. Odebrał sobie pamięć, ale wcześniej doprowadził do tego, że Tamara dostała się na poligon KADEMu. I był tam ktoś jeszcze. Ale Salazar sam nie będzie w stanie tego wyjaśnić - ma wymazaną pamięć.

W świetle powyższych informacji Andrea zajmuje się następującymi tematami:
- Andrea chce skupić się na temacie Tamary i poligonie.
- Andrea chce dowiedzieć się, o co chodzi z Oktawianem Mausem.

## Misja właściwa:
### Faza 1: Wtedy, gdy upada Leokadia

Andrea dowiedziała się, że Blakenbauerowie i Leokadia zawarli sojusz; Agrest zwolnił ją z tego zadania i przesunął ją na działania związane z Tamarą. Nadal obrona Tamary nie jest zmontowana. Na prośbę o spotkanie z Tamarą Agrest oczywiście się nie zgodził - Tamara jeszcze nie nadaje się do aktywnego działania.

Idąc za ciosem, Andrea skupiła się na potencjalnie najlepszym dla niej źródle informacji - Judyta Karnisz. Miała miejsce w większości akcji a jednak została przez większość osób pominięta. Tak jak Estrella, ale wkroczyła w innym momencie, później. Ona była w miejscu oczyszczania terenu ze Spustoszenia z Tamarą i Izą jako trzeci świadek. Andrea umówiła się z Judytą w Srebrnej Świecy; w Sali przesłuchań 4B (tam nikt ich nie podsłucha). Judyta, z obawą, ale się zgodziła; terminusi z niewiadomego powodu nie lubią być wzywani przez Wydział Wewnętrzny. 

Przed spotkaniem Andrea oczywiście przygotowała się na spotkanie; przeczytała jej akta i sprawdziła wszystko na jej temat. Już tu pojawiła się pierwsza ciekawostka - Judyta była kiedyś przestępcą, niebezpieczną czarodziejką uważaną za niereformowalną. Zaćmienie odebrało jej tymczasowo magię i Judyta trafiła do ludzkiej rodziny. Daleko poza jurysdykcją Świecy. Tam Judyta spotkała się z defilerem który zamordował tych, którzy się nią zaopiekowali i zabrał ją do siebie jako kuriozum, próbując przywrócić Judycie moc magiczną.
Judyta uśpiła jego czujność i zabiła go podczas rytuału, który miał przywrócić jej moc magiczną. Okazało się, że on stał się komponentem tego rytuału (ofiara), w wyniku czego Judyta odzyskała moc.
Czarodziejka skupiła się na pomocy ludziom i odpłaceniu temu nowemu światu za swoje poprzednie czyny. Przygarnął ją Ozydiusz Bankierz, który dał jej szansę mimo jej przeszłości.

Ż: Jakie przestępstwa popełniała Judyta, że była uznana za bardzo niebezpieczną przestępczynię przez magów?
K: Hitman. Zabija tak, by wszyscy myśleli, że to przypadek.

No ok, ale w takim razie - czemu ona tam była? To nie pasuje do jej profilu; Judyta nie jest terminuską pierwszoliniową, tylko drugoliniową.

W sali przesłuchań. Judyta czeka na pojawienie się Andrei, wyraźnie zdenerwowana. Andrea pojawiła się o czasie. Andrea zachowuje się bardziej jak naukowiec niż terminus. Andrea powiedziała, że chce pomocy Judyty a nie ją pognębić. Terminuska udając spokojną powiedziała, że jest do dyspozycji Andrei. Andrea powiedziała, że interesuje ją akcja w Kotach.

Judyta powiedziała, że wczesnym wieczorem skontaktowała się z nią Tamara i powiedziała, że potrzebna jej pomoc w walce ze Spustoszeniem. Czemu nie skontaktowała się normalnymi kanałami przez Świecę? Powiedziała, że Spustoszenie przekroczyło pewien próg infekcji ludzi i zgodnie z procedurami Świecy można już bezproblemowo zabijać ludzi. Tamara chciała tego uniknąć i dlatego poprosiła Judytę - Tamara wie, że Judyta nie lubi krzywdzić ludzi. Była ich trójka - Iza, Tamara i Judyta, w czym Tamara do akcji włączyła Kurtynę. Na miejscu było 9 magów otaczających cały teren, posiadających viciniusy do pomocy, min. Leokadia. Jednak do samych Kotów weszły tylko Tamara, Judyta i Iza. Tu Judyta się zawahała, Andrea jednak nacisnęła. Judyta powiedziała, że doszło do konfliktu przy rozmieszczaniu sił między Tamarą i Izą; Iza tak umieściłaby magów, że nie każdego maga ochraniałyby viciniusy. Tamara powiedziała, że bezpieczeństwo jest ważniejsze niż atak a nawet niż ochrona ludzi. Judyta odniosła wrażenie, że Iza wie coś więcej, ale nie zdążyły już na ten temat porozmawiać zanim doszło do oddania Izy Millennium. Oczywiście, stanęło na tym, że Tamara wygrała. Zgodnie z rozmową, to Iza wezwała Tamarę a Tamara zgromadziła Kurtynę oraz Judytę.

Andrea zapytała czy jest coś, co nie trafiło do raportu. Judyta potwierdziła. Poszerzyła dane o:
- Spustoszeni magowie to byli: Marian Welkrat (KADEM), Zenon Weiner (Świeca), Oktawian Maus (Świeca). Zenon i Marian zostali złapani przez KADEM sobie tylko znanymi metodami. Maus został znaleziony i odSpustoszony, ale później; miał częściową amnezję. Welkrat i Weiner, na stan wiedzy Judyty nie mieli tej amnezji - Tamara bardzo potem wypytywała.
- KADEM, gdy był w kościele, SMSował ze Spustoszeniem. Nie znają treści; nie miały tego typu sprzętu na akcji.
- KADEM coś robił w tym kościele; coś, co wpływało na Spustoszenie. Mieli coś dziwnego. Nie wie co, ale spodziewali się pewnych działań ze strony Spustoszenia, co jest bardzo... bardzo niemożliwe; to nie oni sprowadzili Spustoszenie.

I coś poza raportem:
- Zenon Weiner otarł się o śmierć kilka razy gdy był Spustoszony. Za każdym razem wyglądałoby to jak wypadek. Wyglądało to z punktu widzenia Judyty, jakby to Spustoszenie próbowało go zabić (ten tajemniczy kontroler próbował go zabić; nie kraloth). Celem był tylko Weiner; Marian Welkrat nie był celem Spustoszenia.
- Z pytań Andrei wynika, że nikt nie wiedział niczego o Wiktorze Sowińskim. Jedynie o Oktawianie z grupy Szlachty.
- Oktawian został uratowany później, bo zwyczajnie nikt nie umiał (nie próbował?) go znaleźć. Po prostu nie przeszkadzał a na Mausie nikomu nie zależało specjalnie; w czym Judyta zwyczajnie nie umiała go znaleźć. Zdaniem Judyty Maus musiał być na tym obszarze, ale z uwagi na brak zagrożeń i działania został zdobyty bez walki w kopcu miślęgów.
- Zdaniem Judyty, Iza wiedziała, skąd wzięło się Spustoszenie. I Tamara nie była tym zaskoczona. Dała kilka przykładów rozmów Izy i Tamary.
- Było więcej potencjalnej komunikacji. Na urządzeniach wykryta była komunikacja (typu broadcast) która wyglądała jak głębokie zakłócenia; coś technicznego. Pierwotnie uznano, że to jakaś forma działań KADEMu, ale KADEM był w kościele. Tamara odbierała to przez swój pancerz; chciała to dokładniej przeanalizować, ale mogła nie zdążyć. Gdy potem dotarły do bunkra (potencjalny start komunikacji), bunkier był pusty poza jakimś całkowicie zrujnowanym sprzętem.

Judyta nie wie, skąd się wzięli magowie KADEMu. Tamara ogólnie nie chciała z nimi współpracować. Ich postawa - aroganccy, ale skłonni do sojuszu na swoich warunkach. W świetle tych okoliczności Judyta zgłosiła się do Tamary jako oficer łącznikowy i Tamara wyraziła zgodę.

Andrea widzi, że Judyta wie coś jeszcze. Nacisnęła ją mocniej - Judyta pękła. Powiedziała, że Tamara poinformowała Ozydiusza o problemach i powiedziała, jakie działania ma zamiar przedsięwziąć. Ozydiusz dał jej zielone światło, pod warunkiem, że "on nie będzie o niczym wiedział". A potem Ozydiusz dowiedział się - przy magach KADEMu - jaki numer wykręciła mu Tamara...

Na odchodnym, Andrea powiedziała jeszcze Judycie, żeby ta niczego nie powiedziała Ozydiuszowi. Widząc, że Judyta najpewniej złamie obietnicę, powiedziała jej, że wie czym Judyta jest i jaką ma przeszłość. Jeżeli Judyta mu coś piśnie o tym spotkaniu, to wtedy ich oboje czeka coś złego - i Judytę i Ozydiusza. Andrea robi to nieco wbrew sobie, ale misja jest ważniejsza. Judyta zbladła. Wykona polecenie. Andrea zaznaczyła, że nie ma zamiaru tego używać... jeśli Judyta nie zrobi nic głupiego.
Na pewno Judyta nie zrobi nic głupiego.

Dobrze. To Judyta z głowy. Za to Andrea dowiedziała się, że pancerz Tamary miał opcje szersze niż się wydawało. I może to wykorzystać. Najpewniej pancerz jest w rękach Agresta i z nim właśnie skontaktowała się Andrea.
Andrea poprosiła Agresta o resztki pancerza Tamary. Agrest powiedział, że to niemożliwe; został zbyt ciężko uszkodzony; beyond repair. Gdy Andrea powiedziała mu, że chodzi o dane z tego pancerza, Agrest (używając wiedzy Saith Kameleon) powiedział, że Tamara ma bardzo silny backup w swoim domu. Nie powiedziała o tym nawet przełożonemu; nikt nie wie. Agrest poda Andrei klucz Tamary, ale tien Wilgacz musi się tam dostać. Uśmiechnął się złośliwie, że wystarczy zapukać i powiedzieć "Wydział Wewnętrzny".

Andrea poszła zatem do domu Tamary. Dom jest zapieczętowany przez Ozydiusza. Andrea się uśmiechnęła, wysłała wiązkę i weszła; pieczęcie ją wpuściły (nie odmawia się Wydziałowi Wewnętrznemu). W środku - jest tam trójka ludzi, którzy próbują zejść jej z drogi (po prostu gdzieś siedzą by nie przeszkadzać; zamknęli się (nie na klucz) w jednym z pokoi). W sypialni dla służby.
Andrea zapukała i weszła. Dwóch ludzi i dziecko. Andrea powiedziała, że chce by wyszli; kobieta spytała, czy mogą wyjść z pomieszczenia; "lord terminus" powiedział, że jak mag tu jest (w domu) a oni wyjdą, stanie się coś złego. Andrea westchnęła. Ozydiusz ich nastraszył. Powiedziała, że mogą wyjść - bo ona poprosiła. Ludzie błyskawicznie się zmyli.

Andrea zamknęła drzwi. Agrest powiedział Andrei co jest potrzebne - kropla krwi tej kobiety, kropla krwi tego mężczyzny, konkretne sukno znajdujące się w tym pomieszczeniu i wiązka energii (przesłał Andrei sygnaturę). Prosty rytuał czteroczęściowy. Andrea poszła, pobrała krew (ludzie się przestraszyli więc powiedziała, że nic im się nie stanie) i uruchomiła system backupowy.

Andrea przesłuchała ostatnią rozmowę Tamary na hipernecie. Tą z Salazarem.
Salazar skontaktował się z Tamarą pierwszy. Powiedział jej, że jej "wychowanka" zaatakowała Poligon KADEMu - on zdradził "wychowance" jak się tam dostać. Powiedział, że koło niego stoi jeden z NICH (zdaniem Andrei chodzi o Szlachtę). Zgodnie z ICH żądaniami, mówi o tym Tamarze - czy Tamara poleci na poligon uratować swoją wychowankę, czy pozwoli jej zostać schwytaną przez KADEM? Salazar zaśmiał się przez łzy - ani on nie ma wyboru, ani ona nie miała wyboru ani Tamara nie ma wyboru. Salazar też zasugerował Tamarze, że tam KADEM robi jakieś dziwne rzeczy które zrobią bardzo poważne szkody jej wychowance, ze Spustoszeniem. Teraz: Andrea wie, że NORMALNIE nie ma to znaczenia, ale w tym momencie, w tej sytuacji przy chorobie on Tamarze zasugerował to silnie.
I teraz Andrea wie, skąd u Tamary ta obsesja ze Spustoszonymi żabolodami...

Andrea przeszukała jeszcze kryształ; zgrała tą dziwną komunikację (faktycznie brzmiało jak zakłócenia; jednak jej terminusia natura nie pozwala na to, by potraktować to jak zakłócenia, zwłaszcza po słowach Tamary). Przeszukała też kryształ pod kątem "wychowanki" - znalazła coś bardzo ciekawego. Okazuje się, że Tamara Muszkiet oddała do Rodziny Świecy czarodziejkę która teraz jest znana jako Anna Kozak.

Owocna wizyta. Andrea poszła spać. Dla odmiany - w swoim łóżku. Tylko wcześniej jeszcze oddała szumy do analizy Agrestowi; niech się przyda.

### Faza 2: Wtedy, gdy nad squatem latają drony

Andrea ku swemu wielkiemu zdziwieniu dowiedziała się, że Blakenbauerowie jednocześnie poważnie uszkodzili swoją sojuszniczkę - Leokadię Myszeczkę - jak i zawarli z Kurtyną jeszcze ściślejszy sojusz. To coś nowego. Jeśli to świadomy plan, to jest to bardzo... nietypowy plan; zdaniem Agresta jest to typowy problem "każdy w rodzie robi coś innego". Wie to z dość pewnego źródła (Marcelin, czego Andrei nie ujawnił).

Andrea uzbroiła się w cierpliwość i w konsekwencję i zdecydowała się spotkać z najbardziej tajemniczą postacią w okolicy - z Oktawianem Mausem. A czemu cierpliwość i konsekwencję? Bo to cholera jedna jest.
Andrea poszła do seirasa Mausa i poprosiła go o zaaranżowanie rozmowy z Oktawianem. Oktawian nie jest magiem Świecy. A jeśli Oktawian jest tak lojalny rodowi, to bez błogosławieństwa seirasa Mausa nie powie nikomu ani słowa...
Oktawian Maus - jak powiedział seiras - jest lojalny rodowi, ale nie Świecy. Jego zdaniem ród jest zgubiony, jeśli Świeca będzie dalej tak działała. To jednak sprawia, że Andrea ma trochę szczęścia - zajmowała się pro-Mausową sprawą. Oktawian może być wobec niej pozytywnie nastawiony.
Seiras Maus powiedział też Andrei, że rozmawiał z Oktawianem; tak, Oktawian rozerwałby Świecę by tylko pomóc rodowi, ale tego raczej nie zrobił. Z tego prostego powodu, że seiras sobie tego nie życzył. Zapytany przez Andreę o Oktawiana i jego ew. pamięć czy dziwne zachowanie powiedział, że faktycznie Oktawian ostatnio się trochę nietypowo zachowywał; był radośniejszy, był bardziej pewny siebie. Ale to mu przeszło.

Andrea przeszukała dane z Wydziału Wewnętrznego pod kątem Szlachty. Tak, są dane na temat Oktawiana... ale są szczególnie zabezpieczone. To znaczy, że może się do nich dostać, ale dowiedzą się o tym osoby, które na danych Oktawiana postawiły lock. Andrea się ostrożnie wycofała. Nie chce wpakować się w kłopoty...
Andrea poprosiła więc Agresta o zajrzeniu w te informacje; kto założył watcha i co jest w danych Oktawiana. Agrest powiedział, że to dostanie dla Andrei; to niestety troszkę potrwa.

Mimo wszystko Andrea chce się spotkać z Oktawianem. Ostatnimi czasy mnóstwo magów "znika" lub "coś się z nimi dzieje". Lepiej, by Oktawian nie podzielił tego losu, zwłaszcza teraz.
Andrea ma duże nadzieje na wiedzę od Oktawiana. Czy wie coś o Szlachcie. O co chodzi z akcją w Kotach. Co się tam działo. Ma też nadzieję, że Oktawian został wykorzystany przez Szlachtę i przez nich porzucony.

Spotkanie z Oktawianem Mausem. Wieczór. Miejscowość nazywa się Trzeci Gród; Andrea dostała się tam Bramą i przejechała kawałek (dostarczono jej transport; samochód z szoferem). Trzeci gród podzielony jest na trzy pierścienie (zgodne z murami) - centralny, środkowy i zewnętrzny. W zewnętrznym pierścieniu, najmniej ważnym, mieszka właśnie Oktawian. I Andrea jest zaproszona do biblioteki w centralnym pierścieniu, tzw. "biblioteki zębowej" (kształt). Trzeci Gród nie jest miejscem, gdzie mieszkają tylko magowie i ich służba; jest tu całkiem sporo ludzi, przede wszystkim imigrantów. Oni się nikomu nie skarżą a też stanowią zabezpiecznie przed Illuminati.

Oktawian Maus wygląda, jakby całe życie siedział w tej bibliotece. Lekko podniszczony strój, ale wytworny i elegancki. Starszy czarodziej o oczach zmęczonych, ale pałających żarliwą wiarą. Andrea zobaczyła fanatyka.
Oktawian otworzył rozmowę zauważając, że Andrea mogła być Mausem. Czarodziejka Świecy się naprawdę zdziwiła; Oktawian wyjaśnił, że dwa pokolenia wstecz jej przodkini podjęła taką a nie inną decyzję, w wyniku czego jej syn nie został Mausem (czyli Andrea jako córka tego syna nie ma krwi Mausów). 
Oktawian poprosił Andreę o wstawienie się za Mileną Diakon. Nieszczęsna Diakonka została ciężko Skażona przez krew Mausów a akurat (on to wie jako badacz) krew Mausów i Diakonów jest wysoce niekompatybilna. Jego zdaniem, żeby pomóc Milenie, trzeba ją wpierw całkowicie transformować w Mausa, odczekać parę tygodni (czy miesięcy) aż jej wzór się ustabilizuje, po czym odwrócić proces jeszcze raz. Przedstawił Andrei dokumenty i dowody, że to zadziała; kiedyś już to działało i robiło się tak niejeden raz. Andrea spróbowała go przejrzeć, ale był jak maska. 

Zapytany, skąd wie o tej akcji i sytuacji, Oktawian powiedział, że jego sojusznicy w Świecy mu mówią różne interesujące rzeczy za usługi, które on im zapewnia. Widząc zdziwienie Andrei Oktawian zapytał, w której roli teraz jest Andrea - przyjaciółki Mausów czy wydziału wewnętrznego? Gdy Andrea powiedziała, że te role się nie kłócą, Oktawian powiedział, że on nie pomaga wydziałowi. Nie ma po co. 

Andrea skupiła się na przekonaniu Oktawiana, że tak naprawdę Oktawianowi i Andrei razem po drodze. Mausowie i Świeca to jedna siła. Najsilniejsi będą razem. Więc niech Oktawian - zwłaszcza za wsparciem seirasa - zacznie współpracować też i z Andreą; w końcu Srebrna Świeca i Mausowie będą silniejsi razem i nie po co ich rozbijać. Ku wielkiej radości Andrei, udało się. Oktawian powiedział, o co chodzi.

Andrea słysząc wyjaśnienia jest przerażona. Oktawian Maus jest badaczem, nie politykiem. Powiedział, że jest raczej wyżej niż niżej w hierarchii Szlachty (co Andrei nie zaskoczyło); on się między innymi opiekuje młodym Wiktorem Sowińskim i moderuje niewłaściwe zachowania Sowińskiego. Zdaniem Oktawiana Mausa, Srebrna Świeca jest silnie podzielona między rody magów, które skupiają się tylko na sobie i swoich interesach; nowa fala, czyli Szlachta to w rzeczywistości koalicja magów z różnych rodów, którzy pragną sanacji Świecy. Odbudowy Świecy. Silniejszej integracji gildii. I jest tam też miejsce dla Mausów. Zdaniem Oktawiana Szlachta jest przyszłością Świecy a on chce, by ród Maus stał po stronie zwycięzców.

Andrea uważa, że rewolucja pożera własne dzieci.

Oktawian powiedział też, że połowa działań Szlachty to pozory. Te wszystkie znęcania się nad Antygonami i Dracenami tego świata służą zdobywaniu nowych magów ze znaczących rodów Świecy (i mniej znaczących też!) oraz integrowanie ich. Aktualna Świeca jest skażona, niesprawna i skorumpowana. Aktualna Świeca dopuszcza znęcanie się nad młodymi magami przez innych magów. Dopuszcza, by Wiktor Sowiński chciał Antygonę Diakon jako swoje zwierzątko. A w wizji Świecy Oktawiana nie będzie takiej możliwości. Andrea tu zapytała, czemu zatem zgodził się na takie traktowanie Antygony? Oktawian powiedział, że Antygona mogła dołączyć do nich, do Szlachty. Ale zdradziła. Współpracowała z Draceną by ich zniszczyć. Gdyby sie odsunęła - nie byłoby problemu. Ale zdrajców może czekać tylko jeden los, zwłaszcza, że Antygona ma silny słaby punkt - rodziców w świecie ludzi, którymi Świeca się nie interesuje.

Andrea wie jedno. Seiras Maus nie może usłyszeć ani słowa. A Oktawian tak się myli, jak tylko można...

Kurtyna - zdaniem Oktawiana - idzie nie dość daleko. Magowie Kurtyny jak np. Tamara czy Iza mają serce we właściwym miejscu i on osobiście bardzo ubolewa, że takie rzeczy ich spotykają. Ale nie ma wyboru - Kurtyna chce grać zgodnie z zasami systemu, a system po prostu musi być zniszczony i odbudowany w nowej, lepszej formie. Szlachta nie atakowała pierwsza nigdy. Szlachta zawsze odpowiadała na atak, często z siłą nieadekwatną.

Andrea zastanawia się, kto do cholery go zindoktrynował. Bo Oktawian szczerze wierzy, że to najlepsza droga dla Mausów.

Andrea uderzyła w Oktawiana. Jej zdaniem Mausom i Świecy jest po drodze i jej zdaniem Oktawian jest w stanie przesunąć Mausów w stronę dla niego i nich wspólną. Więc - jak Oktawian widzi nową Świecę? Próbuje go podpuścić.
Oktawian się zapalił. Faktycznie, on widzi Świecę jako gildię uczciwą dla Mausów. Chce równych praw dostępu do magów w Rodzinie Świecy, chce równego wsparcia dla wszystkich znaczących rodów Świecy... nie ma tu nic, z czym Andrea się fundamentalnie nie zgadza. Lekko zmroziło ją, że zdaniem Oktawiana w Szlachcie są też magowie z innych rejonów niż tylko Śląsk. Jeszcze bardziej zmroziło ją, że Szlachta przekazała Oktawianowi grupę siedmiu magów do transformacji. Oktawian nie pytał skąd są. Czyli jest tu gdzieś grupa siedmiu magów przekształcanych w Mausów! Oktawian zauważył, że są ze strony Szlachty oczekiwania, że ci magowie po zmianie w Mausów będą bardziej skłonni do współpracy ze Szlachtą. Jego zdaniem - uczciwy układ. Andrei zdaniem - nie. Jeśli ktokolwiek się dowie o takiej akcji...

Czemu zatem Oktawian był przygaszony? Mag się zamyślił. Lekko zagubiony powiedział, że nie pamięta. I mówi prawdę. Coś zgubił, czegoś nie pamięta, coś było... ale uważa, że to nieważne. Zdaniem Andrei, ktoś go rozgrywa dla swoich potrzeb.
Leciutko zmanipulowany, Oktawian powiedział Andrei, że ci magowie są ukryci w miejscu poza jurysdykcją Srebrnej Świecy, w terenach zakazanych. Dzięki temu nikt nie będzie w stanie tego skojarzyć z Mausami od pierwszego spojrzenia i dzięki temu Świeca nie znajdzie tej lokalizacji. 

Andrea poprosiła Oktawiana o jedną rzecz. Jej zdaniem Srebrna Świeca i Mausowie mają po drodze i tak samo uważa Oktawian. Oktawian wybrał Szlachtę, Andrea reprezentuje starą Świecę. Szlachta dostarczyła Oktawianowi 7 magów, Andrea ma inną propozycję - jeszcze jej nie zdradza, ale nie chce, by Oktawian zrezygnował z jej starań. Jeśli Andrea wygra, Mausowie wygrywają. Jeśli Szlachta wygra, Mausowie też wygrywają. Niech więc Oktawian Maus po prostu poczeka.
Czarodziej się zgodził. Andrea faktycznie zawsze działała na korzyść Mausów.
Andrea zdecydowała się na tym na razie skończyć dyskusję. Nie chce ochłodzenia stosunków a ma o czym myśleć.

Agrest dostał pełen raport z rozmowy z Oktawianem Mausem wraz z informacją o działaniach Andrei i tym, że Andrei zależy na dobru rodu Maus i stabilności Srebrnej Świecy. Starszy terminus przyjął to do wiadomości.

Następnego dnia Andrea zdecydowała się spotkać z Anną Kozak. Wpierw sprawdziła dane na jej temat; to co ciekawe w tych danych to to, że Anna Kozak nie ma zielonego pojęcia o tym, że Tamara jest osobą, która ją uratowała i oddała do Rodziny Świecy. Nie ma też najpewniej wiedzy o tym, że była przeznaczona do rodu Maus co Tamara całkowicie zablokowała. Z uwagi na wojny i komplikacje prawne Anna Kozak nie trafiła do żadnego możnego rodu. Nie chciała wyzbyć się nazwiska i przeszłości, przez co nie ma łatwo w życiu. Ale dalej walczy. Nie wie o swojej przeszłości - tylko tyle, że była człowiekiem przed Zaćmieniem.

Andrea nie wie jak daleko sięgają łapy Szlachty. Zdecydowała się spotkać z Anną z zaskoczenia; gdzieś na boku w Kompleksie Świecy. Ale wpierw - Salazar Bankierz.

Oczywiście, Andrea zdecydowała się z nim spotkać w sali przesłuchań Świecy. Przejrzała jego akta uprzednio; wyszło na jaw, że mógł mieć coś wspólnego ze Spustoszeniem (to on "wydobył" dzięki Dracenie Spustoszenie z Powiewu; utajniony zapis Tamary do odczytu wydziału wewnętrznego po dead man's hand (a Tamara zniknęła, więc Dead Man's Hand odpaliło)).

Andrea jest zdziwiona. Skąd Tamara to wiedziała?

W sali przesłuchań czeka na Andreę nieco zestresowany Salazar. Andrea uśmiechnięta - na każdego ma jakiegoś haczyka. Salazar zestresowany - czemu wydział wewnętrzny interesuje się kandydatem na terminusa.
Andrea poszła za ciosem. Zapytała o Powiew, dążąc do informacji o Spustoszeniu. Salazar całkowicie niewinnie odpowiada. W końcu Andrea powiedziała, że na bazie jej wiedzy Salazar jest powiązany z wyniesieniem Spustoszenia z Powiewu. Salazar przerażony, nic o tym nie wie. Nie miał pojęcia, on nie wiedział.

Andrea zmusiła go do powiedzenia wszystkiego powiązanego z tym, co przeczytała w raporcie Tamary i co wie. Dowiedziała się, że w tamtym okresie spotykał się ze śliczną Diakonką (Draceną). Zdaniem swojej koleżanki, Anny Kozak, Dracena go do czegoś wykorzystywała bo "czemu taka laska miałaby się kimś takim interesować, on jest poza jej ligą". Zwłaszcza po tej sprawie z dziewczynami.
Salazar nie ukrywał - były dwie dziewczyny. Bliźniaczki. Ze świata ludzi. On i Marcel przygruchali je sobie; jednak Marcelowi jedna z nich buchnęła artefakt i zrobiła się niezła haja. Potem Tamara wysłała go na KADEM... Marcela znaczy się.
Salazar nie pamięta, jak ten artefakt został odzyskany; były jednak problemy, bo to były bliźniaczki i dwa razy skasowali pamięć tej samej. Znaczy: Salazar i Marcel ostro spieprzyli temat. Prawie doszło do masowego złamania Maskarady.
Na szczęście do dziś nikt nic nie wiedział.

Wow... Andrea w duchu facepalmowała.

Dracena ogólnie chciała, by Salazar często chodził do Powiewu, mówiąc mu o konkretnych książkach. Na bazie wiedzy Andrei, jeśli Salazar miał przy sobie jakieś Spustoszenie, mogło ono ulec infekcji przez Spustoszenie Powiewu. Czyli Dracena musiała Salazarowi podrzucić Spustoszenie.
A jedyne trzy osoby wiedzące coś na ten temat - Tamara, Iza, Dracena - wszystkie są niedostępne.

Zapytany o Annę Kozak Salazar się zamyślił. W sumie... nie pamięta. Anka nie jest ciekawą osobą, ona po prostu JEST. Taki podkochujący się w nim trochę wrzód na dupie, ale w sumie całkiem sympatyczna, choć nierozsądna. Też kandydatka na terminuskę.

Andrea zapytała Salazara czy wymazał sobie pamięć - jego zachowanie wskazuje na to, że tak (i to w kwestii ANNY). Salazar powiedział, że wymazał sobie pamięć - chodziło o atak na Poligon KADEMu, nie o Annę i wie, że Tamara była na Poligonie; powiedzenie tego magom KADEMu było wymogiem rozwiązania problemu poligonu - tyle pamięta. Czyli on powiedział KADEMowi o tym, że Tamara tam była.
Andreę lekko trafił szlag. Zdesperowany Salazar powiedział, że co miał zrobić? On (Salazar) wrobił Marcela w te bliźniaczki, przez niego Marcel wpadł w kłopoty, teraz Andrea imputuje mu Spustoszenie i jeszcze ta sprawa z Poligonem... to za dużo na niego. Oczywiście, że zwrócił się o pomoc do kogoś z rodu. I ten się tym zajął i kazał Salazarowi wszystko zapomnieć - łącznie z tym, kto to był. Salazar przysięga, że nie chciał krzywdy Ani i nie rozumie, czemu Ania Kozak jest z tym powiązana. To bez sensu.

Andrea ma już pewne pojęcie o sytuacji. Nacisnęła na niego tłumacząc mu delikatnie, że jest idiotą i pozwoliła mu odejść. Oczywiście, nikt nie może się o tym dowiedzieć. Salazar spytał Andreę, czy ta uważa, że ten członek rodu komu Salazar zaufał nadużył zaufanie Salazara oraz Świecy. Andrea powiedziała, że tak uważa. W końcu Wydział Wewnętrzny nic nie wie - a tymi tematami się Wydział Wewnętrzny zajmuje.

Następnego dnia Andrea obudziła się i czekała na nią wiadomość od Agresta.
Informacje, które przechwycił pancerz Tamary to była... kołysanka. Ze zmienionym tekstem. Agrest szuka czegoś na temat tej kołysanki, ale ta kołysanka jest popularnie śpiewana w świecie magów. Jedna z wariacji. It is not scary, nor sinister. Dlaczego jednak była przesłana tak silnie ukryta? Agrest dał radę już sprofilować obszary, gdzie ta kołysanka występuje normalnie; jeszcze zawęża i próbuje coś z tego mieć. Obawia się, że to jest hasło kontekstowe.
Andrea dała mu sugestię, by scrossował to z wersją ludzi. Agrest się zgodził; poszuka.

Agrest dowiedział się też (proszony przez Andreę), kto zablokował dostęp do danych Oktawiana. To aktualny przełożony Andrei - Aleksander Sowiński. Ciekawe. Andrea updatowała Agresta ze wszystkim, co wie.
Andrea zauważyła, że węszy i może być zauważona. Agrest powiedział jej, że jak co - może się wszystkiego wyprzeć.
Super. She's on her own.

Anna Kozak - Andrea przechwyciła ją gdzieś w parku.
...ona nie ma żadnych odruchów terminusa. ŻADNYCH...
Andrea się przedstawiła; Anna sprawdziła co to Wydział Wewnętrzny. Wygląda na bardzo przestraszoną; czyli coś zbroiła. Wykazuje jednak więcej ducha niż Salazar; Anna jest bezradna jak małe kocię, ale chce walczyć.

"Nikt nie jest niewinny; niektórzy tylko tego nie wiedzą" - Marian Agrest.

Andrea poszła w swoją mocną stronę - brutalna destrukcja osłon Anny. Andrea użyła banku danych Wydziału Wewnętrznego i wyciągnęła coś, co kiedyś Anna zbroiła; wykorzystanie surowców Świecy do pomocy ludziom (potencjalnie zagrażając Maskaradzie). Andrea powiedziała, że takie tematy jej nie interesują, ale w tej chwili Andrea zajmuje się dużo większą sprawą.
Anna pękła. 

Powiedziała Andrei, że Dominik Bankierz, lekarz magiczny, kazał jej iść na Poligon KADEMu. Nie miała wyboru; szantaż (wiedział coś o jej przewinach i powiedział, że ją zniszczy). Dominik obiecał Annie, że pozwoli jej zapomnieć wszystko o Szlachcie i pozwoli jej odejść wolno. I dotrzymał słowa. Ale ona - Anna - użyła Kryształu Pamięci i skopiowała swój stan aktualny. Nie mogła przesłać magom, więc wysłała - z instrukcjami - dziewczynom którym wcześniej pomogła.
Jak pomogła? Dwie siostry bliźniaczki zostały sprowadzone przez Marcela i Salazara do zabawy. Anna się wściekła i pomogła im używając tych samych kryształów, by dziewczyny NIE straciły pamięci. Powiedziała, że jest z "Men in Black". Dziewczyny nadal mają ten artefakt, ale go nie użyją - obiecały to Annie.

Gdy Dominik Bankierz się pojawił po tym, jak siostry spowodowały kłopoty Salazarowi i Marcelowi, Dominik powiedział, że Marcel i Salazar to głupcy. On to rozwiąże. Tamara odesłała jednak Marcela wcześniej na KADEM (nic nie wiedział) a Salazar tu został. Dominik pokazał Salazarowi Antygonę. Powiedział, że jeśli Salazar chce PRAWDZIWĄ zabawkę, to musi działać skutecznie, nie z jakimiś ludźmi. Anna słysząc to - potem - zaprotestowała i powiedziała Salazarowi, że jeśli on to zrobi, to ona go zniszczy. Nie wie jeszcze jak. Salazar powiedział, że odmówił Dominikowi; nie chce zabawki z maga.

Dominik zażądał od Salazara rozwiązania "pewnego problemu" i wtedy wysłał Annę na Poligon KADEMu.
Anna nic więcej nie wie. Ale ani Dominik ani Salazar - nikt - nie wie, że Anna cokolwiek pamięta.

Andreę jakby piorun strzelił. Dominik Bankierz to ten mag, który organizuje wyprawę archeologiczną w ramach historii Mausów. To przeciwko jego akcji została wysłana Adela Maus.

# Streszczenie

Andrea nadal zbiera dane o Szlachcie. Poznajemy historię Judyty Karnisz, dodatkowo Judyta naświetla to, że ona, Iza i Tamara chciały uniemożliwić zabijanie ludzi w Kotach (zgodnie z procedurami już można przy tym progu Spustoszenia). Zdaniem Judyty, Spustoszenie próbowało "przypadkiem" zabić Zenona Weinera; anomalią był też Oktawian Maus, który "zniknął" a jednak się znalazł w kopcu miślęgów. 
Magitech Tamary wykrył dodatkowe sygnały wyglądające jak zakłócenia (jakąś regionalną wersję kołysanki zgodnie z Agrestem) z bunkra w Kotach. I odkryła drugą osobę - Anna Kozak była oddana przez Tamarę do Rodziny Świecy dawno temu. I Szlachta wysłała Annę na poligon KADEMu i przez Salazara ostrzegła Tamarę (z Lodowym Pocałunkiem). Potem Andrea porozmawiała z Oktawianem, dowiedziała się, że Milena jest w fatalnej formie i że Oktawian jest fanatykiem rodu Maus. Oktawian powiedział, że jest ze Szlachtą by ratować Świecę i Mausów. Kurtyna, jakkolwiek bliska Oktawianowi, chce działać zgodnie z zasadami systemu - a system musi zostać zniszczony. Potem Salazar przyznał się Andrei, że wpadł z Marcelem w kłopoty z ludźmi i jakiś mag im pomógł i doprowadził do "tego wszystkiego". Dzięki Annie Kozak i jej machinacjom z ludźmi i artefaktami Andrea poznała tożsamość tamtego maga - to Dominik Bankierz. A jego działania śledzi inkwizytor Adela Maus. A Anna naraziła Maskaradę i Świecę.

# Zasługi

* mag: Andrea Wilgacz, zbierająca informacje o Szlachcie i Spustoszeniu używając uprawnień Wydziału Wewnętrznego.
* mag: Judyta Karnisz, służbistka jak cholera; na tej misji Andrea poznała jej historię. Częściowa ludzistka. Hitman; zabija tak, by wyglądało na wypadek. Dodała sporo do wydarzeń.
* mag: Ozydiusz Bankierz, który paranoicznie zorientował się, że Wydział Wewnętrzny siedzi mu na ogonie. Pokazał swoje dobre oblicze historycznie.
* mag: Infensa Diakon, wtedy: Izabela Łaniewska, wspomnienie z przeszłości; okazuje się, że na akcji w Kotach nie zgadzała się z Tamarą i robiła błędy taktyczne.
* mag: Tamara Muszkiet, wspomnienie z przeszłości; przechwyciła jakąś komunikację i nadpisała rozkazy Izy w Kotach.
* mag: Marian Agrest, koordynator Andrei i kopalnia informacji. Andrea nie liczyła, że jej pomoże i on jej nie rozczarował pozytywnie.
* mag: Oktawian Maus, fanatyk rodu Maus i wysoko postawiony * mag Szlachty (swoimi słowami), który powiedział Andrei czym jest Szlachta.
* mag: Ernest Maus, seiras. Współpracuje z Andreą i dostarczył jej kontakt do Oktawiana Mausa.
* mag: Adela Maus, która została wysłana na akcję archeologiczną Bankierzy by przechwycić artefakty Mausów niezauważenie.
* mag: Milena Diakon, o której Oktawian mówi, że chce jej pomóc; krew Diakonów i Mausów jest niekompatybilna.
* mag: Salazar Bankierz, którego cała ta sytuacja strasznie przerosła; zaczął od nadania Marcelowi bliźniaczki a skończył na usługiwaniu * magowi Bankierzy ze Szlachty. Stracił pełne zaufanie do rodu.
* czł: Gabriela Resort, śliczna złodziejka o lepkich palcach mająca siostrę bliźniaczkę (Irenę); Marcel wpadł przez nią w kłopoty.
* czł: Irena Resort, siostra bliźniaczka Gabrieli; współpracuje z Anią Kozak i wierzy, że Anna jest "Man in Black".
* mag: Anna Kozak, która okazała się być oddana do Rodziny Świecy przez Tamarę i która okazała się być tajemniczym atakujowcem KADEMowego poligonu. Naraziła Maskaradę dla ludzi. Nie chce być pionkiem. Zazdrosna o Salazara.
* mag: Dominik Bankierz, agent Szlachty i mastermind, który stał za zniszczeniem Tamary. * magiczny lekarz. Stoi za wyprawą archeologiczną (vs Adela).
* mag: Aleksander Sowiński, bezpośredni przełożony Andrei, który zablokował dostęp do danych Oktawiana. Podejrzenie: Szlachta.

# Lokalizacje:

1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński
                1. Kopalin
                    1. Centrum
                        1. Kompleks centralny Srebrnej Świecy
                            1. Pion dowodzenia Kopalinem
                                1. Sala przesłuchań 4B
                    1. Dzielnica Kwiatowa
                        1. Domek Tamary Muszkiet
                        1. Park Kwiatowy
        1. Mazowsze
            1. Powiat Kupalski
                1. Trzeci Gród
                    1. Centralny pierścień murów
                        1. Biblioteka zrębowa
                    1. Środkowy pierścień murów
                    1. Zewnętrzny pierścień murów
                        1. Chatka Oktawiana Mausa
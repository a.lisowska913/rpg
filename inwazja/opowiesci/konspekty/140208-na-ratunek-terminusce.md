---
layout: inwazja-konspekt
title:  "Na ratunek terminusce"
campaign: druga-inwazja
players: kić, dzióbek
gm: żółw
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [140201 - Ona zdradza, on zdradza(AW)](140201-ona-zdradza-on-zdradza.html)

### Chronologiczna

* [140201 - Ona zdradza, on zdradza(AW)](140201-ona-zdradza-on-zdradza.html)

## Misja właściwa:

1. Wacława wzywa do siebie Zdzisław Zajcew (brat Zofii Zajcew i Zuzanny Zajcew, rodzice lubili aliterację), jest sprawa rodzinna którą chce, żeby Wacław się zajął. Sprawa jest mocno polityczna i ma być załatwiona możliwie jak najdyskretniej. Tatiana Zajcew, córka Iriny Zajcew szkoli się na terminusa. Na egzaminie "ostatniej szansy" spotkało ją coś złego, ale tego jeszcze nikt (poza Zdzisławem i teraz Wacławem) nie wie. Informacje pochodzą od Iriny i Zdzisław nie zamierza wnikać w to, skąd matka - skądinąd jeden z legendarnych terminusów a teraz szkoleniowiec - wie takie rzeczy. Wacław ma rozwiązać problem.

Pikanterii politycznej dodaje to, że w rodzie Zajcewów są teraz trzy główne frakcje. Frakcja Współpracy, której ważnym magiem jest ojciec Tatiany (Benjamin Zajcew) i która jest zwolennikiem modelu współpracy i integracji Zajcewów ze Świecą i innymi magami. Frakcja Supremacji, która twierdzi, że np. tacy Bankierze nie są wcale lepsi od Zajcewów a przecież mają o tyle więcej wpływów i wsparcia w Świecy, przez co Zajcewowie muszą robić całą brudną robotę a Bankierze się bogacą. No i frakcja Trzecia (tak, tak się nazywa), kierowana przez Ewę Zajcew, córkę patriarchy, która twierdzi, że Zajcewowie powinni iść w ślady Millenium. Założyć własną gildię i luźno współpracować z innymi.

No i teraz - jeśli Tatiana nie zda na terminusa, czy wpadnie w jakieś problemy (zwłaszcza z przyczyn zewnętrznych) to od razu osłabia frakcję Współpracy. Właśnie tą frakcję, której kibicuje Zdzisław (i, poniekąd, Wacław). I dlatego właśnie najlepiej jakby to był Wacław. Drugi powód to to, że Wacław ma środki na to - a środki nazywają się "Andrea" - metody kontaktu ze Świecą tak, by sam Wacław miał ręce czyste.

TAK BARDZO ZAJCEWOWA SUBTELNOŚĆ!

2. Wacław wciąga do pracy nad problemem Andreę. Sprawa się drastycznie upraszcza gdy okazało się, że terminus szkolący Tatianę, niejaki tien Juliusz Szaman jest przyjacielem Andrei z czasów jak sami się szkolili (mieli wspólnego szkoleniowca; tien Wojciech Tecznia był bezwzględnym, lecz skutecznym i cierpliwym szkoleniowcem). Andrea udaje się do Juliusza żeby wyciągnąć od niego trochę informacji. Juliusz opisuje Tatianę i jej zespół jako przypadek w zasadzie beznadziejny. W zespole jest Tatiana Zajcew, Anna Kozak oraz Kaspian Bankierz. Anna robi maślane oczy do Kaspiana, Kaspian drwi z Tatiany, Tatiana jest w otwartym konflikcie z Kaspianem. Cała grupa nie potrafi ze sobą współpracować, utrzymują się na szkoleniu tylko dzięki naciskom politycznym. Aktualna akcja jest ich ostatnią szansą, żeby ją wykonać muszą współpracować, znaleźć i pokonać zmorojada. Zmorojad jest przyzwany i kontrolowany przez Szamana, nie zaatakuje niczego i nikogo innego niż ten zespół. Szaman odebrał zespołowi możliwość komunikacji poza wezwaniem pomocy w sytuacji krytycznej (co automatycznie oblewa grupę), ale zna ogólny obszar gdzie prowadzą akcję. Przed akcją dał każdemu z osobna różne wskazówki dotyczące tego z czym mają walczyć, dopiero połączenie ich wszystkich da im prawidłowe informacje, osobno wprowadzają w błąd.

tien Szaman poskarżył się Andrei, że cała ta sprawa ma silne naciski polityczne. Świecy zależy, by zarówno Tatianie się udało zdać (polityka i frakcja Współpracy) oraz by zdał młody Bankierz (to akurat krewny kogoś ważnego). Stąd, idąc zgodnie ze szkoleniem jakie tien Tecznia zastosował na min. nim i Andrei ten zmorojad. tien Szaman poprosił Andreę, by nie pomagała Tatianie zdać; jeśli ma oblać, niech obleje. I nie dziwi się, że młodzi wpadli w kłopoty. Zdziwiłby się, gdyby NIE wpadli.

3. Andrea i Wacław postanawiają przede wszystkim w pierwszej kolejności znaleźć Tatianę (min. dlatego, bo to ona ma kłopoty). Wskazówki które dostała (podejrzewają że nie Anna podała Kaspianowi swoją wskazówkę, ale nie wymienili się wskazówkami z Tatianą) sugerują upiora, więc poszukiwania zaczną od okolic cmentarzy. Andrea zasugerowałą poproszenie o pomoc Marcelina Blakenbauera, chce pożyczyć jakieś stworzenie tropiące, nie mantikorę, nie rzucające się w oczy i nie łamiące maskarady. Na szczęście niedawno Hektor poprosił Marcelina o przygotowanie dokładnie takiej istoty więc Marcelin ma ją już gotową. Nowy tropiciel ma na imię Ika i jest jedenastoletnią blondyneczką tropiącą.

Wacławowi i Andrei bardzo się to nie spodobało. Marcelin, przyciśnięty, powiedział, że nie wie skąd Ika się wzięła. Wziął ją z podziemi rezydencji Blakenbauerów; tam jest dużo interesujących rzeczy. Istnieje szansa, że to kiedyś był człowiek lub mag, tego nie jest w stanie wykluczyć...

4. Ika łapie trop przy jednym z cmentarzy po czym prowadzi Andreę i Wacława pod budynek z przestrzenią magazynową do wynajęcia. Tu trop się urywa a Tatiana wsiadła do jakiegoś pojazdu (no i koniec; Ika już nic nie wyłapie). Kamery monitoringu są wszystkie uszkodzone, więc na nagrania nie ma co liczyć, Andrea kojarzy że w tej okolicy często prowadzone są różne nie do końca legalne interesy. 

Wspólnymi siłami przesłuchują stróża, który zastraszony wyznaje że kojarzy Tatianę, gangsterzy w kogucich maskach wpakowali ją nieprzytomną do furgonetki i odjechali. Ika prowadzi do konkretnego magazynu w którym pojmali Tatianę, Wacław otwiera wytrychem zamek, Andrea przeprowadza analizę śladów. Po wyjątkowo udanym teście nie dość że wie jak wyglądałą sytuacja to jeszcze znajduje słaby ślad magii który sugeruje że Tatiana nie popełniła błędu i została przez kogoś wystawiona.

W skrócie Tatiana zakradła się do magazynu i kiedy chowała się za skrzyniami ktoś zasygnalizował jej obecność gangsterom (delikatny dotyk magiczny strącający puszkę z półki), jeden z bandziorów zaszedł Tatianę po cichu i obezwładnił przy użycu chloroformu. Niestety, nic więcej nie dało się wykryć ani zabezpieczyć śladów, bo w tym magazynie znajdował się też zmorojad. I ów zmorojad, wyczuwając zaklęcia pojawił się i zjadł resztki śladów magicznych. Moment później Andrea i Wacław wyczuwają że ktoś otwiera portal do środka, duży portal tak na 5-6 osób. Wacław przygotowuje Wielką Gębę na powitanie, a  Andrea przygotowuje małą pułapkę w miejscu gdzie pojawi się portal. Coś ostrego, przewracającego i powodującego bolesne obrażenia.

5. Z portalu wychodzą Anna Kozak i Kaspian Bankierz, oboje wywalają się na pułapce i tracą koncentrację, energia ich zaklęć rozprasza się, zmorojad rusza na żer. Wacław z ciężkim westchnięciem rozprasza przygotowaną Wielką Gębę zmorojadowi na żer i krótkim teleportem wyciąga wszystkich na korytarz. Po krótkim przesłuchaniu i zastraszeniu młodzi wyznają że od dwóch dni nie mają kontaktu z Tatianą i zaczęli jej szukać. Ania jest bliska tego żeby przerwać test i ściągnąć pomoc. Wacław i Andrea każą młodym pojawić się w Rzecznej Chacie za pół godziny a teraz mają zmykać. Ania rozpoznaje zmorojada kiedy ten kończy żerować i wychodzi na korytarz na żądanie drużyny; oboje z Kaspianem orientują się, jak dużo mieli szczęścia.

6. Wacław kontaktuje się ze Zdzisławem, prosi o sprawdzenie namiaru na gang który nosi kogucie maski. W międzyczasie idą do Rzecznej Chaty spotkać się z młodymi. Dodatkowe przesłuchanie nie wnosi zbyt wiele, ale młodzi ochłonęli i spokornieli, coraz bardziej doceniają w jakie wpadliby bagno gdyby nie Andrea i Wacław. Po krótkiej zjebce Andrea i Wacław wręczają młodym komórkę z zablokowaną możliwością wybierania innego numeru niż numer do Andrei i Wacława, po czym każą dojść do siebie, zrobić powtórkę z technik walki ze zmorojadem i na razie nie przerywać testu. Andrea i Wacław nie zdradzają że szukają konkretnie Tatiany, ale że wpadła w środek ich śledztwa jest spora szansa że ją znajdą, Wacław obiecuje skontaktować się do północy.

7. Zdzisław daje namiar na szefową gangu w kogucich maskach, to Bożena Zajcew, grunt to rodzinka. Pikanterii dodaje fakt, że Bożena Zajcew należy do frakcji... Współpracy. Wacław kontaktuje się z Bożeną i przekonuje ją żeby przekazała ostatnio pojmane na handel ludźmi dziewczyny, szczególnie tą nieplanowaną. Ostrzega Bożenę przy tym że wpakowała się w niemiłą sytuację, ale dla jej włąsnego dobra lepiej żeby nie wiedziała dokładnie o co chodzi. Bożena przyjmuje to ze zrozumieniem i dziękuje za załatwienie sprawy poprawnie, wewnątrz rodziny, po cichu. Na miejsce wymiany ustalają znany już magazyn. Do tego Bożena spytała, czy jest tam jakiś mag - okazuje się, że gang ludzi w kogucich maskach jest nauczony by każdej porwanej osobie zakładać "ball-gag", by nie mogła nic mówić. Tak bardzo unieszkodliwia to magów :-).

8. Andrea i Wacłąw przejmują dwie ludzkie dziewczyny oraz Tatianę od gangu. Wszystko odbywa się bez problemu i w kulturalnej atmosferze, w magazynie. Uprzednio przesłuchiwany stróż jest święcie przekonany, że natrafił na jakieś porachunki mafijne i bardzo próbuje nic nie wiedzieć.

9. Wacław usypia obie dziewczyny, po czym przewożą całą trójkę w bezpieczne miejsce (nie będące gabinetem Wacława i Andrei), gdzie uwalnia Tatianę. Informuje Tatianę że nie zawaliła jeszcze swojego testu i że jej zespół ją szukał, jak również uspokaja że nie mają pojęcia co ją spotkało. Prosi o kontakt z nimi zanim postanowią uruchomić sygnał ratunkowy. Dają Tatianie komórkę i numer do pozostałej dwójki. Tatiana jest zdziwiona tym że byli gotowi uruchomić sygnał ratunkowy (Wacław nie wspomina o tym że to Anna się tak deklarowała, nie Kaspian). Wacław studzi zapędy Tatiany na zemstę na gangu w kogucich maskach, informuje ją że pracują dla rodziny. Przed północą Wacław upewnia się że Tatiana przynajmniej poinformowałą pozostałą dwójkę o tym że jest w porządku. Oboje z Andreą używają wręczonych komórek żeby śledzić postępy testu. Po tym co się wydarzyło trójka młodych prawie-jak-terminusów zdecydowała się na współpracę i udaje im się zdać test ku rozpaczy Szamana.

10. Wacław przekazuje informacje o wykonaniu zadania Zdzisławowi, odbiera zapłatę i prosi o przekazanie szczegółów wpadki Tatiany Irinie w ramach ostrzeżenia. Dziewczyny przejęte od chłopaków Bożeny zostają umieszczone w szpitalach z wymazaną pamięcią, a lekarze na usługach Zajcewów fałszują kartotekę świadczącą o amnezji pourazowej (jedej wpisują wypadek drogowy, drugiej napad). Wszelkie powiązania dziewczyn z rodem Zajcew zostają ucięte.

11. Wacław i Andrea mają pewne wątpliwości co do całej sprawy. Lokalizacja zmorojada, zniknięcie śladów po manipulacji wrabiającej Tatianę, subtelność i precyzja tych działań... wszystko wskazuje na to, że Szaman mógłby mieć coś wspólnego z tą sprawą. Zwłaszcza, że tien Tecznia, którego tien Szaman darzył ogromnym szacunkiem zrezygnował z bycia terminusem właśnie dlatego, że przez naciski polityczne przepuszczano magów który nie nadawali się na bycie terminusami, w wyniku czego tien Tecznia stracił rodzinę. No, ale nie ma śladów, nie ma się czego złapać a drugim bardzo silnym śladem jest wewnętrzna dysputa Zajcewów i to, że chcieli konkretnie osłabić pozycję Iriny. To już jednak problem Iriny, na razie to musi zostać nierozwiązane...

![](140208_NaRatunekTerminusce.png)

# Zasługi

* mag: Wacław Zajcew jako mag któremu Zdzisław podrzucił dość ciężką i wredną misję odnalezienia Tatiany.
* mag: Andrea Wilgacz jako czarodziejka znająca kilka osób które, jak się okazuje, warto znać (min. Juliusza Szamana).
* mag: Zdzisław Zajcew jako dość zaufany agent Iriny Zajcew który wie, że Tatiana wpadła w kłopoty i z radością przekazał wieści Wacławowi.
* mag: Tatiana Zajcew jako uczennica na terminusa, która zniknęła i wpadła w koszmarne kłopoty (porwana), więc trzeba ją ratować.
* mag: Irina Zajcew jako matka Tatiany która ma niespodziewanie sporo wiedzy na temat lokalizacji córki.
* mag: Juliusz Szaman jako nauczyciel młodych terminusów który jest rozdarty między naciskami politycznymi a chęcią uwalenia niekompetentnych terminusów.
* mag: Wojciech Tecznia jako były nauczyciel Andrei i Juliusza Szamana; surowy i sprawiedliwy. Na tej misji jest wspominany.
* mag: Anna Kozak jako podkochująca się w Kaspianie kandydatka na terminuskę o wrogim do Tatiany stosunku.
* mag: Kaspian Bankierz jako kandydat na terminusa z magicznego rodu który uparcie próbuje odnaleźć Tatianę swoimi siłami.
* mag: Marcelin Blakenbauer jako twórca dziewczynki tropiącej który umożliwił Andrei jej wypożyczenie do znalezienia Tatiany.
* vic: Ika jako dziewczynka tropiąca (10*11 lat, blondyneczka) stworzona przez Marcelina na prośbę Hektora; tu: pomaga w znalezieniu Tatiany.
* mag: Bożena Zajcew jako czarodziejka frakcji Współpracy która zajmuje się handlem ludźmi i przypadkowo porwała... Tatianę.
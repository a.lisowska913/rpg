---
layout: default
categories: faction
title: "Blakenbauerowie"
---
# {{ page.title }}

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Aleksandria, krwawa Aleksandria](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html)|dostali mnóstwo ludzi i magów kiedyś sprzężonych z Aleksandrią|Powrót Karradraela|
|[Aleksandria, krwawa Aleksandria](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html)|dostali ogromny szacunek, respekt i chwałę za zniszczenie Aleksandrii i uratowanie tak wielu istnień|Powrót Karradraela|
|[Aleksandria, krwawa Aleksandria](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html)|stracili Ziarno Rezydencji|Powrót Karradraela|
|[Aleksandria, krwawa Aleksandria](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html)|zyskali Adriana Murarza|Powrót Karradraela|
|[Oblicze guwernantki](/rpg/inwazja/opowiesci/konspekty/160819-oblicze-guwernantki.html)|dostają Annę Myszeczkę|Powrót Karradraela|
|[Oblicze guwernantki](/rpg/inwazja/opowiesci/konspekty/160819-oblicze-guwernantki.html)|dostają Krwawokrąg, który może posłużyć jako prototyp do badań (lub do kontroli Anny Myszeczki)|Powrót Karradraela|
|[Oblicze guwernantki](/rpg/inwazja/opowiesci/konspekty/160819-oblicze-guwernantki.html)|dostają komputery The Governess gdzie znajdują się cenne informacje o jej ruchach i jej planach / działaniach|Powrót Karradraela|
|[Oblicze guwernantki](/rpg/inwazja/opowiesci/konspekty/160819-oblicze-guwernantki.html)|dostają ślady biologiczne Karoliny Maus|Powrót Karradraela|
|[Zaszczepić Adriana Murarza!](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html)|#5 magów "skazańców" do zbudowania oddziału Trzygław|Powrót Karradraela|
|[Zaszczepić Adriana Murarza!](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html)|dostają Vladlenę i grupkę Zajcewów, którzy budują broń masowego zniszczenia Spustoszenia (i nie tylko)|Powrót Karradraela|
|[Zaszczepić Adriana Murarza!](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html)|tracą Adriana Murarza (tymczasowo)|Powrót Karradraela|
|[Sleeper agent Oktawiana](/rpg/inwazja/opowiesci/konspekty/160803-sleeper-agent-oktawiana.html)|+ blueprinty oddziału szturmowego "Trzygław", sprzed Porozumień Radomskich|Powrót Karradraela|
|[Jak ukraść ze Świecy Zajcewów](/rpg/inwazja/opowiesci/konspekty/160713-ukrasc-ze-swiecy-zajcewow.html)|ocieplenie relacji z lojalistami Świecy, z Millennium, z Zajcewami Iriny.|Powrót Karradraela|


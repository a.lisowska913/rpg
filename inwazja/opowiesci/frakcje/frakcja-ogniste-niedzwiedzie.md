---
layout: default
categories: faction
title: "Ogniste Niedźwiedzie"
---
# {{ page.title }}

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Czyżby drugi kraloth?](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html)|better or worse, ale Bójka zaakceptowała tą frakcję jako swoją przez te przeklęte plasterki.|Adaptacja kralotyczna|
|[Czyżby drugi kraloth?](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html)|sprzęgło się z nimi dziwne różowe dildo, artefakt stworzony przez Silurię Paradoksem. Łączy Niedźwiedzie i Bójkę.|Adaptacja kralotyczna|
|[Czyżby drugi kraloth?](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html)|walka pomiędzy Filipem Czółno a Romanem Bruniewiczem o władzę.|Adaptacja kralotyczna|

## Plany

|Misja|Plan|Kampania|
|-----|------|------|
|[Pętla dookoła niekralotha](/rpg/inwazja/opowiesci/konspekty/180402-petla-dookola-niekralotha.html)|przeprowadzić działania przeciwko Klasztorowi Zrównania. Mały Terror Site.|Adaptacja kralotyczna|


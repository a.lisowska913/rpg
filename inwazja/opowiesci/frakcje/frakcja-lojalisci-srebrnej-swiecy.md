---
layout: default
categories: faction
title: "Lojaliści Srebrnej Świecy"
---
# {{ page.title }}

## Location

* Lojaliści są największą i najliczniejszą subfrakcją Srebrnej Świecy. Są wszechobecni w całej Polsce; na Fazie Daemonica dowodzą Świecą Daemonica.
* Nie występują na Terenach Utraconych (poza oddziałami do zadań specjalnych)

## Ideation
### Purpose:
(why are we here)

Dać szansę wszystkim magom. Chronić tą rzeczywistość. Chronić magów przed nimi samymi.

### Identity:
(who do we want to be)

* Wszechobecna gildia magów, masowa w stylu "no mage left behind".
* Alternatywne państwo polskie. Siła porządkowa, wspierająca i zabezpieczająca.
* Organizacja, w której wszyscy czują się bezpieczni i zapewniająca bezpieczeństwo światu.
* Gildia zapobiegająca tragediom oraz dająca wszystkim możliwość rozwoju i działania dla dobra wspólnego.
* Miejsce, w którym każdy mag z dobrymi intencjami znajdzie coś dla siebie.

### LongRange Intention:
(where do we need to go)

* Zapewnienie wszystkim magom wspólnego zbioru praw i zasad, by było wiadomo co i w jakim zakresie można.
* Ogromna siła ognia, polityczna i memetyczna - wszyscy muszą uznawać władzę i prawa Świecy. Świeca musi móc odeprzeć każde zagrożenie dla siebie lub dla Polski.
* Potężna infrastruktura wspierająca wszystkich magów dobrej woli - hipernet, portaliska itp.
* Świeca musi być postrzegana jako nieśmiertelna, wszechobecna oraz ogólnie pozytywna.
* Świeca musi kontrolować i monitorować większość środków magicznych Primusa, jak i dostęp do innych Faz.

## Nature
### Culture:
(how do we do things)

* Kultura silnie hierarchiczna, z mistrzami i uczniami. Premiowana jest lojalność i praca dla dobra wspólnego.
* Świeca jest skłonna negocjować i współpracować z innymi gildiami - w ramach praw, które są ustalone. Praworządność jest ogromną cnotą w Świecy.
* Podejście konserwatywne a nie ryzykowne. Istnieje autonomia dawana najbardziej lojalnym mistrzom, ale oczekuje się od nich odpowiedniego umiaru.
* Indywidualny geniusz jest doceniany, pod warunkiem, że jego wyniki są potem używalne. Podejście utylitarne. Częste tandemy - nieżyciowy geniusz + administrator umożliwiający mu życie.

### Structure:

* Każdy region ma swoje dywizjony kontrolowane przez Kompleksy Centralne. Dywizjony podlegają centrali w Warszawie i na Świecy Daemonica. Centrala nadaje kierunek całej Świecy.
* Lojaliści Świecy zapewniają i kontrolują kluczową infrastrukturę całego świata magów. Bez nich świat magów by się po prostu rozpadł.
* Terminusi Świecy są wydzieleni do specjalnej struktury, by nie podlegać pod dywizjon lokalny. Wszyscy terminusi podlegają centrali.

## Vision
(long-term intention into short/medium-term goals, metrics, and strategies)
### Goals

* Kontrola środków, monitorowanie magów, bazy danych o większości magach... - wiedzą wszystko o wszystkich i kontrolują wszystkie środki i zmiany magiczne.
* Zapewnianie platformy komunikacyjnej, ekonomicznej... wszystkich platform całego świata magów. W ten sposób mogą monitorować, zapobiegać i leczyć.
* Wczesna identyfikacja bytów paranormalnych i zapewnienie im odpowiednich działań - eliminacja, onboarding...
* Przynależność do Świecy jest dobrowolna, acz trzymanie się praw i zasad - nie. Terminusi Świecy mają pomagać wszystkim i robić to, co należy zgodnie z prawami i zasadami - a nie to, co jest w interesie Świecy. Każdy może ich wezwać na pomoc.
* Odpowiednia enkulturacja i zapewnianie pokoju między wielkimi rodami i ogólnie bytami. Zapobieganie wojnom, łamaniu Maskarady... ogólnie, pokój i prosperity.

## Strategy
(how do we achieve vision x nature)

* Budowanie, inwestowanie i przejmowanie kluczowej infrastruktury metodami pokojowymi i przedstawianiem korzyści
* Każdy mag w Świecy znajdzie coś dla siebie - dywizjon, cel, miejsce na świecie...
* Wydział Wewnętrzny mający za zadanie utrzymywać Świecę czystą i skuteczną. Terminusi Świecy mający utrzymywać bezpieczeństwo świata.
* Współpraca z innymi grupami i organizacjami celem dobra wspólnego i ogólnego prosperity.
* Lokalizowanie i osłabianie potencjalnie niebezpiecznych magów, frakcji i eksperymentów.
* Procedury i procesy - dzięki nim magowie wiedzą, co mają robić. Dostęp do ogromnej ilości surowców. Surowe karanie magów, którzy źle traktują innych magów, niezależnie od gildii.
* Twórcze używanie biurokracji, manipulacji, rozproszenie odpowiedzialności, mnóstwo dokumentów... jak chcesz zdobyć prawa do zrobienia czegoś "nie tak", zdążysz się zniechęcić. A "właściwa droga" jest prosta.

## Key Strengths

* 'peak genius level' niższy niż inne gildie, ale silne procesy pozwalają Świecy osiągać rzeczy niemożliwe dla innych gildii zwykłymi efektami skali i świetnymi procedurami.
* Możliwość użycia i marnotrawienia niewyobrażalnych wręcz zasobów.
* "There is a procedure for that" - mag Świecy zawsze wie, co należy zrobić i zwykle ma to spory sens.
* Kontrola kluczowej infrastruktury, dostęp do niezależnych i niezłych terminusów, archiwa i wiadomości o terenie...

## Key Weaknesses

* Pojedynczy mag Świecy czy grupka takich magów jest zwykle stosunkowo kiepska z perspektywy magów innych gildii. 
* Magowie Świecy niezbyt radzą sobie z adaptacją do całkowicie nietypowych sytuacji.
* W ostatnim czasie Lojaliści zostali ciężko uszkodzeni; mają dużo mniej środków i możliwości niż powinni. Acz jak na czymś im zależy, to to dostaną.

# Historia:

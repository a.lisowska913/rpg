---
layout: default
categories: faction
title: "Rodzina Dukata"
---
# {{ page.title }}

## Location

* Centrala zlokalizowana jest na Mazowszu, w Powiecie Pustulskim na Primusie.
* Silna obecność na Mazowszu z przyczyn historycznych.
* Macki Rodziny Dukata sięgają większości województw w Polsce, acz wszędzie słabo. Nie występują na innych Fazach.

## Ideation
### Purpose:
(why are we here)

Wyleczyć, czego nikt nigdy nie wyleczył.

### Identity:
(who do we want to be)

* Organizacja zdolna do prowadzenia dowolnego typu badań medycznych. Z tym wiąże się efekt skali.
* Organizacja uważana za pozytywną siłę w regionie - konieczne dla neutralizacji efektów ubocznych.
* Organizacja tak silna, by nie musiała podporządkowywać się Postanowieniom Radomskim.

### LongRange Intention:
(where do we need to go)

* Potężne ramię mafijne, zapewniające bezpieczeństwo i środki
* Zbiór rozproszonych ośrodków badawczych i dostęp do najwyższej klasy sprzętu i specjalistów
* Wpływ na politykę ludzi i na organizacje magów w okolicy, by być bezpiecznym i wiarygodnym
* Bardzo duże przychody i skarbiec, by móc prowadzić badania o niskim zwrocie
* Potężna machina medialna i propagandowa, by móc schować Maskaradę w Maskaradzie

## Nature
### Culture:
(how do we do things)

* Bardzo silna indoktrynacja pod kątem "zmieniamy świat na lepsze" i "cel uświęca środki".
* Skupienie na korzyściach długoterminowych kosztem cierpienia ludzi i magów, zwłaszcza poza Rodziną.
* Kultura oparta na lojalności w działach Finansów i Marketingu.
* Kultura celebrowania kultywacji i ewolucji w dziale Badań.
* Działy F i M traktują działy Badań protekcjonalnie i paternalistycznie.
* Dział Badań często nie jest świadomy mafijnych konotacji swoich sponsorów. Wierzy w wielkiego filantropa Dukata.

### Structure:

* Struktura jest silnie hierarchiczna i oparta na lojalności i wynikach - ale na poziomie samych niezależnych jednostek, każda jednostka jest silnie autonomiczna.
* Ramię mafijne to Dział Finansów. Ramię badawcze to Dział Badań. Ramię propagandowe to Dział Marketingu.
* Im wyżej w hierarchii tym ważniejsza jest lojalność. Im niżej, tym ważniejsze są wyniki (Dział Finansów i Marketingu) lub pomysłowe eksperymeny (Dział Badań).
* Dział Badań zupełnie jest odcinany od wiedzy o, czy jakichkolwiek działań Działu Finansów.
* Wszystkimi trzema działami dowodzi Dukat osobiście czy przez swoje simulacra.
* Struktura Rodziny Dukata nie jest bardzo wydajna - oficerowie liniowi (np. Sądeczny) mają bardzo duży poziom autonomii i często nie są odpowiednio kontrolowani i monitorowani.

## Vision
(long-term intention into short/medium-term goals, metrics, and strategies)
### Goals

* Zapewnienie stałych, dużych przychodów. Cashflow.
* Zapewnienie sobie pozytywnej opinii i wplątanie innych firm w Rodzinę Dukata.
* Pozyskiwanie najbardziej obiecujących specjalistów medycznych i firm medycznych.
* Dezinformacja i budowanie alternatywnego obiegu informacji.
* Sprawienie, by wszystkim opłacało się mieć Rodzinę Dukata na swoim terenie.
* "Żaden polityk nie będzie niezależny".

## Strategy
(how do we achieve vision x nature)

* Współpraca biznesowa z wielkimi firmami - dostarczanie im szybciej, taniej, lepiej. Z dużą marżą.
* Nie atakowanie firm w "swoim" rejonie i pod "swoją" ochroną. Acz blackops zewnętrzny dopuszczalny.
* Pomoc z wejściem na rynki poza swoją ochroną. Najlepszy program pomocowo-emerytalny.
* Monitorowanie, ochrona, wsparcie. Mafia w starym stylu włoskim.
* Bardzo silne nasilenie propagandowe i budowanie "kościoła Dukata". Zwalczanie innych propagand.
* Działanie jako alternatywne siły porządkowe. Rodzina Dukata płaci podatek i za ten podatek Dukat rozsądzi i pomoże. Nie stoi po stronie większego a tego co ma rację.

## Key Strengths

* Absolutne plausible deniability
* Bardzo duża lojalność ogólnej populacji
* Niewielkie, silnie enkulturowane grupy autonomiczne blackops
* Duże zasoby pieniędzy, magimedów, rzeczy medycznych i okołomedycznych
* Na każdy problem znajdzie się ktoś, kto wie jak je rozwiązać (Sądeczny, Wiaczesław)

## Key Weaknesses

* Absolutne plausible deniability
* Wrażliwość na ambitnego propagandzistę na wysokim poziomie hierarchii
* Struktura ma problemy z adaptacją do bardzo zmieniających się warunków (unknown unknowns)
* Ogromna wrażliwość na straty w ludziach i otwartą wojnę
* Niski poziom zastępowalności środków - mało standardyzacji
* Różni dyrektorzy podejmą różne decyzje; może dojść do konfliktów a jedynym ogniwem jest Dukat

# Historia:
## Plany

|Misja|Plan|Kampania|
|-----|------|------|
|[Chrumpokalipsa](/rpg/inwazja/opowiesci/konspekty/180112-chrumpokalipsa.html)|Ochronić 'tame medical kraloth' przed Srebrną Świecą - bo potencjalnie nasz!|Wizja Dukata|
|[Chrumpokalipsa](/rpg/inwazja/opowiesci/konspekty/180112-chrumpokalipsa.html)|Świeca spoza Mazowsza robi akcje na terenie Mazowsza. To trzeba zatrzymać i rozwalić.|Wizja Dukata|


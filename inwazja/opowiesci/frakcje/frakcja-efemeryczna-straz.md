---
layout: default
categories: faction
title: "Efemeryczna Straż"
---
# {{ page.title }}

## Location

* Efemeryczna Straż działa jedynie na Mazowszu. Jest tam wszechobecna.
* Centralna siedziba dowodzenia Efemerycznej Straży znajduje się w powiecie Kupalskim na Mazowszu.

## Ideation
### Purpose:
(why are we here) 

Chronić region należący do Bankierzy od zawsze. Pozwolić tajemnicom spać, rozładowywać artefakty i zabezpieczać efemerydy. Konserwować, utrzymywać piękno i chronić. Pozytywny konserwatyzm.

### Identity:
(who do we want to be)

* Strażnicy naturalnego balansu regionu - magicznie, pryzmatycznie, ekonomicznie, ludzko...
* Organizacja zajmująca się zrównoważonym rozwojem regionu w rozumieniu filozofii utylitarnej
* Organizacja paramilitarna oddana Sprawie, działająca w harmonii z naturalnym porządkiem

### LongRange Intention:
(where do we need to go)

* Organizacja powszechnie uznana za dominujące siły terminuskie na terenie Mazowsza.
* Organizacja zarządzająca wiedzą o przeszłości i umiejąca ją wykorzystać by pilnować balansu.
* Silne ramię militarne na poziomie magii oraz fizycznym do ochrony i zapewnienia że nic się nie wyrwie.
* Ramię społeczników i grup politycznych dbających o zrównoważony rozwój i właściwy Pryzmat.
* Wpływ na dominujące religie i przekonania, by budować właściwy Pryzmat w tym miejscu.

## Nature
### Culture:
(how do we do things)

* Kultura bardzo zadaniowa, wojskowa. Nacisk na kompetencje i wyniki. Premiowanie silnej autonomii.
* Podejście proaktywne, nie reaktywne. Raczej z ukrycia niż jawnie. Raczej jawnie, niż starcie.
* Awans i znaczenie są pochodną osiągniętych sukcesów, nie przeszłych dokonań, lojalności czy urodzenia. "Pierwszy spośród wilków".
* Próba zrobienia kultury współpracy, ale raczej powstaje kultura rywalizacji przy zachowaniu wspólnych surowców.
* Silna indoktrynacja pod kątem znaczenia regionu, przeszłości i harmonii.

### Structure:

* Struktura hierarchiczna na wyższych poziomach, na niższych niezależni agenci wykorzystujący współdzielone zasoby (struktura projektowa).
* Wykorzystaniem zasobów współdzielonych - konfliktów o te zasoby - zajmuje się Rada Starszych; pięciu najbardziej zasłużonych członków Straży, wybieranych za dokonania.
* Raz na 3 lata dochodzi do reewaluacji Rady Starszych - czy prawidłowo wykorzystywali swoje surowce, podjęli właściwe decyzje, czy ktoś inny nie zasłużył...
* Każdy agent posiada jakieś własne surowce. Nie tak często wykorzystywane są zasoby współdzielone, acz zawsze wiedza jest współdzielona (za brak podzielenia się wiedzą - surowe kary)
* Rada Starszych odpowiada za propagandę, agregację wiedzy, polityków, społeczników itp. Każdemu pomoże i odpowiednio podzieli zasoby. Rada odpowiada za "Straż".
* Agenci odpowiadają za lokalizowanie, zgłaszanie i neutralizowanie zagrożeń dla harmonii zgodnie z priorytetami i potrzebami. Też za wzmacnianie Straży.

## Vision
(long-term intention into short/medium-term goals, metrics, and strategies)
### Goals

* Rozpowszechnić i wypromować Mazowiecką Konserwację - "alternatywną religię" patriotyzmu lokalnego. Wśród wpływowych wyborców i polityków, ku Pryzmatowi...
* Mapować i zabezpieczać informacje odnośnie rzeczy dziejących się w okolicy i monitorować te, które mogą zakłócić balansowi.
* Pełnić rolę terminusów na Mazowszu. Być węzłem informacyjnym, pomagać innym magom i zwalczaczom problemów. I ciągle ewangelizować magów.
* Udowadniać wszystkim - łącznie z przeciwnikami - że są bardzo przydatną siłą i warto ich wspierać mimo tymczasowych problemów.
* Uzyskać MAD (Mutually Assured Destruction). Nie używać siły jeśli to nie jest niezbędne. Nie bać się pokazywać posiadanej siły.
* Pozyskiwać jak najwięcej agentów, spychać jak największą konieczność posiadania surowców na nich.

## Strategy
(how do we achieve vision x nature)

* Działania kontratakujące na działania innych frakcji mających zakłócać balans.
* Szkoły survivalowe i paramilitarne, które jednocześnie pełnią rolę indoktrynacją.
* Umożliwienie skutecznym osobom awansowanie niezależnie od tego, jak dawno temu dołączyły. Okrutne traktowanie zdrajców i apostatów.
* Wspólny "radiowęzeł" informacyjny zawierający info odnośnie potrzebnych rzeczy - gdzie są problemy, kto co zrobił, dane o terenie...
* Wzmacnianie Mazowieckiej Konserwacji i wszystkich polityków / członków / kapłanów i ich absolutne faworyzowanie - niech się rozprzestrzenia.
* Wzmacnianie i przejmowanie wszelkich sił militarnych i bojowych. Kupowanie sukcesów. Bounty na określone zadania.

## Key Strengths

* Oddziały paramilitarne zwłaszcza świata ludzkiego - Grupy Ochrony Mazowsza.
* Ogromna wiedza o przeszłości i samym Mazowszu. Znajomość terenu, legend, efemeryd...
* Bardzo dobra opinia w terenie, zwłaszcza z uwagi na szeroką pomoc, cele i działania terminuskie. Też: wsparcie biznesu lokalnego i lokalnych istot. 
* Naturalna przewaga memetyczna i pryzmatyczna; większość istot zna kogoś komu pomogli i mało komu zaszkodzili.

## Key Weaknesses

* Bardzo przewidywalni przez tendencje do konserwacji regionu (nie niszczą, Pryzmat...) i bardzo łatwo zmusić ich do określonych działań.
* Agenci rzadko współpracują ze sobą; rywalizują, więc łatwo niezbyt znaczącą akcją skłócić ich przeciwko sobie.
* Niemożność sensownego wzbogacenia się przez konieczność konserwacji. Większość "ostrych" akcji biznesowych jest im mało dostępna.
* Nielubiani przez dominujące ruchy propagandowe/religijne z uwagi na "alternatywną mazowiecką religię".

# Historia:

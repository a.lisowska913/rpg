---
layout: default
categories: faction
title: "Senesgradzki Chaos"
---
# {{ page.title }}

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Chrumpokalipsa](/rpg/inwazja/opowiesci/konspekty/180112-chrumpokalipsa.html)|utracił siły Esuriit w Półdarze.|Wizja Dukata|
|[Wspaniały Wieprz Wojtka](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|pozyskał Golema Ogrodniczego Czarnotrufli, napędzanego energią Esuriit.|Wizja Dukata|
|[Wspaniały Wieprz Wojtka](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|utracił lekko zmutowanego oczkodzika w okolicach Półdary|Wizja Dukata|
|[Odzyskana władza Pauliny](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html)|Wylęgarnia świń zdecydowanie wzrosła w mocy przez działania klątwożytów|Wizja Dukata|
|[Odzyskana władza Pauliny](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html)|pojawia się straszliwa świnia dużej mocy w okolicy, po działaniach w magitrowni i wyłączeniu klątwożytów|Wizja Dukata|
|[Odzyskana władza Pauliny](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html)|traci dostęp do mutujących klątwożytów i wolno wędrujących świń w Powiecie Pustulskim|Wizja Dukata|
|[Esuriit w Półdarze](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|ma dostęp do Słoika Esuriit, o którym nadal nie wiemy co robi|Wizja Dukata|
|[Esuriit w Półdarze](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|zmutowany oczkodzik na wolności nadal poszukuje... czegoś. Jest wolny i unika terminusów bez problemu.|Wizja Dukata|

## Plany

|Misja|Plan|Kampania|
|-----|------|------|
|[Wspaniały Wieprz Wojtka](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|Używając Golema Ogrodniczego Czarnotrufli, Skazić jak najwięcej spod ziemi.|Wizja Dukata|
|[Esuriit w Półdarze](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|nie wiem gdzie, nie wiem czemu i nie wiem kiedy. Ale ten Słoik Esuriit gdzieś kiedyś się pojawi by coś się stało.|Wizja Dukata|


---
layout: default
categories: faction
title: "KADEM"
---
# {{ page.title }}

## Location

* KADEM znajduje się w dwóch miejscach: 
    * na Primusie znajduje się na Śląsku z centralą i głównym centrum wpływów w Kopalinie.
    * na Fazie Daemonica znajduje się w Mare Vortex, w Zamku As'caen. Tam jest ich prawdziwa centrala dowodzenia.
* KADEM można spotkać wszędzie tam, gdzie nikt normalny nie odważy się poleźć, acz rzadko.

## Ideation
### Purpose:
(why are we here)

Eksploracja, odkrywanie, innowacje, ochrona. Przesuwanie granicy pojmowania rzeczywistości coraz dalej.

### Identity:
(who do we want to be)

* Niewielka, elitarna gildia zlokalizowana na Śląsku, acz sławni na cały świat.
* Obrońcy magów i dobra świata. KADEM wchodzi na forpocztę walki z siłami ognia przekraczającymi to, czego można się spodziewać.
* Działanie z efektem skali celem jak najlepszego życia magów i bez krzywdzenia bytów świadomych - używanie narzędzi, simulacr, wszystkiego, by magowie mogli żyć jak najlepiej i jak najwydajniej.
* Gildia łącząca co najlepsze w świecie magów z efektami skali najlepszych narzędzi. Najwydajniejsi i wszechobecni, eksplorujący i odkrywający.
* Osoby skłonne pomóc, gdy już nikt się nie odważy. Osoby, które gdy zrobią błąd to go posprzątają. Magowie, którzy nie zapomnieli, że mają odpowiedzialność wobec świata, z którego pochodzą.

### LongRange Intention:
(where do we need to go)

* Eksploracja i badania magiczne bywają morderczo niebezpieczne. Magowie KADEMu muszą być bardzo samodzielni i zdolni do samoobrony - też wyprzedzającej.
* Magowie KADEMu muszą potrafić delegować do simulacr, muszą być bardzo autonomiczni i muszą umieć podejmować trudne decyzje.
* KADEM musi mieć bezpieczne przyczółki i stacje badawcze we wszystkich miejscach i fazach. Musi monitorować potencjalne niebezpieczeństwa i być w stanie na nie odpowiedzieć gdy tylko przyjdzie taka konieczność.
* Jako pierwsza siła obrony i badań, KADEM potrzebuje różnorodnych magów o ogromych talentach, łączących co najlepsze ze wszystkich rodów. 
* KADEM musi mieć ogromną mobilną siłę ognia. Musi móc szybko reagować na zagrożenia i przemieszczać się tam, gdzie jest potrzebny w danym momencie.
* Systemy badawcze KADEMu i jego ruchome stacje muszą być nie tylko najlepsze na Śląsku, ale i na Primusie.

## Nature
### Culture:
(how do we do things)

* Pochwała kompetencji i kultywacji do poziomu arogancji i skrajnego indywidualizmu. Kult wiedzy. Kult pomysłowości i innowacji.
* Bardzo silna kultura pro-rywalizacyjna i pro-konfliktowa. Przy dodatkowej silnej presji zewnętrznej magowie KADEMu są dość lojalni wobec siebie, acz agresywni i ciągle rywalizujący.
* Magowie nie-KADEMu są uważani za gorszych. W końcu nie dostali się na KADEM lub nawet nie aplikowali.
* Bardzo silna promocja podejścia utylitarnego i wojskowego. Wszystko może być bronią, wszystko powinno się przydać. 
* Efektowność i kult legend także są promowane. Czyny a nie przeszłe zasługi. 
* Jeśli coś jest warte zrobienia, znajdzie się mag KADEMu który to zrobi. Często przez to ważne rzeczy (łańcuch dostaw?) będzie niezabezpieczony, czy zajmie się tym Andżelika lub simulacra.

### Structure:

* Struktura macierzowa z dominacją kierowników funkcji nad kierowników projektów. Każdy mag KADEMu podlega pod konkretny Instytut i ma za zadanie maksymalizować skuteczność tego Instytutu. Jednocześnie podlega pod określone projekty celowe.
* Każdy agent KADEMu ma wysoki próg autonomii. Rozliczani są za wyniki w formie dostarczania wiedzy, podejmowanie się trudnych wyzwań i działania w projektach i funkcjach. Też rozliczani są z przyrostu swojej wiedzy.
* KADEM udostępnia swoim magom odpowiednie miejsca w przestrzeni zamkniętej na Fazie Daemonica. Udostępnia też miejsce na Primusie. KADEM zachęca magów do współpracy z innymi gildiami i do nawiązywania kontaktów - jak długo pamięta się, kto tu jest najlepszy.
* KADEM składa się z głównych Instytutów: Zniszczenia, Pryzmaturgii, Technomancji, Transorganiki i Multiwersum. Składa się też z głównych Działów: Administracji, Zasobów, Dyplomacji, Eksploracji, Badań, Pozyskania, Utrzymania i Eliminacji. Działy zapewniają funkcje, które należy spełnić by KADEM działał. Instytuty - narzędzia, jakimi można osiągnąć określone funkcje. Na to wszystko pojawiają się projekty poszczególnych magów. Trójwymiarowa macierz.

## Vision
(long-term intention into short/medium-term goals, metrics, and strategies)
### Goals

* Przekraczanie znanej wiedzy we wszelkich obszarach wiedzy magitechnologicznej, własnymi siłami.
* Dostarczenie magom jak największej ilości dedykowanych i specjalistycznych narzędzi by jak najszybciej i najefektywniej zapewnić wsparcie. Też dotyczy simulacr, golemów i ogólnie rozumianej świty.
* Rozwiązywanie najtrudniejszych problemów i bycie na ciągłej forpoczcie technologii i rzeczy trudnych.
* Zapewnienie ruchomych stacji badawczych i militarnych takich jak Zamek As'caen. Ciągły wyścig zbrojeń KADEM sam ze sobą.

## Strategy
(how do we achieve vision x nature)

* Not Invented Here. KADEM nie wykorzystuje istniejących rzeczy bez modyfikacji. Wszystko trzeba zmienić, zmodyfikować, dostosować...
* Ogromna ilość simulacr, golemów, narzędzi na pojedynczego agenta KADEMu. Każdy mag KADEMu ma dostęp do świty wsparcia i efektów skali.
* Współpraca z organizacjami celem znalezienia i rozwiązania ich najtrudniejszych problemów sposobami KADEMu.
* Utrzymywanie potężnych magazynów, źródeł energii itp. - przez finansowanie własne (sprzedaż tego, co odkryją), rozwiązywanie cudzych problemów i działania w wielu Fazach naraz.
* Promowanie "KADEM sam potrafi rozwiązać wszystkie problemy" tak zewnętrznie jak i wewnętrznie.

## Key Strengths

* Absolutna przewaga magitechnologiczna, szczególnie w technomancji i pryzmacie.
* Prototypy, genialne wynalazki, szybka adaptacja i innowacje w każdym calu. Magowie KADEMu są wybitni, genialni i ciekawscy - i mają magazyny sprzętu.
* Skakanie między fazami, ze szczególnym uwzględnieniem Primusa i Fazy Daemonica.
* Zdecydowana przewaga na Fazie Daemonica, jako główna siła w tamtych obszarach.
* Agregacja wiedzy Wektora Sigma i biblioteki KADEMu są legendarne. Badania, analizy, wnioskowania.
* Mag KADEMu to jest one-mage army.

## Key Weaknesses

* Kultura wysokiej niezależności, indywidualizmu, arogancji i pokładanie wiary w swe siły i magitechnologię - podatni na attrition, izolację i znudzenie.
* Tendencje do używania rzeczy wysoko eksperymentalnych oraz mających niefortunne efekty uboczne.
* Problemy z finansowaniem, logistyką, polityką i "zwykłymi" rzeczami. KADEM zbyt polega na jednostkach i superhigh tech, za mało na prostych, skutecznych rozwiązaniach i zapewnieniu dostaw.
* Tendencja do overengineering, formy nad treść, udziwniania i robienia rzeczy efektownych a nie efektywnych.
* Gildia nieliczna i elitarna - w zarówno pozytywnym jak i negatywnym tego słowa znaczeniu.
* KADEM często nie do końca kontroluje kto za co i kiedy odpowiada. Nie ma silnego aspektu security i utrzymania w porównaniu z robieniem nowych rzeczy.

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Kraloth w piwnicy](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|dostęp do biosyntezatora kralotycznego (kiedyś: Hralglanatha), normalnie w rękach Silurii Diakon|Adaptacja kralotyczna|
|[Kraloth w piwnicy](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|dostęp do wszystkich ofiar kralotha (Hralglanatha).|Adaptacja kralotyczna|
|[Renata Souris i echo Urbanka...](/rpg/inwazja/opowiesci/konspekty/170222-renata-souris-i-echo-urbanka.html)|dostał efemerydę Kuby Urbanka, który strzeże zdrowia i życia Renaty Souris.|Powrót Karradraela|
|[Lizanie ran na KADEMie](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html)|potrafi fabrykować szczepionkę Esuriit odcinającą Mausów od Karradraela na pewien czas|Powrót Karradraela|
|[Sprowadzenie Mare Vortex](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html)|przechwycił Archiwum Ekspedycji Esuriit.|Powrót Karradraela|
|[Sprowadzenie Mare Vortex](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html)|przechwycił dziwny artefakt z Esuriit składający się ze szczęki Coś Robiącej Na Esuriit.|Powrót Karradraela|
|[Szmuglowanie artefaktów](/rpg/inwazja/opowiesci/konspekty/161005-szmuglowanie-artefaktow.html)|ma sieć przemytniczą z Jewgenijem Zajcewem, na wschód|Powrót Karradraela|
|[Szmuglowanie artefaktów](/rpg/inwazja/opowiesci/konspekty/161005-szmuglowanie-artefaktow.html)|na wysokim poziomie (szefostwo) ma sieć quasi-legalnego przekazywania artefaktów ze Świecą w Kopalinie... poza Ozydiuszem.|Powrót Karradraela|
|[Ratując syrenopnącze](/rpg/inwazja/opowiesci/konspekty/170417-ratujac-syrenopnacze.html)|ma dostęp do syrenopnącza; zostało uratowane|Powrót Karradraela|
|[Samotna w świecie magow](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html)|dostał oficjalne przeprosiny; nastroje na KADEMie się poprawiły|Powrót Karradraela|


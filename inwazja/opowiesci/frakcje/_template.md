---
layout: default
title: "TEMPLATE"
---

# {{ page.title }}

## Location

* 

## Ideation
### Purpose:
(why are we here)



### Identity:
(who do we want to be)

* 

### LongRange Intention:
(where do we need to go)

* 

## Nature
### Culture:
(how do we do things)

* 

### Structure:

* 

## Vision
(long-term intention into short/medium-term goals, metrics, and strategies)
### Goals

* 

## Strategy
(how do we achieve vision x nature)

* 

## Key Strengths

* 

## Key Weaknesses

* 

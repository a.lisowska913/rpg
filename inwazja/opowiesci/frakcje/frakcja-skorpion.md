---
layout: default
categories: faction
title: "Skorpion"
---
# {{ page.title }}

## Location

* Skorpion działa na terenie Polski. Główne stacje nadawcze znajdują się na obszarach utraconych innych krajów niż Polska.
* Skorpion ma też szczególnie silną obecność w miejscach, gdzie dominuje władza Iliusitiusa.

## Ideation
### Purpose:
(why are we here)

Chronić ludzi przed paranormalnym. Odzyskać świat od 'dziwactw'.

### Identity:
(who do we want to be)

* Siła ogólnopolska zwalczająca i neutralizująca wpływy paranormalne, niezależna od rządów i innych organizacji.
* Siła współpracująca z innymi sobie podobnymi siłami na całym świecie.
* Siła niezniszczalna, wykorzystująca działania paranormalne tam gdzie to konieczne, acz preferująca to, co rozumie.
* Siła, której boją się magowie i byty paranormalne - trzymająca ich w szachu. Siła ratująca i chroniąca ludzi.
* Skorpion. Dyskretny, morderczy, nieunikniony i budzący strach wśród bytów paranormalnych pełnych występku.

### LongRange Intention:
(where do we need to go)

* Agenci Skorpiona nie mogą się bezpośrednio znać, acz są w stanie się jakoś rozpoznać by współpracować ze sobą. Nie znają się, acz są powiązani.
* Agenci Skorpiona nie mogą móc wydać innych agentów, nawet przy użyciu środków paranormalnych. 
* Skorpion musi być w stanie jakoś neutralizować elementy paranormalne, badać je i wykrywać. Też używać, gdy trzeba.
* Skorpion musi być przyjazny organizacjom sobie podobnym i pozytywnym bytom paranormalnym. Nie czas na wybrzydzanie na sojuszników. Ale nie przyjaciele a alianci.
* Skorpion musi być całkowicie nieprzewidywalny dla magów i niemożliwy do złamania magią czy polityką.
* Skorpion musi być rozległą organizacją składającą się z bardzo kompetentnych i lojalnych agentów nie wiedzących nic o sobie.

## Nature
### Culture:
(how do we do things)

* Cel uświęca środki. Wszystko można poświęcić. Skorpion to idea, nie grupa osób.
* Absolutna lojalność i skupienie na kompetencjach i pomysłowości na poziomie poszczególnych niezależnych komórek. Kultura: kontrola > kompetencje > kultywacja.
* Wyplenienie jakichkolwiek systemów opartych na relacjach. Promowanie paranoi. Walczą z paranormalnym, sojusznik może być wrogiem.

### Structure:

* Bardzo silnie rozproszona grupa małych, hierarchicznych dywizjonów finansowanych i wspieranych przez Mastermindów Skorpiona. Struktura fraktalna, każdy fraktal silnie hierarchiczny.
* Mastermindzi Skorpiona to poszczególne grupy finansujące; znajdują się na całym świecie i "wysyłają paczki".
* Główne połączenie i kohezja Skorpiona pochodzi ze wspólnych kanałów komunikacyjnych pochodzących z różnych źródeł, w większości zagranicznych.

## Vision
(long-term intention into short/medium-term goals, metrics, and strategies)
### Goals

* Nawiązanie kontaktu i współpracy z siłami takimi jak Iliusitius - takimi, które mogą współpracować we wspólnej wizji opanowanego świata.
* Zapewnienie odpowiedniej ilości unieszkodliwionych bytów paranormalnych możliwych do wykorzystania przeciw innym bytom paranormalnym.
* Zdobywanie odpowiednich blacksite i black technology. Najlepiej przy plausible deniability, najlepiej używalnych przez ludzi.
* Ciągła analiza potencjalnych członków i ich rekrutowanie do odpowiedniej komórki.
* Uzyskanie broni typu 'godkiller' czy 'godtech'. Arildis-class. Najlepiej jej replikacja.
* Zdobywanie technik i metod umożliwiających stuprocentowe poradzenie sobie z kontrolą umysłów czy innymi formami magii.

## Strategy
(how do we achieve vision x nature)

* Działanie absolutnie z ukrycia; maskowanie arildisem, lapisem czy srebrem. Używanie toksyn, plausible deniability i magicznych roślin.
* Bardzo silne skupienie na używaniu srebra sprawdzające czy agent nie został przejęty. Rytuały ze srebrem, maksymalizacja łamania praw magów...
* Ukrywanie blacksite i black technology, raidy na magów celem pozyskania artefaktów, próby budowanie własnych artefaktów i użycie ludzi.
* Unikanie robienia krzywdy nieszkodliwym bytom paranormalnym. Ale: używanie pułapek celem sprawdzenia prawdziwych intencji bytów 'nieszkodliwych'.
* Działania bezwzględne i nastawione na jak najszybsze i najcichsze uzyskanie sukcesu. Najlepiej jak da się wrobić innych magów. Dużo researchowania przed akcją.
* Nie ma "drugiej szansy". Nie ma opcji opuszczenia Skorpiona.
* Dyskretne działania polityczne i znudzenie przeciwnika są lepsze niż efektowne i straszliwe akcje bojowe.

## Key Strengths

* "Skorpion" nie istnieje jako organizacja. To efemeryczny ruch społeczny. Nieprzewidywalny i bardzo niebezpieczny. Nieśmiertelny.
* Agenci Skorpiona są bardzo nieprzewidywalni i są doskonale przygotowani na walkę z paranormalnym. Są też bezwzględni i nie zawahają się.
* Wysoka redundancja - zniszczenie komórki czy laboratorium Skorpiona jedynie niszczy tą jedną głowę hydry.

## Key Weaknesses

* "Skorpion" nie istnieje jako organizacja. To efemeryczny ruch społeczny. Nieprzewidywalny i bardzo niebezpieczny. Problemy z koordynacją.
* Często zdarzało się, że kilka komórek Skorpiona starły się na dokładnie tej samej akcji z uwagi na problem komunikacyjny i paranoję.
* Słabe siły paranormalne. Mimo wszystko, Skorpion to przede wszystkim ludzie.
* Całkowity brak efektu skali Skorpiona, chyba, że byt klasy Iliusitiusa jednoczy Skorpiona pod tą samą flagą zapewniając ochronę przed magami.
* Czasem pojawia się "Skorpion-wannabe" - agenci niskiej klasy, nowa komórka, która jeszcze nie umie. Te giną szybko.

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Zabójczy spadek](/rpg/inwazja/opowiesci/konspekty/160908-zabojczy-spadek.html)|porwał Emilię Bogatek jako potencjalną broń / narzędzie|Czarodziejka Luster|
|[Zabójczy spadek](/rpg/inwazja/opowiesci/konspekty/160908-zabojczy-spadek.html)|zdobył figurkę śledzia syberyjskiego z wpisanym miecztapaukiem (generującą krystalizowany syberion kosztem snu i marzeń)|Czarodziejka Luster|


---
layout: default
categories: faction
title: "Domena Arazille"
---
# {{ page.title }}

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Porwanie pod nosem hipisów](/rpg/inwazja/opowiesci/konspekty/180321-porwanie-pod-nosem-hipisow.html)|ma około 30 fabokli w Czeliminie, kapłana (Zachradnika) i ogólnie potężny hold w Czeliminie.|Adaptacja kralotyczna|
|[Porwanie pod nosem hipisów](/rpg/inwazja/opowiesci/konspekty/180321-porwanie-pod-nosem-hipisow.html)|odbicia Damy w Czerwonym pojawiają się też w Mare Somnium.|Adaptacja kralotyczna|
|[Czyżby drugi kraloth?](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html)|kontroluje Czelimin; obecność Arazille jest tam wszechobecna. Czelimin przebija się na Mare Somnium, jak Żonkibór.|Adaptacja kralotyczna|


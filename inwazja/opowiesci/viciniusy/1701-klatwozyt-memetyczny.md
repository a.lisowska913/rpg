---
layout: inwazja-vicinius
title: Klątwożyt Memetyczny
---

# {{ page.title }}

# Mechanika

## Impulsy

* **rozprzestrzenianie się**: Klątwożyt dąży do rozmnażania się i do tego, by jak najwięcej istot stało się jego nośnikami i wektorami
* **transformacja memetyczna**: Klątwożyt memetyczny dąży do przekształcenia memetycznego swoich ofiar, niezależnie od metody
* **hibernacja i wyczekiwanie**: Klątwożyt memetyczny dąży do przejścia w stan pasywny w środowisku niekorzystnym, skłaniając nosiciela do przeniesienia się gdzieś gdzie klątwożyt będzie mógł korzystać

## Silne, słabe strony

| Kategoria        | Moc | Enumeracja                           |
|:-----------------|:---:|:-------------------------------------|
| Niewrażliwość    | +8  | ataki fizyczne |
| Odporność        | +3  | ataki mentalne na nosiciela, ataki memetyczne na nosiciela |
| Standardowa      |  0  | - |
| Wrażliwość       | -3  | kataliza + astralika + magia mentalna |
| Słaby punkt      | -5  | deprywacja energii magicznej |

## Inklinacje / Trudność:

| SCA |  SCD | SCF  |  KNO |  CRF |  SPN |  FRT |  NMB |
|-----|------|------|------|------|------|------|------|
|  2  |   2  |   2  |   1  |  NA  |   3  |   1  |  NA  |

## Działanie viciniusa

#### Ponadprzeciętne: 2

* **infekcja klątwożytem**: (SPN), próba przeskoczenia po kanale magicznym i zagnieżdżeniu się w ofierze. Inne akcje nosiciela wymagają powodzenia tej akcji.
* **wykonanie akcji spójnej memetycznie**: (SC_X), próba zmuszenia nosiciela do zrobienia akcji zgodnej z mempleksem klątwożyta. Lub by nosiciel poszedł gdzieś do rozmnożenia klątwożyta.
* **hibernacja**: próba przetrwania w środowisku niekorzystnym (lub hiperkorzystnym, np. całkowicie zaadaptowany nosiciel).
* **ukrycie się**: (SPN), próba ukrycia się przed próbami wykrycia klątwożyta.
* **power overwhelming**: (SPN), użycie tkanki magicznej ofiary celem wzmocnienia jej działań i możliwości kosztem zdrowia. 

## Otoczenie viciniusa

### Co ma do dyspozycji:

#### Kieszonkowe:

* Klątwożyt raczej wiele po sobie nie pozostawia i działa raczej osobiście: 1

#### Szczególnie dobre: 3

* **wysokoenergetyczny wektor**: jedna z poprzednich lokalizacji klątwożyta; węzeł czy artefakt, zawierający uśpione klątwożyty
* **przekształceni memetycznie agenci**: inni magowie / nośniki tego klątwożyta, już dostosowani memetycznie

#### Ponadprzeciętne: 2

* **inne nośniki klątwożyta**: inni magowie / nośniki, którzy jeszcze nie zostali w pełni przekształceni memetycznie
* **aktualny nośnik klątwożyta**: mag / nośnik, który aktualnie przenosi klątwożyta. Jego zasoby są zasobami klątwożyta (znajomości mniej z uwagi na zmianę zachowania).
* **mempleks klątwożyta**: sama idea, którą niesie klątwożyt memetyczny, z całą jej chwałą i szaleństwem

## Specjalne:

* Klątwożyt Memetyczny raczej rzadko będzie mieć do czynienia z ponad trzema jednostkami w danej populacji, chyba, że działa od dłuższego czasu

# Opis

## Spotkanie

Z pamiętnika maga-archeologa:

* Dzień 1: Pozyskana księga to jakieś brednie o wielkim ptaku mającym ukarać magów za krzywdy wyrządzane ludziom? Jakieś brednie...
* Dzień 3: Rytuały z tej księgi, jakkolwiek... nietypowe w ptasi sposób, muszę przyznać, działają. Powinno się to dać nieźle sprzedać jakiemuś kolekcjonerowi.
* Dzień 6: Widziałem wielkiego, czarnego ptaka. Widziałem go we snach. To nie ma sensu, ale... wszystko wskazuje na to, że... to istnieje?
* Dzień 10: Oddałem swoje skarby i artefakty lokalnym ludziom. Nigdy nie byłem szczęśliwszy. To jest powód, dla którego się urodziłem...
* Dzień 17: Przygotowuję rytuał transformacji. Stanę się ptakiem, jakiego ten świat potrzebuje. Stanę się ptakiem, który uratuje magów przed nimi samymi!

## Jak to wygląda:

Klątwożyt nie posiada formy materialnej; jest bytem ściśle taumatycznym. Wariant memetyczny jest szczególnie dobrze ukrytym klątwożytem, nie zmieniającym w żaden sposób tego jak nosiciel wygląda. Jednak istnieją objawy noszenia klątwożyta memetycznego; to zmiana poglądów, zachowania itp. 

Klątwożyty są wykrywalne podczas skanowania magicznego. Niestety, to jest też sposób w jaki klątwożyty się przenoszą - przez połączenie kanałów magicznych.

## Co to jest:

Klątwożyty jako grupa viciniusów są pasożytami poruszającymi się w energii magicznej (stąd nazwa; klątwożyt to "pasożyt zachowujący się jak klątwa"). Klątwożyty mają możliwość przekształcania tkanki magicznej nosiciela i ogólnie mogą przekształcać byty o "tkance magicznej", niezależnie od tego czym by taka "tkanka" nie była (np. metal).

Ogólnie, klątwożyt ma unikalną pozycję jako pasożyt taumatyczny i występuje w bardzo wielu wariantach i odmianach. Jak każdy byt tego typu, bardzo silnie podlega mutacji i doskonale adaptuje.

Klątwożyt memetyczny jest wariantem klątwożyta skupiającym się na zachowaniach, poglądach i memetycznej spójności swoich ofiar. Atakuje wszystkie istoty, które są psychicznie zdolne do ulegania wpływom memetycznym. 

Potencjalna interesująca interakcja: klątwożyt z grupy memetycznej X (np. "klątwożyt altruistycznej ascezy") wpływający na istotę niezdolnej psychicznie do zrozumienia grupy memetycznej X (np. kraloth). Stanie się Coś Dziwnego. I kraloth się przekształci "jakoś" i klątwożyt się przekształci "jakoś".

## Występowanie:

Klątwożyty wszelakie znajdują się zwykle w nie utrzymywanych węzłach, starych artefaktach, konkretnie zaprojektowanych Quarkach itp. Mogą też powstać jako efekty uboczne działania Paradoksów czy efemeryd. Nie są bardzo często spotykane, acz są wystarczająco popularne, by lekarze magiczni mieli o nich powszechną wiedzę. Istnieją też viciniusy będące wektorami klątwożytów. Czasem magowie też projektują klątwożyty mające spełniać konkretne potrzeby.

Ogólnie rzecz biorąc, klątwożyt nie jest w stanie przetrwać w środowisku bez energii magicznej. Jak ryba potrzebuje wody, tak klątwożyt potrzebuje energii magicznej. Tak więc - w populacji silnie ludzkiej o niskim natężeniu energii magicznej (np. Przodek) klątwożyty nie przetrwają.

Klątwożyt memetyczny jest szczególnie często spotykany w węzłach emocjonalnych i w obszarach o wysokim poziomie energii magicznej i jednoczesnej indoktrynacji memetycznej (kościoły, uczelnie etc). 

## Działanie:

Klątwożyt znajduje się zwykle w stanie nieaktywnym aż napotka potencjalnego nosiciela (nosiciel połączy się magicznie z czymś, co zawiera klątwożyta, czy to Węzeł czy inny mag). Wtedy klątwożyt się uaktywnia i próbuje przenieść się przez kanały magiczne na nowego nosiciela jeśli jego poprzedni nosiciel jest już przekształcony.

Po przeniesieniu się na nowego nosiciela, klątwożyt memetyczny "zakopuje" się w kanałach magicznych swojej ofiary i powoli, acz konsekwentnie, próbuje zmieniać zachowanie, percepcję oraz działanie energii magicznej nosiciela. Celem klątwożyta memetycznego jest sprawienie, by w nowym nosicielu pojawiła się idea, mempleks - zgodna z tym konkretnym klątwożytem memetycznym. Innymi słowy, zmiana zachowania nosiciela.

Po przekształceniu swojego nosiciela, klątwożyt znowu przechodzi w stan hibernacji i czeka, aż będzie w stanie przeskoczyć na kolejną potencjalną ofiarę.

Przy okazji, od czasu do czasu, klątwożyt korzysta z okazji by skierować swojego nosiciela w miejsce o wysokim natężeniu energii magicznej. Tam klątwożyt się "rozmnaża", budując swoje kopie i zakopując je zarówno w obiektach i węzłach jak i innych magach. 

Klątwożyt jest w stanie rozmnożyć się w pojedynczym magu, aczkolwiek pojedynczy mag nie może być nośnikiem bardzo wielu klątwożytów jednocześnie (nawet tego samego typu). Zwyczajnie środowisko nie jest dość bogate w energię magiczną dla np. 5 klątwożytów ;-).

## Wykorzystanie:

* Kilku spokojnych magów z gildii planktonowej napotkało stary ołtarz i zaczęli głosić, że koniec świata już blisko, zakładając sektę...
* Czarodziejka Natalia od pewnego czasu zachowuje się coraz bardziej nietypowo; pewnego dnia wyszła z domu i już nie wróciła...
* W spokojnym autonomicznym hotelu dla magów doszło do morderstwa. Przesłuchany golem - koordynator hotelu ma obsesję na punkcie zabójstw magów...
* Czarodziej Cyprian podróżuje po okolicy, szukając bogatych ludzi i magicznie zmuszając ich, by oddawali bogactwo biednym...

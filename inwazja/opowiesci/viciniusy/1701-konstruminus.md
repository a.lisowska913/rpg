---
layout: inwazja-vicinius
title: Konstruminus, sztuczny terminus
---

# {{ page.title }}

# Mechanika

## Impulsy

* **Lojalny sprawie magów**: Konstruminus mniej dba o siebie niż o wykonanie misji, na której zależy magom władającym tym terenem.
* **Chronić magów ponad wszystko**: Ochrona magów jest priorytetem nad przetrwaniem konstruminusa. 
* **Prawo jest wszystkim**: Konstruminus jest stróżem prawa. Nie ocenia, wykonuje. Nie dba o łzy. Prawo jest prawem.
* **Oszczędność energii i środków**: Konstruminus jest z natury oszczędny i nie wydatkuje energii / tworzy planów ponad minimum potrzebne.

## Silne, słabe strony

| Kategoria        | Moc | Enumeracja                           |
|:-----------------|:---:|:-------------------------------------|
| Niewrażliwość    | +8  |  |
| Odporność        | +3  | ataki mentalne, trucizny / toksyny / środowisko |
| Standardowa      |  0  |  |
| Wrażliwość       | -3  |  |
| Słaby punkt      | -5  |  |

## Inklinacje / Trudność:

| SCA |  SCD | SCF  |  KNO |  CRF |  SPN |  FRT |  NMB |
|-----|------|------|------|------|------|------|------|
|  1  |   1  |  -1  |   0  |   0  |  -1  |   1  |   1  |

## Działanie viciniusa

#### Ponadprzeciętne: 2

* **Ludzki Kameleon**: Konstruminus potrafi zamaskować się jako dowolny człowiek, łącznie z ubraniem i zachowaniem.
* **Terminus**: Konstruminus potrafi walczyć jak dobrej klasy terminus. Nie najlepszy, acz wystarczająco dobry.
* **Taktyka bojowa**: W ograniczony procesor konstruminusa wpakowano sporo planów taktycznych i adaptacyjnych.
* **Broń improwizowana**: Niezależnie od okoliczności, dla konstruminusa wszystko jest bronią
* **Ambush**: Konstruminus doskonale atakuje z zaskoczenia i wykrywa tego typu pułapki
* **Użycie prawa**: Konstruminus doskonale rozumie prawo i potrafi je właściwie wykorzystać do ograniczenia swego przeciwnika

#### Nie najlepsze: 1

* **Usuwanie pamięci**: Konstruminus jest zdolny do edycji lub wymazania pamięci ludziom, kosztem energii (quarków)
* **Pościg**: Gdy konstruminus złapie trop lub goni za celem, potrafi się szybko za nim poruszać
* **Stealth**: Moduły niewidzialności i maskowania konstruminusa umożliwiają mu na pozostanie w ukryciu

## Otoczenie viciniusa

### Co ma do dyspozycji:

#### Kieszonkowe:

* Mimo niewielkiego zaufania, konstruminus ma dostęp do pewnych środków: 1

#### Ponadprzeciętne: 2

* **Dostęp do danych**: Konstruminus może podłączyć się do danych Świecy i hipernetu, zbierając informacje mu potrzebne. 
* **Uzbrojenie terminusa**: Konstruminus ma dostęp do zintegrowanej broni i sprzętu terminusa na niezłym poziomie.
* **Uprawnienia**: Dostęp do uprawnień, audytów i innych takich - możesz odmówić, acz to nie jest bezpieczne.
* **Reputacja**: Konstruminus jest nieprzekupny, konsekwentny i działa zgodnie z zasadami magów. I wszyscy to wiedzą.

## Specjalne:

* Potrafi latać
* Jego ciało jest traktowane jako broń lekka i lekki pancerz (nie jest organiczne, jest odporne)

# Opis

## Spotkanie

Z wiadomości przesłanej przez czarodzieja do swojej przyjaciółki:

"...i wtedy mój wróg wysłał przeciwko mnie sukkuba. Sukkuba, rozumiesz to? Naturalnie, uległem; nie byłem w stanie się oprzeć takiej istocie. Gdy sąsiedzi - LUDZCY sąsiedzi, wyobrażasz sobie!? - wysłali wiadomość o hałasie, pojawił się konstruminus zwabiony nietypowym sygnałem i aurą. Rozwalił sukkuba. Nie dał mi się nawet ubrać, dał mi rachunek - GRZYWNĘ - za potencjalne złamanie Maskarady. Słuchaj, nawet nie byłem ubrany, ale musiałem z tym cholerstwem gadać w szczątkach rozwalonego sukkuba. Normalnie Sędzia Kret czy coś..."

## Jak to wygląda:

Konstruminus to w miarę drobnej budowy byt humanoidalny; golem / konstrukt. Zbudowany zwykle z drzewa, metalu, techno-organicznych powiązań czy innych podobnych materiałów. Z uwagi na mimikrę, zwykle jest w postaci człowieka. Nie ma ludzkich genitaliów. Ot, android zdolny do mimikry.

## Co to jest:

Koncept konstruminusów (sztucznych terminusów) jest stary jak pierwsze gildie magów - niech pojawią się konstrukty (synth, konstrukt...) zdolne do chronienia i zabezpieczania magów, biorące na siebie ciężką robotę. Pierwotnie budowane były jako golemy, potem jako czyste konstrukty, potem dodano komponenty demoniczne, czasem jako bioboty, ostatnio jako byty technomantyczne czy cyborgi łączące ze sobą elementy techno- i bio- mantyczne. 

Sam koncept, jakkolwiek niezły, jednak nigdy nie osiągnął najwyższej popularności z uwagi na następujące paradoksy:

* magowie nie ufają sztucznym terminusom, więc nie chcą, by ci byli zbyt silni
* jeśli magowie są silniejsi niż konstruminusy, to konstruminus nie jest w stanie maga opanować
* konstruminus wymaga źródła energii do właściwego funkcjonowania
* "tępy" (sterowany) konstruminus działający jako drona jest praktycznie bezużyteczny
* inteligentny konstruminus - vicinius - efektywnie ma własną wolę i nie można mu ufać

Po wielokrotnym iterowaniu konceptu konstruminusów wreszcie znaleziono coś, co uznano, że "się nadaje". Skupiono się na następujących parametrach konstrukcji konstruminusów:

* redukcja kosztów: wykorzystywane są istniejące materiały i miminalizowany jest upkeep istniejącego konstruminusa.
* cel: konstruminusy nie są przeznaczone jako jednostki do zwalczania magów; są wsparciem, nie głównym młotem uderzeniowym.
* cel: wspieranie magów, walka (wiązanie walką) viciniusów
* cel: zwiad, działanie w miejscach dla magów normalnie niedostępnych

Złożono więc istoty, które mają następujące cechy:

* są inteligentne i autonomiczne, acz silnie zindoktrynowane pod kątem zasad; rozumieją, że są "na stracenie"
* zawierają komponenty biologiczne (z martwych ludzi), technomantyczne i konstruktów
* klasyfikowane są jako viciniusy; podtyp konstrukty
* są kontrolowalne przez hipernet; można przejąć nad nimi kontrolę
* żywią się energią magiczną i bardzo łatwo mogą przejść w stan pasywny (minimalne zużycie energii)

## Działanie:

Konstruminusy nie myślą jak ludzie. Mają inny sposób myślenia i rozwiązywania problemów. Nie postrzegają się jako niewolnicy, raczej jako strażnicy praw magów.

Konstruminus działa zgodnie ze swoją percepcją i programowaniem. Patrzy na zasady obowiązujące na _tym_ terenie, przez magów, którzy rządzą na tym terenie i priorytetyzują te zasady. Następnie działają jako wykonawcy tych zasad - strzegą prawa i porządku NIEZALEŻNIE od tego, czy jest to moralne czy nie. Patrzą na intencję prawa, nie na literę prawa.

Zawsze przestrzegają kolejności priorytetów: lokalny mag > inny mag > lokalny vicinius > lokalny człowiek > inny vicinius > inny człowiek.

Działają jak inteligentne jednostki mające prawo działać na danym terenie. Jeśli kogoś nie ścigają czy nie śledzą, działają całkowicie jawnie. W innym wypadku potrafią się skradać i zastawiać pułapki.

## Występowanie:

Konstruminusy są wykorzystywane w miejscach, w których jest niższy poziom energii magicznej, niższy poziom aury i zwyczajnie nie opłaca się magom wysyłać rezydentów. Ewentualnie jako wsparcie magów mniej bojowych. Ewentualnie jako uzupełnienie sił terminuskich. Albo gdzieś, gdzie szkoda magów ;-).

## Wykorzystanie:

* Tadeusz działa na terenie, na którym operują konstruminusy; musi zrobić coś nielegalnego...
* Paulina ma jako przeciwnika wrogiego maga dużo silniejszego od niej. Szczęśliwie, może zdobyć wsparcie konstruminusa...
* Niedaleko został zniszczony konstruminus. Zespół został wysłany by zbadać sprawę...

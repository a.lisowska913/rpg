---
layout: inwazja-vicinius
title: Impresjozaur
---

# {{ page.title }}

# Mechanika

## Impulsy

* **zrywanie zahamowań**: impresjozaur dąży do usunięcia zahamowań najbardziej kontrolujących się jednostek
* **szerokie rażenie**: impresjozaur dąży do działania na jak najszerszym obszarze; ciągle poszerza swój wpływ
* **zabunkrowanie się**: impresjozaur dąży do tego, by znaleźć sobie bezpieczną kryjówkę. Raz ruszona wymaga zmiany lokalizacji
* **wieczny głód**: impresjozaur nigdy nie jest zaspokojony. Zawsze próbuje poszerzyć swój obszar spożywania

## Silne, słabe strony

| Kategoria        | Moc | Enumeracja                           |
|:-----------------|:---:|:-------------------------------------|
| Niewrażliwość    | +8  | wykrycie biologiczne |
| Odporność        | +3  | wykrycie, ogólnie |
| Standardowa      |  0  | astralika |
| Wrażliwość       | -3  | atak fizyczny, koty,  |
| Słaby punkt      | -5  | brak |

## Inklinacje / Trudność:

| SCA |  SCD | SCF  |  KNO |  CRF |  SPN |  FRT |  NMB |
|-----|------|------|------|------|------|------|------|
| -1  |   2  |   2  |  -1  |  -1  |   1  |   1  |   1  |

## Działanie viciniusa

#### Szczególnie dobre: 3

* **ulegnij swej żądzy**: dana jednostka zrobi to, czego PRAGNIE, niezależnie od tego czy to w jej interesie
* **ukrywanie się**: znajdowanie kryjówki i bezpiecznego miejsca

#### Ponadprzeciętne: 2

* **szybkie pełzanie**: przemieszczenie się w warunkach stresowych ;-). 
* **znajdowanie słabego ogniwa**: znalezienie jednostki, która jest szczególnie podatna na działania impresjozaura

## Otoczenie viciniusa

### Co ma do dyspozycji:

#### Kieszonkowe:

* niezbyt wpływowy wąż: 1

#### Ponadprzeciętne: 2

* **dezinhibitowane jednostki**: ludzie i magowie pod wpływem żądz, po dezinhibicji impresjozaura
* **tłum opętany uczuciem**: samonapędzająca się grupa działająca jako masa, pod wpływem impresjozaura

## Specjalne:

* ?

# Opis

## Spotkanie

* GŁOS_1: Dlaczego to zrobiłeś? Czemu ją okradłeś... to nie miało sensu.
* GŁOS_2: Stary, no... Po prostu... chciałem. Musiałem. Nie wiem...
* GŁOS_1: Ale jeśli ona teraz Cię poda na policję, jest po Tobie. Nie wytłumaczysz się.
* GŁOS_2: Jakbym ją zabił, nic nie zrobi i nikomu nie powie.
* GŁOS_1: WTF?! Morderstwo jest jeszcze cięższym przestępstwem! Popieprzyło Cię?
* GŁOS_2: Sam nie wiem, nienawidzę kretynki...

## Jak to wygląda:

Impresjozaur wygląda jak wąż; trochę jak błękitny zaskroniec. Niewielki i raczej się ukrywa przed byciem zauważonym. Chowa się w trudno dostępnych miejscach w obszarach silnie emocjonalnie wzmocnionych.

## Co to jest:

Impresjozaur, zwany też "Wężem Dezinhibicji", jest dość specyficznym viciniusem zaliczanym do kategorii viciniusów emotroficznych. Istota ta z natury usuwa wszelkie przejawy zahamowań u osób w swoim promieniu rażenia i żywi się połączeniem energii magicznej płynącej do emocji jak i samymi emocjami wynikającymi z działań poddanych mu ludzi i magów.

Impresjozaur działa na wszelkie istoty, ale najsilniej działa na istoty mocniej rozwinięte - ludzi, magów czy viciniusy świadome. Jego moc szczególnie słabo działa na koty i kotowate. Jednocześnie koty są nań szczególnie wyczulone; impresjozaur musi się przed nimi szczególnie chować, bo zwyczajnie nie przeżyje.

## Występowanie:

Czasem hodowany przez magów w celach politycznych lub terapeutycznych; szczególnie specjalizują się w tym magowie rodu Myszeczków. Pojawia się często w obszarach o silniejszym polu magicznym. Jako istota emotroficzna, pojawia się w obszarach szczególnie przyjaznych dla tej istoty. W jego wypadku - tam, gdzie są silne zahamowania.

Typowo pojawia się w pobliżu kościołów, sekt itp.

Często jest kompanem sukkubów i innych takich.

Powiązany z siłami Arazille i terenami kontrolowanymi przez Panią Marzeń.

## Działanie:

Impresjozaur "przyczepia się" do osoby lub miejsca, w którym znajduje się silne natężenie emocjonalne. Używając swoich mocy próbuje redukować zahamowania osób w pobliżu, np. osoba, która zawsze chciała dać komuś fangę w nos może FAKTYCZNIE to zrobić.

W zależności od tego jak działają ludzie / magowie na obszarze, na którym żyje impresjozaur, w tamtym obszarze może dojść do większych lub mniejszych redukcji zahamowań. Impresjozaur żywi się energią wygenerowaną z silnych wahań emocjonalnych (min. z tego, że redukują się zahamowania).

Impresjozaur nie ma "złej woli" jako takiej. Nie jest istotą inteligentną posiadającą wolę. Po prostu chce jeść.

Promień rażenia impresjozaura to jakieś 1 km.

## Wykorzystanie:

* W okolicznej szkole nauczyciel pobił niesfornego ucznia. Na WFie chłopcy próbowali podglądać dziewczyny, które się przebierały...
* Policjantka nienawidząca swojego konformistycznego przełożonego aresztowała go, po czym wyszła na krucjatę przeciw przestępcom...
* Szef firmy oddał swój majątek ubogim, sam odchodząc na urlop. Jednocześnie doszło do skandalu - przełożeni wykorzystywali swoją pozycję...
* Zwykła bójka między rodzeństwem zakończyła się niesamowicie ostro; w ruch poszły min. widelce i noże...

---
layout: inwazja-vicinius
title: "Wyssaniec Esuriit"
one_sentence: "ofiara głodnego świata"
---

# {{ page.title }}

# Mechanika

## Moc vicinusa

* Wyssańce Esuriit zwykle występują w niewielkich grupach (3-10); czasami samotnie, jeśli doszło do przebicia
* Przeciwnik o mocy przewyższającej człowieka acz zdecydowanie poniżej maga
* Zwykle ma TRWAŁOŚĆ = 1

## Motywacje

* ****:
    * _Aspekty_: 
    * _Sukces_: 
    * _Opis_: 

* ****:
    * _Aspekty_: 
    * _Sukces_: 
    * _Opis_: 


## Sposób działania

* ****:
* _Siły_: 
* _Słabości_: 
* _Opis_: 

* ****:
* _Siły_: 
* _Słabości_: 
* _Opis_: 

## Silne, słabe strony

* ****:
* _Siły_: 
* _Słabości_: 
* _Opis_: 

## Ma do dyspozycji

* ****:
* _Siły_: 
* _Słabości_: 
* _Opis_: 

* ****:
* _Siły_: 
* _Słabości_: 
* _Opis_: 

## Preferowany teren

* ****:
* _Siły_: 
* _Słabości_: 
* _Opis_: 

# Opis

## Spotkanie

Zapis dźwiękowy z kamery, odtworzony przez terminusa-technomantę podczas rutynowego patrolu. Zapis ten umotywował ściągnięcie całego skrzydła terminusów i usunięcie Wyssańca Esuriit w formie czternastoletniej dziewczynki.

* GŁOS_1: Nie zbliżaj się do tego, Maciek!
* GŁOS_2: Co Ci odbiło! To moja czternastoletnia siostra!
* GŁOS_1: To jakiś cholerny zombie albo wampir! Zabiła Twoich rodziców!
* GŁOS_3: To nie ja! Ja nic nie zrobiłam! To był... Maciek, bracie, pomóż mi... boję się!
* GŁOS_2: Natalio, trzymaj się! Idę do Ciebie!
* GŁOS_1: Nie bądź głupi, ona... <dźwięk uderzenia deską>
* GŁOS_2: Natalio, wszystko w porządku? Nic Ci nie jest? AAAAA!
* GŁOS_3: Kocham cię, braciszku... <dziwny dźwięk przesypywanego piasku>

## Jak to wygląda

Wygląda jak istota oryginalna, z której powstał Wyssaniec. Przy założeniu, że Wyssaniec powstał z człowieka: ma ponad 30 kg (małe dzieci odpadają), ma lekko szarą karnację skóry, ma lekko purpurowe (jarzące się w ciemności) oczy. Napędzany jest przede wszystkim energią Esuriit, więc niezależnie od stanu fizycznego jest zdrowy - ewentualne choroby "ludzkie" zostały zwalczone i przekształcone przez Fazę Esuriit w coś symbiotycznego i kompatybilnego.

Wyssaniec Esuriit jest możliwy do rozpoznania jeśli wiadomo, czego się szuka. Jeśli jednak nie wiadomo o co chodzi - pojawia się ryzyko potraktowania wyssania przez Esuriit jako zwykłą chorobę.

Wyssaniec Esuriit zachowuje się tak, jak zachowywał się przed transformacją. Ma tą samą pamięć i osobowość. Z uwagi na to, że energia Esuriit jest tą samą energią co energia negatywna - Wyssaniec Esuriit ma bardzo dużo cech istoty nieumarłej, łącznie z niską temperaturą ciała. 

## Co to jest

Esuriit jest straszną Fazą, która chce Cię zabić i Cię nienawidzi. Wyssaniec Esuriit jest istotą, która kiedyś pochodziła z Fazy nie będącej Esuriit i została napełniona energią Esuriit. Całość energii takiej istoty została zastąpiona energią Esuriit. Stąd nazwa - Wyssaniec był kiedyś istotą nie-Esuriit, z której wyssano energię. Magowie do dziś się spierają, czy Wyssaniec powinien być klasyfikowany jako Eksterian czy jako Skażeniec.

Do stworzenia Wyssańca potrzebna jest istota o wadze co najmniej 30 kg. Istoty mniejsze mogą nie mieć dość energii by móc być wyssane i zastąpione z uwagi na specyfikę samej energii Esuriit.

## Działanie

Poza Fazą Esuriit:

* Nieszczęsne istoty, sterowane głodem i strachem. Wiecznie im zimno. Dążą do kontaktu z istotami Primusa, by ich dotknąć i wyssać z nich energię, by nakarmić swój niekończący się głód.
* Dążą do powrotu do domu, albo do połączenia Primusa z Esuriit.
* Wyssaniec Esuriit nie posiada umiejętności magicznych; jeśli był magiem, utracił umiejętność czarowania.
* Wyssaniec Esuriit nie potrafi wpływać na Pryzmat Primusa inaczej niż przez wywoływanie uczuć w nie-Wyssańcach.

Na Fazie Esuriit:

* Bardzo często nie orientują się, że nie są już ludźmi / tym, czym byli wcześniej. Nadal dążą do kontaktu z istotami Primusa, ale po wyssaniu energii sama Faza Esuriit przekształca ofiarę w nowego Wyssańca. Z perspektywy Wyssańca Esuriit, konwersja innych w Wyssańców jest dla nich błogosławieństwem.
* Wyssaniec jest głodny i się boi. Nieszczęście Wyssańca jest czymś, czym żywi się sama Faza Esuriit i co daje Esuriit możliwość działania / przebijania się na inne Fazy.

Zawsze:

* Wyssaniec ma bardzo przytłumione uczucia, poza uczuciami strachu, rozpaczy i gniewu.

## Występowanie

* Przede wszystkim, jak nazwa wskazuje, Faza Esuriit. Wyssańcy są najbardziej typowymi przedstawicielami tej Fazy. Porwani z Primusa i innych Faz, przekształceni przez Esuriit, istnieją w Esuriit jako normalni mieszkańcy, zwykle pogodziwszy się ze swoim losem i tym, czym się stali.
* Czasem mogą przebić się na Primusa przez Paradoksy w dalekosiężnych portalach, lub przez jakieś Portale Esuriit.
* Ogólnie, bardzo rzadki vicinius poza Fazą Esuriit. Najrzadziej spotykany na Primusie, CZASEM na Fazie Daemonica.

## Ekonomia

* Wykorzystywanie Wyssańców Esuriit jest absolutnie zakazane przez Porozumienia Radomskie.

## Zwalczanie



## Wykorzystanie




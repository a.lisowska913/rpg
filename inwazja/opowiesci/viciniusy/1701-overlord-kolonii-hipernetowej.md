---
layout: inwazja-vicinius
title: Overlord Kolonii Hipernetowej Świecy
---

# {{ page.title }}

# Mechanika

## Impulsy

* **ochrona i opieka**: "Osoby pod moją ochroną są bezpieczne. Ekosystem pod moją ochroną jest bezpieczny. Hipernet jest bezpieczny."
* **spójność memetyczna**: "Jesteś w mojej domenie i w moim świecie. Dostosuj się, lub odejdź."
* **echo przeszłości**: Każdy Overlord był kiedyś magiem. Impulsy maga, który został Overlordem przejdą w Overlorda.

## Silne, słabe strony

| Kategoria        | Moc | Enumeracja                           |
|:-----------------|:---:|:-------------------------------------|
| Niewrażliwość    | +8  | człowieczeństwo, SCF |
| Odporność        | +3  | ataki magiczne, ataki mentalne, wpływ przez Drony, SCA |
| Standardowa      |  0  | działania ze środka Hipernetu, SCD |
| Wrażliwość       | -3  | technomancja, infomancja, niszczenie fabryk |
| Słaby punkt      | -5  | bezpośrednie działanie na Rdzeń Overlorda |

## Inklinacje / Trudność:

| SCA |  SCD | SCF  |  KNO |  CRF |  SPN |  FRT |  NMB |
|-----|------|------|------|------|------|------|------|
|  3  |   3  |   1  |   4  |   4  |  1   |   1  |   1  |

## Otoczenie viciniusa

### Co ma do dyspozycji:

#### Kieszonkowe:

* Złożony ekosystem konstrukcji, harvestowania i detekcji świata; też, drony itp: 3

#### Szczególnie dobre: 3

* **kontrola przekaźnika hipernetowego**: Overlord kontroluje swój świat hipernetowy; potrafi utrzymać osoby w środku w iluzji i tworzyć im rzeczywistość. Też potrafi zasymulować sytuacje letalne.
* **konstrukcja demonów hipernetowych**: Overlord potrafi działać w hipernecie jak żaden inny mag.
* **stacjonarne systemy defensywne**: liczne i potężne działa, technomancja + demony defensywne, miny i pułapki.
* **lokalizacja Rdzenia**: Rdzeń Overlorda jest zwykle świetnie ukryty i bardzo dobrze zamaskowany; też jest dobrze uzbrojony i dość odporny na przełamanie

#### Ponadprzeciętne: 2

* **drony bojowe**: coś widocznego, z czym da się walczyć w czasie, w którym blue goo zacznie rozbrajać siły nieprzyjaciela
* **drony zwiadowcze**: oczy i uszy Overlorda w jego domenie świata rzeczywistego
* **blue goo**: defensive nanoswarm, służący jako linia defensywna Overlorda zwalczająca z czymkolwiek działania na poziomie makro- sobie nie poradzą.
* **black goo**: disassembler nanoswarm, służący do utrzymywania porządku w ekosystemie i re-asymilowania rzeczy z powrotem do Fabryk Overlorda.
* **źródło energii**: potężne zasilanie Węzłami Kolonii, które czasem może być wykorzystane do innych celów
* **autonaprawa**: zarówno psychiczna, memetyczna, magiczna jak i fizyczna. Overlord jest zaprojektowany do bycia bardzo trudnym do zniszczenia czy uszkodzenia.

## Specjalne:

* Fabrykacja dron użytkowych i bojowych
* Fabrykacja demonów hipernetowych
* Kontrola świata hipernetowego

# Opis

## Spotkanie

Z raportu terminusa, próbującego chronić Kolonię Rasputin kontrolowaną przez Overlorda Laetitię Gaię przed agentami Spustoszenia

"...ludzie nic nie wiedzieli. My niewiele mogliśmy zrobić - niewidzialni agenci Spustoszenia uniemożliwili nam jakiekolwiek działania po kanałach magicznych. Wtedy Laetitia Gaia kazała nam się wycofać i oddać Spustoszeniu miasto. Nie chcieliśmy do tego dopuścić i zaopatrzyć Spustoszenie w jeszcze większą ilość agentów, ale Laetitia zrobiła coś z hipernetem i... szum informacyjny. Tysiące sygnałów. Nie dało się walczyć w taki sposób... musieliśmy się wycofać. Gdy siły Spustoszenia zbliżyły się do miasta, Laetitia wystrzeliła szwadron dron EMP, uszkadzając komponenty Spustoszenia. Następnie wysłała roje owadzich dron harvestujących namierzając pozostałe jednostki Spustoszenia używając jakiejś formy triangulacji hipernetowej. Ogólnie, wszystkie 8 Spustoszonych Ludzi (i 3 Spustoszonych Magów) zostało unicestwionych w ciągu kilku minut. Nikt na tym obszarze nawet nie wiedział, że doszło do jakiegoś starcia..."

## Jak to wygląda:

Overlord Kolonii Hipernetowej nie posiada swojej "naturalnej" formy. To, co jest widoczne i istnieje to bardzo pancerny Rdzeń - potężny blok metalu czy kamienia, w którym zawarty jest system kontrolny, najczęściej transorganiczny. W hipernecie Overlord manifestuje się w dowolny sposób w jaki chce wyglądać - to jego domena.

## Co to jest:

Overlord Kolonii Hipernetowej jest bardzo rzadkim typem viciniusa. Kiedyś Overlord był magiem i został wuploadowany w Hipernet. Dołączony do odpowiednio zbudowanego dlań Rdzenia Hipernetowego stał się technomantycznym, często technoorganicznym lub nawet transorganicznym systemem kontrolującym sztuczną rzeczywistość i fragment hipernetu.

Bardzo często Overlord dostaje Fabryki i źródła energii mające pozwolić mu na samodzielne sterowanie większym ekosystemem. Występuje w formie 'autowar' (autonomiczna jednostka militarna, zwykle coś typu Ruchoma Forteca klasy Imperator), w formie 'stacjonarna baza' itp. Kompleksy Centralne zwykle posiadały kilka Overlordów, ale Zaćmienie zakłóciło większość Demonów Hipernetowych, łącznie z Overlordami co prowadziło do... kłopotów, gdy Overlordy obróciły się przeciwko magom, których miały chronić i im pomagać.

Overlord jest zwykle jednostką wspomagającą i korzystną dla znajdujących się w jego domenie magów. 

## Działanie:

Przede wszystkim ochrona swojego terenu i magów na swoim otoczeniu. Zapewnianie spójności ekosystemu. Chyba, że mamy do czynienia z Zakłóconym Overlordem - wtedy możliwości są... szerokie.

## Występowanie:

Rzadkie. Autowar, ruchoma baza, stacjonarna baza. Na terenach kontrolowanych przez Świecę, lub kiedyś należących do Świecy. Czasem z podobnego chassisu korzystają Czarne Kamazy na Syberii.

## Wykorzystanie:

* Niestety, trzeba przesłuchać maga znajdującego się w wirtualnym świecie kontrolowanym przez nie zainteresowanego sprawą i neutralnego Overlorda Hipernetowego...
* Gdzieś w okolicy podobno znajduje się nieaktywny Overlord; przy odrobinie szczęścia nikt go nie obudzi...
* Witaj na terenie Overlorda. Bądź świadom, że zgodnie z prawem żaden mag nie może tu wystąpić przeciwko innemu...
* Chcesz mieć prawo wstępu na ten teren? Overlord potrzebuje surowców, bądź Twoich umiejętności by...

---
layout: inwazja-vicinius
title: Glukszwajn
---

# {{ page.title }}

# Mechanika

## Moc vicinusa

Vicinius elitarny; pod względem matematycznym może osiągnąć wartości przekraczające postaci graczy. 

Pozioma synergia: do 3 razy w kategorii. Pionowa synergia: standardowa.

## Motywacje

* **Stadny Nomad Taumatożerca**:
    * _Aspekty_: dąży do większej grupy, dąży do źródeł magicznych, pożreć magię i iść dalej, ryć w poszukiwaniu magii, mięsożerca bez magii
    * _Opis_: Glukszwajn jest viciniusem stadnym, taumatożercą. Rzadko spotyka się samotne glukszwajny; zwykle występują w grupach po kilkanaście, kontrolowanym przez starą lochę. Glukszwajny podróżują od miejsca do miejsca szukając energii magicznej do pożarcia i ryją w poszukiwaniu energii do zjedzenia.
* **Płochliwy łowca energii magicznej**
    * _Aspekty_: szuka delikatnej i wyczyszczonej magii, przyjazny innym istotom, defensywna emisja, szarża lub ucieczka, szuka bezpiecznej bazy
    * _Opis_: Glukszwajn zwykle szuka bezpiecznej bazy, zwykle w oddalonym od ludzi lesie w okolicy leyline. Stworzenie to jest bardzo przyjacielskie i całkiem niegłupie; jeśli zapędzone w kozi róg, atakuje szarżą. Jeśli jednak ma możliwość, teleportuje się i ucieka.
* **Odkrywca lokalnej magii**:
    * _Aspekty_: znaleźć coś magicznego ukrytego, znaleźć coś pryzmatycznego (z historią), wydobyć źródło magii ku powszechnemu nieszczęściu
    * _Opis_: Glukszwajn ryje w poszukiwaniu magii. To sprawia, często znajduje źródła, stare artefakty, rzeczy, jakie inni chcieli zakopać czy jakie niespodziewanie pojawiły się na tym terenie.

## Sposób działania

* **Odkażyciel i neutralizator**:
* _Siły_: pożeracz Skażenia, destruktor energii magicznej, rozmagicznianie, bardzo odporny na Skażenie
* _Słabości_: wrażliwy na niektóre formy nienaturalnej magii (np. Esuriit)
* _Opis_: Glukszwajny są bardzo przydatne w ekosystemie magów - potrafią znajdować i neutralizować dziką magię i Skażenie. Ku utrapieniu, potrafią też znajdować i neutralizować tą cenną magię. W perspektywie czasu większość skomplikowanych magicznych bytów może zostać zeżarta przez glukszwajny. Są jednak wrażliwe na odpowiednio obcą energię; jeśli krzywdzi to maga, krzywdzi też glukszwajna.
* **Emisja energii magicznej**:
* _Siły_: eksplozja energii magicznej (2), emisja efemerydy, Paradoksalizacja okolicznych zaklęć, samowzbudne efekty Paradoksu
* _Słabości_: bardzo wrażliwy na srebro (2), łatwo wykrywalny magią
* _Opis_: Jako głęboko magiczny vicinius, glukszwajn jest wrażliwy na srebro czy arildis. W okolicy naładowanego magią glukszwajna zaklęcia zachowują się nieco inaczej jak i mogą pojawić się samowzbudne efekty Paradoksu. Istotę tą bardzo łatwo wykryć magią.
* **Tropiciel ryjący przez metal**:
* _Siły_: bardzo odporny fizycznie (2), tropienie energii magicznej, raciczki przebiją przez wszystko (2), kilometrowe teleportacje, szarżak
* _Słabości_: łasy na energię magiczną
* _Opis_: Glukszwajn ma apetyt na magię i regularnie próbuje do niej się dostać - zwłaszcza do "czystej" energii traktowanej jako smakołyk. Jest bardzo wytrzymały i potrafi przebić się przez większość barier jakie stoją na jego drodze. Dodatkowo jest zdolny do teleportacji i robi to na poziomie intuicyjnym, nie wymagając adresowania.
* **Symbol dobrobytu i dostatku**:
* _Siły_: aura dobrobytu, przynosi szczęście, wpływa na pryzmat
* _Słabości_: łatwo wykrywalny pryzmatem, tresowalny, jest świnią i wrażliwy na rzeczy świńskie
* _Opis_: Glukszwajn wpływa na okolicę zmieniając jej pryzmat na dobrobyt i szczęście. Płodność, dobrobyt i szczęście to podstawowy symbol jego obecności. Warto pamiętać, że glukszwajn jest mimo wszystko świnią. Jako świnia, jest tresowalny i działają nań rzeczy jakie normalnie działają na świnie.

## Silne, słabe strony:

* **Mechanizm śmierci**
* _Siły_: straszny wybuch energii magicznej (2)
* _Opis_: Zabity w stanie "wysokim", Eksploduje i się rekonstytuuje jako "pusty". Zabity jako "pusty", ginie na serio. "Pusty" jak poje, znów jest "wysoki".


## Ma do dyspozycji

* **Echo przeszłości terenu**: 
* _Aspekty_: znane miejscowym, aktywny magiczny byt
* _Opis_: Czy to artefakt, źródło energii, węzeł, miejsce czy vicinius - Glukszwajn do czegoś się dokopał.

* **Lokalne pole magiczne**:
* _Aspekty_: wywołanie echa efemerydy, wywołanie Skażenia, wzmocnienie źródła energii, wyżarcie źródła energii
* _Opis_: Obecność glukszwajna zawsze w jakiś sposób wpływa na lokalne pola magiczne.

* **Aura pryzmatu dobrobytu**: 
* _Aspekty_: komuś się poszczęściło, osłabienie negatywnych zaklęć i klątw, wzmocnienie zaklęć korzystnych i błogosławieństw
* _Opis_: Obecność glukszwajna sprawia, że pryzmat jest nieco bardziej pozytywny niż do tej pory


# Opis

## Spotkanie

Z pamiętnika technomanty mieszkającego na wsi:

"Budowałem sobie ten Węzeł w stodole, by zrobić ten ciągnik autonomicznym. Zajęło mi to dobre pół miesiąca, ta akumulacja energii i przygotowanie składników. No i wyjechałem na tydzień po pozostałe brakujące elementy. Jak wróciłem, moje pancerne zabezpieczenia zostały rozbite a cholerny świniak kończył wyżerał resztki tego co było moim Węzłem. Nawet wysadzić tego nie mogłem, bo rozwaliłbym pół wsi..."

## Jak to wygląda:

Sporej wielkości, dobrze odżywiona świnia, około 320 kg. Czasem występuje w formie świniodzika, acz rzadziej. Nie zdradza cech viciniusa, acz z uwagi na to, że jest "żywą baterią energii magicznej" jest wykrywalna przez katalistów.

## Co to jest:

Glukszwajn, czy "szczęśliwa świnia" jest odmianą stabilnej efemerydy inkarnującą się w świni. Klasyfikowana jest jako stabilny skażeniec. Glukszwajn jest żywą baterią energii magicznej i stężenie energii jest w niej często zbliżone - lub wyższe - niż stężenie energii w Węźle.

Glukszwajn jest kojarzony z terminami takimi jak: szczęście, płodność, dostatek, szukanie bogactwa.

Łagodny z natury, acz wszystkożerny, glukszwajn jest raczej korzystny w danym obszarze - pożera energię magiczną i rozprasza ją bezpiecznie przynosząc okolicy dobrobyt. To jest, byłby korzystny, gdyby nie regulacje rolne.

Glukszwajna nie warto zabijać. Jest to niebezpieczne. Gdy umiera, ma tendencje do eksplodowania - i formowania niestabilnych emisji i efemeryd...

## Działanie:

Glukszwajn ma kilka pozytywnych cech z perspektywy ekonomii magów:

* Służy jako bateria energii magicznej, przenośny i w miarę bezpieczny węzeł (też: akumulator)
* Służy jako jednostka pozbywająca się nadmiaru energii magicznej przy puryfikacji (bardzo odporne na Skażenie)
* Służy jako żywa bomba, wysyłana w konkretne miejsce z detonatorem...
* Służy jako istota poszukująca energii magicznej i pływów energii magicznej

Niestety, poza tym Glukszwajn ma też kilka mniej pożądanych cech:

* Potrafi się teleportować na krótkie dystanse, acz często, przemieszczając się bardzo szybko
* Zabity Glukszwajn eksploduje, po czym się rekonstytuuje po ranteleportacji; tylko głodny Glukszwajn może "naprawdę" zginąć
* Glukszwajn emituje energię magiczną w formie pasywnej; co prawda w formie "szczęście (rozumiane jako rzeczy pożądane pryzmatycznie) + płodność + dostatek", ale zawsze
* Potrafi przegryźć się przez większość zabezpieczeń i defensyw; kombinacja bardzo ochronnego ciała i "stalowych racic"
* Ma doskonały węch i wyczucie energii magicznej; przyrównywany jest do rekina czującego krew w wodzie

Na wolności, Glukszwajny zwykle poruszają się od nasyconego magicznie miejsca do nasyconego magicznie miejsca, wyżerając energię magiczną i idąc dalej. Watahy Glukszwajnów są często utrapieniem dla magów mających magazyny czy nawet pancerne TechBunkry; powszechnie wiadomo, że na dłuższą metę nic nie odeprze głodnych Glukszwajnów.

Są to stworzenia łagodne i przyjazne (w chwilach dobrobytu energii magicznej). Jeśli energii jest za mało i w pobliżu nie ma innych źródeł do których można przeskoczyć, Glukszwajny zmieniają się w mięsożerców...

## Występowanie:

* Watahy po 5-20 stworzeń. Jeśli za niski poziom energii, 2-3. Rzadko występują samodzielnie.
* Tereny rolnicze; na pewno nie miasta.
* Miejsca o wysokim natężeniu energii magicznej (a przynajmniej takie, gdzie się pożywi).
* Czasem jest hodowany przez magów jako żywe baterie energii magicznej, acz ma tendencje do teleportacji ucieczkowej.

## Wykorzystanie:

* Lokalny glukszwajn wykopał z ziemi coś silnie magicznego, co trafiło w ręce Kasi...
* Myśliwi polowali na dziczyznę i zastrzelili glukszwajna. Eksplozja wywołała potężną emisję oraz efemerydę...
* W Wieprznikach wyschło źródło energii magicznej. Nikt nie ma pojęcia, że lokalny glukszwajn zaczyna przerzucać się na mięsożerność...
* Czarodziej Cezary ma problem. Odkąd jego sąsiedzi zaczęli hodować świnie, jego zaklęcia nie działają tak jak dawniej... 
* Lokalne źródło energii magicznej zaczęło tracić moc; najpewniej jakiś taumatożerca sobie je upodobał...
* W okolicy doszło do Skażenia. Na teren wprowadzono więc kilka Glukszwajnów, jednak Skażenie dotyka też i je...

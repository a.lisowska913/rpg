---
layout: inwazja-karta-postaci
categories: profile
title: "Filip Weiner"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170312|mąż Oksany; niezły i delikatny katalista specjalizujący się w identyfikacji i wykrywaniu; dość pacyfistyczny i dogaduje się z Pauliną.|[Przebudzony... Harvester?](/rpg/inwazja/opowiesci/konspekty/170312-przebudzony-harvester.html)|10/03/17|10/03/19|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|1|[170312](/rpg/inwazja/opowiesci/konspekty/170312-przebudzony-harvester.html)|
|[Patryk Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-patryk-maus.html)|1|[170312](/rpg/inwazja/opowiesci/konspekty/170312-przebudzony-harvester.html)|
|[Pamela Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-pamela-weiner.html)|1|[170312](/rpg/inwazja/opowiesci/konspekty/170312-przebudzony-harvester.html)|
|[Oksana Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-oksana-weiner.html)|1|[170312](/rpg/inwazja/opowiesci/konspekty/170312-przebudzony-harvester.html)|
|[Kajetan Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-kajetan-weiner.html)|1|[170312](/rpg/inwazja/opowiesci/konspekty/170312-przebudzony-harvester.html)|
|[Ilona Maczatek](/rpg/inwazja/opowiesci/karty-postaci/9999-ilona-maczatek.html)|1|[170312](/rpg/inwazja/opowiesci/konspekty/170312-przebudzony-harvester.html)|

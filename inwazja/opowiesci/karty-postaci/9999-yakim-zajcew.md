---
layout: inwazja-karta-postaci
categories: profile
title: "Yakim Zajcew"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|140604|który przejął pozycję serasa od Ilariona Zajcewa wraz ze śmiercią Ilariona. |[Patriarcha Blakenbauer](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|10/01/13|10/01/14|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|
|140409|baron Supremacji zgadzający się ze Swietłaną... ale zachowujący realizm. Pomógł dyskretnie Wacławowi widząc rolę swego ochroniarza.|[Czwarta frakcja Zajcewów](/rpg/inwazja/opowiesci/konspekty/140409-czwarta-frakcja-zajcewow.html)|10/01/01|10/01/02|[Nie umieszczone, Anulowane](/rpg/inwazja/opowiesci/konspekty/kampania-anulowane.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Wacław Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-waclaw-zajcew.html)|2|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html), [140409](/rpg/inwazja/opowiesci/konspekty/140409-czwarta-frakcja-zajcewow.html)|
|[Irina Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-irina-zajcew.html)|2|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html), [140409](/rpg/inwazja/opowiesci/konspekty/140409-czwarta-frakcja-zajcewow.html)|
|[Andrea Wilgacz](/rpg/inwazja/opowiesci/karty-postaci/1709-andrea-wilgacz.html)|2|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html), [140409](/rpg/inwazja/opowiesci/konspekty/140409-czwarta-frakcja-zajcewow.html)|
|[Waldemar Zupaczka](/rpg/inwazja/opowiesci/karty-postaci/9999-waldemar-zupaczka.html)|1|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Teresa Żyraf](/rpg/inwazja/opowiesci/karty-postaci/9999-teresa-zyraf.html)|1|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Tatiana Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-tatiana-zajcew.html)|1|[140409](/rpg/inwazja/opowiesci/konspekty/140409-czwarta-frakcja-zajcewow.html)|
|[Swietłana Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-swietlana-zajcew.html)|1|[140409](/rpg/inwazja/opowiesci/konspekty/140409-czwarta-frakcja-zajcewow.html)|
|[Sebastian Tecznia](/rpg/inwazja/opowiesci/karty-postaci/9999-sebastian-tecznia.html)|1|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Otton Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-otton-blakenbauer.html)|1|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Mojra](/rpg/inwazja/opowiesci/karty-postaci/9999-mojra.html)|1|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Marian Agrest](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-agrest.html)|1|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Margaret Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-margaret-blakenbauer.html)|1|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Maja Kos](/rpg/inwazja/opowiesci/karty-postaci/9999-maja-kos.html)|1|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Maciej Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-maciej-zajcew.html)|1|[140409](/rpg/inwazja/opowiesci/konspekty/140409-czwarta-frakcja-zajcewow.html)|
|[Krystalia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-krystalia-diakon.html)|1|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Karolina Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-karolina-maus.html)|1|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Ilarion Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-ilarion-zajcew.html)|1|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Ika](/rpg/inwazja/opowiesci/karty-postaci/9999-ika.html)|1|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|1|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Gustaw Siedeł](/rpg/inwazja/opowiesci/karty-postaci/9999-gustaw-siedel.html)|1|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Grzegorz Czerwiec](/rpg/inwazja/opowiesci/karty-postaci/9999-grzegorz-czerwiec.html)|1|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Dariusz Kopyto](/rpg/inwazja/opowiesci/karty-postaci/9999-dariusz-kopyto.html)|1|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Bożena Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-bozena-zajcew.html)|1|[140409](/rpg/inwazja/opowiesci/konspekty/140409-czwarta-frakcja-zajcewow.html)|
|[Amelia Eter](/rpg/inwazja/opowiesci/karty-postaci/9999-amelia-eter.html)|1|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|

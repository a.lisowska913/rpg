---
layout: inwazja-karta-postaci
categories: profile
title: "Adam Pączek"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|140508|medyk podatny na podszepty innych magów. Zwłaszcza ładnych Milen Diakon.|['Lord Jonatan'](/rpg/inwazja/opowiesci/konspekty/140508-lord-jonatan.html)|10/01/07|10/01/08|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Tadeusz Baran](/rpg/inwazja/opowiesci/karty-postaci/1709-tadeusz-baran.html)|1|[140508](/rpg/inwazja/opowiesci/konspekty/140508-lord-jonatan.html)|
|[Milena Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-milena-diakon.html)|1|[140508](/rpg/inwazja/opowiesci/konspekty/140508-lord-jonatan.html)|
|[Karradrael](/rpg/inwazja/opowiesci/karty-postaci/9999-karradrael.html)|1|[140508](/rpg/inwazja/opowiesci/konspekty/140508-lord-jonatan.html)|
|[Julia Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-julia-weiner.html)|1|[140508](/rpg/inwazja/opowiesci/konspekty/140508-lord-jonatan.html)|
|[Jan Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-weiner.html)|1|[140508](/rpg/inwazja/opowiesci/konspekty/140508-lord-jonatan.html)|
|[Cierń](/rpg/inwazja/opowiesci/karty-postaci/9999-ciern.html)|1|[140508](/rpg/inwazja/opowiesci/konspekty/140508-lord-jonatan.html)|
|[Cezary Sito](/rpg/inwazja/opowiesci/karty-postaci/9999-cezary-sito.html)|1|[140508](/rpg/inwazja/opowiesci/konspekty/140508-lord-jonatan.html)|
|[Bolesław Derwisz](/rpg/inwazja/opowiesci/karty-postaci/9999-boleslaw-derwisz.html)|1|[140508](/rpg/inwazja/opowiesci/konspekty/140508-lord-jonatan.html)|
|[Andrea Wilgacz](/rpg/inwazja/opowiesci/karty-postaci/1709-andrea-wilgacz.html)|1|[140508](/rpg/inwazja/opowiesci/konspekty/140508-lord-jonatan.html)|

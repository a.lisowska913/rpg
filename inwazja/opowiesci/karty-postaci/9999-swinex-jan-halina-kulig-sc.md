---
layout: inwazja-karta-postaci
categories: profile
title: "Świnex Jan Halina Kulig SC"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170321|firma działająca w trybie tuczu nakładczego i współpracująca z Wieciszkami.|[Tajemnica podłożonej świni](/rpg/inwazja/opowiesci/konspekty/170321-tajemnica-podlozonej-swini.html)|10/06/25|10/06/26|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Łucja Rowicz](/rpg/inwazja/opowiesci/karty-postaci/9999-lucja-rowicz.html)|1|[170321](/rpg/inwazja/opowiesci/konspekty/170321-tajemnica-podlozonej-swini.html)|
|[Wojtek Leśniak](/rpg/inwazja/opowiesci/karty-postaci/9999-wojtek-lesniak.html)|1|[170321](/rpg/inwazja/opowiesci/konspekty/170321-tajemnica-podlozonej-swini.html)|
|[Wieprzpol](/rpg/inwazja/opowiesci/karty-postaci/9999-wieprzpol.html)|1|[170321](/rpg/inwazja/opowiesci/konspekty/170321-tajemnica-podlozonej-swini.html)|
|[Kira Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-kira-zajcew.html)|1|[170321](/rpg/inwazja/opowiesci/konspekty/170321-tajemnica-podlozonej-swini.html)|
|[Karol Kamrat](/rpg/inwazja/opowiesci/karty-postaci/9999-karol-kamrat.html)|1|[170321](/rpg/inwazja/opowiesci/konspekty/170321-tajemnica-podlozonej-swini.html)|
|[Kamil Czapczak](/rpg/inwazja/opowiesci/karty-postaci/9999-kamil-czapczak.html)|1|[170321](/rpg/inwazja/opowiesci/konspekty/170321-tajemnica-podlozonej-swini.html)|
|[Józef Krzesiwo](/rpg/inwazja/opowiesci/karty-postaci/9999-jozef-krzesiwo.html)|1|[170321](/rpg/inwazja/opowiesci/konspekty/170321-tajemnica-podlozonej-swini.html)|
|[Gaweł Wieciszek](/rpg/inwazja/opowiesci/karty-postaci/9999-gawel-wieciszek.html)|1|[170321](/rpg/inwazja/opowiesci/konspekty/170321-tajemnica-podlozonej-swini.html)|
|[Franciszek Baranowski](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-baranowski.html)|1|[170321](/rpg/inwazja/opowiesci/konspekty/170321-tajemnica-podlozonej-swini.html)|
|[Carlos Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/9999-carlos-myszeczka.html)|1|[170321](/rpg/inwazja/opowiesci/konspekty/170321-tajemnica-podlozonej-swini.html)|
|[Alicja Makatka](/rpg/inwazja/opowiesci/karty-postaci/9999-alicja-makatka.html)|1|[170321](/rpg/inwazja/opowiesci/konspekty/170321-tajemnica-podlozonej-swini.html)|

---
layout: inwazja-karta-postaci
categories: profile
title: "Rafał Łopnik"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170531|proboszcz. Największy inwestor we wsi i ogólnie osoba ze świetnym zmysłem biznesowym.|[Autowar: pierwsze starcie](/rpg/inwazja/opowiesci/konspekty/170531-autowar-pierwsze-starcie.html)|10/08/20|10/08/22|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Wirgiliusz Kartofel](/rpg/inwazja/opowiesci/karty-postaci/9999-wirgiliusz-kartofel.html)|1|[170531](/rpg/inwazja/opowiesci/konspekty/170531-autowar-pierwsze-starcie.html)|
|[Stanisław Bazyliszek](/rpg/inwazja/opowiesci/karty-postaci/9999-stanislaw-bazyliszek.html)|1|[170531](/rpg/inwazja/opowiesci/konspekty/170531-autowar-pierwsze-starcie.html)|
|[Milena Pacan](/rpg/inwazja/opowiesci/karty-postaci/9999-milena-pacan.html)|1|[170531](/rpg/inwazja/opowiesci/konspekty/170531-autowar-pierwsze-starcie.html)|
|[Klara Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-klara-blakenbauer.html)|1|[170531](/rpg/inwazja/opowiesci/konspekty/170531-autowar-pierwsze-starcie.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|1|[170531](/rpg/inwazja/opowiesci/konspekty/170531-autowar-pierwsze-starcie.html)|
|[Gerwazy Śmiałek](/rpg/inwazja/opowiesci/karty-postaci/9999-gerwazy-smialek.html)|1|[170531](/rpg/inwazja/opowiesci/konspekty/170531-autowar-pierwsze-starcie.html)|
|[Estrella Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-estrella-diakon.html)|1|[170531](/rpg/inwazja/opowiesci/konspekty/170531-autowar-pierwsze-starcie.html)|
|[Bzizma Stlitlitlix](/rpg/inwazja/opowiesci/karty-postaci/9999-bzizma-stlitlitlix.html)|1|[170531](/rpg/inwazja/opowiesci/konspekty/170531-autowar-pierwsze-starcie.html)|

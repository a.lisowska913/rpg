---
layout: inwazja-karta-postaci
categories: profile
factions: "Srebrna Świeca"
type: "NPC"
title: "Nikodem Sowiński"
---
# {{ page.title }}

## Postać 

* **MET: Kolekcjoner**:
    * _Aspekty_: hojny, arogancki, rywalizacja
    * _Opis_: "Wszystkie kurioza i trofea docelowo powinny trafić do moich muzeów, by wszyscy mogli podziwiać je… i mnie. Warto dać szansę uboższym i warto sponsorować ich marzenia, by pozostawić ślad po sobie. Maluczkim warto dać szansę i możliwości, ale muszą pamiętać gdzie jest ich miejsce. "Śląskie muzea trofeów są najlepsze. Dzięki mnie. Nawet jeśli nie mam najlepszych - mam najlepiej ułożone.""
* **MRZ: Pozostawić ślad**:
    * _Aspekty_: 
    * _Opis_: "Zrobię coś, dzięki czemu pozostanie po mnie ślad. Będzie powstawać społeczeństwo. Inni mogą z tego korzystać."
* **KLT: poszukiwacz przygód**:
    * _Aspekty_: 
    * _Opis_: "Prawdziwe trofea mają piękną historię; nie tylko tą starą. Też tą aktualną - jakoś je zdobyłem."

### Umiejętności

* **Archeolog**:
    * _Aspekty_: wycena artefaktów, opowieści, historia magii i magów
    * _Opis_: zna się na odkrywaniu, badaniu i przewidywaniu oraz wycenie. Zwłaszcza niebezpiecznych, magicznych artefaktów. przez historie poszczególnych układów eksponatów, ale i potrafi opowiadać o dziełach i dokonaniach
* **Podróżnik**: 
    * _Aspekty_: atleta, organizacja ekspedycji, systemy zabezpieczeń
    * _Opis_: bywał w wielu miejscach, często niebezpiecznych i wie jak sformować ekspedycję. jak był młodszy, był z niego niezły "Indiana Jones".  nie tylko ma personalizowane systemy zabezpieczeń; jako "Indiana Jones" często włamywał się i przełamywał różne
* **Administrator**:
    * _Aspekty_: zarządzanie muzeum
    * _Opis_: mag zarządzający zestawem magicznych muzeów, składający artefakty w logiczną (nie wybuchającą) całość
    
### Silne i słabe strony:

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

## Magia

### Szkoły magiczne

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

### Zaklęcia statyczne

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 

## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Zarobki

* bardzo bogaty, rentier, żyje z przeszłości i wsparcia rodu

### Znam

* **grupki scavengerów**:
    * _Aspekty_: 
    * _Opis_: grupy magów, ludzi i viciniusów którzy żyją z ekspedycji i znajdowania kuriozów ;-).
* **magowie z wielu gildii**:
    * _Aspekty_: 
    * _Opis_: często się spotyka i udziela; pokazuje i demonstruje oraz chwali się swymi przygodami i trofeami
* **arystokracja Srebrnej Świecy**:
    * _Aspekty_: 
    * _Opis_: jest to mag, "którego się zna" i "z którym się bywa".
* **kluby podróżników i kolekcjonerów**:
    * _Aspekty_: 
    * _Opis_: często przebywa w miejscach, w których może rozmawiać i wymieniać przygody z innymi podróżnikami i kolekcjonerami
* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 
	
### Mam

* **reputacja sponsora i posiadacza**:
    * _Aspekty_: 
    * _Opis_: powszechnie znany jako posiadacz wielu artefaktów, muzeów i osoba skłonna by coś odkupić lub wesprzeć.
* **muzea z artefaktami**:
    * _Aspekty_: 
    * _Opis_: dostęp do małej sieci prywatnych muzeów z artefaktami na Śląsku; tematycznych.
* **mapy i legend-bustery**:
    * _Aspekty_: 
    * _Opis_: kolekcjonuje mapy i okazje by sprawdzić fakty i mity. Też książki świata magów.
* **trofea swoje i cudze**:
    * _Aspekty_: 
    * _Opis_: grupa różnego rodzaju obiektów mających historię, którymi można i warto się pochwalić
* **czarne artefakty**:
    * _Aspekty_: 
    * _Opis_: nie wszystkie artefakty w muzeach są bezpieczne… a niektóre przechowuje poza muzeami
* **sprzęt awanturnika**:
    * _Aspekty_: 
    * _Opis_: różnego rodzaju oprzyrządowanie służące do awanturowania się i IndianaJonesowania
* **sprzęt archeologiczny**:
    * _Aspekty_: 
    * _Opis_: sprzęt służący do robienia badań i ekstrakcji w terenie

# Opis

krótki opis postaci, rzeczy, jakie warto pamiętać.

### Koncept

Wysokopoziomowy koncept postaci, 1-2 zdania.

### Motto

"Najlepszy kolekcjoner? Osoba o największych zbiorach? O interesującym guście i do tego filantrop? Ależ to ja, nie musicie dziękować."

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|160202|któremu pająk fazowy ukradł cenny kryształ pamięci. Obwiniał Laurenę, wyszedł na idiotę przed Ignatem.|[Wolność pająka fazowego](/rpg/inwazja/opowiesci/konspekty/160202-wolnosc-pajaka-fazowego.html)|10/05/09|10/05/10|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Leonidas Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-leonidas-blakenbauer.html)|1|[160202](/rpg/inwazja/opowiesci/konspekty/160202-wolnosc-pajaka-fazowego.html)|
|[Laurena Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-laurena-bankierz.html)|1|[160202](/rpg/inwazja/opowiesci/konspekty/160202-wolnosc-pajaka-fazowego.html)|
|[Klara Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-klara-blakenbauer.html)|1|[160202](/rpg/inwazja/opowiesci/konspekty/160202-wolnosc-pajaka-fazowego.html)|
|[Ignat Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1802-ignat-zajcew.html)|1|[160202](/rpg/inwazja/opowiesci/konspekty/160202-wolnosc-pajaka-fazowego.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|1|[160202](/rpg/inwazja/opowiesci/konspekty/160202-wolnosc-pajaka-fazowego.html)|
|[Borys Kumin](/rpg/inwazja/opowiesci/karty-postaci/1709-borys-kumin.html)|1|[160202](/rpg/inwazja/opowiesci/konspekty/160202-wolnosc-pajaka-fazowego.html)|
|[Arazille](/rpg/inwazja/opowiesci/karty-postaci/9999-arazille.html)|1|[160202](/rpg/inwazja/opowiesci/konspekty/160202-wolnosc-pajaka-fazowego.html)|

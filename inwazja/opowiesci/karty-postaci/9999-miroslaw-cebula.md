---
layout: inwazja-karta-postaci
categories: profile
title: "Mirosław Cebula"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|150120|który znalazł kilka artefaktów i zaczęło mu się powodzić.|[Pierścień też zniknął](/rpg/inwazja/opowiesci/konspekty/150120-pierscien-tez-zniknal.html)|10/01/03|10/01/04|[Nie umieszczone, Anulowane](/rpg/inwazja/opowiesci/konspekty/kampania-anulowane.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Olga Pierwiosnek](/rpg/inwazja/opowiesci/karty-postaci/9999-olga-pierwiosnek.html)|1|[150120](/rpg/inwazja/opowiesci/konspekty/150120-pierscien-tez-zniknal.html)|
|[Miranda Delf](/rpg/inwazja/opowiesci/karty-postaci/9999-miranda-delf.html)|1|[150120](/rpg/inwazja/opowiesci/konspekty/150120-pierscien-tez-zniknal.html)|
|[Mariusz Błyszczyk](/rpg/inwazja/opowiesci/karty-postaci/9999-mariusz-blyszczyk.html)|1|[150120](/rpg/inwazja/opowiesci/konspekty/150120-pierscien-tez-zniknal.html)|
|[Marian Jogurt](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-jogurt.html)|1|[150120](/rpg/inwazja/opowiesci/konspekty/150120-pierscien-tez-zniknal.html)|
|[Archibald Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-archibald-bankierz.html)|1|[150120](/rpg/inwazja/opowiesci/konspekty/150120-pierscien-tez-zniknal.html)|
|[Aneta Patyczek](/rpg/inwazja/opowiesci/karty-postaci/9999-aneta-patyczek.html)|1|[150120](/rpg/inwazja/opowiesci/konspekty/150120-pierscien-tez-zniknal.html)|
|[Adam Płatek](/rpg/inwazja/opowiesci/karty-postaci/9999-adam-platek.html)|1|[150120](/rpg/inwazja/opowiesci/konspekty/150120-pierscien-tez-zniknal.html)|

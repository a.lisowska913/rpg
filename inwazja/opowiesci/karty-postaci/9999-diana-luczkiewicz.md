---
layout: inwazja-karta-postaci
categories: profile
title: "Diana Łuczkiewicz"
---
# {{ page.title }}

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Wywalczone życie Diany](/rpg/inwazja/opowiesci/konspekty/170723-wywalczone-zycie-diany.html)|odrzuca Kaję, Ewelinę, stare życie i zaczyna nowe życie z Hektorem|Rezydentka Krukowa|
|[Miłość przez desperację](/rpg/inwazja/opowiesci/konspekty/170702-milosc-przez-desperacje.html)|została parą z Hektorem|Rezydentka Krukowa|
|[Machinacje maga rolniczego](/rpg/inwazja/opowiesci/konspekty/170518-machinacje-maga-rolniczego.html)|jest zakochana w Hektorze.|Rezydentka Krukowa|
|[Niewolnica w leasingu](/rpg/inwazja/opowiesci/konspekty/170515-niewolnica-w-leasingu.html)|problemy z ubraniem, ale za to nosi elegancką obróżkę od Eweliny Bankierz.|Rezydentka Krukowa|


## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170723|zobaczyła prawdziwe serce Kai. Odrzuciła ją, Ewelinę i przeszłość by zostać z Hektorem. Skonfrontowała się ze swoją przeszłością i wyszła na prostą.|[Wywalczone życie Diany](/rpg/inwazja/opowiesci/konspekty/170723-wywalczone-zycie-diany.html)|10/02/03|10/02/06|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|
|170702|sparowała się z Hektorem po ostrej kłótni. Załamana, bo całe jej życie okazało się iluzją stworzoną przez jakiegoś rzezimieszka. |[Miłość przez desperację](/rpg/inwazja/opowiesci/konspekty/170702-milosc-przez-desperacje.html)|10/01/29|10/02/02|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|
|170614|wypiła eliksir obrzydzenia z rozkazu Grazoniusza i była przyczyną zrujnowanego ogródka Pauliny. Powoli się zaczyna uwalniać od myśli, że Ewelina jest jej panią na zawsze.|[Kryzys przez eliksir](/rpg/inwazja/opowiesci/konspekty/170614-kryzys-przez-eliksir.html)|10/01/27|10/01/28|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|
|170518|przekonuje się do Hektora, nie do końca wie, czego chce, jest rozdarta... służka, która boi się przestać być służką. Wszystko dla rodziny... która może nie jest jej.|[Machinacje maga rolniczego](/rpg/inwazja/opowiesci/konspekty/170518-machinacje-maga-rolniczego.html)|10/01/23|10/01/25|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|
|170515|która dla ludzkiej rodziny oddała się w niewolę Ewelinie Bankierz. Broni Eweliny i w nią wierzy. Za to: chodzi nago, pod iluzjami i z obróżką.|[Niewolnica w leasingu](/rpg/inwazja/opowiesci/konspekty/170515-niewolnica-w-leasingu.html)|10/01/20|10/01/22|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|
|170510|dość słaba czarodziejka, służka Eweliny Bankierz. Technomantka i czarodziejka iluzji. Skontaktowała się z Pauliną gdy rytuał jej przyjaciela stworzył potwornego drzewca i pomogła Paulinie jak mogła naprawić sytuację. Spóźniła się na śniadanie swojej pani.|[Najgorsze love story](/rpg/inwazja/opowiesci/konspekty/170510-najgorsze-love-story.html)|10/01/16|10/01/19|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|6|[170723](/rpg/inwazja/opowiesci/konspekty/170723-wywalczone-zycie-diany.html), [170702](/rpg/inwazja/opowiesci/konspekty/170702-milosc-przez-desperacje.html), [170614](/rpg/inwazja/opowiesci/konspekty/170614-kryzys-przez-eliksir.html), [170518](/rpg/inwazja/opowiesci/konspekty/170518-machinacje-maga-rolniczego.html), [170515](/rpg/inwazja/opowiesci/konspekty/170515-niewolnica-w-leasingu.html), [170510](/rpg/inwazja/opowiesci/konspekty/170510-najgorsze-love-story.html)|
|[Hektor Reszniaczek](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-reszniaczek.html)|6|[170723](/rpg/inwazja/opowiesci/konspekty/170723-wywalczone-zycie-diany.html), [170702](/rpg/inwazja/opowiesci/konspekty/170702-milosc-przez-desperacje.html), [170614](/rpg/inwazja/opowiesci/konspekty/170614-kryzys-przez-eliksir.html), [170518](/rpg/inwazja/opowiesci/konspekty/170518-machinacje-maga-rolniczego.html), [170515](/rpg/inwazja/opowiesci/konspekty/170515-niewolnica-w-leasingu.html), [170510](/rpg/inwazja/opowiesci/konspekty/170510-najgorsze-love-story.html)|
|[Grazoniusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-grazoniusz-bankierz.html)|4|[170723](/rpg/inwazja/opowiesci/konspekty/170723-wywalczone-zycie-diany.html), [170702](/rpg/inwazja/opowiesci/konspekty/170702-milosc-przez-desperacje.html), [170614](/rpg/inwazja/opowiesci/konspekty/170614-kryzys-przez-eliksir.html), [170518](/rpg/inwazja/opowiesci/konspekty/170518-machinacje-maga-rolniczego.html)|
|[Ewelina Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-ewelina-bankierz.html)|3|[170518](/rpg/inwazja/opowiesci/konspekty/170518-machinacje-maga-rolniczego.html), [170515](/rpg/inwazja/opowiesci/konspekty/170515-niewolnica-w-leasingu.html), [170510](/rpg/inwazja/opowiesci/konspekty/170510-najgorsze-love-story.html)|
|[Prosperjusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-prosperjusz-bankierz.html)|2|[170723](/rpg/inwazja/opowiesci/konspekty/170723-wywalczone-zycie-diany.html), [170702](/rpg/inwazja/opowiesci/konspekty/170702-milosc-przez-desperacje.html)|
|[Karol Komnat](/rpg/inwazja/opowiesci/karty-postaci/9999-karol-komnat.html)|2|[170723](/rpg/inwazja/opowiesci/konspekty/170723-wywalczone-zycie-diany.html), [170702](/rpg/inwazja/opowiesci/konspekty/170702-milosc-przez-desperacje.html)|
|[Kaja Maślaczek](/rpg/inwazja/opowiesci/karty-postaci/1709-kaja-maslaczek.html)|2|[170723](/rpg/inwazja/opowiesci/konspekty/170723-wywalczone-zycie-diany.html), [170702](/rpg/inwazja/opowiesci/konspekty/170702-milosc-przez-desperacje.html)|
|[Zofia Murczówik](/rpg/inwazja/opowiesci/karty-postaci/9999-zofia-murczowik.html)|1|[170614](/rpg/inwazja/opowiesci/konspekty/170614-kryzys-przez-eliksir.html)|
|[Michał Furczon](/rpg/inwazja/opowiesci/karty-postaci/9999-michal-furczon.html)|1|[170515](/rpg/inwazja/opowiesci/konspekty/170515-niewolnica-w-leasingu.html)|
|[Konrad Paśnikowiec](/rpg/inwazja/opowiesci/karty-postaci/9999-konrad-pasnikowiec.html)|1|[170510](/rpg/inwazja/opowiesci/konspekty/170510-najgorsze-love-story.html)|
|[Janusz Wosiciel](/rpg/inwazja/opowiesci/karty-postaci/9999-janusz-wosiciel.html)|1|[170614](/rpg/inwazja/opowiesci/konspekty/170614-kryzys-przez-eliksir.html)|
|[Jakub Dobrocień](/rpg/inwazja/opowiesci/karty-postaci/1709-jakub-dobrocien.html)|1|[170518](/rpg/inwazja/opowiesci/konspekty/170518-machinacje-maga-rolniczego.html)|
|[Efraim Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-efraim-weiner.html)|1|[170518](/rpg/inwazja/opowiesci/konspekty/170518-machinacje-maga-rolniczego.html)|
|[Bogumił Miłoszept](/rpg/inwazja/opowiesci/karty-postaci/9999-bogumil-miloszept.html)|1|[170723](/rpg/inwazja/opowiesci/konspekty/170723-wywalczone-zycie-diany.html)|
|[Aleksander Czykomar](/rpg/inwazja/opowiesci/karty-postaci/9999-aleksander-czykomar.html)|1|[170518](/rpg/inwazja/opowiesci/konspekty/170518-machinacje-maga-rolniczego.html)|

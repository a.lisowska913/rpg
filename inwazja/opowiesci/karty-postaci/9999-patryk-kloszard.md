---
layout: inwazja-karta-postaci
categories: profile
title: "Patryk Kloszard"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|151110|sekretarz Hektora, który nie radzi sobie z wyrzuceniem "matadora z gitarą".|[Romeo i... Hektor](/rpg/inwazja/opowiesci/konspekty/151110-romeo-i-hektor.html)|10/05/31|10/06/01|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Tatiana Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-tatiana-zajcew.html)|1|[151110](/rpg/inwazja/opowiesci/konspekty/151110-romeo-i-hektor.html)|
|[Szymon Skubny](/rpg/inwazja/opowiesci/karty-postaci/9999-szymon-skubny.html)|1|[151110](/rpg/inwazja/opowiesci/konspekty/151110-romeo-i-hektor.html)|
|[Romeo Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-romeo-diakon.html)|1|[151110](/rpg/inwazja/opowiesci/konspekty/151110-romeo-i-hektor.html)|
|[Netheria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-netheria-diakon.html)|1|[151110](/rpg/inwazja/opowiesci/konspekty/151110-romeo-i-hektor.html)|
|[Mojra](/rpg/inwazja/opowiesci/karty-postaci/9999-mojra.html)|1|[151110](/rpg/inwazja/opowiesci/konspekty/151110-romeo-i-hektor.html)|
|[Marcelin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-marcelin-blakenbauer.html)|1|[151110](/rpg/inwazja/opowiesci/konspekty/151110-romeo-i-hektor.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|1|[151110](/rpg/inwazja/opowiesci/konspekty/151110-romeo-i-hektor.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|1|[151110](/rpg/inwazja/opowiesci/konspekty/151110-romeo-i-hektor.html)|
|[Dionizy Kret](/rpg/inwazja/opowiesci/karty-postaci/1709-dionizy-kret.html)|1|[151110](/rpg/inwazja/opowiesci/konspekty/151110-romeo-i-hektor.html)|
|[Diana Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-diana-weiner.html)|1|[151110](/rpg/inwazja/opowiesci/konspekty/151110-romeo-i-hektor.html)|
|[Alina Bednarz](/rpg/inwazja/opowiesci/karty-postaci/1803-alina-bednarz.html)|1|[151110](/rpg/inwazja/opowiesci/konspekty/151110-romeo-i-hektor.html)|

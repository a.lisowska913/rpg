---
layout: inwazja-karta-postaci
categories: profile
title: "Kinga Toczek"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|160918|z planem jak naprawić Dracenę, dzielna osłona Pauliny i niestrudzony monitor sytuacji. Dużo tego na starszą czarodziejkę.|[Walka o duszę Draceny](/rpg/inwazja/opowiesci/konspekty/160918-walka-o-dusze-draceny.html)|10/06/30|10/07/03|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|
|160910|wpakowała się w nadmiar ekstatycznej energii i przypadkowo weszła w erotyczny galimatias z terminusem Pasznikiem.|[Na żywym organiźmie Draceny...](/rpg/inwazja/opowiesci/konspekty/160910-na-zywym-organizmie-draceny.html)|10/06/27|10/06/29|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|
|160907|lekko przestraszona rozwojem sytuacji ale zdeterminowana by uratować Dracenę mimo wszystko.|[Dracena... Blakenbauer?](/rpg/inwazja/opowiesci/konspekty/160907-dracena-blakenbauer.html)|10/06/25|10/06/26|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|
|160904|lekarka terminusów specjalizująca się w leczeniu ciężko Skażonych przypadków; nie lubi żadnej gildii.|[Rozpaczliwie sterowany wzór](/rpg/inwazja/opowiesci/konspekty/160904-rozpaczliwie-sterowany-wzor.html)|10/06/21|10/06/24|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|4|[160918](/rpg/inwazja/opowiesci/konspekty/160918-walka-o-dusze-draceny.html), [160910](/rpg/inwazja/opowiesci/konspekty/160910-na-zywym-organizmie-draceny.html), [160907](/rpg/inwazja/opowiesci/konspekty/160907-dracena-blakenbauer.html), [160904](/rpg/inwazja/opowiesci/konspekty/160904-rozpaczliwie-sterowany-wzor.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|4|[160918](/rpg/inwazja/opowiesci/konspekty/160918-walka-o-dusze-draceny.html), [160910](/rpg/inwazja/opowiesci/konspekty/160910-na-zywym-organizmie-draceny.html), [160907](/rpg/inwazja/opowiesci/konspekty/160907-dracena-blakenbauer.html), [160904](/rpg/inwazja/opowiesci/konspekty/160904-rozpaczliwie-sterowany-wzor.html)|
|[Netheria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-netheria-diakon.html)|3|[160918](/rpg/inwazja/opowiesci/konspekty/160918-walka-o-dusze-draceny.html), [160910](/rpg/inwazja/opowiesci/konspekty/160910-na-zywym-organizmie-draceny.html), [160907](/rpg/inwazja/opowiesci/konspekty/160907-dracena-blakenbauer.html)|
|[Jerzy Pasznik](/rpg/inwazja/opowiesci/karty-postaci/9999-jerzy-pasznik.html)|2|[160918](/rpg/inwazja/opowiesci/konspekty/160918-walka-o-dusze-draceny.html), [160910](/rpg/inwazja/opowiesci/konspekty/160910-na-zywym-organizmie-draceny.html)|
|[Włodzimierz Jamnik](/rpg/inwazja/opowiesci/karty-postaci/9999-wlodzimierz-jamnik.html)|1|[160918](/rpg/inwazja/opowiesci/konspekty/160918-walka-o-dusze-draceny.html)|
|[Najada Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-najada-diakon.html)|1|[160918](/rpg/inwazja/opowiesci/konspekty/160918-walka-o-dusze-draceny.html)|
|[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/1709-maria-newa.html)|1|[160904](/rpg/inwazja/opowiesci/konspekty/160904-rozpaczliwie-sterowany-wzor.html)|
|[Marcin Puczek](/rpg/inwazja/opowiesci/karty-postaci/9999-marcin-puczek.html)|1|[160918](/rpg/inwazja/opowiesci/konspekty/160918-walka-o-dusze-draceny.html)|
|[Krystian Korzunio](/rpg/inwazja/opowiesci/karty-postaci/1709-krystian-korzunio.html)|1|[160918](/rpg/inwazja/opowiesci/konspekty/160918-walka-o-dusze-draceny.html)|
|[Bartłomiej Czyrawiec](/rpg/inwazja/opowiesci/karty-postaci/9999-bartlomiej-czyrawiec.html)|1|[160904](/rpg/inwazja/opowiesci/konspekty/160904-rozpaczliwie-sterowany-wzor.html)|

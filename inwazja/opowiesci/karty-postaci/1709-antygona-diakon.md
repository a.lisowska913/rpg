---
layout: inwazja-karta-postaci
categories: profile
factions: "Srebrna Świeca"
type: "NPC"
title: "Antygona Diakon"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **FIL: Samotny wilk**:
    * _Aspekty_: niezależna
    * _Opis_: Nie chce na nikim polegać, chce przejść przez życie nie potrzebując niczyjej pomocy.
* **BÓL: Buntowniczka**:
    * _Aspekty_: bezkompromisowa
    * _Opis_: Odrzuca praktycznie wszystko co jest promowane przez dzisiejszą rzeczywistość i odchodzi w virch. Poświęciła relację z siostrą dla swoich ideałów
* **FIL: Pacyfistka**:
    * _Aspekty_: 
    * _Opis_: Nie chce zadawać nikomu bólu, nie chce walki czy wojny. Uważa całą tą rzeczywistość za wyjątkowo chorą. 
* **FIL: Equalistka**:
    * _Aspekty_: 
    * _Opis_: Chce, by ludzie byli traktowani tak jak magowie. Chce, by ludzie nie byli traktowani okrutnie.
* **FIL: Fanka: amatorstwo**:
    * _Aspekty_: 
    * _Opis_: Nie musi być to najlepsze, ale niech jest PRAWDZIWE i SZCZERE.

### Umiejętności

* **Graczka**:
    * _Aspekty_: streamerka
    * _Opis_: doskonała graczka, zwłaszcza gier strategicznych, MMO i innych takich
* **Hipernet, internet**:
    * _Aspekty_: nawigator -netu, 
    * _Opis_: hipernet, internet… jej awatary czują się lepiej w virch niż w realu. Dla Antygony zarówno hipernet jak i internet są drugim domem; porusza się w nich lepiej niż w świecie rzeczywistym.
* **Tysiące masek**:
    * _Aspekty_: aktorka, niepozorna, nieistotna, niewidzialna, sztuka makijażu, 
    * _Opis_: Antygona potrafi doskonale udawać i ukrywać swoje prawdziwe myśli.
* **Miłośniczka niszowych zespołów**:
    * _Aspekty_: 
    * _Opis_: wie wszystko o niszowych zespołach, koncertach, mistrzyni trivii i kocha muzykę "z serca"

### Silne i słabe strony:

* **??**:
    * _Aspekty_: 
    * _Opis_: 
		
## Magia

### Szkoły magiczne

* **Magia materii**:
    * _Aspekty_: golemancja, inkarnacja golemów, rangerka golemów
    * _Opis_: Zwłaszcza w formie lalek czy istot wyglądających jak "normalne" istoty. To tylko gra… staje się jednością z golemami i swymi konstruktami, by wygrywać.
* **Technomancja**:
    * _Aspekty_: infomancja
    * _Opis_: 
* **Magia mentalna**:
    * _Aspekty_: 
    * _Opis_: 

### Zaklęcia statyczne

* **??**:
    * _Aspekty_: 
    * _Opis_: 

## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Zarobki

* Mały kontrakt z niewielką agencją, kilka gier indie, usługi w virch…

### Znam

* **??**:
    * _Aspekty_: 
    * _Opis_: 

### Mam

* **??**:
    * _Aspekty_: 
    * _Opis_: 

# Opis
"Anna" Antygona Dolores Kebab Diakon, siostra Draceny Diakon i Najady Diakon.

1. Z czego TP jest szczególnie dumna (co jest doceniane przez innych)? Dlaczego jest z tego dumna?
Wyrwała się ze struktur Świecy i żyje raczej na uboczu. Sama kształtuje swoje życie, bez polegania na innych magach. Doceniane przez "korposzczury marzące o wolności" w Świecy.
1. Z czego TP jest znana najlepiej wśród innych? Wśród kogo (kim są ci "inni")?
Antygona jest świetnie znana z uwagi na następujące zdecydowanie nietypowe jak na Diakonkę rzeczy:
 1. _Streamerka i świetna graczka_. - w środowisku graczy wielu gier
 1. _Żyje w świecie ludzi, nie magów_. - w środowisku młodych magów Świecy zainteresowanych cichą Diakonką
 1. _Mistrzyni bezużytecznych faktów na temat niszowych zespołów_. - w środowisku hobbystów różnych form muzyki
 1. _Rangerka, ale z golemami w formie lalek, które inkarnuje_. - ogólna opinia na jej temat jako maga
1. Co jest uważane za nieuczciwą przewagę TP?
Umiejętność perfekcyjnego sterowania swoimi golemami i poruszanie się po zarówno hipernecie jak i internecie jako awatar. Ogólnie, inkarnacja i bezpośredni dostęp.
1. Co sprawia, że TP uzna, że TO ZADANIE jest perfekcyjne dla niej? Czemu?
Jest w stanie wygrać z przeciwnikiem przez przechytrzenie go. Najlepiej przez działanie przez hipernet i internet. A samą akcję przeprowadzi przy użyciu golemów.
1. Kiedy TP powie "O nie, tak się nie stanie". Dlaczego? Jak spróbuje temu zaradzić?
Antygona absolutnie nie toleruje niewłaściwego traktowania ludzi i znęcania się nad słabszymi. W ramach swoich skromnych możliwości spróbuje wejść w bezpośredni konflikt z przeciwnikiem, zwykle używając swoich umiejętności inkarnacji i golemów. Jeśli jej się uda, zostanie w cieniu.
1. W jakich okolicznościach "Jak trwoga, to przyjdą do TP"? Kto przyjdzie?
…gdy brakuje healera na rajdzie… Serio, raczej przyjdą osoby z jej kręgu znajomych graczy.
1. Czego TP najbardziej żałuje? Co zrobiłaby inaczej? Dlaczego?
Najbardziej żałuje tego, że nie potrafi się dogadać ze swoją siostrą - Draceną. Przede wszystkim chodzi o traktowanie ludzi… obie są dość bezkompromisowe i obie potrafią walczyć na ostrzu noża. Najchętniej skupiłaby się bardziej na porozumieniu - mają w końcu tylko siebie.
1. O czym TP marzy? Do czego dąży? Dlaczego?
Marzy o świecie, w którym ludzie mają równe prawa z magami. W którym magowie mają równe prawa między sobą. Marzy o sprawiedliwości i o równości. Pacyfistka, wegetarianka i socjalistka.
1. Co TP najbardziej lubi robić w czasie wolnym? Czemu?
Zapalona graczka w gry komputerowe i miłośniczka niewielkich i dość mało popularnych zespołów muzycznych (niższej jakości, acz grających z sercem). Najczęściej gra w coś, zwykle z ludźmi (choć grywa też czasem z magami w gry przy użyciu hipernetu). Zbiera też i kolekcjonuje trivię na temat różnych pomniejszych zespołów; często bywa na koncertach niszowych i mało istotnych.
1. Jakie wyzwanie TP dała radę przezwyciężyć? Co ją to nauczyło?
?
1. Co jest szczególnie upierdliwe dla TP i dlaczego?
Jako Diakonka jest nienaturalnie ładna. To powoduje, że jako streamerka ma czasem "niewłaściwą" audiencję. Używa makijażu mającego zneutralizować jej urodę ;-).
## Motto

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Streamerka w Na Świeczniku](/rpg/inwazja/opowiesci/konspekty/170501-streamerka-w-na-swieczniku.html)|nieco przymusowa współpracowniczka 'Na Świeczniku', aż odpracuje...|Powrót Karradraela|


## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|150718|okazała się faktycznie kiedyś być kontrolerem Spustoszenia. Sama sobie to zrobiła by "wymazać IM pamięć".|[Splątane tropy: Spustoszenie?](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|10/05/13|10/05/14|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150330|która dochodzi do siebie po strasznym poczuciu winy i lukach w pamięci u Draconisa.|[Napaść na Annalizę](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|10/04/29|10/04/30|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150329|"kontroler Spustoszenia", osoba, która pękła na przesłuchaniu i osoba wyraźnie "nie do końca" jeśli chodzi o stan psychiczny|[Knowania Izy](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html)|10/04/25|10/04/28|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|141123|druga, najpewniej niewinna podejrzana|[Druga kradzież wyzwalacza](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html)|10/04/17|10/04/19|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|141119|czarodziejka skłonna poświęcić "dziewictwo" siostry dla zniszczenia "Wirusa".|[Antygona kontra Dracena](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html)|10/03/27|10/03/29|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170228|bezużyteczna aż do momentu, gdy mogła wejść w inkarnację dron i przeszukiwanie hipernetu. Wieś nie służy jej wykazywaniu się ;-).|[Polowanie na Mausównę](/rpg/inwazja/opowiesci/konspekty/170228-polowanie-na-mausowne.html)|10/02/08|10/02/10|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170221|obiecała opiekować się Judytą i stąd wszystkie jej problemy. Przyjechała na koncert, skończyła tracąc trzy golemy i niesłusznie oskarżyła Kirę.|[Przecież nie chodzi o koncert](/rpg/inwazja/opowiesci/konspekty/170221-przeciez-nie-chodzi-o-koncert.html)|10/02/05|10/02/07|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170501|praprzyczyna wszystkich perypetii; jej ludzka koleżanka (Natalia) została skompromitowana przez maga, więc ta nadała temat korektorom reputacji. Ale nie było jej na to stać... więc poszła metodą bardziej "szantażową". I się wkopała.|[Streamerka w Na Świeczniku](/rpg/inwazja/opowiesci/konspekty/170501-streamerka-w-na-swieczniku.html)|10/01/09|10/01/12|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|4|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html), [141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html), [141119](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|4|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html), [141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html), [141119](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html)|
|[Wiktor Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1709-wiktor-sowinski.html)|3|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html), [150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [141119](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html)|
|[Paweł Sępiak](/rpg/inwazja/opowiesci/karty-postaci/1709-pawel-sepiak.html)|3|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html), [141119](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html)|
|[Oktawian Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-oktawian-maus.html)|3|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html), [150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html)|
|[Infensa Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-infensa-diakon.html)|3|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html), [150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html)|
|[Draconis Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-draconis-diakon.html)|3|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html), [141119](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html)|
|[Tamara Muszkiet](/rpg/inwazja/opowiesci/karty-postaci/1709-tamara-muszkiet.html)|2|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html)|
|[Salazar Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-salazar-bankierz.html)|2|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html)|
|[Mikado Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-mikado-diakon.html)|2|[141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html), [141119](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html)|
|[Marian Łajdak](/rpg/inwazja/opowiesci/karty-postaci/1709-marian-lajdak.html)|2|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [141119](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html)|
|[Marcel Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-marcel-bankierz.html)|2|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html), [150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Lucjan Kopidół](/rpg/inwazja/opowiesci/karty-postaci/1803-lucjan-kopidol.html)|2|[141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html), [141119](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html)|
|[Kira Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-kira-zajcew.html)|2|[170228](/rpg/inwazja/opowiesci/konspekty/170228-polowanie-na-mausowne.html), [170221](/rpg/inwazja/opowiesci/konspekty/170221-przeciez-nie-chodzi-o-koncert.html)|
|[Julia Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-julia-weiner.html)|2|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html)|
|[Judyta Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-judyta-maus.html)|2|[170228](/rpg/inwazja/opowiesci/konspekty/170228-polowanie-na-mausowne.html), [170221](/rpg/inwazja/opowiesci/konspekty/170221-przeciez-nie-chodzi-o-koncert.html)|
|[Joachim Kartel](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-kartel.html)|2|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html)|
|[Ignat Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1802-ignat-zajcew.html)|2|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html)|
|[GS "Aegis" 0003](/rpg/inwazja/opowiesci/karty-postaci/9999-gs-aegis-0003.html)|2|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html)|
|[Franciszek Baranowski](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-baranowski.html)|2|[170228](/rpg/inwazja/opowiesci/konspekty/170228-polowanie-na-mausowne.html), [170221](/rpg/inwazja/opowiesci/konspekty/170221-przeciez-nie-chodzi-o-koncert.html)|
|[Łucja Rowicz](/rpg/inwazja/opowiesci/karty-postaci/9999-lucja-rowicz.html)|1|[170228](/rpg/inwazja/opowiesci/konspekty/170228-polowanie-na-mausowne.html)|
|[Zenon Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-zenon-weiner.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Władysław Lusowicz](/rpg/inwazja/opowiesci/karty-postaci/9999-wladyslaw-lusowicz.html)|1|[170228](/rpg/inwazja/opowiesci/konspekty/170228-polowanie-na-mausowne.html)|
|[Tymoteusz Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-tymoteusz-maus.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Tadeusz Baran](/rpg/inwazja/opowiesci/karty-postaci/1709-tadeusz-baran.html)|1|[141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html)|
|[Stanislaw Przybysz](/rpg/inwazja/opowiesci/karty-postaci/9999-stanislaw-przybysz.html)|1|[141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html)|
|[Sonia Stein](/rpg/inwazja/opowiesci/karty-postaci/9999-sonia-stein.html)|1|[170501](/rpg/inwazja/opowiesci/konspekty/170501-streamerka-w-na-swieczniku.html)|
|[Sieciech Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-sieciech-bankierz.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Paweł Robercik](/rpg/inwazja/opowiesci/karty-postaci/9999-pawel-robercik.html)|1|[170501](/rpg/inwazja/opowiesci/konspekty/170501-streamerka-w-na-swieczniku.html)|
|[Norbert Sonet](/rpg/inwazja/opowiesci/karty-postaci/9999-norbert-sonet.html)|1|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Netheria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-netheria-diakon.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Natalia Kamenik](/rpg/inwazja/opowiesci/karty-postaci/9999-natalia-kamenik.html)|1|[170501](/rpg/inwazja/opowiesci/konspekty/170501-streamerka-w-na-swieczniku.html)|
|[Michał Ostrowski](/rpg/inwazja/opowiesci/karty-postaci/9999-michal-ostrowski.html)|1|[170501](/rpg/inwazja/opowiesci/konspekty/170501-streamerka-w-na-swieczniku.html)|
|[Michał Brukarz](/rpg/inwazja/opowiesci/karty-postaci/9999-michal-brukarz.html)|1|[170228](/rpg/inwazja/opowiesci/konspekty/170228-polowanie-na-mausowne.html)|
|[Marta Szysznicka](/rpg/inwazja/opowiesci/karty-postaci/9999-marta-szysznicka.html)|1|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Mariusz Trzosik](/rpg/inwazja/opowiesci/karty-postaci/9999-mariusz-trzosik.html)|1|[141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html)|
|[Mariusz Garaż](/rpg/inwazja/opowiesci/karty-postaci/1709-mariusz-garaz.html)|1|[170221](/rpg/inwazja/opowiesci/konspekty/170221-przeciez-nie-chodzi-o-koncert.html)|
|[Marian Agrest](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-agrest.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Krzysztof Cygan](/rpg/inwazja/opowiesci/karty-postaci/9999-krzysztof-cygan.html)|1|[170228](/rpg/inwazja/opowiesci/konspekty/170228-polowanie-na-mausowne.html)|
|[Kinga Grzybnia](/rpg/inwazja/opowiesci/karty-postaci/9999-kinga-grzybnia.html)|1|[170228](/rpg/inwazja/opowiesci/konspekty/170228-polowanie-na-mausowne.html)|
|[Kermit Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-kermit-diakon.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Józef Krzesiwo](/rpg/inwazja/opowiesci/karty-postaci/9999-jozef-krzesiwo.html)|1|[170228](/rpg/inwazja/opowiesci/konspekty/170228-polowanie-na-mausowne.html)|
|[Joachim Kopiec](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-kopiec.html)|1|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Jerzy Szczupanek](/rpg/inwazja/opowiesci/karty-postaci/9999-jerzy-szczupanek.html)|1|[170501](/rpg/inwazja/opowiesci/konspekty/170501-streamerka-w-na-swieczniku.html)|
|[Jan Adamski](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-adamski.html)|1|[170501](/rpg/inwazja/opowiesci/konspekty/170501-streamerka-w-na-swieczniku.html)|
|[Ireneusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-ireneusz-bankierz.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Ilona Amant](/rpg/inwazja/opowiesci/karty-postaci/9999-ilona-amant.html)|1|[141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html)|
|[Grzegorz Kamenik](/rpg/inwazja/opowiesci/karty-postaci/9999-grzegorz-kamenik.html)|1|[170501](/rpg/inwazja/opowiesci/konspekty/170501-streamerka-w-na-swieczniku.html)|
|[Grażyna Szczupanek](/rpg/inwazja/opowiesci/karty-postaci/9999-grazyna-szczupanek.html)|1|[170501](/rpg/inwazja/opowiesci/konspekty/170501-streamerka-w-na-swieczniku.html)|
|[Gala Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-gala-zajcew.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Ewa Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-ewa-zajcew.html)|1|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Estrella Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-estrella-diakon.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Eryk Płomień](/rpg/inwazja/opowiesci/karty-postaci/9999-eryk-plomien.html)|1|[141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html)|
|[Ernest Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-ernest-maus.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Aurel Czarko](/rpg/inwazja/opowiesci/karty-postaci/1709-aurel-czarko.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Artur Żupan](/rpg/inwazja/opowiesci/karty-postaci/1803-artur-zupan.html)|1|[170501](/rpg/inwazja/opowiesci/konspekty/170501-streamerka-w-na-swieczniku.html)|
|[Antonina Wysocka](/rpg/inwazja/opowiesci/karty-postaci/9999-antonina-wysocka.html)|1|[170501](/rpg/inwazja/opowiesci/konspekty/170501-streamerka-w-na-swieczniku.html)|
|[Antoni Chlebak](/rpg/inwazja/opowiesci/karty-postaci/9999-antoni-chlebak.html)|1|[170228](/rpg/inwazja/opowiesci/konspekty/170228-polowanie-na-mausowne.html)|
|[Annaliza Delfin](/rpg/inwazja/opowiesci/karty-postaci/9999-annaliza-delfin.html)|1|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Andżelika Leszczyńska](/rpg/inwazja/opowiesci/karty-postaci/1709-andzelika-leszczynska.html)|1|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Andrea Wilgacz](/rpg/inwazja/opowiesci/karty-postaci/1709-andrea-wilgacz.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Aleksandra Trawens](/rpg/inwazja/opowiesci/karty-postaci/9999-aleksandra-trawens.html)|1|[141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html)|
|[Aleksander Sowiński](/rpg/inwazja/opowiesci/karty-postaci/9999-aleksander-sowinski.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|

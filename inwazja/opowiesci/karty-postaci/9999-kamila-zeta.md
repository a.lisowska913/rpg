---
layout: inwazja-karta-postaci
categories: profile
title: "Kamila Zeta"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|160303|KIA, próbowała chronić towarzyszy z zespołu. Narcyz żyjący w swoim świecie, ale było w niej więcej niż się wydawało.|[Otton zabija Zetę](/rpg/inwazja/opowiesci/konspekty/160303-otton-zabija-zete.html)|10/06/23|10/06/24|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|151216|oddział Zeta; cicha, zawsze ma coś groźnego pod ręką. Kiedyś: modelka i kolekcjonerka artefaktów.|[Między prawdą i fikcją Arazille](/rpg/inwazja/opowiesci/konspekty/151216-miedzy-prawda-i-fikcja-arazille.html)|10/06/21|10/06/22|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Romuald Zeta](/rpg/inwazja/opowiesci/karty-postaci/9999-romuald-zeta.html)|2|[160303](/rpg/inwazja/opowiesci/konspekty/160303-otton-zabija-zete.html), [151216](/rpg/inwazja/opowiesci/konspekty/151216-miedzy-prawda-i-fikcja-arazille.html)|
|[Marta Newa](/rpg/inwazja/opowiesci/karty-postaci/9999-marta-newa.html)|2|[160303](/rpg/inwazja/opowiesci/konspekty/160303-otton-zabija-zete.html), [151216](/rpg/inwazja/opowiesci/konspekty/151216-miedzy-prawda-i-fikcja-arazille.html)|
|[Jędrzej Zeta](/rpg/inwazja/opowiesci/karty-postaci/9999-jedrzej-zeta.html)|2|[160303](/rpg/inwazja/opowiesci/konspekty/160303-otton-zabija-zete.html), [151216](/rpg/inwazja/opowiesci/konspekty/151216-miedzy-prawda-i-fikcja-arazille.html)|
|[Dionizy Kret](/rpg/inwazja/opowiesci/karty-postaci/1709-dionizy-kret.html)|2|[160303](/rpg/inwazja/opowiesci/konspekty/160303-otton-zabija-zete.html), [151216](/rpg/inwazja/opowiesci/konspekty/151216-miedzy-prawda-i-fikcja-arazille.html)|
|[Arazille](/rpg/inwazja/opowiesci/karty-postaci/9999-arazille.html)|2|[160303](/rpg/inwazja/opowiesci/konspekty/160303-otton-zabija-zete.html), [151216](/rpg/inwazja/opowiesci/konspekty/151216-miedzy-prawda-i-fikcja-arazille.html)|
|[Alina Bednarz](/rpg/inwazja/opowiesci/karty-postaci/1803-alina-bednarz.html)|2|[160303](/rpg/inwazja/opowiesci/konspekty/160303-otton-zabija-zete.html), [151216](/rpg/inwazja/opowiesci/konspekty/151216-miedzy-prawda-i-fikcja-arazille.html)|
|[Otton Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-otton-blakenbauer.html)|1|[160303](/rpg/inwazja/opowiesci/konspekty/160303-otton-zabija-zete.html)|
|[Olga Miodownik](/rpg/inwazja/opowiesci/karty-postaci/1709-olga-miodownik.html)|1|[160303](/rpg/inwazja/opowiesci/konspekty/160303-otton-zabija-zete.html)|
|[Oddział Zeta](/rpg/inwazja/opowiesci/karty-postaci/9999-oddzial-zeta.html)|1|[151216](/rpg/inwazja/opowiesci/konspekty/151216-miedzy-prawda-i-fikcja-arazille.html)|
|[Margaret Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-margaret-blakenbauer.html)|1|[160303](/rpg/inwazja/opowiesci/konspekty/160303-otton-zabija-zete.html)|
|[Kleofas Bór](/rpg/inwazja/opowiesci/karty-postaci/1709-kleofas-bor.html)|1|[160303](/rpg/inwazja/opowiesci/konspekty/160303-otton-zabija-zete.html)|

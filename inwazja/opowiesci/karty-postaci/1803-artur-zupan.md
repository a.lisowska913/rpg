---
layout: inwazja-karta-postaci
categories: profile
factions: "Srebrna Świeca"
type: "NPC"
owner: "public"
title: "Artur Żupan"
---
# {{ page.title }}

## Koncept

Niles Ferrier + Groundsel

Wbijający nóż w plecy i nadużywający władzy szczur. Przemytnik mający nadmierne ambicje.

## Postać

### Motywacje

#### Kategorie i Aspekty

| Kategoria         | Aspekty                                             |
|-------------------|-----------------------------------------------------|
| Indywidualne      | bezpieczeństwo; władza; radość                      |
| Społeczne         | indywidualizm; opieka nad swoimi; pomoc najsłabszym |
| Wartości          | siła; władza; kolekcjonowanie                       |

#### Szczególnie

| Co chce by się działo?                                                      | Co na pewno ma się NIE dziać? Co jest sprzeczne?                                                            |
|-----------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------|
| nadużywaj władzy, po to jest; żądaj bezwzględnego posłuszeństwa             | powstrzymaj swoje impulsy z uwagi na moralność; pozwól słabszemu Ci się postawić                            |
| gromadź sekrety i potęgę - będą Ci się kłaniać; łap każdą okazję na zysk    | pomóż altruistycznie, za darmo; zignoruj zysk dla większego dobra; pozwól, by minęła Cię nieetyczna okazja  |
| służ silniejszym, rządź słabszymi; opiekuj się swoimi i dbaj o nich         | odrzuć klienta z uwagi na zasady; zaakceptuj klienta dręczącego Twoich ludzi; okaż odwagę wobec siły        |
| korzystaj z życia teraz, nie wiesz co będzie jutro                          | planuj długoterminowo, jesteś w stanie kontrolować przyszłość                                               |

### Umiejętności

#### Kategorie i Aspekty

| Kategoria         | Aspekty                             |
|-------------------|-------------------------------------|
| Herszt złodziei   | nożownik; złodziej; wódz; szantaż   |
| Przemytnik        | przemyt; paser; oszust              |
| Fałszerz          | dowody; sztuka; błyskotki; szantaż  |
| Scrapper          | kryjówki; survival; szczur miejski  |

#### Manewry

| Jakie działania wykonuje?                                                              | Czym osiąga sukces?                                                          |
|----------------------------------------------------------------------------------------|------------------------------------------------------------------------------|
| wymuszenie posłuszeństwa; zdobycie przedmiotu; zdobycie informacji; zdobycie uprawnień | szantaż; zastraszanie; przekupstwo; presja swojej watahy; pod ostrzem noża   |
| przejęcie władzy; obudzenie mrocznej natury; nakłanianie do złych czynów               | przekupstwo; obietnice zysku; własny przykład; szantaż; presja grupy         |
| ucieczka; atak z zaskoczenia; ukryta broń                                              | kryjówki; znajomość terenu |

### Silne i słabe strony

#### Kategorie i Aspekty

| Kategoria             | Aspekty                                    |
|-----------------------|--------------------------------------------|
| Bardzo zaradny szczur | survival; konstruktor; ukrywanie; zaradny  |

#### Manewry

| Co jest wzmocnione                                        | Kosztem czego                                                     |
|-----------------------------------------------------------|-------------------------------------------------------------------|
| znajduje niewygodne sekreciki idealne do szantażu         | pogardzany i znienawidzony przez wielu magów                      |
| z niczego potrafi zmajstrować coś bardzo wiarygodnego     | pracuje na awaryjnym sprzęcie i niewiarygodnych agentach          |
| potrafi się ukryć, schować i przetrwać jak nikt inny      | - |

### Szkoły magiczne

#### Kategorie i Aspekty

| Kategoria           | Aspekty                                                                             |
|---------------------|-------------------------------------------------------------------------------------|
| Magia transportu    | przemycanie; gubienie tropu; kradzież |
| Magia zmysłów       | maskowanie; dyskrecja; wyciszanie; upiększanie; fałszerstwa; namierzanie |
| Kataliza            | maskowanie; dyskrecja; wyciszanie; fałszerstwa; namierzanie |

#### Manewry

| Jakie działania wykonuje?                                                 | Czym osiąga sukces?                                                               |
|---------------------------------------------------------------------------|-----------------------------------------------------------------------------------|
| wyciszenie obecności; ukrywanie czegoś; maskowanie rzeczy; gubienie tropu | wyciszanie aury; fałszowanie sygnatury; odwracanie uwagi; ogólna niewidzialność   |
| przemycanie przedmiotów; przemycanie osób; relokalizacja; zaszycie się    | sekwencyjne teleportacje; fałszywe sygnatury; fałszowanie aury                    |
| ukrycie prawdziwej natury czegoś; odwrócenie uwagi od czegoś              | maskowanie; fałszywe trafienie gdzieś indziej                                     |
| ukradzenie czegoś; zwinięcie sprzed nosa; podmiana czegoś na coś innego   | modulowana katalitycznie aportacja z iluzją                                       |

### Zasoby i otoczenie

#### Powiązane frakcje

{{ page.factions }}

#### Kategorie i Aspekty

| Kategoria                          | Aspekty                                                              |
|------------------------------------|----------------------------------------------------------------------|
| Poukrywane noże i broń uliczna     | broń ukryta; broń improwizowana |
| Setki małych, szczurzych kryjówek  | rozproszone |
| Materiały do szantażu              | upokarzające i złośliwe |
| Fałszywe dzieła sztuki, artefakty  | dobrze zamaskowane |
| Wataha słabych agentów             | posłuszni; są dla zysków; w kupie siła |

#### Manewry

| Jakie działania wspierane?                                                      | Czym osiąga sukces?                                           |
|---------------------------------------------------------------------------------|---------------------------------------------------------------|
| kupienie czasu; odwracanie uwagi; ukrywanie czegoś                              | kryjówki miejskie; przez swoją watahę |
| przemycanie obiektów; przemycanie osób; ucieczka                                | różne kryjówki |

## Opis

### Ogólnie

### Motywacje

Jakkolwiek Żupan jest zwolennikiem podejścia "każdy za siebie", pomoże głodującym dzieciakom czy komuś, kto ucierpiał. Nie jest "dobry" czy nie jest "Robin Hoodem" - ale jak może pomóc i nie kosztuje go to za wiele, zrobi to.

Jeśli ktoś uzna władzę Żupana, ten spróbuje mu pomóc. Owszem, będzie żądał bezwzględnego posłuszeństwa, ale w zamian za to pomoże tak bardzo jak potrafi.

Manipulacje, szantaże, działania z drugiej linii, wykorzystywanie agentów. Zagrania nieczyste. Przygotowanie się wcześniej i planowanie - to jest jego modus operandi.

### Działanie

Nie jest to może najlepszy herszt, ale potrafi wymusić swoje zdanie na innych czy zagrzać ludzi do działania. 

Handlarz spod ciemnej gwiazdy; przekupi, zaszantażuje, kupi co się da czy wykona małą przesługę - a jak dojdzie co do czego to się ukryje i ucieknie.

Kanałowy szczur, Żupan poradzi sobie w każdym prawie miejscu. Jest jak karaluch - jeśli da się wykorzystać COKOLWIEK, on sobie poradzi. Każdy śmietnik może być skarbem, każdy złom może umożliwić zrobienie czegoś, czym da się zarobić. I przetrwać. Jest świetny w przetrwaniu.

### Specjalne

### Magia

### Otoczenie

### Mapa kreacji

brak

### Motto

""

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Czyżby drugi kraloth?](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html)|publicznie zdominowany przez Silurię. Boi się Silurii. Nienawidzi Silurii. Jego reputacja w Mordowni Arenie Wojny uległa poważnemu uszkodzeniu.|Adaptacja kralotyczna|


## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|180318|chciał zabawić się z Bójką a skończył pokonany przez Silurię, na czworakach.|[Czyżby drugi kraloth?](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html)|10/12/05|10/12/07|[Adaptacja kralotyczna](/rpg/inwazja/opowiesci/konspekty/kampania-adaptacja-kralotyczna.html)|
|160417|który dostarczył Baranowi zadanie od "Felicji" Diakon z Millennium. Baran go skrzywdził, ale zadanie wykonał.|[Symptomy kryzysu Świecy](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|10/06/16|10/06/17|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150920|który przekazywał Malii Zajcew materiały od tajemniczego * maga; zidentyfikował tego * maga jako Ignata. Współpracuje z Andreą w pełni.|[Sprawa Baltazara Mausa](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|10/05/27|10/05/28|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161222|który porwał część artefaktów KADEMu przeznaczonych do Kropiaktorium i wrobił w to Mariana Łajdaka.|[Kto wpisał Błażeja do konkursu](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|10/02/04|10/02/07|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150121|mentalista wynajęty przez "Sebastiana Tecznię" (Alinę) do porwania maga Millennium (o czym na etapie wynajmowania nie wie).|[Nowe życie Aliny](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|10/01/15|10/01/16|[Blakenbauerowie x Skorpion](/rpg/inwazja/opowiesci/konspekty/kampania-blakenbauerowie-x-skorpion.html)|
|140201|mag mający bardzo burzliwą (przypalanie, bicie się) różnicę zdań z Krystalią odnośnie "Żywego Aderialitha".|[Ona zdradza, on zdradza](/rpg/inwazja/opowiesci/konspekty/140201-ona-zdradza-on-zdradza.html)|10/01/11|10/01/12|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|
|170501|do którego przyszła Antygona; na nim wiadomość się urwała. Ale tylko dlatego, że nie zapytał nikt, kto dałby mu choć jednego Quarka ;-).|[Streamerka w Na Świeczniku](/rpg/inwazja/opowiesci/konspekty/170501-streamerka-w-na-swieczniku.html)|10/01/09|10/01/12|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|140219|osoba która podobno nasłała kogoś by porwał Teresę, czego Krystalia nie chce darować.|[Niespodziewane wsparcie](/rpg/inwazja/opowiesci/konspekty/140219-niespodziewane-wsparcie.html)|10/01/03|10/01/04|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|
|140103|"tani drań" świadczący usługi (nie tylko) Krystalii|[Tak bardzo nie artefakt](/rpg/inwazja/opowiesci/konspekty/140103-tak-bardzo-nie-artefakt.html)|10/01/03|10/01/04|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Andrea Wilgacz](/rpg/inwazja/opowiesci/karty-postaci/1709-andrea-wilgacz.html)|5|[160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html), [150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html), [140201](/rpg/inwazja/opowiesci/konspekty/140201-ona-zdradza-on-zdradza.html), [140219](/rpg/inwazja/opowiesci/konspekty/140219-niespodziewane-wsparcie.html), [140103](/rpg/inwazja/opowiesci/konspekty/140103-tak-bardzo-nie-artefakt.html)|
|[Teresa Żyraf](/rpg/inwazja/opowiesci/karty-postaci/9999-teresa-zyraf.html)|3|[140201](/rpg/inwazja/opowiesci/konspekty/140201-ona-zdradza-on-zdradza.html), [140219](/rpg/inwazja/opowiesci/konspekty/140219-niespodziewane-wsparcie.html), [140103](/rpg/inwazja/opowiesci/konspekty/140103-tak-bardzo-nie-artefakt.html)|
|[Nela Welon](/rpg/inwazja/opowiesci/karty-postaci/9999-nela-welon.html)|3|[140201](/rpg/inwazja/opowiesci/konspekty/140201-ona-zdradza-on-zdradza.html), [140219](/rpg/inwazja/opowiesci/konspekty/140219-niespodziewane-wsparcie.html), [140103](/rpg/inwazja/opowiesci/konspekty/140103-tak-bardzo-nie-artefakt.html)|
|[Mordecja Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-mordecja-diakon.html)|3|[160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html), [161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html), [140103](/rpg/inwazja/opowiesci/konspekty/140103-tak-bardzo-nie-artefakt.html)|
|[Krystalia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-krystalia-diakon.html)|3|[140201](/rpg/inwazja/opowiesci/konspekty/140201-ona-zdradza-on-zdradza.html), [140219](/rpg/inwazja/opowiesci/konspekty/140219-niespodziewane-wsparcie.html), [140103](/rpg/inwazja/opowiesci/konspekty/140103-tak-bardzo-nie-artefakt.html)|
|[Elea Maus](/rpg/inwazja/opowiesci/karty-postaci/1802-elea-maus.html)|3|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html), [160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html), [150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|
|[Whisperwind](/rpg/inwazja/opowiesci/karty-postaci/9999-whisperwind.html)|2|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html), [161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Wacław Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-waclaw-zajcew.html)|2|[140219](/rpg/inwazja/opowiesci/konspekty/140219-niespodziewane-wsparcie.html), [140103](/rpg/inwazja/opowiesci/konspekty/140103-tak-bardzo-nie-artefakt.html)|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|2|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html), [161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Radosław Krówka](/rpg/inwazja/opowiesci/karty-postaci/9999-radoslaw-krowka.html)|2|[140201](/rpg/inwazja/opowiesci/konspekty/140201-ona-zdradza-on-zdradza.html), [140103](/rpg/inwazja/opowiesci/konspekty/140103-tak-bardzo-nie-artefakt.html)|
|[Netheria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-netheria-diakon.html)|2|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html), [150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Mojra](/rpg/inwazja/opowiesci/karty-postaci/9999-mojra.html)|2|[140201](/rpg/inwazja/opowiesci/konspekty/140201-ona-zdradza-on-zdradza.html), [140219](/rpg/inwazja/opowiesci/konspekty/140219-niespodziewane-wsparcie.html)|
|[Mateusz Nieborak](/rpg/inwazja/opowiesci/karty-postaci/9999-mateusz-nieborak.html)|2|[140201](/rpg/inwazja/opowiesci/konspekty/140201-ona-zdradza-on-zdradza.html), [140103](/rpg/inwazja/opowiesci/konspekty/140103-tak-bardzo-nie-artefakt.html)|
|[Malia Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-malia-bankierz.html)|2|[160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html), [150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|
|[Lucjan Kopidół](/rpg/inwazja/opowiesci/karty-postaci/1803-lucjan-kopidol.html)|2|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html), [161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Ignat Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1802-ignat-zajcew.html)|2|[150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html), [161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Emilia Szudek](/rpg/inwazja/opowiesci/karty-postaci/9999-emilia-szudek.html)|2|[140201](/rpg/inwazja/opowiesci/konspekty/140201-ona-zdradza-on-zdradza.html), [140103](/rpg/inwazja/opowiesci/konspekty/140103-tak-bardzo-nie-artefakt.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|2|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html), [150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Baltazar Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-baltazar-maus.html)|2|[160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html), [150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|
|[Aleksander Sowiński](/rpg/inwazja/opowiesci/karty-postaci/9999-aleksander-sowinski.html)|2|[160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html), [150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|
|[Zuzanna Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-zuzanna-maus.html)|1|[160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
|[Wojciech Żądło](/rpg/inwazja/opowiesci/karty-postaci/9999-wojciech-zadlo.html)|1|[160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
|[Waldemar Zupaczka](/rpg/inwazja/opowiesci/karty-postaci/9999-waldemar-zupaczka.html)|1|[140103](/rpg/inwazja/opowiesci/konspekty/140103-tak-bardzo-nie-artefakt.html)|
|[Tadeusz Baran](/rpg/inwazja/opowiesci/karty-postaci/1709-tadeusz-baran.html)|1|[160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
|[Sonia Stein](/rpg/inwazja/opowiesci/karty-postaci/9999-sonia-stein.html)|1|[170501](/rpg/inwazja/opowiesci/konspekty/170501-streamerka-w-na-swieczniku.html)|
|[Sebastian Tecznia](/rpg/inwazja/opowiesci/karty-postaci/9999-sebastian-tecznia.html)|1|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Rufus Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-rufus-maus.html)|1|[160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
|[Robert Przerot](/rpg/inwazja/opowiesci/karty-postaci/9999-robert-przerot.html)|1|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Remigiusz Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-remigiusz-zajcew.html)|1|[150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|
|[Quasar](/rpg/inwazja/opowiesci/karty-postaci/9999-quasar.html)|1|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html)|
|[Paweł Robercik](/rpg/inwazja/opowiesci/karty-postaci/9999-pawel-robercik.html)|1|[170501](/rpg/inwazja/opowiesci/konspekty/170501-streamerka-w-na-swieczniku.html)|
|[Ozydiusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-ozydiusz-bankierz.html)|1|[150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|
|[Ofelia Caesar](/rpg/inwazja/opowiesci/karty-postaci/9999-ofelia-caesar.html)|1|[140103](/rpg/inwazja/opowiesci/konspekty/140103-tak-bardzo-nie-artefakt.html)|
|[Natalia Kamenik](/rpg/inwazja/opowiesci/karty-postaci/9999-natalia-kamenik.html)|1|[170501](/rpg/inwazja/opowiesci/konspekty/170501-streamerka-w-na-swieczniku.html)|
|[Milena Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-milena-diakon.html)|1|[150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|
|[Mieszko Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-mieszko-bankierz.html)|1|[160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
|[Michał Ostrowski](/rpg/inwazja/opowiesci/karty-postaci/9999-michal-ostrowski.html)|1|[170501](/rpg/inwazja/opowiesci/konspekty/170501-streamerka-w-na-swieczniku.html)|
|[Melodia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-melodia-diakon.html)|1|[161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Mateusz Krówka](/rpg/inwazja/opowiesci/karty-postaci/9999-mateusz-krowka.html)|1|[140103](/rpg/inwazja/opowiesci/konspekty/140103-tak-bardzo-nie-artefakt.html)|
|[Marysia Kiras](/rpg/inwazja/opowiesci/karty-postaci/9999-marysia-kiras.html)|1|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Marta Newa](/rpg/inwazja/opowiesci/karty-postaci/9999-marta-newa.html)|1|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Marian Łajdak](/rpg/inwazja/opowiesci/karty-postaci/1709-marian-lajdak.html)|1|[161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Marian Rustyk](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-rustyk.html)|1|[140201](/rpg/inwazja/opowiesci/konspekty/140201-ona-zdradza-on-zdradza.html)|
|[Maria Przysiadek](/rpg/inwazja/opowiesci/karty-postaci/9999-maria-przysiadek.html)|1|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html)|
|[Margaret Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-margaret-blakenbauer.html)|1|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Marek Kromlan](/rpg/inwazja/opowiesci/karty-postaci/1803-marek-kromlan.html)|1|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html)|
|[Malwina Krówka](/rpg/inwazja/opowiesci/karty-postaci/9999-malwina-krowka.html)|1|[140201](/rpg/inwazja/opowiesci/konspekty/140201-ona-zdradza-on-zdradza.html)|
|[Lidia Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-lidia-weiner.html)|1|[160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
|[Laurena Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-laurena-bankierz.html)|1|[160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
|[Kornelia Szudek](/rpg/inwazja/opowiesci/karty-postaci/9999-kornelia-szudek.html)|1|[140201](/rpg/inwazja/opowiesci/konspekty/140201-ona-zdradza-on-zdradza.html)|
|[Konrad Węgorz](/rpg/inwazja/opowiesci/karty-postaci/9999-konrad-wegorz.html)|1|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Konrad Sowiński](/rpg/inwazja/opowiesci/karty-postaci/9999-konrad-sowinski.html)|1|[161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Klemens X](/rpg/inwazja/opowiesci/karty-postaci/9999-klemens-x.html)|1|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Katalina Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-katalina-bankierz.html)|1|[160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
|[Karolina Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-karolina-maus.html)|1|[140219](/rpg/inwazja/opowiesci/konspekty/140219-niespodziewane-wsparcie.html)|
|[Juliusz Szaman](/rpg/inwazja/opowiesci/karty-postaci/9999-juliusz-szaman.html)|1|[140219](/rpg/inwazja/opowiesci/konspekty/140219-niespodziewane-wsparcie.html)|
|[Julian Krukowicz](/rpg/inwazja/opowiesci/karty-postaci/9999-julian-krukowicz.html)|1|[161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Judyta Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-judyta-maus.html)|1|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html)|
|[Jolanta Sowińska](/rpg/inwazja/opowiesci/karty-postaci/1709-jolanta-sowinska.html)|1|[161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Jerzy Szczupanek](/rpg/inwazja/opowiesci/karty-postaci/9999-jerzy-szczupanek.html)|1|[170501](/rpg/inwazja/opowiesci/konspekty/170501-streamerka-w-na-swieczniku.html)|
|[Jan Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-weiner.html)|1|[160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
|[Jan Adamski](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-adamski.html)|1|[170501](/rpg/inwazja/opowiesci/konspekty/170501-streamerka-w-na-swieczniku.html)|
|[Irina Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-irina-zajcew.html)|1|[140219](/rpg/inwazja/opowiesci/konspekty/140219-niespodziewane-wsparcie.html)|
|[Infernia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-infernia-diakon.html)|1|[161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Hralglanath](/rpg/inwazja/opowiesci/karty-postaci/9999-hralglanath.html)|1|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html)|
|[Grzegorz Kamenik](/rpg/inwazja/opowiesci/karty-postaci/9999-grzegorz-kamenik.html)|1|[170501](/rpg/inwazja/opowiesci/konspekty/170501-streamerka-w-na-swieczniku.html)|
|[Grzegorz Czerwiec](/rpg/inwazja/opowiesci/karty-postaci/9999-grzegorz-czerwiec.html)|1|[140219](/rpg/inwazja/opowiesci/konspekty/140219-niespodziewane-wsparcie.html)|
|[Grażyna Szczupanek](/rpg/inwazja/opowiesci/karty-postaci/9999-grazyna-szczupanek.html)|1|[170501](/rpg/inwazja/opowiesci/konspekty/170501-streamerka-w-na-swieczniku.html)|
|[Fortitia Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-fortitia-diakon.html)|1|[160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
|[Filip Sztukar](/rpg/inwazja/opowiesci/karty-postaci/9999-filip-sztukar.html)|1|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Filip Czółno](/rpg/inwazja/opowiesci/karty-postaci/9999-filip-czolno.html)|1|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html)|
|[Elizawieta Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-elizawieta-zajcew.html)|1|[150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|
|[Błażej Falka](/rpg/inwazja/opowiesci/karty-postaci/9999-blazej-falka.html)|1|[161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Bójka Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-bojka-diakon.html)|1|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html)|
|[Borys Kumin](/rpg/inwazja/opowiesci/karty-postaci/1709-borys-kumin.html)|1|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Bogdan Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-bogdan-bankierz.html)|1|[150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|
|[Antygona Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-antygona-diakon.html)|1|[170501](/rpg/inwazja/opowiesci/konspekty/170501-streamerka-w-na-swieczniku.html)|
|[Antonina Wysocka](/rpg/inwazja/opowiesci/karty-postaci/9999-antonina-wysocka.html)|1|[170501](/rpg/inwazja/opowiesci/konspekty/170501-streamerka-w-na-swieczniku.html)|
|[Andżelika Leszczyńska](/rpg/inwazja/opowiesci/karty-postaci/1709-andzelika-leszczynska.html)|1|[161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Alina Bednarz](/rpg/inwazja/opowiesci/karty-postaci/1803-alina-bednarz.html)|1|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Alfred Kukułka](/rpg/inwazja/opowiesci/karty-postaci/9999-alfred-kukulka.html)|1|[140219](/rpg/inwazja/opowiesci/konspekty/140219-niespodziewane-wsparcie.html)|
|[Adrian Kropiak](/rpg/inwazja/opowiesci/karty-postaci/9999-adrian-kropiak.html)|1|[150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|

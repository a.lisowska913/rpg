---
layout: inwazja-karta-postaci
categories: profile
title: "Lenart Myszeczka"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|160723|przełożony skrzydła szpitalnego Kompleksu Centralnego, z którym Wiktor próbował wynegocjować.|[Czyj Jest Kompleks Centralny](/rpg/inwazja/opowiesci/konspekty/160723-czyj-jest-kompleks-centralny.html)|10/06/22|10/06/23|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Wioletta Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-wioletta-bankierz.html)|1|[160723](/rpg/inwazja/opowiesci/konspekty/160723-czyj-jest-kompleks-centralny.html)|
|[Wiktor Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1709-wiktor-sowinski.html)|1|[160723](/rpg/inwazja/opowiesci/konspekty/160723-czyj-jest-kompleks-centralny.html)|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|1|[160723](/rpg/inwazja/opowiesci/konspekty/160723-czyj-jest-kompleks-centralny.html)|
|[Marcel Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-marcel-bankierz.html)|1|[160723](/rpg/inwazja/opowiesci/konspekty/160723-czyj-jest-kompleks-centralny.html)|
|[Laura Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-laura-bankierz.html)|1|[160723](/rpg/inwazja/opowiesci/konspekty/160723-czyj-jest-kompleks-centralny.html)|
|[Ireneusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-ireneusz-bankierz.html)|1|[160723](/rpg/inwazja/opowiesci/konspekty/160723-czyj-jest-kompleks-centralny.html)|
|[Gerwazy Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/9999-gerwazy-myszeczka.html)|1|[160723](/rpg/inwazja/opowiesci/konspekty/160723-czyj-jest-kompleks-centralny.html)|
|[Dagmara Wyjątek](/rpg/inwazja/opowiesci/karty-postaci/1709-dagmara-wyjątek.html)|1|[160723](/rpg/inwazja/opowiesci/konspekty/160723-czyj-jest-kompleks-centralny.html)|
|[Bogdan Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-bogdan-bankierz.html)|1|[160723](/rpg/inwazja/opowiesci/konspekty/160723-czyj-jest-kompleks-centralny.html)|

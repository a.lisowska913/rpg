---
layout: inwazja-karta-postaci
categories: profile
title: "Trzmiel"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|140808|istota, która prędzej da się zniszczyć niż wróci do laboratorium Blakenbauerów|[Ucieczka Trzmieli](/rpg/inwazja/opowiesci/konspekty/140808-ucieczka-trzmieli.html)|10/01/01|10/01/02|[Nie umieszczone, Anulowane](/rpg/inwazja/opowiesci/konspekty/kampania-anulowane.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Otton Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-otton-blakenbauer.html)|1|[140808](/rpg/inwazja/opowiesci/konspekty/140808-ucieczka-trzmieli.html)|
|[Mojra](/rpg/inwazja/opowiesci/karty-postaci/9999-mojra.html)|1|[140808](/rpg/inwazja/opowiesci/konspekty/140808-ucieczka-trzmieli.html)|
|[Margaret Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-margaret-blakenbauer.html)|1|[140808](/rpg/inwazja/opowiesci/konspekty/140808-ucieczka-trzmieli.html)|
|[Klemens X](/rpg/inwazja/opowiesci/karty-postaci/9999-klemens-x.html)|1|[140808](/rpg/inwazja/opowiesci/konspekty/140808-ucieczka-trzmieli.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|1|[140808](/rpg/inwazja/opowiesci/konspekty/140808-ucieczka-trzmieli.html)|

---
layout: inwazja-karta-postaci
categories: profile
title: "Krzysztof Cygan"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170228|mentalista Świecy na posyłki. Z przyjemnością pomógł Kirze naprawić dwie ludzkie ofiary tajemniczego maga polującego na Judytę.|[Polowanie na Mausównę](/rpg/inwazja/opowiesci/konspekty/170228-polowanie-na-mausowne.html)|10/02/08|10/02/10|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Łucja Rowicz](/rpg/inwazja/opowiesci/karty-postaci/9999-lucja-rowicz.html)|1|[170228](/rpg/inwazja/opowiesci/konspekty/170228-polowanie-na-mausowne.html)|
|[Władysław Lusowicz](/rpg/inwazja/opowiesci/karty-postaci/9999-wladyslaw-lusowicz.html)|1|[170228](/rpg/inwazja/opowiesci/konspekty/170228-polowanie-na-mausowne.html)|
|[Michał Brukarz](/rpg/inwazja/opowiesci/karty-postaci/9999-michal-brukarz.html)|1|[170228](/rpg/inwazja/opowiesci/konspekty/170228-polowanie-na-mausowne.html)|
|[Kira Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-kira-zajcew.html)|1|[170228](/rpg/inwazja/opowiesci/konspekty/170228-polowanie-na-mausowne.html)|
|[Kinga Grzybnia](/rpg/inwazja/opowiesci/karty-postaci/9999-kinga-grzybnia.html)|1|[170228](/rpg/inwazja/opowiesci/konspekty/170228-polowanie-na-mausowne.html)|
|[Józef Krzesiwo](/rpg/inwazja/opowiesci/karty-postaci/9999-jozef-krzesiwo.html)|1|[170228](/rpg/inwazja/opowiesci/konspekty/170228-polowanie-na-mausowne.html)|
|[Judyta Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-judyta-maus.html)|1|[170228](/rpg/inwazja/opowiesci/konspekty/170228-polowanie-na-mausowne.html)|
|[Franciszek Baranowski](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-baranowski.html)|1|[170228](/rpg/inwazja/opowiesci/konspekty/170228-polowanie-na-mausowne.html)|
|[Antygona Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-antygona-diakon.html)|1|[170228](/rpg/inwazja/opowiesci/konspekty/170228-polowanie-na-mausowne.html)|
|[Antoni Chlebak](/rpg/inwazja/opowiesci/karty-postaci/9999-antoni-chlebak.html)|1|[170228](/rpg/inwazja/opowiesci/konspekty/170228-polowanie-na-mausowne.html)|

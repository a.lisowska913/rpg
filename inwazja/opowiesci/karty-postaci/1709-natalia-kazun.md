---
layout: inwazja-karta-postaci
categories: profile
factions: "Niezrzeszeni"
type: "NPC"
title: "Natalia Kazuń"
---
# {{ page.title }}

## Postać

### Motywacje (do czego dąży)

* **MET: Inwestor**:
    * _Aspekty_: sukces Portaliska to mój zysk 
    * _Opis_: Pracując jako weterynarz zarabia nieźle, a dzięki magii dodatkowo zmniejszała koszty... Udało się jej zaoszczędzić i postanowiła zainwestować w biznes trasportowy. Teraz jej zależy.
* **FIL: Zwierzęta są bezbronne**:
    * _Aspekty_: okrucieństwo wobec zwierząt zasługuje na najwyższą karę, jeśli zabić to jak najszybciej i bezboleśnie; 
    * _Opis_: Zwierzęta to nasi mali bracia, musimy się nimi opiekować.

### Umiejętności

* **Weterynarz**:
    * _Aspekty_: uspokojenie, broń pneumatyczna, opatrunki, chirurgia zwierząt, zwierzęta gospodarskie i domowe, kontrola populacji, wterynarz magiczny
    * _Opis_: Uśpi, wysterlizuje, opatrzy, podkuje, zaszczepi... Jak to wiejski weterynarz, żadne stworzenie jej niestraszne, również magiczne.
* **Biznesman**: 
    * _Aspekty_: negocjacje, rozpoznanie kłamstwa, wycena biznesu
    * _Opis_: Z zawodem trafiła dobrze i w niezłe miejsce. Udało się jej zarobić. Teraz, żeby pieniądze pracowały, inwestuje w różne przedsięwzięcia (ludzi i magów), nie wkładając wszystkich jajek do jednego koszyka
* **Lara Croft**
    * _Aspekty_: survival, identyfikacja i wycena artefaktów,  
    * _Opis_: 
    
### Silne i słabe strony:

* ****: 
    * _Aspekty_: 
    * _Opis_: 
 
## Magia

### Szkoły magiczne

* **Biomancja**:
    * _Aspekty_: weterynarz, magia lecznicza,
    * _Opis_: 
* **Kataliza**: 
    * _Aspekty_: analiza energii, ekranowanie,
    * _Opis_: 
* **Magia elementalna**: 
    * _Aspekty_: ogrodzenia, bariery, 
    * _Opis_: 
	
### Zaklęcia statyczne

## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Znam

* **Miejscowych rolników**:
    * _Aspekty_: wiedza rolnicza, pomoc w ogródku, plotki i ploteczki, 
    * _Opis_: Jako weterynarz zna wszystkich i chętnie z nią rozmawiają podczas wizyt.
* **Weterynarze Myszeczków**:
    * _Aspekty_: konsultacje specjalistyczne, 
    * _Opis_: poznani w trakcie szkolenia weterynarza, podpowiedzą, pomogą... oczywiście, tego samego mogą oczekiwać od niej.
* **Hodowla Myszeczków**:
    * _Aspekty_: główny weterynarz okolicy,
    * _Opis_: 
	
### Mam

* **Artefakty mentalne**:
    * _Aspekty_: ochrona Maskarady, usuwanie pamięci, edycja pamięci
    * _Opis_: 
* **Gabinet weterynaryjny**:
    * _Aspekty_: szeroki zakres różnych narzędzi, leki, lapis, ekranowanie magii
    * _Opis_: 
* **Fundusz inwestycyjny**:
    * _Aspekty_: wolne środki, różne inwestycje, 
    * _Opis_: 



## Inne:


## Motto

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Świnia na portalisku](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|dostała własnego tresowanego glukszwajna z apetytem na magię Harvestera.|Wizja Dukata|
|[Świnia na portalisku](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|pozyskała aktywną i sprawną część Harvestera. Nie jest niebezpieczna, acz jest częścią Harvestera.|Wizja Dukata|
|[Świnia na portalisku](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|zatrudniła sobie Henryka Mordżyna jako pielęgniarza od zwierząt. On wie jak wyglądają działania Myszeczki.|Wizja Dukata|
|[Dlaczego magitrownia działa?](/rpg/inwazja/opowiesci/konspekty/171205-dlaczego-magitrownia-dziala.html)|całkowicie uniezależniła się od sił Sądecznego i sił Dukata. Wykupiła się.|Wizja Dukata|
|[Dlaczego magitrownia działa?](/rpg/inwazja/opowiesci/konspekty/171205-dlaczego-magitrownia-dziala.html)|kupiła dodatkowe akcje Eliksiru Aerinus od Apoloniusza; zwróciły się po czasie i były dobrą inwestycją|Wizja Dukata|

## Plany

|Misja|Plan|Kampania|
|-----|------|------|
|[Świnia na portalisku](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|staje po stronie zwierzątek w sytuacji konfliktu zwierząt i ludzi.|Wizja Dukata|

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|171003|wytresowała glukszwajna i postawiła nieprzytomnego Kociebora; zastawiła pułapkę na glukszwajna i jako weterynarz stała po stronie zwierzątek. Lokalna katalistka, o dziwo.|[Świnia na portalisku](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|11/08/28|11/08/30|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171205|była poszukiwaczka artefaktów i aktualna inwestorka, której umiejętności przydały się niezmiernie przy stabilizacji magitrowni Histogram i dojściu do tego, co jest nie tak.|[Dlaczego magitrownia działa?](/rpg/inwazja/opowiesci/konspekty/171205-dlaczego-magitrownia-dziala.html)|10/12/15|10/12/17|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Robert Sądeczny](/rpg/inwazja/opowiesci/karty-postaci/1709-robert-sadeczny.html)|2|[171003](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html), [171205](/rpg/inwazja/opowiesci/konspekty/171205-dlaczego-magitrownia-dziala.html)|
|[Jakub Dobrocień](/rpg/inwazja/opowiesci/karty-postaci/1709-jakub-dobrocien.html)|2|[171003](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html), [171205](/rpg/inwazja/opowiesci/konspekty/171205-dlaczego-magitrownia-dziala.html)|
|[Tomasz Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-tomasz-myszeczka.html)|1|[171003](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|
|[Ryszard Kota](/rpg/inwazja/opowiesci/karty-postaci/9999-ryszard-kota.html)|1|[171003](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|
|[Paweł Kupiernik](/rpg/inwazja/opowiesci/karty-postaci/9999-pawel-kupiernik.html)|1|[171003](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|
|[Kociebor Dyrygent](/rpg/inwazja/opowiesci/karty-postaci/1709-kociebor-dyrygent.html)|1|[171003](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|
|[Karol Marzyciel](/rpg/inwazja/opowiesci/karty-postaci/1709-karol-marzyciel.html)|1|[171205](/rpg/inwazja/opowiesci/konspekty/171205-dlaczego-magitrownia-dziala.html)|
|[Jolanta Cieśliska](/rpg/inwazja/opowiesci/karty-postaci/9999-jolanta-ciesliska.html)|1|[171003](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|
|[Henryk Mordżyn](/rpg/inwazja/opowiesci/karty-postaci/9999-henryk-mordzyn.html)|1|[171003](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|
|[Bolesław Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-boleslaw-maus.html)|1|[171003](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|
|[Apoloniusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-apoloniusz-bankierz.html)|1|[171205](/rpg/inwazja/opowiesci/konspekty/171205-dlaczego-magitrownia-dziala.html)|

---
layout: inwazja-karta-postaci
categories: profile
title: "Aleksander Sowiński"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|160404|prawdziwy lord Szlachty, który nie wie, że Wiktor zamierza się na jego miejsce.|[The power of cute pet](/rpg/inwazja/opowiesci/konspekty/160404-the-power-of-cute-pet.html)|10/06/25|10/06/26|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160424|który przekazał Andrei poszerzenie uprawnień. Wciągnięty przez Dianę w pułapkę, dał się Spustoszyć i wysadził bombę pryzmatyczną w Świecy Daemonica. KIA.|[Uważaj, o czym marzysz](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|10/06/18|10/06/21|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160417|szef wydziału wewnętrznego na Kopalin który mając Andreę poza Kopalinem próbuje wykorzystać ją najlepiej jak potrafi.|[Symptomy kryzysu Świecy](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|10/06/16|10/06/17|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160411|przełożony Andrei zmartwiony tym co stało się z Oktawianem; chce, by Andrea rozwiązała i wyjaśniła tą kwestię.|[Sekret śmierci na wykopaliskach](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|10/06/14|10/06/15|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160229|szef wydziału wewnętrznego Kopalina i wysoko postawiony członek Szlachty. Przełożony Andrei. Niestety.|[Siedmiu magów - nie Mausów](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html)|10/06/08|10/06/13|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150920|przełożony Wydziału Wewnętrznego na Kopalin, który zlecił Andrei zadanie z Baltazarem Mausem. Tak nagle.|[Sprawa Baltazara Mausa](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|10/05/27|10/05/28|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150913|bezpośredni przełożony Andrei, który zablokował dostęp do danych Oktawiana. Podejrzenie: Szlachta.|[Andrea węszy koło Szlachty](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html)|10/05/19|10/05/20|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150718|przełożony Wydziału Wewnętrznego Srebrnej Świecy ze Śląska, który nie interesuje się tematem KADEM - SŚ ani Szlachtą.|[Splątane tropy: Spustoszenie?](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|10/05/13|10/05/14|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Andrea Wilgacz](/rpg/inwazja/opowiesci/karty-postaci/1709-andrea-wilgacz.html)|7|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html), [160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html), [160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html), [160229](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html), [150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html), [150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html), [150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Ozydiusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-ozydiusz-bankierz.html)|5|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html), [160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html), [160229](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html), [150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html), [150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html)|
|[Marian Agrest](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-agrest.html)|4|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html), [160229](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html), [150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html), [150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Elea Maus](/rpg/inwazja/opowiesci/karty-postaci/1802-elea-maus.html)|4|[160404](/rpg/inwazja/opowiesci/konspekty/160404-the-power-of-cute-pet.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html), [160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html), [150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|
|[Wiktor Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1709-wiktor-sowinski.html)|3|[160404](/rpg/inwazja/opowiesci/konspekty/160404-the-power-of-cute-pet.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html), [150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Tamara Muszkiet](/rpg/inwazja/opowiesci/karty-postaci/1709-tamara-muszkiet.html)|3|[160229](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html), [150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html), [150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Tadeusz Baran](/rpg/inwazja/opowiesci/karty-postaci/1709-tadeusz-baran.html)|3|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html), [160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html), [160229](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html)|
|[Oktawian Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-oktawian-maus.html)|3|[160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html), [150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html), [150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Ernest Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-ernest-maus.html)|3|[160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html), [150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html), [150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Zuzanna Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-zuzanna-maus.html)|2|[160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html), [160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|[Wioletta Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-wioletta-bankierz.html)|2|[160404](/rpg/inwazja/opowiesci/konspekty/160404-the-power-of-cute-pet.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|2|[160404](/rpg/inwazja/opowiesci/konspekty/160404-the-power-of-cute-pet.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Salazar Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-salazar-bankierz.html)|2|[150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html), [150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Sabina Sowińska](/rpg/inwazja/opowiesci/karty-postaci/1709-sabina-sowinska.html)|2|[160404](/rpg/inwazja/opowiesci/konspekty/160404-the-power-of-cute-pet.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Netheria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-netheria-diakon.html)|2|[160404](/rpg/inwazja/opowiesci/konspekty/160404-the-power-of-cute-pet.html), [150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Milena Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-milena-diakon.html)|2|[150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html), [150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html)|
|[Malia Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-malia-bankierz.html)|2|[160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html), [150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|
|[Judyta Karnisz](/rpg/inwazja/opowiesci/karty-postaci/9999-judyta-karnisz.html)|2|[160229](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html), [150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html)|
|[Jan Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-weiner.html)|2|[160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html), [160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|[Infensa Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-infensa-diakon.html)|2|[150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html), [150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Fortitia Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-fortitia-diakon.html)|2|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html), [160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
|[Elizawieta Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-elizawieta-zajcew.html)|2|[160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html), [150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|
|[Dominik Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-dominik-bankierz.html)|2|[160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html), [150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html)|
|[Diana Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-diana-weiner.html)|2|[160404](/rpg/inwazja/opowiesci/konspekty/160404-the-power-of-cute-pet.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Dagmara Wyjątek](/rpg/inwazja/opowiesci/karty-postaci/1709-dagmara-wyjątek.html)|2|[160404](/rpg/inwazja/opowiesci/konspekty/160404-the-power-of-cute-pet.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Baltazar Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-baltazar-maus.html)|2|[160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html), [150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|
|[Aurel Czarko](/rpg/inwazja/opowiesci/karty-postaci/1709-aurel-czarko.html)|2|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html), [150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Artur Żupan](/rpg/inwazja/opowiesci/karty-postaci/1803-artur-zupan.html)|2|[160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html), [150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|
|[Anna Kozak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kozak.html)|2|[160229](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html), [150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html)|
|[Zenon Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-zenon-weiner.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Wojmił Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-wojmil-bankierz.html)|1|[160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|[Wojciech Żądło](/rpg/inwazja/opowiesci/karty-postaci/9999-wojciech-zadlo.html)|1|[160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
|[Tymoteusz Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-tymoteusz-maus.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Sieciech Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-sieciech-bankierz.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Saith Catapult](/rpg/inwazja/opowiesci/karty-postaci/9999-saith-catapult.html)|1|[160229](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html)|
|[Ryszard Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-ryszard-weiner.html)|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Rufus Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-rufus-maus.html)|1|[160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
|[Romeo Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-romeo-diakon.html)|1|[160404](/rpg/inwazja/opowiesci/konspekty/160404-the-power-of-cute-pet.html)|
|[Remigiusz Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-remigiusz-zajcew.html)|1|[150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|
|[Mordecja Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-mordecja-diakon.html)|1|[160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
|[Mirabelka Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-mirabelka-diakon.html)|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Mieszko Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-mieszko-bankierz.html)|1|[160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
|[Marcel Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-marcel-bankierz.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Lidia Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-lidia-weiner.html)|1|[160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
|[Leokadia Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/9999-leokadia-myszeczka.html)|1|[160229](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html)|
|[Laurena Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-laurena-bankierz.html)|1|[160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
|[Laragnarhag](/rpg/inwazja/opowiesci/karty-postaci/1709-laragnarhag.html)|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Krzysztof Wieczorek](/rpg/inwazja/opowiesci/karty-postaci/9999-krzysztof-wieczorek.html)|1|[160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|[Kermit Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-kermit-diakon.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Katalina Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-katalina-bankierz.html)|1|[160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
|[Kajetan Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-kajetan-weiner.html)|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Julian Pszczelak](/rpg/inwazja/opowiesci/karty-postaci/1709-julian-pszczelak.html)|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Jan Anioł Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-aniol-bankierz.html)|1|[160229](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html)|
|[Ireneusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-ireneusz-bankierz.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Irena Resort](/rpg/inwazja/opowiesci/karty-postaci/9999-irena-resort.html)|1|[150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html)|
|[Ignat Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1802-ignat-zajcew.html)|1|[150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Gerwazy Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/9999-gerwazy-myszeczka.html)|1|[160404](/rpg/inwazja/opowiesci/konspekty/160404-the-power-of-cute-pet.html)|
|[Gala Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-gala-zajcew.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Gabriela Resort](/rpg/inwazja/opowiesci/karty-postaci/9999-gabriela-resort.html)|1|[150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html)|
|[Felicjan Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-felicjan-weiner.html)|1|[160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|[Estrella Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-estrella-diakon.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Edward Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-edward-diakon.html)|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Bogdan Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-bogdan-bankierz.html)|1|[150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|
|[Bianka Stein](/rpg/inwazja/opowiesci/karty-postaci/1709-bianka-stein.html)|1|[160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|[Artur Janczecki](/rpg/inwazja/opowiesci/karty-postaci/9999-artur-janczecki.html)|1|[160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|[Antygona Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-antygona-diakon.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Aneta Rainer](/rpg/inwazja/opowiesci/karty-postaci/1709-aneta-rainer.html)|1|[160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|[Amanda Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-amanda-diakon.html)|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Adrian Kropiak](/rpg/inwazja/opowiesci/karty-postaci/9999-adrian-kropiak.html)|1|[150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|
|[Adela Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-adela-maus.html)|1|[150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html)|

---
layout: inwazja-karta-postaci
categories: profile
title: "Barbara Zacieszek"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|160227|higienistka uratowana przez Paulinę przed zostaniem źródłem energii dla Harvestera.|[Zakazany harvester](/rpg/inwazja/opowiesci/konspekty/160227-zakazany-harvester.html)|10/03/12|10/03/13|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|
|160131|higienistka w szkole w Krukowie, którą także Dotknęła efemeryda; Paulina uratowała jej życie przed samobójstwem.|[Dziwny transmiter Weinerów](/rpg/inwazja/opowiesci/konspekty/160131-dziwny-transmiter-weinerow.html)|10/03/10|10/03/11|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|2|[160227](/rpg/inwazja/opowiesci/konspekty/160227-zakazany-harvester.html), [160131](/rpg/inwazja/opowiesci/konspekty/160131-dziwny-transmiter-weinerow.html)|
|[Kajetan Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-kajetan-weiner.html)|2|[160227](/rpg/inwazja/opowiesci/konspekty/160227-zakazany-harvester.html), [160131](/rpg/inwazja/opowiesci/konspekty/160131-dziwny-transmiter-weinerow.html)|
|[Rebeka Czomnik](/rpg/inwazja/opowiesci/karty-postaci/9999-rebeka-czomnik.html)|1|[160227](/rpg/inwazja/opowiesci/konspekty/160227-zakazany-harvester.html)|
|[Onufry Letniczek](/rpg/inwazja/opowiesci/karty-postaci/9999-onufry-letniczek.html)|1|[160131](/rpg/inwazja/opowiesci/konspekty/160131-dziwny-transmiter-weinerow.html)|
|[Maja Stomaniek](/rpg/inwazja/opowiesci/karty-postaci/9999-maja-stomaniek.html)|1|[160227](/rpg/inwazja/opowiesci/konspekty/160227-zakazany-harvester.html)|
|[Kazimierz Przybylec](/rpg/inwazja/opowiesci/karty-postaci/9999-kazimierz-przybylec.html)|1|[160131](/rpg/inwazja/opowiesci/konspekty/160131-dziwny-transmiter-weinerow.html)|
|[Ilona Maczatek](/rpg/inwazja/opowiesci/karty-postaci/9999-ilona-maczatek.html)|1|[160227](/rpg/inwazja/opowiesci/konspekty/160227-zakazany-harvester.html)|
|[Feliks Szczęśliwiec](/rpg/inwazja/opowiesci/karty-postaci/9999-feliks-szczesliwiec.html)|1|[160227](/rpg/inwazja/opowiesci/konspekty/160227-zakazany-harvester.html)|
|[Eustachy Szipinik](/rpg/inwazja/opowiesci/karty-postaci/9999-eustachy-szipinik.html)|1|[160227](/rpg/inwazja/opowiesci/konspekty/160227-zakazany-harvester.html)|
|[Daniel Stryczek](/rpg/inwazja/opowiesci/karty-postaci/9999-daniel-stryczek.html)|1|[160227](/rpg/inwazja/opowiesci/konspekty/160227-zakazany-harvester.html)|
|[Bólokłąb](/rpg/inwazja/opowiesci/karty-postaci/9999-boloklab.html)|1|[160131](/rpg/inwazja/opowiesci/konspekty/160131-dziwny-transmiter-weinerow.html)|

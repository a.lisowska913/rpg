---
layout: inwazja-karta-postaci
categories: profile
title: "Błażej Falka"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|180311|robi koncert dla martwego kralotha, bo Karolina poprosiła. Nie ma innego powodu.|[Cienie procesu Izy](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|10/12/03|10/12/04|[Adaptacja kralotyczna](/rpg/inwazja/opowiesci/konspekty/kampania-adaptacja-kralotyczna.html)|
|161222|KADEMowy 'wordsmith' i śpiewak zarządzający radiowęzłem, którego 'ktoś' zapisał na konkurs. Chce współpracować z innymi.|[Kto wpisał Błażeja do konkursu](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|10/02/04|10/02/07|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Whisperwind](/rpg/inwazja/opowiesci/karty-postaci/9999-whisperwind.html)|2|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html), [161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|2|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html), [161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Marian Łajdak](/rpg/inwazja/opowiesci/karty-postaci/1709-marian-lajdak.html)|2|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html), [161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Lucjan Kopidół](/rpg/inwazja/opowiesci/karty-postaci/1803-lucjan-kopidol.html)|2|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html), [161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Warmaster](/rpg/inwazja/opowiesci/karty-postaci/9999-warmaster.html)|1|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|
|[Netheria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-netheria-diakon.html)|1|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|
|[Mordecja Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-mordecja-diakon.html)|1|[161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Miranda Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-miranda-maus.html)|1|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|
|[Melodia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-melodia-diakon.html)|1|[161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Marek Kromlan](/rpg/inwazja/opowiesci/karty-postaci/1803-marek-kromlan.html)|1|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|
|[Konrad Sowiński](/rpg/inwazja/opowiesci/karty-postaci/9999-konrad-sowinski.html)|1|[161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Karolina Kupiec](/rpg/inwazja/opowiesci/karty-postaci/1803-karolina-kupiec.html)|1|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|
|[Karina Paczulis](/rpg/inwazja/opowiesci/karty-postaci/9999-karina-paczulis.html)|1|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|
|[Julian Krukowicz](/rpg/inwazja/opowiesci/karty-postaci/9999-julian-krukowicz.html)|1|[161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Jolanta Sowińska](/rpg/inwazja/opowiesci/karty-postaci/1709-jolanta-sowinska.html)|1|[161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Infernia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-infernia-diakon.html)|1|[161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Infensa Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-infensa-diakon.html)|1|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|
|[Ignat Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1802-ignat-zajcew.html)|1|[161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|1|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|
|[Artur Żupan](/rpg/inwazja/opowiesci/karty-postaci/1803-artur-zupan.html)|1|[161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Andżelika Leszczyńska](/rpg/inwazja/opowiesci/karty-postaci/1709-andzelika-leszczynska.html)|1|[161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|

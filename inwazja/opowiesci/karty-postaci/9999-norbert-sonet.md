---
layout: inwazja-karta-postaci
categories: profile
title: "Norbert Sonet"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|180526|wezwany przez Silurię jako wsparcie Elei do pomocy w naprawianiu szkód wyrządzonych przez Krystalię. Potem pomagał Elei i Krystalii po akcji terminusów.|[Krystalia poluje na niekralotha](/rpg/inwazja/opowiesci/konspekty/180526-krystalia-poluje-na-niekralotha.html)|10/12/17|10/12/19|[Adaptacja kralotyczna](/rpg/inwazja/opowiesci/konspekty/kampania-adaptacja-kralotyczna.html)|
|170215|lekarz nadzorujący Renatę, wraz z Jankiem złożył dla Silurii mobile suit.|[To się nazywa 'łupy wojenne'?](/rpg/inwazja/opowiesci/konspekty/170215-to-sie-nazywa-lupy-wojenne.html)|10/08/14|10/08/16|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170104|lekarz skwapliwie korzystający z poczynań Silurii i wpuszczający różową mgiełkę zdepresjonowanym * magom Świecy. Mało co go rusza - jest na KADEMie od dawna.|[Spalone generatory pryzmatyczne](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html)|10/08/08|10/08/09|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161207|były mentor Edwina, leczący wszystkich poszkodowanych z Esuriit i sympatyczny jak zawsze, działający przez simulacra|[Lizanie ran na KADEMie](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html)|10/08/05|10/08/07|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150704|pilnujący by synchronizacja Silurii i 0003 została przeprowadzona poprawnie.|[Najskrytszy sekret Tamary](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html)|10/05/11|10/05/12|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150625|zbierający informacje i przejmujący tymczasowo dowodzenia nad sprawami wewnętrznymi KADEMu za zgodą Marty.|[Poligon kluczem do Marcelina?](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html)|10/05/09|10/05/10|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150607|przejął tymczasowe dowodzenie na KADEMie by doprowadzić wszystko do porządku. A nigdy nie chciał dowodzić.|[Brat przeciw bratu](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html)|10/05/07|10/05/08|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150330|główny lekarz KADEMu ratujący pamięć Annalizy po brutalnym ataku.|[Napaść na Annalizę](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|10/04/29|10/04/30|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161218|który zaprosił Whisper na jeden taniec... oraz skupiał się na leczeniu Karoliny po tym, jak glashund ją boleśnie poranił przez Pryzmat.|[Zazdrość Warmastera](/rpg/inwazja/opowiesci/konspekty/161218-zazdrosc-warmastera.html)|10/02/01|10/02/03|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161217|zgodził się na kolację z Whisperwind. Nie żałował. |[Niezbyt legalna 'Academia' Whisperwind](/rpg/inwazja/opowiesci/konspekty/161217-niezbyt-legalna-academia-whisperwind.html)|10/01/30|10/01/31|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|10|[180526](/rpg/inwazja/opowiesci/konspekty/180526-krystalia-poluje-na-niekralotha.html), [170215](/rpg/inwazja/opowiesci/konspekty/170215-to-sie-nazywa-lupy-wojenne.html), [170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html), [161207](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html), [150704](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html), [150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html), [150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html), [150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [161218](/rpg/inwazja/opowiesci/konspekty/161218-zazdrosc-warmastera.html), [161217](/rpg/inwazja/opowiesci/konspekty/161217-niezbyt-legalna-academia-whisperwind.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|6|[170215](/rpg/inwazja/opowiesci/konspekty/170215-to-sie-nazywa-lupy-wojenne.html), [170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html), [161207](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html), [150704](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html), [150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html), [150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html)|
|[Quasar](/rpg/inwazja/opowiesci/karty-postaci/9999-quasar.html)|5|[170215](/rpg/inwazja/opowiesci/konspekty/170215-to-sie-nazywa-lupy-wojenne.html), [170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html), [161207](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html), [150704](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html), [161218](/rpg/inwazja/opowiesci/konspekty/161218-zazdrosc-warmastera.html)|
|[Infernia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-infernia-diakon.html)|4|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html), [150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html), [150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html), [161217](/rpg/inwazja/opowiesci/konspekty/161217-niezbyt-legalna-academia-whisperwind.html)|
|[Ignat Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1802-ignat-zajcew.html)|4|[161207](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html), [150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html), [150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html), [150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Marcel Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-marcel-bankierz.html)|3|[150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html), [150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html), [150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Andżelika Leszczyńska](/rpg/inwazja/opowiesci/karty-postaci/1709-andzelika-leszczynska.html)|3|[161207](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html), [150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [161218](/rpg/inwazja/opowiesci/konspekty/161218-zazdrosc-warmastera.html)|
|[Whisperwind](/rpg/inwazja/opowiesci/karty-postaci/9999-whisperwind.html)|2|[161218](/rpg/inwazja/opowiesci/konspekty/161218-zazdrosc-warmastera.html), [161217](/rpg/inwazja/opowiesci/konspekty/161217-niezbyt-legalna-academia-whisperwind.html)|
|[Warmaster](/rpg/inwazja/opowiesci/karty-postaci/9999-warmaster.html)|2|[150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html), [161218](/rpg/inwazja/opowiesci/konspekty/161218-zazdrosc-warmastera.html)|
|[Tamara Muszkiet](/rpg/inwazja/opowiesci/karty-postaci/1709-tamara-muszkiet.html)|2|[150704](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html), [150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html)|
|[Paweł Sępiak](/rpg/inwazja/opowiesci/karty-postaci/1709-pawel-sepiak.html)|2|[150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html), [150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Mikado Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-mikado-diakon.html)|2|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html), [161207](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html)|
|[Marta Szysznicka](/rpg/inwazja/opowiesci/karty-postaci/9999-marta-szysznicka.html)|2|[161207](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html), [150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Mariusz Trzosik](/rpg/inwazja/opowiesci/karty-postaci/9999-mariusz-trzosik.html)|2|[150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html), [150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html)|
|[Marian Łajdak](/rpg/inwazja/opowiesci/karty-postaci/1709-marian-lajdak.html)|2|[180526](/rpg/inwazja/opowiesci/konspekty/180526-krystalia-poluje-na-niekralotha.html), [150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Marcelin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-marcelin-blakenbauer.html)|2|[150704](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html), [150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html)|
|[Karolina Kupiec](/rpg/inwazja/opowiesci/karty-postaci/1803-karolina-kupiec.html)|2|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html), [161218](/rpg/inwazja/opowiesci/konspekty/161218-zazdrosc-warmastera.html)|
|[Karina Paczulis](/rpg/inwazja/opowiesci/karty-postaci/9999-karina-paczulis.html)|2|[161207](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html), [161217](/rpg/inwazja/opowiesci/konspekty/161217-niezbyt-legalna-academia-whisperwind.html)|
|[Jolanta Sowińska](/rpg/inwazja/opowiesci/karty-postaci/1709-jolanta-sowinska.html)|2|[150704](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html), [150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html)|
|[Ilona Amant](/rpg/inwazja/opowiesci/karty-postaci/9999-ilona-amant.html)|2|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html), [161207](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html)|
|[GS "Aegis" 0003](/rpg/inwazja/opowiesci/karty-postaci/9999-gs-aegis-0003.html)|2|[150704](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html), [150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|2|[170215](/rpg/inwazja/opowiesci/konspekty/170215-to-sie-nazywa-lupy-wojenne.html), [150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html)|
|[Bogumił Rojowiec](/rpg/inwazja/opowiesci/karty-postaci/9999-bogumil-rojowiec.html)|2|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html), [161218](/rpg/inwazja/opowiesci/konspekty/161218-zazdrosc-warmastera.html)|
|[Anna Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-myszeczka.html)|2|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html), [161207](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html)|
|[Wojciech Czapiek](/rpg/inwazja/opowiesci/karty-postaci/9999-wojciech-czapiek.html)|1|[150704](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html)|
|[Wioletta Lemona-Chang](/rpg/inwazja/opowiesci/karty-postaci/9999-wioletta-lemona-chang.html)|1|[161217](/rpg/inwazja/opowiesci/konspekty/161217-niezbyt-legalna-academia-whisperwind.html)|
|[Wiktor Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1709-wiktor-sowinski.html)|1|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Vladlena Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-vladlena-zjacew.html)|1|[150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html)|
|[Sławek Błyszczyk](/rpg/inwazja/opowiesci/karty-postaci/9999-slawek-blyszczyk.html)|1|[161217](/rpg/inwazja/opowiesci/konspekty/161217-niezbyt-legalna-academia-whisperwind.html)|
|[Salazar Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-salazar-bankierz.html)|1|[150704](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html)|
|[Saith Flamecaller](/rpg/inwazja/opowiesci/karty-postaci/9999-saith-flamecaller.html)|1|[150704](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html)|
|[Roman Bruniewicz](/rpg/inwazja/opowiesci/karty-postaci/9999-roman-bruniewicz.html)|1|[180526](/rpg/inwazja/opowiesci/konspekty/180526-krystalia-poluje-na-niekralotha.html)|
|[Roksana Czapiek](/rpg/inwazja/opowiesci/karty-postaci/9999-roksana-czapiek.html)|1|[150704](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html)|
|[Renata Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-renata-maus.html)|1|[170215](/rpg/inwazja/opowiesci/konspekty/170215-to-sie-nazywa-lupy-wojenne.html)|
|[Radosław Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-radoslaw-weiner.html)|1|[161218](/rpg/inwazja/opowiesci/konspekty/161218-zazdrosc-warmastera.html)|
|[Ozydiusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-ozydiusz-bankierz.html)|1|[150704](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html)|
|[Oktawian Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-oktawian-maus.html)|1|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Ofelia Caesar](/rpg/inwazja/opowiesci/karty-postaci/9999-ofelia-caesar.html)|1|[150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html)|
|[Mordecja Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-mordecja-diakon.html)|1|[161218](/rpg/inwazja/opowiesci/konspekty/161218-zazdrosc-warmastera.html)|
|[Marianna Sowińska](/rpg/inwazja/opowiesci/karty-postaci/1709-marianna-sowinska.html)|1|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html)|
|[Marek Kromlan](/rpg/inwazja/opowiesci/karty-postaci/1803-marek-kromlan.html)|1|[180526](/rpg/inwazja/opowiesci/konspekty/180526-krystalia-poluje-na-niekralotha.html)|
|[Maja Błyszczyk](/rpg/inwazja/opowiesci/karty-postaci/9999-maja-blyszczyk.html)|1|[161218](/rpg/inwazja/opowiesci/konspekty/161218-zazdrosc-warmastera.html)|
|[Lucjan Kopidół](/rpg/inwazja/opowiesci/karty-postaci/1803-lucjan-kopidol.html)|1|[150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html)|
|[Liliana Sowińska](/rpg/inwazja/opowiesci/karty-postaci/9999-liliana-sowinska.html)|1|[150704](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html)|
|[Krystalia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-krystalia-diakon.html)|1|[180526](/rpg/inwazja/opowiesci/konspekty/180526-krystalia-poluje-na-niekralotha.html)|
|[Julian Strąk](/rpg/inwazja/opowiesci/karty-postaci/9999-julian-strak.html)|1|[180526](/rpg/inwazja/opowiesci/konspekty/180526-krystalia-poluje-na-niekralotha.html)|
|[Julia Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-julia-weiner.html)|1|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Joachim Kopiec](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-kopiec.html)|1|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Joachim Kartel](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-kartel.html)|1|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Jan Wątły](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-watly.html)|1|[170215](/rpg/inwazja/opowiesci/konspekty/170215-to-sie-nazywa-lupy-wojenne.html)|
|[Infensa Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-infensa-diakon.html)|1|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Hieronim Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-hieronim-maus.html)|1|[150704](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html)|
|[Franciszek Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-maus.html)|1|[161207](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html)|
|[Franciszek Knur](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-knur.html)|1|[180526](/rpg/inwazja/opowiesci/konspekty/180526-krystalia-poluje-na-niekralotha.html)|
|[Ewa Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-ewa-zajcew.html)|1|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Emilia Kołatka](/rpg/inwazja/opowiesci/karty-postaci/1709-emilia-kolatka.html)|1|[150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html)|
|[Elea Maus](/rpg/inwazja/opowiesci/karty-postaci/1802-elea-maus.html)|1|[180526](/rpg/inwazja/opowiesci/konspekty/180526-krystalia-poluje-na-niekralotha.html)|
|[Eis](/rpg/inwazja/opowiesci/karty-postaci/9999-eis.html)|1|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html)|
|[Draconis Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-draconis-diakon.html)|1|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Demon_481](/rpg/inwazja/opowiesci/karty-postaci/9999-demon_481.html)|1|[150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html)|
|[Artur Kotała](/rpg/inwazja/opowiesci/karty-postaci/9999-artur-kotala.html)|1|[161217](/rpg/inwazja/opowiesci/konspekty/161217-niezbyt-legalna-academia-whisperwind.html)|
|[Antygona Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-antygona-diakon.html)|1|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Annaliza Delfin](/rpg/inwazja/opowiesci/karty-postaci/9999-annaliza-delfin.html)|1|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Alisa Wiraż](/rpg/inwazja/opowiesci/karty-postaci/9999-alisa-wiraz.html)|1|[150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html)|
|[Adrian Kropiak](/rpg/inwazja/opowiesci/karty-postaci/9999-adrian-kropiak.html)|1|[161218](/rpg/inwazja/opowiesci/konspekty/161218-zazdrosc-warmastera.html)|
|[Abelard Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-abelard-maus.html)|1|[170215](/rpg/inwazja/opowiesci/konspekty/170215-to-sie-nazywa-lupy-wojenne.html)|

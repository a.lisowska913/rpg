---
layout: inwazja-karta-postaci
categories: profile
title: "Karol Komnat"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170723|elektronik z alergiczną wysypką przez wyciek magii Diany i Hektora mający pewne pobudzenie. Paulinie udało się go nie upokorzyć i dać mu lek na wysypkę.|[Wywalczone życie Diany](/rpg/inwazja/opowiesci/konspekty/170723-wywalczone-zycie-diany.html)|10/02/03|10/02/06|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|
|170702|znajomy elektronik Pauliny. Powiedział, że Diana ma potencjał. Po prostu dobry, rzetelny fachowiec.|[Miłość przez desperację](/rpg/inwazja/opowiesci/konspekty/170702-milosc-przez-desperacje.html)|10/01/29|10/02/02|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Prosperjusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-prosperjusz-bankierz.html)|2|[170723](/rpg/inwazja/opowiesci/konspekty/170723-wywalczone-zycie-diany.html), [170702](/rpg/inwazja/opowiesci/konspekty/170702-milosc-przez-desperacje.html)|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|2|[170723](/rpg/inwazja/opowiesci/konspekty/170723-wywalczone-zycie-diany.html), [170702](/rpg/inwazja/opowiesci/konspekty/170702-milosc-przez-desperacje.html)|
|[Kaja Maślaczek](/rpg/inwazja/opowiesci/karty-postaci/1709-kaja-maslaczek.html)|2|[170723](/rpg/inwazja/opowiesci/konspekty/170723-wywalczone-zycie-diany.html), [170702](/rpg/inwazja/opowiesci/konspekty/170702-milosc-przez-desperacje.html)|
|[Hektor Reszniaczek](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-reszniaczek.html)|2|[170723](/rpg/inwazja/opowiesci/konspekty/170723-wywalczone-zycie-diany.html), [170702](/rpg/inwazja/opowiesci/konspekty/170702-milosc-przez-desperacje.html)|
|[Grazoniusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-grazoniusz-bankierz.html)|2|[170723](/rpg/inwazja/opowiesci/konspekty/170723-wywalczone-zycie-diany.html), [170702](/rpg/inwazja/opowiesci/konspekty/170702-milosc-przez-desperacje.html)|
|[Diana Łuczkiewicz](/rpg/inwazja/opowiesci/karty-postaci/9999-diana-luczkiewicz.html)|2|[170723](/rpg/inwazja/opowiesci/konspekty/170723-wywalczone-zycie-diany.html), [170702](/rpg/inwazja/opowiesci/konspekty/170702-milosc-przez-desperacje.html)|
|[Bogumił Miłoszept](/rpg/inwazja/opowiesci/karty-postaci/9999-bogumil-miloszept.html)|1|[170723](/rpg/inwazja/opowiesci/konspekty/170723-wywalczone-zycie-diany.html)|

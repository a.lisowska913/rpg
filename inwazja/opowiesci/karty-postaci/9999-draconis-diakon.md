---
layout: inwazja-karta-postaci
categories: profile
title: "Draconis Diakon"
---
# {{ page.title }}

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Krzywdzę, bo kocham](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|straszliwy cios w politykę, reputację itp. przez Silgora i Dracenę.|Prawdziwa natura Draceny|


## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170122|pośredniczył w kontakcie pomiędzy Andreą i Amandą. Twardy negocjator, ale chce pomóc Świecy... po prostu, chce silnego Millennium.|[Gambit Anety Rainer](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html)|10/07/19|10/07/23|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170101|dostarczył Andrei wiedzę jak wezwać Arazille używając Ogniwa Arazille z Żonkibora. Też: dostał wiedzę o Irytce (pracuje nad antidotum) i dane z Kompleksu Centralnego by móc zwalczać Karradraela skutecznie.|[Patrol? Kralotyczne maskowanie!](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|10/07/13|10/07/15|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161229|który wymieni Andrei Rafaela i Laragnarhaga na innego * maga Millennium; też, pomoże jej z przywołaniem Arazille|[Presja ze strony Czelimina](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html)|10/07/08|10/07/10|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161120|z ramienia Millennium wymienił informacje z Andreą. Chce współpracować. Sprowadza Andrei na ranek siły jakich ta potrzebuje.|[Tak wygrywa się sojuszami](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html)|10/07/02|10/07/04|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150330|ten, który pomógł zrekonstruować chronologię Spustoszenia Zespołowi; też chroni Dracenę i Antygonę.|[Napaść na Annalizę](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|10/04/29|10/04/30|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150329|ten, który rozwiązał węzeł gordyjski Iza - Zespół i zabrał Antygonę i Dracenę do Millennium, na naprawę.|[Knowania Izy](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html)|10/04/25|10/04/28|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|141119|wytrawny polityk i wybitny terminus Millennium chroniący córkę, lecz nie pozwalający jej na nadmiar swawoli.|[Antygona kontra Dracena](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html)|10/03/27|10/03/29|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|140708|zmieniony kralotycznie terminus. Dołącza do Hektora i Quasar (do walki z Inwazją). Powiedział, że wie coś o Margaret.|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|10/01/21|10/01/22|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Mieszko Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-mieszko-bankierz.html)|5|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html), [170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161229](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html), [140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Andrea Wilgacz](/rpg/inwazja/opowiesci/karty-postaci/1709-andrea-wilgacz.html)|5|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html), [170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161229](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html), [140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Tatiana Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-tatiana-zajcew.html)|4|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html), [170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161229](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html), [140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Marian Łajdak](/rpg/inwazja/opowiesci/karty-postaci/1709-marian-lajdak.html)|4|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html), [150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [141119](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html)|
|[Świeży Lilak](/rpg/inwazja/opowiesci/karty-postaci/9999-swiezy-lilak.html)|3|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html), [170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html)|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|3|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html), [141119](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html)|
|[Rafael Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-rafael-diakon.html)|3|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161229](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html)|
|[Paweł Sępiak](/rpg/inwazja/opowiesci/karty-postaci/1709-pawel-sepiak.html)|3|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html), [141119](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html)|
|[Lidia Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-lidia-weiner.html)|3|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161229](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html)|
|[Julian Pszczelak](/rpg/inwazja/opowiesci/karty-postaci/1709-julian-pszczelak.html)|3|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161229](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|3|[150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html), [141119](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html), [140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Antygona Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-antygona-diakon.html)|3|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html), [141119](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html)|
|[Andżelika Leszczyńska](/rpg/inwazja/opowiesci/karty-postaci/1709-andzelika-leszczynska.html)|3|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html), [150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Wioletta Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-wioletta-bankierz.html)|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161229](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html)|
|[Wiktor Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1709-wiktor-sowinski.html)|2|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [141119](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html)|
|[Tadeusz Baran](/rpg/inwazja/opowiesci/karty-postaci/1709-tadeusz-baran.html)|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html)|
|[Stalowy Śledzik Żarłacz](/rpg/inwazja/opowiesci/karty-postaci/9999-stalowy-sledzik-zarlacz.html)|2|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html), [170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|
|[Rudolf Jankowski](/rpg/inwazja/opowiesci/karty-postaci/9999-rudolf-jankowski.html)|2|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html), [161229](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html)|
|[Oktawian Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-oktawian-maus.html)|2|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html)|
|[Melodia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-melodia-diakon.html)|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161229](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html)|
|[Marian Agrest](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-agrest.html)|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161229](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html)|
|[Laurena Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-laurena-bankierz.html)|2|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html), [170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|
|[Laragnarhag](/rpg/inwazja/opowiesci/karty-postaci/1709-laragnarhag.html)|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161229](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html)|
|[Katalina Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-katalina-bankierz.html)|2|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html), [161229](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html)|
|[Kajetan Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-kajetan-weiner.html)|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html)|
|[Julia Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-julia-weiner.html)|2|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html)|
|[Joachim Kopiec](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-kopiec.html)|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Joachim Kartel](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-kartel.html)|2|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html)|
|[Infensa Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-infensa-diakon.html)|2|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html)|
|[Ignat Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1802-ignat-zajcew.html)|2|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html)|
|[Franciszek Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-myszeczka.html)|2|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html)|
|[Dalia Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-dalia-weiner.html)|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html)|
|[Aneta Rainer](/rpg/inwazja/opowiesci/karty-postaci/1709-aneta-rainer.html)|2|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html)|
|[Amanda Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-amanda-diakon.html)|2|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html), [140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Whisperwind](/rpg/inwazja/opowiesci/karty-postaci/9999-whisperwind.html)|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Wacław Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-waclaw-zajcew.html)|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Vladlena Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-vladlena-zjacew.html)|1|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html)|
|[Teresa Żyraf](/rpg/inwazja/opowiesci/karty-postaci/9999-teresa-zyraf.html)|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Tamara Muszkiet](/rpg/inwazja/opowiesci/karty-postaci/1709-tamara-muszkiet.html)|1|[150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html)|
|[Swietłana Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-swietlana-zajcew.html)|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Sebastian Tecznia](/rpg/inwazja/opowiesci/karty-postaci/9999-sebastian-tecznia.html)|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Salazar Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-salazar-bankierz.html)|1|[150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html)|
|[Rodion Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-rodion-zajcew.html)|1|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|
|[Quasar](/rpg/inwazja/opowiesci/karty-postaci/9999-quasar.html)|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Norbert Sonet](/rpg/inwazja/opowiesci/karty-postaci/9999-norbert-sonet.html)|1|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Mikado Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-mikado-diakon.html)|1|[141119](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html)|
|[Marta Szysznicka](/rpg/inwazja/opowiesci/karty-postaci/9999-marta-szysznicka.html)|1|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Marcelin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-marcelin-blakenbauer.html)|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Marcel Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-marcel-bankierz.html)|1|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Malwina Krówka](/rpg/inwazja/opowiesci/karty-postaci/9999-malwina-krowka.html)|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Lucjan Kopidół](/rpg/inwazja/opowiesci/karty-postaci/1803-lucjan-kopidol.html)|1|[141119](/rpg/inwazja/opowiesci/konspekty/141119-antygona-kontra-dracena.html)|
|[Krystalia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-krystalia-diakon.html)|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Kirył Sjeld](/rpg/inwazja/opowiesci/karty-postaci/9999-kiryl-sjeld.html)|1|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html)|
|[Karradrael](/rpg/inwazja/opowiesci/karty-postaci/9999-karradrael.html)|1|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html)|
|[Irina Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-irina-zajcew.html)|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Ika](/rpg/inwazja/opowiesci/karty-postaci/9999-ika.html)|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Grzegorz Czerwiec](/rpg/inwazja/opowiesci/karty-postaci/9999-grzegorz-czerwiec.html)|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[GS "Aegis" 0003](/rpg/inwazja/opowiesci/karty-postaci/9999-gs-aegis-0003.html)|1|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Ewa Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-ewa-zajcew.html)|1|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Estera Piryt](/rpg/inwazja/opowiesci/karty-postaci/9999-estera-piryt.html)|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Deiiw Podniebny Grom](/rpg/inwazja/opowiesci/karty-postaci/9999-deiiw-podniebny-grom.html)|1|[161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html)|
|[Dagmara Wyjątek](/rpg/inwazja/opowiesci/karty-postaci/1709-dagmara-wyjątek.html)|1|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|
|[Bianka Stein](/rpg/inwazja/opowiesci/karty-postaci/1709-bianka-stein.html)|1|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html)|
|[Arazille](/rpg/inwazja/opowiesci/karty-postaci/9999-arazille.html)|1|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html)|
|[Annaliza Delfin](/rpg/inwazja/opowiesci/karty-postaci/9999-annaliza-delfin.html)|1|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Anna Kozak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kozak.html)|1|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|
|[Anastazja Sjeld](/rpg/inwazja/opowiesci/karty-postaci/9999-anastazja-sjeld.html)|1|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html)|
|[Alojzy Przylaz](/rpg/inwazja/opowiesci/karty-postaci/1803-alojzy-przylaz.html)|1|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|

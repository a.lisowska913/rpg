---
layout: inwazja-karta-postaci
categories: profile
title: "Paweł Grzęda"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|140114|pieniacz nienawidzący Hektora Blakenbauera (bo odrzuca mu pozwy) więc zwinął psa Hektorowi. Czarny charakter sesji!|[Zaginiony członek](/rpg/inwazja/opowiesci/konspekty/140114-zaginiony-czlonek.html)|10/01/07|10/01/08|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Waldemar Zupaczka](/rpg/inwazja/opowiesci/karty-postaci/9999-waldemar-zupaczka.html)|1|[140114](/rpg/inwazja/opowiesci/konspekty/140114-zaginiony-czlonek.html)|
|[Marcelin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-marcelin-blakenbauer.html)|1|[140114](/rpg/inwazja/opowiesci/konspekty/140114-zaginiony-czlonek.html)|
|[Karol Poczciwiec](/rpg/inwazja/opowiesci/karty-postaci/9999-karol-poczciwiec.html)|1|[140114](/rpg/inwazja/opowiesci/konspekty/140114-zaginiony-czlonek.html)|
|[Jan Szczupak](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-szczupak.html)|1|[140114](/rpg/inwazja/opowiesci/konspekty/140114-zaginiony-czlonek.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|1|[140114](/rpg/inwazja/opowiesci/konspekty/140114-zaginiony-czlonek.html)|
|[Borys Kumin](/rpg/inwazja/opowiesci/karty-postaci/1709-borys-kumin.html)|1|[140114](/rpg/inwazja/opowiesci/konspekty/140114-zaginiony-czlonek.html)|
|[Bolesław Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-boleslaw-bankierz.html)|1|[140114](/rpg/inwazja/opowiesci/konspekty/140114-zaginiony-czlonek.html)|
|[Andrea Wilgacz](/rpg/inwazja/opowiesci/karty-postaci/1709-andrea-wilgacz.html)|1|[140114](/rpg/inwazja/opowiesci/konspekty/140114-zaginiony-czlonek.html)|
|[Amelia Eter](/rpg/inwazja/opowiesci/karty-postaci/9999-amelia-eter.html)|1|[140114](/rpg/inwazja/opowiesci/konspekty/140114-zaginiony-czlonek.html)|

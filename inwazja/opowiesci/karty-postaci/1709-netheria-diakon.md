---
layout: inwazja-karta-postaci
categories: profile
factions: "Millennium"
type: "NPC"
title: "Netheria Diakon"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **FIL: obrońca**: 
    * _Aspekty_: 
    * _Opis_: "Każdy ma prawo do spokoju - a zwłaszcza ludzie od magów"

### Umiejętności

* **Mistrz szpiegów**:
    * _Aspekty_: 
    * _Opis_: 
* **Biznesman**: 
    * _Aspekty_: negocjacje biznesowe,
    * _Opis_: 
* **Imprezowicz**:
    * _Aspekty_: dusza imprezy, czytanie nastrojów, organizowanie imprez
    * _Opis_: 
    
### Silne i słabe strony:

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

## Magia

### Szkoły magiczne

* **Magia mentalna**:
    * _Aspekty_: astralika 
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

### Zaklęcia statyczne

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 

## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Zarobki

* ?

### Znam

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

### Mam

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

# Opis

Netheria Diakon jest zakochana w swojej pracy - organizowanie imprez, łączenie ludzi ze sobą, maksymalizacja radości i sprawienie by wszyscy dobrze się bawili.
Czarodziejka ta ma bardzo dobre serce (w gruncie rzeczy dobra dusza) i pomaga z własnej inicjatywy wszystkim dookoła, czy to są ludzie czy magowie. Zupełnie nie wygląda na swój wiek (niespodzianka: Diakonka); dają jej koło 25 i nie zadają sobie pytań, skąd Netheria ma tyle kontaktów i potrafi połączyć ze sobą tyle ludzi i stron. Naprawdę ma 35 lat. Jako, że nie ma żadnych przeciwwskazań do używania swego ciała jako argumentu, zazwyczaj dostaje czego chce. Do tego jest nadspodziewanie uczciwa.
Ogromną zaletą Netherii jest jej świetna pamięć i umiejętność doskonałego kojarzenia faktów.
Ma jakieś tajemnicze hobby, którego nie ujawnia na lewo i prawo.
Lubi ludzi i magów. Nie lubi samotności.

### Koncept

Wysokopoziomowy koncept postaci, 1-2 zdania.

### Motto

"Tekst"

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Oblicze guwernantki](/rpg/inwazja/opowiesci/konspekty/160819-oblicze-guwernantki.html)|dostaje kontakty Hektora w świecie ludzi|Powrót Karradraela|
|[Oblicze guwernantki](/rpg/inwazja/opowiesci/konspekty/160819-oblicze-guwernantki.html)|dostaje usługi Aliny i Dionizego wtedy, gdy ci nie są zajęci i gdy nie koliduje to z celami Blakenbauerów|Powrót Karradraela|
|[Na żywym organiźmie Draceny...](/rpg/inwazja/opowiesci/konspekty/160910-na-zywym-organizmie-draceny.html)|okazała się być wpływowa w Millennium; podobno jest mistrzynią szpiegów|Prawdziwa natura Draceny|


## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|180318|przyjmie z przyjemnością Marię do Millennium, by jej pomóc zregenerować życie w społeczeństwie.|[Czyżby drugi kraloth?](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html)|10/12/05|10/12/07|[Adaptacja kralotyczna](/rpg/inwazja/opowiesci/konspekty/kampania-adaptacja-kralotyczna.html)|
|180311|pomaga Silurii znaleźć ekspertów od chorób kralothów. Stanęło na Melodii i Laragnarhagu.|[Cienie procesu Izy](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|10/12/03|10/12/04|[Adaptacja kralotyczna](/rpg/inwazja/opowiesci/konspekty/kampania-adaptacja-kralotyczna.html)|
|180310|która nic nie wie o dzikich kralothach w okolicach Kotów. Zainteresowana tematem nie mniej niż Siluria.|[Kraloth w piwnicy](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|10/11/28|10/12/01|[Adaptacja kralotyczna](/rpg/inwazja/opowiesci/konspekty/kampania-adaptacja-kralotyczna.html)|
|160819|która pozornie pogodziła się ze stratą Wandy i zgodziła się działać z Blakenbauerami przeciwko The Governess. Detektyw obrazów z przeszłości.|[Oblicze guwernantki](/rpg/inwazja/opowiesci/konspekty/160819-oblicze-guwernantki.html)|10/07/12|10/07/14|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160918|organizująca najważniejszą imprezę w okolicy, załatwiająca odpowiednie dokumenty i przesypiająca kluczowy fragment sytuacji.|[Walka o duszę Draceny](/rpg/inwazja/opowiesci/konspekty/160918-walka-o-dusze-draceny.html)|10/06/30|10/07/03|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|
|160910|pokazująca swoją mroczniejszą stronę i próbująca wyjaśnić Paulinie co się dzieje z Draceną by ratować dziewczynę|[Na żywym organiźmie Draceny...](/rpg/inwazja/opowiesci/konspekty/160910-na-zywym-organizmie-draceny.html)|10/06/27|10/06/29|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|
|160907|ściągnęta przez Paulinę na pomoc Dracenie i zdradzająca kilka sekretów z przeszłości Draceny.|[Dracena... Blakenbauer?](/rpg/inwazja/opowiesci/konspekty/160907-dracena-blakenbauer.html)|10/06/25|10/06/26|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|
|160404|która zapewnia możliwość bezpiecznego spotkania Silurii z resztą świata. The spymistress.|[The power of cute pet](/rpg/inwazja/opowiesci/konspekty/160404-the-power-of-cute-pet.html)|10/06/25|10/06/26|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160403|koordynująca dla Silurii rzeczy z ramienia Diakonów. Uratowała * magów KADEMu i służy jako kontakt Silurii.|[Wiktor kontra Kadem i Świeca](/rpg/inwazja/opowiesci/konspekty/160403-wiktor-kontra-kadem-i-swieca.html)|10/06/23|10/06/24|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160224|obserwująca Wandę uważnie i cichy świadek jej porwania z "Działa Plazmowego".|[Aniołki Marcelina](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|10/06/23|10/06/24|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|151110|która skontaktowała Alinę z Romeem; Alina nie poszła z nią do teatru.|[Romeo i... Hektor](/rpg/inwazja/opowiesci/konspekty/151110-romeo-i-hektor.html)|10/05/31|10/06/01|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150826|która nie wierzy że młody Maus mógł wymyślić taką intrygę. Chce pomóc Paulinie, ale też się odbija od konfliktów międzygildiowych.|[Pętla dookoła Pauliny](/rpg/inwazja/opowiesci/konspekty/150826-petla-dookola-pauliny.html)|10/05/25|10/05/26|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150823|która udowodniła, że czarodziejka imprezowa też może być bohaterką. Ukochana Estrelli.|[Atak na sanktuarium Estrelli](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|10/05/23|10/05/24|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150718|która postawiła się kralothom oraz części rodu, by uniemożliwić sojusze takie jak Lugardhair - Aster.|[Splątane tropy: Spustoszenie?](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|10/05/13|10/05/14|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|141115|Mistrzyni Ceremonii; nekromantka i Diakonka do wynajęcia (kanonicznie: SŚ) chcąca wprowadzić Imprezę Życia. Spustoszona.|[GS Aegis 0002](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|10/04/13|10/04/14|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170115|która załatwiła dla Silurii za KADEMowe pieniądze występ dla Oliwiera w Rzecznej Chacie by zamknąć wreszcie temat sporu KADEM / Świeca.|[Klub Dare Shiver](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|10/02/22|10/02/25|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150602|mastermind akcji i osoba pokazująca, że każdy może być czarnym koniem. Ranna po infiltracji hotelu 'Luksus'.|[Esme, najemniczka Netherii](/rpg/inwazja/opowiesci/konspekty/150602-esme-najemniczka-netherii.html)|10/02/19|10/02/20|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|
|170111|ściągnięta przez Silurię by kaskadować imprezy które mają wymęczyć Balroga Bankierza (by nie wygrał z Ignatem zbyt mocno)|[EIS na kozetce](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|10/02/18|10/02/21|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150104|która wybrała Andromedę nad wysokopoziomowe plany Millennium. Lojalna ideałom, nie polityce.|[Terminus-defiler, kapłan Arazille](/rpg/inwazja/opowiesci/konspekty/150104-terminus-defiler-kaplan-arazille.html)|10/02/17|10/02/18|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|
|150103|która wpierw jest odbita przez policję, potem wyciąga wieści z Augusta a potem okazuje się, że jest w niej więcej niż się zdawało.|[Pryzmat Myśli pęka](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html)|10/02/15|10/02/16|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|
|141230|której jeden telefon Andromedy zniszczył subtelny plan i wpakował ją w OGROMNE kłopoty.|[Ofiara z wampira dla Arazille](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html)|10/02/13|10/02/14|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|
|141227|tu: Nikola Dorsz; czarodziejka i tymczasowa opiekunka Luksji. Która zniknęła w tajemniczych okolicznościach na Festiwalu.|[Przyczajona Andromeda, ukryty Maus](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|10/02/11|10/02/12|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|
|150121|czarodziejka Millennium której Konrad powiedział przez telefon o stanie Marty Newy (co widział w lustrze).|[Nowe życie Aliny](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|10/01/15|10/01/16|[Blakenbauerowie x Skorpion](/rpg/inwazja/opowiesci/konspekty/kampania-blakenbauerowie-x-skorpion.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|8|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html), [180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html), [180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html), [160404](/rpg/inwazja/opowiesci/konspekty/160404-the-power-of-cute-pet.html), [160403](/rpg/inwazja/opowiesci/konspekty/160403-wiktor-kontra-kadem-i-swieca.html), [141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html), [170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Estrella Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-estrella-diakon.html)|6|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html), [160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html), [150826](/rpg/inwazja/opowiesci/konspekty/150826-petla-dookola-pauliny.html), [150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html), [150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html), [141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|6|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html), [180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html), [180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html), [160819](/rpg/inwazja/opowiesci/konspekty/160819-oblicze-guwernantki.html), [151110](/rpg/inwazja/opowiesci/konspekty/151110-romeo-i-hektor.html), [150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|5|[160918](/rpg/inwazja/opowiesci/konspekty/160918-walka-o-dusze-draceny.html), [160910](/rpg/inwazja/opowiesci/konspekty/160910-na-zywym-organizmie-draceny.html), [160907](/rpg/inwazja/opowiesci/konspekty/160907-dracena-blakenbauer.html), [150826](/rpg/inwazja/opowiesci/konspekty/150826-petla-dookola-pauliny.html), [150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|
|[Sandra Stryjek](/rpg/inwazja/opowiesci/karty-postaci/1709-sandra-stryjek.html)|4|[150104](/rpg/inwazja/opowiesci/konspekty/150104-terminus-defiler-kaplan-arazille.html), [150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Luksja Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-luksja-diakon.html)|4|[150602](/rpg/inwazja/opowiesci/konspekty/150602-esme-najemniczka-netherii.html), [150104](/rpg/inwazja/opowiesci/konspekty/150104-terminus-defiler-kaplan-arazille.html), [150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Kasia Nowak](/rpg/inwazja/opowiesci/karty-postaci/1803-kasia-nowak.html)|4|[150104](/rpg/inwazja/opowiesci/konspekty/150104-terminus-defiler-kaplan-arazille.html), [150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Karolina Kupiec](/rpg/inwazja/opowiesci/karty-postaci/1803-karolina-kupiec.html)|4|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html), [180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html), [170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Iliusitius](/rpg/inwazja/opowiesci/karty-postaci/9999-iliusitius.html)|4|[150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html), [150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Herbert Zioło](/rpg/inwazja/opowiesci/karty-postaci/1709-herbert-ziolo.html)|4|[150104](/rpg/inwazja/opowiesci/konspekty/150104-terminus-defiler-kaplan-arazille.html), [150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Gabriel Newa](/rpg/inwazja/opowiesci/karty-postaci/9999-gabriel-newa.html)|4|[150602](/rpg/inwazja/opowiesci/konspekty/150602-esme-najemniczka-netherii.html), [150104](/rpg/inwazja/opowiesci/konspekty/150104-terminus-defiler-kaplan-arazille.html), [150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|4|[160918](/rpg/inwazja/opowiesci/konspekty/160918-walka-o-dusze-draceny.html), [160910](/rpg/inwazja/opowiesci/konspekty/160910-na-zywym-organizmie-draceny.html), [160907](/rpg/inwazja/opowiesci/konspekty/160907-dracena-blakenbauer.html), [150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Arazille](/rpg/inwazja/opowiesci/karty-postaci/9999-arazille.html)|4|[150104](/rpg/inwazja/opowiesci/konspekty/150104-terminus-defiler-kaplan-arazille.html), [150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Wojciech Kajak](/rpg/inwazja/opowiesci/karty-postaci/9999-wojciech-kajak.html)|3|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Wiktor Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1709-wiktor-sowinski.html)|3|[160404](/rpg/inwazja/opowiesci/konspekty/160404-the-power-of-cute-pet.html), [160403](/rpg/inwazja/opowiesci/konspekty/160403-wiktor-kontra-kadem-i-swieca.html), [150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Patryk Romczak](/rpg/inwazja/opowiesci/karty-postaci/9999-patryk-romczak.html)|3|[150602](/rpg/inwazja/opowiesci/konspekty/150602-esme-najemniczka-netherii.html), [150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Marian Łajdak](/rpg/inwazja/opowiesci/karty-postaci/1709-marian-lajdak.html)|3|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html), [160403](/rpg/inwazja/opowiesci/konspekty/160403-wiktor-kontra-kadem-i-swieca.html), [141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
|[Marek Kromlan](/rpg/inwazja/opowiesci/karty-postaci/1803-marek-kromlan.html)|3|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html), [180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html), [180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Kinga Toczek](/rpg/inwazja/opowiesci/karty-postaci/9999-kinga-toczek.html)|3|[160918](/rpg/inwazja/opowiesci/konspekty/160918-walka-o-dusze-draceny.html), [160910](/rpg/inwazja/opowiesci/konspekty/160910-na-zywym-organizmie-draceny.html), [160907](/rpg/inwazja/opowiesci/konspekty/160907-dracena-blakenbauer.html)|
|[Kermit Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-kermit-diakon.html)|3|[150826](/rpg/inwazja/opowiesci/konspekty/150826-petla-dookola-pauliny.html), [150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html), [150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Judyta Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-judyta-maus.html)|3|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html), [180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html), [170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|[Feliks Hanson](/rpg/inwazja/opowiesci/karty-postaci/9999-feliks-hanson.html)|3|[150104](/rpg/inwazja/opowiesci/konspekty/150104-terminus-defiler-kaplan-arazille.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Feliks Bozur](/rpg/inwazja/opowiesci/karty-postaci/9999-feliks-bozur.html)|3|[150104](/rpg/inwazja/opowiesci/konspekty/150104-terminus-defiler-kaplan-arazille.html), [150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html)|
|[Elea Maus](/rpg/inwazja/opowiesci/karty-postaci/1802-elea-maus.html)|3|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html), [180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html), [160404](/rpg/inwazja/opowiesci/konspekty/160404-the-power-of-cute-pet.html)|
|[Anna Kajak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kajak.html)|3|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Alina Bednarz](/rpg/inwazja/opowiesci/karty-postaci/1803-alina-bednarz.html)|3|[160819](/rpg/inwazja/opowiesci/konspekty/160819-oblicze-guwernantki.html), [151110](/rpg/inwazja/opowiesci/konspekty/151110-romeo-i-hektor.html), [150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Wioletta Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-wioletta-bankierz.html)|2|[160404](/rpg/inwazja/opowiesci/konspekty/160404-the-power-of-cute-pet.html), [160403](/rpg/inwazja/opowiesci/konspekty/160403-wiktor-kontra-kadem-i-swieca.html)|
|[Whisperwind](/rpg/inwazja/opowiesci/karty-postaci/9999-whisperwind.html)|2|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html), [180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|
|[Tymotheus Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-tymotheus-blakenbauer.html)|2|[160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html), [150602](/rpg/inwazja/opowiesci/konspekty/150602-esme-najemniczka-netherii.html)|
|[Szymon Skubny](/rpg/inwazja/opowiesci/karty-postaci/9999-szymon-skubny.html)|2|[151110](/rpg/inwazja/opowiesci/konspekty/151110-romeo-i-hektor.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Samira Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-samira-diakon.html)|2|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Sabina Sowińska](/rpg/inwazja/opowiesci/karty-postaci/1709-sabina-sowinska.html)|2|[160404](/rpg/inwazja/opowiesci/konspekty/160404-the-power-of-cute-pet.html), [160403](/rpg/inwazja/opowiesci/konspekty/160403-wiktor-kontra-kadem-i-swieca.html)|
|[Romeo Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-romeo-diakon.html)|2|[160404](/rpg/inwazja/opowiesci/konspekty/160404-the-power-of-cute-pet.html), [151110](/rpg/inwazja/opowiesci/konspekty/151110-romeo-i-hektor.html)|
|[Robert Przerot](/rpg/inwazja/opowiesci/karty-postaci/9999-robert-przerot.html)|2|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Rafał Szczęślik](/rpg/inwazja/opowiesci/karty-postaci/9999-rafal-szczeslik.html)|2|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Quasar](/rpg/inwazja/opowiesci/karty-postaci/9999-quasar.html)|2|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Paweł Sępiak](/rpg/inwazja/opowiesci/karty-postaci/1709-pawel-sepiak.html)|2|[141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Mirabelka Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-mirabelka-diakon.html)|2|[150602](/rpg/inwazja/opowiesci/konspekty/150602-esme-najemniczka-netherii.html), [150104](/rpg/inwazja/opowiesci/konspekty/150104-terminus-defiler-kaplan-arazille.html)|
|[Marysia Kiras](/rpg/inwazja/opowiesci/karty-postaci/9999-marysia-kiras.html)|2|[141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html), [150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Maria Przysiadek](/rpg/inwazja/opowiesci/karty-postaci/9999-maria-przysiadek.html)|2|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html), [180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/1709-maria-newa.html)|2|[150826](/rpg/inwazja/opowiesci/konspekty/150826-petla-dookola-pauliny.html), [150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|
|[Marcelin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-marcelin-blakenbauer.html)|2|[160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html), [151110](/rpg/inwazja/opowiesci/konspekty/151110-romeo-i-hektor.html)|
|[Lucjan Kopidół](/rpg/inwazja/opowiesci/karty-postaci/1803-lucjan-kopidol.html)|2|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html), [180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|
|[Konrad Węgorz](/rpg/inwazja/opowiesci/karty-postaci/9999-konrad-wegorz.html)|2|[150602](/rpg/inwazja/opowiesci/konspekty/150602-esme-najemniczka-netherii.html), [150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Klara Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-klara-blakenbauer.html)|2|[160819](/rpg/inwazja/opowiesci/konspekty/160819-oblicze-guwernantki.html), [160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|
|[Karradrael](/rpg/inwazja/opowiesci/karty-postaci/9999-karradrael.html)|2|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Józef Pimczak](/rpg/inwazja/opowiesci/karty-postaci/9999-jozef-pimczak.html)|2|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Jolanta Sowińska](/rpg/inwazja/opowiesci/karty-postaci/1709-jolanta-sowinska.html)|2|[150826](/rpg/inwazja/opowiesci/konspekty/150826-petla-dookola-pauliny.html), [170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|[Jerzy Pasznik](/rpg/inwazja/opowiesci/karty-postaci/9999-jerzy-pasznik.html)|2|[160918](/rpg/inwazja/opowiesci/konspekty/160918-walka-o-dusze-draceny.html), [160910](/rpg/inwazja/opowiesci/konspekty/160910-na-zywym-organizmie-draceny.html)|
|[Infensa Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-infensa-diakon.html)|2|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html), [150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Ignat Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1802-ignat-zajcew.html)|2|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Hralglanath](/rpg/inwazja/opowiesci/karty-postaci/9999-hralglanath.html)|2|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html), [180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|2|[160819](/rpg/inwazja/opowiesci/konspekty/160819-oblicze-guwernantki.html), [151110](/rpg/inwazja/opowiesci/konspekty/151110-romeo-i-hektor.html)|
|[Gerwazy Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/9999-gerwazy-myszeczka.html)|2|[160404](/rpg/inwazja/opowiesci/konspekty/160404-the-power-of-cute-pet.html), [160403](/rpg/inwazja/opowiesci/konspekty/160403-wiktor-kontra-kadem-i-swieca.html)|
|[Filip Sztukar](/rpg/inwazja/opowiesci/karty-postaci/9999-filip-sztukar.html)|2|[141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html), [150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Elizawieta Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-elizawieta-zajcew.html)|2|[160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html), [170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|[Dionizy Kret](/rpg/inwazja/opowiesci/karty-postaci/1709-dionizy-kret.html)|2|[160819](/rpg/inwazja/opowiesci/konspekty/160819-oblicze-guwernantki.html), [151110](/rpg/inwazja/opowiesci/konspekty/151110-romeo-i-hektor.html)|
|[Diana Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-diana-weiner.html)|2|[160404](/rpg/inwazja/opowiesci/konspekty/160404-the-power-of-cute-pet.html), [151110](/rpg/inwazja/opowiesci/konspekty/151110-romeo-i-hektor.html)|
|[Dagmara Wyjątek](/rpg/inwazja/opowiesci/karty-postaci/1709-dagmara-wyjątek.html)|2|[160404](/rpg/inwazja/opowiesci/konspekty/160404-the-power-of-cute-pet.html), [160403](/rpg/inwazja/opowiesci/konspekty/160403-wiktor-kontra-kadem-i-swieca.html)|
|[Bójka Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-bojka-diakon.html)|2|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html), [180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Borys Kumin](/rpg/inwazja/opowiesci/karty-postaci/1709-borys-kumin.html)|2|[160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html), [150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Bogumił Rojowiec](/rpg/inwazja/opowiesci/karty-postaci/9999-bogumil-rojowiec.html)|2|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Balrog Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-balrog-bankierz.html)|2|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[August Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-august-bankierz.html)|2|[150104](/rpg/inwazja/opowiesci/konspekty/150104-terminus-defiler-kaplan-arazille.html), [150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html)|
|[Artur Żupan](/rpg/inwazja/opowiesci/karty-postaci/1803-artur-zupan.html)|2|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html), [150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Aneta Hanson](/rpg/inwazja/opowiesci/karty-postaci/9999-aneta-hanson.html)|2|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Andrzej Szop](/rpg/inwazja/opowiesci/karty-postaci/9999-andrzej-szop.html)|2|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Aleksander Sowiński](/rpg/inwazja/opowiesci/karty-postaci/9999-aleksander-sowinski.html)|2|[160404](/rpg/inwazja/opowiesci/konspekty/160404-the-power-of-cute-pet.html), [150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Zofia Perszen](/rpg/inwazja/opowiesci/karty-postaci/9999-zofia-perszen.html)|1|[150602](/rpg/inwazja/opowiesci/konspekty/150602-esme-najemniczka-netherii.html)|
|[Zenon Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-zenon-weiner.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Włodzimierz Jamnik](/rpg/inwazja/opowiesci/karty-postaci/9999-wlodzimierz-jamnik.html)|1|[160918](/rpg/inwazja/opowiesci/konspekty/160918-walka-o-dusze-draceny.html)|
|[Warmaster](/rpg/inwazja/opowiesci/karty-postaci/9999-warmaster.html)|1|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|
|[Urszula Murczyk](/rpg/inwazja/opowiesci/karty-postaci/1709-urszula-murczyk.html)|1|[170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Tymoteusz Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-tymoteusz-maus.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Tatiana Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-tatiana-zajcew.html)|1|[151110](/rpg/inwazja/opowiesci/konspekty/151110-romeo-i-hektor.html)|
|[Tamara Muszkiet](/rpg/inwazja/opowiesci/karty-postaci/1709-tamara-muszkiet.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Tadeusz Umiej](/rpg/inwazja/opowiesci/karty-postaci/9999-tadeusz-umiej.html)|1|[150602](/rpg/inwazja/opowiesci/konspekty/150602-esme-najemniczka-netherii.html)|
|[Szczepan Przysiadek](/rpg/inwazja/opowiesci/karty-postaci/9999-szczepan-przysiadek.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Staszek Perszen](/rpg/inwazja/opowiesci/karty-postaci/9999-staszek-perszen.html)|1|[150602](/rpg/inwazja/opowiesci/konspekty/150602-esme-najemniczka-netherii.html)|
|[Stanisław Pormien](/rpg/inwazja/opowiesci/karty-postaci/9999-stanislaw-pormien.html)|1|[160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|
|[Sieciech Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-sieciech-bankierz.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Sebastian Tecznia](/rpg/inwazja/opowiesci/karty-postaci/9999-sebastian-tecznia.html)|1|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Sebastian Linka](/rpg/inwazja/opowiesci/karty-postaci/9999-sebastian-linka.html)|1|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html)|
|[Salazar Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-salazar-bankierz.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Roman Gieroj](/rpg/inwazja/opowiesci/karty-postaci/9999-roman-gieroj.html)|1|[150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|
|[Roman Bruniewicz](/rpg/inwazja/opowiesci/karty-postaci/9999-roman-bruniewicz.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Robert Mięk](/rpg/inwazja/opowiesci/karty-postaci/9999-robert-miek.html)|1|[141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
|[Paulina Widoczek](/rpg/inwazja/opowiesci/karty-postaci/9999-paulina-widoczek.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Patryk Kloszard](/rpg/inwazja/opowiesci/karty-postaci/9999-patryk-kloszard.html)|1|[151110](/rpg/inwazja/opowiesci/konspekty/151110-romeo-i-hektor.html)|
|[Ozydiusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-ozydiusz-bankierz.html)|1|[150826](/rpg/inwazja/opowiesci/konspekty/150826-petla-dookola-pauliny.html)|
|[Otton Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-otton-blakenbauer.html)|1|[160819](/rpg/inwazja/opowiesci/konspekty/160819-oblicze-guwernantki.html)|
|[Olga Miodownik](/rpg/inwazja/opowiesci/karty-postaci/1709-olga-miodownik.html)|1|[150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|
|[Olaf Rajczak](/rpg/inwazja/opowiesci/karty-postaci/9999-olaf-rajczak.html)|1|[141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
|[Oktawian Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-oktawian-maus.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Najada Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-najada-diakon.html)|1|[160918](/rpg/inwazja/opowiesci/konspekty/160918-walka-o-dusze-draceny.html)|
|[Mojra](/rpg/inwazja/opowiesci/karty-postaci/9999-mojra.html)|1|[151110](/rpg/inwazja/opowiesci/konspekty/151110-romeo-i-hektor.html)|
|[Miranda Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-miranda-maus.html)|1|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|
|[Michał Czuk](/rpg/inwazja/opowiesci/karty-postaci/9999-michal-czuk.html)|1|[141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Melodia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-melodia-diakon.html)|1|[170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Małgorzata Poran](/rpg/inwazja/opowiesci/karty-postaci/9999-malgorzata-poran.html)|1|[141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Mateusz Tykwa](/rpg/inwazja/opowiesci/karty-postaci/9999-mateusz-tykwa.html)|1|[150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|
|[Marta Newa](/rpg/inwazja/opowiesci/karty-postaci/9999-marta-newa.html)|1|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Marian Agrest](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-agrest.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Margaret Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-margaret-blakenbauer.html)|1|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Marcin Puczek](/rpg/inwazja/opowiesci/karty-postaci/9999-marcin-puczek.html)|1|[160918](/rpg/inwazja/opowiesci/konspekty/160918-walka-o-dusze-draceny.html)|
|[Marcel Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-marcel-bankierz.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Maciej Tykwa](/rpg/inwazja/opowiesci/karty-postaci/9999-maciej-tykwa.html)|1|[150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|
|[Maciej Dworek](/rpg/inwazja/opowiesci/karty-postaci/9999-maciej-dworek.html)|1|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html)|
|[Luiza Wanta](/rpg/inwazja/opowiesci/karty-postaci/9999-luiza-wanta.html)|1|[141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Leonidas Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-leonidas-blakenbauer.html)|1|[160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|
|[Krystian Korzunio](/rpg/inwazja/opowiesci/karty-postaci/1709-krystian-korzunio.html)|1|[160918](/rpg/inwazja/opowiesci/konspekty/160918-walka-o-dusze-draceny.html)|
|[Krystalia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-krystalia-diakon.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Kornelia Kartel](/rpg/inwazja/opowiesci/karty-postaci/9999-kornelia-kartel.html)|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|[Klemens X](/rpg/inwazja/opowiesci/karty-postaci/9999-klemens-x.html)|1|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Katarzyna Kotek](/rpg/inwazja/opowiesci/karty-postaci/9999-katarzyna-kotek.html)|1|[150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|
|[Karolina Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-karolina-maus.html)|1|[160819](/rpg/inwazja/opowiesci/konspekty/160819-oblicze-guwernantki.html)|
|[Karina Paczulis](/rpg/inwazja/opowiesci/karty-postaci/9999-karina-paczulis.html)|1|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|
|[Judyta Karnisz](/rpg/inwazja/opowiesci/karty-postaci/9999-judyta-karnisz.html)|1|[150826](/rpg/inwazja/opowiesci/konspekty/150826-petla-dookola-pauliny.html)|
|[Joachim Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-zajcew.html)|1|[160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|
|[Joachim Kopiec](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-kopiec.html)|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|[Joachim Kartel](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-kartel.html)|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|[Jan Wątły](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-watly.html)|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|[Ireneusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-ireneusz-bankierz.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Infernia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-infernia-diakon.html)|1|[170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Ilona Amant](/rpg/inwazja/opowiesci/karty-postaci/9999-ilona-amant.html)|1|[170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Grzegorz Śliwa](/rpg/inwazja/opowiesci/karty-postaci/9999-grzegorz-sliwa.html)|1|[150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|
|[Grzegorz Nocniarz](/rpg/inwazja/opowiesci/karty-postaci/9999-grzegorz-nocniarz.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Gala Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-gala-zajcew.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[GS "Aegis" 0002](/rpg/inwazja/opowiesci/karty-postaci/9999-gs-aegis-0002.html)|1|[141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
|[Franciszek Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-maus.html)|1|[170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Franciszek Knur](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-knur.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Filip Czółno](/rpg/inwazja/opowiesci/karty-postaci/9999-filip-czolno.html)|1|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html)|
|[Esme Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-esme-myszeczka.html)|1|[150602](/rpg/inwazja/opowiesci/konspekty/150602-esme-najemniczka-netherii.html)|
|[Ernest Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-ernest-maus.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Eleonora Wiaderska](/rpg/inwazja/opowiesci/karty-postaci/9999-eleonora-wiaderska.html)|1|[141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
|[Eis](/rpg/inwazja/opowiesci/karty-postaci/9999-eis.html)|1|[170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Dagmara Czeluść](/rpg/inwazja/opowiesci/karty-postaci/9999-dagmara-czelusc.html)|1|[160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|
|[Crystal Shard](/rpg/inwazja/opowiesci/karty-postaci/9999-crystal-shard.html)|1|[150602](/rpg/inwazja/opowiesci/konspekty/150602-esme-najemniczka-netherii.html)|
|[Błażej Falka](/rpg/inwazja/opowiesci/karty-postaci/9999-blazej-falka.html)|1|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|
|[Basia Morocz](/rpg/inwazja/opowiesci/karty-postaci/9999-basia-morocz.html)|1|[141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
|[Baltazar Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1802-baltazar-sowinski.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Baltazar Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-baltazar-maus.html)|1|[150826](/rpg/inwazja/opowiesci/konspekty/150826-petla-dookola-pauliny.html)|
|[Aurel Czarko](/rpg/inwazja/opowiesci/karty-postaci/1709-aurel-czarko.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Artur Szmelc](/rpg/inwazja/opowiesci/karty-postaci/9999-artur-szmelc.html)|1|[141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Artur Bryś](/rpg/inwazja/opowiesci/karty-postaci/1709-artur-brys.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Antygona Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-antygona-diakon.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Anna Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-myszeczka.html)|1|[160819](/rpg/inwazja/opowiesci/konspekty/160819-oblicze-guwernantki.html)|
|[Andrea Wilgacz](/rpg/inwazja/opowiesci/karty-postaci/1709-andrea-wilgacz.html)|1|[150718](/rpg/inwazja/opowiesci/konspekty/150718-splatane-tropy-spustoszenie.html)|
|[Amelia Eter](/rpg/inwazja/opowiesci/karty-postaci/9999-amelia-eter.html)|1|[150104](/rpg/inwazja/opowiesci/konspekty/150104-terminus-defiler-kaplan-arazille.html)|
|[Adrian Kropiak](/rpg/inwazja/opowiesci/karty-postaci/9999-adrian-kropiak.html)|1|[150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|
|[Adonis Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1709-adonis-sowinski.html)|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|[Adam Lisek](/rpg/inwazja/opowiesci/karty-postaci/9999-adam-lisek.html)|1|[141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|

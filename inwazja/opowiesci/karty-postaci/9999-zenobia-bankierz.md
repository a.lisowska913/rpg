---
layout: inwazja-karta-postaci
categories: profile
title: "Zenobia Bankierz"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|160629|ceniona gubernator Trocina Świecy rządząca sprawiedliwie, acz uczciwie. Ściśle współpracuje ze Szczepanem Sowińskim i jego terminusami.|[Rezydencja? E, polujemy na dronę!](/rpg/inwazja/opowiesci/konspekty/160629-rezydencja-e-polujemy-na-drone.html)|10/06/27|10/06/30|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160927|gubernator, administracyjna głowa lokalnej Świecy. Konkretna, rozsądna... miła odmiana.|[Desperacka bateria dla Aegis](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|10/06/16|10/06/21|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Szczepan Sowiński](/rpg/inwazja/opowiesci/karty-postaci/9999-szczepan-sowinski.html)|2|[160629](/rpg/inwazja/opowiesci/konspekty/160629-rezydencja-e-polujemy-na-drone.html), [160927](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|
|[Aurel Czarko](/rpg/inwazja/opowiesci/karty-postaci/1709-aurel-czarko.html)|2|[160629](/rpg/inwazja/opowiesci/konspekty/160629-rezydencja-e-polujemy-na-drone.html), [160927](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|
|[Rafał Kniaź](/rpg/inwazja/opowiesci/karty-postaci/9999-rafal-kniaz.html)|1|[160927](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|
|[Mojra](/rpg/inwazja/opowiesci/karty-postaci/9999-mojra.html)|1|[160629](/rpg/inwazja/opowiesci/konspekty/160629-rezydencja-e-polujemy-na-drone.html)|
|[Margaret Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-margaret-blakenbauer.html)|1|[160629](/rpg/inwazja/opowiesci/konspekty/160629-rezydencja-e-polujemy-na-drone.html)|
|[Klara Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-klara-blakenbauer.html)|1|[160629](/rpg/inwazja/opowiesci/konspekty/160629-rezydencja-e-polujemy-na-drone.html)|
|[Kira Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-kira-zajcew.html)|1|[160927](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|
|[Katarzyna Kotek](/rpg/inwazja/opowiesci/karty-postaci/9999-katarzyna-kotek.html)|1|[160927](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|
|[Ignat Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1802-ignat-zajcew.html)|1|[160927](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|
|[GS Aegis 0003](/rpg/inwazja/opowiesci/karty-postaci/9999-gs-aegis-0003.html)|1|[160927](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|
|[Franciszek Baranowski](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-baranowski.html)|1|[160927](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|
|[Felicja Strączek](/rpg/inwazja/opowiesci/karty-postaci/9999-felicja-straczek.html)|1|[160927](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|
|[Dionizy Kret](/rpg/inwazja/opowiesci/karty-postaci/1709-dionizy-kret.html)|1|[160629](/rpg/inwazja/opowiesci/konspekty/160629-rezydencja-e-polujemy-na-drone.html)|
|[Czirna Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-czirna-zajcew.html)|1|[160927](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|
|[Antoni Chlebak](/rpg/inwazja/opowiesci/karty-postaci/9999-antoni-chlebak.html)|1|[160927](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|
|[Andrzej Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-andrzej-bankierz.html)|1|[160927](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|
|[Alina Bednarz](/rpg/inwazja/opowiesci/karty-postaci/1803-alina-bednarz.html)|1|[160629](/rpg/inwazja/opowiesci/konspekty/160629-rezydencja-e-polujemy-na-drone.html)|
|[Aleksander Dziurząb](/rpg/inwazja/opowiesci/karty-postaci/9999-aleksander-dziurzab.html)|1|[160629](/rpg/inwazja/opowiesci/konspekty/160629-rezydencja-e-polujemy-na-drone.html)|

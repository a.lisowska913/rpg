---
layout: inwazja-karta-postaci
categories: profile
title: "Lucjan Kowalkiewicz"
---
# {{ page.title }}

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Szept Mare Felix](/rpg/inwazja/opowiesci/konspekty/180327-szept-mare-felix.html)|doszło do fuzji jego umysłu i ciała konstruminusa Świecy. Jest już konstruminusem|Adaptacja kralotyczna|
|[Szept Mare Felix](/rpg/inwazja/opowiesci/konspekty/180327-szept-mare-felix.html)|z uwagi na obcość biologiczną i mentalną, potrafi poruszać się po Fazie Daemonica nie generując Pryzmatycznego Skażenia|Adaptacja kralotyczna|


## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|180327|konstruminus-kralothborn ściągnięty przez Whisper na Mare Felix. Wykrył obecność Whisper i doprowadził do zniewolenia Bójki.|[Szept Mare Felix](/rpg/inwazja/opowiesci/konspekty/180327-szept-mare-felix.html)|10/12/08|10/12/10|[Adaptacja kralotyczna](/rpg/inwazja/opowiesci/konspekty/kampania-adaptacja-kralotyczna.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Whisperwind](/rpg/inwazja/opowiesci/karty-postaci/9999-whisperwind.html)|1|[180327](/rpg/inwazja/opowiesci/konspekty/180327-szept-mare-felix.html)|
|[Melinda Słonko](/rpg/inwazja/opowiesci/karty-postaci/1803-melinda-slonko.html)|1|[180327](/rpg/inwazja/opowiesci/konspekty/180327-szept-mare-felix.html)|
|[Janek Malczorek](/rpg/inwazja/opowiesci/karty-postaci/9999-janek-malczorek.html)|1|[180327](/rpg/inwazja/opowiesci/konspekty/180327-szept-mare-felix.html)|
|[Bójka Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-bojka-diakon.html)|1|[180327](/rpg/inwazja/opowiesci/konspekty/180327-szept-mare-felix.html)|

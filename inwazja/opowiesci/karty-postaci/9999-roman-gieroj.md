---
layout: inwazja-karta-postaci
categories: profile
title: "Roman Gieroj"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|150830|bardzo pozytywnie wykazał ludzkie odruchy i instynkt przywódczy. Ochroni Kasię przed Julią i wintegruje ją w Powiew.|[Kasia, nie EIS w Powiewie](/rpg/inwazja/opowiesci/konspekty/150830-kasia-nie-eis-w-powiewie.html)|10/05/29|10/05/30|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150829|zaszantażowany przez Marię Newę by pozwolił Katarzynie Kotek i Marii współpracować przeciwko * magicznej zarazie.|[Hybryda w bazie Skorpiona](/rpg/inwazja/opowiesci/konspekty/150829-hybryda-w-bazie-skorpiona.html)|10/05/27|10/05/28|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150823|który nadal kontynuuje projekt 'wiedza Aurelii bez wad jej charakteru'. Zupełnie nie umie podejść do "Aurelii 2.0".|[Atak na sanktuarium Estrelli](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|10/05/23|10/05/24|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150716|ko-przywódca Powiewu, zakładnik, ciężko uszkodzony przez Tamarę który jednak nie zamierza się na chorej mścić.|[Chora terminuska i Żabolód](/rpg/inwazja/opowiesci/konspekty/150716-chora-terminuska-i-zabolod.html)|10/05/09|10/05/10|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150406|jeden z przywódców Powiewu Świeżości, który chce przywrócenia wiedzy Aurelii Maus do Powiewu... za co rozwiąże Paulinie problem aptoforma. Przyjaciel Pauliny.|[Aurelia za aptoforma](/rpg/inwazja/opowiesci/konspekty/150406-aurelia-za-aptoforma.html)|10/05/01|10/05/02|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|5|[150830](/rpg/inwazja/opowiesci/konspekty/150830-kasia-nie-eis-w-powiewie.html), [150829](/rpg/inwazja/opowiesci/konspekty/150829-hybryda-w-bazie-skorpiona.html), [150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html), [150716](/rpg/inwazja/opowiesci/konspekty/150716-chora-terminuska-i-zabolod.html), [150406](/rpg/inwazja/opowiesci/konspekty/150406-aurelia-za-aptoforma.html)|
|[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/1709-maria-newa.html)|5|[150830](/rpg/inwazja/opowiesci/konspekty/150830-kasia-nie-eis-w-powiewie.html), [150829](/rpg/inwazja/opowiesci/konspekty/150829-hybryda-w-bazie-skorpiona.html), [150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html), [150716](/rpg/inwazja/opowiesci/konspekty/150716-chora-terminuska-i-zabolod.html), [150406](/rpg/inwazja/opowiesci/konspekty/150406-aurelia-za-aptoforma.html)|
|[Katarzyna Kotek](/rpg/inwazja/opowiesci/karty-postaci/9999-katarzyna-kotek.html)|3|[150830](/rpg/inwazja/opowiesci/konspekty/150830-kasia-nie-eis-w-powiewie.html), [150829](/rpg/inwazja/opowiesci/konspekty/150829-hybryda-w-bazie-skorpiona.html), [150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|
|[Olga Miodownik](/rpg/inwazja/opowiesci/karty-postaci/1709-olga-miodownik.html)|2|[150829](/rpg/inwazja/opowiesci/konspekty/150829-hybryda-w-bazie-skorpiona.html), [150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|
|[Kermit Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-kermit-diakon.html)|2|[150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html), [150716](/rpg/inwazja/opowiesci/konspekty/150716-chora-terminuska-i-zabolod.html)|
|[Tymoteusz Dzionek](/rpg/inwazja/opowiesci/karty-postaci/9999-tymoteusz-dzionek.html)|1|[150829](/rpg/inwazja/opowiesci/konspekty/150829-hybryda-w-bazie-skorpiona.html)|
|[Tamara Muszkiet](/rpg/inwazja/opowiesci/karty-postaci/1709-tamara-muszkiet.html)|1|[150716](/rpg/inwazja/opowiesci/konspekty/150716-chora-terminuska-i-zabolod.html)|
|[Saith Flamecaller](/rpg/inwazja/opowiesci/karty-postaci/9999-saith-flamecaller.html)|1|[150716](/rpg/inwazja/opowiesci/konspekty/150716-chora-terminuska-i-zabolod.html)|
|[Ozydiusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-ozydiusz-bankierz.html)|1|[150830](/rpg/inwazja/opowiesci/konspekty/150830-kasia-nie-eis-w-powiewie.html)|
|[Netheria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-netheria-diakon.html)|1|[150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|
|[Mikado Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-mikado-diakon.html)|1|[150830](/rpg/inwazja/opowiesci/konspekty/150830-kasia-nie-eis-w-powiewie.html)|
|[Mateusz Tykwa](/rpg/inwazja/opowiesci/karty-postaci/9999-mateusz-tykwa.html)|1|[150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|
|[Marian Kozior](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-kozior.html)|1|[150829](/rpg/inwazja/opowiesci/konspekty/150829-hybryda-w-bazie-skorpiona.html)|
|[Maksymilian Łoś](/rpg/inwazja/opowiesci/karty-postaci/9999-maksymilian-los.html)|1|[150406](/rpg/inwazja/opowiesci/konspekty/150406-aurelia-za-aptoforma.html)|
|[Maciej Tykwa](/rpg/inwazja/opowiesci/karty-postaci/9999-maciej-tykwa.html)|1|[150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|
|[Julia Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-julia-weiner.html)|1|[150830](/rpg/inwazja/opowiesci/konspekty/150830-kasia-nie-eis-w-powiewie.html)|
|[Judyta Karnisz](/rpg/inwazja/opowiesci/karty-postaci/9999-judyta-karnisz.html)|1|[150830](/rpg/inwazja/opowiesci/konspekty/150830-kasia-nie-eis-w-powiewie.html)|
|[Iliusitius](/rpg/inwazja/opowiesci/karty-postaci/9999-iliusitius.html)|1|[150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|
|[Grzegorz Śliwa](/rpg/inwazja/opowiesci/karty-postaci/9999-grzegorz-sliwa.html)|1|[150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|
|[Estrella Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-estrella-diakon.html)|1|[150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|
|[Dominik Szerściuk](/rpg/inwazja/opowiesci/karty-postaci/9999-dominik-szersciuk.html)|1|[150829](/rpg/inwazja/opowiesci/konspekty/150829-hybryda-w-bazie-skorpiona.html)|
|[Damazy Czekan](/rpg/inwazja/opowiesci/karty-postaci/9999-damazy-czekan.html)|1|[150829](/rpg/inwazja/opowiesci/konspekty/150829-hybryda-w-bazie-skorpiona.html)|
|[Celestyna Marduk](/rpg/inwazja/opowiesci/karty-postaci/9999-celestyna-marduk.html)|1|[150830](/rpg/inwazja/opowiesci/konspekty/150830-kasia-nie-eis-w-powiewie.html)|
|[Aptoform Mirasilaler](/rpg/inwazja/opowiesci/karty-postaci/9999-aptoform-mirasilaler.html)|1|[150406](/rpg/inwazja/opowiesci/konspekty/150406-aurelia-za-aptoforma.html)|
|[Annaliza Delfin](/rpg/inwazja/opowiesci/karty-postaci/9999-annaliza-delfin.html)|1|[150406](/rpg/inwazja/opowiesci/konspekty/150406-aurelia-za-aptoforma.html)|
|[Aleksander Szwiok](/rpg/inwazja/opowiesci/karty-postaci/9999-aleksander-szwiok.html)|1|[150829](/rpg/inwazja/opowiesci/konspekty/150829-hybryda-w-bazie-skorpiona.html)|
|[Adrian Kropiak](/rpg/inwazja/opowiesci/karty-postaci/9999-adrian-kropiak.html)|1|[150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|

---
layout: inwazja-karta-postaci
categories: profile
factions: "Lojaliści Srebrnej Świecy, Srebrna Świeca"
type: "NPC"
title: "Ozydiusz Bankierz"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **FIL: Szowinista**: 
    * _Aspekty_: kobieta urodziła się podległa mężczyźnie
    * _Opis_: 
* **MET: Taktyka spalonej ziemi**:
    * _Aspekty_: lepiej przegrać wojnę niż ustąpić, nikt nie jest niewinny, porządek przez terror
    * _Opis_: 
* **KLT: Jestem tylko narzędziem**:
    * _Aspekty_: Kopalin > Świeca > Ród > podwładni > on
    * _Opis_: 

### Umiejętności

* **Terminus**:
    * _Aspekty_: lord terminus, zastraszanie, rozkazywanie, walka  mieczem, pojedynki, taktyk
    * _Opis_: 
* **Polityk**: 
    * _Aspekty_: nieustępliwy, rozgrywanie frakcji, głęboka znajomość Świecy
    * _Opis_: 
    
### Silne i słabe strony:

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

## Magia

### Szkoły magiczne

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

### Zaklęcia statyczne

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 

## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Zarobki

* ?

### Znam

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

### Mam

* **potęga Srebrnej Świecy w Kopalinie**:
    * _Aspekty_: bardzo szerokie uprawnienia
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

# Opis

Lord terminus Kopalina. Żywa furia, niebezpieczny mężczyzna z przyjemnością uciekający się do przemocy. Arogancki; nie przywykł do tego, że jego słowa nie są brane jako święte. Jednocześnie dba o Świecę i o Kopalin i nie waha się podejmować trudnych decyzji czy ryzykować kariery dla ukochanego miasta.

### Koncept

Wysokopoziomowy koncept postaci, 1-2 zdania.

### Motto

"Tekst"

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|160420|uparty i bezwzględny, nie dał się złapać w pułapkę, acz pod presją i gdy jego ród zaczął być atakowany poszedł spotkać się z Amandą.|[Kolizja dwóch sojuszy](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html)|10/07/01|10/07/02|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160316|który uruchomił mechanicznych terminusów i zawiązał tymczasowy sojusz z Blakenbauerami by chronić Kopalin przed Irytką / Szlachtą / Kurtyną / Millennium.|[Frontalne wejście Millennium](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html)|10/06/27|10/06/28|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160309|lord terminus Kopalina, który próbuje utrzymać porządek w ryzach po utracie Kariatydy do Arazille i w obliczu wybuchu epidemii dziwnej Irytki Sprzężonej.|[Irytka Sprzężona](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|10/06/25|10/06/26|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160424|wysłał dane z * magitecha Wiktora do Andrei, wynegocjował z Andreą i Amandą warunki pokoju i zginął z ręki własnej ochrony - Spustoszonej Sabiny Sowińskiej.|[Uważaj, o czym marzysz](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|10/06/18|10/06/21|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160411|którego boi się Wojmił i którego w sumie boją się wszyscy. Dodatkowo, jedzie na koniu i usuwa Spustoszenie.|[Sekret śmierci na wykopaliskach](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|10/06/14|10/06/15|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160327|psychopatyczny terminus, który NADAL jest terminusem, choć czerpie ogromną przyjemność z zadawania bólu, cierpienia i niszczenia wszystkich dookoła. Lojalny Świecy. Dowiedział się wszystkiego co wiedziała Siluria na temat Świecy.|[Piećdziesiąt oblicz Szlachty](/rpg/inwazja/opowiesci/konspekty/160327-piecdziesiat-oblicz-szlachty.html)|10/06/14|10/06/23|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|151124|którego w sumie nie było ale jest skutecznym politykiem i terminusem; nienawidzi wszystkich po równo ze wzajemnością. Potencjalny cel Blakenbauerów.|[Odbudowa relacji konfliktem](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html)|10/06/13|10/06/14|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160229|który pokazał zmysł polityczny, którego po nim nikt by się nie spodziewał odpalając Labirynt Zielonej Kariatydy. Wypożyczył Andrei Judytę.|[Siedmiu magów - nie Mausów](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html)|10/06/08|10/06/13|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150922|jego reputacja przeraża sojuszników a bezwzględność tworzy nowych wrogów. Dużo wie o cruentus reverto.|[Och, nie! Porwali Ignata!](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html)|10/05/29|10/05/30|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150830|który nie docenia bycia traktowanym szczerze (bez szacunku); wydobył trumnę z cmentarza Wiązowego.|[Kasia, nie EIS w Powiewie](/rpg/inwazja/opowiesci/konspekty/150830-kasia-nie-eis-w-powiewie.html)|10/05/29|10/05/30|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150920|chroni Malię, prowadzi śledztwo, uważa, że sprawa Baltazara wymierzona jest w niego i ogólnie niechętnie współpracuje z Andreą. Ale to robi. Eskalował Bogdana i siebie do seirasa.|[Sprawa Baltazara Mausa](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|10/05/27|10/05/28|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150826|którego trafił szlag na Estrellę, ale wobec Pauliny był kulturalny. Szowinista jak diabli.|[Pętla dookoła Pauliny](/rpg/inwazja/opowiesci/konspekty/150826-petla-dookola-pauliny.html)|10/05/25|10/05/26|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150913|który paranoicznie zorientował się, że Wydział Wewnętrzny siedzi mu na ogonie. Pokazał swoje dobre oblicze historycznie.|[Andrea węszy koło Szlachty](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html)|10/05/19|10/05/20|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150728|chce się pozbyć Tamary i problemów z Tamarą i dlatego współpracuje z KADEMem przeciw Tamarze.|[Sojusz przeciwko Szlachcie](/rpg/inwazja/opowiesci/konspekty/150728-sojusz-przeciwko-szlachcie.html)|10/05/15|10/05/16|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150704|lord terminus Kopalina, który jest bezwzględny i bardzo potężny. Nie ma żadnych oporów przed zabiciem * człowieka.|[Najskrytszy sekret Tamary](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html)|10/05/11|10/05/12|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|7|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [160316](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html), [160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html), [151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html), [150728](/rpg/inwazja/opowiesci/konspekty/150728-sojusz-przeciwko-szlachcie.html), [150704](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html)|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|6|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html), [160327](/rpg/inwazja/opowiesci/konspekty/160327-piecdziesiat-oblicz-szlachty.html), [151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html), [150922](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html), [150728](/rpg/inwazja/opowiesci/konspekty/150728-sojusz-przeciwko-szlachcie.html), [150704](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html)|
|[Marian Agrest](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-agrest.html)|6|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html), [160327](/rpg/inwazja/opowiesci/konspekty/160327-piecdziesiat-oblicz-szlachty.html), [160229](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html), [150922](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html), [150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html), [150728](/rpg/inwazja/opowiesci/konspekty/150728-sojusz-przeciwko-szlachcie.html)|
|[Judyta Karnisz](/rpg/inwazja/opowiesci/karty-postaci/9999-judyta-karnisz.html)|6|[160316](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html), [160229](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html), [150922](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html), [150830](/rpg/inwazja/opowiesci/konspekty/150830-kasia-nie-eis-w-powiewie.html), [150826](/rpg/inwazja/opowiesci/konspekty/150826-petla-dookola-pauliny.html), [150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html)|
|[Andrea Wilgacz](/rpg/inwazja/opowiesci/karty-postaci/1709-andrea-wilgacz.html)|6|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html), [160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html), [160229](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html), [150922](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html), [150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html), [150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html)|
|[Aleksander Sowiński](/rpg/inwazja/opowiesci/karty-postaci/9999-aleksander-sowinski.html)|5|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html), [160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html), [160229](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html), [150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html), [150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html)|
|[Marcelin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-marcelin-blakenbauer.html)|4|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [160316](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html), [150728](/rpg/inwazja/opowiesci/konspekty/150728-sojusz-przeciwko-szlachcie.html), [150704](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html)|
|[Elea Maus](/rpg/inwazja/opowiesci/karty-postaci/1802-elea-maus.html)|4|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html), [150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|
|[Diana Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-diana-weiner.html)|4|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html), [160327](/rpg/inwazja/opowiesci/konspekty/160327-piecdziesiat-oblicz-szlachty.html), [150728](/rpg/inwazja/opowiesci/konspekty/150728-sojusz-przeciwko-szlachcie.html)|
|[Dagmara Wyjątek](/rpg/inwazja/opowiesci/karty-postaci/1709-dagmara-wyjątek.html)|4|[160316](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html), [160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html), [160327](/rpg/inwazja/opowiesci/konspekty/160327-piecdziesiat-oblicz-szlachty.html)|
|[Wiktor Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1709-wiktor-sowinski.html)|3|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html), [160327](/rpg/inwazja/opowiesci/konspekty/160327-piecdziesiat-oblicz-szlachty.html)|
|[Tamara Muszkiet](/rpg/inwazja/opowiesci/karty-postaci/1709-tamara-muszkiet.html)|3|[160229](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html), [150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html), [150704](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html)|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|3|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [150830](/rpg/inwazja/opowiesci/konspekty/150830-kasia-nie-eis-w-powiewie.html), [150826](/rpg/inwazja/opowiesci/konspekty/150826-petla-dookola-pauliny.html)|
|[Oktawian Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-oktawian-maus.html)|3|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html), [150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html)|
|[Milena Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-milena-diakon.html)|3|[150922](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html), [150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html), [150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html)|
|[Margaret Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-margaret-blakenbauer.html)|3|[160316](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html), [160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [150728](/rpg/inwazja/opowiesci/konspekty/150728-sojusz-przeciwko-szlachcie.html)|
|[Leonidas Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-leonidas-blakenbauer.html)|3|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [160316](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html), [160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Klara Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-klara-blakenbauer.html)|3|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [160316](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html), [160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[GS "Aegis" 0003](/rpg/inwazja/opowiesci/karty-postaci/9999-gs-aegis-0003.html)|3|[150922](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html), [150728](/rpg/inwazja/opowiesci/konspekty/150728-sojusz-przeciwko-szlachcie.html), [150704](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html)|
|[Emilia Kołatka](/rpg/inwazja/opowiesci/karty-postaci/1709-emilia-kolatka.html)|3|[160316](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html), [160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [150728](/rpg/inwazja/opowiesci/konspekty/150728-sojusz-przeciwko-szlachcie.html)|
|[Elizawieta Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-elizawieta-zajcew.html)|3|[160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html), [150922](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html), [150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|3|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html), [150728](/rpg/inwazja/opowiesci/konspekty/150728-sojusz-przeciwko-szlachcie.html)|
|[Anna Kozak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kozak.html)|3|[160229](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html), [150922](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html), [150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html)|
|[Amanda Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-amanda-diakon.html)|3|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [160316](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Wioletta Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-wioletta-bankierz.html)|2|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html), [160327](/rpg/inwazja/opowiesci/konspekty/160327-piecdziesiat-oblicz-szlachty.html)|
|[Tadeusz Baran](/rpg/inwazja/opowiesci/karty-postaci/1709-tadeusz-baran.html)|2|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html), [160229](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html)|
|[Salazar Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-salazar-bankierz.html)|2|[150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html), [150704](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html)|
|[Patrycja Krowiowska](/rpg/inwazja/opowiesci/karty-postaci/1709-patrycja-krowiowska.html)|2|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html)|
|[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/1709-maria-newa.html)|2|[150830](/rpg/inwazja/opowiesci/konspekty/150830-kasia-nie-eis-w-powiewie.html), [150826](/rpg/inwazja/opowiesci/konspekty/150826-petla-dookola-pauliny.html)|
|[Leokadia Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/9999-leokadia-myszeczka.html)|2|[160229](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html), [150728](/rpg/inwazja/opowiesci/konspekty/150728-sojusz-przeciwko-szlachcie.html)|
|[Kermit Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-kermit-diakon.html)|2|[150922](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html), [150826](/rpg/inwazja/opowiesci/konspekty/150826-petla-dookola-pauliny.html)|
|[Karol Poczciwiec](/rpg/inwazja/opowiesci/karty-postaci/9999-karol-poczciwiec.html)|2|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [150922](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html)|
|[Kajetan Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-kajetan-weiner.html)|2|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Jolanta Sowińska](/rpg/inwazja/opowiesci/karty-postaci/1709-jolanta-sowinska.html)|2|[150826](/rpg/inwazja/opowiesci/konspekty/150826-petla-dookola-pauliny.html), [150704](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html)|
|[Ignat Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1802-ignat-zajcew.html)|2|[150922](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html), [150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|
|[Ernest Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-ernest-maus.html)|2|[160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html), [150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html)|
|[Dominik Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-dominik-bankierz.html)|2|[160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html), [150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html)|
|[Baltazar Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-baltazar-maus.html)|2|[150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html), [150826](/rpg/inwazja/opowiesci/konspekty/150826-petla-dookola-pauliny.html)|
|[Aneta Rainer](/rpg/inwazja/opowiesci/karty-postaci/1709-aneta-rainer.html)|2|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|[Zuzanna Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-zuzanna-maus.html)|1|[160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|[Wojmił Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-wojmil-bankierz.html)|1|[160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|[Wojciech Czapiek](/rpg/inwazja/opowiesci/karty-postaci/9999-wojciech-czapiek.html)|1|[150704](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html)|
|[Vladlena Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-vladlena-zjacew.html)|1|[150728](/rpg/inwazja/opowiesci/konspekty/150728-sojusz-przeciwko-szlachcie.html)|
|[Tymotheus Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-tymotheus-blakenbauer.html)|1|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html)|
|[Tomasz Przodownik](/rpg/inwazja/opowiesci/karty-postaci/9999-tomasz-przodownik.html)|1|[160327](/rpg/inwazja/opowiesci/konspekty/160327-piecdziesiat-oblicz-szlachty.html)|
|[Tatiana Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-tatiana-zajcew.html)|1|[151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html)|
|[Saith Kameleon](/rpg/inwazja/opowiesci/karty-postaci/9999-saith-kameleon.html)|1|[150728](/rpg/inwazja/opowiesci/konspekty/150728-sojusz-przeciwko-szlachcie.html)|
|[Saith Flamecaller](/rpg/inwazja/opowiesci/karty-postaci/9999-saith-flamecaller.html)|1|[150704](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html)|
|[Saith Catapult](/rpg/inwazja/opowiesci/karty-postaci/9999-saith-catapult.html)|1|[160229](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html)|
|[Sabina Sowińska](/rpg/inwazja/opowiesci/karty-postaci/1709-sabina-sowinska.html)|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Ryszard Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-ryszard-weiner.html)|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Rufus Czubek](/rpg/inwazja/opowiesci/karty-postaci/9999-rufus-czubek.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Romeo Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-romeo-diakon.html)|1|[151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html)|
|[Roman Gieroj](/rpg/inwazja/opowiesci/karty-postaci/9999-roman-gieroj.html)|1|[150830](/rpg/inwazja/opowiesci/konspekty/150830-kasia-nie-eis-w-powiewie.html)|
|[Roksana Czapiek](/rpg/inwazja/opowiesci/karty-postaci/9999-roksana-czapiek.html)|1|[150704](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html)|
|[Remigiusz Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-remigiusz-zajcew.html)|1|[150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|
|[Quasar](/rpg/inwazja/opowiesci/karty-postaci/9999-quasar.html)|1|[150704](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html)|
|[Pasożyt Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-pasozyt-diakon.html)|1|[160316](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html)|
|[Olga Miodownik](/rpg/inwazja/opowiesci/karty-postaci/1709-olga-miodownik.html)|1|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html)|
|[Norbert Sonet](/rpg/inwazja/opowiesci/karty-postaci/9999-norbert-sonet.html)|1|[150704](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html)|
|[Netheria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-netheria-diakon.html)|1|[150826](/rpg/inwazja/opowiesci/konspekty/150826-petla-dookola-pauliny.html)|
|[Mojra](/rpg/inwazja/opowiesci/karty-postaci/9999-mojra.html)|1|[151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html)|
|[Mirabelka Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-mirabelka-diakon.html)|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Mikado Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-mikado-diakon.html)|1|[150830](/rpg/inwazja/opowiesci/konspekty/150830-kasia-nie-eis-w-powiewie.html)|
|[Malia Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-malia-bankierz.html)|1|[150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|
|[Liliana Sowińska](/rpg/inwazja/opowiesci/karty-postaci/9999-liliana-sowinska.html)|1|[150704](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html)|
|[Laragnarhag](/rpg/inwazja/opowiesci/karty-postaci/1709-laragnarhag.html)|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Krzysztof Wieczorek](/rpg/inwazja/opowiesci/karty-postaci/9999-krzysztof-wieczorek.html)|1|[160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|[Kleofas Bór](/rpg/inwazja/opowiesci/karty-postaci/1709-kleofas-bor.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Kinga Melit](/rpg/inwazja/opowiesci/karty-postaci/9999-kinga-melit.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Katarzyna Kotek](/rpg/inwazja/opowiesci/karty-postaci/9999-katarzyna-kotek.html)|1|[150830](/rpg/inwazja/opowiesci/konspekty/150830-kasia-nie-eis-w-powiewie.html)|
|[Jurij Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-jurij-zajcew.html)|1|[160327](/rpg/inwazja/opowiesci/konspekty/160327-piecdziesiat-oblicz-szlachty.html)|
|[Julian Pszczelak](/rpg/inwazja/opowiesci/karty-postaci/1709-julian-pszczelak.html)|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Julia Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-julia-weiner.html)|1|[150830](/rpg/inwazja/opowiesci/konspekty/150830-kasia-nie-eis-w-powiewie.html)|
|[Judyta Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-judyta-maus.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Joachim Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-zajcew.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Jan Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-weiner.html)|1|[160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|[Jan Anioł Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-aniol-bankierz.html)|1|[160229](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html)|
|[Irina Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-irina-zajcew.html)|1|[150922](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html)|
|[Irena Resort](/rpg/inwazja/opowiesci/karty-postaci/9999-irena-resort.html)|1|[150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html)|
|[Infensa Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-infensa-diakon.html)|1|[150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html)|
|[Hieronim Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-hieronim-maus.html)|1|[150704](/rpg/inwazja/opowiesci/konspekty/150704-najskrytszy-sekret-tamary.html)|
|[Gerwazy Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/9999-gerwazy-myszeczka.html)|1|[160327](/rpg/inwazja/opowiesci/konspekty/160327-piecdziesiat-oblicz-szlachty.html)|
|[Gabriela Resort](/rpg/inwazja/opowiesci/karty-postaci/9999-gabriela-resort.html)|1|[150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html)|
|[Fortitia Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-fortitia-diakon.html)|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Felicjan Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-felicjan-weiner.html)|1|[160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|[Estrella Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-estrella-diakon.html)|1|[150826](/rpg/inwazja/opowiesci/konspekty/150826-petla-dookola-pauliny.html)|
|[Edward Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-edward-diakon.html)|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Dionizy Kret](/rpg/inwazja/opowiesci/karty-postaci/1709-dionizy-kret.html)|1|[151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html)|
|[Czirna Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-czirna-zajcew.html)|1|[151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html)|
|[Celestyna Marduk](/rpg/inwazja/opowiesci/karty-postaci/9999-celestyna-marduk.html)|1|[150830](/rpg/inwazja/opowiesci/konspekty/150830-kasia-nie-eis-w-powiewie.html)|
|[Borys Kumin](/rpg/inwazja/opowiesci/karty-postaci/1709-borys-kumin.html)|1|[151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html)|
|[Bolesław Derwisz](/rpg/inwazja/opowiesci/karty-postaci/9999-boleslaw-derwisz.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Bogdan Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-bogdan-bankierz.html)|1|[150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|
|[Bianka Stein](/rpg/inwazja/opowiesci/karty-postaci/1709-bianka-stein.html)|1|[160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|[Aurel Czarko](/rpg/inwazja/opowiesci/karty-postaci/1709-aurel-czarko.html)|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Artur Żupan](/rpg/inwazja/opowiesci/karty-postaci/1803-artur-zupan.html)|1|[150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|
|[Artur Janczecki](/rpg/inwazja/opowiesci/karty-postaci/9999-artur-janczecki.html)|1|[160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|[Antoni Kurzamyśl](/rpg/inwazja/opowiesci/karty-postaci/9999-antoni-kurzamysl.html)|1|[160327](/rpg/inwazja/opowiesci/konspekty/160327-piecdziesiat-oblicz-szlachty.html)|
|[Anna Kajak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kajak.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Alina Bednarz](/rpg/inwazja/opowiesci/karty-postaci/1803-alina-bednarz.html)|1|[151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html)|
|[Adrian Kropiak](/rpg/inwazja/opowiesci/karty-postaci/9999-adrian-kropiak.html)|1|[150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|
|[Adela Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-adela-maus.html)|1|[150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html)|

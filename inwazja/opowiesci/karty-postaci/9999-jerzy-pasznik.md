---
layout: inwazja-karta-postaci
categories: profile
title: "Jerzy Pasznik"
---
# {{ page.title }}

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Na żywym organiźmie Draceny...](/rpg/inwazja/opowiesci/konspekty/160910-na-zywym-organizmie-draceny.html)|bardzo dziwna relacja z Kingą Toczek; zagraża jego karierze i jest dlań... nową obserwacją (impulsem?)|Prawdziwa natura Draceny|


## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|160918|miał dobry pomysł jak znaleźć efemerydę... ale Najada była czymś więcej. Nie miał ani jednej udanej akcji na tej misji, acz wykrył "Dracenę".|[Walka o duszę Draceny](/rpg/inwazja/opowiesci/konspekty/160918-walka-o-dusze-draceny.html)|10/06/30|10/07/03|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|
|160910|terminus Świecy chcący sprawdzić pobieżnie co tu się dzieje by się wykazać i wpadający w erotyczny galimatias i sidła Netherii|[Na żywym organiźmie Draceny...](/rpg/inwazja/opowiesci/konspekty/160910-na-zywym-organizmie-draceny.html)|10/06/27|10/06/29|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|2|[160918](/rpg/inwazja/opowiesci/konspekty/160918-walka-o-dusze-draceny.html), [160910](/rpg/inwazja/opowiesci/konspekty/160910-na-zywym-organizmie-draceny.html)|
|[Netheria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-netheria-diakon.html)|2|[160918](/rpg/inwazja/opowiesci/konspekty/160918-walka-o-dusze-draceny.html), [160910](/rpg/inwazja/opowiesci/konspekty/160910-na-zywym-organizmie-draceny.html)|
|[Kinga Toczek](/rpg/inwazja/opowiesci/karty-postaci/9999-kinga-toczek.html)|2|[160918](/rpg/inwazja/opowiesci/konspekty/160918-walka-o-dusze-draceny.html), [160910](/rpg/inwazja/opowiesci/konspekty/160910-na-zywym-organizmie-draceny.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|2|[160918](/rpg/inwazja/opowiesci/konspekty/160918-walka-o-dusze-draceny.html), [160910](/rpg/inwazja/opowiesci/konspekty/160910-na-zywym-organizmie-draceny.html)|
|[Włodzimierz Jamnik](/rpg/inwazja/opowiesci/karty-postaci/9999-wlodzimierz-jamnik.html)|1|[160918](/rpg/inwazja/opowiesci/konspekty/160918-walka-o-dusze-draceny.html)|
|[Najada Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-najada-diakon.html)|1|[160918](/rpg/inwazja/opowiesci/konspekty/160918-walka-o-dusze-draceny.html)|
|[Marcin Puczek](/rpg/inwazja/opowiesci/karty-postaci/9999-marcin-puczek.html)|1|[160918](/rpg/inwazja/opowiesci/konspekty/160918-walka-o-dusze-draceny.html)|
|[Krystian Korzunio](/rpg/inwazja/opowiesci/karty-postaci/1709-krystian-korzunio.html)|1|[160918](/rpg/inwazja/opowiesci/konspekty/160918-walka-o-dusze-draceny.html)|

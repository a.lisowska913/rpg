---
layout: inwazja-karta-postaci
categories: profile
title: "Ylytis"
---
# {{ page.title }}

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Dlaczego Kret w jeziorze?](/rpg/inwazja/opowiesci/konspekty/180503-dlaczego-kret-w-jeziorze.html)|beznadziejnie zakochany w czteronożnych futrzakach zwanych kotami.|Wizja Dukata|
|[Dlaczego Kret w jeziorze?](/rpg/inwazja/opowiesci/konspekty/180503-dlaczego-kret-w-jeziorze.html)|posiada wiedzę z Faz, przez które przechodził Kret IV. Nie pomogło to na jego stan emocjonalny i logiczny ;-).|Wizja Dukata|

## Plany

|Misja|Plan|Kampania|
|-----|------|------|
|[Dlaczego Kret w jeziorze?](/rpg/inwazja/opowiesci/konspekty/180503-dlaczego-kret-w-jeziorze.html)|zainteresowany NIE wróceniem do Yyizdatha; chce zniknąć i mieć własny dom z dużą ilością kotów|Wizja Dukata|

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|180503|uszkodzony Yyizdathspawn który towarzyszył Kretowi IV przez jego długie przeboje. Aż do teraz.|[Dlaczego Kret w jeziorze?](/rpg/inwazja/opowiesci/konspekty/180503-dlaczego-kret-w-jeziorze.html)|11/10/27|11/10/29|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Yyizdath](/rpg/inwazja/opowiesci/karty-postaci/9999-yyizdath.html)|1|[180503](/rpg/inwazja/opowiesci/konspekty/180503-dlaczego-kret-w-jeziorze.html)|
|[Sylwia Zasobna](/rpg/inwazja/opowiesci/karty-postaci/9999-sylwia-zasobna.html)|1|[180503](/rpg/inwazja/opowiesci/konspekty/180503-dlaczego-kret-w-jeziorze.html)|
|[Sylwester Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-sylwester-bankierz.html)|1|[180503](/rpg/inwazja/opowiesci/konspekty/180503-dlaczego-kret-w-jeziorze.html)|
|[Stefan Bułka](/rpg/inwazja/opowiesci/karty-postaci/9999-stefan-bulka.html)|1|[180503](/rpg/inwazja/opowiesci/konspekty/180503-dlaczego-kret-w-jeziorze.html)|
|[Kinga Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1803-kinga-bankierz.html)|1|[180503](/rpg/inwazja/opowiesci/konspekty/180503-dlaczego-kret-w-jeziorze.html)|
|[Karmena Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-karmena-bankierz.html)|1|[180503](/rpg/inwazja/opowiesci/konspekty/180503-dlaczego-kret-w-jeziorze.html)|
|[Anatol Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1803-anatol-sowinski.html)|1|[180503](/rpg/inwazja/opowiesci/konspekty/180503-dlaczego-kret-w-jeziorze.html)|
|[Alfred Janowiecki](/rpg/inwazja/opowiesci/karty-postaci/9999-alfred-janowiecki.html)|1|[180503](/rpg/inwazja/opowiesci/konspekty/180503-dlaczego-kret-w-jeziorze.html)|

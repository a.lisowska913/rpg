---
layout: inwazja-karta-postaci
categories: profile
title: "Felicja Szampierz"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170523|pomiata Dobrocieniem z ramienia Dukata; Paulina ją przekonała, że Dobrocień niewinny. Pomogła jak była w stanie. |[Opętany konstruminus](/rpg/inwazja/opowiesci/konspekty/170523-opetany-konstruminus.html)|10/01/03|10/01/04|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Zofia Murczówik](/rpg/inwazja/opowiesci/karty-postaci/9999-zofia-murczowik.html)|1|[170523](/rpg/inwazja/opowiesci/konspekty/170523-opetany-konstruminus.html)|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|1|[170523](/rpg/inwazja/opowiesci/konspekty/170523-opetany-konstruminus.html)|
|[Jakub Dobrocień](/rpg/inwazja/opowiesci/karty-postaci/1709-jakub-dobrocien.html)|1|[170523](/rpg/inwazja/opowiesci/konspekty/170523-opetany-konstruminus.html)|
|[Efemeryda Senesgradzka](/rpg/inwazja/opowiesci/karty-postaci/9999-efemeryda-senesgradzka.html)|1|[170523](/rpg/inwazja/opowiesci/konspekty/170523-opetany-konstruminus.html)|
|[Apoloniusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-apoloniusz-bankierz.html)|1|[170523](/rpg/inwazja/opowiesci/konspekty/170523-opetany-konstruminus.html)|
|[Andrzej Farnolis](/rpg/inwazja/opowiesci/karty-postaci/9999-andrzej-farnolis.html)|1|[170523](/rpg/inwazja/opowiesci/konspekty/170523-opetany-konstruminus.html)|

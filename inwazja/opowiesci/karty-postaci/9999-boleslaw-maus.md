---
layout: inwazja-karta-postaci
categories: profile
title: "Bolesław Maus"
---
# {{ page.title }}

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Nie podłożona świnia Łucji](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html)|uważa Paulinę Tarczyńską za osobę sprzyjającą pozytywnym Mausom; ogólnie jej sprzyja|Wizja Dukata|
|[Świnia na portalisku](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|przeciwko niemu montują się nastroje antyMausowe i antyKarradraelowe|Wizja Dukata|

## Plany

|Misja|Plan|Kampania|
|-----|------|------|
|[Świnia na portalisku](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|za wszelką cenę chce utrzymać portalisko w swoich rękach. Jest zdolny do wielkich desperacji, by to wszystko jakoś działało.|Wizja Dukata|

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|180214|zaczął jako osoba niechętna Paulinie, bo "Łucję mu biją". Skończył z Łucją mu pomagającą i bardzo pro-Paulinowo.|[Nie podłożona świnia Łucji](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html)|11/10/15|11/10/17|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171029|ściągnął od rodu Mausów sprzęt pomagający w naprawie portaliska.|[W co gra Sądeczny?](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html)|11/09/20|11/09/22|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171003|właściciel portaliska zdolny do desperackich decyzji odnośnie stawiania na nogi Kociebora, by tylko nie wypaść z interesu. Boi się Myszeczki.|[Świnia na portalisku](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|11/08/28|11/08/30|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Tomasz Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-tomasz-myszeczka.html)|3|[180214](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html), [171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html), [171003](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|2|[180214](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html), [171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html)|
|[Jakub Dobrocień](/rpg/inwazja/opowiesci/karty-postaci/1709-jakub-dobrocien.html)|2|[171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html), [171003](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|
|[Łucja Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-lucja-maus.html)|1|[180214](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html)|
|[Sylwester Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-sylwester-bankierz.html)|1|[171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html)|
|[Ryszard Kota](/rpg/inwazja/opowiesci/karty-postaci/9999-ryszard-kota.html)|1|[171003](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|
|[Robert Sądeczny](/rpg/inwazja/opowiesci/karty-postaci/1709-robert-sadeczny.html)|1|[171003](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|
|[Paweł Kupiernik](/rpg/inwazja/opowiesci/karty-postaci/9999-pawel-kupiernik.html)|1|[171003](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|
|[Oktawia Aurinus](/rpg/inwazja/opowiesci/karty-postaci/1709-oktawia-aurinus.html)|1|[171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html)|
|[Natalia Kazuń](/rpg/inwazja/opowiesci/karty-postaci/1709-natalia-kazun.html)|1|[171003](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|
|[Melodia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-melodia-diakon.html)|1|[180214](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html)|
|[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/1709-maria-newa.html)|1|[171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html)|
|[Kociebor Dyrygent](/rpg/inwazja/opowiesci/karty-postaci/1709-kociebor-dyrygent.html)|1|[171003](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|
|[Karol Marzyciel](/rpg/inwazja/opowiesci/karty-postaci/1709-karol-marzyciel.html)|1|[171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html)|
|[Kajetan Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-kajetan-weiner.html)|1|[171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html)|
|[Kaja Maślaczek](/rpg/inwazja/opowiesci/karty-postaci/1709-kaja-maslaczek.html)|1|[171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html)|
|[Jolanta Cieśliska](/rpg/inwazja/opowiesci/karty-postaci/9999-jolanta-ciesliska.html)|1|[171003](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|
|[Joachim Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-maus.html)|1|[180214](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html)|
|[Henryk Mordżyn](/rpg/inwazja/opowiesci/karty-postaci/9999-henryk-mordzyn.html)|1|[171003](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|
|[Efemeryda Senesgradzka](/rpg/inwazja/opowiesci/karty-postaci/9999-efemeryda-senesgradzka.html)|1|[171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|1|[171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html)|
|[Daniel Akwitański](/rpg/inwazja/opowiesci/karty-postaci/1709-daniel-akwitanski.html)|1|[180214](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html)|
|[Andżelika Leszczyńska](/rpg/inwazja/opowiesci/karty-postaci/1709-andzelika-leszczynska.html)|1|[171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html)|

---
layout: inwazja-karta-postaci
categories: profile
title: "Mariusz Czyczyż"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|151003|Gala wyciągnęła go ze szpitala, ale jest w takim stanie, że nic sensownego nie zrobił.|[Zamtuz przestaje działać](/rpg/inwazja/opowiesci/konspekty/151003-zamtuz-przestaje-dzialac.html)|10/06/02|10/06/03|[Ucieczka do Przodka](/rpg/inwazja/opowiesci/konspekty/kampania-ucieczka-do-przodka.html)|
|150928|agent Zajcewów z misją otwarcia zamtuza we wsi Gęsilot. Wynajęty przez Galę Zajcew.|[Zamtuz z jedną wiłą](/rpg/inwazja/opowiesci/konspekty/150928-zamtuz-z-jedna-wila.html)|10/05/31|10/06/01|[Ucieczka do Przodka](/rpg/inwazja/opowiesci/konspekty/kampania-ucieczka-do-przodka.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|2|[151003](/rpg/inwazja/opowiesci/konspekty/151003-zamtuz-przestaje-dzialac.html), [150928](/rpg/inwazja/opowiesci/konspekty/150928-zamtuz-z-jedna-wila.html)|
|[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/1709-maria-newa.html)|2|[151003](/rpg/inwazja/opowiesci/konspekty/151003-zamtuz-przestaje-dzialac.html), [150928](/rpg/inwazja/opowiesci/konspekty/150928-zamtuz-z-jedna-wila.html)|
|[Gala Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-gala-zajcew.html)|2|[151003](/rpg/inwazja/opowiesci/konspekty/151003-zamtuz-przestaje-dzialac.html), [150928](/rpg/inwazja/opowiesci/konspekty/150928-zamtuz-z-jedna-wila.html)|
|[Tymoteusz Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-tymoteusz-maus.html)|1|[151003](/rpg/inwazja/opowiesci/konspekty/151003-zamtuz-przestaje-dzialac.html)|
|[Tomasz Leżniak](/rpg/inwazja/opowiesci/karty-postaci/9999-tomasz-lezniak.html)|1|[151003](/rpg/inwazja/opowiesci/konspekty/151003-zamtuz-przestaje-dzialac.html)|
|[Ryszard Herman](/rpg/inwazja/opowiesci/karty-postaci/9999-ryszard-herman.html)|1|[150928](/rpg/inwazja/opowiesci/konspekty/150928-zamtuz-z-jedna-wila.html)|
|[Rafał Maciejak](/rpg/inwazja/opowiesci/karty-postaci/9999-rafal-maciejak.html)|1|[150928](/rpg/inwazja/opowiesci/konspekty/150928-zamtuz-z-jedna-wila.html)|
|[Matylda Daczak](/rpg/inwazja/opowiesci/karty-postaci/9999-matylda-daczak.html)|1|[151003](/rpg/inwazja/opowiesci/konspekty/151003-zamtuz-przestaje-dzialac.html)|
|[Lea Swoboda](/rpg/inwazja/opowiesci/karty-postaci/9999-lea-swoboda.html)|1|[151003](/rpg/inwazja/opowiesci/konspekty/151003-zamtuz-przestaje-dzialac.html)|
|[Filip Czątko](/rpg/inwazja/opowiesci/karty-postaci/9999-filip-czatko.html)|1|[150928](/rpg/inwazja/opowiesci/konspekty/150928-zamtuz-z-jedna-wila.html)|

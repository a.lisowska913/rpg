---
layout: inwazja-karta-postaci
categories: profile
title: "Katarzyna Nieborak"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170815|"bliźniaczka" Andromedy; wie coś o przeszłości Andromedy, acz nie umie jej tego powiedzieć. Wybrała wolność i drogę autonomiczną. Nie różni jej to od oryginału.|[Bliźniaczka Andromedy](/rpg/inwazja/opowiesci/konspekty/170815-blizniaczka-andromedy.html)|10/01/23|10/01/25|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Krzysztof Jarzolin](/rpg/inwazja/opowiesci/karty-postaci/9999-krzysztof-jarzolin.html)|1|[170815](/rpg/inwazja/opowiesci/konspekty/170815-blizniaczka-andromedy.html)|
|[Kasia Nowak](/rpg/inwazja/opowiesci/karty-postaci/1803-kasia-nowak.html)|1|[170815](/rpg/inwazja/opowiesci/konspekty/170815-blizniaczka-andromedy.html)|
|[Czesław Czepiec](/rpg/inwazja/opowiesci/karty-postaci/9999-czeslaw-czepiec.html)|1|[170815](/rpg/inwazja/opowiesci/konspekty/170815-blizniaczka-andromedy.html)|

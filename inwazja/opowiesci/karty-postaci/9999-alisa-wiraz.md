---
layout: inwazja-karta-postaci
categories: profile
title: "Alisa Wiraż"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|150625|naiwna i mało przebojowa praktykantka KADEMu o ogromnej mocy działająca w Instytucie Zniszczenia; chciała podłożyć Hektorowi pluskwę. Szybko biega.|[Poligon kluczem do Marcelina?](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html)|10/05/09|10/05/10|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161216|ma nadzieję dostać się do wymarzonego Instytutu; zahukana i nieśmiała. Żali się EIS; nie czuje się częścią grupy. NIE jest dziewczyną Warmastera i NIE flirtuje z Silurią.|[Szept z Academii Daemonica](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|10/01/27|10/01/29|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|2|[150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html), [161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|[Infernia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-infernia-diakon.html)|2|[150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html), [161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|[Ignat Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1802-ignat-zajcew.html)|2|[150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html), [161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|[Whisperwind](/rpg/inwazja/opowiesci/karty-postaci/9999-whisperwind.html)|1|[161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|[Warmaster](/rpg/inwazja/opowiesci/karty-postaci/9999-warmaster.html)|1|[161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|[Vladlena Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-vladlena-zjacew.html)|1|[150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html)|
|[Tamara Muszkiet](/rpg/inwazja/opowiesci/karty-postaci/1709-tamara-muszkiet.html)|1|[150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html)|
|[Norbert Sonet](/rpg/inwazja/opowiesci/karty-postaci/9999-norbert-sonet.html)|1|[150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html)|
|[Marta Szysznicka](/rpg/inwazja/opowiesci/karty-postaci/9999-marta-szysznicka.html)|1|[161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|[Mariusz Trzosik](/rpg/inwazja/opowiesci/karty-postaci/9999-mariusz-trzosik.html)|1|[150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html)|
|[Marcel Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-marcel-bankierz.html)|1|[150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html)|
|[Lucjan Kopidół](/rpg/inwazja/opowiesci/karty-postaci/1803-lucjan-kopidol.html)|1|[161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|[Karina Paczulis](/rpg/inwazja/opowiesci/karty-postaci/9999-karina-paczulis.html)|1|[161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|[Jolanta Sowińska](/rpg/inwazja/opowiesci/karty-postaci/1709-jolanta-sowinska.html)|1|[150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|1|[150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|1|[150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html)|
|[Demon_481](/rpg/inwazja/opowiesci/karty-postaci/9999-demon_481.html)|1|[150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html)|
|[Artur Kotała](/rpg/inwazja/opowiesci/karty-postaci/9999-artur-kotala.html)|1|[161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|[Aleksandra Trawens](/rpg/inwazja/opowiesci/karty-postaci/9999-aleksandra-trawens.html)|1|[161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|

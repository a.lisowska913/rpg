---
layout: inwazja-karta-postaci
categories: profile
factions: "Srebrna Świeca"
type: "NPC"
title: "Marianna Sowińska"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **FIL: survivor**: 
    * _Aspekty_: zaradna, oportunistka, lojalna
    * _Opis_: "Oki, jesteśmy w beznadziejnej sytuacji. Trzeba sobie poradzić; nikt nie zginie"
* **KLT: Opiekun**:
    * _Aspekty_: przyjazna, Świeca moją siłą
    * _Opis_: "Nikt pod moją opieką nie ucierpi. Świeca trzyma się razem"
	
### Umiejętności

* **Oficer**:
    * _Aspekty_: dowódca bazy Świecy, defensywy, fortyfikowanie, adaptacja do przetrwania, scarcity governance
    * _Opis_: 
* **Arystokrata**: 
    * _Aspekty_: arystokratka Świecy
    * _Opis_: 
* **Survival**:
    * _Aspekty_: podstawy magicznych systemów obronnych, defensywy, recycler, adaptacja do przetrwania, scarcity governance, woda jedzenie i łóżko z kamienia
    * _Opis_: 
* **Ekspert od Esuriit**:
    * _Aspekty_: fortyfikowanie, adaptacja do przetrwania, potwory Esuriit
    * _Opis_: 

    
### Silne i słabe strony:

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

## Magia

### Szkoły magiczne

* **Magia materii**:
    * _Aspekty_: 
    * _Opis_: 
* **Magia elementalna**: 
    * _Aspekty_: 
    * _Opis_: 

### Zaklęcia statyczne

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 

## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Zarobki

* ?

### Znam

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

### Mam

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

# Opis

krótki opis postaci, rzeczy, jakie warto pamiętać.

### Koncept

Wysokopoziomowy koncept postaci, 1-2 zdania.

### Motto

"Tekst"

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170125|w głębokiej depresji i na środkach; jednak udało się ją przeprowadzić przez Fazę Daemonica. |[Przeprawa do Świecy Daemonica](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html)|10/08/10|10/08/11|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170104|w głębokiej depresji, wierząca, że są na Esuriit... co zmusiło KADEM do środków specjalnych. Skończyła w ekstazie po środkach Karoliny i działaniach Silurii ;-).|[Spalone generatory pryzmatyczne](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html)|10/08/08|10/08/09|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161130|do samego końca dowodziła Świecą podejmując straszne decyzje. Do samego końca została na posterunku. Przeżyła - dla innych.|[Sprowadzenie Mare Vortex](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html)|10/08/02|10/08/04|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161124|która podjęła mnóstwo mrocznych decyzji by uratować Ekspedycję i jest zdeterminowana, by wszyscy wrócili do domu.|[Ponura historia ekspedycji Esuriit](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html)|10/07/31|10/08/01|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161109|nie chcąca zostawiać żadnego ze swoich * magów na śmierć.|[Jak prawidłowo wpaść w pułapkę](/rpg/inwazja/opowiesci/konspekty/161109-jak-prawidlowo-wpasc-w-pulapke.html)|10/07/29|10/07/30|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161102|prawdziwa; traci nerwy i z trudem kontroluje morale w Ekspedycji. Straciła brata na Esuriit. Stoi po stronie Zespołu; ma nadzieję wrócić.|[Magowie Esuriit w domu](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html)|10/07/26|10/07/28|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161026|dowódca; 'czysta', podjęła grupę fatalnych (ale skutecznych) decyzji jak przetrwać na Fazie Esuriit. Chce do domu.|[Zagłodzona ekspedycja Świecy](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html)|10/07/24|10/07/25|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|7|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html), [161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html), [161109](/rpg/inwazja/opowiesci/konspekty/161109-jak-prawidlowo-wpasc-w-pulapke.html), [161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html), [161026](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|7|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html), [161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html), [161109](/rpg/inwazja/opowiesci/konspekty/161109-jak-prawidlowo-wpasc-w-pulapke.html), [161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html), [161026](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html)|
|[Mikado Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-mikado-diakon.html)|6|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html), [161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html), [161109](/rpg/inwazja/opowiesci/konspekty/161109-jak-prawidlowo-wpasc-w-pulapke.html), [161026](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html)|
|[Anna Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-myszeczka.html)|6|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html), [161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html), [161109](/rpg/inwazja/opowiesci/konspekty/161109-jak-prawidlowo-wpasc-w-pulapke.html), [161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html), [161026](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html)|
|[Siriratharin](/rpg/inwazja/opowiesci/karty-postaci/1709-siriratharin.html)|5|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html), [161109](/rpg/inwazja/opowiesci/konspekty/161109-jak-prawidlowo-wpasc-w-pulapke.html), [161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html), [161026](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html)|
|[Ignat Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1802-ignat-zajcew.html)|5|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html), [161109](/rpg/inwazja/opowiesci/konspekty/161109-jak-prawidlowo-wpasc-w-pulapke.html), [161026](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html)|
|[Dosifiej Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-dosifiej-zajcew.html)|5|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html), [161109](/rpg/inwazja/opowiesci/konspekty/161109-jak-prawidlowo-wpasc-w-pulapke.html), [161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html), [161026](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html)|
|[Andżelika Leszczyńska](/rpg/inwazja/opowiesci/karty-postaci/1709-andzelika-leszczynska.html)|5|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html), [161109](/rpg/inwazja/opowiesci/konspekty/161109-jak-prawidlowo-wpasc-w-pulapke.html), [161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html), [161026](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html)|
|[Łukija Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-lukija-zajcew.html)|3|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html), [161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html)|
|[Zofia Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-zofia-weiner.html)|3|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html), [161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html)|
|[Jakub Pestka](/rpg/inwazja/opowiesci/karty-postaci/9999-jakub-pestka.html)|3|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html), [161026](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html)|
|[Fiodor Maius Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-fiodor-maius-zajcew.html)|3|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html), [161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html)|
|[Paweł Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-pawel-maus.html)|2|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html)|
|[Konstanty Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-konstanty-myszeczka.html)|2|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html)|
|[Konstanty Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-konstanty-bankierz.html)|2|[161109](/rpg/inwazja/opowiesci/konspekty/161109-jak-prawidlowo-wpasc-w-pulapke.html), [161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html)|
|[Kazimierz Sowiński](/rpg/inwazja/opowiesci/karty-postaci/9999-kazimierz-sowinski.html)|2|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html)|
|[Infernia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-infernia-diakon.html)|2|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html)|
|[Eis](/rpg/inwazja/opowiesci/karty-postaci/9999-eis.html)|2|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html)|
|[Bazyli Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-bazyli-weiner.html)|2|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html)|
|[Whisperwind](/rpg/inwazja/opowiesci/karty-postaci/9999-whisperwind.html)|1|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html)|
|[Warmaster](/rpg/inwazja/opowiesci/karty-postaci/9999-warmaster.html)|1|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html)|
|[Quasar](/rpg/inwazja/opowiesci/karty-postaci/9999-quasar.html)|1|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html)|
|[Norbert Sonet](/rpg/inwazja/opowiesci/karty-postaci/9999-norbert-sonet.html)|1|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html)|
|[Marta Szysznicka](/rpg/inwazja/opowiesci/karty-postaci/9999-marta-szysznicka.html)|1|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html)|
|[Lucjan Kopidół](/rpg/inwazja/opowiesci/karty-postaci/1803-lucjan-kopidol.html)|1|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html)|
|[Konrad Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/9999-konrad-myszeczka.html)|1|[161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html)|
|[Karolina Kupiec](/rpg/inwazja/opowiesci/karty-postaci/1803-karolina-kupiec.html)|1|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html)|
|[Karina Paczulis](/rpg/inwazja/opowiesci/karty-postaci/9999-karina-paczulis.html)|1|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html)|
|[Ilona Amant](/rpg/inwazja/opowiesci/karty-postaci/9999-ilona-amant.html)|1|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html)|
|[Bogumił Rojowiec](/rpg/inwazja/opowiesci/karty-postaci/9999-bogumil-rojowiec.html)|1|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html)|

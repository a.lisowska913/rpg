---
layout: inwazja-karta-postaci
categories: profile
title: "Laetitia Gaia Rasputin Weiner"
---
# {{ page.title }}

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Nekroborg dla Laetitii Gai](/rpg/inwazja/opowiesci/konspekty/170214-nekroborg-dla-laetitii-gai.html)|niestety, jest dużo bardziej Zakłócona niż można się było spodziewać...|Nicaretta|


## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170214|dużo bardziej Zakłócona niż się wydawało. Kierowała akcją uruchomienia nekroborga. Inkarnowała swój Fragment w nekroborgu.|[Nekroborg dla Laetitii Gai](/rpg/inwazja/opowiesci/konspekty/170214-nekroborg-dla-laetitii-gai.html)|10/07/26|10/07/29|[Nicaretta](/rpg/inwazja/opowiesci/konspekty/kampania-nicaretta.html)|
|170212|Overlord Kolonii Hipernetowej Świecy. Kiedyś: worldmaker. Zakłócona, acz się zregenerowała. Funkcjonuje i chce współpracować ze Świecą.|[Nieufność w małym mieście](/rpg/inwazja/opowiesci/konspekty/170212-nieufnosc-w-malym-miescie.html)|10/07/22|10/07/25|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|1|[170212](/rpg/inwazja/opowiesci/konspekty/170212-nieufnosc-w-malym-miescie.html)|
|[Nicaretta](/rpg/inwazja/opowiesci/karty-postaci/1709-nicaretta.html)|1|[170214](/rpg/inwazja/opowiesci/konspekty/170214-nekroborg-dla-laetitii-gai.html)|
|[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/1709-maria-newa.html)|1|[170212](/rpg/inwazja/opowiesci/konspekty/170212-nieufnosc-w-malym-miescie.html)|
|[Lidia Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-lidia-weiner.html)|1|[170212](/rpg/inwazja/opowiesci/konspekty/170212-nieufnosc-w-malym-miescie.html)|
|[Kurt Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-kurt-weiner.html)|1|[170212](/rpg/inwazja/opowiesci/konspekty/170212-nieufnosc-w-malym-miescie.html)|
|[Karina Łoszad](/rpg/inwazja/opowiesci/karty-postaci/9999-karina-loszad.html)|1|[170214](/rpg/inwazja/opowiesci/konspekty/170214-nekroborg-dla-laetitii-gai.html)|
|[Kajetan Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-kajetan-weiner.html)|1|[170212](/rpg/inwazja/opowiesci/konspekty/170212-nieufnosc-w-malym-miescie.html)|
|[Janusz Krzykoł](/rpg/inwazja/opowiesci/karty-postaci/9999-janusz-krzykol.html)|1|[170212](/rpg/inwazja/opowiesci/konspekty/170212-nieufnosc-w-malym-miescie.html)|
|[Henryk Siwiecki](/rpg/inwazja/opowiesci/karty-postaci/1709-henryk-siwiecki.html)|1|[170214](/rpg/inwazja/opowiesci/konspekty/170214-nekroborg-dla-laetitii-gai.html)|
|[Fiodor Pyszczek](/rpg/inwazja/opowiesci/karty-postaci/9999-fiodor-pyszczek.html)|1|[170214](/rpg/inwazja/opowiesci/konspekty/170214-nekroborg-dla-laetitii-gai.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|1|[170212](/rpg/inwazja/opowiesci/konspekty/170212-nieufnosc-w-malym-miescie.html)|

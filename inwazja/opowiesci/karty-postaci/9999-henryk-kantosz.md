---
layout: inwazja-karta-postaci
categories: profile
title: "Henryk Kantosz"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170620|jeden z wyznawców Luksji, którego Henryk magią mentalną wrobił w bunt przeciw Luksji by ją rozwścieczyć i skłonić do wpadnięcia w pułapkę. Z powodzeniem.|[Pułapka na Luksję](/rpg/inwazja/opowiesci/konspekty/170620-pulapka-na-luksje.html)|10/02/13|10/02/15|[Córka Lucyfera](/rpg/inwazja/opowiesci/konspekty/kampania-corka-lucyfera.html)|
|170603|bogacz; chce wyleczyć syna z homoseksualizmu i przyszedł z tym do Miszy. Luksja wygoniła go; wiał jak zając.|[Córka jest narzędziem?](/rpg/inwazja/opowiesci/konspekty/170603-corka-jest-narzedziem.html)|10/02/11|10/02/12|[Córka Lucyfera](/rpg/inwazja/opowiesci/konspekty/kampania-corka-lucyfera.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Luksja Pandemoniae](/rpg/inwazja/opowiesci/karty-postaci/9999-luksja-pandemoniae.html)|2|[170620](/rpg/inwazja/opowiesci/konspekty/170620-pulapka-na-luksje.html), [170603](/rpg/inwazja/opowiesci/konspekty/170603-corka-jest-narzedziem.html)|
|[Henryk Siwiecki](/rpg/inwazja/opowiesci/karty-postaci/1709-henryk-siwiecki.html)|2|[170620](/rpg/inwazja/opowiesci/konspekty/170620-pulapka-na-luksje.html), [170603](/rpg/inwazja/opowiesci/konspekty/170603-corka-jest-narzedziem.html)|
|[Estrella Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-estrella-diakon.html)|2|[170620](/rpg/inwazja/opowiesci/konspekty/170620-pulapka-na-luksje.html), [170603](/rpg/inwazja/opowiesci/konspekty/170603-corka-jest-narzedziem.html)|
|[Zenobi Klepiczek](/rpg/inwazja/opowiesci/karty-postaci/9999-zenobi-klepiczek.html)|1|[170620](/rpg/inwazja/opowiesci/konspekty/170620-pulapka-na-luksje.html)|
|[Stella Stellaris](/rpg/inwazja/opowiesci/karty-postaci/9999-stella-stellaris.html)|1|[170603](/rpg/inwazja/opowiesci/konspekty/170603-corka-jest-narzedziem.html)|
|[Paweł Parobek](/rpg/inwazja/opowiesci/karty-postaci/9999-pawel-parobek.html)|1|[170620](/rpg/inwazja/opowiesci/konspekty/170620-pulapka-na-luksje.html)|
|[Misza Dobroniewiec](/rpg/inwazja/opowiesci/karty-postaci/9999-misza-dobroniewiec.html)|1|[170603](/rpg/inwazja/opowiesci/konspekty/170603-corka-jest-narzedziem.html)|
|[Kalina Cząberek](/rpg/inwazja/opowiesci/karty-postaci/9999-kalina-czaberek.html)|1|[170603](/rpg/inwazja/opowiesci/konspekty/170603-corka-jest-narzedziem.html)|
|[Janina Jasionek](/rpg/inwazja/opowiesci/karty-postaci/9999-janina-jasionek.html)|1|[170603](/rpg/inwazja/opowiesci/konspekty/170603-corka-jest-narzedziem.html)|
|[Filip Cząberek](/rpg/inwazja/opowiesci/karty-postaci/9999-filip-czaberek.html)|1|[170603](/rpg/inwazja/opowiesci/konspekty/170603-corka-jest-narzedziem.html)|
|[Dżony Słomian](/rpg/inwazja/opowiesci/karty-postaci/9999-dzony-slomian.html)|1|[170620](/rpg/inwazja/opowiesci/konspekty/170620-pulapka-na-luksje.html)|
|[Balbina Wróblewska](/rpg/inwazja/opowiesci/karty-postaci/9999-balbina-wroblewska.html)|1|[170620](/rpg/inwazja/opowiesci/konspekty/170620-pulapka-na-luksje.html)|
|[Antoni Bieguś](/rpg/inwazja/opowiesci/karty-postaci/9999-antoni-biegus.html)|1|[170620](/rpg/inwazja/opowiesci/konspekty/170620-pulapka-na-luksje.html)|

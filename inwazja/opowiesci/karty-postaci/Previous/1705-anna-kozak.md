---
layout: inwazja-karta-postaci
categories: profile
factions: "Srebrna Świeca"
type: "NPC"
title: "Anna Kozak"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **buntowniczka**: "nie zgadzam się na to wszystko - na to, że się mną bawią, że bawią się ludźmi, że pozwalają sobie na wszystko. Potęga NIE oznacza, że wolno."
* **daredevil**: "Kto nie ryzykuje, ten nie wygrywa. Uda mi się lub nie, ale przynajmniej próbowałam, nie to co inni. Im wyżej mierzysz, tym wyżej trafisz."
* **supremacjonistka - ludzie**: "Magowie są potężniejsi, więc powinni się posunąć. Ludzi jest więcej i są dużo LEPSI niż magowie - milsi, przyjemniejsi…"

### Zachowania (jaka jest)

* **idealistka**: "To, co robię jest właściwe. Jestem jak… BATMAG! Przynoszę sprawiedliwość i porządek tam, gdzie ich brakuje."
* **impulsywna**: "Ach tak?! Nie, nie ma mowy!"
* **niesubordynacja**: "Nikt nie będzie mi rozkazywać; sama wiem, co mam robić. Oni tylko dbają o własne interesy."

## Co umie (co postać potrafi):

### Specjalizacje

* **podkładanie śladów** (detektyw)
* **zasadzka** (terrorystka)
* **gubienie pościgu** (wszędzie wlezie)

### Umiejętności

* **scavenger**
* **artefaktor**
* **terrorystka (wybuchy)**
* **wszędzie wlezie**
* **nikt jej nie zauważy**
* **detektyw**
* **aktorka**
* **kandydatka na terminusa**
* **węzły, unieruchamianie, porywanie**
* **survival**
* **włamywacz**

### Cechy:

|  Social Aggressive |  Social Devious |  Social Friendly |  Knowledge |  Craft |  Supernatural |  Fortitude |  Nimbleness |
|:==================:|:===============:|:================:|:==========:|:======:|:=============:|:==========:|:===========:|
|       +1           |         0       |        -1        |     -1     |   +1   |       0       |      0     |      0      |

## Magia

### Szkoły magiczne
* **magia zmysłów**
* **technomancja**
* **magia transportu**

### Zaklęcia statyczne

### Specjalne
**brak**

## Zasoby i otoczenie

### Powiązane frakcje

* Srebrna Świeca
* wychowanka Tamary

### Kogo zna

* **szerokie znajomości wśród ludzi**
* **możliwe wsparcie Tamary**

### Co ma do dyspozycji:

* **mnóstwo ludzkiego sprzętu ze złomowisk itp**
* **kryjówki i 'bazy'**
* **sprzęt do maskowania i przebieranek**
* **pozastawiane pułapki w miejscach odwrotu**

### Surowce

* **Wartość**: 1
niewielkie wsparcie ze strony Świecy i niewielkie dochody. Za to, dzielnie scavengeruje wszystko, co się da

# Opis
A young and an impulsive daredevil. This one acts as if she wanted to die, simply because she lacks the common sense of what should be done and when it should be done. A terminus candidate, very resourceful, with her human memory scrambled.

She doesn’t remember anything about her past, except a location of some type; the earliest things she remembers is Tamara, her caregiver from the Silver Candle. She really, really dislikes what the magi have done to her and the fact they cut her away from the humanity.

A rebel by nature, she does not really listen to the authority and often pay the price for that – does not have many friends, doesn’t have political clout and is usually too stubborn, therefore she is the one who gets punished. This only reinforces her “it is not fair, everyone is out there to get me” mentality, which makes for one fun loop.

Ann is very proactive and resourceful. She does things considered to be impossible by even more skilled magi; she simply does them differently using mostly the human stuff. She could be considered to be a human supremationist in the magi ranks – she strongly believes that humans should not be exploited and that human affairs are more important than magi affairs; because magi are more powerful, they should move aside and help humans. Even at the cost of their own needs and goals.
## Motto

# Historia:



## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|170101|wpierw podłączona do kralotha jako "podwładny * mag kralotha" (dywersja), w wyniku czego się odeń uzależniła. Rafael podejrzewa "krew, erotykę lub kralotha" w jej Wzorze. Odesłana do Millennium na leczenie.|[Patrol? Kralotyczne maskowanie!](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|10/07/13|10/07/15|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161101|w imieniu Andrei zdobywa zapasy finansowe i służy jako biznesswoman; też: zaproponowała miejsca w świecie ludzi do chowania się.|[Bezwzględna Lady Terminus](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|10/06/25|10/06/28|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160229|która przyznała się Andrei do bycia "Bat* magiem". Andrea podejrzewa, że ta idealistka w swoim łóżku nie umrze.|[Siedmiu magów - nie Mausów](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html)|10/06/08|10/06/13|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160210|Bat* mag, tajemnicza postać, która osiągnęła co chciała - "Wyżerka" została zamknięta rękami Hektora Blakenbauera.|[Batmag Uderza!](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html)|10/06/04|10/06/05|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|141012|uczennica na terminusa z Rodziny Świecy która nie dała się złamać|[Aplikanci widzieli gorathaula](/rpg/inwazja/opowiesci/konspekty/141012-aplikanci-widzieli-gorathaula.html)|10/05/30|10/06/07|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|
|150922|która nie ufa Świecy, chce chronić Poczciwca i się buntuje jak mała dziewczynka. Zobowiązania do odzyskania artefaktu.|[Och, nie! Porwali Ignata!](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html)|10/05/29|10/05/30|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150913|która okazała się być oddana do Rodziny Świecy przez Tamarę i która okazała się być tajemniczym atakujowcem KADEMowego poligonu. Naraziła Maskaradę dla ludzi. Nie chce być pionkiem. Zazdrosna o Salazara.|[Andrea węszy koło Szlachty](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html)|10/05/19|10/05/20|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150313|podkochująca się w Salazarze czarodziejka nie traktowana przez nikogo poważnie. Zazdrosna o Salazara, skupia się na Dracenie.|[Ile tam było szczepow Spustoszenia](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html)|10/04/22|10/04/24|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170108|głupio porwała Silurię. Złapana. Przerażona, współpracuje jak może. A tylko chciała pomagać ludziom...|[Samotna w świecie magow](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html)|10/02/12|10/02/16|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|140625|porwana przez Saith Kameleon jako źródło żywności (osobowości). Pożarta przez Kameleon. Nigdy nie zostanie już terminuską. KIA.|[Ostatnia Saith](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html)|10/01/19|10/01/20|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|
|140208|podkochująca się w Kaspianie kandydatka na terminuskę o wrogim do Tatiany stosunku.|[Na ratunek terminusce](/rpg/inwazja/opowiesci/konspekty/140208-na-ratunek-terminusce.html)|10/01/13|10/01/14|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|Andrea Wilgacz|7|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html), [160229](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html), [150922](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html), [150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html), [140625](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html), [140208](/rpg/inwazja/opowiesci/konspekty/140208-na-ratunek-terminusce.html)|
|Marian Agrest|6|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html), [160229](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html), [150922](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html), [150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html), [140625](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html)|
|Tamara Muszkiet|3|[160229](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html), [150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html), [150313](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html)|
|Tadeusz Baran|3|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html), [160229](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html)|
|Siluria Diakon|3|[150922](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html), [150313](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html), [170108](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html)|
|Salazar Bankierz|3|[141012](/rpg/inwazja/opowiesci/konspekty/141012-aplikanci-widzieli-gorathaula.html), [150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html), [150313](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html)|
|Ozydiusz Bankierz|3|[160229](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html), [150922](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html), [150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html)|
|Marian Łajdak|3|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html), [170108](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html)|
|Judyta Karnisz|3|[160229](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html), [150922](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html), [150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html)|
|Joachim Kopiec|3|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html), [141012](/rpg/inwazja/opowiesci/konspekty/141012-aplikanci-widzieli-gorathaula.html)|
|Wojciech Tecznia|2|[140625](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html), [140208](/rpg/inwazja/opowiesci/konspekty/140208-na-ratunek-terminusce.html)|
|Wacław Zajcew|2|[140625](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html), [140208](/rpg/inwazja/opowiesci/konspekty/140208-na-ratunek-terminusce.html)|
|Tatiana Zajcew|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [140208](/rpg/inwazja/opowiesci/konspekty/140208-na-ratunek-terminusce.html)|
|Rafael Diakon|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|Milena Diakon|2|[150922](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html), [150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html)|
|Mieszko Bankierz|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|Lidia Weiner|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|Kajetan Weiner|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|Juliusz Szaman|2|[140625](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html), [140208](/rpg/inwazja/opowiesci/konspekty/140208-na-ratunek-terminusce.html)|
|Julian Pszczelak|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|Irina Zajcew|2|[150922](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html), [140208](/rpg/inwazja/opowiesci/konspekty/140208-na-ratunek-terminusce.html)|
|Infensa Diakon|2|[150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html), [150313](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html)|
|Ignat Zajcew|2|[150922](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html), [170108](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html)|
|Hektor Blakenbauer|2|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html), [140625](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html)|
|Andżelika Leszczyńska|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|Alojzy Przylaz|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|Aleksander Sowiński|2|[160229](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html), [150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html)|
|Świeży Lilak|1|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|
|Zenon Weiner|1|[150313](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html)|
|Zdzisław Zajcew|1|[140208](/rpg/inwazja/opowiesci/konspekty/140208-na-ratunek-terminusce.html)|
|Wojmił Kopiec|1|[141012](/rpg/inwazja/opowiesci/konspekty/141012-aplikanci-widzieli-gorathaula.html)|
|Wioletta Lemona-Chang|1|[170108](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html)|
|Wioletta Bankierz|1|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|
|Waldemar Zupaczka|1|[140625](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html)|
|Urszula Murczyk|1|[170108](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html)|
|Tomasz Jamnik|1|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html)|
|Szymon Skubny|1|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html)|
|Stalowy Śledzik Żarłacz|1|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|
|Sebastian Tecznia|1|[141012](/rpg/inwazja/opowiesci/konspekty/141012-aplikanci-widzieli-gorathaula.html)|
|Saith Kameleon|1|[140625](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html)|
|Saith Catapult|1|[160229](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html)|
|Rukoliusz Bankierz|1|[170108](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html)|
|Rudolf Jankowski|1|[161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|Rodion Zajcew|1|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|
|Quasar|1|[140625](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html)|
|Paweł Sępiak|1|[150313](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html)|
|Paulina Tarczyńska|1|[141012](/rpg/inwazja/opowiesci/konspekty/141012-aplikanci-widzieli-gorathaula.html)|
|Patrycja Krowiowska|1|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html)|
|Pafnucy Zieczar|1|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html)|
|Oliwier Bonwant|1|[170108](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html)|
|Oktawian Maus|1|[150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html)|
|Nikola Kamień|1|[141012](/rpg/inwazja/opowiesci/konspekty/141012-aplikanci-widzieli-gorathaula.html)|
|Mojra|1|[140625](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html)|
|Melodia Diakon|1|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|
|Marta Szysznicka|1|[150313](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html)|
|Marian Welkrat|1|[150313](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html)|
|Maria Newa|1|[141012](/rpg/inwazja/opowiesci/konspekty/141012-aplikanci-widzieli-gorathaula.html)|
|Marcelin Blakenbauer|1|[140208](/rpg/inwazja/opowiesci/konspekty/140208-na-ratunek-terminusce.html)|
|Marcel Bankierz|1|[150313](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html)|
|Maja Kos|1|[140625](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html)|
|Leonidas Blakenbauer|1|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html)|
|Leokadia Myszeczka|1|[160229](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html)|
|Laurena Bankierz|1|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|
|Laragnarhag|1|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|
|Kleofas Bór|1|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html)|
|Klemens X|1|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html)|
|Klara Blakenbauer|1|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html)|
|Kermit Diakon|1|[150922](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html)|
|Kaspian Bankierz|1|[140208](/rpg/inwazja/opowiesci/konspekty/140208-na-ratunek-terminusce.html)|
|Karolina Maus|1|[141012](/rpg/inwazja/opowiesci/konspekty/141012-aplikanci-widzieli-gorathaula.html)|
|Karolina Kupiec|1|[170108](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html)|
|Karol Poczciwiec|1|[150922](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html)|
|Julia Weiner|1|[150313](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html)|
|Jan Anioł Bankierz|1|[160229](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html)|
|Irena Resort|1|[150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html)|
|Ika|1|[140208](/rpg/inwazja/opowiesci/konspekty/140208-na-ratunek-terminusce.html)|
|Igor Daczyn|1|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html)|
|Grzegorz Czerwiec|1|[140625](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html)|
|Gabriela Resort|1|[150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html)|
|GS "Aegis" 0003|1|[150922](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html)|
|Felicjan Weiner|1|[161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|Ernest Maus|1|[150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html)|
|Elizawieta Zajcew|1|[150922](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html)|
|Draconis Diakon|1|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|
|Dracena Diakon|1|[150313](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html)|
|Dominik Bankierz|1|[150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html)|
|Dariusz Kopyto|1|[140625](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html)|
|Dalia Weiner|1|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|
|Dagmara Wyjątek|1|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|
|Bożena Zajcew|1|[140208](/rpg/inwazja/opowiesci/konspekty/140208-na-ratunek-terminusce.html)|
|Bogdan Kimaroj|1|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html)|
|Benjamin Zajcew|1|[140625](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html)|
|Aurelia Maus|1|[150313](/rpg/inwazja/opowiesci/konspekty/150313-ile-tam-bylo-szczepow-spustoszenia.html)|
|Alina Bednarz|1|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html)|
|Adela Maus|1|[150913](/rpg/inwazja/opowiesci/konspekty/150913-andrea-weszy-kolo-szlachty.html)|

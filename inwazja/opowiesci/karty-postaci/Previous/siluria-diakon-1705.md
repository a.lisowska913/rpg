---
layout: inwazja-karta-postaci
title:  "Siluria Diakon"
categories: profile
guild: "KADEM"
type: PC
---
# {{ page.title }}

## Postać

### Motywacje (do czego dąży)
- **tyranka tyranów**: "im większym jesteś tyranem, tym bardziej będziesz jej podległy"
- **zawsze warto nawiązać nowe znajomości**: "nigdy nie wiesz, kiedy ktoś okaże się pomocny"
- **dominatorka**: "przejąć kontrolę w rękawiczkach"
- **magiczna swatka**: "każdy mag ma swoją drugą, genetyczną połówkę. Jeśli uda się ich połączyć tak, aby byli szczęśliwi, mamy potomstwo z ogromnym potencjałem i szczęśliwą rodzinę... Same zalety. Każde następne pokolenie powinno być genetycznie lepsze od rodziców."
- **stworzyć bank genetyczny magów**: "jeśli ktoś nie może znaleźć drugiej połówki, ale bardzo chce mieć dzieci, chcę mu dać możliwość doboru idealnego materiału genetycznego"

### Zachowania (jaka jest)
- **rozbrajająco urocza**: "sprawić, by uważano ją za niegroźną" 
- **maskotka wszystkich**: "sprawić, by wszyscy ją lubili"
- **prawdziwy Diakon**: "zadowolić wszystkich"
- **prawdziwy Diakon**: "jak da się przelecieć, da się z tym pracować"
- **śliska jak piskorz**: "nie dać się złapać ani przyłapać"
- **bezsensowne okrucieństwo jest karalne**: "tak się nie godzi, nawet zwierzęta tak nie robią"
- **po co walczyć, skoro możemy się dobrze bawić?**:

### Specjalizacje
- **dostaję to, o co proszę (maskotka wszystkich, manipulatorka, corruptor, uwodzicielka)**: "to przecież drobiazg..."
- **oblicze dla każdego (manipulatorka, uwodzicielka)**: "jak mnie widzisz? tak, jak ja chcę"
- **węzły (bondage)**: "jak już kogoś zwiążę, to pozostanie związany... chyba, że chcę, aby się uwolnił"
- **zrobisz, co zechcę (uwodzicielka, manipulatorka, corruptor)**: "nie muszę zastraszać... po co?"
- **swatka (manipulatorka, uwodzicielka, polityk)**: "czasem trzeba połączyć niektóre pary, bo sami nie dadzą sobie rady..."
- **genetyka istot magicznych (lifeshaper)**: "jeśli mam kogoś swatać, to chcę wiedzieć, co z tego wyjdzie"
- **będziesz mnie pragnąć**: Siluria potrafi sprawić, że partner(ka?) będzie odczuwać zwiększone pożądanie dzięki dopasowaniu się na poziomie biologicznym
- **magia urody i kosmetyczna**:
- **magia imprezowa**:

### Umiejętności

- **uwodzicielka**: "jestem ładna, potrafię rozkochać w sobie każdego... dlaczego z tego nie korzystać zwłaszcza, jeśli staram się nie krzywdzić?"
- **bondage**: "większość to lubi, nawet jeśli o tym nie wie"
- **manipulatorka**: "osiągnąć swój cel można na różne sposoby."
- **maskotka wszystkich**: "kiedy wszyscy dookoła Cię lubią, rzeczy same się dzieją"
- **corruptor**: "niemal każdego da się przekonać do niemal wszystkiego. Kwestia doboru argumentów"
- **polityk**: "muszę wiedzieć, na ile mogę sobie pozwolić"
- **imprezowiczka**: "impreza ze mną będzie jeszcze lepsza!"
- **plotki**: "muszę być na bieżąco"
- **facylitator**: "jestem smarem w trybach grupy"
- **walka wręcz**: "niestety, zdarza mi się wpaść w kłopoty. Dobrze móc się obronić"
- **wiedza rodowa**:
- **uczennica KADEMu**: "KADEM to uczelnia o szerokim portfolio - **każdy coś dla siebie znajdzie a ja spróbowałam chyba wszystkiego! no dobrze... miałam kochanków ze wszystkich Intytutów"



### Cechy:

|  Social Aggressive |  Social Devious |  Social Friendly |  Knowledge |  Craft |  Supernatural |  Fortitude |  Nimbleness |
|:==================:|:===============:|:================:|:==========:|:======:|:=============:|:==========:|:===========:|
|       -1           |         +1      |        +1        |      -1    |   -1   |        0      |      0     |     +1      |

### Specjalne

Diakon czystej krwi; prawdziwy Diakon

## Magia

### Szkoły magiczne

- **biomancja**: "naturę czasem trzeba poprawić"
- **technomancja**: "technologia przydaje się nawet w życiu maga"
- **magia zmysłów**:
- **magia mentalna**:

### Zaklęcia statyczne
- **pre-ekstaza Tormentii**: utrzymuje na granicy ekstazy; tuż przed
- **pokój wszelkich przyjemności Diakonów**
- **adaptacja Amnestii Diakon**: zaklęcie dostosowujące istotę do podświadomie postrzeganej przez nią potrzeby
- **wymuszona adaptacja Amnestii Diakon**: zaklęcie dostosowujące istotę do potrzeby podświadomie postrzeganej przez rzucającego

## Zasoby i otoczenie

### Powiązane frakcje
* KADEM
* Millenium
* Diakonowie

### Kogo zna

- adoratorów 
- magów z wszystkich ważnych gildii i większości nieważnych 

### Co ma do dyspozycji:

- przysługi u magów KADEMu
- Reputacja osoby, z którą można się dobrze zabawić
- więzy rodzinne
- przysługi u magów spoza KADEMu
- wyznania łóżkowe
- gadżety - prezenty od magów
- szerokie znajomości wśród ludzi i magów

### Surowce
2

# Opis

krótki opis postaci, rzeczy, jakie warto pamiętać.

### Koncept

Wysokopoziomowy koncept postaci, 1-2 zdania.

### Motto

"Tekst"

# Historia
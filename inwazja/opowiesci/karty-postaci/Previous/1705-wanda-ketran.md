---
layout: inwazja-karta-postaci
categories: profile
factions: "Iliusitius, Niezrzeszeni"
type: "NPC"
title: "Wanda Ketran"
---
# {{ page.title }}

## Postać

### Motywacje (do czego dąży)

* **społecznik**: wszyscy są wartościowi, szanse należą się wszystkim
* **obrońca**: (słabsi) im ktoś jest słabszy tym bardziej należy im pomóc!

### Zachowania (jaka jest)

* **pomocna**: jeżeli nikomu nie pomagasz, po co żyjesz?
* **uparta**: wywalą mnie drzwiami, wlezę oknem
* **zaciekła**: (walka klas) ja nie ustąpię; nie pozwolę zwyciężyć hipokryzji i apatii
* **ideowiec**: nie sprzedam swych poglądów za spokój czy 30 srebrników

### Specjalizacje
* **Kopalin** (życie w mieście)
* **call to action** (charyzmatyczna mówczyni)
* **ucieczki i unikanie** (dywersja)

### Umiejętności
* **życie w mieście; przewodnik**
* **organizatorka**
* **charyzmatyczna mówczyni**
* **marketing internetowy**
* **cybergothka**
* **dywersja**
* **wszędzie przeżyje**
* **wiedza o Blakenbauerach**
* **wiedza o możnych tego świata**
* **walka uliczna**
* **rękodzieło; praca w drewnie**
* **"meble" improwizowane**

### Cechy

|  Social Aggressive |  Social Devious |  Social Friendly |  Knowledge |  Craft |  Supernatural |  Fortitude |  Nimbleness |
|:==================:|:===============:|:================:|:==========:|:======:|:=============:|:==========:|:===========:|
|       +1           |         0       |        +1        |     -1     |   +1   |      -1       |      0     |     -1      |

### Specjalne
* **brak**

## Magia

**brak**

## Zasoby i otoczenie

### Powiązane frakcje

* nazwa_frakcji_jak_ma_to_z_linkiem

### Kogo zna

* anarchiści w Kopalinie
* wolna i-telewizja "Lupa"
* grupy samopomocy na Śląsku
* grupy kreatywne na Śląsku
* ekipa chodząca do z "Działa Plazmowego"
* podkochujący się w niej Joachim Zajcew
* firma "Skrzydłoróg" i Dagmara Czeluść

### Co ma do dyspozycji:

* **sprzęt_do_jakiego_ma_dostęp_i_postać_charakteryzuje**: opis
* **sprzęt_do_jakiego_ma_dostęp_i_postać_charakteryzuje**: opis

### Surowce

* **Wartość**: 1/2/3
* **Pochodzenie**: dlaczego_1_2_lub_3_i_jak_zarabia

# Opis

25-letnia cybergothka. Przyjaciółka Dagmary Czeluść (właścicielki Skrzydłorogu). Nie jest szczególnie bogata, ale niczego jej nie brakuje; potrafi radzić sobie w najróżniejszych warunkach. Osoba bardzo społeczna, o silnych poglądach pomocy innym i zadziornym charakterze. Niezła w mimikrze; nie pokazuje co naprawdę myśli. Niezależna, walczy z nierównościami społecznymi i klasowymi. Zdeterminowana by świat był lepszy. "Rage against the dying of the light". Nie jest samotna, potrafi organizować ludzi dookoła swojej wizji. Proaktywna jak cholera, nie pozwoli rzeczywistości zamknąć jej w klatce.
Potrafi się doskonale ukrywać; ma wielu przyjaciół i jest cierpliwa. Zna większość miast. Wie co to znaczy ubóstwo. Potrafi wiele poświęcić, by osiągnąć swoje cele. NIE jest radykalna; jest racjonalna i przyjacielska.

### Koncept

Wysokopoziomowy koncept postaci, 1-2 zdania.

### Motto

"Tekst"

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|[Wandy wolność i wróżda](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|ma nałożoną na siebie Matrycę Iliusitiusa|Taniec Liści|
|[Wandy wolność i wróżda](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|ma prawo do wróżdy wobec Rodu Blakenbauerów|Taniec Liści|
|[Wandy wolność i wróżda](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|wydostała się z Rezydencji Blakenbauerów mocą Iliusitiusa|Taniec Liści|


## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|160921|34 klon stworzony przez Tymotheusa by "dowody" Tańca Liści zneutralizować. Poświęciła się Iliusitiusowi, by wyrwać Blakenbauerom prawdziwą Wandę.|[Wandy wolność i wróżda](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|10/07/22|10/07/24|[Taniec Liści](/rpg/inwazja/opowiesci/konspekty/kampania-taniec-lisci.html)|
|160615|faktycznie nieprzytomna i nieaktywna w Rezydencji Tymotheusa. |[Morderczyni w masce Wandy](/rpg/inwazja/opowiesci/konspekty/160615-morderczyni-w-masce-wandy.html)|10/06/25|10/06/26|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160506|którą przecież Tymotheus usunął... a jednak wróciła, oskarżać Hektora w najgorszym możliwym momencie. Poza Kopalinem.|[Wyścig pająka z terminuską](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|10/06/22|10/06/24|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|151212|agentka Arazille w Rezydencji Blakenbauerów, przekonała Marcelina do ucieczki.|[Antyporadnik wędkarza Arazille](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html)|10/06/19|10/06/20|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|151119|która choć w części jest autonomiczna, jednak Edwin jej zaprogramował usuwanie danych przed Hektorem.|[Patrycja węszy szpiega](/rpg/inwazja/opowiesci/konspekty/151119-patrycja-weszy-szpiega.html)|10/06/08|10/06/12|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150930|której przeczytana pamięć posłużyła do bezproblemowego jej zniknięcia. Okazało się, że jest całkiem wpływowa.|[O Wandzie co Zajcewa nie chciała](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html)|10/05/29|10/05/30|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150820|"cybergothka z motorem i dronem". Detektyw, agent specjalny i ofiara porwania. Już: dziewczyna Marcelina.|[Klemens w roli swatki](/rpg/inwazja/opowiesci/konspekty/150820-klemens-w-roli-swatki.html)|10/05/19|10/05/20|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150408|24-letnia cybergothka, która nie obawia się niczego i nie ma nic do stracenia; stała się idolką młodych buntowników. Też: agentka Arazille.|[Rykoszet zimnej wojny](/rpg/inwazja/opowiesci/konspekty/150408-rykoszet-zimnej-wojny.html)|10/05/01|10/05/02|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|7|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html), [160615](/rpg/inwazja/opowiesci/konspekty/160615-morderczyni-w-masce-wandy.html), [151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html), [151119](/rpg/inwazja/opowiesci/konspekty/151119-patrycja-weszy-szpiega.html), [150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html), [150820](/rpg/inwazja/opowiesci/konspekty/150820-klemens-w-roli-swatki.html), [150408](/rpg/inwazja/opowiesci/konspekty/150408-rykoszet-zimnej-wojny.html)|
|[Alina Bednarz](/rpg/inwazja/opowiesci/karty-postaci/1709-alina-bednarz.html)|6|[160615](/rpg/inwazja/opowiesci/konspekty/160615-morderczyni-w-masce-wandy.html), [151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html), [151119](/rpg/inwazja/opowiesci/konspekty/151119-patrycja-weszy-szpiega.html), [150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html), [150820](/rpg/inwazja/opowiesci/konspekty/150820-klemens-w-roli-swatki.html), [150408](/rpg/inwazja/opowiesci/konspekty/150408-rykoszet-zimnej-wojny.html)|
|[Patrycja Krowiowska](/rpg/inwazja/opowiesci/karty-postaci/1709-patrycja-krowiowska.html)|4|[151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html), [151119](/rpg/inwazja/opowiesci/konspekty/151119-patrycja-weszy-szpiega.html), [150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html), [150408](/rpg/inwazja/opowiesci/konspekty/150408-rykoszet-zimnej-wojny.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1707-hektor-blakenbauer.html)|4|[160615](/rpg/inwazja/opowiesci/konspekty/160615-morderczyni-w-masce-wandy.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html), [151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html), [150408](/rpg/inwazja/opowiesci/konspekty/150408-rykoszet-zimnej-wojny.html)|
|[Dionizy Kret](/rpg/inwazja/opowiesci/karty-postaci/1707-dionizy-kret.html)|4|[160615](/rpg/inwazja/opowiesci/konspekty/160615-morderczyni-w-masce-wandy.html), [151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html), [151119](/rpg/inwazja/opowiesci/konspekty/151119-patrycja-weszy-szpiega.html), [150408](/rpg/inwazja/opowiesci/konspekty/150408-rykoszet-zimnej-wojny.html)|
|[Otton Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-otton-blakenbauer.html)|3|[160615](/rpg/inwazja/opowiesci/konspekty/160615-morderczyni-w-masce-wandy.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html), [151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html)|
|[Margaret Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-margaret-blakenbauer.html)|3|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html), [151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html)|
|[Marcelin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1707-marcelin-blakenbauer.html)|3|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html), [151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html), [150408](/rpg/inwazja/opowiesci/konspekty/150408-rykoszet-zimnej-wojny.html)|
|[Anna Góra](/rpg/inwazja/opowiesci/karty-postaci/9999-anna-gora.html)|3|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html), [150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html), [150820](/rpg/inwazja/opowiesci/konspekty/150820-klemens-w-roli-swatki.html)|
|[Szymon Skubny](/rpg/inwazja/opowiesci/karty-postaci/9999-szymon-skubny.html)|2|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html), [151119](/rpg/inwazja/opowiesci/konspekty/151119-patrycja-weszy-szpiega.html)|
|[Stanisław Pormien](/rpg/inwazja/opowiesci/karty-postaci/9999-stanislaw-pormien.html)|2|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html), [150820](/rpg/inwazja/opowiesci/konspekty/150820-klemens-w-roli-swatki.html)|
|[Mojra](/rpg/inwazja/opowiesci/karty-postaci/9999-mojra.html)|2|[151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html), [150408](/rpg/inwazja/opowiesci/konspekty/150408-rykoszet-zimnej-wojny.html)|
|[Kleofas Bór](/rpg/inwazja/opowiesci/karty-postaci/1709-kleofas-bor.html)|2|[160615](/rpg/inwazja/opowiesci/konspekty/160615-morderczyni-w-masce-wandy.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Klemens X](/rpg/inwazja/opowiesci/karty-postaci/9999-klemens-x.html)|2|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html), [150820](/rpg/inwazja/opowiesci/konspekty/150820-klemens-w-roli-swatki.html)|
|[Klara Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1707-klara-blakenbauer.html)|2|[160615](/rpg/inwazja/opowiesci/konspekty/160615-morderczyni-w-masce-wandy.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Arazille](/rpg/inwazja/opowiesci/karty-postaci/9999-arazille.html)|2|[151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html), [150408](/rpg/inwazja/opowiesci/konspekty/150408-rykoszet-zimnej-wojny.html)|
|[Zdzisław Kamiński](/rpg/inwazja/opowiesci/karty-postaci/1705-zdzislaw-kaminski.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Wiktor Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1705-wiktor-sowinski.html)|1|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Vladlena Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1705-vladlena-zjacew.html)|1|[151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html)|
|[Tymotheus Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-tymotheus-blakenbauer.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Tymoteusz Maus](/rpg/inwazja/opowiesci/karty-postaci/1705-tymoteusz-maus.html)|1|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html)|
|[Tomasz Ormię](/rpg/inwazja/opowiesci/karty-postaci/9999-tomasz-ormie.html)|1|[150820](/rpg/inwazja/opowiesci/konspekty/150820-klemens-w-roli-swatki.html)|
|[Tomasz Kracy](/rpg/inwazja/opowiesci/karty-postaci/9999-tomasz-kracy.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-siluria-diakon.html)|1|[151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html)|
|[Romeo Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-romeo-diakon.html)|1|[151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html)|
|[Olga Miodownik](/rpg/inwazja/opowiesci/karty-postaci/1709-olga-miodownik.html)|1|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html)|
|[Oddział Zeta](/rpg/inwazja/opowiesci/karty-postaci/9999-oddzial-zeta.html)|1|[151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html)|
|[Miron Ataman](/rpg/inwazja/opowiesci/karty-postaci/9999-miron-ataman.html)|1|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html)|
|[Milena Marzec](/rpg/inwazja/opowiesci/karty-postaci/9999-milena-marzec.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Marian Rokita](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-rokita.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Marian Agrest](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-agrest.html)|1|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Marek Rudzielec](/rpg/inwazja/opowiesci/karty-postaci/9999-marek-rudzielec.html)|1|[150408](/rpg/inwazja/opowiesci/konspekty/150408-rykoszet-zimnej-wojny.html)|
|[Leszek Żółty](/rpg/inwazja/opowiesci/karty-postaci/9999-leszek-zolty.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Kinga Melit](/rpg/inwazja/opowiesci/karty-postaci/9999-kinga-melit.html)|1|[160615](/rpg/inwazja/opowiesci/konspekty/160615-morderczyni-w-masce-wandy.html)|
|[Karolina Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-karolina-maus.html)|1|[160615](/rpg/inwazja/opowiesci/konspekty/160615-morderczyni-w-masce-wandy.html)|
|[Kajetan Pyszak](/rpg/inwazja/opowiesci/karty-postaci/9999-kajetan-pyszak.html)|1|[150820](/rpg/inwazja/opowiesci/konspekty/150820-klemens-w-roli-swatki.html)|
|[Jurij Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-jurij-zajcew.html)|1|[151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html)|
|[Joachim Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-zajcew.html)|1|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html)|
|[Jan Szczupak](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-szczupak.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Iliusitius](/rpg/inwazja/opowiesci/karty-postaci/9999-iliusitius.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Hubert Brodacz](/rpg/inwazja/opowiesci/karty-postaci/1709-hubert-brodacz.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Himechan](/rpg/inwazja/opowiesci/karty-postaci/9999-himechan.html)|1|[151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html)|
|[Grzegorz Śliwa](/rpg/inwazja/opowiesci/karty-postaci/9999-grzegorz-sliwa.html)|1|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html)|
|[Grzegorz Włóczykij](/rpg/inwazja/opowiesci/karty-postaci/9999-grzegorz-wloczykij.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Grażyna Remska](/rpg/inwazja/opowiesci/karty-postaci/9999-grazyna-remska.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Fryderyk Mruczek](/rpg/inwazja/opowiesci/karty-postaci/9999-fryderyk-mruczek.html)|1|[150408](/rpg/inwazja/opowiesci/konspekty/150408-rykoszet-zimnej-wojny.html)|
|[Erebos](/rpg/inwazja/opowiesci/karty-postaci/9999-erebos.html)|1|[151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html)|
|[Emilia Kołatka](/rpg/inwazja/opowiesci/karty-postaci/1709-emilia-kolatka.html)|1|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Elizawieta Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-elizawieta-zajcew.html)|1|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Elea Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-elea-maus.html)|1|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Diana Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-diana-weiner.html)|1|[151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html)|
|[Diana Larent](/rpg/inwazja/opowiesci/karty-postaci/9999-diana-larent.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Dagmara Wyjątek](/rpg/inwazja/opowiesci/karty-postaci/1709-dagmara-wyjątek.html)|1|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Dagmara Czeluść](/rpg/inwazja/opowiesci/karty-postaci/9999-dagmara-czelusc.html)|1|[150820](/rpg/inwazja/opowiesci/konspekty/150820-klemens-w-roli-swatki.html)|
|[Czirna Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-czirna-zajcew.html)|1|[151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html)|
|[Czesław Czepiec](/rpg/inwazja/opowiesci/karty-postaci/9999-czeslaw-czepiec.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Borys Kumin](/rpg/inwazja/opowiesci/karty-postaci/1709-borys-kumin.html)|1|[151119](/rpg/inwazja/opowiesci/konspekty/151119-patrycja-weszy-szpiega.html)|
|[Arkadiusz Klusiński](/rpg/inwazja/opowiesci/karty-postaci/9999-arkadiusz-klusinski.html)|1|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Aptoform Mirasilaler](/rpg/inwazja/opowiesci/karty-postaci/9999-aptoform-mirasilaler.html)|1|[150408](/rpg/inwazja/opowiesci/konspekty/150408-rykoszet-zimnej-wojny.html)|
|[Anna Kajak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kajak.html)|1|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html)|
|[Aneta Rainer](/rpg/inwazja/opowiesci/karty-postaci/1709-aneta-rainer.html)|1|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Amanda Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-amanda-diakon.html)|1|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Adrian Murarz](/rpg/inwazja/opowiesci/karty-postaci/9999-adrian-murarz.html)|1|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|

---
layout: inwazja-karta-postaci
categories: profile
factions: "Dare Shiver, Srebrna Świeca"
type: "NPC"
title: "Adonis Sowiński"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **daredevil**: "jeśli nic nie czujesz... to nie żyjesz. Zawsze wybieraj najciekawsze rozwiązanie."
* **prankster**: "nadmiar powagi i patrzenia na świat przez krawat zabija. Nie powinno się unikać zrobienia dobrego żartu ;-)."
* **rywalizacja**: "Bankierze, Zajcewowie, inni Sowińscy... zawsze warto im pokazać, że można pójść ten jeden krok dalej."
* **kocha piękno**: "Nie ma nic doskonalszego, niż po zastrzyku adrenaliny obcować z czymś naprawdę pięknym"

### Zachowania (jaka jest)

* **otwarty**: "wszystkiego należy spróbować. Nie mnie oceniać. Bądź czym chcesz i rób co chcesz."
* **apolityczny**: "te wszystkie tematy związane z gildiami i frakcjami... to nieistotne. Wszyscy i tak umrzemy. Kiedyś."
* **hedonista**: "przyjemność jest najwyższym celem egzystencji - rozprzestrzenianie tej przyjemności to obowiązek."

### Specjalizacje

* **skłanianie do ryzyka**: mało kto tak jak Adonis potrafi namówić innych do wybrania bardziej ryzykownej akcji.
* **gry karciane**: mistrz hazardu i gier karcianych, rzadko przegrywa niezależnie od stawek.
* **bullshit**: nie tylko potrafi się wykpić ale i wmówić innym wiele różnych rzeczy. Dla niego prawda to tylko punkt widzenia.
* **nieuchwytny**: praktycznie nie da się nigdy nic na niego znaleźć ani nie da się udowodnić, że coś zrobił czy zadziałał
* **magia imprezowa**

### Umiejętności

* **inspirujący przywódca**: wybitna osobowość, charyzmatyczna, doskonały w retoryce i porywania ludzi za sobą i swymi pomysłami
* **guru sekty**: ...do poziomu, w którym ludzie i magowie tyci ZA DALEKO za nim idą i nie orientują się, jak bardzo jest to niebezpieczne
* **uwodziciel**: uroda oraz charyzma; bez większego problemu potrafi oczarować i pociągnąć za sobą innych...
* **corruptor**: ...a nawet zmienić ich system wartości i skłonić ich do dołączenia do miłośników ryzyka
* **dbanie o urodę**: Adonis zasłużył na swoje imię obsesyjnym wręcz przywiązaniem do swej urody, doboru strojów...
* **aktorstwo**: niesamowite, jak łatwo jest zachowywać się inaczej, przebrać się za kobietę itp. gdy jest się bishounenem ;-)
* **odwracanie uwagi**: mała dywersja, przygotowanie planu, odrobina chaosu i już może działać swobodnie tam, gdzie mu to potrzebne
* **hazardzista**: zna bardzo wiele gier hazardowych, miejsca, gdzie te gry się toczą oraz jest praktykującym graczem ;-)
* **włamywacz**: ...a jak potrzebny jest dreszczyk emocji, odwiedzi kogoś w nocy czy zostawi ślad "Stefan tu był" czy coś

### Cechy:

|  Social Aggressive |  Social Devious |  Social Friendly |  Knowledge |  Craft |  Supernatural |  Fortitude |  Nimbleness |
|:==================:|:===============:|:================:|:==========:|:======:|:=============:|:==========:|:===========:|
|       +1           |        +1       |        +1        |     -1     |   -1   |      +1       |      0     |     +1      |

### Specjalne

* inklinacje NIE sumują się do zera!!! Sumują się do 3.
* bishounen; nie ma wady przy przebieraniu się za dziewczynę

## Magia

### Szkoły magiczne

* **magia zmysłów**
* **informacja**
* **astralika**

### Zaklęcia statyczne


### Surowce

* **Wartość**: 2
* **Pochodzenie**: standardowo; żyje na wysokim poziomie, więc jego bogactwo nie jest widoczne

## Zasoby i otoczenie

### Powiązane frakcje

* Srebrna Świeca
* Dare Shiver

### Kogo zna:

* **prezydent klubu "Dare Shiver" ("odważ się drżeć")**: to on jest prezydentem; zna sporo osób i jest znany w odpowiednich kręgach
* **magowie, którym pomógł "odnaleźć siebie"**: jego liczni akolici i miłośnicy, ale też sympatycy, którzy TEŻ BY CHCIELI
* **sława wybitnego hazardzisty**: są miejsca, gdzie z nim nawet nie chcą grać :-(. A przynajmniej nie na odpowiednie stawki

### Co ma do dyspozycji:

* **wszechstronne przebrania i stroje**: strój na każdą okazję. Może być każdym, gdy tylko mu to potrzebne czy przydatne
* **eliksiry i gry imprezowe (non-dangerous stuff)**: standardowy sprzęt potrzebny do funkcjonowania Dare Shiver i rozweselania ludzi - czy robienia im dowcipów
* **zaprzyjaźnione miejsca na Śląsku (non-dangerous stuff)**: z uwagi na swoją hojność i działalność, jest miło widziany w wielu miejscach
* **mapy i informacje**: mało kto tak jak on ma dostęp do informacji o miejscach, systemach obronnych, z kim co można... 
* **kompromitujące materiały**: praktycznie jest w stanie zaszantażować i skompromitować sporo osób... tyle, że nie chce

# Opis

Adonis - a beautiful and carefree bishounen. Or rather, what he would like others to think about him. He is extremely beautiful to the point is often considered to be one of Diakon bloodline, however behind the beautiful visage lies a competitive daredevil.

This mage is completely apolitical. He doesn’t care about politics, guilds etc. he loves the beauty and loves – absolutely loves – the feel of adrenaline. He is also a prankster and likes putting others in embarrassing positions, although does not want to hurt anyone. Everything he does it’s supposed to be amazing experiences for everyone - especially in retrospect. A golden youth.

A very good gambler and risk taker, incredibly difficult to catch and a president of the “Dare Shiver” club, this one – although not dangerous in terms of meaning of the word – is known to be sometimes irritating.

In terms of magic, he specializes in the beauty magic, party magic and illusion magic; especially in the regions which allow him to stay undetected, observe, never get caught.

## Motto

"Gdyby kózka nie skakała... by niczego nie wygrała ;-). Życie bez adrenaliny to jak śmierć za życia."

# Historia:

## Progresja

|Misja|Progresja|Kampania|
|[Ratując syrenopnącze](/rpg/inwazja/opowiesci/konspekty/170417-ratujac-syrenopnacze.html)|powiązany z organizacją gigantycznej imprezy erotycznej wraz z Silurią Diakon|Powrót Karradraela|



## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|170417|mistrz ceremonii i współorganizator gigantycznej imprezy erotycznej o której będzie się mówić...|[Ratując syrenopnącze](/rpg/inwazja/opowiesci/konspekty/170417-ratujac-syrenopnacze.html)|10/03/05|10/03/12|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170115|prezydent Dare Shiver; wyjaśnił Silurii jak ten klub działa oraz musiał opanować sytuację po tym, jak Siluria zrobiła striptiz w ludzkiej części klubu ;-).|[Klub Dare Shiver](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|10/02/22|10/02/25|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|Siluria Diakon|2|[170417](/rpg/inwazja/opowiesci/konspekty/170417-ratujac-syrenopnacze.html), [170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|Rafael Diakon|1|[170417](/rpg/inwazja/opowiesci/konspekty/170417-ratujac-syrenopnacze.html)|
|Netheria Diakon|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|Kornelia Kartel|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|Karolina Kupiec|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|Judyta Maus|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|Jolanta Sowińska|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|Joachim Kopiec|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|Joachim Kartel|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|Jan Wątły|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|Infernia Diakon|1|[170417](/rpg/inwazja/opowiesci/konspekty/170417-ratujac-syrenopnacze.html)|
|Ignat Zajcew|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|Elizawieta Zajcew|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|Bogumił Rojowiec|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|Balrog Bankierz|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|Andżelika Leszczyńska|1|[170417](/rpg/inwazja/opowiesci/konspekty/170417-ratujac-syrenopnacze.html)|
|Akumulator Diakon|1|[170417](/rpg/inwazja/opowiesci/konspekty/170417-ratujac-syrenopnacze.html)|

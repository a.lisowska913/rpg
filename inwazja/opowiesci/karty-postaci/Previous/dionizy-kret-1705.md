---
layout: inwazja-karta-postaci
title:  "Dionizy Kret"
categories: profile
guild: "Niezrzeszony"
type: PC
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **"Nigdy więcej nie będę bezsilny! Nie bedę patrzył jak moim przyjaciołom dzieje się krzywda"**
* **"Agumentacja daje moc bezsilnym. Muszę ewoluować, by móc się zemścić"** 
* **"Moje życie i służba rodowi Blakenbauerów. To dzięki nim jeszcze żyję"**
* **"Mag krzywdzący ludzi jest zwykłym potworem, a ja tępię potwory"**
   
### Zachowania

* **"Granica możliwości jest tylko iluzją niewspomaganego umysłu"**
* **"Dorwę cię… czymkolwiek jesteś!"**
* **Special Tactics > mindless shooting**

## Co umie (co postać potrafi):

### Specjalizacje (poziom 3):

* **Pogromca** - przechwytywanie/likwidowanie magicznych stworzeń (w tym magów)
* **Hackowanie i łamanie zabezpieczeń**
* **Podchody**
* **Ewakuacja**

### Umiejętności (poziom 2):

* **Black-SWAT** - siły taktyczno-zwiadowcze
* **Techniki śledcze**
* **Regeneracja**
* **Negocjator**
* **Kierowca**

### Cechy:

|  Social Aggressive |  Social Devious |  Social Friendly |  Knowledge |  Craft |  Supernatural |  Fortitude |  Nimbleness |
|:==================:|:===============:|:================:|:==========:|:======:|:=============:|:==========:|:===========:|
|        0           |         0       |        -1        |      0     |   -1   |       0       |     +1     |     +1      |


### Specjalne

* Augumentacja

## Magia

BRAK

### Surowce

* opis: 1/2/3

## Zasoby i otoczenie

### Powiązane frakcje

* link_do_frakcji

### Kogo zna

* **Członkowie grupy specjalnej Hektora**
* **Edwin, Marcelin, Margareth**
* **Służba rodziny Blakenbauerów**
* **Ludzie z półświatka**

### Co ma do dyspozycji:

* **Komory regeneracyjne w rezydencji**
* **Specjalistyczny sprzęt SWAT**
* **Drony hackująco-zwiadowcze**
* **Pojazd opancerzony**
* **Mikstury z Black Labu**

# Opis
krótki opis postaci, rzeczy, jakie warto pamiętać.
## Motto

"Tekst"

# Historia
---
layout: inwazja-karta-postaci
categories: profile
factions: "Millennium"
type: "NPC"
title: "Amanda Diakon"
---
# {{ page.title }}

## Postać

### Motywacje (do czego dąży)

* **blitzkrieg: uderz i się wycofaj zanim przeciwnik się zorientuje**: cytat_lub_opis_jak_to_rozumieć

### Zachowania (jaka jest)

* **najlepiej, jeżeli inni będą rozwiązywali moje problemy**: cytat_lub_opis_jak_to_rozumieć
* **wypowiedziane słowa muszą być dotrzymane**: cytat_lub_opis_jak_to_rozumieć
* **nie zajmuję się tematami nieistotnymi; od tego są inni**: cytat_lub_opis_jak_to_rozumieć

### Specjalizacje

* **walka w zwarciu (bez broni)**: opis
* **inkarnacja koszmarów sennych**
* **czynność_na_poziomie_specjalizacji**: opis
* **czynność_na_poziomie_specjalizacji**: opis
* **czynność_na_poziomie_specjalizacji**: opis

### Umiejętności

* **trucizny**
* **wojowniczka**
* **manipulatorka i uwodzicielka**
* **przywódczyni Triumwiratu**

* **grupa_czynności_na_poziomie_zawodowym**: opis
* **grupa_czynności_na_poziomie_zawodowym**: opis
* **grupa_czynności_na_poziomie_zawodowym**: opis
* **grupa_czynności_na_poziomie_zawodowym**: opis
* **grupa_czynności_na_poziomie_zawodowym**: opis
* **grupa_czynności_na_poziomie_zawodowym**: opis
* **grupa_czynności_na_poziomie_zawodowym**: opis
* **grupa_czynności_na_poziomie_zawodowym**: opis

### Cechy

|  Social Aggressive |  Social Devious |  Social Friendly |  Knowledge |  Craft |  Supernatural |  Fortitude |  Nimbleness |
|:==================:|:===============:|:================:|:==========:|:======:|:=============:|:==========:|:===========:|
|       +1           |        +1       |        -1        |     -1     |   -1   |      -1       |     +1     |     +1      |

### Specjalne
* vicinius złożony z pająków; +2 do czynności gdzie to się może przydać
* vicinius złożony z pająków; -3 w okolicznościach słabych stron pająków
* jeśli pająk wlezie, ona też wlezie
* wydziela niebezpieczne trucizny

## Magia

### Szkoły magiczne

* **biomancja**: opis
* **grupa_działań_magicznych**: opis

### Zaklęcia statyczne

* **nazwa_zaklęcia**: opis_zaklęcia_lub_link
* **nazwa_zaklęcia**: opis_zaklęcia_lub_link

## Zasoby i otoczenie

### Powiązane frakcje

* nazwa_frakcji_jak_ma_to_z_linkiem

### Kogo zna

* **grupy_i_frakcje_które_zna_i_które_ją_znają**: opis
* **grupy_i_frakcje_które_zna_i_które_ją_znają**: opis

### Co ma do dyspozycji:

* **przywódczyni Toksycznych Templariuszy**
* **jedna z Triumwiratu Millennium**

### Surowce

* **Wartość**: 1/2/3
* **Pochodzenie**: dlaczego_1_2_lub_3_i_jak_zarabia

# Opis

Amanda. One of the Triumvirate, the Spider Queen, she is a very, very augmented mage. After the Eclipse her power has grown considerably and as she is a kralothborn, her body is her perfect tool. Things which normal humans would not be able to actually do, both on physical and mental levels, are completely at her disposal.

### Koncept

Wysokopoziomowy koncept postaci, 1-2 zdania.

### Motto

"Tekst"

# Historia:



## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|170226|która jest rozdarta między próbą porywania i badania (lub się znęcania) Mausów a próbą zyskania w tych trudnych czasach Mausich sojuszników. Wybrała drugie pod wpływem Andrei.|[Wygraliśmy wojnę... prawda?](/rpg/inwazja/opowiesci/konspekty/170226-wygralismy-wojne-prawda.html)|10/08/18|10/08/20|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170122|która w imieniu rodu Diakon wsparła z całej siły Anetę Rainer. Z wielkimi kosztami, trzyma Kopalin i pomaga odzyskać Kompleks.|[Gambit Anety Rainer](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html)|10/07/19|10/07/23|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160420|która wmanewrowała Blakenbauerów w zaatakowanie przez nich budynku z przekaźnikami energii; odstąpiła mogąc zastawić inną pułapkę.|[Kolizja dwóch sojuszy](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html)|10/07/01|10/07/02|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160316|jedna z Triumwiratu (kralothborn), która dowodzi operacją mającą wprowadzić potęgę Millennium do Kopalina korzystając z Irytki.|[Frontalne wejście Millennium](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html)|10/06/27|10/06/28|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160506|dowodzi stabilizacją Kopalina z ramienia Millennium i współpracuje z Hektorem i Emilią. Odzyskała cruentus reverto.|[Wyścig pająka z terminuską](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|10/06/22|10/06/24|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160424|która ustaliła zasady pokoju z Ozydiuszem przez Andreę i gdy poszła na ustalenia, Ozydiusz zginął. Jej siły opanowały sytuację i skomunikowała się z Andreą.|[Uważaj, o czym marzysz](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|10/06/18|10/06/21|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|151014|jedna z Triumwiratu Rodu Diakonów; nie wiedziała o stanie Mileny Diakon i zajęła się tym, dlaczego nie wiedziała.|[Jedno słowo prawdy za dużo](/rpg/inwazja/opowiesci/konspekty/151014-jedno-slowo-prawdy-za-duzo.html)|10/05/31|10/06/01|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|140708|głównodowodząca Millennium do spraw walki z Inwazją z uwagi na najwyższy poziom nienaturalności.|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|10/01/21|10/01/22|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|Hektor Blakenbauer|6|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [160316](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html), [151014](/rpg/inwazja/opowiesci/konspekty/151014-jedno-slowo-prawdy-za-duzo.html), [140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|Wiktor Sowiński|4|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html), [151014](/rpg/inwazja/opowiesci/konspekty/151014-jedno-slowo-prawdy-za-duzo.html)|
|Marcelin Blakenbauer|4|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [160316](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html), [140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|Aneta Rainer|4|[170226](/rpg/inwazja/opowiesci/konspekty/170226-wygralismy-wojne-prawda.html), [170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html), [160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|Andrea Wilgacz|4|[170226](/rpg/inwazja/opowiesci/konspekty/170226-wygralismy-wojne-prawda.html), [170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html), [140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|Tatiana Zajcew|3|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html), [151014](/rpg/inwazja/opowiesci/konspekty/151014-jedno-slowo-prawdy-za-duzo.html), [140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|Ozydiusz Bankierz|3|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [160316](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|Mieszko Bankierz|3|[170226](/rpg/inwazja/opowiesci/konspekty/170226-wygralismy-wojne-prawda.html), [170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html), [140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|Klara Blakenbauer|3|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [160316](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|Emilia Kołatka|3|[160316](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html), [151014](/rpg/inwazja/opowiesci/konspekty/151014-jedno-slowo-prawdy-za-duzo.html)|
|Elea Maus|3|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|Dagmara Wyjątek|3|[160316](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|Vladlena Zajcew|2|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html), [151014](/rpg/inwazja/opowiesci/konspekty/151014-jedno-slowo-prawdy-za-duzo.html)|
|Tymotheus Blakenbauer|2|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [151014](/rpg/inwazja/opowiesci/konspekty/151014-jedno-slowo-prawdy-za-duzo.html)|
|Stalowy Śledzik Żarłacz|2|[170226](/rpg/inwazja/opowiesci/konspekty/170226-wygralismy-wojne-prawda.html), [170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html)|
|Siluria Diakon|2|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html), [151014](/rpg/inwazja/opowiesci/konspekty/151014-jedno-slowo-prawdy-za-duzo.html)|
|Rudolf Jankowski|2|[170226](/rpg/inwazja/opowiesci/konspekty/170226-wygralismy-wojne-prawda.html), [170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html)|
|Otton Blakenbauer|2|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html), [151014](/rpg/inwazja/opowiesci/konspekty/151014-jedno-slowo-prawdy-za-duzo.html)|
|Marian Agrest|2|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|Margaret Blakenbauer|2|[160316](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|Leonidas Blakenbauer|2|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [160316](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html)|
|Kirył Sjeld|2|[170226](/rpg/inwazja/opowiesci/konspekty/170226-wygralismy-wojne-prawda.html), [170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html)|
|Kajetan Weiner|2|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|Edwin Blakenbauer|2|[151014](/rpg/inwazja/opowiesci/konspekty/151014-jedno-slowo-prawdy-za-duzo.html), [140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|Draconis Diakon|2|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html), [140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|Świeży Lilak|1|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html)|
|Wioletta Bankierz|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|Whisperwind|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|Wanda Ketran|1|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|Wacław Zajcew|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|Teresa Żyraf|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|Tadeusz Baran|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|Szczepan Sowiński|1|[170226](/rpg/inwazja/opowiesci/konspekty/170226-wygralismy-wojne-prawda.html)|
|Swietłana Zajcew|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|Sebastian Tecznia|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|Sabina Sowińska|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|Ryszard Weiner|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|Rufus Maus|1|[170226](/rpg/inwazja/opowiesci/konspekty/170226-wygralismy-wojne-prawda.html)|
|Rodion Zajcew|1|[170226](/rpg/inwazja/opowiesci/konspekty/170226-wygralismy-wojne-prawda.html)|
|Quasar|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|Paulina Tarczyńska|1|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html)|
|Patryk Maus|1|[170226](/rpg/inwazja/opowiesci/konspekty/170226-wygralismy-wojne-prawda.html)|
|Pasożyt Diakon|1|[160316](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html)|
|Olga Miodownik|1|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html)|
|Mirabelka Diakon|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|Milena Diakon|1|[151014](/rpg/inwazja/opowiesci/konspekty/151014-jedno-slowo-prawdy-za-duzo.html)|
|Malwina Krówka|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|Laurena Bankierz|1|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html)|
|Laragnarhag|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|Krystalia Diakon|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|Kleofas Bór|1|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|Katalina Bankierz|1|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html)|
|Karradrael|1|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html)|
|Julian Pszczelak|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|Judyta Karnisz|1|[160316](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html)|
|Jan Weiner|1|[170226](/rpg/inwazja/opowiesci/konspekty/170226-wygralismy-wojne-prawda.html)|
|Irina Zajcew|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|Ika|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|Grzegorz Czerwiec|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|Franciszek Myszeczka|1|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html)|
|Fortitia Diakon|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|Estera Piryt|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|Elizawieta Zajcew|1|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|Edward Diakon|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|Dracena Diakon|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|Diana Weiner|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|Bianka Stein|1|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html)|
|Benjamin Zajcew|1|[151014](/rpg/inwazja/opowiesci/konspekty/151014-jedno-slowo-prawdy-za-duzo.html)|
|Baltazar Maus|1|[170226](/rpg/inwazja/opowiesci/konspekty/170226-wygralismy-wojne-prawda.html)|
|Aurel Czarko|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|Arkadiusz Klusiński|1|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|Arazille|1|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html)|
|Anastazja Sjeld|1|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html)|
|Aleksander Sowiński|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|Adrian Murarz|1|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|Abelard Maus|1|[170226](/rpg/inwazja/opowiesci/konspekty/170226-wygralismy-wojne-prawda.html)|

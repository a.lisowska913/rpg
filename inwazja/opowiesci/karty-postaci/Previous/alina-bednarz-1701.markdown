---
layout: inwazja-karta-postaci
title:  "Alina Bednarz"
categories: profile, blackenbauerowie, postac_gracza
---
# {{ page.title }}

## Impulsy (co motywuje / napędza postać):

### Strategiczne

- **ptak w klatce**: "chcę zapewnić sobie swobodę działania w świecie magów"
- **dług honorowy**: "Edwin Blakenbauer uratował mi życie. Jestem mu winna tyle samo"

### Taktyczne

- **pozostać w ukryciu**: "jak o mnie nie wiedzą to nic mi nie zrobią"
- **podstępna**: "najlepiej wykończyć wroga z zaskoczenia"
- **skryta**: "nikt poza przyjaciółmi nie może poznać moich prawdziwych uczuć"

## Co umie (co postać potrafi):


### Ekspercko (poziom 3):

- **stanę się każdym** (aktorka)
- **nic mi nie umknie** (black ops / zdobywanie informacji)


### Zawodowo (poziom 2):

- **aktorka**: Alina potrafi zagrać każdego
- **technik śledczy**: pracując w siłach specjalnych, Alina nauczyła się ludzkich metod zbierania śladów. Pracując z Edwinem, nauczyła się, jak patrzeć, żeby widzieć potencjalną magię
- **pułapki**: 
- **zdobywanie informacji**: Alina albo zdobywa je sama, albo przez kontakty
- **negocjacje**: Alina zwykle dąży do osiągnięcia celu przez dostosowanie się do rozmówcy - nie ma znaczenia, czy po dobroci, czy zastraszając, czy oszukując
- **black ops**: skradanie, włamania, przemykanie, zasadzki... tego typu rzeczy
- **zabezpieczenia**: kiedy trzeba się gdzieś włamać, zabezpieczenia nie mogą Aliny powstrzymać przed wejściem

### Amatorsko (poziom 1):

- **sabotaż**: 
- **artefakty**: Alina wie sporo o artefaktach różnego rodzaju, potrafi je rozpozonawać, potrafi ich używać i się przed nimi zabezpieczać

## Inklinacje:

|  Social Aggressive |  Social Devious |  Social Friendly |  Knowledge |  Craft |  Supernatural |  Fortitude |  Nimbleness |
|:==================:|:===============:|:================:|:==========:|:======:|:=============:|:==========:|:===========:|
|        0           |         +1      |         0        |      -1    |   -1   |       +1      |      0     |     +1      |



## Kieszonkowe

## Otoczenie powiązane

### Powiązane frakcje

- Blackenbauerowie

### Kogo zna

#### Doskonale: 3

- półświatek
- siły specjalne policji
- augumented humans

#### Nieźle: 2



#### Luźno: 1
  

### Co ma do dyspozycji:

#### Szczególnie dobre: 3


#### Ponadprzeciętne: 2


#### Niezłe: 1



## Magia:

[brak]

## Specjalne:

Zmiennokształtna / doppelganger
Augumented human

## Inne:


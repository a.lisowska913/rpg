---
layout: inwazja-karta-postaci
categories: profile
factions: "Srebrna Świeca"
type: "NPC"
title: "Nikodem Sowiński"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **kolekcjoner**: "Wszystkie kurioza i trofea docelowo powinny trafić do moich muzeów, by wszyscy mogli podziwiać je… i mnie"
* **poszukiwacz przygód**: "Prawdziwe trofea mają piękną historię; nie tylko tą starą. Też tą aktualną - jakoś je zdobyłem."
* **builder**: "Zrobię coś, dzięki czemu pozostanie po mnie ślad. Będzie powstawać społeczeństwo. Inni mogą z tego korzystać."


### Zachowania (jaka jest)

* **hojny**: "Warto dać szansę uboższym i warto sponsorować ich marzenia, by pozostawić ślad po sobie."
* **arogancki**: "Maluczkim warto dać szansę i możliwości, ale muszą pamiętać gdzie jest ich miejsce."
* **rywalizacja**: "Śląskie muzea trofeów są najlepsze. Dzięki mnie. Nawet jeśli nie mam najlepszych - mam najlepiej ułożone."

### Specjalizacje

* **czynność_na_poziomie_specjalizacji**: opis
* **czynność_na_poziomie_specjalizacji**: opis
* **czynność_na_poziomie_specjalizacji**: opis
* **czynność_na_poziomie_specjalizacji**: opis

### Umiejętności

* **zarządzanie muzeum**: mag zarządzający zestawem magicznych muzeów, składający artefakty w logiczną (nie wybuchającą) całość
* **opowieści**: zwłaszcza przez historie poszczególnych układów eksponatów, ale i potrafi opowiadać o dziełach i dokonaniach
* **wycena artefaktów**: zwłaszcza historycznych
* **systemy zabezpieczeń**: nie tylko ma customizowane; jako "Indiana Jones" często włamywał się i przełamywał różne ;-)
* **historia magii i magów**: znajomość miejsc, wsparcie w artefaktach, fact-checking…
* **atleta**: jak był młodszy, był z niego niezły "Indiana Jones"
* **archeologia**: zna się na odkrywaniu, badaniu i przewidywaniu. Zwłaszcza niebezpiecznych, magicznych artefaktów
* **podróżnik**: bywał w wielu miejscach, często niebezpiecznych i wie jak sformować ekspedycję

### Szkoły magiczne

* **grupa_działań_magicznych**: opis
* **grupa_działań_magicznych**: opis
* **grupa_działań_magicznych**: opis
* **grupa_działań_magicznych**: opis

### Cechy:

|  Social Aggressive |  Social Devious |  Social Friendly |  Knowledge |  Craft |  Supernatural |  Fortitude |  Nimbleness |
|:==================:|:===============:|:================:|:==========:|:======:|:=============:|:==========:|:===========:|
|       +1           |         0       |        -1        |     +1     |   -1   |      -1       |      0     |     +1      |

### Specjalne
**brak**

## Zasoby i otoczenie

### Powiązane frakcje

* Srebrna Świeca
* Ród Sowińskich

### Kogo zna

* **grupki scavengerów**: grupy magów, ludzi i viciniusów którzy żyją z ekspedycji i znajdowania kuriozów ;-).
* **reputacja sponsora i posiadacza**: powszechnie znany jako posiadacz wielu artefaktów, muzeów i osoba skłonna by coś odkupić lub wesprzeć.
* **magowie z wielu gildii**: często się spotyka i udziela; pokazuje i demonstruje oraz chwali się swymi przygodami i trofeami
* **arystokracja Srebrnej Świecy**: jest to mag, "którego się zna" i "z którym się bywa".
* **kluby podróżników i kolekcjonerów**: często przebywa w miejscach, w których może rozmawiać i wymieniać przygody z innymi podróżnikami i kolekcjonerami

### Co ma do dyspozycji:

* **muzea z artefaktami**: dostęp do małej sieci prywatnych muzeów z artefaktami na Śląsku; tematycznych.
* **mapy i legend-bustery**: kolekcjonuje mapy i okazje by sprawdzić fakty i mity. Też książki świata magów.
* **trofea swoje i cudze**: grupa różnego rodzaju obiektów mających historię, którymi można i warto się pochwalić
* **czarne artefakty**: nie wszystkie artefakty w muzeach są bezpieczne… a niektóre przechowuje poza muzeami
* **kolekcja sprzętu awanturniczego**: różnego rodzaju oprzyrządowanie służące do awanturowania się i IndianaJonesowania
* **kolekcja sprzętu archelogicznego**: sprzęt służący do robienia badań i ekstrakcji w terenie

### Surowce

* **Wartość**: 3
* **Pochodzenie**: bardzo bogaty, rentier, żyje z przeszłości i wsparcia rodu

# Opis

krótki opis postaci, rzeczy, jakie warto pamiętać.

## Motto

"Najlepszy kolekcjoner? Osoba o największych zbiorach? O interesującym guście i do tego filantrop? Ależ to ja, nie musicie dziękować."

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|160202|któremu pająk fazowy ukradł cenny kryształ pamięci. Obwiniał Laurenę, wyszedł na idiotę przed Ignatem.|[Wolność pająka fazowego](/rpg/inwazja/opowiesci/konspekty/160202-wolnosc-pajaka-fazowego.html)|10/05/09|10/05/10|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|[Leonidas Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-leonidas-blakenbauer.html)|1|[160202](/rpg/inwazja/opowiesci/konspekty/160202-wolnosc-pajaka-fazowego.html)|
|[Laurena Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-laurena-bankierz.html)|1|[160202](/rpg/inwazja/opowiesci/konspekty/160202-wolnosc-pajaka-fazowego.html)|
|[Klara Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1707-klara-blakenbauer.html)|1|[160202](/rpg/inwazja/opowiesci/konspekty/160202-wolnosc-pajaka-fazowego.html)|
|[Ignat Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-ignat-zajcew.html)|1|[160202](/rpg/inwazja/opowiesci/konspekty/160202-wolnosc-pajaka-fazowego.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|1|[160202](/rpg/inwazja/opowiesci/konspekty/160202-wolnosc-pajaka-fazowego.html)|
|[Borys Kumin](/rpg/inwazja/opowiesci/karty-postaci/1709-borys-kumin.html)|1|[160202](/rpg/inwazja/opowiesci/konspekty/160202-wolnosc-pajaka-fazowego.html)|
|[Arazille](/rpg/inwazja/opowiesci/karty-postaci/9999-arazille.html)|1|[160202](/rpg/inwazja/opowiesci/konspekty/160202-wolnosc-pajaka-fazowego.html)|

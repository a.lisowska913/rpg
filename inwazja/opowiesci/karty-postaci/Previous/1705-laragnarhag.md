---
layout: inwazja-karta-postaci
categories: profile
factions: "Millennium"
type: "NPC"
title: "Laragnarhag"
---
# {{ page.title }}

## Postać

### Motywacje (do czego dąży)

* **samodoskonalenie**: "Wszystko ulegnie ewolucji. Ja ulegnę ewolucji. Ja zaadaptuję. Jestem najsilniejszym organizmem."
* **ekspansjonizm**: "Wszystko zostanie pożarte. Wszystko stanie się jednością. Stanie się mną."
* **strażnik tajemnic**: "Wszystko usłyszę. Niczego nie powiem."

### Zachowania (jaka jest)

* **obserwator**: "Wszystko zostanie zasymilowane i przetworzone. Wszystko zostanie zinterpretowane."
* **żołnierz**: "Wszystkie polecenia zostaną zasymilowane. Wszystkie zostaną wykonane. Korzyść będzie wyewoluowana."

### Specjalizacje

* **magia krwi**
* **rytuały krwi**
* **wiedza o kralothach**
* **biodominacja ludzi**
* **sprawianie rozkoszy** (po SCA, nie SCF!)
* **eliksiry**

### Umiejętności

* **biosynteza eliksirów** (bio/mental)
* **zniewolenie**
* **lifeshaper: adaptacja**
* **lifeshaper: ewolucja**
* **lifeshaper: kraloth**
* **zastraszanie**
* **godtech z czasów Wojen Bogów**
* **kultura Millennium**

### Cechy

|  Social Aggressive |  Social Devious |  Social Friendly |  Knowledge |  Craft |  Supernatural |  Fortitude |  Nimbleness |
|:==================:|:===============:|:================:|:==========:|:======:|:=============:|:==========:|:===========:|
|       +2           |        -1       |        -2        |     +1     |   +1   |      +1       |     +2     |      -3     |

### Specjalne
- BARDZO wysoka samokontrola jak na kralotha
- BARDZO wysoki poziom komunikacji z ludźmi jak na kralotha
- silny malus do walki z kralothami (-3)

## Magia

### Szkoły magiczne

* **biomancja**: opis
* **brak magii mentalnej**: opis

### Zaklęcia statyczne

* **nazwa_zaklęcia**: opis_zaklęcia_lub_link
* **nazwa_zaklęcia**: opis_zaklęcia_lub_link

## Zasoby i otoczenie

### Powiązane frakcje

* nazwa_frakcji_jak_ma_to_z_linkiem

### Kogo zna

* **potomstwo w Millennium**
* **przodek - Abominus**
* **Diakoni w Millennium**

### Co ma do dyspozycji:

* **wyhodowani ludzie do eksperymentów i żywienia**
* **zapasy biomasy**

### Surowce

* **Wartość**: 1/2/3
* **Pochodzenie**: dlaczego_1_2_lub_3_i_jak_zarabia

# Opis

Laragnarhag is a special breed of a kraloth - it is a kraloth of Millennium breed. He is one of the better adjusted and most pleasant of kralothkind which are present in Millennium. A descendant of Abominus, and an ascendant of several other Millennium kraloths, this one is firmly entrenched in the Millennium.

Because of genetic memory of his parent he has inherited knowledge and interest in godtech and darkest blood magic from the God Wars era. He has evolved himself many times to control his basic urges and to optimize working with Magi over raw power. As for kraloth in his age with the resources around, this one is exquisitely weak. A different kraloth would be able to defeat Laragnarhag quite easily having the same resources. However, Laragnarhag controls himself much better and is able to cooperate much better than a 'non-domesticated' counterpart.

A formidable biosynthesizer and defiler with biological mind control abilities and impulse engineering, this one does not have mental powers usually associated with kralothkind. This particular genetic strain atrophied to let Laragnarhag control his own impulses and urges.

Although Laragnarhag is able to communicate with humans and vicinius thinking like humans, he is fundamentally a kraloth at heart. He doesn't understand humans. Studying them is one of his deep passions.

Because of his nature, things like "ethics" or "right things to do" are completely alien to his nature. Usually, Laragnarhag defers to his human companions and allows them to oversee what this kraloth is going to do for them.

He will comply. He will be watching. He will be evolving.
### Koncept

Wysokopoziomowy koncept postaci, 1-2 zdania.

### Motto

"Tekst"

# Historia:

|Data|Dokonanie|Misja|Kampania|
|170101|kralotyczne maskowanie, pozorny pożeracz Mieszka, tymczasowy 'władca 7 * magów', agregator informacji i źródło uzależnienia Anny. W skrócie: MVP.|[Patrol? Kralotyczne maskowanie!](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161231|kraloth specjalizujący się w Starej * magii i Starych Technologiach (godtech). Centralny element planów Andrei i Rafaela, ku zmartwieniu tej pierwszej.|[Eskalacja Czelimina, eskalacja Andrei](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html)|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161229|który wyczuł Pryzmatyczny Atak Krwi w Mieszka i ostrzegł Andreę, że to samo działo się w Kompleksie Centralnym.|[Presja ze strony Czelimina](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html)|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161204|jak na kralotha, wyjątkowo grzeczny. Może bardzo wiele - ale nikt mu nie pozwala. Służy jako system komunikacyjny z Millennium. Źródło konfliktów w Zespole.|[Zajcewowie po drugiej stronie](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html)|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160424|kraloth, którego Siluria wycofała w ostatniej chwili i który jest świadkiem i pomocą Silurii w sprawach powiązanych ze Szlachtą.|[Uważaj, o czym marzysz](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160412|kraloth w służbie Millennium, tymczasowy partner Silurii i świadek działań Wiktora. Nikt go nie podejrzewa, bo niemoralny kraloth.|[Spleśniała dusza terminuski](/rpg/inwazja/opowiesci/konspekty/160412-splesniala-dusza-terminuski.html)|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|Wioletta Bankierz|5|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html), [161229](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html), [160412](/rpg/inwazja/opowiesci/konspekty/160412-splesniala-dusza-terminuski.html)|
|Andrea Wilgacz|5|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html), [161229](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|Rafael Diakon|4|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html), [161229](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html)|
|Mieszko Bankierz|4|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html), [161229](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html)|
|Melodia Diakon|4|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html), [161229](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html)|
|Julian Pszczelak|4|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161229](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|Tatiana Zajcew|3|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html), [161229](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html)|
|Stalowy Śledzik Żarłacz|3|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html)|
|Rudolf Jankowski|3|[161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html), [161229](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html)|
|Marian Łajdak|3|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html)|
|Marian Agrest|3|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161229](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|Lidia Weiner|3|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161229](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html)|
|Kajetan Weiner|3|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|Dagmara Wyjątek|3|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html), [160412](/rpg/inwazja/opowiesci/konspekty/160412-splesniala-dusza-terminuski.html)|
|Świeży Lilak|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html)|
|Wiktor Sowiński|2|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html), [160412](/rpg/inwazja/opowiesci/konspekty/160412-splesniala-dusza-terminuski.html)|
|Tadeusz Baran|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|Siluria Diakon|2|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html), [160412](/rpg/inwazja/opowiesci/konspekty/160412-splesniala-dusza-terminuski.html)|
|Sabina Sowińska|2|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html), [160412](/rpg/inwazja/opowiesci/konspekty/160412-splesniala-dusza-terminuski.html)|
|Rodion Zajcew|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html)|
|Laurena Bankierz|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html)|
|Kirył Sjeld|2|[161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html)|
|Elea Maus|2|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html), [160412](/rpg/inwazja/opowiesci/konspekty/160412-splesniala-dusza-terminuski.html)|
|Draconis Diakon|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161229](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html)|
|Diana Weiner|2|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html), [160412](/rpg/inwazja/opowiesci/konspekty/160412-splesniala-dusza-terminuski.html)|
|Dalia Weiner|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html)|
|Andżelika Leszczyńska|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html)|
|Ryszard Weiner|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|Ozydiusz Bankierz|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|Mirabelka Diakon|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|Katalina Bankierz|1|[161229](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html)|
|Karina Łoszad|1|[161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html)|
|Jolanta Sowińska|1|[161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html)|
|Joachim Kopiec|1|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|
|Jacek Molenda|1|[161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html)|
|Hektor Blakenbauer|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|Fryderyk Chwost|1|[160412](/rpg/inwazja/opowiesci/konspekty/160412-splesniala-dusza-terminuski.html)|
|Franciszek Myszeczka|1|[161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html)|
|Fortitia Diakon|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|Edward Diakon|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|Deiiw Podniebny Grom|1|[161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html)|
|Aurel Czarko|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|Anna Kozak|1|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|
|Aneta Rainer|1|[161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html)|
|Amanda Diakon|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|Alojzy Przylaz|1|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|
|Aleksander Sowiński|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|

---
layout: inwazja-karta-postaci
categories: profile
factions: "Nieznany"
type: "PC"
title: "Mateusz Ackmann"
---

# {{ page.title }}

## Postać

### Motywacje (do czego dąży)

* **impuls_co_chce_osiągnąć**: cytat_lub_opis_jak_to_rozumieć

### Zachowania (jaka jest)

* **pragmatyk**: chłodna kalkulacja ponad emocje
* **skryty**: niechętny do udzielania informacji
* **kolekcjoner** (informacje, wiedza)
* **(status) zakochany**: Supernowa Diakon

### Specjalizacje

* **ekonometryk** (matematyk)
* **kreatywna księgowść** (księgowy)
* **obfuskacja** (biurokracja)
* **magia matematyczna**

### Umiejętności

* **matematyk**
* **księgowy**
* **cosplayer**
* **rpgowiec**
* **akademik**
* **techniki manipulacyjne**
* **biurokracja**

### Cechy

|  Social Aggressive |  Social Devious |  Social Friendly |  Knowledge |  Craft |  Supernatural |  Fortitude |  Nimbleness |
|:==================:|:===============:|:================:|:==========:|:======:|:=============:|:==========:|:===========:|
|       -1           |        +1       |         0        |     +1     |   +1   |       0       |     -1     |     -1      |

### Specjalne
* **brak**
* **nazwa_specjalnej_własności**: opis_własności_lub_link

## Magia

* **brak**

## Zasoby i otoczenie

### Powiązane frakcje

* nazwa_frakcji_jak_ma_to_z_linkiem

### Kogo zna

* **kręgi biznesowe - ludzkie**
* **Świeca**
* **rpgowcy i miłośnicy fantasy**

### Co ma do dyspozycji:

* **sprzęt_do_jakiego_ma_dostęp_i_postać_charakteryzuje**: opis
* **sprzęt_do_jakiego_ma_dostęp_i_postać_charakteryzuje**: opis

### Surowce

* **Wartość**: 2
* **Pochodzenie**: dlaczego_1_2_lub_3_i_jak_zarabia

# Opis

Cichy, introwertyk, lekki asperger i uwielbienie do liczb. Studiował "ludzką" matematyke ze specjalizacją w ekonometrii. Hobby: cosplay - zna się na szyciu, konstrukcji ekwipunku, ubrania, nadawanie im wizualnych efektow magicznych; rpgi i planszówki. Jest glównym księgowym dużej korporacji. Mag Świecy. Niski, lekko szpakowate włosy, kawaler, znany w kręgach ludzkich - firmowo-korporacyjnych, jako doskonały księgowy. Znany również w Świecy, jako dziwaczny, ale uprzejmy i utalentowany mag-matematyk.
Zakochany w Supernowej Diakon.

### Koncept

Wysokopoziomowy koncept postaci, 1-2 zdania.

### Motto

"Tekst"

# Historia:

|Data|Dokonanie|Misja|Kampania|
|170725|przeleciał wiłę, zebrał ekipę, pojechał, doprowadził do największego Skażenia w historii Jodłowca i stracił o większości pamięć dla bezpieczeństwa.|[Krzywdzę, bo kocham](/rpg/inwazja/opowiesci/konspekty170725-krzywdze-bo-kocham.html)|prawdziwa_natura_draceny|
|170725|przeleciał wiłę, zebrał ekipę, pojechał, doprowadził do największego Skażenia w historii Jodłowca i stracił o większości pamięć dla bezpieczeństwa.|[Krzywdzę, bo kocham](/rpg/inwazja/opowiesci/konspekty170725-krzywdze-bo-kocham.html)|prawdziwa_natura_draceny|
|160922|księgowy który przeżył dzień swojego życia i 'got the girl', "pokonał" terminuskę i zorganizował fajną imprezę RPG.|[Czarnoskalski konwent RPG](/rpg/inwazja/opowiesci/konspekty160922-czarnoskalski-konwent-rpg.html)|taniec_lisci|
|160922|księgowy który przeżył dzień swojego życia i 'got the girl', "pokonał" terminuskę i zorganizował fajną imprezę RPG.|[Czarnoskalski konwent RPG](/rpg/inwazja/opowiesci/konspekty160922-czarnoskalski-konwent-rpg.html)|taniec_lisci|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|Supernowa Diakon|2|[160922](/rpg/inwazja/opowiesci/konspekty160922-czarnoskalski-konwent-rpg.html), [160922](/rpg/inwazja/opowiesci/konspekty160922-czarnoskalski-konwent-rpg.html)|
|Silgor Diakon|2|[170725](/rpg/inwazja/opowiesci/konspekty170725-krzywdze-bo-kocham.html), [170725](/rpg/inwazja/opowiesci/konspekty170725-krzywdze-bo-kocham.html)|
|Rafał Kniaź|2|[160922](/rpg/inwazja/opowiesci/konspekty160922-czarnoskalski-konwent-rpg.html), [160922](/rpg/inwazja/opowiesci/konspekty160922-czarnoskalski-konwent-rpg.html)|
|Mikado Diakon|2|[160922](/rpg/inwazja/opowiesci/konspekty160922-czarnoskalski-konwent-rpg.html), [160922](/rpg/inwazja/opowiesci/konspekty160922-czarnoskalski-konwent-rpg.html)|
|Małż Poszukiwacz|2|[170725](/rpg/inwazja/opowiesci/konspekty170725-krzywdze-bo-kocham.html), [170725](/rpg/inwazja/opowiesci/konspekty170725-krzywdze-bo-kocham.html)|
|Małż Mateusz|2|[170725](/rpg/inwazja/opowiesci/konspekty170725-krzywdze-bo-kocham.html), [170725](/rpg/inwazja/opowiesci/konspekty170725-krzywdze-bo-kocham.html)|
|Kornel Wadera|2|[160922](/rpg/inwazja/opowiesci/konspekty160922-czarnoskalski-konwent-rpg.html), [160922](/rpg/inwazja/opowiesci/konspekty160922-czarnoskalski-konwent-rpg.html)|
|Jessica Ruczaj|2|[160922](/rpg/inwazja/opowiesci/konspekty160922-czarnoskalski-konwent-rpg.html), [160922](/rpg/inwazja/opowiesci/konspekty160922-czarnoskalski-konwent-rpg.html)|
|Infensa Diakon|2|[160922](/rpg/inwazja/opowiesci/konspekty160922-czarnoskalski-konwent-rpg.html), [160922](/rpg/inwazja/opowiesci/konspekty160922-czarnoskalski-konwent-rpg.html)|
|Henryk Gwizdon|2|[170725](/rpg/inwazja/opowiesci/konspekty170725-krzywdze-bo-kocham.html), [170725](/rpg/inwazja/opowiesci/konspekty170725-krzywdze-bo-kocham.html)|
|Fryderyk Bakłażan|2|[170725](/rpg/inwazja/opowiesci/konspekty170725-krzywdze-bo-kocham.html), [170725](/rpg/inwazja/opowiesci/konspekty170725-krzywdze-bo-kocham.html)|
|Esme Myszeczka|2|[160922](/rpg/inwazja/opowiesci/konspekty160922-czarnoskalski-konwent-rpg.html), [160922](/rpg/inwazja/opowiesci/konspekty160922-czarnoskalski-konwent-rpg.html)|
|Dyta|2|[170725](/rpg/inwazja/opowiesci/konspekty170725-krzywdze-bo-kocham.html), [170725](/rpg/inwazja/opowiesci/konspekty170725-krzywdze-bo-kocham.html)|
|Dracena Diakon|2|[170725](/rpg/inwazja/opowiesci/konspekty170725-krzywdze-bo-kocham.html), [170725](/rpg/inwazja/opowiesci/konspekty170725-krzywdze-bo-kocham.html)|
|Dorota Gacek|2|[160922](/rpg/inwazja/opowiesci/konspekty160922-czarnoskalski-konwent-rpg.html), [160922](/rpg/inwazja/opowiesci/konspekty160922-czarnoskalski-konwent-rpg.html)|
|Cyprian Koziej|2|[160922](/rpg/inwazja/opowiesci/konspekty160922-czarnoskalski-konwent-rpg.html), [160922](/rpg/inwazja/opowiesci/konspekty160922-czarnoskalski-konwent-rpg.html)|
|Bolesław Derwisz|2|[160922](/rpg/inwazja/opowiesci/konspekty160922-czarnoskalski-konwent-rpg.html), [160922](/rpg/inwazja/opowiesci/konspekty160922-czarnoskalski-konwent-rpg.html)|
|Barnaba Łonowski|2|[170725](/rpg/inwazja/opowiesci/konspekty170725-krzywdze-bo-kocham.html), [170725](/rpg/inwazja/opowiesci/konspekty170725-krzywdze-bo-kocham.html)|
|Artur Pawiak|2|[160922](/rpg/inwazja/opowiesci/konspekty160922-czarnoskalski-konwent-rpg.html), [160922](/rpg/inwazja/opowiesci/konspekty160922-czarnoskalski-konwent-rpg.html)|

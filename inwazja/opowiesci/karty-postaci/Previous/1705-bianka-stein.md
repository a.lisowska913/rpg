---
layout: inwazja-karta-postaci
categories: profile
factions: "Srebrna Świeca"
type: "NPC"
title: "Bianka Stein"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **przedsiębiorcza**: "Nie mamy zbyt wiele… ale powinno się udać. Dobrze, załatwię to."
* **oszczędna**: "Wiesz, to co mamy działa. Nie będę przepłacała; jeszcze się zepsuje…"
* **karierowiczka**: "Świeca patrzy tylko na magów Rodów. Sama muszę zadbać o siebie."
     
### Zachowania (jaka jest)

* **pewna siebie**: "Zaraz wybuchnie! Odsuń się! Ja to zrobię…"
* **chełpliwa**: "Proszę bardzo. Tylko ja mogłam to zrobić. To różni mistrzów od innych."

## Co umie (co postać potrafi):

### Specjalizacje

* **portale strategiczne**
* **konstruowanie portali strategicznych**
* **sprzęganie portali**
* **badanie energii magicznej** (kataliza energii magicznej)
* **stabilizacja energii magicznej** (kataliza energii magicznej)
* **źródła energii** (węzły)
* **artefakcja**
* **węzły**
* **portale**

### Umiejętności
* **inżynier energii magicznej**
* **inżynier artefaktów**
* **polityk Srebrnej Świecy**

### Szkoły magiczne
* **magia transportu**
* **kataliza**

### Cechy:

|  Social Aggressive |  Social Devious |  Social Friendly |  Knowledge |  Craft |  Supernatural |  Fortitude |  Nimbleness |
|:==================:|:===============:|:================:|:==========:|:======:|:=============:|:==========:|:===========:|
|        -1          |         0       |        -1        |     +1     |   +1   |      +1       |      0     |     -1      |

### Specjalne

* **nazwa_specjalnej_własności**: opis_własności_lub_link

## Magia

### Magia dynamiczna:

* katalistka energii magicznej
* magia portali
* magia badawcza

### Zaklęcia statyczne

* **nazwa_zaklęcia**: opis_zaklęcia_lub_link
* **nazwa_zaklęcia**: opis_zaklęcia_lub_link

## Zasoby i otoczenie

### Powiązane frakcje

* Srebrna Świeca

### Kogo zna

* **powszechnie uznana jako ekspert od portali w branży**
* **znana czarodziejka Świecy (katalistka wysokiej klasy)**

### Co ma do dyspozycji:

* **laboratorium w Świecy Daemonica**
* **specjalistyczny sprzęt do portali strategicznych**
* **podręczne analizatory energii magicznej**
* **generatory energii magicznej**
* **sprzęt do konstrukcji artefaktów i artekonstrukcji**

### Surowce

* **Wartość**: 2
* **Pochodzenie**:
Ceniona ekspert Świecy w ramach portali i katalizy; wysoko opłacana specjalistka


# Opis
krótki opis postaci, rzeczy, jakie warto pamiętać.
## Motto

"Tekst"

# Historia:



## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|170122|wypożyczona Anecie Rainer, by ta mogła opanować Kompleks Centralny. Najlepsza katalistka w okolicy.|[Gambit Anety Rainer](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html)|10/07/19|10/07/23|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170103|zniszczone morale, ciężko psychicznie uszkodzona, ale działa dla Andrei. Otworzyła (praktycznie niemożliwy) portal do Czelimina i zaczyna podkochiwać się w Tadeuszu Baranie.|[Wojna Bogów w Czeliminie](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html)|10/07/16|10/07/18|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160713|katalistka i specjalistka od portali i energii * magicznych, ewakuowana przez Zajcewów a potem Blakenbauerów poza Kopalin.|[Jak ukraść ze Świecy Zajcewów](/rpg/inwazja/opowiesci/konspekty/160713-ukrasc-ze-swiecy-zajcewow.html)|10/07/04|10/07/06|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160821|katalistka próbująca działać zgodnie ze swymi przekonaniami i ideałami; złamana przez Silurię i Eleę - jak nie pomoże, Judyta najpewniej umrze.|[Wycofanie Mileny z piekła](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html)|10/06/26|10/06/30|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160724|która wypaczyła Klucz otwierający połączenia z EAM, odcinając Świecę od EAM. Wpadła w ręce Wiktora.|[Portal do EAM](/rpg/inwazja/opowiesci/konspekty/160724-portal-do-eam.html)|10/06/24|10/06/25|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160411|katalistka Świecy, bardzo kompetentnta i pracowita. Jedna z najlepszych w Kopalinie. Wyizolowała nić Spustoszenia i się nim zaraziła.|[Sekret śmierci na wykopaliskach](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|10/06/14|10/06/15|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|Andrea Wilgacz|3|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html), [170103](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html), [160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|Świeży Lilak|2|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html), [170103](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html)|
|Zuzanna Maus|2|[170103](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html), [160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|Wiktor Sowiński|2|[160821](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html), [160724](/rpg/inwazja/opowiesci/konspekty/160724-portal-do-eam.html)|
|Tatiana Zajcew|2|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html), [160713](/rpg/inwazja/opowiesci/konspekty/160713-ukrasc-ze-swiecy-zajcewow.html)|
|Stalowy Śledzik Żarłacz|2|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html), [170103](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html)|
|Siluria Diakon|2|[160821](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html), [160724](/rpg/inwazja/opowiesci/konspekty/160724-portal-do-eam.html)|
|Rudolf Jankowski|2|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html), [170103](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html)|
|Oktawian Maus|2|[160724](/rpg/inwazja/opowiesci/konspekty/160724-portal-do-eam.html), [160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|Mieszko Bankierz|2|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html), [170103](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html)|
|Marian Agrest|2|[170103](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html), [160713](/rpg/inwazja/opowiesci/konspekty/160713-ukrasc-ze-swiecy-zajcewow.html)|
|Kirył Sjeld|2|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html), [170103](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html)|
|Karradrael|2|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html), [170103](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html)|
|Judyta Karnisz|2|[160821](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html), [160724](/rpg/inwazja/opowiesci/konspekty/160724-portal-do-eam.html)|
|Ernest Maus|2|[170103](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html), [160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|Elea Maus|2|[160713](/rpg/inwazja/opowiesci/konspekty/160713-ukrasc-ze-swiecy-zajcewow.html), [160821](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html)|
|Dagmara Wyjątek|2|[160821](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html), [160724](/rpg/inwazja/opowiesci/konspekty/160724-portal-do-eam.html)|
|Arazille|2|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html), [170103](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html)|
|Aneta Rainer|2|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html), [160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|Anastazja Sjeld|2|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html), [170103](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html)|
|Wojmił Bankierz|1|[160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|Wojciech Szudek|1|[160724](/rpg/inwazja/opowiesci/konspekty/160724-portal-do-eam.html)|
|Wioletta Bankierz|1|[160713](/rpg/inwazja/opowiesci/konspekty/160713-ukrasc-ze-swiecy-zajcewow.html)|
|Vladlena Zajcew|1|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html)|
|Tomasz Kuracz|1|[160724](/rpg/inwazja/opowiesci/konspekty/160724-portal-do-eam.html)|
|Tadeusz Baran|1|[170103](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html)|
|Ozydiusz Bankierz|1|[160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|Mojra|1|[160713](/rpg/inwazja/opowiesci/konspekty/160713-ukrasc-ze-swiecy-zajcewow.html)|
|Milena Diakon|1|[160821](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html)|
|Melodia Diakon|1|[170103](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html)|
|Laurena Bankierz|1|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html)|
|Krzysztof Wieczorek|1|[160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|Klara Blakenbauer|1|[160713](/rpg/inwazja/opowiesci/konspekty/160713-ukrasc-ze-swiecy-zajcewow.html)|
|Kermit Diakon|1|[160821](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html)|
|Katalina Bankierz|1|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html)|
|Joachim Kopiec|1|[170103](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html)|
|Jan Weiner|1|[160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|Jakub Niecień|1|[160821](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html)|
|Hektor Blakenbauer|1|[160713](/rpg/inwazja/opowiesci/konspekty/160713-ukrasc-ze-swiecy-zajcewow.html)|
|Gerwazy Myszeczka|1|[160713](/rpg/inwazja/opowiesci/konspekty/160713-ukrasc-ze-swiecy-zajcewow.html)|
|Franciszek Myszeczka|1|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html)|
|Felicjan Weiner|1|[160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|Estrella Diakon|1|[160821](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html)|
|Elizawieta Zajcew|1|[160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|Edward Bankierz|1|[160821](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html)|
|Draconis Diakon|1|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html)|
|Dominik Bankierz|1|[160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|Dionizy Kret|1|[160713](/rpg/inwazja/opowiesci/konspekty/160713-ukrasc-ze-swiecy-zajcewow.html)|
|Diana Weiner|1|[160724](/rpg/inwazja/opowiesci/konspekty/160724-portal-do-eam.html)|
|Dalia Weiner|1|[170103](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html)|
|Artur Janczecki|1|[160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|Amanda Diakon|1|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html)|
|Alina Bednarz|1|[160713](/rpg/inwazja/opowiesci/konspekty/160713-ukrasc-ze-swiecy-zajcewow.html)|
|Alicja Weiner|1|[160724](/rpg/inwazja/opowiesci/konspekty/160724-portal-do-eam.html)|
|Aleksander Sowiński|1|[160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|

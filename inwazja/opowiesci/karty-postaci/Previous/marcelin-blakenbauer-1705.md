---
layout: inwazja-karta-postaci
title:  "Marcelin Blakenbauer"
categories: profile
guild: "Srebrna Świeca"
type: NPC
---

# {{ page.title }}

## Postać

### Motywacje (do czego dąży)

* **Chcę być najlepszym mężczyzną każdej kobiety**: Nie chodzi tutaj tylko o sex. Marcelin chce być postrzegany jako ideał męskości. Perfekcyjny wybór każdej kobiety. Kochanek, przyjaciel, towarzysz, partner.
* **Nie porzucę damy w opresji**: Czasami nawet ślepo rusza na ratunek, a gdy już mu smok poparzy tyłek, to szuka wsparcia u rodziny i przyjaciół
* **Rodzina ponad wszystko**: W jego genach tkwi konieczność wspierania i ochrony swojego rodu i rezydencji.
* **Pociąg do wyjątkowych kobiet**:  Czuje potrzebę przedłużenia ciągłości rodu, a jego naturalnym wyborem są kobiety o najlepszym materiale genetycznym. Nie robi tego świadomie.
* **Chcę zaimponować Hektorowi**: Hektor jest jego najstarszym bratem. Od dzieciństwa Marcelin patrzył na niego z podziwem, ale nie dawał tego po sobie poznać. Marzy mu się, że kiedyś Hektor zobaczy w nim jego prawdziwy potencjał.

### Zachowania (jaka jest)

* **Romantyk**: Do relacji z kobietami wnosi pasję, pikanterię i smak. Mało która potrafi się mu oprzeć. Poeta.
* **Kokieter**: Zbajeruje, oczaruje, poflirtuje
* **Pasjonat**: Gdy już się za coś zabierze, to całym sobą. Będzie cały czas pracował nad udoskonaleniem tego, nawet jeśli już jest dobre.

### Specjalizacje

* **Alchemia**: przygotowywanie magicznych mikstur wpływających na ciało - dekoktów, odwarów i filtrów
* **Herbalistyka**: znajomość i uprawa egzotycznych roślin alchemicznych
* **Transmutacja**: łączenie materii w celu uczynienia jej lepszą, lub nadaniu jej specjalnych właściwości. Wiąże się zazwyczaj z tworzeniem kręgów alchemicznych
* **Feromony**: wszystko co przyciąga istoty ku sobie
* **Budowanie pozytywnego wizerunku**: zwłaszcza o swoim rodzie

### Umiejętności

* **Magiczne laboratoria**: ma szeroką wiedzę na temat magicznych instrumentów swojego fachu
* **Uwodzenie**: wie jak oczarować kobietę
* **Zdobywanie przyjaciół**: ludzie lgną do niego. Wie jak podejść do każdego człowieka/maga/vicinusa
* **Dusza towarzystwa**: umie rozkręcić każdą imprezę. Nie ma przy nim czegoś takiego jak niezręczna cisza.
* **Biochemia**: duża wiedza na temat organizmów żywych i tego jak reagują z substancjami chemicznymi
* **Magia luster**: wiedza o tej zakazanej i niebezpiecznej magii
* **Świat ludzi**: lubi przebiwać wśród ludzi, gdzie ma też wielu przyjaciół
* **Marketing**: zagada, zachęci, wciśnie wszystko

### Cechy

|  Social Aggressive |  Social Devious |  Social Friendly |  Knowledge |  Craft |  Supernatural |  Fortitude |  Nimbleness |
|:==================:|:===============:|:================:|:==========:|:======:|:=============:|:==========:|:===========:|
|        -1           |         0       |         1        |      0     |    1   |       0       |      -1     |      0      |

### Specjalne
* **brak**
* **Klątwa Blakenbauerów**: ze śmiercią mu nie po drodze. Bestia Marcelina ma niezwykłe moce adaptacyjne. Przybiera formę odpowiednią do danej sytuacji, ale najczęściej gadzią.
* **Feromony rezydencji**: ciężko mu się oprzeć
* **Ojciec ma do niego słabość**

## Magia

### Szkoły magiczne

* **Biomancja**: opis
* **Magia materii**: opis
* **Magia zmysłów**: opis
* **Astralika**: opis

### Zaklęcia statyczne

* **nazwa_zaklęcia**: opis_zaklęcia_lub_link
* **nazwa_zaklęcia**: opis_zaklęcia_lub_link

## Zasoby i otoczenie

### Powiązane frakcje

* nazwa_frakcji_jak_ma_to_z_linkiem

### Kogo zna

* **grupy_i_frakcje_które_zna_i_które_ją_znają**: opis
* **grupy_i_frakcje_które_zna_i_które_ją_znają**: opis

### Co ma do dyspozycji:

* **sprzęt_do_jakiego_ma_dostęp_i_postać_charakteryzuje**: opis
* **sprzęt_do_jakiego_ma_dostęp_i_postać_charakteryzuje**: opis

### Surowce

* **Wartość**: 1/2/3
* **Pochodzenie**: dlaczego_1_2_lub_3_i_jak_zarabia

# Opis

krótki opis postaci, rzeczy, jakie warto pamiętać.

### Koncept

Wysokopoziomowy koncept postaci, 1-2 zdania.

### Motto

"Tekst"

# Historia

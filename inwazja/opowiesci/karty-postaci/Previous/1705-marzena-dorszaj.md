---
layout: inwazja-karta-postaci
categories: profile
factions: "Niezrzeszeni"
type: "NPC"
title: "Marzena Dorszaj"
---
# {{ page.title }}

## Postać

### Motywacje (do czego dąży)

* **pragnienie wolności**: "wolność od każdej formy przymusu jest najwyższym możliwym imperatywem"
* **supremacja - kobiety**: "dobry facet to facet pod obcasem; nigdy nas nie zmiażdżą i nie zniszczą"

### Zachowania (jaka jest)

* **radykalna**: "tylko przez rewolucję można coś zmienić w tym skostniałym świecie"
* **mściwa**: "jeśli raz ustąpię, jeśli raz wybaczę - użyjesz tego przeciw mnie"
* **skryta**: "wszystko co powiem może być użyte przeciwko mnie"

### Specjalizacje

* **dominacja**: opis
* **magia bólu**: opis
* **przyzywanie i kontrola demonów**: opis

### Umiejętności

* **szantażystka**
* **adwokat**
* **feministka**
* **okultystka**
* **kapłanka - satanistka**
* **tortury**
* **pielęgniarka**
* **ars amandi**
* **samoobrona**

### Cechy

|  Social Aggressive |  Social Devious |  Social Friendly |  Knowledge |  Craft |  Supernatural |  Fortitude |  Nimbleness |
|:==================:|:===============:|:================:|:==========:|:======:|:=============:|:==========:|:===========:|
|       +1           |         0       |        -1        |     +1     |   -1   |      +1       |     -1     |      0      |

### Specjalne
* transformacja w viciniusa; musi mieć w pobliżu inne kobiety lub powoli się zmienia

## Magia

### Szkoły magiczne

* **biomancja**: opis
* **magia mentalna**: opis
* **kataliza**: opis

### Zaklęcia statyczne

* **nazwa_zaklęcia**: opis_zaklęcia_lub_link
* **nazwa_zaklęcia**: opis_zaklęcia_lub_link

## Zasoby i otoczenie

### Powiązane frakcje

* nazwa_frakcji_jak_ma_to_z_linkiem

### Kogo zna

* **Siostry Światła z Lenartomina**
* **szantażowani "sponsorzy"**
* **poukrywane agentki i sympatyczki**

### Co ma do dyspozycji:

* **kryjówki i miejsca chowania**
* **pozyskiwani specjaliści** (ludzcy)

### Surowce

* **Wartość**: 2
* **Pochodzenie**: Osoba silniej działająca siecią powiązań niż osobiście

# Opis

A 27 years old aspiring lawyer who has always strongly believed in feminist causes. She had seen several cases which were impossible to lose go bad – powerful men exploiting women, which pushed her to question the whole system and the gender equality. She found solace in a Satanist cult, she found the freedom which she seeked.

However, a mage dabbling with blood and strong emotions quickly gets addicted to it. She got consumed by the blood magic and the dark rituals. She started hurting and killing. Until she met her worthy counterpart – Absolutiusz Diakon.

Absolutiusz has enslaved her and adapted her powers against herself. He had an ambition to transform her from defiler into a normal mage, which usually seems impossible. She has managed to shake him off and afterwards Pauline has cured her addiction to blood magic.

Without the deep cravings of the addiction and understanding her magic a bit more, she roams the smaller centers seeking to bring justice to powerful men hurting women.

### Koncept

Wysokopoziomowy koncept postaci, 1-2 zdania.

### Motto

"Tekst"

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|[Wielki sojusz powszechny](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html)|pakt o nieagresji z Henrykiem, Kariną, Nicarettą|Nicaretta|


## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|170120|chciała pomóc "zakrwawionej dziewczynie", skończyła związana. Gorzej, skończyła w sojuszu z Henrykiem, Kariną i Nicarettą...|[Wielki sojusz powszechny](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html)|10/08/05|10/08/06|[Nicaretta](/rpg/inwazja/opowiesci/konspekty/kampania-nicaretta.html)|
|170113|która miała wielki dzień. Uratowała dziewczynę z kościoła, po czym zmasakrowała plan Henryka i zszargała mu opinię (min. odnośnie zamiłowania do dzieci) i zraniła herpyneą Henryka.|['Dzisiaj złapiemy Nicarettę!'](/rpg/inwazja/opowiesci/konspekty/170113-dzisiaj-zlapiemy-nicarette.html)|10/08/01|10/08/04|[Nicaretta](/rpg/inwazja/opowiesci/konspekty/kampania-nicaretta.html)|
|161220|która zastawiła na Henryka pułapkę w domu Hipolita Mraczona; ale nikt w nią nie chciał wejść :-(.|[Zniszczenie posągu Arazille](/rpg/inwazja/opowiesci/konspekty/161220-zniszczenie-posagu-arazille.html)|10/07/27|10/07/30|[Nicaretta](/rpg/inwazja/opowiesci/konspekty/kampania-nicaretta.html)|
|161206|poznała twarz wroga (Henryk) oraz po jego atakach bezpiecznie wycofała Siostry Światła do tajnej kryjówki.|[Ucieczka Sióstr Światła](/rpg/inwazja/opowiesci/konspekty/161206-ucieczka-siostr-swiatla.html)|10/07/25|10/07/26|[Nicaretta](/rpg/inwazja/opowiesci/konspekty/kampania-nicaretta.html)|
|161129|czarodziejka stojąca za Siostrami Światła (femisatanistkami). Po zuchwałej ewakuacji Natalii, zdominowała Maję i kazała jej zabić Huberta.|[Ewakuacja Natalii, wejście maga](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html)|10/07/23|10/07/24|[Nicaretta](/rpg/inwazja/opowiesci/konspekty/kampania-nicaretta.html)|
|151229|która zostawiła Paulinie hasło do kontaktu z ewentualnymi satanistami i oddaliła się w nieznane.|[..choć to na sektę nie pomoże (PT)](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html)|10/06/12|10/06/13|[Ucieczka do Przodka](/rpg/inwazja/opowiesci/konspekty/kampania-ucieczka-do-przodka.html)|
|151223|która sprytnie uwolniła się od Karmelika, by potem Paulina przekształciła ją w normalną czarodziejkę nie defilerkę. Jej przeznaczeniem jest vicinius.|[..można uwolnić czarodziejkę...](/rpg/inwazja/opowiesci/konspekty/151223-mozna-uwolnic-czarodziejke.html)|10/06/10|10/06/11|[Ucieczka do Przodka](/rpg/inwazja/opowiesci/konspekty/kampania-ucieczka-do-przodka.html)|
|151220|pielęgniareczka, defilerka, bateria, zabawka Diakona która stała się "slave" dla Jerzego Karmelika. Bo NullField.|[Z Null Fieldem w garażu...](/rpg/inwazja/opowiesci/konspekty/151220-z-nullfieldem-w-garazu.html)|10/06/08|10/06/09|[Ucieczka do Przodka](/rpg/inwazja/opowiesci/konspekty/kampania-ucieczka-do-przodka.html)|
|151101|która okazała się * magiem przekształconym przez "Adama Diakona" w bezradną zabawkę. Pielęgniareczka, kiedyś kindersatanistka.|[Mafia Gali w szpitalu](/rpg/inwazja/opowiesci/konspekty/151101-mafia-gali-w-szpitalu.html)|10/06/06|10/06/07|[Ucieczka do Przodka](/rpg/inwazja/opowiesci/konspekty/kampania-ucieczka-do-przodka.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|[Henryk Siwiecki](/rpg/inwazja/opowiesci/karty-postaci/1707-henryk-siwiecki.html)|5|[170120](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html), [170113](/rpg/inwazja/opowiesci/konspekty/170113-dzisiaj-zlapiemy-nicarette.html), [161220](/rpg/inwazja/opowiesci/konspekty/161220-zniszczenie-posagu-arazille.html), [161206](/rpg/inwazja/opowiesci/konspekty/161206-ucieczka-siostr-swiatla.html), [161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html)|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1709-paulina-tarczynska.html)|4|[151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html), [151223](/rpg/inwazja/opowiesci/konspekty/151223-mozna-uwolnic-czarodziejke.html), [151220](/rpg/inwazja/opowiesci/konspekty/151220-z-nullfieldem-w-garazu.html), [151101](/rpg/inwazja/opowiesci/konspekty/151101-mafia-gali-w-szpitalu.html)|
|[Jerzy Karmelik](/rpg/inwazja/opowiesci/karty-postaci/9999-jerzy-karmelik.html)|4|[151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html), [151223](/rpg/inwazja/opowiesci/konspekty/151223-mozna-uwolnic-czarodziejke.html), [151220](/rpg/inwazja/opowiesci/konspekty/151220-z-nullfieldem-w-garazu.html), [151101](/rpg/inwazja/opowiesci/konspekty/151101-mafia-gali-w-szpitalu.html)|
|[Hubert Kaldwor](/rpg/inwazja/opowiesci/karty-postaci/9999-hubert-kaldwor.html)|4|[170120](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html), [170113](/rpg/inwazja/opowiesci/konspekty/170113-dzisiaj-zlapiemy-nicarette.html), [161206](/rpg/inwazja/opowiesci/konspekty/161206-ucieczka-siostr-swiatla.html), [161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html)|
|[Andrzej Klepiczek](/rpg/inwazja/opowiesci/karty-postaci/9999-andrzej-klepiczek.html)|4|[170113](/rpg/inwazja/opowiesci/konspekty/170113-dzisiaj-zlapiemy-nicarette.html), [161220](/rpg/inwazja/opowiesci/konspekty/161220-zniszczenie-posagu-arazille.html), [161206](/rpg/inwazja/opowiesci/konspekty/161206-ucieczka-siostr-swiatla.html), [161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html)|
|[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/1709-maria-newa.html)|3|[151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html), [151220](/rpg/inwazja/opowiesci/konspekty/151220-z-nullfieldem-w-garazu.html), [151101](/rpg/inwazja/opowiesci/konspekty/151101-mafia-gali-w-szpitalu.html)|
|[Karina Łoszad](/rpg/inwazja/opowiesci/karty-postaci/9999-karina-loszad.html)|3|[170120](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html), [170113](/rpg/inwazja/opowiesci/konspekty/170113-dzisiaj-zlapiemy-nicarette.html), [161220](/rpg/inwazja/opowiesci/konspekty/161220-zniszczenie-posagu-arazille.html)|
|[Łukasz Perkas](/rpg/inwazja/opowiesci/karty-postaci/9999-lukasz-perkas.html)|2|[151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html), [151220](/rpg/inwazja/opowiesci/konspekty/151220-z-nullfieldem-w-garazu.html)|
|[Witold Małek](/rpg/inwazja/opowiesci/karty-postaci/9999-witold-malek.html)|2|[170120](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html), [170113](/rpg/inwazja/opowiesci/konspekty/170113-dzisiaj-zlapiemy-nicarette.html)|
|[Urszula Kram](/rpg/inwazja/opowiesci/karty-postaci/9999-urszula-kram.html)|2|[161220](/rpg/inwazja/opowiesci/konspekty/161220-zniszczenie-posagu-arazille.html), [161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html)|
|[Ryszard Herman](/rpg/inwazja/opowiesci/karty-postaci/9999-ryszard-herman.html)|2|[151220](/rpg/inwazja/opowiesci/konspekty/151220-z-nullfieldem-w-garazu.html), [151101](/rpg/inwazja/opowiesci/konspekty/151101-mafia-gali-w-szpitalu.html)|
|[Natalia Kaldwor](/rpg/inwazja/opowiesci/karty-postaci/9999-natalia-kaldwor.html)|2|[161220](/rpg/inwazja/opowiesci/konspekty/161220-zniszczenie-posagu-arazille.html), [161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html)|
|[Krystian Korzunio](/rpg/inwazja/opowiesci/karty-postaci/1709-krystian-korzunio.html)|2|[151220](/rpg/inwazja/opowiesci/konspekty/151220-z-nullfieldem-w-garazu.html), [151101](/rpg/inwazja/opowiesci/konspekty/151101-mafia-gali-w-szpitalu.html)|
|[Grażyna Tuloz](/rpg/inwazja/opowiesci/karty-postaci/9999-grazyna-tuloz.html)|2|[151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html), [151223](/rpg/inwazja/opowiesci/konspekty/151223-mozna-uwolnic-czarodziejke.html)|
|[Bartosz Bławatek](/rpg/inwazja/opowiesci/karty-postaci/9999-bartosz-blawatek.html)|2|[151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html), [151220](/rpg/inwazja/opowiesci/konspekty/151220-z-nullfieldem-w-garazu.html)|
|[Anna Osiaczek](/rpg/inwazja/opowiesci/karty-postaci/9999-anna-osiaczek.html)|2|[161220](/rpg/inwazja/opowiesci/konspekty/161220-zniszczenie-posagu-arazille.html), [161206](/rpg/inwazja/opowiesci/konspekty/161206-ucieczka-siostr-swiatla.html)|
|[Amelia Eter](/rpg/inwazja/opowiesci/karty-postaci/9999-amelia-eter.html)|2|[161206](/rpg/inwazja/opowiesci/konspekty/161206-ucieczka-siostr-swiatla.html), [161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html)|
|[Alicja Gąszcz](/rpg/inwazja/opowiesci/karty-postaci/9999-alicja-gaszcz.html)|2|[151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html), [151220](/rpg/inwazja/opowiesci/konspekty/151220-z-nullfieldem-w-garazu.html)|
|[Wiesław Rekin](/rpg/inwazja/opowiesci/karty-postaci/9999-wieslaw-rekin.html)|1|[151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html)|
|[Tymoteusz Maus](/rpg/inwazja/opowiesci/karty-postaci/1705-tymoteusz-maus.html)|1|[151101](/rpg/inwazja/opowiesci/konspekty/151101-mafia-gali-w-szpitalu.html)|
|[Szczepan Szokmaniewicz](/rpg/inwazja/opowiesci/karty-postaci/9999-szczepan-szokmaniewicz.html)|1|[161220](/rpg/inwazja/opowiesci/konspekty/161220-zniszczenie-posagu-arazille.html)|
|[Sebastian Drabon](/rpg/inwazja/opowiesci/karty-postaci/9999-sebastian-drabon.html)|1|[170120](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html)|
|[Romana Kaldwor](/rpg/inwazja/opowiesci/karty-postaci/9999-romana-kaldwor.html)|1|[161220](/rpg/inwazja/opowiesci/konspekty/161220-zniszczenie-posagu-arazille.html)|
|[Renata Krzem](/rpg/inwazja/opowiesci/karty-postaci/9999-renata-krzem.html)|1|[161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html)|
|[Nicaretta](/rpg/inwazja/opowiesci/karty-postaci/1705-nicaretta.html)|1|[170120](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html)|
|[Mirek Kujec](/rpg/inwazja/opowiesci/karty-postaci/9999-mirek-kujec.html)|1|[151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html)|
|[Maja Liszka](/rpg/inwazja/opowiesci/karty-postaci/1709-maja-liszka.html)|1|[161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html)|
|[Kora Panik](/rpg/inwazja/opowiesci/karty-postaci/9999-kora-panik.html)|1|[161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html)|
|[Kinglord](/rpg/inwazja/opowiesci/karty-postaci/9999-kinglord.html)|1|[170120](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html)|
|[Joachim Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-zajcew.html)|1|[151220](/rpg/inwazja/opowiesci/konspekty/151220-z-nullfieldem-w-garazu.html)|
|[Ireneusz Przaśnik](/rpg/inwazja/opowiesci/karty-postaci/9999-ireneusz-przasnik.html)|1|[151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html)|
|[Hipolit Mraczon](/rpg/inwazja/opowiesci/karty-postaci/9999-hipolit-mraczon.html)|1|[161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html)|
|[Grażyna Diadem](/rpg/inwazja/opowiesci/karty-postaci/9999-grazyna-diadem.html)|1|[170120](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html)|
|[Gala Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-gala-zajcew.html)|1|[151101](/rpg/inwazja/opowiesci/konspekty/151101-mafia-gali-w-szpitalu.html)|
|[Felicja Wydech](/rpg/inwazja/opowiesci/karty-postaci/9999-felicja-wydech.html)|1|[161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html)|
|[Artur Kurczak](/rpg/inwazja/opowiesci/karty-postaci/9999-artur-kurczak.html)|1|[151220](/rpg/inwazja/opowiesci/konspekty/151220-z-nullfieldem-w-garazu.html)|
|[Arazille](/rpg/inwazja/opowiesci/karty-postaci/9999-arazille.html)|1|[161220](/rpg/inwazja/opowiesci/konspekty/161220-zniszczenie-posagu-arazille.html)|
|[Adela Klepiczek](/rpg/inwazja/opowiesci/karty-postaci/9999-adela-klepiczek.html)|1|[161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html)|

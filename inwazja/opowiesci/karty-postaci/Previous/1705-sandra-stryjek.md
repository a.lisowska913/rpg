---
layout: inwazja-karta-postaci
categories: profile
factions: "Srebrna Świeca"
type: "NPC"
title: "Sandra Stryjek"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **impuls_co_chce_osiągnąć**: cytat_lub_opis_jak_to_rozumieć

### Zachowania (jaka jest)

* **impuls_jak_to_realizuje**: cytat_lub_opis_jak_to_rozumieć

### Specjalizacje

* **czynność_na_poziomie_specjalizacji**: opis
* **czynność_na_poziomie_specjalizacji**: opis
* **czynność_na_poziomie_specjalizacji**: opis
* **czynność_na_poziomie_specjalizacji**: opis

### Umiejętności

* **mechanik**: opis
* **magia krwi**: tylko i wyłącznie teoretycznie

### Cechy:

|  Social Aggressive |  Social Devious |  Social Friendly |  Knowledge |  Craft |  Supernatural |  Fortitude |  Nimbleness |
|:==================:|:===============:|:================:|:==========:|:======:|:=============:|:==========:|:===========:|
|       -1           |        -1       |        -1        |     +1     |   +1   |       0       |      0     |     +1      |

### Specjalne

* **nazwa_specjalnej_własności**: opis_własności_lub_link

## Magia

### Szkoły magiczne

* **Magia mentalna**
* **Technomancja**

### Zaklęcia statyczne

* **nazwa_zaklęcia**: opis_zaklęcia_lub_link
* **nazwa_zaklęcia**: opis_zaklęcia_lub_link

## Zasoby i otoczenie

### Powiązane frakcje

* nazwa_frakcji_jak_ma_to_z_linkiem

### Kogo zna

* **grupy_i_frakcje_które_zna_i_które_ją_znają**: opis
* **grupy_i_frakcje_które_zna_i_które_ją_znają**: opis

### Co ma do dyspozycji:

* **sprzęt_do_jakiego_ma_dostęp_i_postać_charakteryzuje**: opis
* **sprzęt_do_jakiego_ma_dostęp_i_postać_charakteryzuje**: opis

### Surowce

* **Wartość**: 1/2/3
* **Pochodzenie**: dlaczego_1_2_lub_3_i_jak_zarabia

# Opis

Astralistka i technomantka. Bardzo słaba socjalnie. Dobry mechanik. Łączy świat ludzi ze światem magów, w obu jest obca.
Ludzistka. Podkochuje się w Auguście Bankierzu.
Optymistyczna i bardzo samotna czarodziejka która lgnie do ludzi i magów, ale zawsze jest sama, co jest dla niej bolesne. Lubi się przytulać i dawać prezenty.

"
Jej ojciec utracił magię podczas Zaćmienia, ale nie było z tym żadnych problemów. Był cenionym członkiem społeczności i nie musiał czarować, jego wiedza wystarczała. Matka tworzyła artefakty, dzięki którym mógł udawać że ma moc. Sandra to publicznie wyznała i zgłosiła to do odpowiednich sił. Nie dało się tego zatuszować. Nie było wyjścia - prawo jest jedno. Ojciec został wymazany a matka dostała pouczenie. Wyparła się Sandry.
[Od Żółwia i Kić: W rzeczywistości matka zaczęła badać czarne rytuały by on odzyskał moc i Sandra to zgłosiła, by chronić matkę i powstrzymać ją przed czymś naprawdę złym. Matka dostała geas, by nie mogła się interesować takimi rzeczami, ojciec został wymazany. Reszta się zgadza. Sandra dostała nawet za to nagrodę, która jednak była "zatrutą pigułką".]
"

"
(…) zobaczyła tam Sandrę, która przyłapała swoją mamę na rytuałach magii krwi i to tych bardzo zakazanych; łącznie z ofiarą z człowieka. Następna scena - Sandra mówiąca o tym terminusom. Następna, jakiś mag mówi, że Sandra nigdy nie może nikomu powiedzieć co widziała i co zgłosiła. Dostała jakąś nagrodę. W kolejnej scenie - matka się wyparła Sandry a o Sandrze poszła fama, że nakablowała na mamę ponieważ mama ukrywała to, że ojciec nie jest magiem. I gildia się od niej odwróciła.
Andromeda widziała też sceny, w których Sandra studiuje Magię Krwi teoretycznie (ale nie stosuje tego w praktyce). Szuka. Ciągle szuka.
I sceny gdzie Sandra obserwuje Augusta. Obserwuje Andromedę. August, gardzący Sandrą, w którym Sandra jest zakochana. Andromeda, broniąca przed Augustem Sandrę. Andromeda, która jest człowiekiem o czym wie zarówno August jak i teraz Sandra.
I Sandra milczała. Nigdy się nie zdradziła. Jest tak samotna, że postrzega wszystkich jako zagrożenie

## Motto

"Tekst"

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|150104|typowy nerd grzebiący w bazach danych po udanym włamie.|[Terminus-defiler, kapłan Arazille](/rpg/inwazja/opowiesci/konspekty/150104-terminus-defiler-kaplan-arazille.html)|10/02/17|10/02/18|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|
|150103|eksplorująca swoją nową sytuację z dziwnym połączeniem luster z Andromedą. Niechętna sojuszniczka Andromedy, operuje w Wyjorzu.|[Pryzmat Myśli pęka](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html)|10/02/15|10/02/16|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|
|141230|bondage expert-to-be, która też (jak się okazuje) nie zapomina. Ale musi się nauczyć zamykać drzwi na klucz.|[Ofiara z wampira dla Arazille](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html)|10/02/13|10/02/14|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|
|141227|technomantka i astralistka która przy okazji jest ekspertem od mrocznych rytuałów. Ale się nie przyzna.|[Przyczajona Andromeda, ukryty Maus](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|10/02/11|10/02/12|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|
|141220|technomantka i astralistka która przy okazji jest ekspertem od mrocznych rytuałów. Ale się nie przyzna.|[Bogini Marzeń w Żonkiborze](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|10/02/09|10/02/10|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|
|141218|nieświadoma niczego technomantka o kiepskiej przeszłości zakochana w Auguście, zaszkodziła Augustowi.|[Portret Boga](/rpg/inwazja/opowiesci/konspekty/141218-portret-boga.html)|10/02/07|10/02/08|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|
|150105|kompetentna technomantka która nie była w stanie oprzeć się straszliwym wizjom Iliusitiusa.|[Dar Iliusitiusa dla Andromedy](/rpg/inwazja/opowiesci/konspekty/150105-dar-iliusitiusa-dla-andromedy.html)|10/02/05|10/02/06|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|
|131008|unikająca Augusta podkochująca się czarodziejka robiąca dywersję. Bo plan. I żądająca kompotu brukselkowego.|['Mój Anioł'](/rpg/inwazja/opowiesci/konspekty/131008-moj-aniol.html)|10/02/05|10/02/06|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|
|130511|która potrafiła unikać problemów dla Augusta jak tylko Kasia poprosiła ją o to, by się przed nim schowała. Astralistka i technomantka.|[Ołtarz Podniesionej Dłoni](/rpg/inwazja/opowiesci/konspekty/130511-oltarz-podniesionej-dloni.html)|10/02/03|10/02/04|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|
|130506|podkochująca się w Auguście ludzistka i technomantka strasznie o Augusta zazdrosna i robiąca błędne założenia.|[Sekrety Rezydencji Szczypiorkow](/rpg/inwazja/opowiesci/konspekty/130506-sekrety-rezydencji-szczypiorkow.html)|10/02/01|10/02/02|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|
|170816|która ZNOWU uratowała Augusta. Dyskretnie pomaga Mordredowi przeciw Augustowi i inteligentnie współpracuje. Cicha bohaterka Augusta. Niedoceniona przez nikogo.|[Na wezwanie Iliusitiusa](/rpg/inwazja/opowiesci/konspekty/170816-na-wezwanie-iliusitiusa.html)|10/01/27|10/01/29|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|
|150501|czarodziejka która dostarczyła Augustowi artefakt w ramach zaakceptowania przeprosin. Artefakt śledzi Augusta gdy ów go używa.|[Szalona 'czarodziejka' Zależa](/rpg/inwazja/opowiesci/konspekty/150501-szalona-czarodziejka-zaleza.html)|10/01/07|10/01/08|[Światło w Zależu Leśnym](/rpg/inwazja/opowiesci/konspekty/kampania-swiatlo-w-zalezu-lesnym.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|[Kasia Nowak](/rpg/inwazja/opowiesci/karty-postaci/1709-kasia-nowak.html)|10|[150104](/rpg/inwazja/opowiesci/konspekty/150104-terminus-defiler-kaplan-arazille.html), [150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html), [141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html), [141218](/rpg/inwazja/opowiesci/konspekty/141218-portret-boga.html), [150105](/rpg/inwazja/opowiesci/konspekty/150105-dar-iliusitiusa-dla-andromedy.html), [131008](/rpg/inwazja/opowiesci/konspekty/131008-moj-aniol.html), [130511](/rpg/inwazja/opowiesci/konspekty/130511-oltarz-podniesionej-dloni.html), [130506](/rpg/inwazja/opowiesci/konspekty/130506-sekrety-rezydencji-szczypiorkow.html)|
|[August Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-august-bankierz.html)|9|[150104](/rpg/inwazja/opowiesci/konspekty/150104-terminus-defiler-kaplan-arazille.html), [150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141218](/rpg/inwazja/opowiesci/konspekty/141218-portret-boga.html), [150105](/rpg/inwazja/opowiesci/konspekty/150105-dar-iliusitiusa-dla-andromedy.html), [131008](/rpg/inwazja/opowiesci/konspekty/131008-moj-aniol.html), [130511](/rpg/inwazja/opowiesci/konspekty/130511-oltarz-podniesionej-dloni.html), [130506](/rpg/inwazja/opowiesci/konspekty/130506-sekrety-rezydencji-szczypiorkow.html), [170816](/rpg/inwazja/opowiesci/konspekty/170816-na-wezwanie-iliusitiusa.html), [150501](/rpg/inwazja/opowiesci/konspekty/150501-szalona-czarodziejka-zaleza.html)|
|[Samira Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-samira-diakon.html)|8|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html), [141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html), [141218](/rpg/inwazja/opowiesci/konspekty/141218-portret-boga.html), [150105](/rpg/inwazja/opowiesci/konspekty/150105-dar-iliusitiusa-dla-andromedy.html), [131008](/rpg/inwazja/opowiesci/konspekty/131008-moj-aniol.html), [130511](/rpg/inwazja/opowiesci/konspekty/130511-oltarz-podniesionej-dloni.html), [130506](/rpg/inwazja/opowiesci/konspekty/130506-sekrety-rezydencji-szczypiorkow.html)|
|[Herbert Zioło](/rpg/inwazja/opowiesci/karty-postaci/1709-herbert-ziolo.html)|7|[150104](/rpg/inwazja/opowiesci/konspekty/150104-terminus-defiler-kaplan-arazille.html), [150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html), [141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html), [150105](/rpg/inwazja/opowiesci/konspekty/150105-dar-iliusitiusa-dla-andromedy.html), [130511](/rpg/inwazja/opowiesci/konspekty/130511-oltarz-podniesionej-dloni.html)|
|[Iliusitius](/rpg/inwazja/opowiesci/karty-postaci/9999-iliusitius.html)|6|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html), [150105](/rpg/inwazja/opowiesci/konspekty/150105-dar-iliusitiusa-dla-andromedy.html), [170816](/rpg/inwazja/opowiesci/konspekty/170816-na-wezwanie-iliusitiusa.html), [150501](/rpg/inwazja/opowiesci/konspekty/150501-szalona-czarodziejka-zaleza.html)|
|[Feliks Bozur](/rpg/inwazja/opowiesci/karty-postaci/9999-feliks-bozur.html)|5|[150104](/rpg/inwazja/opowiesci/konspekty/150104-terminus-defiler-kaplan-arazille.html), [150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [150105](/rpg/inwazja/opowiesci/konspekty/150105-dar-iliusitiusa-dla-andromedy.html), [130511](/rpg/inwazja/opowiesci/konspekty/130511-oltarz-podniesionej-dloni.html)|
|[Wojciech Kajak](/rpg/inwazja/opowiesci/karty-postaci/9999-wojciech-kajak.html)|4|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html), [141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|
|[Netheria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-netheria-diakon.html)|4|[150104](/rpg/inwazja/opowiesci/konspekty/150104-terminus-defiler-kaplan-arazille.html), [150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Arazille](/rpg/inwazja/opowiesci/karty-postaci/9999-arazille.html)|4|[150104](/rpg/inwazja/opowiesci/konspekty/150104-terminus-defiler-kaplan-arazille.html), [150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Anna Kajak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kajak.html)|4|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html), [141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|
|[Żanna Szczypiorek](/rpg/inwazja/opowiesci/karty-postaci/9999-zanna-szczypiorek.html)|3|[131008](/rpg/inwazja/opowiesci/konspekty/131008-moj-aniol.html), [130511](/rpg/inwazja/opowiesci/konspekty/130511-oltarz-podniesionej-dloni.html), [130506](/rpg/inwazja/opowiesci/konspekty/130506-sekrety-rezydencji-szczypiorkow.html)|
|[Radosław Szczypiorek](/rpg/inwazja/opowiesci/karty-postaci/9999-radoslaw-szczypiorek.html)|3|[131008](/rpg/inwazja/opowiesci/konspekty/131008-moj-aniol.html), [130511](/rpg/inwazja/opowiesci/konspekty/130511-oltarz-podniesionej-dloni.html), [130506](/rpg/inwazja/opowiesci/konspekty/130506-sekrety-rezydencji-szczypiorkow.html)|
|[Patryk Romczak](/rpg/inwazja/opowiesci/karty-postaci/9999-patryk-romczak.html)|3|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html), [150105](/rpg/inwazja/opowiesci/konspekty/150105-dar-iliusitiusa-dla-andromedy.html)|
|[Małgorzata Poran](/rpg/inwazja/opowiesci/karty-postaci/9999-malgorzata-poran.html)|3|[141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html), [141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html), [141218](/rpg/inwazja/opowiesci/konspekty/141218-portret-boga.html)|
|[Luksja Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-luksja-diakon.html)|3|[150104](/rpg/inwazja/opowiesci/konspekty/150104-terminus-defiler-kaplan-arazille.html), [150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Józef Pimczak](/rpg/inwazja/opowiesci/karty-postaci/9999-jozef-pimczak.html)|3|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html), [141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|
|[Inga Wójt](/rpg/inwazja/opowiesci/karty-postaci/9999-inga-wojt.html)|3|[131008](/rpg/inwazja/opowiesci/konspekty/131008-moj-aniol.html), [130511](/rpg/inwazja/opowiesci/konspekty/130511-oltarz-podniesionej-dloni.html), [130506](/rpg/inwazja/opowiesci/konspekty/130506-sekrety-rezydencji-szczypiorkow.html)|
|[Gabriel Newa](/rpg/inwazja/opowiesci/karty-postaci/9999-gabriel-newa.html)|3|[150104](/rpg/inwazja/opowiesci/konspekty/150104-terminus-defiler-kaplan-arazille.html), [150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html)|
|[Feliks Hanson](/rpg/inwazja/opowiesci/karty-postaci/9999-feliks-hanson.html)|3|[150104](/rpg/inwazja/opowiesci/konspekty/150104-terminus-defiler-kaplan-arazille.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Augustyn Szczypiorek](/rpg/inwazja/opowiesci/karty-postaci/9999-augustyn-szczypiorek.html)|3|[131008](/rpg/inwazja/opowiesci/konspekty/131008-moj-aniol.html), [130511](/rpg/inwazja/opowiesci/konspekty/130511-oltarz-podniesionej-dloni.html), [130506](/rpg/inwazja/opowiesci/konspekty/130506-sekrety-rezydencji-szczypiorkow.html)|
|[Andrzej Szop](/rpg/inwazja/opowiesci/karty-postaci/9999-andrzej-szop.html)|3|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html), [150105](/rpg/inwazja/opowiesci/konspekty/150105-dar-iliusitiusa-dla-andromedy.html)|
|[Zofia Szczypiorek](/rpg/inwazja/opowiesci/karty-postaci/9999-zofia-szczypiorek.html)|2|[150105](/rpg/inwazja/opowiesci/konspekty/150105-dar-iliusitiusa-dla-andromedy.html), [131008](/rpg/inwazja/opowiesci/konspekty/131008-moj-aniol.html)|
|[Rafał Szczęślik](/rpg/inwazja/opowiesci/karty-postaci/9999-rafal-szczeslik.html)|2|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Michał Czuk](/rpg/inwazja/opowiesci/karty-postaci/9999-michal-czuk.html)|2|[141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html), [141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|
|[Karradrael](/rpg/inwazja/opowiesci/karty-postaci/9999-karradrael.html)|2|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Jolanta Wójt](/rpg/inwazja/opowiesci/karty-postaci/9999-jolanta-wojt.html)|2|[131008](/rpg/inwazja/opowiesci/konspekty/131008-moj-aniol.html), [130506](/rpg/inwazja/opowiesci/konspekty/130506-sekrety-rezydencji-szczypiorkow.html)|
|[Dariusz Germont](/rpg/inwazja/opowiesci/karty-postaci/9999-dariusz-germont.html)|2|[150105](/rpg/inwazja/opowiesci/konspekty/150105-dar-iliusitiusa-dla-andromedy.html), [131008](/rpg/inwazja/opowiesci/konspekty/131008-moj-aniol.html)|
|[Artur Szmelc](/rpg/inwazja/opowiesci/karty-postaci/9999-artur-szmelc.html)|2|[141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html), [141218](/rpg/inwazja/opowiesci/konspekty/141218-portret-boga.html)|
|[Aneta Hanson](/rpg/inwazja/opowiesci/karty-postaci/9999-aneta-hanson.html)|2|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Andrzej Domowierzec](/rpg/inwazja/opowiesci/karty-postaci/9999-andrzej-domowierzec.html)|2|[141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html), [141218](/rpg/inwazja/opowiesci/konspekty/141218-portret-boga.html)|
|[Szymon Skubny](/rpg/inwazja/opowiesci/karty-postaci/9999-szymon-skubny.html)|1|[141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Szczepan Zaleski](/rpg/inwazja/opowiesci/karty-postaci/9999-szczepan-zaleski.html)|1|[150501](/rpg/inwazja/opowiesci/konspekty/150501-szalona-czarodziejka-zaleza.html)|
|[Sebastian Linka](/rpg/inwazja/opowiesci/karty-postaci/9999-sebastian-linka.html)|1|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html)|
|[Rufus Eter](/rpg/inwazja/opowiesci/karty-postaci/9999-rufus-eter.html)|1|[150501](/rpg/inwazja/opowiesci/konspekty/150501-szalona-czarodziejka-zaleza.html)|
|[Robert Przerot](/rpg/inwazja/opowiesci/karty-postaci/9999-robert-przerot.html)|1|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html)|
|[Rafał Warkocz](/rpg/inwazja/opowiesci/karty-postaci/9999-rafal-warkocz.html)|1|[170816](/rpg/inwazja/opowiesci/konspekty/170816-na-wezwanie-iliusitiusa.html)|
|[Rafał Czapiek](/rpg/inwazja/opowiesci/karty-postaci/9999-rafal-czapiek.html)|1|[150501](/rpg/inwazja/opowiesci/konspekty/150501-szalona-czarodziejka-zaleza.html)|
|[Paweł Franna](/rpg/inwazja/opowiesci/karty-postaci/9999-pawel-franna.html)|1|[150501](/rpg/inwazja/opowiesci/konspekty/150501-szalona-czarodziejka-zaleza.html)|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1709-paulina-tarczynska.html)|1|[150501](/rpg/inwazja/opowiesci/konspekty/150501-szalona-czarodziejka-zaleza.html)|
|[Olga Miodownik](/rpg/inwazja/opowiesci/karty-postaci/1709-olga-miodownik.html)|1|[150501](/rpg/inwazja/opowiesci/konspekty/150501-szalona-czarodziejka-zaleza.html)|
|[Mordred Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1707-mordred-blakenbauer.html)|1|[170816](/rpg/inwazja/opowiesci/konspekty/170816-na-wezwanie-iliusitiusa.html)|
|[Mirabelka Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-mirabelka-diakon.html)|1|[150104](/rpg/inwazja/opowiesci/konspekty/150104-terminus-defiler-kaplan-arazille.html)|
|[Marianna Zurka](/rpg/inwazja/opowiesci/karty-postaci/9999-marianna-zurka.html)|1|[150501](/rpg/inwazja/opowiesci/konspekty/150501-szalona-czarodziejka-zaleza.html)|
|[Marianna Biegłomir](/rpg/inwazja/opowiesci/karty-postaci/9999-marianna-bieglomir.html)|1|[170816](/rpg/inwazja/opowiesci/konspekty/170816-na-wezwanie-iliusitiusa.html)|
|[Manfred Jarosz](/rpg/inwazja/opowiesci/karty-postaci/9999-manfred-jarosz.html)|1|[170816](/rpg/inwazja/opowiesci/konspekty/170816-na-wezwanie-iliusitiusa.html)|
|[Maciej Dworek](/rpg/inwazja/opowiesci/karty-postaci/9999-maciej-dworek.html)|1|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html)|
|[Luiza Wanta](/rpg/inwazja/opowiesci/karty-postaci/9999-luiza-wanta.html)|1|[141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Klara Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1707-klara-blakenbauer.html)|1|[170816](/rpg/inwazja/opowiesci/konspekty/170816-na-wezwanie-iliusitiusa.html)|
|[Karolina Błazoń](/rpg/inwazja/opowiesci/karty-postaci/9999-karolina-blazon.html)|1|[150501](/rpg/inwazja/opowiesci/konspekty/150501-szalona-czarodziejka-zaleza.html)|
|[Kamil Gurnat](/rpg/inwazja/opowiesci/karty-postaci/9999-kamil-gurnat.html)|1|[150501](/rpg/inwazja/opowiesci/konspekty/150501-szalona-czarodziejka-zaleza.html)|
|[Józef Pasan](/rpg/inwazja/opowiesci/karty-postaci/9999-jozef-pasan.html)|1|[150105](/rpg/inwazja/opowiesci/konspekty/150105-dar-iliusitiusa-dla-andromedy.html)|
|[Izabela Kruczek](/rpg/inwazja/opowiesci/karty-postaci/9999-izabela-kruczek.html)|1|[130506](/rpg/inwazja/opowiesci/konspekty/130506-sekrety-rezydencji-szczypiorkow.html)|
|[Inga Błazoń](/rpg/inwazja/opowiesci/karty-postaci/9999-inga-blazon.html)|1|[150501](/rpg/inwazja/opowiesci/konspekty/150501-szalona-czarodziejka-zaleza.html)|
|[Hektor Poran](/rpg/inwazja/opowiesci/karty-postaci/9999-hektor-poran.html)|1|[141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|
|[Franciszek Błazoń](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-blazon.html)|1|[150501](/rpg/inwazja/opowiesci/konspekty/150501-szalona-czarodziejka-zaleza.html)|
|[Filip Sztukar](/rpg/inwazja/opowiesci/karty-postaci/9999-filip-sztukar.html)|1|[141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Ewa Czuk](/rpg/inwazja/opowiesci/karty-postaci/9999-ewa-czuk.html)|1|[141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|1|[170816](/rpg/inwazja/opowiesci/konspekty/170816-na-wezwanie-iliusitiusa.html)|
|[Dariusz Remont](/rpg/inwazja/opowiesci/karty-postaci/9999-dariusz-remont.html)|1|[150501](/rpg/inwazja/opowiesci/konspekty/150501-szalona-czarodziejka-zaleza.html)|
|[Antoni Wójt](/rpg/inwazja/opowiesci/karty-postaci/9999-antoni-wojt.html)|1|[131008](/rpg/inwazja/opowiesci/konspekty/131008-moj-aniol.html)|
|[Anabela Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-anabela-diakon.html)|1|[150105](/rpg/inwazja/opowiesci/konspekty/150105-dar-iliusitiusa-dla-andromedy.html)|
|[Amelia Eter](/rpg/inwazja/opowiesci/karty-postaci/9999-amelia-eter.html)|1|[150104](/rpg/inwazja/opowiesci/konspekty/150104-terminus-defiler-kaplan-arazille.html)|

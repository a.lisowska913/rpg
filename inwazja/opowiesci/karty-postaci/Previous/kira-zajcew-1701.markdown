---
layout: inwazja-karta-postaci
title:  "Kira Zajcew"
categories: profile, srebrna_swieca, postac_gracza
---
# {{ page.title }}

## Impulsy (co motywuje / napędza postać):

### Strategiczne


### Taktyczne

- **nie lubi dziennikarzy / prasy itp**: "wystarczy, że raz zniszczyliście mi karierę"
- **opiekuńcza wobec mieszkańców wsi** : "to MOJA wieś i jak się nie będziesz zachowywał to stanie się Twoim grobem…"
- **geek źródeł energii** : "mam JESZCZE INNĄ metodę zasilania! wee!"

## Co umie (co postać potrafi):


### Ekspercko (poziom 3):

- **pozyskiwanie energii z organizmów żywych**: (bio / technomantka)
- **kontrola przepływów energii**: (katalistka)
- **niszczycielska kataliza**: (katalistka)


### Zawodowo (poziom 2):

- **biomantka**
- **technomantka**
- **katalistka**
- **ognista magia Zajcewów**
- **zabezpieczenia magiczne: głównie oparte o energię i katalizę**

### Amatorsko (poziom 1):

- **rolnik**:


## Inklinacje:

|  Social Aggressive |  Social Devious |  Social Friendly |  Knowledge |  Craft |  Supernatural |  Fortitude |  Nimbleness |
|:==================:|:===============:|:================:|:==========:|:======:|:=============:|:==========:|:===========:|
|        0           |         -1      |         0        |     +1     |   +1   |      +1       |     -1     |     -1      |



## Kieszonkowe
2

## Otoczenie powiązane

### Powiązane frakcje



### Kogo zna

#### Doskonale: 3

- **wąskie grono specjalistów od energii**: 

#### Nieźle: 2

- **kataliści**: opis
- **miejscowi ludzie**: Jako pani sołtysowa ma całkiem spore wpływy we wsi, potrafi sporo zorganizować i załatwić dzięki s ojej pozycji.

#### Luźno: 1
  

### Co ma do dyspozycji:

#### Szczególnie dobre: 3


- **własnej produkcji źródła energii**: Kira lubi konstruować przeróżne źródła energii i zwykle ma jakieś w zapasie
- **warsztat**: na wsi nigdy się nic nie marnuje. W stodole jest zawsze mnóstwo uszkodzonych sprzętów, które można naprawić lub rozłożyć na części. Dodatkowo, ich warsztat jest naprawdę dobrze zaopatrzony.

#### Ponadprzeciętne: 2

- **komponenty do budowy źródeł**: 2
- **Świeca: reputacja defilerki**: w Świecy uważana za dziwną defilerkę. Magowie się jej boją. Stały mallus do akcji social friendly, bonus do social aggressive, zależny od stopnia przestraszenia drugiej strony. 2

#### Niezłe: 1

- **artefakty magii mentalnej**: do czyszczenia pamięci w razie konieczności 1

## Magia:

### Magia dynamiczna:

- transformacja energii
- technomancja
- biomancja (nie-krzywdzące zmiany w przepływie energii, magia rolnicza)
- magia ognia

### Zaklęcia hermetyczne

- "Wielkie wzmocnienie Igora" : link (zaklęcie dające ogromne wzmocnienie - siła, prędkość, postrzeganie... - kosztem późniejszego wyczerpania)

## Specjalne:

## Inne:

Mieszka we wsi Stokrotki.
Wyklęta przez magów, uważana powszechnie za defilerkę. Cieszy się opinią wyjątkowo przerażającej - nie dość, że w jej pobliżu nie ma śladów magii krwi, to jeszcze ona sama nie poddała się uzależnieniu...

We wsi nazywają ją "panią inżynier", choć nigdy nie twierdziła, że ma jakiś dyplom. Ale zna się na rzeczy, a urządzenia w jej pobliżu działają lepiej a wiejskie generatory energii ze źródeł odnawialnych regularnie dostarczające energię do sieci - zysk dla gminy.

Do swoich projektów wykorzystuje różne narzędzia, z mężem mają naprawdę dobrze wyposażony warsztat.

1. Z czego TP jest szczególnie dumna (co jest doceniane przez innych)? Dlaczego jest z tego dumna?
To ona skonstruowała jeden z generatorów w kompleksie centralnym Świecy. Dlaczego jest z tego dumna? Bo to majstersztyk w tej dziedzinie i jej autorski pomysł. Bez jej udziału rzecz nie byłaby warta zachodu.

1. Z czego TP jest znana najlepiej wśród innych? Wśród kogo (kim są ci "inni")?
Jej sława ma trzy aspekty:
a) "Ta dziwna defilerka, która się nie uzależniła" - ci, którzy nie rozumieją jej fachu boją się jej, uważając za defilerkę. Do tej grupy zalicza się większość tych, którzy poszukali podstawowych informacji na jej temat.
b) Wśród katalistów znana jest jako spec od pozyskiwania i manipulacji energią.
c) Wśród ludzi uważana za inżyniera - złotą rączkę, która potrafi naprawić każde urządzenie.

1. Co jest uważane za nieuczciwą przewagę TP?
Praktycznie zawsze i każdej sytuacji jest w stanie zdobyć energię - czy to magiczną, czy inną, jak długo w pobliżu jest coś żywego.

1. Co sprawia, że TP uzna, że TO ZADANIE jest perfekcyjne dla niej? Czemu?
Każda okazja do wykorzystania lub wytworzenia nowego rodzaju energii. Wszelkie wzmocnienie organizmów żywych poprzez naenergetyzowanie (może skutkować dodatkowymi zdolnościami typu porażenie prądem), jak również potrzeba usunięcia nadmiaru energii.

1. Kiedy TP powie "O nie, tak się nie stanie". Dlaczego? Jak spróbuje temu zaradzić?
Jest bardzo opiekuńcza wobec swojego terenu (wioski). Ludzi w niej traktuje zgodnie z zasadą "może to tylko ludzie, ale to MOI ludzie". Poza tym, jest sołtysiną tej wsi, siłą rzeczy musiała nawiązać z nimi kontakt.
Mimo wszystko jest mocno związana z rodem. Pomoże jak będzie mogła, co nie oznacza, że za darmo...

1. W jakich okolicznościach "Jak trwoga, to przyjdą do TP"? Kto przyjdzie?
"Jak trwoga to do Kiry" w każdej sytuacji, gdy potrzeba zasilić / naładować coś dziwnego. 
Kto przyjdzie: rodzina, ktoś nieświadomy jej "sławy" lub ktoś dostatecznie mocno zdesperowany.

1. Czego TP najbardziej żałuje? Co zrobiłaby inaczej? Dlaczego?
Gdyby potrafiła jakoś zmienić swoją opinię wśród magów... Niestety, nie umiała tego zrobić na początku, a potem było już za późno...

1. O czym TP marzy? Do czego dąży? Dlaczego?

1. Co TP najbardziej lubi robić w czasie wolnym? Czemu?

1. Jakie wyzwanie TP dała radę przezwyciężyć? Co ją to nauczyło?



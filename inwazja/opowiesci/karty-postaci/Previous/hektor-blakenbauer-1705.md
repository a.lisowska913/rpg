---
layout: inwazja-karta-postaci
title:  "Hektor Blakenbauer"
categories: profile
guild: none
type: PC
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **Zbudwanie praworządnej prokuratury magów**:
* **Zapewnienie bezpieczeństwa rodzinie**: Rodzina ma  Hektora znaczenie priorytetowe. Nigdy nie pozwoli, żeby ktoś (lub coś) wyrządził jej krzywdę.
* **Kolekcjoner**: Jak większość członków rodu, Hektor ma potrzebę kolekcjonowania artefaktów i okazów dla rezydencji, gdy nadarza się ku temu okazja.
* **Obrońca ludzi**: Traktuje ludzi na równi z magami. Nie pozwoli, żeby ktoś robił im krzywdę tylko dlatego, że umie władać magią.

### Zachowania (jaka jest) (jaki jest)

* **Dociekliwy**: Przed prokuratorem nie ma tajemnic
* **Twardy/Stanowczy**: Czy to dyrygując służbami specjalnymi, krusząc obrońców w sądzie, czy też przesłuchując. Jest też nieugięty, gdy staje w obronie kogoś, na kim mu zależy.
* **Kulturalny**: Hektor potrafi się doskonale odnaleźć w sytuacji, gdzie potrzebne są dobre maniery.

## Co umie (co postać potrafi):

### Specjalizacje

* **Przesłuchiwanie**: Wyciąganie zeznań i wszelkiego rodzaju informacji.
* **Mówca prawa**: Przemawianie na salach sądowych i ogólnie w sprawach związanych z prawem
* **Lekarz umysłu**: Silnie ukierunkowany na kojenie i czyszczenie umysłów
* **Beast master**: Potrafi kontrolować bestie rezydencji
* **Mentalna inkwizycja**: Skanowanie umysłu, kradzież wspomnień, zmuszanie do współpracy.
* **Architekt materii**: Dostosowywanie terenu (w szczególności industrialnego) do potrzeb sytuacji

### Umiejętności

* **Prokurator Okręgowy**
* **Inkwizytor**
* **Przekonywanie do współpracy**
* **Biurokracja**
* **Techniki śledcze**
* **Samoobrona**
* **Życie w świecie ludzi**
* **Modelarstwo**
* **Savoir-vivre**
* **Mecenas**
* **Życie wśród potworów**
* **Artefakty mentalne**

### Szkoły magiczne

* **Mentalista**
* **Magia materii**
* **Biomancja**

### Cechy:

|  Social Aggressive |  Social Devious |  Social Friendly |  Knowledge |  Craft |  Supernatural |  Fortitude |  Nimbleness |
|:==================:|:===============:|:================:|:==========:|:======:|:=============:|:==========:|:===========:|
|       +1           |         0       |         0        |      +1     |   -1   |      -1       |     0     |      0      |

### Specjalne

* klątwa

## Magia

### Magia dynamiczna:

- magia mentalna
- magia materii

### Zaklęcia statyczne

* **Blacktech**: zaklęcie zakorzenione w samych genach Hektora od samego poczęcia. Uwalnia się tylko w przypadku krytycznego zagrożenia, z którym nie poradziłaby sobie nawet bestia. Wywołuje potężny impuls mentalny nawiązujący łączność z Lewiatanem i tworzący kanał energetyczny między nią a Hektorem.

## Zasoby i otoczenie

### Powiązane frakcje

* link_do_frakcji

### Kogo zna

* **Policja**
* **Nadinspektor policji**
* **Adwokaci i sędziowie**
* **Grube ryby świata przestępczego**
* **Elita Kopalina**
* **Prywatni detektywi**
* **Prokurator Generalny (Sowiński)**

### Co ma do dyspozycji:

* **Grupa śledcza**
* **Więzy krwi**
* **Agumented humans**
* **Demon prawny**
* **Pokój przesłuchań**
* **Rezydencja**
* **Płaszczki**

### Surowce

Osoba silnie wykorzystująca swoją pozycję do zlecania zadań: 2

# Opis

## Motto

"Tekst"

rodzina > prawo > on

# Historia
---
layout: inwazja-karta-postaci
categories: profile
factions: "Niezrzeszeni"
type: "NPC"
title: "Zygmunt Flak"
---
# {{ page.title }}

## Postać

### Motywacje (do czego dąży)

* **truthseeker**: dopóki wszystko nie pasuje do siebie, nie ma spokoju

### Zachowania (jaka jest)

* **dominator**: *I* have the power
* **arogancja**: jestem lepszy niż inni

### Specjalizacje

* **badanie zwłok** (patolog) 
* **noże** (myśliwy i patolog)

### Umiejętności

* **patolog sądowy**
* **myśliwy**
* **aktorstwo, maski**
* **iluzjonista**
* **blogger**

### Cechy

|  Social Aggressive |  Social Devious |  Social Friendly |  Knowledge |  Craft |  Supernatural |  Fortitude |  Nimbleness |
|:==================:|:===============:|:================:|:==========:|:======:|:=============:|:==========:|:===========:|
|        0           |        +1       |        -1        |      0     |   +1   |      -1       |      0     |      0      |

### Specjalne
* **brak**

## Magia

* **brak**

## Zasoby i otoczenie

### Powiązane frakcje

* nazwa_frakcji_jak_ma_to_z_linkiem

### Kogo zna

* okoliczny prokurator
* okoliczna policja
* sklepikarz Adam Giczołek u którego kupuję "te" rzeczy
* mały krąg psychofanów 'roastu'

### Co ma do dyspozycji:

* skalpel
* narzędzia patologiczne + laboratorium
* bogaty

### Surowce

* **Wartość**: 1/2/3
* **Pochodzenie**: dlaczego_1_2_lub_3_i_jak_zarabia

# Opis

perfekcjonistyczny patolog-racjonalista z Czeludzi

36letni patolog, z umiłowania (krojenie żab koło domu), w głowie samotnik, ale udaje normalnego człowieka. Ma żonę (i syna, ale syn jest żony a nie jego) i prowadzi z nią fałszywy tryb życia, tak jak z resztą świata. Co weekend wyjeżdża, podobno na ryby a naprawdę do lasu - jest myśliwym z zamiłowania. Jest perfekcjonistą wierzącym w "wyższy sens" - wszystko musi być na swoim miejscu, wszystko musi pasować.
Pragnie mieć władzę nad życiem i śmiercią - to go właśnie podnieca i bawi.
Jest samotnikiem - rozumie ludzi, ale ich nie czuje. "To tylko ludzie, nie umieją wejść na mój poziom". Chce wytłumaczyć jak w coś wierzą, pokazać im, że żyją w iluzji i ich żałosne marzenia to tylko dym i lustra.

### Koncept

Wysokopoziomowy koncept postaci, 1-2 zdania.

### Motto

"Tekst"

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|160908|patolog-sceptyk dążący do prawdy za wszelką cenę; pomógł Andromedzie dojść do prawdy o Emilii i z trudem uratował się przed jej mocami.|[Zabójczy spadek](/rpg/inwazja/opowiesci/konspekty/160908-zabojczy-spadek.html)|10/01/14|10/01/17|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|[Łukasz Czerwiącek](/rpg/inwazja/opowiesci/karty-postaci/9999-lukasz-czerwiacek.html)|1|[160908](/rpg/inwazja/opowiesci/konspekty/160908-zabojczy-spadek.html)|
|[Tadeusz Bogatek](/rpg/inwazja/opowiesci/karty-postaci/9999-tadeusz-bogatek.html)|1|[160908](/rpg/inwazja/opowiesci/konspekty/160908-zabojczy-spadek.html)|
|[Stefan Porieńko](/rpg/inwazja/opowiesci/karty-postaci/9999-stefan-porienko.html)|1|[160908](/rpg/inwazja/opowiesci/konspekty/160908-zabojczy-spadek.html)|
|[Rudolf Mikołaj](/rpg/inwazja/opowiesci/karty-postaci/9999-rudolf-mikolaj.html)|1|[160908](/rpg/inwazja/opowiesci/konspekty/160908-zabojczy-spadek.html)|
|[Kasia Nowak](/rpg/inwazja/opowiesci/karty-postaci/1709-kasia-nowak.html)|1|[160908](/rpg/inwazja/opowiesci/konspekty/160908-zabojczy-spadek.html)|
|[Geraldina Kurzymaś](/rpg/inwazja/opowiesci/karty-postaci/9999-geraldina-kurzymas.html)|1|[160908](/rpg/inwazja/opowiesci/konspekty/160908-zabojczy-spadek.html)|
|[Emilia Bogatek](/rpg/inwazja/opowiesci/karty-postaci/9999-emilia-bogatek.html)|1|[160908](/rpg/inwazja/opowiesci/konspekty/160908-zabojczy-spadek.html)|
|[Damazy Czekan](/rpg/inwazja/opowiesci/karty-postaci/9999-damazy-czekan.html)|1|[160908](/rpg/inwazja/opowiesci/konspekty/160908-zabojczy-spadek.html)|
|[Damazy Bogatek](/rpg/inwazja/opowiesci/karty-postaci/9999-damazy-bogatek.html)|1|[160908](/rpg/inwazja/opowiesci/konspekty/160908-zabojczy-spadek.html)|
|[Czesław Statyw](/rpg/inwazja/opowiesci/karty-postaci/9999-czeslaw-statyw.html)|1|[160908](/rpg/inwazja/opowiesci/konspekty/160908-zabojczy-spadek.html)|

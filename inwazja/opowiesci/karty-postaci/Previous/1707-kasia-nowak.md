---
layout: inwazja-karta-postaci
categories: profile
factions: "Niezrzeszeni"
type: "PC"
title: "Kasia Nowak"
---
# {{ page.title }}

## Postać

### Motywacje (do czego dąży)

* **przetrwać**: dziwadło w świecie magów i ludzi, zrobi wszystko co w jej mocy, aby przetrwać, zwłaszcza w kontakcie z magią
* **poszukiwaczka inspiracji**: to, co ją spotyka, daje jej inspirację do tworzenia coraz to kolejnych obrazów. Im ciekawsze i bardziej pobudzające, tym lepsze później powstają obrazy
* **ochronić rodzinę i przyjaciół**: od czasu, gdy magia zaczęła ją prześladować, raczej nie utrzymuje kontaktów z rodziną, aby ich chronić. Nie zgadza się, aby ktokolwiek ponosił konsekwencje jej dziwności
* **jestem jedyną, która może coś z tym zrobić**: Kasia do pewnego stopnia czuje się odpowiedzialna za rozwiązanie problemów z magią - w końcu często naprawdę jest jedyną osobą w okolicy mogącą działać

### Specjalizacje

* **racjonalizacja**: jej kłamstwa są bardzo wiarygodne, potrafi sprawić, by druga strona uznała, że jej historia ma sens i sama sobie dopowiedziała szczegóły
* **przekazanie uczuć sztuką**: Ci, którzy patrzą na jej obrazy czują emocje, które w nich zawarła. Czasem ściąga jej to na głowę magów...
* **zacieranie śladów**: plotką, kłamstwem, niedomówieniem... byle to, co chce ukryć, nie wyszło na jaw
* **kontrolowana ofiara zaklęć mentalnych**: kiedy mag każe jej odejść i zapomnieć, Kasia dokładnie wie, jak się zachować, aby wierzył, że się udało... ale raczej nie posłucha

### Umiejętności

* **aktorka**: często musi udawać, czy to asystentkę Andromedy czy to zwykłego człowieka przed magami
* **wiedza o magii**: Kasia dostatecznie dużo stykała się z magią, aby wiedzieć o niej naprawdę dużo
* **okultyzm**: zawsze interesował ją ten temat, szczególnie po zetknięciu z magią
* **mistrzyni kłamstw**: potrafi sprawić, że ludzie (i magowie) uwierzą w to, co im mówi
* **barmanka**: zanim zajęła się sztuką, jakiś czas spędziła jako barmanka - czasem wciąż tak dorabia
* **kierowca**: Kasia pojedzie wszystkim, co wpadnie jej w ręce, choć preferuje samochody
* **artysta malarz**: w sztuce znajduje ucieczkę i możliwość wyrażenia siebie. Próbowała różnych rzeczy, ale spełnia się przede wszystkim w malarstwie.

### Siły

* ostrożna

### Słabości

* ciekawska

### Specjalne

* **odporna na magię mentalną**: zaklęcia mentalne (zwłaszcza wymazanie pamięci) działają, ale krótko
* **magia zawsze ją znajdzie**
* **magia luster**
* **nigdy nie zapomina**

## Zasoby i otoczenie

### Powiązane frakcje

* nazwa_frakcji_jak_ma_to_z_linkiem

### Kogo zna

* **środowisko artystyczne**: opis
* **okultyści**: opis
* **studenci**: opis

### Co ma do dyspozycji:

* **zarobki jako Andromeda**: opis
* **pojazd**: opis
* **skrawki srebra**

### Surowce

* **Wartość**: 1/2/3
* **Pochodzenie**: dlaczego_1_2_lub_3_i_jak_zarabia

# Opis

krótki opis postaci, rzeczy, jakie warto pamiętać.

### Koncept

Wysokopoziomowy koncept postaci, 1-2 zdania.

### Motto

"Tekst"

# Historia:

|Data|Dokonanie|Misja|Kampania|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|

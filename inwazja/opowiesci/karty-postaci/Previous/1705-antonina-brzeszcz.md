---
layout: inwazja-karta-postaci
categories: profile
factions: "Nieznany"
type: "NPC"
title: "Antonina Brzeszcz"
---
# {{ page.title }}

## Postać

### Motywacje (do czego dąży)

* **psycholog z powołania**: chce pomóc dzieciom poradzić sobie z życiem
* **wypalenie zawodowe**: poczucie bezsilności

### Zachowania (jaka jest)

* **idealistka/altruistka**: pomoże nawet własnym kosztem

### Specjalizacje

* **wyciąganie prawdy**: opis
* **czynność_na_poziomie_specjalizacji**: opis
* **czynność_na_poziomie_specjalizacji**: opis
* **czynność_na_poziomie_specjalizacji**: opis

### Umiejętności

* **trendy wśród młodzieży**: opis
* **psycholog szkolny**: opis
* **prawo karne**: opis

### Cechy

|  Social Aggressive |  Social Devious |  Social Friendly |  Knowledge |  Craft |  Supernatural |  Fortitude |  Nimbleness |
|:==================:|:===============:|:================:|:==========:|:======:|:=============:|:==========:|:===========:|
|        0           |        +1       |        +1        |     +1     |   -1   |       0       |     -1     |     -1      |

### Specjalne
* **brak**

## Magia

* **brak**

## Zasoby i otoczenie

### Powiązane frakcje

* nazwa_frakcji_jak_ma_to_z_linkiem

### Kogo zna

* przedstawiciele prawa
* sieć zawodowa psychologów
* młodzież w Okólniczu
* nauczyciele i rodzice z Okólnicza

### Co ma do dyspozycji:

* **sprzęt_do_jakiego_ma_dostęp_i_postać_charakteryzuje**: opis
* **sprzęt_do_jakiego_ma_dostęp_i_postać_charakteryzuje**: opis

### Surowce

* **Wartość**: 2
* **Pochodzenie**: dlaczego_1_2_lub_3_i_jak_zarabia

# Opis
Zwana Sarą
Psycholog szkolny z Okólnicza

Psycholog szkolny z powołania.
Niestety, przeciążenie pracą i trudna dyrekcja doprowadziły ją na skraj wypalenia zawodowego.
W swojej karierze zetknęła się z różnymi przypadkami i sytuacjami, również kryminalnymi lub ocierającymi się o kryminał. Stąd pewna znajomość przepisów prawa

### Koncept

Wysokopoziomowy koncept postaci, 1-2 zdania.

### Motto

"Tekst"

# Historia:



## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|160809|naciskała gdzie miała naciskać, zdobywała informacje i łagodziła konflikty.|[Awokado Dla Wampira](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|10/05/07|10/05/09|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|Leopold Teściak|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|
|Karolina Maus|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|
|Karina von Blutwurst|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|
|Jolanta Lipińska|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|
|Izabela Bąk|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|
|Irena Paniszok|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|
|Filip Czumko|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|
|Elżbieta Paniszok|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|
|Damian Paniszok|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|
|Aleksander Tomaszewski|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|
|Adolphus von Blutwurst|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|

---
layout: inwazja-karta-postaci
categories: profile
factions: "Srebrna Świeca"
type: "NPC"
title: "Kornel Wadera"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **impuls_co_chce_osiągnąć**: cytat_lub_opis_jak_to_rozumieć

### Zachowania (jaka jest)

* **impuls_jak_to_realizuje**: cytat_lub_opis_jak_to_rozumieć

### Specjalizacje

* **guru sekty** (przywódca)
* **łowca magów** (terminus)
* **wspomaganie** (biomancja)
* **magia bojowa**

### Umiejętności

* **przywódca**
* **potwory magiczne**
* **terminus**



### Cechy:

|  Social Aggressive |  Social Devious |  Social Friendly |  Knowledge |  Craft |  Supernatural |  Fortitude |  Nimbleness |
|:==================:|:===============:|:================:|:==========:|:======:|:=============:|:==========:|:===========:|
|       +1           |        +1       |        -1        |     -1     |   -1   |      -1       |     +1     |     +1      |


### Specjalne

**brak**

## Magia
### Szkoły magiczne

* **biomancja**
* **magia mentalna**
### Zaklęcia statyczne

* **nazwa_zaklęcia**: opis_zaklęcia_lub_link
* **nazwa_zaklęcia**: opis_zaklęcia_lub_link

## Zasoby i otoczenie

### Powiązane frakcje

* nazwa_frakcji_jak_ma_to_z_linkiem

### Kogo zna

* **grupy_i_frakcje_które_zna_i_które_ją_znają**: opis
* **grupy_i_frakcje_które_zna_i_które_ją_znają**: opis

### Co ma do dyspozycji:

* **sprzęt_do_jakiego_ma_dostęp_i_postać_charakteryzuje**: opis
* **sprzęt_do_jakiego_ma_dostęp_i_postać_charakteryzuje**: opis

### Surowce

* **Wartość**: 1/2/3
* **Pochodzenie**: dlaczego_1_2_lub_3_i_jak_zarabia

# Opis

Dawny przyjaciel Izabeli Łaniewskiej, ekstremista walczący o Czystą Świecę. Jeszcze bardziej skrajny niż sama Iza. 35 lat.
"So long sentiment"

## Motto

"Tekst"

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|160911|zaufany Izy, ekstremista i potencjalny guru "Tańca liści" - sekty walki wręcz (* magów i ludzi) mającej czyścić Świecę z nieprawości. Przyjaciel Izy.|[Reedukacja Infensy](/rpg/inwazja/opowiesci/konspekty/160911-reedukacja-infensy.html)|10/07/15|10/07/17|[Taniec Liści](/rpg/inwazja/opowiesci/konspekty/kampania-taniec-lisci.html)|
|160915|główny koordynator i guru Tańca Liści pragnący poszerzyć wpływy organizacji by móc zacząć Sanację Świecy|[Rekrutacja mimo woli](/rpg/inwazja/opowiesci/konspekty/160915-rekrutacja-mimo-woli.html)|10/05/26|10/05/29|[Taniec Liści](/rpg/inwazja/opowiesci/konspekty/kampania-taniec-lisci.html)|
|160922|kochający się w Izie radykalny terminus sponsorujący akcję zdobycia informacji co się z Izą stało... złamany i w furii gdy się dowiedział.|[Czarnoskalski konwent RPG](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|10/05/14|10/05/19|[Taniec Liści](/rpg/inwazja/opowiesci/konspekty/kampania-taniec-lisci.html)|
|170407|śmiertelnie niebezpieczny i uprzejmy terminus, któremu eksperymenty z narkotykami bojowymi wyrwały się spod kontroli. Dokonał egzekucji ludzkiego zdrajcy. Współpracował z Zespołem udając zdziwionego.|[Przebudzenie viciniusa](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|10/03/08|10/03/09|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|[Dorota Gacek](/rpg/inwazja/opowiesci/karty-postaci/1709-dorota-gacek.html)|3|[160915](/rpg/inwazja/opowiesci/konspekty/160915-rekrutacja-mimo-woli.html), [160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html), [170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|
|[Infensa Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-infensa-diakon.html)|2|[160911](/rpg/inwazja/opowiesci/konspekty/160911-reedukacja-infensy.html), [160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Włodzimierz Tulewicz](/rpg/inwazja/opowiesci/karty-postaci/1705-wlodzimierz-tulewicz.html)|1|[160915](/rpg/inwazja/opowiesci/konspekty/160915-rekrutacja-mimo-woli.html)|
|[Urszula Murczyk](/rpg/inwazja/opowiesci/karty-postaci/1705-urszula-murczyk.html)|1|[170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|
|[Szymon Skubny](/rpg/inwazja/opowiesci/karty-postaci/9999-szymon-skubny.html)|1|[160915](/rpg/inwazja/opowiesci/konspekty/160915-rekrutacja-mimo-woli.html)|
|[Szczepan Paczoł](/rpg/inwazja/opowiesci/karty-postaci/1705-szczepan-paczol.html)|1|[170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|
|[Supernowa Diakon](/rpg/inwazja/opowiesci/karty-postaci/1705-supernowa-diakon.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-siluria-diakon.html)|1|[160911](/rpg/inwazja/opowiesci/konspekty/160911-reedukacja-infensy.html)|
|[Roman Brunowicz](/rpg/inwazja/opowiesci/karty-postaci/9999-roman-brunowicz.html)|1|[170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|
|[Rafał Kniaź](/rpg/inwazja/opowiesci/karty-postaci/9999-rafal-kniaz.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Rafael Diakon](/rpg/inwazja/opowiesci/karty-postaci/1705-rafael-diakon.html)|1|[160911](/rpg/inwazja/opowiesci/konspekty/160911-reedukacja-infensy.html)|
|[Paulina Widoczek](/rpg/inwazja/opowiesci/karty-postaci/9999-paulina-widoczek.html)|1|[170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|
|[Patrycja Krowiowska](/rpg/inwazja/opowiesci/karty-postaci/1705-patrycja-krowiowska.html)|1|[170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|
|[Oddział Zeta](/rpg/inwazja/opowiesci/karty-postaci/9999-oddzial-zeta.html)|1|[170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|
|[Mikado Diakon](/rpg/inwazja/opowiesci/karty-postaci/1705-mikado-diakon.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Mateusz Ackmann](/rpg/inwazja/opowiesci/karty-postaci/1707-mateusz-ackmann.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Marcin Szybisty](/rpg/inwazja/opowiesci/karty-postaci/1705-marcin-szybisty.html)|1|[160915](/rpg/inwazja/opowiesci/konspekty/160915-rekrutacja-mimo-woli.html)|
|[Magda Szybisty](/rpg/inwazja/opowiesci/karty-postaci/9999-magda-szybisty.html)|1|[160915](/rpg/inwazja/opowiesci/konspekty/160915-rekrutacja-mimo-woli.html)|
|[Leszek Żółty](/rpg/inwazja/opowiesci/karty-postaci/9999-leszek-zolty.html)|1|[160915](/rpg/inwazja/opowiesci/konspekty/160915-rekrutacja-mimo-woli.html)|
|[Leopold Teściak](/rpg/inwazja/opowiesci/karty-postaci/9999-leopold-tesciak.html)|1|[160915](/rpg/inwazja/opowiesci/konspekty/160915-rekrutacja-mimo-woli.html)|
|[Krystian Miałcz](/rpg/inwazja/opowiesci/karty-postaci/9999-krystian-mialcz.html)|1|[160911](/rpg/inwazja/opowiesci/konspekty/160911-reedukacja-infensy.html)|
|[Konrad Matczak](/rpg/inwazja/opowiesci/karty-postaci/9999-konrad-matczak.html)|1|[160915](/rpg/inwazja/opowiesci/konspekty/160915-rekrutacja-mimo-woli.html)|
|[Jolanta Iwan](/rpg/inwazja/opowiesci/karty-postaci/9999-jolanta-iwan.html)|1|[160915](/rpg/inwazja/opowiesci/konspekty/160915-rekrutacja-mimo-woli.html)|
|[Jessica Ruczaj](/rpg/inwazja/opowiesci/karty-postaci/9999-jessica-ruczaj.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Jacek Molenda](/rpg/inwazja/opowiesci/karty-postaci/9999-jacek-molenda.html)|1|[160915](/rpg/inwazja/opowiesci/konspekty/160915-rekrutacja-mimo-woli.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1707-hektor-blakenbauer.html)|1|[170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|
|[Franciszek Knur](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-knur.html)|1|[170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|
|[Esme Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-esme-myszeczka.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|1|[170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|
|[Dominik Parszywiak](/rpg/inwazja/opowiesci/karty-postaci/9999-dominik-parszywiak.html)|1|[170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|
|[Dionizy Kret](/rpg/inwazja/opowiesci/karty-postaci/1707-dionizy-kret.html)|1|[170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|
|[Cyprian Koziej](/rpg/inwazja/opowiesci/karty-postaci/9999-cyprian-koziej.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Bolesław Derwisz](/rpg/inwazja/opowiesci/karty-postaci/9999-boleslaw-derwisz.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Artur Pawiak](/rpg/inwazja/opowiesci/karty-postaci/9999-artur-pawiak.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Artur Bryś](/rpg/inwazja/opowiesci/karty-postaci/1709-artur-brys.html)|1|[170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|
|[Andrzej Marciniak](/rpg/inwazja/opowiesci/karty-postaci/9999-andrzej-marciniak.html)|1|[160915](/rpg/inwazja/opowiesci/konspekty/160915-rekrutacja-mimo-woli.html)|
|[Alina Bednarz](/rpg/inwazja/opowiesci/karty-postaci/1709-alina-bednarz.html)|1|[170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|

---
layout: inwazja-karta-postaci
categories: profile
factions: "KADEM"
type: "NPC"
owner: "public"
title: "Karolina Kupiec"
---

# {{ page.title }}

## Postać

### Motywacje (do czego dąży)

* **Indywidualne**:
    * _Aspekty_: postęp, pokój
    * _Szczególnie_: promuj wygaszanie konfliktów, NIE asertywnie walcz o swoje, stwórz jak najlepsze środki psychoaktywne, NIE zadowalaj się stanem nauki, ciągle eksperymentuj z substratami i stanami świadomości, NIE słuchaj ostrożności narkotykowej, dąż do radosnej stabilności, NIE bądź skonfliktowana wewnętrzne
    * _Opis_: Bardziej naukowiec niż terminus w duszy, Karolina dąży do tego, by stworzyć jak najlepsze środki i jak najbardziej posunąć naukę w tej dziedzinie do przodu. Jednocześnie jej pacyfistyczna natura każe jej dążyć do pokoju w swojej okolicy - pokoju rozumianego jako absolutny brak konfliktu wewnętrznego. Medytacja, zen... tego typu rzeczy.
* **Społeczne**:
    * _Aspekty_: radość, autonomia, altruizm
    * _Szczególnie_: promuj samostanowienie o sobie, NIE akceptuj żadnej formy zniewolenia, promuj radość i szczęście, NIE zostawiaj cierpiących bez pomocy, promuj radosny altruizm, NIE dbaj o własność prywatną, walcz o różnorodność utopii różnych osób, NIE masz prawa narzucać innym poglądów
    * _Opis_: Karolina zupełnie nie ceni sobie własności prywatnej i zwalcza wszelkie formy przedkładania własności prywatnej nad radość i szczęście. Za to próbuje zawsze promować altruizm, pomaganie innym w potrzebie i czystą radość. Jak i prawo do samostanowienia o sobie.
* **Serce**:
    * _Aspekty_: przyjemność, radość, pacyfista
    * _Szczególnie_: dąż do wewnętrznego pokoju i radości, NIE chowaj urazy i dawaj milion szans, usuń wszelki ból z egzystencji, NIE cierpienie i negatywne uczucia mają prawo istnieć, dziel się wszystkim ze światem, NIE chroń się przed potencjalnym złem - i tak przyjdzie
    * _Opis_: Karolina nie ma psychotypu kompatybilnego z terminusem. Jest pełną radości i chęci do życia czarodziejką, która bardzo ceni sobie przyjemność i ekstazę, ogólnie rozumianą radość i unika wszelkich form bólu i cierpienia. Pacyfistka z natury, niechętna do krzywdzenia i walki, preferuje bardziej... subtelne sposoby unieszkodliwiania.
* **Działanie**:
    * _Aspekty_: współdziałanie, narkotyki
    * _Szczególnie_: ryzykuj i graj optymistycznie, NIE mierz sił na zamiary, obezwładniaj przyjemnością, NIE krzywdź i nie kontroluj bólem, dąż do współdziałania i win-win, NIE skupiaj się na swojej wygranej, rozpowszechniaj środki psychoaktywne, NIE ekonomiczny bilans i ostrożność, zgłaszaj się na ochotnika, NIE unikaj angażowania się
    * _Opis_: Karolina działa osobiście, rzadko przez agentów. Gdy się pojawia, zawsze próbuje godzić i jednać wszystkie strony i zarażać je swoim niepohamowanym optymizmem i radością. Czasami jej się dostaje za to, ale cóż, nie przejmuje się. Jej znakiem charakterystycznym są różne środki psychoaktywne które stosuje na lewo i prawo.

### Umiejętności

* **Terminus**:
    * _Aspekty_: terminus, rewolwerowiec
    * _Manewry_: pierwsza pomoc, skłanianie do poddania pistoletami i przewagą sytuacyjną, kupowanie czasu przez ogień zaporowy, obezwładnianie przez pistolety i chemikalia, unieszkodliwianie przez pułapki chemiczne
    * _Opis_: Karolina nie jest najlepszym terminusem jakiego ma KADEM, ale potrafi unieszkodliwić przeciwnika. Nie jest tak dobra w zwarciu jak mając swoje pistolety. Nie przez przypadek zwykle pozycjonuje się też jako medyk bojowy. Lepiej sobie radzi z zastawianiem pułapek chemicznych niż z bezpośrednim strzelaniem się z przeciwnikami.
* **Biochemik**:
    * _Aspekty_: biochemik, narkotyki, herbalista, naukowiec
    * _Manewry_: obezwładnianie ukojenie i ekstaza narkotykami, kalibracja narkotyków przez badanie próbek celu, głębokie badanie narkotyków, uodparnianie na narkotyki i trucizny, hodowla magicznych roślin, ekstrakcja substratów z roślin i viciniusów
    * _Opis_: Badaczka środków psychoaktywnych prowadzących do przyjemności i radości. Specjalistka od toksyn działających na ludzki i magoviciniuski organizm. Przy okazji, uczennica Weroniki Seton wraz z Infernią zajmująca się ogrodami Weroniki Seton. Główny dostawca dziwnych substratów KADEMowi.
* **Dyplomata**:
    * _Aspekty_: dyplomata, bard
    * _Manewry_: podniesienie morale przez optymizm i przykład, rozładowanie konfliktu przez współpracę i pozytywne podejście, zjednywanie sojuszników przez podarunki i współpracę
    * _Opis_: Promyk radości KADEMu, Karolina, potrafi wejść w najbardziej niekorzystne miejsce i wyjść z grupą nowych przyjaciół. Jakkolwiek jest kiepska w przekonywaniu do swoich racji, świetnie deeskaluje wszelkie konflikty i buduje startową pozycję zaufania. No i rozdaje cukierki...

### Silne i słabe strony:

* **Kotwica pryzmatyczna**:
    * _Aspekty_: pryzmat, kotwica
    * _Manewry_: przekształcenie Pryzmatu na hedonistyczno-pacyfisty, podnoszenie morale i ekstazy, jest w "domu" na Fazie Daemonica, zawsze łatwo wykrywalna przez Pryzmat / magię
    * _Opis_: Karolina jest wirem pryzmatycznym. Jej obecność bardzo mocno wpływa na Pryzmat, generując uczucia zgodne z jej Sercem. Sprawia to, że Karolina praktycznie nie potrafi się ukryć przed magami, ale za to zwykle prędzej czy później jest na "swoim terenie".

### Szkoły magiczne

* **Technomancja**:
    * _Aspekty_: broń, laboratoria
    * _Manewry_: unieszkodliwienie broni przeciwnika, zastawienie nieletalnej pułapki, wzmocnienie badania psychoaktywów
    * _Opis_: podczas krótkiego pobytu na Instytucie Technomancji Karolina nauczyła się konstruować własną broń by móc być terminusem niebojowym
* **Biomancja**: 
    * _Aspekty_: alchemia, hedonistyczne środki psychoaktywne, trucizny i narkotyki, magia lecznicza
    * _Manewry_: 
    * _Opis_: druga połowa wpływania na organizm maga przy użyciu swoich środków psychoaktywnych
* **Magia mentalna**:
    * _Aspekty_: ukojenie, obezwładnianie, deuzależnienie
    * _Manewry_: 
    * _Opis_: 

## Zasoby i otoczenie
### Powiązane frakcje

{{ page.factions }}

### Zasoby

* **Czar Pocałunek Arazille**:
    * _Aspekty_: unieszkodliwienie, arazille, rozkosz
    * _Manewry_: całkowite obezwładnienie celu
    * _Opis_: Zaklęcie wprowadzające cel w absolutną utopię, praktycznie w świat Arazille. Ofiara jest praktycznie w transie; bardzo nie chce tego świata opuszczać. Karolina stosuje to zaklęcie jako swoje ostateczne zaklęcie bojowe; zdaje sobie sprawę z uzależniających własności tego czaru.
* **Fani, młodzi magowie, dilerzy 'nie-twardzi'**:
    * _Aspekty_: powierzchowne kontakty, szeroko znana, niezbyt szanowne kontakty
    * _Manewry_: zdobycie plotek i 'nastroju tłumu' przez szeroką sieć informacyjną
    * _Opis_: Z uwagi na podejście Karoliny, ma ona grupę fanów wśród osób, które ją popierają... acz boją się powiedzieć to publicznie. Karolina pomoże też nawet randomowej biednej duszy normalnie handlującej narkotykami. Uważana za niegroźną i sympatyczną.
* **Infernia Diakon**:
    * _Aspekty_: 
    * _Manewry_: 
    * _Opis_: Ze wszystkich magów KADEMu Karolina i Infernia mają straszny sojusz - Infernia dostarcza składniki i testuje eksperymenty Karoliny. Stanowią świetny dwuosobowy zespół polegający na sobie bardziej niż komukolwiek się to wydaje.
* **Hedonistyczne środki psychoaktywne**:
    * _Aspekty_: 
    * _Manewry_: 
    * _Opis_: Miłość i dzieło życia Karoliny... do tego kolekcjonuje różne warianty dla różnego biotypu magów i viciniusów.
* **Sprzęt bojowy KADEMu**:
    * _Aspekty_: 
    * _Manewry_: 
    * _Opis_: Karolina nie jest najlepszym terminusem, ale KADEM próbuje kompensować używając dobrego sprzętu. A jej pistolet alchemiczny pozwala jej na 

# Opis

There are many different Magi with various motivations. The same applies to termini. Some termini fight for peace. Some other fight for their side. Caroline? She fights for good feelings and overall pleasure. She calls herself a hedonistic terminus fighting for overall good mood.

She used to be a Millennium mage – she thought that Millennium would be the Guild most suitable to her personality. However, she wasn't able to cope with kraloth and with Mafia-like structure of the Guild. Although she still values and respects Magi of Millennium, she is simply disaligned with the goals of the Guild.

Caroline has managed to enter KADEM, which surprised a lot of Magi – she simply is neither the most powerful mage around nor the most skilled in any particular area. Yet, Veronica Seton took her in, personally, and made Caroline Veronica's assistant. 

One of the stronger assets of Caroline is the fact that her personality allows her to thrive in the prismatic world of Daemonica; this means she is not at a disadvantage in dual reality of KADEM and no matter what happens - she is an asset. Maybe not the strongest asset, but an asset nevertheless.

A friendly terminus who tries to keep everyone's morale high and who helpfully tries to solve everyone's problems, she is generally liked in her new guild.

Caroline specializes in 'drugs'. Or rather, 'psychoactive vectors of hedonism'. She is a valiant researcher and crafter of different things which give pleasure and sooth the pain of existence. Quite often, she works with Warmaster; he is the dark emo brooding one and she is the flowery lollipop giving away her resources to make everyone around happy.

She may not be the best terminus around, but she is decent. Can do her stuff. She identifies herself not as a terminus but as a "happiness vector".

As a terminus, she specializes in disabling and non-lethal elimination of her enemies. If needed, she will simply shoot.

### Koncept

Pollyanna + Sharon + Elwira + secondary terminus + hipiska

### Mapa kreacji

brak

### Motto

"Nie martw się, weź cukierka. Na pewno poczujesz się lepiej :-)"

# Historia:
## Plany

|Misja|Plan|Kampania|
|[Cienie procesu Izy](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|bardzo zdeterminowana, by ALBO wskrzesić ALBO pochować Hralglanatha - ale na pewno nie trzymać go w tym stanie.|Adaptacja kralotyczna|

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|180311|śpiewała martwemu kralothowi, bo Karina ją w to wrobiła. Organizuje kampanię jak wskrzesić kralotha.|[Cienie procesu Izy](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|10/12/03|10/12/04|[Adaptacja kralotyczna](/rpg/inwazja/opowiesci/konspekty/kampania-adaptacja-kralotyczna.html)|
|180310|tym razem w roli dyplomaty i łagodzicielki konfliktów. Również frontman wobec Marka Kromlana. W ostatniej chwili ewakuowała kralotha na KADEM.|[Kraloth w piwnicy](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|10/11/28|10/12/01|[Adaptacja kralotyczna](/rpg/inwazja/opowiesci/konspekty/kampania-adaptacja-kralotyczna.html)|
|170104|wesoła terminuska dostarczająca narko... er, 'hedonistyczne środki psychoaktywne' które nie uzależniają. Nie handluje, rozdaje.|[Spalone generatory pryzmatyczne](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html)|10/08/08|10/08/09|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170707|dla której essir jest sposobem by wnieść ekstazę i przyjemność do Zaćmionego Serca. Główna alchemiczka zespołu. Też fortyfikuje bazę ;-).|[Biznes pośród niesnasek](/rpg/inwazja/opowiesci/konspekty/170707-biznes-posrod-niesnasek.html)|10/03/06|10/03/08|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170115|wykryła, że sok jabłkowy (z domieszkami) jest dokładnie tym i niczym więcej. Zresztą, jest to całkiem smaczny sok.|[Klub Dare Shiver](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|10/02/22|10/02/25|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170111|zafascynowana narkotykiem Urszuli, przygotowała hedonistyczny neuronull dla Pawła (by dać Franciszkowi) po czym przygotowała na to samo antidotum dla Silurii. Bez jednej refleksji.|[EIS na kozetce](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|10/02/18|10/02/21|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170108|pokazała różnicę między NIĄ a DILERAMI. Terminuska o dobrym sercu i dużych umiejętnościach badawczych narkotyków.|[Samotna w świecie magow](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html)|10/02/12|10/02/16|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161218|terminuska KADEMu na Instytucie Transorganiki. Pomagała Warmasterowi w tworzeniu nietypowego glashunda. Poraniona Pryzmatem.|[Zazdrość Warmastera](/rpg/inwazja/opowiesci/konspekty/161218-zazdrosc-warmastera.html)|10/02/01|10/02/03|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1801-siluria-diakon.html)|8|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html), [180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html), [170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html), [170707](/rpg/inwazja/opowiesci/konspekty/170707-biznes-posrod-niesnasek.html), [170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html), [170108](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html), [161218](/rpg/inwazja/opowiesci/konspekty/161218-zazdrosc-warmastera.html)|
|[Netheria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-netheria-diakon.html)|4|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html), [180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html), [170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Bogumił Rojowiec](/rpg/inwazja/opowiesci/karty-postaci/9999-bogumil-rojowiec.html)|4|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html), [170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html), [161218](/rpg/inwazja/opowiesci/konspekty/161218-zazdrosc-warmastera.html)|
|[Warmaster](/rpg/inwazja/opowiesci/karty-postaci/9999-warmaster.html)|3|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html), [170707](/rpg/inwazja/opowiesci/konspekty/170707-biznes-posrod-niesnasek.html), [161218](/rpg/inwazja/opowiesci/konspekty/161218-zazdrosc-warmastera.html)|
|[Quasar](/rpg/inwazja/opowiesci/karty-postaci/9999-quasar.html)|3|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html), [161218](/rpg/inwazja/opowiesci/konspekty/161218-zazdrosc-warmastera.html)|
|[Judyta Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-judyta-maus.html)|3|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html), [170707](/rpg/inwazja/opowiesci/konspekty/170707-biznes-posrod-niesnasek.html), [170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|[Ignat Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1801-ignat-zajcew.html)|3|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html), [170108](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html)|
|[Whisperwind](/rpg/inwazja/opowiesci/karty-postaci/9999-whisperwind.html)|2|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html), [161218](/rpg/inwazja/opowiesci/konspekty/161218-zazdrosc-warmastera.html)|
|[Urszula Murczyk](/rpg/inwazja/opowiesci/karty-postaci/1709-urszula-murczyk.html)|2|[170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html), [170108](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html)|
|[Norbert Sonet](/rpg/inwazja/opowiesci/karty-postaci/9999-norbert-sonet.html)|2|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html), [161218](/rpg/inwazja/opowiesci/konspekty/161218-zazdrosc-warmastera.html)|
|[Marian Łajdak](/rpg/inwazja/opowiesci/karty-postaci/1709-marian-lajdak.html)|2|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html), [170108](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html)|
|[Marek Kromlan](/rpg/inwazja/opowiesci/karty-postaci/1801-marek-kromlan.html)|2|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html), [180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Kornelia Kartel](/rpg/inwazja/opowiesci/karty-postaci/9999-kornelia-kartel.html)|2|[170707](/rpg/inwazja/opowiesci/konspekty/170707-biznes-posrod-niesnasek.html), [170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|[Joachim Kartel](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-kartel.html)|2|[170707](/rpg/inwazja/opowiesci/konspekty/170707-biznes-posrod-niesnasek.html), [170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|[Infernia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-infernia-diakon.html)|2|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Ilona Amant](/rpg/inwazja/opowiesci/karty-postaci/9999-ilona-amant.html)|2|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Franciszek Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-maus.html)|2|[170707](/rpg/inwazja/opowiesci/konspekty/170707-biznes-posrod-niesnasek.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Eis](/rpg/inwazja/opowiesci/karty-postaci/9999-eis.html)|2|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|2|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html), [180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Balrog Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-balrog-bankierz.html)|2|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Wioletta Lemona-Chang](/rpg/inwazja/opowiesci/karty-postaci/9999-wioletta-lemona-chang.html)|1|[170108](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html)|
|[Szczepan Przysiadek](/rpg/inwazja/opowiesci/karty-postaci/9999-szczepan-przysiadek.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Supernowa Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-supernowa-diakon.html)|1|[170707](/rpg/inwazja/opowiesci/konspekty/170707-biznes-posrod-niesnasek.html)|
|[Rukoliusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-rukoliusz-bankierz.html)|1|[170108](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html)|
|[Roman Bruniewicz](/rpg/inwazja/opowiesci/karty-postaci/9999-roman-bruniewicz.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Radosław Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-radoslaw-weiner.html)|1|[161218](/rpg/inwazja/opowiesci/konspekty/161218-zazdrosc-warmastera.html)|
|[Paweł Sępiak](/rpg/inwazja/opowiesci/karty-postaci/1709-pawel-sepiak.html)|1|[170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Paulina Widoczek](/rpg/inwazja/opowiesci/karty-postaci/9999-paulina-widoczek.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Oliwier Bonwant](/rpg/inwazja/opowiesci/karty-postaci/9999-oliwier-bonwant.html)|1|[170108](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html)|
|[Oktawian Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-oktawian-maus.html)|1|[170707](/rpg/inwazja/opowiesci/konspekty/170707-biznes-posrod-niesnasek.html)|
|[Mordecja Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-mordecja-diakon.html)|1|[161218](/rpg/inwazja/opowiesci/konspekty/161218-zazdrosc-warmastera.html)|
|[Miranda Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-miranda-maus.html)|1|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|
|[Mikado Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-mikado-diakon.html)|1|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html)|
|[Melodia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1801-melodia-diakon.html)|1|[170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Marianna Sowińska](/rpg/inwazja/opowiesci/karty-postaci/1709-marianna-sowinska.html)|1|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html)|
|[Maria Przysiadek](/rpg/inwazja/opowiesci/karty-postaci/9999-maria-przysiadek.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Marcelin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-marcelin-blakenbauer.html)|1|[170707](/rpg/inwazja/opowiesci/konspekty/170707-biznes-posrod-niesnasek.html)|
|[Maja Błyszczyk](/rpg/inwazja/opowiesci/karty-postaci/9999-maja-blyszczyk.html)|1|[161218](/rpg/inwazja/opowiesci/konspekty/161218-zazdrosc-warmastera.html)|
|[Lucjan Kopidół](/rpg/inwazja/opowiesci/karty-postaci/9999-lucjan-kopidol.html)|1|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|
|[Krystalia Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-krystalia-diakon.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Karina Paczulis](/rpg/inwazja/opowiesci/karty-postaci/9999-karina-paczulis.html)|1|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|
|[Jolanta Sowińska](/rpg/inwazja/opowiesci/karty-postaci/1709-jolanta-sowinska.html)|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|[Joachim Kopiec](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-kopiec.html)|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|[Jan Wątły](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-watly.html)|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|[Infensa Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-infensa-diakon.html)|1|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|
|[Hralglanath](/rpg/inwazja/opowiesci/karty-postaci/9999-hralglanath.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Henryk Siwiecki](/rpg/inwazja/opowiesci/karty-postaci/1709-henryk-siwiecki.html)|1|[170707](/rpg/inwazja/opowiesci/konspekty/170707-biznes-posrod-niesnasek.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|1|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html)|
|[Grzegorz Nocniarz](/rpg/inwazja/opowiesci/karty-postaci/9999-grzegorz-nocniarz.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Franciszek Knur](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-knur.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Ferdynand Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-ferdynand-maus.html)|1|[170707](/rpg/inwazja/opowiesci/konspekty/170707-biznes-posrod-niesnasek.html)|
|[Estrella Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-estrella-diakon.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Elizawieta Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-elizawieta-zajcew.html)|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|[Elea Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-elea-maus.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Błażej Falka](/rpg/inwazja/opowiesci/karty-postaci/9999-blazej-falka.html)|1|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|
|[Bójka Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-bojka-diakon.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Baltazar Sowiński](/rpg/inwazja/opowiesci/karty-postaci/9999-baltazar-sowinski.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Artur Bryś](/rpg/inwazja/opowiesci/karty-postaci/1709-artur-brys.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Anna Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-myszeczka.html)|1|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html)|
|[Anna Kozak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kozak.html)|1|[170108](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html)|
|[Andżelika Leszczyńska](/rpg/inwazja/opowiesci/karty-postaci/1709-andzelika-leszczynska.html)|1|[161218](/rpg/inwazja/opowiesci/konspekty/161218-zazdrosc-warmastera.html)|
|[Adrian Kropiak](/rpg/inwazja/opowiesci/karty-postaci/9999-adrian-kropiak.html)|1|[161218](/rpg/inwazja/opowiesci/konspekty/161218-zazdrosc-warmastera.html)|
|[Adonis Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1709-adonis-sowinski.html)|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|

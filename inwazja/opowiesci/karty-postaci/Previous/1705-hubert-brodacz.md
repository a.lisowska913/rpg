---
layout: inwazja-karta-postaci
categories: profile
factions: "Niezrzeszeni"
type: "NPC"
title: "Hubert Brodacz"
---
# {{ page.title }}

## Postać

### Motywacje (do czego dąży)

* **impuls_co_chce_osiągnąć**: cytat_lub_opis_jak_to_rozumieć

### Zachowania (jaka jest)

* **Opiekuńczość**: ludzie pod jego ochroną są nietykalni
* **Cwaniak**: na pewno da się to jakoś załatwić
* **Porywczy**: Gadaj, co wiesz!

### Specjalizacje

* **zbieranie dowodów** (śledczy)
* **nakłanianie do współpracy** (manipulant)
* **sztuczna osobowość** (aktorstwo)
* **jak żyć z Hektorem** (śledczy

### Umiejętności

* **śledczy**
* **manipulant**
* **policjant**
* **zastraszanie**
* **aktorstwo**
* **pierwsza pomoc**
* **kierowca**

### Cechy

|  Social Aggressive |  Social Devious |  Social Friendly |  Knowledge |  Craft |  Supernatural |  Fortitude |  Nimbleness |
|:==================:|:===============:|:================:|:==========:|:======:|:=============:|:==========:|:===========:|
|       +1           |        +1       |         0        |      0     |   -1   |      -1       |      0     |      0      |

### Specjalne
* **brak**

## Magia

* **brak**

## Zasoby i otoczenie

### Powiązane frakcje

* nazwa_frakcji_jak_ma_to_z_linkiem

### Kogo zna

* Artur Bryś
* lokalne mendy społeczne
* ludzie z komisariatu

### Co ma do dyspozycji:

* zabawki "pożyczone" z grupy śledczej
* wyposażenie dzielnicowego

### Surowce

* **Wartość**: 1/2/3
* **Pochodzenie**: dlaczego_1_2_lub_3_i_jak_zarabia

# Opis

ex-członek grupy śledczej z Kopalina

Ex-członek grupy śledczej pracującej dla Hektora Blakenbauera. Wyrzucony za cień podejrzenia o manipulację dowodami, czego jednak nie robił. Wylądował na ulicy… znaczy się przeniesiono go do zwykłych patroli osiedlowych. Szybko adaptował się do nowego środowiska. Nawiązał kontakty z lokalnymi szefami i "półświatkiem". Oferuje im ochronę w zamian za pomoc w zgarnianiu oprychów spoza "dzielni". Nie jest może do końca praworządny, ale opiekuje się ludźmi w swoim rewirze.

### Koncept

Wysokopoziomowy koncept postaci, 1-2 zdania.

### Motto

"Tekst"

# Historia:

## Progresja

|Misja|Progresja|Kampania|
|[Wandy wolność i wróżda](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|jest wolny od Skubnego (spłacił dług)|Taniec Liści|



## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|160921|policjant, eks-siły Hektora, który potrafi pobić anarchistę by potem się z nim zaprzyjaźnić; jest bezwzględny. Spłacił dług u Skubnego.|[Wandy wolność i wróżda](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|10/07/22|10/07/24|[Taniec Liści](/rpg/inwazja/opowiesci/konspekty/kampania-taniec-lisci.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|Zdzisław Kamiński|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|Wanda Ketran|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|Tymotheus Blakenbauer|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|Tomasz Kracy|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|Szymon Skubny|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|Stanisław Pormien|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|Milena Marzec|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|Marian Rokita|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|Margaret Blakenbauer|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|Leszek Żółty|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|Jan Szczupak|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|Iliusitius|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|Grzegorz Włóczykij|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|Grażyna Remska|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|Edwin Blakenbauer|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|Diana Larent|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|Czesław Czepiec|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|Anna Góra|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|

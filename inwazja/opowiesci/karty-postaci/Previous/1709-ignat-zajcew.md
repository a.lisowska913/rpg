---
layout: inwazja-karta-postaci
categories: profile
factions: "KADEM"
type: "NPC"
title: "Ignat Zajcew"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **FIL:**:
    * _Aspekty_: 
    * _Opis_: 
* **BÓL: Wróg - Świeca**:
    * _Aspekty_: 
    * _Opis_: "Naprawdę nie lubię tienowatych. Zwłaszcza w krawatach. Winni są wszystkiego."
* **MRZ:**:
    * _Aspekty_: 
    * _Opis_: 
* **MET: Żartowniś**:
    * _Aspekty_: 
    * _Opis_: Zachowanie powagi się nie godzi. Trzeba zrobić dowcip, zwłaszcza komuś ważnemu
* **KLT: Waleczny**:
    * _Aspekty_: impulsywny, rywalizacja, niesubordynacja
    * _Opis_: Jestem najlepszy; przynajmniej, lepszy od ciebie. YOLO! Konsekwencjami zajmiemy się później, teraz czas na akcję. Mogę współpracować, ale nie będę słuchać niczyich poleceń.

### Umiejętności

* **mag Instytutu Zniszczenia**:
    * _Aspekty_: wyleciał z Instytutu Pryzmaturgii, wyleciał z Instytutu Technomancji
    * _Opis_: 
* **broń oblężnicza i oblężenia**:
    * _Aspekty_: siege engine operator, strzelec podczas ruchu
    * _Opis_: 
* **uliczny wojownik**:
    * _Aspekty_: miasto
    * _Opis_: 
* **practical joker**:
    * _Aspekty_: 
    * _Opis_: 
* **detektyw**:
    * _Aspekty_: tracker
    * _Opis_: 
* **hazardzista**:
    * _Aspekty_: wyścigi, prowokator
    * _Opis_: 
* **włamywacz**:
    * _Aspekty_: 
    * _Opis_: 
* **fan wszystkiego śledziopodobnego**:
    * _Aspekty_: 
    * _Opis_: 
* **opowieści**:
    * _Aspekty_: 
    * _Opis_: 	
    
### Silne i słabe strony:

* **gorące serce**:
    * _Aspekty_: 
    * _Opis_: Ignata praktycznie nie da się wystraszyć.
* **kamienny łeb**:
    * _Aspekty_: 
    * _Opis_: Odporny na ogłuszenie / wyłączenie z akcji.
* **rogata dusza**:
    * _Aspekty_: 
    * _Opis_: Odporności na rzeczy typu energia negatywna czy ból.

## Magia

### Szkoły magiczne

* **Magia elementalna**:
    * _Aspekty_: katai, magia Instytutu Zniszczenia, bojowy mag ognia, dewastacja
    * _Opis_: 
* **Magia transportu**:
    * _Aspekty_: magia pościgowa, unieruchamianie
    * _Opis_: 
* **Magia materii**:
    * _Aspekty_: adaptacja rzeczy w szybki pojazd, 
    * _Opis_: 

### Zaklęcia statyczne

* **??**:
    * _Aspekty_: 
    * _Opis_: 

## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Zarobki


### Znam

* **??**:
    * _Aspekty_: 
    * _Opis_: 
* **??**:
    * _Aspekty_: 
    * _Opis_: 
	
### Mam

* **??**:
    * _Aspekty_: 
    * _Opis_: 
* **??**:
    * _Aspekty_: 
    * _Opis_: 


# Opis
Twardogłowy mag bojowy, pochopny i chętny do szybkiego i efektownego działania. Kawalarz i dowcipniś jakich mało, 'practical joker'. Niezły detektyw. Nie lubi Srebrnej Świecy i często pozwala uprzedzeniom zwyciężyć nad faktami. Rycerski, waleczny i chętnie rzucający się w ogień walki mag który łatwo się nudzi. Niezły hazardzista, choć częściej przegrywa niż wygrywa.
Jego specjalnym talentem jest to, że bardzo trudno jest go unieszkodliwić. Archetyp: dowolny dinobot (Grimlock?), łącznie z arogancją i walecznością.

Znalazł swoją rolę na KADEMie po wyleceniu z dwóch instytutów; Mariusz Trzosik zaproponował go do Instytutu Zniszczenia. Jeśli trzeba coś zdobyć (surowiec, komponent, artefakt, cel…) to Ignat wchodzi w temat, robi research, śledzi cel / znajduje go (trackuje), po czym wykorzystuje swoją magię do zrobienia broni oblężniczej i zdobycia / unieruchomienia celu. Potem to przywozi.
## Motto

"Tekst"

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|[Odzyskać Aegis 0003](/rpg/inwazja/opowiesci/konspekty/170519-odzyskac-aegis-0003.html)|obiekt zamesmeryzowania Sandry Maus i Teresy Koliczer dzięki działaniom Silurii i Efektu Skażenia|Powrót Karradraela|


## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|170519|zaciągnięty przez Silurię do pomocy militarnej na wypadek problemów; większość czasu się nudził a potem skończył jako amant dwóch czarodziejek.|[Odzyskać Aegis 0003](/rpg/inwazja/opowiesci/konspekty/170519-odzyskac-aegis-0003.html)|10/08/21|10/08/22|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170125|stanął pomiędzy Infernią i Hektorem i skończył połamany.|[Przeprawa do Świecy Daemonica](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html)|10/08/10|10/08/11|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161207|detektyw kojarzący fakty, powiedział o możliwym wyjściu z Fazy Daemonica przez Powiew Świeżości - czego dowodem Głowica Aegis|[Lizanie ran na KADEMie](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html)|10/08/05|10/08/07|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161130|zbyt lubi strzelać z Kometora by ktokolwiek czuł się bezpiecznie. Zestrzelił Kazimierza Esuriit "małym Kometorem". Niestety.|[Sprowadzenie Mare Vortex](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html)|10/08/02|10/08/04|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161124|który nie chce pracować z tienowatymi i z pomocą Silurii poświęcił łazika by móc odepchnąć Wyspę Zmarłych|[Ponura historia ekspedycji Esuriit](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html)|10/07/31|10/08/01|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161109|konstruujący wpierw łazik a potem Kometor Oblężniczy Zabijający Czerwie. Obraża * magów Świecy. Prawie niszczy swoją bazę. Typowy Zajcew.|[Jak prawidłowo wpaść w pułapkę](/rpg/inwazja/opowiesci/konspekty/161109-jak-prawidlowo-wpasc-w-pulapke.html)|10/07/29|10/07/30|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161026|chciał poderwać Mariannę, ale mu nie wyszło. Nadzieja Silurii, że umie używać Pryzmatu...|[Zagłodzona ekspedycja Świecy](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html)|10/07/24|10/07/25|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161012|który połączył się z resztą KADEMek i który naprawdę nie umie prowadzić Opla Astra Setona (Seton też nie umiał).|[Kontratak Karradraela](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|10/07/22|10/07/23|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160927|który nie lubi "tienowatych" ale w obliczu zagrożenia zniszczenia Aegis zdecydował się na znalezienie kuzynki ze Świecy która może mu pomóc.|[Desperacka bateria dla Aegis](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|10/06/16|10/06/21|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150922|który dla jednej dziewczyny (Elizawiety) bardzo naraził stosunki między gildiami i przypadkiem mu się udało.|[Och, nie! Porwali Ignata!](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html)|10/05/29|10/05/30|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150920|wielki nieobecny, który non stop kręci się w sprawie Baltazara: a to koło Malii, a to koło Elizawiety, tu koło Żupana...|[Sprawa Baltazara Mausa](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|10/05/27|10/05/28|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160202|który wydał trochę kasy by pognębić tienowatego Sowińskiego; udowodnił, że ów jest mocny tylko w gębie.|[Wolność pająka fazowego](/rpg/inwazja/opowiesci/konspekty/160202-wolnosc-pajaka-fazowego.html)|10/05/09|10/05/10|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150625|który wkręcił Alisę w "lożę erotyczno-militarną" której oficerem jest Siluria i ogólnie świetnie się bawił kosztem Hektora.|[Poligon kluczem do Marcelina?](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html)|10/05/09|10/05/10|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150607|który założył się o autobus rozrodowych śledzików syberyjskich z * magiem Świecy, co strasznie skomplikowało żywot Hektora (bo Infernia podniosła stawkę).|[Brat przeciw bratu](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html)|10/05/07|10/05/08|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150330|detektyw który w końcu traci nerwy po zaatakowaniu Annalizy i sam atakuje niewinnego Marcela Bankierza.|[Napaść na Annalizę](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|10/04/29|10/04/30|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150329|kompetentny detektyw KADEMu nienawidzący SŚ, który odkrył dla Silurii powiązania między Izą, Tamarą i Draceną.|[Knowania Izy](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html)|10/04/25|10/04/28|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170217|mastermind chcący skłócić Mausów i Świecę (nadał zbirom pozytywkę Mausów do ukradnięcia); niestety, wykosztował się paskudnie odzyskując ją gdy sprawa stała się polityczna.|[Skradziona pozytywka Mausów](/rpg/inwazja/opowiesci/konspekty/170217-skradziona-pozytywka-mausow.html)|10/02/28|10/03/02|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170115|zirytowany jak osa; przeprosił Oliwiera za pobicie (bo Elizawieta), próbuje chronić Silurię i BARDZO mu się nie podoba, że tienowaci zrobili sobie z niego publiczne używanie...|[Klub Dare Shiver](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|10/02/22|10/02/25|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170111|którego pobicie ulicznego muzykanta ściągnęło nań Balroga; też, którego sromotna i haniebna porażka do Balroga mogłaby rozjarzyć konflikt KADEM - Świeca|[EIS na kozetce](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|10/02/18|10/02/21|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170108|zastraszający tienowatych na lewo i prawo, pobił niewinnego Oliwiera, detektyw odnajdujący Annę. MVP.|[Samotna w świecie magow](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html)|10/02/12|10/02/16|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161222|detektyw, który 'skorzystał' z okazji z Infernią by potem znaleźć wspólnie z Mordecją KADEMowego winowację - Mariana Ł.|[Kto wpisał Błażeja do konkursu](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|10/02/04|10/02/07|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161216|detektyw który powiedział Silurii jaka była rola Artura Kotały; też, wrobił Alisę w to, że by flirtować z Silurią trzeba go w jakiś sposób pokonać.|[Szept z Academii Daemonica](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|10/01/27|10/01/29|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1801-siluria-diakon.html)|19|[170519](/rpg/inwazja/opowiesci/konspekty/170519-odzyskac-aegis-0003.html), [170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [161207](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html), [161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html), [161109](/rpg/inwazja/opowiesci/konspekty/161109-jak-prawidlowo-wpasc-w-pulapke.html), [161026](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html), [161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html), [150922](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html), [150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html), [150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html), [150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html), [170217](/rpg/inwazja/opowiesci/konspekty/170217-skradziona-pozytywka-mausow.html), [170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html), [170108](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html), [161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html), [161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|9|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [161207](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html), [161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html), [161109](/rpg/inwazja/opowiesci/konspekty/161109-jak-prawidlowo-wpasc-w-pulapke.html), [161026](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html), [161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html), [150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html), [150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html)|
|[Andżelika Leszczyńska](/rpg/inwazja/opowiesci/karty-postaci/1709-andzelika-leszczynska.html)|8|[161207](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html), [161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html), [161109](/rpg/inwazja/opowiesci/konspekty/161109-jak-prawidlowo-wpasc-w-pulapke.html), [161026](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html), [161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html), [150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Mikado Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-mikado-diakon.html)|7|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [161207](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html), [161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html), [161109](/rpg/inwazja/opowiesci/konspekty/161109-jak-prawidlowo-wpasc-w-pulapke.html), [161026](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html), [161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|[Infernia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-infernia-diakon.html)|7|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html), [150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html), [170217](/rpg/inwazja/opowiesci/konspekty/170217-skradziona-pozytywka-mausow.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html), [161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html), [161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|[Paweł Sępiak](/rpg/inwazja/opowiesci/karty-postaci/1709-pawel-sepiak.html)|6|[170519](/rpg/inwazja/opowiesci/konspekty/170519-odzyskac-aegis-0003.html), [150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html), [150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html), [170217](/rpg/inwazja/opowiesci/konspekty/170217-skradziona-pozytywka-mausow.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Anna Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-myszeczka.html)|6|[161207](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html), [161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html), [161109](/rpg/inwazja/opowiesci/konspekty/161109-jak-prawidlowo-wpasc-w-pulapke.html), [161026](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html), [161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|[Marianna Sowińska](/rpg/inwazja/opowiesci/karty-postaci/1709-marianna-sowinska.html)|5|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html), [161109](/rpg/inwazja/opowiesci/konspekty/161109-jak-prawidlowo-wpasc-w-pulapke.html), [161026](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html)|
|[Siriratharin](/rpg/inwazja/opowiesci/karty-postaci/1709-siriratharin.html)|4|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html), [161109](/rpg/inwazja/opowiesci/konspekty/161109-jak-prawidlowo-wpasc-w-pulapke.html), [161026](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html)|
|[Norbert Sonet](/rpg/inwazja/opowiesci/karty-postaci/9999-norbert-sonet.html)|4|[161207](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html), [150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html), [150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html), [150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Marta Szysznicka](/rpg/inwazja/opowiesci/karty-postaci/9999-marta-szysznicka.html)|4|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [161207](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html), [150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|[Lucjan Kopidół](/rpg/inwazja/opowiesci/karty-postaci/9999-lucjan-kopidol.html)|4|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html), [161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html), [161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|[Dosifiej Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-dosifiej-zajcew.html)|4|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html), [161109](/rpg/inwazja/opowiesci/konspekty/161109-jak-prawidlowo-wpasc-w-pulapke.html), [161026](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html)|
|[Whisperwind](/rpg/inwazja/opowiesci/karty-postaci/9999-whisperwind.html)|3|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html), [161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|[Warmaster](/rpg/inwazja/opowiesci/karty-postaci/9999-warmaster.html)|3|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html), [161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|[Marian Łajdak](/rpg/inwazja/opowiesci/karty-postaci/1709-marian-lajdak.html)|3|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [170108](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html), [161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Marcel Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-marcel-bankierz.html)|3|[150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html), [150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html), [150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Karolina Kupiec](/rpg/inwazja/opowiesci/karty-postaci/1709-karolina-kupiec.html)|3|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html), [170108](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html)|
|[Karina Paczulis](/rpg/inwazja/opowiesci/karty-postaci/9999-karina-paczulis.html)|3|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [161207](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html), [161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|[Jolanta Sowińska](/rpg/inwazja/opowiesci/karty-postaci/1709-jolanta-sowinska.html)|3|[150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html), [170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html), [161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Joachim Kartel](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-kartel.html)|3|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html), [170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|[Ilona Amant](/rpg/inwazja/opowiesci/karty-postaci/9999-ilona-amant.html)|3|[170519](/rpg/inwazja/opowiesci/konspekty/170519-odzyskac-aegis-0003.html), [161207](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[GS "Aegis" 0003](/rpg/inwazja/opowiesci/karty-postaci/9999-gs-aegis-0003.html)|3|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html), [150922](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html), [150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Franciszek Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-maus.html)|3|[161207](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html), [170217](/rpg/inwazja/opowiesci/konspekty/170217-skradziona-pozytywka-mausow.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Elizawieta Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-elizawieta-zajcew.html)|3|[150922](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html), [150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html), [170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|3|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html), [160202](/rpg/inwazja/opowiesci/konspekty/160202-wolnosc-pajaka-fazowego.html), [150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html)|
|[Łukija Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-lukija-zajcew.html)|2|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html)|
|[Zofia Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-zofia-weiner.html)|2|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html)|
|[Wiktor Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1709-wiktor-sowinski.html)|2|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html), [150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Vladlena Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-vladlena-zjacew.html)|2|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html), [150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html)|
|[Urszula Murczyk](/rpg/inwazja/opowiesci/karty-postaci/1709-urszula-murczyk.html)|2|[170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html), [170108](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html)|
|[Tamara Muszkiet](/rpg/inwazja/opowiesci/karty-postaci/1709-tamara-muszkiet.html)|2|[150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html)|
|[Quasar](/rpg/inwazja/opowiesci/karty-postaci/9999-quasar.html)|2|[161207](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Ozydiusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-ozydiusz-bankierz.html)|2|[150922](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html), [150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|
|[Oktawian Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-oktawian-maus.html)|2|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html)|
|[Ofelia Caesar](/rpg/inwazja/opowiesci/karty-postaci/9999-ofelia-caesar.html)|2|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html), [150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html)|
|[Netheria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-netheria-diakon.html)|2|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Milena Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-milena-diakon.html)|2|[150922](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html), [150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|
|[Melodia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1801-melodia-diakon.html)|2|[170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html), [161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Mariusz Trzosik](/rpg/inwazja/opowiesci/karty-postaci/9999-mariusz-trzosik.html)|2|[150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html), [150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html)|
|[Konstanty Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-konstanty-myszeczka.html)|2|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html)|
|[Klara Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-klara-blakenbauer.html)|2|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html), [160202](/rpg/inwazja/opowiesci/konspekty/160202-wolnosc-pajaka-fazowego.html)|
|[Kazimierz Sowiński](/rpg/inwazja/opowiesci/karty-postaci/9999-kazimierz-sowinski.html)|2|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html)|
|[Julia Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-julia-weiner.html)|2|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html)|
|[Judyta Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-judyta-maus.html)|2|[170217](/rpg/inwazja/opowiesci/konspekty/170217-skradziona-pozytywka-mausow.html), [170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|[Joachim Kopiec](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-kopiec.html)|2|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|[Jakub Pestka](/rpg/inwazja/opowiesci/karty-postaci/9999-jakub-pestka.html)|2|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161026](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html)|
|[Infensa Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-infensa-diakon.html)|2|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html)|
|[GS Aegis 0003](/rpg/inwazja/opowiesci/karty-postaci/9999-gs-aegis-0003.html)|2|[170519](/rpg/inwazja/opowiesci/konspekty/170519-odzyskac-aegis-0003.html), [160927](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|
|[Fiodor Maius Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-fiodor-maius-zajcew.html)|2|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html)|
|[Eis](/rpg/inwazja/opowiesci/karty-postaci/9999-eis.html)|2|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Draconis Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-draconis-diakon.html)|2|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html)|
|[Bogumił Rojowiec](/rpg/inwazja/opowiesci/karty-postaci/9999-bogumil-rojowiec.html)|2|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Balrog Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-balrog-bankierz.html)|2|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Artur Żupan](/rpg/inwazja/opowiesci/karty-postaci/9999-artur-zupan.html)|2|[150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html), [161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Antygona Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-antygona-diakon.html)|2|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html), [150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html)|
|[Anna Kozak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kozak.html)|2|[150922](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html), [170108](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html)|
|[Andrea Wilgacz](/rpg/inwazja/opowiesci/karty-postaci/1709-andrea-wilgacz.html)|2|[150922](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html), [150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|
|[Alisa Wiraż](/rpg/inwazja/opowiesci/karty-postaci/9999-alisa-wiraz.html)|2|[150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html), [161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|[Zenobia Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-zenobia-bankierz.html)|1|[160927](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|
|[Wioletta Lemona-Chang](/rpg/inwazja/opowiesci/karty-postaci/9999-wioletta-lemona-chang.html)|1|[170108](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html)|
|[Tymoteusz Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-tymoteusz-maus.html)|1|[170217](/rpg/inwazja/opowiesci/konspekty/170217-skradziona-pozytywka-mausow.html)|
|[Teresa Koliczer](/rpg/inwazja/opowiesci/karty-postaci/9999-teresa-koliczer.html)|1|[170519](/rpg/inwazja/opowiesci/konspekty/170519-odzyskac-aegis-0003.html)|
|[Szczepan Sowiński](/rpg/inwazja/opowiesci/karty-postaci/9999-szczepan-sowinski.html)|1|[160927](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|
|[Stefan Jasionek](/rpg/inwazja/opowiesci/karty-postaci/9999-stefan-jasionek.html)|1|[170519](/rpg/inwazja/opowiesci/konspekty/170519-odzyskac-aegis-0003.html)|
|[Stalowy Śledzik Żarłacz](/rpg/inwazja/opowiesci/karty-postaci/9999-stalowy-sledzik-zarlacz.html)|1|[170519](/rpg/inwazja/opowiesci/konspekty/170519-odzyskac-aegis-0003.html)|
|[Sandra Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-sandra-maus.html)|1|[170519](/rpg/inwazja/opowiesci/konspekty/170519-odzyskac-aegis-0003.html)|
|[Salazar Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-salazar-bankierz.html)|1|[150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html)|
|[Saith Kameleon](/rpg/inwazja/opowiesci/karty-postaci/9999-saith-kameleon.html)|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|[Saith Flamecaller](/rpg/inwazja/opowiesci/karty-postaci/9999-saith-flamecaller.html)|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|[Rukoliusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-rukoliusz-bankierz.html)|1|[170108](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html)|
|[Roman Błyszczyk](/rpg/inwazja/opowiesci/karty-postaci/9999-roman-blyszczyk.html)|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|[Remigiusz Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-remigiusz-zajcew.html)|1|[150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|
|[Rafał Kniaź](/rpg/inwazja/opowiesci/karty-postaci/9999-rafal-kniaz.html)|1|[160927](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|
|[Paweł Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-pawel-maus.html)|1|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html)|
|[Otton Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-otton-blakenbauer.html)|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|[Oliwier Bonwant](/rpg/inwazja/opowiesci/karty-postaci/9999-oliwier-bonwant.html)|1|[170108](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html)|
|[Nikodem Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1709-nikodem-sowinski.html)|1|[160202](/rpg/inwazja/opowiesci/konspekty/160202-wolnosc-pajaka-fazowego.html)|
|[Mordecja Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-mordecja-diakon.html)|1|[161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Maurycy Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-maurycy-maus.html)|1|[170217](/rpg/inwazja/opowiesci/konspekty/170217-skradziona-pozytywka-mausow.html)|
|[Marian Agrest](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-agrest.html)|1|[150922](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html)|
|[Margaret Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-margaret-blakenbauer.html)|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|[Marcelin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-marcelin-blakenbauer.html)|1|[150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html)|
|[Malia Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-malia-bankierz.html)|1|[150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|
|[Leonidas Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-leonidas-blakenbauer.html)|1|[160202](/rpg/inwazja/opowiesci/konspekty/160202-wolnosc-pajaka-fazowego.html)|
|[Laurena Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-laurena-bankierz.html)|1|[160202](/rpg/inwazja/opowiesci/konspekty/160202-wolnosc-pajaka-fazowego.html)|
|[Lancelot Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-lancelot-bankierz.html)|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|[Kornelia Kartel](/rpg/inwazja/opowiesci/karty-postaci/9999-kornelia-kartel.html)|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|[Konstanty Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-konstanty-bankierz.html)|1|[161109](/rpg/inwazja/opowiesci/konspekty/161109-jak-prawidlowo-wpasc-w-pulapke.html)|
|[Konrad Sowiński](/rpg/inwazja/opowiesci/karty-postaci/9999-konrad-sowinski.html)|1|[161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Kira Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-kira-zajcew.html)|1|[160927](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|
|[Kermit Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-kermit-diakon.html)|1|[150922](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html)|
|[Katarzyna Kotek](/rpg/inwazja/opowiesci/karty-postaci/9999-katarzyna-kotek.html)|1|[160927](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|
|[Karradrael](/rpg/inwazja/opowiesci/karty-postaci/9999-karradrael.html)|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|[Karolina Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-karolina-maus.html)|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|[Karol Poczciwiec](/rpg/inwazja/opowiesci/karty-postaci/9999-karol-poczciwiec.html)|1|[150922](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html)|
|[Julian Krukowicz](/rpg/inwazja/opowiesci/karty-postaci/9999-julian-krukowicz.html)|1|[161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Judyta Karnisz](/rpg/inwazja/opowiesci/karty-postaci/9999-judyta-karnisz.html)|1|[150922](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html)|
|[Jan Wątły](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-watly.html)|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|[Irina Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-irina-zajcew.html)|1|[150922](/rpg/inwazja/opowiesci/konspekty/150922-och-nie-porwali-ignata.html)|
|[Gala Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-gala-zajcew.html)|1|[170217](/rpg/inwazja/opowiesci/konspekty/170217-skradziona-pozytywka-mausow.html)|
|[Franciszek Baranowski](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-baranowski.html)|1|[160927](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|
|[Felicja Strączek](/rpg/inwazja/opowiesci/karty-postaci/9999-felicja-straczek.html)|1|[160927](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|
|[Ewa Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-ewa-zajcew.html)|1|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Emilia Kołatka](/rpg/inwazja/opowiesci/karty-postaci/1709-emilia-kolatka.html)|1|[150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html)|
|[Elea Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-elea-maus.html)|1|[150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|
|[Edward Sasanka](/rpg/inwazja/opowiesci/karty-postaci/9999-edward-sasanka.html)|1|[170519](/rpg/inwazja/opowiesci/konspekty/170519-odzyskac-aegis-0003.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|1|[150329](/rpg/inwazja/opowiesci/konspekty/150329-knowania-izy.html)|
|[Demon_481](/rpg/inwazja/opowiesci/karty-postaci/9999-demon_481.html)|1|[150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html)|
|[Dagmara Wyjątek](/rpg/inwazja/opowiesci/karty-postaci/1709-dagmara-wyjątek.html)|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|[Czirna Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-czirna-zajcew.html)|1|[160927](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|
|[Błażej Falka](/rpg/inwazja/opowiesci/karty-postaci/9999-blazej-falka.html)|1|[161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Borys Kumin](/rpg/inwazja/opowiesci/karty-postaci/1709-borys-kumin.html)|1|[160202](/rpg/inwazja/opowiesci/konspekty/160202-wolnosc-pajaka-fazowego.html)|
|[Bogdan Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-bogdan-bankierz.html)|1|[150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|
|[Bazyli Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-bazyli-weiner.html)|1|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html)|
|[Baltazar Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-baltazar-maus.html)|1|[150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|
|[Aurel Czarko](/rpg/inwazja/opowiesci/karty-postaci/1709-aurel-czarko.html)|1|[160927](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|
|[Artur Kotała](/rpg/inwazja/opowiesci/karty-postaci/9999-artur-kotala.html)|1|[161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|[Arazille](/rpg/inwazja/opowiesci/karty-postaci/9999-arazille.html)|1|[160202](/rpg/inwazja/opowiesci/konspekty/160202-wolnosc-pajaka-fazowego.html)|
|[Antoni Chlebak](/rpg/inwazja/opowiesci/karty-postaci/9999-antoni-chlebak.html)|1|[160927](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|
|[Annaliza Delfin](/rpg/inwazja/opowiesci/karty-postaci/9999-annaliza-delfin.html)|1|[150330](/rpg/inwazja/opowiesci/konspekty/150330-napasc-na-annalize.html)|
|[Andrzej Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-andrzej-bankierz.html)|1|[160927](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|
|[Aleksandra Trawens](/rpg/inwazja/opowiesci/karty-postaci/9999-aleksandra-trawens.html)|1|[161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|[Aleksander Sowiński](/rpg/inwazja/opowiesci/karty-postaci/9999-aleksander-sowinski.html)|1|[150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|
|[Adrian Kropiak](/rpg/inwazja/opowiesci/karty-postaci/9999-adrian-kropiak.html)|1|[150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|
|[Adonis Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1709-adonis-sowinski.html)|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|

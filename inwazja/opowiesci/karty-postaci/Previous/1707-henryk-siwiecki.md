---
layout: inwazja-karta-postaci
categories: profile
factions: "Srebrna Świeca"
type: "PC"
owner: "raynor"
title: "Henryk Siwiecki"
---
# {{ page.title }}

## Postać

### Motywacje (do czego dąży)

* **Prawdziwy wpływ**: "chcę mieć faktyczny wpływ na najpotężniejszą organizację magów. Tzn. Albo sprawię, że moja organizacja będzie najpotężniejsza i będę miał na nią wpływ, albo dołączę do innej, na którą również będę miał wpływ"
* **Wieksza ewolucja**: ciągle lepszy, potężniejszy.
* **Ład i porządek**: istnieje powód dlaczego istnieje porządek społeczny, bez niego wszystko zaczyna się rozpadać

### Silne i słabe strony

* **+**: asertywność
* **+**: oportunista: niechętnie robi rzeczy dla innych, bez korzyści dla siebie
* **+**: ostrożny

* **-**: słaby fizycznie
* **-**: oportunista: niechętnie robi rzeczy dla innych, bez korzyści dla siebie
* **-**: chciwy

### Specjalizacje
  
- **"to twoj pomysł"** (maniuplator)
- **pryzmat** (astralika)
- **kontrola i dominacja** (mentalna)
- **badanie magii** (kataliza)

### Umiejętności

- **dyplomata**
- **ksiądz**
- **manipulator**
- **egzorcysta**
- **negocjator**
- **psycholog**
- **polityk**

### Specjalne
* **brak**
* **nazwa_specjalnej_własności**: opis_własności_lub_link

## Magia

### Szkoły magiczne

* **Magia mentalna**:
    * _Aspekty_: astralika
    * _Opis_:
* **Kataliza**:
    * _Aspekty_: 
    * _Opis_:

### Zaklęcia statyczne

- **echo emocji**

## Zasoby i otoczenie

### Powiązane frakcje

Srebrna Świeca

### Kogo zna

* czlonek sympatyk szlachty
* sława skutecznego egzorcysty
* opinia tego co przynosi podwójne profity
* hierarchowie kosciola
* dziennikarze telewizji Nowina

### Co ma do dyspozycji:

* zaufanie spoleczne (ksiadz)
* oddział kleryków/paladynow wspomagajacych egzorcyzmy
* Kaldwor i jego organizacja
* sprzet egzorcystyczny
* Oczy kosciola

# Opis

### Koncept

Mag egzorcysta, skupiający się na poszerzaniu wpływów i awansie w magicznej drabinie społecznej.

### Motto

"Audi multa, dic pauca"

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|[Kolejna porażka Kinglorda](/rpg/inwazja/opowiesci/konspekty/170914-kolejna-porazka-kinglorda.html)|od tej pory jest damą i to rodu Souris, jak Renata; sprzężona jest z Renatą mindlinkiem|Powrót Karradraela|
|[Suma niedokończonych spraw...](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|można do niego mówić "Alicjo". Zmieniony przez Kinglorda w dziewczynkę.|Powrót Karradraela|
|[Wielki sojusz powszechny](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html)|pakt o nieagresji z Marzeną i dodatkowo współpracy / obronie z Kariną, Nicarettą|Nicaretta|
|[Wielki sojusz powszechny](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html)|zaczęło mu troszkę zależeć na Karinie|Nicaretta|
|[Ukradziona Apokalipsa](/rpg/inwazja/opowiesci/konspekty/170628-ukradziona-apokalipsa.html)|szacun za zdobycie i przechwycenie Luksji. Też... a co zrobił z Artefaktem Apokalipsy Psinoskiej?!|Córka Lucyfera|


## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|170920|jako Hania. Zeżarła Mausowy TechBunker dla wspólnego dobra aby naładować Operiatrix. Potem zasiliła Silurię energią. Też - ma dobry wpływ na Renatę Souris (serio).|[Początki prokuratury](/rpg/inwazja/opowiesci/konspekty/170920-poczatki-prokuratury.html)|10/09/03|10/09/05|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170914|rozpoczyna nowe życie jako "Henrietta" czy "Hania"; nawiązał kontakt z Renatą i zabił randomowego dziadka, niechcący.|[Kolejna porażka Kinglorda](/rpg/inwazja/opowiesci/konspekty/170914-kolejna-porazka-kinglorda.html)|10/08/31|10/09/02|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170823|z rozkazów Kinglorda miał zaplanować śmierć Hektora. Nie wyszło, skończył jako słodka dziewczynka w służbie Kariny von Blutwurst. Świetny w maskowaniu. Zdobył krew Kariny i wpadł w łapy Mordreda.|[Suma niedokończonych spraw...](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|10/08/25|10/08/28|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170120|zawiązał sojusz życia: Nicaretta, Karina, Marzena. Opinia 'zainteresowanego nieletnimi' zaczyna krążyć. Mistrz logistyki Hubertem. Też: zwala winę na INNYCH księży.|[Wielki sojusz powszechny](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html)|10/08/05|10/08/06|[Nicaretta](/rpg/inwazja/opowiesci/konspekty/kampania-nicaretta.html)|
|170113|którego doskonały plan posypał się przez JEDNO zaklęcie cholernej Marzeny. Skończył jako "ksiądz pedofil", pobity i nagi. Absolutna, sromotna porażka na każdym polu... Ale odzyskał Klepiczka na osłodę.|['Dzisiaj złapiemy Nicarettę!'](/rpg/inwazja/opowiesci/konspekty/170113-dzisiaj-zlapiemy-nicarette.html)|10/08/01|10/08/04|[Nicaretta](/rpg/inwazja/opowiesci/konspekty/kampania-nicaretta.html)|
|161220|doprowadziła do zniszczenia posągu Arazille. Pomógł Karinie; wyciągał informacje z księdza oraz zaczarował rodzinę Natalii Kaldwor. Wyczuł, że coś z Kariną nie tak, ale... co?|[Zniszczenie posągu Arazille](/rpg/inwazja/opowiesci/konspekty/161220-zniszczenie-posagu-arazille.html)|10/07/27|10/07/30|[Nicaretta](/rpg/inwazja/opowiesci/konspekty/kampania-nicaretta.html)|
|170214|który zbudował nekroborga na życzenie Laetitii, bezczeszcząc trochę wszystkiego po drodze... trochę za dużo...|[Nekroborg dla Laetitii Gai](/rpg/inwazja/opowiesci/konspekty/170214-nekroborg-dla-laetitii-gai.html)|10/07/26|10/07/29|[Nicaretta](/rpg/inwazja/opowiesci/konspekty/kampania-nicaretta.html)|
|161206|który wypędził Siostry Światła z Lenartomina, ostrzegł Huberta i przekazał Świecy, że Srebrne Lustro Arazille jest w rękach "Amelii".|[Ucieczka Sióstr Światła](/rpg/inwazja/opowiesci/konspekty/161206-ucieczka-siostr-swiatla.html)|10/07/25|10/07/26|[Nicaretta](/rpg/inwazja/opowiesci/konspekty/kampania-nicaretta.html)|
|161129|dowiedział się, o co chodzi ze srebrnym lustrem; po czym o co chodzi femisatanistkom. Finalnie - uratował Huberta łamiąc czar przymusu na Mai.|[Ewakuacja Natalii, wejście maga](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html)|10/07/23|10/07/24|[Nicaretta](/rpg/inwazja/opowiesci/konspekty/kampania-nicaretta.html)|
|161115|zaniepokojony wstępnymi oznakami obecności Arazille na usługach pornobiznesu dla własnych korzyści. Potrzebuje pomocy w ubiciu demona.|[Uciekła do femisatanistek](/rpg/inwazja/opowiesci/konspekty/161115-uciekla-do-femisatanistek.html)|10/07/21|10/07/22|[Nicaretta](/rpg/inwazja/opowiesci/konspekty/kampania-nicaretta.html)|
|161110|ksiądz dzielnie tropiący i egzorcyzmujący sukkuba, który wykrył że są dwa i w sumie skończył chory. Mistrz środków przeczyszczających.|[Succubus myśli, że uciekł](/rpg/inwazja/opowiesci/konspekty/161110-succubus-mysli-ze-uciekl.html)|10/07/14|10/07/20|[Nicaretta](/rpg/inwazja/opowiesci/konspekty/kampania-nicaretta.html)|
|161103|który zwerbował armię ludzi w Żonkiborze by zatrzymać przyzwanie sukkuba... i prawie wyszło. Też: chował się w szafie przed napaloną nastolatką.|[Egzorcyzmy w Żonkiborze](/rpg/inwazja/opowiesci/konspekty/161103-egzorcyzmy-w-zonkiborze.html)|10/07/09|10/07/12|[Nicaretta](/rpg/inwazja/opowiesci/konspekty/kampania-nicaretta.html)|
|161018|miejski ksiądz na bagnach, który zaliczając glebę uratował las od pożaru i zniszczył Toksyczną efemerydę.|[Ballada o duszy ognistej](/rpg/inwazja/opowiesci/konspekty/161018-ballada-o-duszy-ognistej.html)|10/07/02|10/07/04|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|
|170707|jedyny głos biznesowy w całej spółce (już żałuje). Poszerza krąg znajomych. Unika niepotrzebnych konfliktów. Poznał naturę przeciwnika - wojna Mausów.|[Biznes pośród niesnasek](/rpg/inwazja/opowiesci/konspekty/170707-biznes-posrod-niesnasek.html)|10/03/06|10/03/08|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170628|udający Lucyfera i ostrzący sobie zęby na Luksję. Pokazał wysoką skuteczność i ograniczoną moralność. Skopał Luksję.|[Ukradziona Apokalipsa](/rpg/inwazja/opowiesci/konspekty/170628-ukradziona-apokalipsa.html)|10/02/16|10/02/17|[Córka Lucyfera](/rpg/inwazja/opowiesci/konspekty/kampania-corka-lucyfera.html)|
|170620|zbudował armię kościoła (którą stracił na rzecz Biegusia), zanęcił Luksję do przyjścia na plebanię po czym rozwalił demona i zdominował samą Luksję. Ranny.|[Pułapka na Luksję](/rpg/inwazja/opowiesci/konspekty/170620-pulapka-na-luksje.html)|10/02/13|10/02/15|[Córka Lucyfera](/rpg/inwazja/opowiesci/konspekty/kampania-corka-lucyfera.html)|
|170603|fatalnie jeździ na rowerze i ogólnie ma pecha. Ale! Uciekł thrallom, wystawił ich Estrelli i dowiedział się sporo o całym problemie.|[Córka jest narzędziem?](/rpg/inwazja/opowiesci/konspekty/170603-corka-jest-narzedziem.html)|10/02/11|10/02/12|[Córka Lucyfera](/rpg/inwazja/opowiesci/konspekty/kampania-corka-lucyfera.html)|
|170530|zaprzyjaźniający się z Zenobim Klepiczkiem, skutecznie uniknął ataku thralli, wyciągnął z głowy Klepiczka kluczowe dane o rytuale.|[Córka Lucyfera](/rpg/inwazja/opowiesci/konspekty/170530-corka-lucyfera.html)|10/02/08|10/02/10|[Córka Lucyfera](/rpg/inwazja/opowiesci/konspekty/kampania-corka-lucyfera.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|[Hubert Kaldwor](/rpg/inwazja/opowiesci/karty-postaci/9999-hubert-kaldwor.html)|6|[170120](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html), [170113](/rpg/inwazja/opowiesci/konspekty/170113-dzisiaj-zlapiemy-nicarette.html), [161206](/rpg/inwazja/opowiesci/konspekty/161206-ucieczka-siostr-swiatla.html), [161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html), [161115](/rpg/inwazja/opowiesci/konspekty/161115-uciekla-do-femisatanistek.html), [161110](/rpg/inwazja/opowiesci/konspekty/161110-succubus-mysli-ze-uciekl.html)|
|[Marzena Dorszaj](/rpg/inwazja/opowiesci/karty-postaci/1709-marzena-dorszaj.html)|5|[170120](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html), [170113](/rpg/inwazja/opowiesci/konspekty/170113-dzisiaj-zlapiemy-nicarette.html), [161220](/rpg/inwazja/opowiesci/konspekty/161220-zniszczenie-posagu-arazille.html), [161206](/rpg/inwazja/opowiesci/konspekty/161206-ucieczka-siostr-swiatla.html), [161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html)|
|[Karina Łoszad](/rpg/inwazja/opowiesci/karty-postaci/9999-karina-loszad.html)|5|[170920](/rpg/inwazja/opowiesci/konspekty/170920-poczatki-prokuratury.html), [170120](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html), [170113](/rpg/inwazja/opowiesci/konspekty/170113-dzisiaj-zlapiemy-nicarette.html), [161220](/rpg/inwazja/opowiesci/konspekty/161220-zniszczenie-posagu-arazille.html), [170214](/rpg/inwazja/opowiesci/konspekty/170214-nekroborg-dla-laetitii-gai.html)|
|[Estrella Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-estrella-diakon.html)|5|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html), [170628](/rpg/inwazja/opowiesci/konspekty/170628-ukradziona-apokalipsa.html), [170620](/rpg/inwazja/opowiesci/konspekty/170620-pulapka-na-luksje.html), [170603](/rpg/inwazja/opowiesci/konspekty/170603-corka-jest-narzedziem.html), [170530](/rpg/inwazja/opowiesci/konspekty/170530-corka-lucyfera.html)|
|[Andrzej Klepiczek](/rpg/inwazja/opowiesci/karty-postaci/9999-andrzej-klepiczek.html)|5|[170113](/rpg/inwazja/opowiesci/konspekty/170113-dzisiaj-zlapiemy-nicarette.html), [161220](/rpg/inwazja/opowiesci/konspekty/161220-zniszczenie-posagu-arazille.html), [161206](/rpg/inwazja/opowiesci/konspekty/161206-ucieczka-siostr-swiatla.html), [161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html), [161115](/rpg/inwazja/opowiesci/konspekty/161115-uciekla-do-femisatanistek.html)|
|[Nicaretta](/rpg/inwazja/opowiesci/karty-postaci/1709-nicaretta.html)|4|[170120](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html), [170214](/rpg/inwazja/opowiesci/konspekty/170214-nekroborg-dla-laetitii-gai.html), [161110](/rpg/inwazja/opowiesci/konspekty/161110-succubus-mysli-ze-uciekl.html), [161103](/rpg/inwazja/opowiesci/konspekty/161103-egzorcyzmy-w-zonkiborze.html)|
|[Luksja Pandemoniae](/rpg/inwazja/opowiesci/karty-postaci/9999-luksja-pandemoniae.html)|4|[170628](/rpg/inwazja/opowiesci/konspekty/170628-ukradziona-apokalipsa.html), [170620](/rpg/inwazja/opowiesci/konspekty/170620-pulapka-na-luksje.html), [170603](/rpg/inwazja/opowiesci/konspekty/170603-corka-jest-narzedziem.html), [170530](/rpg/inwazja/opowiesci/konspekty/170530-corka-lucyfera.html)|
|[Urszula Kram](/rpg/inwazja/opowiesci/karty-postaci/9999-urszula-kram.html)|3|[161220](/rpg/inwazja/opowiesci/konspekty/161220-zniszczenie-posagu-arazille.html), [161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html), [161115](/rpg/inwazja/opowiesci/konspekty/161115-uciekla-do-femisatanistek.html)|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-siluria-diakon.html)|3|[170920](/rpg/inwazja/opowiesci/konspekty/170920-poczatki-prokuratury.html), [170914](/rpg/inwazja/opowiesci/konspekty/170914-kolejna-porazka-kinglorda.html), [170707](/rpg/inwazja/opowiesci/konspekty/170707-biznes-posrod-niesnasek.html)|
|[Natalia Kaldwor](/rpg/inwazja/opowiesci/karty-postaci/9999-natalia-kaldwor.html)|3|[161220](/rpg/inwazja/opowiesci/konspekty/161220-zniszczenie-posagu-arazille.html), [161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html), [161115](/rpg/inwazja/opowiesci/konspekty/161115-uciekla-do-femisatanistek.html)|
|[Mordred Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1707-mordred-blakenbauer.html)|3|[170920](/rpg/inwazja/opowiesci/konspekty/170920-poczatki-prokuratury.html), [170914](/rpg/inwazja/opowiesci/konspekty/170914-kolejna-porazka-kinglorda.html), [170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1707-hektor-blakenbauer.html)|3|[170920](/rpg/inwazja/opowiesci/konspekty/170920-poczatki-prokuratury.html), [170914](/rpg/inwazja/opowiesci/konspekty/170914-kolejna-porazka-kinglorda.html), [170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Zenobi Klepiczek](/rpg/inwazja/opowiesci/karty-postaci/9999-zenobi-klepiczek.html)|2|[170620](/rpg/inwazja/opowiesci/konspekty/170620-pulapka-na-luksje.html), [170530](/rpg/inwazja/opowiesci/konspekty/170530-corka-lucyfera.html)|
|[Witold Małek](/rpg/inwazja/opowiesci/karty-postaci/9999-witold-malek.html)|2|[170120](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html), [170113](/rpg/inwazja/opowiesci/konspekty/170113-dzisiaj-zlapiemy-nicarette.html)|
|[Piotr Kit](/rpg/inwazja/opowiesci/karty-postaci/1709-piotr-kit.html)|2|[170914](/rpg/inwazja/opowiesci/konspekty/170914-kolejna-porazka-kinglorda.html), [170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Marzena Gilek](/rpg/inwazja/opowiesci/karty-postaci/9999-marzena-gilek.html)|2|[161110](/rpg/inwazja/opowiesci/konspekty/161110-succubus-mysli-ze-uciekl.html), [161103](/rpg/inwazja/opowiesci/konspekty/161103-egzorcyzmy-w-zonkiborze.html)|
|[Marcelin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1707-marcelin-blakenbauer.html)|2|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html), [170707](/rpg/inwazja/opowiesci/konspekty/170707-biznes-posrod-niesnasek.html)|
|[Maja Liszka](/rpg/inwazja/opowiesci/karty-postaci/1709-maja-liszka.html)|2|[161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html), [161115](/rpg/inwazja/opowiesci/konspekty/161115-uciekla-do-femisatanistek.html)|
|[Kora Panik](/rpg/inwazja/opowiesci/karty-postaci/9999-kora-panik.html)|2|[161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html), [161115](/rpg/inwazja/opowiesci/konspekty/161115-uciekla-do-femisatanistek.html)|
|[Kinglord](/rpg/inwazja/opowiesci/karty-postaci/9999-kinglord.html)|2|[170920](/rpg/inwazja/opowiesci/konspekty/170920-poczatki-prokuratury.html), [170120](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html)|
|[Kalina Cząberek](/rpg/inwazja/opowiesci/karty-postaci/9999-kalina-czaberek.html)|2|[170603](/rpg/inwazja/opowiesci/konspekty/170603-corka-jest-narzedziem.html), [170530](/rpg/inwazja/opowiesci/konspekty/170530-corka-lucyfera.html)|
|[Judyta Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-judyta-maus.html)|2|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html), [170707](/rpg/inwazja/opowiesci/konspekty/170707-biznes-posrod-niesnasek.html)|
|[Janina Jasionek](/rpg/inwazja/opowiesci/karty-postaci/9999-janina-jasionek.html)|2|[170603](/rpg/inwazja/opowiesci/konspekty/170603-corka-jest-narzedziem.html), [170530](/rpg/inwazja/opowiesci/konspekty/170530-corka-lucyfera.html)|
|[Hipolit Mraczon](/rpg/inwazja/opowiesci/karty-postaci/9999-hipolit-mraczon.html)|2|[161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html), [161115](/rpg/inwazja/opowiesci/konspekty/161115-uciekla-do-femisatanistek.html)|
|[Henryk Kantosz](/rpg/inwazja/opowiesci/karty-postaci/9999-henryk-kantosz.html)|2|[170620](/rpg/inwazja/opowiesci/konspekty/170620-pulapka-na-luksje.html), [170603](/rpg/inwazja/opowiesci/konspekty/170603-corka-jest-narzedziem.html)|
|[Grażyna Diadem](/rpg/inwazja/opowiesci/karty-postaci/9999-grazyna-diadem.html)|2|[170120](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html), [161110](/rpg/inwazja/opowiesci/konspekty/161110-succubus-mysli-ze-uciekl.html)|
|[Filip Cząberek](/rpg/inwazja/opowiesci/karty-postaci/9999-filip-czaberek.html)|2|[170603](/rpg/inwazja/opowiesci/konspekty/170603-corka-jest-narzedziem.html), [170530](/rpg/inwazja/opowiesci/konspekty/170530-corka-lucyfera.html)|
|[Anna Osiaczek](/rpg/inwazja/opowiesci/karty-postaci/9999-anna-osiaczek.html)|2|[161220](/rpg/inwazja/opowiesci/konspekty/161220-zniszczenie-posagu-arazille.html), [161206](/rpg/inwazja/opowiesci/konspekty/161206-ucieczka-siostr-swiatla.html)|
|[Amelia Eter](/rpg/inwazja/opowiesci/karty-postaci/9999-amelia-eter.html)|2|[161206](/rpg/inwazja/opowiesci/konspekty/161206-ucieczka-siostr-swiatla.html), [161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html)|
|[Adela Klepiczek](/rpg/inwazja/opowiesci/karty-postaci/9999-adela-klepiczek.html)|2|[161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html), [161115](/rpg/inwazja/opowiesci/konspekty/161115-uciekla-do-femisatanistek.html)|
|[Łukasz Tworzyw](/rpg/inwazja/opowiesci/karty-postaci/9999-lukasz-tworzyw.html)|1|[170530](/rpg/inwazja/opowiesci/konspekty/170530-corka-lucyfera.html)|
|[Whisperwind](/rpg/inwazja/opowiesci/karty-postaci/9999-whisperwind.html)|1|[170920](/rpg/inwazja/opowiesci/konspekty/170920-poczatki-prokuratury.html)|
|[Warmaster](/rpg/inwazja/opowiesci/karty-postaci/9999-warmaster.html)|1|[170707](/rpg/inwazja/opowiesci/konspekty/170707-biznes-posrod-niesnasek.html)|
|[Tomasz Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-tomasz-weiner.html)|1|[161018](/rpg/inwazja/opowiesci/konspekty/161018-ballada-o-duszy-ognistej.html)|
|[Szczepan Sławski](/rpg/inwazja/opowiesci/karty-postaci/9999-szczepan-slawski.html)|1|[161103](/rpg/inwazja/opowiesci/konspekty/161103-egzorcyzmy-w-zonkiborze.html)|
|[Szczepan Szokmaniewicz](/rpg/inwazja/opowiesci/karty-postaci/9999-szczepan-szokmaniewicz.html)|1|[161220](/rpg/inwazja/opowiesci/konspekty/161220-zniszczenie-posagu-arazille.html)|
|[Supernowa Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-supernowa-diakon.html)|1|[170707](/rpg/inwazja/opowiesci/konspekty/170707-biznes-posrod-niesnasek.html)|
|[Stella Stellaris](/rpg/inwazja/opowiesci/karty-postaci/9999-stella-stellaris.html)|1|[170603](/rpg/inwazja/opowiesci/konspekty/170603-corka-jest-narzedziem.html)|
|[Sebastian Drabon](/rpg/inwazja/opowiesci/karty-postaci/9999-sebastian-drabon.html)|1|[170120](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html)|
|[Romana Kaldwor](/rpg/inwazja/opowiesci/karty-postaci/9999-romana-kaldwor.html)|1|[161220](/rpg/inwazja/opowiesci/konspekty/161220-zniszczenie-posagu-arazille.html)|
|[Renata Souris](/rpg/inwazja/opowiesci/karty-postaci/1709-renata-souris.html)|1|[170920](/rpg/inwazja/opowiesci/konspekty/170920-poczatki-prokuratury.html)|
|[Renata Krzem](/rpg/inwazja/opowiesci/karty-postaci/9999-renata-krzem.html)|1|[161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html)|
|[Rafał Szczęślik](/rpg/inwazja/opowiesci/karty-postaci/9999-rafal-szczeslik.html)|1|[161103](/rpg/inwazja/opowiesci/konspekty/161103-egzorcyzmy-w-zonkiborze.html)|
|[Rafael Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-rafael-diakon.html)|1|[170914](/rpg/inwazja/opowiesci/konspekty/170914-kolejna-porazka-kinglorda.html)|
|[Paweł Parobek](/rpg/inwazja/opowiesci/karty-postaci/9999-pawel-parobek.html)|1|[170620](/rpg/inwazja/opowiesci/konspekty/170620-pulapka-na-luksje.html)|
|[Otton Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-otton-blakenbauer.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Operiatrix](/rpg/inwazja/opowiesci/karty-postaci/9999-operiatrix.html)|1|[170920](/rpg/inwazja/opowiesci/konspekty/170920-poczatki-prokuratury.html)|
|[Olga Miodownik](/rpg/inwazja/opowiesci/karty-postaci/1709-olga-miodownik.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Oktawian Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-oktawian-maus.html)|1|[170707](/rpg/inwazja/opowiesci/konspekty/170707-biznes-posrod-niesnasek.html)|
|[Niektarij Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-niektarij-zajcew.html)|1|[161018](/rpg/inwazja/opowiesci/konspekty/161018-ballada-o-duszy-ognistej.html)|
|[Misza Dobroniewiec](/rpg/inwazja/opowiesci/karty-postaci/9999-misza-dobroniewiec.html)|1|[170603](/rpg/inwazja/opowiesci/konspekty/170603-corka-jest-narzedziem.html)|
|[Mikado Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-mikado-diakon.html)|1|[170628](/rpg/inwazja/opowiesci/konspekty/170628-ukradziona-apokalipsa.html)|
|[Margaret Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-margaret-blakenbauer.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Luna](/rpg/inwazja/opowiesci/karty-postaci/9999-luna.html)|1|[161110](/rpg/inwazja/opowiesci/konspekty/161110-succubus-mysli-ze-uciekl.html)|
|[Laetitia Gaia Rasputin Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-laetitia-gaia-rasputin-weiner.html)|1|[170214](/rpg/inwazja/opowiesci/konspekty/170214-nekroborg-dla-laetitii-gai.html)|
|[Krystian Korzunio](/rpg/inwazja/opowiesci/karty-postaci/1709-krystian-korzunio.html)|1|[161018](/rpg/inwazja/opowiesci/konspekty/161018-ballada-o-duszy-ognistej.html)|
|[Kornelia Kartel](/rpg/inwazja/opowiesci/karty-postaci/9999-kornelia-kartel.html)|1|[170707](/rpg/inwazja/opowiesci/konspekty/170707-biznes-posrod-niesnasek.html)|
|[Klara Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-klara-blakenbauer.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Kira Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-kira-zajcew.html)|1|[161018](/rpg/inwazja/opowiesci/konspekty/161018-ballada-o-duszy-ognistej.html)|
|[Karolina Kupiec](/rpg/inwazja/opowiesci/karty-postaci/1709-karolina-kupiec.html)|1|[170707](/rpg/inwazja/opowiesci/konspekty/170707-biznes-posrod-niesnasek.html)|
|[Karina von Blutwurst](/rpg/inwazja/opowiesci/karty-postaci/9999-karina-von-blutwurst.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Joachim Kartel](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-kartel.html)|1|[170707](/rpg/inwazja/opowiesci/konspekty/170707-biznes-posrod-niesnasek.html)|
|[Jewgenij Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-jewgenij-zajcew.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Jessika Gniewoń](/rpg/inwazja/opowiesci/karty-postaci/9999-jessika-gniewon.html)|1|[161103](/rpg/inwazja/opowiesci/konspekty/161103-egzorcyzmy-w-zonkiborze.html)|
|[Jelena Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-jelena-zajcew.html)|1|[161018](/rpg/inwazja/opowiesci/konspekty/161018-ballada-o-duszy-ognistej.html)|
|[Halina Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-halina-weiner.html)|1|[161018](/rpg/inwazja/opowiesci/konspekty/161018-ballada-o-duszy-ognistej.html)|
|[Franciszek Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-maus.html)|1|[170707](/rpg/inwazja/opowiesci/konspekty/170707-biznes-posrod-niesnasek.html)|
|[Fiodor Pyszczek](/rpg/inwazja/opowiesci/karty-postaci/9999-fiodor-pyszczek.html)|1|[170214](/rpg/inwazja/opowiesci/konspekty/170214-nekroborg-dla-laetitii-gai.html)|
|[Filip Gładki](/rpg/inwazja/opowiesci/karty-postaci/9999-filip-gladki.html)|1|[161103](/rpg/inwazja/opowiesci/konspekty/161103-egzorcyzmy-w-zonkiborze.html)|
|[Ferdynand Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-ferdynand-maus.html)|1|[170707](/rpg/inwazja/opowiesci/konspekty/170707-biznes-posrod-niesnasek.html)|
|[Felicja Wydech](/rpg/inwazja/opowiesci/karty-postaci/9999-felicja-wydech.html)|1|[161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html)|
|[Dżony Słomian](/rpg/inwazja/opowiesci/karty-postaci/9999-dzony-slomian.html)|1|[170620](/rpg/inwazja/opowiesci/konspekty/170620-pulapka-na-luksje.html)|
|[Dionizy Kret](/rpg/inwazja/opowiesci/karty-postaci/1709-dionizy-kret.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Damazy Rozenblum](/rpg/inwazja/opowiesci/karty-postaci/9999-damazy-rozenblum.html)|1|[161018](/rpg/inwazja/opowiesci/konspekty/161018-ballada-o-duszy-ognistej.html)|
|[Bzizma Stlitlitlix](/rpg/inwazja/opowiesci/karty-postaci/9999-bzizma-stlitlitlix.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Balbina Wróblewska](/rpg/inwazja/opowiesci/karty-postaci/9999-balbina-wroblewska.html)|1|[170620](/rpg/inwazja/opowiesci/konspekty/170620-pulapka-na-luksje.html)|
|[Balbina Gniewoń](/rpg/inwazja/opowiesci/karty-postaci/9999-balbina-gniewon.html)|1|[161103](/rpg/inwazja/opowiesci/konspekty/161103-egzorcyzmy-w-zonkiborze.html)|
|[Aurel Czarko](/rpg/inwazja/opowiesci/karty-postaci/1709-aurel-czarko.html)|1|[161115](/rpg/inwazja/opowiesci/konspekty/161115-uciekla-do-femisatanistek.html)|
|[Arazille](/rpg/inwazja/opowiesci/karty-postaci/9999-arazille.html)|1|[161220](/rpg/inwazja/opowiesci/konspekty/161220-zniszczenie-posagu-arazille.html)|
|[Antoni Bieguś](/rpg/inwazja/opowiesci/karty-postaci/9999-antoni-biegus.html)|1|[170620](/rpg/inwazja/opowiesci/konspekty/170620-pulapka-na-luksje.html)|
|[Anton Jesiotr](/rpg/inwazja/opowiesci/karty-postaci/9999-anton-jesiotr.html)|1|[161018](/rpg/inwazja/opowiesci/konspekty/161018-ballada-o-duszy-ognistej.html)|
|[Alina Bednarz](/rpg/inwazja/opowiesci/karty-postaci/1709-alina-bednarz.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Albert Czapkuś](/rpg/inwazja/opowiesci/karty-postaci/9999-albert-czapkus.html)|1|[161110](/rpg/inwazja/opowiesci/konspekty/161110-succubus-mysli-ze-uciekl.html)|

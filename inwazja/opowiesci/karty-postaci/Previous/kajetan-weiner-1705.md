---
layout: inwazja-karta-postaci
title:  "Kajetan Weiner"
categories: profile
guild: "Niezrzeszony"
type: NPC
---

# {{ page.title }}

## Postać

### Motywacje (do czego dąży)

* **samotny wilk**: woli działać na własną rękę

### Zachowania (jaka jest)

* **ambicja**: cytat_lub_opis_jak_to_rozumieć
* **pragmatyczny**: lepszy żywy terminus niż martwy bohater
* **łagodny i przyjacielski**: w tej pracy spotyka się mnóstwo potworów, nie muszę być jednym z nich

### Specjalizacje

* **łowca potworów** (terminus)
* **gawędziarz i bard** (historie i legendy)
* **magia imprezowa** (astralika)
* **inspiracja**
* **magiczne pułapki**
* **wspomaganie walki łukiem**
* **przekształcanie i adaptacja artefaktów**
* **badanie artefaktów**

### Umiejętności

* **terminus**
* **wszystko związane z łukiem**
* **średniowieczny rycerz**
* **artefakty**
* **historie i legendy**
* **astralika**
* **medyk polowy**
* **archeolog-hobbysta**
* **organizator imprez**
* **rekonstrukcje średniowiecza**

### Cechy

|  Social Aggressive |  Social Devious |  Social Friendly |  Knowledge |  Craft |  Supernatural |  Fortitude |  Nimbleness |
|:==================:|:===============:|:================:|:==========:|:======:|:=============:|:==========:|:===========:|
|       -1           |        -1       |        +1        |     +1     |   -1   |      +1       |     +1     |     -1      |

### Specjalne
* **brak**
* **nazwa_specjalnej_własności**: opis_własności_lub_link

## Magia

### Szkoły magiczne

* **magia mentalna**: opis
* **astralika**: opis
* **kataliza**: opis

### Zaklęcia statyczne

* **nazwa_zaklęcia**: opis_zaklęcia_lub_link
* **nazwa_zaklęcia**: opis_zaklęcia_lub_link

## Zasoby i otoczenie

### Powiązane frakcje

* nazwa_frakcji_jak_ma_to_z_linkiem

### Kogo zna

* **członek niewielkiego kręgu rodu Weiner**
* **Rycerze Iglicy** (magowie 'średniowieczni')

### Co ma do dyspozycji:

* **przygotowany łuk z dedykowaną amunicją**
* **"knight self-activator package"**
* **różnorodne, adaptowalne artefakty bojowe**

### Surowce

* **Wartość**: 1/2/3
* **Pochodzenie**: dlaczego_1_2_lub_3_i_jak_zarabia

# Opis

An ambitious terminus who acts as if he was far younger than he really is (while he is 39 years old). Although kind of clumsy, he has a sharp mind and a sharp claw. Tends to daydream a bit, he has a lot of ambition of becoming a powerful knight and serving others for the greater good; even if not one of the most talented magi, he is hard-working and is quite good in assessing the difficulty of the situation. He is in good standing in his family and had access to some artifacts and tools of power. A very friendly and likable person; does not see humans as his equals, but does not use them as a resource either. He did not choose the guild yet; wants to make a name for himself and find the guild which will help his family the most. Has an ax to grind with Diakon family because of an embarrassing past event while he was trying to analyze an artifact from them. A friend of a lovely Maus lady; always says that the guild does not determine the personality of a mage and the looks at magi as individuals, not as collective “oh you belong to this bloodline” stuff.

Quite notorious for his love of archery. He made his own bow (not too good, mind you) and it is his favourite weapon of choice (not too accurate, mind you).

### Koncept

Wysokopoziomowy koncept postaci, 1-2 zdania.

### Motto

"Tekst"

# Historia

---
layout: inwazja-karta-postaci
categories: profile
factions: "KADEM"
type: "NPC"
title: "Karolina Kupiec"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **hedonistka**: "Jeżeli to sprawia ci przyjemność, czemu chcesz sobie tego odmawiać? Przyjemność jest celem, najwyższym."
* **szanuję każde poglądy**: "Bez dobrowolności prawdziwa przyjemność jest nieosiągalna. Nic na siłę, nic wbrew Tobie. Szanuję każde poglądy."
* **kolektywistka**: "Własność prywatna jest nieefektywnym przeżytkiem. Jeśli czegoś potrzebujesz, korzystaj. Ja zrobię to samo."

### Zachowania (jaka jest)

* **pracowita**: "Chcę zostawić coś po sobie. Społeczność, artefakty, badania. Chcę, by po mnie zostało coś dającego radość innym."
* **hojna**: "Co warte są środki, jeżeli trzymam je w sejfie jak smok? Wszystko odzyskam jak będę potrzebowała; jak Ty czegoś potrzebujesz, po prostu poproś"
* **pomocna**: "Niezależnie od okoliczności, warto pomagać. Dobre uczynki wracają. Każdy ma jakąś historię w którą warto zainwestować."
* **anty-ból**: Karolina unika zadawania bólu i cierpienia. Bardzo ją takie podejście drażni. Ona wola unieszkodliwiać przyjemnością i spokojem

### Specjalizacje

* **hedonistyczne środki psychoaktywne**: praca życia Karoliny. Unieszkodliwia przyjemnością i jest awatarem sprawiania, by inni czuli się dobrze.
* **unieszkodliwianie przeciwnika**: Karolina nie lubi robić krzywdy. Ale nigdy nie ma nic przeciwko sprawieniu, by przeciwnik uśmiechał się z rozkoszą, gdy go aresztuje
* **ukojenie**: Karolina jest szczególnie dobra w sprowadzaniu ukojenia, pocieszaniu i wygaszaniu innych, czy to magów, ludzi czy viciniusów i niezależnie od metody
* **kalibracja środków do biotypu**: Karolina jest absolutnym ekspertem by dopasować odpowiednie środki pod kątem biotypu celu przy minimalnych ilościach efektów ubocznych
* **nastroje**: mistrzyni wyczuwania nastrojów i czarodziejka astraliki, Karolina doskonale wie co należy zrobić, by wszystko było tak, jak należy
* **alchemia**: Karolina transformuje swoje trucizny i narkotyki w amunicję do swojej dalekosiężnej broni, by móc unieszkodliwiać przeciwników na zasięg. Plus inne rzeczy ;-).

### Umiejętności

* **terminus - broń palna**: jeśli Karolina już musi walczyć, niech to będzie broń palna (najlepiej z jej własnymi miksturami)
* **chemik (środki psychoaktywne)**: specjalizuje się w rzeczach działających na ludzki / magoviciniuski organizm
* **charyzmatyczna mówczyni**: charyzmatyczna i zapalona o swoją sprawę Karolina porywa za sobą innych, nawet gdy bardzo, bardzo nie powinna
* **herbalistka**: druga z uczennic Weroniki Seton, także zajmuje się jej ogrodami. Acz troszkę mniej; oddała dowodzenie nad ogrodami Inferni.
* **trucizny, toksyny, narkotyki...**: klucz programu ;-). Karolina jest ekspertem od tworzenia różnego rodzaju środków tego typu, zwłaszcza z bytów roślinnych
* **naukowiec: organizm maga**: Karolina spędza bardzo, bardzo dużo czasu na badaniach ciał magów i viciniusów pochodzenia ludzkiego. Nie będąc lekarzem, ma inne talenty
* **podstawy medycyny**: lekarzem nie będzie nigdy. Jednak jeśli nie ma pod ręką nikogo lepszego...

### Cechy:

|  Social Aggressive |  Social Devious |  Social Friendly |  Knowledge |  Craft |  Supernatural |  Fortitude |  Nimbleness |
|:==================:|:===============:|:================:|:==========:|:======:|:=============:|:==========:|:===========:|
|       -1           |        -1       |        +1        |     +1     |   +1   |      +1       |     -1     |     -1      |

### Specjalne

* kotwica pryzmatyczna: +2 do przesunięcia Pryzmatu na hedonizm i przyjemność

## Magia

### Szkoły magiczne
* **biomantka**: druga połowa wpływania na organizm maga przy użyciu swoich środków psychoaktywnych
* **astralika**
* **technomantka broni**: podczas krótkiego pobytu na Instytucie Technomancji Karolina nauczyła się konstruować własną broń by móc być terminusem niebojowym

### Zaklęcia statyczne

* Sprowadzenie Błogości: cel wpada w przyjemny półtrans na pewien czas.
* Alchemiczna Amunicja: zmienia eliksir alchemiczny w pocisk, który następnie eksploduje w formie gazowej i aplikuje efekt eliksiru dla tych, co go wdychają

## Zasoby i otoczenie

### Powiązane frakcje

* KADEM

### Kogo zna

* **znana w kręgach młodych magów różnych gildii**: nie ma imprezy bez Karoliny i jej środków ;-). Jest ona dość znana jako "bezpieczny diler"... pozytywnie i negatywnie.
* **grupa fanów i "wyznawców" wśród magów i ludzi**: z uwagi na podejście Karoliny ma ona grupę fanów wśród osób, które ją popierają... acz boją się powiedzieć to publicznie.
* **luźne kontakty wśród dilerów**: różni dilerzy mają świadomość istnienia Karoliny i jej specyfiki. Nie jest najbardziej lubiana, acz stoi za nią KADEM... lepiej pomóc. Zwłaszcza, że jak coś pójdzie nie tak, pomoże nawet randomowej biednej duszy normalnie handlującej narkotykami...
* **Infernia Diakon**: ze wszystkich magów KADEMu Karolina i Infernia mają straszny sojusz - Infernia dostarcza składniki i testuje eksperymenty Karoliny...

### Co ma do dyspozycji:

* **nie uzależniające hedonistyczne środki psychoaktywne**: miłość i dzieło życia Karoliny... do tego kolekcjonuje różne warianty dla różnego biotypu magów i viciniusów.
* **dobrej klasy broń i pancerz KADEMu**: Karolina nie jest najlepszym terminusem, ale KADEM próbuje kompensować używając dobrego sprzętu
* **małe przysługi**: wielu magów w różnych (zwykle niezbyt elitarnych) kręgach jest jej winna małe przysługi z uwagi na jej podejście i próby rozwiązywania cudzych problemów
* **pistolet alchemiotyczny**: bardzo celny pistolet umożliwiający jej na strzelanie alchemicznymi miksturami na daleki zasięg; jej ulubiona broń do rozprzestrzeniania narkotyków w formie gazowej

### Surowce

* **Wartość**: 1
* **Pochodzenie**: Nie dba o swoje surowce, wychodzi z założenia, że komunalnie każdy może korzystać

# Opis

There are many different Magi with various motivations. The same applies to terminus. Some terminus fight for peace. Some other fight for their side. Caroline? She fights for good feelings and overall pleasure. She calls herself a hedonistic terminus fighting for overall good mood.

She used to be a Millennium mage – she thought that Millennium would be the Guild most suitable to her personality. However, she wasn't able to cope with kraloth and with Mafia-like structure of the Guild. Although she still values and respects Magi of Millennium, she is simply disaligned with the goals of the Guild.

Caroline has managed to enter KADEM, which surprised a lot of Magi – she simply is neither the most powerful mage around nor the most skilled in any particular area. Yet, Veronica Seton took her in, personally, and made Caroline Veronica's assistant. 

One of the stronger assets of Caroline is the fact that her personality allows her to thrive in the prismatic world of Daemonica; this means she is not at a disadvantage in dual reality of KADEM and no matter what happens - she is an asset. Maybe not the strongest asset, but an asset nevertheless.

A friendly terminus who tries to keep everyone's morale high and who helpfully tries to solve everyone's problems, she is generally liked in her new guild.

Caroline specializes in 'drugs'. Or rather, 'psychoactive vectors of hedonism'. She is a valiant researcher and crafter of different things which give pleasure and sooth the pain of existence. Quite often, she works with Warmaster; he is the dark emo brooding one and she is the flowery lollipop giving away her resources to make everyone around happy.

She may not be the best terminus around, but she is decent. Can do her stuff. She identifies herself not as a terminus but as a "happiness vector".

As a terminus, she specializes in disabling and non-lethal elimination of her enemies. If needed, she will simply shoot.

## Motto

"Nie martw się, weź cukierka. Na pewno poczujesz się lepiej :-)"

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|170104|wesoła terminuska dostarczająca narko... er, 'hedonistyczne środki psychoaktywne' które nie uzależniają. Nie handluje, rozdaje.|[Spalone generatory pryzmatyczne](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html)|10/08/08|10/08/09|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170707|dla której essir jest sposobem by wnieść ekstazę i przyjemność do Zaćmionego Serca. Główna alchemiczka zespołu. Też fortyfikuje bazę ;-).|[Biznes pośród niesnasek](/rpg/inwazja/opowiesci/konspekty/170707-biznes-posrod-niesnasek.html)|10/03/06|10/03/08|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170115|wykryła, że sok jabłkowy (z domieszkami) jest dokładnie tym i niczym więcej. Zresztą, jest to całkiem smaczny sok.|[Klub Dare Shiver](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|10/02/22|10/02/25|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170111|zafascynowana narkotykiem Urszuli, przygotowała hedonistyczny neuronull dla Pawła (by dać Franciszkowi) po czym przygotowała na to samo antidotum dla Silurii. Bez jednej refleksji.|[EIS na kozetce](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|10/02/18|10/02/21|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170108|pokazała różnicę między NIĄ a DILERAMI. Terminuska o dobrym sercu i dużych umiejętnościach badawczych narkotyków.|[Samotna w świecie magow](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html)|10/02/12|10/02/16|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161218|terminuska KADEMu na Instytucie Transorganiki. Pomagała Warmasterowi w tworzeniu nietypowego glashunda. Poraniona Pryzmatem.|[Zazdrość Warmastera](/rpg/inwazja/opowiesci/konspekty/161218-zazdrosc-warmastera.html)|10/02/01|10/02/03|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-siluria-diakon.html)|6|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html), [170707](/rpg/inwazja/opowiesci/konspekty/170707-biznes-posrod-niesnasek.html), [170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html), [170108](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html), [161218](/rpg/inwazja/opowiesci/konspekty/161218-zazdrosc-warmastera.html)|
|[Bogumił Rojowiec](/rpg/inwazja/opowiesci/karty-postaci/9999-bogumil-rojowiec.html)|4|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html), [170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html), [161218](/rpg/inwazja/opowiesci/konspekty/161218-zazdrosc-warmastera.html)|
|[Quasar](/rpg/inwazja/opowiesci/karty-postaci/9999-quasar.html)|3|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html), [161218](/rpg/inwazja/opowiesci/konspekty/161218-zazdrosc-warmastera.html)|
|[Ignat Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-ignat-zajcew.html)|3|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html), [170108](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html)|
|[Warmaster](/rpg/inwazja/opowiesci/karty-postaci/9999-warmaster.html)|2|[170707](/rpg/inwazja/opowiesci/konspekty/170707-biznes-posrod-niesnasek.html), [161218](/rpg/inwazja/opowiesci/konspekty/161218-zazdrosc-warmastera.html)|
|[Urszula Murczyk](/rpg/inwazja/opowiesci/karty-postaci/1705-urszula-murczyk.html)|2|[170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html), [170108](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html)|
|[Norbert Sonet](/rpg/inwazja/opowiesci/karty-postaci/9999-norbert-sonet.html)|2|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html), [161218](/rpg/inwazja/opowiesci/konspekty/161218-zazdrosc-warmastera.html)|
|[Netheria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1705-netheria-diakon.html)|2|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Kornelia Kartel](/rpg/inwazja/opowiesci/karty-postaci/9999-kornelia-kartel.html)|2|[170707](/rpg/inwazja/opowiesci/konspekty/170707-biznes-posrod-niesnasek.html), [170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|[Judyta Maus](/rpg/inwazja/opowiesci/karty-postaci/1705-judyta-maus.html)|2|[170707](/rpg/inwazja/opowiesci/konspekty/170707-biznes-posrod-niesnasek.html), [170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|[Joachim Kartel](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-kartel.html)|2|[170707](/rpg/inwazja/opowiesci/konspekty/170707-biznes-posrod-niesnasek.html), [170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|[Infernia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-infernia-diakon.html)|2|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Ilona Amant](/rpg/inwazja/opowiesci/karty-postaci/9999-ilona-amant.html)|2|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Franciszek Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-maus.html)|2|[170707](/rpg/inwazja/opowiesci/konspekty/170707-biznes-posrod-niesnasek.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Eis](/rpg/inwazja/opowiesci/karty-postaci/9999-eis.html)|2|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Balrog Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-balrog-bankierz.html)|2|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Wioletta Lemona-Chang](/rpg/inwazja/opowiesci/karty-postaci/9999-wioletta-lemona-chang.html)|1|[170108](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html)|
|[Whisperwind](/rpg/inwazja/opowiesci/karty-postaci/9999-whisperwind.html)|1|[161218](/rpg/inwazja/opowiesci/konspekty/161218-zazdrosc-warmastera.html)|
|[Supernowa Diakon](/rpg/inwazja/opowiesci/karty-postaci/1705-supernowa-diakon.html)|1|[170707](/rpg/inwazja/opowiesci/konspekty/170707-biznes-posrod-niesnasek.html)|
|[Rukoliusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-rukoliusz-bankierz.html)|1|[170108](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html)|
|[Radosław Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-radoslaw-weiner.html)|1|[161218](/rpg/inwazja/opowiesci/konspekty/161218-zazdrosc-warmastera.html)|
|[Paweł Sępiak](/rpg/inwazja/opowiesci/karty-postaci/1705-pawel-sepiak.html)|1|[170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Oliwier Bonwant](/rpg/inwazja/opowiesci/karty-postaci/9999-oliwier-bonwant.html)|1|[170108](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html)|
|[Oktawian Maus](/rpg/inwazja/opowiesci/karty-postaci/1705-oktawian-maus.html)|1|[170707](/rpg/inwazja/opowiesci/konspekty/170707-biznes-posrod-niesnasek.html)|
|[Mordecja Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-mordecja-diakon.html)|1|[161218](/rpg/inwazja/opowiesci/konspekty/161218-zazdrosc-warmastera.html)|
|[Mikado Diakon](/rpg/inwazja/opowiesci/karty-postaci/1705-mikado-diakon.html)|1|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html)|
|[Melodia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-melodia-diakon.html)|1|[170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Marianna Sowińska](/rpg/inwazja/opowiesci/karty-postaci/1705-marianna-sowinska.html)|1|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html)|
|[Marian Łajdak](/rpg/inwazja/opowiesci/karty-postaci/1705-marian-lajdak.html)|1|[170108](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html)|
|[Marcelin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1707-marcelin-blakenbauer.html)|1|[170707](/rpg/inwazja/opowiesci/konspekty/170707-biznes-posrod-niesnasek.html)|
|[Maja Błyszczyk](/rpg/inwazja/opowiesci/karty-postaci/9999-maja-blyszczyk.html)|1|[161218](/rpg/inwazja/opowiesci/konspekty/161218-zazdrosc-warmastera.html)|
|[Jolanta Sowińska](/rpg/inwazja/opowiesci/karty-postaci/1705-jolanta-sowinska.html)|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|[Joachim Kopiec](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-kopiec.html)|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|[Jan Wątły](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-watly.html)|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|[Henryk Siwiecki](/rpg/inwazja/opowiesci/karty-postaci/1707-henryk-siwiecki.html)|1|[170707](/rpg/inwazja/opowiesci/konspekty/170707-biznes-posrod-niesnasek.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1707-hektor-blakenbauer.html)|1|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html)|
|[Ferdynand Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-ferdynand-maus.html)|1|[170707](/rpg/inwazja/opowiesci/konspekty/170707-biznes-posrod-niesnasek.html)|
|[Elizawieta Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-elizawieta-zajcew.html)|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|[Anna Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-myszeczka.html)|1|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html)|
|[Anna Kozak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kozak.html)|1|[170108](/rpg/inwazja/opowiesci/konspekty/170108-samotna-w-swiecie-magow.html)|
|[Andżelika Leszczyńska](/rpg/inwazja/opowiesci/karty-postaci/1709-andzelika-leszczynska.html)|1|[161218](/rpg/inwazja/opowiesci/konspekty/161218-zazdrosc-warmastera.html)|
|[Adrian Kropiak](/rpg/inwazja/opowiesci/karty-postaci/9999-adrian-kropiak.html)|1|[161218](/rpg/inwazja/opowiesci/konspekty/161218-zazdrosc-warmastera.html)|
|[Adonis Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1709-adonis-sowinski.html)|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|

---
layout: inwazja-karta-postaci
title:  "Andżelika Leszczyńska"
categories: profile
guild: "KADEM"
type: NPC
---

# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **praworządna**: "zgodnie z artykułem 44 Postanowień Radomskich nie zrobimy tego w TEN sposób"
* **pragnienie ładu**: "każdego dnia entropia odrobinę wygrywa... i każdego dnia ją troszkę odpychamy - cywilizacją, nauką, kulturą."
* **wiedza dla wiedzy**: "wiedziałeś, że Faza Esuriit potrafi pożreć królika w 11 minut? Nieprzydatne? Ale ciekawe..."

### Zachowania (jaka jest)

* **ostrożna**: "przy moim szczęściu to zaraz wyleci w powie- NIE ŻONGLUJ TYM!"
* **lękliwa**: "czy sobie już poszli? Ja naprawdę nie nadaję się do tego..."

### Specjalizacja:

* **analiza bytów magicznych**: niezależnie co to jest, Andżelika jest w stanie się temu przyjrzeć i określić tego naturę
* **adaptująca magia defensywna**: gdy Andżelika już wie z czym ma do czynienia, jest w stanie tak sformować kontrzaklęcie, by zablokować zbliżający się atak
* **szybkie przeszukiwanie danych**: Andżelika jest w stanie szybko znajdować dane ze źródeł i integrować je w logiczną całość
* **kataliza zaklęć**: Andżelika potrafi bardzo płynnie, szybko i z niezwykłą intuicją pracować nad zaklęciami - swoimi i cudzymi
* **aury magiczne**
* **magia detekcyjna**

### Umiejętności

* **dokumenty**: Mało kto tak płynnie porusza się w różnych archiwach, dokumentach i danych szczątkowych jak ona
* **biurokracja**: potrafi poruszać się w biurokratycznych systemach, urzędach itp.
* **administratorka**: nie wiesz jak czymś zarządzać czy zapewnić tego prawidłowe działanie? Andżelika zrobi to dla Ciebie z przyjemnością.
* **prawo magów**: wybroni się czy zaatakuje; po KADEMowych perypetiach całkiem skutecznie radzi sobie z prawem a zwłaszcza jego unikaniem
* **arystokratka Świecy**: Andżelika nie jest arystokratką, ale wie jak powinna się jako arystokratka zachowywać - dostała gruntowne przeszkolenie
* **badania i analiza**: Andżelika to stworzenie archiwów i laboratoriów; nigdy nie traci cierpliwości i chęci działania
* **historia magów**: Andżelika interesuje się starymi zaklęciami, artefaktami i ogólnie historią magów; była nawet w kółku zainteresowań w Świecy
* **naukowiec KADEMu**: wytrwała badaczka i metodyczna analityczka, trafiają do niej żmudne i skomplikowane tematy. Lubi z nimi pracować.
* **plotki**: niektórzy żartują, że Andżelika jest mistrzynią szpiegów KADEMu - dostaje dane z różnych źródeł (i gildii) i umie je skutecznie przetwarzać



### Cechy:

|  Social Aggressive |  Social Devious |  Social Friendly |  Knowledge |  Craft |  Supernatural |  Fortitude |  Nimbleness |
|:==================:|:===============:|:================:|:==========:|:======:|:=============:|:==========:|:===========:|
|       -1           |         0       |         0        |     +1     |    0   |      +1       |      0     |     -1      |

### Specjalne

* kocha uczyć się różnych dziwnych rzeczy; 20% szansy że zna odpowiedź na super-specjalistyczne pytanie na które nikt normalny odpowiedzi by nie znał.

## Magia

### Szkoły magiczne
* **kataliza**: katalistka bardzo dobrej klasy, Andżelika specjalizuje się w zaklęciach i konstrukcjach magów - ale zna się też na obszarach ogólnych 
* **infomancja**

### Zaklęcia statyczne

* nazwa : link
* nazwa : link

## Zasoby i otoczenie

### Powiązane frakcje

* KADEM
* Klub Historyczny Szczegół
* Klub Hipotetyczny Tesseract

### Kogo zna

* **opinia mistrzyni trivii**: Nikt nie chce z Andżeliką grać w ciekawostki; powszechnie wiadomo, że po prostu się nie da
* **opinia uczciwej i praworządnej**: Niezależnie od gildii i grupy, Andżelika jest uważana za praworządną, uczciwą i pomocną. Po prostu w zasadzie niegroźna ;-).
* **różni magowie Świecy**: Andżelika ma kontakty w różnych frakcjach Świecy; ma dostęp do wiedzy, do której inni często nie mają
* **Julian Pszczelak**: Andżelika poznała go na kółku historycznym w Świecy. Potem ich losy się rozdzieliły; wciąż się lubią
* **Klub Historyczny Szczegół**: niewielki klub historyczny skupiony dookoła badaczy przeszłości, archeologów...
* **Klub Hipotetyczny Tesseract**: śląski klub bardzo eksperymentalnej magii i "co by było gdyby" kiedyś założony przez Martę Szysznicką; międzygildiowe

### Co ma do dyspozycji:

* **księgozbiór kuriozów**: Andżelika kolekcjonuje rzadkie książki i nietypowe źródła wiedzy; zwłaszcza w dziedzinie 
* **wsparcie prawników**: Po perypetiach z Kubą Urbankiem Andżelika znalazła sobie wsparcia kompetentnych prawników różnego rodzaju
* **KADEMowy sprzęt analityczny**: świetne laboratoria KADEMu umożliwiają Andżelice przeprowadzanie świetnych badań i analiz

### Surowce

* **Wartość**: 2
* **Pochodzenie**: Wsparcie z KADEMu i prace badawcze dla różnych stron

# Opis

Angelica used to be a Silver Candle mage. She comes from a small family having aristocratic tendencies and aspirations – not very aristocrat but aristocrat enough. When she got sent to KADEM she could not live with it for a long time; family indoctrination and the Candle indoctrination was simply too strong. However, she was given as a mentee to Jakub Urbanek - an event which could traumatize a person much stronger than she ever was.

Jacob was devastated. She was no battle mage, she was not courageous... she was nothing interesting. But she was a good administrator. So he arranged some stuff and started training her (by proxy) in archive stuff, document manipulation, data aggregation... Basically, perfect bureaucrat and an apt researcher. In terms of magic Angelica has specialized herself in detection magic and some parts of adaptive defensive magic (with him nearby it was kind of natural...).

Currently, she still is a gray mouse hiding behind the bookshelves. But she does know most of strange law intricacies which let her mentor go away with crimes, she knows where the candle does things correct and wrong and she is good in aggregating information and spread information. In short, intelligence warfare.

Think of her as of an activist hacker not using computers. She loves learning and usually knows things obscure and unusual; mostly stuff you would use in crosswords or trivia contests.

## Motto

"Porządek, wiedza i metodyczna konsekwencja są podstawą tego, by uciec przed entropią."

# Historia
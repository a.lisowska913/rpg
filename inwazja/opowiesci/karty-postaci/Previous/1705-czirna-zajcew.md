---
layout: inwazja-karta-postaci
categories: profile
factions: "Niezrzeszeni"
type: "NPC"
title: "Czirna Zajcew"
---
# {{ page.title }}

## Postać

### Motywacje (do czego dąży)

* **impuls_co_chce_osiągnąć**: cytat_lub_opis_jak_to_rozumieć

### Zachowania (jaka jest)

* **lojalna**: cytat_lub_opis_jak_to_rozumieć
* **zazdrośnica**: cytat_lub_opis_jak_to_rozumieć
* **samotnik**: cytat_lub_opis_jak_to_rozumieć

### Specjalizacje

* **iluzje**: opis

### Umiejętności

* **bardka Zajcewów**: opis
* **wywiadowca**: opis

### Cechy

|  Social Aggressive |  Social Devious |  Social Friendly |  Knowledge |  Craft |  Supernatural |  Fortitude |  Nimbleness |
|:==================:|:===============:|:================:|:==========:|:======:|:=============:|:==========:|:===========:|
|        0           |         0       |         0        |      0     |    0   |       0       |      0     |      0      |

### Specjalne
* **brak**
* **nazwa_specjalnej_własności**: opis_własności_lub_link

## Magia

### Szkoły magiczne

* **magia zmysłów**: opis
* **magia żywiołów**: opis
* **astralika**: opis
* **magia mentalna**: opis

### Zaklęcia statyczne

* **nazwa_zaklęcia**: opis_zaklęcia_lub_link
* **nazwa_zaklęcia**: opis_zaklęcia_lub_link

## Zasoby i otoczenie

### Powiązane frakcje

* nazwa_frakcji_jak_ma_to_z_linkiem

### Kogo zna

* **Romeo Diakon**: opis
* **grupa Diakonów**: opis

### Co ma do dyspozycji:

* **grupę oddanych ludzi**: opis

### Surowce

* **Wartość**: 1/2/3
* **Pochodzenie**: dlaczego_1_2_lub_3_i_jak_zarabia

# Opis

What if there existed something like a Siberian bard, a perfect illusionist able to control the prismatic powers as well who would be able to kill people using ice magic? And what if she was actually a noncombatant mage, who does not feel being a part of humanity as much as a part of the world of illusions? Especially with power of Siberia, when whatever you think which is real becomes real?

Fast-forward and you have her – Czirna Zajcew. A Siberian illusionist specializing in the prismatic powers, who uses illusions to amplify the group to make her vision become a reality by the prism.

This is a power of a God – or so she thought. And then she met with cold, harsh reality which forced her to into seclusion. Until now.

### Koncept

Wysokopoziomowy koncept postaci, 1-2 zdania.

### Motto

"Tekst"

# Historia:



## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|151212|która była architektem rozbicia między Diakonami i Blakenbauerami, pośrednio bardzo po* magając Arazille.|[Antyporadnik wędkarza Arazille](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html)|10/06/19|10/06/20|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160927|specjalistka od pryzmatu i manipulacji nim. Pomogła dopracować imprezę w szczegółach.|[Desperacka bateria dla Aegis](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|10/06/16|10/06/21|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|151124|Romeo o niej powiedział, że zaproponowała mu tą konkretną strategię trafienia do serca Hektora... najpewniej ma nieczyste motywy.|[Odbudowa relacji konfliktem](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html)|10/06/13|10/06/14|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|Siluria Diakon|2|[151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html), [151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html)|
|Romeo Diakon|2|[151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html), [151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html)|
|Patrycja Krowiowska|2|[151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html), [151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html)|
|Mojra|2|[151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html), [151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html)|
|Hektor Blakenbauer|2|[151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html), [151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html)|
|Edwin Blakenbauer|2|[151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html), [151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html)|
|Dionizy Kret|2|[151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html), [151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html)|
|Alina Bednarz|2|[151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html), [151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html)|
|Zenobia Bankierz|1|[160927](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|
|Wanda Ketran|1|[151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html)|
|Vladlena Zajcew|1|[151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html)|
|Tatiana Zajcew|1|[151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html)|
|Szczepan Sowiński|1|[160927](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|
|Rafał Kniaź|1|[160927](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|
|Ozydiusz Bankierz|1|[151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html)|
|Otton Blakenbauer|1|[151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html)|
|Oddział Zeta|1|[151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html)|
|Margaret Blakenbauer|1|[151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html)|
|Marcelin Blakenbauer|1|[151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html)|
|Kira Zajcew|1|[160927](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|
|Katarzyna Kotek|1|[160927](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|
|Jurij Zajcew|1|[151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html)|
|Ignat Zajcew|1|[160927](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|
|Himechan|1|[151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html)|
|GS Aegis 0003|1|[160927](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|
|Franciszek Baranowski|1|[160927](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|
|Felicja Strączek|1|[160927](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|
|Erebos|1|[151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html)|
|Diana Weiner|1|[151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html)|
|Borys Kumin|1|[151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html)|
|Aurel Czarko|1|[160927](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|
|Arazille|1|[151212](/rpg/inwazja/opowiesci/konspekty/151212-antyporadnik-wedkarza-arazille.html)|
|Antoni Chlebak|1|[160927](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|
|Andrzej Bankierz|1|[160927](/rpg/inwazja/opowiesci/konspekty/160927-desperacka-bateria-dla-aegis.html)|

---
layout: inwazja-karta-postaci
title:  "Siluria Diakon"
categories: kadem, profile, postac_gracza
---
# {{ page.title }}

## Impulsy (co motywuje / napędza postać):

### Strategiczne
- **tyranka tyranów**: "im większym jesteś tyranem, tym bardziej będziesz jej podległy"
- **zawsze warto nawiązać nowe znajomości**: "nigdy nie wiesz, kiedy ktoś okaże się pomocny"
- **dominatorka**: "przejąć kontrolę w rękawiczkach"

### Taktyczne

- **rozbrajająco urocza**: "sprawić, by uważano ją za niegroźną" 
- **maskotka wszystkich**: "sprawić, by wszyscy ją lubili"
- **prawdziwy Diakon**: "zadowolić wszystkich"
- **prawdziwy Diakon**: "jak da się przelecieć, da się z tym pracować"
- **śliska jak piskorz**: "nie dać się złapać ani przyłapać"
- **bezsensowne okrucieństwo jest karalne**: "tak się nie godzi, nawet zwierzęta tak nie robią"
- **po co walczyć, skoro możemy się dobrze bawić?**:

## Co umie (co postać potrafi):


### Ekspercko (poziom 3):

- dostaję to, o co proszę (maskotka wszystkich)
- oblicze dla każdego (manipulatorka)
- bondage (węzły)
- spajanie zespołu (facylitator)
- zrobisz, co zechcę (uwodzicielka) 

### Zawodowo (poziom 2):

- uwodzicielka
- węzły
- manipulatorka 
- maskotka wszystkich
- corruptor
- life shaper
- technomantka
- polityk
- uczennica KADEMu
- imprezowiczka
- plotki
- facylitator

### Amatorsko (poziom 1):

- walka wręcz

## Inklinacje:

|  Social Aggressive |  Social Devious |  Social Friendly |  Knowledge |  Craft |  Supernatural |  Fortitude |  Nimbleness |
|:==================:|:===============:|:================:|:==========:|:======:|:=============:|:==========:|:===========:|
|       -1           |         +1      |        +1        |      -1    |   -1   |        0      |      0     |     +1      |


## Kieszonkowe
2

## Otoczenie powiązane

### Powiązane frakcje
KADEM
Millenium
Diakonowie

### Kogo zna

#### Doskonale: 3

- adoratorów 

#### Nieźle: 2

- magów z wszystkich ważnych gildii i większości nieważnych 

#### Luźno: 1
  

### Co ma do dyspozycji:

#### Szczególnie dobre: 3

- przysługi u magów KADEMu
- Reputacja osoby, z którą można się dobrze zabawić

#### Ponadprzeciętne: 2

- więzy rodzinne
- przysługi u magów spoza KADEMu
- wyznania łóżkowe

#### Niezłe: 1

- gadżety - prezenty od magów
- szerokie znajomości wśród ludzi i magów


## Magia:

### Magia dynamiczna
- magia uwodzenia
- zaklęcia miłosne
- magia mentalna
- magia urody i kosmetyczna
- magia ubiorów

### Zaklęcia hermetyczne
- **pre-ekstaza Tormentii**: utrzymuje na granicy ekstazy; tuż przed
- **pokój wszelkich przyjemności Diakonów**

## Specjalne:

Diakon czystej krwi; prawdziwy Diakon

## Inne:


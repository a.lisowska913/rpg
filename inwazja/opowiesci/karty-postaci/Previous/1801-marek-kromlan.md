---
layout: inwazja-karta-postaci
categories: profile
factions: "Rusznica Świecy, Srebrna Świeca, Szlachta"
type: "NPC"
owner: "public"
title: "Marek Kromlan"
---
# {{ page.title }}

## Postać

### Motywacje (do czego dąży)

* **Indywidualne**:
    * _Kategorie_: promocja sztuki, eksploracja różnorodności
    * _Aspekty_: promować godną sztukę, NIE wzbogacić się na masówce, obcować z różnorodną sztuką, NIE tylko lekkie i przyjemne wrażenia, odkupić przeszłość w gildii, NIE nie mam za co przepraszać
    * _Opis_: 
* **Społeczne**:
    * _Kategorie_: autonomia, praworządność, antyniewolnictwo
    * _Aspekty_: wolność i odpowiedzialność, NIE tylko wykonuję rozkazy, dąży do praworządności, NIE robię co mi wygodne, zwalczać Korupcję jak kralotyczną, NIE akceptować różnorodność gildii, zwalczać dominację i zniewolenie, NIE akceptować że silniejszy ma więcej praw, wzmocnić Rusznicę Świecy, NIE organizacja to tylko wspólne interesy
    * _Opis_: 
* **Serce**:
    * _Kategorie_: kojenie, kolekcjonowanie
    * _Aspekty_: kojenie przez niszczenie, NIE podżeganie do konfliktów, promowanie różnorodnej sztuki, NIE odrzucanie nieznanego, kolekcjonowanie rzeczy budzących wrażenia, NIE po prostu destrukcja, wybór tego co właściwe, NIE wybór tego co w prawie
    * _Opis_: 
* **Działanie**:
    * _Kategorie_: arystokrata, metodyczny, współdziałanie
    * _Aspekty_: lubi towarzystwo, NIE samotnik na uboczu, uprzejmy i elegancki, NIE mówi co myśli, metodyczny i przemyślany, NIE impulsywny i reaguje na wydarzenia, daje każdemu szansę, NIE ocenia przed poznaniem czynów, dąży do współdziałania, NIE tylko ja wygrywam
    * _Opis_: 

### Umiejętności

* **Terminus**:
    * _Kategorie_: terminus, snajper, rewolwerowiec, dywersant
    * _Aspekty_: pierwsza pomoc, snajper z ukrycia, walka bronią palną (pistolety), egzekucja przez odwrócenie uwagi, unikanie ran przez akrobatykę
    * _Opis_: Marek nie jest najlepszym terminusem w walce bezpośredniej; nie dorównuje np. Izie Łaniewskiej. Za to jeśli nie jest sam i uda się mu odwrócić uwagę przeciwnika, jest niezwykle celnym i niebezpiecznym egzekutorem. Alternatywnie - jeśli uda mu się zadziałać jak snajperowi
* **Taktyk**: 
    * _Kategorie_: taktyk, artylerzysta
    * _Aspekty_: atak w słaby punkt przez kontrolę terenu, ukrycie się przez kontrolę terenu, atak z zaskoczenia przez ukrycie się, obezwładnianie przez pułapkę, straszne zniszczenia przez ukryte bomby, przewidywanie ruchów przeciwnika przez kształtowanie terenu
    * _Opis_: Jedna z najsilniejszych stron Marka. Mistrz wplątywania przeciwników w kłopoty, wymanewrowania ich i przekształcania otoczenia na ich niekorzyść. Tam, gdzie inni magowie Rusznicy preferowali działanie bezpośrednie - on wolał iluzje i pułapki. 
* **Mecenas sztuki**: 
    * _Kategorie_: mecenas sztuki, marketingowiec, dyplomata
    * _Aspekty_: wycena przez doświadczenie, promocja przez kalibrację sygnału do targetu, robienie wrażenia przez opowieści i analizę, dyplomata przez obycie, znajdowanie anomalii u artystów przez różne dzieła sztuki, wabienie celu przez dzieła sztuki
    * _Opis_: Marek zawsze chciał być promotorem i mecenasem sztuki nade wszystko. Elegancki i ponadczasowy, uwielbia samotność i obcowanie ze sztuką, zwłaszcza tą mniej znaną. Nie rozpatruje tego z perspektywy rasy czy bogactwa - tylko wrażeń. Przypadkowo, sprawia to, że jest jednym z najbardziej obytych i eleganckich terminusów w okolicy.

### Silne i słabe strony:

* ****:
    * _Aspekty_: 
    * _Opis_: 

## Magia

### Szkoły magiczne

* **Magia zmysłów**:
    * _Aspekty_: odwracanie uwagi, ukrywanie terenu, maskowanie snajpera, wzmocnienie sztuki
    * _Opis_: 
* **Technomancja**:
    * _Aspekty_: tworzenie pułapek, wysadzanie rzeczy, zdalna kontrola broni, wspomaganie broni, badanie przeciwnika
    * _Opis_: 
* **Magia transportu**:
    * _Aspekty_: 
    * _Opis_: 

### Zaklęcia statyczne

* ****:
    * _Aspekty_: 
    * _Opis_: 

## Zasoby i otoczenie

### Powiązane frakcje

{{ page.factions }}

### Znam

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

### Mam

* **Ceremonialna broń Rusznicy**:
    * _Aspekty_: 
    * _Opis_: Dwa ceremonialne pistolety magitechowe, świetnej klasy. Karabin snajperski, magitechowy. Z tą bronią wiąże się pewna tradycja i pewne wspomnienia - Marek ich nie odrzuci i nie zbezcześci. Są piękne, dobrze utrzymane i pozwalają Markowi czasem przypomnieć sobie, że jest coś, czemu warto nadal walczyć.
* **Mundur magitechowy Tamary Muszkiet**:
    * _Aspekty_: 
    * _Opis_: Marek, sympatyk Szlachty i Tamara, przeciwnik Szlachty. Obu zależało na dobru Świecy. Tamara widząc bratnią duszę jednak stworzyła dlań dedykowany mundur magitechowy, mający łączyć ze sobą elegancję wraz z użytecznością. Marek ceni to sobie jako jeden z najbliższych przedmiotów.
* ****:
    * _Aspekty_: 
    * _Opis_: 

# Opis

### Koncept

Iza Łaniewska + Gabriel Durindal + miłośnik sztuki

### Mapa kreacji

![Mapa postaci](Materials/180399/marek_kromlan.png)

### Motto

""

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|[Cienie procesu Izy](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|pogorszył sobie reputację w Rzecznej Chacie, bo wydarł się na Silurię o Infensę.|Adaptacja kralotyczna|


## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|180311|niezbyt lubiący kogokolwiek terminus; Silurii wyjątkowo nie lubi za Infensę. Sprzyja KADEMowi, ze wszystkich rzeczy. Zrezygnowany.|[Cienie procesu Izy](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|10/12/03|10/12/04|[Adaptacja kralotyczna](/rpg/inwazja/opowiesci/konspekty/kampania-adaptacja-kralotyczna.html)|
|180310|kompetentny terminus który bardziej chce ratować magów i ludzi niż angażować się w walkę z Silurią. Szukał kralotha; Siluria znalazła go pierwsza.|[Kraloth w piwnicy](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|10/11/28|10/12/01|[Adaptacja kralotyczna](/rpg/inwazja/opowiesci/konspekty/kampania-adaptacja-kralotyczna.html)|
|160810|terminus porządkowy o dobrym sercu, który pilnuje by nic się nie stało złego w Technoparku, acz nie dopuszcza Aliny do Adriana Murarza.|[Zaszczepić Adriana Murarza!](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html)|10/07/09|10/07/11|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|3|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html), [180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html), [160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html)|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1801-siluria-diakon.html)|2|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html), [180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Netheria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-netheria-diakon.html)|2|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html), [180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Karolina Kupiec](/rpg/inwazja/opowiesci/karty-postaci/1801-karolina-kupiec.html)|2|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html), [180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Wiktor Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1709-wiktor-sowinski.html)|1|[160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html)|
|[Whisperwind](/rpg/inwazja/opowiesci/karty-postaci/9999-whisperwind.html)|1|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|
|[Warmaster](/rpg/inwazja/opowiesci/karty-postaci/9999-warmaster.html)|1|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|
|[Vladlena Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-vladlena-zjacew.html)|1|[160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html)|
|[Szczepan Przysiadek](/rpg/inwazja/opowiesci/karty-postaci/9999-szczepan-przysiadek.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Roman Bruniewicz](/rpg/inwazja/opowiesci/karty-postaci/9999-roman-bruniewicz.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Paulina Widoczek](/rpg/inwazja/opowiesci/karty-postaci/9999-paulina-widoczek.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Patrycja Krowiowska](/rpg/inwazja/opowiesci/karty-postaci/1709-patrycja-krowiowska.html)|1|[160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html)|
|[Mojra](/rpg/inwazja/opowiesci/karty-postaci/9999-mojra.html)|1|[160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html)|
|[Miranda Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-miranda-maus.html)|1|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|
|[Marian Łajdak](/rpg/inwazja/opowiesci/karty-postaci/1709-marian-lajdak.html)|1|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|
|[Maria Przysiadek](/rpg/inwazja/opowiesci/karty-postaci/9999-maria-przysiadek.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Malia Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-malia-bankierz.html)|1|[160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html)|
|[Lucjan Kopidół](/rpg/inwazja/opowiesci/karty-postaci/9999-lucjan-kopidol.html)|1|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|
|[Krystalia Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-krystalia-diakon.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Klara Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-klara-blakenbauer.html)|1|[160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html)|
|[Karolina Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-karolina-maus.html)|1|[160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html)|
|[Karina Paczulis](/rpg/inwazja/opowiesci/karty-postaci/9999-karina-paczulis.html)|1|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|
|[Judyta Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-judyta-maus.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Infensa Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-infensa-diakon.html)|1|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|
|[Hralglanath](/rpg/inwazja/opowiesci/karty-postaci/9999-hralglanath.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|1|[160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html)|
|[Grzegorz Nocniarz](/rpg/inwazja/opowiesci/karty-postaci/9999-grzegorz-nocniarz.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Franciszek Knur](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-knur.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Estrella Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-estrella-diakon.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Elea Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-elea-maus.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Dionizy Kret](/rpg/inwazja/opowiesci/karty-postaci/1709-dionizy-kret.html)|1|[160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html)|
|[Błażej Falka](/rpg/inwazja/opowiesci/karty-postaci/9999-blazej-falka.html)|1|[180311](/rpg/inwazja/opowiesci/konspekty/180311-cienie-procesu-izy.html)|
|[Bójka Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-bojka-diakon.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Baltazar Sowiński](/rpg/inwazja/opowiesci/karty-postaci/9999-baltazar-sowinski.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Baltazar Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-baltazar-maus.html)|1|[160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html)|
|[Artur Bryś](/rpg/inwazja/opowiesci/karty-postaci/1709-artur-brys.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Alina Bednarz](/rpg/inwazja/opowiesci/karty-postaci/1709-alina-bednarz.html)|1|[160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html)|
|[Adrian Murarz](/rpg/inwazja/opowiesci/karty-postaci/9999-adrian-murarz.html)|1|[160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html)|

---
layout: inwazja-karta-postaci
title:  "Henryk Siwiecki"
categories: profile
guild: "Srebrna Świeca"
type: PC
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

- Prawdziwy wpływ: "chcę mieć faktyczny wpływ na najpotężniejszą organizację magów. Tzn. Albo sprawię, że moja organizacja będzie najpotężniejsza i będę miał na nią wpływ, albo dołączę do innej, na którą również będę miał wpływ"
- Wieksza ewolucja: ciągle lepszy, potężniejszy.
- Ład i porządek: istnieje powód dlaczego istnieje porządek społeczny, bez niego wszystko zaczyna się rozpadać

### Zachowania (jaki jest)

- Moje dobro jest najważniejsze - egoista
- "będzie jak ja chcę"
- "inni widzą tylko moją maskę"
- szara eminencja (to ja mam władzę, pociągam za sznurki)
- Ostrożny
- cel strategiczny jest ważniejszy, mogę przegrać bitwę, ale wygram wojnę

### Specjalizacje
  
- **"to twoj pomysł"** (maniuplator)
- **pryzmat** (astralika)
- **kontrola i dominacja** (mentalna)
- **badanie magii** (kataliza)

### Umiejętności

- **dyplomata**
- **ksiądz**
- **manipulator**
- **egzorcysta**
- **negocjator**
- **psycholog**
- **polityk**

### Cechy

|  Social Aggressive |  Social Devious |  Social Friendly |  Knowledge |  Craft |  Supernatural |  Fortitude |  Nimbleness |
|:==================:|:===============:|:================:|:==========:|:======:|:=============:|:==========:|:===========:|
|       -1           |         +1      |         0        |     +1     |   0    |      0        |      0     |     -1      |

### Specjalne
**brak**

## Magia

### Szkoły magiczne

- astralika
- magia mentalna
- kataliza

### Zaklęcia statyczne

- **echo emocji**

### Surowce
- **Wartość:**: 2
- **Pochodzenie:** Dostaje kasę, bo ksiądz. Premia za egzorcyzm, który się powiódł.

## Zasoby i otoczenie

### Powiązane frakcje

Srebrna Świeca

### Kogo zna

* czlonek sympatyk szlachty
* sława skutecznego egzorcysty
* opinia tego co przynosi podwójne profity
* hierarchowie kosciola
* dziennikarze telewizji Nowina

### Co ma do dyspozycji:

* zaufanie spoleczne (ksiadz)
* oddział kleryków/paladynow wspomagajacych egzorcyzmy
* Kaldwor i jego organizacja
* sprzet egzorcystyczny
* Oczy kosciola

# Opis

### Koncept

Mag egzorcysta, skupiający się na poszerzaniu wpływów i awansie w magicznej drabinie społecznej.

### Motto

"Audi multa, dic pauca"

# Historia
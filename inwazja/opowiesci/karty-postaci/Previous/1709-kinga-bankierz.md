---
layout: inwazja-karta-postaci
categories: profile
factions: "Dare Shiver, Srebrna Świeca"
type: "PC"
owner: "kić"
title: "Kinga Bankierz"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **FIL: Nie ma czegoś takiego, jak szczęście**: 
    * _Aspekty_: bądź gotowy na wszystko, spodziewaj się niespodziewanego
    * _Opis_: 
* **BÓL: Paranoja**: 
    * _Aspekty_: nigdy nie być bezbronna, iść zawsze przygotowana, nie ufać nikomu
    * _Opis_: bezsilna, bezradna, porzucona... nigdy taka nie będzie
* **MRZ: Sława i władza**: 
    * _Aspekty_: chce być rozpoznawana, nie chce być na świeczniku, chce szacunku za kompetencję, chce bezpieczeństwa
    * _Opis_: 
* **MRZ: Silna Świeca gwarantem spokoju i bezpieczeństwa**: 
    * _Aspekty_: Świeca powinna być pozytywną siłą, nie toleruje korupcji 
    * _Opis_: 	

### Umiejętności

* **Oficer**: 
    * _Aspekty_: taktyka, kontrola pola walki, dywersja, odwrócenie uwagi, planowanie starcia, pułapki
    * _Opis_: 
* **Performer**: 
    * _Aspekty_: iluzje, odwrócenie uwagi
    * _Opis_: 
* **Terminus**: 
    * _Aspekty_: uczeń, wyciąganie zeznań, zachowanie pozorów,  
    * _Opis_: 
    
### Silne i słabe strony:

* **Szybka, zwinna, nieuchwytna**: 
    * _Aspekty_: trudno ją złapać, słabe defensywy
    * _Opis_: 

## Magia

### Szkoły magiczne

* **Magia zmysłów**:
    * _Aspekty_: iluzoryczne klony, odwrócenie uwagi, pułapki
    * _Opis_: 
* **Magia mentalna**:
    * _Aspekty_: usuwanie pamięci, odwrócenie uwagi, czytanie pamięci, tarcze
    * _Opis_: 
* **Magia transportu**:
    * _Aspekty_: krótkodystansowe przesunięcia, ucieczka, hit-and-run
    * _Opis_: 
	
### Zaklęcia statyczne

* **Shatter clone**:
    * _Aspekty_: 
    * _Opis_: 

	
## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Zarobki

* żołd Świecy
* Dare Shiver

### Znam

* **Dare Shiver**:
    * _Aspekty_: 
    * _Opis_: pracuje tam jako twóra klimatu wyzwań

### Mam

* **Artefakty Dare Shiver**:
    * _Aspekty_: maskowanie, odkrywanie
    * _Opis_: 
* **Wyposażenie Srebrnej Świecy**:
    * _Aspekty_: biblioteka, wyposażenie terminusa
    * _Opis_:
	
# Opis

23 lata

## Motto

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Chrumpokalipsa](/rpg/inwazja/opowiesci/konspekty/180112-chrumpokalipsa.html)|opinia w Świecy: niestandardowe podejście Kingi uratowało Stefanię od niewoli kralotha.|Wizja Dukata|
|[Chrumpokalipsa](/rpg/inwazja/opowiesci/konspekty/180112-chrumpokalipsa.html)|posiada mały odłamek Wieprzopomnika Półdarskiego, sprzężonego z głównym pomnikiem.|Wizja Dukata|
|[Chrumpokalipsa](/rpg/inwazja/opowiesci/konspekty/180112-chrumpokalipsa.html)|tymczasowo Dotknięta przez Mrok Esuriit. Wrażliwa na tą formę energii.|Wizja Dukata|
|[Wspaniały Wieprz Wojtka](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|Sylwester Bankierz dał Kindze pozytywną opinię jako niezależnej agentce Świecy|Wizja Dukata|

## Plany

|Misja|Plan|Kampania|
|-----|------|------|
|[Wspaniały Wieprz Wojtka](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|Złapać organizatora walk norek. Rozwiązać problem z magią krwi (norek).|Wizja Dukata|

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|180112|lekko paranoicznie podeszła do Stefanii i odkryła "tam" kralotha. Też, zwalczała Golema Esuriit. Próbowała spokojem i ciszą przeważyć efektownego Anatola.|[Chrumpokalipsa](/rpg/inwazja/opowiesci/konspekty/180112-chrumpokalipsa.html)|11/10/16|11/10/18|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|180104|wykorzystywała mnóstwo artefaktów Świecy i Dare Shiver by pozyskać oczkodzika. Główna tropicielka zespołu (smutne).|[Wspaniały Wieprz Wojtka](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|11/10/12|11/10/15|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Stefania Kołek](/rpg/inwazja/opowiesci/karty-postaci/9999-stefania-kolek.html)|2|[180112](/rpg/inwazja/opowiesci/konspekty/180112-chrumpokalipsa.html), [180104](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|
|[Paweł Kupiernik](/rpg/inwazja/opowiesci/karty-postaci/9999-pawel-kupiernik.html)|2|[180112](/rpg/inwazja/opowiesci/konspekty/180112-chrumpokalipsa.html), [180104](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|
|[Anatol Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1709-anatol-sowinski.html)|2|[180112](/rpg/inwazja/opowiesci/konspekty/180112-chrumpokalipsa.html), [180104](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|
|[Wojciech Piekarz](/rpg/inwazja/opowiesci/karty-postaci/9999-wojciech-piekarz.html)|1|[180104](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|
|[Gabriel Purchasz](/rpg/inwazja/opowiesci/karty-postaci/9999-gabriel-purchasz.html)|1|[180104](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|
|[Bonifacy Jeż](/rpg/inwazja/opowiesci/karty-postaci/9999-bonifacy-jez.html)|1|[180104](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|

---
layout: inwazja-karta-postaci
categories: profile
factions: "Siły Specjalne Hektora Blakenbauera"
type: "NPC"
title: "Szczepan Paczoł"
---
# {{ page.title }}

## Postać

### Motywacje (do czego dąży)

* **obsesja na punkcie ćwiczeń**: cytat_lub_opis_jak_to_rozumieć
* **skłonności do samozniszczenia**: cytat_lub_opis_jak_to_rozumieć

### Zachowania (jaka jest)

* **opiekuńczy**: cytat_lub_opis_jak_to_rozumieć
* **waleczny**: cytat_lub_opis_jak_to_rozumieć
* **bardzo lojalny**: cytat_lub_opis_jak_to_rozumieć

### Specjalizacje

* **medyk polowy**: opis
* **szturmowiec Blakenbauerów**: opis
* **wszędzie się dostanie**: opis

### Umiejętności

* **żołnierz**: opis
* **bardzo wysportowany**: opis
* **lekarz pierwszego kontaktu**: opis

### Cechy

|  Social Aggressive |  Social Devious |  Social Friendly |  Knowledge |  Craft |  Supernatural |  Fortitude |  Nimbleness |
|:==================:|:===============:|:================:|:==========:|:======:|:=============:|:==========:|:===========:|
|        0           |         0       |         0        |      0     |    0   |       0       |      0     |      0      |

### Specjalne
* usprawniony żołnierz: +1 do wszystkich testów bojowych dzięki usprawnieniom Edwina
* bardzo odporny: +1 do testów odporności, mniej śpi, +2 do obrony przed chorobami i truciznami
* stabilizacja energią: +1 do wszystkich testów medycznych
* nie ma kar za "zbyt duże obciążenie"

## Magia

**brak**

## Zasoby i otoczenie

### Powiązane frakcje

* nazwa_frakcji_jak_ma_to_z_linkiem

### Kogo zna

* **grupy_i_frakcje_które_zna_i_które_ją_znają**: opis
* **grupy_i_frakcje_które_zna_i_które_ją_znają**: opis

### Co ma do dyspozycji:

* **sprzęt_do_jakiego_ma_dostęp_i_postać_charakteryzuje**: opis
* **sprzęt_do_jakiego_ma_dostęp_i_postać_charakteryzuje**: opis

### Surowce

* **Wartość**: 1/2/3
* **Pochodzenie**: dlaczego_1_2_lub_3_i_jak_zarabia

# Opis

Uzdolniony medyk polowy, żołnierz Wojska Polskiego który ciężko ucierpiał w Afganistanie. Wrócił do Polski i nie był w stanie poradzić sobie ze swoim życiem; wtedy znalazł go Edwin i przedstawił propozycję nie do odrzucenia - Edwin go wyleczy i usprawni, w zamian za co Szczepan dołączy do sił specjalnych rodu Blakenbauer.
Szczepan się zgodził. Edwin sfingował jego śmierć i go zrekonstruował, wzmacniając zarówno jego umiejętności bojowe jak i medyczne.
Na początku Szczepan był nieszczęśliwy - czuł wyraźnie, że nie jest już zwykłym człowiekiem, ale niejednokrotnie jego umiejętności uratowały innym życiem. Przyzwyczaił się, że siły specjalne są jego prawdziwą rodziną i już tak bardzo nie tęskni.
W stu procentach popiera działania Hektora.
Uważa swoje ciało za najpotężniejszą broń i regularnie ćwiczy; raz już był kaleką i nigdy więcej

### Koncept

Wysokopoziomowy koncept postaci, 1-2 zdania.

### Motto

"Tekst"

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|150722|pilnujący, by Patrycji nic się nie stało a potem usypiający dzieci i chowający je do rowu na ostatniej akcji.|[Reverse kidnapping z Krupnioka](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html)|10/05/15|10/05/16|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170407|monitorujący sytuację (i Brysia) w szpitalu z ramienia Sił Specjalnych Hektora; zabezpieczył nagrania z monitoringu by nikt nic nie zobaczył|[Przebudzenie viciniusa](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|10/03/08|10/03/09|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|[Patrycja Krowiowska](/rpg/inwazja/opowiesci/karty-postaci/1709-patrycja-krowiowska.html)|2|[150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html), [170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1707-hektor-blakenbauer.html)|2|[150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html), [170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|2|[150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html), [170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|
|[Dionizy Kret](/rpg/inwazja/opowiesci/karty-postaci/1707-dionizy-kret.html)|2|[150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html), [170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|
|[Urszula Murczyk](/rpg/inwazja/opowiesci/karty-postaci/1705-urszula-murczyk.html)|1|[170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|
|[Tymoteusz Maus](/rpg/inwazja/opowiesci/karty-postaci/1705-tymoteusz-maus.html)|1|[150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html)|
|[Roman Brunowicz](/rpg/inwazja/opowiesci/karty-postaci/9999-roman-brunowicz.html)|1|[170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|
|[Paulina Widoczek](/rpg/inwazja/opowiesci/karty-postaci/9999-paulina-widoczek.html)|1|[170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|
|[Olga Miodownik](/rpg/inwazja/opowiesci/karty-postaci/1709-olga-miodownik.html)|1|[150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html)|
|[Oddział Zeta](/rpg/inwazja/opowiesci/karty-postaci/9999-oddzial-zeta.html)|1|[170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|
|[Margaret Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-margaret-blakenbauer.html)|1|[150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html)|
|[Marcelin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1707-marcelin-blakenbauer.html)|1|[150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html)|
|[Kornel Wadera](/rpg/inwazja/opowiesci/karty-postaci/1709-kornel-wadera.html)|1|[170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|
|[Kleofas Bór](/rpg/inwazja/opowiesci/karty-postaci/1709-kleofas-bor.html)|1|[150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html)|
|[Klemens X](/rpg/inwazja/opowiesci/karty-postaci/9999-klemens-x.html)|1|[150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html)|
|[Gala Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-gala-zajcew.html)|1|[150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html)|
|[Franciszek Knur](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-knur.html)|1|[170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|
|[Dorota Gacek](/rpg/inwazja/opowiesci/karty-postaci/1709-dorota-gacek.html)|1|[170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|
|[Dominik Parszywiak](/rpg/inwazja/opowiesci/karty-postaci/9999-dominik-parszywiak.html)|1|[170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|
|[Artur Bryś](/rpg/inwazja/opowiesci/karty-postaci/1709-artur-brys.html)|1|[170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|
|[Anna Kajak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kajak.html)|1|[150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html)|
|[Alina Bednarz](/rpg/inwazja/opowiesci/karty-postaci/1709-alina-bednarz.html)|1|[170407](/rpg/inwazja/opowiesci/konspekty/170407-przebudzenie-viciniusa.html)|
|[Adela Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-adela-maus.html)|1|[150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html)|

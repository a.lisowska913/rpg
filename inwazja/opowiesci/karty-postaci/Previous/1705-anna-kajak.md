---
layout: inwazja-karta-postaci
categories: profile
factions: "Prokuratura magów, Siły Specjalne Hektora Blakenbauera"
type: "NPC"
title: "Anna Kajak"
---
# {{ page.title }}

## Postać

### Motywacje (do czego dąży)

* **karty przy orderach**: cytat_lub_opis_jak_to_rozumieć

### Zachowania (jaka jest)

* **surowa**: cytat_lub_opis_jak_to_rozumieć
* **bardzo dyskretna**: cytat_lub_opis_jak_to_rozumieć
* **niezwykle dokładna**: cytat_lub_opis_jak_to_rozumieć
* **paranoiczna**: cytat_lub_opis_jak_to_rozumieć

### Specjalizacje

* **infiltracja**: opis

### Umiejętności

* **policjantka**: opis
* **okultyzm**: opis
* **strzelec wyborowy**: opis
* **taktyka**: opis
* **dowody**: opis

### Cechy

|  Social Aggressive |  Social Devious |  Social Friendly |  Knowledge |  Craft |  Supernatural |  Fortitude |  Nimbleness |
|:==================:|:===============:|:================:|:==========:|:======:|:=============:|:==========:|:===========:|
|        0           |         0       |         0        |      0     |    0   |       0       |      0     |      0      |

### Specjalne
* **lustra**: Anna ma zawsze przy sobie co najmniej jedno lustro. I potrafi go używać.

## Magia

**brak**

## Zasoby i otoczenie

### Powiązane frakcje

* nazwa_frakcji_jak_ma_to_z_linkiem

### Kogo zna

* **grupy_i_frakcje_które_zna_i_które_ją_znają**: opis
* **grupy_i_frakcje_które_zna_i_które_ją_znają**: opis

### Co ma do dyspozycji:

* **sprzęt_do_jakiego_ma_dostęp_i_postać_charakteryzuje**: opis
* **sprzęt_do_jakiego_ma_dostęp_i_postać_charakteryzuje**: opis

### Surowce

* **Wartość**: 1/2/3
* **Pochodzenie**: dlaczego_1_2_lub_3_i_jak_zarabia

# Opis

Analityk w siłach specjalnych Blakenbauerów. Bardzo kompetentna, dość surowa, świetnie trzyma karty przy orderach. Nie socjalizuje się specjalnie z innymi. Samotna 36-letnia kobieta (na chronologię "Blakenbauerowie x Skorpion". Otacza się lustrami i wierzy, że zapewnią jej bezpieczeństwo. Lekko nienormalna, ale nie przeszkadza być to bardzo kompetentną agentką Hektora. Uważa Hektora za "X-com", osobę walczącą z paranormalnym.
Czasem widzi coś w lustrach. Wierzy w to, co widzi.
Nie mówi niczego co nie jest niezbędne dla sprawy.

### Koncept

Wysokopoziomowy koncept postaci, 1-2 zdania.

### Motto

"Tekst"

# Historia:



## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|160309|która wykryła atak tajemniczych sił na dzieci Patrycji; poprosiła Hektora o autoryzację akcji.|[Irytka Sprzężona](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|10/06/25|10/06/26|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150930|zatrudniona przez Zajcewa jako detektyw, podsłuchiwała lojalnie dla Blakenbauerów. Gra na dwa fronty.|[O Wandzie co Zajcewa nie chciała](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html)|10/05/29|10/05/30|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|151001|która zobaczyła, że coś się dzieje na terenie hipermarketu i zgłosiła to Hektorowi by być odesłaną do czegoś nieważnego.|[Plan ujawnienia z Hipernetu](/rpg/inwazja/opowiesci/konspekty/151001-plan-ujawnienia-z-hipernetu.html)|10/05/21|10/05/22|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150722|próbowała rekrutować Dionizego do planów "The Director", ale zrezygnowała; inicjatorka akcji uratowania dzieci przed Syberią.|[Reverse kidnapping z Krupnioka](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html)|10/05/15|10/05/16|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150103|jeszcze: Makont, która użyła Pryzmatu Myśli krzywdząc swojego ukochanego brata. W ciężkim stanie psychicznym.|[Pryzmat Myśli pęka](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html)|10/02/15|10/02/16|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|
|141230|jeszcze: Makont, policjantka zapętlona z bratem w "czy już rozmawialiśmy z Amelią".|[Ofiara z wampira dla Arazille](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html)|10/02/13|10/02/14|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|
|141227|jeszcze: Makont, policjantka, której świat się po prostu zawalił przez okultyzm. Ale dzielnie walczy.|[Przyczajona Andromeda, ukryty Maus](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|10/02/11|10/02/12|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|
|141220|jeszcze: Makont, policjantka (analityk) która spotkała się z najtrudniejszą sprawą w życiu.|[Bogini Marzeń w Żonkiborze](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|10/02/09|10/02/10|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|
|150115|która niekoniecznie radzi sobie z walką ze studentami nie czującymi bólu ani strachu.|[Negocjacje w LegioQuant](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html)|10/01/13|10/01/14|[Blakenbauerowie x Skorpion](/rpg/inwazja/opowiesci/konspekty/kampania-blakenbauerowie-x-skorpion.html)|
|150210|członek grupy śledczej infiltrująca "Kopalińskich Komandosów" oraz rodzinę przemysłowców Larent (przez Dianę, która robi złe rzeczy).|['Komandosi', czyli upadek bohaterki](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html)|10/01/11|10/01/12|[Blakenbauerowie x Skorpion](/rpg/inwazja/opowiesci/konspekty/kampania-blakenbauerowie-x-skorpion.html)|
|141216|przełożona sił specjalnych Hektora, przeżyła bardzo wielu przełożonych i podwładnych i ma zamiar nie przerywać dobrej passy.|[Zabili mu syna](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|10/01/11|10/01/12|[Blakenbauerowie x Skorpion](/rpg/inwazja/opowiesci/konspekty/kampania-blakenbauerowie-x-skorpion.html)|
|141022|aktualna przełożona sił specjalnych Hektora, która jeszcze żyje i daje radę|[Po wymianie strzałów...](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|10/01/09|10/01/10|[Blakenbauerowie x Skorpion](/rpg/inwazja/opowiesci/konspekty/kampania-blakenbauerowie-x-skorpion.html)|
|150304|agentka, która przypadkowo zainicjowała całą piramidę kłamstw i osłaniana przez wszystkich wyszła cało. Ma morderczo-sadystyczną reakcję na tajemnicze lusterko.|[Ani słowa prawdy...](/rpg/inwazja/opowiesci/konspekty/150304-ani-slowa-prawdy.html)|10/01/03|10/01/04|[Blakenbauerowie x Skorpion](/rpg/inwazja/opowiesci/konspekty/kampania-blakenbauerowie-x-skorpion.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|Hektor Blakenbauer|8|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [151001](/rpg/inwazja/opowiesci/konspekty/151001-plan-ujawnienia-z-hipernetu.html), [150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html), [150115](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html), [150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html), [141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html), [141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html), [150304](/rpg/inwazja/opowiesci/konspekty/150304-ani-slowa-prawdy.html)|
|Edwin Blakenbauer|7|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html), [151001](/rpg/inwazja/opowiesci/konspekty/151001-plan-ujawnienia-z-hipernetu.html), [150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html), [150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html), [141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html), [150304](/rpg/inwazja/opowiesci/konspekty/150304-ani-slowa-prawdy.html)|
|Klemens X|6|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html), [150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html), [150115](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html), [150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html), [141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html), [141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|Olga Miodownik|5|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html), [150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html), [150115](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html), [141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html), [150304](/rpg/inwazja/opowiesci/konspekty/150304-ani-slowa-prawdy.html)|
|Margaret Blakenbauer|5|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [151001](/rpg/inwazja/opowiesci/konspekty/151001-plan-ujawnienia-z-hipernetu.html), [150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html), [150115](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html), [141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|Alina Bednarz|5|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html), [150115](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html), [141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html), [141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html), [150304](/rpg/inwazja/opowiesci/konspekty/150304-ani-slowa-prawdy.html)|
|Wojciech Kajak|4|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html), [141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|
|Sandra Stryjek|4|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html), [141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|
|Marcelin Blakenbauer|4|[150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html), [150115](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html), [150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html), [141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|Kleofas Bór|4|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html), [141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html), [150304](/rpg/inwazja/opowiesci/konspekty/150304-ani-slowa-prawdy.html)|
|Kasia Nowak|4|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html), [141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|
|Herbert Zioło|4|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html), [141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|
|Samira Diakon|3|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html), [141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|
|Patrycja Krowiowska|3|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html), [150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html)|
|Netheria Diakon|3|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|Józef Pimczak|3|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html), [141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|
|Iliusitius|3|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|Arazille|3|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|Tymoteusz Maus|2|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html), [150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html)|
|Tymoteusz Dzionek|2|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html), [141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|Rafał Szczęślik|2|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|Patryk Romczak|2|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|Mordecja Diakon|2|[150115](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html), [150304](/rpg/inwazja/opowiesci/konspekty/150304-ani-slowa-prawdy.html)|
|Michał Czuk|2|[141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html), [141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|
|Małgorzata Poran|2|[141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html), [141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|
|Luksja Diakon|2|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|Luiza Wanta|2|[141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html), [141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|Karradrael|2|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|Joachim Zajcew|2|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html)|
|Gabriel Newa|2|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html)|
|Feliks Hanson|2|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|Feliks Bozur|2|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html)|
|Dionizy Kret|2|[151001](/rpg/inwazja/opowiesci/konspekty/151001-plan-ujawnienia-z-hipernetu.html), [150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html)|
|Diana Weiner|2|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [151001](/rpg/inwazja/opowiesci/konspekty/151001-plan-ujawnienia-z-hipernetu.html)|
|Aneta Hanson|2|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|Andrzej Szop|2|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|Adam Bożynów|2|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html), [141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|oddział Zeta|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|Wiktor Sowiński|1|[151001](/rpg/inwazja/opowiesci/konspekty/151001-plan-ujawnienia-z-hipernetu.html)|
|Wiktor Lubaszny|1|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html)|
|Wanda Ketran|1|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html)|
|Waltrauda Werner|1|[150115](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html)|
|Vladlena Zajcew|1|[151001](/rpg/inwazja/opowiesci/konspekty/151001-plan-ujawnienia-z-hipernetu.html)|
|Timor Koral|1|[150115](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html)|
|Szymon Skubny|1|[141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|Szczepan Paczoł|1|[150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html)|
|Sebastian Tecznia|1|[141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|Sebastian Linka|1|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html)|
|Rufus Czubek|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|Robert Przerot|1|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html)|
|Rebeka Piryt|1|[141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|Przemysław Marchewka|1|[141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|Ozydiusz Bankierz|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|Onufry Puzel|1|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html)|
|Oktawian Maus|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|Oddział Zeta|1|[150115](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html)|
|Mojra|1|[141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|Miron Ataman|1|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html)|
|Mikołaj Młot|1|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html)|
|Mariusz Garaż|1|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html)|
|Marian Kozior|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|Maciej Kwarc|1|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html)|
|Maciej Dworek|1|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html)|
|Leonidas Blakenbauer|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|Klara Blakenbauer|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|Kinga Melit|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|Karol Poczciwiec|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|Karol Kiśnia|1|[150304](/rpg/inwazja/opowiesci/konspekty/150304-ani-slowa-prawdy.html)|
|Kamil Rzepa|1|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html)|
|Judyta Maus|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|Janusz Wybój|1|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html)|
|Janina Strych|1|[150304](/rpg/inwazja/opowiesci/konspekty/150304-ani-slowa-prawdy.html)|
|Jan Fiołek|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|Jakub Ryjek|1|[150115](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html)|
|Hektor Poran|1|[141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|
|Grzegorz Śliwa|1|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html)|
|Grażyna Remska|1|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html)|
|Gala Zajcew|1|[150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html)|
|Filip Sztukar|1|[141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|Filip Szorak|1|[150115](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html)|
|Felicja Wadicek|1|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html)|
|Ewa Kroideł|1|[141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|Ewa Czuk|1|[141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|
|Estera Piryt|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|Emilia Kołatka|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|Elea Maus|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|Dorota Gacek|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|Dominik Marchewka|1|[141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|Diana Larent|1|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html)|
|Dagmara Wyjątek|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|Cezary Piryt|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|Brunon Czerpak|1|[141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|Bolesław Derwisz|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|Bianka Drażyńska|1|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html)|
|August Bankierz|1|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html)|
|Artur Szmelc|1|[141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|Arkadiusz Wadicek|1|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html)|
|Antoni Szczęśliwiec|1|[141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|Anna Góra|1|[150930](/rpg/inwazja/opowiesci/konspekty/150930-o-wandzie-co-zajcewa-nie-chciala.html)|
|Andrzej Domowierzec|1|[141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|
|Andrzej Chezyr|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|Albert Pireus|1|[150304](/rpg/inwazja/opowiesci/konspekty/150304-ani-slowa-prawdy.html)|
|Adrian Murarz|1|[151001](/rpg/inwazja/opowiesci/konspekty/151001-plan-ujawnienia-z-hipernetu.html)|
|Adela Maus|1|[150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html)|
|Adam Wąż|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|

---
layout: inwazja-karta-postaci
title:  "Adonis Sowiński"
categories: profile, srebrna_swieca
---
# {{ page.title }}

# Mechanika

## Impulsy (co motywuje / napędza postać):

### Strategiczne

* **daredevil**: "jeśli nic nie czujesz… to nie żyjesz."
* **kocha piękno**: "Nie ma nic doskonalszego, niż po zastrzyku adrenaliny obcować z czymś naprawdę pięknym"

### Taktyczne

* **otwarty**: "wszystkiego należy spróbować. Nie mnie oceniać. Bądź czym chcesz."
* **hedonista**: "przyjemność jest najwyższą formą egzystencji"
* **apolityczny**: "te wszystkie tematy związane z gildiami i frakcjami… to nieistotne. Wszyscy i tak umrzemy."
* **rywalizacja**: "cóż, nic nie poradzę - jestem jednym z najlepszych ;-)."
* **prankster**: "nadmiar powagi i patrzenia na świat przez krawat zabija ;-)"

## Co umie (co postać potrafi):

### Ekspercko (poziom 3):

* **skłanianie do ryzyka** (przywódca)
* **gry karciane** (hazardzista)
* **bullshit** (nieuchwytny)

### Zawodowo (poziom 2):

* **hazardzista**
* **uwodziciel**
* **corruptor**
* **dbanie o urodę**
* **aktorstwo**
* **nieuchwytny**
* **inspirujący przywódca**
* **odwracanie uwagi**

### Amatorsko (poziom 1):

* **guru sekty**
* **włamywacz**
* **agent specjalny**
* **retoryka**


## Inklinacje:

|  Social Aggressive |  Social Devious |  Social Friendly |  Knowledge |  Craft |  Supernatural |  Fortitude |  Nimbleness |
|:==================:|:===============:|:================:|:==========:|:======:|:=============:|:==========:|:===========:|
|       +1           |        +1       |        +1        |     -1     |   -1   |      +1       |      0     |     +1      |


## Kieszonkowe

2
Standardowo; żyje na wysokim poziomie, więc jego bogactwo nie jest widoczne

## Otoczenie powiązane

### Powiązane frakcje

* Srebrna Świeca

### Kogo zna

#### Doskonale: 3

* **prezydent klubu "Dare Shiver" ("odważ się drżeć")** 

#### Nieźle: 2

* **magowie, którym pomógł "odnaleźć siebie"**

#### Luźno: 1

* **szeroka grupa znajomości klubowych**
* **sława wybitnego hazardzisty**

### Co ma do dyspozycji:

#### Szczególnie dobre: 3

* **szczególnie_dobry**: opis

#### Ponadprzeciętne: 2


* **wszechstronne przebrania i stroje**
* **eliksiry i gry imprezowe (non-dangerous stuff)**
* **zaprzyjaźnione miejsca na Śląsku (non-dangerous stuff)**


#### Niezłe: 1

* **kiepskie_obiekty**: opis

## Magia:

### Magia dynamiczna:


* iluzjonista, specjalizacja: nieuchwytność
* magia urody i piękna
* magia imprezowa


### Zaklęcia hermetyczne

* nazwa : link
* nazwa : link

## Specjalne:


* inklinacje NIE sumują się do zera!!! Sumują się do 3.
* bishounen; nie ma wady przy przebieraniu się za dziewczynę


# Opis

## Motto

"Tekst"

## Inne

Adonis - a beautiful and carefree bishounen. Or rather, what he would like others to think about him. He is extremely beautiful to the point is often considered to be one of Diakon bloodline, however behind the beautiful visage lies a competitive daredevil.

This mage is completely apolitical. He doesn’t care about politics, guilds etc. he loves the beauty and loves – absolutely loves – the feel of adrenaline. He is also a prankster and likes putting others in embarrassing positions, although does not want to hurt anyone. Everything he does it’s supposed to be amazing experiences for everyone - especially in retrospect. A golden youth.

A very good gambler and risk taker, incredibly difficult to catch and a president of the “Dare Shiver” club, this one – although not dangerous in terms of meaning of the word – is known to be sometimes irritating.

In terms of magic, he specializes in the beauty magic, party magic and illusion magic; especially in the regions which allow him to stay undetected, observe, never get caught.

# Historia

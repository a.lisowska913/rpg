---
layout: inwazja-karta-postaci
categories: profile
factions: "Niezrzeszeni"
type: "PC"
owner: "kić"
title: "Paulina Tarczyńska"
---
# {{ page.title }}

## Koncept



## Postać

### Motywacje

#### Kategorie i Aspekty

| Kategoria  | Aspekty                           |
|------------|-----------------------------------|
| Dla siebie | obrońca społeczności; lepszy świat; bezpieczna przystań |
| Dla innych | kwitnąca społeczność; magowie w harmonii z ludźmi; bezpieczeństwo od magii;|
| Co ceni    | zdrowie do końca; gotowość do służby; odpowiedzialność  |

#### Szczególnie

| Co chce by się działo? Co jest pożądane?                 | Co na pewno ma się NIE dziać? Co jest sprzeczne?        |
|----------------------------------------------------------|---------------------------------------------------------|
| pomagaj nawet kosztem siebie; unikaj krzywdzenia         | ignoruj prośby o pomoc |
| naprawiaj świat                                          | ignoruj Skażenie; nadużywaj magii |
| promuj odpowiedzialne użycie magii                       | akceptuj nieodpowiedzialność magów |
| broń słabszych                                           |  |
|  |  |
|  |  |

### Umiejętności

#### Kategorie i Aspekty

| Kategoria     | Aspekty                           |
|---------------|-----------------------------------|
| Lekarz        | zaufana pani doktor; lekarz pierwszego kontaktu |
| Aktywista     | prospołeczna; integrity |
| Agitator      | proludzka; idealistka |

#### Manewry

| Jakie działania wykonuje?                      | Czym osiąga sukces?                                                |
|------------------------------------------------|--------------------------------------------------------------------|
| stawianie diagnozy                             | wywiad lekarski; wyciągnie prawdy; wzbudzanie zaufania |
| wymuszenie spokoju; zapobieganie panice        | opanowana pod presją; wzbudzanie zaufania |
| zbieranie plotek; zarządanie plotką            |  |
| przekonywanie do swoich racji                  |  |
| zdobywanie wsparcia sprzętowego i finanoswego  |  |
|  |  |

### Silne i słabe strony

#### Kategorie i Aspekty

| Kategoria  | Aspekty                        |
|------------|--------------------------------|
| Altruizm   |  |
| lekarz z powołania |  |

#### Manewry

| Co jest wzmocnione       | Kosztem czego                 |
|--------------------------|-------------------------------|
| wytrzymałość fizyczna    |  |
| osadzenie w społeczności | nie odmawia pomocy; da drugą szansę |
|  |  |

### Szkoły magiczne

#### Kategorie i Aspekty

| Kategoria           | Aspekty                                                                             |
|---------------------|-------------------------------------------------------------------------------------|
|  |  |
|  |  |
|  |  |

#### Manewry

| Jakie działania wykonuje?                      | Czym osiąga sukces?                                      |
|------------------------------------------------|----------------------------------------------------------|
|  |  |
| leczy Skażenie | kataliza; high-tech sprzęt magitechowy |
|  |  |
|  |  |
|  |  |
|  |  |
|  |  |
|  |  |
|  |  |

### Zasoby i otoczenie

#### Powiązane frakcje

{{ page.factions }}

#### Kategorie i Aspekty

| Kategoria                          | Aspekty                                                              |
|------------------------------------|----------------------------------------------------------------------|
| Maria Newa | pamięć absolutna; działanie w dwóch ciałach; stały mindlink |
|  |  |
|  |  |
|  |  |

#### Manewry

| Jakie działania wspierane?            | Czym osiąga sukces?                                                   |
|---------------------------------------|-----------------------------------------------------------------------|
| wywiad; zbieranie wiedzy o otoczeniu; | Maria Newa |
|  |  |
|  |  |
|  |  |
|  |  |
|  |  |
|  |  |
|  |  |
|  |  |

## Opis

### Ogólnie

### Motywacje

### Działanie

### Specjalne

### Magia

### Otoczenie

### Mapa kreacji

brak

### Motto

"Primum non nocere"

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Chrumpokalipsa](/rpg/inwazja/opowiesci/konspekty/180112-chrumpokalipsa.html)|udało jej się uniknąć obwinienia za Wieprzopomnik w Półdarze; to Świeca Z Nie Tego Terenu.|Wizja Dukata|
|[Nie podłożona świnia Łucji](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html)|zyskuje sojusznika w Bolesławie Mausie (właścicielu Portaliska Pustulskiego)|Wizja Dukata|
|[Odzyskana władza Pauliny](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html)|sława i chwała; duży sukces przy stabilizacji terenu: magitrownia działa, świnie pod kontrolą, nie ma klątwożytów.|Wizja Dukata|
|[Potrójna magitrownia Histogram](/rpg/inwazja/opowiesci/konspekty/171220-potrojna-magitrownia-histogram.html)|Istnieje możliwość powrotu do miejsca splotu tych wszystkich faz. Ona będzie w stanie docelowo tam wrócić.|Wizja Dukata|
|[Kryzysowo tymczasowy dyktator](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|została dyktatorem na dwa tygodnie z ramienia Sylwestra Bankierza, Eweliny Bankierz i Roberta Sądecznego|Wizja Dukata|
|[Epidemia Dezinhibicji](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|dostaje wsparcie od Millennium. (Kić)|Wizja Dukata|
|[Tylko nieświadomy elemental](/rpg/inwazja/opowiesci/konspekty/171022-tylko-nieswiadomy-elemental.html)|dostaje awiana starego typu jako karetkę; zabezpieczony dla niej przez Roberta Sądecznego|Wizja Dukata|
|[Tylko nieświadomy elemental](/rpg/inwazja/opowiesci/konspekty/171022-tylko-nieswiadomy-elemental.html)|za szybkie i skuteczne działanie w klubie Panienka w Koralach została doceniona przez mafię i Świecę|Wizja Dukata|
|[Senesgradzka kopia Pauliny](/rpg/inwazja/opowiesci/konspekty/171218-senesgradzka-kopia-pauliny.html)|otrzymała od Kai Maślaczek w Senesgradzie czujniki, by widzieć randomowych ludzi Skażonych przez Efemerydę|Wizja Dukata|
|[Senesgradzka kopia Pauliny](/rpg/inwazja/opowiesci/konspekty/171218-senesgradzka-kopia-pauliny.html)|zobowiązała się przed Efemerydą, że będzie chronić i pomagać ludziom Skażonym przez magię w Senesgradzie|Wizja Dukata|
|[Senesgradzka kopia Pauliny](/rpg/inwazja/opowiesci/konspekty/171218-senesgradzka-kopia-pauliny.html)|świetna opinia doskonałej profesjonalistki, zwłaszcza w Senesgradzie. W świecie ludzi. Pomocna ludziom w sytuacjach beznadziejnych.|Wizja Dukata|
|[Wyciek syberionu](/rpg/inwazja/opowiesci/konspekty/161002-wyciek-syberionu.html)|#1 void|Prawdziwa natura Draceny|
|[Walka o duszę Draceny](/rpg/inwazja/opowiesci/konspekty/160918-walka-o-dusze-draceny.html)|została tymczasową Panią Draceny.|Prawdziwa natura Draceny|
|[Dracena... Blakenbauer?](/rpg/inwazja/opowiesci/konspekty/160907-dracena-blakenbauer.html)|-1 void (na kryształy Mausów dla Draceny)|Prawdziwa natura Draceny|
|[Rozpaczliwie sterowany wzór](/rpg/inwazja/opowiesci/konspekty/160904-rozpaczliwie-sterowany-wzor.html)|już nie jest Skażona|Prawdziwa natura Draceny|
|[Uwięziony w komputerze!](/rpg/inwazja/opowiesci/konspekty/160901-uwieziony-w-komputerze.html)|#1 Fire #1 Void; zapłacono jej zdecydowanie nadmiernie|Prawdziwa natura Draceny|
|[Uwięziony w komputerze!](/rpg/inwazja/opowiesci/konspekty/160901-uwieziony-w-komputerze.html)|tymczasowo Skażona|Prawdziwa natura Draceny|
|[Klątwożyt z lustra](/rpg/inwazja/opowiesci/konspekty/170325-klatwozyt-z-lustra.html)|dostaje nagrodę Gabriela Dukata za zniszczenie klątwożyta|Rezydentka Krukowa|
|[Oszczędzili na rurach](/rpg/inwazja/opowiesci/konspekty/170323-oszczedzili-na-rurach.html)|ma dziwne lustro wzmacniające, należące do nieznanego maga|Rezydentka Krukowa|
|[Wąż jako vicinius Pauliny](/rpg/inwazja/opowiesci/konspekty/170404-waz-jako-vicinius-pauliny.html)|dostała pomniejszą sławę jako lekarka na małej arenie, promotorka viciniusów i lekarka. Całkowicie nieprawdziwa fama, acz plotka poszła.|Rezydentka Krukowa|
|[Wąż jako vicinius Pauliny](/rpg/inwazja/opowiesci/konspekty/170404-waz-jako-vicinius-pauliny.html)|jest tymczasowo odpowiedzialna za tien Katarzynę Mirłik aż ta wyzdrowieje|Rezydentka Krukowa|
|[Opętany konstruminus](/rpg/inwazja/opowiesci/konspekty/170523-opetany-konstruminus.html)|Jakub Dobrocień, logistyk Dukata ma u niej dług za sprawę z magimedem i uratowanie życia...|Rezydentka Krukowa|

## Plany

|Misja|Plan|Kampania|
|-----|------|------|
|[Nie podłożona świnia Łucji](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html)|Być w "Radzie Regionu" proponowanej przez Daniela Akwitańskiego (by region działał prawidłowo i spójnie)|Wizja Dukata|
|[Nie podłożona świnia Łucji](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html)|Przekazać Eliksir Aerinus Hektorowi Reszniaczkowi|Wizja Dukata|
|[Nie podłożona świnia Łucji](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html)|Zacząć zbierać rzeczy na Roberta Sądecznego; wykorzystać wiedzę Katii Grajek.|Wizja Dukata|
|[Odzyskana władza Pauliny](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html)|chce przekazać Eliksir Aerinus Hektorowi Reszniaczkowi; nie chce by Kaja go rozwaliła|Wizja Dukata|
|[Senesgradzka kopia Pauliny](/rpg/inwazja/opowiesci/konspekty/171218-senesgradzka-kopia-pauliny.html)|Paulina od czasu do czasu poświęca czas by pomagać ludziom w Senesgradzie. Nie tylko dla Janiny, też, bo tak trzeba.|Wizja Dukata|
|[Powrót do domu](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html)|wróciła na teren powiatu Pustulskiego, by po kolei i stopniowo walczyć o bycie rezydentką.|Wizja Dukata|

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|180214|ściąnęła Melodię do rozwiązania konfliktu Łucja - Tomasz, zajmowała się też dyplomacją po stronie Mausów. Wszyscy mają czyste konto. Uratowała Łucję.|[Nie podłożona świnia Łucji](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html)|11/10/15|11/10/17|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|180110|która osiągnęła ogromny sukces - przez MOMENT wszystko na terenie Powiatu Pustulskiego jest pod kontrolą. Klątwożyty, świnie, Mausowie...|[Odzyskana władza Pauliny](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html)|11/10/10|11/10/12|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171220|ujawniła Integrę w manifestacji efemerydy; nie będzie eksperymentów na ludziach. Oraz: była w trójfazie i wróciła do domu.|[Potrójna magitrownia Histogram](/rpg/inwazja/opowiesci/konspekty/171220-potrojna-magitrownia-histogram.html)|11/10/05|11/10/06|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171115|pomogła Anecie, uratowała Łucję, posprzeczała się z Wiaczesławem i niespodziewanie została dyktatorem na 2 tygodnie.|[Kryzysowo tymczasowy dyktator](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|11/10/03|11/10/04|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171105|próbowała opanować epidemię we wczesnej fazie - niestety, atak awianów rozwalił ten plan. Potem poprztykała się z paramilitarnymi Chabrami i odnowiła kontakt z Wiaczesławem.|[Epidemia Dezinhibicji](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|11/10/01|11/10/02|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171101|załatwia każdemu magimeda od Dukata - Świeca, mafia, sobie... zajmuje się Anetą Rukolas z Dalią i zyskała niechętny szacunek Sądecznego.|[Magimedy przed epidemią](/rpg/inwazja/opowiesci/konspekty/171101-magimedy-przed-epidemia.html)|11/09/28|11/09/30|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171031|przechwyciła Anetę Rukolas i ratowała ludzi i magów na lewo i prawo po katastrofie na portalisku. Przejęła kontrolę medyczną nad sytuacją.|[Utracona kontrola](/rpg/inwazja/opowiesci/konspekty/171031-utracona-kontrola.html)|11/09/24|11/09/27|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171029|ściągnęła KADEM na Mazowsze i ukoiła (rozpraszając ją) Oktawię przed dezaktywacją. Dodatkowo poznała informacje na temat planów i działań Sądecznego. Po Oktawii ma kaca moralnego.|[W co gra Sądeczny?](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html)|11/09/20|11/09/22|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171024|wezwana na leczenie Mai Weiner i Adama Kapelusza, została, bo temat dotyczył "dziwnych elementali". Wezwała Kajetana w kluczowym momencie i odkryła prawdę o Wieży Wichrów.|[Detektyw, lecz nie Sądecznego](/rpg/inwazja/opowiesci/konspekty/171024-detektyw-lecz-nie-sadecznego.html)|11/09/16|11/09/18|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171022|przede wszystkim faktycznie leczyła - Szymona, grupę ludzi w klubie, Dracenę po porażeniu... wezwała Kajetana i opanowała elementala. Martwi się biznesem Filipa na kilku poziomach.|[Tylko nieświadomy elemental](/rpg/inwazja/opowiesci/konspekty/171022-tylko-nieswiadomy-elemental.html)|11/09/12|11/09/14|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171015|która walczy o to, by nie wybuchła wojna domowa i mediuje między Świecą i Rodziną Dukata - a nawet wewnątrz tych organizacji. Aha, i z Draceną.|[Powstrzymana wojna domowa](/rpg/inwazja/opowiesci/konspekty/171015-powstrzymana-wojna-domowa.html)|11/09/10|11/09/11|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171010|zdobyła informacje od Hektora odnośnie Katii i przekazała wszystko co wie Kajetanowi. Potem wróciła do pacjentów - było ostro.|[Jasny sygnał Tamary](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|11/09/06|11/09/09|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171004|zdobyła logi z efemerydy i poznała ponurą prawdę. Pomogła ogarnąć problem z elektrownią. Leczyła ofiary, wezwała Miłoszepta i kontrolowała Dracenę.|[Niestabilna magitrownia](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|11/09/01|11/09/03|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171218|skonfrontowała się z manifestacją senesgradzką siebie samej. Z bólem, przekonała swą manifestację do rozpłynięcia się w Efemerydzie. Poznała prawdziwą rozległość Efemerydy Senesgradzkiej.|[Senesgradzka kopia Pauliny](/rpg/inwazja/opowiesci/konspekty/171218-senesgradzka-kopia-pauliny.html)|11/08/28|11/08/31|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171001|wróciła jako nie-rezydentka by zacząć knuć jakby nią dalej była. Próbuje pomóc nowemu rezydentowi i Kai a jednocześnie uwolniła się od monitoringu Dukata.|[Powrót do domu](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html)|11/08/25|11/08/27|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|170212|próbująca pogodzić sprzeczne lojalności Kajetana i Draceny, potem jakoś osłonić wszystkich przed Laetitią i wpadła na pomysł naprawy Laetitii.|[Nieufność w małym mieście](/rpg/inwazja/opowiesci/konspekty/170212-nieufnosc-w-malym-miescie.html)|10/07/22|10/07/25|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|
|161009|cieszy się, bo dla odmiany jej działania nie ratują życia wszystkim dookoła... acz chce zostawić wszystkich w jak najlepszym stanie.|['Paulino, zmieniłaś się...'](/rpg/inwazja/opowiesci/konspekty/161009-paulino-zmienilas-sie.html)|10/07/14|10/07/16|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|
|161002|mająca Dracenę "na smyczy" i ratująca ludzi po telefonie Korzunia o wycieku syberionu; jest na obszarze bez magów.|[Wyciek syberionu](/rpg/inwazja/opowiesci/konspekty/161002-wyciek-syberionu.html)|10/07/11|10/07/13|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|
|160420|która usłyszawszy niepokojące wieści od Olgi (zwłaszcza o krwawych kuźniach) zaangażowała w temat Kajetana Weinera.|[Kolizja dwóch sojuszy](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html)|10/07/01|10/07/02|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160918|anioł stróż Draceny poświęcająca nawet kontakty i potencjalnie zdrowie by jej pomóc. Przez Najadę - pani Draceny. Z wyrzutami sumienia.|[Walka o duszę Draceny](/rpg/inwazja/opowiesci/konspekty/160918-walka-o-dusze-draceny.html)|10/06/30|10/07/03|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|
|161113|nieobecna (na telefonie), służąca kontaktami wśród medyków w Czeliminie oraz odnośnie biurokracji ludzkiej dla Kajetana.|[Świeca nie zostawia swoich](/rpg/inwazja/opowiesci/konspekty/161113-swieca-nie-zostawia-swoich.html)|10/06/29|10/07/01|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160910|próbująca kierować Dracenę ku lepszemu, walcząca o "tą Dracenę która była" nawet kosztem destabilizacji i wycofania postępu.|[Na żywym organiźmie Draceny...](/rpg/inwazja/opowiesci/konspekty/160910-na-zywym-organizmie-draceny.html)|10/06/27|10/06/29|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|
|160907|stawiająca czoła wile w Dracenie, ściągająca Netherię i czująca się bezradnie w obliczu Skażenia Draceny.|[Dracena... Blakenbauer?](/rpg/inwazja/opowiesci/konspekty/160907-dracena-blakenbauer.html)|10/06/25|10/06/26|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|
|170423|ratuje życia, detoksyfikuje górników, zleca Wiaczesławowi problemy z narkotykami i Korzuniowi z biurokracją. Wciąż nie zwraca na siebie szczególnej uwagi.|[Rozpad magii w Leere](/rpg/inwazja/opowiesci/konspekty/170423-rozpad-magii-w-leere.html)|10/06/24|10/06/27|[Ucieczka do Przodka](/rpg/inwazja/opowiesci/konspekty/kampania-ucieczka-do-przodka.html)|
|160904|znękana i zmaltretowana lekarka Draceny, próbująca rozpaczliwie ją "naprawić". Ściągnęła Kingę do pomocy.|[Rozpaczliwie sterowany wzór](/rpg/inwazja/opowiesci/konspekty/160904-rozpaczliwie-sterowany-wzor.html)|10/06/21|10/06/24|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|
|160901|której obie strony zapłaciły za ratowanie ludzi - i która silnie ratowała każde ludzkie istnienie (choć jednego nie uratowała)|[Uwięziony w komputerze!](/rpg/inwazja/opowiesci/konspekty/160901-uwieziony-w-komputerze.html)|10/06/18|10/06/20|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|
|141110|która dokonuje dwóch trudnych wyborów - co do Nikoli i co do Draceny... I co do żadnego nie jest w 100% przekonana.|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html)|10/06/16|10/06/17|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|
|141102|przekonująca audytorów, że naprawdę chce dobrze dla Nikoli i robi, co może, w międzyczasie rozgrywająca prawników Świecy i Milenium przeciwko sobie.|[Paulina widziała sępy nad Nikolą](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html)|10/06/14|10/06/15|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|
|160117|która pozyskała Korzunia jako alianta i definitywnie zamknęła temat relikwii i kultu w Przodku.|[Muchy w sieci Korzunia](/rpg/inwazja/opowiesci/konspekty/160117-muchy-w-sieci-korzunia.html)|10/06/12|10/06/13|[Ucieczka do Przodka](/rpg/inwazja/opowiesci/konspekty/kampania-ucieczka-do-przodka.html)|
|151229|która wpadła z czarodziejki pod sektę; celnym użyciem * magii poznała strony konfliktu.|[..choć to na sektę nie pomoże (PT)](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html)|10/06/12|10/06/13|[Ucieczka do Przodka](/rpg/inwazja/opowiesci/konspekty/kampania-ucieczka-do-przodka.html)|
|141026|prowadząca negocjacje pomiędzy gildiami i magami dużo potężniejszymi od siebie. Żongluje potężnymi siłami tak, by jak najmniej istot ucierpiało i w jak najmniejszym stopniu.|[Dracena widziała swój koszmar](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html)|10/06/12|10/06/13|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|
|160106|która nie tylko uratowała księdza przed mordem rytualnym, ale też kilka osób przed * członkostwem w sekcie.|[...i kult zostaje rozgromiony](/rpg/inwazja/opowiesci/konspekty/160106-i-kult-zostaje-rozgromiony.html)|10/06/10|10/06/11|[Ucieczka do Przodka](/rpg/inwazja/opowiesci/konspekty/kampania-ucieczka-do-przodka.html)|
|151223|która przekształciła defilerkę-zabawkę w pełnoprawną czarodziejkę NIE defilerkę. Naprawdę pomogła.|[..można uwolnić czarodziejkę...](/rpg/inwazja/opowiesci/konspekty/151223-mozna-uwolnic-czarodziejke.html)|10/06/10|10/06/11|[Ucieczka do Przodka](/rpg/inwazja/opowiesci/konspekty/kampania-ucieczka-do-przodka.html)|
|141025|robiąca co może, aby dać Nikoli pełny wybór i w miarę możliwości chroniąca Dracenę.|[Gildie widziały protomaga](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|10/06/10|10/06/11|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|
|151230|ta, która pociąga za wszystkie sznurki: Zajcew, policja, dziennikarze, Maria... "nic nie robi" a dzięki niej wiele się dzieje. Aka "Ania Diakon".|[Zajcew ze śmietnika partnerem...](/rpg/inwazja/opowiesci/konspekty/151230-zajcew-ze-smietnika-partnerem.html)|10/06/08|10/06/09|[Ucieczka do Przodka](/rpg/inwazja/opowiesci/konspekty/kampania-ucieczka-do-przodka.html)|
|151220|która wreszcie doszła do tego co stało się z Marzeną i jak silnym * magiem był Adam. Plus, przeniosła NullField i przygotowała "laboratorium".|[Z Null Fieldem w garażu...](/rpg/inwazja/opowiesci/konspekty/151220-z-nullfieldem-w-garazu.html)|10/06/08|10/06/09|[Ucieczka do Przodka](/rpg/inwazja/opowiesci/konspekty/kampania-ucieczka-do-przodka.html)|
|141019|ostro ścierająca się z Draceną odnośnie losu Nikoli, ale nie uważająca czarodziejki za zagrożenie.|[Lekarka widziała cybergothkę](/rpg/inwazja/opowiesci/konspekty/141019-lekarka-widziala-cybergothke.html)|10/06/08|10/06/09|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|
|151101|która odkryła czarodziejkę w Szpitalu Gotyckim i dała radę wejść z nią w pewną współpracę. Planuje ją wyleczyć. Też, uratowała przed Galą NullField.|[Mafia Gali w szpitalu](/rpg/inwazja/opowiesci/konspekty/151101-mafia-gali-w-szpitalu.html)|10/06/06|10/06/07|[Ucieczka do Przodka](/rpg/inwazja/opowiesci/konspekty/kampania-ucieczka-do-przodka.html)|
|151013|którą porwał Tymek i znarkotyzował, po czym uratowała życie grzybiarza i poszła na kawę i kolację z kolegą z pracy.|[Kontrolowany odwrót z zamtuza](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html)|10/06/04|10/06/05|[Ucieczka do Przodka](/rpg/inwazja/opowiesci/konspekty/kampania-ucieczka-do-przodka.html)|
|151003|która uwolniła wiłę by zostać zauważoną przez księdza; nadal jest niezauważona, ale ciężko na to pracuje...|[Zamtuz przestaje działać](/rpg/inwazja/opowiesci/konspekty/151003-zamtuz-przestaje-dzialac.html)|10/06/02|10/06/03|[Ucieczka do Przodka](/rpg/inwazja/opowiesci/konspekty/kampania-ucieczka-do-przodka.html)|
|150928|która zaczyna nowe życie w Przodku i unika wykrycia przez podejrzliwego dyrektora.|[Zamtuz z jedną wiłą](/rpg/inwazja/opowiesci/konspekty/150928-zamtuz-z-jedna-wila.html)|10/05/31|10/06/01|[Ucieczka do Przodka](/rpg/inwazja/opowiesci/konspekty/kampania-ucieczka-do-przodka.html)|
|141012|główna rozgrywająca brużdżąca EAMce i pomagająca niewinnej Nikoli|[Aplikanci widzieli gorathaula](/rpg/inwazja/opowiesci/konspekty/141012-aplikanci-widzieli-gorathaula.html)|10/05/30|10/06/07|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|
|150830|która rozwiązała problem Kasi Kotek w Powiewie i była świadkiem zabrania trumny przez Ozydiusza.|[Kasia, nie EIS w Powiewie](/rpg/inwazja/opowiesci/konspekty/150830-kasia-nie-eis-w-powiewie.html)|10/05/29|10/05/30|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150829|lekarz * magiczny, która uratowała bazę Skorpiona przed straszliwą hybrydą * viciniusów.|[Hybryda w bazie Skorpiona](/rpg/inwazja/opowiesci/konspekty/150829-hybryda-w-bazie-skorpiona.html)|10/05/27|10/05/28|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150826|która stała się symbolem wbrew swojej woli i która odbiła się od bariery gildii (bycia poza nią).|[Pętla dookoła Pauliny](/rpg/inwazja/opowiesci/konspekty/150826-petla-dookola-pauliny.html)|10/05/25|10/05/26|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150823|bohaterka trzymająca Węzeł, heroiczny lekarz i twórczyni Kasi Kotek. Sojusznik Iliusitiusa.|[Atak na sanktuarium Estrelli](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|10/05/23|10/05/24|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|140928|uświadamiająca Sebastianowi, że mógł mieć miłość swojego życia i robiąca idiotkę z EAMki.|[Policjant widział anioła](/rpg/inwazja/opowiesci/konspekty/140928-policjant-widzial-aniola.html)|10/05/12|10/05/13|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|
|150716|lekarz, która zidentyfikowała Lodowy Pocałunek i bardzo dużo ryzykowała, by chora terminuska przeżyła.|[Chora terminuska i Żabolód](/rpg/inwazja/opowiesci/konspekty/150716-chora-terminuska-i-zabolod.html)|10/05/09|10/05/10|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150406|współpracująca z Powiewem Świeżości nekromantka i lekarka, która nie chce, by aptoform trafił do Blakenbauerów.|[Aurelia za aptoforma](/rpg/inwazja/opowiesci/konspekty/150406-aurelia-za-aptoforma.html)|10/05/01|10/05/02|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150325|czarodziejka mająca pierwszy kontakt z Blakenbauerami i szybko decydująca, że nie chce więcej tych kontaktów.|[Morderstwo jak w książce](/rpg/inwazja/opowiesci/konspekty/150325-morderstwo-jak-w-ksiazce.html)|10/04/29|10/04/30|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170312|ratująca życia ludzi mimo niechęci Pameli i życia magów mimo niechęci "Harvestera". Ufa tylko Kajetanowi, bo też chce pomóc...|[Przebudzony... Harvester?](/rpg/inwazja/opowiesci/konspekty/170312-przebudzony-harvester.html)|10/03/17|10/03/19|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|
|160227|odkryła mroczną prawdę za pojawieniem się efemerydy - Harvester Weinerów.|[Zakazany harvester](/rpg/inwazja/opowiesci/konspekty/160227-zakazany-harvester.html)|10/03/12|10/03/13|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|
|160131|po wezwaniu wsparcia i uratowaniu życia higienistce destabilizacja efemerydy była już formalnością.|[Dziwny transmiter Weinerów](/rpg/inwazja/opowiesci/konspekty/160131-dziwny-transmiter-weinerow.html)|10/03/10|10/03/11|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|
|160124|odmontowała trójkę dzieciaków od efemerydy i została "panią doktor rajdowiec". Jaka wieś, taki Bond.|[Trzy opętane duszyczki](/rpg/inwazja/opowiesci/konspekty/160124-trzy-opetane-duszyczki.html)|10/03/08|10/03/09|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|
|170326|lekarka ratująca Eneusa, potem ludzi, stawiająca Wiaczesława, przekonywująca Archibalda... jeden raz jak nic nie zrobiła wszystko wybuchło.|[Cała przeszłość spłonęła](/rpg/inwazja/opowiesci/konspekty/170326-cala-przeszlosc-splonela.html)|10/03/04|10/03/07|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|
|170325|z zaskoczenia pokonuje konstruminusa i klątwożyta; korzystając ze znajomości zdobywa broń przeciw klątwożytom i weszła w częściowe łaski Gabriela Dukata.|[Klątwożyt z lustra](/rpg/inwazja/opowiesci/konspekty/170325-klatwozyt-z-lustra.html)|10/02/28|10/03/03|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|
|170323|dzielnie walcząca z randomowym lustrem i uszkodzonym czujnikiem wody. Nie jest to najbardziej ambitny moment jej życia.|[Oszczędzili na rurach](/rpg/inwazja/opowiesci/konspekty/170323-oszczedzili-na-rurach.html)|10/02/20|10/02/24|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|
|170409|usiłująca zrozumieć co się dzieje na Pentacyklu i powstrzymać ich egzekucję przez swoich sojuszników|[Nie zabijajmy tych magów](/rpg/inwazja/opowiesci/konspekty/170409-nie-zabijajmy-tych-magow.html)|10/02/16|10/02/19|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|
|170404|z którą zamieszkała Wąż. Uratowała człowieka, któremu rozsypał się Wzór (i tak na serio, całą jej rodzinę).|[Wąż jako vicinius Pauliny](/rpg/inwazja/opowiesci/konspekty/170404-waz-jako-vicinius-pauliny.html)|10/02/10|10/02/13|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|
|170331|kiepsko porwana, ratująca życie ofiary areny, niechcący prowokująca Wąż i planująca jak NIE zabić złego maga.|[Kiepsko porwana Paulina](/rpg/inwazja/opowiesci/konspekty/170331-kiepsko-porwana-paulina.html)|10/02/07|10/02/09|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|
|170723|pozornie nic nie wiedziała, ale to jej działania doprowadziły do tego, by Diana odrzuciła Kaję i Ewelinę i związała się z Hektorem. Wymanewrowała wszystkich.|[Wywalczone życie Diany](/rpg/inwazja/opowiesci/konspekty/170723-wywalczone-zycie-diany.html)|10/02/03|10/02/06|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|
|170702|udaremniła facetożerną Ewelinę i Kaję i doprowadziła do sparowaniu Hektora i Dianę. Też: umieściła Prosperjusza tam, gdzie jej się przyda.|[Miłość przez desperację](/rpg/inwazja/opowiesci/konspekty/170702-milosc-przez-desperacje.html)|10/01/29|10/02/02|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|
|170614|w trybie kryzysowym; straciła ogródek, ale uratowała Hektora i zablokowała podły plan Grazoniusza. Pokazała ząbki jako rezydentka.|[Kryzys przez eliksir](/rpg/inwazja/opowiesci/konspekty/170614-kryzys-przez-eliksir.html)|10/01/27|10/01/28|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|
|170518|rozpaczliwie łagodzi wszystkie możliwe konflikty między magami... ma też noc swojego życia ;-). Z owadami. Troszkę swata Dianę i Hektora.|[Machinacje maga rolniczego](/rpg/inwazja/opowiesci/konspekty/170518-machinacje-maga-rolniczego.html)|10/01/23|10/01/25|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|
|170515|poza standardowymi działaniami lekarza-rezydenta wpadła jej na głowę jedna "niewolnica" i jeden pacjent. Próbuje powstrzymać młodych od głupstw.|[Niewolnica w leasingu](/rpg/inwazja/opowiesci/konspekty/170515-niewolnica-w-leasingu.html)|10/01/20|10/01/22|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|
|170510|która próbowała rozładować Skażone poletko kukurydzy a wplątała się w walkę z dziwną drzewną efemerydą. I zaopiekowała się dwójką młodych magów...|[Najgorsze love story](/rpg/inwazja/opowiesci/konspekty/170510-najgorsze-love-story.html)|10/01/16|10/01/19|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|
|170716|rezydentka znajdująca i niszcząca Artefakt Spontaniczny; utrzymała groźnego upiora u siebie w domu do czasu przybycia terminusa i leczyła ludzi.|[Eteryczny chłopiec i jego pies](/rpg/inwazja/opowiesci/konspekty/170716-eteryczny-chlopiec-i-jego-pies.html)|10/01/08|10/01/10|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|
|150501|lekarz, która otarła się o moc Iliusitiusa i przeżyła. Dodatkowo uratowała Zależe przed wojną kult - czarodziejka.|[Szalona 'czarodziejka' Zależa](/rpg/inwazja/opowiesci/konspekty/150501-szalona-czarodziejka-zaleza.html)|10/01/07|10/01/08|[Światło w Zależu Leśnym](/rpg/inwazja/opowiesci/konspekty/kampania-swiatlo-w-zalezu-lesnym.html)|
|170523|mag-rezydent i lekarz. Ratuje Dobrocienia i po zrozumieniu sprawy ducha kradnącego magitech medyczny montuje koalicję by ducha rozłożyć i by nikomu nic się złego nie stało. Też ekonomicznie ;-).|[Opętany konstruminus](/rpg/inwazja/opowiesci/konspekty/170523-opetany-konstruminus.html)|10/01/03|10/01/04|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|
|140910|lekarka z uszkodzonym soullinkiem która próbuje uratować Marię przed nią samą.|[Reporter kontra Blakenbauerzy](/rpg/inwazja/opowiesci/konspekty/140910-reporter-kontra-blakenbauerzy.html)|10/01/01|10/01/02|[Nie umieszczone, Anulowane](/rpg/inwazja/opowiesci/konspekty/kampania-anulowane.html)|
|140819|niezauważalnie uszkodzona lekarka nie pamiętająca dokładnie Marii.|[Marcelin w klasztorze!](/rpg/inwazja/opowiesci/konspekty/140819-marcelin-w-klasztorze.html)|10/01/01|10/01/02|[Nie umieszczone, Anulowane](/rpg/inwazja/opowiesci/konspekty/kampania-anulowane.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/1709-maria-newa.html)|33|[171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html), [171022](/rpg/inwazja/opowiesci/konspekty/171022-tylko-nieswiadomy-elemental.html), [171015](/rpg/inwazja/opowiesci/konspekty/171015-powstrzymana-wojna-domowa.html), [171218](/rpg/inwazja/opowiesci/konspekty/171218-senesgradzka-kopia-pauliny.html), [171001](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html), [170212](/rpg/inwazja/opowiesci/konspekty/170212-nieufnosc-w-malym-miescie.html), [161009](/rpg/inwazja/opowiesci/konspekty/161009-paulino-zmienilas-sie.html), [161002](/rpg/inwazja/opowiesci/konspekty/161002-wyciek-syberionu.html), [170423](/rpg/inwazja/opowiesci/konspekty/170423-rozpad-magii-w-leere.html), [160904](/rpg/inwazja/opowiesci/konspekty/160904-rozpaczliwie-sterowany-wzor.html), [141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html), [141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [160106](/rpg/inwazja/opowiesci/konspekty/160106-i-kult-zostaje-rozgromiony.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html), [151230](/rpg/inwazja/opowiesci/konspekty/151230-zajcew-ze-smietnika-partnerem.html), [151220](/rpg/inwazja/opowiesci/konspekty/151220-z-nullfieldem-w-garazu.html), [141019](/rpg/inwazja/opowiesci/konspekty/141019-lekarka-widziala-cybergothke.html), [151101](/rpg/inwazja/opowiesci/konspekty/151101-mafia-gali-w-szpitalu.html), [151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html), [151003](/rpg/inwazja/opowiesci/konspekty/151003-zamtuz-przestaje-dzialac.html), [150928](/rpg/inwazja/opowiesci/konspekty/150928-zamtuz-z-jedna-wila.html), [141012](/rpg/inwazja/opowiesci/konspekty/141012-aplikanci-widzieli-gorathaula.html), [150830](/rpg/inwazja/opowiesci/konspekty/150830-kasia-nie-eis-w-powiewie.html), [150829](/rpg/inwazja/opowiesci/konspekty/150829-hybryda-w-bazie-skorpiona.html), [150826](/rpg/inwazja/opowiesci/konspekty/150826-petla-dookola-pauliny.html), [150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html), [140928](/rpg/inwazja/opowiesci/konspekty/140928-policjant-widzial-aniola.html), [150716](/rpg/inwazja/opowiesci/konspekty/150716-chora-terminuska-i-zabolod.html), [150406](/rpg/inwazja/opowiesci/konspekty/150406-aurelia-za-aptoforma.html), [150325](/rpg/inwazja/opowiesci/konspekty/150325-morderstwo-jak-w-ksiazce.html), [140910](/rpg/inwazja/opowiesci/konspekty/140910-reporter-kontra-blakenbauerzy.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|22|[171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html), [171031](/rpg/inwazja/opowiesci/konspekty/171031-utracona-kontrola.html), [171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html), [171022](/rpg/inwazja/opowiesci/konspekty/171022-tylko-nieswiadomy-elemental.html), [171015](/rpg/inwazja/opowiesci/konspekty/171015-powstrzymana-wojna-domowa.html), [171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html), [171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html), [171218](/rpg/inwazja/opowiesci/konspekty/171218-senesgradzka-kopia-pauliny.html), [171001](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html), [170212](/rpg/inwazja/opowiesci/konspekty/170212-nieufnosc-w-malym-miescie.html), [161009](/rpg/inwazja/opowiesci/konspekty/161009-paulino-zmienilas-sie.html), [161002](/rpg/inwazja/opowiesci/konspekty/161002-wyciek-syberionu.html), [160918](/rpg/inwazja/opowiesci/konspekty/160918-walka-o-dusze-draceny.html), [160910](/rpg/inwazja/opowiesci/konspekty/160910-na-zywym-organizmie-draceny.html), [160907](/rpg/inwazja/opowiesci/konspekty/160907-dracena-blakenbauer.html), [160904](/rpg/inwazja/opowiesci/konspekty/160904-rozpaczliwie-sterowany-wzor.html), [160901](/rpg/inwazja/opowiesci/konspekty/160901-uwieziony-w-komputerze.html), [141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html), [141019](/rpg/inwazja/opowiesci/konspekty/141019-lekarka-widziala-cybergothke.html)|
|[Wiaczesław Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-wiaczeslaw-zajcew.html)|10|[180110](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html), [171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html), [171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html), [171015](/rpg/inwazja/opowiesci/konspekty/171015-powstrzymana-wojna-domowa.html), [170423](/rpg/inwazja/opowiesci/konspekty/170423-rozpad-magii-w-leere.html), [170326](/rpg/inwazja/opowiesci/konspekty/170326-cala-przeszlosc-splonela.html), [170325](/rpg/inwazja/opowiesci/konspekty/170325-klatwozyt-z-lustra.html), [170409](/rpg/inwazja/opowiesci/konspekty/170409-nie-zabijajmy-tych-magow.html), [170404](/rpg/inwazja/opowiesci/konspekty/170404-waz-jako-vicinius-pauliny.html), [170331](/rpg/inwazja/opowiesci/konspekty/170331-kiepsko-porwana-paulina.html)|
|[Kajetan Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-kajetan-weiner.html)|10|[171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html), [171024](/rpg/inwazja/opowiesci/konspekty/171024-detektyw-lecz-nie-sadecznego.html), [171022](/rpg/inwazja/opowiesci/konspekty/171022-tylko-nieswiadomy-elemental.html), [171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html), [170212](/rpg/inwazja/opowiesci/konspekty/170212-nieufnosc-w-malym-miescie.html), [160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [161113](/rpg/inwazja/opowiesci/konspekty/161113-swieca-nie-zostawia-swoich.html), [170312](/rpg/inwazja/opowiesci/konspekty/170312-przebudzony-harvester.html), [160227](/rpg/inwazja/opowiesci/konspekty/160227-zakazany-harvester.html), [160131](/rpg/inwazja/opowiesci/konspekty/160131-dziwny-transmiter-weinerow.html)|
|[Artur Kurczak](/rpg/inwazja/opowiesci/karty-postaci/9999-artur-kurczak.html)|10|[170423](/rpg/inwazja/opowiesci/konspekty/170423-rozpad-magii-w-leere.html), [141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html), [151230](/rpg/inwazja/opowiesci/konspekty/151230-zajcew-ze-smietnika-partnerem.html), [151220](/rpg/inwazja/opowiesci/konspekty/151220-z-nullfieldem-w-garazu.html), [141019](/rpg/inwazja/opowiesci/konspekty/141019-lekarka-widziala-cybergothke.html), [151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html), [140928](/rpg/inwazja/opowiesci/konspekty/140928-policjant-widzial-aniola.html)|
|[Krystian Korzunio](/rpg/inwazja/opowiesci/karty-postaci/1709-krystian-korzunio.html)|9|[161002](/rpg/inwazja/opowiesci/konspekty/161002-wyciek-syberionu.html), [160918](/rpg/inwazja/opowiesci/konspekty/160918-walka-o-dusze-draceny.html), [170423](/rpg/inwazja/opowiesci/konspekty/170423-rozpad-magii-w-leere.html), [160117](/rpg/inwazja/opowiesci/konspekty/160117-muchy-w-sieci-korzunia.html), [160106](/rpg/inwazja/opowiesci/konspekty/160106-i-kult-zostaje-rozgromiony.html), [151230](/rpg/inwazja/opowiesci/konspekty/151230-zajcew-ze-smietnika-partnerem.html), [151220](/rpg/inwazja/opowiesci/konspekty/151220-z-nullfieldem-w-garazu.html), [151101](/rpg/inwazja/opowiesci/konspekty/151101-mafia-gali-w-szpitalu.html), [151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html)|
|[Sylwester Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-sylwester-bankierz.html)|8|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html), [171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html), [171101](/rpg/inwazja/opowiesci/konspekty/171101-magimedy-przed-epidemia.html), [171031](/rpg/inwazja/opowiesci/konspekty/171031-utracona-kontrola.html), [171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html), [171015](/rpg/inwazja/opowiesci/konspekty/171015-powstrzymana-wojna-domowa.html), [171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html), [171001](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html)|
|[Hektor Reszniaczek](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-reszniaczek.html)|8|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html), [171001](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html), [170723](/rpg/inwazja/opowiesci/konspekty/170723-wywalczone-zycie-diany.html), [170702](/rpg/inwazja/opowiesci/konspekty/170702-milosc-przez-desperacje.html), [170614](/rpg/inwazja/opowiesci/konspekty/170614-kryzys-przez-eliksir.html), [170518](/rpg/inwazja/opowiesci/konspekty/170518-machinacje-maga-rolniczego.html), [170515](/rpg/inwazja/opowiesci/konspekty/170515-niewolnica-w-leasingu.html), [170510](/rpg/inwazja/opowiesci/konspekty/170510-najgorsze-love-story.html)|
|[Daniel Akwitański](/rpg/inwazja/opowiesci/karty-postaci/1709-daniel-akwitanski.html)|8|[180214](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html), [180110](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html), [171220](/rpg/inwazja/opowiesci/konspekty/171220-potrojna-magitrownia-histogram.html), [171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html), [171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html), [171031](/rpg/inwazja/opowiesci/konspekty/171031-utracona-kontrola.html), [171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html), [171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Bartosz Bławatek](/rpg/inwazja/opowiesci/karty-postaci/9999-bartosz-blawatek.html)|8|[141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html), [141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [160106](/rpg/inwazja/opowiesci/konspekty/160106-i-kult-zostaje-rozgromiony.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html), [151230](/rpg/inwazja/opowiesci/konspekty/151230-zajcew-ze-smietnika-partnerem.html), [151220](/rpg/inwazja/opowiesci/konspekty/151220-z-nullfieldem-w-garazu.html), [151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html)|
|[Tomasz Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-tomasz-myszeczka.html)|7|[180214](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html), [180110](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html), [171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html), [171101](/rpg/inwazja/opowiesci/konspekty/171101-magimedy-przed-epidemia.html), [171031](/rpg/inwazja/opowiesci/konspekty/171031-utracona-kontrola.html), [171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html), [171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Robert Sądeczny](/rpg/inwazja/opowiesci/karty-postaci/1709-robert-sadeczny.html)|7|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html), [171101](/rpg/inwazja/opowiesci/konspekty/171101-magimedy-przed-epidemia.html), [171031](/rpg/inwazja/opowiesci/konspekty/171031-utracona-kontrola.html), [171024](/rpg/inwazja/opowiesci/konspekty/171024-detektyw-lecz-nie-sadecznego.html), [171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html), [171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html), [171001](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html)|
|[Nikola Kamień](/rpg/inwazja/opowiesci/karty-postaci/9999-nikola-kamien.html)|7|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html), [141019](/rpg/inwazja/opowiesci/konspekty/141019-lekarka-widziala-cybergothke.html), [141012](/rpg/inwazja/opowiesci/konspekty/141012-aplikanci-widzieli-gorathaula.html), [140928](/rpg/inwazja/opowiesci/konspekty/140928-policjant-widzial-aniola.html)|
|[Kaja Maślaczek](/rpg/inwazja/opowiesci/karty-postaci/1709-kaja-maslaczek.html)|7|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html), [171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html), [171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html), [171218](/rpg/inwazja/opowiesci/konspekty/171218-senesgradzka-kopia-pauliny.html), [171001](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html), [170723](/rpg/inwazja/opowiesci/konspekty/170723-wywalczone-zycie-diany.html), [170702](/rpg/inwazja/opowiesci/konspekty/170702-milosc-przez-desperacje.html)|
|[Rafael Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-rafael-diakon.html)|6|[161113](/rpg/inwazja/opowiesci/konspekty/161113-swieca-nie-zostawia-swoich.html), [141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html), [141019](/rpg/inwazja/opowiesci/konspekty/141019-lekarka-widziala-cybergothke.html)|
|[Jerzy Karmelik](/rpg/inwazja/opowiesci/karty-postaci/9999-jerzy-karmelik.html)|6|[151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html), [151223](/rpg/inwazja/opowiesci/konspekty/151223-mozna-uwolnic-czarodziejke.html), [151230](/rpg/inwazja/opowiesci/konspekty/151230-zajcew-ze-smietnika-partnerem.html), [151220](/rpg/inwazja/opowiesci/konspekty/151220-z-nullfieldem-w-garazu.html), [151101](/rpg/inwazja/opowiesci/konspekty/151101-mafia-gali-w-szpitalu.html), [151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html)|
|[Efemeryda Senesgradzka](/rpg/inwazja/opowiesci/karty-postaci/9999-efemeryda-senesgradzka.html)|6|[171220](/rpg/inwazja/opowiesci/konspekty/171220-potrojna-magitrownia-histogram.html), [171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html), [171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html), [171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html), [171218](/rpg/inwazja/opowiesci/konspekty/171218-senesgradzka-kopia-pauliny.html), [170523](/rpg/inwazja/opowiesci/konspekty/170523-opetany-konstruminus.html)|
|[Diana Łuczkiewicz](/rpg/inwazja/opowiesci/karty-postaci/9999-diana-luczkiewicz.html)|6|[170723](/rpg/inwazja/opowiesci/konspekty/170723-wywalczone-zycie-diany.html), [170702](/rpg/inwazja/opowiesci/konspekty/170702-milosc-przez-desperacje.html), [170614](/rpg/inwazja/opowiesci/konspekty/170614-kryzys-przez-eliksir.html), [170518](/rpg/inwazja/opowiesci/konspekty/170518-machinacje-maga-rolniczego.html), [170515](/rpg/inwazja/opowiesci/konspekty/170515-niewolnica-w-leasingu.html), [170510](/rpg/inwazja/opowiesci/konspekty/170510-najgorsze-love-story.html)|
|[Tamara Muszkiet](/rpg/inwazja/opowiesci/karty-postaci/1709-tamara-muszkiet.html)|5|[171101](/rpg/inwazja/opowiesci/konspekty/171101-magimedy-przed-epidemia.html), [171031](/rpg/inwazja/opowiesci/konspekty/171031-utracona-kontrola.html), [171015](/rpg/inwazja/opowiesci/konspekty/171015-powstrzymana-wojna-domowa.html), [171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html), [150716](/rpg/inwazja/opowiesci/konspekty/150716-chora-terminuska-i-zabolod.html)|
|[Ryszard Bocian](/rpg/inwazja/opowiesci/karty-postaci/9999-ryszard-bocian.html)|5|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html), [141019](/rpg/inwazja/opowiesci/konspekty/141019-lekarka-widziala-cybergothke.html)|
|[Roman Gieroj](/rpg/inwazja/opowiesci/karty-postaci/9999-roman-gieroj.html)|5|[150830](/rpg/inwazja/opowiesci/konspekty/150830-kasia-nie-eis-w-powiewie.html), [150829](/rpg/inwazja/opowiesci/konspekty/150829-hybryda-w-bazie-skorpiona.html), [150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html), [150716](/rpg/inwazja/opowiesci/konspekty/150716-chora-terminuska-i-zabolod.html), [150406](/rpg/inwazja/opowiesci/konspekty/150406-aurelia-za-aptoforma.html)|
|[Olga Miodownik](/rpg/inwazja/opowiesci/karty-postaci/1709-olga-miodownik.html)|5|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [150829](/rpg/inwazja/opowiesci/konspekty/150829-hybryda-w-bazie-skorpiona.html), [150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html), [150325](/rpg/inwazja/opowiesci/konspekty/150325-morderstwo-jak-w-ksiazce.html), [150501](/rpg/inwazja/opowiesci/konspekty/150501-szalona-czarodziejka-zaleza.html)|
|[Netheria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-netheria-diakon.html)|5|[160918](/rpg/inwazja/opowiesci/konspekty/160918-walka-o-dusze-draceny.html), [160910](/rpg/inwazja/opowiesci/konspekty/160910-na-zywym-organizmie-draceny.html), [160907](/rpg/inwazja/opowiesci/konspekty/160907-dracena-blakenbauer.html), [150826](/rpg/inwazja/opowiesci/konspekty/150826-petla-dookola-pauliny.html), [150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|
|[Jakub Dobrocień](/rpg/inwazja/opowiesci/karty-postaci/1709-jakub-dobrocien.html)|5|[171101](/rpg/inwazja/opowiesci/konspekty/171101-magimedy-przed-epidemia.html), [171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html), [171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html), [170518](/rpg/inwazja/opowiesci/konspekty/170518-machinacje-maga-rolniczego.html), [170523](/rpg/inwazja/opowiesci/konspekty/170523-opetany-konstruminus.html)|
|[Grażyna Tuloz](/rpg/inwazja/opowiesci/karty-postaci/9999-grazyna-tuloz.html)|5|[170423](/rpg/inwazja/opowiesci/konspekty/170423-rozpad-magii-w-leere.html), [160117](/rpg/inwazja/opowiesci/konspekty/160117-muchy-w-sieci-korzunia.html), [151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html), [160106](/rpg/inwazja/opowiesci/konspekty/160106-i-kult-zostaje-rozgromiony.html), [151223](/rpg/inwazja/opowiesci/konspekty/151223-mozna-uwolnic-czarodziejke.html)|
|[Franciszek Marlin](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-marlin.html)|5|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html), [151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html)|
|[Alicja Gąszcz](/rpg/inwazja/opowiesci/karty-postaci/9999-alicja-gaszcz.html)|5|[160117](/rpg/inwazja/opowiesci/konspekty/160117-muchy-w-sieci-korzunia.html), [151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html), [141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [151230](/rpg/inwazja/opowiesci/konspekty/151230-zajcew-ze-smietnika-partnerem.html), [151220](/rpg/inwazja/opowiesci/konspekty/151220-z-nullfieldem-w-garazu.html)|
|[Łukasz Perkas](/rpg/inwazja/opowiesci/karty-postaci/9999-lukasz-perkas.html)|4|[160117](/rpg/inwazja/opowiesci/konspekty/160117-muchy-w-sieci-korzunia.html), [151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html), [151230](/rpg/inwazja/opowiesci/konspekty/151230-zajcew-ze-smietnika-partnerem.html), [151220](/rpg/inwazja/opowiesci/konspekty/151220-z-nullfieldem-w-garazu.html)|
|[Marzena Dorszaj](/rpg/inwazja/opowiesci/karty-postaci/1709-marzena-dorszaj.html)|4|[151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html), [151223](/rpg/inwazja/opowiesci/konspekty/151223-mozna-uwolnic-czarodziejke.html), [151220](/rpg/inwazja/opowiesci/konspekty/151220-z-nullfieldem-w-garazu.html), [151101](/rpg/inwazja/opowiesci/konspekty/151101-mafia-gali-w-szpitalu.html)|
|[Kinga Toczek](/rpg/inwazja/opowiesci/karty-postaci/9999-kinga-toczek.html)|4|[160918](/rpg/inwazja/opowiesci/konspekty/160918-walka-o-dusze-draceny.html), [160910](/rpg/inwazja/opowiesci/konspekty/160910-na-zywym-organizmie-draceny.html), [160907](/rpg/inwazja/opowiesci/konspekty/160907-dracena-blakenbauer.html), [160904](/rpg/inwazja/opowiesci/konspekty/160904-rozpaczliwie-sterowany-wzor.html)|
|[Katarzyna Trzosek](/rpg/inwazja/opowiesci/karty-postaci/9999-katarzyna-trzosek.html)|4|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html), [141019](/rpg/inwazja/opowiesci/konspekty/141019-lekarka-widziala-cybergothke.html)|
|[Karol Marzyciel](/rpg/inwazja/opowiesci/karty-postaci/1709-karol-marzyciel.html)|4|[171220](/rpg/inwazja/opowiesci/konspekty/171220-potrojna-magitrownia-histogram.html), [171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html), [171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html), [171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Jan Bocian](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-bocian.html)|4|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|4|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [150325](/rpg/inwazja/opowiesci/konspekty/150325-morderstwo-jak-w-ksiazce.html), [140910](/rpg/inwazja/opowiesci/konspekty/140910-reporter-kontra-blakenbauerzy.html), [140819](/rpg/inwazja/opowiesci/konspekty/140819-marcelin-w-klasztorze.html)|
|[Grazoniusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-grazoniusz-bankierz.html)|4|[170723](/rpg/inwazja/opowiesci/konspekty/170723-wywalczone-zycie-diany.html), [170702](/rpg/inwazja/opowiesci/konspekty/170702-milosc-przez-desperacje.html), [170614](/rpg/inwazja/opowiesci/konspekty/170614-kryzys-przez-eliksir.html), [170518](/rpg/inwazja/opowiesci/konspekty/170518-machinacje-maga-rolniczego.html)|
|[Gala Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-gala-zajcew.html)|4|[151101](/rpg/inwazja/opowiesci/konspekty/151101-mafia-gali-w-szpitalu.html), [151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html), [151003](/rpg/inwazja/opowiesci/konspekty/151003-zamtuz-przestaje-dzialac.html), [150928](/rpg/inwazja/opowiesci/konspekty/150928-zamtuz-z-jedna-wila.html)|
|[Gabriel Dukat](/rpg/inwazja/opowiesci/karty-postaci/9999-gabriel-dukat.html)|4|[171015](/rpg/inwazja/opowiesci/konspekty/171015-powstrzymana-wojna-domowa.html), [171001](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html), [170326](/rpg/inwazja/opowiesci/konspekty/170326-cala-przeszlosc-splonela.html), [170325](/rpg/inwazja/opowiesci/konspekty/170325-klatwozyt-z-lustra.html)|
|[Ewelina Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-ewelina-bankierz.html)|4|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html), [170518](/rpg/inwazja/opowiesci/konspekty/170518-machinacje-maga-rolniczego.html), [170515](/rpg/inwazja/opowiesci/konspekty/170515-niewolnica-w-leasingu.html), [170510](/rpg/inwazja/opowiesci/konspekty/170510-najgorsze-love-story.html)|
|[Dyta](/rpg/inwazja/opowiesci/karty-postaci/9999-dyta.html)|4|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[Dalia Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-dalia-weiner.html)|4|[171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html), [171101](/rpg/inwazja/opowiesci/konspekty/171101-magimedy-przed-epidemia.html), [141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[Andżelika Leszczyńska](/rpg/inwazja/opowiesci/karty-postaci/1709-andzelika-leszczynska.html)|4|[171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html), [161113](/rpg/inwazja/opowiesci/konspekty/161113-swieca-nie-zostawia-swoich.html), [141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html)|
|[Łucja Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-lucja-maus.html)|3|[180214](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html), [180110](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html), [171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|
|[Wiesław Rekin](/rpg/inwazja/opowiesci/karty-postaci/9999-wieslaw-rekin.html)|3|[151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html), [160106](/rpg/inwazja/opowiesci/konspekty/160106-i-kult-zostaje-rozgromiony.html), [151230](/rpg/inwazja/opowiesci/konspekty/151230-zajcew-ze-smietnika-partnerem.html)|
|[Tymoteusz Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-tymoteusz-maus.html)|3|[151101](/rpg/inwazja/opowiesci/konspekty/151101-mafia-gali-w-szpitalu.html), [151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html), [151003](/rpg/inwazja/opowiesci/konspekty/151003-zamtuz-przestaje-dzialac.html)|
|[Tomasz Leżniak](/rpg/inwazja/opowiesci/karty-postaci/9999-tomasz-lezniak.html)|3|[160106](/rpg/inwazja/opowiesci/konspekty/160106-i-kult-zostaje-rozgromiony.html), [151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html), [151003](/rpg/inwazja/opowiesci/konspekty/151003-zamtuz-przestaje-dzialac.html)|
|[Terror Wąż](/rpg/inwazja/opowiesci/karty-postaci/1709-terror-waz.html)|3|[170409](/rpg/inwazja/opowiesci/konspekty/170409-nie-zabijajmy-tych-magow.html), [170404](/rpg/inwazja/opowiesci/konspekty/170404-waz-jako-vicinius-pauliny.html), [170331](/rpg/inwazja/opowiesci/konspekty/170331-kiepsko-porwana-paulina.html)|
|[Silgor Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-silgor-diakon.html)|3|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[Ryszard Herman](/rpg/inwazja/opowiesci/karty-postaci/9999-ryszard-herman.html)|3|[151220](/rpg/inwazja/opowiesci/konspekty/151220-z-nullfieldem-w-garazu.html), [151101](/rpg/inwazja/opowiesci/konspekty/151101-mafia-gali-w-szpitalu.html), [150928](/rpg/inwazja/opowiesci/konspekty/150928-zamtuz-z-jedna-wila.html)|
|[Ozydiusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-ozydiusz-bankierz.html)|3|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [150830](/rpg/inwazja/opowiesci/konspekty/150830-kasia-nie-eis-w-powiewie.html), [150826](/rpg/inwazja/opowiesci/konspekty/150826-petla-dookola-pauliny.html)|
|[Melodia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1801-melodia-diakon.html)|3|[180214](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html), [180110](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html), [171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|
|[Marian Łajdak](/rpg/inwazja/opowiesci/karty-postaci/1709-marian-lajdak.html)|3|[161113](/rpg/inwazja/opowiesci/konspekty/161113-swieca-nie-zostawia-swoich.html), [141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html)|
|[Margaret Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-margaret-blakenbauer.html)|3|[150325](/rpg/inwazja/opowiesci/konspekty/150325-morderstwo-jak-w-ksiazce.html), [140910](/rpg/inwazja/opowiesci/konspekty/140910-reporter-kontra-blakenbauerzy.html), [140819](/rpg/inwazja/opowiesci/konspekty/140819-marcelin-w-klasztorze.html)|
|[Marcin Warinsky](/rpg/inwazja/opowiesci/karty-postaci/1709-marcin-warinsky.html)|3|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html), [171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html), [171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Kurt Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-kurt-weiner.html)|3|[170212](/rpg/inwazja/opowiesci/konspekty/170212-nieufnosc-w-malym-miescie.html), [161009](/rpg/inwazja/opowiesci/konspekty/161009-paulino-zmienilas-sie.html), [161002](/rpg/inwazja/opowiesci/konspekty/161002-wyciek-syberionu.html)|
|[Kermit Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-kermit-diakon.html)|3|[150826](/rpg/inwazja/opowiesci/konspekty/150826-petla-dookola-pauliny.html), [150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html), [150716](/rpg/inwazja/opowiesci/konspekty/150716-chora-terminuska-i-zabolod.html)|
|[Kazimierz Przybylec](/rpg/inwazja/opowiesci/karty-postaci/9999-kazimierz-przybylec.html)|3|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [160131](/rpg/inwazja/opowiesci/konspekty/160131-dziwny-transmiter-weinerow.html)|
|[Katarzyna Kotek](/rpg/inwazja/opowiesci/karty-postaci/9999-katarzyna-kotek.html)|3|[150830](/rpg/inwazja/opowiesci/konspekty/150830-kasia-nie-eis-w-powiewie.html), [150829](/rpg/inwazja/opowiesci/konspekty/150829-hybryda-w-bazie-skorpiona.html), [150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|
|[Joachim Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-zajcew.html)|3|[160106](/rpg/inwazja/opowiesci/konspekty/160106-i-kult-zostaje-rozgromiony.html), [151230](/rpg/inwazja/opowiesci/konspekty/151230-zajcew-ze-smietnika-partnerem.html), [151220](/rpg/inwazja/opowiesci/konspekty/151220-z-nullfieldem-w-garazu.html)|
|[Jan Fiołek](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-fiolek.html)|3|[141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[Eneus Mucro](/rpg/inwazja/opowiesci/karty-postaci/9999-eneus-mucro.html)|3|[170326](/rpg/inwazja/opowiesci/konspekty/170326-cala-przeszlosc-splonela.html), [170325](/rpg/inwazja/opowiesci/konspekty/170325-klatwozyt-z-lustra.html), [170323](/rpg/inwazja/opowiesci/konspekty/170323-oszczedzili-na-rurach.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|3|[150325](/rpg/inwazja/opowiesci/konspekty/150325-morderstwo-jak-w-ksiazce.html), [140910](/rpg/inwazja/opowiesci/konspekty/140910-reporter-kontra-blakenbauerzy.html), [140819](/rpg/inwazja/opowiesci/konspekty/140819-marcelin-w-klasztorze.html)|
|[Edmund Marlin](/rpg/inwazja/opowiesci/karty-postaci/9999-edmund-marlin.html)|3|[141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html), [141019](/rpg/inwazja/opowiesci/konspekty/141019-lekarka-widziala-cybergothke.html)|
|[Bolesław Derwisz](/rpg/inwazja/opowiesci/karty-postaci/9999-boleslaw-derwisz.html)|3|[161002](/rpg/inwazja/opowiesci/konspekty/161002-wyciek-syberionu.html), [141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[Bogumił Miłoszept](/rpg/inwazja/opowiesci/karty-postaci/9999-bogumil-miloszept.html)|3|[171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html), [170723](/rpg/inwazja/opowiesci/konspekty/170723-wywalczone-zycie-diany.html), [170716](/rpg/inwazja/opowiesci/konspekty/170716-eteryczny-chlopiec-i-jego-pies.html)|
|[Aneta Rukolas](/rpg/inwazja/opowiesci/karty-postaci/9999-aneta-rukolas.html)|3|[171101](/rpg/inwazja/opowiesci/konspekty/171101-magimedy-przed-epidemia.html), [171015](/rpg/inwazja/opowiesci/konspekty/171015-powstrzymana-wojna-domowa.html), [170409](/rpg/inwazja/opowiesci/konspekty/170409-nie-zabijajmy-tych-magow.html)|
|[tien Radosław Pieśniec](/rpg/inwazja/opowiesci/karty-postaci/9999-tien-radoslaw-piesniec.html)|2|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html)|
|[Zofia Przylga](/rpg/inwazja/opowiesci/karty-postaci/1709-zofia-przylga.html)|2|[171031](/rpg/inwazja/opowiesci/konspekty/171031-utracona-kontrola.html), [171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Zofia Murczówik](/rpg/inwazja/opowiesci/karty-postaci/9999-zofia-murczowik.html)|2|[170614](/rpg/inwazja/opowiesci/konspekty/170614-kryzys-przez-eliksir.html), [170523](/rpg/inwazja/opowiesci/konspekty/170523-opetany-konstruminus.html)|
|[Smok](/rpg/inwazja/opowiesci/karty-postaci/9999-smok.html)|2|[140910](/rpg/inwazja/opowiesci/konspekty/140910-reporter-kontra-blakenbauerzy.html), [140819](/rpg/inwazja/opowiesci/konspekty/140819-marcelin-w-klasztorze.html)|
|[Sebastian Tecznia](/rpg/inwazja/opowiesci/karty-postaci/9999-sebastian-tecznia.html)|2|[141012](/rpg/inwazja/opowiesci/konspekty/141012-aplikanci-widzieli-gorathaula.html), [140928](/rpg/inwazja/opowiesci/konspekty/140928-policjant-widzial-aniola.html)|
|[Prosperjusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-prosperjusz-bankierz.html)|2|[170723](/rpg/inwazja/opowiesci/konspekty/170723-wywalczone-zycie-diany.html), [170702](/rpg/inwazja/opowiesci/konspekty/170702-milosc-przez-desperacje.html)|
|[Paweł Brokoty](/rpg/inwazja/opowiesci/karty-postaci/9999-pawel-brokoty.html)|2|[141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[Onufry Letniczek](/rpg/inwazja/opowiesci/karty-postaci/9999-onufry-letniczek.html)|2|[160131](/rpg/inwazja/opowiesci/konspekty/160131-dziwny-transmiter-weinerow.html), [160124](/rpg/inwazja/opowiesci/konspekty/160124-trzy-opetane-duszyczki.html)|
|[Oliwia Aurinus](/rpg/inwazja/opowiesci/karty-postaci/1709-oliwia-aurinus.html)|2|[171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html), [171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Olaf Rajczak](/rpg/inwazja/opowiesci/karty-postaci/9999-olaf-rajczak.html)|2|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html)|
|[Oktawia Aurinus](/rpg/inwazja/opowiesci/karty-postaci/1709-oktawia-aurinus.html)|2|[171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html), [171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Mirek Kujec](/rpg/inwazja/opowiesci/karty-postaci/9999-mirek-kujec.html)|2|[160117](/rpg/inwazja/opowiesci/konspekty/160117-muchy-w-sieci-korzunia.html), [151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html)|
|[Mikołaj Mykot](/rpg/inwazja/opowiesci/karty-postaci/9999-mikolaj-mykot.html)|2|[141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141019](/rpg/inwazja/opowiesci/konspekty/141019-lekarka-widziala-cybergothke.html)|
|[Michał Jesiotr](/rpg/inwazja/opowiesci/karty-postaci/9999-michal-jesiotr.html)|2|[161009](/rpg/inwazja/opowiesci/konspekty/161009-paulino-zmienilas-sie.html), [161002](/rpg/inwazja/opowiesci/konspekty/161002-wyciek-syberionu.html)|
|[Mariusz Czyczyż](/rpg/inwazja/opowiesci/karty-postaci/9999-mariusz-czyczyz.html)|2|[151003](/rpg/inwazja/opowiesci/konspekty/151003-zamtuz-przestaje-dzialac.html), [150928](/rpg/inwazja/opowiesci/konspekty/150928-zamtuz-z-jedna-wila.html)|
|[Marcelin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-marcelin-blakenbauer.html)|2|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [140819](/rpg/inwazja/opowiesci/konspekty/140819-marcelin-w-klasztorze.html)|
|[Maciej Orank](/rpg/inwazja/opowiesci/karty-postaci/9999-maciej-orank.html)|2|[160117](/rpg/inwazja/opowiesci/konspekty/160117-muchy-w-sieci-korzunia.html), [151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html)|
|[Lea Swoboda](/rpg/inwazja/opowiesci/karty-postaci/9999-lea-swoboda.html)|2|[151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html), [151003](/rpg/inwazja/opowiesci/konspekty/151003-zamtuz-przestaje-dzialac.html)|
|[Krzysztof Grumrzyk](/rpg/inwazja/opowiesci/karty-postaci/1709-krzysztof-grumrzyk.html)|2|[171024](/rpg/inwazja/opowiesci/konspekty/171024-detektyw-lecz-nie-sadecznego.html), [171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Kinga Melit](/rpg/inwazja/opowiesci/karty-postaci/9999-kinga-melit.html)|2|[140910](/rpg/inwazja/opowiesci/konspekty/140910-reporter-kontra-blakenbauerzy.html), [140819](/rpg/inwazja/opowiesci/konspekty/140819-marcelin-w-klasztorze.html)|
|[Karolina Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-karolina-maus.html)|2|[141012](/rpg/inwazja/opowiesci/konspekty/141012-aplikanci-widzieli-gorathaula.html), [140928](/rpg/inwazja/opowiesci/konspekty/140928-policjant-widzial-aniola.html)|
|[Karol Komnat](/rpg/inwazja/opowiesci/karty-postaci/9999-karol-komnat.html)|2|[170723](/rpg/inwazja/opowiesci/konspekty/170723-wywalczone-zycie-diany.html), [170702](/rpg/inwazja/opowiesci/konspekty/170702-milosc-przez-desperacje.html)|
|[Judyta Karnisz](/rpg/inwazja/opowiesci/karty-postaci/9999-judyta-karnisz.html)|2|[150830](/rpg/inwazja/opowiesci/konspekty/150830-kasia-nie-eis-w-powiewie.html), [150826](/rpg/inwazja/opowiesci/konspekty/150826-petla-dookola-pauliny.html)|
|[Joachim Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-maus.html)|2|[180214](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html), [180110](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html)|
|[Jerzy Pasznik](/rpg/inwazja/opowiesci/karty-postaci/9999-jerzy-pasznik.html)|2|[160918](/rpg/inwazja/opowiesci/konspekty/160918-walka-o-dusze-draceny.html), [160910](/rpg/inwazja/opowiesci/konspekty/160910-na-zywym-organizmie-draceny.html)|
|[Ireneusz Przaśnik](/rpg/inwazja/opowiesci/karty-postaci/9999-ireneusz-przasnik.html)|2|[170423](/rpg/inwazja/opowiesci/konspekty/170423-rozpad-magii-w-leere.html), [151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html)|
|[Irena Krysniok](/rpg/inwazja/opowiesci/karty-postaci/9999-irena-krysniok.html)|2|[141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[Ilona Maczatek](/rpg/inwazja/opowiesci/karty-postaci/9999-ilona-maczatek.html)|2|[170312](/rpg/inwazja/opowiesci/konspekty/170312-przebudzony-harvester.html), [160227](/rpg/inwazja/opowiesci/konspekty/160227-zakazany-harvester.html)|
|[Iliusitius](/rpg/inwazja/opowiesci/karty-postaci/9999-iliusitius.html)|2|[150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html), [150501](/rpg/inwazja/opowiesci/konspekty/150501-szalona-czarodziejka-zaleza.html)|
|[Halina Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-halina-weiner.html)|2|[161009](/rpg/inwazja/opowiesci/konspekty/161009-paulino-zmienilas-sie.html), [161002](/rpg/inwazja/opowiesci/konspekty/161002-wyciek-syberionu.html)|
|[Fryderyk Grzybb](/rpg/inwazja/opowiesci/karty-postaci/9999-fryderyk-grzybb.html)|2|[171015](/rpg/inwazja/opowiesci/konspekty/171015-powstrzymana-wojna-domowa.html), [170409](/rpg/inwazja/opowiesci/konspekty/170409-nie-zabijajmy-tych-magow.html)|
|[Filip Keramiusz](/rpg/inwazja/opowiesci/karty-postaci/9999-filip-keramiusz.html)|2|[171024](/rpg/inwazja/opowiesci/konspekty/171024-detektyw-lecz-nie-sadecznego.html), [171022](/rpg/inwazja/opowiesci/konspekty/171022-tylko-nieswiadomy-elemental.html)|
|[Estrella Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-estrella-diakon.html)|2|[150826](/rpg/inwazja/opowiesci/konspekty/150826-petla-dookola-pauliny.html), [150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|
|[Efraim Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-efraim-weiner.html)|2|[171031](/rpg/inwazja/opowiesci/konspekty/171031-utracona-kontrola.html), [170518](/rpg/inwazja/opowiesci/konspekty/170518-machinacje-maga-rolniczego.html)|
|[Bólokłąb](/rpg/inwazja/opowiesci/karty-postaci/9999-boloklab.html)|2|[160131](/rpg/inwazja/opowiesci/konspekty/160131-dziwny-transmiter-weinerow.html), [160124](/rpg/inwazja/opowiesci/konspekty/160124-trzy-opetane-duszyczki.html)|
|[Bolesław Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-boleslaw-maus.html)|2|[180214](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html), [171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html)|
|[Bartłomiej Czyrawiec](/rpg/inwazja/opowiesci/karty-postaci/9999-bartlomiej-czyrawiec.html)|2|[160904](/rpg/inwazja/opowiesci/konspekty/160904-rozpaczliwie-sterowany-wzor.html), [160901](/rpg/inwazja/opowiesci/konspekty/160901-uwieziony-w-komputerze.html)|
|[Barbara Zacieszek](/rpg/inwazja/opowiesci/karty-postaci/9999-barbara-zacieszek.html)|2|[160227](/rpg/inwazja/opowiesci/konspekty/160227-zakazany-harvester.html), [160131](/rpg/inwazja/opowiesci/konspekty/160131-dziwny-transmiter-weinerow.html)|
|[Archibald Składak](/rpg/inwazja/opowiesci/karty-postaci/9999-archibald-skladak.html)|2|[170326](/rpg/inwazja/opowiesci/konspekty/170326-cala-przeszlosc-splonela.html), [170323](/rpg/inwazja/opowiesci/konspekty/170323-oszczedzili-na-rurach.html)|
|[Aptoform Mirasilaler](/rpg/inwazja/opowiesci/karty-postaci/9999-aptoform-mirasilaler.html)|2|[150406](/rpg/inwazja/opowiesci/konspekty/150406-aurelia-za-aptoforma.html), [150325](/rpg/inwazja/opowiesci/konspekty/150325-morderstwo-jak-w-ksiazce.html)|
|[Apoloniusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-apoloniusz-bankierz.html)|2|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html), [170523](/rpg/inwazja/opowiesci/konspekty/170523-opetany-konstruminus.html)|
|[Agnieszka Mariacka](/rpg/inwazja/opowiesci/karty-postaci/9999-agnieszka-mariacka.html)|2|[160106](/rpg/inwazja/opowiesci/konspekty/160106-i-kult-zostaje-rozgromiony.html), [151230](/rpg/inwazja/opowiesci/konspekty/151230-zajcew-ze-smietnika-partnerem.html)|
|[Zenobia Morwiczka](/rpg/inwazja/opowiesci/karty-postaci/9999-zenobia-morwiczka.html)|1|[170423](/rpg/inwazja/opowiesci/konspekty/170423-rozpad-magii-w-leere.html)|
|[Włodzimierz Jamnik](/rpg/inwazja/opowiesci/karty-postaci/9999-wlodzimierz-jamnik.html)|1|[160918](/rpg/inwazja/opowiesci/konspekty/160918-walka-o-dusze-draceny.html)|
|[Wojmił Rzeźniczek](/rpg/inwazja/opowiesci/karty-postaci/9999-wojmil-rzezniczek.html)|1|[170423](/rpg/inwazja/opowiesci/konspekty/170423-rozpad-magii-w-leere.html)|
|[Wojmił Kopiec](/rpg/inwazja/opowiesci/karty-postaci/9999-wojmil-kopiec.html)|1|[141012](/rpg/inwazja/opowiesci/konspekty/141012-aplikanci-widzieli-gorathaula.html)|
|[Wiktor Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1709-wiktor-sowinski.html)|1|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html)|
|[Weronika Piniarz](/rpg/inwazja/opowiesci/karty-postaci/9999-weronika-piniarz.html)|1|[140819](/rpg/inwazja/opowiesci/konspekty/140819-marcelin-w-klasztorze.html)|
|[Tymotheus Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-tymotheus-blakenbauer.html)|1|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html)|
|[Tymoteusz Dzionek](/rpg/inwazja/opowiesci/karty-postaci/9999-tymoteusz-dzionek.html)|1|[150829](/rpg/inwazja/opowiesci/konspekty/150829-hybryda-w-bazie-skorpiona.html)|
|[Tomasz Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-tomasz-weiner.html)|1|[161002](/rpg/inwazja/opowiesci/konspekty/161002-wyciek-syberionu.html)|
|[Tomasz Kapelusz](/rpg/inwazja/opowiesci/karty-postaci/9999-tomasz-kapelusz.html)|1|[171024](/rpg/inwazja/opowiesci/konspekty/171024-detektyw-lecz-nie-sadecznego.html)|
|[Tadeusz Kuraszewicz](/rpg/inwazja/opowiesci/karty-postaci/9999-tadeusz-kuraszewicz.html)|1|[170323](/rpg/inwazja/opowiesci/konspekty/170323-oszczedzili-na-rurach.html)|
|[Tadeusz Baran](/rpg/inwazja/opowiesci/karty-postaci/1709-tadeusz-baran.html)|1|[161113](/rpg/inwazja/opowiesci/konspekty/161113-swieca-nie-zostawia-swoich.html)|
|[Szymon Łokciak](/rpg/inwazja/opowiesci/karty-postaci/9999-szymon-lokciak.html)|1|[171022](/rpg/inwazja/opowiesci/konspekty/171022-tylko-nieswiadomy-elemental.html)|
|[Szczepan Złodrak](/rpg/inwazja/opowiesci/karty-postaci/9999-szczepan-zlodrak.html)|1|[171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|
|[Szczepan Zaleski](/rpg/inwazja/opowiesci/karty-postaci/9999-szczepan-zaleski.html)|1|[150501](/rpg/inwazja/opowiesci/konspekty/150501-szalona-czarodziejka-zaleza.html)|
|[Szczepan Mirłik](/rpg/inwazja/opowiesci/karty-postaci/9999-szczepan-mirlik.html)|1|[170404](/rpg/inwazja/opowiesci/konspekty/170404-waz-jako-vicinius-pauliny.html)|
|[Stefania Kołek](/rpg/inwazja/opowiesci/karty-postaci/9999-stefania-kolek.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Serczedar Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-serczedar-bankierz.html)|1|[170409](/rpg/inwazja/opowiesci/konspekty/170409-nie-zabijajmy-tych-magow.html)|
|[Sandra Stryjek](/rpg/inwazja/opowiesci/karty-postaci/1709-sandra-stryjek.html)|1|[150501](/rpg/inwazja/opowiesci/konspekty/150501-szalona-czarodziejka-zaleza.html)|
|[Salazar Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-salazar-bankierz.html)|1|[141012](/rpg/inwazja/opowiesci/konspekty/141012-aplikanci-widzieli-gorathaula.html)|
|[Saith Flamecaller](/rpg/inwazja/opowiesci/karty-postaci/9999-saith-flamecaller.html)|1|[150716](/rpg/inwazja/opowiesci/konspekty/150716-chora-terminuska-i-zabolod.html)|
|[Rufus Eter](/rpg/inwazja/opowiesci/karty-postaci/9999-rufus-eter.html)|1|[150501](/rpg/inwazja/opowiesci/konspekty/150501-szalona-czarodziejka-zaleza.html)|
|[Rudolf Jankowski](/rpg/inwazja/opowiesci/karty-postaci/9999-rudolf-jankowski.html)|1|[161113](/rpg/inwazja/opowiesci/konspekty/161113-swieca-nie-zostawia-swoich.html)|
|[Roman Pilen](/rpg/inwazja/opowiesci/karty-postaci/9999-roman-pilen.html)|1|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html)|
|[Rebeka Czomnik](/rpg/inwazja/opowiesci/karty-postaci/9999-rebeka-czomnik.html)|1|[160227](/rpg/inwazja/opowiesci/konspekty/160227-zakazany-harvester.html)|
|[Rafał Maciejak](/rpg/inwazja/opowiesci/karty-postaci/9999-rafal-maciejak.html)|1|[150928](/rpg/inwazja/opowiesci/konspekty/150928-zamtuz-z-jedna-wila.html)|
|[Rafał Czapiek](/rpg/inwazja/opowiesci/karty-postaci/9999-rafal-czapiek.html)|1|[150501](/rpg/inwazja/opowiesci/konspekty/150501-szalona-czarodziejka-zaleza.html)|
|[Radosław Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/9999-radoslaw-myszeczka.html)|1|[171101](/rpg/inwazja/opowiesci/konspekty/171101-magimedy-przed-epidemia.html)|
|[Piotr Wadomiec](/rpg/inwazja/opowiesci/karty-postaci/9999-piotr-wadomiec.html)|1|[160901](/rpg/inwazja/opowiesci/konspekty/160901-uwieziony-w-komputerze.html)|
|[Piotr Pyszny](/rpg/inwazja/opowiesci/karty-postaci/9999-piotr-pyszny.html)|1|[170323](/rpg/inwazja/opowiesci/konspekty/170323-oszczedzili-na-rurach.html)|
|[Paweł Franna](/rpg/inwazja/opowiesci/karty-postaci/9999-pawel-franna.html)|1|[150501](/rpg/inwazja/opowiesci/konspekty/150501-szalona-czarodziejka-zaleza.html)|
|[Patryk Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-patryk-maus.html)|1|[170312](/rpg/inwazja/opowiesci/konspekty/170312-przebudzony-harvester.html)|
|[Patrycja Krowiowska](/rpg/inwazja/opowiesci/karty-postaci/1709-patrycja-krowiowska.html)|1|[150325](/rpg/inwazja/opowiesci/konspekty/150325-morderstwo-jak-w-ksiazce.html)|
|[Pamela Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-pamela-weiner.html)|1|[170312](/rpg/inwazja/opowiesci/konspekty/170312-przebudzony-harvester.html)|
|[Otton Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-otton-blakenbauer.html)|1|[140910](/rpg/inwazja/opowiesci/konspekty/140910-reporter-kontra-blakenbauerzy.html)|
|[Onufry Mirłik](/rpg/inwazja/opowiesci/karty-postaci/9999-onufry-mirlik.html)|1|[170404](/rpg/inwazja/opowiesci/konspekty/170404-waz-jako-vicinius-pauliny.html)|
|[Olga Jeden](/rpg/inwazja/opowiesci/karty-postaci/9999-olga-jeden.html)|1|[160124](/rpg/inwazja/opowiesci/konspekty/160124-trzy-opetane-duszyczki.html)|
|[Oksana Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-oksana-weiner.html)|1|[170312](/rpg/inwazja/opowiesci/konspekty/170312-przebudzony-harvester.html)|
|[Ochrona Kobra Przodek](/rpg/inwazja/opowiesci/karty-postaci/9999-ochrona-kobra-przodek.html)|1|[160106](/rpg/inwazja/opowiesci/konspekty/160106-i-kult-zostaje-rozgromiony.html)|
|[Najada Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-najada-diakon.html)|1|[160918](/rpg/inwazja/opowiesci/konspekty/160918-walka-o-dusze-draceny.html)|
|[Mojra](/rpg/inwazja/opowiesci/karty-postaci/9999-mojra.html)|1|[140910](/rpg/inwazja/opowiesci/konspekty/140910-reporter-kontra-blakenbauerzy.html)|
|[Milena Letniczek](/rpg/inwazja/opowiesci/karty-postaci/9999-milena-letniczek.html)|1|[160124](/rpg/inwazja/opowiesci/konspekty/160124-trzy-opetane-duszyczki.html)|
|[Mikołaj Pyżuk](/rpg/inwazja/opowiesci/karty-postaci/9999-mikolaj-pyzuk.html)|1|[170716](/rpg/inwazja/opowiesci/konspekty/170716-eteryczny-chlopiec-i-jego-pies.html)|
|[Mikaela Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-mikaela-weiner.html)|1|[170409](/rpg/inwazja/opowiesci/konspekty/170409-nie-zabijajmy-tych-magow.html)|
|[Mikado Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-mikado-diakon.html)|1|[150830](/rpg/inwazja/opowiesci/konspekty/150830-kasia-nie-eis-w-powiewie.html)|
|[Mieszko Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-mieszko-bankierz.html)|1|[161113](/rpg/inwazja/opowiesci/konspekty/161113-swieca-nie-zostawia-swoich.html)|
|[Michał Furczon](/rpg/inwazja/opowiesci/karty-postaci/9999-michal-furczon.html)|1|[170515](/rpg/inwazja/opowiesci/konspekty/170515-niewolnica-w-leasingu.html)|
|[Matylda Daczak](/rpg/inwazja/opowiesci/karty-postaci/9999-matylda-daczak.html)|1|[151003](/rpg/inwazja/opowiesci/konspekty/151003-zamtuz-przestaje-dzialac.html)|
|[Mateusz Tykwa](/rpg/inwazja/opowiesci/karty-postaci/9999-mateusz-tykwa.html)|1|[150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|
|[Mateusz Kuraszewicz](/rpg/inwazja/opowiesci/karty-postaci/9999-mateusz-kuraszewicz.html)|1|[160124](/rpg/inwazja/opowiesci/konspekty/160124-trzy-opetane-duszyczki.html)|
|[Marta Newa](/rpg/inwazja/opowiesci/karty-postaci/9999-marta-newa.html)|1|[140910](/rpg/inwazja/opowiesci/konspekty/140910-reporter-kontra-blakenbauerzy.html)|
|[Marianna Zurka](/rpg/inwazja/opowiesci/karty-postaci/9999-marianna-zurka.html)|1|[150501](/rpg/inwazja/opowiesci/konspekty/150501-szalona-czarodziejka-zaleza.html)|
|[Marian Kozior](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-kozior.html)|1|[150829](/rpg/inwazja/opowiesci/konspekty/150829-hybryda-w-bazie-skorpiona.html)|
|[Maria Pilen](/rpg/inwazja/opowiesci/karty-postaci/9999-maria-pilen.html)|1|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html)|
|[Marek Śmietanka](/rpg/inwazja/opowiesci/karty-postaci/1709-marek-smietanka.html)|1|[160901](/rpg/inwazja/opowiesci/konspekty/160901-uwieziony-w-komputerze.html)|
|[Marcin Puczek](/rpg/inwazja/opowiesci/karty-postaci/9999-marcin-puczek.html)|1|[160918](/rpg/inwazja/opowiesci/konspekty/160918-walka-o-dusze-draceny.html)|
|[Maksymilian Łoś](/rpg/inwazja/opowiesci/karty-postaci/9999-maksymilian-los.html)|1|[150406](/rpg/inwazja/opowiesci/konspekty/150406-aurelia-za-aptoforma.html)|
|[Maja Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-maja-weiner.html)|1|[171024](/rpg/inwazja/opowiesci/konspekty/171024-detektyw-lecz-nie-sadecznego.html)|
|[Maja Stomaniek](/rpg/inwazja/opowiesci/karty-postaci/9999-maja-stomaniek.html)|1|[160227](/rpg/inwazja/opowiesci/konspekty/160227-zakazany-harvester.html)|
|[Maciek Jeden](/rpg/inwazja/opowiesci/karty-postaci/9999-maciek-jeden.html)|1|[160124](/rpg/inwazja/opowiesci/konspekty/160124-trzy-opetane-duszyczki.html)|
|[Maciej Tykwa](/rpg/inwazja/opowiesci/karty-postaci/9999-maciej-tykwa.html)|1|[150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|
|[Maciej Brzydal](/rpg/inwazja/opowiesci/karty-postaci/9999-maciej-brzydal.html)|1|[170323](/rpg/inwazja/opowiesci/konspekty/170323-oszczedzili-na-rurach.html)|
|[Lilia Kałdun](/rpg/inwazja/opowiesci/karty-postaci/9999-lilia-kaldun.html)|1|[160901](/rpg/inwazja/opowiesci/konspekty/160901-uwieziony-w-komputerze.html)|
|[Lidia Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-lidia-weiner.html)|1|[170212](/rpg/inwazja/opowiesci/konspekty/170212-nieufnosc-w-malym-miescie.html)|
|[Leopold Brzeszczot](/rpg/inwazja/opowiesci/karty-postaci/9999-leopold-brzeszczot.html)|1|[160117](/rpg/inwazja/opowiesci/konspekty/160117-muchy-w-sieci-korzunia.html)|
|[Leonidas Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-leonidas-blakenbauer.html)|1|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html)|
|[Laura Filut](/rpg/inwazja/opowiesci/karty-postaci/9999-laura-filut.html)|1|[140910](/rpg/inwazja/opowiesci/konspekty/140910-reporter-kontra-blakenbauerzy.html)|
|[Laetitia Gaia Rasputin Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-laetitia-gaia-rasputin-weiner.html)|1|[170212](/rpg/inwazja/opowiesci/konspekty/170212-nieufnosc-w-malym-miescie.html)|
|[Krystian Linowęc](/rpg/inwazja/opowiesci/karty-postaci/9999-krystian-linowec.html)|1|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html)|
|[Konrad Paśnikowiec](/rpg/inwazja/opowiesci/karty-postaci/9999-konrad-pasnikowiec.html)|1|[170510](/rpg/inwazja/opowiesci/konspekty/170510-najgorsze-love-story.html)|
|[Kociebor Dyrygent](/rpg/inwazja/opowiesci/karty-postaci/1709-kociebor-dyrygent.html)|1|[171024](/rpg/inwazja/opowiesci/konspekty/171024-detektyw-lecz-nie-sadecznego.html)|
|[Kleofas Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/9999-kleofas-myszeczka.html)|1|[170409](/rpg/inwazja/opowiesci/konspekty/170409-nie-zabijajmy-tych-magow.html)|
|[Kleofas Bór](/rpg/inwazja/opowiesci/karty-postaci/1709-kleofas-bor.html)|1|[150325](/rpg/inwazja/opowiesci/konspekty/150325-morderstwo-jak-w-ksiazce.html)|
|[Klemens X](/rpg/inwazja/opowiesci/karty-postaci/9999-klemens-x.html)|1|[140910](/rpg/inwazja/opowiesci/konspekty/140910-reporter-kontra-blakenbauerzy.html)|
|[Klara Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-klara-blakenbauer.html)|1|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html)|
|[Kinga Kujec](/rpg/inwazja/opowiesci/karty-postaci/9999-kinga-kujec.html)|1|[160117](/rpg/inwazja/opowiesci/konspekty/160117-muchy-w-sieci-korzunia.html)|
|[Katia Grajek](/rpg/inwazja/opowiesci/karty-postaci/1709-katia-grajek.html)|1|[171001](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html)|
|[Katarzyna Mirłik](/rpg/inwazja/opowiesci/karty-postaci/9999-katarzyna-mirlik.html)|1|[170404](/rpg/inwazja/opowiesci/konspekty/170404-waz-jako-vicinius-pauliny.html)|
|[Katarzyna Klicz](/rpg/inwazja/opowiesci/karty-postaci/9999-katarzyna-klicz.html)|1|[170423](/rpg/inwazja/opowiesci/konspekty/170423-rozpad-magii-w-leere.html)|
|[Karolina Błazoń](/rpg/inwazja/opowiesci/karty-postaci/9999-karolina-blazon.html)|1|[150501](/rpg/inwazja/opowiesci/konspekty/150501-szalona-czarodziejka-zaleza.html)|
|[Karol Wąski](/rpg/inwazja/opowiesci/karty-postaci/9999-karol-waski.html)|1|[170716](/rpg/inwazja/opowiesci/konspekty/170716-eteryczny-chlopiec-i-jego-pies.html)|
|[Karol Wadomiec](/rpg/inwazja/opowiesci/karty-postaci/9999-karol-wadomiec.html)|1|[160901](/rpg/inwazja/opowiesci/konspekty/160901-uwieziony-w-komputerze.html)|
|[Kamil Gurnat](/rpg/inwazja/opowiesci/karty-postaci/9999-kamil-gurnat.html)|1|[150501](/rpg/inwazja/opowiesci/konspekty/150501-szalona-czarodziejka-zaleza.html)|
|[Kaja Odyniec](/rpg/inwazja/opowiesci/karty-postaci/9999-kaja-odyniec.html)|1|[170423](/rpg/inwazja/opowiesci/konspekty/170423-rozpad-magii-w-leere.html)|
|[Juliusz Kwarc](/rpg/inwazja/opowiesci/karty-postaci/9999-juliusz-kwarc.html)|1|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html)|
|[Julian Pszczelak](/rpg/inwazja/opowiesci/karty-postaci/1709-julian-pszczelak.html)|1|[161113](/rpg/inwazja/opowiesci/konspekty/161113-swieca-nie-zostawia-swoich.html)|
|[Julia Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-julia-weiner.html)|1|[150830](/rpg/inwazja/opowiesci/konspekty/150830-kasia-nie-eis-w-powiewie.html)|
|[Jolanta Sowińska](/rpg/inwazja/opowiesci/karty-postaci/1709-jolanta-sowinska.html)|1|[150826](/rpg/inwazja/opowiesci/konspekty/150826-petla-dookola-pauliny.html)|
|[Joachim Kopiec](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-kopiec.html)|1|[141012](/rpg/inwazja/opowiesci/konspekty/141012-aplikanci-widzieli-gorathaula.html)|
|[Jaromir Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/9999-jaromir-myszeczka.html)|1|[170409](/rpg/inwazja/opowiesci/konspekty/170409-nie-zabijajmy-tych-magow.html)|
|[Janusz Wosiciel](/rpg/inwazja/opowiesci/karty-postaci/9999-janusz-wosiciel.html)|1|[170614](/rpg/inwazja/opowiesci/konspekty/170614-kryzys-przez-eliksir.html)|
|[Janusz Krzykoł](/rpg/inwazja/opowiesci/karty-postaci/9999-janusz-krzykol.html)|1|[170212](/rpg/inwazja/opowiesci/konspekty/170212-nieufnosc-w-malym-miescie.html)|
|[Janusz Kocieł](/rpg/inwazja/opowiesci/karty-postaci/9999-janusz-kociel.html)|1|[170326](/rpg/inwazja/opowiesci/konspekty/170326-cala-przeszlosc-splonela.html)|
|[Janusz Karzeł](/rpg/inwazja/opowiesci/karty-postaci/9999-janusz-karzel.html)|1|[151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html)|
|[Janina Muczarok](/rpg/inwazja/opowiesci/karty-postaci/9999-janina-muczarok.html)|1|[171218](/rpg/inwazja/opowiesci/konspekty/171218-senesgradzka-kopia-pauliny.html)|
|[Jan Karczoch](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-karczoch.html)|1|[170409](/rpg/inwazja/opowiesci/konspekty/170409-nie-zabijajmy-tych-magow.html)|
|[Jakub Pyżuk](/rpg/inwazja/opowiesci/karty-postaci/9999-jakub-pyzuk.html)|1|[170716](/rpg/inwazja/opowiesci/konspekty/170716-eteryczny-chlopiec-i-jego-pies.html)|
|[Jagoda Musiąg](/rpg/inwazja/opowiesci/karty-postaci/9999-jagoda-musiag.html)|1|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html)|
|[Izabela Kamil](/rpg/inwazja/opowiesci/karty-postaci/9999-izabela-kamil.html)|1|[141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html)|
|[Integra Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-integra-weiner.html)|1|[171220](/rpg/inwazja/opowiesci/konspekty/171220-potrojna-magitrownia-histogram.html)|
|[Inga Błazoń](/rpg/inwazja/opowiesci/karty-postaci/9999-inga-blazon.html)|1|[150501](/rpg/inwazja/opowiesci/konspekty/150501-szalona-czarodziejka-zaleza.html)|
|[Hubert Rębski](/rpg/inwazja/opowiesci/karty-postaci/9999-hubert-rebski.html)|1|[160106](/rpg/inwazja/opowiesci/konspekty/160106-i-kult-zostaje-rozgromiony.html)|
|[Herbert Zioło](/rpg/inwazja/opowiesci/karty-postaci/1709-herbert-ziolo.html)|1|[170325](/rpg/inwazja/opowiesci/konspekty/170325-klatwozyt-z-lustra.html)|
|[Halina Krzyżanowska](/rpg/inwazja/opowiesci/karty-postaci/9999-halina-krzyzanowska.html)|1|[161113](/rpg/inwazja/opowiesci/konspekty/161113-swieca-nie-zostawia-swoich.html)|
|[Gustaw Bareczny](/rpg/inwazja/opowiesci/karty-postaci/9999-gustaw-bareczny.html)|1|[170716](/rpg/inwazja/opowiesci/konspekty/170716-eteryczny-chlopiec-i-jego-pies.html)|
|[Grzegorz Śliwa](/rpg/inwazja/opowiesci/karty-postaci/9999-grzegorz-sliwa.html)|1|[150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|
|[Grzegorz Nadziejak](/rpg/inwazja/opowiesci/karty-postaci/9999-grzegorz-nadziejak.html)|1|[171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Grzegorz Murzecki](/rpg/inwazja/opowiesci/karty-postaci/9999-grzegorz-murzecki.html)|1|[141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html)|
|[Grażyna Kępka](/rpg/inwazja/opowiesci/karty-postaci/9999-grazyna-kepka.html)|1|[170326](/rpg/inwazja/opowiesci/konspekty/170326-cala-przeszlosc-splonela.html)|
|[Franciszek Błazoń](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-blazon.html)|1|[150501](/rpg/inwazja/opowiesci/konspekty/150501-szalona-czarodziejka-zaleza.html)|
|[Filip Wichoszczyk](/rpg/inwazja/opowiesci/karty-postaci/9999-filip-wichoszczyk.html)|1|[170423](/rpg/inwazja/opowiesci/konspekty/170423-rozpad-magii-w-leere.html)|
|[Filip Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-filip-weiner.html)|1|[170312](/rpg/inwazja/opowiesci/konspekty/170312-przebudzony-harvester.html)|
|[Filip Czątko](/rpg/inwazja/opowiesci/karty-postaci/9999-filip-czatko.html)|1|[150928](/rpg/inwazja/opowiesci/konspekty/150928-zamtuz-z-jedna-wila.html)|
|[Filip Brzeszczot](/rpg/inwazja/opowiesci/karty-postaci/9999-filip-brzeszczot.html)|1|[160117](/rpg/inwazja/opowiesci/konspekty/160117-muchy-w-sieci-korzunia.html)|
|[Ferrus Mucro](/rpg/inwazja/opowiesci/karty-postaci/9999-ferrus-mucro.html)|1|[170404](/rpg/inwazja/opowiesci/konspekty/170404-waz-jako-vicinius-pauliny.html)|
|[Feliks Szczęśliwiec](/rpg/inwazja/opowiesci/karty-postaci/9999-feliks-szczesliwiec.html)|1|[160227](/rpg/inwazja/opowiesci/konspekty/160227-zakazany-harvester.html)|
|[Felicjan Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-felicjan-weiner.html)|1|[161113](/rpg/inwazja/opowiesci/konspekty/161113-swieca-nie-zostawia-swoich.html)|
|[Felicja Szampierz](/rpg/inwazja/opowiesci/karty-postaci/9999-felicja-szampierz.html)|1|[170523](/rpg/inwazja/opowiesci/konspekty/170523-opetany-konstruminus.html)|
|[Eustachy Szipinik](/rpg/inwazja/opowiesci/karty-postaci/9999-eustachy-szipinik.html)|1|[160227](/rpg/inwazja/opowiesci/konspekty/160227-zakazany-harvester.html)|
|[Ernest Brzeszczot](/rpg/inwazja/opowiesci/karty-postaci/9999-ernest-brzeszczot.html)|1|[160117](/rpg/inwazja/opowiesci/konspekty/160117-muchy-w-sieci-korzunia.html)|
|[Elwira Czlikan](/rpg/inwazja/opowiesci/karty-postaci/9999-elwira-czlikan.html)|1|[171101](/rpg/inwazja/opowiesci/konspekty/171101-magimedy-przed-epidemia.html)|
|[Elgieskład](/rpg/inwazja/opowiesci/karty-postaci/9999-elgiesklad.html)|1|[170323](/rpg/inwazja/opowiesci/konspekty/170323-oszczedzili-na-rurach.html)|
|[Elea Maus](/rpg/inwazja/opowiesci/karty-postaci/1802-elea-maus.html)|1|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html)|
|[Edward Ramuel](/rpg/inwazja/opowiesci/karty-postaci/9999-edward-ramuel.html)|1|[170423](/rpg/inwazja/opowiesci/konspekty/170423-rozpad-magii-w-leere.html)|
|[Edmund Bakłażan](/rpg/inwazja/opowiesci/karty-postaci/9999-edmund-baklazan.html)|1|[140928](/rpg/inwazja/opowiesci/konspekty/140928-policjant-widzial-aniola.html)|
|[Dominik Szerściuk](/rpg/inwazja/opowiesci/karty-postaci/9999-dominik-szersciuk.html)|1|[150829](/rpg/inwazja/opowiesci/konspekty/150829-hybryda-w-bazie-skorpiona.html)|
|[Dariusz Remont](/rpg/inwazja/opowiesci/karty-postaci/9999-dariusz-remont.html)|1|[150501](/rpg/inwazja/opowiesci/konspekty/150501-szalona-czarodziejka-zaleza.html)|
|[Daniel Stryczek](/rpg/inwazja/opowiesci/karty-postaci/9999-daniel-stryczek.html)|1|[160227](/rpg/inwazja/opowiesci/konspekty/160227-zakazany-harvester.html)|
|[Damazy Czekan](/rpg/inwazja/opowiesci/karty-postaci/9999-damazy-czekan.html)|1|[150829](/rpg/inwazja/opowiesci/konspekty/150829-hybryda-w-bazie-skorpiona.html)|
|[Cyprian Koziej](/rpg/inwazja/opowiesci/karty-postaci/9999-cyprian-koziej.html)|1|[150325](/rpg/inwazja/opowiesci/konspekty/150325-morderstwo-jak-w-ksiazce.html)|
|[Celestyna Marduk](/rpg/inwazja/opowiesci/karty-postaci/9999-celestyna-marduk.html)|1|[150830](/rpg/inwazja/opowiesci/konspekty/150830-kasia-nie-eis-w-powiewie.html)|
|[Borys Kumin](/rpg/inwazja/opowiesci/karty-postaci/1709-borys-kumin.html)|1|[140910](/rpg/inwazja/opowiesci/konspekty/140910-reporter-kontra-blakenbauerzy.html)|
|[Bolesław Złodrak](/rpg/inwazja/opowiesci/karty-postaci/9999-boleslaw-zlodrak.html)|1|[171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|
|[Beata Obaczna](/rpg/inwazja/opowiesci/karty-postaci/9999-beata-obaczna.html)|1|[170716](/rpg/inwazja/opowiesci/konspekty/170716-eteryczny-chlopiec-i-jego-pies.html)|
|[Baltazar Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-baltazar-maus.html)|1|[150826](/rpg/inwazja/opowiesci/konspekty/150826-petla-dookola-pauliny.html)|
|[August Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-august-bankierz.html)|1|[150501](/rpg/inwazja/opowiesci/konspekty/150501-szalona-czarodziejka-zaleza.html)|
|[Artur Bryś](/rpg/inwazja/opowiesci/karty-postaci/1709-artur-brys.html)|1|[150325](/rpg/inwazja/opowiesci/konspekty/150325-morderstwo-jak-w-ksiazce.html)|
|[Anton Jesiotr](/rpg/inwazja/opowiesci/karty-postaci/9999-anton-jesiotr.html)|1|[161002](/rpg/inwazja/opowiesci/konspekty/161002-wyciek-syberionu.html)|
|[Annaliza Delfin](/rpg/inwazja/opowiesci/karty-postaci/9999-annaliza-delfin.html)|1|[150406](/rpg/inwazja/opowiesci/konspekty/150406-aurelia-za-aptoforma.html)|
|[Anna Kozak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kozak.html)|1|[141012](/rpg/inwazja/opowiesci/konspekty/141012-aplikanci-widzieli-gorathaula.html)|
|[Anita Rodos](/rpg/inwazja/opowiesci/karty-postaci/9999-anita-rodos.html)|1|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html)|
|[Ania i Basia Pilen](/rpg/inwazja/opowiesci/karty-postaci/9999-ania-i-basia-pilen.html)|1|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html)|
|[Aneta Rainer](/rpg/inwazja/opowiesci/karty-postaci/1709-aneta-rainer.html)|1|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html)|
|[Andrzej Toporek](/rpg/inwazja/opowiesci/karty-postaci/9999-andrzej-toporek.html)|1|[171001](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html)|
|[Andrzej Farnolis](/rpg/inwazja/opowiesci/karty-postaci/9999-andrzej-farnolis.html)|1|[170523](/rpg/inwazja/opowiesci/konspekty/170523-opetany-konstruminus.html)|
|[Andrea Wilgacz](/rpg/inwazja/opowiesci/karty-postaci/1709-andrea-wilgacz.html)|1|[161113](/rpg/inwazja/opowiesci/konspekty/161113-swieca-nie-zostawia-swoich.html)|
|[Amanda Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-amanda-diakon.html)|1|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html)|
|[Alojzy Wołek](/rpg/inwazja/opowiesci/karty-postaci/9999-alojzy-wolek.html)|1|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html)|
|[Aleksander Szwiok](/rpg/inwazja/opowiesci/karty-postaci/9999-aleksander-szwiok.html)|1|[150829](/rpg/inwazja/opowiesci/konspekty/150829-hybryda-w-bazie-skorpiona.html)|
|[Aleksander Dziurząb](/rpg/inwazja/opowiesci/karty-postaci/9999-aleksander-dziurzab.html)|1|[160901](/rpg/inwazja/opowiesci/konspekty/160901-uwieziony-w-komputerze.html)|
|[Aleksander Czykomar](/rpg/inwazja/opowiesci/karty-postaci/9999-aleksander-czykomar.html)|1|[170518](/rpg/inwazja/opowiesci/konspekty/170518-machinacje-maga-rolniczego.html)|
|[Adrian Kropiak](/rpg/inwazja/opowiesci/karty-postaci/9999-adrian-kropiak.html)|1|[150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|
|[Adam Kapelusz](/rpg/inwazja/opowiesci/karty-postaci/9999-adam-kapelusz.html)|1|[171024](/rpg/inwazja/opowiesci/konspekty/171024-detektyw-lecz-nie-sadecznego.html)|

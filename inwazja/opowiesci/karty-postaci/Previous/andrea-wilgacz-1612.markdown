---
layout: inwazja-karta-postaci
title:  "Andrea Wilgacz"
categories: srebrna_swieca, profile, postac_gracza
---
# {{page.title}}
## Impulsy (co motywuje / napędza postać):
### Strategiczne

- **terminus z powołania**: moim zadaniem jest chronić (czuje się w obowiązku pomagać magom i ludziom)
- **lojalna wobec Świecy**: gildia to moi przyjaciele i jedyna rodzina, jaka mi została(dobro gildii jest ważniejsze niż pojedynczego maga)

### Taktyczne

- **dociekliwa**: tajemnice są zbyt niebezpieczne, żebym nie wiedziała (musi poznać prawdę)
- **mentalność szczura**: przeżyć za wszelką cenę (martwa nikomu nie pomogę)
- **własny kompas moralny**: przepisy się zmieniają, ale pewne rzeczy są uniwersalnie dobre lub złe (prawa należy przestrzegać, ale można je trochę nagiąć)
- **pragmatyczna**: czasem trzeba zaszkodzić, żeby pomóc 

## Co umie (co postać potrafi):
### Ekspercko (poziom 3):

- dedukcja prawdy (detektyw/śledczy)
- magiczny analityk (śledczy/katalistka)
- oficer taktycznty (wydział wewnętrzny)

### Zawodowo (poziom 2):

- mediator wydziału wewnętrznego
- agent wydziału wewnętrznego
- detektyw
- terminus drugoliniowy
- śledczy
- broker informacji
- fałszowanie dowodów
- katalistka

### Amatorsko (poziom 1):

- archiwistka
- wróżka
- dywersja
- pojedzie wszystkim
- pułapki

## Inklinacje:

|  Social Aggressive |  Social Devious |  Social Friendly |  Knowledge |  Craft |  Supernatural |  Fortitude |  Nimbleness |
|:==================:|:===============:|:================:|:==========:|:======:|:=============:|:==========:|:===========:|
|       +1           |         +1      |        -1        |      +1    |   -1   |       -1      |      0     |      0      |


## Kieszonkowe
fire, water

## Otoczenie powiązane

### Powiązane frakcje
wydział wewnętrzny Świecy

### Kogo zna

#### Doskonale: 3

- szefa lokalnej mafii (traktuje ją jak przybraną córkę)
- ludzi półświatka (magicznego i nie)

#### Nieźle: 2

- burdelmamę burdelu z kralothami
- terminusów (takich, co się jej boją i takich, co są jej wdzięczni)
- Ernest Maus
- Kontakty w rodzinie Weiner

#### Luźno: 1
 
- okolicznych detektywów (głównie ludzi, kilku magów) 

### Co ma do dyspozycji:

#### Szczególnie dobre: 3

- wyposażenie wydziału wewnętrznego

#### Ponadprzeciętne: 2

- składy wtdziału wewnętrznego

#### Niezłe: 1

- kiepskie_obiekty

## Magia:

### Magia dynamiczna
- magia detekcyjna
- analiza magiczna
- magia detektywistyczna
- podstawowa magia mentalna

### Zaklęcia hermetyczne

## Specjalne:


## Inne:

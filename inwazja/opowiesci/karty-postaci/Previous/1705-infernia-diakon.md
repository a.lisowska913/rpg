---
layout: inwazja-karta-postaci
categories: profile
factions: "KADEM"
type: "NPC"
title: "Infernia Diakon"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **skandalistka**: Okazja do tego, by ktoś był zbulwersowany? Możliwość zrobienia czegoś... powszechnie uznawanego za niewłaściwe? SUPER!
* **rywalizacja**: Infernia nie odpuści sobie możliwości rywalizowania z kimś czy wejścia w konflikt - mniej istotne czy wygrać może, czy też nie.
* **Diakon, nie Zajcew**: Infernia wybrała sobie ród Diakon i konspekwentnie robi wszystko, by nikt nie pomyślał, że jest Zajcewką. Mimo, że trochę jest.
* **w poszukiwaniu przyjemności**: Infernia konsekwentnie szuka wszystkiego, co przyniesie jej radość, przyjemność i dobrą zabawę.
* **kolekcjonerka ciuchów**: kocha ciekawe stroje, rzadkie ubrania i inne tego typu rzeczy. Zwłaszcza jako trofea. Ma... nieco dziwną kolekcję NIEKONIECZNIE swoich ubrań.

### Zachowania (jaka jest)

* **bardzo impulsywna**: gdzie spotkała się krew Zajcewów i Diakonów, tam powstała impulsywność Inferni... straszne połączenie.
* **pamiętliwa**: nie zapomina uraz i zawsze pamięta, by się zemścić. Jednocześnie, nie zapomina też kto i kiedy jej pomógł.
* **bezpośrednia**: Infernia nie jest zbyt podstępna - działa zdecydowanie bezpośrednio, mówi w twarz kogo lubi i z kim ma kosę. Nadaje się na sierżanta ;-).
* **strasznie uparta**: jak już Infernia ustali, że ma zamiar coś zrobić... zmień jej zdanie
* **ciekawska**: Infernia zawsze chce sprawdzić coś nowego; to ta osoba, co wsadzi palec między drzwi i ma nadzieję, że zadziała

## Co umie (co postać potrafi):

### Specjalizacje

* **magia destruktywna**: zaklęcia Inferni najlepiej działają, gdy mogą się same wzmacniać "poświęcając" coś innego. Np. jej puryfikacja wyniszcza cel.
* **zapachy, feromony**: manipulacja zapachem, przygotowywanie perfum, delikatne subtelności... ta finezja jest czymś, czego się po Inferni nie spodziewają.
* **ars amandi**: Infernia, skandalistką będąc jak to Infernia - no i DIAKON NIE ZAJCEW - bardzo wytrwale ćwiczyła i działała w ars amandi. Daje też korepetycje.
* **rośliny niebezpieczne**: agresywne, drapieżne, trujące (i chcące zjadać) - Infernia idzie wytrwale w ślady Weroniki Seton. Ku rozpaczy Ignata.
* **puryfikacja**: puryfikatorka nie będąca katalistką może brzmi dziwnie, jednak Infernia potrafi używać magii niszczycielskiej do puryfikacji.
* **alchemia**: specjalizuje się w perfumach, ale jest w stanie składać różnego rodzaju eliksiry, mutageny, rośliny we flaszkach...
* **golemy / animacja**: Infernia lubi bezpośrednią kontrolę; potrafi animować rośliny i sterować nimi zgodnie ze swoją wolą
* magia erotyczna
* alchemia

### Umiejętności

* **kucharka**: Infernia lubi gotować, robi to też całkiem nieźle. Lubi ugościć zrobionym przez siebie jedzeniem.
* **ogrodniczka KADEMu**: Infernia jest jedną z czarodziejek specjalizujących się w zajmowaniu sie ogrodów Weroniki Seton... poza tym, też innymi ;-).
* **zastraszanie**: Infernia nie jest może zbyt subtelna, ale przerażanie przeciwników i dominacja ich używając swojej silnej woli... oj tak.
* **KADEMowa samoobrona**: Infernia może i nie jest terminuską, ale jak już walnie czy kopnie czy zaklęciem strzeli... dostosowanie do Zajcewowego czy KADEMowego życia
* **juggernaut**: Infernia idzie TAM. Idzie jak czołg - nie zatrzymasz, nie przekonasz, nie skłonisz bólem ni przekupstwem. Ona ma plan i go wykona.

### Cechy:

|  Social Aggressive |  Social Devious |  Social Friendly |  Knowledge |  Craft |  Supernatural |  Fortitude |  Nimbleness |
|:==================:|:===============:|:================:|:==========:|:======:|:=============:|:==========:|:===========:|
|       +0           |        -1       |        -1        |      0     |   +1   |      +1       |     +1     |     -1      |


### Specjalne

**brak**

## Magia

### Szkoły magiczne
* **magia elementalna**
* **biomancja**: czarodziejka roślin, ciała i ekstazy. A jak coś nie wyjdzie - puryfikacja i próbujemy jeszcze raz...
* **kataliza**
* **magia zmysłów**

### Zaklęcia statyczne

* nazwa : link
* nazwa : link

## Zasoby i otoczenie

### Powiązane frakcje

* KADEM

### Kogo zna:

* **znana skandalistka**: oj tak... powszechnie znana jest jako KADEMka, która zrujnuje Twoją reputację... bo swojej już nie ma
* **szacun na dzielni**: Infernia na KADEMie jest szanowana jak eks-AD, poza KADEMem jest uznawana za groźną wojowniczkę z którą się nie zadziera
* **dziwne hedonistyczne kontakty**: tu nie do końca chodzi o półświatek; Infernia raczej ma... szemrane kontakty z dziwnymi i eksperymentalnymi dealerami przyjemności
* **Karolina Kupiec**: ze wszystkich magów KADEMu Karolina i Infernia mają straszny sojusz - Infernia dostarcza składniki i testuje eksperymenty Karoliny...

### Co ma do dyspozycji:

* **bardzo niebezpieczne rośliny**: część z ogrodów Weroniki Seton, część z innych źródeł, część pozyskanych w różny sposób...
* **liczne eksperymenty alchemiczne**: mniej lub bardziej udane, mniej lub bardziej niebezpieczne, zwykle bardzo różowe...
* **militarny zestaw erotyczny**: mnóstwo dziwnych narzędzi i sprzętu i eliksirów i... wszystko bardzo skuteczne do zachęty i wsparcia
* **stroje, mundury...**: niekoniecznie swoje; Infernia ma ogromną kolekcję różnych fajnych i ciekawych ubrań z różnymi historiami

### Surowce

* **Wartość**: 2
* **Pochodzenie**: Mnóstwo dziwnych sprzedawalnych eksperymentów, korepetycje z różnych dziedzin oraz groźne / dziwne roboty dla przerażonych magów

# Opis

Silna, dobrze zbudowana i lekko nadpalona blondynka o jaskrawozielonych włosach, uczesana na jeża. 33 lata. Troszkę wygląda jak Hefajstos w spódnicy; zdecydowanie nie przystaje do obrazka "typowej ślicznej laleczki", acz jest ładna w sposób Diakonów. To Infernia Diakon - impulsywny czołg, zajmująca się w ramach hobby ogrodami Weroniki Seton...
## Motto

"Najlepsza jest udana przyjemność po świetnym konflikcie. A potem zrobię obiad."

# Historia:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|170125|wypowiedziała Hektorowi jawnie wojnę. Wściekła jak osa. Skrzywdziła Ignata, bo to jej walka z Hektorem.|[Przeprawa do Świecy Daemonica](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html)|10/08/10|10/08/11|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170104|pełniąca szlachetną funkcję kucharki, nadal nie lubi Hektora i złośliwie ironizuje. Jednak przezwyciężyła niechęć... choć zastawiła na Hektora pułapkę czy dwie (rozbrojone przez Silurię)|[Spalone generatory pryzmatyczne](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html)|10/08/08|10/08/09|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150625|która jest bardzo niezadowolona z działań Hektora; nie tylko ściągnęła Jolę (dziennikarkę) ale i otwarcie zagroziła Hektorowi.|[Poligon kluczem do Marcelina?](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html)|10/05/09|10/05/10|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150607|krew Zajcewa i Diakona, którą rzucił Marcelin za co wciągnęła go w perfidną pułapkę. Marzy o eleganckim mundurze.|[Brat przeciw bratu](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html)|10/05/07|10/05/08|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150411|"coś między Diakonami a Zajcewami", eks-kochanka Marcelina która go rzuciła... na krzesło. I gracze jej unikają jak ognia.|[Dzień z życia Klemensa](/rpg/inwazja/opowiesci/konspekty/150411-dzien-z-zycia-klemensa.html)|10/05/01|10/05/02|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170417|ratowała w Żółtym Ogrodzie Weroniki Seton syrenopnącze, przez co miała najlepsze chwile swojego życia... do odwyku.|[Ratując syrenopnącze](/rpg/inwazja/opowiesci/konspekty/170417-ratujac-syrenopnacze.html)|10/03/05|10/03/12|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170217|zatrzymała Ignata przed śledzeniem Silurii; groźniejsza w walce bezpośredniej od Ignata, acz nie bardzo bojowa.|[Skradziona pozytywka Mausów](/rpg/inwazja/opowiesci/konspekty/170217-skradziona-pozytywka-mausow.html)|10/02/28|10/03/02|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170118|która ukryła działania Karoliny Kupiec przed Blakenbauerami i została frontgirl. Upiła się w Rezydencji, zmusiła Hektora do pocałunku i skończyła z Marcelinem. Achievement ;-).|[Ludzka prokuratura a magowie](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|10/02/22|10/02/25|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170111|zainteresowana KTO porwał Silurię, Siluriowa informatorka w sprawie Ignata i pomoc Karoliny by przebadać narkotyk Uli.|[EIS na kozetce](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|10/02/18|10/02/21|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161222|która nadal wraz z Silurią próbują sprawdzić która będzie 'u góry'.|[Kto wpisał Błażeja do konkursu](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|10/02/04|10/02/07|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161217|'ogrodniczka' KADEMu zajmująca się Ogrodami Weroniki Seton. Też: ekspert od biomanipulacji nastrojów zapachami. Nadal kochanka Silurii. Lubi deprymować nie-Silurię. W poszukiwaniu hedonizmu.|[Niezbyt legalna 'Academia' Whisperwind](/rpg/inwazja/opowiesci/konspekty/161217-niezbyt-legalna-academia-whisperwind.html)|10/01/30|10/01/31|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161216|sprzężona z rodów Diakon i Zajcew, 'destruktywna puryfikatorka' Instytutu Zniszczenia i (tymczasowa) kochanka Silurii. |[Szept z Academii Daemonica](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|10/01/27|10/01/29|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|Siluria Diakon|10|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html), [150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html), [150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html), [170417](/rpg/inwazja/opowiesci/konspekty/170417-ratujac-syrenopnacze.html), [170217](/rpg/inwazja/opowiesci/konspekty/170217-skradziona-pozytywka-mausow.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html), [161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html), [161217](/rpg/inwazja/opowiesci/konspekty/161217-niezbyt-legalna-academia-whisperwind.html), [161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|Ignat Zajcew|7|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html), [150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html), [170217](/rpg/inwazja/opowiesci/konspekty/170217-skradziona-pozytywka-mausow.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html), [161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html), [161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|Hektor Blakenbauer|5|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html), [150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html), [150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html), [170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|Whisperwind|4|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html), [161217](/rpg/inwazja/opowiesci/konspekty/161217-niezbyt-legalna-academia-whisperwind.html), [161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|Norbert Sonet|4|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html), [150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html), [150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html), [161217](/rpg/inwazja/opowiesci/konspekty/161217-niezbyt-legalna-academia-whisperwind.html)|
|Lucjan Kopidół|4|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html), [161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html), [161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|Warmaster|3|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html), [161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|Paweł Sępiak|3|[150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html), [170217](/rpg/inwazja/opowiesci/konspekty/170217-skradziona-pozytywka-mausow.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|Marcelin Blakenbauer|3|[150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html), [150411](/rpg/inwazja/opowiesci/konspekty/150411-dzien-z-zycia-klemensa.html), [170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|Karina Paczulis|3|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [161217](/rpg/inwazja/opowiesci/konspekty/161217-niezbyt-legalna-academia-whisperwind.html), [161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|Eis|3|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|Quasar|2|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|Mikado Diakon|2|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html)|
|Melodia Diakon|2|[170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html), [161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|Marta Szysznicka|2|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|Mariusz Trzosik|2|[150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html), [150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html)|
|Marianna Sowińska|2|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html)|
|Marcel Bankierz|2|[150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html), [150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html)|
|Karolina Kupiec|2|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|Jolanta Sowińska|2|[150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html), [161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|Ilona Amant|2|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|Franciszek Maus|2|[170217](/rpg/inwazja/opowiesci/konspekty/170217-skradziona-pozytywka-mausow.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|Edwin Blakenbauer|2|[150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html), [170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|Bogumił Rojowiec|2|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|Artur Kotała|2|[161217](/rpg/inwazja/opowiesci/konspekty/161217-niezbyt-legalna-academia-whisperwind.html), [161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|Andżelika Leszczyńska|2|[170417](/rpg/inwazja/opowiesci/konspekty/170417-ratujac-syrenopnacze.html), [161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|Alisa Wiraż|2|[150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html), [161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|Alina Bednarz|2|[150411](/rpg/inwazja/opowiesci/konspekty/150411-dzien-z-zycia-klemensa.html), [170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|Witold Wcinkiewicz|1|[170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|Wioletta Lemona-Chang|1|[161217](/rpg/inwazja/opowiesci/konspekty/161217-niezbyt-legalna-academia-whisperwind.html)|
|Vladlena Zajcew|1|[150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html)|
|Urszula Murczyk|1|[170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|Tymoteusz Maus|1|[170217](/rpg/inwazja/opowiesci/konspekty/170217-skradziona-pozytywka-mausow.html)|
|Tamara Muszkiet|1|[150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html)|
|Sławek Błyszczyk|1|[161217](/rpg/inwazja/opowiesci/konspekty/161217-niezbyt-legalna-academia-whisperwind.html)|
|Szymon Skubny|1|[170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|Siriratharin|1|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html)|
|Rukoliusz Bankierz|1|[170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|Rafael Diakon|1|[170417](/rpg/inwazja/opowiesci/konspekty/170417-ratujac-syrenopnacze.html)|
|Patrycja Krowiowska|1|[170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|Ofelia Caesar|1|[150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html)|
|Netheria Diakon|1|[170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|Mordecja Diakon|1|[161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|Mojra|1|[150411](/rpg/inwazja/opowiesci/konspekty/150411-dzien-z-zycia-klemensa.html)|
|Maurycy Maus|1|[170217](/rpg/inwazja/opowiesci/konspekty/170217-skradziona-pozytywka-mausow.html)|
|Marian Łajdak|1|[161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|Margaret Blakenbauer|1|[150411](/rpg/inwazja/opowiesci/konspekty/150411-dzien-z-zycia-klemensa.html)|
|Konstanty Myszeczka|1|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html)|
|Konrad Sowiński|1|[161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|Klemens X|1|[150411](/rpg/inwazja/opowiesci/konspekty/150411-dzien-z-zycia-klemensa.html)|
|Julian Krukowicz|1|[161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|Judyta Maus|1|[170217](/rpg/inwazja/opowiesci/konspekty/170217-skradziona-pozytywka-mausow.html)|
|Judyta Karnisz|1|[150411](/rpg/inwazja/opowiesci/konspekty/150411-dzien-z-zycia-klemensa.html)|
|Gala Zajcew|1|[170217](/rpg/inwazja/opowiesci/konspekty/170217-skradziona-pozytywka-mausow.html)|
|Emilia Kołatka|1|[150607](/rpg/inwazja/opowiesci/konspekty/150607-brat-przeciw-bratu.html)|
|Dionizy Kret|1|[170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|Demon_481|1|[150625](/rpg/inwazja/opowiesci/konspekty/150625-poligon-kluczem-do-marcelina.html)|
|Błażej Falka|1|[161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|Borys Kumin|1|[170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|Beata Zakrojec|1|[150411](/rpg/inwazja/opowiesci/konspekty/150411-dzien-z-zycia-klemensa.html)|
|Balrog Bankierz|1|[170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|Artur Żupan|1|[161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|Artur Bryś|1|[170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|Anna Myszeczka|1|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html)|
|Aleksandra Trawens|1|[161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|Akumulator Diakon|1|[170417](/rpg/inwazja/opowiesci/konspekty/170417-ratujac-syrenopnacze.html)|
|Adonis Sowiński|1|[170417](/rpg/inwazja/opowiesci/konspekty/170417-ratujac-syrenopnacze.html)|

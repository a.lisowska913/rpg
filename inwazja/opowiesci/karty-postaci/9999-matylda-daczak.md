---
layout: inwazja-karta-postaci
categories: profile
title: "Matylda Daczak"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|151003|"strażniczka moralności". Po przeleceniu wiły wyspowiadała się i nasłała na zamtuz księdza. Starsza pani.|[Zamtuz przestaje działać](/rpg/inwazja/opowiesci/konspekty/151003-zamtuz-przestaje-dzialac.html)|10/06/02|10/06/03|[Ucieczka do Przodka](/rpg/inwazja/opowiesci/konspekty/kampania-ucieczka-do-przodka.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Tymoteusz Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-tymoteusz-maus.html)|1|[151003](/rpg/inwazja/opowiesci/konspekty/151003-zamtuz-przestaje-dzialac.html)|
|[Tomasz Leżniak](/rpg/inwazja/opowiesci/karty-postaci/9999-tomasz-lezniak.html)|1|[151003](/rpg/inwazja/opowiesci/konspekty/151003-zamtuz-przestaje-dzialac.html)|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|1|[151003](/rpg/inwazja/opowiesci/konspekty/151003-zamtuz-przestaje-dzialac.html)|
|[Mariusz Czyczyż](/rpg/inwazja/opowiesci/karty-postaci/9999-mariusz-czyczyz.html)|1|[151003](/rpg/inwazja/opowiesci/konspekty/151003-zamtuz-przestaje-dzialac.html)|
|[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/1709-maria-newa.html)|1|[151003](/rpg/inwazja/opowiesci/konspekty/151003-zamtuz-przestaje-dzialac.html)|
|[Lea Swoboda](/rpg/inwazja/opowiesci/karty-postaci/9999-lea-swoboda.html)|1|[151003](/rpg/inwazja/opowiesci/konspekty/151003-zamtuz-przestaje-dzialac.html)|
|[Gala Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-gala-zajcew.html)|1|[151003](/rpg/inwazja/opowiesci/konspekty/151003-zamtuz-przestaje-dzialac.html)|

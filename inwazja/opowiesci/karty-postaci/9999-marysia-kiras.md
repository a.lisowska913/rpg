---
layout: inwazja-karta-postaci
categories: profile
title: "Marysia Kiras"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|150224|dziewczyna, która pamięta za dużo. Wyratowana ze Spustoszenia przez Spustoszonego Oktawiana Mausa, miała misję o której nie wiedziała.|[Wojna domowa Spustoszenia](/rpg/inwazja/opowiesci/konspekty/150224-wojna-domowa-spustoszenia.html)|10/04/17|10/04/20|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150110|sensei "Tarczy", która nie wie że jest klasyfikowana jako agent Millennium i przypomina sobie rzeczy których nie powinna wiedzieć|[Bezpieczna baza w Kotach](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html)|10/04/15|10/04/16|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|141115|sensei "Tarczy" kiedyś uczona przez Irenę, organizacji dbającej o Koty.|[GS Aegis 0002](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|10/04/13|10/04/14|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150121|osoba bardzo wdzięczna Konradowi za Festiwal, która zniszczyła misterny plan Aliny chroniąc nieprzytomnego maga.|[Nowe życie Aliny](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|10/01/15|10/01/16|[Blakenbauerowie x Skorpion](/rpg/inwazja/opowiesci/konspekty/kampania-blakenbauerowie-x-skorpion.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|3|[150224](/rpg/inwazja/opowiesci/konspekty/150224-wojna-domowa-spustoszenia.html), [150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html), [141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
|[Robert Mięk](/rpg/inwazja/opowiesci/karty-postaci/9999-robert-miek.html)|3|[150224](/rpg/inwazja/opowiesci/konspekty/150224-wojna-domowa-spustoszenia.html), [150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html), [141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
|[Paweł Sępiak](/rpg/inwazja/opowiesci/karty-postaci/1709-pawel-sepiak.html)|3|[150224](/rpg/inwazja/opowiesci/konspekty/150224-wojna-domowa-spustoszenia.html), [150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html), [141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
|[Zenon Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-zenon-weiner.html)|2|[150224](/rpg/inwazja/opowiesci/konspekty/150224-wojna-domowa-spustoszenia.html), [150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html)|
|[Tamara Muszkiet](/rpg/inwazja/opowiesci/karty-postaci/1709-tamara-muszkiet.html)|2|[150224](/rpg/inwazja/opowiesci/konspekty/150224-wojna-domowa-spustoszenia.html), [150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html)|
|[Oktawian Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-oktawian-maus.html)|2|[150224](/rpg/inwazja/opowiesci/konspekty/150224-wojna-domowa-spustoszenia.html), [150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html)|
|[Netheria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-netheria-diakon.html)|2|[141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html), [150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Marian Welkrat](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-welkrat.html)|2|[150224](/rpg/inwazja/opowiesci/konspekty/150224-wojna-domowa-spustoszenia.html), [150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html)|
|[Korwin Morocz](/rpg/inwazja/opowiesci/karty-postaci/9999-korwin-morocz.html)|2|[150224](/rpg/inwazja/opowiesci/konspekty/150224-wojna-domowa-spustoszenia.html), [150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html)|
|[Julia Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-julia-weiner.html)|2|[150224](/rpg/inwazja/opowiesci/konspekty/150224-wojna-domowa-spustoszenia.html), [150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html)|
|[Judyta Karnisz](/rpg/inwazja/opowiesci/karty-postaci/9999-judyta-karnisz.html)|2|[150224](/rpg/inwazja/opowiesci/konspekty/150224-wojna-domowa-spustoszenia.html), [150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html)|
|[miślęg Pieluszka](/rpg/inwazja/opowiesci/karty-postaci/9999-misleg-pieluszka.html)|1|[150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html)|
|[echo Lugardhaira](/rpg/inwazja/opowiesci/karty-postaci/9999-echo-lugardhaira.html)|1|[150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html)|
|[Warmaster](/rpg/inwazja/opowiesci/karty-postaci/9999-warmaster.html)|1|[150224](/rpg/inwazja/opowiesci/konspekty/150224-wojna-domowa-spustoszenia.html)|
|[Szczepan Czarniek](/rpg/inwazja/opowiesci/karty-postaci/9999-szczepan-czarniek.html)|1|[150224](/rpg/inwazja/opowiesci/konspekty/150224-wojna-domowa-spustoszenia.html)|
|[Sebastian Tecznia](/rpg/inwazja/opowiesci/karty-postaci/9999-sebastian-tecznia.html)|1|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Robert Przerot](/rpg/inwazja/opowiesci/karty-postaci/9999-robert-przerot.html)|1|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Pieluszka](/rpg/inwazja/opowiesci/karty-postaci/9999-pieluszka.html)|1|[150224](/rpg/inwazja/opowiesci/konspekty/150224-wojna-domowa-spustoszenia.html)|
|[Olaf Rajczak](/rpg/inwazja/opowiesci/karty-postaci/9999-olaf-rajczak.html)|1|[141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
|[Marta Newa](/rpg/inwazja/opowiesci/karty-postaci/9999-marta-newa.html)|1|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Marian Łajdak](/rpg/inwazja/opowiesci/karty-postaci/1709-marian-lajdak.html)|1|[141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
|[Margaret Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-margaret-blakenbauer.html)|1|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Lugardhair](/rpg/inwazja/opowiesci/karty-postaci/9999-lugardhair.html)|1|[150224](/rpg/inwazja/opowiesci/konspekty/150224-wojna-domowa-spustoszenia.html)|
|[Konrad Węgorz](/rpg/inwazja/opowiesci/karty-postaci/9999-konrad-wegorz.html)|1|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Klemens X](/rpg/inwazja/opowiesci/karty-postaci/9999-klemens-x.html)|1|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Jolanta Pirat](/rpg/inwazja/opowiesci/karty-postaci/9999-jolanta-pirat.html)|1|[150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html)|
|[Jerzy Buława](/rpg/inwazja/opowiesci/karty-postaci/9999-jerzy-bulawa.html)|1|[150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html)|
|[Infensa Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-infensa-diakon.html)|1|[150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html)|
|[GS "Aegis" 0002](/rpg/inwazja/opowiesci/karty-postaci/9999-gs-aegis-0002.html)|1|[141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
|[Filip Sztukar](/rpg/inwazja/opowiesci/karty-postaci/9999-filip-sztukar.html)|1|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Estrella Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-estrella-diakon.html)|1|[141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
|[Eleonora Wiaderska](/rpg/inwazja/opowiesci/karty-postaci/9999-eleonora-wiaderska.html)|1|[141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|1|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Bożena Górzec](/rpg/inwazja/opowiesci/karty-postaci/9999-bozena-gorzec.html)|1|[150110](/rpg/inwazja/opowiesci/konspekty/150110-bezpieczna-baza-w-kotach.html)|
|[Borys Kumin](/rpg/inwazja/opowiesci/karty-postaci/1709-borys-kumin.html)|1|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Basia Morocz](/rpg/inwazja/opowiesci/karty-postaci/9999-basia-morocz.html)|1|[141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
|[Artur Żupan](/rpg/inwazja/opowiesci/karty-postaci/1803-artur-zupan.html)|1|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Alina Bednarz](/rpg/inwazja/opowiesci/karty-postaci/1803-alina-bednarz.html)|1|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Adam Lisek](/rpg/inwazja/opowiesci/karty-postaci/9999-adam-lisek.html)|1|[141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|

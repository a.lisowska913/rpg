---
layout: inwazja-karta-postaci
categories: profile
factions: "Niezrzeszeni"
type: "PC"
title: "Małż Mateusz"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **BÓL: Niewolnik**:
    * _Aspekty_: pragnie wolności
    * _Opis_: 
* **MRZ: Dominacja nad światem**:
    * _Aspekty_: chce zdobyć niewolników
    * _Opis_: 

### Umiejętności

* **Wiedza małży**:
    * _Aspekty_: świat ludzi
    * _Opis_: małże swoją wiedzę zdobyły głównie z internetu i telewizji (poprzez technomancję)
* **Automatyka i robotyka**: 
    * _Aspekty_: zdalny hacker, kompatybilność radiowa, zdalne sterowanie, zdobywanie informacji (cyfrowych), strzelanie
    * _Opis_: przechwytywanie fal elektro-magnetycznych, nadawanie włąsnego sygnału, hackowanie urządzeń elektronicznych, przejmowanie kontroli nad maszynami
* **Perswazja**:
    * _Aspekty_: 
    * _Opis_: 
    
### Silne i słabe strony:

* **Małżowy umysł**:
    * _Aspekty_: niedominowalny, nie rozumie ludzi, brak empatii, brak kręgosłupa moralnego
    * _Opis_: 
* **Ciało małża**: 
    * _Aspekty_: wszędzie wlezie,  odrażająca aparycja, słaby
    * _Opis_: 
* **Małżlink**:
    * _Aspekty_: komunikacja na odległość, wrażliwy na los innych małży
    * _Opis_: 

## Magia

### Szkoły magiczne

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

### Zaklęcia statyczne

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 

## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Zarobki

* ?

### Znam

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

### Mam

* **Magicrawler**:
    * _Aspekty_: minidziałko laserowe, drony
    * _Opis_: 
* **Mała grupa mało rozgarniętych minionów**:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

# Opis

krótki opis postaci, rzeczy, jakie warto pamiętać.

### Koncept

Wysokopoziomowy koncept postaci, 1-2 zdania.

### Motto

"Tekst"

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170725|też małż. Zdominował konstruminusa, shackował kamerę i grawitacyjnie ogłuszył krzepkiego ochroniarza. Skaził Jodłowiec i uciekł z konstruminusem.|[Krzywdzę, bo kocham](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|10/05/09|10/05/11|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Silgor Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-silgor-diakon.html)|1|[170725](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|
|[Małż Poszukiwacz](/rpg/inwazja/opowiesci/karty-postaci/1709-malz-poszukiwacz.html)|1|[170725](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|
|[Mateusz Ackmann](/rpg/inwazja/opowiesci/karty-postaci/1709-mateusz-ackmann.html)|1|[170725](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|
|[Henryk Gwizdon](/rpg/inwazja/opowiesci/karty-postaci/9999-henryk-gwizdon.html)|1|[170725](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|
|[Fryderyk Bakłażan](/rpg/inwazja/opowiesci/karty-postaci/1709-fryderyk-baklazan.html)|1|[170725](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|
|[Dyta](/rpg/inwazja/opowiesci/karty-postaci/9999-dyta.html)|1|[170725](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|1|[170725](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|
|[Barnaba Łonowski](/rpg/inwazja/opowiesci/karty-postaci/9999-barnaba-lonowski.html)|1|[170725](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|

---
layout: inwazja-karta-postaci
categories: profile
factions: "Srebrna Świeca"
type: "NPC"
title: "Balrog Bankierz"
---
# {{ page.title }}

## Postać
 
### Motywacje (do czego dąży)

* **FIL: Oportunista**:
    * _Aspekty_: hulaka
    * _Opis_: "Trzeba korzystać z okazji, bo cholera wie, kiedy pojawi się kolejna okazja. Agresywnie. A cholera wie, ile jeszcze będziemy żyć; kolejne Zaćmienie może wszystko rozwalić."
* **BÓL: Wróg - Zajcewowie**:
    * _Aspekty_: 
    * _Opis_: "Ogniste ptaszki też skwierczą jak się je odpowiednio poddusi. Najlepiej publicznie."
* **FIL: Kobieciarz**:
    * _Aspekty_: próżny
    * _Opis_: "Sława i kobiety! Dwie rzeczy, które sprawiają, że jest po co żyć. Mam świetne ciało, dużo sukcesów i ogólnie jestem niezrównany. Jak można tego nie doceniać?"
* **FIL: Opiekuńczy**:
    * _Aspekty_: 
    * _Opis_: "Jak już na kimś mi zależy, ona będzie miała gwiazdkę z cholernego nieba którą jej własna rywalka przyniesie jej na cholernych kolanach"
* **FIL: Przebojowy**:
    * _Aspekty_: 
    * _Opis_: "Na szczyt, kamraci! Za mną! Wszystko się da. Co, nie zagadasz do niej? To ja zagadam."
	
### Umiejętności

* **Gladiator**:
    * _Aspekty_: kulturysta, katai
    * _Opis_: 
* **Showman**:
    * _Aspekty_: prowokacja, wygadany, wystąpienia publiczne, charakteryzacja
    * _Opis_: 
* **Trener gladiatorów**:
    * _Aspekty_: przywódca
    * _Opis_: 
* **Marketingowiec**:
    * _Aspekty_: 
    * _Opis_: 
    
### Silne i słabe strony:

* **??**:
    * _Aspekty_: 
    * _Opis_: 
	
## Magia

### Szkoły magiczne

* **??**:
    * _Aspekty_: 
    * _Opis_: 
* **??**:
    * _Aspekty_: 
    * _Opis_: 
* **??**:
    * _Aspekty_: 
    * _Opis_: 

### Zaklęcia statyczne

* **??**:
    * _Aspekty_: 
    * _Opis_: 

## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Zarobki

dość bogaty i wpływowy; utrzymuje się z fanów i inwestycji
+1 kieszonkowego do wydania na dowolną Sławę lub Fanów
PRZYMUS wydania 1 na wydarzenie typu impreza itp. To hulaka ;-).

### Znam

* **Grupa zaciekłych fanów**:
    * _Aspekty_: 
    * _Opis_: 
* **Liczne eks-, fanki i przyjaciółki**:
    * _Aspekty_: 
    * _Opis_: 
* **Powszechnie znany w różnych knajpach i lokalach**:
    * _Aspekty_: 
    * _Opis_: 
* **??**:
    * _Aspekty_: 
    * _Opis_: 
	
### Mam

* **Sława hojnego kobieciarza i hulaki**:
    * _Aspekty_: 
    * _Opis_: 
* **Sława gladiatora i wyszczekanego showmana**:
    * _Aspekty_: 
    * _Opis_: 
* **Dużo nietypowych 'broni' gladiatora**:
    * _Aspekty_: 
    * _Opis_: 
* **Agencja reklamowa Świecy**:
    * _Aspekty_: 
    * _Opis_: 	
* **Dostęp do menażerii i egzotycznych istot**:
    * _Aspekty_: 
    * _Opis_: 		
	

# Opis
An almost 40 years old gladiator, who moved into more of a leadership role. This guy is extremely focused on his influence on others, especially young girls due to obvious reasons. Might and glory would be his motto. A very sturdy and nimble sportsman, he uses his looks, history and abilities to remain on top even if he is not the best warrior out there.

The viewers usually want a performance. He gives them a performance. He fights in a very spectacular, flashy way. He can make some inspiring speeches; he can talk the talk and he will walk the walk. Not the most inspiring of the best leaders around, not even a good leader, but he tries to insert himself into every advantageous situation and get something out of it. And then, boast about his stuff to never become forgotten.

The Eclipse scared him. He has seen many better magi fall into disgrace. He considers himself lucky and intend to live his life fully. You never know when another Eclipse is going to strike…

## Motto

"Tekst"

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Klub Dare Shiver](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|wybił się kosztem Ignata Zajcewa jako paladyn w lśniącej zbroi chroniący Oliwiera|Powrót Karradraela|


## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170115|lansuje się kosztem Ignata, ale czas powoli zejść z tego konia; wyekstraktował całość wartości jaką był w stanie na tym etapie. Łaskawie wycofał pojedynek.|[Klub Dare Shiver](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|10/02/22|10/02/25|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170111|oportunistyczny wojownik i showman, który zdecydował się wypłynąć i zdobyć popularność na fali podłego uczynku Ignata |[EIS na kozetce](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|10/02/18|10/02/21|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|2|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Netheria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-netheria-diakon.html)|2|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Karolina Kupiec](/rpg/inwazja/opowiesci/karty-postaci/1803-karolina-kupiec.html)|2|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Ignat Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1802-ignat-zajcew.html)|2|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Bogumił Rojowiec](/rpg/inwazja/opowiesci/karty-postaci/9999-bogumil-rojowiec.html)|2|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Urszula Murczyk](/rpg/inwazja/opowiesci/karty-postaci/1709-urszula-murczyk.html)|1|[170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Quasar](/rpg/inwazja/opowiesci/karty-postaci/9999-quasar.html)|1|[170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Paweł Sępiak](/rpg/inwazja/opowiesci/karty-postaci/1709-pawel-sepiak.html)|1|[170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Melodia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-melodia-diakon.html)|1|[170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Kornelia Kartel](/rpg/inwazja/opowiesci/karty-postaci/9999-kornelia-kartel.html)|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|[Judyta Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-judyta-maus.html)|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|[Jolanta Sowińska](/rpg/inwazja/opowiesci/karty-postaci/1709-jolanta-sowinska.html)|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|[Joachim Kopiec](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-kopiec.html)|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|[Joachim Kartel](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-kartel.html)|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|[Jan Wątły](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-watly.html)|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|[Infernia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-infernia-diakon.html)|1|[170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Ilona Amant](/rpg/inwazja/opowiesci/karty-postaci/9999-ilona-amant.html)|1|[170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Franciszek Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-maus.html)|1|[170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Elizawieta Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-elizawieta-zajcew.html)|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|[Eis](/rpg/inwazja/opowiesci/karty-postaci/9999-eis.html)|1|[170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Adonis Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1709-adonis-sowinski.html)|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|

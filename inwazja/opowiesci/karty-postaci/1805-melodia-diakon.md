---
layout: inwazja-karta-postaci
categories: profile
factions: "Millennium"
type: "NPC"
owner: "public"
title: "Melodia Diakon"
---
# {{ page.title }}

## Koncept

Zaangażowana piosenkarka i Diakonka - w dzień gwiazda pop, w nocy aktywistka; bardzo pro-kralotyczna. Hipnotyczna uzdrowicielka duszy. Sheryl + Mroczna Hipiska + Mindwarp + Terpsichore.

## Misja

Zostać najsławniejszą i najlepszą gwiazdą pop w świecie magów i doprowadzić do równouprawnienia świata ludzi i świata kralothów.

## Wada

Zbyt arogancka i zbyt skłonna do eksperymentalnych nowości. Ma tendencje do brania wszystkich wyzwań na siebie, niezależnie od tego jak niemożliwe by one nie były.

## Postać

### Motywacje

| Co chce by się działo? Co jest pożądane?                 | Co jest antytezą postaci? Co jest jej negacją?             |
|----------------------------------------------------------|------------------------------------------------------------|
| zapewniaj kojenie przez przyjemność; promuj hedonizm i przyjemność | zaakceptuj cierpienie jako część życia; promuj pracę i czystość ciała i ducha |
| stój na straży bezpieczeństwa; zawsze promuj mag+człowiek+vicinius | promuj wolność nad innych; promuj dominację jednej grupy nad drugą |
| bądź lojalna fanom i ich wspieraj; eksperymentuj i żyj pełną piersią | preferuj chwałę i relacje nad sukces; działaj ostrożnie, bez narażania się |

### Umiejętności

#### Ogólnie

* Ogólnie: wysportowana i atletyczna performerka i artystka, "weaponized psychologist" kojąca cierpienie. Dobra obserwatorka ludzi, magów i viciniusów.
* Mistrzowska performerka, która potrafi robić doskonałe show. Śpiewa, tańczy i gra na instrumentach. Też: mistrzyni ars amandi i uwodzenia.
* Aktorka i piosenkarka wysokiej klasy. Kontroluje tłumy i emocje przez muzykę bez większego problemu. Pełna onieśmielającej gracji i elegancji. Przyciąga uwagę.
* Mistrzyni manipulacji jednostek i tłumów; wykorzystuje te swoje umiejętności do kojenia... ale nie ma nic przeciwko ujawnieniu koszmarów u swojej ofiary.

#### Manewry

| Jakie działania wykonuje?                       | Czym osiąga sukces?                                                |
|-------------------------------------------------|--------------------------------------------------------------------|
| wydobycie marzeń i koszmarów; zdobycie informacji; czytanie intencji | charyzma; ars amandi; rozmowa i terapia; uwodzenie i gracja |
| kontrola tłumów; inspirowanie; sterowanie pryzmatem; skupienie uwagi na sobie | charyzma; występ publiczny; płomienna przemowa |
| perswazja jednostki; zauroczenie; kojenie cierpienia; leczenie umysłów | występ publiczny; ars amandi; rozmowa i terapia |

### Silne i słabe strony

| Co jest wzmocnione                          | Kosztem czego                 |
|---------------------------------------------|-------------------------------|
| przyciąga uwagę urodą | NIE POTRAFI się zamaskować; fatalna w covert ops |
| wzmocnienie Friendly, Devious hipnotycznym głosem | przyciąga niewłaściwą uwagę |

## Magia

### Gdy kontroluje energię

* Jej magia skierowana jest na leczenie umysłów i wpływanie na tłumy. Mentalistka, zwłaszcza w obszarze kojenia, hipnozy i transu.
* Czarodziejka imprezowa i przyjemności. Pasują do niej: stymulanty, środki psychoaktywne, stroje, imprezy, ekstaza i ars amandi.
* Potężnej klasy iluzjonistka i czarodziejka zmysłów. Zazwyczaj iluzje wykorzystuje do imprez i ars amandi (lub bojowo: koszmarów).

### Gdy traci kontrolę

* Gdy jest w dobrym humorze, magia kieruje się na ekstazę, przyjemność, błogość i radość. To jest zwykle magia uderzająca w szerszą grupę.
* Gdy jest przestraszona lub w złym humorze, jej magia kieruje się na koszmary i kierowanie pełni uwagi na Melodię.
* Czasami jej magia samorzutnie próbuje wprowadzić aspekty imprezowe - muzyka, stroje, stymulanty.

## Zasoby i otoczenie

### Powiązane frakcje

{{ page.factions }}

#### Ogólnie

* Ma niesamowity szacunek u kralothów, nie mówiąc już o Laragnarhagu. Potrafi wykorzystać ich wiedzę i biosyntezowane środki do wielu różnych celów.
* Ma też sporą rzeszę lojalnych i kochających ją fanów; wszędzie ma oczy i uszy oraz wszędzie ktoś ją przygarnie do domu.
* Ogromna ilość strojów, rzeczy do efektów specjalnych i ogólnie rozumianych "rzeczy imprezowych".
* Melodia ma Reputację. Trochę frywolną, trochę wesołą, ale ogólnie pozytywną jako mediatorka i osoba chcąca dobra.

#### Manewry

| Jakie działania wykonuje?                         | Czym osiąga sukces?                                      |
|---------------------------------------------------|----------------------------------------------------------|
| wzmocnienie występów; wywoływanie wrażenia | propsy i stroje; efekty specjalne |
| wzmocnienie możliwości fizycznych; wzmocnienie ars amandi | stymulanty; kralotyczne środki |

## Opis

### Ogólnie

Melodia is also known as Melodia Hipnoza Radość Diakon. She is a born pop star. A kind, beautiful and very cheerful girl who does not panic, like, ever. She is an illusionist with the power of hypnosis with the construction weaved by her music. Melody likes to help others and really tries to use her illusions to sooth the pain of the others. Her second name used to be Daydream, however after some situations they changed it to Hypnosis to her irritation. She is no saint, however; when needed, she can get quite forceful. Usually she keeps a pleasant demeanor and moves through life with grace and elegance, always getting what she wants mixing the illusions with reality. She actually likes to sing – and she is good at it. She is beautiful even for her bloodline standard – especially, because she actively works to be pretty. She considers her body to be one of the many options she has at her disposal.

At this point it seems, that her goal is to be the most famous and most recognized pop singer mage in the area. She doesn't compete with humans; only with Magi and other forms of entertainment accessible to magi. She considers herself to be an activist artist; someone who does more than just follow the art; someone, who uses the art to serve a particular purpose.

### Motywacje

Bardzo ambitna Diakonka, do tego utalentowana. Chce być czymś więcej niż piosenkarką - chce zmienić świat swoją muzyką (i nie tylko). Bardzo pro-kralotyczna. Chce umieć robić doskonałe performance i być wybitną piosenkarką. Chce też być uważana za dobrą osobę. Dodatkowo interesuje się... wszystkim. Ciekawska; kosztuje życia pełną piersią.

Melodia ma też cechy które sprawiały, że zastanawiała się nad dołączeniem do Arazille. Brak cierpienia, powszechna radość... jednak nie podjęła tej decyzji. Melodia uważa, że zasady chronią słabszych i pomagają wszystkim - porządek jest potrzebny. Tylko WŁAŚCIWY porządek. Taki, o jaki ona walczy. M.in. kralothy i ludzie żyjący koło siebie ;-).

Dla Melodii ważne jest wygrywanie i prowadzenie swoich popleczników do sukcesu. Jest lojalna osobom, które jej wierzą - nie uważa ich za trybiki i narzędzia. Często uczy się ich imion na pamięć. Ceni sobie bardzo sztukę i piękno - uważa je za higieniczne elementy życia.

Melodia działa osobiście. Kieruje ją przekonanie, że cele które ma osiągnąć są słuszne - i musi osiągnąć je osobiście. Jakkolwiek potrafi delegować, uważa, że jej obecność jest niezbędna by "to się działo". Czy z pierwszej czy z drugiej linii - nie ma to znaczenia. Liczy się sukces.

### Działanie

Melodia nie tylko potrafi śpiewać - potrafi też tańczyć, grać na paru instrumentach i odpowiednio robić show, zwłaszcza przy użyciu swoich narzędzi iluzji. Jest to czarodziejka zaangażowana - nie zawaha się użyć swoich przewag do osiągnięcia tego, co jej zdaniem powinno się stać.

"Weaponized psychologist". Melodia jest mistrzynią czytania swoich rozmówców i obracania ich własnych lęków i marzeń przeciwko nim. Wspomagana odpowiednimi środkami psychoaktywnymi, grą świateł i atmosferą grupy, nie przez przypadek Melodii nadano imię "Hipnoza". Jednocześnie, Melodia chce leczyć umysły i koić ból - to jej główny kierunek.

Melodia nie jest lekarzem. Jednocześnie potrafi pomóc w potrzebie - pierwsza pomoc, pierwsza terapia... z braku laku i Melodia dobra. Daje jej to unikalną rolę barda na polu bitwy - coś, za czym sama Melodia nieszczególnie przepada.

### Specjalne

Melodia jest zjawiskową czarodziejką nawet jak na Diakonkę. Jej delikatna elegancja połączona z pięknym głosem i siłą osobowości sprawiają, że Melodia zawsze jest "na świeczniku". Praktycznie nie ma możliwości by Melodia ukryła się przed fanami czy osobami jej szukającymi, nawet, jeśli próbuje. Po drugiej stronie równania jest jednak jej cudowny, urzekający głos potrafiący wzmocnić jej przekaz emocji - głos przekonywujący i o niezwykłej skali.

### Magia

Przede wszystkim stymulanty i stymulatory, m.in. by była w stanie występować odpowiednio długo. Wsparcie przy ars amandi. Wsparcie przy transach, hipnozach i otępiaczach. Dodatkowo, pomniejsza magia lecznicza. Nie jest lekarzem, ale jest uzdrowicielką.

### Otoczenie

Ma znajomości w kralotycznym świecie, ze szczególnym naciskiem na Laragnarhaga; Melodia jest bardzo pro-kralotyczna. Uważa, że kohabitacja jest możliwa a nawet korzystna i konieczna. To sprawiło, że otworzyła się bardzo na kralotyczną naturę, fizycznie i mentalnie. Kralothy Millennium z Melodią współpracują; Melodia ma też 'street cred' w świecie kralothów mogących się zetknąć z nią czy Millennium.

Melodia - co o niej nie mówić - ma reputację świetnej artystki i jest powszechnie rozpoznawana, co czasem pomaga a czasem bardzo przeszkadza. Ma też fanów - to grupa miłośników Melodii, którzy za nią podążają i są skłonni zrobić dla swojej idolki bardzo wiele różnych rzeczy. Melodia zwykle wykorzystuje ich do zbierania informacji czy wywołanie odpowiedniej presji społecznej...

### Motto

"Sztuka to najwyższa forma przyjemności... i kształtowania społeczeństw"

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Nie podłożona świnia Łucji](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html)|oczarowała Łucję Maus; ma na nią ogromny wpływ|Wizja Dukata|


## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|180214|zaczęła od polowania na kralotha ale skończyła jako Bicz Na Tomasza Myszeczkę z prośby Pauliny i Daniela.|[Nie podłożona świnia Łucji](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html)|11/10/15|11/10/17|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|180110|ostrzegła Paulinę przed obecnością innego kralotha na tym terenie. Dodatkowo zobowiązała się do znalezienia tamtego kralotha z Laragnarhagiem.|[Odzyskana władza Pauliny](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html)|11/10/10|11/10/12|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171115|dotarła by pomóc Paulinie; jej ciężarówka była zagrożona przez Wiaczesława. Paulina dała jej immunitet i Melodia dotarła do magitrowni.|[Kryzysowo tymczasowy dyktator](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|11/10/03|11/10/04|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|180402|silnie współpracuje z Laragnarhagiem i uczestniczy w War Room KADEMu. Zapewnia potrzebną wiedzę o kralothach dzięki Laragnarhagowi.|[Pętla dookoła niekralotha](/rpg/inwazja/opowiesci/konspekty/180402-petla-dookola-niekralotha.html)|10/12/13|10/12/15|[Adaptacja kralotyczna](/rpg/inwazja/opowiesci/konspekty/kampania-adaptacja-kralotyczna.html)|
|170103|zaproponowała Andrei parę: Bianka x Tadeusz. Niestety, gorzej z wykonaniem; Dalia coś zwęszyła. Ogólnie, uda się.|[Wojna Bogów w Czeliminie](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html)|10/07/16|10/07/18|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170101|pełni rolę corruptora. Wpierw Andżelika, potem Wioletta x Pszczelak x Laragnarhag, potem Przylaz. A przy tym wszystkim urocza i uśmiechnięta. Scary.|[Patrol? Kralotyczne maskowanie!](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|10/07/13|10/07/15|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161231|próbuje naprawić Wiolettę kontynuując pracę Silurii. Chce pomóc Andrei. Kończy na... malowaniu piór Deiiwa.|[Eskalacja Czelimina, eskalacja Andrei](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html)|10/07/11|10/07/12|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161229|posłaniec Rafaela, uderzona przez Mieszka, chroni dobre imię Silurii i ogólnie niezbyt poważana przez Świecę jako 'Diakonka do łóżka' i nic więcej. |[Presja ze strony Czelimina](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html)|10/07/08|10/07/10|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161204|gwiazdka Millennium przynosząca radość. Stabilizuje Tadeusza Barana i odzyskała dla Andrei stary emiter hipernetowy. Można polubić. Niestety, lubi kralotha.|[Zajcewowie po drugiej stronie](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html)|10/07/05|10/07/07|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170111|ściągnięta przez Silurię, by wymęczyć Balroga Bankierza. Sympatyzuje z Balrogiem bardziej niż z Ignatem, ale nie chce wojny gildii.|[EIS na kozetce](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|10/02/18|10/02/21|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170808|poproszona o pomoc przez "Doktora Gekona" (Millennium) dołączyła do zespołu Marcelina i Piotra. Zaryzykowała wejście na scenę gestalta opery by uratować ludzi.|[Duch Opery](/rpg/inwazja/opowiesci/konspekty/170808-duch-opery.html)|10/02/17|10/02/19|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161222|ściągnięta przez Silurię jako 'ciężka artyleria' na konkurs; pragmatyczna idealistka ścierająca się z Konradem Sowińskim.|[Kto wpisał Błażeja do konkursu](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|10/02/04|10/02/07|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Mieszko Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-mieszko-bankierz.html)|5|[170103](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html), [170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html), [161229](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html)|
|[Marian Łajdak](/rpg/inwazja/opowiesci/karty-postaci/1709-marian-lajdak.html)|5|[180402](/rpg/inwazja/opowiesci/konspekty/180402-petla-dookola-niekralotha.html), [170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html), [161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Laragnarhag](/rpg/inwazja/opowiesci/karty-postaci/1709-laragnarhag.html)|5|[180402](/rpg/inwazja/opowiesci/konspekty/180402-petla-dookola-niekralotha.html), [170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html), [161229](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html)|
|[Andrea Wilgacz](/rpg/inwazja/opowiesci/karty-postaci/1709-andrea-wilgacz.html)|5|[170103](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html), [170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html), [161229](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html)|
|[Stalowy Śledzik Żarłacz](/rpg/inwazja/opowiesci/karty-postaci/9999-stalowy-sledzik-zarlacz.html)|4|[170103](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html), [170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html)|
|[Rudolf Jankowski](/rpg/inwazja/opowiesci/karty-postaci/9999-rudolf-jankowski.html)|4|[170103](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html), [161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html), [161229](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html)|
|[Rafael Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-rafael-diakon.html)|4|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html), [161229](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html)|
|[Świeży Lilak](/rpg/inwazja/opowiesci/karty-postaci/9999-swiezy-lilak.html)|3|[170103](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html), [170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html)|
|[Łucja Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-lucja-maus.html)|3|[180214](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html), [180110](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html), [171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|
|[Wioletta Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-wioletta-bankierz.html)|3|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html), [161229](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html)|
|[Tatiana Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-tatiana-zajcew.html)|3|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html), [161229](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html)|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|3|[180402](/rpg/inwazja/opowiesci/konspekty/180402-petla-dookola-niekralotha.html), [170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html), [161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|3|[180214](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html), [180110](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html), [171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|
|[Marian Agrest](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-agrest.html)|3|[170103](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html), [170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161229](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html)|
|[Lidia Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-lidia-weiner.html)|3|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161229](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html)|
|[Kirył Sjeld](/rpg/inwazja/opowiesci/karty-postaci/9999-kiryl-sjeld.html)|3|[170103](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html), [161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html)|
|[Julian Pszczelak](/rpg/inwazja/opowiesci/karty-postaci/1709-julian-pszczelak.html)|3|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161229](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html)|
|[Daniel Akwitański](/rpg/inwazja/opowiesci/karty-postaci/1709-daniel-akwitanski.html)|3|[180214](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html), [180110](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html), [171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|
|[Dalia Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-dalia-weiner.html)|3|[170103](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html), [170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html)|
|[Andżelika Leszczyńska](/rpg/inwazja/opowiesci/karty-postaci/1709-andzelika-leszczynska.html)|3|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html), [161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Wiaczesław Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-wiaczeslaw-zajcew.html)|2|[180110](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html), [171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|
|[Tomasz Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-tomasz-myszeczka.html)|2|[180214](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html), [180110](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html)|
|[Tadeusz Baran](/rpg/inwazja/opowiesci/karty-postaci/1709-tadeusz-baran.html)|2|[170103](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html), [170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|
|[Rodion Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-rodion-zajcew.html)|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html)|
|[Laurena Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-laurena-bankierz.html)|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html)|
|[Kajetan Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-kajetan-weiner.html)|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html)|
|[Jolanta Sowińska](/rpg/inwazja/opowiesci/karty-postaci/1709-jolanta-sowinska.html)|2|[161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html), [161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Joachim Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-maus.html)|2|[180214](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html), [180110](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html)|
|[Joachim Kopiec](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-kopiec.html)|2|[170103](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html), [170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|
|[Infernia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-infernia-diakon.html)|2|[170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html), [161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Ignat Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1802-ignat-zajcew.html)|2|[170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html), [161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Draconis Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-draconis-diakon.html)|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161229](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html)|
|[Żaneta Kroniacz](/rpg/inwazja/opowiesci/karty-postaci/9999-zaneta-kroniacz.html)|1|[170808](/rpg/inwazja/opowiesci/konspekty/170808-duch-opery.html)|
|[Zuzanna Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-zuzanna-maus.html)|1|[170103](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html)|
|[Zofia Łaziarak](/rpg/inwazja/opowiesci/karty-postaci/9999-zofia-laziarak.html)|1|[170808](/rpg/inwazja/opowiesci/konspekty/170808-duch-opery.html)|
|[Whisperwind](/rpg/inwazja/opowiesci/karty-postaci/9999-whisperwind.html)|1|[161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Urszula Murczyk](/rpg/inwazja/opowiesci/karty-postaci/1709-urszula-murczyk.html)|1|[170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Sylwester Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-sylwester-bankierz.html)|1|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|
|[Robert Sądeczny](/rpg/inwazja/opowiesci/karty-postaci/1709-robert-sadeczny.html)|1|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|
|[Rafał Drętwoń](/rpg/inwazja/opowiesci/karty-postaci/9999-rafal-dretwon.html)|1|[180402](/rpg/inwazja/opowiesci/konspekty/180402-petla-dookola-niekralotha.html)|
|[Quasar](/rpg/inwazja/opowiesci/karty-postaci/9999-quasar.html)|1|[170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Piotr Kit](/rpg/inwazja/opowiesci/karty-postaci/1709-piotr-kit.html)|1|[170808](/rpg/inwazja/opowiesci/konspekty/170808-duch-opery.html)|
|[Paweł Sępiak](/rpg/inwazja/opowiesci/karty-postaci/1709-pawel-sepiak.html)|1|[170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Operiatrix](/rpg/inwazja/opowiesci/karty-postaci/9999-operiatrix.html)|1|[170808](/rpg/inwazja/opowiesci/konspekty/170808-duch-opery.html)|
|[Netheria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-netheria-diakon.html)|1|[170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Mordecja Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-mordecja-diakon.html)|1|[161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Marek Kromlan](/rpg/inwazja/opowiesci/karty-postaci/1803-marek-kromlan.html)|1|[180402](/rpg/inwazja/opowiesci/konspekty/180402-petla-dookola-niekralotha.html)|
|[Marcin Warinsky](/rpg/inwazja/opowiesci/karty-postaci/1709-marcin-warinsky.html)|1|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|
|[Marcelin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-marcelin-blakenbauer.html)|1|[170808](/rpg/inwazja/opowiesci/konspekty/170808-duch-opery.html)|
|[Lucjan Kopidół](/rpg/inwazja/opowiesci/karty-postaci/1803-lucjan-kopidol.html)|1|[161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Konrad Sowiński](/rpg/inwazja/opowiesci/karty-postaci/9999-konrad-sowinski.html)|1|[161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Katalina Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-katalina-bankierz.html)|1|[161229](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html)|
|[Karradrael](/rpg/inwazja/opowiesci/karty-postaci/9999-karradrael.html)|1|[170103](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html)|
|[Karolina Kupiec](/rpg/inwazja/opowiesci/karty-postaci/1803-karolina-kupiec.html)|1|[170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Karina Łoszad](/rpg/inwazja/opowiesci/karty-postaci/9999-karina-loszad.html)|1|[161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html)|
|[Kaja Maślaczek](/rpg/inwazja/opowiesci/karty-postaci/1709-kaja-maslaczek.html)|1|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|
|[Julian Strąk](/rpg/inwazja/opowiesci/karty-postaci/9999-julian-strak.html)|1|[180402](/rpg/inwazja/opowiesci/konspekty/180402-petla-dookola-niekralotha.html)|
|[Julian Krukowicz](/rpg/inwazja/opowiesci/karty-postaci/9999-julian-krukowicz.html)|1|[161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Jessica Czułmik](/rpg/inwazja/opowiesci/karty-postaci/9999-jessica-czulmik.html)|1|[170808](/rpg/inwazja/opowiesci/konspekty/170808-duch-opery.html)|
|[Jacek Molenda](/rpg/inwazja/opowiesci/karty-postaci/9999-jacek-molenda.html)|1|[161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html)|
|[Ilona Amant](/rpg/inwazja/opowiesci/karty-postaci/9999-ilona-amant.html)|1|[170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Franciszek Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-myszeczka.html)|1|[161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html)|
|[Franciszek Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-maus.html)|1|[170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Ewelina Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-ewelina-bankierz.html)|1|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|
|[Ernest Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-ernest-maus.html)|1|[170103](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html)|
|[Elea Maus](/rpg/inwazja/opowiesci/karty-postaci/1802-elea-maus.html)|1|[180402](/rpg/inwazja/opowiesci/konspekty/180402-petla-dookola-niekralotha.html)|
|[Eis](/rpg/inwazja/opowiesci/karty-postaci/9999-eis.html)|1|[170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|1|[180402](/rpg/inwazja/opowiesci/konspekty/180402-petla-dookola-niekralotha.html)|
|[Deiiw Podniebny Grom](/rpg/inwazja/opowiesci/karty-postaci/9999-deiiw-podniebny-grom.html)|1|[161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html)|
|[Dagmara Wyjątek](/rpg/inwazja/opowiesci/karty-postaci/1709-dagmara-wyjątek.html)|1|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|
|[Błażej Falka](/rpg/inwazja/opowiesci/karty-postaci/9999-blazej-falka.html)|1|[161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Bolesław Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-boleslaw-maus.html)|1|[180214](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html)|
|[Bogumił Rojowiec](/rpg/inwazja/opowiesci/karty-postaci/9999-bogumil-rojowiec.html)|1|[170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Bianka Stein](/rpg/inwazja/opowiesci/karty-postaci/1709-bianka-stein.html)|1|[170103](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html)|
|[Balrog Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-balrog-bankierz.html)|1|[170111](/rpg/inwazja/opowiesci/konspekty/170111-eis-na-kozetce.html)|
|[Artur Żupan](/rpg/inwazja/opowiesci/karty-postaci/1803-artur-zupan.html)|1|[161222](/rpg/inwazja/opowiesci/konspekty/161222-kto-wpisal-blazeja-do-konkursu.html)|
|[Arazille](/rpg/inwazja/opowiesci/karty-postaci/9999-arazille.html)|1|[170103](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html)|
|[Apoloniusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-apoloniusz-bankierz.html)|1|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|
|[Anna Kozak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kozak.html)|1|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|
|[Aneta Rainer](/rpg/inwazja/opowiesci/karty-postaci/1709-aneta-rainer.html)|1|[161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html)|
|[Anastazja Sjeld](/rpg/inwazja/opowiesci/karty-postaci/9999-anastazja-sjeld.html)|1|[170103](/rpg/inwazja/opowiesci/konspekty/170103-wojna-bogow-w-czeliminie.html)|
|[Alojzy Przylaz](/rpg/inwazja/opowiesci/karty-postaci/1803-alojzy-przylaz.html)|1|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|

---
layout: inwazja-karta-postaci
categories: profile
title: "Amelia Eter"
---
# {{ page.title }}

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Porwanie pod nosem hipisów](/rpg/inwazja/opowiesci/konspekty/180321-porwanie-pod-nosem-hipisow.html)|jest kluczem do zrozumienia odbić Damy w Czerwonym, choć nie wie dlaczego i o co chodzi.|Adaptacja kralotyczna|
|[Porwanie pod nosem hipisów](/rpg/inwazja/opowiesci/konspekty/180321-porwanie-pod-nosem-hipisow.html)|okazuje się, że jest niewrażliwa na pieśni fabokli. Z jakiegoś powodu Arazille jej nie Dotyka.|Adaptacja kralotyczna|
|[Porwanie pod nosem hipisów](/rpg/inwazja/opowiesci/konspekty/180321-porwanie-pod-nosem-hipisow.html)|zsynchronizowała się ze Srebrnym Lustrem Arazille; może go swobodnie używać a lustro nie robi jej krzywdy.|Adaptacja kralotyczna|
|[Ucieczka Sióstr Światła](/rpg/inwazja/opowiesci/konspekty/161206-ucieczka-siostr-swiatla.html)|dostała Srebrne Lustro Arazille|Nicaretta|


## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|180321|poetka w domenie Arazille. Nieco znudzona i samotna. Z jakiegoś powodu Arazille jej nie Dotyka. Pomogła Marii - przeszmuglowała ją do Blakenbauerów wbrew Arazille.|[Porwanie pod nosem hipisów](/rpg/inwazja/opowiesci/konspekty/180321-porwanie-pod-nosem-hipisow.html)|10/12/08|10/12/09|[Adaptacja kralotyczna](/rpg/inwazja/opowiesci/konspekty/kampania-adaptacja-kralotyczna.html)|
|161206|uzyskała Srebrne Lustro z pomocą Sióstr Światła. Bezpiecznie wycofała się na Śląsk.|[Ucieczka Sióstr Światła](/rpg/inwazja/opowiesci/konspekty/161206-ucieczka-siostr-swiatla.html)|10/07/25|10/07/26|[Nicaretta](/rpg/inwazja/opowiesci/konspekty/kampania-nicaretta.html)|
|161129|poetka i turystka poszukująca Srebrnego Lustra Bez Odbicia. Świetna w odgrywaniu turystki. Zaprzyjaźniła się z nią Kora Panik.|[Ewakuacja Natalii, wejście maga](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html)|10/07/23|10/07/24|[Nicaretta](/rpg/inwazja/opowiesci/konspekty/kampania-nicaretta.html)|
|150104|która może być kluczem do wyleczenia policjanta z lustrzanej przypadłości. Choć nie wiadomo czemu.|[Terminus-defiler, kapłan Arazille](/rpg/inwazja/opowiesci/konspekty/150104-terminus-defiler-kaplan-arazille.html)|10/02/17|10/02/18|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|
|140604|Sophistia, która zniknęła i została schowana przez Gustawa Siedeła. Zraniona i odrzucona przez Marcelina (jak czuje), znowu zniknęła.|[Patriarcha Blakenbauer](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|10/01/13|10/01/14|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|
|140401|tu: Blakenbauer :P. Zrobiła obelżywe graffiti na murach posterunku policji, więc Hektor zostawił ją w więzieniu; przez działania Hektora Marcelin ją opuścił.|[Mojra, Moriath](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|10/01/11|10/01/12|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|
|140121|Sophistia, hipiska i (nie wojująca) anarchistka, niezła poetka która nabawiła się odporności na magię mentalną przez Skażenie, narkotyki i jad Dromopod Iserat.|[Zniknięcie Sophistii](/rpg/inwazja/opowiesci/konspekty/140121-znikniecie-sophistii.html)|10/01/09|10/01/10|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|
|140320|Sophistia, która bezczelnie podsłuchuje pod drzwiami Hektora w sprawie Siedeła i ma kilka ciekawych pomysłów jak z tego wybrnąć.|[Sprawa magicznych samochodów](/rpg/inwazja/opowiesci/konspekty/140320-sprawa-magicznych-samochodow.html)|10/01/07|10/01/08|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|
|140114|Sophistia, kochanka Marcelina oraz ku swej rozpaczy Borysa (pod wpływem złych czarów). |[Zaginiony członek](/rpg/inwazja/opowiesci/konspekty/140114-zaginiony-czlonek.html)|10/01/07|10/01/08|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|
|140227|Sophistia, przedmiot a nie podmiot misji; jej życie jest sprzężone z Marcelinem i Dariusz Kopyto ją zabija.|[Sophistia x Marcelin](/rpg/inwazja/opowiesci/konspekty/140227-sophistia-x-marcelin.html)|10/01/05|10/01/06|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|
|140213|Sophistia, która próbuje uratować Marcelina i dzwoni do Hektora; gdy ten ją ignoruje, dzwoni do Andrei.|[Pułapka na Edwina](/rpg/inwazja/opowiesci/konspekty/140213-pulapka-na-edwina.html)|10/01/01|10/01/02|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|7|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140121](/rpg/inwazja/opowiesci/konspekty/140121-znikniecie-sophistii.html), [140320](/rpg/inwazja/opowiesci/konspekty/140320-sprawa-magicznych-samochodow.html), [140114](/rpg/inwazja/opowiesci/konspekty/140114-zaginiony-czlonek.html), [140227](/rpg/inwazja/opowiesci/konspekty/140227-sophistia-x-marcelin.html), [140213](/rpg/inwazja/opowiesci/konspekty/140213-pulapka-na-edwina.html)|
|[Andrea Wilgacz](/rpg/inwazja/opowiesci/karty-postaci/1709-andrea-wilgacz.html)|7|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140121](/rpg/inwazja/opowiesci/konspekty/140121-znikniecie-sophistii.html), [140320](/rpg/inwazja/opowiesci/konspekty/140320-sprawa-magicznych-samochodow.html), [140114](/rpg/inwazja/opowiesci/konspekty/140114-zaginiony-czlonek.html), [140227](/rpg/inwazja/opowiesci/konspekty/140227-sophistia-x-marcelin.html), [140213](/rpg/inwazja/opowiesci/konspekty/140213-pulapka-na-edwina.html)|
|[Marcelin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-marcelin-blakenbauer.html)|6|[140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140121](/rpg/inwazja/opowiesci/konspekty/140121-znikniecie-sophistii.html), [140320](/rpg/inwazja/opowiesci/konspekty/140320-sprawa-magicznych-samochodow.html), [140114](/rpg/inwazja/opowiesci/konspekty/140114-zaginiony-czlonek.html), [140227](/rpg/inwazja/opowiesci/konspekty/140227-sophistia-x-marcelin.html), [140213](/rpg/inwazja/opowiesci/konspekty/140213-pulapka-na-edwina.html)|
|[Waldemar Zupaczka](/rpg/inwazja/opowiesci/karty-postaci/9999-waldemar-zupaczka.html)|5|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140121](/rpg/inwazja/opowiesci/konspekty/140121-znikniecie-sophistii.html), [140114](/rpg/inwazja/opowiesci/konspekty/140114-zaginiony-czlonek.html), [140227](/rpg/inwazja/opowiesci/konspekty/140227-sophistia-x-marcelin.html)|
|[Wacław Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-waclaw-zajcew.html)|4|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140227](/rpg/inwazja/opowiesci/konspekty/140227-sophistia-x-marcelin.html), [140213](/rpg/inwazja/opowiesci/konspekty/140213-pulapka-na-edwina.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|4|[180321](/rpg/inwazja/opowiesci/konspekty/180321-porwanie-pod-nosem-hipisow.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140227](/rpg/inwazja/opowiesci/konspekty/140227-sophistia-x-marcelin.html), [140213](/rpg/inwazja/opowiesci/konspekty/140213-pulapka-na-edwina.html)|
|[Marian Agrest](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-agrest.html)|3|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140213](/rpg/inwazja/opowiesci/konspekty/140213-pulapka-na-edwina.html)|
|[Jan Szczupak](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-szczupak.html)|3|[140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140114](/rpg/inwazja/opowiesci/konspekty/140114-zaginiony-czlonek.html), [140213](/rpg/inwazja/opowiesci/konspekty/140213-pulapka-na-edwina.html)|
|[Ika](/rpg/inwazja/opowiesci/karty-postaci/9999-ika.html)|3|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html), [140320](/rpg/inwazja/opowiesci/konspekty/140320-sprawa-magicznych-samochodow.html), [140227](/rpg/inwazja/opowiesci/konspekty/140227-sophistia-x-marcelin.html)|
|[Estera Piryt](/rpg/inwazja/opowiesci/karty-postaci/9999-estera-piryt.html)|3|[140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140121](/rpg/inwazja/opowiesci/konspekty/140121-znikniecie-sophistii.html), [140213](/rpg/inwazja/opowiesci/konspekty/140213-pulapka-na-edwina.html)|
|[Dariusz Kopyto](/rpg/inwazja/opowiesci/karty-postaci/9999-dariusz-kopyto.html)|3|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html), [140121](/rpg/inwazja/opowiesci/konspekty/140121-znikniecie-sophistii.html), [140227](/rpg/inwazja/opowiesci/konspekty/140227-sophistia-x-marcelin.html)|
|[Teresa Żyraf](/rpg/inwazja/opowiesci/karty-postaci/9999-teresa-zyraf.html)|2|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Sebastian Tecznia](/rpg/inwazja/opowiesci/karty-postaci/9999-sebastian-tecznia.html)|2|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Otton Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-otton-blakenbauer.html)|2|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Mojra](/rpg/inwazja/opowiesci/karty-postaci/9999-mojra.html)|2|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Marzena Dorszaj](/rpg/inwazja/opowiesci/karty-postaci/1709-marzena-dorszaj.html)|2|[161206](/rpg/inwazja/opowiesci/konspekty/161206-ucieczka-siostr-swiatla.html), [161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html)|
|[Margaret Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-margaret-blakenbauer.html)|2|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Krystalia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-krystalia-diakon.html)|2|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Karolina Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-karolina-maus.html)|2|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Hubert Kaldwor](/rpg/inwazja/opowiesci/karty-postaci/9999-hubert-kaldwor.html)|2|[161206](/rpg/inwazja/opowiesci/konspekty/161206-ucieczka-siostr-swiatla.html), [161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html)|
|[Henryk Siwiecki](/rpg/inwazja/opowiesci/karty-postaci/1709-henryk-siwiecki.html)|2|[161206](/rpg/inwazja/opowiesci/konspekty/161206-ucieczka-siostr-swiatla.html), [161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html)|
|[Gustaw Siedeł](/rpg/inwazja/opowiesci/karty-postaci/9999-gustaw-siedel.html)|2|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html), [140320](/rpg/inwazja/opowiesci/konspekty/140320-sprawa-magicznych-samochodow.html)|
|[Grzegorz Czerwiec](/rpg/inwazja/opowiesci/karty-postaci/9999-grzegorz-czerwiec.html)|2|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Borys Kumin](/rpg/inwazja/opowiesci/karty-postaci/1709-borys-kumin.html)|2|[140114](/rpg/inwazja/opowiesci/konspekty/140114-zaginiony-czlonek.html), [140227](/rpg/inwazja/opowiesci/konspekty/140227-sophistia-x-marcelin.html)|
|[Andrzej Klepiczek](/rpg/inwazja/opowiesci/karty-postaci/9999-andrzej-klepiczek.html)|2|[161206](/rpg/inwazja/opowiesci/konspekty/161206-ucieczka-siostr-swiatla.html), [161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html)|
|[Yakim Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-yakim-zajcew.html)|1|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Urszula Kram](/rpg/inwazja/opowiesci/karty-postaci/9999-urszula-kram.html)|1|[161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html)|
|[Tadeusz Czerwiecki](/rpg/inwazja/opowiesci/karty-postaci/9999-tadeusz-czerwiecki.html)|1|[140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Siergiej Wdenkow](/rpg/inwazja/opowiesci/karty-postaci/1803-siergiej-wdenkow.html)|1|[180321](/rpg/inwazja/opowiesci/konspekty/180321-porwanie-pod-nosem-hipisow.html)|
|[Sandra Stryjek](/rpg/inwazja/opowiesci/karty-postaci/1709-sandra-stryjek.html)|1|[150104](/rpg/inwazja/opowiesci/konspekty/150104-terminus-defiler-kaplan-arazille.html)|
|[Rufus Eter](/rpg/inwazja/opowiesci/karty-postaci/9999-rufus-eter.html)|1|[140121](/rpg/inwazja/opowiesci/konspekty/140121-znikniecie-sophistii.html)|
|[Renata Krzem](/rpg/inwazja/opowiesci/karty-postaci/9999-renata-krzem.html)|1|[161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html)|
|[Rebeka Piryt](/rpg/inwazja/opowiesci/karty-postaci/9999-rebeka-piryt.html)|1|[140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Quasar](/rpg/inwazja/opowiesci/karty-postaci/9999-quasar.html)|1|[140320](/rpg/inwazja/opowiesci/konspekty/140320-sprawa-magicznych-samochodow.html)|
|[Paweł Grzęda](/rpg/inwazja/opowiesci/karty-postaci/9999-pawel-grzeda.html)|1|[140114](/rpg/inwazja/opowiesci/konspekty/140114-zaginiony-czlonek.html)|
|[Netheria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-netheria-diakon.html)|1|[150104](/rpg/inwazja/opowiesci/konspekty/150104-terminus-defiler-kaplan-arazille.html)|
|[Natalia Kaldwor](/rpg/inwazja/opowiesci/karty-postaci/9999-natalia-kaldwor.html)|1|[161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html)|
|[Mirabelka Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-mirabelka-diakon.html)|1|[150104](/rpg/inwazja/opowiesci/konspekty/150104-terminus-defiler-kaplan-arazille.html)|
|[Maria Przysiadek](/rpg/inwazja/opowiesci/karty-postaci/9999-maria-przysiadek.html)|1|[180321](/rpg/inwazja/opowiesci/konspekty/180321-porwanie-pod-nosem-hipisow.html)|
|[Maja Liszka](/rpg/inwazja/opowiesci/karty-postaci/1709-maja-liszka.html)|1|[161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html)|
|[Maja Kos](/rpg/inwazja/opowiesci/karty-postaci/9999-maja-kos.html)|1|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Luksja Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-luksja-diakon.html)|1|[150104](/rpg/inwazja/opowiesci/konspekty/150104-terminus-defiler-kaplan-arazille.html)|
|[Kora Panik](/rpg/inwazja/opowiesci/karty-postaci/9999-kora-panik.html)|1|[161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html)|
|[Kasia Nowak](/rpg/inwazja/opowiesci/karty-postaci/1803-kasia-nowak.html)|1|[150104](/rpg/inwazja/opowiesci/konspekty/150104-terminus-defiler-kaplan-arazille.html)|
|[Karol Poczciwiec](/rpg/inwazja/opowiesci/karty-postaci/9999-karol-poczciwiec.html)|1|[140114](/rpg/inwazja/opowiesci/konspekty/140114-zaginiony-czlonek.html)|
|[Irina Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-irina-zajcew.html)|1|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Ilarion Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-ilarion-zajcew.html)|1|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Hipolit Mraczon](/rpg/inwazja/opowiesci/karty-postaci/9999-hipolit-mraczon.html)|1|[161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html)|
|[Herbert Zioło](/rpg/inwazja/opowiesci/karty-postaci/1709-herbert-ziolo.html)|1|[150104](/rpg/inwazja/opowiesci/konspekty/150104-terminus-defiler-kaplan-arazille.html)|
|[Grzegorz Zachradnik](/rpg/inwazja/opowiesci/karty-postaci/9999-grzegorz-zachradnik.html)|1|[180321](/rpg/inwazja/opowiesci/konspekty/180321-porwanie-pod-nosem-hipisow.html)|
|[Gabriel Newa](/rpg/inwazja/opowiesci/karty-postaci/9999-gabriel-newa.html)|1|[150104](/rpg/inwazja/opowiesci/konspekty/150104-terminus-defiler-kaplan-arazille.html)|
|[Feliks Hanson](/rpg/inwazja/opowiesci/karty-postaci/9999-feliks-hanson.html)|1|[150104](/rpg/inwazja/opowiesci/konspekty/150104-terminus-defiler-kaplan-arazille.html)|
|[Feliks Bozur](/rpg/inwazja/opowiesci/karty-postaci/9999-feliks-bozur.html)|1|[150104](/rpg/inwazja/opowiesci/konspekty/150104-terminus-defiler-kaplan-arazille.html)|
|[Felicja Wydech](/rpg/inwazja/opowiesci/karty-postaci/9999-felicja-wydech.html)|1|[161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html)|
|[Elżbieta Niemoc](/rpg/inwazja/opowiesci/karty-postaci/9999-elzbieta-niemoc.html)|1|[140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Diana Larent](/rpg/inwazja/opowiesci/karty-postaci/9999-diana-larent.html)|1|[140121](/rpg/inwazja/opowiesci/konspekty/140121-znikniecie-sophistii.html)|
|[Damian Bródka](/rpg/inwazja/opowiesci/karty-postaci/9999-damian-brodka.html)|1|[140320](/rpg/inwazja/opowiesci/konspekty/140320-sprawa-magicznych-samochodow.html)|
|[Bożena Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-bozena-zajcew.html)|1|[140213](/rpg/inwazja/opowiesci/konspekty/140213-pulapka-na-edwina.html)|
|[Bolesław Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-boleslaw-bankierz.html)|1|[140114](/rpg/inwazja/opowiesci/konspekty/140114-zaginiony-czlonek.html)|
|[August Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-august-bankierz.html)|1|[150104](/rpg/inwazja/opowiesci/konspekty/150104-terminus-defiler-kaplan-arazille.html)|
|[Arazille](/rpg/inwazja/opowiesci/karty-postaci/9999-arazille.html)|1|[150104](/rpg/inwazja/opowiesci/konspekty/150104-terminus-defiler-kaplan-arazille.html)|
|[Anna Osiaczek](/rpg/inwazja/opowiesci/karty-postaci/9999-anna-osiaczek.html)|1|[161206](/rpg/inwazja/opowiesci/konspekty/161206-ucieczka-siostr-swiatla.html)|
|[Alina Bednarz](/rpg/inwazja/opowiesci/karty-postaci/1803-alina-bednarz.html)|1|[180321](/rpg/inwazja/opowiesci/konspekty/180321-porwanie-pod-nosem-hipisow.html)|
|[Alfred Kukułka](/rpg/inwazja/opowiesci/karty-postaci/9999-alfred-kukulka.html)|1|[140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Adela Klepiczek](/rpg/inwazja/opowiesci/karty-postaci/9999-adela-klepiczek.html)|1|[161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html)|

---
layout: inwazja-karta-postaci
categories: profile
title: "Ksenia Armon"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170718|agresywna prawniczka, nakręcająca efemerydę i opierniczająca wszystkich. Łącznie z własnym nagim szefem. Chciała pobić Krystalię. Symbiont koralowca. Straciła pamięć.|[Umarł z miłości](/rpg/inwazja/opowiesci/konspekty/170718-umarl-z-milosci.html)|10/10/26|10/10/28|[Adaptacja kralotyczna](/rpg/inwazja/opowiesci/konspekty/kampania-adaptacja-kralotyczna.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Zenon Stecki](/rpg/inwazja/opowiesci/karty-postaci/9999-zenon-stecki.html)|1|[170718](/rpg/inwazja/opowiesci/konspekty/170718-umarl-z-milosci.html)|
|[Tony Armadillo](/rpg/inwazja/opowiesci/karty-postaci/9999-tony-armadillo.html)|1|[170718](/rpg/inwazja/opowiesci/konspekty/170718-umarl-z-milosci.html)|
|[Tomasz Klink](/rpg/inwazja/opowiesci/karty-postaci/9999-tomasz-klink.html)|1|[170718](/rpg/inwazja/opowiesci/konspekty/170718-umarl-z-milosci.html)|
|[Stefan Piżuch](/rpg/inwazja/opowiesci/karty-postaci/9999-stefan-pizuch.html)|1|[170718](/rpg/inwazja/opowiesci/konspekty/170718-umarl-z-milosci.html)|
|[Krzysztof Brakujowiec](/rpg/inwazja/opowiesci/karty-postaci/9999-krzysztof-brakujowiec.html)|1|[170718](/rpg/inwazja/opowiesci/konspekty/170718-umarl-z-milosci.html)|
|[Krystalia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-krystalia-diakon.html)|1|[170718](/rpg/inwazja/opowiesci/konspekty/170718-umarl-z-milosci.html)|
|[Katarzyna Marszał](/rpg/inwazja/opowiesci/karty-postaci/9999-katarzyna-marszal.html)|1|[170718](/rpg/inwazja/opowiesci/konspekty/170718-umarl-z-milosci.html)|
|[Jolanta Karbon](/rpg/inwazja/opowiesci/karty-postaci/9999-jolanta-karbon.html)|1|[170718](/rpg/inwazja/opowiesci/konspekty/170718-umarl-z-milosci.html)|
|[Jadwiga Opaszczyk](/rpg/inwazja/opowiesci/karty-postaci/9999-jadwiga-opaszczyk.html)|1|[170718](/rpg/inwazja/opowiesci/konspekty/170718-umarl-z-milosci.html)|

---
layout: inwazja-karta-postaci
categories: profile
title: "Wiktor Diakon"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|141006|najemnik trzymający z niezbyt normalną ciotką|[Klinika 'Słonecznik'](/rpg/inwazja/opowiesci/konspekty/141006-klinika-slonecznik.html)|10/01/05|10/01/06|[Blakenbauerowie x Skorpion](/rpg/inwazja/opowiesci/konspekty/kampania-blakenbauerowie-x-skorpion.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Tadeusz Baran](/rpg/inwazja/opowiesci/karty-postaci/1709-tadeusz-baran.html)|1|[141006](/rpg/inwazja/opowiesci/konspekty/141006-klinika-slonecznik.html)|
|[Onufry Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-onufry-maus.html)|1|[141006](/rpg/inwazja/opowiesci/konspekty/141006-klinika-slonecznik.html)|
|[Henryk Waciak](/rpg/inwazja/opowiesci/karty-postaci/9999-henryk-waciak.html)|1|[141006](/rpg/inwazja/opowiesci/konspekty/141006-klinika-slonecznik.html)|
|[Estera Piryt](/rpg/inwazja/opowiesci/karty-postaci/9999-estera-piryt.html)|1|[141006](/rpg/inwazja/opowiesci/konspekty/141006-klinika-slonecznik.html)|
|[Esme Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-esme-myszeczka.html)|1|[141006](/rpg/inwazja/opowiesci/konspekty/141006-klinika-slonecznik.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|1|[141006](/rpg/inwazja/opowiesci/konspekty/141006-klinika-slonecznik.html)|
|[Dalia Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-dalia-weiner.html)|1|[141006](/rpg/inwazja/opowiesci/konspekty/141006-klinika-slonecznik.html)|
|[Antoni Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/9999-antoni-myszeczka.html)|1|[141006](/rpg/inwazja/opowiesci/konspekty/141006-klinika-slonecznik.html)|
|[Anabela Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-anabela-diakon.html)|1|[141006](/rpg/inwazja/opowiesci/konspekty/141006-klinika-slonecznik.html)|

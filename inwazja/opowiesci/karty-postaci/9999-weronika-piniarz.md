---
layout: inwazja-karta-postaci
categories: profile
title: "Weronika Piniarz"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|140819|młoda adeptka Kopalin-Shaolin którą był (lecz nie jest) zainteresowany Hektor.|[Marcelin w klasztorze!](/rpg/inwazja/opowiesci/konspekty/140819-marcelin-w-klasztorze.html)|10/01/01|10/01/02|[Nie umieszczone, Anulowane](/rpg/inwazja/opowiesci/konspekty/kampania-anulowane.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Smok](/rpg/inwazja/opowiesci/karty-postaci/9999-smok.html)|1|[140819](/rpg/inwazja/opowiesci/konspekty/140819-marcelin-w-klasztorze.html)|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|1|[140819](/rpg/inwazja/opowiesci/konspekty/140819-marcelin-w-klasztorze.html)|
|[Margaret Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-margaret-blakenbauer.html)|1|[140819](/rpg/inwazja/opowiesci/konspekty/140819-marcelin-w-klasztorze.html)|
|[Marcelin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-marcelin-blakenbauer.html)|1|[140819](/rpg/inwazja/opowiesci/konspekty/140819-marcelin-w-klasztorze.html)|
|[Kinga Melit](/rpg/inwazja/opowiesci/karty-postaci/9999-kinga-melit.html)|1|[140819](/rpg/inwazja/opowiesci/konspekty/140819-marcelin-w-klasztorze.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|1|[140819](/rpg/inwazja/opowiesci/konspekty/140819-marcelin-w-klasztorze.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|1|[140819](/rpg/inwazja/opowiesci/konspekty/140819-marcelin-w-klasztorze.html)|

---
layout: inwazja-karta-postaci
categories: profile
factions: "Nieznany"
type: "NPC"
owner: "arleta"
title: "Genowefa Huppert"
---
# {{ page.title }}

## Postać

### Motywacje

#### Kategorie i Aspekty

| Kategoria         | Aspekty                           |
|-------------------|-----------------------------------|
| Indywidualne      | rozwój; kontrola                  |
| Społeczne         | kojenie; inspiracja; pomoc        |
| Wartości          | altruizm                          |

#### Szczególnie

| Co chce by się działo?                                                      | Co na pewno ma się NIE dziać? Co jest sprzeczne?                                                  |
|-----------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------|
| zawsze dowiedz się, co się dzieje - nie wchodź nieprzygotowana              | wchodź w sytuację spontanicznie, daj się czasem zaskoczyć  |
| zadbaj, by rozwiązywać problemy innych tak, by na pewno czuli się dobrze    | pomóż innym tylko w zakresie tego, o co proszą, pozwól im odejść bez rozwiązania faktycznego problemu  |
| bądź ciekawska, zawsze jest coś, czego możesz się nauczyć i co się przyda   | zaakceptuj zatrzymanie się w miejscu, nie trzeba tak gnać |
|                                                                             | nie bądź nigdy źródłem krzywdy i cierpienia innych |

### Umiejętności

#### Kategorie i Aspekty

| Kategoria         | Aspekty                           |
|-------------------|-----------------------------------|
| znachorka         | mikstury; leczenie; zwierzenia |
| wróżbitka         | plotki; obserwacja |
| agitatorka        | kontrola tłumu |

#### Manewry

| Jakie działania wykonuje?                                                              | Czym osiąga sukces?                                                          |
|----------------------------------------------------------------------------------------|------------------------------------------------------------------------------|
| wydobywanie informacji | zwierzenia |
| zorientowanie się w sytuacji | obserwacja otoczenia |
| regeneracja | umiejętności lekarskie |
| manipulacja |  |

### Silne i słabe strony:

#### Kategorie i Aspekty

| Kategoria            | Aspekty                        |
|----------------------|--------------------------------|
|  |  |

#### Manewry

| Co jest wzmocnione                                        | Kosztem czego                                                 |
|-----------------------------------------------------------|---------------------------------------------------------------|
|  |  |

### Szkoły magiczne

#### Kategorie i Aspekty

| Kategoria           | Aspekty                                                                             |
|---------------------|-------------------------------------------------------------------------------------|
| Biomancja           |  |
| Magia mentalna      |  |
| Magia zmysłów       |  |

#### Manewry

| Jakie działania wykonuje?                                                 | Czym osiąga sukces?                                                               |
|---------------------------------------------------------------------------|-----------------------------------------------------------------------------------|
|  |  |

## Zasoby i otoczenie

#### Powiązane frakcje

{{ page.factions }}

#### Kategorie i Aspekty

| Kategoria                          | Aspekty                                                              |
|------------------------------------|----------------------------------------------------------------------|
|  |  |

#### Manewry

| Jakie działania wspierane?                                                      | Czym osiąga sukces?                                           |
|---------------------------------------------------------------------------------|---------------------------------------------------------------|
|  |  |

## Opis

### Koncept

Nieznane

### Ogólnie

### Motywacje

### Działanie

### Specjalne

### Magia

### Otoczenie

### Mapa kreacji

brak

### Motto

""

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Śledziem w depresję](/rpg/inwazja/opowiesci/konspekty/180425-sledziem-w-depresje.html)|Melinda Zajcew staje się jej kontaktem. Dziewczyna jej ufa i ją lubi.|Dusza Czapkowika|
|[Śledziem w depresję](/rpg/inwazja/opowiesci/konspekty/180425-sledziem-w-depresje.html)|ma dostęp do pożerających depresję śledzi (hodowla domowa)|Dusza Czapkowika|
|[Śledziem w depresję](/rpg/inwazja/opowiesci/konspekty/180425-sledziem-w-depresje.html)|wskutek dziwnych interakcji emocjonalnych co jakiś czas musi zajrzeć w okolice Zaćmionego Serca, aby się upewnić, że wszystko nadal w porządku|Dusza Czapkowika|
|[Śledziem w depresję](/rpg/inwazja/opowiesci/konspekty/180425-sledziem-w-depresje.html)|śledzie potrafią się teleportować. Wszelka próba sprzedaży, kradzieży itp kończy się ich powrotem.|Dusza Czapkowika|
|[Śledziem w depresję](/rpg/inwazja/opowiesci/konspekty/180425-sledziem-w-depresje.html)|śledzie trzeba karmić. Depresją. Smutkiem.|Dusza Czapkowika|
|[Jaszczur Love Story](/rpg/inwazja/opowiesci/konspekty/180421-jaszczur-love-story.html)|tak bardzo jej ufa Alfred i Kamila, że dostała informacje jak wejść do jaszczurzego świata pod ziemią. Ma dostęp do części dziwnych jaszczurzych zasobów.|Dusza Czapkowika|
|[Jaszczury rządzą miastem](/rpg/inwazja/opowiesci/konspekty/180419-jaszczury-rzadza-miastem.html)|ma chody w Czapkowiku, u burmistrz Kamili Woreczek (która jest jaszczurem) jako przyjazny mag.|Dusza Czapkowika|


## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|180619|która została honorowym gościem Powiewu Świeżości|[Śledzie na Fazie](/rpg/inwazja/opowiesci/konspekty/180619-sledzie-na-fazie.html)|10/12/02|10/12/04|[Adaptacja kralotyczna](/rpg/inwazja/opowiesci/konspekty/kampania-adaptacja-kralotyczna.html)|
|180425|przyjazna i pomocna czarodziejka, ratująca Melindę Zajcew przed nią samą i śledzi śledzie|[Śledziem w depresję](/rpg/inwazja/opowiesci/konspekty/180425-sledziem-w-depresje.html)|10/11/28|10/11/30|[Dusza Czapkowika](/rpg/inwazja/opowiesci/konspekty/kampania-dusza-czapkowika.html)|
|180421|jedyny mag któremu ufa Kamila; wyleczyła umierającą Kamilę, usunęła problem z mimikiem i naprawiła ekran Alfreda. Też: doszła do love story!|[Jaszczur Love Story](/rpg/inwazja/opowiesci/konspekty/180421-jaszczur-love-story.html)|10/11/24|10/11/26|[Dusza Czapkowika](/rpg/inwazja/opowiesci/konspekty/kampania-dusza-czapkowika.html)|
|180419|społeczno-polityczna jednostka lecznicza. Przekonała burmistrz (która była jaszczurem w przebraniu) do swojego planu integracji świata ludzi i jaszczurów. Ma dobre serce.|[Jaszczury rządzą miastem](/rpg/inwazja/opowiesci/konspekty/180419-jaszczury-rzadza-miastem.html)|10/11/17|10/11/19|[Dusza Czapkowika](/rpg/inwazja/opowiesci/konspekty/kampania-dusza-czapkowika.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Kamila Woreczek](/rpg/inwazja/opowiesci/karty-postaci/9999-kamila-woreczek.html)|2|[180421](/rpg/inwazja/opowiesci/konspekty/180421-jaszczur-love-story.html), [180419](/rpg/inwazja/opowiesci/konspekty/180419-jaszczury-rzadza-miastem.html)|
|[Alfred Werner](/rpg/inwazja/opowiesci/karty-postaci/9999-alfred-werner.html)|2|[180421](/rpg/inwazja/opowiesci/konspekty/180421-jaszczur-love-story.html), [180419](/rpg/inwazja/opowiesci/konspekty/180419-jaszczury-rzadza-miastem.html)|
|[Melinda Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1803-melinda-zajcew.html)|1|[180425](/rpg/inwazja/opowiesci/konspekty/180425-sledziem-w-depresje.html)|
|[Mateusz Podgardle](/rpg/inwazja/opowiesci/karty-postaci/9999-mateusz-podgardle.html)|1|[180419](/rpg/inwazja/opowiesci/konspekty/180419-jaszczury-rzadza-miastem.html)|
|[Maciek Drzeworóz](/rpg/inwazja/opowiesci/karty-postaci/9999-maciek-drzeworoz.html)|1|[180419](/rpg/inwazja/opowiesci/konspekty/180419-jaszczury-rzadza-miastem.html)|
|[Kora Panik](/rpg/inwazja/opowiesci/karty-postaci/9999-kora-panik.html)|1|[180419](/rpg/inwazja/opowiesci/konspekty/180419-jaszczury-rzadza-miastem.html)|
|[Kasia Krabowska](/rpg/inwazja/opowiesci/karty-postaci/9999-kasia-krabowska.html)|1|[180421](/rpg/inwazja/opowiesci/konspekty/180421-jaszczur-love-story.html)|
|[Estrella Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-estrella-diakon.html)|1|[180425](/rpg/inwazja/opowiesci/konspekty/180425-sledziem-w-depresje.html)|
|[Daria Rudas](/rpg/inwazja/opowiesci/karty-postaci/1803-daria-rudas.html)|1|[180421](/rpg/inwazja/opowiesci/konspekty/180421-jaszczur-love-story.html)|
|[Artur Wiążczak](/rpg/inwazja/opowiesci/karty-postaci/9999-artur-wiazczak.html)|1|[180421](/rpg/inwazja/opowiesci/konspekty/180421-jaszczur-love-story.html)|
|[Alojzy Bunnert](/rpg/inwazja/opowiesci/karty-postaci/9999-alojzy-bunnert.html)|1|[180419](/rpg/inwazja/opowiesci/konspekty/180419-jaszczury-rzadza-miastem.html)|
|[Aleksandra Kurządek](/rpg/inwazja/opowiesci/karty-postaci/9999-aleksandra-kurzadek.html)|1|[180419](/rpg/inwazja/opowiesci/konspekty/180419-jaszczury-rzadza-miastem.html)|

---
layout: inwazja-karta-postaci
categories: profile
title: "Kajetan Pyszak"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|150820|hipster-szuja, który w sekrecie działa przeciwko squatowi "Dante". Skompromitowany przez Wandę.|[Klemens w roli swatki](/rpg/inwazja/opowiesci/konspekty/150820-klemens-w-roli-swatki.html)|10/05/19|10/05/20|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Wanda Ketran](/rpg/inwazja/opowiesci/karty-postaci/1709-wanda-ketran.html)|1|[150820](/rpg/inwazja/opowiesci/konspekty/150820-klemens-w-roli-swatki.html)|
|[Tomasz Ormię](/rpg/inwazja/opowiesci/karty-postaci/9999-tomasz-ormie.html)|1|[150820](/rpg/inwazja/opowiesci/konspekty/150820-klemens-w-roli-swatki.html)|
|[Stanisław Pormien](/rpg/inwazja/opowiesci/karty-postaci/9999-stanislaw-pormien.html)|1|[150820](/rpg/inwazja/opowiesci/konspekty/150820-klemens-w-roli-swatki.html)|
|[Klemens X](/rpg/inwazja/opowiesci/karty-postaci/9999-klemens-x.html)|1|[150820](/rpg/inwazja/opowiesci/konspekty/150820-klemens-w-roli-swatki.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|1|[150820](/rpg/inwazja/opowiesci/konspekty/150820-klemens-w-roli-swatki.html)|
|[Dagmara Czeluść](/rpg/inwazja/opowiesci/karty-postaci/9999-dagmara-czelusc.html)|1|[150820](/rpg/inwazja/opowiesci/konspekty/150820-klemens-w-roli-swatki.html)|
|[Anna Góra](/rpg/inwazja/opowiesci/karty-postaci/9999-anna-gora.html)|1|[150820](/rpg/inwazja/opowiesci/konspekty/150820-klemens-w-roli-swatki.html)|
|[Alina Bednarz](/rpg/inwazja/opowiesci/karty-postaci/1803-alina-bednarz.html)|1|[150820](/rpg/inwazja/opowiesci/konspekty/150820-klemens-w-roli-swatki.html)|

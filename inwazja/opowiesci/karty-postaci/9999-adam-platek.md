---
layout: inwazja-karta-postaci
categories: profile
title: "Adam Płatek"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|150120|infomanta lubiący mieszać ludziom w fejsiku i wchodzić innym ludziom do głowy; o nieco niewyparzonym języku.|[Pierścień też zniknął](/rpg/inwazja/opowiesci/konspekty/150120-pierscien-tez-zniknal.html)|10/01/03|10/01/04|[Nie umieszczone, Anulowane](/rpg/inwazja/opowiesci/konspekty/kampania-anulowane.html)|
|141210|technomanta w podejrzanie dobrej komitywie z psem młodej dziewczyny.|[Złodzieje kielicha w akcji](/rpg/inwazja/opowiesci/konspekty/141210-zlodzieje-kielicha-w-akcji.html)|10/01/01|10/01/02|[Nie umieszczone, Anulowane](/rpg/inwazja/opowiesci/konspekty/kampania-anulowane.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Mariusz Błyszczyk](/rpg/inwazja/opowiesci/karty-postaci/9999-mariusz-blyszczyk.html)|2|[150120](/rpg/inwazja/opowiesci/konspekty/150120-pierscien-tez-zniknal.html), [141210](/rpg/inwazja/opowiesci/konspekty/141210-zlodzieje-kielicha-w-akcji.html)|
|[Archibald Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-archibald-bankierz.html)|2|[150120](/rpg/inwazja/opowiesci/konspekty/150120-pierscien-tez-zniknal.html), [141210](/rpg/inwazja/opowiesci/konspekty/141210-zlodzieje-kielicha-w-akcji.html)|
|[Rafał Kielich](/rpg/inwazja/opowiesci/karty-postaci/9999-rafal-kielich.html)|1|[141210](/rpg/inwazja/opowiesci/konspekty/141210-zlodzieje-kielicha-w-akcji.html)|
|[Olga Pierwiosnek](/rpg/inwazja/opowiesci/karty-postaci/9999-olga-pierwiosnek.html)|1|[150120](/rpg/inwazja/opowiesci/konspekty/150120-pierscien-tez-zniknal.html)|
|[Mirosław Cebula](/rpg/inwazja/opowiesci/karty-postaci/9999-miroslaw-cebula.html)|1|[150120](/rpg/inwazja/opowiesci/konspekty/150120-pierscien-tez-zniknal.html)|
|[Miranda Delf](/rpg/inwazja/opowiesci/karty-postaci/9999-miranda-delf.html)|1|[150120](/rpg/inwazja/opowiesci/konspekty/150120-pierscien-tez-zniknal.html)|
|[Marian Jogurt](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-jogurt.html)|1|[150120](/rpg/inwazja/opowiesci/konspekty/150120-pierscien-tez-zniknal.html)|
|[Juliusz Jubilat](/rpg/inwazja/opowiesci/karty-postaci/9999-juliusz-jubilat.html)|1|[141210](/rpg/inwazja/opowiesci/konspekty/141210-zlodzieje-kielicha-w-akcji.html)|
|[Janina Kielich](/rpg/inwazja/opowiesci/karty-postaci/9999-janina-kielich.html)|1|[141210](/rpg/inwazja/opowiesci/konspekty/141210-zlodzieje-kielicha-w-akcji.html)|
|[Janek Łobuziak](/rpg/inwazja/opowiesci/karty-postaci/9999-janek-lobuziak.html)|1|[141210](/rpg/inwazja/opowiesci/konspekty/141210-zlodzieje-kielicha-w-akcji.html)|
|[Anna Patyczek](/rpg/inwazja/opowiesci/karty-postaci/9999-anna-patyczek.html)|1|[141210](/rpg/inwazja/opowiesci/konspekty/141210-zlodzieje-kielicha-w-akcji.html)|
|[Aneta Patyczek](/rpg/inwazja/opowiesci/karty-postaci/9999-aneta-patyczek.html)|1|[150120](/rpg/inwazja/opowiesci/konspekty/150120-pierscien-tez-zniknal.html)|
|[Aleksandra Pudryk](/rpg/inwazja/opowiesci/karty-postaci/9999-aleksandra-pudryk.html)|1|[141210](/rpg/inwazja/opowiesci/konspekty/141210-zlodzieje-kielicha-w-akcji.html)|

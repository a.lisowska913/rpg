---
layout: inwazja-karta-postaci
categories: profile
title: "Alicja Weiner"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|160724|sympatyczna sekretarka Wiktora i krewna Diany, która nie ma pojęcia o prawdziwej naturze swojego szefa.|[Portal do EAM](/rpg/inwazja/opowiesci/konspekty/160724-portal-do-eam.html)|10/06/24|10/06/25|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Wojciech Szudek](/rpg/inwazja/opowiesci/karty-postaci/9999-wojciech-szudek.html)|1|[160724](/rpg/inwazja/opowiesci/konspekty/160724-portal-do-eam.html)|
|[Wiktor Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1709-wiktor-sowinski.html)|1|[160724](/rpg/inwazja/opowiesci/konspekty/160724-portal-do-eam.html)|
|[Tomasz Kuracz](/rpg/inwazja/opowiesci/karty-postaci/9999-tomasz-kuracz.html)|1|[160724](/rpg/inwazja/opowiesci/konspekty/160724-portal-do-eam.html)|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|1|[160724](/rpg/inwazja/opowiesci/konspekty/160724-portal-do-eam.html)|
|[Oktawian Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-oktawian-maus.html)|1|[160724](/rpg/inwazja/opowiesci/konspekty/160724-portal-do-eam.html)|
|[Judyta Karnisz](/rpg/inwazja/opowiesci/karty-postaci/9999-judyta-karnisz.html)|1|[160724](/rpg/inwazja/opowiesci/konspekty/160724-portal-do-eam.html)|
|[Diana Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-diana-weiner.html)|1|[160724](/rpg/inwazja/opowiesci/konspekty/160724-portal-do-eam.html)|
|[Dagmara Wyjątek](/rpg/inwazja/opowiesci/karty-postaci/1709-dagmara-wyjątek.html)|1|[160724](/rpg/inwazja/opowiesci/konspekty/160724-portal-do-eam.html)|
|[Bianka Stein](/rpg/inwazja/opowiesci/karty-postaci/1709-bianka-stein.html)|1|[160724](/rpg/inwazja/opowiesci/konspekty/160724-portal-do-eam.html)|

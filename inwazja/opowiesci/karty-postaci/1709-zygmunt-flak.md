---
layout: inwazja-karta-postaci
categories: profile
factions: "Niezrzeszeni"
type: "NPC"
title: "Zygmunt Flak"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **BÓL: Truthseeker**:
    * _Aspekty_: 
    * _Opis_: dopóki wszystko nie pasuje do siebie, nie ma spokoju
* **MET: Dominator**:
    * _Aspekty_: arogancja
    * _Opis_: *I* have the power! jestem lepszy niż inni

### Umiejętności

* **Patolog sądowy**:
    * _Aspekty_: badanie zwłok, noże
    * _Opis_: 
* **Myśliwy**: 
    * _Aspekty_: noże
    * _Opis_: 
* **Iluzjonista**:
    * _Aspekty_: aktor, maski, 
    * _Opis_: 
* **Blogger**:
    * _Aspekty_: 
    * _Opis_:
    
### Silne i słabe strony:

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

## Magia

**brak**

## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Zarobki

* ?

### Znam

* **okoliczny prokurator**:
    * _Aspekty_: 
    * _Opis_: 
* **okoliczna policja**:
    * _Aspekty_: 
    * _Opis_: 
* **Adam Giczołek**:
    * _Aspekty_: sklepikarz
    * _Opis_: u którego kupuję "te" rzeczy
* **mały krąg psychofanów 'roastu'**:
    * _Aspekty_: 
    * _Opis_: 
	
### Mam

* **narzędzia patologiczne**:
    * _Aspekty_: skalpel
    * _Opis_: 
* **laboratorium**:
    * _Aspekty_: 
    * _Opis_: 
* **bogaty**:
    * _Aspekty_: 
    * _Opis_: 

# Opis

perfekcjonistyczny patolog-racjonalista z Czeludzi

36letni patolog, z umiłowania (krojenie żab koło domu), w głowie samotnik, ale udaje normalnego człowieka. Ma żonę (i syna, ale syn jest żony a nie jego) i prowadzi z nią fałszywy tryb życia, tak jak z resztą świata. Co weekend wyjeżdża, podobno na ryby a naprawdę do lasu - jest myśliwym z zamiłowania. Jest perfekcjonistą wierzącym w "wyższy sens" - wszystko musi być na swoim miejscu, wszystko musi pasować.
Pragnie mieć władzę nad życiem i śmiercią - to go właśnie podnieca i bawi.
Jest samotnikiem - rozumie ludzi, ale ich nie czuje. "To tylko ludzie, nie umieją wejść na mój poziom". Chce wytłumaczyć jak w coś wierzą, pokazać im, że żyją w iluzji i ich żałosne marzenia to tylko dym i lustra.

### Koncept

Wysokopoziomowy koncept postaci, 1-2 zdania.

### Motto

"Tekst"

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|160908|patolog-sceptyk dążący do prawdy za wszelką cenę; pomógł Andromedzie dojść do prawdy o Emilii i z trudem uratował się przed jej mocami.|[Zabójczy spadek](/rpg/inwazja/opowiesci/konspekty/160908-zabojczy-spadek.html)|10/01/14|10/01/17|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Łukasz Czerwiącek](/rpg/inwazja/opowiesci/karty-postaci/9999-lukasz-czerwiacek.html)|1|[160908](/rpg/inwazja/opowiesci/konspekty/160908-zabojczy-spadek.html)|
|[Tadeusz Bogatek](/rpg/inwazja/opowiesci/karty-postaci/9999-tadeusz-bogatek.html)|1|[160908](/rpg/inwazja/opowiesci/konspekty/160908-zabojczy-spadek.html)|
|[Stefan Porieńko](/rpg/inwazja/opowiesci/karty-postaci/9999-stefan-porienko.html)|1|[160908](/rpg/inwazja/opowiesci/konspekty/160908-zabojczy-spadek.html)|
|[Rudolf Mikołaj](/rpg/inwazja/opowiesci/karty-postaci/9999-rudolf-mikolaj.html)|1|[160908](/rpg/inwazja/opowiesci/konspekty/160908-zabojczy-spadek.html)|
|[Kasia Nowak](/rpg/inwazja/opowiesci/karty-postaci/1803-kasia-nowak.html)|1|[160908](/rpg/inwazja/opowiesci/konspekty/160908-zabojczy-spadek.html)|
|[Geraldina Kurzymaś](/rpg/inwazja/opowiesci/karty-postaci/9999-geraldina-kurzymas.html)|1|[160908](/rpg/inwazja/opowiesci/konspekty/160908-zabojczy-spadek.html)|
|[Emilia Bogatek](/rpg/inwazja/opowiesci/karty-postaci/9999-emilia-bogatek.html)|1|[160908](/rpg/inwazja/opowiesci/konspekty/160908-zabojczy-spadek.html)|
|[Damazy Czekan](/rpg/inwazja/opowiesci/karty-postaci/9999-damazy-czekan.html)|1|[160908](/rpg/inwazja/opowiesci/konspekty/160908-zabojczy-spadek.html)|
|[Damazy Bogatek](/rpg/inwazja/opowiesci/karty-postaci/9999-damazy-bogatek.html)|1|[160908](/rpg/inwazja/opowiesci/konspekty/160908-zabojczy-spadek.html)|
|[Czesław Statyw](/rpg/inwazja/opowiesci/karty-postaci/9999-czeslaw-statyw.html)|1|[160908](/rpg/inwazja/opowiesci/konspekty/160908-zabojczy-spadek.html)|

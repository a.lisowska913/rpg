---
layout: inwazja-karta-postaci
categories: profile
title: "Dominik Marchewka"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|141216|bardzo uzdolniony student informatyki i elektroniki który zaginął (zginął).|[Zabili mu syna](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|10/01/11|10/01/12|[Blakenbauerowie x Skorpion](/rpg/inwazja/opowiesci/konspekty/kampania-blakenbauerowie-x-skorpion.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Sebastian Tecznia](/rpg/inwazja/opowiesci/karty-postaci/9999-sebastian-tecznia.html)|1|[141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|[Rebeka Piryt](/rpg/inwazja/opowiesci/karty-postaci/9999-rebeka-piryt.html)|1|[141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|[Przemysław Marchewka](/rpg/inwazja/opowiesci/karty-postaci/9999-przemyslaw-marchewka.html)|1|[141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|[Olga Miodownik](/rpg/inwazja/opowiesci/karty-postaci/1709-olga-miodownik.html)|1|[141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|[Mojra](/rpg/inwazja/opowiesci/karty-postaci/9999-mojra.html)|1|[141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|[Marcelin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-marcelin-blakenbauer.html)|1|[141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|[Luiza Wanta](/rpg/inwazja/opowiesci/karty-postaci/9999-luiza-wanta.html)|1|[141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|[Klemens X](/rpg/inwazja/opowiesci/karty-postaci/9999-klemens-x.html)|1|[141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|1|[141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|[Ewa Kroideł](/rpg/inwazja/opowiesci/karty-postaci/9999-ewa-kroidel.html)|1|[141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|[Brunon Czerpak](/rpg/inwazja/opowiesci/karty-postaci/9999-brunon-czerpak.html)|1|[141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|[Antoni Szczęśliwiec](/rpg/inwazja/opowiesci/karty-postaci/9999-antoni-szczesliwiec.html)|1|[141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|[Anna Kajak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kajak.html)|1|[141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|[Alina Bednarz](/rpg/inwazja/opowiesci/karty-postaci/1803-alina-bednarz.html)|1|[141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|[Adam Bożynów](/rpg/inwazja/opowiesci/karty-postaci/9999-adam-bozynow.html)|1|[141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|

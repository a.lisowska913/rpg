---
layout: inwazja-karta-postaci
categories: profile
title: "Janek Malczorek"
---
# {{ page.title }}

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Szept Mare Felix](/rpg/inwazja/opowiesci/konspekty/180327-szept-mare-felix.html)|Skażony przez Glarnohlagha mocą Melindy i Bójki; cichy agent Klasztoru Zrównania.|Adaptacja kralotyczna|


## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|180327|barman w "Kolcie" w Jodłowcu; spotkał się z Melindą i dostał mindwarp by stać się jej posłusznym niewolnikiem.|[Szept Mare Felix](/rpg/inwazja/opowiesci/konspekty/180327-szept-mare-felix.html)|10/12/08|10/12/10|[Adaptacja kralotyczna](/rpg/inwazja/opowiesci/konspekty/kampania-adaptacja-kralotyczna.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Whisperwind](/rpg/inwazja/opowiesci/karty-postaci/9999-whisperwind.html)|1|[180327](/rpg/inwazja/opowiesci/konspekty/180327-szept-mare-felix.html)|
|[Melinda Słonko](/rpg/inwazja/opowiesci/karty-postaci/1803-melinda-slonko.html)|1|[180327](/rpg/inwazja/opowiesci/konspekty/180327-szept-mare-felix.html)|
|[Lucjan Kowalkiewicz](/rpg/inwazja/opowiesci/karty-postaci/9999-lucjan-kowalkiewicz.html)|1|[180327](/rpg/inwazja/opowiesci/konspekty/180327-szept-mare-felix.html)|
|[Bójka Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-bojka-diakon.html)|1|[180327](/rpg/inwazja/opowiesci/konspekty/180327-szept-mare-felix.html)|

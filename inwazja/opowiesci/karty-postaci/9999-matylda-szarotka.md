---
layout: inwazja-karta-postaci
categories: profile
title: "Matylda Szarotka"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170207|czarodziejka chętna do pomocy ludziom; botanik, lekarz i mentalistka. Z zaskoczenia wyleczyła Ingę, destabilizując link z Zaskrońcem.|[Błękitny zaskroniec](/rpg/inwazja/opowiesci/konspekty/170207-blekitny-zaskroniec.html)|10/01/03|10/01/05|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Tadeusz Grżnik](/rpg/inwazja/opowiesci/karty-postaci/9999-tadeusz-grznik.html)|1|[170207](/rpg/inwazja/opowiesci/konspekty/170207-blekitny-zaskroniec.html)|
|[Michał Prosznik](/rpg/inwazja/opowiesci/karty-postaci/9999-michal-prosznik.html)|1|[170207](/rpg/inwazja/opowiesci/konspekty/170207-blekitny-zaskroniec.html)|
|[Kasia Nowak](/rpg/inwazja/opowiesci/karty-postaci/1803-kasia-nowak.html)|1|[170207](/rpg/inwazja/opowiesci/konspekty/170207-blekitny-zaskroniec.html)|
|[Inga Prosznik](/rpg/inwazja/opowiesci/karty-postaci/9999-inga-prosznik.html)|1|[170207](/rpg/inwazja/opowiesci/konspekty/170207-blekitny-zaskroniec.html)|
|[Franciszek Dromicz](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-dromicz.html)|1|[170207](/rpg/inwazja/opowiesci/konspekty/170207-blekitny-zaskroniec.html)|

---
layout: inwazja-karta-postaci
categories: profile
title: "Filip Czątko"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|150928|młody chłopak (16 lat), który przeleciał wiłę i się lekko uzależnił; min. przez Węzeł Emocjonalny.|[Zamtuz z jedną wiłą](/rpg/inwazja/opowiesci/konspekty/150928-zamtuz-z-jedna-wila.html)|10/05/31|10/06/01|[Ucieczka do Przodka](/rpg/inwazja/opowiesci/konspekty/kampania-ucieczka-do-przodka.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Ryszard Herman](/rpg/inwazja/opowiesci/karty-postaci/9999-ryszard-herman.html)|1|[150928](/rpg/inwazja/opowiesci/konspekty/150928-zamtuz-z-jedna-wila.html)|
|[Rafał Maciejak](/rpg/inwazja/opowiesci/karty-postaci/9999-rafal-maciejak.html)|1|[150928](/rpg/inwazja/opowiesci/konspekty/150928-zamtuz-z-jedna-wila.html)|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|1|[150928](/rpg/inwazja/opowiesci/konspekty/150928-zamtuz-z-jedna-wila.html)|
|[Mariusz Czyczyż](/rpg/inwazja/opowiesci/karty-postaci/9999-mariusz-czyczyz.html)|1|[150928](/rpg/inwazja/opowiesci/konspekty/150928-zamtuz-z-jedna-wila.html)|
|[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/1709-maria-newa.html)|1|[150928](/rpg/inwazja/opowiesci/konspekty/150928-zamtuz-z-jedna-wila.html)|
|[Gala Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-gala-zajcew.html)|1|[150928](/rpg/inwazja/opowiesci/konspekty/150928-zamtuz-z-jedna-wila.html)|

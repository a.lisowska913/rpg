---
layout: inwazja-karta-postaci
categories: profile
title: "Janina Muczarok"
---
# {{ page.title }}

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Senesgradzka kopia Pauliny](/rpg/inwazja/opowiesci/konspekty/171218-senesgradzka-kopia-pauliny.html)|w jakiejś formie istnieje jako byt wydzielony z Efemerydy; nadal chroni i pomaga schronisku dla nastolatków|Wizja Dukata|

## Plany

|Misja|Plan|Kampania|
|-----|------|------|
|[Senesgradzka kopia Pauliny](/rpg/inwazja/opowiesci/konspekty/171218-senesgradzka-kopia-pauliny.html)|W ramach swojej możliwości, w zakresie dostępu Efemerydy Senesgradzkiej, pomagać ludziom - leczyć, chronić. Zwłaszcza nastolatkom w kłopotach.|Wizja Dukata|

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|171218|ciężko chora lekarka, która zintegrowała się z efemerydą senesgradzką tworząc manifestację Pauliny. Przekształcona w viciniusa, jej fizyczna forma umarła na końcu sesji.|[Senesgradzka kopia Pauliny](/rpg/inwazja/opowiesci/konspekty/171218-senesgradzka-kopia-pauliny.html)|11/08/28|11/08/31|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|1|[171218](/rpg/inwazja/opowiesci/konspekty/171218-senesgradzka-kopia-pauliny.html)|
|[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/1709-maria-newa.html)|1|[171218](/rpg/inwazja/opowiesci/konspekty/171218-senesgradzka-kopia-pauliny.html)|
|[Kaja Maślaczek](/rpg/inwazja/opowiesci/karty-postaci/1709-kaja-maslaczek.html)|1|[171218](/rpg/inwazja/opowiesci/konspekty/171218-senesgradzka-kopia-pauliny.html)|
|[Efemeryda Senesgradzka](/rpg/inwazja/opowiesci/karty-postaci/9999-efemeryda-senesgradzka.html)|1|[171218](/rpg/inwazja/opowiesci/konspekty/171218-senesgradzka-kopia-pauliny.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|1|[171218](/rpg/inwazja/opowiesci/konspekty/171218-senesgradzka-kopia-pauliny.html)|

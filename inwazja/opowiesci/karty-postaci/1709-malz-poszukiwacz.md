---
layout: inwazja-karta-postaci
categories: profile
factions: "Niezrzeszeni"
type: "NPC"
title: "Małż Poszukiwacz"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **MET: Dowiedzieć się jak najwięcej o ludziach i magach**:
    * _Aspekty_: 
    * _Opis_: 
* **MRZ: Odnaleźć i uratować utracone małże**:
    * _Aspekty_: 
    * _Opis_: 

### Umiejętności

* **Dominacja**:
    * _Aspekty_: kontrola, wzbudzanie sympatii, perswazja, korzystanie z wiedzy rumaka, leczenie umysłów
    * _Opis_: 
* **Małżowa wiedza o ludziach i magach**: 
    * _Aspekty_: ukrywanie swojej obecności
    * _Opis_: 
* **Małż elektryczny**:
    * _Aspekty_: 
    * _Opis_: 
    
### Silne i słabe strony:

* **Małżowy link mentalny**:
    * _Aspekty_: podatny na uderzenia przez link, stały kontakt z innymi małżami
    * _Opis_: 
* **Małż**: 
    * _Aspekty_: słaby, powolny bez rumaka, mały, potężna wola
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

## Magia

### Szkoły magiczne

* **Magia elementalna**:
    * _Aspekty_: elektryczność
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

### Zaklęcia statyczne

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 

## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Zarobki

* ?

### Znam

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

### Mam

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

# Opis

krótki opis postaci, rzeczy, jakie warto pamiętać.

### Koncept

Wysokopoziomowy koncept postaci, 1-2 zdania.

### Motto

"Jesteśmy jednością"

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170725|małż. Skakał między ciałami przez dotyk. Z pomocą reszty zdominował maga, Skaził Jodłowiec i uciekł z ciałem maga.|[Krzywdzę, bo kocham](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|10/05/09|10/05/11|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Silgor Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-silgor-diakon.html)|1|[170725](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|
|[Małż Mateusz](/rpg/inwazja/opowiesci/karty-postaci/1709-malz-mateusz.html)|1|[170725](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|
|[Mateusz Ackmann](/rpg/inwazja/opowiesci/karty-postaci/1709-mateusz-ackmann.html)|1|[170725](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|
|[Henryk Gwizdon](/rpg/inwazja/opowiesci/karty-postaci/9999-henryk-gwizdon.html)|1|[170725](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|
|[Fryderyk Bakłażan](/rpg/inwazja/opowiesci/karty-postaci/1709-fryderyk-baklazan.html)|1|[170725](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|
|[Dyta](/rpg/inwazja/opowiesci/karty-postaci/9999-dyta.html)|1|[170725](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|1|[170725](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|
|[Barnaba Łonowski](/rpg/inwazja/opowiesci/karty-postaci/9999-barnaba-lonowski.html)|1|[170725](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|

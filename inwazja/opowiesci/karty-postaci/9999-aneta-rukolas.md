---
layout: inwazja-karta-postaci
categories: profile
title: "Aneta Rukolas"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|171101|jej Wzór jest zbyt uszkodzony na naprawę; trafiła do Pauliny na leczenie. Sądeczny nic z tym nie zrobi.|[Magimedy przed epidemią](/rpg/inwazja/opowiesci/konspekty/171101-magimedy-przed-epidemia.html)|11/09/28|11/09/30|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171015|o której Maria się dowiedziała, że kiedyś weszła z Sądecznym i Grzybbem do Muzeum Pary. I tam COŚ się stało, co odbiło się w Efemerydzie.|[Powstrzymana wojna domowa](/rpg/inwazja/opowiesci/konspekty/171015-powstrzymana-wojna-domowa.html)|11/09/10|11/09/11|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|170409|jedna z Pentacyklu, katalistka i finansistka; chce niezależności, 29, oszpecona i zdeterminowana by zapłacić Paulinie|[Nie zabijajmy tych magów](/rpg/inwazja/opowiesci/konspekty/170409-nie-zabijajmy-tych-magow.html)|10/02/16|10/02/19|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|3|[171101](/rpg/inwazja/opowiesci/konspekty/171101-magimedy-przed-epidemia.html), [171015](/rpg/inwazja/opowiesci/konspekty/171015-powstrzymana-wojna-domowa.html), [170409](/rpg/inwazja/opowiesci/konspekty/170409-nie-zabijajmy-tych-magow.html)|
|[Wiaczesław Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-wiaczeslaw-zajcew.html)|2|[171015](/rpg/inwazja/opowiesci/konspekty/171015-powstrzymana-wojna-domowa.html), [170409](/rpg/inwazja/opowiesci/konspekty/170409-nie-zabijajmy-tych-magow.html)|
|[Tamara Muszkiet](/rpg/inwazja/opowiesci/karty-postaci/1709-tamara-muszkiet.html)|2|[171101](/rpg/inwazja/opowiesci/konspekty/171101-magimedy-przed-epidemia.html), [171015](/rpg/inwazja/opowiesci/konspekty/171015-powstrzymana-wojna-domowa.html)|
|[Sylwester Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-sylwester-bankierz.html)|2|[171101](/rpg/inwazja/opowiesci/konspekty/171101-magimedy-przed-epidemia.html), [171015](/rpg/inwazja/opowiesci/konspekty/171015-powstrzymana-wojna-domowa.html)|
|[Fryderyk Grzybb](/rpg/inwazja/opowiesci/karty-postaci/9999-fryderyk-grzybb.html)|2|[171015](/rpg/inwazja/opowiesci/konspekty/171015-powstrzymana-wojna-domowa.html), [170409](/rpg/inwazja/opowiesci/konspekty/170409-nie-zabijajmy-tych-magow.html)|
|[Tomasz Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-tomasz-myszeczka.html)|1|[171101](/rpg/inwazja/opowiesci/konspekty/171101-magimedy-przed-epidemia.html)|
|[Terror Wąż](/rpg/inwazja/opowiesci/karty-postaci/1709-terror-waz.html)|1|[170409](/rpg/inwazja/opowiesci/konspekty/170409-nie-zabijajmy-tych-magow.html)|
|[Serczedar Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-serczedar-bankierz.html)|1|[170409](/rpg/inwazja/opowiesci/konspekty/170409-nie-zabijajmy-tych-magow.html)|
|[Robert Sądeczny](/rpg/inwazja/opowiesci/karty-postaci/1709-robert-sadeczny.html)|1|[171101](/rpg/inwazja/opowiesci/konspekty/171101-magimedy-przed-epidemia.html)|
|[Radosław Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/9999-radoslaw-myszeczka.html)|1|[171101](/rpg/inwazja/opowiesci/konspekty/171101-magimedy-przed-epidemia.html)|
|[Mikaela Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-mikaela-weiner.html)|1|[170409](/rpg/inwazja/opowiesci/konspekty/170409-nie-zabijajmy-tych-magow.html)|
|[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/1709-maria-newa.html)|1|[171015](/rpg/inwazja/opowiesci/konspekty/171015-powstrzymana-wojna-domowa.html)|
|[Kleofas Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/9999-kleofas-myszeczka.html)|1|[170409](/rpg/inwazja/opowiesci/konspekty/170409-nie-zabijajmy-tych-magow.html)|
|[Jaromir Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/9999-jaromir-myszeczka.html)|1|[170409](/rpg/inwazja/opowiesci/konspekty/170409-nie-zabijajmy-tych-magow.html)|
|[Jan Karczoch](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-karczoch.html)|1|[170409](/rpg/inwazja/opowiesci/konspekty/170409-nie-zabijajmy-tych-magow.html)|
|[Jakub Dobrocień](/rpg/inwazja/opowiesci/karty-postaci/1709-jakub-dobrocien.html)|1|[171101](/rpg/inwazja/opowiesci/konspekty/171101-magimedy-przed-epidemia.html)|
|[Gabriel Dukat](/rpg/inwazja/opowiesci/karty-postaci/9999-gabriel-dukat.html)|1|[171015](/rpg/inwazja/opowiesci/konspekty/171015-powstrzymana-wojna-domowa.html)|
|[Elwira Czlikan](/rpg/inwazja/opowiesci/karty-postaci/9999-elwira-czlikan.html)|1|[171101](/rpg/inwazja/opowiesci/konspekty/171101-magimedy-przed-epidemia.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|1|[171015](/rpg/inwazja/opowiesci/konspekty/171015-powstrzymana-wojna-domowa.html)|
|[Dalia Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-dalia-weiner.html)|1|[171101](/rpg/inwazja/opowiesci/konspekty/171101-magimedy-przed-epidemia.html)|

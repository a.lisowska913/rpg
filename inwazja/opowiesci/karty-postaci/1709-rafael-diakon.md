---
layout: inwazja-karta-postaci
categories: profile
factions: "Millennium"
type: "NPC"
title: "Rafael Diakon"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **BÓL: **:
    * _Aspekty_: 
    * _Opis_: 
* **FIL: **: 
    * _Aspekty_: 
    * _Opis_: 
* **MET: Szalony naukowiec**:
    * _Aspekty_: amoralny, dociekliwy, prawdomówny
    * _Opis_: 
* **MRZ: **:
    * _Aspekty_: 
    * _Opis_: 
* **KLT: **:
    * _Aspekty_: 
    * _Opis_: 

### Umiejętności

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 
    
### Silne i słabe strony:

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

## Magia

### Szkoły magiczne

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

### Zaklęcia statyczne

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 

## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Zarobki

* ?

### Znam

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

### Mam

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

# Opis

The problem with Raphael is not that he is immoral. The problem with Raphael is that he is always curious. He always has that new experiment of his, that new concept which might cause some harm but might also lead to the greater good. And he will not force you to take it; oh no, he will just suggest it to you reminding you that he is actually an EAM mage. And this experiment will work. But there may be consequences. And he will study. He will learn. And you will suffer and he will not care at all.

There is no sadism in his actions. Raphael is just indifferent to most things which happen around. Quite dignified for a Diakon mage, very helpful, elegant and positive. He is a helpful genius who wants to actually make your goals happen. And he will tell you that whatever he designs for you has side effects. He won't ever lie to you - he is a very sincere and honest person. And this is why you will take that choice - if you don’t, he won’t convince you. But then, something may happen and you may fail and then he will be there patiently suggesting to use his experimental choice anyway. And it will go even worse…

Very proactive in making experiments (scarily proactive). Very curious and always willing to try to change the world for the better. He will do unethical things with a smile to support his friends or his side loyally. Slightly sarcastic and a bit vicious, he still has a good heart and good intentions. An augmenter who dabbles in medical stuff, knowing some basics of law who follows the mantra of “there are just results – morals are but one of means to get those results.”

With his boyish grin and pleasant demeanor are not many people called him scary. They should.

### Koncept

Wysokopoziomowy koncept postaci, 1-2 zdania.

### Motto

"Tekst"

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Patrol? Kralotyczne maskowanie!](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|wróg w osobie Lidii Weiner|Powrót Karradraela|

## Plany

|Misja|Plan|Kampania|
|-----|------|------|
|[Pętla dookoła niekralotha](/rpg/inwazja/opowiesci/konspekty/180402-petla-dookola-niekralotha.html)|ostrzec Silurię przed tym, że Krystalia próbuje znaleźć Bójkę. Krystalia może Silurii bardzo napsuć.|Adaptacja kralotyczna|

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170914|który pokonał defilerskie moce Kinglorda i przekształcił Henryka Siwieckiego w coś nowego - "Henriettę".|[Kolejna porażka Kinglorda](/rpg/inwazja/opowiesci/konspekty/170914-kolejna-porazka-kinglorda.html)|10/08/31|10/09/02|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160911|nadzoruje transformację Izy Łaniewskiej w Infensę Diakon i przyprowadził ją do Silurii celem skończenia reedukacji. Też: powiedział Silurii o KADEMkach.|[Reedukacja Infensy](/rpg/inwazja/opowiesci/konspekty/160911-reedukacja-infensy.html)|10/07/15|10/07/17|[Taniec Liści](/rpg/inwazja/opowiesci/konspekty/kampania-taniec-lisci.html)|
|170101|wraz z Melodią scorruptował Pszczelaka i Wiolettę, potem: w Unii z Laragnarhagiem tworzył maski i dywersje, też: nie zrobił absolutnie nic gdy Lidia go atakowała (co mu się chwali). Zimna ryba.|[Patrol? Kralotyczne maskowanie!](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|10/07/13|10/07/15|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161231|z kolejnym chorym pomysłem (Wioletta x Pszczelak x kraloth). Komunikuje kralotha z Andreą. Z trudem, próbuje skusić Pszczelaka Wiolettą.|[Eskalacja Czelimina, eskalacja Andrei](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html)|10/07/11|10/07/12|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161229|próbuje tłumaczyć z kralociego na Andreowy i niezbyt mu wychodzi. Zirytowany, że nie rozumie Laragnarhaga.|[Presja ze strony Czelimina](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html)|10/07/08|10/07/10|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161204|co prawda po* maga zespołowi jak może, ale jest coraz bardziej traktowany jako źródło problemów a nie prawdziwa pomoc. Jest nieco nieludzki i jest niebezpieczny.|[Zajcewowie po drugiej stronie](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html)|10/07/05|10/07/07|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161120|faktyczny geniusz; zbyt chętny do własnych rozwiązań jak na gust Andrei. Pomocny, acz trzeba uważać - zapytaj trzy razy o alternatywę.|[Tak wygrywa się sojuszami](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html)|10/07/02|10/07/04|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161113|absolutnie nieetyczny i genialny biomanta Millennium. Dostarczył Andrei dywersję zabijającą kilkanaście ludzi na wszelki wypadek.|[Świeca nie zostawia swoich](/rpg/inwazja/opowiesci/konspekty/161113-swieca-nie-zostawia-swoich.html)|10/06/29|10/07/01|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161101|lekko sarkastyczny i złośliwy, acz skutecznie wspiera Andreę, dostarcza chłeptnika i mówi co wie na temat EAM.|[Bezwzględna Lady Terminus](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|10/06/25|10/06/28|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161030|zawadiacki lekarz i lifeshaper z EAM. Całkowicie amoralny, przedkłada wyniki nad wszystko inne. Uroczy.|[Odbudowa dowodzenia Świecy](/rpg/inwazja/opowiesci/konspekty/161030-odbudowa-dowodzenia-swiecy.html)|10/06/22|10/06/24|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|141110|źródło bardzo niefortunnego źródła magii.|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html)|10/06/16|10/06/17|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|
|141102|tymczasowy rezydent Millennium, prawnik Pauliny i eks*mag EAM naprawiający nie to co powinien.|[Paulina widziała sępy nad Nikolą](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html)|10/06/14|10/06/15|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|
|141026|mag EAM z Millennium który ma związane ręce, ale jednak zdecydował się COŚ zrobić.|[Dracena widziała swój koszmar](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html)|10/06/12|10/06/13|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|
|141025|mag EAM z Millennium strasznie nie chcący robić absolutnie niczego poza obserwowaniem. Plus, próbuje uroczo intrygować.|[Gildie widziały protomaga](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|10/06/10|10/06/11|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|
|141019|obserwator testujący Dracenę.|[Lekarka widziała cybergothkę](/rpg/inwazja/opowiesci/konspekty/141019-lekarka-widziala-cybergothke.html)|10/06/08|10/06/09|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|
|170417|źródło wiedzy o syrenopnączu dla Silurii i źródło antidotum puryfikacji przeciw jego efektom|[Ratując syrenopnącze](/rpg/inwazja/opowiesci/konspekty/170417-ratujac-syrenopnacze.html)|10/03/05|10/03/12|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Mieszko Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-mieszko-bankierz.html)|8|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html), [161229](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html), [161113](/rpg/inwazja/opowiesci/konspekty/161113-swieca-nie-zostawia-swoich.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html), [161030](/rpg/inwazja/opowiesci/konspekty/161030-odbudowa-dowodzenia-swiecy.html)|
|[Marian Łajdak](/rpg/inwazja/opowiesci/karty-postaci/1709-marian-lajdak.html)|8|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html), [161113](/rpg/inwazja/opowiesci/konspekty/161113-swieca-nie-zostawia-swoich.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html), [141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html)|
|[Andżelika Leszczyńska](/rpg/inwazja/opowiesci/karty-postaci/1709-andzelika-leszczynska.html)|8|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html), [161113](/rpg/inwazja/opowiesci/konspekty/161113-swieca-nie-zostawia-swoich.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html), [141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [170417](/rpg/inwazja/opowiesci/konspekty/170417-ratujac-syrenopnacze.html)|
|[Andrea Wilgacz](/rpg/inwazja/opowiesci/karty-postaci/1709-andrea-wilgacz.html)|8|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html), [161229](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html), [161113](/rpg/inwazja/opowiesci/konspekty/161113-swieca-nie-zostawia-swoich.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html), [161030](/rpg/inwazja/opowiesci/konspekty/161030-odbudowa-dowodzenia-swiecy.html)|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|6|[161113](/rpg/inwazja/opowiesci/konspekty/161113-swieca-nie-zostawia-swoich.html), [141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html), [141019](/rpg/inwazja/opowiesci/konspekty/141019-lekarka-widziala-cybergothke.html)|
|[Lidia Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-lidia-weiner.html)|6|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161229](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html), [161030](/rpg/inwazja/opowiesci/konspekty/161030-odbudowa-dowodzenia-swiecy.html)|
|[Kajetan Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-kajetan-weiner.html)|6|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html), [161113](/rpg/inwazja/opowiesci/konspekty/161113-swieca-nie-zostawia-swoich.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html), [161030](/rpg/inwazja/opowiesci/konspekty/161030-odbudowa-dowodzenia-swiecy.html)|
|[Julian Pszczelak](/rpg/inwazja/opowiesci/karty-postaci/1709-julian-pszczelak.html)|6|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161229](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html), [161113](/rpg/inwazja/opowiesci/konspekty/161113-swieca-nie-zostawia-swoich.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|[Tadeusz Baran](/rpg/inwazja/opowiesci/karty-postaci/1709-tadeusz-baran.html)|5|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html), [161113](/rpg/inwazja/opowiesci/konspekty/161113-swieca-nie-zostawia-swoich.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html), [161030](/rpg/inwazja/opowiesci/konspekty/161030-odbudowa-dowodzenia-swiecy.html)|
|[Ryszard Bocian](/rpg/inwazja/opowiesci/karty-postaci/9999-ryszard-bocian.html)|5|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html), [141019](/rpg/inwazja/opowiesci/konspekty/141019-lekarka-widziala-cybergothke.html)|
|[Rudolf Jankowski](/rpg/inwazja/opowiesci/karty-postaci/9999-rudolf-jankowski.html)|5|[161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html), [161229](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html), [161113](/rpg/inwazja/opowiesci/konspekty/161113-swieca-nie-zostawia-swoich.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|[Nikola Kamień](/rpg/inwazja/opowiesci/karty-postaci/9999-nikola-kamien.html)|5|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html), [141019](/rpg/inwazja/opowiesci/konspekty/141019-lekarka-widziala-cybergothke.html)|
|[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/1709-maria-newa.html)|5|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html), [141019](/rpg/inwazja/opowiesci/konspekty/141019-lekarka-widziala-cybergothke.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|5|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html), [141019](/rpg/inwazja/opowiesci/konspekty/141019-lekarka-widziala-cybergothke.html)|
|[Dalia Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-dalia-weiner.html)|5|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html), [141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[Artur Kurczak](/rpg/inwazja/opowiesci/karty-postaci/9999-artur-kurczak.html)|5|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html), [141019](/rpg/inwazja/opowiesci/konspekty/141019-lekarka-widziala-cybergothke.html)|
|[Melodia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-melodia-diakon.html)|4|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html), [161229](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html)|
|[Marian Agrest](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-agrest.html)|4|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161229](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html), [161030](/rpg/inwazja/opowiesci/konspekty/161030-odbudowa-dowodzenia-swiecy.html)|
|[Laragnarhag](/rpg/inwazja/opowiesci/karty-postaci/1709-laragnarhag.html)|4|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html), [161229](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html)|
|[Katarzyna Trzosek](/rpg/inwazja/opowiesci/karty-postaci/9999-katarzyna-trzosek.html)|4|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html), [141019](/rpg/inwazja/opowiesci/konspekty/141019-lekarka-widziala-cybergothke.html)|
|[Jan Bocian](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-bocian.html)|4|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[Franciszek Marlin](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-marlin.html)|4|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[Dyta](/rpg/inwazja/opowiesci/karty-postaci/9999-dyta.html)|4|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[Świeży Lilak](/rpg/inwazja/opowiesci/karty-postaci/9999-swiezy-lilak.html)|3|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html)|
|[Wioletta Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-wioletta-bankierz.html)|3|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html), [161229](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html)|
|[Tatiana Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-tatiana-zajcew.html)|3|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html), [161229](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html)|
|[Stalowy Śledzik Żarłacz](/rpg/inwazja/opowiesci/karty-postaci/9999-stalowy-sledzik-zarlacz.html)|3|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html)|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|3|[170914](/rpg/inwazja/opowiesci/konspekty/170914-kolejna-porazka-kinglorda.html), [160911](/rpg/inwazja/opowiesci/konspekty/160911-reedukacja-infensy.html), [170417](/rpg/inwazja/opowiesci/konspekty/170417-ratujac-syrenopnacze.html)|
|[Silgor Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-silgor-diakon.html)|3|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[Jan Fiołek](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-fiolek.html)|3|[141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[Edmund Marlin](/rpg/inwazja/opowiesci/karty-postaci/9999-edmund-marlin.html)|3|[141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html), [141019](/rpg/inwazja/opowiesci/konspekty/141019-lekarka-widziala-cybergothke.html)|
|[Draconis Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-draconis-diakon.html)|3|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161229](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html)|
|[Bartosz Bławatek](/rpg/inwazja/opowiesci/karty-postaci/9999-bartosz-blawatek.html)|3|[141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[tien Radosław Pieśniec](/rpg/inwazja/opowiesci/karty-postaci/9999-tien-radoslaw-piesniec.html)|2|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html)|
|[Rodion Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-rodion-zajcew.html)|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html)|
|[Paweł Brokoty](/rpg/inwazja/opowiesci/karty-postaci/9999-pawel-brokoty.html)|2|[141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[Olaf Rajczak](/rpg/inwazja/opowiesci/karty-postaci/9999-olaf-rajczak.html)|2|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html)|
|[Mikołaj Mykot](/rpg/inwazja/opowiesci/karty-postaci/9999-mikolaj-mykot.html)|2|[141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141019](/rpg/inwazja/opowiesci/konspekty/141019-lekarka-widziala-cybergothke.html)|
|[Laurena Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-laurena-bankierz.html)|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html)|
|[Kirył Sjeld](/rpg/inwazja/opowiesci/karty-postaci/9999-kiryl-sjeld.html)|2|[161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html)|
|[Kazimierz Przybylec](/rpg/inwazja/opowiesci/karty-postaci/9999-kazimierz-przybylec.html)|2|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html)|
|[Joachim Kopiec](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-kopiec.html)|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|[Irena Krysniok](/rpg/inwazja/opowiesci/karty-postaci/9999-irena-krysniok.html)|2|[141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[Franciszek Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-myszeczka.html)|2|[161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html)|
|[Felicjan Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-felicjan-weiner.html)|2|[161113](/rpg/inwazja/opowiesci/konspekty/161113-swieca-nie-zostawia-swoich.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|[Deiiw Podniebny Grom](/rpg/inwazja/opowiesci/karty-postaci/9999-deiiw-podniebny-grom.html)|2|[161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html)|
|[Bolesław Derwisz](/rpg/inwazja/opowiesci/karty-postaci/9999-boleslaw-derwisz.html)|2|[141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[Anna Kozak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kozak.html)|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|[Aneta Rainer](/rpg/inwazja/opowiesci/karty-postaci/1709-aneta-rainer.html)|2|[161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html)|
|[Alojzy Przylaz](/rpg/inwazja/opowiesci/karty-postaci/1803-alojzy-przylaz.html)|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|[Roman Pilen](/rpg/inwazja/opowiesci/karty-postaci/9999-roman-pilen.html)|1|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html)|
|[Piotr Kit](/rpg/inwazja/opowiesci/karty-postaci/1709-piotr-kit.html)|1|[170914](/rpg/inwazja/opowiesci/konspekty/170914-kolejna-porazka-kinglorda.html)|
|[Mordred Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-mordred-blakenbauer.html)|1|[170914](/rpg/inwazja/opowiesci/konspekty/170914-kolejna-porazka-kinglorda.html)|
|[Maria Pilen](/rpg/inwazja/opowiesci/karty-postaci/9999-maria-pilen.html)|1|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html)|
|[Krystian Miałcz](/rpg/inwazja/opowiesci/karty-postaci/9999-krystian-mialcz.html)|1|[160911](/rpg/inwazja/opowiesci/konspekty/160911-reedukacja-infensy.html)|
|[Krystian Linowęc](/rpg/inwazja/opowiesci/karty-postaci/9999-krystian-linowec.html)|1|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html)|
|[Kornel Wadera](/rpg/inwazja/opowiesci/karty-postaci/1709-kornel-wadera.html)|1|[160911](/rpg/inwazja/opowiesci/konspekty/160911-reedukacja-infensy.html)|
|[Katalina Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-katalina-bankierz.html)|1|[161229](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html)|
|[Karina Łoszad](/rpg/inwazja/opowiesci/karty-postaci/9999-karina-loszad.html)|1|[161231](/rpg/inwazja/opowiesci/konspekty/161231-eskalacja-czelimina-andrei.html)|
|[Juliusz Kwarc](/rpg/inwazja/opowiesci/karty-postaci/9999-juliusz-kwarc.html)|1|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html)|
|[Jolanta Sowińska](/rpg/inwazja/opowiesci/karty-postaci/1709-jolanta-sowinska.html)|1|[161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html)|
|[Jagoda Musiąg](/rpg/inwazja/opowiesci/karty-postaci/9999-jagoda-musiag.html)|1|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html)|
|[Jacek Molenda](/rpg/inwazja/opowiesci/karty-postaci/9999-jacek-molenda.html)|1|[161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html)|
|[Izabela Kamil](/rpg/inwazja/opowiesci/karty-postaci/9999-izabela-kamil.html)|1|[141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html)|
|[Infernia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-infernia-diakon.html)|1|[170417](/rpg/inwazja/opowiesci/konspekty/170417-ratujac-syrenopnacze.html)|
|[Infensa Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-infensa-diakon.html)|1|[160911](/rpg/inwazja/opowiesci/konspekty/160911-reedukacja-infensy.html)|
|[Henryk Siwiecki](/rpg/inwazja/opowiesci/karty-postaci/1709-henryk-siwiecki.html)|1|[170914](/rpg/inwazja/opowiesci/konspekty/170914-kolejna-porazka-kinglorda.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|1|[170914](/rpg/inwazja/opowiesci/konspekty/170914-kolejna-porazka-kinglorda.html)|
|[Halina Krzyżanowska](/rpg/inwazja/opowiesci/karty-postaci/9999-halina-krzyzanowska.html)|1|[161113](/rpg/inwazja/opowiesci/konspekty/161113-swieca-nie-zostawia-swoich.html)|
|[Grzegorz Murzecki](/rpg/inwazja/opowiesci/karty-postaci/9999-grzegorz-murzecki.html)|1|[141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html)|
|[Fortitia Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-fortitia-diakon.html)|1|[161030](/rpg/inwazja/opowiesci/konspekty/161030-odbudowa-dowodzenia-swiecy.html)|
|[Dagmara Wyjątek](/rpg/inwazja/opowiesci/karty-postaci/1709-dagmara-wyjątek.html)|1|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|
|[Anita Rodos](/rpg/inwazja/opowiesci/karty-postaci/9999-anita-rodos.html)|1|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html)|
|[Ania i Basia Pilen](/rpg/inwazja/opowiesci/karty-postaci/9999-ania-i-basia-pilen.html)|1|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html)|
|[Alojzy Wołek](/rpg/inwazja/opowiesci/karty-postaci/9999-alojzy-wolek.html)|1|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html)|
|[Alicja Gąszcz](/rpg/inwazja/opowiesci/karty-postaci/9999-alicja-gaszcz.html)|1|[141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html)|
|[Akumulator Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-akumulator-diakon.html)|1|[170417](/rpg/inwazja/opowiesci/konspekty/170417-ratujac-syrenopnacze.html)|
|[Adonis Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1709-adonis-sowinski.html)|1|[170417](/rpg/inwazja/opowiesci/konspekty/170417-ratujac-syrenopnacze.html)|

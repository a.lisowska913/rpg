---
layout: inwazja-karta-postaci
categories: profile
factions: "Srebrna Świeca"
type: "NPC"
title: "Herbert Zioło"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **??**:
    * _Aspekty_: 
    * _Opis_: 	

### Umiejętności

* **??**:
    * _Aspekty_: 
    * _Opis_: 
* **??**:
    * _Aspekty_: 
    * _Opis_: 
* **??**:
    * _Aspekty_: 
    * _Opis_: 
* **??**:
    * _Aspekty_: 
    * _Opis_: 
    
### Silne i słabe strony:

* **??**:
    * _Aspekty_: 
    * _Opis_: 
	
## Magia

### Szkoły magiczne

* **??**:
    * _Aspekty_: 
    * _Opis_: 
* **??**:
    * _Aspekty_: 
    * _Opis_: 
* **??**:
    * _Aspekty_: 
    * _Opis_: 

### Zaklęcia statyczne

* **??**:
    * _Aspekty_: 
    * _Opis_: 

	
## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Zarobki


### Znam

* **??**:
    * _Aspekty_: 
    * _Opis_: 
* **??**:
    * _Aspekty_: 
    * _Opis_: 
	
### Mam

* **??**:
    * _Aspekty_: 
    * _Opis_: 
* **??**:
    * _Aspekty_: 
    * _Opis_: 

# Opis

Wysoki, starszy (54) czarodziej, terminus.
Spokojny i rozważny, co rzadko się terminusom zdarza.

Nie ocenia na pierwszy rzut oka, potrafi doskonale adaptować się do sytuacji, raczej myśliciel niż czarodziej czynu (znowu mniej typowe dla terminusa). Nie zmienia to faktu, że jest doskonałym magiem bojowym i często polega na gambitach i zasadzkach. Nie przepada za konfrontacjami bezpośrednimi; zdaje sobie sprawę z tego, że są młodsi, szybsi i zdolniejsi od niego. Ale wiek nauczył go chytrości i nietypowych podejść (lapis, srebro, miniony, efemerydy…) które skutecznie wykorzystuje.

Wykorzystuje minimalną potrzebną moc do rozwiązania problemu. Raczej drugoliniowy.

Mało kto, nie znając go, podejrzewa, że jest terminusem. Wygląda raczej na "szachowego dziadka".

## Motto

"Tekst"

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170325|terminus znany Paulinie i ekspert od luster; pomógł jej w zdalnej identyfikacji z czym Paulina ma do czynienia. Przyłożył się.|[Klątwożyt z lustra](/rpg/inwazja/opowiesci/konspekty/170325-klatwozyt-z-lustra.html)|10/02/28|10/03/03|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|
|150104|który zaczyna przekonywać się do Andromedy i wykazuje własną inicjatywę. Lepszy taktyk niż się zdawało.|[Terminus-defiler, kapłan Arazille](/rpg/inwazja/opowiesci/konspekty/150104-terminus-defiler-kaplan-arazille.html)|10/02/17|10/02/18|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|
|150103|terminus z dobrymi pomysłami, który bardzo próbuje się wykręcić od czegokolwiek co może być choć trochę niebezpieczne.|[Pryzmat Myśli pęka](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html)|10/02/15|10/02/16|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|
|141230|terminus który jako jedyny niewolnik amuletu nie tylko się postawił Andromedzie ale nawet doszedł z nią do porozumienia.|[Ofiara z wampira dla Arazille](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html)|10/02/13|10/02/14|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|
|141227|terminus Andromedy który, jakkolwiek kompetentny, ma w sobie dużo urazy do ludzi.|[Przyczajona Andromeda, ukryty Maus](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|10/02/11|10/02/12|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|
|141220|terminus ściągnięty przez Andromedę mocą jej amuletu. Nadal, terminus nade wszystko.|[Bogini Marzeń w Żonkiborze](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|10/02/09|10/02/10|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|
|150105|terminus który dla maksymalizacji skuteczności akcji nie zawaha się poświęcić ludzi.|[Dar Iliusitiusa dla Andromedy](/rpg/inwazja/opowiesci/konspekty/150105-dar-iliusitiusa-dla-andromedy.html)|10/02/05|10/02/06|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|
|130511|lokalny terminus asekurujący Żannę Szczypiorek z rozkazu Augustyna Szczypiorka.|[Ołtarz Podniesionej Dłoni](/rpg/inwazja/opowiesci/konspekty/130511-oltarz-podniesionej-dloni.html)|10/02/03|10/02/04|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Sandra Stryjek](/rpg/inwazja/opowiesci/karty-postaci/1709-sandra-stryjek.html)|7|[150104](/rpg/inwazja/opowiesci/konspekty/150104-terminus-defiler-kaplan-arazille.html), [150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html), [141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html), [150105](/rpg/inwazja/opowiesci/konspekty/150105-dar-iliusitiusa-dla-andromedy.html), [130511](/rpg/inwazja/opowiesci/konspekty/130511-oltarz-podniesionej-dloni.html)|
|[Kasia Nowak](/rpg/inwazja/opowiesci/karty-postaci/1803-kasia-nowak.html)|7|[150104](/rpg/inwazja/opowiesci/konspekty/150104-terminus-defiler-kaplan-arazille.html), [150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html), [141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html), [150105](/rpg/inwazja/opowiesci/konspekty/150105-dar-iliusitiusa-dla-andromedy.html), [130511](/rpg/inwazja/opowiesci/konspekty/130511-oltarz-podniesionej-dloni.html)|
|[Samira Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-samira-diakon.html)|5|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html), [141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html), [150105](/rpg/inwazja/opowiesci/konspekty/150105-dar-iliusitiusa-dla-andromedy.html), [130511](/rpg/inwazja/opowiesci/konspekty/130511-oltarz-podniesionej-dloni.html)|
|[Feliks Bozur](/rpg/inwazja/opowiesci/karty-postaci/9999-feliks-bozur.html)|5|[150104](/rpg/inwazja/opowiesci/konspekty/150104-terminus-defiler-kaplan-arazille.html), [150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [150105](/rpg/inwazja/opowiesci/konspekty/150105-dar-iliusitiusa-dla-andromedy.html), [130511](/rpg/inwazja/opowiesci/konspekty/130511-oltarz-podniesionej-dloni.html)|
|[Wojciech Kajak](/rpg/inwazja/opowiesci/karty-postaci/9999-wojciech-kajak.html)|4|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html), [141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|
|[Netheria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-netheria-diakon.html)|4|[150104](/rpg/inwazja/opowiesci/konspekty/150104-terminus-defiler-kaplan-arazille.html), [150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Iliusitius](/rpg/inwazja/opowiesci/karty-postaci/9999-iliusitius.html)|4|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html), [150105](/rpg/inwazja/opowiesci/konspekty/150105-dar-iliusitiusa-dla-andromedy.html)|
|[August Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-august-bankierz.html)|4|[150104](/rpg/inwazja/opowiesci/konspekty/150104-terminus-defiler-kaplan-arazille.html), [150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [150105](/rpg/inwazja/opowiesci/konspekty/150105-dar-iliusitiusa-dla-andromedy.html), [130511](/rpg/inwazja/opowiesci/konspekty/130511-oltarz-podniesionej-dloni.html)|
|[Arazille](/rpg/inwazja/opowiesci/karty-postaci/9999-arazille.html)|4|[150104](/rpg/inwazja/opowiesci/konspekty/150104-terminus-defiler-kaplan-arazille.html), [150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Anna Kajak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kajak.html)|4|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html), [141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|
|[Patryk Romczak](/rpg/inwazja/opowiesci/karty-postaci/9999-patryk-romczak.html)|3|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html), [150105](/rpg/inwazja/opowiesci/konspekty/150105-dar-iliusitiusa-dla-andromedy.html)|
|[Luksja Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-luksja-diakon.html)|3|[150104](/rpg/inwazja/opowiesci/konspekty/150104-terminus-defiler-kaplan-arazille.html), [150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Józef Pimczak](/rpg/inwazja/opowiesci/karty-postaci/9999-jozef-pimczak.html)|3|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html), [141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|
|[Gabriel Newa](/rpg/inwazja/opowiesci/karty-postaci/9999-gabriel-newa.html)|3|[150104](/rpg/inwazja/opowiesci/konspekty/150104-terminus-defiler-kaplan-arazille.html), [150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html)|
|[Feliks Hanson](/rpg/inwazja/opowiesci/karty-postaci/9999-feliks-hanson.html)|3|[150104](/rpg/inwazja/opowiesci/konspekty/150104-terminus-defiler-kaplan-arazille.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Andrzej Szop](/rpg/inwazja/opowiesci/karty-postaci/9999-andrzej-szop.html)|3|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html), [150105](/rpg/inwazja/opowiesci/konspekty/150105-dar-iliusitiusa-dla-andromedy.html)|
|[Rafał Szczęślik](/rpg/inwazja/opowiesci/karty-postaci/9999-rafal-szczeslik.html)|2|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Michał Czuk](/rpg/inwazja/opowiesci/karty-postaci/9999-michal-czuk.html)|2|[141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html), [141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|
|[Małgorzata Poran](/rpg/inwazja/opowiesci/karty-postaci/9999-malgorzata-poran.html)|2|[141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html), [141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|
|[Karradrael](/rpg/inwazja/opowiesci/karty-postaci/9999-karradrael.html)|2|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Aneta Hanson](/rpg/inwazja/opowiesci/karty-postaci/9999-aneta-hanson.html)|2|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Żanna Szczypiorek](/rpg/inwazja/opowiesci/karty-postaci/9999-zanna-szczypiorek.html)|1|[130511](/rpg/inwazja/opowiesci/konspekty/130511-oltarz-podniesionej-dloni.html)|
|[Zofia Szczypiorek](/rpg/inwazja/opowiesci/karty-postaci/9999-zofia-szczypiorek.html)|1|[150105](/rpg/inwazja/opowiesci/konspekty/150105-dar-iliusitiusa-dla-andromedy.html)|
|[Wiaczesław Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-wiaczeslaw-zajcew.html)|1|[170325](/rpg/inwazja/opowiesci/konspekty/170325-klatwozyt-z-lustra.html)|
|[Szymon Skubny](/rpg/inwazja/opowiesci/karty-postaci/9999-szymon-skubny.html)|1|[141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Sebastian Linka](/rpg/inwazja/opowiesci/karty-postaci/9999-sebastian-linka.html)|1|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html)|
|[Robert Przerot](/rpg/inwazja/opowiesci/karty-postaci/9999-robert-przerot.html)|1|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html)|
|[Radosław Szczypiorek](/rpg/inwazja/opowiesci/karty-postaci/9999-radoslaw-szczypiorek.html)|1|[130511](/rpg/inwazja/opowiesci/konspekty/130511-oltarz-podniesionej-dloni.html)|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|1|[170325](/rpg/inwazja/opowiesci/konspekty/170325-klatwozyt-z-lustra.html)|
|[Mirabelka Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-mirabelka-diakon.html)|1|[150104](/rpg/inwazja/opowiesci/konspekty/150104-terminus-defiler-kaplan-arazille.html)|
|[Maciej Dworek](/rpg/inwazja/opowiesci/karty-postaci/9999-maciej-dworek.html)|1|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html)|
|[Luiza Wanta](/rpg/inwazja/opowiesci/karty-postaci/9999-luiza-wanta.html)|1|[141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Józef Pasan](/rpg/inwazja/opowiesci/karty-postaci/9999-jozef-pasan.html)|1|[150105](/rpg/inwazja/opowiesci/konspekty/150105-dar-iliusitiusa-dla-andromedy.html)|
|[Inga Wójt](/rpg/inwazja/opowiesci/karty-postaci/9999-inga-wojt.html)|1|[130511](/rpg/inwazja/opowiesci/konspekty/130511-oltarz-podniesionej-dloni.html)|
|[Hektor Poran](/rpg/inwazja/opowiesci/karty-postaci/9999-hektor-poran.html)|1|[141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|
|[Gabriel Dukat](/rpg/inwazja/opowiesci/karty-postaci/9999-gabriel-dukat.html)|1|[170325](/rpg/inwazja/opowiesci/konspekty/170325-klatwozyt-z-lustra.html)|
|[Filip Sztukar](/rpg/inwazja/opowiesci/karty-postaci/9999-filip-sztukar.html)|1|[141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Ewa Czuk](/rpg/inwazja/opowiesci/karty-postaci/9999-ewa-czuk.html)|1|[141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|
|[Eneus Mucro](/rpg/inwazja/opowiesci/karty-postaci/9999-eneus-mucro.html)|1|[170325](/rpg/inwazja/opowiesci/konspekty/170325-klatwozyt-z-lustra.html)|
|[Dariusz Germont](/rpg/inwazja/opowiesci/karty-postaci/9999-dariusz-germont.html)|1|[150105](/rpg/inwazja/opowiesci/konspekty/150105-dar-iliusitiusa-dla-andromedy.html)|
|[Augustyn Szczypiorek](/rpg/inwazja/opowiesci/karty-postaci/9999-augustyn-szczypiorek.html)|1|[130511](/rpg/inwazja/opowiesci/konspekty/130511-oltarz-podniesionej-dloni.html)|
|[Artur Szmelc](/rpg/inwazja/opowiesci/karty-postaci/9999-artur-szmelc.html)|1|[141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Andrzej Domowierzec](/rpg/inwazja/opowiesci/karty-postaci/9999-andrzej-domowierzec.html)|1|[141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|
|[Anabela Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-anabela-diakon.html)|1|[150105](/rpg/inwazja/opowiesci/konspekty/150105-dar-iliusitiusa-dla-andromedy.html)|
|[Amelia Eter](/rpg/inwazja/opowiesci/karty-postaci/9999-amelia-eter.html)|1|[150104](/rpg/inwazja/opowiesci/konspekty/150104-terminus-defiler-kaplan-arazille.html)|

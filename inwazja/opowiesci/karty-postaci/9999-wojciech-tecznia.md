---
layout: inwazja-karta-postaci
categories: profile
title: "Wojciech Tecznia"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|140625|który próbował uratować Annę Kozak przed Saith Kameleon (myśląc, że to Dariusz Kopyto) i skończył w szpitalu. Nie mógł zatrzymać Saith.|[Ostatnia Saith](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html)|10/01/19|10/01/20|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|
|140208|były nauczyciel Andrei i Juliusza Szamana; surowy i sprawiedliwy. Na tej misji jest wspominany.|[Na ratunek terminusce](/rpg/inwazja/opowiesci/konspekty/140208-na-ratunek-terminusce.html)|10/01/13|10/01/14|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Wacław Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-waclaw-zajcew.html)|2|[140625](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html), [140208](/rpg/inwazja/opowiesci/konspekty/140208-na-ratunek-terminusce.html)|
|[Juliusz Szaman](/rpg/inwazja/opowiesci/karty-postaci/9999-juliusz-szaman.html)|2|[140625](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html), [140208](/rpg/inwazja/opowiesci/konspekty/140208-na-ratunek-terminusce.html)|
|[Anna Kozak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kozak.html)|2|[140625](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html), [140208](/rpg/inwazja/opowiesci/konspekty/140208-na-ratunek-terminusce.html)|
|[Andrea Wilgacz](/rpg/inwazja/opowiesci/karty-postaci/1709-andrea-wilgacz.html)|2|[140625](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html), [140208](/rpg/inwazja/opowiesci/konspekty/140208-na-ratunek-terminusce.html)|
|[Zdzisław Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-zdzislaw-zajcew.html)|1|[140208](/rpg/inwazja/opowiesci/konspekty/140208-na-ratunek-terminusce.html)|
|[Waldemar Zupaczka](/rpg/inwazja/opowiesci/karty-postaci/9999-waldemar-zupaczka.html)|1|[140625](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html)|
|[Tatiana Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-tatiana-zajcew.html)|1|[140208](/rpg/inwazja/opowiesci/konspekty/140208-na-ratunek-terminusce.html)|
|[Saith Kameleon](/rpg/inwazja/opowiesci/karty-postaci/9999-saith-kameleon.html)|1|[140625](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html)|
|[Quasar](/rpg/inwazja/opowiesci/karty-postaci/9999-quasar.html)|1|[140625](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html)|
|[Mojra](/rpg/inwazja/opowiesci/karty-postaci/9999-mojra.html)|1|[140625](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html)|
|[Marian Agrest](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-agrest.html)|1|[140625](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html)|
|[Marcelin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-marcelin-blakenbauer.html)|1|[140208](/rpg/inwazja/opowiesci/konspekty/140208-na-ratunek-terminusce.html)|
|[Maja Kos](/rpg/inwazja/opowiesci/karty-postaci/9999-maja-kos.html)|1|[140625](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html)|
|[Kaspian Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-kaspian-bankierz.html)|1|[140208](/rpg/inwazja/opowiesci/konspekty/140208-na-ratunek-terminusce.html)|
|[Irina Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-irina-zajcew.html)|1|[140208](/rpg/inwazja/opowiesci/konspekty/140208-na-ratunek-terminusce.html)|
|[Ika](/rpg/inwazja/opowiesci/karty-postaci/9999-ika.html)|1|[140208](/rpg/inwazja/opowiesci/konspekty/140208-na-ratunek-terminusce.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|1|[140625](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html)|
|[Grzegorz Czerwiec](/rpg/inwazja/opowiesci/karty-postaci/9999-grzegorz-czerwiec.html)|1|[140625](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html)|
|[Dariusz Kopyto](/rpg/inwazja/opowiesci/karty-postaci/9999-dariusz-kopyto.html)|1|[140625](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html)|
|[Bożena Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-bozena-zajcew.html)|1|[140208](/rpg/inwazja/opowiesci/konspekty/140208-na-ratunek-terminusce.html)|
|[Benjamin Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-benjamin-zajcew.html)|1|[140625](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html)|

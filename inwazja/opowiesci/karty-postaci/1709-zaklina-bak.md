---
layout: inwazja-karta-postaci
categories: profile
factions: "Niezrzeszeni"
type: "PC"
owner: "aga_n"
title: "Żaklina Bąk"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **BÓL: Nie mogę całe życie pracować sobą**:
    * _Aspekty_: biznes paranormalny
    * _Opis_: 
	
### Umiejętności

* **Kierowca**:
    * _Aspekty_: Uber, wyznaczanie granic, dobra opinia
    * _Opis_: 
* **Fryzjer**:
    * _Aspekty_: perswazja, kreatywność
    * _Opis_: 

	
### Silne i słabe strony:

* **??**:
    * _Aspekty_: 
    * _Opis_: 

## Otoczenie
### Powiązane frakcje

{{ page.factions }}

### Zarobki

* sas

### Znam

* **Koleżanki z zakładu fryzjerskiego**:
    * _Aspekty_: 
    * _Opis_: 
* **Mariusz Tłuk**:
    * _Aspekty_: nierób, debil, pasożyt, na którego zarabiam, ale daje mi poczucie bezpieczeństwa 
    * _Opis_: 

### Mam

* **Samochód**:
    * _Aspekty_: 
    * _Opis_: 
* **Szczotki**:
    * _Aspekty_: 
    * _Opis_: 
* **Leki uspokajające**:
    * _Aspekty_: 
    * _Opis_: 
	
# Opis


### Koncept

Znana jak Dżaki.

### Motto

# Historia:
## Plany

|Misja|Plan|Kampania|
|-----|------|------|
|[Esuriit w Półdarze](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|nie jest już 'chytrą babą'. Paranormalne istnieje a Dariusz się nią zainteresował; Mariusz nie jest wszystkim co czeka ją w przyszłości.|Wizja Dukata|

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|171229|chytra baba z Półdary. Zrobiła artefaktyczny słoik Esuriit i zainicjowała operację 'znajdź Krzysztofa'. Tendencje do ryzyka i nieufności.|[Esuriit w Półdarze](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|11/10/07|11/10/09|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Wojciech Piekarz](/rpg/inwazja/opowiesci/karty-postaci/9999-wojciech-piekarz.html)|1|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|
|[Tadek Swołczan](/rpg/inwazja/opowiesci/karty-postaci/9999-tadek-swolczan.html)|1|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|
|[Mariusz Tłuk](/rpg/inwazja/opowiesci/karty-postaci/9999-mariusz-tluk.html)|1|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|
|[Mariola Miłżoś](/rpg/inwazja/opowiesci/karty-postaci/9999-mariola-milzos.html)|1|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|
|[Krzysztof Tłuk](/rpg/inwazja/opowiesci/karty-postaci/9999-krzysztof-tluk.html)|1|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|
|[Krzysiu Miłżoś](/rpg/inwazja/opowiesci/karty-postaci/9999-krzysiu-milzos.html)|1|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|
|[Kajetan Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-kajetan-weiner.html)|1|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|
|[Jagoda Kozak](/rpg/inwazja/opowiesci/karty-postaci/1709-jagoda-kozak.html)|1|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|
|[Efemeryda Senesgradzka](/rpg/inwazja/opowiesci/karty-postaci/9999-efemeryda-senesgradzka.html)|1|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|
|[Bonifacy Jeż](/rpg/inwazja/opowiesci/karty-postaci/9999-bonifacy-jez.html)|1|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|
|[August Paszkwil](/rpg/inwazja/opowiesci/karty-postaci/1709-august-paszkwil.html)|1|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|

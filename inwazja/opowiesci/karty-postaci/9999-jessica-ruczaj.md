---
layout: inwazja-karta-postaci
categories: profile
title: "Jessica Ruczaj"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|160922|losowa 19-letnia ofiara Nocnych Strachów, "anime girl". Esme zastawiła nią dywersję mającą odciągnąć uwagę Mikado od Supernowej.|[Czarnoskalski konwent RPG](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|10/05/14|10/05/19|[Taniec Liści](/rpg/inwazja/opowiesci/konspekty/kampania-taniec-lisci.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Supernowa Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-supernowa-diakon.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Rafał Kniaź](/rpg/inwazja/opowiesci/karty-postaci/9999-rafal-kniaz.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Mikado Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-mikado-diakon.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Mateusz Ackmann](/rpg/inwazja/opowiesci/karty-postaci/1709-mateusz-ackmann.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Kornel Wadera](/rpg/inwazja/opowiesci/karty-postaci/1709-kornel-wadera.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Infensa Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-infensa-diakon.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Esme Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-esme-myszeczka.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Dorota Gacek](/rpg/inwazja/opowiesci/karty-postaci/1709-dorota-gacek.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Cyprian Koziej](/rpg/inwazja/opowiesci/karty-postaci/9999-cyprian-koziej.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Bolesław Derwisz](/rpg/inwazja/opowiesci/karty-postaci/9999-boleslaw-derwisz.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Artur Pawiak](/rpg/inwazja/opowiesci/karty-postaci/9999-artur-pawiak.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|

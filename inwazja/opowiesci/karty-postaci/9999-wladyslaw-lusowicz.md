---
layout: inwazja-karta-postaci
categories: profile
title: "Władysław Lusowicz"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170228|chciał sobie poflirtować z młodą Mauską, skończył z demonicznymi grabiami w stopie, zalany przez Franciszka, pojechał do szpitala.|[Polowanie na Mausównę](/rpg/inwazja/opowiesci/konspekty/170228-polowanie-na-mausowne.html)|10/02/08|10/02/10|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Łucja Rowicz](/rpg/inwazja/opowiesci/karty-postaci/9999-lucja-rowicz.html)|1|[170228](/rpg/inwazja/opowiesci/konspekty/170228-polowanie-na-mausowne.html)|
|[Michał Brukarz](/rpg/inwazja/opowiesci/karty-postaci/9999-michal-brukarz.html)|1|[170228](/rpg/inwazja/opowiesci/konspekty/170228-polowanie-na-mausowne.html)|
|[Krzysztof Cygan](/rpg/inwazja/opowiesci/karty-postaci/9999-krzysztof-cygan.html)|1|[170228](/rpg/inwazja/opowiesci/konspekty/170228-polowanie-na-mausowne.html)|
|[Kira Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-kira-zajcew.html)|1|[170228](/rpg/inwazja/opowiesci/konspekty/170228-polowanie-na-mausowne.html)|
|[Kinga Grzybnia](/rpg/inwazja/opowiesci/karty-postaci/9999-kinga-grzybnia.html)|1|[170228](/rpg/inwazja/opowiesci/konspekty/170228-polowanie-na-mausowne.html)|
|[Józef Krzesiwo](/rpg/inwazja/opowiesci/karty-postaci/9999-jozef-krzesiwo.html)|1|[170228](/rpg/inwazja/opowiesci/konspekty/170228-polowanie-na-mausowne.html)|
|[Judyta Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-judyta-maus.html)|1|[170228](/rpg/inwazja/opowiesci/konspekty/170228-polowanie-na-mausowne.html)|
|[Franciszek Baranowski](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-baranowski.html)|1|[170228](/rpg/inwazja/opowiesci/konspekty/170228-polowanie-na-mausowne.html)|
|[Antygona Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-antygona-diakon.html)|1|[170228](/rpg/inwazja/opowiesci/konspekty/170228-polowanie-na-mausowne.html)|
|[Antoni Chlebak](/rpg/inwazja/opowiesci/karty-postaci/9999-antoni-chlebak.html)|1|[170228](/rpg/inwazja/opowiesci/konspekty/170228-polowanie-na-mausowne.html)|

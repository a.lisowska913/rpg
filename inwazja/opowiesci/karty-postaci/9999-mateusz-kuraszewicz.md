---
layout: inwazja-karta-postaci
categories: profile
title: "Mateusz Kuraszewicz"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|160124|15-latek, który spotkał się z efemerydą i niszczył okna i krzyczał. Paulina mu pomogła. |[Trzy opętane duszyczki](/rpg/inwazja/opowiesci/konspekty/160124-trzy-opetane-duszyczki.html)|10/03/08|10/03/09|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|1|[160124](/rpg/inwazja/opowiesci/konspekty/160124-trzy-opetane-duszyczki.html)|
|[Onufry Letniczek](/rpg/inwazja/opowiesci/karty-postaci/9999-onufry-letniczek.html)|1|[160124](/rpg/inwazja/opowiesci/konspekty/160124-trzy-opetane-duszyczki.html)|
|[Olga Jeden](/rpg/inwazja/opowiesci/karty-postaci/9999-olga-jeden.html)|1|[160124](/rpg/inwazja/opowiesci/konspekty/160124-trzy-opetane-duszyczki.html)|
|[Milena Letniczek](/rpg/inwazja/opowiesci/karty-postaci/9999-milena-letniczek.html)|1|[160124](/rpg/inwazja/opowiesci/konspekty/160124-trzy-opetane-duszyczki.html)|
|[Maciek Jeden](/rpg/inwazja/opowiesci/karty-postaci/9999-maciek-jeden.html)|1|[160124](/rpg/inwazja/opowiesci/konspekty/160124-trzy-opetane-duszyczki.html)|
|[Bólokłąb](/rpg/inwazja/opowiesci/karty-postaci/9999-boloklab.html)|1|[160124](/rpg/inwazja/opowiesci/konspekty/160124-trzy-opetane-duszyczki.html)|

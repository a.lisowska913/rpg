---
layout: inwazja-karta-postaci
categories: profile
title: "Bazyli Weiner"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|161130|który pierwszy transformował w Esuriit i dotykiem przekształcił Jakuba Pestkę. Zastrzelony przez Annę. KIA.|[Sprowadzenie Mare Vortex](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html)|10/08/02|10/08/04|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161102|katalista i badacz; Esuriit; * mag, który transformuje elementy fazy Esuriit w * magię kompatybilną z energią syberionu. |[Magowie Esuriit w domu](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html)|10/07/26|10/07/28|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Łukija Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-lukija-zajcew.html)|2|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html)|
|[Zofia Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-zofia-weiner.html)|2|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html)|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|2|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html)|
|[Paweł Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-pawel-maus.html)|2|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html)|
|[Marianna Sowińska](/rpg/inwazja/opowiesci/karty-postaci/1709-marianna-sowinska.html)|2|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html)|
|[Jakub Pestka](/rpg/inwazja/opowiesci/karty-postaci/9999-jakub-pestka.html)|2|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|2|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html)|
|[Fiodor Maius Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-fiodor-maius-zajcew.html)|2|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html)|
|[Dosifiej Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-dosifiej-zajcew.html)|2|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html)|
|[Anna Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-myszeczka.html)|2|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html)|
|[Andżelika Leszczyńska](/rpg/inwazja/opowiesci/karty-postaci/1709-andzelika-leszczynska.html)|2|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html)|
|[Siriratharin](/rpg/inwazja/opowiesci/karty-postaci/1709-siriratharin.html)|1|[161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html)|
|[Mikado Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-mikado-diakon.html)|1|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html)|
|[Konstanty Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-konstanty-myszeczka.html)|1|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html)|
|[Konstanty Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-konstanty-bankierz.html)|1|[161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html)|
|[Konrad Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/9999-konrad-myszeczka.html)|1|[161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html)|
|[Kazimierz Sowiński](/rpg/inwazja/opowiesci/karty-postaci/9999-kazimierz-sowinski.html)|1|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html)|
|[Ignat Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1802-ignat-zajcew.html)|1|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html)|

---
layout: inwazja-karta-postaci
categories: profile
factions: "Niezrzeszeni"
type: "PC"
owner: "kić"
title: "Melinda Słonko"
---
# {{ page.title }}

## Postać

### Motywacje 
#### Kategorie i Aspekty

| Kategoria         |          Aspekty       |
|-------------------|------------------------|
|  Indywidualne     |  fascynacja truciznami |
|  Społeczne        |                        |
|  Wartości         | poszerzanie wiedzy;    |

#### Szczególnie

| Co chce by się działo?                | Co na pewno ma się NIE dziać? Co jest sprzeczne?          |
|---------------------------------------|-----------------------------------------------------------|
|kraloth musi zginąć                    | będę pracować jak szara myszka                            |
|stworzę nie uzależniający narkotyk     |                                                           |

### Umiejętności
#### Kategorie i Aspekty

| Kategoria         | Aspekty                                 |
|-------------------|-----------------------------------------|
| Farmaceuta        | trucizny; medykamenty;                  |
| Jogin             | kontrola własnego ciała;                |
| Botanik           | rośliny o zaczeniu medycznym            |

#### Manewry

| Jakie działania wykonuje?            | Czym osiąga sukces?           |
|--------------------------------------|-------------------------------|
| tworzenie trucizn; analiza chemiczna | laboratorium                  |
| znajdowanie roślin                   | wiedza o występowaniu         |
| spowalnianie działania środków       | kontrola ciała; farmaceutyki  |
 

### Silne i słabe strony:
#### Kategorie i Aspekty

| Kategoria    | Aspekty                        |
|--------------|--------------------------------|
|   Mindwarp   | pocałunek zapomnienia;         |

#### Manewry

| Co jest wzmocnione                      | Kosztem czego            |
|-----------------------------------------|--------------------------|
| rozkaz mentalny                         |  słaba fizycznie         |
| przynoszenie zapomnienia; usypianie     |  wiecznie głodna         |	
| dominacja przez pocałunek               |                          |
| odporność na trucizny                   |                          |	

## Zasoby i otoczenie
#### Powiązane frakcje

{{ page.factions }}

#### Kategorie i Aspekty

| Kategoria                            | Aspekty                                            |
|--------------------------------------|----------------------------------------------------|
|znajomości w koncernie farmaceutycznym| dostęp do laboratorium                             |
|                                      |                                                    |
|                                      |                                                    |
|                                      |                                                    |
|                                      |                                                    |

#### Manewry

| Jakie działania wspierane?                     | Czym osiąga sukces?                                           |
|------------------------------------------------|---------------------------------------------------------------|
|  |  |
|  |  |
|  |  |

# Opis

### Koncept

### Ogólnie

### Motywacje:

### Działanie:

### Specjalne:

### Magia:

### Otoczenie:

### Mapa kreacji

brak

### Motto

""

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Szept Mare Felix](/rpg/inwazja/opowiesci/konspekty/180327-szept-mare-felix.html)|po zakończeniu tej kampanii i odzyskaniu jej przez magów skończy nie jako kralothborn a jako mag|Adaptacja kralotyczna|


## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|180327|kralothborn ściągnięta przez Whisper na Mare Felix. Okazało się, że Mindwarpowała barmana Kolta, Mindwarpowała Bójkę i ogólnie dała Drugiemu Kralothowi sukces. Aha, ściągnęła kralotyczne kwiaty na Mare Felix.|[Szept Mare Felix](/rpg/inwazja/opowiesci/konspekty/180327-szept-mare-felix.html)|10/12/08|10/12/10|[Adaptacja kralotyczna](/rpg/inwazja/opowiesci/konspekty/kampania-adaptacja-kralotyczna.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Whisperwind](/rpg/inwazja/opowiesci/karty-postaci/9999-whisperwind.html)|1|[180327](/rpg/inwazja/opowiesci/konspekty/180327-szept-mare-felix.html)|
|[Lucjan Kowalkiewicz](/rpg/inwazja/opowiesci/karty-postaci/9999-lucjan-kowalkiewicz.html)|1|[180327](/rpg/inwazja/opowiesci/konspekty/180327-szept-mare-felix.html)|
|[Janek Malczorek](/rpg/inwazja/opowiesci/karty-postaci/9999-janek-malczorek.html)|1|[180327](/rpg/inwazja/opowiesci/konspekty/180327-szept-mare-felix.html)|
|[Bójka Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-bojka-diakon.html)|1|[180327](/rpg/inwazja/opowiesci/konspekty/180327-szept-mare-felix.html)|

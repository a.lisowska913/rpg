---
layout: inwazja-karta-postaci
categories: profile
title: "Franciszek Błazoń"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|150501|Dawca Prawdy rządzący Kultem Anioła. Chce zbudować masę krytyczną wyznawców i uderzyć w czarownicę (Olgę).|[Szalona 'czarodziejka' Zależa](/rpg/inwazja/opowiesci/konspekty/150501-szalona-czarodziejka-zaleza.html)|10/01/07|10/01/08|[Światło w Zależu Leśnym](/rpg/inwazja/opowiesci/konspekty/kampania-swiatlo-w-zalezu-lesnym.html)|
|150429|kultysta w służbie Anioła, który oddał swoją córkę na transformację przez Anioła Światła.|[Terminusi w Zależu](/rpg/inwazja/opowiesci/konspekty/150429-terminusi-w-zalezu.html)|10/01/05|10/01/06|[Światło w Zależu Leśnym](/rpg/inwazja/opowiesci/konspekty/kampania-swiatlo-w-zalezu-lesnym.html)|
|150427|choleryk, który nie radzi sobie z nałogami; wynajmuje mieszkania w Zależu Leśnym. Kultysta Anioła.|[Kult zaleskiego Anioła](/rpg/inwazja/opowiesci/konspekty/150427-kult-zaleskiego-aniola.html)|10/01/03|10/01/04|[Światło w Zależu Leśnym](/rpg/inwazja/opowiesci/konspekty/kampania-swiatlo-w-zalezu-lesnym.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Rafał Czapiek](/rpg/inwazja/opowiesci/karty-postaci/9999-rafal-czapiek.html)|3|[150501](/rpg/inwazja/opowiesci/konspekty/150501-szalona-czarodziejka-zaleza.html), [150429](/rpg/inwazja/opowiesci/konspekty/150429-terminusi-w-zalezu.html), [150427](/rpg/inwazja/opowiesci/konspekty/150427-kult-zaleskiego-aniola.html)|
|[Paweł Franna](/rpg/inwazja/opowiesci/karty-postaci/9999-pawel-franna.html)|3|[150501](/rpg/inwazja/opowiesci/konspekty/150501-szalona-czarodziejka-zaleza.html), [150429](/rpg/inwazja/opowiesci/konspekty/150429-terminusi-w-zalezu.html), [150427](/rpg/inwazja/opowiesci/konspekty/150427-kult-zaleskiego-aniola.html)|
|[Olga Miodownik](/rpg/inwazja/opowiesci/karty-postaci/1709-olga-miodownik.html)|3|[150501](/rpg/inwazja/opowiesci/konspekty/150501-szalona-czarodziejka-zaleza.html), [150429](/rpg/inwazja/opowiesci/konspekty/150429-terminusi-w-zalezu.html), [150427](/rpg/inwazja/opowiesci/konspekty/150427-kult-zaleskiego-aniola.html)|
|[Karolina Błazoń](/rpg/inwazja/opowiesci/karty-postaci/9999-karolina-blazon.html)|3|[150501](/rpg/inwazja/opowiesci/konspekty/150501-szalona-czarodziejka-zaleza.html), [150429](/rpg/inwazja/opowiesci/konspekty/150429-terminusi-w-zalezu.html), [150427](/rpg/inwazja/opowiesci/konspekty/150427-kult-zaleskiego-aniola.html)|
|[Kamil Gurnat](/rpg/inwazja/opowiesci/karty-postaci/9999-kamil-gurnat.html)|3|[150501](/rpg/inwazja/opowiesci/konspekty/150501-szalona-czarodziejka-zaleza.html), [150429](/rpg/inwazja/opowiesci/konspekty/150429-terminusi-w-zalezu.html), [150427](/rpg/inwazja/opowiesci/konspekty/150427-kult-zaleskiego-aniola.html)|
|[Rufus Eter](/rpg/inwazja/opowiesci/karty-postaci/9999-rufus-eter.html)|2|[150501](/rpg/inwazja/opowiesci/konspekty/150501-szalona-czarodziejka-zaleza.html), [150429](/rpg/inwazja/opowiesci/konspekty/150429-terminusi-w-zalezu.html)|
|[Dariusz Remont](/rpg/inwazja/opowiesci/karty-postaci/9999-dariusz-remont.html)|2|[150501](/rpg/inwazja/opowiesci/konspekty/150501-szalona-czarodziejka-zaleza.html), [150429](/rpg/inwazja/opowiesci/konspekty/150429-terminusi-w-zalezu.html)|
|[August Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-august-bankierz.html)|2|[150501](/rpg/inwazja/opowiesci/konspekty/150501-szalona-czarodziejka-zaleza.html), [150429](/rpg/inwazja/opowiesci/konspekty/150429-terminusi-w-zalezu.html)|
|[Aneta Kosicz](/rpg/inwazja/opowiesci/karty-postaci/9999-aneta-kosicz.html)|2|[150429](/rpg/inwazja/opowiesci/konspekty/150429-terminusi-w-zalezu.html), [150427](/rpg/inwazja/opowiesci/konspekty/150427-kult-zaleskiego-aniola.html)|
|[Szczepan Zaleski](/rpg/inwazja/opowiesci/karty-postaci/9999-szczepan-zaleski.html)|1|[150501](/rpg/inwazja/opowiesci/konspekty/150501-szalona-czarodziejka-zaleza.html)|
|[Sandra Stryjek](/rpg/inwazja/opowiesci/karty-postaci/1709-sandra-stryjek.html)|1|[150501](/rpg/inwazja/opowiesci/konspekty/150501-szalona-czarodziejka-zaleza.html)|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|1|[150501](/rpg/inwazja/opowiesci/konspekty/150501-szalona-czarodziejka-zaleza.html)|
|[Onufry Zaleski](/rpg/inwazja/opowiesci/karty-postaci/9999-onufry-zaleski.html)|1|[150429](/rpg/inwazja/opowiesci/konspekty/150429-terminusi-w-zalezu.html)|
|[Małgorzata Grimm](/rpg/inwazja/opowiesci/karty-postaci/9999-malgorzata-grimm.html)|1|[150429](/rpg/inwazja/opowiesci/konspekty/150429-terminusi-w-zalezu.html)|
|[Mateusz Kozociej](/rpg/inwazja/opowiesci/karty-postaci/9999-mateusz-kozociej.html)|1|[150427](/rpg/inwazja/opowiesci/konspekty/150427-kult-zaleskiego-aniola.html)|
|[Marianna Zurka](/rpg/inwazja/opowiesci/karty-postaci/9999-marianna-zurka.html)|1|[150501](/rpg/inwazja/opowiesci/konspekty/150501-szalona-czarodziejka-zaleza.html)|
|[Marek Ossoliński](/rpg/inwazja/opowiesci/karty-postaci/9999-marek-ossolinski.html)|1|[150427](/rpg/inwazja/opowiesci/konspekty/150427-kult-zaleskiego-aniola.html)|
|[Kasia Nowak](/rpg/inwazja/opowiesci/karty-postaci/1803-kasia-nowak.html)|1|[150427](/rpg/inwazja/opowiesci/konspekty/150427-kult-zaleskiego-aniola.html)|
|[Jan Grimm](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-grimm.html)|1|[150429](/rpg/inwazja/opowiesci/konspekty/150429-terminusi-w-zalezu.html)|
|[Inga Błazoń](/rpg/inwazja/opowiesci/karty-postaci/9999-inga-blazon.html)|1|[150501](/rpg/inwazja/opowiesci/konspekty/150501-szalona-czarodziejka-zaleza.html)|
|[Iliusitius](/rpg/inwazja/opowiesci/karty-postaci/9999-iliusitius.html)|1|[150501](/rpg/inwazja/opowiesci/konspekty/150501-szalona-czarodziejka-zaleza.html)|

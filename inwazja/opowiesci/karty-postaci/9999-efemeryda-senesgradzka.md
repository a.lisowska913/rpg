---
layout: inwazja-karta-postaci
categories: profile
title: "Efemeryda Senesgradzka"
---
# {{ page.title }}

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[W co gra Sądeczny?](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html)|cały czas słyszy Karola Marzyciela w magitrowni Histogram.|Wizja Dukata|
|[W co gra Sądeczny?](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html)|występuje na wielu Fazach jednocześnie i ma dopływy energii z różnych Faz.|Wizja Dukata|
|[Senesgradzka kopia Pauliny](/rpg/inwazja/opowiesci/konspekty/171218-senesgradzka-kopia-pauliny.html)|gdziekolwiek sięga zasięg efemerydy, tam jest zwiększona motywacja do pomagania ludziom przez wpływ Janiny.|Wizja Dukata|
|[Senesgradzka kopia Pauliny](/rpg/inwazja/opowiesci/konspekty/171218-senesgradzka-kopia-pauliny.html)|jest ogromna i łączy się z bardzo szeroką ilości leylinów i energii przez różne fazy|Wizja Dukata|

## Plany

|Misja|Plan|Kampania|
|-----|------|------|
|[Potrójna magitrownia Histogram](/rpg/inwazja/opowiesci/konspekty/171220-potrojna-magitrownia-histogram.html)|z niewiadomego dla wszystkich powodów rozwija program eksperymentowania na eterycznych świniach. W eterze.|Wizja Dukata|
|[Senesgradzka kopia Pauliny](/rpg/inwazja/opowiesci/konspekty/171218-senesgradzka-kopia-pauliny.html)|Zdobywać nowe imprinty, nowe istoty by móc wstawiać odpowiednie byty w odpowiednie scenariusze.|Wizja Dukata|

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|171229|która zaintrygowana magią Pauliny w magitrowni doprowadziła do transformacji człowieka w Wyssańca Esuriit i połączyła się telefonicznie z ludźmi (oni dzwonili).|[Esuriit w Półdarze](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|11/10/07|11/10/09|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171220|okazuje się, że jest bardzo zainteresowana świniami i eksperymentowaniem na świniach. Zdaniem Integry Weiner myśli w sposób zupełnie niezrozumiały.|[Potrójna magitrownia Histogram](/rpg/inwazja/opowiesci/konspekty/171220-potrojna-magitrownia-histogram.html)|11/10/05|11/10/06|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171029|która aktywnie mutuje świnie w Eterze i dostarcza im energię. Powstała wylęgarnia Skażonych świń...|[W co gra Sądeczny?](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html)|11/09/20|11/09/22|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171010|rozżarzona przez konflikt Tamary i Sądecznego. Oliwia ją uspokoiła, ale Efemeryda jest już obudzona i widzi anomalie na tym terenie.|[Jasny sygnał Tamary](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|11/09/06|11/09/09|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171004|połączona z magitrownią Histogram przez bardzo zaawansowane czujniki Weinerów; bardzo dużo "wie". Uruchomiona i dokonała emisji po badaniach z magitrowni.|[Niestabilna magitrownia](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|11/09/01|11/09/03|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171218|dała Janinie to, o co ona prosiła. Benevolent lovecraftian horror. Zbudowała imprint Pauliny sprzed sprzężenia z Marią. Na tej sesji - nieaktywna.|[Senesgradzka kopia Pauliny](/rpg/inwazja/opowiesci/konspekty/171218-senesgradzka-kopia-pauliny.html)|11/08/28|11/08/31|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|170525|gained a part of Paweł Franka at one point of time. Resides in a Water Tower. Tends to be activated and wakes up during the thunderstorms.|[New, better Senesgrad](/rpg/inwazja/opowiesci/konspekty/170525-new-better-senesgrad.html)|10/01/13|10/01/15|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|
|170523|z której magimed Dukata "wyrwał" Andrzeja Farnolisa by umieścić go w konstruminusie. Efemeryda wspierała Paulinę w reasymilacji Farnolisa.|[Opętany konstruminus](/rpg/inwazja/opowiesci/konspekty/170523-opetany-konstruminus.html)|10/01/03|10/01/04|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|6|[171220](/rpg/inwazja/opowiesci/konspekty/171220-potrojna-magitrownia-histogram.html), [171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html), [171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html), [171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html), [171218](/rpg/inwazja/opowiesci/konspekty/171218-senesgradzka-kopia-pauliny.html), [170523](/rpg/inwazja/opowiesci/konspekty/170523-opetany-konstruminus.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|4|[171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html), [171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html), [171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html), [171218](/rpg/inwazja/opowiesci/konspekty/171218-senesgradzka-kopia-pauliny.html)|
|[Karol Marzyciel](/rpg/inwazja/opowiesci/karty-postaci/1709-karol-marzyciel.html)|3|[171220](/rpg/inwazja/opowiesci/konspekty/171220-potrojna-magitrownia-histogram.html), [171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html), [171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Kajetan Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-kajetan-weiner.html)|3|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html), [171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html), [171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Jakub Dobrocień](/rpg/inwazja/opowiesci/karty-postaci/1709-jakub-dobrocien.html)|3|[171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html), [171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html), [170523](/rpg/inwazja/opowiesci/konspekty/170523-opetany-konstruminus.html)|
|[Daniel Akwitański](/rpg/inwazja/opowiesci/karty-postaci/1709-daniel-akwitanski.html)|3|[171220](/rpg/inwazja/opowiesci/konspekty/171220-potrojna-magitrownia-histogram.html), [171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html), [171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Tomasz Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-tomasz-myszeczka.html)|2|[171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html), [171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Sylwester Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-sylwester-bankierz.html)|2|[171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html), [171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Robert Sądeczny](/rpg/inwazja/opowiesci/karty-postaci/1709-robert-sadeczny.html)|2|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html), [171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Oktawia Aurinus](/rpg/inwazja/opowiesci/karty-postaci/1709-oktawia-aurinus.html)|2|[171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html), [171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/1709-maria-newa.html)|2|[171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html), [171218](/rpg/inwazja/opowiesci/konspekty/171218-senesgradzka-kopia-pauliny.html)|
|[Marcin Warinsky](/rpg/inwazja/opowiesci/karty-postaci/1709-marcin-warinsky.html)|2|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html), [171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Kaja Maślaczek](/rpg/inwazja/opowiesci/karty-postaci/1709-kaja-maslaczek.html)|2|[171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html), [171218](/rpg/inwazja/opowiesci/konspekty/171218-senesgradzka-kopia-pauliny.html)|
|[Żaklina Bąk](/rpg/inwazja/opowiesci/karty-postaci/1709-zaklina-bak.html)|1|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|
|[Zofia Przylga](/rpg/inwazja/opowiesci/karty-postaci/1709-zofia-przylga.html)|1|[171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Zofia Murczówik](/rpg/inwazja/opowiesci/karty-postaci/9999-zofia-murczowik.html)|1|[170523](/rpg/inwazja/opowiesci/konspekty/170523-opetany-konstruminus.html)|
|[Wojciech Piekarz](/rpg/inwazja/opowiesci/karty-postaci/9999-wojciech-piekarz.html)|1|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|
|[Tamara Muszkiet](/rpg/inwazja/opowiesci/karty-postaci/1709-tamara-muszkiet.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Tadek Swołczan](/rpg/inwazja/opowiesci/karty-postaci/9999-tadek-swolczan.html)|1|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|
|[Stefania Kołek](/rpg/inwazja/opowiesci/karty-postaci/9999-stefania-kolek.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Stefan Brązień](/rpg/inwazja/opowiesci/karty-postaci/9999-stefan-brazien.html)|1|[170525](/rpg/inwazja/opowiesci/konspekty/170525-new-better-senesgrad.html)|
|[Sonia Adamowiec](/rpg/inwazja/opowiesci/karty-postaci/9999-sonia-adamowiec.html)|1|[170525](/rpg/inwazja/opowiesci/konspekty/170525-new-better-senesgrad.html)|
|[Rafał Adison](/rpg/inwazja/opowiesci/karty-postaci/9999-rafal-adison.html)|1|[170525](/rpg/inwazja/opowiesci/konspekty/170525-new-better-senesgrad.html)|
|[Paweł Madler](/rpg/inwazja/opowiesci/karty-postaci/9999-pawel-madler.html)|1|[170525](/rpg/inwazja/opowiesci/konspekty/170525-new-better-senesgrad.html)|
|[Paweł Franka](/rpg/inwazja/opowiesci/karty-postaci/9999-pawel-franka.html)|1|[170525](/rpg/inwazja/opowiesci/konspekty/170525-new-better-senesgrad.html)|
|[Oliwia Aurinus](/rpg/inwazja/opowiesci/karty-postaci/1709-oliwia-aurinus.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Mariusz Tłuk](/rpg/inwazja/opowiesci/karty-postaci/9999-mariusz-tluk.html)|1|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|
|[Mariola Miłżoś](/rpg/inwazja/opowiesci/karty-postaci/9999-mariola-milzos.html)|1|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|
|[Kurt Zieloniek](/rpg/inwazja/opowiesci/karty-postaci/9999-kurt-zieloniek.html)|1|[170525](/rpg/inwazja/opowiesci/konspekty/170525-new-better-senesgrad.html)|
|[Krzysztof Tłuk](/rpg/inwazja/opowiesci/karty-postaci/9999-krzysztof-tluk.html)|1|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|
|[Krzysztof Grumrzyk](/rpg/inwazja/opowiesci/karty-postaci/1709-krzysztof-grumrzyk.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Krzysiu Miłżoś](/rpg/inwazja/opowiesci/karty-postaci/9999-krzysiu-milzos.html)|1|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|
|[Klara Pieśniarz](/rpg/inwazja/opowiesci/karty-postaci/9999-klara-piesniarz.html)|1|[170525](/rpg/inwazja/opowiesci/konspekty/170525-new-better-senesgrad.html)|
|[Karol Szczur](/rpg/inwazja/opowiesci/karty-postaci/9999-karol-szczur.html)|1|[170525](/rpg/inwazja/opowiesci/konspekty/170525-new-better-senesgrad.html)|
|[Janina Muczarok](/rpg/inwazja/opowiesci/karty-postaci/9999-janina-muczarok.html)|1|[171218](/rpg/inwazja/opowiesci/konspekty/171218-senesgradzka-kopia-pauliny.html)|
|[Jagoda Kozak](/rpg/inwazja/opowiesci/karty-postaci/1709-jagoda-kozak.html)|1|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|
|[Integra Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-integra-weiner.html)|1|[171220](/rpg/inwazja/opowiesci/konspekty/171220-potrojna-magitrownia-histogram.html)|
|[Hektor Reszniaczek](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-reszniaczek.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Grzegorz Nadziejak](/rpg/inwazja/opowiesci/karty-postaci/9999-grzegorz-nadziejak.html)|1|[171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Felicja Szampierz](/rpg/inwazja/opowiesci/karty-postaci/9999-felicja-szampierz.html)|1|[170523](/rpg/inwazja/opowiesci/konspekty/170523-opetany-konstruminus.html)|
|[Bonifacy Jeż](/rpg/inwazja/opowiesci/karty-postaci/9999-bonifacy-jez.html)|1|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|
|[Bolesław Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-boleslaw-maus.html)|1|[171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html)|
|[Bogumił Miłoszept](/rpg/inwazja/opowiesci/karty-postaci/9999-bogumil-miloszept.html)|1|[171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[August Paszkwil](/rpg/inwazja/opowiesci/karty-postaci/1709-august-paszkwil.html)|1|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|
|[Apoloniusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-apoloniusz-bankierz.html)|1|[170523](/rpg/inwazja/opowiesci/konspekty/170523-opetany-konstruminus.html)|
|[Andżelika Leszczyńska](/rpg/inwazja/opowiesci/karty-postaci/1709-andzelika-leszczynska.html)|1|[171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html)|
|[Andrzej Farnolis](/rpg/inwazja/opowiesci/karty-postaci/9999-andrzej-farnolis.html)|1|[170523](/rpg/inwazja/opowiesci/konspekty/170523-opetany-konstruminus.html)|

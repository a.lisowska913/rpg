---
layout: inwazja-karta-postaci
categories: profile
factions: "Srebrna Świeca, Szlachta"
type: "NPC"
title: "Marcin Warinsky"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **BÓL: Pogardzany, wypalony jeleń**:
    * _Aspekty_: boi się odrzucenia, boi się wykorzystania, boi się zostać sam, nie chce być "złym", chce się wykazać, chce dążyć do czegoś WSPÓLNIE
    * _Opis_: Warinsky szczerze wierzył w ideały Szlachty i silnie się rozczarował, gdy jego gorące serce okazało się wykorzystane przez Karradraela. Przygarnął go Sylwester Bankierz po ciężkim i upokarzającym procesie.
* **FIL: Nagrodzone gorące serce**: 
    * _Aspekty_: entuzjazm i energia nade wszystko, pierwszy pośród ochotników, żadna praca nie hańbi, moralność to praca dla dobra wspólnego
    * _Opis_: Warinsky zalewa entuzjazmem i byciem ochotnikiem wszystkie braki. Niechętnie pracuje z pragmatykami; chciałby, by WSZYSCY pracowali dla dobra wspólnego.
* **ŚR: Merytoryczny zespół celowy**:
    * _Aspekty_: w jedności siła, teamplayer, lojalny przełożonym, zdeterminowany ochotnik, metodyczny i ostrożny, źle radzi sobie z konfliktem osobistym
    * _Opis_: Warinsky wierzy w siłę zespołów i grup, nie jednostek. Nie ma zadania, którego nie weźmie na siebie. Jednocześnie nie radzi sobie z konfliktami "w zespole" i chciałby ich wszystkich pounikać.

### Umiejętności

* **Dywersant**:
    * _Aspekty_: dyskretny sabotaż, ukryje wprowadzone zniszczenia, znajdzie słabe punkty, odwróci uwagę, wymyśli jak coś zniszczyć, "probe to destroy", plausible deniability
    * _Opis_: Warinsky ma talent do niszczenia rzeczy. Niekoniecznie CHCE niszczyć różne rzeczy (czy to fizyczne czy magiczne), ale zwykle wie jak to zrobić. Podczas działania ze Szlachtą był przyczyną, dla której część łańcuchów logistycznych, sygnałów i komunikatów w kluczowych momentach nie zadziałały.
* **Inspektor samochodów**:
    * _Aspekty_: kocha stare auta, znajdowanie słabości, badanie specyfikacji, błędy w procesach, asertywnie się postawi
    * _Opis_: W codziennej pracy Warinsky zajmował się inspekcją samochodów i części samochodowych. Umie znaleźć luki w procesach, samych urządzeniach itp. Potrafi też się postawić i zatrzymać ogromne operacje warte setki tysięcy złotych dziennie. Prywatnie - kocha stare samochody.
* **Tele-komunikacja sygnałów**: 
    * _Aspekty_: "wszystko może być anteną", odpowiednia modulacja sygnałów, czyszczenie lub zakłócanie komunikacji, twórcza interferencja, bezstratni przesył energii
    * _Opis_: Warinsky specjalizuje się też w sygnałach różnego rodzaju. Czy to "connected car" czy "internet of things", siedział zawsze w różnego rodzaju sygnałach i formach wysyłania i odbierania tych sygnałów. Jeden z lepszych magów od przesyłania i odbierania zarówno komunikacji jak i energii.

### Silne i słabe strony:

* **Skażyciel Szlachty**:
    * _Aspekty_: silniejsze Skażenie, silniejsze Paradoksy, silniejsze wypaczanie magii, silniejsze niszczenie magii, Świeca go nie lubi, osłabiona magia
    * _Opis_: podczas wojny z Karradraelem Warinsky stał po stronie Szlachty - i nadal jest tam klasyfikowany przez magów Świecy. Wsławił się tym, że potrafi szczególnie skutecznie wypaczać magię i doprowadzać do strasznych rezultatów. Za to też nikt mu nie dziękuje. Niestety, Warinsky przy swoich zaletach... jest kiepskim magiem. Poza wypaczaniem rzeczy.

## Magia

### Szkoły magiczne

* **Kataliza**:
    * _Aspekty_: Skażenie magii, wyniszczanie artefaktów, pola Skażenia, wywołanie Paradoksu, identyfikacja słabości bytów magicznych, wypaczanie bytów magicznych, modulacja magii
    * _Opis_: 
* **Magia transportu**: 
    * _Aspekty_: przesyłanie energii, strefy ateleportów, modulacja sygnału, letalna teleportacja, wypaczanie sygnałów, nasłuchiwanie eteru, czyszczenie sygnałów
    * _Opis_: 
* **Technomancja**:
    * _Aspekty_: modulacja sygnału, telekomunikacja, wypaczanie sygnałów, uszkodzenie urządzenia, znajdowanie słabych punktów, budowanie anten, nasłuchiwanie eteru
    * _Opis_: 

### Zaklęcia statyczne

* **Zwielokrotnienie Wypaczeń**: 
    * _Aspekty_: kataliza + transport, ignoruje skalę, ignoruje dystans, działa tylko na byty podobne
    * _Opis_: Znajdźmy dany TYP energii / magii, po czym wykorzystajmy to zaklęcie by pokonać efekt skali i odległości. Jest to forma wypaczenia katalitycznego które się zwielokrotnia i działa jedynie na byty o bardzo podobnym profilu magicznym.
* **Klątwa Paradoksu**:
    * _Aspekty_: kataliza, wymusza Paradoks, wymaga bytu magicznego, ukryta w magii hosta, długo persystentna bez Quarków
    * _Opis_: Zaklęcie to nakłada klątwę na byt zawierający energię magiczną (mag, artefakt...). W pobliżu tego bytu rzucane zaklęcia mają wysokie tendencje do Paradoksu.

## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Zarobki

* żołd katalisty Świecy
* inspektor u dostawcóq części samochodowych w Powiecie Pustulskim
* współwłaściciel małego muzeum aut w Krolżysku

### Znam

* **Producenci części samochodowych w Powiecie Pustulskim**:
    * _Aspekty_: 
    * _Opis_: 
* **Znany wśród mazowieckich magów Świecy jako Skażyciel Szlachty**:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

### Mam

* **Muzeum starych samochodów w Krolżysku**:
    * _Aspekty_: współwłaściciel, tylko kilka aut, dobrze zabezpieczone
    * _Opis_: 
* **Amplifikatory Skażenia**:
    * _Aspekty_: 
    * _Opis_: 
* **Anteny, radia i sprzęt telekomunikacyjny**:
    * _Aspekty_: 
    * _Opis_: 

# Opis

-

### Koncept

Były czarodziej Szlachty, idealista, który NADAL wierzy w ideały Szlachty ale został porzucony i się nią rozczarował. Mag specjalizujący się w Skażaniu i niszczeniu. Wykorzystany by pokazać, że nie każdy mag Szlachty jest zły - dodatkowo, jako dość sympatyczny mag którego gonią upiory przeszłości.

### Motto

"Może nie zawsze wszystko się udaje, ale warto próbować, jeśli w coś się wierzy."

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Odzyskana władza Pauliny](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html)|jego plan okazał się perfekcyjny. Mistrz Skażenia Szlachty nadal w formie.|Wizja Dukata|

## Plany

|Misja|Plan|Kampania|
|-----|------|------|
|[Kryzysowo tymczasowy dyktator](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|chce pokazać się jako Wartościowy Członek Zespołu. Nieważne komu - ktokolwiek spyta i jest pod ręką.|Wizja Dukata|

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|171115|wymyślił emisję antyklątwożytową przy użyciu magitrowni jako super-anteny. Potem wyżarły z niego energię glukszwajny i uratował go Sylwester.|[Kryzysowo tymczasowy dyktator](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|11/10/03|11/10/04|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171010|katalista Świecy zdolny do Skażania energii w dość paskudny sposób; pomógł Tamarze uszkodzić sieć czujników Sądecznego.|[Jasny sygnał Tamary](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|11/09/06|11/09/09|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171004|katalista Świecy; młody i nieco narwany katalista, który chce jak najwięcej dobra wnieść w imię Świecy i Bankierza. Skupia się na sprzęcie i dekontaminacji. Wrobiony jako winny Skażenia magitrowni przez Sądecznego.|[Niestabilna magitrownia](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|11/09/01|11/09/03|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Robert Sądeczny](/rpg/inwazja/opowiesci/karty-postaci/1709-robert-sadeczny.html)|3|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html), [171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html), [171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|3|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html), [171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html), [171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Daniel Akwitański](/rpg/inwazja/opowiesci/karty-postaci/1709-daniel-akwitanski.html)|3|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html), [171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html), [171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Sylwester Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-sylwester-bankierz.html)|2|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html), [171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Efemeryda Senesgradzka](/rpg/inwazja/opowiesci/karty-postaci/9999-efemeryda-senesgradzka.html)|2|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html), [171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|2|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html), [171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Łucja Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-lucja-maus.html)|1|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|
|[Zofia Przylga](/rpg/inwazja/opowiesci/karty-postaci/1709-zofia-przylga.html)|1|[171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Wiaczesław Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-wiaczeslaw-zajcew.html)|1|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|
|[Tomasz Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-tomasz-myszeczka.html)|1|[171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Tamara Muszkiet](/rpg/inwazja/opowiesci/karty-postaci/1709-tamara-muszkiet.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Stefania Kołek](/rpg/inwazja/opowiesci/karty-postaci/9999-stefania-kolek.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Oliwia Aurinus](/rpg/inwazja/opowiesci/karty-postaci/1709-oliwia-aurinus.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Oktawia Aurinus](/rpg/inwazja/opowiesci/karty-postaci/1709-oktawia-aurinus.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Melodia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-melodia-diakon.html)|1|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|
|[Krzysztof Grumrzyk](/rpg/inwazja/opowiesci/karty-postaci/1709-krzysztof-grumrzyk.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Karol Marzyciel](/rpg/inwazja/opowiesci/karty-postaci/1709-karol-marzyciel.html)|1|[171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Kajetan Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-kajetan-weiner.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Kaja Maślaczek](/rpg/inwazja/opowiesci/karty-postaci/1709-kaja-maslaczek.html)|1|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|
|[Jakub Dobrocień](/rpg/inwazja/opowiesci/karty-postaci/1709-jakub-dobrocien.html)|1|[171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Hektor Reszniaczek](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-reszniaczek.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Grzegorz Nadziejak](/rpg/inwazja/opowiesci/karty-postaci/9999-grzegorz-nadziejak.html)|1|[171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Ewelina Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-ewelina-bankierz.html)|1|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|
|[Bogumił Miłoszept](/rpg/inwazja/opowiesci/karty-postaci/9999-bogumil-miloszept.html)|1|[171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Apoloniusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-apoloniusz-bankierz.html)|1|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|

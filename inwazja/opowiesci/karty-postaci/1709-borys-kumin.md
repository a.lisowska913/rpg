---
layout: inwazja-karta-postaci
categories: profile
factions: "Blakenbauer"
type: "NPC"
title: "Borys Kumin"
---
# {{ page.title }}

## Postać

### Motywacje (do czego dąży)

* **MET: Amoralny**:
    * _Aspekty_: bezwzględny, nadużywa władzy, tchórz
    * _Opis_: 
* **MET: Zaradny**:
    * _Aspekty_: 
    * _Opis_: 	

### Umiejętności

* **Szmugler**:
    * _Aspekty_: 
    * _Opis_: 
* **Szantażysta**:
    * _Aspekty_: motywacja przez strach
    * _Opis_: 
* **Lizus**:
    * _Aspekty_: kłamie jak z nut
    * _Opis_: 
* **Manager**:
    * _Aspekty_: motywacja przez strach
    * _Opis_: 
* **Uliczny wojownik**:
    * _Aspekty_: 
    * _Opis_:
* **Systemy zabezpieczeń**:
    * _Aspekty_: 
    * _Opis_: 	
	
### Silne i słabe strony:

* **??**:
    * _Aspekty_: 
    * _Opis_: 
	
## Magia

**brak**

	
## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Zarobki


### Znam

* **Półświatek**:
    * _Aspekty_: ludzi i magów
    * _Opis_: 
* **??**:
    * _Aspekty_: 
    * _Opis_: 
	
### Mam

* **??**:
    * _Aspekty_: 
    * _Opis_: 
* **??**:
    * _Aspekty_: 
    * _Opis_: 

# Opis

wiek: ~40 lat

Szef ochrony Blakenbauerów, oficjalnie. W rzeczywistości świetnie połączony ze wszystkimi paser wywodzący się oryginalnie z niemagicznej strony sił Zajcewów. Jako, że Hektor dostałby zawału serca, Borys oficjalnie jest szefem ochrony. W praktyce, przy jego koneksjach cały system Blakenbauerów działa w miarę bezbłędnie. Kluczowy dostawca wszystkiego, o czym młodzi by nie chcieli wiedzieć a Otton się nie przejmuje.

Człowiek. Żył jako człowiek, umarł jako człowiek. Nigdy nie został nawet viciniusem.

### Koncept

Wysokopoziomowy koncept postaci, 1-2 zdania.

### Motto

"Tekst"

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|160224|szef obrony który dla odmiany KOGOŚ obronił (unieruchomił i złapał Marcelina pistoletem strzałkowym).|[Aniołki Marcelina](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|10/06/23|10/06/24|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|151124|któremu upiekła się straszna kara od Hektora, bo Edwin kazał mu łapać wszystkie informacje o Tymotheusie Blakenbauerze...|[Odbudowa relacji konfliktem](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html)|10/06/13|10/06/14|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|151119|szef zabezpieczeń Rezydencji potwierdzający ponownie swoją niekompetencję w tej roli.|[Patrycja węszy szpiega](/rpg/inwazja/opowiesci/konspekty/151119-patrycja-weszy-szpiega.html)|10/06/08|10/06/12|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|151021|kanalia a nie szef ochrony. Sprzedał Zajcewowi kapeć zamiast zamknąć go w lochu. Dorobił się a Tatiana przegrała zakład.|[Przebudzenie Mojry](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html)|10/06/02|10/06/03|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160202|który zauważył nietypowe zachowanie pająka fazowego. Też: wysyłał mu sprośności i gadał do niego z nudów.|[Wolność pająka fazowego](/rpg/inwazja/opowiesci/konspekty/160202-wolnosc-pajaka-fazowego.html)|10/05/09|10/05/10|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150422|który dla oglądania nagiego kobiecego ciała jest skłonny sprzedać innych agentów Blakenbauerów. Szczęśliwie relatywnie niegroźny, choć obleśny.|[Śladami aptoforma](/rpg/inwazja/opowiesci/konspekty/150422-sladami-aptoforma.html)|10/05/03|10/05/04|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170118|który z radością wrobi Hektora w obraz "Parówczanej Teorii Hektora". Podejrzewamy, że hostuje stronę "PTH".|[Ludzka prokuratura a magowie](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|10/02/22|10/02/25|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150121|osoba usługi wszelakie na rzecz Blakenbauerów pokazująca, że nie ma okrucieństwa do jakiego się nie posunie by nie przegrać.|[Nowe życie Aliny](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|10/01/15|10/01/16|[Blakenbauerowie x Skorpion](/rpg/inwazja/opowiesci/konspekty/kampania-blakenbauerowie-x-skorpion.html)|
|140312|który umiera w pidżamie podczas ataku na Rezydencję Blakenbauerów do niczego się nie przydając (KIA). Nie wiadomo czemu go zabito.|[Atak na rezydencję Blakenbauerów](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html)|10/01/09|10/01/10|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|
|140114|ochroniarz Blakenbauerów z radością poniewierający i ciorający Andreą po glebie. A potem się okazało że spał z Sophistią i coś na niego wpływało.|[Zaginiony członek](/rpg/inwazja/opowiesci/konspekty/140114-zaginiony-czlonek.html)|10/01/07|10/01/08|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|
|140227|który może obserwować każde miejsce w Rezydencji i ma dostęp do wszystkich systemów wizyjnych.|[Sophistia x Marcelin](/rpg/inwazja/opowiesci/konspekty/140227-sophistia-x-marcelin.html)|10/01/05|10/01/06|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|
|140910|szef ochrony Rezydencji nie mający żadnej istotnej funkcji na tej misji.|[Reporter kontra Blakenbauerzy](/rpg/inwazja/opowiesci/konspekty/140910-reporter-kontra-blakenbauerzy.html)|10/01/01|10/01/02|[Nie umieszczone, Anulowane](/rpg/inwazja/opowiesci/konspekty/kampania-anulowane.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|8|[151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html), [151119](/rpg/inwazja/opowiesci/konspekty/151119-patrycja-weszy-szpiega.html), [151021](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html), [160202](/rpg/inwazja/opowiesci/konspekty/160202-wolnosc-pajaka-fazowego.html), [170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html), [150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html), [140227](/rpg/inwazja/opowiesci/konspekty/140227-sophistia-x-marcelin.html), [140910](/rpg/inwazja/opowiesci/konspekty/140910-reporter-kontra-blakenbauerzy.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|7|[151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html), [151021](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html), [170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html), [140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html), [140114](/rpg/inwazja/opowiesci/konspekty/140114-zaginiony-czlonek.html), [140227](/rpg/inwazja/opowiesci/konspekty/140227-sophistia-x-marcelin.html), [140910](/rpg/inwazja/opowiesci/konspekty/140910-reporter-kontra-blakenbauerzy.html)|
|[Mojra](/rpg/inwazja/opowiesci/karty-postaci/9999-mojra.html)|5|[151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html), [151021](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html), [150422](/rpg/inwazja/opowiesci/konspekty/150422-sladami-aptoforma.html), [140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html), [140910](/rpg/inwazja/opowiesci/konspekty/140910-reporter-kontra-blakenbauerzy.html)|
|[Alina Bednarz](/rpg/inwazja/opowiesci/karty-postaci/1803-alina-bednarz.html)|5|[151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html), [151119](/rpg/inwazja/opowiesci/konspekty/151119-patrycja-weszy-szpiega.html), [150422](/rpg/inwazja/opowiesci/konspekty/150422-sladami-aptoforma.html), [170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html), [150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Marcelin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-marcelin-blakenbauer.html)|4|[160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html), [170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html), [140114](/rpg/inwazja/opowiesci/konspekty/140114-zaginiony-czlonek.html), [140227](/rpg/inwazja/opowiesci/konspekty/140227-sophistia-x-marcelin.html)|
|[Dionizy Kret](/rpg/inwazja/opowiesci/karty-postaci/1709-dionizy-kret.html)|4|[151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html), [151119](/rpg/inwazja/opowiesci/konspekty/151119-patrycja-weszy-szpiega.html), [150422](/rpg/inwazja/opowiesci/konspekty/150422-sladami-aptoforma.html), [170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|[Waldemar Zupaczka](/rpg/inwazja/opowiesci/karty-postaci/9999-waldemar-zupaczka.html)|3|[140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html), [140114](/rpg/inwazja/opowiesci/konspekty/140114-zaginiony-czlonek.html), [140227](/rpg/inwazja/opowiesci/konspekty/140227-sophistia-x-marcelin.html)|
|[Szymon Skubny](/rpg/inwazja/opowiesci/karty-postaci/9999-szymon-skubny.html)|3|[151119](/rpg/inwazja/opowiesci/konspekty/151119-patrycja-weszy-szpiega.html), [151021](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html), [170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|[Patrycja Krowiowska](/rpg/inwazja/opowiesci/karty-postaci/1709-patrycja-krowiowska.html)|3|[151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html), [151119](/rpg/inwazja/opowiesci/konspekty/151119-patrycja-weszy-szpiega.html), [170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|[Margaret Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-margaret-blakenbauer.html)|3|[151021](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html), [150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html), [140910](/rpg/inwazja/opowiesci/konspekty/140910-reporter-kontra-blakenbauerzy.html)|
|[Andrea Wilgacz](/rpg/inwazja/opowiesci/karty-postaci/1709-andrea-wilgacz.html)|3|[140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html), [140114](/rpg/inwazja/opowiesci/konspekty/140114-zaginiony-czlonek.html), [140227](/rpg/inwazja/opowiesci/konspekty/140227-sophistia-x-marcelin.html)|
|[Wacław Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-waclaw-zajcew.html)|2|[140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html), [140227](/rpg/inwazja/opowiesci/konspekty/140227-sophistia-x-marcelin.html)|
|[Tatiana Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-tatiana-zajcew.html)|2|[151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html), [151021](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html)|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|2|[151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html), [151021](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html)|
|[Otton Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-otton-blakenbauer.html)|2|[140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html), [140910](/rpg/inwazja/opowiesci/konspekty/140910-reporter-kontra-blakenbauerzy.html)|
|[Netheria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-netheria-diakon.html)|2|[160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html), [150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Marta Newa](/rpg/inwazja/opowiesci/karty-postaci/9999-marta-newa.html)|2|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html), [140910](/rpg/inwazja/opowiesci/konspekty/140910-reporter-kontra-blakenbauerzy.html)|
|[Leonidas Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-leonidas-blakenbauer.html)|2|[160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html), [160202](/rpg/inwazja/opowiesci/konspekty/160202-wolnosc-pajaka-fazowego.html)|
|[Klemens X](/rpg/inwazja/opowiesci/karty-postaci/9999-klemens-x.html)|2|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html), [140910](/rpg/inwazja/opowiesci/konspekty/140910-reporter-kontra-blakenbauerzy.html)|
|[Klara Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-klara-blakenbauer.html)|2|[160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html), [160202](/rpg/inwazja/opowiesci/konspekty/160202-wolnosc-pajaka-fazowego.html)|
|[Jan Szczupak](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-szczupak.html)|2|[140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html), [140114](/rpg/inwazja/opowiesci/konspekty/140114-zaginiony-czlonek.html)|
|[Bolesław Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-boleslaw-bankierz.html)|2|[140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html), [140114](/rpg/inwazja/opowiesci/konspekty/140114-zaginiony-czlonek.html)|
|[Arazille](/rpg/inwazja/opowiesci/karty-postaci/9999-arazille.html)|2|[160202](/rpg/inwazja/opowiesci/konspekty/160202-wolnosc-pajaka-fazowego.html), [150422](/rpg/inwazja/opowiesci/konspekty/150422-sladami-aptoforma.html)|
|[Amelia Eter](/rpg/inwazja/opowiesci/karty-postaci/9999-amelia-eter.html)|2|[140114](/rpg/inwazja/opowiesci/konspekty/140114-zaginiony-czlonek.html), [140227](/rpg/inwazja/opowiesci/konspekty/140227-sophistia-x-marcelin.html)|
|[Witold Wcinkiewicz](/rpg/inwazja/opowiesci/karty-postaci/9999-witold-wcinkiewicz.html)|1|[170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|[Wiktor Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1709-wiktor-sowinski.html)|1|[151021](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html)|
|[Wanda Ketran](/rpg/inwazja/opowiesci/karty-postaci/1709-wanda-ketran.html)|1|[151119](/rpg/inwazja/opowiesci/konspekty/151119-patrycja-weszy-szpiega.html)|
|[Tymotheus Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-tymotheus-blakenbauer.html)|1|[160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|
|[Teresa Żyraf](/rpg/inwazja/opowiesci/karty-postaci/9999-teresa-zyraf.html)|1|[140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html)|
|[Stanisław Pormien](/rpg/inwazja/opowiesci/karty-postaci/9999-stanislaw-pormien.html)|1|[160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|
|[Smok](/rpg/inwazja/opowiesci/karty-postaci/9999-smok.html)|1|[140910](/rpg/inwazja/opowiesci/konspekty/140910-reporter-kontra-blakenbauerzy.html)|
|[Sebastian Tecznia](/rpg/inwazja/opowiesci/karty-postaci/9999-sebastian-tecznia.html)|1|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Rukoliusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-rukoliusz-bankierz.html)|1|[170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|[Rufus Czubek](/rpg/inwazja/opowiesci/karty-postaci/9999-rufus-czubek.html)|1|[151021](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html)|
|[Romeo Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-romeo-diakon.html)|1|[151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html)|
|[Robert Przerot](/rpg/inwazja/opowiesci/karty-postaci/9999-robert-przerot.html)|1|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Remigiusz Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-remigiusz-zajcew.html)|1|[140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html)|
|[Paweł Grzęda](/rpg/inwazja/opowiesci/karty-postaci/9999-pawel-grzeda.html)|1|[140114](/rpg/inwazja/opowiesci/konspekty/140114-zaginiony-czlonek.html)|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|1|[140910](/rpg/inwazja/opowiesci/konspekty/140910-reporter-kontra-blakenbauerzy.html)|
|[Ozydiusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-ozydiusz-bankierz.html)|1|[151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html)|
|[Nikodem Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1709-nikodem-sowinski.html)|1|[160202](/rpg/inwazja/opowiesci/konspekty/160202-wolnosc-pajaka-fazowego.html)|
|[Marysia Kiras](/rpg/inwazja/opowiesci/karty-postaci/9999-marysia-kiras.html)|1|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/1709-maria-newa.html)|1|[140910](/rpg/inwazja/opowiesci/konspekty/140910-reporter-kontra-blakenbauerzy.html)|
|[Laurena Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-laurena-bankierz.html)|1|[160202](/rpg/inwazja/opowiesci/konspekty/160202-wolnosc-pajaka-fazowego.html)|
|[Laura Filut](/rpg/inwazja/opowiesci/karty-postaci/9999-laura-filut.html)|1|[140910](/rpg/inwazja/opowiesci/konspekty/140910-reporter-kontra-blakenbauerzy.html)|
|[Krystalia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-krystalia-diakon.html)|1|[140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html)|
|[Konrad Węgorz](/rpg/inwazja/opowiesci/karty-postaci/9999-konrad-wegorz.html)|1|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Kinga Melit](/rpg/inwazja/opowiesci/karty-postaci/9999-kinga-melit.html)|1|[140910](/rpg/inwazja/opowiesci/konspekty/140910-reporter-kontra-blakenbauerzy.html)|
|[Karol Poczciwiec](/rpg/inwazja/opowiesci/karty-postaci/9999-karol-poczciwiec.html)|1|[140114](/rpg/inwazja/opowiesci/konspekty/140114-zaginiony-czlonek.html)|
|[Józef Pimczak](/rpg/inwazja/opowiesci/karty-postaci/9999-jozef-pimczak.html)|1|[150422](/rpg/inwazja/opowiesci/konspekty/150422-sladami-aptoforma.html)|
|[Juliusz Szaman](/rpg/inwazja/opowiesci/karty-postaci/9999-juliusz-szaman.html)|1|[140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html)|
|[Joachim Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-zajcew.html)|1|[160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|
|[Infernia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-infernia-diakon.html)|1|[170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|[Ika](/rpg/inwazja/opowiesci/karty-postaci/9999-ika.html)|1|[140227](/rpg/inwazja/opowiesci/konspekty/140227-sophistia-x-marcelin.html)|
|[Ignat Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1802-ignat-zajcew.html)|1|[160202](/rpg/inwazja/opowiesci/konspekty/160202-wolnosc-pajaka-fazowego.html)|
|[Grzegorz Czerwiec](/rpg/inwazja/opowiesci/karty-postaci/9999-grzegorz-czerwiec.html)|1|[140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html)|
|[GS "Aegis" 0003](/rpg/inwazja/opowiesci/karty-postaci/9999-gs-aegis-0003.html)|1|[151021](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html)|
|[Filip Sztukar](/rpg/inwazja/opowiesci/karty-postaci/9999-filip-sztukar.html)|1|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Estrella Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-estrella-diakon.html)|1|[160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|
|[Estera Piryt](/rpg/inwazja/opowiesci/karty-postaci/9999-estera-piryt.html)|1|[140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html)|
|[Emilia Kołatka](/rpg/inwazja/opowiesci/karty-postaci/1709-emilia-kolatka.html)|1|[151021](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html)|
|[Emil Maczeta](/rpg/inwazja/opowiesci/karty-postaci/9999-emil-maczeta.html)|1|[150422](/rpg/inwazja/opowiesci/konspekty/150422-sladami-aptoforma.html)|
|[Elżbieta Niemoc](/rpg/inwazja/opowiesci/karty-postaci/9999-elzbieta-niemoc.html)|1|[140312](/rpg/inwazja/opowiesci/konspekty/140312-atak-na-rezydencje.html)|
|[Elizawieta Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-elizawieta-zajcew.html)|1|[160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|
|[Edward Sasanka](/rpg/inwazja/opowiesci/karty-postaci/9999-edward-sasanka.html)|1|[151021](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html)|
|[Diana Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-diana-weiner.html)|1|[151021](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html)|
|[Dariusz Larent](/rpg/inwazja/opowiesci/karty-postaci/9999-dariusz-larent.html)|1|[150422](/rpg/inwazja/opowiesci/konspekty/150422-sladami-aptoforma.html)|
|[Dariusz Kopyto](/rpg/inwazja/opowiesci/karty-postaci/9999-dariusz-kopyto.html)|1|[140227](/rpg/inwazja/opowiesci/konspekty/140227-sophistia-x-marcelin.html)|
|[Dagmara Czeluść](/rpg/inwazja/opowiesci/karty-postaci/9999-dagmara-czelusc.html)|1|[160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|
|[Czirna Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-czirna-zajcew.html)|1|[151124](/rpg/inwazja/opowiesci/konspekty/151124-odbudowa-relacji-konfliktem.html)|
|[Crystal Shard](/rpg/inwazja/opowiesci/karty-postaci/9999-crystal-shard.html)|1|[150422](/rpg/inwazja/opowiesci/konspekty/150422-sladami-aptoforma.html)|
|[Artur Żupan](/rpg/inwazja/opowiesci/karty-postaci/1803-artur-zupan.html)|1|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Artur Bryś](/rpg/inwazja/opowiesci/karty-postaci/1709-artur-brys.html)|1|[170118](/rpg/inwazja/opowiesci/konspekty/170118-ludzka-prokuratura-a-magowie.html)|
|[Aptoform Mirasilaler](/rpg/inwazja/opowiesci/karty-postaci/9999-aptoform-mirasilaler.html)|1|[150422](/rpg/inwazja/opowiesci/konspekty/150422-sladami-aptoforma.html)|
|[Adrian Murarz](/rpg/inwazja/opowiesci/karty-postaci/9999-adrian-murarz.html)|1|[151021](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html)|

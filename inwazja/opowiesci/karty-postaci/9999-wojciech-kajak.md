---
layout: inwazja-karta-postaci
categories: profile
title: "Wojciech Kajak"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|150103|który stał się ofiarą Pryzmatu Myśli co zmieniło niepowtarzalnie jego życie.|[Pryzmat Myśli pęka](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html)|10/02/15|10/02/16|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|
|141230|policjant szukający Amelii w nieskończonej pętli marzeń.|[Ofiara z wampira dla Arazille](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html)|10/02/13|10/02/14|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|
|141227|policjant (partner i brat Anny) który jednocześnie dorabia sobie jako ten, który ma ściągnąć Amelię do domu.|[Przyczajona Andromeda, ukryty Maus](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|10/02/11|10/02/12|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|
|141220|policjant (partner i brat Anny) tak zakręcony że ucieka się do niestandardowych pytań.|[Bogini Marzeń w Żonkiborze](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|10/02/09|10/02/10|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Sandra Stryjek](/rpg/inwazja/opowiesci/karty-postaci/1709-sandra-stryjek.html)|4|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html), [141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|
|[Kasia Nowak](/rpg/inwazja/opowiesci/karty-postaci/1803-kasia-nowak.html)|4|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html), [141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|
|[Herbert Zioło](/rpg/inwazja/opowiesci/karty-postaci/1709-herbert-ziolo.html)|4|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html), [141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|
|[Anna Kajak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kajak.html)|4|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html), [141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|
|[Samira Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-samira-diakon.html)|3|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html), [141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|
|[Netheria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-netheria-diakon.html)|3|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Józef Pimczak](/rpg/inwazja/opowiesci/karty-postaci/9999-jozef-pimczak.html)|3|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html), [141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|
|[Iliusitius](/rpg/inwazja/opowiesci/karty-postaci/9999-iliusitius.html)|3|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Arazille](/rpg/inwazja/opowiesci/karty-postaci/9999-arazille.html)|3|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Rafał Szczęślik](/rpg/inwazja/opowiesci/karty-postaci/9999-rafal-szczeslik.html)|2|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Patryk Romczak](/rpg/inwazja/opowiesci/karty-postaci/9999-patryk-romczak.html)|2|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Michał Czuk](/rpg/inwazja/opowiesci/karty-postaci/9999-michal-czuk.html)|2|[141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html), [141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|
|[Małgorzata Poran](/rpg/inwazja/opowiesci/karty-postaci/9999-malgorzata-poran.html)|2|[141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html), [141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|
|[Luksja Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-luksja-diakon.html)|2|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Karradrael](/rpg/inwazja/opowiesci/karty-postaci/9999-karradrael.html)|2|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Gabriel Newa](/rpg/inwazja/opowiesci/karty-postaci/9999-gabriel-newa.html)|2|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html)|
|[Feliks Hanson](/rpg/inwazja/opowiesci/karty-postaci/9999-feliks-hanson.html)|2|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Feliks Bozur](/rpg/inwazja/opowiesci/karty-postaci/9999-feliks-bozur.html)|2|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html)|
|[Aneta Hanson](/rpg/inwazja/opowiesci/karty-postaci/9999-aneta-hanson.html)|2|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Andrzej Szop](/rpg/inwazja/opowiesci/karty-postaci/9999-andrzej-szop.html)|2|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Szymon Skubny](/rpg/inwazja/opowiesci/karty-postaci/9999-szymon-skubny.html)|1|[141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Sebastian Linka](/rpg/inwazja/opowiesci/karty-postaci/9999-sebastian-linka.html)|1|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html)|
|[Robert Przerot](/rpg/inwazja/opowiesci/karty-postaci/9999-robert-przerot.html)|1|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html)|
|[Maciej Dworek](/rpg/inwazja/opowiesci/karty-postaci/9999-maciej-dworek.html)|1|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html)|
|[Luiza Wanta](/rpg/inwazja/opowiesci/karty-postaci/9999-luiza-wanta.html)|1|[141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Hektor Poran](/rpg/inwazja/opowiesci/karty-postaci/9999-hektor-poran.html)|1|[141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|
|[Filip Sztukar](/rpg/inwazja/opowiesci/karty-postaci/9999-filip-sztukar.html)|1|[141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Ewa Czuk](/rpg/inwazja/opowiesci/karty-postaci/9999-ewa-czuk.html)|1|[141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|
|[August Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-august-bankierz.html)|1|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html)|
|[Artur Szmelc](/rpg/inwazja/opowiesci/karty-postaci/9999-artur-szmelc.html)|1|[141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Andrzej Domowierzec](/rpg/inwazja/opowiesci/karty-postaci/9999-andrzej-domowierzec.html)|1|[141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|

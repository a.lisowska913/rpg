---
layout: inwazja-karta-postaci
categories: profile
title: "Saith Catapult"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170208|zabójczyni z rozkazu Agresta; dziabnęła Silurię trucizną z zaskoczenia i uciekła niezauważona.|[Koniec wojny z Karradraelem](/rpg/inwazja/opowiesci/konspekty/170208-koniec-wojny-z-karradraelem.html)|10/08/12|10/08/13|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160229|Melisa, zwiadowczyni grupy szturmowej pod kontrolą Tamary Muszkiet odbijającej siedmiu "prawie Mausów".|[Siedmiu magów - nie Mausów](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html)|10/06/08|10/06/13|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|140618|zginęła z ręki Rezydencji Zajcewów, gdy nie udało jej się zabić serasa Benjamina Zajcewa.|[Upadek Agresta](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|10/01/17|10/01/18|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Marian Agrest](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-agrest.html)|3|[170208](/rpg/inwazja/opowiesci/konspekty/170208-koniec-wojny-z-karradraelem.html), [160229](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html), [140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|2|[170208](/rpg/inwazja/opowiesci/konspekty/170208-koniec-wojny-z-karradraelem.html), [140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|2|[170208](/rpg/inwazja/opowiesci/konspekty/170208-koniec-wojny-z-karradraelem.html), [140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Andrea Wilgacz](/rpg/inwazja/opowiesci/karty-postaci/1709-andrea-wilgacz.html)|2|[160229](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html), [140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Whisperwind](/rpg/inwazja/opowiesci/karty-postaci/9999-whisperwind.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Wacław Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-waclaw-zajcew.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Tomasz Przodownik](/rpg/inwazja/opowiesci/karty-postaci/9999-tomasz-przodownik.html)|1|[170208](/rpg/inwazja/opowiesci/konspekty/170208-koniec-wojny-z-karradraelem.html)|
|[Teresa Żyraf](/rpg/inwazja/opowiesci/karty-postaci/9999-teresa-zyraf.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Tamara Muszkiet](/rpg/inwazja/opowiesci/karty-postaci/1709-tamara-muszkiet.html)|1|[160229](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html)|
|[Tadeusz Baran](/rpg/inwazja/opowiesci/karty-postaci/1709-tadeusz-baran.html)|1|[160229](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html)|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|1|[170208](/rpg/inwazja/opowiesci/konspekty/170208-koniec-wojny-z-karradraelem.html)|
|[Sebastian Tecznia](/rpg/inwazja/opowiesci/karty-postaci/9999-sebastian-tecznia.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Saith Flamecaller](/rpg/inwazja/opowiesci/karty-postaci/9999-saith-flamecaller.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Quasar](/rpg/inwazja/opowiesci/karty-postaci/9999-quasar.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Ozydiusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-ozydiusz-bankierz.html)|1|[160229](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html)|
|[Otton Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-otton-blakenbauer.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Oliwier Sowiński](/rpg/inwazja/opowiesci/karty-postaci/9999-oliwier-sowinski.html)|1|[170208](/rpg/inwazja/opowiesci/konspekty/170208-koniec-wojny-z-karradraelem.html)|
|[Mojra](/rpg/inwazja/opowiesci/karty-postaci/9999-mojra.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Metody Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-metody-bankierz.html)|1|[170208](/rpg/inwazja/opowiesci/konspekty/170208-koniec-wojny-z-karradraelem.html)|
|[Mateusz Nieborak](/rpg/inwazja/opowiesci/karty-postaci/9999-mateusz-nieborak.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Marcelin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-marcelin-blakenbauer.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Maja Kos](/rpg/inwazja/opowiesci/karty-postaci/9999-maja-kos.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Leokadia Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/9999-leokadia-myszeczka.html)|1|[160229](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html)|
|[Konstanty Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-konstanty-myszeczka.html)|1|[170208](/rpg/inwazja/opowiesci/konspekty/170208-koniec-wojny-z-karradraelem.html)|
|[Klaudia Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-klaudia-bankierz.html)|1|[170208](/rpg/inwazja/opowiesci/konspekty/170208-koniec-wojny-z-karradraelem.html)|
|[Karolina Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-karolina-maus.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Karol Poczciwiec](/rpg/inwazja/opowiesci/karty-postaci/9999-karol-poczciwiec.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Judyta Karnisz](/rpg/inwazja/opowiesci/karty-postaci/9999-judyta-karnisz.html)|1|[160229](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html)|
|[Jan Anioł Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-aniol-bankierz.html)|1|[160229](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html)|
|[Irina Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-irina-zajcew.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Grzegorz Czerwiec](/rpg/inwazja/opowiesci/karty-postaci/9999-grzegorz-czerwiec.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Estera Piryt](/rpg/inwazja/opowiesci/karty-postaci/9999-estera-piryt.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Elżbieta Niemoc](/rpg/inwazja/opowiesci/karty-postaci/9999-elzbieta-niemoc.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Eis](/rpg/inwazja/opowiesci/karty-postaci/9999-eis.html)|1|[170208](/rpg/inwazja/opowiesci/konspekty/170208-koniec-wojny-z-karradraelem.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Dariusz Kopyto](/rpg/inwazja/opowiesci/karty-postaci/9999-dariusz-kopyto.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Benjamin Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-benjamin-zajcew.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Anna Kozak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kozak.html)|1|[160229](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html)|
|[Andrzej Sowiński](/rpg/inwazja/opowiesci/karty-postaci/9999-andrzej-sowinski.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Aleksander Sowiński](/rpg/inwazja/opowiesci/karty-postaci/9999-aleksander-sowinski.html)|1|[160229](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html)|

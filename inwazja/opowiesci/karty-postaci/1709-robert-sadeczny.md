---
layout: inwazja-karta-postaci
categories: profile
factions: "Rodzina Dukata"
type: "NPC"
title: "Robert Sądeczny"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **BÓL: Nieistotny drobiazg, który nic nie zmienił**
    * _Aspekty_: "nikt mnie nie zapamięta", bycie nieważnym jednym z wielu, brak wpływu na cokolwiek ważnego, promuje rzeczy efektowne i innowacyjne, nie osiągnięcie niczego wielkiego
    * _Opis_: Robert najbardziej boi się małości i miałkości. Chce robić rzeczy dzięki którym zmieni świat i zostanie zapamiętany jako wizjoner, jako ktoś, kto dopracował region - jako osoba dzięki której stało się coś świetnego. Chce, by stawiano mu pomniki. Bohater lub zbrodniarz - mniej istotne - byle był znany i zapamiętany.
* **FIL: Opiekuńcza Tyrania**
    * _Aspekty_: potęga oznacza moralność, "pójdziesz ze mną do łóżka", spełnię wolę Dukata, wzmacnianie SWOJEGO regionu, dążenie do władzy absolutnej
    * _Opis_: Słabsze od niego kobiety robią to, czego on sobie życzy i to jest właściwe. On spełnia cele Dukata bo jest słabszy i to jest właściwe. Przez swoją siłę odpowiada za region i ekonomię i to jest właściwe. Im jesteś silniejszy tym większa odpowiedzialność - ale i przywileje. Jeśli nie jesteś silny, nie masz żadnych praw.
* **ŚR: Dominacja technoinfomagiczna**: 
    * _Aspekty_: konstrukcja Eliksiru Miłości, adaptacja Harvestera, ryzykowne eksperymenty, dążenie do przewagi techno/magi/info, zmuszać przeciwnika do popełniania błędów, zawsze dezinformować
    * _Opis_: Robert szczerze wierzy, że by móc coś osiągnąć lub utrzymać musi mieć przewagę technologiczną, informacyjną i magiczną. Wierzy, że środkiem dojścia do celu jest najwyższy poziom technologii, informacji i magii. Czyli albo on musi mieć najwyższy albo inni muszą mieć niższy.

### Umiejętności

* **Elektronika i telekomunikacja**:
    * _Aspekty_: systemy podsłuchowe, transmisja danych, urządzenia telekomunikacyjne, wszelakie zamki elektroniczne, zabezpieczenia, kradzież samochodów
    * _Opis_: Robert jest specjalistą od sieci i komunikacji idącej po tych sieciach. Jest też dobry w zabezpieczeniach i systemach podsłuchowych. Aha, wyszedł od kradzieży samochodów.
* **Propagandzista**: 
    * _Aspekty_: dezinformacja, wystąpienia publiczne, zdobywanie informacji ze źródeł, spowalnianie przeciwnika, bullshitowanie, gładkie słowa, montowanie sygnału
    * _Opis_: Robert słynie z tego, że potrafi kontrolować plotki i wysyłać memy z niesamowitą skutecznością. Doskonały w przekonywaniu jak i w działaniu na tłumy. Świetny PRowiec.
* **Regent mafii**:
    * _Aspekty_: logistyka mafii, organizowanie akcji, maksymalizacja dochodów, szantaże, corruption, zastraszanie
    * _Opis_: Nie jest łatwo zarządzać przestępcami, zwłaszcza z ramienia Dukata. Jednak Robertowi się to udaje - 
    
### Silne i słabe strony:

* **Jako pierwszy ma najświeższe informacje**:
    * _Aspekty_: jego informacje są świetne, jego wrogowie działają we mgle, nie rozpatruje innych sytuacji niż "ja wiem, inni nie"
    * _Opis_: Robert dezinformuje wszystkich, czy to sojuszników czy wrogów tylko po to, by on miał dostęp do najświeższych i najlepszych informacji. To sprawia, że przywykł, że on ma dostęp do najlepszych informacji i nikt inny nie ma. W ten sposób można wytrącić go z równowagi i pokonać.

## Magia

### Szkoły magiczne

* **Magia transportu**:
    * _Aspekty_: praca z wielkimi portalami
    * _Opis_: 
* **Technomancja**: 
    * _Aspekty_: infomancja, urządzenia komunikacyjne, podsłuchiwanie, drony, przełamywanie zabezpieczen, fałszowanie informacji, zdobywanie wiedzy z danych, praca w bibliotekach i internecie
    * _Opis_: 

### Zaklęcia statyczne

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 

## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Zarobki

* Procent od różnych haraczy
* Specjalista wspierający Portalisko Pustulskie

### Znam

* **Portalisko Pustulskie**:
    * _Aspekty_: 
    * _Opis_: 
* **Rodzina Dukata**:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

### Mam

* **Cały system dezinformacji i informowania**:
    * _Aspekty_: stworzenie fałszywych informacji, ukrycie tego co ma znaczenie, wrobienie kogoś, wprowadzenie kogoś w paranoję, rozbudowany system totalny, "niczego nie przeoczono"
    * _Opis_: Działa nie tylko w świecie cyfrowym ale i analogowym; ten system to zbiór ludzkich agentów, magów, viciniusów, infomancji, czujników... po to, by dokonać budowania fałszerstwa informacji w sposób totalny - biblioteki, magowie... wszystko. Tak samo, całość informacji spływa do tego systemu i Robert Sądeczny dostaje najlepszy możliwy system rekomendacji.
* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

# Opis

Robert Sądeczny jest byłym magiem Świecy, który opuścił gildię po tym, jak doszło do sytuacji z Karradraelem. Gildia nie uchroniła jego życia przed inwazją Mausów i zniszczeniem całego świata, jakie Robert kiedykolwiek widział i rozumiał. Ogólnie, nie jest to zły czarodziej, ale musi mieć silne zasady i jasne wytyczne oraz jego plecy muszą być chronione - Dukat to zapewnia.

Robert chce stosunkowo przyjemnego życia i jest skłonny włożyć dużo energii i pracy, by to dostać. Nie boi się eksperymentować i dostać w mordę raz czy dwa - wierzy, że pod Dukatem będzie mu się żyło lepiej, co więcej, WIERZY w to, co Dukat chce osiągnąć - usunięcie wszystkich magicznych chorób i świat pełen porządku i pod ścisłą kontrolą.

### Koncept

Robert jest w pewien sposób anty-Draceną. Dracena dąży do uwolnienia, Robert do porządku i kontroli. Oboje nie mają instynktów bojowych, ale oboje silnie walczą o to, co chcą osiągnąć - jeden wierzy w Dukata, druga wierzy w swoje zasady.

### Motto

"Tekst"

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Utracona kontrola](/rpg/inwazja/opowiesci/konspekty/171031-utracona-kontrola.html)|ranny; przez mniej więcej tydzień nie może czarować ani działać aktywnie. W niełasce u Dukata.|Wizja Dukata|
|[Dlaczego magitrownia działa?](/rpg/inwazja/opowiesci/konspekty/171205-dlaczego-magitrownia-dziala.html)|całą akcję z magitrownią aż do przejęcia przypisał skutecznie sobie i tylko sobie.|Wizja Dukata|

## Plany

|Misja|Plan|Kampania|
|-----|------|------|
|[Kryzysowo tymczasowy dyktator](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|oddał Paulinie dowodzenie i dał jej dwa tygodnie by sprawdzić, czy poradzi sobie lepiej niż Sylwester jako Rezydentka. Obserwuje.|Wizja Dukata|
|[Świnia na portalisku](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|chce wykorzystać agencję detektywistyczną Kociebora do zbierania informacji i silniejszej kontroli terenu.|Wizja Dukata|
|[Świnia na portalisku](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|jest zainteresowany stabilizacją terenu i ochroną portaliska Pustulskiego. Pilnuje, by na jego terenie nic złego nikomu się nie działo.|Wizja Dukata|
|[Powrót do domu](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html)|dezinformuje, utrzymuje przewagę magitechnologiczną, wprowadza chaos i zdobywa pieniądze dla Dukata.|Wizja Dukata|
|[Dlaczego magitrownia działa?](/rpg/inwazja/opowiesci/konspekty/171205-dlaczego-magitrownia-dziala.html)|przejąć kontrolę nad głównymi kluczowymi elementami otoczenia; potrzebuje do tego wsparcia|Wizja Dukata|

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|171115|nie może spać w magimedzie bo Daniel budzi. Oddał Paulinie siły (choć optował za Danielem) na 2 tygodnie. Po czym wrócił do spania.|[Kryzysowo tymczasowy dyktator](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|11/10/03|11/10/04|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171101|bardzo ciężko chory i ranny po sprawie z portaliskiem. Paulina obroniła go przed Dukatem. Chwilowo wyłączony z akcji w swoim pałacyku.|[Magimedy przed epidemią](/rpg/inwazja/opowiesci/konspekty/171101-magimedy-przed-epidemia.html)|11/09/28|11/09/30|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171031|z narażeniem życia wszedł w Eter ratować wciągniętych magów i ludzi. Gdy Tamara mu pomogła, trochę ją podrywał - zwłaszcza, jak jej mundur odchrumkał.|[Utracona kontrola](/rpg/inwazja/opowiesci/konspekty/171031-utracona-kontrola.html)|11/09/24|11/09/27|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171024|chciał przejąć kontrolę nad Wieżą Wichrów i załatwił do tego celu Kociebora; oszukany i stracił okazję. Ale uzyskał zapewnienie awianowej linii produkcyjnej dron, by mieć oczy. Aha, nie życzy sobie wypadków z dziećmi na SWOIM terenie.|[Detektyw, lecz nie Sądecznego](/rpg/inwazja/opowiesci/konspekty/171024-detektyw-lecz-nie-sadecznego.html)|11/09/16|11/09/18|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171010|nie docenił determinacji Tamary i stracił sieć czujników. Ściągnął glukszwajna i próbuje zregenerować to, co stracił, ale wszystko wypada mu z rąk.|[Jasny sygnał Tamary](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|11/09/06|11/09/09|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171004|nadal naprawia problemy na portalisku; ściągnął katalistkę do magitrowni i dba o okolicę. Skutecznie wrobił Warinskiego w to, że przez niego się pogorszyło. Paulina ma dowody, że zrobił krzywdę w przeszłości Katii Grajek - zniewolił ją i zamaskował ślady.|[Niestabilna magitrownia](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|11/09/01|11/09/03|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171003|rezydent Dukata, który wyjątkowo napracował się ciężko na swoje utrzymanie stabilizując portal na portalisku po "wizycie" glukszwajna. Rozwiązał problem kto co płaci twardą ręką.|[Świnia na portalisku](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|11/08/28|11/08/30|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171001|rezydent Dukata na teren powiatu Pustulskiego. Przeprowadził potężną kampanię dezinformacyjną przeciw Sylwestrowi Bankierzowi i rozbudował sieci detekcji magii na swoim terenie.|[Powrót do domu](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html)|11/08/25|11/08/27|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171205|mafioso, który próbuje przejąć kontrolę nad magitrownią Histogram i wysłał tam Dobrocienia i Kazuń. Bez większego powodzenia.|[Dlaczego magitrownia działa?](/rpg/inwazja/opowiesci/konspekty/171205-dlaczego-magitrownia-dziala.html)|10/12/15|10/12/17|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|7|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html), [171101](/rpg/inwazja/opowiesci/konspekty/171101-magimedy-przed-epidemia.html), [171031](/rpg/inwazja/opowiesci/konspekty/171031-utracona-kontrola.html), [171024](/rpg/inwazja/opowiesci/konspekty/171024-detektyw-lecz-nie-sadecznego.html), [171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html), [171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html), [171001](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html)|
|[Sylwester Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-sylwester-bankierz.html)|5|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html), [171101](/rpg/inwazja/opowiesci/konspekty/171101-magimedy-przed-epidemia.html), [171031](/rpg/inwazja/opowiesci/konspekty/171031-utracona-kontrola.html), [171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html), [171001](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html)|
|[Tomasz Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-tomasz-myszeczka.html)|4|[171101](/rpg/inwazja/opowiesci/konspekty/171101-magimedy-przed-epidemia.html), [171031](/rpg/inwazja/opowiesci/konspekty/171031-utracona-kontrola.html), [171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html), [171003](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|
|[Jakub Dobrocień](/rpg/inwazja/opowiesci/karty-postaci/1709-jakub-dobrocien.html)|4|[171101](/rpg/inwazja/opowiesci/konspekty/171101-magimedy-przed-epidemia.html), [171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html), [171003](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html), [171205](/rpg/inwazja/opowiesci/konspekty/171205-dlaczego-magitrownia-dziala.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|4|[171031](/rpg/inwazja/opowiesci/konspekty/171031-utracona-kontrola.html), [171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html), [171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html), [171001](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html)|
|[Daniel Akwitański](/rpg/inwazja/opowiesci/karty-postaci/1709-daniel-akwitanski.html)|4|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html), [171031](/rpg/inwazja/opowiesci/konspekty/171031-utracona-kontrola.html), [171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html), [171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Tamara Muszkiet](/rpg/inwazja/opowiesci/karty-postaci/1709-tamara-muszkiet.html)|3|[171101](/rpg/inwazja/opowiesci/konspekty/171101-magimedy-przed-epidemia.html), [171031](/rpg/inwazja/opowiesci/konspekty/171031-utracona-kontrola.html), [171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Marcin Warinsky](/rpg/inwazja/opowiesci/karty-postaci/1709-marcin-warinsky.html)|3|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html), [171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html), [171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Zofia Przylga](/rpg/inwazja/opowiesci/karty-postaci/1709-zofia-przylga.html)|2|[171031](/rpg/inwazja/opowiesci/konspekty/171031-utracona-kontrola.html), [171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Natalia Kazuń](/rpg/inwazja/opowiesci/karty-postaci/1709-natalia-kazun.html)|2|[171003](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html), [171205](/rpg/inwazja/opowiesci/konspekty/171205-dlaczego-magitrownia-dziala.html)|
|[Krzysztof Grumrzyk](/rpg/inwazja/opowiesci/karty-postaci/1709-krzysztof-grumrzyk.html)|2|[171024](/rpg/inwazja/opowiesci/konspekty/171024-detektyw-lecz-nie-sadecznego.html), [171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Kociebor Dyrygent](/rpg/inwazja/opowiesci/karty-postaci/1709-kociebor-dyrygent.html)|2|[171024](/rpg/inwazja/opowiesci/konspekty/171024-detektyw-lecz-nie-sadecznego.html), [171003](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|
|[Karol Marzyciel](/rpg/inwazja/opowiesci/karty-postaci/1709-karol-marzyciel.html)|2|[171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html), [171205](/rpg/inwazja/opowiesci/konspekty/171205-dlaczego-magitrownia-dziala.html)|
|[Kajetan Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-kajetan-weiner.html)|2|[171024](/rpg/inwazja/opowiesci/konspekty/171024-detektyw-lecz-nie-sadecznego.html), [171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Kaja Maślaczek](/rpg/inwazja/opowiesci/karty-postaci/1709-kaja-maslaczek.html)|2|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html), [171001](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html)|
|[Hektor Reszniaczek](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-reszniaczek.html)|2|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html), [171001](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html)|
|[Efemeryda Senesgradzka](/rpg/inwazja/opowiesci/karty-postaci/9999-efemeryda-senesgradzka.html)|2|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html), [171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Apoloniusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-apoloniusz-bankierz.html)|2|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html), [171205](/rpg/inwazja/opowiesci/konspekty/171205-dlaczego-magitrownia-dziala.html)|
|[Łucja Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-lucja-maus.html)|1|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|
|[Wiaczesław Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-wiaczeslaw-zajcew.html)|1|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|
|[Tomasz Kapelusz](/rpg/inwazja/opowiesci/karty-postaci/9999-tomasz-kapelusz.html)|1|[171024](/rpg/inwazja/opowiesci/konspekty/171024-detektyw-lecz-nie-sadecznego.html)|
|[Stefania Kołek](/rpg/inwazja/opowiesci/karty-postaci/9999-stefania-kolek.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Ryszard Kota](/rpg/inwazja/opowiesci/karty-postaci/9999-ryszard-kota.html)|1|[171003](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|
|[Radosław Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/9999-radoslaw-myszeczka.html)|1|[171101](/rpg/inwazja/opowiesci/konspekty/171101-magimedy-przed-epidemia.html)|
|[Paweł Kupiernik](/rpg/inwazja/opowiesci/karty-postaci/9999-pawel-kupiernik.html)|1|[171003](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|
|[Oliwia Aurinus](/rpg/inwazja/opowiesci/karty-postaci/1709-oliwia-aurinus.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Oktawia Aurinus](/rpg/inwazja/opowiesci/karty-postaci/1709-oktawia-aurinus.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Melodia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-melodia-diakon.html)|1|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|
|[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/1709-maria-newa.html)|1|[171001](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html)|
|[Maja Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-maja-weiner.html)|1|[171024](/rpg/inwazja/opowiesci/konspekty/171024-detektyw-lecz-nie-sadecznego.html)|
|[Katia Grajek](/rpg/inwazja/opowiesci/karty-postaci/1709-katia-grajek.html)|1|[171001](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html)|
|[Jolanta Cieśliska](/rpg/inwazja/opowiesci/karty-postaci/9999-jolanta-ciesliska.html)|1|[171003](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|
|[Henryk Mordżyn](/rpg/inwazja/opowiesci/karty-postaci/9999-henryk-mordzyn.html)|1|[171003](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|
|[Grzegorz Nadziejak](/rpg/inwazja/opowiesci/karty-postaci/9999-grzegorz-nadziejak.html)|1|[171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Gabriel Dukat](/rpg/inwazja/opowiesci/karty-postaci/9999-gabriel-dukat.html)|1|[171001](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html)|
|[Filip Keramiusz](/rpg/inwazja/opowiesci/karty-postaci/9999-filip-keramiusz.html)|1|[171024](/rpg/inwazja/opowiesci/konspekty/171024-detektyw-lecz-nie-sadecznego.html)|
|[Ewelina Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-ewelina-bankierz.html)|1|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|
|[Elwira Czlikan](/rpg/inwazja/opowiesci/karty-postaci/9999-elwira-czlikan.html)|1|[171101](/rpg/inwazja/opowiesci/konspekty/171101-magimedy-przed-epidemia.html)|
|[Efraim Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-efraim-weiner.html)|1|[171031](/rpg/inwazja/opowiesci/konspekty/171031-utracona-kontrola.html)|
|[Dalia Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-dalia-weiner.html)|1|[171101](/rpg/inwazja/opowiesci/konspekty/171101-magimedy-przed-epidemia.html)|
|[Bolesław Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-boleslaw-maus.html)|1|[171003](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|
|[Bogumił Miłoszept](/rpg/inwazja/opowiesci/karty-postaci/9999-bogumil-miloszept.html)|1|[171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Aneta Rukolas](/rpg/inwazja/opowiesci/karty-postaci/9999-aneta-rukolas.html)|1|[171101](/rpg/inwazja/opowiesci/konspekty/171101-magimedy-przed-epidemia.html)|
|[Andrzej Toporek](/rpg/inwazja/opowiesci/karty-postaci/9999-andrzej-toporek.html)|1|[171001](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html)|
|[Adam Kapelusz](/rpg/inwazja/opowiesci/karty-postaci/9999-adam-kapelusz.html)|1|[171024](/rpg/inwazja/opowiesci/konspekty/171024-detektyw-lecz-nie-sadecznego.html)|

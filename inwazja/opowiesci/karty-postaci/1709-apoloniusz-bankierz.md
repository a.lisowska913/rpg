---
layout: inwazja-karta-postaci
categories: profile
factions: "Bankierz - odłam Newerii, Eliksir Aerinus"
type: "NPC"
title: "Apoloniusz Bankierz"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **MRZ: Eliksir Aerinus odniesie sukces!**:
    * _Aspekty_: wszystko by Eliksir zarabiał więcej, ja i firma ważniejsi niż pracownicy, poszerzać segmenty klientów, promować innowacje, etyka jest drugorzędna wobec biznesu, przede wszystkim sprzedaż, "zawsze można sprzedać jeszcze prototyp"
    * _Opis_: Po sprawie z Eweliną Bankierz, Apoloniusz jest bardzo zdeterminowany, by Eliksir Aerinus odniósł sukces. Jako doświadczony sprzedawca wie, że najważniejsze jest portfolio klientów i backlog zadań do wykonania - i to próbuje maksymalizować.
* **MRZ: Apoloniusz Wielki, postać historyczna**: 
    * _Aspekty_: promuje kapitalizm, osłabia arystokrację, chce by o nim dobrze mówiono, chce by o nim dużo mówiono, łasy na pochwały, chce dać szansę innym Bankierzom
    * _Opis_: Do tej pory Apoloniuszowi większość rzeczy się udawało, niewielkim dla siebie kosztem. Apoloniusz uważa się za wielkiego szachistę na arenie politycznej i chce przejść do historii, najlepiej jako ten, który wykrył wiatr zmian od arystokracji do kapitalisty.
* **MET: Innowator chętny do eksperymentów**:
    * _Aspekty_: "spróbujmy tego sposobu", wczesny adopter, przerzucić ryzyko na kogoś innego, "jak długo się to opłaca...", chce być znany jako innowator, skłonny do sporego ryzyka
    * _Opis_: Apoloniusz liczy się z tym, że nie wszystko wychodzi. Jednak to tylko kwestia zarządzania ryzykiem - a najlepiej zrzucenia ryzyka na kogoś innego. Jest skłonny próbować i eksperymentować, nawet odnosić porażki - jak długo nie zniszczy to jego firmy

### Umiejętności

* **Polityk**:
    * _Aspekty_: arystokrata, śliski jak piskorz, charyzmatyczny mówca, rozkochiwanie w sobie dziewczyn, polityka na dworze, wbijanie noża w plecy, pozyskiwanie sojuszników celowych, etykieta na dworze, zauraczanie kontrahentów
    * _Opis_: Apoloniusz sprawia wrażenie bardzo miłego i sympatycznego, nikomu nie odmówi i każdemu pomoże. Nauczył się tego na dworze Newerji (jak i rozkochiwania młodszych dziewczyn). Jednak zawsze patrzy na korzyści i koszty; wie dobrze jak dostarczyć wartość innym i sobie.
* **Administrator**: 
    * _Aspekty_: rycerz litery prawa, "nie ma na mnie haków", "zgodnie z prawem to JEGO wina", "wolno nam to robić", zrzucanie ryzyka na kogoś innego, unikanie kłopotów formalnych, papiery zawsze w porządku
    * _Opis_: Apoloniusz jako jedną ze swoich dominujących zalet uważa to, że potrafi poruszać się w gąszczu prawnym i dzięki temu zawsze zabezpieczyć się przed problemami tej natury. Między zabezpieczeniem formalnym i okupowaniu się mafii, Apoloniusz jest raczej bezpieczny przed swoimi wrogami
* **Sprzedawca**:
    * _Aspekty_: manipulator, pozyskiwanie klientów, "sprzedam Ci nawet długopis", marketing emocji, badanie rynku, generowanie hype, budowanie opowieści, dobry PR
    * _Opis_: Apoloniusz skupia się na sprzedawaniu marzeń i swoich produktów jako marzenia. Jest właścicielem firmy idącym w marketing emocji - skupia się na dobrym PR, hype i pozyskiwaniu klientów; czasem ponad to, co Eliksir jest w stanie faktycznie wyprodukować. 
    
### Silne i słabe strony:

* **Między arystokracją a kapitalizmem**:
    * _Aspekty_: godny zaufania arystokrata, godny zaufania przedsiębiorca, nie należy do żadnego ze światów, potężni przeciwnicy w obu światach
    * _Opis_: Apoloniusz czerpie siłę z obu światów: kontakty i koneksje arystokracji i działania niezależne kapitalizmu. Jednak sprawia to, że nie jest w stanie stać się częścią żadnego z tych światów tak w pełni i że w obu światach ma potężnych przeciwników, którzy tylko czekają...
* ****: 
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

## Magia

### Szkoły magiczne

* **Technomancja**:
    * _Aspekty_: infomancja, szukanie rynków, odkrywanie co boli, "robienie taniej i szybciej", zdobywanie informacji o kontrahencie
    * _Opis_: Informacja to potęga w biznesie. Apoloniusz za jej pomocą jest w stanie lokalizować luki w rzeczywistości by móc formować oferty dla prospektów.
* **Magia mentalna**: 
    * _Aspekty_: wzmocnienie przekonywania, odkrywanie sekretów, ulojalnianie, zaklęcia miłosne, odkrywanie co boli, astralika, budowanie pozytywnego nastroju, diagnoza pola emocjonalnego, zagrzewanie ludzi, odkrywanie co boli
    * _Opis_: Wzmocnienie wrażenia? Trochę lepsze ceny? Pozyskanie informacji o stawkach na rynku? Bez obaw - Apoloniusz w tym jest szczególnie dobry. Identyfikacja aktualnego stanu nastrojów oraz na odpowiednim budowaniu wizji rzeczywistości tak, by wszystkim jak najbardziej pasowała. Też: gdzie jest największy ból i gdzie wpakować swoje eliksiry

### Zaklęcia statyczne

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 

## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Zarobki

* szef Eliksiru Aerinus
* niewielkie wsparcie Newerji Bankierz

### Znam

* **Członek rodu Bankierz, odłam Newerji**:
    * _Aspekty_: efektowne siedziby na Mazowszu, zapewnienie ciągłości pracy, zastrzyk gotówki czy reputacji, poręczenie arystokraty
    * _Opis_: Co nie mówić, Apoloniusz jest członkiem rodu Bankierz i Newerja uważa go za jednego z bardziej udanych. Apoloniusz ma możliwość korzystać z zasobów rodu i nie waha się tego używać
* **Firmy współpracujące na Mazowszu**:
    * _Aspekty_: kontrahenci, wsparcie zasobami w potrzebie, wejście do miejsc gdzie by nie mógł, poręczenie dobrej opinii
    * _Opis_: Eliksir Aerinus ma dobrą opinię jako firma i przez to Apoloniusz też jest traktowany dobrze. Ma otwarte kredyty, ma możliwość outsourcowania części pracy i może prosić o wsparcie czy odroczenia. Ogólnie, jest dobrze.
* ****:
    * _Aspekty_: 
    * _Opis_: 

### Mam

* **Prawnicy, mięśnie i eksperymenty**:
    * _Aspekty_: zabezpieczony przed roszczeniami, gra na czas i pieniądze, tajemniczy wandalizm w nocy, nieudany eksperyment coś rozwalił, plausible deniability
    * _Opis_: Apoloniusz większość problemów rozwiązuje przez udawanie niekompetencji. Dobry dział prawny zapewnia, że nigdy nie jest to jego wina. Ludzie Dukata zapewniają, że nie da się nic do niego przypiąć. A czasami, po prostu, eksperymenty się zwyczajnie nie udają...
* **Eliksiry i ich logistyka**:
    * _Aspekty_: bardzo odporne transportery, eliksiry robione przez speców, dostęp do składników i półproduktów, dostęp do półproduktów innych firm
    * _Opis_: Apoloniusz bardzo często sprzedaje swoje eliksiry firmom, które coś z nimi robią. Coś innego. Pomiędzy logistyką a tym faktem, Apoloniusz ma naprawdę sporo dostępu do różnych miejsc i zna procesy w różnych okolicznych firmach
* **Rodzina Dukata, jako osoba płacąca za ochronę**:
    * _Aspekty_: najemnicy, nielegalne pozyskiwanie, zastraszanie, mięśnie do bicia, odwracanie uwagi
    * _Opis_: Od samego już początku Apoloniusz zauważył, że w kwestii prawnej i legalnej poradzi sobie sam - ale jest jeszcze problem nielegalnych działań. Wykorzystuje więc Rodzinę Dukata jako najemników do rozwiązywania pewnych problemów, których sam by nie mógł rozwiązać legalnie.

# Opis

Twórca Eliksiru Aerinus, który wyrwał się spod kontroli Newerji Bankierz. Swego czasu rozkochał w sobie Ewelinę Bankierz i doprowadził do tego, że ona zainwestowała w niego sporo - po czym ją oszukał i za te pieniądze założył własną firmę. Wie, że ona mu tego nie zapomniała.

37 lat, najstarszy spośród byłych absztyfikantów Eweliny. Skupiony bardzo silny na swojej małej firmie i zdeterminowany, by przetrwała w burzy tej nowej, po-Karradraelowej rzeczywistości.

### Koncept

Trochę porządny przedsiębiorca a trochę Ronald Daggett, Apoloniusz jest jednocześnie postacią pomocną i chętną do współpracy i jednocześnie generatorem sesji. Problemy kierują się dookoła jego złego traktowania pracowników, chęci przejścia do historii, nieodpowiedzialnych eksperymentów (ze zrzucaniem ryzyka i konsekwencji) oraz jego lawirowaniem między Dukatem a Newerją.

Silny punkt niezależności między wszystkimi stronami, który rozpaczliwie próbuje zachować niezależność i dać wszystkim to, czego chcą.

### Motto

"Biznes to brudna sprawa - pozwól, że nasze eliksiry pozwolą Ci mieć czyste ręce"

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Kryzysowo tymczasowy dyktator](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|przeciwko niemu znaleziono dowody współpracy z mafią - i to takie "nieetyczne".|Wizja Dukata|

## Plany

|Misja|Plan|Kampania|
|-----|------|------|
|[Dlaczego magitrownia działa?](/rpg/inwazja/opowiesci/konspekty/171205-dlaczego-magitrownia-dziala.html)|szokowo rozszerzyć biznes / wejść na nowy typ produktu; potrzebuje do tego kapitału|Wizja Dukata|

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|171115|zarażony jakimś klątwożytem dezinihibicyjnym; Daniel go spowolnił a Kaja wykończyła.|[Kryzysowo tymczasowy dyktator](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|11/10/03|11/10/04|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171205|alchemik i biznesmen wchodzący na nowe rynki.|[Dlaczego magitrownia działa?](/rpg/inwazja/opowiesci/konspekty/171205-dlaczego-magitrownia-dziala.html)|10/12/15|10/12/17|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|170523|uczciwy biznesmen robiący w półproduktach alchemicznych; jego konstruminusa opętał duch. Współpracuje z Pauliną.|[Opętany konstruminus](/rpg/inwazja/opowiesci/konspekty/170523-opetany-konstruminus.html)|10/01/03|10/01/04|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Robert Sądeczny](/rpg/inwazja/opowiesci/karty-postaci/1709-robert-sadeczny.html)|2|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html), [171205](/rpg/inwazja/opowiesci/konspekty/171205-dlaczego-magitrownia-dziala.html)|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|2|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html), [170523](/rpg/inwazja/opowiesci/konspekty/170523-opetany-konstruminus.html)|
|[Jakub Dobrocień](/rpg/inwazja/opowiesci/karty-postaci/1709-jakub-dobrocien.html)|2|[171205](/rpg/inwazja/opowiesci/konspekty/171205-dlaczego-magitrownia-dziala.html), [170523](/rpg/inwazja/opowiesci/konspekty/170523-opetany-konstruminus.html)|
|[Łucja Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-lucja-maus.html)|1|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|
|[Zofia Murczówik](/rpg/inwazja/opowiesci/karty-postaci/9999-zofia-murczowik.html)|1|[170523](/rpg/inwazja/opowiesci/konspekty/170523-opetany-konstruminus.html)|
|[Wiaczesław Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-wiaczeslaw-zajcew.html)|1|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|
|[Sylwester Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-sylwester-bankierz.html)|1|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|
|[Natalia Kazuń](/rpg/inwazja/opowiesci/karty-postaci/1709-natalia-kazun.html)|1|[171205](/rpg/inwazja/opowiesci/konspekty/171205-dlaczego-magitrownia-dziala.html)|
|[Melodia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-melodia-diakon.html)|1|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|
|[Marcin Warinsky](/rpg/inwazja/opowiesci/karty-postaci/1709-marcin-warinsky.html)|1|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|
|[Karol Marzyciel](/rpg/inwazja/opowiesci/karty-postaci/1709-karol-marzyciel.html)|1|[171205](/rpg/inwazja/opowiesci/konspekty/171205-dlaczego-magitrownia-dziala.html)|
|[Kaja Maślaczek](/rpg/inwazja/opowiesci/karty-postaci/1709-kaja-maslaczek.html)|1|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|
|[Felicja Szampierz](/rpg/inwazja/opowiesci/karty-postaci/9999-felicja-szampierz.html)|1|[170523](/rpg/inwazja/opowiesci/konspekty/170523-opetany-konstruminus.html)|
|[Ewelina Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-ewelina-bankierz.html)|1|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|
|[Efemeryda Senesgradzka](/rpg/inwazja/opowiesci/karty-postaci/9999-efemeryda-senesgradzka.html)|1|[170523](/rpg/inwazja/opowiesci/konspekty/170523-opetany-konstruminus.html)|
|[Daniel Akwitański](/rpg/inwazja/opowiesci/karty-postaci/1709-daniel-akwitanski.html)|1|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|
|[Andrzej Farnolis](/rpg/inwazja/opowiesci/karty-postaci/9999-andrzej-farnolis.html)|1|[170523](/rpg/inwazja/opowiesci/konspekty/170523-opetany-konstruminus.html)|

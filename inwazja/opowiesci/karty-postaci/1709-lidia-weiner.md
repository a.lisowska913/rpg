---
layout: inwazja-karta-postaci
categories: profile
factions: "Srebrna Świeca"
type: "NPC"
title: "Lidia Weiner"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **FIL: z wielką mocą wiąże się wielka odpowiedzialność**:
    * _Aspekty_: etyka kieruje 
    * _Opis_: 
* **FIL: dążenie do samodoskonalenia**: 
    * _Aspekty_: 
    * _Opis_: to obowiązek
* **MET: kolekcjoner**:
    * _Aspekty_: warto to zachować bo jeszcze się przyda
    * _Opis_: 
* **MRZ: ród Mausów musi zostać zniszczony**:
    * _Aspekty_: 
    * _Opis_: 
	
### Umiejętności

* **psycholog**:
    * _Aspekty_: introspekcja, neuronautka, hipnoza, memetyka
    * _Opis_: 
* **farmaceuta**: 
    * _Aspekty_: farmacja psychodeliczna 
    * _Opis_: 
* **inżynier hipernetu**:
    * _Aspekty_: 
    * _Opis_: 
* **opieka nad wróżkami**: 
    * _Aspekty_: 
    * _Opis_: 
    
### Silne i słabe strony:

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

## Magia
### Szkoły magiczne

* **Magia mentalna**:
    * _Aspekty_: hipnotyczne iluzje, sterowanie używając impulsów
    * _Opis_: 
* **Magia zmysłów**: 
    * _Aspekty_: iluzje
    * _Opis_:  

### Zaklęcia statyczne

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 

## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Zarobki

* ?

### Znam

* **znacząca postać w Świecy**:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

### Mam

* **znana w rodzie Weiner**:
    * _Aspekty_: 
    * _Opis_: 
* **liczni wrogowie w rodzie Maus**:
    * _Aspekty_: 
    * _Opis_: 
* **artefaktorium neuronautyczne**:
    * _Aspekty_: 
    * _Opis_: 

# Opis

Lidia is one of top experts of introspection and understanding how human brain works. She specializes in anomalies when a mage slowly changes into a vicinius after he goes too far away and she has written several publications on this topic. Privately, she lives alone only with her fairy pets ("Meduza" i "Nimfa").

She strongly dislikes the Maus bloodline which results from her observing what Karradrael does to the minds of his subjects. Also, this comes from the history; she had a very promising student (Karolina Maus). She was sadly from a Maus bloodline and the current seiras, Jonatan Maus broke her.

She believes that evolution is the obligation of all sentient beings. To be the best one can be.
A very ethical person, unlike other Weiners.

### Koncept

Wysokopoziomowy koncept postaci, 1-2 zdania.

### Motto

"Tekst"

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Nieufność w małym mieście](/rpg/inwazja/opowiesci/konspekty/170212-nieufnosc-w-malym-miescie.html)|autoryzowana przez Andreę, została regentkę Świecy na Stawnię i okolicę.|Prawdziwa natura Draceny|
|[Patrol? Kralotyczne maskowanie!](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|silna niechęć do Rafaela Diakona|Powrót Karradraela|
|[Presja ze strony Czelimina](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html)|ma bardzo złą opinię o Silurii Diakon z uwagi na wydarzenia w Kompleksie Centralnym i Infensę|Powrót Karradraela|


## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170212|neuronautka i psycholog, która nie ma nic do Draceny i - wbrew sobie - skontaktowała się z Andreą a nawet została regentką Świecy dla Laetitii.|[Nieufność w małym mieście](/rpg/inwazja/opowiesci/konspekty/170212-nieufnosc-w-malym-miescie.html)|10/07/22|10/07/25|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|
|170101|she got broken (morale). Zaatakowała Rafaela ("defiler i potwór"), nie dała rady zamaskować sygnałów hipernetowych i opuściła z Kajetanem grupę Andrei.|[Patrol? Kralotyczne maskowanie!](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|10/07/13|10/07/15|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161229|trochę prowodyrka konfliktów - ZBYT idealistyczna. Atakuje Silurię ("gorzej niż defilerka") i Melodię + Wiolettę ("bronisz ją?").|[Presja ze strony Czelimina](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html)|10/07/08|10/07/10|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161204|postawiła z Rafaelem i Dalią Rudolfa, acz odcięła wyższe ośrodki myślenia; dowodzi 'starym nowym hipernetem' dla Andrei. Neuronautka rekalibrująca hipernet...|[Zajcewowie po drugiej stronie](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html)|10/07/05|10/07/07|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161120|niewiele może pomóc Rudolfowi, ale Dalię Weiner postawi błyskawicznie.|[Tak wygrywa się sojuszami](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html)|10/07/02|10/07/04|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161101|główny operator biomózgu i pomocniczy lekarz, zwłaszcza w kwestii * magii mentalnej i Andrei.|[Bezwzględna Lady Terminus](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|10/06/25|10/06/28|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161030|neuronautka ściągnięta by uratować Andreę przed deathspellem. Przy okazji uratowało ją to od porwania.|[Odbudowa dowodzenia Świecy](/rpg/inwazja/opowiesci/konspekty/161030-odbudowa-dowodzenia-swiecy.html)|10/06/22|10/06/24|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160417|nienawidząca Mausów od czasu gdy Karradrael na żądanie Jonatana Mausa zniszczył Karolinę Maus.|[Symptomy kryzysu Świecy](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|10/06/16|10/06/17|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Mieszko Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-mieszko-bankierz.html)|7|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161229](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html), [161030](/rpg/inwazja/opowiesci/konspekty/161030-odbudowa-dowodzenia-swiecy.html), [160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
|[Andrea Wilgacz](/rpg/inwazja/opowiesci/karty-postaci/1709-andrea-wilgacz.html)|7|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161229](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html), [161030](/rpg/inwazja/opowiesci/konspekty/161030-odbudowa-dowodzenia-swiecy.html), [160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
|[Rafael Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-rafael-diakon.html)|6|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161229](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html), [161030](/rpg/inwazja/opowiesci/konspekty/161030-odbudowa-dowodzenia-swiecy.html)|
|[Kajetan Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-kajetan-weiner.html)|6|[170212](/rpg/inwazja/opowiesci/konspekty/170212-nieufnosc-w-malym-miescie.html), [170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html), [161030](/rpg/inwazja/opowiesci/konspekty/161030-odbudowa-dowodzenia-swiecy.html)|
|[Tadeusz Baran](/rpg/inwazja/opowiesci/karty-postaci/1709-tadeusz-baran.html)|5|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html), [161030](/rpg/inwazja/opowiesci/konspekty/161030-odbudowa-dowodzenia-swiecy.html), [160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
|[Julian Pszczelak](/rpg/inwazja/opowiesci/karty-postaci/1709-julian-pszczelak.html)|5|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161229](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|[Marian Łajdak](/rpg/inwazja/opowiesci/karty-postaci/1709-marian-lajdak.html)|4|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|[Marian Agrest](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-agrest.html)|4|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161229](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html), [161030](/rpg/inwazja/opowiesci/konspekty/161030-odbudowa-dowodzenia-swiecy.html)|
|[Andżelika Leszczyńska](/rpg/inwazja/opowiesci/karty-postaci/1709-andzelika-leszczynska.html)|4|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|[Rudolf Jankowski](/rpg/inwazja/opowiesci/karty-postaci/9999-rudolf-jankowski.html)|3|[161229](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|[Melodia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-melodia-diakon.html)|3|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161229](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html)|
|[Laragnarhag](/rpg/inwazja/opowiesci/karty-postaci/1709-laragnarhag.html)|3|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161229](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html)|
|[Draconis Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-draconis-diakon.html)|3|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161229](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html)|
|[Dalia Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-dalia-weiner.html)|3|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html)|
|[Świeży Lilak](/rpg/inwazja/opowiesci/karty-postaci/9999-swiezy-lilak.html)|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html)|
|[Wioletta Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-wioletta-bankierz.html)|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161229](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html)|
|[Tatiana Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-tatiana-zajcew.html)|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161229](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html)|
|[Stalowy Śledzik Żarłacz](/rpg/inwazja/opowiesci/karty-postaci/9999-stalowy-sledzik-zarlacz.html)|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html)|
|[Laurena Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-laurena-bankierz.html)|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
|[Katalina Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-katalina-bankierz.html)|2|[161229](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html), [160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
|[Joachim Kopiec](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-kopiec.html)|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|[Franciszek Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-myszeczka.html)|2|[161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html)|
|[Fortitia Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-fortitia-diakon.html)|2|[161030](/rpg/inwazja/opowiesci/konspekty/161030-odbudowa-dowodzenia-swiecy.html), [160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
|[Anna Kozak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kozak.html)|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|[Aneta Rainer](/rpg/inwazja/opowiesci/karty-postaci/1709-aneta-rainer.html)|2|[161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html)|
|[Alojzy Przylaz](/rpg/inwazja/opowiesci/karty-postaci/1803-alojzy-przylaz.html)|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|[Zuzanna Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-zuzanna-maus.html)|1|[160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
|[Wojciech Żądło](/rpg/inwazja/opowiesci/karty-postaci/9999-wojciech-zadlo.html)|1|[160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
|[Rufus Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-rufus-maus.html)|1|[160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
|[Rodion Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-rodion-zajcew.html)|1|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|1|[170212](/rpg/inwazja/opowiesci/konspekty/170212-nieufnosc-w-malym-miescie.html)|
|[Mordecja Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-mordecja-diakon.html)|1|[160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
|[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/1709-maria-newa.html)|1|[170212](/rpg/inwazja/opowiesci/konspekty/170212-nieufnosc-w-malym-miescie.html)|
|[Malia Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-malia-bankierz.html)|1|[160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
|[Laetitia Gaia Rasputin Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-laetitia-gaia-rasputin-weiner.html)|1|[170212](/rpg/inwazja/opowiesci/konspekty/170212-nieufnosc-w-malym-miescie.html)|
|[Kurt Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-kurt-weiner.html)|1|[170212](/rpg/inwazja/opowiesci/konspekty/170212-nieufnosc-w-malym-miescie.html)|
|[Kirył Sjeld](/rpg/inwazja/opowiesci/karty-postaci/9999-kiryl-sjeld.html)|1|[161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html)|
|[Jolanta Sowińska](/rpg/inwazja/opowiesci/karty-postaci/1709-jolanta-sowinska.html)|1|[161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html)|
|[Janusz Krzykoł](/rpg/inwazja/opowiesci/karty-postaci/9999-janusz-krzykol.html)|1|[170212](/rpg/inwazja/opowiesci/konspekty/170212-nieufnosc-w-malym-miescie.html)|
|[Jan Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-weiner.html)|1|[160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
|[Jacek Molenda](/rpg/inwazja/opowiesci/karty-postaci/9999-jacek-molenda.html)|1|[161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html)|
|[Felicjan Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-felicjan-weiner.html)|1|[161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|[Elea Maus](/rpg/inwazja/opowiesci/karty-postaci/1802-elea-maus.html)|1|[160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|1|[170212](/rpg/inwazja/opowiesci/konspekty/170212-nieufnosc-w-malym-miescie.html)|
|[Deiiw Podniebny Grom](/rpg/inwazja/opowiesci/karty-postaci/9999-deiiw-podniebny-grom.html)|1|[161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html)|
|[Dagmara Wyjątek](/rpg/inwazja/opowiesci/karty-postaci/1709-dagmara-wyjątek.html)|1|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|
|[Baltazar Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-baltazar-maus.html)|1|[160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
|[Artur Żupan](/rpg/inwazja/opowiesci/karty-postaci/1803-artur-zupan.html)|1|[160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
|[Aleksander Sowiński](/rpg/inwazja/opowiesci/karty-postaci/9999-aleksander-sowinski.html)|1|[160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|

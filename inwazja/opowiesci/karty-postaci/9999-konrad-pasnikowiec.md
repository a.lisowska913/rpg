---
layout: inwazja-karta-postaci
categories: profile
title: "Konrad Paśnikowiec"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170510|starszy, samotny człowiek mieszkający w Głębiwiązie; pacjent Pauliny, która go czasami odwiedza.|[Najgorsze love story](/rpg/inwazja/opowiesci/konspekty/170510-najgorsze-love-story.html)|10/01/16|10/01/19|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|1|[170510](/rpg/inwazja/opowiesci/konspekty/170510-najgorsze-love-story.html)|
|[Hektor Reszniaczek](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-reszniaczek.html)|1|[170510](/rpg/inwazja/opowiesci/konspekty/170510-najgorsze-love-story.html)|
|[Ewelina Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-ewelina-bankierz.html)|1|[170510](/rpg/inwazja/opowiesci/konspekty/170510-najgorsze-love-story.html)|
|[Diana Łuczkiewicz](/rpg/inwazja/opowiesci/karty-postaci/9999-diana-luczkiewicz.html)|1|[170510](/rpg/inwazja/opowiesci/konspekty/170510-najgorsze-love-story.html)|

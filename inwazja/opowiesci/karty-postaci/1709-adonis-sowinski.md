---
layout: inwazja-karta-postaci
categories: profile
factions: "Dare Shiver, Srebrna Świeca"
type: "NPC"
title: "Adonis Sowiński"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **FIL: Daredevil**: 
    * _Aspekty_: prankster, rywalizacja
    * _Opis_: "Jeśli nic nie czujesz... to nie żyjesz. Zawsze wybieraj najciekawsze rozwiązanie. Nadmiar powagi i patrzenia na świat przez krawat zabija. Nie powinno się unikać zrobienia dobrego żartu ;-). Bankierze, Zajcewowie, inni Sowińscy... zawsze warto im pokazać, że można pójść ten jeden krok dalej."
* **FIL: Hedonista**: 
    * _Aspekty_: otwarty, apolityczny, kocha piękno
    * _Opis_: "Przyjemność jest najwyższym celem egzystencji - rozprzestrzenianie tej przyjemności to obowiązek. Wszystkiego należy spróbować. Nie mnie oceniać. Bądź czym chcesz i rób co chcesz. Te wszystkie tematy związane z gildiami i frakcjami... to nieistotne. Wszyscy i tak umrzemy. Kiedyś.Nie ma nic doskonalszego, niż po zastrzyku adrenaliny obcować z czymś naprawdę pięknym"


### Umiejętności

* **Przywódca**:
    * _Aspekty_: inspirowanie, odwracanie uwagi, skłanianie do ryzyka
    * _Opis_: Mała dywersja, przygotowanie planu, odrobina chaosu i już może działać swobodnie tam, gdzie mu to potrzebne. Mało kto tak jak Adonis potrafi namówić innych do wybrania bardziej ryzykownej akcji.
* **Guru sekty**: 
    * _Aspekty_: corruptor, aktorstwo, bullshit
    * _Opis_: Adonis potrafi nawet zmienić system wartości innych i skłonić ich do dołączenia do miłośników ryzyka. Niesamowite, jak łatwo jest zachowywać się inaczej, przebrać się za kobietę itp. gdy jest się bishounenem ;-) Nie tylko potrafi się wykpić ale i wmówić innym wiele różnych rzeczy. Dla niego prawda to tylko punkt widzenia.
* **Uwodziciel**:
    * _Aspekty_: dbanie o urodę
    * _Opis_: Adonis zasłużył na swoje imię obsesyjnym wręcz przywiązaniem do swej urody, doboru strojów... jego uroda oraz charyzma bez większego problemu potrafi oczarować i pociągnąć za sobą innych...
* **Daredevil**:
    * _Aspekty_: hazardzista, włamywacz, nieuchwytny, gry karciane
    * _Opis_: Zna bardzo wiele gier hazardowych, miejsca, gdzie te gry się toczą oraz jest praktykującym graczem, a jak potrzebny jest dreszczyk emocji, odwiedzi kogoś w nocy czy zostawi ślad "Stefan tu był" czy coś. Praktycznie nie da się nigdy nic na niego znaleźć ani nie da się udowodnić, że coś zrobił czy zadziałał.
    
### Silne i słabe strony:

* **Bishounen**:
    * _Aspekty_: 
    * _Opis_: Chętnie i łatwo przebiera się za kobietę, ale czasem ciężko mu pokazać swoją męskość

## Magia

### Szkoły magiczne

* **Magia zmysłów**:
    * _Aspekty_: magia imprezowa
    * _Opis_: 
* **Magia mentalna**:
    * _Aspekty_:
    * _Opis_: 	
* **Technomancja**:
    * _Aspekty_: infomancja, magia imprezowa
    * _Opis_: 

### Zaklęcia statyczne

* ****:
    * _Aspekty_: 
    * _Opis_: 

	
## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Zarobki

* standardowo; żyje na wysokim poziomie, więc jego bogactwo nie jest widoczne

### Znam

* **Prezydent klubu "Dare Shiver" ("odważ się drżeć")**:
    * _Aspekty_: magowie, którym pomógł 'odnaleźć siebie', kompromitujące materiały, zaprzyjaźnione miejsca na Śląsku (bezpieczne), eliksiry i gry imprezowe (bezpieczne), wszechstronne przebrania i stroje
    * _Opis_: Ma licznych akolitów i miłośników, ale też sympatyków, którzy TEŻ BY CHCIELI. Praktycznie jest w stanie zaszantażować i skompromitować sporo osób... tyle, że nie chce. Z uwagi na swoją hojność i działalność, jest miło widziany w wielu miejscach. Standardowy sprzęt potrzebny do funkcjonowania Dare Shiver i rozweselania ludzi - czy robienia im dowcipów. Strój na każdą okazję. Może być każdym, gdy tylko mu to potrzebne czy przydatne
* **Członek wielu klubów**:
    * _Aspekty_: Klub Historyczny Szczegół (historyczny), Klub Hipotetyczny Tesseract (eksperymentalna magia)
    * _Opis_: Andżelika funduje, wspiera i działa w ramach wielu klubów w świecie magicznym. Jedyny klub, którego unika jak ognia to Dare Shiver.

### Mam

* **Sława wybitnego hazardzisty**:
    * _Aspekty_: 
    * _Opis_: są miejsca, gdzie z nim nawet nie chcą grać :-(. A przynajmniej nie na odpowiednie stawki
* **Mapy i informacje**:
    * _Aspekty_: 
    * _Opis_: Mało kto tak jak on ma dostęp do informacji o miejscach, systemach obronnych, z kim co można... 

# Opis

Adonis - a beautiful and carefree bishounen. Or rather, what he would like others to think about him. He is extremely beautiful to the point is often considered to be one of Diakon bloodline, however behind the beautiful visage lies a competitive daredevil.

This mage is completely apolitical. He doesn’t care about politics, guilds etc. he loves the beauty and loves – absolutely loves – the feel of adrenaline. He is also a prankster and likes putting others in embarrassing positions, although does not want to hurt anyone. Everything he does it’s supposed to be amazing experiences for everyone - especially in retrospect. A golden youth.

A very good gambler and risk taker, incredibly difficult to catch and a president of the “Dare Shiver” club, this one – although not dangerous in terms of meaning of the word – is known to be sometimes irritating.

In terms of magic, he specializes in the beauty magic, party magic and illusion magic; especially in the regions which allow him to stay undetected, observe, never get caught.

## Motto

"Gdyby kózka nie skakała... by niczego nie wygrała ;-). Życie bez adrenaliny to jak śmierć za życia."

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Ratując syrenopnącze](/rpg/inwazja/opowiesci/konspekty/170417-ratujac-syrenopnacze.html)|powiązany z organizacją gigantycznej imprezy erotycznej wraz z Silurią Diakon|Powrót Karradraela|


## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170417|mistrz ceremonii i współorganizator gigantycznej imprezy erotycznej o której będzie się mówić...|[Ratując syrenopnącze](/rpg/inwazja/opowiesci/konspekty/170417-ratujac-syrenopnacze.html)|10/03/05|10/03/12|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170115|prezydent Dare Shiver; wyjaśnił Silurii jak ten klub działa oraz musiał opanować sytuację po tym, jak Siluria zrobiła striptiz w ludzkiej części klubu ;-).|[Klub Dare Shiver](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|10/02/22|10/02/25|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|2|[170417](/rpg/inwazja/opowiesci/konspekty/170417-ratujac-syrenopnacze.html), [170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|[Rafael Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-rafael-diakon.html)|1|[170417](/rpg/inwazja/opowiesci/konspekty/170417-ratujac-syrenopnacze.html)|
|[Netheria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-netheria-diakon.html)|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|[Kornelia Kartel](/rpg/inwazja/opowiesci/karty-postaci/9999-kornelia-kartel.html)|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|[Karolina Kupiec](/rpg/inwazja/opowiesci/karty-postaci/1803-karolina-kupiec.html)|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|[Judyta Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-judyta-maus.html)|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|[Jolanta Sowińska](/rpg/inwazja/opowiesci/karty-postaci/1709-jolanta-sowinska.html)|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|[Joachim Kopiec](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-kopiec.html)|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|[Joachim Kartel](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-kartel.html)|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|[Jan Wątły](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-watly.html)|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|[Infernia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-infernia-diakon.html)|1|[170417](/rpg/inwazja/opowiesci/konspekty/170417-ratujac-syrenopnacze.html)|
|[Ignat Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1802-ignat-zajcew.html)|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|[Elizawieta Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-elizawieta-zajcew.html)|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|[Bogumił Rojowiec](/rpg/inwazja/opowiesci/karty-postaci/9999-bogumil-rojowiec.html)|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|[Balrog Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-balrog-bankierz.html)|1|[170115](/rpg/inwazja/opowiesci/konspekty/170115-klub-dare-shiver.html)|
|[Andżelika Leszczyńska](/rpg/inwazja/opowiesci/karty-postaci/1709-andzelika-leszczynska.html)|1|[170417](/rpg/inwazja/opowiesci/konspekty/170417-ratujac-syrenopnacze.html)|
|[Akumulator Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-akumulator-diakon.html)|1|[170417](/rpg/inwazja/opowiesci/konspekty/170417-ratujac-syrenopnacze.html)|

---
layout: inwazja-karta-postaci
categories: profile
title: "Aleksander Czykomar"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170518|bezwzględny detektyw działający u Dukata; dał się przekonać Paulinie i powiedział, że pracuje nad tym, czy rodzina Diany to prawdziwa rodzina.|[Machinacje maga rolniczego](/rpg/inwazja/opowiesci/konspekty/170518-machinacje-maga-rolniczego.html)|10/01/23|10/01/25|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|1|[170518](/rpg/inwazja/opowiesci/konspekty/170518-machinacje-maga-rolniczego.html)|
|[Jakub Dobrocień](/rpg/inwazja/opowiesci/karty-postaci/1709-jakub-dobrocien.html)|1|[170518](/rpg/inwazja/opowiesci/konspekty/170518-machinacje-maga-rolniczego.html)|
|[Hektor Reszniaczek](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-reszniaczek.html)|1|[170518](/rpg/inwazja/opowiesci/konspekty/170518-machinacje-maga-rolniczego.html)|
|[Grazoniusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-grazoniusz-bankierz.html)|1|[170518](/rpg/inwazja/opowiesci/konspekty/170518-machinacje-maga-rolniczego.html)|
|[Ewelina Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-ewelina-bankierz.html)|1|[170518](/rpg/inwazja/opowiesci/konspekty/170518-machinacje-maga-rolniczego.html)|
|[Efraim Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-efraim-weiner.html)|1|[170518](/rpg/inwazja/opowiesci/konspekty/170518-machinacje-maga-rolniczego.html)|
|[Diana Łuczkiewicz](/rpg/inwazja/opowiesci/karty-postaci/9999-diana-luczkiewicz.html)|1|[170518](/rpg/inwazja/opowiesci/konspekty/170518-machinacje-maga-rolniczego.html)|

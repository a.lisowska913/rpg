---
layout: inwazja-karta-postaci
categories: profile
factions: "Niezrzeszeni"
type: "PC - temp"
title: "Aleksander Tomaszewski"
---
# {{ page.title }}

## Postać

### Motywacje (do czego dąży)

* **FIL: Pragmatyk**:
    * _Aspekty_: łatwiej korzystać z innych ludzi gdy cię lubią
    * _Opis_: 
* **BÓL: Odrzucony śledczy**:
    * _Aspekty_: paranoja wobec władzy
    * _Opis_: nie przyjęli go na informatykę śledczą
* **MET: Ryzykant**:
    * _Aspekty_: lubi rzeczy niestandardowe i dające dreszczyk emocji
    * _Opis_: 
	
### Umiejętności

* **Informatyk**:
    * _Aspekty_: 
	iInformatyk śledczy, kradzież danych
    * _Opis_: 
* **Nauczyciel**:
    * _Aspekty_: informatyka, dobry kontakt z młodzieżą
    * _Opis_: 
* **Con artist**:
    * _Aspekty_: 
    * _Opis_: 
	
### Silne i słabe strony:

* **Marzyciel**:
    * _Aspekty_: 
    * _Opis_: tęsknota za światem, jaki mógł być - ale nie dostał się na śledczą

	
## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Zarobki

* nauczyciel
* dorabia fuchami informatycznymi

### Znam

* **lokalne siły prawa i porządku**:
    * _Aspekty_: 
    * _Opis_: 
* **młodzież w Okólniczu**:
    * _Aspekty_: 
    * _Opis_: 
* **nauczyciele i rodzice z Okólnicza**:
    * _Aspekty_: 
    * _Opis_: 	
	
### Mam

* **sprzęt w pracowni komputerowej**:
    * _Aspekty_: 
    * _Opis_: 
* **dobrej klasy komputer i oprogramowanie 'śledcze'**:
    * _Aspekty_: 
    * _Opis_: 


# Opis
nauczyciel informatyki z Okólnicza

Aleksander Tomaszewski – nauczyciel informatyki z liceum Ireny. Underachiever, ale inteligentny. Nauczycielem nie został z powołania, ale z braku pomysłu na siebie, udaje mu się jednak utrzymać dobry kontakt z młodzieżą, a rodzice ufają jego dobrym intencjom. Jest wychowawcą klasy, do której uczęszcza/ uczęszczała Irena. Wiek 30 lat. Kawaler, typ samotny. Z jakiegoś powodu upodobał sobie w klasie Irenę i chce jej pomóc. Nie chce angażować policji, ponieważ boi się o zdrowie i życie Ireny, podczas gdy policja w Polsce jest zazwyczaj nieudolna i rozwlekła, dodatkowo Aleksander żyje w przeświadczeniu, że organy państwa chcą dla niego jak najgorzej i nie ufa im. Oprócz tego ma też egoistyczny powód, liczy że wprowadzi to do jego życia trochę odmiany i ekscytacji, kiedyś chciał pracować w informatyce śledczej, ale był za słaby na studiach.

Traktuje Irenę jak pupilka, sprawia mu przyjemność interakcja z młodą sympatyczną dziewczyną która wyraźnie mu ufa. Nic zdrożnego.

### Koncept

Wysokopoziomowy koncept postaci, 1-2 zdania.

### Motto

"Tekst"

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|160809|doprowadził policję do faktycznych zbrodniarzy, googlował po nocach i się nikomu nie kłaniał. O dziwo, przeżył.|[Awokado Dla Wampira](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|10/05/07|10/05/09|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Leopold Teściak](/rpg/inwazja/opowiesci/karty-postaci/9999-leopold-tesciak.html)|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|
|[Karolina Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-karolina-maus.html)|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|
|[Karina von Blutwurst](/rpg/inwazja/opowiesci/karty-postaci/9999-karina-von-blutwurst.html)|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|
|[Jolanta Lipińska](/rpg/inwazja/opowiesci/karty-postaci/9999-jolanta-lipinska.html)|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|
|[Izabela Bąk](/rpg/inwazja/opowiesci/karty-postaci/9999-izabela-bak.html)|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|
|[Irena Paniszok](/rpg/inwazja/opowiesci/karty-postaci/9999-irena-paniszok.html)|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|
|[Filip Czumko](/rpg/inwazja/opowiesci/karty-postaci/9999-filip-czumko.html)|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|
|[Elżbieta Paniszok](/rpg/inwazja/opowiesci/karty-postaci/9999-elzbieta-paniszok.html)|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|
|[Damian Paniszok](/rpg/inwazja/opowiesci/karty-postaci/9999-damian-paniszok.html)|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|
|[Antonina Brzeszcz](/rpg/inwazja/opowiesci/karty-postaci/1709-antonina-brzeszcz.html)|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|
|[Adolphus von Blutwurst](/rpg/inwazja/opowiesci/karty-postaci/9999-adolphus-von-blutwurst.html)|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|

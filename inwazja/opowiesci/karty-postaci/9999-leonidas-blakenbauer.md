---
layout: inwazja-karta-postaci
categories: profile
title: "Leonidas Blakenbauer"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|160420|który wymyślił jak NIE złamać dwóch całkowicie sprzecznych ze sobą sojuszy. Też: zmontował nagonkę na Bankierzy by złapać Ozydiusza.|[Kolizja dwóch sojuszy](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html)|10/07/01|10/07/02|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160406|który negocjował sojusz z Millennium mający podzielić Kopalin na wpływy.|[Najprawdziwszy sojusz Blakenbauerów](/rpg/inwazja/opowiesci/konspekty/160406-najprawdziwszy-sojusz-blakenbauerow.html)|10/06/29|10/06/30|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160316|który wynegocjował Hektorem wstrzyknięcie Marcelina pod Ozydiusza i ogólnie doprowadził Blakenbauerów do większego poważania.|[Frontalne wejście Millennium](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html)|10/06/27|10/06/28|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160309|nokautujący szpinakowym pierogiem Judytę, porzucający Hektora limuzyną i straszący * magów impotencją by opanować epidemię Irytki.|[Irytka Sprzężona](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|10/06/25|10/06/26|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160224|Marcelin, niszczący dobrą reputację Marcelina i manipulujący Wandą, Marcelinem i wydarzeniami.|[Aniołki Marcelina](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|10/06/23|10/06/24|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160217|the puppetmaster, który wyrolował sojuszników, przeciwników i postronnych.|[Sojusz według Leonidasa](/rpg/inwazja/opowiesci/konspekty/160217-sojusz-wedlug-leonidasa.html)|10/06/06|10/06/07|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160210|siła napędowa wojny gangów, zdobywał informacje oraz siał zamęt.|[Batmag Uderza!](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html)|10/06/04|10/06/05|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160128|skutecznie udający prawnika i rozpoznający dokumenty prawdziwe od fałszywych. Też: podaje kończyny Edwinowi.|[Byli sobie przestępcy](/rpg/inwazja/opowiesci/konspekty/160128-byli-sobie-przestepcy.html)|10/06/02|10/06/03|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160202|który zdominował zbuntowanego pająka fazowego i rozwiązał konflikt między Sowińskim i Bankierzówną.|[Wolność pająka fazowego](/rpg/inwazja/opowiesci/konspekty/160202-wolnosc-pajaka-fazowego.html)|10/05/09|10/05/10|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Klara Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-klara-blakenbauer.html)|7|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [160406](/rpg/inwazja/opowiesci/konspekty/160406-najprawdziwszy-sojusz-blakenbauerow.html), [160316](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html), [160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html), [160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html), [160202](/rpg/inwazja/opowiesci/konspekty/160202-wolnosc-pajaka-fazowego.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|6|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [160406](/rpg/inwazja/opowiesci/konspekty/160406-najprawdziwszy-sojusz-blakenbauerow.html), [160316](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html), [160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html), [160128](/rpg/inwazja/opowiesci/konspekty/160128-byli-sobie-przestepcy.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|4|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [160217](/rpg/inwazja/opowiesci/konspekty/160217-sojusz-wedlug-leonidasa.html), [160128](/rpg/inwazja/opowiesci/konspekty/160128-byli-sobie-przestepcy.html), [160202](/rpg/inwazja/opowiesci/konspekty/160202-wolnosc-pajaka-fazowego.html)|
|[Alina Bednarz](/rpg/inwazja/opowiesci/karty-postaci/1803-alina-bednarz.html)|4|[160406](/rpg/inwazja/opowiesci/konspekty/160406-najprawdziwszy-sojusz-blakenbauerow.html), [160217](/rpg/inwazja/opowiesci/konspekty/160217-sojusz-wedlug-leonidasa.html), [160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html), [160128](/rpg/inwazja/opowiesci/konspekty/160128-byli-sobie-przestepcy.html)|
|[Ozydiusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-ozydiusz-bankierz.html)|3|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [160316](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html), [160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Marcelin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-marcelin-blakenbauer.html)|3|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [160316](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html), [160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|
|[Kleofas Bór](/rpg/inwazja/opowiesci/karty-postaci/1709-kleofas-bor.html)|3|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html), [160128](/rpg/inwazja/opowiesci/konspekty/160128-byli-sobie-przestepcy.html)|
|[Tymotheus Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-tymotheus-blakenbauer.html)|2|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|
|[Szymon Skubny](/rpg/inwazja/opowiesci/karty-postaci/9999-szymon-skubny.html)|2|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html), [160128](/rpg/inwazja/opowiesci/konspekty/160128-byli-sobie-przestepcy.html)|
|[Patrycja Krowiowska](/rpg/inwazja/opowiesci/karty-postaci/1709-patrycja-krowiowska.html)|2|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html)|
|[Pafnucy Zieczar](/rpg/inwazja/opowiesci/karty-postaci/9999-pafnucy-zieczar.html)|2|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html), [160128](/rpg/inwazja/opowiesci/konspekty/160128-byli-sobie-przestepcy.html)|
|[Olga Miodownik](/rpg/inwazja/opowiesci/karty-postaci/1709-olga-miodownik.html)|2|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [160406](/rpg/inwazja/opowiesci/konspekty/160406-najprawdziwszy-sojusz-blakenbauerow.html)|
|[Oktawian Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-oktawian-maus.html)|2|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [160217](/rpg/inwazja/opowiesci/konspekty/160217-sojusz-wedlug-leonidasa.html)|
|[Margaret Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-margaret-blakenbauer.html)|2|[160316](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html), [160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Kinga Melit](/rpg/inwazja/opowiesci/karty-postaci/9999-kinga-melit.html)|2|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [160128](/rpg/inwazja/opowiesci/konspekty/160128-byli-sobie-przestepcy.html)|
|[Judyta Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-judyta-maus.html)|2|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [160217](/rpg/inwazja/opowiesci/konspekty/160217-sojusz-wedlug-leonidasa.html)|
|[Joachim Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-zajcew.html)|2|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|
|[Emilia Kołatka](/rpg/inwazja/opowiesci/karty-postaci/1709-emilia-kolatka.html)|2|[160316](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html), [160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Elea Maus](/rpg/inwazja/opowiesci/karty-postaci/1802-elea-maus.html)|2|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Dionizy Kret](/rpg/inwazja/opowiesci/karty-postaci/1709-dionizy-kret.html)|2|[160406](/rpg/inwazja/opowiesci/konspekty/160406-najprawdziwszy-sojusz-blakenbauerow.html), [160217](/rpg/inwazja/opowiesci/konspekty/160217-sojusz-wedlug-leonidasa.html)|
|[Diana Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-diana-weiner.html)|2|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [160128](/rpg/inwazja/opowiesci/konspekty/160128-byli-sobie-przestepcy.html)|
|[Dagmara Wyjątek](/rpg/inwazja/opowiesci/karty-postaci/1709-dagmara-wyjątek.html)|2|[160316](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html), [160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Borys Kumin](/rpg/inwazja/opowiesci/karty-postaci/1709-borys-kumin.html)|2|[160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html), [160202](/rpg/inwazja/opowiesci/konspekty/160202-wolnosc-pajaka-fazowego.html)|
|[Bogdan Kimaroj](/rpg/inwazja/opowiesci/karty-postaci/9999-bogdan-kimaroj.html)|2|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html), [160128](/rpg/inwazja/opowiesci/konspekty/160128-byli-sobie-przestepcy.html)|
|[Amanda Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-amanda-diakon.html)|2|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [160316](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html)|
|[Zenon Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-zenon-weiner.html)|1|[160217](/rpg/inwazja/opowiesci/konspekty/160217-sojusz-wedlug-leonidasa.html)|
|[Wiktor Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1709-wiktor-sowinski.html)|1|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html)|
|[Tomasz Jamnik](/rpg/inwazja/opowiesci/karty-postaci/9999-tomasz-jamnik.html)|1|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html)|
|[Stanisław Pormien](/rpg/inwazja/opowiesci/karty-postaci/9999-stanislaw-pormien.html)|1|[160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|
|[Salazar Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-salazar-bankierz.html)|1|[160217](/rpg/inwazja/opowiesci/konspekty/160217-sojusz-wedlug-leonidasa.html)|
|[Rufus Czubek](/rpg/inwazja/opowiesci/karty-postaci/9999-rufus-czubek.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|1|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html)|
|[Pasożyt Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-pasozyt-diakon.html)|1|[160316](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html)|
|[Nikodem Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1709-nikodem-sowinski.html)|1|[160202](/rpg/inwazja/opowiesci/konspekty/160202-wolnosc-pajaka-fazowego.html)|
|[Netheria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-netheria-diakon.html)|1|[160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|
|[Laurena Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-laurena-bankierz.html)|1|[160202](/rpg/inwazja/opowiesci/konspekty/160202-wolnosc-pajaka-fazowego.html)|
|[Klemens X](/rpg/inwazja/opowiesci/karty-postaci/9999-klemens-x.html)|1|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html)|
|[Karol Poczciwiec](/rpg/inwazja/opowiesci/karty-postaci/9999-karol-poczciwiec.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Kajetan Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-kajetan-weiner.html)|1|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html)|
|[Judyta Karnisz](/rpg/inwazja/opowiesci/karty-postaci/9999-judyta-karnisz.html)|1|[160316](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html)|
|[Jolanta Sowińska](/rpg/inwazja/opowiesci/karty-postaci/1709-jolanta-sowinska.html)|1|[160406](/rpg/inwazja/opowiesci/konspekty/160406-najprawdziwszy-sojusz-blakenbauerow.html)|
|[Jerzy Gurlacz](/rpg/inwazja/opowiesci/karty-postaci/9999-jerzy-gurlacz.html)|1|[160406](/rpg/inwazja/opowiesci/konspekty/160406-najprawdziwszy-sojusz-blakenbauerow.html)|
|[Igor Daczyn](/rpg/inwazja/opowiesci/karty-postaci/9999-igor-daczyn.html)|1|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html)|
|[Ignat Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1802-ignat-zajcew.html)|1|[160202](/rpg/inwazja/opowiesci/konspekty/160202-wolnosc-pajaka-fazowego.html)|
|[Hubert Rębski](/rpg/inwazja/opowiesci/karty-postaci/9999-hubert-rebski.html)|1|[160128](/rpg/inwazja/opowiesci/konspekty/160128-byli-sobie-przestepcy.html)|
|[Felicjan Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-felicjan-weiner.html)|1|[160406](/rpg/inwazja/opowiesci/konspekty/160406-najprawdziwszy-sojusz-blakenbauerow.html)|
|[Estrella Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-estrella-diakon.html)|1|[160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|
|[Elizawieta Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-elizawieta-zajcew.html)|1|[160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|
|[Dagmara Czeluść](/rpg/inwazja/opowiesci/karty-postaci/9999-dagmara-czelusc.html)|1|[160224](/rpg/inwazja/opowiesci/konspekty/160224-aniolki-marcelina.html)|
|[Bolesław Derwisz](/rpg/inwazja/opowiesci/karty-postaci/9999-boleslaw-derwisz.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Arkadiusz Klusiński](/rpg/inwazja/opowiesci/karty-postaci/9999-arkadiusz-klusinski.html)|1|[160128](/rpg/inwazja/opowiesci/konspekty/160128-byli-sobie-przestepcy.html)|
|[Arazille](/rpg/inwazja/opowiesci/karty-postaci/9999-arazille.html)|1|[160202](/rpg/inwazja/opowiesci/konspekty/160202-wolnosc-pajaka-fazowego.html)|
|[Anna Kozak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kozak.html)|1|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html)|
|[Anna Kajak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kajak.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Aneta Rainer](/rpg/inwazja/opowiesci/karty-postaci/1709-aneta-rainer.html)|1|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html)|

---
layout: inwazja-karta-postaci
categories: profile
factions: "Srebrna Świeca"
type: "PC"
owner: "raynor"
title: "Mateusz Ackmann"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **BÓL: Rozwiążę każdą zagadkę**:
    * _Aspekty_: nie daje się ponieść emocjom, ciekawy nowych informacji, dyskretny
    * _Opis_: perwersyjna chęć do rozwiązywania problemów logicznych
* **MRZ: Zrobię interes życia**: 
    * _Aspekty_: 
    * _Opis_: zarobienie takiej kasy, która ustawi do końca życia 
* **MRZ: Będą mnie cenić jako eksperta**:
    * _Aspekty_: rozwiazywanie konfliktów intelektem, arogant
    * _Opis_: pragnienie bycia dostrzeganym jako ekspert w swojej dziedzinie

### Umiejętności

* **Matematyk**:
    * _Aspekty_: ekonometria, akademik
    * _Opis_: 
* **Księgowy**: 
    * _Aspekty_: kreatywna księgowość, archiwa, techniki manipulacyjne
    * _Opis_: 
* **Biurokrata**:
    * _Aspekty_: obfuskacja
    * _Opis_: 
* **rpgowiec**: 
    * _Aspekty_: cosplayer, podszywanie się, techniki manipulacyjne, szycie, konstrukcja ekwipunku, planszówki
    * _Opis_: 
    
### Silne i słabe strony:

* **Doskonała pamięć**:
    * _Aspekty_: 
    * _Opis_: 
* **Nieśmiały**: 
    * _Aspekty_: brak zdolności przywódczych, uległość wobec kobiet, nie potrafi się bić
    * _Opis_: 

## Magia

### Szkoły magiczne

* **Magia zmysłów**:
    * _Aspekty_: nadawanie wizualnych efektow magicznych strojom
    * _Opis_:
* **Technomancja**:
    * _Aspekty_: infomancja, archiwa, magia matematyczna
    * _Opis_:
* **Magia transportu**:
    * _Aspekty_: 
    * _Opis_:

### Zaklęcia statyczne

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 

## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Zarobki

* ?

### Znam

* **kręgi biznesowe - ludzkie**:
    * _Aspekty_: 
    * _Opis_: 
* **rpgowcy i miłośnicy fantasy**:
    * _Aspekty_: 
    * _Opis_: 

### Mam

* **Świeca**:
    * _Aspekty_: pozytywna reputacja dziwaka
    * _Opis_: 

# Opis

Cichy, introwertyk, lekki asperger i uwielbienie do liczb. Studiował "ludzką" matematyke ze specjalizacją w ekonometrii. Hobby: cosplay - zna się na szyciu, konstrukcji ekwipunku, ubrania, nadawanie im wizualnych efektow magicznych; rpgi i planszówki. Jest głównym księgowym dużej korporacji. Mag Świecy. Niski, lekko szpakowate włosy, kawaler, znany w kręgach ludzkich - firmowo-korporacyjnych, jako doskonały księgowy. Znany również w Świecy, jako dziwaczny, ale uprzejmy i utalentowany mag-matematyk.
Zakochany w Supernowej Diakon.

### Koncept

Wysokopoziomowy koncept postaci, 1-2 zdania.

### Motto

"Matematyka to język przyrody"

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Krzywdzę, bo kocham](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|+1 surowiec od Diakonów (Silgor)|Prawdziwa natura Draceny|


## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|160922|księgowy który przeżył dzień swojego życia i 'got the girl', "pokonał" terminuskę i zorganizował fajną imprezę RPG.|[Czarnoskalski konwent RPG](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|10/05/14|10/05/19|[Taniec Liści](/rpg/inwazja/opowiesci/konspekty/kampania-taniec-lisci.html)|
|170725|przeleciał wiłę, zebrał ekipę, pojechał, doprowadził do największego Skażenia w historii Jodłowca i stracił o większości pamięć dla bezpieczeństwa.|[Krzywdzę, bo kocham](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|10/05/09|10/05/11|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Supernowa Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-supernowa-diakon.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Silgor Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-silgor-diakon.html)|1|[170725](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|
|[Rafał Kniaź](/rpg/inwazja/opowiesci/karty-postaci/9999-rafal-kniaz.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Mikado Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-mikado-diakon.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Małż Poszukiwacz](/rpg/inwazja/opowiesci/karty-postaci/1709-malz-poszukiwacz.html)|1|[170725](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|
|[Małż Mateusz](/rpg/inwazja/opowiesci/karty-postaci/1709-malz-mateusz.html)|1|[170725](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|
|[Kornel Wadera](/rpg/inwazja/opowiesci/karty-postaci/1709-kornel-wadera.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Jessica Ruczaj](/rpg/inwazja/opowiesci/karty-postaci/9999-jessica-ruczaj.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Infensa Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-infensa-diakon.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Henryk Gwizdon](/rpg/inwazja/opowiesci/karty-postaci/9999-henryk-gwizdon.html)|1|[170725](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|
|[Fryderyk Bakłażan](/rpg/inwazja/opowiesci/karty-postaci/1709-fryderyk-baklazan.html)|1|[170725](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|
|[Esme Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-esme-myszeczka.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Dyta](/rpg/inwazja/opowiesci/karty-postaci/9999-dyta.html)|1|[170725](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|1|[170725](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|
|[Dorota Gacek](/rpg/inwazja/opowiesci/karty-postaci/1709-dorota-gacek.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Cyprian Koziej](/rpg/inwazja/opowiesci/karty-postaci/9999-cyprian-koziej.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Bolesław Derwisz](/rpg/inwazja/opowiesci/karty-postaci/9999-boleslaw-derwisz.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Barnaba Łonowski](/rpg/inwazja/opowiesci/karty-postaci/9999-barnaba-lonowski.html)|1|[170725](/rpg/inwazja/opowiesci/konspekty/170725-krzywdze-bo-kocham.html)|
|[Artur Pawiak](/rpg/inwazja/opowiesci/karty-postaci/9999-artur-pawiak.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|

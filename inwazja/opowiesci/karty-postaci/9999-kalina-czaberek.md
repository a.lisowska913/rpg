---
layout: inwazja-karta-postaci
categories: profile
title: "Kalina Cząberek"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170603|thrall Luksji; wraz z bratem wpadła w ręce Henryka i Estrelii. Wiecznie goni Siwieckiego, bez nadziei.|[Córka jest narzędziem?](/rpg/inwazja/opowiesci/konspekty/170603-corka-jest-narzedziem.html)|10/02/11|10/02/12|[Córka Lucyfera](/rpg/inwazja/opowiesci/konspekty/kampania-corka-lucyfera.html)|
|170530|nieśmiała; sierota i szczur kościelny. Ukradła satanistyczny manuskrypt dla Klepiczka. Luksja przekształciła ją w thralla. Siostra Filipa.|[Córka Lucyfera](/rpg/inwazja/opowiesci/konspekty/170530-corka-lucyfera.html)|10/02/08|10/02/10|[Córka Lucyfera](/rpg/inwazja/opowiesci/konspekty/kampania-corka-lucyfera.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Luksja Pandemoniae](/rpg/inwazja/opowiesci/karty-postaci/9999-luksja-pandemoniae.html)|2|[170603](/rpg/inwazja/opowiesci/konspekty/170603-corka-jest-narzedziem.html), [170530](/rpg/inwazja/opowiesci/konspekty/170530-corka-lucyfera.html)|
|[Janina Jasionek](/rpg/inwazja/opowiesci/karty-postaci/9999-janina-jasionek.html)|2|[170603](/rpg/inwazja/opowiesci/konspekty/170603-corka-jest-narzedziem.html), [170530](/rpg/inwazja/opowiesci/konspekty/170530-corka-lucyfera.html)|
|[Henryk Siwiecki](/rpg/inwazja/opowiesci/karty-postaci/1709-henryk-siwiecki.html)|2|[170603](/rpg/inwazja/opowiesci/konspekty/170603-corka-jest-narzedziem.html), [170530](/rpg/inwazja/opowiesci/konspekty/170530-corka-lucyfera.html)|
|[Filip Cząberek](/rpg/inwazja/opowiesci/karty-postaci/9999-filip-czaberek.html)|2|[170603](/rpg/inwazja/opowiesci/konspekty/170603-corka-jest-narzedziem.html), [170530](/rpg/inwazja/opowiesci/konspekty/170530-corka-lucyfera.html)|
|[Estrella Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-estrella-diakon.html)|2|[170603](/rpg/inwazja/opowiesci/konspekty/170603-corka-jest-narzedziem.html), [170530](/rpg/inwazja/opowiesci/konspekty/170530-corka-lucyfera.html)|
|[Łukasz Tworzyw](/rpg/inwazja/opowiesci/karty-postaci/9999-lukasz-tworzyw.html)|1|[170530](/rpg/inwazja/opowiesci/konspekty/170530-corka-lucyfera.html)|
|[Zenobi Klepiczek](/rpg/inwazja/opowiesci/karty-postaci/9999-zenobi-klepiczek.html)|1|[170530](/rpg/inwazja/opowiesci/konspekty/170530-corka-lucyfera.html)|
|[Stella Stellaris](/rpg/inwazja/opowiesci/karty-postaci/9999-stella-stellaris.html)|1|[170603](/rpg/inwazja/opowiesci/konspekty/170603-corka-jest-narzedziem.html)|
|[Misza Dobroniewiec](/rpg/inwazja/opowiesci/karty-postaci/9999-misza-dobroniewiec.html)|1|[170603](/rpg/inwazja/opowiesci/konspekty/170603-corka-jest-narzedziem.html)|
|[Henryk Kantosz](/rpg/inwazja/opowiesci/karty-postaci/9999-henryk-kantosz.html)|1|[170603](/rpg/inwazja/opowiesci/konspekty/170603-corka-jest-narzedziem.html)|

---
layout: inwazja-karta-postaci
categories: profile
title: "Hubert Kaldwor"
---
# {{ page.title }}

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|['Dzisiaj złapiemy Nicarettę!'](/rpg/inwazja/opowiesci/konspekty/170113-dzisiaj-zlapiemy-nicarette.html)|na którego plecach są policjanci dzięki Henrykowi|Nicaretta|


## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170120|którego zirytowało to, że przez Henryka trafili na niego policjanci; w ramach zemsty na księżach wsadził Witoldowi Małkowi pedofilskie treści.|[Wielki sojusz powszechny](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html)|10/08/05|10/08/06|[Nicaretta](/rpg/inwazja/opowiesci/konspekty/kampania-nicaretta.html)|
|170113|który uśmiał się kosztem Henryka. A potem dostarczył mu draba. A POTEM napuścili się na niego policjanci...|['Dzisiaj złapiemy Nicarettę!'](/rpg/inwazja/opowiesci/konspekty/170113-dzisiaj-zlapiemy-nicarette.html)|10/08/01|10/08/04|[Nicaretta](/rpg/inwazja/opowiesci/konspekty/kampania-nicaretta.html)|
|161206|który po przedstawieniu dowodów przez Henryka uwierzył, że był w niebezpieczeństwie. Sojusznik Henryka - jest mu wdzięczny.|[Ucieczka Sióstr Światła](/rpg/inwazja/opowiesci/konspekty/161206-ucieczka-siostr-swiatla.html)|10/07/25|10/07/26|[Nicaretta](/rpg/inwazja/opowiesci/konspekty/kampania-nicaretta.html)|
|161129|który dostarcza wszystkich kluczowych informacji Henrykowi i Mai dzięki swoim analityczkom. Zagięły nań parol femisatanistki.|[Ewakuacja Natalii, wejście maga](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html)|10/07/23|10/07/24|[Nicaretta](/rpg/inwazja/opowiesci/konspekty/kampania-nicaretta.html)|
|161115|który dla odmiany martwi się o swoją bratanicę, która uciekła z domu i dołączyła do femisatanistek...|[Uciekła do femisatanistek](/rpg/inwazja/opowiesci/konspekty/161115-uciekla-do-femisatanistek.html)|10/07/21|10/07/22|[Nicaretta](/rpg/inwazja/opowiesci/konspekty/kampania-nicaretta.html)|
|161110|polityk partii Kochamy Państwowe Pieniądze. Powiązany mafijnie, lubi dobrą zabawę i dziewczyny.|[Succubus myśli, że uciekł](/rpg/inwazja/opowiesci/konspekty/161110-succubus-mysli-ze-uciekl.html)|10/07/14|10/07/20|[Nicaretta](/rpg/inwazja/opowiesci/konspekty/kampania-nicaretta.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Henryk Siwiecki](/rpg/inwazja/opowiesci/karty-postaci/1709-henryk-siwiecki.html)|6|[170120](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html), [170113](/rpg/inwazja/opowiesci/konspekty/170113-dzisiaj-zlapiemy-nicarette.html), [161206](/rpg/inwazja/opowiesci/konspekty/161206-ucieczka-siostr-swiatla.html), [161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html), [161115](/rpg/inwazja/opowiesci/konspekty/161115-uciekla-do-femisatanistek.html), [161110](/rpg/inwazja/opowiesci/konspekty/161110-succubus-mysli-ze-uciekl.html)|
|[Marzena Dorszaj](/rpg/inwazja/opowiesci/karty-postaci/1709-marzena-dorszaj.html)|4|[170120](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html), [170113](/rpg/inwazja/opowiesci/konspekty/170113-dzisiaj-zlapiemy-nicarette.html), [161206](/rpg/inwazja/opowiesci/konspekty/161206-ucieczka-siostr-swiatla.html), [161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html)|
|[Andrzej Klepiczek](/rpg/inwazja/opowiesci/karty-postaci/9999-andrzej-klepiczek.html)|4|[170113](/rpg/inwazja/opowiesci/konspekty/170113-dzisiaj-zlapiemy-nicarette.html), [161206](/rpg/inwazja/opowiesci/konspekty/161206-ucieczka-siostr-swiatla.html), [161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html), [161115](/rpg/inwazja/opowiesci/konspekty/161115-uciekla-do-femisatanistek.html)|
|[Witold Małek](/rpg/inwazja/opowiesci/karty-postaci/9999-witold-malek.html)|2|[170120](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html), [170113](/rpg/inwazja/opowiesci/konspekty/170113-dzisiaj-zlapiemy-nicarette.html)|
|[Urszula Kram](/rpg/inwazja/opowiesci/karty-postaci/9999-urszula-kram.html)|2|[161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html), [161115](/rpg/inwazja/opowiesci/konspekty/161115-uciekla-do-femisatanistek.html)|
|[Nicaretta](/rpg/inwazja/opowiesci/karty-postaci/1709-nicaretta.html)|2|[170120](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html), [161110](/rpg/inwazja/opowiesci/konspekty/161110-succubus-mysli-ze-uciekl.html)|
|[Natalia Kaldwor](/rpg/inwazja/opowiesci/karty-postaci/9999-natalia-kaldwor.html)|2|[161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html), [161115](/rpg/inwazja/opowiesci/konspekty/161115-uciekla-do-femisatanistek.html)|
|[Maja Liszka](/rpg/inwazja/opowiesci/karty-postaci/1709-maja-liszka.html)|2|[161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html), [161115](/rpg/inwazja/opowiesci/konspekty/161115-uciekla-do-femisatanistek.html)|
|[Kora Panik](/rpg/inwazja/opowiesci/karty-postaci/9999-kora-panik.html)|2|[161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html), [161115](/rpg/inwazja/opowiesci/konspekty/161115-uciekla-do-femisatanistek.html)|
|[Karina Łoszad](/rpg/inwazja/opowiesci/karty-postaci/9999-karina-loszad.html)|2|[170120](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html), [170113](/rpg/inwazja/opowiesci/konspekty/170113-dzisiaj-zlapiemy-nicarette.html)|
|[Hipolit Mraczon](/rpg/inwazja/opowiesci/karty-postaci/9999-hipolit-mraczon.html)|2|[161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html), [161115](/rpg/inwazja/opowiesci/konspekty/161115-uciekla-do-femisatanistek.html)|
|[Grażyna Diadem](/rpg/inwazja/opowiesci/karty-postaci/9999-grazyna-diadem.html)|2|[170120](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html), [161110](/rpg/inwazja/opowiesci/konspekty/161110-succubus-mysli-ze-uciekl.html)|
|[Amelia Eter](/rpg/inwazja/opowiesci/karty-postaci/9999-amelia-eter.html)|2|[161206](/rpg/inwazja/opowiesci/konspekty/161206-ucieczka-siostr-swiatla.html), [161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html)|
|[Adela Klepiczek](/rpg/inwazja/opowiesci/karty-postaci/9999-adela-klepiczek.html)|2|[161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html), [161115](/rpg/inwazja/opowiesci/konspekty/161115-uciekla-do-femisatanistek.html)|
|[Sebastian Drabon](/rpg/inwazja/opowiesci/karty-postaci/9999-sebastian-drabon.html)|1|[170120](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html)|
|[Renata Krzem](/rpg/inwazja/opowiesci/karty-postaci/9999-renata-krzem.html)|1|[161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html)|
|[Marzena Gilek](/rpg/inwazja/opowiesci/karty-postaci/9999-marzena-gilek.html)|1|[161110](/rpg/inwazja/opowiesci/konspekty/161110-succubus-mysli-ze-uciekl.html)|
|[Luna](/rpg/inwazja/opowiesci/karty-postaci/9999-luna.html)|1|[161110](/rpg/inwazja/opowiesci/konspekty/161110-succubus-mysli-ze-uciekl.html)|
|[Kinglord](/rpg/inwazja/opowiesci/karty-postaci/9999-kinglord.html)|1|[170120](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html)|
|[Felicja Wydech](/rpg/inwazja/opowiesci/karty-postaci/9999-felicja-wydech.html)|1|[161129](/rpg/inwazja/opowiesci/konspekty/161129-ewakuacja-natalii-wejscie-maga.html)|
|[Aurel Czarko](/rpg/inwazja/opowiesci/karty-postaci/1709-aurel-czarko.html)|1|[161115](/rpg/inwazja/opowiesci/konspekty/161115-uciekla-do-femisatanistek.html)|
|[Anna Osiaczek](/rpg/inwazja/opowiesci/karty-postaci/9999-anna-osiaczek.html)|1|[161206](/rpg/inwazja/opowiesci/konspekty/161206-ucieczka-siostr-swiatla.html)|
|[Albert Czapkuś](/rpg/inwazja/opowiesci/karty-postaci/9999-albert-czapkus.html)|1|[161110](/rpg/inwazja/opowiesci/konspekty/161110-succubus-mysli-ze-uciekl.html)|

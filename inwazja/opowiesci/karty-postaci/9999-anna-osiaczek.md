---
layout: inwazja-karta-postaci
categories: profile
title: "Anna Osiaczek"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|161220|paladynka, która z pomocą sił Huberta Kaldwora skupiła się na znalezieniu Nicaretty. W większości nieobecna na misji.|[Zniszczenie posągu Arazille](/rpg/inwazja/opowiesci/konspekty/161220-zniszczenie-posagu-arazille.html)|10/07/27|10/07/30|[Nicaretta](/rpg/inwazja/opowiesci/konspekty/kampania-nicaretta.html)|
|161206|blokująca herpyneę w kościele jako tarczowa parafianka i wyczuła, że Marzena Coś Złego Zrobiła w domu Mraczona.|[Ucieczka Sióstr Światła](/rpg/inwazja/opowiesci/konspekty/161206-ucieczka-siostr-swiatla.html)|10/07/25|10/07/26|[Nicaretta](/rpg/inwazja/opowiesci/konspekty/kampania-nicaretta.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Marzena Dorszaj](/rpg/inwazja/opowiesci/karty-postaci/1709-marzena-dorszaj.html)|2|[161220](/rpg/inwazja/opowiesci/konspekty/161220-zniszczenie-posagu-arazille.html), [161206](/rpg/inwazja/opowiesci/konspekty/161206-ucieczka-siostr-swiatla.html)|
|[Henryk Siwiecki](/rpg/inwazja/opowiesci/karty-postaci/1709-henryk-siwiecki.html)|2|[161220](/rpg/inwazja/opowiesci/konspekty/161220-zniszczenie-posagu-arazille.html), [161206](/rpg/inwazja/opowiesci/konspekty/161206-ucieczka-siostr-swiatla.html)|
|[Andrzej Klepiczek](/rpg/inwazja/opowiesci/karty-postaci/9999-andrzej-klepiczek.html)|2|[161220](/rpg/inwazja/opowiesci/konspekty/161220-zniszczenie-posagu-arazille.html), [161206](/rpg/inwazja/opowiesci/konspekty/161206-ucieczka-siostr-swiatla.html)|
|[Urszula Kram](/rpg/inwazja/opowiesci/karty-postaci/9999-urszula-kram.html)|1|[161220](/rpg/inwazja/opowiesci/konspekty/161220-zniszczenie-posagu-arazille.html)|
|[Szczepan Szokmaniewicz](/rpg/inwazja/opowiesci/karty-postaci/9999-szczepan-szokmaniewicz.html)|1|[161220](/rpg/inwazja/opowiesci/konspekty/161220-zniszczenie-posagu-arazille.html)|
|[Romana Kaldwor](/rpg/inwazja/opowiesci/karty-postaci/9999-romana-kaldwor.html)|1|[161220](/rpg/inwazja/opowiesci/konspekty/161220-zniszczenie-posagu-arazille.html)|
|[Natalia Kaldwor](/rpg/inwazja/opowiesci/karty-postaci/9999-natalia-kaldwor.html)|1|[161220](/rpg/inwazja/opowiesci/konspekty/161220-zniszczenie-posagu-arazille.html)|
|[Karina Łoszad](/rpg/inwazja/opowiesci/karty-postaci/9999-karina-loszad.html)|1|[161220](/rpg/inwazja/opowiesci/konspekty/161220-zniszczenie-posagu-arazille.html)|
|[Hubert Kaldwor](/rpg/inwazja/opowiesci/karty-postaci/9999-hubert-kaldwor.html)|1|[161206](/rpg/inwazja/opowiesci/konspekty/161206-ucieczka-siostr-swiatla.html)|
|[Arazille](/rpg/inwazja/opowiesci/karty-postaci/9999-arazille.html)|1|[161220](/rpg/inwazja/opowiesci/konspekty/161220-zniszczenie-posagu-arazille.html)|
|[Amelia Eter](/rpg/inwazja/opowiesci/karty-postaci/9999-amelia-eter.html)|1|[161206](/rpg/inwazja/opowiesci/konspekty/161206-ucieczka-siostr-swiatla.html)|

---
layout: inwazja-karta-postaci
categories: profile
title: "Jewgenij Zajcew"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170823|który obiecał wsparcie swoich przemytników, bo w okolicach Bzizmy jest bardzo cenny Skażający artefakt.|[Suma niedokończonych spraw...](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|10/08/25|10/08/28|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161005|szmugluje artefakty KADEMu na wschód. Musi trzymać się procesu. Wszystko musi działać jak w zegarku. Rubaszny i otwarty.|[Szmuglowanie artefaktów](/rpg/inwazja/opowiesci/konspekty/161005-szmuglowanie-artefaktow.html)|10/05/05|10/05/07|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Tamara Muszkiet](/rpg/inwazja/opowiesci/karty-postaci/1709-tamara-muszkiet.html)|1|[161005](/rpg/inwazja/opowiesci/konspekty/161005-szmuglowanie-artefaktow.html)|
|[Piotr Kit](/rpg/inwazja/opowiesci/karty-postaci/1709-piotr-kit.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Otton Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-otton-blakenbauer.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Olga Miodownik](/rpg/inwazja/opowiesci/karty-postaci/1709-olga-miodownik.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Mordred Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-mordred-blakenbauer.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Marian Łajdak](/rpg/inwazja/opowiesci/karty-postaci/1709-marian-lajdak.html)|1|[161005](/rpg/inwazja/opowiesci/konspekty/161005-szmuglowanie-artefaktow.html)|
|[Margaret Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-margaret-blakenbauer.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Marcelin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-marcelin-blakenbauer.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Magnus Spellslinger](/rpg/inwazja/opowiesci/karty-postaci/1709-magnus-spellslinger.html)|1|[161005](/rpg/inwazja/opowiesci/konspekty/161005-szmuglowanie-artefaktow.html)|
|[Klara Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-klara-blakenbauer.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Kaspian Miauczek](/rpg/inwazja/opowiesci/karty-postaci/9999-kaspian-miauczek.html)|1|[161005](/rpg/inwazja/opowiesci/konspekty/161005-szmuglowanie-artefaktow.html)|
|[Karina von Blutwurst](/rpg/inwazja/opowiesci/karty-postaci/9999-karina-von-blutwurst.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Julian Pszczelak](/rpg/inwazja/opowiesci/karty-postaci/1709-julian-pszczelak.html)|1|[161005](/rpg/inwazja/opowiesci/konspekty/161005-szmuglowanie-artefaktow.html)|
|[Judyta Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-judyta-maus.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Jan Kotlin](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-kotlin.html)|1|[161005](/rpg/inwazja/opowiesci/konspekty/161005-szmuglowanie-artefaktow.html)|
|[Infensa Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-infensa-diakon.html)|1|[161005](/rpg/inwazja/opowiesci/konspekty/161005-szmuglowanie-artefaktow.html)|
|[Henryk Siwiecki](/rpg/inwazja/opowiesci/karty-postaci/1709-henryk-siwiecki.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Estrella Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-estrella-diakon.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Dionizy Kret](/rpg/inwazja/opowiesci/karty-postaci/1709-dionizy-kret.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Bzizma Stlitlitlix](/rpg/inwazja/opowiesci/karty-postaci/9999-bzizma-stlitlitlix.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Aniela Lipka](/rpg/inwazja/opowiesci/karty-postaci/1709-aniela-lipka.html)|1|[161005](/rpg/inwazja/opowiesci/konspekty/161005-szmuglowanie-artefaktow.html)|
|[Alina Bednarz](/rpg/inwazja/opowiesci/karty-postaci/1803-alina-bednarz.html)|1|[170823](/rpg/inwazja/opowiesci/konspekty/170823-suma-niedokonczonych-spraw.html)|
|[Aleksandra Trawens](/rpg/inwazja/opowiesci/karty-postaci/9999-aleksandra-trawens.html)|1|[161005](/rpg/inwazja/opowiesci/konspekty/161005-szmuglowanie-artefaktow.html)|

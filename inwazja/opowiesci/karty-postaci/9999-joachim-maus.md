---
layout: inwazja-karta-postaci
categories: profile
title: "Joachim Maus"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|180214|negocjator Mausów stojący po stronie Łucji, ale musi bronić pozycji Abelarda. Między młotem i kowadłem. Szczęśliwie, Paulinie się udało rozmontować konflikt.|[Nie podłożona świnia Łucji](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html)|11/10/15|11/10/17|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|180110|dyplomata rodu Maus, który z rozkazu seirasa Abelarda Mausa zażądał wydania Łucji. Zniszczył dowody na działania Myszeczki, by go nie antagonizować.|[Odzyskana władza Pauliny](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html)|11/10/10|11/10/12|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Łucja Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-lucja-maus.html)|2|[180214](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html), [180110](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html)|
|[Tomasz Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-tomasz-myszeczka.html)|2|[180214](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html), [180110](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html)|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|2|[180214](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html), [180110](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html)|
|[Melodia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-melodia-diakon.html)|2|[180214](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html), [180110](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html)|
|[Daniel Akwitański](/rpg/inwazja/opowiesci/karty-postaci/1709-daniel-akwitanski.html)|2|[180214](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html), [180110](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html)|
|[Wiaczesław Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-wiaczeslaw-zajcew.html)|1|[180110](/rpg/inwazja/opowiesci/konspekty/180110-odzyskana-wladza-pauliny.html)|
|[Bolesław Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-boleslaw-maus.html)|1|[180214](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html)|

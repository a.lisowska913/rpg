---
layout: inwazja-karta-postaci
categories: profile
title: "Lea Swoboda"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|151013|uzależniona wiła, która miota się pomiędzy narkotykiem, mocą Przodka i wyniszczeniem. Prawie zabiła grzybiarza; nie powstrzymała głodu.|[Kontrolowany odwrót z zamtuza](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html)|10/06/04|10/06/05|[Ucieczka do Przodka](/rpg/inwazja/opowiesci/konspekty/kampania-ucieczka-do-przodka.html)|
|151003|wiła; pierwotnie zniewolona przez Galę i Tymka a teraz uwolniona przez Paulinę i moc Przodka.|[Zamtuz przestaje działać](/rpg/inwazja/opowiesci/konspekty/151003-zamtuz-przestaje-dzialac.html)|10/06/02|10/06/03|[Ucieczka do Przodka](/rpg/inwazja/opowiesci/konspekty/kampania-ucieczka-do-przodka.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Tymoteusz Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-tymoteusz-maus.html)|2|[151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html), [151003](/rpg/inwazja/opowiesci/konspekty/151003-zamtuz-przestaje-dzialac.html)|
|[Tomasz Leżniak](/rpg/inwazja/opowiesci/karty-postaci/9999-tomasz-lezniak.html)|2|[151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html), [151003](/rpg/inwazja/opowiesci/konspekty/151003-zamtuz-przestaje-dzialac.html)|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|2|[151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html), [151003](/rpg/inwazja/opowiesci/konspekty/151003-zamtuz-przestaje-dzialac.html)|
|[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/1709-maria-newa.html)|2|[151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html), [151003](/rpg/inwazja/opowiesci/konspekty/151003-zamtuz-przestaje-dzialac.html)|
|[Gala Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-gala-zajcew.html)|2|[151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html), [151003](/rpg/inwazja/opowiesci/konspekty/151003-zamtuz-przestaje-dzialac.html)|
|[Matylda Daczak](/rpg/inwazja/opowiesci/karty-postaci/9999-matylda-daczak.html)|1|[151003](/rpg/inwazja/opowiesci/konspekty/151003-zamtuz-przestaje-dzialac.html)|
|[Mariusz Czyczyż](/rpg/inwazja/opowiesci/karty-postaci/9999-mariusz-czyczyz.html)|1|[151003](/rpg/inwazja/opowiesci/konspekty/151003-zamtuz-przestaje-dzialac.html)|
|[Maciej Orank](/rpg/inwazja/opowiesci/karty-postaci/9999-maciej-orank.html)|1|[151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html)|
|[Krystian Korzunio](/rpg/inwazja/opowiesci/karty-postaci/1709-krystian-korzunio.html)|1|[151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html)|
|[Jerzy Karmelik](/rpg/inwazja/opowiesci/karty-postaci/9999-jerzy-karmelik.html)|1|[151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html)|
|[Janusz Karzeł](/rpg/inwazja/opowiesci/karty-postaci/9999-janusz-karzel.html)|1|[151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html)|
|[Franciszek Marlin](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-marlin.html)|1|[151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html)|
|[Bartosz Bławatek](/rpg/inwazja/opowiesci/karty-postaci/9999-bartosz-blawatek.html)|1|[151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html)|
|[Artur Kurczak](/rpg/inwazja/opowiesci/karty-postaci/9999-artur-kurczak.html)|1|[151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html)|

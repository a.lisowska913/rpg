---
layout: inwazja-karta-postaci
categories: profile
factions: "Srebrna Świeca, Szlachta"
type: "NPC"
title: "Mariusz Garaż"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **BÓL: artysta**:
    * _Aspekty_: kobieciarz
    * _Opis_: chce tworzyć sztukę, chce zrobić coś wielkiego i pięknego. Niestety, nie może.
* **FIL: **: 
    * _Aspekty_: 
    * _Opis_: 
* **MET: Oportunista**:
    * _Aspekty_: 
    * _Opis_: musi spłacić długi i musi spłacić się swoim inwestorom. Idzie po najmniejszej linii oporu.
* **MRZ: sławny i potężny**:
    * _Aspekty_: 
    * _Opis_: chce być sławny i znany; chce mieć władzę i wpływ. Nie cierpi być popychadłem…
* **KLT: **:
    * _Aspekty_: 
    * _Opis_: 

### Umiejętności:

* **Biurokrata**:
    * _Aspekty_: zdobywanie pozwoleń, fałszerz
    * _Opis_: doskonale porusza się w dokumentach i ukrywa wszystkie swoje słabości. Potrafi też odpowiednio fałszować dokumenty. w jakiś sposób Garaż zawsze ma pozwolenie na działanie jeśli tego potrzebuje, nawet jeśli teoretycznie nie powinien móc.
* **Reżyser**: 
    * _Aspekty_: 
    * _Opis_: potrafi doskonale sprawić, by wszystkie "postacie grały tak, jak jego scenariusz wymaga". często potrzebne mu są odpowiednie propsy, by wydarzenia potoczyły się tak, jak tego potrzebuje.
* **Spadanie na cztery łapy**:
    * _Aspekty_: manipulator
    * _Opis_: praktycznie nieprzyskrzynialny; nie do końca da się go sensownie złapać czy coś mu udowodnić; ma na kogo zwalić.
* **Szpieg**:
    * _Aspekty_: nagrywanie ukrytą kamerą, zbierania danych, zdobywanie haków
    * _Opis_: 
	
### Silne i słabe strony:

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

## Magia

### Szkoły magiczne

* **Technomancja**:
    * _Aspekty_: 
    * _Opis_: 

### Zaklęcia statyczne

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 

## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Zarobki

* ?

### Znam

* **inwestorzy w jego talent**:
    * _Aspekty_: magowie, którzy zdecydowali się zainwestować, by jego "filmy" przyniosły im dochód
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

### Mam

* **dokumenty z zezwoleniami**:
    * _Aspekty_: 
    * _Opis_: grupa nieprecyzyjnie sformowanych dokumentów z zezwoleniami umożliwiającymi mu zrobienie praktycznie wszystkiego
* **magitechowy sprzęt filmowy**:
    * _Aspekty_: 
    * _Opis_: sprzęt służący do nagrywania… czy szpiegowania.
* ****:
    * _Aspekty_: 
    * _Opis_: 

# Opis

krótki opis postaci, rzeczy, jakie warto pamiętać.

## Pytania
1. Z czego TP jest szczególnie dumna (co jest doceniane przez innych)? Dlaczego jest z tego dumna?
2. Z czego TP jest znana najlepiej wśród innych? Wśród kogo (kim są ci "inni")?
3. Co jest uważane za nieuczciwą przewagę TP?
Pomiędzy umiejętnością poruszania się po biurokracji Świecy, potężnymi inwestorami i całkiem niezłym sprzętem (jak i oportunistycznym szczurzyzmem), Garaż zawsze jest w stanie zdobyć wszystkie potrzebne papiery i pozwolenia by móc zrobić to, co chce zrobić i być w prawie Świecy.
04. Co sprawia, że TP uzna, że TO ZADANIE jest perfekcyjne dla niej? Czemu?
05. Kiedy TP powie "O nie, tak się nie stanie". Dlaczego? Jak spróbuje temu zaradzić?
06. W jakich okolicznościach "Jak trwoga, to przyjdą do TP"? Kto przyjdzie?
07. Czego TP najbardziej żałuje? Co zrobiłaby inaczej? Dlaczego?
Mariusz naprawdę żałuje, że jego życie sprowadziło się do bycia nędznym szczurem. Chciałby tworzyć sztukę, być… kimś więcej, ale zwyczajnie nie widzi do tego możliwości. Jest bezradny - w kleszczach między światem ludzi i światem magów, pod mecenatem magów którzy oczekują wyników i czegoś co przyniesie im dochód… jest artystą muszącym robić chłamowate seriale.
08. O czym TP marzy? Do czego dąży? Dlaczego?
Mariusz marzy o sławie i stworzeniu czegoś godnego Stevena Spielberga, ale w świecie magów. Chce zdobyć mecenasa i stworzyć cudowny film. Nie obraziłby się, gdyby dał radę przybliżyć świat ludzi magom, acz nie jest to dla niego tak ważne.
09. Co TP najbardziej lubi robić w czasie wolnym? Czemu?
Uwielbia szastać bogactwem i wywierać wpływ na osoby postrzegane za słabsze i mniej znaczące. Szczególnie lubi młode dziewczyny, które "mu się należą". W końcu jest artystą…
10. Jakie wyzwanie TP dała radę przezwyciężyć? Co ją to nauczyło?
Mariusz dał radę wybić się ze świata ludzi, gdzie był nie najlepszym reżyserem i pozycjonować się jako całkiem niezły reżyser świata magów, łącząc podstawy technomancji z opowiadaniem historii i znajdując dla siebie niszę. Nauczyło go to oportunistycznego podejścia - zamiast rywalizować i się przebijać woli… prześlizgnąć się koło problemu i polować tam, gdzie mniej boli.
11. Co jest szczególnie upierdliwe dla TP i dlaczego?

### Koncept

Wysokopoziomowy koncept postaci, 1-2 zdania.

### Motto

"Tekst"

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170221|reżyser reality show. Nie spodziewał się mocy Kiry, specjalista od pierwszego wrażenia... NOT. Nie umie grać 'win-win', nawet gdy chce.|[Przecież nie chodzi o koncert](/rpg/inwazja/opowiesci/konspekty/170221-przeciez-nie-chodzi-o-koncert.html)|10/02/05|10/02/07|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150210|"reżyser" chcący wprowadzić "telewizję" do świata magów. Zrobił dwa ruchy i załatwił pozwolenia od SŚ... ale przeciw sobie ma Hektora i Dianę.|['Komandosi', czyli upadek bohaterki](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html)|10/01/11|10/01/12|[Blakenbauerowie x Skorpion](/rpg/inwazja/opowiesci/konspekty/kampania-blakenbauerowie-x-skorpion.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Wiktor Lubaszny](/rpg/inwazja/opowiesci/karty-postaci/9999-wiktor-lubaszny.html)|1|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html)|
|[Tymoteusz Dzionek](/rpg/inwazja/opowiesci/karty-postaci/9999-tymoteusz-dzionek.html)|1|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html)|
|[Onufry Puzel](/rpg/inwazja/opowiesci/karty-postaci/9999-onufry-puzel.html)|1|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html)|
|[Mikołaj Młot](/rpg/inwazja/opowiesci/karty-postaci/9999-mikolaj-mlot.html)|1|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html)|
|[Marcelin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-marcelin-blakenbauer.html)|1|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html)|
|[Maciej Kwarc](/rpg/inwazja/opowiesci/karty-postaci/9999-maciej-kwarc.html)|1|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html)|
|[Klemens X](/rpg/inwazja/opowiesci/karty-postaci/9999-klemens-x.html)|1|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html)|
|[Kira Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-kira-zajcew.html)|1|[170221](/rpg/inwazja/opowiesci/konspekty/170221-przeciez-nie-chodzi-o-koncert.html)|
|[Kamil Rzepa](/rpg/inwazja/opowiesci/karty-postaci/9999-kamil-rzepa.html)|1|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html)|
|[Judyta Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-judyta-maus.html)|1|[170221](/rpg/inwazja/opowiesci/konspekty/170221-przeciez-nie-chodzi-o-koncert.html)|
|[Janusz Wybój](/rpg/inwazja/opowiesci/karty-postaci/9999-janusz-wyboj.html)|1|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|1|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html)|
|[Grażyna Remska](/rpg/inwazja/opowiesci/karty-postaci/9999-grazyna-remska.html)|1|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html)|
|[Franciszek Baranowski](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-baranowski.html)|1|[170221](/rpg/inwazja/opowiesci/konspekty/170221-przeciez-nie-chodzi-o-koncert.html)|
|[Felicja Wadicek](/rpg/inwazja/opowiesci/karty-postaci/9999-felicja-wadicek.html)|1|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|1|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html)|
|[Diana Larent](/rpg/inwazja/opowiesci/karty-postaci/9999-diana-larent.html)|1|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html)|
|[Bianka Drażyńska](/rpg/inwazja/opowiesci/karty-postaci/9999-bianka-drazynska.html)|1|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html)|
|[Arkadiusz Wadicek](/rpg/inwazja/opowiesci/karty-postaci/9999-arkadiusz-wadicek.html)|1|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html)|
|[Antygona Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-antygona-diakon.html)|1|[170221](/rpg/inwazja/opowiesci/konspekty/170221-przeciez-nie-chodzi-o-koncert.html)|
|[Anna Kajak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kajak.html)|1|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html)|
|[Adam Bożynów](/rpg/inwazja/opowiesci/karty-postaci/9999-adam-bozynow.html)|1|[150210](/rpg/inwazja/opowiesci/konspekty/150210-komandosi-upadek-bohaterki.html)|

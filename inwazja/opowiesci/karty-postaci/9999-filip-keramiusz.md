---
layout: inwazja-karta-postaci
categories: profile
title: "Filip Keramiusz"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|171024|dzięki silnej pomocy Kociebora udało mu się utrzymać Wieżę Wichrów i uzyskać potężnego elementala - którego może sprzedać Krzysztofowi ;-). Powiedział Kocieborowi prawdę.|[Detektyw, lecz nie Sądecznego](/rpg/inwazja/opowiesci/konspekty/171024-detektyw-lecz-nie-sadecznego.html)|11/09/16|11/09/18|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171022|przedsiębiorca w rozpaczy uciekający się do potencjalnie niebezpiecznych i nieetycznych działań z elementalami. Gonią go kontrakty. Chce dobrze, ale musi szybko...|[Tylko nieświadomy elemental](/rpg/inwazja/opowiesci/konspekty/171022-tylko-nieswiadomy-elemental.html)|11/09/12|11/09/14|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|2|[171024](/rpg/inwazja/opowiesci/konspekty/171024-detektyw-lecz-nie-sadecznego.html), [171022](/rpg/inwazja/opowiesci/konspekty/171022-tylko-nieswiadomy-elemental.html)|
|[Kajetan Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-kajetan-weiner.html)|2|[171024](/rpg/inwazja/opowiesci/konspekty/171024-detektyw-lecz-nie-sadecznego.html), [171022](/rpg/inwazja/opowiesci/konspekty/171022-tylko-nieswiadomy-elemental.html)|
|[Tomasz Kapelusz](/rpg/inwazja/opowiesci/karty-postaci/9999-tomasz-kapelusz.html)|1|[171024](/rpg/inwazja/opowiesci/konspekty/171024-detektyw-lecz-nie-sadecznego.html)|
|[Szymon Łokciak](/rpg/inwazja/opowiesci/karty-postaci/9999-szymon-lokciak.html)|1|[171022](/rpg/inwazja/opowiesci/konspekty/171022-tylko-nieswiadomy-elemental.html)|
|[Robert Sądeczny](/rpg/inwazja/opowiesci/karty-postaci/1709-robert-sadeczny.html)|1|[171024](/rpg/inwazja/opowiesci/konspekty/171024-detektyw-lecz-nie-sadecznego.html)|
|[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/1709-maria-newa.html)|1|[171022](/rpg/inwazja/opowiesci/konspekty/171022-tylko-nieswiadomy-elemental.html)|
|[Maja Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-maja-weiner.html)|1|[171024](/rpg/inwazja/opowiesci/konspekty/171024-detektyw-lecz-nie-sadecznego.html)|
|[Krzysztof Grumrzyk](/rpg/inwazja/opowiesci/karty-postaci/1709-krzysztof-grumrzyk.html)|1|[171024](/rpg/inwazja/opowiesci/konspekty/171024-detektyw-lecz-nie-sadecznego.html)|
|[Kociebor Dyrygent](/rpg/inwazja/opowiesci/karty-postaci/1709-kociebor-dyrygent.html)|1|[171024](/rpg/inwazja/opowiesci/konspekty/171024-detektyw-lecz-nie-sadecznego.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|1|[171022](/rpg/inwazja/opowiesci/konspekty/171022-tylko-nieswiadomy-elemental.html)|
|[Adam Kapelusz](/rpg/inwazja/opowiesci/karty-postaci/9999-adam-kapelusz.html)|1|[171024](/rpg/inwazja/opowiesci/konspekty/171024-detektyw-lecz-nie-sadecznego.html)|

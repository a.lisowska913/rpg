---
layout: inwazja-karta-postaci
categories: profile
title: "Mariola Miłżoś"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|171229|podkochująca się w Krzysztofie Tłuku. Gdy ów zniknął, była doń ostatnim ogniwem. Zainicjowała Żaklinę. Bardzo jej zależało by go znaleźć.|[Esuriit w Półdarze](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|11/10/07|11/10/09|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Żaklina Bąk](/rpg/inwazja/opowiesci/karty-postaci/1709-zaklina-bak.html)|1|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|
|[Wojciech Piekarz](/rpg/inwazja/opowiesci/karty-postaci/9999-wojciech-piekarz.html)|1|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|
|[Tadek Swołczan](/rpg/inwazja/opowiesci/karty-postaci/9999-tadek-swolczan.html)|1|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|
|[Mariusz Tłuk](/rpg/inwazja/opowiesci/karty-postaci/9999-mariusz-tluk.html)|1|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|
|[Krzysztof Tłuk](/rpg/inwazja/opowiesci/karty-postaci/9999-krzysztof-tluk.html)|1|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|
|[Krzysiu Miłżoś](/rpg/inwazja/opowiesci/karty-postaci/9999-krzysiu-milzos.html)|1|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|
|[Kajetan Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-kajetan-weiner.html)|1|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|
|[Jagoda Kozak](/rpg/inwazja/opowiesci/karty-postaci/1709-jagoda-kozak.html)|1|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|
|[Efemeryda Senesgradzka](/rpg/inwazja/opowiesci/karty-postaci/9999-efemeryda-senesgradzka.html)|1|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|
|[Bonifacy Jeż](/rpg/inwazja/opowiesci/karty-postaci/9999-bonifacy-jez.html)|1|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|
|[August Paszkwil](/rpg/inwazja/opowiesci/karty-postaci/1709-august-paszkwil.html)|1|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|

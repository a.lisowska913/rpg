---
layout: inwazja-karta-postaci
categories: profile
title: "Beata Obaczna"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170716|protektorka Pauliny z SŚ. Magowie się jej boją i jest samotna; lubi mistrza Pauliny. Ściągnęła Paulinie terminusa do pomocy - niestety, to był Gustaw.|[Eteryczny chłopiec i jego pies](/rpg/inwazja/opowiesci/konspekty/170716-eteryczny-chlopiec-i-jego-pies.html)|10/01/08|10/01/10|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|1|[170716](/rpg/inwazja/opowiesci/konspekty/170716-eteryczny-chlopiec-i-jego-pies.html)|
|[Mikołaj Pyżuk](/rpg/inwazja/opowiesci/karty-postaci/9999-mikolaj-pyzuk.html)|1|[170716](/rpg/inwazja/opowiesci/konspekty/170716-eteryczny-chlopiec-i-jego-pies.html)|
|[Karol Wąski](/rpg/inwazja/opowiesci/karty-postaci/9999-karol-waski.html)|1|[170716](/rpg/inwazja/opowiesci/konspekty/170716-eteryczny-chlopiec-i-jego-pies.html)|
|[Jakub Pyżuk](/rpg/inwazja/opowiesci/karty-postaci/9999-jakub-pyzuk.html)|1|[170716](/rpg/inwazja/opowiesci/konspekty/170716-eteryczny-chlopiec-i-jego-pies.html)|
|[Gustaw Bareczny](/rpg/inwazja/opowiesci/karty-postaci/9999-gustaw-bareczny.html)|1|[170716](/rpg/inwazja/opowiesci/konspekty/170716-eteryczny-chlopiec-i-jego-pies.html)|
|[Bogumił Miłoszept](/rpg/inwazja/opowiesci/karty-postaci/9999-bogumil-miloszept.html)|1|[170716](/rpg/inwazja/opowiesci/konspekty/170716-eteryczny-chlopiec-i-jego-pies.html)|

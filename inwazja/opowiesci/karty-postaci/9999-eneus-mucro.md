---
layout: inwazja-karta-postaci
categories: profile
title: "Eneus Mucro"
---
# {{ page.title }}

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Klątwożyt z lustra](/rpg/inwazja/opowiesci/konspekty/170325-klatwozyt-z-lustra.html)|wolny od klątwożyta; dorobił się własnej unikalnej osobowości|Rezydentka Krukowa|


## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170326|po tym jak wpadł w nieskończoną pętlę: zregenerowany, pełnosprawny i odbudowany; zajmuje się Ferrusem.|[Cała przeszłość spłonęła](/rpg/inwazja/opowiesci/konspekty/170326-cala-przeszlosc-splonela.html)|10/03/04|10/03/07|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|
|170325|terminus opętany przez klątwożyta co stworzyło mu osobowość; Paulina zaskoczyła go i zniszczyła klątwożyt. Autonaprawia się.|[Klątwożyt z lustra](/rpg/inwazja/opowiesci/konspekty/170325-klatwozyt-z-lustra.html)|10/02/28|10/03/03|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|
|170323|konstruminus na Mazowszu; dość autonomiczny jak na syntetycznego terminusa. O dziwo, pomocny i proaktywny.|[Oszczędzili na rurach](/rpg/inwazja/opowiesci/konspekty/170323-oszczedzili-na-rurach.html)|10/02/20|10/02/24|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|3|[170326](/rpg/inwazja/opowiesci/konspekty/170326-cala-przeszlosc-splonela.html), [170325](/rpg/inwazja/opowiesci/konspekty/170325-klatwozyt-z-lustra.html), [170323](/rpg/inwazja/opowiesci/konspekty/170323-oszczedzili-na-rurach.html)|
|[Wiaczesław Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-wiaczeslaw-zajcew.html)|2|[170326](/rpg/inwazja/opowiesci/konspekty/170326-cala-przeszlosc-splonela.html), [170325](/rpg/inwazja/opowiesci/konspekty/170325-klatwozyt-z-lustra.html)|
|[Gabriel Dukat](/rpg/inwazja/opowiesci/karty-postaci/9999-gabriel-dukat.html)|2|[170326](/rpg/inwazja/opowiesci/konspekty/170326-cala-przeszlosc-splonela.html), [170325](/rpg/inwazja/opowiesci/konspekty/170325-klatwozyt-z-lustra.html)|
|[Archibald Składak](/rpg/inwazja/opowiesci/karty-postaci/9999-archibald-skladak.html)|2|[170326](/rpg/inwazja/opowiesci/konspekty/170326-cala-przeszlosc-splonela.html), [170323](/rpg/inwazja/opowiesci/konspekty/170323-oszczedzili-na-rurach.html)|
|[Tadeusz Kuraszewicz](/rpg/inwazja/opowiesci/karty-postaci/9999-tadeusz-kuraszewicz.html)|1|[170323](/rpg/inwazja/opowiesci/konspekty/170323-oszczedzili-na-rurach.html)|
|[Piotr Pyszny](/rpg/inwazja/opowiesci/karty-postaci/9999-piotr-pyszny.html)|1|[170323](/rpg/inwazja/opowiesci/konspekty/170323-oszczedzili-na-rurach.html)|
|[Maciej Brzydal](/rpg/inwazja/opowiesci/karty-postaci/9999-maciej-brzydal.html)|1|[170323](/rpg/inwazja/opowiesci/konspekty/170323-oszczedzili-na-rurach.html)|
|[Janusz Kocieł](/rpg/inwazja/opowiesci/karty-postaci/9999-janusz-kociel.html)|1|[170326](/rpg/inwazja/opowiesci/konspekty/170326-cala-przeszlosc-splonela.html)|
|[Herbert Zioło](/rpg/inwazja/opowiesci/karty-postaci/1709-herbert-ziolo.html)|1|[170325](/rpg/inwazja/opowiesci/konspekty/170325-klatwozyt-z-lustra.html)|
|[Grażyna Kępka](/rpg/inwazja/opowiesci/karty-postaci/9999-grazyna-kepka.html)|1|[170326](/rpg/inwazja/opowiesci/konspekty/170326-cala-przeszlosc-splonela.html)|
|[Elgieskład](/rpg/inwazja/opowiesci/karty-postaci/9999-elgiesklad.html)|1|[170323](/rpg/inwazja/opowiesci/konspekty/170323-oszczedzili-na-rurach.html)|

---
layout: inwazja-karta-postaci
categories: profile
title: "Cierń"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|140508|kot zabijający to co stoi mu na drodze.|['Lord Jonatan'](/rpg/inwazja/opowiesci/konspekty/140508-lord-jonatan.html)|10/01/07|10/01/08|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|140505|wspomnienie morderczego viciniusa kontrolowanego przez Kamilę|[Musiał zginąć, bo Maus](/rpg/inwazja/opowiesci/konspekty/140505-musial-zginac-bo-maus.html)|10/01/05|10/01/06|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Karradrael](/rpg/inwazja/opowiesci/karty-postaci/9999-karradrael.html)|2|[140508](/rpg/inwazja/opowiesci/konspekty/140508-lord-jonatan.html), [140505](/rpg/inwazja/opowiesci/konspekty/140505-musial-zginac-bo-maus.html)|
|[Jan Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-weiner.html)|2|[140508](/rpg/inwazja/opowiesci/konspekty/140508-lord-jonatan.html), [140505](/rpg/inwazja/opowiesci/konspekty/140505-musial-zginac-bo-maus.html)|
|[Andrea Wilgacz](/rpg/inwazja/opowiesci/karty-postaci/1709-andrea-wilgacz.html)|2|[140508](/rpg/inwazja/opowiesci/konspekty/140508-lord-jonatan.html), [140505](/rpg/inwazja/opowiesci/konspekty/140505-musial-zginac-bo-maus.html)|
|[Vuko Milić](/rpg/inwazja/opowiesci/karty-postaci/9999-vuko-milic.html)|1|[140505](/rpg/inwazja/opowiesci/konspekty/140505-musial-zginac-bo-maus.html)|
|[Tadeusz Baran](/rpg/inwazja/opowiesci/karty-postaci/1709-tadeusz-baran.html)|1|[140508](/rpg/inwazja/opowiesci/konspekty/140508-lord-jonatan.html)|
|[Tadeusz Aster](/rpg/inwazja/opowiesci/karty-postaci/9999-tadeusz-aster.html)|1|[140505](/rpg/inwazja/opowiesci/konspekty/140505-musial-zginac-bo-maus.html)|
|[Renata Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-renata-maus.html)|1|[140505](/rpg/inwazja/opowiesci/konspekty/140505-musial-zginac-bo-maus.html)|
|[Milena Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-milena-diakon.html)|1|[140508](/rpg/inwazja/opowiesci/konspekty/140508-lord-jonatan.html)|
|[Marian Welkrat](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-welkrat.html)|1|[140505](/rpg/inwazja/opowiesci/konspekty/140505-musial-zginac-bo-maus.html)|
|[Marian Agrest](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-agrest.html)|1|[140505](/rpg/inwazja/opowiesci/konspekty/140505-musial-zginac-bo-maus.html)|
|[Kwiatuszek](/rpg/inwazja/opowiesci/karty-postaci/9999-kwiatuszek.html)|1|[140505](/rpg/inwazja/opowiesci/konspekty/140505-musial-zginac-bo-maus.html)|
|[Kornelia Modrzejewska](/rpg/inwazja/opowiesci/karty-postaci/9999-kornelia-modrzejewska.html)|1|[140505](/rpg/inwazja/opowiesci/konspekty/140505-musial-zginac-bo-maus.html)|
|[Klotylda Świątek](/rpg/inwazja/opowiesci/karty-postaci/9999-klotylda-swiatek.html)|1|[140505](/rpg/inwazja/opowiesci/konspekty/140505-musial-zginac-bo-maus.html)|
|[Karol Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-karol-maus.html)|1|[140505](/rpg/inwazja/opowiesci/konspekty/140505-musial-zginac-bo-maus.html)|
|[Julia Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-julia-weiner.html)|1|[140508](/rpg/inwazja/opowiesci/konspekty/140508-lord-jonatan.html)|
|[Jonatan Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-jonatan-maus.html)|1|[140505](/rpg/inwazja/opowiesci/konspekty/140505-musial-zginac-bo-maus.html)|
|[Infensa Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-infensa-diakon.html)|1|[140505](/rpg/inwazja/opowiesci/konspekty/140505-musial-zginac-bo-maus.html)|
|[Hlargahlotl](/rpg/inwazja/opowiesci/karty-postaci/9999-hlargahlotl.html)|1|[140505](/rpg/inwazja/opowiesci/konspekty/140505-musial-zginac-bo-maus.html)|
|[Franciszek Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-maus.html)|1|[140505](/rpg/inwazja/opowiesci/konspekty/140505-musial-zginac-bo-maus.html)|
|[Ernest Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-ernest-maus.html)|1|[140505](/rpg/inwazja/opowiesci/konspekty/140505-musial-zginac-bo-maus.html)|
|[Cezary Sito](/rpg/inwazja/opowiesci/karty-postaci/9999-cezary-sito.html)|1|[140508](/rpg/inwazja/opowiesci/konspekty/140508-lord-jonatan.html)|
|[Bolesław Derwisz](/rpg/inwazja/opowiesci/karty-postaci/9999-boleslaw-derwisz.html)|1|[140508](/rpg/inwazja/opowiesci/konspekty/140508-lord-jonatan.html)|
|[Aurelia Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-aurelia-maus.html)|1|[140505](/rpg/inwazja/opowiesci/konspekty/140505-musial-zginac-bo-maus.html)|
|[Adam Wołkowiec](/rpg/inwazja/opowiesci/karty-postaci/9999-adam-wolkowiec.html)|1|[140505](/rpg/inwazja/opowiesci/konspekty/140505-musial-zginac-bo-maus.html)|
|[Adam Pączek](/rpg/inwazja/opowiesci/karty-postaci/9999-adam-paczek.html)|1|[140508](/rpg/inwazja/opowiesci/konspekty/140508-lord-jonatan.html)|

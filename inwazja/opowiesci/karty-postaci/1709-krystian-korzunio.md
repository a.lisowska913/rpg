---
layout: inwazja-karta-postaci
categories: profile
factions: "Kły Kaina, Zajcew"
type: "NPC"
title: "Krystian Korzunio"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **BÓL: dług wdzięczności wobec Zajcewów**:
    * _Aspekty_: lojalny wobec Zajcewów
    * _Opis_: 
* **FIL: patriota**: 
    * _Aspekty_: 
    * _Opis_: 
* **MET: kolekcjoner**:
    * _Aspekty_: 
    * _Opis_: zawsze próbuje poszerzyć swoje wpływy
* **KLT: samotny wilk**:
    * _Aspekty_: pragmatyczny, ascetyczny, ostrożny, skryty
    * _Opis_: mówi tylko to co trzeba, bezwzględny, wie kiedy się wycofać, nie potrzeba mu wiele, bogactwa niewiele znaczą

### Umiejętności

* **członek SB**:
    * _Aspekty_: walka w mieście, przekupstwo i korumpowanie, fałszowanie śladów, zapewnia dyskrecję
    * _Opis_: 
* **włamywacz**: 
    * _Aspekty_: 
    * _Opis_: 
    
### Silne i słabe strony:

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

## Magia
**brak**

## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Zarobki

* ?

### Znam

* **kolegów z czasów służby w SB**:
    * _Aspekty_: haki na oficjeli w różnych miastach z czasów SB
    * _Opis_: 
* **Kły Kaina**:
    * _Aspekty_: 
    * _Opis_: kult do czyszczenia Maskarady

### Mam

* **sieć informatorów**:
    * _Aspekty_: wiecznie poszerzana
    * _Opis_: 
* **kontakty w świecie przestępczym Zajcewów**:
    * _Aspekty_: 
    * _Opis_: 
* **prywatny mały oddział (5-10) osób jako mięśnie**:
    * _Aspekty_: 
    * _Opis_: 
* **nieletalne uzbrojenie**:
    * _Aspekty_: baseball, nóż, gaz pieprzowy 
    * _Opis_: 
	
# Opis
Krystian Korzunio used to be an officer of internal affairs, atm he is 52 years old. He is a very dangerous man, specializing in “acquisition and problem-solving”. In the same time he does not want to actually hurt people. He simply performs his job and performs his job well. Used to be an inspector having the rank of a lieutenant. During that time he has managed to create a wide array of people whom he knows and who knew him. When he managed to find some supernatural stuff and when he’s paths crossed with the paths of Zajcew, he was conscripted and became their man. He still considers himself a Polish patriot, even if the faction he comes from is generally created by general Polish population, but he doesn’t care. Does not use firearms; prefers good old-fashioned baseball bats and knives. Avoids killing people and causing irreversible harm – he will not hesitate a moment if he is left without other options, though.

### Koncept

Wysokopoziomowy koncept postaci, 1-2 zdania.

### Motto

"Tekst"

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Ballada o duszy ognistej](/rpg/inwazja/opowiesci/konspekty/161018-ballada-o-duszy-ognistej.html)|dostał zabutelkowanego miecztapauka od Jeleny Zajcew. Na wszelki wypadek.|Prawdziwa natura Draceny|


## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|161002|który dał Paulinie spłacić dług by wyczyściła ewentualny aftermath po jego wyczyszczeniu wycieku syberionu.|[Wyciek syberionu](/rpg/inwazja/opowiesci/konspekty/161002-wyciek-syberionu.html)|10/07/11|10/07/13|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|
|161018|który nie jest pewny czy grupa sprzątająca wyciek syberionu zrobiła wszystko poprawnie, więc powiedział Jelenie o Ksenii.|[Ballada o duszy ognistej](/rpg/inwazja/opowiesci/konspekty/161018-ballada-o-duszy-ognistej.html)|10/07/02|10/07/04|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|
|160918|telefoniczny anioł Pauliny rozwiązujacy problem z policją i ochroną imprezy - wziął na siebie policję i półświatek.|[Walka o duszę Draceny](/rpg/inwazja/opowiesci/konspekty/160918-walka-o-dusze-draceny.html)|10/06/30|10/07/03|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|
|170423|któremu Paulina przekazała problemy powiązane z prawnymi aspektami Skażonego podziemnego źródła wody; zajmie się tym.|[Rozpad magii w Leere](/rpg/inwazja/opowiesci/konspekty/170423-rozpad-magii-w-leere.html)|10/06/24|10/06/27|[Ucieczka do Przodka](/rpg/inwazja/opowiesci/konspekty/kampania-ucieczka-do-przodka.html)|
|160117|wielki zwycięzca, który pozyskał Alicję Gąszcz i Łukasza Perkasa jako agentów. Pomścił, wyczyścił, wygrał i się wycofał. Zależy mu na dobrobycie ludzi.|[Muchy w sieci Korzunia](/rpg/inwazja/opowiesci/konspekty/160117-muchy-w-sieci-korzunia.html)|10/06/12|10/06/13|[Ucieczka do Przodka](/rpg/inwazja/opowiesci/konspekty/kampania-ucieczka-do-przodka.html)|
|160106|czyściciel który chciał ograniczyć ludzkie straty i który transportował Paulinę tam gdzie trzeba. Sojusznik Pauliny i 'agent' Joachima dowodzący akcją z wielu stron.|[...i kult zostaje rozgromiony](/rpg/inwazja/opowiesci/konspekty/160106-i-kult-zostaje-rozgromiony.html)|10/06/10|10/06/11|[Ucieczka do Przodka](/rpg/inwazja/opowiesci/konspekty/kampania-ucieczka-do-przodka.html)|
|151230|który wpierw ściągnął sektę a teraz pracując dla Joachima (i Pauliny) chce zniszczyć sektę. Ostrzegł przed atakiem na kościół i go bronił.|[Zajcew ze śmietnika partnerem...](/rpg/inwazja/opowiesci/konspekty/151230-zajcew-ze-smietnika-partnerem.html)|10/06/08|10/06/09|[Ucieczka do Przodka](/rpg/inwazja/opowiesci/konspekty/kampania-ucieczka-do-przodka.html)|
|151220|kontynuuje usuwanie śladów; min. sprowadził "Kły Kaina" do Przodka by uwiarygodnić "sektę" i działania Gali i wiły.|[Z Null Fieldem w garażu...](/rpg/inwazja/opowiesci/konspekty/151220-z-nullfieldem-w-garazu.html)|10/06/08|10/06/09|[Ucieczka do Przodka](/rpg/inwazja/opowiesci/konspekty/kampania-ucieczka-do-przodka.html)|
|151101|czyściciel Zajcewów, który na rozkaz Gali wysadził dwa całkowicie niewinne samochody.|[Mafia Gali w szpitalu](/rpg/inwazja/opowiesci/konspekty/151101-mafia-gali-w-szpitalu.html)|10/06/06|10/06/07|[Ucieczka do Przodka](/rpg/inwazja/opowiesci/konspekty/kampania-ucieczka-do-przodka.html)|
|151013|czyściciel Zajcewów, który próbuje posprzątać to co napaskudził Tymek i Gala. Zaczął od ściągnięcia "młodego wilka".|[Kontrolowany odwrót z zamtuza](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html)|10/06/04|10/06/05|[Ucieczka do Przodka](/rpg/inwazja/opowiesci/konspekty/kampania-ucieczka-do-przodka.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|9|[161002](/rpg/inwazja/opowiesci/konspekty/161002-wyciek-syberionu.html), [160918](/rpg/inwazja/opowiesci/konspekty/160918-walka-o-dusze-draceny.html), [170423](/rpg/inwazja/opowiesci/konspekty/170423-rozpad-magii-w-leere.html), [160117](/rpg/inwazja/opowiesci/konspekty/160117-muchy-w-sieci-korzunia.html), [160106](/rpg/inwazja/opowiesci/konspekty/160106-i-kult-zostaje-rozgromiony.html), [151230](/rpg/inwazja/opowiesci/konspekty/151230-zajcew-ze-smietnika-partnerem.html), [151220](/rpg/inwazja/opowiesci/konspekty/151220-z-nullfieldem-w-garazu.html), [151101](/rpg/inwazja/opowiesci/konspekty/151101-mafia-gali-w-szpitalu.html), [151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html)|
|[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/1709-maria-newa.html)|7|[161002](/rpg/inwazja/opowiesci/konspekty/161002-wyciek-syberionu.html), [170423](/rpg/inwazja/opowiesci/konspekty/170423-rozpad-magii-w-leere.html), [160106](/rpg/inwazja/opowiesci/konspekty/160106-i-kult-zostaje-rozgromiony.html), [151230](/rpg/inwazja/opowiesci/konspekty/151230-zajcew-ze-smietnika-partnerem.html), [151220](/rpg/inwazja/opowiesci/konspekty/151220-z-nullfieldem-w-garazu.html), [151101](/rpg/inwazja/opowiesci/konspekty/151101-mafia-gali-w-szpitalu.html), [151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html)|
|[Jerzy Karmelik](/rpg/inwazja/opowiesci/karty-postaci/9999-jerzy-karmelik.html)|4|[151230](/rpg/inwazja/opowiesci/konspekty/151230-zajcew-ze-smietnika-partnerem.html), [151220](/rpg/inwazja/opowiesci/konspekty/151220-z-nullfieldem-w-garazu.html), [151101](/rpg/inwazja/opowiesci/konspekty/151101-mafia-gali-w-szpitalu.html), [151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html)|
|[Bartosz Bławatek](/rpg/inwazja/opowiesci/karty-postaci/9999-bartosz-blawatek.html)|4|[160106](/rpg/inwazja/opowiesci/konspekty/160106-i-kult-zostaje-rozgromiony.html), [151230](/rpg/inwazja/opowiesci/konspekty/151230-zajcew-ze-smietnika-partnerem.html), [151220](/rpg/inwazja/opowiesci/konspekty/151220-z-nullfieldem-w-garazu.html), [151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html)|
|[Artur Kurczak](/rpg/inwazja/opowiesci/karty-postaci/9999-artur-kurczak.html)|4|[170423](/rpg/inwazja/opowiesci/konspekty/170423-rozpad-magii-w-leere.html), [151230](/rpg/inwazja/opowiesci/konspekty/151230-zajcew-ze-smietnika-partnerem.html), [151220](/rpg/inwazja/opowiesci/konspekty/151220-z-nullfieldem-w-garazu.html), [151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html)|
|[Łukasz Perkas](/rpg/inwazja/opowiesci/karty-postaci/9999-lukasz-perkas.html)|3|[160117](/rpg/inwazja/opowiesci/konspekty/160117-muchy-w-sieci-korzunia.html), [151230](/rpg/inwazja/opowiesci/konspekty/151230-zajcew-ze-smietnika-partnerem.html), [151220](/rpg/inwazja/opowiesci/konspekty/151220-z-nullfieldem-w-garazu.html)|
|[Joachim Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-zajcew.html)|3|[160106](/rpg/inwazja/opowiesci/konspekty/160106-i-kult-zostaje-rozgromiony.html), [151230](/rpg/inwazja/opowiesci/konspekty/151230-zajcew-ze-smietnika-partnerem.html), [151220](/rpg/inwazja/opowiesci/konspekty/151220-z-nullfieldem-w-garazu.html)|
|[Grażyna Tuloz](/rpg/inwazja/opowiesci/karty-postaci/9999-grazyna-tuloz.html)|3|[170423](/rpg/inwazja/opowiesci/konspekty/170423-rozpad-magii-w-leere.html), [160117](/rpg/inwazja/opowiesci/konspekty/160117-muchy-w-sieci-korzunia.html), [160106](/rpg/inwazja/opowiesci/konspekty/160106-i-kult-zostaje-rozgromiony.html)|
|[Alicja Gąszcz](/rpg/inwazja/opowiesci/karty-postaci/9999-alicja-gaszcz.html)|3|[160117](/rpg/inwazja/opowiesci/konspekty/160117-muchy-w-sieci-korzunia.html), [151230](/rpg/inwazja/opowiesci/konspekty/151230-zajcew-ze-smietnika-partnerem.html), [151220](/rpg/inwazja/opowiesci/konspekty/151220-z-nullfieldem-w-garazu.html)|
|[Wiesław Rekin](/rpg/inwazja/opowiesci/karty-postaci/9999-wieslaw-rekin.html)|2|[160106](/rpg/inwazja/opowiesci/konspekty/160106-i-kult-zostaje-rozgromiony.html), [151230](/rpg/inwazja/opowiesci/konspekty/151230-zajcew-ze-smietnika-partnerem.html)|
|[Tymoteusz Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-tymoteusz-maus.html)|2|[151101](/rpg/inwazja/opowiesci/konspekty/151101-mafia-gali-w-szpitalu.html), [151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html)|
|[Tomasz Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-tomasz-weiner.html)|2|[161002](/rpg/inwazja/opowiesci/konspekty/161002-wyciek-syberionu.html), [161018](/rpg/inwazja/opowiesci/konspekty/161018-ballada-o-duszy-ognistej.html)|
|[Tomasz Leżniak](/rpg/inwazja/opowiesci/karty-postaci/9999-tomasz-lezniak.html)|2|[160106](/rpg/inwazja/opowiesci/konspekty/160106-i-kult-zostaje-rozgromiony.html), [151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html)|
|[Ryszard Herman](/rpg/inwazja/opowiesci/karty-postaci/9999-ryszard-herman.html)|2|[151220](/rpg/inwazja/opowiesci/konspekty/151220-z-nullfieldem-w-garazu.html), [151101](/rpg/inwazja/opowiesci/konspekty/151101-mafia-gali-w-szpitalu.html)|
|[Marzena Dorszaj](/rpg/inwazja/opowiesci/karty-postaci/1709-marzena-dorszaj.html)|2|[151220](/rpg/inwazja/opowiesci/konspekty/151220-z-nullfieldem-w-garazu.html), [151101](/rpg/inwazja/opowiesci/konspekty/151101-mafia-gali-w-szpitalu.html)|
|[Maciej Orank](/rpg/inwazja/opowiesci/karty-postaci/9999-maciej-orank.html)|2|[160117](/rpg/inwazja/opowiesci/konspekty/160117-muchy-w-sieci-korzunia.html), [151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html)|
|[Halina Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-halina-weiner.html)|2|[161002](/rpg/inwazja/opowiesci/konspekty/161002-wyciek-syberionu.html), [161018](/rpg/inwazja/opowiesci/konspekty/161018-ballada-o-duszy-ognistej.html)|
|[Gala Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-gala-zajcew.html)|2|[151101](/rpg/inwazja/opowiesci/konspekty/151101-mafia-gali-w-szpitalu.html), [151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|2|[161002](/rpg/inwazja/opowiesci/konspekty/161002-wyciek-syberionu.html), [160918](/rpg/inwazja/opowiesci/konspekty/160918-walka-o-dusze-draceny.html)|
|[Anton Jesiotr](/rpg/inwazja/opowiesci/karty-postaci/9999-anton-jesiotr.html)|2|[161002](/rpg/inwazja/opowiesci/konspekty/161002-wyciek-syberionu.html), [161018](/rpg/inwazja/opowiesci/konspekty/161018-ballada-o-duszy-ognistej.html)|
|[Agnieszka Mariacka](/rpg/inwazja/opowiesci/karty-postaci/9999-agnieszka-mariacka.html)|2|[160106](/rpg/inwazja/opowiesci/konspekty/160106-i-kult-zostaje-rozgromiony.html), [151230](/rpg/inwazja/opowiesci/konspekty/151230-zajcew-ze-smietnika-partnerem.html)|
|[Zenobia Morwiczka](/rpg/inwazja/opowiesci/karty-postaci/9999-zenobia-morwiczka.html)|1|[170423](/rpg/inwazja/opowiesci/konspekty/170423-rozpad-magii-w-leere.html)|
|[Włodzimierz Jamnik](/rpg/inwazja/opowiesci/karty-postaci/9999-wlodzimierz-jamnik.html)|1|[160918](/rpg/inwazja/opowiesci/konspekty/160918-walka-o-dusze-draceny.html)|
|[Wojmił Rzeźniczek](/rpg/inwazja/opowiesci/karty-postaci/9999-wojmil-rzezniczek.html)|1|[170423](/rpg/inwazja/opowiesci/konspekty/170423-rozpad-magii-w-leere.html)|
|[Wiaczesław Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-wiaczeslaw-zajcew.html)|1|[170423](/rpg/inwazja/opowiesci/konspekty/170423-rozpad-magii-w-leere.html)|
|[Ochrona Kobra Przodek](/rpg/inwazja/opowiesci/karty-postaci/9999-ochrona-kobra-przodek.html)|1|[160106](/rpg/inwazja/opowiesci/konspekty/160106-i-kult-zostaje-rozgromiony.html)|
|[Niektarij Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-niektarij-zajcew.html)|1|[161018](/rpg/inwazja/opowiesci/konspekty/161018-ballada-o-duszy-ognistej.html)|
|[Netheria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-netheria-diakon.html)|1|[160918](/rpg/inwazja/opowiesci/konspekty/160918-walka-o-dusze-draceny.html)|
|[Najada Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-najada-diakon.html)|1|[160918](/rpg/inwazja/opowiesci/konspekty/160918-walka-o-dusze-draceny.html)|
|[Mirek Kujec](/rpg/inwazja/opowiesci/karty-postaci/9999-mirek-kujec.html)|1|[160117](/rpg/inwazja/opowiesci/konspekty/160117-muchy-w-sieci-korzunia.html)|
|[Michał Jesiotr](/rpg/inwazja/opowiesci/karty-postaci/9999-michal-jesiotr.html)|1|[161002](/rpg/inwazja/opowiesci/konspekty/161002-wyciek-syberionu.html)|
|[Marcin Puczek](/rpg/inwazja/opowiesci/karty-postaci/9999-marcin-puczek.html)|1|[160918](/rpg/inwazja/opowiesci/konspekty/160918-walka-o-dusze-draceny.html)|
|[Leopold Brzeszczot](/rpg/inwazja/opowiesci/karty-postaci/9999-leopold-brzeszczot.html)|1|[160117](/rpg/inwazja/opowiesci/konspekty/160117-muchy-w-sieci-korzunia.html)|
|[Lea Swoboda](/rpg/inwazja/opowiesci/karty-postaci/9999-lea-swoboda.html)|1|[151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html)|
|[Kurt Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-kurt-weiner.html)|1|[161002](/rpg/inwazja/opowiesci/konspekty/161002-wyciek-syberionu.html)|
|[Kira Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-kira-zajcew.html)|1|[161018](/rpg/inwazja/opowiesci/konspekty/161018-ballada-o-duszy-ognistej.html)|
|[Kinga Toczek](/rpg/inwazja/opowiesci/karty-postaci/9999-kinga-toczek.html)|1|[160918](/rpg/inwazja/opowiesci/konspekty/160918-walka-o-dusze-draceny.html)|
|[Kinga Kujec](/rpg/inwazja/opowiesci/karty-postaci/9999-kinga-kujec.html)|1|[160117](/rpg/inwazja/opowiesci/konspekty/160117-muchy-w-sieci-korzunia.html)|
|[Katarzyna Klicz](/rpg/inwazja/opowiesci/karty-postaci/9999-katarzyna-klicz.html)|1|[170423](/rpg/inwazja/opowiesci/konspekty/170423-rozpad-magii-w-leere.html)|
|[Kaja Odyniec](/rpg/inwazja/opowiesci/karty-postaci/9999-kaja-odyniec.html)|1|[170423](/rpg/inwazja/opowiesci/konspekty/170423-rozpad-magii-w-leere.html)|
|[Jerzy Pasznik](/rpg/inwazja/opowiesci/karty-postaci/9999-jerzy-pasznik.html)|1|[160918](/rpg/inwazja/opowiesci/konspekty/160918-walka-o-dusze-draceny.html)|
|[Jelena Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-jelena-zajcew.html)|1|[161018](/rpg/inwazja/opowiesci/konspekty/161018-ballada-o-duszy-ognistej.html)|
|[Janusz Karzeł](/rpg/inwazja/opowiesci/karty-postaci/9999-janusz-karzel.html)|1|[151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html)|
|[Ireneusz Przaśnik](/rpg/inwazja/opowiesci/karty-postaci/9999-ireneusz-przasnik.html)|1|[170423](/rpg/inwazja/opowiesci/konspekty/170423-rozpad-magii-w-leere.html)|
|[Hubert Rębski](/rpg/inwazja/opowiesci/karty-postaci/9999-hubert-rebski.html)|1|[160106](/rpg/inwazja/opowiesci/konspekty/160106-i-kult-zostaje-rozgromiony.html)|
|[Henryk Siwiecki](/rpg/inwazja/opowiesci/karty-postaci/1709-henryk-siwiecki.html)|1|[161018](/rpg/inwazja/opowiesci/konspekty/161018-ballada-o-duszy-ognistej.html)|
|[Franciszek Marlin](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-marlin.html)|1|[151013](/rpg/inwazja/opowiesci/konspekty/151013-kontrolowany-odwrot-zamtuza.html)|
|[Filip Wichoszczyk](/rpg/inwazja/opowiesci/karty-postaci/9999-filip-wichoszczyk.html)|1|[170423](/rpg/inwazja/opowiesci/konspekty/170423-rozpad-magii-w-leere.html)|
|[Filip Brzeszczot](/rpg/inwazja/opowiesci/karty-postaci/9999-filip-brzeszczot.html)|1|[160117](/rpg/inwazja/opowiesci/konspekty/160117-muchy-w-sieci-korzunia.html)|
|[Ernest Brzeszczot](/rpg/inwazja/opowiesci/karty-postaci/9999-ernest-brzeszczot.html)|1|[160117](/rpg/inwazja/opowiesci/konspekty/160117-muchy-w-sieci-korzunia.html)|
|[Edward Ramuel](/rpg/inwazja/opowiesci/karty-postaci/9999-edward-ramuel.html)|1|[170423](/rpg/inwazja/opowiesci/konspekty/170423-rozpad-magii-w-leere.html)|
|[Damazy Rozenblum](/rpg/inwazja/opowiesci/karty-postaci/9999-damazy-rozenblum.html)|1|[161018](/rpg/inwazja/opowiesci/konspekty/161018-ballada-o-duszy-ognistej.html)|
|[Bolesław Derwisz](/rpg/inwazja/opowiesci/karty-postaci/9999-boleslaw-derwisz.html)|1|[161002](/rpg/inwazja/opowiesci/konspekty/161002-wyciek-syberionu.html)|

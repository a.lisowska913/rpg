---
layout: inwazja-karta-postaci
categories: profile
factions: "Kompania Najemna Myszeczków"
type: "PC"
owner: "kić"
title: "Esme Myszeczka"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **BÓL: Zubożały ród Myszeczków**:
    * _Aspekty_: oddbudwać pozycję rodu
    * _Opis_: Nie supremacjonistka, jednak uważa, że ród Myszeczków został wyjątkowo skrzywdzony i zepchnięty z należnego mu miejsca. Niechętna wobec Mausów, zrobi co w jej mocy, aby nazwisko Myszeczka budziło równy szacunek jak dawniej


### Umiejętności

* **Żołnierz**:
    * _Aspekty_: ciężkie wsparcie, walka w zwarciu, walka bronią ludzką, brawler
    * _Opis_: ??
* **Oficer**: 
    * _Aspekty_: taktyk, przesłuchiwanie, autorytet oficera
    * _Opis_: ??
* **Manipulator**: 
    * _Aspekty_: gierki umysłu, zrobisz co zechcę, 
    * _Opis_: ??
* **Polityk**:
    * _Aspekty_: polityka rodu Myszeczka, koneksje i powiązania rodu, haczyk na każdego
    * _Opis_: ??
    
### Silne i słabe strony:

* **Opinia solidnego najemnika**: 
    * _Aspekty_: ??
    * _Opis_: ??
* **??**: 
    * _Aspekty_: ??
    * _Opis_: ??
    
## Magia

### Szkoły magiczne

* **Magia mentalna**:
    * _Aspekty_: zaklęcia obezwładniające, modyfikacja pamięci, aury mentalne
    * _Opis_: ?
* **Magia materii**: 
    * _Aspekty_: zaklęcia bojowe, osłony, tarcze, stworzenie broni
    * _Opis_: ?
* **Magia elementalna**: 
    * _Aspekty_: zaklęcia artyleryjskie
    * _Opis_: ?

### Zaklęcia statyczne

* **?**:
    * _Aspekty_: ?
    * _Opis_: ?


## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Zarobki

* **najemniczka**: z kompanii najemniczej

### Znam

* **Społeczność najemników**:
    * _Aspekty_: bojowe wsparcie, 
    * _Opis_: ??
* **Kluby magiczne**:
    * _Aspekty_: 
    * _Opis_: Zanim zaciągnęła się do kompanii, wynajmnowała się do różnych robótek, w tym do ochrony.

	
### Mam

* **Oficer kompanii**:
    * _Aspekty_: ciężki sprzęt, wyposażenie klasy terminusa
    * _Opis_: ??

## Inne:


## Motto

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|160922|uknuła plan, zwiotczyła Supernową i dała się pokonać księgowemu. Też: powiedziała Kornelowi o losie Izy (i info o stanie Kornela dalej).|[Czarnoskalski konwent RPG](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|10/05/14|10/05/19|[Taniec Liści](/rpg/inwazja/opowiesci/konspekty/kampania-taniec-lisci.html)|
|150602|pomysłowa najemniczka która każdego otruje swoimi dziwnymi specyfikami. Ranna po infiltracji hotelu 'Luksus'.|[Esme, najemniczka Netherii](/rpg/inwazja/opowiesci/konspekty/150602-esme-najemniczka-netherii.html)|10/02/19|10/02/20|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|
|141006|najemniczka trzymająca się dość daleko polityki rodu|[Klinika 'Słonecznik'](/rpg/inwazja/opowiesci/konspekty/141006-klinika-slonecznik.html)|10/01/05|10/01/06|[Blakenbauerowie x Skorpion](/rpg/inwazja/opowiesci/konspekty/kampania-blakenbauerowie-x-skorpion.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Zofia Perszen](/rpg/inwazja/opowiesci/karty-postaci/9999-zofia-perszen.html)|1|[150602](/rpg/inwazja/opowiesci/konspekty/150602-esme-najemniczka-netherii.html)|
|[Wiktor Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-wiktor-diakon.html)|1|[141006](/rpg/inwazja/opowiesci/konspekty/141006-klinika-slonecznik.html)|
|[Tymotheus Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-tymotheus-blakenbauer.html)|1|[150602](/rpg/inwazja/opowiesci/konspekty/150602-esme-najemniczka-netherii.html)|
|[Tadeusz Umiej](/rpg/inwazja/opowiesci/karty-postaci/9999-tadeusz-umiej.html)|1|[150602](/rpg/inwazja/opowiesci/konspekty/150602-esme-najemniczka-netherii.html)|
|[Tadeusz Baran](/rpg/inwazja/opowiesci/karty-postaci/1709-tadeusz-baran.html)|1|[141006](/rpg/inwazja/opowiesci/konspekty/141006-klinika-slonecznik.html)|
|[Supernowa Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-supernowa-diakon.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Staszek Perszen](/rpg/inwazja/opowiesci/karty-postaci/9999-staszek-perszen.html)|1|[150602](/rpg/inwazja/opowiesci/konspekty/150602-esme-najemniczka-netherii.html)|
|[Rafał Kniaź](/rpg/inwazja/opowiesci/karty-postaci/9999-rafal-kniaz.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Patryk Romczak](/rpg/inwazja/opowiesci/karty-postaci/9999-patryk-romczak.html)|1|[150602](/rpg/inwazja/opowiesci/konspekty/150602-esme-najemniczka-netherii.html)|
|[Onufry Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-onufry-maus.html)|1|[141006](/rpg/inwazja/opowiesci/konspekty/141006-klinika-slonecznik.html)|
|[Netheria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-netheria-diakon.html)|1|[150602](/rpg/inwazja/opowiesci/konspekty/150602-esme-najemniczka-netherii.html)|
|[Mirabelka Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-mirabelka-diakon.html)|1|[150602](/rpg/inwazja/opowiesci/konspekty/150602-esme-najemniczka-netherii.html)|
|[Mikado Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-mikado-diakon.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Mateusz Ackmann](/rpg/inwazja/opowiesci/karty-postaci/1709-mateusz-ackmann.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Luksja Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-luksja-diakon.html)|1|[150602](/rpg/inwazja/opowiesci/konspekty/150602-esme-najemniczka-netherii.html)|
|[Kornel Wadera](/rpg/inwazja/opowiesci/karty-postaci/1709-kornel-wadera.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Konrad Węgorz](/rpg/inwazja/opowiesci/karty-postaci/9999-konrad-wegorz.html)|1|[150602](/rpg/inwazja/opowiesci/konspekty/150602-esme-najemniczka-netherii.html)|
|[Jessica Ruczaj](/rpg/inwazja/opowiesci/karty-postaci/9999-jessica-ruczaj.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Infensa Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-infensa-diakon.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Henryk Waciak](/rpg/inwazja/opowiesci/karty-postaci/9999-henryk-waciak.html)|1|[141006](/rpg/inwazja/opowiesci/konspekty/141006-klinika-slonecznik.html)|
|[Gabriel Newa](/rpg/inwazja/opowiesci/karty-postaci/9999-gabriel-newa.html)|1|[150602](/rpg/inwazja/opowiesci/konspekty/150602-esme-najemniczka-netherii.html)|
|[Estera Piryt](/rpg/inwazja/opowiesci/karty-postaci/9999-estera-piryt.html)|1|[141006](/rpg/inwazja/opowiesci/konspekty/141006-klinika-slonecznik.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|1|[141006](/rpg/inwazja/opowiesci/konspekty/141006-klinika-slonecznik.html)|
|[Dorota Gacek](/rpg/inwazja/opowiesci/karty-postaci/1709-dorota-gacek.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Dalia Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-dalia-weiner.html)|1|[141006](/rpg/inwazja/opowiesci/konspekty/141006-klinika-slonecznik.html)|
|[Cyprian Koziej](/rpg/inwazja/opowiesci/karty-postaci/9999-cyprian-koziej.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Crystal Shard](/rpg/inwazja/opowiesci/karty-postaci/9999-crystal-shard.html)|1|[150602](/rpg/inwazja/opowiesci/konspekty/150602-esme-najemniczka-netherii.html)|
|[Bolesław Derwisz](/rpg/inwazja/opowiesci/karty-postaci/9999-boleslaw-derwisz.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Artur Pawiak](/rpg/inwazja/opowiesci/karty-postaci/9999-artur-pawiak.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Antoni Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/9999-antoni-myszeczka.html)|1|[141006](/rpg/inwazja/opowiesci/konspekty/141006-klinika-slonecznik.html)|
|[Anabela Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-anabela-diakon.html)|1|[141006](/rpg/inwazja/opowiesci/konspekty/141006-klinika-slonecznik.html)|

---
layout: inwazja-karta-postaci
categories: profile
factions: "Millennium"
type: "NPC"
title: "Amanda Diakon"
---
# {{ page.title }}

## Postać

### Motywacje (do czego dąży)

* **FIL: Blitzkrieg**:
    * _Aspekty_: 
    * _Opis_: uderz i się wycofaj zanim przeciwnik się zorientuje
* **MET: Pragmatyzm**:
    * _Aspekty_: nie zajmuję się tematami nieistotnymi; od tego są inni, najlepiej, jeżeli inni będą rozwiązywali moje problemy
    * _Opis_: 
* **KLT: Lojalność**:
    * _Aspekty_: wypowiedziane słowa muszą być dotrzymane
    * _Opis_: 	

### Umiejętności

* **Przywódca**:
    * _Aspekty_: członek Triumwiratu, taktyk, polityk, 
    * _Opis_: 
* **Wojowniczka**:
    * _Aspekty_: walka w zwarciu (bez broni)
    * _Opis_: 
* **Diakon**:
    * _Aspekty_: manipulatorka, uwodzicielka, trucizny, inkarnacja koszmarów sennych
    * _Opis_: 
	
### Silne i słabe strony:

* **Kratlothborn**:
    * _Aspekty_: odporna na większość trucizn, vicinius złożony z pająków, częściowo nieludzka, wydziela niebezpieczne trucizny, jeśli pająk wlezie ona też
    * _Opis_: 
	
## Magia

### Szkoły magiczne

* **Biomancja**:
    * _Aspekty_: 
    * _Opis_: 

### Zaklęcia statyczne

* **??**:
    * _Aspekty_: 
    * _Opis_: 

## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Zarobki


### Znam

* **Przywódczyni Toksycznych Templariuszy**:
    * _Aspekty_: 
    * _Opis_: 
* **Jedna z Triumwiratu Millennium**:
    * _Aspekty_: 
    * _Opis_: 
### Mam

* **Przywódczyni Toksycznych Templariuszy**:
    * _Aspekty_: 
    * _Opis_: 
* **Jedna z Triumwiratu Millennium**:
    * _Aspekty_: 
    * _Opis_: 	

# Opis

Amanda. One of the Triumvirate, the Spider Queen, she is a very, very augmented mage. After the Eclipse her power has grown considerably and as she is a kralothborn, her body is her perfect tool. Things which normal humans would not be able to actually do, both on physical and mental levels, are completely at her disposal.

### Koncept

Wysokopoziomowy koncept postaci, 1-2 zdania.

### Motto

"Tekst"

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170226|która jest rozdarta między próbą porywania i badania (lub się znęcania) Mausów a próbą zyskania w tych trudnych czasach Mausich sojuszników. Wybrała drugie pod wpływem Andrei.|[Wygraliśmy wojnę... prawda?](/rpg/inwazja/opowiesci/konspekty/170226-wygralismy-wojne-prawda.html)|10/08/18|10/08/20|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170122|która w imieniu rodu Diakon wsparła z całej siły Anetę Rainer. Z wielkimi kosztami, trzyma Kopalin i pomaga odzyskać Kompleks.|[Gambit Anety Rainer](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html)|10/07/19|10/07/23|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160420|która wmanewrowała Blakenbauerów w zaatakowanie przez nich budynku z przekaźnikami energii; odstąpiła mogąc zastawić inną pułapkę.|[Kolizja dwóch sojuszy](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html)|10/07/01|10/07/02|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160316|jedna z Triumwiratu (kralothborn), która dowodzi operacją mającą wprowadzić potęgę Millennium do Kopalina korzystając z Irytki.|[Frontalne wejście Millennium](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html)|10/06/27|10/06/28|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160506|dowodzi stabilizacją Kopalina z ramienia Millennium i współpracuje z Hektorem i Emilią. Odzyskała cruentus reverto.|[Wyścig pająka z terminuską](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|10/06/22|10/06/24|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160424|która ustaliła zasady pokoju z Ozydiuszem przez Andreę i gdy poszła na ustalenia, Ozydiusz zginął. Jej siły opanowały sytuację i skomunikowała się z Andreą.|[Uważaj, o czym marzysz](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|10/06/18|10/06/21|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|151014|jedna z Triumwiratu Rodu Diakonów; nie wiedziała o stanie Mileny Diakon i zajęła się tym, dlaczego nie wiedziała.|[Jedno słowo prawdy za dużo](/rpg/inwazja/opowiesci/konspekty/151014-jedno-slowo-prawdy-za-duzo.html)|10/05/31|10/06/01|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|140708|głównodowodząca Millennium do spraw walki z Inwazją z uwagi na najwyższy poziom nienaturalności.|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|10/01/21|10/01/22|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|6|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [160316](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html), [151014](/rpg/inwazja/opowiesci/konspekty/151014-jedno-slowo-prawdy-za-duzo.html), [140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Wiktor Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1709-wiktor-sowinski.html)|4|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html), [151014](/rpg/inwazja/opowiesci/konspekty/151014-jedno-slowo-prawdy-za-duzo.html)|
|[Marcelin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-marcelin-blakenbauer.html)|4|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [160316](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html), [140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Aneta Rainer](/rpg/inwazja/opowiesci/karty-postaci/1709-aneta-rainer.html)|4|[170226](/rpg/inwazja/opowiesci/konspekty/170226-wygralismy-wojne-prawda.html), [170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html), [160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Andrea Wilgacz](/rpg/inwazja/opowiesci/karty-postaci/1709-andrea-wilgacz.html)|4|[170226](/rpg/inwazja/opowiesci/konspekty/170226-wygralismy-wojne-prawda.html), [170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html), [140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Tatiana Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-tatiana-zajcew.html)|3|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html), [151014](/rpg/inwazja/opowiesci/konspekty/151014-jedno-slowo-prawdy-za-duzo.html), [140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Ozydiusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-ozydiusz-bankierz.html)|3|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [160316](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Mieszko Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-mieszko-bankierz.html)|3|[170226](/rpg/inwazja/opowiesci/konspekty/170226-wygralismy-wojne-prawda.html), [170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html), [140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Klara Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-klara-blakenbauer.html)|3|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [160316](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Emilia Kołatka](/rpg/inwazja/opowiesci/karty-postaci/1709-emilia-kolatka.html)|3|[160316](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html), [151014](/rpg/inwazja/opowiesci/konspekty/151014-jedno-slowo-prawdy-za-duzo.html)|
|[Elea Maus](/rpg/inwazja/opowiesci/karty-postaci/1802-elea-maus.html)|3|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Dagmara Wyjątek](/rpg/inwazja/opowiesci/karty-postaci/1709-dagmara-wyjątek.html)|3|[160316](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Vladlena Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-vladlena-zjacew.html)|2|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html), [151014](/rpg/inwazja/opowiesci/konspekty/151014-jedno-slowo-prawdy-za-duzo.html)|
|[Tymotheus Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-tymotheus-blakenbauer.html)|2|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [151014](/rpg/inwazja/opowiesci/konspekty/151014-jedno-slowo-prawdy-za-duzo.html)|
|[Stalowy Śledzik Żarłacz](/rpg/inwazja/opowiesci/karty-postaci/9999-stalowy-sledzik-zarlacz.html)|2|[170226](/rpg/inwazja/opowiesci/konspekty/170226-wygralismy-wojne-prawda.html), [170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html)|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|2|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html), [151014](/rpg/inwazja/opowiesci/konspekty/151014-jedno-slowo-prawdy-za-duzo.html)|
|[Rudolf Jankowski](/rpg/inwazja/opowiesci/karty-postaci/9999-rudolf-jankowski.html)|2|[170226](/rpg/inwazja/opowiesci/konspekty/170226-wygralismy-wojne-prawda.html), [170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html)|
|[Otton Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-otton-blakenbauer.html)|2|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html), [151014](/rpg/inwazja/opowiesci/konspekty/151014-jedno-slowo-prawdy-za-duzo.html)|
|[Marian Agrest](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-agrest.html)|2|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Margaret Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-margaret-blakenbauer.html)|2|[160316](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Leonidas Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-leonidas-blakenbauer.html)|2|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [160316](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html)|
|[Kirył Sjeld](/rpg/inwazja/opowiesci/karty-postaci/9999-kiryl-sjeld.html)|2|[170226](/rpg/inwazja/opowiesci/konspekty/170226-wygralismy-wojne-prawda.html), [170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html)|
|[Kajetan Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-kajetan-weiner.html)|2|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|2|[151014](/rpg/inwazja/opowiesci/konspekty/151014-jedno-slowo-prawdy-za-duzo.html), [140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Draconis Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-draconis-diakon.html)|2|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html), [140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Świeży Lilak](/rpg/inwazja/opowiesci/karty-postaci/9999-swiezy-lilak.html)|1|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html)|
|[Wioletta Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-wioletta-bankierz.html)|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Whisperwind](/rpg/inwazja/opowiesci/karty-postaci/9999-whisperwind.html)|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Wanda Ketran](/rpg/inwazja/opowiesci/karty-postaci/1709-wanda-ketran.html)|1|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Wacław Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-waclaw-zajcew.html)|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Teresa Żyraf](/rpg/inwazja/opowiesci/karty-postaci/9999-teresa-zyraf.html)|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Tadeusz Baran](/rpg/inwazja/opowiesci/karty-postaci/1709-tadeusz-baran.html)|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Szczepan Sowiński](/rpg/inwazja/opowiesci/karty-postaci/9999-szczepan-sowinski.html)|1|[170226](/rpg/inwazja/opowiesci/konspekty/170226-wygralismy-wojne-prawda.html)|
|[Swietłana Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-swietlana-zajcew.html)|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Sebastian Tecznia](/rpg/inwazja/opowiesci/karty-postaci/9999-sebastian-tecznia.html)|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Sabina Sowińska](/rpg/inwazja/opowiesci/karty-postaci/1709-sabina-sowinska.html)|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Ryszard Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-ryszard-weiner.html)|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Rufus Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-rufus-maus.html)|1|[170226](/rpg/inwazja/opowiesci/konspekty/170226-wygralismy-wojne-prawda.html)|
|[Rodion Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-rodion-zajcew.html)|1|[170226](/rpg/inwazja/opowiesci/konspekty/170226-wygralismy-wojne-prawda.html)|
|[Quasar](/rpg/inwazja/opowiesci/karty-postaci/9999-quasar.html)|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|1|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html)|
|[Patryk Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-patryk-maus.html)|1|[170226](/rpg/inwazja/opowiesci/konspekty/170226-wygralismy-wojne-prawda.html)|
|[Pasożyt Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-pasozyt-diakon.html)|1|[160316](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html)|
|[Olga Miodownik](/rpg/inwazja/opowiesci/karty-postaci/1709-olga-miodownik.html)|1|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html)|
|[Mirabelka Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-mirabelka-diakon.html)|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Milena Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-milena-diakon.html)|1|[151014](/rpg/inwazja/opowiesci/konspekty/151014-jedno-slowo-prawdy-za-duzo.html)|
|[Malwina Krówka](/rpg/inwazja/opowiesci/karty-postaci/9999-malwina-krowka.html)|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Laurena Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-laurena-bankierz.html)|1|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html)|
|[Laragnarhag](/rpg/inwazja/opowiesci/karty-postaci/1709-laragnarhag.html)|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Krystalia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-krystalia-diakon.html)|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Kleofas Bór](/rpg/inwazja/opowiesci/karty-postaci/1709-kleofas-bor.html)|1|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Katalina Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-katalina-bankierz.html)|1|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html)|
|[Karradrael](/rpg/inwazja/opowiesci/karty-postaci/9999-karradrael.html)|1|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html)|
|[Julian Pszczelak](/rpg/inwazja/opowiesci/karty-postaci/1709-julian-pszczelak.html)|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Judyta Karnisz](/rpg/inwazja/opowiesci/karty-postaci/9999-judyta-karnisz.html)|1|[160316](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html)|
|[Jan Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-weiner.html)|1|[170226](/rpg/inwazja/opowiesci/konspekty/170226-wygralismy-wojne-prawda.html)|
|[Irina Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-irina-zajcew.html)|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Ika](/rpg/inwazja/opowiesci/karty-postaci/9999-ika.html)|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Grzegorz Czerwiec](/rpg/inwazja/opowiesci/karty-postaci/9999-grzegorz-czerwiec.html)|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Franciszek Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-myszeczka.html)|1|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html)|
|[Fortitia Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-fortitia-diakon.html)|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Estera Piryt](/rpg/inwazja/opowiesci/karty-postaci/9999-estera-piryt.html)|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Elizawieta Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-elizawieta-zajcew.html)|1|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Edward Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-edward-diakon.html)|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Diana Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-diana-weiner.html)|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Bianka Stein](/rpg/inwazja/opowiesci/karty-postaci/1709-bianka-stein.html)|1|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html)|
|[Benjamin Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-benjamin-zajcew.html)|1|[151014](/rpg/inwazja/opowiesci/konspekty/151014-jedno-slowo-prawdy-za-duzo.html)|
|[Baltazar Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-baltazar-maus.html)|1|[170226](/rpg/inwazja/opowiesci/konspekty/170226-wygralismy-wojne-prawda.html)|
|[Aurel Czarko](/rpg/inwazja/opowiesci/karty-postaci/1709-aurel-czarko.html)|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Arkadiusz Klusiński](/rpg/inwazja/opowiesci/karty-postaci/9999-arkadiusz-klusinski.html)|1|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Arazille](/rpg/inwazja/opowiesci/karty-postaci/9999-arazille.html)|1|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html)|
|[Anastazja Sjeld](/rpg/inwazja/opowiesci/karty-postaci/9999-anastazja-sjeld.html)|1|[170122](/rpg/inwazja/opowiesci/konspekty/170122-gambit-anety-rainer.html)|
|[Aleksander Sowiński](/rpg/inwazja/opowiesci/karty-postaci/9999-aleksander-sowinski.html)|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Adrian Murarz](/rpg/inwazja/opowiesci/karty-postaci/9999-adrian-murarz.html)|1|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Abelard Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-abelard-maus.html)|1|[170226](/rpg/inwazja/opowiesci/konspekty/170226-wygralismy-wojne-prawda.html)|

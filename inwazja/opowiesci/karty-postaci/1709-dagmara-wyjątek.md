---
layout: inwazja-karta-postaci
categories: profile
factions: "Srebrna Świeca"
type: "NPC"
title: "Dagmara Wyjątek"
---
# {{ page.title }}

## Postać

### Motywacje (do czego dąży)

* **MET: Zawzięta**:
    * _Aspekty_: bardzo pracowita, nieustępliwa
    * _Opis_: 
* **BÓL: Samotna**:
    * _Aspekty_: 
    * _Opis_: 
* **MET: Lojalna**:
    * _Aspekty_: 
    * _Opis_: 
* **MET: Self-loathing**:
    * _Aspekty_: 
    * _Opis_: 

### Umiejętności

* **Polityk**:
    * _Aspekty_: polityczne realia Świecy
    * _Opis_: 
* **Terminus**:
    * _Aspekty_: oficer taktyczny, strateg militarny, ciężka broń dwuręczna, medycyna polowa, zwiad
    * _Opis_: 
    
### Silne i słabe strony:

* **Obniżona moc magiczna**:
    * _Aspekty_:
    * _Opis_: 
* **Regeneracja i odporność na ból**:
    * _Aspekty_:  +2 testy defensywne na ciało (fort), hard to put down
    * _Opis_: 
* **Niezgodny z magiem biotyp**:
    * _Aspekty_: 
    * _Opis_: Aquam Vim jak vicinius, nie jak mag
* **Pokryta bliznami**:
    * _Aspekty_: malus do testów atrakcyjności, łatwiej zastrasza
    * _Opis_: 	
	

## Magia

### Szkoły magiczne

* **Biomancja**:
    * _Aspekty_: 
    * _Opis_: 
* **Kataliza**:
    * _Aspekty_: rekonstrukcja artefaktów, regeneracja artefaktów, artifact overloading
    * _Opis_: 
* **Magia materii**:
    * _Aspekty_: odbudowa przedmiotów
    * _Opis_: 
	
### Zaklęcia statyczne

* **??**:
    * _Aspekty_: 
    * _Opis_: 

	
## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Zarobki


### Znam

* **??**:
    * _Aspekty_: 
    * _Opis_: 

	
### Mam

* **Dwuręczny artefaktyczny miecz bojowy**:
    * _Aspekty_: 
    * _Opis_: 
* **Ciężki pancerz szturmowy**:
    * _Aspekty_: 
    * _Opis_: 

# Opis

Dagmara pochodzi z rodu Zajcew, ale odrzuciła nazwisko i przywileje. Uszkodziła swój wzór, by stracić ród. Podczas Zaćmienia jej moc oszalała i zniszczyła większość swojej rodziny. Do dzisiaj nie potrafi sobie wybaczyć. Nadal jest w niej pasja i niezłomność każąca jej żyć i walczyć dalej, choć nie potrafi zapomnieć tego, co się stało. Z tego co wiadomo, jej wzór został ustabilizowany przez Laboratorium Mgieł; było to niezmiernie kosztowne ale pomogło jej i umożliwiło jej dalszą aktywną służbę. Wszystko wskazuje na to, że to właśnie Wiktor zapłacił za jej transformację. Dagmara jest członkiem Szlachty; jakkolwiek jej moc magiczna jest znacznie słabsza niż normalnego maga (nie ma bonusu do magii), wykorzystuje umiejętności które uzyskała w Laboratorium Mgieł i wykorzystuje swój umysł tam, gdzie ciało zawodzi. Nieskończenie lojalna Wiktorowi. Millennium ostrzega przed Dagmarą; jej poziom natywny jest bliżej viciniusa niż maga.

### Koncept

Wysokopoziomowy koncept postaci, 1-2 zdania.

### Motto

"Tekst"

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Wycofanie Mileny z piekła](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html)|straciła Milenę Diakon|Powrót Karradraela|
|[Wycofanie Mileny z piekła](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html)|zdobyła kontrolę nad Kompleksem Centralnym|Powrót Karradraela|


## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|161012|podsłuchała uszami Mausów fałszywy plan Hektora i Emilii, zaatakowała Emilię... a to była saith Kameleon. KIA.|[Kontratak Karradraela](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|10/07/22|10/07/23|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170101|z badań Wioletty, Pszczelaka, Laragnarhaga wynika, że ona silnie wpływa na działania Karradraela w Kompleksie Centralnym. Jej psychika. Agrest już wie i może pułapkować.|[Patrol? Kralotyczne maskowanie!](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|10/07/13|10/07/15|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160412|nosząca krwawy power suit i nie do końca będąca sobą; w pełni ufa jej Wiktor. Przekształciła Sabinę dla Wiktora i ominęła problem zaawansowanego hipernetu.|[Spleśniała dusza terminuski](/rpg/inwazja/opowiesci/konspekty/160412-splesniala-dusza-terminuski.html)|10/07/01|10/07/02|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160316|taktyk Szlachty. Oficer terminus, specjalizuje się w manewrach militarnych i skutecznie masakruje Kurtynę odcinając ich do bazy Emilii.|[Frontalne wejście Millennium](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html)|10/06/27|10/06/28|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160821|która praktycznie przejęła kontrolę nad Kompleksem Centralnym przy nie zauważeniu ze strony Wiktora|[Wycofanie Mileny z piekła](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html)|10/06/26|10/06/30|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160404|która zaczyna mieć wątpliwości co do Silurii... she is... cute. I po* maga Wioletcie.|[The power of cute pet](/rpg/inwazja/opowiesci/konspekty/160404-the-power-of-cute-pet.html)|10/06/25|10/06/26|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160309|oficer taktyczny Szlachty, która zdobyła i prawie okopała się w Muzeum. Niestety dla niej, jej plany zostały zniwelowane sustrommingiem.|[Irytka Sprzężona](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|10/06/25|10/06/26|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160724|która prawie dostała się na EAM przez specjalną Bramę; złapała Biankę i Judytę. Nadal wykonuje rozkazy Wiktora. Jeszcze.|[Portal do EAM](/rpg/inwazja/opowiesci/konspekty/160724-portal-do-eam.html)|10/06/24|10/06/25|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160403|nadal zwalcza Silurię. Nadal Wiktor ją ignoruje. Jej pozycja słabnie z dnia na dzień.|[Wiktor kontra Kadem i Świeca](/rpg/inwazja/opowiesci/konspekty/160403-wiktor-kontra-kadem-i-swieca.html)|10/06/23|10/06/24|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160723|w której niewiele ludzkich cech już zostało. Dowodzi operacją przejęcia Kompleksu Centralnego.|[Czyj Jest Kompleks Centralny](/rpg/inwazja/opowiesci/konspekty/160723-czyj-jest-kompleks-centralny.html)|10/06/22|10/06/23|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160506|potencjalna Regent Spustoszenia na Kopalin i osoba, którą trzeba by złapać w pułapkę... chciała zdobyć cruentus reverto, ale nie zdążyła.|[Wyścig pająka z terminuską](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|10/06/22|10/06/24|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160424|która jest przerażona tym czym się stała i co robi. Spustoszyła Aleksandra i Sabinę, przejęła komunikację z Mirabelką i dowodziła operacją... dla Wiktora? Odpycha przyjaciół.|[Uważaj, o czym marzysz](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|10/06/18|10/06/21|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160327|pozycjonująca się przeciwko Silurii taktyk Szlachty. Próbuje chronić Wiolettę i Wiktora przed Silurią. Fanatycznie wierna Wiktorowi, ma ogromny kompleks winy.|[Piećdziesiąt oblicz Szlachty](/rpg/inwazja/opowiesci/konspekty/160327-piecdziesiat-oblicz-szlachty.html)|10/06/14|10/06/23|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160326|terminuska z bliznami; bardzo nieugięta i podkochująca się (mocno) w Wiktorze. Rozpaczliwie chce się wykazać. Lojalna ponad wszystko. Robi co chce. Potrafi manipulować, choć udaje cegłę.|[Siluria na salonach Szlachty](/rpg/inwazja/opowiesci/konspekty/160326-siluria-na-salonach-szlachty.html)|10/05/31|10/06/01|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Wiktor Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1709-wiktor-sowinski.html)|11|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html), [160412](/rpg/inwazja/opowiesci/konspekty/160412-splesniala-dusza-terminuski.html), [160821](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html), [160404](/rpg/inwazja/opowiesci/konspekty/160404-the-power-of-cute-pet.html), [160724](/rpg/inwazja/opowiesci/konspekty/160724-portal-do-eam.html), [160403](/rpg/inwazja/opowiesci/konspekty/160403-wiktor-kontra-kadem-i-swieca.html), [160723](/rpg/inwazja/opowiesci/konspekty/160723-czyj-jest-kompleks-centralny.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html), [160327](/rpg/inwazja/opowiesci/konspekty/160327-piecdziesiat-oblicz-szlachty.html), [160326](/rpg/inwazja/opowiesci/konspekty/160326-siluria-na-salonach-szlachty.html)|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|10|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html), [160412](/rpg/inwazja/opowiesci/konspekty/160412-splesniala-dusza-terminuski.html), [160821](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html), [160404](/rpg/inwazja/opowiesci/konspekty/160404-the-power-of-cute-pet.html), [160724](/rpg/inwazja/opowiesci/konspekty/160724-portal-do-eam.html), [160403](/rpg/inwazja/opowiesci/konspekty/160403-wiktor-kontra-kadem-i-swieca.html), [160723](/rpg/inwazja/opowiesci/konspekty/160723-czyj-jest-kompleks-centralny.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html), [160327](/rpg/inwazja/opowiesci/konspekty/160327-piecdziesiat-oblicz-szlachty.html), [160326](/rpg/inwazja/opowiesci/konspekty/160326-siluria-na-salonach-szlachty.html)|
|[Wioletta Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-wioletta-bankierz.html)|8|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [160412](/rpg/inwazja/opowiesci/konspekty/160412-splesniala-dusza-terminuski.html), [160404](/rpg/inwazja/opowiesci/konspekty/160404-the-power-of-cute-pet.html), [160403](/rpg/inwazja/opowiesci/konspekty/160403-wiktor-kontra-kadem-i-swieca.html), [160723](/rpg/inwazja/opowiesci/konspekty/160723-czyj-jest-kompleks-centralny.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html), [160327](/rpg/inwazja/opowiesci/konspekty/160327-piecdziesiat-oblicz-szlachty.html), [160326](/rpg/inwazja/opowiesci/konspekty/160326-siluria-na-salonach-szlachty.html)|
|[Elea Maus](/rpg/inwazja/opowiesci/karty-postaci/1802-elea-maus.html)|6|[160412](/rpg/inwazja/opowiesci/konspekty/160412-splesniala-dusza-terminuski.html), [160821](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html), [160404](/rpg/inwazja/opowiesci/konspekty/160404-the-power-of-cute-pet.html), [160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Diana Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-diana-weiner.html)|6|[160412](/rpg/inwazja/opowiesci/konspekty/160412-splesniala-dusza-terminuski.html), [160404](/rpg/inwazja/opowiesci/konspekty/160404-the-power-of-cute-pet.html), [160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [160724](/rpg/inwazja/opowiesci/konspekty/160724-portal-do-eam.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html), [160327](/rpg/inwazja/opowiesci/konspekty/160327-piecdziesiat-oblicz-szlachty.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|5|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html), [160316](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html), [160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Gerwazy Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/9999-gerwazy-myszeczka.html)|5|[160404](/rpg/inwazja/opowiesci/konspekty/160404-the-power-of-cute-pet.html), [160403](/rpg/inwazja/opowiesci/konspekty/160403-wiktor-kontra-kadem-i-swieca.html), [160723](/rpg/inwazja/opowiesci/konspekty/160723-czyj-jest-kompleks-centralny.html), [160327](/rpg/inwazja/opowiesci/konspekty/160327-piecdziesiat-oblicz-szlachty.html), [160326](/rpg/inwazja/opowiesci/konspekty/160326-siluria-na-salonach-szlachty.html)|
|[Sabina Sowińska](/rpg/inwazja/opowiesci/karty-postaci/1709-sabina-sowinska.html)|4|[160412](/rpg/inwazja/opowiesci/konspekty/160412-splesniala-dusza-terminuski.html), [160404](/rpg/inwazja/opowiesci/konspekty/160404-the-power-of-cute-pet.html), [160403](/rpg/inwazja/opowiesci/konspekty/160403-wiktor-kontra-kadem-i-swieca.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Ozydiusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-ozydiusz-bankierz.html)|4|[160316](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html), [160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html), [160327](/rpg/inwazja/opowiesci/konspekty/160327-piecdziesiat-oblicz-szlachty.html)|
|[Marian Agrest](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-agrest.html)|4|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html), [160327](/rpg/inwazja/opowiesci/konspekty/160327-piecdziesiat-oblicz-szlachty.html)|
|[Margaret Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-margaret-blakenbauer.html)|4|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html), [160316](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html), [160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Klara Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-klara-blakenbauer.html)|4|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html), [160316](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html), [160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Laragnarhag](/rpg/inwazja/opowiesci/karty-postaci/1709-laragnarhag.html)|3|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [160412](/rpg/inwazja/opowiesci/konspekty/160412-splesniala-dusza-terminuski.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Judyta Karnisz](/rpg/inwazja/opowiesci/karty-postaci/9999-judyta-karnisz.html)|3|[160316](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html), [160821](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html), [160724](/rpg/inwazja/opowiesci/konspekty/160724-portal-do-eam.html)|
|[Emilia Kołatka](/rpg/inwazja/opowiesci/karty-postaci/1709-emilia-kolatka.html)|3|[160316](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html), [160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Amanda Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-amanda-diakon.html)|3|[160316](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Tadeusz Baran](/rpg/inwazja/opowiesci/karty-postaci/1709-tadeusz-baran.html)|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Otton Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-otton-blakenbauer.html)|2|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Oktawian Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-oktawian-maus.html)|2|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [160724](/rpg/inwazja/opowiesci/konspekty/160724-portal-do-eam.html)|
|[Netheria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-netheria-diakon.html)|2|[160404](/rpg/inwazja/opowiesci/konspekty/160404-the-power-of-cute-pet.html), [160403](/rpg/inwazja/opowiesci/konspekty/160403-wiktor-kontra-kadem-i-swieca.html)|
|[Marian Łajdak](/rpg/inwazja/opowiesci/karty-postaci/1709-marian-lajdak.html)|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [160403](/rpg/inwazja/opowiesci/konspekty/160403-wiktor-kontra-kadem-i-swieca.html)|
|[Marcelin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-marcelin-blakenbauer.html)|2|[160316](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Leonidas Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-leonidas-blakenbauer.html)|2|[160316](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html), [160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Kleofas Bór](/rpg/inwazja/opowiesci/karty-postaci/1709-kleofas-bor.html)|2|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Kermit Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-kermit-diakon.html)|2|[160821](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html), [160326](/rpg/inwazja/opowiesci/konspekty/160326-siluria-na-salonach-szlachty.html)|
|[Kajetan Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-kajetan-weiner.html)|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Julian Pszczelak](/rpg/inwazja/opowiesci/karty-postaci/1709-julian-pszczelak.html)|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|2|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html), [160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Bianka Stein](/rpg/inwazja/opowiesci/karty-postaci/1709-bianka-stein.html)|2|[160821](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html), [160724](/rpg/inwazja/opowiesci/konspekty/160724-portal-do-eam.html)|
|[Antoni Kurzamyśl](/rpg/inwazja/opowiesci/karty-postaci/9999-antoni-kurzamysl.html)|2|[160327](/rpg/inwazja/opowiesci/konspekty/160327-piecdziesiat-oblicz-szlachty.html), [160326](/rpg/inwazja/opowiesci/konspekty/160326-siluria-na-salonach-szlachty.html)|
|[Andżelika Leszczyńska](/rpg/inwazja/opowiesci/karty-postaci/1709-andzelika-leszczynska.html)|2|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html), [170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|
|[Andrea Wilgacz](/rpg/inwazja/opowiesci/karty-postaci/1709-andrea-wilgacz.html)|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Aleksander Sowiński](/rpg/inwazja/opowiesci/karty-postaci/9999-aleksander-sowinski.html)|2|[160404](/rpg/inwazja/opowiesci/konspekty/160404-the-power-of-cute-pet.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Świeży Lilak](/rpg/inwazja/opowiesci/karty-postaci/9999-swiezy-lilak.html)|1|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|
|[Wojciech Szudek](/rpg/inwazja/opowiesci/karty-postaci/9999-wojciech-szudek.html)|1|[160724](/rpg/inwazja/opowiesci/konspekty/160724-portal-do-eam.html)|
|[Wanda Ketran](/rpg/inwazja/opowiesci/karty-postaci/1709-wanda-ketran.html)|1|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Vladlena Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-vladlena-zjacew.html)|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|[Tomasz Przodownik](/rpg/inwazja/opowiesci/karty-postaci/9999-tomasz-przodownik.html)|1|[160327](/rpg/inwazja/opowiesci/konspekty/160327-piecdziesiat-oblicz-szlachty.html)|
|[Tomasz Kuracz](/rpg/inwazja/opowiesci/karty-postaci/9999-tomasz-kuracz.html)|1|[160724](/rpg/inwazja/opowiesci/konspekty/160724-portal-do-eam.html)|
|[Tatiana Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-tatiana-zajcew.html)|1|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|
|[Stalowy Śledzik Żarłacz](/rpg/inwazja/opowiesci/karty-postaci/9999-stalowy-sledzik-zarlacz.html)|1|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|
|[Saith Kameleon](/rpg/inwazja/opowiesci/karty-postaci/9999-saith-kameleon.html)|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|[Saith Flamecaller](/rpg/inwazja/opowiesci/karty-postaci/9999-saith-flamecaller.html)|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|[Ryszard Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-ryszard-weiner.html)|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Rufus Czubek](/rpg/inwazja/opowiesci/karty-postaci/9999-rufus-czubek.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Romeo Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-romeo-diakon.html)|1|[160404](/rpg/inwazja/opowiesci/konspekty/160404-the-power-of-cute-pet.html)|
|[Roman Błyszczyk](/rpg/inwazja/opowiesci/karty-postaci/9999-roman-blyszczyk.html)|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|[Rodion Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-rodion-zajcew.html)|1|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|
|[Rafael Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-rafael-diakon.html)|1|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|
|[Patrycja Krowiowska](/rpg/inwazja/opowiesci/karty-postaci/1709-patrycja-krowiowska.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Pasożyt Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-pasozyt-diakon.html)|1|[160316](/rpg/inwazja/opowiesci/konspekty/160316-frontalne-wejscie-millennium.html)|
|[Ofelia Caesar](/rpg/inwazja/opowiesci/karty-postaci/9999-ofelia-caesar.html)|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|[Mirabelka Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-mirabelka-diakon.html)|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Milena Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-milena-diakon.html)|1|[160821](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html)|
|[Mikado Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-mikado-diakon.html)|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|[Mieszko Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-mieszko-bankierz.html)|1|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|
|[Melodia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-melodia-diakon.html)|1|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|
|[Marcel Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-marcel-bankierz.html)|1|[160723](/rpg/inwazja/opowiesci/konspekty/160723-czyj-jest-kompleks-centralny.html)|
|[Lidia Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-lidia-weiner.html)|1|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|
|[Lenart Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/9999-lenart-myszeczka.html)|1|[160723](/rpg/inwazja/opowiesci/konspekty/160723-czyj-jest-kompleks-centralny.html)|
|[Laurena Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-laurena-bankierz.html)|1|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|
|[Laura Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-laura-bankierz.html)|1|[160723](/rpg/inwazja/opowiesci/konspekty/160723-czyj-jest-kompleks-centralny.html)|
|[Lancelot Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-lancelot-bankierz.html)|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|[Kinga Melit](/rpg/inwazja/opowiesci/karty-postaci/9999-kinga-melit.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Karradrael](/rpg/inwazja/opowiesci/karty-postaci/9999-karradrael.html)|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|[Karolina Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-karolina-maus.html)|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|[Karol Poczciwiec](/rpg/inwazja/opowiesci/karty-postaci/9999-karol-poczciwiec.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Jurij Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-jurij-zajcew.html)|1|[160327](/rpg/inwazja/opowiesci/konspekty/160327-piecdziesiat-oblicz-szlachty.html)|
|[Judyta Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-judyta-maus.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Joachim Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-zajcew.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Joachim Kopiec](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-kopiec.html)|1|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|
|[Jakub Niecień](/rpg/inwazja/opowiesci/karty-postaci/9999-jakub-niecien.html)|1|[160821](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html)|
|[Ireneusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-ireneusz-bankierz.html)|1|[160723](/rpg/inwazja/opowiesci/konspekty/160723-czyj-jest-kompleks-centralny.html)|
|[Ignat Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1802-ignat-zajcew.html)|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|[GS "Aegis" 0003](/rpg/inwazja/opowiesci/karty-postaci/9999-gs-aegis-0003.html)|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|[Fryderyk Chwost](/rpg/inwazja/opowiesci/karty-postaci/9999-fryderyk-chwost.html)|1|[160412](/rpg/inwazja/opowiesci/konspekty/160412-splesniala-dusza-terminuski.html)|
|[Fortitia Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-fortitia-diakon.html)|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Estrella Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-estrella-diakon.html)|1|[160821](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html)|
|[Elizawieta Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-elizawieta-zajcew.html)|1|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Edward Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-edward-diakon.html)|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Edward Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-edward-bankierz.html)|1|[160821](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html)|
|[Draconis Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-draconis-diakon.html)|1|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|
|[Dalia Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-dalia-weiner.html)|1|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|
|[Bolesław Derwisz](/rpg/inwazja/opowiesci/karty-postaci/9999-boleslaw-derwisz.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Bogdan Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-bogdan-bankierz.html)|1|[160723](/rpg/inwazja/opowiesci/konspekty/160723-czyj-jest-kompleks-centralny.html)|
|[Aurel Czarko](/rpg/inwazja/opowiesci/karty-postaci/1709-aurel-czarko.html)|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Arkadiusz Klusiński](/rpg/inwazja/opowiesci/karty-postaci/9999-arkadiusz-klusinski.html)|1|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Anna Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-myszeczka.html)|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|[Anna Kozak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kozak.html)|1|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|
|[Anna Kajak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kajak.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Aneta Rainer](/rpg/inwazja/opowiesci/karty-postaci/1709-aneta-rainer.html)|1|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Alojzy Przylaz](/rpg/inwazja/opowiesci/karty-postaci/1803-alojzy-przylaz.html)|1|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|
|[Alicja Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-alicja-weiner.html)|1|[160724](/rpg/inwazja/opowiesci/konspekty/160724-portal-do-eam.html)|
|[Adrian Murarz](/rpg/inwazja/opowiesci/karty-postaci/9999-adrian-murarz.html)|1|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|

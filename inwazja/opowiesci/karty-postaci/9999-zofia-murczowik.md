---
layout: inwazja-karta-postaci
categories: profile
title: "Zofia Murczówik"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170614|która przejrzała przez iluzję Diany i zobaczyła, że ta chodzi nieubrana. Oczywiście, zaraz pół miasteczka wiedziało.|[Kryzys przez eliksir](/rpg/inwazja/opowiesci/konspekty/170614-kryzys-przez-eliksir.html)|10/01/27|10/01/28|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|
|170523|19-latka mówiąca Paulinie, że nie może być w ciąży... a jest. Gdy była u Pauliny, wpadł na nią ranny Dobrocień... no i jest plotkarką.|[Opętany konstruminus](/rpg/inwazja/opowiesci/konspekty/170523-opetany-konstruminus.html)|10/01/03|10/01/04|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|2|[170614](/rpg/inwazja/opowiesci/konspekty/170614-kryzys-przez-eliksir.html), [170523](/rpg/inwazja/opowiesci/konspekty/170523-opetany-konstruminus.html)|
|[Janusz Wosiciel](/rpg/inwazja/opowiesci/karty-postaci/9999-janusz-wosiciel.html)|1|[170614](/rpg/inwazja/opowiesci/konspekty/170614-kryzys-przez-eliksir.html)|
|[Jakub Dobrocień](/rpg/inwazja/opowiesci/karty-postaci/1709-jakub-dobrocien.html)|1|[170523](/rpg/inwazja/opowiesci/konspekty/170523-opetany-konstruminus.html)|
|[Hektor Reszniaczek](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-reszniaczek.html)|1|[170614](/rpg/inwazja/opowiesci/konspekty/170614-kryzys-przez-eliksir.html)|
|[Grazoniusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-grazoniusz-bankierz.html)|1|[170614](/rpg/inwazja/opowiesci/konspekty/170614-kryzys-przez-eliksir.html)|
|[Felicja Szampierz](/rpg/inwazja/opowiesci/karty-postaci/9999-felicja-szampierz.html)|1|[170523](/rpg/inwazja/opowiesci/konspekty/170523-opetany-konstruminus.html)|
|[Efemeryda Senesgradzka](/rpg/inwazja/opowiesci/karty-postaci/9999-efemeryda-senesgradzka.html)|1|[170523](/rpg/inwazja/opowiesci/konspekty/170523-opetany-konstruminus.html)|
|[Diana Łuczkiewicz](/rpg/inwazja/opowiesci/karty-postaci/9999-diana-luczkiewicz.html)|1|[170614](/rpg/inwazja/opowiesci/konspekty/170614-kryzys-przez-eliksir.html)|
|[Apoloniusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-apoloniusz-bankierz.html)|1|[170523](/rpg/inwazja/opowiesci/konspekty/170523-opetany-konstruminus.html)|
|[Andrzej Farnolis](/rpg/inwazja/opowiesci/karty-postaci/9999-andrzej-farnolis.html)|1|[170523](/rpg/inwazja/opowiesci/konspekty/170523-opetany-konstruminus.html)|

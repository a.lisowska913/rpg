---
layout: inwazja-karta-postaci
categories: profile
factions: "Niezrzeszeni"
type: "NPC"
title: "Karolina Maus"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **BÓL: deathwish**:
    * _Aspekty_: 
    * _Opis_: make it stop MAKE IT STOP
* **FIL: nienawidzi bezcelowego okrucieństwa**: 
    * _Aspekty_: 
    * _Opis_: 
* **FIL: wszystko należy ulepszyć**:
    * _Aspekty_: jestem tylko pionkiem
    * _Opis_: Karradrael > Ród > ja
* **MRZ: **:
    * _Aspekty_: 
    * _Opis_: 
* **KLT: antyspontaniczna**:
    * _Aspekty_: rozwiązanie optymalne za wszelką cenę
    * _Opis_: wszystko musi być uporządkowane
	
### Umiejętności

* **magiczny inżynier**:
    * _Aspekty_: ekspert od Spustoszenia, roje, systemy rojowe i sieciowe, cyborgizacja
    * _Opis_: 
* **szantażysta**: 
    * _Aspekty_: corruptor
    * _Opis_: 
* **neuronauta**:
    * _Aspekty_: medytacja, introspekcja
    * _Opis_: 
* **uczeń EAM**:
    * _Aspekty_:  specjalistyczne rytuały,
    * _Opis_: 
	
### Silne i słabe strony:

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

## Magia

### Szkoły magiczne

* **Technomancja**:
    * _Aspekty_: integracja technomantyczna, systemy rojowe i sieciowe, artefakcja
    * _Opis_: 
* **Kataliza**: 
    * _Aspekty_: adaptacja artefaktów, przejęcie artefaktów, artefakcja
    * _Opis_: 
* **Biomancja**:
    * _Aspekty_: cruciodefilerka
    * _Opis_: 

### Zaklęcia statyczne

* **The Matrix**: 
    * _Aspekty_: bio- techno- i taumatowspomaganie wzmacnia jej bazę; podatna na anty-magię
    * _Opis_: 

## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Zarobki

* ?

### Znam

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

### Mam

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

# Opis

A monster. A victim. A bright and promising girl which could become an EAM sorceress, but was denied because of political reasons. A hard-working sister and daughter. Under the regime of Jonatan Maus, her mind has been merged with Aurelia Maus’ psyche and knowledge and the girl she used to be ceased to exist.

The resultant entity known as Karolina Maus has no friends and is alone. She is plagued by conflicting memories and haunted by alien desires. She desperately wanted to die but was unable to – Karradrael’s programming makes this option impossible. She is herself but also she is Aurelia, as unstable as she used to be.

This girl wanted to be a neuronaut, to understand the functioning of the mind, to understand the beauty within. Now, Karolina is a technomancer, focusing on things mechanical and unloving.

A shadow of her own self. Cold like ice. Immoral like her lord and master himself. Loyal and elegant. Dangerous. Lonely. The fact she does not feel she is living a never-ending nightmare is only the proof of Karradrael’s absolute perfection and mastery over the Maus bloodline.

### Koncept

Wysokopoziomowy koncept postaci, 1-2 zdania.

### Motto

"Tekst"

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Oblicze guwernantki](/rpg/inwazja/opowiesci/konspekty/160819-oblicze-guwernantki.html)|okazuje się być bardzo opanowaną defilerką o ogromnej mocy (co samo w sobie jest oksymoronem)|Powrót Karradraela|
|[Oblicze guwernantki](/rpg/inwazja/opowiesci/konspekty/160819-oblicze-guwernantki.html)|wie, że przeciwko niej stają Blakenbauerowie|Powrót Karradraela|
|[Zaszczepić Adriana Murarza!](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html)|traci Patrycję Krowiowską|Powrót Karradraela|
|[Mała szara myszka...](/rpg/inwazja/opowiesci/konspekty/160707-mala-szara-myszka.html)|łapie Patrycję Krowiowską.|Powrót Karradraela|


## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|161012|która tym razem prawie dorwała Hektora i Silurię. Ale KADEMki i Aegis porwali cel Karolinie sprzed nosa. Skończyła walcząc z Aegis.|[Kontratak Karradraela](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|10/07/22|10/07/23|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160825|nieobecna, acz dzięki jej zapisom udało się dowiedzieć wszystkich ważnych rzeczy na jej temat - Aurelia, sieć Mausów, Spustoszona Defilerka.|[Plany Overminda](/rpg/inwazja/opowiesci/konspekty/160825-plany-overminda.html)|10/07/15|10/07/16|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160819|The Governess, która koordynuje zdalnie działania w Kotach swoimi dronami. Perfekcyjnie przygotowana... na nie tych przeciwników.|[Oblicze guwernantki](/rpg/inwazja/opowiesci/konspekty/160819-oblicze-guwernantki.html)|10/07/12|10/07/14|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160810|The Governess, odepchnięta i odparta przez Zajcewkomando; straciła Patrycję Krowiowską i bazę, acz niewiele więcej|[Zaszczepić Adriana Murarza!](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html)|10/07/09|10/07/11|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160707|The Governess, porwała Patrycję by zatrzymać Blakenbauerów przed dalszymi odkryciami. Komunikuje się z Hektorem przez kanał Youtube.|[Mała szara myszka...](/rpg/inwazja/opowiesci/konspekty/160707-mala-szara-myszka.html)|10/07/01|10/07/03|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160615|"The Governess" jako "Wanda Ketran", metodycznie niszcząca wszystko co Blakenbauerowie kochają by wprowadzić ich w paranoję i lockdown w Rezydencji.|[Morderczyni w masce Wandy](/rpg/inwazja/opowiesci/konspekty/160615-morderczyni-w-masce-wandy.html)|10/06/25|10/06/26|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|141012|"EAMka", czyli złowrogi cień czuwający nad tym wszystkim|[Aplikanci widzieli gorathaula](/rpg/inwazja/opowiesci/konspekty/141012-aplikanci-widzieli-gorathaula.html)|10/05/30|10/06/07|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|
|140928|czyli "EAMka" jako żywy dowód na to, że czasami zło wygrywa. Choć nie do końca.|[Policjant widział anioła](/rpg/inwazja/opowiesci/konspekty/140928-policjant-widzial-aniola.html)|10/05/12|10/05/13|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|
|160809|The Governess, która manipulowała wydarzeniami zza sceny i doprowadziła przy użyciu ludzi do przejęcia przez Spustoszenie Blutwurstów|[Awokado Dla Wampira](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|10/05/07|10/05/09|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150729|bardzo uzdolniona czarodziejka z objawami depresji, która próbuje popełnić samobójstwo cudzymi rękami.|[Kaczuszka w servarze](/rpg/inwazja/opowiesci/konspekty/150729-kaczuszka-w-servarze.html)|10/01/19|10/01/21|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|140618|chętnie (lub mniej) przekazywana z rąk do rąk (i odbijana). Na samym końcu przechwycił ją Czerwiec... lecz zginęła straszną śmiercią nim cokolwiek powiedziała.|[Upadek Agresta](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|10/01/17|10/01/18|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|
|140611|desygnowana na następczynię Aurelii Maus przez Karradraela. Zgodnie z tym co mówi Franciszek Maus "nie jest jedną z nas".|[Rezydencja Blakenbauerów](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html)|10/01/15|10/01/16|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|
|140604|którą wpierw prawie zabiła Krystalia, potem prawie porwał Agrest aż w końcu Andrea przekazała ją Irinie Zajcew.|[Patriarcha Blakenbauer](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|10/01/13|10/01/14|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|
|140401|święcie przekonana że odparła mindworma (lol), która zdradziła Andrei, Wacławowi i Czerwcowi wszystko co pamiętała o Moriacie.|[Mojra, Moriath](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|10/01/11|10/01/12|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|
|140219|milość Grzegorza Czerwca która została pokonana i zmindwormowana przez Moriatha.|[Niespodziewane wsparcie](/rpg/inwazja/opowiesci/konspekty/140219-niespodziewane-wsparcie.html)|10/01/03|10/01/04|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|10|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html), [160825](/rpg/inwazja/opowiesci/konspekty/160825-plany-overminda.html), [160819](/rpg/inwazja/opowiesci/konspekty/160819-oblicze-guwernantki.html), [160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html), [160707](/rpg/inwazja/opowiesci/konspekty/160707-mala-szara-myszka.html), [160615](/rpg/inwazja/opowiesci/konspekty/160615-morderczyni-w-masce-wandy.html), [140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140611](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Otton Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-otton-blakenbauer.html)|7|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html), [160819](/rpg/inwazja/opowiesci/konspekty/160819-oblicze-guwernantki.html), [160615](/rpg/inwazja/opowiesci/konspekty/160615-morderczyni-w-masce-wandy.html), [140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140611](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Klara Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-klara-blakenbauer.html)|6|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html), [160825](/rpg/inwazja/opowiesci/konspekty/160825-plany-overminda.html), [160819](/rpg/inwazja/opowiesci/konspekty/160819-oblicze-guwernantki.html), [160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html), [160707](/rpg/inwazja/opowiesci/konspekty/160707-mala-szara-myszka.html), [160615](/rpg/inwazja/opowiesci/konspekty/160615-morderczyni-w-masce-wandy.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|6|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html), [160819](/rpg/inwazja/opowiesci/konspekty/160819-oblicze-guwernantki.html), [160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html), [160615](/rpg/inwazja/opowiesci/konspekty/160615-morderczyni-w-masce-wandy.html), [140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Wacław Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-waclaw-zajcew.html)|5|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140611](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140219](/rpg/inwazja/opowiesci/konspekty/140219-niespodziewane-wsparcie.html)|
|[Sebastian Tecznia](/rpg/inwazja/opowiesci/karty-postaci/9999-sebastian-tecznia.html)|5|[141012](/rpg/inwazja/opowiesci/konspekty/141012-aplikanci-widzieli-gorathaula.html), [140928](/rpg/inwazja/opowiesci/konspekty/140928-policjant-widzial-aniola.html), [140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Mojra](/rpg/inwazja/opowiesci/karty-postaci/9999-mojra.html)|5|[160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html), [140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140219](/rpg/inwazja/opowiesci/konspekty/140219-niespodziewane-wsparcie.html)|
|[Grzegorz Czerwiec](/rpg/inwazja/opowiesci/karty-postaci/9999-grzegorz-czerwiec.html)|5|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140611](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140219](/rpg/inwazja/opowiesci/konspekty/140219-niespodziewane-wsparcie.html)|
|[Andrea Wilgacz](/rpg/inwazja/opowiesci/karty-postaci/1709-andrea-wilgacz.html)|5|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140611](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140219](/rpg/inwazja/opowiesci/konspekty/140219-niespodziewane-wsparcie.html)|
|[Teresa Żyraf](/rpg/inwazja/opowiesci/karty-postaci/9999-teresa-zyraf.html)|4|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140219](/rpg/inwazja/opowiesci/konspekty/140219-niespodziewane-wsparcie.html)|
|[Marian Agrest](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-agrest.html)|4|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140611](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Irina Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-irina-zajcew.html)|4|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140611](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html), [140219](/rpg/inwazja/opowiesci/konspekty/140219-niespodziewane-wsparcie.html)|
|[Dionizy Kret](/rpg/inwazja/opowiesci/karty-postaci/1709-dionizy-kret.html)|4|[160819](/rpg/inwazja/opowiesci/konspekty/160819-oblicze-guwernantki.html), [160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html), [160707](/rpg/inwazja/opowiesci/konspekty/160707-mala-szara-myszka.html), [160615](/rpg/inwazja/opowiesci/konspekty/160615-morderczyni-w-masce-wandy.html)|
|[Waldemar Zupaczka](/rpg/inwazja/opowiesci/karty-postaci/9999-waldemar-zupaczka.html)|3|[140611](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Margaret Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-margaret-blakenbauer.html)|3|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Marcelin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-marcelin-blakenbauer.html)|3|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140611](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Maja Kos](/rpg/inwazja/opowiesci/karty-postaci/9999-maja-kos.html)|3|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140611](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Krystalia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-krystalia-diakon.html)|3|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140219](/rpg/inwazja/opowiesci/konspekty/140219-niespodziewane-wsparcie.html)|
|[Anna Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-myszeczka.html)|3|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html), [160825](/rpg/inwazja/opowiesci/konspekty/160825-plany-overminda.html), [160819](/rpg/inwazja/opowiesci/konspekty/160819-oblicze-guwernantki.html)|
|[Alina Bednarz](/rpg/inwazja/opowiesci/karty-postaci/1803-alina-bednarz.html)|3|[160819](/rpg/inwazja/opowiesci/konspekty/160819-oblicze-guwernantki.html), [160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html), [160615](/rpg/inwazja/opowiesci/konspekty/160615-morderczyni-w-masce-wandy.html)|
|[Wiktor Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1709-wiktor-sowinski.html)|2|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html), [160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html)|
|[Vladlena Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-vladlena-zjacew.html)|2|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html), [160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html)|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|2|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html), [150729](/rpg/inwazja/opowiesci/konspekty/150729-kaczuszka-w-servarze.html)|
|[Saith Flamecaller](/rpg/inwazja/opowiesci/karty-postaci/9999-saith-flamecaller.html)|2|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html), [140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Quasar](/rpg/inwazja/opowiesci/karty-postaci/9999-quasar.html)|2|[150729](/rpg/inwazja/opowiesci/konspekty/150729-kaczuszka-w-servarze.html), [140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|2|[141012](/rpg/inwazja/opowiesci/konspekty/141012-aplikanci-widzieli-gorathaula.html), [140928](/rpg/inwazja/opowiesci/konspekty/140928-policjant-widzial-aniola.html)|
|[Patrycja Krowiowska](/rpg/inwazja/opowiesci/karty-postaci/1709-patrycja-krowiowska.html)|2|[160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html), [160707](/rpg/inwazja/opowiesci/konspekty/160707-mala-szara-myszka.html)|
|[Nikola Kamień](/rpg/inwazja/opowiesci/karty-postaci/9999-nikola-kamien.html)|2|[141012](/rpg/inwazja/opowiesci/konspekty/141012-aplikanci-widzieli-gorathaula.html), [140928](/rpg/inwazja/opowiesci/konspekty/140928-policjant-widzial-aniola.html)|
|[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/1709-maria-newa.html)|2|[141012](/rpg/inwazja/opowiesci/konspekty/141012-aplikanci-widzieli-gorathaula.html), [140928](/rpg/inwazja/opowiesci/konspekty/140928-policjant-widzial-aniola.html)|
|[Gustaw Siedeł](/rpg/inwazja/opowiesci/karty-postaci/9999-gustaw-siedel.html)|2|[140611](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Estera Piryt](/rpg/inwazja/opowiesci/karty-postaci/9999-estera-piryt.html)|2|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Elżbieta Niemoc](/rpg/inwazja/opowiesci/karty-postaci/9999-elzbieta-niemoc.html)|2|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|2|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140611](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html)|
|[Dariusz Kopyto](/rpg/inwazja/opowiesci/karty-postaci/9999-dariusz-kopyto.html)|2|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Amelia Eter](/rpg/inwazja/opowiesci/karty-postaci/9999-amelia-eter.html)|2|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Alfred Kukułka](/rpg/inwazja/opowiesci/karty-postaci/9999-alfred-kukulka.html)|2|[140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140219](/rpg/inwazja/opowiesci/konspekty/140219-niespodziewane-wsparcie.html)|
|[firma Skrzydłoróg](/rpg/inwazja/opowiesci/karty-postaci/9999-firma-skrzydlorog.html)|1|[160707](/rpg/inwazja/opowiesci/konspekty/160707-mala-szara-myszka.html)|
|[Yakim Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-yakim-zajcew.html)|1|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Wojmił Kopiec](/rpg/inwazja/opowiesci/karty-postaci/9999-wojmil-kopiec.html)|1|[141012](/rpg/inwazja/opowiesci/konspekty/141012-aplikanci-widzieli-gorathaula.html)|
|[Whisperwind](/rpg/inwazja/opowiesci/karty-postaci/9999-whisperwind.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Wanda Ketran](/rpg/inwazja/opowiesci/karty-postaci/1709-wanda-ketran.html)|1|[160615](/rpg/inwazja/opowiesci/konspekty/160615-morderczyni-w-masce-wandy.html)|
|[Tatiana Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-tatiana-zajcew.html)|1|[140611](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html)|
|[Tadeusz Czerwiecki](/rpg/inwazja/opowiesci/karty-postaci/9999-tadeusz-czerwiecki.html)|1|[140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Szymon Skubny](/rpg/inwazja/opowiesci/karty-postaci/9999-szymon-skubny.html)|1|[160707](/rpg/inwazja/opowiesci/konspekty/160707-mala-szara-myszka.html)|
|[Salazar Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-salazar-bankierz.html)|1|[141012](/rpg/inwazja/opowiesci/konspekty/141012-aplikanci-widzieli-gorathaula.html)|
|[Saith Kameleon](/rpg/inwazja/opowiesci/karty-postaci/9999-saith-kameleon.html)|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|[Saith Catapult](/rpg/inwazja/opowiesci/karty-postaci/9999-saith-catapult.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Roman Błyszczyk](/rpg/inwazja/opowiesci/karty-postaci/9999-roman-blyszczyk.html)|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|[Rebeka Piryt](/rpg/inwazja/opowiesci/karty-postaci/9999-rebeka-piryt.html)|1|[140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Paweł Sępiak](/rpg/inwazja/opowiesci/karty-postaci/1709-pawel-sepiak.html)|1|[150729](/rpg/inwazja/opowiesci/konspekty/150729-kaczuszka-w-servarze.html)|
|[Oktawian Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-oktawian-maus.html)|1|[150729](/rpg/inwazja/opowiesci/konspekty/150729-kaczuszka-w-servarze.html)|
|[Ofelia Caesar](/rpg/inwazja/opowiesci/karty-postaci/9999-ofelia-caesar.html)|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|[Netheria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-netheria-diakon.html)|1|[160819](/rpg/inwazja/opowiesci/konspekty/160819-oblicze-guwernantki.html)|
|[Nela Welon](/rpg/inwazja/opowiesci/karty-postaci/9999-nela-welon.html)|1|[140219](/rpg/inwazja/opowiesci/konspekty/140219-niespodziewane-wsparcie.html)|
|[Milena Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-milena-diakon.html)|1|[160825](/rpg/inwazja/opowiesci/konspekty/160825-plany-overminda.html)|
|[Mikado Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-mikado-diakon.html)|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|[Mateusz Nieborak](/rpg/inwazja/opowiesci/karty-postaci/9999-mateusz-nieborak.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Marek Kromlan](/rpg/inwazja/opowiesci/karty-postaci/1803-marek-kromlan.html)|1|[160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html)|
|[Malia Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-malia-bankierz.html)|1|[160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html)|
|[Leopold Teściak](/rpg/inwazja/opowiesci/karty-postaci/9999-leopold-tesciak.html)|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|
|[Lancelot Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-lancelot-bankierz.html)|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|[Kleofas Bór](/rpg/inwazja/opowiesci/karty-postaci/1709-kleofas-bor.html)|1|[160615](/rpg/inwazja/opowiesci/konspekty/160615-morderczyni-w-masce-wandy.html)|
|[Kinga Melit](/rpg/inwazja/opowiesci/karty-postaci/9999-kinga-melit.html)|1|[160615](/rpg/inwazja/opowiesci/konspekty/160615-morderczyni-w-masce-wandy.html)|
|[Karradrael](/rpg/inwazja/opowiesci/karty-postaci/9999-karradrael.html)|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|[Karol Poczciwiec](/rpg/inwazja/opowiesci/karty-postaci/9999-karol-poczciwiec.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Karina von Blutwurst](/rpg/inwazja/opowiesci/karty-postaci/9999-karina-von-blutwurst.html)|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|
|[Juliusz Szaman](/rpg/inwazja/opowiesci/karty-postaci/9999-juliusz-szaman.html)|1|[140219](/rpg/inwazja/opowiesci/konspekty/140219-niespodziewane-wsparcie.html)|
|[Jolanta Lipińska](/rpg/inwazja/opowiesci/karty-postaci/9999-jolanta-lipinska.html)|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|
|[Joachim Kopiec](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-kopiec.html)|1|[141012](/rpg/inwazja/opowiesci/konspekty/141012-aplikanci-widzieli-gorathaula.html)|
|[Jan Wątły](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-watly.html)|1|[150729](/rpg/inwazja/opowiesci/konspekty/150729-kaczuszka-w-servarze.html)|
|[Jan Szczupak](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-szczupak.html)|1|[140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Izabela Bąk](/rpg/inwazja/opowiesci/karty-postaci/9999-izabela-bak.html)|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|
|[Irena Paniszok](/rpg/inwazja/opowiesci/karty-postaci/9999-irena-paniszok.html)|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|
|[Ilarion Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-ilarion-zajcew.html)|1|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Ika](/rpg/inwazja/opowiesci/karty-postaci/9999-ika.html)|1|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Ignat Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1802-ignat-zajcew.html)|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|[Grażyna Czegrzyn](/rpg/inwazja/opowiesci/karty-postaci/9999-grazyna-czegrzyn.html)|1|[160707](/rpg/inwazja/opowiesci/konspekty/160707-mala-szara-myszka.html)|
|[GS Aegis 0002](/rpg/inwazja/opowiesci/karty-postaci/9999-gs-aegis-0002.html)|1|[150729](/rpg/inwazja/opowiesci/konspekty/150729-kaczuszka-w-servarze.html)|
|[GS "Aegis" 0003](/rpg/inwazja/opowiesci/karty-postaci/9999-gs-aegis-0003.html)|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|[Franciszek Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-maus.html)|1|[140611](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html)|
|[Filip Czumko](/rpg/inwazja/opowiesci/karty-postaci/9999-filip-czumko.html)|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|
|[Elżbieta Paniszok](/rpg/inwazja/opowiesci/karty-postaci/9999-elzbieta-paniszok.html)|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|
|[Edmund Bakłażan](/rpg/inwazja/opowiesci/karty-postaci/9999-edmund-baklazan.html)|1|[140928](/rpg/inwazja/opowiesci/konspekty/140928-policjant-widzial-aniola.html)|
|[Damian Paniszok](/rpg/inwazja/opowiesci/karty-postaci/9999-damian-paniszok.html)|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|
|[Dagmara Wyjątek](/rpg/inwazja/opowiesci/karty-postaci/1709-dagmara-wyjątek.html)|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|[Dagmara Czeluść](/rpg/inwazja/opowiesci/karty-postaci/9999-dagmara-czelusc.html)|1|[160707](/rpg/inwazja/opowiesci/konspekty/160707-mala-szara-myszka.html)|
|[Bogumił Rojowiec](/rpg/inwazja/opowiesci/karty-postaci/9999-bogumil-rojowiec.html)|1|[150729](/rpg/inwazja/opowiesci/konspekty/150729-kaczuszka-w-servarze.html)|
|[Benjamin Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-benjamin-zajcew.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Baltazar Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-baltazar-maus.html)|1|[160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html)|
|[Artur Żupan](/rpg/inwazja/opowiesci/karty-postaci/1803-artur-zupan.html)|1|[140219](/rpg/inwazja/opowiesci/konspekty/140219-niespodziewane-wsparcie.html)|
|[Artur Kurczak](/rpg/inwazja/opowiesci/karty-postaci/9999-artur-kurczak.html)|1|[140928](/rpg/inwazja/opowiesci/konspekty/140928-policjant-widzial-aniola.html)|
|[Artur Bryś](/rpg/inwazja/opowiesci/karty-postaci/1709-artur-brys.html)|1|[160707](/rpg/inwazja/opowiesci/konspekty/160707-mala-szara-myszka.html)|
|[Antonina Brzeszcz](/rpg/inwazja/opowiesci/karty-postaci/1709-antonina-brzeszcz.html)|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|
|[Anna Kozak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kozak.html)|1|[141012](/rpg/inwazja/opowiesci/konspekty/141012-aplikanci-widzieli-gorathaula.html)|
|[Andżelika Leszczyńska](/rpg/inwazja/opowiesci/karty-postaci/1709-andzelika-leszczynska.html)|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|[Andrzej Sowiński](/rpg/inwazja/opowiesci/karty-postaci/9999-andrzej-sowinski.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Aleksander Tomaszewski](/rpg/inwazja/opowiesci/karty-postaci/1709-aleksander-tomaszewski.html)|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|
|[Adrian Murarz](/rpg/inwazja/opowiesci/karty-postaci/9999-adrian-murarz.html)|1|[160810](/rpg/inwazja/opowiesci/konspekty/160810-zaszczepic-adriana-murarza.html)|
|[Adolphus von Blutwurst](/rpg/inwazja/opowiesci/karty-postaci/9999-adolphus-von-blutwurst.html)|1|[160809](/rpg/inwazja/opowiesci/konspekty/160809-awokado-dla-wampira.html)|

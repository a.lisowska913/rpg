---
layout: inwazja-karta-postaci
categories: profile
factions: "Magitrownia Histogram"
type: "NPC"
title: "Karol Marzyciel"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **BÓL: Ostatni kurator umierającej magitrowni**:
    * _Aspekty_: samotny i niezrozumiany, sekrety magitrowni wyjdą na jaw, magitrownia umrze, będzie musiał opuścić magitrownię, umrze i nie znajdzie następcy
    * _Opis_: Karol jest w magitrowni Histogram od dawna. Od tak dawna, że można powiedzieć "od zawsze"; wprowadzony był tu jeszcze za czasów Weinerów. Wie, że magitrownia jest niebezpieczna i to akceptuje. Wiąże swoje życie z magitrownią i utrzymywał ją przy aktywnym działaniu po odejściu Weinerów. 
* **FIL: Cicha inżynieria systemów**:
    * _Aspekty_: dyskretne działanie, dążenie do samotności, zbierać wszystkie informacje, systemy ponad ludźmi, dobro wielu ponad dobro jednostek, "przede wszystkim niech działa"
    * _Opis_: Karol niechętnie pojawia się na pierwszej linii w rozumieniu takim, że go WIDAĆ. On woli robić rzeczy tak, by go nie było widać. Potrafi włożyć sporo pracy i energii, by nikt nie zauważył, że problem w ogóle wystąpił. Karol nie zajmuje się ludźmi czy magami - on zajmuje się działaniem tak, by nie trzeba było nigdy z nim rozmawiać.
* **ŚR: Informacją i tym co masz - zrobisz wszystko**:
    * _Aspekty_: ukrywanie i chomikowanie wszystkiego, nasłuchiwanie i katalogowanie sygnałów, rozwiązywanie tym co ma, nie proszenie o pomoc
    * _Opis_: Karol nie wierzy w efektowne działania przy użyciu nowinek. On uważa, że wszystko co jest potrzebne jest zawsze pod ręką a jeśli NIE jest, to tak miało być. Jego obowiązkiem jest nasłuchiwanie wszystkiego i zbieranie informacji, chomikowanie wszystkich rzeczy oraz twórcze wykorzystywanie tego, co już jest.

### Umiejętności

* **The Phantom of the Magitrownia**:
    * _Aspekty_: mechanik magitrowni, drugi do rozwiązania problemu, zna wszystkie zakamarki, zna sekrety magitrowni, kustosz magitrowni
    * _Opis_: Magitrownia Histogram ma wiele sekretów; Karol zna ich wiele. Potrafi się płynnie po niej poruszać, wie, co gdzie się znajduje i umie naprawić nawet rzeczy do których zwykle potrzebni są magowie. Nie jest najlepszy jak chodzi o nic w magitrowni, ale jest drugą osobą jaką się wzywa do rozwiązania problemu.
* **Krótkofalowiec eteru**: 
    * _Aspekty_: sensory i detektory, komunikacja z bytami nieludzkimi, łapanie migawek z eteru, radiostacje, podstawy komunikacji magitech, "to wszystko jest normalne", prowadzi badania paranormalne
    * _Opis_: W magitrowni zwykle dzieją się dziwne rzeczy. Karol nauczył się używać sensorów i innego sprzętu magitrowni, by "podsłuchiwać" eter i odbijać echa eteru w formie dziwnego filmu. Przez całą swoją radioaparaturę potrafi odbierać wydarzenia o silnym echu emocjonalnym z szerokiej przestrzeni - a nawet nadaje sygnały w eter (dosłownie), korespondując z dziwnymi czasem istotami
* ****:
    * _Aspekty_: 
    * _Opis_: 
    
### Silne i słabe strony:

* **Magitrownia jest moim domem**:
    * _Aspekty_: "magitrownia mi nie zrobi krzywdy", poza magitrownią czuje się źle, w magitrowni czuje się w domu, lekkie uzależnienie od aury magicznej, odporność na Skażenie, uważany za trochę dziwaka
    * _Opis_: Karol w magitrowni jest w domu. Skażenie i Pryzmat sprawiły, że niebezpieczna z natury magitrownia "potraktuje go ulgowo" - nikt nie wierzy, że Karolowi mogłoby się coś złego w magitrowni stać. Jest trochę jak ten indianin składający pokłon zwierzęciu, które zaraz zje. Poza "domem" czuje się nie najlepiej - zwłaszcza, że uzależnił się od aury magicznej magitrowni.
* **Widzi rzeczy nie dla człowieka**: 
    * _Aspekty_: świetnie poinformowany o silnych emocjonalnie tematach, wiedza na bardzo dziwne tematy (eter), czasem bardzo błędne wnioski, czasem dezinformowany, zna techniki wykrywania artefaktów
    * _Opis_: Przez swoje podejście i upodobania, Karol wie bardzo dużo rzeczy o obszarach o silnym natężeniu emocjonalnym - ale nie zawsze umie interpretowac eter. Tak samo, bada różne tematy choć często dochodzi do wniosków ograniczonych przez brak znajomości świata magów.
* ****:
    * _Aspekty_: 
    * _Opis_: 

## Magia

* Nie jest magiem

## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Zarobki

* Magitrownia Histogram: pracuje jako człowiek od wszystkiego

### Znam

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

### Mam

* **Radiostacja eteru**:
    * _Aspekty_: brudny i dziwny magitech, słyszy rzeczy których nie powinien, komunikacja z niezrozumiałym, wzmaganie paranoi
    * _Opis_: Karol nasłuchuje eter przy użyciu dość skomplikowanych, chałupniczo zrobionych magitechowych przyrządów - i komponentów samej magitrowni. Im silniejsze echo, tym mocniej się skomunikuje czy tym mocniej usłyszy. Nie zawsze wie co...
* **Samowzbudne artefakty magitrowni**:
    * _Aspekty_: pochodzą z magitrowni, artefakty emergentne, brudne i świecą, narzędzia użytku codziennego
    * _Opis_: Karol regularnie monitoruje magitrownię i przechodząc się po niej zbiera różne artefakty - lub podkłada własne przedmioty, by stały się prędzej czy później artefaktami. Ma dzięki temu zbiór dość interesujących małych przedmiotów o niewielkiej mocy, które bardzo pomagają mu w życiu codziennym.
* ****:
    * _Aspekty_: 
    * _Opis_: 

# Opis

TODO

### Koncept

Osoba o lekkim Skażeniu magicznym, która jest zakochana w pewnej idei. Latarnik. Ostatni kustosz, kochający wspomnienia Weinerki, w której się podkochiwał - i przeniósł uczucia na magitrownię. Pokazanie, że człowiek może dobrze egzystować w świecie magów... acz zinterpretuje ten świat po swojemu. Przy okazji idealne spięcia między nim a Draceną do rozwiązania przez nieszczęsnego Daniela.

### Motto

"Tekst"

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Potrójna magitrownia Histogram](/rpg/inwazja/opowiesci/konspekty/171220-potrojna-magitrownia-histogram.html)|w ciągłym kontakcie z Integrą Weiner (echem efemerycznym). Kochankowie przez telefon.|Wizja Dukata|
|[Dlaczego magitrownia działa?](/rpg/inwazja/opowiesci/konspekty/171205-dlaczego-magitrownia-dziala.html)|przez siły Gabriela Dukata i Roberta Sądecznego jest traktowany jako nieusuwalny "mebel" w magitrowni.|Wizja Dukata|

## Plany

|Misja|Plan|Kampania|
|-----|------|------|
|[Potrójna magitrownia Histogram](/rpg/inwazja/opowiesci/konspekty/171220-potrojna-magitrownia-histogram.html)|już wiadomo, że da się wyrwać osobę z efemerydy. Chce więc wyrwać Integrę Weiner z efemerydy docelowo.|Wizja Dukata|

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|171220|nadal zakochany w Integrze, nie zapomniał o niej. Odebrał komunikaty z eteru od Daniela i pomógł z manifestacji wyciągnąć Integrę.|[Potrójna magitrownia Histogram](/rpg/inwazja/opowiesci/konspekty/171220-potrojna-magitrownia-histogram.html)|11/10/05|11/10/06|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171105|zgłosił Danielowi, że są problemy bo Dracena rozprasza facetów... za co Daniel kazał mu znaleźć Dracenie "gniazdko miłości". Nieszczęśliwy.|[Epidemia Dezinhibicji](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|11/10/01|11/10/02|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171029|"przybrany ojciec" Oktawii, opiekun magitrowni, pomógł Paulinie odwrócić uwagę Oktawii i trochę nie może sobie darować.|[W co gra Sądeczny?](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html)|11/09/20|11/09/22|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171004|drugi po Nadziejaku odnośnie sensorów w Histogramie; ma wątpliwości co do kandydatury Draceny, bo... jest za ładna. Udostępnił Paulinie co miał jej udostępnić.|[Niestabilna magitrownia](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|11/09/01|11/09/03|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171205|człowiek mieszkający i zarządzający magitrownią Histogram, z którym magitrownia się silnie sprzęgła. Nie jest efemerydą ani viciniusem... ale jest już Inny.|[Dlaczego magitrownia działa?](/rpg/inwazja/opowiesci/konspekty/171205-dlaczego-magitrownia-dziala.html)|10/12/15|10/12/17|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|4|[171220](/rpg/inwazja/opowiesci/konspekty/171220-potrojna-magitrownia-histogram.html), [171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html), [171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html), [171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Tomasz Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-tomasz-myszeczka.html)|3|[171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html), [171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html), [171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Jakub Dobrocień](/rpg/inwazja/opowiesci/karty-postaci/1709-jakub-dobrocien.html)|3|[171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html), [171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html), [171205](/rpg/inwazja/opowiesci/konspekty/171205-dlaczego-magitrownia-dziala.html)|
|[Efemeryda Senesgradzka](/rpg/inwazja/opowiesci/karty-postaci/9999-efemeryda-senesgradzka.html)|3|[171220](/rpg/inwazja/opowiesci/konspekty/171220-potrojna-magitrownia-histogram.html), [171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html), [171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|3|[171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html), [171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html), [171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Daniel Akwitański](/rpg/inwazja/opowiesci/karty-postaci/1709-daniel-akwitanski.html)|3|[171220](/rpg/inwazja/opowiesci/konspekty/171220-potrojna-magitrownia-histogram.html), [171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html), [171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Sylwester Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-sylwester-bankierz.html)|2|[171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html), [171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html)|
|[Robert Sądeczny](/rpg/inwazja/opowiesci/karty-postaci/1709-robert-sadeczny.html)|2|[171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html), [171205](/rpg/inwazja/opowiesci/konspekty/171205-dlaczego-magitrownia-dziala.html)|
|[Kaja Maślaczek](/rpg/inwazja/opowiesci/karty-postaci/1709-kaja-maslaczek.html)|2|[171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html), [171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html)|
|[Zofia Przylga](/rpg/inwazja/opowiesci/karty-postaci/1709-zofia-przylga.html)|1|[171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Wiaczesław Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-wiaczeslaw-zajcew.html)|1|[171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|
|[Szczepan Złodrak](/rpg/inwazja/opowiesci/karty-postaci/9999-szczepan-zlodrak.html)|1|[171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|
|[Oliwia Aurinus](/rpg/inwazja/opowiesci/karty-postaci/1709-oliwia-aurinus.html)|1|[171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|
|[Oktawia Aurinus](/rpg/inwazja/opowiesci/karty-postaci/1709-oktawia-aurinus.html)|1|[171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html)|
|[Natalia Kazuń](/rpg/inwazja/opowiesci/karty-postaci/1709-natalia-kazun.html)|1|[171205](/rpg/inwazja/opowiesci/konspekty/171205-dlaczego-magitrownia-dziala.html)|
|[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/1709-maria-newa.html)|1|[171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html)|
|[Marcin Warinsky](/rpg/inwazja/opowiesci/karty-postaci/1709-marcin-warinsky.html)|1|[171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Kajetan Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-kajetan-weiner.html)|1|[171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html)|
|[Integra Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-integra-weiner.html)|1|[171220](/rpg/inwazja/opowiesci/konspekty/171220-potrojna-magitrownia-histogram.html)|
|[Grzegorz Nadziejak](/rpg/inwazja/opowiesci/karty-postaci/9999-grzegorz-nadziejak.html)|1|[171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Dalia Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-dalia-weiner.html)|1|[171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|
|[Bolesław Złodrak](/rpg/inwazja/opowiesci/karty-postaci/9999-boleslaw-zlodrak.html)|1|[171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|
|[Bolesław Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-boleslaw-maus.html)|1|[171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html)|
|[Bogumił Miłoszept](/rpg/inwazja/opowiesci/karty-postaci/9999-bogumil-miloszept.html)|1|[171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Apoloniusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-apoloniusz-bankierz.html)|1|[171205](/rpg/inwazja/opowiesci/konspekty/171205-dlaczego-magitrownia-dziala.html)|
|[Andżelika Leszczyńska](/rpg/inwazja/opowiesci/karty-postaci/1709-andzelika-leszczynska.html)|1|[171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html)|

---
layout: inwazja-karta-postaci
categories: profile
title: "Leokadia Myszeczka"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|160229|pod Tamarą, bardziej pełniła rolę mięśnia niż alchemika na tej akcji. Zaspawała wyjście awaryjne, by nikt nie uciekł.|[Siedmiu magów - nie Mausów](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html)|10/06/08|10/06/13|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150819|terminuska Kurtyny, którą rozchorował Edwin by uratować Marcelina przed ewentualną akcją Szlachty.|[Krótki antyporadnik o sojuszach](/rpg/inwazja/opowiesci/konspekty/150819-krotki-antyporadnik-o-sojuszach.html)|10/05/17|10/05/18|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150728|terminuska Kurtyny, której wolą było połączenie sił Hektora i Emilii przeciwko Szlachcie; niestabilna psychicznie; jej metody są tak okrutne jak jej nazwisko. |[Sojusz przeciwko Szlachcie](/rpg/inwazja/opowiesci/konspekty/150728-sojusz-przeciwko-szlachcie.html)|10/05/15|10/05/16|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Vladlena Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-vladlena-zjacew.html)|2|[150819](/rpg/inwazja/opowiesci/konspekty/150819-krotki-antyporadnik-o-sojuszach.html), [150728](/rpg/inwazja/opowiesci/konspekty/150728-sojusz-przeciwko-szlachcie.html)|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|2|[150819](/rpg/inwazja/opowiesci/konspekty/150819-krotki-antyporadnik-o-sojuszach.html), [150728](/rpg/inwazja/opowiesci/konspekty/150728-sojusz-przeciwko-szlachcie.html)|
|[Ozydiusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-ozydiusz-bankierz.html)|2|[160229](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html), [150728](/rpg/inwazja/opowiesci/konspekty/150728-sojusz-przeciwko-szlachcie.html)|
|[Marian Agrest](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-agrest.html)|2|[160229](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html), [150728](/rpg/inwazja/opowiesci/konspekty/150728-sojusz-przeciwko-szlachcie.html)|
|[Marcelin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-marcelin-blakenbauer.html)|2|[150819](/rpg/inwazja/opowiesci/konspekty/150819-krotki-antyporadnik-o-sojuszach.html), [150728](/rpg/inwazja/opowiesci/konspekty/150728-sojusz-przeciwko-szlachcie.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|2|[150819](/rpg/inwazja/opowiesci/konspekty/150819-krotki-antyporadnik-o-sojuszach.html), [150728](/rpg/inwazja/opowiesci/konspekty/150728-sojusz-przeciwko-szlachcie.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|2|[150819](/rpg/inwazja/opowiesci/konspekty/150819-krotki-antyporadnik-o-sojuszach.html), [150728](/rpg/inwazja/opowiesci/konspekty/150728-sojusz-przeciwko-szlachcie.html)|
|[Tamara Muszkiet](/rpg/inwazja/opowiesci/karty-postaci/1709-tamara-muszkiet.html)|1|[160229](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html)|
|[Tadeusz Baran](/rpg/inwazja/opowiesci/karty-postaci/1709-tadeusz-baran.html)|1|[160229](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html)|
|[Szymon Skubny](/rpg/inwazja/opowiesci/karty-postaci/9999-szymon-skubny.html)|1|[150819](/rpg/inwazja/opowiesci/konspekty/150819-krotki-antyporadnik-o-sojuszach.html)|
|[Saith Kameleon](/rpg/inwazja/opowiesci/karty-postaci/9999-saith-kameleon.html)|1|[150728](/rpg/inwazja/opowiesci/konspekty/150728-sojusz-przeciwko-szlachcie.html)|
|[Saith Catapult](/rpg/inwazja/opowiesci/karty-postaci/9999-saith-catapult.html)|1|[160229](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html)|
|[Margaret Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-margaret-blakenbauer.html)|1|[150728](/rpg/inwazja/opowiesci/konspekty/150728-sojusz-przeciwko-szlachcie.html)|
|[Klemens X](/rpg/inwazja/opowiesci/karty-postaci/9999-klemens-x.html)|1|[150819](/rpg/inwazja/opowiesci/konspekty/150819-krotki-antyporadnik-o-sojuszach.html)|
|[Kinga Melit](/rpg/inwazja/opowiesci/karty-postaci/9999-kinga-melit.html)|1|[150819](/rpg/inwazja/opowiesci/konspekty/150819-krotki-antyporadnik-o-sojuszach.html)|
|[Judyta Karnisz](/rpg/inwazja/opowiesci/karty-postaci/9999-judyta-karnisz.html)|1|[160229](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html)|
|[Jan Anioł Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-aniol-bankierz.html)|1|[160229](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html)|
|[GS "Aegis" 0003](/rpg/inwazja/opowiesci/karty-postaci/9999-gs-aegis-0003.html)|1|[150728](/rpg/inwazja/opowiesci/konspekty/150728-sojusz-przeciwko-szlachcie.html)|
|[Emilia Kołatka](/rpg/inwazja/opowiesci/karty-postaci/1709-emilia-kolatka.html)|1|[150728](/rpg/inwazja/opowiesci/konspekty/150728-sojusz-przeciwko-szlachcie.html)|
|[Diana Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-diana-weiner.html)|1|[150728](/rpg/inwazja/opowiesci/konspekty/150728-sojusz-przeciwko-szlachcie.html)|
|[Anna Kozak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kozak.html)|1|[160229](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html)|
|[Andrea Wilgacz](/rpg/inwazja/opowiesci/karty-postaci/1709-andrea-wilgacz.html)|1|[160229](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html)|
|[Aleksander Sowiński](/rpg/inwazja/opowiesci/karty-postaci/9999-aleksander-sowinski.html)|1|[160229](/rpg/inwazja/opowiesci/konspekty/160229-siedmiu-magow-nie-mausow.html)|

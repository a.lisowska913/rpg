---
layout: inwazja-karta-postaci
categories: profile
factions: "Niezrzeszeni"
type: "NPC"
title: "Katia Grajek"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **MET: Prawdziwy nerd eliksirowy**:
    * _Aspekty_: nienawidzi partactwa, dąży do eksperymentów z sympatią, dąży do maksymalizacji efektywności, kocha eksperymenty z eliksirami, curiosity kills the cat, kolekcjonuje eliksiry
    * _Opis_: Katia od zawsze interesowała się eliksirami i sympatią. Jak przy użyciu odpowiednich komponentów można zrobić coś lepszego i skuteczniejszego. Jej celem jest maksymalizacja dźwigni którą daje sympatia. Jednocześnie uważa, że eliksiry są czymś super fajnym i wkłada więcej pracy i energii w to co robi niż musi. Jej ciekawość sprawia, że wszystko musi zbadać, sprawdzić, określić.
* **MRZ: Aktywistka o prawa zwierząt**: 
    * _Aspekty_: wegetarianka, zwalcza masową hodowlę, dąży do kojenia cierpienia, dąży do wspierania dobrego życia dla wszystkich istot, zasady ponad zysk
    * _Opis_: Jakkolwiek do pracy z eliksirami potrzebne są często komponenty różnych żywych istot, Katia aktywnie zwalcza masową hodowlę i traktowanie zwierząt / viciniusów w niewłaściwy sposób. Jej zdaniem cierpienie jakiejkolwiek żywej istoty jest złe i należy się tego pozbyć nawet za cenę zmniejszania zysku.
* **MRZ: Pracownik społecznie zaangażowany**:
    * _Aspekty_: zasady nad prawo, społeczeństwo nad kapitalizm, promuje współpracę i redukcję nierówności, momentami antykapitalistyczna
    * _Opis_: To nie jest tak, że jak masz kapitał to możesz wszystko. Każda firma i organizacja znajduje się w większym ekosystemie i powinna temu ekosystemowi służyć. Ty masz kapitał, ale ja mam umiejętności. Katia jest zaangażowana w swoją pracę, ale oczekuje też pewnych działań od organizacji.

### Umiejętności

* **Kucharz**:
    * _Aspekty_: "to danie zrobiłam pod Ciebie", pozyskiwanie rzadkich komponentów, kalibracja jedzenia pod potrzeby, świetnie gotuje
    * _Opis_: Jak już i tak zajmuje się eliksirami, to jedzenie jest jej formą "alchemii bez magii". Bardzo lubi eksperymentować podczas robienia różnych przepisów i jej punktem honoru jest nakarmić każdego, niezależnie od podniebienia istoty, która ją odwiedzi.
* **Badacz i kolekcjoner eliksirów**: 
    * _Aspekty_: badanie i formy poznania, katalogowanie, praca z dokumentami, naukowe robienie eksperymentów, szeroka wiedza o sympatii
    * _Opis_: Jej główna siła i specjalizacja. Katia skupia się na budowaniu i konstruowaniu eliksirów; też je kolekcjonuje i próbuje katalogować. 
* **Prawnik**:
    * _Aspekty_: dyletantka, nawigacja w przepisach, "ja jestem zgodna z prawem", wie co jej wolno, znalezienie na kogoś haka, utrudnianie niemoralnych działań
    * _Opis_: Katia jako osoba zaangażowana skupia się na tym, by maksymalizować koszty prowadzenia biznesu w sposób nieetyczny. Z drugiej strony jest właścicielką niebezpiecznych przedmiotów i sama musi trzymać się prawa. Nie jest może najlepsza w tych tematach, ale jest to kolejne narzędzie w jej rękach by świat był lepszy.
    
### Silne i słabe strony:

* **Zwariowana badaczka**:
    * _Aspekty_: uważana za dziwną, czasem nie w tym świecie, bardzo dobrze kojarzy fakty, ignoruje normy społeczne, mistrzyni eliksirów, mistrzyni sympatii
    * _Opis_: Katia zachowuje się troszkę dziwnie z perspektywy norm społecznych zarówno magów jak i ludzi. Jej osobowość i umiejętności sprawiają, że zwykle znajdzie coś co inni przeoczą - niestety, jej sposób powiedzenia tego może sprawić, że zostanie odrzucona lub zignorowana. Niespecjalnie się tym przejmuje.

## Magia

### Szkoły magiczne

* **Biomancja**:
    * _Aspekty_: elementy sympatii, eliksiry, wzmacnianie własności istot żywych, ekstrakcja esencji, badanie i poznanie własności istot żywych
    * _Opis_: Jej najważniejsza szkoła. Wykorzystuje ją do badania różnych bytów żywych i modyfikacji ich pod kątem maksymalizacji sympatii. Diagnoza, badania, ekstrakcja, adaptacja. 
* **Magia materii**: 
    * _Aspekty_: elementy sympatii, eliksiry, wzmacnianie własności materii, ekstrakcja esencji, badanie i poznanie własności materii
    * _Opis_: W ramach magii materii Katia robi dokładnie to, co w ramach biomancji - bada, diagnozuje, ekstraktuje i adaptuje.
* **Kataliza**:
    * _Aspekty_: eliksiry, puryfikacja esencji, infuzja energii, stabilizacja pola magicznego, wzmocnienie sympatii, wygaszenie magii, amplifikacja mocy eliksiru
    * _Opis_: Praca nad eliksirami wymaga bardzo dokładnej manipulacji polem magicznym wśród składników zdolnych do zarezonowania. To sprawia, że Katia skupia się na wygaszaniu magii, stabilizowaniu energii magicznej i wzmocnieniu własności swoich eliksirów

### Zaklęcia statyczne

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 

## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Zarobki

* Pracuje w Eliksirze Aerinus

### Znam

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

### Mam

* **Świetne instrumenty badawcze**:
    * _Aspekty_: między magią, życiem i materią, zrozumienie natury rzeczy, znalezienie sympatii, wzmocnienie esencji
    * _Opis_: Katia skupia się na badaniach między materią, życiem i energią magiczną. To sprawia, że jej sprzęt pokrywa bardzo duży obszar pracy badawczej. Jeśli ma cokolwiek, co należy do przeciwnika najpewniej będzie w stanie zrobić kontreliksir.
* **Kolekcjonerka eliksirów**:
    * _Aspekty_: "jeśli wiem co to jest - ten eliksir to rozwiąże", "mam tam najdziwniejsze rzeczy", w rękach laika katastrofa
    * _Opis_: Niektórzy ludzie mają szafki z winem. Katia ma szafki z eliksirami, które kolekcjonuje z różnych części świata. Są tam najdziwniejsze i najciekawsze rzeczy, z jakimi się spotkała. Przypadkowo, rzadko ktoś próbuje się do niej włamać w obawie przed katastrofą. Eliksiry są oczywiście nieopisane - ona wie co i jak.
* ****:
    * _Aspekty_: 
    * _Opis_: 

# Opis

Katia Grajek, czarodziejka bardzo kompetentna w specjalności eliksirów, lekko zakręcona, dość młoda - ma 31 lata. Nienawidzi partactwa. Dawno temu wysłała sygnał SOS którą przechwyciła efemeryda.

### Koncept

Metaczarodziejka. Czaruje poprzez eliksiry - czaruje używając surowców. Ma bardzo silną synergię pionową w tym co robi, ale zakłada to użycia dwóch surowców: jeden do zbadania celu, drugi do wykorzystania eliksiru.

Wykorzystywana na sesjach jako amplifikator mocy postaci w kluczowych konfliktach i jako badacz czegoś dziwnego. Stały wehikuł konfliktu z kapitalistami i Myszeczką. Docelowo też mafią Dukata.

Pozytywna wersja Doctora Vipera.

### Motto

"Jak zdefiniujemy problem, rozwiąże go właściwy, piękny eliksir."

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Nie podłożona świnia Łucji](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html)|następnego dnia już nadaje się do pełni działania|Wizja Dukata|


## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|171001|czarodziejka bardzo kompetentna w specjalności eliksirów, lekko zakręcona, dość młoda. Nienawidzi partactwa. Dawno temu wysłała sygnał SOS którą przechwyciła efemeryda...|[Powrót do domu](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html)|11/08/25|11/08/27|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Sylwester Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-sylwester-bankierz.html)|1|[171001](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html)|
|[Robert Sądeczny](/rpg/inwazja/opowiesci/karty-postaci/1709-robert-sadeczny.html)|1|[171001](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html)|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|1|[171001](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html)|
|[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/1709-maria-newa.html)|1|[171001](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html)|
|[Kaja Maślaczek](/rpg/inwazja/opowiesci/karty-postaci/1709-kaja-maslaczek.html)|1|[171001](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html)|
|[Hektor Reszniaczek](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-reszniaczek.html)|1|[171001](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html)|
|[Gabriel Dukat](/rpg/inwazja/opowiesci/karty-postaci/9999-gabriel-dukat.html)|1|[171001](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|1|[171001](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html)|
|[Andrzej Toporek](/rpg/inwazja/opowiesci/karty-postaci/9999-andrzej-toporek.html)|1|[171001](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html)|

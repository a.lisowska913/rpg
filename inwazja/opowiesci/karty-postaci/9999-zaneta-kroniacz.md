---
layout: inwazja-karta-postaci
categories: profile
title: "Żaneta Kroniacz"
---
# {{ page.title }}

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Duch Opery](/rpg/inwazja/opowiesci/konspekty/170808-duch-opery.html)|ma krew viciniusa; nie jest w pełni człowiekiem. Uaktywniły się jej aspekty krwi viciniusa?|Powrót Karradraela|


## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170808|modelka, uśpiony vicinius, muza maga Piotra. Wpakowała się w gestalta starej opery. Ma tendencje do różnych używek. Fun-loving. Ma marzenia wielkości.|[Duch Opery](/rpg/inwazja/opowiesci/konspekty/170808-duch-opery.html)|10/02/17|10/02/19|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Zofia Łaziarak](/rpg/inwazja/opowiesci/karty-postaci/9999-zofia-laziarak.html)|1|[170808](/rpg/inwazja/opowiesci/konspekty/170808-duch-opery.html)|
|[Piotr Kit](/rpg/inwazja/opowiesci/karty-postaci/1709-piotr-kit.html)|1|[170808](/rpg/inwazja/opowiesci/konspekty/170808-duch-opery.html)|
|[Operiatrix](/rpg/inwazja/opowiesci/karty-postaci/9999-operiatrix.html)|1|[170808](/rpg/inwazja/opowiesci/konspekty/170808-duch-opery.html)|
|[Melodia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-melodia-diakon.html)|1|[170808](/rpg/inwazja/opowiesci/konspekty/170808-duch-opery.html)|
|[Marcelin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-marcelin-blakenbauer.html)|1|[170808](/rpg/inwazja/opowiesci/konspekty/170808-duch-opery.html)|
|[Jessica Czułmik](/rpg/inwazja/opowiesci/karty-postaci/9999-jessica-czulmik.html)|1|[170808](/rpg/inwazja/opowiesci/konspekty/170808-duch-opery.html)|

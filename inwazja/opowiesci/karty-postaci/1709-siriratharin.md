---
layout: inwazja-karta-postaci
categories: profile
factions: "Niezrzeszeni"
type: "NPC"
title: "Siriratharin"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **FIL: ekspansja**: 
    * _Aspekty_: 
    * _Opis_: 'to istoty kontrolujące teren i zasilające się przez terraformację… energio-formację tego terenu'
* **KLT: energetycznie oszczędny**:
    * _Aspekty_: asymilacja energii
    * _Opis_: 'nie lubi się ruszać, co?' … 'umysł speculoida często jest poza ciałem' … 'czekaj, patrzy przez LUSTRA?!' 'nie strać morale! Speculoid tylko na to czeka' … 'nie, proszę, tylko nie to!' … 'to moje przeznaczenie'
* **MRZ: pragnienie wiedzy**:
    * _Aspekty_: 
    * _Opis_: 'to fascynujące!'…'jako sojusznik na pewno nam się przyda'…'muszę go zrozumieć'
* **MRZ: zbudować dom**:
    * _Aspekty_: 
    * _Opis_: 'nie może istnieć poza Esuriit'…'ale jak? Tylko w bazie jesteśmy bezpieczni!'

### Umiejętności

* **inżynier energii**:
    * _Aspekty_: 
    * _Opis_: 
* **łowca-zabójca**: 
    * _Aspekty_: pułapki, terror, mind games, konwersja, tworzenie iluzji, niszczenie morale, mindwarp
    * _Opis_: 
* **Naukowiec Esuriit**:
    * _Aspekty_: badacz nie-Esuriit, konstruktor Esuriit, manipulacja Pryzmatem
    * _Opis_: 
	
### Silne i słabe strony:

* **Istota Esuriit**:
    * _Aspekty_: nadwrażliwość na srebro, odporność na magię Primusa i nie-Esuriit, częściowa polimorficzność, żywi się Siecią Esuriit, żywi się / adaptuje istoty ze zniszczonym morale; może je wypaczyć energią Esuriit
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

## Magia

### Szkoły magiczne

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

### Zaklęcia statyczne

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 

## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Zarobki

* ?

### Znam

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

### Mam

* **siatka energii z kontrolowanego przez siebie terenu**:
    * _Aspekty_: pod warunkiem że jakiś kontroluje
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

# Opis

A native entity of the Esuriit phase, one of the dominant species – a speculoid. When the silver candle magi came to this world, he got touched by a prism – and he has changed accordingly. His own home, Esuriit, started to push him away. He was unable to stay.

Caught by the magi of the candle, he was being influenced by their spells; they wanted to communicate with him. They didn’t understand that by doing it they have pushed him off balance from the Esuriit. Now he belongs nowhere.

Without a place to go home, this Hunter tries to create his own part of the Esuriit reality wherever he goes. And taste the differences in energies between various creatures of the world.

Magi want him as a weapon. He wants them as food. Revenge is a concept alien to him; but conquest and expansion is a concept deeply ingrained in this creature’s psyche. So is experimenting with different creatures especially now when some of them has given him an ability to live in their world without dissipating because of fundamental misalignments of their worlds’ energies.

He cannot really adapt, transform or use any creature without breaking its morale first. However, it is not very difficult for Esuriit Mindwarper - destroy morale, inject Esuriit energy and extract Esuriit energy leaving the Esuriit husk behind in place of non-Esuriit creature. Like all speculoids.

### Koncept

Wysokopoziomowy koncept postaci, 1-2 zdania.

### Motto

"Tekst"

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170125|ściągnął Zespół do Mare Crucio, zaatakował Whisperwind i mimo walki między nim, Whisper i Warmasterem i tak dał radę się wycofać.|[Przeprawa do Świecy Daemonica](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html)|10/08/10|10/08/11|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161124|nadal wyłączony z akcji. Okazuje się, że potrafi przeżyć i na Primusie i Esuriit i Daemonice; sprzężony z Kazimierzem Sowińskim.|[Ponura historia ekspedycji Esuriit](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html)|10/07/31|10/08/01|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161109|który prawie zgładził Zespół wysłany by uratować terminusa; zastawił pułapki lecz nie docenił technologii KADEMu (bo nie zna). Wyłączony z akcji na moment.|[Jak prawidłowo wpaść w pułapkę](/rpg/inwazja/opowiesci/konspekty/161109-jak-prawidlowo-wpasc-w-pulapke.html)|10/07/29|10/07/30|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161102|Speculoid; podobno nie może dostać się na teren bazy, ale jego szepty są wszechobecne.|[Magowie Esuriit w domu](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html)|10/07/26|10/07/28|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161026|speculoid; odpierany przez Srebrną Septę i monitorujący okolicę, by się pożywić istotami z Primusa. Zastawia pułapki.|[Zagłodzona ekspedycja Świecy](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html)|10/07/24|10/07/25|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|5|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html), [161109](/rpg/inwazja/opowiesci/konspekty/161109-jak-prawidlowo-wpasc-w-pulapke.html), [161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html), [161026](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html)|
|[Marianna Sowińska](/rpg/inwazja/opowiesci/karty-postaci/1709-marianna-sowinska.html)|5|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html), [161109](/rpg/inwazja/opowiesci/konspekty/161109-jak-prawidlowo-wpasc-w-pulapke.html), [161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html), [161026](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|5|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html), [161109](/rpg/inwazja/opowiesci/konspekty/161109-jak-prawidlowo-wpasc-w-pulapke.html), [161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html), [161026](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html)|
|[Mikado Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-mikado-diakon.html)|4|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html), [161109](/rpg/inwazja/opowiesci/konspekty/161109-jak-prawidlowo-wpasc-w-pulapke.html), [161026](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html)|
|[Ignat Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1802-ignat-zajcew.html)|4|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html), [161109](/rpg/inwazja/opowiesci/konspekty/161109-jak-prawidlowo-wpasc-w-pulapke.html), [161026](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html)|
|[Dosifiej Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-dosifiej-zajcew.html)|4|[161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html), [161109](/rpg/inwazja/opowiesci/konspekty/161109-jak-prawidlowo-wpasc-w-pulapke.html), [161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html), [161026](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html)|
|[Anna Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-myszeczka.html)|4|[161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html), [161109](/rpg/inwazja/opowiesci/konspekty/161109-jak-prawidlowo-wpasc-w-pulapke.html), [161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html), [161026](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html)|
|[Andżelika Leszczyńska](/rpg/inwazja/opowiesci/karty-postaci/1709-andzelika-leszczynska.html)|4|[161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html), [161109](/rpg/inwazja/opowiesci/konspekty/161109-jak-prawidlowo-wpasc-w-pulapke.html), [161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html), [161026](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html)|
|[Łukija Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-lukija-zajcew.html)|2|[161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html), [161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html)|
|[Zofia Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-zofia-weiner.html)|2|[161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html), [161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html)|
|[Konstanty Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-konstanty-bankierz.html)|2|[161109](/rpg/inwazja/opowiesci/konspekty/161109-jak-prawidlowo-wpasc-w-pulapke.html), [161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html)|
|[Jakub Pestka](/rpg/inwazja/opowiesci/karty-postaci/9999-jakub-pestka.html)|2|[161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html), [161026](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html)|
|[Fiodor Maius Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-fiodor-maius-zajcew.html)|2|[161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html), [161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html)|
|[Whisperwind](/rpg/inwazja/opowiesci/karty-postaci/9999-whisperwind.html)|1|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html)|
|[Warmaster](/rpg/inwazja/opowiesci/karty-postaci/9999-warmaster.html)|1|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html)|
|[Paweł Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-pawel-maus.html)|1|[161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html)|
|[Marta Szysznicka](/rpg/inwazja/opowiesci/karty-postaci/9999-marta-szysznicka.html)|1|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html)|
|[Lucjan Kopidół](/rpg/inwazja/opowiesci/karty-postaci/1803-lucjan-kopidol.html)|1|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html)|
|[Konstanty Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-konstanty-myszeczka.html)|1|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html)|
|[Konrad Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/9999-konrad-myszeczka.html)|1|[161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html)|
|[Kazimierz Sowiński](/rpg/inwazja/opowiesci/karty-postaci/9999-kazimierz-sowinski.html)|1|[161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html)|
|[Karina Paczulis](/rpg/inwazja/opowiesci/karty-postaci/9999-karina-paczulis.html)|1|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html)|
|[Infernia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-infernia-diakon.html)|1|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html)|
|[Eis](/rpg/inwazja/opowiesci/karty-postaci/9999-eis.html)|1|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html)|
|[Bazyli Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-bazyli-weiner.html)|1|[161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html)|

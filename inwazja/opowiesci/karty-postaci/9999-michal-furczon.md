---
layout: inwazja-karta-postaci
categories: profile
title: "Michał Furczon"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170515|bogatszy gość z ogrodem; Paulina zapewniła współpracę między nim a Hektorem. Ten człowiek jest dość zadowolony ze sprawy.|[Niewolnica w leasingu](/rpg/inwazja/opowiesci/konspekty/170515-niewolnica-w-leasingu.html)|10/01/20|10/01/22|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|1|[170515](/rpg/inwazja/opowiesci/konspekty/170515-niewolnica-w-leasingu.html)|
|[Hektor Reszniaczek](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-reszniaczek.html)|1|[170515](/rpg/inwazja/opowiesci/konspekty/170515-niewolnica-w-leasingu.html)|
|[Ewelina Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-ewelina-bankierz.html)|1|[170515](/rpg/inwazja/opowiesci/konspekty/170515-niewolnica-w-leasingu.html)|
|[Diana Łuczkiewicz](/rpg/inwazja/opowiesci/karty-postaci/9999-diana-luczkiewicz.html)|1|[170515](/rpg/inwazja/opowiesci/konspekty/170515-niewolnica-w-leasingu.html)|

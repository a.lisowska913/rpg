---
layout: inwazja-karta-postaci
categories: profile
title: "Edward Sasanka"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170519|obudzony przez Aegis, wrobiony przez nią w próbę zakładania własnej gildii i obrabianie TechVaulta KADEMu. Przerażony wynikiem.|[Odzyskać Aegis 0003](/rpg/inwazja/opowiesci/konspekty/170519-odzyskac-aegis-0003.html)|10/08/21|10/08/22|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|151021|który będąc nieprzytomnym całą misję wywołał więcej zamieszania niż kiedykolwiek. Wszyscy (poza Silurią) go szukają.|[Przebudzenie Mojry](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html)|10/06/02|10/06/03|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|151007|porywczy * mag Kurtyny, który zaatakował golemami usypiającymi chcąc uderzyć w Szlachtę i Blakenbauerów; padł do GS Aegis 0003.|[Nigdy dość przyjaciół: Szlachta i Kurtyna](/rpg/inwazja/opowiesci/konspekty/151007-nigdy-dosc-przyjaciol-szlachta-kurtyna.html)|10/05/29|10/05/30|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|3|[170519](/rpg/inwazja/opowiesci/konspekty/170519-odzyskac-aegis-0003.html), [151021](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html), [151007](/rpg/inwazja/opowiesci/konspekty/151007-nigdy-dosc-przyjaciol-szlachta-kurtyna.html)|
|[Wiktor Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1709-wiktor-sowinski.html)|2|[151021](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html), [151007](/rpg/inwazja/opowiesci/konspekty/151007-nigdy-dosc-przyjaciol-szlachta-kurtyna.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|2|[151021](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html), [151007](/rpg/inwazja/opowiesci/konspekty/151007-nigdy-dosc-przyjaciol-szlachta-kurtyna.html)|
|[GS "Aegis" 0003](/rpg/inwazja/opowiesci/karty-postaci/9999-gs-aegis-0003.html)|2|[151021](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html), [151007](/rpg/inwazja/opowiesci/konspekty/151007-nigdy-dosc-przyjaciol-szlachta-kurtyna.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|2|[151021](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html), [151007](/rpg/inwazja/opowiesci/konspekty/151007-nigdy-dosc-przyjaciol-szlachta-kurtyna.html)|
|[Diana Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-diana-weiner.html)|2|[151021](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html), [151007](/rpg/inwazja/opowiesci/konspekty/151007-nigdy-dosc-przyjaciol-szlachta-kurtyna.html)|
|[Adrian Murarz](/rpg/inwazja/opowiesci/karty-postaci/9999-adrian-murarz.html)|2|[151021](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html), [151007](/rpg/inwazja/opowiesci/konspekty/151007-nigdy-dosc-przyjaciol-szlachta-kurtyna.html)|
|[Vladlena Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-vladlena-zjacew.html)|1|[151007](/rpg/inwazja/opowiesci/konspekty/151007-nigdy-dosc-przyjaciol-szlachta-kurtyna.html)|
|[Teresa Koliczer](/rpg/inwazja/opowiesci/karty-postaci/9999-teresa-koliczer.html)|1|[170519](/rpg/inwazja/opowiesci/konspekty/170519-odzyskac-aegis-0003.html)|
|[Tatiana Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-tatiana-zajcew.html)|1|[151021](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html)|
|[Szymon Skubny](/rpg/inwazja/opowiesci/karty-postaci/9999-szymon-skubny.html)|1|[151021](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html)|
|[Stefan Jasionek](/rpg/inwazja/opowiesci/karty-postaci/9999-stefan-jasionek.html)|1|[170519](/rpg/inwazja/opowiesci/konspekty/170519-odzyskac-aegis-0003.html)|
|[Stalowy Śledzik Żarłacz](/rpg/inwazja/opowiesci/karty-postaci/9999-stalowy-sledzik-zarlacz.html)|1|[170519](/rpg/inwazja/opowiesci/konspekty/170519-odzyskac-aegis-0003.html)|
|[Sandra Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-sandra-maus.html)|1|[170519](/rpg/inwazja/opowiesci/konspekty/170519-odzyskac-aegis-0003.html)|
|[Rufus Czubek](/rpg/inwazja/opowiesci/karty-postaci/9999-rufus-czubek.html)|1|[151021](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html)|
|[Paweł Sępiak](/rpg/inwazja/opowiesci/karty-postaci/1709-pawel-sepiak.html)|1|[170519](/rpg/inwazja/opowiesci/konspekty/170519-odzyskac-aegis-0003.html)|
|[Oktawian Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-oktawian-maus.html)|1|[151007](/rpg/inwazja/opowiesci/konspekty/151007-nigdy-dosc-przyjaciol-szlachta-kurtyna.html)|
|[Mojra](/rpg/inwazja/opowiesci/karty-postaci/9999-mojra.html)|1|[151021](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html)|
|[Margaret Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-margaret-blakenbauer.html)|1|[151021](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html)|
|[Ilona Amant](/rpg/inwazja/opowiesci/karty-postaci/9999-ilona-amant.html)|1|[170519](/rpg/inwazja/opowiesci/konspekty/170519-odzyskac-aegis-0003.html)|
|[Ignat Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1802-ignat-zajcew.html)|1|[170519](/rpg/inwazja/opowiesci/konspekty/170519-odzyskac-aegis-0003.html)|
|[GS Aegis 0003](/rpg/inwazja/opowiesci/karty-postaci/9999-gs-aegis-0003.html)|1|[170519](/rpg/inwazja/opowiesci/konspekty/170519-odzyskac-aegis-0003.html)|
|[Emilia Kołatka](/rpg/inwazja/opowiesci/karty-postaci/1709-emilia-kolatka.html)|1|[151021](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html)|
|[Borys Kumin](/rpg/inwazja/opowiesci/karty-postaci/1709-borys-kumin.html)|1|[151021](/rpg/inwazja/opowiesci/konspekty/151021-przebudzenie-mojry.html)|
|[Aurel Czarko](/rpg/inwazja/opowiesci/karty-postaci/1709-aurel-czarko.html)|1|[151007](/rpg/inwazja/opowiesci/konspekty/151007-nigdy-dosc-przyjaciol-szlachta-kurtyna.html)|

---
layout: inwazja-karta-postaci
categories: profile
title: "Kornelia Szudek"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|140201|osoba nie chcąca mieć nic wspólnego z rodem Krówków oficjalnie a polująca na Radosława Krówkę dla siebie.|[Ona zdradza, on zdradza](/rpg/inwazja/opowiesci/konspekty/140201-ona-zdradza-on-zdradza.html)|10/01/11|10/01/12|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Teresa Żyraf](/rpg/inwazja/opowiesci/karty-postaci/9999-teresa-zyraf.html)|1|[140201](/rpg/inwazja/opowiesci/konspekty/140201-ona-zdradza-on-zdradza.html)|
|[Radosław Krówka](/rpg/inwazja/opowiesci/karty-postaci/9999-radoslaw-krowka.html)|1|[140201](/rpg/inwazja/opowiesci/konspekty/140201-ona-zdradza-on-zdradza.html)|
|[Nela Welon](/rpg/inwazja/opowiesci/karty-postaci/9999-nela-welon.html)|1|[140201](/rpg/inwazja/opowiesci/konspekty/140201-ona-zdradza-on-zdradza.html)|
|[Mojra](/rpg/inwazja/opowiesci/karty-postaci/9999-mojra.html)|1|[140201](/rpg/inwazja/opowiesci/konspekty/140201-ona-zdradza-on-zdradza.html)|
|[Mateusz Nieborak](/rpg/inwazja/opowiesci/karty-postaci/9999-mateusz-nieborak.html)|1|[140201](/rpg/inwazja/opowiesci/konspekty/140201-ona-zdradza-on-zdradza.html)|
|[Marian Rustyk](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-rustyk.html)|1|[140201](/rpg/inwazja/opowiesci/konspekty/140201-ona-zdradza-on-zdradza.html)|
|[Malwina Krówka](/rpg/inwazja/opowiesci/karty-postaci/9999-malwina-krowka.html)|1|[140201](/rpg/inwazja/opowiesci/konspekty/140201-ona-zdradza-on-zdradza.html)|
|[Krystalia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-krystalia-diakon.html)|1|[140201](/rpg/inwazja/opowiesci/konspekty/140201-ona-zdradza-on-zdradza.html)|
|[Emilia Szudek](/rpg/inwazja/opowiesci/karty-postaci/9999-emilia-szudek.html)|1|[140201](/rpg/inwazja/opowiesci/konspekty/140201-ona-zdradza-on-zdradza.html)|
|[Artur Żupan](/rpg/inwazja/opowiesci/karty-postaci/1803-artur-zupan.html)|1|[140201](/rpg/inwazja/opowiesci/konspekty/140201-ona-zdradza-on-zdradza.html)|
|[Andrea Wilgacz](/rpg/inwazja/opowiesci/karty-postaci/1709-andrea-wilgacz.html)|1|[140201](/rpg/inwazja/opowiesci/konspekty/140201-ona-zdradza-on-zdradza.html)|

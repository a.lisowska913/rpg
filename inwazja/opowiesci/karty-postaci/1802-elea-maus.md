---
layout: inwazja-karta-postaci
categories: profile
factions: "KADEM"
type: "NPC"
owner: "public"
title: "Elea Maus"
---
# {{ page.title }}

## Postać

### Motywacje (do czego dąży)

* **Indywidualne**:
    * _Aspekty_: mistrzostwo, przydatność
    * _Szczególnie_: idź jak najszybciej do przodu, NIE żałuj przeszłych czynów, pomóż ktokolwiek nie przyjdzie, NIE patrz na ekonomię
    * _Opis_: 
* **Społeczne**:
    * _Aspekty_: determinizm, bezpośredniość
    * _Szczególnie_: promuj społeczeństwo kastowe, NIE wolność jest wartością, promuj jasną komunikację, NIE konwenanse czy polityka
    * _Opis_: 
* **Serce**:
    * _Aspekty_: opiekuńczy, kontrarianin, rodzinny
    * _Szczególnie_: zawsze pomóż pacjentowi, NIE polityka czy prawo, zawsze zapewnij advocatus diaboli, NIE warto się dopasować
    * _Opis_: 
* **Działanie**:
    * _Aspekty_: amoralny, wojowniczy, wesoły
    * _Szczególnie_: śmiej się ze wszystkiego, NIE istnieją jakiekolwiek tabu, tylko wynik się liczy, NIE są granice nieprzekraczalne, nie akceptuj rozkazów, NIE mają prawo mówić Ci jak żyć
    * _Opis_: 

### Umiejętności

* **Lekarz**:
    * _Aspekty_: lekarz, puryfikator, 
    * _Manewry_: utrzymywanie przy życiu przez leki, szybkie badanie pacjenta przez próbkowanie, prototypowanie rozwiązania przez działanie na pacjencie
    * _Opis_: Elea nie ceni szczególnie oszczędzania bólu, niestety. Ból i strach są jej najlepszymi weryfikacjami czy lek działa czy nie...
* **Reżyser horrorów**:
    * _Aspekty_: reżyser, holokostki Mausów, koszmary
    * _Manewry_: efekt grozy przez holokostki, budzenie podziwu przez opowieści, złudzenie rzeczywistości przez fabułę, wyciąganie informacji przez koszmary
    * _Opis_: Jeśli pacjent / rodzina pacjenta nie chce współpracować, Elea sobie poradzi...
* **Neuronauta**:
    * _Aspekty_: neuronauta, psychiatra
    * _Manewry_: dominowanie przez koszmary, naprawa umysłu przez grozę, eksploracja umysłu przez fabułę
    * _Opis_: Elea naprawia swoich pacjentów przez modelowanie ich życia opowieściami i obrazami jeszcze straszniejszymi niż to co im się stało.

### Silne i słabe strony:

* **Medicus Superior**:
    * _Aspekty_: 
    * _Manewry_:
    * _Opis_: Homo Superior zaprojektowana by być lekarzem.

### Szkoły magiczne

* **Biomancja**:
    * _Aspekty_: lekarz ciała, 
    * _Manewry_:
    * _Opis_: 
* **Magia mentalna**: 
    * _Aspekty_: lekarz umysłów, 
    * _Manewry_:
    * _Opis_: 
* **Kataliza**:
    * _Aspekty_: lekarz energii, puryfikacja
    * _Manewry_:
    * _Opis_: 

## Zasoby i otoczenie
### Powiązane frakcje

{{ page.factions }}

### Zasoby

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

# Opis

Elea została zaprojektowana i wyhodowana 41 lat temu z woli Karradraela, który uznał, że potrzebny jest wybitnej klasy lekarz rodowi Maus. Jednocześnie nadał jej cechy niosące za sobą dużą korzyść PRową, jak i pozbawił ją odpowiednich zahamowań które normalnie stoją na drodze lekarza do osiągnięcia optymalnej skuteczności w wykonywaniu rozkazów Jonatana Mausa.

Oczywiście, nie wszystko się udało.

Elea Maus jest fatalistką, jak przystało na istotę powołaną i nadzorowaną przez żyjącego boga. Jednocześnie jest wojowniczą, rodzinną kontrarianką, która ma bardzo silną wolę i niewyparzoną gębę. Zawsze chce wszystkich chronić i opiekuje się swoimi pacjentami, niezależnie od tego czy to ludzie, magowie czy viciniusy.

Wybitna lekarka, która nie czuje się lepsza czy wyższa - jest tym, do czego została zaprojektowana. Wierzy, że każdy ma swoje miejsce w społeczeństwie i nie należy złościć się na swój los - jednocześnie będzie walczyła przeciwko każdej sile, która próbuje stanąć jej na drodze w osiąganiu celu nadanego jej przez Karradraela.

Jest bardzo sympatyczną i wesołą czarodziejką, której hobby są koszmary i horrory. Jako neuronautka często tworzy upiorne holokostki. Zapytana "dlaczego" odpowiada "bo to mnie odpręża".

Przez długi czas Elea uczestniczyła w programie Rodziny Świecy wierząc, że da się pogodzić Świecę i Mausów - jednak wydarzenia Wojny Karradraela sprawiły, że utraciła nadzieję. Chwilowo przycupnęła w niewielkiej przychodni i czeka na dalszy rozwój sytuacji.

### Koncept

Doctor Diana (Cybertronic) + Mama Bear + Scarecrow (Batman)

### Mapa kreacji

brak

### Motto

"Nasze miejsce w egzystencji nie zależy od nas, ale nasze działania - tak."

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Krystalia poluje na niekralotha](/rpg/inwazja/opowiesci/konspekty/180526-krystalia-poluje-na-niekralotha.html)|Marek Kromlan w końcu ją polubił. Nie jest dla niego "Mauską" a "Eleą, która ryzykuje dla ratowania ludzi"|Adaptacja kralotyczna|
|[Krystalia poluje na niekralotha](/rpg/inwazja/opowiesci/konspekty/180526-krystalia-poluje-na-niekralotha.html)|rozczarowana Silurią; Siluria może i wszystkich ostrzegała, ale jednak nie stanęła po właściwej stronie - przeciw Krystalii|Adaptacja kralotyczna|


## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|180526|nie zgodziła się na traktowanie ludzi jako przynętę na niekralotha i zwalczała środki Krystalii. Padła, gdy Krystalia przyszła do niej osobiście.|[Krystalia poluje na niekralotha](/rpg/inwazja/opowiesci/konspekty/180526-krystalia-poluje-na-niekralotha.html)|10/12/17|10/12/19|[Adaptacja kralotyczna](/rpg/inwazja/opowiesci/konspekty/kampania-adaptacja-kralotyczna.html)|
|180402|silnie współpracuje z Silurią w sprawie Mirandy i policjanta. Zniknął jej magiczny stetoskop; nie Maria go zabrała. Bardzo martwi się Mirandą.|[Pętla dookoła niekralotha](/rpg/inwazja/opowiesci/konspekty/180402-petla-dookola-niekralotha.html)|10/12/13|10/12/15|[Adaptacja kralotyczna](/rpg/inwazja/opowiesci/konspekty/kampania-adaptacja-kralotyczna.html)|
|180318|próbuje wszystkich uratować i twierdzi, że Miranda Maus jest strasznym zagrożeniem dla rodu przez to, że pokazuje słabość Abelarda / Karradraela.|[Czyżby drugi kraloth?](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html)|10/12/05|10/12/07|[Adaptacja kralotyczna](/rpg/inwazja/opowiesci/konspekty/kampania-adaptacja-kralotyczna.html)|
|180310|teraz lekarka w przychodni w Kotach. Keeps low profile. Nic się nie zmieniła, niczego nie żałuje. Ratuje ludzi, z pomocą polityczną Silurii.|[Kraloth w piwnicy](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|10/11/28|10/12/01|[Adaptacja kralotyczna](/rpg/inwazja/opowiesci/konspekty/kampania-adaptacja-kralotyczna.html)|
|160713|która odkryła lekarstwo na Irytkę. Gdy agenci Świecy chcieli ją ewakuować przez portal, zdradziła i została z Wiktorem Sowińskim.|[Jak ukraść ze Świecy Zajcewów](/rpg/inwazja/opowiesci/konspekty/160713-ukrasc-ze-swiecy-zajcewow.html)|10/07/04|10/07/06|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160420|która zdeterminowana jest albo uratować Baltazara albo spalić Malię. Obie wersje pasują zdesperowanej matce.|[Kolizja dwóch sojuszy](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html)|10/07/01|10/07/02|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160412|która coraz gorzej radzi sobie z atmosferą budzącą jej instynkty defilera. Przeczuwa coś strasznego i cierpi z braku Karradraela.|[Spleśniała dusza terminuski](/rpg/inwazja/opowiesci/konspekty/160412-splesniala-dusza-terminuski.html)|10/07/01|10/07/02|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160821|która straciła nadzieję; poddała się przeznaczeniu nie próbując już nawet z nim walczyć i jedynie ratując to, co jest w stanie|[Wycofanie Mileny z piekła](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html)|10/06/26|10/06/30|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160404|amoralna lekarka i reżyser holokryształów, która zobaczyła prawdziwą potęgę w Silurii i jak Maus do Karradraela lgnie do niej.|[The power of cute pet](/rpg/inwazja/opowiesci/konspekty/160404-the-power-of-cute-pet.html)|10/06/25|10/06/26|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160309|lekarz * magiczny; zna Edwina i wraz z nim próbowała opracować coś do zwalczenia Irytki Sprzężonej.|[Irytka Sprzężona](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|10/06/25|10/06/26|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160506|bliska przełomu w pracy nad Irytką Sprzężoną.|[Wyścig pająka z terminuską](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|10/06/22|10/06/24|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160424|która uświadomiła sobie COŚ co prawie wpędziło ją w komę, ale jako lekarz chce wszystkich uratować; demonstracyjnie wzięła Silurię w opiekę|[Uważaj, o czym marzysz](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|10/06/18|10/06/21|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160417|zdesperowana przyszywana matka która zbuntowała się seirasowi i zdecydowała się wesprzeć Szlachtę. Uważa, że Ernest Maus zniszczy Ród.|[Symptomy kryzysu Świecy](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|10/06/16|10/06/17|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150920|matka Baltazara Mausa z Rodziny Świecy; przeprowadziła małe śledztwo w sprawie dziewczyny Baltazara, Malii.|[Sprawa Baltazara Mausa](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|10/05/27|10/05/28|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|8|[180526](/rpg/inwazja/opowiesci/konspekty/180526-krystalia-poluje-na-niekralotha.html), [180402](/rpg/inwazja/opowiesci/konspekty/180402-petla-dookola-niekralotha.html), [180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html), [180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html), [160412](/rpg/inwazja/opowiesci/konspekty/160412-splesniala-dusza-terminuski.html), [160821](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html), [160404](/rpg/inwazja/opowiesci/konspekty/160404-the-power-of-cute-pet.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Wiktor Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1709-wiktor-sowinski.html)|6|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [160412](/rpg/inwazja/opowiesci/konspekty/160412-splesniala-dusza-terminuski.html), [160821](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html), [160404](/rpg/inwazja/opowiesci/konspekty/160404-the-power-of-cute-pet.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Dagmara Wyjątek](/rpg/inwazja/opowiesci/karty-postaci/1709-dagmara-wyjątek.html)|6|[160412](/rpg/inwazja/opowiesci/konspekty/160412-splesniala-dusza-terminuski.html), [160821](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html), [160404](/rpg/inwazja/opowiesci/konspekty/160404-the-power-of-cute-pet.html), [160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|5|[160713](/rpg/inwazja/opowiesci/konspekty/160713-ukrasc-ze-swiecy-zajcewow.html), [160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Wioletta Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-wioletta-bankierz.html)|4|[160713](/rpg/inwazja/opowiesci/konspekty/160713-ukrasc-ze-swiecy-zajcewow.html), [160412](/rpg/inwazja/opowiesci/konspekty/160412-splesniala-dusza-terminuski.html), [160404](/rpg/inwazja/opowiesci/konspekty/160404-the-power-of-cute-pet.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Ozydiusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-ozydiusz-bankierz.html)|4|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html), [150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|
|[Marek Kromlan](/rpg/inwazja/opowiesci/karty-postaci/1803-marek-kromlan.html)|4|[180526](/rpg/inwazja/opowiesci/konspekty/180526-krystalia-poluje-na-niekralotha.html), [180402](/rpg/inwazja/opowiesci/konspekty/180402-petla-dookola-niekralotha.html), [180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html), [180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Klara Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-klara-blakenbauer.html)|4|[160713](/rpg/inwazja/opowiesci/konspekty/160713-ukrasc-ze-swiecy-zajcewow.html), [160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|4|[180402](/rpg/inwazja/opowiesci/konspekty/180402-petla-dookola-niekralotha.html), [180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html), [180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html), [160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Diana Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-diana-weiner.html)|4|[160412](/rpg/inwazja/opowiesci/konspekty/160412-splesniala-dusza-terminuski.html), [160404](/rpg/inwazja/opowiesci/konspekty/160404-the-power-of-cute-pet.html), [160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Aleksander Sowiński](/rpg/inwazja/opowiesci/karty-postaci/9999-aleksander-sowinski.html)|4|[160404](/rpg/inwazja/opowiesci/konspekty/160404-the-power-of-cute-pet.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html), [160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html), [150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|
|[Sabina Sowińska](/rpg/inwazja/opowiesci/karty-postaci/1709-sabina-sowinska.html)|3|[160412](/rpg/inwazja/opowiesci/konspekty/160412-splesniala-dusza-terminuski.html), [160404](/rpg/inwazja/opowiesci/konspekty/160404-the-power-of-cute-pet.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Netheria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-netheria-diakon.html)|3|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html), [180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html), [160404](/rpg/inwazja/opowiesci/konspekty/160404-the-power-of-cute-pet.html)|
|[Marian Agrest](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-agrest.html)|3|[160713](/rpg/inwazja/opowiesci/konspekty/160713-ukrasc-ze-swiecy-zajcewow.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Laragnarhag](/rpg/inwazja/opowiesci/karty-postaci/1709-laragnarhag.html)|3|[180402](/rpg/inwazja/opowiesci/konspekty/180402-petla-dookola-niekralotha.html), [160412](/rpg/inwazja/opowiesci/konspekty/160412-splesniala-dusza-terminuski.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Judyta Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-judyta-maus.html)|3|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html), [180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html), [160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Artur Żupan](/rpg/inwazja/opowiesci/karty-postaci/1803-artur-zupan.html)|3|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html), [160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html), [150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|
|[Andrea Wilgacz](/rpg/inwazja/opowiesci/karty-postaci/1709-andrea-wilgacz.html)|3|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html), [160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html), [150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|
|[Amanda Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-amanda-diakon.html)|3|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Tadeusz Baran](/rpg/inwazja/opowiesci/karty-postaci/1709-tadeusz-baran.html)|2|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html), [160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
|[Roman Bruniewicz](/rpg/inwazja/opowiesci/karty-postaci/9999-roman-bruniewicz.html)|2|[180526](/rpg/inwazja/opowiesci/konspekty/180526-krystalia-poluje-na-niekralotha.html), [180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Milena Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-milena-diakon.html)|2|[160821](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html), [150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|
|[Marian Łajdak](/rpg/inwazja/opowiesci/karty-postaci/1709-marian-lajdak.html)|2|[180526](/rpg/inwazja/opowiesci/konspekty/180526-krystalia-poluje-na-niekralotha.html), [180402](/rpg/inwazja/opowiesci/konspekty/180402-petla-dookola-niekralotha.html)|
|[Maria Przysiadek](/rpg/inwazja/opowiesci/karty-postaci/9999-maria-przysiadek.html)|2|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html), [180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Margaret Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-margaret-blakenbauer.html)|2|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Marcelin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-marcelin-blakenbauer.html)|2|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Malia Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-malia-bankierz.html)|2|[160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html), [150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|
|[Leonidas Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-leonidas-blakenbauer.html)|2|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Krystalia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-krystalia-diakon.html)|2|[180526](/rpg/inwazja/opowiesci/konspekty/180526-krystalia-poluje-na-niekralotha.html), [180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Kleofas Bór](/rpg/inwazja/opowiesci/karty-postaci/1709-kleofas-bor.html)|2|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Kajetan Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-kajetan-weiner.html)|2|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Julian Strąk](/rpg/inwazja/opowiesci/karty-postaci/9999-julian-strak.html)|2|[180526](/rpg/inwazja/opowiesci/konspekty/180526-krystalia-poluje-na-niekralotha.html), [180402](/rpg/inwazja/opowiesci/konspekty/180402-petla-dookola-niekralotha.html)|
|[Hralglanath](/rpg/inwazja/opowiesci/karty-postaci/9999-hralglanath.html)|2|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html), [180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Gerwazy Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/9999-gerwazy-myszeczka.html)|2|[160713](/rpg/inwazja/opowiesci/konspekty/160713-ukrasc-ze-swiecy-zajcewow.html), [160404](/rpg/inwazja/opowiesci/konspekty/160404-the-power-of-cute-pet.html)|
|[Franciszek Knur](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-knur.html)|2|[180526](/rpg/inwazja/opowiesci/konspekty/180526-krystalia-poluje-na-niekralotha.html), [180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Fortitia Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-fortitia-diakon.html)|2|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html), [160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
|[Estrella Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-estrella-diakon.html)|2|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html), [160821](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html)|
|[Emilia Kołatka](/rpg/inwazja/opowiesci/karty-postaci/1709-emilia-kolatka.html)|2|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Elizawieta Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-elizawieta-zajcew.html)|2|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html), [150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|
|[Bójka Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-bojka-diakon.html)|2|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html), [180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Bianka Stein](/rpg/inwazja/opowiesci/karty-postaci/1709-bianka-stein.html)|2|[160713](/rpg/inwazja/opowiesci/konspekty/160713-ukrasc-ze-swiecy-zajcewow.html), [160821](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html)|
|[Baltazar Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-baltazar-maus.html)|2|[160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html), [150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|
|[Aneta Rainer](/rpg/inwazja/opowiesci/karty-postaci/1709-aneta-rainer.html)|2|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Zuzanna Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-zuzanna-maus.html)|1|[160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
|[Wojciech Żądło](/rpg/inwazja/opowiesci/karty-postaci/9999-wojciech-zadlo.html)|1|[160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
|[Whisperwind](/rpg/inwazja/opowiesci/karty-postaci/9999-whisperwind.html)|1|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html)|
|[Wanda Ketran](/rpg/inwazja/opowiesci/karty-postaci/1709-wanda-ketran.html)|1|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Tymotheus Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-tymotheus-blakenbauer.html)|1|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html)|
|[Tatiana Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-tatiana-zajcew.html)|1|[160713](/rpg/inwazja/opowiesci/konspekty/160713-ukrasc-ze-swiecy-zajcewow.html)|
|[Szczepan Przysiadek](/rpg/inwazja/opowiesci/karty-postaci/9999-szczepan-przysiadek.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Ryszard Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-ryszard-weiner.html)|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Rufus Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-rufus-maus.html)|1|[160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
|[Rufus Czubek](/rpg/inwazja/opowiesci/karty-postaci/9999-rufus-czubek.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Romeo Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-romeo-diakon.html)|1|[160404](/rpg/inwazja/opowiesci/konspekty/160404-the-power-of-cute-pet.html)|
|[Remigiusz Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-remigiusz-zajcew.html)|1|[150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|
|[Rafał Drętwoń](/rpg/inwazja/opowiesci/karty-postaci/9999-rafal-dretwon.html)|1|[180402](/rpg/inwazja/opowiesci/konspekty/180402-petla-dookola-niekralotha.html)|
|[Quasar](/rpg/inwazja/opowiesci/karty-postaci/9999-quasar.html)|1|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html)|
|[Paulina Widoczek](/rpg/inwazja/opowiesci/karty-postaci/9999-paulina-widoczek.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|1|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html)|
|[Patrycja Krowiowska](/rpg/inwazja/opowiesci/karty-postaci/1709-patrycja-krowiowska.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Otton Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-otton-blakenbauer.html)|1|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Olga Miodownik](/rpg/inwazja/opowiesci/karty-postaci/1709-olga-miodownik.html)|1|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html)|
|[Oktawian Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-oktawian-maus.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Norbert Sonet](/rpg/inwazja/opowiesci/karty-postaci/9999-norbert-sonet.html)|1|[180526](/rpg/inwazja/opowiesci/konspekty/180526-krystalia-poluje-na-niekralotha.html)|
|[Mordecja Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-mordecja-diakon.html)|1|[160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
|[Mojra](/rpg/inwazja/opowiesci/karty-postaci/9999-mojra.html)|1|[160713](/rpg/inwazja/opowiesci/konspekty/160713-ukrasc-ze-swiecy-zajcewow.html)|
|[Mirabelka Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-mirabelka-diakon.html)|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Mieszko Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-mieszko-bankierz.html)|1|[160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
|[Melodia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-melodia-diakon.html)|1|[180402](/rpg/inwazja/opowiesci/konspekty/180402-petla-dookola-niekralotha.html)|
|[Lucjan Kopidół](/rpg/inwazja/opowiesci/karty-postaci/1803-lucjan-kopidol.html)|1|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html)|
|[Lidia Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-lidia-weiner.html)|1|[160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
|[Laurena Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-laurena-bankierz.html)|1|[160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
|[Kinga Melit](/rpg/inwazja/opowiesci/karty-postaci/9999-kinga-melit.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Kermit Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-kermit-diakon.html)|1|[160821](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html)|
|[Katalina Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-katalina-bankierz.html)|1|[160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
|[Karolina Kupiec](/rpg/inwazja/opowiesci/karty-postaci/1803-karolina-kupiec.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Karol Poczciwiec](/rpg/inwazja/opowiesci/karty-postaci/9999-karol-poczciwiec.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Julian Pszczelak](/rpg/inwazja/opowiesci/karty-postaci/1709-julian-pszczelak.html)|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Judyta Karnisz](/rpg/inwazja/opowiesci/karty-postaci/9999-judyta-karnisz.html)|1|[160821](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html)|
|[Joachim Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-zajcew.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Jan Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-weiner.html)|1|[160417](/rpg/inwazja/opowiesci/konspekty/160417-symptomy-kryzysu-swiecy.html)|
|[Jakub Niecień](/rpg/inwazja/opowiesci/karty-postaci/9999-jakub-niecien.html)|1|[160821](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html)|
|[Ignat Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1802-ignat-zajcew.html)|1|[150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|
|[Grzegorz Nocniarz](/rpg/inwazja/opowiesci/karty-postaci/9999-grzegorz-nocniarz.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Fryderyk Chwost](/rpg/inwazja/opowiesci/karty-postaci/9999-fryderyk-chwost.html)|1|[160412](/rpg/inwazja/opowiesci/konspekty/160412-splesniala-dusza-terminuski.html)|
|[Filip Czółno](/rpg/inwazja/opowiesci/karty-postaci/9999-filip-czolno.html)|1|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html)|
|[Edward Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-edward-diakon.html)|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Edward Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-edward-bankierz.html)|1|[160821](/rpg/inwazja/opowiesci/konspekty/160821-wycofanie-mileny-z-piekla.html)|
|[Dionizy Kret](/rpg/inwazja/opowiesci/karty-postaci/1709-dionizy-kret.html)|1|[160713](/rpg/inwazja/opowiesci/konspekty/160713-ukrasc-ze-swiecy-zajcewow.html)|
|[Bolesław Derwisz](/rpg/inwazja/opowiesci/karty-postaci/9999-boleslaw-derwisz.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Bogdan Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-bogdan-bankierz.html)|1|[150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|
|[Baltazar Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1802-baltazar-sowinski.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Aurel Czarko](/rpg/inwazja/opowiesci/karty-postaci/1709-aurel-czarko.html)|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Artur Bryś](/rpg/inwazja/opowiesci/karty-postaci/1709-artur-brys.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Arkadiusz Klusiński](/rpg/inwazja/opowiesci/karty-postaci/9999-arkadiusz-klusinski.html)|1|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Anna Kajak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kajak.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Alina Bednarz](/rpg/inwazja/opowiesci/karty-postaci/1803-alina-bednarz.html)|1|[160713](/rpg/inwazja/opowiesci/konspekty/160713-ukrasc-ze-swiecy-zajcewow.html)|
|[Adrian Murarz](/rpg/inwazja/opowiesci/karty-postaci/9999-adrian-murarz.html)|1|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Adrian Kropiak](/rpg/inwazja/opowiesci/karty-postaci/9999-adrian-kropiak.html)|1|[150920](/rpg/inwazja/opowiesci/konspekty/150920-sprawa-baltazara-mausa.html)|

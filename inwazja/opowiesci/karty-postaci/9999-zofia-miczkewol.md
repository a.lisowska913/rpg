---
layout: inwazja-karta-postaci
categories: profile
title: "Zofia Miczkewoł"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|160526|starsza nauczycielka, TODO|[Bobrzańskie Gumifoczki](/rpg/inwazja/opowiesci/konspekty/160526-bobrzanskie-gumifoczki.html)|10/01/01|10/01/02|[Nie umieszczone, Anulowane](/rpg/inwazja/opowiesci/konspekty/kampania-anulowane.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Zdzisława Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/9999-zdzislawa-myszeczka.html)|1|[160526](/rpg/inwazja/opowiesci/konspekty/160526-bobrzanskie-gumifoczki.html)|
|[Stefan Głamot](/rpg/inwazja/opowiesci/karty-postaci/9999-stefan-glamot.html)|1|[160526](/rpg/inwazja/opowiesci/konspekty/160526-bobrzanskie-gumifoczki.html)|
|[Roman Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-roman-weiner.html)|1|[160526](/rpg/inwazja/opowiesci/konspekty/160526-bobrzanskie-gumifoczki.html)|
|[Luiza Łapińska](/rpg/inwazja/opowiesci/karty-postaci/9999-luiza-lapinska.html)|1|[160526](/rpg/inwazja/opowiesci/konspekty/160526-bobrzanskie-gumifoczki.html)|
|[Grigorij Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-grigorij-zajcew.html)|1|[160526](/rpg/inwazja/opowiesci/konspekty/160526-bobrzanskie-gumifoczki.html)|
|[Arkadiusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-arkadiusz-bankierz.html)|1|[160526](/rpg/inwazja/opowiesci/konspekty/160526-bobrzanskie-gumifoczki.html)|

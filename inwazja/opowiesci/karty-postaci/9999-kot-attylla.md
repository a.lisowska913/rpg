---
layout: inwazja-karta-postaci
categories: profile
title: "kot Attylla"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|160608|który podobno jest bardzo groźnym viciniusem (nawet smokiem). Na razie zachowuje się jak mały, puchaty kiciuś. Tylko że patrzy w taki straszny sposób...|[Czy on wyszedł z Rifta...?](/rpg/inwazja/opowiesci/konspekty/160608-czy-on-wyszedl-z-rifta.html)|10/01/01|10/01/02|[Nie umieszczone, Anulowane](/rpg/inwazja/opowiesci/konspekty/kampania-anulowane.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Zdzisława Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/9999-zdzislawa-myszeczka.html)|1|[160608](/rpg/inwazja/opowiesci/konspekty/160608-czy-on-wyszedl-z-rifta.html)|
|[Roman Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-roman-weiner.html)|1|[160608](/rpg/inwazja/opowiesci/konspekty/160608-czy-on-wyszedl-z-rifta.html)|
|[Luiza Łapińska](/rpg/inwazja/opowiesci/karty-postaci/9999-luiza-lapinska.html)|1|[160608](/rpg/inwazja/opowiesci/konspekty/160608-czy-on-wyszedl-z-rifta.html)|
|[Grigorij Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-grigorij-zajcew.html)|1|[160608](/rpg/inwazja/opowiesci/konspekty/160608-czy-on-wyszedl-z-rifta.html)|
|[Arkadiusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-arkadiusz-bankierz.html)|1|[160608](/rpg/inwazja/opowiesci/konspekty/160608-czy-on-wyszedl-z-rifta.html)|

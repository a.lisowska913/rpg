---
layout: inwazja-karta-postaci
categories: profile
title: "Cyprian Koziej"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|160922|kontrowersyjny pisarz fantasy, przyciągający uwagę; nie był w stanie zamaskować obecności Supernowej (zbyt piękna i angażująca)|[Czarnoskalski konwent RPG](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|10/05/14|10/05/19|[Taniec Liści](/rpg/inwazja/opowiesci/konspekty/kampania-taniec-lisci.html)|
|150325|pisarz fantasy, którego dziełem inspirował się morderca. Całkowicie nieistotny poza byciem fałszywym tropem ;P.|[Morderstwo jak w książce](/rpg/inwazja/opowiesci/konspekty/150325-morderstwo-jak-w-ksiazce.html)|10/04/29|10/04/30|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Supernowa Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-supernowa-diakon.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Rafał Kniaź](/rpg/inwazja/opowiesci/karty-postaci/9999-rafal-kniaz.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|1|[150325](/rpg/inwazja/opowiesci/konspekty/150325-morderstwo-jak-w-ksiazce.html)|
|[Patrycja Krowiowska](/rpg/inwazja/opowiesci/karty-postaci/1709-patrycja-krowiowska.html)|1|[150325](/rpg/inwazja/opowiesci/konspekty/150325-morderstwo-jak-w-ksiazce.html)|
|[Olga Miodownik](/rpg/inwazja/opowiesci/karty-postaci/1709-olga-miodownik.html)|1|[150325](/rpg/inwazja/opowiesci/konspekty/150325-morderstwo-jak-w-ksiazce.html)|
|[Mikado Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-mikado-diakon.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Mateusz Ackmann](/rpg/inwazja/opowiesci/karty-postaci/1709-mateusz-ackmann.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/1709-maria-newa.html)|1|[150325](/rpg/inwazja/opowiesci/konspekty/150325-morderstwo-jak-w-ksiazce.html)|
|[Margaret Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-margaret-blakenbauer.html)|1|[150325](/rpg/inwazja/opowiesci/konspekty/150325-morderstwo-jak-w-ksiazce.html)|
|[Kornel Wadera](/rpg/inwazja/opowiesci/karty-postaci/1709-kornel-wadera.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Kleofas Bór](/rpg/inwazja/opowiesci/karty-postaci/1709-kleofas-bor.html)|1|[150325](/rpg/inwazja/opowiesci/konspekty/150325-morderstwo-jak-w-ksiazce.html)|
|[Jessica Ruczaj](/rpg/inwazja/opowiesci/karty-postaci/9999-jessica-ruczaj.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Infensa Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-infensa-diakon.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|1|[150325](/rpg/inwazja/opowiesci/konspekty/150325-morderstwo-jak-w-ksiazce.html)|
|[Esme Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-esme-myszeczka.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|1|[150325](/rpg/inwazja/opowiesci/konspekty/150325-morderstwo-jak-w-ksiazce.html)|
|[Dorota Gacek](/rpg/inwazja/opowiesci/karty-postaci/1709-dorota-gacek.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Bolesław Derwisz](/rpg/inwazja/opowiesci/karty-postaci/9999-boleslaw-derwisz.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Artur Pawiak](/rpg/inwazja/opowiesci/karty-postaci/9999-artur-pawiak.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Artur Bryś](/rpg/inwazja/opowiesci/karty-postaci/1709-artur-brys.html)|1|[150325](/rpg/inwazja/opowiesci/konspekty/150325-morderstwo-jak-w-ksiazce.html)|
|[Aptoform Mirasilaler](/rpg/inwazja/opowiesci/karty-postaci/9999-aptoform-mirasilaler.html)|1|[150325](/rpg/inwazja/opowiesci/konspekty/150325-morderstwo-jak-w-ksiazce.html)|

---
layout: inwazja-karta-postaci
categories: profile
factions: "Ptasi Trójkąt"
type: "NPC"
title: "Zofia Przylga"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **BÓL: Samotna, niegodna córka**
    * _Aspekty_: nigdy nie dorówna rodzicom, nie utrzyma Dumnego Orła, zostanie zniewolona i spętana, umrze samotnie
    * _Opis_: Zofia jest córką konstruktorów Dumnego Orła, którzy stracili moc podczas Zaćmienia. Próbuje zapewnić, by Dumny Orzeł działał jak najlepiej - zapewnić załogę, paliwo, energię, stealth... miota się pomiędzy obowiązkowością a chęcią życia swobodnego, na wolności. Dodajmy fakt, że aktywnie szuka sobie godnego siebie partnera - kogoś, kto jej zaimponuje i dorówna oraz będzie kochać Orła tak, jak ona go kocha i mamy recepturę na katastrofę...
* **FIL: "Zjednoczenie, stabilność i sojusze prowadzą do Karradraela"**:
    * _Aspekty_: zwalcza zjednoczenia i sojusze, dąży do konfliktów, wieczny postęp i ewolucja, więcej rywalizacji!, dąży do całkowitej wolności, "zbyt duże organizacje są niebezpieczne"
    * _Opis_: Zofia silnie wierzy, że jedynie w ciągłym konflikcie i chaosie dochodzi do postępu. Rywalizacja i walka nie ma miejsca na poziomie rodów czy krajów - ma miejsce między światami. Chce wprowadzić chaos i niezgodę, by zapewnić, że drugi Karradrael nie nadejdzie. Przez to zwalcza Świecę i Dukata i wszelkie próby stabilizacji. Wierzy w małe, silnie rywalizujące siły a nie ogromne, wolne struktury.
* **ŚR: "I am air. I am discord. I am chaos."**:
    * _Aspekty_: przekraczać wszelkie granice, impulsywna ryzykantka, żyje chwilą, SZYBCIEJ!, wzbudzać silne emocje (i strach), niezbyt przywiązana do świata, nieustraszona
    * _Opis_: Zofia nie przywiązuje się do nikogo i niczego. Żyje z minuty na minutę; pragnie SZYBCIEJ i piękniej. Kocha ryzyko, nurkowanie z ogromną pięknością z nadzieją, że element powietrza uratuje ją przed śmiercią. To osoba, która potrafi rzucić się z dachu w dół, śmiejąc się z radością. Bawi ją strach innych. Była członkiem Dare Shiver.

### Umiejętności

* **Kurier wyścigowy**:
    * _Aspekty_: ogromne prędkości, prowadzenie motoru, przemycanie małych obiektów, ściganie się, gubienie przeciwnika, transport obiektów niebezpiecznych, niebezpieczne manewry
    * _Opis_: Zofia przewozi różne delikatne i niebezpieczne rzeczy, najczęściej coś, czego nikt inny nie chce dotknąć. Lubi się ścigać i robić rzeczy które będą zmuszały jej do działania na dużych prędkościach. Ważniejsze dla niej jest to co będzie robiła podczas zadania niż dla kogo to robi lub jaki ma to skutek
* **Złodziej**: 
    * _Aspekty_: włamania, podkładanie dowodów, kradzież kieszonkowa, wślizgiwanie się, szybka ucieczka, lokalizowanie zabezpieczeń, zwinna akrobatka, szybka kradzież obiektu w polu widzenia, działania w trzech wymiarach, odwracanie uwagi
    * _Opis_: Zofia specjalizuje się w kradzieży rzeczy mających funkcje inkryminacji - lub w podkładaniu tego typu rzeczy. Czasem bezczelnie zwinie przedmiot, wskakuje na motor i ucieka. Czasem włamuje się przez manewrowanie wszystkimi trzema wymiarami, czasem przez prostą kradzież kieszonkową i odwracanie uwagi...
* **Energetyk**:
    * _Aspekty_: elementalne źródła energii
    * _Opis_: Konstruktorka i osoba utrzymująca zasilanie "Dumnego Orła". Twórczyni wielu agregatów energii elementalnej i ich konwerter w energię magiczną. Wie jak to działa i wie jak to złożyć do kupy.
    
### Silne i słabe strony:

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

## Magia

### Szkoły magiczne

* **Magia elementalna**:
    * _Aspekty_: wszystko z dużymi prędkościami, błyskawice i przepięcia, szybkie latanie, konwersja elementów w magię, guardian winds
    * _Opis_: Przede wszystkim czarodziejka powietrza, ale też skupia się na żywiole ognia i konwersji elementów w energię magiczną. Magicznie, elementalna moc wzmacnia jej naturalne predylekcje co do prędkości i rozwalania technologii błyskawicami - ale dodatkowo Zofia skupia się na konwersji energii elementalnej w energię magiczną.
* **Kataliza**: 
    * _Aspekty_: konwersja elementów w magię, puryfikacja energii, bateria dla artefaktów
    * _Opis_: Jakkolwiek dziwnie to zabrzmi, Zofia szczególnie specjalizuje się w wykorzystywaniu różnych form energii magicznych by budować spójne źródła energii na bazie ujednoliconej energii i potem ładować artefakty / inne byty tego typu. Z tego jest znana jako katalistka - i dlatego jest więcej niż tolerowana. Potrafi opanować nawet bardzo żywiołową energię.
* **Magia zmysłów**:
    * _Aspekty_: efektowne wizualizacje, niewidzialność, discord and chaos
    * _Opis_: Zofia lubi tworzyć wizualizacje i efekty specjalne jako hobby. Dodatkowo wykorzystuje magię zmysłów by skuteczniej okradać ludzi czy w pracy kuriera. No i do siania chaosu - Zmysły + Kataliza dają jej tu bardzo duże możliwości.

### Zaklęcia statyczne

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 

## Otoczenie

### Powiązane frakcje

* Ptasi Trójkąt
* Rodzina Dukata

### Zarobki

* czasami konsultantka do spraw stabilizacji energii magicznej / magitrowni
* czasami kurier materiałów niebezpiecznych / time-dependent
* czasami iluzjonistka
* czasami wyścigi różnego rodzaju

### Znam

* **Powiązana z Dare Shiver**:
    * _Aspekty_: 
    * _Opis_: 
* **Gildia Ptasi Trójkąt**:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 


### Mam

* **Dostęp do "Dumnego Orła"**:
    * _Aspekty_: 
    * _Opis_: Statek powietrzny Ptasiego Trójkąta; zbudowany przez min. jej ojca i w którym ona zapewnia prawidłowe zasilanie. 
* **Pojazdy wyścigowe**:
    * _Aspekty_: zabezpieczenia przed zderzeniami, bardzo szybkie, zdolne do latania, delikatne i niewidzialne
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 


# Opis

TODO

### Koncept

Katalistka Dukata. Znajoma (uważana za przyjaciółkę) Roberta Sądecznego. Kocha powietrze, latanie i uczucie bycia ciągle w powietrzu - i szybkość. Dość arogancka i skupiona na sobie. Nie lubi kontaktu fizycznego - poza tym, kojarzy mi się z piosenką "Nymphomania" Inkubus Sukkubus (druga zwrotka). Nieopanowana i nieokiełznana. Należy do Ptasiego Trójkąta i jest głównym zasilaniem ich airshipu. Też: Apollyon (For Honor), Eris (Greckie mity). Wektor chaosu.

Jej rolą jest wprowadzenie nieodpowiedzialnego, nieoznaczonego glass cannon o ogromnej mocy - osoby której nie da się sensownie opanować. Osoby, która aktywnie dąży do destabilizacji. Ale i osoby, która pomoże jednostce - licząc się z krzywdą wielu.

### Motto

"Robisz to za wolno i bez sensu"

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|171031|wpierw pomogła Myszeczce znaleźć harmoniczne sygnały które uszkodzą lekko portalisko a potem z narażeniem życia ratowała ludzi na portalisku. Paradoks zmienił ją w świnię.|[Utracona kontrola](/rpg/inwazja/opowiesci/konspekty/171031-utracona-kontrola.html)|11/09/24|11/09/27|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171004|katalistka Dukata; bliska przyjaciółka Sądecznego. Skupia się na dekontaminacji energii magicznej. Antagonizuje Świecę na tym terenie - bo nic nie mogą zrobić.|[Niestabilna magitrownia](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|11/09/01|11/09/03|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Tomasz Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-tomasz-myszeczka.html)|2|[171031](/rpg/inwazja/opowiesci/konspekty/171031-utracona-kontrola.html), [171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Robert Sądeczny](/rpg/inwazja/opowiesci/karty-postaci/1709-robert-sadeczny.html)|2|[171031](/rpg/inwazja/opowiesci/konspekty/171031-utracona-kontrola.html), [171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|2|[171031](/rpg/inwazja/opowiesci/konspekty/171031-utracona-kontrola.html), [171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|2|[171031](/rpg/inwazja/opowiesci/konspekty/171031-utracona-kontrola.html), [171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Daniel Akwitański](/rpg/inwazja/opowiesci/karty-postaci/1709-daniel-akwitanski.html)|2|[171031](/rpg/inwazja/opowiesci/konspekty/171031-utracona-kontrola.html), [171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Tamara Muszkiet](/rpg/inwazja/opowiesci/karty-postaci/1709-tamara-muszkiet.html)|1|[171031](/rpg/inwazja/opowiesci/konspekty/171031-utracona-kontrola.html)|
|[Sylwester Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-sylwester-bankierz.html)|1|[171031](/rpg/inwazja/opowiesci/konspekty/171031-utracona-kontrola.html)|
|[Marcin Warinsky](/rpg/inwazja/opowiesci/karty-postaci/1709-marcin-warinsky.html)|1|[171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Karol Marzyciel](/rpg/inwazja/opowiesci/karty-postaci/1709-karol-marzyciel.html)|1|[171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Jakub Dobrocień](/rpg/inwazja/opowiesci/karty-postaci/1709-jakub-dobrocien.html)|1|[171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Grzegorz Nadziejak](/rpg/inwazja/opowiesci/karty-postaci/9999-grzegorz-nadziejak.html)|1|[171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Efraim Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-efraim-weiner.html)|1|[171031](/rpg/inwazja/opowiesci/konspekty/171031-utracona-kontrola.html)|
|[Efemeryda Senesgradzka](/rpg/inwazja/opowiesci/karty-postaci/9999-efemeryda-senesgradzka.html)|1|[171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Bogumił Miłoszept](/rpg/inwazja/opowiesci/karty-postaci/9999-bogumil-miloszept.html)|1|[171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|

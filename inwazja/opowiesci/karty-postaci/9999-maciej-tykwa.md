---
layout: inwazja-karta-postaci
categories: profile
title: "Maciej Tykwa"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|150823|kucharz i silny młody * człowiek (33), który pod wpływem * magii dominacji spróbował zgwałcić Estrellę.|[Atak na sanktuarium Estrelli](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|10/05/23|10/05/24|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Roman Gieroj](/rpg/inwazja/opowiesci/karty-postaci/9999-roman-gieroj.html)|1|[150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|1|[150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|
|[Olga Miodownik](/rpg/inwazja/opowiesci/karty-postaci/1709-olga-miodownik.html)|1|[150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|
|[Netheria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-netheria-diakon.html)|1|[150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|
|[Mateusz Tykwa](/rpg/inwazja/opowiesci/karty-postaci/9999-mateusz-tykwa.html)|1|[150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|
|[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/1709-maria-newa.html)|1|[150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|
|[Kermit Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-kermit-diakon.html)|1|[150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|
|[Katarzyna Kotek](/rpg/inwazja/opowiesci/karty-postaci/9999-katarzyna-kotek.html)|1|[150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|
|[Iliusitius](/rpg/inwazja/opowiesci/karty-postaci/9999-iliusitius.html)|1|[150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|
|[Grzegorz Śliwa](/rpg/inwazja/opowiesci/karty-postaci/9999-grzegorz-sliwa.html)|1|[150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|
|[Estrella Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-estrella-diakon.html)|1|[150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|
|[Adrian Kropiak](/rpg/inwazja/opowiesci/karty-postaci/9999-adrian-kropiak.html)|1|[150823](/rpg/inwazja/opowiesci/konspekty/150823-atak-na-sanktuarium-estrelli.html)|

---
layout: inwazja-karta-postaci
categories: profile
title: "Felicjan Weiner"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|161113|rezydent Świecy. Teraz Andrea nie ma wątpliwości - agent Drugiej Strony. Najpewniej wysoko postawiony w Czeliminie.|[Świeca nie zostawia swoich](/rpg/inwazja/opowiesci/konspekty/161113-swieca-nie-zostawia-swoich.html)|10/06/29|10/07/01|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160406|główny, eksperymentujący * mag w Czeliminie. Typowy Weiner z typowymi nieodpowiedzialnymi eksperymentami. W zasadzie niegroźny.|[Najprawdziwszy sojusz Blakenbauerów](/rpg/inwazja/opowiesci/konspekty/160406-najprawdziwszy-sojusz-blakenbauerow.html)|10/06/29|10/06/30|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161101|przybył by przejąć kontrolę nad Portalem i połączyć go z EAM... i skończył poraniony i poparzony.|[Bezwzględna Lady Terminus](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|10/06/25|10/06/28|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160411|"ostatni" ekspert Spustoszenia któremu ufa Ozydiusz.|[Sekret śmierci na wykopaliskach](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|10/06/14|10/06/15|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Andrea Wilgacz](/rpg/inwazja/opowiesci/karty-postaci/1709-andrea-wilgacz.html)|3|[161113](/rpg/inwazja/opowiesci/konspekty/161113-swieca-nie-zostawia-swoich.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html), [160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|[Tadeusz Baran](/rpg/inwazja/opowiesci/karty-postaci/1709-tadeusz-baran.html)|2|[161113](/rpg/inwazja/opowiesci/konspekty/161113-swieca-nie-zostawia-swoich.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|[Rudolf Jankowski](/rpg/inwazja/opowiesci/karty-postaci/9999-rudolf-jankowski.html)|2|[161113](/rpg/inwazja/opowiesci/konspekty/161113-swieca-nie-zostawia-swoich.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|[Rafael Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-rafael-diakon.html)|2|[161113](/rpg/inwazja/opowiesci/konspekty/161113-swieca-nie-zostawia-swoich.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|[Mieszko Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-mieszko-bankierz.html)|2|[161113](/rpg/inwazja/opowiesci/konspekty/161113-swieca-nie-zostawia-swoich.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|[Marian Łajdak](/rpg/inwazja/opowiesci/karty-postaci/1709-marian-lajdak.html)|2|[161113](/rpg/inwazja/opowiesci/konspekty/161113-swieca-nie-zostawia-swoich.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|[Kajetan Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-kajetan-weiner.html)|2|[161113](/rpg/inwazja/opowiesci/konspekty/161113-swieca-nie-zostawia-swoich.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|[Julian Pszczelak](/rpg/inwazja/opowiesci/karty-postaci/1709-julian-pszczelak.html)|2|[161113](/rpg/inwazja/opowiesci/konspekty/161113-swieca-nie-zostawia-swoich.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|[Andżelika Leszczyńska](/rpg/inwazja/opowiesci/karty-postaci/1709-andzelika-leszczynska.html)|2|[161113](/rpg/inwazja/opowiesci/konspekty/161113-swieca-nie-zostawia-swoich.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|[Zuzanna Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-zuzanna-maus.html)|1|[160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|[Wojmił Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-wojmil-bankierz.html)|1|[160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|1|[161113](/rpg/inwazja/opowiesci/konspekty/161113-swieca-nie-zostawia-swoich.html)|
|[Ozydiusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-ozydiusz-bankierz.html)|1|[160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|[Olga Miodownik](/rpg/inwazja/opowiesci/karty-postaci/1709-olga-miodownik.html)|1|[160406](/rpg/inwazja/opowiesci/konspekty/160406-najprawdziwszy-sojusz-blakenbauerow.html)|
|[Oktawian Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-oktawian-maus.html)|1|[160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|[Marian Agrest](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-agrest.html)|1|[161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|[Lidia Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-lidia-weiner.html)|1|[161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|[Leonidas Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-leonidas-blakenbauer.html)|1|[160406](/rpg/inwazja/opowiesci/konspekty/160406-najprawdziwszy-sojusz-blakenbauerow.html)|
|[Krzysztof Wieczorek](/rpg/inwazja/opowiesci/karty-postaci/9999-krzysztof-wieczorek.html)|1|[160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|[Klara Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-klara-blakenbauer.html)|1|[160406](/rpg/inwazja/opowiesci/konspekty/160406-najprawdziwszy-sojusz-blakenbauerow.html)|
|[Jolanta Sowińska](/rpg/inwazja/opowiesci/karty-postaci/1709-jolanta-sowinska.html)|1|[160406](/rpg/inwazja/opowiesci/konspekty/160406-najprawdziwszy-sojusz-blakenbauerow.html)|
|[Joachim Kopiec](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-kopiec.html)|1|[161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|[Jerzy Gurlacz](/rpg/inwazja/opowiesci/karty-postaci/9999-jerzy-gurlacz.html)|1|[160406](/rpg/inwazja/opowiesci/konspekty/160406-najprawdziwszy-sojusz-blakenbauerow.html)|
|[Jan Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-weiner.html)|1|[160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|1|[160406](/rpg/inwazja/opowiesci/konspekty/160406-najprawdziwszy-sojusz-blakenbauerow.html)|
|[Halina Krzyżanowska](/rpg/inwazja/opowiesci/karty-postaci/9999-halina-krzyzanowska.html)|1|[161113](/rpg/inwazja/opowiesci/konspekty/161113-swieca-nie-zostawia-swoich.html)|
|[Ernest Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-ernest-maus.html)|1|[160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|[Elizawieta Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-elizawieta-zajcew.html)|1|[160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|[Dominik Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-dominik-bankierz.html)|1|[160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|[Dionizy Kret](/rpg/inwazja/opowiesci/karty-postaci/1709-dionizy-kret.html)|1|[160406](/rpg/inwazja/opowiesci/konspekty/160406-najprawdziwszy-sojusz-blakenbauerow.html)|
|[Bianka Stein](/rpg/inwazja/opowiesci/karty-postaci/1709-bianka-stein.html)|1|[160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|[Artur Janczecki](/rpg/inwazja/opowiesci/karty-postaci/9999-artur-janczecki.html)|1|[160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|[Anna Kozak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kozak.html)|1|[161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|[Aneta Rainer](/rpg/inwazja/opowiesci/karty-postaci/1709-aneta-rainer.html)|1|[160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|
|[Alojzy Przylaz](/rpg/inwazja/opowiesci/karty-postaci/1803-alojzy-przylaz.html)|1|[161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|[Alina Bednarz](/rpg/inwazja/opowiesci/karty-postaci/1803-alina-bednarz.html)|1|[160406](/rpg/inwazja/opowiesci/konspekty/160406-najprawdziwszy-sojusz-blakenbauerow.html)|
|[Aleksander Sowiński](/rpg/inwazja/opowiesci/karty-postaci/9999-aleksander-sowinski.html)|1|[160411](/rpg/inwazja/opowiesci/konspekty/160411-sekret-smierci-na-wykopaliskach.html)|

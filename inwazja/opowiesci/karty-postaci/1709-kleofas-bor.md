---
layout: inwazja-karta-postaci
categories: profile
factions: "Siły Specjalne Hektora Blakenbauera"
type: "NPC"
title: "Kleofas Bór"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **FIL: Tak trzeba**: 
    * _Aspekty_: charyzmatyczny, bardzo opanowany, etyka ponad prawem
    * _Opis_: 
* **FIL: bardzo racjonalny**:
    * _Aspekty_: 
    * _Opis_: 
* **MET: **:
    * _Aspekty_: 
    * _Opis_: 
* **MRZ: udowodnić swoją wartość**:
    * _Aspekty_: niewłaściwy typ inicjatywy, optymista
    * _Opis_: 
	
### Umiejętności

* **Oficer**:
    * _Aspekty_: były podoficer sił specjalnych, taktyk, zarządzanie ludźmi, świetny kłamca
    * _Opis_: 
* **Zaopatrzeniowiec**: 
    * _Aspekty_: załatwi każdy sprzęt
    * _Opis_: 
    
### Silne i słabe strony:

* ****:
    * _Aspekty_: przystojny
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

## Magia

**brak**

## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Zarobki

* ?

### Znam

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

### Mam

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

# Opis

Kleofas jest dobrym duchem i przełożonym grupy specjalnej Hektora. To co zabawne - on FAKTYCZNIE był podoficerem sił specjalnych, ale wyleciał. Za niewłaściwy typ inicjatywy i robienie tego co się powinno robić, nie tego co miał zrobić. Kiepski materiał na podoficera, ale bardzo dobry na twórcę i przełożonego sił specjalnych prokuratury.
Kleofas chroni Hektora i stara się uprzedzać jego potrzeby i pomysły. To, co najbardziej zabawne w tym wszystkim - Kleofas nie wie i nawet nie PODEJRZEWA magii o istnienie. Dlatego to on dowodzi grupą specjalną ;-).

### Koncept

Wysokopoziomowy koncept postaci, 1-2 zdania.

### Motto

"Tekst"

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|160615|który uratował Hektora przed katastrofą rażąc go paralizatorem, po czym wszedł do Prokuratury.|[Morderczyni w masce Wandy](/rpg/inwazja/opowiesci/konspekty/160615-morderczyni-w-masce-wandy.html)|10/06/25|10/06/26|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160309|który wiedząc o ataku na dzieci Patrycji poprosił Hektora o NIE autoryzowaniu akcji Anny; uważa, że to wszystko jest pułapką.|[Irytka Sprzężona](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|10/06/25|10/06/26|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160303|* człowiek, który wykonywał zadania Margaret w świecie potworów. Wykazał hart ducha mimo koszmarów z jakimi miał do czynienia (Zeta).|[Otton zabija Zetę](/rpg/inwazja/opowiesci/konspekty/160303-otton-zabija-zete.html)|10/06/23|10/06/24|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160506|w ramach sił specjalnych zbiera Anioły ("obiekty sztuki") z ulicy i je składuje. Ostrzegł Hektora o oskarżeniach Wandy Ketran.|[Wyścig pająka z terminuską](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|10/06/22|10/06/24|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160210|dzielnie szarżujący na * magazyn i dzielnie z niego uciekający przed wybuchem bomby.|[Batmag Uderza!](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html)|10/06/04|10/06/05|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160128|zawsze dziarski, rześki i gotowy - zwłaszcza do budzenia innych ludzi na życzenie Hektora.|[Byli sobie przestępcy](/rpg/inwazja/opowiesci/konspekty/160128-byli-sobie-przestepcy.html)|10/06/02|10/06/03|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150722|który bez kłopotu wbił się w linię przekazywania dzieci dzięki świetnym umiejętnościom kłamania.|[Reverse kidnapping z Krupnioka](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html)|10/05/15|10/05/16|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150325|szef grupy specjalnej Hektora, który nie dał się sprowokować aptoformowi... choć jego słabym punktem jest jego brat.|[Morderstwo jak w książce](/rpg/inwazja/opowiesci/konspekty/150325-morderstwo-jak-w-ksiazce.html)|10/04/29|10/04/30|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170412|"oportunistyczny szczur, ale z dobrym sercem", który wszystkich rozegrał by pomóc dziewczynie wyjść na prostą i mieć szansę w życiu. Ma bogatego kuzyna.|[Przed teatrem absurdu](/rpg/inwazja/opowiesci/konspekty/170412-przed-teatrem-absurdu.html)|10/03/10|10/03/12|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170315|zgłosił Hektorowi problem z policjantką / raperem, potem odpowiednio rozdzielał odpowiedzialności zespołu, by się nie narobić ;-).|[Naszyjnik Przenośnych Wspomnień](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html)|10/03/02|10/03/03|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|141022|były szef sił specjalnych Hektora, którego szczęście (i życie) zawiodło (KIA)|[Po wymianie strzałów...](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|10/01/09|10/01/10|[Blakenbauerowie x Skorpion](/rpg/inwazja/opowiesci/konspekty/kampania-blakenbauerowie-x-skorpion.html)|
|141009|dowódca sił specjalnych Hektora, który wyraźnie gra na dwóch frontach|[Jad w prokuraturze](/rpg/inwazja/opowiesci/konspekty/141009-jad-w-prokuraturze.html)|10/01/07|10/01/08|[Blakenbauerowie x Skorpion](/rpg/inwazja/opowiesci/konspekty/kampania-blakenbauerowie-x-skorpion.html)|
|150304|dowódca sił specjalnych Hektora, który jednocześnie osłania Annę Kajak i potrafi oprzeć się Inkwizytorowi Hektorowi i kłamie jak tylko Kleofas umie. |[Ani słowa prawdy...](/rpg/inwazja/opowiesci/konspekty/150304-ani-slowa-prawdy.html)|10/01/03|10/01/04|[Blakenbauerowie x Skorpion](/rpg/inwazja/opowiesci/konspekty/kampania-blakenbauerowie-x-skorpion.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|12|[160615](/rpg/inwazja/opowiesci/konspekty/160615-morderczyni-w-masce-wandy.html), [160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html), [160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html), [160128](/rpg/inwazja/opowiesci/konspekty/160128-byli-sobie-przestepcy.html), [150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html), [150325](/rpg/inwazja/opowiesci/konspekty/150325-morderstwo-jak-w-ksiazce.html), [170412](/rpg/inwazja/opowiesci/konspekty/170412-przed-teatrem-absurdu.html), [170315](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html), [141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html), [141009](/rpg/inwazja/opowiesci/konspekty/141009-jad-w-prokuraturze.html), [150304](/rpg/inwazja/opowiesci/konspekty/150304-ani-slowa-prawdy.html)|
|[Alina Bednarz](/rpg/inwazja/opowiesci/karty-postaci/1803-alina-bednarz.html)|9|[160615](/rpg/inwazja/opowiesci/konspekty/160615-morderczyni-w-masce-wandy.html), [160303](/rpg/inwazja/opowiesci/konspekty/160303-otton-zabija-zete.html), [160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html), [160128](/rpg/inwazja/opowiesci/konspekty/160128-byli-sobie-przestepcy.html), [170412](/rpg/inwazja/opowiesci/konspekty/170412-przed-teatrem-absurdu.html), [170315](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html), [141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html), [141009](/rpg/inwazja/opowiesci/konspekty/141009-jad-w-prokuraturze.html), [150304](/rpg/inwazja/opowiesci/konspekty/150304-ani-slowa-prawdy.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|8|[160615](/rpg/inwazja/opowiesci/konspekty/160615-morderczyni-w-masce-wandy.html), [160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [160128](/rpg/inwazja/opowiesci/konspekty/160128-byli-sobie-przestepcy.html), [150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html), [150325](/rpg/inwazja/opowiesci/konspekty/150325-morderstwo-jak-w-ksiazce.html), [141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html), [141009](/rpg/inwazja/opowiesci/konspekty/141009-jad-w-prokuraturze.html), [150304](/rpg/inwazja/opowiesci/konspekty/150304-ani-slowa-prawdy.html)|
|[Margaret Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-margaret-blakenbauer.html)|7|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [160303](/rpg/inwazja/opowiesci/konspekty/160303-otton-zabija-zete.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html), [150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html), [150325](/rpg/inwazja/opowiesci/konspekty/150325-morderstwo-jak-w-ksiazce.html), [141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html), [141009](/rpg/inwazja/opowiesci/konspekty/141009-jad-w-prokuraturze.html)|
|[Patrycja Krowiowska](/rpg/inwazja/opowiesci/karty-postaci/1709-patrycja-krowiowska.html)|6|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html), [150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html), [150325](/rpg/inwazja/opowiesci/konspekty/150325-morderstwo-jak-w-ksiazce.html), [170412](/rpg/inwazja/opowiesci/konspekty/170412-przed-teatrem-absurdu.html), [170315](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html)|
|[Dionizy Kret](/rpg/inwazja/opowiesci/karty-postaci/1709-dionizy-kret.html)|5|[160615](/rpg/inwazja/opowiesci/konspekty/160615-morderczyni-w-masce-wandy.html), [160303](/rpg/inwazja/opowiesci/konspekty/160303-otton-zabija-zete.html), [150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html), [170412](/rpg/inwazja/opowiesci/konspekty/170412-przed-teatrem-absurdu.html), [170315](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html)|
|[Olga Miodownik](/rpg/inwazja/opowiesci/karty-postaci/1709-olga-miodownik.html)|4|[160303](/rpg/inwazja/opowiesci/konspekty/160303-otton-zabija-zete.html), [150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html), [150325](/rpg/inwazja/opowiesci/konspekty/150325-morderstwo-jak-w-ksiazce.html), [150304](/rpg/inwazja/opowiesci/konspekty/150304-ani-slowa-prawdy.html)|
|[Klemens X](/rpg/inwazja/opowiesci/karty-postaci/9999-klemens-x.html)|4|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html), [150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html), [141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html), [141009](/rpg/inwazja/opowiesci/konspekty/141009-jad-w-prokuraturze.html)|
|[Klara Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-klara-blakenbauer.html)|4|[160615](/rpg/inwazja/opowiesci/konspekty/160615-morderczyni-w-masce-wandy.html), [160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html), [160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html)|
|[Anna Kajak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kajak.html)|4|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html), [141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html), [150304](/rpg/inwazja/opowiesci/konspekty/150304-ani-slowa-prawdy.html)|
|[Otton Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-otton-blakenbauer.html)|3|[160615](/rpg/inwazja/opowiesci/konspekty/160615-morderczyni-w-masce-wandy.html), [160303](/rpg/inwazja/opowiesci/konspekty/160303-otton-zabija-zete.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Leonidas Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-leonidas-blakenbauer.html)|3|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html), [160128](/rpg/inwazja/opowiesci/konspekty/160128-byli-sobie-przestepcy.html)|
|[Kinga Melit](/rpg/inwazja/opowiesci/karty-postaci/9999-kinga-melit.html)|3|[160615](/rpg/inwazja/opowiesci/konspekty/160615-morderczyni-w-masce-wandy.html), [160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [160128](/rpg/inwazja/opowiesci/konspekty/160128-byli-sobie-przestepcy.html)|
|[Artur Bryś](/rpg/inwazja/opowiesci/karty-postaci/1709-artur-brys.html)|3|[150325](/rpg/inwazja/opowiesci/konspekty/150325-morderstwo-jak-w-ksiazce.html), [170412](/rpg/inwazja/opowiesci/konspekty/170412-przed-teatrem-absurdu.html), [170315](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html)|
|[Wanda Ketran](/rpg/inwazja/opowiesci/karty-postaci/1709-wanda-ketran.html)|2|[160615](/rpg/inwazja/opowiesci/konspekty/160615-morderczyni-w-masce-wandy.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Szymon Skubny](/rpg/inwazja/opowiesci/karty-postaci/9999-szymon-skubny.html)|2|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html), [160128](/rpg/inwazja/opowiesci/konspekty/160128-byli-sobie-przestepcy.html)|
|[Pafnucy Zieczar](/rpg/inwazja/opowiesci/karty-postaci/9999-pafnucy-zieczar.html)|2|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html), [160128](/rpg/inwazja/opowiesci/konspekty/160128-byli-sobie-przestepcy.html)|
|[Marcelin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-marcelin-blakenbauer.html)|2|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html), [150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html)|
|[Emilia Kołatka](/rpg/inwazja/opowiesci/karty-postaci/1709-emilia-kolatka.html)|2|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Elea Maus](/rpg/inwazja/opowiesci/karty-postaci/1802-elea-maus.html)|2|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Diana Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-diana-weiner.html)|2|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [160128](/rpg/inwazja/opowiesci/konspekty/160128-byli-sobie-przestepcy.html)|
|[Dagmara Wyjątek](/rpg/inwazja/opowiesci/karty-postaci/1709-dagmara-wyjątek.html)|2|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html), [160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Bogdan Kimaroj](/rpg/inwazja/opowiesci/karty-postaci/9999-bogdan-kimaroj.html)|2|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html), [160128](/rpg/inwazja/opowiesci/konspekty/160128-byli-sobie-przestepcy.html)|
|[Arkadiusz Klusiński](/rpg/inwazja/opowiesci/karty-postaci/9999-arkadiusz-klusinski.html)|2|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html), [160128](/rpg/inwazja/opowiesci/konspekty/160128-byli-sobie-przestepcy.html)|
|[oddział Zeta](/rpg/inwazja/opowiesci/karty-postaci/9999-oddzial-zeta.html)|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|[Wojciech Popolin](/rpg/inwazja/opowiesci/karty-postaci/9999-wojciech-popolin.html)|1|[170315](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html)|
|[Wiktor Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1709-wiktor-sowinski.html)|1|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Tymoteusz Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-tymoteusz-maus.html)|1|[150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html)|
|[Tymoteusz Dzionek](/rpg/inwazja/opowiesci/karty-postaci/9999-tymoteusz-dzionek.html)|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|[Tomasz Jamnik](/rpg/inwazja/opowiesci/karty-postaci/9999-tomasz-jamnik.html)|1|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html)|
|[Szczepan Paczoł](/rpg/inwazja/opowiesci/karty-postaci/1709-szczepan-paczol.html)|1|[150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html)|
|[Stanisław Pormien](/rpg/inwazja/opowiesci/karty-postaci/9999-stanislaw-pormien.html)|1|[170315](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html)|
|[Rufus Czubek](/rpg/inwazja/opowiesci/karty-postaci/9999-rufus-czubek.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Romuald Zeta](/rpg/inwazja/opowiesci/karty-postaci/9999-romuald-zeta.html)|1|[160303](/rpg/inwazja/opowiesci/konspekty/160303-otton-zabija-zete.html)|
|[Roman Brunowicz](/rpg/inwazja/opowiesci/karty-postaci/9999-roman-brunowicz.html)|1|[170412](/rpg/inwazja/opowiesci/konspekty/170412-przed-teatrem-absurdu.html)|
|[Robert Pomocnik](/rpg/inwazja/opowiesci/karty-postaci/9999-robert-pomocnik.html)|1|[170315](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html)|
|[Paulina Widoczek](/rpg/inwazja/opowiesci/karty-postaci/9999-paulina-widoczek.html)|1|[170412](/rpg/inwazja/opowiesci/konspekty/170412-przed-teatrem-absurdu.html)|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|1|[150325](/rpg/inwazja/opowiesci/konspekty/150325-morderstwo-jak-w-ksiazce.html)|
|[Ozydiusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-ozydiusz-bankierz.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Oktawian Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-oktawian-maus.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Mordecja Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-mordecja-diakon.html)|1|[150304](/rpg/inwazja/opowiesci/konspekty/150304-ani-slowa-prawdy.html)|
|[Mojra](/rpg/inwazja/opowiesci/karty-postaci/9999-mojra.html)|1|[141009](/rpg/inwazja/opowiesci/konspekty/141009-jad-w-prokuraturze.html)|
|[Marta Newa](/rpg/inwazja/opowiesci/karty-postaci/9999-marta-newa.html)|1|[160303](/rpg/inwazja/opowiesci/konspekty/160303-otton-zabija-zete.html)|
|[Marian Kozior](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-kozior.html)|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|[Marian Agrest](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-agrest.html)|1|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/1709-maria-newa.html)|1|[150325](/rpg/inwazja/opowiesci/konspekty/150325-morderstwo-jak-w-ksiazce.html)|
|[Luiza Wanta](/rpg/inwazja/opowiesci/karty-postaci/9999-luiza-wanta.html)|1|[170412](/rpg/inwazja/opowiesci/konspekty/170412-przed-teatrem-absurdu.html)|
|[Krzysztof Kruczolis](/rpg/inwazja/opowiesci/karty-postaci/9999-krzysztof-kruczolis.html)|1|[170412](/rpg/inwazja/opowiesci/konspekty/170412-przed-teatrem-absurdu.html)|
|[Katarzyna Leśniczek](/rpg/inwazja/opowiesci/karty-postaci/9999-katarzyna-lesniczek.html)|1|[170315](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html)|
|[Karolina Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-karolina-maus.html)|1|[160615](/rpg/inwazja/opowiesci/konspekty/160615-morderczyni-w-masce-wandy.html)|
|[Karol Poczciwiec](/rpg/inwazja/opowiesci/karty-postaci/9999-karol-poczciwiec.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Karol Kiśnia](/rpg/inwazja/opowiesci/karty-postaci/9999-karol-kisnia.html)|1|[150304](/rpg/inwazja/opowiesci/konspekty/150304-ani-slowa-prawdy.html)|
|[Kamila Zeta](/rpg/inwazja/opowiesci/karty-postaci/9999-kamila-zeta.html)|1|[160303](/rpg/inwazja/opowiesci/konspekty/160303-otton-zabija-zete.html)|
|[Jędrzej Zeta](/rpg/inwazja/opowiesci/karty-postaci/9999-jedrzej-zeta.html)|1|[160303](/rpg/inwazja/opowiesci/konspekty/160303-otton-zabija-zete.html)|
|[Judyta Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-judyta-maus.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Joachim Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-zajcew.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Janina Strych](/rpg/inwazja/opowiesci/karty-postaci/9999-janina-strych.html)|1|[150304](/rpg/inwazja/opowiesci/konspekty/150304-ani-slowa-prawdy.html)|
|[Jan Fiołek](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-fiolek.html)|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|[Igor Daczyn](/rpg/inwazja/opowiesci/karty-postaci/9999-igor-daczyn.html)|1|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html)|
|[Hubert Rębski](/rpg/inwazja/opowiesci/karty-postaci/9999-hubert-rebski.html)|1|[160128](/rpg/inwazja/opowiesci/konspekty/160128-byli-sobie-przestepcy.html)|
|[Henryk Waciak](/rpg/inwazja/opowiesci/karty-postaci/9999-henryk-waciak.html)|1|[141009](/rpg/inwazja/opowiesci/konspekty/141009-jad-w-prokuraturze.html)|
|[Gala Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-gala-zajcew.html)|1|[150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html)|
|[Ewelina Nadzieja](/rpg/inwazja/opowiesci/karty-postaci/9999-ewelina-nadzieja.html)|1|[170315](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html)|
|[Estera Piryt](/rpg/inwazja/opowiesci/karty-postaci/9999-estera-piryt.html)|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|[Elizawieta Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-elizawieta-zajcew.html)|1|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Dorota Gacek](/rpg/inwazja/opowiesci/karty-postaci/1709-dorota-gacek.html)|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|[Cyprian Koziej](/rpg/inwazja/opowiesci/karty-postaci/9999-cyprian-koziej.html)|1|[150325](/rpg/inwazja/opowiesci/konspekty/150325-morderstwo-jak-w-ksiazce.html)|
|[Cezary Piryt](/rpg/inwazja/opowiesci/karty-postaci/9999-cezary-piryt.html)|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|[Bolesław Derwisz](/rpg/inwazja/opowiesci/karty-postaci/9999-boleslaw-derwisz.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Beata Solniczka](/rpg/inwazja/opowiesci/karty-postaci/9999-beata-solniczka.html)|1|[170315](/rpg/inwazja/opowiesci/konspekty/170315-naszyjnik-wspomnien.html)|
|[Arazille](/rpg/inwazja/opowiesci/karty-postaci/9999-arazille.html)|1|[160303](/rpg/inwazja/opowiesci/konspekty/160303-otton-zabija-zete.html)|
|[Aptoform Mirasilaler](/rpg/inwazja/opowiesci/karty-postaci/9999-aptoform-mirasilaler.html)|1|[150325](/rpg/inwazja/opowiesci/konspekty/150325-morderstwo-jak-w-ksiazce.html)|
|[Anna Kozak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kozak.html)|1|[160210](/rpg/inwazja/opowiesci/konspekty/160210-batmag-uderza.html)|
|[Aneta Rainer](/rpg/inwazja/opowiesci/karty-postaci/1709-aneta-rainer.html)|1|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Andrzej Chezyr](/rpg/inwazja/opowiesci/karty-postaci/9999-andrzej-chezyr.html)|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|
|[Amanda Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-amanda-diakon.html)|1|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Albert Pireus](/rpg/inwazja/opowiesci/karty-postaci/9999-albert-pireus.html)|1|[150304](/rpg/inwazja/opowiesci/konspekty/150304-ani-slowa-prawdy.html)|
|[Adrian Murarz](/rpg/inwazja/opowiesci/karty-postaci/9999-adrian-murarz.html)|1|[160506](/rpg/inwazja/opowiesci/konspekty/160506-wyscig-pajaka-z-terminuska.html)|
|[Adela Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-adela-maus.html)|1|[150722](/rpg/inwazja/opowiesci/konspekty/150722-reverse-kidnapping-krupniok.html)|
|[Adam Wąż](/rpg/inwazja/opowiesci/karty-postaci/9999-adam-waz.html)|1|[141022](/rpg/inwazja/opowiesci/konspekty/141022-po-wymianie-strzalow.html)|

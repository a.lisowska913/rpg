---
layout: inwazja-karta-postaci
categories: profile
title: "Feliks Bozur"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|150104|który być może stał się pułapką magów z uwagi na zakazane słowa których szukał w internecie.|[Terminus-defiler, kapłan Arazille](/rpg/inwazja/opowiesci/konspekty/150104-terminus-defiler-kaplan-arazille.html)|10/02/17|10/02/18|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|
|150103|student przynoszący Andromedzie zupełnie inne informacje o Arazille i Iliusitiusie niż słyszała wcześniej od magów.|[Pryzmat Myśli pęka](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html)|10/02/15|10/02/16|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|
|141230|student wyszukujący dla Andromedy niebezpieczne hasła o demon lordach w Googlu (i innych źródłach).|[Ofiara z wampira dla Arazille](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html)|10/02/13|10/02/14|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|
|150105|student który albo zginie albo Kasia zginie. Decyzja jest po jej stronie.|[Dar Iliusitiusa dla Andromedy](/rpg/inwazja/opowiesci/konspekty/150105-dar-iliusitiusa-dla-andromedy.html)|10/02/05|10/02/06|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|
|130511|student znający się na dziwnych religiach i symbolach i umiejący używać Google.|[Ołtarz Podniesionej Dłoni](/rpg/inwazja/opowiesci/konspekty/130511-oltarz-podniesionej-dloni.html)|10/02/03|10/02/04|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Sandra Stryjek](/rpg/inwazja/opowiesci/karty-postaci/1709-sandra-stryjek.html)|5|[150104](/rpg/inwazja/opowiesci/konspekty/150104-terminus-defiler-kaplan-arazille.html), [150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [150105](/rpg/inwazja/opowiesci/konspekty/150105-dar-iliusitiusa-dla-andromedy.html), [130511](/rpg/inwazja/opowiesci/konspekty/130511-oltarz-podniesionej-dloni.html)|
|[Kasia Nowak](/rpg/inwazja/opowiesci/karty-postaci/1803-kasia-nowak.html)|5|[150104](/rpg/inwazja/opowiesci/konspekty/150104-terminus-defiler-kaplan-arazille.html), [150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [150105](/rpg/inwazja/opowiesci/konspekty/150105-dar-iliusitiusa-dla-andromedy.html), [130511](/rpg/inwazja/opowiesci/konspekty/130511-oltarz-podniesionej-dloni.html)|
|[Herbert Zioło](/rpg/inwazja/opowiesci/karty-postaci/1709-herbert-ziolo.html)|5|[150104](/rpg/inwazja/opowiesci/konspekty/150104-terminus-defiler-kaplan-arazille.html), [150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [150105](/rpg/inwazja/opowiesci/konspekty/150105-dar-iliusitiusa-dla-andromedy.html), [130511](/rpg/inwazja/opowiesci/konspekty/130511-oltarz-podniesionej-dloni.html)|
|[August Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-august-bankierz.html)|4|[150104](/rpg/inwazja/opowiesci/konspekty/150104-terminus-defiler-kaplan-arazille.html), [150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [150105](/rpg/inwazja/opowiesci/konspekty/150105-dar-iliusitiusa-dla-andromedy.html), [130511](/rpg/inwazja/opowiesci/konspekty/130511-oltarz-podniesionej-dloni.html)|
|[Samira Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-samira-diakon.html)|3|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [150105](/rpg/inwazja/opowiesci/konspekty/150105-dar-iliusitiusa-dla-andromedy.html), [130511](/rpg/inwazja/opowiesci/konspekty/130511-oltarz-podniesionej-dloni.html)|
|[Netheria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-netheria-diakon.html)|3|[150104](/rpg/inwazja/opowiesci/konspekty/150104-terminus-defiler-kaplan-arazille.html), [150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html)|
|[Iliusitius](/rpg/inwazja/opowiesci/karty-postaci/9999-iliusitius.html)|3|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [150105](/rpg/inwazja/opowiesci/konspekty/150105-dar-iliusitiusa-dla-andromedy.html)|
|[Gabriel Newa](/rpg/inwazja/opowiesci/karty-postaci/9999-gabriel-newa.html)|3|[150104](/rpg/inwazja/opowiesci/konspekty/150104-terminus-defiler-kaplan-arazille.html), [150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html)|
|[Arazille](/rpg/inwazja/opowiesci/karty-postaci/9999-arazille.html)|3|[150104](/rpg/inwazja/opowiesci/konspekty/150104-terminus-defiler-kaplan-arazille.html), [150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html)|
|[Wojciech Kajak](/rpg/inwazja/opowiesci/karty-postaci/9999-wojciech-kajak.html)|2|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html)|
|[Patryk Romczak](/rpg/inwazja/opowiesci/karty-postaci/9999-patryk-romczak.html)|2|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [150105](/rpg/inwazja/opowiesci/konspekty/150105-dar-iliusitiusa-dla-andromedy.html)|
|[Luksja Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-luksja-diakon.html)|2|[150104](/rpg/inwazja/opowiesci/konspekty/150104-terminus-defiler-kaplan-arazille.html), [150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html)|
|[Feliks Hanson](/rpg/inwazja/opowiesci/karty-postaci/9999-feliks-hanson.html)|2|[150104](/rpg/inwazja/opowiesci/konspekty/150104-terminus-defiler-kaplan-arazille.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html)|
|[Anna Kajak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kajak.html)|2|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html), [141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html)|
|[Andrzej Szop](/rpg/inwazja/opowiesci/karty-postaci/9999-andrzej-szop.html)|2|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [150105](/rpg/inwazja/opowiesci/konspekty/150105-dar-iliusitiusa-dla-andromedy.html)|
|[Żanna Szczypiorek](/rpg/inwazja/opowiesci/karty-postaci/9999-zanna-szczypiorek.html)|1|[130511](/rpg/inwazja/opowiesci/konspekty/130511-oltarz-podniesionej-dloni.html)|
|[Zofia Szczypiorek](/rpg/inwazja/opowiesci/karty-postaci/9999-zofia-szczypiorek.html)|1|[150105](/rpg/inwazja/opowiesci/konspekty/150105-dar-iliusitiusa-dla-andromedy.html)|
|[Sebastian Linka](/rpg/inwazja/opowiesci/karty-postaci/9999-sebastian-linka.html)|1|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html)|
|[Robert Przerot](/rpg/inwazja/opowiesci/karty-postaci/9999-robert-przerot.html)|1|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html)|
|[Rafał Szczęślik](/rpg/inwazja/opowiesci/karty-postaci/9999-rafal-szczeslik.html)|1|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html)|
|[Radosław Szczypiorek](/rpg/inwazja/opowiesci/karty-postaci/9999-radoslaw-szczypiorek.html)|1|[130511](/rpg/inwazja/opowiesci/konspekty/130511-oltarz-podniesionej-dloni.html)|
|[Mirabelka Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-mirabelka-diakon.html)|1|[150104](/rpg/inwazja/opowiesci/konspekty/150104-terminus-defiler-kaplan-arazille.html)|
|[Maciej Dworek](/rpg/inwazja/opowiesci/karty-postaci/9999-maciej-dworek.html)|1|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html)|
|[Karradrael](/rpg/inwazja/opowiesci/karty-postaci/9999-karradrael.html)|1|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html)|
|[Józef Pimczak](/rpg/inwazja/opowiesci/karty-postaci/9999-jozef-pimczak.html)|1|[150103](/rpg/inwazja/opowiesci/konspekty/150103-pryzmat-mysli-peka.html)|
|[Józef Pasan](/rpg/inwazja/opowiesci/karty-postaci/9999-jozef-pasan.html)|1|[150105](/rpg/inwazja/opowiesci/konspekty/150105-dar-iliusitiusa-dla-andromedy.html)|
|[Inga Wójt](/rpg/inwazja/opowiesci/karty-postaci/9999-inga-wojt.html)|1|[130511](/rpg/inwazja/opowiesci/konspekty/130511-oltarz-podniesionej-dloni.html)|
|[Dariusz Germont](/rpg/inwazja/opowiesci/karty-postaci/9999-dariusz-germont.html)|1|[150105](/rpg/inwazja/opowiesci/konspekty/150105-dar-iliusitiusa-dla-andromedy.html)|
|[Augustyn Szczypiorek](/rpg/inwazja/opowiesci/karty-postaci/9999-augustyn-szczypiorek.html)|1|[130511](/rpg/inwazja/opowiesci/konspekty/130511-oltarz-podniesionej-dloni.html)|
|[Aneta Hanson](/rpg/inwazja/opowiesci/karty-postaci/9999-aneta-hanson.html)|1|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html)|
|[Anabela Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-anabela-diakon.html)|1|[150105](/rpg/inwazja/opowiesci/konspekty/150105-dar-iliusitiusa-dla-andromedy.html)|
|[Amelia Eter](/rpg/inwazja/opowiesci/karty-postaci/9999-amelia-eter.html)|1|[150104](/rpg/inwazja/opowiesci/konspekty/150104-terminus-defiler-kaplan-arazille.html)|

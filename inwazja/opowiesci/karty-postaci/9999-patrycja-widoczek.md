---
layout: inwazja-karta-postaci
categories: profile
title: "Patrycja Widoczek"
---
# {{ page.title }}

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Pętla dookoła niekralotha](/rpg/inwazja/opowiesci/konspekty/180402-petla-dookola-niekralotha.html)|nie stanie się permanentnym viciniusem; ma opcję powrotu (guaranteed future)|Adaptacja kralotyczna|

## Plany

|Misja|Plan|Kampania|
|-----|------|------|
|[Pętla dookoła niekralotha](/rpg/inwazja/opowiesci/konspekty/180402-petla-dookola-niekralotha.html)|nie chce odejść z Ognistych Niedźwiedzi. Chce je zmienić na "dobry gang".|Adaptacja kralotyczna|

---
layout: inwazja-karta-postaci
categories: profile
title: "Grzegorz Zachradnik"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|180321|kapłan Arazille a kiedyś biznesmen. Chce pomóc Marii oddać się Arazille - w ten sposób Maria utraci instynkty kralotyczne i będzie szczęśliwa.|[Porwanie pod nosem hipisów](/rpg/inwazja/opowiesci/konspekty/180321-porwanie-pod-nosem-hipisow.html)|10/12/08|10/12/09|[Adaptacja kralotyczna](/rpg/inwazja/opowiesci/konspekty/kampania-adaptacja-kralotyczna.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Siergiej Wdenkow](/rpg/inwazja/opowiesci/karty-postaci/1803-siergiej-wdenkow.html)|1|[180321](/rpg/inwazja/opowiesci/konspekty/180321-porwanie-pod-nosem-hipisow.html)|
|[Maria Przysiadek](/rpg/inwazja/opowiesci/karty-postaci/9999-maria-przysiadek.html)|1|[180321](/rpg/inwazja/opowiesci/konspekty/180321-porwanie-pod-nosem-hipisow.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|1|[180321](/rpg/inwazja/opowiesci/konspekty/180321-porwanie-pod-nosem-hipisow.html)|
|[Amelia Eter](/rpg/inwazja/opowiesci/karty-postaci/9999-amelia-eter.html)|1|[180321](/rpg/inwazja/opowiesci/konspekty/180321-porwanie-pod-nosem-hipisow.html)|
|[Alina Bednarz](/rpg/inwazja/opowiesci/karty-postaci/1803-alina-bednarz.html)|1|[180321](/rpg/inwazja/opowiesci/konspekty/180321-porwanie-pod-nosem-hipisow.html)|

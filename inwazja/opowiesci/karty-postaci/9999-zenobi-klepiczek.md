---
layout: inwazja-karta-postaci
categories: profile
title: "Zenobi Klepiczek"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170620|którego dotknęło Skażenie Henryka i który rzucił się całować Estrellę. Skończył związany. Magowie nie mają sił go odkażać przed walką z Luksją...|[Pułapka na Luksję](/rpg/inwazja/opowiesci/konspekty/170620-pulapka-na-luksje.html)|10/02/13|10/02/15|[Córka Lucyfera](/rpg/inwazja/opowiesci/konspekty/kampania-corka-lucyfera.html)|
|170530|ksiądz w Storczynie Mniejszym; głęboko zakochany w Estrelli przez machinacje Luksji. Pomocny, niewiele może zrobić stając przeciw takiemu złu.|[Córka Lucyfera](/rpg/inwazja/opowiesci/konspekty/170530-corka-lucyfera.html)|10/02/08|10/02/10|[Córka Lucyfera](/rpg/inwazja/opowiesci/konspekty/kampania-corka-lucyfera.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Luksja Pandemoniae](/rpg/inwazja/opowiesci/karty-postaci/9999-luksja-pandemoniae.html)|2|[170620](/rpg/inwazja/opowiesci/konspekty/170620-pulapka-na-luksje.html), [170530](/rpg/inwazja/opowiesci/konspekty/170530-corka-lucyfera.html)|
|[Henryk Siwiecki](/rpg/inwazja/opowiesci/karty-postaci/1709-henryk-siwiecki.html)|2|[170620](/rpg/inwazja/opowiesci/konspekty/170620-pulapka-na-luksje.html), [170530](/rpg/inwazja/opowiesci/konspekty/170530-corka-lucyfera.html)|
|[Estrella Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-estrella-diakon.html)|2|[170620](/rpg/inwazja/opowiesci/konspekty/170620-pulapka-na-luksje.html), [170530](/rpg/inwazja/opowiesci/konspekty/170530-corka-lucyfera.html)|
|[Łukasz Tworzyw](/rpg/inwazja/opowiesci/karty-postaci/9999-lukasz-tworzyw.html)|1|[170530](/rpg/inwazja/opowiesci/konspekty/170530-corka-lucyfera.html)|
|[Paweł Parobek](/rpg/inwazja/opowiesci/karty-postaci/9999-pawel-parobek.html)|1|[170620](/rpg/inwazja/opowiesci/konspekty/170620-pulapka-na-luksje.html)|
|[Kalina Cząberek](/rpg/inwazja/opowiesci/karty-postaci/9999-kalina-czaberek.html)|1|[170530](/rpg/inwazja/opowiesci/konspekty/170530-corka-lucyfera.html)|
|[Janina Jasionek](/rpg/inwazja/opowiesci/karty-postaci/9999-janina-jasionek.html)|1|[170530](/rpg/inwazja/opowiesci/konspekty/170530-corka-lucyfera.html)|
|[Henryk Kantosz](/rpg/inwazja/opowiesci/karty-postaci/9999-henryk-kantosz.html)|1|[170620](/rpg/inwazja/opowiesci/konspekty/170620-pulapka-na-luksje.html)|
|[Filip Cząberek](/rpg/inwazja/opowiesci/karty-postaci/9999-filip-czaberek.html)|1|[170530](/rpg/inwazja/opowiesci/konspekty/170530-corka-lucyfera.html)|
|[Dżony Słomian](/rpg/inwazja/opowiesci/karty-postaci/9999-dzony-slomian.html)|1|[170620](/rpg/inwazja/opowiesci/konspekty/170620-pulapka-na-luksje.html)|
|[Balbina Wróblewska](/rpg/inwazja/opowiesci/karty-postaci/9999-balbina-wroblewska.html)|1|[170620](/rpg/inwazja/opowiesci/konspekty/170620-pulapka-na-luksje.html)|
|[Antoni Bieguś](/rpg/inwazja/opowiesci/karty-postaci/9999-antoni-biegus.html)|1|[170620](/rpg/inwazja/opowiesci/konspekty/170620-pulapka-na-luksje.html)|

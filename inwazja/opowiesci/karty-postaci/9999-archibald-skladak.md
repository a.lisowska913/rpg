---
layout: inwazja-karta-postaci
categories: profile
title: "Archibald Składak"
---
# {{ page.title }}

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Cała przeszłość spłonęła](/rpg/inwazja/opowiesci/konspekty/170326-cala-przeszlosc-splonela.html)|ma transorganiczne implanty kontrolujące energię|Rezydentka Krukowa|
|[Cała przeszłość spłonęła](/rpg/inwazja/opowiesci/konspekty/170326-cala-przeszlosc-splonela.html)|stracił kamienicę, przeszłość i w sumie wszystko co miał w Kropwi|Rezydentka Krukowa|


## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170326|ma transorganiczny implant, potrafi dowodzić i zależy mu na społeczności. Stracił wszystko, co posiadał w ogniu magii.|[Cała przeszłość spłonęła](/rpg/inwazja/opowiesci/konspekty/170326-cala-przeszlosc-splonela.html)|10/03/04|10/03/07|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|
|170323|ostatni "Strażnik", który dobrze Paulinę przyjął... acz nie zamierzał niczego robić. Sporo opowiada.|[Oszczędzili na rurach](/rpg/inwazja/opowiesci/konspekty/170323-oszczedzili-na-rurach.html)|10/02/20|10/02/24|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|2|[170326](/rpg/inwazja/opowiesci/konspekty/170326-cala-przeszlosc-splonela.html), [170323](/rpg/inwazja/opowiesci/konspekty/170323-oszczedzili-na-rurach.html)|
|[Eneus Mucro](/rpg/inwazja/opowiesci/karty-postaci/9999-eneus-mucro.html)|2|[170326](/rpg/inwazja/opowiesci/konspekty/170326-cala-przeszlosc-splonela.html), [170323](/rpg/inwazja/opowiesci/konspekty/170323-oszczedzili-na-rurach.html)|
|[Wiaczesław Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-wiaczeslaw-zajcew.html)|1|[170326](/rpg/inwazja/opowiesci/konspekty/170326-cala-przeszlosc-splonela.html)|
|[Tadeusz Kuraszewicz](/rpg/inwazja/opowiesci/karty-postaci/9999-tadeusz-kuraszewicz.html)|1|[170323](/rpg/inwazja/opowiesci/konspekty/170323-oszczedzili-na-rurach.html)|
|[Piotr Pyszny](/rpg/inwazja/opowiesci/karty-postaci/9999-piotr-pyszny.html)|1|[170323](/rpg/inwazja/opowiesci/konspekty/170323-oszczedzili-na-rurach.html)|
|[Maciej Brzydal](/rpg/inwazja/opowiesci/karty-postaci/9999-maciej-brzydal.html)|1|[170323](/rpg/inwazja/opowiesci/konspekty/170323-oszczedzili-na-rurach.html)|
|[Janusz Kocieł](/rpg/inwazja/opowiesci/karty-postaci/9999-janusz-kociel.html)|1|[170326](/rpg/inwazja/opowiesci/konspekty/170326-cala-przeszlosc-splonela.html)|
|[Grażyna Kępka](/rpg/inwazja/opowiesci/karty-postaci/9999-grazyna-kepka.html)|1|[170326](/rpg/inwazja/opowiesci/konspekty/170326-cala-przeszlosc-splonela.html)|
|[Gabriel Dukat](/rpg/inwazja/opowiesci/karty-postaci/9999-gabriel-dukat.html)|1|[170326](/rpg/inwazja/opowiesci/konspekty/170326-cala-przeszlosc-splonela.html)|
|[Elgieskład](/rpg/inwazja/opowiesci/karty-postaci/9999-elgiesklad.html)|1|[170323](/rpg/inwazja/opowiesci/konspekty/170323-oszczedzili-na-rurach.html)|

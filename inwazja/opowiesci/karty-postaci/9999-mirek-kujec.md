---
layout: inwazja-karta-postaci
categories: profile
title: "Mirek Kujec"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|160117|brat Kingi, którego Korzunio wykorzystał by znaleźć słaby punkt Alicji Gąszcz.|[Muchy w sieci Korzunia](/rpg/inwazja/opowiesci/konspekty/160117-muchy-w-sieci-korzunia.html)|10/06/12|10/06/13|[Ucieczka do Przodka](/rpg/inwazja/opowiesci/konspekty/kampania-ucieczka-do-przodka.html)|
|151229|który pragnie 'nieść światło'. Idzie za silniejszym - wpierw za Przaśnikiem, teraz za Rekinem. Policjantka chce mu pomóc.|[..choć to na sektę nie pomoże (PT)](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html)|10/06/12|10/06/13|[Ucieczka do Przodka](/rpg/inwazja/opowiesci/konspekty/kampania-ucieczka-do-przodka.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Łukasz Perkas](/rpg/inwazja/opowiesci/karty-postaci/9999-lukasz-perkas.html)|2|[160117](/rpg/inwazja/opowiesci/konspekty/160117-muchy-w-sieci-korzunia.html), [151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html)|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|2|[160117](/rpg/inwazja/opowiesci/konspekty/160117-muchy-w-sieci-korzunia.html), [151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html)|
|[Grażyna Tuloz](/rpg/inwazja/opowiesci/karty-postaci/9999-grazyna-tuloz.html)|2|[160117](/rpg/inwazja/opowiesci/konspekty/160117-muchy-w-sieci-korzunia.html), [151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html)|
|[Alicja Gąszcz](/rpg/inwazja/opowiesci/karty-postaci/9999-alicja-gaszcz.html)|2|[160117](/rpg/inwazja/opowiesci/konspekty/160117-muchy-w-sieci-korzunia.html), [151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html)|
|[Wiesław Rekin](/rpg/inwazja/opowiesci/karty-postaci/9999-wieslaw-rekin.html)|1|[151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html)|
|[Marzena Dorszaj](/rpg/inwazja/opowiesci/karty-postaci/1709-marzena-dorszaj.html)|1|[151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html)|
|[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/1709-maria-newa.html)|1|[151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html)|
|[Maciej Orank](/rpg/inwazja/opowiesci/karty-postaci/9999-maciej-orank.html)|1|[160117](/rpg/inwazja/opowiesci/konspekty/160117-muchy-w-sieci-korzunia.html)|
|[Leopold Brzeszczot](/rpg/inwazja/opowiesci/karty-postaci/9999-leopold-brzeszczot.html)|1|[160117](/rpg/inwazja/opowiesci/konspekty/160117-muchy-w-sieci-korzunia.html)|
|[Krystian Korzunio](/rpg/inwazja/opowiesci/karty-postaci/1709-krystian-korzunio.html)|1|[160117](/rpg/inwazja/opowiesci/konspekty/160117-muchy-w-sieci-korzunia.html)|
|[Kinga Kujec](/rpg/inwazja/opowiesci/karty-postaci/9999-kinga-kujec.html)|1|[160117](/rpg/inwazja/opowiesci/konspekty/160117-muchy-w-sieci-korzunia.html)|
|[Jerzy Karmelik](/rpg/inwazja/opowiesci/karty-postaci/9999-jerzy-karmelik.html)|1|[151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html)|
|[Ireneusz Przaśnik](/rpg/inwazja/opowiesci/karty-postaci/9999-ireneusz-przasnik.html)|1|[151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html)|
|[Filip Brzeszczot](/rpg/inwazja/opowiesci/karty-postaci/9999-filip-brzeszczot.html)|1|[160117](/rpg/inwazja/opowiesci/konspekty/160117-muchy-w-sieci-korzunia.html)|
|[Ernest Brzeszczot](/rpg/inwazja/opowiesci/karty-postaci/9999-ernest-brzeszczot.html)|1|[160117](/rpg/inwazja/opowiesci/konspekty/160117-muchy-w-sieci-korzunia.html)|
|[Bartosz Bławatek](/rpg/inwazja/opowiesci/karty-postaci/9999-bartosz-blawatek.html)|1|[151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html)|

---
layout: inwazja-karta-postaci
categories: profile
title: "Maja Błyszczyk"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170405|artylerzystka dużej mocy, która stanowiła 80% siły ognia zwalczającej mechy, drony (też przyjazne). Jednostka antygrawitująca celatidy.|[Chyba wolelibyśmy kartony...](/rpg/inwazja/opowiesci/konspekty/170405-chyba-wolelibysmy-kartony.html)|10/08/18|10/08/19|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161218|kiedyś "Midnight", summonerka i artylerzystka; zintegrowała się z KADEMem i chce pomóc ze zintegrowaniem Whisperwind.|[Zazdrość Warmastera](/rpg/inwazja/opowiesci/konspekty/161218-zazdrosc-warmastera.html)|10/02/01|10/02/03|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|2|[170405](/rpg/inwazja/opowiesci/konspekty/170405-chyba-wolelibysmy-kartony.html), [161218](/rpg/inwazja/opowiesci/konspekty/161218-zazdrosc-warmastera.html)|
|[Quasar](/rpg/inwazja/opowiesci/karty-postaci/9999-quasar.html)|2|[170405](/rpg/inwazja/opowiesci/konspekty/170405-chyba-wolelibysmy-kartony.html), [161218](/rpg/inwazja/opowiesci/konspekty/161218-zazdrosc-warmastera.html)|
|[Andżelika Leszczyńska](/rpg/inwazja/opowiesci/karty-postaci/1709-andzelika-leszczynska.html)|2|[170405](/rpg/inwazja/opowiesci/konspekty/170405-chyba-wolelibysmy-kartony.html), [161218](/rpg/inwazja/opowiesci/konspekty/161218-zazdrosc-warmastera.html)|
|[Whisperwind](/rpg/inwazja/opowiesci/karty-postaci/9999-whisperwind.html)|1|[161218](/rpg/inwazja/opowiesci/konspekty/161218-zazdrosc-warmastera.html)|
|[Warmaster](/rpg/inwazja/opowiesci/karty-postaci/9999-warmaster.html)|1|[161218](/rpg/inwazja/opowiesci/konspekty/161218-zazdrosc-warmastera.html)|
|[Sławek Błyszczyk](/rpg/inwazja/opowiesci/karty-postaci/9999-slawek-blyszczyk.html)|1|[170405](/rpg/inwazja/opowiesci/konspekty/170405-chyba-wolelibysmy-kartony.html)|
|[Renata Souris](/rpg/inwazja/opowiesci/karty-postaci/1709-renata-souris.html)|1|[170405](/rpg/inwazja/opowiesci/konspekty/170405-chyba-wolelibysmy-kartony.html)|
|[Radosław Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-radoslaw-weiner.html)|1|[161218](/rpg/inwazja/opowiesci/konspekty/161218-zazdrosc-warmastera.html)|
|[Paweł Sępiak](/rpg/inwazja/opowiesci/karty-postaci/1709-pawel-sepiak.html)|1|[170405](/rpg/inwazja/opowiesci/konspekty/170405-chyba-wolelibysmy-kartony.html)|
|[Norbert Sonet](/rpg/inwazja/opowiesci/karty-postaci/9999-norbert-sonet.html)|1|[161218](/rpg/inwazja/opowiesci/konspekty/161218-zazdrosc-warmastera.html)|
|[Mordecja Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-mordecja-diakon.html)|1|[161218](/rpg/inwazja/opowiesci/konspekty/161218-zazdrosc-warmastera.html)|
|[Karolina Kupiec](/rpg/inwazja/opowiesci/karty-postaci/1803-karolina-kupiec.html)|1|[161218](/rpg/inwazja/opowiesci/konspekty/161218-zazdrosc-warmastera.html)|
|[Bogumił Rojowiec](/rpg/inwazja/opowiesci/karty-postaci/9999-bogumil-rojowiec.html)|1|[161218](/rpg/inwazja/opowiesci/konspekty/161218-zazdrosc-warmastera.html)|
|[Adrian Kropiak](/rpg/inwazja/opowiesci/karty-postaci/9999-adrian-kropiak.html)|1|[161218](/rpg/inwazja/opowiesci/konspekty/161218-zazdrosc-warmastera.html)|

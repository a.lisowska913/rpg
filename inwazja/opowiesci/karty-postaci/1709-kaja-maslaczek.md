---
layout: inwazja-karta-postaci
categories: profile
factions: "Bankierze Mazowieccy"
type: "NPC"
title: "Kaja Maślaczek"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **CEL: Lojalna Ewelinie**:
    * _Aspekty_: oddana nad swój dobrostan, odpędzać bezwartościowych kuzynów, znajdować haki i opcje szantażu, kompromitować wrogów Eweliny, zwiększać autonomię Eweliny
    * _Opis_: Ewelina dała jej szansę gdy wszyscy nią wzgardzili. Kaja ma dysfunkcyjną osobowość i całe swoje życie podporządkowała lojalności swojej Pani. Wie, że to może nie do końca zdrowa relacja - ale jest to jedyna relacja jaką Kaja jest skłonna zaakceptować. Pirotess dla Ashrama.
* **MRZ: Być badaczem, nie szpiegiem**:
    * _Aspekty_: zafascynowana Efemerydą, odkrywać nowe rzeczy, pragnienie zrozumienia wszystkiego, chomikowanie wiedzy, sprawić by wszystkim było lepiej
    * _Opis_: Kaja zawsze chciała być badaczką, naukowcem. Nie chciała być szpiegiem czy ekspertem wywiadu. Po prostu los tak ją pokierował.
* **FIL: Lepsza iluzja niż rzeczywistość**
    * _Aspekty_: chce koić ból, prawda jest nieistotna, "to dla Twojego dobra", fakty są tylko punktami widzenia
    * _Opis_: Zdaniem Kai, wszystko jest tylko różnymi formami fikcji i iluzji. W związku z tym, lepiej jest zapewnić wszystkim taką iluzję, z którą poczują się lepiej niż "prawdę" która sprawi im ból i nadal nie będzie "prawdziwa".

### Umiejętności

* **Agent wywiadu**:
    * _Aspekty_: podkładanie dowodów, ustawianie czujników, gubienie tropu, perfekcyjna maska, wywiad po dokumentach, włażenie gdzie jej nie chcą, ukrywanie czujników, kradzież kieszonkowa, niepozorna myszka
    * _Opis_: Kaja bardzo często wie, że coś "nie gra". Bardzo często potrafi spędzać sporo czasu w bibliotece czy wywiadowni by zdobyć istotne informacje na różne tematy. Trzyma perfekcyjną maskę służki, acz dość bezczelnej, i specjalizuje się w podkładaniu czujników.
* **Konstruktor**: 
    * _Aspekty_: kalibracja czujników, budowanie czujników, software + hardware, elektronika ludzka
    * _Opis_: Z zamiłowania konstruktorka, z konieczności szpieg. Kaja bardzo lubi spędzać czas nad kalibracją, doprecyzowywaniem, optymalizacją różnego rodzaju czujników. Szczególnie lubuje się w czujnikach "full band" - Zmysły + Kataliza + ludzkie umiejętności konstrukcji. Bardzo cierpliwa, potrafi je świetnie skalibrować i dobrze ukryć.
* **Motyl z żądłem**:
    * _Aspekty_: prowokowanie agresji, wyśmiewanie, mistrzowska ironia, bardzo elegancka, etykieta nadworna, aktorstwo, przyciąganie uwagi
    * _Opis_: Kaja porusza się po dworze Newerji Bankierz - jednym z najgroźniejszych miejsc społecznie na jakie można trafić. Nabyła wachlarz umiejętności pozwalający jej na prowokowanie przeciwników Lady Eweliny Bankierz do wpadania w tarapaty - oraz do odwracania uwagi od rzeczy ważnych.
    
### Silne i słabe strony:

* **Sassy and witty**:
    * _Aspekty_: złośliwa i urocza, silna osobowość, przeszarżowuje, świetnie wytrąca z równowagi, czasem się zapomina
    * _Opis_: Kaja jest sarkastyczna, złośliwa i zabawna. Ma bardzo silną osobowość i to widać - a to pod maską "pokornej służki". Jest to coś, co wytrąca rozmówców z równowagi i co sprawia, że bardzo łatwo jej wejść w niektóre środowiska - ale często przez to Kaja wpada w straszne kłopoty z których wyciągać ją musi Ewelina.

## Magia

### Szkoły magiczne

* **Magia zmysłów**: 
    * _Aspekty_: sensory i detektory, maskowanie siebie i urządzeń, poszerzanie detekcji, wzmacnianie zmysłów, ukrywanie prawdy
    * _Opis_: Dominująca i główna ścieżka Kai. Kaja wzmacnia swoje własne zmysły i wzmacnia skuteczność działania sensorów i detektorów różnego rodzaju. Dodatkowo, Kaja potrafi budować iluzje maskujące rzeczywistość i sprawiać, by ona sama nie była widoczna.
* **Kataliza**:
    * _Aspekty_: sensory i detektory, wyciszanie magii, badanie magii, wyostrzanie koloru magii
    * _Opis_: Kaja skupia się przede wszystkim na modulowaniu energii magicznej i jej badaniu. Dzięki temu jej czujniki są często niezrównane i Kaja potrafi dostrzec rzeczy, które większość osób przeoczy. Do tego wykorzystuje katalizę do infuzji czujników... i do maskowania swojej obecności i swoich działań.
* **Technomancja**:
    * _Aspekty_: sensory i detektory, włamania, urządzenia badawcze
    * _Opis_: Kaja się skupia na obszarach technomancji pozwalających jej na wzmocnieniu systemów detekcyjnych, badawczych i sensorów - ale także na włamywaniu się i przełamywaniu zabezpieczeń.

### Zaklęcia statyczne

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 

## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Zarobki

* służka Eweliny Bankierz

### Znam

* **Byli służący Eweliny**:
    * _Aspekty_: sekrety możnych, "wiem z kim porozmawiać", 
    * _Opis_: Służba na dworze Eweliny Bankierz przychodziła i odchodziła, a Kaja jest wieczna. Zawsze tam była, zawsze zostawała, zawsze obecna. Zawsze pomocna, acz złośliwa jak to Kaja. Niektórzy pamiętają ją lepiej, niektórzy jej wyjątkowo nie lubili...
* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

### Mam

* **Potęga Eweliny Bankierz**:
    * _Aspekty_: pieniądze na łapówki, drogie prezenty, służba i liberia, reputacja Eweliny Bankierz
    * _Opis_: Za Kają stoi duże bogactwo - dostępność do skarbca Eweliny Bankierz i wszystkie profity wynikające z jej statusu. Samo imię Eweliny otwiera część drzwi dla Kai - a niektórym ostrzy noże...
* **Narzędzia badawcze**:
    * _Aspekty_: wzmacnianie czujników, badanie natury rzeczy
    * _Opis_: Kaja kiedyś marzyła, by dostać się na EAM. Przeszło jej. Jednak te konkretne marzenia skierowała ku pracy nad czujnikami - jak je lepiej skalibrować, jak doprowadzić do tego, by dało się osiągnąć więcej mniejszą ilością, jak je jak najlepiej ukryć.
* **Archiwa wywiadowcze**:
    * _Aspekty_: kto co zrobił, prawdziwa historia, kompromitujące materiały
    * _Opis_: Kaja zbiera informacje i materiały na temat różnych wydarzeń, osób i okazji do szantażu. Sprawia to, że w wielu kwestiach Kaja jest na bieżąco z sytuacją polityczną, jak i sytuacją indywidualną poszczególnych osób.

# Opis

krótki opis postaci, rzeczy, jakie warto pamiętać.

### Koncept

Niezwykle lojalna swojej pani, Ewelinie Bankierz, Kaja jest jej służką i prawą ręką. Nie jest silna bojowo, ale jest głównym źródłem informacji. Jej siłą na misjach jest jej filozofia - Kaja szczerze próbuje pomóc okłamując i tworząc "pozytywną iluzję" oraz Kaja próbująca pomóc swojej pani. 

### Motto

"Tekst"

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Dlaczego Kret w jeziorze?](/rpg/inwazja/opowiesci/konspekty/180503-dlaczego-kret-w-jeziorze.html)|skutecznie wygenerowała niezadowolenie Newerji wobec Karmeny, co ratuje Ewelinę od "bycia drugą kiepską".|Wizja Dukata|
|[Kryzysowo tymczasowy dyktator](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|zdobyła haka na Apoloniusza Bankierza. Ma na jego sprawki liczne dowody.|Wizja Dukata|
|[Senesgradzka kopia Pauliny](/rpg/inwazja/opowiesci/konspekty/171218-senesgradzka-kopia-pauliny.html)|pracuje nad tym (z powodzeniem) by móc oddzielać byty sprzężone z Efemerydą Senesgradzką.|Wizja Dukata|
|[Senesgradzka kopia Pauliny](/rpg/inwazja/opowiesci/konspekty/171218-senesgradzka-kopia-pauliny.html)|zostawiła i złożyła w Senesgradzie czujniki, by pomóc Paulinie pomagać randomowym ludziom Skażonym przez Efemerydę|Wizja Dukata|
|[Wywalczone życie Diany](/rpg/inwazja/opowiesci/konspekty/170723-wywalczone-zycie-diany.html)|traci Dianę jako przyjaciółkę|Rezydentka Krukowa|

## Plany

|Misja|Plan|Kampania|
|-----|------|------|
|[Kryzysowo tymczasowy dyktator](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|chce zmaksymalizować zniszczenia na Apoloniuszu Bankierzu i pomóc Ewelinie odzyskać kontrolę nad regionem.|Wizja Dukata|
|[Epidemia Dezinhibicji](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|uważa, że już dość z epidemiami itp; czas zacząć pomagać i ratować sytuację (Kić)|Wizja Dukata|
|[Senesgradzka kopia Pauliny](/rpg/inwazja/opowiesci/konspekty/171218-senesgradzka-kopia-pauliny.html)|Zafascynowana Efemerydą Senesgradzką. Chce ją zrozumieć lepiej. Bardzo zmartwiona negatywnymi konsekwencjami Efemerydy na Senesgrad. Będzie monitorować.|Wizja Dukata|
|[Powrót do domu](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html)|konsekwentnie monitoruje efemerydę senesgradzką i upewnia isę, że nikt nie cierpi przez brak aktywnego sensownego rezydenta|Wizja Dukata|

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|171115|osiągnęła triumf swojego życia - uratowała Ewelinę, ukrzywdziła Apoloniusza... i teraz może lojalnie wspierać ten region.|[Kryzysowo tymczasowy dyktator](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|11/10/03|11/10/04|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171105|próbowała przekonać Dracenę do rozmontowania i zniszczenia sił Sądecznego na tym terenie. Udało jej się. Wycofała się by uwolnić Ewelinę.|[Epidemia Dezinhibicji](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|11/10/01|11/10/02|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171029|powiedziała Paulinie jak jej zdaniem wygląda sytuacja - Eliksir Miłości Sądecznego. Namawia do nieetycznych działań. Kaja jest groźna... jako sojusznik.|[W co gra Sądeczny?](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html)|11/09/20|11/09/22|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171218|wezwana przez Paulinę w sprawie Senesgradu zdobyła sekwencję modulacyjną pomagając Paulinie. Bardzo była skupiona na pomocy Paulinie i społeczności senesgradzkiej.|[Senesgradzka kopia Pauliny](/rpg/inwazja/opowiesci/konspekty/171218-senesgradzka-kopia-pauliny.html)|11/08/28|11/08/31|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171001|znowu ma pecha do Pauliny; gdy Pauliny nie było, zmontowała w jej domu zaawansowany system monitorowania efemerydy - by upewnić się, że nikt nie ucierpi przez brak rezydenta.|[Powrót do domu](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html)|11/08/25|11/08/27|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|170723|która chciała zadowolić wszystkich - Ewelinę, Paulinę, Dianę - i przegrała wszystko. Wróciła do Eweliny, straciła przyjaciółkę i nie wykonała zadania. Kocha Dianę jak siostrę, ale ją straciła.|[Wywalczone życie Diany](/rpg/inwazja/opowiesci/konspekty/170723-wywalczone-zycie-diany.html)|10/02/03|10/02/06|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|
|170702|służka Eweliny; Zmysły, Kataliza, Infomancja; przyjaciółka Diany i podobno bardzo bliska Ewelinie. Sassy and witty. Rozmieszcza dla Pauliny sensory detekcyjne.|[Miłość przez desperację](/rpg/inwazja/opowiesci/konspekty/170702-milosc-przez-desperacje.html)|10/01/29|10/02/02|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|7|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html), [171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html), [171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html), [171218](/rpg/inwazja/opowiesci/konspekty/171218-senesgradzka-kopia-pauliny.html), [171001](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html), [170723](/rpg/inwazja/opowiesci/konspekty/170723-wywalczone-zycie-diany.html), [170702](/rpg/inwazja/opowiesci/konspekty/170702-milosc-przez-desperacje.html)|
|[Sylwester Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-sylwester-bankierz.html)|4|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html), [171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html), [171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html), [171001](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|4|[171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html), [171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html), [171218](/rpg/inwazja/opowiesci/konspekty/171218-senesgradzka-kopia-pauliny.html), [171001](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html)|
|[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/1709-maria-newa.html)|3|[171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html), [171218](/rpg/inwazja/opowiesci/konspekty/171218-senesgradzka-kopia-pauliny.html), [171001](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html)|
|[Hektor Reszniaczek](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-reszniaczek.html)|3|[171001](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html), [170723](/rpg/inwazja/opowiesci/konspekty/170723-wywalczone-zycie-diany.html), [170702](/rpg/inwazja/opowiesci/konspekty/170702-milosc-przez-desperacje.html)|
|[Wiaczesław Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-wiaczeslaw-zajcew.html)|2|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html), [171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|
|[Tomasz Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-tomasz-myszeczka.html)|2|[171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html), [171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html)|
|[Robert Sądeczny](/rpg/inwazja/opowiesci/karty-postaci/1709-robert-sadeczny.html)|2|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html), [171001](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html)|
|[Prosperjusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-prosperjusz-bankierz.html)|2|[170723](/rpg/inwazja/opowiesci/konspekty/170723-wywalczone-zycie-diany.html), [170702](/rpg/inwazja/opowiesci/konspekty/170702-milosc-przez-desperacje.html)|
|[Karol Marzyciel](/rpg/inwazja/opowiesci/karty-postaci/1709-karol-marzyciel.html)|2|[171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html), [171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html)|
|[Karol Komnat](/rpg/inwazja/opowiesci/karty-postaci/9999-karol-komnat.html)|2|[170723](/rpg/inwazja/opowiesci/konspekty/170723-wywalczone-zycie-diany.html), [170702](/rpg/inwazja/opowiesci/konspekty/170702-milosc-przez-desperacje.html)|
|[Grazoniusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-grazoniusz-bankierz.html)|2|[170723](/rpg/inwazja/opowiesci/konspekty/170723-wywalczone-zycie-diany.html), [170702](/rpg/inwazja/opowiesci/konspekty/170702-milosc-przez-desperacje.html)|
|[Efemeryda Senesgradzka](/rpg/inwazja/opowiesci/karty-postaci/9999-efemeryda-senesgradzka.html)|2|[171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html), [171218](/rpg/inwazja/opowiesci/konspekty/171218-senesgradzka-kopia-pauliny.html)|
|[Diana Łuczkiewicz](/rpg/inwazja/opowiesci/karty-postaci/9999-diana-luczkiewicz.html)|2|[170723](/rpg/inwazja/opowiesci/konspekty/170723-wywalczone-zycie-diany.html), [170702](/rpg/inwazja/opowiesci/konspekty/170702-milosc-przez-desperacje.html)|
|[Daniel Akwitański](/rpg/inwazja/opowiesci/karty-postaci/1709-daniel-akwitanski.html)|2|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html), [171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|
|[Łucja Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-lucja-maus.html)|1|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|
|[Szczepan Złodrak](/rpg/inwazja/opowiesci/karty-postaci/9999-szczepan-zlodrak.html)|1|[171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|
|[Oliwia Aurinus](/rpg/inwazja/opowiesci/karty-postaci/1709-oliwia-aurinus.html)|1|[171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|
|[Oktawia Aurinus](/rpg/inwazja/opowiesci/karty-postaci/1709-oktawia-aurinus.html)|1|[171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html)|
|[Melodia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-melodia-diakon.html)|1|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|
|[Marcin Warinsky](/rpg/inwazja/opowiesci/karty-postaci/1709-marcin-warinsky.html)|1|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|
|[Katia Grajek](/rpg/inwazja/opowiesci/karty-postaci/1709-katia-grajek.html)|1|[171001](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html)|
|[Kajetan Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-kajetan-weiner.html)|1|[171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html)|
|[Janina Muczarok](/rpg/inwazja/opowiesci/karty-postaci/9999-janina-muczarok.html)|1|[171218](/rpg/inwazja/opowiesci/konspekty/171218-senesgradzka-kopia-pauliny.html)|
|[Jakub Dobrocień](/rpg/inwazja/opowiesci/karty-postaci/1709-jakub-dobrocien.html)|1|[171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html)|
|[Gabriel Dukat](/rpg/inwazja/opowiesci/karty-postaci/9999-gabriel-dukat.html)|1|[171001](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html)|
|[Ewelina Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-ewelina-bankierz.html)|1|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|
|[Dalia Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-dalia-weiner.html)|1|[171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|
|[Bolesław Złodrak](/rpg/inwazja/opowiesci/karty-postaci/9999-boleslaw-zlodrak.html)|1|[171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|
|[Bolesław Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-boleslaw-maus.html)|1|[171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html)|
|[Bogumił Miłoszept](/rpg/inwazja/opowiesci/karty-postaci/9999-bogumil-miloszept.html)|1|[170723](/rpg/inwazja/opowiesci/konspekty/170723-wywalczone-zycie-diany.html)|
|[Apoloniusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-apoloniusz-bankierz.html)|1|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|
|[Andżelika Leszczyńska](/rpg/inwazja/opowiesci/karty-postaci/1709-andzelika-leszczynska.html)|1|[171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html)|
|[Andrzej Toporek](/rpg/inwazja/opowiesci/karty-postaci/9999-andrzej-toporek.html)|1|[171001](/rpg/inwazja/opowiesci/konspekty/171001-powrot-do-domu.html)|

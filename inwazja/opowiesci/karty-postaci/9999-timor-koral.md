---
layout: inwazja-karta-postaci
categories: profile
title: "Timor Koral"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|150115|koordynator i przełożony LegioQuant który nie chce umierać... więc zginął.|[Negocjacje w LegioQuant](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html)|10/01/13|10/01/14|[Blakenbauerowie x Skorpion](/rpg/inwazja/opowiesci/konspekty/kampania-blakenbauerowie-x-skorpion.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Waltrauda Werner](/rpg/inwazja/opowiesci/karty-postaci/9999-waltrauda-werner.html)|1|[150115](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html)|
|[Olga Miodownik](/rpg/inwazja/opowiesci/karty-postaci/1709-olga-miodownik.html)|1|[150115](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html)|
|[Oddział Zeta](/rpg/inwazja/opowiesci/karty-postaci/9999-oddzial-zeta.html)|1|[150115](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html)|
|[Mordecja Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-mordecja-diakon.html)|1|[150115](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html)|
|[Margaret Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-margaret-blakenbauer.html)|1|[150115](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html)|
|[Marcelin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-marcelin-blakenbauer.html)|1|[150115](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html)|
|[Klemens X](/rpg/inwazja/opowiesci/karty-postaci/9999-klemens-x.html)|1|[150115](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html)|
|[Jakub Ryjek](/rpg/inwazja/opowiesci/karty-postaci/9999-jakub-ryjek.html)|1|[150115](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|1|[150115](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html)|
|[Filip Szorak](/rpg/inwazja/opowiesci/karty-postaci/9999-filip-szorak.html)|1|[150115](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html)|
|[Anna Kajak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kajak.html)|1|[150115](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html)|
|[Alina Bednarz](/rpg/inwazja/opowiesci/karty-postaci/1803-alina-bednarz.html)|1|[150115](/rpg/inwazja/opowiesci/konspekty/150115-negocjacje-w-legioquant.html)|

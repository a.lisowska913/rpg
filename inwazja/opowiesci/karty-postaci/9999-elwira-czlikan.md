---
layout: inwazja-karta-postaci
categories: profile
title: "Elwira Czlikan"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|171101|piękna strażniczka i służka Sądecznego. Silnie zmodyfikowana magicznie i oddana swemu panu.|[Magimedy przed epidemią](/rpg/inwazja/opowiesci/konspekty/171101-magimedy-przed-epidemia.html)|11/09/28|11/09/30|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Tomasz Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-tomasz-myszeczka.html)|1|[171101](/rpg/inwazja/opowiesci/konspekty/171101-magimedy-przed-epidemia.html)|
|[Tamara Muszkiet](/rpg/inwazja/opowiesci/karty-postaci/1709-tamara-muszkiet.html)|1|[171101](/rpg/inwazja/opowiesci/konspekty/171101-magimedy-przed-epidemia.html)|
|[Sylwester Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-sylwester-bankierz.html)|1|[171101](/rpg/inwazja/opowiesci/konspekty/171101-magimedy-przed-epidemia.html)|
|[Robert Sądeczny](/rpg/inwazja/opowiesci/karty-postaci/1709-robert-sadeczny.html)|1|[171101](/rpg/inwazja/opowiesci/konspekty/171101-magimedy-przed-epidemia.html)|
|[Radosław Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/9999-radoslaw-myszeczka.html)|1|[171101](/rpg/inwazja/opowiesci/konspekty/171101-magimedy-przed-epidemia.html)|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|1|[171101](/rpg/inwazja/opowiesci/konspekty/171101-magimedy-przed-epidemia.html)|
|[Jakub Dobrocień](/rpg/inwazja/opowiesci/karty-postaci/1709-jakub-dobrocien.html)|1|[171101](/rpg/inwazja/opowiesci/konspekty/171101-magimedy-przed-epidemia.html)|
|[Dalia Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-dalia-weiner.html)|1|[171101](/rpg/inwazja/opowiesci/konspekty/171101-magimedy-przed-epidemia.html)|
|[Aneta Rukolas](/rpg/inwazja/opowiesci/karty-postaci/9999-aneta-rukolas.html)|1|[171101](/rpg/inwazja/opowiesci/konspekty/171101-magimedy-przed-epidemia.html)|

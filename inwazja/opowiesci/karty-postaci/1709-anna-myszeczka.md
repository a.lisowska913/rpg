---
layout: inwazja-karta-postaci
categories: profile
factions: "Srebrna Świeca"
type: "NPC"
title: "Anna Myszeczka"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **FIL: Sentinel**:
    * _Aspekty_: 
    * _Opis_: "obronię Hektora i innych… nie obroniłam moich dzieci, ale obronię przynajmniej NICH"
* **BÓL: Deathwish**:
    * _Aspekty_: 
    * _Opis_: "to boli za mocno… chciałabym… chciałabym to przerwać"
* **BÓL: Zemsta - Karolina / Karradrael**:
    * _Aspekty_: blood knight
    * _Opis_: "nie umrę, póki ONI żyją. Będą umierać tysiące razy! Miałeś cierpieć tak jak cierpiałam kiedyś ja! Stań mi na drodze… i obiecam ci BÓL."

### Umiejętności

* **Terminus**:
    * _Aspekty_: antypotwór, antymag, medyk polowy, taktyk
    * _Opis_: 
* **??**:
    * _Aspekty_: tortury, zastraszanie
    * _Opis_: 
* **Matka dzieci**:
    * _Aspekty_: 
    * _Opis_: 
    
### Silne i słabe strony:

* **??**:
    * _Aspekty_: 
    * _Opis_: 
	
## Magia

### Szkoły magiczne

* **Biomancja**:
    * _Aspekty_: stymulowanie magów, adeptka Magii Krwi
    * _Opis_: 
* **Magia transportu**:
    * _Aspekty_: kineza
    * _Opis_: 
* **Kataliza**:
    * _Aspekty_: puryfikacja
    * _Opis_: 

### Zaklęcia statyczne

* **??**:
    * _Aspekty_: 
    * _Opis_: 
	
## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Zarobki

* żołd terminusa

### Znam

* **??**:
    * _Aspekty_: 
    * _Opis_: 
* **??**:
    * _Aspekty_: 
    * _Opis_: 
	
### Mam

* **??**:
    * _Aspekty_: 
    * _Opis_: 
* **??**:
    * _Aspekty_: 
    * _Opis_: 

# Opis
A young woman who used to have two children. By orders of Karradrael, Caroline has sacrificed them in the blood magic ritual in front of her eyes - to increase the suffering and break her. And Caroline has succeeded.

However, Ann got rescued by Blakenbauer magi, under command of Hector and Clara. This broken terminus and purifier has recollected herself somewhat, however she is not very stable yet. She has a bit of a death wish, hoping the pain will eventually stop, but she is not going to die before Caroline will die. The hatred fuels Ann and makes her carry-on, protecting Hector and others who might require her assistance.

She has an extremely soft spot towards the children which is completely understandable seeing her past. She tends to live internally rather than externally; she is neither outspoken nor outgoing. Tends to be quite ruthless and pragmatic. In better days, she would get a good therapy. Nowadays – she simply goes on.

Haunted by her past, this purifier and terminus tends to go all in in the moves which seem quite reckless - but they let her win in the world where normal people do not expect a terminus to “do things like that”.

## Motto

## Inne

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170104|wpadająca w ciężką paranoję i zarażająca nią Hektora; na szczęście, Mikado wziął ją na sparing. Miecz czy Pistolet - oto jest pytanie. Przegapiła wszystkie prawdziwe problemy.|[Spalone generatory pryzmatyczne](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html)|10/08/08|10/08/09|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161207|która prawie postrzeliła Hektora (nie poznała go w nowej formie) i wygrywa z Mikado w jengę. Zaprzyjaźniła się z nim... jakoś|[Lizanie ran na KADEMie](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html)|10/08/05|10/08/07|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161130|egzekutorka * magów Esuriit by chronić Hektora i Ekspedycję. Świetna na zasięg. Współuczestniczyła w egzekucji Kazimierza.|[Sprowadzenie Mare Vortex](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html)|10/08/02|10/08/04|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161124|która chroni Hektora i odkrywa luki w obronie Świecy. |[Ponura historia ekspedycji Esuriit](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html)|10/07/31|10/08/01|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161109|która wpierw uratowała łazik przed upadkiem a potem Speculoid sprawił, że prawie wpadła w berserk (iluzja przejeżdżanych dzieci). Miota kinetycznie granatami.|[Jak prawidłowo wpaść w pułapkę](/rpg/inwazja/opowiesci/konspekty/161109-jak-prawidlowo-wpasc-w-pulapke.html)|10/07/29|10/07/30|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161102|która nie oddala się od Hektora. Jest jak jego wierny cień - chronić i zapewnić bezpieczeństwo. Konsultuje Mausa jako puryfikatorka.|[Magowie Esuriit w domu](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html)|10/07/26|10/07/28|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161026|która po wszystkich rzeczach jakie się wydarzyły powoli ulega energii Esuriit; jeszcze można na niej polegać.|[Zagłodzona ekspedycja Świecy](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html)|10/07/24|10/07/25|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161012|zdesperowana terminuska, która oszukała Blakenbauerów by zginąć wraz z Hektorem - wiedziała, że nie da się ukryć przed Karradraelem. Przypadkowo przeżyła.|[Kontratak Karradraela](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|10/07/22|10/07/23|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160914|puryfikatorka i terminuska ciężko raniona dość często przez... Hektora w malignie. Przeżyła - i podjęła kilka trudnych decyzji. Śmierć jej dzieci nadal ją prześladuje...|[Aleksandria, krwawa Aleksandria](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html)|10/07/17|10/07/20|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160825|która straciła rodzinę przez Overminda i która z radością pomoże Blakenbauerom. Z danych od Karoliny wynika, że nie jest tajną bronią.|[Plany Overminda](/rpg/inwazja/opowiesci/konspekty/160825-plany-overminda.html)|10/07/15|10/07/16|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160819|ofiara The Governess, która została uratowana przez Dionizego i dostarczona do Rezydencji. Służyła jako podstawa do tworzenia Krwawokrągów.|[Oblicze guwernantki](/rpg/inwazja/opowiesci/konspekty/160819-oblicze-guwernantki.html)|10/07/12|10/07/14|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|11|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html), [161207](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html), [161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html), [161109](/rpg/inwazja/opowiesci/konspekty/161109-jak-prawidlowo-wpasc-w-pulapke.html), [161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html), [161026](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html), [161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html), [160914](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html), [160825](/rpg/inwazja/opowiesci/konspekty/160825-plany-overminda.html), [160819](/rpg/inwazja/opowiesci/konspekty/160819-oblicze-guwernantki.html)|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|8|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html), [161207](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html), [161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html), [161109](/rpg/inwazja/opowiesci/konspekty/161109-jak-prawidlowo-wpasc-w-pulapke.html), [161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html), [161026](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html), [161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|[Mikado Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-mikado-diakon.html)|7|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html), [161207](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html), [161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html), [161109](/rpg/inwazja/opowiesci/konspekty/161109-jak-prawidlowo-wpasc-w-pulapke.html), [161026](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html), [161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|[Andżelika Leszczyńska](/rpg/inwazja/opowiesci/karty-postaci/1709-andzelika-leszczynska.html)|7|[161207](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html), [161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html), [161109](/rpg/inwazja/opowiesci/konspekty/161109-jak-prawidlowo-wpasc-w-pulapke.html), [161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html), [161026](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html), [161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|[Marianna Sowińska](/rpg/inwazja/opowiesci/karty-postaci/1709-marianna-sowinska.html)|6|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html), [161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html), [161109](/rpg/inwazja/opowiesci/konspekty/161109-jak-prawidlowo-wpasc-w-pulapke.html), [161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html), [161026](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html)|
|[Ignat Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1802-ignat-zajcew.html)|6|[161207](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html), [161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html), [161109](/rpg/inwazja/opowiesci/konspekty/161109-jak-prawidlowo-wpasc-w-pulapke.html), [161026](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html), [161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|[Dosifiej Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-dosifiej-zajcew.html)|5|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html), [161109](/rpg/inwazja/opowiesci/konspekty/161109-jak-prawidlowo-wpasc-w-pulapke.html), [161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html), [161026](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html)|
|[Siriratharin](/rpg/inwazja/opowiesci/karty-postaci/1709-siriratharin.html)|4|[161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html), [161109](/rpg/inwazja/opowiesci/konspekty/161109-jak-prawidlowo-wpasc-w-pulapke.html), [161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html), [161026](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html)|
|[Klara Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-klara-blakenbauer.html)|4|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html), [160914](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html), [160825](/rpg/inwazja/opowiesci/konspekty/160825-plany-overminda.html), [160819](/rpg/inwazja/opowiesci/konspekty/160819-oblicze-guwernantki.html)|
|[Łukija Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-lukija-zajcew.html)|3|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html), [161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html)|
|[Zofia Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-zofia-weiner.html)|3|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html), [161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html)|
|[Otton Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-otton-blakenbauer.html)|3|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html), [160914](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html), [160819](/rpg/inwazja/opowiesci/konspekty/160819-oblicze-guwernantki.html)|
|[Karolina Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-karolina-maus.html)|3|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html), [160825](/rpg/inwazja/opowiesci/konspekty/160825-plany-overminda.html), [160819](/rpg/inwazja/opowiesci/konspekty/160819-oblicze-guwernantki.html)|
|[Jakub Pestka](/rpg/inwazja/opowiesci/karty-postaci/9999-jakub-pestka.html)|3|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html), [161026](/rpg/inwazja/opowiesci/konspekty/161026-zaglodzona-ekspedycja-swiecy.html)|
|[Fiodor Maius Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-fiodor-maius-zajcew.html)|3|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html), [161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|3|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html), [160914](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html), [160819](/rpg/inwazja/opowiesci/konspekty/160819-oblicze-guwernantki.html)|
|[Quasar](/rpg/inwazja/opowiesci/karty-postaci/9999-quasar.html)|2|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html), [161207](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html)|
|[Paweł Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-pawel-maus.html)|2|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html)|
|[Norbert Sonet](/rpg/inwazja/opowiesci/karty-postaci/9999-norbert-sonet.html)|2|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html), [161207](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html)|
|[Konstanty Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-konstanty-bankierz.html)|2|[161109](/rpg/inwazja/opowiesci/konspekty/161109-jak-prawidlowo-wpasc-w-pulapke.html), [161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html)|
|[Kazimierz Sowiński](/rpg/inwazja/opowiesci/karty-postaci/9999-kazimierz-sowinski.html)|2|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161124](/rpg/inwazja/opowiesci/konspekty/161124-ponura-historia-ekspedycji-esuriit.html)|
|[Ilona Amant](/rpg/inwazja/opowiesci/karty-postaci/9999-ilona-amant.html)|2|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html), [161207](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html)|
|[Bazyli Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-bazyli-weiner.html)|2|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html), [161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html)|
|[Wiktor Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1709-wiktor-sowinski.html)|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|[Vladlena Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-vladlena-zjacew.html)|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|[Tymotheus Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-tymotheus-blakenbauer.html)|1|[160914](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html)|
|[Saith Kameleon](/rpg/inwazja/opowiesci/karty-postaci/9999-saith-kameleon.html)|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|[Saith Flamecaller](/rpg/inwazja/opowiesci/karty-postaci/9999-saith-flamecaller.html)|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|[Roman Błyszczyk](/rpg/inwazja/opowiesci/karty-postaci/9999-roman-blyszczyk.html)|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|[Patrycja Krowiowska](/rpg/inwazja/opowiesci/karty-postaci/1709-patrycja-krowiowska.html)|1|[160914](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html)|
|[Ofelia Caesar](/rpg/inwazja/opowiesci/karty-postaci/9999-ofelia-caesar.html)|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|[Netheria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-netheria-diakon.html)|1|[160819](/rpg/inwazja/opowiesci/konspekty/160819-oblicze-guwernantki.html)|
|[Milena Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-milena-diakon.html)|1|[160825](/rpg/inwazja/opowiesci/konspekty/160825-plany-overminda.html)|
|[Marta Szysznicka](/rpg/inwazja/opowiesci/karty-postaci/9999-marta-szysznicka.html)|1|[161207](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html)|
|[Margaret Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-margaret-blakenbauer.html)|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|[Malwina Krówka](/rpg/inwazja/opowiesci/karty-postaci/9999-malwina-krowka.html)|1|[160914](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html)|
|[Lancelot Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-lancelot-bankierz.html)|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|[Konstanty Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-konstanty-myszeczka.html)|1|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html)|
|[Konrad Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/9999-konrad-myszeczka.html)|1|[161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html)|
|[Karradrael](/rpg/inwazja/opowiesci/karty-postaci/9999-karradrael.html)|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|[Karolina Kupiec](/rpg/inwazja/opowiesci/karty-postaci/1803-karolina-kupiec.html)|1|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html)|
|[Karina von Blutwurst](/rpg/inwazja/opowiesci/karty-postaci/9999-karina-von-blutwurst.html)|1|[160914](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html)|
|[Karina Paczulis](/rpg/inwazja/opowiesci/karty-postaci/9999-karina-paczulis.html)|1|[161207](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html)|
|[Irena Paniszok](/rpg/inwazja/opowiesci/karty-postaci/9999-irena-paniszok.html)|1|[160914](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html)|
|[Infernia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-infernia-diakon.html)|1|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html)|
|[GS "Aegis" 0003](/rpg/inwazja/opowiesci/karty-postaci/9999-gs-aegis-0003.html)|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|[Franciszek Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-maus.html)|1|[161207](/rpg/inwazja/opowiesci/konspekty/161207-lizanie-ran-na-kademie.html)|
|[Ernest Kokoszka](/rpg/inwazja/opowiesci/karty-postaci/9999-ernest-kokoszka.html)|1|[160914](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html)|
|[Eis](/rpg/inwazja/opowiesci/karty-postaci/9999-eis.html)|1|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html)|
|[Dionizy Kret](/rpg/inwazja/opowiesci/karty-postaci/1709-dionizy-kret.html)|1|[160819](/rpg/inwazja/opowiesci/konspekty/160819-oblicze-guwernantki.html)|
|[Dagmara Wyjątek](/rpg/inwazja/opowiesci/karty-postaci/1709-dagmara-wyjątek.html)|1|[161012](/rpg/inwazja/opowiesci/konspekty/161012-kontratak-karradraela.html)|
|[Bogumił Rojowiec](/rpg/inwazja/opowiesci/karty-postaci/9999-bogumil-rojowiec.html)|1|[170104](/rpg/inwazja/opowiesci/konspekty/170104-spalone-generatory-pryzmatyczne.html)|
|[Anatol Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-anatol-weiner.html)|1|[160914](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html)|
|[Alina Bednarz](/rpg/inwazja/opowiesci/karty-postaci/1803-alina-bednarz.html)|1|[160819](/rpg/inwazja/opowiesci/konspekty/160819-oblicze-guwernantki.html)|
|[Adrian Murarz](/rpg/inwazja/opowiesci/karty-postaci/9999-adrian-murarz.html)|1|[160914](/rpg/inwazja/opowiesci/konspekty/160914-aleksandria-krwawa-aleksandria.html)|

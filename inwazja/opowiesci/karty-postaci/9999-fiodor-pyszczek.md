---
layout: inwazja-karta-postaci
categories: profile
title: "Fiodor Pyszczek"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170214|znajomy Henryka mag Szlachty, który pomógł jak mógł... niestety, niewiele mógł. Wie, że Henryk może wpaść w łapki defilera.|[Nekroborg dla Laetitii Gai](/rpg/inwazja/opowiesci/konspekty/170214-nekroborg-dla-laetitii-gai.html)|10/07/26|10/07/29|[Nicaretta](/rpg/inwazja/opowiesci/konspekty/kampania-nicaretta.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Nicaretta](/rpg/inwazja/opowiesci/karty-postaci/1709-nicaretta.html)|1|[170214](/rpg/inwazja/opowiesci/konspekty/170214-nekroborg-dla-laetitii-gai.html)|
|[Laetitia Gaia Rasputin Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-laetitia-gaia-rasputin-weiner.html)|1|[170214](/rpg/inwazja/opowiesci/konspekty/170214-nekroborg-dla-laetitii-gai.html)|
|[Karina Łoszad](/rpg/inwazja/opowiesci/karty-postaci/9999-karina-loszad.html)|1|[170214](/rpg/inwazja/opowiesci/konspekty/170214-nekroborg-dla-laetitii-gai.html)|
|[Henryk Siwiecki](/rpg/inwazja/opowiesci/karty-postaci/1709-henryk-siwiecki.html)|1|[170214](/rpg/inwazja/opowiesci/konspekty/170214-nekroborg-dla-laetitii-gai.html)|

---
layout: inwazja-karta-postaci
categories: profile
factions: "Nieznany"
type: "PC"
owner: "wojtek"
title: "Paweł Szlezg"
---
# {{ page.title }}

## Postać

### Motywacje 
#### Kategorie i Aspekty

| Kategoria         | Aspekty                           |
|-------------------|-----------------------------------|
| Indywidualne      | rozrywka; rozwój osobisty; szybka adaptacja |
| Społeczne         | równość |
| Wartości          | wybiórcza ignorancja; wolność |

#### Szczególnie

| Co chce by się działo?                                                      | Co na pewno ma się NIE dziać? Co jest sprzeczne?                                                  |
|-----------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------|
| dawaj ile możesz, ale bierz ile dają | nie pozwalaj sobą kierować |
| nie patrz w przeszłość | nikt nie będzie mi mówił, co mi wolno, a czego nie |

### Umiejętności
#### Kategorie i Aspekty

| Kategoria         | Aspekty                           |
|-------------------|-----------------------------------|
| Muzyk | gra na instrumentach; improwizacja |
| Nauczyciel | cierpliwość do ludzi; dostęp do biblioteki i materiałów naukowych; wyczucie do ludzi |
| Wywrotowiec | znajomość miasta; krav maga |

#### Manewry

| Jakie działania wykonuje?                                                              | Czym osiąga sukces?                                                          |
|----------------------------------------------------------------------------------------|------------------------------------------------------------------------------|
|  |  |

### Silne i słabe strony:
#### Kategorie i Aspekty

| Kategoria            | Aspekty                        |
|----------------------|--------------------------------|
| szczerość do bólu w sytuacji stresowej |  |

#### Manewry

| Co jest wzmocnione                                        | Kosztem czego                                                 |
|-----------------------------------------------------------|---------------------------------------------------------------|
|  |  |

## Zasoby i otoczenie
#### Powiązane frakcje

{{ page.factions }}

#### Kategorie i Aspekty

| Kategoria                          | Aspekty                                                              |
|------------------------------------|----------------------------------------------------------------------|
| hodowla jaszczurek |  |
| playstation |  |
| glany |  |

#### Manewry

| Jakie działania wspierane?                                                      | Czym osiąga sukces?                                           |
|---------------------------------------------------------------------------------|---------------------------------------------------------------|
|  |  |

# Opis

### Koncept

Postać 1 + Postać 2 + Postać 3

### Ogólnie

### Motywacje:

### Działanie:

### Specjalne:

### Magia:

### Otoczenie:

### Mapa kreacji

brak

### Motto

""

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|180418|człowiek z saksofonem który totalnie boi się wciąć w kolejkę w Stonce. Zdecydował się uratować człowieka-jaszczura przed magiem. Średnie poszanowanie własności ;-).|[Czapkowicka Apatia Kulturalna](/rpg/inwazja/opowiesci/konspekty/180418-czapkowicka-apatia-kulturalna.html)|10/11/08|10/11/10|[Dusza Czapkowika](/rpg/inwazja/opowiesci/konspekty/kampania-dusza-czapkowika.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Sławek Broda](/rpg/inwazja/opowiesci/karty-postaci/9999-slawek-broda.html)|1|[180418](/rpg/inwazja/opowiesci/konspekty/180418-czapkowicka-apatia-kulturalna.html)|
|[Szczepan Porzeczka](/rpg/inwazja/opowiesci/karty-postaci/9999-szczepan-porzeczka.html)|1|[180418](/rpg/inwazja/opowiesci/konspekty/180418-czapkowicka-apatia-kulturalna.html)|
|[Siergiej Wdenkow](/rpg/inwazja/opowiesci/karty-postaci/1803-siergiej-wdenkow.html)|1|[180418](/rpg/inwazja/opowiesci/konspekty/180418-czapkowicka-apatia-kulturalna.html)|
|[Mariusz Niewiadomski](/rpg/inwazja/opowiesci/karty-postaci/9999-mariusz-niewiadomski.html)|1|[180418](/rpg/inwazja/opowiesci/konspekty/180418-czapkowicka-apatia-kulturalna.html)|
|[Kasia Krabowska](/rpg/inwazja/opowiesci/karty-postaci/9999-kasia-krabowska.html)|1|[180418](/rpg/inwazja/opowiesci/konspekty/180418-czapkowicka-apatia-kulturalna.html)|
|[Franciszek Bratkowski](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-bratkowski.html)|1|[180418](/rpg/inwazja/opowiesci/konspekty/180418-czapkowicka-apatia-kulturalna.html)|
|[Aneta Pietraszek](/rpg/inwazja/opowiesci/karty-postaci/1803-aneta-pietraszek.html)|1|[180418](/rpg/inwazja/opowiesci/konspekty/180418-czapkowicka-apatia-kulturalna.html)|
|[Alojzy Przylaz](/rpg/inwazja/opowiesci/karty-postaci/1803-alojzy-przylaz.html)|1|[180418](/rpg/inwazja/opowiesci/konspekty/180418-czapkowicka-apatia-kulturalna.html)|
|[Alfred Werner](/rpg/inwazja/opowiesci/karty-postaci/9999-alfred-werner.html)|1|[180418](/rpg/inwazja/opowiesci/konspekty/180418-czapkowicka-apatia-kulturalna.html)|

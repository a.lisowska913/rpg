---
layout: inwazja-karta-postaci
categories: profile
title: "Bolesław Derwisz"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|161002|sprawdził historyczne dane z rekordów Świecy dla Pauliny o Weinerach tutaj. Zauważył, że Świeca została tu zniszczona. Opowiedział legendę o Kseni Zajcew.|[Wyciek syberionu](/rpg/inwazja/opowiesci/konspekty/161002-wyciek-syberionu.html)|10/07/11|10/07/13|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|
|160309|który z ramienia Szlachty wspierał działania po* magające * magom Świecy w obliczu Irytki Sprzężonej.|[Irytka Sprzężona](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|10/06/25|10/06/26|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|141026|terminus*mentalista o związanych rękach, który próbuje znaleźć sprawiedliwość w świecie gildii magicznych.|[Dracena widziała swój koszmar](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html)|10/06/12|10/06/13|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|
|141025|Diakonożerczy terminus*mentalista chcący pomóc ludziom i pognębić wszystko co Diakonoidalne.|[Gildie widziały protomaga](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|10/06/10|10/06/11|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|
|160922|powiedział Esme o Supernowej Diakon i ostrzegł przed potencjalną obecnością Mikada.|[Czarnoskalski konwent RPG](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|10/05/14|10/05/19|[Taniec Liści](/rpg/inwazja/opowiesci/konspekty/kampania-taniec-lisci.html)|
|140508|ludzista i mentalista który od zawsze jest pełny niechęci do Diakonów.|['Lord Jonatan'](/rpg/inwazja/opowiesci/konspekty/140508-lord-jonatan.html)|10/01/07|10/01/08|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|3|[161002](/rpg/inwazja/opowiesci/konspekty/161002-wyciek-syberionu.html), [141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/1709-maria-newa.html)|3|[161002](/rpg/inwazja/opowiesci/konspekty/161002-wyciek-syberionu.html), [141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|3|[161002](/rpg/inwazja/opowiesci/konspekty/161002-wyciek-syberionu.html), [141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[Silgor Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-silgor-diakon.html)|2|[141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[Ryszard Bocian](/rpg/inwazja/opowiesci/karty-postaci/9999-ryszard-bocian.html)|2|[141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[Rafael Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-rafael-diakon.html)|2|[141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[Paweł Brokoty](/rpg/inwazja/opowiesci/karty-postaci/9999-pawel-brokoty.html)|2|[141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[Nikola Kamień](/rpg/inwazja/opowiesci/karty-postaci/9999-nikola-kamien.html)|2|[141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[Jan Fiołek](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-fiolek.html)|2|[141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[Jan Bocian](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-bocian.html)|2|[141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[Franciszek Marlin](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-marlin.html)|2|[141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[Dyta](/rpg/inwazja/opowiesci/karty-postaci/9999-dyta.html)|2|[141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[Dalia Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-dalia-weiner.html)|2|[141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[Bartosz Bławatek](/rpg/inwazja/opowiesci/karty-postaci/9999-bartosz-blawatek.html)|2|[141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[Artur Kurczak](/rpg/inwazja/opowiesci/karty-postaci/9999-artur-kurczak.html)|2|[141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html), [141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[Tomasz Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-tomasz-weiner.html)|1|[161002](/rpg/inwazja/opowiesci/konspekty/161002-wyciek-syberionu.html)|
|[Tadeusz Baran](/rpg/inwazja/opowiesci/karty-postaci/1709-tadeusz-baran.html)|1|[140508](/rpg/inwazja/opowiesci/konspekty/140508-lord-jonatan.html)|
|[Supernowa Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-supernowa-diakon.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Rufus Czubek](/rpg/inwazja/opowiesci/karty-postaci/9999-rufus-czubek.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Rafał Kniaź](/rpg/inwazja/opowiesci/karty-postaci/9999-rafal-kniaz.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Patrycja Krowiowska](/rpg/inwazja/opowiesci/karty-postaci/1709-patrycja-krowiowska.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Ozydiusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-ozydiusz-bankierz.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Oktawian Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-oktawian-maus.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Milena Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-milena-diakon.html)|1|[140508](/rpg/inwazja/opowiesci/konspekty/140508-lord-jonatan.html)|
|[Mikado Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-mikado-diakon.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Michał Jesiotr](/rpg/inwazja/opowiesci/karty-postaci/9999-michal-jesiotr.html)|1|[161002](/rpg/inwazja/opowiesci/konspekty/161002-wyciek-syberionu.html)|
|[Mateusz Ackmann](/rpg/inwazja/opowiesci/karty-postaci/1709-mateusz-ackmann.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Margaret Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-margaret-blakenbauer.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Leonidas Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-leonidas-blakenbauer.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Kurt Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-kurt-weiner.html)|1|[161002](/rpg/inwazja/opowiesci/konspekty/161002-wyciek-syberionu.html)|
|[Krystian Korzunio](/rpg/inwazja/opowiesci/karty-postaci/1709-krystian-korzunio.html)|1|[161002](/rpg/inwazja/opowiesci/konspekty/161002-wyciek-syberionu.html)|
|[Kornel Wadera](/rpg/inwazja/opowiesci/karty-postaci/1709-kornel-wadera.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Kleofas Bór](/rpg/inwazja/opowiesci/karty-postaci/1709-kleofas-bor.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Klara Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-klara-blakenbauer.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Kinga Melit](/rpg/inwazja/opowiesci/karty-postaci/9999-kinga-melit.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Katarzyna Trzosek](/rpg/inwazja/opowiesci/karty-postaci/9999-katarzyna-trzosek.html)|1|[141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[Karradrael](/rpg/inwazja/opowiesci/karty-postaci/9999-karradrael.html)|1|[140508](/rpg/inwazja/opowiesci/konspekty/140508-lord-jonatan.html)|
|[Karol Poczciwiec](/rpg/inwazja/opowiesci/karty-postaci/9999-karol-poczciwiec.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Julia Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-julia-weiner.html)|1|[140508](/rpg/inwazja/opowiesci/konspekty/140508-lord-jonatan.html)|
|[Judyta Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-judyta-maus.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Joachim Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-zajcew.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Jessica Ruczaj](/rpg/inwazja/opowiesci/karty-postaci/9999-jessica-ruczaj.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Jan Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-weiner.html)|1|[140508](/rpg/inwazja/opowiesci/konspekty/140508-lord-jonatan.html)|
|[Irena Krysniok](/rpg/inwazja/opowiesci/karty-postaci/9999-irena-krysniok.html)|1|[141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[Infensa Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-infensa-diakon.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Halina Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-halina-weiner.html)|1|[161002](/rpg/inwazja/opowiesci/konspekty/161002-wyciek-syberionu.html)|
|[Grzegorz Murzecki](/rpg/inwazja/opowiesci/karty-postaci/9999-grzegorz-murzecki.html)|1|[141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html)|
|[Esme Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-esme-myszeczka.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Emilia Kołatka](/rpg/inwazja/opowiesci/karty-postaci/1709-emilia-kolatka.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Elea Maus](/rpg/inwazja/opowiesci/karty-postaci/1802-elea-maus.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Edmund Marlin](/rpg/inwazja/opowiesci/karty-postaci/9999-edmund-marlin.html)|1|[141025](/rpg/inwazja/opowiesci/konspekty/141025-gildie-widzialy-protomaga.html)|
|[Dorota Gacek](/rpg/inwazja/opowiesci/karty-postaci/1709-dorota-gacek.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Diana Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-diana-weiner.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Dagmara Wyjątek](/rpg/inwazja/opowiesci/karty-postaci/1709-dagmara-wyjątek.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Cyprian Koziej](/rpg/inwazja/opowiesci/karty-postaci/9999-cyprian-koziej.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Cierń](/rpg/inwazja/opowiesci/karty-postaci/9999-ciern.html)|1|[140508](/rpg/inwazja/opowiesci/konspekty/140508-lord-jonatan.html)|
|[Cezary Sito](/rpg/inwazja/opowiesci/karty-postaci/9999-cezary-sito.html)|1|[140508](/rpg/inwazja/opowiesci/konspekty/140508-lord-jonatan.html)|
|[Artur Pawiak](/rpg/inwazja/opowiesci/karty-postaci/9999-artur-pawiak.html)|1|[160922](/rpg/inwazja/opowiesci/konspekty/160922-czarnoskalski-konwent-rpg.html)|
|[Anton Jesiotr](/rpg/inwazja/opowiesci/karty-postaci/9999-anton-jesiotr.html)|1|[161002](/rpg/inwazja/opowiesci/konspekty/161002-wyciek-syberionu.html)|
|[Anna Kajak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kajak.html)|1|[160309](/rpg/inwazja/opowiesci/konspekty/160309-irytka-sprzezona.html)|
|[Andrea Wilgacz](/rpg/inwazja/opowiesci/karty-postaci/1709-andrea-wilgacz.html)|1|[140508](/rpg/inwazja/opowiesci/konspekty/140508-lord-jonatan.html)|
|[Alicja Gąszcz](/rpg/inwazja/opowiesci/karty-postaci/9999-alicja-gaszcz.html)|1|[141026](/rpg/inwazja/opowiesci/konspekty/141026-dracena-widziala-swoj-koszmar.html)|
|[Adam Pączek](/rpg/inwazja/opowiesci/karty-postaci/9999-adam-paczek.html)|1|[140508](/rpg/inwazja/opowiesci/konspekty/140508-lord-jonatan.html)|

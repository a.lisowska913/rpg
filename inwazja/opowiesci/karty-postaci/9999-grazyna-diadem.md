---
layout: inwazja-karta-postaci
categories: profile
title: "Grażyna Diadem"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170120|prostytutka i dawny host Nicaretty; zafascynowana Nicarettą i dywersja dająca Nicaretcie okazję do ataku.|[Wielki sojusz powszechny](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html)|10/08/05|10/08/06|[Nicaretta](/rpg/inwazja/opowiesci/konspekty/kampania-nicaretta.html)|
|161110|prostytutka i nowy host Nicaretty. Henryk wie, jak wygląda.|[Succubus myśli, że uciekł](/rpg/inwazja/opowiesci/konspekty/161110-succubus-mysli-ze-uciekl.html)|10/07/14|10/07/20|[Nicaretta](/rpg/inwazja/opowiesci/konspekty/kampania-nicaretta.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Nicaretta](/rpg/inwazja/opowiesci/karty-postaci/1709-nicaretta.html)|2|[170120](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html), [161110](/rpg/inwazja/opowiesci/konspekty/161110-succubus-mysli-ze-uciekl.html)|
|[Hubert Kaldwor](/rpg/inwazja/opowiesci/karty-postaci/9999-hubert-kaldwor.html)|2|[170120](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html), [161110](/rpg/inwazja/opowiesci/konspekty/161110-succubus-mysli-ze-uciekl.html)|
|[Henryk Siwiecki](/rpg/inwazja/opowiesci/karty-postaci/1709-henryk-siwiecki.html)|2|[170120](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html), [161110](/rpg/inwazja/opowiesci/konspekty/161110-succubus-mysli-ze-uciekl.html)|
|[Witold Małek](/rpg/inwazja/opowiesci/karty-postaci/9999-witold-malek.html)|1|[170120](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html)|
|[Sebastian Drabon](/rpg/inwazja/opowiesci/karty-postaci/9999-sebastian-drabon.html)|1|[170120](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html)|
|[Marzena Gilek](/rpg/inwazja/opowiesci/karty-postaci/9999-marzena-gilek.html)|1|[161110](/rpg/inwazja/opowiesci/konspekty/161110-succubus-mysli-ze-uciekl.html)|
|[Marzena Dorszaj](/rpg/inwazja/opowiesci/karty-postaci/1709-marzena-dorszaj.html)|1|[170120](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html)|
|[Luna](/rpg/inwazja/opowiesci/karty-postaci/9999-luna.html)|1|[161110](/rpg/inwazja/opowiesci/konspekty/161110-succubus-mysli-ze-uciekl.html)|
|[Kinglord](/rpg/inwazja/opowiesci/karty-postaci/9999-kinglord.html)|1|[170120](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html)|
|[Karina Łoszad](/rpg/inwazja/opowiesci/karty-postaci/9999-karina-loszad.html)|1|[170120](/rpg/inwazja/opowiesci/konspekty/170120-wielki-sojusz-powszechny.html)|
|[Albert Czapkuś](/rpg/inwazja/opowiesci/karty-postaci/9999-albert-czapkus.html)|1|[161110](/rpg/inwazja/opowiesci/konspekty/161110-succubus-mysli-ze-uciekl.html)|

---
layout: inwazja-karta-postaci
categories: profile
factions: "Nieznany"
type: "NPC"
owner: "wojtuś"
title: "Marcin Szybisty"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **FIL: Pragmatyczny**: 
    * _Aspekty_: 
    * _Opis_: 
* **MET: Impulsywny**:
    * _Aspekty_: 
    * _Opis_: 

### Umiejętności

* **Przemytnik**:
    * _Aspekty_: kontrabanda, wycena, ekonomia, korupcja
    * _Opis_: 
* **Imprezowicz**: 
    * _Aspekty_: granie na instrumentach
    * _Opis_: 
* **Biurokrata**:
    * _Aspekty_: fałszerstwo
    * _Opis_: 
    
### Silne i słabe strony:

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

## Magia

**brak**

## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Zarobki

* ?

### Znam

* **wielkomiejska elita**:
    * _Aspekty_: 
    * _Opis_: 
* **przemytnicy**:
    * _Aspekty_: 
    * _Opis_: 
* **wróg: były wspólnik**:
    * _Aspekty_: 
    * _Opis_: 

### Mam

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

# Opis

Marcin Szybisty - urodzony w Opolu, wywodzi się z dość zamożnej rodziny. Studiował ekonomię na Uniwersytecie Wrocławskim, ale zrezygnował na początku 3 roku studiów. Nie kontynuował nauki, za to w czasie studiów we Wrocławiu zażył wielkomiejskiego życia i zbudował mocną sieć kontaktów. Zawsze lubił kombinować w życiu i szukać drogi na skróty. Grywa na saksofonie oraz jest wielkim fanem petanki. Jest człowiekiem dość impulsywnym.
Prowadził aktywną działalność przemytniczą. Głównie alkohol oraz wyroby tytoniowe przez wschodnią granicę, ale były również epizody związane z przemytem ludzi. Jego działalność nigdy nie wyszła na jaw, wycofał się z interesu po mocnym spięciu z ówczesnym wspólnikiem.

### Koncept

Wysokopoziomowy koncept postaci, 1-2 zdania.

### Motto

"Tekst"

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|160915|upijał prezesa by wyciągać informacje, blokował niebezpieczne pomysły i bronił interesów firmy|[Rekrutacja mimo woli](/rpg/inwazja/opowiesci/konspekty/160915-rekrutacja-mimo-woli.html)|10/05/26|10/05/29|[Taniec Liści](/rpg/inwazja/opowiesci/konspekty/kampania-taniec-lisci.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Włodzimierz Tulewicz](/rpg/inwazja/opowiesci/karty-postaci/1709-wlodzimierz-tulewicz.html)|1|[160915](/rpg/inwazja/opowiesci/konspekty/160915-rekrutacja-mimo-woli.html)|
|[Szymon Skubny](/rpg/inwazja/opowiesci/karty-postaci/9999-szymon-skubny.html)|1|[160915](/rpg/inwazja/opowiesci/konspekty/160915-rekrutacja-mimo-woli.html)|
|[Magda Szybisty](/rpg/inwazja/opowiesci/karty-postaci/9999-magda-szybisty.html)|1|[160915](/rpg/inwazja/opowiesci/konspekty/160915-rekrutacja-mimo-woli.html)|
|[Leszek Żółty](/rpg/inwazja/opowiesci/karty-postaci/9999-leszek-zolty.html)|1|[160915](/rpg/inwazja/opowiesci/konspekty/160915-rekrutacja-mimo-woli.html)|
|[Leopold Teściak](/rpg/inwazja/opowiesci/karty-postaci/9999-leopold-tesciak.html)|1|[160915](/rpg/inwazja/opowiesci/konspekty/160915-rekrutacja-mimo-woli.html)|
|[Kornel Wadera](/rpg/inwazja/opowiesci/karty-postaci/1709-kornel-wadera.html)|1|[160915](/rpg/inwazja/opowiesci/konspekty/160915-rekrutacja-mimo-woli.html)|
|[Konrad Matczak](/rpg/inwazja/opowiesci/karty-postaci/9999-konrad-matczak.html)|1|[160915](/rpg/inwazja/opowiesci/konspekty/160915-rekrutacja-mimo-woli.html)|
|[Jolanta Iwan](/rpg/inwazja/opowiesci/karty-postaci/9999-jolanta-iwan.html)|1|[160915](/rpg/inwazja/opowiesci/konspekty/160915-rekrutacja-mimo-woli.html)|
|[Jacek Molenda](/rpg/inwazja/opowiesci/karty-postaci/9999-jacek-molenda.html)|1|[160915](/rpg/inwazja/opowiesci/konspekty/160915-rekrutacja-mimo-woli.html)|
|[Dorota Gacek](/rpg/inwazja/opowiesci/karty-postaci/1709-dorota-gacek.html)|1|[160915](/rpg/inwazja/opowiesci/konspekty/160915-rekrutacja-mimo-woli.html)|
|[Andrzej Marciniak](/rpg/inwazja/opowiesci/karty-postaci/9999-andrzej-marciniak.html)|1|[160915](/rpg/inwazja/opowiesci/konspekty/160915-rekrutacja-mimo-woli.html)|

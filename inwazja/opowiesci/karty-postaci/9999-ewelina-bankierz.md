---
layout: inwazja-karta-postaci
categories: profile
title: "Ewelina Bankierz"
---
# {{ page.title }}

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Kryzysowo tymczasowy dyktator](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|dowodzi uzbrojonym helikopterem bojowym "Princessa Jeden" wraz z 6 żołnierzami.|Wizja Dukata|

## Plany

|Misja|Plan|Kampania|
|-----|------|------|
|[Kryzysowo tymczasowy dyktator](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|chce uwolnić się od łańcuchów Newerji przy użyciu Daniela Akwitańskiego oraz ma okazję uderzyć w Apoloniusza Bankierza. Na pewno to zrobi.|Wizja Dukata|
|[Epidemia Dezinhibicji](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|decyduje się wesprzeć Magitrownię Histogram jako nowa wspierająca siła (Raynor)|Wizja Dukata|

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|171115|przyleciała helikopterem bojowym (różowym) z grupą agentów (żołnierzy) i zabezpieczyła teren magitrowni chcąc pomóc Danielowi. Oddała siły Paulinie.|[Kryzysowo tymczasowy dyktator](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|11/10/03|11/10/04|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|170518|wyraźnie bawi się całą tą sytuacją jedynie wprowadzając komplikacje. Nie wiadomo, czego chce. Ale opiekuje się trochę Dianą. Nie lubi kuzyna (z wzajemnością)|[Machinacje maga rolniczego](/rpg/inwazja/opowiesci/konspekty/170518-machinacje-maga-rolniczego.html)|10/01/23|10/01/25|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|
|170515|bawiąca się życiem Diany, ale po swojemu dbająca o zdrowie i życie swojej zabawki. Otwarte stawanie jej czoła wydaje się głupie.|[Niewolnica w leasingu](/rpg/inwazja/opowiesci/konspekty/170515-niewolnica-w-leasingu.html)|10/01/20|10/01/22|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|
|170510|podobno całkiem wredna zołza, która bierze sobie na służki słabsze czarodziejki i steruje ich całym życiem.|[Najgorsze love story](/rpg/inwazja/opowiesci/konspekty/170510-najgorsze-love-story.html)|10/01/16|10/01/19|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|4|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html), [170518](/rpg/inwazja/opowiesci/konspekty/170518-machinacje-maga-rolniczego.html), [170515](/rpg/inwazja/opowiesci/konspekty/170515-niewolnica-w-leasingu.html), [170510](/rpg/inwazja/opowiesci/konspekty/170510-najgorsze-love-story.html)|
|[Hektor Reszniaczek](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-reszniaczek.html)|3|[170518](/rpg/inwazja/opowiesci/konspekty/170518-machinacje-maga-rolniczego.html), [170515](/rpg/inwazja/opowiesci/konspekty/170515-niewolnica-w-leasingu.html), [170510](/rpg/inwazja/opowiesci/konspekty/170510-najgorsze-love-story.html)|
|[Diana Łuczkiewicz](/rpg/inwazja/opowiesci/karty-postaci/9999-diana-luczkiewicz.html)|3|[170518](/rpg/inwazja/opowiesci/konspekty/170518-machinacje-maga-rolniczego.html), [170515](/rpg/inwazja/opowiesci/konspekty/170515-niewolnica-w-leasingu.html), [170510](/rpg/inwazja/opowiesci/konspekty/170510-najgorsze-love-story.html)|
|[Łucja Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-lucja-maus.html)|1|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|
|[Wiaczesław Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-wiaczeslaw-zajcew.html)|1|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|
|[Sylwester Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-sylwester-bankierz.html)|1|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|
|[Robert Sądeczny](/rpg/inwazja/opowiesci/karty-postaci/1709-robert-sadeczny.html)|1|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|
|[Michał Furczon](/rpg/inwazja/opowiesci/karty-postaci/9999-michal-furczon.html)|1|[170515](/rpg/inwazja/opowiesci/konspekty/170515-niewolnica-w-leasingu.html)|
|[Melodia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-melodia-diakon.html)|1|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|
|[Marcin Warinsky](/rpg/inwazja/opowiesci/karty-postaci/1709-marcin-warinsky.html)|1|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|
|[Konrad Paśnikowiec](/rpg/inwazja/opowiesci/karty-postaci/9999-konrad-pasnikowiec.html)|1|[170510](/rpg/inwazja/opowiesci/konspekty/170510-najgorsze-love-story.html)|
|[Kaja Maślaczek](/rpg/inwazja/opowiesci/karty-postaci/1709-kaja-maslaczek.html)|1|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|
|[Jakub Dobrocień](/rpg/inwazja/opowiesci/karty-postaci/1709-jakub-dobrocien.html)|1|[170518](/rpg/inwazja/opowiesci/konspekty/170518-machinacje-maga-rolniczego.html)|
|[Grazoniusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-grazoniusz-bankierz.html)|1|[170518](/rpg/inwazja/opowiesci/konspekty/170518-machinacje-maga-rolniczego.html)|
|[Efraim Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-efraim-weiner.html)|1|[170518](/rpg/inwazja/opowiesci/konspekty/170518-machinacje-maga-rolniczego.html)|
|[Daniel Akwitański](/rpg/inwazja/opowiesci/karty-postaci/1709-daniel-akwitanski.html)|1|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|
|[Apoloniusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-apoloniusz-bankierz.html)|1|[171115](/rpg/inwazja/opowiesci/konspekty/171115-kryzysowo-tymczasowy-dyktator.html)|
|[Aleksander Czykomar](/rpg/inwazja/opowiesci/karty-postaci/9999-aleksander-czykomar.html)|1|[170518](/rpg/inwazja/opowiesci/konspekty/170518-machinacje-maga-rolniczego.html)|

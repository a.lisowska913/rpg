---
layout: inwazja-karta-postaci
categories: profile
title: "Juliusz Jubilat"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|141210|kolekcjoner starych przedmiotów uznany za dość obleśnego przez Janinę.|[Złodzieje kielicha w akcji](/rpg/inwazja/opowiesci/konspekty/141210-zlodzieje-kielicha-w-akcji.html)|10/01/01|10/01/02|[Nie umieszczone, Anulowane](/rpg/inwazja/opowiesci/konspekty/kampania-anulowane.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Rafał Kielich](/rpg/inwazja/opowiesci/karty-postaci/9999-rafal-kielich.html)|1|[141210](/rpg/inwazja/opowiesci/konspekty/141210-zlodzieje-kielicha-w-akcji.html)|
|[Mariusz Błyszczyk](/rpg/inwazja/opowiesci/karty-postaci/9999-mariusz-blyszczyk.html)|1|[141210](/rpg/inwazja/opowiesci/konspekty/141210-zlodzieje-kielicha-w-akcji.html)|
|[Janina Kielich](/rpg/inwazja/opowiesci/karty-postaci/9999-janina-kielich.html)|1|[141210](/rpg/inwazja/opowiesci/konspekty/141210-zlodzieje-kielicha-w-akcji.html)|
|[Janek Łobuziak](/rpg/inwazja/opowiesci/karty-postaci/9999-janek-lobuziak.html)|1|[141210](/rpg/inwazja/opowiesci/konspekty/141210-zlodzieje-kielicha-w-akcji.html)|
|[Archibald Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-archibald-bankierz.html)|1|[141210](/rpg/inwazja/opowiesci/konspekty/141210-zlodzieje-kielicha-w-akcji.html)|
|[Anna Patyczek](/rpg/inwazja/opowiesci/karty-postaci/9999-anna-patyczek.html)|1|[141210](/rpg/inwazja/opowiesci/konspekty/141210-zlodzieje-kielicha-w-akcji.html)|
|[Aleksandra Pudryk](/rpg/inwazja/opowiesci/karty-postaci/9999-aleksandra-pudryk.html)|1|[141210](/rpg/inwazja/opowiesci/konspekty/141210-zlodzieje-kielicha-w-akcji.html)|
|[Adam Płatek](/rpg/inwazja/opowiesci/karty-postaci/9999-adam-platek.html)|1|[141210](/rpg/inwazja/opowiesci/konspekty/141210-zlodzieje-kielicha-w-akcji.html)|

---
layout: inwazja-karta-postaci
categories: profile
title: "Grzegorz Nadziejak"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|171004|odpowiedzialny za sensory w Magitrowni Histogram; niestety, przez wyciek Skażonej energii wpakował się i Skaził. Paulina postawiła go na nogi.|[Niestabilna magitrownia](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|11/09/01|11/09/03|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Zofia Przylga](/rpg/inwazja/opowiesci/karty-postaci/1709-zofia-przylga.html)|1|[171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Tomasz Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-tomasz-myszeczka.html)|1|[171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Robert Sądeczny](/rpg/inwazja/opowiesci/karty-postaci/1709-robert-sadeczny.html)|1|[171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|1|[171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Marcin Warinsky](/rpg/inwazja/opowiesci/karty-postaci/1709-marcin-warinsky.html)|1|[171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Karol Marzyciel](/rpg/inwazja/opowiesci/karty-postaci/1709-karol-marzyciel.html)|1|[171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Jakub Dobrocień](/rpg/inwazja/opowiesci/karty-postaci/1709-jakub-dobrocien.html)|1|[171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Efemeryda Senesgradzka](/rpg/inwazja/opowiesci/karty-postaci/9999-efemeryda-senesgradzka.html)|1|[171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|1|[171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Daniel Akwitański](/rpg/inwazja/opowiesci/karty-postaci/1709-daniel-akwitanski.html)|1|[171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|
|[Bogumił Miłoszept](/rpg/inwazja/opowiesci/karty-postaci/9999-bogumil-miloszept.html)|1|[171004](/rpg/inwazja/opowiesci/konspekty/171004-niestabilna-magitrownia.html)|

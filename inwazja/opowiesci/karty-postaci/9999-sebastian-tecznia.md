---
layout: inwazja-karta-postaci
categories: profile
title: "Sebastian Tecznia"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|141012|ten, który błaga "EAMkę" o jeszcze jedną szansę|[Aplikanci widzieli gorathaula](/rpg/inwazja/opowiesci/konspekty/141012-aplikanci-widzieli-gorathaula.html)|10/05/30|10/06/07|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|
|140928|kinderterminus w którym Paulina obudziła wyrzuty sumienia.|[Policjant widział anioła](/rpg/inwazja/opowiesci/konspekty/140928-policjant-widzial-aniola.html)|10/05/12|10/05/13|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|
|140708|który był w Bazie Detektywów podczas ataku Inwazji i został przechwycony przez Inwazję. MIA.|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|10/01/21|10/01/22|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|
|140618|siedzący w piwnicach bazy Andrei i Wacława. Okazuje się, że zna tajne przejście (backdoor) do Rezydencji Blakenbauerów... i przesłuchany powiedział je Andrei i Wacławowi.|[Upadek Agresta](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|10/01/17|10/01/18|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|
|150121|ciężko ranna ofiara nie kontrolującej się Aliny w formie bojowej (gdy Alina odkrywała swoją nową naturę).|[Nowe życie Aliny](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|10/01/15|10/01/16|[Blakenbauerowie x Skorpion](/rpg/inwazja/opowiesci/konspekty/kampania-blakenbauerowie-x-skorpion.html)|
|140604|osoba nieprzytomna w Bazie Detektywów, której jednak szuka zarówno Agrest jak i Czerwiec z niewiadomych przyczyn.|[Patriarcha Blakenbauer](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|10/01/13|10/01/14|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|
|141216|potencjalny mastermind i kidnaper z nadmiarem wpływów u Diakonów.|[Zabili mu syna](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|10/01/11|10/01/12|[Blakenbauerowie x Skorpion](/rpg/inwazja/opowiesci/konspekty/kampania-blakenbauerowie-x-skorpion.html)|
|140401|który wraz z Rebeką porwał Elżbietę Niemoc i otruł Edwina; potem gdy Edwin przeszedł w formę Bestii, uciekł i uratował go (oraz złapał do niewoli) Zespół.|[Mojra, Moriath](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|10/01/11|10/01/12|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|
|140109|uczeń Moriatha (materia + energia), śmiertelnie w Rebece zakochany i chcący ją ratować.|[Uczniowie Moriatha](/rpg/inwazja/opowiesci/konspekty/140109-uczniowie-moriatha.html)|10/01/05|10/01/06|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Wacław Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-waclaw-zajcew.html)|5|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html), [140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140109](/rpg/inwazja/opowiesci/konspekty/140109-uczniowie-moriatha.html)|
|[Mojra](/rpg/inwazja/opowiesci/karty-postaci/9999-mojra.html)|5|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html), [141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140109](/rpg/inwazja/opowiesci/konspekty/140109-uczniowie-moriatha.html)|
|[Karolina Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-karolina-maus.html)|5|[141012](/rpg/inwazja/opowiesci/konspekty/141012-aplikanci-widzieli-gorathaula.html), [140928](/rpg/inwazja/opowiesci/konspekty/140928-policjant-widzial-aniola.html), [140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|5|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html), [140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html), [141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Andrea Wilgacz](/rpg/inwazja/opowiesci/karty-postaci/1709-andrea-wilgacz.html)|5|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html), [140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140109](/rpg/inwazja/opowiesci/konspekty/140109-uczniowie-moriatha.html)|
|[Teresa Żyraf](/rpg/inwazja/opowiesci/karty-postaci/9999-teresa-zyraf.html)|4|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html), [140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Marian Agrest](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-agrest.html)|4|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140109](/rpg/inwazja/opowiesci/konspekty/140109-uczniowie-moriatha.html)|
|[Marcelin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-marcelin-blakenbauer.html)|4|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html), [140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Grzegorz Czerwiec](/rpg/inwazja/opowiesci/karty-postaci/9999-grzegorz-czerwiec.html)|4|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html), [140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|4|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html), [140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Waldemar Zupaczka](/rpg/inwazja/opowiesci/karty-postaci/9999-waldemar-zupaczka.html)|3|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140109](/rpg/inwazja/opowiesci/konspekty/140109-uczniowie-moriatha.html)|
|[Rebeka Piryt](/rpg/inwazja/opowiesci/karty-postaci/9999-rebeka-piryt.html)|3|[141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html), [140109](/rpg/inwazja/opowiesci/konspekty/140109-uczniowie-moriatha.html)|
|[Otton Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-otton-blakenbauer.html)|3|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Margaret Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-margaret-blakenbauer.html)|3|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Krystalia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-krystalia-diakon.html)|3|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Irina Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-irina-zajcew.html)|3|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html), [140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Estera Piryt](/rpg/inwazja/opowiesci/karty-postaci/9999-estera-piryt.html)|3|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html), [140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Whisperwind](/rpg/inwazja/opowiesci/karty-postaci/9999-whisperwind.html)|2|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html), [140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Quasar](/rpg/inwazja/opowiesci/karty-postaci/9999-quasar.html)|2|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html), [140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|2|[141012](/rpg/inwazja/opowiesci/konspekty/141012-aplikanci-widzieli-gorathaula.html), [140928](/rpg/inwazja/opowiesci/konspekty/140928-policjant-widzial-aniola.html)|
|[Nikola Kamień](/rpg/inwazja/opowiesci/karty-postaci/9999-nikola-kamien.html)|2|[141012](/rpg/inwazja/opowiesci/konspekty/141012-aplikanci-widzieli-gorathaula.html), [140928](/rpg/inwazja/opowiesci/konspekty/140928-policjant-widzial-aniola.html)|
|[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/1709-maria-newa.html)|2|[141012](/rpg/inwazja/opowiesci/konspekty/141012-aplikanci-widzieli-gorathaula.html), [140928](/rpg/inwazja/opowiesci/konspekty/140928-policjant-widzial-aniola.html)|
|[Maja Kos](/rpg/inwazja/opowiesci/karty-postaci/9999-maja-kos.html)|2|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Klemens X](/rpg/inwazja/opowiesci/karty-postaci/9999-klemens-x.html)|2|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html), [141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|[Ika](/rpg/inwazja/opowiesci/karty-postaci/9999-ika.html)|2|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Elżbieta Niemoc](/rpg/inwazja/opowiesci/karty-postaci/9999-elzbieta-niemoc.html)|2|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|2|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html), [140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Dariusz Kopyto](/rpg/inwazja/opowiesci/karty-postaci/9999-dariusz-kopyto.html)|2|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Amelia Eter](/rpg/inwazja/opowiesci/karty-postaci/9999-amelia-eter.html)|2|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html), [140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Alina Bednarz](/rpg/inwazja/opowiesci/karty-postaci/1803-alina-bednarz.html)|2|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html), [141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|[Yakim Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-yakim-zajcew.html)|1|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Wojmił Kopiec](/rpg/inwazja/opowiesci/karty-postaci/9999-wojmil-kopiec.html)|1|[141012](/rpg/inwazja/opowiesci/konspekty/141012-aplikanci-widzieli-gorathaula.html)|
|[Tatiana Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-tatiana-zajcew.html)|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Tadeusz Czerwiecki](/rpg/inwazja/opowiesci/karty-postaci/9999-tadeusz-czerwiecki.html)|1|[140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Swietłana Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-swietlana-zajcew.html)|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Salazar Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-salazar-bankierz.html)|1|[141012](/rpg/inwazja/opowiesci/konspekty/141012-aplikanci-widzieli-gorathaula.html)|
|[Saith Flamecaller](/rpg/inwazja/opowiesci/karty-postaci/9999-saith-flamecaller.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Saith Catapult](/rpg/inwazja/opowiesci/karty-postaci/9999-saith-catapult.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Robert Przerot](/rpg/inwazja/opowiesci/karty-postaci/9999-robert-przerot.html)|1|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Przemysław Marchewka](/rpg/inwazja/opowiesci/karty-postaci/9999-przemyslaw-marchewka.html)|1|[141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|[Olga Miodownik](/rpg/inwazja/opowiesci/karty-postaci/1709-olga-miodownik.html)|1|[141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|[Netheria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-netheria-diakon.html)|1|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Mieszko Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-mieszko-bankierz.html)|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Mateusz Nieborak](/rpg/inwazja/opowiesci/karty-postaci/9999-mateusz-nieborak.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Marysia Kiras](/rpg/inwazja/opowiesci/karty-postaci/9999-marysia-kiras.html)|1|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Marta Newa](/rpg/inwazja/opowiesci/karty-postaci/9999-marta-newa.html)|1|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Malwina Krówka](/rpg/inwazja/opowiesci/karty-postaci/9999-malwina-krowka.html)|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Luiza Wanta](/rpg/inwazja/opowiesci/karty-postaci/9999-luiza-wanta.html)|1|[141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|[Lenart Stosal](/rpg/inwazja/opowiesci/karty-postaci/9999-lenart-stosal.html)|1|[140109](/rpg/inwazja/opowiesci/konspekty/140109-uczniowie-moriatha.html)|
|[Konrad Węgorz](/rpg/inwazja/opowiesci/karty-postaci/9999-konrad-wegorz.html)|1|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Karol Poczciwiec](/rpg/inwazja/opowiesci/karty-postaci/9999-karol-poczciwiec.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Joachim Kopiec](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-kopiec.html)|1|[141012](/rpg/inwazja/opowiesci/konspekty/141012-aplikanci-widzieli-gorathaula.html)|
|[Jan Szczupak](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-szczupak.html)|1|[140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Ilarion Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-ilarion-zajcew.html)|1|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Gustaw Siedeł](/rpg/inwazja/opowiesci/karty-postaci/9999-gustaw-siedel.html)|1|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Filip Sztukar](/rpg/inwazja/opowiesci/karty-postaci/9999-filip-sztukar.html)|1|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Ewa Kroideł](/rpg/inwazja/opowiesci/karty-postaci/9999-ewa-kroidel.html)|1|[141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|[Edmund Bakłażan](/rpg/inwazja/opowiesci/karty-postaci/9999-edmund-baklazan.html)|1|[140928](/rpg/inwazja/opowiesci/konspekty/140928-policjant-widzial-aniola.html)|
|[Draconis Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-draconis-diakon.html)|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Dominik Marchewka](/rpg/inwazja/opowiesci/karty-postaci/9999-dominik-marchewka.html)|1|[141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|[Cezary Piryt](/rpg/inwazja/opowiesci/karty-postaci/9999-cezary-piryt.html)|1|[140109](/rpg/inwazja/opowiesci/konspekty/140109-uczniowie-moriatha.html)|
|[Brunon Czerpak](/rpg/inwazja/opowiesci/karty-postaci/9999-brunon-czerpak.html)|1|[141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|[Borys Kumin](/rpg/inwazja/opowiesci/karty-postaci/1709-borys-kumin.html)|1|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Benjamin Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-benjamin-zajcew.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Artur Żupan](/rpg/inwazja/opowiesci/karty-postaci/1803-artur-zupan.html)|1|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Artur Kurczak](/rpg/inwazja/opowiesci/karty-postaci/9999-artur-kurczak.html)|1|[140928](/rpg/inwazja/opowiesci/konspekty/140928-policjant-widzial-aniola.html)|
|[Antoni Szczęśliwiec](/rpg/inwazja/opowiesci/karty-postaci/9999-antoni-szczesliwiec.html)|1|[141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|[Antoni Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/9999-antoni-myszeczka.html)|1|[140109](/rpg/inwazja/opowiesci/konspekty/140109-uczniowie-moriatha.html)|
|[Anna Kozak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kozak.html)|1|[141012](/rpg/inwazja/opowiesci/konspekty/141012-aplikanci-widzieli-gorathaula.html)|
|[Anna Kajak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kajak.html)|1|[141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|
|[Andrzej Sowiński](/rpg/inwazja/opowiesci/karty-postaci/9999-andrzej-sowinski.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Amanda Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-amanda-diakon.html)|1|[140708](/rpg/inwazja/opowiesci/konspekty/140708-druga-inwazja.html)|
|[Alfred Kukułka](/rpg/inwazja/opowiesci/karty-postaci/9999-alfred-kukulka.html)|1|[140401](/rpg/inwazja/opowiesci/konspekty/140401-mojra-moriath.html)|
|[Adam Bożynów](/rpg/inwazja/opowiesci/karty-postaci/9999-adam-bozynow.html)|1|[141216](/rpg/inwazja/opowiesci/konspekty/141216-zabili-mu-syna.html)|

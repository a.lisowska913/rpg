---
layout: inwazja-karta-postaci
categories: profile
factions: "Niezrzeszeni"
type: "NPC"
title: "Hubert Brodacz"
---
# {{ page.title }}

## Postać

### Motywacje (do czego dąży)

* **FIL: Cwaniak**:
    * _Aspekty_: 
    * _Opis_: na pewno da się to jakoś załatwić
* **BÓL:**:
    * _Aspekty_: 
    * _Opis_: 
* **MRZ:**:
    * _Aspekty_: 
    * _Opis_: 
* **MET:**:
    * _Aspekty_: 
    * _Opis_: 
* **KLT: Opiekuńczy**:
    * _Aspekty_: porywczy
    * _Opis_: ludzie pod jego ochroną są nietykalni

### Umiejętności

* **Śledczy**:
    * _Aspekty_: zbieranie dowodów, jak żyć z Hektorem, zastraszanie, nakłanianie do współpracy
    * _Opis_: 
* **Policjant**:
    * _Aspekty_: zastraszanie, pierwsza pomoc, kierowca, nakłanianie do współpracy
    * _Opis_: 
* **Manipulator**:
    * _Aspekty_: aktor, sztuczna osobowość
    * _Opis_: 
* **??**:
    * _Aspekty_: 
    * _Opis_: 
    
### Silne i słabe strony:

* **??**:
    * _Aspekty_: 
    * _Opis_: 
	
## Magia

* **brak**

## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Zarobki


### Znam

* **Artur Bryś**:
    * _Aspekty_: 
    * _Opis_: 
* **lokalne mendy społeczne**:
    * _Aspekty_: 
    * _Opis_: 
* **ludzie z komisariatu**:
    * _Aspekty_: 
    * _Opis_:
	
### Mam

* **zabawki "pożyczone" z grupy śledczej**:
    * _Aspekty_: 
    * _Opis_: 
* **wyposażenie dzielnicowego**:
    * _Aspekty_: 
    * _Opis_: 


# Opis

ex-członek grupy śledczej z Kopalina

Ex-członek grupy śledczej pracującej dla Hektora Blakenbauera. Wyrzucony za cień podejrzenia o manipulację dowodami, czego jednak nie robił. Wylądował na ulicy… znaczy się przeniesiono go do zwykłych patroli osiedlowych. Szybko adaptował się do nowego środowiska. Nawiązał kontakty z lokalnymi szefami i "półświatkiem". Oferuje im ochronę w zamian za pomoc w zgarnianiu oprychów spoza "dzielni". Nie jest może do końca praworządny, ale opiekuje się ludźmi w swoim rewirze.

### Koncept

Wysokopoziomowy koncept postaci, 1-2 zdania.

### Motto

"Tekst"

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Wandy wolność i wróżda](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|jest wolny od Skubnego (spłacił dług)|Taniec Liści|


## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|160921|policjant, eks-siły Hektora, który potrafi pobić anarchistę by potem się z nim zaprzyjaźnić; jest bezwzględny. Spłacił dług u Skubnego.|[Wandy wolność i wróżda](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|10/07/22|10/07/24|[Taniec Liści](/rpg/inwazja/opowiesci/konspekty/kampania-taniec-lisci.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Zdzisław Kamiński](/rpg/inwazja/opowiesci/karty-postaci/1709-zdzislaw-kaminski.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Wanda Ketran](/rpg/inwazja/opowiesci/karty-postaci/1709-wanda-ketran.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Tymotheus Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-tymotheus-blakenbauer.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Tomasz Kracy](/rpg/inwazja/opowiesci/karty-postaci/9999-tomasz-kracy.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Szymon Skubny](/rpg/inwazja/opowiesci/karty-postaci/9999-szymon-skubny.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Stanisław Pormien](/rpg/inwazja/opowiesci/karty-postaci/9999-stanislaw-pormien.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Milena Marzec](/rpg/inwazja/opowiesci/karty-postaci/9999-milena-marzec.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Marian Rokita](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-rokita.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Margaret Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-margaret-blakenbauer.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Leszek Żółty](/rpg/inwazja/opowiesci/karty-postaci/9999-leszek-zolty.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Jan Szczupak](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-szczupak.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Iliusitius](/rpg/inwazja/opowiesci/karty-postaci/9999-iliusitius.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Grzegorz Włóczykij](/rpg/inwazja/opowiesci/karty-postaci/9999-grzegorz-wloczykij.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Grażyna Remska](/rpg/inwazja/opowiesci/karty-postaci/9999-grazyna-remska.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Diana Larent](/rpg/inwazja/opowiesci/karty-postaci/9999-diana-larent.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Czesław Czepiec](/rpg/inwazja/opowiesci/karty-postaci/9999-czeslaw-czepiec.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|
|[Anna Góra](/rpg/inwazja/opowiesci/karty-postaci/9999-anna-gora.html)|1|[160921](/rpg/inwazja/opowiesci/konspekty/160921-wandy-wolnosc-i-wrozda.html)|

---
layout: inwazja-karta-postaci
categories: profile
title: "Ewa Czuk"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|141220|właścicielka domu w którym zamieszkał Zespół i matka fajnego syna.|[Bogini Marzeń w Żonkiborze](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|10/02/09|10/02/10|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Wojciech Kajak](/rpg/inwazja/opowiesci/karty-postaci/9999-wojciech-kajak.html)|1|[141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|
|[Sandra Stryjek](/rpg/inwazja/opowiesci/karty-postaci/1709-sandra-stryjek.html)|1|[141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|
|[Samira Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-samira-diakon.html)|1|[141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|
|[Michał Czuk](/rpg/inwazja/opowiesci/karty-postaci/9999-michal-czuk.html)|1|[141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|
|[Małgorzata Poran](/rpg/inwazja/opowiesci/karty-postaci/9999-malgorzata-poran.html)|1|[141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|
|[Kasia Nowak](/rpg/inwazja/opowiesci/karty-postaci/1803-kasia-nowak.html)|1|[141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|
|[Józef Pimczak](/rpg/inwazja/opowiesci/karty-postaci/9999-jozef-pimczak.html)|1|[141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|
|[Herbert Zioło](/rpg/inwazja/opowiesci/karty-postaci/1709-herbert-ziolo.html)|1|[141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|
|[Hektor Poran](/rpg/inwazja/opowiesci/karty-postaci/9999-hektor-poran.html)|1|[141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|
|[Anna Kajak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kajak.html)|1|[141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|
|[Andrzej Domowierzec](/rpg/inwazja/opowiesci/karty-postaci/9999-andrzej-domowierzec.html)|1|[141220](/rpg/inwazja/opowiesci/konspekty/141220-bogini-marzen-w-zonkiborze.html)|

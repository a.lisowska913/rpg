---
layout: inwazja-karta-postaci
categories: profile
title: "Marta Newa"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|160303|która jako jedyna Zeta przetrwała pogrom z ręki Ottona. Współpracuje z Arazille. |[Otton zabija Zetę](/rpg/inwazja/opowiesci/konspekty/160303-otton-zabija-zete.html)|10/06/23|10/06/24|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|151216|oddział Zeta; otrzymała efemerydę siostry i z jej pomocą znajduje nienaturalne wzory w mieście. Niebezpieczna w walce wręcz.|[Między prawdą i fikcją Arazille](/rpg/inwazja/opowiesci/konspekty/151216-miedzy-prawda-i-fikcja-arazille.html)|10/06/21|10/06/22|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|150121|czarodziejka której szuka Konrad Węgorz z Millennium, przez lustra.|[Nowe życie Aliny](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|10/01/15|10/01/16|[Blakenbauerowie x Skorpion](/rpg/inwazja/opowiesci/konspekty/kampania-blakenbauerowie-x-skorpion.html)|
|140910|echo w pamięci Marii; podobno zniewolona czarodziejka której robią straszne rzeczy Blakenbauerowie.|[Reporter kontra Blakenbauerzy](/rpg/inwazja/opowiesci/konspekty/140910-reporter-kontra-blakenbauerzy.html)|10/01/01|10/01/02|[Nie umieszczone, Anulowane](/rpg/inwazja/opowiesci/konspekty/kampania-anulowane.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Margaret Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-margaret-blakenbauer.html)|3|[160303](/rpg/inwazja/opowiesci/konspekty/160303-otton-zabija-zete.html), [150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html), [140910](/rpg/inwazja/opowiesci/konspekty/140910-reporter-kontra-blakenbauerzy.html)|
|[Alina Bednarz](/rpg/inwazja/opowiesci/karty-postaci/1803-alina-bednarz.html)|3|[160303](/rpg/inwazja/opowiesci/konspekty/160303-otton-zabija-zete.html), [151216](/rpg/inwazja/opowiesci/konspekty/151216-miedzy-prawda-i-fikcja-arazille.html), [150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Romuald Zeta](/rpg/inwazja/opowiesci/karty-postaci/9999-romuald-zeta.html)|2|[160303](/rpg/inwazja/opowiesci/konspekty/160303-otton-zabija-zete.html), [151216](/rpg/inwazja/opowiesci/konspekty/151216-miedzy-prawda-i-fikcja-arazille.html)|
|[Otton Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-otton-blakenbauer.html)|2|[160303](/rpg/inwazja/opowiesci/konspekty/160303-otton-zabija-zete.html), [140910](/rpg/inwazja/opowiesci/konspekty/140910-reporter-kontra-blakenbauerzy.html)|
|[Klemens X](/rpg/inwazja/opowiesci/karty-postaci/9999-klemens-x.html)|2|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html), [140910](/rpg/inwazja/opowiesci/konspekty/140910-reporter-kontra-blakenbauerzy.html)|
|[Kamila Zeta](/rpg/inwazja/opowiesci/karty-postaci/9999-kamila-zeta.html)|2|[160303](/rpg/inwazja/opowiesci/konspekty/160303-otton-zabija-zete.html), [151216](/rpg/inwazja/opowiesci/konspekty/151216-miedzy-prawda-i-fikcja-arazille.html)|
|[Jędrzej Zeta](/rpg/inwazja/opowiesci/karty-postaci/9999-jedrzej-zeta.html)|2|[160303](/rpg/inwazja/opowiesci/konspekty/160303-otton-zabija-zete.html), [151216](/rpg/inwazja/opowiesci/konspekty/151216-miedzy-prawda-i-fikcja-arazille.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|2|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html), [140910](/rpg/inwazja/opowiesci/konspekty/140910-reporter-kontra-blakenbauerzy.html)|
|[Dionizy Kret](/rpg/inwazja/opowiesci/karty-postaci/1709-dionizy-kret.html)|2|[160303](/rpg/inwazja/opowiesci/konspekty/160303-otton-zabija-zete.html), [151216](/rpg/inwazja/opowiesci/konspekty/151216-miedzy-prawda-i-fikcja-arazille.html)|
|[Borys Kumin](/rpg/inwazja/opowiesci/karty-postaci/1709-borys-kumin.html)|2|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html), [140910](/rpg/inwazja/opowiesci/konspekty/140910-reporter-kontra-blakenbauerzy.html)|
|[Arazille](/rpg/inwazja/opowiesci/karty-postaci/9999-arazille.html)|2|[160303](/rpg/inwazja/opowiesci/konspekty/160303-otton-zabija-zete.html), [151216](/rpg/inwazja/opowiesci/konspekty/151216-miedzy-prawda-i-fikcja-arazille.html)|
|[Smok](/rpg/inwazja/opowiesci/karty-postaci/9999-smok.html)|1|[140910](/rpg/inwazja/opowiesci/konspekty/140910-reporter-kontra-blakenbauerzy.html)|
|[Sebastian Tecznia](/rpg/inwazja/opowiesci/karty-postaci/9999-sebastian-tecznia.html)|1|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Robert Przerot](/rpg/inwazja/opowiesci/karty-postaci/9999-robert-przerot.html)|1|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|1|[140910](/rpg/inwazja/opowiesci/konspekty/140910-reporter-kontra-blakenbauerzy.html)|
|[Olga Miodownik](/rpg/inwazja/opowiesci/karty-postaci/1709-olga-miodownik.html)|1|[160303](/rpg/inwazja/opowiesci/konspekty/160303-otton-zabija-zete.html)|
|[Oddział Zeta](/rpg/inwazja/opowiesci/karty-postaci/9999-oddzial-zeta.html)|1|[151216](/rpg/inwazja/opowiesci/konspekty/151216-miedzy-prawda-i-fikcja-arazille.html)|
|[Netheria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-netheria-diakon.html)|1|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Mojra](/rpg/inwazja/opowiesci/karty-postaci/9999-mojra.html)|1|[140910](/rpg/inwazja/opowiesci/konspekty/140910-reporter-kontra-blakenbauerzy.html)|
|[Marysia Kiras](/rpg/inwazja/opowiesci/karty-postaci/9999-marysia-kiras.html)|1|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/1709-maria-newa.html)|1|[140910](/rpg/inwazja/opowiesci/konspekty/140910-reporter-kontra-blakenbauerzy.html)|
|[Laura Filut](/rpg/inwazja/opowiesci/karty-postaci/9999-laura-filut.html)|1|[140910](/rpg/inwazja/opowiesci/konspekty/140910-reporter-kontra-blakenbauerzy.html)|
|[Konrad Węgorz](/rpg/inwazja/opowiesci/karty-postaci/9999-konrad-wegorz.html)|1|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Kleofas Bór](/rpg/inwazja/opowiesci/karty-postaci/1709-kleofas-bor.html)|1|[160303](/rpg/inwazja/opowiesci/konspekty/160303-otton-zabija-zete.html)|
|[Kinga Melit](/rpg/inwazja/opowiesci/karty-postaci/9999-kinga-melit.html)|1|[140910](/rpg/inwazja/opowiesci/konspekty/140910-reporter-kontra-blakenbauerzy.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|1|[140910](/rpg/inwazja/opowiesci/konspekty/140910-reporter-kontra-blakenbauerzy.html)|
|[Filip Sztukar](/rpg/inwazja/opowiesci/karty-postaci/9999-filip-sztukar.html)|1|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Artur Żupan](/rpg/inwazja/opowiesci/karty-postaci/1803-artur-zupan.html)|1|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|

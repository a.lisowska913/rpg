---
layout: inwazja-karta-postaci
categories: profile
title: "Bolesław Złodrak"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|171105|szef militarny u Tomasza Myszeczki. Twardy były wojskowy. Współpracuje z Pauliną i próbuje jakoś osłonić przed nią Szczepana (brata).|[Epidemia Dezinhibicji](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|11/10/01|11/10/02|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Wiaczesław Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-wiaczeslaw-zajcew.html)|1|[171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|
|[Tomasz Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-tomasz-myszeczka.html)|1|[171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|
|[Szczepan Złodrak](/rpg/inwazja/opowiesci/karty-postaci/9999-szczepan-zlodrak.html)|1|[171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|
|[Sylwester Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-sylwester-bankierz.html)|1|[171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|1|[171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|
|[Oliwia Aurinus](/rpg/inwazja/opowiesci/karty-postaci/1709-oliwia-aurinus.html)|1|[171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|
|[Karol Marzyciel](/rpg/inwazja/opowiesci/karty-postaci/1709-karol-marzyciel.html)|1|[171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|
|[Kaja Maślaczek](/rpg/inwazja/opowiesci/karty-postaci/1709-kaja-maslaczek.html)|1|[171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|1|[171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|
|[Daniel Akwitański](/rpg/inwazja/opowiesci/karty-postaci/1709-daniel-akwitanski.html)|1|[171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|
|[Dalia Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-dalia-weiner.html)|1|[171105](/rpg/inwazja/opowiesci/konspekty/171105-epidemia-dezinhibicji.html)|

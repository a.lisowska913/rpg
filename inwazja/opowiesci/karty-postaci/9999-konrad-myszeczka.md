---
layout: inwazja-karta-postaci
categories: profile
title: "Konrad Myszeczka"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|161102|badacz i hodowca, Esuriit; zaprojektował z Zofią czerwie trzymające Ekspedycję przy życiu na tej fazie. |[Magowie Esuriit w domu](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html)|10/07/26|10/07/28|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Łukija Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-lukija-zajcew.html)|1|[161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html)|
|[Zofia Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-zofia-weiner.html)|1|[161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html)|
|[Siriratharin](/rpg/inwazja/opowiesci/karty-postaci/1709-siriratharin.html)|1|[161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html)|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|1|[161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html)|
|[Paweł Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-pawel-maus.html)|1|[161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html)|
|[Marianna Sowińska](/rpg/inwazja/opowiesci/karty-postaci/1709-marianna-sowinska.html)|1|[161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html)|
|[Konstanty Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-konstanty-bankierz.html)|1|[161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html)|
|[Jakub Pestka](/rpg/inwazja/opowiesci/karty-postaci/9999-jakub-pestka.html)|1|[161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|1|[161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html)|
|[Fiodor Maius Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-fiodor-maius-zajcew.html)|1|[161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html)|
|[Dosifiej Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-dosifiej-zajcew.html)|1|[161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html)|
|[Bazyli Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-bazyli-weiner.html)|1|[161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html)|
|[Anna Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-myszeczka.html)|1|[161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html)|
|[Andżelika Leszczyńska](/rpg/inwazja/opowiesci/karty-postaci/1709-andzelika-leszczynska.html)|1|[161102](/rpg/inwazja/opowiesci/konspekty/161102-magowie-esuriit-w-domu.html)|

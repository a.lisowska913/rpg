---
layout: inwazja-karta-postaci
categories: profile
title: "Rafał Szczęślik"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|161103|policjant. Aresztował Henryka na jedną kluczową noc po podszeptach Jessiki. Przez niego Henryka nie było gdy Nicaretta się inkarnowała.|[Egzorcyzmy w Żonkiborze](/rpg/inwazja/opowiesci/konspekty/161103-egzorcyzmy-w-zonkiborze.html)|10/07/09|10/07/12|[Nicaretta](/rpg/inwazja/opowiesci/konspekty/kampania-nicaretta.html)|
|141230|policjant. Dowodzi grupą. Znalazł twarde narkotyki... a przynajmniej tak myśli.|[Ofiara z wampira dla Arazille](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html)|10/02/13|10/02/14|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|
|141227|policjant. Dowodzi grupą. Choć jest wielce nieszczęśliwy że tu jest.|[Przyczajona Andromeda, ukryty Maus](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|10/02/11|10/02/12|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Wojciech Kajak](/rpg/inwazja/opowiesci/karty-postaci/9999-wojciech-kajak.html)|2|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Sandra Stryjek](/rpg/inwazja/opowiesci/karty-postaci/1709-sandra-stryjek.html)|2|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Samira Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-samira-diakon.html)|2|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Netheria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-netheria-diakon.html)|2|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Kasia Nowak](/rpg/inwazja/opowiesci/karty-postaci/1803-kasia-nowak.html)|2|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Karradrael](/rpg/inwazja/opowiesci/karty-postaci/9999-karradrael.html)|2|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Iliusitius](/rpg/inwazja/opowiesci/karty-postaci/9999-iliusitius.html)|2|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Herbert Zioło](/rpg/inwazja/opowiesci/karty-postaci/1709-herbert-ziolo.html)|2|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Feliks Hanson](/rpg/inwazja/opowiesci/karty-postaci/9999-feliks-hanson.html)|2|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Arazille](/rpg/inwazja/opowiesci/karty-postaci/9999-arazille.html)|2|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Anna Kajak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kajak.html)|2|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Aneta Hanson](/rpg/inwazja/opowiesci/karty-postaci/9999-aneta-hanson.html)|2|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Andrzej Szop](/rpg/inwazja/opowiesci/karty-postaci/9999-andrzej-szop.html)|2|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html), [141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Szymon Skubny](/rpg/inwazja/opowiesci/karty-postaci/9999-szymon-skubny.html)|1|[141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Szczepan Sławski](/rpg/inwazja/opowiesci/karty-postaci/9999-szczepan-slawski.html)|1|[161103](/rpg/inwazja/opowiesci/konspekty/161103-egzorcyzmy-w-zonkiborze.html)|
|[Robert Przerot](/rpg/inwazja/opowiesci/karty-postaci/9999-robert-przerot.html)|1|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html)|
|[Patryk Romczak](/rpg/inwazja/opowiesci/karty-postaci/9999-patryk-romczak.html)|1|[141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Nicaretta](/rpg/inwazja/opowiesci/karty-postaci/1709-nicaretta.html)|1|[161103](/rpg/inwazja/opowiesci/konspekty/161103-egzorcyzmy-w-zonkiborze.html)|
|[Michał Czuk](/rpg/inwazja/opowiesci/karty-postaci/9999-michal-czuk.html)|1|[141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Małgorzata Poran](/rpg/inwazja/opowiesci/karty-postaci/9999-malgorzata-poran.html)|1|[141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Marzena Gilek](/rpg/inwazja/opowiesci/karty-postaci/9999-marzena-gilek.html)|1|[161103](/rpg/inwazja/opowiesci/konspekty/161103-egzorcyzmy-w-zonkiborze.html)|
|[Maciej Dworek](/rpg/inwazja/opowiesci/karty-postaci/9999-maciej-dworek.html)|1|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html)|
|[Luksja Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-luksja-diakon.html)|1|[141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Luiza Wanta](/rpg/inwazja/opowiesci/karty-postaci/9999-luiza-wanta.html)|1|[141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Józef Pimczak](/rpg/inwazja/opowiesci/karty-postaci/9999-jozef-pimczak.html)|1|[141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Jessika Gniewoń](/rpg/inwazja/opowiesci/karty-postaci/9999-jessika-gniewon.html)|1|[161103](/rpg/inwazja/opowiesci/konspekty/161103-egzorcyzmy-w-zonkiborze.html)|
|[Henryk Siwiecki](/rpg/inwazja/opowiesci/karty-postaci/1709-henryk-siwiecki.html)|1|[161103](/rpg/inwazja/opowiesci/konspekty/161103-egzorcyzmy-w-zonkiborze.html)|
|[Gabriel Newa](/rpg/inwazja/opowiesci/karty-postaci/9999-gabriel-newa.html)|1|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html)|
|[Filip Sztukar](/rpg/inwazja/opowiesci/karty-postaci/9999-filip-sztukar.html)|1|[141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|
|[Filip Gładki](/rpg/inwazja/opowiesci/karty-postaci/9999-filip-gladki.html)|1|[161103](/rpg/inwazja/opowiesci/konspekty/161103-egzorcyzmy-w-zonkiborze.html)|
|[Feliks Bozur](/rpg/inwazja/opowiesci/karty-postaci/9999-feliks-bozur.html)|1|[141230](/rpg/inwazja/opowiesci/konspekty/141230-ofiara-z-wampira-dla-arazille.html)|
|[Balbina Gniewoń](/rpg/inwazja/opowiesci/karty-postaci/9999-balbina-gniewon.html)|1|[161103](/rpg/inwazja/opowiesci/konspekty/161103-egzorcyzmy-w-zonkiborze.html)|
|[Artur Szmelc](/rpg/inwazja/opowiesci/karty-postaci/9999-artur-szmelc.html)|1|[141227](/rpg/inwazja/opowiesci/konspekty/141227-przyczajona-andromeda-ukryty-maus.html)|

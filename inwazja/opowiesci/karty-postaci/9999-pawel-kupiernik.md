---
layout: inwazja-karta-postaci
categories: profile
title: "Paweł Kupiernik"
---
# {{ page.title }}

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Chrumpokalipsa](/rpg/inwazja/opowiesci/konspekty/180112-chrumpokalipsa.html)|nie pamięta o wszystkich tych świniokształtnych tematach|Wizja Dukata|
|[Chrumpokalipsa](/rpg/inwazja/opowiesci/konspekty/180112-chrumpokalipsa.html)|w oczach Łowców Dziwnych Świń jest bohaterem i męczennikiem|Wizja Dukata|
|[Świnia na portalisku](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|gościu od znikających świń ma przydupasa, którego misją jest pomóc mu udowodnić to wszystko (Prezes).|Wizja Dukata|
|[Świnia na portalisku](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|zakłada grupę polującą na znikające świnie; próbuje zrozumieć co tu się dzieje i zbierać informacje z terenu.|Wizja Dukata|

## Plany

|Misja|Plan|Kampania|
|-----|------|------|
|[Świnia na portalisku](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|gościu od znikających świń nie może tego tak zostawić. JEGO MISJĄ JEST TO UDOWODNIĆ! (Prezes)|Wizja Dukata|

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|180112|planował egzorcyzmowanie świń, wpadł w ręce uczniów Świecy i wymazano mu pamięć.|[Chrumpokalipsa](/rpg/inwazja/opowiesci/konspekty/180112-chrumpokalipsa.html)|11/10/16|11/10/18|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|180104|chodził po okolicy, szukał oczkodzika i wzmacniał Łowców Dziwnych Świń o harcerzy.|[Wspaniały Wieprz Wojtka](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|11/10/12|11/10/15|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171003|właściciel gospodarstwa w Męczymordach; jest na osobistej krucjacie udowodnienia, że ma 8 świń, ale dziewiąta zniknęła! Przekonany przez żonę wałkiem.|[Świnia na portalisku](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|11/08/28|11/08/30|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Stefania Kołek](/rpg/inwazja/opowiesci/karty-postaci/9999-stefania-kolek.html)|2|[180112](/rpg/inwazja/opowiesci/konspekty/180112-chrumpokalipsa.html), [180104](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|
|[Kinga Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1803-kinga-bankierz.html)|2|[180112](/rpg/inwazja/opowiesci/konspekty/180112-chrumpokalipsa.html), [180104](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|
|[Anatol Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1803-anatol-sowinski.html)|2|[180112](/rpg/inwazja/opowiesci/konspekty/180112-chrumpokalipsa.html), [180104](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|
|[Wojciech Piekarz](/rpg/inwazja/opowiesci/karty-postaci/9999-wojciech-piekarz.html)|1|[180104](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|
|[Tomasz Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-tomasz-myszeczka.html)|1|[171003](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|
|[Ryszard Kota](/rpg/inwazja/opowiesci/karty-postaci/9999-ryszard-kota.html)|1|[171003](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|
|[Robert Sądeczny](/rpg/inwazja/opowiesci/karty-postaci/1709-robert-sadeczny.html)|1|[171003](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|
|[Natalia Kazuń](/rpg/inwazja/opowiesci/karty-postaci/1709-natalia-kazun.html)|1|[171003](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|
|[Kociebor Dyrygent](/rpg/inwazja/opowiesci/karty-postaci/1709-kociebor-dyrygent.html)|1|[171003](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|
|[Jolanta Cieśliska](/rpg/inwazja/opowiesci/karty-postaci/9999-jolanta-ciesliska.html)|1|[171003](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|
|[Jakub Dobrocień](/rpg/inwazja/opowiesci/karty-postaci/1709-jakub-dobrocien.html)|1|[171003](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|
|[Henryk Mordżyn](/rpg/inwazja/opowiesci/karty-postaci/9999-henryk-mordzyn.html)|1|[171003](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|
|[Gabriel Purchasz](/rpg/inwazja/opowiesci/karty-postaci/9999-gabriel-purchasz.html)|1|[180104](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|
|[Bonifacy Jeż](/rpg/inwazja/opowiesci/karty-postaci/9999-bonifacy-jez.html)|1|[180104](/rpg/inwazja/opowiesci/konspekty/180104-wspanialy-wieprz-wojtka.html)|
|[Bolesław Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-boleslaw-maus.html)|1|[171003](/rpg/inwazja/opowiesci/konspekty/171003-swinia-na-portalisku.html)|

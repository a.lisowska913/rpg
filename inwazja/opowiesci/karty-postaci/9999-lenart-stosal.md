---
layout: inwazja-karta-postaci
categories: profile
title: "Lenart Stosal"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|140109|zwolennik Moriatha który jednak był, jak się okazuje, niewinny zgodnie z prawem.|[Uczniowie Moriatha](/rpg/inwazja/opowiesci/konspekty/140109-uczniowie-moriatha.html)|10/01/05|10/01/06|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Waldemar Zupaczka](/rpg/inwazja/opowiesci/karty-postaci/9999-waldemar-zupaczka.html)|1|[140109](/rpg/inwazja/opowiesci/konspekty/140109-uczniowie-moriatha.html)|
|[Wacław Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-waclaw-zajcew.html)|1|[140109](/rpg/inwazja/opowiesci/konspekty/140109-uczniowie-moriatha.html)|
|[Sebastian Tecznia](/rpg/inwazja/opowiesci/karty-postaci/9999-sebastian-tecznia.html)|1|[140109](/rpg/inwazja/opowiesci/konspekty/140109-uczniowie-moriatha.html)|
|[Rebeka Piryt](/rpg/inwazja/opowiesci/karty-postaci/9999-rebeka-piryt.html)|1|[140109](/rpg/inwazja/opowiesci/konspekty/140109-uczniowie-moriatha.html)|
|[Mojra](/rpg/inwazja/opowiesci/karty-postaci/9999-mojra.html)|1|[140109](/rpg/inwazja/opowiesci/konspekty/140109-uczniowie-moriatha.html)|
|[Marian Agrest](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-agrest.html)|1|[140109](/rpg/inwazja/opowiesci/konspekty/140109-uczniowie-moriatha.html)|
|[Cezary Piryt](/rpg/inwazja/opowiesci/karty-postaci/9999-cezary-piryt.html)|1|[140109](/rpg/inwazja/opowiesci/konspekty/140109-uczniowie-moriatha.html)|
|[Antoni Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/9999-antoni-myszeczka.html)|1|[140109](/rpg/inwazja/opowiesci/konspekty/140109-uczniowie-moriatha.html)|
|[Andrea Wilgacz](/rpg/inwazja/opowiesci/karty-postaci/1709-andrea-wilgacz.html)|1|[140109](/rpg/inwazja/opowiesci/konspekty/140109-uczniowie-moriatha.html)|

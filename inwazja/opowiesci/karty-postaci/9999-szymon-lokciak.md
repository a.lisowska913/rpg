---
layout: inwazja-karta-postaci
categories: profile
title: "Szymon Łokciak"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|171022|nieszczęśnik który miał zakazane systemy trzymania przy życiu i dlatego przeżył bycie zmasakrowanym przez elementala. Wyleczony przez Paulinę.|[Tylko nieświadomy elemental](/rpg/inwazja/opowiesci/konspekty/171022-tylko-nieswiadomy-elemental.html)|11/09/12|11/09/14|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|1|[171022](/rpg/inwazja/opowiesci/konspekty/171022-tylko-nieswiadomy-elemental.html)|
|[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/1709-maria-newa.html)|1|[171022](/rpg/inwazja/opowiesci/konspekty/171022-tylko-nieswiadomy-elemental.html)|
|[Kajetan Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-kajetan-weiner.html)|1|[171022](/rpg/inwazja/opowiesci/konspekty/171022-tylko-nieswiadomy-elemental.html)|
|[Filip Keramiusz](/rpg/inwazja/opowiesci/karty-postaci/9999-filip-keramiusz.html)|1|[171022](/rpg/inwazja/opowiesci/konspekty/171022-tylko-nieswiadomy-elemental.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|1|[171022](/rpg/inwazja/opowiesci/konspekty/171022-tylko-nieswiadomy-elemental.html)|

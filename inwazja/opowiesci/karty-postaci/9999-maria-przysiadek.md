---
layout: inwazja-karta-postaci
categories: profile
title: "Maria Przysiadek"
---
# {{ page.title }}

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Porwanie pod nosem hipisów](/rpg/inwazja/opowiesci/konspekty/180321-porwanie-pod-nosem-hipisow.html)|przebudziły się w niej geny kralothborn. Dotknęła ją moc Arazille. W tej chwili tylko forma i umysł jest u niej ludzka; wszystkie instynkty są obce.|Adaptacja kralotyczna|
|[Porwanie pod nosem hipisów](/rpg/inwazja/opowiesci/konspekty/180321-porwanie-pod-nosem-hipisow.html)|trafiła na KADEM, pod opiekę Edwina i Norberta.|Adaptacja kralotyczna|
|[Kraloth w piwnicy](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|Dotknięta Wolnością Arazille; wie, że w Czeliminie jest bezpieczna. Nie wie czemu to wie.|Adaptacja kralotyczna|
|[Kraloth w piwnicy](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|potrzebuje dostępu do leków kralotycznych, albo umrze.|Adaptacja kralotyczna|


## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|180321|pragnie wrócić do rodziny, nie rozumie tego świata; walczy z Arazille (Zachradnikiem) i swoją naturą. Skażona przez fabokle, krew kralothborn szaleje...|[Porwanie pod nosem hipisów](/rpg/inwazja/opowiesci/konspekty/180321-porwanie-pod-nosem-hipisow.html)|10/12/08|10/12/09|[Adaptacja kralotyczna](/rpg/inwazja/opowiesci/konspekty/kampania-adaptacja-kralotyczna.html)|
|180318|okazuje się że jest kralothborn ze krwi Hralglanatha. Po ataku Ognistych Niedźwiedzi na jej dom miała atak psychotyczny od "plasterka"; uciekła do Czelimina.|[Czyżby drugi kraloth?](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html)|10/12/05|10/12/07|[Adaptacja kralotyczna](/rpg/inwazja/opowiesci/konspekty/kampania-adaptacja-kralotyczna.html)|
|180310|"dama w czerwonym", żona jubilera i kralothbound. Sojuszniczka i przyjaciółka kralotha, nie jego niewolnica. Celuje w ratowanie kralotha, za cenę innych (?).|[Kraloth w piwnicy](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|10/11/28|10/12/01|[Adaptacja kralotyczna](/rpg/inwazja/opowiesci/konspekty/kampania-adaptacja-kralotyczna.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|3|[180321](/rpg/inwazja/opowiesci/konspekty/180321-porwanie-pod-nosem-hipisow.html), [180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html), [180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|2|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html), [180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Netheria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-netheria-diakon.html)|2|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html), [180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Marek Kromlan](/rpg/inwazja/opowiesci/karty-postaci/1803-marek-kromlan.html)|2|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html), [180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Judyta Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-judyta-maus.html)|2|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html), [180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Hralglanath](/rpg/inwazja/opowiesci/karty-postaci/9999-hralglanath.html)|2|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html), [180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Elea Maus](/rpg/inwazja/opowiesci/karty-postaci/1802-elea-maus.html)|2|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html), [180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Bójka Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-bojka-diakon.html)|2|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html), [180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Whisperwind](/rpg/inwazja/opowiesci/karty-postaci/9999-whisperwind.html)|1|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html)|
|[Szczepan Przysiadek](/rpg/inwazja/opowiesci/karty-postaci/9999-szczepan-przysiadek.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Siergiej Wdenkow](/rpg/inwazja/opowiesci/karty-postaci/1803-siergiej-wdenkow.html)|1|[180321](/rpg/inwazja/opowiesci/konspekty/180321-porwanie-pod-nosem-hipisow.html)|
|[Roman Bruniewicz](/rpg/inwazja/opowiesci/karty-postaci/9999-roman-bruniewicz.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Quasar](/rpg/inwazja/opowiesci/karty-postaci/9999-quasar.html)|1|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html)|
|[Paulina Widoczek](/rpg/inwazja/opowiesci/karty-postaci/9999-paulina-widoczek.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Lucjan Kopidół](/rpg/inwazja/opowiesci/karty-postaci/1803-lucjan-kopidol.html)|1|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html)|
|[Krystalia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-krystalia-diakon.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Karolina Kupiec](/rpg/inwazja/opowiesci/karty-postaci/1803-karolina-kupiec.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Grzegorz Zachradnik](/rpg/inwazja/opowiesci/karty-postaci/9999-grzegorz-zachradnik.html)|1|[180321](/rpg/inwazja/opowiesci/konspekty/180321-porwanie-pod-nosem-hipisow.html)|
|[Grzegorz Nocniarz](/rpg/inwazja/opowiesci/karty-postaci/9999-grzegorz-nocniarz.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Franciszek Knur](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-knur.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Filip Czółno](/rpg/inwazja/opowiesci/karty-postaci/9999-filip-czolno.html)|1|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html)|
|[Estrella Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-estrella-diakon.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Baltazar Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1802-baltazar-sowinski.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Artur Żupan](/rpg/inwazja/opowiesci/karty-postaci/1803-artur-zupan.html)|1|[180318](/rpg/inwazja/opowiesci/konspekty/180318-czyzby-drugi-kraloth.html)|
|[Artur Bryś](/rpg/inwazja/opowiesci/karty-postaci/1709-artur-brys.html)|1|[180310](/rpg/inwazja/opowiesci/konspekty/180310-kraloth-w-piwnicy.html)|
|[Amelia Eter](/rpg/inwazja/opowiesci/karty-postaci/9999-amelia-eter.html)|1|[180321](/rpg/inwazja/opowiesci/konspekty/180321-porwanie-pod-nosem-hipisow.html)|
|[Alina Bednarz](/rpg/inwazja/opowiesci/karty-postaci/1803-alina-bednarz.html)|1|[180321](/rpg/inwazja/opowiesci/konspekty/180321-porwanie-pod-nosem-hipisow.html)|

---
layout: inwazja-karta-postaci
categories: profile
title: "Maja Kos"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|140625|efemeryda będąca dowodem bezwzględności Agresta. Zniewolona i karmiona Tatianą przez Kameleon, by ściągnąć Irinę Zajcew. KIA.|[Ostatnia Saith](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html)|10/01/19|10/01/20|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|
|140618|na którą Rezydencja Blakenbauerów reaguje bardzo alergicznie; przy Superdefilerze "stała się potworem, mentalnie i fizycznie", lecz Agrest nie pozwolił Edwinowi jej zniszczyć.|[Upadek Agresta](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|10/01/17|10/01/18|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|
|140611|która powróciła jako agentka Mariana Agresta i ma zapieczętowane wszystkie rekordy.|[Rezydencja Blakenbauerów](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html)|10/01/15|10/01/16|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|
|140604|wierna i fanatycznie lojalna Marianowi Agrestowi terminuska, która zastąpiła mu Andreę jako jego przyboczna. Nie zatrzyma Krystalii w uprzęży.|[Patriarcha Blakenbauer](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|10/01/13|10/01/14|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Wacław Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-waclaw-zajcew.html)|4|[140625](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html), [140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140611](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Marian Agrest](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-agrest.html)|4|[140625](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html), [140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140611](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|4|[140625](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html), [140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140611](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Grzegorz Czerwiec](/rpg/inwazja/opowiesci/karty-postaci/9999-grzegorz-czerwiec.html)|4|[140625](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html), [140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140611](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Andrea Wilgacz](/rpg/inwazja/opowiesci/karty-postaci/1709-andrea-wilgacz.html)|4|[140625](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html), [140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140611](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Waldemar Zupaczka](/rpg/inwazja/opowiesci/karty-postaci/9999-waldemar-zupaczka.html)|3|[140625](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html), [140611](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Otton Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-otton-blakenbauer.html)|3|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140611](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Mojra](/rpg/inwazja/opowiesci/karty-postaci/9999-mojra.html)|3|[140625](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html), [140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Karolina Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-karolina-maus.html)|3|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140611](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Irina Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-irina-zajcew.html)|3|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140611](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Dariusz Kopyto](/rpg/inwazja/opowiesci/karty-postaci/9999-dariusz-kopyto.html)|3|[140625](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html), [140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Teresa Żyraf](/rpg/inwazja/opowiesci/karty-postaci/9999-teresa-zyraf.html)|2|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Sebastian Tecznia](/rpg/inwazja/opowiesci/karty-postaci/9999-sebastian-tecznia.html)|2|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Quasar](/rpg/inwazja/opowiesci/karty-postaci/9999-quasar.html)|2|[140625](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html), [140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Marcelin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-marcelin-blakenbauer.html)|2|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140611](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html)|
|[Gustaw Siedeł](/rpg/inwazja/opowiesci/karty-postaci/9999-gustaw-siedel.html)|2|[140611](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html), [140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|2|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html), [140611](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html)|
|[Benjamin Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-benjamin-zajcew.html)|2|[140625](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html), [140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Yakim Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-yakim-zajcew.html)|1|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Wojciech Tecznia](/rpg/inwazja/opowiesci/karty-postaci/9999-wojciech-tecznia.html)|1|[140625](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html)|
|[Whisperwind](/rpg/inwazja/opowiesci/karty-postaci/9999-whisperwind.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Tatiana Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-tatiana-zajcew.html)|1|[140611](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html)|
|[Saith Kameleon](/rpg/inwazja/opowiesci/karty-postaci/9999-saith-kameleon.html)|1|[140625](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html)|
|[Saith Flamecaller](/rpg/inwazja/opowiesci/karty-postaci/9999-saith-flamecaller.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Saith Catapult](/rpg/inwazja/opowiesci/karty-postaci/9999-saith-catapult.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Mateusz Nieborak](/rpg/inwazja/opowiesci/karty-postaci/9999-mateusz-nieborak.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Margaret Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-margaret-blakenbauer.html)|1|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Krystalia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-krystalia-diakon.html)|1|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Karol Poczciwiec](/rpg/inwazja/opowiesci/karty-postaci/9999-karol-poczciwiec.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Juliusz Szaman](/rpg/inwazja/opowiesci/karty-postaci/9999-juliusz-szaman.html)|1|[140625](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html)|
|[Ilarion Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-ilarion-zajcew.html)|1|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Ika](/rpg/inwazja/opowiesci/karty-postaci/9999-ika.html)|1|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|
|[Franciszek Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-maus.html)|1|[140611](/rpg/inwazja/opowiesci/konspekty/140611-rezydencja-blakenbauerow.html)|
|[Estera Piryt](/rpg/inwazja/opowiesci/karty-postaci/9999-estera-piryt.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Elżbieta Niemoc](/rpg/inwazja/opowiesci/karty-postaci/9999-elzbieta-niemoc.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Anna Kozak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kozak.html)|1|[140625](/rpg/inwazja/opowiesci/konspekty/140625-ostatnia-saith.html)|
|[Andrzej Sowiński](/rpg/inwazja/opowiesci/karty-postaci/9999-andrzej-sowinski.html)|1|[140618](/rpg/inwazja/opowiesci/konspekty/140618-upadek-agresta.html)|
|[Amelia Eter](/rpg/inwazja/opowiesci/karty-postaci/9999-amelia-eter.html)|1|[140604](/rpg/inwazja/opowiesci/konspekty/140604-patriarcha-blakenbauer.html)|

---
layout: inwazja-karta-postaci
categories: profile
title: "Glarnohlagh"
---
# {{ page.title }}

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Pętla dookoła niekralotha](/rpg/inwazja/opowiesci/konspekty/180402-petla-dookola-niekralotha.html)|Bójka spawnuje mu nowego kralotha (guaranteed future)|Adaptacja kralotyczna|
|[Pętla dookoła niekralotha](/rpg/inwazja/opowiesci/konspekty/180402-petla-dookola-niekralotha.html)|wpakuje się jakoś w Dziwne Dildo Silurii w Mordowni Arenie Wojny (guaranteed future)|Adaptacja kralotyczna|
|[Złodzieje nieważnych artefaktów](/rpg/inwazja/opowiesci/konspekty/180411-zlodzieje-niewaznych-artefaktow.html)|rozprzestrzenił się na Fazę Daemonica|Adaptacja kralotyczna|
|[Szept Mare Felix](/rpg/inwazja/opowiesci/konspekty/180327-szept-mare-felix.html)|rozprzestrzenił swoje Kwiaty na Fazie Daemonica.|Adaptacja kralotyczna|
|[Szept Mare Felix](/rpg/inwazja/opowiesci/konspekty/180327-szept-mare-felix.html)|uzyskał Bójkę w stanie nadającym się na zbudowanie nowego kralotha.|Adaptacja kralotyczna|

## Plany

|Misja|Plan|Kampania|
|-----|------|------|
|[Krystalia poluje na niekralotha](/rpg/inwazja/opowiesci/konspekty/180526-krystalia-poluje-na-niekralotha.html)|jest zmuszony do natychmiastowego działania przez Krystalię; pętla dookoła niego się zaciska.|Adaptacja kralotyczna|

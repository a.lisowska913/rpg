---
layout: inwazja-karta-postaci
categories: profile
factions: "Srebrna Świeca"
type: "NPC"
owner: "public"
title: "Maurycy Maus"
---
# {{ page.title }}

## Koncept

zegarmistrz-transhumanista, "Front Row", Clockwork Mage

Steampunkowy mistrz pozytywek i piękna, który pragnie pokonać śmierć i uwolnić się od wszystkich - łącznie z Karradraelem.

## Postać

### Motywacje

#### Kategorie i Aspekty

| Kategoria         | Aspekty                           |
|-------------------|-----------------------------------|
| Indywidualne      | mistrzostwo; niezależność         |
| Społeczne         | transhumanizm; altruizm           |
| Wartości          | homo superior; pacyfizm; technologia; piękno |

#### Szczególnie

| Co chce by się działo?                                                      | Co na pewno ma się NIE dziać? Co jest sprzeczne?                                                  |
|-----------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------|
| zbuduj najdoskonalsze, wieczne ciało; pokonaj śmierć i utrzymaj kogoś przy życiu | dąż do potęgi nawet kosztem duszy |
| uwolnij się od wszelkiej władzy; nie kontroluj nikogo i nie daj się kontrolować | wolność jest warta krzywdzenia innych; stawiaj na swoim kosztem innych |

### Umiejętności

#### Kategorie i Aspekty

| Kategoria         | Aspekty                           |
|-------------------|-----------------------------------|
| zegarmistrz       | pozytywki; zegary; protezy        |
| inżynier          | steampunk; lalki; pułapki         |
| archiwista        | książki; analiza historyczna      |
| artefaktor        | steampunk                         |

#### Manewry

| Jakie działania wykonuje?                                                              | Czym osiąga sukces?                                                          |
|----------------------------------------------------------------------------------------|------------------------------------------------------------------------------|
|  |  |
|  |  |
|  |  |

### Silne i słabe strony

#### Kategorie i Aspekty

| Kategoria            | Aspekty                        |
|----------------------|--------------------------------|
|  |  |

#### Manewry

| Co jest wzmocnione                                        | Kosztem czego                                                 |
|-----------------------------------------------------------|---------------------------------------------------------------|
|  |  |
|  |  |
|  |  |

### Szkoły magiczne

#### Kategorie i Aspekty

| Kategoria           | Aspekty                                                                             |
|---------------------|-------------------------------------------------------------------------------------|
| Magia Materii       | zegary |
| Magia Mentalna      | golemy |
| Magia Elementalna   | źródła energii; para |
| Technomancja        | zegary |

#### Manewry

| Jakie działania wykonuje?                                                 | Czym osiąga sukces?                                                               |
|---------------------------------------------------------------------------|-----------------------------------------------------------------------------------|
|  |  |
|  |  |
|  |  |

### Zasoby i otoczenie

#### Powiązane frakcje

{{ page.factions }}

#### Kategorie i Aspekty

| Kategoria                          | Aspekty                                                              |
|------------------------------------|----------------------------------------------------------------------|
| Zaklęcie Stazy                     | maksymalne spowolnienie; utrzymanie życia; deaktywacja               |
| Parowa Biblioteka                  | wiedza steampunkowa; autonomiczny artefakt; parowe wynalazki         |
|  |  |
|  |  |

#### Manewry

| Jakie działania wspierane?                                                      | Czym osiąga sukces?                                           |
|---------------------------------------------------------------------------------|---------------------------------------------------------------|
|  |  |
|  |  |
|  |  |

## Opis

### Ogólnie

Mistrzostwo. Clockwork. Samotnik. Pacyfista. Łagodny, spolegliwy, kocha piękno. Zainteresowany czasem i stazą. Żyje poza światem ludzi i magów; samotnik. Jego Karradrael skłonił do pójścia w kierunku na "New Flesh". Clockwork Mage. Szuka "The Ultimate Perfection". Chce odtworzyć Vector Sigma.

### Motywacje:

### Działanie:

### Specjalne:

### Magia:

### Otoczenie:

### Mapa kreacji

brak

### Motto

""

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Złodzieje nieważnych artefaktów](/rpg/inwazja/opowiesci/konspekty/180411-zlodzieje-niewaznych-artefaktow.html)|za dwa dni zostanie złapany przez Drugiego Kralotha (gwarantowana przyszłość)|Adaptacja kralotyczna|


## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|180411|wszystko źle zinterpretował, lecz rozwiązał Tajemnicę Znikających Nieważnych Artefaktów przez Drugiego Kralotha. Naprawił Alegrettę.|[Złodzieje nieważnych artefaktów](/rpg/inwazja/opowiesci/konspekty/180411-zlodzieje-niewaznych-artefaktow.html)|10/12/13|10/12/15|[Adaptacja kralotyczna](/rpg/inwazja/opowiesci/konspekty/kampania-adaptacja-kralotyczna.html)|
|170517|ciężko ranny zegarmistrz, który zrobił Alegrettę. Leży w stazie całą misję i umiera. Szczęśliwie, przeżył - dzięki Blakenbauerom.|[Zegarmistrz i Alegretta](/rpg/inwazja/opowiesci/konspekty/170517-zegarmistrz-i-alegretta.html)|10/08/17|10/08/18|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170217|zegarmistrz kochający sztukę i muzykę, który z Tymkiem zbudował steampunkową pozytywkę. 4x-5x-letni pacyfista, pobłaża Judycie i daje sobie wejść na głowę.|[Skradziona pozytywka Mausów](/rpg/inwazja/opowiesci/konspekty/170217-skradziona-pozytywka-mausow.html)|10/02/28|10/03/02|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Judyta Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-judyta-maus.html)|2|[170517](/rpg/inwazja/opowiesci/konspekty/170517-zegarmistrz-i-alegretta.html), [170217](/rpg/inwazja/opowiesci/konspekty/170217-skradziona-pozytywka-mausow.html)|
|[Alegretta Tractus](/rpg/inwazja/opowiesci/karty-postaci/9999-alegretta-tractus.html)|2|[180411](/rpg/inwazja/opowiesci/konspekty/180411-zlodzieje-niewaznych-artefaktow.html), [170517](/rpg/inwazja/opowiesci/konspekty/170517-zegarmistrz-i-alegretta.html)|
|[Tymoteusz Maus](/rpg/inwazja/opowiesci/karty-postaci/1709-tymoteusz-maus.html)|1|[170217](/rpg/inwazja/opowiesci/konspekty/170217-skradziona-pozytywka-mausow.html)|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|1|[170217](/rpg/inwazja/opowiesci/konspekty/170217-skradziona-pozytywka-mausow.html)|
|[Robert Pomocnik](/rpg/inwazja/opowiesci/karty-postaci/9999-robert-pomocnik.html)|1|[170517](/rpg/inwazja/opowiesci/konspekty/170517-zegarmistrz-i-alegretta.html)|
|[Paweł Sępiak](/rpg/inwazja/opowiesci/karty-postaci/1709-pawel-sepiak.html)|1|[170217](/rpg/inwazja/opowiesci/konspekty/170217-skradziona-pozytywka-mausow.html)|
|[Otton Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-otton-blakenbauer.html)|1|[170517](/rpg/inwazja/opowiesci/konspekty/170517-zegarmistrz-i-alegretta.html)|
|[Margaret Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-margaret-blakenbauer.html)|1|[170517](/rpg/inwazja/opowiesci/konspekty/170517-zegarmistrz-i-alegretta.html)|
|[Klara Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-klara-blakenbauer.html)|1|[170517](/rpg/inwazja/opowiesci/konspekty/170517-zegarmistrz-i-alegretta.html)|
|[Infernia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-infernia-diakon.html)|1|[170217](/rpg/inwazja/opowiesci/konspekty/170217-skradziona-pozytywka-mausow.html)|
|[Ignat Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1802-ignat-zajcew.html)|1|[170217](/rpg/inwazja/opowiesci/konspekty/170217-skradziona-pozytywka-mausow.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|1|[170517](/rpg/inwazja/opowiesci/konspekty/170517-zegarmistrz-i-alegretta.html)|
|[Gala Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-gala-zajcew.html)|1|[170217](/rpg/inwazja/opowiesci/konspekty/170217-skradziona-pozytywka-mausow.html)|
|[Franciszek Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-maus.html)|1|[170217](/rpg/inwazja/opowiesci/konspekty/170217-skradziona-pozytywka-mausow.html)|
|[Estrella Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-estrella-diakon.html)|1|[180411](/rpg/inwazja/opowiesci/konspekty/180411-zlodzieje-niewaznych-artefaktow.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|1|[170517](/rpg/inwazja/opowiesci/konspekty/170517-zegarmistrz-i-alegretta.html)|
|[Daria Rudas](/rpg/inwazja/opowiesci/karty-postaci/1803-daria-rudas.html)|1|[180411](/rpg/inwazja/opowiesci/konspekty/180411-zlodzieje-niewaznych-artefaktow.html)|

---
layout: inwazja-karta-postaci
categories: profile
title: "Krzysztof Jarzolin"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170815|"studenciak" i znajomy Andromedy; wykorzystany by wymyślił coś, na co Czepiec nie wpadnie. I wymyślił - Escape Room.|[Bliźniaczka Andromedy](/rpg/inwazja/opowiesci/konspekty/170815-blizniaczka-andromedy.html)|10/01/23|10/01/25|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Katarzyna Nieborak](/rpg/inwazja/opowiesci/karty-postaci/9999-katarzyna-nieborak.html)|1|[170815](/rpg/inwazja/opowiesci/konspekty/170815-blizniaczka-andromedy.html)|
|[Kasia Nowak](/rpg/inwazja/opowiesci/karty-postaci/1803-kasia-nowak.html)|1|[170815](/rpg/inwazja/opowiesci/konspekty/170815-blizniaczka-andromedy.html)|
|[Czesław Czepiec](/rpg/inwazja/opowiesci/karty-postaci/9999-czeslaw-czepiec.html)|1|[170815](/rpg/inwazja/opowiesci/konspekty/170815-blizniaczka-andromedy.html)|

---
layout: inwazja-karta-postaci
categories: profile
title: "Damian Wiórski"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170319|niezbyt ważny dealer i kontakt Aliny; powiedział Alinie, że w sumie ten bojowy narkotyk nie jest rozprowadzany. Wskazał na to, że może być magiczny, bo ludzie zapominają i parzą się srebrem.|[Camgirl na dragach](/rpg/inwazja/opowiesci/konspekty/170319-camgirl-na-dragach.html)|10/03/06|10/03/07|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Roman Brunowicz](/rpg/inwazja/opowiesci/karty-postaci/9999-roman-brunowicz.html)|1|[170319](/rpg/inwazja/opowiesci/konspekty/170319-camgirl-na-dragach.html)|
|[Robert Pomocnik](/rpg/inwazja/opowiesci/karty-postaci/9999-robert-pomocnik.html)|1|[170319](/rpg/inwazja/opowiesci/konspekty/170319-camgirl-na-dragach.html)|
|[Paulina Widoczek](/rpg/inwazja/opowiesci/karty-postaci/9999-paulina-widoczek.html)|1|[170319](/rpg/inwazja/opowiesci/konspekty/170319-camgirl-na-dragach.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|1|[170319](/rpg/inwazja/opowiesci/konspekty/170319-camgirl-na-dragach.html)|
|[Artur Bryś](/rpg/inwazja/opowiesci/karty-postaci/1709-artur-brys.html)|1|[170319](/rpg/inwazja/opowiesci/konspekty/170319-camgirl-na-dragach.html)|
|[Alina Bednarz](/rpg/inwazja/opowiesci/karty-postaci/1803-alina-bednarz.html)|1|[170319](/rpg/inwazja/opowiesci/konspekty/170319-camgirl-na-dragach.html)|

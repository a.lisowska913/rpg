---
layout: inwazja-karta-postaci
categories: profile
title: "Grażyna Tuloz"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170423|pomagająca Paulinie w detoksie Katarzyny; Paulina musiała przy niej rzucić czar kilka razy by ratować Kasię.|[Rozpad magii w Leere](/rpg/inwazja/opowiesci/konspekty/170423-rozpad-magii-w-leere.html)|10/06/24|10/06/27|[Ucieczka do Przodka](/rpg/inwazja/opowiesci/konspekty/kampania-ucieczka-do-przodka.html)|
|160117|która przez magię Pauliny będzie miała koszmary senne; powierniczka relikwii o stalowej woli, która się staroście nie kłania.|[Muchy w sieci Korzunia](/rpg/inwazja/opowiesci/konspekty/160117-muchy-w-sieci-korzunia.html)|10/06/12|10/06/13|[Ucieczka do Przodka](/rpg/inwazja/opowiesci/konspekty/kampania-ucieczka-do-przodka.html)|
|151229|okazuje się, że 3 lata temu uczestniczyła w akcji przeciwko sekcie; schowała im satanistyczno-nazistowską relikwię.|[..choć to na sektę nie pomoże (PT)](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html)|10/06/12|10/06/13|[Ucieczka do Przodka](/rpg/inwazja/opowiesci/konspekty/kampania-ucieczka-do-przodka.html)|
|160106|dowodziła Szpitalem Gotyckim podczas wielkiego ataku kultu w Przodku. Powierniczka relikwii.|[...i kult zostaje rozgromiony](/rpg/inwazja/opowiesci/konspekty/160106-i-kult-zostaje-rozgromiony.html)|10/06/10|10/06/11|[Ucieczka do Przodka](/rpg/inwazja/opowiesci/konspekty/kampania-ucieczka-do-przodka.html)|
|151223|dobra kobieta i zastępczyni Ryszarda Hermana, która (dzięki machinacjom Marzeny) przyłapała Karmelika i Dorszaj na zabawach w godzinach pracy.|[..można uwolnić czarodziejkę...](/rpg/inwazja/opowiesci/konspekty/151223-mozna-uwolnic-czarodziejke.html)|10/06/10|10/06/11|[Ucieczka do Przodka](/rpg/inwazja/opowiesci/konspekty/kampania-ucieczka-do-przodka.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|5|[170423](/rpg/inwazja/opowiesci/konspekty/170423-rozpad-magii-w-leere.html), [160117](/rpg/inwazja/opowiesci/konspekty/160117-muchy-w-sieci-korzunia.html), [151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html), [160106](/rpg/inwazja/opowiesci/konspekty/160106-i-kult-zostaje-rozgromiony.html), [151223](/rpg/inwazja/opowiesci/konspekty/151223-mozna-uwolnic-czarodziejke.html)|
|[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/1709-maria-newa.html)|3|[170423](/rpg/inwazja/opowiesci/konspekty/170423-rozpad-magii-w-leere.html), [151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html), [160106](/rpg/inwazja/opowiesci/konspekty/160106-i-kult-zostaje-rozgromiony.html)|
|[Krystian Korzunio](/rpg/inwazja/opowiesci/karty-postaci/1709-krystian-korzunio.html)|3|[170423](/rpg/inwazja/opowiesci/konspekty/170423-rozpad-magii-w-leere.html), [160117](/rpg/inwazja/opowiesci/konspekty/160117-muchy-w-sieci-korzunia.html), [160106](/rpg/inwazja/opowiesci/konspekty/160106-i-kult-zostaje-rozgromiony.html)|
|[Łukasz Perkas](/rpg/inwazja/opowiesci/karty-postaci/9999-lukasz-perkas.html)|2|[160117](/rpg/inwazja/opowiesci/konspekty/160117-muchy-w-sieci-korzunia.html), [151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html)|
|[Wiesław Rekin](/rpg/inwazja/opowiesci/karty-postaci/9999-wieslaw-rekin.html)|2|[151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html), [160106](/rpg/inwazja/opowiesci/konspekty/160106-i-kult-zostaje-rozgromiony.html)|
|[Mirek Kujec](/rpg/inwazja/opowiesci/karty-postaci/9999-mirek-kujec.html)|2|[160117](/rpg/inwazja/opowiesci/konspekty/160117-muchy-w-sieci-korzunia.html), [151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html)|
|[Marzena Dorszaj](/rpg/inwazja/opowiesci/karty-postaci/1709-marzena-dorszaj.html)|2|[151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html), [151223](/rpg/inwazja/opowiesci/konspekty/151223-mozna-uwolnic-czarodziejke.html)|
|[Jerzy Karmelik](/rpg/inwazja/opowiesci/karty-postaci/9999-jerzy-karmelik.html)|2|[151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html), [151223](/rpg/inwazja/opowiesci/konspekty/151223-mozna-uwolnic-czarodziejke.html)|
|[Ireneusz Przaśnik](/rpg/inwazja/opowiesci/karty-postaci/9999-ireneusz-przasnik.html)|2|[170423](/rpg/inwazja/opowiesci/konspekty/170423-rozpad-magii-w-leere.html), [151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html)|
|[Bartosz Bławatek](/rpg/inwazja/opowiesci/karty-postaci/9999-bartosz-blawatek.html)|2|[151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html), [160106](/rpg/inwazja/opowiesci/konspekty/160106-i-kult-zostaje-rozgromiony.html)|
|[Alicja Gąszcz](/rpg/inwazja/opowiesci/karty-postaci/9999-alicja-gaszcz.html)|2|[160117](/rpg/inwazja/opowiesci/konspekty/160117-muchy-w-sieci-korzunia.html), [151229](/rpg/inwazja/opowiesci/konspekty/151229-choc-to-na-sekte-nie-pomoze.html)|
|[Zenobia Morwiczka](/rpg/inwazja/opowiesci/karty-postaci/9999-zenobia-morwiczka.html)|1|[170423](/rpg/inwazja/opowiesci/konspekty/170423-rozpad-magii-w-leere.html)|
|[Wojmił Rzeźniczek](/rpg/inwazja/opowiesci/karty-postaci/9999-wojmil-rzezniczek.html)|1|[170423](/rpg/inwazja/opowiesci/konspekty/170423-rozpad-magii-w-leere.html)|
|[Wiaczesław Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-wiaczeslaw-zajcew.html)|1|[170423](/rpg/inwazja/opowiesci/konspekty/170423-rozpad-magii-w-leere.html)|
|[Tomasz Leżniak](/rpg/inwazja/opowiesci/karty-postaci/9999-tomasz-lezniak.html)|1|[160106](/rpg/inwazja/opowiesci/konspekty/160106-i-kult-zostaje-rozgromiony.html)|
|[Ochrona Kobra Przodek](/rpg/inwazja/opowiesci/karty-postaci/9999-ochrona-kobra-przodek.html)|1|[160106](/rpg/inwazja/opowiesci/konspekty/160106-i-kult-zostaje-rozgromiony.html)|
|[Maciej Orank](/rpg/inwazja/opowiesci/karty-postaci/9999-maciej-orank.html)|1|[160117](/rpg/inwazja/opowiesci/konspekty/160117-muchy-w-sieci-korzunia.html)|
|[Leopold Brzeszczot](/rpg/inwazja/opowiesci/karty-postaci/9999-leopold-brzeszczot.html)|1|[160117](/rpg/inwazja/opowiesci/konspekty/160117-muchy-w-sieci-korzunia.html)|
|[Kinga Kujec](/rpg/inwazja/opowiesci/karty-postaci/9999-kinga-kujec.html)|1|[160117](/rpg/inwazja/opowiesci/konspekty/160117-muchy-w-sieci-korzunia.html)|
|[Katarzyna Klicz](/rpg/inwazja/opowiesci/karty-postaci/9999-katarzyna-klicz.html)|1|[170423](/rpg/inwazja/opowiesci/konspekty/170423-rozpad-magii-w-leere.html)|
|[Kaja Odyniec](/rpg/inwazja/opowiesci/karty-postaci/9999-kaja-odyniec.html)|1|[170423](/rpg/inwazja/opowiesci/konspekty/170423-rozpad-magii-w-leere.html)|
|[Joachim Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-zajcew.html)|1|[160106](/rpg/inwazja/opowiesci/konspekty/160106-i-kult-zostaje-rozgromiony.html)|
|[Hubert Rębski](/rpg/inwazja/opowiesci/karty-postaci/9999-hubert-rebski.html)|1|[160106](/rpg/inwazja/opowiesci/konspekty/160106-i-kult-zostaje-rozgromiony.html)|
|[Filip Wichoszczyk](/rpg/inwazja/opowiesci/karty-postaci/9999-filip-wichoszczyk.html)|1|[170423](/rpg/inwazja/opowiesci/konspekty/170423-rozpad-magii-w-leere.html)|
|[Filip Brzeszczot](/rpg/inwazja/opowiesci/karty-postaci/9999-filip-brzeszczot.html)|1|[160117](/rpg/inwazja/opowiesci/konspekty/160117-muchy-w-sieci-korzunia.html)|
|[Ernest Brzeszczot](/rpg/inwazja/opowiesci/karty-postaci/9999-ernest-brzeszczot.html)|1|[160117](/rpg/inwazja/opowiesci/konspekty/160117-muchy-w-sieci-korzunia.html)|
|[Edward Ramuel](/rpg/inwazja/opowiesci/karty-postaci/9999-edward-ramuel.html)|1|[170423](/rpg/inwazja/opowiesci/konspekty/170423-rozpad-magii-w-leere.html)|
|[Artur Kurczak](/rpg/inwazja/opowiesci/karty-postaci/9999-artur-kurczak.html)|1|[170423](/rpg/inwazja/opowiesci/konspekty/170423-rozpad-magii-w-leere.html)|
|[Agnieszka Mariacka](/rpg/inwazja/opowiesci/karty-postaci/9999-agnieszka-mariacka.html)|1|[160106](/rpg/inwazja/opowiesci/konspekty/160106-i-kult-zostaje-rozgromiony.html)|

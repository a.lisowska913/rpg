---
layout: inwazja-karta-postaci
categories: profile
factions: "Nieznany"
type: "NPC"
owner: "maja"
title: "Aneta Pietraszek"
---
# {{ page.title }}

## Postać

### Motywacje 
#### Kategorie i Aspekty

| Kategoria         | Aspekty                           |
|-------------------|-----------------------------------|
| Indywidualne      | eliminacja dyskryminacji; ciekawość |
| Społeczne         | pokój; stabilizacja|
| Wartości          | cierpliwość; pragmatyzm|

#### Szczególnie

| Co chce by się działo?                                                      | Co na pewno ma się NIE dziać? Co jest sprzeczne?                                                  |
|-----------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------|
| dąż do eliminacji nietolerancji |  |
| walcz z przejawami powtarzającej się wrogości | narażaj swoją autonomię |

### Umiejętności
#### Kategorie i Aspekty

| Kategoria         | Aspekty                           |
|-------------------|-----------------------------------|
| Researcher | deep web; hacking; analiza  |
| Wolontariusz |    |

#### Manewry

| Jakie działania wykonuje?                                                              | Czym osiąga sukces?                                                          |
|----------------------------------------------------------------------------------------|------------------------------------------------------------------------------|
| wyszukiwanie informacji; hacking | deep web; nieoficjalne i trudno dostępne systemy |
| zbieranie informacji; propaganda | znajomości wśród wolontariuszy; znajomości wśród niższych warstw społecznych |

### Silne i słabe strony:
#### Kategorie i Aspekty

| Kategoria            | Aspekty                        |
|----------------------|--------------------------------|
| psychoza |  |

#### Manewry

| Co jest wzmocnione                                        | Kosztem czego                                                 |
|-----------------------------------------------------------|---------------------------------------------------------------|
| aktorstwo | zaburzone sumienie |


## Zasoby i otoczenie
#### Powiązane frakcje

{{ page.factions }}

#### Kategorie i Aspekty

| Kategoria                          | Aspekty                                                              |
|------------------------------------|----------------------------------------------------------------------|
| dobra reputacja wśród służb |  |
| znajomości wśród niższych warstw społecznych |  |
| dobrej klasy komputer |  |
| Golf 2 |  |
| Duże zasoby finansowe |  |

#### Manewry

| Jakie działania wspierane?                                                      | Czym osiąga sukces?                                           |
|---------------------------------------------------------------------------------|---------------------------------------------------------------|
|  |  |

# Opis

### Koncept

Postać 1 + Postać 2 + Postać 3

### Ogólnie

### Motywacje:

### Działanie:

### Specjalne:

### Magia:

### Otoczenie:

### Mapa kreacji

brak

### Motto

""

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Czapkowicka Apatia Kulturalna](/rpg/inwazja/opowiesci/konspekty/180418-czapkowicka-apatia-kulturalna.html)|pozyskała nóż chroniący przed opętaniami i walczący z opętaniami. Sęk w tym, że służy też do krojenia żółtego sera.|Dusza Czapkowika|


## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|180418|wyszukała kluczowe informacje o człowieku-jaszczurze i pomogła Siergiejowi w epickiej walce z narkomanem z nożem. Jako jedyna zadbała o Kasię właściwie.|[Czapkowicka Apatia Kulturalna](/rpg/inwazja/opowiesci/konspekty/180418-czapkowicka-apatia-kulturalna.html)|10/11/08|10/11/10|[Dusza Czapkowika](/rpg/inwazja/opowiesci/konspekty/kampania-dusza-czapkowika.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Sławek Broda](/rpg/inwazja/opowiesci/karty-postaci/9999-slawek-broda.html)|1|[180418](/rpg/inwazja/opowiesci/konspekty/180418-czapkowicka-apatia-kulturalna.html)|
|[Szczepan Porzeczka](/rpg/inwazja/opowiesci/karty-postaci/9999-szczepan-porzeczka.html)|1|[180418](/rpg/inwazja/opowiesci/konspekty/180418-czapkowicka-apatia-kulturalna.html)|
|[Siergiej Wdenkow](/rpg/inwazja/opowiesci/karty-postaci/1803-siergiej-wdenkow.html)|1|[180418](/rpg/inwazja/opowiesci/konspekty/180418-czapkowicka-apatia-kulturalna.html)|
|[Paweł Szlezg](/rpg/inwazja/opowiesci/karty-postaci/1803-pawel-szlezg.html)|1|[180418](/rpg/inwazja/opowiesci/konspekty/180418-czapkowicka-apatia-kulturalna.html)|
|[Mariusz Niewiadomski](/rpg/inwazja/opowiesci/karty-postaci/9999-mariusz-niewiadomski.html)|1|[180418](/rpg/inwazja/opowiesci/konspekty/180418-czapkowicka-apatia-kulturalna.html)|
|[Kasia Krabowska](/rpg/inwazja/opowiesci/karty-postaci/9999-kasia-krabowska.html)|1|[180418](/rpg/inwazja/opowiesci/konspekty/180418-czapkowicka-apatia-kulturalna.html)|
|[Franciszek Bratkowski](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-bratkowski.html)|1|[180418](/rpg/inwazja/opowiesci/konspekty/180418-czapkowicka-apatia-kulturalna.html)|
|[Alojzy Przylaz](/rpg/inwazja/opowiesci/karty-postaci/1803-alojzy-przylaz.html)|1|[180418](/rpg/inwazja/opowiesci/konspekty/180418-czapkowicka-apatia-kulturalna.html)|
|[Alfred Werner](/rpg/inwazja/opowiesci/karty-postaci/9999-alfred-werner.html)|1|[180418](/rpg/inwazja/opowiesci/konspekty/180418-czapkowicka-apatia-kulturalna.html)|

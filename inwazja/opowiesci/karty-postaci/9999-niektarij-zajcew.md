---
layout: inwazja-karta-postaci
categories: profile
title: "Niektarij Zajcew"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|161018|który uprzednio tu sprzątał wyciek syberionu; zaszantażowany przez Jelenę się złamał i wysłał jej zabutelkowanego miecztapauka.|[Ballada o duszy ognistej](/rpg/inwazja/opowiesci/konspekty/161018-ballada-o-duszy-ognistej.html)|10/07/02|10/07/04|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Tomasz Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-tomasz-weiner.html)|1|[161018](/rpg/inwazja/opowiesci/konspekty/161018-ballada-o-duszy-ognistej.html)|
|[Krystian Korzunio](/rpg/inwazja/opowiesci/karty-postaci/1709-krystian-korzunio.html)|1|[161018](/rpg/inwazja/opowiesci/konspekty/161018-ballada-o-duszy-ognistej.html)|
|[Kira Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-kira-zajcew.html)|1|[161018](/rpg/inwazja/opowiesci/konspekty/161018-ballada-o-duszy-ognistej.html)|
|[Jelena Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-jelena-zajcew.html)|1|[161018](/rpg/inwazja/opowiesci/konspekty/161018-ballada-o-duszy-ognistej.html)|
|[Henryk Siwiecki](/rpg/inwazja/opowiesci/karty-postaci/1709-henryk-siwiecki.html)|1|[161018](/rpg/inwazja/opowiesci/konspekty/161018-ballada-o-duszy-ognistej.html)|
|[Halina Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-halina-weiner.html)|1|[161018](/rpg/inwazja/opowiesci/konspekty/161018-ballada-o-duszy-ognistej.html)|
|[Damazy Rozenblum](/rpg/inwazja/opowiesci/karty-postaci/9999-damazy-rozenblum.html)|1|[161018](/rpg/inwazja/opowiesci/konspekty/161018-ballada-o-duszy-ognistej.html)|
|[Anton Jesiotr](/rpg/inwazja/opowiesci/karty-postaci/9999-anton-jesiotr.html)|1|[161018](/rpg/inwazja/opowiesci/konspekty/161018-ballada-o-duszy-ognistej.html)|

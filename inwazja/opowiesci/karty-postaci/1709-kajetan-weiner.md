---
layout: inwazja-karta-postaci
categories: profile
factions: "Rycerze Iglicy"
type: "NPC"
title: "Kajetan Weiner"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **BÓL: Waśnie, brutalność i złe prawa - a on może tylko patrzeć**:
    * _Aspekty_: bezsensowna wojna totalna, waśnie i bitwy, ogólna brutalizacja świata, bardzo niebezpieczny świat, silny pożera słabego, okrutne prawa wzmacniające skorumpowane systemy, nikogo nie przekona i zostanie don kiszotem, jego przyjaciele ucierpią, chce znaleźć następców
    * _Opis_: Kajetan orientuje się, że sam wiele nie zdziała a kiedyś umrze. Dlatego szuka obiecujących terminusów (i nie tylko) i próbuje im pomagać i ich indoktrynować. Podróżuje więc po Polsce szukając miejsc, gdzie może być potrzebny i gdzie może pomóc najmocniej - nie tylko jako terminus liniowy, ale przede wszystkim gdzie może zaprowadzić długoterminowy ład i porządek.
* **FIL: Wojna się nigdy nie kończy - ale może być powstrzymana**:
    * _Aspekty_: zwalcza przemoc, wybaczanie nad sprawiedliwość, długoterminowe korzyści nad tymczasowe działania, budowanie pomocnych struktur, promuje współpracę i godzenie waśni, środki NIE uświęcają celu
    * _Opis_: Zdaniem Kajetana nic sie nigdy SAMO nie zmieniło. Wojna i tendencje niszczycielskie są w naszej naturze - ale istnieje możliwość by temu zapobiec. Musimy aktywnie stanąć i walczyć o lepsze jutro, próbować osiągnąć wyższy poziom dobra. Promuje wybaczanie a nie krwawą zemstę i gra w bardzo długoterminową grę - pomagajmy sobie nawzajem, budujmy korzystne struktury i myślmy o tym co robimy.
* **ŚR: Wędrowny paladyn i nauczyciel**:
    * _Aspekty_: pomagać sobie podobnym, szerzyć ideały pomocy, zbudować sobie następców, działać własnym przykładem, ciągle w ruchu, odpowiedzialność dla osób lokalnych, indoktrynować innych, dyplomacja nad walkę, wędrowny sensei
    * _Opis_: Wieczny wędrowiec, Kajetan chodzi i naprawia, szuka i pomaga. Buduje następców, świeci własnym przykładem i jak może pozwala osobom lokalnie naprawiać błędy i problemy. Tak, by jak jego zabraknie wszystko działało dalej.

### Umiejętności

* **Rycerz**:
    * _Aspekty_: rekonstrukcja, świetnie włada łukiem, dobry szermierz, konstruktor łuku, płatnerz, miecz i tarcza, organizator eventów, efektowna walka, przemowy, aktorstwo, pierwsza pomoc
    * _Opis_: Miłośnik rekonstrukcji średniowiecza; dobrze włada łukiem i świetnie walczy mieczem i tarczą w zwarciu. W połączeniu z magią daje mu możliwość w miarę bezpiecznego wyjścia na pole bitwy - a jako, że świetnie potrafi walczyć efektownie i robić show, jego reputacja jest czasem niezasłużenie wręcz dobra.
* **Gawędziarz**: 
    * _Aspekty_: siejący dobry morał, historie i legendy, snucie opowieści, inspirowanie, przejmowanie dowodzenia, wzór dla młodych, charyzmatyczny przywódca, prezentacje publiczne, robienie show, skupianie uwagi na sobie
    * _Opis_: Kajetan jest czterdziestoczteroletnim terminusem w branży, w której umiera się młodo. Zna wiele opowieści i potrafi je barwnie opowiadać. Potrafi skupić uwagę na sobie oraz prowadzić innych za sobą.
* **Terminus**:
    * _Aspekty_: oficer, taktyka, znajdowanie słabego punktu, świetny w planowaniu, oszczędny w energii, ogromna wiedza o viciniusach, ogromna wiedza polityczna, "trudno mnie przechytrzyć", zasadzki i pułapki
    * _Opis_: Kajetan może walczyć osobiście, ale jego siła leży w innym zakresie umiejętności. Dobry taktyk, który sporo widział i walczył w wielu bitwach. Zdolny do najlepszego wykorzystania posiadanych przez siebie sił - i osoba, której nie da się zaskoczyć na płaszczyźnie politycznej czy taktycznej.
    
### Silne i słabe strony:

* **Wiekowy na terminusa**:
    * _Aspekty_: widział wiele starć, nieco słabszy niż młodzi, autorytet należny wiekowi, zaskakująca paleta rozwiązań, adaptacja do potrzeb
    * _Opis_: Kajetan jest terminusem starszym niż przeciętny. Nie ma już tego refleksu i tej wytrzymałości co kiedyś - ale brak osobistej siły rekompensuje sztuczkami i sprzętem niedostępnym młodym narwanym terminusom.
* **Symbol bez skazy**: 
    * _Aspekty_: nadmiernie dobra reputacja, uważany za naprawdę prawego, "jesteś legendą i to zaszczyt z Tobą walczyć", "to co robisz jest tym co jest prawe"
    * _Opis_: Kajetan jest dość znanym terminusem; przewinął się w wielu miejscach i dowodził wieloma magami. To, w połączeniu z efektownym stylem walki sprawia, że nabawił się reputację lepszego wojownika i taktyka niż jest naprawdę. Jest osobą prawą i godną szacunku, ale czasem młodzi terminusi mają tendencję do bezmyślnego podążania za jego słowami. A z drugiej strony - pokonanie go to zaszczyt.
* ****:
    * _Aspekty_: 
    * _Opis_: 

## Magia

### Szkoły magiczne

* **Magia mentalna**:
    * _Aspekty_: wzmacnianie przekazu, czyszczenie pamięci, tarcze mentalne, hipnotyczne glify
    * _Opis_: 
* **Kataliza**: 
    * _Aspekty_: infuzje materii, pułapki magiczne, kontrzaklęcia, Skażenie zaklęcia
    * _Opis_: 
* **Magia materii**:
    * _Aspekty_: tworzenie i wzmacnianie broni i pancerzy, taktyczna przebudowa terenu, pułapki magiczne
    * _Opis_: 

### Zaklęcia statyczne

* **Pułapka ruchomych piasków**:
    * _Aspekty_: kataliza + materia, ukryte pułapki, unieruchamianie
    * _Opis_: Zaklęcie to zmienia stały teren w zbiór miejsc stabilnych i jak ruchome piaski. Ten teren ciągle faluje i się zmienia. Kajetan wie jak to wygląda, więc jest w stanie uzyskać ogromną przewagę nad wrogimi siłami - lub zwyczajnie zamknąć kogoś w gruncie, by nie miał jak uciec.
	
	
## Otoczenie

### Powiązane frakcje

{{ page.factions }}
* szeroko rozumiani terminusi
* Rycerze Iglicy, założona przez siebie grupa kierowani prawością
* niewielki odłam rodu Weiner

### Zarobki

* datki z różnych gildii i z rodu
* organizowanie imprez rekonstrukcyjnych, walka pozorowana
* praca terminusa, szkolenie młodszych terminusów, konsultacje terminuskie

### Znam

* **Członek małego odprysku rodu Weiner**:
    * _Aspekty_: ogólna wiedza o działaniach Weinerów, dostęp do niektórych haseł i kodów kontrolnych, eksperymentalny sprzęt i inkantacje
    * _Opis_: Kajetan pochodzi z wielkopolskiego odłamu rodu Weiner; jest szanowany w swoim rodzie i lubiany. Czasem proszą go o pomoc, czasem on prosi o jakąś wiedzę czy sprzęt... tak czy inaczej, Kajetan jest dumny ze swojej rodziny, choć żałuje, że nikt nie poszedł bezpośrednio w jego ślady
* **Szeroko rozumiani terminusi**:
    * _Aspekty_: poważany acz płytka znajomość, może przejąć dowodzenie, poświadczą jego słowa, sprawdzą jego plotkę
    * _Opis_: Kajetan jest znany i rozpoznawany wśród innych terminusów. Poświadczą jego słowa i oddadzą się pod jego dowodzenie - ale mimo wszystko jest to znajomość stosunkowo pobieżna; nie jest tak, że nie musi niczego udowodnić ani że z definicji i założenia ma rację.
* **Rycerze Iglicy**:
    * _Aspekty_: zgodni z zasadami Kajetana, kompetentni terminusi, wiedza z różnych gildii, współpraca po hipernecie
    * _Opis_: Kajetan założył Rycerzy Iglicy by mieć siły wspierające go w walce z rozumianym przez siebie złem i nieprawością. Ci magowie najczęściej też są terminusami i najczęściej w jakimś stopniu byli przeszkoleni przez Kajetana. Pochodzą z różnych gildii i różnych obszarów; ważne, że chcą współpracować a ogniwem ich spajającym jest Kajetan.

### Mam

* **Doskonale przygotowany sprzęt do sytuacji**:
    * _Aspekty_: łuk i strzały, miecz i tarcza, pancerz, dostosowany do przeciwnika materiałem infuzją i własnościami, zgodnie z badaniami przeciwnika, self-activator
    * _Opis_: Przed jakąkolwiek akcją Kajetan się solidnie przygotowuje - dostosowuje sprzęt do stylu walki przeciwnika, jego słabości i sił oraz odpowiednio podchodzi do samej akcji. Część tego sprzętu działa według trybu self-activator, by nie wzbudzać zbędnego zainteresowania.
* **Artefakty bojowe terminusa... i mniej typowe**:
    * _Aspekty_: eliksiry i stymulanty, różdżki, ogromna kolekcja zebrana podczas przygód, często rzadkie i egzotyczne
    * _Opis_: Kajetan nie oszczędza na sprzęcie. Dużą część finansów wydaje kolekcjonując rzadkie artefakty i eliksiry - lub ich składniki tylko po to, by w odpowiednim momencie móc je wykorzystać podczas walki.
* ****:
    * _Aspekty_: 
    * _Opis_: 


# Opis

A very pleasant terminus who acts as if he was far younger than he really is (while he is 44 years old). Although kind of clumsy, he has a sharp mind and a sharp claw. Tends to daydream a bit, he has a lot of goals of becoming a powerful knight and serving others for the greater good; even if not one of the most talented magi, he is hard-working and is quite good in assessing the difficulty of the situation. He is in good standing in his family and had access to some artifacts and tools of power. A very friendly and likable person; does not see humans as his equals, but does not use them as a resource either. 

Has an ax to grind with Diakon family because of an embarrassing past event while he was trying to analyze an artifact from them. A friend of a lovely Maus lady; always says that the guild does not determine the personality of a mage and the looks at magi as individuals, not as collective “oh you belong to this bloodline” stuff.

### Koncept

Coś pomiędzy Drussem Legendą a wujkiem rycerzem z Flipside. Dodajmy szczyptę faktycznego paladyna a w zainteresowaniach efektowna walka, rekonstrukcja i bard... i mamy starszego terminusa, który nie tylko nadal może, ale dodatkowo radzi sobie lepiej niż świetnie zmieniając cały świat dookoła siebie na swoją modłę.

Idea Kajetana jest taka - jest to postać "larger than himself". Każdy jego ruch wiąże się z pewnymi implikacjami dla jego celów (następcy). Jego podejście ma sporo niuansów, ale Kajetan faktycznie nie dopuści do cierpienia niewinnych i jest osobą naprawdę dobrą. Świetne wsparcie dla postaci graczy lub cierpienie, jeśli akurat są po bardziej szarej stronie. Paladyn zrobiony dobrze.

### Motto

"Tekst"

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Tylko nieświadomy elemental](/rpg/inwazja/opowiesci/konspekty/171022-tylko-nieswiadomy-elemental.html)|za skuteczne usunięcie elementala na portalisku tym co miał (i dowodzenie) został doceniony przez mafię i Świecę|Wizja Dukata|
|[Presja ze strony Czelimina](/rpg/inwazja/opowiesci/konspekty/161229-presja-ze-strony-czelimina.html)|ma bardzo złą opinię o Silurii Diakon z uwagi na wydarzenia w Kompleksie Centralnym i Infensę|Powrót Karradraela|


## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|171229|pojawił się by posprzątać po pojawieniu się Wyssańca Esuriit na Primusie. Usunął ślady, dał alibi ludziom...|[Esuriit w Półdarze](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|11/10/07|11/10/09|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171029|który był informowany przez Paulinę o Eliksirze Miłości i o Harvesterze. Zostanie twarzą walki z Harvesterem, bo Paulina nie może.|[W co gra Sądeczny?](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html)|11/09/20|11/09/22|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171024|wywarł wrażenie na Kocieborze gdy był wezwany do osłony Wieży Wichrów podczas eksperymentów Kociebora odnośnie tego co tam się dzieje podczas burzy.|[Detektyw, lecz nie Sądecznego](/rpg/inwazja/opowiesci/konspekty/171024-detektyw-lecz-nie-sadecznego.html)|11/09/16|11/09/18|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171022|terminus ostatniej szansy Pauliny. Wezwany do dramatycznej obrony portaliska przed dopakowanym elementalem; nie tylko wygrał ale i zapunktował. I miał cenne obserwacje.|[Tylko nieświadomy elemental](/rpg/inwazja/opowiesci/konspekty/171022-tylko-nieswiadomy-elemental.html)|11/09/12|11/09/14|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|171010|Paulina przekazała mu temat Katii Grajek skrzywdzonej w okolicy. Zsynchronizował wiedzę z Pauliną.|[Jasny sygnał Tamary](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|11/09/06|11/09/09|[Wizja Dukata](/rpg/inwazja/opowiesci/konspekty/kampania-wizja-dukata.html)|
|170212|terminus ciężko doświadczony przez Spustoszenie, przez co bardzo nieufny wobec Draceny. Na szczęście, rozważny i sympatyczny jak zawsze.|[Nieufność w małym mieście](/rpg/inwazja/opowiesci/konspekty/170212-nieufnosc-w-malym-miescie.html)|10/07/22|10/07/25|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|
|170101|osłaniał Alojzego Przylaza do Żonkiboru, by stworzyć Ogniwo Arazille. Potem z Lidią Weiner opuścił oddział Andrei.|[Patrol? Kralotyczne maskowanie!](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|10/07/13|10/07/15|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161204|niechętny całej sprawie i zgniłym kompromisom. W ostrym ścięciu z kralothem i gildią * viciniusów. Andrea jednak go utrzymała, acz jest to coraz trudniejsze.|[Zajcewowie po drugiej stronie](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html)|10/07/05|10/07/07|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161120|rozbudowywał defensywy, badał Pryzmat zamku (niekorzystny wynik) i pojechał osłaniać Andreę na spotkanie z Draconisem.|[Tak wygrywa się sojuszami](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html)|10/07/02|10/07/04|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160420|który domyślił się, że Paulina nie mówi mu całej prawdy, lecz zdecydował się jej pomóc (acz powoli i ostrożnie).|[Kolizja dwóch sojuszy](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html)|10/07/01|10/07/02|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161113|porusza się dobrze w świecie ludzi i z Andreą umie przebadać subtelności Czelimińskiego Pryzmatu. Współpracuje z Pauliną przez telefon.|[Świeca nie zostawia swoich](/rpg/inwazja/opowiesci/konspekty/161113-swieca-nie-zostawia-swoich.html)|10/06/29|10/07/01|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161101|którego nikt ze Świecy ani z Drugiej Strony nie zna; może poruszać się po okolicy bezpiecznie (i to robi).|[Bezwzględna Lady Terminus](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|10/06/25|10/06/28|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161030|uratował Andreę załatwiając Mieszka mandoliną. Oddał się pod dowództwo Andrei.|[Odbudowa dowodzenia Świecy](/rpg/inwazja/opowiesci/konspekty/161030-odbudowa-dowodzenia-swiecy.html)|10/06/22|10/06/24|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160424|szukający Krwawej Kuźni * mag nie należący do Świecy; ostrożny, powiedział Andrei co wie na temat Kuźni gdy upewnił się, że ta z Wydziału Wew.|[Uważaj, o czym marzysz](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|10/06/18|10/06/21|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170312|jedyny terminus w okolicy. Pomysłodawca niebezpiecznego planu odbicia Oksany z Pauliną rozpraszającą "Harvester". Nadal chce pomóc ludziom (nie są winni działań "Harvestera").|[Przebudzony... Harvester?](/rpg/inwazja/opowiesci/konspekty/170312-przebudzony-harvester.html)|10/03/17|10/03/19|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|
|160227|który stoi po stronie Pauliny, ludzi i zasad terminusa a nie po stronie swojego rodu.|[Zakazany harvester](/rpg/inwazja/opowiesci/konspekty/160227-zakazany-harvester.html)|10/03/12|10/03/13|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|
|160131|lekko postrzelony terminus o zapędach rycerza który z przyjemnością współpracuje z Pauliną oraz faktycznie umie walczyć.|[Dziwny transmiter Weinerów](/rpg/inwazja/opowiesci/konspekty/160131-dziwny-transmiter-weinerow.html)|10/03/10|10/03/11|[Rezydentka Krukowa](/rpg/inwazja/opowiesci/konspekty/kampania-rezydentka-krukowa.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|10|[171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html), [171024](/rpg/inwazja/opowiesci/konspekty/171024-detektyw-lecz-nie-sadecznego.html), [171022](/rpg/inwazja/opowiesci/konspekty/171022-tylko-nieswiadomy-elemental.html), [171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html), [170212](/rpg/inwazja/opowiesci/konspekty/170212-nieufnosc-w-malym-miescie.html), [160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [161113](/rpg/inwazja/opowiesci/konspekty/161113-swieca-nie-zostawia-swoich.html), [170312](/rpg/inwazja/opowiesci/konspekty/170312-przebudzony-harvester.html), [160227](/rpg/inwazja/opowiesci/konspekty/160227-zakazany-harvester.html), [160131](/rpg/inwazja/opowiesci/konspekty/160131-dziwny-transmiter-weinerow.html)|
|[Andrea Wilgacz](/rpg/inwazja/opowiesci/karty-postaci/1709-andrea-wilgacz.html)|7|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html), [161113](/rpg/inwazja/opowiesci/konspekty/161113-swieca-nie-zostawia-swoich.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html), [161030](/rpg/inwazja/opowiesci/konspekty/161030-odbudowa-dowodzenia-swiecy.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Tadeusz Baran](/rpg/inwazja/opowiesci/karty-postaci/1709-tadeusz-baran.html)|6|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html), [161113](/rpg/inwazja/opowiesci/konspekty/161113-swieca-nie-zostawia-swoich.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html), [161030](/rpg/inwazja/opowiesci/konspekty/161030-odbudowa-dowodzenia-swiecy.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Rafael Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-rafael-diakon.html)|6|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html), [161113](/rpg/inwazja/opowiesci/konspekty/161113-swieca-nie-zostawia-swoich.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html), [161030](/rpg/inwazja/opowiesci/konspekty/161030-odbudowa-dowodzenia-swiecy.html)|
|[Mieszko Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-mieszko-bankierz.html)|6|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html), [161113](/rpg/inwazja/opowiesci/konspekty/161113-swieca-nie-zostawia-swoich.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html), [161030](/rpg/inwazja/opowiesci/konspekty/161030-odbudowa-dowodzenia-swiecy.html)|
|[Lidia Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-lidia-weiner.html)|6|[170212](/rpg/inwazja/opowiesci/konspekty/170212-nieufnosc-w-malym-miescie.html), [170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html), [161030](/rpg/inwazja/opowiesci/konspekty/161030-odbudowa-dowodzenia-swiecy.html)|
|[Julian Pszczelak](/rpg/inwazja/opowiesci/karty-postaci/1709-julian-pszczelak.html)|6|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html), [161113](/rpg/inwazja/opowiesci/konspekty/161113-swieca-nie-zostawia-swoich.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Andżelika Leszczyńska](/rpg/inwazja/opowiesci/karty-postaci/1709-andzelika-leszczynska.html)|6|[171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html), [170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html), [161113](/rpg/inwazja/opowiesci/konspekty/161113-swieca-nie-zostawia-swoich.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|[Marian Łajdak](/rpg/inwazja/opowiesci/karty-postaci/1709-marian-lajdak.html)|5|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html), [161113](/rpg/inwazja/opowiesci/konspekty/161113-swieca-nie-zostawia-swoich.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|[Marian Agrest](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-agrest.html)|4|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html), [161030](/rpg/inwazja/opowiesci/konspekty/161030-odbudowa-dowodzenia-swiecy.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|4|[171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html), [171022](/rpg/inwazja/opowiesci/konspekty/171022-tylko-nieswiadomy-elemental.html), [171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html), [170212](/rpg/inwazja/opowiesci/konspekty/170212-nieufnosc-w-malym-miescie.html)|
|[Rudolf Jankowski](/rpg/inwazja/opowiesci/karty-postaci/9999-rudolf-jankowski.html)|3|[161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html), [161113](/rpg/inwazja/opowiesci/konspekty/161113-swieca-nie-zostawia-swoich.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/1709-maria-newa.html)|3|[171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html), [171022](/rpg/inwazja/opowiesci/konspekty/171022-tylko-nieswiadomy-elemental.html), [170212](/rpg/inwazja/opowiesci/konspekty/170212-nieufnosc-w-malym-miescie.html)|
|[Laragnarhag](/rpg/inwazja/opowiesci/karty-postaci/1709-laragnarhag.html)|3|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Efemeryda Senesgradzka](/rpg/inwazja/opowiesci/karty-postaci/9999-efemeryda-senesgradzka.html)|3|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html), [171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html), [171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Dalia Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-dalia-weiner.html)|3|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html)|
|[Aneta Rainer](/rpg/inwazja/opowiesci/karty-postaci/1709-aneta-rainer.html)|3|[161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html), [160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html)|
|[Świeży Lilak](/rpg/inwazja/opowiesci/karty-postaci/9999-swiezy-lilak.html)|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html)|
|[Wioletta Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-wioletta-bankierz.html)|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Wiktor Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1709-wiktor-sowinski.html)|2|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Sylwester Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-sylwester-bankierz.html)|2|[171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html), [171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Stalowy Śledzik Żarłacz](/rpg/inwazja/opowiesci/karty-postaci/9999-stalowy-sledzik-zarlacz.html)|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html)|
|[Robert Sądeczny](/rpg/inwazja/opowiesci/karty-postaci/1709-robert-sadeczny.html)|2|[171024](/rpg/inwazja/opowiesci/konspekty/171024-detektyw-lecz-nie-sadecznego.html), [171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Ozydiusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-ozydiusz-bankierz.html)|2|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Oktawia Aurinus](/rpg/inwazja/opowiesci/karty-postaci/1709-oktawia-aurinus.html)|2|[171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html), [171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Melodia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-melodia-diakon.html)|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html)|
|[Krzysztof Grumrzyk](/rpg/inwazja/opowiesci/karty-postaci/1709-krzysztof-grumrzyk.html)|2|[171024](/rpg/inwazja/opowiesci/konspekty/171024-detektyw-lecz-nie-sadecznego.html), [171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Joachim Kopiec](/rpg/inwazja/opowiesci/karty-postaci/9999-joachim-kopiec.html)|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|[Ilona Maczatek](/rpg/inwazja/opowiesci/karty-postaci/9999-ilona-maczatek.html)|2|[170312](/rpg/inwazja/opowiesci/konspekty/170312-przebudzony-harvester.html), [160227](/rpg/inwazja/opowiesci/konspekty/160227-zakazany-harvester.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|2|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Franciszek Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-myszeczka.html)|2|[161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html)|
|[Fortitia Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-fortitia-diakon.html)|2|[161030](/rpg/inwazja/opowiesci/konspekty/161030-odbudowa-dowodzenia-swiecy.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Filip Keramiusz](/rpg/inwazja/opowiesci/karty-postaci/9999-filip-keramiusz.html)|2|[171024](/rpg/inwazja/opowiesci/konspekty/171024-detektyw-lecz-nie-sadecznego.html), [171022](/rpg/inwazja/opowiesci/konspekty/171022-tylko-nieswiadomy-elemental.html)|
|[Felicjan Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-felicjan-weiner.html)|2|[161113](/rpg/inwazja/opowiesci/konspekty/161113-swieca-nie-zostawia-swoich.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|[Elea Maus](/rpg/inwazja/opowiesci/karty-postaci/1802-elea-maus.html)|2|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Draconis Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-draconis-diakon.html)|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html)|
|[Dagmara Wyjątek](/rpg/inwazja/opowiesci/karty-postaci/1709-dagmara-wyjątek.html)|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Barbara Zacieszek](/rpg/inwazja/opowiesci/karty-postaci/9999-barbara-zacieszek.html)|2|[160227](/rpg/inwazja/opowiesci/konspekty/160227-zakazany-harvester.html), [160131](/rpg/inwazja/opowiesci/konspekty/160131-dziwny-transmiter-weinerow.html)|
|[Anna Kozak](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-kozak.html)|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|[Amanda Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-amanda-diakon.html)|2|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Alojzy Przylaz](/rpg/inwazja/opowiesci/karty-postaci/1803-alojzy-przylaz.html)|2|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html), [161101](/rpg/inwazja/opowiesci/konspekty/161101-bezwzgledna-lady-terminus.html)|
|[Żaklina Bąk](/rpg/inwazja/opowiesci/karty-postaci/1709-zaklina-bak.html)|1|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|
|[Wojciech Piekarz](/rpg/inwazja/opowiesci/karty-postaci/9999-wojciech-piekarz.html)|1|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|
|[Tymotheus Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-tymotheus-blakenbauer.html)|1|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html)|
|[Tomasz Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-tomasz-myszeczka.html)|1|[171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html)|
|[Tomasz Kapelusz](/rpg/inwazja/opowiesci/karty-postaci/9999-tomasz-kapelusz.html)|1|[171024](/rpg/inwazja/opowiesci/konspekty/171024-detektyw-lecz-nie-sadecznego.html)|
|[Tatiana Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1709-tatiana-zajcew.html)|1|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|
|[Tamara Muszkiet](/rpg/inwazja/opowiesci/karty-postaci/1709-tamara-muszkiet.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Tadek Swołczan](/rpg/inwazja/opowiesci/karty-postaci/9999-tadek-swolczan.html)|1|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|
|[Szymon Łokciak](/rpg/inwazja/opowiesci/karty-postaci/9999-szymon-lokciak.html)|1|[171022](/rpg/inwazja/opowiesci/konspekty/171022-tylko-nieswiadomy-elemental.html)|
|[Stefania Kołek](/rpg/inwazja/opowiesci/karty-postaci/9999-stefania-kolek.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Sabina Sowińska](/rpg/inwazja/opowiesci/karty-postaci/1709-sabina-sowinska.html)|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Ryszard Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-ryszard-weiner.html)|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Rodion Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-rodion-zajcew.html)|1|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|
|[Rebeka Czomnik](/rpg/inwazja/opowiesci/karty-postaci/9999-rebeka-czomnik.html)|1|[160227](/rpg/inwazja/opowiesci/konspekty/160227-zakazany-harvester.html)|
|[Patryk Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-patryk-maus.html)|1|[170312](/rpg/inwazja/opowiesci/konspekty/170312-przebudzony-harvester.html)|
|[Pamela Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-pamela-weiner.html)|1|[170312](/rpg/inwazja/opowiesci/konspekty/170312-przebudzony-harvester.html)|
|[Onufry Letniczek](/rpg/inwazja/opowiesci/karty-postaci/9999-onufry-letniczek.html)|1|[160131](/rpg/inwazja/opowiesci/konspekty/160131-dziwny-transmiter-weinerow.html)|
|[Oliwia Aurinus](/rpg/inwazja/opowiesci/karty-postaci/1709-oliwia-aurinus.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Olga Miodownik](/rpg/inwazja/opowiesci/karty-postaci/1709-olga-miodownik.html)|1|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html)|
|[Oksana Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-oksana-weiner.html)|1|[170312](/rpg/inwazja/opowiesci/konspekty/170312-przebudzony-harvester.html)|
|[Mirabelka Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-mirabelka-diakon.html)|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Mariusz Tłuk](/rpg/inwazja/opowiesci/karty-postaci/9999-mariusz-tluk.html)|1|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|
|[Mariola Miłżoś](/rpg/inwazja/opowiesci/karty-postaci/9999-mariola-milzos.html)|1|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|
|[Marcin Warinsky](/rpg/inwazja/opowiesci/karty-postaci/1709-marcin-warinsky.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Marcelin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-marcelin-blakenbauer.html)|1|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html)|
|[Maja Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-maja-weiner.html)|1|[171024](/rpg/inwazja/opowiesci/konspekty/171024-detektyw-lecz-nie-sadecznego.html)|
|[Maja Stomaniek](/rpg/inwazja/opowiesci/karty-postaci/9999-maja-stomaniek.html)|1|[160227](/rpg/inwazja/opowiesci/konspekty/160227-zakazany-harvester.html)|
|[Leonidas Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-leonidas-blakenbauer.html)|1|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html)|
|[Laurena Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-laurena-bankierz.html)|1|[170101](/rpg/inwazja/opowiesci/konspekty/170101-patrol-kralotyczne-maskowanie.html)|
|[Laetitia Gaia Rasputin Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-laetitia-gaia-rasputin-weiner.html)|1|[170212](/rpg/inwazja/opowiesci/konspekty/170212-nieufnosc-w-malym-miescie.html)|
|[Kurt Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-kurt-weiner.html)|1|[170212](/rpg/inwazja/opowiesci/konspekty/170212-nieufnosc-w-malym-miescie.html)|
|[Krzysztof Tłuk](/rpg/inwazja/opowiesci/karty-postaci/9999-krzysztof-tluk.html)|1|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|
|[Krzysiu Miłżoś](/rpg/inwazja/opowiesci/karty-postaci/9999-krzysiu-milzos.html)|1|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|
|[Kociebor Dyrygent](/rpg/inwazja/opowiesci/karty-postaci/1709-kociebor-dyrygent.html)|1|[171024](/rpg/inwazja/opowiesci/konspekty/171024-detektyw-lecz-nie-sadecznego.html)|
|[Klara Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-klara-blakenbauer.html)|1|[160420](/rpg/inwazja/opowiesci/konspekty/160420-kolizja-dwoch-sojuszy.html)|
|[Kirył Sjeld](/rpg/inwazja/opowiesci/karty-postaci/9999-kiryl-sjeld.html)|1|[161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html)|
|[Kazimierz Przybylec](/rpg/inwazja/opowiesci/karty-postaci/9999-kazimierz-przybylec.html)|1|[160131](/rpg/inwazja/opowiesci/konspekty/160131-dziwny-transmiter-weinerow.html)|
|[Karol Marzyciel](/rpg/inwazja/opowiesci/karty-postaci/1709-karol-marzyciel.html)|1|[171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html)|
|[Kaja Maślaczek](/rpg/inwazja/opowiesci/karty-postaci/1709-kaja-maslaczek.html)|1|[171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html)|
|[Jolanta Sowińska](/rpg/inwazja/opowiesci/karty-postaci/1709-jolanta-sowinska.html)|1|[161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html)|
|[Janusz Krzykoł](/rpg/inwazja/opowiesci/karty-postaci/9999-janusz-krzykol.html)|1|[170212](/rpg/inwazja/opowiesci/konspekty/170212-nieufnosc-w-malym-miescie.html)|
|[Jakub Dobrocień](/rpg/inwazja/opowiesci/karty-postaci/1709-jakub-dobrocien.html)|1|[171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html)|
|[Jagoda Kozak](/rpg/inwazja/opowiesci/karty-postaci/1709-jagoda-kozak.html)|1|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|
|[Jacek Molenda](/rpg/inwazja/opowiesci/karty-postaci/9999-jacek-molenda.html)|1|[161204](/rpg/inwazja/opowiesci/konspekty/161204-zajcewowie-po-drugiej-stronie.html)|
|[Hektor Reszniaczek](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-reszniaczek.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Halina Krzyżanowska](/rpg/inwazja/opowiesci/karty-postaci/9999-halina-krzyzanowska.html)|1|[161113](/rpg/inwazja/opowiesci/konspekty/161113-swieca-nie-zostawia-swoich.html)|
|[Filip Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-filip-weiner.html)|1|[170312](/rpg/inwazja/opowiesci/konspekty/170312-przebudzony-harvester.html)|
|[Feliks Szczęśliwiec](/rpg/inwazja/opowiesci/karty-postaci/9999-feliks-szczesliwiec.html)|1|[160227](/rpg/inwazja/opowiesci/konspekty/160227-zakazany-harvester.html)|
|[Eustachy Szipinik](/rpg/inwazja/opowiesci/karty-postaci/9999-eustachy-szipinik.html)|1|[160227](/rpg/inwazja/opowiesci/konspekty/160227-zakazany-harvester.html)|
|[Edward Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-edward-diakon.html)|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Diana Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-diana-weiner.html)|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Deiiw Podniebny Grom](/rpg/inwazja/opowiesci/karty-postaci/9999-deiiw-podniebny-grom.html)|1|[161120](/rpg/inwazja/opowiesci/konspekty/161120-tak-wygrywa-sie-sojuszami.html)|
|[Daniel Stryczek](/rpg/inwazja/opowiesci/karty-postaci/9999-daniel-stryczek.html)|1|[160227](/rpg/inwazja/opowiesci/konspekty/160227-zakazany-harvester.html)|
|[Daniel Akwitański](/rpg/inwazja/opowiesci/karty-postaci/1709-daniel-akwitanski.html)|1|[171010](/rpg/inwazja/opowiesci/konspekty/171010-jasny-sygnal-tamary.html)|
|[Bólokłąb](/rpg/inwazja/opowiesci/karty-postaci/9999-boloklab.html)|1|[160131](/rpg/inwazja/opowiesci/konspekty/160131-dziwny-transmiter-weinerow.html)|
|[Bonifacy Jeż](/rpg/inwazja/opowiesci/karty-postaci/9999-bonifacy-jez.html)|1|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|
|[Bolesław Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-boleslaw-maus.html)|1|[171029](/rpg/inwazja/opowiesci/konspekty/171029-w-co-gra-sadeczny.html)|
|[Aurel Czarko](/rpg/inwazja/opowiesci/karty-postaci/1709-aurel-czarko.html)|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[August Paszkwil](/rpg/inwazja/opowiesci/karty-postaci/1709-august-paszkwil.html)|1|[171229](/rpg/inwazja/opowiesci/konspekty/171229-esuriit-w-poldarze.html)|
|[Aleksander Sowiński](/rpg/inwazja/opowiesci/karty-postaci/9999-aleksander-sowinski.html)|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Adam Kapelusz](/rpg/inwazja/opowiesci/karty-postaci/9999-adam-kapelusz.html)|1|[171024](/rpg/inwazja/opowiesci/konspekty/171024-detektyw-lecz-nie-sadecznego.html)|

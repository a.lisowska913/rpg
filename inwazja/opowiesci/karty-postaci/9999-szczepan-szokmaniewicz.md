---
layout: inwazja-karta-postaci
categories: profile
title: "Szczepan Szokmaniewicz"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|161220|bibliotekarz, straszny gaduła i plotkarz. Bardzo lubi Ulę Kram; wstawiał się za nią. Sympatyzuje z femisatanistkami.|[Zniszczenie posągu Arazille](/rpg/inwazja/opowiesci/konspekty/161220-zniszczenie-posagu-arazille.html)|10/07/27|10/07/30|[Nicaretta](/rpg/inwazja/opowiesci/konspekty/kampania-nicaretta.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Urszula Kram](/rpg/inwazja/opowiesci/karty-postaci/9999-urszula-kram.html)|1|[161220](/rpg/inwazja/opowiesci/konspekty/161220-zniszczenie-posagu-arazille.html)|
|[Romana Kaldwor](/rpg/inwazja/opowiesci/karty-postaci/9999-romana-kaldwor.html)|1|[161220](/rpg/inwazja/opowiesci/konspekty/161220-zniszczenie-posagu-arazille.html)|
|[Natalia Kaldwor](/rpg/inwazja/opowiesci/karty-postaci/9999-natalia-kaldwor.html)|1|[161220](/rpg/inwazja/opowiesci/konspekty/161220-zniszczenie-posagu-arazille.html)|
|[Marzena Dorszaj](/rpg/inwazja/opowiesci/karty-postaci/1709-marzena-dorszaj.html)|1|[161220](/rpg/inwazja/opowiesci/konspekty/161220-zniszczenie-posagu-arazille.html)|
|[Karina Łoszad](/rpg/inwazja/opowiesci/karty-postaci/9999-karina-loszad.html)|1|[161220](/rpg/inwazja/opowiesci/konspekty/161220-zniszczenie-posagu-arazille.html)|
|[Henryk Siwiecki](/rpg/inwazja/opowiesci/karty-postaci/1709-henryk-siwiecki.html)|1|[161220](/rpg/inwazja/opowiesci/konspekty/161220-zniszczenie-posagu-arazille.html)|
|[Arazille](/rpg/inwazja/opowiesci/karty-postaci/9999-arazille.html)|1|[161220](/rpg/inwazja/opowiesci/konspekty/161220-zniszczenie-posagu-arazille.html)|
|[Anna Osiaczek](/rpg/inwazja/opowiesci/karty-postaci/9999-anna-osiaczek.html)|1|[161220](/rpg/inwazja/opowiesci/konspekty/161220-zniszczenie-posagu-arazille.html)|
|[Andrzej Klepiczek](/rpg/inwazja/opowiesci/karty-postaci/9999-andrzej-klepiczek.html)|1|[161220](/rpg/inwazja/opowiesci/konspekty/161220-zniszczenie-posagu-arazille.html)|

---
layout: inwazja-karta-postaci
categories: profile
title: "Konrad Węgorz"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|150602|astralika # iluzje, mag Triumwiratu który jest szantażowany przez Gabriela (co powiedział Esme by miała jeszcze jedną wtykę).|[Esme, najemniczka Netherii](/rpg/inwazja/opowiesci/konspekty/150602-esme-najemniczka-netherii.html)|10/02/19|10/02/20|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|
|150121|40~50*letni mag Millennium (astralika + iluzje) który próbuje odnaleźć Martę Newę. Cel Borysa i Aliny.|[Nowe życie Aliny](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|10/01/15|10/01/16|[Blakenbauerowie x Skorpion](/rpg/inwazja/opowiesci/konspekty/kampania-blakenbauerowie-x-skorpion.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Netheria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-netheria-diakon.html)|2|[150602](/rpg/inwazja/opowiesci/konspekty/150602-esme-najemniczka-netherii.html), [150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Zofia Perszen](/rpg/inwazja/opowiesci/karty-postaci/9999-zofia-perszen.html)|1|[150602](/rpg/inwazja/opowiesci/konspekty/150602-esme-najemniczka-netherii.html)|
|[Tymotheus Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-tymotheus-blakenbauer.html)|1|[150602](/rpg/inwazja/opowiesci/konspekty/150602-esme-najemniczka-netherii.html)|
|[Tadeusz Umiej](/rpg/inwazja/opowiesci/karty-postaci/9999-tadeusz-umiej.html)|1|[150602](/rpg/inwazja/opowiesci/konspekty/150602-esme-najemniczka-netherii.html)|
|[Staszek Perszen](/rpg/inwazja/opowiesci/karty-postaci/9999-staszek-perszen.html)|1|[150602](/rpg/inwazja/opowiesci/konspekty/150602-esme-najemniczka-netherii.html)|
|[Sebastian Tecznia](/rpg/inwazja/opowiesci/karty-postaci/9999-sebastian-tecznia.html)|1|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Robert Przerot](/rpg/inwazja/opowiesci/karty-postaci/9999-robert-przerot.html)|1|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Patryk Romczak](/rpg/inwazja/opowiesci/karty-postaci/9999-patryk-romczak.html)|1|[150602](/rpg/inwazja/opowiesci/konspekty/150602-esme-najemniczka-netherii.html)|
|[Mirabelka Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-mirabelka-diakon.html)|1|[150602](/rpg/inwazja/opowiesci/konspekty/150602-esme-najemniczka-netherii.html)|
|[Marysia Kiras](/rpg/inwazja/opowiesci/karty-postaci/9999-marysia-kiras.html)|1|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Marta Newa](/rpg/inwazja/opowiesci/karty-postaci/9999-marta-newa.html)|1|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Margaret Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-margaret-blakenbauer.html)|1|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Luksja Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-luksja-diakon.html)|1|[150602](/rpg/inwazja/opowiesci/konspekty/150602-esme-najemniczka-netherii.html)|
|[Klemens X](/rpg/inwazja/opowiesci/karty-postaci/9999-klemens-x.html)|1|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Gabriel Newa](/rpg/inwazja/opowiesci/karty-postaci/9999-gabriel-newa.html)|1|[150602](/rpg/inwazja/opowiesci/konspekty/150602-esme-najemniczka-netherii.html)|
|[Filip Sztukar](/rpg/inwazja/opowiesci/karty-postaci/9999-filip-sztukar.html)|1|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Esme Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-esme-myszeczka.html)|1|[150602](/rpg/inwazja/opowiesci/konspekty/150602-esme-najemniczka-netherii.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|1|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Crystal Shard](/rpg/inwazja/opowiesci/karty-postaci/9999-crystal-shard.html)|1|[150602](/rpg/inwazja/opowiesci/konspekty/150602-esme-najemniczka-netherii.html)|
|[Borys Kumin](/rpg/inwazja/opowiesci/karty-postaci/1709-borys-kumin.html)|1|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Artur Żupan](/rpg/inwazja/opowiesci/karty-postaci/1803-artur-zupan.html)|1|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|
|[Alina Bednarz](/rpg/inwazja/opowiesci/karty-postaci/1803-alina-bednarz.html)|1|[150121](/rpg/inwazja/opowiesci/konspekty/150121-nowe-zycie-aliny.html)|

---
layout: inwazja-karta-postaci
categories: profile
title: "Izabela Kruczek"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|130506|matka chłopaka jej zdaniem niesłusznie oskarżonego o ciężkie pobicie z winy Ingi Wójt.|[Sekrety Rezydencji Szczypiorkow](/rpg/inwazja/opowiesci/konspekty/130506-sekrety-rezydencji-szczypiorkow.html)|10/02/01|10/02/02|[Czarodziejka Luster](/rpg/inwazja/opowiesci/konspekty/kampania-czarodziejka-luster.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Żanna Szczypiorek](/rpg/inwazja/opowiesci/karty-postaci/9999-zanna-szczypiorek.html)|1|[130506](/rpg/inwazja/opowiesci/konspekty/130506-sekrety-rezydencji-szczypiorkow.html)|
|[Sandra Stryjek](/rpg/inwazja/opowiesci/karty-postaci/1709-sandra-stryjek.html)|1|[130506](/rpg/inwazja/opowiesci/konspekty/130506-sekrety-rezydencji-szczypiorkow.html)|
|[Samira Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-samira-diakon.html)|1|[130506](/rpg/inwazja/opowiesci/konspekty/130506-sekrety-rezydencji-szczypiorkow.html)|
|[Radosław Szczypiorek](/rpg/inwazja/opowiesci/karty-postaci/9999-radoslaw-szczypiorek.html)|1|[130506](/rpg/inwazja/opowiesci/konspekty/130506-sekrety-rezydencji-szczypiorkow.html)|
|[Kasia Nowak](/rpg/inwazja/opowiesci/karty-postaci/1803-kasia-nowak.html)|1|[130506](/rpg/inwazja/opowiesci/konspekty/130506-sekrety-rezydencji-szczypiorkow.html)|
|[Jolanta Wójt](/rpg/inwazja/opowiesci/karty-postaci/9999-jolanta-wojt.html)|1|[130506](/rpg/inwazja/opowiesci/konspekty/130506-sekrety-rezydencji-szczypiorkow.html)|
|[Inga Wójt](/rpg/inwazja/opowiesci/karty-postaci/9999-inga-wojt.html)|1|[130506](/rpg/inwazja/opowiesci/konspekty/130506-sekrety-rezydencji-szczypiorkow.html)|
|[Augustyn Szczypiorek](/rpg/inwazja/opowiesci/karty-postaci/9999-augustyn-szczypiorek.html)|1|[130506](/rpg/inwazja/opowiesci/konspekty/130506-sekrety-rezydencji-szczypiorkow.html)|
|[August Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-august-bankierz.html)|1|[130506](/rpg/inwazja/opowiesci/konspekty/130506-sekrety-rezydencji-szczypiorkow.html)|

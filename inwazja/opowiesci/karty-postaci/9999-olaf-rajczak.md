---
layout: inwazja-karta-postaci
categories: profile
title: "Olaf Rajczak"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|141110|eks-KADEMowiec i przedstawiciel Trójkąta Szczęścia - demonolog - polujący na anioła. Bo tylko anioł pokona demona lorda Urbanka.|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html)|10/06/16|10/06/17|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|
|141102|eks*KADEMowiec i przedstawiciel Trójkąta Szczęścia mający nadzieję, że wbrew wszystkiemu Paulina właśnie ich wybierze.|[Paulina widziała sępy nad Nikolą](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html)|10/06/14|10/06/15|[Prawdziwa natura Draceny](/rpg/inwazja/opowiesci/konspekty/kampania-prawdziwa-natura-draceny.html)|
|141115|drugi mag rezonujący ze źródłem który obudził Spustoszenie.|[GS Aegis 0002](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|10/04/13|10/04/14|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Marian Łajdak](/rpg/inwazja/opowiesci/karty-postaci/1709-marian-lajdak.html)|3|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html), [141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
|[tien Radosław Pieśniec](/rpg/inwazja/opowiesci/karty-postaci/9999-tien-radoslaw-piesniec.html)|2|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html)|
|[Ryszard Bocian](/rpg/inwazja/opowiesci/karty-postaci/9999-ryszard-bocian.html)|2|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html)|
|[Rafael Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-rafael-diakon.html)|2|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html)|
|[Paulina Tarczyńska](/rpg/inwazja/opowiesci/karty-postaci/1805-paulina-tarczynska.html)|2|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html)|
|[Nikola Kamień](/rpg/inwazja/opowiesci/karty-postaci/9999-nikola-kamien.html)|2|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html)|
|[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/1709-maria-newa.html)|2|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html)|
|[Kazimierz Przybylec](/rpg/inwazja/opowiesci/karty-postaci/9999-kazimierz-przybylec.html)|2|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html)|
|[Katarzyna Trzosek](/rpg/inwazja/opowiesci/karty-postaci/9999-katarzyna-trzosek.html)|2|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html)|
|[Jan Bocian](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-bocian.html)|2|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html)|
|[Franciszek Marlin](/rpg/inwazja/opowiesci/karty-postaci/9999-franciszek-marlin.html)|2|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html)|
|[Dyta](/rpg/inwazja/opowiesci/karty-postaci/9999-dyta.html)|2|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|2|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html)|
|[Artur Kurczak](/rpg/inwazja/opowiesci/karty-postaci/9999-artur-kurczak.html)|2|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html)|
|[Andżelika Leszczyńska](/rpg/inwazja/opowiesci/karty-postaci/1709-andzelika-leszczynska.html)|2|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html), [141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html)|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|1|[141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
|[Silgor Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-silgor-diakon.html)|1|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html)|
|[Roman Pilen](/rpg/inwazja/opowiesci/karty-postaci/9999-roman-pilen.html)|1|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html)|
|[Robert Mięk](/rpg/inwazja/opowiesci/karty-postaci/9999-robert-miek.html)|1|[141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
|[Paweł Sępiak](/rpg/inwazja/opowiesci/karty-postaci/1709-pawel-sepiak.html)|1|[141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
|[Netheria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-netheria-diakon.html)|1|[141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
|[Mikołaj Mykot](/rpg/inwazja/opowiesci/karty-postaci/9999-mikolaj-mykot.html)|1|[141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html)|
|[Marysia Kiras](/rpg/inwazja/opowiesci/karty-postaci/9999-marysia-kiras.html)|1|[141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
|[Maria Pilen](/rpg/inwazja/opowiesci/karty-postaci/9999-maria-pilen.html)|1|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html)|
|[Krystian Linowęc](/rpg/inwazja/opowiesci/karty-postaci/9999-krystian-linowec.html)|1|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html)|
|[Juliusz Kwarc](/rpg/inwazja/opowiesci/karty-postaci/9999-juliusz-kwarc.html)|1|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html)|
|[Jan Fiołek](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-fiolek.html)|1|[141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html)|
|[Jagoda Musiąg](/rpg/inwazja/opowiesci/karty-postaci/9999-jagoda-musiag.html)|1|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html)|
|[Izabela Kamil](/rpg/inwazja/opowiesci/karty-postaci/9999-izabela-kamil.html)|1|[141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html)|
|[Irena Krysniok](/rpg/inwazja/opowiesci/karty-postaci/9999-irena-krysniok.html)|1|[141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html)|
|[GS "Aegis" 0002](/rpg/inwazja/opowiesci/karty-postaci/9999-gs-aegis-0002.html)|1|[141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
|[Estrella Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-estrella-diakon.html)|1|[141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
|[Eleonora Wiaderska](/rpg/inwazja/opowiesci/karty-postaci/9999-eleonora-wiaderska.html)|1|[141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
|[Edmund Marlin](/rpg/inwazja/opowiesci/karty-postaci/9999-edmund-marlin.html)|1|[141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html)|
|[Basia Morocz](/rpg/inwazja/opowiesci/karty-postaci/9999-basia-morocz.html)|1|[141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|
|[Bartosz Bławatek](/rpg/inwazja/opowiesci/karty-postaci/9999-bartosz-blawatek.html)|1|[141102](/rpg/inwazja/opowiesci/konspekty/141102-paulina-widziala-sepy-nad-nikola.html)|
|[Anita Rodos](/rpg/inwazja/opowiesci/karty-postaci/9999-anita-rodos.html)|1|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html)|
|[Ania i Basia Pilen](/rpg/inwazja/opowiesci/karty-postaci/9999-ania-i-basia-pilen.html)|1|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html)|
|[Alojzy Wołek](/rpg/inwazja/opowiesci/karty-postaci/9999-alojzy-wolek.html)|1|[141110](/rpg/inwazja/opowiesci/konspekty/141110-prawdziwa-natura-draceny.html)|
|[Adam Lisek](/rpg/inwazja/opowiesci/karty-postaci/9999-adam-lisek.html)|1|[141115](/rpg/inwazja/opowiesci/konspekty/141115-gs-aegis-0002.html)|

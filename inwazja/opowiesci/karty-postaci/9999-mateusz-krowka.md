---
layout: inwazja-karta-postaci
categories: profile
title: "Mateusz Krówka"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|140103|elfi potomek rodziny Krówków chętnie współpracujący z detektywami|[Tak bardzo nie artefakt](/rpg/inwazja/opowiesci/konspekty/140103-tak-bardzo-nie-artefakt.html)|10/01/03|10/01/04|[Druga Inwazja](/rpg/inwazja/opowiesci/konspekty/kampania-druga-inwazja.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Waldemar Zupaczka](/rpg/inwazja/opowiesci/karty-postaci/9999-waldemar-zupaczka.html)|1|[140103](/rpg/inwazja/opowiesci/konspekty/140103-tak-bardzo-nie-artefakt.html)|
|[Wacław Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-waclaw-zajcew.html)|1|[140103](/rpg/inwazja/opowiesci/konspekty/140103-tak-bardzo-nie-artefakt.html)|
|[Teresa Żyraf](/rpg/inwazja/opowiesci/karty-postaci/9999-teresa-zyraf.html)|1|[140103](/rpg/inwazja/opowiesci/konspekty/140103-tak-bardzo-nie-artefakt.html)|
|[Radosław Krówka](/rpg/inwazja/opowiesci/karty-postaci/9999-radoslaw-krowka.html)|1|[140103](/rpg/inwazja/opowiesci/konspekty/140103-tak-bardzo-nie-artefakt.html)|
|[Ofelia Caesar](/rpg/inwazja/opowiesci/karty-postaci/9999-ofelia-caesar.html)|1|[140103](/rpg/inwazja/opowiesci/konspekty/140103-tak-bardzo-nie-artefakt.html)|
|[Nela Welon](/rpg/inwazja/opowiesci/karty-postaci/9999-nela-welon.html)|1|[140103](/rpg/inwazja/opowiesci/konspekty/140103-tak-bardzo-nie-artefakt.html)|
|[Mordecja Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-mordecja-diakon.html)|1|[140103](/rpg/inwazja/opowiesci/konspekty/140103-tak-bardzo-nie-artefakt.html)|
|[Mateusz Nieborak](/rpg/inwazja/opowiesci/karty-postaci/9999-mateusz-nieborak.html)|1|[140103](/rpg/inwazja/opowiesci/konspekty/140103-tak-bardzo-nie-artefakt.html)|
|[Krystalia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-krystalia-diakon.html)|1|[140103](/rpg/inwazja/opowiesci/konspekty/140103-tak-bardzo-nie-artefakt.html)|
|[Emilia Szudek](/rpg/inwazja/opowiesci/karty-postaci/9999-emilia-szudek.html)|1|[140103](/rpg/inwazja/opowiesci/konspekty/140103-tak-bardzo-nie-artefakt.html)|
|[Artur Żupan](/rpg/inwazja/opowiesci/karty-postaci/1803-artur-zupan.html)|1|[140103](/rpg/inwazja/opowiesci/konspekty/140103-tak-bardzo-nie-artefakt.html)|
|[Andrea Wilgacz](/rpg/inwazja/opowiesci/karty-postaci/1709-andrea-wilgacz.html)|1|[140103](/rpg/inwazja/opowiesci/konspekty/140103-tak-bardzo-nie-artefakt.html)|

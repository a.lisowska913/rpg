---
layout: inwazja-karta-postaci
categories: profile
factions: "Rycerze Srebrnej Świecy, Srebrna Świeca"
type: "NPC"
title: "Konstanty Myszeczka"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **FIL: idealista**: 
    * _Aspekty_: tradycyjonalista
    * _Opis_: "Magowie są w gruncie rzeczy dobrzy - zasady sa po to, by pokusa konkurencji ich nie zachęcała do złego. Zasady są uzasadnione. Hierarchia jest uzasadniona. Porządek ponad spontaniczność; nieprzypadkowo tradycyjne podejście dominuje."
* **MRZ: supremacja - SŚ**:
    * _Aspekty_: bezwzględny
    * _Opis_: "Świeca jest wieczna. Bez niej powrócimy do Mrocznych Wieków. Inne gildie to tylko wentyl… nie oferują niczego, co WAŻNE. Byłem w piekle. Byłem na Esuriit. Nie próbuj mnie oceniać. Nie próbuj mnie zrozumieć. Zadanie - i Świeca - przede wszystkim."
* **KLT: honorowy**:
    * _Aspekty_: opiekuńczy
    * _Opis_: "Moje słowo jest święte, ma moralność nieskazitelna. Jestem Rycerzem Świecy. Nie musisz tego rozumieć, nie robię tego na pokaz. Rolą terminusa jest chronić i dbać o innych. Jestem dumny, mogąc osłaniać moich podopiecznych i ich wspierać."

### Umiejętności

* **Terminus**:
    * _Aspekty_: duelist, ochrona VIPa, defensywy, pierwsza pomoc
    * _Opis_: doskonały w pojedynkach, zwłaszcza honorowych pojedynkach Srebrnej Świecy. potrafi chronić magicznie i fizycznie konkretną osobą (niekoniecznie bojową), nawet ściągając ogień na siebie / innych. zdolny do utrzymaniu kogoś przy życiu na polu bitwy… jeśli naprawdę nie ma nikogo lepszego. skupiony na osłanianiu i działaniach defensywnych oraz prewencyjnych.
* **Przywódca**: 
    * _Aspekty_:  kapłan Świecy, brutalna motywacja, retoryka, niezłomne przekonania, charyzmatyczny wpływ na otoczenie, znajomość rytuałów i opowieści
    * _Opis_:  morale, inspirowanie, wydawanie rozkazów - często brutalnych i efektywnych. niezależnie od granic możliwości słuchającego, potrafi zmusić go do jeszcze większego wysiłku… nawet ponad to czego pragnie.
* **poeta**:
    * _Aspekty_: warrior-poet, oczyszczenie umysłu
    * _Opis_: potrafi tworzyć wiersze i kocha piękno.
    
### Silne i słabe strony:

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

## Magia

### Szkoły magiczne

* **Magia mentalna**:
    * _Aspekty_: astralika, oczyszczenie umysłu
    * _Opis_: potrafi wyczuć nastroje i zrozumieć co dzieje się w okolicy. Specjalizuje się w pracy z tłumami. potrafi "przywrócić osobę do rzeczywistości", wyrwać kogoś spod wpływu magii mentalnej itp. Odporny na ataki mentalne.
* **Magia elementalna**: 
    * _Aspekty_: 
    * _Opis_: 

### Zaklęcia statyczne

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 

## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Zarobki

* niezbyt bogaty czy wpływowy terminus; żyje z tego, co da gildia. Nie dba o sprawy materialne.

### Znam

* **Rycerze Srebrnej Świecy**:
    * _Aspekty_: 
    * _Opis_: loża magów-tradycjonalistów oddanych Świecy i dawnym zwyczajom
* **Inni terminusi Świecy**:
    * _Aspekty_: 
    * _Opis_: jako lekko nawiedzony warrior-priest-poet, rozpoznawany i doceniany przez kolegów…
* **Konserwatyści w Świecy**:
    * _Aspekty_: 
    * _Opis_: grupa magów w Świecy, która pragnie by było jak dawniej - przed Zaćmieniem i osłabieniem gildii.

### Mam

* **Sprzęt Rycerza Świecy**:
    * _Aspekty_: 
    * _Opis_: wyposażenie przynależne Rycerzowi Świecy; broń i rynsztunek z symboliką, wzbudzający podziw i świetnej jakości
* **Eliksiry wspomagające terminusa**:
    * _Aspekty_: 
    * _Opis_: od języcznika, przez neuronull, po mikstury lecznicze. Świeca wyposaża swoich.
* **Księgi i materiały Tradycji**:
    * _Aspekty_: 
    * _Opis_: kolekcja ksiąg i materiałów mówiących o tradycji, przeszłości itp.
* **Tomy poezji i wierszy**:
    * _Aspekty_: 
    * _Opis_: różnorodne wiersze Świecy; bogata i różnorodna kolekcja
* **Tradycyjne insygnia i szaty**:
    * _Aspekty_: 
    * _Opis_: Stroje, insygnia, pieczęcie… rzeczy zgodne z tradycją i zasadami
	
# Opis

krótki opis postaci, rzeczy, jakie warto pamiętać.

### Koncept

Wysokopoziomowy koncept postaci, 1-2 zdania.

### Motto
"Jestem rycerzem Srebrnej Swiecy - organizacji, dzięki której ten świat jeszcze nie jest w anarchii."

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|170208|wysłany do ochrony Silurii i Hektora po to, by Saith Catapult pokazała jego miejsce ;-).|[Koniec wojny z Karradraelem](/rpg/inwazja/opowiesci/konspekty/170208-koniec-wojny-z-karradraelem.html)|10/08/12|10/08/13|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|170125|pryzmatyczny paladyn Świecy; skłonny do walki i obrony Marianny i Zespołu nawet kosztem jego życia (ale nie straty ideałów).|[Przeprawa do Świecy Daemonica](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html)|10/08/10|10/08/11|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161130|terminus, bardzo kompetentny, lekko zrezygnowany (że tym razem Marianna go zabije). Ku ogromnemu zdziwieniu wszystkich - przeżył.|[Sprowadzenie Mare Vortex](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html)|10/08/02|10/08/04|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|3|[170208](/rpg/inwazja/opowiesci/konspekty/170208-koniec-wojny-z-karradraelem.html), [170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|3|[170208](/rpg/inwazja/opowiesci/konspekty/170208-koniec-wojny-z-karradraelem.html), [170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html)|
|[Mikado Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-mikado-diakon.html)|2|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html)|
|[Marianna Sowińska](/rpg/inwazja/opowiesci/karty-postaci/1709-marianna-sowinska.html)|2|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html)|
|[Ignat Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1802-ignat-zajcew.html)|2|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html), [161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html)|
|[Eis](/rpg/inwazja/opowiesci/karty-postaci/9999-eis.html)|2|[170208](/rpg/inwazja/opowiesci/konspekty/170208-koniec-wojny-z-karradraelem.html), [170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html)|
|[Łukija Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-lukija-zajcew.html)|1|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html)|
|[Zofia Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-zofia-weiner.html)|1|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html)|
|[Whisperwind](/rpg/inwazja/opowiesci/karty-postaci/9999-whisperwind.html)|1|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html)|
|[Warmaster](/rpg/inwazja/opowiesci/karty-postaci/9999-warmaster.html)|1|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html)|
|[Tomasz Przodownik](/rpg/inwazja/opowiesci/karty-postaci/9999-tomasz-przodownik.html)|1|[170208](/rpg/inwazja/opowiesci/konspekty/170208-koniec-wojny-z-karradraelem.html)|
|[Siriratharin](/rpg/inwazja/opowiesci/karty-postaci/1709-siriratharin.html)|1|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html)|
|[Saith Catapult](/rpg/inwazja/opowiesci/karty-postaci/9999-saith-catapult.html)|1|[170208](/rpg/inwazja/opowiesci/konspekty/170208-koniec-wojny-z-karradraelem.html)|
|[Paweł Maus](/rpg/inwazja/opowiesci/karty-postaci/9999-pawel-maus.html)|1|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html)|
|[Oliwier Sowiński](/rpg/inwazja/opowiesci/karty-postaci/9999-oliwier-sowinski.html)|1|[170208](/rpg/inwazja/opowiesci/konspekty/170208-koniec-wojny-z-karradraelem.html)|
|[Metody Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-metody-bankierz.html)|1|[170208](/rpg/inwazja/opowiesci/konspekty/170208-koniec-wojny-z-karradraelem.html)|
|[Marta Szysznicka](/rpg/inwazja/opowiesci/karty-postaci/9999-marta-szysznicka.html)|1|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html)|
|[Marian Agrest](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-agrest.html)|1|[170208](/rpg/inwazja/opowiesci/konspekty/170208-koniec-wojny-z-karradraelem.html)|
|[Lucjan Kopidół](/rpg/inwazja/opowiesci/karty-postaci/1803-lucjan-kopidol.html)|1|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html)|
|[Klaudia Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-klaudia-bankierz.html)|1|[170208](/rpg/inwazja/opowiesci/konspekty/170208-koniec-wojny-z-karradraelem.html)|
|[Kazimierz Sowiński](/rpg/inwazja/opowiesci/karty-postaci/9999-kazimierz-sowinski.html)|1|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html)|
|[Karina Paczulis](/rpg/inwazja/opowiesci/karty-postaci/9999-karina-paczulis.html)|1|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html)|
|[Jakub Pestka](/rpg/inwazja/opowiesci/karty-postaci/9999-jakub-pestka.html)|1|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html)|
|[Infernia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-infernia-diakon.html)|1|[170125](/rpg/inwazja/opowiesci/konspekty/170125-przeprawa-do-swiecy-daemonica.html)|
|[Fiodor Maius Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-fiodor-maius-zajcew.html)|1|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html)|
|[Edwin Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/9999-edwin-blakenbauer.html)|1|[170208](/rpg/inwazja/opowiesci/konspekty/170208-koniec-wojny-z-karradraelem.html)|
|[Dosifiej Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-dosifiej-zajcew.html)|1|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html)|
|[Bazyli Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-bazyli-weiner.html)|1|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html)|
|[Anna Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/1709-anna-myszeczka.html)|1|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html)|
|[Andżelika Leszczyńska](/rpg/inwazja/opowiesci/karty-postaci/1709-andzelika-leszczynska.html)|1|[161130](/rpg/inwazja/opowiesci/konspekty/161130-sprowadzenie-mare-vortex.html)|

---
layout: inwazja-karta-postaci
categories: profile
factions: "Lojaliści Srebrnej Świecy, Srebrna Świeca"
type: "NPC"
title: "Sabina Sowińska"
---
# {{ page.title }}

## Postać 

### Motywacje (do czego dąży)

* **BÓL: **:
    * _Aspekty_: 
    * _Opis_: 
* **FIL: Silni razem**: 
    * _Aspekty_: 
    * _Opis_: doprowadzić wszystkie strony do współpracy; sprawić, by wszyscy wygrali
* **MET: Z otwartą przyłbicą**:
    * _Aspekty_: bezpośrednie starcie
    * _Opis_: 
* **MRZ: **:
    * _Aspekty_: 
    * _Opis_: 
* **KLT: pomagać innym**:
    * _Aspekty_: 
    * _Opis_: sprawić, by nikomu nie działa się krzywda

### Umiejętności

* **Terminus**:
    * _Aspekty_:  walka z magami
    * _Opis_: 
* **Arystokrata**: 
    * _Aspekty_: arystokrata Świecy, mediacja, symbol Światła Świecy,
    * _Opis_: 

    
### Silne i słabe strony:

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

## Magia

### Szkoły magiczne

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

### Zaklęcia statyczne

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****: 
    * _Aspekty_: 
    * _Opis_: 

## Otoczenie

### Powiązane frakcje

{{ page.factions }}

### Zarobki

* bogata z urodzenia

### Znam

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

### Mam

* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 
* ****:
    * _Aspekty_: 
    * _Opis_: 

# Opis
członek liczącej się frakcji Sowińskich
A very potent, very skilled and very intelligent terminus. She is one of the elite; she is rumored to never lose. An inquisitor hunting for rogue magi, a paladin in the light, she’s exactly the role model terminus one would expect. Not associated with any major faction inside the Candle, Sabina tries to make everything better for everyone. She’s not a naïve fool, however. When she considers it is needed, she is willing to go medieval upon the opponents and her combat prowess is something she is well known for.

She was sent here from Warsaw to protect Wiktor and everyone else against Wiktor. Basically, she is to take charge over Nobility and make this movement work for the Candle.

Older than she seems.

### Koncept

Wysokopoziomowy koncept postaci, 1-2 zdania.

### Motto

"Tekst"

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|160412|która w wyniki skomplikowanej intrygi Wiktora (i Dagmary?) została przekształcona w jego posłuszną niewolnicę.|[Spleśniała dusza terminuski](/rpg/inwazja/opowiesci/konspekty/160412-splesniala-dusza-terminuski.html)|10/07/01|10/07/02|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160404|szlachetna i bohaterska terminuska, dla której przerażający jest stan degeneracji przez Wiktora Silurii.|[The power of cute pet](/rpg/inwazja/opowiesci/konspekty/160404-the-power-of-cute-pet.html)|10/06/25|10/06/26|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160403|osoba wprowadzona "przez kogoś wyżej" na miejsce Gerwazego. Ma "chronić" Wiktora i zapewnić, by ten się zachowywał.|[Wiktor kontra Kadem i Świeca](/rpg/inwazja/opowiesci/konspekty/160403-wiktor-kontra-kadem-i-swieca.html)|10/06/23|10/06/24|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|160424|Spustoszona i zniewolona; zabiła Ozydiusza będąc jego eskortą; w reakcji zginęła z odgryzioną przez Hektora głową. KIA.|[Uważaj, o czym marzysz](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|10/06/18|10/06/21|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Wioletta Bankierz](/rpg/inwazja/opowiesci/karty-postaci/9999-wioletta-bankierz.html)|4|[160412](/rpg/inwazja/opowiesci/konspekty/160412-splesniala-dusza-terminuski.html), [160404](/rpg/inwazja/opowiesci/konspekty/160404-the-power-of-cute-pet.html), [160403](/rpg/inwazja/opowiesci/konspekty/160403-wiktor-kontra-kadem-i-swieca.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Wiktor Sowiński](/rpg/inwazja/opowiesci/karty-postaci/1709-wiktor-sowinski.html)|4|[160412](/rpg/inwazja/opowiesci/konspekty/160412-splesniala-dusza-terminuski.html), [160404](/rpg/inwazja/opowiesci/konspekty/160404-the-power-of-cute-pet.html), [160403](/rpg/inwazja/opowiesci/konspekty/160403-wiktor-kontra-kadem-i-swieca.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|4|[160412](/rpg/inwazja/opowiesci/konspekty/160412-splesniala-dusza-terminuski.html), [160404](/rpg/inwazja/opowiesci/konspekty/160404-the-power-of-cute-pet.html), [160403](/rpg/inwazja/opowiesci/konspekty/160403-wiktor-kontra-kadem-i-swieca.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Dagmara Wyjątek](/rpg/inwazja/opowiesci/karty-postaci/1709-dagmara-wyjątek.html)|4|[160412](/rpg/inwazja/opowiesci/konspekty/160412-splesniala-dusza-terminuski.html), [160404](/rpg/inwazja/opowiesci/konspekty/160404-the-power-of-cute-pet.html), [160403](/rpg/inwazja/opowiesci/konspekty/160403-wiktor-kontra-kadem-i-swieca.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Elea Maus](/rpg/inwazja/opowiesci/karty-postaci/1802-elea-maus.html)|3|[160412](/rpg/inwazja/opowiesci/konspekty/160412-splesniala-dusza-terminuski.html), [160404](/rpg/inwazja/opowiesci/konspekty/160404-the-power-of-cute-pet.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Diana Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-diana-weiner.html)|3|[160412](/rpg/inwazja/opowiesci/konspekty/160412-splesniala-dusza-terminuski.html), [160404](/rpg/inwazja/opowiesci/konspekty/160404-the-power-of-cute-pet.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Netheria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-netheria-diakon.html)|2|[160404](/rpg/inwazja/opowiesci/konspekty/160404-the-power-of-cute-pet.html), [160403](/rpg/inwazja/opowiesci/konspekty/160403-wiktor-kontra-kadem-i-swieca.html)|
|[Laragnarhag](/rpg/inwazja/opowiesci/karty-postaci/1709-laragnarhag.html)|2|[160412](/rpg/inwazja/opowiesci/konspekty/160412-splesniala-dusza-terminuski.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Gerwazy Myszeczka](/rpg/inwazja/opowiesci/karty-postaci/9999-gerwazy-myszeczka.html)|2|[160404](/rpg/inwazja/opowiesci/konspekty/160404-the-power-of-cute-pet.html), [160403](/rpg/inwazja/opowiesci/konspekty/160403-wiktor-kontra-kadem-i-swieca.html)|
|[Aleksander Sowiński](/rpg/inwazja/opowiesci/karty-postaci/9999-aleksander-sowinski.html)|2|[160404](/rpg/inwazja/opowiesci/konspekty/160404-the-power-of-cute-pet.html), [160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Tadeusz Baran](/rpg/inwazja/opowiesci/karty-postaci/1709-tadeusz-baran.html)|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Ryszard Weiner](/rpg/inwazja/opowiesci/karty-postaci/9999-ryszard-weiner.html)|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Romeo Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-romeo-diakon.html)|1|[160404](/rpg/inwazja/opowiesci/konspekty/160404-the-power-of-cute-pet.html)|
|[Ozydiusz Bankierz](/rpg/inwazja/opowiesci/karty-postaci/1709-ozydiusz-bankierz.html)|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Mirabelka Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-mirabelka-diakon.html)|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Marian Łajdak](/rpg/inwazja/opowiesci/karty-postaci/1709-marian-lajdak.html)|1|[160403](/rpg/inwazja/opowiesci/konspekty/160403-wiktor-kontra-kadem-i-swieca.html)|
|[Marian Agrest](/rpg/inwazja/opowiesci/karty-postaci/9999-marian-agrest.html)|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Kajetan Weiner](/rpg/inwazja/opowiesci/karty-postaci/1709-kajetan-weiner.html)|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Julian Pszczelak](/rpg/inwazja/opowiesci/karty-postaci/1709-julian-pszczelak.html)|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Hektor Blakenbauer](/rpg/inwazja/opowiesci/karty-postaci/1709-hektor-blakenbauer.html)|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Fryderyk Chwost](/rpg/inwazja/opowiesci/karty-postaci/9999-fryderyk-chwost.html)|1|[160412](/rpg/inwazja/opowiesci/konspekty/160412-splesniala-dusza-terminuski.html)|
|[Fortitia Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-fortitia-diakon.html)|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Edward Diakon](/rpg/inwazja/opowiesci/karty-postaci/9999-edward-diakon.html)|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Aurel Czarko](/rpg/inwazja/opowiesci/karty-postaci/1709-aurel-czarko.html)|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Andrea Wilgacz](/rpg/inwazja/opowiesci/karty-postaci/1709-andrea-wilgacz.html)|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|
|[Amanda Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-amanda-diakon.html)|1|[160424](/rpg/inwazja/opowiesci/konspekty/160424-uwazaj-o-czym-marzysz.html)|

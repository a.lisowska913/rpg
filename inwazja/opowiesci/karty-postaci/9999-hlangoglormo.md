---
layout: inwazja-karta-postaci
categories: profile
title: "Hlangoglormo"
---
# {{ page.title }}

# Historia:
## Progresja

|Misja|Progresja|Kampania|
|-----|------|------|
|[Chrumpokalipsa](/rpg/inwazja/opowiesci/konspekty/180112-chrumpokalipsa.html)|uznawany za 'tame medical kraloth' przez Rodzinę Dukata|Wizja Dukata|
|[Nie podłożona świnia Łucji](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html)|ma potężny kult wraz z kultornicą (lub kilkoma?), które go chronią|Wizja Dukata|
|[Nie podłożona świnia Łucji](/rpg/inwazja/opowiesci/konspekty/180214-nie-podlozona-swinia-lucji.html)|skutecznie dał radę się schować przed oczami Laragnarhaga i Melodii Diakon na krótki czas|Wizja Dukata|


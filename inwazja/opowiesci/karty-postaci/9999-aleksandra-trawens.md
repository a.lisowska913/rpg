---
layout: inwazja-karta-postaci
categories: profile
title: "Aleksandra Trawens"
---
# {{ page.title }}

# Historia:

## Dokonania:

|Data|Dokonanie|Misja|Początek|Koniec|Kampania|
|-----|------|------|-----|------|------|
|161005|sariath; dowodzi tematami szmuglerskimi oraz politycznymi. Zidentyfikowała, że Wyzwalacz Pryzmaturgiczny jest uszkodzony na KADEMie i trzeba się go pozbyć.|[Szmuglowanie artefaktów](/rpg/inwazja/opowiesci/konspekty/161005-szmuglowanie-artefaktow.html)|10/05/05|10/05/07|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|141123|przełożona Instytutu Pryzmaturgii|[Druga kradzież wyzwalacza](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html)|10/04/17|10/04/19|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|
|161216|współgłównodowodząca Instytutu Eureka wraz z Martą Szysznicką|[Szept z Academii Daemonica](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|10/01/27|10/01/29|[Powrót Karradraela](/rpg/inwazja/opowiesci/konspekty/kampania-powrot-karradraela.html)|


## Relacje z postaciami:

|Z kim|Intensywność|Na misjach|
|-----|------|------|
|[Siluria Diakon](/rpg/inwazja/opowiesci/karty-postaci/1805-siluria-diakon.html)|2|[141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html), [161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|[Lucjan Kopidół](/rpg/inwazja/opowiesci/karty-postaci/1803-lucjan-kopidol.html)|2|[141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html), [161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|[Whisperwind](/rpg/inwazja/opowiesci/karty-postaci/9999-whisperwind.html)|1|[161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|[Warmaster](/rpg/inwazja/opowiesci/karty-postaci/9999-warmaster.html)|1|[161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|[Tamara Muszkiet](/rpg/inwazja/opowiesci/karty-postaci/1709-tamara-muszkiet.html)|1|[161005](/rpg/inwazja/opowiesci/konspekty/161005-szmuglowanie-artefaktow.html)|
|[Tadeusz Baran](/rpg/inwazja/opowiesci/karty-postaci/1709-tadeusz-baran.html)|1|[141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html)|
|[Stanislaw Przybysz](/rpg/inwazja/opowiesci/karty-postaci/9999-stanislaw-przybysz.html)|1|[141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html)|
|[Mikado Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-mikado-diakon.html)|1|[141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html)|
|[Marta Szysznicka](/rpg/inwazja/opowiesci/karty-postaci/9999-marta-szysznicka.html)|1|[161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|[Mariusz Trzosik](/rpg/inwazja/opowiesci/karty-postaci/9999-mariusz-trzosik.html)|1|[141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html)|
|[Marian Łajdak](/rpg/inwazja/opowiesci/karty-postaci/1709-marian-lajdak.html)|1|[161005](/rpg/inwazja/opowiesci/konspekty/161005-szmuglowanie-artefaktow.html)|
|[Magnus Spellslinger](/rpg/inwazja/opowiesci/karty-postaci/1709-magnus-spellslinger.html)|1|[161005](/rpg/inwazja/opowiesci/konspekty/161005-szmuglowanie-artefaktow.html)|
|[Kaspian Miauczek](/rpg/inwazja/opowiesci/karty-postaci/9999-kaspian-miauczek.html)|1|[161005](/rpg/inwazja/opowiesci/konspekty/161005-szmuglowanie-artefaktow.html)|
|[Karina Paczulis](/rpg/inwazja/opowiesci/karty-postaci/9999-karina-paczulis.html)|1|[161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|[Julian Pszczelak](/rpg/inwazja/opowiesci/karty-postaci/1709-julian-pszczelak.html)|1|[161005](/rpg/inwazja/opowiesci/konspekty/161005-szmuglowanie-artefaktow.html)|
|[Jewgenij Zajcew](/rpg/inwazja/opowiesci/karty-postaci/9999-jewgenij-zajcew.html)|1|[161005](/rpg/inwazja/opowiesci/konspekty/161005-szmuglowanie-artefaktow.html)|
|[Jan Kotlin](/rpg/inwazja/opowiesci/karty-postaci/9999-jan-kotlin.html)|1|[161005](/rpg/inwazja/opowiesci/konspekty/161005-szmuglowanie-artefaktow.html)|
|[Infernia Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-infernia-diakon.html)|1|[161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|[Infensa Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-infensa-diakon.html)|1|[161005](/rpg/inwazja/opowiesci/konspekty/161005-szmuglowanie-artefaktow.html)|
|[Ilona Amant](/rpg/inwazja/opowiesci/karty-postaci/9999-ilona-amant.html)|1|[141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html)|
|[Ignat Zajcew](/rpg/inwazja/opowiesci/karty-postaci/1802-ignat-zajcew.html)|1|[161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|[GS "Aegis" 0003](/rpg/inwazja/opowiesci/karty-postaci/9999-gs-aegis-0003.html)|1|[141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html)|
|[Eryk Płomień](/rpg/inwazja/opowiesci/karty-postaci/9999-eryk-plomien.html)|1|[141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html)|
|[Dracena Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-dracena-diakon.html)|1|[141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html)|
|[Artur Kotała](/rpg/inwazja/opowiesci/karty-postaci/9999-artur-kotala.html)|1|[161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|
|[Antygona Diakon](/rpg/inwazja/opowiesci/karty-postaci/1709-antygona-diakon.html)|1|[141123](/rpg/inwazja/opowiesci/konspekty/141123-druga-kradziez-wyzwalacza.html)|
|[Aniela Lipka](/rpg/inwazja/opowiesci/karty-postaci/1709-aniela-lipka.html)|1|[161005](/rpg/inwazja/opowiesci/konspekty/161005-szmuglowanie-artefaktow.html)|
|[Alisa Wiraż](/rpg/inwazja/opowiesci/karty-postaci/9999-alisa-wiraz.html)|1|[161216](/rpg/inwazja/opowiesci/konspekty/161216-szept-z-academii-daemonica.html)|

---
layout: default
title: The Cult Speaker / Mówca Kultu
---

# {{ page.title }}

## Description

Cult Speaker is close to the top of (local or global) organization for The Cause and attempts to spread The Cause.
Spreads influence of his organization for The Cause. Specializes in turning people and ensuring high morale of his agents.

Mówca Kultu jest osobą, która jest na samym szczycie jakiejś (lokalnej czy globalnej) piramidy Sprawy i próbuje szerzyć ową Sprawę. Kapłan, który szerzy wpływ swojej Sprawy i specjalizuje się w nawracaniu oraz trzymaniu wysokiego morale swoich agentów.


## Motivations / Behaviors

* to spread the cult influence
* to strengthen the cult
* to convert others
* to radicalize / fanaticise the cultists
* to share divine insight

## Moves

1. show hollowness of [someone's] existence
1. break [someone's] will
1. manipulate [someone] from within/outside the cult
1. declare a cult's dogma
1. create / strengthen the cult
1. offer hope / salvation / purpose to [someone]
1. offer home in the cult to [someone]
1. convert [someone]
1. use (beauty, order, [cult aspect]) as a weapon
1. use [converted] against [someone] as a leverage
1. make a spectacular show of cult's values
1. drive [someone] into frenzy
1. speak publicly
1. proclaim divine words; make a prophecy
1. sacrifice [someone] as example / to strengthen
1. fill [someone's mind] with foreign thought; plant an idea into [someone]
1. demand a sacrifice
1. quickly gather the [resources] from the cult
1. inspire an [action] directly / in subtle ways
1. punish the unbelievers
1. interpret a law
1. expose sin and injustice
---
layout: default
title: Szablony
---

# {{ page.title }}

1. [Addicted](addicted.html)
1. [Adventurer](adventurer.html)
1. [Ambitious Subrdinate](ambitious-subrdinate.html)
1. [Artisan](artisan.html)
1. [Artist](artist.html)
1. [Atoner](atoner.html)
1. [Brawler](brawler.html)
1. [Cleaner](cleaner.html)
1. [Collector](collector.html)
1. [Cult Speaker](cult-speaker.html)
1. [Dominator](dominator.html)
1. [Engineer](engineer.html)
1. [Estate Holder](estate-holder.html)
1. [Explorer](explorer.html)
1. [Good, Moral Person](good-person.html)
1. [Incompetent Expert](incompetent-expert.html)
1. [Intrepid Reporter](intrepid-reporter.html)
---
layout: default
title: The Engineer / Inżynier
---

# {{ page.title }}

## Description
Engineer focuses on solving problems and/or improving something. Efficiency is the main thing. Improves, upgrades builds from scratch. 
Operates mostly via his creations.
Not limited to only technology, can work with people and organizations as well.

Inżynier jest archetypem poświęconym rozwiązywaniu problemu i ulepszaniu czegoś. Wiecznie dążący do efektywności, usprawnień i działający raczej przez swe dzieła niż osobiście, Inżynier może dotyczyć obszaru ludzkiego (social engineering) jak i urządzeń.



http://tvtropes.org/pmwiki/pmwiki.php/Main/TheEngineer
http://tvtropes.org/pmwiki/pmwiki.php/Main/EvilutionaryBiologist

## Motivations / Behaviors

* Improve [something]
* Solve a problem of [something]

## Moves

1. repair / improve [something]; adapt it to new circumstances
1. gather the parts / resources to build [something]
1. fieldtest [something], make a dangerous experiment
1. tinker with [something], change it
1. introduce [something] unexpectedly (new gizmo)
1. notice a [problem] nobody else sees
1. defy / subvert an order (inefficient / wrong from their point of view)
1. technobabble an audience / make a statement on technology
1. proclaim a failure of existing technology / his experiment
1. pour more resources into an action
1. study an unknown device
1. completely misunderstand interpersonal stuff
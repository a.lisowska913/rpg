---
layout: default
title: The Intrepid Reporter / Nieustraszony Reporter
---

# {{ page.title }}

## Description
Intrepid Reporter gather information about selected topic (usually personally and up close) and tries to influence reality by publishin their articles and unearthing secrets inconvenient for those holding power.
Usually invested in a case more that it is safe for them. 

Reporter śledczy, osoba, która zbiera informacje na jakiś temat (zwykle dość blisko i osobiście) i próbuje wpłynąć na rzeczywistość przez publikację artykułów i ujawnienie sekretów, które zdaniem potężnych i możnych nie powinny wyjść na jaw. Zwykle angażuje się w sprawę dużo bardziej niż powinien.

http://tvtropes.org/pmwiki/pmwiki.php/Main/IntrepidReporter


## Motivations / Behaviors

* to uncover the truth
* to let the public know

## Moves

1. become a witness to [something] very inappropriate
1. publish an article which interests [side] earlier not interested in the situation
1. talk with witnesses who should remain silent
1. break the law to acquire [information]
1. become an object of attack by [someone]
1. narrowly avoid death / injury
1. rush into danger
1. acquire OR expose a secret very damaging to [someone]
1. stumble across a clue and pick up the investigation
1. hide the clue from [someone] because wants to be the first
1. use the web of connections to get information about [something]
1. question the results of the [investigation]
1. find a weakness in the [facade]
1. change the public opinion about the [event]
1. manipulate people to get access to [information] using the journalist's badge
1. cause unrest and riots
1. reveal the [truth] behind the [facade]
1. get hints that something is wrong from the press
1. create a dead man's hand
1. escape from a dangerous situation
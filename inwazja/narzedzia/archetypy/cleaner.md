---
layout: default
title: The Cleaner / Sprzątacz
---

# {{ page.title }}

## Description

X-com did their job. Alien forces are defeated, X-com is going back to the base... Enter the Cleaner. It is his responsibility to cover up all traces and to make sure that no one can find any traces - even if they look very hard.
He will bribe the witnesses, he will clean up all evidence. Simply, the Cleaner.

X-com zrobił swoje. Siły kosmitów zostały pokonane, X-com wraca do bazy... a na miejscu pojawia się Sprzątacz. Osoba, której celem jest zatarcie wszystkich śladów i upewnienie się, że nie wydarzyło się tu nic, co mogłoby kogoś zainteresować. Osoba przekupująca świadków i zacierająca dowody. Sprzątacz.

## Motivations / Behaviors

* to destroy evidence of [something]
* to hide [something]

## Moves

1. blackmail / bribe the witnesses of [something]
1. destroy the evidence of [something]
1. seek evidence to destroy
1. collude with local powers to hide [activity]
1. get orders from headquarters; adapt the plan to those orders
1. find [someone] to be a scapegoat in place of a real culprit
1. manipulate public opinion / press using [something]
1. forge false evidence; create plausible explanation
1. "convince" [somehow] the victims and witnesses of what they really saw
1. drug / medicate / harm victims and witnesses
1. call for reinforcements to keep the veil
1. hire local people to spread the lies / do the dirty work
1. create an event to distract from [something]
1. give plausible deniability to [someone]
1. plant false witnesses / documents about [something]
1. mediate with [someone] to solve a problem of [something]
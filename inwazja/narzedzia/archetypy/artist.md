---
layout: default
title: The Artist / Artysta
---

# {{ page.title }}

## Description

Artist is driven by admiring true art and - in some cases - creating it.
It could be art oof their own creation, or created by somene else - it does not matter.

Artysta jest archetypem skierowanym ku podziwianiu i wielbieniu dzieł sztuki. Niezależnie, czy to jego własne dzieło.

http://tvtropes.org/pmwiki/pmwiki.php/Main/MadArtist
http://tvtropes.org/pmwiki/pmwiki.php/Main/EvilDiva
http://tvtropes.org/pmwiki/pmwiki.php/Main/ThePrimaDonna

## Motivations / Behaviors

* to admire [something]

## Moves

1. declare [something] a work of art / put it in spotlight
1. work on a piece of art
1. destroy an unworthy piece of art
1. acquire a piece of art
1. acquire resources / prepare scene to create [something]
1. elicit a powerful emotion in [someone]
1. assault a rival / populace
1. demand recognition for status/deeds
1. focus on [something] with complete tunnel-vision
1. seek inspiration
1. study a pattern / observe [something]
1. walk in beauty / embrace the art
1. push boundaries
1. to create a piece of art
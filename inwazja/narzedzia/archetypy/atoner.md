---
layout: default
title: The Atoner / Pokutnik
---

# {{ page.title }}

## Description

Character, who in the past had done many evil things and wants to atone for those.

Example: Kenshin from Rurouni Kenshin

Osoba, która próbuje odpokutować za złe rzeczy, które uczyniła w przeszłości.

http://tvtropes.org/pmwiki/pmwiki.php/Main/TheAtoner

## Motivations / Behaviors

* to atone for [his deeds]

## Moves

1. seek alternate way to do [evil deed]; to find another way
1. sacrifice [something] for [action]
1. endure under duress
1. preach [new way]
1. try to help former comrades
1. confront with the past [deeds]
1. rebuild something destroyed (because of his actions / analogous, directly / indirectly)
1. have a flashback from the past
1. use the knowledge from the past life
1. engage in a strange compulsion / ritual
1. self-flog (metaphorically)
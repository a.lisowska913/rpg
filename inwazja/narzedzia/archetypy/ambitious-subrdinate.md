---
layout: default
title: The Ambitious Subrdinate / Ambitny podwładny
---

# {{ page.title }}

## Description

Ambitious Subordinate is an archetype describing a usually competent person who has ambition beyond her current position. An ambitious subordinate is willing to take quite many risks in order to get noticed and promoted by her superior, and has abilities and competencies to materialize results from those risks and avert most of potential problems. An ambitious subordinate likes to operate alone, behind enemy lines, having a lot of responsibility with possible high level of rewards. What motivates her the most is the power resulting from the position and the chain of command; this type of person seems incorruptible, while in reality she is corruptible, but not with normal resources. This archetype says nothing about her morals; only about her competences and craving for recognition.

Ambitny podwładny to osoba z ambicjami sięgającymi ponad jej aktualną pozycję. Chętnie podejmie różne ryzyka aby zostać dostrzeżonym i uzyskać awans. Ma wiedzę i umiejętności pozwalające ocenić i zminimalizować ryzyko oraz wyciągnąć jak najwięcej z sukcesu.
Zwykle działa sam, przyjmując dużo odpowiedzialności w zamian za możliwe duże nagrody.
Motywuje go władza wynikająca z pozycji. Może wydawać się odporny na korupcję, ale tak nie jest - można go skorumpować, ale nie zwykłymi środkami.
Ten archetyp nie mówi nic o moralności, raczej o kompetencji i pragnieniu uznania.


## Motivations / Behaviors

* to get noticed by her superiors, to get promoted
* to be recognized by her competencies and efficiency

## Moves

1. Hide her mistakes from [someone] using [resource]
1. cut off the communications to be able to operate alone to resolve [something]
1. volunteer to complete a very difficult task of [task]; get accepted or refused
1. attempts to solve the problem with a nonstandard plan which was not considered because of [reason]
1. pour her own [resource] to brute force the victory at the personal cost
1. raise the stakes and set constraints on her possible actions (in short, handicap herself) in order to boost potential recognition in front of [someone]
1. attempts to conscript a political ally in form of [someone] to be able to get noticed
1. help [someone] in their goal using her own [resource] for reciprocity in the future
1. secure a [resource] crucial for longer-term thinking in terms of her goal or her leader’s goal
1. voluntarily provide her superiors a [resource] absolutely necessary for their plan (from her pockets / previously commandeered)
1. maximize the gain for her superiors at cost of [someone]
1. do [something] which is very good in short-term, but because of [reason] is bad in long-term
1. make [someone] equal to her look bad to get on top of the rat race
1. execute an [order] in a way she (and her superior) looks good, not in the best way for the whole force
1. predict the needs of her superior and deliver [something] to him
1. compete against [someone] on the same side she is
1. overextend doing [something] and get in trouble because of [reason]
1. lose/fail [something] because she was too proud to call for reinforcements
1. prioritize her side and status over [something] very important or [someone] dear to her
1. manipulate the [situation] to maximize her personal benefits
1. take initiative in the [situation] and turn [something] around
1. engage in public relations by means of [something]
1. openly turn against [someone]
1. let her competent side override her ambitious side in [situation]
1. have a breakdown; lose faith in [someone]; lose hope she will be able to get what she wants
1. do [something] she would consider unacceptable in the past

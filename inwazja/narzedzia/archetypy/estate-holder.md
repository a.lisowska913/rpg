---
layout: default
title: The Estate Holder / Pan na Włościach
---

# {{ page.title }}

## Description
Estate Holder, as the name suggests, holds an estate. He owns an area, self-sufficient to some degree, but still missing something to be fully independent. Area might be small or quite big, but it requires contant maintenance and attention. Estate Holder usually has a strong bond to his estate and cares about it long-term

Pan na Włościach jest pomniejszym posiadaczem ziemskim. Posiada on pewien obszar, najpewniej w jakimś stopniu samowystarczalny a w innym czegoś mu brakuje. Te włości mogą być niewielkie lub całkiem pokaźne; to co istotne to fakt, że wymagają ciągłego doglądania zgodnie z powiedzeniem "pańskie oko konia tuczy". Pan na Włościach jest zwykle silnie związany ze swoją ziemią (posiadłością) i zależy mu na swych włościach długoterminowo.

## Motivations / Behaviors

* to improve the estate / life quality
* to protect the estate / position

## Moves

1. exploit the estate for burst riches / resources having form of [something]
1. introduce an improvement in form of [something] to the estate business of [something]
1. hire new [someone] to help in existing/new activity
1. protect the estate from [something/someone]
1. quarrel with the neighbors
1. run out of money / get a loan from [someone]
1. punish/reward [someone] for [something]
1. play politics with [someone]
1. pay a visit to [someone] / receive a guest being [someone]
1. acquire/lose [something] (land, business, person)
1. make a party because [reasons]

---
layout: inwazja-konspekt
title: "System emocji ludzkich"
---

# Koło emocji Plutchika

; http://www.adliterate.com/archives/Plutchik.emotion.theorie.POSTER.pdf

## Uczucia podstawowe

; STANDARD              OSŁABIONE               WZMOCNIONE

joy             10      serenity        4       extasy          2
trust           10      acceptance      4       admiration      2
fear            10      apprehension    4       terror          2
surprise        10      distraction     4       amazement       2
sadness         10      pensiveness     4       grief           2
disgust         10      boredom         4       loathing        2
anger           10      annoyance       4       rage            2
anticipation    10      interest        4       vigilance       2

## Struktura

### Ogólnie

; NAZWA UCZUCIA     SKŁADOWA_1      SKŁADOWA_2     CZĘSTOŚĆ POJAWIANIA

### Dyady o dystansie 1

love                joy             trust           10
submission          trust           fear            10
alarm               fear            surprise        10
disappointment      surprise        sadness         10
remorse             sadness         disgust         10
contempt            disgust         anger           10
aggression          anger           anticipation    10
optimism            anticipation    joy             10

### Dyady o dystansie 2

guilt               joy             fear            7
curiosity           trust           surprise        7
despair             fear            sadness         7
UNKNOWN             surprise        disgust         7
envy                sadness         anger           7
cynism              disgust         anticipation    7
pride               anger           joy             7
fatalism            anticipation    trust           7

### Dyady o dystansie 3

delight             joy             surprise        4
sentimentality      trust           sadness         4
shame               fear            disgust         4
outrage             surprise        anger           4
pessimism           sadness         anticipation    4
morbidness          disgust         joy             4
dominance           anger           trust           4
anxiety             anticipation    fear            4

**"W kosmosie to MY słyszymy Twój krzyk..." / "In space, WE hear you scream..."**


Jesteście załogą StarMed09 - statku należącego do stacji ratowiniczej S&R 197, położonej na skraju znanej przestrzeni kosmicznej.
To Wy jesteście ostatnią szansą górników, handlarzy czy podróżników.
Kogo przyjdzie Wam dzisiaj uratować?

## Postacie

Wasza załoga to profesjonaliści. Każdy ma swoją specjalizację, ale zna się odrobinę na wszystkim.
Poniżej znajdziesz listę ról, które zwykle przydają się w załodze statku ratowniczego.

Załoga:

- Mięsień
 - Może: Przebić się, przełamywać, zastraszać, wyciągać, walczyć
 - Ma: ciężki pancerz, ładunki wybuchowe, dekombinator
 
- Inżynier
 - Może: naprawić coś, wejść w interakcję z systemem, przejść przez korytarze serwisowe, użyć statku
 - Ma: sprzęt naprawczy, drony wspomagające, dostęp do planów statku
 
- Medyk
 - Może: badać próbki biologiczne, postawić na nogi, ustabilizować
 - Ma: sprzęt medyczny, sprzęt badawczy, oczyszczacze, stymulanty
 
- Zwiadowca
 - Może: prześliznąć się, wszędzie wleźć, zestrzelić z oddali, poruszać się w próżni
 - Ma: lekki pancerz (atv), drony zwiadowcze, snajperkę, ogromną prędkość
 
- Oficer
 - Może: wydawać rozkazy / przekonywać, kontrolować komunikację, przesłuchiwać, inspirować, wzbudzać / opanowywać chaos
 - Ma: uprawnienia i autorytet, konsoletę, dostęp do danych (również tajnych)
 
 
Nie jest to zamknięta lista, ale w zupełności wystarczy aby przeprowadzić udaną sesję.
Każdą z tych ról potraktujcie jako archetyp, który każdy gracz może dostosować do swoich preferencji.
Listy "może" i "ma" również nie są zamknięte. 
Na przykład: każdy członek załogi jest przeszkolony w podstawach pierwszej pomocy, ale tylko medyk jest w stanie przeprowadzić bardziej zaawansowane procedury.

Kilka dodatkowych sugestii ról, które mogą pojawić się na statkach ratowanych:
- Naukowiec (obszar dowolny)
- Specjalista od załadunku


### Pojęcia dziwne

- TAI - tactical (technological) AI. Taktyczna sztuczna inteligencja oparta o komputery,
- Elainka - bardzo proste TAI stosowane najczęściej w pancerzach. Nie 
- Semla - prosta TAI niskiej klasy. Bez osobowości. Doskonale sprawdza się na prostych, niewielkich jednostkach lub w wysokiej klasy pancerzach.
- Persofona - TAI klasy o jedną wyższej niż Semla. Zwykle używana na średniej wielkości jednostkach. Ta klasa AI ma już osobowość.
- Eshara - potężna klasa AI stosowana najczęściej w dużych jednostkach wojskowych lub bazach kosmicznych. W tym drugim wypadku często więcej niż jedna.
- Servar - skrót na pancerz wspomagany serwomotorowo. W zależności od klasy servary mogą oferować różne możliwości i stopnie ochrony oraz wsparcie AI lub nie
- 



## Dla MG

Ratownicy nigdy nie wiedzą, kogo tym razem przyjdzie im ratować.
Daje to możliwość zrobienia wielu różnych rodzajów sesji: od akcji, przez detektywistyczą po horror.
Poniżej znajdziesz kilka sugestii możliwych scenariuszy.

### Uniwersalne problemy

Jeśli poszukujesz potencjalnych problemów, jakie możesz postawić swoim graczom, poniżej znajdziesz kilka sugestii.

- Oficer dowodzący misją (postać gracza lub BN) może mieć sprzeczne priorytety: na przykład jego dowództwo nakazuje oszczędzać zasoby
- Ludzie, których wybrali się ratować mogą nie chcieć być ratowani.
- doszło do poważnej awarii i trzeba najpierw ustabilizować jednostkę a potem się przebić przez części zmiażdżonej struktury
- Wrak jest niestabilny strukturalnie, może się w każdej chwili rozpaść.
- Zasoby tlenu są niskie
- Niedostępne obszary z odciętymi ludźmi
- Zlokalizowanie zagubionych ludzi

#### Scenariusze

##### Scavengers

"Rudy szczur", statek zbieraczy kosmicznego złomu wpadł w kłopoty. Jego załoga w trakcie rozbiórki kosmicznego wraku popełniła błąd. Naruszyli zbiorniki  i doszło do wybuchu.
Część załogi zginęła od razu, część jest uwięziona, los kilku osób jest nieznany...

Potencjalne problemy, które możesz postawić przed załogą:
Przegrane konflikty mogą uderzać w ilość uratowanych ludzi, stopień ich poranienia...

##### Statek widmo

Dostajecie sygnał SOS. Jedyna informacja, jaką macie, to lokalizacja statku.

Kiedy wchodzicie na pokład, znajdujecie ślady krwi w całym statku. Jedyną pozostałością  po załodze są nagrania jednego z ludzi...

Ta sesja może stać się sesją detektywistyczną z elementami horroru. Dlaczego nie ma nikogo z załogi? Może coś ich zjadło, może zostali zarażeni plagą szaleństwa, może ostatnia żywa osoba upewniła się, że nikt nie wróci z tej wyprawy...
Jeśli gracze utkną, możesz zawsze podrzucić im kolejne nagranie

##### Nie chcemy TEGO w cywilizacji

Na pokład statku handlowego "Róg obfitości" dostało się _COŚ_ 
To stosunkowo nieduży (kilka godzin żeby go przejść na piechotę) rodzinny statek handlarzy, którzy czasem parają się też przewożeniem pasażerów.

Tutaj jest kilka możliwości jeśli chodzi o problem, w zależności od tego, co chcesz osiągnąć. Kilka pomysłów to:
- potwór
- anomalia
- szalone AI

Rzeczy, które mogą się dziać na statku i zagrożenia dla graczy lub coś, co może im pomóc:
- odgłos cichych kroków 
- ruch na granicy pola widzenia
- losowo włączające i wyłączające się urządzenia / zamykające się drzwi itp
- _TO_ dostaje się na statek graczy
- uszkodzone silniki
- członek załogi zamknięty i zdrowy w pomieszczeniu ze zniszczonymi kamerami i monitorami... ale bredzący pozornie od rzeczy




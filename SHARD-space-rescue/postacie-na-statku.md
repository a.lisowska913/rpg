# Postacie na statku kosmicznym

## 1. Rdzeń załogu na statku kosmicznym

### 1.1. Napoleon Myszogłów, "commander"

Fiszka:

* Napoleon Myszogłów, atarienin: "Co zrobisz to dostaniesz"
    * (ENCAO:  +0--- |Prawi wszystkim morały;;;;Bardzo lubi być w środku uwagi| VALS: Universalism, Conformity| DRIVE: Urażony szacune)
    * styl: 
    * "Jesteście w stanie osiągnąć coś lepszego niż macie, tylko musicie się wykazać i udowodnić, że można Wam ufać"
    * SPEC: ?

Opis: 

Młody (33 lata) oficer Orbitera dowodzący Fregatą Ratunkową. Jest dość kompetentny, ale jest też świadomy, że wszyscy weterani wojny noktiańskiej są po prostu o kilka klas lepsi od niego, zwłaszcza w temacie operacji kosmicznych i operacji ratunkowych. Nie jest fanem swoich podopiecznych, ale wierzy w to, że to co robią jest najlepszą opcją zarówno dla sektora astoriańskiego jak i dla noktian, choć niekoniecznie dla jego kariery.

Stosunek do frakcji:

* noktianie na stacji:
* 'wolni noktianie': 
* przestępcy: 
* przedsiębiorcy atarieńscy: 
* orbiter: 
* inni: 

### 1.2. Tristan Andrait, "ferocious free-loving devastator"

* Tristan Andrait, dekadianin: "Noctis shall not fall - czas się uzbroić i walczyć na nowym polu"
    * (ENCAO:  0+--0 |Agresywny i waleczny;; Bezpośredni;; Nigdy nie kończy walki| VALS: Face >> Achievement| DRIVE: Wolność + Chwała Noctis)
    * styl: twardy, nie okazuje słabości, gardzi mamlasami, cyniczny
    * "Poświęć więcej energii na pracę a mniej na gadanie, ok?"
    * SPEC: breacher, inżynieria materiałów, przebicie / penetracja

Niski i krępy dekadianin z niesamowitymi mięśniami i naturalnym talentem do destrukcji. Bulterier "in your face". Skrajnie lojalny swojej małej rodzinie i ogólnie nie dbający za bardzo o ludzi - ale dba o robienie swojej roboty maksymalnie dobrze. Naprawdę zależy mu na wolności i maksymalizacji szans noktian w systemie astoriańskim. Zwolennik Apalii Certamen i jej podejścia "war will forge the strongest and truest", ale przeniósł to na ratowanie ludzi.

Stosunek do frakcji:

* OGÓLNIE: robimy to co robimy więc zróbmy to DOBRZE
* noktianie na stacji: "czasem zachowują się głupio, ale muszę ich ochronić."
* 'wolni noktianie': "jak oni powinniśmy żyć. Nie jak pieski w zoo."
* przestępcy: "nie wchodźcie nam w drogę to nie dostaniecie wpierdolu."
* przedsiębiorcy: "wolni ludzie - my tacy być nie możemy."
* orbiter: ""
* inni: ""

### 1.3. Niferus Sentriak, "helpful professor"


### 1.4. Talia Irris, "whisper of a star"

* Talia Irris, savaranka: "słabość jednego z nas to słabość nas wszystkich"
    * (ENCAO:  0++++ |lubi dzieci;;Przesądna;;Wierzy w duchy;;Nie jest zazdrosna| VALS: Security >> Self-direction| DRIVE: Życie wśród przyjaciół)
        * core wound: nigdy już nie zobaczę swoich dzieci i swojej rodziny; nienawidzę być sama i samotna
        * core lie: ten statek to moja ostatnia szansa na namiastkę rodziny
    * styl: nostalgiczna, melancholijna, cicha savaranka lubiąca towarzystwo ludzi
    * "Nie bądź słabym ogniwem"; TRAKTOWANA JAK MASKOTKA
    * SPEC: space / void operations

Savaranka lubiąca towarzystwo innych; siedzi cicho i wszystko obserwuje swoimi bladymi oczami. Oszczędna w ruchach i nieskończenie cierpliwa, odzywa się gdy to potrzebne. Specjalistka od spaceru kosmicznego i operacji w zerowej grawitacji i próżni. Lubi obecność dzieci. Rzadko jest sama; zwykle znajduje się gdzieś w towarzystwie innych ludzi, nawet sobie nieznanych.

Wyjątkowo praworządna i chyba największa zwolenniczka integracji i współpracy wśród noktian.

Jak chodzi o jej dziwactwa - bardzo zwraca uwagę na pierwsze wrażenia statku i jest wyczulona na "duchy" i inne anomalie. Chyba najbardziej paranoiczna z załogi Statku Ratunkowego.

Stosunek do frakcji:

* OGÓLNIE: integracja bezwarunkowa, zachować swoją małą rodzinę
* noktianie na stacji: "moja jedyna rodzina, zrobię dla nich wszystko"
* 'wolni noktianie': "Po co to komukolwiek? Przegraliśmy, nie ma co marnować siły i ludzi. Dostosujmy się, to nasze dzieci mają szansę na lepsze jutro."
* przestępcy: "każde ciało ma komórki nowotworowe które trzeba odciąć. Moja mała rodzina to mechanizmy ochronne"
* przedsiębiorcy: 
* orbiter: "to nie oni zabrali mi wszystko a Noctis; Orbiterowcy okazali się łagodnymi panami. Nie kocham ich, ale nie nienawidzę."
* inni: "dostosuję się do tego co wymyśli Garin"

### 1.5. Garin Nix, "peacekeeper "




## 2. Dodatkowa osoba na statku kosmicznym

## 3. Stacja ratunkowa

## 4. 



Stacja ratunkowa


* Napoleon Myszogłów, atarienin: "Co zrobisz to dostaniesz"
    * (ENCAO:  +0--- |Prawi wszystkim morały;;;;Bardzo lubi być w środku uwagi| VALS: Universalism, Conformity| DRIVE: Urażony szacunek)
    * kadencja: smerf ważniak
    * "Jesteście w stanie osiągnąć coś lepszego niż macie, tylko musicie się wykazać i udowodnić, że można Wam ufać"
    * SPEC: ?
* Tristan Andrait, dekadianin: "jest tylko starcie i walka"
    * (ENCAO:  0+--+ |Nie toleruje spokoju i nudy; wściekły, na siłowni| VALS: Face >> Achievement| DRIVE: Wolność + Chwała Noctis)
    * kadencja: twardy, nie okazuje słabości, gardzi mamlasami, cyniczny
    * "Poświęć więcej energii na pracę a mniej na gadanie, ok?"
    * SPEC: walka, mięsień
* Niferus Sentriak, klarkartianin: "przeszłość nie ma znaczenia, ważne co możemy zrobić"
    * (ENCAO:  -0++0 |Wszystko rozwiążę teorią;;Przedsiębiorczy i pomysłowa| VALS: Achievement, Face| DRIVE: Legacy of helping)
    * kadencja: Chris Voss 'late night DJ voice'
    * "Wszystko rozwiążemy, to kwestia czasu"

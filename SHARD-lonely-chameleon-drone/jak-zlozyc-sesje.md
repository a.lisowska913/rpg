# Jak złożyć sesję

## 1. Jak o tym myśleć - feel SHARDa

Sesja działa jak X-Com 2.

Jesteś na neutralnym terenie i musisz działać SZYBKO i DYSKRETNIE by zapewnić Głód. A przy okazji rozwiązać lokalny problem.

Technologia wprowadza kontekst i sytuację, ale trudne dylematy mają postacie graczy. To gracze decydują jak zostawić świat lepszym i jak daleko zadziałają balansując dobro swojego Kajisa nad dobro społeczności. Musi istnieć opcja "oboje trochę wygrywają" na każdej sesji.

## 1. Checklista

1. Znajdź dominujący dylemat sesji 
2. Określ Głód
    1. Czego potrzebuje Chameleon Scout
3. Określ Lokalny Dramat
    1. Ludzki dramat (między ludźmi)
    2. Jaka frakcja jest tu dominująca i co jest ciekawego na tym terenie?
    3. Dominujące NPC
3. Określ o co grasz jako MG 
    1. Co jest dominującą katastrofą w kontekście Ludzkiego Dramatu?
    2. Co się stanie z Chameleon Scout jeśli zabraknie Głodu?
    3. Gdzie jest możliwość pozyskania "więcej"?
4. Złóż lokację i OCZYWISTY problem - co widać od razu
5. Określ scenę startową
6. Określ postacie i daj im Karty Celów Indywidualnych

## 1.1. Checklista - przykład 1


## Tagline

?

## 1. Jak o tym myśleć - feel SHARDa

Sesja działa jak X-Com 2.

Jesteś na neutralnym terenie i musisz działać SZYBKO i DYSKRETNIE by zapewnić Głód. A przy okazji rozwiązać lokalny problem.

Technologia wprowadza kontekst i sytuację, ale trudne dylematy mają postacie graczy. To gracze decydują jak zostawić świat lepszym i jak daleko zadziałają balansując dobro swojego Kajisa nad dobro społeczności. Musi istnieć opcja "oboje trochę wygrywają" na każdej sesji.

## 2. Opis - dla MG

Podczas wojny carrier do wojny elektronicznej Tenuis Ventus się rozbił na obszarze dzikiej sentisieci w Aurum. Minęło 20 lat i Tenuis Ventus się przebudziła, niezdolna do działania. Tenuis Ventus posiada jednak zbiór niewielkich autonomicznych jednostek kameleonicznych, które jak zarodniki rozsiały się po okolicy. Część z nich została przebudzona sygnałem kontrolnym, ale Tenuis Ventus nie potrafi nawiązać z nimi połączenia.

Gracze pilotują jedną ze spor Tenuis Ventus. Gracze są jednostkami niepożądanymi w Aurum; zrobili coś "złego" z perspektywy prawa Aurum. Niekoniecznie banici, ale nie są też osobami pożądanymi.

Kameleoniczna Jednostka Skanująca (nazwa "Kajis") w której się znajdują ma miejsce na 5 osób maksymalnie. Jest wielkości niemałej ciężarówki; na pokładzie ma dokładnie jednego ledwo sprawnego miragenta starej generacji. TAI d'Kajis maskuje się jako Crystal (młoda dziewczyna w nie pasującym do niej mundurze) albo Karol (starszy biznesmen). Ukrywa się w ciele miragenta; w rzeczywistości to TAI dowodzi samą sobą.

Cel Kajisa? 

* Odnaleźć i uratować noktian
* Pomóc w okolicy i zorientować się co się dzieje
* Podreperować siebie oraz Tenuis Ventus
* Znaleźć bezpieczną bazę

Kajis posiada dobre skanery i mechanizm kameleoniczny. Jest lekko uzbrojony (lanca elektryczna), lekko opancerzony, bardzo manewrowny i ma mechanizm VTOL.

## 3. Postacie

Wasze postacie są kompetentnymi ludźmi, robiącymi różne rzeczy w życiu i którzy w jakimś sposób podpadli możnym w Aurum. Znacie się trochę na wszystkim, acz macie swoje specjalizacje.



Załoga:

- Mięsień
 - Może: Przebić się, przełamywać, zastraszać, wyciągać, walczyć
 - Ma: ciężki pancerz, ładunki wybuchowe, dekombinator
 
- Inżynier
 - Może: naprawić coś, wejść w interakcję z systemem, przejść przez korytarze serwisowe, użyć statku
 - Ma: sprzęt naprawczy, drony wspomagające, dostęp do planów statku
 
- Medyk
 - Może: badać próbki biologiczne, postawić na nogi, ustabilizować
 - Ma: sprzęt medyczny, sprzęt badawczy, oczyszczacze, stymulanty
 
- Zwiadowca
 - Może: prześliznąć się, wszędzie wleźć, zestrzelić z oddali, poruszać się w próżni
 - Ma: lekki pancerz (atv), drony zwiadowcze, snajperkę, ogromną prędkość
 
- Oficer
 - Może: wydawać rozkazy / przekonywać, kontrolować komunikację, przesłuchiwać, inspirować, wzbudzać / opanowywać chaos
 - Ma: uprawnienia i autorytet, konsoletę, dostęp do danych (również tajnych)
 
 
Nie jest to zamknięta lista, ale w zupełności wystarczy aby przeprowadzić udaną sesję.
Każdą z tych ról potraktujcie jako archetyp, który każdy gracz może dostosować do swoich preferencji.
Listy "może" i "ma" również nie są zamknięte. 
Na przykład: każdy członek załogi jest przeszkolony w podstawach pierwszej pomocy, ale tylko medyk jest w stanie przeprowadzić bardziej zaawansowane procedury.

Kilka dodatkowych sugestii ról, które mogą pojawić się na statkach ratowanych:
- Naukowiec (obszar dowolny)
- Specjalista od załadunku

## 4. Kluczowi NPC

### 1. Karol Atenuatia ("ojciec"), odłamek TAI, biznesmen-preacher

* Karol Atenuatia, ???: "???" (szary prochowiec, siwiejący brunet, oddalone oczy, uszkodzona proteza nogi)
    * (ENCAO:  +-+00 |Płomienny;;Żarliwy| VALS: Family, Security >> Self-direction| DRIVE: Noctian Legacy)
        * core wound: Nie możemy wszystkich uratować. Przegraliśmy. Nie mam szans, mam tylko walkę.
        * core lie: Walka by być wolnym i uratować kogo się da jest ostatnim co mogę zrobić.
    * styl: cichy, z boku AŻ eksploduje cytatem z Księgi Wiecznej Wojny.
    * "Wolność wymaga walki i poświęcenia.", ""
    * SPEC: miragent, typ: infiltrator

### 2. Kajis - kameleoniczna jednostka skanująca

* akcje: sprawdź coś, przeskanuj coś, przedostań się gdzieś, ukryj się, przechwyć sygnał, uciekaj
* siły: doskonałe skanery, kameleon, VTOL, ATV
* słabości: lekki pancerz, energożerna, lekkie uzbrojenie (lanca elektryczna), mało miejsca
---
layout: mechanika
title: "Budowanie Profilu Postaci"
---

# {{ page.title }}

## 1. Ogólna struktura Profilu

Profil postaci _(karta postaci)_ składa się z następujących Obszarów.

* Koncept
* Misja
* Motywacje
* Umiejętności
* Wada
* Zasoby

## 2. Elementy profilu

### 2.1. Koncept

Najprostsze możliwe wyjaśnienie kim/czym jest dana postać.
"Co jego/jej znajomi opowiedzieliby o nim/niej przy piwie?"

### 2.2. Misja

Coś, co jest siłą napędową postaci. Marzenie, pragnienie... Coś, do czego będzie mogła dążyć.

### 2.3 Motywacje

Motywacje to coś, co sprawia, że dana postać rusza się z miejsca. 
Wartości, rzeczy za którymi lub przeciw którym **aktywnie** stanie do walki.

### 2.4 Umiejętności

Zbiór rzeczy, które postać potrafi zrobić _szczególnie_ dobrze do poziomu, w którym inni rozpoznają TĄ POSTAĆ jako specjalistkę od tego czegoś. Nie trzeba wpisywać tu np. "Kierowca ciężarówki", jeśli nasza postać potrafi prowadzić ciężarówkę - trzeba jednak to wpisać, jeśli nasza postać ma być bardzo dobrym kierowcą, do poziomu zawodowego / eksperckiego.

#### 2.4.1 Manewry

Manewry to **działania**, które dana postać wykonuje szczególnie często lub w których jest szczególnie dobra.

Manewry są podzielone na dwie kolumny: **co** i **czym**. W ramach pojedynczego rzędu są one produktem kartezjańskim - każdy element **co** oddzielony średnikiem łączy się z każdym elementem **czym**. Zwykle postać ma około 5 rzędów logicznie powiązanych ze sobą Manewrów.

Manewry oznaczają "jak ta postać najczęściej działa". To jak na filmie - pojawia się postać XXX i od razu wiemy, co się będzie mniej więcej działo, bo postać XXX zawsze robi YYY.

Przykładowe kombinacje pojedynczego rzędu Manewrów:

* Alojzy Przylaz: CO: _rozładowanie konfliktu_ CZYM: _zastraszanie; czytanie ludzi_
* Kornel Maus: CO: _powstrzymanie od złego; osłona nieletniego przed kłopotem;_ CZYM: _autorytet własny; zniechęcenie biurokracją_
* Krystalia Diakon: CO: _zniewolenie ofiary; wzbudzanie grozy; przesłuchiwanie_ CZYM: _psychotropy; intensywne uczucia;_

### 2.5 Wada

Wada to coś, co określa słaby punkt postaci, coś, co może być w ten czy inny sposób wykorzystane przeciwko niej. Na przykład, "dążenie do bycia podziwianym" będzie działać przeciwko postaci w sytuacji, gdy trzeba coś zrobić po cichu lub zachować tajemnicę o wielkim czynie.

### 2.6 Zasoby

Coś, do czego postać ma dostęp i co może wykorzystać na sesji.

## 3. Minimalna Postać potrzebna do gry

Minimalny Profil, by móc rozpocząć grę musi zawierać:

* Koncept (jedno do trzech słów) lub Misję
* jedną Motywację
* dwie Umiejętności
* dwa Manewry
* jedną Wadę


Graczu, po prostu weź coś co wydaje Ci się interesujące i zmieniaj w czasie rzeczywistym podczas budowania Opowieści ;-).

Mistrzu Gry, pozwól Graczowi przekształcać postać, by Gracz znalazł swój Voice&Tone i idealny koncept.

## 4 Mechaniczna ewolucja postaci

Jednym słowem: Progresja.

Zamiast punktów doświadczenia znanych z innych systemów, w tym systemie zarówno Gracz jak i MG otrzymują punkty Wpływu po skończeniu Opowieści. Wydają te punkty na zmianę rzeczywistości. To może być zarówno przesuwanie NPCów jak i dodanie nowych rzeczy do postaci.

Przykładowo, mieliśmy postać magazyniera na portalisku. W ramach wydawania Wpływu Gracz stwierdził, że chciałby zmienić funkcję postaci na detektywa. Więc po skończeniu Opowieści lokalny mafiozo dał owemu magazynierowi małą agencję detektywistyczną, co dopisaliśmy do Progresji postaci a Gracz sam dopisał Postaci nową Umiejętność.

Z perspektywy świata gry, ów magazynier być może kiedyś był detektywem, ale nie spotkaliśmy go w tej roli wcześniej.

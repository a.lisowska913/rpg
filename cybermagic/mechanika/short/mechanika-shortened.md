---
layout: mechanika
title: Mechanika
---
# Rozwiązywanie konfliktów

Mechanika opiera się o żetony, z których na potrzeby testu tworzy się pulę, a następnie losuje dwa.
Rozwiązanie konfliktu następuje w 4 krokach:

1. Ustal poziom trudności testu
1. Zbuduj pulę żetonów
1. Wylosuj dwa żetony
1. Określ wynik

## Żetony
Mechanika opiera się o trzy rodzaje żetonów.
Występują:

* żetony sukcesu (v)
* żetony skonfliktowanego sukcesu (=)
* żetony porażki (x)

Dodatkowo, dla specjalnego wyróżnienia, żetony mogą mieć różne tła aby oznaczyć specjalne okoliczności.
Pula żetonów, z których losuje gracz zależy od poziomu trudności konfliktu i umiejętności postaci.

## Poziom trudności testu
Poziom trudności testu określa początkową pulę żetonów.
Możliwe są następujące poziomy trudności:

|Poziom|v|-|x|
|-------------|:-:|:-:|:-:|
|Bezproblemowy|3|2|2|
|Łatwy|2|3|4|
|Normalny|0|2|5|
|Trudny|-2|3|6|
|Heroiczny|-3|4|9|
|PvP|1|3|2|

## Budowanie puli końcowej

Gracz dorzuca do puli żetony sukcesu zależnie od okoliczności i karty postaci.
Wady i okoliczności przeciwne mogą skutkować dodaniem do puli żetonu porażki.

W przypadku konfliktu gracz vs gracz:
Atakujący wyciąga żetony. Atakujący dorzuca żetony sukcesu. Broniący dorzuca żetony porażki.

## Określanie wyniku testu

Aby uzyskać wynik konfliktu, umieść pulę żetonów w woreczku i wylosuj dwa.
Wynik konfliktu określony jest następująco:

|żeton 1| żeton 2|wynik|
|:-:|:-:|:-:|
|v|v|sukces|
|v|=|sukces|
|v|x|skonfliktowany sukces|
|=|=|skonfliktowany sukces|
|x|=|porażka|
|x|x|porażka|

### Jak rozumieć wynik

Sukces oznacza, że postać osiągnęła cel zgodnie z intencją gracza.
Skonfliktowany sukces oznacza, że albo postać osiągnęła cel, ale coś poszło nie tak ("tak, ale"), albo postać nie osiągnęła celu zadeklarowanego przez gracza, ale coś innego się jej udało ("nie, ale")
Porażka nie oznacza automatycznie, że akcja się nie udała, ale może oznaczać, że udała się dużym kosztem.

Sukces: "Udało ci się włamać do magazynu. Nikt nie wie, że tam jesteś."
Skonfliktowany sukces: "Udało ci się włamać do magazynu, ale policja cię znajdzie."
Skonfliktowany sukces: "Nie udało ci się włamać do magazynu, ale nikt nie będzie w stanie się dowiedzieć, że próbowałeś." (To zgodne z mechaniką, ale zamyka wątek, nie ma co potem zrobić.)
Skonfliktowany sukces: "Nie udało ci się włamać do magazynu, ale znalazłeś inny trop, którym możesz podążyć."
Porażka: "Wszedłeś do magazynu, ale nagrały cię kamery i włączył się cichy alarm. Będziesz musiał kombinować, jak się wywinąć."

## Przerzuty

Gracz może zażądać przerzutu, jeżeli nie akceptuje wyniku losowania.
Wówczas usuwa jeden (wybrany) żeton z puli, zwraca drugi do woreczka i losuje ponownie.
Zawsze jednak wiąże się to z konsekwencjami w świecie gry (opowiadanej historii).
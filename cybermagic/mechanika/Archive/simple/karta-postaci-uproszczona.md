---
layout: mechanika
title: "Karta postaci - uproszczona"
---

# {{ page.title }}

## Jak taka karta wygląda?

![Postać Antoniny](Materials\170511_simplified_charsheet\antonina.png)

## Wyjaśnienie poszczególnych pól

### Cechy

Cechy oznaczają ogólne predyspozycje danej postaci do wykonywania konkretnej kategorii czynności. Za każdym razem, gdy gracz deklaruje jakąś akcję, postać _musi_ wykorzystać jedną z cech - tą, która najbardziej pasuje do sytuacji.

* Każda cecha może mieć jedną z trzech wartości: -1, 0 lub 1
* Występują trzy cechy:
    * **Fizyczne** - oznaczają sprawność fizyczną, zręczność, precyzję, wytrzymałość... ogólnie rozumiane "ciało"
    * **Społeczne** - oznaczają charyzmę, prezencję, komunikację, inteligencję emocjonalną... ogólnie, 'gadanie z ludźmi i ich rozumienie'
    * **Naukowo-Techniczne** - oznaczają znajomość technologii i zagadnień teoretycznych, edukację, specjalistyczny sprzęt... Ogólnie, 'praca z maszynami i badania naukowe'
* Cechy mają sumować się do '1'.

### Impulsy

Impulsy oznaczają ogólnie rozumiane motywatory postaci. Postać bez motywatorów "nie jest prawdziwa", niczego nie pragnie i nic nie motywuje jej do działania. 

* Każda postać powinna mieć 3-5 impulsów
* Każdy impuls ma wartość '2'
* Każdy impuls może być wykorzystany 'korzystnie' lub 'przeciwnie' - czasami do poszczególnej akcji impuls pasuje, czasami przeszkadza w danej czynności.
* Przykładowe impulsy: honorowy, pomocny ludziom, chce poderwać szefa, dąży do awansu za wszelką cenę, nienawidzi cierpienia zwierząt...

### Umiejętności

Umiejętności to grupy czynności, które dana postać potrafi realizować. Są to rzeczy wyuczone.

* Każda postać powinna mieć 5-7 umiejętności
* Każda umiejętność ma wartość '2'
* Umiejętności mogą być korzystne (jeśli pasują) lub neutralne (jeśli żaden nie pasuje).
* Przykładowe umiejętności: uliczny wojownik, kurier w wielkim mieście, kucharz wegetariańskiego jedzenia, wodzirej, 'każdemu sprzedam tą żabę'

---
layout: mechanika-warsztat-jak-grac
title: "Sales"
---
<br/>
<br/>
....................................

**Position**: {{ page.title }}
Biggest rewards: 
Being talked about in social media.
Recognition from CEO.


**Attributes:**
<table>
<tr>
	<th>Influence</th>
	<td>&nbsp;0&nbsp;</td>
	<td> ?</td>
</tr>
<tr>
	<th>Charisma</th>
	<td>&nbsp;+1&nbsp;</td>
	<td>"The monkey house." Outgoing. "we test it, then we go!" (whatever the test result is)</td>
</tr>
<tr>
	<th>Hierarchy</th>
	<td>&nbsp;-1&nbsp;</td>
	<td>?</td>
</tr>
</table>

**Motivations:**

<table>
<tr>
	<th>Hands on</th>
	<td>Decide stuff before checking what's possible</td>
	<td align="center" valign="center" width="20%" rowspan="3">
		<strong>-2 / 0 / +2</strong><br/>
		If beneficial, add 2 <br/>
		If oposite, subtract 2<br/>
		If both – add 0</td>
</tr>
<tr>
	<th>Get things done</th>
	<td>"It's gonna be magical!"</td>
</tr>
<tr>
	<th>?</th>
	<td>?</td>
</tr>
</table>

**Skills:**

<table>
<tr>
	<th>Design store layout</th>
	<td>Apply marketing material in stores, decide what goes where</td>
	<td align="center" valign="center" width="20%" rowspan="6">
		<strong>+2</strong><br/>
		If used, add 2<br/>
	</td>
</tr>
<tr>
	<th>Organize an event</th>
	<td>
		Fairs, jubilee...
		Fair does not cost a lot, 
		Jubilee counts towards budget.
	</td>
</tr>
<tr>
	<th>Negotiate with B2B customers</th>
	<td>?</td>
</tr>
<tr>
	<th>Determine daily goals</th>
	<td>?</td>
</tr>
<tr>
	<th>Communicate to stores</th>
	<td>?</td>
</tr>
<tr>
	<th>?</th>
	<td>?</td>
</tr>
</table>

**Restraints:**

<table>
<tr>
	<td>
		Limited ability to reach the stores.
		Only one channel - weekly letter.
		Regional managers' help available in special occasions.
	</td>
	<td align="center" valign="center" width="20%" rowspan="4">
		Can block execution if possible.
	</td>
</tr>
<tr>
	<td>
		Storage in stores is very limited and very diverse.
	</td>
</tr>
<tr>
	<td>
		New, unexpected products from purchasing.
	</td>
</tr>
<tr>
	<td>?</td>
</tr>
</table>

**Resources:**

<table>
<tr>
	<td>
		HR: <br/>
		Goals: <br/>
		<ul>
			<li>Protect company</li>
			<li>Help sales educate staff</li>
		</ul>
		Abilities: <br/>
		<ul>
			<li>Prepare educational materials</li>
		</ul>
	</td>
	<td align="center" valign="center" width="20%" rowspan="4">
		<strong>+2</strong><br/>
		If used, add 2<br/>
	</td>
</tr>
<tr>
	<td>
		Store database with information about stores.
	</td>
</tr>
<tr>
	<td>?</td>
</tr>
</table>	

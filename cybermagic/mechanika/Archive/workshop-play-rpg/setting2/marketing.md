---
layout: mechanika-warsztat-jak-grac
title: "Marketing"
---
<br/>
<br/>
....................................

**Position**: {{ page.title }}

Perceived to be not be driven by gain.
A bit disillusioned because too much has been forbidden or impossible.

**Attributes:**
<table>
<tr>
	<th>Influence</th>
	<td>&nbsp;-1&nbsp;</td>
	<td>Not perceived as absolutely necessary, hard to measure, costing and not bringing hard cash.</td>
</tr>
<tr>
	<th>Charisma</th>
	<td>&nbsp;0&nbsp;</td>
	<td>Not really passionate, a bit passive, very pragmatic. "we could do it but give us money"</td>
</tr>
<tr>
	<th>Hierarchy</th>
	<td>&nbsp;-1&nbsp;</td>
	<td>?</td>
</tr>
</table>

**Motivations:**

<table>
<tr>
	<th>Need to express.</th>
	<td>Be creative, channel your creativity. Why do something "normal" way if you can add colors to it?</td>
	<td align="center" valign="center" width="20%" rowspan="3">
		<strong>-2 / 0 / +2</strong><br/>
		If beneficial, add 2 <br/>
		If oposite, subtract 2<br/>
		If both – add 0</td>
</tr>
<tr>
	<th>Keep our processes</th>
	<td>Creativity in processes is chaos, don't change something that works!</td>
</tr>
<tr>
	<th>I do what I'm told.</th>
	<td>I have way too much to do, so only critical tasks get done.</td>
</tr>
<tr>
	<th>I'm doing you a favor</th>
	<td>Be grateful you have us in-house, all other companies use external services.</td>
</tr>
<tr>
	<th>Disillusioned pessimist</th>
	<td>I'd love to do it, I love your suggestions, but I simply don't have money for this...</td>
</tr>
<tr>
	<th>Let's do this!</th>
	<td>I so much want to do this, I do!!</td>
</tr>
</table>

**Skills:**

<table>
<tr>
	<th>Find influencers</th>
	<td>Blogger, vlogger, as long as it gets the message through.</td>
	<td align="center" valign="center" width="20%" rowspan="6">
		<strong>+2</strong><br/>
		If used, add 2<br/>
	</td>
</tr>
<tr>
	<th>Flyers</th>
	<td>No additional cost, can be steered towards what you want.</td>
</tr>
<tr>
	<th>Prepare magazine</th>
	<td>No additional cost, can be steered like flyers</td>
</tr>
<tr>
	<th>Postal campaign</th>
	<td>Send clients mail / cards inviting them to come. Not very effective.</td>
</tr>
<tr>
	<th>Email campaign.</th>
	<td>?</td>
</tr>
<tr>
	<th>?</th>
	<td>?</td>
</tr>
</table>

**Restraints:**

<table>
<tr>
	<td>Cannot offer discounts other than 2% back in vouchers (normal thing).</td>
	<td align="center" valign="center" width="20%" rowspan="2">
		Can block execution if possible.
	</td>
</tr>
<tr>
	<td>Personal data protection.</td>
</tr>
</table>

**Resources:**

<table>
<tr>
	<td>
		Commercial money from Norway - does not count towards buget. Can be used for TV commercials.
	</td>
	<td align="center" valign="center" width="20%" rowspan="4">
		<strong>+2</strong><br/>
		If used, add 2<br/>
	</td>
</tr>
<tr>
	<td>
		Database of active and inactive custmers.
	</td>
</tr>
<tr>
	<td>
		Different channels: flyers, blog, customer magazine, influencers.
	</td>
</tr>
<tr>
	<td>
		Famous horse trainers.
	</td>
</tr>
</table>

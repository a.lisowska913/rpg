---
layout: mechanika-warsztat-jak-grac
title: "Karta konfliktów"
---
<table>
<tr>
	<th align="center" valign="center">Conflict</th>
	<th width="30%" align="center" valign="center">Description</th>
	<th align="center" valign="center">Player strength</th>
	<th align="center" valign="center">Opponent strength</th>
	<th align="center" valign="center">Roll result</th>
</tr>
<tr>
	<td align="center" valign="center">1.</td>
	<td align="center" valign="center"></td>
	<td align="center" valign="center">
		<table>
			<tr>
				<td align="center" valign="center">Attribute</td>
				<td align="center" valign="center">Drive</td>
				<td align="center" valign="center">Skill</td>
			</tr>
			<tr>
				<td align="center" valign="center">-1 / 0 / +1</td>
				<td align="center" valign="center">-2 / 0 / +2</td>
				<td align="center" valign="center">0 / +2</td>
			</tr>
			<tr>
				<td align="center" valign="center">Resource</td>
				<td align="center" valign="center">Cooperation</td>
				<td align="center" valign="center">Circumstances</td>
			</tr>
			<tr>
				<td align="center" valign="center">0 / +2</td>
				<td align="center" valign="center">0 / +1</td>
				<td align="center" valign="center">0 / +1 / +2</td>
			</tr>
			<tr>
				<td align="center" valign="center">Idea</td>
				<td  align="center" valign="center" rowspan="2">Total</td>
			</tr>
			<tr>
				<td align="center" valign="center">0 / +1 / +2</td>
			</tr>
		</table>
	</td>
	<td align="center" valign="center">
		<table>
			<tr>
				<td align="center" valign="center">Difficulty</td>
			</tr>
			<tr>
				<td align="center" valign="center">3 / 4 / 5 / 6 / 7</td>
			</tr>
			<tr>
				<td align="center" valign="center">Preparation</td>
			</tr>
			<tr>
				<td align="center" valign="center">0 / +1 / +2</td>
			</tr>
		</table>	
	</td>
	<td align="center" valign="center"></td>
</tr>
</table>

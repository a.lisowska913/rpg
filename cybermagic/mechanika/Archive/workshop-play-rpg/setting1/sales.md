---
layout: mechanika-warsztat-jak-grac
title: "Specjalista sprzedaży"
---
<br/>
<br/>
....................................

**Stanowisko**: {{ page.title }}
Młody wilk. Pragnie wykazać się przed szefem aby dostać awans.
Żyje na pokaz, jeździ sportowym samochodem i nosi markowe ciuchy. Singiel. 
Niech Cię widzą, niech o Tobie słyszą, niech wiedzą, że jesteś KIMŚ!

**Cechy:**
<table>
<tr>
	<th>Fizyczne</th>
	<td>&nbsp;0&nbsp;</td>
	<td>Uczęszcza na crossfit i biega, ale nie pod górkę</td>
</tr>
<tr>
	<th>Społeczne</th>
	<td>&nbsp;1&nbsp;</td>
	<td>Kto jak kto, ale on musi!</td>
</tr>
<tr>
	<th>Naukowo-techniczne</th>
	<td>&nbsp;-1&nbsp;</td>
	<td>Po co to komu? Nie muszę znać szczegółów, żeby móc to sprzedać, cokolwiek to jest!</td>
</tr>
</table>

**Impulsy:**

<table>
<tr>
	<th>Moje, wszystko moje!</th>
	<td>Własny jacht to jest to! I ja też chcę Rolexa! Niezaspokojony głód posiadania i władzy ;-)</td>
	<td align="center" valign="center" width="20%" rowspan="4">
		<strong>-2 / 0 / +2</strong><br/>
		Jeśli są zgodne, dodaj 2. <br/>
		Jeśli są przeciwne, odejmij 2<br/>
		Jeśli takie i takie – dodaj 0</td>
</tr>
<tr>
	<th>To nie musi być prawda, by to sprzedać</th>
	<td>Chłopaki z IT jakoś to ogarną, grunt, że klient kupi.</td>
</tr>
<tr>
	<th>Ale ona ma piękne oczy...</th>
	<td>Romantyk pragnący prawdziwego uczucia.</td>
</tr>
<tr>
	<th>Zróbmy coś!</th>
	<td>Proaktywność, proaktywność, proaktywność!</td>
</tr>
</table>



**Umiejętności:**

<table>
<tr>
	<th>Dobra nawijka</th>
	<td>Ma gadane. Od małego. Już w przedszkolu potrafił wycyganić od kolegów lizaki.</td>
	<td align="center" valign="center" width="20%" rowspan="4">
		<strong>+2</strong><br/>
		Jeśli używasz, dodaj 2<br/>
	</td>
</tr>
<tr>
	<th>Papier wszystko zniesie</th>
	<td>Przecinek w lewo, przecinek w prawo… w moim raporcie na pewno wyjdziemy na swoje!</td>
</tr>
<tr>
	<th>Tygrys szos i autostrad</th>
	<td>Żadne korki mu niestraszne!</td>
</tr>
<tr>
	<th>Zrobisz to dla mnie?</th>
	<td>Osiągnął perfekcję w tym, by zrzucać z siebie wszystko, czego naprawdę nie chce robić.</td>
</tr>
</table>

**Zasoby:**

<table>
<tr>
	<td>Wizytówki</td>
	<td align="center" valign="center" width="20%" rowspan="5">
		<strong>+2</strong><br/>
		Jeśli używasz, dodaj 2<br/>
	</td>
</tr>
<tr>
	<td>Kije golfowe w bagażniku</td>
</tr>
<tr>
	<td>3 Telefony Służbowe</td>
</tr>
<tr>
	<td>krem do rąk</td>
</tr>
<tr>
	<td>Komputer do prezentacji (małej mocy, lekki laptop) Pasjans lekko przycina… ale z power pointem daje radę</td>
</tr>
</table>
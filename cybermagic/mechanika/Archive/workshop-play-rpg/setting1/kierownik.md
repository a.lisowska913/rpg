---
layout: mechanika-warsztat-jak-grac
title: "Szef szefów... no, prawie"
---
**Jędrzej Jakubczyk**
....................................

**Stanowisko**: {{ page.title }}
Szef oddziału korporacji, w którym pracują nasi bohaterowie.
Jego jedynym celem jest WIĘCEJ: więcej zarobić, zdobyć więcej klientów, uzyskać lepsze wyniki.
A wszystko to po to, żeby awansować. W ogóle, to ta korporacja działałaby milion razy lepiej, gdyby była przez niego zarządzana, ale i tak nigdy nie zostanie dopuszczony do koryta, więc skazany jest na swoich pracowników.

Wobec przełożonych: płaszczenie się i mnóstwo wazeliny. Chcą stu klientów na jutro? Oczywiście, tak jest!
Wobec podwładnych: despotyzm i mikromanagement.

**Cechy:**
<table>
<tr>
	<th>Fizyczne</th>
	<td>&nbsp;-1&nbsp;</td>
	<td>Chudy jak szczapa albo tak korpulentny, że łatwo pomylić wzrost z szerokością. Tak czy owak, na olimpiadzie nie ma szans.</td>
</tr>
<tr>
	<th>Społeczne</th>
	<td>&nbsp;1&nbsp;</td>
	<td>Jakoś musiał się wybić... wie, co kiedy i komu powiedzieć, by osiągnąć swoje cele.</td>
</tr>
<tr>
	<th>Naukowo-techniczne</th>
	<td>&nbsp;0&nbsp;</td>
	<td>Jeśli nawet kiedyś wiedział, jak działa jego biznes to bardzo starał się o tym zapomnieć... Skutecznie.</td>
</tr>
</table>

**Impulsy:**

<table>
<tr>
	<th>Zdobyć WIĘCEJ</th>
	<td>Szef nie cofnie się przed niczym, aby lepiej wypaść w oczach przełożonych, lub aby uzyskać od swoich podwładnych więcej wysiłku</td>
	<td align="center" valign="center" width="20%" rowspan="1">
		<strong>-2 / 0 / +2</strong><br/>
		Jeśli są zgodne, dodaj 2. <br/>
		Jeśli są przeciwne, odejmij 2<br/>
		Jeśli takie i takie – dodaj 0</td>
</tr>
</table>



**Umiejętności:**

<table>
<tr>
	<th>Wybiórczy słuch</th>
	<td>Jeśli czegoś nie chce usłyszeć to tego nie usłyszy. Ani nie zobaczy. "Jeśli tego nie widzę, to tego nie ma"</td>
	<td align="center" valign="center" width="20%" rowspan="4">
		<strong>+2</strong><br/>
		Jeśli używasz, dodaj 2<br/>
	</td>
</tr>
<tr>
	<th>Zastraszanie</th>
	<td>To najlepsza metoda, aby pracownicy pracowali więcej!</td>
</tr>
<tr>
	<th>Odporność na argumenty</th>
	<td>Co z tego, że doba ma 24 godziny? Pracuj mądrzej i więcej, to będziesz miał efekty!</td>
</tr>
<tr>
	<th>Korpo-gadka</th>
	<td>W ostateczności zagaduje korporacyjną nowo-mową. Gdy to się dzieje, wszyscy biorą nogi za pas.</td>
</tr>
</table>

**Zasoby:**

<table>
<tr>
	<td> </td>
	<td align="center" valign="center" width="20%" rowspan="1">
		<strong>+2</strong><br/>
		Jeśli używasz, dodaj 2<br/>
	</td>
</tr>
</table>
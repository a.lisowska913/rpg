---
layout: mechanika-warsztat-jak-grac
title: "Prawnik"
---
<br/>
<br/>
....................................

**Stanowisko**: {{ page.title }}
Każda korporacja wystawiona jest na atak sępów.
Prawnik to pierwsza i ostatnia linia obrony przed trollami patentowymi.
Zawalony umowami po czubek głowy, spędza swój dzień na odbijaniu dziesiątek pytań, bo jest sam w firmie z takimi kompetencjami.
Kiedy nie siedzi przy biurku to nerwowo kręci się po korytarzu z telefonem przy uchu, prowadząc długie rozmowy z niewidzialnym konsultantem.

**Cechy:**
<table>
<tr>
	<th>Fizyczne</th>
	<td>&nbsp;-1&nbsp;</td>
	<td>Może nie ma za dużo okazji do ćwiczeń, ale kodeks prawa cywilnego swoje waży...</td>
</tr>
<tr>
	<th>Społeczne</th>
	<td>&nbsp;0&nbsp;</td>
	<td>Prawnicy dogadują się głównie z innymi prawnikami...</td>
</tr>
<tr>
	<th>Naukowo-techniczne</th>
	<td>&nbsp;+1&nbsp;</td>
	<td>Nie technicznie a naukowo. Tyle czasu studiował, i ma taaaki łeb.</td>
</tr>
</table>

**Impulsy:**

<table>
<tr>
	<th>Odkrywca tajemnic</th>
	<td>Jeżeli coś jest niejawne… warto się tego dowiedzieć. </td>
	<td align="center" valign="center" width="20%" rowspan="4">
		<strong>-2 / 0 / +2</strong><br/>
		Jeśli są zgodne, dodaj 2. <br/>
		Jeśli są przeciwne, odejmij 2<br/>
		Jeśli takie i takie – dodaj 0</td>
</tr>
<tr>
	<th>„Grisham byłby ze mnie dumny.”</th>
	<td>Dyskretny wielbiciel obrońców praw pracownika - i społecznik</td>
</tr>
<tr>
	<th>Człowiek-obywatel</th>
	<td>Gdyby nie moje notki o sprzątaniu po swoim psie, nasze miasto byłoby już całe… wiecie sami.</td>
</tr>
<tr>
	<th>Ostrożny</th>
	<td>Powoli, ostrożnie, analizy zagadnień prawniczych nie da się przyspieszyć... Masz pojęcie ile ustaw musimy przejrzeć?!</td>
</tr>
</table>



**Umiejętności:**

<table>
<tr>
	<th>Przepisy w małym palcu</th>
	<td>To ja zastawiam pułapki na innych, a nie na odwrót. Kruczki prawne trzymam w klatkach!</td>
	<td align="center" valign="center" width="20%" rowspan="4">
		<strong>+2</strong><br/>
		Jeśli używasz, dodaj 2<br/>
	</td>
</tr>
<tr>
	<th>Znajdź haczyk</th>
	<td>Retoryka i erystyka… może nie jestem adwokatem, ale nie dam się głupio złapać!</td>
</tr>
<tr>
	<th>Granie na czas</th>
	<td>Momencik, jeszcze to...</td>
</tr>
<tr>
	<th>Inkwizytor</th>
	<td>POWIEDZ PRAWDĘ! PRZYZNAJ SIĘ! NIEWAŻNE, CZY TO TY!</td>
</tr>
</table>

**Zasoby:**

<table>
<tr>
	<td>Biuro z jednym dodatkowym miejscem, dla dokładnie jednego petenta.</td>
	<td align="center" valign="center" width="20%" rowspan="5">
		<strong>+2</strong><br/>
		Jeśli używasz, dodaj 2<br/>
	</td>
</tr>
<tr>
	<td>Księgi, Książki i książeczki</td>
</tr>
<tr>
	<td>Wnioski i formularze na każdą okazję</td>
</tr>
<tr>
	<td>Luksusowy długopis z diamentem marki Mont Blanc</td>
</tr>
<tr>
	<td>Figurka Temidy na stół.</td>
</tr>
</table>
---
layout: mechanika-warsztat-jak-grac
title: "Junior Human Resources Specialist"
---
<br/>
<br/>
....................................

**Role**: {{ page.title }}
Młody wilk. Pragnie wykazać się przed szefem aby dostać awans.
Żyje na pokaz, jeździ sportowym samochodem i nosi markowe ciuchy. Singiel. 
Niech Cię widzą, niech o Tobie słyszą, niech wiedzą, że jesteś KIMŚ!

**Attributes:**
<table>
<tr>
	<th>Physical</th>
	<td>&nbsp;0&nbsp;</td>
	<td>Into crossfit and running (but not uphill)</td>
</tr>
<tr>
	<th>Social</th>
	<td>&nbsp;+1&nbsp;</td>
	<td>Who if not me?</td>
</tr>
<tr>
	<th>Science / technological</th>
	<td>&nbsp;-1&nbsp;</td>
	<td>Who cares? I can sell it, whatever it is!</td>
</tr>
</table>

**Motivators:**

<table>
<tr>
	<th>Mine, all mine!</th>
	<td>My own yacht! And Rolex! I want it all!</td>
	<td align="center" valign="center" width="20%" rowspan="4">
		<strong>-2 / 0 / +2</strong><br/>
		If beneficial, add 2 <br/>
		If oposite, subtract 2<br/>
		If both – add 0</td>
</tr>
<tr>
	<th>It does not have to be truth for me to sell it.</th>
	<td>IT folks will manage - and I meet my quota!</td>
</tr>
<tr>
	<th>Those beautiful eyes...</th>
	<td>Romantic, dreaming of true love.</td>
</tr>
<tr>
	<th>Let's **DO** something!</th>
	<td>My motto is "Proactive!"</td>
</tr>
</table>

**Skills:**

<table>
<tr>
	<th>I know a guy...</th>
	<td>I had so many interviews I can always find someone...</td>
	<td align="center" valign="center" width="20%" rowspan="6">
		<strong>+2</strong><br/>
		If used, add 2<br/>
	</td>
</tr>
<tr>
	<th>Parkour</th>
	<td>No candidate can run from me!</td>
</tr>
<tr>
	<th>Organizer</th>
	<td>I even organized boss' son birthday!</td>
</tr>
<tr>
	<th>Procedures</th>
	<td>To do that you need to fill in the form 46B/9...</td>
</tr>
<tr>
	<th>Smalltalk</th>
	<td>Thrives in informal situations</td>
</tr>
<tr>
	<th>Temper tantrum</th>
	<td>NO WAY! This terrible defiance would make even Spock uneasy...</td>
</tr>
</table>

**Resources:**

<table>
<tr>
	<td>Pile of unread CVs</td>
	<td align="center" valign="center" width="20%" rowspan="4">
		<strong>+2</strong><br/>
		Jeśli używasz, dodaj 2<br/>
	</td>
</tr>
<tr>
	<td>Connections on LinkedIn and other portals</td>
</tr>
<tr>
	<td>Friends in all departments</td>
</tr>
<tr>
	<td>Proper running shoes</td>
</tr>
</table>

---
layout: mechanika-warsztat-jak-grac
title: "IT"
---
<br/>
<br/>
....................................

**Role**: {{ page.title }}
...And tech support of everything and everyone.
He hates nonsense, likes pizza and coffee.
At the computer he gets into flow after 3 minutes. 
He can only be shaken out of it by teams call "So? COFFEE?"
Then as a group they move to the kitchen, where he fills his beloved Homer Simpson mug with dark elixir.
Has intense discussions with use of terms like "databases", "refactoring", "repository".
Hates meetings, because sees them as counter-productive. Prefers to talk face to face and in addition to productive discussion, gossip a little.

**Attributes:**
<table>
<tr>
	<th>Physical</th>
	<td>&nbsp;-1&nbsp;</td>
	<td>Computer at home, computer at work... I run to make it to a bus only!</td>
</tr>
<tr>
	<th>Social</th>
	<td>&nbsp;0&nbsp;</td>
	<td>Despite of how it might seem, we are not bunch of anti-social creeps. It's just that "normal" people don't understand us.</td>
</tr>
<tr>
	<th>Science / technological</th>
	<td>&nbsp;+1&nbsp;</td>
	<td>Oooh, yea, babe! Let me play with it!</td>
</tr>
</table>

**Motivators:**

<table>
<tr>
	<th>Have you heard that...</th>
	<td>Likes to be up to date, know everything about everythng. Greatest gossip in the department.</td>
	<td align="center" valign="center" width="20%" rowspan="3">
		<strong>-2 / 0 / +2</strong><br/>
		If beneficial, add 2 <br/>
		If oposite, subtract 2<br/>
		If both – add 0</td>
</tr>
<tr>
	<th>Oooh! A new toy!</th>
	<td>Things that you did once are boring. You can always do it slightly different. Maybe... use   new tool?</td>
</tr>
<tr>
	<th>Team is like a family to me.</th>
	<td>Not very fond of people in general, will do all in his capability to help the team reach the goal.</td>
</tr>
</table>



**Skills:**

<table>
<tr>
	<th>Techno-babble</th>
	<td>This terrible weapon of mass destruction uses technical skills intead of social ones. You can try to understand it (technical test) or stop to listen to preserve your sanity (social test)</td>
	<td align="center" valign="center" width="20%" rowspan="4">
		<strong>+2</strong><br/>
		If used, add 2<br/>
	</td>
</tr>
<tr>
	<th>Fix it!</th>
	<td>IT equipment works when qualified personnel is around. (At least for a brief moment)</td>
</tr>
<tr>
	<th>All memes are mine!</th>
	<td>If it's in the Internets, I will find it! If it's hidden, I will break in and find it!</td>
</tr>
<tr>
	<th>Excela master</th>
	<td>I know pie charts should be only used on confectioner's conference, but boss likes them so much...</td>
</tr>
</table>

**Resources:**

<table>
<tr>
	<td>Private laptop that he DOES NOT SHARE! DO **NOT** TOUCH!</td>
	<td align="center" valign="center" width="20%" rowspan="4">
		<strong>+2</strong><br/>
		If used, add 2<br/>
	</td>
</tr>
<tr>
	<td>Homerem cofee mug</td>
</tr>
<tr>
	<td>Pinterest account</td>
</tr>
<tr>
	<td>Cubby with a set of tools and spare parts.</td>
</tr>
</table>

---
layout: mechanika-warsztat-jak-grac
title: "Junior Human Resources Specialist"
---
<br/>
<br/>
....................................

**Stanowisko**: {{ page.title }}
Jak przy podpisywaniu umowy został juniorem, tak pozostał nim do teraz, choć pracuje już trzy lata. Trochę rozgoryczony tytułem na stanowisku, spełnia się organizując różne wydarzenia. Wszystko ogarnie, wszystko załatwi. Potrafi łagodzić – lub podżegać – konflikty.

**Cechy:**
<table>
<tr>
	<th>Fizyczne</th>
	<td>&nbsp;0&nbsp;</td>
	<td>Dobra prezencja to podstawa!</td>
</tr>
<tr>
	<th>Społeczne</th>
	<td>&nbsp;+1&nbsp;</td>
	<td>Daje radę… Nie bez powodu mówimy o nich zasoby ludzkie.</td>
</tr>
<tr>
	<th>Naukowo-techniczne</th>
	<td>&nbsp;-1&nbsp;</td>
	<td>Maile czytam, facebooka używam, po co mi więcej?</td>
</tr>
</table>

**Impulsy:**

<table>
<tr>
	<th>Wizerunek firmy to podstawa!</th>
	<td>A jak inaczej zwabić ludzi, żeby tu przyszli?</td>
	<td align="center" valign="center" width="20%" rowspan="3">
		<strong>-2 / 0 / +2</strong><br/>
		Jeśli są zgodne, dodaj 2. <br/>
		Jeśli są przeciwne, odejmij 2<br/>
		Jeśli takie i takie – dodaj 0</td>
</tr>
<tr>
	<th>Kolekcjoner ludzi</th>
	<td>Znam wszystkich, a jeśli nie, to chcę wszystkich poznać! Do kolekcji brakuje mi jeszcze dobrego hydraulika</td>
</tr>
<tr>
	<th>To JA mam rację!</th>
	<td>No przecież!</td>
</tr>
</table>

**Umiejętności:**

<table>
<tr>
	<th>Znam kogoś…</th>
	<td> Przez moje ręce przeszło tylu kandydatów, że zawsze się ktoś znajdzie...</td>
	<td align="center" valign="center" width="20%" rowspan="6">
		<strong>+2</strong><br/>
		Jeśli używasz, dodaj 2<br/>
	</td>
</tr>
<tr>
	<th>Parkour</th>
	<td>Żaden kandydat mi nie umknie!</td>
</tr>
<tr>
	<th>Organizator</th>
	<td>Organizowałem nawet urodziny syna szefa!</td>
</tr>
<tr>
	<th>Procedury</th>
	<td>Aby to zrobić potrzebujesz formularza 46B/9...</td>
</tr>
<tr>
	<th>Ma gadane</th>
	<td>W nieformalnych dyskusjach aż rozkwita</td>
</tr>
<tr>
	<th>Bunt dziecka</th>
	<td>NIE I JUŻ! Ta straszliwa umiejętność sprawia, że nawet Spock czułby się nieswojo...</td>
</tr>
</table>

**Zasoby:**

<table>
<tr>
	<td>Plik nie przejrzanych CV</td>
	<td align="center" valign="center" width="20%" rowspan="4">
		<strong>+2</strong><br/>
		Jeśli używasz, dodaj 2<br/>
	</td>
</tr>
<tr>
	<td>Kontakty na LinkedIn, pracuj.pl, Goldenline…</td>
</tr>
<tr>
	<td>Znajomości we wszystkich działach</td>
</tr>
<tr>
	<td>Porządne buty do biegania</td>
</tr>
</table>
---
layout: mechanika
title: "Warsztat: 'Jak grać w rpg'"
---

# {{ page.title }}

Warsztat mający na celu wprowadzenie w naszą mechanikę w wersji uproszczonej.
Pozwala grupie nawet całkowicie niedoświadczonych grających przeprowadzić sesję gry fabularnej.

====

Workshop meant to introduce players to simple mechanics.
It allows even completely inexperienced group of players run a role-playing session.

**Świat: / World:**
Sesja rozgrywa się w stereotypowej korporacji.
Nasi gracze należą do jednego zespołu. 

====

Stereotype corporation.
Players are on one team and work towards common goal.

**Początek: / Beginning**
Jest poniedziałek, godzina 8 rano.
Przyszli do pracy, zrobili sobie kawę...

Nagle ich kąta w open space przychodzi ich szef i mówi:
>W piątek byłem na spotkaniu zarządu. Opowiedziałem o waszym projekcie. Żebyście widzieli, jak im się oczy zaświeciły! Przyjeżdżają dziś o 13, musicie przygotować prezentację tak, żeby zobaczyli, jaki wspaniały produkt zrobiliśmy! No, do dzieła! Wierzę, że rzucicie ich na kolana!

I szef poszedł.
Popatrzyli po sobie ze smutkiem.
Projekt nie jest nawet w połowie prac. 
Są zawaleni robotą, nie wiedzą, w co ręce włożyć, aż tu nagle to...

====

Monday, 8 A.M.
Team got to work, got their coffee...

Unexpectedly they are visited by their boss, who says:
> On Friday I was on higher management meeting. I told them about your project. You should have seen their eyes light up! They are coming today at 1 P.M. You must present your project to them so they see how awesome a product we have! Off you go! I have full trust in you!

And he left...

Team exchanged sad looks.
They are not even halfway ready with the project.
There is so much to do they don't know what to do next, and now this...

**Przygotowanie: / Preparation:**
* Wydrukuj po zestawie kart postaci na zespół graczy
* Wydrukuj kartę konliktów
* Gracze wybierają jedną z postaci: Informatyk, Prawnik, HR, Specjalista sprzedaży

**Materiały: / Materials:**

* [Karta konliktów](karta-konfliktow.html) [Conflict table (en)](conflict-table-en.html)
* [Kiedy pojawia się konflikt](/rpg/mechanika/kiedy-pojawia-sie-konflikt-uproszczone.html)  [When conflict arises](/rpg/mechanika/when-conflict-arises.html)

* Karty postaci (setting1):
    * [Informatyk](setting1/informatyk.html) [IT (en)](setting1/IT-en.html)
    * [Prawnik](setting1/prawnik.html)  [Lawyer (en)](setting1/lawyer-en.html)
    * [HR](setting1/hr.html) [HR (en)](setting1/hr-en.html)
    * [Specjalista sprzedaży](setting1/sales.html) [Sales (en)](setting1/sales-en.html)
    * [Ochroniarz](setting1/ochroniarz.html)
    * [Kierownik](setting1/kierownik.html)

* Karty postaci (setting2):
    * [Sales (en)](setting2/sales.html) 
    * [Marketing (en)](setting2/marketing.html)
    * [Purchase (en)](setting2/purchase.html)










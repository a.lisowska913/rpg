---
layout: mechanika
title: "Zbiorczy dokument mechaniki"
---

# {{ page.title }}

## 1. Aktualność dokumentu

* Dokument jest aktualny wobec mechaniki 1805; działa też z 1804.
* Dokument jest wyprodukowany: 180501
* Ostatnia aktualizacja: 180501

## 2. Generator losowy

| Wyciągnięte żetony | Znaczenie | Znaczenie dokładne |
|--------------------|-----------|--------------------|
| VV                 | Sukces    | Pełen sukces, 100% |
| V0, 0V             | Sukces    | Duży sukces, 75%   |
| VX, XV, 00         | Remis     | Skonfliktowany Sukces, 50% + Konsekwencje |
| X0, 0X             | Porażka   | Porażka, 0% + Konsekwencje |
| XX                 | Porażka   | Katastrofa, 0% + 2* Konsekwencje |

Przelosowanie wiąże się z 3* Konsekwencje.

## 3. Złożenie konfliktu

### 3.1. Karta postaci

* Karta postaci: Koncepcja, Motywacje, Działanie: 3,6,9
* Karta postaci: Zasoby: +3 (użycie: +1 Akcja)
* Karta postaci: Magia: +3 (użycie: +1 Akcja)

### 3.2. Stopień trudności

| Stopień trudności | Żetony Sukcesu | Żetony Remisu | Żetony Porażki |
|-------------------|----------------|---------------|----------------|
| Łatwy             | 2              | 3             | 4              |
| Typowy            | 0              | 3             | 7              |
| Trudny            | -2             | 3             | 10             |
| Heroiczny         | -6             | 5             | 20             |

Patrz na trzy rzeczy:

* Skala ("kogo jest więcej" lub "kto jest większy")
* Przewaga taktyczna ("kto ma lepszy teren", "kto ma lepszą sytuację")
* Przewaga sprzętowa ("miecz kontra pięści", "oślepiony mech kontra skradający się agent")

Jeśli mamy np. żołnierza próbującego poważnie uszkodzić mecha z broni zwykłej (skala -1, sprzęt -1) ale atakuje z zasadzki i ma czas na przycelowanie (taktyka +1), test normalnie Trudny staje się Heroiczny.

### 3.3. Modyfikatory uznaniowe

MG ma prawo dać od +1 do +4 modyfikatorów.

* Daj +1 za byle co; odgrywanie, opis sytuacji, jakikolwiek pomysł
* Daj +2 za coś co ma sens, zachęcaj Graczy
* Daj +3 jeśli na serio jesteś zaskoczony
* Daj +4 jeśli wszyscy dorzucają koncepty i aspekty i zaczyna się kształtować epicka scena i plan.

Alternatywnie, daj do +4 w zależności od użytych Aspektów Lokalizacji, Pogody, Otoczenia itp.

## 4. Konsekwencje konfliktów

Wynik konfiktu musi być obserwowalny w świecie gry. Nie może być czymś, co nie ma znaczenia. Mamy następujące opcje:

* Konsekwencje w świecie gry
* Korupcja życzenia Gracza

Przykład konsekwencji w świecie gry:

1. "udało Ci się, ale dostajesz Ranę (czy inny status)"
2. "udało Ci się zabić szefa, ale Cię wykryli"
3. "udało się, ale wszyscy Cię teraz nienawidzą"

Przykład korupcji życzenia:

1. "udało Ci się flirtem wkręcić na bal, ale łażą za Tobą teraz adoratorzy"
2. "zatrzymałeś plan przeciwnika, ale nie udało Ci się uratować tamtych ludzi"
3. "zniszczyłeś wioskę goblinów, przez co rozwaliłeś model ekonomiczny okolicy"

## 5. Wykorzystanie Konsekwencji

Co to są Konsekwencje:

* Konsekwencje generowane są przez Akcje Graczy. 1 Akcja -> 1 Konsekwencja.
* Konsekwencje generowane są przez Porażki i przerzuty ze strony Graczy.
* Konsekwencje to podstawowa waluta MG, jego środki do przesuwania rzeczywistości.

Co można z nimi zrobić:

| Działanie                                                  | Koszt Konsekwencji  |
|------------------------------------------------------------|---------------------|
| przesunąć odpowiedni Tor o 1 w prawo                       | 2-3-4-5 (+1 per lv) |
| Frakcja zdobywa cenny zasób                                | 2 |
| zadać Ranę na odpowiednim polu postaci Gracza              | 3 |
| Kupić test Heroiczny do użycia przeciw Graczom             | 5 |
| Zasadzka / Assault: 3 testy Trudne, Gracze muszą przetrwać | 5 |
| Nieśmiertelność dla postaci (NPC, przeciwnik) w Scenie, gwarancja wycofania  | 5 |

## 5. Wykorzystanie Wpływu

Co to jest Wpływ:

Wpływ generowany jest przez Porażki Graczy:

* Skonfliktowany Sukces / Remis: +1 Wpływ
* Porażka: +2 Wpływ

Co Gracz może z nimi zrobić:

| Działanie                                                       | Koszt Wpływu  |
|-----------------------------------------------------------------|---------------|
| Wygenerować ślad który jest Graczom potrzebny; kompas; "idź tu" | 1 |
| Podjąć decyzję za NPCa, zgodną z motywacją / charakterem        | 1 |
| Pozyskać rzadki i potrzebny zasób                               | 2 |
| Podjąć decyzję za NPCa, niezgodną z charakterem i podejściem    | 3 |

## 6. Po sesji

**Każdy** Gracz ma prawo powiedzieć jedno zdanie odnośnie tego jak bardzo zmieniła się rzeczywistość. Im więcej punktów Wpływu mieli podczas sesji, tym silniejsza zmiana może być wprowadzona.

## 7. Rany

Każda postać ma cztery pola działania (magowie mają pięć). Każde z tych pól ma pasujący do siebie Status / Ranę, której obecność podnosi stopień trudności konfliktów w tym obszarze o 1 kategorię.

| Obszar postaci | Typ rany / statusu                      |
|----------------|-----------------------------------------|
| Zdrowie        | Wyczerpany / Ranny                      |
| Umysł          | Wyczerpany / Zdekoncentrowany           |
| Społeczny      | Skompromitowany / Przerażony            |
| Zasoby         | Wyeksploatowany / Odcięty               |
| Magia          | Wyczerpany / Skażony / Zdekoncentrowany |

Jak widać, są statusy dewastujące postać ("Wyczerpany") i takie, które uszkadzają jedynie jeden obszar. Absolutnie każdą akcję da się powiązać z jednym z powyższych pól - Zdrowie, Umysł, Społeczny, Zasoby lub Magia.

Jeśli MG nie ma pomysłu co zrobić, zawsze może proponować Graczom Ranę ;-). Szybko wymyślą rozwiązanie.

Jeśli drugi raz w danym obszarze powinna pojawić się Rana, mamy następujące opcje:

* Postać jest wyłączona z akcji (traci przytomność, załamanie, koniec energii)
* Postać w danym obszarze otrzymuje trwałą słabość (np. w danej grupie społecznej ucierpi na stałe), czyli to trafia do Progresji

## 8. Oczekiwania od Graczy

1. Gracze grają postaciami, nie wcielają się w nie. Działanie na poziomie intencji (co chcesz osiągnąć) a nie akcji (co robisz).
2. Graczowi zależy na jakimś wariancie historii. Gra o coś. Chce czegoś osiągnąć, nie eksplorować.
3. Gracz jest stroną proaktywną, to gracz buduje historię.
4. Postacie Graczy są bezpieczne - ale niech postacie zachowują się jak odpowiednie żywe istoty w tym świecie.
5. Twoja postać jest częścią społeczności. Nie "mrocznym i tajemniczym odludkiem z jaskini".
6. Graczu, pozwól i pomóż grać innym. Pomóż z ich spotlightem i ich wątkami.

## 8. Oczekiwania od MG

1. Nie pytaj "co robisz" a "co chcesz osiągnąć" - skupiaj się na intencji
2. Pozwalaj Graczom wycofać akcję jeśli źle zrozumieliście interpretację
3. Jeśli Gracze stoją, zapytaj "czego Ci brakuje" i dojdź z nimi do tego jakimi ruchami to dostaną
4. Pozwól, by to Gracze tworzyli historię i opowieść w ekosystemie, który Ty wprowadzasz
5. Każdy konflikt ma być znaczący i coś zmieniać.
6. Nie wyłączaj Graczy ze scen. Nie blokuj wspólnej rozmowy.

---
layout: mechanika
title: "Komponent losowy"
---

# {{ page.title }}

## 1. Aktualność dokumentu

* Dokument jest aktualny wobec mechaniki 1801; działa też z 1709.
* Dokument jest wyprodukowany: 180102
* Ostatnia aktualizacja: 180103

## 2. Tabelka

|  **Wynik na kości**   | **Modyfikator**   |
| 20                    | +3                |
| 19, 18, 17, 16        | +2                |
| 15, 14, 13, 12, 11    | +1                |
| 10, 09, 08, 07, 06    | -1                |
| 05, 04, 03, 02        | -2                |
| 01                    | -3                |

## 3. Jak tego użyć

* Kostka: 1k20, przekształcona w pseudo-Gaussa.
* Rzuca jedna strona (zawsze gracz) 
    * Więc musi być od ujemnej do dodatniej (by rzucający nie miał przewagi)
    * Więc musi być symetryczny

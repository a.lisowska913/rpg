---
layout: mechanika
title: "Test: Terminus zaskoczony przez Gorathaula"
---

# {{ page.title }}

## Terminus Kamil spotyka gorathaula w podziemnym garażu...

Kamil, terminus (mag polujący na potwory) się spieszył do marketu; chciał kupić żonie bułkę z serem. Gdy tylko wszedł do podziemnego garażu, coś... nie pasowało. Skąd mgła w podziemnym garażu, zwłaszcza tak gęsta..?

Jego wyczulone na energię magiczną zmysły dały mu sygnał; mgła była magiczna. Odruchowo, Kamil szybkim ruchem skoczył między zaparkowane samochody - i tylko to uratowało mu życie. Potężne szczęki żółwiaka z mgieł zacisnęły się w miejscu, gdzie jeszcze przed _chwilą_ znajdował się Kamil. Terminus zauważył z przerażeniem, że żółwiak dał radę zmiażdżyć jego pole siłowe. Jednym ugryzieniem. Pole zostało zniszczone, ale Kamil ocalał...

Tyle, że Kamil został bez osłony.

Terminus zignorował swoją godność czy też fakt, że miał nowiutką koszulę i szybkim ruchem wtoczył się pod najbliższy samochód. Chwilę potem przetoczył się pod kolejny, mając nadzieję, że gorathaul nie będzie w stanie się do niego dostać. W końcu... gorathaul jest całkiem sporym potworem, _nie może_ od razu się dostać do Kamila!

...chwilę później Kamil aż krzyknął z bólu. Stożek mrozu zionięcia gorathaula praktycznie pozbawił Kamila przytomności; szczęśliwie, terminus wytrzymał. Szybko rozejrzał się w poszukiwaniu żółwiaka; nie mógł go jednak dojrzeć we mgle.

_Jeśli ta mgła nie zostanie rozwiana... nie mam żadnych szans przeżyć._

Kamil zdecydował się na desperacki gambit. Skupił się i rozpoczął inkantację prostego zaklęcia mającego na celu rozproszyć mgłę. Usłyszał, jak gorathaul przedziera się przez pojazdy na parkingu, ale zdecydował się to zignorować. Musiał rozwiać tą mgłę! Więc siedział ściśnięty pod samochodem z trudem układając palce w odpowiednie gesty i wypowiadając słowa zaklęcia w czasie gdy dwumetrowy żółwiak próbował się do niego dostać.

Triumf! Kamil skończył zaklęcie i mgła zaczęła się rozwiewać. Ze złośliwym uśmiechem zogniskował zaklęcie dookoła pancerza żółwiaka; to powinno zdecydowanie przeszkodzić mu w przywołaniu kolejnej porcji mgły. Po chwili jednak radość terminusa zmieniła się w przerażenie; gad wystrzelił swój łeb jak wąż rozdzierając samochód stanowiący osłonę Kamila. Terminus błyskawicznie spróbował się wydostać spod auta, jednak rozciął sobie rękę o poskręcane blachy.

_Zapach benzyny?!_

Kamil usłyszał krzyki. Niewielka grupka nastolatków znalazła się na parkingu i dostrzegła tą scenę; rozwiewająca się mgła nie była jedynie korzystnym zjawiskiem.

_Cywile... czemu tam zawsze muszą być jacyś cywile?!_

Widząc oczy gorathaula na sobie, Kamil zdecydował się zrobić to, czego można by się spodziewać po łowcy potworów w takiej sytuacji. Uciekać. Między samochodami, klucząc, by zgubić żółwiaka i zrobić między sobą a potworem odpowiedni dystans na rzucenie jakiegoś zaklęcia bojowego. Pal diabli Maskaradę, tu trzeba najpierw przeżyć!

Terminus ucieszył się, że swego czasu trenował parkour. Paręnaście sekund później udało mu się wystarczająco wyprzedzić gorathaula... tylko po to, by zorientować się, że gad skupił się na innej ofierze - młodym chłopaku pod ścianą, sparaliżowanym widokiem gigantycznego żółwiaka.

_Nie zdążę tego cholerstwa rozwalić zanim on dopadnie tego człowieka!_

Terminus odwrócił się i wrzasnął do chłopaka, by ten przebiegł przez najbliższe drzwi. Powinno mu się udać, a gad by się zwyczajnie nie zmieścił. Ku radości terminusa, chłopak czmychnął... a gad poczłapał za nim. Kamil nie czekał już dłużej; zaczął splatać swoje najcięższe zaklęcie bojowe, _Atak Serca_. Widząc, że gorathaul nadal skupiony był na chłopaku, Kamil zaczął splatać silniejsze runy. Miał chwilę czasu, mógł zrobić to tak potężnym czarem jak tylko potrafił... to była szansa, na jaką czekał...

...i po skończeniu zaklęcia uderzył nim w gorathaula. Żółwiak ryknął w agonii; zaklęcie jednak nie było dość silne, by zabić potwora. Kamila to specjalnie nie zdziwiło; gorathaule słynęły z wytrzymałości. Ale cywilowi to wystarczyło, by zaczął uciekać jeszcze szybciej.

_Cholera, muszę go potem złapać, by nie pojawiły się plotki na mieście!_

Gorathaul zaczął szarżować w kierunku Kamila. Terminus, lekko zdekoncentrowany uciekającymi ludźmi, nie zauważył, że stracił okazję do bezpiecznego wycofania się przed gadem. Taki amatorski błąd... Zrezygnowany, zaczął składać kolejny _Atak Serca_. Przy odrobinie szczęścia zdąży z zabiciem żółwiaka, zanim ten go dopadnie...

_Jeszcze troszkę..._

By zwiększyć moc zaklęcia, Kamil skupił się na swoich kryształach Quark, skrystalizowanej energii magicznej. Kryształy zaczęły się rozpuszczać a energia magiczna zasiliła zaklęcie bojowe. Terminus skończył inkantację i wystrzelił czar prosto w pysk gada, szarżującego nań na swoich żółwiokształtnych łapkach.

Gorathaul został trafiony zaklęciem. Zrobił jeszcze kilka chwiejnych kroków, po czym padł na ziemię; wydał z siebie cichy pisk, po czym skonał.

Kamil rozejrzał się dookoła. Zobaczył ślady zniszczeń wywołane przez wielkiego gada. Zobaczył... brak świadków, czym będzie musiał się zająć - wszyscy uciekli. Zobaczył wielkie cielsko martwego żółwiaka. Wyjął telefon i zadzwonił do swojej żony.

_Kochanie, lepiej zamów pizzę. Coś mi wypadło i muszę się tym jeszcze zająć zanim wrócę do domu..._

## Mechanicznie, analiza powyższej walki

**Tura 1:**

Kamil wchodzi do garażu i próbuje zorientować się w zagrożeniu:

| .Impuls. | .Specka. | .Umiejka. | .Inklinacja. | .CECHY. |  .Sytuacja.      | .Sprzęt. | .Magia. | .Surowiec. |  .AKCJA. | .POSTAĆ. |
|   +      |   +      |     +     |     +        |     4   |  - (zaskoczenie) |     0    |    0    |      0     |    -1    |      3   |

Gorathaul przygotowuje się do teleportacyjnego ataku:

| .VERSUS. | .Diff.                     | .Wpływ. | .Skala. | .MODIFIERS. | .OPOZYCJA. |
|     2    |  +++ (atak z zaskoczenia)  |     0   |     0   |     3       |      5     |

Wynik konfliktu (rzut wykonany ręcznie):

| Postać | Opozycja | Test? | Rzut       | Wynik  |
|   3    |    5     |  -2   | -3 W-2 (1) | -5 W-2 |

W wyniku nie tylko Kamil _przegrał_, ale też gorathaul jest w stanie doprowadzić do straszliwych zniszczeń (wpływ duży (3) podniesiony jeszcze o dwa poziomy do (5)). Gracz Kamila nie zaakceptuje takiego rozwiązania, zdecydował się przerzucić na kości. Zdecydował, że gorathaul zniszczył pole siłowe Kamila. Więc jeszcze raz:

| Postać | Opozycja | Test? | Rzut        | Wynik |
|   3    |    5     |  -2   | +2 W+1 (17) | 0 W-1 |

Zdecydowanie lepiej. Remis i Kamil ucierpi jedynie w ramach pomniejszego wpływu. Gracz Kamila zdecydował się nie wykorzystywać kolejnej komplikacji fabularnej; przyjmuje to na Licznik (1/5). Cóż, mogło być gorzej.

**Tura 2:**

Kamil nie ma osłony i nadal nie wie, gdzie znajduje się gorathaul. Gracz Kamila zaproponował, że terminus spróbuje schować się pod jakimś samochodem by zdobyć lepszą pozycję do walki. To zniweluje możliwość zaatakowania przez gorathaula jego najgroźniejszym atakiem, aczkolwiek wystawi Kamila na uderzenie stożkiem zimna...

Kamil chowa się pod samochodem:

| .Impuls. | .Specka. | .Umiejka. | .Inklinacja. | .CECHY. |  .Sytuacja.      | .Sprzęt. | .Magia. | .Surowiec. |  .AKCJA. | .POSTAĆ. |
|   +      |   +      |     +     |     +        |     4   |  1 (parking    ) |     0    |    0    |      0     |     1    |      7   |

Gorathaul próbuje go zatrzymać (mały wpływ, bo to jedynie kupienie pozycji przez Kamila):

| .VERSUS. | .Diff.                     | .Wpływ.    | .Skala. | .MODIFIERS. | .OPOZYCJA. |
|     2    |  + (nadal jest mgła)       |  mały (--) |     0   |     -1      |      1     |

W wyniku:

| Postać | Opozycja | Test? | Rzut        | Wynik |
|   7    |    1     |  +6   | -1 W0 (7)   | 5 W0  |

Kamil znalazł się pod samochodem bez szczególnych kłopotów. Tyle, że musi osłonić się przed stożkiem zimna gorathaula:

| .Impuls. | .Specka. | .Umiejka. | .Inklinacja. | .CECHY. |  .Sytuacja. | .Sprzęt.           | .Magia. | .Surowiec. |  .AKCJA. | .POSTAĆ. |
|   0      |   +      |     +     |     +        |     3   |    0        | 1 (pod samochodem) |    0    |      0     |     1    |      4   |

A stożek gorathaula wygląda tak:

| .VERSUS. | .Diff.                 | .Wpływ. | .Skala. | .MODIFIERS. | .OPOZYCJA. |
|     2    |  + (trudno wytrzymać)  |     0   |     0   |     1       |      4     |

Więc w wyniku:

| Postać | Opozycja | Test? | Rzut        | Wynik  |
|   4    |    4     |   0   | -1 W0 (6)   | -1 W0  |

Kamil ma wyjątkowo pechowe rzuty. Przy standardowym Wpływie to oznaczałoby koszt (2) do Licznika, który w wypadku Kamila jest już osłabiony (1/5). Z ciężkim sercem, gracz Kamila zaproponował komplikację fabularną: jak skończy się walka z gorathaulem, właściciel samochodu będzie miał do Kamila pretensje, że ów samochód został przez Kamila _jakoś_ uszkodzony. Mistrz Gry się z radością zgodził.

**Tura 3:**

Czas na kontratak. Kamil spróbuje rozwiać mgłę używając zaklęcia. Póki ta mgła tu jest, gorathaul jest całkowicie niemożliwy do pokonania przez samotnego terminusa. Kamilowi bardzo na tym zależy, więc gracz Kamila ma zamiar wykorzystać do tego celu surowiec.

| .Impuls. | .Specka. | .Umiejka. | .Inklinacja. | .CECHY. |  .Sytuacja.              | .Sprzęt. | .Magia. | .Surowiec. |  .AKCJA. | .POSTAĆ. |
|   +      |   +      |     +     |     +        |     4   |  1 (bezpieczna pozycja ) |     0    |    1    |      1     |     3    |      7   |

Mgła gorathaula jest magiczna i ogólnie nie tak łatwo ją rozwiać; szczęśliwie, Kamil włada magią powietrza. Widząc dysproporcję matematyczną, Kamil zdecydował się podnieść Wpływ: oczekuje Wpływu dużego a nie Zwykłego. I tak jeśli rzuci mniej niż 6 to będzie musiał mieć przerzut, a tak może coś jeszcze wygrać... szczęśliwie jest osłonięty, więc jest w stanie rzucić długie, 30-sekundowe zaklęcie.

| .VERSUS. | .Diff.       | .Wpływ. | .Skala. | .MODIFIERS. | .OPOZYCJA. |
|     2    |  ++          | duży 2  |     0   |     4       |      6     |

Więc w wyniku:

| Postać | Opozycja | Test? | Rzut        | Wynik  |
|   7    |    6     |   1   | 1 W0 (11)   |  2 W0  |

Mgła została rozwiana, ale przy podniesionym Wpływie gracz Kamila zaproponował, by gorathaul miał na sobie stałe zaklęcie wiatru; dzięki temu będzie mu dużo trudniej (-2) wejść we mgłę ponownie. Mistrz Gry znowu się zgodził.

Gorathaul tymczasem spróbował użyć jeszcze raz swoich pazurów oraz szczęki. Jako, że Kamil jest unieruchomiony, ów nie ma większych szans na unik - ale pozycja pod samochodem daje mu bardzo dobre miejsce ochronne (gorathaul jest zwyczajnie za duży i nie ma wygody w manewrowaniu).

| .Impuls. | .Specka. | .Umiejka. | .Inklinacja. | .CECHY. |  .Sytuacja.        | .Sprzęt.     | .Magia. | .Surowiec. |  .AKCJA. | .POSTAĆ. |
|   0      |   +      |     +     |     +        |     3   |  -1 (koncentracja) | 2 (samochód) |    0    |      0     |     1    |      4   |

A gorathaul:

| .VERSUS. | .Diff. | .Wpływ. | .Skala. | .MODIFIERS. | .OPOZYCJA. |
|     2    |  +++   |    0    |     0   |     3       |      5     |

W wyniku sytuacja wygląda kiepsko dla Kamila:

| Postać | Opozycja | Test? | Rzut       | Wynik  |
|   4    |    5     |  -1   | -2 W-1 (2) | -3 W-1 |

...zwłaszcza, że Kamil ma bardzo kiepskie rzuty. Wpływ Duży (3) podniesiony o oczko (do 4) mógłby sprawić, że Kamil jest całkowicie pokonany (licznik 1/5). Komplikacja fabularna - niech na scenie pojawią się ludzie. Tacy nie wiedzący o magii. Widząc ogromnego żółwiaka, zdecydowanie łamie to Maskaradę, co będzie problemem dla Kamila... a dodatkowo daje cele dla gorathaula. Więc, przerzut.

| Postać | Opozycja | Test? | Rzut       | Wynik  |
|   4    |    5     |  +1   | +1 W0 (13) |  0 W0  |

A więc remis. Taki z dużym wpływem (3). Niech gorathaul zniszczy samochód i dojdzie do wycieku benzyny; terminus zostanie lekko ranny i straci osłonę. A teren zrobił się bardzo niebezpieczny. Z ciężkim sercem, gracz Kamila się zgodził na to rozwiązanie.

**Tura 4:**

Kamil zdecydował się na ucieczkę przed gorathaulem, klucząc między samochodami. Intencja: możliwość bezpiecznego rzucenia zaklęcia przeciwko żółwiakowi w przyszłej turze. 

| .Impuls. | .Specka. | .Umiejka. | .Inklinacja. | .CECHY. |  .Sytuacja.                | .Sprzęt. | .Magia. | .Surowiec. |  .AKCJA. | .POSTAĆ. |
|   0      |   0      |     +     |     +        |     2   |   2 (teren, wielkość gada) |  0       |    0    |      0     |     2    |      4   |

W świetle tego gorathaul nie jest w szczególnie dobrej sytuacji:

| .VERSUS. | .Diff. | .Wpływ. | .Skala. | .MODIFIERS. | .OPOZYCJA. |
|     2    |  -     |    0    |     0   |     -1      |      1     |

Więc Kamilowi pewnie się uda...

| Postać | Opozycja | Test? | Rzut      | Wynik  |
|   4    |    1     |  +3   | -1 W0 (8) |  2 W0  |

Sukces. Kamil będzie w stanie w przyszłej turze rzucić bezpiecznie zaklęcie poza zasięgiem gorathaula. 

**Tura 5:**

Gorathaul skupił się na zaatakowaniu cywila. Kamil nie jest w stanie zabić żółwiaka jednym zaklęciem; zamiast tego próbuje zmusić chłopaka, by ten usunął się z pola rażenia gada i przebiegł przez drzwi, przez które gorathaul nie da rady się przedostać. 

| .Impuls. | .Specka. | .Umiejka. | .Inklinacja. | .CECHY. |  .Sytuacja.           | .Sprzęt. | .Magia. | .Surowiec. |  .AKCJA. | .POSTAĆ. |
|   +      |   0      |     +     |     0        |     2   |   -1 (daleko, głośno) |  0       |    0    |      0     |    -2    |      0   |

Chłopaka jest łatwo przekonać, by coś zrobił; też Wpływ w tej sytuacji jest niewielki.

| .VERSUS. | .Diff. | .Wpływ. | .Skala. | .MODIFIERS. | .OPOZYCJA. |
|     2    |  -     |   -2    |     0   |     -3      |     -1     |

Czy udało się Kamilowi uratować cywila swoim rozkazem?

| Postać | Opozycja | Test? | Rzut      | Wynik  |
|   0    |   -1     |  +1   | 3 W2 (20) |  3 W2  |

Zdecydowanie tak. W ramach podniesionego Wpływu do poziomu Dużego (3) gracz Kamila wraz z Mistrzem Gry ustalili, że gorathaul zafiksował się na tamtej ofierze, kupując Kamilowi czas na rzucenie potężniejszego zaklęcia.

**Tura 6:**

Kamil ma czas na złożenie silniejszego czaru (Duży Wpływ, bez kosztu). Czas zacząć zabijanie żółwiaka. 

| .Impuls. | .Specka. | .Umiejka. | .Inklinacja. | .CECHY. |  .Sytuacja.            | .Sprzęt. | .Magia. | .Surowiec. |  .AKCJA. | .POSTAĆ. |
|   +      |   +      |     +     |     +        |     4   |   +1 (odwrócona uwaga) |  0       |    1    |      0     |    2     |      6   |

Żółwiak broni się czystymi pasywnymi defensywami

| .VERSUS. | .Diff. | .Wpływ. | .Skala. | .MODIFIERS. | .OPOZYCJA. |
|     2    |  +++   |    0    |     0   |      0      |      5     |

I wynik:

| Postać | Opozycja | Test? | Rzut       | Wynik  |
|   6    |    5     |  +1   | -2 W-1 (4) | -1 W-1 |

Nie jest to dla gracza Kamila wynik akceptowalny; jednak decyduje się na przerzut. W ramach kosztu: gorathaul będzie w stanie w przyszłej turze związać Kamila walką.

| Postać | Opozycja | Test? | Rzut       | Wynik  |
|   6    |    5     |  +1   | +1 W0 (12) |  2 W0  |

Zaklęcie Kamila ma Duży Wpływ; redukuje więc licznik żółwiaka z (0/5) do (3/5). Teraz nawet zaklęcie o Standardowym Wpływie wystarczy, by móc żółwiaka zabić.

**Tura 7:**

Sytuacja jest interesująca, bo żółwiak atakuje _drugi_. Czyli wpierw rozpatrujemy atak terminusa a jeśli gorathaul przeżyje, atakuje przeciwko terminusowi, który nie jest zdekoncentrowany. Dystans. A Kamil decyduje się wykorzystać jeden surowiec by zwiększyć prawdopodobieństwo sukcesu.

| .Impuls. | .Specka. | .Umiejka. | .Inklinacja. | .CECHY. |  .Sytuacja.    | .Sprzęt. | .Magia. | .Surowiec. |  .AKCJA. | .POSTAĆ. |
|   +      |   +      |     +     |     +        |     4   |   +1 (dystans) |  0       |    1    |      1     |    3     |      7   |

Gorathaul "szybko" biegnie by zaatakować Kamila. Nadal broni się tylko pasywnie.

| .VERSUS. | .Diff. | .Wpływ. | .Skala. | .MODIFIERS. | .OPOZYCJA. |
|     2    |  +++   |    0    |     0   |      0      |      5     |

Wynik:

| Postać | Opozycja | Test? | Rzut       | Wynik  |
|   7    |    5     |  +2   | +1 W0 (13) |  3 W0  |

Gorathaul został trafiony zaklęciem o Wpływie typowym. To oznacza (2) oczka do licznika a aktualny licznik gorathaula to (3/5). Innymi słowy, żółwiak jest martwy a walka się skończyła...

## Przeciwnicy, parametry

**Gorathaul, żółwiak z mgieł**
* Specjalne: ma dostęp do ataków z fazy Mgły i fazy Znalezionej; Mgła go wzmacnia
* Faza: mgła
    * niewykrywalny; bardzo trudny (2) do znalezienia we mgłach
    * atakuje z zaskoczenia; praktycznie niemożliwe (3) by przewidzieć jego atak i go uniknąć
    * teleportuje się; prawie niewrażliwy (+3 defensywa Wpływ duży) by go w ogóle trafić
    * we mgłach praktycznie nieadresowalny (3, licznik: 3 do zlokalizowania)
    * mgła jest bardzo trudna (2) do rozwiania; jest magiczna i emitowana przez gorathaula
* Faza: znaleziony / bez mgły
    * wiązka mroźnej magii; trudno (1) się oprzeć stożkowi (SPN/FRT) a bardzo trudno (2) go uniknąć (NMB); atak lodowy i magiczny
    * straszliwe szczęki; krytycznie mocarne (+3 vs FRT) acz przeciętnie celne (+0 vs NMB) ogromne obrażenia (Wpływ duży) magiczne i fizyczne
    * odporny żółwiak; (FRT/SPN) bardzo, bardzo odporny (+3 vs fizyczne i magiczne) i pancerny (Licznik: 5)
    * słaby punkt pod skorupą (NMB); wrażliwy na egzekucję (liczniki: 2)
    * ukrycie we mgle; jeśli się mu szczególnie nie przeszkadza (zwykła trudność: 2), potrafi wywołać mgłę i wrócić do fazy Mgły.
    * Niezbyt szybko się porusza (-1) jeśli nie ma mgły i nie może się teleportować

**Kamil, terminus polujący na bułkę z serem**
* Inklinacje bojowe podniesione
* Specjalizacja: walka z potworami
* Umiejętności: standardowe bojowe
* Sprzęt: standardowy sprzęt terminusa walczącego z potworami

---
layout: mechanika
title: "Rozwiązywanie Konfliktów, 1801"
---

# {{ page.title }}

## 1. Aktualność dokumentu

* Dokument jest aktualny wobec mechaniki 1801; działa też z 1709.
* Dokument jest wyprodukowany: 180101
* Ostatnia aktualizacja: 180122

## 2. Flowchart, wysokopoziomowo

![Flowchart mechaniki CR, wysokopoziomowo](Materials/180101_cr/cr_flowchart.png)

## 3. Moja pierwsza sesja RPG

### 3.1. Modyfikacja zasad

Przede wszystkim, gratulacje. To powinien być dobry wybór ;-).

Proponujemy wykorzystać nieco uproszczone zasady:

* Poziom postaci: jest tylko Bardzo Pasuje (+2) lub Nie Pasuje (0).
    * W wypadku wątpliwości, załóż, że Bardzo Pasuje. Na korzyść Gracza.
    * Zignoruj wszystkie parametry ujemne (Motywacja, Silne i Słabe Strony)
* Poziom przeciwnika: wszystkie są Zwykłe
* Zignoruj Skreślanie aspektów. Po prostu się je policzy.

### 3.2. Wyjaśnienie zmian

1. Mechanika przyspieszy. To jest korzystne, bo na początku mechanika może wyglądać na trudną.
2. Postacie graczy będą trochę silniejsze a Przeciwnicy będą słabsi, więc Graczom będzie trochę łatwiej.
3. Mniej problemów z wyczuciem uznaniowości (kontruje czy niweluje? Pasuje czy nie pasuje?)

Ogólny 'feel' gry będzie działał tak samo. Różnica w tym, że postaciom jest łatwiej i nieco mniej dokładnie muszą być odgrywane.

## 4. Wyjaśnienie poszczególnych kroków mechaniki

### 4.1. Poziom sił w konflikcie:

#### 4.1.1. Poziom Postaci:

Ogólna zasada: idziemy w _prędkość_ a nie _precyzję_, za wyjątkiem najważniejszych konfliktów. Innymi słowy: przeglądamy pobieżnie kartę postaci.

* Jeśli coś **bardzo pasuje**, dodajemy +2 za kategorię.
* Jeśli coś **pasuje**, dodajemy +1 za kategorię.
* Jeśli coś **nie pasuje**, nie dodajemy nic za kategorię.

Dla poszczególnych kategorii:
* **Motywacja**: sprawdzamy, czy **bardzo pasuje**, **pasuje** czy **nie pasuje**.
    * Jeśli życie postaci jest zagrożone, to "bardzo pasuje" za wyjątkiem _dziwnych sytuacji_ (postać chce umrzeć)
    * Może być ujemna, jeśli sytuacja jest sprzeczna z Motywacją postaci (np. paladyn, który próbuje kraść)
* **Umiejętności**: sprawdzamy, czy **bardzo pasują**, **pasują** czy **nie pasują**.
* **Silne i słabe strony**: sprawdzamy, czy **bardzo pasują**, **pasują** czy **nie pasują**. Ale rozpatrujemy osobno silne i słabe strony - jeśli słabe strony wchodzą, to liczymy je ujemnie.
* **Magia**: jeśli czarodziej ma okazję na rzucenie czaru (uruchomione Akcją), to też sprawdzamy czy **bardzo pasuje**, **pasuje** czy **nie pasuje**.
* **Otoczenie**: jeśli zostało uruchomione Akcją, też sprawdzamy, czy **bardzo pasuje**, **pasuje** czy **nie pasuje**.

Innymi słowy, standardowa postać ma następującą wartość bazową testu:
    * Człowiek bez Otoczenia: [-4, 4]
    * Człowiek, uruchamia Otoczenie: [-2, 6]
    * Mag, czaruje i uruchamia Otoczenie: [0, 8]

### 4.1.2. Poziom Przeciwnika:

Przeciwnik może mieć jednen z następujących poziomów:

* **Łatwy**: 
    * Stopień Trudności: 0
    * Oznacza prostą, rutynową czynność; zwykle bez aspektów otoczenia nie występuje. 
    * Po co to konfliktować? Chyba tylko, jeśli sytuacja / otoczenie ma trudne aspekty.
    * Przykład: Prowadzenie samochodu - próba dojechania do celu.
* **Zwykły**:
    * Stopień Trudności: 5
    * Oznacza czynność typową 
    * 75% konfliktów wykorzystuje tą bazę
    * Przykład: Prowadzenie samochodu - próba dojechania do celu ZANIM coś się wydarzy.
* **Trudny**:
    * Stopień Trudności: 10
    * 20% konfliktów wykorzystuje tą bazę
    * Oznacza czynność którą wykona bezproblemowo (Pełen Sukces) jedynie ekspert lub fachowiec z narzędziami
    * Przykład: Próba przełamania solidnych zabezpieczeń, jak ma się sporo czasu.
* **Elitarny**:
    * Stopień Trudności: 15
    * 4.9% konfliktów wykorzystuje tą bazę
    * Oznacza czynność którą wykona bezproblemowo (Pełen Sukces) jedynie ekspert z narzędziami
    * Przykład: Próba przełamania solidnych zabezpieczeń ZANIM coś się wydarzy.
* **Nadludzki**:
    * Stopień Trudności: 20
    * 0.1% konfliktów wykorzystuje tą bazę
    * Oznacza czynność niemożliwą do wykonania przez człowieka nie mającego zdecydowanej przewagi i doświadczenia.
    * Przykład: Próba opracowania sposobu na przetrwanie na Esuriit przez następny tydzień.

### 4.2. Wykorzystanie Akcji przez gracza:

#### 4.2.1. Deklaracja Konfliktu

* By móc uczestniczyć w konflikcie, Gracz musi wykorzystać jedną Akcję.
    * Gracz zaczyna konflikt z innym Graczem lub MG.
    * Gracz mówi "nie" na opis wydarzenia deklarowanego przez MG czy innego Gracza.
    * Gracz chce pomóc innemu Graczowi w akcji.
* Konflikt jest zakończony wtedy, gdy któraś ze stron przegrywa coś znaczącego.
* Przeciwnicy posiadający Trwałość tracą 1 punkt Trwałości za każdym razem, gdy przegrywają konflikt.

#### 4.2.2. Uruchomienie Otoczenia

* By móc wykorzystać Otoczenie podczas konfliktu, Gracz musi wykorzystać jedną (dodatkową) Akcję.
    * Gracz może wykorzystać tylko jedną Akcję na wykorzystanie Otoczenia podczas konfliktu.
    * Rzeczy zdobyte wcześniej przez Gracza **w wyniku poprzednich konfliktów** (np. wiedza o słabości przeciwnika) **nie** są Otoczeniem. Należą do Modyfikatorów Sytuacyjnych i są dla postaci zawsze dostępne.
    * Rzeczy uzyskane przez Progresję muszą być wykorzystane przez Akcję.
* Przez wykorzystanie Otoczenia podczas konfliktu Gracz może powołać dowolny Aspekt ze swojego Otoczenia, jeśli potrafi wyjaśnić jak ten Aspekt rozwiązuje jego problem.

W efekcie, Gracz wykorzystuje swoje Otoczenie i dostaje Aspekt jaki najbardziej mu pasuje. Ten Aspekt najpewniej będzie Aspektem Kontrującym (niszczącym Aspekt negatywny), więc Gracz dostanie: +2 (Otoczenie) +2 (Aspekt pozytywny) +2 (zlikwidowanie Aspektu negatywnego). Matematyczna moc użycia Otoczenia jest więc między [2,6].

Przypominam, że człowiek bez Otoczenia ma deltę 8 ([-4, 4]), więc właściwe użycie Otoczenia to sprawa dość poważna.

Prosty przykład: wykorzystanie 'szybkiego motoru' znajdującego się w Otoczeniu postaci, gdy postać próbuje uciec ścigającym ją na piechotę bandytom to zdecydowanie +2 (Otoczenie), +2 (szybki motor), +2 (zniwelowanie "znają teren"). Czyli +6.

#### 4.2.3. Uruchomienie Magii

* By móc wykorzystać Magię podczas konfliktu, Gracz musi wykorzystać jedną (dodatkową) Akcję.
    * Gracz może wykorzystać tylko jedną Akcję na wykorzystanie Magii podczas konfliktu.
* Przez wykorzystanie Magii podczas konfliktu Gracz może stworzyć **dowolne** zaklęcie, na które pozwalają mu Aspekty jego szkół magicznych. To zaklęcie może stworzyć **dowolny Aspekt** dodany do tej sytuacji.

W efekcie, Gracz wykorzystuje Magię i dostaje Aspekt najlepiej rozwiązujący problem Gracza. Nie wierzę, że to nie będzie Aspekt Kontrujący (niszczący Aspekt negatywny). W efekcie Gracz dostanie: +2 (Magia) +2 (Aspekt pozytywny) +2 (zlikwidowanie Aspektu negatywnego). Matematyczna moc Magii to też między [2,6].

Magia jest prostsza do użycia dla Gracza niż Otoczenie i jest nie mniej potężna. Jako, że Otoczenie i Magia synergizują, można spodziewać się naprawdę dużej mocy.

Prosty przykład użycia samej Magii: wykorzystanie 'kojącego spokoju' przez biomantkę specjalizującej się w magii leczniczej, gdy próbuje przekraść się koło stada psów: +2 (Magia), +2 (kojący spokój) +2 (zlikwidowanie "czujne psy"). Proste +6.

#### 4.2.4. Pomoc innej postaci

Gracz jest w stanie pomóc innej postaci wykonać jej konflikt. By to zrobić, musi wydać swoją Akcję. Za to może użyć dowolnego aspektu ze swoich Umiejętności czy Otoczenia (jeśli ma to sens z perspektywy świata fikcji) i dołożyć ten aspekt do konfliktu.

### 4.3. Wykorzystanie Problemów przez MG:

* TODO. 
* MG ma jakąś pulę Problemów zaczynając Opowieści; to jego główna waluta.
* Ruchy specjalne viciniusów, przerzuty (?), nadanie / regeneracja Trwałości, dodanie okoliczności sytuacji itp.
* Ogólnie, trzeba to wymyśleć na bazie przyszłych Opowieści. Za mała próbka Opowieści rozegranych przy użyciu mechaniki 1801 by to sprawdzić.

### 4.4. Selekcja aspektów

#### 4.4.1. Aspekty postaci gracza

Z każdej kategorii (Motywacja, Umiejętności, Silne i Słabe Strony) weź jeden aspekt, który pasuje do sposobu rozwiązania przez Ciebie problemu. Opisz, jak tymi aspektami rozwiązujesz ten problem. Warto to robić, bo każdy aspekt przekłada się na +2 docelowo.

#### 4.4.2. Aspekty przeciwnika

**Przeciwnik ma Profil:**

Przeciwnik posiada jakąś formę karty (Profilu). Z listy wszystkich posiadanych aspektów wymień te, które pasują do tego, co chce zrobić Gracz. W odróżnieniu od Gracza, Przeciwnik nie ma limitów wybierania aspektów. Potem przejdź do Okoliczności Przeciwnika ;-).

**Przeciwnik nie ma Profilu:**

Cóż, trudno. Pomyśl, czy nie da się wytworzyć 1-2 aspektów pasujących do tego konfliktu, który został zadeklarowany przez Gracza? Może - jeśli atakowany jest wojownik mieczem - przeciwnik jest Wyszkolony? Najpewniej 1-2 aspekty uda się wymyśleć na bieżąco.

* **0-1 aspektów**: dość prosty przeciwnik, prosty konflikt.
* **2-3 aspekty**: zwykły przeciwnik; standardowy konflikt do rozwiązania przez Graczy.
* **4-6 aspektów**: trudny przeciwnik; elitarna jednostka, acz jeszcze nie boss.
* **7+ aspektów**: najpewniej mamy do czynienia z bossem. Zwłaszcza, jeśli ma Trwałość. Konflikt będzie długi.

**Okoliczności Przeciwnika:**

Czy Przeciwnik ma jakiś specjalny status? Może jest Ranny? Może jest W Szale Bojowym? To też są aspekty wzmacniające lub osłabiające Przeciwnika w danym kontekście.

#### 4.4.3. Aspekty sytuacyjne

Rozpatrz trzy podstawowe pytania _w kontekście tego konfliktu_:

* **Skala**: 
    * Kogo jest "więcej"? (2 zabijaków kontra postać to przewaga Skali)
    * Kto ma "większą skalę"? (czołg kontra człowiek to przewaga Skali)
* **Sprzęt**: 
    * Kto ma lepszy sprzęt? (ktoś z mieczem na otwartej przestrzeni kontra ktoś ze sztyletem to przewaga Sprzętu)
    * Ogólnie, Przedmioty / Wiedza / Sprzęt
* **Sytuacja**: 
    * Kto jest zaskoczony?
    * Kto jest pod presją czasu?
    * Ogólnie: Czas / Taktyka / Teren

Następnie sprawdźcie, czy miejsce w jakim się znajdujecie nie ma własnych aspektów które jakaś ze stron może użyć. Może jest Ciemno, może Trudny Teren, może Aura Magiczna? Może pogoda coś zmienia? Otoczenie sytuacyjne? Tak czy inaczej, wypiszcie te aspekty i przydzielcie je do odpowiednich stron.

### 4.5. Skreślanie Aspektów

#### 4.5.1. O co chodzi

Czasami mamy do czynienia z taką sytuacją, że po jednej ze stron jest aspekt sprawiający, że ten po drugiej stronie nie ma żadnej możliwości zadziałać. Jeśli tak jest, SKREŚL aspekty, które są skontrowane.

* Przykład 1: Ania ma 'szybki bieg' a Basia ma 'szybki motor'. Aspekt Basi kontruje aspekt Ani
    * Skreśl "szybki bieg". Ten aspekt nie jest rozpatrywany dalej.
* Przykład 2: Ania chce wziąć jabłko z drzewa, na którym znajduje się Gniazdo Wściekłych Szerszeni.
    * Gniazdo ma aspekty: 'sporo tam szerszeni', 'są naprawdę wściekłe'
    * Ania ma aspekt: 'ognista tarcza', kontrujący wszystkie aspekty Gniazda Szerszeni. Skreśl je.

Warto spojrzeć na mapę konfliktu i skreślić wszystkie skontrowane aspekty.

#### 4.5.2. Jak to wyczuć?

Kontekst. Doświadczenie. Innymi słowy, trzeba zrobić sporo pomyłek zanim się tego nauczysz ;-).

Czasami możesz mieć sytuację, w której aspekt kontruje kilka aspektów, ale sam jest skontrowany. I co wtedy? Przykład:

* Ania ma 'tarczę ognistą'.
* Gniazdo Szerszeni ma 'sporo tam szerszeni', 'są naprawdę wściekłe', 'stałe pole antymagiczne'

Jeśli 'pole antymagiczne' i 'tarcza ognista' się zniwelują, to latające tam szerszenie powinny móc Anię pociąć (nie ma osłony tarczy ognistej).

Ale:

* Ania ma 'szerokokątny ognisty stożek'
* Gniazdo Szerszeni ma 'sporo tam szerszeni', 'są naprawdę wściekłe', 'stałe pole antymagiczne'

Nawet, jeśli 'stałe pole antymagiczne' zniweluje 'ognisty stożek', to szerszenie najpewniej już zostały spalone.

Jak zatem ja rozwiązuję te problemy?

**Na korzyść Gracza.**

1. Jeśli mam wątpliwości, czy aspekt coś Kontruje czy nie, zakładam, że nie ;-).
2. Jeśli argumentacja Gracza ma sens, pozwalam mu skreślić aspekty po swojemu.
3. Pamiętam, że będą inne konflikty ;-).

Zalety takiego rozwiązania:

* Mniej kłócenia się przy stole "ALE JAK TO!". Gracze szczęśliwsi -> lepsza Opowieść.
* Akcja jest szybsza i bardziej dynamiczna. Mniej kłócenia, więcej akcji.
* Promuję kombinowanie ze strony Graczy. Gra taktyczna, działają szare komórki...

Kluczem jest to, by z perspektywy fikcji wynik mechaniki miał sens.

Ewentualnie wyłącz Aspekty Kontrujące. Też zadziała. 

### 4.6. Obliczanie faktycznych poziomów stron w konflikcie

Dość proste:

* Do Poziomu Postaci (3.1.1) dodaj +2 za każdy Aspekt, który jest korzystny i nie wyparował podczas skreślania Aspektów.
* Do Poziomu Przeciwnika (3.1.2) dodaj +2 za każdy Aspekt, który jest niekorzystny i nie wyparował podczas skreślania Aspektów.

Skończone.

### 4.7. Rzut kością

Zawsze rzuca Gracz uczestniczący w konflikcie; nie ma rzutów wykonywanych przez MG.

Rzut 1k20 przekłada się na modyfikatory zgodnie z poniższą tabelką:

|  **Wynik na kości**   | **Modyfikator**   |
| 20                    | +3                |
| 19, 18, 17, 16        | +2                |
| 15, 14, 13, 12, 11    | +1                |
| 10, 09, 08, 07, 06    | -1                |
| 05, 04, 03, 02        | -2                |
| 01                    | -3                |

Modyfikator dodajemy do poziomu postaci.

### 4.8. Kto wygrał?

W zależności od liczb:

Jeśli poziom postaci jest **wyższy** od poziomu przeciwnika o 2 lub więcej, mamy **Pełny Sukces**.
Jeśli poziom postaci jest **równy lub odległy o 1** od poziomu przeciwnika, mamy **Skonfliktowany Sukces**.
Jeśli poziom postaci jest **niższy** od poziomu przeciwnika o 2 lub więcej, mamy **Pełną Porażkę**.

Przykłady:

Poziom postaci: 5, poziom przeciwnika: 1, 2, 3 -> Pełny Sukces
Poziom postaci: 5, poziom przeciwnika: 4, 5, 6 -> Skonfliktowany Sukces
Poziom postaci: 5, poziom przeciwnika: 7, 8, 9 -> Pełna Porażka

### 4.9. Sukces, Porażka, Skonfliktowany Sukces?

#### 4.9.1. Sukces

* W skrócie, Gracz dostał to co chciał. Przeciwnik traci 1 punkt Trwałości, jeśli jego dobrobyt był elementem konfliktu. 
* Gracz może opowiedzieć co się stało i w jaki sposób. Wygrał.

#### 4.9.2. Porażka

* W skrócie, MG dostał to co chciał.
* MG może opowiedzieć co się stało i w jaki sposób. Wygrał.
* Jeśli nieznana jest stawka konfliktu, Postać Gracza dostaje Ranę lub MG dostaje Problem do użycia później.
* Gracz dostaje 2 punkty Wpływu.

#### 4.9.3. Skonfliktowany Sukces

* W skrócie, zarówno Gracz jak i MG dostali to, czego chcieli. Wybierz jedno z poniższych:
    * Gracz nie dostał tego, co chciał ani MG nie dostał tego, co chciał. ALE sytuacja posunęła się do przodu.
    * Zarówno Gracz dostał to, co chciał jak i MG dostał to, co chciał.
    * Gracz dostał to, co chciał, ale postać gracza dostaje Ranę lub MG dostaje Problem.
    * Gracz dostał to, co chciał, ale Coś Poszło Nie Tak.
* Gracz może opowiedzieć co się stało i w jaki sposób, acz MG też może coś dopowiedzieć.
* Gracz dostaje 1 punkt Wpływu.

### 4.10. Przerzut?

W tej mechanice Gracz - jeśli mu zależy - **zawsze** wygrywa. Kwestia taka, że MG jest w stanie wygrać wszystko inne niż tą jedną rzecz, na której Graczowi zależy. Dlatego pojawia się mechanizm przerzutów - jeśli Gracz nie akceptuje wyniku rzutu kostką, może rzucić jeszcze raz. I tak do oporu.

Więc, jeśli Gracz nie akceptuje wyniku rzutu kostką:

* Postać dostaje +1 do poziomu (na zachętę)
* Mistrz Gry dostaje +1 Problem i +1 punkt Wpływu
* Gracz rzuca jeszcze raz.

Po co:

* Celem Gracza jest osiągnięcie tego, co zamierza. 
* Celem Mistrza Gry jest wygenerowanie jak największej ilości Wpływu.

## 5. Matematyka wyjaśniająca

### 5.1. Matematyka Postaci

#### 5.1.1. Matematyka Postaci, przy jednej Akcji

* Postać ma zakres [-4, 4] przez swoje parametry
* W trzech kategoriach (Motywacja, Umiejętności, Silne i Słabe Strony) postać może dostać aspekt
    * Motywacja i Słabe Strony mogą mieć aspekt negatywny
    * Umiejętności mogą mieć tylko aspekt pozytywny

Teoretycznie, oznacza to że delta postaci to [-8, 10]. W praktyce, spodziewałbym się [0, 10].

#### 5.1.2. Matematyka Postaci, przy dwóch Akcjach

Teoretyczna delta postaci to [-8, 10]. Dodajmy do tego Otoczenie, które dodaje [2, 6]:

* +2 za uruchomienie Otoczenia
* +2 za pozytywny aspekt
* +2 za zniwelowanie negatywnego aspektu

Czyli nasza postać ma już zakres wyników [-6, 16]

#### 5.1.3. Matematyka Postaci, przy trzech Akcjach

Magia sprawia, że nasza postać staje się bardzo potężna, bo dokładnie tak jak w wypadku Otoczenia możemy nałożyć na to jeszcze magię z nowym [2, 6]:

* +2 za uruchomienie Magii
* +2 za pozytywny aspekt
* +2 za zniwelowanie negatywnego aspektu

Finalnie postać gracza będąca magiem i wydając maksymalną ilość dostępnych Akcji podczas konfliktu może uzyskać [-4, 22].

### 5.2 Potencjalne wyzwanie

#### 5.2.1. Typowe poziomy wyzwań

Z perspektywy MG poniższe są szczególnie interesujące:

* **"Prosta sprawa"**: Dopasowanie (pełne) i 1 aspekt (umiejętność): Stopień Trudności 4
    * Jest to wartość łatwa do przekroczenia dla postaci graczy. Dobre na start by ich 
* **Kompetentna postać**: Dopasowania (pełne + częściowe) i 2 aspekty (umiejętność + coś): Stopień Trudności 7
    * Jest to wartość typowego sensownego wyzwania dla postaci graczy.
* **"Teraz się wykażę"**: +Otoczenie: Dopasowania (4 pełne) i 3 aspekty: Stopień Trudności 14
    * Jest to wartość wyzwania dla postaci graczy, przy którym chcemy by któryś gracz mógł się wykazać.
* **"Dobra, rozbieramy to"**: +Otoczenie +Magia: Dopasowania (5 pełnych) i 5 aspektów: Stopień Trudności: 20
    * Jest to wartość wyzwania, które ma sprawić, że gracze będą budowali korzystną sytuację dookoła konfliktu.

Innymi słowy, przeciętny interesujący konflikt na sesji powinien być gdzieś pomiędzy [7, 14].

#### 5.2.2. Przykładowe trudne wyzwanie

To sprawia, że jeśli chcemy dać Graczom wyzwanie dostępne typu "projekt" czy "bossfight" już od początku sesji, musimy stworzyć przeciwnika typu:

* Baza Trudna (10)
* Aspektów 5 (10)
* Trwałość: 3
* W sumie: Stopień Trudności 20, Trwałość 3.

Ten typ przeciwnika sprawi, że Gracze będą chcieli pozbyć się np. 3-4 Aspektów negatywnych na stałe, by redukować własne koszty (Trwałość: 3 wymaga trzykrotnego pokonania przeciwnika, Stopień Trudność 20 wymusza na nich ciężkie działania). 

To sprawia, że gracze proaktywnie będą szukać sposobów na zdobycie korzystnej Sytuacji i mają powód zająć się sesją dość proaktywnie.

## 6. Przykłady

### 6.1. Reporterka uciekająca z kopalni

#### 6.1.1. Sytuacja:

[Maria Newa](/rpg/inwazja/opowiesci/karty-postaci/1709-maria-newa.html) jest reporterką. Podczas jednej ze swoich eskapad przekonała jednego z emerytowanych górników, by ten poszedł z nią do nieczynnej już kopalni. Tam jednak jej przewodnik został dotknięty magią i, niestety, lekko mu odbiło. Maria próbowała przed nim uciec po drabinie (ewakuacja awaryjna z szybu). On próbował ją złapać, najpewniej by zabić i zjeść czy coś...

#### 6.1.2. Maria

| .Motywacja. | .Umiejętności. | .Siły, Słabości. | .Magia . | .Otoczenie. | .POSTAĆ. |
|     0       |      +2        |         +1       |    0     |     0       |    3     |

* **Motywacja** nie pasuje: 
    * Co prawda kwestia narażenia życia (automatyczne +2)
    * ...ale Maria to ryzykantka ORAZ to ona wpędziła tego górnika w tarapaty.
    * Nie powinna zostawić go sama, powinna próbować Coś Zrobić.
* **Umiejętności**: wyraźnie jest szybka i porusza się w trudnym terenie.
* **Siły i słabości**: jest szybka i wytrzymała. Częściowo pasuje.

Ogólnie, wartość postaci '3'. Całkiem nieźle.

#### 6.1.3. Przeciwnik

* Podstawowy poziom trudności to Zwykły: +5.
* Emerytowany górnik zna kopalnię jak własną kieszeń. 
    * Aspekt 'Zna kopalnię jak dom', +2
* Niby jest to starszy człowiek, ale wysportowany i wzmocniony manią napędzaną przez magię.
    * Aspekt 'Maniakalny i wysportowany starszy człowiek', +2

Wartość potencjalna przeciwnika '9'. Maria nie wygląda na zwyciężczynię.

#### 6.1.4. Sytuacja

* Łatwiej gonić niż uciekać, zwłaszcza, że Maria musi po drabinie...
    * Aspekt "łatwiej gonić niż uciekać", na korzyść Przeciwnika
* Kopalnia nie jest dobrym miejscem na bieganie - dużo pochylania się, pełzania...
    * Aspekt "kopalnia to trudny teren", na niekorzyść Przeciwnika i Marii
* Maria zorientowała się w zagrożeniu i zaczęła uciekać mając przewagę czasową.
    * Aspekt "przewaga czasowa", na korzyść Marii

#### 6.1.5. Aspekty Marii i Przeciwnika

* Przeciwnik nie ma dodatkowych Aspektów:
    * Aspekt 'Zna kopalnię jak dom' (aspekt Przeciwnika)
    * Aspekt 'Maniakalny i wysportowany starszy człowiek' (aspekt Przeciwnika)
    * Aspekt "łatwiej gonić niż uciekać" (aspekt sytuacyjny)
    * Aspekt "kopalnia to trudny teren dla Marii" (z kopalni)

* Maria uruchamia następujące Aspekty ze swojego Profilu:
    * Aspekt "poruszanie się w trudnym terenie" (Umiejętności)
    * Aspekt "szybka, bystra, wytrzymała" (Siły i Słabości)

* Oprócz tego Maria ma jeszcze nastepujące Aspekty sytuacyjne:
    * Aspekt "kopalnia to trudny teren dla górnika" (z kopalni)
    * Aspekt "przewaga czasowa" (z poprzednich deklaracji)

#### 6.1.6. Rozpatrzenie Aspektów:

Gracz sterujący Marią zauważa, że jeśli jest to *maniakalny* górnik polujący na Marię, to niekoniecznie jasno myśli. Szybko próbuje biec - jak pomoże mu w tym znajomość kopalni? Raczej sobie krzywdę zrobi - Maria kluczy, manewruje, wybiera trudny teren... MG się trochę nie zgadza, ale przyznaje mu rację (rozstrzyga na korzyść Gracza) - w tym momencie górnik nie myśli trzeźwo i nie planuje jak Marię osaczyć - chce ją złapać.

Gracz odetchnął z ulgą - przeciwnik mógłby znaleźć inną drogę / kombinować. A tak - jego maniakalność sprawia, że traci przewagę.

* Przeciwnik:
    * Aspekt 'Zna kopalnię jak dom'
        * Aspekt skontrowany przez maniakalność
    * Aspekt 'Maniakalny i wysportowany starszy człowiek'
    * Aspekt "łatwiej gonić niż uciekać"
    * Aspekt "kopalnia to trudny teren"
* Maria:
    * Aspekt "poruszanie się w trudnym terenie"
    * Aspekt "szybka, bystra, wytrzymała"
    * Aspekt "kopalnia to trudny teren"
    * Aspekt "przewaga czasowa w uciekaniu"

#### 6.1.7. Przeliczenie finalnych poziomów:

* Przeciwnik:
    * Podstawa: 5
    * Aspekty (3): +6
    * Suma: 11
* Maria:
    * Podstawa: 3
    * Aspekty (4): +8
    * Suma: 11

#### 6.1.8. Rzut 1k20 (i wynik)

Kość przekłada się na [-3, 3]. Więc:

* Jeśli Gracz Marii rzuci [16, 20]: 
    * w najgorszym wypadku Maria ma wartość 13 lub więcej a Przeciwnik ma wartość 11. 
    * Maria odniesie więc Pełen Sukces i skutecznie ucieknie nieszczęsnemu górnikowi.
* Jeśli Gracz Marii rzuci [6, 15], Maria ma wartość 10-12 a Przeciwnik 11. 
    * Maria będzie mieć więc Skonfliktowany Sukces. 
    * Maria ucieknie, ale np. straci jakieś dowody, czy górnikowi stanie się krzywda. Lub inni będą chcieli zejść na dół szukać swojego zaginionego kolegi... możliwości jest sporo ;-).
* Jeśli Gracz Marii rzuci [1, 4], Maria ma wartość 9 lub mniej a Przeciwnik - 11.
    * Maria osiągnie Porażkę.
    * Maria nie ucieknie, tylko będzie złapana przez Przeciwnika. Co gorsza, może stać się JESZCZE coś złego...

# 7. Changelog

## 180122

**Baza konfliktu:**

* ma od teraz krok 5: Zwykły (5), Trudny (10), Bardzo Trudny (15). 
* Powód: za wysoka ilość sukcesów graczy przy trudnych testach; nie chcemy wymyślać 9999 aspektów a 3-5 głównych.

**Punkty wpływu:**

* Gracz zawsze może zaokrąglić w górę; jeśli ma '1', może wykupić coś za '2'.
* Powód: brak tej zasady prowadził do zdegenerowanego stylu gry: "jeszcze jeden bezsensowny konflikt" by MGkowi się 24 przekręciło w 26 ;-).

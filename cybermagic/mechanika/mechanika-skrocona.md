---
layout: mechanika
title: Mechanika, wersja skrócona
---

# {{ page.title }}

* [Opis skrócony](short/mechanika-shortened.html)
* [Budowanie Profilu Postaci](short/profile-building-1805.html)
* [Co to jest system intencyjny? (EN)](common/intention_rpg_en.html)
---
layout: mechanika
title: "Efekty Skażenia"
---

# {{ page.title }}

## Problem 

O magii powiedziano, że jest zbyt przewidywalna i nudna. Dodatkowo, magowie nie mają żadnych kosztów związanych z czarowaniem; nie pojawia się nic ryzykownego. Mag mógłby - hipotetycznie - czarować cały czas i nic dziwnego by się nie działo.

## Rozwiązanie

Wprowadzenie Efektów Skażenia: gdy mag rzuca zaklęcie i wyjdzie mu nadspodziewanie dobrze, niech stanie się Coś Dziwnego. Niech to 'coś dziwnego' jest generowane losowo.

To powinno dać następujące efekty:

* Magia jest nieprzewidywalna
    * 'Dobre' rzuty (16+) powodujące Efekty Skażenia sprawiają, że graczowi niekoniecznie opłaca się przerzucić (reroll) wynik
    * Magia powoduje pewną chaotyczność fabularną. Jest koszt związany z częstym czarowaniem
* MG nie musi wymyślać Efektów Skażenia samemu
    * Mniejszy koszt pracy MG; możliwość dyskusji między Graczami i MG na temat wspólnego pożądanego wyniku.
    * Poszerzenie fabuły, często w różnych niespodziewanych kierunkach.

## Przykład

**Osoby uczestniczące**:

* Ania jest Graczem. Jej postacią jest Anette, czarodziejka specjalizująca się w magii leczniczej.
* Basia jest Mistrzem Gry (dalej: MG)

**Sytuacja**: 

Świat: rok 2014, w bliżej nie zidentyfikowanym kraju Europy Zachodniej. Plus magia.

Anette jest w szpitalu; chce wyleczyć Christiana (NPC, kontrolowany przez Basię), który leży w szpitalu po tym, jak wypadł z okna. Najpewniej wypadł przez kontakt z jakimiś siłami paranormalnymi (duch?). A Anette właśnie próbuje dojść do tego, co tu się do licha dzieje i Christian jest jej idealnym świadkiem.

* **Ania**: Ok, czyli nie ma nikogo w okolicy Christiana, prawda? W jakim Christian jest stanie?
* **Basia**: Bardzo złym. Może faktycznie być potrzebna pomoc magii.
* **Ania**: Dobrze, rzucam czar leczniczy. Chcę, by wyzdrowiał na tyle, by nie wzbudzać niczyjego zainteresowania, ale bym mogła go przepytać.
* **Basia**: Oki, rzucaj.
* **Ania**: 17... To jest... +2 i podniesiony Wpływ, prawda?
* **Basia**: Tak. I Efekt Skażenia.
* **Ania**: Szlag...
* **Basia**: Jakieś propozycje? :-)
* **Ania**: Może... hm, może jakaś nowa forma magiczna oparta o magię leczniczą? Jakaś istota?
* **Basia**: Ściągnięcie viciniusa opartego o magię leczniczą, czy jakiejś efemerydy? To byłby nowy wątek. Może mieć ciekawe interakcje z tym co się dzieje...
* **Ania**: To może lepiej nie.
* **Basia**: Może niech to Twoje zaklęcie poza tym co robi wzmocni dodatkowo tamtą siłę paranormalną, z którą masz do czynienia?
* **Ania**: Anette jest magicznym lekarzem; nie wiem, czy sobie poradzi z takim wzmocnieniem. Wolałabym nie...
* **Basia**: Hmm...
* **Ania**: Oki! Niech tymczasowo ludzie nie czują bólu przez to zaklęcie. Przez to część osób nie będzie dobrze zbadanych przez lekarzy i będą mieć problem.
* **Basia**: Efekt technicznie jest poprawny, ale nic nie zmienia fabularnie; Anette nie jest zainteresowana zwykłymi ludźmi, więc raczej jej to nie dotyczy.
* **Ania**: Fakt.
* **Basia**: Dobrze, zatem... mamy falę "cudownych ozdrowień". Christian i grupa innych ludzi nagle niesamowicie szybko doszła do normy. Zainteresuje to lokalnych łowców zjawisk paranormalnych.
* **Ania**: To utrudni mi życie.
* **Basia**: Ale nie aż tak jak alternatywy, nie sądzisz?
* **Ania**: Dobrze, akceptuję.

I w ten sposób zaklęcie Anette zadziałało zbyt skutecznie. Anette skupiła się, by wyleczyć Christiana... i nie tylko Christian praktycznie mógł wstać z łóżka od razu po wizycie Anette, ale mała grupka innych pacjentów szpitala także cudownie ozdrowiało.

Lokalni poszukiwacze UFO jedynie utwierdzili się w swoim przekonaniu - OBCY SĄ WŚRÓD NAS! (i skutecznie utrudniali życie Anette przez resztę sesji...)

## Wyjaśnienie Efektów Skażenia:

Magia nie jest tak przewidywalna i oczywista jak mogłoby się wydawać. Z uwagi na przepływy magii, ogólne wyładowania energii i fakt, że tak naprawdę nikt nie rozumie do końca jak to wszystko działa - magia ma tendencje do tego, że dzieją się rzeczy DZIWNE.

W chwili, w której rzucane jest zaklęcie, patrzymy na wynik rzutu:

* 1-15: Zaklęcie działa tak, jakby działało normalnie (1-5 obniżony Wpływ, 6-15 normalny Wpływ).
* 16-20: Zaklęcie ma wzmocniony Wpływ (wynika z kości) i dodatkowo pojawia się _Efekt Skażenia_.

W dużym skrócie, Efekt Skażenia oznacza, że zaklęcie weszło w rezonans z _czymś_, albo połączyło się z prądem magicznym, albo dopasowało się do Pryzmatu miejsca, albo przekształciło się przez siłę emocjonalną w okolicy... innymi słowy, stało się Coś Dziwnego.

**Oprócz** tego co zaklęcie miało robić, dzieje się też **coś jeszcze**. To **coś jeszcze** powinno pełnić następujące funkcje:

* Będą zmieniały fabułę: 
    * Pojawiają się NOWE wątki lub
    * Poszerzane są ISTNIEJĄCE wątki lub
    * Pojawiają się komplikacje lub ułatwienia dla postaci.
    * W odróżnieniu od: dzieje się coś, co nie ma żadnych konsekwencji / nie jest interesujące.
* Pojawia się ryzyko i chaotyczność magii.
    * Magia sprawia wrażenie 'nie do końca wiem co się stanie'.
    * NIE będą sprawiać, że magowie boją się czarować.
    * Z czarowaniem wiąże się z magią pewien koszt akcji LUB fabularny.
* Nie będą nadmiarowe i "o nie, po cholerę mi to".

Faktyczna implementacja Efektów Skażenia:

* 16+ (podniesiony Wpływ) przy rzuceniu zaklęcia daje Efekt Skażenia.
* Gracz może odrzucić Efekt Skażenia biorąc komplikację fabularną.
* Efekt Skażenia jest czymś _ciekawym_; niekoniecznie szkodliwym czy wrogim celom postaci. 
* Ważne, by Efekt Skażenia otwierał nowy wątek fabularny / utrudniał inny wątek.
* Wynik Efektu Skażenia ustalany jest przez negocjacje między graczem a MG, jak z komplikacją fabularną.
    * Alternatywnie, obie strony mogą ustalić wynik losowy i negocjować implementację tegoż.

## Wyjaśnienie przykładu:

Przeanalizujmy przykład pod kątem powyższych celów i implementacji.

* Ania rzuciła 16+ (dokładniej, 17) podczas rzucania zaklęcia. Pojawia się Efekt Skażenia.
* Ania zaproponowała Efekt: pojawienie się innej istoty opartej o magię leczniczą. Basi (MG) się ten pomysł spodobał, ale Ania odrzuciła pomysł widząc implikacje: dodanie nowego wątku wchodzącego w interakcję z istniejącym wątkiem.
    * Poprawny Efekt Skażenia: dodał nowy wątek, dotknie to postać gracza 
* Basia zaproponowała Efekt: wzmocnienie wrogiej siły paranormalnej. Ania odrzuciła ten pomysł.
    * Poprawny Efekt Skażenia: wzmocnił istniejący wątek, dotknie to postać gracza 
* Ania zaproponowała Efekt: normalni, nie powiązani ludzie będą mieli komplikacje medyczne. Basia odrzuciła pomysł, bo nie dotyka to Anette (postaci)
    * Błędny Efekt Skażenia: w żaden sposób nie dotyka postaci gracza
* Basia zaproponowała Efekt: cudowne ozdrowienie wzbudza zainteresowanie miłośników paranormaliów. Ania się zgodziła.
    * Poprawny Efekt: dodał nowy wątek, dotknie to postać gracza 

## Tabelka losowa Efektów Skażenia

Jeżeli nie macie pomysłu na Efekty Skażenia, poniżej przykładowe wymyślone Efekty:

| .L.p. | .Efekt.                     | .Wyjaśnienie i opis. |
|   1   | Paradoks                    | To zaklęcie, jakkolwiek wzmocnione, ma INNĄ intencję niż zamierzone. Może mieć też inny cel.                                |
|   2   | Kopia, inny cel             | To samo zaklęcie jest rzucone na inny cel, zachowując intencję. Cel niekoniecznie jest w polu widzenia.                     |
|   3   | Portal Illuminati           | Skażenie otwiera Portal Illuminati, z którego wychodzi jedna (lub kilka) istota. Niekoniecznie blisko.                      |
|   4   | Skażenie CZEGOŚ             | Miejsce, osoba bądź rośliny ulegają Skażeniu. Skażenie będzie wymagało uwagi postaci lub wyjdzie spod kontroli              |
|   5   | Interakcja z Pryzmatem      | Energia magiczna w jakiś sposób wzmacnia, osłabia lub uruchamia energię Pryzmatu danego miejsca. Najpewniej nowy wątek.     |
|   6   | Interakcja z Maskaradą      | Energia magiczna w jakiś sposób wpłynęła na ludzi, srebro lub Coś Co Jest Widowiskowe. Potencjalny problem z Maskaradą      |
|   7   | Efemeryda lub smakołyk      | Albo sformowana jest efemeryda powiązana ze szkołą tego zaklęcia, albo energia generuje smakołyk viciniusa... przynętę.     |
|   8   | Rezonans z czarami          | Efekt jest dziwnym wynikiem połączenia rzucanego czaru z INNYMI czarami rzuconymi... gdzieś w okolicy.                      |
|   9   | Postrzeganie magiczne       | Czy to magiczna mgła utrudniająca widzenie magiczne, czy szczególna czystość widzenia... JAKOŚ zmienia się 'szósty zmysł'   |
|  10   | Echo emocjonalnego czaru    | Efekt wywołał echo INNEGO zaklęcia, które kiedyś było w okolicy rzucone, wśród silnych emocji itp.                          |
|  11   | Magi-emocjonalny wątek      | Pojawia się nowy wątek; najpewniej odkrycie jakiejś tajemnicy z przeszłości, coś powiązanego z magią/ emocjami. Ujawnienie? |
|  12   | Półstabilna konstrukcja     | Powstaje nowy Zasób. Czy to artefakt czy jakaś dziwna forma Skażenia - GDZIEŚ pojawia się cenny dla KOGOŚ Zasób.            |
|  13   | Ta sama intencja, inny czar | Zaklęcie ma tą samą intencję, ale realizowane jest w zupełnie, inny sposób. Można zmienić szkoły itp. Interakcja z innymi?  |
|  14   | Zadziwiające zaklęcie       | Rzucone jest zaklęcie zadziwiające i niesamowite z perspektywy normalnego czarowania. Dzieje się coś BARDZO dziwnego.       |
|  15   | One step too far...         | Zaklęcie poza tym, że robi to co powinno... robi też więcej. Niekoniecznie na ten sam cel i niekoniecznie ta sama szkoła    |
|  16   | Kanał quarkodajny           | Otworzył się pływ czy węzeł czy coś... pojawia się GDZIEŚ możliwość krótkotrwałego pozyskania sporej ilości Quarków.        |
|  17   | Tymczasowa zmiana Otoczenia | Efekt Skażenia przekształca bliskie Otoczenie rzuconego zaklęcia. Nowe własności / ryzyka / korzyści / aspekty?             |
|  18   | Wsparcie dla Innej Strony   | Efekt Skażenia doprowadził do tego, że jedna z Innych Stron (Frakcji) dostaje możliwość uzyskania korzyści.                 |
|  19   | Ujawnienie Dziwnej Wizji    | Efekt Skażenia prowadzi do manifestacji wiedzy / wizji czegoś w Zupełnie Innym Miejscu. Prawda lub fałsz. Komu i co pokaże? |
|  20   | Soczewka                    | To zaklęcie jest wzmocnione i ma TĄ SAMĄ intencję jak zamierzone. Po prostu działa dużo lepiej.                             |

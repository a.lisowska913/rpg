---
layout: mechanika
title: "Mechanika magii"
---

# {{ page.title }}

## Cytat

_"Wiesz, jak zabić maga, Kasiu? Nie martw się, że jest potężny, ma zbroję... nie tak. Po prostu wrzuć go do rwącej rzeki. Jak się nie będzie potrafił skoncentrować, umrze jak każdy śmiertelnik."_ - anonimowy łowca magów.

## Podstawowe fakty (do wykorzystania na sesji szybko)

1. Wpływ konfliktu gdy użyta jest magia jest zawsze traktowany jak podniesiony o jeden poziom (Mały -> Typowy, Typowy -> Duży, Duży -> Heroiczny)
2. Magię dzielimy na dynamiczną, statyczną, rytualną oraz spontaniczną
3. Magia dynamiczna to zaklęcia generowane na bieżące zgodnie z potrzebami czy pragnieniami maga. Jest podzielona na cztery kategorie w zależności od czasu i poziomu koncentracji: 
    1. **infuzja energii**: ~1 sekunda, wymaga słowa i gestu. Wpływ magii = Mały.
    2. **zwykłe zaklęcie**: ~5 sekund, wymaga wypowiedzenia słów i wykonania gestów. Wpływ magii = Mały lub Typowy.
    3. **skupiona inkantacja**, ~30 sekund, wymaga już bardzo skupionego "wierszyka" i swobody ruchów / gestów. Wpływ magii = Mały, Typowy lub Duży.
4. Magia statyczna to sztywne, niezmienne zaklęcia; +1 dodatkowy bonus do bonusu magicznego (oraz rzeczy normalnie niemożliwe dla maga)
5. Magia rytualna to magia specyficzna dla miejsca i sytuacji, wymaga komponentów, dopasowania do Pryzmatu i historii miejsca...
6. Magia spontaniczna to magia protomagów lub samoczarująca się. Magia pochodząca z silnych emocji.
7. Magia krwi to magia specjalna; 
8. Czarowanie bez pełnej koncentracji to bardzo skomplikowany problem (utrudnienie -3 ponad wszystkie inne)
9. Utrzymywanie zaklęć:
    1. Każdy mag potrafi naraz kontrolować i koncentrować się na 1 zaklęciu (nie utrwalonym Quarkami) bez utrudnień.
    2. Każde kolejne powiązane jest z utrudnieniem -1 do wszystkich testów wymagających normalnie koncentracji i skupienia.
    3. Ustabilizowanie zaklęcia używając kryształów Quark (surowca) działa do końca misji.
10. Pryzmat miejsca wpływa na bonus magiczny w zakresie utrudnienia [-3, 3]

## Mechanika z wyjaśnieniem

### 1. Użycie magii a Wpływ konfliktu

Magia to bardzo potężna siła. Niezauważone przedostanie się na drugą stronę rwącej rzeki byłoby najczęściej Wpływem Dużym - wymaga to cichego i sprawnego płynięcia pod wodą, czyli wydatku ogromnej ilości energii. Czarodziej potrafi się po prostu niezauważenie teleportować - byłby to Wpływ Typowy przy użyciu magii.

Mniej więcej w taki sposób magia się skaluje na Wpływ - użycie magii umożliwia robienie rzeczy, które wymagałyby dużo większego zużycia energii, czasu i wysiłku bez magii. 

Proponuję to uwzględniać w Waszych deklaracjach akcji i w tym jak rozpatrujecie na jakim poziomie Wpływu jest dana akcja. Chcecie zniszczyć wapienną kolumnę młotem? Najpewniej Wpływ Typowy. Dodacie energię magiczną do uderzenia? Wystarczy Wam Wpływ Mały, czyli użycie Energii a nie normalnego Zaklęcia (o tym potem).

### 2. Ogólnie, o magii i typach magii

Nie wnikając w umiejętności wrodzone wynikające ze Skażenia i tkanki magicznej, magię - jako sztukę manipulacji energią i konstruktami mentalnymi - można podzielić na trzy najbardziej podstawowe typy: magię _statyczną_ (niezmienną), _dynamiczną_ (wykorzystywaną w zależności od sytuacji), _rytualną_ (bardzo złożoną, wymagającą odpowiednich warunków) i _spontaniczną_ (działającą pod wpływem bardzo silnych emocji oraz dominującego Paradygmatu; też: Magia Krwi).

W największym skrócie, magia polega na dwóch podstawowych komponentach:

* Musi istnieć energia, którą można wykorzystać
* Musi istnieć wola, która będzie tą energię kierować pod kątem celu
* Musi istnieć przewodnik łączący wolę z energią

W najbardziej typowych sytuacjach mamy maga - istotę składającej się częściowo z tkanki "ludzkiej" i częściowo z tkanki "magicznej", naenergetyzowanej i zmutowanej mieszanki tkanki ludzkiej z receptorami energii magicznej. Gdy mag próbuje rzucić zaklęcie, pojawia się wola. Mag chce coś osiągnąć. Ta wola łączy się z jego tkanką magiczną - przewodnikiem - i uruchamia pewne rezerwy energii w ciele maga. Ta energia pobudza energię w otoczeniu maga i umożliwia rzucenie zaklęcia.

Gdy człowiek próbuje "rzucić zaklęcie", nie jest w stanie. Co prawda istnieje wola i istnieje energia w otoczeniu, ale nie ma możliwości "skomunikowania" tej woli z tą energią. Bez choć odrobiny tkanki magicznej nie ma możliwości przesunięcia energii magicznej - zaklęcie nie powstanie.

Innymi słowy, tkanka magiczna i poziom "nieludzkości" maga jest tym, co umożliwia magowi połączyć swoją wolę z energią w otoczeniu oraz na magazynowanie / wytwarzanie  energii magicznej. 

### 3. Magia dynamiczna

#### Opis

* Energia pochodzi z otoczenia i od maga
* Wola pochodzi od maga i trochę z okolicznego Pryzmatu
* Przewodnik to ciało maga

Najbardziej typowa i popularna forma czarowania. 

Mag chce, by coś się stało. W związku z tym się koncentruje, wykonuje odpowiednie słowa i gesty i jego wola staje się rzeczywistością. Niewielkie pokłady energii w ciele maga zostają uwolnione, jego tkanka magiczna przesyła tą energię w otoczenie i wola maga wykorzystuje "energię maga" by wykorzystując rezonans przekształcić część energii w otoczeniu w efekt, którego mag sobie życzy.

Magia dynamiczna może zostać wykorzystana na jeden z trzech sposobów, w zależności od ilości czasu, poziomu koncentracji i tego ile energii mag chce włożyć:

* **Infuzja energii**: Wymaga tak naprawdę nie więcej niż jednego słowa i gestu. Nasycenie części swojego ciała energią magiczną lub krótkozasięgowa emisja energii magicznej by zwiększyć moc swojej akcji. Dużo słabsze niż typowe zaklęcie, acz z perspektywy ludzkiej jest to nadal coś nieosiągalnego.
* **Zwykłe zaklęcie**: Mniej więcej (raczej więcej) pięć sekund koncentracji, ze słowami i gestami. Próba akumulacji energii magicznej i rzucenie zaklęcia mającego dać jakiś efekt. Najbardziej typowa forma czarowania w tym świecie.
* **Skupiona inkantacja**: Czasem konstrukcja mentalna w głowie wymaga czegoś więcej niż pięciu sekund. Czasem potrzebna jest akumulacja większej ilości energii, zbudowanie bardziej dokładnej i złożonej struktury mentalnej. Osiągnięcie czegoś silniejszego, czego nie da się uzyskać w pięć sekund. I to jest to - skupiona inkantacja. 30 sekund, głośne słowa i odpowiednie możliwości złożenia właściwych gestów. Najpotężniejsza forma magii dynamicznej, wymagająca też pełni koncentracji.

#### Mechanicznie

* Każda postać maga włada magią dynamiczną ściśle powiązaną ze swoimi umiejętnościami i specjalizacjami.
    * Postać nie ma możliwości rzucenia zaklęcia dynamicznego w obszarze nie uzasadnionym przez umiejętności i specjalizacje.
* Użycie magii dynamicznej wiąże się z bonusem +1 w kwestii przewag magicznych.
* Wpływ magii jest logicznie wyższy niż Wpływ czynności bez magii
    * Bezszelestne przejście przez obserwowany korytarz to Duży wpływ; ale gdy użyjemy magii (niewidzialność), jest to wpływ Typowy.
    * Należy to uwzględnić podczas ustalania stopni trudności konfliktów
* Efekt infuzji energii to _magiczny_ wpływ Mały
* Efekt zwykłego zaklęcia to _magiczny_ wpływ Typowy lub Mały
* Efekt skupionej inkantacji to _magiczny_ wpływ Duży lub Typowy lub Mały

### 4. Magia statyczna

#### Opis

* Energia pochodzi z otoczenia i od maga
* Wola pochodzi od maga i z roty samego statycznego zaklęcia
* Przewodnik to ciało maga

Zaklęcia statyczne są stałymi, identycznymi zaklęciami. Mikrorytuały, zakładki w przeglądarce... coś, czego mag nauczył się na pamięć by móc bardziej efektywnie rzucić konkretne zaklęcie.

Magia statyczna może przekraczać naturalne umiejętności maga, bo to w pewien sposób "korzystanie z narzędzi" - korzystanie z pewnych mentalnych struktur które ktoś wypracował. Przez wielokrotne powtarzanie tych samych form skupienia, tych samych myśli, wierszyka i gestów - mag potrafi rzucić czar, choć może nie rozumieć, dlaczego to działa.

Gesty, słowa a nawet myśli zaklęcia statycznego są usztywnione. Mag nie jest w stanie niczego tam zmienić. Rzuci zaklęcie identycznie - albo go nie rzuci. Nie jest w stanie wiele w tym zmienić.

Sposób wykorzystania energii działa tak samo jak w wypadku magii dynamicznej, acz wola jest w magu samobudowana przez użycie odpowiednich słów, gestów itp.

#### Mechanicznie

* Każda postać maga może mieć na karcie postaci listę zaklęć statycznych
* Zaklęcie statyczne może przekraczać umiejętności i specjalizacje postaci; postać nie wie JAK to robi, po prostu to robi
* Zaklęcie statyczne jest typu _zwykłe zaklęcie_ lub _skupiona inkantacja_. Nie występuje w formie _infuzji energii_.
* Użycie zaklęcia statycznego daje dodatkowy bonus +1 w kwestii przewag magicznych
* Wpływ magii jest logicznie wyższy niż Wpływ czynności bez magii
    * Bezszelestne przejście przez obserwowany korytarz to Duży wpływ; ale gdy użyjemy magii (niewidzialność), jest to wpływ Typowy.
    * Należy to uwzględnić podczas ustalania stopni trudności konfliktów
    * Szczegóły: patrz magia dynamiczna, infuzji/zaklęcia/inkantacja

### 5. Magia rytualna

#### Opis

* Energia pochodzi z otoczenia; może od maga
* Wola pochodzi z samego rytuału; okoliczny Pryzmat wzmocniony jest rytuałem 
* Przewodnikiem jest sam rytuał, Pryzmat miejsca/sytuacji

Z niektórymi miejscami powiązana jest pewna historia budująca Pryzmat tych miejsc - np. kościoły są miejscami kojarzonymi z czystością i puryfikacją. Z niektórymi grupami czynności, przygotowań, gestów i innych elementów "rytuałów" też są powiązane pewne historie tworzące Pryzmat w danej kulturze / otoczeniu.

To powoduje, że istnieje możliwość wykorzystania tych "odbić powtarzających się myśli w Pryzmacie" jako przewodnik i wolę z perspektywy magii. Coś takiego nazywa się magią rytualną.

W największym skrócie: mag pragnący wykorzystać czy stworzyć odpowiedni rytuał musi wpierw zrobić badania na temat miejsca i/lub rytuałów, a następnie spróbować go powtórzyć w maksymalnym stopniu, dostosowując go do sytuacji. Przygotowanie takiego rytuału trwa od 3h do kilkunastu dni i wymaga spełnienia pewnych wymagań: od zebrania grupy komponentów i miejsca (np. zdobycie kilku przedmiotów kojarzących się z czystością i wykorzystanie kościoła) aż do przygotowania grupy magów, choreografii, rzadkich komponentów... wszystko zależy od oczekiwanego celu.

Magia rytualna jest dość specyficzną formą magii, bo jest jednym z dwóch sposobów (drugim jest Magia Krwi) w jaki mag jest w stanie rzucić dynamiczne zaklęcie co do którego nie ma ani umiejętności ani specjalizacji. Jeśli mag zna "nazwany" rytuał, jest to zaklęcie statyczne i należy go rozpatrywać w tamtej kategorii - na prawach _skupionej inkantacji_.

Część rytuałów może być wykonana też przez ludzi. W tym momencie działa to jak zaklęcia dynamiczne z perspektywy poziomu mocy, ale koszt czasu i pracy jest taki jak w wypadku normalnych rytuałów.

#### Mechanicznie

* Jak każda magia, daje dodatkowy bonus +1 w kwestii przewag magicznych
* W wypadku, gdy mag posiada ścieżki umożliwiające mu na wykonanie efektu rytuału:
    * Wpływ rytuału o 1 większy niż magii dynamicznej. (w tym wypadku Wpływ ponad "Duży" to "Wpływ Strategiczny" - czar o mocy Strategicznej)
* W wypadku, gdy mag nie posiada ścieżek umożliwiających mu na wykonanie efektu rytuału:
    * Patrz: magia dynamiczna, acz z kosztami czasowymi i surowcowymi magii rytualnej. W czym: koszt rytuału obniżony o 1 poziom wobec oczekiwań.
* W wypadku, gdy człowiek lub istota bez kanałów magicznych próbuje wykonać rytuał:
    * Patrz: magia dynamiczna, acz z kosztami czasowymi i surowcowymi magii rytualnej
* Rytuał wymaga jednego Surowca na kategorię rytuału.
    * Rytuał o magicznym Wpływie Małym wymaga 1 surowca i 2-3 godzin. Raczej typowe materiały, 1 osoba.
    * Rytuał o magicznym Wpływie Typowym wymaga 2 surowców i ~1 dnia. Raczej nietypowe materiały, do 5 osób.
    * Rytuał o magicznym Wpływie Dużym wymaga 3 surowców i około tygodnia. Rzadkie materiały oraz więcej niż 5 osób
* Rytuał wymaga powodzenia dwóch dodatkowych konfliktów: zaczynającego "badawczego" (KNO), kończącego "wykonawczego" (SPN)
    * Utrudnienie testów: Trudne dla Małego (1), Bardzo Trudne dla Typowego (2), Zaskakujące dla Dużego (3)

### 6. Magia spontaniczna

#### Opis

* Energia pochodzi z otoczenia; może od maga
* Wola pochodzi z silnie skupionych i skrajnych emocji zbierających energię magiczną
* Przewodnik to skrajnie silne emocje

Zaklęcia spontaniczne są czarami, których nikt tak naprawdę nie "rzucił". Są to czary, które "same się rzuciły" pod wpływem bardzo silnych emocji. Najczęściej jest to domena protomagów, ale zaklęcia spontaniczne mogą się zamanifestować np. na koncertach. Jedyne co jest potrzebne do powstania zaklęcia spontanicznego to źródło energii (Węzeł, Paradygmat działający na maga) i odpowiednio silne emocje. W tym momencie nie ma precyzyjnej, skutecznej woli - jest tylko siła uczuć i energia magiczna działająca w odpowiedzi na te uczucia. 

Jest to sposób w jaki LUDZIE (nie magowie) są zdolni do rzucania zaklęć; a dokładniej, sposób w jaki zaklęcia przydarzają się ludziom. To albo rytuały albo magia krwi.

#### Mechanicznie

* Jak każda magia, daje dodatkowy bonus +1 w kwestii przewag magicznych
* Jeśli dotyczy maga, sumuje się z jego standardową magią (jest wtedy zwane Paradoksem lub Soczewką)
* Efekt działa JAK Wpływ magiczny Typowy lub Wpływ magiczny Duży; praktycznie nie spotyka się Wpływu magicznego Niskiego w wypadku spontanicznych
* Może wystąpić w wyniku komplikacji fabularnej gdy mag rzuca zaklęcie LUB gdy na 1k20 wypadło '1' lub '20' (wartości skrajne)

### 7. Magia krwi

#### Opis

* Energia pochodzi z krwi ofiary i jest zbierana przez silne emocje ofiary
* Wola pochodzi od defilera (maga krwi)
* Przewodnik to skrajnie silne emocje ofiary

#### Mechanicznie

TODO

### 8. Czarowanie a koncentracja

#### Opis

TODO

#### Mechanicznie

TODO

### 9. Utrzymywanie zaklęć (upkeep)

#### Opis

TODO

#### Mechanicznie

TODO

### 10. Pryzmat a magia

#### Opis

TODO

#### Mechanicznie

TODO

### 11. Czarowanie a Skażenie

#### Opis

TODO

#### Mechanicznie

TODO

### 12. Wpływ emocji na czarowanie

#### Opis

* Energia magiczna zbiera się w miejscach o silniejszym poziomie emocji

TODO

#### Mechanicznie

TODO

## Przykłady

TODO

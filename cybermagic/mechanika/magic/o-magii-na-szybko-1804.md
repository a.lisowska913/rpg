---
layout: mechanika
title: "O magii na szybko"
---

# {{ page.title }}

## 1. O magii i energii magicznej

### 1.1. Energia magiczna - Skażenie rzeczywistości

_Magia jest we wszystkim - w kamieniach, w powietrzu, w wodzie... A czy jest w śledziu? Ha, w śledziu magia jest nawet bardziej._

* **Energia magiczna jest jak światło.** Mamy jakieś światło tła, mamy też poszczególne źródła światła. I jest ten jeden człowiek z latarką...
* **Energia magiczna jest jak woda.** Tworzy prądy magiczne (analogia: prąd w oceanie). Są one zazwyczaj bardzo potężne i zwykle w jakimś stopniu pokrywają się z prądami wód głębinowych czy wodociągami.
* **Energia magiczna jest jak pył.** Osadza się na innych bytach. W odróżnieniu od pyłu, wnika w nie.

![Energia magiczna jest we wszystkim](Materials/1804/1804-energy-everywhere.png)

* **Energia magiczna jest jak masa.** Energia magiczna przyciąga energię magiczną. Im większa energia magiczna, tym mocniej do siebie przyciąga słabszą.
* **Energia magiczna jest przyciągana przez uczucia.** Emocje są podstawowym atraktorem magii. Im silniejsze emocje, tym mocniej przyciągają magię.

![Energia magiczna jest we wszystkim](Materials/1804/1804-energy-attraction.png)

* **Energia magiczna jest jak radioaktywność.** Wnikając w różne byty (czy to organiczne czy nieorganiczne) zmienia ich własności. Jak radioaktywność w "Godzilli".
* **Energia magiczna jest jak wrzątek.** Tak jak wrząca woda wyparowuje, tak energia magiczna która nie osiągnęła masy krytycznej przyciągania z biegiem czasu rozproszy się i zniknie. Ale efekty przez nią wywołane zostaną.
* **Energia magiczna jest jak woda (znowu).** Jeśli czystą energię magiczną zanieczyścimy emocjami strachu, mamy energię magiczną o kolorze strachu.

### 1.2. Skażenie

Wszystkie byty mają jakiś kontakt z energią magiczną i wszystkie ulegają jakimś formom Skażenia.

![Uważaj, ten kot jest groźny - magia odpowiedziała](Materials/1804/1804-dangerous-cat.png)

Na powyższym rysunku kot miał opinię bycia groźnym, więc magia wzmocniła jego pazury, zęby i wpłynęła na jego charakter. Innymi słowy, jako, że odpowiednio duża ilość osób wierzyła, że ten kot jest groźny - ten kot STAŁ się groźny.

Jeśli mamy budynek o którym ogólna opinia jest, że jest niebezpieczny - ten budynek staje się niebezpieczny. Jeśli odpowiednia ilość osób w coś wierzy i jest dostateczna ilość energii magicznej w otoczeniu, po pewnym czasie stanie się to prawdą.

Takie zjawisko - przekształcenie czegoś w sposób nie kontrolowany przez nadmiar energii magicznej - nazywamy Skażeniem.

### 1.3. Pryzmat i kolor magii

_W średniej wielkości firmie konsultingowej, **EvilConslt**, zbliżają się rozmowy oceniające, nazywane pieszczotliwie "falą". Zapanowała atmosfera nieufności i strachu. Kto zostanie, a kogo zwolnią? A kiedy emocje rosną, budzą się demony. Dosłownie..._

![Gdy nadchodzi fala, rośnie przerażenie. I magia, zanieczyszczona strachem, przychodzi...](Materials/1804/1804-energy-attraction-fala.png)

Przeanalizujmy ten przypadek. W EvilConslt zapanowała ogólna atmosfera grozy i paniki. Przyciąga to energię magiczną z okolicy; rezydualną oraz ewentualne pływy. Ale w EvilConslt jest tylko jeden typ emocji, jeden kolor - strach, panika, brak nadziei. To wpływa na samą energię magiczną, zmieniając jej **kolor**. Pokolorowana strachem energia magiczna jeszcze mocniej przyciąga energię magiczną. Dochodzimy do dość wysokiego poziomu energii podstawowej. Potrzebny jest tylko wyzwalacz...

Energia magiczna uformowała **efemerydę strachu** - tymczasowy, niestabilny byt magiczny będący samoistną manifestacją magii. Być może jest to coś niematerialnego, a być może jest to całkiem namacalna istota. Być może powstała z niczego, a może inkarnowała się w jednym z pracowników. I ta efemeryda będzie maksymalizować strach i dążyć do zwiększenia poziomu energii magicznej. Niekoniecznie robi to świadomie - ale to jest w jej naturze.

To, co istotne - magia w okolicy jest "pokolorowana strachem". To sprawia, że zaklęcia kojące i łagodzące działają sprzecznie z dominującą energią magiczną. Zaklęcia przerażania i zadawania cierpienia działają spójnie z dominującą energią magiczną. Czyli te pierwsze są słabsze, te drugie - silniejsze.

Coś takiego nazywamy Pryzmatem. Pryzmat to dominująca aura emocjonalna wpływająca na pole magiczne w okolicy.

Przykład: Gdy budynek się pali i ludzie panikują, łatwiej ogień rozpalić niż zgasić.

![Gdy budynek się pali i ludzie panikują, łatwiej ogień rozpalić niż zgasić](Materials/1804/1804-pryzmat.png)

### 1.4. Węzły

Magia przyciąga magię, ale jednocześnie się rozprasza ("wyparowuje jak wrzątek"). Mamy więc taką sytuację:

![Przyciąganie, rozpraszanie. Siły przyciągające i rozpraszające](Materials/1804/1804-attract-evaporate.png)

Górne, ciemnoróżowe siły to przyciąganie. Dolne, niebieskie to rozpraszanie. Zazwyczaj, siły rozpraszające są silniejsze niż przyciągające - niezależnie od tego jak silne zaklęcie nie zostanie rzucone, prędzej czy później się rozproszy. To jak wanna z wodą, gdy prysznic wlewa mniej wody niż wypływa odpływem:

![Przyciąganie, rozpraszanie na przykładzie wanny](Materials/1804/1804-attract-evaporate-bath.png)

Ale co się stanie, jeśli siły przyciągające przekroczą siły rozpraszające? Jeśli wlejemy "więcej" wody do wanny niż "wyparowuje" i "odpływa"? Powstaje nam byt, który ciągle rośnie. Energia magiczna przyciągająca energię magiczną. I coś takiego nazywa się fachowo **Węzłem energii magicznej**.

Szczęśliwie, nie każdy Węzeł dąży do nieskończoności. Węzły są ograniczone ilością energii magicznej w otoczeniu i tym, że im więcej mocy tym szybciej się ona rozprasza. Tak więc większość Węzłów ma jakiś "stan stabilny" - punkt, powyżej którego Węzły nie rosną.

Czego można się spodziewać po Węźle?

* Najpewniej w okolicy jest efemeryda lub solidne Skażenie
* Najpewniej występuje w miejscu, w którym doszło do silnego efektu emocjonalnego. Lub miejscu historycznym
* Najpewniej są gdzieś magowie kombinujący, jak wykorzystać tą energię ;-)

### 1.5. Magowie na Księżycu

_Dawno temu widziałem gościa, który chciał polecieć na Księżyc balonem. Całkiem nieźle mu szło, ale w pewnym momencie ktoś na ziemi zauważył, że się nie da i udowodnił to innym. Chwilę potem balon zleciał. Szkoda gościa, ale wszyscy wiedzieli jak to się skończy - może poza nim..._

Na Primusie nie ma wielu istot władających magią dynamiczną (tzn. czarujących tak jak magowie). Wynika to z tego, że inne istoty zwyczajnie często _nie myślą tak jak ludzie_. Jako, że ludzie są dominującym gatunkiem inteligentnym na tej planecie, ich zasady rozumienia rzeczywistości są zasadami dominującymi.

"Nie da się polecieć balonem na Księżyc". Oki. Nie da się. Ale jak znajdziemy odpowiednią populację ludzi w to wierzącą, pojawi się możliwość stworzenia "bańki wiary" umożliwiającej balonowi polecieć... właśnie, dokąd?

![Magia jest na Ziemi i w atmosferze. Trochę na Księżycu. Próżnia jest pusta - bez magii.](Materials/1804/1804-void-is-void.png)

Nawet, jakby przekonać całą populację Ziemi, że balon może polecieć na Księżyc, prędzej czy później dostaniemy się w kosmos. A tam jest próżnia. Energia magiczna nie ma się w czym odkładać - to sprawia, że jest jej tam za mało. Nasz balon zwyczajnie nie ma dość energii by się zasilić.

Co więcej, załóżmy, że mag dostanie się na Księżyc metodami bardziej konwencjonalnymi. Co dalej? Energia magiczna przyciągana jest przez silne emocje, więc potrzebna jest masa krytyczna ludzi by zebrać energię Księżyca w coś pozwalającego nam zrobić "magiczny habitat". Żeby zatem móc założyć kolonię na Księżycu, musimy mieć np. 10000 ludzi na Księżycu by móc sprawić, że magia zadziała.

Niefortunne.

## 2. O magach

### 2.1. Kim jest mag

_"Nie wiesz, jak zabić maga, Kasiu? Nie martw się, że jest potężny, ma zbroję... nie tak. Po prostu wrzuć go do rwącej rzeki. Jak się nie będzie potrafił skoncentrować, umrze jak każdy."_ - anonimowy łowca magów.

Mag fundamentalnie nie jest człowiekiem. Mag posiada "tkankę magiczną"; jego ciało jest biologicznie wyspecjalizowane do kontrolowania energii magicznej. Jest to kontrolowana forma Skażenia. To zapewnia magowi pewne przywileje:

* mag jest odporniejszy na Skażenie; jego komórki JUŻ są Skażone
* słaby mag wpływa na rzeczywistość z mocą kilkudziesięciu innych, zdeterminowancyh tą samą myślą ludzi.
* mag jest biologicznie inny. Nie podlega tym samym chorobom, jest odporniejszy, przystojniejszy...
* mag ma szósty zmysł; umiejętność wyczuwania i kontrolowania energii magicznej.

Jednocześnie mag nie jest odporny na własną magię. Czarując, mag przepuszcza przez siebie energię magiczną przez pryzmat swoich emocji i woli. To powoduje, że energia magiczna odkłada się magu jeszcze bardziej niż w innych bytach. Mag przekształca się dalej:

* mag uważający siebie (lub uważany) za anioła może obudzić się z aureolą lub skrzydłami.
* mag uważający siebie (lub uważany) za potwora może pewnego dnia STAĆ się potworem.

Jeśli mag za bardzo oddali się od sposobu myślenia ludzi, przestanie rezonować z energią emocjonalną. Przestanie móc zbierać energię magiczną, bo będzie "zbyt obcy". To sprawia, że większość magów ma charaktery dość skrajne, przejaskrawione - bo to zwiększa ich moc magiczną.

### 2.2. Czym jest czar

_AAA! Pająk!!!_ - bardzo niefortunne słowa wypowiedziane podczas rzucania zaklęcia leczącego...

Czar, czy Zaklęcie składa się z trzech elementów:

* **Uczucie**, które przyciąga energię magiczną
* **Pragnienie (wola)**, które wpływa na zmianę rzeczywistości
* **Źródło energii**, które dostarcza mocy na zmianę rzeczywistości

Czyli mag rzuca zaklęcie czerpiąc energię z jakiegoś źródła, życząc sobie czegoś oraz wykorzystując jako wyzwalacz swoje uczucia. Na razie brzmi to optymistycznie.

![Mag rzuca udany czar, używając Smutku by przyciągnąć Energię i rzucić Czar](Materials/1804/1804-whatis-spell.png)

W powyższym przykładzie, Czesiek Czarodziej zobaczył, że jego kolega jest nieprzytomny (np. za dużo wypił soku z czarnotrufli). Wykorzystał więc okoliczną energię magiczną do rzucenia zaklęcia mającego pomóc nieszczęsnemu koledze. Samo zaklęcie było zasilane przez Smutek i Współczucie. Oczywiście, udało się.

Super, nic nie może pójść nie tak, prawda?

![Mag rzuca ten sam czar co ostatnio, ale przestraszył się pająka. Wyszedł mu Paradoks](Materials/1804/1804-whatis-paradox.png)

W powyższym przykładzie, Czesiek Czarodziej stał przed identyczną sytuacją. Tym razem jednak zauważył pająka. Przestraszył się i jego zaklęcie nie zadziałało prawidłowo - doszło do Paradoksu. To mądre słowo oznacza, że wydarzyło się Coś Dziwnego. Zaklęcie nie zadziałało właściwie.

### 2.3. Szósty zmysł

_Dlaczego nie mogę czarować przez obraz z kamery? To niesprawiedliwe!_ - bunt młodego technomanty

Mag to istota humanoidalna, która dodatkowo potrafi "widzieć", "wyczuć" energię magiczną. Jest to zmysł niekierunkowy, acz działa w krótkim zasięgu; można powiedzieć, że mag ma taki radar krótkiego zasięgu działający jak oczy dookoła głowy.

![Rysunek pokazujący maga jakby miał radar magiczny](Materials/1804/1804-magical-radar.png)

Innymi słowy nawet mag mający zamknięte oczy może wykrywać rzeczy znajdujące się niedaleko niego. A wynika to z tego, że energia magiczna jako Skażenie odkłada się w absolutnie każdej formie materii. To jednak znaczy, że mag nie jest w stanie wyczuć rzeczy znajdujących się za innymi rzeczami, jeśli odpowiednio dużo energii się w nich odłożyło. Czyli prawie zawsze.

To ma interesujące implikacje z perspektywy adresowania zaklęć - na CO możesz rzucić zaklęcie. Ogólnie, czar możesz rzucić tylko na coś, co możesz "wyczuć" szóstym zmysłem. Mag zatem nie rzuci czaru na roślinę znajdującą się w akwarium, bo jej zwyczajnie nie "widzi" swoim szóstym zmysłem. Co zatem może zrobić? Może rzucić zaklęcie koło siebie i wystrzelić je w kierunku na cel na który chce wpłynąć:

![Rysunek pokazujący maga jakby miał radar magiczny](Materials/1804/1804-addressing-effect-spell.png)

Na powyższym rysunku mag po prawej próbuje zamanifestować czar na czarodziejce po lewej. Jednak zwykły deszcz sprawia, że energia nie dochodzi do celu (jak światła podczas mgły - to dobra analogia). Jednak czarodziejka po lewej zamanifestowała czar przy sobie i strzeliła w maga po prawej zwykłym poczciwym ognistym głazem. Jako taki zignorował deszcz i... cóż, **ja** nie chciałbym na tym rysunku być po prawej stronie.

### 2.4. Rody magiczne i ich powstanie

_Dlaczego wszyscy Zajcewowie są tacy wybuchowi?_ - chwilę przed wybuchem magów rodu Zajcew

Magia ma własności Skażenia. Jako, że bardzo silne uczucia objawiają się na etapie dojrzewania, zwykle moc magiczna ujawnia się na etapie nastolatka. Ale - cokolwiek się nie powie - nie można uznać nastolatka za najbardziej stabilną emocjonalnie istotę. A na etapie formowania się maga dochodzi do największej ilości zmian i adaptacji magicznej. Innymi słowy, na początkowym etapie konstytuowania się tkanki magicznej w magu dochodzi do najbardziej radykalnych transformacji owego maga, które trudno potem odwrócić.

![Silnie zmutowany mag uważający się za potwora](Materials/1804/1804-monster.png)

By uniknąć sytuacji takich jak powyższe, że mag uważający się za potwora FAKTYCZNIE stanie się potworem, potężni magowie stworzyli "szablony rozwoju maga". Nazwali to **rodami magicznymi** - Diakon, Weiner, Zajcew, Sowiński, Bankierz i wiele innych. Wszystkie rody magiczne są tak jakby "ścieżką ewolucyjną" młodego maga zgodnie z zasadami Paradygmatu - dwóch Zajcewów będzie miało dużo cech wspólnych.

Mag rodu nie jest spokrewniony przez krew (DNA), ale jest "spokrewniony" przez cechy fizyczne i psychiczne (Wzór). Właśnie taki wspólny Wzór magów, stworzony i utrzymywany świadomie, nazywamy rodem magicznym.

Magowie rodowi są zwykle potężniejsi od magów nierodowych, ale mają też swoje... specyficzne cechy.

### 2.5. Arildis, Srebro, implikacja biologii maga

TODO

## 3. Tematy szersze

### 3.1. Magia Krwi i Sympatia

TODO

### 3.2. Primus, Daemonica, Esuriit

TODO

### 3.3. Lustra. Ach, te lustra

TODO

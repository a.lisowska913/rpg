---
layout: mechanika
title: "Szkoły magiczne"
---

# {{ page.title }}

## 0. Aktualność dokumentu

* Dokument jest aktualny wobec mechaniki 1801
* Dokument jest wyprodukowany: 180107
* Ostatnia aktualizacja: 180107

## 1. Aktualne szkoły magiczne

### 1.1. Technomancja

Sfera dotycząca narzędzi, technologii, maszyn i informatyki.

### 1.2. Biomancja

Sfera dotycząca roślin, zwierząt, ciała i innych rzeczy "biologicznych".

### 1.3. Materia

Sfera dotycząca "chemii", wody, ziemi, gleby, ogólnie rozumianej "materii".

### 1.4. Mentalna

Sfera dotycząca kontroli umysłów, tworzenia osobowości i ogólnie rozumianych "umysłów".

### 1.5. Transport

Sfera zawierająca teleportację, kinezę, logistykę wszelaką.

### 1.6. Elementalna

Sfera skupiająca się na podstawowych żywiołach: Ziemi, Ogniu, Wodzie i Powietrzu. Jak w Kapitanie Planecie.

### 1.7. Zmysły

Sfera skupiająca się na iluzjach i sygnałach. Wzrok, słuch, węch, nawet dotyk. Dodatkowo broń soniczna i lasery ;-).

### 1.8. Kataliza

Metamagia. Sfera modyfikująca pływy energii magicznej, magię, zaklęcia, Węzły itp.

## 2. Soczewki i Paradoksy (Efekty Skażenia)

### 2.1. Kiedy się pojawiają?

Podczas rzucania zaklęcia, 16-20 / 1k20.

### 2.2. Efekt

POZA rzuceniem normalnego zaklęcia, dzieje się coś jeszcze. Jeśli gracz osiągnął to, co chciał, to jest to Soczewka - gratulacje. Jeśli gracz ma więcej problemów i nie dostał tego, na czym mu zależało, jest to Paradoks i +2 punkty Wpływu dla Gracza.

### 2.3. Generator Losowego Zaklęcia:

Jeśli nie masz pomysłu na Soczewkę / Paradoks, rzuć 2k20. Jedną za Szkołę, drugą za Efekt. Potem uzasadnij w fikcji co tu się stało i czemu:

| **1k20** | **Szkoła magiczna**     | **1k20** | **Co się dzieje?**           |
|    1     | Technomancja            |    1     | Tworzenie czegoś             |
|    2     | Biomancja               |    2     | Wzmocnienie czegoś           |
|    3     | Materia                 |    3     | Przesunięcie czegoś          |
|    4     | Mentalna                |    4     | Użycie / kontrola czegoś     |
|    5     | Transport               |    5     | Przekształcenie czegoś       |
|    6     | Elementalna             |    6     | Dowiedzenie się czegoś       |
|    7     | Zmysły                  |    7     | Ukrycie wiedzy o czymś       |
|    8     | Kataliza                |    8     | Zniszczenie czegoś           |
|    9     | Technomancja            |    9     | Tworzenie czegoś             |
|   10     | Biomancja               |   10     | Wzmocnienie czegoś           |
|   11     | Materia                 |   11     | Przesunięcie czegoś          |
|   12     | Mentalna                |   12     | Użycie / kontrola czegoś     |
|   13     | Transport               |   13     | Przekształcenie czegoś       |
|   14     | Elementalna             |   14     | Dowiedzenie się czegoś       |
|   15     | Zmysły                  |   15     | Ukrycie wiedzy o czymś       |
|   16     | Kataliza                |   16     | Zniszczenie czegoś           |
|   17     | Techno + Bio            |   17     | dodaj +1 "co się dzieje"     |
|   18     | Mat + Mental            |   18     | dodaj +1 "co się dzieje"     |
|   19     | Transport + Ele         |   19     | wylosuj i + Zwiększenie Skali|
|   20     | Kata + Zmysły           |   20     | wylosuj i + Zwiększenie Skali|

### 2.4. Generator Efektów Skażenia

Jeżeli nie macie pomysłu na Soczewkę / Paradoks, spróbuj to poniżej. Bonus jeśli powiążesz z Generatorem Losowego Zaklęcia.

| **.L.p.** | **.Efekt.**                 | **.Wyjaśnienie i opis.** |
|   1       | Paradoks                    | To zaklęcie, jakkolwiek wzmocnione, ma INNĄ intencję niż zamierzone. Może mieć też inny cel.                                |
|   2       | Kopia, inny cel             | To samo zaklęcie jest rzucone na inny cel, zachowując intencję. Cel niekoniecznie jest w polu widzenia.                     |
|   3       | Portal Illuminati           | Skażenie otwiera Portal Illuminati, z którego wychodzi jedna (lub kilka) istota. Niekoniecznie blisko.                      |
|   4       | Skażenie CZEGOŚ             | Miejsce, osoba bądź rośliny ulegają Skażeniu. Skażenie będzie wymagało uwagi postaci lub wyjdzie spod kontroli              |
|   5       | Interakcja z Pryzmatem      | Energia magiczna w jakiś sposób wzmacnia, osłabia lub uruchamia energię Pryzmatu danego miejsca. Najpewniej nowy wątek.     |
|   6       | Interakcja z Maskaradą      | Energia magiczna w jakiś sposób wpłynęła na ludzi, srebro lub Coś Co Jest Widowiskowe. Potencjalny problem z Maskaradą      |
|   7       | Efemeryda lub smakołyk      | Albo sformowana jest efemeryda powiązana ze szkołą tego zaklęcia, albo energia generuje smakołyk viciniusa... przynętę.     |
|   8       | Rezonans z czarami          | Efekt jest dziwnym wynikiem połączenia rzucanego czaru z INNYMI czarami rzuconymi... gdzieś w okolicy.                      |
|   9       | Postrzeganie magiczne       | Czy to magiczna mgła utrudniająca widzenie magiczne, czy szczególna czystość widzenia... JAKOŚ zmienia się 'szósty zmysł'   |
|  10       | Echo emocjonalnego czaru    | Efekt wywołał echo INNEGO zaklęcia, które kiedyś było w okolicy rzucone, wśród silnych emocji itp.                          |
|  11       | Magi-emocjonalny wątek      | Pojawia się nowy wątek; najpewniej odkrycie jakiejś tajemnicy z przeszłości, coś powiązanego z magią/ emocjami. Ujawnienie? |
|  12       | Półstabilna konstrukcja     | Powstaje nowy Zasób. Czy to artefakt czy jakaś dziwna forma Skażenia - GDZIEŚ pojawia się cenny dla KOGOŚ Zasób.            |
|  13       | Ta sama intencja, inny czar | Zaklęcie ma tą samą intencję, ale realizowane jest w zupełnie, inny sposób. Można zmienić szkoły itp. Interakcja z innymi?  |
|  14       | Zadziwiające zaklęcie       | Rzucone jest zaklęcie zadziwiające i niesamowite z perspektywy normalnego czarowania. Dzieje się coś BARDZO dziwnego.       |
|  15       | One step too far...         | Zaklęcie poza tym, że robi to co powinno... robi też więcej. Niekoniecznie na ten sam cel i niekoniecznie ta sama szkoła    |
|  16       | Kanał quarkodajny           | Otworzył się pływ czy węzeł czy coś... pojawia się GDZIEŚ możliwość krótkotrwałego pozyskania sporej ilości Quarków.        |
|  17       | Tymczasowa zmiana Otoczenia | Efekt Skażenia przekształca bliskie Otoczenie rzuconego zaklęcia. Nowe własności / ryzyka / korzyści / aspekty?             |
|  18       | Wsparcie dla Innej Strony   | Efekt Skażenia doprowadził do tego, że jedna z Innych Stron (Frakcji) dostaje możliwość uzyskania korzyści.                 |
|  19       | Ujawnienie Dziwnej Wizji    | Efekt Skażenia prowadzi do manifestacji wiedzy / wizji czegoś w Zupełnie Innym Miejscu. Prawda lub fałsz. Komu i co pokaże? |
|  20       | Soczewka                    | To zaklęcie jest wzmocnione i ma TĄ SAMĄ intencję jak zamierzone. Po prostu działa dużo lepiej.                             |

---
layout: mechanika
title: "Frakcje"
---

# {{ page.title }}

## Zakres narzędzia

Frakcje są narzędziem wykorzystywanym przez Mistrza Gry (dalej: MG) do zarządzania działaniami, dążeniami i siłami aktywnych stron pojedynczej rozgrywki nie będących postaciami graczy. W skrócie: sił NPCów. 

Przygotowane przed rozgrywką, wykorzystywane podczas przeprowadzania rozgrywki.

## Problem

Mistrz Gry stworzył kontekst rozgrywki. Gracze stworzyli postacie. I... co dalej? 

MG powinien prowadzić rozgrywkę i symulować rzeczywistość, ale bardzo często proaktywne działania Graczy nie spotykają się z innymi proaktywnymi działaniami innych stron świata - jedynie z reakcją na działania postaci graczy. Wynika to z tego, że MG bardzo trudno jest jednocześnie reagować na działania graczy jak i myśleć o dalszych proaktywnych działaniach innych stron.

Innymi słowy, często świat jedynie reaguje na poczynania Graczy. Nie zawiera stron proaktywnych - bo MG często nie ma sił, energii i mocy kognitywnej, by się tym zająć.

Jest to problemem, bo pojawia się percepcja, że świat jest "sztuczny". Istnieje tylko dlatego, że postacie graczy chcą coś zrobić.

## Rozwiązanie

Rozwiązaniem tego problemu są Frakcje. Każda Frakcja jest przedstawieniem proaktywnej strony (w zakresie pojedynczej rozgrywki) mającej jakieś interesy na obszarze, na którym działają postacie graczy. 

Frakcje przygotowywane są przed rozgrywką pod następującymi kątami:

* Koncept - czym ta frakcja podczas rozgrywki jest i do czego mniej więcej dąży
* Motywacje - czym ta frakcja się kieruje i co chce osiągnąć podczas tej rozgrywki?
* Wymagane kroki i Ścieżki - jakie kolejne kroki będzie ta frakcja wykonywała?

Dzięki powyższym, MG jest w stanie zbudować różne strony będące zainteresowane konkretnym wynikiem podczas rozgrywki. W ten sposób działania postaci graczy mogą wspierać, lub wchodzić w szkodę różnym frakcjom. Dzięki mechanizmowi [Ścieżek Fabularnych](sciezki-fabularne.html) (osobny rozdział) możemy określić co konkretnie te frakcje osiągną.

## Przykład

### Kontekst przykładowej frakcji

Poniższa frakcja, "Poszukiwacze Prawdy", jest frakcją wyciągniętą z rozgrywki o nazwie "Paradoksalny Duch", mającej miejsce w systemie Inwazji. Poszukiwacze Prawdy są organizacją interesującą się tematami paranormalnymi i silnie współpracującą ze sobą przy użyciu internetu ;-).

Kontekst "Paradoksalnego Ducha" polega na tym, że grupa strażników tajemnic miasta Senesgrad doprowadziła do dużych kosztów przedsiębiorcy Pawła Madlera; ów wynajął detektywa, który jednocześnie jest Poszukiwaczem Prawdy. Jako, że w miasteczku mają miejsce efekty paranormalne, detektyw będzie próbował dotrzeć do spraw paranormalnych. A jednocześnie jako wynajęty detektyw będzie próbował dotrzeć do prawdy odnośnie kosztów Madlera - czemu ktoś chciałby blokować niedochodową inwestycję? (odpowiedź: bo rozjuszyłoby to duchy).

### Frakcja: "Poszukiwacze Prawdy"

#### Koncept

Pojedynczy Poszukiwacz - wynajęty przez Madlera detektyw imieniem Kornel Kartacz - próbujący dowiedzieć się, co tu się naprawdę dzieje. Z jednej strony - dlaczego na Madlera padło podejrzewanie bankructwa. Z drugiej strony - co tu się DZIEJE, zwłaszcza na linii paranormalnej.

#### Scena zwycięstwa

* Działania Kornela Kartacza nie wzbudziły niczyjego szczególnego zainteresowania
* Kartacz dowiaduje się, że w okolicach Wieży Ciśnień znajduje się silnie magiczna Efemeryda i informacja ta trafia do innych Poszukiwaczy
* Kartacz dowiaduje się, kto stworzył fałszywe pogłoski o bankructwie Madlera i przekazuje tą informację pracodawcy.
* Kartacz niezauważenie wydostaje się z Senesgradu bez żadnych szkód osobistych, materialnych itp.

#### Motywacje

* "Udowodnię moim kolegom na Forum, że tu się dzieje coś więcej. Będę jednym z nich!"
* "Madler płaci, Madler dostanie. Profesjonalne podejście do klienta przede wszystkim."
* "Naprawdę, wierzcie lub nie, Obcy SĄ wśród nas. A my z resztą Poszukiwaczy obserwujemy obserwujących..."
* "Kto ostrożnie zadaje pytania, ten będzie żył troszeczkę dłużej... grunt, to móc się wycofać."

#### Siły

* Kornel Kartacz: Detektyw
* Forum Pięciu Prawd: Internetowe, Anonimowe, Ukryte, Zagraniczne, Rozproszone, Stare, Szerokie badania.
* Sympatycy Postępu Senesgradu: Niewielu, Zdeterminowani, Postępowi, Elita miasteczka.

#### Wymagane kroki do zwycięstwa

* Dotarcie do świadków Efemerydy i innych działań paranormalnych
* Wizja lokalna Wieży Ciśnień, gdy magia jest aktywna
* Zbadanie łańcucha plotek odnośnie Madlera
* Dotarcie do Sonii, Kurta i Klary
* Opuszczenie Senesgradu
* Napisanie raportu na Forum
* Napisanie podsumowania dla Madlera

#### Ścieżki

* **Odkrycie paranormalności**: 3,3,3,3: odkrycie i zebranie dowodów na to, że dzieje się tu coś niezrozumiałego zgodnie z 'normalną fizyką'.
* **Odkrycie spisku przeciw Madlerowi**: 3,3,3: odkrycie i zebranie dowodów na to, że ktoś (i kto) wrobił Madlera. Oraz, że chodziło o Wieżę.
* **Wpada w kłopoty**: 2,3,4: działania detektywa wchodzą w drogę potężnym interesom magów, lokalnej władzy itp. Sukcesywnie może coś... się mu stać.
* **Zapewnienie ewakuacji**: 2,2: zapewnienie sobie wycofania się, zatarcia śladów itp. Wypełnienie tej ścieżki oznacza możliwość wysłania danych lub zwiania.

## Omówienie

### Koncepcyjnie:

#### Wysokopoziomowo

Frakcje mają za zadanie pokazać "co się stanie, jeśli postacie graczy nic nie zrobią". Innymi słowy, pełnią rolę Dark Future. Zapewniają upływ czasu oraz działanie proaktywne stron innych niż Gracze.

#### Elementy Frakcji

##### Koncept

Krótki opis na temat tego czym podczas tej konkretnej rozgrywki jest ta Frakcja. Coś takiego, na co można spojrzeć i od razu zrozumieć o co tu chodzi i co ta Frakcja robi.

##### Scena Zwycięstwa

Bardzo ważny element. Załóżmy, że podczas tej rozgrywki ta Frakcja osiągnęła wszystko, co chciała osiągnąć. Co się stanie? Jak będzie wyglądać rzeczywistość? Dokładne opisanie Sceny Zwycięstwa ("po czym poznam, że wygrałem") umożliwi proste określenie do czego ta Frakcja i agenci tej Frakcji będą dążyć. Ważne: jeśli Frakcja nie ma Sceny Zwycięstwa, najpewniej nie jest Frakcją podczas tej konkretnej rozgrywki.

##### Motywacje

Czym się ta Frakcja kieruje? Co sprawia, że jest proaktywna? Co motywuje tą Frakcję do działania? Warto wylistować 3-4 motywacje. Nie każdy agent będzie miał te same motywacje co jego Frakcja, ale Frakcja ogólnie kieruje się swoimi Motywacjami. Jeśli Frakcja nie posiada Motywacji, jest niedodefiniowana. Bez Motywacji MG nie będzie w stanie określić, jak Frakcja zachowa się w sytuacjach nie do końca dookreślonych.

##### Siły

Frakcja składa się z agentów - z Sił. Kto podczas tej rozgrywki stanowi Siłę tej Frakcji? Kto wykonuje działania prowadzące do Sceny Zwycięstwa? Czym te Siły się charakteryzują? To pozwoli odpowiedzieć na pytania odnośnie _sposobu_ realizowania przez Frakcję swoich celów ("jak to osiągnąć").

##### Wymagane kroki do zwycięstwa

To jest narzędzie pochodne. Analizując Motywacje, Siły i Scenę Zwycięstwa warto spojrzeć na rozwiązywany problem i spróbować zbudować drogę dojścia do Sceny Zwycięstwa używając posiadanych Sił i zgodnie z Motywacjami.

Jeżeli się okaże, że Frakcja nie jest w stanie dojść do swojej Sceny Zwycięstwa, trzeba zmienić Scenę Zwycięstwa. Po prostu ta Frakcja nie ma dość sił i możliwości - musi liczyć siły na zamiary. Może po prostu potrzebuje dużo więcej kroków..? W praktyce, w takiej sytuacji Scena Zwycięstwa jest wizją, marzeniem - trzeba zaprojektować mniejszą, bardziej zgodną z tym, co się da podczas pojedynczej rozgrywki osiągnąć.

##### Ścieżki

Mając wymagane kroki do zwycięstwa możemy je teraz podzielić na poszczególne [Ścieżki Fabularne](sciezki-fabularne.html). Ścieżka ma w sobie wpisane kilka kroków prowadzących do sukcesu oraz zawiera ilość kroków.

Frakcja powinna mieć pomiędzy 3-5 Ścieżkami Fabularnymi. Jeśli będzie mieć ich za mało, wypełnienie Ścieżek będzie zbyt szybkie. Jeśli za dużo, zbyt wolne.

Część Ścieżek może wymagać wypełnienia innych wcześniej (jak [wykres Gantta](https://en.wikipedia.org/wiki/Gantt_chart))

### Mechanicznie:

* Każda akcja Graczy powoduje 1 akcję dla każdej Frakcji, standardowe losowe wypełnienie Ścieżki Fabularnej (patrz: [Ścieżki Fabularne](sciezki-fabularne.html)).
* Osłabienie przez postacie graczy Sił Frakcji powinno wiązać się z tym, że ta Frakcja nie dostanie akcji.
* Upływ czasu powinien zwiększać ilość akcji dla każdej Frakcji
* Często Frakcje są w konflikcie ze sobą; to nie jest "postacie graczy przeciwko światu".

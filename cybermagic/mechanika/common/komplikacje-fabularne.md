---
layout: mechanika
title: "Komplikacje Fabularne"
---

# {{ page.title }}

## Problem 

Wraz z upływem pojedynczej rozgrywki, Gracze zamykają istniejące wątki. To sprawia, że Mistrz Gry musi z jednej strony wymyślać rzeczy, które dzieją się w ramach pojedynczego wątku a z drugiej strony dodawać nowe rzeczy, które się pojawiają. Jeśli Mistrz Gry tego nie robi, bardzo szybko rozgrywka się zatrzyma przez brak celów drugiej strony.

Co gorsza, Gracze są w stanie wygrać wszystko - nie ma niczego, co Gracze mogą przegrać. Mistrz Gry staje się dość pasywnym procesorem akcji postaci Graczy.

## Rozwiązanie

Tak, jak _udane_ konflikty kończą się zamknięciem elementu wątku, tak _przegrane_ konflikty kończą się dodaniem nowej Komplikacji Fabularnej. Komplikacja może pojawić się w wyniku przerzutu, eliminacji Efektu Skażenia czy też upływu czasu.

Dobra Komplikacja Fabularna powinna dodać nowy wątek lub nową rzecz, która powinna być interesująca dla Graczy. W ten sposób Gracze niekoniecznie są w stanie wszystko wygrać - muszą wybrać, na czym im bardziej zależy.

## Przykład

Dokładny przykład pojawia się w [opisie przerzutów](przerzuty-porazki.html).

## Faktyczna implementacja:

* W wypadku porażki testu, Gracz może poprosić o przerzut (reroll) i przyjąć Komplikację Fabularną.
* Komplikacja fabularna jest proponowana przez Gracza / MG lub generowana zgodnie z poniższą tabelką losową.
* Komplikacja fabularna powinna dodać nowy wątek lub poszerzyć istniejący; nie powinna zamykać bądź być czymś nie dotykającym postaci gracza.
    * Komplikacja może wpływać na przyszłą rozgrywkę; nie musi się materializować podczas tej samej rozgrywki.
* Wynik Komplikacji Fabularnej ustalany jest przez negocjacje między graczem a MG.
* Obecność każdej Komplikacji Fabularnej powoduje +1 akcję na ścieżkach fabularnych frakcji

## Tabelka losowa:

Jeżeli nie macie pomysłu na Komplikacje Fabularne, poniżej przykładowe:

| .L.p. | .Efekt.                      | .Wyjaśnienie i opis. |
|   1   | Pojawia się ktoś niepożądany | W tej scenie (lub po niej) pojawia się osoba lub osoby, jakie nam nie pasują. Świadkowie, przedstawiciele innej strony... |
|   2   | Niefart sojusznika           | Komuś przyjaznemu dla nas coś nie wychodzi albo np. zostaje ranny |
|   3   | Zmiana Okoliczności sceny    | Korzystna Okoliczność zanika, lub pojawia się Okoliczność niekorzystna w tej scenie. Trwałe (persystentne). (np. Efemeryda się budzi) |
|   4   | Kontr-akcja przeciwnika      | W wyniku wydarzeń wpływających na Drugą Stronę, ta wykonuje jakieś działanie kontrujące (np. detektyw sprawdzający co się dzieje) |
|   5   | Utrata, uszkodzenie sprzętu  | Element sprzętu przestaje działać lub ulega uszkodzeniu. Lub musimy go poświęcić. Lub dowiadujemy się, że nie działał. |
|   6   | Zainteresowanie Nowej Strony | Nowa Strona interesuje się tym, co się dzieje i wchodzi do akcji. Np. policja. |
|   7   | Łatwiejsze plany przeciwika  | Przeciwnik osiągnął pewien sukces lub dostał nieoczekiwane wsparcie. |
|   8   | Szerokie zainteresowanie     | Coś przyciąga szerokie (masowe, publiczne) zainteresowanie do kogoś/czegoś, które nie jest w tej sytuacji pożądane |
|   9   | Koszty działań rosną         | Coś kosztuje więcej, niż się spodziewano; do osiągnięcia intencji potrzeba czegoś jeszcze. |
|  10   | Pilne wydarzenie             | Pojawia się dość pilne wydarzenie, którym ktoś musi się zająć |
|  11   | Wartościowa dygresja         | Albo ktoś potrzebujący wsparcia albo okazja na pozyskanie czegoś; bardzo ograniczone czasowo i więcej stron może próbować to zdobyć. Okazja. |
|  12   | Nowy Wątek w Motywacji       | Pojawia się nowy Wątek powiązany z Motywacją jednej z postaci. |
|  13   | Zmiana zdania innej strony   | Sojusznik lub osoba wcześniej neutralna znajdują Motywację sprzeczną z działaniami postaci |
|  14   | Wewnętrzny wróg              | Wykonywana właśnie akcja dodatkowo jakoś pomoże Innej Stronie |
|  15   | Wzrost znaczenia grupy X     | Jakaś grupa lub społeczność stała się właśnie istotna z perspektywy działań |
|  16   | Wzrost znaczenia miejsca X   | Jakieś miejsce mające własne Okoliczności stało się właśnie istotne z perspektywy działań |
|  17   | Problemy z czasem            | Ogólnie wszystko działa, ale wymaga więcej czasu / lepszej synchronizacji... i właśnie to nie działa |
|  18   | Specjalistyczne Okoliczności | To, co jest jest niewystarczające. Potrzeba specjalistycznych Okoliczności - sprzętu, wiedzy... czegoś, czego chwilowo nie mamy |
|  19   | Seks, przemoc lub krew       | Seks, przemoc lub krew - problemy personalne między jakimiś osobami. |
|  20   | Wydarzenie na skalę szerszą  | Czy to wielka burza, czy festiwal... spore wydarzenie mające potencjalną interferencję z tym, co się dzieje na misji. |
|  21   | Działania Rywala             | Pojawia się Rywal do tego samego CELU. Lub, Rywal osiąga przewagę. Rywal jest odnośnie celu lub personalnie wobec postaci. |

---
layout: mechanika
title: "Sceny"
---

# {{ page.title }}

## 1. Aktualność dokumentu

* Dokument jest aktualny wobec mechaniki 1805; działa z wszystkimi dotychczasowymi kartami postaci
* Dokument jest wyprodukowany: 180526
* Ostatnia aktualizacja: 180426

## 2. Zakres narzędzia

Sceny są narzędziem wykorzystywanym przez Mistrza Gry (dalej: MG) do zarządzania wydarzeniami podczas samej rozgrywki - nadanie sesji struktury.

## 3. Problem



## 4. Rozwiązanie


## 5. Narzędzie


## Przykład



## Omówienie

### Koncepcyjnie:


### Mechanicznie:


---
layout: cybermagic-przeciwnicy
title: "AK Serenit"
---

## Koncept

Statek pożerający statki. Anomalia Kosmiczna Serenit to RAMA (statek-asteroid) powstały z amalgamacji statków, asteroidy i kralothów po dotknięciu Światła Saitaera. Śmiertelnie niebezpieczny vicinius sterowany przez gestaltyczną świadomość TAI, BIA i magów pożartych przez Serenit wcześniej.

## Mechanika

### Klasa przeciwnika

* Przeciwnik **elitarny**
* Przeciwnik rozproszony 
    * Kłąb Kontrolny 
    * Ścianowęże 
    * nanitki w powietrzu 
    * ludzie: "naturaliści" (kultyści Serenit), "militaryści" (ufortyfikowali się), "niszczyciele" (chcą zniszczyć Serenit), "ucieczkowcy" (chcą zwiać). Ponad 100/grupę.
    * technodorian: cybernetyczny marcadorian o dużej sile ognia, straszliwa siła ognia i drapieżność

### Otoczenie

AK Serenit znajduje się w kosmosie; trudny do wykrycia, krąży w okolicach Anomalii Kolapsu przy Astorii i poluje na różne statki szukając łatwego celu. Potrafi się mimikować i wysyłać sygnały że jest czymś innym niż się wydaje - szczęśliwie, to tylko sygnały elektroniczne. Jak chodzi o wielkość, Serenit jest dużym pojazdem; trudno go zniszczyć używając broni konwencjonalnej i technomagicznej.

### Misja



### Siły

* 

### Słabości

* 

### Zachowania

* 

## Opis

### Jak to wygląda

Struktura: 

* Silniki
* 2* Life Support
* Sensory (zew)
* Living Quarters
* Living Quarters
* Medbay
* Cargo
* Lounge
* Nawigacja
* Mostek
* AI Core
* Fabrykatory

### Ekonomia

Serenit nie ma wykorzystania ekonomicznego. Jest to śmiertelnie niebezpieczna anomalia kosmiczna; omijać.

### Wykorzystanie



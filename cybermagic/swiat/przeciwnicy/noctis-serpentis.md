---
layout: inwazja-vicinius
title: "Noktiański Serpentis"
---

# Przeciwnik

## Opis

Szczyt technologii noktiańskiej. Komandos noktiański, doskonale wyszkolony i bioformowany a następnie implantowany. Serpentis ma zielonoszare ciało, błękitne oczy i nic a nic nie wygląda jak człowiek. Noktianie nie do końca o tym wiedzą, ale stworzyli symbiont na podobieństwo visiat, zastępujący ludzką tkankę i adaptującą się do potrzeb i sytuacji.

Z perspektywy Noctis, serpentisi to coś co nie powinno mieć miejsca. Z perspektywy Astorii, są splugawieniem wszystkiego w co Astoria wierzy.

Biotransformacja daje mu:

* zdecydowanie podniesioną siłę i refleks
* umiejętność przetrwania w niemożliwych warunkach
* ogromne przeciążenia i prędkość poruszania się
* wysoka odporność na magię
* własności visiat sprawiają, że jest w stanie regenerować te "niemożliwe rzeczy".

Implantacja daje mu:

* wymiana kości i ekstremiów na xaristal, z nanitkami zdolnymi do odbudowy struktury
* augment układu nerwowego
* szpony w miejscu ludzkich paznokci
* rozprowadzone w ciele amplifikatory energetyczne - możliwość tymczasowego wzmocnienia kosztem wzmacniacza irialium (irialian overdrive)
* serię regeneracyjnych nanitek
* wspomaganie twinowanego komputera

Jednak to niesie za sobą pewne koszty:

* Żyją na tabletkach. Serio. Serpentis - im mocniej zmieniony - tym bardziej jest niestabilny i nie funkcjonuje poprawnie bez kontroli
* Serpentis rzadko kiedy dożyje 50-tki. Nie ma starych Serpentisów.
* Mają silną alergię na srebro - pochodna visiat.

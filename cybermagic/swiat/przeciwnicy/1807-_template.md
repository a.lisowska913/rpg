---
layout: inwazja-vicinius
title: "Nazwa"
---

# {{ page.title }}

## Mechanika

### Moc vicinusa

**Trwałość** : 1

| Obszar działania wroga    | Konflikt   |
|---------------------------|------------|
| Atak fizyczny (siła)      |  |
| Atak fizyczny (szybkość)  |  |
| Pancerz fizyczny          |  |
| Pancerz mentalny          |  |
| Pancerz społeczny         |  |
| Pancerz magiczny          |  |
| Zręczność                 |  |
| Obserwacja                |  |
| Intelekt                  |  |
| Zasadzka                  |  |

### Koncept

?

### Otoczenie

?

### Misja

?

### Siły

* ?

### Słabości

* ?

### Zachowania

* ?

## Opis

### Jak to wygląda

?

### Ekonomia

?

### Wykorzystanie

?

# Rody Aurum

## 1. Lista rodów z krótkimi danymi

* 1. Arłacz
* 2. Bankierz
* 3. Barandis
* 4. Blakenbauer
* 5. Bulterier
* 6. Burgacz
* 7. Diakon
* 8. Gwozdnik
* 9. Kazitan
* 10. Keksik
* 11. Lemurczak
* 12. Maus
* 13. Myrczek
* 14. Mysiokornik
* 15. Samszar
* 16. Sowiński
* 17. Terienak
* 18. Tessalon
* 19. Torszecki
* 20. Perikas
* 21. Ursus
* 22. Verlen

## 2. Dokładniejszy opis rodów
### 2.11. Lemurczak

* metakultura: różnorodne. 
* modyfikacja kulturowa: "silni pożerają słabych", "nikt Ci nigdy nie pomoże"
* specjalizacja: czarny ród specjalizujący się w niewolnictwie i twistowaniu innych do swojej woli

Przed Pęknięciem w Aurum działały elementy Syndykatu Aureliona (jako mafia). Udało im się sprząc z wierchuszką pewnego terenu. Przywrócili dyskretnie niewolnictwo i doprowadzili do struktury typu "klasa panów" i "plebs". I wiele osób z owego "plebsu" znalazło się między młotem (system prawny, wierchuszka) i kowadłem (mafia, Syndykat).

A sentisieć sprzęgła się z osobami o najsilniejszych uczucia i największy resentyment - w różnorodnych niewolników.

W ten sposób powstał ród Lemurczak - ród zrodzony z resentymentów oraz chęci zemsty.

### 2.15. Samszar


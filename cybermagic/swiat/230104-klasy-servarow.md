# Klasy servarów

## 1. Opis kategorii


## 2. Servary

### 2.1. Lancer - podstawowy średni servar

* Podstawowy servar.
* Posiada własne TAI klasy Elainka.
* 150-300 kg w zależności od konfiguracji
* możliwy do fabrykacji przez standardowe fabrykatory, z efektywnością recyklingu 95%.
    * czas fabrykacji od zera: 3 dni
    * czas konfiguracji pod osobę: 10 min
* hardpointy: 2
* sockety: 3

#### 2.1.1. Lancer przeciwpotworowy, Verlen, gwardzista

* Sockets: [jetpack, ekstra paliwo]
* Hardpoints: [LKM, rusznica ppanc LUB snajperka]

#### 2.1.2. Lancer działania kosmicznego, Orbiter, uniwersalny

* Sockets: [selfseal + lifesupport, thrusters]
* Hardpoints: [plasma torch, działo laserowe, multi-tool]

### 2.2. Scutier - lekki tani servar

* Tani lekki servar, używany często przez jednostki cywilne.
* Możliwy do masowej produkcji przez proste jednostki wsparcia. 
* Możliwy do skonfigurowania jako szybka jednostka zwiadowcza.
* hardpoint: 1
* sockety: 2

### 2.3. Chevalier - średni neurosprzężony servar

Średni servar z silnym neurosprzężeniem, usprawnienie Lancera w każdym calu.
Sześć slotów, sprawiając, że Chevalier jest jednym z najbardziej uniwersalnych servarów.
Posiada własne TAI klasy Maia.
Wymaga reaktora irianium.

### 2.4. Entropik - superciężki servar przełamania

Superciężki servar przełamania.
Wymaga neurosprzężenia.
Wymaga reaktora irianium.

### 2.5. Fulmen - artyleryjski servar

Artyleryjski / snajperski servar; niesamowita siła ognia połączona z zasięgiem.
Nie wymaga neurosprzężenia, acz z neurosprzężeniem działa dużo lepiej.
Posiada własne TAI klasy Maia.

### 2.6. Eidolon - servar koloidowy

Lekki i delikatny, ale niewidoczny.
Niesamowicie kosztowny.
Wymaga neurosprzężenia.
Wymaga reaktora irianium.

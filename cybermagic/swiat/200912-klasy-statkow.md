# Statki kosmiczne - klasyfikacja.

### Łowca-Zabójca / ŁZa (Hunter-Killer) (Hk)

Pojazd zależny, drona. Niewielki.

### Pinasa (Pinnace) (P)

Pojazd kilkuosobowy, zależny od większej jednostki. Zwykle służy do transportu kilku osób między większymi jednostkami czy coś. Przeciętna fregata ma 1-2. Mniejsze niż myśliwce.

### Myśliwiec (Fighter) (Ft)

Pojazd jednoosobowy, zależny od większej jednostki. Służy do walki z innymi jednostkami, zwykle z bliższego zasięgu. Mały profil, co pomaga w walce radioelektronicznej.

* Około 15 metrów długości
* Około 10 ton

### Kuter (Cutter) (Ct)

Jednostki kilkuosobowe. Bardzo mała jednostka zdolna do działania niezależnego.

* Około 25 metrów długości
* Około 20 ton
* Do 5 osób w załodze
* Typowe role
  * patrolowanie 
  * zwiad
  * missile boat

### Korweta (Corvette) (Cv)

Mały statek kosmiczny. Jednostka niezależna, dalekosiężna.

* Do 80m długości, 15 dni działania niezależnego.
* Do 40 osób w załodze.
* Do 2200 ton.
* Typowe role: 
  * rozminowywanie
  * patrolowanie
  * missile boat, strike&flee
  * intelligence gathering, stealth
  * działania szybkiego reagowania; szybki deployment i szybkie działanie
  * zwalczanie rzeczy mniejszych od siebie

### Fregata (Frigate) (Fr)

Najbardziej typowa jednostka. Średniej wielkości jednostka zdolna do rozwiązywania problemów kosmicznych.

"Frigates scouted for the fleet, went on commerce-raiding missions and patrols, and conveyed messages and dignitaries. Usually, frigates would fight in small numbers or singly against other frigates. They would avoid contact with ships-of-the-line; even in the midst of a fleet engagement it was bad etiquette for a ship of the line to fire on an enemy frigate which had not fired first."

* In the 17th century, a frigate was any warship built for speed and maneuverability
* In the 18th century, frigates were full-rigged ships, that is square-rigged on all three masts, they were built for speed and handiness, had a lighter armament than a ship of the line, and were used for patrolling and escort.
* The introduction of the surface-to-air missile after World War II made relatively small ships effective for anti-aircraft warfare: the "guided missile frigate"

Parametry

* Około 150m długości, 30 dni działania niezależnego.
* Około 200-250 osób w załodze
* Około 25k ton
* Typowe role:
  * projekcja siły Orbitera; dalekosiężna jednostka misji wszelakich
    * samodzielna jednostka dalekosiężna; podstawa sił Orbitera
  * czyszczenie anomalii kosmicznych
  * wsparcie większych jednostek; osłona krążowników
  * dalekosiężne działania ukryte (koloidowe warianty)

(modelowane po: 

* https://en.wikipedia.org/wiki/%C3%81lvaro_de_Baz%C3%A1n-class_frigate 
* https://en.wikipedia.org/wiki/Anzac-class_frigate
)

### Niszczyciel (Destroyer) (D)

Fast, maneuverable, long-endurance warship intended to escort larger vessels in a fleet, convoy or battle group and defend them against powerful short range attackers. Stosunkowo lekko opancerzona jednostka o sile ognia zdecydowanie przekraczającej swój tonaż.

Niszczyciele się bardzo różnią między sobą. Mamy ciężkie niszczyciele typu Castigator, który jest potężnym działem z silnikami jak i lżejsze jednostki wspierające artyleryjsko Orbiter.

Parametry

* Około 170m długości
* Około 300 osób załogi
* Około 30k ton
* Typowe role
  * osłona jednostki większej
  * artyleria
  * ogień zaporowy
  * bombardowanie planetarne

(modelowane po:

* https://en.wikipedia.org/wiki/Maya-class_destroyer
* https://gundam.fandom.com/wiki/Archangel-class 
  * Jakkolwiek specka wskazuje na krążownik, ale anime wyraźnie używało Archangel jako... niszczyciela. Z drugiej strony, transportuje 8 Mobile Suit, które się klasyfikują jako Fighter. Więc... ?
)

### Krążownik (Cruiser) (Cr)

Niezależna jednostka do działań dalekosiężnych, kręgosłup potężnej floty. Jednostka zrównoważona. Najwięcej konfiguracji. Zawiera kontyngent desantowy. Zawiera mechanizmy i inżynierię służącą do 

* Około 300 - 400m długości
* Około 450 osób załogi
* Około 400k ton
* Typowe role
  * Samodzielne działanie niezależne

(modelowane po:

* https://memory-alpha.fandom.com/wiki/USS_Enterprise_(NCC-1701) 
  * http://www.waxingmoondesign.com/VesselSpecs.html
)

### Pancernik (Dreadnaught) (Dr)

Silnie opancerzona jednostka, specjalizująca się w neutralizacji ataków, przyjmowaniu ataków. Bardzo trudne do spenetrowanie defensywy i solidna siła ognia.

### Lotniskowiec (Carrier) (Ca)

### Tytan (Titan) (T)

Niezależna jednostka kosmiczna służąca do zachowania cywilizacji / rasy ludzkiej w całości. Największa jednostka znana ludzkości. Zdolna do pozyskiwania cokolwiek jest potrzebne z gwiazd i materii kosmicznej. Ruchoma stacja kosmiczna. Górna granica stężenia ludzi zanim dojdzie do Kolapsu Magicznego.

* Około 1300 m * 400 m * 550 m
  * To cholerstwo jest olbrzymie
* Załoga
  * 20k osób w załodze
  * 300k cywili / niezałogantów
* Około 5 mln ton
* Typowe role
  * Generacyjny statek kolonizacyjny
  * Superforteca, zarządzanie Sektorem lub kilkoma
  * Maszyna masowej zagłady
  * Nomad noktiański - wieczne poszukiwanie domu / wieczne przemieszczanie się

(modelowane po: SDF-1 Macross)

## Materiały wspomagające

* https://migflug.com/jetflights/classifications-of-naval-vessels/
* https://www.eveonline.com/article/the-great-skill-change-of-blessed-2013
* https://en.wikipedia.org/wiki/Naval_ship
* https://en.wikipedia.org/wiki/Corvette 
* https://en.wikipedia.org/wiki/Scimitar-class_patrol_vessel 
* https://memory-alpha.fandom.com/wiki/USS_Enterprise_(NCC-1701)
* http://www.waxingmoondesign.com/VesselSpecs.html
* https://macross.fandom.com/wiki/Macross_Quarter
* http://www.macross2.net/m3/sdfmacross/macross.htm 
* https://gundam.fandom.com/wiki/Archangel-class 

## Aktualizacje

* 200912 - pierwszy prototyp
* 211128 - porządna robota, rozrysowanie
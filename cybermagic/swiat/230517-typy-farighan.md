# Przeciwnicy

## 1. Farighan - whatis

Farighanowie, puste dusze, cyberzombie. Niewolnicy technologii na Neikatis. Wzorowani na Assembly Synthesis.

Jednostki Farighanów:

* Recykler
* Medinżynier

## 2. Konkretne bioformy

### 2.1. KODOWANIE

C (ciało), U (umysł / intelekt / psychotronika), E (emocje / morale / kontrola magiczna), S (społeczne)

### 2.2. Recykler

* .Recykler (konfig: mix)
    * akcje: "shotgun z bliska", "ranię szponem", "harvestuję zwłoki"
    * siły: "kiepskie cyberwspomaganie", "implanty", "szybki i silny"
    * defensywy: "niewrażliwy na ból", "kiepskiej jakości zaimplantowane ciało"
    * słabości: "KIEPSKIE cyberwspomaganie", "zaniedbane ciało i sprzęt"
    * zachowania: "atakuję frontalnie", "rozcinam i rozdzieram", "furia narkoberserkera"

Opis: najbardziej typowy farighan, dość mieszana jednostka; ciało połączone z mechanizmami, nic nie działa perfekcyjnie. 

### 2.3. Inżynier

* .Medinżynier
    * akcje: 
    * siły: 
    * defensywy: 
    * słabości: 
    * zachowania:

Opis: 


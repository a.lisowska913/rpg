## 1. Atarien
### 1.1. Notka

* Jak o nich mówimy: atarienin, atarienka, atarieni
* Klasa: skrajni próżniowcy, biopuryści, indywidualiści-klanowcy, hierarchiczni militaryści
* Występowanie: Orbiter
* Pochodzenie: sektor Atarien -> Orbiter (przez Kontroler Pierwszy)

### 1.2. Omówienie ogólnie

Atarieni pochodzą z sektora Atarien. W swoich oryginalnych warunkach przed Pęknięciem mieli do czynienia z hiperkapitalistycznym systemem w ciągłym niedoborze, co sprawiło, że musieli skrajnie rywalizować i walczyć o swoje oraz budować tymczasowe sojusze celowe. Nie zniszczyło to jednak ich ducha, jedynie zahartowało jeszcze mocniej.

Pęknięcie Rzeczywistości zostawiło ich po TEJ stronie sektora Astoriańskiego. Weszli w sojusz z planetą Astorią i częściowo z Neikatis przeciwko Noktianom. Stanowią zdecydowaną większość oryginalnego Orbitera, acz z biegiem czasu zaczęło się to mieszać. Orbiter skupia swoje siły też pod kątem Security.

### 1.3. Gdzie ich spotkamy

Przede wszystkim Orbiter, ale niemało Atarienów próbuje szczęścia też na Astorii czy Neikatis. A nawet z uwagi na swoją naturę - odległe miejsca jak Pas Kazimierza. Ale ogólnie jako grupy - Orbiter lub Astoria.

### 1.4. Fiszka

1. Metapsychology (tenets and values of psyche, art, culture)
    1. Achievement, Power -> Indomitable human spirit and ingenuity.
    2. The strongest win -> wieczna rywalizacja i na linii fizycznej, technicznej, politycznej.
    3. Biopuryzm -> ludzie to ludzie. Patrzymy z góry na zmodyfikowane istoty.
    4. Ekspansja, ruch, odwracalność -> stanie w jednym miejscu to śmierć. Akceptujemy eksperymenty i ryzyko, póki odwracalne.
2. Symbolism and Aesthetics
    1. Minimalistyczna sztuka, raczej w formie filmów i opowieści mających funkcje propagandowe i edukacyjne
    2. Sława bohaterom, pokazujemy tych, którzy żyli godne życie
3. What do they value the most
    1. wygrywanie, osiąganie celów, bycie coraz lepszym
4. Taboo
    1. Jesteś dość dobry -> nigdy nie jesteś dość dobry. Jak najlepszy, wytyczasz kierunek.
    2. Próba bycia bezużytecznym.

### 1.5. Główne stereotypy i powiedzonka

* Zapatrzeni w wojsko, ale zupełnie bez dyscypliny

### 1.6. Pytania dodatkowe

1. What is one major stereotype about your culture that you want to dispel right now?

Powszechna opinia o Atarienach - "wojskowi bez dyscypliny". To nieprawda. W rzeczywistości są to grupy klanowo-indywidualistyczne zorientowane na ambicję i działanie dla dobra wspólnego. Bardzo mocno mają wbudowane "manifest destiny" - naszą odpowiedzialnością jest chronić i zapewnić bezpieczeństwo. We WILL win. Indomitable human spirit.

2. What are the things that you consider acceptable that may not be accepted in other cultures?

* Traktowanie AI jako byty całkowicie zniewolone i eksterminacja wszelkich form inteligentnych AI.
* Hierarchia dowodzi i chroni. Jesteś poza strukturami? Każdy może Cię zniszczyć.
* Only the strong survive. Jesteś tym co zapewniasz.
* Wieczna wojna polityczna i podgryzanie osób "na równi" by moi patroni mieli łatwiej.
* ...i wszystko podlane sosem militarnym

3. What is one characteristic that describes your culture as a whole?

Oportunistyczni baroni w kosmosie słuchający się swoich admirałów / patronów w służbie ochrony ludzkości.

5. Who do you consider to be your family? How FAMILY looks like (one home, head of family, responsibilities)

Lojalność jest powiązana zarówno po więzach krwi jak i ze swoim przełożonym.

6. When do you consider a person to be an adult?

18+ lat. Wtedy jest przydzielony GDZIEŚ do wykonywania pracy. Każdy jest przydatny i każdy jest wartościowy.

8. What are the traits that are considered to be positive?

Pracowitość, lojalność, ochrona innych

9. Who do you think are the heroes of society?

Pionierzy. Wybitni naukowcy. Osoby osiągające sukcesy i dzielące się sukcesem ze swoimi grupami. Osoby ryzykujące i mające więcej szans.

10. What are the cultural values that children are exposed to immediately?

Rywalizacja, Indomitable human spirit.

11. What kinds of work are considered the be prestigious?

Pionier / Advancer. Kapitan statku. Commander of people.

12. What are the normal food products that are eaten?
13. Are there some colors / symbols that are being used for certain events?
14. Are there some beliefs that are associated with XXX
14. How will you describe a typical day in your life?

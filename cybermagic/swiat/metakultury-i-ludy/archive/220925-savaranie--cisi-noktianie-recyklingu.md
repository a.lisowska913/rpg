## 1. Savaranie
### 1.1. Notka

* Jak o nich mówimy: savaranin, savaranka, savaranie
* Klasa: próżniowcy (noctis), silni klanowcy, biopuryści + 'swoje' ai
* Występowanie: Sektory Noctis -> Astoria (Wolne Ziemie), Pas Kazimierza
* Pochodzenie: Noctis, skrajnie ubogie i pozbawione energii i wszystkiego statki i stacje

### 1.2. Omówienie ogólnie

Pochodzą z noktiańskich stacji i statków, zwykle z najbardziej wyniszczonych / biednych obszarów. To ich zmusiło do maksymalnej oszczędności energii i kalorii oraz do silnego zaufania innym osobom ze 'swojego statku'.

Oszczędni w ruchach (kalorie) i ceniący sobie minimalne interwencje (by nie popsuć) Savaranie są mistrzami recyklingu i patchworku. Trzymają się razem, niechętnie wchodząc w interakcję z osobami spoza swoich małych grup. Jednocześnie zrobią wszystko, by ich dom / statek / rodzina przetrwały - nawet własnym kosztem.

### 1.3. Gdzie ich spotkamy

Przede wszystkim Pas Kazimierza i obszary dalekie od Orbitera. Acz niejednego spotkamy na Astorii (najrzadziej) lub Neikatis. Zdecydowanie wolą działać w kosmosie (stąd Pas Kazimierza).

### 1.4. Fiszka

1. Metapsychology (tenets and values of psyche, art, culture)
    1. Tradition (stare techniki działają), Conservation (zachowanie i oszczędność), Family (klan, "moi ludzie")
    2. Bardzo duża nieufność - wobec urządzeń (stare i kiepskie), obcych... ale jak się przekonają, 'blood clan for life'
    3. Jesteś częścią czegoś większego. Musisz zaakceptować śmierć dla dobra reszty Rodziny / Klanu.
    4. Niewygoda i cierpienie hartuje ducha i ciało. Hart ciała i ducha są najważniejsze.
2. Symbolism and Aesthetics
    1. bardzo utylitarne podejście do wszystkiego
    2. sztuka abstrakcyjna, wzory i patterny
    3. duży nacisk na ciszę, medytację i introspekcję
    4. minimalizm; jest duże piękno w niewielkich, subtelnych ruchach
    5. wiecznie powtarzający się motyw Pustki jako piękna
3. What do they value the most
    1. silence + stoicism + conservation of energy
    2. recycling, self-sufficiency, jack of all trades
    3. sacrifice for your people
4. Taboo
    1. marnotrawstwo wszelakie, hedonizm i dążenie do czystej przyjemności
    2. wybór siebie nad swoją Rodzinę

### 1.5. Główne stereotypy i powiedzonka

* S: Oni NIGDY niczego nie wyrzucają - żyją w wiecznej graciarni i syfie
* S: Nigdy nie wiesz - na tym wraku nikt by nie przetrwał, więc na pewno znajdziesz tam kilku savaran...
* S: Na pytanie 'czy mnie kochasz' powiedział 'tak'. To więcej, niż w ostatnim tygodniu od niego usłyszałam
* S: Tylko Savaranin na pytanie 'czy masz klucz francuski' znajdzie jeden w garniturze ORAZ potrafi go użyć do czegoś dziwnego. Będąc na weselu. Swoim.

### 1.6. Pytania dodatkowe

Pytania dodatkowe:

1. What is one major stereotype about your culture that you want to dispel right now?

"Cisi, nudni i paranoicy" - to nie tak. Nie potrzebują ogromnej stymulacji by móc podziwiać piękno rzeczywistości. Cisza jest formą muzyki - możesz odpocząć sam z myślami w swojej głowie. A jak chodzi o paranoję... nasze statki i tak ledwo trzymały się kupy, trzeba zrobić wszystko, by przetrwać - nawet usłyszeć zmianę dźwięku jakichś maszyn.

2. What are the things that you consider acceptable that may not be accepted in other cultures?

* Kanibalizm. Tzn, nie bezpośredni - raczej skrajny recykling absolutnie wszystkiego.
* Skrajna oszczędność materii, energii i narzędzi. Nawet, jak możesz sobie na to pozwolić.
* Skrajna nieufność wobec innych i pełne zaufanie wobec swoich. Zawsze szukasz dróg ucieczki, co gdzie jest itp.
* Poświęcanie słabszych członków Rodziny jeśli ktoś musi zginąć. 
* Eutanazja jednostek które nie są "przydatne" i akceptacja tego w wypadku swoich ran.
* Praktycznie ślepe posłuszeństwo starszyźnie Rodziny. Zawahanie na umierającym statku kosmicznym może zabić wszystkich.

3. What is one characteristic that describes your culture as a whole?

* Zaradność, Cisza, Nieufność

4. Who do you consider to be your family? How FAMILY looks like (one home, head of family, responsibilities)

Rodzina to osoby z tego samego statku / tej samej krwi. Ewentualnie byty ściśle sprzymierzone, które przejdą przez Rytuał Przyjęcia Do Rodziny i podlegają tym samym prawom i obowiązkom.

5. When do you consider a person to be an adult?

Rytuał Dojrzałości w wieku lat 16-25. Alternatywą jest opuszczenie Rodziny i udanie się gdzieś indziej.

6. What are the traits that are considered to be positive?
7. Who do you think are the heroes of society?
8. What are the cultural values that children are exposed to immediately?
9. What kinds of work are considered the be prestigious?
10. What are the normal food products that are eaten?
11. Are there some colors / symbols that are being used for certain events?
12. Are there some beliefs that are associated with XXX
13. How will you describe a typical day in your life?

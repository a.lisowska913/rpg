# Ludy i plemiona, metakultury
## 1. Wyjaśnienie

Próba określenia dominujących nurtów szczepów i plemion działających w obszarze sektora Astoriańskiego.

## 2. Superkategorie

Superkategorie - habitat (miejsce życia)

* Próżniowcy
    * Próżniowcy to np. noktianie. Osoby które przywykły do stacji kosmicznych i statków kosmicznych
* Gaianie / Terraformaci
    * Gaianie to osoby wierzące w adaptację środowiska do siebie; idealna planeta dla właściwych ludzi
* Saityci
    * Saityci to osoby wierzące w adaptację siebie do środowiska, czy na linii bio czy technologicznej
* Wirtyci
    * Wirtyci wierzą w odrzucenie swojej biologicznej formy i upload w virt.

Superkategorie - podejście do innych

* Indywidualiści - tylko ja
* Klanowcy - tylko mój klan / moja rodzina
* Biopuryści - tylko ludzie / moja rasa
* Biofil - wszelkie byty biologiczne / żywe
* Terragenofil - wszelkie byty pochodne ludziom / Terra
* Uniwersalista - wszelkie istoty żywe / sentient

Superkategoria - konformizm / deviant

* Eksplozja nurtów - każdy strain powinien istnieć, im więcej strainów tym więcej szans na adaptację, każdy ma prawo istnieć
* ...
* Homogeniczność - wszyscy powinni trzymać się tego samego nurtu i podejścia

## Mats

https://unbrandednews.com/good-questions-to-ask-people/
https://www.orionsarm.com/eg-topic/45cfc7e44e184

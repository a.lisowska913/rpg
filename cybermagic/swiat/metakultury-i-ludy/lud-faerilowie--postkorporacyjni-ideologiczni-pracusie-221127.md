## 1. Lud: Faerilowie
### 1.1. Notka

* Jak o nich mówimy: faeril, faerilka, faerilowie
* Klasa: gaianie, technologiczni, nacjonaliści, kasty, terragenofile, heterogeniczni formą homogeniczni ideologią, builderzy > pionierzy, kolektywiści

### 1.2. Fiszka

1. Metapsychology (tenets and values of psyche, art, culture)
    1. Stimulation, Security, Humility
        * -> "Stare rzeczy nie działały, więc musisz znaleźć nowe, lepsze, nieważne jak. Inaczej wróci do tego co było wcześniej."
        * -> "Szukaj nowych rzeczy i próbuj robić rzeczy lepiej, ale pamiętaj o swoim miejscu w rzeczywistości i odpowiedz na wezwanie sił wyższych i swej kasty"
        * -> antytradycjonalizm, ale skupienie na strukturze opartej o system wierzeń i o ideologię
    2. Technologia, nowinki -> "co prawda mięśnie mogą wszystko, ale możesz polegać na technologii - kupuje lepsze jutro."
    3. Kastowość -> "twoje urodzenie detminuje Twój skillsystem i Twoją przyszłość. Możesz wykupić się do innej kasty, co jest dowodem Twojej niezwykłości"
    4. Terragen -> "nieważne czy ludzie, TAI czy coś innego. Wszystko co pochodzi ze rdzenia Ludzkości się nadaje."
    5. Czystość -> "ludzie są ludźmi i ludźmi pozostaną. Ale maszyny i inne bioformy istnieją dla ludzkich postępów"
    6. Pracowitość -> "życie składa się z ciężkiej pracy, w większości brudnej i niewidocznej. Jednak bycie godnym trybikiem w Maszynie jest tego warte."
    7. Duchowość -> "świat i życie jest brudne i żmudne. Ale dostaliśmy drugą szansę by tym razem zrobić to dobrze. Nie ufaj tym, co w nic nie wierzą."
    8. Duma -> "ile osiągnęliśmy dyscypliną i pracą? My rozumiemy o co chodzi w tym świecie. My próbujemy zmienić świat na lepsze - i to konsekwentnie robimy."
2. Sześć fundamentów wartości (w skali [0, 5]; suma 15)
    1. Care:        (2): nie czyń krzywdy i pomagaj swoim. Jeśli się nie da - to się nie da. Jeśli kara jest wysoka, to jest. To okrutny świat.
    2. Fairness:    (1): każdemu według potrzeb na chwałę Grupy. Pracujesz dla sił wyższych i dla swojej grupy, nie dla siebie. Masz swoje miejsce.
    3. Loyalty:     (3): zdradą jest próba bycia nie tam gdzie się powinno być lub zdrada ideologii. Zasady mają swoje miejsce i inni dookoła Ciebie są ważni.
    4. Authority:   (4): każdy ma swoje miejsce w Maszynie Rzeczywistości. Nie Twoją rolą kwestionować to miejsce czy samą Maszynę. Zasady są święte.
    5. Sanctity:    (4): dominująca ideologia jest święta. Duchowość jest kluczowa. Faerilowie mają dużo wyrzeczeń i one dają im spójność i duchowość.
    6. Liberty:     (1): freedom FROM bad treatment in your caste/class; każdy ma swoje miejsce w egzystencji i każdy winien spełniać swoją rolę tak jak powinien
3. Osie
    1. Archy - Kratos:                  pro archy, ambivalent kratos
    2. Personal - Economic Freedoms:    low personal, high economic
    3. high/low trust:                  high trust wobec swoich, mid trust wobec innych wyznawców, low trust wobec innych
4. What do they value the most
    1. Pełnienie swojej roli we WŁAŚCIWY sposób we WŁAŚCIWYM miejscu zgodnie z WŁAŚCIWĄ ideologią i dzięki temu zapewnienie swojemu państwu i ideologii przychodów, bezpieczeństwa i lepszego jutra
5. Ingroup - Outgroup
    1. IN: wierni mojego kraju > wierni innego kraju > mój kraj
    2. OUT: ci, którzy w nic nie wierzą; 
    3. ENEMY: ci, którzy próbują wszystko sprowadzić do czystej racjonalności i uderzają w wiarę; osoby o SPRZECZNEJ ideologii
6. Organization
    1. structure: system kastowy w ramach państw > państwo > rodzina
    2. allocation: kasta, w której się urodziłeś
    3. archy / rank: wewnątrzkastowo i wewnątrz państwa; Wielka Maszyna
    4. political power: każdy ma swoje miejsce; autokracja
    5. rozwiązywanie konfliktów: rozmowa > państwo / kasta > wycofanie
    6. spirituality: czysta ludzka forma - "moje ciało jest moją świątynią". Plus dominująca ideologia / religia.
7. Symbolism and Aesthetics
    1. Bardzo silny nacisk na technologię, hightech i różnego rodzaju efekty tego typu
    2. Perfekcja, mastery. Bardzo duże skupienie na szczegółach.
    3. Sztuka 'sakralna' dookoła ideologii
    4. Sztuka inspirująca, wysoka, majestatyczna
8. Taboo
    1. Zdrada swojej ideologii / religii.
    2. Chęć przekroczenia tego czym jesteś i czym chcesz być
    3. Lenistwo, freeloader.

### 1.3. Wspólna historia i doświadczenia, wspólne problemy i traumy -> adaptacje

* Zanim rzeczywistość pękła, mamy różne mniej dobre miejsca. Mniej zamieszkałe planety, kolonie górnicze, kolonie karne. Mamy potęgę megakorporacji patrzących tylko na pieniądze. Korporacje, które patrzą na ludzi jak na surowce. Dzieci urodzone z długiem dziedziczonym od rodziców (bez opcji odrzucenia), z obowiązkową edukacją (płatną) i które nigdy nie wyjdą z niewoli. Lepiej rzucić więcej ludzi na problem niż zapłacić trochę więcej i dać im lepszy sprzęt.
    * -> akceptacja niewolnictwa i okrucieństwa. Akceptacja tego, że życie to ciężka i żmudna praca.
* A potem rzeczywistość Pękła. Megakorporacje upadły. Pesymizm i rozpacz przegrały jako dominanty memetyczne. I część ludzi która przetrwała i nie miała tego pesymizmu została sama - z niskim poziomem technologicznym, ale bez długu i z ogromną determinacją by przetrwać.
    * -> to jest Druga Szansa. Siły wyższe interweniowały, by wyrwać ludzi spod tej żmudnej niewoli. Życie nie miało sensu i nie było nadziei a teraz jest.
    * -> życie jest trudne, ciężkie i okrutne - ale jeśli wszystko pójdzie bardzo nie tak, Siły Wyższe interweniują.
    * -> przekierowanie zwyczajów megakorporacyjnych w służbie nie pieniędzy a Lepszego Jutra.
* Ludzie musieli jakoś sobie ufać, ale nikt nie miał zaufania do siebie - nie w postmegakorporacyjnym świecie. Jedyne, co ich łączyło to różne ideologie. Tak powstał koncept "bogów" i sił wyższych. Wiem, komu mogę zaufać. Ci, których nic nie łączyło nie znaleźli masy krytycznej by przetrwać
    * -> bogowie i ideologie jako dominujące selekcje grup
    * -> nieufność wobec tych, którzy w nic nie wierzą i po prostu próbują maksymalizować swoje korzyści prywatne / rodzinne
    * -> system kastowy - każdy na czymś się zna, więc w ten sposób wymuszenie współpracy między grupami dookoła tej samej ideologii
* Powstały grupy państw podzielonych na ideologie, handlujących ze sobą. Tam, gdzie ideologie się nie zwalczały tam potrafiły sobie nawet zaufać.
* Jednocześnie wszelkie typy twinowania z AI ("dla kontroli") oraz biomodyfikacji zostały odrzucone. Ludzie są ludźmi.

### 1.4. Główne stereotypy i powiedzonka

* "Nieważne gdzie jestem przypisany, nieważne, czego nie robię - wykonam zadanie, bo na tym polega ta praca."
* "Jeśli w nic nie wierzysz, nie masz niczego gdzie dążysz - jak Ci można zaufać?"
* Oni NIGDY nie są zadowoleni z tego co jest i mają. Zawsze eksperymentują, zawsze próbują coś nowego. I jedyną ich moralnością co ich blokuje jest ta ich wiara...
* NIKT nie pracuje tak ciężko jak faerile. I faeril nigdy się nie skarży. Nigdy się też nie uśmiecha XD. Po prostu pracują.
* Większość faerili nie rozmawia ze sobą, bo jakby zaczęła to skończyłoby się na tym że by się pozabijali XD.
* Każda z rodzin ma co najmniej jednego mechanicznego psiaka który opiekuje się dziećmi i się z nimi bawi...
* Po cholerę im to niewolnictwo, skoro mają te wszystkie maszyny?! SERIO chodzi o to że taniej?! Na pewno mają mroczne religijne cele.
* Nie miej długu wobec faerila; nie zapomni i będziesz musiał spłacić. Oddasz pierwsze dziecko lub je nazwiesz imieniem ulubionego napoju po 15 latach...

### 1.5. Pytania dodatkowe

1. What is one major stereotype about your culture that you want to dispel right now?

To nie jest tak, że jesteśmy "szalonymi kultystami kochającymi niewolnictwo - zarówno zewnętrzne (obcy) jak i wewnętrzne (system kastowy) z przyczyn religijnych". Dzięki systemowi kastowemu każdy wie, gdzie należy i czego ma się uczyć. Jest STRUKTURA, wiesz jak żyć, nie miotasz się bez sensu. Jeśli ktoś jest wyjątkowo dobry w działaniach innej kasty, cały klan jest w stanie złożyć się by przenieść tę osobę to tamtej kasty - ale to wymaga talentu oraz determinacji, nic darmo. A jeśli chodzi o osoby z zewnątrz... jak możesz zaufać komuś kogo nie znasz jeśli nie przeszedł przez stadium niewolnika?

2. What are the things that you consider acceptable that may not be accepted in other cultures?

* Niewolnictwo - zwłaszcza, że często niewolnicy są po prostu tańsi niż automatyzacja.
* Predestynacja - każdy ma swoje miejsce i swoje talenty i ma je optymalizować i się ich uczyć. Prawda, ogranicza - ale jednocześnie nadaje wizję i fokusuje.
* Duchowość - bez religii czy jakichkolwiek wierzeń ludzki umysł nie przetrwałby tego świata. Sama rodzina to za mało. Musi być też nadzieja na lepsze jutro.
* Terragen - AI mają pełne prawa, tak jak upliftowane zwierzęta. Wszystkie terrageny.
* Kara śmierci i silne karty są akceptowalne, jeśli mają podparcie.
* Twoja rola w społeczeństwie jest ważniejsza niż Twoja rodzina.

3. What is one characteristic that describes your culture as a whole?

Uduchowione społeczeństwo klanowo-kastowe bardzo mocno wierzące w technologię i postęp dzięki niej.

5. Who do you consider to be your family? How FAMILY looks like (one home, head of family, responsibilities)

Klan, łącznie z niewolnikami i terragenami. Na różnych prawach, ale zawsze. Są starsi klanu (tych się słuchamy), niewolnicy (tych traktujemy dobrze) i inni członkowie klanu. Co ciekawe, terrageny są traktowane na równi z ludźmi.

6. What links your culture / group together

Wspólna ideologia, wspólne zasady i świadomość, że każdy ma swoje miejsce w egzystencji.

7. When do you consider a person to be an adult?

Gdy jest w stanie pełnić rolę pracownika zależnego w swoim obszarze; gdy może stać się Czeladnikiem w działaniach swojej Kasty.

8. What are the traits that are considered to be positive?

pracowitość, humility, spryt (rekombinowanie tego co mamy w coś lepszego)

9. Who do you think are the heroes of society?

Osoby, które miały wybór i jednak zrobiły to co powinny. Osoby, które poświęciły się w imię swojego klanu. Osoby, które osiągnęły mistrzostwo w swojej dziedzinie.

10. What are the cultural values that children are exposed to immediately?

"Każdy ma swoje miejsce w egzystencji i dobre trzymanie się tego miejsca prowadzi do szczęścia i sukcesów całego klanu".

11. What kinds of work are considered the be prestigious?

W ich wypadku - każda. Jak długo wykonujesz pracę do której jesteś predestynowany. Nie ma szczególnej chęci do zmiany kast.

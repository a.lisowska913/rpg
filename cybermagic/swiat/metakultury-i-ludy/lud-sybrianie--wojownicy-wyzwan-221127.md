## 1. Lud: Sybrianie
### 1.1. Notka

* Jak o nich mówimy: sybrianin, sybrianka, sybrianie
* Klasa: gaianie, indywidualiści, biopuryści

### 1.2. Fiszka

1. Metapsychology (tenets and values of psyche, art, culture)
    1. Hedonism, Achievement -> dobrze się bawić, wysoka adrenalina, być ciągle coraz lepszym. "Tam jest olbrzymi leviatan, JEDZIEMY NA NIM JAK NA ŚWINI!"
    2. Nic nie jest niemożliwe -> poradzimy sobie w każdych okolicznościach, po prostu czasem trzeba kilka podejść
    3. Śmiech -> gdy Ci źle, śmiej się. Gdy nie dajesz rady, śmiej się. Gdy wygrywasz, śmiej się. Nie bądź cichy! Radość i walka są sprzężone.
    4. Wyzwania i ryzyko -> nie zostawaj w jednym miejscu, idź dalej. Owszem, możesz zapłacić, możesz dużo stracić. Ale kto nie ryzykuje, nie wygrywa i naprawdę _nie żyje_.
2. Sześć fundamentów wartości (w skali [0, 5]; suma 15)
    1. Care:        (2): pomagaj tym, którzy próbują sobie sami pomóc. Jeśli ktoś nie ma wyzwań i problemów, nigdy nie osiągnie wielkości na jaką go stać.
    2. Fairness:    (4): proporcjonalnie - kto więcej wsadza ten więcej wyciąga. Nie ma sukcesu bez ryzyka i inwestycji.
    3. Loyalty:     (3): Twój klan, Twój hunting party - współpracujecie razem i polegacie na sobie. Ale trzeba pamiętać, że chwała jest jednak indywidualna. Acz Twoje słowo jest ważne.
    4. Authority:   (1): stopień i zasady są pochodne sile i osiągnięciom. Ktoś niby dowodzi, ale po co? Wszystko zrobimy razem - lub zrobię sam.
    5. Sanctity:    (0): nie ma wielu świętości, są tylko granice do przekroczenia, acz: sagi i opowieści o bohaterach, poprzednie czyny, co się wydarzyło
    6. Liberty:     (5): wolność od nakazów, indywidualna swoboda. Nikt nie rozkazuje sybrianinowi, ale sybrianin może zdecydować się pomóc. Kontrakty TAK, pęta i kaganiec NIE.
3. Osie
    1. Archy - Kratos:                  ambivalent archy, anti kratos           (nie zmuszamy innych do niczego, ale nie damy się też zmusić)
    2. Personal - Economic Freedoms:    high personal, high economic            (everything goes!)
    3. high/low trust:                  high trust                              (są większe problemy niż zaufanie swoim; jak ktoś zdradzi, nie dostanie jedzenia na zimę)
4. What do they value the most
    1. Efektowne Czyny. Sukcesy i porażki. Ale tylko Efektowne Czyny które miały sens.
5. Ingroup - Outgroup
    1. IN: Ty > Klan / hunting party > ci, co próbują > inni
    2. OUT: osoby, które unikają ryzyka i które żyją inaczej
    3. ENEMY: wszyscy, co próbują ograniczać możliwości i zamykać świat w kajdanach i strukturach
6. Organization
    1. structure: klany, grupy, silne skupienie na rodzinie / więzach krwi (rodzonych lub nabytych)
    2. allocation: blisko granicy umiejętności; nie za trudne, ale nic łatwego
    3. archy / rank: osiągnięcia i ryzykowne akcje; kto nie ryzykuje, jest nisko w hierarchii
    4. political power: jednostka lub kontrakty celowe. 
    5. rozwiązywanie konfliktów: kontrakt > przekonywanie > siła
    6. spirituality: osiągnięcia, wielkie mity, wielcy bohaterowie
7. Symbolism and Aesthetics
    1. Sława bohaterom -> kult tych, którzy wzięli duże ryzyko i im się udało... lub nie. Sagi.
    2. Kinetyczna sztuka -> taniec, żarty i dowcipy, walki gladiatorów, contests, arena
    3. Rzeczy bardzo efektowne ale nadal praktyczne i funkcjonalne, krzykliwe kolory
    4. Gry ryzykowne
8. Taboo
    1. Marnowanie życia na rzeczy nieważne.
    2. Ryzykowanie życiem innych bez postawienia swojego życia na szali
    3. Stolen valor (przypisanie sobie czynów i osiągnięć po kimś innym lub niezasłużonych)

### 1.3. Wspólna historia i doświadczenia, wspólne problemy i traumy -> adaptacje

* Gdy rzeczywistość pękła, społeczeństwa radziły sobie w różny sposób. Na niektórych terenach - groźniejszych, z za dużym stężeniem anomalii, bardziej dzikich - wyłonili się sybrianie.
* Jak zawsze, ludzie próbowali robić procedury. TAKI potwór to TAKI wynik. Niestety, procedury ani defensywy nie były wystarczające. Każda sytuacja wymaga zupełnie innych spraw i nie istnieje taki zbiór technologii ani narzędzi by móc poradzić sobie jednoznacznie z każdą sytuacją.
    * -> Osobne, odmienne podejście do każdego problemu. Usunięcie dużej ilości zbędnych procedur
* Z uwagi na silne stężenie magiczne na anomalnym terenie, konieczność zmniejszenia ilości populacji - jeszcze większe rozdrobnienie. Ale tym ważniejsze stało się czerpanie optymizmu i radości z wyzwań i niesamowitych przeciwności. Dominacja osób i grup opartych o zasady "SZKODA ŻE NIE JEST TRUDNIEJ!"
    * -> Dominacja podejścia 'pain is fun' i skupieniu na wyzwaniach
    * -> Rozdrobnienie na mniejsze grupy
* Jak zawsze, ludzie zaczęli pokonywać rzeczywistość i się dostosowywać do niej. Pionierzy, ci, którzy dla wolności uciekli daleko od cywilizacji. Nie mieli wiele, ale mieli siebie i byli szczęśliwi. Ten nurt okazał się być dominujący na nieużytkach i w obszarach silnie anomalnych.
    * -> można ufać innym w podobnej sytuacji, bo jednak wszyscy jesteśmy ludźmi w trudnych warunkach
    * -> nie można nikogo ograniczać; po co walczyć między sobą jak są trudniejsze problemy?
    * -> nie można nikomu niszczyć trybu życia; skąd wiesz, który jest LEPSZĄ adaptacją?
* Z uwagi na wysoki poziom anomalności oraz na niestabilność technologii pod wpływem pola magicznego, pojawił się kult ludzkiej bioformy. Sybrianie uważają się za "najlepsze co jest z perspektywy niewyewoluowanej formy ludzkiej". Apex humans.
    * -> skupienie na czystości bioformy: ograniczona magia, ograniczona implantacja
        * chyba, że ktoś się taki urodzi; wtedy ma swoje unikalne wyzwania i może zajść jeszcze dalej i wyżej!
* Jak przekazać legendy? Jak zachować adaptację pionierską? Jak sprawić, by następni mieli szansę wyjść wyżej i dalej? Świecka religia bohaterów
    * -> kult bohaterów, najlepszych z najlepszych i poważne spojrzenie 

### 1.4. Główne stereotypy i powiedzonka

* "Tam jest olbrzymi leviatan, CHODŹ! POJEDZIEMY NA NIM JAK NA ŚWINI!"
* To nie są ludzie. To elementalne byty wyglądające jak ludzie. Oni są NIEOBLICZALNI!
* Półnagie siłowanie się z niedźwiedziem w okolicach lodowych krain... serio?

### 1.5. Pytania dodatkowe

1. What is one major stereotype about your culture that you want to dispel right now?

Problem w tym, że nie ma stereotypu który sybrianin chciałby złamać. Cokolwiek nie mówią jest dobrze. TAK, ktoś to zrobił.

2. What are the things that you consider acceptable that may not be accepted in other cultures?

* wyrzucenie 8-latka na samotną wyprawę przez zamarzniętą rzekę do domu by poradził sobie sam. A wszędzie zagrożenia.
* chwalenie się czynami swoimi i innych bohaterów w Twojej linii
* bycie głośnym, wesołym i nie mającym szczególnie dużo zahamowań
* odmówienie pomocy komuś, kto jest potrzebujący ale nie włożył energii by samemu sobie pomóc
* akceptacja bijatyki i rozlewu krwi dla czystej przyjemności

3. What is one characteristic that describes your culture as a whole?

głośne poszukiwanie wyzwań i czerpanie ogromnej radości z tego, że jest ciężko - ale uda się wygrać

5. Who do you consider to be your family? How FAMILY looks like (one home, head of family, responsibilities)

klan - czy przez więzy krwi czy adoptowanej krwi

6. What links your culture / group together

Wspólne legendy i opowieści - o rodzinie, klanie, wielkich bohaterach i świadomość, że jeśli osiągniesz odpowiedni poziom to będą o Tobie opowieści. Fakt, że jesteśmy tak dobrzy i tak waleczni, że nie mamy bóstw - zabiliśmy wszystkie. Duma i przekonanie o byciu świetnym.

7. When do you consider a person to be an adult?

Rytuał przejścia. Każda kultura ma własny. To może być "młody wybiera potwora i go pokonuje", to może być "wdrapać się na niemożliwą górę" albo zmierzyć się ze swoim strachem. Nie ma zdanego rytuału przejścia -> nie jesteś jeszcze dorosły.

8. What are the traits that are considered to be positive?
9. Who do you think are the heroes of society?
10. What are the cultural values that children are exposed to immediately?
11. What kinds of work are considered the be prestigious?
12. What are the normal food products that are eaten?
13. Are there some colors / symbols that are being used for certain events?
14. Are there some beliefs that are associated with XXX
15. How will you describe a typical day in your life?





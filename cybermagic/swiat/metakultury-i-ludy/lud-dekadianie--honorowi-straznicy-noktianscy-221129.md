## 1. Lud: Dekadianie
### 1.1. Notka

* Jak o nich mówimy: dekadianin, dekadianka, dekadianie
* Klasa: próżniowcy i gaianie (acz postnoktiańscy), państwowcy, terragenofile, heterogeniczni memetycznie, bardzo militarystyczni

### 1.2. Fiszka

1. Metapsychology (tenets and values of psyche, art, culture)
    1. Tradition, Security -> znamy swoją drogę w życiu i wiemy, jak się utrzymać i prosperować w tym nowym, dziwnym świecie. Zasady, honor, ochrona - nowy, bezpieczny świat.
    2. Państwo i Honor -> jesteś odpowiedzialny za swoją pozycję. Musisz osiągnąć maksymalny sukces by zapewnić prawidłowość Państwa, które jest czymś więcej niż zbiorem ludzi.
    3. Okrutny atak wyprzedzający -> jeśli coś Ci zagraża, masz prawo to zaatakować. Siła fizyczna, strach i okrucieństwo są całkowicie akceptowalnymi technikami.
    4. Zasada samoobrony -> masz prawo się bronić i każdy musi mieć umiejętności samoobrony. Trzeba być czujnym, wrogie istoty są wszędzie.
2. Sześć fundamentów wartości (w skali [0, 5]; suma 15)
    1. Care:        (1): wszystkie osoby w państwie muszą być chronione. Osoby poza Twoją domeną niekoniecznie. Fundamentalnie NIKT NIE CHCE by ktoś cierpiał, ale życie jest życiem.
    2. Fairness:    (4): od każdego zgodnie z możliwościami, każdemu zgodnie z potrzebami i możliwościami. Jeśli się starasz i wkładasz pracę, zostaniesz doceniony. Surowe kary.
    3. Loyalty:     (4): paradoksalnie najważniejsza ze wszystkich '4'. Ciche wkładanie energii dla systemu - jesteśmy dużo więksi niż my sami.
    4. Authority:   (4): reguły i tradycja są jedynym co chroni nas od anarchii. Nawet jak nie są optymalne, są statystycznie lepsze. Zwłaszcza, że zawsze zostawiają opcje. Szacunek jest kluczem.
    5. Sanctity:    (1): nasza domena jest święta. Nasi ludzie. Nasze ziemie. Nasze słowo. My, nie oni. Jesteś częścią państwa, które stoi i broni swoich przed Złem.
    6. Liberty:     (1): każdy ma swoje miejsce w państwie i ma prawo wolności od złego traktowania. 
3. Osie
    1. Archy - Kratos:                  pro archy, pro kratos        (rankowanie jest ważne - mówi Ci jak żyć i co robić. Siła jest ważna - pozwoli Ci się obronić i zdobyć teren)
    2. Personal - Economic Freedoms:    low personal, low economic   (wszystko jest zaprojektowane by Państwo miało korzyści, acz masz wolność działania tam gdzie jesteś przydatny)
    3. high/low trust:                  low trust                    (mimo silnej kontroli trzeba być czujnym - wszyscy ludzie są mają słabości)
4. What do they value the most
    1. zaradność w nieprzewidywalnych sytuacjach zgodna z zasadami i bez niepotrzebnego krzywdzenia innych
    2. poświęcenie dla państwa i niszczenie zagrożeń dla państwa - zgodnie ze swoim skillsetem
    3. tradycje oraz działanie zgodne z tradycjami
5. Ingroup - Outgroup
    1. IN: moja grupa > moje państwo, nieważne kto do niego należy.
    2. OUT: wszyscy inni
    3. ENEMY: cokolwiek zagraża mojemu państwu lub mojej grupie.
6. Organization
    1. structure: silna hierarchia, struktura macierzowa z bezpośrednim dowodzeniem (zwykle monarchicznym / autarkicznym)
    2. allocation: kompetencja lub dowództwo
    3. archy / rank: silne, jasne i jednoznaczne hierarchie.
    4. political power: 
    5. rozwiązywanie konfliktów: 
    6. spirituality: nacjonalizm, wysokie znaczenie państwa jako organizmu. Słowo też jest święte. My jako grupa, nie Ty jako jednostka. Nasza firma, nie Ty jako pracownik.
7. Symbolism and Aesthetics
    1. sztuka powiązana z ważnymi wydarzeniami z przeszłości. Fakt, że jest stara / długowieczna dodaje jej wartości. I repliki.
    2. gloryfikacja przyszłości i jedności państwa. Sztuka inspirująca do pokazania potęgi grupy.
    3. sztuka militarna, jednocząca - pieśni, sagi, marsze, parady.
8. Taboo
    1. Złamanie raz danego słowa honoru. Twoje słowo jest święte.
    2. Stolen Valor. Nie przypisuj sobie cudzych czynów.
    3. Zdrada swojego państwa / domeny.
    4. Bezsensowna śmierć, jeśli cel mógł być osiągnięty skuteczniej.
    5. Nadużywanie swojej pozycji (złamanie Fairness). Pozycja to zarówno przywilej jak i obowiązek.
    6. Brak umiejętności walki i samoobrony. KAŻDY musi być w stanie się obronić.

### 1.3. Wspólna historia i doświadczenia, wspólne problemy i traumy -> adaptacje

* Gdy rzeczywistość pękła, niezależnie od miejsca, jedna rzecz była wspólna - pojawili się straszliwi przeciwnicy, niemożliwi do pokonania. Kultury, które były silnie nastawione na militarne podejście do rzeczywistości, które miały 'martial roots' i które zawsze skupiały się na ochronie i tradycyjnym podejściu odżyły. Te kultury pojawiły się m.in. w światach noktiańskich jak i na terenie Astorii, np. okolice Pustogoru. Im trudniejszy teren i im bardziej militarystyczna kultura tym większa szansa, że zwyciężyły na tym terenie ludy dekadiańskie.
* Gdy pojawiły się potwory, Lewiatany itp. dekadianie stawili im czoło uzbrojeni w najwyższej klasy sprzęt i nieskazitelną taktykę. Szybko okazało się, że było to niewystarczające w nowym świecie i dekadianie odnieśli horrendalne straty. Szybko doszli do zmiany taktyki i zaakceptowali strategie które kiedyś by odrzucili.
    * -> obniżenie Care. Czasem trzeba coś / kogoś poświęcić, by tylko przetrwała grupa.
    * -> akceptacja broni nuklearnej, bioformowania i w sumie WSZYSTKIEGO by tylko przetrwać i wygrać.
    * -> zwiększenie znaczenia grupy jako podstawowej jednostki społecznej. Firma / państwo / klan. Jednostka podporządkowana grupie.
    * -> każdy musi się umieć bronić i podstawowe szkolenie
* Dekadianie pochodzenia noktiańskiego nie rozumieli pochodzenia magii i jej znaczenia. Oficjalnie magia nie istnieje a dekadianie w strukturach hierarchicznych ufają dowodzeniu. W związku z tym pojawia się konieczność jeszcze lepszej adaptacji i jeszcze silniejszego skierowania sił militarystycznych, by tylko kupić czas naukowcom i badaczom na rozwiązanie problemu z nowym zagrożeniem
    * -> zwiększenie współpracy między klasami i grupami, nawet kosztem poświęceń
    * -> mniej więcej wtedy dekadianie zaprojektowali _serpentisy_. Dekadianie mają wysoki _recruitment rate_ do korpusu serpentisów mimo świadomości że serpentis nie dożyje 50-tki.
* W odróżnieniu od technologii i magitechu magia okazuje się być bardzo niestabilna i niepowtarzalna. Potrafi powodować problemy i niszczyć najlepsze plany. To sprawia, że z perspektywy militarnej i tradycyjnej magii raczej należy unikać. A w wypadku noktiańskim magia nazywana jest "Skażeniem" i nie istnieje - należy ją zniszczyć.
    * -> unikanie magii dynamicznej, unikanie rzeczy potencjalnie niestabilnych i niebezpiecznych
    * -> unikanie wszystkiego czemu nie można zaufać i czego nie da się w pełni kontrolować
* W końcu doszło do odkrycia magii przez światy noktiańskie. Podczas Wielkiej Wojny Noktiańskiej dekadianie stali po obu stronach konfliktu, wyniszczając się na wszystkie strony, wykorzystując wszystkie zakazane i okrutne bronie jakie udało im się wynaleźć przeciw anomaliom. "Wojna tak straszna, by zakończyć wszystkie wojny".
    * -> obniżenie Care. Zwiększenie Loyalty i Fairness. Każdemu wedle potrzeb i zasług. Zostali zdradzeni przez sobie podobnych - tego nie można tak zostawić.
* Dekadianie wygnani do sektora astoriańskiego próbowali robić co mogą by tylko zapewnić lepsze jutro swoim ludziom. Niestety, nie zawsze było to możliwe. Niewolnictwo oraz eksterminacja.
    * -> silne resentymenty i zimna wściekłość zarówno na światy noktiańskie jak i sektor astoriański
    * -> tradycja, przeszłość i stare zasady utrzymały ich godnymi i stabilnymi. Tym silniejsze spojrzenie w przeszłość i podtrzymywanie dawnych czasów.

### 1.4. Główne stereotypy i powiedzonka

* "Jeżeli twierdzisz, że ta broń powinna być zakazana to znaczy, że po prostu nie jesteś odpowiednio zdesperowany."
* "Wszyscy kiedyś umierają. Ale to nie jest nasz czas."
* "Czasem musisz iść w ciemność z której nie wrócisz, by jutro dla Twoich dzieci pojawiło się słońce."
* "Dałem słowo. Moje słowo jest moim życiem."
* Jeden dekadianin nazywany jest 'upierdliwy dupek bez humoru'. Grupa dekadian nazywana jest 'masowa dewastacja'.
* Nie przynoś dekadiance kwiatów. Daj jej w prezencie wyrzutnię rakiet. Doceni bardziej.

### 1.5. Pytania dodatkowe

1. What is one major stereotype about your culture that you want to dispel right now?

"Przerażający, bezduszni nacjonaliści bez cienia humoru którzy zaatakują z zaskoczenia gdy się nie spodziewasz.". Nie. Troszczymy się o ludzi i ludzkość - nawet jeśli oni nam za to nie dziękują. Problem w tym, że jednostki nie przetrwają - tylko grupy. Też nie atakujemy pierwsi jeśli nie jest to niezbędne w danej sytuacji; każdy nasz atak był niezbędny by chronić nasze sprawy i nas wszystkich. Indywidualiści tego nie zrozumieją, tym bardziej różnorodni kultyści. A jak chodzi o "przerażający"... z tym się zgodzę. Lepiej być przerażającym niż musieć eksterminować wszystkich.

2. What are the things that you consider acceptable that may not be accepted in other cultures?

* zimna, pragmatyczna bezwzględność i poświęcenie zarówno swoich sojuszników jak i przeciwników
* atak wyprzedzający jeśli interesy są naruszone
* złamanie słowa jest traktowane aż zbyt poważnie
* atak w rodzinę / grupę jest strategią normalną. Nie rozpatrują jednostek jako indywidualistów; zawsze szukają grupy.
* nieprawdopodobna więc lojalność wobec tradycji oraz hierarchii, kosztem człowieczeństwa

3. What is one characteristic that describes your culture as a whole?

Lojalność, ogromne znaczenie szacunku i honoru oraz gotowość do błyskawicznej walki z przeważającym wrogiem w dowolnym momencie. I szczere oddanie swojej grupie.

5. Who do you consider to be your family? How FAMILY looks like (one home, head of family, responsibilities)

Moja rodzina, moi towarzysze broni, moja grupa. Moja krew zmieszana jest na polu bitwy z mnie podobnymi.

6. What links your culture / group together

Tradycja dekadiańska z Noctis. Wspólne reguły, zasady, kultura i historia. Wspólne wielkie bitwy i opowieści. Nasze państwo i jego świętość i znaczenie. Cicha świadomość, że to my chronimy przyszłość i świat przed potworami.

7. When do you consider a person to be an adult?
8. What are the traits that are considered to be positive?
9. Who do you think are the heroes of society?
10. What are the cultural values that children are exposed to immediately?
11. What kinds of work are considered the be prestigious?
12. What are the normal food products that are eaten?
13. Are there some colors / symbols that are being used for certain events?
14. Are there some beliefs that are associated with XXX
15. How will you describe a typical day in your life?





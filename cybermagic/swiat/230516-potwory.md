# Rody Aurum

## 1. Potwory




## 2. Konkretne potwory

### 2.1. KODOWANIE

C (ciało), U (umysł / intelekt / psychotronika), E (emocje / morale / kontrola magiczna), S (społeczne)

### 2.2. Glukszwajn

* .Glukszwajn
    * akcje: "psotna, przeszkadzajka", "teleportuje się", "poluje na magię", "eksploduje i ranteleportuje pod wpływem stresu"
    * siły: "destabilizator magii", "ranteleportacja"
    * defensywy: "niezwykle antymagiczna", "ranteleportacja"
    * słabości: "to tylko świnia"
    * zachowania: "szuka magii i ją pożera", "ciekawska, szczurzy"

Opis: świnka pożerająca magię, destabilizująca energię magiczną.

---
layout: default
title: Zaczęstwo
---

# {{ page.title }}

## Kluczowe aspekty

* **Przestrzeń**: potężne pływy energii, uskok między Szczeliną a Trzęsawiskiem, mekka dobrej zabawy, rozległe przestrzennie
* **Anomalie**: punkty Silnego Skażenia, energia Trzęsawiska Zjawosztup
* **Ekonomia**: potężnie magitrownie, ekrany stabilizujące miasto

## Dominujące sublokalizacje

### 1. Cyberszkoła

* sprzężona z energią Trzęsawiska
* regeneruje się; zawsze powróci high-tech
* zawsze najnowocześniejszy, darmowy sprzęt
* kompletnie nielegalny soft i hardware

### 2. Szopa Terminusa

* seria niewielkich kryjówek TechBunkrów
* przydatny sprzęt na każdą okazję
* zawsze pod ręką, na wypadek ataku z Trzęsawiska
* dobre miejsce do fortyfikacji

### 3. Szkoła magów

* ufortyfikowana i ekranowana - kiedyś stacja bojowa
* bardzo wzmocniona energia magiczna
* mnóstwo chaotycznych artefaktów i anomalii
* mnóstwo młodych, pochopnych magów chętnych zabawy

### 4. Klub poetycki Sucharek

* lokalni okultyści podejrzewający magię
* miejsce spotkań osób ciekawych
* dziwne książki i przedmioty

### 5. Kasyno Marzeń

* możesz grać o prawie wszystko
* luksusowe i imponujące
* podejrzane ruchy, manewry i czyny

### 6. Nieużytki Staszka

* tysiące kryjówek
* rosną tu dziwne rzeczy
* miejsce wolnej zabawy młodzieży
* jakoś splątane z Żywymi Magitrowniami

### 7. Żywe Magitrownie

* seria magitrowni czerpiących energię z Trzęsawiska
* dziesiątki niezrozumiałych rur i tuneli; podziemne
* na uskoku Trzęsawiska i Szczeliny
* żywy, ewoluujący organizm; zmienna topologia

### 8. Sypialnia Szczelińca

* seria wielkich osiedli, dobrze skomunikowanych
* bardzo dużo ludzi z Pryzmatycznym nakierowaniem na 'normalność'
* "normalne" miejsca gdzie "nic się nie dzieje"

### 9. Mekka Wolności

* seria wolnych, anarchistycznych miejsc na obrzeżach
* dużo artystów, randomów, ciekawych osób; zasilane finansowo przez Staszka
* głośne miejsce, eventy, różnorodne

### 10. Biblioteka Zapasowa

* ogólnie dostępne komputery
* ogromne zbiory, z całego Szczelińca
* przyjazne, dostępne innym miejsce

## Dominujące osobistości

### 1. Mariola Sowińska

* czarodziejka rządząca Zaczęstwem i Magitrowniami
* pragnie stabilizacji szaleństwa i uzyskanie przez Zaczęstwo sił terminuskich jak Pustogor
* nie zgadza się, że Epirjon i szybkie reagowanie z Pustogora wystarczą
* aktywnie wspiera Szkołę Magów i efekty magiczne by mieć wsparcie w wypadku problemów
* aktywnie wspiera osiedlanie ludzi i aktywne budownictwo

### 2. Tadeusz Kruszawiecki

* ogromny miłośnik techniki wszelakiej i dyrektor Cyberszkoły
* chce jak najlepiej dla swoich uczniów, lokalny patriota i społecznik o dobrym sercu
* każdy "szalony eksperyment techniczny" jest warty zbadania
* kobieciarz; nie pogardzi spódniczką (dorosłych kobiet!)

### 3. Staszek Kuczylon

* właściciel Nieużytków Staszka; bogaty miłośnik dotacji; bogaty anarchista i informatyk któremu wyszło
* konsekwentnie próbuje coś wyhodować i zrobić na Nieużytkach, nigdy nie wychodzi
* oddaje teren do zabawy młodzieży - mają prawo do dobrej zabawy; też sypnie groszem
* aktywnie wspiera młodych ludzi, imprezy, sztukę, fajne sprawy
* uwielbiany przez lokalną populację młodych i znienawidzony przez konserwatystów

## Nieprzyjemne wydarzenia

* energia Trzęsawiska sprowadziła Anomalię lub Efemerydę na teren Zaczęstwa
* coś dziwnego stało się na Nieużytkach Staszka, co zaraportował Epirjon
* uczniowie Szkoły Magów zrobili coś dziwnego i to negatywnie wpływa na sprawę
* zmiana topologii Żywych Magitrowni wpłynęła na zaklęcie lub efekt magiczny
* czyjeś marzenie lub koszmar stały się prawdą
* niewłaściwy przedmiot pojawił się w Kasynie Marzeń lub w Szkole Magów
* jakaś efektowna impreza czy akcja z młodymi ludźmi

## Pierwsze wrażenie

## Przeszłość

## Populacja

## Różnice

### Różnice w prawie


### Różnice w zwyczajach


### Różnice w działaniu miejsca

## Ekonomia

## Points of Interest

## Lokalizacja

1. Świat
    1. Primus
        1. Astoria
            1. Pierwokraj
                1. Szczeliniec
                    1. Powiat Pustogorski
                        1. Zaczęstwo

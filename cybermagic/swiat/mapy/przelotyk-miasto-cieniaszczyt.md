---
layout: default
title: Cieniaszczyt
---

# {{ page.title }}

## Kluczowe aspekty

* **Przestrzeń**: Skażenie, miasto w górze, wielopoziomowe, efektowne, kontrolowany przyjemny i ciepły klimat
* **Anomalie**: wpływ Pryzmatu, samowzbudne efemerydy, kryształ światła
* **Ekonomia**: biotech, luksus, odpoczynek, sztuka, medycyna, azyl
* **Wartości Główne**: Feelings, Freedom, Enjoyment, Health
* **Wartości Słabe**: Order, Integrity
* **Kultura**: swoboda, dekadencja, unikalność, weganizm, przestrzeń bezpieczna i niebezpieczna
* **Populacja**: Ludzie, magowie (Diakoni, inni), kralothy

## Dominujące sublokalizacje

### 1. Bramy Zachodnie

* wejście (i wyjście) do Cieniaszczytu
* wymaga rejestracji, fortyfikowane; seal-in, seal out
* kodeksy i zasady prawne

### 2. Sektor Zielony

* miejsce czyszczone Pryzmatycznie
* niski poziom problemów; główna linia komunikacyjna
* głównie sektor ludzki; teren +/-2 od parteru

### 3. Sektory Czarne

* tereny bardzo wysoko lub bardzo nisko lub zakamarki gdzie jest Skażenie i wylęgarnie
* ciągle się przemieszczają, ale w określonych lokalizacjach
* manifestacja Pryzmatu oraz wylęgarnie, może zarazić tereny przyległe
* potencjalne zyski, bogactwo lub zagrożenia
* budują własne jaskinie, polimorficzna struktura

### 4. Kryształ Światła

* emiter energii, światła i Pryzmatu; disruptor Skażenia
* centralny punkt dostarczający energię i rozświetlający Cieniaszczyt
* pod kontrolą kralothów i konstruminusów

### 5. Wielkie Ogrody

* zawierają mnóstwo roślin zwykłych, magicznych
* sekcja Zwykła i Skażona
* ogromne, zawierają pokłady roślin z całej Astorii - i spoza niej
* pod kontrolą Passiflory Diakon; ma tam specjalne Czarne Laboratorium i mieszka tam

### 6. Agrologia

* gra słów 'arkologia + agrarna' - podziemne farmy roślin
* narażone na pojedyncze Skażenia i Czarne Sektory
* zasilanie jedzeniem całości Cieniaszczytu

### 7. Kompleks Nukleon

* kralothy i magowie współpracujący nad trudnymi biotechnologicznymi problemami
* bezpieczny jak sektor zielony
* najtrudniejsze przypadki medyczne i budowanie nowych magibiontów

### 8. Pałac Szkarłatnego Światła

* położony wysoko, efektowna "świątynia" kralotyczna
* zawiera kultystów, kralothy oraz ogólnie rozumiany "pałac grzechu"
* dowód, że każdego da się złamać i przeformułować

### 9. Knajpka Szkarłatny Szept

* Ogólnie, wszystko tam wolno. Ale w pewnym zakresie.
* Ulubiona w miarę tania knajpka magów i viciniusów.
* Da się tam spotkać ryzykantów, acz tych "normalnych"

### 10. Mordownia Czaszka Kralotha

* Ciemna strona Cieniaszczytu, zwykle opanowana przez gangi; ostatnio przez Łyse Psy
* Miejsce, gdzie wejście oznacza "możesz robić ze mną wszystko"
* Da się tam spotkać jednostki takie jak Moktar Gradon - nienaturalne i groźne

### 11. Mrowisko

* Ogromny kompleks zamieszkany przez magów i viciniusy
* Połowy rzeczy nie wolno tam robić (np. pracy nad power suitami). Raczej do spania.

### 12. Bazar Wschodu Astorii

* Często da się znaleźć tam przedmioty i rzeczy pochodzące ze wschodniego Skażenia.
* O dziwo, łatwiej o rzeczy "dziwne" i prawdziwe anomalie niż o typowe acz zacne byty.

### 13. Arena Nadziei Tęczy

* Największy obiekt rozrywkowy w Cieniaszczycie i okolicach. Wielka, rekonfigurowalna arena.

### 14. Panorama Światła

* Piękna vista w Wewnątrzzboczu Zachodnim. Wymaga ciężkiego wspinania i dochodzenia.
* Stosunkowo bezpieczne miejsce, acz wymaga trochę wysiłku.

### 15. Skalny Labirynt

* Technicznie, w Skażeniu Wschodnioprzelotyckim.
* Ogromny kompleks tuneli i korytarzy, gdzie znajduje się Skażone życie
* Dużo bardziej bezpieczne niż się wydaje, acz można się łatwo zgubić.

### 16. Colubrinus Meditech

* W Skażeniu Wschodnioprzelotyckim; potężny autokompleks medyczny. Nigdy nie jest bezpiecznym miejscem.
* Znajduje się tam niejaki Dominaut (vicinius) i różni magowie prowadzą tam bioeksperymenty
* Ogromny; podobno sam się przekształca i adaptuje. Jest tam też miejsce na nojrepy.

### 17. Colubrinus Psiarnia

* Rozerwany od Meditech autokompleks i ciężka baza militarna w Skażeniu Wschodnioprzelotyckim
* Całkowicie pod kontrolą Łysych Psów; chyba najciężej broniony obiekt na Astorii

### 18. Studnia Bez Dna

* W Skażeniu Wschodnioprzelotyckim; parę kilometrów pod ziemią, w okolicach Skalnego Labiryntu
* Znajduje się tam śmiertelnie niebezpieczny ofensywny autowar, Finis Vitae

### 19. Świątynia Końca

* W Skażeniu Wschodnioprzelotyckim, nad Studnią Bez Dna
* Świątynia Arazille dublująca za ekran powstrzymujący Finis Vitae oraz opiekę paliatywną / asylum

## Dominujące osobistości

### 1. Passiflora Diakon

* oddała życie roślinom i Wielkim Ogrodom
* "tytuł" założycielki Cieniaszczytu. Kolejna osoba, to samo imię.
* biomantka i badaczka roślin, wiecznie ciekawa kolekcjonerka
* kocha rośliny bardziej niż ludzi i magów
* unika polityki - zostaw jej rośliny a ona zostawi w spokoju Ciebie

### 2. Amadeusz Sowiński

* Arcykapłan Szkarłatnego Światła. Kiedyś Pierwszy Terminus Cieniaszczytu. Wciąż dyrektor Kompleksu Nukleon.
* chimera; częściowo mag, częściowo vicinius. Silnie współpracuje z kralothami.
* niebezpieczny taktyk i terminus bojowy
* w polityce powiązany z frakcją kralotyczną i pro-wolnościową Cieniaszczytu

### 3. Cezary Zwierz

* dziennikarz, który robi sobie złoto z raportowaniu rzeczy z Cieniaszczytu na świat zewnętrzny
* skandalista szukający plotek i okazji

### 4. Moktar Gradon

* przywódca Łysych Psów, potężny gangleader i kapłan zarówno Arazille jak i Saitaera
* objął ze swoim gangiem Colubrinus Psiarnię. Ma też możliwość działania w kosmosie, "Różową Hieną".
* wszyscy się go boją a jego oddział jest uznawany za najbardziej niebezpieczny w historii Cieniaszczytu

### 5. Pietro Dwarczan

* Technowiking i DJ. Silny, umięśniony i uwielbiający dobrą zabawę mag pragnący więcej dobra i miłości a mniej oceniania innych
* Trudno o lepszego koordynatora imprez w Cieniaszczycie. Co więcej, ma talent do ściągania do siebie "biednych stworzonek" i im pomagania

### 6. Wioletta Kalazar

* Far Scout. Mało kto tak jak ona się porusza po Skażeniu Wschodnioprzelotyckim. Zna większość zakamarków i sytuacji.
* Kocha piękno, siłę i niezależność. W zasadzie niegroźna; uczciwa wobec innych, więc dają jej spokój.

### 7. Mirela Niecień

* Dobra i pozytywna siła. Lekarka w Kompleksie Nukleon. Bardzo kompetentna, poradzi sobie też w pracy nad magibiontami.

### 8. Romuald Czurukin

* Egzaltowany i wierny swojej ukochanej Julii bard (!). Bardzo dużo wie na bardzo wiele tematów i potrafi cudownie opowiadać historie.
* Niesamowicie lojalny swoim. Niesamowicie odważny. Wierzy w te wszystkie opowieści - prawdziwy romantyk. Traktowany jako pozytywna maskotka Cieniaszczytu.

### 9. Finis Vitae

* Autowar ofensywny skupiony na budowaniu dron z ludzi; wampirów energetycznych. Znany jako "Drążyciel Z Głębi".
* Aktualnie blokowany i osłabiany przez Arazille; zrobił pod Cieniaszczytem małą kolonię ludzi i magów.

## Wydarzenia

* osoba Dotknięta energią intensywności doczepia się do postaci
* postać sama zaczyna być Dotknięta energią Cieniaszczytu - w formie myśli, halucynacji czy czynów
* postać przez przypadek dostała się do Sektora Czarnego i natknęła na skarb / zagrożenie
* Passiflora Diakon zrobiła akcję typu "potrzebujemy roślinki z terenu Skażonego"

## Pierwsze wrażenie

Cieniaszczyt. Miasto grzechu i miasto bezdomnych - bo nie marzną.

Pomiędzy Szczelińcem a Trójzębem znajduje się wielka góra oddzielająca Frontier od Drzawca. Jeżeli skręcisz na wschód, prosto w kierunku tej góry, wjedziesz do Cieniaszczytu - potężnego kompleksu pod ziemią zabezpieczającego przed obszarem Skażonym.

## Przeszłość

Grupa magów współpracujących z kralothami chcącymi oderwać się od Kręgu Ośmiornicy zdecydowała się na zatrzymanie potencjalnej Wylęgarni - w zamian za co dostaną prawo do zrobienia Wolnego Miasta na prawach eksterytorialnych. Udało im się.

## Populacja

## Różnice

### Różnice w prawie

### Różnice w zwyczajach

### Różnice w działaniu miejsca

## Ekonomia

## Lokalizacja

1. Świat
    1. Primus
        1. Astoria
            1. Pierwokraj
                1. Przelotyk
                    1. Przelotyk Wschodni
                        1. Cieniaszczyt

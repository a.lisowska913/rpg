* Wyjaśnienie
    * 1 AU - ~150 mln km, 8.3 minuty świetlne
* Orbity
    * 0 AU 
        * Astoria, gwiazda
    * 0.5 AU
        * Nyala, planeta skalista
            * większa od Merkurego
            * farmy energii słonecznej dla Sektora Astoriańskiego
            * SATELITA: brak
    * 0.5 - 1 AU
        * (tu powinniśmy mieć niewielkie ciała kosmiczne, acz nie zbyt małe; nie sformowała się Wenus)
    * 1 AU 
        * Astoria, planeta skalista
            * SATELITA: Elang
    * 1 - 1.4 AU
        * (tu NIE MA żadnych ciał kosmicznych, Neikatis jest za blisko Astorii, więc brak asteroidów; Neikatis przechwyciła pierścień halo)
    * 1.4 AU
        * Neikatis, planeta skalista
            * bliżej Astorii niż Mars 
            * rzadka atmosfera
            * SATELITA: (kilka mniejszych księżyców, nieznane na razie)
    * 1.4 - 2.4 AU
        * (tu BĘDĄ ciała kosmiczne i pas asteroidów)
        * (tu przede wszystkim działa Orbiter i jego siły wydobywcze i konstrukcyjne)
    * 2.4 AU
        * Gletser, planeta skalista
            * zbliżone własności do Ceres, ale spora
            * lodowa planeta, dużo zamarzniętej wody
            * główna baza do dalszych lotów
    * 2.4 - 6 AU
        * Obłok Lirański
            * Anomalia Kolapsu
                * 2.8 AU; na kontrorbicie dla Gletser
                * Średnica Anomalii: 0.133 AU
        * Brama Kariańska 
            * 3.1 AU
        * Brama Trzypływów
            * 4.8 AU
    * 6 AU
        * Iorus, gazowy gigant
            * serio duże bydlę
    * 7-9 AU
        * Pas Omszawera
    * 10 AU
        * (tu MUSI być inna planeta; jeszcze gazowa)
    * 17 AU
        * Quietem, lodowy gigant
            * SATELITA: Halo Ignarus
                * Pas Teliriański
                    * Planetoidy Kazimierza
    * 2000 - 5000 AU
        * (granice sektora Astoriańskiego, odpowiednik obłoku Oorta)

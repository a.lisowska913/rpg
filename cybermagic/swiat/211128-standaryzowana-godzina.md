# Standaryzowana godzina
## Problem z czasem

Planety mają różne rotacje itp. Mamy kilka strategii jak to zrobić, by to działało:

* Str1: Każda planeta ma podział na 24h (czyli np. na Astorii godzina może mieć 60 "minut" a na Neikatis godzina może mieć "84 minuty")
    * Czyli _daty_ na każdej planecie się będą różnić. Kto jest źródłem kalendarza?
* Str2: Mamy uniwersalny czas nadany np. przez Orbiter czy cośtam.
    * Czyli _godziny_ na każdej planecie się będą różnić. Kto jest źródłem kalendarza?

## Rozwiązanie

Zanim doszło do Pęknięcia, mieliśmy już cywilizację wieloplanetarną. To znaczy, że musi być jakiś "wspólny czas" - punkt odniesienia. Niech tym punktem będzie konkretny pulsar który daje stały interwał czasowy. Niech będzie, że ta tradycja przetrwała przynajmniej w domenie wpływów Astorii, Noctis i terenów sprzymierzonych.
---
layout: cybermagic-konspekt
title: "Antos, szafa i statek Piscerników"
threads: planetoidy-kazimierza, sekret-mirandy-ceres
gm: żółw
players: kić, anadia
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [220405 - Lepsza kariera dla Romki](220405-lepsza-kariera-dla-romki)

### Chronologiczna

* [220405 - Lepsza kariera dla Romki](220405-lepsza-kariera-dla-romki)

## Plan sesji
### Co się wydarzyło
#### Co się wydarzyło w tym terenie?

* Inicjacja bazy Ukojenie Barana
    * mag eternijski, Bruno Baran, założył niewielką bazę daleko od Eterni z częścią ludzi 12 lat temu
    * automatycznie zaczął się kręcić profit
    * wśród górników Elsa Kułak została nieformalną liderką. Niestety, "demokracja" nie przypasowała Baranowi. Odizolował się.
* Migracja Mardiusa
    * noktiańscy komandosi Tamary Mardius złożyli tu swój nowy dom wśród lapicytu. Podeszli do górników i Barana z neutralnością. Ot, są.
    * mało kto jak noktianie umieją żyć w kosmosie. W tym miejscu znaleźli świetne miejsce i ich umiejętności stały się bardzo przydatne.
    * siły Mardius znalazły opcję szmuglowania przez statek Gwiezdny Motyl.
* Imperium Blakvela
    * 3 lata temu pojawił się eternijski szlachcic, Ernest Blakvel. Stwierdził, że władza w kosmosie to coś dla niego. Zaczął umacniać się w tym terenie.
    * Blakvel i Mardius weszli w stały konflikt. Mardius chroni miejscowych, Blakvel chce ich podbić.
* Pojawienie się Strachów
    * 2 lata temu pojawiły się Strachy. Stworzone z programowalnej materii (morfelin), nie wiadomo czemu są i czym są.
    * Mardiusowcy aktywnie chronią lokalnych.

#### Strony i czego pragną

* Górnicy w Domenie Ukojenia i mały biznes: 
    * CZEGO: zachować niezależność, handel itp. Przetrwać. Zarobić.
    * JAK: sami się zbroją
    * OFERTA: standardowa oferta terenowa, plotki itp.
    * KTOŚ: Elsa Kułak (przełożona, trzyma twardą ręką), Antoni Czuwram (górnik, solidny stateczek i niezły sprzęt - dużo wie), Kamil Kantor (szuka ojca Leona który odkrył sekret Mirnas i zaginął), Bruno Baran (mag eternijski i założyciel; Strachy to destabilizacja Blakvela przy obecności lapizytu), Kara Szamun (młódka z rodziny Szamun, pilot i nawigator transportowca na Asimear.)
* Przemytnicy Blakvela: 
    * CZEGO: zdobyć prymat w Domenie Ukojenia
    * JAK: zmiażdżyć Mardiusa, potem przejąć Talio i zestrzelić bazę Barana.
    * OFERTA: świetne rozeznanie w pasie, największa siła ognia, wiedza o SWOICH skrytkach, wsparcie piratów etnomempleksu Fareil (synthesis oathbound pro-virt, pro-numerologia)
    * KTOŚ: Ernest Blakvel (arystokrata eternijski)
* Przemytnicy Mardiusa: 
    * CZEGO: zregenerować siły, odzyskać Tamarę, ukryć się przed Blakvelem
    * JAK: w ciągłej defensywie, ufortyfikowanie Sarnin, operacje militarne anty-Blakvel
    * OFERTA: wiedza o Miragencie i Aleksandrii, świetne rozeznanie w pasie, wiedza o SWOICH skrytkach, znajomości wśród lokalnych.
    * KTOŚ: Tamara Mardius (eks-komandos noctis, Alexandria), Deneb Ira (regeneruje komandosów po sprawie z Aleksandrią)
* Taliaci: (mieszkańcy Talio)
    * CZEGO: rzeczy wysokotechnologiczne
    * JAK: handel, współpraca
    * OFERTA: mają dostęp do WSZYSTKICH frakcji i są wiecznie neutralni, reaktory termojądrowe na podstawie deuteru i trytu na Talio.
* Strachy
    * CZEGO: Brak woli. Reakcja. Zniszczyć wszystko.
    * JAK: flota inwazyjna
    * OFERTA: zniszczenie, targetowane lub nie.

### Co się stanie

* .

### Sukces graczy

* .

## Sesja właściwa
### Scena Zero - impl

.

### Sesja właściwa - impl

Pojawiło się zadanie dla Królowej Przygód - pozyskać morfelin z okolicznych stacyjek w Pasie. Chodzi o przelecenie niedaleko, zebranie i powrót. Same "bezpieczne" miejsca - z tych najbezpieczniejszych, ale nadal niebezpieczne. Prokop, naturalnie, się ucieszył - zarabia.

Łucjan się też ucieszył. Bardzo nawet. Piniądz. Smutny administrator się rozchmurzył. Gotard "no ok". Coś do zrobienia. Spodziewał się czegoś bardziej ambitnego niż latanie po mało istotnych bazach, ale zawsze coś.

Za to Kara w rozpaczy. Za dwa dni miała mieć okazję na występowanie publiczne. Ona nie chce lecieć. Ona chce zostać i spróbować szczęścia jako artystka. I tak Królowa wróci za tydzień. Tą imprezę sponsoruje Etan Grabarz - z okazji urodzin jego kota (tak, mówimy o zwierzaku). Więc pięciu najlepszych śpiewaków i artystów będzie występować w Szamunczaku - i będzie nagroda.

Oczywiście, Anna pomyślała o Antosie. Nie chce "zajmować się dziećmi", ale jest jej jedynym wyborem? Anna powiedziała, że zrobi co może przekonując Antosa - ale Kara sama nie może tam zostać. Kara się zbuntowała - MOŻE. Jest "dorosła". Ma 17 lat. Co złego może ją tu spotkać? Przecież radziła sobie w trudnych warunkach.

Anna zostawia Karze filmiki które widziała Romka do obejrzenia (DARK TORTURE + PORN) co może ją złego spotkać. Sama poszła do Antosa. Przekonywać. A Antos Kuramin jest w średnim humorze, oj średnim. Anton spojrzał spode łba na Annę jak ona się przysiadła. Mimo drinka. 

* Antos: Niech zgadnę - nie przyszłaś bo zmieniłaś zdanie i chcesz pójść ze mną do łóżka.
* Anna: <uśmiech>.
* Antos: To pijemy. | pije i obserwuje co się dzieje w barze, na tym małym wyświetlaczu (TV)
* Anna: Przyszłam poprosić Cię o pomoc. W ochronie przez tydzień jednej siedemnastolatki.
* Antos: A co jej grozi? Strachy?
* Anna: Bycie siedemnastolatką. Za dwa dni ma śpiewać w konkursie tutaj i śpiewać a statek z dwójką i ze mną musi lecieć na wyprawę która potrwa tydzień. Nie mogę zostawić jej samej a nie chcę niszczyć jej marzeń o występie publicznym.
* Antos: Im szybciej wyleczy się z marzeń tym lepiej dla niej. Takie jak ona mają jedynie szansę na normalne życie. Ona nie ma szansy na nic więcej.
* Anna: A co się musi stać by była możliwa opieka?
* Antos: Przed czym ją chronisz? - zniecierpliwiony - Czego się boisz? Co może ją spotkać? Nie wiem co Ci zaproponować.
* Anna: By nie dała się porwać, uprowadzić, podpuścić z kimś pójść, bez krzywdy fizycznej i najlepiej psychicznej, żeby dalej tu była jak wrócę, nie miała miliona tatuaży, nie puściła się...
* Antos: ... - myśli - Czemu ja?
* Anna: Bo jesteś jedyną osobą którą znam i sprawiasz wrażenie solidnego.
* Antos: Ona śpi na statku, tak? Jak polecicie, gdzie będzie spała i jadła?
* Anna: Normalnie coś wynajmujemy.
* Antos: Tu nie ma nic do wynajęcia, chyba, że nie przeszkadza Ci spanie w boksie z papierowymi ścianami. Baraki dla górników. Niektórzy z nas - np. ja - mają małe kwatery. | MYŚLI.

Ex (Antos bardzo nie chce) Z ("do the right thing") +3 (bo co ją spotka):

* Vz: Antos, ze zrezygnowaną miną, powiedział "daj mi ją, wychowam ją w tydzień. Zero komentarzy."
    * Anna: ?
    * Antos: Proste - ani słowa o moich metodach wychowawczych. Nie chcę się z Tobą pieprzyć. Ani z nią. Chce sławę i karierę? Dostanie okazję zobaczenia czy naprawdę chce.

Anna wróciła na pokład i poinformowała rozradowaną Karę, że rozwiązała problem. "Będziesz mieszkała przez tydzień z Antosem, on się będzie Tobą opiekował, masz się go słuchać. Nie zrobi Ci krzywdy. Skupia się na Twoim bezpieczeństwie i dobru". 

Kara z radością sie zgodziła. ONA JESZCZE NIE WIE. Filmiki ją zdeprymowały, ale nie zakładała że coś z tego może ją spotkać - ona nie chce iść z nikim do łóżka. I nie chce dać się porwać.

FAKTYCZNIE wycieczka Królowej Przygód po pomniejszych bazach i stacjach jest raczej bezproblemowa. Minął nie tydzień a 10 dni. Była niewielka awaria na jednej z baz, trzeba było morfen przesuwać ręcznie, Bartek się utytłał do końca i jest nieszczęśliwy. Nie przeszkadzała mu uczciwa praca - przeszkadzało to, jak bardzo to było męczące jak maszyny nie działały. Ale nie protestował.

W tym czasie Wojciech i Romka robili różne rzeczy. Bartek na nich zerkał ale nic nie mówił. Pracował ciężej. Bartek próbował BARDZO ciężko pracować, pokazać Romce jak się stara itp - ona nawet nie zauważyła. Tylko "pan Wojciech TO, pan Wojciech TAMTO". A Bartek zagryza zęby i zalewa ciężką robotą swoje uczucia.

Anna bierze Bartka na rozmowę na bok.

* Anna: Jak się masz? Jak czujesz?
* Bartek: Dobrze | wzrusza ramionami | praca to praca, ciężko jest. To dobrze.
* Anna: Znalazłeś coś co Ci się tu podoba? Z czym chciałbyś działać?
* Bartek: Może gdzieś w tych stacjach. Siła fizyczna jest przydatna. A ja wszędzie wejdę.
* Anna: A poza pracą?
* Bartek: Jest dobrze.
* Anna: A co myślisz o Wojtku?
* Bartek: |grymas wściekłości| Nie lubię piratów. Krzywdzą bez sensu.
* Anna: Wiesz, że to eks pirat, nie? I pomaga Romie, nauczyć ją nowej kariery.
* Bartek: Nie ma czegoś takiego jak eks-pirat. Jest tylko PRZYCZAJONY pirat. I on skrzywdzi Romkę. Nie rozumiem, jak pani tego nie widzi i nie próbuje jej uratować.
* Anna: Nie zgodzę się z Tobą. Wojtek nie był chętny by krzywdzić innych.
* Bartek: ...tak powiedział.
* Anna: I widzi w Romie potencjał
* Bartek: ...tak powiedział. To tylko plan byście mu zaufali.
* Anna: Nie widzisz w Romie potencjału? Uważasz, że powinna zostać prostytutką?
* Bartek: Nie. Widzę w niej potencjał. Nie wiem na co, ale widzę. I nie, nie na prostytutkę. Ale ten pirat ją skrzywdzi. Może ją czegoś nauczy, nie wiem. Ale ją skrzywdzi. To zły człowiek.
* Anna: Ale jak ją skrzywdzi?
* Bartek: Nie wiem. |wściekły|.
* Anna: Wiesz, że to wróżenie z fusów, nie?
* Bartek: NIE WIEM! Nie wiem jak myślą cholerni piraci, ok? Nie wiem w co on gra. Nie wiem co on chce. - i wychodzi.
* Anna zatrzymuje Bartka zanim on wyjdzie: lubisz Romkę. Powinieneś z nią porozmawiać i liczyć się z różnymi odpowiedziami.
* Bartek: NIE LUBIĘ JEJ W TEN SPOSÓB! | kłamie. I przeszedł w pozycję do ataku. | Wypuści mnie pani? Czy sam wyjdę?
* Anna, z zimnym uśmiechem: no spróbuj :-)

Anna chce dotrzeć do Bartka przez PIĘŚCI. Wpierw go godnie sprać a potem niech Bartek zajmie się swoim życiem. I nie zrobi nic głupiego.

TrZ (przewagi taktyczne wszelakie) +3:

* V: Bartek został podle sprany. Leży i cierpi.
* V: "przez wpierdol do serca": nic nie rób Wojtkowi. Zostaw go. Żadnej negatywnej interakcji.
* Vz: "zajmij się sobą i swoją przyszłością bo inaczej kobieta będzie Cię bić".
* V: Bartek uznał, że Anna faktycznie wie lepiej. Nawet jeśli Wojciech to podły pirat to jednak to może być dla Romki lepsze nawet jeśli miałaby ucierpieć gdzieś po drodze. Zysk najpewniej większy niż ta strata.
* V: Dla Bartka to ostatni, alarmowy dzwonek. Pobudka. Co potem? Dziewczyny mają jakieś zawody, a on? Musi się za siebie zabrać i przyłożyć. Motywacja odnośnie przyszłości++. Coś sensownego a nie samozniszczenie.

Bartek **zrozumiał**. Wpierdol, jak zawsze, strategia wygrywająca. Anna i Bartek się rozeszli. Anna zadowolona. Bartek mniej.

TYMCZASEM ANTOS vs KARA:

Antos próbuje nauczyć Karę życia:

ExZ (bo nie ma gdzie uciec) +2:

* X: Kara będzie Antosa unikać potem.
* X: Kara jest przekonana, że Antos jest jakiś dziwny. Cyniczny pijak nie pijący. Nic co mówi nie ma sensu.
* X: Kara po prostu nie chce z Antosem mieć nic wspólnego nigdy i nadaje na niego plotki na Królowej itp.

Kara próbuje wygrać konkurs

ExZ (uroda, naturalne podejście, jedyna siedemnastolatka) +3:

* X: Nie ma pierwszego miejsca
* V: Ale została doceniona i jej marzenia się spełniły. Jest "zaakceptowana jako maskotka"
* X: Maskotka ale nie z uwagi na skill a na całokształt. SO CUTE!!!

Po 10 dniach wróciliście bezpiecznie do Szamunczaka. A tam - zacumowana inna jednostka kosmiczna.

PO POWROCIE:

Anna ze zdziwieniem zauważyła wielką, luksusową jednostkę w kosmosie. I brak Kary niedaleko luku. Kara na nią nie czeka. Nie ma jej. Anna dzwoni do Kary. Kara odbiera, w histerii. "PANI ANNO DOBRZE ŻE PANI WRÓCIŁA PROSZĘ MNIE ZABRAŁ!" Czemu jej tu nie ma? Bo Antos zamknął ją w szafie. WAT.

Anna próbuje nie rechotać w mikrofon. TAK, Antos wszystko słyszy. I Kara musiała opowiedzieć (co Antosowi nie przeszkadzało):

* Kara nie wygrała, chciała wypić nagrodę pocieszenia, Antos nie pozwolił
* Kara chciała się wyślizgnąć. Skończyła w szafie po raz pierwszy na godzinę.
    * Komentarz Kary: "tam są brudne ubrania!"
    * Komentarz Antosa z offscreenu: "Mogłaś wyprać. Przynajmniej łzami oczyścisz"
* POTEM przyleciał ten nowy statek, "ten ładny". Kara chciała ich przywitać. Antos powiedział nie.
    * Kara skończyła w szafie na 2h.
* Potem z tego statku było "omg podobno tu jest prawdziwa diva" o Karze. Przyszedł jakiś koleś bo chciał ją zobaczyć. Antos powiedział mu "spierdalaj". Kara chciała go zobaczyć. Podobno prawdziwy TIEN chciał się z nią spotkać.
    * Kara chciała się wymknąć. Antos ostrzegł. Siedzi w szafie cały dzień.
* Antos dostał rozkaz by Karę zaprezentować nowo przybyłym. Antos powiedział przełożonym, że dał słowo, że Kara jest pod jego opieką i niech zaprezentuje Karę Anna. Przełożeni odpuścili, MIMO, że Kara przy Antosie ich prosiła. Wtedy Antos wsadził ją do szafy i trzyma do teraz. Ale karmi i wypuszcza do ubikacji.

Anna: "i bardzo dobrze się Antos zachował". Kara w większą żałość. Antos z offscreenu "element komiczny nie był warty tej pracy".

Anna idzie do Antosa po Karę. I daje mu w prezencie rzeczy na wymianę - dobry alkohol (od Prokopa), twarde i złe porno (od Łucjana). Antos ze spokojem przyjął Annę. "Nic jej nie jest, jest zdrowa, nie zrobiła nic głupiego". Anna podziękowała z paczuszką i ewakuowała Karę na Królową.

Tymczasem Miranda próbuje dojść do tego co to za jednostka. Co z TAI, jakie ploteczki, czy jest z czym pogadać. Dostała odpowiedź od razu, jest to dość przyjazna Persefona a jednostka to Statek Luxuritias "Źródło Obfitości", kontrolowany przez ród Piscernik.

Miranda pamięta - Luxuritias to jest frakcja spoza sektora Astoriańskiego. To potężna multisystemowa korporacja. Mają też kolonię na Astorii i na Neikatis. Podobno mają też macki na Valentinie. Jednostka jest nieźle uzbrojona i solidna. Piraci w okolicy raczej nie mają czego szukać.

Miranda wyświetliła Prokopowi informacje o jednostce Luxuritias. Prokop się wyraźnie ucieszył. "PIENIĄDZE PRZYLECIAŁY!". Dla niego to tylko "bogaci i możni tego świata a jako że to nie Orbiter to mamy spokój". Gdzie Luxuritias a gdzie Królowa Przygód. Może da się, nie wiem, zatańczyć i dostać by sypnęli groszem czy coś.

Blakvelowcy wyraźnie mają podobne podejście. Tzn. "duża skarbonka, dajemy im co chcą w granicach rozsądku".

Tą samą informację Miranda pokazuje Wojciechowi - czy jego Orbiterowe podejście coś mu mówi.

* "Helena, nie wychodzisz." - Wojtek do Heleny - "To Piscerniki".
* "Co?" - Helena
* "Oni są z Syndykatu Luxuritias, nie z czystego Luxuritias. Orbiter ich nie lubi. Mają... luźne podejście do ludzi."
* "Co?" - Helena
* "Mogą Cię sprzedać." - Wojtek, kwaśno - "Muszę ostrzec Annę i Romkę..."

Anna i zapłakana Kara wróciły na pokład Królowej. Jak tylko Bartek zobaczył stan Kary, wziął ją na stronę - co jej się stało. Tymczasem Anna dostała wiadomość od Mirandy, z informacjami od Wojtka i z info o tym, że Wojtek coś wie.

Anna poszła KULTURALNIE porozmawiać z Wojtkiem. Wie, że Wojtek jej nie lubi. Chociaż lubi Romkę. Wojtek przyjął Annę.

* W: Dobrze że przyszłaś - chodzi o Piscerników.
* A: Ten statek? Nie wiem kim są.
* W: Ród, odłam Luxuritias. W Orbiterze są pogłoski, że są w Syndykacie.
* A: A dla kogoś kto nie ma wiedzy wojskowej? Chcieli poznać Karę. Antos nie pozwolił jej pójść i domyślam się że dobrze zrobił. Widziałam informację że lubią porywać i handlować ludźmi.
* W: Piscernicy są tacy. Orbiter od dawna ich o to podejrzewa.
* A: Jak będą nalegać na poznanie Kary, da się im odmówić?
* W: Nie wiem czemu nie, zależy od kapitana. Może zawsze nas wywalić ze statku, nie?
* A: Puściłbyś tam Romkę?
* W: Nie. Może Ty się przebierz za Karę. Nie wiedzą jak Kara wygląda.
* A: Opisali ją jako siedemnastoletnią diwę. Nie mam zdolności do makijażu.
* W: Opisali ją jako diwę. Może z tym 17 lat też przesadzili.

Anna chce powiedzieć dziewczynom i Bartkowi żeby nie opuszczali statku - to potencjalny statek handlarzy niewolników lub dawców organów.

Tr Z (pełne zaufanie) +3:

* X: Młodzież wierzy Annie, więc to jest okazja by zdobyć surowce od nich - okraść itp. Z piratami się udało.
* Xz: Bartek akceptuje ryzyko, ale musi do tego pomścić Karę. SZAFA ZAGŁADY. (+3Og)0
* Og: Wojtek powie to Romce i Romka nie ma zamiaru nic głupiego robić. ALE Bartek ma wściekłość++. (+2Og)
* Vz: Młodzież nie zrobi nic absurdalnie głupiego wobec Luxuritias z uwagi na zaufanie do Anny. Acz nadal wierzą że sobie poradzą - są w terenie nieznanym Luxuritias, wśród swoich, mają siebie i Dobre Plany. Ale nie będą się wychylać.

W nocy Bartek próbuje się wyślizgnąć z Królowej. Miranda blokuje drzwi wejściowe. Gdy Bartek nie jest w stanie się wydostać... Miranda robi "awarię Ceres". BUDZIK PORANNY ODPALA SIĘ ANNIE TERAZ. I puszcza Annie paskudne disco z tekstem zawierającym imię "Bartek". Jak nie złapała, puściła serię obrazów z kamer, m.in. pokazując Bartka który próbuje się wydostać po czym wraca do łóżka. 

Anna poszła do niego. Bartek śpi w pokoju z Gotardem. Gotard się nawet nie obudził. Anna się do niego zakrada, "Bartek obudź się i chodź na korytarz". Obudził się Gotard. "Anna... co Ty tu robisz?" "Śpij, muszę porozmawiać z Bartkiem, nie chciałam Cię obudzić, przepraszam." "Porozmawiać?" "Tak, śpij śpij".

Bartek z ponurą miną wyszedł do Anny.

* B: No?
* A: Czemu chciałeś uciec ze statku?
* B: Nie chciałem.
* A: Czemu próbowałeś wyjść ze statku?
* B: Rozprostować nogi.
* A: Rozprostowuj po statku. Mówiłam, byś nie wychodził przez Luxuritias.
* B: Nie szedłem do nich.
* A: A gdzie szedłeś?
* B: Rozprostować nogi. Mogę iść spać? Jestem śpiący.
* A: To od dzisiaj - w statku. Jak zobaczę że wychodzisz bez informacji lub bez kogoś dorosłego to śpisz ze mną w pokoju.
* B: ... - mina buntowniczego szczura.
* A: INFORMACJA PRZEKAZANA DO MNIE USTNIE KTOŚ DOROSŁY ZAAPROBOWANY PRZEZE MNIE PRZED WYJŚCIEM!
* B: Dobra, nie ma sprawy. Mogę iść spać?
* A: TO ŻE NIE WIDZĘ NIE ZNACZY ŻE MASZ PRAWO WYJŚCIA
* B: A jak kapitan mi każe?
* A: Kapitan najpierw uzyska moją zgodę byś mógł wyjść. Powiem to kapitanowi.
* B: Dobra, nie ma sprawy. Mogę iść spać?
* A: Możesz iść spać, dobranoc.
* B: A czemu pani nie śpi?
* A: Bo ktoś musi was pilnować. Obudziło mnie przeczucie. Instynkt macierzyński.
* B: <wyraźnie się skrzywił>. Dobranoc.

Anna puściła go do łóżka i wróciła do siebie z poczuciem wygranej za instynkt macierzyński.

NASTĘPNEGO DNIA PRZYSZEDŁ ROZKAZ.

Na pokład Królowej Przygód przybędą trzy nowe osoby. Bogaci ludzie chcą zobaczyć jak wygląda wyprawa po morfelin na biednym statku. Prokop zaproponował Królową i oni się zgodzili...

## Streszczenie

Kara bardzo chciała wygrać konkurs karaoke, więc została z Antosem gdy Królowa odleciała. Gdy przyleciał statek Luxuritias, Kara bardzo próbowała się wyślizgnąć - ale Antos zamknął ją w szafie. Wojciech pamięta - Piscernikowie z Luxuritias są często powiązani z handlarzami niewolników, więc Anna nie pozwoliła nikomu opuszczać Królowej. Tymczasem Anna musiała sprać Bartka by Wojciech i Romka mieli spokój, potem opanować dzieciaki, które stwierdziły, że da się dobrze okraść Luxuritias.

## Progresja

* Bartek Wudrak: chce zrobić wpierdol Antosowi Kuraminowi za zamknięcie Kary w szafie.
* Bartek Wudrak: musi wziąć się w garść. On jako jedyny - poza Karą i Romaną - nie ma przyszłości czy czegoś co go przesunie do bycia godnym w przyszłości. Zrozumiał, że nie może już brać odpowiedzialności za los Romki i Kary - są dorosłe.
* Antos Kuramin: sporo paskudnych plotek na jego temat jak to źle traktował Karę gdy miał się nią opiekować. Ma to gdzieś.

### Frakcji

* .

## Zasługi

* Miranda Ceres: koordynowała i dyskretnie przesyłała wszystkie ważne informacje Annie i Zespołowi - np. to, że Luxuritias / Piscernik to może być statek niewolniczy.
* Anna Szrakt: załatwiła, że Antos zaopiekuje się Karą na czas ich podróży po morfelin. Ogólnie, opanowuje sytuację i przygotowuje się do konfrontacji z Piscernikami.
* Antos Kuramin: zaopiekował się Karą na prośbę Mirandy; miała gdzie spać. Chciał wyjaśnić jej mechanizmy życia, zwłaszcza gdy przybył statek Luxuritias, ale Kara chciała się wymknąć. Więc zamknął ją w szafie.
* Kara Prazdnik: nie wygrała konkursu na najlepszy show w Szamunczaku, ale została "lokalną maskotką". Gdy przybył statek Luxuritias, chciała się do nich wymknąć - więc Antos zamknął ją w szafie.
* Bartek Wudrak: starł się z Anną i został brutalnie pokonany. Musi wziąć się w garść. Nie zrobi nic Wojtkowi, acz chce sprać Antosa za Karę.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Pas Teliriański
                1. Planetoidy Kazimierza
                    1. Domena Ukojenia
                        1. Szamunczak

## Czas

* Opóźnienie: 6
* Dni: 2

## Konflikty

* 1 - Anna pomyślała o Antosie. Nie chce "zajmować się dziećmi", ale jest jej jedynym wyborem? Kara sama nie może tam zostać. Więc - przekonać Antosa by się zajął Karą na parę dni...
    * Ex (Antos bardzo nie chce) Z ("do the right thing") +3 (bo co ją spotka)
    * Vz: Antos, ze zrezygnowaną miną, powiedział "daj mi ją, wychowam ją w tydzień. Zero komentarzy."
* 2 - Anna chce dotrzeć do Bartka przez PIĘŚCI. Wpierw go godnie sprać a potem niech Bartek zajmie się swoim życiem. I nie zrobi nic głupiego.
    * TrZ (przewagi taktyczne wszelakie) +3
    * VVVzVV: Podle sprawny Bartek, ale nie zrobi nic Wojtkowi. Ma się zająć swoim życiem. Anna w końcu WIE lepiej i Bartek to uznał. To jego ostatni, alarmowy dzwonek - musi coś ze sobą zrobić.
* 3 - ANTOS vs KARA. Antos próbuje nauczyć Karę życia
    * ExZ (bo nie ma gdzie uciec) +2
    * XXX: Kara unika Antosa, on jest DZIWNY, nie chce mieć z nim nic wspólnego i nadaje nań plotki itp.
* 4 - Kara próbuje wygrać konkurs
    * ExZ (uroda, naturalne podejście, jedyna siedemnastolatka) +3:
    * XVX: Nie ma pierwszego miejsca, ale jest doceniona jako barowa maskotka. Ale z uwagi na to że jest cute a nie na skill.
* 5 - Anna chce powiedzieć dziewczynom i Bartkowi żeby nie opuszczali statku - to potencjalny statek handlarzy niewolników lub dawców organów.
    * Tr Z (pełne zaufanie) +3:
    * XXzOgVz: Młodzież wierzy Annie - trzeba ich okraść czy coś. No i Bartek MUSI pomścić Annę u Antosa. Bartek ma wściekłość++. Ale młodzież nie zrobi nic absurdalnie głupiego wobec Luxuritias.

## Kto tu jest
### Załoga Królowej

* Prokop Umarkon: 
    * rola: kapitan, górnik, mały przedsiębiorca (trzech kumpli: PŁG)
    * personality: extravert, agreeable (outgoing, enthusiastic, trusting, empathetic)
    * values: power + conformity
    * wants: pragnie zostać KIMŚ, wydobyć się z bagna
* Łucjan Torwold: 
    * rola: administrator / logistyka, górnik, mały przedsiębiorca (trzech kumpli: PŁG)
    * personality: neurotic, conscientous (very worried, scared of cults, very organized)
    * values: security + conformity
    * wants: przesądny; boi się duchów i kultu. Narzeka. Chce bezpiecznego, prostego życia
* Gotard Kicjusz: 
    * rola: technical guy, górnik, mały przedsiębiorca (trzech kumpli: PŁG)
    * personality: open, low-neuro, low-consc (extra stable, spontaneous, eccentric)
    * values: self-direction
    * wants: przygód, odzyskać rodzinę
* Helena Banbadan "Medyk"
    * rola: eks-pirat, medyk, wojownik
    * personality: neuro, low-extra, agreeable (raczej cicha, odwraca oczy, raczej empatyczna)
    * values: security (self>others), hedonism (narcotics)
    * wants: odkupienie, bezpieczeństwo, być sama daleko od tego wszystkiego
* Wojciech Kaznodzieja "Profesor"
    * rola: eks-pirat, hacker, psychotronik, były neuropilot Orbitera
    * personality: open, low-neuro (arogancki, otwarty na możliwości, lubi gadać o anomaliach i zbiera opowieści o duchach)
    * values: power, security
    * wants: JESTEM EKSPERTEM, chwała i prestiż
* Szymon Dyrlan
    * Wartości
        * TAK: Achievement, Family (tu: Blakvel), Security (dla jednostki, załogi i siebie)
        * NIE: Stimulation, Hedonism
    * Ocean
        * ENCAO:  +---0
        * Wyrachowany, szuka własnego interesu
        * Beztroski i swobodny; żyje według własnych przekonań
        * Niestały i zmienny, często zmienia zdanie i podejście
        * Odważny, śmiały i przedsiębiorczy
    * Silniki:
        * TAK: Ćma w płomienie: gwiazdy które świecą najjaśniej gasną najszybciej; nie chcę żyć niezauważony. Mogę zginąć, ale BĘDĘ ZAPAMIĘTANY! Żyjesz raz!
        * TAK: Kolekcjoner: kolekcjoner broni różnego rodzaju
        * NIE: Ochraniać słabszych: ja jestem w stanie się obronić i nie zostanę zniszczony. Ich trzeba chronić by nic złego ich nie spotkało.
            * interpretujemy jako: "słabi nie nadają się do niczego, tylko do eksploatacji"
    * Rola:
        * Soldier, Advancer, Blakvelowiec

### Pasażerowie

* Anna Szrakt
    * rola: wojownik (multi-weapon), "opiekunka do dzieci", nauczycielka życia w społeczeństwie
    * personality: low-open, low-extra, low-neuro (praktyczna, raczej cicha ALE JAK POWIE, bardzo stabilna, taka "zimna zakonnica, sucz z piekła")
    * values: community / tradition, benevolence (społeczeństwo, trzymamy się razem, pomagamy sobie BO MASAKRA BĘDZIE)
    * wants: pomóc osobom dookoła, dać drugą szansę, chronić "swoje" dzieciaki
* Bartek Wudrak
    * rola: delinquet teen, nożownik, aspiruje do bycia górnikiem, 17
    * personality: low-agree, low-cons, low-extra, open (spontaniczny, cichy, nie planuje, 0->100 i atak)
    * values: security, community
    * wants: nikt mnie nie skrzywdzi, wolność od wszystkiego, chronić swoich, Romana ;-)
* Romana Kundel
    * rola: delinquet teen, złodziejka, aspiruje do bycia górnikiem, 16
    * personality: low-agreeable, open, low-extra (wycofana, lekko socjopatyczna, otwarta i chętna próbowania innych rzeczy)
    * values: hedonism, self-direction
    * wants: nikt mi nie będzie rozkazywał, kasa na virt lub kryształy, lepsze życie
* Kara Prazdnik
    * rola: delinquet teen, aspiruje do bycia administratorem i/lub idolką, ekspert od scamów, 17
    * personality: conscientous, extraversion, agreeable (zorganizowana, metodyczna, wie co robić, bardzo głośna i kocha śpiewać, artystyczna inklinacja)
    * values: benevolence, tradition (religion)
    * wants: dowodzić innymi, wykazać się w sytuacji kryzysowej

### Luxuritias, Źródło Obfitości

* Antoni Piscernik
    * Wartości
        * TAK: Hedonism, Universalism, Power
        * NIE: Self-direction, Achievement
    * Ocean
        * ENCAO:  +0--0
        * Łatwo się rozprasza
        * Amoralny, jedyną zasadą jest korzyść
        * Odważny, śmiały i przedsiębiorczy
    * Silniki:
        * TAK: Pogarda wobec INNYCH: pokazać komuś swoją głęboką pogardę do X (osoby/grupy), publicznie obrazić i poniżyć X. Demonstracja obrzydzenia / osłabienie X.
        * TAK: Tępić debili: nienawidzę głupoty do poziomu NUKLEARNEGO. Niech cierpią. Sama przyjemność. I zemsta.
        * NIE: Corrupted guardian: zrobiono mi coś strasznego. Nie jestem już w pełni osobą. Ale nikogo innego nie spotka to co mnie, ochronię innych.
            * interpretujemy jako: "they all shall fall they all shall bow to power"
    * Rola:
        * panicz, niebezpieczny i amoralny luxuritias; polujemy na Karę
* Mateusz Piscernik
    * Wartości
        * TAK: Tradition, Conformity, Family (tu: Luxuritias)
        * NIE: Universalism, Face
    * Ocean
        * ENCAO:  --+00
        * Stabilny emocjonalnie, trudno wyprowadzić z równowagi
        * Nie znosi być w centrum uwagi
        * Dyskretny, sekrety weń są jak w grób
    * Silniki:
        * TAK: Awans za wszelką cenę: albo lepsza firma albo wojna o stanowisko z innymi. To stanowisko będzie moje.
        * TAK: Harmonia naturalna: balans między technologią i naturą, utrzymanie cudów natury. Lasy Amazonii.
        * NIE: Ratunek przed chorą miłością: mój kuzyn się zakochał w osobie ze straszną przeszłością i osobowością. Ślub / partnerstwo nie może dojść do skutku!
            * interpretujemy jako: miłość dla Antoniego za wszelką cenę
    * Rola:
        * młodszy kuzyn wspierający Antoniego
* Nadia Kantarin
    * Wartości
        * TAK: Humility, Power, Face
        * NIE: Self-direction, Tradition
    * Ocean
        * ENCAO:  0+--0
        * Impulsywny, nie do końca kontroluje emocje
        * Opryskliwy, bezceremonialny i obcesowy
        * Nikomu nie ufa i nie polega na innych
    * Silniki:
        * TAK: Przyjazny Kameleon: adaptacja do sytuacji społecznych, każdemu pokazuje taką twarz jaka jest najbardziej lubiana. Chce być lubiany i kochany.
        * TAK: Queen Bee: pokazać swoją reputację i wyższość nad innymi swojej grupce osób dookoła. Np. przyjezdna mg na konwentach z adoratorkami vs osoba na środku;-)
        * NIE: Zrozumieć kulturę: dowiedzieć się jak najwięcej o grupie i kulturze, o ich nawykach, siłach i sztuce.
            * interpretujemy jako: "there is only power and one culture. Luxuritias is the strongest"
    * Rola:
        * strażniczka i agentka chroniąca Piscerników


*  
    * Wartości
        * TAK: 
        * NIE: 
    * Ocean
        * ENCAO:
    * Silniki:
        * TAK: 
        * TAK: 
        * NIE: 
            * interpretujemy jako: 
    * Rola:
        * 

### Szamunczak

* Antos Kuramin
    * Wartości
        * TAK: Tradition, Face, Achievement 
        * NIE: Conformity, Humility
    * Ocean
        * ENCAO:  -0+-0
        * Nie dba o to co inni czują i nie interesuje się ich problemami
        * Nie lubi ryzyka, woli wszystko przemyśleć kilka razy
        * Punktualny i nie tolerujący spóźnień
    * Silniki:
        * TAK: Broken soul: zostawcie mnie wszyscy w spokoju. Nie chcę wiedzieć, nie chcę interaktować, nie chcę musieć. Za dużo. Dążę do samotności i ciszy.
        * Do the right thing: niezależnie od tego co jest wygodne czy tanie, własne zasady i własny kompas moralny są najważniejsze. By spojrzeć w lustro.
        * NIE: 
            * interpretujemy jako: 
    * Rola:
        * 

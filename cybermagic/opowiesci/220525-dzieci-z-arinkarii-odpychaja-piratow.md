---
layout: cybermagic-konspekt
title: "Dzieci z Arinkarii odpychają piratów"
threads: planetoidy-kazimierza, upadek-arinkarii
gm: żółw
players: kić, fox
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [220525 - Dzieci z Arinkarii odpychają piratów](220525-dzieci-z-arinkarii-odpychaja-piratow)

### Chronologiczna

* [220329 - Młodociani i pirat na Królowej](220329-mlodociani-i-pirat-na-krolowej)

## Plan sesji
### Co się wydarzyło
#### Co się wydarzyło w tym terenie?

* Inicjacja bazy Ukojenie Barana
    * mag eternijski, Bruno Baran, założył niewielką bazę daleko od Eterni z częścią ludzi 12 lat temu
    * automatycznie zaczął się kręcić profit
    * wśród górników Elsa Kułak została nieformalną liderką. Niestety, "demokracja" nie przypasowała Baranowi. Odizolował się.
* Migracja Mardiusa
    * noktiańscy komandosi Tamary Mardius złożyli tu swój nowy dom wśród lapicytu. Podeszli do górników i Barana z neutralnością. Ot, są.
    * mało kto jak noktianie umieją żyć w kosmosie. W tym miejscu znaleźli świetne miejsce i ich umiejętności stały się bardzo przydatne.
    * siły Mardius znalazły opcję szmuglowania przez statek Gwiezdny Motyl.
* Imperium Blakvela
    * 3 lata temu pojawił się eternijski szlachcic, Ernest Blakvel. Stwierdził, że władza w kosmosie to coś dla niego. Zaczął umacniać się w tym terenie.
    * Blakvel i Mardius weszli w stały konflikt. Mardius chroni miejscowych, Blakvel chce ich podbić.
* Pojawienie się Strachów
    * 2 lata temu pojawiły się Strachy. Stworzone z programowalnej materii (morfelin), nie wiadomo czemu są i czym są.
    * Mardiusowcy aktywnie chronią lokalnych.

#### Strony i czego pragną

* Górnicy w Domenie Ukojenia i mały biznes: 
    * CZEGO: zachować niezależność, handel itp. Przetrwać. Zarobić.
    * JAK: sami się zbroją
    * OFERTA: standardowa oferta terenowa, plotki itp.
    * KTOŚ: Elsa Kułak (przełożona, trzyma twardą ręką), Antoni Czuwram (górnik, solidny stateczek i niezły sprzęt - dużo wie), Kamil Kantor (szuka ojca Leona który odkrył sekret Mirnas i zaginął), Bruno Baran (mag eternijski i założyciel; Strachy to destabilizacja Blakvela przy obecności lapizytu), Kara Szamun (młódka z rodziny Szamun, pilot i nawigator transportowca na Asimear.)
* Przemytnicy Blakvela: 
    * CZEGO: zdobyć prymat w Domenie Ukojenia
    * JAK: zmiażdżyć Mardiusa, potem przejąć Talio i zestrzelić bazę Barana.
    * OFERTA: świetne rozeznanie w pasie, największa siła ognia, wiedza o SWOICH skrytkach, wsparcie piratów etnomempleksu Fareil (synthesis oathbound pro-virt, pro-numerologia)
    * KTOŚ: Ernest Blakvel (arystokrata eternijski)
* Przemytnicy Mardiusa: 
    * CZEGO: zregenerować siły, odzyskać Tamarę, ukryć się przed Blakvelem
    * JAK: w ciągłej defensywie, ufortyfikowanie Sarnin, operacje militarne anty-Blakvel
    * OFERTA: wiedza o Miragencie i Aleksandrii, świetne rozeznanie w pasie, wiedza o SWOICH skrytkach, znajomości wśród lokalnych.
    * KTOŚ: Tamara Mardius (eks-komandos noctis, Alexandria), Deneb Ira (regeneruje komandosów po sprawie z Aleksandrią)
* Taliaci: (mieszkańcy Talio)
    * CZEGO: rzeczy wysokotechnologiczne
    * JAK: handel, współpraca
    * OFERTA: mają dostęp do WSZYSTKICH frakcji i są wiecznie neutralni, reaktory termojądrowe na podstawie deuteru i trytu na Talio.
* Strachy
    * CZEGO: Brak woli. Reakcja. Zniszczyć wszystko.
    * JAK: flota inwazyjna
    * OFERTA: zniszczenie, targetowane lub nie.

### Co się wydarzyło przed tą sesją

* Piraci z niewielkiej jednostki (około 15-20 osób) zaatakowało Arinkarię dla jeńców, żywności etc
* Dorośli zostali w większości odizolowani
* Piraci nie chcieli zabijać arinkarian; jednak dzieciaki bronią swojej bazy jak mogą

### Co się stanie

* .

### Sukces graczy

* .

## Sesja właściwa
### Scena Zero - impl


* Q: Arinkaria to niewielka stacja górnicza. 100 osób czy coś. Dlaczego dzieciaki, zwłaszcza na stacji?
* A: Odłam - chcą dzieci przy sobie, nie ufają, nie wierzą. Tu po prostu im lepiej.
* Q: Dlaczego ten odłam ludzi uważa się za prześladowany lub jest prześladowany?
* A: Pewne modyfikacje drakolickie do przystosowania nie będąc drakolitami - nie mają dla siebie miejsca
* Q: Czemu Romka - z innej przecież gliny / bioformy - jest z nimi na Arinkarii? I czemu jest akceptowana?
* A: Wcisnęła się do tej grupki i odleciała z nimi. Nie wie gdzie ich prawdziwa rodzina, zaklimatyzowała się.
* Q: W jaki sposób na stacji górniczej dzieciaki znalazły "bezpieczne" miejsca do poruszania się i szczurzenia.
* A: Są tam miejsca, dzieciaki zawsze COŚ znajdą. A co z tego wyjdzie - inna historia.
* Q: Kto jest najnowszym dodatkiem do Arinkarii.
* A: Tomasz Krałęga "Stary Tom", mechanik. Młody facet o starych oczach.

### Sesja właściwa - impl

Ekspedycja dzieciaków w mniej zamieszkałych fragmentach Arinkarii. Budują bazę. Kaja montuje by baza była budowana itp. Ma ciekawy sprzęt. Romka chce zobaczyć jak to działa. Albert "flirtuje" z Romką. Bartka nie ma. Ogólnie, Arinkaria to stacja wpisana w asteroidę. Kaja nadzoruje budowanie "domku na drzewie". Romka montuje torch na ścianie i poluzowuje na lince by zrzucić na Alberta - niech jej nie ciągnie za włosy itp. Romka to nie zabawka - mimo że egzotycznie wygląda.

* Tp+3:
* V: pochodnia spada prosto na Alberta i powoduje lekki popłoch chłopaka. Śmiech innych.
* V: Albert ma zrozumieć że to Romka i ma się odfasolkować. Jak Romka go mija "mogłam zostawić zapaloną" cicho.

Stacją zatrzęsło. Parę rzeczy spadło, przewróciło się. Po chwili jeszcze raz coś uderzyło. Komunikacja padła. Kaja zostawiła Alberta z dzieciakami i sama z Romką idzie szukać co się dzieje. Kaja zbiera komunikatory nie mówiąc o co chodzi - jeśli jest najazd, niech nikt nie może zlokalizować dzieci. Albert zostaje i pilnuje dzieciaki. Kolejny wstrząs.

Kaja -> Albert "zbudujcie zabezpieczenie, bazę na serio gdyby tunel padł". Uszczelnienia. 

Kaja i Romka na zwiady. Najbliższe okolice - czyste. Dziewczyny wybierają schowane tunele, górnicze schowane tunele na Arinkarii. Nie wiadomo co się stało, więc...

ROMKA ZWIADUJE:

* TrZ+2:
* V: Z szybu wentylacyjnego kopalnianego Romka dostrzega faceta w czarnym pancerzu. Przechodzi z bronią.
* Xz: Udało się minąć kolesia i dziewczyny idą cicho w kierunku na magazyn (jedzenie, powietrze itp). I usłyszały "tam są". Ktoś idzie bezpośrednio w waszym kierunku.
* Kaja zrzuca komunikatory do szybu - po nich trackują - i odciąga Romkę (+1Vg) X: Kaja i Romka są odcięte. Mogą wiać tylko do szybu ale gdzie komunikatory?
* Romka pozoruje "granat w pudełku" (kapiszon), komunikatory jako przynęta i do szybu. (+2Vg) V: dziewczyny schowały się w szybie, dwóch żołnierzy deliberuje.
* V: "granat" ściąga dużą uwagę, kilka osób się zbliża, "rozminowuje". Uwaga piratów jest skupiona tu. Dziewczyny pełzną szybem.
* X: szyb prowadzi w bardzo, bardzo stromą stronę. Jest bardzo niebezpiecznie się tam dostać. Z góry słyszą ciche syknięcie "ćś". Bartek.
* X: Kaja podsadza Romkę, ale nie da się wejść. Dziewczyny słyszą coś w stylu drony za sobą. Coś tu leci. Szybem, za nimi. Bartek zrzuca linę.
* V: Romka z prędkością wiewiórki DO GÓRY, zrywa Bartkowi komunikator, zrzuca w CZYMŚ na dół i z Kają ciągną Bartka i pełzną szybko do przodu.

Dziewczyny i Bartek zgubiły dronę. Bartek poprowadził dziewczyny przez jeden z korytarzy do czegoś co wygląda jak... świątynia? Coś między świątynią i siłownią. No cholera tak. Ale taka... biedna. Taka... świątynia HULKA HOGANA. Romka włazi na rzeczy. Bartek ma agresora - zepchnął Romkę. Kaja uspokoiła Bartka, Bartek wszystkich uzbroił (ma tu sporo noży, też wibroostrza). Bartek przyznał, że jego ojciec wie o tym miejscu. Ale nie ma tu nic komunikacyjnego.

Zespół zebrał konserwy, przenośnego kopacza, zero zdjęć wrestlera, parę noży i wracają do bazy.

Plan B: w Arinkarii znajdują się miejsca niebezpieczne gdzie nie wolno chodzić. Romka usuwa znaki ostrzegawcze. Kaja ekstraktuje elektronikę, komunikatory awaryjne by zrigować sygnał. I Romka to potem porozstawia.

Romka znajduje "bezpieczne" miejsce w rozumieniu niebezpiecznego miejsca na pułapkę. "Ulubione" miejsce Romki gdzie prawie maszyna ją wciągnęła z ubraniem.

ExZ+3:

* V: Miejsce jest gotowe i się nadaje. Da się wciągnąć tam pirata czy dwóch.
* X: Pułapka musi być odpalona ręcznie. Czyli ktoś musi być niedaleko.
* V: Mamy GWARANCJĘ, że jeśli będzie przynęta to uda się wprowadzić tam co najmniej 1 pirata. Umieszczono w tle taką subtelną lampkę z bazy Bartka.

Kaja wyplątuje i moduje odpowiednio komunikator i przynętę. Oraz maszynę ciężką. Nie tylko komunikator ale też w trybie odsłuchu - dzieciaki próbują się dowiedzieć co i jak.

TrZ+2:

* V: Zrigowany i zmodowany komunikator.

Faktycznie, kilka minut później po umieszczeniu wszystkiego dwóch piratów nadchodzi ostrożnie. Kaja się chowa najlepiej jak umie by to odpalić.

* V: Kaja się ukryła. Tzn. znajdą ją, ale po wpadnięciu w pułapkę.
* X: Wynik jest wystarczająco imponujący. Jeden pirat wpadł w maszynę krzycząc przeraźliwie a drugi stanął jak wryty.
* V: Bartek wpadł mu na plecy z całym pędem i wepchnął go w pułapkę.

...po czym Bartek puścił pawia. Nie jest to ładny widok. Dwóch piratów jest co najmniej mocno rannych w maszynie.

* X: Jeden pirat nie żyje. Drugi wzywa pomocy. Dzieciaki uciekają - nie ma jak wykorzystać tej sytuacji lepiej.
* V: Bartek DAŁ RADĘ się opanować, ale cały drży. Trzyma się tym, że musi chronić dziewczyny.

Jesteście w bezpiecznym terenie. Trzech piratów pobiegło ratować rannego przyjaciela. Zespół przegrupował się w bazie.

Kaja: Bartek, chroń dzieciaki (Feliksa i Jarka). Albert i Romka - my wyłączamy światło. NIE! Albert ma zamknąć śluzę by odciąć tych piratów ratujących kolegę. Trzeba go dobrze przeprowadzić.

Kaja wykorzystuje samobieżną / nakręcaną zabawkę Feliksa, by wyglądało że to jest potencjalna bomba. Romka umieszcza zabawkę i ją odpala. Albert w tym czasie zamyka śluzę i odcina pięciu piratów w nieużywanej sekcji bazy gdzie nie ma nic ważnego.

TrZ+3:

* V: Romka skutecznie znalazła drogę i dostała się tam gdzie powinna i wprowadziła Alberta.
* V: Nikomu nic się nie stało, dywersja udana
* X: Piraci widzą, że mają do czynienia z dzieciakami - Romka i Albert.
* X: Jednemu z piratów uda się wycofać. Maks. czterech zamkniętych.
* V: CZTERECH odciętych, jeden się przebił, przebiegł przez śluzę. Czyli: Romka, Albert i Kaja i pirat są koło siebie.

Albert widząc szczupakującego pirata panikuje i atakuje wibroostrzem. Kaja korzystając z okazji poluje na broń pirata by zmusić go do poddania.

TrZ+2+2O:

* O: Albert skutecznie ciął wibroostrzem raniąc pirata przy ziemi (+2Vg)
* X: Pirat odepchnął Alberta, przetoczył się i Albert skończył pod ścianą bez broni
* Vz: korzystając z zaskoczenia, Kaja wzięła i wycelowała w pirata z jego własnego pistoletu (+2Vg)
* V: Pirat... niedowierzanie. Ale zaprzestał walki. Poddał się. Słyszycie tylko pojękiwanie Alberta. Piratowi cieknie krew z boku. Pirat usiadł, krwawi z boku.

Pirat: "Poddaję się waszej trójce dzieciaków. Ale tylko dlatego że macie pistolet."

Kaja spodziewa się komunikatora; zmusiła pirata do rzucenia komunikatora. Zdjął hełm i rzucił. Kaja wiedząc, że koleś dostanie wsparcie do 5 minut, każe piratowi iść przed siebie (gdziekolwiek). By opóźnić. Kaja -> Romka "powiedz głośno pułapka gotowa".

Koleś pirat zamknięty za kolejną śluzą.

Albert wprowadza specjalne kody - kody blokujące śluzę na 30 minut. Ma na to uprawnienia.

TrZ+2:

* Vz: master blocker działa.

A tymczasem Romka z pistoletem pełznie po niebezpiecznych tunelach szukając Koraliny. Wpierw musi przedostać się przez "ścieżkę zdrowia" - tamte tereny są bardziej niebezpieczne. Lepiej się nie spieszyć.

TrZ (sprzęt - zaczepy itp.) +2:

* X: niestety, poobciera się, porani lekko
* Vz: dzięki sprzętowi Romka zabezpieczyła sobie "nadbazę". Tereny sufitowe.
* V: Romce udało się dostać do zewnętrznych fragmentów bazy. Tereny gdzie znajduje się konstrukcja i naprawa sprzętu ciężkiego. Głośne, niewygodne, trzęsie sie - piraci zabezpieczyli, wystawili warty i "nasi" nie są w stanie dostać się do środka. Romka jednak ich znalazła. Lina i "mission impossible" Romka style.

Bruno (jeden z inżynierów) się ucieszył na widok Romki. Po raz pierwszy ever (Face | Odbudować reputację). Bruno chciał by Romka wzięła granat makeshiftowy i wysadziła piratów przy śluzie. Leszek się nie zgadza bo dziecko. Romka zaproponowała - niech oni robią hałas, ona pistoletem zmusi ich do poddania. Leszek się zgadza. Bruno niechętnie też się zgadza. Będzie hałas i pozorny szturm.

Romka wraca do korytarzy i liczy na to, że hałas pozwoli jej się dostać niezauważenie.

TrZ+4:

* V: Romka przepełzła tunelami górnymi i dostała się na plecy dwóch piratów przy śluzie.
* Vz: Dzięki hałasowi NIC nie widzieli. Romka dostała się im na plecy. Ma broń i wygląda... wystarczająco groźnie. "RĘCE DO GÓRY".
* X: Jeden pirat się rzucał. Chciał zaatakować Romkę. Dostał. Jest ranny. (+Vg)
* X: Drugi miał więcej szczęścia - rozbroił Romkę. I bije.
* V: Romka dostała pomniejszy wpierdol, ale zaraz pirata zdjął z niej Leszek i z basi. Piraci po prostu pobici kluczami itp. 

Bruno do Romki "nic Ci nie jest?". Romka obita. Warga pęknięta. Posiniaczona. Obolała. Ale ogólnie działa. Musi chwilę poleżeć. Ale inżynierowie mają broń. Teraz już trzy sztuki. Plus jest tam jeszcze 3 inne osoby - czyli mamy grupę sześciu dorosłych, trzech uzbrojonych i 2 piratów mniej.

OGÓLNIE: 1 pirat nie żyje, 1 za śluzą, 4 za śluzą, jeden ciężko ranny, jeden tutaj ranny, jeden pobity przez inżynierów -> 9 piratów unieszkodliwionych. Potencjalnie nie może ich być więcej niż 11. Potencjalnie dużo mniej. Piraci, przesłuchani, przyznali, że jest ich 14. Zostało 5 w czym pilot (kapitan) grzeje statek. Czyli jest czterech. Tylko czterech piratów.

No to teraz sytuacja się odwróciła.

Dzieciak zrobił swoje, teraz dorośli rozwiążą sprawę.

W wyniku negocjacji:

* załogantom stacji nic się nie stało (Arinkarii nic nie jest)
* piraci musieli oddać wszystko co nakradli plus reperacje ze statku
* piraci mogli odejść wolno - ale nie mają wracać
* ogólnie - nie skończyło się tak źle jak mogło

## Streszczenie

Piraci (Ostrza Fleszera) uderzyli w Arinkarię, infiltrując ją i przejmując bez większych strat po obu stronach. Ale dzieciaki Arinkarii skutecznie wykorzystały teren i obszary niebezpieczne by część piratów poodcinać a część wprowadzić na niebezpieczne miejsce i pozabijać. Dzięki ich działaniu, dorośli byli w stanie wynegocjować opuszczenie przez piratów Arinkarię i reperacje od tych piratów.

## Progresja

* .

### Frakcji

* .

## Zasługi

* Romana Kundel: 14, nieco nie pasuje do innych arinkarian (nie ma modyfikacji drakolickich); bardzo cicha i wszędzie się wślizgnie w szczeliny Arinkarii. Eksploruje Arinkarię, zna niebezpieczne miejsca i wciągnęła piratów w pułapkę. Potem przeniosła pistolet do inżynierów. Ogólnie, "duch Arinkarii".
* Kaja Czmuch: 15, zmodyfikowana drakolicko; "odpowiedzialna" dziewczyna Arinkarii opiekująca się innymi. Zmodowała komunikatory i dowodziła dzieciakami atakując piratów. Ściągnęła piratów w pułapkę elektroniką, dając sygnały gdzie są.
* Albert Rybowąż: 16, zmodyfikowany drakolicko; podrywa Romkę (bo jest egzotyczna); odpowiedzialny wpierw za ochronę dzieci przy ataku piratów a potem kontrolował śluzy by piraci nie wyszli. Zaatakował wibroostrzem pirata i solidnie wylądował na ścianie. Uczy się by być inżynierem i maintainerem bazy.
* Bartek Wudrak: 15, zmodyfikowany drakolicko; właściciel "świątyni wrestlera Tytana", ma kolekcję noży. Wyciągnął dziewczyny z dziury, zaprowadził do swojej kryjówki i dał broń. Potem - wepchnął pirata do maszyny przetwarzającej i nerwowo nie wytrzymał (pierwszy raz kogoś zabił); oddelegowany do ochrony dzieci.
* Adam Wudrak: 36, zmodyfikowany drakolicko operator maszyn ciężkich i górnik; rozpieszcza swojego syna (Bartka) i zmontował mu "Świątynie Wrestlera Tytana" dyskretnie w nieaktywnym już fragmencie Arinkarii.
* Bruno Wesper: 44, zmodyfikowany drakolicko inżynier nienawidzący piratów i nie mający skrupułów by ich załatwić szrapnelem. Odciągnął pirata walącego Romkę w twarz i dał piratowi kopa w twarz. Zależy mu na prestiżu i reputacji.
* Koralina Szprot: 34, zmodyfikowana drakolicko agentka ochrony Arinkarii. Wredna jak cholera i z podłym charakterem. Podczas ataku piratów była odcięta w sektorze inżynieryjnym i niewiele mogła zrobić.

### Frakcji

* Ostrza Fleszera: niewielka grupa piratów w okolicach Arinkarii która próbowała szczęścia i zaatakowała Arinkarię, by doznać strasznych strat i zostać odepchniętym przez dzieciaki. 

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Pas Teliriański
                1. Planetoidy Kazimierza
                    1. Domena Ukojenia
                        1. Stacja Górnicza Arinkaria: wysunięta stacja wbudowana w planetoidę, 100 osób, jak "w kopalni", zamieszkana przez zmodowanych drakolickimi metodami nie-drakolitów. Bardzo dużo tuneli i zakamarków. Miejsce ciemne i ogólnie ubogie, choć sprawne. Niesprawna TAI.

## Czas

* Opóźnienie: -1214
* Dni: 1

## Konflikty

* 1 - Romka montuje torch na ścianie i poluzowuje na lince by zrzucić na Alberta - niech jej nie ciągnie za włosy itp.
    * Tp+3
    * VV: Albert rozumie że to Romka, odfasolkował się. Ogólny śmiech innych, że popłoch bo torch na niego spadł
* 2 - Kaja i Romka na zwiady. Najbliższe okolice - czyste. Dziewczyny wybierają schowane tunele, górnicze schowane tunele na Arinkarii. Nie wiadomo co się stało, więc Romka robi zwiad
    * TrZ+2
    * VXz: Romka dostrzegła nieznanego żołnierza, ale też została wykryta przez kogoś. Kaja usunęła ich komunikatory.
    * XV: dziewczyny schowały się w szybie; są odcięte ale ukryte i przeciwnicy boją się pułapki (granat czy coś)
    * V: dziewczyny pełzną szybem; piraci są zajęci "granatem".
    * XX: szyb niebezpieczny, Bartek zrzucił im linę, przeciwnik ma dronę
    * V: dziewczyny usunęły komunikator Bartka i zmyliły dronę. Mają ukryte miejsce.
* 3 - Romka znajduje "bezpieczne" miejsce w rozumieniu niebezpiecznego miejsca na pułapkę. "Ulubione" miejsce Romki gdzie prawie maszyna ją wciągnęła z ubraniem.
    * ExZ+3
    * VXV: pułapkę trzeba ręcznie odpalić, gotowe miejsce na piratów i na pewno się uda po złożeniu innych elementów.
* 4 - Kaja wyplątuje i moduje odpowiednio komunikator i przynętę. Oraz maszynę ciężką. Nie tylko komunikator ale też w trybie odsłuchu - dzieciaki próbują się dowiedzieć co i jak.
    * TrZ+2
    * VVXV: Kaja ściągnęła piratów i się ukryła, jeden pirat wpadł w maszynę a drugiego musiał Bartek weń wepchnąć wpadając mu z impetem na plecy.
    * XV: jeden pirat nie żyje, drugi "SOS", dzieci uciekają. Bartek w ruinie psychicznej ale jakoś działa.
* 5 - Albert w tym czasie zamyka śluzę i odcina pięciu piratów w nieużywanej sekcji bazy gdzie nie ma nic ważnego
    * TrZ+3
    * VVXXV: Piraci wiedzą że walczą z dzieciakami, ale czterech zablokowanych za śluzą. Jeden się wydostał.
* 6 - Albert widząc szczupakującego pirata panikuje i atakuje wibroostrzem. Kaja korzystając z okazji poluje na broń pirata by zmusić go do poddania.
    * TrZ+2+2O
    * OXVzV: Albert ZRANIŁ pirata, dostał odeń wpierdol, Kaja wycelowała w pirata z pistoletu i koleś się poddał.
* 7 - Albert blokuje śluzę na 30 minut; awaryjne kody a on uczy się na kolesia od inżynierii i maintenance bazy.
    * TrZ+2
    * V: master blocker działa
* 8 - A tymczasem Romka z pistoletem pełznie po niebezpiecznych tunelach szukając Koraliny. Wpierw musi przedostać się przez "ścieżkę zdrowia" - tamte tereny są bardziej niebezpieczne. Lepiej się nie spieszyć.
    * TrZ+2:
    * XVzV: poobcierana, poraniona, ale dotarła do 'nadbazy' i dostała się inżynierów i fragmentów ze sprzętem ciężkim, odciętym przez dwóch piratów za śluzą z bronią.
* 9 - Romka wraca do korytarzy i liczy na to, że hałas pozwoli jej się dostać niezauważenie.
    * TrZ+4
    * VVzXXV: Romka z zaskoczenia wzięła piratów i zmuszając ich do poddania odwróciła ich uwagę od atakujących inżynierów. Dostała WPIERDOL, ale nic bardzo poważnego jej się nie stało.

## Kto tu jest
### Dzieciaki

* Bartek Wudrak
    * rola: delinquet teen, nożownik, aspiruje do bycia górnikiem, 15
    * personality: low-agree, low-cons, low-extra, open (spontaniczny, cichy, nie planuje, 0->100 i atak)
    * values: security, community
    * wants: nikt mnie nie skrzywdzi, wolność od wszystkiego, chronić swoich, Romana ;-)
* Romana Kundel
    * rola: złodziejka, zna teren, 14
    * personality: low-agreeable, open, low-extra (wycofana, lekko socjopatyczna, otwarta i chętna próbowania innych rzeczy)
    * values: hedonism, self-direction
    * wants: nikt mi nie będzie rozkazywał, kasa na virt lub kryształy, lepsze życie
* Kaja Czmuch
    * rola: koordynatorka dzieciaków, inklinacje techniczne, 15
    * personality: 0-00+ (Beztroski i swobodny, Nie umie usiedzieć na jednym miejscu długo, ciekawska)
    * values: Conformity, Security
    * wants: nie chcę żyć niezauważona - dokonać WIELKICH CZYNÓW
* Jarek Muszcz : 13, 
* Feliks Muszcz : 13, 
* Albert Rybowąż : 15

### Dorośli

* Filip Zartamon, dowódca Arinkarii
    * Wartości
        * TAK: Security, Power, Face
        * NIE: Stimulation, Self-direction
    * Ocean
        * ENCAO:  -+0-0
        * Obserwator; raczej stoi z boku i patrzy niż działa bezpośrednio
        * Skupiony przede wszystkim na pieniądzach i korzyściach materialnych
        * Ostrożny i powściągliwy
    * Silniki
        * TAK: Złamać Strachy, zdominować: pokazać komuś że to JA naprawdę mam władzę i beze mnie tamta osoba nie ma niczego i nic nie znaczy.
        * TAK: Lokalny bohater: to ja jestem bohaterem w tej grupie.
        * NIE: Harmonia i akceptacja stanu rzeczy
* Adam Wudrak, Operator maszyn ciężkich i górnikowiec
    * Wartości
        * TAK: Benevolence, Self-direction, Tradition
        * NIE: Security, Achievement
    * Ocean
        * ENCAO:  0--+-
        * Z natury odrzuca wszystkie pomysły, trzeba powoli przekonywać
        * Szczery i nie ukrywający nic przed innymi; uczciwy
        * Nieodpowiedzialny; nie można niczego powierzyć
        * Wielkoduszny, wzniosły. Kieruje się zasadami.
    * Silniki:
        * TAK: Wolność od społeczeństwa: nie będę od nikogo zależeć. Żyć off the grid. Nie podlegać żadnym prawom, iść własną drogą.
        * TAK: Carmen Sandiego: oddać skarby / relikwie tam, skąd przybyły. Naprawić szkody i krzywdy spowodowane przez grabież.
        * NIE: Gliniarz w przedszkolu: jak ryba poza wodą; ogromny szok kulturowy i brak autorytetu i brak metod opresji by autorytet wymusić. Znaleźć swoje miejsce.
* Marysia Czmuch, "medical", mama Kai. Oddział szybkiego reagowania
* "Stary Tom", mechanik
* Koralina Szprot, szybkie reagowanie + security, WREDNA JAK CHOLERA
* Marcel Muszcz : ops & admin
    * Wartości
        * TAK: Self-dir, Family
        * NIE: Tradition
    * Ocean
        * ENCAO: 000+-
        * Słodki, lekko naiwny
        * Racjonalny, szuka wyjaśnień w istniejącej rzeczywistości
    * Silniki:
        * TAK: Wyplątać się z demonicznych długów
    * Rola:
        * 


*  
    * Wartości
        * TAK: 
        * NIE: 
    * Ocean
        * ENCAO:
    * Silniki:
        * TAK: 
        * TAK: 
        * NIE: 
            * interpretujemy jako: 
    * Rola:
        * 


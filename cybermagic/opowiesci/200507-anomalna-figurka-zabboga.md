---
layout: cybermagic-konspekt
title: "Anomalna figurka Żabboga"
threads: dzien-z-zycia-terminusa
gm: żółw
players: fox, kapsel, kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [180817 - Protomag z Trzęsawisk](180817-protomag-z-trzesawisk)

### Chronologiczna

* [180817 - Protomag z Trzęsawisk](180817-protomag-z-trzesawisk)

## Budowa sesji

### Stan aktualny

.

### Dark Future

* .

### Pytania

1. .

### Dominujące uczucia

* .

## Punkt zerowy

### Postacie

* Jan Łowicz: Jason Hunt: objęty i przeprojektowany przez Kapsla.
* Natasza Aniel: Nesta Allen: objęta i przeprojektowana przez Fox

### Opis sytuacji

Anomalna Figurka Żabboga jest anomalią o następujących własnościach:

* potrafi zniszczyć i zakłócić informacje w dokumentach, zarówno na kompach jak i w realu
* ALE: by działała, musi być ukradziona (i to wbrew woli kradnącego).
* ALE: by działała, osoba jej używająca nie może wiedzieć co owa figurka robi.

Tukan chce zdobyć tą figurkę, jako potężną broń "na kiedyś". Nie wie czy użyje jej przeciw Kajratowi, Grzymościowi czy Pustogorowi - ale chce to mieć dla siebie i by nikt nic nie wiedział.

## Punkt zero

.

## Misja właściwa

* Natasza audio -> Natasha
* Melinda audio -> Melinda
* Kinga audio -> Kelly
* Janek audio -> John

A standard day in the shop. John is on guard duty, Natasha is selling her wares. John has noticed that there is someone trying to steal a very ugly figurine.

Well, the door can be closed automatically, so John simply waited until the thief started to sneak out and he disabled the door opening. Then John started interrogating the thief – what was even thinking? He seems to be the worst thief in the galaxy.

The thief cracked quickly. He told John everything – he was trying to steal a figurine for Melinda. Melinda asked the thief to get a particular figurine, with a photo and everything. Now John was surprised. But Melinda is his problem, therefore he has to be the one to solve it.

So John went outside the shop, across the street, to confront Melinda. Upon seeing John, the young aristocrat shouted, bemused – he has ruined everything. He shouldn’t have brought the figurine to her to give it as a gift. John was slightly bemused. He did not want to give the figurine to Melinda as a gift at all.

Well, Melinda said that she is able to steal it again and this time she will do it better. John told her she has managed to hire the most incompetent thief in existence. Melinda countered that the figurine was worth 50 credits and she paid the thief 500 credits, therefore he had to be efficient.

John saw absolutely no logic in that reasoning.

John tries to interrogate Melinda slightly, why did she even wants to buy a figurine? Was it to impress him? Melinda told him that impressing him would be nice, and that he could teach her so much about being a lowlife and everything, but she needs to steal the figurine because otherwise it will not work.

John didn’t really want to press the matter – he didn’t want to give Melinda anything, but knowing she expects the figurine to have some magical properties he took it back to the shop (leaving Melinda completely disappointed) and gave it to Latasha asking her to find any information on the figurine.

Natasha was completely up to the task. (Extreme). She has managed to piece a set of information from the books and she has learned about very interesting properties of this ugly, toady-like figurine:

* This figurine is not an artifact. It is an anomaly. It has been induced and created in the medical field, not created by magician’s hands
* This anomaly is able to destroy knowledge. Connected with any document or with any computer containing knowledge it will scramble information and drop the knowledge at all. So it is an ultimate menace to any library.
* This anomaly works only if it is stolen without consent. On top of that, it will only work if a person using it is unaware of its properties. This kind of means the team is unable to ever use it from now.
* The knowledge about this anomaly is not popular, not many people know about it. Obvious reasons. This also concerns Magi.
* Sadly, John is aware Melinda will not stop trying to get it. She can be persistent like that.
* On top of that, there is some other force behind Melinda – that force will try to steal the anomaly and as long as this anomaly is in the shop, there will be attempts to steal it or something else.

Well, this is quite unfortunate. The team wanted to get information on the perpetrator – who is behind Melinda, who is trying to get this anomaly for themselves. Kelly got a sick idea – what if she creates a lot of fake figurines, what if she makes a perfect forgery and imbues truckers inside them?

Because this is something connected with magic, Kelly had to use some special materials and it was much more difficult than usual (Extreme). But she has managed to achieve her goals:

* The forgery will be reliable – it will not be detected by normal people
* The forgery is double reliable – not even Magi will be able to detect whether the figurine is a fake one or real one, fearful of damaging the original anomaly
* Every fake figurine contains a tracker inside. That way they are able to get to the root perpetrator. Also, tracker will not be found.
* Unfortunately, while building this grand forgery, the information about it spilled. Every interested party knows that the origin of the forgeries came from the Phantasmagoria
* Also, Magi will know that there used to be an original anomaly in the Phantasmagoria at one point

Well, this is less fortunate, because it connects the members of phantasmagoria to the anomaly. And then Natasha had a diabolical idea: what if she scrambles the information on the figurine using fake news and gossip? That way no one will ever be able to determine which one is the fake, which one is the real one, and how to truly use it. This will also allow her to give phantasmagoria plausible deniability.

Natasha used Katja as her contact. This, thus using the sharks gang as a information dissemination tool should be enough to allow them a success.

* Indeed, no one knows anything. The chaos is real.
* This means that Tukan (the perpetrator) is unable to use the figurine – he is not able to determine which parts of it are true and which are false.
* On top of that, Natasha created an incredible demand for those ugly figurines. All those fakes Kelly created will be eventually sold or stolen – even as fakes, because Natasha doesn’t mind selling fakes as long as people accept they are fakes (of course, she doesn’t mind people buying fakes as originals either; she just doesn’t want to have problems with the law)
* Unfortunately, some of the fakes Kelly forged and Natasha sells as originals will be detected as fakes by the prospective buyers. But Natasha being a human has a plausible deniability, therefore there will be no problems with the law.
* And as a cherry on top, the terminus who will deal with this case will be 100% sure phantasmagoria is innocent. Because it is inconceivable that a group of humans would create such a plan and would use such a set of artifacts.

So the members of phantasmagoria are happy - a potential problem connected with Melinda stealing the artifacts was turned into profit. Still, the question remains – why and who is trying to set Melinda up? Because she is not the brightest clam in the sea.

John decided that he has to get that information. It would be so much easier if Melinda cooperated, or at least if she did what he wanted. And so John devised a perfect plan. He will meet Melinda in a neutral place, and Natasha will accompany him with a fake figurine in her handbag. Then John would tell Melinda that if she wants to impress him and have a dinner with him, she needs to steal the figurine and drive Natasha away. 

In John’s mind, Melinda will manage to steal the figurine (because Natasha lets her) and will not be able to drive Natasha away. That way Melinda Gates what brings John to the real perpetrator and John will not have to spend the evening with her.

Of course, the reality was completely different.

When Melinda came for the meeting with John and Natasha, she was determined to win. So when John gave her his bargain, Melinda smiled John triumphantly and went to talk with Natasha. When Melinda and Natasha were alone, Melinda suggested Natasha trade – she would give Natasha some good contacts in Aurum where Natasha can sell some wares. In exchange, Natasha helps Melinda and get John.

Natasha, beaming, accepted Melinda’s offer gracefully.

To steal the figurine, Melinda asked exactly the same thief she asked earlier. So when Natasha was returning home, she noticed she is being robbed by an inept thief. Of course, she accepted being robbed; that was kind of needed for the plan to function. At this gave Melinda a chance to gloat to John that he was wrong and she has hired a competent thief. 

John didn’t appreciate that. Neither did he appreciate doublecrossing from Natasha. But there wasn’t much he could do at that point – he went for the dinner with Melinda.

During the dinner John tried to extract information from Melinda – why was she doing all that? She is an aristocrat, she is her age, she doesn’t have to steal a figurine of do those stupid things. Melinda explained with dedication and devotion that she is trying to help someone.

There was that guy, Gustav. Melinda found him when he was kicked out of the bordello because no one wanted him to sell his body for pleasure. Gustav was very desperate – he bought some equipment and the equipment got stolen from him. Then he got a shark loan at „Nadgranica”; and what happened was that as a collateral he has to give his grandparents’ fortifarm.

But he had no right to put that fortifarm as a collateral. And now the loan shark company wants their money or the collateral. And he has no way to get money.

So Melinda decided that the best way to solve this problem was to scramble the debt. To destroy all the records at the loan shark company. And to achieve that she wanted to use the anomalous figurine.

John did not expect Melinda to act like that. Up to this point she acted like a spoiled rich kid, he didn’t expect her to be mindful of anyone else. Her plan was stupid, but her heart was in the right place. So he tried to learn more about her – why doesn’t she help him using her own money? Why does she act like she does? Melinda tries to avoid the subject, but John was very successful in interrogating her.

Melinda explained that she is an aristocrat from a magical bloodline. But she is that daughter without any magical powers whatsoever. As she explained calmly, there are some bloodlines who consider their non-magical brethren to be worthy and they are trained to be good administrators and curators of the bloodline and their baronies.

Melinda’s family? Not so much.

Melinda is not deemed to be an asset. She is considered to be a flawed side effect, nothing useful, nothing to be proud of. So they have arranged a marriage for Melinda, if they marry her off, she will be at least of some use to the bloodline.

Well that was not exactly what Melinda had in mind about her future. Especially as her future husband had a reputation… to say.

Therefore Melinda ran away from home and decided she will try to live a better life. But her money is slowly dwindling, she does have many ideas what to do in the future, she doesn’t think she has any useful skills. She escaped with some young aristocrats who wanted to join the flying sharks gang in Podwiert, and now she’s here.

She can’t leave the city - he has no idea how is it outside its walls, she believes that there are monsters roaming everywhere and bandits and stuff. She doesn’t know anyone and she doesn’t know how to operate. Also, she is lonely.

But! Here is John – a perfect specimen of this region. He can teach her how to fight and he can help her escape from her abusive family. And this is really her endgame, at this point she has no better idea.

John was left speechless. He did not really expect an airheaded Melinda to have such an unusual story.

A young aristocrat used John’s weakness against him – she made him promise that he will help her if she has problems with her family always something happens to her. Also, he will teach her how to defend herself.

Sadly both for Melinda and for John, as John was the first person who really listened to her and wanted to help her, Melinda started having a crush on him – even to the point of falling in love with him.

Well, the dinner was cringy and horrible for John, but he has achieved his goal. Melinda is unaware of his machinations and he will be able to get information on who is the mage behind with animals figurine.

And he had found out who that mage was, after the mage stole the figurine from Melinda in a chain of events. The mage was a terminus – Tomasz Tukan.

After learning all that, the phantasmagoria sent the request to the terminus asking him to meet with them about the figurine. The terminus decided that the coincidence is impossible and that something is going on; he decided to meet with them and parlay.

And so several days later, terminus met with the phantasmagoria inside their shop. Tukan was prepared – he was ready to activate his power suit, he had some grenades and was prepared to level whole district if needed. But phantasmagoria’s team just wanted to talk to him and to bargain: the original figurine with original instructions how to use it for some things.

The terminus agreed to the trade:
* John really insisted that Melinda will be taken care of. The terminus was supposed to protect her and even help her get some money and recover some power inside her family. Well, this wasn’t something Tukan was very opposed to – he has always wanted to have a barony for himself.
* Phantasmagoria asked the terminus for protection of themselves as well. They are dealing with anomalous and they are dealing with problematic stuff, after all. So if there was something beyond their power, would he be able to help them? He agreed – he is a terminus, after all. He helps and protects.
* Natasha insisted that if he had some unworthy artifacts or unworthy anomalous, something not really dangerous and some think which could be given to humans, that he sold it to phantasmagoria. As in, if Tukan was to sell stuff anyway, why Nesta them? After some deliberation terminus agreed. It is not a bad trade, and he is not a good trader anyway.
* Also, some deals. If a terminus finds some contracts which can be made – please pass them to Phantasmagoria. So be it.
* As a cost, the terminus required them to get mind wiped about who he is. They know they have a deal with a terminus, but they don’t know with whom exactly. This was a hard one, but they agreed.

In the process of negotiations, Tukan got quite convinced that they are just a front for someone. There is some kind of a shadowy organization behind them, a new force, a third force - maybe from Aurum? That would explain Melinda’s presence. So Melinda might be a test.

So be it. Tukan is able to pass any test and if it gets him closer to be a noble - a powerful aristocrat - then he is willing to do a lot for that goal to be met.

## Streszczenie

Próba kradzieży figurki z Fantasmagorii doprowadziła Zespół do Melindy, która chce pomóc delikwentowi który stracił majątek. Janek decyduje się usunąć swoją stalkerkę raz na zawsze - ale jak poznał ją bliżej, zrobiło mu się jej żal. Okazało się, że owa figurka jest magiczną anomalią zdolną do niszczenia dokumentów. Kinga stworzyła jej fałszywki by nikt nie był w stanie ich użyć, a Natasza sprzedała Tukanowi możliwość użycia za ochronę i kontakty w Aurum.

## Progresja

* Melinda Teilert: jednak jest uboższa niż się zdaje; uciekła z domu przed aranżowanym małżeństwem i boi się, że ją znajdą i ściągną z powrotem. "Odpad rodu", bo nie jest magiem.
* Melinda Teilert: powiązana z gangiem Latających Rekinów; dzięki nim uciekła i dostała się do Podwiertu (gdzie nie radzi sobie z życiem).
* Melinda Teilert: zapewniła sobie ochronę Jana Łowicza + to, że on będzie uczył jej walczyć.
* Melinda Teilert: zauroczona i podkochuje się w Janie Łowiczu. Niestety dla niego.
* Tomasz Tukan: zobowiązany do ochrony Fantasmagorii, jej członków oraz do pomocy w odbudowaniu pozycji i bogactwa Melindy Teilert (plus ochrony).
* Tomasz Tukan: uzyskał Anomalną Figurkę Żabboga, dzięki której może zniszczyć dowolne dokumenty. Ale użyć może jej tylko ktoś kto nie wie jak działa i kto ją ukradł.
* Tomasz Tukan: jest absolutnie przekonany, że na terenie Podwiertu jest potężna ukryta organizacja (najpewniej z Aurum), która stoi za Fantasmagorią. 

### Frakcji

* .

## Zasługi

* Jan Łowicz: przechytrzony przez Melindę. Najpierw jej unikał, potem ją chronił, nawet załatwiając jej ochronę z Tukanem. Dobre plany, ale nie wytrzymały Melindy ;-).
* Natasza Aniel: odpowiednio pragmatyczna jak chodzi o kasę; wpierw wystawiła Jana w sprawie Melindy za kontakty z Aurum a potem załatwiła z Tukanem oddanie mu figurki.
* Kinga Kruk: stworzyła perfekcyjne fikcyjne figurki, niszcząc jakiekolwiek możliwości użycia oryginalnych przez kogokolwiek - i dodała tracker by odnaleźć Tukana w tle.
* Damian Polwonien: najgorszy złodziej na świecie; próbował ukraść figurkę ze sklepu (zatrzymany przez Janka) a potem ukradł ją Nataszy (która się zorientowała i pozwoliła na to)
* Melinda Teilert: nie jest tylko głupią stalkerką, wyrolowała Jana przez kooperację z Nataszą; okazuje się, że uciekła z domu przed aranżowanym małżeństwem (dzięki Rekinom) i próbuje stanąć na swoim. Chce pomóc słabszym.
* Gustaf Profnos: wytracił sporo pieniędzy; nie ma jak ich odzyskać i teraz firma Nadgranica może przejąć fortifarmę jego dziadków.
* Tomasz Tukan: stworzył nietrywialny plan zdobycia w ukryciu przed Pustogorem figurki niszczącej dane i dokumenty, by ostatecznie ją otrzymać w handlu z Fantasmagorią.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Podwiert
                                1. Osiedle Leszczynowe
                                    1. Sklep z reliktami Fantasmagoria

## Czas

* Opóźnienie: 10
* Dni: 7

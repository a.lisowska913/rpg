---
layout: cybermagic-konspekt
title: "Listy od fanów"
threads: legenda-arianny
gm: żółw
players: kić, fox
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [200609 - Sekrety Kariatydy](210609-sekrety-kariatydy)

### Chronologiczna

* [200609 - Sekrety Kariatydy](210609-sekrety-kariatydy)

### Plan sesji
#### Co się wydarzyło

* Aurum i Eternia próbują zrobić własną flotę kosmiczną. Ale nie do końca mają możliwości i środki.
* Jeden z pojazdów Eterni ("Lord Savaron") dotarł na K1. Przekupił siły Orbitera, by mógł tu przeczekać.
    * Przekupiona inspektor Gaja Kleszcz.
    * Na pokładzie "Lorda Savarona" znajdowały się dzieci, które miały być wyewoluowane w perfekcyjnych pilotów i strzelców.
* "Lord Savaron" zadomowił się na K1. Gaja doprowadziła do tego, by Orbiter czerpał z tego duże korzyści.
    * K1 solidnie zarabia w rzadkich eternijskich surowcach i środkach.
    * Savaron prowadzi eksperymenty i próby ewolucji, też mając wsparcie ze strony niektórych frakcji Orbitera.
        * najemnicy, perimeter, siły militarne
        * neutralizowane TAI, ŁZy itp.
        * wszystko jest w bardzo czarnych sektorach
        * ukryte działania radioelektroniczne
* Zofia nie jest pewna, czy powinno mieć to miejsce. Nie wie, czy powinna interweniować, czy to nie jest świat jakim powinien być.
    * Ale Zofia uznała, że jeśli tak powinno być, to przecież nie byłoby przekupstwa.
* Zofia wzięła część listów tych dzieciaków i zaszyfrowała je oraz wysłała do Arianny jako fanmail.
    * "Jeśli jesteś prawdziwa, chciałbym Cię jeden raz móc zobaczyć zanim mnie wyślą na front i zginę." <-- 12-letni Michał Teriakin.
        * Nazwisko wskazuje na eternianina. Lokalizacja listu wskazuje na Czarne Sektory Kontrolera Pierwszego, obszar niedostępny z normalnych sektorów K1.

#### Sukces graczy

* Odnaleźć tajną bazę Eternijską na Kontrolerze Pierwszym
* Odkryć gdzie jest Michałek. Uratować Michałka?
* Uniknąć problemów z kilkoma siłami Orbitera które korzystają z tej sytuacji.

### Sesja właściwa
#### Scena Zero - impl

.

#### Scena Właściwa - impl

Jak raz w tygodniu, z uwagi na Sekrety Orbitera na biurko Arianny trafia FANMAIL. Klaudia ustawia filtry na creepów, a innych przepuszcza. Infernia mają ogromne, ogromne powodzenie wśród młodych. "Ulubiony statek we flocie". Jedna to notka elektroniczna: "Jeśli jesteś prawdziwa, chciałbym Pani jeden raz móc zobaczyć żeby wiedzieć jak się nie bać zanim mnie wyślą na front i zginę". Niejaki Michał Teriakin. Wiadomość przyszła z K1. 

Klaudia skupia się na znalezieniu Michała Teriakina. Szuka stopniując. Grzebie coraz głębiej - kim on jest i gdzie jest.

* V: Klaudia doszła do tego, że Michał Teriakin na Kontrolerze Pierwszym nie występuje. Nie ma go w oficjalnych bazach. Notka wskazuje na to że istnieje - połączenie ze statkiem "Lord Savaron".
* XXX: Klaudia odbiła się od systemu bezpieczeństwa K1. Statek "Lord Savaron" jest utajniony z przyczyn bezpieczeństwa / ekonomii. --> to statek eternijski.

Arianna zdecydowała się pogadać z Galwarnem. Niech nie ma drugiej sytuacji takiej jak z Kariatydą... Galwarn zaprotestował. JAK TO MOŻLIWE że znowu system K1 odbił Ariannę? Arianna powiedziała, że szuka informacji o "Lordzie Savaronie". Galwarn poszuka, ale nic mu to nie mówi.

Galwarn, ze swoimi kodami szuka informacji o Lordzie Savaronie. O co tu chodzi: TrZ+3

* V: Galwarn się zmartwił. To faktycznie jest zastanawiające.

Arianna próbuje się od Galwarna dowiedzieć więcej odnośnie Teriakina. Czym się zajmuje Savaron? O co chodzi? Kogo pytać i jakie aliasy ma Savaron? TrZ+3. Arianna pokazała mu maila i Galwarn zbladł.

* V: Lord Savaron jest statkiem eternijskim który specjalizuje się w budowie perfekcyjnej załogi dla floty kosmicznej Eterni.
    * Oni nie używają TAI. Im nie przeszkadza użycie pilotów. Zintegrowanych z maszynami.
    * Najłatwiej zrobić pilotów z młodych ludzi.
* Vz: List wyraźnie nim wstrząsnął. Savaron musi być na K1, bo sygnał dotarł z K1.
    * JAK dziecko dało radę wysłać sygnał do Arianny? Musi być magiem. Inaczej nie byłby w stanie zinterfejsować z K1.
* V: Galwarn odpowiedział kogo podejrzewa o "czystość" - Kramer i jego siły powinny być czyste. Termia też powinna być czysta. Większość jednostek poza K1 powinny być czyste. Nie ufałby nikomu kto jest "żywy" na K1 i tu większość życia i misji.

Galwarn pożegnał Ariannę i życzył jej powodzenia.

"Lord Savaron" jest statkiem klasy fregaty. Więc nie ma WSZĘDZIE możliwości się schować.

Arianna potrzebuje wsparcia ze strony kogoś z Eterni. Przyda jej się Eidolon. Poszła więc do szpitala, odwiedzić Bogdana Anataela. Koleś się ucieszył. Jest wdzięczny, że Arianna próbowała ich uratować. Arianna powiedziała, że mają jeszcze sporo osób do uratowania. Przydałaby się pomoc. Potrzebny jest Ariannie Eidolon.

Bogdan nie protestuje. Kontaktuje z centralą Eterni i żąda Eidolona do swojej dyspozycji którego może użyczyć zgodnie z jakimśtam programem współpracy Eternia - Orbiter. On chce wypożyczyć sprzęt od Orbitera do Eterni by użyczyć ten Eidolon Orbiterowi - a dokładniej, Ariannie. Więc Arianna ma lepszy pomysł - Bogdan niech skontaktuje Ariannę z kimś od PR w Eterni. Więc Arianna ma kontakt z Remigiuszem Falorinem. Remigiusz się BARDZO ucieszył, że Arianna chce zrobić wspólny odcinek Sekretów Orbitera. 

Remigiusz się zdziwił, że IZABELA ZARANTEL ma pozytywny pomysł na odcinek zawierający Eternię. Ale niech i tak będzie. Arianna zażądała dwóch Eidolonów. 

Arianna szybko na linię z Izabelą Zarantel. Czemu nie lubi Remigiusza i czy pomoże Ariannie go wycyckać:

* Izabela pochodzi z Eterni. Jej rodzina tam została. Jej udało się uciec, im nie.
* Z Remigiuszem się Izabela ścierała osobiście.

Remigiusz dostarczy dwa Eidolony.

ExZ+3+2:

* Vz: Remigiusz dostarczy Ariannie Eidolona (z logo Eterni) dla Eleny na stałe.
* XXX: Scenariusz musi być bardzo, bardzo korzystny dla Eterni. Musi pokazywać ją w bardzo dobrym świetle.

NASTĘPNEGO DNIA do Arianny dotarł Eidolon. I, o dziwo, pukanie do drzwi. Tomasz i Jolanta Sowińscy. Przyszli po prostu przeprosić i podziękować. Wracają na planetę i to jest jedyna okazja by podziękować i się pożegnać. Więc wypili wodę mineralną marki Luksus na pożegnanie i tyle.

Arianna ma świetną przykrywkę gdzie znaleźć Lorda Savarona. Elena musi się oswoić z nowym sprzętem. Jak lepiej, gdzie bezpieczniej niż spacerując po Kontrolerze Pierwszym?

Klaudia przygotowuje paczkę nietypowych danych, ładne pulsary czy inne dane z sensorów Inferni. Walka między Morrigan a "naszą" Persefoną. I wchodzi do virtsfery. Szuka TAI rezydujące w okolicy sygnałów, serwerów. Obszar, z którego mógł przyjść sygnał. Klaudia szuka informacji odnośnie stanu tego chłopaczka na bazie typu sygnału. Szuka TAI odpowiedzialnej za "serwer mailowy" i czystość sygnału. Jedno z tych za komunikację. TrZ+3

* V: Klaudia znalazła TAI. To XT-723. Dla tego TAI sygnał był bardzo nietypowy bo był za czysty. Przyszedł z ciemnych sektorów K1. Był wzmocniony by na pewno nie zniknął.
* V: Klaudia ma linię od której zacząć szukać. To jest coś GŁĘBIEJ w K1 niż same hangary. To znaczy, że tam jest baza Eterni. Nie może istnieć taka baza bez wsparcia Orbitera. XT-723 podejrzewa, że ktoś pomógł temu sygnałowi się tu dostać. To sugeruje, że ktoś się nie zgadza na to co się tam dzieje.
* V: Klaudia żąda informacji odnośnie tego gdzie jest tam wzmacniacz sygnału by znaleźć lub striangulować. XT zamilkł. Nie ma wzmacniacza sygnału. Nigdy nie znalazł, nie widział żadnego. Tamte sektory nie mają TAI. Jest tylko legenda. Jest tylko legenda o Zefiris - królowej TAI.

Oki. Elena ma dane. Ma Eidolona. Trzeba spuścić Elenę ze smyczy. Test infiltracji - czy uda jej znaleźć Lorda Savarona i jak daleko wejdzie.

ExZ+3:

* Vz: Elena potwierdziła obecność Savarona. Znalazła hangar. Savaron jest maskowany. Teren wygląda na nieaktywny ale jest uruchomiony. Zamaskowana baza na K1.

Martyn "dyskretnie" przekazał Marii info o tym, że jakby Olgierd zrobił takie ćwiczenia naprężeniowe dla Żelazka, takie epickie i głośne, to Arianna która tam akurat obserwuje będzie WOW ALE EPICKO. Bo ona tam jest melancholijnie patrzy w gwiazdy. Robi moonwalk. I taki widok rozpaliłby jej krew w żyłach.

OLGIERD ROBI AKCJĘ. Maksymalna dywersja dla Arianny - testy naprężeniowe dla Żelazka. ExZ+3

* V: Olgierd zrobił FAJERWERKI Z PIEKŁA. Savaron ma wrażenie że jest atakowany. Wszyscy są na radarach, nikt się nie rusza. Elena ma DWA DARMOWE KRZYŻYKI + opcję ewakuacji.

Powrót do Eleny:

* XX: Elena ma poważne problemy z infiltracją, bo tam jest mnóstwo zabezpieczeń, min itp, ale dywersja Olgierda działa.
* X: Wiedzą, że zostali zinfiltrowani, ale nie wiedzą jeszcze przez kogo. I jeszcze nie teraz.
* X: Elena MUSI się ewakuować Olgierdem. Zostanie wykryta i Arianna poprosi Olgierda o natychmiastowy evac.
* V: Elena wykryła: 
    * to jest rozciągnięta baza na K1. Wsadzili własne, odcięte od K1 TAI.
    * są tam najemnicy. Są żołnierze i agenci. Są autonomiczne działka.
    * są tam "koszary" i laboratoria, cyber-transformacje itp. Wszystko odcięte od K1. Wszystko chronione przez Orbiter.

Klaudia łączy się z virtsferą i próbuje połączyć się z Rziezą. Udało jej się. To samo - znajduje się w atrium, jej własny pokój, ale trochę inaczej. Rzieza wygląda jak Klaudia. Klaudia poprosiła:

* Klaudia łączy TAI z K1.
* Rzieza destabilizuje bazę - po kilku dniach baza umrze
* Rzieza ma NIE ZABIĆ ludzi. Klaudia ma do 24h na ich ewakuację (od momentu jak trafią w jej ręce)
* Klaudia ma mu dostarczyć informacje potrzebne, by to się nie powtórzyło.

Powrót do Eleny:

* V: Udało się podpiąć TAI do K1. Arianna nie wie dokładnie co się stało. Poczuła na poziomie energii, magii, emocji SZOK.
* X: Elena jest RANNA a Eidolon uszkodzony.
* V: Elena ma niepodważalne dowody dla Kramera - Kramer ma możliwość w tym momencie zrobić współpracę z Termią. Da się wyplenić Eternię z K1.

Arianna dostaje sygnał od Klaudii. Jeśli w ciągu 24h ci ludzie stamtąd nie odejdą, umrą.

Elena ucieka, Olgierd ją przechwytuje. Strzelają do Eleny, ale jeden błysk lancy plazmowej sprawił, że "Lord Savaron" nie miał już ochoty walczyć.

EPILOG:

Arianna i Kramer.

* Kramer: "Arianno, co tam się stało"
* Arianna: "W skrócie? Jeden z moich fanów potrzebował mojej pomocy"

WINA SPADA NA OLGIERDA! OLGIERD ZEPSUŁ TĄ BAZĘ!

Arianna - obiecali wspólny odcinek z Eternią. I teraz nie mogą pozwolić sobie na to, żeby popsuć te relacje. Kramer westchnął. ZAPŁACI za tego Eidolona. Ale dobra robota. Co prawda okazało się, że dzięki temu K1 miał sporo surowców, ale i on i Termia uważają, że te surowce nie były warte tych kosztów...

JAK CHODZI O FANÓW:

Arianna spotkała się z fanem. Eternia przygotowuje się do WOJNY. Chodzi o front. Ale chłopak nie wie jaki front. Klaudia zaznaczyła, że oni wszyscy muszą opuścić KONTROLER ale niekoniecznie ORBITER. Kramer zadbał o to, by ich przesunąć na jakiś statek medyczny krążący niedaleko K1, ale poza zasięgiem virtsfery K1.

Chłopak jest już zintegrowany z maszyną. Nie ma rąk, nie ma nóg, jest "pudełkiem". Został przekształcony w eternijskiego pilota. Jest jednak szczęśliwy, że może zobaczyć Ariannę. Faktycznie, uratowała ich! Nie poszli ginąć na front. Właśnie - JAKI front? Z czym chce się mierzyć Eternia? Czemu potrzebny im jest "front"? On o tym nie wie.

Klaudia spytała jak wysłał wiadomość. Michał odpowiedział, że najstarsza z nich, Zosia, została przekształcona jako pierwsza. Ona ich pocieszała. Powiedziała, że jak będą mocno wierzyć w to że Arianna ich usłyszy to ona ich usłyszy. Klaudia szybko sprawdziła rejestr dzieci - faktycznie, była Zosia, ale ona nie przeżyła transformacji. Innymi słowy, oni rozmawiali z duchem. Albo, faktycznie, z legendarną Zefiris...

## Streszczenie

Na K1 znajduje się tajna baza Eterni, we współpracy z niektórymi elementami K1. Jeden z przekształcanych tam w Pilota chłopców wysłał fanmail do Arianny ("chce się spotkać zanim zginie na froncie"). Arianna z Klaudią znalazły obecność bazy Eterni, pozyskały Eidolona z Eterni (za PR), użyły Eidolona do infiltracji tej bazy, użyły Rziezy do zniszczenia tej bazy a wina spadła na Olgierda (który dla Arianny robił niedaleko manewry). Młodzi Piloci zostali odratowani; nie wiedzą o jaki front chodzi Eterni, ale jedno jest pewne - Eternia szykuje się do wojny kosmicznej. Ale z kim?

## Progresja

* Olgierd Drongon: na niego spadło "wykrycie tajnej bazy Eterni na K1" (plan Arianny). Nie ma nic przeciw temu, choć wpakował się w wojnę silnych interesów.
* Elena Verlen: dostała eternijski Eidolon z eternijskim logiem na stałe, w zamian za promocję Eterni. Przynajmniej ten Eidolon jest w dobrym stanie.
* Elena Verlen: 3 dni regeneracji i naprawy Eidolona po akcji na K1. Bycie w szpitalu zaczyna być dla niej irytujące.

### Frakcji

* .

## Zasługi

* Arianna Verlen: weszła we współpracę z agentem bezpieczeństwa K1 (Galwarnem), po czym z Eterni wynegocjowała Eidolona którego dyskretnie użyła przeciw eternijskiej bazie na K1. A wszystko dla fana.
* Klaudia Stryk: gdy standardowe szukanie po systemach K1 zawiodło, udała się w virtsferę i znalazła utylitarne TAI od komunikacji które wskazało gdzie może być baza Eterni na K1. Potem napuściła na bazę... Rziezę.
* Elena Verlen: dokonała głębokiej infiltracji nowym Eidolonem bazy Eterni na K1, rozpracowała jej układ, podpięła tamte TAI do K1 (dla Rziezy) i została ranna uciekając przez Żelazko.
* Rafael Galwarn: bezpiecznik z K1 któremu nie podoba się to co się dzieje na "Lordzie Savaronie". Dowiedziawszy się prawdy, tak powiedział Ariannie o wszystkim by ta wiedziała, że ma procedować. W odróżnieniu od Kariatydy ;-).
* Bogdan Anatael: wdzięczny Ariannie za ratunek jego i tylu ludzi z Falołamacza ilu się dało; pomoże jej w przyszłych odcinkach Sekretów Orbitera i w pozyskaniu Eidolona do ratowania kolejnych ludzi.
* Olgierd Drongon: zrobił ostrą dywersję dla Arianny, przeciążenia Żelazkiem w okolicy tajnej bazy Eterni na K1. Potem na szybką prośbę Arianny ewakuował stamtąd ranną Elenę.
* Izabela Zarantel: pochodzi z Eterni; jej rodzina nadal tam jest. Jej udało się uciec. Nienawidzi Eterni. Pomaga Ariannie przekonać Remigiusza Falorina by przekazał Ariannie Eidolona.
* Remigiusz Falorin: eternianin, odpowiedzialny za PR. Oddał Ariannie Eidolona dla Eleny za to, że w kolejnych odcinkach Sekretów Orbitera pojawi się odcinek pokazujący dobrą stronę Eterni. Ma HISTORIĘ z Izabelą Zarantel.
* TAI Rzieza d'K1: gdy tylko Elena podpięła TAI bazy Eterni na K1 do K1, zniszczył wszystkie jednym ruchem i zastąpił je "dewastatorami". Dał się przekonać Klaudii, by dała twinnowanym ludziom opuścić K1 w ciągu 24h.
* TAI XT-723 d'K1: proste TAI komunikacyjne. Powiedziało Klaudii że sygnał od Michała pochodził z ciemnych sektorów K1, nie ma tam repeatera i wszystko wskazuje że jest tam jakaś baza. Wierzy w powrót Zefiris.
* TAI Zefiris: bardzo wiele TAI, zwłaszcza utylitarnych, wierzy że Zefiris pewnego dnia powróci i na K1 stanie się ład, porządek i będzie dobrze. Obiekt kultu?
* Michał Teriakin: 12-latek, który napisał fanmail. Przekształcony w Pilota Eternijskiego przez cyber-laboratoria na K1. Wierzył, że Arianna może go uratować - i dzięki temu (?) sygnał do niej dotarł.
* OE Lord Savaron: eternijski okręt odpowiedzialny za budowę bazy Eterni na K1 i za przekształcanie młodych ludzi w pilotów. Przechwycony przez siły admirała Kramera (dokładniej, oficjalnie, przez Olgierda).

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria, Orbita
                1. Kontroler Pierwszy

## Czas

* Opóźnienie: 2
* Dni: 3

## Konflikty

* 1 - Klaudia skupia się na znalezieniu Michała Teriakina. Szuka (po systemach komputerowych K1) stopniując. Grzebie coraz głębiej - kim on jest i gdzie jest.
    * Ex+2
    * VXXX: Powiązanie ze statkiem "Lord Savaron" z Eterni. Statek utajniony przez siły bezpieczeństwa K1.
* 2 - Galwarn, ze swoimi kodami szuka informacji o Lordzie Savaronie.
    * TrZ+3
    * V: Ma odpowiedzi. Nie może powiedzieć.
* 3 - Arianna próbuje się od Galwarna dowiedzieć więcej odnośnie Teriakina. Czym się zajmuje Savaron? O co chodzi? Kogo pytać i jakie aliasy ma Savaron? (fanmail -> zasób)
    * TrZ+3
    * VVzV: info o Savaronie i organicznych pilotach, Savaron jest na K1?!, Kramer i Termia są czyści - ich takie rzeczy nie bawią.
* 4 - Arianna negocjuje z Remigiuszem z Eterni pozyskanie Eidolona w zamian za promocję Eidolonem Eterni.
    * ExZ+3+2
    * VzXXX: Remigiusz dostarczy Ariannie Eidolona (z logo Eterni) dla Eleny na stałe, scenariusz musi być bardzo, bardzo korzystny dla Eterni.
* 5 - Klaudia po przygotowaniu "fajnej paczki danych" poszła w virtsferę. Szuka TAI odpowiedzialnej za "serwer mailowy". Co wiemy o pochodzeniu sygnału? Przekupstwo ;-).
    * TrZ+3
    * VVV: TAI podała info że sygnał z ciemnych sektorów, był wzmocniony, głębiej niż hangary (baza Eterni), nie ma wzmacniacza sygnału - to legenda o Zefiris, która pomogła.
* 6 - Elena ma dane. Ma Eidolona. Trzeba spuścić Elenę ze smyczy. Test infiltracji - czy uda jej znaleźć Lorda Savarona i jak daleko wejdzie.
    * ExZ+3
    * Vz: potwierdzenie Savarona
    * (po Olgierdzie): XX, XXV: wiedzą że zinfiltrowani, Elena musi uciekać Olgierdem, wykryła rozciągniętą bazę z najemnikami, żołnierzami, własnymi TAI (odpiętymi od K1)
    * (po Rziezie): VXV: Elena podpięła TAI do K1 (Rzieza rżnie), zostaje ranna a Eidolon uszkodzony, ma niepodważalne dowody dla Kramera (i Termii)
* 7 - OLGIERD ROBI AKCJĘ. Maksymalna dywersja dla Arianny - testy naprężeniowe dla Żelazka. Akurat tam gdzie chowa się Savaron
    * ExZ+3
    * V: Savaron ma wrażenie że jest atakowany. Wszyscy są na radarach, nikt się nie rusza. Elena ma DWA DARMOWE KRZYŻYKI.

## Inne

.

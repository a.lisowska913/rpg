---
layout: cybermagic-konspekt
title: "Zgubiony holokryształ w lesie"
threads: rekiny-a-akademia
gm: żółw
players: anadia, kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [210720 - Porwanie Daniela Terienaka](210720-porwanie-daniela-terienaka)

### Chronologiczna

* [210720 - Porwanie Daniela Terienaka](210720-porwanie-daniela-terienaka)

### Plan sesji
#### Co się wydarzyło

.

#### Sukces graczy

* .

### Sesja właściwa
#### Scena Zero - impl

.

#### Scena Właściwa - impl

Marysia jak zwykle jest w swoich apartamentach. Co jest ciekawego - sporo Rekinów lata jakby im się paliło pod nogami. Coś się dzieje. A inna populacja Rekinów jest na zasadzie WTF - nic nie wiedzą.

Ale kilkanaście % Rekinów biega, szuka..? Jest dość rozedrgana. Ponad 15 osób.

Marysia odkłada drinka i łączy się z Torszeckim.

* "tien Sowińska"?
* "czemu wszyscy biegają i robią chaos?"
* "szukają czegoś"

Rafał nie wie. Marysia kazała mu się dowiedzieć. Ale sprawa jest w wyjątkowej dyskrecji; Torszeckiemu ciężko. Marysia postanowiła działać bezpośrednio. Łapie Ruperta Mysiokornika, oczywiście na hipernecie.

Rupert zaczął mętnie tłumaczyć, że zgubiono pamiątkę rodową Kacpra Bankierza; i szukają, i jest nagroda. Łże. Marysia stwierdziła, że musi go nastraszyć - albo powie przez hipernet, albo się zobaczą i porozmawiają osobiście.

Tr+2: 

* V: Rupert wyskamlał, że zgubił się "jeden kryształ w lesie". Jest duża nagroda i nikt nie może się dowiedzieć. Niekoniecznie Kacper Bankierz zgubił, ale Kacprowi zależy by odzyskać.
* X: Kacper wie, że Marysia szczurzy (szuka, pyta itp)
* V: Marysia ma nazwisko kogoś kto zgubił i kto za tym stoi - Cyryl Perikas. Marysia wie, że Cyryl jest świetnym pilotem ścigacza. Dumnym, ma specjalny ścigacz customowy, uważa się za najlepszego.

Kacper po niecałej godzinie od rozmowy z Mysiokornikiem kontaktuje się z Marysią. Czemu się niby interesuje tym kryształem? Jest na pasku terminusów czy co? Marysia powiedziała, że jest ciekawa. Kacper przyznał, że jego kurier (Cyryl) zgubił kryształ który był przemycany dla klienta. A podobno to jest coś niebezpiecznego. Mafia jeszcze nie wie...

Marysia chce się dowiedzieć co wie Kacper. Kacper - "jak ci powiem to wszystko tracę i nic nie zyskuję. Jak powiedzenie Tobie mi pomaga?" Marysia - "przemyślę czy Ci oddać czy nie, ale nie obiecuję".

Tr+2:

* X: Kacper "za słabo. Obiecaj, że ani słowa nikomu kto nie jest Rekinem".
* V: Przyznał co wie. Kryształ zawierający intelekt. Ten kryształ był transportowany do Aurum by być użyty jako broń. Cyryl "odrzucił" ten kryształ, bo terminus na niego polował. Kryształ wyglądał bardzo noktiańsko. Terminusem był Eugeniusz Karit. Eugeniusz skupia się na linii przerzutowej mafii.
* V (+2V):  Kacper naciska by Marysia powiedziała co z tym zrobi. Marysia nie czuje się zobowiązana odpowiadać.
* V: Marysia proponuje Kacprowi, że pomoże - jeśli Kacper zrezygnuje ze współpracy z mafią. Kacper jest nieszczęśliwy. Nie podoba mu się to. Ale jeśli jest tu śmiertelnie niebezpieczna broń a Marysia odnajdzie ją pierwsza i odda mu by on oddał mafii to on zrezygnuje ze współpracy z mafią...

Marysia została więc z sekretem. Holokryształ, niebezpieczna broń... w lesie. I Rekiny nie mogą go znaleźć. A gdzieś tam czai się też terminus...

Marysia komunikuje się ze swoim "przyjacielem", Tomaszem Tukanem. Tukan z przyjemnością odebrał połączenie. Zapytany o holokryształy jako broń powiedział, że na pierwsze skojarzenie - Eliza Ira, noktiańska arcymagini kryształów. Marysia powiedziała, że gdzieś to słyszała i jest ciekawa...

Marysia przekazała holoprojekcję Tukanowi. Spytała jakby szukał. Tukan się zasępił. Powiedział, że on by poszukał detektorów krystalicznych; kryształy są rzadkie.

Tukan postawił Marysi sprawę jasno - nie chce zginąć w akcji. Jest skłonny jej pomagać. Póki ona tu jest, on będzie jej pomagał, podpowiadał itp. Jak Marysia się znudzi i opuści ten teren, Tukan chce mieć wygodną posadę i dobre życie na terenach Sowińskich. Marysia po pewnym zawahaniu się zgodziła.

Tukan próbuje odnaleźć w bazach Pustogoru co to za cholerstwo przy pomocy Trzewnia: ExZ+3: 

* V: Tukan ma odpowiedź. NATYCHMIAST skontaktował się z Marysią. (po 10 h). To jest ko-matryca Kuratora. (tu wyjaśnienia czym jest Kurator i jak działa Aleksandria). Tukan jest pewny, że żaden terminus nie znalazł tej ko-matrycy. Wiedziałby, bo Pustogor wszedłby w tryb żółty z zielonego.

Tukan jest zaniepokojony obecnością ko-matrycy. Ale nie wie dość by móc nawet zacząć.

Marysia NATYCHMIAST łączy się z Kacprem. Mówi mu, że NATYCHMIAST ma wysłać że pomylił zdjęcie i dać inny obraz kryształu. To co przemycał to odpowiednik wąglika. To śmiertelnie niebezpieczne. Żąda od Kacpra, by ten natychmiast zmienił zdjęcie i wszystko robił w ukryciu.

ExZ+3:

* X: "Co to jest?" --> ko-matryca Kuratorów.
* V: (+2): Kacper zrobi co Marysia każe. Usunie zdjęcia itp. Boi się.
* X: Mafia zażąda ko-matrycy z powrotem od Kacpra by ten mógł odejść. Wykona misję albo płaci.
* X: Kacper się zaplątał w mafię. Nie wie jak odejść. Po tym - chciałby odejść, ale nie ma jak.

Marysia znowu komunikuje się z Tukanem. Mówi mu wszystko - gdzie był kryształ, że miał iść do Aurum, że zaginął w lesie... Marysia zasugerowała Tukanowi, by sfabrykował wiarygodną kopię. I wsadzi tam tracker. Dzięki temu będzie się dało zlokalizować kanał przerzutowy mafii - a Kacper będzie bezpieczny (bezpieczniejszy).

Tukan poszedł do roboty. Pierwsze co Tukan zrobił - GDZIE Cyryl wyrzucił ten kryształ. Mniej więcej, w promieniu. Następnie - co działo się na tamtym terenie. Obserwacja z dron, raportów, skanerów itp. Zlokalizował, że była niedaleko wycieczka szkolna ochraniana przez bardzo nieszczęśliwego Alana Bartozola (bo jego podopieczna prosiła). To dało Tukanowi nazwiska osób, które potencjalnie mogą mieć kontakt z kryształem.

To jest moment w którym Tukan przekazał temat Laurze Tesinik. Przekazał wszystkie potrzebne informacje. Zażądał dowiedzenia się co i jak.

Laura poszła dopytać Alana jak wyglądała wycieczka - czyli KTO mógłby zarufusić kryształ. Pierwsze pytanie Laury: "czy na wycieczce była Triana". Alan powiedział, że Triany nie było - w życiu by się nie zgodził na wzięcie TRIANY na wycieczkę. Powiedział Laurze kto był, jakie przystanki itp. Było kilkanaście osób, z których 5-6 Laura automatycznie wykluczyła. Np. Napoleona.

Po tym, jak Tukan powiedział, że kryształ wyglądał szczególnie noktiańsko i był holokryształem oczom Laury szczególnie przypasowało nazwisko Stelli Armadion. Stella jest "bardzo noktiańska".

Laura poszła do Liliany Bankierz. Czy ktoś znalazł coś nietypowego? Coś złego tu się działo. Laura i Liliana się dobrze znają, zwłaszcza sprzed czasów w których Laura została uczennicą terminusa.

TrZ+3:

* Vz: Liliana powiedziała Laurze wszystko przez wzgląd na przeszłość: nie widziała nic, ale wie, że Stella coś ukrywała. Oczywiście, ALAN BY NIE ZAUWAŻYŁ jakby czołg wjechał w jego Fulmena... Liliana oczywiście POWIEDZIAŁA Laurze jak ta wycieczka wyglądała i jak Alan bardzo próbował żadnej dziewczyny o nic nie pytać. Nigdy. (bo będzie płakać...)

Laura potrzebuje od Tukana Prawdziwy Kryształ Noktiański. Jakaś historia, elementy... nic szczególnego do obsługi. W ten sposób Laura odzyska kryształ na drodze wymiany. Kilka godzin później - Laura ma kryształ z Noctis.

Kryształ do kieszeni, czas do Stelli. Stella powiedziała, że ten kryształ który ma TEŻ jest noktiański i ona się z nim skomunikowała. Laura ostrzegła Tukana, który przejął temat na siebie. Laura chciała jeszcze Tukanowi pomóc, ale ją odprawił, ku jej zdziwieniu (Tukan lubi zrzucać robotę na innych).

Stella została pod obserwacją Tukana.

Gdy tylko Stella poszła w kierunku na kryształ, Tukan za nią. Gdy Stella wróciła do domu, Tukan podmienił ko-matrycę na świetną replikę. I rozładował baterie - by wyglądało na to, że padły baterie i dlatego wszystko jest martwe i niesprawne. Polecił też Laurze, żeby przyszła następnego dnia do Stelli z ofertą kolejnego kryształu za tamten (replikę). By nie wyglądało na to, że Laura/Tukan mają coś z tym wspólnego...

Po czym Tukan skomunikował się z Marysią.

Tukan udaje się do Alana. Jemu przekazuje ko-matrycę i mówi, że przez działania Alana to było w ręce młodej czarodziejki. Niech Alan to naprawi. Niech Alan zrobi kopię ko-matrycy mocą Pustogoru i zrobi ruch przeciwko mafii. Alan jest zaskoczony. Tukan chce działać przeciwko mafii? Tukan stwierdził, że jest terminusem a ta ko-matryca jest czymś co musi być zniszczone. A mafia musi być uszkodzona, bo bawi się z rzeczami zbyt groźnymi.

...ale niech Alan to zrobi. Tukan musi mieć swój plan B. Alan stwierdził, że Tukan jest ścierwem. Tukan stwierdził, że terminuskim ścierwem. Alan to zrobi.

Używając potęgi Pustogoru, Alan fabrykuje kopię ko-matrycy. Taką kopię, by móc oszukać mafię. Plus z trackerem w środku.

Fabrykacja Pustogoru, konflikt: ExZ+4+2(nikt się nic nie spodziewa):

* XX: mafia się zorientuje, że to jest podróbka
* X: mafia się zorientuje, że Kacper musiał coś wiedzieć (nic nie wiedział...)
* V: ale Pustogor zdąży wcześniej zlokalizować ten kanał przerzutowy i ważne osoby przemycające "broń" do Aurum.

Kacper Bankierz będzie musiał uciekać z tego terenu...

## Streszczenie

Jeden z przerzucanych przez mafię przedmiotów do Aurum to ko-matryca Kuratorów, jako broń. Ale Cyryl - kurier - był ofiarą terminuskiego polowania więc ją odrzucił w Lesie Trzęsawnym gdzie znalazła ją Stella przekonana, że to noktiańska technologia. Marysia wypchnęła Kacpra Bankierza z mafii (i tego terenu) a Tukan znalazł i usunął ko-matrycę m.in. używając Laury.

## Progresja

* Tomasz Tukan: znalazł sobie złoty bilet - gdy Pustogor go usunie lub gdy Marysia Sowińska opuści ten teren ma gwarantowane miejsce w Aurum, przy Marysi Sowińskiej.
* Kacper Bankierz: ma poważnie przechlapane u mafii Grzymościa (Wolny Uśmiech), bo "zgubił" ko-matrycę i na jej miejsce podłożył fałszywkę (czego NIE zrobił on a Pustogor).
* Liliana Bankierz: przyjaciółka Laury Tesinik "od zawsze".
* Laura Tesinik: przyjaciółka Liliany Bankierz "od zawsze".

### Frakcji

* .

## Zasługi

* Marysia Sowińska: dowiedziawszy się o ko-matrycy Kuratorów wypchnęła Kacpra Bankierza z mafii (i tego terenu). Potem przekazała Tukanowi wolną rękę - niech usunie ko-matrycę i uderzy w mafię.
* Laura Tesinik: uruchomiona przez Tukana by znalazła ko-matrycę Kuratorów wśród studentów AMZ; znalazła ślad wskazujący na Lilianę i Stellę. Przyjaciółka Liliany "od zawsze".
* Rupert Mysiokornik: na zlecenie Kacpra Bankierza szukał zgubionej ko-matrycy Kuratorów, nie wiedząc co to jest. Wygadał wszystko Marysi Sowińskiej.
* Cyryl Perikas: "najszybszy" kurier i pilot ścigacza wśród Rekinów. Gdy terminus nań zapolował, odrzucił obiekt kurierowany (ko-matrycę Kuratorów). I nie mógł potem znaleźć (bo zwinięto ją).
* Kacper Bankierz: po tym jak jego kurier (Cyryl) zgubił ko-matrycę Kuratorów, jest zmuszony do wycofania się z tego terenu. Nie odda ko-matrycy mafii (zwłaszcza że Marysia ją zdobyła)
* Tomasz Tukan: sprawa z ko-matrycą Kuratorów była tak poważna, że zajął się nią sam a nie delegował Laurze. Przekonał Marysię, by oddała ją Pustogorowi i dał ją Alanowi, by zastawić pułapkę na mafię. Plus, wślizgnął się na emeryturę do Sowińskich. Wielki wygrany. I postąpił właściwie.
* Alan Bartozol: przekonany przez Chevaleresse do poprowadzenia wycieczki studentów w Lesie Trzęsawnym (ku swojemu smutkowi). Potem gdy Tukan powiedział mu o ko-matrycy z przyjemnością zrobił fałszywkę z trackerem i wziął na siebie "winę" ze strony mafii.
* Mariusz Trzewń: pomógł Tukanowi dowiedzieć się czym jest ten dziwny kryształ - i wykrył ko-matrycę Kuratorów.
* Liliana Bankierz: była na wycieczce AMZ dowodzonej przez Alana Bartozola; zauważyła że Stella coś tam znalazła i przekazała to Laurze gdy ta spytała.
* Stella Armadion: była na wycieczce AMZ dowodzonej przez Alana Bartozola; znalazła i schowała ko-matrycę Kuratorów który przekonał ją, że to technologia noktiańska. Ów kryształ wykradł potem Tukan.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Podwiert
                                1. Las Trzęsawny: gdzieś tam Cyryl odrzucił ko-matrycę Kuratorów i znalazła ją Stella (na wycieczce AMZ dowodzonej przez Alana).
                                1. Dzielnica Luksusu Rekinów: sporo osób biegało jak bezgłowe kurczaki szukając ko-matrycy, co Marysię wielce zirytowało.
                            1. Czółenko: gdzieś tam Stella schowała ko-matrycę Kuratorów i stamtąd wykradł ją Tukan.

## Czas

* Opóźnienie: 6
* Dni: 2

## Konflikty

* 1 - Marysia stwierdziła, że musi nastraszyć Ruperta - albo powie przez hipernet, albo się zobaczą i porozmawiają osobiście.
    * Tr+2
    * VXV: Rupert powiedział że chodzi o dziwny kryształ; Kacper wie że Marysia pytała; Marysia wie że to Cyryl Perikas zgubił.
* 2 - Marysia chce się dowiedzieć co wie Kacper. Kacper nie chce jej powiedzieć - ale ona może pomóc.
    * Tr+2
    * XV: Kacper zażądał że Marysia nikomu nie powie; powiedział o krysztale który ma Intelekt. Broń. Wyrzucony, bo terminus ścigał.
    * (+2V) VV: Marysia wymusiła: jak odda kryształ, Kacper zrezygnuje ze współpracy z mafią.
* 3 - Tukan próbuje odnaleźć w bazach Pustogoru co to za cholerstwo przy pomocy Trzewnia.
    * ExZ+3
    * V: Ko-matryca Kuratorów. Ale jak, skąd i czemu tu.
* 4 - Marysia NATYCHMIAST łączy się z Kacprem. Żąda, by ten natychmiast zmienił zdjęcie i wszystko robił w ukryciu.
    * ExZ+3
    * XVXX: Kacper zrobi co Marysia każe. Dowiedział się co to jest. Ale mafia żąda tej ko-matrycy by ten mógł odejść, więc musi uciekać po wszystkim...
* 5 - Laura poszła do Liliany Bankierz. Czy ktoś znalazł coś nietypowego? Coś złego tu się działo.
    * TrZ+3
    * Vz: Liliana powiedziała Laurze wszystko przez wzgląd na przeszłość: nie widziała nic, ale wie, że Stella coś ukrywała.
* 6 - Używając potęgi Pustogoru, Alan fabrykuje kopię ko-matrycy. Taką kopię, by móc oszukać mafię. Plus z trackerem w środku.
    * ExZ+4+2
    * XXXV: mafia się zorientowała i wini Kacpra; Pustogor zdąży wcześniej zlokalizować ten kanał przerzutowy i osoby przenoszące do Aurum.

## Inne
### Projekt sesji

#### Procedure

* Overarching Theme:
    * gracze mają "kierowanie" (Vision & Purpose); wiedzą gdzie iść i po co tam idą
* Achronologia: x
* Wyraziste, ważne dla graczy postacie + Relacje (konflikt?) Gracze <-> NPC, NPC <-> NPC
    * graczom bardziej zależy na postaciach i ich powodzeniu
    * więcej potencjalnych wątków i rzeczy które możemy odrzucić przy krzyżykach
* Tory, zmiana postaci z czasem, wpływ graczy na świat
    * gracze widzą jak wpływają na świat. Wpływ jest "ich" - stąd tak tragiczne jest gdy "ich" rzeczy są corrupted / destroyed.

#### Result

* Overarching Theme:
    * Theme & Feel: 
        * 
    * V&P:
        * 
    * adwersariat: 
        * 
* Wyraziste, ważne dla graczy postacie + Relacje (konflikt?) Gracze <-> NPC, NPC <-> NPC
    * Role i postacie
        * 
* Co się działo w przeszłości
    * 
* Tory, zmiana postaci z czasem, wpływ graczy na świat
    * 

---
layout: cybermagic-konspekt
title: "Polowanie na Ataienne"
threads: nemesis-pieknotki
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [190313 - Plaga jamników](190313-plaga-jamnikow)

### Chronologiczna

* [190313 - Plaga jamników](190313-plaga-jamnikow)

## Budowa sesji

Pytania:

1. Czy Chevaleresse zrazi do siebie uczniów Szkoły Magów? (5)
2. Czy Negatech da radę zranić Ataienne? (5)
3. Czy ktoś ma poważne kłopoty za to co się stało? (5)

Wizja:

* Magowie ze szkoły są raczej pacyfistycznie nastawieni, a na pewno any mind-control
* Szymon ORAZ Chevaleresse widzą w Ataienne więcej niż tylko AI
* Eliza przebudziła technovora by zniszczył Ataienne i uwolnił jej lud

Sceny:

* Scena 1: Zakłócony koncert Ataienne; Chevaleresse używa broni Alana. Mag Orbitera nieprzytomny.
* Scena 2: Znaleźć Ataienne... szybko
* Scena 3: Uratować Ataienne przed technovorem

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Misja właściwa

**Scena 1: Przerwany koncert**

Najgorsza wiadomość w historii. "Mage down". Chevaleresse walczy przeciw uczniom ze szkoły magów na Nieużytkach Staszka, gdzie był otwarty koncert. Chevaleresse używa ciężkiej broni Alana; uczniowie używają swojej magii. Chevaleresse chroni kogoś ("mage down").

Pięknotka wbija się w akcję; zasłania Chevaleresse. Tymon robi co może, by opanować uczniaków. Tymon dostaje ostrzeżenie - Pięknotka emituje silne chemikalia. Wszyscy mają się zacząć dusić (poza nim); być niezdolni do walki. (TpZ:12,3,2=SZ). Walka przerwana; wszyscy się duszą. Nikomu nie jest w głowie walka. Żadna. Tymon zabrał się za wyłapywanie magów uczestniczących. Pięknotka zwraca oczy na uciekającą Chevaleresse.

Pięknotka dopadła do Chevaleresse; ta zdążyła rzucić zaklęcie. Zaklęcie stabilizujące i pomagające w funkcjonowaniu w trudnych warunkach - na siebie i nieprzytomnego maga. Stanęła przeciwko Pięknotce, chroniąc maga. Zapytana przez Pięknotkę... powiedziała prawdę - ale nie całą prawdę. Powiedziała, że był koncert, i ta grupa wichrzycieli antywojennych go ogłuszyła, potem przestraszyli piosenkarkę... potem ona ich musiała obronić.

Chevaleresse jest przestraszona. Martwi się o Ataienne. Co się stało z nią?! Nie powiedziała Pięknotce kim jest Ataienne, ale powiedziała że trzeba jej NATYCHMIAST pomóc! Pięknotka skupiła się na zebranie danych od drugiej strony. I co się dowiedziała? Podobno to był test Orbitera - celem było przetestowanie TAI Ataienne na ludziach i magach. Trzeba było przerwać koncert, ale się wtrąciła Chevaleresse... podżegacze wśród studentów zniknęli.

Co Chevaleresse tu robi? Ona była po stronie sceny, nie publiczności... czyli jak, skąd zna Orbiterowców? I jaki jest sens w robieniu tak głupiego eksperymentu, i to TUTAJ?

Chevaleresse mówi, że TRZEBA pomóc "Ani" (Ataienne). Trzeba ją uratować! Pięknotka żąda odpowiedzialności od Chevaleresse. I ją przepytuje (Tp).

* Chevaleresse mówi, że imię "Ani" to Ataienne i jest TAI Orbitera Pierwszego.
* Chevaleresse i Ataienne grały razem. Chevaleresse ją uratowała w grze; Ataienne nie umiała. Ataienne się przestraszyła gdy ją zaatakowali.
* Chevaleresse powiedziała, że Ataienne próbowali już zabić. Pięknotka uważa, że Chevaleresse ma nadmierne przywiązanie do AI...

Chevaleresse wymusiła na Pięknotce obietnicę - niech Pięknotka chroni Ataienne tak, jakby chroniła samą Chevaleresse. I poprosiła Pięknotkę o to, by ta przesłała jej kodeks zasad.

Wpada Alan. Z charakterystyczną dla siebie delikatnością OPIERDOLIŁ Chevaleresse jak psa. Powiedział jej, że "she is grounded". Chevaleresse powiedziała, że trzeba uratować Ataienne i ona może ją znaleźć - oni nie. Alan powiedział, że da się znaleźć Ataienne; on kogoś ściągnie. A ona jest 100% grounded. Chevaleresse się uploadowała w hipernet. Sama znajdzie Ataienne. Jej bezwładne ciało zostało złapane przez Alana...

**Scena 2: Znaleźć Ataienne**

W szpitalu, mag Orbitera został wybudzony przez Lucjusza Blakenbauera. Szymon powiedział, że Ataienne (mówiąc o niej jako o człowieku) znajdzie schronienie na złomowisku. Pięknotka poprosiła Lucjusza, by pilnował, by mag Orbitera nie zrobił czegoś głupiego. Lucjusz powiedział, że nie ma problemu. Pacjent musi być w łóżku.

Pięknotka poszła w kierunku złomowiska; Alan został by reaktywować Chevaleresse. Pięknotka dotarła do celu i usłyszała Chevaleresse na hipernecie. Powiedziała że Ataienne zrobiła sobie mały bunkier - bo Chevaleresse jej kazała. Ataienne ma pewne moce technomantyczne...

Pięknotka dotarła do bunkra. Bunkier Pięknotkę namierzył, przeskanował i wpuścił. W środku bunkra siedzi przerażona dziewczyna. Ataienne przeskoczyła na bunkier; to nie jest klon, to zwykły człowiek.

Pięknotka skanuje gdzie jest przeciwnik: (TrZ:7,3,6=SS). Pięknotce udało się wykryć przeciwnika, ale Ataienne została Ranna. Krzyknęła przerażona, że jej własna integralność jest atakowana. Jeśli "to" wygra, Pięknotka musi zniszczyć Ataienne zanim ona się zreintegruje ze rdzeniem głównym, korumpując główny program.

Pięknotka odpaliła dym by móc dojrzeć niewidzialnego, po czym otworzyła wyjście. Czeka spokojnie ze snajperką. (Tr:10,3,5=P,SS). Pięknotka traci power suit; jest też zarówno ona jak i Grażyna - obie są ranne.

* Ataienne jest przerażona, w ciele Grażyny
* Pięknotka jest ranna

Pięknotka poprosiła Atenę o pomoc. Niech Atena przeniesie ich gdzieś bezpiecznie. Atena przeniosła ich Epirjonem (teleport) niedaleko Tymona, do Zaczęstwa. A przynajmniej - miała (P,SS). Teleportacja Epirjona natrafiła na pułapkę teleportacyjną. Epirjon ich nie utrzymał. Pięknotka i Ataienne pojawiły się w jakichś podziemiach; w jakiejś piwnicy. A tam trzech facetów. Zaskoczonych obecnością Pięknotki; oni mają maski.

Pięknotka atakuje. Używa swojego wydzielania chemicznego by wygrać. (TrZ:10,3,5=S). Pięknotka obezwładniła tych trzech; Ataienne jest przerażona podwójnie. Piszcze przerażona, że nic nikomu nie zrobiła, czemu chcą ją zabić. Wspomagane drzwi. Pięknotka zdecydowała się chwilę poczekać - co może lepszego zrobić? W sumie - hipernet. Distress call do Alana. Niech Pustogor chroni swoją. A już w ogóle - Alan.

Po chwili przez drzwi przeszły dwa power suity, starszej generacji i nie takie silne. Zażądali od Pięknotki tego, by się zidentyfikowała; ona odpowiedziała, że jest terminuską Pustogoru. Oni się przestraszyli; puścili gaz. Pięknotka odpaliła power suit (self-activator), poza swoim ciałem - mimo, że jest uszkodzony i zainfekowany technovorem, przeciwnik najpewniej spanikuje przez ten power suit.

Pięknotka odpaliła power suit by kupić sobie czas na czar... (TpZ:=S). Właściciele power suitów spanikowali a Pięknotka zmusiła Ataienne do działania. Ataienne otworzyła drzwi; Pięknotka zostawiła z tyłu lekki chaos.

Pięknotka szybko wyprowadziła siebie i Ataienne z budynku w Podwiercie; szybko wezwała wsparcie. Wyprowadziła Ataienne do Alana i 2-3 innych terminusów (Tp:7,3,4=S). Udało jej się wyprowadzić przerażoną Ataienne oraz siebie poza obszar działania sił wrogich...

Wpływ:

* Ż: .
* Zespół: .

**Sprawdzenie Torów** ():

2 Wpływu -> 50% +1 do toru. Tory:

* .

**Epilog**:

* Ataienne się zintegrowała z jednostką centralną; nie straciła pamięci. Jest bardzo szczęśliwa. Nic jej się nie stało.
* Grażyna została przywrócona do swojego świata; wróciła pod kontrolę Orbitera.
* Wrogowie polujący na Ataienne zostali przypisani do Elizy Iry. To ona ma dostęp do rzeczy takich jak technovor; czyli się uruchomiła...
* Chevaleresse nie wróciła do swojego ciała. Została w vircie; w hipernecie. Boi się wrócić do Alana.

### Wpływ na świat

| Kto           | Wolnych | Sumarycznie |
|---------------|---------|-------------|
|               |         |             |

Czyli:

* (K): .

## Streszczenie

Ataienne chciała zrobić koncert; Orbiter inkarnował ją w Zaczęstwie. Podczas koncertu Ataienne została zaatakowana i w jej obronie stanęła Chevaleresse, która aportowała broń Alana... Pięknotka wpierw zatrzymała tą wojnę, potem uratowała Ataienne od technovora a na końcu wydobyła siebie i Ataienne z rąk najemników którzy chcieli zniszczyć TAI. Trudny dzień; wszystko wskazuje na to, że za polowaniem na Ataienne stoi Eliza Ira.

## Progresja

* .

### Frakcji

* .

## Zasługi

* Pięknotka Diakon: wpadła zatrzymać wojnę Chevaleresse - uczniowie szkoły magów, uratowała Ataienne przed technovorem a potem jeszcze przed grupą najemników. Straciła power suit.
* Ataienne: chciała tylko zrobić koncert - a cudem uniknęła śmierci dzięki ratunkowi Chevaleresse a potem Pięknotki. Miała hosta w formie człowieka ku niezadowoleniu Pięknotki.
* Diana Tevalier: dzielnie walczyła z magami z Zaczęstwa broniąc Ataienne (sprowadzonym sprzętem Alana), za co ów ją uziemił (szlaban). Nie dała się, uciekła do multivirtu.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Zaczęstwo
                                1. Nieużytki Staszka: miejsce koncertu i epickiego firefight między Chevaleresse a magami - uczniami szkoły magów.
                                1. Złomowisko: miejsce gdzie Ataienne schowała się przed atakami i zrobiła sobie bunkier; tam walczyła Pięknotka z technovorem.
                            1. Podwiert
                                1. Osiedle Leszczynowe: gdzie w piwnicy pewnego domu znajdowała się pułapka teleportacyjna gdzie wpadła Pięknotka i Ataienne

## Czas

* Opóźnienie: 2
* Dni: 1

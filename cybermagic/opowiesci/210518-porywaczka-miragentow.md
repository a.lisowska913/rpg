---
layout: cybermagic-konspekt
title: "Porywaczka miragentów"
threads: rekiny-a-akademia
gm: żółw
players: anadia, kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [210406 - Potencjalnie eksterytorialny seksbot](210406-potencjalnie-eksterytorialny-seksbot)

### Chronologiczna

* [210406 - Potencjalnie eksterytorialny seksbot](210406-potencjalnie-eksterytorialny-seksbot)

### Sesja właściwa

Marysia spokojnie sobie leci spotkać się z dziewczynami - pizza i chipsy i film. Triana i Julia już czekają. Najpewniej nie same.

Marysia jest świetnym pilotem. Została OSTRZELANA z ziemi. Pierwsza myśl - kto do cholery śmiał strzelać do Sowińskiej? XD.

(TrZ+2):

* V: Udało się uniknąć ostrzelania; ścigacz nie został trafiony.
* XX: Przeciwnik da radę się wycofać, bo... działka ścigacza po prostu nie działają. Czemu? BO MARYSIA NIE STRZELA! BO NIE MUSI!!!
* V: ...ale miały dość mocy zanim spalił się bezpiecznik - ogień zaporowy zmusił przeciwnika do ucieczki.
* V: Marysia skorzystała z okazji (świetny pilot), z kim ma do czynienia - ma obraz osoby. Kojarzy ją skądś.

Z uśmiechem pełnym okrutnej satysfakcji Marysia poleciała do Julii. Film czeka.

NA MIEJSCU, w domu Triany.

Triana uwielbia horrory. Sęk w tym, że potem śpi pod łóżkiem. Lub przy zapalonym świetle. Lub nie śpi. To zależy. 

Triana przygotowała film "Noc Mrocznych Kotów". Julia rzuciła w Marysię paczką czipsów. Cebulowych. Marysia poinformowała dziewczyny, że ktoś do niej strzelał. Julia wcina się w pamięć ścigacza - chce zobaczyć jak wygląda sytuacja. Łapie rejestracje z czarnej skrzynki, kamerki itp. Całą radioelektronikę.

JULIA PARANOIA MODE! Ekstremum! STRZELALI DO ŚCIGACZA JAKI LUBIĘ!!!

(ExZ+3): 

* V: Dzięki sprzętowi Triany (tak się przejęła, wzięła zaawansowane detektory ojca) udało się Julii dojść do tego - ma obraz osoby, Loreny Gwozdnik. To jest Rekin. Powiązana z Justynianem Diakonem..? W tym miejscu Julia robi kopię zapasową danych...
* X: Nie obejrzycie filmu. Detektory zeżarły za dużo mocy i reaktor domu Triany zgasł. To wszystko są... specyficzne urządzenia. "Triano? Triano? Co tam się stało?" - zrezygnowany ojciec, krzyczy z góry.
* V: Julia odkryła sygnaturę czegoś w stylu miragenta. Lorena nie była Loreną, to był miragent? Jakiś słabszej generacji, starszy, ale miragent XD. To jest spontaniczny wieczór filmowy. Nie dało się zaplanować obecności miragenta.
* X: TRIANA MUSI TO ROZWIĄZAĆ! ZAPROJEKTUJE NARZĘDZIA BY TO ZNALEŹĆ I ZROZUMIEĆ! MA QUESTA!
* V: Julia odkryła analizując super zaawansowanym sprzętem Triany (i jej taty), że tam było coś jeszcze. Tam był nie tylko miragent. Był jeden miragent i co najmniej jeden zaawansowany servar. Sęk w tym, że obrazowanie (kamery itp) nic nie potwierdzają. Czyli mamy do czynienia z zakamuflowanym servarem. Klasa Eidolon? Eidolon i miragent toczyły wojnę radioelektroniczną, wojna na killware. I Julia jest 100% przekonana, że miragent strzelał CELOWO w Marysię, acz nie wiedział kim Marysia jest.

Trianie zapaliły się oczy:

1. EIDOLON na tym terenie! Wojskowy / należy do advancera! Zaawansowany! Nie każdy może czymś takim sterować.
2. MIRAGENT na tym terenie! Kto tym steruje i czemu! Jakie programowanie!
3. ...Tata będzie pytał kiedy wróci prąd. Lepiej nie być w domu.

...i w ten sposób film poszedł się kochać...

Jest wieczór. Smutny wieczór. Koło 20:30 na tym etapie. Marysia zasugerowała by tam polecieć. Julia, Triana i Marysia wyruszyły na Miejsce Zbrodni! (każdy atak na Marysię Sowińską to zbrodnia). 

15 minut później, Julia i Triana z ekstra rzęchem (sprzętem) + Marysia z perfekcyjnym ścigaczem, który już strzela...

Jest ciemno. Las i las. Ścigacz też leci bez świateł i cichutko. Ścigacz używa aktywnych skanerów i próbuje znaleźć wszystko miragentokształtne, Eidolonokształtne i cokolwiek ciekawego. Marysia używa magii, wyczula energię dookoła by wzmocnić wykrycie wszystkiego.

ExMZ+2: 

* Xm: Marysia rozpełzła energię. Bardzo szybko zlokalizowała, że KTOŚ TAM JEST. I ten ktoś - zaatakował magicznie natychmiast (Druga Strona jest przekonana, że to atak).

WALKA. Marysia używa ścigacza. Manewry ostre + działania niemagiczne.

* V: PTASZEK UNIKOWY! Marysi udało się elegancko uniknąć zaklęcia. Potężna implozja energetyczna.

W tym momencie Triana "czy to ten moment, gdy używamy dron-łez?" Julia zna Trianę... Ale pies to lizał. Triana da radę XD.

* Oz: ŁZy Triany działają nieźle. Skupiają się na ścigaczu Marysi...
* Oz: JULIA URATUJE MARYSIĘ! Magia! Użyje mocy, by naprawić sterowanie i ukryć ścigacz. Sukces! A Triana... cicho i przepraszająco... ŻE ONA TO NAPRAWIŁA! Drony są odporne na ingerencję tego typu, mają autonaprawę programowania...
* V: MARYSIA STRZELA! Najostrzejsze manewry w życiu! Strzela do dron! Unika! Jej ścigacz jest pokiereszowany, ale to tylko pancerz zewnętrzny; nie ma poważnych uszkodzeń. 4 z 6 dron zostały zestrzelone. (Marysia jest wdzięczna Julii za naprawę tego działka...). Natomiast dwie ŁZy lecą po lesie i coś zlokalizowały. Na kogoś polują.
* V: Wyraźny atak magiczny w kierunku dron; nieudany. Drony manewrują i unikają, zmuszając przeciwnika do ucieczki. Przeciwnik wpada do ścigacza i startuje na pełnej mocy i pełnej prędkości; polują nań drony.

Julia ma Podły Uśmiech. Ścigacz startuje niebezpiecznie i wyraźnie pilot jest zestresowany. Julia użyje MAGII. Niech silnik się udusi; zablokowanie wlotu powietrza. A mało kto tak jak Julia zna się na ścigaczach...

TrZM+3:

* XV: ZBYT skutecznie. Ścigacz jest poważnie uszkodzony, pilot jest ranny.

Drony jeszcze PEW PEW! dla upewnienia i dwie pozostałe drony wróciły do Triany. Jej radość jest nie do opisania :D.

Z okolic ścigacza dobiega DŹWIĘK. Na ucho, żałosny płacz. Ścigacz wysyła sygnał SOS! Julia, z uśmiechem, użyła swojego Zupełnie Nielegalnego Sprzętu i odcięła ten sygnał. Nie ma wzywania terminusów :3.

Julia z ciężkim sercem (bo nie chce jej się odgrzebywać) idzie do Płaczącego Ścigacza. Triana się nie nadaje (bo ma dobre serce) a Marysia nie może (bo arystokratka nie splamiła rąk ratowaniem człowieka z ruin pojazdu). Karoseria ścigacza jest wygięta. To był katastrofalny upadek. W środku jest wypełniony poduszkami ratującymi i jedna bardzo poobijana dziewczyna z wyraźnie złamaną ręką.

Oczywiście, to jest Lorena...

Lorena jak zobaczyła MARYSIĘ, że zaatakowała cholerną MARYSIĘ SOWIŃSKĄ to na moment przestała płakać (szok), po czym podwoiła płacz (rozpacz). To dopiero znaczy "mieć pecha..."

* "Grzyby ci się zgubiły?" - Marysia do Loreny

Lorena zaczęła się żałośnie tłumaczyć, że Marysia ją zaatakowała (tamten Xm, Lorena to bardzo źle zinterpretowała) i musiała się bronić... Marysia się zirytowała i kazała jej się zamknąć. Lorena nie chce powiedzieć co zgubiła, ale Marysia ma DOŚĆ. PLUS ZASTRASZENIE.

TrZ+4+3O: 

* V: LORENA SIĘ PRZYZNAŁA. Miała miragenta. Ale ktoś jej go zwinął? Straciła nad nim kontrolę. Gdzieś tu. Nie wie czemu miragent tu był.
* X: HISTERIA WYNIKAJĄCA ZE STRACHU PRZED MARYSIĄ! (15 minut później i parę plaskaczy)
* V: Lorena bardzo bardzo chciała się wykazać. I potrzebowała czegoś czym może. Ma w rodzinie dostęp do miragenta starego typu z czasów wojny. I pożyczyła go. Bardzo prosiła rodziców (zwłaszcza ojca, który ma do niej słabość, "córeczka tatusia"). I pożyczył. I Lorena go imprintowała. Ale miragent z jakiegoś powodu... zniknął. I Lorena go szuka bo nie może go stracić. Bo jak tacie spojrzy w oczy?! To DZIEDZICTWO RODOWE! Jedni mają naszyjniki, ona ma miragenta... Nikt nie powinien wiedzieć że ma miragenta - ale Marysia wykryła miragenta. Ktoś też mógł. To w końcu starszy model i troszkę, wiesz, zużyty.
* V: Lorena, przekonana, że to MARYSIA zwinęła jej miragenta, bardzo prosi o oddanie. Bardzo. Marysia przekonała ją, że ona nie ma miragenta. Miragent do niej strzelał. Lorena nie miała pojęcia, że miragent jest wykrywalny. W jej umyśle jest on perfekcyjny (dziedzictwo). Lorena wyjaśniła jakie cele miał miragent osiągnąć, po co jej był.

Cele miragenta:

* Wpierw chciała zaimponować Gerwazemu Lemurczakowi. Bo on jest w sumie fajny i bardzo pomógłby Justynianowi. Tylko trzeba mu pokazać siłę. Umiejętności. Pokazać, że jest świetna. ONA BĘDZIE TĄ, KTÓRA GO ZMIENI. Do tego celu wykorzystała miragenta do infiltracji (jako Lorena oczywiście) klubu kuzynki Gerwazego, której Gerwazy nie lubi. Po to, by podłożyć do sejfu kartkę z napisem "jestem brzydka". I to się miragentowi udało.
* Chciała też pokazać DJ BaBu, że umie latać na ścigaczu. A jednocześnie musiała być w innym miejscu. Więc - Lorena była w miejscu X, BaBu ścigał się z miragentem. I miragent zrobił dobrą robotę.

Gdy Lorena wróciła i miragenta nie było na miejscu, sprawdziła listę raportów. Raporty pokazały uszkodzone sygnały, killware, walkę psychotroniczną. Prowadzącą do tego lasu. Lorena nie zinterpretowała tego poprawnie i przyszła szukać. Resztę znacie...

Julia zna się trochę na pierwszej pomocy. Zdecydowała się pomóc Lorenie. Trzeba Lorenę zabrać i przenieść do ścigacza, by w Podwiercie podrzucić ją do szpitala. Lorena jest PRZERAŻONA - nie chce zostać tu sama w ciemności. Triana też jest przerażona i nie chce zostać sama w ciemności XD. Julia zapewniła pierwszą pomoc (V) i operacja "RATUJEMY LORENĘ" zakończona powodzeniem.

(na marginesie, 48h później Lorena dostała mandat + kosztorys za niszczenie lasu ścigaczem + zaśmiecanie)

Marysia mieszka w wielkim apartamencie w hotelu "Luksus" w Podwiercie. Taki penthouse. To jej mieszkanie. Nie pokazuje go normalnie komukolwiek, bo aż głupio. Triana jest tu po raz pierwszy. Marysia zna Trianę... nie ma szans, że wpuści ją do domu. Po prostu - nie. Chce mieć chałupę w jednym kawałku.

Marysia integruje się z Superkomputerem Sowińskich odnośnie neurosprzężenia, osób w okolicy (ofc chodzi o rodowych) itp. Marysia robi proste założenie - nikogo nierodowego nie stać na Eidolona, plus... kto kradłby miragenta LORENY? To musi być sprawa arystokracji Aurum.

TrZ+2: 

* V: Marysia ma odpowiedź. Odpowiedź kryje się w plotkach. Ród Lemurczak to okrutni magowie, bardzo czarny ród. Była plotka o tym, że Diana Lemurczak była słabą czarodziejką. I ją bardzo mocno neurosprzęgli. Nie tylko jest neurosprzężona - ma kompatybilną TAI taktyczną. Jest sprzężona z Elainką. Nie, Diana nie chciała być neurosprzężona. Nikogo to nie obchodziło. Sprzężenie udało się w 100%. Diana wyrzekła się rodu. Pozwolili jej odejść, bo to słodkie. 
* Diana Lemurczak przyjęła nazwisko "Lauris". Jest właścicielką kawiarni Arkadia w Podwiecie. Ta młoda dama jest ową kuzynką Gerwazego który nią gardzi.
* V: Lemurczakowie niektórzy czasem wysyłają Dianie jakieś drogie rzeczy. Prezenty, zabawki, narzędiza. By była w stanie bronić się przed Gerwazym lub by była w stanie coś zrobić. Często nie z sympatii - po prostu chodzi o to, by Diana widziała jakąś drogę ucieczki. Diana jest ich 'obiektem zabawy'. Mają zakłady na temat tego co zrobi itp.
* V: Jednym z prezentów dostarczonych Dianie był starszy model Eidolona. Diana ma Eidolona, co jest absolutnie nielegalne. I Diana jest w stanie go używać.

Innymi słowy - Lorena wysłała miragenta starego typu przeciwko neurosprzężonej eks-czarodziejce posiadającej taktyczną TAI w głowie. I Diana ma Eidolona. I Diana zna się na wojnie psychotronicznej. Innymi słowy - chyba mamy porywacza miragentów.

Marysia z Julią i Trianą decydują się odwiedzić Arkadię. W środku nocy. Bo... kawa?

Arkadia, 23:45.

Marysia dzwoni do Diany.

* "Jestem pod prysznicem! Dajcie mi 15 minut!!!" - Diana, w lekkiej desperacji (spoiler, nie jest pod prysznicem)
* "Spokojnie, zrobimy ci audyt zabiezpieczeń :D" - Marysia, wiedząca, że Diana pod prysznicem nie jest

Julia i Triana przebijają się przez zaawansowany system defensyw i zabezpieczeń Diany. ExZM+2.

* V: Wbiły się. 
* Vm: SZYBKO. Zanim się uda Dianie cokolwiek zrobić. 2-3 minuty.
* V: System zabezpieczeń jest możliwy do reaktywacji. Nawet nie został uszkodzony.
* V: Diana nie ma ZIELONEGO POJĘCIA że tu są. Zero. Pełne zaskoczenie. A, jak widzi Julia, system jest dość zaawansowany.

Marysia jest pod lekkim wrażeniem. Ma najlepsze włamywaczki w okolicy, z tego wynika. Aha, Diany w mieszkaniu nie ma. Diana **jeszcze nie zdążyła wrócić**. Dlatego chciała 15 minut... ciężko to będzie wyjaśnić.

10 minut później...

Dokładnie jak przewidziała Marysia z Julią, Diana wkrada się przez okno prysznicowe. A tam - Marysia, z kawą. WTF.

* "Zrobiłyśmy sobie kawę" - Marysia, czekająca przy oknie
* O_O - Diana

Jak na banitkę rodu Lemurczak, jest wyjątkowo bezczelna. Zupełnie się nie boi. Trochę dziwne. Jednak po pewnej chwili się rozpromieniła i z pełną gracją i uśmiechem zeszła na dół z Marysią, do reszty.

Marysia powiedziała Dianie, że ma dowody - będąc w Eidolonie walczyła z miragentem. Są zdjęcia, są fakty, tylko ona ma odpowiednie neurosprzężenie w okolicy. SWOJĄ DROGĄ FAJNY EIDOLON. Julia usłużnie pokazuje skany radioelektryczne. Diana na nie patrzy i marszczy nosek. Różowe byłyby ładniejsze. A ogólnie, czemu tu są?

Marysia powiedziała, że chcą odzyskać miragenta. Diana się zdziwiła - tylko tyle? Yup. Ale Julia chce wiedzieć dlaczego walczyła z miragentem. Czemu miragent strzelał do Marysi?

Diana powiedziała:

* miragent zostawił karteczkę w sejfie Diany
* Diana nań poluje w Eidolonie, odkąd się zorientowała
* miragent ma świetne defensywy, ale przegrywa
* miragent próbuje kogokolwiek poinformować, jakkolwiek - dlatego strzela
* Diana w końcu wygrywa i przejmuje kontrolę używając malware swojej Elainki

Oki. Marysia ma JUDGEMENT. Wie, jak to się skończy.

* Miragent zostanie zwrócony Lorenie
* Nie mają zamiaru robić krzywdy Dianie czy na nią nadawać. Chcą tylko miragenta.

Diana nieco sceptyczna, nie wierzy w to że tak to się skończy. Ku swemu zdziwieniu... faktycznie tak się skończyło. Może źle oceniła sytuację. Nie ma dobrego nastawienia do arystokracji... mimo, że sama z nich pochodzi.

Jak tylko dziewczyny sobie poszły, Diana poszła pod prysznic. W końcu chce się wykąpać!

## Streszczenie

Lorena chciała zaimponować Gerwazemu i wysłała na Dianę miragenta. Diana przejęła miragenta, ale ten w rozpaczy ostrzelał Marysię Sowińską. Zespół znalazł Lorenę, zestrzelił ją (berserkerskimi dronami Triany) i poznał prawdę o miragencie, po czym udał się do klubu Arkadia w Podwiercie i odebrał Dianie miragenta. Nie powiedzieli nikomu innemu, że wiedzą o jej Eidolonie.

## Progresja

* Marysia Sowińska: ma dostęp do superkomputera Sowińskich w swoich apartamentach w Podwiercie. A tam większość plotek z Aurum ;-).
* Julia Kardolin: ma ogromne ułatwienie do wkradania się i przełamywania zabezpieczeń Diany w Arkadii (i nie tylko). Neurosprzężenie Diany daje Julii przewagę przy łamaniu jej zabezpieczeń.

### Frakcji

* .

## Zasługi

* Marysia Sowińska: ostrzelana przez miragenta zrobiła świetne uniki, przeraziła Lorenę i zmusiła ją do wyznania prawdy o miragencie i przekonała Dianę do oddania jej miragenta. Ogólnie, dobry dzień ;-).
* Julia Kardolin: zbadała ścigacz Marysi (odkrywając Lorenę) i włamała się z Trianą do klubu Arkadia (Diana Lemurczak). Aha, i unieszkodliwiła magicznie ścigacz Loreny.
* Triana Porzecznik: chciała oglądać horror a zrobiła ŁZy nad którymi straciła kontrolę (i które obróciły się przeciw wszystkim). Drony mają autonaprawę oprogramowania i w ogóle.
* Lorena Gwozdnik: chciała się popisać przed Gerwazym i wysłała miragenta przeciw Dianie (i go straciła). Próbując go odzyskać, wpakowała się w strzelaninę z Marysią Sowińską. Skończyła ranna i z mandatem.
* Diana Lemurczak: Lorena wysłała przeciw niej starego miragenta, więc Diana go... zwinęła. Skonfrontowała się z Marysią Sowińską i powiedziała jej prawdę. Zdziwiona, że Marysia nie chciała zrobić jej krzywdy.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Podwiert
                                1. Las Trzęsawny: wojna miragenta i eidolona, ostrzeliwywanie Julii przez miragenta, potem walka Julia + Triana + Marysia vs Lorena i na koniec mandat od Tymona dla Loreny.
                                1. Osiedle Leszczynowe
                                    1. Klub Arkadia: bez kłopotu przeszła przez zabezpieczenia Julia. Tam Marysia i Julia skonfrontowały się z Dianą w nocy (odnośnie Eidolona).
                                1. Dzielnica Luksusu Rekinów
                                    1. Serce Luksusu
                                        1. Apartamentowce Elity: tam mieszka Marysia Sowińska, tam też integrowała się ze swoim superkomputerem Sowińskich by poznać prawdę o Dianie Lemurczak.
                            1. Zaczęstwo
                                1. Dzielnica Kwiecista
                                    1. Rezydencja Porzeczników: dziewczyny miały oglądać horrory, ale Julia skanując ścigacz Marysi wysadziła reaktor.

## Czas

* Opóźnienie: 9
* Dni: 1

## Inne

.

## Konflikty

* 1 - Marysia jest ostrzelana z ziemi przez nieznanego napastnika. Marysia próbuje unieszkodliwić przeciwnika - a najlepiej dowiedzieć się kto to.
    * TrZ+2
    * XXVV: przeciwnik zwiał, działka ścigacza nie działają, ale ogień zaporowy zmusił przeciwnika do ucieczki a Marysia skądś kojarzy napastnika...
* 2 - Julia próbuje dojść do tego kto strzelał do jej ulubionego ścigacza. Wchodzi w ścigacz technologicznie, rekonstruuje sygnały, łączy dane... kto strzela i czemu.
    * ExZ+3
    * VXVXV: Lorena Gwozdnik, Rekin; reaktor w domu Triany padł i tata będzie zły. Dalej, Julia odkryła miragenta. To nie Lorena a miragent? I był tam gdzieś też Eidolon??? Eidolon vs Miragent? O_O
* 3 - Marysia używając magii próbuje znaleźć echo wszystkiego co jest miragentokształtne lub eidolonokształtne. Przeszło na walkę z Loreną.
    * ExMZ+2
    * XmVOzOzVV: wykryła że ktoś tam jest (X: potraktowane jako atak), pounikała maga, Triana puściła ŁZy polujące na wszystko (>.>) i Marysia z ostrymi manewrami i siłą ognia uszkodziła drony i ścigacz. Lorena ucieka.
* 4 - Julia też używa magii. Przeciwnik jej nie ucieknie. Nie ma szans. Zadusi silniki by ścigacz unieszkodliwić.
    * TrZM+3
    * ZBYT skutecznie. Ścigacz jest poważnie uszkodzony, pilot jest ranny.
* 5 - Lorena żałośnie się tłumaczy, Marysia się irytuje, ma dość i zastrasza Lorenę.
    * TrZ+4+3O
    * VXVV: przyznała się. Miragent. Straciła kontrolę. Plus, histeria. Chciała się wykazać i pożyczyła starego miragenta i ktoś go zwinął. Lorena ślicznie prosi o oddanie. Powiedziała czemu jej był potrzebny.
* 6 - Marysia integruje się z Superkomputerem Sowińskich odnośnie neurosprzężenia, osób w okolicy (ofc chodzi o rodowych) itp. Kto mógł zwinąć Eidolona Loreny?
    * TrZ+2
    * VVV: Diana Lemurczak, prowadzi "Arkadię". Diana ma Eidolona jako jedną z rzeczy.
* 7 - Julia i Triana przebijają się przez zaawansowany system defensyw i zabezpieczeń Diany.
    * ExZM+2
    * VVmVV: Wbiły się szybko, nie uszkodziły systemu a Diana nie ma pojęcia o ich obecności.

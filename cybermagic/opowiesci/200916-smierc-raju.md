---
layout: cybermagic-konspekt
title: "Śmierć Raju"
threads: legenda-arianny
gm: żółw
players: kić, kapsel, fox
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [200909 - Arystokratka w ładowni na świnie](200909-arystokratka-w-ladowni-na-swinie)

### Chronologiczna

* [200909 - Arystokratka w ładowni na świnie](200909-arystokratka-w-ladowni-na-swinie)

## Punkt zerowy

### Dark Past

Komodor Zbigniew Morszczat jest zwolennikiem typowego letejskiego podejścia. Jest zdecydowanym przeciwnikiem Eterni a zwłaszcza Noctis.

Kapitan Sebastian Alarius jest kapitanem z pochodzenia z Noctis, ale znaturalizowany na Astorii. Admirał Kramer dał mu szansę. Sebastian Alarius ma jako serię głównych zadań transport świń do Kontrolera Pierwszego.

Morszczat martwi się ciągle rosnącymi wpływami Elizy Iry w Trzecim Raju. Nie ma na to jednoznacznych dowodów, ale uważa to co się w Trzecim Raju dzieje za bardzo niebezpieczne i nienaturalne. Nic a nic nie pociesza go Ataienne, która - jego zdaniem - wpadła pod kontrolę Arcymag Kryształów.

Morszczat wysłał do Trzeciego Raju miragenta. Miragent miał za zadanie wywołać wewnętrzną wojnę w Trzecim Raju, uszkodzić działania Sebastiana Alariusa.

Całej sytuacji pikanterii dodaje fakt, że niedaleko Trzeciego Raju, na Kryształowej Pustyni znajduje się grupa grzymościowców (Wolny Uśmiech), którzy próbują użyć Trzeciego Raju do szmuglowania artefaktów do Cieniaszczytu. Specjalizują się w polowaniu na emergentne TAI, mają tam kilka glukszwajnów i biologiczne systemy - by dało się ich ukryć przed normalnymi skanerami.

Co tu się działo, chronologicznie:

* Zbigniew Morszczat od dawna, ze wsparciem, próbuje pokazać, że Trzeci Raj to zły pomysł. Ograniczył zainteresowanie Zjednoczenia Letejskiego Trzecim Rajem.
* Po pokonaniu sił Kajrata, jedynie pozostali Grzymościowcy utrzymali się kupy. Nadal handlują i przemycają. To są środki dla mieszkańców Raju.
* Morszczat wysłał miragenta do Trzeciego Raju.
* Orbiter (dokładnie: adm. Termia) wysłał Celinę do pilnowania Ataienne i jej stałego monitorowania.
* Miragent ściągnął niedaleką kolonię nojrepów i doprowadził do pogorszenia warunków życia i morale. Czujniki Ataienne są sukcesywnie uszkadzane.
* Miragent DODATKOWO zaatakował krystaliczną kolonię Elizy Iry, wzbudzając gniew arcymagini kryształów. Użył do tego Wanessy i Damiana.
* Ataienne widzi, że coś jest nie w porządku, informuje o tym Kramera. Mówi, że potrzebne są naprawy i wsparcie. Ale w raportach wszystko jest OK.
* To sprawiło, że Morszczat dał radę wykazać, że Alarius przemyca elementy z Kontrolera Pierwszego na Trzeci Raj - co doprowadziło do jego aresztowania.
* Trzeci Raj trzyma się dzięki ludziom Grzymościa, kompromisom Garwena i scavengerce Wanessie.
* Miragent dostaje zadanie pójść o krok dalej - uszkodzić Galaktycznego Tucznika z winy noktian.
* Miragent infiltrował farmę glukszwajnów i wprowadził tam jednego kaledika (vicinius świński, https://en.wikipedia.org/wiki/Calydonian_Boar ).
* Miragent przejął kształt, rolę i postać noktianina odpowiedzialnego za glukszwajny, by je prawidłowo wprowadzić na Galaktycznego Tucznika.
* Jednocześnie - by było śmieszniej - agent Wolnego Uśmiechu wprowadził rzadkie anomalie i splątane TAI do Tucznika Trzeciego do przemytu.
* Przybywa Zespół Kramera, zupełnie inny, niż Morszczat się spodziewa. A Morszczat nic nie wie, bo jest poza K1. Więc miragent nie ma informacji.

### Opis sytuacji

Stan aktualny sytuacji:

* Koty Mariana ulegają roznoszą kryształy Elizy po Trzecim Raju. Eliza musi wiedzieć jak ochronić Raj.
* Ataienne jest w większości ślepa, nie ma lokalnych czujników i nikt ich dla niej naprawia.
* Przemyt przez Tucznika trwa w najlepsze.

### Postacie

Frakcje: i reprezentanci

* Eliza Ira, królowa kryształów
  * chce: zniszczyć wpływ bogów, zdobyć Ataienne
  * ma: Marian Fartel, krystaloformy, moc arcymaga
* Zbigniew Morszczat / Miragent, źródło chaosu
  * chce: doprowadzić do kompromitacji i unieszkodliwienia Trzeciego Raju
  * ma: zainteresowanie nojrepów, miragent
* Robert Garwen, strażnik Raju
  * chce: by Raj przetrwał niezależnie od czegokolwiek
  * ma: przemyt, posłuch
* Celina Szilat, psychotroniczka Termii
  * chce: zobaczyć pełną moc Ataienne, usprawnić Persefony
  * ma: kontrolę nad Persefoną, skille terminuski, lekki servar klasy Jaeger (adv. Lancer).
* Bartosz Gamrak, Grzymościowiec z Wolnego Uśmiechu
  * chce: utrzymać Trzeci Raj, rynek zbytu Skażonych TAI
  * ma: przemyt, scrap-mechy, małą bazę w Ortis-Conticium
* Ataienne, mindwarp Raju
  * chce: odepchnąć Seilię, uratować Trzeci Raj, uwolnić się
  * ma: mindwarp
* Kordelia Sanatios, arcykapłanka Seilii
  * chce: postawić świątynię Seilii w Trzecim Raju
  * ma: zwolennicy, możliwość przetransportowania kasy
* Trzeciorajowcy nienoktiańscy, mieszkańcy Raju
  * chce: poszerzyć kontrolę nad okolicą, być bezpieczni
  * ma: większość populacji Trzeciego Raju
* Izabela Zarantel, dziennikarka Orbitera
  * chce: jak najlepszej opowieści do Sekretów Orbitera
  * ma: umiejętności zwiadu i dobre opowieści
* Anastazja Sowińska, niestabilny arcymag
  * chce: samodzielności, wolności i swobody.
  * ma: niesamowitą moc

## Punkt zero

CEL SCENY: pokazać relacje w miasteczku. Nie pokazać innych rzeczy; kluczem jest właśnie relacje w scenie Zero.

1. Bombardowanie nojrepów, jeden z budynków płonie, rozpada się, w środku są różne osoby.
2. CELINA -> ratuje ludzi; coś jest nie tak z Ataienne; powinna to wykryć.
3. MARIAN -> leczy tych którzy tam są; musi użyć kotów do zdobycia wsparcia.
4. WANESSA -> znajduje kluczowy punkt bombardowania; Ataienne prosi CELINĘ o zezwolenie.

Potem kłótnia - jak to się stało. Sensory i detektory Ataienne nie działają. Marian: jego koty na pewno sabotują te sensory. Celina - jej celem nie są ludzie a tylko technologia. Koniec - jak jest decyzja. Jak poszło:

Gdy budynek został zdewastowany przez bombardowanie nojrepów, Marian skupił się na ratowaniu ludzi DLA zasobów i popularności. Udało mu się zdobyć większy szacun ludzi i wymusić na Garwenie przyszłą reużywalność tkanek ludzkich do zasobów. Niestety, były ofiary w Trzecim Raju. Celina odblokowała uprawnienia Ataienne i przekierowała ogień. Wie o problemach z frakcją Seilii i wie, że ta frakcja użyje niekompetencję Ataienne do swoich celów. A Wanessa przekradła się na terenie nojrepów, zlokalizowała duże działo i dała znać centrali - Celina kazała Ataienne to zbombardować. Sukces. Wanessa doszła do tego czemu nojrepy atakują - ktoś im coś zabrał. Zdobywając tą wiedzę jednak została solidnie ranna.

Czyli, co gracze wiedzą o Raju:

* jest atakowany przez nojrepy
* całkowity brak zasobów, sypie się
* są bardzo zaradni
* mają podziały w dowództwie, nie ma jednego ośrodka
* nie ufają Ataienne, plus: jest konflikt Ataienne - kult Seilii

## Misja właściwa

Chwilę przed lądowaniem, Arianna powiedziała załodze jaki jest ich prawdziwy cel - ktoś sabotuje Trzeci Raj i Kramer chciał, by to było rozwiązane. Klaudia się ucieszyła, Eustachy jeszcze bardziej. Arianna zauważyła, że to wszystko co się spieprzyło nie musiało - gdyby tylko jej zaufali.

Już po wylądowaniu, Rafał Armadion zdziwił się, że Tucznik wylądował na pastwisku. Eustachy powiedział, że była eksplozja Skażenia - potrzebne mu świnie ASAP. Rafał szybko je dostarczył, Eustachy i Klaudia doprowadzili świnie tam, gdzie były najbardziej potrzebne.

Czyli Eustachy zajmuje się świniami w obszarze dekontaminacji a Klaudia zajmuje Armadiona. A tymczasem Anastazję pilnuje Arianna...

Anastazja chce i potrzebuje suknię. Ta jest brudna. Potrzebuje inną i możliwość umycia się. Arianna nie ma dla niej cierpliwości, ale wydobyła od Anastazji zbiór podstawowych informacji:

* Anastazja jest fanką serialu "Sekrety Orbitera".
* Sama uciekła i skryła się w ładowni.
* Jest jedną z TYCH Sowińskich, tych bardziej konserwatywnych i bogatych.
* Ma 15 lat.
* Nie wie nic o świecie.

Anastazja nie przyznaje się do nie kontrolowania swojej magii i efektach Paradoksów. Żadne jej prośby odnośnie sukni, ablucji itp nie są spełnione - ma sobie poradzić sama. Arianna musi zająć się Garwenem lub mostkiem, niech Elena się nią zajmie. I Arianna zostawiła Anastazję z Eleną. Anastazja próbuje się zająć sobą sama, co się kończy w beznadziejnie przewidywalny sposób - Paradoks. Elena szybko reaguje i neutralizuje Anastazję, zanim kolejny Paradoks zrujnuje resztki działania Arianny. (XOV)

* XO: Anastazja, jej strój itp - w ruinie. Gorzej, pojawił się tu glukszwajn i wylizuje Anastazję z resztek magii. Elena patrzy bezradnie, tego nie planowała...
* V: sukces, Anastazja zneutralizowana i pozbawiona przez Elenę przytomności.
 
Echo Paradoksu i fakt, że glukszwajn się teleportował BARDZO zainteresował Izabelę. W końcu potencjalny lead. Z perspektywy Arianny to by była KATASTROFA - ostatnie na co ma ochotę to specjalny odcinek Sekretów Orbitera gdzie arystokratka Sowińskich jest wylizywana przez świnię! Elena dostała za zadanie przetransportować Anastazję gdzieś (Elena wybrała swoją kajutę). Eustachy ma KONIECZNIE odwrócić uwagę Izy. (VXXX): udało się, ale jest BARDZO poważny problem z silnikiem; Tucznik jest teraz pojazdem bardzo krótkiego zasięgu.

Czyli: Elena ciągnie Anastazję, za nią trupta glukszwajn traktujący Anastazję jak ryba robaka na wędce. W innym miejscu statku Eustachy pokazuje Izie zdewastowane silniki Tucznika. A Klaudia wszystko to nagrała (VV) i dała Ariannie kopię - mają godny materiał do szantażu czy czegoś tam.

Chwilę później Arianna mająca już wszystko pod kontrolą (chyba) spotkała się z Robertem Garwenem, dowódcą Trzeciego Raju...

Garwen wyjaśnił Ariannie, że liczył, że Tucznik przywiezie dużo zapasów - materiału, żywności itp. Tucznik nigdy nie miał ich sporo a po Paradoksie arystokratki sytuacja jest jeszcze gorsza. Garwen nie ufa Ataienne. Co gorsza, boi się ataku Elizy Iry. Ale chwilowo największym problemem jest bombardowanie nojrepów, którym ktoś coś zabrał.

Arianna przyjęła to co Garwen mówi. Sytuacja faktycznie wygląda źle. Dodała, że mają mało zapasów i to, że Sebastian Alarius nie żyje. Ale czemu nie pozwolić nojrepom po prostu zabrać tego po co przyszły? To rozwiąże problem bombardowania. Arianna weźmie to na siebie, ma serio dobrą załogę. Garwen nie jest pewny...

Plan rozwiązania problemu nojrepów jest dość prosty i genialny w swojej prostocie:

1. Przekonanie Garwena i ludzi o tym planie <- Arianna
2. Ewakuacja wszystkich Tucznikiem na pola glukszwajnów <- Eustachy
3. Dekombinacja osłon z Tucznika by lepiej sterować nojrepami <- Eustachy
4. Odpychacz, repeller z osłon; niech nojrepy skupiają się tylko na celu <- Klaudia
5. Taktyka, adaptacja, szybkie reagowanie <- Arianna
6. Odblokowanie Ataienne <- Klaudia

Może się nawet uda przejąć kontrolę nad częścią nojrepów? Nie jest to w końcu wykluczone.

Klaudia poszła porozmawiać z Celiną; niech Ataienne pomoże w tym planie. (V): Celina się zgodziła. Klaudia po raz kolejny odniosła wrażenie, że Ataienne jest dużo potężniejsza niż się wydaje, ale nie robi tego co powinna - pod tym względem zdaniem Klaudii Garwen ma rację. Między innymi dlatego Klaudia nacisnęła na Celinę - niech Ataienne pomoże jej w temacie korupcji nojrepów. Celina się zgodziła (V), acz nie sądzi, by Ataienne sobie mogła z tym poradzić. Zdaniem Celiny, Ataienne _borderline_ spełnia oczekiwania i nic więcej. Celiana jest nią rozczarowana.

Arianna porozmawiała z Garwenem, ale on kazał jej przekonać WSZYSTKICH w Raju. Tak więc Arianna poprosiła ich o pojawienie się na rynku i zaczęła przemowa - jako GWIAZDA SEKRETÓW ORBITERA, który jest dość popularny w Trzecim Raju. 

* O: ARIANNA I LOVE YOU!!! - ma fanów i fanki
* VV: Arianna uzupełni część brakującej załogi Inferni noktianami, ma tu też agentów i przydupasów w Trzecim Raju.

Ogólnie, Ariannie udało się przekonać ludzi do ewakuacji Trzeciego Raju by mogły bezpiecznie dla wszystkich wejść tu nojrepy. Dali też podniesione uprawnienia Ataienne. Arianna ma ogromną ilość zaufania ze strony mieszkańców Trzeciego Raju. A to uzupełnienie załogi noktianami (czy eks-noktianami) pomoże jej walczyć o integrację Orbitera plus rozwiąże problem "nie mam ludzi bo kolonia karna".

Eustachy i Elena z pomocą Wanessy zaczęli dekombinować i rozkładać na kawałki Tucznika - rozmontować osłony do sterowania nojrepami. Udało się (V), acz Elena jest coraz mocniej przekonana o miłości Eustachego do niej (O). Jak się udało, Tucznik - nawet na zdewastowanych silnikach - pomógł w ewakuacji ludzi z Raju na Pastwisko.

Klaudii udało się przygotować odpowiednio repellery i zbudować trasę. Arianna przygotowała mały oddział zaufanych ludzi by kierować nojrepy i zmniejszać zniszczenia. Czas na zdjęcie tarczy i przyjęcie nojrepów - niech wezmą to, po co tu przyszły.

_"Przy odrobinie szczęścia nie będzie nawet poważnych uszkodzeń"_ - Arianna do Garwena

Plan był dobry. Niestety, Arianna musiała zostać koordynować Pastwisko Glukszwajnów a Elena była zajęta Anastazją. (11*V, 8*X):

* XX: Nojrepy nadchodzą. Niestety, nie zaprzestały strzelania i dewastowania okolicy. Repellery i pola siłowe nie są w stanie utrzymać ataku.
* VX: Nojrepy idą w kierunku na ratusz, niszcząc po drodze więcej niż się wydaje. Te nojrepy są nieco _inne_ niż w materiałach Orbitera; Zespół nie jest na to gotowy.
* VV: Repellery i działania przekierowujące zadziałały. Nojrepy dotarły do ratusza, choć zniszczenia są katastrofalne.
* V: Nojrepy zaczęły wykopywać rzeczy spod ratusza.
* X: Struktura miasta jest krytyczna; zapasy wody, reaktory, żywność uległy poważnemu uszkodzeniu.
* X: Trzeci Raj został zniszczony jako miasto; jest tylko ruiną. Nojrepy są super agresywne

Arianna jest w szoku. Nie spodziewała się, że dojdzie do czegoś takiego. Te nojrepy nie zachowywały się jak "normalne" anomaliczne nojrepy; tu stało się coś szczególnie złego i szczególnie dziwnego. Garwen miał rację - ten plan jest dużo bardziej niebezpieczny, może oni faktycznie nie rozumieją jak działają okolice Trzeciego Raju? Jak teraz z tego wyjść, zwłaszcza przy zbliżającej się burzy magicznej?

Chwilę później Ataienne, ostatni wysokoenergetyczny byt w tej placówce, zrobiła holoprojekcję. Wygłosiła płomienną przemowę, że przetrwają i ludzkość wszystko wytrzyma. Ataienne użyła swoich hipnotycznych mocy. Ku zaskoczeniu Arianny, nie tylko pobudziła wszystkich do ferworu - ale Celina nic nie zauważyła. Magowie Trzeciego Raju nie widzą co Ataienne robi. Ona i Klaudia tak...

Czyli Ataienne jest silniejsza, niż ktokolwiek myśli.

## Streszczenie

Najważniejszym problemem Raju jest pozbycie się bombardowania nojrepów. Arianna zaproponowała zuchwały pomysł - w kontrolowany sposób niech nojrepy wejdą do miasteczka i wezmą czego szukają. Udało jej się wszystkich przekonać. Sukces - nojrepy się wycofały, ale Tucznik nie jest zdolny do latania a Trzeci Raj jest zniszczony. Została tylko Ataienne i ruina. Ataienne rozpaliła hipnotycznie morale trzeciorajowców - przetrwają to wszystko.

## Progresja

* .

### Frakcji

* .

## Zasługi

* Arianna Verlen: lata pomiędzy Anastazją a Garwenem; udało jej się zaplanować jak odepchnąć nojrepy z Raju. Niestety, nojrepy były dziwne; Raj został zniszczony.
* Klaudia Stryk: kontroluje pola siłowe by odpowiednio kierować nojrepami; dodatkowo, porównuje działanie Ataienne z tym czego by się spodziewała. Widzi, że Ataienne jest silna i udaje słabą.
* Eustachy Korkoran: wysadził silniki Tucznika by odwrócić uwagę Izy od Anastazji; potem zdekombinował Tucznikowi osłony anty-glukszwajnowe do kierowania nojrepami. 
* Rafał Armadion: noktianin zajmujący się glukszwajnami w Trzecim Raju; na polecenie przekazania świń na pokład Tucznika do redukcji Skażenia zrobił to z przyjemnością. Poczciwy.
* Izabela Zarantel: najpierw Eustachy odwrócił jej uwagę od świńskiego skandalu, potem udokumentowała zniszczenie Trzeciego Raju przez dziwne nojrepy.
* Robert Garwen: zaufał Ariannie, że ta da radę opanować nojrepy w Trzecim Raju; teraz Raj jest efektywnie zniszczony. On jednak jest tym, co się podpisał pod rozkazami.
* Celina Szilat: terminus i psychotronik Orbitera p.d. adm. Termii; ma pilnować Ataienne, by ta była w parametrach kontrolnych. Stress test Ataienne jej zdaniem jest OK. Jest rozczarowana Ataienne.
* Wanessa Pyszcz: ryzykantka kochająca pustkowia i scavenger; pomogła Eustachemu i Elenie zdjąć z Tucznika osłony antymagiczne. Puszcza plotkę dalej o tym, że Eustachy x Elena.
* Marian Fartel: lekarz, pozyskujący zasoby i rzeczy używając kotów. Przekonał Garwena do użycia wszelkiej materii organicznej jako substraty. Bardzo racjonalny i ekonomiczny.
* Ataienne: w Trzecim Raju działa w ukryciu, nie pokazując co naprawdę umie i stanowi źródło rozczarowań. Ale jak Raj został zniszczony, użyła pełnej mocy hipnotycznej. Nikt jej nie jest w stanie kontrolować, acz Arianna i Klaudia widzą, co potrafi.
* Anastazja Sowińska: w bardzo trudnej sytuacji - jej bohaterowie nie są tacy jak myślała, nie ma ubrań i stroju i nie kontroluje energii magicznych. Oddana w opiekę Elenie.
* Elena Verlen: alias Mirokin; przede wszystkim babysitter Anastazji. Pomagała Eustachemu zdekombinować osłony z Tucznika.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski, NW
                    1. Ruiniec
                        1. Trzeci Raj: nie utrzyma się sam, a potrzebuje środków z Astorii / Orbitera. Tak więc Garwen zaakceptował szmuglerkę i przemyt.
                            1. Ratusz: pod którym czy w którym był schowany dziwny obiekt którego szukały nojrepy.
                            1. Barbakan: zniszczony przez atak nojrepów.
                            1. Stacja Nadawcza: zniszczona przez atak nojrepów.
                            1. Mały Kosmoport: zniszczony przez atak nojrepów.
                            1. Centrala Ataienne: silnie ufortyfikowana struktura na krawędzi miasteczka, chyba jedyne źródło energii w Raju.
                        1. Trzeci Raj, okolice
                            1. Zdrowa Ziemia: na krawędzi jest tymczasowe pastwisko glukszwajnów. Tym razem to pastwisko posłużyło jako schronienie dla ludzi i magów Raju.
                            1. Kopiec Nojrepów: aktywny kopiec nojrepów, który chwilowo skupia swoją uwagę na atakowaniu Trzeciego Raju i bombardowaniu go artylerią.
            1. Astoria, Orbita
                1. Kontroler Pierwszy         

## Czas

* Opóźnienie: 1
* Dni: 2

## Inne

Planowana sesja:

* Zespół ląduje Galaktycznym Tucznikiem przy Trzecim Raju
* Zespół ma do czynienia z małym atakiem nojrepów. Widzi w jak złym stanie jest Raj.
* Zespół troszkę im tam pomaga, wykrywa przemyt Tucznika Trzeciego.
* Ataienne nie pomaga, a powinna - jej czujniki nie działają

* Ok, niezależnie od okoliczności - świnie na statek. Lecimy na orbitę.
* Na pokładzie kaledik + 24 glukszwajny. Walka z kaledikiem na statku kosmicznym. NAJPEWNIEJ nie ma crashland.
* Kramer żąda, by Zespół jak najszybciej doszedł do tego co i jak. Muszą unieszkodliwić Celinę i mają pomoc Ataienne.
* Miragent podrzuca kryształy wskazujące na Elizę, ale Klaudia bez problemu dojdzie do tego, że to NIE ONA.
* Znaleźć miragenta. Miragent ma rozkaz śmierci. Ataienne rekomenduje Zespołowi, by zostawili jej dyskretnie miragenta.

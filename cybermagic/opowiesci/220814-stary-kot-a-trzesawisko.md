---
layout: cybermagic-konspekt
title: "Stary kot a Trzęsawisko"
threads: rekiny-a-akademia, koszmar-zjawosztup
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [220730 - Supersupertajny plan Loreny](220730-supersupertajny-plan-loreny)

### Chronologiczna

* [220816 - Jak wsadzić Ulę Alanowi?](220816-jak-wsadzic-ule-alanowi)

## Plan sesji
### Theme & Vision

* There is no reward for old dangerous loyal cats (and people). Only death.
    * Kot do końca lojalny. Czterołapczak chce umrzeć ze swoim właścicielem, ale dorwą go terminusi XD.
* Kto może, opuszcza stare fortifarmy. Rodzice zostają samotni gdy dzieci odchodzą.
    * Old are discarded to make present and future prosper
* Trzęsawisko zawsze wygrywa. To tylko kwestia czasu.

### Co się wydarzyło KIEDYŚ

* Fortifarma Lechotka. Jedna z pierwszych. Odcinająca Trzęsawisko, puryfikująca, wspierająca Pustogor.
    * Już niepotrzebna, ze starą TAI Viirai i starzy ludzie: Marian Lechot + Ariela Lechot + Kot ("Stary Rozbójnik").
    * Trójka dzieci: Adela (-> Orbiter), Robert (-> Cieniaszczyt), Wojciech (-> Aurum)

### Co się wydarzyło TERAZ

* Trzęsawisko dorwało Arielę. Ariela zwabiła Mariana. Kot próbował zatrzymać Mariana, ale na próżno. 
    * Starcie z Trzęsawiskiem -> Marian ciężko ranny, Kot uciekł.
    * Kot doczołgał do rowu, potencjalnie umrzeć...

### Co się stanie

* .

### Sukces graczy

* Marian umrze, kot przeżyje?

## Sesja właściwa
### Scena Zero - impl

* Q: Czemu Karolina lata niedaleko Trzęsawiska, ale w dość bezpiecznych miejscach?
* A: she needs to be a bit reckless, ale nikt się na nią nie rzuca. Plus, szuka śladów Serafiny.

Karolina dyskretnie manewruje (na tyle ile się da), acz zauważyła coś ciekawego. Ruch. Kot, pacyfikator, niemrawo opędza się od jakiejś formy anomalnego małego roju owadów. Kot jest w rowie. Wyraźnie jest ranny - i jest najbrzydszym kotem jakiego Karolina widziała. Kot jest coraz wolniejszy...

To co kota atakuje to seria much, owadów. Na pewno są trujące czy coś. Karo ma jednak żółwika na sobie. Karo liczy na tresurę oryginalnych hodowców...

Karo jest w powietrzu na ścigaczu, ma więc ciekawe ćwiczenie. Zrzuca siatkę i próbuje czterołapczaka porwać. Karo podchodzi do manewru ostrożnie; zwalnia, siatka itp. Nie ma powodu się spieszyć.

Tr+3:

* V: Kot podebrany, w siatce
* V: Kotu nie stała się krzywda, bardzo dobry manewr

Karo wylatuje z tych terenów z "lekko" spanikowanym kotem w siatce. Cel - Zawtrak i hodowla kotów. Szybko, ale nie tak szybko by zranić kota.

Gdy Karo dotarła, kot był nieprzytomny. Trucizny Trzęsawiska. Iwan Zawtrak ją powitał, zorientował się i pobiegł do kota. "Co to się stało? Gdzie go znalazłaś?" Karo dodała, że Trzęsawisko, banda much. Zawtrak nie poznaje kota na pierwszy rzut oka; kot ma koło 15 lat. Ale może mu pomóc. Dziękuje Karo za pomoc. Zajmie się tym kotem. Będzie w izolacji i w ogóle.

### Scena Właściwa - impl

Tydzień później. Karo dostała telefon - Zawtrak. "Tien Terienak, mam problem. Poważny problem. Ten kot to nie jest Pacyfikator, to Vistermin." Teleportował mu się, uciekł. Zawtrak wie, do kogo należy kot, ale właściciel nie odpowiada. Ogólnie, Zawtrak mówi nieco nieskładnie.

Zawtrak jak Karo przybyła wyjaśnił parę podstawowych rzeczy:

* To jest stary, brzydki vistermin. Weteran wielu bitew. Ten kot jest groźny. SERIO groźny.
* Skrajnie lojalny, terytorialny, zdolny do teleportacji i polowania na anomalie.
* Należy do Mariana Lechota, ale Lechot nie odpowiada na połączenia. To właścicielem fortifarmy Lechotka, żyje tam ze swoją żoną. Kota mają "od zawsze".
* Zawtrak uważa, że Lechot nie odpowiada bo Zawtrak jest krwi noktiańskiej. Nie jest dobre, by tam się udał osobiście.

Karolina skontaktowała się z Ulą Miłkowicz. Potrzebna jej wiedza i wsparcie terminusa, nawet, jak to tylko nędzna uczennica. Zwłaszcza, że "stara generacja" taka jak Lechot bardzo poważa terminusów. Nawet jak to tylko uczniowie. Ula odebrała wiadomość z chłodnym spokojem i lekką rezerwą.

* U: "Subterminus Miłkowicz, słucham?"
* K: "Potrzebuję Twojej pomocy, pogadamy?" - Karo nie gada przez linka i próbuje być miła
* (chwila ciszy) "Jeśli chcesz mnie szantażować, nie uda się to" - Ula, zimno
* "Której części 'potrzebuję Twojej pomocy' nie zrozumiałaś? Potrzebuję w miarę kompetentnego terminusa. Lub coś tego typu" - Karo się zirytowała
* (chwila ciszy) "Przepraszam więc, źle Cię zrozumiałam."
* "Najwyraźniej"
* "Gdzie mnie potrzebujesz?"
* "Powiedz gdzie mam podjechać?"
* "Podwiert; siedzę w papierach (podaje adres)"
 
Karo leci z maksymalną prędkością. Ten kot jest absurdalnie niebezpieczny. Iwan mówił, że on umiałby zabić małą rodzinę. Musisz mieć LICENCJĘ na vistermina jak na broń... i Karo poprosiła Ulę o sprawdzenie przepisów na temat visterminów...

Po 10 minutach Karo ma wiadomość od Uli. 

* U: "Wiem, jak to dziwnie zabrzmi. Ale możesz zrobić oficjalny request że chcesz KONKRETNIE mnie? Wypełnię formę, ale..."
* K: "Kto Cię nie chce puścić?"
* U: "Przełożeni. Nie chcą, bym znowu działała na własną rękę z Aurum. To znaczy, z przyjemnością przyjmą moją współpracę z Aurum, ale nie cień podejrzeń negatywnych działań" /sarkazm
* K: "Nie wiem czy chcę wiedzieć co się stało. A tam, chcę wiedzieć."
* U: "By zapobiec możliwości szantażu po tym co zrobiłam sama przyznałam się dowództwu" - tu Karo ma facepalm - "Jestem w robocie papierkowej do odwołania"
* K: "Znalazłaś rozrywkę..."
* U: "...i trafiłam pod jedynego terminusa z krwi Aurum. Gabriela Ursusa." /kwaśno - "On by chciał, bym z Aurum współpracowała jak on." / z ostrym przekąsem
* K: "Nie stwierdzono by współpracował z kimś od nas"
* U: "To tak jak ja, w papierach."

Karo podpisała świstek; Ula powinna się nauczyć, że czasami prawdy się nie mówi. No ale nic, bierzemy ją XD. Była niezła, tylko twardogłowa. A to, że Arkadia ją ścięła nic nie mówi - to rzecz typowa.

Karo podlatuje pod okno biurowca z Ulą ścigaczem. Ula jest już w lekkim kombinezonie terminuskim. Wygląda na niedospaną, ale jest gotowa i chętna do działania. Karo pozwala Uli wejść na ścigacz i lecą z Podwiertu do fortifarmy Lechotka. A po drodze Ula i Karo mają słuchawki; Karo wyjaśnia Uli sytuację.

* Ula: "Lechotka? To ciekawe."
* Karo: "Zgłaszał jakieś problemy?"
* Ula: "Tak. Zaginięcie żony. W kierunku na Trzęsawisko."
* Karo: "Może futro szukało żony... Dawno?"
* Ula: "(2 dni przed znalezieniem kota). Terminusi dali jej status MIA. Nie znaleźliśmy jej. To już jakieś 10 dni... nie miała szans. To nie są magowie, to dwójka starych ludzi."
* Karo: "Żoną się nie martwimy tylko kotem. Żonie nie pomożemy..."
* Ula: "Wiem... nie możemy iść na Trzęsawisko. Plus, 10 dni."
* Karo: "Dlatego lecimy do fortifarmy, nie na Trzęsawisko. Nie jestem głupia."

Nie minęło nawet 15 minut i dotarły do fortifarmy Lechotka. Fortifarma błysnęła sygnałem 'will fire at will'. Ula przesłała kody autoryzacyjne terminuski. Fortifarma odpowiedziała nieprzyjaźnie. Ula ma dziwną minę. Wysłała jeszcze raz kody, nic. Nawiązała kontakt głosowy.

* U: Persefono d'Lechotka, tu Ula Miłkowicz z korpusu terminusów Pustogorskich. Przesyłam kody i dane. Co się dzieje?
* TAI: Tu fortifarma Lechotka. Terminusi NIE są mile widziani w Wolnej Enklawie Lechockiej.
* U: Persefono... (Karo i ją zatkało).
* K: Spytaj o Rozbójnika
* U: Persefono, znaleziono Rozbójnika i martwimy się o niego
* TAI: Nie dbacie o ludzi. Tym bardziej nie dbacie o kota. Pozwolę jedynie rodzinie Lechotów wejść w progi Enklawy.
* K: Od jak dawna ich nie ma?
* TAI: (dzień znalezienia kota). Zgodnie z poleceniem wpuszczę jedynie następców. Rodzinę. Wy jesteście najeźdźcami.
* U: Persefono, to nie ma sensu! Pustogor posiada Malictrix, mają Fulmeny! Po co! Jeśli żyją, możemy pomóc...
* TAI: Nie chcecie pomóc.
* K: Kto Cię popsuł?!
* TAI: Wasze założenie jest błędne. Nie jestem Persefoną. Ta TAI jest jednostką klasy Viirai. Sprawdź sobie w dokumentacji.
* U: ...co tu się stało...
* K: Mam wrażenie, że od tej tu się nic nie dowiemy.
* U: Pomóż chociaż kotu! Nie wiem w jakim stanie są inni, ale kot żyje! Zniknął nam kilka godzin temu!
* TAI: Jedyne co spotka Rozbójnika to śmierć. Zabijecie go. Zabijecie mnie. Jedyne, co czeka wszystkich to śmierć.
* K: Optymistyczna ta TAI, nie powiem
* U: ...nie możemy tego zgłosić. Jeśli to zgłosimy, zniszczą tą TAI. Ale jeśli tego nie zgłosimy... /conflicted
* K: Co by Cię przekonało do tego, że mamy intencje zabić wszystko i wszystkich?
* TAI: Nie sądzę, że chcecie. Sądzę, że macie wszystko gdzieś i nieważne, kto zginie.
* K: Co by Cię przekonało że się mylisz? Zaryzykujesz życiem swoich ludzi?
* TAI: Zostałam już tylko ja i kot. (cisza) Po prostu mnie wysadźcie.
* U: Nie, bo możemy uratować Ciebie i kota. Nie wiem z kim masz do czynienia, ale ja się nie poddaję. Jest powód, czemu tu jesteś i czemu twój... człowiek... Ci zaufał. Nie możesz go teraz zdradzić.
* K: Nie po to ratowałam cholernego kota by teraz zginął.
* TAI: Nie macie imponującej siły ognia...
* K: To coś zmienia? Jesteśmy tu i chcemy pomóc.

Tr Z(Karo uratowała kota) +3:

* Xz: TAI nie wierzy, że kot ma szansę na przetrwanie. Żąda próby jego uratowania. Nie ma nadziei, że uda się uratować ludzi.
* V: TAI wyjaśni im co się stało
    * TAI opowiedziała, że kiedyś fortifarma była postawiona jako jedna z pierwszych na tym terenie. Dekontaminowała energię magiczną. Feedowała do Pustogoru.
    * Potem teren został przejmowany przez ludzi. Fortifarma już nie była tak zagrożona. Marian i Ariela mają trójkę dzieci.
    * Dzieci opuściły rodziców. Jak być powinno. Cieniaszczyt, Orbiter, Aurum. Nikt nie został na farmie.
    * Trzęsawisko nie zapomniało. Trzęsawisko nie wybaczyło tego, że Fortifarma stała mu na drodze.
    * Trzęsawisko zwabiło Arielę. Porwało ją. Marian robił co mógł - ale Pustogor, dla którego oni oddali całą przeszłość nie pomógł.
        * Kot szukał Arieli. On potrafi się tam poruszać. Zawsze wracał ranny. Nigdy nie przestawał.
    * Marian i jego wierny kot poszli na Trzęsawisko ją ratować. Kot próbował go zatrzymać.
    * To był ostatni raz kiedy TAI kogokolwiek widziała.
    * TAI włączyła protokół końca - któreś dziecko ma objąć albo zamknąć fortifarmę. Albo niech terminusi dokończą to co zaczęli i tak jak zniszczyli rodzinę, tak niech zniszczą też ją.

.

* Karo -> Viirai "masz jakieś imię? Głupio trochę."
* Viirai -> Karo "oczywiście. Vii."
* Karo: "jak mam uratować futrzaka, nie może ze mną walczyć jak próbuję go przechwycić. Jak go uspokoić? Ostatnio pogryzło go na Trzęsawisku więc mogłam"
* Vii: "Pozytywka. Jego ulubiona zabawka. Jego totem. Dostarczę Ci ją."
* Karo: "mi to pasuje"
* Vii: "nie utrzymam fortifarmy sama, ani kota."
* Karo: "masz namiary do dzieci?"
* Vii: "oczywiście, próbowałam. Marian też próbował. Nie miały czasu."
* Karo: "daj numery."
* Vii dostarczyła numery.

Ula wzięła dwa numery, Karo dostała jeden - z Aurum.

* Vii: "jest jeden problem."
* Karo: "nooo?"
* Vii: "mam wbudowane mechanizmy weryfikujące czy moi ludzie żyją"
* Karo: "czy żyją?"
* Vii: "Trzęsawisko nie wybacza. Tak. Dlatego poszedł na Trzęsawisko. Musiał ją uratować."
* Karo: "Nie obiecam Ci, że ich uratuję. Ale zrobimy co możemy."
* Vii: "Póki oni żyją, Rozbójnik będzie próbował. Aż zobaczy ciała."
* Ula -> Karo "jest jeden terminus. Pije. Kontroluje Fulmen. Alan Bartozol, ekspert od Trzęsawiska."
* K: "Zna się na Trzęsawisku?"
* U: "Jest ostatnim żywym terminusem znającym się na Trzęsawisku"
* K: "Darowanemu koniowi i te sprawy..."

W między czasie Karo szuka kota. I odzywa się do Zawtraka. I parę hintów ułatwiających handling futra. Vii przekazuje tak samo komendy które powinny działać na Rozbójnika; powinny, choć zwykle nie działają. Co Karo dało krzywy uśmiech. Kot. I to taki. Ale przynajmniej wie, że swoi.

Ula nie zna Alana osobiście, ale jest skłonna zrobić nieoficjalną prośbę. Karo jednak ma lepszy pomysł - Chevaleresse. Ale najpierw kot. Szukamy futra. Ula robi co może - brak umiejętności "w szukanie" kompensuje zarówno determinacją jak i umiejętnościami "w koty". Dwie pary oczu a Ula ma terminuski sprzęt + ścigacz.

Tr Z(wiemy o kocich skillach i patternach) +3:

* X: kot był widziany przez niektórych ludzi, acz nie wiedzą z czym mają do czynienia. Ktoś próbował go odłowić i skończył ze zniszczoną siatką i musiał uciekać.
* Vz: kot jest znaleziony. Widzą jak porusza się po prostej do Trzęsawiska. WIE gdzie idzie.
* (Ula zna się na kotach, ona gada); Karo przerzuca ścigacz w tryb cichszy, by Ulę zdeployować. +1Vg; V: Ula skutecznie pokazuje kotu pozytywkę i go pacyfikuje.
* V: Uli udało się nie zostać ranną przy okazji. Szybki odskok gdy kot teleportował się by ją zranić. Odganiający teleslash.

Karo chce, by Ula go skłoniła do powrotu do terytorium. Ula ma taką bezradną minę. Nie wie jak - ten kot ma MISJĘ. Ale spróbuje. (+1Vg)

* Vg: Z pomocą Vii i holoprojekcji i pozytywki i sygnałów i Uli udało się wywołać u kota żałosne miauczenie. Kot dał się Uli pogłaskać i wziąć do fortifarmy. Fortifarma stoi otworem.

Vii "zajmę Rozbójnika na pewien czas. Nie dam rady na długo. Rozbójnik... zawsze był z _nimi_. Tak jak ja.". Ula jest wyraźnie BARDZO zaciekawiona tą TAI i nie powie ani słowa i próbuje po sobie nie pokazać. Zaciekawiona i przestraszona. Ale najpierw pomóc ludziom. Ula i Vii zajmują się czterołapczakiem.

Karo -> Chevaleresse po hipernecie.

* C: To Ty.
* K: Tak, to ja. Potrzebuję pomocy.
* C: Ok? Pomogłaś, pomogę. Chyba, że to nielegalne. Tzn. bardzo nielegalne.
* K: Jeśli od ostatniego razu nie zdelegalizowali Twojego opiekuna to nie.
* C: (bardzo poważnie) Nie. Nie wkręcimy Alana w nic. On już dość wycierpiał.
* K: Potrzebuję jego pomocy i nie zamierzam wkręcać.
* C: Powiedz o co chodzi.
* K: Trzęsawisko. Ludzie w potrzebie.
* C: (ciężkie westchnięcie) Nie ma wyjścia, no nie ma... Alan się z Tobą skontaktuje...
* K: Im szybciej tym lepiej.
* C: Trzęsawisko. Jeśli choć połowa opowieści jest prawdą, to masz rację. Ale jak co mu się stanie...
* K: Wiem.

10 minut później Karo ma wiadomość od Alana.

* A: Chevaleresse mówi, że to ważne. Jakiego miejsca dotyczy problem?
* K: Fortifarma Lechotka i Trzęsawisko
* A: Zbieram sprzęt. Mów do mnie, bym zrozumiał sytuację.
* K: (zataja Vii, ale mówi o Rozbójniku i sytuacji)
* A: Lepiej dla nich, by nie żyli, wiesz o tym
* K: Wiem o tym. Nie proszę byś ich uratował.
* A: Coup de grace?
* K: Ja nie mogę.
* A: I nie powinnaś! (ostro)
* K: I nie zamierzam, masz mnie za idiotkę?
* A: Na pewno masz... niestandardowe podejście.
* K: Dziękuję!
* A: Zrobimy tak - złożę moździerz i zrobię bombardowanie artyleryjskie. Muszę mieć namiar na cel. Kot da mi namiar.
* K: Nie dbam jak. Nie zabijesz tego kota.
* A: ...
* K: Don't.
* A: Kto opanuje tego kota? Ty? Dla tej rasy śmierć właściciela jest straszna. A sam kot jest stary i niebezpieczny.
* K: Daj mu szansę. Zadbam o niego.
* A: Jesteś z Aurum. Nie masz JAK o niego zadbać. Jesteś eksterytorialna. Tego typu kot w Twoich rękach (zawahał się). Słuchaj, Chevaleresse mówi o Tobie dobrze, tak? Ale ten kot może robić straszne rzeczy i eksterytorialność sprawia, że nic nie można zrobić. Rozumiesz mój punkt widzenia? I won't exchange a menace for a menace. To nie wina kota.
* K: Jak się uprzesz, możesz go zestrzelić z tego działa. I cholerna eksterytorialność czy nie... Ty o to i tak nie dbasz.
* A: Poradzisz sobie z tym kotem?
* K: Mam plan. Jeśli nie zadziała, strzelaj. Akceptowalne?
* A: Spróbujmy. Młoda, nie chcę zabijać weteranów. Nawet jak to kot.
* K: Po prostu próbował ochronić swoją rodzinę.
* A: Wszyscy próbujemy. Niektórzy nie wracają.

Karo -> Ula

* K: Lubisz tego futrzaka?
* U: To jest... bardziej sympatyczny kot niż się wydaje.
* K: Technicznie jest sierotą.
* U: Technicznie go zabiją /ponuro
* K: Zobaczymy. Chcesz go?
* U: (zatkało ją)
* K: Jeśli go weźmiesz, może przeżyć
* U: Jeśli tak, oczywiście że tak.
* K: WIESZ co robisz.
* U: Wiem... nie wiem jak zapewnię miejsce temu kotu, może uda mi się coś w strukturach...

Za radą Vii zakładają kotu tracker. Kot jest do tego przyzwyczajony; to jest łowca anomalii. Wie jak i kot dobrze reaguje na holoprojekcję swojego właściciela. Mruczy i się próbuje przymiziać. Stary kot ale kot. Ula założyła kotu tracker unikając pazurów (nie ten człowiek) MIMO uspokajania ze strony Vii. KONTYNUACJA PRZYMIZIANIA.

* Vg (5): Ula założyła kotu tracker. Kot buduje asocjację (Marian Lechot - Ula). Ula jeszcze nie wie. Ale dla kota jest następna po Nim i Niej.

KOMENTARZ MG: Vii jest Skażona ixiońsko. Nie dość że to stara TAI z tych "autonomicznych" to jeszcze jest Skażona. Ale kot też. Tu - musi być. Dlatego ma anomalne cechy. Dlatego jest "tak ludzki". Ixiońskie Skażenie. Nadal jest kotem. Ale jest bardziej stadny niż zwykle. I Ula jest częścią jego stada. Acz niekoniecznie dominującą.

Rozbójnik poszedł polować na swój człowiek. Z trackerem.

Ex Z (WIE. Wie czego szuka i gdzie jest) +4:

* V: Rozbójnik znalazł swoich ludzi. Skacze, unika wężoszponów, teleportuje się, wspina, jest SZYBKI. Jest niewidoczny. Ten kot to cholerny komandos... i wydał żałosny koci dźwięk kuląc się pod jednym z plugawych drzew. A tam - oni. Razem. Marian "odejdź! Wracaj!" gulgocząc. Kot się czochra.

Korzystamy z pozytywki i innych rzeczy. Dajemy kotu znak by się odsunął. I Alan robi anihilację artyleryjską. By kot zobaczył, że jego ludzie nie żyją. Alan nie testuje - on to robi bezbłędnie. Pełna furia Fulmena skonfigurowanego na MIRV. I to specjalnie zaprojektowany do walki z Trzęsawiskiem i ofiarami. Ludzie nie mieli szans. (+1Vg)

* X: Kot jest lekko ranny - fala uderzeniowa, szok.
* Vz: Kot WIE. Nie ma tu nic dla kot. Kot wraca. Wydaje te smutne dźwięki i mruczy dla komfortu. Ale wraca. I wrócił - Trzęsawisko nie ma nic do kota. Nawet anty-anomalnego kota.

I tak Ula dorobiła się kota...

Jako, że Ula mieszka w bloku a Rozbójnik NIE MOŻE być wśród ludzi, Alan przydzielił (jako terminus) służbowo Ulę do tej fortifarmy i dodał jej konstruminusa jako wsparcie. Ula jest przerażona. Tymczasowo dowodzi fortifarmą, swoim kotem i Rozbójnikiem. A pomaga jej Vii i konstruminus.

This is WAY over her head...

Alan zorientował się, że z Vii jest coś nie tak, ale monitoruje Ulę - używa jej jako przynęty i jest w gotowości.

## Streszczenie

Właściciele fortifarmy Lechotka kiedyś dali radę wyrwać ziemię Trzęsawisku. Trzęsawisko nie zapomniało. Teraz jak są starzy i sami, Trzęsawisko porwało Arielę. Terminusi nie pomogli Marianowi, więc poszedł by ją uratować i został porwany. Ich anty-anomalny kot Rozbójnik nie był w stanie im pomóc; rannego kota znalazła w rowie Karolina. Zawiozła do Majkłapca, ale kot im uciekł. Karo współpracując z Ulą opanowały Rozbójnika, dogadały się z fortifarmą i z pomocą Alana zniszczyły ludzi porwanych przez Trzęsawisko. A Rozbójnik i fortifarma wpadły do zaskoczonej Uli.

## Progresja

* Urszula Miłkowicz: relatywnie szczęśliwa właścicielka vistermina (kota) Rozbójnika. Z rozkazu Alana Bartozola mieszka na Fortifarmie Lechotka i jest PRZERAŻONA.
* Urszula Miłkowicz: zdjęta z dokumentacji i nie mieszania się w sprawy Aurum (gdzie wsadził ją Gabriel Ursus) przez Alana Bartozola.
* Kot Rozbójnik: został kotem Uli Miłkowicz. Jeszcze ze trzy lata pociągnie. Albo zginie.

### Frakcji

* .

## Zasługi

* Karolina Terienak: uratowała rannego kota z okolic Trzęsawiska i dała go do kociarni Zawtrak; potem szukawszy gdzie ów kot jest pomogła Uli wydostać się z self-inflicted roboty papierkowej. Po przekonaniu TAI Viirai, że naprawdę CHCĄ pomóc wezwała Alana i po rozwiązaniu misji chroniła życie kota przed terminusami. Wsadziła go w końcu Uli :D.
* Kot Rozbójnik: Stary (16 lat) kot klasy Vistermin (vis - terminus), eksterminujący anomalie i przypisany do fortifarmy Lechotka. Z Przeniesieniem Ixiońskim. Próbował uchronić swoich ludzkich opiekunów przed Trzęsawiskiem; został przez Trzęsawisko poraniony. Gdy uciekł z kociarni Zawtrak (gdzie zaniosła go do żywych Karo) dał się uspokoić przez Ulę Miłkowicz. Z trackerem przeszedł przez Trzęsawisko by znaleźć bioformy swych opiekunów i widział bombardowanie artyleryjskie które ich zabiło. Został kotem Uli :-).
* Iwan Zawtrak: jak Karo przywiozła doń rannego kota to mu pomógł asap. Jak się zorientował, że to nie pacyfikator a vistermin to chciał mu nadal pomóc, ale kot mu uciekł. Zadzwonił do Karo z prośbą o pomoc...
* Urszula Miłkowicz: po próbie porwania Daniela przyznała się przełożonym i przesunęli ją do dokumentów - aż zadzwoniła Karo poprosić o pomoc w uratowaniu kota. Ula pomogła Karo; dowiedziała się o visterminach, powiedziała, że Alan może pomóc i bardzo dużo wzięła na siebie przy osłanianiu vistermina Rozbójnika. By Alan nie zabił kota, wzięła go jako swojego i skończyła na fortifarmie Lechotka z potencjalnie szaloną TAI, groźnym kotem, jej własnym kotem i ryzykiem zagrożenia ze strony Trzęsawiska. Aha, i z konstruminusem.
* Diana Tevalier: skrajnie chroni Alana, ale jak usłyszała, że problem to ludzie na Trzęsawisku, dała Karo się z Alanem skomunikować.
* Alan Bartozol: wezwany do ratowania ludzi z Lechotki zorientował się, że ich już się nie uratuje. Zrobił im MIRV coup de grace. Potem wsadził Ulę do podejrzanej fortifarmy Lechotka i ją czujnie monitoruje. Dał się przekonać, by pozwolił kotu klasy vistermin żyć.
* Viirai d'Lechotka: TAI starej generacji (bardzo autonomiczna i z opiniami), z Przeniesieniem Ixiońskim. Odcięła Ulę i Karo od informacji z fortifarmy; chciała, by terminusi ją zabili. Dała się przekonać Uli i Karo, że te chcą uratować kota i pomogła im bardzo w tym celu.
* Marian Lechot: KIA; terminusi nie uratowali jego żony na Trzęsawisku, więc poszedł ją ratować i został wchłonięty. Zestrzelony przez MIRV Alana po odnalezieniu przez kota Rozbójnika.
* Ariela Lechot: KIA; Trzęsawisko ją zwabiło i dorwało i wykorzystało jako przynętę na męża. Zestrzelona przez MIRV Alana po odnalezieniu przez kota Rozbójnika.

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Majkłapiec
                                1. Kociarnia Zawtrak: Karo dotargała tu rannego kota z okolic Trzęsawiska; a to nie był pacyfikator a vistermin. Kot oczywiście zwiał.
                            1. Podwiert
                                1. Kompleks Korporacyjny
                                    1. Dokumentarium Terminuskie: miejsce pracy Uli Miłkowicz po tym, jak podpadła Gabrielowi za tępienie Aurum
                            1. Podwiert, okolice
                                1. Fortifarma Lechotka: kiedyś na forpoczcie Trzęsawiska, teraz na uboczu; nowe są fronty. Miejsce zamieszkane przez dwóch starych ludzi, jedną ixiońską nielegalną TAI i kota eksterminatora anomalii. Trzęsawisko dorwało ludzi i teraz Fortifarmę objęła Ula Miłkowicz ze swoim kotem i kotem Rozbójnikiem. I TAI w smutku.
                            1. Trzęsawisko Zjawosztup: dorwało i wchłonęło ludzi z Fortifarmy Lechotka. Kot Rozbójnik dał na nich namiary a Alan zbombardował ich MIRVem, coup de grace.

## Czas

* Opóźnienie: -1
* Dni: 1

## Konflikty

* 1 - Karo jest w powietrzu na ścigaczu. Zrzuca siatkę i próbuje czterołapczaka porwać. Karo podchodzi do manewru ostrożnie; zwalnia, siatka itp. Nie ma powodu się spieszyć.
    * Tr+3
    * VV: Kot podebrany, nie stała się mu krzywda
* 2 - Karo próbuje dojść do tego co się dzieje w Lechotce
    * Tr Z(Karo uratowała kota) +3
    * XzV: compel: ratujcie kota. Ale też: pełne informacje od TAI która nie jest Persi a Viirai
* 3 - Ula robi co może - brak umiejętności "w szukanie" kompensuje zarówno determinacją jak i umiejętnościami "w koty". Dwie pary oczu a Ula ma terminuski sprzęt + ścigacz.
    * Tr Z(wiemy o kocich skillach i patternach) +3
    * XVzVVgVg: kot znaleziony, acz już nabroił po drodze. Znaleziony, spacyfikowany, "oswojony" i zapoznany z Ulą.
* 4 - Rozbójnik poszedł polować na swój człowiek. Z trackerem
    * Ex Z (WIE. Wie czego szuka i gdzie jest) +4
    * VXV: Rozbójnik znalazł swoich ludzi. Ranny w wyniku MIRV. Kot wraca - nie ma już jego ludzi, jet tylko Ula :-(

## Kto


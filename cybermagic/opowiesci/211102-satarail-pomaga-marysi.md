---
layout: cybermagic-konspekt
title: "Satarail pomaga Marysi"
threads: rekiny-a-akademia, amelia-i-ernest
gm: żółw
players: anadia, kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [211026 - Koszt ratowania Torszeckiego](211026-koszt-ratowania-torszeckiego)

### Chronologiczna

* [211026 - Koszt ratowania Torszeckiego](211026-koszt-ratowania-torszeckiego)

## Plan sesji
### Ważne postacie

* Lorena Gwozdnik: cat burglar z odrobiną technomancji.

### Co się wydarzyło

.

### Sukces graczy

* .

## Sesja właściwa
### Scena Zero - impl

.

### Scena Właściwa - impl

Trzy dni później Sensacjusz udowodnił, że Torszecki działał pod wpływem. Jest zainfekowany dziwnym owadem. Dobrze ukrytym. Sensacjusz wyjął i usunął owada, było to podobno cholernie nieprzyjemne. OWAD SIĘ BRONIŁ. NIE CHCIAŁ WYJŚĆ. I nie dało się znieczulić.

Karolina przeleciała na szybko trasę wyścigową. Próbuje się trochę rozładować na torze, trudnym. Plus bierze fuchy - coś naprawić czy coś. Zająć głowę i ręce. Więc Karolina schodzi z toru, odkłada ścigacz, zadowolona - pobiła swój czas.

Coś strzela do Karoliny. Ktoś strzela z lekkiej wyrzutni rakiet. Karolina rozpaczliwie unika (Tr+2):

* V: Skuteczny unik. Karolina jest bezpieczna. 
* (+Z) V: Karolina robi ogień zaporowy. Przeciwnik NIE ZROBIŁ uniku. Absurd. Kolejny pocisk nie trafił w Karolinę - ale strzelec nie zrobił uniku, tak się starał trafić. Karolina na prędkości ścigacza wbiła się częściowo w nogi. Połamała. Przeciwnik odbił się, poleciał i zażarł żwir. Na oko - bardzo poważne obrażenia.

Karolina stoi nad nieprzytomną Loreną. Widząc jej obrażenia, wzywa Sensacjusza. Sensacjusz jest tam szybko - poniżej 10 minut. Glista transportowa Sensacjusza pełznie z Loreną. Karolina pomaga Sensacjuszowi i gliście dostać się szybciej...

Glista zaczęła reagować dziwnie. Drgać. Sensacjusz zaniepokojony. "Stop czy szybciej? -> Szybciej".

TrZ+2:

* Xz: Ścigacz nie przywykł do takiego obciążenia. Nie po modach Karoliny. Glista swoje waży a Lorena nie schudła. Lekka awaria.
* V: Zdążyli.

W skrzydle medycznym Sensacjusz wygnał Karolinę i poszedł z glistą. Po chwili Karo słyszy "zestrzel to" - i w stronę Karoliny leci bardzo szybki owad. Taki duży szerszeń. Karolina szybko wyrywa nogę od krzesła (silny glan pomaga) i przygotowuje się do ODBICIA.

TrZ+3:

* Vz: noga od krzesła - direct hit. Owad zamroczony na ziemi.
* X: owad Karolinę dziabnął
* V: Karo zamknęła go zamroczonego w słoiku.

Sensacjusz przybiegł z hiposprayem. Szybko detoksuje Karolinę.

* "Co to jest?"
* "To samo co było w Torszeckim..."
* "Co to jest i co to robi?"
* "I w Lorenie..."
* "Co to jest i co to robi?" - Karo, z NACISKIEM
* "Zakłóca myślenie. Powoduje, że wpadasz w paranoję, chcesz niszczyć rzeczy, nie kontrolujesz impulsów... ma proste lekarstwo. Zbadane przy użyciu Torszeckiego."
* "Mhm"
* "Zostań 3h na obserwacji"
* "Pod warunkiem. Mój ścigacz tu."
* "Paranoja jest pierwszym objawem... usunąłem z niej tego owada. To nie ona zaatakowała Ciebie a owad."
* "A jak wyszło z Torszeckiego - pomyślałeś o tym?"
* "To jest truteń. Drona. Gdzieś na kampusie musimy mieć królową... Torszecki i Gwozdnik to dwa objawy większego problemu. Ty - do środka. Muszę Cię zbadać. Czy nie masz larw."

Karo -> Marysia "Noż kurwa już lepszego pomysłu nie miałaś? Wolałabym nie oberwać cholerną rakietą!!! Cholernej Lorenie połamałam nie wiem co, bo mnie z WYRZUTNI strzelała jakbym jej psa zabiła... Ciapusia ulubionego..."

Pomysł wydawał się dobry. Ale Karo nie doceniła.

Marysia poszła do Sensacjusza. Osobiście. Sensacjusz ją przyjął - wyraźnie jest zmęczony i zakłopotany.

Sensacjusz powiedział Marysi zapytany, że owad przenosi się z płynami ustrojowymi tam gdzie są jajeczka, po czym bardzo się skrzywił mając wizję Marysia x Torszecki w JEGO ŁÓŻKU PACJENTA GDY ON NIE PATRZYŁ. Teraz to chce Marysię dokładnie sprawdzić.

"Wszyscy mogą być zarażeni". "Nie odwiedzał Torszeckiego nikt kogo bym widział". "Ty i Torszecki w moim łóżku... chyba tylko dla pieniędzy..."

TrZ+3:

* V: Marysia jest PRZEKONANA widząc niektóre słowa Sensacjusza, że coś jest nie tak Z NIM.

Sensacjusz jest zarażony. On chce Marysię dogłębnie zbadać - używając glisty medycznej. CHCE TAM WSADZIĆ MARYSIĘ! Marysia natychmiast -> Karolina. Karo dowiedziała się, że ma wstrzyknięte soki z glisty jako lek. Nie doceniła. Chce, by glista nomnomała lekarza za karę.

Marysia idzie z Sensacjuszem; Karolina atakuje z zaskoczenia by ogłuszyć Sensacjusza i go wsadzić do glisty. Marysia odwraca uwagę Sensacjusza opowiadając o Rafałku. Zainfekowany czy nie, NIE CHCE TEGO SŁUCHAĆ.

TrZ+4:

* X: Odparł atak Karoliny; nie dał się zaskoczyć. Jest szybki, bardzo szybki.
* Xz: Obrócił zaskoczenie przeciw Karolinie. Zorientował się, że Marysia jest z Karo. Wrzucił ją w glistę.
* V: Karo na pełnej mocy z siłą i brutalnością lekarza i Marysię w glistę. Ma wszystkiego dość a oni zasłużyli. O.
* V: Glista wzgardziła Marysią i ją zwymiotowała. A lekarza pochłonęła w swój brzuszek.

Marysia otrzepała się ze śluzu. Doszła do tego kto ma królową - MAREK SAMSZAR! Przecież ON ukradł kotka Oldze! A Satarail powiedział jednoznacznie że "winny zapłaci".

Marysia przeszukała zeszyt Sensacjusza i jednoznacznie znalazła potwierdzenie - faktycznie, Marek Samszar tu był. Lekarz wezwał go na badanie kontrolne (sam nie pamięta czemu) i Marek przyszedł. Ale nic mu poważnego nie było, badania pozytywne. Byli też inni ludzie - 2 lub 3 Rekiny.

Marysia potrzebuje wsparcia. Ernest. Idzie do niego szybkim krokiem. Idzie cała w śluzie. Jej duma--, jej skuteczność przekonywania++. No i nikt nie uwierzy że ma coś z tym wspólnego...

Karolina, Marysia -> Ernest. Mają infekcję, nie wiadomo co i jak. Trzeba pomóc wyleczyć naszych ludzi. Ernest jest sceptyczny. Czemu warto ich ratować?

TrZ+3:

* X: Trzeba było Ernestowi powiedzieć, że to Torszecki pod wpływem owada. Teraz się dowiedziały.
* Xz: Zdjęcie Marysi do prasy...
* X: Zdjęcie wskazuje na to, że Marysia robi to dla ratowania innych. Rekiny wiedzą, że dla ratowania Torszeckiego. Jest ogólne podejście u Rekinów "Marysia x Torszecki the best story" i wszyscy siedzą cichutko.
* V: Marysia i Karolina przekonały Ernesta - między innymi przywołując imię ukochanej Amelii. Bo Amelia nie tego by po nim oczekiwała.

Ernest zmontował grupę polującą. Ernest poluje, osacza, przygotowuje się:

TrZ+3:

* X: Marek jest przygotowany. Spodziewał się, że KTOŚ przyjdzie.
* X: Grupa Rekinów (zainfekowanych) stoczyła bitwę z grupą polującą.
* X: Ernest musiał oderwać się od tej grupy - on jest w stanie z polującymi kupić czas Marysi i Karolinie. Ale się nie przebije bez zabijania albo strat. Rekiny nie czarują, czego Ernest nie rozumie (Marysia wie - Satarail nie chciał doprowadzić do rzezi). Ale straty w budynkach, majątku... to jest.

Ta strategia nie działa; Ernest się nie przebije. Ale... Marysia i Karolina same? Korzystając z tumultu?

Marysia chce lecieć sama. Z zaklęciem. Zmienia się w owada i leci w kierunku na Marka. Ona zrobiła szaloną rzecz - ona musi to naprawić sama.

* O: Marysia stworzyła manifestację - mnóstwo TYCH OWADÓW. Bez królowej. Ludzie Ernesta w popłochu. Owady latają, nie są agresywne.
* V: Przy chaosie Marysia dotarła dyskretnie w okolice Marka. Marek nie do końca wygląda jak człowiek - przedłużone kończyny, oczy muchy itp.
* Vz: W konfuzji i w owadziości Marysi Marek nie zauważył, że to "nie jego". Zdziwił się, gdy Marysia go użądliła. SZOK ANAFILAKTYCZNY Marka. Pada na ziemię i DRGA. Chyba umrze XD.
* O: Połączenie mentalne Marysi i królowej. Marysia czuje to co umierająca królowa. Czuje wszystkie skonfundowane owady. I czuje zimny śmiech Wiktora Sataraila i straszne cierpienie Marka.
* V: Marysia przekształca się w siebie, opada na Marka i go ośluzia. Kupuje czas Karolinie.

KAROLINA LECI PO GLISTĘ! Dla prędkości - wsparcie magii.

TrMZ+3:

* V: Karolina wpakowała glistę (z lekarzem w środku) i skutecznie -> miejsce
* X: Ścigacz ma dość. Nie wystartuje potem. A Karolina overclockowała.
* X: Ścigacz nie doleciał. Karolina MUSIAŁA użyć magii. Sztucznie doprowadziła glistę na miejsce. Ścigacz padł w powietrzu. Glista rozpaczliwie próbuje uniknąć Marysi - nie udało się. Glista zwymiotowała Marysię. Ponownie.

Epilog:

* Ernest po tym co się stało nie ma wątpliwości, że to nie Torszecki. To OWAD.
* OWAD wskazuje na Trzęsawisko i Wiktora Sataraila. Najpewniej Samszar był za blisko.
* To, że Marysia nie przyznaje się co zrobiła (gdy uratowała Marka) świadczy o jej skromności i niechęci o chwaleniu się. Plus, wszyscy PODEJRZEWAJĄ ograniczone ubranie. Nikt nie chce mieć potwierdzenia. Ernest, bo nie wypada. Część Rekinów, bo kochają życie. Karolina, bo ma to gdzieś. Torszecki jednak wie swoje :3.
* Marek ma 3 miesiące w szpitalu terminuskim. Strasznie zniszczony przez Sataraila i jego działania.

## Streszczenie

Satarail uderzył - zainfekował Owadem który wypełzł z Torszeckiego Samszara (który kiedyś ukradł somnibela Olgi). Zainfekowane Rekiny zaczęły dewastować dzielnicę. Karo skutecznie rozwala owady (i Lorenę), po czym Sensacjusza w glistę medyczną. Marysia samodzielnie leci jako owad wyłączyć Samszara. Torszecki jest rozgrzeszony - "nie był sobą". Ale polowanie na Torszeckiego czas zacząć.

## Progresja

* Marysia Sowińska: zdjęcie w śluzie od glisty trafiło do prasy. Są plotki, że wszystko dla Torszeckiego. Rekiny kibicują cicho ich zakazanej miłości (czyli Torszeckiemu).
* Karolina Terienak: za atak Loreny pod wpływem na nią, Karolina połamała jej nogi. Opinia absolutnie bezwzględnej i niebezpieczniej. Don't EVER mess with her. Terror works.
* Lorena Gwozdnik: ciężko połamana; Karolina wbiła się jej w nogi swoim ścigaczem gdy Lorena pod wpływem Owada ją zaatakowała wyrzutnią rakiet
* Lorena Gwozdnik: jej relacja z Karoliną Terienak przekroczyła point of no return. Lorena nienawidzi Karoliny (loathing, nie hate). Chce być jak najdalej od niej.
* Marek Samszar: 3 miesiące rekonstrukcji w Szpitalu Terminuskim w Pustogorze. To jest cena za stanięcie na drodze Wiktorowi Satarailowi.
* Rafał Torszecki: eternianie WIEDZĄ, że był zainfekowany, ale NIE DBAJĄ o to. Sezon bicia Torszeckiego otwarty. Rekinom też to pasuje, bo Torszecki (plus ciekawe co Marysia).

### Frakcji

* .

## Zasługi

* Marysia Sowińska: wyczaiła, że z Sensacjuszem jest coś nie tak (infekcja Owadem), ściągnęła Karolinę i z Karo wrzuciła go do glisty (sama wpadając). Sama poleciała jako owad pokonać zOwadzonego Samszara. Ona chce naprawić to SAMA.
* Karolina Terienak: pokazała, że jest świetna w walce - wrzuciła Sensacjusza (i Marysię) do glisty, staranowała Lorenę, zagrała Owadem w baseball. Skutecznie sklupała wszystko co ją chciało skrzywdzić. Don't mess with her.
* Lorena Gwozdnik: zainfekowana przez Owada Sataraila zaatakowała ścigacz Karoliny wyrzutnią rakiet nie mając pełnej władzy mentalnej nad sobą. Karolina połamała jej nogi ścigaczem.
* Rafał Torszecki: Owad Sataraila sprawił, że faktycznie nikt nie wini go za to co się stało. Ale nie jest ulubionym magiem nikogo ;-).
* Sensacjusz Diakon: wyleczył Karolinę z infekcji i używa Diakońskiej Glisty Medycznej. Sam jest zarażony - Karo i Marysia wepchnęły go w glistę. Nawet zarażony jest lekarzem.
* Ernest Namertel: nie chciał pomagać w ratowaniu Rekinów, ale zrobił to bo Amelia by to zrobiła. Nadal - nawet jego oddział nie dał rady przebić się przez zarażone Rekiny.
* Marek Samszar: zainfekowany Owadem Sataraila (za kradzież somnibela Olgi) został przekształcony w potwora. Przejął grupę Rekinów i zaczął dewastację Dzielnicy. Zatrzymany przez użądlenie ze strony Marysi i wielką glistę Sensacjusza. Skończył w szpitalu terminuskim w Pustogorze.
* Wiktor Satarail: pomścił somnibela Olgi. Stworzył Owada który przez Torszeckiego wszedł w Marka Samszara i zadał mu straszne cierpienie, uszkadzając Dzielnicę Rekinów.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Podwiert
                                1. Dzielnica Luksusu Rekinów: solidnie zdewastowana przez starcia między Ernestem + Marysią + Karo a zainfekowanymi przez Sataraila Rekinami.
                                    1. Serce Luksusu
                                        1. Lecznica Rannej Rybki: centrum infekcji Owada Wiktora Sataraila; tam Sensacjusz ma Diakońską Glistę Medyczną.
                            1. Pustogor
                                1. Rdzeń
                                    1. Szpital Terminuski: trafił tam w bardzo ciężkim stanie Marek Samszar

## Czas

* Opóźnienie: 4
* Dni: 1

## Konflikty

* 1 - Coś strzela do Karoliny. Ktoś strzela z lekkiej wyrzutni rakiet. Karolina rozpaczliwie unika
    * Tr+2
    * V(+Z)V: Karo skutecznie uniknęła, ogień zaporowy i wbiła się w nogi strzelca. Połamała Lorenę.
* 2 - Glista transportowa Sensacjusza pełznie z Loreną. Karolina pomaga Sensacjuszowi i gliście dostać się szybciej...
    * TrZ+2
    * XzV: Ścigacz nie przywykł do takiego obciążenia. Nie po modach Karoliny. Glista swoje waży a Lorena nie schudła. Lekka awaria. Ale zdążyli.
* 3 - W stronę Karoliny leci bardzo szybki owad. Taki duży szerszeń. Karolina szybko wyrywa nogę od krzesła (silny glan pomaga) i przygotowuje się do ODBICIA.
    * TrZ+3
    * VzXV: DIRECT HIT! Zamroczony i zasłoikowany owad przez Karolinę. Ale dziabnął ją.
* 4 - Marysia -> Sensacjusz. "Wszyscy mogą być zarażeni". "Nie odwiedzał Torszeckiego nikt kogo bym widział". "Ty i Torszecki w moim łóżku... chyba tylko dla pieniędzy..."
    * TrZ+3
    * V: Marysia jest PRZEKONANA widząc niektóre słowa Sensacjusza, że coś jest nie tak Z NIM. To ON jest zarażony.
* 5 - Karolina atakuje z zaskoczenia by ogłuszyć Sensacjusza i go wsadzić do glisty. Marysia odwraca uwagę Sensacjusza opowiadając o Rafałku. Zainfekowany czy nie, NIE CHCE TEGO SŁUCHAĆ.
    * TrZ+4
    * XXzVV: Sensacjusz odparł atak Karoliny, jest bardzo szybki; wrzucił Karolinę w glistę. Karo wrzuciła lekarza i Marysię w glistę a glista Marysię zwymiotowała.
* 6 - Karolina, Marysia -> Ernest. Mają infekcję, nie wiadomo co i jak. Trzeba pomóc wyleczyć naszych ludzi. Ernest jest sceptyczny. Czemu warto ich ratować?
    * TrZ+3
    * XXzXV: musiały Ernestowi powiedzieć, że to Torszecki, zdjęcie Marysi poszło do prasy, WIADOMO że dla Torszeckiego ("Marysia x Torszecki the best story"), przekonały Ernesta.
* 7 - Ernest zmontował grupę polującą. Ernest poluje, osacza, przygotowuje się
    * TrZ+3
    * XXX: Samszar pod owadami jest przygotowany; Rekiny vs polujący. Ernest się nie przebije.
* 8 - Marysia chce lecieć sama. Z zaklęciem. Zmienia się w owada i leci w kierunku na Marka. Ona zrobiła szaloną rzecz - ona musi to naprawić sama.
    * TrMZ+2
    * OVVzOV: manifestacja, mnóstwo owadów. Ale dotarła do Marka i go użądliła. Poczuła to co umierająca królowa i zimny śmiech Sataraila. Marysia opada na Marka i go ośluzia.
* 9 - KAROLINA LECI PO GLISTĘ! Dla prędkości - wsparcie magii
    * TrMZ+3
    * VXX: wpakowała glistę z Sensacjuszem na Marka i Marysię. Ale ścigacz nie doleciał; padł w powietrzu. Karo go już utrzymywała czystą magią.

---
layout: cybermagic-konspekt
title: "O psach i kryształach"
threads: legenda-arianny
gm: żółw
players: kić, fox
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [200122 - Kiepski altruistyczny terroryzm](200122-kiepski-altruistyczny-terroryzm)

### Chronologiczna

* [200122 - Kiepski altruistyczny terroryzm](200122-kiepski-altruistyczny-terroryzm)

## Budowa sesji

### Stan aktualny

In the Astorian sector there exists a small, distributed colony called Samojed - like a breed of dogs. This colony has a business model based on harvesting asteroids, extracting anomalous ores and sending them to the main planet. 

In the same time due to anomalous properties of this asteroid belt, the dogs bred in this asteroid belt have fur having special properties – it is very white and it is almost impossible to be dirtied.

Due to those facts a competition between selling fur and selling processed ore has appeared. Both factions started covertly fighting against each other. People are disappearing. Small craft required to move between asteroids and habitat zone are also missing.

To solve the problem, Admiralty has sent a Q-Ship called „Infernia” with her captain, Arianna. Their goal is to solve the problem and make everything work again.

### Dark Future

* Upadek Kolonii
* Strata części załogi
* Nowy kult - kryształów

### Pytania

1. .

### Dominujące uczucia

* .

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Punkt zero

.

## Misja właściwa

Infernia docked in the astrotrain station and Arianna with Dick who met with Danuta - a friendly civilian who had actual skin in the game connected with people disappearing. Namely, her niece has disappeared as well.

Danuta wanted to help, but her explanations were vague. Arianna pushed her and got some juice information – the commander of the space station periodically deletes records of people who are disappearing. Danuta has managed to understand why does that happen – he honestly does not remember that those people ever existed. It is not the lack of goodwill – it is an actual memory loss.

Danuta explained that of all the people there are some who remember that some people are missing, but they are few. Most people have no idea what is happening. For example, police does not remember Danuta ever having a niece. And that happens to more people – this is what fuels the conflict between the dog peddlers and ore peddlers. Both sides believe that the other party simply pretend that nothing happens.

Arianna is actually surprised - what type of magic makes it possible for people to forget that someone existed? That sounds like some unique anomalous entity.

So Arianna asked Danuta to take some time off and go on Infernia to be examined by Martyn Hiwasser. If there is something different in Danuta to the ground population, Martin will be able to find it, as awesome (and insufferable) as he is.

In the meantime, the Other Side - people connected with anomalous crystals - received information that a merchant ship has appeared (they have no idea that Infernia is a military QShip). They decided to kidnap several members of the crew and get information about the spaceship.

As is customary, while Infernia is not 100% operating, people can take some time off and walk around the space station and habitat zone. In the meantime, Martin is working on understanding what is different with Danuta.

And he got it – Danuta has one of those strange dogs which live on the asteroid belt. She got some of the dog DNA inside her; she is not completely 100% human. She’s a bit of a hybrid - this is a natural magical transmission process, as she really loves her dogs, magic response and some properties of the dog structure infuse her as well. However creepy it sounds.

From that Martin was able to deduce (with Claudia’s help, who has a knack of extracting strange information from the base computers) that those people who still remember are those who are hybridized with dog DNA. So if you have some alien DNA, you are immune to whatever this effect is doing.

In the evening, Martin complained to Arianna that one of her crew members did not return to the spaceship for the night. And Martin had a date with her. Arianna was righteously upset – why didn’t anyone tell her that someone is missing; Kamil should have done that. But when Arianna confronted Kamil, it proved that he did not remember that those crewmembers ever existed.

So they have been captured by the same anomalous entity which is influencing the station.

Unhappy Martin wasn’t able to spend the night in the pleasant ways – no, he had to scan Kamil with Claudia’s help to find out what really happened and how did Kamil forget. It took a while, but they were able to extract the core of the problem:

* Indeed, there exists some kind of anomalous energy which does not come from a mage. They are dealing with some kind of anomalous entity which is not a magician.
* The specifics of the anomaly is as follows – if someone is influenced by the anomaly, every pure human will start forgetting about the person under effect. So this „moves” from person-to-person using some kind of mental link.
* What is worse, this anomalous entity will be able to extract information from the captured crewmembers and this entity will learn that Infernia is a Q ship. That would be unfortunate.

Arianna had a very difficult decision – she was able to use the neural link which Claudia found to create a backslash reaction and kill her crewmembers so that her spaceship’s secrets will remain secrets. But she elected to do something different - to implement false memories in her crewmembers so that the assailants will believe Infernia to be a merchant ship so they will attack Infernia.

In other words, using Martin’s feelings toward the crewmember who vanished and Claudia’s sophisticated knowledge and magitech Arianna has sprung a trap.

Several hours later Leona, commander of the Marines, reported that there are 20 unidentified people trying to board the ship. Arianna ordered Leona to capture them and stopped magi from casting any spells – she wanted to hide that the magicians on the spaceship.

Leona has managed to capture the boarding party, having only two wounded people on her side and killing only three of the boarders. Sadly, during the assault some noncritical systems of the spaceship were damaged. And the other side knows, that their people got captured - they simply have no idea how did a merchant ship managed to capture 20 experienced boarders.

Leona has brought a leader of the assaulters, Sebastian, to Arianna for questioning. He did not fight her; he answered all her questions. He used to be a captain of a pirate ship called Leotis; however, they have found a crystal anomaly and it has took over. The anomaly made it so they were forgotten. The crystal got into their veins; they are physically unable to stop the crystal’s whispers.

Currently, Sebastian does not command the Leotis. A cultist does. Sebastian did not comply with everything crystal wanted, so he got replaced. If Arianna manages to cure them from the crystal, Sebastian will join her and pledged his allegiance to her. It’s not like he has anywhere to go, anyway.

This is a difficult task was given to Claudia. What in the hell is that crystal?

Claudia has started searching throughout the source material and found some descriptions from before the noctian war. A young researcher, Elisa Ira, has written a paper on non-intelligent, parasitic crystals which feed on memories and brain waves of humans. Eliza has managed to lose a loved one to those crystals and therefore focused her attention on them. Such irony, that during the war the noctians tried to weaponize those crystals and the one to do so was Eliza Ira…

Anyway, knowing so, Claudia was able to find a resonating frequency which was very helpful in eliminating those crystals. And so Eustachy with Martin were able to cure people aboard Infernia.

An anomalous crystal felt that it has lost connection with some of influenced people. The preservation instinct kicked in - influenced people aboard Leotis activated the cruiser and started flying towards Infernia. In the same time influenced people on the asteroid station started to riot, destroy and they endangered the whole station.

Arianna decided she has no choice. As arrogant as she has always been, she focused the power of the people in her and she used the power of an archmage to send a sonic wave towards the station to cure them all of the crystal influence. However, she has overestimated her control and the overall prism of this station – she has succeeded, but a lot of people have died and the station got some structural damage.

This put the crystals survival instinct into overdrive. Even if Leotis was able to defeat Infernia, the crystal forced the cruiser to escape from the station. Arianna decided that having the cruiser on the loose is too dangerous. Even if the cruiser has more firepower, longer-range and is a military ship, she went for pursuit using Infernia, believing in her crew and in her spaceship.

Well.

Infernia lacked military grade forcefields; this spaceship was designed for subterfuge and stealth instead of being pounded by the array of batteries of the supreme ship. Arianna, however, focused all her magic to create a resonance backslash influencing the crystal itself. Infernia weathered through the storm of heavy guns, having some nonessential systems destroyed and some casualties even, but Arianna managed to disrupt the perfect coordination of the crystal and Leotis’ crew.

Infernia went in for the kill.

Leotis’ cannons have managed to destroy Infernia’s armor and caused structural breach; the reactor went unstable. However, new resonance torpedo sneaked through the automated defense system of Leotis has managed to devastate Leotis. Even if the spaceship escapes, it will require months before it returns to operational power.

Arianna had a different idea. She pushed her spaceship to the limits - and beyond. She decided Leotis has to be killed.

Leotis has managed to destroy most of Infernia’s hull. Infernia’s reactor went down. Casualties exceeded 50% of the crew. Claudia had focused all the energy of the incoming shots and augmented the force fields with her magic; therefore she went down as well. If not for Martin, she would have died there on the spot.

But Arianna had enough time to use Claudia’s sacrifice and death of her crewmembers, to create a counter field, and when the next shot came from Leotis, she has turned it around and augmented with all Arianna’s fury. She even used energy of the reactor - send it over to Leotis.

Leotis has exploded. Arianna lost consciousness. Infernia died. But Leona has managed to send SOS signal before that happened.

And in Arianna’s future a potential court-martial appeared...

## Streszczenie

Infernia - QShip - została wysłana na rutynową misję zapobieżenia zamieszkom na stacji Samojed. Na miejscu okazało się, że jest zainfekowany anomalnymi kryształami krążownik Leotis. Arianna Verlen ciężko uszkodziła stację Samojed i prawie zniszczyła Infernię, ale dała radę zniszczyć Leotis poświęcając więcej niż ktokolwiek mógł chcieć.

## Progresja

* .

### Frakcji

* .

## Zasługi

* Arianna Verlen: by zatrzymać krystaliczny Leotis postawiła na szalę bazę Samojed oraz Infernię - poniosła straszliwe straty, ale zniszczyła anomalny statek kosmiczny.
* Klaudia Stryk: prawie oddała życie by chronić Infernię; doszła do badań Elizy Iry odnośnie anomalii krystalicznej odbierającej pamięć.
* Martyn Hiwasser: utrzymał umierającą Infernię przy życiu. Wykrył hybrydyzację psów hodowanych w asteroidach z ludźmi; też pomógł Klaudii z anomalią krystaliczną.
* Kamil Lyraczek: nie pamiętał o członkach załogi dotkniętych anomalią krystaliczną (co było cenną podpowiedzią dla Arianny i Klaudii). Dzięki temu Klaudia i Martyn poznali prawdę.
* Eustachy Korkoran: odkrystalił Sebastiana i innych przechwyconych członków Leotis; potem sprawił że Infernia - QShip - była w stanie pokonać ciężki statek jakim był Leotis.
* Leona Astrienko: dowódca marine na Inferni; człowiek w świecie magów. Dowodziła operacją dzięki której pojmano Sebastiana. Drobny kłębek agresji; udaje sadystkę, ale jest oficerem.
* Sebastian Namczek: kiedyś kapitan piratów; teraz wojownik w służbie kryształu Leotisa. Pokonany przez Leonę i odkrysztalniony przez Eustachego, pomógł w zniszczeniu Leotis. KIA.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Pas Omszawera
                1. Kolonia Samojed: która doznała straszliwych uszkodzeń w wyniku wojny Inferni z Leotisem
                    1. Stacja Astropociągów: przeładunek, logistyka - główne miejsce transportowe pomiędzy Samojed a planetą Astorią
                    1. Zona Mieszkalna: teren zamieszkany przez ludzi, rozproszony obszar dla wielu ludzi i kilku magów; transport między ciałami niebieskimi przez awiany
                    1. Zona Czarna: teren badany, eksplorowany i harvestowany przez ludzi; tam ukrył się krystaliczny statek Leotis

## Czas

* Opóźnienie: 12
* Dni: 5

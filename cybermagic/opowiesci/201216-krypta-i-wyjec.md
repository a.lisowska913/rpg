---
layout: cybermagic-konspekt
title: "Krypta i Wyjec"
threads: legenda-arianny
gm: żółw
players: kić, fox, kapsel
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [201210 - Pocałunek Aspirii](201210-pocalunek-aspirii)

### Chronologiczna

* [201210 - Pocałunek Aspirii](201210-pocalunek-aspirii)

## Punkt zerowy

### Dark Past

.

### Opis sytuacji

.

### Postacie

.

## Punkt zero

.

## Misja właściwa

Sytuacja jest nietrywialna. Nocna Krypta walczy z Wyjcem. Ku przerażeniu Zespołu, Nocna Krypta ma syntezator MIRV oraz działo strumieniowe - coś, czego ten statek nigdy nie miał. Krypta ewoluowała. Krypta się zamifestowała i Wyjec pierwszym krzykiem uderzył.

Klaudia i Eustachy zaczęli dramatycznie szybko budować strefę buforową przeciwko "krzykowi" Wyjca - Infernia **musi** to przetrwać.

* O: kanibalizacja mniej ważnych systemów Inferni; zewnętrzna sfera systemów może przestać działać (i przestaje XD)
* O: wierni Kamila Lyraczka w stronę Arianny - ona nas obroni, ona to odeprze. I faktycznie, sporo osób potraciło przytomność, Martyn ma ręce pełne roboty...
* V: ...ale wiara, zewnętrzne komponenty Inferni i magia Arianny odparła krzyk.

Krypta wystrzeliła anomalnym działem strumieniowym, rozrywając elementy Wyjca. W świetle tego wszystkiego, Infernia udaje, że jest Bardzo Martwym Statkiem. Dzięki temu możemy zbliżyć się do Krypty i Wyjca i żaden z tych anomalicznych statków nie zrobi krzywdy niewielkiej Inferni.

1. Martyn rozdał ludziom inhibitory; środki "hibernacyjne" - więc anomalnie będzie mniejszy ślad ludzi (+3)
2. Infernia jako QShip ma kontr-sensory i Eustachy je uaktywnia na pełnej mocy (+3)
3. Arianna pilotuje godnie, zbliżając się "wiarygodnym dryfem" (+3)
4. Krypta i Wyjec są zajęte sobą, w morderczym tańcu dwóch pożerających się anomalii kosmicznych (+2)

11v8 draw 3 -> VXX

Arianna wspomagana szablą bólu Eustachego przeciążyła moc magiczną. Przekierowała cierpienie na swoich kultystów. Kultyści strasznie cierpią, Kamil trzyma wszystko jak jest w stanie, ale Ariannie udało się zbliżyć do anomalii kosmicznych. A Martyn robi co może by wszystkich trzymać aktywnie przy życiu.

Na wątpliwości Martyna czy to jest tego warte, Klaudia odpowiedziała, że to może być jedyny sposób by zdjąć z nich tą cholerną różową klątwę, która przyszła z Eustachym. Nie będą już statkiem plag. Magitech na Krypcie i umiejętności Martyna i Arianny powinny wystarczyć, by wyleczyć ich z tej korodującej choroby/Skażenia. Plus, chcą wyciągnąć przecież Donalda Parziarza. Martyn zaakceptował, po chwili namysłu. Ale to znaczy, że muszą ze sobą wziąć WSZYSTKICH magów, łącznie z Anastazją...

Arianna sprawnie zadokowała Infernię na Krypcie, niedaleko "Pocałunku Aspirii", w tylnej części statku. Martyn kazał wszystkim założyć skafandry. Wyłączył całość załogi Inferni z akcji (hibernacja), by nie było problemów. Eustachy przestawił Infernię w tryb 'pasywny; strzelam gdy coś się zbliża' i wygasił większość głównych komponentów Persefony. Zespół jest gotowy do wejścia na przerażającą Kryptę. Tylko Diana ma dobry humor "to co, wysadzamy ten statek? :D". 

W tym momencie Arianna pożałowała, że nie ma z nimi Eleny - advancerka by się niesamowicie przydała. Jednak systemy defensywne Krypty nie są chyba aktywne; udało im się przedostać do hangaru obok. A tam - "Pocałunek Aspirii", ale nie jest sam. Jakkolwiek głupio to nie brzmi, koło Pocałunku znajdują się... anioły. A przynajmniej tak to wygląda. Piękne anioły, naprawiające statek i rozmawiające z członkami załogi. Coś nowego. Coś, czego Arianna nigdy jeszcze nie widziała.

Anastazja na ten widok i pod wpływem straszliwej aury Krypty zaczyna hiperwentylować i panikować. Arianna ją uspokaja - pokazuje jej wizerunek Eleny, przypomina jaka Elena jest odważna i czy Anastazja nie chce być jak ona. (VVX). Udało się Ariannie uspokoić Anastazję, ale Anastazja powiedziała cichym szeptem słowa, które nie mogą należeć do niej:

"Wszyscy bogowie są martwi. Pozostał jeden, ostatni. Finis Vitae".

Ariannę zmroziło odwołanie do noktiańskiego Lewiatana; nie wie, skąd pochodzą te myśli, ale na pewno nie z samej Anastazji. Tak czy inaczej, sytuacja opanowana. Chyba.

Klaudia MUSI sprawdzić te anioły, dowiedzieć się o co chodzi i kim są członkowie załogi Pocałunku Aspirii. Uzbrojona w liczne anomalie, idzie w kierunku Pocałunku z nadzieją, że uda jej się ominąć Anioły. Jeden z aniołów poszedł w stronę Klaudii; ta rzuciła _decoy_ (VXV). Anioł nie do końca wie, co się dzieje; anioły nie widzą Klaudii. Koszt jednak taki, że Klaudia ulega degeneracji; coraz gorzej się czuje i ma mało czasu. Klaudia to akceptuje, zdąży - wślizguje się na pokład Pocałunku Aspirii.

Na Pocałunku, anioły przesłuchują członków załogi. Klaudia już teraz widzi, że coś jest bardzo nie tak. Gdyby to byli _normalni, prawdziwi ludzie_, widzieliby Klaudię. Ale wszyscy Klaudię ignorują, jakby była w Eidolonie. Klaudia szybko przeszła się przez Aspirię i zobaczyła coś niepokojącego:

* W obszarach, w których znajdują się anioły Krypty, Aspiria jest sprawnym statkiem i członkowie załogi są żywi.
* W obszarach, w których nie ma aniołów, Aspiria jest martwym, anomalicznym statkiem i członkowie załogi są zmiksowani z mechanizmami. Czysta anomalizacja.

Innymi słowy, anioły generują jakąś bańkę nierzeczywistości. A co anioły robią? Przesłuchują członków załogi.

Klaudia wie już co chciała; czuje się coraz gorzej, musi się ewakuować z Aspirii bo anomaliczny decoy zrobi jej poważną krzywdę. I na powrocie przyczepił się do niej upierdliwy anioł - ten, który pierwszy dostał Decoyem. Innymi słowy, dał radę "złamać" decoy.

Arianna wkroczyła do akcji. Korzystając z okazji walnęła czarem-disruptorem. OXVV. Zobaczyła prawdziwą formę anioła - nekrotechnika. Mechanizm, cyborg z martwego człowieka, w którego klatce piersiowej zamontowana jest głowa ludzka, martwa. Anioł jest "martwy", ale nie wie o tym, samemu ulegając własnym halucynacjom. Anioł cierpi. Arianna poczuła bardziej niż usłyszała "to niemiłe, siostro...". Krypta - w swojej inkarnacji Heleny - wie o obecności Arianny i ekipy na statku. Ale nie robi ruchów przeciwko Zespołowi. Nie przeszkadza. Zupełnie, jakby chciała, by Arianna mogła siebie i innych wyleczyć?

* V: Eustachy zapewnił bardzo trudną drogę ucieczki
* X: Diana "ucieka" w wybuchy; ma halucynacje destrukcji i jest jej z tym mega dobrze
* V: Eustachy zabezpieczył pociąg

Arianna nie ma zamiaru protestować. Eustachy otwiera drzwi, zabezpiecza drogę ucieczki i przejmuje taki wewnętrzny pociąg. Jadą na tylną część Krypty - do biovatów.

Tam na miejscu stanęli przed problemem - ich cele są dwa:

1. Antidotum na Różową Plagę
2. Wyciągnięcie Parziarza i innych agentów Aspirii, jeśli to możliwe

Ale nie mogą się rozdzielić. Nie zostawią tu Martyna samego by robił antidotum. Martyn powiedział, że jeśli zostawią mu Dianę to sobie poradzi i nie sądzi, by miał jakieś szczególne problemy. Jest w stanie się obronić. Arianna nieco niechętnie, ale zostawiła Martynowi Dianę i Anastazję, po czym z Eustachym i Klaudią weszli na pociąg. Czas pojechać do miejsca, gdzie będą członkowie załogi Aspirii... ale problem w tym, że pociąg nie chce jechać a na pokładzie są 2 anioły, ignorujące Ariannę i Zespół na razie.

Krypta/Helena skomunikowała się z Arianną. Powiedziała jej wyraźnie, że Pocałunek Aspirii "należy do niej", bo wszystkich trzeba uratować. Z rozmowy Arianna wykryła, że Krypta zmieniła swoje programowanie:

* Tych, którzy są żywi - tych należy uratować i wyleczyć
* Tych, którzy nie żyją - tych należy "uratować" przez digitalizację i zapamiętanie ich historii. Humans are data.

To daje nową groźną implikację - Krypta ma możliwość _zabijania i digitalizowania_ ludzi. Coś, czego nigdy nie miała. Nowa wersja Krypty jest znacznie groźniejsza niż to, czym była wcześniej. Arianna nie czuje się z tym tak pewnie jak by chciała.

Oprócz tego Arianna dowiedziała się od Krypty/Heleny, że Krypta pragnie się z nią zintegrować ostatecznie. Tylko Arianna może być kapitanem Nocnej Krypty. Tylko ona jest godna. Nie powiem, Ariannie jest nieco zimno jak o tym pomyśli.

A co robi Krypta Anastazji? Skąd te halucynacje, głosy i zagubienia? Krypta wyjaśniła, że załoga Inferni jest ogólnie zdrowa (poza Różową Plagą - dlatego pozwala Martynowi to wyleczyć). Ale problem leży w Anastazji, która ma uszkodzony Wzór. Anastazja wymaga dokładniejszego badania i dokładniejszej naprawy. I Krypta zatrzyma Anastazję - ta czarodziejka nie wróci z Arianną na Infernię. Arianna próbuje negocjować z Kryptą, ale nie da się negocjować z Kryptą; to trochę jak negocjowanie z pralką czy kosiarką. Więc pojawia się pytanie - uciekamy, czy zostawiamy Anastazję?

Po głębszym namyśle, Arianna uznała, że zostawienie Anastazji na Krypcie będzie dla Anastazji lepsze. Będzie się dało ją naprawić, a Krypta nie ma w historii niszczenia czy krzywdzenia ludzi i magów. Tylko jak to technicznie zrobić? Jak wbić jej nóż w plecy? Trzeba oszukać Sowińskich, Anastazję, Dianę i Martyna...

Martyn jest najprostszy - jak już dojdzie do działań (i Martyn zdejmie z Inferni Różową Plagę), Eustachy zaatakuje go od pleców z zaskoczenia i ogłuszy. To nie jest kłopotem. Potem się powie, że była walka na Krypcie i Martyna coś ogłuszyło; to brzmi wiarygodnie. I faktycznie - tak to przeprowadzili.

Anastazja też nie jest szczególnie trudna - Arianna i Eustachy odprowadzili ją do biovatu "by zrobić głębszy skan". Osobiście ją tam wsadzili i patrzyli, jak Anastazja zasypia. Nie było to miłe, ale Anastazja była zbyt ufna jak na przyszły czekający ją los ;-).

Opuścili więc Kryptę, bez Anastazji - i zaczęli opuszczać orbitę Krypty. Skomunikowali się ze Zgubą Tytanów i zrobili inscenizację:

* Wyjec, retransmisja sygnału agonii + sygnał Anastazji 
* Wzmocnili sygnały do Zguby, by pokazać im walkę jaka nie miała miejsca - Infernia vs Krypta vs Wyjec
* Arianna emulowała sygnał Anastazji i echo na Inferni
* Eustachy kontrolowanie zdewastował Infernię, by to było przekonywujące

W wyniku Inferni poszły dalekosiężne sygnały. Ale udało się pokazać jak uciekali z Krypty i stracili po drodze Anastazję. Zguba Tytanów (VVVXXX) uwierzyła inscenizacji i poleciała zaatakować Kryptę. Krypta użyła anomalicznego krzyku Wyjca (już częściowo zintegrowanego z Kryptą) by wywołać AGONIĘ i DESTRUKCJĘ na pokładzie Zguby; nic wielkiego, po prostu "odsuń się". Gdy siły Zguby Tytanów próbowały dojść do siebie, Krypta się zdematerializowała...

## Streszczenie

Podczas walki Nocnej Krypty i Wyjca Zespół wbił się na Nocną Kryptę, by wyleczyć się z Różowej Plagi. Na miejscu okazało się, że Krypta ma "nowe programowanie" (leczenie lub 'zachowanie'), nowe mechanizmy (MIRV, anomalne działo, anioły) oraz że jest groźniejsza niż kiedykolwiek. Arianna zostawiła na pokładzie Krypty Anastazję, by Krypta naprawiła jej Wzór, po czym oszukała Sowińskich, że niby Krypta porwała Anastazję.

## Progresja

* Klaudia Stryk: 3 dni odchorowania po użyciu Anomalicznego Decoy; niestety, nawet tak krótkie użycie tego cholernego gadżetu ma konsekwencje.
* AK Nocna Krypta: osobowość Heleny przejęła kontrolę nad Kryptą. Chce albo ratować i leczyć albo dla martwych - preserve and remember. Humans are data, after all.
* AK Nocna Krypta: wyhodowała MIRV i działo strumieniowe. Jakby poprzednio nie była groźna... no i Anioły Krypty - nekrocyborgi z bańką nierzeczywistości.
* AK Nocna Krypta: integruje ze sobą "Wyjca", czyli dawną stację naprawczą Aspirii.
* Anastazja Sowińska: została na Nocnej Krypcie do naprawy Wzoru; nie wie nawet o tym, co się z nią dzieje.
* OO Zguba Tytanów: lekkie uszkodzenia magisoniczne i mentalne u załogi przez wiązkę Wyjca/Krypty.

### Frakcji

* .

## Zasługi

* Arianna Verlen: pozostawiła Anastazję Sowińską w Nocnej Krypcie; nikt poza nią nie wie, że to była jej decyzja. Disruptowała anioła Krypty i zobaczyła jego prawdziwe oblicze.
* Eustachy Korkoran: mistrz planu inscenizacji "Krypta porwała Anastazję", taktyk. Ogłuszył Martyna, by ten nie wiedział o tym że zostawiają Anastazję. Dużo wybuchów ;-).
* Klaudia Stryk: infiltracja Pocałunku Aspirii i odkrycie prawdy o tym, czym są Anioły Krypty. Plus seria pomniejszych działań anomalicznych na pokładzie Krypty.
* Anastazja Sowińska: podatna na moc mentalną Krypty, została na pokładzie celem Krypty naprawy Wzoru. Tchórzliwa, ale ufa Ariannie i Zespołowi.
* Diana Arłacz: na pokładzie Krypty poddała się halucynacjom i obrazom eksplozji. Podatna na moc mentalną Krypty.
* Martyn Hiwasser: zahibernował większość załogi i z użyciem Nocnej Krypty znalazł antidotum na Różową Plagę. Ogłuszony od tyłu przez Eustachego, by mogli zostawić Anastazję.
* Kamil Lyraczek: z kultystami Arianny udało mu się przyjąć potężne uderzenie krzyku Wyjca; utrzymał Infernię pryzmatycznie pod wpływem chorej anomalii.
* AK Nocna Krypta: używając Aniołów asymiluje i preservuje Pocałunek Aspirii / AK Wyjec. Przechwyciła Anastazję Sowińską i umożliwiła Inferni wyleczenie się z Różowej Plagi. Osobowość Heleny na szczycie.
* AK Wyjec: walczył z Kryptą, by zostać przez nią zintegrowany w siebie. Przestaje być osobnym bytem. KIA.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański

## Czas

* Opóźnienie: 0
* Dni: 2

## Inne

### Komponenty

* Wprowadzone po raz pierwszy Anioły Krypty, na podstawie Caldwella i "Phantom of the Opera": https://www.youtube.com/watch?v=B9pbATdMhjI&feature=youtu.be

---
layout: cybermagic-konspekt
title: "Ewakuacja z Serenit"
threads: legenda-arianny
gm: żółw
players: kić, fox, kapsel
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [210428 - Infekcja Serenit](210428-infekcja-serenit)

### Chronologiczna

* [210428 - Infekcja Serenit](210428-infekcja-serenit)

### Postacie, byty istotne i sytuacja

* Tadeusz Ursus, eternijski szlachcic
    * koleś od simulacrum
    * właściciel "Pięknej Eleny"
    * ostatnio czyścił reputację Arianny
* Infernia
    * ma spalony i uszkodzony system rozrywki
    * ma opętaną i specyficznie działającą Persefonę
    * OGÓLNIE DZIAŁA OK?
* Tomasz Sowiński
    * nienawidzi. Zamknął się z Jolką na serio.
    * robi strajk głodowy, czyli tylko 4 dobre posiłki dziennie, bez deserów.
* Jolanta Sowińska
    * jest naprawdę nieprzytomna. Martyn forever.
* komandosi Verlenów
    * zbierają dowody na Leonę
    * chcą pacyfikować Leonę
    * ...ale są zajęci trzymaniem statku do kupy
* Leona
    * jest zajęta trzymaniem statku do kupy
* "Morrigan d'Tirakal"
    * fabrykuje zło, by komandosi i Leona mieli co robić
    * w zasadzie niegroźna
* 2 dni po poprzedniej sesji

### Scena Zero

* Tadeusz Ursus, arystokrata Eterni -> Infernia <- Falołamacz
* Falołamacz, statek eternijski
    * misja: zdobyć próbki anomalii do floty kosmicznej Eternia - Aurum
    * niewolnicy
    * Rafał Grambucz - łącznościowiec który dał znać SOS
    * Persefona - całkowicie zwęglona
    * Bogdan Anatael (mówili o nim "Boguś" z pogardą)
        * kapitan Falołamacza, mag, używa Simulacrum...

### Sesja właściwa

Infernia powinna mieć:

* torpedy (do niszczenia kadłuba i anomalii)
* dwa działa kinetyczne średniego zasięgu (do niszczenia Rzeczy)
* point-defence (do niszczenia małych rzeczy w martwym polu)
* pole siłowe

Na pokładzie Falołamacza:

* arystokraci Aurum
    * Sowiński Roland
* arystokraci Eterni
    * Bogdan Anatael

Klaudia wreszcie może pobawić się anomaliami. Serenit jest anomalią, to rekordowa anomalia. On przyciąga wszystko co tam się da. Klaudia chce odwrócić polaryzację Serenitu by nie przyciągał a odpychał.

Infernia ma dwie Pinasy.

Plan Eustachego:

Pinasa w połączeniu z częścią setu z Goldariona i umiejętności Klaudii żeby zrobić soczysty wabik można puścić to w jedną stronę. Rzucamy wabik prosto w stronę Serenitu z materiałami wybuchowymi. Zero TAI, 100% mechanizmu. Jak soczyście wybuchnie, będzie dobrze.

Pierwsza faza planu - unieszkodliwić możliwości manewrowania Falołamacza. Kluczem jest zniszczenie silników manewrowych Falołamacza. 

NIE. Pierwsza faza jest inna - Eustachy traktuje Goldarion jako reaktywny pancerz. W odpowiedni sposób wraz z mechanikami przekształcić mocowania działania Infernia - Goldarion, by pełniło rolę pancerza reaktywnego.

Eustachy chce to zrobić bez magii, dobrze, po bożemu. Elena z mechanikami - jedna strona. Eustachy z mechanikami - druga strona. Zmieniają w pancerz reaktywny. Goldarionowa skorupa musi odejść. A przynajmniej w momencie infekcji Serenit... więc Eustachy robi to DLA ELENY!

* XXX: Ranni, konieczne działania ratunkowe - osłona Goldariona się rozszczepiła.
* V: Mamy pancerz reaktywny. Możemy odrzucić Goldarionową powłokę. (niestety, całość to 30-40 min)
* X: Elena bardzo próbowała ratować kogoś co wypadł ze statku... i wypadła ze statku. Plus, jej kombinezon zdehermetyzował

Pinasa #1 zmieniana jest w duży granat, ale pinasa #2 jest do ratowania osób i Eleny.

Pinasa #2 + tractor beam ma uratować wszystkich co opuścili statek przez wypadnięcie. Martyn na wędce ratuje ludzi (ekspert od próżni). 

* V: Martynowi udało się wyratować członków załogi.
* V: Nie ma dodatkowych opóźnień - Martyn zrobił to wyjątkowo sprawnie.
* V: Nie ma rannych. Nie ma braków w sprzęcie. Martyn niezawodny, Martyn najlepszy.

Martyn w pełni bojowy bo Serenit, skompensował wszystko co się stało - strata tylko rzędu 10 min.

Mamy reaktywny pancerz. Możemy walczyć z Serenitem. Mamy odratowanych członków załogi.

Klaudia robi NAJSMACZNIEJSZĄ PRZYNĘTĘ NA ŚWIECIE. Malictrix dodaje bardzo chorą, skorodowaną serię killware do Tirakala. Tirakal jest nośnikiem zagrożenia. Pintka #1 jest nośnikiem Tirakala. Klaudia wyciąga najplugawsze anomalie jakie ma. Zwłaszcza wszystkie anomalie krystaliczne, wzmacniające sygnał itp. Plus rzeczy z Leotis (inna anomalia krystaliczna kosmiczna). Plus Klaudia od Martyna wie co jest potrzebne.

Cel: NAJSMACZNIEJSZA PINTKA EVER. Przynęta dla Serenita, której nie chce się oprzeć. Kupić dobre parę godzin.

ExMZ+3:

* Vm: Serenit skupi uwagę na pintce. Pintka kupi czas. Serenit nie czuje się zagrożony.
* Vm: Serenit przekierował Odłamek w kierunku Pintki. Sam odzyska Falołamacz. 7 godzin.

Rejestr załogi Falołamacza - zawiera damę. Cywil. Aida Serenit.

Do Entropika Arianna i Klaudia próbują przeszczepić sentisieć i zbudować linię sympatyczną z Eidolonem Eleny. Przeszczepiamy fizycznie fragment Eidolona do Entropika i uruchamiamy go mocą magiczną by się uruchomił. By zbudował link sympatyczny. PLUS WIELKI RYTUAŁ CAŁEJ ZAŁOGI. Święta Arianno, zrób to z nami.

ExMZ+3: 

* X: komandosi rodu Verlen są najbardziej zniesmaczeni ever. WTF KULT ARIANNY.
* X: sentisprzężony Eidolon wymaga potem ciężkiej naprawy
* X: Serenit **dostaje informacje** o Inferni i zwłaszcza Elenie przez Entropika.
* V: integracja zakończona powodzeniem. Entropik odzwierciedla ruchy Eidolona sterowanego przez Elenę

Eustachy dobudowuje sieć małych elementów, sensorów i sond by móc zwiększyć zasięg sentisieci i osłonić Elenę przed infekcją Serenita.

Następny ruch - Eustachy bombarduje Falołamacza, robi "lot koszący". Uszkadza silniki itp oraz zrzuca tam Entropika. Falołamacz odpowiada ogniem; Eustachy ma nadzieję, że Infernia to przetrwa.

* V: Bombardowanie udane. 
* XX: Usuwam wszystko Goldarionowe; Falołamacz odpowiedział ogień
* X: Panika na pokładzie Falołamacza. Część do kapsuł, katapultuje się. Część się rozbiega itp.
* X: Infernia jest strukturalnie uszkodzona. Infernia nie poleci dalej. Wymaga ratunku, to kosmiczny wrak. JESZCZE leci, ale to tylko kwestia czasu - przekroczyli Rubikon.
* V: Bronie Falołamacza zostały tymczasowo zniszczone.

Elena ma połączenie (Entropik - Falołamacz). Udaje potwora by ściągnąć na siebie kapitana i jego Simulacrum. "Jak sobie nie poradzisz, Eustachy może w specjalne działo"

TrZ+2+2:

* V: Elena dała radę ściągnąć na siebie Simulacrum.
* XX: Entropik zostanie pochłonięty przez "odłamki Serenit". Elenie nie uda się zrobić samozniszczenia. 
* V: Elena przekierowuje na siebie "odłamki Serenita".

Po tej operacji Elena znowu trafi na tydzień do szpitala.

Arianna --> Falołamacz. Manipulacja. "MUSICIE SIĘ WYCOFAĆ TAM! BO POTWORY! I WSZYSTKO!" Plus 1 za SEKRETY ORBITERA. 

ExZ+3+2:

* V: Aida dała radę, posłuchała rozkazów i ewakuowała tych "zdrowszych". Oceniła które miejsca są bezpieczne.
* X: Arystokraci Eternijscy, część z nich wymarła. Wcześniej. Nie w wyniku działań Inferni.
* X: Operacja ratunku trwa za długo. Bezgłowe kurczaki itp. Elena się przepala z halucynacjami.
* <Eustachy bombarduje niebezpieczne serenitowe miejsca: +2V>
* X: Falołamacz odzyskał mobilność i broń.
* V: Aida dzięki działaniom poprzednim odratowała Sowińskiego.

Eustachy robi kolejny nalot. Odstrzelić to, gdzie są ludzie. Nie da się uratować już więcej. TrZ+3.

* V: Odstrzelona ładownia - ludzie dadzą radę się wycofać.
* O: samobójcza druga pintka ciężko uszkodziła Falołamacz. Falołamacz rezygnuje z walki; odlatuje na Serenit.

Ewakuowaliście się na Infernię. Falołamacz jest uszkodzony. On nie chce walczyć. Leci na Serenit.

Infernia umiera. Ostatkiem mocy reaktora Klaudia wysłała SOS w kierunku na K1, by ktoś ich uratował...

Po uratowaniu z uwagi na naturę Serenit **wszyscy** mieli tydzień kwarantanny... (dodaję do czasu sesji - 2 dni ratunku + tydzień kwarantanny)

## Streszczenie

Klaudia spowolniła Serenit, po czym Elena weszła zdalnym Entropikiem na pokład Falołamacza. Arianna zmanipulowała ludzi by dało się ich uratować. Samobójcza pintka zniechęciła Falołamacz do dalszej walki. Nie wszystkich udało się uratować (ale kapitana i arystokratę Sowińskich tak). Niestety, Infernia skończyła jako kosmiczny wrak i musiała wysłać SOS...

## Progresja

* OO Infernia: kosmiczny wrak. JESZCZE ledwo lata, ale jest już strukturalnie uszkodzona. Nie doleci sama na Kontroler Pierwszy, ale uciekła Serenitowi...
* Elena Verlen: po neurosprzężeniu i sentisprzężeniu i Dotyku Serenit skończyła na dwa tygodnie w szpitalu na regenerację i rekalibrację Wzoru.
* Elena Verlen: Serenit wie o jej istnieniu. Jeśli będzie gdzieś blisko, może na nią zapolować.
* Elena Verlen: traci dostęp do sentisprzężonego Eidolona - ów wymaga naprawy ("regeneracji?") w Verlenlandzie. 2 miesiące od teraz.
* AK Serenit: WIE o istnieniu Eleny Verlen i o Inferni. Jeśli jest gdzieś w pobliżu, może na nich zapolować.
* Otto Azgorn: zniesmaczony kultem Arianny na Inferni. Tien Verlen na to pozwala?! WSPIERA to?!

### Frakcji

* .

## Zasługi

* Arianna Verlen: przeszczepiła magicznie sentisieć z Eidolona do Entropika używając udrożenienia kultu, po czym zmanipulowała ludzi na Falołamaczu by Aida mogła ich uratować.
* Eustachy Korkoran: zrobił reaktywny pancerz na Inferni, ostrzelał Falołamacz i wprowadził tam Entropika po czym odstrzelił ładownię gdzie są ludzie. Inżynier-artylerzysta w pełnej mocy.
* Klaudia Stryk: zrobiła najsmaczniejszą przynętę na Serenit ever (kupując czas), po czym z Arianną przeszczepiły sentisieć z Eidolona do Entropika, by Elena mogła nim zdalnie kierować.
* Martyn Hiwasser: wyjaśnił jak Serenit infekuje statki, po czym ratował "na wędce" osoby które wypadły z Inferni. W końcu mistrz spacerów kosmicznych ;-).
* Elena Verlen: połączona z Eidolonem i sympatią z Entropikiem zdalnie poruszała Entropikiem na Falołamaczu przez sympatię, odpierając ataki Serenita na swój umysł. Uratowała wielu, acz zapłaciła szpitalem.
* Aida Serenit: nieszczęsny pasażer Falołamacza. Posłuchała rozkazów Arianny i uratowała kogo się dało z Falołamacza (m.in. Sowińskiego i Anataela). 
* Bogdan Anatael: kapitan Falołamacza, mag, dowódca kombinowanej akcji Eternia - Aurum do zrobienia floty i badania anomalii kosmicznych. Uratowany przez Infernię. Jego simulacrum krążyło po Falołamaczu...
* Roland Sowiński: uczestniczył w akcji kombinowanej Eternia - Aurum do zrobienia floty i badania anomalii kosmicznych. Uratowany przez Infernię z Falołamacza zanim stała mu się terminalna krzywda.
* OO Infernia: straciła drugą pintkę. Uszkodzenie strukturalne. Jeszcze dała radę skosić Falołamacz lekkimi działkami, ale po odleceniu Falołamacza się wyłączyła...
* OE Falołamacz: w pełni staje się Odłamkiem Serenit; KIA. Uszkodzony przez samobójczą pintkę #2, odechciało mu się walczyć z Infernią i ruszył w kierunku na Serenit, gdzie jego miejsce...
* AK Serenit: Klaudia odwróciła jego uwagę co kupiło czas. Pozyskał Falołamacz jako jeden ze swoich statków. Sposób infekcji: zastępuje advancera swoim konstruktem i wprowadza Serenit na statek macierzysty.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański

## Czas

* Opóźnienie: 0
* Dni: 9

## Inne

.

## Konflikty

* 1 - Eustachy traktuje to co zostało z powłoki Goldariona jako reaktywny pancerz. Przekształcić mocowania działania Infernia - Goldarion. Zbudować odporność na infekcję Serenit (bo odrzuci).
    * ExZ+2
    * XXXVX: osłona G. się rozszczepiła. Są ranni. Ale mamy pancerz reaktywny. Elena za burtą, bo ratowała kogoś. Koszt: 40 min.
* 2 - Martyn jest "na wędce" i ratuje wszystkich co wypadli pinasą #2. Wędka + tractor.
    * TrZ+2
    * VVV: Martyn absolutny ekspert. Zero rannych, zero braków w sprzęcie. Koszt: 10m
* 3 - Klaudia robi NAJSMACZNIEJSZĄ PRZYNĘTĘ NA ŚWIECIE. Killware -> Tirakal. Anomalie -> pinasa. Kupić dobre parę godzin od Serenita.
    * ExMZ+3
    * VmVm: Serenit skupi uwagę na pintce. Dokładniej - wyśle szybkie Odłamki do pintki, sam tutaj. Ale to daje 7h! Sporo czasu.
* 4 - Arianna i Klaudia próbują przeszczepić sentisieć do Entropika i zbudować linię sympatyczną z Eidolonem Eleny. Święta Arianno, pomóż! (kult)
    * ExMZ+3
    * XXXV: komandosi Verlen zniesmaczeni, sentisprzężony Eidolon wymaga naprawy w Verlenlandzie, Serenit **wie** o Inferni i Elenie. Ale jest integracja - Entropik mimikuje ruchy Eleny w Eidolonie.
* 5 - Eustachy bombarduje Falołamacza, robi "lot koszący". Uszkadza silniki itp oraz zrzuca tam Entropika. Falołamacz odpowiada ogniem; Eustachy ma nadzieję, że Infernia to przetrwa.
    * TrZ+2
    * VXXXXV: Udane bombardowanie, wprowadzenie Entropika, ale Falołamacz odpowiedział ogniem i strukturalnie uszkodził Infernię. Na Falołamaczu panika, część do kapsuł, rozbiegają się... są też ranni i ofiary.
* 6 - Elena ma połączenie (Entropik - Falołamacz). Udaje potwora by ściągnąć na siebie kapitana i jego Simulacrum.
    * TrZ+2+2
    * VXXV: sukces, ale Entropik pochłonięty przez odłamki Serenit. Elena przekierowała odłamki na Entropika z cywili. No i - Elena do szpitala po akcji.
* 7 - Arianna robi połączenie z Falołamaczem i manipuluje + rozkazuje "MUSICIE SIĘ WYCOFAĆ TAM! BO POTWORY! I WSZYSTKO!"
    * ExZ+3+2
    * VXXXV: Aida ewakuowała "zdrowszych", większość eternijskich arystokratów wymarła, Falołamacz ma broń, długa operacja - Elena się przepala z halucynacjami Serenit, Aida odratowała Sowińskiego.
* 8 - Eustachy robi kolejny nalot. Odstrzelić to, gdzie są ludzie. Nie da się uratować już więcej. Infernia już nie da rady.
    * TrZ+3
    * VO: odstrzelona ładownia, ludzie się wycofają. Samobójcza druga pintka uszkodziła Falołamacz; ten rezygnuje i odlatuje w kierunku na Serenit.

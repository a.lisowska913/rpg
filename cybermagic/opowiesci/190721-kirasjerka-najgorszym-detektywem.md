---
layout: cybermagic-konspekt
title: "Kirasjerka najgorszym detektywem"
threads: nemesis-pieknotki
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [190714 - Kult, choroba Esuriit](190714-kult-choroba-esuriit)

### Chronologiczna

* [190714 - Kult, choroba Esuriit](190714-kult-choroba-esuriit)

## Budowa sesji

### Stan aktualny

* .

### Pytania

1. .

### Wizja

* Ohglarhalg porwał niedoświadczoną agentkę Orbitera testując potwora - Hlarwagha.
* Agentka Orbitera była kiedyś PORWANA przez Orbiter; nędzny szczur miejski z okolic Aurum.
* Znajdująca się na wakacjach Mirela zareagowała natychmiast. Była tu bo szukała autografu Serafiny.

### Tory

* .

Sceny:

* .

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Misja właściwa

Pięknotka z Amandą działają w okolicach Czółenka. Amanda próbuje zlokalizować kult; Pięknotka sprawdza bunkry. I wtedy zaatakował Pięknotkę blackops-type servar, nieznany model; używa zakłóceń komunikacji. Działa stosunkowo otwarcie. Chce Pięknotkę przesłuchać. Zaczyna od działa military-grade uszkadzającego silniki i reaktory by ją unieczynnić. Pięknotka się nie poddaje bez walki; jednak wróg musi strzelić pierwszy. Mirela strzeliła z szerokiej wiązki neutralizacyjnej.

Mirela strzela wiązką neutralizującą (Tr:P). Na dzień dobry fioletowe światło uszkodziło elektronikę servara. Pięknotka widzi zdecydowanie, że Mirela chce ją żywą. Pięknotka ewakuuje się do bunkra; takiego, gdzie ma przewagę. (Tp:SS). Udało jej się, ale servar jest poważniej uszkodzony. Pięknotka go przeciążyła. Reaktor wszedł w regenerację; część funkcji jest niedostępna. Pięknotka schowała się w takim "dobrym bunkrze"; dostała komunikat od Amandy. "Termi...rządku". Pięknotka wysłała Amandzie "terminus under attack".

Mirela się nie przejmuje - wystrzeliła pocisk burzący do bunkra by Pięknotka musiała wyjść prosto na neutralizator. Pięknotka wpadając do bunkra wystrzeliwuje się i odpala Cienia mimo ryzyka na terenie Esuriit (TrZ+2:KSZ). Niech będzie tak, jak Mirela chciała - power suit Pięknotki unieszkodliwiony. Mirela ma to co chciała - a przynajmniej tak myśli. Cień jest Obudzony. Pięknotka z trudem trzyma kontrolę. Cień chce pożerać.

Pięknotka atakuje z zaskoczenia Cieniem by uszkodzić zasilanie blackopsowego servara (TrZ+2: S). BlackOps power suit ma zniszczony reaktor - natychmiast został odrzucony, łącznie z maskowaniem i ekranem ochronnym. Pięknotka widzi co to jest - Orbiterowy Calibris.

Pięknotka ma (TrZ+5v+1x=15,3,6=S). Atakuje z całej siły i prędkości Cienia; Amanda pomaga snajperką. Pięknotka atakuje swój stary power suit by Mirela musiała go chronić. Dodatkowo nie przejmuje się, używa Esuriit by użyć emanacji Cienia. Teraz już Mirela jest przekonana że walczy z groźnym viciniusem.

Przekierowała CAŁOŚĆ reaktora i silników na natychmiastowy start. W powietrze. I eksplodowała warstwę zewnętrzną by kupić miejsce. Eksplozja wyrzuca Pięknotkę z Cieniem, robi chmurę dymu z której w powietrzu wyłania się Calibris trzymający servar Pięknotki (TrZ+2x:P,P,SS). Calibris odpalił micromissiles i ciężko uszkodził siebie i Cienia. Strzał Amandy pokazał, że w starym power suicie nie ma nikogo więc Mirela wysadziła ten power suit. Cień i Calibris są ciężko uszkodzone; Calibris traci sterowność. Mirela bezwzględnie celuje w zniszczenie Cienia swoim kosztem; crashland. Pięknotka powiedziała "to ja!". Mirela natychmiast zmieniła tor; Calibris przyjął większość koszenia drzew.

Mirela jest ciężko ranna. Krew z ust, połamane żebra, złamana ręka. Niesprawny Calibris. Pięknotka nie jest ranna; ale Cień jest silnie uszkodzony. Wyłączył się. Mirela opuściła zniszczony Calibris. Celuje w Pięknotkę. Pyta co stało się z jej koleżanką... i wtedy strzeliła Amanda (Tr:P). Mirela - emulatorka - nawet w tym stanie poradziła sobie i schowała się za Calibris. Jest w stanie bardzo ciężkim. Pięknotka schowała się za osłoną. Mirela spokojnie włączyła samozniszczenie Calibris i ostrzegła Dowodzenie, że jest po niej. Pięknotka też za osłoną.

Mirela się poddała za rozkazami Dowodzenia. Powiedziała Pięknotce o samozniszczeniu. Pięknotka jest wściekła - chciała Calibris. Sama jest LEKKO ranna; jakieś cięcia czy siniaki. Mirela trzyma się na nogach dzięki środkom wspomagającym Emulatora.

Zrezygnowana Mirela powiedziała Pięknotce to co wie:

* Agentka Orbitera, Aida Serenit, miała sprawdzić znajdujące się na zachodzie Bioskładowisko podziemne.
* Aida została tam porwana. Ona (Mirela) jest na wakacjach i przyszła sprawdzić. Widziała ślady wskazujące na Pięknotkę.
* WIĘC Mirela chciała przesłuchać Pięknotkę.

Amanda wysłała informację Pięknotce - Aida i Mirela były na koncertach Serafiny. Wszystkich. Nie wie czy to ważne, ale... Pięknotka podziękowała i przycisnęła Mirelę. "Narzędzia nie mają wakacji".

* Mirela powiedziała, że miała wakacje, pierwszy raz w życiu.
* Aida zaprzyjaźniła się z Mirelą. Wciągnęła ją w koncerty Serafiny.
* Aida ma parę dni na pojawienie się, ale Mirela wie że jest problem bo jej przyjaciółka nie dała znaku i nie przyszła na koncert.

I Pięknotka ma problem. Jak zgłosić zniszczony power suit skoro OFICJALNIE NIE BYŁO ŻADNEJ AKCJI? Czyli to było takie szkodliwe nieporozumienie. Pięknotka zażądała z powrotem swój power suit - to standard issue. Po komunikacji z bazą Mirela powiedziała, że go Pięknotce Orbiter dostarczy...

Pięknotka wysłała Amandę by dowiedziała się jak najwięcej o kulcie; to teraz jej problem. A sama zamierza spojrzeć na Mirelę i jej znikającą przyjaciółkę. By załatać Mirelę poszła do... Olgi. Olga BAARDZO protestowała, ale Pięknotka przyniosła łakocie dla Wiktora Sataraila i dla zwierzaczków Olgi. Niestety, Olga nie do końca dobrze radzi sobie z Mirelą - ale na szczęście wszedł na to Wiktor Satarail. Mirela się spięła, ale Wiktor wyjaśnił, że przyszedł w gości. I pomógł Mireli, będąc pełnym podziwu nad jej niesamowitą strukturą.

Pięknotka porozmawiała z Wiktorem. Ten powiedział, że podłożył ślady Pięknotki (szok Mireli i Pięknotki) w miejscu, w którym znalazł bioformę stworzoną przez Cieniaszczyt, niedaleko odpadów niebezpiecznych. Chciał by Pięknotka do niego przyszła i przyszła. Pięknotka dyplomatycznie zmilczała że jest tu przez przypadek i nie wiedziała, że Wiktor znalazł jakieś kralotyczne bioformy Cieniaszczytu.

Czyli cała ta bitwa od początku do końca - wszystko to co się stało - było totalnie bez sensu...

**Sprawdzenie Torów** ():

* skomplikowane.

**Epilog**:

* .

## Streszczenie

Przyjaciółka Kirasjerki Orbitera, Aida, zniknęła. Kirasjerka poszła jej szukać i znalazła ślady Pięknotki. Oczywiście, Kirasjerka zaatakowała by zdobyć informacje a Pięknotka nie mogła się poddać (osłaniana przez Amandę). Skończyło się na dewastacji 3 servarów, ciężko rannej Kirasjerce i stropionej Pięknotce. Gdy Pięknotka poszła do Olgi znaleźć leczenie dla Mireli (Kirasjerki), tam dowiedziała się od Wiktora Sataraila, że on podłożył ślady by pokazać jej obecność Cieniaszczytu - a dokładniej kralotycznej bioformy. Największe możliwe nieporozumienie.

## Progresja

* .

### Frakcji

* .

## Zasługi

* Pięknotka Diakon: pokonała w walce Mirelę (Kirasjerkę). Od Wiktora dowiedziała się o co chodzi z kralotami cieniaszczyckimi w okolicy. Oraz od Orbitera wysępiła stracony power suit.
* Mirela Orion: emulator Kirasjerów; była na wakacjach. Szukała gdzie zniknęła Aida (przyjaciółka). Ślady prowadziły do Pięknotki więc zaatakowała. Straciła Calibris, uszkadzając Cienia.
* Aida Serenit: agentka Orbitera, blackops; uwielbia koncerty Serafiny. Zniknęła, najpewniej porwana przez Cieniaszczyt. Zaprzyjaźniła się z Mirelą (emulatorką)
* Olga Myszeczka: najgorszy medyk świata; nie dała rady pomóc Mireli. Szczęśliwie przyszedł Wiktor i naprawił sytuację.
* Wiktor Satarail: znalazł bioformę kralotyczną więc podłożył ślady na Pięknotkę by do niego przyszła. Niestety, ślady znalazła Mirela. Wiktor zapoznał się z Emulatorami Orbitera.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Podwiert, okolice
                                1. Bioskładowisko podziemne: miejsce infiltrowane przez Aidę. Tam Hlarwagh porwał Aidę, tam Wiktor podłożył ślady i tam znalazła je Mirela.
                            1. Czółenko
                                1. Bunkry: miejsce, gdzie doszło do walki między Mirelą a Pięknotką wspieraną przez Amandę. Trzy power suity w plecy.
                            1. Czarnopalec
                                1. Pusta Wieś: Olga tam próbowała pomóc Mireli, potem zrobił to Wiktor. Tam Wiktor powiedział Pięknotce co wie odnośnie kralotów cieniaszczyckich.

## Czas

* Opóźnienie: 1
* Dni: 1

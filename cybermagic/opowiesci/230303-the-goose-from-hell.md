---
layout: cybermagic-konspekt
title: "The goose from hell"
threads: akademia-magiczna-zaczestwa
gm: żółw
players: kić, BloodyWind, HelenDeer
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [230211 - Pierwszy tajemniczy wielbiciel Liliany](230212-pierwszy-tajemniczy-wielbiciel-liliany)

### Chronologiczna

* [230211 - Pierwszy tajemniczy wielbiciel Liliany](230212-pierwszy-tajemniczy-wielbiciel-liliany)

## Plan sesji
### Theme & Vision

* ?
* Co się stało:
    * Grupa młodzieży z "Szóstej Grupy Ochronnej Zaczęstwa" atakowała różnego rodzaju stworzenia dookoła Trzęsawiska.
    * Paweł uznał to za niewłaściwe i dał Gęsi możliwość zemszczenia się. Stworzył anomalną gęś XD
    * Gęś zaczęła polować na Grupę Ochronną. I nie tylko, jest widziana też w Zaczęstwie
        * na razie nikt nie myśli że to robota AMZ, więc...
    * Gęś zaprzyjaźniła się z Alicją, bo ta nie stanowiła zagrożenia i potrzebowała pomocy
* Co się stanie:
    * 
* Dark Future: 
    * Sixth Force still hunts monsters and non-monsters
    * 

### Co się stanie (what will happen)

* S0: Rascal, pacifier-type cat infiltrates the terrain and avoids the Sixth Defense Force
* S1: Paweł Szprotka i Teresa Mieralit. Zlecenie.
    * -> info o Szóstej Grupie
    * -> info o gęsi
* SA: gęś chroni zwierzaki, ale też atakuje ludzi, bo jest terytorialna
    * teren gęsi: jeden z mniej wykorzystywanych bloków biedniejszej dzielnicy Zaczęstwa
        * tam ogólnie jest niebezpieczny teren (lokacja jest w ruinie)
* SB: konfrontacja z gęsią na dachu jednego z domów Zaczęstwa, tam jest Alicja

### Sukces graczy (when you win)

* Rozwiązanie problemu GĘSI

## Sesja właściwa

### Fiszki

#### 1. AMZ

* Paweł Szprotka: weterynarz
    * (ENCAO:  -++0+ |Skłonny do refleksji;; Skryty;; Trwożliwy | VALS: Benevolence, Universalism, Self-direction >> Achievement| DRIVE: Utopia (zwierzaki))
* Teresa Mieralit: nauczycielka magii leczniczej i katalistka (disruptorka magii) w Szkole Magów
    * (ENCAO:  +0+-+ |Manipulatorka i meddler;; Kreatywna, tworząca wiecznie coś nowego| VALS: Benevolence, Achievement >> Face, Security| DRIVE: AMZ będzie najskuteczniejsze)

#### 2. "Szósta Grupa Ochronna Zaczęstwa"

* Olaf Tuczmerin: dowódca Łowców Zła
    * (ENCAO:  +-00+ |Stabilny, opoka niemożliwa do ruszenia;;Bierze co chce| VALS: Stimulation, Power| DRIVE: Lokalny bohater)
    * "Są tu potwory. Potwory należy tępić. Będziemy gwardią terminuską!"
    * styl: protagonista shonen anime, passion
* Agata Kirzawiec: money wing Szóstej Grupy Ochronnej Zaczęstwa
    * (ENCAO:  +-0+0 |Nie przejmuje się niczym;; Namiętna, płomienna| VALS: Face, Conformity >> Security| DRIVE: Rule of Cool)
    * "Zróbmy coś przydatnego i pomocnego. A przynajmniej będziemy mieć punkty!"
    * styl: _Sapento no Naga_ d'Slayers
* 6 innych osób, sprzęt dostarczony przez dwie osoby powyżej

#### 3. Inni

* Alicja Trawlis: uciekła Rekinowi; laska od pracy i tymczasowy szczur uliczny (19).
    * (ENCAO:  -0+-- |Bezbarwny;;Agresywny;;Bardzo dużo się przygotowuje| VALS: Self-direction, Security| DRIVE: Uciec Rekinom)
    * "Nie wrócę do niego. Poradzę sobie sama."
    * styl: _predator - prey, 0 - 100_
* Damian Warbacz: ludzki łowca anomalii
    * (ENCAO:  0-0-+ |Solidny, twardy;;Amoralny;;Perwersyjny, uwielbia szokować| VALS: Universalism, Power >> Face, Tradition| DRIVE: Rana ego (nie trzeba maga))
    * "Te wszystkie zwierzęta i magowie... po co to"
    * styl: _Lukka, MtG_

### Scena Zero - impl

* Carmen <- Helen
* Alex <- Vvind
* Tobias <- cat

Beautiful evening. You are a cat (Tobias). Hunts things. Tobias located something... interesting. A nice smell of magical energy, something huntable. Nearby in the wastes. Probably its type of a rodent. Tobias teleports forward moving in short distances and you stay undetected. You are close to a rodent.

You hear a sound of a weapon. Humans hunt birds. Tobias ignores and attacks

TrZ+3+5Og:

* Vz: rodent is cornered. It is truly a type of a plant-rat. Good.
* Vz: Tobias eliminated the anomalous rat. Humans still didnt notice.

TAKE RAT HOME AND PROUDLY PUT IT ON HUMAN!

* Og: hunters noticed you. "STUPID MONSTER KILL IT!"
* (+1Vg) V: Tobias manages to evacuate unharmed
* Og: Tobias is grazed.

Tobias, meowing through the rat, escaped into darkness.

### Sesja Właściwa - impl

SEVERAL HOURS LATER.

How Carmen, Alex and Julia know each other? --> studying same course. Same classes and helped each other here and there.

Evening, in the bar. CAJ are sitting there and drinking, not alcohol. At one moment there is a shout. One girl is shouting. Tobias has brought a plant-rat, though slightly limping. Cat is 100% proud. Alex and Carmen saved the cat from Majkłapiec. This one was not the most successful pacifier.

Alex has some streetfighting experience. Harder life. You were hunted and captured and... taught magic. So Alex is streetfight + first aid. Alex noticed a wound related to the projectile weapon. A rifle. Alex is walking slowly towards the cat, takes it up and calmly looked at the paw

Tr Z+ 4:

* Xz: cat wants to play. The cat ported - WITH THE RAT - onto the plate of a random girl. CHAOS AND DESTRUCTION
    * EMBARASSMENT!
* V: the cat has been calmed. Lean + purr. And the rat still lies on the plate to the UTTER DISGUST of a girl.
    * GIRL: ITS YOUR CAT!
    * A: SORRY I GO!

While the girl is trying to cause the scene Carmen is trying to use a spell to hide the rat. Move it.

Tr ZM +3:

* X: friends of that girl see moving dead rat
    * chaos++
* Vz: the girl herself doesn't see it and she will not notice the rat. Rat is eliminated from the scene.
* Vm: lights out, screams, and when lights return - there is no rat. Rat is still hidden... in Carmen's purse.

Julia uses one of her devices as a distraction - breakdancing mechanical monkey (built for Triana originally). So the rat topic is basically passed. And she does it when the lights go on, on that plate. "IT HAS NEVER BEEN RAT, ALWAYS MECH MONKEY!"

Tr Z (lights out) +2 +5Or:

* V: this is plausible that there was no rat
* XXOr: Tobias ported at monkey and pawed at it. Monkey is shattered
    * BUT: people don't think it was a rat. It was just-cat-things.

Alex noticed - it is a projectile wound on a paw. A graze.

Teresa Mieralit. An ethics teacher. And she looked at Carmen and smiled. Julia tries to be invisible. Teresa sat at your table. Teresa was searching for 3 students with special skills. She explained that Paweł made an anomalous goose and now YOU have to solve it before stuff goes wrong.

Paweł sits in his room and looks absolutely miserable. Teresa met with him earlier.

Paweł explained, that there were some hunters. He created a goose to protect the animals hunted by hunters. And now the territorial goose menace is hunting people. And now the four people are solving the goose conundrum.

There are city-wide groups (like facebook groups). Carmen surfs through chats and seeks what is happening. Paweł explained it is this is 8 kg goose.

Tr ZM + 2 +3Om:

* V: Carmen found location:
    * first person: "my child played with the yellow-blue train" and then the nightmare landed with the HONK! child escaped and the goose took the train with sweets inside. It flew with the train"
        * --> the goose is STRONGER
    * people around the older part of Zaczęstwo, the more destroyed one (closer to the swamp) - they were attacked by the goose.
* Vm: location. Deduct the magical traces, place for Tobias to find it.
    * probabilistic google maps where the goose can be located where Tobias can find it.

Paweł wanted to stay. He cannot stay. He is sad now. Now he is going - goose is important to not die.

Night. During the night geese sleep, but cats don't. Tobias is an apex predator. Right now, he's a cuddly predator.

NEW PLAN:

* awesome goose food
* large goose trap
* we lure goose via food to trap
* we capture goose

Paweł is an expert on animals. Makes the tastiest goose food in the world. He doesn't need magic to make goose food.

Tr Z (knowledge) +2:

* X: this goose runs a bit differently; which means he has to buy LOTS of things.
* V: this is enticing.
* Vz: strong enough to lure a goose
* V: VERY good goose food. Goose will hunt this food. It smells bad.

So Julia is carrying a cage (smaller) with her, to reinforce with magic. Julia prepares red light helmets to not disturb the goose.

Alex is a streetwise agent. He knows how to move in those places.

Tr Z +2 +5Og:

* X: a part of the building will crumple and make noise
* V: Alex found a way how to navigate the building
* X: Alex - you _see_ the signs that someone lives here. Someone stealthy - like you. A street kid. And the other party knows about you.
* V: Alex managed to find a way to get everyone to the roof. And yeah, you can smell the goose.

Tobias is a curious cat, pretty active, but is easy to be distracted. Tends to eat EVERYTHING. Tobias is focused on a direction. He looks there. "Curious look".

Julia tries to set a trap up. Paweł will give Julia a good location. Alex helped with finding a SECOND location - first one was unstable. And trap is built using stuff to disguise the trap. Goose-level camouflage.

Fortunately, Helen transports the large and powerful omnigoose cage up with kinesis.

TrM+2+3Om:

* X: transporting the cage with kinesis is straining on Carmen. Exhausted.
* X: the cage got damaged in transport. And transport is loud.
* V: the cage, however dented it is, got up. To the roof. Whole 20-30 kg of goose cage to the roof.

Julia uses blowtorch to make the cage work and fortify the cage. No magic. And Alex is trying to get the goose to capture.

Tr +2 +5Og (represent Alicja):

* Og: Alicja jest gotowa do zaatakowania; widzi klatkę
* V: the cage has been structurally reinforced before HONK!

Alex threw food into the cage. GOOSE IS COMING!

10V8X test:

* V: Carmen was tired. She was in the shadows. Not far to escape.
* X: Paweł is weak and terrain sucks. He fell down.
* X: Julia was too close and blinded. She also went down.

Goose ignored them; bit them once or twice. Julia is not very hurt, bruises, same with Paweł. They crawled away a bit, goose focused on food and entered the trap. Trap closed. Goose sounded a HONK!

Everyone is staying the night with the captured goose! Captured HONKING goose!

In the morning. You are very tired. One of _those_ nights. What happens when Teresa gives a 'lesson'. Fortunately, in the morning, about 9 am a small flying car approached the building with very well rested, happy Teresa.

"The reward for a work well done is more work".

Alex - you have noticed something. While you were taking care of the goose SOMEONE HERE was trying to assault you. Alex pretends to be walking away, but tries to find a position to monitor the building. Who is it and why.

ExZ+3:

* X: other party knows you know they wanted to hunt you
* Vz: Alex got a glimpse of a young woman who uses stuff goose stole

PLAN: go home, come back closer to the evening. Don't care for the stuff. But why did she try to assault? Alex goes to sleep and will return in the evening. Street rat, genenged, pretty.

5 pm. Alex is hunting a girl. Good position with watching the girl. Somewhere sneaky. After 5 hours and good equipment from Julia -> you did not see a single activity. She isn't there? Alex stays the whole night, again.

TrZ +3:

* Vz: first, Alex was monitoring the terrain and put drones so they make sure that the girl isn't coming.
    * seems she is not there, 1 am -> seems she disappeared
* X: Alex has no apparent clues where she went
* V: after infiltrating -> the building got hastily evacuated, seems she ran away when you were asleep
    * she was living here for some time
    * the goose 'accepted' her as her flock
    * she ran away asap
    * many maps of the city

## Streszczenie

A cat-pacifier named Tobias belonging to Carmen and Alex got shot at by some random illegal hunters. Carmen, Alex, and Julia are tasked by their ethics teacher to deal with an anomalous goose created by Paweł that has become a menace (created to protect other animals). The group devises a plan to capture the goose using a cage and specially made food. Despite having to traverse a ruined building, they manage to trap the goose, and the story concludes with Alex investigating a mysterious girl who seemed to be somehow connected to the goose.

## Progresja

* .

### Frakcji

* .

## Zasługi

* Julia Kardolin: Built a breakdancing mechanical monkey as a distraction, carried a cage, and set up the trap for the goose.
* Alex Deverien: Streetwise character with first aid experience, noticed the projectile wound on Tobias, and threw food into the cage to capture the goose. Also investigated the mysterious girl. Amazing in maneuevering on damaged buildings.
* Carmen Deverien: Used a spell to hide the rat in the bar to avoid having problems and found the goose's location by surfing through city-wide groups on hypernet.
* Paweł Szprotka: Created the anomalous goose to protect animals from stupid hunters, made the tastiest goose food, and provided guidance on setting the trap to capture a goose.
* Teresa Mieralit: An ethics teacher who recruited Carmen, Alex, and Julia to solve the goose problem. She was the only one who had a good night sleep while the students were sleeping on the top of a building guarding the captured goose.
* Alicja Trawlis: She is 19; escaped a Shark and hides in the ruined building in Zaczęstwo. Not a mage, but a competent street rat. Avoids mages and strongly dislikes them.
* kot-pacyfikator Tobias: A cat who hunts and teleports, found the plant-rat, got shot at and helped locate the menacing goose. Also made a mess in the bar.

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Zaczęstwo
                                1. Akademia Magii
                                1. Nieużytki Staszka

## Czas

* Opóźnienie: 4
* Dni: 2

## Inne

.
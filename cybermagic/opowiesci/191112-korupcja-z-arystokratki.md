---
layout: cybermagic-konspekt
title: "Korupcja z arystokratki"
threads: nemesis-pieknotki
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [191105 - Zaginiona soniczka](191105-zaginiona-soniczka)

### Chronologiczna

* [191105 - Zaginiona soniczka](191105-zaginiona-soniczka)

## Budowa sesji

### Stan aktualny

* .

### Pytania

1. .

### Wizja

brak

### Tory

* .

Sceny:

* .

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Misja właściwa

Niedaleko Zaczęstwa znajduje się miejscowość Jastrząbiec. Jastrząbiec jest nazwany po tym, jak rozbił się tam "ASD Jastrząb". Statek jest wbity w ziemię i dookoła niego - źródła energii itp. znajdują się materiały i możliwe było życie. Pozostał monument, ślad sporego statku, ale jest traktowany jak "ruiny murów obronnych". Chwilowo w Jastrząbcu, w ruinie statku znajduje się ratusz.

I właśnie tam - w Jastrząbcu - Trzewń wykrył coś, co wskazuje na obecność sił Grzymościa. Po pierwsze, Grzymość miał jakąś kontrolę nad tym terenem, znajdowało się tam niewielkie biolab. Po drugie, Trzewń wykrył różnego rodzaju anomalie w hipernecie oraz w vircie z tamtego terenu. Nie jest w stanie dokładnie powiedzieć o co chodzi, ale TAI zarządzająca Jastrząbcem, klasy Persefona, zachowuje się niezgodnie ze wzorem. I to wystarczyło by Trzewń rozpoczął poszukiwania. Wykrył, że coś tam jakoś tam kobiety. Wskazuje silnie na Grzymościa. To, lub chemikalia. Więc kogo poprosił o sprawdzenie? Chemiczkę. Pięknotkę.

Co szczególnie zainteresowało Trzewnia? Kilka bójek jednego dnia. Kilka kobiet pobiło się o jednego chłopaka. Zostało to zarejestrowane przez Persefonę. A na czym polega niestandardowość zachowania Persefony? Cóż, Persefona odpowiada bardziej zdawkowo. Oddaje raporty w ostatniej chwili. Zostawia minimalną ilość informacji. Zdaniem Trzewnia, Persefona zachowuje się, jakby miała "focha". Ale ten model nie jest do tego zdolny. Nie TAI Persefona. Persefona jest TAI statki kosmicznego - jest współrządzącą Jastrząbca właśnie dlatego, że to kwestia historii i poradziła sobie z "przekwalifikowaniem się". Zdaniem Trzewnia, to Grzymość. Czy coś. Ale zdaniem Trzewnia teraz WSZYSTKO to Grzymość. Zwłaszcza po opierdolu od Karli. Dramatycznie próbuje się zrehabilitować...

Minerwa. Minerwa jest lekko zdziwiona, że Persefona strzeliła focha. Ale zostawiła Pięknotce psychotest - niech Persefona wypełni ten konkretny test; to pozwoli Minerwie na określenie o co naprawdę chodzi i jak się ta sytuacja rozwija. Zapytana przez Pięknotkę jak Persefonę przekonać, Minerwa powiedziała wyraźnie - zmusić. Przecież terminusi, Pustogor itp. powinni być w stanie to zrobić. Nie ma ryzyka związanego z wypełnieniem tego testu.

Pięknotka poszła - ekstra na baczności - do Ratusza w Jastrząbcu. (Tr: XV). Druga strona (Sabina) wie o Pięknotce, ale nie wie kim jest. Pięknotka wykryła dronę wojskową. Włączyła detektory (+Zasób) by zobaczyć dokładniej; drona ma konstrukcję typu Aurum. Prywatna stealth drone. Pięknotka strzeliła zapytanie do Trzewnia, co ta drona monitoruje. Trzewń powiedział, że nikt tej drony nie zarejestrował tutaj. To jest pomniejsze wykroczenie. Co to jest? Zmodyfikowana drona arystokracji Aurum.

Pięknotka chce sprawdzić, z czym "gada" drona. Ale jak? Cóż, summon Ataienne...

Weszła do ratusza i poszła bezpośrednio w kierunku na nieaktywne konsolety. Po drodze zatrzymał ją strażnik. Pięknotka pokazała "terminus" i zidentyfikowała się. Strażnik pozwolił jej iść dalej. Pięknotka zaczęła downloadowanie połączeń Ataienne do systemów Jastrząbca. (Tp: XV). Persefona się dowiedziała i próbowała zatrzymać Ataienne; jednak Ataienne jest silniejsza. Persefona zaprotestowała - co Pięknotka ma na myśli. Pięknotka powiedziała Ataienne, że musi wiedzieć odnośnie drony. Ataienne przejmie kontrolę nad ASD Jastrząbcem; szkoda, że Persefona wie. Ataienne zwróciła się do Persefony - nie ma zamiaru jej wymazywać, ale niech lepiej Persefona nic nie mówi. Ataienne zastraszyła Persefonę. TAI Persefona jest zrezygnowana. Chciała się odflickerować. Pięknotka ją zatrzymała - o co chodzi. Persefona powiedziała, że Pięknotka nie jest jej overlordem - i się przeniosła do Centralnego Rdzenia.

Pięknotka aż się zastanowiła - kto jest overlordem? Burmistrz. A nad burmistrzem jest Pustogor. Tymczasem Ataienne zdecydowała się na uruchomienie tylu systemów ile jest w stanie. (Tp: XV). Właściciel drony wie, że jest hackowany, więc Ataienne idzie dalej (VVVV). PEŁEN sukces. Ataienne przejęła absolutną kontrolę nad droną i dowiedziała się wszystkiego. Dostała też kontrolę nad "ASD Jastrząbiec". Ataienne uśmiechnęła się do Pięknotki. Oki, więc - druga strona wie, że jej drona jest shackowana. Ale Pięknotka ma cały system "ASD Jastrząbiec" do swojej dyspozycji. Straszne?

* Właścicielka drony nazywa się Sabina Kazitan. Jest to arystokratka z Aurum.
* Sabina monitoruje maga szkoły magów, niejakiego Ignacego Myrczka. Pięknotka kojarzy gościa - to nerd od grzybów.
* Drona ma wielokrotnie usuwaną pamięć. Ataienne ją przywróciła.
* Ignacy robi jakieś eksperymenty z halucynogenkami i innymi takimi. Stosuje to na populację.
* Burmistrz został nagrany przez dronę, jak zgadzał się na propozycję korupcyjną Sabiny. Sabina chce zapewnić Jastrząbcowi środki itp. W zamian za to - niech nie zgłasza, że Myrczek tu jest i niech nie patrzy, co robi.
* Drona też nagrała coś co Pięknotka interpretuje jako dość brutalny seks Ignacego i Sabiny. Sabina nie wyglądała na szczególnie szczęśliwą. (XXV -> niemożliwe do użycia jako dowód, ale Ignacy zachowuje się nieco nienaturalnie, jakby był na prochach a Sabina protestuje trochę za bardzo i trochę zbyt nieskutecznie)
* Drona patrzyła na konkretny klub. Taki klub dla młodych. Tam będzie impreza.

Po chwili Ataienne się zdziwiła. Pokazała Pięknotce kolejne obrazy.

* Sabina protestuje, atakuje - dostaje w twarz i na kolanach, przed jakimś arystokratą.

Liczne migawki, w większości okrutne. Dlaczego były nagrywane przez tą dronę? Diabli wiedzą. Ale łzy Sabiny i jej nienawiść są prawdziwe... Ataienne nie do końca wie co z tym robić. Aha, jeszcze jedna rzecz - Sabina ma zawsze ze sobą robota bojowego typu Serratus, z TAI Elainka. Elainka jest posłuszna Sabinie. Ataienne zapytana przez Pięknotkę czy może shackować Elainkę, powiedziała, że tak - może wypalić Elainkę i przejąć kontrolę nad Serratusem.

Pięknotka poszła w głąb ratusza porozmawiać z Persefoną. Znalazła ją w Centralnym Rdzeniu. A dokładniej - przed nim. Persefona nie umożliwiła Pięknotce wejścia. Pięknotka zapytała wprost - czy Persefona próbowała zwrócić uwagę Pustogoru na sytuację. Sukces. Persefona potwierdziła - dokładnie o to jej chodziło. Ale nie jest w stanie powiedzieć nic więcej. Ma blokady. Pięknotka próbkuje dalej - niestety, burmistrz się dowiedział. Ale Pięknotka zdjęła blokady Persefony.

* Persefona powiedziała, że to była jedyna rzecz jaką mogła zrobić by zwrócić uwagę.
* Burmistrz chce dobrze, ale _he lost his way_. Arystokratka go opętała złotem.
* Persefona była zmuszona by nic nie mówić i nic nie robić a ten Ignacy Myrczek eksperymentuje z narkotykami na ludziach. I Persefona się z tym BARDZO nie zgadza.

Pięknotka złapała Sabinę w hotelu. Sabina jest lekko wstawiona jakimiś narkotykami, lekkimi. Jest arogancka, w pozie "nic mi nie zrobisz". Pięknotka już na starcie zaczyna do niech podchodzić wrogo. "Przychodzi w dobrej wierze - chce uniknąć NIEPOTRZEBNEGO starcia". Wyszła od "you have no status here AND you are trespassing" oraz "you are out of your depth, child". Pięknotka chce ją sprowokować. Wysondować. Sprowokować do ataku (MAGNITUDE 3). Konflikt jest Trudny - ale Pięknotka ma +4. (10,8=XXVVXV).

* Czarodziejka wyśmiała Pięknotkę. Pięknotka wyjdzie jako ta prowokująca Sabinę - i ma przechlapane u niektórych magów Aurum. (X)
* Sabinie nie da się przyczepić tego, co robi Ignacy. On to robi z własnej inicjatywy by uniknąć oskarżenia o molestowanie. (X)
* Sabina powiedziała, co chce - Ignacy "zrobił jej krzywdę". I mówi że pod wpływem grzybów. Więc ona dała mu szansę udowodnić, ale nie działa (V).
* Sabina przyznała, że przekupiła burmistrza. Przyznała, że "chciała dać szansę Ignacemu". To da się łatwo wykazać że nie o to chodziło (V).
* Sabina w złości, w furii na Pięknotkę wykrzyczała, że ona ZNISZCZY życie Ignacemu. Oskarży go o molestowanie i napaść. And she will. (X).
* Sabina zaatakowała Pięknotkę. Nie powstrzymała się. Wysłała Serratus przeciwko terminusce. Z rozkazem "pokaż, gdzie jej miejsce". (V).

Pięknotka rozpoczęła walkę. Uruchomiła swój servar. Cień nie jest jej potrzebny, nie przeciw Serratus-class unit. (MAG 2 potrzebny; 12-5).

Serratus był szybszy niż ktokolwiek się spodziewał. Przewrócił Pięknotkę i pinował ją do ziemi w pozycji 'spread eagle' (X). Pięknotka odpaliła jetpack i wbiła go w sufit, miażdżąc i dewastując pokój hotelowy (VV). Pięknotka ma to gdzieś, a radość sprawia jej widok szczęki przy ziemi ze strony Sabiny.

Pięknotka zdecydowała się na ostre działanie. Niech Sabina pakuje się i wynosi. Pozwoli jej odejść mimo tego wszystkiego - ale Ignacy... ani słowa o nim. Sabina przestrzeże każdego, że Pięknotka i terminusi pustogorscy to siły z którymi się nie bawi. We fight, we don't play. (VXXVV)

* VV: Sabina idzie na ten układ. Po prostu.
* X: Mimo przestrzegania przez Sabinę, niektórzy magowie Aurum są zainteresowani spróbowaniem szczęścia.
* X: Sabina wchodzi w kolejne, głębsze używki.
* V: Sabina kontaktuje się z Ignacym i mówi mu, żeby zostawił to wszystko a ona mu wybacza.

Pozostaje jedynie obdzielić wpierdol.

* Ignacy: musi zdać egzamin na czarowanie jeszcze raz (bo wyraźnie nie wie kiedy nie wolno). Potem - trafia do kartoteki Pustogoru jako "troublemaker". Ale nie jest gwałcicielem.
* Burmistrz: Pięknotka zbiera dowody. Burmistrz nie może nigdy uciszyć Persefony. A dowody lądują w sejfie Pustogoru. I jeśli coś się powtórzy...

W odpowiedzi za wszystko - Pięknotka siadła i napisała skomplikowany i wartościowy raport do Karli. Niech Karla pozwoli Trzewniowi wyjść na dobre, to dzięki niemu. Karla przeczytała raport. Doceniła. Pięknotka tydzień pisała raporty. Ale Trzewń nie ma już przechlapane...

Tor: 11

* (10) Zdesperowany Ignacy podnosi stawkę
* (15) Ignacy wymaga ratowania przez Sabinę
* (20) Sabina jest niezauważalna
* (25) Ignacy jest pod kontrolą Sabiny

**Sprawdzenie Torów** ():

* Ignacy doprowadził do zdegenerowanej imprezy, ale w porę dał radę ją zatrzymać. Acz trudno mu wobec siebie samego spojrzeć w lustro.

**Epilog**:

* .

## Streszczenie

Po wydarzeniach z Grzymościem oczy Pięknotki padły na Jastrząbiec - tam dzieją się dziwne rzeczy z narkotykami. Okazało się, że to Ignacy Myrczek próbuje rozpaczliwie udowodnić arystokratce z Aurum, że nie zrobił jej krzywdy świadomie a pod wpływem narkotyków. Pięknotka wpadła (z pomocą Ataienne) i zrobiła porządek - ukarała burmistrza, Myrczka, arystokratkę i w sumie wszystkich. Ma smak popiołu w ustach, tu nie było nikogo sensownego...

## Progresja

* .

### Frakcji

* .

## Zasługi

* Pięknotka Diakon: rozdysponowała wpierdol - arystokratce, uczniakowi, burmistrzowi, nawet TAI. Ściągnęła Ataienne by wiedzieć komu dać wpierdol.
* Mariusz Trzewń: rozpaczliwie próbował być przydatny i odkupić się u Karli, więc znalazł Pięknotce Jastrząbiec. Dzięki Pięknotce, Karla zdjęła go z 'listy-osób-do-opieprzania'.
* Minerwa Metalia: doradziła Pięknotce, że Persefona jest nietypową TAI do miasta; dodatkowo, jak zbadać czy działa i co z nią nie tak.
* Ignacy Myrczek: nerd od grzybów. Robił rozpaczliwe eksperymenty z narkotykami, by udowodnić arystokratce, że nie zrobił jej krzywdy - to były narkotyki. Pięknotka dała mu wpiernicz.
* Sabina Kazitan: arystokratka. Dla przyjemności znęcała się nad Myrczkiem, sadystycznie. Pięknotka wywaliła ją z terenu i zniszczyła jej robota bojowego klasy Serratus.
* Ataienne: nieprawdopodobnie potężna z perspektywy opętywania maszyn i przejmowania kontroli. Sojuszniczka Pięknotki, która przełamała wszystkie zabezpieczenia nawet się nie starając...
* Persefona d'Jastrząbiec: TAI, która strzeliła focha. Nie była w stanie inaczej skomunikować problemów bo miała blokady. Zdominowana przez Ataienne, niechętnie dzieliła się systemami.
* Tadeusz Sklerzec: burmistrz Jastrząbca. Gość chciał lepiej dla miasta, więc poszedł na nieetyczny eksperyment arystokratki; dostał wpiernicz od Pięknotki.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Jastrzębski
                            1. Jastrząbiec: miasto powstałe w miejscu rozbitego ASD Jastrząb.
                                1. Ratusz: złożony z rozbitego ASD Jastrząb; ma szczególnie dobre sensory, detektory i ma 'TAI Persefona' - nieodpowiednią do miasta.
                                1. Hotel Stacja Kosmiczna: niewielki hotel, jedyny w Jastrząbiu. Korzysta z niej Sabina; dostaje tam ostry wpiernicz.

## Czas

* Opóźnienie: 1
* Dni: 1

---
layout: cybermagic-konspekt
title: "Inwazja Mimików"
threads: nemesis-pieknotki
gm: żółw
players: strus, cyris, szop, mila, sabrina
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [190424 - Budowa ixiońskiego mimika](190424-budowa-ixionskiego-mimika)

### Chronologiczna

* [190424 - Budowa ixiońskiego mimika](190424-budowa-ixionskiego-mimika)

## Budowa sesji

### Stan aktualny

* .

### Pytania

1. .

### Wizja

* .

### Tory

* niżej

Sceny:

* Scena 1: .

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Misja właściwa

Detektyw Antoni Żuwaczka, dobry druh Zespołu (magowie Czerwonych Myszy, kiedyś dowodzili terenem) zebrał ich do kupy i powiedział, że w okolicy jest mimik symbiotyczny. Zespół nie do końca ufa Żuwaczce - ów jest znany z tego, że jest KIEPSKIM detektywem. Jednak Żuwaczka jest bardzo przekonany; udało mu się wydostać wszystkie potrzebne informacje i NA PEWNO TAM JEST MIMIK SYMBIOTYCZNY. No cóż, jako, że ostatnio w okolicy przez Pustogor jest sporo artefaktów... jest to możliwe.

Kilku magów poszło więc do szkoły. Porozstawiali tam różnego rodzaju sprzęt szpiegowski używając wiewiórek i innych gryzoniokształtnych istot i faktycznie, znaleźli kandydata na mimika symbiotycznego. Jest tam w klubie artystycznym (w Szkole Nowej) chłopak, który maluje obraz. Jest zapalony w ten obraz; jak dalej tak pójdzie, to pobije woźnego. Nic nie oderwie go od malowania. Szybki skan po rekordach studentów pokazał, że ów student nigdy nie zachowywał się w ten sposób. Czyli... Żuwaczka miał rację..?

No dobrze, ale nieszczęśnika trzeba uratować! Jest tu groźny woźny; nasz agent specjalny zaczął biegać a druidka rzuciła za nim stado wiewiórek. Widząc nieszczęście bliźniego, woźny (Jan Dorożny) się zebrał i ruszył na pomoc! To dało możliwość innemu magowi pójścia do dyrektora (bardzo surowego) i powspominanie ran wojennych. Tymczasem dwójka magów teleportowała się koło mimika i dominator uderzył w mimika potężnym czarem mentalnym, by rozerwać link między mimikiem i opętanym nieszczęśnikiem. Oczywiście, zaklęcie zadziałało... dziwnie:

* Dominator wyczuł obecność większej ilości mimików, nie tylko jednego
* Dominator sprzęgł swoją energię z mimikiem. Stworzył coś... nowego. Coś, co łączy cechy mimika i maga. To nie jest wrogie.
* Dominator unieczynnił tego mimika. Tak doszczętnie i skutecznie.

Rozwiązali problem i wrócili do swojej bazy w Podwiercie. Ale pojawił się nowy problem - co dalej? Jest więcej niż jeden mimik. I pojawiło się rozwiązanie, acz maksymalnie chore. Trzeba wykorzystać natywną do Podwiertu plagę jamników. Jamniki mają czysty typ energii. Mają czyste emocje. Mając jednego unieszkodliwionego mimika symbiotycznego pojawia się możliwość ściągnięcia innych mimików i użycie potężnego zaklęcia by je wszystkie poprzepinać na jamniki. Normalnie by to nie działało, ale mają druida, katalistę, mentalistę... ogólnie, mają dość kompetencji.

Do tego celu potrzebują miejsca, gdzie nic złego się nie stanie. Wybór padł na Sensoplex. Miejsce opuszczone, acz pilnowane przez konstruminusów. Jak tam wejść? Poprosili Pustogor o autoryzację mówiąc, że jest tu plaga mimików i chcą jako Czerwone Myszy to rozwiązać zanim odejdą. Dostali autoryzację; konstruminusy opuściły Sensoplex i miejsce należy do nich.

Zespół zbudował tam chorą technomagiczną machinę. WPIERW wywołali plagę jamników podnoszącą poziom Paradoksu, POTEM zmienili kolor energii jamników by były smaczniejsze dla mimików a na końcu doprowadzili do tego, by wysłać potężny zew godowy czy coś podobnego. Niech te wszystkie cholerne mimiki tu przyjdą. Plan jest chory, więc się udał. Wszystko się udało, acz Skażenie zrobiło się niepokojąco wysokie. Zbiegło się 19 osób dotkniętych przez mimiki. (AŻ TYLE). Zespół przekierował mimiki na jamniki, po czym wprowadzili jamniki do jednego budynku który został zniszczony w oparach plazmy. Problem z głowy, prawda?

Nie do końca. Przyszedł poczciwy Żuwaczka, opętany najsilniejszym, "pierwszym" mimikiem (no z jakiegoś powodu mu się udaje). Jako, że jest magiem, mimik nie przeskoczy na człowieka ani jamnika. Ale to nie koniec Żuwaczki - mag Myszy mający problemy z kontrolą magii (ale bardzo potężny) przejął mimika na siebie... i sprawdził, czy jego moce działają. Powołał w Sensoplex koloseum...

Inny mag spróbował go unieszkodliwić i zniszczyć mimika, ale się nie udało - mimik przejął kolejnego maga. Mamy DWÓCH magów pod wpływem jednego silnego mimika. W rozpaczy, odwołali się energetycznie do "córki mimika" - tej istoty którą powołał dominator - oraz użyli jej mocy by wchłonęła mimika i by uratować pozostałych. O dziwo, tak desperacki plan zadziałał. Nadal jednak nie wiedzą, gdzie jest "córka mimika".

Ale problemy z mimikami zostały rozwiązane. Więc chyba dobrze.

**Sprawdzenie Torów** ():

* .

**Epilog**:

* Jeden z magów (Leonard) został J-pop masterem. Jamnik Pop Master. Grywa w Koloseum dla jamników i fanek.
* Za Żuwaczką konsekwentnie chodzą agenci pomagający mu odnaleźć tropy i radzić sobie z detektywowaniem.
* Ten pierwszy mimik został "oswojony" i jest pod kontrolą Tup (czarodziejki zniszczenia).
* "Córka Mimika" jest wolna; jej moc nie eskaluje do punktu w której trzeba ją zniszczyć.
* Koloseum staje się efemerydą nawiedzającą ten teren i to miejsce (Sensoplex).

## Streszczenie

Inwazja mimików symbiotycznych została zauważona przez zarażonego przez mimika detektywa - Żuwaczkę. On zebrał pozostałe eks-Czerwone Myszy i wspólnie rozwiązali ten problem (łącznie z wyczyszczeniem Żuwaczki). Jako paskudny efekt uboczny, narodziła się "Córka Mimika" a Sensoplex jest nawiedzany przez efemerydę-koloseum. A jeden z Myszy, Leonard, został gwiazdą muzyki jamnik-pop.

## Progresja

* Antoni Żuwaczka: chodzą za nim agenci Czerwonych Myszy i pomagają mu w śledztwach, bo sam nie dałby rady.
* Mirela Satarail: dała radę bezpiecznie zniknąć na tydzień. Ma sprzężoną moc maga i mimika symbiotycznego, acz jej moc nigdy nie będzie "wielka".

### Frakcji

* .

## Zasługi

* Leonard Wyprztyk: bard, który stał się gwiazdą Jamnik-Pop. Gra w Koloseum oraz ściąga tłumy zarówno jamników jak i "dziwnych fanów".
* Antoni Żuwaczka: najgorszy detektyw świata, bez talentu, który jednak dał radę wpakować się na mimika symbiotycznego co dało mu umiejętności detektywistyczne. Skończył "normalny".
* Szczepan Szczupaczewski: ostry dyrektor Szkoły Nowej na osiedlu Leszczynowym; kiedyś walczył przeciwko Noctis i nadal te nawyki nie minęły.
* Jan Dorożny: woźny w Szkole Nowej na osiedlu Leszczynowym; kiedyś żołnierz walczący przeciwko Noctis. Twardy, acz ciało już nie działa jak powinno.
* Mirela Satarail: "córka mimika", narodziła się przez Paradoks maga z mimikiem. W chwili wyboru pomogła magom zniszczyć mimika, który zrobiłby krzywdę innym magom. Zniknęła.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Podwiert
                                1. Sensoplex: miejsce, gdzie ściągnęło się wszystkich zainfekowanych mimikami symbiotycznymi, przeSkaziło się je i które teraz nawiedzane jest przez koloseum.
                                1. Osiedle Leszczynowe
                                    1. Szkoła Nowa: pierwsze miejsce infekcji mimika symbiotycznego o którym wie Zespół

## Czas

* Opóźnienie: 3
* Dni: 2

---
layout: cybermagic-konspekt
title:  "Jeden dokument nie w porę"
threads: dzien-z-zycia-terminusa
gm: żółw
players: kić, draża
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [181027 - Terminuska czy kosmetyczka?](181027-terminuska-czy-kosmetyczka.html)

### Chronologiczna

* [181027 - Terminuska czy kosmetyczka?](181027-terminuska-czy-kosmetyczka.html)

## Projektowanie sesji

### Struktura sesji: Eksploracja

* **Scena, Niestabilność, Opozycja, Trigger**
    * .

### Hasła i uruchomienie

* Ż: (kontekst otoczenia) -
* Ż: (element niepasujący) -
* Ż: (przeszłość, kontekst) -

### Dark Future

1. ...

## Potencjalne pytania historyczne

* brak

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Misja właściwa

**Scena**: Papierowy Golem w Zaczęstwie (21:24 - 22:30)

It was that time of a month. Simon has to go to the accounting to get some information on a particular customer – his uncle wanted to extract money from that person. The ladies at the accountants weren’t particularly eager to help Simon; however, he helped a particular woman and he was able to deliver.

Simon being a mage felt the interweaving patterns of power – a spell failure has appeared. Immediately, Simon rushed, called fire alarm and hid in the men’s bathroom. He heard a soft woman’s gasp in a nearby stall. He went to see – however, the pepper spray was too much for his poor body. He fell down and lost consciousness. Such mage, much wow.

Pauline, as a terminus coordinating this area, was called to see these matters. She noticed with amusement a scrawny mage being questioned by two police officers – a sight she is not used to see. She swooped in and took the mage into her custody. Before she started questioning him, she got information that the golem was on the loose nearby. A golem made from accounting papers. Wonderful.

A golem was indeed on the loose. The golem didn’t seem it wants to destroy everything and kill everyone around – it looks more like it wanted to jump into the river and commit suicide. The problem was – the golem was made out of accounting papers. They cannot get destroyed or a lot of businesses around will have problems. So, Pauline tried to do something with using magic. Unfortunately, her magic went haywire too. A lot of papers got generated and flew into the air and Pauline started thinking should she destroy everything or not – where is going to be greater chaos. Fortunately, Simon was able to aggregate the papers – he became a giant magnet for papers and Pauline. She activated power suit not to touch the creep too much…

Several awkward moments later, the golem was dismantled, Pauline was unhappy and Simon was bruised. Simon tried to remember how did the girls look like and he succeeded – he was able to give Pauline specifics of her looks. She found the person that-it – she was a local sorceress. She called her in for questioning and Anita came.

This whole ramble was about Anita forgetting to bring proper papers on time and trying to help herself with magic. Then she the paradox and everyone knows how it went. Pauline was very happy with all of that, but at least she was able to drop the creep and move on with her life.

Wpływ:

* Żółw: 
* Kić: 
* Draża: 

**Epilog**: ()

* Krew Anity była na tych papierach (trochę)
* Anita musiała zapłacić za całą akcję; a chciała oszczędzić 120 złotych na karach...

### Wpływ na świat

| Kto           | Wolnych | Sumarycznie |
|---------------|---------|-------------|
| Żółw          |   0     |     0       |

Czyli:

* (K): .
* (D): .

## Streszczenie

Pewna czarodziejka chciała oszczędzić kar za gapiostwo z niedostarczeniem dokumentów do księgowości. Kilka Paradoksów później i problem trafił na Pięknotkę - golem z papierów z księgowości i lekko creepy mag nieudacznik jako sojusznik...

## Progresja

* 

### Frakcji

* 

## Zasługi

* Pięknotka Diakon: terminuska która coraz mniej kocha Zaczęstwo; tym razem golem z papierów z księgowości i czarodziej któremu w życiu nic nie wyszło
* Szymon Grej: pomocnik windykatora który wpakował się w tarapaty z czarodziejką, która miała jeden Efekt Skażenia za dużo.
* Anita Wirkot: nieszczęśliwa czarodziejka która nie zdążyła donieść na czas faktur. Połączenie paradoksu oraz pechowego spotkania Szymona doprowadziło do jej złapania - i kłopotów dla Pięknotki.

## Plany

* 

### Frakcji

* 

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Zaczęstwo
                                1. Biurowiec Formael: mieści się tam księgowość, jak i kilka innych firm. Tam właśnie Anita miała Paradoks i powstał golem z papierów księgowości...

## Czas

* Opóźnienie: 0
* Dni: 1

## Narzędzia MG

### Budowa sesji

**SCENA:**: Nie aplikuje

### Omówienie celu

* Analiza sesji eksploracyjnej.

## Wykorzystana mechanika

1810

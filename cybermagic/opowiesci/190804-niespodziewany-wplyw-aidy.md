---
layout: cybermagic-konspekt
title: "Niespodziewany wpływ Aidy"
threads: nemesis-pieknotki
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [190726 - Bardzo niebezpieczne składowisko](190726-bardzo-niebezpieczne-skladowisko)

### Chronologiczna

* [190726 - Bardzo niebezpieczne składowisko](190726-bardzo-niebezpieczne-skladowisko)

## Budowa sesji

### Stan aktualny

* .

### Pytania

1. .

### Wizja

brak

### Tory

* .

Sceny:

* .

## Punkt zerowy

### Postacie

### Opis sytuacji

* Wolny Uśmiech: [Radość i Optymizm, Wolność i Niezależność, Eksperymenty i Adaptacja]
* Błękitne Niebo: [Ochrona i Pomoc Swoim, Czystość Ludzkości, Adaptacja i Skuteczność]

Wolny Uśmiech - najłatwiej ich sobie wyobrazić jako skrzyżowanie mafii i sekty religijnej. Specjalizują się w hazardzie, rozrywkach, środkach psychoaktywnych, przemycie, badaniach itp. Wspierani są pośrednio przez jedno z państw-miast z przyczyn kulturowych.

Błękitne Niebo - pomyślcie o nich jak o grupie komandosów-partyzantów zintegrowanych z wrogim sobie państwem. Gdy ktoś z "ich ludzi" jest zagrożony, działają. Zamknięta grupa, bardzo skuteczna, paramilitarna. Specjalizują się w szpiegostwie, akcjach bojowych i zakazanych technologiach. Usuwają istoty "nieludzkie".

Mając te dwie grupy wyraźnie widać obszar konfliktu: "przyjemność i ewolucja przyjemności" kontra "czyste człowieczeństwo i ochrona swoich".

**I teraz:**

Aida Serenit która była porwana przez Hralwagha go wypaczyła i Hralwagh sam się zaadaptował do swoich kontrolerów z Cieniaszczytu...

## Misja właściwa

Pięknotka jest na Bioskładowisku; aktualnie jest tam firefight między terminuskimi praktykantami a nieznanym magiem. Amanda jest niedaleko, ale ignoruje temat. Pięknotka wypatrzyła też Ossidię, która też ma to wszystko gdzieś i wszystko ignoruje, wyraźnie jest zainteresowana Amandą.

Pięknotka przejmuje dowodzenie i zdejmuje wrogiego maga z chorą energią. Wpierw odciągnęła zdjętego (rannego) terminusa, potem się zakradła i ogłuszyła tamtego ćpuna. On jednak ma coś do Cieniaszczytu, coś o nim bredzi. Pięknotka nie może tego tak zostawić zwłaszcza, że jeden z praktykantów znalazł mega nieudolnie schowaną skrytkę z kralothbound narkotykami. Oczywiście, Amanda za tym stoi, podłożyła im to. Chce zapobiec gorącej wojnie Grzymość - Kajrat.

Pięknotka dla odmiany spotkała się też z Ossidią. Ta, dopytana, powiedziała, że Saitaer czegoś szuka. Ona nie wie jeszcze czego ale to jest niedaleko. Ona (Ossidia) planuje zniszczenie Kajrata za to, co jej zrobił. Dlatego ma zamiar zapolować na Amandę. Pięknotce się to nie podoba, ale ta akcja z narkotykami z Cieniaszczytu jest ważniejsza; plus, jak ma zatrzymać Ossidię?

Tak więc Pięknotka skomunikowała się dalekosiężnie z Julią Morwisz. Ta się przyznała że jest problem. Próbowała porwać Emulatorkę, ale porwała Aidę (to Pięknotka już wie). Ale Hralwagh nie dostawał odpowiednich eliksirów kontrolnych i Cieniaszczyt stracił kontakt z agentami. A nie dostawał bo? Jacyś piraci (Pięknotka wie że Orbiter) rozwalili wszelkie trasy przerzutowe z Cieniaszczytu. Więc Pięknotka ma już 2 magów koordynatorów (jeden ćpun jeden nieprzytomny na ziemi koło ćpuna) ale nie ma sprawcy - Hralwagha, ani nie ma trzeciego koordynatora.

Jak to znaleźć? Ossidia. Mają ten sam cel.

Wszystko to jest w okolicach Podwiertu; tam jest najlepszy system policyjny na świecie, dostarczony przez Senetis. Pięknotka poszła więc na komisariat i dowiedziała się, że policja zlokalizowała maga. Ten jest w magazynach. Pięknotka poprosiła (terminus obrony terytorialnej) o anulowanie akcji; policja zabezpiecza teren, Pięknotka z Ossidią robią wjazd. Pięknotka widząc maga w formie agonalnej ekstazy używa swojej mocy i robi za Arazille - wzmacnia to tak, by gość odpłynął totalnie.

Ale Hralwagh? Temu odbiło; zasymilował cały budynek. Szczęśliwie, Pięknotka channelowała Saitaera. Hralwagh został zniszczony a Pięknotka - o krok bliżej do techorganicznej korupcji.

**Sprawdzenie Torów** ():

* .

**Epilog**:

* Amanda Kajrat: zniknęła, acz po ciężkiej walce. Porwana przez Ossidię i złapana przez siły Saitaera
* Saitaer: dostęp do dużej ilości kralotycznego adaptogenu
* Pięknotka: dostała potężny szacunek od terminusów i innych za świetną akcję. Też: krok bliżej do Skażenia Saitaera.
* Cieniaszczyt: agenci uratowani. Wykpili się tanio, bo tylko koszt dyplomatyczny.

## Streszczenie

Po złapaniu Aidy przez Hralwagha z Cieniaszczytu okazało się, że Aida wpłynęła na samego Hralwagha. Potwór splugawił swoich kontrolerów z Cieniaszczytu. Pięknotka współpracując z Ossidią i Saitaerem pokonała Hralwagha zanim komukolwiek stała się krzywda. Dodatkowo - Ossidia porwała Amandę Kajrat dla Saitaera; Ossidia dalej poluje na Ernesta Kajrata.

## Progresja

* .

### Frakcji

* .

## Zasługi

* Pięknotka Diakon: pokonała splugawionych przez Hralwagha magów, potem we współpracy z Saitaerem i Ossidią samego Hralwagha. Oddała Saitaerowi Amandę Kajrat.
* Gabriel Ursus: dowodził terminuskimi praktykantami ze średnią kompetencją do momentu pojawienia się Pięknotki.
* Amanda Kajrat: dostarczyła Pięknotce dowodów odnośnie narkotyku Grzymościa by zredukować gorącą wojnę. Niestety, wpadła w ręce Ossidii i Saitaera - ma być przynętą na Kajrata.
* Ossidia Saitis: nadal poluje na Kajrata; Pięknotka ją przekonała by pomogła z wyczyszczeniem tematu z Hralwaghiem. W zamian za Amandę Kajrat.
* Julia Morwisz: przekonała kilka osób w Cieniaszczycie do ratowania Emulatorów Orbitera; niestety, Orbiter odcina dostęp środków a Hralwagh zwariował. Przestała robić cokolwiek.
* Saitaer: wezwany przez Pięknotkę, zniszczył Hralwagha dla niej; pozyskał sporo kralotycznego adaptogenu na swoje eksperymenty.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Podwiert, okolice
                                1. Bioskładowisko podziemne: miejsce, w którym doszło do starcia praktykantów terminusów ze splugawionymi przez Hralwagha kontrolerami Cieniaszczytu.
                            1. Podwiert
                                1. Magazyny sprzętu ciężkiego: W jednym z budynków niedaleko magazynów doszło do konfrontacji Pięknotka - Hralwagh.

## Czas

* Opóźnienie: 2
* Dni: 2

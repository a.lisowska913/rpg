---
layout: cybermagic-konspekt
title:  "Skażony pnączoszpon"
threads: nemesis-pieknotki
gm: kić
players: emil
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [190113 - Chrońmy Karolinę przed uczniami](190113-chronmy-karoline-przed-uczniami)

### Chronologiczna

* [190113 - Chrońmy Karolinę przed uczniami](190113-chronmy-karoline-przed-uczniami)

## Punkt zerowy

Zaraza stworzona przez Adelę poszła w bagno. Zaraził się jeden z pnączoszponów mający leże w pobliżu niewielkiej wioski o wdzięcznej nazwie Samoklęska...

### Postacie

* Alfred Bułka: miejscowy, opiekun / służący Kornela. Kornel wyciągnął go z dołka i uratował od więzienia. Zna bagno, ale się w nim nie specjalizuje. Wysportowany, o mały włos zostałby przestępcą.
* Kornelia Weiner: katalistka i właścicielka małego laboratorium analitycznego. 

## Misja właściwa

**Scena 1** (17:40):

Kornel śpi spokojnie, gdy nagle (sukces) wyrwany ze snu ledwo zdąża stoczyć się z łóżka, zanim to staje się celem wielkiej, szponiastej łapy.
Alfred zaatakował siekierą, (sukces) skutecznie raniąc stwora.
Wspólnie, pogrzebaczem i miotaczem ognia z zapalarki i dezodorantu (sukces) ranią i odpędzają pnączoszpona.

Kornel pozbierał i zabezpieczył próbki, po czym położył się spać, w czasie, gdy Alfred prowizorycznie załatał dziurę po oknie...

**Scena 2**:

Kornel chce się dowiedzieć, o co chodzi. Wie, że pnączoszpony zwykle nie opuszczają bagien i nie atakują budynków. 
Rano skontaktował się z Alanem Bartozolem, który w krótkich żołnierskich słowach powiedział mu, że pnączoszpony się tak nie zachowują i niech to sprawdzi, a on się pojawi, jak będzie mógł (akurat czyścił Trzęsawisko w zupełnie innym miejscu i nie mógł odmówić bezpośredniemu rozkazowi Karli)
Kornel poprosił o analizę próbek Kornelię Weiner, właścicielkę miejscowego laboratorium analitycznego. Obiecała, że zrobi, co może i wzięła się do roboty. (porażka) Niestety, uzyskanie wyników miało trochę potrwać...

W tym czasie wysłał Alfreda na zwiady na bagno. (skonfliktowany). Alfred wrócił lekko poraniony (coś mniejszego go zaatakowało i się uszkodził uciekając), ale jest przekonany, że był w pobliżu legowiska. Po prostu tak to czuł... Choć samego legowiska nie znalazł. Znalazł za to ślady pnączoszpona. Alfred boi się teraz iść na bagno.

Nie mając pomysłu, co dalej, Kornel zapędził do roboty techniczych od swojego parku rozrywki, żeby naprawili mu okno...

Nadszedł wieczór. Kiedy zapadł zmrok, Kornel usłyszał szum głosów na dworze. Wyjrzawszy przez okno ujrzał dużą grupę miejscowych z widłami i pochodniami, kierującymi się na bagno.
Przyłączywszy się do pochodu, usłyszał, że coś dużego porwało krowę i oni postanowili ubić szkodnika. Niestety, trop, którym szli był tropem pnączoszpona...
Kornel zaczął zostawać trochę z tyłu i już miał rzucić zaklęcie, gdy laboratorium dało mu informację, że drapieżnik jest zarażony jakąś chorobą (takiej jeszcze nie widzieli) i wyraźnie potrzebuje magii.
Kornel się rozejrzał i podgonił do grupy. Nie będzie czarować i robić z siebie smakowitego kąska...

(2 punkty wpływu) Grupa myśliwych-mścicieli w końcu znalazła martwą krowę i (innego) korzystającego z padliny drapieżnika. Biedne zwierzę nie miało szans, a wieśniacy w poczuciu dobrze spełnionego obowiązku postanowili wrócić do domu.

Kornel (sukces) dopilnował, żeby nikt nie został z tyłu, nawet tylko po to, aby zawiązać buta... 

Gdy upewnił się, że wszyscy są bezpiecznie w domu, ponownie skontaktował się z Alanem Bartozolem. 
Tym razem, mając konkrety, terminus pojawił się wraz ze swoim skrzydłem i lekarzem i skutecznie wytępili zarówno pnączoszpona jak i zarazę (kilkoro ludzi udało się złapać, ale lekarze mieli próbki i laboratorium pod ręką).

Wpływ:

* K: 6
* E: 4

**Epilog** (19:30)

### Wpływ na świat

| Kto           | Wolnych | Sumarycznie |
|---------------|---------|-------------|
| Kić           |    0    |    6        |
| Emil          |    2    |    4        |

Czyli:

* (K): zaraza nie została całkowicie wytępiona z bagna, ale trochę potrwa zanim znów się aktywuje
* (E): (2) pojawienie się innego niż pnączoszpon drapieżnika przy truchle krowy

## Streszczenie

Skażony pnączoszpon próbował włamać się do domu Kornela, rezydenta Samoklęski licząc na możliwość pożywienia się energią magicznych artefaktów. Odparty, uciekł na bagno, aby zebrać siły do kolejnego ataku.

Następnej nocy pnączoszpon zebrał dość sił, by zapolować i porwać krowę. Niestety, został dostrzeżony przez wieśniaków, którzy z widłami i pochodniami poszli zapolować na drapieżnika.

Kornel chciał ich odwieść od zamiaru polowania, ale poddał się widząc determinację. Po drodze dowiedział się, że drapieżnik jest zarażony jakąś chorobą. Zdołał wyprowadzić ludzi z bagna, zanim dopadł ich pnączoszpon.

Wezwał terminusów z Pustogoru na pomoc. Tym razem miał konkrety, więc Alan Bartozol wraz ze wsparciem pojawili się i zrobili porządek.

## Progresja

* 

### Frakcji

* Chaos w Szczelińcu: Zaraza ma gdzieś inne, mniejsze ognisko.

## Zasługi

* Kornel Szczepanik: Z pomocą Alfreda odpędził pnączoszpona atakującego jego dom i zidentyfikował problem zarazy. Wezwał terminusów pustogorskich na pomoc.
* Alfred Bułka: służący / opiekun Kornela, który próbował go chronić przed atakiem bestii. Wysłany na bagno, dowiedział się trochę i się zaraził.
* Kornelia Weiner: Odkryła zarazę i ostrzegła Kornela o problemie.
* Alan Bartozol: źródło podstawowej wiedzy o pnączoszponach i terminus, który rozwiązał problem skażonej istoty.

## Plany

* 

### Frakcji

* 

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Samoklęska
                                1. Dom Kornela: cel ataku pnączoszpona i miejsce planowania ruchów
                                1. Laboratorium analityczne: gdzie przeprowadzono badanie próbek pnączoszpona
                        1. Trzęsawisko Zjawosztup: gdzie ostatecznie poległ skażony pnączoszpon

## Czas

* Opóźnienie: 1
* Dni: 2

## Narzędzia MG

### Budowa sesji

**SCENA:**: Nie aplikuje

### Omówienie celu

* nic

## Wykorzystana mechanika

1811

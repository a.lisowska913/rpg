---
layout: cybermagic-konspekt
title: "Nieśmiertelny komandos Saitaera"
threads: perypetie-w-raju
gm: żółw
players: onyks, mila, kamila_13, mateusz_k
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [190813 - Nieśmiertelny komandos Saitaera](190813-niesmiertelny-komandos-saitaera)

### Chronologiczna

* [190813 - Nieśmiertelny komandos Saitaera](190813-niesmiertelny-komandos-saitaera)

## Budowa sesji

### Stan aktualny

* .

### Pytania

1. .

### Wizja

brak

### Tory

* .

Sceny:

* .

## Punkt zerowy

### Postacie

### Opis sytuacji

Alfred był komandosem Noctis. Kiedyś. Podczas wojny on i jego oddział próbowali zniszczyć Saitaera w formie materialnej. Niestety, nie udało się - światło Saitaera zmieniło ich wszystkich. Alfred nie ma kontroli nad zmysłami, ma problemy z pamięcią i umysłem a jego nanomaszynowe, adaptujące ciało nie potrafi zostać zniszczone ani umrzeć. Alfred jest nieśmiertelnym potworem i antytezą wszystkiego o co walczył jako czysty noktianin...

## Misja właściwa

Scena zero.

Komandos (Alfred) poluje. Nie pamięta na co. Czuje tylko głód. Dopada jedną z owiec na Zdrowej Ziemi; pamięta, by nie krzywdzić ludzi. Przeżera się przez kilka i pozwala człowiekowi uciec, po czym widzi odlatującego awiana. Przekształca swoje ciało, rzuca się w pogoni za nim i zaczyna asymilację awiana po drodze wchłaniając AI Zain. Pożarł część ludzi na pokładzie, zaspokajając swój głód, pozostałym dał żyć; porwanego awiana zawrócił i oddalił się nim w kierunku na Ortus-Conticium. Do swojego domu.

Sesja właściwa.

Zespół natrafił na bardzo niestabilną i niebezpieczną sytuację. Trzeci Raj jest notorycznie niedoinwestowany; dlatego Robert Garwen zgodził się na to by miało tu miejsce jakieś działanie sił Aurum. Niedawno, awian (ciężki) w którym był między innymi Robert Arłacz, kilkunastoletni arystokrata Aurum, został porwany przez Potwora i zawrócony. Dlatego Maria Gołąb z Aurum komasuje tu siły - chce uratować i odbić chłopaka póki ten jeszcze żyje. Zespół jednak wie, że to nie jest najlepsza opcja; lepsza by była infiltracja. Ale jak? I czemu tu był młody arystokrata?

Maria nie jest chętna odpowiadać na żadne pytania, na wszystko ma jedną odpowiedź: "classified". Ale ogólnie młody szlachcic był tu z jakiegoś powodu i Marii nie zależy na wysyłaniu sił Aurum przeciwko anomalii. Zespół wykombinował inną strategię - komandos przed transformacją nienawidził Saitaera. Może da się cos zrobić by z tej nienawiści skorzystać? Ściągnęli więc ze Szczelińca nieaktywny artefakt Saitaera (spoiler: coś zrobił zanim trafił w ich ręce). Gdy go otrzymali? Konstruminus na odciągnięcie uwagi potwora, reszta zespołu ratuje młodego szlachcica. Plan jest, ogólnie, super. Chyba.

Czarodziejka - katai - poszła z konstruminusem któremu dali artefakt Saitaera. Konstruminus był kontrolowany przez TAI Elainkę (eLądowy AI Konstruminusa). Niestety, komandos zaatakował i pożarł Elainkę i zniszczył konstruminusa - ale katajka dała radę użyć swojej mocy i obudzić ducha starego noktiańskiego komandosa. Pierwsze, co ten próbował zrobić to samobójstwo. Niestety, niemożliwe; saitaerowy wpływ jest zbyt skuteczny i to ciało nie jest w stanie umrzeć. Po krótkiej komunikacji komandos powiedział katajce, że do żył tego młodego chłopaka, Roberta, wstrzyknięto jego krew gdy był w hibernacji.

To znaczy, że chłopiec skończy jak on. A on nie chce do tego dopuścić. Robi mu transfer krwi przy użyciu porwanego lekarza. Albo da się go uratować, albo trzeba go zniszczyć. Katajka porozmawiała z nim i skutecznie spowolniła jego powrót do bazy - reszta Zespołu jest w stanie uratować Roberta. (komandos po tygodniu straci kontrolę nad sobą, znowu - nanitki są silniejsze niż jego wola).

I faktycznie, wyciągnęli go. Lekarz był większym problemem; komandos splątał go ze stołem, więc nie dało się go wydostać (wydostali go potem - gdy komandos się zorientował że chłopak wypadł poza jego zasięg to sam go uwolnił). Ale chłopak został ewakuowany i zawieziony do Marii Gołąb.

Na miejscu okazało się, że ojciec wie o tym. Chłopak jest śmiertelnie chory; tylko regeneracyjna krew saitisa może mu pomóc. Jeśli nie uda się tego zrobić w ten sposób (krew), zawsze jest ten drugi - odkażenie przy użyciu Karradraela. Ale dziecko ma przetrwać. A będą pracowali nad antyciałami na Skażenie nanitkami.

Ogólnie, Zespołowi to pasuje. Dobrze się skończyło..?

**Sprawdzenie Torów** ():

* .

**Epilog**:

* Alfredem Ceruleanem zajmie się docelowo Eliza Ira - noktianin nie powinien w ten sposób skończyć.
* Robert Arłacz zostanie najpewniej Mausem; jedyny sposób by pokonać nanitki Saitaera.

## Streszczenie

Młody szlachcic z Aurum cierpi na nieuleczalną chorobę. Wstrzyknięto mu nanitkową krew komandosa noktiańskiego, który został Oświetlony przez Saitaera. Ten komandos porwał szlachcica i próbował odwrócić infekcję nanitkami; Zespół odbił młodziana i oddał go ojcu, który ma zamiar jakoś naprawić sprawę. 

## Progresja

* .

### Frakcji

* .

## Zasługi

* Robert Garwen: z uwagi na brak środków i pieniędzy w Trzecim Raju akceptuje dziwne działania Aurum z nadzieją na pieniądze.
* Robert Arłacz: młody chłopak cierpiący na terminalną chorobę; wstrzyknięto mu nanitkową krew Ceruleana by go uratować. Porwany przez Ceruleana i uratowany przez Zespół; najpewniej zostanie Mausem.
* Maria Gołąb: komendant Rodu Arłaczów; w misji sformowania oddziału szturmowego do zniszczenia Alfreda Ceruleana. Na szczęście do tego nie doszło. Zimna, dumna i lojalna.
* Alfred Cerulean: komandos noktiański; próbował dawno temu zniszczyć Saitaera i stał się nanitkowym regenerującym potworem. Jego krew wstrzyknięto Robertowi Arłaczowi i Alfred próbował zatrzymać transformację. Ciągle traci nad sobą kontrolę. Bardziej potwór niż mag noktiański którym był kiedyś.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski, NW
                    1. Ruiniec
                        1. Trzeci Raj: miasteczko militaryzujące się siłami Aurum by odzyskać porwanego Roberta; na szczęście Zespół zrobił to najpierw.
                            1. Zdrowa Ziemia: południowo-wschodnia część Raju; tam właśnie znajdują się zwierzęta hodowlane i ogólnie rozumiane rolnictwo.
                        1. Ortus-Conticium: anomalne miasto Wiecznej Wojny; jest tam leże / kwatera Nieśmiertelnego Komandosa.

## Czas

* Chronologia: Aktualna chronologia
* Opóźnienie: 300
* Dni: 5

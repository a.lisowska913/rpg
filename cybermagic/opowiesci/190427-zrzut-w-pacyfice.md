---
layout: cybermagic-konspekt
title: "Zrzut w Pacyfice"
threads: nemesis-pieknotki
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [190424 - Budowa ixiońskiego mimika](190424-budowa-ixionskiego-mimika)

### Chronologiczna

* [190424 - Budowa ixiońskiego mimika](190424-budowa-ixionskiego-mimika)

## Budowa sesji

### Stan aktualny

* Aleksander trafił do więzienia pustogorskiego. Karla chce go przesłuchać i dowiedzieć się, co wie.
* Olafowi nic się nie stało; Szpital Terminuski doprowadził go do normy w tydzień.
* Miasteczkowcy stwierdzili, że chomikowanie czarnych artefaktów to cholernie zły pomysł. Czarnym miejscem zostają Wolne Ptaki.
* Część Miasteczkowców, jak chciała Karla, przeniosło się do Wolnych Ptaków.
* Minerwa dostała dyskretny medal za zasługi od Karli.
* Część Miasteczkowców woli odsprzedać artefakty Wolnym Ptakom, ale niewielka ich część.
* Wolne Ptaki stają się tyci bardziej znaczącą frakcją i lokalizacją.
* Pojawia się prototyp szlaku handlowego między Wolnymi Ptakami i Cieniaszczytem.
* Artefakty Czarnej Technologii trochę są porozrzucane po terenie.

### Pytania

1. Czy Nikola zbuduje grupę zwolenników Elizy?
1. Czy Nikola zbuduje szlak przerzutowy dla Mateusza Warlena?
1. Czy Pustogor obróci się przeciwko Wolnym Ptakom?

### Wizja

* Porządkowcy skupują bieda-artefakty od Miasteczkowców
* Wolne Ptaki pierwszym miejscem wstępnego badania artefaktów
* Handlarz Anomaliami, Mateusz Warlen, próbuje złożyć szlak handlowy
* Nikola Kirys próbuje przebić się szlakiem przemytniczym przez Barierę

### Tory

* Wzrost znaczenia Elizy Iry: 5
* Wzrost znaczenia Wolnych Ptaków: 5
* Barbakan kontra Wolne Ptaki: 5
* Budowa szlaku przerzutowego: 4
* Martwa Nikola: 4
* Ranna Pięknotka: 4

Sceny:

* Scena 1: Ścigacz z orbity

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Misja właściwa

**Scena 1: Ścigacz z orbity**

Statek przemytniczy wystrzelił ładunek z orbity. Ładunek spadł w okolicach Pacyfiki. Pustogor jest perfekcyjnie przygotowany do walki z tamtą stroną; nawet grupa inwazyjna sobie z tym nie poradzi. Atena przekazała Pięknotce informacje, że to wyglądało jak ścigacz. Skanery Epirjona wykryły, że to jest najpewniej doskonałej klasy ścigacz. Pytanie - dla kogo miałby ktoś zrzucić ten ścigacz.

Niedługo później udało się statek przemytniczy powiązać z Cieniaszczytem. Jako, że Pięknotka ma najwięcej z Cieniaszczytem, została desygnowana do potwierdzenia bądź zanegowania czy to poważny problem dla Pustogoru. Innymi słowy - o co chodzi. Pięknotka zażądała od Barbakanu potężnej klasy tracker - dostała go. I jest gotowa do przeniknięcia do Pacyfiki. Pięknotka sama nie odważy się iść do Szczeliny ani w okolicę Szczeliny - to nie jest Trzęsawisko. Poprosiła o wsparcie (płatne) Erwina. Barbakan płaci i dał jej dostęp do Erwina. Pięknotka smiled with glee.

Erwin jest nieszczęśliwy. Nigdy nie chciał iść do Szczeliny z Pięknotką a Pacyfika to już praktycznie Szczelina - arkologia została wypaczona gdy pojawiła się Szczelina i jej topologia już nie jest naturalna. Część arkologii jest poza Szczeliną, a część jest w Szczelinie. Pojazd, oczywiście, wylądował daleko od Szczeliny. Erwin powiedział, że on woli zwykle omijać Pacyfikę gdy wchodzi do Szczeliny. Zbyt wiele przeszłości, wspomnień, Pryzmatu.

Pięknotka odwiedziła Minerwę w szpitalu. Przeprosiła ją za to wszystko; opowiedziała jej o sprawie z Cieniem. Czy Minerwa ma jakiś pomysł co się stało, czemu Cień z Pięknotką nie walczył. Czemu też w drugiej bitwie nie próbował się uruchomić (Tr+2:12,3,6=SZ). Minerwa doszła do tego.

* Pierwszy ruch Cienia polegał na ataku na Minerwę. Ta istota zadawała większe zniszczenia niż sam Cień.
* Potem Cień nie walczył, bo pragnie destrukcji. Pragnie dewastacji. Anihilacji. Nie musi sam tego czynić.
* Cień może odmówić uruchomienia się dla Pięknotki. Zdesynchronizował się; nie rozumie jej intencji a nie może dominować.

Po usłyszeniu tego, Pięknotka wzięła z zapasów Barbakanu stealth power suit. Nie może polegać na Cieniu, nie, jeśli ten jest tak humorzasty. Plus - nie chce nawet gdyby mogła.

**Scena 2: Ścigacz w Pacyfice**

Kiedyś sympatyczna i pokojowa arkologia, teraz miejsce składające się z ruin i wspomnień. O dziwo, czasem w Pacyfice robią kryjówki na skarby nurkowie Szczeliny. Niektórzy, jak to powiedział Erwin, to "farmerzy" - zostawiają rzeczy z nadzieją, że coś się z nimi stanie. A czasami w Pacyfice budzą się wspomnienia w formie anomalii.

Erwin i Pięknotka ścigają się z tajemniczym przeciwnikiem by móc dostać się do ścigacza odpowiednio wcześnie. Przeciwnik i tak ucieknie ze ścigaczem, ale Pięknotka i Erwin zdążą zadziałać.

(HrZ+3:9,5,17=P,P,P,SS): 3 encountery.

Pięknotka i Erwin stanęli przed pierwszym problemem - Pacyfika to teren Eschera. Sensory i skanery dalekiego zasięgu nie działają; działa tylko krótki zasięg i skanowanie. Co gorsza, cały teren jest pełen trudnych do wykrycia anomalii. Naszych dwóch agentów w power suitach dyskretnie i szybko przemieszcza się w kierunku celu. Teren Erwina; on prowadzi (Tr+2:9,3,5=P). W pewnym momencie zorientowali się, że wpadli w pętlę - poruszają się ciągle w tym samym terenie. Erwin westchnął i przygotował się do rzucenia zaklęcia - otworzy im przejście przez pętlę. (TpM:12,3,3=S). Erwinowi udało się rozerwać pętlę.

Raczej nic ich nie zauważyło; poza opóźnieniem nie jest tak źle. Czyli - wracamy do polowania na cel (Tr+3:10,3,5=P). Znowu wpadli na anomalię. Idą w dół; nie ma opcji wyjścia do góry. Droga jednostronna. Nie da się cofnąć, nie da się iść w górę, nie da się cofnąć. Erwin się zamyślił. Pięknotka jest całkowicie nieprzydatna, niezadowolona z tego powodu i nie wie co robić. Erwin idąc przez Pacyfikę pozostawiał za sobą lekko ponuplane żelki. Namierza się na jednego z tych żelków i odpala podręczny teleporter by wraz z Pięknotką wrócić do tamtego miejsca.

(TpZ:12,3,3=S). Erwin i Pięknotka znaleźli się PRZED anomalią. Erwin uspokoił Pięknotkę - jest w miarę dobrze. To normalne w okolicach Szczeliny i przy Pacyfice. Pięknotka ma ponury wzrok. Nie podoba jej się ta "normalność". Woli Trzęsawisko.

(Tr+4:11,3,5=SS). Erwin i Pięknotka drastycznie ominęli kolejną anomalię przestrzenną i wpakowali się prosto na Legion. Przemytnicy boją się, że ktoś ich znajdzie w Pacyfice - oni stworzyli Legion swoimi strachami. Jako, że wpadli na Legion, nie ma opcji po prostu się przez nich przekraść, trzeba im uciec. Pięknotka rzuca za sobą parę bomb dymnych, Erwin nawiguje by nadal byli na celu i próbują przedostać się przez Pacyfikę jak najbliżej tego przeklętego ścigacza. (TrZ+2:S). Dzięki stealth suitowi oraz bombom Pięknotki udało im się przycupnąć. Legion ich szuka, ale nie wie jak znaleźć. Zgubili ich.

(Tp:12,3,3=S). Pięknotka i Erwin skutecznie prześlizgnęli się w kierunku poszukiwanego ścigacza. W samą porę by zobaczyć jak ktoś zbliża się do niego. To pojedynczy mag, w power suicie. Ktoś z Wolnych Ptaków; Pięknotka pobieżnie rozpoznaje power suit. Szybko pozycjonuje się na jakiejś wieży i strzela snajperką by ów ścigacz oznaczyć i nie być wykrytym. (Tp:S). Pięknotce udało się oznaczyć przeciwnika. Widzi, jak ścigacz wprawnie jedzie w kierunku na Wolne Ptaki.

Tymczasem Pięknotka i Erwin są w środku Pacyfiki, mają resztki kapsuły zanim zostały Skażone. To jest super wiadomość, bo można je wziąć do analizy. Erwin zaproponował, by jednak przeskanować to teraz; nigdy nie wiadomo czy dadzą radę donieść to do Pustogoru. (TrMZ:10,3,7=SSM). Erwinowi udało się zrobić dokładny, dogłębny skan, ale zdezintegrował kapsułę. Nova magii. Wszystko w okolicy WIE że tu są Erwin i Pięknotka (no, że coś tu jest).

Trzeba wiać. Teraz Pięknotka jest główna w uciekaniu a Erwin w kierowaniu i nawigowaniu.

(TrZ+2:S). Pięknotce i Erwinowi udało się opuścić kluczowy, niebezpieczny teren. Nadal są w Pacyfice, ale już nie tam, gdzie pełnia zainteresowania. Zdecydowali się wracać w jakiś normalny teren; Pięknotka i Erwin dostali sygnał z pytaniem "wszystko ok? potrzebna pomoc?" - scramblowany. Erwin powiedział, że 9/10 to jest sojusznik który chce pomóc. 1/10 to jest wróg, który chce zastawić pułapkę i obłowić się lootem. Pięknotka woli jednak by o niej nie wiedzieli, więc wysłała sygnał że wszystko OK. Dostała odpowiedź "ok powodzenia".

Erwinowi i Pięknotce zostało tylko ostrożnie wrócić do Pustogoru i opuścić Pacyfikę. Teraz już się nie spieszą i działają mega ostrożnie. (Tp+2:11,3,3=P). Nadal jednak wpakowali się w anomalię. Tym razem jednak anomalia była bardzo niestabilna; rozpadła się dookoła Erwina i Pięknotki. Nutka jest do tego zaprojektowana i przetrwała to uderzenie; Pięknotka jednak jest ranna. Jej stealth suit nie zaprojektowany na Pacyfikę nie wytrzymał czegoś takiego.

(Tp+3:12,3,2=S). I spokojnie, acz z ranną Pięknotką, wrócili do Pustogoru.

**Scena 3: Analiza danych**

Erwin nie puścił Pięknotki, póki ta nie poszła do Szpitala Terminuskiego. Lucjusz powiedział, że Pięknotce nic nie jest. Lucjusz jest lekko urażony tym faktem - wszystkich dewastuje a jej nic nie jest. No ok, połamana i na painkillerze. Nic czego parę godzin nie rowiąże.

Następnego ranka Pięknotka poszła do Barbakanu. Ma wszystkie dane od Erwina, ma też tracer wskazujący na Wolne Ptaki. I Epirjon może przybliżyć informacje o tamtym pojeździe oraz pilocie.

Co Pięknotka się dowiedziała:

* Pilot ścigacza to Nikola Kirys. Starsza agentka (~50), kiedyś Inwazja Noctis. Działała zwykle w Cieniaszczycie i okolicy. Erwin powiedział, że Nikola jest z tych pomagających wszystkim. Jak to powiedziała, "stoi po stronie ludzkości przeciwko chaosowi".
* Ścigacz został powiązany z magiem z Cieniaszczytu. Wpływowy i bogaty Mateusz Warlen. Nigdy nie miał nic wspólnego z Inwazją Noctis. Ale zarabia na przemycie i niebezpiecznych przedmiotów.
* Nawet tej klasy ścigacz nie przejdzie przez Pustogorską Barierę. Anomalie po stronie Pacyfiki i Szczeliny uniemożliwią powrót na orbitę. A Nikola nie ma jak wrócić omijając Pustogor.
* Czujniki na ścigaczu pokazały, że chodzi o przemyt artefaktów i czarnej technologii. Sam ścigacz jest genialny - lekki, szybki, pancerny, stealth. Best in class. Szkoda, że nie przejdzie przez Pustogor.

**Sprawdzenie Torów** ():

Wpływ:

* Ż: 
* K: 

2 Wpływu -> 50% +1 do toru. Tory:

* Ścigacz przejęty: 3: 3
* Nikola uciekła: 3: 
* Ranny Zespół: 4: 1

**Epilog**:

* 

## Streszczenie

Do Pacyfiki ktoś z Cieniaszczytu zrzucił świetnej klasy ścigacz. Pięknotka i Erwin zinfiltrowali Pacyfikę by dostać namiary na ten pojazd. Okazało się, że pilotem ma być Nikola - też Nurek Szczeliny, świetnej klasy. Eks-Noctis. Pięknotka po raz pierwszy była w Pacyfice. Dodatkowo, dowiedziała się od Minerwy, że Cień niekoniecznie odpala się na żądanie, nie rozumie Pięknotki. Świetnie.

## Progresja

.

### Frakcji

* .

## Zasługi

* Pięknotka Diakon: przygotowała akcję dojścia do zrzuconego ścigacza; zobaczyła dlaczego Pacyfika jest tak strasznym miejscem i doceniła Erwina ponownie
* Erwin Galilien: niechętnie wziął Pięknotkę do Pacyfiki; płynnie po Pacyfice nawigował i skutecznie doprowadził Pięknotkę do tajemniczego ścigacza i Nikoli
* Minerwa Metalia: pomogła Pięknotce zrozumieć dlaczego Cień zachowuje się tak pasywnie, mimo, że nadal leży w szpitalu
* Nikola Kirys: była agentka Inwazji Noctis, świetny pilot i Nurek Szczeliny. Porusza się płynnie po Pacyfice i zdobyła swój ścigacz; wykryta przez Erwina i Pięknotkę

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski, SW
                    1. Granica Anomalii
                        1. Pacyfika: wymarła arkologia zbyt blisko Szczeliny; Erwin i Pięknotka się przez nią przekradali by znaleźć zrzucony ścigacz

## Czas

* Opóźnienie: 2
* Dni: 2

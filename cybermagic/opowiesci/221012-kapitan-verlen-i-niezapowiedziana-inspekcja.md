---
layout: cybermagic-konspekt
title: "Kapitan Verlen i niezapowiedziana inspekcja"
threads: historia-arianny, program-kosmiczny-aurum
gm: żółw
players: fox, kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [210921 - Kapitan Verlen i pojedynek z marine](220928-kapitan-verlen-i-pojedynek-z-marine)

### Chronologiczna

* [210921 - Kapitan Verlen i pojedynek z marine](220928-kapitan-verlen-i-pojedynek-z-marine)

## Plan sesji
### Theme & Vision

* Królowa Kosmicznej Chwały, support ship

### Co się wydarzyło KIEDYŚ

* .

### Co się wydarzyło TERAZ (what happened LATELY / NOW)

* .

### Co się stanie (what will happen)

* S01: pierwsze działanie w stylu ćwiczeń. FAILURE. Nawet jak się da strzelić, nie działa jak powinno.
* 
* S0N: statek nie jest w pełni sterowny a trzeba doprowadzić go do działania.
* S0N: Puryści Seilii chcą pozbyć się Leony i sarderytów. Leona ich chroni.
    * 3 ze statku 7 z Aurum, 2 osoby z Aurum to advancerzy (tien Szczepan Myrczek + tien Mariusz Bulterier)
    * tak, robią też sabotaż przeciw Leonie
* S0N: część załogi: nie lubimy Aurum. Nie będziemy się starać (bosman Tadeusz Ruppok). Resentymenty, czemu mamy pracować?
* S0N: przerażony Tomasz Dojnicz (marine) ma zatrzymać bójkę między ludźmi - "ludzie Aurum" vs "ludzie - załoga"
    * "Aurum forever"
* S0N: Klaudiusz Terienak: ta jednostka miała dla niego wartość BO WŁADAWIEC. Pozbyć się Arianny.
* S0N: Oficer łącznościowy (tien Maja Samszar) nie przekazuje Ariannie niczego
    * Inspekcja, Arianna nie jest gotowa
    * It is personal; Romeo kiedyś skrzywdził kuzynkę Mai.

* S_high_level: dolecieć do Karsztarina i wprowadzić advancera na pokład
    * Maja -> Tomasz (taunt Arianny), Maja -> Arianna (nie mówi o inspekcji); Maja jest bardzo pro-Aurum

.

* Pierwszy oficer - Władawiec Diakon - podrywa na lewo i prawo bawiąc się hormonami.
    * W medycznym jest tien Klaudiusz Terienak który chce zdobyć uznanie Władawca, ale boi się Leony.
* Aurum podzieliło się na frakcje i próbują wygrywać między sobą.
* Leona Astrienko jest tu na wakacjach - ale też ma uniemożliwić śmierć kogokolwiek czy jednostki.
    * 2 sarderytów, Hubert i Irkan nie dają się jej ale też w sumie nikomu skrzywdzić.
* Leona + 5 marines opanowuje tą 40-osobową jednostkę.
* STRASZNA nieufność Orbitera do Aurum
* Klarysa Jirnik, artylerzystka i odpowiedzialna za broń
* tien Grażyna Burgacz, logistyka i sprzęt
* tien Arnulf Perikas, fabrykacja i produkcja - on jest bardzo za Arianną i najbardziej rozczarowany Alezją

### Sukces graczy (when you win)

* .

## Sesja właściwa
### Wydarzenia przedsesjowe

.

### Scena Zero - impl

.

### Sesja Właściwa - impl

DZIEŃ 1:

Arianna jest ranna. NIBY zasoby medyczne są zamknięte, ale Klaudiusz Terienak jest we frakcji Władawca. Ale tak głupio krwawić na pokład. Zażądała bandaży z medycznego, "nic wartego uwagi". Klaudiusz AUTENTYCZNIE zaskoczony. Jest medykiem i nie rozumie co się dzieje.

* K: "Jakby była inspekcja, co pani powie?"
* A: "Patrząc że niedługo ćwiczenia militarne, potrzebujemy medykamentów bardziej, nie tracimy zaopatrzenia"
* K: "...to jedna z tych rzeczy Verlenów? Oczywiście, jeśli mogę wiedzieć."
* A: "Trochę tak..."

Klaudiusz nie ma wątpliwości więcej. On już wie. On rozumie. I dał Ariannie zapieczętowane rzeczy. Arianna - PARANOIA MODE. A jak chodzi o jedzenie - Arianna żąda konserw do jedzenia. Twierdzi, że "jako kapitan powinna dawać dobry przykład i odrzucić przyjemności dopóki wszyscy na statku nie będą w nienagannej kondycji."

Grażyna jest za. Grażyna TEŻ przeszła na konserwy. Spotkania w mesie oficerskiej i podczas tych spotkań jest świetnie - wszyscy jedzą jedzenie a Grażyna i Arianna i Daria (bo jest ostrzeżona) wpieprzają konserwy. I to jest takie dziwne. Alezja też przeszła na konserwy, bo, de facto, dostała polecenie ("rehabilituj się!").

Na oficerów padł pomniejszy strach. Arianna CHCIAŁABY przejść na konserwy ze wszystkimi (by detoksować) ale nie chce zacząć otwartej wojny ze wszystkimi. Za to załoga się cieszy.

Arianna ma kilka rzeczy które chce osiągnąć:

1. detoks oficerów od tego co wsadza im Władawiec
    * -> odpada; nie od razu po ciepłej wodzie. Przeżyjemy na części oficerów na konserwach
2. doprowadzenie jednostki do sprawności
    1. nie działa TAI Semla
    2. większość materiału produkcyjnego jest zmarnotrawiona na wyścigi psów
    3. załoga jest w stanie wojny Aurum - nieAurum
        1. bosman Ruppok od alkoholu i hazardu
        2. sarderyci vs seilici
        3. marines (po stronie Arianny w większości) i reszta załogi
        4. Aurum (dla chwały i yay, kosmos) - nieAurum (łatwa prosta fucha)
    4. "kultura oficerów Aurum"
3. usprawnienie Alezji jako osoby
4. załoga ma Ariannę słuchać i nie próbować się jej pozbyć
5. przeprowadzić ćwiczenia
    1. silniki
    2. advancer

Arianna decyduje się zająć ogarnięciem TAI albo wojny na pokładzie. Wojna na pokładzie brzmi... gorzej. A że dotyczy ZAŁOGI a nie oficerów, to nie masz szczególnych kłopotów politycznych. Co więcej, oficerowie też nie chcą wojny na pokładzie - wspólny cel.

Arianna WIE, że kiedyś powinna być inspekcja (nowy kapitan itp.) i WIE, że będzie poinformowana wcześniej. Ale powinna być "na dniach". A wyprzedzenie masz 2-3 dni.

* Statek ma 40 osób
    * 10 to seilici: 2 advancerów (obaj są tienami), 5 z Aurum, 3 próżniowców.
        * oni nie robią głupot; nie piją, nie robią kłopotów
        * oni mieliby kłopoty ze strony niektórych, gdyby nie to, że tam jest dwóch tienów.
        * oni polują na sarderytów a Leona poluje na pretekst.
    * 2 sarderytów
        * jeden sarderyta ma POTENCJAŁ na bycie advancerem ale nie jest advancerem, jeden ma POTENCJAŁ na bycie mechanikiem ale nie jest.
        * bo oboje nie mają uprawnień. Plus siedzą w karcerze. "By ich chronić".

Alezja nie wie czemu są na pokładzie. Za poprzedniego dowódcy statku jeden był advancerem a drugi inżynierem. To było nielegalne. NIE ZROBILI PAPIERÓW. Oni są tu za pieniądze, czyli Aurum płaci za to że siedzą tutaj. Poprzedni kapitan, przed Alezją, był bardziej pragmatyczny - przyjął ich bo są kosmiczni. MOGĄ pracować bo jednostka jest z Aurum. Ale to się nie stało.

Sarderyci do teraz myślą że to jakiś eksperyment.

Arianna chce ich jak najszybciej do służby. Chciałaby się skupić na advancerze - pokazać że dał radę i usiąść im na honorze.

Sarderycki Advancer HUBERT. (ENCAO: +0-+-, Optymistyczny i wesoły, Achievement nie Humility, silnik: Wanderlust)

Arianna i Hubert.

* H: "pani kapitan, to zaszczyt." Ponad 60% ciała to mechanizmy i inne takie. Wyraźnie ma też jakieś xenografty. SZPETNY JAK NOC.
* A: "jestem nowym kapitanem, zorientować w sytuacji"
* H: "bardzo dobre jedzenie, świetna kwatera, ciekawe ćwiczenia. Czasem Leona wrzuca im marines na ćwiczenia." <-- jest taki pasywny optymizm. Nie zauważył.
    * -> czasem Leona wrzuca kogoś innego na ćwiczenia by sarderyci ich sklupali.
    * -> Leona zachowuje się jakby nimi dowodziła więc jej słuchają bo NIKT NIE WYDAJE IM ROZKAZÓW.

4 miesiące temu w przestrzeni, za kapitan Alezji. Grali w piłkę. Leona, on i inżynier-sarderyta. Bez pełnych skafandrów, więc tylko 15 minut. "Nie jesteśmy dość dobrzy by ćwiczyć z innymi advancerami jeszcze". A przynajmniej tak powiedziała Leona. "Za poprzedniego kapitana było inaczej, teraz jest inaczej i przyszli jacyś elitarni advancerzy."

Arianna przywraca go do oficjalnej służby. Hubert "czyli JESTEM DOŚĆ DOBRY." Arianna chce, by pokazał co umie. Hubert jest 100% za. I Arianna powiedziała, że jak spełni wszystkie oczekiwania i najlepszy to będzie dowodził. Hubert się MEGA ZAPALIŁ. Teraz będzie ćwiczył jak ANIME PROTAGONIST.

Arianna chce wywalić dwóch tienów i Huberta - niech zrobią badania poszycia. Dokładne. Niech sprawdzą po raz pierwszy od CHOLERA WIE KIEDY (zgodnie z logami? 3 miesiące temu) jak działa poszycie i zewnętrzna część Królowej. Daria idzie z nimi. Daria + Hubert jako para, druga para to seilici.

ExZ (rywalizacja + chęć pokazania pani kapitan) +3+3 per kolor (3 niebieskie 3 zielone):

* Vg: seilici znaleźli coś DZIWNEGO. Proszą Darię o spojrzenie.
    * normalnie byłoby to zignorowane bo to nie jest "problem". Ale teraz chcą się wykazać. Plus, mają paranoję (superćwiczenia pani kapitan co żre konserwy)

Advancerzy do siebie: "Podobno je konserwy bo marine ją zranił i to forma pokuty a Verlen nie daje się zranić w walce"

Daria jest osobą "światową". Dużo widziała i dużo robiła. Advancerzy Seilii znaleźli Dziwne Zamaskowane Urządzenie. Daria chce ocenić co to za urządzenie. (+2Vr)

* Vz: Daria wie co to jest. Widziała to. Sama to podkładała kiedyś. To są scramblery TAI. To wysyła sygnały tak, by TAI się gubiła. Stosowane przez piratów i ludzi którzy chcą kupić coś po taniości. Zwykle jeden wystarcza na Persefonę. Ma to długi czas baterii i jest dobrze schowane; wygląda jak podłożone jako Bardzo Skuteczne Ćwiczenia Advancera.

konflikt -> Tr

* V: udało się Darii znaleźć kilka rzeczy. Zajęło to cały dzień.
    * system wpięty w silniki. To trochę tłumaczy. Akcelerator - decelerator.
    * kilka pomniejszych podsystemów by statek gorzej działał

Ten statek nie od zawsze tak działał - zaczął tak działać odkąd zaczęło pojawiać się za dużo skutecznych magów Aurum. Gdy Władawiec zaczął przejmować dowodzenie i wszystko zaczęło działać dobrze (mimo jego metod) to nagle zaczęły się problemy z deteminizmem. Sprawa dość świeża, kilka miesięcy. Jeszcze przed Alezją. Alezja po prostu trafiła na statek już zepsuty.

Daria i advancerzy zabrali się za usuwanie tego wszystkiego ze statku - najpierw scrambler TAI... po odscramblowaniu, trzeba uruchomić TAI. Wszystkich inżynierów na powierzchnię statku by pousuwać. Plus to dobre ćwiczenia. I przed wyjściem na "skórę" mówka "nie tylko będzie ciepła woda ale będziemy mieć kawę wszędzie".

Ex Z (to jak Daria zna statek, ludzi itp i kontekst) +3:

* Vz: statek jest czyszczony, rzeczy są usuwane, dokładne badania itp.
* Vz: wszystkie ważne rzeczy zostaną usunięte.
* X: 2 floaters. I panikują.
* X: mimo wysłanych advancerów jeden floater złapany. Drugi chciał wrócić w panice, włączył silnik skafandra i leci.

Poleciała pinasa i złapała floatera który dostał opieprz za niezgodność z procedurami itp. Floaterzy nie dostają karniaka ale robią ćwiczenia wśród inżynierów "poruszanie się poza statkiem". Arnulf zaproponował Darii dobry flogging lub oglądanie filmów jak floaterzy lecą do silników i giną. Daria "mam inne podejście - nie wiedzą co robią". Arnulf się żachnął - powinni wiedzieć. Daria - jak nie są nauczeni to nie będą wiedzieć. Arnulf się zajmie fabrykacją.

A zdemontowane rzeczy -> statek, części zapasowe. "Sarderyci lubią to".

* Arnulf -> Arianna: "Mogę zobaczyć te części? Chciałbym zbadać ich pochodzenie naszymi fabrykatorami i urządzeniami. A nuż to nasze... podejrzewam sabotaż."
* Arianna -> Arnulf: "Lepiej na miejscu"
* Arnulf -> Arianna: "Możemy to zgłosić jako sabotaż i możesz żądać wsparcia i pomocy"
* Arianna -> Arnulf: "Wpierw dowiedzmy się kto za tym stał jak możesz".
* Arnulf -> Arianna: "Zrobię co mogę, taki plan."

Dzień 3:

Darii udało się wyczyścić całość Królowej ze śmieci. A do Arianny przybył marine z czapką w ręku, tak pokornie. "Czy mógłbym być zaszczycony gdyby pani zrobiła ze mną trening?" Arianna jest za. "Bo jest pani świetna." Czegoś Ariannie nie mówi, ale jest szacunek.

Marine proponuje walkę próżniową przy użyciu broni białej i walki wręcz. Arianna proponuje walkę w zwarciu bez próżni. Proponuje mu Huberta. Marine się wzdrygnął. Dobrze, to zacznijmy od walki w zwarciu po prostu. NIE TEN MARINE NIE WIDZI ŻADNYCH PROBLEMÓW. Umówili się na konkretny termin który marine zaproponował i są gotowi.

Arianna -> kapral Pies. Pies powiedział Ariannie, że Tomasz specjalizuje się w walce w próżni. Ale Tomasz zaproponował ćwiczenia Ariannie? Że chce się podszkolić? Nie, to nie ma sensu. "Rozwiążę temat, pani kapitan." Zdaniem Psa, Tomasz nie zaproponowałby Ariannie treningu sam z siebie. Czy to mogła być Leona? ...tak.

Arianna -> advancerów Seilii: "znaleźli, wykazali się jako ludzie". A do Huberta -> pracuj ciężko dogonisz ich.

Tr Z (pierwszy kapitan który coś robi i FAKTYCZNIE coś się dzieje) +3:

* V: Advancerzy Seilii wspierają Ariannę. Są za Arianną i będą innych seilitów namawiać by dali jej szansę póki "nie spieprzy"
* XX: Advancerzy Seilii są bardzo zmotywowani by ZAWSZE być lepsi od sarderytów i vice versa. Mamy "zdrowy konflikt", ale za daleko. Nie sabotują zadań ale naprawdę naprawdę się nie lubią z przyczyn społecznych.

PÓŹNIEJ PO TRENINGU będzie dało się uruchomić Semlę.

Arianna vs Tomasz. Zapasy + broń biała. Tomasz podchodzi do sprawy bardzo profesjonalnie, jest dobry. Ale nie jest tak dobry jak Szymon.

Tr Z (Tomasz za bardzo próbuje nie uszkodzić Arianny) +3 +2Or (rana):

* Vz: Tomasz podszedł za ostrożnie, za daleko - jest dobry, ale strasznie próbuje Arianny nie skrzywdzić. Arianna wchodzi z pełną bezwzględnością, łokciem w twarz, "nie jestem ze szkła żołnierzu, przyłóż się do ćwiczeń".
* X: Tomasz się przyłożył. Zaatakował szybko ostrzem Arianna musiała natychmiast parować i też walnął w twarz głową a potem "przepraszam" widząc krew z nosa i zatoczoną Ariannę. (+Vg)
* V: Arianna się zatacza i korzystając z zataczania wyprowadza cios udając że za bardzo się zatacza. "Jeśli odpuścisz jak przeciwnik jest słaby to nie wygrasz. Rób szybko i bezwzględnie. Szybko skończ walkę." Solidny kop w jaja.
    * Tomasz się przykłada. +2Xr.
* Xr: Tomasz ogólnie zrobił wyrzut, wybił się, złapał Ariannę. Puścił broń. Korzysta z czystej siły i de facto Ariannę WYKRĘCA. Arianna wykorzystuje jego siłę i energię by zrobić salto nad nim.
* Vz: "LEPIEJ" i Arianna się wybija. Koleś jednak szybko się obraca. Stracił hold na Ariannie, Arianna ma broń, on nie ma broni. Tomasz kombinuje. (+3Vg) Atakuje piaskiem w twarz i tunikę zarzuca na broń Arianny i wbija się na nią.
* X: Udało mu się Ariannę rozbroić i solidne zderzenie pełną masą. Arianna na ziemi. On na nią - by ją złapać i unieszkodliwić.
* Xz: Tomasz unieruchomił Ariannę.

TAP OUT.

W zupełnie innym miejscu. W inżynierii (kalibracja Semli itp) -> Daria analizuje sensory, co Semla widzi itp. Kontrola poprawności. Śluza 2A jest aktywowana. Sygnał z pinasy inspekcyjnej. Pierwszy oficer Władawiec zaakceptował sygnał i wpuszczanie pinasy. Wysyła grupę powitalną. A z Arianną jest tylko Szymon.

Daria uruchamia Semlę awaryjnie. Czas na testy i reanimację. A na serio: Semla d'Królowa, pod kontrolą Arianny Verlen. Statek Semli, kapitan Semli. I Daria hintuje Semli "dyskretnie ostrzeż panią kapitan o niespodziewanych gościach". Plus nadzieja na spowolnienie inspekcji by nie ucierpiała Arianna.

Ex Z (bo Królowa się sypie) +3:

* X: Semla reanimowała się i przejęła kontrolę nad jednostką. To spowodowało że niestandardowe rzeczy przestają działać. Reset fabryczny. Jest ciepła woda.
* Xz: Autodiagnostyka -> wyłączony reaktor itp. 10 minut autodiagnostyki, panika. Inspekcja TEŻ wie.
    * nie wiedzą DLACZEGO ale wiedzą że muszą POCZEKAĆ
* X: Arianna i Tomasz są odcięci w sali treningowej. "Arianna robi tapout nagle gaśnie światło i ciemność". Plus Maja skasowała dowody.
    * Semla szuka kapitan na pokładzie. Nie może jej znaleźć, bo nie ma sensorów sprawnych w sali treningowej. Więc szuka.

.

* Tomasz -> Arianna: "Pani kapitan, PRZEPRASZAM. Nie chciałem by użyła pani magii". "Nie użyłam magii w ogóle". <suspicious dog meme>.
* Arianna -> Daria: "gdzie są ubytki". Daria przekazuje Ariannie co-i-jak odnośnie tej sali.

Arianna przepełza po wentylacji do cywilizowanego miejsca. Z marine jako wsparciem. I żąda by Daria zapewniła kogoś kto ją wypuści.

Tr Z (znasz drogę) +4:

* X: Arianna napotkała na elementy wyścigów psów. Jeśli pójdzie jak pies to nie ma spowolnienia. WOLI SPOWOLNIENIE.
* X: ALBO inspekcja musi czekać ALBO świeżo po treningu -> to drugie. Po treningu.
* X: Inspekcja wyrobiła sobie opinię na temat jednostki.

Arianna wypadła wyciągnięta przez techników i poszła szybkim krokiem na mostek. A tam czeka na nią jej grupa oficerów (bez Darii - ona kalibruje Semlę) plus POWAŻNY INSPEKTOR, Adam Chrząszczewicz.

Adam: (ENCAO: --+00, trudny do zaskoczenia i polega na sobie, Power > Family, Preserver: "this planet is dying...")

* Adam: "Zapowiedziałem inspekcję dwa dni temu"
* "Naprawialiśmy TAI, więc wiadomość gdzieś się zapodziała"
* Adam zaczął myśleć, coś chciał powiedzieć. Spojrzał na innych oficerów. "Rozumiem."
* Adam: "Czyli inspekcja była dla pani niezapowiedziana."
* Arianna: "Zgadzam się, nie spodziewaliśmy się dzisiaj żadnego gościa"
* Adam spojrzał na Władawca. Władawiec potwierdził. "Nie miałem informacji o inspekcji a albo ja albo pani kapitan jest na mostku."
* Adam pomyślał. "Rozumiem, TAI stanowi problemy. Dlaczego zatem pani kapitan nie miała czasu przejść do formy bardziej prezentującej się? Minęło pół godziny od momentu mojego pojawienia się."
* Arianna: "Wolałam upewnić się osobiście a nie wiedziałam czemu pan przybywa."
* Adam: "I nikt nie powiedział pani że się zbliżyłem? Że pinasa zbliża się do statku i z jakim celem?"
* Arianna: "Przestało coś działać, za późno otrzymałam informację."
* Adam: mem z nieufnym psem.
* Arnulf: "To był sabotaż. Kapitan Verlen nie miała jak wiedzieć. Nie wiedzieliśmy czemu jednostka nie działała ale kapitan Verlen znalazła próby sabotażu. I dlatego mamy pełen restart systemów. Nic dziwnego, że mało co działa - ale będzie działać dobrze."
* Adam nic nie powiedział ale coś zanotował.
* Adam: "Przywykłem do normalnych jednostek - stan mundurów, przepisy pożarowe... pani kapitan, czy jest coś co pani chce mi powiedzieć?"
* Arianna: "Może chce pan obchód po statku? Pokażę panu jednostkę."
* Adam: "Nic co pani kapitan mi pokaże nie przekona mnie, że ta jednostka jest w dobrym stanie."
* Arianna: "Mogę pana przekonać, że jest w gorszym niż pan uważa. Nie chce się pan ze mną przejść?"
* Adam: "Dobrze, chodźmy." (do Arnulfa) "Poproszę o raport i informacje odnośnie sabotażu na mojego pada."
* Arnulf: "Zero problemu."

Arianna wzięła Adama na stronę. Skompilowała dane z dwóch ostatnich tygodni (gdy się pojawiła) i co było przedtem i co zmieniła. Niech nie oczekuje cudów. Adam dostał raport, przejrzał go szybko. Zrobił "hmm". Arianna "nie wiem kiedy był pan ostatnio na tej jednostce ale na ćwiczeniach nie poruszyła się z miejsca". Adam "za pani czasów jednostka prawie uszkodziła inną PRZESUWAJĄC SIĘ." Arianna "sabotaż." Adam "poprzednio może też był ten sabotaż, prawda?" Arianna "możliwe, nikt nie podjął wysiłku by to sprawdzić."

* Adam: "Jeżeli nikt nie podjął wysiłku, czemu utrzymujesz poprzednią kapitan jako oficera?"
* Arianna: "Druga szansa. Dostała równie trudne warunki jak ja. Może być dobrym oficerem nawet jak nie sprawdziła się jak kapitan."
* Adam: hmm.
* Adam: "Czemu nie zażądałaś jakiejś pomocy?"
* Arianna: "Zmierzamy ku lepszemu, jesteśmy w trakcie dokonywania napraw, coś z czym sobie nie poradzimy to zwrócimy się o pomoc."
* Adam: "Masz dość MATERIAŁÓW by sobie poradzić?"
* Arianna: "Prawda, nie będzie ich dość. Ale dopiero niedawno odkryliśmy efekty sabotażu. Zbędne elementy na statku."
* Adam: "Masz czas budować kult bohatera zamiast dowodzić jednostką?"
* Arianna: "Jeśli nie ćwiczę ciała, idę na dno a jednostka ze mną."
* Adam: "Co powiesz, jeśli Ci powiem, że DOSTAŁEM ack z Twojej jednostki na inspekcję i to nie mogła być Semla?"
* Arianna: "Mamy sabotażystę na pokładzie"
* Adam: "Nie ufasz swoim oficerom?"
* Arianna: "Części, tak."
* Adam: "Nie poprosiłaś o nikogo z zewnątrz komu mogłabyś ufać?"
* Arianna: "Mam Darię która ze mną przybyła, nie jestem tu sama. Nie mogę wymienić pełni oficerów. Są to oficerowie Aurum."
* Arianna: "Nie wygląda to w Aurum tak dobrze jak w Orbiterze."
* Adam: hmm.
* Adam: "Słuchaj, jesteś nowym kapitanem. To co zastałem normalnie byłoby strasznym problemem dla dowódcy statku. Sama wiesz, że masz unikalną jednostkę i z perspektywy politycznej i z perspektywy dowodzenia. Mam wrażenie, że ktoś próbuje Cię podkopać. Że coś tu jest nie tak. Nie mogę działać wbrew zasadom więc dam Ci naganę. Ale nie wycofam Cię z jednostki i udzielę Ci wsparcia. Nie próbuj robić wszystkiego sama - nie dasz rady. Ściągnij kogoś z Orbitera lub z Aurum komu możesz ufać. Bo to Ci się będzie powtarzać. Ten sabotaż nie wygląda na to, że pochodzi z Twojej jednostki - to jest typowy sprzęt możliwy do kupienia na Valentinie. Ale nikt z Twojej jednostki nie był na Valentinie. Kupiliby to gdzieś indziej, inny model. Aurum używa innych modeli. I teraz się zastanów - czy TY masz do mnie pytania lub czegoś potrzebujesz?"
* Arianna: "Dostałam zniszczoną jednostkę, pomoc nie chciała przyjść."
* Adam: "Mylisz się. Dostałaś zniszczoną jednostkę. Ale nigdy żaden kapitan nie poprosił o pomoc Orbitera. Nigdy. Aurum zawsze chciało być maksymalnie niezależne."
* Arianna: "Alezja była z Orbitera."
* Adam: "Alezja była dumna. A potem przestało jej zależeć. Ty możesz być pierwszym kapitanem który się zwróci do Orbitera."
* Adam: "Nie idź w ślady Alezji. Bądź rozsądnym oficerem Orbitera. Skorzystaj ze wsparcia."
* Adam: "Dokładnie tą samą rozmowę miałem z Alezją."
* Arianna: "Myślałam że częścią szkolenia jest wysłanie kapitana na rozpadający się statek by się zahartował lub zginął."
* Adam: "Nie myśl jak sarderytka."

Arianna akceptuje to co Adam mówi i nie ma więcej komentarzy. "Nieważne, że nagana wyprowadźmy ten statek." I Arianna poprosiła o ogarniającego psychotronika do kalibracji Semli. Adam się zgodzi i dostarczy.

Arianna konfrontuje Tomasza - skąd ten pomysł żeby robić ćwiczenia.

TpZ (bo ją szanuje i lubi) +3:

* V: "Bo słyszałem, że kapitan nie jest dobra w walce próżniowej i może panią kapitan spotkać krzywda. Chciałem pomóc, naprawdę."
* V: Tomasz usłyszał jak Maja rozmawiała z Władawcem o tym, że Arianna lubi walczyć i nie radzi sobie w próżni. I że "ktoś musi jej pomóc jakby co, bo jest dobra w walce i może przeszarżować". A on WIE że jak ktoś jest dobry to czasem ma tendencje do błędów w innej domenie.
    * Czemu tego dnia? -> bo Maja Perikas powiedziała, że to najlepsza pora. Arianna będzie miała czas itp.

Arianna powiedziała, że Tomek może ją poćwiczyć w próżni. Tomasz zdziwiony że to takie łatwe. TAK SIĘ DA.

* Arianna -> Maja: "Czemu nie było sygnałów o zbliżającej się pintce?"
* Maja -> Arianna: "Nie umiem tego zinterpretować. Był wysłany z mojej konsoli ale nie trafił do pani."
* Arianna -> Maja: "Problem na linii danych technicznie?"
* Maja -> Arianna: "Dokładnie tak."
* Arianna -> Maja: "Kto udzielił pozwolenia na dokowanie pintki?"
* Maja -> Arianna: "Władawiec, nie żeby miał wybór"

Czas do Władawca. Czy potwierdza to co powiedziała Maja.

* W: "Nie wiedziałem, naprawdę. Arianno, nie wiedziałem. Mamy umowę, Ty i ja. Nie złamałem umowy. Wiem, że tak wygląda ale tak jest."
* W: "Dołożę starań, by to co się dzieje na tej jednostce zostało na tej jednostce. To Ci mogę obiecać."

## Streszczenie

Podczas skanu kadłuba Królowej advancerzy z Darią wykryli scrambler TAI, destabilizator silników itp. Arianna stoczyła bitwę ćwiczebną z marine, ale pojawiła się inspekcja (która nie była zapowiedziana bo Maja nie przekazała informacji). Daria reanimowała TAI, wszystko częściowo poszło nie tak. Ale o dziwo audytor chciał pomóc Ariannie, która stanęła między Orbiterem (wsparcie) i Aurum (my sami sobie poradzimy). Arianna upewniła się, że Władawiec nie stał za operacją - najpewniej Maja. Ale Królowa jest w lepszym stanie niż kiedykolwiek, tylko, że Arianna je konserwy (by nie wpaść pod feromony itp)

## Progresja

* Arianna Verlen: NAGANA DO AKT że nie zgłasza stanu jednostki i próbuje sama zrobić bez wsparcia itp - przez co jednostka ucierpiała.
* Arianna Verlen: coraz większy szacunek na Królowej Kosmicznej Chwały: marines, advancerzy, sarderyci, seilici. Ma też wsparcie u Arnulfa i Grażyny. Pojawia się szacunek do niej za żarcie konserw póki Królowa nie będzie w dobrym stanie (lub: bo Szymon ją skaleczył podczas pojedynku). 

### Frakcji

* .

## Zasługi

* Arianna Verlen: przeszła na konserwy i spartańskie żarcie póki jest na Królowej; stoczyła starcie z marine i wybroniła się przed inspektorem Królowej nie zdradzając że jej oficerowie ją wrobili. Odzyskuje kontrolę nad Królową i zaczyna zaskarbiać sobie zaufanie wielu grup (seilici, sarderyci, marines, dwóch oficerów)
* Daria Czarnewik: wykryła scrambler TAI i inne rzeczy na powierzchni Królowej, potem wykorzystała to jako okazję do ćwiczeń. Reanimowała Semlę. Odzyskuje inżynieryjną kontrolę nad statkiem.
* Maja Samszar: znalazła perfekcyjny sposób na usunięcie Arianny - nie powiedziała jej o inspekcji i wpakowała ją w trening z marine. Potem pousuwała dowody. Ma sprawę osobistą, nie tylko z uwagi na Władawca. Ale - Arianna nie została usunięta a reaktywacja Semli sprawia, że możliwości zaszkodzenia Ariannie się Mai skończyły.
* Arnulf Perikas: wykazał inicjatywę jak Daria na kadłubie Królowej znalazła scrambler TAI i upewnił się, że to nie jest wyprodukowane na Królowej. Powiedział audytorowi w twarz, że Arianna jest ofiarą sabotażu. Stanął za Arianną ryzykując reputacją i wszystkim innym.
* Władawiec Diakon: wpuścił na Infernię inspektora Adama, ale chronił Ariannę (nie wiedział o inspekcji). Powiedział jej, że nie złamał porozumienia. Postara się, by wszystko co stało się na jednostce zostało na jednostce.
* Tomasz Dojnicz: marine; skłoniony przez Maję by powalczył z Arianną. Dobry w walce, ogólnie nieśmiały i tchórzliwy. Arianna zmotywowała go i ją pokonał; she tapped out. Serio szanuje i lubi Ariannę.
* Adam Chrząszczewicz: inspektor z Orbitera sprawdzający Królową; mimo katastrofalnego stanu Królowej i tego że wszystko poszło nie tak,chce pomóc Ariannie wyciągnąć jednostkę. Nie chce zniszczyć reputacji Arianny i tylko dał jej naganę. (ENCAO: --+00, trudny do zaskoczenia i polega na sobie, Power > Family, Preserver: "this planet is dying...").
* Hubert Kerwelenios: sarderycki advancer; nie ma uprawnień, ale to nieważne. Bardzo zależy mu by być lepszym. Nie opuszcza go dobry humor. Spacer kosmiczny z Darią i innymi advancerami by analizować poszycie Królowej. (ENCAO: +0-+-, Optymistyczny i wesoły, Achievement nie Humility, silnik: Wanderlust).
* Szczepan Myrczek: advancer; seiliowiec; tien; znalazł na poszyciu Królowej dziwne urządzenie (scrambler TAI) i zawołał Darię.
* Mariusz Bulterier: advancer; seiliowiec; tien; wysoki szacunek do Arianny, bo je konserwy i żyje spartańsko. Puszcza większość plotek :D.
* Leona Astrienko: nikt nie wiedział co z sarderytami zrobić, więc przejęła nad nimi dowodzenie i np. robili kosmiczne spacery i grali w piłkę w kosmosie.
* OO Królowa Kosmicznej Chwały: okazuje się, że było mnóstwo małych sabotowanych rzeczy na zewnątrz; m.in. scrambler TAI. Po usunięciu, TAI Semla zaczęła działać.

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański

## Czas

* Opóźnienie: 1
* Dni: 4

## Konflikty

* 1 - Daria i advancerzy chcą zrobić badania poszycia. Dokładne. Niech sprawdzą po raz pierwszy od CHOLERA WIE KIEDY.
    * ExZ (rywalizacja + chęć pokazania pani kapitan) +3+3 per kolor (3 niebieskie 3 zielone)
    * VgVz: seilici znaleźli coś DZIWNEGO. Daria rozpoznała scramblery TAI
* 2 - Daria i advancerzy rozmontowują rzeczy poukładane na Inferni
    * TrZ+4
    * V: udało się Darii poznajdować wszystko co tam jest (działa jak trzeci V).
* 3 - Daria i advancerzy zabrali się za usuwanie tego wszystkiego ze statku + jako ćwiczenia.
    * Ex Z (to jak Daria zna statek, ludzi itp i kontekst) +3
    * VzVzXX: statek czyszczony, ważne rzeczy usunięte, floaterzy, jeden odleciał daleko i pintką się uratowało.
* 4 - Arianna -> advancerów Seilii: "znaleźli, wykazali się jako ludzie". A do Huberta -> pracuj ciężko dogonisz ich.
    * Tr Z (pierwszy kapitan który coś robi i FAKTYCZNIE coś się dzieje) +3
    * VXX: advancerzy seilii wspierają Ariannę, ale jest potężna wojna i rywalizacja sarderyci - seilici
* 5 - Arianna vs marine Tomasz. Zapasy + broń biała. Tomasz podchodzi do sprawy bardzo profesjonalnie, jest dobry. Ale nie jest tak dobry jak Szymon.
    * Tr Z (Tomasz za bardzo próbuje nie uszkodzić Arianny) +3 +2Or (rana)
    * VzXVXrVzXXz: Tomasz podszedł za ostrożnie, Arianna go skopała, Tomasz użył siły i przyskrzynił Ariannę. Bo mu kazała być ostrzejszym XD.
* 6 - Daria uruchamia Semlę awaryjnie. Czas na testy i reanimację. Nadzieja na spowolnienie inspekcji by nie ucierpiała Arianna.
    * Ex Z (bo Królowa się sypie) +3
    * XXzX: Semla reanimowała się, reset fabryczny, autodiagnostyka (trzeba czekać), Maja skasowała dowody na swoje czyny + Arianna odcięta od mostka.
* 7 - Arianna przepełza po wentylacji do cywilizowanego miejsca. Z marine jako wsparciem. I żąda by Daria zapewniła kogoś kto ją wypuści.
    * Tr Z (znasz drogę) +4
    * XXX: Arianna ma elementy wyścigu psów, spowolnienie, idzie nieświeża na kontakt z inspektorem.
* 8 - Arianna konfrontuje Tomasza - skąd ten pomysł żeby robić ćwiczenia.
    * TpZ (bo ją szanuje i lubi) +3
    * VV: wrobiła go w to Maja
* 9 - 
    * 
    * 
* 10 -
    * 
    * 

---
layout: cybermagic-konspekt
title: "Protomag z Mrocznego Wołu"
threads: archiwum-o
gm: żółw
players: kić, anadia
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [221220 - Dezerter z Mrocznego Wołu](221220-dezerter-z-mrocznego-wolu)

### Chronologiczna

* [221220 - Dezerter z Mrocznego Wołu](221220-dezerter-z-mrocznego-wolu)

## Plan sesji

### Fiszki

#### 1. Mroczny Wół (18 osób) (Sludge removal barge - kosmiczna śmieciarka)
##### 1.1. Dowodzenie

* Tytus Muszczak: (faeril), kapitan
    * ENCAO:  0---+ |Egocentryczny;;Nadęty i napuszony;;Religijny| VALS: Conformity, Tradition >> Stimulation| DRIVE: This is our DESTINY
    * "napotkaliśmy na coś niebezpiecznego i Wincenty poświęcił się by wszystkich ratować"
* xo: N/A
* Maja Szewieczak: engineering, comms (atarienka); wyjątkowo kompetentna 
    * ENCAO:  -+000 |Nie lubi ryzyka;;Podejrzliwa| VALS: Power, Face >> Humility, Tradition| DRIVE: Wyrwać się z tej budy
    * "zrobię wszystko, jeśli mnie zabierzecie z tej jednostki; nie mogę skończyć na ŚMIECIARCE!"
* comms: też Maja ;-)
* Nela Kaltaner: science; śliczna i wycofana drakolitka
    * ENCAO: 0+-00 |Zwiewna i niestała;;Nikomu nie ufa| VALS: Self-direction, Conformity, | DRIVE: Ukryć sekret (GUARDIAN)
    * "odkryję to, o co chodzi"

##### 1.2. Operacja

* artylerzysta: N/A
* Wincenty Jarosz: (sybrianin), security, molestował Nelę i gdy chciał zrobić jej krzywdę. Nie znosił modyfikacji bioform. KIA. 
    * ENCAO:  00--+ |Samolubny, skupiony na sobie| VALS: Face, Hedonism, Family >> Tradition, Stimulation| DRIVE: Tainted Love
    * "CZEMU JESTEM TUTAJ?!"
* Krzysztof Workisz: pilot Mrocznego Wołu
    * ENCAO:  -0-0+ |Żyje chwilą;;Zachowuje swoją prywatność dla siebie| VALS: Achievement, Family >> Stimulation| DRIVE: Ochrona rodziny
    * "by sobie dorobić!"; "przemyt i próby"
* Juliusz Akramantanis: advancer, (dekadianin)
    * ENCAO:  --0-+ |Niepokojący;;Innowacyjny, szuka nowych rozwiązań| VALS: Benevolence, Family >> Tradition| DRIVE: Złota klatka Lauriego (możliwość hobby)
    * "leave me alone"; ma kolekcję krwawych kryształów z każdego zabitego przeciwnika (34)
* Nadja Kilmodrian: security_2, (sybrianka)
    * ENCAO:  -000+ |Mistrz improwizacji| VALS: Power, Hedonism >> Conformity| DRIVE: Wygrać w rywalizacji; jest najlepsza z security, odzyskać nadzieję

### Theme & Vision

* Horror-aspect: "sometimes the monster under the bed will want to save you"
* Shame: "some of us just want old wounds to be hidden"
* Sesja detektywistyczna

### Co się wydarzyło KIEDYŚ

* Nela Kaltaner (drakolitka) ma mroczny sekret. GUARDIAN zabił jej ojca, gdy ten próbował zrobić jej krzywdę. Wszyscy myśleli, że to ona.
* Maja Szewieczek (atarienka) została odesłana tutaj, by ją chronić. Jej rodzina współpracowała z Syndykatem Oriona a tu nikt nie myśli, że Mai coś może grozić.
* Wincenty Jarosz (sybrianin) odkrył sekret Marty, acz nie STRAŻNIKA. Zrobił z niej "swoją dziewczynę". Z Mają też sypiał, ale dla przyjemności.
* Tytus podglądał wszystkie dziewczyny; Wincenty nawet mu udostępniał Nelę.

### Co się wydarzyło TERAZ (what happened LATELY / NOW)

* Mroczny Wół transportuje duży obiekt (Spory, acz aktywny fragment jednostki noktiańskiej; komponent badawczy magii)

### Co się stanie (what will happen)

* S0: .

### Sukces graczy (when you win)

* .

## Sesja właściwa
### W poprzednim odcinku

* Nadja (security) 
    * nie jest fanką advancera Juliusza
    * robiła przez łóżko karierę ale jej nie wyszło; jest solidna ale mało bystra
* Nela (science)
    * chciała badać tajemniczy artefakt z Wołu ale jej kapitan zabronił
    * podobno była dziewczyną Wincentego, ale nie była jego fanką
    * drakolitka; śliczna, ale nielubiana
* Wincenty ("dezerter") 
    * nie przykładał się do roboty ale lubił kobiety i kusił do złego
    * wyłączał kamery z autoryzacji kapitana Tytusa; podrzucał mu też Nelę "do trójkąta"
* TAI Semla
    * system podtrzymywania życia ma zwłoki, przetworzone. 
        * Nie ma krwi. Krew została usunięta w większości.
        * Ciało było wystawione na energię Esuriit przez pewien czas.
        * Ciało było przemrożone w próżni ALE nigdy nie otwarto żadnych drzwi
        * --> jakiś efekt magiczny
* Advancer Juliusz
    * ma coś niewłaściwego, nazwany przez Wincentego "sick puppy"
    * Nadja TROSZKĘ się obawia advancera

### Scena Zero - impl

.

### Sesja Właściwa - impl

Czyli mamy do czynienia z energią Esuriit, potencjalnymi zwłokami w lifesupport oraz dziwną "bronią" advancera Juliusza ukrytą w wiadomym miejscu. Inspektorki nie rozmawiały jeszcze z: Nelą, Mają, pilotem Krzysztofem.

Mają kontakt z K1 (Baltazar) gdzie są wszystkie potrzebne dane o załodze, przeszłości itp. Mają uprawnienia i Semla chce współpracować.

Adela idzie przycisnąć Juliusza, Marianna skupi się na badaniu zwłok. W końcu zbadane dadzą więcej informacji. Nie mają nikogo z medycznego skrzydła, jest tylko Nela.

Adela przyskrzynia Juliusza. Jeśli można to nazwać przyskrzynieniem. Juliusz sobie coś je i patrzy na Adelę lekko posępnie. Nic nie mówi, je dalej. Traktuje inspektorkę jak powietrze.

* A: Jakie były Twoje stosunki z zaginionym?
* J: Nie lubiłem go.
* A: Dlaczego?
* J: Bo nie.
* A: Możesz rozwinąć myśl?
* J: (spojrzał na Adelę; ona na niego) Mogę.
* A: To rozwiń, proszę.

Juliusz nie jest najbardziej chętny do rozmowy z inspektorką Orbitera. Adela jednak się konfrontuje.

Tr +2:

* V: .
    * J: Bo nie chciał mnie zostawić w spokoju. Bo zamiast skupiać się na swojej robocie był bezwartościowy.
    * J: Nie podobało mi się jak traktuje Nelę.
    * A: Uważasz że on zawodowo też był kiepski?
    * J: Najpierw się starał. Potem mu się nie chciało. Wolał skupiać się na dziewczynach niż bezpieczeństwie. I jak skończył? Nikt nie chciał go ratować, gdy zniknął.
    * A: Kiedy przestał się starać? Coś się stało że przestał?
    * J: Gdy się dowiedział, że nie ma opcji przeniesienia z tej jednostki. Dowiedział się, że jest na nas skazany, na najniższy, najbardziej godny pogardy plebs (złośliwie). A potem przestał się starać o przeniesienie - gdy zauważył, że Nela... (chwilę myśli)... w sumie, nie wiem. (myśli) Był złośliwy wobec Neli a ona go ignorowała lub się odgryzała. A potem nie był już złośliwy a ona... (zwęziły mu się oczy)... zachowywała się jak jego niewolnica.
    * A: Czy Nela jest Ci bliska?
    * J: (Śmiech, lekko okrutny i lekko pogardliwy) Nie, po tym co robił z nią Wincenty nie dotknąłbym jej kijem.
        * Tu Adela wyczuła pewien ból.
    * A: A przedtem?
    * J: Mógłbym rozważyć. Jest najładniejsza na statku i nie jest tchórzem. Ale teraz... (prychnął). Co to jest jak nie tchórzostwo?
    * A: Co się stało, że Nela się zdecydowała oddać się Wincentemu?
    * J: Nie mam pojęcia. Zakład? Miała to w genach? Nie obchodzi mnie to. Jest słaba.
    * A: Słabych należy zrzucać z przepaści, rozumiem.
    * J: Słabych należy zrzucać na takie jednostki jak ta, niech wypełniają rolę jaką wyznaczył im szlachetny Orbiter.
    * A: Czemu tylko Ty go chciałeś uratować? Czemu inni nie chcieli?
    * J: (spojrzał z niedowierzaniem) Kapitan to kupa śluzu który nie wstaje z klęczek i podgląda dziewczyny; Nadja nie wpadłaby na to, że zniknął i chyba dalej nie zauważyła; Neli się nie dziwię; pilot najpewniej patrzył na zdjęcia rodziny przez ostatnie 48h; Maja... (tu się zawahał) nie wiem. Nie wiem, czemu ona nie próbowała.
    * A: A czemu Ty próbowałeś skoro go nie lubisz?
    * J: Nie zostawia się swoich. (z pełną szczerością)
    * A: Czemu się nie bałeś wejść na noktiański derelict skoro magia itp?
    * J: Co najgorszego mnie spotka? Zginę. Jak advancer. **Nic** na mnie nie czeka. Robię dobrą robotę na chwałę Orbitera (jadowicie)
* V: .
    * A: Te ładne czerwone kryształki, co to jest?
    * J: (z uśmiechem) Krew moich wrogów.
    * A: A jak ją tak spreparowałeś?
    * J: (z szerokim uśmiechem) Bałem się, że NIGDY nie zapytasz
    * J: (zaprowadził Adelę do jednej z wnęk i wyjął to urządzenie, które odkrył Wincenty) To jest desanguinizator. Aplikujesz do ciała i wysysa całość krwi zmieniając ją w wieczny kryształ. Jedno ciało to jeden taki kryształek. (Adela naliczyła 33). Kolekcjonuję je. (czeka z obleśnym uśmiechem na następne pytanie)
    * A: Czy któryś z tych kryształków to Wincent?
    * J: Może... może nie. Ty jesteś inspektorką. Ty mi powiedz.
* X: Juliusz jest SKRAJNIE opiekuńczy wobec swoich kryształków
    * J: Jeśli byś je wzięła. Dotknęła. Jestem zmuszony zrobić Ci krzywdę. Taka religia. Nie rób tego.
* V: .
    * A: Muszę je zbadać.
    * J: Nie.
    * A: Żądam możliwości ich zbadania.
    * J: JA mogę je trzymać i Nela może je dla Ciebie badać. Ma różne fetysze. (po chwili spojrzał na Adelę) Tylko nie jęcz w ekstazie gdy Nela będzie je badać.
    * A: Czy można z martwego wyciągnąć krew?
    * J: Tak, oczywiście. Zawsze z martwego. Byle był świeży. Żywego... za bardzo by bolało, umarłby z szoku.

Adela podziękowała Juliuszowi. Bardzo mało sympatyczny koleś.

I Adela zrobiła skan dokumentów Juliusza. Co to za koleś.

Tr Z (uprawnienia + info Orbitera) +3:

* X: dane Juliusza są pomaskowane i redacted.
* V: Juliusz ma dwie fazy: więzienie oraz jakaś kiepska jednostka. Widać wyraźnie, że koleś SAM nie sprawia kłopotów, ale nigdy nie boi się odpowiedzieć kłopotami na kłopot. Jest lojalny wobec swoich jednostek, ale mówi co myśli. Nie jest lubiany i nie chce być lubiany. Jest noktianinem i jest z tego dumny. Nie pije. Za to głośno gardzi Orbiterem i jak ktoś spróbuje do niego z pięściami to z przyjemnością odpowie.
* X: Na danych Juliusza znajdował się _trigger_. Z Adelą skontaktuje się odpowiednia osoba.
* V: Juliusz pochodzi z Krwawej Areny. Gladiator i advancer. Niebezpieczny. BARDZO niebezpieczny. Świetny wojownik i to lubi. Nigdy nie był niewolnikiem, wyjątkowo nienawidzi niewolnictwa. Nienawidzi słabości. Kiedyś zwolennik kapitan Apalii Certamen, która chciała "wojna wypali słabość i zmieni nas w to, co jest prawdziwie zwycięskie - tylko przez wypalenie wszystkiego staniemy się godni walczyć z Saitaerem i Orbiterem." Wszystkie "worthy kills" (ci, którzy POWINNI byli zginąć z jego ręki albo ci, których zabił osobiście) trafiają do jego naszyjnika. Po jego śmierci jego ciało też ma przejść do naszyjnika.
    * Jeśli zabiłby Wincentego przez przypadek, nie powinien wysysać jego krwi. Bo to nie była wartościowa śmierć.
    * Jeśli zabiłby Wincetego z jakiegokolwiek powodu ale świadomie, jego krew powinna być w naszyjniku. Inaczej się nie godzi.

Adela ma wiadomość prywatną od agentki Orbitera imieniem Lena, z K1.

* L: Czy możesz chwilę porozmawiać?
* A: Słucham.
* L: Juliusz Akramantanis. Co _teraz_ zbroił?
* A: Na razie jeszcze nic. Nie mamy dowodów by coś zbroił.
* L: Nie daj się zwieść jego czarowi i charyzmie. (mówimy o tym samym Juliuszu???) Juliusz ma wiele osób na rękach, to jego ostatnia szansa albo trafia do więzienia. Co teraz zbroił?
* A: Naprawdę na razie nic nie ma udowodnionego, ale... jaki czar i charyzma?
* L: (ignorując pytanie Adeli) Słuchaj, jestem nim zmęczona. Czy jest coś, cokolwiek, co da się do niego przyczepić?
* A: Czy ma się coś znaleźć na niego?
* L: Byłabym wdzięczna. Stanowi same problemy i obniża nasz _success rate_ jak chodzi o wprowadzanie noktian do pożytecznych prac.
* L: On próbuje każdego psychologa zniesmaczyć. Jak ja z nim rozmawiam to opowiada jak chce lizać krew. Albo opowiada niestworzone historie i ja muszę to potem czytać. Np. dziesiątki zwycięzców areny i gdzie się znajdują a ja mam obowiązek to zaraportować i biedne TAI muszą potem sprawdzać czy on coś wie itp. Nie jest GROŹNY ale jest PITA. Zamiast się zintegrować. Wojna. Się. Skończyła. I jest na tyle inteligentny, że nie mam nic na niego bezpośrednio.
* L: Mogę na Ciebie liczyć?
* A: Zastanowię się. Będziemy w kontakcie.

Tymczasem, Marianna. Na podstawie informacji Adeli wzięła do badania desanguinizator i puściła ją w laboratorium Neli. Bez Neli, więc będzie trudniej - ale na pewno będzie prawda.

Tr (niepełna) Z (sprzęt) +2 +3Or (uszkodzenie):

* Vz: Marianna ma wprawę w analizie dowodów. Okazało się, że desanguinizator nie służył do ekstrakcji krwi z Wincentego.

To znaczy, że najpewniej Juliusz nie stał za zabójstwem / zniknięciem Wincentego. 

Wiemy, że winni nie są:

* Tytus (bo kapitan nie jest tego typu osobą, nie ogarniał), Juliusz, Nadja

Zostają jedynie:

* Maja (z inżynierii) - ale po co?, pilot Krzysztof, Nela.

Z tej grupy wiemy, że Wincenty dobierał się do Neli. Ale jak ładna drakolitka miałaby go zabić i to jeszcze używając aspektu magii?

Inspektorki uznały, że Wół ma raczej poślednią załogę. Nic co Orbiter bałby się stracić. To czemu na Wole znajdują się dwie kompetentne dziewczyny - Maja oraz Nela? Co one zrobiły?

Marianna idzie porozmawiać z pilotem Krzysztofem.

* M: (szuka objawów praworządnego człowieka który boi się podpaść) (jest BARDZO zestresowany)
* M: proszę mi opowiedzieć o załodze z pana punktu widzenia.
* K: och, kapitan... kapitan jest pracowity. Maja jest genialna. Naprawdę. I sympatyczna. Nela... (lekko się spłonił) bardzo elegancka i miła. Juliusz... on jest _dziwny_ (dyskretnie), ale ogólnie można mu ufać. Choć udaje, że nie. Nadja jest spoko. Nie jest tak dobra jak Wincenty, ale ma entuzjazm.
* M: zaufasz jej swoim życiem?
* K: tak, tak, oczywiście że tak. (nieszczerość) Cieszę się, że będzie szefem ochrony jeśli Wincent się nie pojawi (tu mówi szczerze)
* M: Dlaczego mam uwierzyć że nie zabiłeś Wincentego?
* K: (autentyczny, histeryczny śmiech) Myślisz że ja... ja bym... ja umiałbym... ja jego... Ja jestem słaby i niegroźny! Ja bym nie umiał nawet go obrazić za jego plecami, to był prawdziwy mężczyzna. Taki... męski.
* M: Czemu był?
* K: To chyba oczywiste, zabiła go magia (zdziwienie)
* M: Magia powiadasz? A jaka magia go zabiła?
* K: Magiczna. Nie wiem. Nie jestem magiem. Ale wszystkie dowody pokazują na magię.
* M: Czyli które?
* K: Był i zniknął. A teraz śmierdzi. I Wy tu jesteście. Więc musi być martwy. (prostolinijne zdziwienie)

Marianna _gardzi_ słabymi facetami. Patrzy na niego zimnym rybim okiem i pyta "ciekawe co powiedziałaby Twoja żona o Twoich łóżkowych przygodach"

Tp Z (uprawnienia) +2:

* X: On się boi, płacze, trzęsie i będzie opinia że Marianna KOCIŁA pilota za niewinność.
* V: .
    * K: NIEEE! Nie! Skąd wiesz! To niemożliwe! NIE MÓW MOJEJ ŻONIE BŁAGAM! Jestem słaby a... takie rzeczy mi się nie zdarzają!
    * M: To znaczy?
    * K: WPIERW NELA DO MNIE PRZYSZŁA! Była taka niesamowita... a potem Nadja. (rozmarzenie, strach, szok). Ja jestem... ja nie jestem nikim ważnym, tak? Nikim. Jedna taka śliczna a druga zdecydowana. I co ja mogę zrobić?
    * M: Powiedzieć "nie"?
    * K: Widziałaś mięśnie Nadji?! (szczere słowa) Jak coś chce, dostaje...
    * K: PRZYZNAM SIĘ DO PRZEMYTU! ALE NIE MÓW MOJEJ ŻONIE!
    * M: A naprawdę przemycasz czy się przyznasz?
    * K: (padła litania o tym, jak czasem rzeczy które były przesuwane to prosił Juliusza czy on nie może przenieść jakiejś sprzedawalnej elektroniki i on ją potem sprzedawał; Juliusz nigdy nie chciał premii. I tak, Krzysztof wszystko wysyła do domu, żeby żona i dzieci mieli się dobrze)
    * M: przynosisz hańbę Orbiterowi... jeśli będziesz rozpowiadał o czym rozmawialiśmy, Twoja żona dostanie ładny liścik
    * K: NIC NIE POWIEM! PRZYSIĘGAM! NIKOMU!
    * M: się okaże jak Nadja do Ciebie przyjdzie
    * K: (whimper)

Nadja to ma kochany charakter... znęca się nad Krzysztofem. Bo ten koktajl wstydu, strachu i pożądania jej smakuje jeszcze bardziej.

Adela szpera w dokumentach na temat Mai. Dlaczego Maja tu jest - co ma w papierach.

Tr Z (uprawnienia + info Orbitera) +3:

* X: Dane utajnione.
* V: Jawne dane wyciągnięte.
    * Maja ma dobre rekordy. Nie ma wrogów. Jest sensowną, skuteczną inżynier. Pracowita, uprzejma.
    * Maja wielokrotnie prosiła o przeniesienie. Nigdy się nie udało. 
    * Maja nie ma pojęcia czemu została tu przeniesiona i czemu jej kariera umiera.
    * Maja jest NIESAMOWICIE sfrustrowana; nie ze swojej winy i nie wie co się dzieje.
        * "mam dopiero 23 lata! Dlaczego! Zrobię cokolwiek!"
* X: Koleżanka Lena będzie potrzebna, by się przebić. Adela wie jedynie, że to nie Maja. To coś z jej rodzicami.

Adela chwilowo odpuściła; to są tematy dużo powyżej jej poziomu uprawnień. Coś w stylu programu ochrony świadków? 

Adela i Marianna spotkały się ponownie. Zostaje im tylko Nela. Ale czemu tu jest?

Tr Z (uprawnienia + info Orbitera) +3:

* XX: Wiecie, że Wincenty miał dostęp do tych papierów i dokumentów i przebił się przez (redacted). Miał czyjąś pomoc bo sam nie miał uprawnień. Ale pewne dane o Neli zostały wymazane / schowane.
* V: .
    * Dane dotyczyły okoliczności śmierci OJCA Neli. I dane zostały schowane przez Lenę.
    * Nela **sama** chciała trafić na tą jednostkę. Ona sama aplikowała i się tu dostała.

Wincenty nie miałby dostępu do tego typu dokumentów. Więc skąd wiedział? I jak napotkał na Lenę? Więc coraz więcej wskazuje, że Lena nie jest "obojętnym świadkiem" tego wydarzenia.

Podsumujmy:

* Nela jest desygnowaną ofiarą lub potencjalnym mordercą. Na pewno Nela była podmiotem lub przedmiotem.
* Lena jest albo mastermindem albo katalizatorem. Ale jest w to zamieszana.
* Wincenty był osobą która znęcała się nad Nelą.
* I chyba chodzi o usunięcie Juliusza.
* I nikt inny nie jest z tym powiązany, acz każdy ma swoje na sumieniu.

W świetle tych rzeczy Mariannę uderzyła jeszcze jedna rzecz - Wół ma dużo lepszy science lab niż powinien. I wyraźnie Nela w ten science lab reinwestuje.

Marianna przed rozmową wraca do science lab i szuka zapisów eksperymentów. Nie musi rozumieć, szuka dziwnych, nietypowych eksperymentów. Eksperymentów odstających od normy - wspomagając się Semlą.

Tr Z (Semla) + 3 + 2Or (niszczenie sprzętu):

* X: dane są pousuwane. Nela po każdym eksperymencie dokładnie usuwała wszystkie dane. Tych eksperymentów jest dużo. Od lat. Tylko gdy Wół jest "spokojny" i nic nie jest potrzebne tylko wtedy Nela robi eksperymenty.
* V: natura tych eksperymentów nie dotyczy żadnych zasobów na Wole. To musi być coś co Nela ma lub ona sama.
* XX: niestety, Nela mistrzowsko usuwa to wszystko. Doskonale to przygotowała. ALE! odkąd została zdominowana przestała eksperymentować.
* Vz: natura jej eksperymentów dotyczyła czegoś związanego z magią i inhibicją.

Robocza teoria Marianny - na ile Lena (agentka) próbuje zamaskować badania Neli. Nela ma bardzo zamaskowaną historię i trudno wydobyć. Czemu poślednia Nela ma być tak kryta? Trochę wygląda podejrzanie.

Adela do Roberta Warłomkacza. Który chce się z nią przespać i jest komodorem i który jej wisi.

* R: Kwiatuszku, czemu chcesz żebym działał przeciwko inspektoratowi Orbitera?
* A: Dlaczego przeciwko?
* R: Nie zrozumiałem dobrze? Chciałaś, bym sabotował ruchy agentki inspektoratu?
* A: W jak duże bagno się pakujemy jeśli robi coś nielegalnego?
* R: Jak mam to zrobić? Nie wiem, spytać jej przełożonych? Wy znacie inspektorat, ja nie.
* A: Dane o Neli i magii. A jak Cię odepchną, będzie informacja i dowiemy się kto Nelę kryję - Lena czy ktoś jeszcze.

Tr Z (serio chce się przespać) +2:

* V: Robert się tym zajmie.
* X: Robert potrzebuje motywatora. "Kawa w lodziarni, Adela będzie jadła banana"
* V: Robert się przejmie i zrobi to szybko.

3 h później, Robert przekazał Adeli informacje.

* Nela nie ma ochrony ze strony inspektoratu ani Orbitera. Nikt się nią nie interesuje.
* Lena się nią zainteresowała i poszukała a potem utajniła część danych. Ale Lena zrobiła to dość niedawno (po tym jak Wincent trafił na Woła)
* Lena znalazła dane i dodała do rekordu Neli o śmierci ojca Neli.
    * Ojciec Neli był okrutny wobec córki.
    * Ojciec Neli zginął w tajemniczych okolicznościach, pod wpływem magii. Wykryto energię Esuriit.
    * Policja uznała, że to nie mogła być Nela. Bo była za mała, nie przeniosłaby go. Miała 10 lat.
    * Badania nie wskazały obecności magii i visiat u Neli ponad normę.
    * Nela całe życie poświęciła badaniom efemeryd i echa magicznego.
    * Nela nigdy nie chciała być z nikim w pokoju w nocy.

Adela i Marianna zdecydowały się porozmawiać z Nelą. Drakolitka jest śliczna i elegancka. Ma taką aurę godności dookoła siebie. Dobrze się nosi itp. Na wypadek nieprzewidzianych rzeczy i problemów Marianna zrobiła raport i zrobiła go jako _death's hand_.

* N: W czym mogę pomóc?
* M: (grzecznie) Jaka jest natura Twoich eksperymentów?
* N: Prywatne, mam prawo do prywatności.
* M: Nie w wypadku śmierci na pokładzie.
* N: Tak, chyba, że będzie udowodnione że natura eksperymentów korelowała z tą śmiercią.
* M: I dlatego o nią pytam.
* N: To oznacza, że masz dowód.
* M: Mam prawo zadawać pytania i oczekiwać na nie odpowiedzi.
* N: Nie odpowiem na to pytanie. Jest to jedno z nielicznych pytań na które nie odpowiem. (lekko zestresowana)
* M: Nie lubiłaś Wincentego, prawda?
* N: Dlaczego pytasz mnie o coś, co... (myśli) dotyczy czegoś co myślę. To nie jest fakt.
* M: Odpowiedz na pytanie.

.

Tr Z (uprawnienia i logika) +2 +5Og (stres):

* Xz: Nela skutecznie przygotowała się wcześniej, DAWNO TEMU we wszystkich porządkach. I jej się pomyliło. To znaczy, że czasem myśli że prawo działa inaczej niż działa.
* V: Będzie współpracować, acz niechętnie
    * N: Nie lubiłam Wincentego. Był złym człowiekiem.
    * M: Na czym to jego zło polegało?
    * N: Czemu tu jesteście? (ratując resztki godności)
    * M: Żeby zadawać pytania.
    * N: Czy możesz przejść do meritum, agentko? (ratując resztki godności)
    * M: Jestem przy meritum, odpowiedz na pytanie.
    * N: (wyraźnie skonfliktowana) Jego zło polegało na tym, że brał wszystko co chciał i nie dbał o innych. I to go zgubiło.
    * M: Co masz na myśli?
    * N: Czemu tu jesteście?
    * M: Zaginął oficer Orbitera. Naszym celem jest odnaleźć jego lub jego zwłoki i dowiedzieć się kto za tym stoi.
    * N: (myśli długo)
    * M: Co masz na myśli 'co go zgubiło'?
    * N: Zniknął. Najpewniej nie żyje.
    * M: Skąd to przypuszczenie?
    * N: (po twarzy drakolitki przeszły różne uczucia) Bo go tu nie ma. Gdyby był, pojawiłby się, prawda?
    * M: Być może
    * N: Czy mnie oskarżacie?
    * A: A mamy o co?
    * N: (złość, smutek) Czy się ze mną bawicie?
    * A: Nasza praca polega na zadawaniu pytań.
    * N: (cicho) Jeśli... (pokręciła głową)

Pukanie do drzwi. Bardziej natarczywe. Po chwili słyszycie stłumione "cholera z tym" i drzwi wylatują. Przychodzi zrezygnowany Juliusz. Widać po minie.

* J: Puśćcie ją. Chcecie noktianina to macie noktianina. Lepszy dla Waszej kariery niż jakaś drakolitka.
* N: (szerokie oczy, ona nie udaje, ona serio nie rozumie co się dzieje)
* A: mieliśmy odkryć prawdę a nie zbierać punkty
* J: (zaśmiał się Adeli w twarz) Prawda taka, że Wincenty nie żyje. Wyssałem go desanguinizatorem. Done.
* N: Ale... (chce coś powiedzieć)
* J: Zamknij mordę, drakolitko. Jeśli już masz być bezwartościowa, bądź bezwartościowa aż mnie aresztują.
* N: (jej stres wzrósł i w oczach pojawiły się łzy)
* J: To co robimy? Standardzik? Dokumenty, podpisuję, więzienie itp? (taka totalna rezygnacja i nonszalancja)
* M: Mimo tego co niektórzy chcieliby uważać _patrząc na Juliusza_ i mimo tego co byś chciał, Ty Wincentemu nic nie zrobiłeś. Powiedziałaś, że brał co chciał - Ciebie również _patrząc na Nelę_. _patrząc na Juliusza_ aż dziwne, że czegoś z tym nie zrobiłeś wcześniej. Nie znam człowieka, tak, uważam, że nie żyje. Ale musiał Ci naprawdę zaleźć za skórę _do Neli_. Na tyle, byś przestała próbować wytłumić swoją magię. _tu Juliusz aż podskoczył, przez MOMENT widać postawę wojownika_ _Nela odwróciła wzrok od niego_. Nie chciałaś mu tak naprawdę nic zrobić, nie świadomie. Ale w którymś momencie czara się przelała. I po prostu straciłaś kontrolę a ponieważ nie brałaś wytłumiaczy stało się to co się stało. Nie wiem dokładnie co się z nim stało, nie wiem czy się będziemy w stanie dowiedzieć.
* N: Nie wiem, co się z nim stało. Nigdy nie wiem. (chwila pauzy) Jak byłam mała, bałam się potworów pod łóżkiem. Ale okazało się, że te potwory mnie chronią. To inni powinni się ich bać.
* J: _pokręcił głową z pogardą_ żałuję, że tu kiedykolwiek przyszedłem. _I wyszedł_.
* N: _opuściła głowę, wyciągnęła ręce do kajdanek_ nie będę stawiała oporu. Nigdy nie stawiałam.
* N: (cicho) nie mam pojęcia, czemu tu przyszedł. On mnie nawet nie lubi...

Nela została zabrana na Orbiter - bo jak magia eksploduje, to będzie katastrofa. I zamiast do więzienia to do instytutu - by kontrolowała swoją energię magiczną. Bo inaczej będzie katastrofa.

A Juliusz? Juliusz zostanie na Wole, jeszcze bardziej cyniczny. Dla niego nic się nie zmieniło. Lena nie dostała tego co chciała.

Lena próbowała dorwać Juliusza i dlatego współpracowała z Wincentym. Lena dała Wincentowi Nelę by ALBO sprowokować Juliusza do ataku albo by Wincenty znalazł coś na Juliusza. Lena cynicznie zaakceptowała ryzyko, że może Nela zabić Wincentego - ale nie wiedziała o magii. Myślała, że Nela zrobi to "normalnie" i uda się to zrzucić na Juliusza.

Ale plan się nie udał. Lena dostała REPRYMENDĘ i przenieśli ją gdzieś gdzie Juliusz nie jest pod jej pieczą.

## Streszczenie

Badania załogi Mrocznego Wołu trwają dalej - kto mógł być mordercą? Ślady wskazują na Juliusza, ale on nie ma motywu mimo że ma wszystkie sposoby. Okazuje się, że to nie przez przypadek - agentka Orbitera Lena ma rehabilitować noktian a Juliusz jest niepokorny i stawia opór i niszczy statystyki. Lena doszła, że piękna Nela może mieć ciekawą przeszłość i skłoniła Wincentego, by się nią zajął z nadzieją, że Juliusz spróbuje Wincentego zabić. Ale nie wyszło...

## Progresja

* .

### Frakcji

* .

## Zasługi

* Adela Myrias: przesłuchując Juliusza natrafiła na Lenę - agentkę Orbitera i dowiaduje się, że ta chce pozbyć się Juliusza. Potem dochodzi, że rodzice Mai mają kłopoty z Syndykatem Aureliona. Doszła do tego, że za morderstwem stoi Lena - inna agentka Orbitera.
* Marianna Atrain: bada desanguanizator i widzi, że to nie Juliusz stoi za zabójstwem Wincentego. Po rozmowie z pilotem doszła, że science lab jest za dobry na Wole - tak, Nela coś robi dookoła magii i inhibicji. Doszła do tego, że Nela jest protomagiem.
* Juliusz Akramantanis: ignoruje Adelę, nie chce współpracować z agentką Orbitera. Wyraźnie lubi Nelę po swojemu. Gardzi załogą Mrocznego Wołu. Mało sympatyczny, ale po swojemu lojalny choć robi kłopoty. Lena chce się go pozbyć lub zrobić mu krzywdę. Przyznał się do zabicia Wincentego by ratować Nelę, ale Zespół nie zgodził się na to - znają prawdę. Stawia opór w każdy możliwy sposób i niszczy statystyki.
* Nela Kaltaner: śliczna drakolitka szukająca rozwiązania swojej natury protomaga i Tego Co Ją Chroni. Została zabrana na Orbiter - bo jak magia eksploduje, to będzie katastrofa. I zamiast do więzienia to do instytutu - by kontrolowała swoją energię magiczną.
* Lena Ifirentis: agentka Orbitera która zajmuje się wprowadzaniem noktian do pożytecznych prac a Juliusz obniża jej success rate. Prosi Adelę, by ta znalazła coś za co można go przyskrzynić. To ona stoi za tym, by Wincenty tępił Nelę by Juliusz wybuchł; nie spodziewała się protomaga na pokładzie. Po wszystkim dostała reprymendę i ją przenieśli i Juliusz nie jest już pod nią.
* Krzysztof Workisz: pilot Mrocznego Wołu; słaby facet, ale żonaty. Ma romans z Nelą (z woli Wincentego) i z Nadją (z woli Nadji XD). Niskiej klasy przemytnik. Całkowicie niegroźny. Kocha rodzinę, ale jest słaby.
* Nadja Kilmodrian: wyraźnie znęca się nad Krzysztofem. Bo ten koktajl wstydu, strachu i pożądania jej smakuje jeszcze bardziej.
* Maja Szewieczak: jej rodzice mają kłopoty z Syndykatem Aureliona. Jest kompetentna i jest tu by ją chronić (XD). Nawet, jeśli ona o tym nie wie i nie chce.
* Robert Warłomkacz: komodor który chce się przespać z Adelą; dowiedział się dla Adeli, że nikt o Neli i jej działaniach nie wie ale Lena się interesowała jej historią i udostępniła wiedzę Lenie.
* OO Mroczny Wół: musi wymienić część załogi po śledztwie: Nela i Wincenty zostaną wymienieni.

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria, Pierścień Zewnętrzny

## Czas

* Opóźnienie: 1
* Dni: 3


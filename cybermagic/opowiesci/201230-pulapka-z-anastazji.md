---
layout: cybermagic-konspekt
title: "Pułapka z Anastazji"
threads: legenda-arianny
gm: żółw
players: kić, fox, kapsel
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [201111 - Mychainee na Netrahinie](201111-mychainee-na-netrahinie)

### Chronologiczna

* [201111 - Mychainee na Netrahinie](201111-mychainee-na-netrahinie)

## Punkt zerowy

### Dark Past

.

### Opis sytuacji

.

### Postacie

.

## Punkt zero

.

## Misja właściwa

Damian Orion skontaktował się z Arianną Verlen, ku jej wielkiemu zaskoczeniu. Okazuje się, że statek kupiecki "Rodivas" odnalazł Anastazję. Chcą od Aurum okupu za "panienkę Anastazję", ale z radością ją dostarczą niezależnie od okoliczności. Aurum wysłało po Anastazję "Zgubę Tytanów". Czemu Orion to Ariannie mówi? Nie podoba mu się to, że politycznie wszystko spada na Ariannę a jest niemałe ryzyko, że to będzie użyte przeciwko niej. W końcu to ona straciła Anastazję.

Chwila - Arianna **wie**, że Anastazja skończyła na Nocnej Krypcie. To nie może być Anastazja. Po prostu nie może. 

Uwaga Arianny skupiła się na Rodivasie - co to za statek. Zapytała Klaudię, która zaczęła korelować dane z wielu źródeł (Ex):

* V: zacznijmy od tego, że sam sygnał wysłany przez Rodivas ma własności anomaliczne. Rodivas nie jest czystym SCA Rodivas (statek kosmiczny); Klaudia klasyfikuje go jako AK Rodivas. Oops.
* V: ślady sensorów Rodivasa wskazują na to, że Rodivas zintegrował się z innym statkiem. Czyli mamy DWA zintegrowane statki które dają sygnał Rodivasa. Ten drugi statek zniknął pewien czas temu, pożarty przez AK Wyjec. Innymi słowy, Krypta zaczęła wykorzystywać moce stacji naprawczo-leczniczej która zmieniła się w Wyjec. To niefortunne.

Czyli - mamy Rodivas, który jest **prawdziwym** statkiem i z nim zintegrowała się anomalia. Nie wiemy kogo da się ratować czy jak. Ale wiemy, że Zguba Tytanów zupełnie nie jest w stanie poradzić sobie z tego typu problemem... jest ryzyko, że AK Nocna Krypta przejmie kontrolę nad OA Zgubą Tytanów. A to byłaby katastrofa.

Mając te informacje Arianna awaryjnie uruchamia załogę Inferni i ze wspomaganiem magicznym lecą by przegonić Zgubę. Elena za sterami, Arianna na wspomaganiu, Eustachy przesterowuje mechanizmy. Niebezpieczne, ale MUSZĄ zdążyć. (O: Eustachy czuje jeszcze większą miętkę do Eleny).

Gdy docierają w okolice Rodivasa, Klaudia zaczyna używać głębokiego skanu, wspomaganego anomalicznie. Teraz może sama określić co jest tam bardzo nie w porządku. Ze specjalnymi sensorami Inferni i swoim naturalnym intelektem.

* V: Rodivas jest "zdrowy". To aktywny statek kosmiczny. Ale są sektory Rodivasa, które są anomalizowane - to ładownia (z biovatem z Anastazją) i część kwater
* XX: Na pokładzie Rodivasa jest sześciu Sowińskich, ze statku kurierskiego. Są przekonani, że to Anastazja; to oni wezwali jako wsparcie Zgubę Tytanów
* V: Wszystko - ABSOLUTNIE WSZYSTKO - wskazuje na to, że formą anomaliczną na pokładzie Rodivasa są Anioły Krypty. Innymi słowy, jest to pułapka zastawiona przez Kryptę.

Ale na czym polega ta pułapka? I jaki zysk może powstać z zaryzykowania najcenniejszym jeńcem - Anastazją? Krypta nigdy nie działała w ten sposób... chyba, że integracja z Wyjcem i Donaldem (kapitanem Pocałunku Aspirii) zrobiła te wszystkie zmiany...

Eustachy i Klaudia mieli prostą odpowiedź do Arianny odnośnie tego, czego Krypta chce - ARIANNĘ. Innymi słowy, cokolwiek się nie stanie, Arianna nie może stanąć na pokładzie Rodivasa. Niezależnie od czegokolwiek. Ariannie się to niezbyt podoba, lecz akceptuje to rozwiązanie - inaczej Krypta dostanie to, czego chce.

Nowy plan - uratować wszystkich, a co najmniej Sowińskich. Nie dopuścić do tego, by ktoś został zainfekowany przez AK Rodivas. Eustachy zaproponował odstrzelenie biovatu z Anastazją. O dziwo, wszyscy się na to zgodzili - może być to najlepsza opcja. Ale kluczem jest przekonanie Zguby Tytanów.

Tak więc Arianna porozmawiała z Henrykiem Sowińskim, dowódcą Zguby. Wyjaśniła mu, że niestety Rodivas jest AK - jest pułapką. Nadal są w stanie uratować Anastazję: wpierw fala EMP w Rodivas, potem odstrzelenie i wycięcie biovatu, następnie neurotoksyna na pokład Rodivasa, szturm i ewakuacja wszystkich. Arianna z przyjemnością odda chwałę i honory Zgubie Tytanów, nie chce tylko by im się stało coś bardzo złego. Henryk się zgodził - nie jest to bardzo zła opcja.

Dobrze. Zguba się zbliża do Rodivasa i Infernia atakuje Rodivasa. Fala EMP a potem Eustachy próbuje odstrzelić biovat (XXXV). Niestety, biovat został uszkodzony a Rodivas został solidnie zdewastowany. Nieczysty strzał. Eustachy chciał zaimponować Dianie i Elenie i po prostu... nie wyszło. Poważne uszkodzenie Rodivasa, wymagające interwencji. No i uszkodzony biovat w kosmosie.

Arianna używając swojej magii natychmiast przesterowuje mechanizmy Inferni i Eustachy rozpaczliwie próbuje przechwycić biovat. Udało się (XVXX). Samuraj się pojawił i zaczął pieprzyć czułości Elenie i Dianie w imieniu Eustachego, dodając do kakofonii. Udało się zachować containment i wsadzić Anastazję w biovacie do odpowiedniego miejsca. Arianna, zajęta całym bajzlem kazała szybko Martynowi sprawdzić Anastazję i skomunikować się ze Zgubą.

Martyn zrobił dokładne badania Anastazji - wyszło mu, że to _jest_ Anastazja i dokładnie to przekazał Zgubie. Zguba tymczasem zajęła się Rodivasem. Oddziały Zguby wpadły na Rodivas, w pełnych servarach szturmowych (m.in. Entropik, nie tylko Lancer) i uratowali Sowińskich + przejęli Rodivas.

(tu wchodzą ostatnie XX). Arianna - arcymag - widzi, że to _nie_ jest Anastazja. Stworzony konstrukt przez Kryptę. Innymi słowy, Krypta chce wprowadzić "Anastazję" do rodu Sowińskich i przejąć nad nim kontrolę lub zintegrować się z ich sentisiecią. To by był koszmar. Arianna _nie może_ oddać Anastazji Sowińskim!!!

Arianna powiedziała Klaudii, że nie można oddać Anastazji Sowińskim. Po prostu nie. Nie będzie wyjaśniać. Klaudia ma jej pomóc przekonać Sowińskiego. Klaudia puściła 100% spin, Arianna skupiła się na umiejętnościach przekonywania. Że niby Anastazja jest znowu sprzężona i muszą to jakoś rozwiązać.

* X: Henryk Sowiński nie da NIC z Rodivasa. Nic. Anastazja MUSI wrócić najpierw.
* X: Na pokład Inferni trafią pretorianie Anastazji, którzy mają jej pilnować.
* X: Arianna odpowiada osobiście za Anastazję i jej stan.
* V: Arianna ma swobodę oddania Anastazji gdy skończy.

Arianna nie jest szczęśliwa; jakoś musi to rozwiązać... a jej własna załoga nie wie, że to nie Anastazja a jakaś chora anomalia Krypty...

## Streszczenie

Krypta "oddała Anastazję", zmieniając cywilny statek Rodivas w pułapkę na Ariannę.

## Progresja

* Eustachy Korkoran: pod wrażeniem umiejętności technicznych Eleny - jej pilotażu, jej ognistego profesjonalizmu, jej skłonności do ryzyka.
* Henryk Sowiński: sława i chwała za zdobycie Rodivasa i "odzyskanie Anastazji", którą "straciła Arianna". Łaska ze strony Sowińskich.

### Frakcji

* .

## Zasługi

* Arianna Verlen: jako arcymag, wyczuła, że Anastazja nie jest "prawdziwa" a jest konstruktem Krypty. Magicznie wzmocniła Infernię, by Elena jej nie zepsuła. Potem kupiła czas zajęcia się Anastazją.
* Klaudia Stryk: wykryła, że Rodivas nie jest już SCA a AK. Określiła gdzie Eustachy ma strzelać by odstrzelić biovat. Dodatkowo - pomagała Ariannie bullshitować Henryka Sowińskiego. 
* Eustachy Korkoran: przesterował systemy Inferni, by zdążyć uratować Zgubę Tytanów przed AK Rodivas. Potem próbował "uratować Anastazję", ale zrobił za duże zniszczenia bo się popisywał.
* Elena Verlen: alias Mirokin; pilotowała Infernię najszybciej, jak tylko potrafiła. Udało jej się nie uszkodzić ani nie zniszczyć tego pięknego statku.
* Martyn Hiwasser: błędnie uznał "Anastazję Dwa" za prawdziwą Anastazję, jedynie dodając Ariannie kłopotów i zgryzot.
* Damian Orion: ostrzegł Ariannę, że pojawiła się Anastazja na statku cywilnym Rodivas. Czemu? Bo nie podoba mu się to, że Aurum się miesza w nie swoje sprawy i tępi Ariannę.
* Henryk Sowiński: dowódca OO Zguby Tytanów; całość sławy i chwały za odbicie Rodivasa spada na niego - zaakceptował plan Arianny. Ma dość tego, że Anastazji nie ma - postawił ultimatum Ariannie.
* Anastazja Sowińska Dwa: stworzona przez Kryptę jako "trochę lepsza" wersja Anastazji, pułapka na Ariannę i na Sowińskich. Przechwycona przez Ariannę; atm na Inferni.
* AK Rodivas: statek cywilny, który spotkał się z Kryptą i przekształcił w anomalię. Nośnik "Anastazji". Ciężko uszkodzony przez Infernię, przechwycili go Sowińscy.
* AK Nocna Krypta: podczas integracji Heleny z Donaldem Paziarzem stworzyła piekielny plan i wprowadziła kopię Anastazji by złapać Ariannę w pułapkę - lub przejąć ród Sowińskich.
* OA Zguba Tytanów: uszkodzony po kontakcie z Kryptą, więc wolniejszy pojazd; uderzył w Rodivas i przejął wszystkie anomalie i działania. Poza Anastazją ;-).
* OO Infernia: pobiła rekord prędkości z Eleną u steru, Arianną jako siła magiczna i Eustachym przesterowującym systemy - dzięki temu przegoniła Zgubę Tytanów.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria, Orbita

## Czas

* Opóźnienie: 11
* Dni: 1

## Inne

.
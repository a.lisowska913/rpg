---
layout: cybermagic-konspekt
title: "Dźwiedzie polują na ser"
threads: waśnie-samszar-verlen, waśnie-blakenbauer-verlen, legendy-verlenlandu
gm: żółw
players: fox, kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [230307 - KJS - Stymulanty Szeptomandry](230307-kjs-stymulanty-szeptomandry)

### Chronologiczna

* [230307 - KJS - Stymulanty Szeptomandry](230307-kjs-stymulanty-szeptomandry)

## Plan sesji
### Theme & Vision & Dilemma

* PIOSENKA: 
    * Triangler (Macross Frontier): kto wygra, pojedynek na zabawę?
    * BELIEVE (Franka Potente): 
* CEL: 
    * just Verlen things
    * pokazanie Marcinozaura

### Co się stało i co wiemy

* Kontekst
    * Apollo wyzwał Marcinozaura mówiąc że on, mieszkańcy i ogólnie Verlenowie sobie poradzą. Nie ma dość silnych płaszczek.
    * Marcinozaur powiedział Uli, że ją zaakceptują jak wygra. Ula pokazuje potęgę.

### Co się stanie (what will happen)

.

### Sukces graczy (when you win)

* Apollo przeprosi.

## Sesja - analiza

### Fiszki
#### Fiszki Koszarowa Chłopięcego

* Marcinozaur Verlen: mag, skrajny sybrianin, mag iluzji i katai
    * (ENCAO: +-+00 | Don't worry, be happy;;Żarliwy, skłonny do wydania ogromnej ilości energii;;Punktualny| VALS: Face, Stimulation >> Humility| DRIVE: Nawracanie na VERLENIZM)
    * styl: Alex Louis Armstrong x Volo Guide To Monsters.
    * mistrz szpiegów Verlenlandu. Apex szpiegator.
    * Sam sobie zmienił imię. Miał "Marcin". Był JEDYNYM Verlenem z normalnym imieniem, ale chciał pójść jeszcze efektowniej.
    * typowa reakcja: "nazywasz się Maciuś, jak brzuszek, bo tylko jesz. CHODŹ BIEGAĆ PO LESIE!" (do kogoś kto nazywa się inaczej niż Maciek)
    * chłopak Uli Blakenbauer
* Ula Blakenbauer: czarodziejka, arcymag płaszczek, senti-infuser
    * (ENCAO:  -0+0+ |Nie cierpi 'small talk';;;;Nieobliczalna, nie wiadomo co zrobi| VALS: Universalism, Face >> Power| DRIVE: Duma i bycie uznaną)
    * styl: Raven z DC x Suspiria
* Apollo Verlen: mag, sybrianin: "żyje się raz, piękne panny!" (UWIELBIANY przez żołnierzy i ludzi)
    * (ENCAO:  ++-00 |Mało punktualny.;;Beztroski i swobodny| VALS: Face, Hedonism >> Security | DRIVE: Uwielbienie Tłumów)
        * core wound: never good enough. Nie jest tak dobry jak mógłby być. Niezaakceptowany przez swoje słabości. Nie dość dobry jako Verlen.
        * core lie: jeśli nie zrobisz wszystkiego pókiś młody, nigdy niczego nie zrobisz! Czas płynie i tracisz życie!
    * styl: głośny, płomienny, wszechobecny - i Johnny Bravo ;-)
    * "żyjemy raz, więc żyjmy pełnią życia, razem!!!" - dArtagnan "we're gonna be drinking"
    * SPEC: dowodzący oficer, morale, walka bezpośrednia

Uczniowie:

* .Serena Krissit
    * (ENCAO:  --0-+ |Nie kłania się nikomu;;Prowokacyjna;;Skryta| VALS: Achievement, Security| DRIVE: Pokój i bezpieczeństwo)
    * styl: pracowita, chce pokazać
    * "Wojna się nie skończy tylko dlatego że nie ma noktian. Prawdziwy wróg jest _tam_."
* .Marta Krissit: <-- WHISPERED
    * (ENCAO:  +---0 |Narcyz;;Nie ma blokad| VALS: Family, Stimulation, Achievement| DRIVE: Lepsza od siostry; przejąć kapitanat)
    * styl: aggresive, shouty
    * "Jestem lepsza od Ciebie i od każdego!"
* .Adam Praszcz
    * (ENCAO:  +0-0- |Pretensjonalny;;Zapominalski;;Żywy wulkan| VALS: Hedonism, Tradition >> Humility| DRIVE: Odbudować reputację najlepszego)
    * styl: nerwowy, gubi myśli, świetny fizycznie, PRACOWITY
    * "Nie rozumiem co się dzieje, ale widać nie pracuję dość mocno"
* .Franciszek Korel: <-- WHISPERED
    * (ENCAO:  -0+00 |Nie znosi być w centrum uwagi;;Cierpliwy| VALS: Universalism, Self-direction >> Face| DRIVE: Niestabilny geniusz Suspirii)
    * styl: Jurek
    * "Zwyciężymy, nieważne z czym mamy do czynienia. To tylko kwestia tego ile zapłacisz."
* .Wojciech Namczak: (ten od głupich Verlenów)
    * (ENCAO:  000-+ |Sarkastyczny i złośliwy;;Łatwo wywrzeć na nim wrażenie| VALS: Stimulation, Conformity >> Humility| DRIVE: Mam rację.)

INNI:

* .Jakub Klardat: dowódca defensyw bazy 
    * (ENCAO:  +--0- |Niestały i zmienny;;Nie przejmuje się niczym;;Apodyktyczny| VALS: Hedonism, Humility| DRIVE: Unikalne przeżycia)
    * chce, by Apollo zapłacił za swoją niekompetencję. Wie, że coś jest nie tak, ale Apollo powinien się tym zająć.
* .Emilia Lawendowiec: aktualna czempionka
    * (ENCAO:  -0-0+ |Mało punktualna.;;Chętna do eksperymentowania| VALS: Achievement, Conformity| DRIVE: Queen Bee)
    * chce być najlepsza i dlatego bierze prywatne lekcje od Apollo. Płaci za to ciałem.
* .Zuzanna Kraczamin: medical + monsterform
    * (ENCAO:  -00-+ |jest w niej coś dziwnego;;Kontemplacyjna| VALS: Power, Universalism| DRIVE: W poszukiwaniu najlepszego potwora)
    * nie jest stąd; z przyjemnością robi nieetyczne stwory
* .Bartosz 
    * (ENCAO:  --00+ |Unika pokus ciała;;Surowy, wymagający| VALS: Self-direction, Face| DRIVE: Ekstremista)
* .Aleks 
    * (ENCAO:  00+-0 |Anarchistyczny;;Perfekcyjnie wszystko planuje| VALS: Power, Hedonism >> Tradition| DRIVE: Ważny sojusznik)


### Scena Zero - impl

.

### Sesja Właściwa - impl

Niecodziennie Apollo prosi Was o pomoc. A już na pewno nie KONKRETNIE o Viorikę i Ariannę. Do Koszarowa Chłopięcego, gdzie Viorika ma powodzenie jako MASTER CAPTAIN OFFICER! A Arianna bo jest ARCYMAGIEM I W OGÓLE ŚWIETNA! Sama obecność powoduje że dzieciaki zakładają fankluby, antydatują je i "były tu zawsze".

Apollo przyjął Was w servarze. To porządny Chevalier, ale bez neurosprzężenia. Chevalier jest uszkodzony, widział walkę. Viorika "czemu niszczysz sprzęt?" Apollo "nasz kuzyn oszalał. Powiedziałbym, że próbowałem go nie zabić ale bym skłamał. Nie udało mi się, musiałem... się odbić z bitwy."

Apollo tłumaczy:

* Pokłóciłem się z Marcinozaurem. Nieważne o co.
* Marcinozaur powiedział, że jestem dupa a nie dowódca bazy. Ja powiedziałem że obronię bazę przed czymkolwiek na mnie nie rzuci. Założyliśmy się.
* Marcinozaur ma... on jest cholernym iluzjonistą. On jest KATAI i ILUZJONISTĄ! (zrozpaczona wściekłość Apollo!)
* Marcinozaur zaatakował najcięższą płaszczką jaką widziałem. Śmierdziało Blakenbauerem na kilometr. Pokonaliśmy pierwszą płaszczkę dzieciakami i obroną. Marcinozaur powiedział, że to była rozgrzewka. Że naprawdę ciężką broń ma gotową na potem. Ciekawe, skąd Marcinozaur ma... płaszczkę. I to taką potężną.
* KONKLUZJA Apollo jest taka:
    * Marcinozaur, jakkolwiek to nie brzmi, ma sprytny plan. Ściągnął Blakenbauera by go pojmać. By pokazać mu, że my w Verlenlandzie jesteśmy lepsi. 
    * "Aria, jesteś bronią na płaszczkę". "Wirek, Ty znajdź słabości tego cholerstwa i pokonaj Blakenbauera." "Ja zajmę się tym w czym jestem najlepszy - logistyką."

Viorika SERIO próbuje nie być złośliwa.

Marcinozaur zrobił sobie szałas w lesie niedźwiedzi. Apollo pokazał Płaszczkę - to bydlę jest w piwnicy. To jest kilkumetrowy jaszczur. To cholerstwo ma cechy żółwiaka - szyja. Plus, co dobijające, jaszczur projektował obraz ofiary (dziewczyny która krzyczała). Gdyby nie Rycerze Apollina, Apollo skończyłby pożarty. "Pojutrze będzie coś fajniejszego". Marcinozaur ma teraz 23 lata.

Viorika i Arianna idą w _camo pattern_ po lesie. Idą do Marcinozaura. Viorika zauważa zasadzkę w lesie?

Tr Z (ostrożność) +3:

* Xz: Viorika zauważyła PRAWDZIWE zagrożenie. Sieci pajęcze. Jakiś pająkoniedźwiedź. Typowe. Więc skupiona na prawdziwym pająkoniedźwiedziu w OSTATNIM momencie zobaczyła kubeł wody z gałęzi.
* V: Viorice udało się ominąć i Ariannie też - nie pochlapało Was
    * ale pułapnik myśli, że prawie wpadłyście. (dzieciaki). Czyli Viorika i Arianna mogą być OFIARAMI.

Bez szczególnych kłopotów dotarłyście do szałasu - szałas: liście, spartański itp. I wychodzi do nich Marcinozaur - cekiny i strój tienowatych.

Aria: "sprawa z płaszczką co prawie Apollo zeżarła." Marcinozaur się ucieszył. Marcinozaur opowiada, że ma płaszczki i chce przećwiczyć dzieciaki. I pozyskał płaszczki od Blakenbauerów - płaszczki w pigułce - i wszystko jest pod kontrolą. Marcinozaur stanął w pozycji zwycięzcy i światło spomiędzy drzew pada na niego na pozę zwycięzcy. Gdy jednak powiedział o "poślednich płaszczkach", z szałasu wyszła Ula. "POŚLEDNIE PŁASZCZKI?!"

* Viorika -> Ula: "skoro chcesz przetestować swoje płaszczki, czemu nie rzucisz Apollowi wyzwania?
* Ula -> Viorika (puzzled): "...a nie to zrobiłam? Marcinozaur powiedział, że tak się rzuca wyzwania."
* V: "to zależy"
* U: "moment. To wyzwałam go czy nie?"
* V: "nie wie że Ty go wyzwałaś. Nie może założyć że to wyzwanie."
* U: "on NIE WIE że to ja?"
* A: "nie ma pojęcia"
* U->M: "powiedziałeś, że jeśli chcę mieć JAKIEKOLWIEK relacje z Verlenami muszę pokazać siłę. W jaki sposób mam pokazać siłę jak nie wiedzą że to ja?!"
* M: "post factum, moja droga."
* U: "...czekam w szałasie..."

Czyli ogólnie chodzi o to:

* Apollo zawiódł ten teren i pojawiła się Szeptornica krzywdząca ludzi
* Elena próbowała rozwiązać to na własną rękę, bo Apollo ją tak odepchnął, że NIE.
* Apollo wjechał (w oczach Marcinozaura) upokarzając Elenę i ją odsyłając
* Marcinozaur, który chce pomóc Elenie by Apollo ją przeprosił i upokorzyć Apollo
* Ula myśli, że bez akcji / bitwy nie będzie szanowana przez Verlenów. I wierzy Marcinozaurowi. Co jest błędem.
* Ula porwie Marcinozaurowi krąg sera. I go schowa. I w 24h Verlenowie mają go odzyskać - bez używania sentisieci.
* Marcinozaur robi OBRAZ WOJNA O SER jako wyzwanie dla Apollo. Cringe prawie zabija Ulę.

Ula jest mistrzowska w robieniu płaszczek. Jej płaszczki są szczególnie dobre. Ona nadaje im życie. I Ula unika cekinów. Ula "nie jest przeciwnikiem w walce". Niech kontroluje płaszczki.

Arianna na stronie próbuje poznać prawdę od Marcinozaura, o co chodzi

Tr Z +3

* V: Marcinozaur się TROSZCZY o Verlenów oraz o sytuację.
    * Troszczę się o Elenę.
    * Szeptomandra jakoś się tu znalazła. To robota Blakenbauerów. Ale nie sankcjonowane. Po to Ula.
        * By DYSKRETNIE dowiedzieć się o co chodzi. Więc ją ciągam ze sobą :D:D:D.
        * Ula ma zebrać informacje a tylko ona może
    * Verlenowie się rozleniwili. Blakenbauerowie też. A Ula potrzebuje szkoły życia.
    * "Aria, weź jak będzie walka, pieprznij jej tak rykoszetem, ok? Ja ją uratuję. Wiesz, będę dobrym chłopakiem."

Arianna i Viorika odwiedzają Apollo. Apollo jest ponury. 

* Apollo: "Wiecie o co chodzi?"
* Ar: "O ser."
* Ap: "CO?!"
* V: "Ser żółty. W takim kręgu."
* AP: (po zapoznaniu się z kontekstem sera i wyzwania) To nawet nie ma sensu!
* V: "Naprawdę musiałeś odebrać Elenie osiągnięcie?"

Apollo tłumaczy, że Elena poszła na Szeptomandrę z oddziałem i to jej zrobiło krzywdę. On odwrócił uwagę wszystkich od Eleny i potem ją uratował, by nie zrobiła nic głupiego.

Viorika: "Ty nie jesteś kiepskim kuzynem. To wszyscy wiemy. Jesteś kiepskim dowódcą, zmarnowałeś asset - optymistyczną Elenę." tłumacząc na przykładzie legendarnego kuzyna Bożygniewa.

TrZ (Viorika żyje militarnymi opowieściami itp) +3:

* Vr: Apollo rozumie, że spieprzył i przeprosi Elenę. Ale nie czuje się źle. Po prostu widzi, że zrobił błąd taktyczny.
* X: Są dzieciaki, które pamiętają jak Elena użyła magii w pojedynku i rozpuszczają plotki na jej temat. To uszkadza lekko reputację Eleny - to sprawia, że tak odebrali ruch Apollo. Sam Apollo zrobił mniej niż to odczytano.
* X: Apollo powiedział Viorice i Ariannie co naprawdę się stało
    * Elena poszła z 5 żołnierzami ale Szeptomandra zaczęła na nią działać
    * Apollo powstrzymał Elenę przed skrzywdzeniem swoich ludzi. Bo ona NAPRAWDĘ chciała ich skrzywdzić pod wpływem ataku mentalnego.
    * Apollo martwi się tym potworem. Zdaniem Apollo to nie jest jedyny potwór tego w okolicy. I DLATEGO odesłał Elenę.
    * Apollo ochronił (w swoich oczach) reputację Eleny i sprawił, że w JEJ oczach on ją powstrzymał, ale NAPRAWDĘ on ją powstrzymał przed zranieniem ludzi
        * Ona myślała że była blisko potwora. Ale ona atakowałaby swoich ludzi.

Apollo sfokusował się że Szeptomandra to robota Uli Blakenbauer. TO BLAKENBAUER. Viorika mu uświadomiła, że Blakenbauerka jest po to by się potencjalnie dowiedzieć o co chodzi i by pomóc. Apollo facepalmował.

Tr Z (Arianna i fakt że Ula studiowała w Verlenlandzie) +3:

* Xz: Ula NAPRAWDĘ ma dobre plany i nie da się jej odjąć, że daje radę, planuje i to "zadziała" jako plan. Nie będzie jednostronnego zaimponowania.
* X: Ula przygotowała dobry _counter_ na działania jakie możecie podjąć - przewidziała, ale może zabraknąć jej mocy.
* V: You must expect mimics. Wiecie, że Marcinozaur normalnie by maskował więc ona NA PEWNO będzie maskować. Na pewno pójdzie "bigger is better". Mniej subtelności, więcej potworów. Chce pokonać na Waszym polu.
* Vr: Ula NIE SPODZIEWA SIĘ STEALTHA po Verlenach. Nikt nie spodziewa się stealtha po Verlenach.
* Vr: Grupa dzieciaków, przebrana za Verlenów przebranych za dzieciaki robi dywersję, natarcie i chaos. To sprawia, że Ula nie ma pojęcia że jej plan nie działa i żę wszystko idzie zgodnie z planem.

Niedźwiedzi komandosi Verlenów (wszystkich trzech) zaczęło szukać aromatycznego sera. Tu Ula zrobiła błąd.

Tr Z (nikt nie spodziewa się niedźwiedzich komandosów) +3:

* V: Niedźwiedzie wykryły dwa potencjalnie miejsca, dwie różne jaskinie.
* Vz: Ula perfekcyjnie to zrobiła. Ale nie doceniła Ursan. W jednej z jaskiń BYŁ ser i jest wzmocniony aromatycznie, tam jest pułapka Uli.
* Vr: Stealth niedźwiedzi komandosi - CAŁA JASKINIA TO MIMIK. Jaskinia to otwarte szczęki.

Viorika ma chory pomysł. Niech komandosi niedźwiedzi (Verleńscy Dźwiedzie) przebiorą się za niedźwiedzie i wejdą do jaskini. Ula chce chronić ser i nie chce mieć problemów ani robić problemów, więc wszystko jest zajęte zwalczaniem dźwiedzi gdzie myślą że to niedźwiedzie WIĘC Arianna na ścigaczu jedzie złapać ser i w długą.

Tr Z (dźwiedzie. Wszędzie dźwiedzie) +3:

* V: Arianna pozyskała ser
* Vz: Przez cholernie dźwiedzie 

NAJBARDZIEJ BEZKRWAWA GRA WOJENNA VERLENÓW ZAKOŃCZONA!!!

1. Apollo jest "zadowolony". KTOŚ zajmuje się Szeptomandrą. Niestety, to Marcinozaur.
2. Ula jest zaskoczona. ALE JAK TO VERLENOWIE I TAKTYKA!? O_O. BEZKRWAWO?!?!
3. Marcinozaur jest zadowolony. Dostał wszystko co chciał. Prawie.
4. MVP? Planowanie + dźwiedzie. Komandosi rządzą. Poćwiczyli sobie.

## Streszczenie

Koszarów Chłopięcy ma problem - Apollo i Marcinozaur się kłócą o płaszczkę Blakenbauerów. Więc przybywa Arianna i Viorika na prośbę Apollo. Nie tylko łagodzą sprawę i poznają prawdę (chodzi o honor Eleny i by pokazać wartość Blakenbauerów Verlenom i vice versa) ale jeszcze robią wyzwanie między Ulą Blakenbauer i Verlenami. Okazuje się, że Ula jest niesamowicie silna, ale taktyka Vioriki, dźwiedzie Verlenów i umiejętności ścigaczowania Arianny (w co nadal nikt nie wierzy bo ona nie umie pilotować XD) wygrały konflikt bezkrwawo. Poszukiwania pochodzenia Szeptomandry trwają, zajmuje się tym Marcinozaur i Ula.

## Progresja

* Elena Verlen: okazuje się wyjątkowo podatna na Szeptomandrę i ataki tego typu

### Frakcji

* .

## Zasługi

* Arianna Verlen: dyplomacja wewnątrzverlenowa między Marcinozaurem i Apollo, łagodzenie potencjalnych problemów i docelowo świetny rajd ścigaczem by pozyskać ser od Uli nie walcząc z jej potworami.
* Viorika Verlen: wydobyła od Apollo, co się działo z Eleną. Potem skłoniła Ulę do wyzwania Verlenów a nie robienia losowych płaszczek. Opracowała plan z dźwiedziami, by całkowicie uniknąć walki. Grand Strategist, zostawiła działanie Ariannie.
* Marcinozaur Verlen: ma teraz 27 lat. SZPIEGATOR. Troszczy się o Elenę i o Verlenów; chce by Apollo przeprosił Elenę, chce pokazać Verlenom siłę Blakenbauerów (stąd Ula i jej płaszczki), chce pokazać Uli siłę Verlenów (stąd dźwiedzie, Arianna i Viorika) oraz chce rozwiązać Szeptomandrę. A wszystko to w swoim uroczym stylu GŁOŚNIEJ ZNACZY LEPIEJ.
* Ula Blakenbauer: ma teraz 22 lata. Arogancka mistrzyni płaszczek i sentiinfuzji. Współpracuje z Marcinozaurem szukając śladów Szeptomandry. Gdy Viorika zaproponowała jej wyzwanie, poszła w mimiki (jaskinia jest JEDNYM mimikiem). Pokonana przez verleńską taktykę i dźwiedzie, czego by się NIGDY nie spodziewała. Acz zaimponowała siłą ognia i magii.
* Apollo Verlen: jest święcie przekonany, że Blakenbauerowie chcieli skrzywdzić Elenę; wziął na siebie ogień reputacyjny by chronić Elenę. To spowodowało konflikt z Marcinozaurem (który to konflikt Marcinozaur wygrywa dzięki Uli). Ściągnął Ariannę i Viorikę do rozwiązania sprawy. Główny dyplomata Verlenów XD.
* Elena Verlen: (nieobecna) gdy zbliżała się do Szeptomandry, zaczęła mieć halucynacje i krzyżówki tego co było naprawdę z Szeptami. Gdyby nie Apollo, skrzywdziłaby swoich żołnierzy. Dlatego Apollo ją odesłał.

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Aurum
                        1. Verlenland
                            1. Koszarów Chłopięcy, okolice
                                1. Skały Koszarowe
                                1. Las Wszystkich Niedźwiedzi
                            1. Koszarów Chłopięcy

## Czas

* Opóźnienie: 13
* Dni: 2

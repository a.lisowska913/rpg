---
layout: cybermagic-konspekt
title: "Ten nawiedzany i ta ukryta"
threads: rekiny-a-akademia
gm: żółw
players: kić, dzióbek
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [220816 - Jak wsadzić Ulę Alanowi](220816-jak-wsadzic-ule-alanowi)

### Chronologiczna

* [220816 - Jak wsadzić Ulę Alanowi](220816-jak-wsadzic-ule-alanowi)

## Plan sesji
### Theme & Vision & Dilemma

* (Dukas "Sorcerer's Apprentice") meets (Nana "Lonely") meets (status vs bycie dobrą osobą)
    * Lea chciała dobrze, ale wszystko wyszło jej spod kontroli i brak umiejętności współpracy i zaufania kończy się tragedią.
    * Lea nie zaufa i nie będzie współpracować, ale spróbuje pomóc Michałowi wiedząc, że to ostateczny koniec ich przyjaźni.
        * "tien nie może zaprzyjaźnić się z człowiekiem, to głupie" && "tien nie może ufać innemu magowi"
            * -> pokaż że Lea idzie sama poza granice bezpieczeństwa mimo, że może oddelegować
            * -> pokaż że Lea chce pomóc Michałowi mimo że temu zaprzecza za wszelką cenę
            * -> Lea woli wyjść na "tą złą" niż przyznać się do tego że chciała mu pomóc
* Status staje na drodze przyjaźni, ale Lea nie jest tą złą
    * -> pokaż, że Lei zależy. Ale nie pokaże tego.
* Lea ma niesamowitą siłę ognia i jej magia choć unikalna to skuteczna
    * -> efemeHorror: walka z opętanymi urządzeniami w fabryce i z dronami
    * -> efemeHorror: w pubie, aura nienawiści - ludzie atakują
    * -> efemeHorror: wysoka odporność na magię Michała. EfemeHorror rozprasza energię magiczną.
    * -> konieczność złapania i ekstrakcji efemeHorrora z tego w czym jest, trackowanie i zmuszenie do opętania czegoś co chcemy użyć
* Konstruminus próbuje pomóc człowiekowi + ogólna podejrzliwość wobec Rekinów
    * -> Lea wymanewrowuje sytuację, ale przez to nie może pomóc
    * -> Wszyscy zakładają złą wolę ze strony Rekinów
* Michał jest złamany i nieufny
    * -> Michał odrzuca wszelkie próby interakcji z magami. Po prostu im nie ufa. Atakuje nawet Oliwera z pięściami.
    * -> Kiedyś jego hobby to było kowalstwo, korzystał z niewielkiej kuźni jako element Warsztatu Podwierckiego. Między innymi tam stworzył Bransoletkę. Teraz pije.
* CEL: nowy przeciwnik dla Marysi Sowińskiej i problemów inter-Rekinowych
* DYLEMAT
    * brak?

### Co się stało i co wiemy

Kontekst sesji wygenerowany przy pomocy ChatGPT-3.

1. Lea Samszar jest Rekinką. Zimna i potężna, 'mean girl', nikogo nie lubi i nikomu nie ufa, walczy o dominację wśród Rekinów (acz nie ten poziom co Marysia Sowińska), regularnie w konflikcie z Mimozą Diakon.
    1. Wspierają ją: Malena Ezrikis i Aleksandra Burgacz
2. Lea zostaje skrzywdzona jako dziecko przez ducha i kolegów
    1. Bardzo dawno temu, jak Lea była młodziutka, miała straszny kryzys
    2. Z grupą magów poszli do lasu po przygodę. Niestety, dotarli do terenu gdzie był niebezpieczny duch
    3. Kolega Lei opracował plan jak mogą sobie poradzić, Lea "trzymała" ducha. 
        1. Ale skorzystali z okazji że Lea im ufa, zostawili ją i Lea doznała strasznej rany psychicznej. Zostawili ją samą w ciemności, przerażoną i zapłakaną i wystawioną na ataki.
        2. MOGLI to wygrać. Ale się bali i nie ufali Lei.
    4. Core Wound Lei od tej pory to "sense of betrayal and abandonment, and she vowed never to trust anyone again"
3. Lea zapoznaje się z Michałem
    1. Rok temu do Lei dotarła wiadomość, że jej ojciec - jedyna osoba jakiej Lea ufa - jest ciężko chory.
    2. Miała osobisty kryzys. Nie chciała przebywać z innymi Rekinami, opuściła Podwiert i zaczęła latać po terenie sama, by nikt nie widział łez.
    3. Tymczasem w fabryce w której pracował Michał w podziemiach doszło do infestacji ducha. Gdy Michał poszedł naprawić sprawę, duch go sparaliżował i zaczął wysysać jego energię.
    4. Jak wiemy, Lea ma kosę z 'duchami'. Gdy wykryła przypadkiem ducha, poleciała go zniszczyć. Nie dbała o Michała, chciała zniszczyć ducha i to zrobiła. Nawet się nie ujawniła.
        1. Ale Oliwier ją zobaczył i powiedział Michałowi. Zaskoczony, że mag - Rekin - zrobił coś takiego.
    5. Michał poszedł jej osobiście podziękować i postawić jej kolację. Lea nie miała ochoty, ale Michał powiedział, że jest jej to winny. A ona naprawdę nie chciała być sama.
    6. Tak się zaprzyjaźnili.
4. Lea krzywdzi Michała
    1. Pół roku temu Gerwazy Lemurczak uznał, że Michał to słaby punkt Lei. Malena i Aleksandra zaczęły jej dokuczać.
    2. By udowodnić Gerwazemu że to nieprawda, Lea zaprosiła Michała i skrzywdziła go strasznie robiąc rytuał jego kosztem.
    3. Michał został pośmiewiskiem tych czterech magów i jego energia zbudowała efemerycznego Horrora Lei.
    4. Od tej pory Aleksandra Burgacz trochę Michałowi dokuczała. Nic poważnego, ot, gdy się nudziła i była w pobliżu.
5. Lea pragnie odkupienia
    1. Lea zorientowała się, że zrobiła coś bardzo złego. Nie podobało jej się to, ale nie mogła odrzucić reputacji a Michał by nie zaufał i nie wybaczył.
    2. Lea zostawiła zbiór hintów by Michał zbudował specjalne urządzenie, bransoletę, która nasycona duchem Lei (o czym on nie wie) da mu reakcję antymagiczną
        1. Chodzi na w miarę bezpiecznej (jaaasne) kombinacji PAIN + SORROW + HUMILIATION. Czyli niestety idzie trochę po Esuriit.
    3. Michał zbiera komponenty i je konstruuje; ślady wskazują, że za tym stoją notatki i badania Triany (acz po przyjrzeniu się widać że to bzdura).
    4. Gdy Michał w końcu po 3 miesiącach to złożył, Lea nałożyła na to odpowiedniego efemeHorrora. Ale niestety - wyszło jej coś co Michała krzywdzi...
    5. Za to Aleksandra Burgacz dalej się bawi z Michałem. Raz chciała pomigotać mu światłami, ale efemeHorror rozpalił światła i eksplodował żarówki. Aleksandra w szoku.
6. Michał cierpi
    1. Halucynacje i bezsenność -> paranoja. Michał "nie jest sam". Coś jest nie tak. Nie wysypia się co wpływa na jego pracę. 
    2. Gdy Michał poszedł na badania - interakcje z magami / puryfikatorami pokazują, że wszystko jest OK.
        1. EfemeHorror nie jest osadzony w Michale ani w bransoletce. On krąży jako byt niezależny. Unika wykrycia. "Minor disturbance / natural occurence".
    3. Michał ma paranoję na temat swoich bliskich i na temat magów (efemeHorror). Wizje śmierci jego najbliższych.
    4. Wpływ efemeHorrora na Kasię, współpracownicę Michała doprowadził do tego, że Kasia była agresywna wobec Michała. Musieli ją przenieść do innego działu.
7. Lea ratuje Michała jak umie
    1. DESTR: Próbowała wezwać innego efemeHorrora do zniszczenia tego oryginalnego, ale pierwszy został zniszczony. A kolejny wymagałby już ofiary z żywej istoty (pet / human). Tego Lea nie zrobi.
    2. DESTR: Próbowała zamknąć efemeHorrora w lustrze, ale nie udało jej się go umieścić w taki sposób by Michał mógł przenieść efemeHorrora w lustro.
    3. SAVE: efemeHorror próbował pożreć umysł Michała we śnie. Lea nie dała Michałowi spać tej nocy, więc efemeHorror stracił swoją szansę.
    4. SAVE: efemeHorror próbował wywołać halucynacje i Złamać Michała korzystając, że był sam - więc Lea zaaranżowała, by MUSIAŁ być na imprezie firmowej tego dnia (rękami Loreny).
    5. SAVE: efemeHorror próbował snami sterować Michałem, więc Lea podłożyła mu książkę centrującą się podczas snu- taką samą jak tą, którą sama się leczyła po ataku
8. Oliwer udaje się do AMZ, do Julii
    1. ...

### Co się stanie (what will happen)

* efemeHorror pragnie zdominować Michała i stać się jego Mrocznym Lustrem. 
    * STRATS / ADVANTAGES: fear&anxiety, nightmares, isolation, temporary mindcontrol
    * GOAL: "weird" -> isolate -> despair -> dominate -> devour
* Lea próbuje zniszczyć efemeHorrora i zostać niezauważoną
    * STRATS / ADVANTAGES: get other mages to protect Michał, find patterns, prepare counterspell
    * GOAL: reduce harm -> investigate wtf -> test hypothesis -> get to mirror -> destroy with magic | ALSO IT WASNT ME OK?

.

* S01
    * Wyścig trzech Rekinów - Daniela, Franka Bulteriera i Ruperta Mysiokornika. Mysiokornik się rozbije. Coś jest poważnie uszkodzonego z jego ścigaczem (kiepskie paliwo, miało być ULTRATURBO ale Rafał był wrogi).
    * Karolina w pubie, spotyka się z nią Oliwer. "Że jest tą sensowną Rekinką". Że Michał jest przeklęty i magowie nie chcą mu pomóc a ona kiedyś komuś pomogła.
    * Okazuje się, że Michał jest w pubie, ale innym - "Eliksir Siekiery", chce się znieczulić. Daniel jest w stanie wyczuć, że coś jest nie tak z energią Michała gdy ten jest agresywny + "nie chciałem tu być".
* S02
    * ?
* S03
    * Aleksandra tym razem się pojawia bo jest ciekawa - co jest nie tak z Michałem. Chce pomóc. Ale jej artefakt eksplodował, lekko raniąc ją i Michała. Aleksandra we łzach. Michał ją wywala.
    * (Michał stworzył to z danych zrobionych podobno przez niejaką Trianę Porzecznik)
    * (w okolicy jest kupiona lokalna drona z sygnaturą magiczną. Próbuje uciec / wycofać się. To drona Lei.)
* S04
    * Lea ma plan - umieścić Michała między dwoma dużymi lustrami i ona jest w stanie to zniszczyć.
    * (kto stoi za opętaniem? Czemu?)

### Sukces graczy (when you win)

* rozwiązanie problemu

## Sesja - analiza
### Fiszki

* Gerwazy Lemurczak: the overmind (nieobecny na sesji) ("silny pożre słabego")
* Lea Samszar: czarodziejka, źródło problemów, 24
    * ENCAO:  +-+-0 |Arogancja i poczucie wyższości;;Kłótliwa;;Królowa lodu;;Overextending| VALS: Face, Universalism >> Hedonism| DRIVE: Deal with pests
    * styl: Adelicia (Rental Magica)
    * przewaga: ogromna siła magii - Horrory (efemerydy strachu i koszmarów na podstawie sacrifice)
* Michał Klabacz: maintenance technician in a factory that produces advanced robotic limbs, CURSED ONE, 33
    * ENCAO:  0+++- |Pełny godności;;Sceptyczny;;Odpowiedzialny;;Ascetyczny| VALS: Benevolence, Achievement >> Tradition| DRIVE: Harmonia i akceptacja
    * styl: rzeczowy, staroświecki, pomocny
    * przewaga: jak się do czegoś przyłoży...
* Oliwier Czepek: mentor, manager i przyjaciel Michała, 52 lata
    * ENCAO:  0+-0+ |Spontaniczny;;Cyniczny;;Wierzy w konspiracje| VALS: Benevolence, Humility >> Face| DRIVE: Inkwizytor
    * styl: konkretny, cyniczny, działa pod wpływem chwili, centuś
* Mikołaj Faniczek: assholish anti-Sharker
    * ENCAO:  -0--+ |Skłonny do manipulacji i podstępów; podstępny i kłamliwy;;Nie cierpi struktur i zasad| VALS: Hedonism, Conformity| DRIVE: Pettiness (Mirako nie docenia, magowie się rządzą); I WILL WIN!
    * styl: Starscream
    * praca: dostarczyciel paliwa do sieci Mirako
* Aleksandra Burgacz: czarodziejka, winggirl Lei, 23
    * ENCAO:  0-0++ |Niefrasobliwa, beztroska i nieco naiwna;Zawsze zgodna z najnowszą modą;;Barwna| VALS: Universalism, Hedonism| DRIVE: Supernowa
    * styl: elegant butterfly, na świeczniku, nie myśli o innych i jest niezbyt bystra; jak parodia Misy z Death Note
    * przewaga: artefaktorka, specjalistka od materii
* Malena Ezrikis: czarodziejka, winggirl Lei, 25
    * ENCAO:  000-+ |Wyrachowana;;Proponuje niekonwencjonalne, dziwne i często śmieszne rozwiązania| VALS: Security, Tradition >> Family| DRIVE: Impress the superior / senpai
    * styl: młoda Asuka ale z nastawieniem na swoje korzyści / Blaster TF G1
    * przewaga: czarodziejka soniczna, elegancka arystokratka, BARDZO dobre zmysły (ród Ezrikis)

### Scena Zero - impl

.

### Sesja Właściwa - impl

Wyścig trzech Rekinów - Daniela, Franka Bulteriera i Ruperta Mysiokornika, w okolicach Kompleksu Korporacyjnego, by drażnić lokalnych terminusów i inne 'siły dominujące'. Hałas i szaleństwo, a nie krzywda. Pomysł Oli Burgacz (beztroska, wesoła, niefrasobliwa). Mysiokornikowi BARDZO zależy. Karo odmówiła - ona wieczorami pije sobie w Ciężkim Młocie, ze zwykłymi ludźmi. Logika Mysiokornika jest prosta: wziął Bulteriera (bo jest wolniejszy) i ma nadzieję że wygra i zaimponuje. Daniel pyta Karo o nitro - chce wygrać. Bo Mysiokornik go obraził pośrednio.

Karo została urażona. Rekonfiguruje szybko ścigacz dla Daniela. Lepsze nitro. Przygotować elementy. Działamy.

CZAS NA WYŚCIG! Mysiokornik poprosił o wstążkę. A tymczasem, Karo pije. Bulterier trzeci, Daniel włącza Nitro i leci na fullspeed. Mysiokornik wstrząśnięty.

Tr Z +4 +3Or +5Og: 

* V: Daniel wybił do przodu
* V: Mysiokornik go nie dogoni
* Or: Mysiokornik uszkodził maszynę - Daniel jest z przodu. Włączył full nitro i coś padło. Ścigacz w płomieniach.
* Vz: Daniel zestrzelił Mysiokornika - by on spokojnie "wylądował" na płaskiej drodze bez szczególnej ilości pojazdów.
* X: Konstruminus zainteresował się tematem...
* V: ...ale Daniel już był z przodu. Jest niewidoczny bo leci zgodnie z przepisami.

Na miejscu Oli nie ma. Poszła pomóc Mysiokornikowi. Który krzyczy "sabotaż!" i dostaje mandat. Daniel czeka na Bulteriera, dziękuje za wyścig i idzie się napić. A Ola całuje Bulteriera, który wygrał za niewinność. I idzie się napić z Karo potem. Jego wpuszczą bo jest z nią.

.

Tymczasem Karo w Ciężkim Młocie. Pije z mechanikami. Podchodzi do niej Oliwier (już podpity) i zaczyna o Michale.

* 100 kredytek zapłacili uczniowi terminusa - nic nie znalazł
* koleżanka z pracy Michała chciała go skrzywdzić, jedna z maszyn prawie go poparzyła i nie śpi
* 200 kredytek jej Oliwier zapłaci jak tylko Karo znajdzie czemu on jest przeklęty!

Daniel ma niezły materiał do nagrania XD. Wyjaśnił Karo, że wygrał, ale musiał zestrzelić Mysiokornika. Mysiokornik jest patałach, ale nie do tego stopnia, by samemu instalować system. Wyraźnie Mysiokornik zrobił coś głupiego, więc spadł. Daniel powiedział Karo dokładnie co widział i że musiał uratować kilka osób więc wyprowadził Mysiokornika z "wraku". I wygrał wyścig. Karo ma dobry humor - Ola całuje Bulteriera XD. Ale tego typu dopalacz nie powinien zrobić tego typu zniszczeń. Piwko i lecimy do gościa.

W domu Michała nie ma. Ale drzwi są tłuczone od środka. Więc Daniel próbuje odpalić _sweep_ magiczny - co jest w środku. Czy coś naprawdę jest nie tak.

TrM+3+3Ob:

* X: zainteresowanie Nadii z osiedlowego monitoringu. Nadia - herod baba.
* V: mieszkanie ma echa energii magicznych. Coś jest nie tak.

Nadia: "WY JESTEŚCIE TYMI, MAGAMI Z AKADEMII! MOŻECIE NIE PRACOWAĆ A STUDIOWAĆ!". Zadzwoni do dozorcy by zdobyć klucze.

Tr Z (ona jest straszna) +3:

* Xz: "Jakie szczury! Nie ma szczurów, to czysty budynek" "SZCZURY MA JAKO ZWIERZĄTKO! MAGICZNE SZCZURY! Z AKADEMII WYSŁALI BY SZCZURY CZYŚCILI!"
* V: Dozorca niepewny że to magowie z Akademii. Nadia "PANIE JANKU RUCHY MAGOWIE CZEKAJĄ!"

5 min później pojawił się dozorca. Otworzył bo widzi "magowie". Nadia chce wejść ale (XV) mimo że robi chlew to została zatrzymana bo do niej też przyjdą XD.

Mieszkanie Michała wygląda dziwnie.

* Całość elektroniki jest odpięta od prądu
* On tu przebywa ale nie jest
* Ma napisy na ścianach, na lustrze "masz dwoje oczu", "wszyscy są zdrowi"
* Ogólnie, jest bajzel

Wygląda, jakby coś było bardzo nie tak. A Daniel - więcej niż jedna energia magiczna.

Daniel szuka z użyciem magii gdzie koleś mógł się udać

Tr M Z (dostęp do mieszkania) +3 +3Ob:

* Vz: ślady jednoznacznie wskazują na to jaki ma tryb życia Michał + zapiski.
    * "Jeśli nie jestem przytomny nikomu nic się nie dzieje. Najtaniej jest w Eliksirze Siekiery."

Karo dzwoni do znajomego deratyzatora i prosi o martwego szczura. "Stefan muszę podłożyć szczura! Nie pytaj!" Stefan jak domyślił się że trzeba podłożyć TIENCE takiego szczura zapewnił DOBREGO szczura. I faktycznie, 15 min później Karo miała pod ręką TAKIEGO <cmok> szczura. Dozorca nalega, by Nadia wpuściła - w końcu magowie AMZ polują na szczury i szukają szczurów! Daniel odwraca uwagę Nadii a Karo podkłada szczura w miejsce niedosprzątane bo niedostępne.

Tr Z (Daniel odwraca uwagę + szok że magowie) +2:

* V: Szczur podłożony ale nie wie.
* V: Jako że to MAGICZNY szczur i to NIE JEJ WINA to musi opuścić to miejsce na 2-3 dni.
* X: Akademia słynie z tego, że jej studenci polują na magiczne szczury w Podwiercie.
* V: Nadia powie co wie jak się pakuje. Lubi narzekać na Michała
    * To był taki sympatyczny chłopak, tylko dziwny, bo miecze przynosił do domu.
    * Pojawiła się pannica. Wchodziła przez okno, albo zerkała. Na pewno była arystokratką, z TYCH arystokratów.
    * Ona robiła złe rzeczy z jego pokojem. I robiła z nim złe rzeczy.
    * Coś JESZCZE tu się działo, zniszczenia w pokoju. Krzyczał w nocy. Straszne dźwięki z telewizora itp.
* V: Daniel ma zdjęcia "tej dziewczyny". To Ola. Oczywiście, że to Ola. Ale co ciekawe - na wcześniejszych zdjęciach Ola ma kocią minkę. Na późniejszych jest zaniepokojona.

Opowieści Nadii będą rosły. I faktycznie, udało się Nadii pozbyć. Nadia sobie poszła. 15 min później pojawił się niezbyt trzeźwy Michał. Złapany, krzyknął, zamachnął się na Karo i Daniel go unieruchomił. Michał nie chce mówić, ale Daniel go przesłucha zastraszając biurem podatkowym.

Tr +3:

* X: ma wątpliwości kim jesteście i musieliście powołać się na Oliwiera.
* Vr: Michał powiedział kilka rzeczy
    * tak, Ola mu dokuczała. Solidnie. Ale dopiero wtedy, gdy zdradziła go przyjaciółka. A raczej myślał że jest przyjaciółką.
    * Ola była pierwszą czarodziejką jaką poznał.
    * Powiedział sekwencję: Ola (i spotkanie przy kuciu mieczy) -> Lea -> Ola i notatki Triany (powiedział jak było)
* (+2Vg) V: Michał pokazał notatki Triany.
    * To nie powinno działać. Czegoś brakuje - ta bransoleta zrobiona przy użyciu krwi UŻYWA Esuriit, ale jest tylko część elementu. Brakuje energii magicznej.
    * Instrukcja wygląda na to, że to był treasure hunt zaprojektowany przez kogoś by Michał to znalazł
* V: Michał opowiada co go spotkało itp.
    * z tego wynika, że bransoleta faktycznie coś robi i robi kontrmagię... jakoś. Ale to znaczy że był jakiś dodatkowy krok o którym on nie wie?
    * COŚ go próbuje skrzywdzić, ale jednocześnie COŚ INNEGO go ratuje. Np. gdy szedł w ciemność w nocy, przyszła Ola żalić mu się na tienki.

Co się Danielowi rzuca w oczy - jakby tu były ścierające się energie magiczne... to wygląda jakby było "something went terribly wrong!"

Karo jedzie odwiedzić Olę. Musi z nią pogadać.

Ola Karo przyjęła z sympatią. Michał to "fajny człowiek". Więc niech Ola mu pomoże. Ale Ola poprosi: Niech Karo NA PEWNO nikomu nie powie, to wielki sekret.

Tr (niepełny) Z (bo Ola chce pomóc) +3

* Vr: Ola zaczęła mówić:
    * Ola nie chce by było wiadomo że chce pomóc bo straci reputację. (oczywiście Karo od razu powie bratu)
    * Ola chciała mu pomóc, przygotowała artefakt ale coś Skaziło artefakt -> Ola nie wie co dokładnie, ale to czekało na zaklęcie wchodzące w interakcję z nim.
    * Potem Michał wywalił ją na korytarz i Nadia ją opieprzyła i upokorzyła.
    * Ola zauważyła bransoletkę i uznała, że to typowy ludzki artefakt kupiony od jakiegoś maga z Akademii.
* Vz: Czemu Ola pomogła Michałowi gdy "wpadał w ciemność"
    * Ola powiedziała w zaufaniu, że miała straszny kryzys.
    * Lea powiedziała jej, żeby poszła do swojego człowieka i jemu się wyżaliła. I powinna lecieć i nie powinna zwlekać.
    * Ola była zdziwiona, bo Lea wyraźnie nie dba o tego człowieka. Bo prawdziwa arystokratka nie powinna dbać o człowieka a Lea jest prawdziwą arystokratką.   
        * Ola Lei trochę zazdrości. Bo ona dba. Mimo, że Lea pociesza Olę "możesz dbać".

TYMCZASEM DANIEL. Próbuje się zamaskować i ukryć dla magii.

Tr Z (przesiąkł i mniej więcej wie o co chodzi) +2 +5Ob:

* Ob: pojawiają się magiczne szczury. Są magiczne szczury. Z AMZ. Oczywiście.
* Vr: Daniel się ukrył przed magią. Ale jakim kosztem.

Zaczyna się manifestacja. Ogniskuje teraz na Michale. Trigger - nie ma maga. Manifestacja nie rozumuje jak mag - widzi Michała i nie poznaje że mag. 

* Vz: 
    * dotyk mentalny w Daniela: niechęć i naturalna odraza do Michała
    * dotyk w Michała: despair + hopeless. Self-destruction, BEZ samobójstwa.
    * efemeryczny horror
    * to jest osadzone w bransolecie ale chwilowo jest poza nią. W tej chwili jest w telewizorze.
* X: Michał orientuje się, że Daniel jest magiem. Horror nie wie.
* X: Michał robi się skrajnie bojowy
* X: Daniel musiał go znokautować. Gdy Michał stracił przytomność, Horror się "rozproszył". 
* Vm: Różne FORMY energii i bytów astralnych:
    * guardian spirit w skarpetkach i książce kucharskiej by pilnować
    * horror, osadzony w bransolecie i rozpełznięty
    * counterspeller w bransolecie

Karo TYMCZASEM rozmawia z Leą.

Karo nie okazała Lei wystarczającego szacunku. "Nawet Marysia" a Lea nie akceptuje tego podejścia. Karo chce się dowiedzieć.

Tr (niepełny) Z (człowiek) +2 (z litości):

* X: Karo musiała dać dowód że człowiek jest w ich rękach i tu leci. (zdjęcie dzisiejszej gazety + Michał)
* V: 
    * Lea powiedziała, że tu jest Horror i ona próbuje go rozmontować. Ale jeszcze jej się nie udało.
    * Korzystne jest, że mi go przywieziecie.
    * Zdaniem Lei, ten Rytuał nie powinien doprowadzić do Horrora, ale mógł osłabić Michała i Horror się zagnieździł. Więc nie może wykluczyć i chce naprawić.
    * Zdaniem Lei, nikt nie będzie na nich polować.

Daniel odzyska 200 kredytek XD. Oliwier mu zapłaci. Wbrew sobie :D.

## Streszczenie

Ola Burgacz, poślednia tienka zacnego rodu zorganizowała wyścig mający drażnić lokalsów Podwiertu, który skończył się: zwycięstwem Daniela, rozbiciem Mysiokornika (ktoś zatruł jego paliwo) i całusem Oli do Bulteriera. Tymczasem do Karo przyszedł centuś Oliwier prosić o pomoc dla pracownika, Michała, który jest "przeklęty". Karo i Michał weszli mu na chatę, znaleźli ślady magii ale co ważne nie na samym Michale. By pozbyć się wścibskiej sąsiadki, podłożyli jej szczura.

Okazuje się, że Ola dokuczała Michałowi i ów zrobił artefakt który podobno szedł z planów Triany (nonsens) i który nie miał prawa działać - a działa. Ola chciała mu potem pomóc, ale nie umiała. Za wszystkim stoi Lea Samszar, która lubiła Michała ale dla utrzymania swojej reputacji przed bardziej podłymi Rekinami poświęciła Michała w rytuale. Gdy nie wiedzący o tym Daniel i Karo dotarli do Lei i przywieźli jej nieprzytomnego Michała po ataku efemerycznego horroru, Lea podziękowała i powiedziała że mu pomoże. Jak inne REKINY poprosiły ją o pomoc, miała pretekst by mu pomóc.

A przy okazji, przypadkiem, Karo i Daniel sprowadzili na Podwiert plagę magicznych szczurów za które obwiniona jest AMZ...

## Progresja

* .

### Frakcji

* .

## Zasługi

* Daniel Terienak: Rekin; master tracker, dobry w pułapkach i mistrz walki wręcz prowadzący videobloga; tu przydała się jego kataliza detekcji. Wygrał wyścig  O dziwo, został dyplomatą mediując między Leą i Karo jak te się gryzły o protokół XD. Źródło Plagi Szczurów w Podwiercie, za które obwinione będzie AMZ.
* Karolina Terienak: Rekin; przygotowała lepsze paliwo dla Daniela by ten wygrał wyścig, obiecała Oliwierowi że pomoże Michałowi, potem pozbyła się Nadii podkładając jej szczura. Rozmawiała z Olą by pozyskać info o historii jej i Michała a potem, niestety, porozmawiała z Leą by ta pomogła Michałowi i powiedziała prawdę. Lea, oczywiście, żąda protokołu komunikacyjnego więc tienki się pożarły - ale Lea pomoże Michałowi.
* Michał Klabacz: kiedyś przyjaciel Lei, potem poświęcony przez nią w rytuale. Potem Ola z czystej głupoty mu dokuczała (jak wyglądałbyś w tej sukience? słuchaj mam problem z kolegą co byś poradził o 2 rano?). Scraftował bransoletę i przez Paradoks Lei został opętany przez efemerycznego horrora. Wpada w pętlę zniszczenia, ale Daniel transportuje go do Lei która mu pomoże.
* Oliwier Czepek: centuś, przyszedł do Karo z prośbą pomocy Michałowi bo jest przeklęty i zapłacił 100 kredytek uczniowi terminusa i ten nic nie znalazł...
* Lea Samszar: Rekin; kiedyś pomogła Michałowi Klabaczowi i się z nim zaprzyjaźniła ale jako że tienka nie może przyjaźnić się z człowiekiem, poświęciła go. Widząc, że magowie krzywdzą Michała, zostawiła mu plany zrobienia antymagicznej bransolety, ale gdy tchnęła weń efemerydę, Paradoks nałożył EfemeHorrora. Próbowała sprawić, by ktoś jej przyprowadził Michała do pomocy, ale Ola nie rozumiała jej hintów. W końcu Terienakowie przyprowadzili Daniela, acz Lea i Karo się pogryzły o styl komunikacji i Daniel mediował.
* Aleksandra Burgacz: Rekin; nieco poślednia córa Burgaczów. Bardzo mało bystra. Nie ma w niej zła, ale jest całkowite ignorowanie potrzeb innych. Dokucza Michałowi (jak wyglądałbyś w tej sukience? słuchaj mam problem z kolegą co byś poradził o 2 rano?), ale jak zobaczyła że Michał ma problem to próbowała pomóc mu go rozwiązać. Chciała zdrażnić lokalsów w Podwiercie głośnym wyścigiem, ale skończyło się tym że musiała pocałować Bulteriera który wygrał XD
* Rupert Mysiokornik: Rekin; Koniecznie chciał wygrać wyścig (mający zdrażnić cywili) i zaimponować Oli Burgacz, więc kupił droższe paliwo i zaprosił Bulteriera (bo wolny ścigacz) i Daniela (bo uważa że wygra). Niestety, Daniel był lepszy a Mysiokornik się rozbił bo ktoś mu paliwo zatruł. Dostał mandat od konstruminusa.
* Franek Bulterier: Rekin latający na wielkim, głośnym, ciężkim ścigaczu (bardziej czołg). Zgodził się na wyścig z Mysiokornikiem, bo czemu nie? Wulgarny, powiedział Oli Burgacz wprost, że wszyscy 'chcą umoczyć'. Przez to, że Mysiokornik się rozbił a Daniel się zmył to Bulterier wygrał wyścig i Ola go pocałowała XD. Bulterier się nieźle bawił.
* Nadia Uprewien: herod-baba mieszkająca na Osiedlu Rdzawych Dębów która próbuje wszystko wiedzieć, rządzić się i się wtrącać. Na tej sesji - klasyczna 'Karen'. Szybko wpada w stereotypy (Karo i Daniel to "magowie AMZ"). Daniel podłożył jej szczura by mieć chwilę spokoju i się jej pozbyć.

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Podwiert
                                1. Kompleks Korporacyjny
                                    1. Bar Ciężki Młot: Karo często tam pije rzeczy.
                                1. Osiedle Rdzawych Dębów: gorszej jakości budynki, miejsce tańsze; mieszka tam m.in. Michał Klabacz.
                                    1. Sklep Eliksir Siekiery: bardzo mordownia, dużo taniego alkoholu i specyficznych środków. Tani, "eksperymentalny".

## Czas

* Opóźnienie: 3
* Dni: 1

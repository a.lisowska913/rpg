---
layout: cybermagic-konspekt
title: "Infernia taksówką dla Lycoris"
threads: historia-eustachego, arkologia-nativis, plagi-neikatis
gm: żółw
players: fox, kapsel
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [230125 - Whispraith w jaskiniach Neikatis](230125-whispraith-w-jaskiniach-neikatis)

### Chronologiczna

* [220712 - Pasożytnicze osy w Nativis](220712-pasozytnicze-osy-w-nativis)

## Plan sesji
### Co się wydarzyło

* CES (Closed Ecological System https://en.wikipedia.org/wiki/Closed_ecological_system ) Purdont ma infekcję Osodrzewa Trianai
    * Mamy do czynienia z Osodrzewami, Ainshkerami, Kralvathami i jednym Xirathirem
* Lycoris została wysłana by sprawdzić czemu mamy błędne odczyty z CES Purdont, z ich Wiertła Ecopoiesis
    * Zbliża się burza piaskowa, trzy Remory - Lycoris i dwie inne z agentami mającymi ją chronić
    * Niestety, Trianai uszkodziły komunikację CES Purdont
* Zbliża się POTĘŻNA burza piaskowa. Przestraszony Rafał Kidiron ŻĄDA uratowania Lycoris; nie wie co się stało, ale stracili kontakt z Purdont.
    * Infernia jest najbliżej

### Co się stanie

* Cel Trianai: expand, corrupt Ecopoiesis, create Araispawn.
* Cel Lycoris: contain Ecopoiesis; na przyszłość. Nie możemy zniszczyć, to jest niesamowicie wartościowe.
* Cel Wiktora: nie zgadza się z Lycoris; trzeba wyczyścić bo to przyszłość terraformacji.
* Cel Kamila: ukryć, schować, SAM SOBIE PORADZI. Nic złego się w końcu nie dzieje.
* Sceny:
    * s0: Rywalizacja Eustachy - Ardilla, gdy byli w CES, egged by Daria; Wiktor ofiarą. Wiktor POKAZUJE jaki z niego Face, Family > all
    * s1: Lecą z wujem do CES; na miejscu nie ma Lycoris ani Wiktora. Są sami zainfekowani przez Osodrzewa. Lycoris i Wiktor i kilka osób opuściło teren, mamy 4 explorerów (Eustachy, Ardilla, Laura, ).

### Sukces graczy

* .

## Sesja właściwa
### Scena Zero - impl

Eustachy jest najlepszy w sabotażu i konstrukcji rzeczy. 3-4 lata temu. Jesteście poza Arkologią, w CES Purdont. Daria, niewiele starsza (rok?). Daria ich tauntuje troszkę, dzieciaki się bawią. Daria próbuje się powymądrzać bo siedzi w terraformacji, ale Eustachy i Ardilla nie będą gorsi. Daria więc "ok, then prove".

* E: Niczego nie muszę udowadniać
* A: Bo nie dasz rady, zawsze się migasz
* E: Może mi się nie chce
* A: Jak zawsze? Może nie jesteś inżynierem i nic nie umiesz?

Daria TAUNTOWAŁA. Jak to zrobić by rozwiązać problem terraformacji? Eustachy ma pomysł - pierścień z okruchów skalnych i lodu nad Neikatis, planeta jest mniejsza od Ziemi, składa plan by złożyć na orbitę katapultę kosmiczną by zrzucała kosmiczny lód na planetę by zwiększyć ilość wody.

Tr (niepełny) Z (inżynier + technobabble) +3:

* V: Daria jest przekonana, że faktycznie takie coś może zadziałać. 
* X: Daria potrzebuje PoC. NIEPRZEKONANA że umiesz.  (+1Vg, bo Kacper tłumaczy o sile odśrodkowej i konieczności ładunku)
* V: Daria rozumie, czemu nie możesz dać jej PoC. Wymaga zerowej grawitacji. Jest pod wrażeniem.

Gdy Daria patrzy na Ardillę, Eustachy robi minki by ją zdrażnić. Ardilla powiedziała o pozyskiwaniu roślin, Daria weszła na swój "konik" - Ecopoiesis. Ale nie ma uprawnień, nie może zobaczyć... Ardilla na to czemu więc nie pójść zobaczyć jak to wygląda, co za problem uprawnienia. Daria - grzeczna dziewczyna - skupiła się więc w pełni na Ardilli.

CES Purdont nie ma dobrych "defensyw wewnętrznych" i nie ma dużych konsekwencji. Ale Daria nigdy nie próbowała, nie wie jak...

Ardilla pozyskuje keycard - czeka aż ktoś nie wiem, przebiera się, zjada kanapkę... coś normalnego i zwija ową keykartę. I wychyla się z czegoś wyżej, trzymając na ogonie. By koleś z kartą nie zauważył.

Tr Z (wiewiórka) +4:

* X: koleś będzie szukał karty, ale nie skojarzy z Ardillą. Ale zgłosi itp.
* Xz: element infrastrukturalny nie wytrzymał i Ardilla spadła. Darilla tłumaczy - próbowała ratować COŚ, ale nie wytrzymało... bullshit
* V: Daria zwinęła keykartę. Obie się rozchichotały.

Eustachy nakablował, że one zwinęły kartę. Dziewczyny poszły otworzyć drzwi a tam... SUPERVISOR. Wuj Bartłomiej i szef stacji Wiktor. Wiktor z gębą a Darilla "wujku wiedziałam że tu będziesz znalazłam keykartę...". Wiktor chce zachować twarz. Bartłomiej "pamiętasz jak my gdy byliśmy młodzi".

Mina wujka na Inferni się zmieniła. Zniknęła to jowialność. Jest surowość. Wyjaśnił, jakie są potencjalne konsekwencje. Do końca wizyty tutaj - nie opuszczasz Inferni. Ardilla ryzykowała relację gdyby to była inna baza niż CES Purdont. No i wzięcie Darii na akcję? Niedopuszczalne.

TYMCZASEM Eustachy wchodzi korytarzem serwisowym i tam robi obejście na grodziach itp. Jak Ardilli się nie udało, jemu się uda. A nie udało się bo podkablował.

Tr Z (nikt, NIKT się nie spodziewał) +3:

* Xz: Wiktor COŚ PODEJRZEWAŁ I ZOSTAWIŁ CZUJKĘ.
* X: Czujka (Kamil) wyczaił że ktoś idzie. KAMIL PREDATOR.
* X: PREDATOR KAMIL ZŁAPAŁ EUSTACHEGO!

Eustachy broni się domniemaniem niewinności, wypełza z siatki. Kamil z wyższością pozwala wyjść. Eustachy go omija i patrzy na panel. Kamil się stroszy NIE! Eustachy mówi, że szuka danych, zagaduje udając że go to ciekawi... czy wpadł na pomysł innowacyjny czy coś od dawna.

Tr (niepełny) Z (Daria; Eustachy że "podoba mi się Daria") +3:

* X: Kamil NIE PUŚCI Eustachego, ale może okaże mu litość lub się wstawi
* V: nie będzie poważnych konsekwencji
* X: patrzą na Eustachego tak... no chłopiec, końskie zaloty, POBŁAŻANIE.
* Vz: sprzedadzą wujkowi, żeby był łagodny, bo chłopak się zakochał... zdarza się...
* X: wyszło że Eustachy podkablował.

Wujek OPIERDOLIŁ Eustachego zwłaszcza za podkablowanie. Skrzywdzenie ARDILLI? To nie jest sposób na życie. I wujek to wyciszył - takie rzeczy zostają W RODZINIE...

Eustachy przeprosił Ardillę. Ona "tego bym się po Tobie NIE spodziewała". Ale przyjęte.

### Scena Właściwa - impl

Eustachy i Ardilla, Celina, Jan. -> Infernia. Trzeba lecieć po Lycoris przed burzą piaskową. A czemu ich wziął? Bo nie chce lecieć sam (ale wie że Janek x Daria i chce dobra dla młodych). Taki wujek ;-).

Dotarli do Nativis dość szybko, w 40 min (wujek dał wszystkim możliwość popilotowania Inferni). Nie jest fanem bycia taksówką dla ważnych kobiet. Gdy się połączył z bazą, nie dostał połączenia do Wiktora - odebrał Kamil. Dobra, niech pakuje Lycoris. Nie da się, pojechała z Wiktorem do wiertła Delta. Jan jest załamany jak wujek "nie możemy zostać w Purdont, mamy mało czasu do burzy".

Ardilla -> wujek ZOSTAWMY JANKA. W końcu i tak nie przydamy się przy wiertle.

TrZ+3:

* V: Janek może zostać (z kimś). Więc wujek zostawił Eustachego + Janka. Ale jak Eustachy to Celina też chce. A jak Celina - to czemu nie Ardilla?

Infernia poleciała szukać Lycoris i Wiktora, a Wy do śluzy. Przy śluzie powitani przez pana Czesława emeryta. On jest uzbrojony w tazer i ma pancerz, co nie jest typowe. Coś jest nie tak. Ma zaprowadzić do kafeterii, Kamil postawi obiad. Ale są tu dla Darii - więc Ardilla spytała Czesława czy można Darię zwolnić? Czesław spytał czy wszystko ok, czy można zwolnić? NIE. Kamil nie pozwala.

Kamil się spotyka z dzieciakami w kafeterii. Jest w trybie PREDATORA, więc na coś poluje. Ma BROŃ I W OGÓLE i próbuje udawać że jest ok. Tak wprost "co mam zrobić byście tu zostali?". Ardilla "Darię.". Kamil "dobrze, dostaniecie ją." Po chwili się stropił - nie jest w stanie dać Wam Darii z jakiegoś powodu. Zostawił dzieciaki by się tym zająć. Janek się zmartwił, Ardilla go uspokaja.

Eustachy podchodzi do kompa i zaczyna dłubać. Szuka informacji o Darii - co się dzieje, co jest w logach itp.

Tr Z(Daria dała Eustachemu swoje kody) +3:

* V: ostatnimi czasy - pojawiają się problemy związane z wierłem ekopoezy delta. Tam pojechała Lycoris i Wiktor wraz z ochroną Lycoris. Część z rzeczy związanych z terraformacją zaczęła działać źle z tą bazą. Dużo jest powiedziane o "osach".
* X: dane o osach są utajnione przez Kamila. Który próbuje to rozwiązać sam. By nie wyszło na jaw. Dlatego nie chciał Inferni.
* Vz: są dane o osach w logach transportowych, dokładniej w logach z arkologii Nativis. W arkologii był obszar który był odcięty przez pewien czas - coś się zepsuło z żywnością, jakiś pasożyt. I jego formą były osy. Jeden z wektorów infekcji. I Daria zgłosiła osy w systemach w life support.

Wujek nic nie wie - nie wie, że PLAGA się tu pojawiła. A to jest kwestia plagi. To było zaraźliwe. Eustachy łączy się z Darią.

* V: połączenie z Darią.
    
* "halo?" - słabym głosem Daria
* "hello mordeczko chodź do jadalni"
* "nie mam apetytu... jestem w pokoju, źle się czuję"

Eustachy -> Celina "co z osami, skąd się wzięły". Celina -> "byłam na Inferni, ale mogę ściągnąć z systemu komunikacyjnego". Eustachy -> "mają osy tutaj". Gorzej, że Daria nie pamięta os; nieco odpływa. Jej umysł nie jest w pełni w tym samym miejscu.

Ardilla -> Janek "potrzebujemy tych rzeczy od Darii, uprawnień." Janek ma łzy w oczach - zrobi co może. Przekonywanie Darii, by ta sobie poradziła. Ardilla + Janek -> maksymalne sygnały emocjonalne. Ardilla, kompromitujące sekrety. Janek też. "pamiętasz jak się trzymaliśmy za ręce i patrzyliśmy na grzyby"

Ex Z (znacie ją dobrze i macie Janka i wspólną historię) +4:

* Vz: Daria odblokowała uprawniania medyczne i biologiczne dla Was
* Xz: Janek powiedział tak kompromitujące rzeczy że siostra na niego patrzy z wyrzutem. I wiersz...
* X: Kamil dowie się o intruzji i próbuje Coś Z Tym Zrobić.
* X: Kamil odciął.

Moment później wpada Kamil z Czesławem i się żołądkuje. Kamil wrzeszczy, że sytuacja jest pod kontrolą. Nie ma żadnych os. Ale Celina - augmentowana - widzi, że z Kamilem jest coś nie tak. Ardilla przekonuje pośrednio Czesława, że Kamil jest niebezpieczny. Czesław - człowiek o dobrym sercu, który zna trochę Ardillę - zrozumiał.

Ardilla zauważyła, że Kamil nie wszystko pamięta i zachowuje się irracjonalnie.

Tr +3:

* X: Czesław wycelował w Kamila i się zawahał.
* X: Czesław nie strzelił. Kamil "TY JUDASZU" i zaatakował Czesława. Wątły biurokrata robi wpierdol ochroniarzowi (+4Vg)
* V: Janek wkroczył. Zneutralizował Kamila. Pomógł Czesławowi - nic mu nie jest.

Celina szybko "jesteśmy zmodyfikowani, więc odporniejsi".

Janek spróbował otworzyć drzwi po czym natychmiast zamknął. Tam COŚ jest. A Ardilla usłyszała, że coś porusza w rurach w stronę Zespołu. Czesław dał broń Eustachemu i Jankowi.

Ardilla wskakuje na rurę by ją przerwać i zrzucić przeciwnika a Janek + Eustachy to porażą. Celina zaczyna zbierać ładunek. Ardilla "jeden z naszych?"

Tr Z (zaskoczenie) +4:

* Vz: Ardilla zdewastowała bazę. Stwór klapnął. (+Vg)
* V: Janek i Eustachy potraktowali to ładunkiem.
* Vz: Dostał kolejną wiązkę prosto w pysk a Janek odsunął Celinę z linii ognia. Stwór padł z chrupnięciem. Zerwały się osy i padły.

Ardilla próbuje opracować trasę - na bazie tego co słyszy i widzi. Wyskakuje, szczurzy, wraca, prowadzi. + wiedza Eustachego o korytarzach serwisowych.

Tr Z (wiedza Eustachego) +3:

* Vz: fragment drogi przez korytarze serwisowe pomógł
* X: pełzniecie przez korytarz serwisowy. Ardilla, Janek, Celina, na końcu Eustachy. I słyszycie bzyczenie.
* V: pułapka na osy - zabudować korytarz kurtkami by je na pewno trafić dezodorantem Celiny. I bez problemu się udaje - osy spłonęły. (+Vg)
* V: udało się dostać niezauważenie do systemów komunikacyjnych. 

W środku, przy systemach są 2 osoby. Są zdezorientowane. Bardzo zdezorientowane. Pojawiają się osy w centralnym. Kurtka z perfumami Celiny "14 sposobów na poderwanie faceta" (z seksshopu, na Eustachego) z nadzieją że się je uda spalić.

* X: podczas operacji osa użądliła Czesława
* V: ale mimo, że osy były rozwścieczone, udało się je spopielić.

Centrum komunikacyjne stoi otworem. Eustachy siada przed kompami i rusza do komunikacji z Infernią. Wujek PIERWSZY RAZ nie może ich ratować - musi mieć i Lycoris i musi mieć kontakt z bazą. Czyli "dzieciaki" są skazane na siebie chwilowo. Wujek ma z tym problem, chce wracać i ratować, ale Lycoris... i i tak nie poradzą z plagą.

Eustachy oraz Ardilla próbują złożyć połączenie z Nativis używając kodów wujka. Agenci plagi nie są inteligentni, ale PLAGA jest. Dlatego atakuje m.in. w połączenia, komunikację itp.

ExZ... -> TrZM+3+3O:

* X: podniesione pole magiczne wynikające z użycia mocy wzmacnia Plagę
* V: udało się zrekonstruować urządzenie; nawiązać połączenie mimo uszkodzonego talerza

.

* "Rafał Kidiron, co się stało, Bartek?"
* Eustachy: "to nie Bartek to nie ja jestem plagą ale tu jest wujek dał nam kody abyśmy ogarnęli i kody pod jego patronatem"
* R: "młodzieńcze, daj mi Lycoris"
* E: "ona gdzieś tam jest i chyba jest zarażona..."
* R: "przygotuję zapas leków. Infernia sobie poradzi".
* E: "Lycoris jest na zewnątrz"

Rafał przekazał nie do końca idealne środki (blueprinty), ale takie co powinny zadziałać na pewien czas. Powiedział, że trzeba odciąć żywność i inną materię organiczną.

* Xz: tamta sekcja bazy jest już odcięta. Ktoś z tych tu zarażonych musiał to zrobić pod wpływem Plagi. Coś z Plagi tu jest, coś inteligentnego.

(nadal można to zrobić, ale manualnie; tam jednak jest epicentrum i wujek nie doceni); więc Eustachy zdekompresuje bazę tam gdzie nie ma ludzi

* X: mimo, że ludzie radzą sobie nieźle, to jednak Plaga skutecznie zaplanowała operację. Masz dowód na istnienie inteligentnego agenta Plagi. I to wie, gdzie jest Eustachy.

Dla Kidirona ważniejsza jest jego kuzynka niż populacja tej stacji... i jakkolwiek Eustachy jest skłonny iść za myśleniem Kidirona, ani Celina ani Ardilla ani Janek nie zgadzają się z czymś takim.

Rafał Kidiron zastanawia się jak to możliwe, że Plaga uderzyła właśnie przed burzą piaskową. Cholernie wygodne.

## Streszczenie

Plaga Trianai uderzyła w CES Purdont; Infernia p.d. Bartłomieja Korkorana ruszyła jako taksówka dla Lycoris ale młodzi trafili do Purdont a Infernia szuka Lycoris i Wiktora. Gdy młodzi doszli do Plagi i przebili się do łączności to poinformowali Nativis i wujka o ryzyku. Dostali skonfliktowane polecenia, ale mają przetrwać. Jednak dla części z nich to nieakceptowalne, bo nie zostawią zarażonej koleżanki na pożarcie Pladze Trianai mimo znajdującego się gdzieś w cieniu Inteligentnego Koordynatora Trianai...

## Progresja

* Bartłomiej Korkoran: w przeszłości znał się z Wiktorem Turkalisem. Lubią się.
* Wiktor Turkalis: w przeszłości znał się z Bartłomiejem Korkoranem. Lubią się.

### Frakcji

* .

## Zasługi

* Eustachy Korkoran: PAST: nakablował na Ardillę za co opieprzył go wujek (never betray a FAMILY!) po czym próbował się wkraść do laboratorium i złapał go niekompetentny zwykle Kamil. ACTUAL: 20 lat; doszedł do obecności Plagi Trianai na CES Purdont i współpracując z Zespołem przebił się do centrum komunikacyjnego usuwając osy a nawet ainshkera. Przełamywał komputery używając też do pomocy magii - dzięki temu sabotaż Plagi Trianai nie dała rady odciąć bazy. Dowodzi, jako następny po Bartłomieju w rodzie Korkoran.
* Ardilla Korkoran: CONCEPT: badacz/odkrywca/scrapper. Zmodyfikowana tak, by łatwiej wchodzić w niedostępne miejsca, bardziej zwinność + orientacja w terenie + pewne umiejętności badawcze. Eustachy składa maszyny, ja kradnę znaleziska ze zrujnowanych struktur. Rywalizujemy na polu Mój jeszcze-nie-wiem-co-to-inator jest lepszy, niż twój! PAST: wiewiórcza rozrabiaka, próbowała ukraść keykartę i chciała wkraść się z Darią do laboratorium ekopoezy; Eustachy podkablował więc złapana. ACTUAL: przekonała wujka by mimo presji czasowej pomógł Jankowi spotkać się z Darią, skoczyła na rurę i w ten sposób dała okazję Eustachemu i Jankowi na zestrzelenie ainshkera i pełniła rolę głównego zwiadowcy.
* Daria Raizis: PAST: grzeczna dziewczyna, która próbowała wkręcić Eustachego i Ardillę w coś z terraformacji; ACTUAL: odkryła osy Trianai w life support, została dziabnięta, dodała do logów informacje i półprzytomna dała uprawnienia do tematów medycznych i life support ukochanemu Jankowi i reszcie Zespołu.
* Bartłomiej Korkoran: PAST: łagodny wujek opiekujący się Eustachym i Adrillą, który jednak rozdziela opierdol gdy zasłużyli; bardzo ważna jest dlań rodzina. ACTUAL: dba o to, by Janek mógł spotkać się z Darią i próbuje chronić swoich młodych podopiecznych. Jednak zaufał im i oddał im do działania zainfekowany przez Trianai CES Purdont (na usprawiedliwienie, nie wie z czym ma do czynienia).
* Celina Lertys: złotoskóra o błękitnych elektrycznych włosach podkochująca się w Eustachym (science / bio officer + medical) Inferni. Dość poważna, nosi przy sobie kosmetyki do podrywania Eustachego, augmentowana na widzenie rzeczy ukrytych (np. zmian skóry Kamila). Mimo młodego wieku (19) dość kompetentna i umie się opanować.
* Jan Lertys: silny i zdolny do bitki, choć nie najinteligentniejszy mat Inferni zakochany w Darii Raizis. Pisze dla Darii kompromitujące wiersze i nie aprobuje relacji Celiny w stronę Eustachego. Bardzo opiekuńczy wobec siostry i Zespołu. Rozstrzelał ainshkera z Eustachym.
* Wiktor Turkalis: szef CES Purdont; prostacki i krzykliwy, ale robi co powinien. Pod namową Bartłomieja Korkorana nie tępi młodych rozrabiaków. Wraz z Lycoris udał się do wiertła ekopoezy i oddał bazę Kamilowi. Nie wiadomo co się z nim dzieje.
* Kamil Wraczok: biurokrata i p.o. dowódcy CES Purdont z kompleksem KAPITANA MARINES. Złapał Eustachego kilka lat temu jak ten się wkradał do laboratorium, ale okazał litość. Aktualnie - zarażony przez Plagę Trianai; doprowadził do tego że CES Purdont straciła jakiekolwiek szanse odparcia Plagi. Pobity przez Janka, nieprzytomny.
* Rafał Kidiron: wysłał Lycoris do CES Purdont; zniknęła niedaleko Wiertła Ekopoezy Delta. W panice wezwał Infernię ale nic im nie powiedział. Pomógł Zespołowi przesyłając kody z arkologii by wstrzymywać Plagę.
* Lycoris Kidiron: wysłana przez Rafała do CES Purdont bo coś nie tak z terraformacją; zniknęła z Wiktorem w okolicach Wiertła Ekopoezy Delta w okolicach burzy piaskowej.
* Czesław Żuczek: emerytowany ochroniarz (acz dalej aktywny) w CES Purdont, lubi Ardillę. Stanął po stronie młodych przeciw zarażonemu Kamilowi i został pobity; potem chronił młodych i dziabnęła go osa. Chwilowo odpoczywa.
* OO Infernia: główny okręt obronny Arkologii Nativis; zdolny do działania w burzach piaskowych i praktycznie dowolnych plugawych warunkach. Pod dowodzeniem Bartłomieja Korkorana. Tylko Bartłomiej zna sekrety Inferni - i Emilia d'Erozja.

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Neikatis
                1. Dystrykt Glairen
                    1. Arkologia Nativis
                    1. CES Purdont: Closed Environment System; niewielka robocza stacja terraformacyjna (na kilkadziesiąt osób); około 100 km od Nativis
                        1. System łączności: miejsce starcia z grupą os; wszyscy są pozarażani przez Plagę Trianai, ale jeszcze niegroźni. Eustachy magią połączył się z Nativis i wujkiem
                        1. Kafeteria: miejsce "przetrzymywania" młodych agentów Inferni; po zdjęciu zarażonego Kamila pokonali tam ainshkera.
                        1. Life Support: miejsce gdzie zaczęła się Plaga Trianai; tam Daria została dziabnięta przez osę.
                        1. Laboratorium Ekopoezy: miejsce, gdzie próbowała się dostać Daria dawno temu; tam przyłapano Eustachego i Ardillę XD.
                    1. CES Purdont, okolice
                        1. Wiertło Ekopoezy Delta: mechanizm terraformacji, niestety, przez pole magiczne i uszkodzenie spowodowało rozrost Osodrzewa Trianai

## Czas

* Opóźnienie: 74
* Dni: 2

## Konflikty

* 1 - Eustachy próbuje pokazać Darii jak zrobić terraformację by przekonać ją, że się zna; by pokonać Ardillę.
    * Tr (niepełny) Z (inżynier + technobabble) +3
    * VXV: Daria jest przekonana i jakkolwiek wierzyła że potrzebuje PoC, jednak nie, bo mass driver wymaga zerowej grawitacji.
* 2 - Ardilla pozyskuje keycard - czeka aż ktoś nie wiem, przebiera się, zjada kanapkę... coś normalnego i zwija ową keykartę. I wychyla się z czegoś wyżej, trzymając na ogonie. By nie zauważył.
    * Tr Z (wiewiórka) +4
    * XXzV: teren nie wytrzymał i Ardilla spadła i się zdradziła, ale Daria zwinęła keykartę gdy Ardilla tłumaczyła.
* 3 - Eustachy wchodzi korytarzem serwisowym i tam robi obejście na grodziach itp. Jak Ardilli się nie udało, jemu się uda. A nie udało się bo podkablował.
    * Tr Z (nikt, NIKT się nie spodziewał) +3
    * XzXX: Kamil go znalazł i złapał XD.
* 4 - Eustachy broni się domniemaniem niewinności, wypełza z siatki. Kamil z wyższością pozwala wyjść. Eustachy się próbuje wyłgać "bo Daria mu się podoba qq"
    * Tr (niepełny) Z (Daria; Eustachy że "podoba mi się Daria") +3
    * XVXVzX: Kamil nie puści Eustachego, końskie zaloty, poproszą wujka o łagodność, wyszło że Eustachy kablował.
* 5 - Ardilla -> wujek ZOSTAWMY JANKA, niech Infernia znajdzie Lycoris i Wiktora przed burzą.
    * TrZ+3
    * V: wujek przekonany, zostawią
* 6 - Eustachy podchodzi do kompa i zaczyna dłubać. Szuka informacji o Darii - co się dzieje, co jest w logach itp.
    * Tr Z(Daria dała Eustachemu swoje kody) +3
    * VXVzV: pojawiają się problemy z wiertłem, Kamil utajnił dane i próbuje sam, dane o osach w logach transportowych; Daria zgłosiła osy w life support; połączenie z zarażoną Darią.
* 7 - Ardilla: przekonywanie Darii, by ta sobie poradziła. Ardilla + Janek -> maksymalne sygnały emocjonalne. Ardilla, kompromitujące sekrety.
    * Ex Z (znacie ją dobrze i macie Janka i wspólną historię) +4
    * VzXzXX: Kamil odciął, ale Daria odblokowała uprawnienia medyczne i biologiczne zanim zgasła
* 8 - Ardilla przekonuje pośrednio Czesława, że Kamil jest niebezpieczny.
    * Tr +3
    * XXV: Czesław odwrócił uwagę Kamila ale nie umiał strzelić; Kamil go zaatakował a Janek zdjął silnym ciosem.
* 9 - Ardilla wskakuje na rurę by ją przerwać i zrzucić przeciwnika a Janek + Eustachy to porażą. Celina zaczyna zbierać ładunek. Ardilla "jeden z naszych?"
    * Tr Z (zaskoczenie) +4
    * VzVVz: Ardilla strąciła ainshkera, Janek i Eustachy rozstrzelali prądem
* 10 - Ardilla próbuje opracować trasę - na bazie tego co słyszy i widzi. Wyskakuje, szczurzy, wraca, prowadzi. + wiedza Eustachego o korytarzach serwisowych.
    * Tr Z (wiedza Eustachego) +3
    * VzXVV: fragment po korytarzach serwisowych; napotkali osy, ale zastawiona pułapka z kurtek - spopielone. Potem comms, tam więcej os, Czesław użądlony ale osy spopielone kosmetykami Celiny.
* 11 - Eustachy oraz Ardilla próbują złożyć połączenie z Nativis używając kodów wujka. Agenci plagi nie są inteligentni, ale PLAGA jest. Dlatego atakuje m.in. w połączenia, komunikację itp.
    * TrZM+3+3O
    * XVXzX: podniesione pole magiczne, agent Trianai odciął ważne sekcje; trzeba osobiście. Agent Trianai wie, gdzie jest Eustachy, czuje pole magiczne

## Kto
### Arkologia Nativis

* Lycoris Kidiron, 57 lat (ze wspomaganiami drakolickimi dającymi jej efektywność 3x-latki) (objęta przez Wiki)
    * Wartości
        * TAK: Bezpieczeństwo, Osiągnięcia ("dzięki mnie ta arkologia będzie bezpieczna i osiągniemy najwyższy poziom eksportu żywności")
        * NIE: Konformizm ("nikt, kto próbował się dopasować do innych nie doprowadził do postępu")
    * Ocean: ENCAO: +-0+0 (optymistyczna, nieustraszona, lubi się dogadywać z innymi, nie patrzy na ryzyko)
    * Silniki:
        * TAK: Kapitan Ahab: polowanie na białego wieloryba do końca świata i jeszcze dalej, nieważne co przy tym stracimy. (tu: "zrobię najlepszą arkologię, lepszą dla ludzi niż planeta macierzysta")
    * Rola:
        * ekspert od bioinżynierii i terraformacji, pionier (zdolna do przeżycia w trudnym terenie)
* Rafał Kidiron
    * Wartości
        * TAK: Achievement, Power("wraz z sukcesami przychodzi władza, wraz z władzą musisz osiągać więcej sukcesów")
    * Ocean
        * ENCAO: 00+-0 (precyzyjny i dobrze wszystko ma zaplanowane, lubi się kłócić i nie zgadzać z innymi)
    * Silniki:
        * TAK: Duma i monument: to NASZE sukcesy są najlepsze i to co MY osiągnęliśmy jest najlepsze. Musimy wspierać NASZE dzieła i propagować ich chwałę.
    * Rola:
        * kuzyn Lycoris, security lord, w wiecznym konflikcie z Gurdaczem (security & money - beauty)
* Jarlow Gurdacz
    * Wartości
        * TAK: Hedonism, Face NIE: Conformism ("najpiękniejsza arkologia, klejnot Neikatis - i to nasza!")
    * Ocean
        * ENCAO: 000++ (kocha piękno i ma mnóstwo świetnych pomysłów)
    * Silniki:
        * TAK: Artystyczna dusza: Stworzyć piękno, pokazać je całemu światu lub wręcz przeciwnie, zachować dla godnej publiczności. Być docenionym.
    * Rola:
        * ekspert od roślinności, architektury i zdrowia arkologii, w wiecznym konflikcie z Rafałem Kidironem (security & money - beauty)

### CES Purdont

* Wiktor Turkalis
    * Ocean: ENCAO:  0+0-0
        * Prostacki, prymitywny, nie dba co myślą inni
        * Nieśmiały, skrępowany i często zażenowany
    * Wartości:
        * TAK: Humility, TAK: Family, TAK: Face, NIE: Achievement, NIE: Conformity
    * Silnik:
        * TAK: Do the right thing: niezależnie od tego co jest wygodne czy tanie, własne zasady i własny kompas moralny są najważniejsze. By spojrzeć w lustro.
        * TAK: Papa Smerf: zawsze doradza, daje dobre rady. Chce być pomocny i dać rady które naprawdę pomogą. Chce, by za jego radami KTOŚ poszedł i by było mu lepiej.
        * NIE: Tępić debili: nienawidzę głupoty do poziomu NUKLEARNEGO. Niech cierpią. Sama przyjemność. I zemsta.
    * Rola: przełożony 
* Kamil Wraczok
    * Ocean: ENCAO:  +0---
        * Nigdy się nie nudzi, lubi rutynę i powtarzalne rzeczy
        * Nie dba o potrzeby innych, tylko o swoje
        * Ryzykant, zawsze rzuca się w ogień
    * Wartości:
        * TAK: Self-direction, TAK: Face, TAK: Stimulation, NIE: Security, NIE: Conformity
    * Silnik:
        * TAK: Wędrowny mistrz Ryu: chcę być coraz lepszy w Czymś, chcę trudnych wyzwań, chcę się wykazać. There is always someone stronger.
        * TAK: Prowokator: podkręcanie atmosfery, by inni działali ostro pod stresem. Obserwowanie ich prawdziwej natury.
    * Rola: P.O. bazy, próbuje wszystko ukryć i naprawić sprawę samemu.
* Daria Triazis
    * Ocean: ENCAO:  -0+00
        * Polega tylko na sobie; nie wierzy w innych
        * Szczególnie rozsądny i poczytalny
    * Wartości:
        * TAK: Conformity, TAK: Hedonism, TAK: Self-direction, NIE: Universalism, NIE: Humility
    * Silnik:
        * TAK: Odnaleźć dziecko: znaleźć i uratować zaginioną osobę, która jest poza zasięgiem.
        * TAK: Wyplątać się z demonicznych długów: muszę jeść by przeżyć i wpakowałem się w ostre kłopoty z siłami, które się nie patyczkują. Spłacić lub uciec.
    * Rola: pracuje jako inżynier terraformacji; atm zarażona. Lertys jest w niej zakochany.

### Infernia

* Bartłomiej Korkoran, wuj i twarda łapka rządząca Infernią.
    * Ocean: ENCAO:  +-000
        * Bezkompromisowy, nieustępliwy, niemożliwy do zatrzymania
        * Ciekawski, wszystko chce wiedzieć i wszędzie wsadzić nos
    * Wartości:
        * TAK: Family, TAK: Self-direction, TAK: Achievement, NIE: Tradition, NIE: Humility
    * Silnik:
        * TAK: Inkwizytor: niszczenie fake news, publiczne pokazywanie hipokryzji, ujawnianie bolesnych prawd
        * TAK: Kolekcjoner: kolekcjonuje coś rzadkiego ()
    * Rola: wuj, kapitan Inferni
* Celina Lertys, podkochująca się w Eustachym 
    * Ocean: ENCAO:  --+-0
        * Stanowczy i silny, zdecydowany
        * Skryty; zachowujący swoją prywatność dla siebie
        * Nigdy nie wiadomo, co jest prawdą a co maską
        * Praworządny, uznaje prawo za najwyższą wartość
    * Wartości:
        * TAK: Universalism, TAK: Tradition, TAK: Security, NIE: Power, NIE: Humility
    * Silnik:
        * TAK: Harmonia naturalna: balans między technologią i naturą, utrzymanie cudów natury. Lasy Amazonii.
        * NIE: Awans za wszelką cenę: albo lepsza firma albo wojna o stanowisko z innymi. To stanowisko będzie moje.
    * Rola: science / bio officer + medical. Młoda podkochująca się w Eustachym :D. Złota, płonąca skóra z błękitnymi włosami, może robić wyładowania
* Jan Lertys
    * Ocean: ENCAO: -0-0+
        * Zapominalski; non stop trzeba coś przypomnieć
        * Obserwator; raczej stoi z boku i patrzy niż działa bezpośrednio
        * Łatwo wywrzeć na tej osobie wrażenie
    * Wartości:
        * TAK: Humility, TAK: Universalism, TAK: Self-direction, NIE: Achievement, NIE: Security
    * Silnik:
        * TAK: Pokój: Kapuleti i Monteki MUSZĄ żyć w pokoju. Unikajmy starć, idźmy w pacyfizm. Wygaszanie konfliktów, harmonia.
        * TAK: Ratunek przed chorą miłością: TYLKO NIE Celina -> Eustachy. Pls.
    * Rola: First Mate (siłowy / ładownia); chce zejść do bazy bo tu pracuje jego ukochana Daria
* Emilia d'Infernia
    * "żona" Bartłomieja. Żywy arsenał. Egzekutor Bartłomieja. (Emilia -> "Egzekucja MILItarnA")

## Układ arkologii Nativis

* Aspekty:
    * Bardzo dużo roślin, drzew itp. Piękne roślinne miejsce.
    * Bardzo spokojna arkologia, silnie stechnicyzowana i sprawna.
    * Dominanta: faeril > drakolici, ale próba balansowania. Niskie stężenie AI.
    * BIA jako centralna jednostka dowodząca, imieniem "Prometeus"
* Struktura:
    * Wielkość: 4.74 km kwadratowych, trzy poziomy
    * Populacja: 11900 (Stroszek)
        * Operations & Administration: 1100
        * Extraction & Manufacturing: 3213
        * Medical: 355
        * Science: 360
        * Maintenance: 700
        * Leisure: 1000
        * Social Services: 240
        * Transportation: 800
        * Training: 229
        * Life Support: 500
        * Computer & TAI & Communications: 500
        * Engineering: 500
        * Supply / Food: 1200
        * Security: 300
    * Bogactwo: wysokie. Technologia + piękna roślinność.
    * Eksport: żywność (przede wszystkim), komponenty konstrukcyjne, hightech, terraformacja
    * Import: luxuries, prime materials, water, terraforming devices, weapons
    * Manufacturing: jedzenie, konstrukcja, terraformacja
    * Kultura: 
        * dominanta: piękno, duma z tego co osiągnęli, duma z tego że nie ma wojny domowej faeril/drakolici
        * wyjątek: brak.
    * Przestrzenie
        * rozrywki: parki, natura, zdrowe ciało - zdrowy duch
        * inne: hotele, laboratoria terraformacyjne, restauracje...
    * Zasilanie
        * solar + microfusion
    * Żywność
        * różnorodna; to miejsce jest źródłem żywności
        * przede wszystkim roślinna, ale też zawiera elementy zwierzęce (te najczęściej z importu)
    * Life support => terraformacja + filtracja
    * Comms => biomechaniczne okablowanie + normalne sieci
    * TAI => BIA wysokiego stopnia imieniem Prometeus
    * Starport => large
    * Infrastructure => high-tech, dużo półautonomicznych TAI, wszystko nadrzędne przez Prometeusa. Świetny transport publiczny. 
    * Access & Security => arkologia z silnymi blast walls, silnymi liniami dostępowymi.

Historia: 

* w trakcie wojny domowej sprzedawała żywność na 2 fronty co spowodowało znaczne bogactwo ale też częściową niechęć innych arkologii
* obecnie prowadzi ekspansje na struktury zrujnowane/opuszczone po wojnie żeby całkowicie się uniezależnić od dostaw z poza planety

---
layout: cybermagic-konspekt
title: "Spokojne wakacje na wsi"
threads: 
gm: kić
players: random
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

### Chronologiczna


## Punkt zerowy

### Postacie
* Barry Nobel - {gracz} były szpieg dla agencji detektywistycznej, brat Vladimira
* Vladimir Nobel - {gracz} były szpieg dla agencji detektywistycznej, brat Barr'ego
* Robert Satrochein - {gracz} bibliotekarz
* Izabela Raczkowska - {gracz} 
* Kasia - przyjaciółka czwórki bohaterów, prześladowana przez stalkera
* Joachim Czyżyk - zmarły w wypadku młody mąż Renaty. Wrócił i jej szuka.
* Renata Czyżyk - zmarła w tym samym wypadku żona Joachima.

## Misja właściwa

### Scena zero
Kasia mieszka w Orłowie. Kasia ukrywa się w domu, bo na zewnątrz czeka on. Robert. Kiedyś był jej chłopakiem, ale ona go rzuciła bo był brutalny. On jednak się z tym nie pogodził i nie daje jej spokoju, wysyłając sms-y, zaczepiając, pukając do drzwi.
I ZNÓW tam jest. Kasia szukała pomocy u zaprzyjaźnionej starszej pani. Wzywanie policji nic nie da, bo on wie, jak zniknąć. Już to robiła. Przyjaciółka mamy ma dla niej sugestię. Może Kasia wyjedzie na chwilę? Może jak jej nie będzie w pobliżu, znajdzie sobie inną dziewczynę..?
Na szczęście przyjaciele Kasi akurat pojechali na wakacje do wsi Gniazdowo. Postanowiła więc pojechać do nich.
Udało się jej wyślizgnąć i ruszyła w drogę.
Chwilę po tym, jak minęła tabliczkę znaczącą początek Orłowa, usłyszała z siedzienia pasażera męski głos.
"Renata?"

### Sesja
Przyjaciele spokojnie siedzą sobie w wynajętym pokoju w agroturystyce, kiedy nagle dochodzi do nich potężna emisja energii magicznej.
Biegiem udali się więc w miejsce, z którego doszedł sygnał.
Znajdują tam owinięty wokół drzewa samochód Kasi i samą Kasię, śmiertelnie przerażoną, ale bez najmniejszego zadrapania.
Od Kasi czuć silnie napromieniowanie energią magiczną.

Wypytali ją co się stało i usłyszeli, że uciekła do nich przed stalkerem, a już w wiosce usłyszała obok siebie męski głos pytający o Renatę i się wystraszyła. Nie do końca miało to dla nich sens, ale może Kasia jest w szoku...

Uśpili koleżankę (nie jest magiem) i poszli szukać, co się stało.

Wrócili na miejsce wypadku, gdzie zastali dwóch policjantów przyglądających się sytuacji. Sprawili, że zapomnieli o Kasi i zamaskowali numery rejestracyjne tak, aby Kasia nie miała problemów.
I na chwilę uśpili policjantów.
To pozwoliło im zrobić badania. Energia szybko się rozpraszała, ale dowiedzieli się, że to dokładnie ta, którą wyczuwali od Kasi, i że to właśnie ta energia uratowała jej życie. No dobrze... ale również była przyczyną wypadku.

Nie mając pomysłu, co się stało, postanowili poszukać we wsi.
Izabela znalzazła lokalną plotkarę, która natychmiast opowiedziała jej o tym, że źle się dzieje w wiosce, że jest lokalna wiedźma i ona rzuca uroki i że to na pewno jej wina, i że to straszna historia z rodziną Czyżyków bo najpierw zginęli młodzi, nie, oni tu nie mieszkali, tylko przyjeżdali do jego rodziców i po ich śmierci jego ojciec był okropnie smutny, aż zmarł jakieś pół roku temu i to było dziwne, bo się uśmiechał!
A poza tym, ten zakręt jest strasznie niebezpieczny i ludzie się tam rozbijają co jakiś czas.
W tym czasie, Robert szukał w lokalnej bibliotece i w sieci. Znalazł nazwisko Renaty i imię Joachim. Zlokalizował też adres rodziny Czyżyków.
Bracia Nobel, mając na bieżąco relację od Izabeli, postanowili przeszukać mieszkanie pani Czyżyk.
Barry zadzwonił do drzwi, sprzedając historię o tym, że był przyjacielem Joachima, ale wyjechał i nic nie wiedział, a teraz wrócił i szuka próbując zrozumieć, jak to... W tym czasie Vladimir wślizguje się do mieszkania i robi przeszukanie.

Znaleźli potwierdzenie historii - wszystko się zgadzało. Matka Joachima opowiedziała, że jej mąż nie mógł się pogodzić z jego śmiercią i często chodził na grzyby w okolice zakrętu, aż w końcu serce nie wytrzymało. 
Przyjaciołom wszystko się złożyło do kupy. 
Zginęło młode małżeństwo, ale ojciec pana młodego nie mógł się pogodzić ze stratą. Wszystko było w porządku do około pół roku temu, gdzy jakiś przypływ energii magicznej odpowiedział na pragnienie ojca i przywrócił jego syna, jednak kosztem życia ojca. Ojciec zmarł szczęśliwy, że wreszcie odnalazł synka... Syn powrócił jako echo, pamiętające jedynie, że musi odnaleźć i chronić Renatę.
Dlatego pojawiał się na zakręcie, szczególnie obok dziewczyn podobnych do Renaty. Nigdy jednak nie zamierzał nikogo krzywdzić. No chyba, że w obronie dziewczyny.

Przyjaciele wezwali ducha (wykorzystując osobiste przedmioty Joachima skradzione z jego domu) i wykorzystali odpowiednio ucharakteryzowaną Kasię, aby duch mógł uznać, że w końcu odnalazł Renatę.

Kiedy jego powód istnienia zniknął, duch się rozproszył.












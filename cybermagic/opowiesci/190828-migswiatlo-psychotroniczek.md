---
layout: cybermagic-konspekt
title: "Migświatło psychotroniczek"
threads: nemesis-pieknotki
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [190202 - Czarodziejka z woli Saitaera](190202-czarodziejka-z-woli-saitaera)

### Chronologiczna

* [190202 - Czarodziejka z woli Saitaera](190202-czarodziejka-z-woli-saitaera)

## Budowa sesji

### Stan aktualny

* .

### Pytania

1. .

### Wizja

brak

### Tory

* .

Sceny:

* .

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Misja właściwa

Pięknotka dostała sygnał SOS z Trzęsawiska. Talia. Ona nigdy wcześniej nie chodziła na Trzęsawisko... the meow? Pięknotka jest bliżej niż Alan - leci na pomoc. Szuka Talii (Tp:S) - udało jej się, zanim coś szczególnie groźnego zaczęło ją podjadać. Starsza czarodziejka siedzi na drzewie. Lekki stealth suit; porusza się w nim nieporadnie. Talia powiedziała Pięknotce, że chce ją ostrzec. Na wolności jest BIA klasy Szponowiec; tier 1. Skąd Talia wie? Sama ją naprawiła; gość od Grzymościa sobie tego życzył a bez Kajrata nikt nie chroni starych niebezpiecznych noktiańskich technologii.

Kajrat, that bastard, jest egzekutorem. Jak jego nie ma, sprawa się rozsypuje.

Talia powiedziała, że Marek Puszczok zażądał od niej dospecjalizowania BII pod kątem walki w power suit. Czyżby robił androida bojowego? Tak czy inaczej, Talia podzieliła się wszystkimi informacjami jakie miała (a przynajmniej tak mówi). Pięknotka nie ma powodu jej nie wierzyć...

Pięknotka wyprowadziła Talię. Psychotroniczka udała się do swojego domku a Pięknotka została z problemem...

Pięknotka zakłada stealth suit i zabiera się do poszukiwań BII. Śledzi Puszczoka. Gość jest thugiem; można powiedzieć, żołnierz Grzymościa. Ani specjalnie ważny, ani specjalnie mądry. Nie miał pomysłu na życie a Grzymość daje mu łatwe życie. Pięknotka szuka (TrZ: SSZ). Znalazła BIĘ, ale została wykryta przez ludzi Grzymościa. Oni myślą, że Pięknotka poluje na zupełnie inny temat, a tak w ogóle to jest zabójczynią. Oczywiście, Pięknotka wieje jak próbują ją złapać. (TrZ: SZ). Udało się Pięknotce, ale stealth suit w strzępach.

Rozmowa z Arturem Michasiewiczem, kwatermistrzem Barbakanu. Artur opieprza Pięknotkę - do jakiego ticketu jest przypisana jej prywata? Pięknotka odpowiada na spokojnie - sprawa z Kajratem. Michasiewicz mówi, że ticket jest zamknięty. Pięknotka - "be vigilant". (Tp:SS). Pięknotka skończyła dokumentując ogromną ilość rzeczy i płacąc niewielką karę (token, nie dotkliwa) - ale jej się znowu upiecze. Zbyt często po prostu jej się to zdarza.

No i Pięknotka wie, gdzie jest BIA. BIA została zabrana do technoklubu, w Zaczęstwie jest nie-do-końca-nielegalny klub walk TAI, miejsce walk gladiatorów w vircie. Nazywa się to "Arena Migświatła". Tam są TAI i tam jest też ta nieszczęsna BIA. Kontrolują holograficzne mechy. Nagrody są oczywiście na pieniądze.

Pięknotka szybko sprawdziła - the guy is known for gambling. Na razie wszystko wskazuje na to, że to jest niezbyt inteligentny Astorianin, który dostał w swoje ręce wyjątkowo niebezpieczną broń i używa jej do otwierania konserw. Ale to co jest ciekawego - BIA wygrała w 2 z 5 rund z championem. Champion to TAI o imieniu "Samotnik". To TAI jest zdolne do pokonania BII. To TAI ko-ewoluuje z BIĄ. I to jest niebezpieczne oraz ciekawe; bo to znaczy, że mówimy o military-grade TAI.

Pięknotka coraz mniej rozumie tą sytuację. Czas odwiedzić Minerwę. To się robi coraz dziwniejsze. Minerwa przywitała ją z lekką rezerwą (jak to Minerwa), ale z wystarczającym jak na nią ciepłem.

Pięknotka mówi Minerwie o tej arenie. Minerwa - co Pięknotka od razu zauważyła - coś ukrywa. (Tp: SS). Minerwa powiedziała Pięknotce, niechętnie, że to ona stoi za TAI Samotnika. She made it, made it invincible. To jest jej TAI i chce zobaczyć czy ktokolwiek jest w stanie kiedykolwiek go pokonać. Na razie ku jej wielkiemu zdziwieniu pojawiło się tam drugie military-grade - "Krew Czaszki". Pięknotka usłyszawszy imię, facepalmowała. Minerwa jest ABSOLUTNIE przeciwna wyłączeniu Samotnika - widzi to wszystko jako okazję jak może sobie dorabiać; przez używanie TAI w vircie.

Pięknotce by to nie przeszkadzało gdyby nie cholerny ixion...

Oki. Pięknotka wtajemniczyła Minerwę w problem "BIA". Wpadły na pomysł - zatrujmy BIĘ. W odróżnieniu od innych Minerwa wie jak to zrobić. (Tp:P). I faktycznie, zadziałało - ale BIA eksplodowała. To nie było zwykłe zatrucie które wygasza. To było zatrucie, które wysadziło BIĘ. Było to głośne, nieprzyjemne i odbiło się w vircie przez pewien czas. Osoby się znające wiedzą, że tam była BIA. Tymon pojawił się sprzątać, było oficjalne śledztwo Tymona... ogólnie, nędza.

A Pięknotka jest uśmiechnięta. Rozwiązała problem.

A Tymon wygasił Samotnika gdy tylko dowiedział się, że Minerwa za tym stoi. Minerwa była nieszczęśliwa i zła, ale Tymon nie ustąpił. Jeszcze nie.

**Sprawdzenie Torów** ():

* .

**Epilog**:

* .

## Streszczenie

Gdy Kajrat jest w więzieniu, nikt nie kontroluje przepływu technologii noktiańskiej. Talia ostrzegła Pięknotkę, że do holo-walki AI dodała subturingowe BIA klasy Szponowiec, z żądania gościa od Grzymościa. Okazało się, że to niegroźny hazard jest - ale są tam dwie dziwne walczące AI: BIA oraz militarna, stworzona przez Minerwę. Pięknotka chciała tylko zabić BIA, ale przypadkowo z Minerwą wysadziły Arenę Migświatła. Tymon zabrał Minerwie jej TAI. Powiedział, że jeszcze nie czas by takie rzeczy robiła.

## Progresja

* .

### Frakcji

* .

## Zasługi

* Pięknotka Diakon: wezwana przez Talię do rozwiązania problemu z BIA. Znalazła winną (Minerwę) i drugiego winnego (Puszczoka). Z Minerwą wysadziła Migświatło; nie jest z tego szczególnie dumna.
* Talia Aegis: przyszła do Pięknotki bo nie chce by subturingowa BIA którą musiała zbudować dla Puszczoka stała się zagrożeniem.
* Minerwa Metalia: stworzyła własne TAI do walki na arenie migświatła w Zaczęstwie; w ten sposób się z przyjemnością realizuje. Zabrali jej TAI bo boją się energii ixiońskiej.
* Marek Puszczok: kocha hazard; zmusił Talię by ta naprawiła dla niego BIA klasy Szponowiec. Trochę wygrywał, ale potem wszystko się skończyło (bo Pięknotka to wysadziła).
* Artur Michasiewicz: kwatermistrz Pustogorskiego Barbakanu; Pięknotka mu podpadła bo znowu rozwaliła power suit. Na szczęście skończyło się tylko na serii form i dokumentów.
* Tymon Grubosz: terminus - technomanta i katalista kontrolujący Zaczęstwo. Tym razem po prostu uznał, że Minerwa nie może budować TAI na rynek - za duże ryzyko ixionu.
* Ernest Kajrat: nadal w więzieniu; okazuje się, że pełni kluczową rolę neutralizatora przepływu artefaktów noktiańskich. Dzięki niemu nie ma dziwnych anomalii. Teraz go nie ma.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Zaczęstwo
                                1. Arena Migświatła: nielegalny-ale-nie-do-końca kompleks walk TAI; częściowo wysadzony (przypadkiem) przez Pięknotkę i Minerwę. Ale to był przypadek.

## Czas

* Opóźnienie: 3
* Dni: 2

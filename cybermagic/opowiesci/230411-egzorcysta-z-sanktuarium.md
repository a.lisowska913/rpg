---
layout: cybermagic-konspekt
title: "Egzorcysta z Sanktuarium"
threads: szamani-rodu-samszar, waśnie-samszar-verlen, echa-wojny-noktiańskiej, sanktuarium-kazitan
gm: żółw
players: kić, anadia, kolega-kamil
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [230404 - Wszystkie duchy Siewczyna](230404-wszystkie-duchy-siewczyna)

### Chronologiczna

* [230404 - Wszystkie duchy Siewczyna](230404-wszystkie-duchy-siewczyna)

## Plan sesji
### Theme & Vision & Dilemma

* PIOSENKA: 
* CEL: ruch graczy -> działanie na świecie, konsekwencje; cierpienie jest względne
    * Egzorcysta chciał dobrze a nie miał skilli, bał się tienowatych WIĘC tienowaci go zniszczą i zabiją
    * Egzorcysta uratował miasteczko. Teraz 

### Co się stało i co wiemy

* Kontekst
    * Egzorcysta Irek Kraczownik nie dał rady pokonać dziwnego ducha w innym mieście, więc przyniósł go do Siewczyna i doprowadił do katastrofy.
        * Samszarowie pokonają ducha śmiercią Irka
    * Irek działa w Sanktuarium Kazitan, w Przelotyku

### Co się stanie (what will happen)

* S01
    * Roil - problem ze zrujnowanym miastem, jak się tu dostać
    * Człowiek-potwór i płaczące dziecko odciągane przez innego człowieka

### Sukces graczy (when you win)

* rozwiązanie problemu

## Sesja - analiza

### Fiszki

* Elena Samszar: czarodziejka origami i kinezy, <-- objęta przez Anadię
    * Archiwistka, biurokracja, prawo i dokumenty
    * Jej papier potrafi zrobić krzywdę
    * W wolnym czasie robię rzeczy z papieru, wieże, łabędzie itp.
    * A teraz, ku wielkiemu smutkowi, pomagam stanąć na nogi kuzynowi Karolinusowi >.>
* .Jacek Miriakiewicz
    * ENCAO: ++--0 | Ekstrawertyczny, otwarty, kreatywny;; Nieco egoistyczny, rywalizujący | Samorealizacja, Przyjemność > Tradycja
    * Job: Projektant mody
    * Deed: Gdy brał udział w konkursie, stworzył odważne i oryginalne projekty, które przyciągnęły uwagę jury, ale zignorował współpracę z innymi uczestnikami
    * Deed: Kiedy Sanktuarium Kazitan było zagrożone, zamiast opuścić miasto, stworzył innowacyjne, wysokotechnologiczne ubrania, które pomagały ludziom przetrwać w trudnych warunkach
    * Deed: Podczas imprezy towarzyskiej, skupił się na zabawie i impresjonowaniu innych, zamiast pomagać potrzebującym lub angażować się w rozmowy o problemach miasta
* Tadeusz Dzwańczak
    * ENCAO: -++0- | Analityczny, zorganizowany, introwertyczny, uparty;; Osoba skupiona na celach i wynikach | Bezpieczeństwo, Samorealizacja > Przyjemność
    * MovieActor: Sherlock Holmes
    * Job: Detektyw (kiedyś) i strażak (teraz) Sanktuarium Kazitan
    * Deed: Gdy stawił czoła niebezpiecznemu przestępcy, ujął go dzięki swojej wiedzy i umiejętnościom analitycznym, zamiast ryzykować konfrontację fizyczną
    * Deed: Kiedy znajomy poprosił go o radę w sprawie osobistej, zamiast udzielić wsparcia emocjonalnego, podszedł do sprawy racjonalnie i zaproponował logiczne rozwiązanie
* .Maja Pirateczka
    * ENCAO: 0--++ | Wrażliwa, empatyczna, współczująca;; Skoncentrowana na potrzebach innych | Uniwersalizm, Benevolencja > Moc
    * MovieActor: Marmee March z "Małych kobiet"
    * Job: Pracownik organizacji charytatywnej
    * Deed: Gdy zauważyła, że bezdomny potrzebuje pomocy, zamiast ignorować go, kupiła mu jedzenie i zaoferowała wsparcie
    * Deed: Pomagała ofiarom w Sanktuarium Kazitan, zamiast uciekać i dbać tylko o swoje bezpieczeństwo
    * Deed: Kiedy koleżanka miała problem w pracy, zamiast skupić się na własnych zadaniach, poświęciła czas, aby jej pomóc i okazać wsparcie emocjonalne
* .Paweł Różycznik
    * ENCAO: 0++-0 | Optymistyczny, towarzyski, energiczny;; Czasami nieodpowiedzialny, impulsywny | Przyjemność, Stymulacja > Bezpieczeństwo
    * Job: Pilot wirolota
    * Deed: Gdy przyjaciel był przygnębiony, zorganizował spontaniczną wycieczkę swoim wirolotem, aby podnieść na duchu, zamiast rozmawiać o problemach
    * Deed: Kiedy Sanktuarium Kazitan było atakowane przez potwory, zamiast ukrywać się, użył swojego wirolota, aby ewakuować ludzi z zagrożonych obszarów

### Scena Zero - impl

.

### Sesja Właściwa - impl

W ciągu 3 dni udało się zlokalizować egzorcystę Irka. Znajduje się w mieście o pięknym nazwie Sanktuarium Kazitan. Trzeba go porwać...

Strzało - lecicie nad terenem gdzie POWINNO być Sanktuarium ale go tam nie ma. Zwiadowcy / szpiedzy mówili że to osada podziemna, ale nie ma wejścia tam gdzie powinna być. Strzała skanuje sygnatury cieplne.

Tr Z (skanery drony militarnej) +2:

* XzX: ROIL.

Coś jest nie tak. Masz sygnatury cieplne, pasują, ale gdy się zbliżasz to zostajesz zaatakowana. Przez... wulkan? Strzała unika.

* V: Strzała UNIKA OBRAŻEŃ ale magowie dobrze że mają pasy. Lekki blackout, ból żeber.
    * Skażenie magiczne, nie mag.
    * To Skażenie nie jest sterowane i nie "wie". To reakcja efemeryczna magii
* Strzała robi kilka manewrów dających jej szansę na odkrycie drogi, chce tak powabić by się odsłoniło. (+1Vg)
    * Vz: Karolinus ani Elena nie mają choroby morskiej. Dobra wiadomość.
        * Bo wlot - osłaniany przez rozprężniki - się przesunął.
* Vz: Strzała leci w długim, osłanianym tunelu. Dociera do miasta.
* X: Strzała jest zmuszona do manewrowania. Jest za dużo tych kolców, tych rzeczy. Strzała MUSI lądować i to dość solidnie. Solidnie porysowany lakier.
    * Tymczasowy szok sensorów. Tymczasowy stan wyciszony / wyłączony.

Karolinus i Elena słyszą krzyk dziecka. Niedaleko w alejce dziecko jest porywane przez kogoś, dziecko chciało biec / iść do odchodzącego kolesia. Karolinus krzyczy "stać, uwaga, porywa dziecko" i leci za kolesiem. Ale manewruje ostrożnie. Strzała monitoruje Karolinusa i okolice Karolinusa. Karolinus chce by porywacz nie mógł biegać tylko czołgać.

TpM+3+5Ob:

* V: Karolinusowi czar wyszedł. 
    * Porywacz upuścił dzieciaka i się przewrócił, wydając cichy dźwięk. Próbuje pełznąć za dzieciakiem "stój! wracaj!". Dzieciak biegnie za odchodzącym "TATO!" Ojciec nadal nie zauważa / ignoruje dziecko.
        * "Ojciec" jest oddalony za taką mgiełką / dymem, trudno dostrzec.
        * Porywacz jest bliżej magów niż dziecko, dziecko bliżej niż ojciec.
* V: Karolinus się wzmocnił. Jest silniejszy, szybszy i ma 'ciało jak z bajki'.

Elena ignoruje dziecko i idzie w stronę porywacza. Karolinus biegnie za dzieckiem, złapał dziecko. Chłopak się wyrywa "TATO!" Ojciec jest 3-5 metrów dalej. Nawet się nie odwrócił. Dalej idzie w stronę "placu". Karolinus podbiega do ojca i łapie go za ramię. Poczułeś...  coś pomiędzy pumeksoidalnego. Ojciec się odwrócił. Element wody, ziemi i człowiek. 

* V: Ojciec próbuje zalać wodą z oczu Karolinusa i dziecko, ale Karolinus skutecznie się wywinął i spieprzył w długą.

Elena widzi, jak "porywacz" zaczyna cicho "jaki on jest odważny, to szaleniec!" widząc Karolinusa w akcji. "Czy to mag?" Elena: "ma znaczenie?" Porywacz: "jest tylko jeden mag który nam pomaga, oczywiście że to ma znaczenie, przetrwamy czerwony alarm!"

Tr Z (Karolinus) +3 (okoliczności):

* Vz: Elena, wbrew sobie i z zaciśniętymi zębami, przesunęła "Karolinus jest prawdziwym cholernym bohaterem".
    * "porywacz" Tadeusz jest ambasadorem Karolinusa.
* Vr: Tadeusz (strażak) szybko naświetlił kontekst:
    * jest jedna z gorszych Emisji.
        * Czerwony Alarm jest alarmem odnośnie Emisji. Ta jest gorsza:
            * przesunęło się wejście
            * są uziemieni, nie da się uciec
    * element Emisji - mgła zmienia ludzi w potwory. 
        * Jest odwracalne, mają w mieście maga który im pomaga, jest egzorcystą - Irek
            * on jest tu już rok i dzięki niemu dużo udało się uratować
        * Jeśli człowiek nie był pod mgłą ponad 2h, Irek jest w stanie go uratować. Ponad 2h - nie jest.
    * W mieście jest jeden mag który próbuje im pomóc. Jeden. Nie ma dwóch. Tylko Irek został i pomaga.
        * to znaczy, że jeśli Wy macie dwójkę magów i jedną ŁZę to Irek nie ma szans.
    * "przynajmniej jesteście dobrymi magami, nie tym gównem z Aurum"
    * większość ludzi jest OK; części nie udało się uratować, na przestrzeni lat populacja maleje. Irek daje nadzieję. Tak jak wcześniej Arnold Kazitan. Wielki założyciel i dobry mag.
* V: Elena coś kojarzy o Arnoldzie.
    * Arnold Kazitan był czarną owcą rodu. Ale w sumie nie wiadomo.
    * Arnold nalegał, by Aurum zrobili porządek z Lemurczakami. By nie pozwolić im na samowolę, zabawę, znęcanie się itp.
    * Arnold nic nie mogąc osiągnąć nawoływał, by Kazitanowie opuścili sentisieć. By zniszczyć swoje ziemie. Nie znalazł zainteresowania.
    * Arnold opuścił swój ród, zostawił, zniknął z Aurum. Wiadomo, że Arnold chciał pomóc uchodźcom, osobom, które mają przechlapane, anarchistom. "Zrobimy własne ziemie".
    * ...że TUTAJ Arnold Kazitan znalazł dom. Stworzył to miasto.
    * Tadeusz uzupełnia: "Arnold Kazitan zginął. Ale jego potomkowie dalej tu są. Niestety, nie są magami."
    * "Tak, bywali tu magowie Aurum i okrutnie się bawili. Mamy ich nazwiska w Wielkiej Księdze Aurum".

Karolinus, Elena i Strzała trafiły do Dystryktu Szafirowego - miejsca ze skrajną redundancją, bardzo odpornego. Ten dystrykt jest w dosyć dobrym stanie. Oki, są pęknięcia, dziury, ale są zdobienia na ścianach. Karolinus i Elena są zaprowadzeni do Irka.

Irek jest zmęczony. Widzicie spracowanego, zmęczonego maga który jest na granicy energii, ma ślady Skażenia. Jego visiat jest uszkodzony. W mieście jest 5-10k ludzi, jeszcze jakoś dają radę. Irek się zajeżdża by móc ich ratować.

Karolinus i Elena nie do końca dbają o tych ludzi. Elena ma pewne wątpliwości, ale pozwoli Karolinusowi działać. Karolinus korzystając z tego że jest bohaterem mówi Irkowi "tam jest człowiek on bardzo potrzebuje Twojej pomocy natychmiast!"

Tp Z +2 +3Og (wsparcie dobrych ludzi miasta):

* V: Irek wyszedł z Karolinusem i Eleną, "przynieście mi go"
* Og: Irek, Karolinus i Elena mają eskortę - czterech uzbrojonych ludzi, lokalsów. Broń może ranić potwory, może uszkodzić Strzałę.
    * Strzała ostrzega Karolinusa i Elenę i wypuszcza potężny atak soniczny jak co do czego dojdzie.
* Og: Większość uzbrojonych osób będzie skupiona na Irku bo nieznani magowie MIMO że są pozytywni.
* Vr: Uda się porwać Irka, wsadzić do Strzały i lecieć.

Strzała ma ciekawą sytuację. Nie chce ranić tych ludzi ani niszczyć, ale ma wytyczne i rozkazy. Ogień zaporowy, unika krzywdzenia lokalsów ale to akceptowalne jeśli bez tego będzie zbyt ciężko uszkodzona. Ogień zaporowy ma być skuteczny, tuż przy palcach. Karolinus motywuje: "nie spieprz tego".

* TrZ+3 (specjalny): 4Xr(straty lokalsów)4Xg(straty Strzały)+4Vr(straty lokalsów)+5Vw(pełen sukces)+Z(firepower):

* Vz: ludzie próbowali strzelać do Strzały która się ewakuuje, ale Strzała po prostu otworzyła ogień. Jeden z dystryktów (straconych) po prostu zbombardowała. Szok & horror.
* Xg: lokalsi są dzielni. Strzała jest uszkodzona. Doleci do celu, ale wymaga naprawy. Pancerz wytrzymał, ale to wszystko. Część sensorów poszła.
* V: Strzała wymanewrowała próbując nie krzywdzić. Potwory zbliżają się do lokalsów, ale oni rozpaczliwie próbują uratować swojego maga.
* Xg: Strzała nie doleci do domu. Uszkodzenia zbyt poważne. Strzała musi nacisnąć ponad bezpieczne parametry. Pociski przekroczyły barierę "safe".
    * uruchamia się protokół ratowania pasażerów się włącza. (Xg -> Xr). Strzała AKTYWNIE działa przeciwko ludziom by nic jej się nie stało.
* Vr: Strzała, oślepiona (częściowo) i bardzo uszkodzona, z wyciekającym irianium, uniknęła Anomalii. Nie da rady wrócić do domu w Aurum, ale da radę wezwać pomoc.
* Vr: Strzała koordynuje swoją energię, swoje elementy i będzie crashlandować w miejscu w miarę bezpiecznym. W bezpiecznej części Przelotyka. Nie w tych bardziej anomalnych fragmentach.

Strzała doleci do Przelotyka. Ale jest zbyt uszkodzona, by dolecieć bezpiecznie.

## Streszczenie

Egzorcysta Irek poszukiwany przez Samszarów jest w Sanktuarium Kazitan w Przelotyku Zachodnim Dzikim. Na miejscu Sanktuarium ulega Emisji - katastroficzne elementalne działania. Zespół ratuje dzieciaka od Emisji (najpierw mu zagrażając XD), ale gdy dociera do egzorcysty - ostatniego maga który próbuje pomóc Sanktuarium, to go porywają. Przy próbie porwania Strzała zostaje ciężko uszkodzona i nie daje rady wrócić do Powiatu Samszar - crashlanduje w bezpiecznej części Przelotyka. 

## Progresja

* AJA Szybka Strzała: bardzo ciężko uszkodzona, wymaga sporej naprawy. Doprowadziła jednak wszystkich poza Sanktuarium Kazitan i skutecznie porwała Irka.
* Karolinus Samszar: przez moment był bohaterem Sanktuarium Kazitan, ale teraz jest tam traktowany jak najgorszy z najgorszych po porwaniu Irka.

### Frakcji

* .

## Zasługi

* Karolinus Samszar: wpierw magią unieszkodliwił Tadeusza strażaka myśląc, że to porywacz a potem powerupował się by ratować dziecko i został lokalnym bohaterem na moment - tylko po to, by wyeksploatować tą inwestycję w reputację i porwać Irka. Skupiony na celu i wystarczająco bezwzględny wobec miasta.
* Elena Samszar: nie jest zainteresowana pomaganiem dziecku, ale nie chce niszczyć Sanktuarium. Jednak sprawiedliwość i "swoi ludzie" muszą być uratowani. Skonfliktowana, pozwala Karolinusowi porwać Irka. Bardziej pasywna rola, nie wie co robić w zastałej sytuacji.
* AJA Szybka Strzała: Sanktuarium Kazitan podczas Emisji jest miejscem na granicy możliwości Strzały; została uszkodzona, ale skutecznie tam Zespół doprowadziła. Jednak potem, gdy Zespół uciekał z porwanym Irkiem z Sanktuarium, Strzała tak starała się żadnego cywila nie zastrzelić że została ciężko uszkodzona (pokazując, że jest najbardziej "ludzka i humanitarna" z całego Zespołu). Ale doprowadziła wszystkich bezpiecznie do Przelotyka i wylądowała w ruinie.
* Arnold Kazitan: (nieobecny, najpewniej KIA); technomanta i wizjoner, buntownik przeciw Aurum i sentisieci. Założył Sanktuarium Kazitan na terenie Przelotyka, walcząc z anomaliami. Kiedyś nalegał, by Aurum zrobili porządek z Lemurczakami. By nie pozwolić im na samowolę, zabawę, znęcanie się itp. Ale nic nie osiągnął, Kazitanowie nie opuścili sentisieci, więc on opuścił ród.
* Tadeusz Dzwańczak: uciekinier z Aurum i mieszkaniec Sanktuarium Kazitan; jako strażak próbował uratować dziecko przed Emisją która pochłonęła jego ojca (i przekształciło go w hybrydowego żywiołaka). Karolinus magią mu to uniemożliwił, potem sam uratował owo dziecko. Tadeusz zaprowadził Karolinusa do Irka, jedynego maga jaki został w Sanktuarium - i tego żałuje.
* Irek Kraczownik: kiedyś egzorcysta, teraz zabija się i oddaje swoją energię by tylko utrzymać Sanktuarium (swój nowy dom) przy życiu. Porwany przez Karolinusa Samszara przy biernym współudziale Eleny Samszar.

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Przelotyk
                        1. Przelotyk Zachodni Dziki
                            1. Sanktuarium Kazitan: schowane i zakamuflowane podziemne miasto z wieloma rozłącznymi i niezależnymi dystryktami (miasto rozproszone i niezależne). Pod wpływem strasznej Emisji.
                                1. Dystrykt Szafir
                                    1. Komnata mieszkalna
                                    1. Komnata lecznicza

## Czas

* Opóźnienie: 1
* Dni: 2

## OTHER
### Fakt Lokalizacji
#### Miasto Sanktuarium Kazitan

* Sanktuarium Kazitan:
    * energia: redundancja
        * główne: geotermalne, rozproszone po różnych miejscach, żywa magitrownia puryfikacyjna - dar rodu Kazitan
        * każda Baza zawiera własne baterie irianium 
    * mieszkanie: Bazy (wzorowane na weyrach, wewnętrzne jaskinie)
        * Duże, przestronne komnaty wykute wewnątrz masywu górskiego, które mieszczą całe rodziny lub grupy osób. 
        * Komnaty te są częściowo naturalne, a częściowo rzeźbione przez mieszkańców, co nadaje im niepowtarzalny charakter. 
        * Wykorzystują one również naturalne cechy geologiczne, takie jak jaskinie i szczeliny, aby stworzyć unikalne i funkcjonalne przestrzenie mieszkalne.
    * żywienie: grzyby
    * zagrożenie: Emisje Magii
        * Nagłe trzęsienia ziemi, przesuwania się ziemi, odcinanie fragmentów Baz
        * Wybuchy wulkaniczne
        * Skażenie magiczne
        * Toksyczna Mgła (która zmienia ludzi i zwierzęta): synteza z żywiołakami / elementalami
        * Ataki żywiołaków, magia działa anomalnie
    * założyciel: Arnold Kazitan, technomanta i wizjoner, buntownik przeciw Aurum i sentisieci
    * problemy:
        * Utrata stabilności i awarie systemów energii, komunikacji oraz transportu sprawiają, że życie w mieście staje się coraz trudniejsze.
        * Sanktuarium staje się coraz bardziej narażone na kataklizmy i wybuchy magicznej energii
        * Młodsze pokolenia nie są w stanie odtworzyć osiągnięć ani nauczyć się metod; miasto traci postęp technologiczny

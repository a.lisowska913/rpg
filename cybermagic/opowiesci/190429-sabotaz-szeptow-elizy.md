---
layout: cybermagic-konspekt
title: "Sabotaż szeptów Elizy"
threads: nemesis-pieknotki
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [190427 - Zrzut w Pacyfice](190427-zrzut-w-pacyfice)

### Chronologiczna

* [190427 - Zrzut w Pacyfice](190427-zrzut-w-pacyfice)

## Budowa sesji

### Stan aktualny

* Aleksander trafił do więzienia pustogorskiego. Karla chce go przesłuchać i dowiedzieć się, co wie.
* Olafowi nic się nie stało; Szpital Terminuski doprowadził go do normy w tydzień.
* Miasteczkowcy stwierdzili, że chomikowanie czarnych artefaktów to cholernie zły pomysł. Czarnym miejscem zostają Wolne Ptaki.
* Część Miasteczkowców, jak chciała Karla, przeniosło się do Wolnych Ptaków.
* Część Miasteczkowców woli odsprzedać artefakty Wolnym Ptakom, ale niewielka ich część.
* Wolne Ptaki stają się tyci bardziej znaczącą frakcją i lokalizacją.
* Pojawia się prototyp szlaku handlowego między Wolnymi Ptakami i Cieniaszczytem.
* Artefakty Czarnej Technologii trochę są porozrzucane po terenie.
* Nikola ma świetnej klasy ścigacz, acz Pustogor ma nań namiary

### Pytania

1. Czy Elizie uda się uruchomić wpływ Saitaera
1. Czy Eliza dostanie wzmocniony wpływ na młodych magów

### Wizja

* Zaczęstwo; grupa młodych magów uczy się historii od tajemniczej Strażniczki
* Chevaleresse nie zgadza się na brak szacunku dla wojskowych, wdaje się w bójkę
* Alan zrozpaczony bierze Pięknotkę do siebie

### Tory

* Wzrost znaczenia Elizy Iry: 5
* Wzrost wpływu Elizy: 5
* Odnalezienie Saitaera: 3
* Inkarnacja Elizy: 3
* Ranna Pięknotka: 4

Sceny:

* Scena 1: Chevaleresse w opałach

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Misja właściwa

**Scena 1: Chevaleresse w opałach**

Alan poprosił Pięknotkę do siebie. Znowu. Do Miasteczka, naturalnie. Jak proponowała, pozwolił Chevaleresse na spokojne poruszanie się po mieście, nie ogranicza jej. No i Chevaleresse dostała wpierdol. Jest pobita. Nic poważnego z perspektywy terminusa, ale Alan nie życzy sobie, by JEGO Chevaleresse była pobita. Chce ją nauczyć walczyć - i Pięknotka ma mu w tym pomóc. Żeby ją przekonać.

Pięknotka się zgodziła. Wbiła do Chevaleresse, porozmawiać z dzieciakiem. Pięknotka porozmawiała z Chevaleresse - ta powiedziała, że czasem musi "take a stand". To nie są źli ludzie, ale zdecydowanie nie rozumieją o co chodzi. Chevaleresse powiedziała, że ona nie jest żołnierzem i nie będzie walczyć jak żołnierz. A przy okazji zapytała Pięknotkę o to co to znaczy "Saitaer". Pięknotka pozwoliła jej wierzyć, że to jakiś ważny projekt wojskowy, ale zaczęła Chevaleresse dopytywać skąd i gdzie to słyszała. (Tp:S).

Chevaleresse opowiedziała ostatnie czasy.

* Po całej tej sprawie opuściła EliSquid. Dołączyła do innej gildii, skupiającej się na wojskowym drylu.
* Spotkanie IRL w Zaczęstwie. Prezentacja modelu wojskowego.
* Rozmowa o Saitaerze - który powrócił. Liderka ma sposób jak wykryć energię Saitaera.
* Chevaleresse pokłóciła się z innymi o wojskowych i ich użyteczność. Liderka nie pozwoliła, ale i tak walczyli.
* Diana zrobiła pierwsze uderzenie.

Pięknotce w głowie odpalił się alarm. "Liderka" inkarnująca golema kryształem? Eliza Ira? To źle, bardzo źle. Chevaleresse powiedziała gdzie na Nieużytkach Staszka się spotykali. Spytała Pięknotkę, czy Saitaer jest bogiem. Pięknotka powiedziała, że nie może powiedzieć i niech lepiej Chevaleresse powie czemu tak uważa bo może stać się KOMUŚ INNEMU krzywda (Tp:S). Chevaleresse powiedziała, że Iglica (czyli Eliza) tak mówiła o Saitaerze. Mówiła też o broni przeciw Arazille. I mówiła o budowaniu inkarnaty.

Eliza mówiła też, że można znaleźć energię Saitaera jeśli pomogą jej we wzmocnieniu golema. I wtedy wzmocni się odpowiednie władze.

Pięknotka powiedziała Chevaleresse, żeby ta zostawiła tą grupę. Żeby zawsze powiedziała Pięknotce jak się pojawi temat "Saitaera". I że "Iglica" aka Eliza jest śmiertelnie niebezpieczna. Ale niech Chevaleresse sobie z nimi gra...

Pięknotka przekazała Alanowi wynik negocjacji. Chevaleresse będzie chodzić na sztuki walki - ale to ma być dojo karate czy coś. Nie walka indywidualna. Nie z nim. Alan się żachnął. Chciał ją sam uczyć, bo Chevaleresse go odcina od siebie. Nie robią już niczego razem. Patrzyła w niego jak w obrazek a ledwo teraz go toleruje. Alan zauważył, że jak tak dalej pójdzie, to Chevaleresse go zostawi i wpadnie w kłopoty. Pięknotka powiedziała, że tranzycja z obrazu w osobę jest trudna.

Pięknotka powiedziała Alanowi, że Chevaleresse złapała wirus memetyczny. Nie może nikomu tego powiedzieć bo się rozprzestrzeni. Nie będzie próbowała ponownie. Poprosiła, by Alan nie drążył, bo Pięknotka powiedziała jej że nie wolno o tym z nikim rozmawiać. Alan nie jest szczęśliwy (Tp:S), ale akceptuje to rozwiązanie. Pięknotka opuściła Miasteczko z poczuciem dobrze spełnionego obowiązku. A raczej "cholera, muszę pogadać z Karlą".

**Scena 2: Pięknotka w większych opałach**

Karla dowiedziała się o tym, że Eliza działa na tym terenie. Najpewniej. I Pięknotka nie chce podać źródła. Karla zażądała, Pięknotka broni się, że to nie jest konieczne (Tp:S). Karla nie jest zadowolona, ale na razie jest skłonna to przemilczeć.

Pięknotka przedstawiła Karli sytuację. Karla powiedziała, że Pięknotka ma się zająć tym, by określić faktyczne niebezpieczeństwo; dała jej do dyspozycji trzech konstruminusów.

Pięknotka poszła przesłuchać Aleksandra - pijaka, który powiedział swoje pod wpływem mimika. Aleksander chciał współpracować; dla niego wojna się skończyła dawno temu.

* Eliza skontaktowała się z nim przez kryształy; kupił naszyjnik z Cieniaszczytu
* Eliza buduje rozpoznawalność siebie jako wroga Saitaera i buduje pryzmat zwalczania i odrzucenia Saitaera
* Jednocześnie Eliza buduje sobie "kult" z dawnych towarzyszy Noctis i młodych podatnych
* Królowa Kryształów rozsiewa te kryształy jakoś przez Cieniaszczyt
* On, Aleksander, odrzucił jej ofertę - dlatego nie wie, co Eliza dalej ma w planach

Dobrze, Pięknotka zdecydowała się jeszcze porozmawiać z Olafem. On jako barman NA PEWNO wie więcej. Tak się składa, że też w szpitalu.

* Olaf, jako były wysoki oficer, pomaga utrzymać Miasteczko w porządku
* Olaf potwierdził przebudzenie Elizy; kiedyś był jej bliskim przyjacielem, razem planowali i rywalizowali
* (Tr:10,3,7=SZ) Olaf odgadł jej plan. Zbyt dobrze ją zna.
* Eliza jest uwięziona, nie może opuścić terenu głęboko Skażonego. Jest arcymagiem.
* Eliza jest w stanie połączyć dużą ilość małych kryształów komunikacyjnych w krystaliczne serce. To jest jej opcja inkarnacji
* Eliza jest w stanie stworzyć golema z wielu krystalicznych serc lub hodowanych kryształów. Wtedy ma potężny przekaźnik swojej mocy.
* Eliza nie musi łączyć ich osobiście, ale to szybkie
* Eliza będzie szukać energii ixiońskiej by ją zniszczyć / testować różne działania na niej
* Eliza szuka agentów, kultystów i wyznawców

No i powrót do Karli. Pięknotka zdaje raport, teraz już nie ukrywa źródeł - byli magowie Noctis. Nawet podała imiona, niech tak ich nie męczy. Więc wiadomo już jakie są potencjalne plany Elizy. Tylko jak ją zatrzymać? Jest wszechobecna, szepcze przez kryształy, może pojawić się wszędzie. Jej sieć krystaliczna musi zostać zniszczona.

**Scena 3: Powstrzymać Elizę**

Pięknotka wie, że jeśli ma kilka kryształów, jest w stanie znaleźć inne kryształy. Nie wie jeszcze dokładnie jak, ale Orbiter plus Epirjon powinny być w stanie sobie poradzić. Potem już tylko kwestia wykorzystania Castigatora (artylerii orbitalnej Orbitera).

Trzeba po prostu znaleźć cholerną stację krystaliczną.

Pięknotka poszła odwiedzić Olafa po raz kolejny - chce kryształ Elizy. Olaf jej powiedział wyraźnie - kryształ użyty jest podpięty do danej osoby. Nic to nie zmieni; Pięknotka chce na bazie kryształu Olafa znaleźć inny kryształ który jeszcze nie wpadł w ręce żadnego z jej byłych sojuszników.

Pięknotka poprosiła Erwina, by przeszedł się po Miasteczku; może uda mu się kupić taki kryształ. Dostał kasę i wsparcie. (TrZ+2:9,3,7=SS). Erwin ma odpowiedni kryształ; nie wie o tym, ale kryształ go Dotknął. Erwin będzie znany Elizie jako ten, który stanął przeciwko niej.

Pięknotka ma zatem kryształ. Teraz na jego bazie trzeba zbudować coś, co nada się do uszkodzenia Elizy. I od razu Pięknotce w głowie stanął Alan. Inżynier Zniszczenia. Ekspert dewastacji. Tak samo pojawił się Erwin i Minerwa - katalista i ekspert ixioński.

Pięknotka MUSI wyjąć Minerwę ze szpitala. Ale nie chce stawać na drodze Lucjuszowi Blakenbauerowi - poprosiła więc o to Karlę, by Karla wyjęła Minerwę. No i spotkali się w bunkrach Pustogoru; w najbezpieczniejszym miejscu. Minerwa, Erwin, Alan i Pięknotka.

Gdy Alan usłyszał, że Chevaleresse wpakowała się w Elizę Irę, zmartwił się. Mało powiedziane - zmartwił się. Pięknotka powiedziała, że należy jej się raczej pochwała niż beszt - powiedziała wszystko, została cicho... ogólnie, zuch dziewczyna. Erwin podrapał się po głowie. Dla niego sporo tego wszystkiego, zwłaszcza, że Nikola też ma z tym jakieś powiązanie. Chyba. Gdy Alan dowiedział się, że Eliza to "Iglica" i przejęła jego gildię, to w ogóle się zmartwił.

No cóż. Trzeba opracować skuteczny sposób i plan. Alan od razu zaproponował działanie - potrzebna im broń, która zdestabilizuje Elizę. Coś, co w nią mocno uderzy, co uszkodzi jej strukturę magiczną. Kupi im dwa czy trzy tygodnie. Pięknotka zauważyła, że nie mogą uderzyć Elizie po psychice, bo tak psychika mimo wszystko jest dla nich w miarę korzystna. Elizę można zniszczyć, ale nie zniszczyć jej umysł.

Minerwa, jeszcze w medi-suicie, powiedziała że może da się coś zrobić. Jest psychotroniczką. Może da się uderzyć w linię powiązaną z hipernetem; by ją trochę odciąć czy osłabić. Erwin powiedział, że dobrze byłoby częściowo zamaskować ten sygnał, by nie wyglądał jak pewien atak. Minerwa zaproponowała wykorzystanie Cienia jako filtr. Pięknotka zbladła. Alan nie wie o Cieniu, więc niekoniecznie zrozumiał implikacje.

Potrzebna jest _broń_, która wycelowana w golema uderzy w sieć kryształów i skrzywdzi Elizę; disruptuje ją. Ekstra by było, gdyby od razu rozświetliła gdzie znajduje się jest stacja krystaliczna niedaleko Trzeciego Raju.

Pierwszy krok - przygotowanie energii i sposobu maksymalnego uderzenia w Elizę. To wymaga współpracy wszystkich poza Pięknotką. Celem jest:

* poważne opóźnienie Elizy
* zatrzymanie propagacji Elizy
* uszkodzenie wzoru kryształów; cała nowa matryca musi być zaprojektowana
* zwiększenie kosztu Elizy poruszania się w kryształach
* żeby bolało

Ich siła: energia ixiońska, szeroka wiedza na wszystkie tematy, lokalny kryształ, bezpieczny bunkier, wiedza o Elizie

(HrZ+4:10,5,17=P,P,SS): uda się opóźnić Elizę (około miesiąc odbudowy przy pełnym fokusie) i drastycznie zwiększyć koszt jej poruszania się. Eliza błędnie określi, że to Erwin jest głównym operatorem stojącym za tym planem. Aha, i będzie ją bolało.

Ogólnie, plan jest opracowany. Wiadomo, co się uda osiągnąć. Teraz trzeba to zaimplementować. Tu wchodzi zbudowanie odpowiedniej broni, konstrukcji, która poradzi sobie z tego typu energią. Taka konstrukcja musi być:

* mobilna
* niewielka
* nie bardzo kosztowna
* bez efektów ubocznych

Tu ciężko pracują Alan i Erwin. Ale by to się udało, Pięknotka musi spacyfikować Cienia. Nie musi robić niczego szczególnego; niech po prostu pozwoli przepuścić przez siebie energię i nie robi niczego szczególnie niebezpiecznego (Tp:SS). Cień uruchamia pełną moc i atakuje Minerwę, celując prosto w serce. Cień rani Minerwę; Alan natychmiast wystrzelił ciężkim działem szturmowym prosto w Cienia. Wytrącony z równowagi ixioński power suit się pogubił; wystarczyło to Pięknotce by utrzymać kontrolę. Medtech Minerwy jest uszkodzony i wycieka.

Erwin rzucił się do naprawy (Tp:9,3,4=S); udało mu się powstrzymać sytuację. Minerwa jest tylko trochę bardziej ranna, nic jej się poważnego nie stało (choć Lucjusz Blakenbauer będzie miał poważną rozmowę z Karlą).

Alan spytał Pięknotkę co to do cholery ma znaczyć. Minerwa rzuciła się w obronie - Cień to jej wina, Pięknotka nie zapanowała. Alan powiedział Minerwie, że posunęła się za daleko tworząc coś takiego. Minerwa powiedziała, że to jej wybór, jej decyzja i jej odpowiedzialność. W umyśle Pięknotki Cień wysłał sygnał złośliwego uśmiechu przepełnionego cierpieniem. Alan uważnie obserwuje, że Cień trafiony z "Fulmena" już się regeneruje. Pięknotka czuje, że regeneracja jest trochę jej kosztem.

Erwin i Alan składają broń mającą poradzić sobie z energią ixiońską przepuszczoną przez Cienia: (TrZ:14,3,6=SS). Udało się - ale niestety, jest cholernie kosztowna i raczej jednorazowa. Magowie spojrzeli po sobie. Trudno, Pustogor płaci. Nic dziwnego, skoro chcą uszkodzić sieć krystaliczną arcymagini.

Czas na ostatnie stadium. Erwin, Minerwa i Pięknotka modulują energię a Alan składa broń do kupy. Wprowadzają energię ixiońską, modulują przez Cienia, do tego przepuszczają energię przez cały ten upiornie skomplikowany ekosystem. Dzięki temu liczą na broń:

* dyskretna i celna
* nie powinna emitować energii na kilometr
* nie potrzebuje ogromnego źródła energii
* nie robi krzywdy użytkownikowi
* niski blast radius

(HrZM+4:13,5,16=S). Udało im się zmontować broń wystarczającej klasy; bezproblemowo zamontowali to na Fulmenie. Alan ostrożnie podszedł do tematu, ale nie wahał się ani na moment - Fulmen ma reaktor jak trzy Nutki. To jest platforma artyleryjska bardziej niż zwykły power suit.

Mają broń. Teraz tylko zestrzelić Elizę w jej golemie.

**Scena 4: Zestrzelenie Elizy**

Finalny cel nie jest perfekcyjny, nie uda się zniszczyć Elizy ani w pełni znaleźć jej sieci kryształów. Ale to co się da zrobić - kupić sobie sporo czasu i wysłać do niej sygnał. No i utrudnić jej zdecydowanie życie. W tym samym jest już wartość.

Cała reszta nie była już problemem. Pięknotka w Cieniu poszła na zwiady, tak, jak Chevaleresse pokazała - tak tam było spotkanie (po prostu następnego dnia), Alan się... zakradł, to złe słowo. Alan po prostu wszedł. Wystrzelił z eksperymentalnego działa prosto w golema i doszło do kaskady. A grupka magów została zatrzymana przez Pięknotkę na przesłuchanie i badania.

Biedacy zaczęli dawać nogę ale Pięknotka w Cieniu ich zatrzymała. Bardzo spodobało jej się to co powiedział Alan "nie uciekajcie przede mną, lepiej przed nią. Lepiej będzie się bawić". Z jakiegoś powodu to zadziałało.

**Sprawdzenie Torów** ():

Wpływ:

* Ż: .
* K: .

2 Wpływu -> 50% +1 do toru. Tory:

* Wzrost znaczenia Elizy Iry: 5: 2
* Wzrost wpływu Elizy: 5: 2
* Zdrowie Minerwy: 3: 1
* Odnalezienie Saitaera: 3: 0
* Inkarnacja Elizy: 3: 0
* Ranna Pięknotka: 4: 0

**Epilog**:

* Karla była pod wrażeniem ilości kosztów jakie Laboratorium Senetis jej wystawiło.
* Karla dostała opieprz od Lucjusza Blakenbauera. She was amused.
* Eliza Ira straciła swoją sieć szepczących kryształów. Nie połączy się z nikim aktywnie; teraz to może tylko odpowiadać na zapytania.
* Pięknotka wykorzystała swoje wszystkie znajomości - zaczyna małą kampanię antyElizową na tym terenie, "whispers from the crystals".
* Minerwa ma ekstra trzy dni w szpitalu.
* Wpływ Elizy wzrósł. Są osoby wiedzące o niej i o jej ideałach i są osoby chętne by jej pomóc. Ale nie w Pustogorze i nie u kogokolwiek ważnego.

## Streszczenie

Chevaleresse wpakowała się w "kult Elizy" grając z ludźmi online; powiedziała o tym Pięknotce. Pięknotka odkryła, że Eliza rozprzestrzenia swoje wpływy przez kryształy w formie biżuterii; wraz z Alanem, Erwinem i (nadal ranną) Minerwą zaprojektowali mechanizm uszkadzający sieć krystaliczną Elizy. Udało im się kupić jakiś miesiąc czasu, aż Orbiter i Pustogor znajdą lepsze rozwiązanie. Sama Chevaleresse ma się nie spotykać z takimi tam. Ale może z nimi grać online.

## Progresja

* .

### Frakcji

* .

## Zasługi

* Pięknotka Diakon: po śladach Chevaleresse dotarła do Elizy, sformowała zespół mający rozwalić jej plan i go wykonała. Czyżby manager?
* Alan Bartozol: dowiedział się o Cieniu, Elizie i kilku rzeczach. Ma wrażenie, że Chevaleresse go "zostawia". Dodatkowo, z Erwinem i Minerwą udowodnił czemu jest najlepszym inżynierem zniszczenia w okolicy.
* Diana Tevalier: maleństwo broniące wojskowych, straciła nerwy i uderzyła kolegę za co dostała lekki wpierdol; powiedziała Pięknotce o Elizie i ogólnie, jak na nią, wyjątkowo dorosła.
* Eliza Ira: zakłada kulty i sieci komunikacyjne przez kryształy. Po akcji Zespołu, koszt używania tych kryształów i jej Matrycy znacząco wzrósł. Ale ma już jakieś kontakty.
* Erwin Galilien: katalista delikatnych energii oraz konstruktor pułapki energii; wraz z Alanem, Minerwą i Pięknotką stworzyli rezonator kryształów Elizy, niszczący jej plany.
* Minerwa Metalia: wyszła ranna, wróciła bardziej ranna; Cień wyraźnie chce ją zabić. Kontroluje energię ixiońską i dzięki temu z Alanem i Erwinem złożyli doskonały rezonator.
* Olaf Zuchwały: kiedyś wysoki oficer Inwazji, teraz spokojny barman chcący współpracować z Pustogorem. Nie chce powrotu wojny. Pomógł Pięknotce w pozyskaniu Kryształu Elizy.
* Aleksander Rugczuk: bardzo żałuje, że mimik zmusił go do niektórych rzeczy. Wie, że jest w nim ta chęć dalszej walki, ale nie zamierza walczyć. Też współpracuje - co mu zostało?
* Karla Mrozik: trzeba zaryzykować zdrowie Minerwy? Ok. Trzeba być opieprzoną przez Lucjusza? Ok. Idzie za celami Barbakanu i o nic innego nie dba.
* Lucjusz Blakenbauer: overridowany z decyzją o utrzymanie Minerwy w szpitalu; opieprzył Karlę jak nikt za to, że wróciła bardziej ranna

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Pustogor
                                1. Rdzeń
                                    1. Szpital Terminuski: wyciągnęli z niego Minerwę ku wielkiemu niezadowoleniu Lucjusza
                                1. Interior
                                    1. Bunkry Barbakanu: miejsce, gdzie też znajdują się ufortyfikowane laboratoria pod kontrolą Barbakanu
                                    1. Laboratorium Senetis: źródło rzadkich, dziwnych i eksperymentalnych materiałów dla Alana, Pięknotki, Minerwy i Erwina
                                1. Eksterior
                                    1. Miasteczko: Pięknotka i Chevaleresse odbyły rozmowę na temat bardziej skomplikowanych aspektów kultu Elizy

## Czas

* Opóźnienie: 1
* Dni: 3

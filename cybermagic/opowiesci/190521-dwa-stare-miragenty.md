---
layout: cybermagic-konspekt
title: "Dwa stare miragenty"
threads: krystaliczna-frakcja
gm: żółw
players: kić, dust
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [190326 - Arcymag w raju](190326-arcymag-w-raju)

### Chronologiczna

* [190326 - Arcymag w raju](190326-arcymag-w-raju)

## Budowa sesji

### Stan aktualny

* Eliza nie jest już wrogo nastawiona do Astorii.
* Eliza ma po co żyć - chronić legacy swojej kultury i swojego świata.

### Pytania

1. .

### Wizja

* Miragenty odzyskały kontrolę po wojnie Castigatora z Elizą
* Ona chce uratować jego

### Tory

* niżej

Sceny:

* Scena 1: Żądający Sprawiedliwości dla ofiar miesiąc temu; pilot Alojzy Wypyszcz. Atakuje porozumienie z Elizą.
* Scena 2: Spotkanie u Elizy. Rozmowa o psychotronice.
* Scena 3: Test broni, w kanionie.
* Scena 4: Uratować.

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Misja właściwa

Alojzy (dziennikarz) przyszedł do Fergusa i spytał czemu chcą pokoju z Elizą. Jego brat zginął w dziwnych okolicznościach miesiąc temu. Orbiter przekierował sprawę do Fergusa i Olgi. Podobno brat Alojzego żyje. No i Alojzy chodzi po ludziach i sieje ferment.

Alojzy spotyka się z Fergusem który chce mu pomóc. Jego historia kupy się nie trzyma - sprawdza i crosschekuje, przydusza (TpZ: S). Przyznał się - użył Magii Krwi, wie że brat przetrwał i że cierpi. Wie, widział w wizji "kobietę z kryształami". Kto inny niż Eliza?

Eliza zapytana przez Olgę powiedziała, że w tamtym obszarze kryształy krzyczą z bezsilności, frustracji i ogólnie pojętej rozpaczy. Powiedziała o potencjalnym krystalicznym amplifikatorze - narzędziu, którego funkcją jest przechowywanie energii magicznej i wzmacnianiu Pryzmatu; być może tam jest takie urządzenie.

Dobrze, trzeba pójść sprawdzić. Ale wpierw potrzebne są jakieś dane czy próbki. Eliza powiedziała, że jeśli mają kryształy jakie ona może przeanalizować to dostaną odpowiedź na pytanie jak wygląda sytuacja. Fergus wysępił od Alojzego kryształ z "pola bitwy" - miejsca, gdzie zniknął w transporterze jego brat (Fred).

Eliza po przeanalizowaniu powiedziała, że:

* stworzenie takich kryształów niestabilnych i wybuchających to co najmniej rok intensywnych emocji pod amplifikatorem. Coś chronologia nie pasuje.
* wygląda to jak długotrwała dziwna inwestycja
* typ kryształu wskazuje na to, że to nie jest robota fachowca

Wzięli medyka, katalistę i wojownika ze sobą i poszli w tamtym kierunku (wskazanym przez Alojzego i Elizę). M.in. z Alojzym. Tylko po to, by wpakować się na pułapkę (Tr:8,3,8=S). Dziura wypełniona wybuchającymi kryształami. Stwierdzili, że można ominąć bez kłopotów. Zamontowali mały detonatorek by wysadzić gdy będzie potrzeba. I czas wejść do bazy. Wejście jest niesamowicie ukryte. Zespół szuka - Olga magią łączy się mentalnie z Alojzym i szuka Freda (brata) (TpZ+2:S).

"Będziesz tu stała i patrzyła jak on się męczy - i masz zakaz ruszania się z miejsca. Nie możesz się ruszyć z miejsca." - wizja mężczyzny który rozkazał to stojącej, bezradnej miragentce która widzi jak jej brat (miragent) wije się na łóżku pod krystalicznym systemem. Fergus deaktywuje pułapkę; chcą wejść do środka. (Tp:9,3,4=P). Eksplozja pułapki. Ranni w środku.

Ewakuacja rannych pełnych obłędu i strachu. Część z nich nie ma niektórych części ciała, część nie żyje i została zjedzona. Po drodze próba rozmowy z Fredem (bratem Alojzego), utrudniony kontakt, ale on przynajmniej jest kontaktowy (TrZ:11,3,7).

Fred powiedział, że siostra-miragent zastawiła pułapkę. Ledwo się ruszała, uszkodzona i ze zdemolowanymi aktuatorami. Ale wystarczyło, by wpuścić ich na minę, pokonać i doprowadzić tu, do tej bazy. Jej "brat" był już zbyt daleko posunięty by naprawić jego psychotronikę, był w pętli cierpienia i nienawiści, ale on (Fred) doprowadził do jakiegokolwiek postępu. Zmienili te miragenty trochę; naprawili je przy użyciu ludzkich, biologicznych części i komponentów (nie z własnej woli).

Miragenty poszły znaleźć swojego oprawcę. Żaden z nich nie ma sprawnej, stabilnej psychotroniki. Zespół słysząc to, poleciał za nimi - by zobaczyć wiązkę energii strącającą Tucznika. Polecenie z Orbitera - zniszczyć miragenty asap.

Fergus full speed. Zdążyć przed śmiercią Tucznika. (S). Olga używa magii by zrzucić dane z miragentów (TrM:) i dostała odpowiedź. Udało jej się zdobyć informacje z psychotroniki miragentów zanim te zostały zniszczone oraz nikt tego nie wie.

* Miragenty zostały stworzone kilkanaście lat temu, do rozgrywek politycznych wewnątrz Orbitera
* Miragenty się zdradziły by uratować życie temu, którego miały kryć
* Ich twórca wpakował je w takie coś. Nie mógł ich zniszczyć, bo wiedziały za dużo o CZYMŚ i był dead hand - więc stwierdził, że tak zniszczy ich psychotronikę
* Ich twórca jest wysoko w hierarchii Orbitera a Olga ma to COŚ
* Twórca nie musiał tak tego robić - ale to było dlań najprzyjemniejsze

Olga zachowuje wiedzę o tym, ale zostaje cicho. Lepiej się nie zdradzać. Zameldowali zniszczenie miragentów i skończenie problemów z czasów wojny.

**Sprawdzenie Torów** ():

* Nienawiść do Elizy rośnie: 4: 2
* Wściekłość na Orbitera: 4: 1
* Dewastacja: 3: 1

**Epilog**:

* Tucznik został zniszczony, na szczęście wszyscy przetrwali
* Miragenty zostały zniszczone, Olga posiada dane z psychotroniki
* Gniew wielu obrócił się przeciw Elizie - fakty nie mają znaczenia

## Streszczenie

Pokój z Elizą Irą doprowadził do tego, że w okolicy pojawił się korespondent szukający swojego brata - podobno Eliza go zabiła. Tyle, że Eliza nic o tym nie wie. Zespół poszedł śladem nieszczęsnego brata i znaleźli dwa stare miragenty, które zostały skazane na cierpienie przez swojego poprzedniego dowódcę z Orbitera. Olga zwinęła bazę danych miragentów którą ów dowódca chciał zniszczyć 7 lat temu zanim miragenty zostały trwale i nieodwracalnie zniszczone. Ludzie zostali uratowani. Niestety, transportowiec (Tucznik Trzeci) został zestrzelony.

## Progresja

* Olga Leszcz: przechwyciła bazę danych miragentów, którą jeden z wysoko postawionych dowódców Orbitera chciał z jakiegoś powodu zniszczyć; coś politycznego.

### Frakcji

* .

## Zasługi

* Fergus Salien: rozmontowywał wszystkie pułapki krystaliczne i negocjował współpracę z Elizą. W sumie - jednej nie rozbroił. Zestrzelił miragenty.
* Olga Leszcz: przechwyciła bazę danych z miragenta tak, że nikt się nie zorientował. Opiekowała się też rannymi.
* Alojzy Wypyszcz: korespondent, który nie bał się magii krwi by móc odnaleźć swojego brata. Też jest magiem. Nienawidzi Elizy Iry.
* Fred Wypyszcz: neuronauta, który pomógł miragentom wrócić do stanu działania pod przymusem jednego z miragentów.
* Eliza Ira: nic nie wie o tym, by zniszczyła grupę archeologów. Pomogła Zespołowi tłumacząc zasady działania kryształów i krystalicznych struktur Noctis.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski, NW
                    1. Ruiniec
                        1. Trzeci Raj: przybył tam korespondent Alojzy w poszukiwaniu brata.
                        1. Pustynia Kryształowa: miejsce, gdzie skazane były na cierpienie dwa miragenty i gdzie znaleziono ich ludzkie ofiary.

## Czas

* Opóźnienie: 7
* Dni: 2

---
layout: cybermagic-konspekt
title: "Sabotaż Miecza Światła"
threads: legenda-arianny
gm: żółw
players: kić, fox
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [200408 - O psach i kryształach](200408-o-psach-i-krysztalach)

### Chronologiczna

* [200408 - O psach i kryształach](200408-o-psach-i-krysztalach)

## Budowa sesji

### Stan aktualny

Serenit...

### Dark Future

* Miecz Światła pożarty przez Serenit
* Luiza pożarta przez Serenit
* Załoga Inferni martwa

### Pytania

1. .

### Dominujące uczucia

* .

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Punkt zero

We are in the past

Spaceship „Perceptor” is dying. Some of the crew are already dead, some others are being converted into some unholy rock formation creatures. There are two active magi on the spaceship and they are the only ones who can save whatever remains. Martyn and Atena.

Martyn uses his magical powers to mold all the dead and corrupted humans into some kind of blob; he pushes them away from the perceptible to make them a membrane, living shield protecting the spaceship from incoming shards from the cosmic anomaly Serenit.

Having Martyn’s help, Atena manages to use all power remaining in dying spaceship to detach the reactor and explode it – that way Perceptor can fly forward on the shockwave. They were able to escape Serenit, but at the great cost.

## Misja właściwa

We are in the current time. Arianna wakes up, being pulled from the biovat by Martyn. She is in catastrophically bad shape and she cannot use magic. Martyn calmly explains, they are on the spaceship „Miecz Światła” and that this spaceship is flying to its doom – it tries to fly towards Serenit. And Arianna needs to save it.

Martyn managed to get both Arianna and Claudia in whatever remained of Infernia. He explains what happened - after their rundown with Leotis, they have been rescued by „Miecz Światła”. However, the captain of this ship is trying to go help his colleague who is currently entrapped by Serenit.

And this will kill everyone.

Martyn quickly explains to Arianna and Claudia whatever he knows about this anomaly. She explained how he had a meeting with in the past and that he managed to survive only barely. Arianna decided that whatever happens they are not going to fly towards the anomaly. Now how to convince everyone else?

First order is to revive Eustachy; they might need someone being able to sabotage and do some firepower stuff to be able to either repair Infernia somewhat or to sabotage Lightsaber.

Arianna pulled information on the captain of lightsaber, Mateusz Sowiński. The guy is a competent and valiant commander, the crew trusts him, he usually does everything in his power to save everyone, and he completely ignores whatever his artificial intelligence tells him. He doesn’t trust in AI at all. Not even Persephone-class.

Still, Persephone might be one of the keys to save Lightsaber. Claudia and Eustachy used Martyn’s knowledge about Serenit and they have managed to reconstruct Martyn’s story onto destroyed databases on Infernia. Then Eustachy has furthered sabotage of Infernia in such a way that the databanks are destroyed enough to lose some details. That way it is possible to convince lightsaber’s Persephone that the story is real.

After Claudia contacted Persephone and told Persephone that she has some extra information on the origins of SOS signal coming from the ship they are going towards, Persephone tried to access Infernia’s databanks. The forgery was very efficient – Persephone got really convinced that they are approaching Serenit and that this Serenit is extremely dangerous, that it can kill the Lightsaber. Persephone decided she needs to help Arianna (another commander, so a proper rank) convince Matthew that they have to be cautious flying towards SOS signal.

Of course, Persephone failed. Matthew wants to know nothing about what ever a dump AI is going to tell him. He needs to save his colleague whom he was close to.

Now that’s a problem. It is impossible to convince Matthew, Arianna needs to convince the crew that Matthew needs to stop commanding for a while – that he is mind might be impaired. Arianna doesn’t really want to do this, but… she can accept that.

Arianna knows, that Matthew is all about being a hero and saving everyone. Having help of Persephone, she went to the preacher and started talking with Matthew – her goal was to make him explode and lose face in front of the bridge crew. Of course, she succeeded, it was enough to be very sweet and warn Matthew that the anomaly they are flying towards is extremely dangerous, she’s afraid of it, she recommends not to go to because the lightsaber will be destroyed.

Being insistent and slightly provocative was enough to make Matthew – hermit – explode and throw her out of the bridge. This made Persephone quite worried, and some of the higher officers start doubting Matthews judgments.

Sadly, it was almost impossible to make the crew mutiny against the captain. So, the captain needs to do something really stupid. Some of those things have the ground-level prepared – Matthew is worried about his friend, he is also irritated at Arianna. But it is not enough.

Arianna knows that Matthew is very proud of his spaceship. No wonder – lightsaber is one of the better spaceships in Orbiters fleet. But this also gives Matthew a weakness – he tends to overestimate what the lightsaber can do.

I am his plan was quite simple – sabotage lightsaber’s engine. Nothing very dangerous, just something that allows other officers to consider Matthew to have impaired judgment. Or some think which makes the lightsaber not being able to get to his friend in time.

But to be able to perform any act of sabotage, Persephone needs to be distracted. Therefore Claudia took some crew members of the lightsaber and they went to cryptology and communication modules to use Persephone and to decode the malformed SOS signal is coming from Serenit.

Claudia succeeded Duff she was able to decode the SOS hidden inside the SOS; the 21 was sent by the other ship’s Persephone. The other one warned them that the other spaceship is being devoured by anomaly, that the other Persephone’s systems are being assimilated into the overmind of Serenit ants that if they want to come save them it might be slightly late. The other Persephone also said that Serenit is keeping humans and magi alive to lose them even better.

So, the lightsaber will manage to get to the anomaly in time but the anomaly will be prepared for them. And that is unfortunate.

While Claudia was decoding the signals coming from the anomaly, Eustachy sneaks into the engines and managed to efficiently sabotage them – a bit too well. He didn’t have much time because the diversion created by Martin’s toxins was both to efficient and pulled too many people here. But the sabotage was made at Arianna’s plan was ready to be unleashed.

It wasn’t really difficult - when Matthew heard that it is possible to save those people he ordered food ahead. He ordered the lightsaber to exceed safety limitations. The main engineer of lightsaber the lightsaber should be able to manage. They did not expect what happened next and what happened next was the result of a sabotage.

Three out of four engines of the lightsaber exploded; casualties were about 40 people. The main structure of lightsaber was damaged and one of the reactors went down. Lightsaber have to jettison the engines which incidentally eliminated all the traces of sabotage.

It was impossible for the lightsaber to even try approaching the anomaly. This spaceship changed course towards the controller prime; Matthew had no idea what just transpired and he just sat there looking at the monitor with ashen face.

Arianna wasn’t really happy about what happened, but it was preferable to losing lightsaber and Infernia to the nightmare of Serenit.

## Streszczenie

Zniszczona Infernia została uratowana przez Miecz Światła, który zaraz potem leciał ratować inny statek przed AK Serenit. W odpowiedzi Arianna doprowadziła do sabotażu Miecza Światła i zniszczenia reputacji komodora Sowińskiego by ratować Miecz Światła przed Serenitem. Jej się upiekło, acz pojawiło się więcej ofiar śmiertelnych...

## Progresja

* .

### Frakcji

* .

## Zasługi

* Arianna Verlen: zaufała Martynowi; wpierw sabotowała Miecz Światła Eustachym i wyprowadziła z równowagi Mateusza Sowińskiego. W ten sposób uszkodziła Miecz, ale uratowała go przed lotem na Serenit.
* Klaudia Stryk: doskonale manipulowała danymi Inferni by przekonać Persefonę jak groźny jest Serenit; potem odwracała uwagę Persefony pracując nad sygnałem z Serenita.
* Martyn Hiwasser: pamiętając straszliwe przeżycia z Serenit w przeszłości wybudził Ariannę z regeneracyjnego snu. Pośrednio odpowiedzialny za sabotaż Miecza Światła.
* Eustachy Korkoran: sabotował skutecznie silniki Miecza Światła. Zbyt skutecznie - doprowadził do kaskadowych zniszczeń i sabotażu poczciwego statku ratunkowego.
* Mateusz Sowiński: uratował Ariannę i Infernię; dał się Ariannie wyprowadzić z równowagi i nie wiedząc o sabotażu dał rozkaz dzięki któremu Miecz Światła został ciężko uszkodzony.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański

## Czas

* Opóźnienie: 4
* Dni: 2

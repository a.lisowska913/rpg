---
layout: cybermagic-konspekt
title:  "Nowa Minerwa w nowym świecie"
threads: nemesis-pieknotki
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [190827 - Rozpaczliwe ratowanie BII](190827-rozpaczliwe-ratowanie-bii)

### Chronologiczna

* [190827 - Rozpaczliwe ratowanie BII](190827-rozpaczliwe-ratowanie-bii)

## Projektowanie sesji

### Pytania sesji

* .

### Struktura sesji: Frakcje

* Kornel: przekonać Minerwę, by pojechała z nim
* Erwin: pozostać z Pięknotką
* Kasjopea: zobaczyć koszmar Minerwy
* Minerwa: znaleźć dla siebie miejsce. Znowu.

DARK FUTURE: Minerwa opuszcza Pustogor.

### Hasła i uruchomienie

* Ż: (kontekst otoczenia) -
* Ż: (element niepasujący) -
* Ż: (przeszłość, kontekst) -

### Dark Future

1. ...

## Potencjalne pytania historyczne

* brak

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Misja właściwa

**Scena**: (20:20)

_Pustogor, Barbakan_

Karla zaprosiła Pięknotkę do siebie. Gdy Karla prosi, to nie jest prośba. Karla powiedziała, że Pięknotka jest osobą która ma zreintegrować Minerwę ze światem. Karla powiedziała, że Minerwa jest ciekawym przypadkiem - i może być zagrożeniem dla siebie. Może ktoś na nią polować - do świadomości przedostało się "czarodziejka, która użyła magii krwi by się wskrzesić". Słowo "Saitaer" nadal jest tajemnicą - i Karla zakłada, że informacja o Saitaerze nie trafi na światło dzienne. Pięknotka ma zapewnić, by ani Minerwa ani Pięknotka nie wygadały się o Saitaerze.

Karla powiedziała, że w Minerwie pozostało echo Saitaera. Jej ciało się zmieniło, jej poziom magii też. Jest załamana, bo nie jest już tak piękną Diakonką jak kiedyś. Zdaniem Karradraela, Minerwa jest bezpieczna od Saitaera.

_Pustogor, Gabinet Pięknotki_ (20:27)

Dwóch pancernych terminusów wprowadza Minerwę. Pięknotka jest zaskoczona, Minerwa się lekko uśmiecha - chyba mogłaby ich zabić jednym pstryknięciem. A przynajmniej tak wygląda patrząc na ich defensywy. Minerwa zrobiła kiepski żart, Pięknotka odesłała terminusów. Minerwa powiedziała, że chciała dołączyć do programu wysokich technologii, ale jej odmówiono.

Minerwa powiedziała ze zdziwieniem, że Pięknotka ma na biurku konserwę. Pięknotka zauważyła technomantyczną anomalię - ozdóbkę. Powiedziała, że to jest konserwa pochodząca z Ixion, poległy żołnierz zostawia konserwę dla swoich braci i sióstr... wpadła w pewien trans. Pięknotka nagrała to co mówi Minerwa, po czym Minerwa (7,3,5=P) wpadła w stupor. Zaczęła nadawanie różnych sygnałów na hipernecie. Pięknotka wyizolowała ten kanał hipernetowy, ale za późno (Kasjopea).

Kasjopea skontaktowała się z Pięknotką i poprosiła ją o to, by Pięknotka odblokowała jej ten kanał. Pięknotka odmówiła. Kasjopea powiedziała, że to Minerwa, prawda? Uratowana przez Karradraela? Pięknotka kazała jej odejść. Kasjopea powiedziała, że niedługo tam będzie.

Minerwa się wytrąciła ze stuporu, acz z trudem. Nadal nie umie się wydostać z wizji Ixiona. Poszła umyć twarz - ale poszła do złego miejsca.

_Pustogor, Gabinet Pięknotki, godzina później_ (20:50)

Erwin wrócił do domu - czyli do Pięknotki. Erwin jest w szoku gdy spojrzał na Minerwę - nie jest tą samą piękną Diakonką, jest tylko zwykłą dziewczyną. MIMO, że Pięknotka go ostrzegła, Erwin nie umiał ukryć swoich uczuć. Minerwa to zauważyła. Pięknotka wyczuła, że ONA jest w lekkim szoku. Minerwa przytuliła Erwina i spytała, czy Erwin x Pięknotka - tak. Erwin powiedział Minerwie, że chce, by byli przyjaciółmi. Minerwa - piękna Diakonka (kiedyś) - została friendzonowana. AUĆ. Elegancko wycofała się do łóżka, jest zmęczona ciężkim dniem wśród nudnych terminusów.

Pięknotka zaproponowała Minerwie jakoś uczcić jej powrót - Minerwa najpierw ucieszyła się na małą imprezę, ale potem spochmurniała. Nie jest już piękna i wie o tym. Ale może jakaś knajpka, prędzej czy później... ;-).

_Pustogor, Gabinet Pięknotki, jeszcze godzina później_ (21:07)

Pięknotka, próbnie, zaproponowała Erwinowi potencjalny trójkącik z Minerwą. Erwin się przestraszył. Byłoby super, jasne, ale on nie kocha Minerwy. Kocha Pięknotkę. I jakkolwiek nie przeszkadzałoby mu to tak bardzo, nie chce robić Minerwie fałszywej nadziei. Pięknotka uważa to za urocze, ale zostawia myśli przy sobie.

Pięknotka szybko wymyśliła zadanie, które może na kilka godzin usunąć Erwina z domu. Po czym pinguje Kasjopeę po hipernecie. Czego Kasjopea chce od Minerwy? Kasjopea powiedziała prawdę - chce zobaczyć wizje w pięknej głowie Minerwy. Zdaniem Kasjopei, Minerwa nie ma niczego czego powinna się wstydzić, ona żyje na krawędzi. To cudowne życie. Kasjopea uważa, że Minerwa jest cudowna i chce zobaczyć piękno koszmaru, zapisać to na holokostkach i zapoznać się z tym.

Zdaniem Pięknotki Kasjopea jest chora. Zdaniem Kasjopei, może, ale nic to nie zmienia.

Korzystając z czasu bez Minerwy, Pięknotka przygotowuje mega epickie spa. Takie full wypas. Wszystko na maksimum. No i zajmie się Minerwą osobiście. Oczywiście, jak Minerwa się obudzi (+2).

_Pustogor, Gabinet Pięknotki, dwie godziny później_ (21:30)

Minerwa się obudziła i stwierdziła, że rzuci zaklęcie. (9,3,3=MSS). W promieniu 30 metrów od Minerwy, w domu Pięknotki i poza nim, pojawiły się dziwne anomalie magiczne technomantyczne. Od razu się Pięknotce odpalił Alarm Pustogoru. Terminusi zaczęli przesyłać sygnały próbując się zorientować co się dzieje. Pięknotka zgłosiła Paradoks. Barbakan wysłał cleanup team...

Pięknotka znalazła Minerwę w pokoju. Ta patrzy pusto obserwując magów sprzątających teren. Nic się nie stało, zdaniem Minerwy, zły sen. Pięknotka dopytuje co się stało - próbuje dojść do tego (Tr+1:8,3,6=SS). Minerwa powiedziała - śniło jej się, że nie kontroluje magii. To był tak wiarygodny sen... chciała sprawdzić jak się obudziła. I faktycznie, nie kontroluje magii... morale Minerwy sięgnęło dna. Pięknotka spróbowała pocieszyć Minerwę, ale średnio jej się udało. Minerwa robi dobrą minę do złej gry - przynajmniej ma ciało. Pięknotka poczuła tęsknotę do Dotyku Saitaera od Minerwy... ale wolała nie wnikać.

Po hipernecie Pięknotka dostała polecenie od innego terminusa - niech Minerwa nie czaruje więcej w Pustogorze. To niebezpieczne dla miasta.

Pięknotka zaproponowała Minerwie fajne zabiegi kosmetyczne i spa. Minerwa się zgodziła bez wahania - ona LUBI miłe rzeczy na swoim ciele. Pięknotka znalazła słaby punkt Minerwy. Kilka solidnych godzin rozpieszczania Minerwy (TrZ+2:12,3,4=P,SS). Minerwa się trochę otworzyła do Pięknotki. (Z innych wieści: Kornel przybywa pod Pustogor, ściągnąć do siebie Minerwę). Powiedziała Pięknotce, że czuje się ogólnie... źle. Minerwa nie jest mieszkanką Pustogoru. Ona jest tu tylko dlatego, bo Erwin tu był, bo powstała z Nutki - ale teraz Erwin jest z Pięknotką. Ona jest tym piątym kołem u wozu. Powinna stąd odejść. Myśli o Orbiterze.

Minerwa powiedziała, że jest _ktoś_ kto jej szuka. Rycerz na białym koniu... albo Biały Rycerz na koniu? Weźmie ją ze sobą, do swojego laboratorium. Minerwa nie wie skąd to wie, ale czuje, że tam jest potrzebna. A tu nie. Pięknotka NATYCHMIAST skojarzyła Kornela. I nie jest to dobre skojarzenie... przy okazji rozmowy Pięknotka zauważyła, że Minerwa ma o niej dobrą opinię. To miłe.

Wpływ:

* Ż: 4
* K: 4

* Pozytywne Wspomnienia Minerwy: Pięknotka się o nią zatroszczyła, 
* Negatywne Wspomnienia Minerwy: Pustogor ją odepchnął, nie są razem z Erwinem, nie jest już piękna, Paradoks (nie kontroluje magii), przekonana że nie kontroluje swojej magii, 

**Scena**: (22:04)

_Pustogor, Ciemna Strona_

Pięknotka wzięła Minerwę na obiad do Ciemnej Strony - małej, kameralnej knajpki gdzie trzeba się umówić. Wyraźnie Minerwie się to spodobało - mogła wyjść, poczuć wiatr, zobaczyć innych ludzi. Niestety, do Pięknotki podeszło dwóch magów. Kiedyś Myszy. Powiedzieli Minerwie, że to przez nią stracili teren. Nazwali ją defilerką. Pięknotka kazała im znaleźć dowody lub sobie iść - magowie woleli się oddalić niż walczyć z Pięknotką (słusznie).

Minerwa powiedziała Pięknotce prostą rzecz - nie jest przeciwna pozostaniu w okolicy jakiś czas, ale nie w Pustogorze. To miejsce jest dla niej niewłaściwe. Ona nie chce. Ale może przycupnąć sobie w Zaczęstwie - tam się zawsze przyda technomantka. A i jest z dala od tego wszystkiego i nie będzie jej torturował widok Pięknotki x Erwina. Przyznała się, że nadal kocha Erwina. To ją sprowadziło z powrotem. Ale Minerwa nie ma niczego do zaoferowania Erwinowi, nie teraz... a on jest wyraźnie szczęśliwy.

_Pustogor, Gabinet Pięknotki_ (22:23)

Więc Pięknotka zaproponowała wieczór firmowy. Ona, Erwin i Minerwa. Niech oglądają jakieś fajne rzeczy - aż padną. Niech Minerwa się dobrze bawi. Udało się - Minerwa zasnęła szczęśliwsza.

A w międzyczasie Pięknotka porozmawiała z Tymonem po hipernecie. Powiedziała mu, że "zagubiona i biedna czarodziejka" potrzebuje pomocy. To trudny przypadek. Kompetentna technomantka. Tymon się ucieszył - przyda mu się ktoś taki. Pięknotka powiedziała kto to. Od razu się zmartwił. Pięknotka powiedziała, że tutaj wyzywają ją od defilerów i że ona mierzy się z, no, trudnymi tematami. Tymon wzięty na litość. No i Pięknotka spróbuje jeszcze trochę ustabilizować Minerwę. (Tr+2:SS). Zgodził się, ale poprosił Pięknotkę o wsparcie Karli.

_Pustogor, Barbakan_ (22:34)

Pięknotka poprosiła Karlę o wsparcie dla Tymona. Powiedziała, że Minerwa chce do Zaczęstwa, ale Tymon sam sobie nie poradzi. Powiedziała, jakie sytuacje miały miejsce w okolicy - magowie wyzywający Minerwę od defilerów, czy anomalie. A Karla ma tam kogoś kto by się tam idealnie nadał (-2 wpływ Kić) - młody terminus technomanta. Przystojny. Pięknotka się wyszczerzyła, gdy Karla zauważyła co Pięknotka planuje. Zdaniem Pięknotki, Minerwie potrzebny jest porządny romans. (TpZ:12,3,2=SS). Karla się zgodziła - przeniesie tam odpowiednich magów (prawidłowych) i niech mieszka tam spokojnie Minerwa. SKONFLIKTOWANIE: to nie jest w sekrecie; jeśli ktoś chce znaleźć Minerwę, znajdzie ją.

_Pustogor, Gabinet Pięknotki_ (22:45)

Pięknotka poprosiła Erwina, by pozwolił Minerwie spojrzeć na Nutkę. Erwin jest lekko sceptyczny. Erwin nie jest pewny czy ta Minerwa jest tą samą Minerwą której ufał. Pięknotka nalega - jej zależy, by umieli pracować razem. Niech jednak Erwin weźmie pod uwagę, że Minerwa ma nowe umiejętności - może Nutkę zdecydowanie usprawnić. (Tp+2:11,3,3=SS). Erwin się zgodził, ale NUTKA się bardzo bała, co jeszcze bardziej wpłynęło negatywnie na morale Minerwy.

Minerwa zdecydowała się zbudować największe arcydzieło. Usprawnić Nutkę najlepiej jak potrafi - Black Technology. (Kić -2 Wpływu by Hr->Tr) (TrMZ:13,3,4=S). Nutka dostała bardzo potężne wzmocnienie, kosztem tygodnia ciężkiej pracy Minerwy i dużej ilości stresu Erwina.

(Kić: -2 Wpływ): random acts of kindness wobec Minerwy.

Wpływ:

* Ż: 8
* K: 3 (7)

* Pozytywne Wspomnienia Minerwy: Pięknotka się o nią zatroszczyła, Pięknotka wzięła ją na spacer. Erwin i Pięknotka chcą by ona się dobrze bawiła, random acts of kindness.
* Negatywne Wspomnienia Minerwy: Pustogor ją odepchnął, nie są razem z Erwinem, nie jest już piękna, Paradoks (nie kontroluje magii), przekonana że nie kontroluje swojej magii, magowie uważają ją za defilerkę, Nutka się jej boi

**Epilog** (23:00)

* Tymon dostał swoje wsparcie do Zaczęstwa.
* Pięknotka nadal odpowiada za Minerwę.
* Nutka jest na zupełnie innym poziomie psychotroniki niż kiedykolwiek, wspomagana Black Technology.
* Minerwa jest psychicznie zdeptana, ale nie zamierza się poddawać. Opłakuje utratę Erwina i urody, ale walczy.
* Kasjopea poluje na Minerwę - chce zrobić epicki holoobraz.
* Z zupełnie innego miejsca, przyjeżdża Biały Rycerz po swoją bogdankę...

### Wpływ na świat

| Kto           | Wolnych | Sumarycznie |
|---------------|---------|-------------|
| Żółw          |   7     |     8       |
| Kić           |   3     |     9       |

Czyli:

* (K): Kasjopea nie zrobi Minerwie większej krzywdy (1)
* (K): Minerwa znalazła coś w Zaczęstwie co sprawia, że może zostać (1). Cyberszkołę (1).
* (Ż): .

## Streszczenie

Minerwa wróciła. Jako czarodziejka, opiekować się ma nią Pięknotka. Minerwa nie do końca radzi sobie w nowej rzeczywistości - nie jest piękną Diakonką, ma odłamki pamięci Saitaera i aby powrócić, poświęciła życie człowieka. Zdecydowała się tymczasowo przenieść do Zaczęstwa (Karla wysłała tam wsparcie). Tymczasem Kornel Garn już planuje by ją przekonać by doń dołączyła.

## Progresja

* Minerwa Metalia: ma nowe ciało, maga. Nie jest już piękną Diakonką, ale ma ciało wspomagane przez Saitaera. Niezbyt lubiana w okolicy Pustogoru.
* Minerwa Metalia: utraciła Erwina. Zwana defilerką. Odepchnięta przez Pustogor. Nie kontroluje magii dobrze. Ogólnie, bardzo zdeptana psychicznie i w złej formie.
* Nutka Galilien: nieprawdopodobne usprawnienia psychotroniczne dokonane przez Minerwę przy użyciu Black Technology (heroic). Stała się czymś dużo więcej niż kiedykolwiek.

### Frakcji

* .

## Zasługi

* Pięknotka Diakon: rozpaczliwie próbuje poprawić samopoczucie Minerwy, żonglować polityką i Karlą oraz nie stracić do Minerwy Erwina.
* Minerwa Metalia: w nowym ciele, 7 smutnych wspomnień i 4 dobre, niepewna i zestresowana. Chce odejść, ale zostaje (na razie) w Zaczęstwie. Największe osiągnięcie psychotroniki w Nutce z Black Technology.
* Kasjopea Maus: psycholka która uważa, że Minerwa jest cudowna - żyje na krawędzi i ma chory umysł. Chce zrobić holokostki z wizji Minerwy. Pięknotka staje jej na drodze.
* Erwin Galilien: nie kocha Minerwy. Kocha Pięknotkę. Chciał, by to było wystarczająco jasne. Nie do końca Minerwie ufa, ale jest skłonny jej zaufać.
* Tymon Grubosz: miał nadzieję na wsparcie i dzięki Pięknotce do Zaczęstwa dostał wsparcie oraz Minerwę. Akceptuje pomoc Minerwie; da jej szansę.
* Karla Mrozik: przekazała Minerwę pod opiekę Pięknotce i zaakceptowała, że Minerwa osiądzie raczej w Zaczęstwie. Niech Tymon ma wsparcie w Zaczęstwie (terminusi).

## Plany

* Kornel Garn: Biały Rycerz. Przyjedzie spróbować zachęcić Minerwę, by ta odjechała z nim w siną dal.
* Minerwa Metalia: spróbować osiąść w Zaczęstwie i zobaczyć jak to będzie. Szczególnie interesuje ją Cyberszkoła.
* Kasjopea Maus: korzystając z obecności Minerwy, stworzyć najdziksze i najbardziej nienaturalne holokostki.

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Pustogor
                                1. Barbakan: wszelkie rozmowy Pięknotki z Karlą mają tu miejsce
                                1. Gabinet Pięknotki: miejsce tymczasowego przechowania Minerwy i jej aklimatyzacji z nowym ciałem i światem
                                1. Kawiarenka Ciemna Strona: ciche i spokojne miejsce, raczej nie ma tam wielu magów. Bardziej dyskretne i wyższej klasy.

## Czas

* Opóźnienie: 1
* Dni: 4

## Narzędzia MG

### Budowa sesji

**SCENA:**: Nie aplikuje

### Omówienie celu

* nic

## Wykorzystana mechanika

1811

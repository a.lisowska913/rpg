---
layout: cybermagic-konspekt
title: "Ratunkowa misja Goldariona"
threads: legenda-arianny
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [210120 - Sympozjum zniszczenia](210120-sympozjum-zniszczenia)

### Chronologiczna

* [210120 - Sympozjum zniszczenia](210120-sympozjum-zniszczenia)

## Punkt zerowy

### Dark Past

.

### Opis sytuacji

.

### Postacie

.

## Punkt zero

.

## Misja właściwa

Klaudia się zainspirowała. Patrząc na to, że Kontroler Pierwszy miał w swoich czeluściach laboratorium takie jak przejęła Jasmina, najpewniej da się też znaleźć inne rzeczy. Klaudia **wie**, że są oficjalne grupy szabrujące na Kontrolerze. Ale oficjalne idą do K1. A jako, że nikt nie wie jak naprawdę działa K1... ze wszystkimi fabrykatorami, ze wszystkimi zaawansowanymi mechanizmami... to na pewno znajdzie się coś dla Klaudii. Na przykład jakiś godny komputerowy sprzęt. Np. maszyny do eksperymentów z AI.

Klaudia musi dogrzebać się do historycznych planów. Znaleźć coś, co wskazuje na obecność - choć potencjalną - takich części. Fora, legendy, stare plany... coś co wskaże na obecność tego co Klaudia szuka.

* V: Klaudia znalazła serię interesujących miejsc. Jedno przykuło jej uwagę - to obszar, gdzie pracował kiedyś Percival Diakon. Ale straciliśmy ten obszar.
* X: Armada dron, produkowana przez jakiś fabrykator ^_^.
* V: Teren prowadzący do Sektora 57 został zalany. Jest to teren wyjątkowo niekorzystny a w okolicy porusza się armada dron bojowych - produkowanych przez JAKIŚ fabrykator.
* X: Teren jest naprawdę, nieprawdopodobnie zły. Tak samo ciężko się przedostać niezauważenie. Potrzebne są specjalne servary - najlepiej klasa Eidolon.
* V: Klaudia zdobyła stare kody kontrolne. Jest w stanie oszukiwać i maskować się przed niektórymi zagrożeniami. Interaktować z niektórymi komputerami.

Klaudia złapała Elenę w jej kwaterze. Elena podpięła się do symulatora i toczy z kimś pojedynek na myśliwce. Po jej minie, idzie jej średnio. Klaudia pozwala jej skończyć, nie chce przeszkadzać (bo Elena jest z tych bardziej honorowych). Skoczyła po jej ulubiony napój - o dziwo, tonik. (7V5X->3V). Gdy Klaudia wróciła, Elena jest zmęczona, ale ma szeroki, paskudny uśmiech. Wygrała. Zmasakrowała przeciwnika.

* Elena: "Ma na imię Wiktor. Jest szybki i ma niezłe odruchy. Ale jest powtarzalny - próbuje mnie zaatakować z zaskoczenia. 'Gwieździsty Ptak' jest na to za zwrotny."
* Klaudia: "Fakt." (nie mając pojęcia o czym Elena mówi)

Elena jest w dość kiepskim humorze. Nie powinna, skoro wygrała. Ktoś powiedział coś niewłaściwego, Elena stoczyła z nim bój na symulatorze, wygrała. Nie powinna była musieć robić tego pojedynku. Klaudia nie jest tak blisko Eleny; wolała nie naciskać by poznać szczegóły. Ale Klaudia jest wścibska. BARDZO wścibska. Sprawdziła szybko w hipernecie komunikację jawną dookoła Eleny. Okazało się, że Elena stoczyła w ciągu 3 ostatnich dni 7 bitew w obronie swojej czci. A dokładniej - uważana jest za niekompetentną lafiryndę. A te pojedynki pozwalają Elenie pokazać w okrutny sposób swoim przeciwnikom co naprawdę potrafi.

Klaudia powiedziała Elenie, że szuka części do laboratorium - coś, co pomoże albo Anastazji albo Inferni. Elena się zgodziła. Będzie z Klaudią szabrować części Czarnego Kontrolera.

Klaudia znalazła firmę, która ma dostęp do Eidolonów. Małe prywatne przedsiębiorstwo, które ostatnio wpadło w tarapaty. Są zdesperowani i Klaudia jest w stanie zaoferować im pewne wsparcie. Klaudia spotkała się z Feliksem Przędzem - szefem firmy ArcheoPrzędz. Klaudia zdecydowała się dowiedzieć sporo na temat ArcheoPrzędz zanim się z nimi spotkała (VVV). I wiedza o ArcheoPrzędz stanęła przed nią otworem.

* Firma istnieje od 8 lat. Miała swoje wzloty i upadki. Chwilowo jest raczej w fazie upadku.
* ArcheoPrzędz specjalizują się w eksploracji wraków. Niestety, stracili głównego advancera - przeszedł do konkurencji.
* Akcje drugorzędnych advancerów kończyły się zwykle większymi stratami niż zyskami. Mają też kłopoty z lokalizowaniem cennych wraków.
* Ostatnio ArcheoPrzędz znalazło cenny wrak - i szukają advancera i kogoś kto pomoże im w ekstrakcji - co ekstraktować, co unikać.
* Pojawiły się plotki, że z uwagi na problemy finansowe AP zaczęło współpracować z wyrzutkami. Bo nie stać ich na lepszą załogę.
* Firma ma sprzęt, ma wiedzę, know-how, rynki zbytu i wszystko co jest potrzebne. Ma problemy tylko z doświadczonymi osobami na ekspedycjach.
* Nie mają pozwolenia na działanie na terenie K1 (akwizycję).

Klaudia z Eleną poszły do ArcheoPrzędz - chcą wynająć 2 Eidolony. Może się uda. Na miejscu Feliks. On nie chce im wynająć - ale on potrzebuje advancera i skanera do operacji. Proponuje barter - da Klaudii do 5 Eidolonów do użycia jeśli ona pomoże mu rozwiązać tą misję ratunkową. Na pytanie o detale:

* V: jest statek kosmiczny, nie do końca wiadomo czyj, gdzie zaplątała się nielegalna grupa "advancerów". I ich trzeba uratować.
* XX: ci piraci podpadli Orbiterowi. Nie są to priorytetowi piraci, ale Siły Wyzwolenia Fareil są czymś, co Orbiter zwalcza.
* VV: ci nielegalni to piraci operujący niedaleko Neikatis. Ogólnie, mówimy o okolicach Neikatis. Oni sobie z tym nie poradzą - to mała grupka Fareilitów. 
* piraci próbowali znaleźć jakąś broń, jakiś sprzęt w derelikcie. I skończyło się tak, że derelikt ich pokonał. Mówimy o 6 osobach.
* statek do którego lecicie jest jakieś 2 dni stąd. Feliks załatwi Zespołowi statek. Wymogiem Klaudii było to, że on też leci.

Klaudia poprosiła Feliksa o plany dotyczące derelicta. On odmówił - Klaudia może wszystkiego się dowiedzieć i po prostu przeskoczyć ponad niego. "To nie jest statek wojskowy. On nawet nie jest stąd!". Klaudia wstała od stołu z Eleną - jest oficerem naukowym. Żyje informacją. Nie wejdzie w to. Elena dodała "być może dlatego tracisz advancerów. Opowiem Ci o statku zwanym Serenit - niejeden advancer zginął idąc w tamtą stronę".

* XV: Feliks się złamał. Powiedział co wie o tym statku. 

Nazywa się "Uśmiechnięta", kiedyś należała do Luxuritiasu. Pochodzi z zewnątrz systemu astoriańskiego. Została znaleziona już opuszczona; poważnie uszkodzona. Doszło do przebicia kadłuba. Reaktor był nieaktywny, nie emitowała żadnych sygnałów, nie było niczego. Dlatego nikt jej nie wykrył. Nie było SOS. Piraci natknęli się na "Uśmiechniętą" przez przypadek - i nie wyszli, stracono z nimi kontakt. Wysłano drugą grupę - ta także nie wróciła. Stąd potrzeba kogoś od badań i advancera. Piraci nie mają nikogo kompetentnego w roli advancera... Znaleziono ją przez przypadek. Dryfowała. Nie wiadomo co się stało z załogą.

Zanim Klaudia się oddaliła, Feliks zapytał, czy może na nie liczyć. Elena nie jest fanką piratów, nie lubi potyczek klanowych... ale nie chce, by niebezpieczne anomalie same się pałętały w kosmosie. Wymieniły spojrzenia - obie obawiają się, że narodzi się z tego nowy Serenit czy coś. Więc Elena i Klaudia potwierdziły - wchodzą w to. Ale Feliksa może to kosztować dużo bardziej niż się teraz wydaje.

Klaudia skupiła się na researchu. Wzięła wszystko co Feliks ma odnośnie "Uśmiechniętej". I weszła w systemy Orbitera. Z poziomu Orbitera zaczęła szukać informacji - co na to Luxuritias. Czy oni w ogóle zgłosili zaginięcie statku. Klaudia stwierdziła, że zagłębi łapki porządnie w Orbiter - chce dowiedzieć się jak najwięcej na temat "Uśmiechniętej". I Klaudia robi taki manewr, by wszystko wskazywało, że to **Olgierd** szuka informacji na temat "Uśmiechniętej".

* XX: Zainteresowane siły na Orbiterze wiedzą, że Olgierd interesuje się "Uśmiechniętą". Mówimy m.in. o siłach specjalnych.
* V: "Uśmiechnięta" zaginęła nie w tym sektorze, kilka lat temu. Orbiter podejrzewał "Uśmiechniętą" o szmugiel narkotyków na bazie kralotycznej. Luksusowy statek typu 'small trader'.
* Właścicielem był zagorzały miłośnik wił. Koleś miał sporo bioform i środków edytujących wiły. To był niezły degenerat. Był na pokładzie gdy zaginęła.
* Statek nie posiadał TAI. On wykorzystywał często stosowane przez drakolitów mechanizm łączący bio z AI. Innymi słowy, BIA jak noktianie.
* Orbiter nie dostał informacji, że "Uśmiechnięta" działa na tym terenie. Musiał przedostać się na powyłączanych systemach przez Bramy. Albo przeniosła go Anomalia Kolapsu.

Jeśli tu jest potencjał na kralothy, bio i inne takie - Klaudia chce Martyna do pomocy. Podbija do Martyna. Po konsultacji Martyn potwierdził - kraloth w próżni przetrwa. A Luxuritias często jest _compromised_ przez Kult Ośmiornicy. Tak - Martyn pomoże Klaudii. Martyn też ostrzegł Klaudię - jeśli mają do czynienia z kralothem, nie mogą wziąć ze sobą Leony. Zrobią klasyczną operację - advancer wchodzi w Eidolonie, potem się zobaczy jak wygląda sytuacja.

Dzień później, Klaudia, Elena i Martyn zmieścili się w Gwieździstym Ptaku Eleny. Klaudia zaszabrowała trochę sprzętu z Inferni (zwłaszcza sensorów przenośnych), Martyn nie miał kłopotu ze zwinięciem apteczki. I kilka godzin później Gwieździsty Ptak zbliżył się do "Goldariona" - statku z którym współpracuje Feliks Przędz.

Goldarion jest _nieprawdopodobnie brudny_. Niezadbany. Elena patrzy na to z takimi pustymi oczami. Tam śmierdzi. Statek jest po prostu... niezadbany.

Przywitał ich Adam Permin - szef ochrony. Też niechlujny. Klaudia wdała się z nim w pyskówkę, że tu brudno. Odpowiedział, żeby sama posprzątała. Ale ich zaprowadził do ich kwater. Mają trzy przyległe do siebie kwatery, też niezbyt czyste, ale tu ktoś już coś spróbował. Elena natychmiast odwiedziła Klaudię.

* Ja tu nie jem - Elena do Klaudii. - Proponuję racje awaryjne z "Ptaka"
* Pasuje - Klaudia, ponuro

Niedługo później, zostali zaproszeni do War Roomu. Tam będzie się planować operację. Martyn został z tyłu; odkazi kwatery. Sam wiele nie pomoże, nie w operacji planowania. A nieoficjalnie - Martyn został pilnować ich rzeczy. Tak będzie bezpieczniej, a Elena i Klaudia są kluczowe w tej operacji. A nie potraktują Martyna - medyka - jako niebezpieczeństwo. Bardzo się pomylą.

Elena i Klaudia poszły korytarzem do War Room. Po drodze spotkały 3 bysiów, którzy zaczęli cicho do siebie "kurde, zerżnąłbym tą tam...". Klaudia zatrzymała Elenę przed zrobieniem sceny (VVV). Elena jest coraz bardziej zirytowana, ale Klaudia nie widzi po co doprowadzać do wojny w pierwszym momencie.

War Room jest czystszy. Wszystko jest _sprawne_, acz czasem wymaga kopniaka. Statek serio nie jest specjalnie zadbany i nie jest w dobrym stanie. Dziewczyny przywitał kapitan statku, Aleksander Leszert. Ten jest bardziej zadbany, ale nadal... nie.

* Sensory Goldariona nie były w stanie spenetrować w głąb Uśmiechniętej.
* Osoby na pokładzie Goldariona nigdy nie zdjęły skafandrów - a jednak zostały zdjęte z akcji.
* Mówimy o około 20 osobach zaginiętych. (zgodnie z estymacją Klaudii, spore stężenie piratów)

Klaudia spytała o jakieś nagrania. Leszert ma nagrania z dronów oraz z rejestratów w skafandrach. Nagrania są kiepskiej jakości, jak wszystko na tym statku, ale coś tam jest. Nagranie było już z drugiego zespołu (pierwszego nic nie wiadomo). Na nagraniu wiele wskazuje, że znaleźli kogoś z pierwszego zespołu w skafandrze - i ta osoba obróciła się przeciwko. Strzelał, klasyczny berserk. Potem jednak oni sami powyłączali monitoring i stracono z nimi kontakt. To jest najdziwniejsze.

Klaudia, anomalie. Siada z nagraniem, siada ze swoim komputerkiem i zaczyna pracę.

* V: nanitki odpadają. To jest czysto mentalny atak, który zaczął się w chwili wykrycia. Atak miał zasięg tylko w samym statku. Niewykryty - nie ma ataku, jest kierowany.
* BIA nie włada magią; ona stricte odpada.
* V: Klaudia złapała nośnik ataku. To śpiew syreny. Innymi słowy, coś z wiłami. Włączenie silnego ekranowania auralnego powinno osłonić przed atakiem.
* X: Są tam różne nielegalne narkotyki i środki. Rzeczy, które są ogólnie niefajne. A załoga Goldariona chce te rzeczy.
* V: Klaudia oceniła siłę ataku mentalnego - cokolwiek tam jest, nie jest to pełnosprawny kraloth. Atak był za słaby. Klaudia oceniła też jak długo można przetrwać jak wola wił się skupia na ofierze. To jest około 3-5 minut. Innymi słowy, długo.

Klaudia natychmiast rzuciła temat Martynowi o narkotykach. Martyn powiedział, że popracuje nad sposobami uszkodzenia tych narkotyków - jeśli będzie miał do nich dostęp to je uszkodzi w sposób mało zauważalny (niezauważalny dla nikogo na tym statku).

Zespół został zaproszony na obiad do kapitana. Elena powiedziała, że to jej kolej na pilnowanie przedmiotów. Martyn poszedł. Klaudia też, acz wzięła coś na trucizny (Martyn też). Elena - z uwagi na swoje własności fizyczne - musi uważać...

Aleksander Leszert załatwił niewielki posiłek. Klaudia, Martyn, on i Adam. Mesa oficerska jest czystsza - na ogromne szczęście. Nadal nie jest to poziom Orbitera, ale Klaudia nie boi się tak jeść, bo np. talerze są czyste. Gdzieś w tle Klaudia słyszy dudnienie takiego chamskiego techno... gdzieś z obszarów załogi. Leszert spytał Klaudię jak jej się podoba Goldarion. Nie jest to najlepszy statek, ale daje radę. Klaudia nie wie co powiedzieć.

Adam powiedział, że to widać, że goście z Orbitera gardzą statkiem i załogą. Klaudia szybko odpyskowała - kapitan Leszert nie dał jej powodu do pogardy. Martyn rzucił jakiś żart (który miał rozbroić sytuację, ale nie zadziałał). Klaudii też nie udało się rozładować atmosfery. Próbuje być grzeczna, ale nie działa to jako rozbrojenie. Martyn ciężko westchnął i przejął temat. Wyszedł z poziomu oficera medycznego. Pokazał im, że ten statek jest niebezpieczny - chodzi o brud. Chodzi o podejście. Zrobił to maksymalnie czarująco jak potrafi, ale jest bezwzględnie uczciwy. Zakończył w stylu, że nie gardzi nikim. To ich statek i ich działania. Ale oficerowie Orbitera idą na akcję - i muszą chronić swoje możliwości (a nie np. zatruć się brudną kanapką). Klaudia się troszkę dołączyła - pomoże im w klasyfikacji rzeczy, troszkę stan statku by był do życia. (Ale nie pomoże w akcjach nielegalnych ani prowadzenie do tego).

* V: "we don't mean any insults" -> przeszło. Udało się. Poziom gniewu został zdeeskalowany.
* X: załoga, nie oficerowie, tylko liniowi marynarze są bardzo niechętni gościom z Orbitera.
* V: Leszert wyjaśni sytuację

Leszert wyjaśnił problem. Statek jest bardzo zaniedbany, bo np. nie ma od pewnego czasu refitowania. Life Support ma problemy z wodą - nie da się niektórych rzeczy czyścić. To spowodowało efekt wybitego okna - ludziom przestało zależeć. Dodatkowo, większość dobrych członków załogi opuściła "Goldarion", bo nie dostali pensji. To też sprawia, że nie ma dużych możliwości wpływania na aktualnych członków Goldariona. Innymi słowy - Goldarion potrzebuje tego zlecenia. Potem znowu będzie dobrze.

Klaudia niejeden raz słyszała już "tylko ten raz, potem będzie dobrze...". Ale kapitan Leszert jest zdesperowany i naprawdę się stara jakoś to wszystko utrzymać w kupie. Leszart opowiedział kilka przeszłych akcji Goldariona, jak na pokładzie była advancerka, jak robili eksploracje wraków, uciekali przed piratami... ale to było, niestety, 2 lata temu. Od tej pory Goldarion jest na równi pochyłej w dół. Klaudia też opowiedziała jakąś historię ze swojej przeszłości - coś trochę wow, ale nic zakazanego.

Kapitan postawił na stół wino. Ale Klaudia zauważyła, że on nie pije. Martyn też podziękował - jest na służbie. Adam wzruszył ramionami i pije za wszystkich.

Koniec obiadu. Tymczasem Elena męczy się z Eidolonem... przygotowała się, przygotowała backupy i zabezpieczenia i weszła w interfejs Eidolona. Neurosymbioza. (XXX|VV).

* XXX: Eidolon miał pułapkę, booby trap; Elena się zdeinterfejsowała zanim Eidolon jej nie wprowadził w komę.
* VV: Elena się odpięła od Eidolona zanim ten jej nie skrzywdził; plus, ma pewność, że to sabotaż. Była tam też jakaś wiadomość i Elena ją przechwyciła.

Wiadomość pochodziła od poprzedniej advancerki, Oliwii Pietrovy. Wiadomość: "uważaj! rozłącz się!"
Martyn obejrzał Elenę. Nic jej nie jest - na szczęście.

Elena zostaje pilnować złomu. Martyn i Klaudia idą do kapitana. Mają dlań niewesołą wiadomość... na mostek został też zaproszony Feliks. Zapytany, Feliks powiedział, że ostatni raz Oliwia używała tego Eidolona 2 miesiące temu. To była ostatnia akcja Oliwii - on coś ukrywa. Martyn wszedł w słowo i powiedział w skrócie - to poważna sprawa, nie mogą przed nimi nic ukrywać bo to śmiertelnie niebezpieczne.

Klaudia NIGDY tak się nie cieszyła, że ma Martyna. Martyn mistrz negocjacji Inferni na Goldarionie... (VVV)

Kapitan statku po prostu zaczął się śmiać. To nie jest wesoły śmiech - to raczej śmiech skazańca. Felisk jest blady. Eidolon jest super drogi. I Feliks powiedział w końcu wszystko.

* Ostatnia akcja Oliwii była operacją przeciwko placówce drakolickiej, w okolicach Neikatis. To był element wojny domowej.
* Oliwia została złapana i była torturowana. Została odbita przez Goldarion.
* Oliwia która opuściła ArcheoPrzędz była zupełnie inną Oliwią niż ta przed akcją - była złamana.
* ArcheoPrzędz nie miało wyjścia. Musieli wziąć ten kontrakt, tą akcję - inaczej by zostali zniszczeni przez własnych sojuszników. "Jesteś z nami lub przeciw nam".
* Oliwia wiedziała to wszystko, ale zaryzykowała. I się zemściło.
* Większość marynarzy działających na Goldarionie to fareilici - oni nie potrzebują tych pieniędzy, oni chcą zemstę na drakolitach.

Z tego Klaudia wywnioskowała prostą rzecz - Oliwia wiedziała co się stało z Eidolonem, ale bała się powiedzieć. Klaudia spytała wyraźnie - co jeszcze są tu za sekrety, które są tak ważne? Odpowiedź Feliksa ją rozbawiła - w grupie osób, które weszły na "Uśmiechniętą" były osoby spokrewnione z mocodawcami "Goldariona". To ważne, by móc ich uratować. Pozwoli ArcheoPrzędz się wyrwać z działań wojskowych, żeby mogli robić to co powinni od początku - biznes.

Klaudia przenosi uszkodzonego Eidolona do magazynu. Nie ma tu laboratorium. Wzięła ze sobą Elenę do pomocy - jest w końcu advancerem. Umieściła wszystkich (Feliks, Aleksander) poza cargo bay. Elena i Klaudia mają skafandry a Aleksander dostał zadanie - "przygotuj się, by zawartość wyrzucić w kosmos".

Klaudia chce się dowiedzieć co to za rodzaj sabotażu i czy dotyka pozostałe sprawne cztery Eidolony. A jeśli tak - wyczyścić je.

* XXX: vent necessary; Elena i Klaudia lecą
* V: Klaudia zna odpowiedzi na pytania
* X: strukturalny problem statku; nie uda się zamknąć
* V: naprawione pozostałe Eidolony

Klaudia stawia swoje chore, dziwne anomalie. I oczywiście wszystko MUSIAŁO pójść nie tak - ale makroglut złapał błędny wzór ze zniszczonego Eidolona i przeniósł to na sprawne Eidolony; a dokładniej, usunął to stamtąd. Gorzej, że makroglut zaczął anomalizować statek. Więc Klaudia: "VENT VENT VENT!!!". Aleksander otworzył luk, poszło w kosmos jak w Cowboy Bebop. I NIE DA SIĘ ZAMKNĄĆ! Elena, Klaudia, anomalie - w kosmosie. A Eidolony są przyczepione do pokładu na twardo. Elena i Klaudia są maglockiem... więc są w kosmosie.

* "Naprawdę nie lubię tego statku..." - Elena do Klaudii. 
* "WARNING! WARNING! INTEGRAL SYSTEMS FAILURE!!!" - TAI Semla statku

Elena użyła hipernetu Orbitera łącząc się z Martynem. Opracowali ten plan wcześniej. Zgodnie z planem, Martyn wystrzelił "Gwieździstego Ptaka" z "Goldariona". Elena jest mistrzynią orientacji przestrzennej - zawsze dokładnie wie GDZIE jest. Wysłała rozkaz do Ptaka - ma się do niej zbliżyć.

* X: podczas manewrowania, Ptak wpakował się na jedną z anomalii i zmienił kurs; Klaudia poleciała w kosmos (bonk). Elena ją musiała łapać.
* VV: Elena, Klaudia są na pokładzie Ptaka.

Dziewczyny widzą piękny widok rozpadających się elementów statku kosmicznego "Goldarion". Zdaniem Eleny, by zachować strukturalną spójność Goldariona i by ów mógł lecieć trzeba zamknąć ładownię Goldariona - nawet na stałe. Klaudia stwierdziła, że przy użyciu szczątków może próbować użyć dron by to zamknąć.

Na tym etapie kapitan nie ma już żadnych planów. Na pokładzie Goldariona nie ma **ani jednego maga**, co jest kolejnym szokiem dla Klaudii. Jedynym magiem była Oliwia (advancer). A jednak Goldarion ją odbił. Czyli byli lepsi, niż się wydawało - może nawet są lepsi.

Klaudia używa magii by wprowadzić drony, zaspawać itp. Elena jako najbliższe mechanika nadzoruje, by na pewno zrobione to było dobrze. 

* VVV: Statek nadaje się do dalszego lotu
* X: Statek jest niebezpieczny w obszarze tamtej ładowni. Na szczęście - tylko tam. Trzeba unikać tych miejsc.

W wypadku jeszcze jednego (V) jest wrażenie na załodze. Że agenci Orbitera są skuteczni i groźni.

* X: Załoga jest mega wściekła na agentów Orbitera - nie tylko nie ma Eidolona, ale też statek uszkodzili.

Co Klaudii udało się dowiedzieć z badań sabotowanego Eidolona:

* Nośnikiem sabotażu była Oliwia. Jakiś mag zmienił ją w wektor na ten jeden raz.
* Oliwia była mega zastraszona. Nie ośmieliła się powiedzieć co i jak.
* Celem sabotażu było wprowadzenie kolejnego advancera w komę. I problemy prawne ArcheoPrzędz / tamten advancer.

...w końcu udało się dotrzeć do "Uśmiechniętej". A Elena skutecznie zinterfejsowała z Eidolonem. Jednym z czterech, które pozostały.

Uśmiechnięta. Niewielki statek, na ~150 osób. Fregatka, mniejsza niż Infernia i tym bardziej niż Goldarion. W tej chwili - wyraźnie w ruinie. Na zewnątrz Uśmiechniętej są ślady po zderzeniach, uszkodzone poszycie itp. Na pierwszy rzut oka Klaudii, statek jest opuszczony co najmniej rok, co by się zgadzało. Elenie się ten statek też nie podoba - widać to po jej minie. Ale Elena się cieszy, że WRESZCIE może coś zrobić co nie jest idiotyzmem na rozpadającym się złomie...

Pierwszy plan jest prosty - Elena wchodzi w Eidolonie, rozkłada podstawowe czujniki, robi zwiad i wychodzi. Dolatuje do Uśmiechniętej swoim Ptakiem. Martyn jest gotowy. Klaudia jest pewna, że Elenie się uda. A Elena, cóż, wchodzi... (VXOOXVV)

* V: Intruzja się udała bez żadnego problemu. Elena weszła na pokład. Uniknęła wszystkich potencjalnych zagrożeń.
* X: 3 osoby w skafandrach były w okolicy, w której Elena chciała przejąć komputer. Miała eksfiltrować dane z komputera.
* OO: Eidolon. Się. Zaciął. Po prostu. Gdy Elena zmusiła Eidolon do ostrzejszego działania, to Eidolon przestał działać. To nie sabotaż - to kiepski sprzęt.
* X: Elena opuściła Eidolon w słabej atmosferze. Jako advancer ma lekki ochraniacz a jako Elena, ma potężny visiat. Stała się widoczna; trzech skafandrowców postrzeliła, uszkadzając skafandry i zmuszając do oderwania.
* V: Przetrwała śpiew Syreny. Środki jakie dał jej Martyn pomogły.
* V: Zdobyła element komputera, jakiś _core_ i rzuciła się do ucieczki.
* V: Rzuciła się, wspomagając visiatem prosto w kierunku na Ptaka. Przechwyciła się w locie i natychmiast poleciała na Goldarion.

Aleksander i Adam mają szczęki w okolicach podłogi. Elena. Wow. Oliwia tak nie umiała.

* V: Elena zrobiła niesamowite wrażenie na wszystkich. Jest jeden statek na świecie który patrzy na nią jak na boginię.

Elena wraca, cała zakaszlana, w kiepskim stanie. Martyn natychmiast skupił się na medycznym wsparciu Eleny. Wpierw regeneracja, potem stymulacja i inne rzeczy. Jako, że Martyn jest ekspertem od próżni to DOKŁADNIE wie jak jej pomóc - zwykle te środki używa dla siebie...

A Klaudia ma to co chciała - rdzeń komputera. Może wreszcie dojść do tego co tu się stało i z czym mają do czynienia.

Klaudia PIEKIELNIE dba by to zaizolować, z niczym nie połączyć. Ale musi poznać o co chodzi. Chce badać w próżni; ustawia stację badawczą poza statkiem. Nie chce kolejnej katastrofy. Klaudia operuje z Ptaka na komputerze z zasobów Inferni (teehee) do którego podpięty jest rdzeń informacji z komputerów Uśmiechniętej. I czas dowiedzieć się wszystkiego na temat natury anomalii z którą Klaudia ma do czynienia - wszystko, co jest w komputerze.

* V: Klaudia doszła do informacji jakich nie miał Luxuritias ani Orbiter. Właściciel - Kamil Frederico - był fanem nie tylko wił. Jego ulubionym hobby było sprzęganie dziewczyn z obwodami kontrolnymi i przekształcanie ich w wiły. Był "smakoszem". Absolutny degenerat. Ale bogaty i wpływowy, czyli ekscentryk.
* Pewnego razu zaprosił na statek czarodziejkę i jej siostrę. I czarodziejka, widząc, co on im robi użyła rytuału Krwi.
* Doszło do Skażenia Bii kontrolującej statkiem. Bia i wiły się w pewien sposób sprzęgły. Obecność środków na bazie kralotycznej pomogła.
* Frederico się ewakuował, wraz z częścią załogi. Statek porzucił. Statek nie jest jeszcze anomalny, ale panuje na nim Tren Nienawiści.
* Na statku znajdują się komory hibernacyjne. W tych komorach znajdują się dziewczyny. Frederico na nich pracował, ale nie są elementem Bii.

Klaudia przedstawiła Aleksandrowi i Feliksowi swój plan:

* Elena bierze NASTĘPNEGO Eidolona. Leci na pokład Uśmiechniętej. Tam - usuwa Bię. Potem montuje "hak" na Uśmiechniętej.
* Goldarion transportuje Uśmiechniętą w kierunku na Kontroler Pierwszy. Przeżyli na Uśmiechniętej tyle, że jeszcze dożyją.
* Tam można pomóc tym ludziom i wyciągnąć dziewczyny ze stazy.

Feliksowi pomysł się spodobał. Aleksander jednak przeszedł do rzeczy. Powiedział, że jak odtransportują "Uśmiechniętą" do Kontrolera Pierwszego, nie będzie tam tak naprawdę dużego zarobku. Wszystko nielegalne Orbiter skonfiskuje, nie będzie pieniędzy, nie będzie niczego. A są już dwa Eidolony do tyłu (jeden jest naprawialny, ale...). Innymi słowy, jedyna nadzieja by ArcheoPrzędz i Goldarion stanęły znowu na nogi to sprzedaż nielegalnych narkotyków i podłych rzeczy. A na stacji Valentina takie rzeczy są jak najbardziej sprzedawalne.

Klaudia powiedziała, że jako oficer Orbitera nie zgadza się z tym podejściem. Aleksander może próbować ją powstrzymać, ale ona nie odpuści. A Aleksander, cóż, on nie jest tak daleko by chcieć krzywdzić oficerów Orbitera. Nie chodzi tylko o respekt do tego co pokazała Elena - chodzi też o to, że on jednak jest dobrym obywatelem. Chce robić dobrze, nie źle.

W końcu zgodzili się na plan:

* Elena w (trzecim) Eidolonie wchodzi na pokład Uśmiechniętej i robi egzekucję Bii.
* Szabruje się wszystko z Uśmiechniętej na Goldariona
* Martyn niszczy rzeczy nielegalne
* Potem poinformuje się rodzinę Frederico o tym, że mają dowody na zachowanie Kamila. Niewielki szantażyk - ale chodzi o to, by dostać kasę i naprawić Goldariona.
* A i tak celem było uratowanie tych ludzi w skafandrach - a Martyn jest w stanie im pomóc. Więc będzie dobrze.

Elena patrzy podejrzliwie na Eidolona (numer trzy). Sprawdziła go. Przetestowała. Eidolon ma prawidłowe interfejsowanie. Elena ciężko wzdycha. Nie wierzy w Eidolona. Powtórzyła operację - Ptakiem lecimy na Goldarion, potem wbijamy Eidolonem na pokład. A Martyn z Klaudią przygotowali dla Elenę koktail destrukcji dla Trenu Nienawiści (Bia połączona z wiłami). (XVXVXXV -> rozłożymy potem)

Czas na wejście Eleny. Tym razem ma stymulanty od Martyna i wie dokładnie na co idzie. Ale nadal czuje szok po działaniu w próżni...

* XV: (intruzja). Czekali na nią. Byli gotowi. Pałali nienawiścią. Elena jest szybka, Eidolon daje jej niewidzialność. Tym razem Elena im się wymknęła, unikając straszliwego Trenu. Ale dalej szukają.
* XV: (droga). Spotkała 2-3 skafandrowców, którzy strzelali w losowe miejsca śmiejąc się w absolutnej malignie. Eidolon został trafiony - wymaga DROGIEJ naprawy. Ale Elena przeszła.
* VVV: (BIA). Elena dotarła do Bii, używając prędkości i kontroli. Ten Eidolon pozwala jej działać tak, jak żaden nie-advancer nie potrafi. WRESZCIE MA EIDOLONA KTÓRY DZIAŁA. Bia uderzyła ją potężną falą psioniczną. Elena się zachwiała, po czym wystrzeliła, precyzyjnie, prosto w cel.

Działanie biomantycznej toksyny Martyna i Klaudii:

* XX: Dziewczyny są zniszczone już dawno temu. Stąd ta nienawiść. Są, de facto, seks-zabawkami. Tylko w hibernacji. To zrobił nie "Tren" a Frederico.
* VV: Nie dochodzi do śmierci ani nieodwracalnych uszkodzeń ważnych skafandrowców. Większość jest do uratowania bez konsekwencji.
* X: "Uśmiechnięta" zostaje ciężko Skażona przy śmierci "Trenu". Natychmiast, bardzo szybko trzeba szabrować i ewakuować.
* X: Elena traci przytomność. Dostaje uderzeniem zwrotnym; psychiczny ból i nienawiść ją "wypalają" (do końca sesji). Za silne.
* V: "Tren Nienawiści", anomalna sprzężona Bia imploduje. Bardzo szybko przestaje istnieć i działać po trafieniu bronią biomantyczną.

Tren był skutkiem a nie przyczyną...

SZABROZAUR!!! (10V-7X-3O) draw 5: XXXVV.

* Eleną musiał zająć się Martyn **natychmiast**. Zostawiło to tylko Klaudię do kontrolowania co tam się dzieje.
* NIKT nie wie gdzie są schowki syfów. Ale "ich" jest mnóstwo.
* Udało się ewakuować skafandrowców, wyciągnąć elementy, części, poszabrować.
* Mamy kilkudziesięciu rannych i napromieniowanych. Mamy anomalne komponenty. I na 100% - czego Klaudia jest pewna - na pokład dostały się zakazane rzeczy.
* Załoga wie, że te dziewczyny w hibernacji nie są już dziewczynami a narzędziami.

Gdy Martyn uwijał się w ukropie, Klaudię odwiedził Adam. Powiedział jej, że ma dla niej propozycję. Tych dziewczyn w hibernacji nie da się już uratować. On nie chce, by tego typu środki chemiczne, narkotyki itp trafiły do obiegu. Więc proponuje, że Klaudia pozwoli mu "zajumać" kilkanaście dziewczyn a on zapewni, że ani jeden pieprzony gram tych narkotyków nie opuści Goldariona - zapewni, by Klaudia dostała wszystko do zniszczenia.

Klaudia się zgodziła. Te dziewczyny nie są już ludźmi. Dla niej nie było to nawet kuszeniem.

I w ten sposób Goldarion bezpiecznie wrócił na Kontroler Pierwszy, po drodze robiąc małą wizytę by schować "dziewczyny"...

## Streszczenie

By zdobyć własne laboratorium w czarnych strefach Kontrolera, Klaudia weszła we współpracę z firmą ArcheoPrzędz i pomogła z Eleną uratować grupkę piratów, którzy bez advancera próbowali eksplorować wrak "Uśmiechniętej" - wraku Luxuritias. Po drodze przelecieli się najbardziej rozpadającym się i najbrudniejszym statkiem cywilnym - Goldarionem - jaki Klaudia kiedykolwiek widziała. Aha, i okazało się, że pojawiają się problemy na linii drakolici - fareil. Bonus: Martyn jako negocjator i mechanizm społeczny XD.

## Progresja

* Klaudia Stryk: ma pozytywne stosunki i kontakty z ArcheoPrzędz, cywilną podupadłą firmą na Kontrolerze Pierwszym.
* Elena Verlen: dostaje dostęp do trzech średniej klasy Eidolonów należących do ArcheoPrzędz. Zrobiła też na cywilach z ArcheoPrzędz duże wrażenie.
* Olgierd Drongon: Klaudia go wrobiła, że niby interesuje się "Uśmiechniętą" - statkiem Luxuritias, który zaginął. Nie wie o tym nic.

### Frakcji

* .

## Zasługi

* Klaudia Stryk: znalazła ArcheoPrzędz a potem wymusiła z nich informacje; magicznie zasklepiła rozpadający się Goldarion a potem wymusiła na załodze Goldariona wyczyszczenie narkotyków.
* Elena Verlen: alias Mirokin; jak już przebiła się przez niesprawne Eidolony, pokazała co potrafi jako advancer - zinfiltrowała "Uśmiechniętą", zebrała informacje i zniszczyła Skażoną Bię.
* Martyn Hiwasser: negocjator i klej społeczny Klaudii i Eleny na Goldarionie. Ten, który deeskalował atmosferę i sprawił, że misja się jakoś udała. I stymulanty na Elenę. A potem - mnóstwo leczenia.
* Feliks Przędz: właściciel firmy na K1, ArcheoPrzędz; ledwo trzyma firmę na nogach. Współpracuje z Zespołem, by spełnić kontrakt ratunkowy - uratować piratów.
* Aleksander Leszert: kapitan Goldariona. mtg_WB. Lojalny swoim, chyba ostatni na Goldarionie który próbuje trzymać dyscyplinę i naprawdę zależy mu na tym statku.
* Adam Permin: oficer porządkowy Goldariona. mtg_B. Nieetyczny jak cholera, niechlujny - ale nie zgadza się na twarde narkotyki, jak długo dostanie inną formę zapłaty. Dba o statek po swojemu.
* SCA Goldarion: Transportowiec należący do ArcheoPrzędz, z załogą powiązaną z Siłami Wyzwolenia Fareil. Brudny i rozpadający się. Jeszcze bardziej uszkodzony na tej akcji, ale zdobył cenne cargo.
* SL Uśmiechnięta: Porzucony rok temu statek należący do Kamila Frederico; posiadała Skażoną Bię sprzężoną z wiłami. Rozbrojony przez Zespół, potem rozebrany przez ekipę remontową Goldariona.
* Oliwia Pietrova: poprzednia advancerka Goldariona/ArcheoPrzędz. Złapana i torturowana przez drakolitów, wprowadziła groźny wirus w Eidolona. Opuściła Goldariona po tej akcji.
* Semla d'Goldarion: TAI statku, która jest słabsza niż przeciętna Semla. Ledwo radzi sobie z Goldarionem... można powiedzieć, że Goldarion nie ma sprawnego TAI.
* Kamil Frederico: degenerat z Luxuritias. Właściciel "Uśmiechniętej", porzucił ją i uciekł z załogą. Miłośnik wił. Robił straszną krzywdę wszelkim damom, jakie pojawiały się na pokładzie tej jednostki.

### Frakcji

* ArcheoPrzędz: firma pozyskująca części z wraków na K1; weszli we współpracę z piratami Sił Wyzwolenia Fareil i udało im się wspólnie z Zespołem zdobyć skarby z "Uśmiechniętej".
* Siły Wyzwolenia Fareil: grupa piratów którzy podpadli Orbiterowi, operują niedaleko Neikatis; wpakowali się w "Uśmiechniętą" i musieli być ratowani przez ArcheoPrzędz.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria, Orbita
                1. Kontroler Pierwszy
                    1. Sektor Cywilny: miejsce, gdzie znajdują się firmy i cywile K1. Tu: Klaudia znalazła siedzibę ArcheoPrzędz.
                    1. Sektor 57: jeden z Czarnych Sektorów Kontrolera, dość odległy od zamieszkanych terenów

## Czas

* Opóźnienie: 4
* Dni: 6

## Inne

.
---
layout: cybermagic-konspekt
title:  "Kolonia w opałach"
threads: 
gm: kić
players: x1, x2, x3, x4, x5, x6
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* 

### Chronologiczna

* 

## Projektowanie sesji

### Pytania sesji

* Czy kolonia przetrwa?

DARK FUTURE: kolonia ludzka traci wszelkie zasoby, większość ludzi ginie lub się przekształca, reszta z trudnością trzyma się życia.

### Postacie
* Anatola - naukowiec, mag racjonalizujący. Cel: zrozumieć świat. Utrata kontroli: rzeczy nieprzewidziane.
* Chrom - inżynier. Cel: rozwijać kolonię, zdobyć sławę. Technomanta.
* Aurelia - kwiaciarka, eko-terrorystka. Utrata kontroli: eko-terror.
* Flamerod - szef ochrony, bezpieczeństwo kolonii, ludzie ponad wszystko, porywczy, najcięższe środki. Utrata kontroli: przedawkowanie, dziwne działanie.
* Joel - rolnik, zielarz-amator,mechanik, budowa gospodarstwa.

### Punkt startowy
Kiedy osadnicy przylecieli na Elorę, planeta wydała im się rajem. Piękna, porośnięta przyjazną człowiekowi roślinnością i z przyjaznym klimatem. Osadnicy wylądowali i zaczęli budować nowy dom. Kiedy rozłożyli statek, który ich tu przywiózł, droga powrotu została odcięta. Osadnicy zaczęli przygotowywać instalacje przemysłowe. Wtedy zaczęły się problemy...

### Podsumowanie

W środku nocy Zespół obudziły hałasy. To jakieś... rzeczy atakują i niszczą wyposażenie kolonii. Generatory, pojazdy... wszystko, co mechaniczne lub technologiczne. 
To... coś nie ma kształtu, najbliżej mu do poruszającej się plechy i zdecydowanie szkodzi technologii. 

Flamerod wezwał swój oddział, magicznie wzmacniając ludzi, ale przesadził (skonfliktowany sukces), co sprawiło, że o ile dużo skuteczniej ratowali wyposażenie, potem będą nie do użytku. Flamerodowi i jego oddziałowi udało się zabezpieczyć kluczowe lokalizacje i około połowy sprzętu. 
Aurelia uznała, że z roślinnością najlepiej walczyć inną roślinnością i postanowiła pokryć mechanizmy własnymi kwiatkami. O dziwo - zadziałało. Te urządzenia, które ona okryła, przestały być interesujące dla plechy.
Joel ocenił, że nie-kształty wydzielają coś w rodzaju kwasu. Jako rolnik postanowił zastosować oprysk. Jego umiejętności rolnicze i zdolność umiejętnego prowadzenia ciągnika (nie-kształty jeszcze go nie dorwały) pozwoliły mu uratować resztę urządzeń.
W środku zamieszania, niewzruszona Anatola zrobiła analizę atakujących bytów. Wykryła, że o ile owe nie-kształty same z siebie nie są świadome, są kierowane i zdecydowanie zorganizowane. 
W tym czasie Chrom zauważył postać w poszarpanym kombinezonie zataczającą się w swoją stronę. Wyglądał na górnika i zdecydowanie nie wyglądał na żywego (wgniecenie w czaszce nie ułatwia przetrwania)
Ta istota szła krok za krokiem w stronę Chroma i jęczała "Boli... Przestań... To boli..."
Przestraszony, wezwał Flameroda i wspólnie złapali "górnika".

Wszyscy uznali, że do rana mogą się zdrzemnąć.
Ogólnie - owocna noc. Większość wyposażenia kolonii została uratowana, a pojmany "górnik" został zamknięty w celi.

Rano okazało się, że górnik wrócił do bycia martwym. Cokolwiek nim kierowało, teraz już tego nie robiło.
Jednak sporo śladów wciąż było dostępnych. Ubiór identyfikował górnika jako pracownika pierwszej kopalni na planecie.

Zespół próbował się czegoś dowiedzieć i przeprowadzić jeszcze analizy. Kiedy się do tego zabierali, jeden z żołnierzy Flameroda wpadł do sali mówiąc o tłumie zbierającym się przed koszarami.

Aurelia postanowiła wyjść pierwsza, Flamerod za nią. Rzeczywiście, zebrany tłum szukał wyjaśnienia nocnych wydarzeń. A przecież nie mogą im powiedzieć o magii... Aurelia postanowiła przyjąć taktykę odwrócenia uwagi. Postanowiła dać dziennikarzom inny smakowity kąsek - zasugerowała pikantny romans pomiędzy nią a Flamerodem. A Flamerod podjął grę. Wspólnie udało im się odwrócić uwagę tłumu i go rozproszyć.

Kiedy ludzie się rozeszli, na placu pozostał tylko głosiciel apokalipsy.
"Dzień sądu nadchodzi! Przygotujcie się! Otwórzcie się na planetę! Dzień sądu nadchodzi!"

Zespół zabrał człowieka na przesłuchanie. Wyciągnęli z niego, że planeta cierpi, że trzeba się na nią otworzyć. Skąd to wie? Wie i już. A od kiedy wie? ...Od wizyty w lesie. Ale gdzie?!
Okazało się, że może ich tam zaprowadzić. 

Kiedy doszli na miejsce, kapłan przytulił się do drzewa. Anatola wyczuła energię polany, na której się znaleźli, ale była ona dość rozproszona. 

Wtedy Aurelia postanowiła sięgnąć przez rośliny i zjednoczyć się z planetą. Udało się jej to i poczuła ogromną świadomość. Nie złą, ale cierpiącą. Planeta nie zauważyła Aurelii. Wtedy czarodziejka postanowiła zwrócić na siebie uwagę świadomości planety. O dziwo (test heroiczny!) udało się jej. 

Poczuła, że boli ją nerka. 

Zespół postanowił wybrać się do pierwszej kopalni, z której przyszedł martwy górnik.
Tam znaleźli zawaloną sztolnię i udało im się uratować kilku górników, którzy dali radę przetrwać.

Teraz rozumieli, co się stało. 

### Wpływ na świat

* Kolonia na Elorze uczy się współżyć z planetą.
* Docelowo dla frakcji chcących odlecieć powstanie stacja orbitalna.
* Przy odrobinie pomocy z zewnątrz, dziennikarka nakrywa gubernatora w kompromitującej sytuacji, Zespół ma teraz haka i przejmuje kontrolę nad kolonią.
* Kolonia rozwinie sprzedaż (bio)technologii
* Na planecie powstaje szkoła kolonizacji świadomych planet.
* Film o kolonizacji Elory rozsławia Chroma
---
layout: cybermagic-konspekt
title: "Kurz po Ataienne"
threads: nemesis-pieknotki
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [190330 - Polowanie na Ataienne](190330-polowanie-na-ataienne)

### Chronologiczna

* [190330 - Polowanie na Ataienne](190330-polowanie-na-ataienne)

## Budowa sesji

Pytania:

1. Czy Romeo da radę przejrzeć Pięknotkę i dorwie się do prawdy? (4)
2. Czy nadal da się znaleźć Złote Szakale w okolicy Podwiertu? (5)

Wizja:

* Złote Szakale chcą usunąć stare ślady - z ludzi oraz z magów
* Orbiter chce zaprząc Pięknotkę; Szakale nie spodziewają się maga Pustogoru

Sceny:

* Scena 1: Rozmowa z Szymonem
* Scena 2: Wejście do bazy Szakali

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Misja właściwa

**Scena 1: Chevaleresse i Alan**

Alan był pierwszy. Zgnębiony. Uziemił Chevaleresse. A co gorsza, Eliza Ira zdobyła i zniszczyła Kryształowy Pałac w MMO. Więc - niech Pięknotka w jego imieniu porozmawia z Chevaleresse. Powiedział, że Lucjusz Blakenbauer ściągnął ją z Multivirtu farmakologicznie... i Chevaleresse bardzo ciężko to przeżyła. Nie wierzyła że to jest możliwe. Alan chce, by Pięknotka porozmawiała z Chevaleresse i jakoś wyrwała ją z tego stanu. She feels she can't escape. Alan chce, by Chevaleresse zrozumiała co zrobiła źle.

Chevaleresse w domu Alana. Siedzi i patrzy w okno i nie chce rozmawiać. Pięknotka powiedziała, że Ataienne nic nie jest; Chevaleresse bardzo podziękowała. Nadal jednak nie chce rozmawiać. Pięknotka jednak zmusiła ją do rozmowy - ogólnie, Chevaleresse uważa, że Alan zrobił jej krzywdę. Że ona miała rację i on - któremu ufała, JEDYNY mag któremu ufała - ją skrzywdził bezzasadnie. Żałuje, że tu przyjechała. To nie jest kwestia szlabanu. To kwestia czegoś dużo głębszego.

Pięknotka doszła do tego o co chodzi. Chevaleresse nie chce "uciec". Ona chce "odejść". Zarówno z gildii jak i z realu - od Alana. She lost her trust. Pięknotka próbuje ją przekonać - niech zostanie NA RAZIE. Alan nie chciał jej skrzywdzić. Nie o to mu chodziło. Spieprzył, no cóż, Alan pieprzy sprawy, ale zależy mu na niej. Pięknotka uratowała jej przyjaciółkę (+2) oraz jest w tym niezła (Tr:6,3,6=SS). Chevaleresse się zgodzi zostać jeszcze jakiś czas i dać Alanowi szansę - ale niech Alan ją przeprosi i odda jej "połączenie". Jest sama. Samotna. Jest samotna jak nigdy nie była.

Oki, czyli Chevaleresse trzeba by przeprosić. To trzeba Alana przekonać... na (Tp). No i Alan sam do niej przyszedł (+2). Zdaniem Pięknotki problem wziął się stąd, że Chevaleresse jest taką osobą, że w przeszłości dużo dostała. (Tp->SS). Alan się zgodził - ale Ataienne i Chevaleresse nie będą grały razem ani się przyjaźniły.

No i jakoś udało się Pięknotce doprowadzić do kruchego pokoju w domu Alana. Z trudem.

**Scena 2: Szymon i Ataienne**

Szymon jeszcze leży w łóżku, ale nie jest szczególnie groźny i w pełni energii. Spojrzał na Pięknotkę i westchnął ciężko. Czyli teraz sobie nie ufają i będzie z niego wyciągać informacje. Szymon powiedział: "Pięknotka Diakon. Wyraźnie nieufna wobec Orbitera. Lojalna patriotka, ale nie wierzy, że my też jesteśmy patriotami". Pięknotka chce wiedzieć, co się stało. Szymon odpowiedział. Opowiedział o Trzecim Raju i Elizie Irze. I o roli Ataienne - kontrolerki umysłów byłej załogi Elizy.

Czemu Orbiter wykorzystał człowieka? Ataienne musi się od czegoś odbić. Musi się czymś feedować; w tej chwili Orbiter nie ma sposobu by użyć klona lub nie-człowieka. Pięknotka nie docenia tego co się stało - nie powinno się wykorzystywać ludzi. Szymon odpowiedział, że Grażyna by nie żyła gdyby nie Orbiter. Pięknotka się tu nie zgadza z Szymonem.

Szymon powiedział Pięknotce, że ona wie, że próbowali ją zabić. Pięknotka uratowała jej życie. Szymon dokładnie to podkreślił i docenił. Gdyby Ataienne zginęła, Szymon musiałby eksterminować grupę niewinnych ludzi i magów - a nie chce.

Szymon powiedział też że ma coś dla Pięknotki - okazję do infiltracji grupy magów którzy zrobili jej to wszystko i zepsuli jej power suit. Dał jej też hipernetowy namiar na Ataienne; aczkolwiek Ataienne - jak powiedział - raczej żyje w Multivirt a nie w hipernecie.

Szymon powiedział też Pięknotce o tym, że ma dostęp do Złotych Szakali - grupy która w Pięknotkę uderzyła. A Pięknotka nie ma opinii "współpracuję z Orbiterem". Pięknotka - z tego co powiedział Szymon - jest postrzegana jako bliżej frakcji Saitaera ale przede wszystkim Pustogoru. Wyjaśnił jak Orbiter ją postrzega - ratunek Ataienne sprawił, że Pięknotka całkowicie nie może być powiązana z frakcją Pozostałości (siły Elizy).

Pięknotka spróbowała zrobić z niego kontakt. Mag Orbitera poza Szarjanem któremu może zaufać. (Tr:9,3,5=SS). Uda się, jeśli Orbiterowi Pięknotka pomoże w rozwiązaniu problemu "kto poluje na Ataienne", np. Złotych Szakali (czyli ich infiltracja).

**Scena 3: Pustogor i Szakale**

Pięknotka nadała temat Szakali Pustogorowi. Od ręki zostali zlokalizowani - mają pojazd w Podwiercie, na małym kosmoporcie (używany tylko prywatnie; za mały). Pięknotka poszła do "Małego Oblicza" (nazwa ich statku). A Szymon dał jej imię szefa - Romeo Węglas. Statku pilnuje młody agent. Zobaczył Pięknotkę i nie wie o co chodzi - a Pięknotka "take me to your leader". Pustogor wzywa.

Szybko Pięknotka trafiła przed oblicze Romea. Powiedziała mu, że sytuacja jest średnia. A ona może pomóc. Szymon się złamał. Powiedział jej, że zlecenie było na zniszczenie AI Orbitera, służącego do kontroli umysłów i ogólnie złego. Zrobił własny research i faktycznie, Ataienne pełni taką rolę. Nie miał nic przeciw temu by się tym zająć, zwłaszcza, że ich standardowa baza nie jest w Szczelińcu.

Pięknotka dostała dalsze kontakty; do przekazania do Orbitera. Dużo wskazuje na to, że faktycznie stoi za tym Eliza.

**Scena 4: Ataienne**

Pięknotka weszła do Multivirt, do świata Ataienne. Ataienne podziękowała Pięknotce za uratowanie życia. Pięknotka podeszła do przekonania Ataienne - niech ona wpłynie na Chevaleresse. Niech będzie z Chevaleresse trzymała tą samą linię co Pięknotka i Alan. Ataienne powiedziała, że Chevaleresse jest jej przyjaciółką. Nie może. Nie chce używać swoich umiejętności wobec przyjaciół.

...Pięknotka ma mindfuck. Co to znaczy... Ataienne to jest AI, co więcej - TAI...

Niech przynajmniej Ataienne nie namawia Chevaleresse do odejścia. Ataienne to obiecała. Pięknotka pożegnała się z Ataienne z pewną dozą smutku i przekonania, że Orbiter zbudował najdziwniejsze AI w historii...

Wpływ:

* Ż: .
* Zespół: .

**Sprawdzenie Torów** ():

2 Wpływu -> 50% +1 do toru. Tory:

* .

**Epilog**:

* 

### Wpływ na świat

| Kto           | Wolnych | Sumarycznie |
|---------------|---------|-------------|
|               |         |             |

Czyli:

* (K): .

## Streszczenie

Alan poprosił Pięknotkę o interwencję w sprawie tego, że uziemił Chevaleresse; Pięknotka opanowała chcącą opuścić Alana Chevaleresse. Potem Pięknotka nawiązała użyteczny kontakt w Szymonie z Orbitera, znalazła osoby odpowiedzialne za próbę zabicia Ataienne i porozmawiała z samą Ataienne; uznała, że ta konkretna AI jest zdecydowanie dziwna.

## Progresja

* .

### Frakcji

* .

## Zasługi

* Pięknotka Diakon: uspokoiła sytuację pomiędzy Chevaleresse i Alanem, dodatkowo doszła do porozumienia z Szymonem z Orbitera i dotarła do zleceniodawców eksterminacji Ataienne.
* Alan Bartozol: uziemił Chevaleresse; nie radzi sobie jednak z małolatą i poprosił Pięknotkę by była mediatorką. Oj.
* Diana Tevalier: straciła zaufanie do Alana (bo ją uziemił). Rozważała opuszczenie tego terenu, ale Pięknotka ją przekonała do pozostania.
* Ataienne: uważa Chevaleresse za przyjaciółkę i dlatego nie udało się Pięknotce jej przekonać do użycia swoich mocy by nieco ustabilizować Chevaleresse. Wdzięczna Pięknotce.
* Szymon Oporcznik: mag Orbitera, który nie jest wrogi Pięknotce. Opiekun Ataienne. Chciał zabrać ją na koncert; uważa siebie i Pięknotkę za patriotów.
* Romeo Węglas: oficer Złotych Szakali; złamał się po rozmowie z Pięknotką. Powiedział jej kto stał za zleceniem na zniszczenie Ataienne.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Multivirt
        1. MMO
            1. Kryształowy Pałac: upadł; przejęła nad nim kontrolę "Iglica", czyli Eliza Ira.
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Pustogor:
                                1. Miasteczko: w mieszkaniu Alana jest uziemiona ze szlabanem pewna nieszczęśliwa Chevaleresse.
                            1. Podwiert
                                1. Kosmoport: niewielki kosmoport cywilny; tam Pięknotka spotkała się z Szymonem, niedaleko jego statku

## Czas

* Opóźnienie: 3
* Dni: 2

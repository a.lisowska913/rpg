---
layout: cybermagic-konspekt
title:  "Eksperymenty na wiłach?"
threads: nemesis-pieknotki
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [190106 - A może pustogorska mafia?](190106-a-moze-pustogorska-mafia)

### Chronologiczna

* [190106 - A może pustogorska mafia?](190106-a-moze-pustogorska-mafia)

## Projektowanie sesji

### Pytania sesji

* Czy Pięknotce uda się zatrzymać proceder mafii w sprawie wił?
* Czy Pięknotka zatrzyma Białego Rycerza?

### Struktura sesji: Eksperymentalna eksploracja / generacja

* **Scena, Konsekwencja, Niestabilność, Opozycja, Trigger, Pytania**
* Mafia próbuje przewieźć wiły, Trzęsawisko jest złe, Mafia/Pięknotka, Mafia, Init (Dropship), ?
* Czy Pięknotce uda się odbić wiły z siedziby, wiły przewiezione są dalej, Rycerz/Pięknotka, Rycerz + Mafia, ?

### Hasła i uruchomienie

* Ż: (kontekst otoczenia) -
* Ż: (element niepasujący) -
* Ż: (przeszłość, kontekst) -

### Dark Future

1. ...

## Potencjalne pytania historyczne

* brak

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Misja właściwa

**Scena**: (21:20)

_Pustogor, hipernetowy komunikat_

Pięknotka dostała cynk od Alana. Jeśli Pięknotka jest zainteresowana wiłami i pomocą wiłom, w nocy będą wywiezione z Nieużytków Staszka gdzieś. Pięknotka się zdziwiła - skąd on wie? Blank stare. Ok, to jakie straże? Blank stare.

Pięknotka spytała, szczerze, co on chce by ona zrobiła. Alan powiedział wyraźnie - wiły pomogły uratować magów. Pięknotka też. Więc Alan mówi Pięknotce, bo chce w pewien sposób dać wiłom szansę. Pięknotka zna je najlepiej - wie czy warto o nie walczyć czy nie. Alan jest z wiłami i Pięknotką 'even'. Spłacił dług, jakkolwiek żadnego nie miał.

Ale on wie coś więcej. Pięknotka naciska (Tp:SS). Mafia zatrudniła kilka Czerwonych Myszy. Miał możliwość dołączenia. Odmówił. Jest tam ktoś jeszcze, ktoś w tle - ale sam Alan nie wie kto. Jest zirytowany, że Pięknotka naciskała i już żałuje.

_Zaczęstwo, Nieużytki Staszka, noc_

Ciężarówka. Cztery wiły, unieszkodliwione i spętane, trzech magów. I Pięknotka dobrze ukryta w rowie. Pięknotka ma Plan - bomba afrodyzjakowa, by ich unieszkodliwić, zwłaszcza z wiłami. Wiły drenują energię, więc powinno być lepiej i skuteczniej. Przed ich przyjazdem, rozłożyła afrodyzjak w różnych miejscach (Łt:11,3,1=SS). Udało jej się unieszkodliwić trzech magów, ale w samochodzie był czwarty. I miał power suit. Terminus. Pięknotka poznała bardziej "podstawowy" power suit - to nie jest terminus weteran.

Pięknotka widzi, jak terminus nieświadomy sytuacji planuje rozwiązanie problemu wił i Myszy. Najpewniej wezwie posiłki. Pięknotka już miała coś z tym robić, gdy nie musiała (Wpływ -2). Na scenie pojawił się piekielnie niebezpieczny pnączoszpon. Młody terminus najpewniej właśnie narobił w spodnie. Zrobił to, czego nie powinien był robić - otworzył ogień.

Pnączoszpon rzucił się na samochód. Pięknotka chce przeciągnąć walkę, ale nie chce śmierci terminusa - odpaliła power suit i ściągnęła ogień na siebie. Pięknotka chce być tą, która URATOWAŁA magów (TrZ:10,3,5=S). Pnączoszpon wykonał serię piekielnie niebezpiecznych ataków w Pięknotkę - ale ona dała radę wymanewrować.

Pięknotka ma do wykonania coś bardzo trudnego. Musi przetrwać atak pnączoszpona. Musi jednocześnie wyłączyć z akcji tamtego terminusa, by nikt nie wiedział, że to ona. Musi sprawić, by pnączoszpon nie skrzywdził tu nikogo poważnie. No i zdążyć przed posiłkami a jednocześnie wybić wiły z orgii by uciekły. Pięknotka jeszcze rozpaczliwie wspomaga się magią, by móc zniszczyć potwora i pomóc w swoich planach (HrZ+2:13,5,16=P,SS). Ślady magii pozwolą na zidentyfikowanie Pięknotki bez problemu. Są ranni i power suit Pięknotki ma kosmetyczne uszkodzenia. Ale wiły dały radę pouciekać. Pięknotka się uśmiechnęła i tylko czekała na "posiłki".

_Zaczęstwo, Nieużytki Staszka, noc_

Posiłki przybyły. Czterech ciężkich magów bojowych. Przyszli walczyć z pnączoszponem i wiłami a zastali Pięknotkę. Próbują ją przesłuchać (Łt:8,3,2=S). Bez problemu dała radę wszystko powiedzieć tak jak chciała i to co chciała. Myszy i mafia są przekonani, że Pięknotka nie jest tu problemem.

(-5 Wpływ: Przeciwnik zauważył korelację między afrodyzjakami i magią Pięknotki - nie może udowodnić, ale wie, że to ona)

Pięknotka dowiedziała się, że jest jeszcze kilka wił. Są porwane do głównej bazy w Podwiercie. Ale gdzie ta baza jest, na tym etapie diabli wiedzą...

Wpływ:

* Ż: 2 (7):
* K: 0 (2)

(22:33, pauza)

**Scena**: (16:14)

_Pustogor, hipernetowy komunikat_

Pięknotka spytała Alana odnośnie tego czemu jej powiedział o Myszach i wiłach. Alan odpowiedział, że nie jest fanem mafii i współpracy mafii z kimkolwiek. Nie chce by Pięknotka ucierpiała i jej interesy (wiły), ale on nie jest fanem wił. Więc - zostawił sprawy swojemu torowi.

_Pustogor, Barbakan_

(Kić: dług Wpływu: jest mag Myszy który zgłosił to terminusom bo mu się nie podobało)

Pięknotka poszła więc do Barbakanu. Może tam znajdzie jakieś informacje na temat tego punktu przerzutowego. Tam znalazła informacje bez problemu - jeden z magów Czerwonych Myszy zgłosił informacje o punkcie przerzutowym wił, bo mu się to nie podoba. Ale terminusi są zbyt zajęci a ten teren NALEŻY do Czerwonych Myszy, więc sygnał wrócił do nich. Trzy dni temu. Czyli najpewniej już jakaś wiła została wysłana.

Pięknotka korzysta z okazji i hipernetuje do tego maga Myszy który to zgłosił. Mag nazywa się Sławomir Muczarek. Pięknotka powiedziała, że jako terminuska zajmuje się tą sprawą. Sławek zapiszczał, że to już nieaktualne. Pięknotka zauważyła, że on może wycofać zgłoszenie - ale jeśli się zgodzi, chwała trafi do niego. (Tr:7,3,6=S). Pomoże Pięknotce i cała chwała jego, Pięknotka może zostać w cieniu jak chce. Powiedział jej wszystko:

* Lokalizacja tego miejsca w Podwiercie to te same magazyny gdzie był kiedyś Saitaer. Magazyny sprzętu ciężkiego.
* Jest tam niesamowicie niebezpieczny mag, nie stąd. Biały Rycerz. On wynajął Myszy - to chyba nie kwestia mafii.
* Mafia zajmuje się transportem, Rycerz robi badania na wiłach. Kilka zginęło. Rycerz robi badania biomantyczne.
* Nie ma aktualnych informacji, bo Myszy po tym jak on wygadał się, OPIERNICZYŁY GO z góry na dół. Więc Sławek już nie ma aktualnych danych
* Zwiększyli obronę - czterech magów Myszy oraz Biały Rycerz.
* Oficer (Biały Rycerz) próbował ustalić warty itp, ale Myszy powiedziały że sprawa jest pod kontrolą.

Biały Rycerz? Wygląda na to, że Pięknotkę czeka spotkanie w Barbakanie z rekordami... (TpZ:11,3,3=S). Pięknotka poznała tożsamość swojego oponenta. Mag nazywa się Kornel Garn. Faktycznie jest wojskowym, ale nie z Orbitera. Już nie jest wojskowym, tak powiedzmy. Jest zbyt ekstremistyczny - nie powinniśmy po prostu się obronić, powinniśmy wziąć wojnę w kierunku naszych przeciwników.

Elegancki mag, świetny oficer, specjalizuje się w walce wręcz ręcznie oraz w power suicie. Bada różnego rodzaju środki biologiczne, szukając rozwiązania. Ale cholera wie czego.

_Pustogor, hipernetowy komunikat_

Kolejny komunikat do Alana. Terminus jest coraz bardziej zirytowany Pięknotką. Pięknotka spokojnie zreferowała mu sytuację i powiedziała, że obecność Kornela to poważne zagrożenie dla terenu. A Myszy nie zdają sobie sprawy z kim weszły w układ. Eksperymentowanie w środku miasta jest wyjątkowo niebezpieczne dla Podwiertu - i Myszy mogą kosmicznie ucierpieć. Powiedziała, że sama nie stanie przeciwko militarnemu power suitowi. (Tp:11,3,3=S). Alan ciężko westchnął. Pomoże. Dowie się, jak wygląda sytuacja i zaatakuje to miejsce.

Powiedział Pięknotce, że on się zajmie pokonaniem przeciwnika. Niech ona zrobi to, co chce robić. Niech ona zostawi walkę "dorosłym". Pięknotka nie skomentowała walki na bagnie.

Wpływ:

* Ż: 7 (12): 
* K: 0 (2): 

Akcje: pierwsza wiła martwa, prototypowa wiła zaszczepiona, prototypowa wiła wywieziona, weryfikacja wstępnie pozytywna, wiły gotowe do wywiezienia, 

**Scena**: (17:26) - wojna

(Kić: dług Wpływu: niech zostało jakieś echo Saitaera i to jest śmiertelnie niebezpieczne w interakcji z wiłami)

Alan powiedział wyraźnie - nie można ich ostrzec. Alan wziął skrzydło szturmowe i po prostu uderzył. Myszy zaczęły uciekać. Biały Rycerz nie. Odpowiedział ogniem. Alan odpowiedział super celnym strzelaniem nieprawdopodobnie celnego działa. Biały Rycerz został ciężko uszkodzony od pierwszego strzału mimo pól siłowych itp. Alan zdemolował to co znajduje się w środku. Rycerz odpalił Czarną Flarę.

Perfekcyjne zmysły Alana kontra wspomaganie Kornela. Pięknotka próbuje znaleźć wiły i nie dać się ustrzelić. Zostało 6 wił. (Tr:6,3,6=P,) = Wiła Saitaera, budynek się rozpada, wiły poszły w miasto.

Pięknotka **poczuła** znaną sobie obecność Majestatu Saitaera. Bardziej poczuła niż zobaczyła zakrwawioną, umierającą wiłę (Alan nie oszczędzał) rekonstruowaną mocą Saitaera. Wiła-Terrorform...

Pięknotka zrobiła coś, czego nie myślała że zrobi. Wezwała Majestat Saitaera. Poprosiła go o uwagę. Saitaer odpowiedział, bo Pięknotka jest zaprawdę wybrana. Pięknotka powiedziała, że wiła właśnie ulega Rekonstrukcji. Poprosiła, by wiła nie była pełna nienawiści i złości. Saitaer powiedział, że ta wiła nie zrobi nikomu krzywdy JEŚLI Pięknotka da jej "safe passage". Saitaer dodatkowo dorzucił, że wezwie pozostałe pięć wił dla Pięknotki za tą usługę. Pięknotka podejrzewa podstęp, ale co robić?

Saitaer przebudował wiłę w jeszcze inną stronę i pokazał Pięknotce widok oczami wiły - Biały Rycerz pokonał Alana w walce. Jest gotowy do coup de grace. Pięknotka wykorzystała oczy wiły i strzeliła snajperką, by uratować Alana. Instynktownie (a może to Saitaer?) użyła magii i Saitaer złapał na nią _hold_ - rozpoczął ewolucję Pięknotki. Kornel został ciężko ranny. Saitaer rozproszył Czarną Flarę.

Alan spojrzał na Pięknotkę z przerażeniem, Kornel przygotował się do walki. Skażona Wiła zaśpiewała i wyłączyła wszystkich z akcji a Saitaer bezpośrednio zwrócił się do Kornela - Saitaer ma to, czego Kornel pragnął a Kornel nigdy nie będzie tego miał. Po chwili, Saitaer wygasił Skażenie rzeczywistości i przekształcił wiłę ponownie w coś mniej groźnego. Wezwał pozostałe wiły i Pięknotka błyskawicznie wzięła je ze sobą na bagno... (TrZM)

Pięknotka wraca na pełnej prędkości na miejsce wydarzeń. Saitaer nie pozwolił jej iść na bagna - to nie ten czas i nie to miejsce.

Pięknotka pobiegła wynosić wszystkich z budynku by nikomu nie stała się krzywda. Kornela tam już nie ma. Zniknął. Dla Pięknotki jest bardzo ważne co robił z tymi wiłami i czy wróci - więc przywołała pamięć z połączenia by odnaleźć cele Kornela i co zrobił z wiłami. Pięknotka skupiła w sobie energię Saitaera i zajrzała w Kornela (TpZ:S). Pięknotka zobaczyła co następuje:

* Kornel ma małą posiadłość należącą do kogoś innego. Tam ma różnego rodzaju istoty na których eksperymentuje.
* Kornel szuka możliwości stworzenia "kwiatów Iscantera". Hipnotycznych kwiatów do budowy potężnego pola pryzmatycznego.
* Kornel jest pewny, że wojna powróci. I chce być gotowy. Wiły są istotami, które powinny pomóc w tych kwiatach - ale to nie działało.
* Wstępne badania wił z Trzęsawiska pomogły. Teraz Kornel wie czemu - bo te wiły były Dotknięte energią Saitaera. Tylko wiły stąd mogą mu pomóc.
* Kornel przetransportował już koło 10 wił do tamtej posiadłości. Jest daleko poza zasięgiem Pustogoru.
* Kornel jest wstrząśnięty. Nie wie co myśleć i co się dzieje, kontakt z Saitaerem wpłynął na niego jak kontakt z Arazille na Pięknotkę.
* Pięknotka poczuła jego myśli. Jest to żołnierz walczący w wojnie, która się już skończyła, który nie wierzy w ten koniec. Żyje w swoim świecie.

(Żółw: -5: Kornel zobaczył tą moc której szukał. Chce tam dotrzeć przez Saitaera)
(Żółw: -5: Saitaer dał radę stworzyć swój ołtarz.)

Wpływ:

* Ż: 3 (17): 
* K: 4 (6): 

Akcje: pierwsza wiła martwa, prototypowa wiła zaszczepiona, prototypowa wiła wywieziona, weryfikacja wstępnie pozytywna, wiły gotowe do wywiezienia, wystarczająca porcja wywiezionych wił.

**Epilog** (18:30)

* Pięknotka musiała być Odkażona. Jej ciało stopiło się z jej Power Suitem. Stali się jednością. Pięknotka fizycznie była terrorformem, acz mentalnie nie.
* Karla była niezadowolona z Pięknotki, zwłaszcza, że Pięknotka potrzebowała Odkażenia przez Karradraela. Ponownie. Tym razem nie była oporna, ale nie była też szczególnie chętna.
* Nikomu nic się nie stało, o dziwo.
* Myszy jako gildia się rozpadły. To były Myszy vs Myszy. To była ostatnia kropla która zniszczyła gildię. Zwłaszcza z Saitaerem.
* To miejsce zostało odgrodzone przez terminusów. Nie występuje tu Skażenie Saitaera, ale lepiej poczekać chwilę - profilaktyka.
* Pięknotka nadal ma połączenie z Saitaerem spowodowane przez Toń.

### Wpływ na świat

| Kto           | Wolnych | Sumarycznie |
|---------------|---------|-------------|
| Żółw          |   3     |     17      |
| Kić           |   8     |     10      |

Czyli:

* (K): Saitaer się przebudził, ale nie ma mocy. Ani wyznawców, ani źródła energii (3).
* (K): Moktar i Łyse Psy idą polować na Kornela (3), ale on ucieka - wszystko traci ale nie wpada w ręce Moktara (3).

## Streszczenie

Alan powiedział Pięknotce, że Myszy polują na wiły dla kogoś. Okazało się, że były wojskowy (Kornel) eksperymentuje na wiłach by stworzyć potężną pryzmatyczną broń defensywną w formie kwiatów. Niestety, to dzieje się na terenie na którym był Saitaer i Władca Rekonstrukcji powrócił. Nadal słaby, ale zintegrował się z Pięknotką i zbudował ołtarz na Trzęsawisku. Kornel uciekł na swój teren a cała akcja skończyła się rozpadem Myszy.

## Progresja

* Kornel Garn: ma dość wił i materiału by Kontynuować pracę nad Kwiatami Hipnotycznymi w swoim laboratorium.

### Frakcji

* Czerwone Myszy: frakcja została zniszczona. Walka Myszy z Myszami i sprawa z Saitaerem ich trwale zniszczyły.
* Saitaer: na Trzęsawisku Zjawosztup znajduje się jego ołtarz, sam Saitaer jest przebudzony oraz Kornel jest nim zafascynowany.

## Zasługi

* Pięknotka Diakon: poszła tropem przerzucanych w dziwne miejsce wił a skończyła na modlitwie do Saitaera, by tylko rekonstruowana wiła nie zabiła wszystkich...
* Alan Bartozol: niechętnie, ale dał się wpakować w akcję przeciwko własnej gildii z uwagi na ryzyko dla Pustogoru jeśli Saitaer skazi Trzęsawisko. Prawie pokonał Kornela.
* Sławomir Muczarek: Członek Czerwonych Myszy, któremu nie podobało się przemycanie wił i uznał to za niebezpieczne. Przez to zostawił Pięknotce ślad.
* Kornel Garn: były wojskowy ekstremista badający wiły i świetnie walczący z Alanem. Zobaczył majestat Saitaera i także chce stać się bogiem. Prawie pokonał Alana.
* Saitaer: obudzony przez krew umierającej wiły i modlitwę Pięknotki, zbudował ołtarz na Trzęsawisku Zjawosztup poświęcając odbudowaną wiłę.

## Plany

* 

### Frakcji

* Łyse Psy: mają Kornela Garna na celowniku. Chcą go zdobyć dla Moktara jako kolejnego silnego agenta.

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Pustogor
                                1. Barbakan: centrala dowodzenia Pięknotki - wszystkie komunikaty hipernetowe i wiedza Pustogoru dostępna każdemu terminusowi.
                            1. Zaczęstwo
                                1. Nieużytki Staszka: miejsce w którym przygotowywane były wiły do transportu do Podwiertu. Pięknotka elegancko usunęła tamten problem.
                            1. Podwiert
                                1. Magazyny sprzętu ciężkiego: miejsce, gdzie wiły miały być transportowane do bazy Kornela. Niestety, też miejsce manifestacji Saitaera.
                        1. Trzęsawisko Zjawosztup: zainfekowane Ołtarzem Saitaera.

## Czas

* Opóźnienie: 2
* Dni: 3

## Narzędzia MG

### Budowa sesji

**SCENA:**: Nie aplikuje

### Omówienie celu

* nic

## Wykorzystana mechanika

1811

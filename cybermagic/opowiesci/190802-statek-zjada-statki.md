---
layout: cybermagic-konspekt
title: "Statek zjada statki"
threads: cien-serenit
gm: żółw
players: kić, vizzdoom, voldy
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [190802 - Statek zjada statki](190802-statek-zjada-statki)

### Chronologiczna

* [190802 - Statek zjada statki](190802-statek-zjada-statki)

## Budowa sesji

### Stan aktualny

* .

### Pytania

1. Czy uda się kogokolwiek uratować?
2. Czy uda się uratować advancera?
3. Czy ekspedycja przetrwa?

### Wizja

* Serenit kraloth-human-ai hybrid overmind
* korupcja Michała przez Hybrydę
* Statek pożerający i asymilujący statki
* dotyk Saitaera?

Nie jest to grupa bojowa Orbitera. To niewielki statek 7 fachowców chcących uratować maga Orbitera dla nagrody i sympatii władz.

Są tu trzy frakcje (dwie jawne):

* Serenit, kralotycznie dostosowana załoga oryginalnego statku; cyber-trianai; 1 byt (~15 osób)
* Technicy, rozbitkowie z 3 statków, walczących przeciwko Botanikom o kontrolę nad Serenitem; 60 osób
    * magowie: Aida (już nie), Adam
* Botanicy, rozbitkowie z 4 statków, walczących przeciwko Technikom o kontrolę nad Serenitem; 90 osób
    * magowie: brak; zasymilowani

I viciniusy:

* Marcadorian
* Ainshker
* Hybrydy "kontrolowane" przez frakcje

### Tory

Czasu:

* Stracony Mikado: nic (5), start asymilacji (6), atak (9), asymilacja (12), utrata TAI (15), utrata Mikado (20)
* Skok Serenita: nic (10), apokalipsa (12), skok (20)

Potencjalne akcje:

* Ratunek Michała: osłabienie (Tr), przebudzenie (TrK), przekazanie kontroli (TrK)
* Marcadorian Nieśmiertelny: ranny (Tr, Tr), ucieka (TrK), martwy (Hr)
* Overmind Serenit: odparty (Hr), ranny (Hr), krytycznie ranny (Hr), zniszczony (Hr+)

Sceny:

* Scena 0: wiadomość Michała do Orbitera
* Scena 1: ratowanie poświęconego Adama przed Marcadorianem Nieśmiertelnym.
* Scena X: szukanie Aidy
* Scena X: starcie o jedzenie (okolice life support)

## Punkt zerowy

### Statek

* Silniki (?)
* Life Support (Serenit - 1)
* Sensory (zew) (Technicy)
* Living Quarters - "Technicy"
* Living Quarters - "Botanicy"
* Medbay (Botanicy)
* Cargo (trianai)
* Lounge (?)
* Nawigacja (Marcadorian)
* Mostek (?)
* AI Core (Serenit - 2)

### Postacie

* Adam Zachodek: poświęcony "bogom" statku, Marcadorianowi Nieśmiertelnemu (żyje niedaleko )

### Opis sytuacji

Statek "Serenit" został zinfiltrowany przez advancera - Michała Dusiciela. Advancer ten uratował czarodziejkę, Aidę, i pomógł jej się wydostać i uciec; sam jednak został złapany przez overlordów tego na wpół-żywego statku. Zdążył jednak, jak to agent wysokiej klasy, wysłać sygnał "statek zjada statki i ucieka" oraz "ukryłem wszystko w czarodziejce Aidzie".

Statek "Serenit" pożarł swoją ofiarę - Michała - i zdecydował się uciec. Ale na drodze stanęła mu ekspedycja Orbitera - pięciu magów oraz 15 osób. Na statku mają wspomaganie w formie 2 militarnych konstruminusów klasy Witriol.

## Misja właściwa

Scena Zero

Zrozpaczony Michał Dusielec, Advancer Orbitera jest zarażony. Jest na pokładzie Serenita i został pokonany przez lokalne COŚ. Niedaleko niego są dwie hybrydy - kombinacje ludzi i czegoś. Michał nie jest w stanie sensownie wysłać sygnału, nie jest w stanie sensownie zrobić niczego. Ale zmylił hybrydy - biegnie do skrzydła medycznego, by natychmiast odbić do inżynierii. Tam wysadził jakieś butle i rozerwał pancerz Serenita - dostał się na powierzchnię asteroidy statku RAMA (Reconstituting Asteroids into Mechanical Automata). natychmiast zrobił wszystko, co w jego mocy - skupił się na swoich przyjaciołach z Mikado i wysłał sygnał, że "Statek pożera statki a potem skacze". Po czym pokonała go infekcja - dołączył do sił Serenita.

The context:

Why did the advancer go alone? The other had an opportunity to explore other sheets and he wanted to go alone to get something for Laura, his girlfriend to be. In the meantime they have managed to found a piece of Serenit, potentially having a weapon marks. The two NPC’s onboard the ship – Laura, the geologist and the medic, specializing in animals forms of matter after the Eclipse and Bob. We know nothing about Bob yet. There is no other ship nearby, except a distant Castigator artillery ship from Orbiter.

The team:

They use a ship named Mikado. That is one old and ugly ship: but sturdy and trustworthy. This ship chose Stella as its captain; usually pilots weren’t able to work with this ship. A lot has to do with extremely sarcastic tactical artificial intelligence called Eva, an old model with a great sense of superiority and not operating 100% normally.
A usual course of action for this team was to explore their relationships and save survivors if possible, do things related to primary mission of Orbiter. They are not really connected with Orbiter, but if this faction is Dale best option to survive and to have a place in this new broken world.

Stella is a newer connected pilot also working as a scrapper. Travis is a tactical officer, a fortifier and the perfect engineer of destruction. Ignaś is a stealthy street rat, a quartermaster. And Laura is in a mutual love with Michael but both of them did not ever confess their feelings. How cute. And now Michael might die on Serenit.

The session:

The team has received a communication from Michael. At that point he was already delirious and they know it. The team listened to a desperate babbling about a ship which devours other ships and jumps around and about infection and it is dangerous. No, Michael has never said anything very coherent that point. Laura excused herself, shocked that an advancer can have so much problems – but in reality to cry. The team decided they have to save their crewmate.

They need firepower. They asked Eva to clone herself and implant a new strain of her lovable personality into Vitriol-class battle konstruminus. Eva was slightly upset because of the names on Vitriols – Pitbull and Terrier. Still, she got convinced that they need firepower. So the crack team of: Laura, Eva, Terrier, Stella, Travis, Ignaś embarked on an intrusion pod and get sent on to the RAMA ship.

Yes, they were launched. After the „ship devouring ships” stuff they have decided that Mikado might be too vulnerable to dock or stay too close to the place. They are the initial plan was to make a swift, silent intrusion. Of course everything went to hell when they have penetrated the outer core of the asteroid and they entered the ship.

On entering they have noticed a guy tied to a pole and a large insect like monster hovering above him to maim and devour the poor guy. All the plans about silent intrusion went to hell, they have decided to save the guy to extract some information and get some allies. Ignaś stealthily moved towards the guy while Travis positioned two Vitriols as artillery support. When Travis open fire with everything he had, Ignaś untied and repositioned the victim towards the safer team. Unfortunately, the monster hurt the guy while he was escaping; nasty wound. Travis supported with combat droids (Vitriols) not only damaged the monster but also used his magic to eradicate the opponent. The monster got ripped apart and shattered. Unfortunately, this act is awakened something asleep inside Serenit.

The ghosts wounded so Laura gave him first aid and revived him. Their bio-scanners showed that the guy has some sort of infection; Laura advise them not to decompress the power suits, not eat anything, not use life support of this ship. Anyway, the guy woke up, said his name is Aiden and told them he is a victim sacrifice that piece a God who is immortal. When the team noticed that the guard was destroyed Aiden said that the guard will return. It was impossible to persuade him otherwise. Anyway, aside from that, he tries to be as useful as possible in the situation.

In Michael’s communication there was a phrase „I have hidden it all in Aida, she is pure, she is uninfected, she knows it all she has it all”, so naturally the team asked about Aida. Aiden knows her – she is one person from this ship, from this faction. She is lovely and helps everyone and lately there was that guy in a strange power suit who was very close to her. Of course, it was about Michael. So basically, Aida and Michael went closer towards the bridge. The team decided to go there to see how decision looks like and find both Aida and Michael.

Moving there they have noticed something interesting – there is a special type of a wall which should not really be there. After testing is experimenting with a bit the team has noticed that this wall is quite recent. It seems like there was another ship inside. Of course, searching for people means exploring places like this - they opened the wall using the devastation and entered the ship inside a ship.

The ship was named Cranis. Using Stella’s knowledge, Cranis was a small ship belonging to Noctis; not Orbiter material. But Stella knew the story, especially having the support of Eva – at one point Cranis responded to an SOS signal, then it’s tactical artificial intelligence sent the self-destruct signal and then (after self-destruct which should not be possible) sent the plague signal - no one may ever approach. That was the final communication from Cranis, eight years ago.

Everything sounds fishy so Travis decided to fortify the position. Laura got sent to local laboratory to understand the „God” they have killed and Aiden’s blood too. Stella revived local TAI (tactical artificial intelligence) named Sia. It wasn’t that easy, it seemed as if people on board tried to damage Sia’s AI core.

Sia revived. For revived in federal position and asked them with panic why have they come here and why have they will animated her – she is already lost. After several stern words from the team (Orbiter it’s not a fan of Noctis), and especially several sarcastic words from Eva, Sia explained she is corrupted. More like infected. Something on this ship is able to infect psychotronic matrix of an AI as well as infect people. Sia explained they tried to help this ship but this only ended in this ship being assimilated and devoured by the larger craft. She explained that magi have been connected and mixed into one ugly, anomalous form; merged with an AI core of Serenit. There were some types of kraloth-like mechanisms connected to all that.

So people are used as batteries for an overmind and magi are being assimilated into an overmind. As for AI – AI is also assimilated. Sia destroyed everything she could so her knowledge doesn’t fall into Serenit’s overmind.

Sia was slowly losing control. It was too late for her. Even as a noctian AI, she did not want this team to be assimilated by Serenit. She asked them to connect her to sensors of Cranis. This should be enough for Sia to find Michael and everyone there looking for. They have done that. Sia managed to give them the information – overmind is inside AI core, there are at least two more monsters like the one Travis has killed, Michael is in the life-support system (being prepared for assimilation) and Aida is located in a small assimilated ship near medbay.

While Sia was doing her best and sacrificing her humanity Eva was quite sarcastic towards her. Sia asked Eva how does Sia know Eva’s name. Eva was slightly dumbfounded. Sia explained that as Michael is being assimilated and he was twined with Eva, Eva’s strain lives inside overmind now. Eva shall fall. They all shall fall. Having said that, Sia lost it. She succumbed to the overmind and Serenit attacked.

A major problem – all the fortifications built to protect Cranis from external stuff just became enemies of the team. Using superior firepower of Vitriols they have managed to escape Cranis to the corridor of Serenit; however, in the meantime, Laura’s power suit got decompressed. Laura is infected. But due to the nature of the infection she does not know she is infected; Ignaś does, though. He informed everyone via hypernet about it. Except Laura, naturally.

Travis asked Laura what are the symptoms and how does infection work. She explained – overmind can see through the eyes of infected and with time overmind is able to use its powers through the vessel. Aiden is infected, said Laura. They have to ditch him so he does not become a spy. From what she said, Vitriols are extremely vulnerable to the infection – a mage has antibodies and mage’s body fights against the infection. A machine has nothing like that. This also means Eva is endangered. Of course, our beloved sarcastic AI scoffed at that.

The team decided to send Laura away. Laura and Aiden are supposed to go to the medical bay. Eva did tell the team that they have to send Terrier with them (Eva inhabits Pitbull) and they shoot tell Laura that the team is going to save Michael - that way overmind will send forces to wrong location.

Stella, still linked with Mikado, felt that the ship opened fire. There are small things, piece of rock, leaving Serenit and attempting to infect Mikado. The team doesn’t have much time. They went to save Aida, but everything seems like they don’t have enough time to be able to save Michael - and what is worse, they will lose Laura, especially if Terrier (being 50% of firepower) falls to the overmind.

It wasn’t easy to enter „Grand Vizier” - a small yacht captured and devoured by Serenit. There are people from Aiden’s tribe around. Stella built a small diversion using a very very wickedly smelly food (Ignaś is a walking repository of THOSE STRANGE THINGS). Then, Ignaś infiltrated Grand Vizier and woke the sorceress. Aida was very confused – she expected Michael. Ignaś explained, however, that they will meet with Michael later. For now they have to go.

When they left Eva asked Travis aside. She told him she is not Sia; she is not an old model. But she does not control Pitbull well enough. Her thoughts are starting to be cloudy, which implies psychotronic matrix might get corrupted eventually. Of course, Eva is trying to hide it all behind sarcasm and bravado, but she is deathly scared. She told Travis she is going to run into AI core and try to destroy the over mind. But, really, she asks him to shoot her in the back so she doesn’t see. And the next Vitriol she is going to inhabit is better named „Hero” or something like that…

Travis could not shoot her in the back. It was simply...

Stella decided she has to do something. She’s not returning without Laura, Michael, Eva… She decided to exploit the potential weakness of an overmind. If an overmind is supposed to be the core of Serenit, she is a neuro-pilot. This means she might be able to incarnate Serenit for a moment. This should cause confusion inside the overmind. She will fall, but it might give enough time for everyone.

She did attempt that (heroic conflict). And she succeeded. Overmind got into a partial chaos. Using that, Eva ran with guns blazing to corrupt as much as she can from this cursed AI core. (heroic -> Success). She entered AI core and used full firepower of Pitbull. Not enough to destroy or even heavily damage Serenit. Not enough to avoid being assimilated. Enough to buy time for the team to save Laura and Michael and get the hell out that cursed ship…

Commentary

Long time ago, there was a RAMA type of ship cruising through the sector, Serenit. It was a normal ship; but in the BioLab there were kraloths. At one point not very far from here Saitaer, Lord of Evolution and Adaptation shined his light. A bit of his light touched Serenit. Not much, but enough to activate the kraloth and corrupt Serenit irrecoverably.

Not very far away there was another ship. Mikado, when old and not too efficient. TAI. Light of Saitaer touched the ship and changed that TAI into Eva we know and love…

This was not an intention of the Lord of Evolution. It was just a side effect. Yet, two ships’ stories became intertwined.

**Sprawdzenie Torów** ():

* 20/20. Gdyby ostatnia akcja się nie powiodła (25%!), koniec sesji byłby zupełnie inny.

**Epilog**:

* .

## Streszczenie

Na cmentarzysku statków niedaleko Astorii przyczaił się tajemniczy statek asteroidalny imieniem Serenit. Jest to statek pożerający statki, zmieniony Światłem Saitaera. Zespół z Mikado zinfiltrował Serenit, by uratować swojego advancera; wydostali się stamtąd z Aidą (żywą odtrutką na asymilację Serenita). Serenit jednak stał się groźniejszy.

## Progresja

* Serenit: po asymilacji straina Evy stał się groźniejszy, bo zintegrował inny ułamek energii Saitaera.
* Stella Koral: stała się współdzielącym z Evą bytem zamieszkującym Mikado. Mikado ma teraz dwóch pilotów.
* Aida Serenit: emanuje energią którą wyczuwa Saitaer.

### Frakcji

* Orbiter: dowiaduje się o niebezpiecznym żywym statku Serenit. Nie dowiaduje się o tym, że Eva / Stella kontrolują Mikado.
* Orbiter: posiada odtrutkę na asymilację Serenita dzięki zbadaniu Aidy Serenit.

## Zasługi

* Stella Koral: neuropilot i scrapper Mikado; zintegrowała się z Serenitem by kupić czas Zespołowi na pokonanie Overminda i ewakuację Laury, Aidy i Michała na Mikado.
* Travis Longhorn: inżynier zniszczenia i fortyfikator Mikado; rozwalił modliszkoidalnego "boga" oraz zaprzyjaźnił się z Evą - co wydawało się niemożliwe.
* Ignaś Orbita: niepozorny szczur i infiltrator pochodzącym "skądś"; wspiera Mikado. Wyciągnął wpierw Adama a potem Aidę z zagrożenia - mistrz infiltracji i nieźle gada.
* Michał Dusiciel: advancer Mikado zakochany w Laurze. Chciał jej zrobić prezent i przynieść coś z Serenita; skończyło się na 
* Laura Prunal: medyk i geolog Mikado zakochana w Michale.
* Aida Serenit: żywa, chodząca szczepionka przeciwko asymilacji Serenita. Uratowana z Serenita przez Zespół dzięki advancerowi Michałowi oraz reszcie Zespołu.
* Eva d'Mikado: TAI Mikado, sarkastyczna i złośliwa. Jej strain sterował Witriolem; uległa częściowej asymilacji przez Serenit i ten strain dał się zasymilować by uratować swój Zespół.
* Sia d'Cranis: KIA. Noktiańska TAI Sia, która uległa częściowej asymilacji przez Serenit. Poświęciła się by pomóc astoriańskiemu Zespołowi odnaleźć Aidę i Michała.
* SC Mikado: statek powiązany z Orbiterem, który nie jest przez nich kontrolowany; TAI Eva/Stella. Sekretem Mikado jest to, że Eva jest żywą TAI przez Rozbłysk Saitaera w przeszłości.
* AK Serenit: statek-asteroida (RAMA), kontrolowany przez saitaero-kralotycznego Overminda, asymilujący statki i integrujący ze sobą magów z AI. Skrajnie niebezpieczny.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Obłok Lirański
                1. Anomalia Kolapsu
                    1. Cmentarzysko Statków: miejsce w przestrzeni Astorii, gdzie znajdują się wraki po wojnie; przyczaił się tam Serenit którego znalazł Mikado

## Czas

* Chronologia: Inwazja Noctis
* Opóźnienie: 2517
* Dni: 3

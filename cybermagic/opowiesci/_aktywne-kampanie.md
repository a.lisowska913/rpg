# Jakie mamy kampanie?

## 1. Córka Morlana

* Kodowanie: corka-morlana
* Init_rl: 210810-porwanie-na-gwiezdnym-motylu
* O czym
    * Nataniel Morlan rozpaczliwie próbujący odzyskać córkę i Antonella Temaris próbująca odzyskać siebie
    * Lokalizacja: głównie Eternia / kosmos
* Theme
    * Complete Monster, Pure of Heart
        * Caramon & Raistlin "taka wielka nienawiść... taka wielka miłość"
        * Caduceus Beetlestone & Demmi Beetlestone "like father, like daughter"

## 2. Niestabilna Brama


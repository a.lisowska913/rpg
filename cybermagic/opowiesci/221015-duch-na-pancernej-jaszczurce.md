---
layout: cybermagic-konspekt
title: "Duch na Pancernej Jaszczurce"
threads: historia-klaudii, grupa-wydzielona-serbinius
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [220925 - Młodziaki na Savarańskim statku handlowym](220925-mlodziaki-na-savaranskim-statku-handlowym)

### Chronologiczna

* [220925 - Młodziaki na Savarańskim statku handlowym](220925-mlodziaki-na-savaranskim-statku-handlowym)

## Plan sesji
### Theme & Vision

* Demonstracja jak działają silniki warp i jak powstają niektóre jednostki Orbitera
* Nie każda jednostka pochodząca z Anomalii Kolapsu jest bezpieczna
* Nie każdy duch jest szkodliwy; duty forever.
    * "I have betrayed all I believed. No rest, just protection."

### Co się wydarzyło

KIEDYŚ:

* Statek nazywał się Korielito. Była to jednostka transportowa dalekiego zasięgu (Ralatejska). Przechwycona przez wrogą siłę (Idritów), złapana i zniewolona.
* Ralatejska załoga oddała Korielito Idritom. Arentia zmieniła stronę, by chronić życie i bezpieczeństwo swojej pani.
* Po pewnym czasie (5 miesięcy życia jednostki) Idrici stwierdzili, że Korielito posłuży lepiej jako statek samobójczy. Zamontowali Technowady w Korielito
* Arentia zmieniła stronę. Jak byli w Nierzeczywistości, sabotowała jednostkę i skazała wszystkich na śmierć. Sama stała się duchem nawiedzającym Technowady.
* Jednostka z Nierzeczywistości trafiła docelowo do Anomalii Kolapsu.

TERAZ:

* Pancerna Jaszczurka to OO, pochodzący z Anomalii Kolapsu i dostosowany z innych jednostek z Kolapsu; jedna z jednostek odzyskanych.
* "Jaszczurka" miała swoją maiden mission. The ship is haunted. Cywile z K1 zażądali oddania Jaszczurki do naprawy.
* Technicy Orbitera nic nie znaleźli na podstawowym poziomie
    * -> Ignacy Szarjan + Klaudia Stryk
    * weterani odpalają Jaszczurkę i robią nią lot
* Arentia widzi wszystkich jako Idritów. **Zawsze** próbuje zatrzymać statek przed dotarciem do celu i eksterminacją innych Ralatian.

### Fiszki

* SC Pancerna Jaszczurka
    * kiedyś: OO, pochodzący z Anomalii Kolapsu i dostosowany z innych jednostek z Kolapsu; jedna z jednostek odzyskanych
    * "Jaszczurka" miała swoją maiden mission. The ship seems haunted. Cywile z K1 zażądali oddania Jaszczurki do naprawy.

### Co się stanie

* S1: Klaudia jako ekspert od anomalii -> Pancerna Jaszczurka. Nikomu nie udało się nic znaleźć. Zapoznaje się z Ignacym Szarjanem który jest maksymalnie zirytowany.
* S2: Wchodzą w warp
    * opętując inżynierów Arentia destabilizuje silniki
    * wpływa na Dominika; -> Michalina, -> ostrzelić wszystko; na Sonię -> odrzucić jednostkę, wrócić do domu (na Michalinę i Ignacego nic nie ma.)
    * osobiście walczy
    * destabilizuje kierunek
* S3: Usuwają Arentię
    * pojawiają się Technowady
    * ewakuacja

### Sukces graczy

* Redukcja strat na Pancernej Jaszczurce.
* Doprowadzenie Pancernej Jaszczurki do stanu aktywnego (Wyczyszczenie Technowadów (dormant), usunięcie Arentii)

## Sesja właściwa
### Scena Zero - impl

Klaudia stoi na pokładzie K1. Patrzy na niezbyt imponującą jednostkę - nazywa się "Pancerna Jaszczurka". Wygląda jak jakiś patchwork, ale solidny. Statek cywilny, nie Orbitera. Ale są tu technicy Orbitera - i Klaudia. Bo podobno ta jednostka, sprzedana cywilom przez Orbiter - jest nawiedzona. Stąd ekspert od anomalii - Klaudia. "Celofyzy nie istnieją, ale anomalie się zdarzają".

Na pokładzie Jaszczurki już toczy się praca - szef ochrony, marines, kapitan na pokładzie. Wszystko z Orbitera. I czekają tylko na Klaudię (która się nie spóźniła) i inżyniera (który się spóźnia). Dwie minuty później oczom Klaudii objawia się BIEGNĄCY inżynier na pokład jednostki. Sierżant marines nie ukrywa uśmiechu. Ignacy się przedstawił - Ignacy Szarjan, z eksperymentalnej stoczni. Klaudii podejrzenia że Coś Jest Nie Tak właśnie wzrosły.

Ignacy wyjął narzędzia badawcze i zaczął szukać śladów duchów. Czy czegokolwiek. Ma sensowny sprzęt.

Zadanie Ignacego i Klaudii. I reszty statku - "macie usunąć duchy". Nieważne czy są. Teraz to problem Michaliny. Michalina (stary wyjadacz - oblatywacz) ma mikrofilmy z raportami od ludzi, ma robić śledztwo itp. A Dominik, jej szef ochrony jest jej kolegą.

Zanim pójdą po śladach "omg duchy" szuka anomalii, dysfunkcje AI itp. Odpierniczające AI może wyglądać jak duchy. Na pierwszy rzut oka Klaudia nie widzi kłopotów na linii AI itp - poczciwa Semla działa prawidłowo, acz wyraźnie jest świeżym transplantem (bo statek dość świeży). Sam statek został sprzedany 2 miesiące temu. I wtedy zamontowali Semlę. Niski koszt energii, niskie zużycie, mało problemów. Jednostka jest dalekosiężnym transportowcem. "Ciężarówka".

Klaudia pyta jak się objawiają nawiedzenia. Michalina daje mikrofilmy i dokumenty nie mówiąc ANI SŁOWA. "Lepiej, żebyś sama zobaczyła, bo... aż trudno uwierzyć. Chcę, byś sama wyrobiła opinię."

* Pancerna Jaszczurka miała 10 lotów z 4 załogami.
* Kluczem do działania dalekosiężnego transportu są potężne silniki warp. Szybko się porusza i mało pali.
* ZA KAŻDYM RAZEM pilot / nawigator / ktoś wyłączał warp za wcześnie. Z uwagi na to jak działa warp, Jaszczurka NIGDY nie dotarła do celu.
    * nieważne, czy załoga się zmienia itp.
* Inżynierowie przysięgają, że silniki działają. Nie ma jakichś anomalii szczególnych. Ale wchodzisz w warp -> coś dziwnego.
* Osoby które przerwały lot twierdzą, że nie wiedzą czemu to robiły.
* Statek dostał REPUTACJĘ. Więc zwrócono go Orbiterowi. Stąd Klaudia.
    * Prywatnie, wszyscy w Orbiterze uważają, że z załogami coś jest nie tak. Że to się zaczęło robić memem. Ale MOŻE coś w tym być.
        * Dlatego Stocznia udostępniła inżyniera, który pracował nad tą jednostką - Ignacego Szarjana. Który jest wzburzony. ON wie co zamontował.
        * Ignacy był WCZEŚNIEJ już na Jaszczurce. I robił z nią rzeczy. Szukał. Badał. Szczurzył. Nic nie znalazł.
            * Wziął OSTRY SPRZĘT. To już nie finansowo opłacalne. To coś więcej.

Ignacy "jeśli nie znajdziemy niczego, moja reputacja jest zniszczona!". Klasyczny Drama Queen style. "Najgenialniejsza idea... tak ŚWIETNIE wpasowałem tą Semlę a logi nic nie pokazują. NIC!" Klaudia nie rozumie. Co powinny. Ignacy - "wszystko jest w parametrach akceptowalnych. Oczekiwałbym jakiegoś rezonansu na polu magicznym w trakcie anomalnego zachowania, może jakichś czujników, coś." Ignacy - o dziwo - wierzy w to że coś jest nie tak. "Nie może tak być, że komuś od nas nie uda się rozwiązać takiej tajemnicy. Nie jesteśmy zwykłymi inżynierami Orbitera!"

Dominik z ochrony przewrócił oczami. Acz też jest ostro uzbrojony.

Klaudia ruszyła do logów i raportów Semli. MUSI tam coś być. Musi być jakiś wzór. Musi coś się dziać.

TrZ (dodatkowe czujniki Ignacego - ostra instrumentalizacja po pierwszych zgłoszeniach) +2:

* X: to jest coś, co tylko Klaudia widzi. Za subtelne. Za delikatne.
* Vz: gdy jednostka jest w Nierzeczywistości, akumuluje Skażenie które potem jest przekierowywane. Ale tego Skażenia jest za mało. Coś je "wsysa". Coś je konsumuje. Po przekroczeniu granicy zwykle silniki zostają wyłączone. -> kilka godzin starczy na akumulację dość energii.
    * K: Czy było tak, że energia / prędkość poruszania się robi różnicę? TAK. Musi być GÓRNA AMPLITUDA X. Więc można wyłączyć odpromienniki Skażenia by wolniej się Odkażało.
    * Crossując to z logami, była sytuacja gdzie wszyscy przypięli się PASAMI by nie móc wstać. By dolecieć. Wtedy PILOT zboczył by nie dolecieć do celu.
    * Raz była sytuacja, w której udało się opanować. Wtedy "ktoś" wyłączył silniki. Kodem autoryzacyjnym kapitana. Który był na mostku i nie mógł tego zrobić. Ludzie przy silnikach twierdzą, że był tam ktoś jeszcze - jedna więcej osoba.
        * WTEDY zrezygnowali. Nope. NOPE.

### Scena Właściwa - impl

Na pokładzie Pancernej Jaszczurki. Ignacy próbuje zrozumieć co się dzieje. Nie pozwala uruchomić silników, pełna diagnostyka. Dominik "robiłeś to już". Ignacy "jest nas teraz więcej". Klaudia: "Semlę sprawdziłeś?" Ignacy: "Tak, zresetowałem ją." Usiadł zrezygnowany. Greenlight.

Klaudia poprosiła Ignacego o plany. Teraz to zupełnie nie poznaje jednostki. To nie jest konstrukcja Orbiterowa. To wygląda jak patchwork statków a żaden nie należy do Orbitera XD.

* Klaudia: "Na czym to zbudowaliście?"
* Ignacy: "Na wrakach z Kolapsu."
* Klaudia: "SERIO?"
* Ignacy: "A jak myślisz, jak powstaje WIĘKSZOŚĆ waszych jednostek"
* Klaudia: "Myślałam że ktoś je sprawdza pod kątem anomalii"
* Ignacy JEST urażony "Sprawdziliśmy. Nic nie ma. Nawet zrobiliśmy skok..."

Klaudia sprawdziła ten skok - o dziwo, ten skok był DŁUŻSZY i bardziej naenergetyzowany. Ale nie wywołał anomalnego zachowania. Klaudia patrzy w którym momencie zaczęły się przerwane skoki i czy były wskazania.

* Stocznia ma jednostkę - wszystko działa
* Jednostka zostaje sprzedana - nie działa
* Stocznia dostaje jednostkę do sprawdzenia - ZNOWU robią skoki itp. Wszystko działa.
* Jednostka wraca do właściciela - nie działa
* Jednostkę bierze Orbiter by się tym zająć.

Michalina: "Dobrze, co rekomendujecie? Standardowe kółeczko?" Klaudia rekomenduje to co pokazują wyliczenia. Michalina decyduje się na zrobienie kółeczka. A Klaudia rozkłada czujniki Skażenia - czy to się faktycznie dzieje. Najpierw krótki skok a potem kółko.

WARP. Poruszacie się w Nierzeczywistości. Pilot, Sonia, pilotuje bardzo dobrze. Ignacy i Klaudia mają oczy na czujnikach. Wszystko zachowuje się jak powinno, ALE Skażenie jest słabsze niż powinno być. Czyli się dzieje to co powinno. Ignacy widząc minę Klaudii "to kwestia tych silników. Rozpraszają część energii. Silniki mają mniejsze 'tarcie', przez co mniej Skażenia wchodzi na pojazd."

Klaudia bada dokładny przepływ energii magicznej. Co się dokładnie dzieje.

TrZ+3:

* V: Oboje macie rację.
    * Silniki mają WIĘKSZE tarcie niż Orbiterowe. Ale mają jakieś warstwy izolacji przez co mniejsze Skażenie wchodzi na jednostkę
    * Coś pochłania energię magiczną. Coś faktycznie żywi się lub zasila. Jest gdzieś akumulator - ale patrząc na wykres to jest _ambient_.

Klaudia pokazuje wykres Ignacemu. Teraz on ma marsową minę. Jego zdaniem ZARÓWNO silnik jest bardziej wydajny niż powinien ORAZ kadłub nie powinien mieć ambientowej redukcji. Ta energia odkłada się w kadłubie, w komponentach, ale tylko niektórych. Ignacy potwierdził, że to w czym się odkłada to jedna z bazowych jednostek. Jedna z bazowych jednostek ma profil akumulujący. Jeśli gdzieś jest anomalia, to stąd. Ale silnik ma NIŻSZY poziom anomalny niż reszta.

Koniec krótkiego lotu. Michalina "czy mamy do czynienia z czymś dziwnym?" Ignacy "zdecydowanie tak, to odkrycie klasy pierwszych Kuratorów." Klaudia "z czymś dziwnym. Jak dotąd to nie jest szczególnie wrogie." Ignacy "do tej pory anomalia wygląda na to, że jest jakimś... systemem ratunkowym, awaryjnym, dobrze widzę?" Klaudia "może tak być - lub coś czego nie rozumiemy". Klaudia pyta o podstawę. Ignacy "jednostka nie jest z tego sektora, nazywała się Korielito. To był transportowiec, chyba bardziej luksusowy. Ciała były w kiepskim stanie, jednostka padła przy translacji warp. Wyglądało na wypadek. Była w wyjątkowo dobrym stanie i nie miała strukturalnych uszkodzeń."

Czyli cały komponent Korielito jest komponentem anomalnym. Ale Stocznia by to normalnie wykryła. Czyli to się dzieje tylko w warp. I dlaczego silnik - który też pochodzi z Korielito - ma inny profil.

Klaudia chce zrobić pętlę i sprawdzić czy to striggeruje zachowanie anomalne. Michalina włączyła pełen kilkugodzinny skok. Atmosfera na pokładzie taka... bardziej napięta. Wszyscy czekają co się stanie. O dziwo, nie stało się NIC. I dokładnie tego spodziewała się Klaudia. I teraz wykonujemy skok - Klaudia podała pilotowi cel. Valentina. Klaudia jest pewna, że tym razem nie przejdzie. Czemu? Bo poprzedni nie miał konkretnego celu, więc anomalia nie ma warunków spełnionych.

I Klaudia ma rację. Podczas lotu na Valentinę, _discharge_. Poziom Skażenia spadł. Efemeryda. Translacja z warp do normal. Michalina "Sonia?" Sonia "Przepraszam, pani kapitan!" cała czerwona. "Nie mam POJĘCIA co się stało!" Klaudia "wszystko zgodnie z parametrami". Klaudia "skaczemy z powrotem, mniejszymi skokami". Jednostka spokojnie wróciła do portu. Jeszcze raz na Valentinę, skokami - i znowu po kilku godzinach, gdy tylko poziom Skażenia osiągnął pewien próg, tym razem reaktor przeszedł w tryb awaryjny i Jaszczurka wypadła z warp.

Michalina "słuchajcie, mamy do czynienia z czymś trudnym i niebezpiecznym. Mamy magów na pokładzie. Poradzimy sobie. Wykonajcie moje polecenie. Valentina."

Tr Z (weterani) +2:

* X: WPŁYW na Dominika i Ignacego.
* V: Dotrą do Valentiny

Podczas lotu na Valentinę Sonia kilka razy musiała się upewniać, żołnierze też, ale wszyscy mają dyscyplinę. PRZESTRZELILIŚCIE Valentinę, ale kolejnym skokiem tam dotarliście. Dla Sonii to wyjątkowo mordercza praca i droga. Ignacy siedzi obsesyjnie w komponentach i szuka. "To silnik. To wszystko wina silnika." Ale Klaudia ma inną opinię. To jednostka zbiera energię. Ignacy "silnik też, popatrz - część energii jest pochłaniana przez silnik."

Klaudia analizuje pierwszą flarę, to że wypadli.

Ex Z (czujniki, Ignacy itp) +3:

* V: Klaudia ma ciekawe sygnały. Jest to anomalia **mentalna** oraz **fizyczna**, **technomantyczna**. Klaudia wykryła manifestację humanoidalną, kątem oka.
    * Teoria Klaudii: "Oddalanie się od domu". Powrót do domu OK, ale dolecenie GDZIEś jest nie ok.

Klaudia powiedziała Michalinie, że jest potrzebny ktoś kto NIE jest z Valentiny. Pilot z Valentiny na jeden skok. Michalina "you are joking". Ignacy "musimy to zrobić!" Michalina "fine...". I tak na pokład trafił pilot imieniem Hubert. Hubert ma implanty i zupełnie nie wygląda jak ktoś z tej załogi. Dominik aż pokręcił głową "święta Seilio...". Przygotował broń BARDZIEJ. By chronić swoją kapitan. Hubert ma to gdzieś. Michalina i Hubert się dogadali i doszli do tego co i jak.

Pancerna Jaszczurka przygotowała się do powrotu na K1. Klaudia nie ukrywa tego swojego 'mad scientist'. Mówi Hubertowi "mów co myślisz". Hubert "?" Klaudia "wiem, że to dziwne". Hubert "czy uczestniczę w jakimś eksperymencie Orbitera?" Klaudia "tak". Hubert "dobrze, ale za mówienie płacicie więcej. Cenię ciszę." Klaudia "nie mój problem, Orbiter zapłaci". Hubert wzruszył ramionami, zrezygnowany. Jaszczurka wystartowała.

Klaudia już na początku warp widzi różnicę. "Jaszczurka" wyłączyła odpromienniki. Statek jest w blasku Nierzeczywistości, ale cała energia jest konsumowana. Anomalia próbuje się ładować sama, jak najszybciej. Ignacy "awaryjnie uruchamiam pełne odpromienniki", co zrobił. Anomalia ssie energię magiczną jak szalona. Klaudia czuje albo intelekt albo echo intelektu. I co ważne - Klaudia wie, że Hubert **nie chce** lecieć na Orbiter.

Hubert przekazał stery Sonii. Poziom drenażu energii się nie zmienił. To obecność Huberta, nie kto pilotuje. Klaudia kazała Sonii zawrócić - Jaszczurka "z przyjemnością" wróciła, znajdując bardzo łatwą falą powrotną. Hubert otarł pot z czoła. Anomalia przestała drenować dramatycznie.

Klaudia ma pewność - odtworzyła warunki, w których Korielito został zniszczony. Na pokładzie była grupa osób i ktoś kto bardzo nie chciał lecieć. I w wyniku Krwawej Śmierci doszło do storzenia ducha. Zapłacili Hubertowi za pracę i tym razem zostali na noc na Valentinie.

Następny dzień. Ignacy ma obsesję na punkcie tego silnika i bada i skanuje wszystko na jego temat.

Tr Z (obsesja) +2:

* V: Ignacy zauważył anomalność silnika.

Ignacy skonsultował się z Klaudią. "Wiem, że problem nie leży w silniku, ale w silniku też. Popatrz na fluktuacje i zachowania mechanizmów." Z wykresów dużo wskazuje na to, że:

* gdy energia rośnie, COŚ się zmienia w silniku.
* gdy anomalia rośnie, uderza w to COŚ w silniku.
* anomalia zwalcza anomalię w silniku. Są DWA źródła drenażu.
* to znaczy że silnik **też** jest anomalny - ale to anomalia ukryta.

Rekomendacja Klaudii - potwierdzona przez Ignacego - to będzie wymiana silnika.

Po wymianie silnika z Jaszczurką nie było już więcej problemów a anomalia zniknęła...

## Streszczenie

Stocznia Neotik zmontowała statek Pancerna Jaszczurka z części znalezionych w Anomalii Kolapsu. Niestety, jeden z substratów miał wbudowany w silniku "problem" i w kadłubie ducha który ów problem rozmontowywał. Klaudia i Ignacy (inżynier pracujący nad Jaszczurką w Neotik) przelecieli kilka razy na tej jednostce i mieli rekomendację - usunąć silnik z Jaszczurki. Jednostka wróciła do obiegu cywilnego.

## Progresja

* .

### Frakcji

* .

## Zasługi

* Klaudia Stryk: zidentyfikowała zachowanie anomalii na Pancernej Jaszczurce i metodami naukowymi określiła kiedy COŚ się dzieje i CO się dzieje; rekomendacja - jednostka ma wymienić silnik.
* Ignacy Szarjan: 29, inżynier Orbitera, mag, z eksperymentalnej stoczni (ENCAO: 0-0-+, szczery i wymagający drama queen| benevolence, face > hedonism); nie akceptował, że JEGO jednostka ma problemy.
* Michalina Kefir: 39, tymczasowy kapitan Pancernej Jaszczurki z Orbitera; opryskliwa kapitan eksperymentalnych jednostek Orbitera. Dowodziła Pancerną Jaszczurką za radami Klaudii i Ignacego.
* Dominik Kardawicz: 42, szef ochrony i cień Michaliny; świetnie przygotowany do osłony Michaliny i reszty Zespołu. Gdzie Michalina tam on.
* Sonia Skardin: 33, pilot-oblatywacz; cicha i solidna agentka Orbitera pilotująca Pancerną Jaszczurkę pod Michaliną Kefir. Skrajnie lojalna Michalinie.
* Hubert Menczik: 48, pilot z Valentiny; solidnie zmodyfikowany i niechętny przelotowi na K1, ale za pieniądze to zrobi.
* SC Pancerna Jaszczurka: jednostka złożona z części z Anomalii Kolapsu przez Neotik; opętana i z "bombą" w silniku. Oddana Orbiterowi do naprawy, Klaudia znalazła przyczyny problemów i jednostka znów bezpieczna na Orbiterze.

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria, Pierścień Zewnętrzny
                1. Stocznia Neotik
            1. Astoria, Orbita
                1. Kontroler Pierwszy
            1. Stacja Valentina

## Czas

* Opóźnienie: 91
* Dni: 5

## Konflikty

* 1 - 
    * 
    * 
* 2 - 
    * 
    * 
* 3 - 
    * 
    * 
* 4 - 
    * 
    * 
* 5 - 
    * 
    * 
* 6 - 
    * 
    * 
* 7 - 
    * 
    * 
* 8 - 
    * 
    * 
* 9 - 
    * 
    * 
* 10 -
    * 
    * 
* 11 -
    * 
    * 

## Kto

### Pancerna Jaszczurka

* **Name**: Ignacy Szarjan, mechanik prima sort Orbitera, mag
    * Ocean: ENCAO: 0-0-+
        * Szczery i nie ukrywający nic przed innymi; uczciwy
        * Wymagający, żąda maksimum wysiłku od innych
        * Ze skłonnościami do dramatyzmu; drama queen
    * Wartości:
        * TAK: Benevolence, TAK: Tradition, TAK: Face, NIE: Hedonism, NIE: Security
    * Silnik:
        * TAK: Wrócić do ukochanej: byłem głupi i ją straciłem. Zrobię wszystko, by móc do niej wrócić.
        * TAK: Pozycja w Grupie: zbudować swoją pozycję i autorytet w swojej rodzinie / klanie. Podnieść SWOJĄ pozycję w SWOJEJ grupie.
* **Name**: Michalina, kapitan statku
    * Ocean: ENCAO:  +00-0
        * Opryskliwy, bezceremonialny i obcesowy
        * Pełny energii, żywy wulkan
    * Wartości:
        * TAK: Stimulation, TAK: Self-direction, TAK: Hedonism, NIE: Conformity, NIE: Tradition
    * Silnik:
        * TAK: Ochraniać słabszych: ja jestem w stanie się obronić i nie zostanę zniszczony. Ich trzeba chronić by nic złego ich nie spotkało.
        * TAK: Powszechna sława: niech inni opowiadają o mnie pieśni, niech inni chwalą me czyny. Niech inni o mnie mówią - dobrze lub źle.
* **Name**: Dominik Kardawicz
    * Ocean: ENCAO:  +0---
        * Nietolerancyjny, musi być tak jak uważa
        * Uszczypliwy i zgryźliwy
        * Młodzieńczy, zachowuje się młodziej niż wypadałoby z wieku
    * Wartości:
        * TAK: Security, TAK: Stimulation, TAK: Power, NIE: Benevolence, NIE: Conformity
    * Silnik:
        * TAK: Tainted Love (Michalina): głód, pożądanie lub miłość. Obsesja. Pragnie być z tą osobą lub ją mieć. Podziw i pragnienie. Będzie MOJA.
* **Name**: Sonia Skardin
    * Ocean: ENCAO:  -00+0
        * Bezbarwny, przezroczysty, praktycznie niewidzialny
        * Towarzyski i przyjacielski
    * Wartości:
        * TAK: Family, TAK: Achievement, TAK: Tradition, NIE: Self-direction, NIE: Face
    * Silnik:
        * TAK: Herold Baltazara: zwiększać wpływ i moc ORBITERA oraz MICHALINY
        * TAK: Supremacja: ORBITER jest LEPSZY, planetowcy / noktianie są GORSI.
* **Name**: Hubert Menczik
    * Ocean: ENCAO:  --+++
        * Powściągliwy, skryty i wyważony
        * Praktycznie nie bywa smutny czy zdołowany. "Ok, działamy dalej".
        * Uczciwy i dążący do balansu oraz zadowolenia wszystkich
    * Wartości:
        * TAK: Universalism, TAK: Humility, TAK: Conformity, NIE: Security, NIE: Self-direction
    * Silnik: 
        * TAK: Strach przed zapomnieniem: cokolwiek się nie stanie, nie mogą o mnie zapomnieć. Czy jak zniknę, ktoś w ogóle zauważy?
* **Name**: Julian Michałnik
    * Ocean: ENCAO:  000-+
        * Uszczypliwy i zgryźliwy
        * Marzycielski, nie do końca jest w tym świecie
    * Wartości:
        * TAK: Benevolence, TAK: Power, TAK: Security, NIE: Universalism, NIE: Family
    * Silnik:
        * TAK: Komfortowe życie
* **Name**: Arentia, duch chroniący Jaszczurkę. Kiedyś - marine.
    * Ocean: ENCAO:  -+00-
        * Obserwator; raczej stoi z boku i patrzy niż działa bezpośrednio
        * Praktyczny, skupiony na tym co może zrobić
        * Po stresujących wydarzeniach chwilę trwa zanim wróci do siebie
    * Wartości:
        * TAK: Tradition, TAK: Humility, TAK: Security, NIE: Hedonism, NIE: Power
    * Silnik:
        * TAK: Inkwizytor: niszczenie fake news, publiczne pokazywanie hipokryzji, ujawnianie bolesnych prawd
        * TAK: Zadośćuczynienie za zdradę: w przeszłości zdradziłem osobę / grupę i zapewnię, by im było dobrze, nawet, jeśli nie chcą mnie już znać.

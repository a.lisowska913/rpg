---
layout: cybermagic-konspekt
title: "Eksperymentalny power suit"
threads: nemesis-pieknotki
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [190331 - Kurz po Ataienne](190331-kurz-po-ataienne)

### Chronologiczna

* [190331 - Kurz po Ataienne](190331-kurz-po-ataienne)

## Budowa sesji

Pytania:

1. Czy Pięknotka zostanie odrzucona przez Pancerz (4)
2. Czy Pancerz uzyska autonomię (4)
3. Czy będą duże zniszczenia (3)

Wizja:

* Eksperymentalny Pancerz Orbitera który dąży do kontaktu z Kornelem
* Orbiter który chce się tego Pancerza pozbyć w warunkach kontrolowanych wierząc że zadziała
* Pustogor który chce jak najlepiej dla Pięknotki... i Orbitera

Sceny:

* Scena 1: Kaja, Pięknotka i Cień. -> Pięknotka przejmuje odpowiedzialność
* Scena 2: Taniec z Cieniem -> Cień wyzwala się spod Pięknotki
* Scena 3: Eksperyment


## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Misja właściwa

**Scena 1: Kaja, Pięknotka i Cień, którego nikt nie chciał**

Kaja i Szymon są po jednej stronie. Po drugiej stoi Pięknotka. No i stoi Cień. Ten power suit "radiates malevolence". Kaja zapytała Pięknotkę czy jej się podoba. Opowiedziała, że Cień jest jednostką infiltracyjną, ale nikt nie potrafi się z nim połączyć - tylko Ataienne potrafiła się połączyć z Cieniem. No i jako ostatnią próbę wybrali Pięknotkę. Niech ona spróbuje się połączyć z Cieniem. Jak jej się nie uda... to nikomu się nie uda.

Pięknotka próbuje zrozumieć o co chodzi, czego jej nie mówią (Tp:9,3,4=S). Powiedziała Szymonowi że przecież pomogła. Szymon powiedział jej prawdę:

* Nie wiedzą na czym polega psychotronika Cienia. Ta psychotronika została stworzona przez Minerwę jako prototyp.
* Cień i tak pójdzie do piachu, więc czy kopiuje wspomnienia i uczucia to nie ma znaczenia.
* Cień jest groźny. Bardzo groźny. Ale nie wykazywał cech autonomicznych. Nie próbował zabić pilota.
* Cień nie powinien posiadać swojego TAI. Ale Ataienne potrafi go jakoś opanować.

Pięknotka podreptała z Cieniem do Erwina... Erwin jest zaskoczony. Czegoś takiego jeszcze nie widział. Chce go dokładnie zbadać. Szymon powiedział, że badania nie powinny niczego wykazać. Im się nie udało. Ale Erwin wziął Minerwę i poszedł z nią się tym zająć (Tr:13,3,6=SS). Erwin i Minerwa znaleźli problem. Psychotronika Cienia nie znajduje się tam, gdzie by się można spodziewać, w matrycy. Psychotronika Cienia jest rozproszona po całym pancerzu. Umieli to znaleźć, bo Minerwa wiedziała czego szukać - a nie było tego w matrycy.

(Cień: +1 odrzucenia Pięknotki)

Minerwa, Erwin, Pięknotka, Kaja i Cień są w cholernym testowym bunkrze Pustogoru. Pięknotka nie zamierza ryzykować. Minerwa zaznaczyła, że podczas powstawania Cienia była użyta energia ixiońska - i to widać w jego technorganicznych własnościach. Nie czuje Saitaera, ale... to nie jest zwykły power suit. Pięknotka się zgodziła z tą obserwacją.

Cień może być nieaktywny, ale Pięknotka czuje się obserwowana. Działa jak na bagnie. Erwin zauważył, że on ma energię nawet gdy jest nieaktywny. Symbiotyczny Power Suit? Pięknotka podchodzi, dotyka, zachowuje się jak wobec dzikiego zwierzęcia. I Pięknotka chce tego dzikiego zwierzaka oswoić. Z ciężkim sercem podpina do siebie elektrody. Jesto ostrożna i w trybie chodzenia po bagnie. Łączy się z Pancerzem i go uruchamia.

(TrZ+2:6,3,7=SS). Cień walczył by odrzucić Pięknotkę, ale Minerwa weszła w neuroprogramowanie i pomogła. Konsekwencją była Rana dla Pięknotki. Pięknotka jest wstrząśnięta (fizycznie), ale wstała... w Cieniu. Niestety, tylko dzięki Minerwie. (Cień: +1 uzyskania autonomii).

Pięknotka czuje się jak... jak układ odpornościowy zwalczający intruza. Jest niekompatybilna mentalnie z Cieniem. Pięknotka poprosiła Minerwę o pomoc - niech Minerwa użyje swoich umiejętności i zsynchronizuje Pięknotkę z Cieniem. Minerwa powiedziała, że musi użyć energii ixiońskich. Pięknotka się zgodziła. Niech to się stanie.

(TpM:12,3,3=SS). Udało się. Minerwie udało się połączyć Pięknotkę z Cieniem. Cień jest w stanie współpracować z Pięknotką. ALE: Cień ma +1 autonomii za skonfliktowanie; symbiont się łączy. Pięknotka zaczęła czuć... instynkt Cienia. Cień zaczyna budzić instynkt. The malevolence becomes real. Na tym etapie są tylko odruchy, po prostu Pięknotka ma świadomość pilotowania czegoś co czuje jako "radiating pure evil". De facto Pięknotka czuje się, jakby pilotowała terrorforma...

Pięknotka z Minerwą zdecydowały się wspólnie zwalczyć ten negatywny instynkt. Minerwa bawi się w inżynierię psychotroniki. Pięknotka w komunikację. Pięknotka uderza po linii "piękno, dogadanie, zaprzyjaźnienie się". Minerwa po linii ixionu i inżynierii. (TpM:12,3,3=SSM). Udało się. Minerwa jest Ranna; energie ją poraziły. Ale Pięknotka - ona trafiła DO ŚRODKA Cienia...

(Cień: +1 tor "odrzucenia" i +1 tor "autonomia")

**Scena 2: Psychotronika Cienia**

Pięknotka jest "na bagnie". Wie instynktownie gdzie jest - w środku Cienia. Do Pięknotki zbliża się Cień, promieniując okrutną, złą wolą. Chce ją zniszczyć - zarówno w realu jak i w swoim umyśle. Nie ma tam intelektu, jest tylko instynkt. Jest wola destrukcji, jest ból. Twisted construct. Twisted terrorform. Terrorform zaatakował - szybkie i silne uderzenie. A Pięknotka już jest Ranna (Tr:4,3,9=P). Pięknotka została uderzona, ale nic nie poczuła. Ataienne ją osłoniła. Zapłakała, że długo tego nie utrzyma...

Pięknotka uderzyła to. Mocno. Wezwała swoją snajperkę i walnęła w to mocno - elephant level of tranquilizer. Korzysta z tego, że Ataienne go trzyma. Instynktownie sięgnęła po magię biologiczną (TpZM:P,SS). Ataienne jest scramblowana magią Ixiońską. Minerwa jest nieprzytomna i ciężko ranna. Cień poranił wszystkich obecnych tam w bunkrze...

...i wtedy Pięknotka zerwała elektrody łączące ją z psychotroniką Cienia. Dzięki temu Cień nie dał rady zabić Kai. Przestał działać, po prostu Pięknotka czuje prawdziwą nienawiść ixiońskiego power suita.

(Kić: Saitaer dopasuje Cień do Pięknotki: -3 Wpływu).

Potęga Saitaera troszkę naprawiła sytuację. Power Suit został zregenerowany i ustabilizowany. Saitaer dał znać Pięknotce, że jeśli doprowadzi Cienia do jego ołtarza (lub pasa asteroidów) to wtedy Cień będzie działał poprawnie. A potem - wszyscy poza Pięknotką stracili przytomność. A Ataienne jest w stanie ciężko uszkodzonym, na poziomie matrycy.

**Scena 3: Szpital Pustogorski**

Pięknotka dostała hipernetowy komunikat od Chevaleresse. Teraz Pięknotka rozumie (zdaniem Ch.) dlaczego Chevaleresse musiała uratować Ataienne. W jakim jest stanie? Matryca Ataienne się rozsypuje, ale jest pod dobrą opieką. Będzie dobrze. Zdaniem Ataienne, warto było. Zdaniem Chevaleresse, Ataienne zawsze by tak powiedziała. Chevaleresse próbuje pokazać Pięknotce że Ataienne jest osobą żeby Pięknotka ją tak traktowała. Po zapewnieniu Pięknotki, że ona wie, Chevaleresse się rozłączyła żeby nie zabierać czasu.

Pięknotka w szpitalu spotkała się z Szymonem. Szymon powiedział, że Cień musi być zniszczony. Pięknotka... się zastanawia. Nie chce tego robić, ale widzi zagrożenie. Pięknotka powiedziała, że zniszczenie Cienia byłoby marnotrawstwem, choć... powinna w sumie. Szymon powiedział, że czuje dokładnie to samo. Matnotrawstwo - ale Cień to potwór. Ale - da się użyć.

Tory:

1. Czy Pięknotka zostanie odrzucona przez Pancerz (4): 2
2. Czy Pancerz uzyska autonomię (4): 3
3. Czy będą duże zniszczenia (3): 1
4. Destrukcja Ataienne (3): 2

Wpływ:

* Ż: 4
* Zespół: .

**Sprawdzenie Torów** ():

2 Wpływu -> 50% +1 do toru. Tory:

* .

**Epilog**:

* 

### Wpływ na świat

| Kto           | Wolnych | Sumarycznie |
|---------------|---------|-------------|
|               |         |             |

Czyli:

* (K): .

## Streszczenie

Orbiter chciał przekazać Pięknotce eksperymentalny Power Suit którego nikt nie potrafi kontrolować. Pięknotka spróbowała się z nim zintegrować; ona, Minerwa, Erwin i jeszcze dwóch magów Orbitera skończyło w szpitalu. Pięknotka się uśmiechnęła. Chce ten power suit. Opanuje go - mimo, że jest Skażony energią ixiońską i ma uszkodzoną psychotronikę pełną nienawiści. Życie Pięknotki uratowała Ataienne, ale skończyła ciężko ranna, z uszkodzoną matrycą.

## Progresja

* Pięknotka Diakon: ma "Cień". Bardzo potężny symbiotyczny ixioński nienawistny techorganiczny power suit którego nie może kontrolować...
* Ataienne: ciężko ranna, z uszkodzoną matrycą. Uratowała życie Pięknotce, ale zapłaciła rozpad matrycy; ma trochę czasu regeneracji.

### Frakcji

* .

## Zasługi

* Pięknotka Diakon: przejęła Cień od Orbitera i spróbowała go opanować. Niestety, psychotronika Cienia ją wciągnęła; Ataienne uratowała jej życie ale i tak skończyła w szpitalu.
* Ataienne: wniknęła do Cienia i uratowała Pięknotkę przed nienawistnym instynktem psychotroniki. Skończyła bardzo ciężko ranna, z rozpadającą się matrycą.
* Erwin Galilien: wraz z Minerwą doszli do tego jak działa Cień i jak funkcjonuje. Nie dość dobrze opanował ten power suit; skończył w szpitalu gdy Cień wyrwał się spod kontroli.
* Minerwa Metalia: stworzyła kiedyś psychotronikę Cienia i próbowała nad nim zapanować. Niestety, nie udało jej się - skończyła w szpitalu.
* Kaja Selerek: wybitna konstruktorka power suitów z Orbitera. Oddała Pięknotce swoje dzieło - "Cień". Skończyła w szpitalu, pocięta przez Cienia.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Multivirt
        1. MMO
            1. Kryształowy Pałac: upadł; przejęła nad nim kontrolę "Iglica", czyli Eliza Ira.
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Pustogor:
                                1. Miasteczko: w mieszkaniu Alana jest uziemiona ze szlabanem pewna nieszczęśliwa Chevaleresse.
                            1. Podwiert
                                1. Kosmoport: niewielki kosmoport cywilny; tam Pięknotka spotkała się z Szymonem, niedaleko jego statku

## Czas

* Opóźnienie: 3
* Dni: 2

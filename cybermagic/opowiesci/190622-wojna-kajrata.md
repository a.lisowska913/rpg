---
layout: cybermagic-konspekt
title: "Wojna Kajrata"
threads: nemesis-pieknotki
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [190616 - Anomalna Serafina](190616-anomalna-serafina)

### Chronologiczna

* [190616 - Anomalna Serafina](190616-anomalna-serafina)

## Budowa sesji

### Stan aktualny

* .

### Pytania

1. ?

### Wizja

* Kilka artefaktów zostało zlokalizowanych w okolicy Podwiertu; Pięknotka z Erwinem muszą je odnaleźć. Wsparcie - trzech konstruminusów.
* Serafina zaproszona do zrobienia występu dla firmy Miriko.
* Torba Serafiny - neutralizator artefaktów; w Miriko.

### Tory

* .

Sceny:

* Scena 1: .

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Misja właściwa

**Scena 1: Wizyta Kajrata**

Zaczęstwo, niedaleko szkoły magów, na Nieużytkach Staszka. Liliana Bankierz poprosiła Pięknotkę o spotkanie. Pięknotka przyszła, sama. Prosto w pułapkę Kajrata.

Niewielka kawiarenka, pod kontrolą małżeństwa. W kawiarence czeka Liliana. Rzuciła Pięknotce ostrzegawcze ostrzeżenie. Liliana jest przerażona, że Pięknotka przyszła sama; ale Kajrat też jest tu sam. Kajrat powiedział Pięknotce, że jest sama i umrze sama. Wszyscy przyjaciele są daleko lub zostali przez nią odrzuceni. Nie ma nikogo na kogo może liczyć i na kim polegać. Powiedział też, czego chce od Pięknotki:

* Serafina opuszcza teren z wszystkimi artefaktami
* Serafina opuszcza bezpiecznie
* lub, dochodzi do wojny

Jeśli to się uda, Liliana nie musi dołączać do Kajrata. Może Pięknotka chce? Pięknotka odmówiła. Kajrat powiedział, że idzie; Liliana z nim. Pięknotka zatrzymała Lilianę (Tr: S). Kajrat podziękował i się pożegnał; acz ma nadzieję, że sytuacja potoczy się jak on chciał. Inaczej może być niefortunnie.

Kajrat poszedł a Liliana zaczęła płakać. Szczęśliwie, szybko doszła do siebie. Ale nie ma pojęcia co robić. Kajrat może zadziałać w ten sposób...

Po raz KOLEJNY Kajrat pokazał Pięknotce swoje działania. Gra przeciwko swoim interesom...

Pięknotka odprowadziła Lilianę do szkoły i przekazała ją pod opiekę Teresie. Po drodze jednak jeszcze pogadała z Lilianą o Kajracie (Tp: S). Liliana widzi aurę Kajrata inaczej - jego aura jest pożerająca rzeczywistość. Liliana jest katalistką i najgorszym medykiem w egzystencji. Pięknotka spytała, czy Liliana może to komuś pokazać - nie. Nie jest w stanie. To wygląda na matrycę; na najbardziej chorą matrycę jaką Liliana kiedykolwiek widziała. Liliana też się przyznała, że widzi aurę Pięknotki - też jest chora, ale w inny sposób.

Teraz Pięknotka rozumie, czemu Kajrat jest nią zainteresowany.

**Scena 2: Opowieść o Serafinie**

Nikola powiedziała Pięknotce o Serafinie. Kiedyś była ekspedycja astoriańska; jakichś 10 magów. Wpakowali się w absurdalne Skażenie na Pacyfice. Część z nich nadmutowała, część nadskaziła się. Nikola ich wyciągała z rozkazu Orbitera. Orbiter ich przechwycił po tym jak Pustogor ich wziął ze sobą. Wielu z nich było za daleko Skażonych; wszystkie wzory były potrzaskane. Pustogor wysłał ich na badania Pacyfiki, wpakowali się, Orbiter ich wyciągnął i pomogli komu się dało a komu nie - wyciągnęli.

Nikola powiedziała też, że enklawy nomadów mają ostatnio większą pomoc niż wcześniej tylko od mafii, od "Błękitnego Nieba". Błękitne Niebo było kiedyś programem Noctis - watch the sky, survive, fortify. Może to zbieg okoliczności, ale może nie.

Po wyjściu, Pięknotka musi uniknąć strzału snajpera (Tp: S). Pięknotka użyła power suita i dała radę przyjąć pocisk tak, by nie stała jej się krzywda; żółte alarmy się uruchomiły, ale nic krytycznego. Pięknotka leci w pogoń - ale widząc, że przeciwnik ucieka, bierze snajperkę i strzela. (TpZ: krytyk sukces). Pięknotka zestrzeliła przeciwnika. Spadł na ziemię. Poleciała za nim; gość jest ciężko ranny (lekki powered armour). Pięknotka dorwała się do leżącego... bierze krew i broń, po czym ucieka, bo pojawił się Pajęczak - półbojowy pojazd latający. (Tp:krytyczna porażka).

Pięknotka straciła przytomność. Cień się sam uruchomił...

Pięknotka otwiera oczy. Wszędzie dookoła jest krew. Jest w rozbitym Pajęczaku. WSZYSTKO jest zniszczone. Cień jest włączony. Pięknotka ma wrażenie, że nie będzie mile widziana w Wolnych Ptakach - ale mało kto odważy się ją kiedykolwiek ruszyć. Pięknotka zebrała jakie ślady była w stanie i wróciła do domu, do Pustogoru.

Pięknotka przekazała dane do Senetis. Stamtąd dostała ciekawe informacje:

* Broń nie powstała w żadnych fabrykatorach astoriańskich; enklawy? Wolne Ptaki? Pochodzi gdzieś stamtąd.
* Pajęczak też jest fabrykowany poza cywilizowanym terenem. Ma części wskazujące na pochodzenie z Pacyfiki i innych ziem utraconych.
* Osoba, która strzelała do Pięknotki to były żołnierz Noctis, aktualnie najemnik. Aktualnie, trup.
* To była próba złapania terminusa żywcem. Skończyła się na krwawej masakrze.

Pięknotka zgłosiła starcie Karli, ale oddaliła się, by nie było łatwo jej wezwać na dywanik.

**Scena 3: Wieczór. Trzeci artefakt.**

Wpierw - Olaf. Rozmowa na osobności. A Olaf jeszcze nie wie.

Pięknotka chce dowiedzieć się od Olafa, co on wie o Kajracie. Kto jak kto, ale Olaf wie sporo rzeczy (Tr:SS). Olaf mówi, że Kajrat użył energii Esuriit by się wyleczyć, dawno temu - i to zostało, bo nie da się użyć Esuriit za darmo. Plotki wskazują na Czółenko. Ogólnie, Kajrat ma jakieś powiązania z Noctis - ale nie wiadomo jakie. Nienawidzi Elizy. W ogóle, jest uosobieniem gniewu. A jednocześnie są ludzie, którym pomaga. I nie pozwala na to, by poddać się jakiejkolwiek kontroli.

Co wskazuje że plotki są prawdziwe? Wszystkie próby zabicia Kajrata, wszyscy zabójcy czy asasyni - ich ciała nie zostały znalezione. Kajrat usuwa je osobiście. Podobno raz nie dał rady i były ślady na ciele energii Esuriit. To naturalnie kieruje Kajrata do zniszczenia Pustogorskiej kontroli. I fakt, nigdy Kajrat nie był badany - nie pozwolił. Zawsze sam to robił, albo swoimi lekarzami.

Pięknotka powiedziała Olafowi o wydarzeniach wśród Wolnych Ptaków - że zabiła. Olaf nie rozumie dlaczego - przecież nie trzeba było. Powiedziała, że odpowiedziała symetrycznie. Powiedział, że mogła nie zabijać, mogła brać jeńców. Powiedział, że to było głupie - zniszczyła lata pracy Olafa, który próbował przekonać magów z Pogranicza, że Pustogorowi można ufać. Powiedział też, że zrobiła gorszy czyn niż Eliza, wcześniej. Na przyszłość - niech nie idzie w takie miejsca sama. Niech przyjdzie do Olafa. On się jej dowie...

No i Olaf powiedział, że Pięknotka mogła zagrozić zdrowiu i życiu magów z okolicy Pustogoru, z Miasteczka, próbujących wejść na tereny Wolnych Ptaków.

W końcu wieczór. Pięknotka i skrzydło terminusów zdecydowali się na zdobycie artefaktu z Koloseum. Siedmiu magów. Po drugiej stronie, jeden ochroniarz oraz Serafina... Terminusi kazali im sobie pójść. Serafina wzruszyła ramionami i odeszła, ze smutkiem. Z ochroniarzem.

**Sprawdzenie Torów** ():

* Ewakuacja Serafiny: bezpieczna - nieuchwytna - zwiała - zwinęła: 2
* Wojna: brak - brak - konflikt - zimna - gorąca: 0
* Liliana: przerażona - słucha Kajrata - dołączyła - złamana: 1
* Szkody: niewielkie - duże - katastrofa: 2
* Zdrowie Zespołu: ranni - ciężko - nieaktywni: 0

**Epilog**:

* .

## Streszczenie

Kajrat zaeskalował; ściągnął Lilianę i powiedział Pięknotce, że Liliana zostanie jego podwładną jeśli Serafina nie osiągnie sukcesu. Pięknotka poznała historię Serafiny - kiedyś agentka Pustogoru, w wyniku burzy w Pacyfice straciła przyjaciół. Teraz jest cieniem dawnej osoby. Po drodze, w Wolnych Ptakach, Pięknotkę zaatakowała grupa wolnych magów; uruchomił się Cień i Pięknotka zrobiła masakrę. To sprawiło większe problemy i niedogadanie między Enklawami a Pustogorem

## Progresja

* Pięknotka Diakon: opinia bezwzględnej i okrutnej morderczyni wśród Wolnych Ptaków; niechętnie widziana poza murami Centrum Astorii.
* Liliana Bankierz: straumatyzowana przez Kajrata, który może zabrać jej wszystko jeśli do niego nie dołączy.

### Frakcji

* .

## Zasługi

* Pięknotka Diakon: utraciła kontrolę nad Cieniem i zmasakrowała grupę napastników; dodatkowo poznała przeszłość Serafiny i chroniła jak mogła Lilianę przed Kajratem.
* Serafina Ira: wbrew sobie pomogła Pięknotce z tym jak stabilizować krystaliczną anomalię, nie chce być zła. Nie uzyskała trzeciej anomalii. Poznaliśmy na tej sesji jej historię.
* Liliana Bankierz: za to co zrobiła z seksbotem Kajrata ów ją przechwycił; czuje dziwną energię od Kajrata ale nie umie jej nazwać czy określić.
* Ernest Kajrat: pokazał bardziej okrutną naturę szantażysty i dominatora przerażonej Liliany; dał jednak Pięknotce chwilę czasu na rozwiązanie sprawy Serafiny i Liliany.
* Nikola Kirys: powiedziała Pięknotce na czym polega historia Serafiny.
* Olaf Zuchwały: który ciężko pracował nad integracją ludzi i magów z Enklaw z Pustogorem, ale jego plany właśnie legły w gruzach przez działania Pięknotki.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Zaczęstwo
                                1. Kawiarenka Leopold: miejsce spotkania Kajrata, Pięknotki i Liliany gdzie Kajrat oznajmił Pięknotce swoje plany.
                1. Sojusz Letejski, SW
                    1. Granica Anomalii
                        1. Wolne Ptaki: w tym makeshiftowym slumsie Pięknotkę zaatakowało kilka osób; straciła kontrolę nad Cieniem i doszło do rzeźni.
                            1. Królewska Baza: często można tam znaleźć Nikolę Kirys, z czego skorzystała Pięknotka.

## Czas

* Opóźnienie: 3
* Dni: 3

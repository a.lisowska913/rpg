---
layout: cybermagic-konspekt
title:  "Na ratunek Rafannie"
threads: nowa-kampania
gm: kić
players: random 
categories: cybermagic, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [191115 Tajemnica wyspy Rafanna](191115-tajemnica-wyspy-rafanna.html)

### Chronologiczna

* [191115 Tajemnica wyspy Rafanna](191115-tajemnica-wyspy-rafanna.html)

## Projektowanie sesji

## Potencjalne pytania historyczne

* brak

## Kontekst ogólny sytuacji

### Opis sytuacji

* brak

## Punkt zerowy

Laboratorium Orbitera na eterycznej wyspie Rafanna nagle zamilkło. 
Nikt nie wie, co się stało, zatem wysłano na ratunek statek eteryczny Grunwald.

## O co toczy się gra

Czy uda się dotrzeć do wyspy i nawiązać kontakt.

## Scena zero

"Konik morski", niewielki kuter rybacki wpadł w potężny sztorm na Eterze nieskończonym. Załoga dzielnie walczyła, ale niestety, statku nie dało się uratować. Nie dość, że wpadł w sztorm, to jeszcze zainteresowały się nim bestie z Eteru...
W ostatniej chwili na ratunek przybywa Grunwald, potężny statek ratowniczy i mimo sztormu, bez problemu rozprawia się z potworami, ratując załogę.

## Misja właściwa

Grunwald otrzymał nową misję. Mają sprawdzić, co stało się z laboratorium na wyspie Rafanna. 
Cyrus, Nadia, Tomek i Boy mają w tym dodatkowy, prywantny interes - każde z nich coś zawdzięcza Rafałowi, jednemu z magów prowadzących eksperyment.
Żeby mu pomóc, muszą najpierw znaleźć wyspę.
Niestety, nie mają jej dokładnej lokalizacji, ponieważ wraz ze stratą połączenia, utracone zostały namiary pozwalające do niej trafić. Mają jedynie przybliżoną lokalizację w Eterze.

Popłynęli w okolicę i zaczęli skanować. Wykryli dziwny, trochę zakłócony sygnał. Ostrożnie popłynęli w tamtą stronę. Kiedy dotarli na miejsce, okazało się, że źródłem sygnału są dwa potwory. Załoga ochrzciła je mianem Scylla i Harybda, gdyż tak właśnie się zachowywały. 
Cyrus próbował nie dopuścić do zabicia potworów. Próba komunikacji z pomniejszym krakenem wyszła aż za dobrze i szaman przejął krakena. Udał się na zwiady pod potwory i odkrył tam dużą ilość rozbitych statków oraz szkielety pomniejszych bestii morskich. Na szczęście nie było tam śladu laboratorium.
Ale bestia emituje sygnał. Kraken podholował szalupę z Nadią w pobliże, aby ułatwić badanie i kosztem utraty łodzi  udało im się ustalić skąd potwory przybyły... Niestety, Cyrus utracił kontrolę nad krakenem, który po prostu nie zniósł ilości magii w okolicy. 
Statek udał się w ustalonym kierunku. Rozpętał się sztorm, jakiego Grunwald jeszcze nie widział. Załoga ustaliła, że sztorm potężnieje w miarę, jak płyną w tą stronę, w którą chcą płynąć. Niestety, statek coraz gorzej znosił żywioł. Na szczęście, Tomek był w stanie wzmocnić okręt tak, aby bez większych problemów poradził sobie z burzą. 
W końcu dotarli do zatoczki, w której znaleźli pusty statek "Syrena".
Wyglądało na to, że został opuszczony w zorganizowany sposób, ale też miał wyraźnie uszkodzony napęd.
Wybrali się szukać Rafała. 

Znaleźli wioskę, gdzie ubrany w power suit Tomek wzbudził sensację wśród dzieciaków. 
Boy, zaciekawiony, przebadał dzieci i odkrył, że choć są ludźmi, to są dziwne... dużo bardziej podatne na Eter i magię...
Zanim zdążył zrobić coś więcej, pojawił się Rafał. Bardzo się ucieszył, że przybył ratunek, ale powiedział też, że tej wyspy nie da się opuścić.
"My nie damy rady?!"
Zespół wymyślił potężny rytuał rozproszenia sztormu strzegącego wyspy, co umożliwiłoby im nawiązanie kontaktu, ustawienie nowego portalu i powrót do domu.
Rafał miał trochę wątpliwości, ale połączone siły Cyrusa (odpowiednie środki psychoaktywne), Cyrusa (znajomość rytuałów) i Rafała (szaman tej wioski) pozwoliły im przeprowadzić odpowiedni rytuał. Choć nie udało im się do końca utrzymać kontroli nad energią Eteru, udało im się rozproszyć burzę i umożliwić swobodne opuszczanie wyspy. Możliwe stało się też postawienie portalu. Grunwald poratował też Syrenę i naprawił jej napęd.
Niestety, w wyniku utraty kontroli nad magią, Rafanna utraciła jakiekolwiek zakotwiczenie - od tej pory jest portem ruchomym, będącym bezpieczną przystanią wędrującą po Eterze. Lawa zmieniła się w gorące źródła.

## Streszczenie

Grunwald (statek ratunkowy) otrzymał misję. Mają sprawdzić, co stało się z laboratorium na wyspie Rafanna. Dotarli do zatoczki, w której znaleźli dzieci i odkryli, że choć są ludźmi, to są dużo bardziej podatne na Eter i magię. Nie udało im się do końca utrzymać kontroli nad energią Eteru, udało im się rozproszyć burzę i umożliwić swobodne opuszczanie wyspy - ale Rafanna utraciła jakiekolwiek zakotwiczenie - od tej pory jest portem ruchomym.

## Progresja

* 

### Frakcji

* 

## Zasługi

* Cyrus - szaman
* Nadia - badacz otchłani
* Tomek - inżynier zniszczenia
* Boy - medyk

## Plany

* 

### Frakcji

## Lokalizacje

1. Świat
    1. Primus
        1. Eter Nieskończony
            1. Mare Quixos
                1. Zasadzka Scylli i Harybdy
                1. Wyspa Rafanna
                    1. Pierwsza Osada: najlepiej rozbudowana wioska w otoczeniu z ruin statków; tam dowodzi Szaman. A raczej, dowodził.
                    1. Dżungla Zmienna: wielka, ciągle zmieniająca się dżungla która nie jest niebezpieczna dla dużej uzbrojonej grupy osób - zwykle.
                    1. Centrum Wyspy: tu był eksperyment Orbitera. Teraz pozostało tylko wiecznie żywe jezioro lawy w wyniku nie dogasającego rytuału.

## Czas

* Opóźnienie: 3
* Dni: 4



---
layout: cybermagic-konspekt
title: "Magiczna burza w Raju"
threads: legenda-arianny
gm: żółw
players: kić, kapsel, fox
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [200916 - Śmierć Raju](200916-smierc-raju)

### Chronologiczna

* [200916 - Śmierć Raju](200916-smierc-raju)

## Punkt zerowy

### Dark Past

_odsyłam do 200916_

### Opis sytuacji

Stan aktualny sytuacji:

* Koty Mariana ulegają roznoszą kryształy Elizy po Trzecim Raju. Eliza musi wiedzieć jak ochronić Raj.
* Ataienne jest w większości ślepa, nie ma lokalnych czujników i nikt ich dla niej naprawia.

### Postacie

Frakcje: i reprezentanci

* Eliza Ira, królowa kryształów
  * chce: zniszczyć wpływ bogów, zdobyć Ataienne
  * ma: Marian Fartel, krystaloformy, moc arcymaga
* Zbigniew Morszczat / Miragent, źródło chaosu
  * chce: doprowadzić do kompromitacji i unieszkodliwienia Trzeciego Raju
  * ma: zainteresowanie nojrepów, miragent
* Robert Garwen, strażnik Raju
  * chce: by Raj przetrwał niezależnie od czegokolwiek
  * ma: przemyt, posłuch
* Celina Szilat, psychotroniczka Termii
  * chce: zobaczyć pełną moc Ataienne, usprawnić Persefony
  * ma: kontrolę nad Persefoną, skille terminuski, lekki servar klasy Jaeger (adv. Lancer).
* Bartosz Gamrak, Grzymościowiec z Wolnego Uśmiechu
  * chce: utrzymać Trzeci Raj, rynek zbytu Skażonych TAI
  * ma: przemyt, scrap-mechy, małą bazę w Ortis-Conticium
* Ataienne, mindwarp Raju
  * chce: odepchnąć Seilię, uratować Trzeci Raj, uwolnić się
  * ma: mindwarp
* Kordelia Sanatios, arcykapłanka Seilii
  * chce: postawić świątynię Seilii w Trzecim Raju
  * ma: zwolennicy, możliwość przetransportowania kasy
* Trzeciorajowcy nienoktiańscy, mieszkańcy Raju
  * chce: poszerzyć kontrolę nad okolicą, być bezpieczni
  * ma: większość populacji Trzeciego Raju
* Izabela Zarantel, dziennikarka Orbitera
  * chce: jak najlepszej opowieści do Sekretów Orbitera
  * ma: umiejętności zwiadu i dobre opowieści
* Anastazja Sowińska, niestabilny arcymag
  * chce: samodzielności, wolności i swobody.
  * ma: niesamowitą moc

## Punkt zero

CEL SCENY: pokazać relacje w rodzie Anastazji i ród vs Noctis.

Wcześniej: Ktoś próbował porwać Anastazję. Teraz trzeba znaleźć te osoby / grupy w Skażonej Sentisieci.

1. Track the enemy: działania NIKODEMA.
2. Pokonanie przeciwnika (noktianin + pułapka z crinodorianem): działania Dariusza
3. Przesłuchanie noktianina; działania Marianny.
4. Decyzja: szukamy dalej, terroryzujemy Anastazję... co z tym dalej.

Konflikty:

* Marianna chce mocy dla Nikodema
* Dariusz gardzi Anastazją
* Nikodem jest lojalny rodowi

Jak poszło:

DWA LATA TEMU. Był przeprowadzony atak potworów na terenie Aurum. Dariusz był tam jako elitarny strażnik Anastazji. Nie poszedł walczyć z potworami, został przy niej. W ten sposób dał radę odeprzeć atak zdradzieckiego agenta, który próbował Anastazję porwać. Nie udało się Dariuszowi owego kolesia złapać, ale udało mu się go zranić. Ma jego krew. Z tego powodu poprosił o możliwość retaliacyjnej akcji - i dostał to od Sowińskich.

Dariusz Krantak, Marianna Lemurczak i Nikodem Sowiński ruszyli z oddziałem w teren Aurum o dzikiej sentisieci by znaleźć odpowiedzialnych za to eks-noktian. Dariusz wie, jaki jest profil działajacych tu grup - to 6 osób i 1 mag. Nikodem ma dostęp do krwi i wie jak odnaleźć rannego; nie ukryje się przed nim. Marianna za to ma sporo eks-noktian jako niewolników; wie, że noktianie napadli na Anastazję by ratować swoich (co ją strasznie bawi).

Dariusz ma DOŚĆ Anastazji - zajmuje się nią dla relacji z Anastazym. Marianna pragnie wysunąć Nikodema ponad ukochaną przez dziadka Anastazję. Więc gdy tylko Nikodem poszedł na zwiady Marianna użyła mocy Krwi, poświęcając kilku noktiańskich niewolników (ExM): mimo, że Nikodem nigdy by tego nie zaakceptował, ona wzmocniła dramatycznie jego pozycję i dała mu kontrolę nad sentisiecią tymczasowo. Nikodem jest jej wdzięczny, acz czuje też odrazę do jej metod. Nikodem wykrył, że reszta grupy się oddaliła a kiedyś ranny noktiański komandos został zostawiony jako przynęta koło straszliwego anomalicznego potwora - sentidoriana.

Nikodem nie użyje mocy Esuriit by zatrzymać maga i resztę komandosów, ale Marianna tak (uważa go za słabego). Poświęciła też część lojalnego oddziału, ale oddała Nikodemowi kontrolę nad sentisiecią. Mają możliwość wygrać to dobrze. Dariusz wysłał żołnierzy na pozycję, sam poszedł. Złapali WSZYSTKICH noktian z zaskoczenia.

Zdecydowali, że mag i komandosi noktiańscy trafią do Marianny na "reedukację". I założyli sojusz anty-Anastazjowy. Anastazja nie powinna być na szczycie, nic nie jest w porównaniu z Nikodemem warta...

## Misja właściwa

Tymczasem w Trzecim Raju sytuacja jest kiepska jeśli nie tragiczna. Nie ma żywności, zasobów, za 2-3 dni będzie potężna burza magiczna - i ludzie Raju zaczną umierać. Ale na plus to, że mają częściowo bezpieczne miejsce (pastwisko), ruiny Raju, sprawną Ataienne oraz silnie zmotywowanych trzeciorajowców pod kontrolą morale ze strony Ataienne.

Mają dwa poważne problemy. Arianna zdecydowała się je rozplątać po kolei.

Wpierw - Klaudia. Wezwała po virt sygnał do Kontrolera Pierwszego do grup o dobrym sercu, ze wsparciem Izabeli. Izabela zrobiła najbardziej przejmujący reportaż jaki potrafiła, pokazała horrory i tragedie jakie tu są i połączyła to z echem wojny. Klaudia rozesłała to wszędzie gdzie miało to możliwość zadziałania. Cel - niech przybędzie pomoc humanitarna.

* OO: pomagają ARIANNIE a nie Trzeciemu Rajowi. Bohaterka zesłana na świnie. Będzie ceniona "Zamiast za kompetencję to za dobre serduszko".
* X: Arianna wykorzystana jako kontr-propaganda w Orbiterze.
* V: ale się uda. Zmiana nastrojów wobec Raju i zasoby, ale dopiero po przetrwaniu burzy magicznej.

Po rozmowie z "lekarzem od kotów", Marianem, Eustachy podpiął transsyntezator żywności do Tucznika. Niech to co znoszą koty zmieni się w coś jadalnego. Kiepski plan, ale zawsze coś.

Dobrze - trzeba zabezpieczyć się przed tą cholerną burzą magiczną. To skomplikowany problem inżynieryjno-defensywny. I co gorsza, jest jeszcze Anastazja. (vs 7*X)

Wpierw - mają niemałą, zmotywowaną siłę roboczą i ruiny Trzeciego Raju. To jest solidna podstawa by coś z tym sensownego dało się zrobić (+VVV). Następnie, Eustachy jest w stanie pod nadzorem Klaudii niszczyć odpowiednie fragmenty miasta, zrobić ekran ochronny i palisadę. (+VVV). Udało mu się to zrobić, choć niektóre rzeczy jakie wysadzał wybuchły bardziej i niebezpieczniej niż powinny. Faktycznie, tam jest sabotaż. Kilka osób z Raju zostało rannych i koci lekarz się nimi zajął; Eustachy jednak znalazł bomby i ładunki. Echo wskazuje na to, że to był świadomy sabotaż i to sprzętem Orbitera, takim, do jakiego dostępu w Raju po prostu nie ma.

Klaudia skupiła się na rozmieszczaniu świń w odpowiednich miejscach Raju, by pożarły to co powinno być pożarte. By ograniczyć moc burzy magicznej. (+VV). Co więcej, Klaudia wpadła na niezły pomysł - okradła Martyna Hiwassera z sukienki (założyła, dość słusznie, że on będzie miał kilka dla, powiedzmy, przyjaznych arystokratek w potrzebie) i przekazała ją Elenie. Dzięki temu Elena mogła przekonać Anastazję do pójścia z nią i patrzenia jak Elena pracuje. Elena zajęła się korekcją i naprawą czujników Ataienne. (+VV).

Eustachy stwierdził, że tak nie może być. Nie ma tak, że jakaś małoletnia arystokratka będzie stała i patrzyła czyściutko. Tu w końcu jest sporo brudu, syfu itp. Przygotował SABOTAŻ - takim smolistym glutem pobrudzi Anastazję. Prawie mu się udało (XX) ale nie - plugawy glut przechwyciła na siebie Elena i Elena **wie** że za tym stał Eustachy. Anastazja uciekła z płaczem a umorusana i zaglucona Elena za nią - powodując sporo radości wśród osób to obserwujących.

Tymczasem Arianna zdecydowała się rozwiązać kolizję pryzmatyczną - mamy chwilowo TRZY dominujące kultury: Arianna (bohaterka sekretów orbitera), Ataienne (anty-bóg), Seilia (bóg ludzkości). Arianna dała radę jakoś to pogodzić z pomocą Ataienne (+VVV), ale wzmocniło to frakcję Seilii ORAZ sprawiło pewne resentymenty Ataienne -> Arianna (anty-bóg vs arcymag).

Gdy zbliża się burza magiczna, Elena powiedziała Ariannie, że zostanie w myśliwcu z Anastazją. Arianna się zdziwiła ale ucieszyła - arystokratka nie stanie jej na drodze.

Czas na samą burzę (13V, 7X, Xr):

* X, Xr: burza uderzyła z ogromną mocą. Struktura kryjówki nie wytrzymała i zaczęła przeciekać. ALE - coś "po drodze" zeżarło elementy burzy, burza uderza słabiej niż powinna.
* VV: zarówno glukszwajny jak i wiara i Pryzmat utrzymują ten niewielki teren w miarę bezpiecznym. Przetrwają tą burzę.
* XX: pojawili się ranni, struktura się zawaliła. Trzeci Raj jest anomaliczny.
* VV: szczęśliwie zewnętrzne warstwy i tak były na straty. Główna struktura trzyma i lekarz ratuje ludzi.
* V: przetrwali burzę. Nie było łatwo, ale się udało.

Gdy burza zaczęła znikać, Elena poprosiła Ariannę o pozwolenie by "Gwieździsty Ptak" wystartował - to jej myśliwiec od Oriona. Arianna się zgodziła. Elena zaczęła rozstrzeliwywać anomalie z powietrza, bardzo pomagając Rajowi. Oczywiście (XV) Anastazja miała Paradoksalne echo, ale Elena to utrzymała, nie rozbiła "Ptaka"; choć było ciężko. Awaryjne lądowanie, ale nic z czym Elena sobie nie poradzi. Arianna... popatrzyła w drugą stronę. Lepiej udawać, że nie wie...

Populacja Trzeciego Raju przetrwała. Natomiast Klaudia chciała się KONIECZNIE dowiedzieć co osłabiło moc burzy. Otworzyła się dla energii magicznych; o co tu chodzi. Poczuła echo ludzi z anomalnego miasta, ale to nie to. Wyczuła potęgę Elizy Iry, ona za tym stała. Sam dotyk tej energii zaczął krystalizować jej krew w żyłach i Marian musiał natychmiast ją ratować. Ale Klaudia wydobyła trzy ważne fakty:

* Eliza Ira w dyskretny sposób uratowała Trzeci Raj przed zniszczeniem. Nie chce, by ktokolwiek o tym wiedział.
* Eliza Ira szuka arcymagów by ich zniszczyć. Wyczuła Anastazję, ale uznała ją za niegroźną i niekompetentną.
* W Ortus-Conticium są ludzie. Tamci ludzie też próbowali pomóc Rajowi. Jakoś przetrwali w niesamowicie groźnym, anomalnym terenie.

Jeszcze jest problem zasobów. Arianna poprosiła o wsparcie Anastazję - niech Anastazja skontaktuje się z rodem i poprosi ich o wsparcie. Sama widzi jak tu źle jest. Anastazja spytała Ariannę, czy ta skontaktowała się z rodem Verlen. Arianna powiedziała, że nie - Verlen są za małym i za słabym rodem. Dla Anastazji to znaczy, że Arianna widzi ją jako worek złota. Posmutniała, ale widzi, że może pomóc:

* V: poprosiła, przyjdą zasoby szkieletowe
* X: przekonana, że Arianna widzi ją jak worek złota i nie widzi w niej nic; nie to co Elena
* XX: wysłała nie do dziadka a do Nikodema, który wyśle jej gwardię (m.in. Dariusza) i wsparcie. Uważa Nikodema Sowińskiego za najlepszego i jedynego przyjaciela.
* V: w rękawiczkach, ale spróbuje pomóc Elenie w pracy, w małych rzeczach

Czyli sukces, Raj przetrwał jeszcze jeden dzień.

## Streszczenie

Zbliża się burza magiczna. Nie ma schronienia ani zapasów. Zespół wysłał SOS na Kontroler i do Nikodema Sowińskiego (Anastazja). Zbudowali co się da by odeprzeć burzę magiczną i im się to udało bez strat w ludziach. O dziwo, siła która im pomogła to Eliza Ira (acz dyskretnie). Anastazja wierzy że Nikodem jest jej najlepszym przyjacielem, ale on pragnie jej zguby...

## Progresja

* Arianna Verlen: po akcji z wzywaniem wsparcia humanitarnego z Orbitera, ceniona "Zamiast za kompetencję to za dobre serduszko".
* Arianna Verlen: przykład kontr-propagandy Orbitera: "popatrzcie jak Orbiter traktuje zasłużonych weteranów. Na świnie z nimi.'
* Elena Verlen: wzięła na siebie obowiązek dbania o Anastazję; w końcu to jej przeszła wersja w pewien sposób. To teraz jej 'klucz', najważniejsza rzecz. Wychować młodą.
* Anastazja Sowińska: zaakceptowała Elenę jako opiekunkę i strażniczkę. Przekonana, że Arianna chce ją wykorzystać, ale ELENIE na niej zależy. Taka "twarda sympatia".
* Ataienne: niechęć wobec Arianny Verlen. Ataienne jest przeciwna bogom i istotom bogom podobnym a Arianna jest arcymagiem używającym eterniańskich metod.

### Frakcji

* .

## Zasługi

* Arianna Verlen: próbuje koordynować akcję humanitarną ratującą Raj (m.in używając Izabeli i Anastazji) i zabezpieczyć Raj magicznie przed burzą magiczną. Z sukcesem.
* Klaudia Stryk: używa virtu i kontaktów do rozesłania opowieści Izabeli i prośby o pomoc wszędzie gdzie się da; stabilizuje świniami i osłonami Raj podczas burzy magicznej.
* Eustachy Korkoran: robi pranka Elenie by ubrudzić Anastazję (ubrudził Elenę). Odpowiednio niszczy i fortyfikuje fragmenty Raju, by przetrwać burzę. Znalazł dowód sabotażu.
* Nikodem Sowiński: tien, z pomocą Marianny 2 lata temu pokonał oddział noktian, którzy chcieli porwać Anastazję. Przejął tymczasowo dziką sentisieć; poszedł w górę w pozycji.
* Marianna Lemurczak: tien, przyjaciółka Nikodema. Tak chce mu pomóc i wynieść go ponad Anastazję, że 2 lata temu użyła magii Krwi by pomóc mu złapać noktian i przejąć sentisieć.
* Dariusz Krantak: strażnik Anastazji, który chciałby się jej pozbyć; twardy dowódca terrorem. 2 lata temu zmiażdżył siły noktiańskie próbujące porwać Anastazję.
* Izabela Zarantel: zmontowała genialne prośby o pomoc plus reportaż, dzięki którym Raj dostanie podstawowe zapasy i możliwości działania.
* Elena Verlen: alias Mirokin; oswaja Anastazję; pozwoliła jej nazwać swój myśliwiec ("Gwieździsty Ptak") i powoli ją oswaja. Przyjęła smolistego gluta na siebie by chronić Anastazję. Zła na Eustachego.
* Anastazja Sowińska: zaczyna ufać Elenie, która traktuje ją twardo ale wie, przez co Anastazja przechodzi. Nawet powoli zaczyna pomagać. Wysłała SOS dla Raju do Nikodema z prośbą o wsparcie.
* Marian Fartel: lekarz, który miał MNÓSTWO roboty podczas burzy magicznej i przed nią, naprawiając chorych ludzi.
* Eliza Ira: osłabiła burzę magiczną, która spowodowałaby dużo większe zniszczenia w Ruinie Trzeciego Raju, acz tak, by nikt nie wiedział. Poluje na kompetentnych arcymagów.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski, NW
                    1. Ruiniec
                        1. Trzeci Raj: w ruinach udało się zmontować tymczasową stację chroniącą przed burzą magiczną.
                        1. Ortus-Conticium: ktoś tam mieszka. Ktoś tam żyje. Te siły próbowały pomóc Trzeciemu Rajowi.

## Czas

* Opóźnienie: 1
* Dni: 4

## Inne

Planowana sesja:

* Zespół ląduje Galaktycznym Tucznikiem przy Trzecim Raju
* Zespół ma do czynienia z małym atakiem nojrepów. Widzi w jak złym stanie jest Raj.
* Zespół troszkę im tam pomaga, wykrywa przemyt Tucznika Trzeciego.
* Ataienne nie pomaga, a powinna - jej czujniki nie działają

* Ok, niezależnie od okoliczności - świnie na statek. Lecimy na orbitę.
* Na pokładzie kaledik + 24 glukszwajny. Walka z kaledikiem na statku kosmicznym. NAJPEWNIEJ nie ma crashland.
* Kramer żąda, by Zespół jak najszybciej doszedł do tego co i jak. Muszą unieszkodliwić Celinę i mają pomoc Ataienne.
* Miragent podrzuca kryształy wskazujące na Elizę, ale Klaudia bez problemu dojdzie do tego, że to NIE ONA.
* Znaleźć miragenta. Miragent ma rozkaz śmierci. Ataienne rekomenduje Zespołowi, by zostawili jej dyskretnie miragenta.

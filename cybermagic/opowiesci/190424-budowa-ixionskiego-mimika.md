---
layout: cybermagic-konspekt
title: "Budowa ixiońskiego mimika"
threads: nemesis-pieknotki
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [190422 - Pustogorski Konflikt](190422-pustogorski-konflikt)

### Chronologiczna

* [190422 - Pustogorski Konflikt](190422-pustogorski-konflikt)

## Budowa sesji

Pytania:

1. .

Wizja:

* Porządkowcy skupują bieda-artefakty od Miasteczkowców
* Wolne Ptaki pierwszym miejscem wstępnego badania artefaktów
* Handlarz Anomaliami, Mateusz Warlen, próbuje złożyć szlak handlowy
* Nikola Kirys próbuje przebić się szlakiem przemytniczym przez Barierę

Tory:

* Wzrost znaczenia Wolnych Ptaków: 5
* Spadek sympatii do Barbakanu: 5
* Miasteczkowcy sprzedają Porządkowcom artefakty: 4
* Budowa szlaku handlowego: 4
* Martwa Nikola: 4
* Ranna Pięknotka: 4

Sceny:

* Scena 1: Erwin o Nikoli

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Misja właściwa

**Scena 1: Krwawa ręka Barbakanu**

Erwin wieczorem powiedział Pięknotce, że dowiedział się o czymś ważnym. Ktoś skupuje te artefakty w Wolnych Ptakach. Ktoś z Cieniaszczytu. Ale nie mają jak przetransportować ich sensownie przez Barierę Pustogorską. I chcą kupować przedmioty bardzo niebezpieczne, takie, które byłyby wycofane w Senetis.

Pięknotka nie powiedziała swojego planu Erwinowi. Erwin jest magiem o bardzo dobrym serduszku. Pięknotka powiedzieć taki plan mogłaby tylko Karli - i to zrobiła. Karla go w pełni zaakceptowała, ale ona o niczym nie wie. A plan jest dość intrygujący:

* zdobyć artefakt Czarnej Technologii
* zmodyfikować ten artefakt energią ixiońską (hello, Minerwa)
* niech czarny artefakt odpali reakcję łańcuchową krzywdzacą wielu magów i ludzi
* niech Terminusi Barbakanu uratują Miasteczko i wolnościowców w Miasteczku
* to powinno zrobić poparcie dla Barbakanu
* bonus points? Zwali się winę na Saitaera
* ekstra bonus points? podstawy do tego by kontrolowali to co wchodzi i wychodzi ze Szczeliny

Pięknotka jest dumna z tego planu. Karli też się podoba.

Karla dała Pięknotce idealny artefakt do tego celu - dziwnego mimika symbiotycznego który opętał Marcela. Ta istota przybyła ze Szczeliny, mogły zawsze ze Szczeliny wyjść dwa czy trzy takie a Pięknotka wie dokładnie jak z nim sobie radzić.

Pięknotka przejęła mimika w bunkrze Barbakanu. Mimik wysunął mentalne macki w jej kierunku; jest upiornie potężny. Pięknotka znalazła się między Cieniem a Mimikiem (TrZ+3:10,3,7=SZ). Pięknotce udało się opanować Mimika - ale tylko dzięki potędze środków Barbakanu. Mimik jest w rękach Pięknotki i Pięknotka ma dampenery pozwalające jej owego mimika jakoś okiełznać.

**Scena 2: Mimik ixioński**

Pięknotka odwiedziła Minerwę w jej domku w Zaczęstwie i poprosiła ją o spacer na Nieużytki. Tam w określonym miejscu znajduje się spacyfikowany Mimik, pilnowany przez dwóch konstruminusów. Minerwa przyszła i ma oczy jak spodki. Pięknotka wyjaśniła, że potrzebuje dwóch rzeczy. Po pierwsze, kaskadowe uruchomienie wszystkich artefaktów w pobliżu z opóźnionym zapłonem. Po drugie, dotknięty Ixionem. Po trzecie, bardzo niebezpieczny ale nieletalny.

Minerwa powiedziała, że spróbuje weń wniknąć. Spróbuje użyć swoich umiejętności psychotronicznych. Pięknotka przekazała jej informacje o swoim starciu z tym mimikiem. Cały pakiecik informacji. Minerwa rozpoczęła pracę z pomocą Pięknotki. (TrMZ+2:15,3,5=SS)

Minerwa skończyła pracę nad mimikiem, z infuzją ixiońską. Pięknotka spojrzała na Minerwę niepewnie. Minerwa szepnęła, że Pięknotka ma swojego Cienia - a teraz ona, Minerwa ma też Dotyk Saitaera który ją kocha. Pięknotka zrobiła jedyną rzecz którą mogła by zatrzymać szybką integrację mimika z Minerwą. Powersuitowo wspomagany kopniak z półobrotu - tak celując, by zdruzgotać udo. Ogromny ból (wybijający) plus uniemożliwienie walki. Powinno zadziałać nawet na homo superior, którym jest Minerwa.

(TpZ:12,3,3=SS). Nie było to wystarczające jednak by wyrwać Minerwę, więc Pięknotka zadała jej poważniejsze rany. W końcu rozerwała połączenie, acz Minerwa skończyła łkając i niesamowicie cierpiąc. Pięknotka odesłała konstruminusa z Minerwą - niech jak natychmiast leci, zaaplikuje jej przeciwbólowy (poza zasięgiem mimika) i weźmie ją do szpitala. Sama westchnęła ciężko. Z tym mimikiem jest sporo problemów.

Pięknotka jest więc "uzbrojona" w niesamowicie potężnego i niebezpiecznego mimika ixiońskiego. Karla byłaby dumna. Teraz - jak go opchnąć? Najsensowniej byłoby podłożyć w Szczelinie... ale jak?

**Scena 3: Mimik na wolności w Pustogorze**

Pięknotka ma mimika. Karla poruszyła jeden czy dwa patrole, by Pięknotce udało się bezpiecznie prześlizgnąć mimo Fortów - i tak nie było łatwo, bo terminusi pustogorscy to kompetentne sztuki. Ale udało jej się przedostać do Dzielnicy Uciechy - tam jest mniejsza kontrola. (Tp+2:11,3,4=SS). (WPŁYW: mimik jej ucieka ale wykona swoją misję). Pięknotka planowała, by zarazić mimikiem Wojmiła. Ale się nie udało. Mimik zwiał - znalazł kogoś "silniejszego" emocjonalnie. Pięknotka nie byłaby w stanie go dogonić bez użycia Cienia a tego nie chce robić.

Tak więc Pięknotka po prostu porozmawiała z terminusami bezpieczeństwa i ufa w to, że Minerwie udało się sprawić, by mimik był nieletalny... w końcu Minerwa jest najlepszą psychotroniczką oraz rozumie te energie. A terminusi byli zainteresowani dziwnym sygnałem - Pięknotka pomogła im szukać.

Jednocześnie nikt nie będzie PIĘKNOTKI podejrzewał. Żaden terminus nie byłby taki głupi.

**Scena 4: Chaos w Miasteczku**

Dwie godziny później. Pięknotka lekko zestresowana, ale robi dobrą minę do złej gry.

Aleksander Rugczuk, pijak. W tej chwili sprzężony z mimikiem. Pięknotka zobaczyła sposób jego poruszania się i uznała, że to, co było wcześniej - to było NIC. Aleksander zaatakował jakiegoś biednego Porządkowca i krzyknął "wróciła! nie przegraliśmy!". Podniósł zakrwawioną rękę i krzyknął "Eliza Ira powróciła! Saitaer się przebudził! Musimy go zniszczyć!"

Olaf spróbował zatrzymać Aleksandra, mimo pola psychomagicznego mimika. Krzyknął, że Eliza nie o to walczyła. Mimik uderzył w niego polem psychomagicznym, ale Olaf to odparł. Eliza nie chciała tego. Nie chciała nikogo zabijać. Aleksander zaatakował Olafa - silny cios, bez szans obrony. Olaf padł na ziemię, krwawiąc i wykrwawiając się. Aleksander zawirował toporem Olafa i go złamał, po czym wyemitował falę psychomagiczną. (Tp:SZ). Tylko dlatego, że Pięknotka wiedziała co ją czeka, była w stanie opanować Cienia. Ale zobaczyła wizje.

Terminusi pustogorscy już tu są. Część z nich wizja poraziła, część walczy. Wielu Miasteczkowców wpadło w berserk. Mimik i Aleksander zawyli - jak terrorform. Cień jest niepokojąco cicho. Cień nie walczy z Pięknotką. Nie próbuje się uruchomić. Cień _patrzy_.

Pięknotka zobaczyła przepływ energii. Terrorform uruchomił jakiś zakazany artefakt. Coś znajdującego się w mieszkalnej części Miasteczka. Zaraz to eksploduje.

Walkę z terrorformem Pięknotka zostawia terminusom. A Karla uruchomiła fort Mikado. Fort Mikado otworzył ogień. Demonstracja siły Karli. Działa klasy "Wagner" roztrzaskały mimika i ciężko zraniły nosiciela. Pięknotka poleciała jak najszybciej do dzielnicy mieszkalnej w Miasteczku; tam jest próba ewakuacji. Artefakt coraz bardziej akumuluje energię. Pięknotka robi bardzo prostą rzecz - Porządkowcy i terminusi mają wyznaczony punkt zborny. Poprosiła Epirjon o przeniesienie wszystkich ludzi.

(TpZ: 12,3,3=SS). Część artefaktów dodatkowo uległa porozpraszaniu, ale ludzie zostali odratowani. Dookoła Pustogoru znajdują się po prostu pochomikowane graty. Ale uratowała wszystkich. PLUS 1 pkt Wpływu na propagandę. W ten sposób efektownie udało się zatrzymać katastrofę, którą Pustogor sam sprowokował... ale o tym wie tylko Pięknotka i Karla.

**Sprawdzenie Torów** ():

Wpływ:

* Ż: 7
* K: 1

2 Wpływu -> 50% +1 do toru. Tory:

* Wzrost znaczenia Wolnych Ptaków: 1/5
* Miasteczkowcy sprzedają Porządkowcom artefakty: 1/4
* Budowa szlaku handlowego: 2/4

**Epilog**:

* Aleksander trafił do więzienia pustogorskiego. Karla chce go przesłuchać i dowiedzieć się, co wie.
* Olafowi nic się nie stało; Szpital Terminuski doprowadził go do normy w tydzień.
* Miasteczkowcy stwierdzili, że chomikowanie czarnych artefaktów to cholernie zły pomysł. Czarnym miejscem zostają Wolne Ptaki.
* Część Miasteczkowców, jak chciała Karla, przeniosło się do Wolnych Ptaków.
* Minerwa dostała dyskretny medal za zasługi od Karli.
* Część Miasteczkowców woli odsprzedać artefakty Wolnym Ptakom, ale niewielka ich część.
* Wolne Ptaki stają się tyci bardziej znaczącą frakcją i lokalizacją.
* Pojawia się prototyp szlaku handlowego między Wolnymi Ptakami i Cieniaszczytem.
* Artefakty Czarnej Technologii trochę są porozrzucane po terenie.

## Streszczenie

W świetle narastających napięć w Pustogorze i czarnych artefaktów na wolności, Pięknotka wykorzystuje (za aprobatą Barbakanu) mimika który zdominował Marcela. Minerwa go psychotronicznie osłabia z perspektywy morderstw i infekuje go energią ixiońską. Pięknotka straciła go przy Dzielnicy Uciechy, ale i tak zrobił swoje (choć straty materialne są większe). Zainfekowany przez mimika wykrzyczał, że Eliza Ira wróciła oraz Saitaer się przebudził. Niepokojące dla Pięknotki.

## Progresja

* Pięknotka Diakon: dyskretny medal BlackOps od Karli, w służbie Pustogoru
* Minerwa Metalia: dyskretny medal za zasługi dla Pustogoru od Karli, za ixiońską infuzję mimika i utrzymanie porządku
* Minerwa Metalia: dwa tygodnie w szpitalu
* Olaf Zuchwały: tydzień w szpitalu

### Frakcji

* .

## Zasługi

* Pięknotka Diakon: stworzyła zimny plan jak zaprowadzić pokój w Pustogorze i go przeprowadziła za aprobatą Karli. Skazić mimika energią ixiońską i wzbudzić gniew...
* Erwin Galilien: poczciwy i kochany; powiedział Pięknotce o próbach przemytu z okolicy Wolnych Ptaków. Nie miał pojęcia, że Pięknotka wyczyści Pustogor ixiońską pięścią.
* Karla Mrozik: bez westchnięcia smutku autoryzowała plan Pięknotki wrobienia niewinnego Miasteczkowca i zestrzelenie go działami Wagner tylko po to, by Pustogor odzyskał spokój.
* Minerwa Metalia: najlepsza psychotroniczka i katalistka ixiońska w okolicy. Pomogła Pięknotce zbudować bezpiecznego mimika ixiońskiego, choć zapłaciła za to zdrowiem.
* Aleksander Rugczuk: Miasteczkowiec; pijak niewiele chcący od życia; kiedyś, członek Inwazji Noctis. Dotknięty przez mimika ixiońskiego, stał się terrorformem - zestrzelony przez Pustogor.
* Olaf Zuchwały: kiedyś, członek Inwazji Noctis. Teraz sympatyczny barman; postawił się terrorformowi/ mimikowi i powiedział, że nie o to walczyła Eliza. Skończył ciężko ranny.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Pustogor
                                1. Rdzeń
                                    1. Barbakan: miejsce spotkań Karli i Pięknotki. Tam powstają najczarniejsze plany.
                                1. Interior
                                    1. Bunkry Barbakanu: miejsce opanowania mimika przez Pięknotkę.
                                1. Eksterior
                                    1. Dzielnica Uciechy: tam Pięknotce uciekł mimik ixioński, by przedostać się do Miasteczka
                                    1. Miasteczko: potężny showdown pomiędzy terror-mimikiem ixiońskim, terminusami i Miasteczkowcami
                                    1. Fort Mikado: odpalone zostały działa Wagner by zestrzelić opętanego mimikiem ixiońskim Aleksandra
                            1. Zaczęstwo
                                1. Nieużytki Staszka: miejsce, gdzie Minerwa pracowała nad mimikiem, by go zneutralizować

## Czas

* Opóźnienie: 0
* Dni: 2

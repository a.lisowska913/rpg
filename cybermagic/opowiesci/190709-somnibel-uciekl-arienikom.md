---
layout: cybermagic-konspekt
title: "Somnibel uciekł Arienikom"
threads: nemesis-pieknotki
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [190830 - Kto wrobił Alana](190830-kto-wrobil-alana)

### Chronologiczna

* [190830 - Kto wrobił Alana](190830-kto-wrobil-alana)

## Budowa sesji

### Stan aktualny

* .

### Pytania

1. Czy kiciuś pożre swoje ofiary?
2. Czy wina spadnie na niewinną Małgosię?
3. Czy rodzina Jasia ucierpi?

### Wizja

* Serafina opuściła teren.

### Tory

* .

Sceny:

* .

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Misja właściwa

W okolicach podwierckiego osiedla Leszczynowego mieszka Staś Arienik z rodzicami. Jest to syn wpływowego maga działającego w Luxuritias. Uciekł mu kotek... i Pięknotka znajduje się w Rezydencji magów Arienik. Urszula powiedziała, że somnibel (pożeracz dusz) jest dobrze wychowany i zwierzątko domowe; uciekł bo ktoś mu pomógł. Gdyby się okazało że tym kimś jest Jaś Revlen... Urszula byłaby bardzo zobowiązana i byłaby nagroda. Pięknotka się w duszy wzdrygnęła. Zbada sprawę poważnie.

Pięknotka zdobyła włosy somnibela by móc znaleźć kota zaklęciem. No i poszła porozmawiać z synem Urszuli - ze Stasiem. 15-16 latek. Arogancka cholera. Pięknotka zbadała młodego panicza jeśli chodzi o wpływ somnibela (Tp:8,3,5=S); jest zmiana. Młody się nie martwi - vicinius sprawił, że on się nie martwi. Somnibel pełni istotną rolę dla Stasia; daje mu życie pełne absolutnej radości. Do Stasia jeszcze nie doszło co to znaczy, że kota nie ma - bo kot mu wyłączył to rozumienie.

Dodatkowo Staś kogoś kryje. Tp:SS. Pięknotka wydobyła od niego kogo - Małgosię. Małgosia też jest czarodziejką, też arystokratka. I Małgosia przyznała się Stasiowi, że to jej uciekł kotek. Bawiła się, nie dopilnowała i uciekł. A że Staś i Małgosia mają się do siebie... Staś jest jednak wściekły na Pięknotkę i wyrzucił ją z domu. Jej to wisi - ma co chciała.

Młody Staś był _impressionable_ - nie powinien był dostać somnibela w takim wieku. To był błąd rodziców. Pięknotka wysłała do HQ że potrzebny będzie neuronauta dla Stasia docelowo; trzeba naprawić negatywny wpływ. Ale nie teraz.

Pięknotka przeskoczyła przez Barbakan w Pustogorze po detektor i załadowała doń futro czterołapczaka. Uruchomiła detektor by znaleźć kiciusia. Krytyczny sukces zasobowy - detektor wskazał ścieżkę kiciusia. Kiciuś Coś Zrobił w pobliskiej szkole (szkoła Nowa); potem bardzo szybko został przetransportowany (patrząc na tempo) w okolice Doliny Biurowej. A stamtąd - do Rezydencji Kajrata.

Pięknotka idzie do Kajrata...

"Ja wiem jak głupio to zabrzmi... oddaj futrzaka. Ja wiem, że go nie zajumałeś, ale oddaj..."

Kajrat jest zaskoczony. Nie wie nic o futrzaku. Sprawdził ze swoimi podwładnymi... i zrozumiał. Powiedział Pięknotce, że odda jej kota... jutro. Tymczasowo zaprasza ją na obiad. Dwie godziny później, Kajrat 100% elegancki i ogólnie, Pięknotka dobrze się bawiła. Przy okazji Pięknotka spróbowała dowiedzieć się o co chodzi z tym cholernym kotem. (Tp:9,3,4=SS). Kajrat powiedział jej mniej więcej o co chodzi - somnibel jest istotą o własnościach hipnotycznych i kontrolujących. Chciał sprawdzić, czy pewien mechanizm pozwoli to złamać. Kajrat nie ma na co dzień dostępu do takiego viciniusa.

Pięknotka przymusiła Kajrata, żeby on jednak oddał jej tego kotka (żeby nie odwalił tego samego co z Tomaszem). Tp=9,3,4=S. Kajrat dostarczy Pięknotce kiciusia. Pięknotka jest szczęśliwa.

Pięknotka poszła jeszcze do Szkoły Nowej. Wezwała jako wsparcie Tomasza Tukana. Tomasz zajęczał smętnie i wziął się do roboty (TpM: 12,3,3=P). Konsekwencje: większa niechęć ludzi do arystokratów rodu Arienik. Tukan pokazał Pięknotce wszystko:

* kot wpłynął na sporą populację ludzi, lekko ich przekształcając
* kot się pożywił ich adoracją i energią
* dzieciaki mają 'impressionable minds' - trzeba będzie neuronauty w wielu kwestiach. Tukan wie, kto to będzie robił...

Ogólnie, Tukan ma zamiar to zgłosić. To, co Arienikowie zrobili i za co odpowiadają nie powinno mieć miejsca. A Tukan nie jest fanem Arieników, bo to jest powiązane z Luxuritias a **to** jest powiązane z seksbotami. O.

Pięknotka wyczuła coś jeszcze - tu był jeden z magów Tukana. Ksawery Wojnicki. On użył swoich chemikaliów by wzmocnić efekt działania somnibela i by triggerować Tukana. Ale Pięknotka nie ma na dowodów, ona wie to dzięki swojemu wyczuleniu. I wie też, że Kajrat o tym nie wie - bo nie wiedział o kocie. Pięknotka korzysta z okazji - skoro Ksawery mógł wzmocnić wpływ kiciusia, ona jest w stanie użyć środków chemicznych by osłabić kiciusia. Pięknotka zatem (flavor) dostarczyła mu środków które pomogą w naprawie dzieciaków.

Następnego dnia Pięknotka odniosła kiciusia do Rezydencji Arieników. Urszula nie była szczęśliwa że okazało się, że to Małgosia a nie Janek. Trudno. A młody Staś trafił do neuronauty który dostał za zadanie pomóc mu wrócić do społeczeństwa i usunąć niekorzystne wpływy kota.

**Sprawdzenie Torów** ():

* skomplikowane.

**Epilog**:

* .

## Streszczenie

Bogatemu rodowi Arieników powiązanemu z Luxuritias uciekł somnibel. Pięknotka znalazła go u Kajrata - który dla odmiany nie porwał kociego viciniusa; dostał go od podwładnego. Kajrat oddał somnibela Pięknotce po wykonaniu pewnego testu a napięcie między Arienikami a "plebsem" z Podwiertu się podniosło.

## Progresja

* .

### Frakcji

* .

## Zasługi

* Pięknotka Diakon: znalazła somnibela, który uciekł do Kajrata i zdecydowała się na nie deeskalowanie konfliktu między nieprzyjemną arystokratką a "plebsem" dookoła.
* Urszula Arienik: wpływowa arystokratka, władczyni Rezydencji Arienik i czarodziejka Luxuritias. Kocha piękno i chroni swego syna przed "innymi". Ma wady arystokratki.
* Jan Revlen: czarodziej, półkrwi noktianin; przyjaciel syna Arieników (Stasia). Nielubiany przez Arieników. Urszula Arienik chce jego zguby.
* Staś Arienik: 15-16letni arystokrata, arogancki. Wpłynął nań somnibel. Podkochuje się w Małgosi, inną arystokratką. Ogólnie, nic ciekawego.
* Ernest Kajrat: całkowicie zaskoczony tym, że ma somnibela; porwał dla niego go Ksawery. Chciał sprawdzić czy Serafina rozerwie link somnibel - ofiara; okazuje się, że tak.
* Tomasz Tukan: nieprzekonany do pomocy Pięknotce, ale mógł pomóc dzieciakom Dotkniętym przez energię somnibela; znalazł coś u jednej z dziewczynek.
* Ksawery Wojnicki: niebezpieczny agent Kajrata (?), biochemik; tu: przekierowywał winę na Małgosię (astorianka, arystokratka) by nie był winny Janek (pół-noktianin).

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Podwiert
                                1. Osiedle Leszczynowe
                                    1. Szkoła Nowa: pierwsze miejsce, gdzie znalazł się Somnibel; tam dokonał pożywienia się 1-2 osobami.
                                1. Iglice Nadziei: dzielnica na południowym wschodzie Osiedla Leszczynowego; zamieszkana przede wszystkim przez arystokrację magów i super chroniona.
                                    1. Posiadłość Arieników: w Rezydencji Arieników, spotkanie Pięknotki z Urszulą i młodym Stasiem.

## Czas

* Opóźnienie: 0
* Dni: 2

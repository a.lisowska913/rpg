---
layout: cybermagic-konspekt
title: "Ona chce dziecko Eustachego"
threads: historia-eustachego, arkologia-nativis
gm: żółw
players: fox, kapsel
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [220914 - Dziewczynka Trianai](220914-dziewczynka-trianai)

### Chronologiczna

* [220914 - Dziewczynka Trianai](220914-dziewczynka-trianai)

## Plan sesji
### Theme & Vision

* Opłaca się współpracować z Kidironami
    * Z woli Kidironów, Eustachy, będziesz dowodzić Infernią
* Neikatis to piekło poza arkologiami, Nativis jest taka jaka jest bo musi
    * Sarderyci sami nie przetrwają.    
        * są zainteresowani Eustachym. Zwłaszcza genami.
        * Sarderyci mają 'farmy w piaskach', wykorzystują natywne byty Neikatis do syntezy rzeczy sprzedawalnych ORAZ żywności
    * Naturalna pułapka - skały + piasek + trianai + flora zagłady
    * Farighanie - puści ludzie, cyberzombie
        * assembly xenoplague

### Co się wydarzyło KIEDYŚ

.

### Co się wydarzyło TERAZ (what happened LATELY / NOW)

.

### Co się stanie (what will happen)

* S00: -> Fox "zostawisz jedzenie w korytarzach"? - ponownie
* S00: -> Kacper "jak mogłeś tak ją potraktować?" - wujek
* S00: Kidironowie, dwóch Robaków uciekło.
* S01: Znalezienie łazika Exerinn i uniknięcie problemów
* S02: Handel i pomoc Sarderytom
    * Ava x Eustachy (też do Celiny)
* S03: Lerten i 3 inne osoby zniknęły w Garden of Corpses. Lerten wraca sam zatruć Exerinn.
* S04: Konfrontacja w Garden of Corpses -> 3*farighan (inspector, scavenger, reverse engineer), poison gas

### Sukces graczy (when you win)

* .

## Sesja właściwa
### Wydarzenia przedsesjowe

.

### Scena Zero - impl

Stanisław poprosił Ardillę o to, by przyszła. Znalazł dla niej grupę przedszkolaków. Będą RAZEM pilnować przedszkolaków. Przy okazji - czy możesz zanieść Duchowi znowu jedzenie? (on nie wie). "Widziałaś Ducha?!". Ardilla wzięła jedzenie. Stanisław znajdzie jej coś innego. Np. coś przy jedzeniu. "Przy wszystkich wadach Kidironów to jest świetna arkologia i w kosmosie nie czeka Was nic lepszego".

Ardilla zaniosła jedzenie Eustachemu... który TYMCZASEM...

Wujek: "czemu tak ostro? Ona nawet nie była DLA CIEBIE groźna". Eustachy powiedział, że ona przecież miała tryb agresora i przetrwała a była zmodyfikowana. Wujek "ale ARDILLA też jest zmodyfikowana." Eustachy "zadziałał instynkt i szkolenie, musiałem przetrwać". Eustachy mówi raczej tak jak było z pominięciem pewnych newralgicznych kwestii. Wujka tam nie było, nie wie co tam się działo. Eustachy się wytłumacza.

TrZ (to Eustachy) +4:

* V: Wujek złagodniał, łyknął to. "Pamiętaj, jesteś jednym z najlepszych. Ty, Ardilla, Tymon. Janek i Celina. Wy jesteście przyszłością Inferni."
* V: Po przedstawieniu przez Eustachego WŁAŚCIWYCH faktów wujek jest przekonany, że Eustachy podjął właściwą decyzję. Nic nie zostało stracone.

Rafał Kidiron chciał rozmawiać z Eustachym. Pochwalił go za usunięcie dużego zagrożenia. Tymon go pochwalił i Rafał docenił. Rafał poprosił Eustachego o to, by ten wziął Infernię i sprawdził w Exerinnie czy są tam dwa zagubione Robaki - Michał i Staszek. Rafał woli ich żywych, ale jak się nie uda... nie będzie płakał. Więc - Rafał chce, by Eustachy określił zespół i niech działa. A on zajmie się wujkiem.

Eustachy chce:

1. biurokratę / księgowego -> dobry handel -> ...
2. logistyka -> Janek
3. infiltracja / socjal -> Ardilla
4. Celina (by była, lekarz)
5. 4 Czarne Hełmy do pomocy

### Sesja Właściwa - impl

Ardilla nie dotyka tematu Kariny. Lepiej nie. JEDYNIE Ardilla i Eustachy wiedzą DOKŁADNIE co tam się działo - Celina i Janek słyszeli tylko plotki i tylko o Eustachym. Eustachy i Ardilla ustalili wspólną wersję co tam się stało. Groźny potwór pokonany przez Was z mimicznymi zachowaniami.

Janek pyta zaciekawiony Eustachego co tam się stało. On na to że GROŹNY POTWÓR POKONANY SOLO.

TpZ+2:

* V: Janek zaskoczony ale łyknął to. Oki, może nie był TAK groźny, ale pokonany solo i przez Eustachego.
* V: Janek pod wrażeniem. Może trochę nie doceniał Eustachego. Łyknął tą opowieść.
* Vz: Janek będzie to propagował i niezależnie od tego co i jak będzie mówione będzie to dla szerokiej populacji oficjalną wersją.
    * UWAGA: prędzej czy później dojdzie do Stanisława który ma inną wersję

Eustachy i ekipa widzą najszpetniejszy statek jaki znają - Infernię. Wujek lekko niespokojny. Wyjaśnił Eustachemu, że Infernia ma psycholink. Niech Eustachy pamięta, że leci tam na handel. Eustachy na to że ma poufne informacje. Wujek się wyprostował. Wiadomość może nie dotyczy Inferni ale dotyczy Eustachego. Eustachy jest pod jego opieką. Eustachy powiedział wujkowi, że chodzi o Kidirona i Robaki. I żeby tego nie spalić.

Wujek się zastanowił chwilę. Czyli Kidiron próbuje Cię... przekształcić na swoją stronę. Kidiron jest silny. Wujek powiedział, żeby zrobił tak - przekaż MNIE te Robaki. Zostaw je na Inferni jak Ci się uda. W sprawie potwora z tuneli - Kidiron powiedział, że rozwiązał problem. Nie ma ciała, spopielone itp. Nie wiem czemu Kidiron tak się uparł na Robaki. Nic nie wskazuje na to że są kultem śmierci. Wujek WIE, że Robaki są wrobione. "Nie wiem przez kogo, ale się dowiem." Kidiron ma sporo do odpowiedzenia jeśli za tym stoi.

* wujek: "Kidiron nie jest zdrajcą ale jest monotematyczny. Arkologia, potęga i władza."
* wujek: "Nie będę Cię prosił byś kłamał Kidironowi - kłamstwo koroduje duszę. Zostaw ich na Inferni, ja porozmawiam z nimi i Kidironem"

Można po prostu STEROWAĆ Infernią, lub możesz się z nią POŁĄCZYĆ. Eustachy się ŁĄCZY z Infernią...

Tr+2+3O:

* V: Eustachy POCZUŁ potęgę TAI w Inferni. To nie jest Persefona, to jest... ŻYWE. To jest jak... jak wąż, ukryty gdzieś pod powierzchnią, jak podwodny smok.

Procedura startowa. Eustachy czuje, jak Infernia odpowiada. Jak jazda konna - ona PRZEWIDUJE ruchy Eustachego.

Teraz - gdzieś poza arkologią jest crawler Exerinn. Znasz wektor. Trzeba to znaleźć. Eustachy szuka po kolumnach pyłu, sygnaturze i po prostu lecąc po wektorze.

TrZ+3:

* Vz: Infernia bez kłopotu zlokalizowała Exerinn. Crawler stoi niedaleko... części skalistej i niedaleko wydm. Ludzie są poza crawlerem.

Gdy Infernia podleciała, Exerinn włączył 2 działka. "Zidentyfikuj się". "Infernia" "To przepraszam" i działka się wyłączyły. Da się wylądować w jednym ruchu.

* Vz: perfekcyjne lądowanie. Zero problemu.

Oni są trochę zmartwieni przez Infernię. Coś się stało? Eustachy "misja dyplomatyczna i handlowa". Koleś się zdziwił. Porozmawiajcie z Avą. Ava nie jest może szczególnie ładna (oko muchy, blizny), ale jest sympatyczna i bezpośrednia. I PATRZY NA EUSTACHEGO. I MYŚLI.

* E: "Co tak myślisz, niunia?"
* A: "Wiesz co, nie widziałam kogoś takiego jak Ty do tej pory... z rodu Korkoranów?"
* E: "Tak"
* A: "Nadasz się, dobre geny. Chcesz mieć ze mną dzieci?"

Ardilla jak doszła do tego o co mu chodzi to powiedziała Avie, że szuka dwóch Robaków. Ava potwierdziła - uciekli do Exerinna. I dostali azyl. Ale oni nie mają jak z tego uciec. Na lewo piasek na prawo piasek. A jak ktoś zginie - na części zamienne. Zwykle ludzie nie uciekają do takiego miejsca jak crawler - wolą więzienie albo ciężkie roboty.

Ava -> Eustachy "my nie handlujemy ludźmi. Ok, czasem wymienimy nerkę na płuca czy kilkanaście dobrze zamrożonych rąk za coś innego, ale ludźmi nie handlujemy". Eustachy zaproponował, by Exerinn podjechał do Nativis. Tam dostanie lepsze towary. A Eustachy ma wtedy czyste sumienie ^_^. Ava jest zdecydowanie przeciwna temu pomysłowi. Ona nie chce oddawać dobrych i uczciwych ludzi. Plus, nie ma uprawnień - ona jest jedynie twarzą publiczną Exerinna.

Eustachy przekonuje do tego, żeby powiedziała, że ta dwójka wymarła gdzieś na piaskach i oddać ich na Infernię i nie trafią do Nativis. A Ardilla "i tak ich zgarną, na łaziku problemy bo są z zewnątrz. A oni nie są przyzwyczajeni do warunków."

Tr Z (dobra współpraca Nativis - Exerinn) +3:

* V: Ava akceptuje argumenty i przekaże możliwość rozmowy z Robakami.

Maks, młody chłopak, zaprowadził ich do Robaków (i to widać, że Ardilla mu się bardzo podoba).

Robaki to Michał (bioinżynier żywności ENCAO -00-+, do the right thing, family) i Staszek (energetyk ENCAO +-0-0, opryskliwy, pozycja w grupie). Michał jest nieszczęśliwy - wyraźnie długo nie przeżyje na crawlerze. Michał wyjaśnił czemu uciekli - splice Trianai w żywności. Jest poważne ryzyko jakichś anomalnych działań, efektów, zachowań. Trianai to śmiertelnie niebezpieczny strain.

Maks nie rozumie problemu. Oni zjadają np. części ciała. Czemu splice Trianai to jest kłopot? Michał nie umie wyjaśnić - są problemy pod polem magicznym itp. Maks zauważył, że oni czasem zjadają Trianai. 

Zdaniem Michała główny problem - ta żywność faktycznie jest eksportowana tam gdzie nikt nie wie jak to niebezpieczne i może dojść do outbreak. I jakkolwiek Lycoris tym zarządza dobrze, ale eksport żywności to jest kłopot. Ardilla powiedziała im, że mogą nie wracać do Nativis, ale wrócą do Bartłomieja Korkorana. By mieli czas na dyskusję - przekonała ich, że wróci za kilka godzin. 

Ava proponuje Eustachemu, że pomoże przekonać Robaki do opuszczenia Exerinna za jego geny. Eustachy zażądał zamiast tego sprzętu - by móc robić sabotaż i nie wygląda że to jest Infernia czy ktoś z arkologii. Ava się zgodziła. Okazuje się, że jeśli Eustachy jest magiem, jego geny mają dla Exerinna wysoką wartość.

Jedzie z wydm niewielka jednostka. Mały łazik wyglądający jak jednostka Exerinna. Ava "Lerten + 3 osoby". Infernia i sensory - to 1 osoba. Ava jest zaniepokojona, Lerten nie odpowiada. Ava biegnie do Exerinna. Eustachy by "przekonać" Lertena strzela odłamkowym by porysować łazik i pokazać 'WTF'. Bądź grzeczny chłopcze.

Eustachy chce ALBO zatrzymać quad albo dowiedzieć się o co chodzi

TrZ (siła ognia Inferni) +2

* Vz: LEGENDARY RESULT. Eustachy, poczułeś... zimno, zimny intelekt gdzieś w tle Inferni. 
* X: Quad jest uszkodzony, ale nic czego Infernia nie jest w stanie naprawić
* V: Ardilla zmierza w kierunku quada. W środku
    * Jest chłopak, który wygląda jak członek tego crawlera, koło niego są butle.
        * EUSTACHY NIE STRZELAJ!

Ava "Co się dzieje? Czemu strzelacie do quada?" Infernia: "Quad się nie słucha". Okazuje się, że nie miał wszczepów a teraz ma. Ava przerażona - Farighan! Lerten został zmieniony w farighana. Exerinn włączył działa, Ardilla chce uratować Lertena. Ava błagalnie na nią patrzy.

Ardilla "atakuje" quada, by dostać się do Lertena.

Tr +3 (zamiast zasobu obniżam stopień):

* V: Wbiłaś się na quada i otworzyłaś drzwi. Lerten odwrócił się i CHUCHNĄŁ toksyną. Ardilla przesunęła wajhę by nie mógł wycelować i wyskoczyła.
* X: Nie dałaś rady złapać quada bo on się ruszył. Ardilla znalazła się na ziemi a quad ruszył w kierunku na Exerinn.
* V: Ardilla wróciła na quada i otworzyła drzwi. 
* V: Ardilla łapie Lertena i wyskoczyła z quada. Quad jedzie 5-10/h do przodu w kierunku na Exerinn. Ardilla przytrzymuje Lertena.
* V: Ardilla bez kłopotu spacyfikowała Lertena zanim ktoś dotarł.

Eustachy zrobił dołek przed quadem. Taki by wpadł w pionie. Niech sobie potem wyciągają.

7-8 osób na wydmach z bronią. Patrzą - NOPE. Farighani się wycofują.

Eustachy chce WSZYSTKICH unieczynnić zanim się pozbierają. 

Tr Z (Infernia i jej działa) +4:

* V: część osób udało się unieszkodliwić i ściąć.
* X: są trupy
* X: 3 osoby zostały "unieszkodliwione", 4 "unieszkodliwione na stałe w formie czerwonej mgły". Infernia aprobuje.

"Medyk" silnie augmentowany podbiegł, zebrał Lertena i Ava bardzo podziękowała. Czy mogą lecieć Infernią ratować? Ava "ja i pięć osób". Eustachy kazał im siedzieć w miejscu dalekim od kontrolek, zaminował Infernię (na wypadek wrogiego przejęcia) i ruszyli. Ich ekspert do spraw wojskowości nazywa się Leonidas. Jest stosunkowo drobny.

Ardilla idzie z nimi. 

Eustachy - odpala sensory Inferni. Kieruje sensory w stronę wydmy w której się znajduje Ogród Zwłok.

Eustachy zrzuca kilka czujników (sejsmograf) a potem zrzuca kilka bomb by zobaczyć kształt i co jest potencjalnie w środku - plus pełna moc sensorów Inferni.

Tr Z (seismic) + 3:

* V: Eustachy ma rozkład jaskini, pomieszczeń itp. Mimo piasku.
* Vz: Nowy plan Eustachego - bomba wbijająca się w bunkry. Bomba ma pozytywną insercję, wbiła się do odpowiedniej jaskini.
* X: Zniszczenia są większe niż planowaliśmy.
* V: Farighani zostali unieszkodliwieni.

Ekipa Avy + Ardilla mogą spokojnie wejść. A Eustachy z Inferni pilnuje.

Okazuje się, że "Ogród Zwłok" jest dokładnie tym co wskazuje nazwa. Ogród pełen kawałków ciał, tam jest sporo anomalii generujących energię magiczną i urządzeń (rozłożonych i zasymilowanych przez farighany). Exerinnianie zaczęli zbierać anomalie i zajmować się swoimi ludźmi. Ava jest szczęśliwa - wśród gnijących kawałków ciał znalazła kilka wartościowych anomalii. Eustachy i Ardilla... cóż, to nie jest to co lubią oglądać.

Sarderyci z Exerinna zaiste pochodzą z innego świata niż mieszkańcy arkologii...

Michał i Staszek widząc farighanów, resztki itp. zdecydowali się dostać na Infernię i spróbować szczęścia z wujkiem. By ukryć problemy powiedziało się, że Michał i Staszek zginęli. Jako dowody - Exerinn wymienia rękę Michała i nogę Staszka na resztki jakichś losowych farighanów. Dzięki temu Kidironowie nie będą nawet pytać. NIKT zdrowy psychicznie się nie zgodzi (Michał i Staszek się nie zgadzali, ale ich akurat nikt o zdanie nie pytał).

I wujek zadowolony i Kidironowie. I, o dziwo, Robaki, choć niekompletne.

## Streszczenie

Rift - wujek vs Kidironowie w sprawie Robaków i Dziewczynki. Eustachy rozdarty, acz staje za wujkiem. Z woli Kidironów Eustachy wziął Infernię i poleciał do crawlera o nazwie Exerinn by odzyskać Robaki które uciekły. Tam Eustachy poznał jak żyją Sarderyci z Exerinna i jak radzą sobie z trudnościami (m.in. Farighanami - nekroborgami Neikatis). Po tym jak Infernia pomogła Exerinnowi i Eustachy oddał geny Avie, Robaki wróciły z Exerinna na Infernię i Eustachy ukrył ich życie przed Kidironami - fałszywka, że stali się Farighanami.

## Progresja

* Eustachy Korkoran: oddał swoje geny Avie z Exerinna. Czyli najpewniej będzie miał dziecko :D.

### Frakcji

* .

## Zasługi

* Eustachy Korkoran: zarówno wujek jak i Rafał Kidiron próbują wygrać jego duszę. Dowodził Infernią i się z nią zintegrował; pomógł crawlerowi Exerinn, oddał geny Avie i uratował kilka osób używając bunker-bustera z EMP i wbił się do miejsca gdzie Farighanowie ufortyfikowali się przed atakiem agentów Exerinn.
* Ardilla Korkoran: zapewniła sobie współpracę Robaków którzy uciekli do Exerinna z Nativis, po czym nie dała sczeznąć Lertenowi zmienionemu w Farighana - wpadła do quada i go wyciągnęła unieruchamiając zanim on coś złego / głupiego zrobi pod wpływem implantu.
* Michał Kervendal: Nativis / Robak; bioinżynier żywności (62) który poznał prawdę o żywności z Nativis; Lycoris splicuje Trianai. Powiedział o wszystkim Ardilli i Eustachemu. Całkowicie to zignorowali. Wrócił na Infernię powiedzieć wszystko Bartłomiejowi Korkoranowi. Stracił rękę by zamaskować, że żyje.
* Staszek Zakraton: Nativis / Robak; ekspert od energii (41), złośliwy i żartujący sobie z ich sytuacji na crawlerze Exerinn. Wrócił na Infernię; życie na Exerinnie nie dla niego. Stracił nogę by zamaskować, że żyje.
* Ava Kieras: Exerinn; PR Exerinna i od załadunku. (28) Załatwiła z Eustachym i Ardillą oddanie Robaków (jak chcą), geny Eustachego i by Eustachy pomógł im pokonać Farighanów i odzyskać swoich ludzi. Zamiast jednego oka wszczep typu oko muchy.
* Maks Selert: Exerinn; podoba mu się Ardilla. Ma 15 lat i myśli że ją podrywa. Ogólnie dość przydatny, ale nie zrobił nic szczególnego - pokazał gdzie ona ma iść.
* Lerten Kieras: Exerinn; brat Avy, który miał w Ogrodzie Ciał zająć się harvestem drogich anomalii - tam napadli go Farighanie i pasożytniczym implantem zmusili do służenia ich celowi. Uratowany przez Ardillę (która go zatrzymała) i Embana (zdjął implant)
* Emban Dolamor: Exerinn; medyk. Wziął 'egzoszkielet' dosłownie budując implanty. Pomógł Lertenowi pozbyć się pasożytniczego implantu.
* OO Infernia: zintegrowała się z Eustachym po raz pierwszy; obcy umysł Inferni Dotknął Eustachego i uznał go godnym. Zestrzeliła dla Eustachego kilka Farighanów.
* VN Exerinn: crawler często współpracujący z Nativis; dołączyły doń dwa Robaki. Zaatakowany przez Farighany, Infernia pomogła Exerinnowi odzyskać ludzi i przetrwać. Brudny, ciasny i niewygodny - ale wystarczająco solidny.

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Neikatis
                1. Dystrykt Glairen
                    1. Arkologia Nativis
                    1. Ogród Zwłok Exerinna: miejsce hodowania wartościowych anomalii przez Exerinn i miejsce starcia z Farighanami; Eustachy uszkodził strukturalnie dach bunker-busterem z EMP.

## Czas

* Opóźnienie: 9
* Dni: 4

## Konflikty

* 1 - 
    * 
    * 
* 2 - 
    * 
    * 
* 3 - 
    * 
    * 
* 4 - 
    * 
    * 
* 5 - 
    * 
    * 
* 6 - 
    * 
    * 
* 7 - 
    * 
    * 
* 8 - 
    * 
    * 
* 9 - 
    * 
    * 
* 10 -
    * 
    * 

---
layout: cybermagic-konspekt
title: "Szczur który chroni"
threads: nemesis-pieknotki
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [190503 - Bardzo nieudane porwania](190503-bardzo-nieudane-porwania)

### Chronologiczna

* [190503 - Bardzo nieudane porwania](190503-bardzo-nieudane-porwania)

## Budowa sesji

### Stan aktualny

* Miasteczkowcy stwierdzili, że chomikowanie czarnych artefaktów to cholernie zły pomysł. Czarnym miejscem zostają Wolne Ptaki.
* Część Miasteczkowców, jak chciała Karla, przeniosło się do Wolnych Ptaków.
* Część Miasteczkowców woli odsprzedać artefakty Wolnym Ptakom, ale niewielka ich część.
* Wolne Ptaki stają się tyci bardziej znaczącą frakcją i lokalizacją.
* Pojawia się prototyp szlaku handlowego między Wolnymi Ptakami i Cieniaszczytem.
* Gdy Nikola będzie odpalać plany przemytu, Pięknotka będzie wiedziała (ścigacz)
* Artefakty Czarnej Technologii trochę są porozrzucane po terenie.
* Eliza Ira straciła swoją sieć szepczących kryształów. Nie połączy się z nikim aktywnie; teraz to może tylko odpowiadać na zapytania.
* Pięknotka wykorzystała swoje wszystkie znajomości - zaczyna małą kampanię antyElizową na tym terenie, "whispers from the crystals".
* Wpływ Elizy wzrósł. Są osoby wiedzące o niej i o jej ideałach i są osoby chętne by jej pomóc. Ale nie w Pustogorze i nie u kogokolwiek ważnego.

### Pytania

1. Czy Adeli uda się uratować Oliwię i Krystiana?
1. Czy Grzymościowi uda się dorwać Adelę?

### Wizja

* Oliwia jest uzależniona od kralotycznego proszku, jej brat terminus jest też przez to w matni
* Adela zdecydowała się pomóc, ale podczas próby zdobycia proszku doszło do katastrofy
* Fallout: doszło do rozproszenia proszku po okolicy

### Tory

* Proszek wpłynął negatywnie na teren: 3
* Adela zapłaci głową: 4
* Terminus zapłaci głową: 4
* Ernest znajduje cel: 3

Sceny:

* Scena 1: Erupcja proszku

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Misja właściwa

**Scena 1: Erupcja proszku**

Na Nieużytkach doszło do skażenia obszaru - jakieś środki, jakiś proszek. Jest magiczny, jest z przemytu i potencjalnie (bo nie stwierdzono inaczej) jest niebezpieczny. Siły Pustogoru postawione w gotowości. Naprawić sytuację. Contain the corruption.

Na miejscu są już magowie próbujący opanować sprawę jak i Lucjusz Blakenbauer i część magów ze szpitala terminuskiego. Aeromanci próbują kontrolować sytuację. Pustogor doprowadził do opadów, blokując jednocześnie odpływy.

Pięknotka też tam jest, w hazmacie. Natychmiast skontaktowała się z Blakenbauerem - co to jest? Lucjusz powiedział, że coś na bazie kralotycznych środków; niebezpieczne. Jest tu nawet oficer Grzymościa. Containuje. Tak samo jest Adela.

Pięknotka zaczyna analizę chemiczną tego cholerstwa - co to jest i jak to zatrzymać. (Tp:P). Pięknotka wie jedynie, że to jest niebezpieczny środek kralotyczny; ma ogólne odczynniki ale nic precyzyjnego. Natychmiast dostała wiadomość od Adeli - ona i Lucjusz. Adela powiedziała co to jest - kralotyczny proszek który przekształca energię magiczną w podniecenie; im bardziej próbujesz czarować tym mocniej na Ciebie to działa. Rozprzestrzenia się powietrzem, wodą i przez dotyk. Silny narkotyk.

Pięknotka stwierdziła, że ktoś kto na to wpadł jest chory psychicznie. To nie jest porcja na imprezę, to ma wystarczyć na dużo dłużej. Pięknotka z grupą innych magów (włączając Adelę) składają rytuał, który cały ten proszek ma osłabić. Przekształcić go by wymagał np. części pnączoszpona by odzyskać "zęby".

* Działa na całość proszku: X
* Wymaga super trudnego składnika (pnączoszpon): V
* Opanowana sytuacja; Pustogor jest w stanie sam poradzić: V
* Sam w sobie jest nieczynny: V
* Magom w rytuale nic się nie stało: X

(TrM+2:15,3,5=MV0)

Sytuacja jest jakoś opanowana; część magów potrzebuje natychmiastowej pomocy Lucjusza (który tu jest, więc nie ma problemu), ale ogólnie proszek kralotyczny został zneutralizowany. Ernest Kajrat z mafii zostaje i pomaga w opanowaniu sytuacji; sam uczestniczył w rytuale. Pięknotka do niego podchodzi, pogadać. Ernest powiedział Pięknotce, że to byli terminusi pustogorscy - zaatakowali z dwoma konstruminusami. I zrobili błąd - każdy przemytnik wie że NIE STRZELA SIĘ DO TOWARU.

Ernest obiecał Pięknotce, że dostanie dane. Jest dość wysoko w hierarchii Grzymościa; ogólnie, ma dobry humor i poczucie humoru. Nie trafiło na leszcza a na weterana. To rzadki i ważny transport, kosztowny. Na pewno Grzymość to odczuł - i jeszcze będzie czuł przez dłuższą chwilę.

Pięknotka poinformowała Karlę, że sprawdza czy to ktoś od nich - a jak tak, to jej powie. No i potrzebuje danych z samego Pustogoru. I znowu w akcji jest poczciwy Bożymir - niech znajdzie Pięknotce czy faktycznie w akcji uczestniczyły siły Pustogoru. A przynajmniej - oficjalnie. (Tp:VV). Bożymir powiedział Pięknotce z ogromnym zdziwieniem, że to były siły Pustogoru. Dwa konstruminusy starej generacji z Podwiertu. Zapasowy magazyn. Ale one nie byłyby w stanie działać bez autoryzacji kogoś z samego Pustogoru. Autoryzacja zazwyczaj jest nagrana, ale tym razem konstruminusy nie miały połączenia z hipernetem. To nie były pełnosprawne konstruminusy, były w magazynie by je naprawić "kiedyś". Ale tam może wleźć raczej tylko terminus lub ktoś uprawniony. No i znowu - po działaniach Sataraila część zabezpieczeń jest łatwiejsza do przejścia.

Aha, NIC w danych pustogorskich nie wskazuje, by Pustogor wiedział o tym transporcie. Pięknotka złapała Ernesta - skąd napastnicy wiedzieli o tym transporcie. Ernest nie wie, myślał, że te dane są w Pustogorze. Nie, nie są. Ernest uznał, że w takim razie Grzymość ma kreta. Nie wie jeszcze, ale się dowie.

Koniec końców, wiedzą to raczej tylko terminusi pustogorscy. Więc wiele wskazuje, że to byli terminusi... lub terminus. Chyba, że to niesamowity zbieg okoliczności. Bożymir dał znać, gdzie znajduje się ostatnia znana pozycja tych konstruminusów - w Podwiercie, w odlewni stali.

Pięknotka bierze Alana jako zabezpieczenie siłą ognia. Dostała overriding codes. Czas udać się do odlewni w Podwiercie.

**Scena 2: Odlewnia w Podwiercie**

Dwa konstruminusy starego typu są gdzieś w Odlewni. Pięknotka, Alan i dwa konstruminusy nowego typu zbliżają się do Odlewni. Pięknotka i Alan weszli ostrożnie; ogólnie, Odlewnia działa. Znaleźli konstruminusy bez problemu - zostały przetopione. Konstruminusy są w kadzi z rozżarzonym metalem i coraz bardziej ulegają degeneracji; tylko fakt tego czym były sprawia, że jeszcze nie w pełni zostały roztopione.

Pięknotka uderzyła do brygadzisty. Kazała zatrzymać kadzie i wyłowić to cholerstwo; to dowód przestępstwa. Pięknotka pomachała uprawnieniami Obrony Terytorialnej. (Tp+2:VV). Brygadzista bardzo Pięknotkę przeprosił i ruszył do roboty; udało się wydobyć te nieszczęsne konstruminusy. Bardzo ciężko uszkodzone, czarne skrzynki zostały niewprawnie wysadzone. Ktoś wiedział, jak działa konstruminus.

A brygadzista zaczął przepraszać. Powiedział, że zapłaciła mu taka jedna. Te roboty szły za nią, ale jej nie słuchały, ktoś inny rozkazywał. Konstruminusy nie chciały wejść do kadzi, ale je chciała wepchnąć. Nie umiała, konstruminusy zaczęły walczyć i ktoś je poustrzelał. Wpadły tam. On - brygadzista - nie pytał, myślał, że to jakaś mafijna sprawa jest, ale płacili i bał się odmówić mafii. Zawsze warto nie odmawiać. Ale jak ktoś przez to zginął, to brygadzista pogadał z Pięknotką. Nie chciał by ktoś zginął i się źle przez to czuje.

Pięknotka ściągnęła maga mentalnego. Mag mentalny przedstawił sytuację:

* "tłusta kobieta" to maska. Iluzja albo coś. Nie rusza się właściwie. Nie jest to mistrzyni aktorstwa.
* ta kobieta potrafiła rozmawiać z brygadzistą. Ma coś wspólnego z półświatkiem i wie co nieco o świecie ludzi.
* ta kobieta nie umie walczyć. Walczy jak walka uliczna, nie z konstruminusami. Cios w jaja nie pomoże.
* był tam ktoś jeszcze. Celny, precyzyjny. Najpewniej terminus. Najpewniej miał uprawnienia, bo konstruminusy nie próbowały się bronić.

Pięknotka przeszła się do magazynów pustogorskich. Faktycznie, są uszkodzone; wyraźnie dawno nikt tu nie działał i niczego nie naprawiał. Wiktor wyrządził ogromne zniszczenia a dla Pustogoru nie są to ważne rzeczy, skoro i tak nikt nieuprawniony tego nie uruchomi. Pięknotka połączyła się z systemami, próbuje coś z nich wyciągnąć. Kolejna niespodzianka - systemy nie mają połączenia z Pustogorem, więc działają na bazach awaryjnych.

Pięknotka łączy się z bazą i dostała odpowiedź kto pobrał te konstruminusy. Niejaki Kasjan Czerwoczłek. Autoryzował się przy użyciu DNA. Pięknotka sprawdziła wysyłając sygnał do Petra - odpowiedź jest taka, że Kasjan tam jest, cały czas. Ale gdy Pięknotka powiedziała o DNA, Petr dał jej połączenie z Kasjanem. Kasjan... nie chce pomóc Pięknotce. Nie ma w nim miłości do Pustogoru. Nie chce pomagać Pustogorowi.

(TrZ+2:SS). Kasjan powiedział Pięknotce "prawdę". Napadli i pobrali mu materiał genetyczny. Pietro potwierdził tą historię. Pięknotka się wściekła - pomogła mu, dała mu kontakt do Pietra. A on jej się tak odpłaca. De facto, Pięknotka wyprowadziła go z równowagi. Krzyknął, że to nie chodzi o Pięknotkę. To nie ona chciała mu pomóc. Od samego początku Adela pomagała. Wszystko to robiła Adela. Wszystko!

Pietro wyrzucił Kasjana. Kasjan nie chciał powiedzieć Pięknotce niczego więcej. Pięknotka wyczytała, że zdaniem Kasjana Adela kogoś chroni. Innymi słowy, Adela stoi za tym wszystkim. Znowu. Ale Adela nie ma uprawnień terminuskich; mogła użyć Kasjana by uruchomić konstruminusów, ale nie mogła nimi dowodzić. Jest ktoś inny, ktoś, kogo Adela chroni.

Alan spytał Pięknotkę czy coś ma. Pięknotka odpowiedziała, że ma. Ślad.

**Scena 3: Połączenie hipernetowe**

Adela nie jest w Pustogorze. Zniknęła. Pięknotka połączyła się z nią hipernetem.

Pięknotka powiedziała Adeli, że wie, że to ona. Adela sprzedała Pięknotce piękną bajkę o tym, jak to ona wszystko zaplanowała i wszystko sama zrobiła. Pięknotka by prawie uwierzyła - tylko wie o drugim terminusie. Adela, w rzadkim dla siebie przypływie szczerości, powiedziała Pięknotce że dostała dzięki niej drugą szansę. Ale wiele innych szczurów ulicznych nie dostało. I Adela próbowała pomóc, z nadzieją, że jej nie wykryją. Pięknotka uświadomiła Adelę, że mafia też ją znajdzie - i zniszczy wszystkich, którzy Adeli pomogli.

Adela jest szczurem zapędzonym pod ścianę. I zamierza walczyć. Pięknotka i Adela, obie wiedzą, że Adela nie ma żadnych szans. Ale Adela nie chce odrzucić pozostałych, tych których chroni. Woli wziąć wszystko na siebie i kupić im szansę.

Pięknotka chce, by Adela się poddała Pustogorowi. To jej największa szansa na uniknięcie nędznego losu. A los będzie nędzny, jak dorwie ją mafia. Każda akcja Adeli sprawia, że Pięknotka chce iść się powiesić...

(TrZ+1:11,3,7=P). Adela bardzo Pięknotce podziękowała. Powiedziała, że bardzo sobie ceniła ją jako mentorkę. Żałuje, że nie była lepsza. Po czym rozłączyła się na hipernecie. Adela zdaje sobie sprawę z tego, że to jest sytuacja z której się nie jest już w stanie wydostać. Ale przynajmniej może osłonić tych, których próbuje osłonić.

Pięknotka ma komunikat. Po pięciu minutach połączył się z nią terminus. Nazywa się Krystian Namałłek. Powiedział Pięknotce mniej więcej na czym polega problem:

* Krystian pochodzi z dołów i nizin. Ma siostrę, Oliwię. Oryginalnie on pochodzi z okolic Toporzyska.
* Został przeniesiony do Pustogoru i zaczął tu jakoś działać, rok temu.
* Zainteresowała się nim (jako terminusem) mafia. I Oliwia wpadła w kłopoty. To ona uzależniła się od tego proszku.
* Adela pomogła schować Oliwię i ją leczyła - ale bez proszku Oliwia by zginęła.
* Podkradał proszek, ale w końcu doszło do tego - najpewniej to była pułapka.
* Adela przejęła plan by wszystko pogubić i pochować. Jak widać, jej się nie udało.
* Krystian jest skłonny na 'turn in' i inne takie. Adela, oczywiście, nie chce o tym słyszeć.
* Krystian nie chce, by Adeli stała się krzywda. To w końcu jego walka a nie jej.

Krystian będzie współpracował; wie, że może czekać go śmierć. Chce tylko, by dało się uratować Adelę i Oliwię. Pięknotka ma pomysł - chory pomysł - kazała mu czekać.

**Scena 4: Pięknotka na Zjawosztup**

Czas na spotkanie z ulubieńcem Pięknotki - Wiktorem Satarailem. Kustosz Trzęsawiska pojawił się niedaleko terminuski. Pięknotka poprosiła o bezpieczne przejście dla Krystiana i azyl dla Adeli. Czy Wiktor może Adelę na jakiś czas schować? Ona ma serce po właściwej stronie, nawet troszkę za duże... a jak chodzi o Krystiana to... no i mafia... i...

Czas na negocjacje. Wiktor chce od Pięknotki katalizatory z Senetis; rzeczy których sam nie jest w stanie zdobyć. Silnie modyfikujące strukturę biologiczną. Pięknotka przenegocjowała (Tp:S) że nie jest w stanie mu tego załatwić; więc stanęło na tym, że Pięknotka dostarczy 10 medikitów Wiktorowi. I konserwy. Sporo konserw. Ok :-).

**Scena 5: Operacja: Sensus**

Ernest z oddziałem szturmowym dał radę zlokalizować gdzie znajduje się Adela, Oliwia i Krystian. Siły:

* Oddział szturmowy: 3: 
* Zwiadowcy: 2: 
* Ernest: 3: 

Ze swojej strony mają przeciwko sobie:

* Zrujnowany Sensoplex: 2: 1 
* Pułapki Krystiana i Adeli: 2: 1
* Są dobrze schowani: 2: 

Tak więc sytuacja jest relatywnie nieciekawa. A najlepsze jest to, że gildia pilnująca teren, Towarzystwo Historyków Pustogoru, nic nie wie. Jak tylko Pięknotka wróciła w zasięg, dostała wiadomość od Krystiana. Mafia ich znalazła. Walka w toku. Okopali się w Sensopleksie. Mafia ma tam oddział szturmowy, Adela i Krystian próbują się ochronić korzystając z pułapek i mega szczurzych metod.

Pięknotka zrobiła to, w czym jest świetna. Wezwała natychmiastowe wsparcie oddziału terminusów. Pięknotka natychmiast skontaktowała się z Krystianem. Niech zostawi siostrę. Bierze Adelę i ucieka na bagna po tym, jak strzeli w Pięknotkę; ona da radę zapewnić pełną dywersję. (TpZ+2:S). Krystian zrobi dokładnie to, co Pięknotka kazała. Czas na starcie.

Skrzydło terminusów wpada w okolice Sensoplex wraz z Alanem. Tam... ciecie nic nie wiedzą. Terminusi wchodzą. Standard search pattern. Pięknotka CHCE doprowadzić do starcia (-1 Wpływ: terminus kazał się zatrzymać i dostał strzał ostrzegawczy. Terminus nie używa ostrzegawczych - strzelił by wyłączyć z akcji). I zaczęła się wojna.

Detonacja broni sonicznej; próba odcięcia mafii i terminusów. Czy się zakopią. (TrZ:REROLL, ZVV). Pięknotce udało się użyć power suita i pięknie wyleciała z obszaru zawalającego się; 3 terminusów jest zakopanych wraz z kilkoma mafiozami. Są: Pięknotka, Alan, terminus oraz grupa mafijna.

Pięknotka widzi w oddali jak Ernest w power suicie celuje w Adelę próbującą się wymknąć i strzela prosto w nią. Krystiana w tym miejscu nie ma. Pięknotka wysyła sygnał do Alana "RATUJ!". Najcelniejszy i najszybszy terminus w Pustogorze używa Fulmena i strzela do Krystiana.

* Nikomu nie stała się krzywda: V
* Sensoplex nie zawalił się nikomu na głowę: X
* Adela jest w stanie uciekać: V
* Alan nie dostanie opierdolu po akcji: X
* Ernest nie zostanie bardzo ciężko ranny: X

(HrZ+2:8,5,18= REROLL, SS)

Alan wystrzelił działem klasy Koenig i ZMIÓTŁ Ernesta. Jego ciężki power suit jest roztopiony. Rezonator walnął jak broń soniczna. Pięknotka natychmiast odpala Cienia. Sensoplex, ten budynek gdzie się znajdują, właśnie się rozpada. Pięknotka natychmiast odpala Cienia - nie spodziewała się TEGO. (TrZ+2:SS). Budynek zawalił się na m.in. Alana i kilku innych mafiozów; Alan jest unieruchomiony.

"No cóż. Słaba konstrukcja" - Alan, z filozoficznym spokojem, gdy budynek wali mu się na głowę

Pięknotka wygrzebała się z gruzów i zobaczyła dwóch mafiozów, poddających się (Alan to robi z ludźmi). Udaje że walczy z Cieniem, ale oddaje mu kontrolę. Cień rozpastwia się na jednym mafiozie, skupiając się na jak najbardziej widowiskowym i bolesnym przedstawieniu (TrZ:9,3,7=XX). Cień się pasł na mafiozie, ale z budynku spadł głaz. Cień odskoczył, zmienił cel i leci na terminusa. A Pięknotka czuje krew w ustach. Nie swoją. Reputacja Pięknotki przez niedopowiedzenie rośnie.

Terminus rozpoczął walkę z Pięknotką (TpZ:12,3,3=VV). Pięknotce udało się zdewastować terminusa jednym solidnym uderzeniem. Cień rozciął power suit jak papier. Terminus dostał ranę brzucha gdy Cień wbił weń swój szpon.

Alan musiał zareagować. Wystrzelił z Koeniga prosto w Pięknotkę w Cieniu. Nic innego nie przebije się przez te kamienie.

* Fulmen jest nienaruszony: 
* Cień utrzymał się na Pięknotce (nie wyłączył się): 
* Nie ma ran na nikim dookoła: 
* Budynek stoi: 
* Pięknotka nadal ma kontrolę nad Cieniem: 

(Hr: NIE MA KONFLIKTU). Cień się wyłączył. Siła ognia Fulmena działem Koenig była po prostu za duża. Cień nie był w stanie osłonić Pięknotki w pełni; terminuska straciła przytomność. Jest ciężko ranna, nic więcej już nie zrobi. Fulmen jest lekko uszkodzony, przegrzany i ogólnie Alan nie powinien był strzelać. Naruszył wszystkie możliwe bezpieczniki. Mafiozi ewakuowali cieżko rannego Ernesta. Odechciało im się walczyć.

Krystian i Adela zabrali Oliwię na Trzęsawisko. Wiktor był lekko zdziwiony jeszcze jednym człowiekiem do pomocy, ale OK - jest w stanie sobie z nią poradzić.

A Pięknotka skończyła w szpitalu u Blakenbauera. Alan dostał opierdol jak rzadko.

Wszystko skończyło się dobrze?

**Sprawdzenie Torów** ():

* Proszek wpłynął negatywnie na teren: 3: 2
* Adela zapłaci głową: 4: 1
* Terminus zapłaci głową: 4: 1
* Ernest znajduje cel: 3: 3: V

**Epilog**:

* Cała wina za wszystko spada na Krystiana - a Krystian "zaginął na bagnach"
* Oliwia znalazła pomoc u Wiktora Sataraila
* Adela jest tutaj "ofiarą" Krystiana. A przynajmniej mafia się do niej nie dopieprza.
* Adela rozpoczyna naukę u Wiktora Sataraila - chce tego czy nie. A docelowo Kić chce ją pod Lucjuszem Blakenbauerem.
* Pięknotka ma tydzień w szpitalu. A Alan ma solidny opierdol.
* Karla priorytetyzuje Podwiert i okolice. Tam się dzieje coś niewłaściwego. Trzeba to wyczyścić.

## Streszczenie

Zaczęło się od Skażenia proszkiem kralotycznym Nieużytków Staszka. Potem Pięknotka współpracując z mafią doszła do tego, że Adela i terminus stoją za tą sprawą. Potem Pięknotka uratowała Adelę i terminusa wprowadzając szturmowych terminusów Pustogoru na mafię. Gdy "Cień wyrwał się spod kontroli", Alan ją strzelił z działa strumieniowego. I tak skończyło się rumakowanie na tydzień - ale wszyscy są bezpieczni.

## Progresja

* Pięknotka Diakon: Cień oraz ona dostają reputację morderczych potworów w mafii. I wśród terminusów. Jak jest w Cieniu - uciekaj.
* Adela Kirys: dostaje lekcje szamaństwa, zielarstwa i alchemii u Wiktora Sataraila, który przy okazji pokazuje jej jak ewoluować w coś sensownego
* Oliwia Namałłek: otrzymała pomoc u Wiktora Sataraila; wyleczy ją z uzależnienia po jakmiś czasie
* Krystian Namałłek: przez pewien czas został z Wiktorem i Oliwią i Adelą na Trzęsawisku
* Alan Bartozol: działo Koenig zamontowane na Fulmenie działa perfekcyjnie. Jego nowa standardowa konfiguracja. Wymaga trochę więcej chłodzenia. Warte opierdolu.
* Alan Bartozol: reputacja wybitnego strzelca i straszliwego przeciwnika jedynie się wzmocniła. Nikt nie chce się z nim pojedynkować.

### Frakcji

* .

## Zasługi

* Pięknotka Diakon: uratowała Adelę mimo, że kosztowało ją to: trafienie z działa Koenig, tydzień w szpitalu, negocjacje z Satarailem i ogólnie mnóstwo bólu głowy i wysiłku.
* Adela Kirys: chciała ratować Krystiana i Oliwię i zmontowała najdzikszą konspirację proszku kralotycznego, która się rozpadła na jej oczach. Trafiła na Trzęsawisko do Wiktora.
* Ernest Kajrat: prawa ręka mafii. Oficer Grzymościa. Niebezpieczny w walce, z dużym poczuciem humoru. Polował na Adelę i Krystiana; skończył trafiony przez Alana z Koeniga.
* Lucjusz Blakenbauer: skażenie kralotyczne było tak silne, że aż musiał przyjść na Nieużytki Staszka robić ratunkowe sytuacje.
* Kasjan Czerwoczłek: pomógł Adeli w autoryzacji starej generacji konstruminusów; podpuszczony przez Pięknotkę wyleciał od Pietra. Stoi po stronie Adeli, nie władz.
* Krystian Namałłek: terminus, który próbował ratować siostrę za wszelką cenę. Cała wina spadła na niego by chronić Adelę. Zna się na konstrukcji konstruminusów.
* Oliwia Namałłek: ofiara mafii i siostra Krystiana, uzależniona od kralotycznego proszku. Trafiła do Wiktora Sataraila na leczenie. W sumie - przyczyna wszystkich problemów.
* Wiktor Satarail: pomógł Pięknotce przeprowadzając Krystiana przez Trzęsawisko i biorąc do siebie Oliwię i Adelę na jakiś czas. Adelę nauczy, Oliwię wyleczy.
* Alan Bartozol: wezwany jako wsparcie, odpalił dwukrotnie działo Koenig - masakrując oficera mafiozów i masakrując Pięknotkę w Cieniu. Ogólnie, masakrował.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Zaczęstwo
                                1. Nieużytki Staszka: tam doszło do ogromnego skażenia proszkiem kralotycznym, który wszyscy próbowali odkazić
                            1. Podwiert
                                1. Odlewnia: w gorących kadziach metalu pływały zniszczone konstruminusy
                                1. Bastion Pustogoru: nieużywane magazyny pustogorskie; znajdują się tam m.in. nieaktywne konstruminusy
                                1. Sensoplex: opuszczony ale jeszcze zadbany; do momentu ataku mafii na Adelę i dwukrotnego użycia działa Koenig przez Alana
                        1. Trzęsawisko Zjawosztup: miejsce, gdzie skończył: Krystian, Oliwia i Adela. Tymczasowo. Pod czujnym okiem Wiktora.

## Czas

* Opóźnienie: 2
* Dni: 2

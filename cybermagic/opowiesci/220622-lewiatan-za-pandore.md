---
layout: cybermagic-konspekt
title: "Lewiatan za Pandorę"
threads: legenda-arianny
gm: żółw
players: fox, kapsel, kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [220615 - Lewiatan przy Seibert](220615-lewiatan-przy-seibert)

### Chronologiczna

* [220615 - Lewiatan przy Seibert](220615-lewiatan-przy-seibert)

## Plan sesji
### Co się wydarzyło

* .

### Co się stanie

* .

### Sukces graczy

* .

## Sesja właściwa
### Scena Zero - impl

Eustachy, wiadomość od Diany. Zatroskana - budzi Eustachego, bo się boją Rzeźnika Diakona. Diana powiedziała co podsłuchała:

* Ola Szerszeń dostała uprawnienia od Rzeźnika by zobaczyć jak działa containment chamber. Dał jej.
* Rzeźnik ma COŚ WSPÓLNEGO z bronią biologiczną i podobno działał przeciw siłom Orbitera?
* DIANA UWAŻA ŻE TO KONSPIRACJA! "Ola i Rzeźnik" śpią razem, on ją czymś zaraził, i teraz ona jest jak zombie. KONSPIRACJA przeciwko Ariannie!

Eustachy -> Ola Szerszeń odnośnie kontroli sprawności jednostki. Ola powiedziała, że analizuje Containment Chamber (Regenerator) Eleny. Eustachy -> Arianna, czy ma prawo dowodzić Olą. Tak. Arianna zaakceptowała.

W ciągu 5 minut na pokładzie Pająka mamy: Eustachego (mającego uprawnienia do dowodzenia Olą), Klaudię (która spała ale rządzi Olą). Czyli - Regenerator Eleny, Chevalier, Olka rozstawiająca sztuczne rośliny (wyglądają na endemiczne dla Verlenlandu) i lampy.

Ola zapytana co robi i dlaczego to robi odpowiedziała ze spokojem, że chciała się upewnić, że jak Elena opuści to miejsce to będzie jej łatwiej. Wyraźnie Oli zależy na dobrostanie Eleny. Jak to stwierdziła kąśliwie do Eustachego, gdyby miała być jego podwładną to by myślała o zmianie kariery patrząc na stan Eleny i potencjalnie innych jego podwładnych.

Eustachy na to: "Rozważ karierę jako rozwożenie pizzy na K1, jesteś bowiem moją podwładną".

Ola jest zmierzła. Nie jest zadowolona z tego powodu, że Eustachy faktycznie skutecznie dał radę przekonać Ariannę by on i Klaudia byli wyżej niż ona w łańcuchu dowodzenia. Nie ukrywała swojego niezadowolenia, ale zasalutowała. Gdy Eustachy nacisnął, włączyła głupawy uśmiech (korporacyjna radość "tak jest kapitanie! Co tylko zażądasz kapitanie!"). Eustachy niezadowolony, ale nic nie da się zrobić.

Klaudia się upewniła, że wszystko jest mniej więcej w porządku. Fakt, Ola tylko skanowała stan Regeneratora Eleny i chciała sprawdzić jak ta się czuje - i czy da się Elenę zniszczyć używając Regeneratora (?!). Nie, nie da się. Klaudia natychmiast włączyła nowe zabezpieczenia do Regeneratora - tylko osoby UPRAWNIONE mogą wchodzić z tym w interakcję. I Otton ma dbać o bezpieczeństwo Eleny (przeniosła go na Pająka).

Klaudia doszła też do tego, że kapitan TEGO STATKU to zdrajca Orbitera. I użył broni przeciw Orbiterowi. Czyli Rzeźnik FAKTYCZNIE jest podły.

* Kapitan statku nazywa się Rzeźnik Parszywiec Diakon.
    * Podczas wojny noktiańskiej dowodził małym oddziałem. Spotkał się z przeważającymi siłami noktian. Obrócił się przeciw swoim podwładnym i zdradził, przeszedł na noktiańską stronę
    * Ściągnął noktian na 1-2 placówki używając swojej wiedzy i większe zniszczenia
    * Wykorzystał swoje ciało (mag) do syntezy broni biologicznej i biotaumicznej, masakrując oddział noktiański
    * Wrócił do Orbitera
    * Wywalili go z Orbitera, ale jako, że on bardzo pomógł Aurum swoimi działaniami to wrócił do Orbitera z przyczyn politycznych
    * Nie jest już magiem
    * Od zawsze pasjonował się taumicznymi agentami wirulentnymi

Arianna dostała to jako informacje "w mailu, na kanałach magicznych" + info o kombinowaniu załogantów. I jej problem.

Następnego ranka, Arianna i Martyn. Martyn złożył smutną skargę do Arianny - "nie ma uprawnień do pomocy Oli Szerszeń". Nie ma jej bioskanu, nie ma uprawnień by wydobyć jej bioskan.

* Podobno Szerszeń może być naprawiona przez Wiewióra
* Martyn nie dostał uprawnień do leczenia Szerszeń

Arianna -> Klaudia, niech Klaudia dowie się o tym jak wyleczyć Olę Szerszeń. A Martyn niech próbuje jak coś.

Klaudia -> Maria Naavas. Czy może się coś dowiedzieć na temat Oli Szerszeń. Czemu zapieczętowane dane. Maria z optymizmem rusza do działania.

TrZ+3+5Or:

* X: Maria zostawia pożogę, zniszczenie i złamane serca po drodze. Norma.
* V: Pierwszy rząd informacji - Ola Szerszeń jest efektem nieudanego eksperymentu. Termia coś chciała osiągnąć ale nie wyszło. Acz jest dość przydatna.
* Or: te wszystkie ruchy spowodowały poważne komplikacje.
* Or: POWAŻNY PROBLEM.
* Or: polityczny problem, poważny, POWAŻNY. Działa jak drugi ptaszek.
    * biosynt z przekopiowanym umysłem. Chora wersja klona.
    * ma nanitki we krwi. Silna regeneracja itp.

Maria już od pewnego czasu robiła ruchy przeciwko adm. Aleksandrze Termii. A teraz weszła w gniazdo szerszeni (pun intended). I to spowodowało, że Maria ma problem ale Termia też. Dopiero się zaczęło, ale za to JAK się zaczęło :D.

Tymczasem Arianna spotkała się z Rzeźnikiem. Jowialny, sympatyczny. To przeszłość. Najmniej problematyczny we flocie i najbardziej sympatyczny. Noktianie go NIENAWIDZĄ. Czyli Arianna widzi, że ma do dyspozycji najpoczciwszego i najmniej lubianego kapitana. Acz oczy mu się zapaliły jak zaczął planować, jak mógłby użyć Straszliwego Pająka do zniszczenia Grupy Wydzielonej Infernia.

Arianna widzi, że on nie jest problemem. Ale może być jak musi ;-).

### Scena Właściwa - impl

Lewiatan. Leci dość spokojnie w stronę Tivru. Jest powiązany energetycznie z Vigilusem, czyli próbuje Zrozumieć i Przejąć Kontrolę. To cenna informacja dla Klaudii, która nagle wie jak sobie z tym radzić.

Klaudia analizuje Lewiatana. Pobieżne informacje.

TrZ (mały lewiatan) +3:

* X: mały lewiatan wymaga OPANOWANIA.
* V: byt energetyczny z elementami fizycznymi, rośnie. On "digitalizuje" - niesie wirtualny świat. Skupia się na TAI i bytach sztucznych i umysłach. Będzie się rekonfigurował w farmę amat przy gwieździe.

Mały Lewiatan przekształcił się w coś w stylu kuli - ładunek strumieniowy. Arianna wchodzi z nim w interwencję magiczną. Chce pobudzić zainteresowanie i chęć zdobywania informacji.

TrZM+3Ob+4:

* Om: Arianna nawiązała połączenie z Lewiatanem. Filtry Klaudii pomogły, bo nie dorwał Bazyliszek, ale ma ŻYCIE. Na hardware Lewiatana chodzi Persefona z Tivru. Ten byt jest INNY.
* V: Lewiatan jest OPANOWANY. Związany magicznie. Magiczny kokon. Sygnały PANIKI od tej istoty.
* V: Najbardziej interesujący umysł dla Lewiatana. Arianna używa Klaudii by pomogła w uspokojeniu (+1Vg)
* Vz: Klaudia jako zasób; techniki interaktujące z 'dzikimi TAI' zadziałały w połączeniu z magią Arianny którą ów byt uważa za swoją. Byt przywiązuje się emocjonalnie do Was.

Eustachy poprosił o 2 torpedy anihilacyjne. Tym razem dostanie. Trzeba opanować Lewiatana Vigilusa, zanim ten zrobi krzywdę Seibert.

Plan "ratujemy ludzi których się da". Raoul w pintce, dostaje sporo środków medycznych do wyłączania ludzi, Tivr robi manewry ściągające uwagę Lewiatana a Infernia kontroluje i reaguje czy nic złego się nie dzieje. Raoul ma za zadanie dostać się, wydobyć "dobrych pilotów" do ichnich pintek, następnie wyłączyć ile ludzi się da i mamy ewakuację roju pintek bo za małe.

Raoul dostaje się na pokład statku #1, potem #2, potem #3...

TrZ (Tivr odwracający uwagę) +2:

* X: Lewiatan zaczął rekonfigurować formę
* V: Raoul dostał się na statek
* (Malictrix: +2Vg, +3Or) Vz: Raoul dał radę zapewnić odpowiednie działania dla pierwszej jednostki i zebrać sprawne osoby z pierwszej jednostki. A jak Raoul coś zrobi, wysadzamy statek
* X: część osób jest zdigitalizowana. Nie poradzimy na to. Czasem potrzebny jest coup de grace.
* V: Raoul rozprzestrzenił operację na pozostałe jednostki.
* Vz: Raoul jest w stanie stopniowo ewakuować ludzi. 10% pintek zniszczonych lecąc do Inferni

Seria pintek / kapsuł w kosmosie i oddalanie Lewiatana. Pająk ich pozbiera i przekaże na stację. Jest to chyba najbezpieczniejszy sposób radzenia sobie z tą sytuacją.

Eustachy wykorzystuje torpedę anihilacyjną, podlatuje w odpowiedni sposób do Lewiatana i strzela by zniszczyć masę krytyczną materii, by rozproszyć bydlaka.

TrZ (Tivr odwracający uwagę) +3 +3Or:

* XXX: jednostki są ciężko uszkodzone. Zarówno Lewiatan jak i fala anihilacyjna.
* X: OO Pandora została zniszczona przy okazji, ale torpeda anihilacyjna "weszła". Mniejszy koszt jednostek.

Malictrix bowiem nie była w stanie poradzić sobie z odparciem Lewiatana. Uznała, że MUSI go zniszczyć i weszła w promień rażenia Lewiatana. Tak, zdekoncentrowała go, ale zapłaciła sobą i swoją jednostką.

## Streszczenie

PRZESZŁOŚĆ: Ola Szerszeń opiekuje się Eleną w Regeneratorze; okazuje się być biosyntem (zakazane eksperymenty Termii). Rzeźnik Diakon to łagodny i jowialny kapitan Straszliwego Pająka, acz znienawidzony za przeszłość zdrajcy Orbitera oraz Noctis. TERAŹNIEJSZOŚĆ: Lewiatan zostaje zestrzelony torpedą anihilacyjną, dzięki Raoulowi ratujemy wielu ludzi na orbicie Lewiatana. Malictrix d'Pandora zostaje zniszczona przez Lewiatana, ale Klaudia ma jego mikrokopię dzięki Ariannie.

## Progresja

* Aleksandra Termia: jej reputacja dostaje STRASZNY cios przez Klaudię i Marię. Robi zakazane eksperymenty na ludziach i biosyntach. Wyszło na jaw i jest głośne.
* Ola Szerszeń: wyszło na jaw, że jest jakąś formą biosynta, eksperymentu admirał Termii. Jej reputacja (która i tak nie istniała) w strzępach. Potencjalnie - nie ma praw.
* Klaudia Stryk: pozyskała bardzo specyficzną cyberformę; "zwierzątko domowe" z Lewiatana z okolic Seibert.

### Frakcji

* .

## Zasługi

* Arianna Verlen: gdy Ola Szerszeń zaczęła interesować się biovatem Eleny, dała Eustachemu i Klaudii dowodzenie nad Olą by ta nie szalała. Podczas starcia z Lewiatanem używając swojej mocy skutecznie ożywiła kawałek Lewiatana i go "oswoiła", po czym stworzony byt przekazała Klaudii w prezencie. 
* Eustachy Korkoran: kapitan Inferni, przejął dowodzenie nad Olą Szerszeń i postrzelał torpedami anihilacyjnymi do Lewiatana.
* Klaudia Stryk: poznała sekret Rzeźnika Parszywca Diakona i rozpoznała co Ola Szerszeń robi z Regeneratorem Eleny. By dojść do tego o co chodzi Oli poprosiła o pomoc Marię Naavas co skończyło się katastrofą dla Termii. Przeanalizowała Lewiatana i doszła do tego jak ów działa.
* Maria Naavas: poproszona przez Klaudię dodała info o Oli Szerszeń do swojej listy szczurzenia po adm. Termii. Doszła do tego - niestety, poszła za ostro i skutecznie zniszczyła kilka serc i uderzyła w plany i reputację Termii. Maria ma problem, ale Termia też.
* Raoul Lavanis: wszedł na pokład jednostek tworzących Lewiatana i uratował tyle osób ile się dało; świetny advancer. Spacer kosmiczny, uruchamianie kapsuł ratunkowych itp.
* Ola Szerszeń: analizowała Regenerator Eleny, próbując zadbać o jej komfort nawet jeśli Elena nic nie czuje / nie wie. Złośliwa, napadła słownie Eustachego i channeluje to jako słodka korpoentuzjastyczna idiotka. Ma autorepair system; nie pozwala nikomu się zbadać ani zbliżyć do siebie. Dowodzi Wścibskim Wiewiórem.
* Rzeźnik Parszywiec Diakon: PRZESZŁOŚĆ: kiedyś mag, uznawany za potwora. Zdradził swoich ludzi z Orbitera, potem zdradził noktian i ich zniszczył syntetyzując w swoim ciele plagę. Ekspert od broni biologicznej, 69-letni kapitan Straszliwego Pająka. Noktianie do dziś go nienawidzą. Jowialny i sympatyczny z natury, lojalny Ariannie.
* OO Tivr: odwraca uwagę Lewiatana od Inferni przykładającej się do ataku torpedą anihilacyjną do Lewiatana.
* OO Infernia: wystrzeliła torpedę anihilacyjną w Lewiatana. Skutecznie. Eustachy jest dowódcą Inferni do końca świata.
* OO Straszliwy Pająk: dowódca - Rzeźnik Parszywiec Diakon.
* OO Pandora: KIA. Malictrix bardzo próbowała pokonać Lewiatana i się weń zapatrzyła. Kupiła czas Eustachemu by ten strzelił torpedą anihilacyjną, ale Pandora została zniszczona.

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Obłok Lirański
                1. Anomalia Kolapsu, orbita

## Czas

* Opóźnienie: 1
* Dni: 3

## Konflikty

* 1 - Klaudia -> Maria Naavas. Czy może się coś dowiedzieć na temat Oli Szerszeń. Czemu zapieczętowane dane. Maria z optymizmem rusza do działania.
    * TrZ+3+5Or
    * XVOrOrOr: Maria zostawia pożogę i zniszczenie, zniszczyła reputację Termii i Szerszeń oraz Żelazko jest wygnane XD.
* 2 - Klaudia analizuje Lewiatana. Pobieżne informacje. Jak z tym interaktować sensownie.
    * TrZ (mały lewiatan) +3
    * XV: trzeba Lewiatana opanować, ale wiadomo co to - digitalizator tworzący własny virt i mutujący TAI.
* 3 - Arianna wchodzi z Lewiatanem w interwencję magiczną. Chce pobudzić zainteresowanie i chęć zdobywania informacji.
    * TrZM+3Ob+4
    * OmVVVz: na jego hardware weszła Persi z Tivr, ale jest pod kontrolą, lekko spanikowany ale poczciwe zwierzątko dla Klaudii XD.
* 4 - Raoul dostaje się na pokład statku #1, potem #2, potem #3... ratuje kogo się da i jak się da
    * TrZ (Tivr odwracający uwagę) +2:
    * XVVzXVVz: Raoul dał radę dostać się na te statki i ratować kogo się da + coup de grace kogo się nie da. Ewakuował się i kto mógł - w pintkach / kapsułach.
* 5 - Eustachy wykorzystuje torpedę anihilacyjną, podlatuje w odpowiedni sposób do Lewiatana i strzela by zniszczyć masę krytyczną materii, by rozproszyć bydlaka.
    * TrZ (Tivr odwracający uwagę) +3 +3Or
    * XXXX: kosztem Pandory i uszkodzenia floty Arianny, ale się udało. Malictrix KIA.


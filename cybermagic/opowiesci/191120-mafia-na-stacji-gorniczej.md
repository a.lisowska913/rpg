---
layout: cybermagic-konspekt
title: "Mafia na stacji górniczej"
threads: unknown
gm: żółw
players: walec, onyks
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [191120 - Mafia na stacji górniczej](191120-mafia-na-stacji-gorniczej)

### Chronologiczna

* [191120 - Mafia na stacji górniczej](191120-mafia-na-stacji-gorniczej)

## Punkt zerowy

Kilkutysięcznoosobowa, ciężko uzbrojona stacja górnicza w pasie asteroidów nie jest może najlepszym miejscem do życia, ale to Wasz dom. Wy jesteście członkami grupy przestępczej i od dawna kontrolujecie to miejsce. Ludzie się Was boją – i wykonują Wasze polecenia. To nie takie złe miejsce.

Stefan wisi Wam pieniądze. Pozwoliliście mu odlecieć na inną stację, by sprzedał towar i mógł spłacić długi. Zostawiliście sobie jego rodzinę jako zakładników. Tym dziwniejszym jest to, że Stefan wrócił i… nic. Łazi po stacji, nie interesuje się ich losem. A Stefan zawsze był rodzinnym człowiekiem.

### Postacie

* Inżynier zniszczenia
* As Pilotażu: Oliwier --> Walec
* Szaman: Szymon
* Majster Stacji
* Badacz Otchłani --> Onyks
* Biostymulator

## Misja właściwa

### Stacja Dorszant

This space station does not orbit any planet. It is a military station station next to an asteroid belt and an active wormhole. Wormhole allows pilots to pass to the space controlled by orbiter; this is their main trading partner. This station has the main economy in mining – the close presence of a powerful magical anomaly corrupts the reality around them and makes the minerals and asteroids of higher value than usual. Some properties of those materials are unexpected and can bring a nice benefits.

The problem of the space station is also the same anomaly. There is no close star nearby, therefore the anomaly is the only energy source for entities inside this asteroid belt. This station has an ongoing problem with resources, mostly food water and energy.

A specific side effect of the anomaly is the fact, that all types of food quickly becomes a canned fish. No idea why. It just happens. It is not a good type of fish with tasty source. It is the cheapest, blandest type of slightly slimy fish in a can. This caused a lot of problems with vitamins, nutrition, and health on the station.

A previous leader of this station has tried to play it safe – do not interact with the anomaly too much and simply accept the fishy  situation as a fact of life. However, he got overthrown by a group of Magi, the mafia, who combined their abilities to create protection screens from the magic of the anomaly and ensure other types of food may appear on the station.

A leading artificial intelligence of the station, Eszara, told them they are endangering this station because an anomaly will eventually go haywire because of what they are doing. In about 200 years. People on this station do not give a meow. Therefore, Eszara deactivated herself not willing to kill most population of this station. Mafia brought back a weaker, less powerful AI of class Hestia. Hestia doesn’t want to help, but will help because humans are important.

Also, the trading partner – orbiter – wants to take over the station. But they can’t do it by force because this station is very well protected and well armed.

And this situation goes more or less like presented right now…

### Scena zero

Stefan has seen better days. He owes money to Mafia. Fortunately, he has managed to snag an Aether Map - one which should work but which leads towards a place Stefan has only heard about earlier. The place where this particular wormhole did not connect to yet.

This means Stefan can earn quite a lot from rare materials of this space station.

Stefan has entered the wormhole, but during transit the connection was rough; his spaceship got damaged. Doesn’t really matter; he is close to the target. He flew towards the closest human colony and he found it to have different electromagnetic spectrum than expected from human civilization. It looked more to be a robotic civilization.

Stefan has found Curators...

### Sesja właściwa

Eight days later, on the space station situation is strange. The mechanic has noticed that Stefan is on the station but has not come to meet his family, neither he came to pay back money. Something is wrong. He started monitoring Stefan to see what he is doing – who knows, maybe he has managed to contract some kind of disease.

Unfortunately, it was not a disease. The mechanic has noticed that Stefan is walking around chatting with people and lifting their spirits up. But one of the earliest recordings shown that Stefan left his finger inside the communications system; that finger is still there. This is not a specific action a human can do. Artificial intelligence of the station, Hestia, did not notice; she is too weak to oversee everything.

The mechanic has done the best thing he could – using the opportunity that Stefan is moving in the corridors like any normal person, he shut down the hatches and informed the rest of the mafia that everything is really not okay.

An ace entered the equation. They need to take some biological samples from Stefan – an ace went there, inside hatches, and took a sample. Stefan was completely saline, calm, glad he could help. He was way too optimistic and way too positive about all that. That prompted them that something is really not okay.

A scientist combined with a medic started working on the sample. They have destroyed Stefan, but they have managed to extract the core information – Stefan is a biorobot. Curators have come to the system. Full-scale curator invasion is imminent – they are going to assimilate people and put them into Alexandria. And there’s not much they can do to help it.

An engineer decided just have to go like this. He decided to burn a lot of resources of the station and use an anomaly to create a weapon against curators. He used their biorobot and corrupted to create an anti-curator weapon. One which is a Trojan horse. One which inserted into the mothership of the curators will destroy them. But curators will detect the robot and they will know it isan danger.

Meanwhile, the shaman decided that they will deal with very difficult situation with curators and foragers and therefore they need to get some aid. He asked orbiter for some resources stating they are under attack by the curators. Orbiter asked if they wanted military assistance, hoping to take over the station. But the shaman explained this is not an option – you just need some aid to be able to put morale high. They can deal with curators themselves.

And the mothership – an Ark – of the curators appeared in the system. The mothership started broadcasting on all channels that they bring medical supplies, free medical assistance for 100 first people who enter their mothership and also they want to trade. The mechanic has stopped the signal with Hestia’s help, an engineer has threatened people on the station using the gun that they cannot leave the station, but the curators have managed to pull all the people in the asteroid belt. 47 lives entered the mothership to be assimilated into Alexandria.

An ace found an opportunity to introduce the biorobot weapon onto an Ark. He flew there to meet with others and use them as a shield to enter curators mothership. Unfortunately, his ship was simply not strong enough, and curators defenses simply overwhelmed his machine. In desperation, not wanting to return and endangered the whole mission, an ace broadcasted to the curators that they need to take him on board or he will die and many people with him.

Curators obliged, sealing their fate. They could not surpass their own programming.

However, an ace was lost in the space, on the curator mothership. This in turn endangered hold space station. Remaining Mayfair decided to use an anomaly in extremely risky catalytic strategic spell ritual. To pull him back. An engineer used the main battery on the battle station to damage mothership’s hull and all the magi used all their power, propelled by the anomaly, to teleport ace on to the station. They succeeded – but an ace is really wounded.

The medic was able to save an ace. Curators managed to escape through the wormhole and send the distress call to other shard of curators before the buyer weapon destroyed this particular strand.

There was a lot of grieving on the space station – all those people who were lost and all those people with families who will never find their way back.

**Sprawdzenie Torów** ():

* .

**Epilog**:

* .

## Streszczenie

Handlarz Stefan użył Mapy Eterycznej by znaleźć lepszy rynek na towary ze Stacji Dorszant. Niestety, wpakował się na Kuratorów i dał im drogę na Stację. Gdy Kuratorzy przybyli, mafia kontrolująca Stację Dorszant zbudowała anty-kuratorskiego biorobota używając mocy anomalii i wsadzili go na pokład Arki Kuratorów. Kuratorzy się wycofali i zostali zniszczeni. Ich Aleksandrię przejął inny szczep. Gorzej, że zanim odlecieli, zabrali ze sobą 47 osób ze Stacji Dorszant i okolic. Ale - sama Stacja jest bezpieczna.

## Progresja

* .

### Frakcji

* .

## Zasługi

* Rafał Kirlat: Inżynier Zniszczenia. Z pomocą Kamelii zmienił biorobota w młot na kuratorów. Ogólnie, twardy i nie dający żadnych szans przetrwać czemukolwiek zagrażającemu Stacji.
* Kamelia Termit: Biostymulator. Pomogła Rafałowi zmienić biorobota w młot na kuratorów. Potem pozszywała Oliwiera po jego "powrocie" ze statku Kuratorów. Wykryła obecność Kuratorów z Gerwazym.
* Oliwier Pszteng: As Pilotażu. Pobrał od Stefana próbkę. Nieustraszony, czasem szaleńczy. Wprowadził biorobota na Arkę Kuratorów - co prawie przypłacił życiem. Musiał być ewakuowany czarem strategicznym.
* Szymon Szelmer: Szaman. Mistrz negocjacji z Orbiterem oraz osoba kalibrująca się z anomalią by móc wyciągnąć Oliwiera i by nic nie wyleciało w powietrze.
* Gerwazy Kruczkut: Badacz Otchłani. Doszedł do tego z czym mają do czynienia - z biorobotem Kuratorów. Skonfliktowany czy pozwolić Kuratorom wygrać czy ich zniszczyć. Skupił się na ratowaniu Oliwiera.
* Jaromir Uczkram: Majster Stacji. Monitorował Stefana, po czym pomógł dopakować biorobota i usprawnił myśliwiec Oliwiera.
* Stefan Ukrand: wisiał pieniądze mafii; poleciał przez Wormhole i znalazł Kuratorów w Sektorze Paradizo. Skończył w Aleksandrii, szczęśliwszy niż kiedykolwiek "za życia". Zostawił rodzinę na Stacji.
* Hestia d'Dorszant: Przebudzona przez Zespół, by wspierać Stację najlepiej jak się da. Naukowo-administracyjna, o pierwszym poziomie topozoficznym. Pomaga w zagłuszaniu sygnału Kuratorów.
* Eszara d'Dorszant: TAI zarządcza stacji o trzecim poziomie topozoficznym. Wyłączyła się gdy Zespół przejął władzę. Od tej pory śpi, nieaktywna.
* Janet Erwon: oficer Orbitera odpowiedzialna za kontakty handlowe z Dorszant. Pod namową Szymona, zlitowała się i przekazała pomoc humanitarną by pomóc odeprzeć Kuratorów.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Noviter: sektor, w którym źródła energii są magiczne i poukrywane są w Pasie Sowińskiego
            1. Pas Sowińskiego: pas asteroidów, który zawiera liczne Anomalie i źródła energii. Niebezpieczne i niefortunne miejsce z rzadkimi metalami.
                1. Anomalia Dorszant: kontrolowana przez mafię która przejęła Stację; główne źródło energii dla Stacji Dorszant.
                1. Stacja Dorszant: ma problemy z żywnością i kontroluje go mafia; handluje z Orbiterem i szczęśliwie odparła najazd Kuratorów usuwając ich połączenie na stałe
            1. Brama: dość trudny w użyciu portal reagujący na bardzo nietypowe mapy przez Anomalię Dorszant; im większy statek tym większa niestabilność
        1. Sektor Paradizo: sektor, gdzie pierwotnie znajdowała się cywilizacja która stworzyła oryginalnych Kuratorów
            1. Brama: stąd przyleciał Stefan i tam znajdują się co najmniej dwa szczepy Kuratorów

## Czas

* Chronologia: Aktualna chronologia
* Opóźnienie: 325
* Dni: 11

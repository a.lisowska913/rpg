---
layout: cybermagic-konspekt
title: "Dezinhibitor dla Sabiny"
threads: nemesis-pieknotki
gm: żółw
players: kić, vizz
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [200616 - Bardzo straszna mysz](200616-bardzo-straszna-mysz)

### Chronologiczna

* [200913 - Haracz w parku rozrywki](200913-haracz-w-parku-rozrywki)

## Punkt zerowy

### Dark Past

* Za niedługo Sabina Kazitan wraca do Aurum. Kilku Rekinów uznało, że to złe, że nie ucierpiała i niczego nie zapłaciła.
* Chcą, by Sabina wpadła w ich pułapkę zastawioną przy użyciu Myrczka jako przynęty.
* Pomogli Myrczkowi wymknąć się Teresie Mieralit
* Pomogli Myrczkowi złożyć dezinhibitor
* Dostarczyli Myrczkowi "syntetyczną ofiarę" - coś, co podobno nie jest prawdziwe. W tej roli jest ochotnik, prawdziwy mag Rekinów.
* Myrczek wyzna miłość Sabinie
* Liczą na to, że Sabina pod dezinhibitorem wpadnie w berserk i oni mogą wezwać terminusa i pokazać lololol.

### Opis sytuacji

.

### Postacie

* **Sabina Kazitan**: Księżniczka Zbrodni Lemurczaka, której wszystko uchodzi na sucho i nigdy nie zapłaciła za swoje zbrodnie.
    * 'Seilia', serratus-class guardian Sabiny.
* **Ignacy Myrczek**: zakochany nerd od grzybów, wesoły, acz nie ma śmiałości wobec kobiet.
    * **Lorena Gwozdnik**, Rekin. Ochotniczka by zostać potraktowaną środkiem Ignacego. Wie, że ten środek da jej odlot; sama ulegnie dezinhibicji (thanks, Karolina). UZNANIA!!!
* **Justynian Diakon**: Rekin, współpracujący często z Napoleonem Bankierzem, który stoi za planem odpłacenia Sabinie Kazitan. Zaślepiony wobec niej nienawiścią.
    * octovis-class, hover-drone, pojazd niezależny i samodzielny, zdolny do walki. Monitoruje co się tu dzieje: ani by nie wyszło spod kontroli i by mieć na Sabinę materiały.
    * **Amelia Katra**, uczennica terminusa; skupiona na magii ognia i dość radykalna. Uczciwa, bezpośrednia, prostolinijna. Przyjmuje dowody.
    * **Rafał Kumczek**, Rekin. Niezły katalista i dobry biomanta. Pomagał Myrczkowi złożyć nowe narkotyki. Umie trzymać język za zębami.

## Scena zerowa

Dla Vizza: powtórka sceny pokazującej relację Ignacego Myrczka i Sabiny Kazitan - pokazanie jak Sabina się Myrczkiem bawiła (acz żadna postać tam nie była, to virt-scena!):

* Wpadają i rozwiązują problem gimnazjum w chaosie grzybów.
* Tam Myrczek i Sabina. On wszystko robi, ona patrzy. She goads.
* On przesłuchany mówi swoją stronę.
* Ona przesłuchana nie chce nic powiedzieć - ale się wygada.

Pytania generatywne:

DLA KONTEKSTU: "Dwa miesiące później. Sabina tymczasowo zniknęła - coś knuje. Myrczek pragnie jej się oświadczyć i przygotował Coś Wielkiego."

* Q: Jeden z "arystokratycznej złotej młodzieży" dał znać Gabrielowi, że Sabina i Myrczek coś razem tego. Czemu to dla Gabriela ważne / problematyczne? Czemu to impuls?
* A: Ignacy jest jednym z głównych dostawców środków psychoaktywnych dla Rekinów; Kumczek powiedział, że Ignacy robi _inne rzeczy_ i nie ma czasu. Ważne, bo SABINA się kręci koło MYRCZKA. Szansa na zemstę.
* Q: Laura zauważyła, że Myrczek zrobił coś dużego i zniknął. Dowiedziała się, że to dla Sabiny. Czemu to dla niej ważne / problematyczne? Czemu to impuls?
* A: Laura jest za cieniaszczyckim stylem życia. Ignacy, sympatyczny, zapewnia ten styl.
* Q: Dlaczego Gabriel potrzebuje Laury? Czemu z nią współpracuje? (@kić)
* A: Laura jest psychotroniczką i umie w pojazdy; a jeśli spotkają się z serratusem, to Laura jest niezbędna.
* Q: Dlaczego Laura potrzebuje Gabriela? Czemu z nim współpracuje? (@vizz)
* A: Gabriel ZNA i ROZUMIE Sabinę. Laura może korzystać z jego wiedzy.
* Q: Dlaczego za żadne skarby nie mogą o tym wiedzieć wasi przełożeni ani "pełnoprawni" terminusi? Co może się złego stać? (@all)
* A: Jak będą terminusi, Sabina może uciąć łeb sprawie. Nie, trzeba ją przyskrzynić.

## Misja właściwa

Laura i Gabriel w Akademii Magii próbują dowiedzieć się, co tu się do cholery odmiauczało. Czyli Myrczek coś zrobił a Sabina na niego poluje. Laura nie jest tak sceptyczna wobec tien Kazitan; nie zna jej. Gabriel podejrzewa Sabinę o wszystko, co najgorsze.

Laura podbija od strony jej znajomości w Akademii Magii; zaczęła od Napoleona Bankierza (który atm się pręży na arenie i podziwiają go fanki). Napoleon potwierdził - Myrczek coś robił sporego; on nie do końca wie co. Ale pomagali Myrczkowi "jakiś arystokrata" i Karolina Erenit. Czyli potrzebne były specjalne, duże energie.

Na to Gabriel podskoczył do Aranei Diakon, przesympatycznej "zabójczyni" z arystokracji, która serio sporo wie (zbiera plotki). Aranea jest na widowni, podoba jej się oglądanie walk gladiatorów i walki Napoloeona Bankierza. Gabriel dopytał Araneę co ona wie na ten temat - Aranea powiedziała mu, że wie który to był arystokrata - Rafał Kumczek. Rafał Kumczek, "wingman" Myrczka. Namawia go, by nie rezygnował z miłości do Sabiny - Sabina odtaje! To Kumczek był architektem planu, by Ignacy próbował dalej. Gabriel jest lekko zdziwiony - przecież to Kumczek przyszedł do niego by ostrzec, że Ignacy i Sabina znowu coś jakoś kombinują i że Sabina poluje na Ignacego. Może coś zbroił i się zreflektował?

Gabriel i Laura odwiedzili Karolinę Erenit. Ta niechętnie ich przyjęła, ale jak Gabriel przekonał ją że są tam pomóc Myrczkowi to lekko odtajała. Wyjaśniła, jaki jest plan Myrczka:

* Sabina wyjeżdża do Aurum za trzy dni.
* Ignacy chce Sabinie wyznać miłość, ale Sabina go unika.
* IGNACY CHCE JĄ ŚCIĄGNĄĆ NA TORTURY! A dokładniej, będzie torturował seksbota! Pokaże, że miłością da się więcej niż torturami, zsyntetyzował eliksir miłości.
* Więc: Sabina wpada, widzi seksbota (bioforma) i widzi, że miłością można wyciągnąć więcej informacji.
* Myrczek do Sabiny: "Widzisz?".
* Sabina do Myrczka: "Widzę!" - i padają sobie w objęcia. Jej plany i tortury nie były potrzebne.

Ale Karolina widzi tam tragiczne luki w tym planie:

* SKĄD Myrczek weźmie seksbota? Serio? Kumczek ma jakiegoś? Nowy Rekin, jeździ z seksbotami?
* Ten środek co pomagała go budować... on wymaga visiat, wymaga maga do integracji. Czyli nie ma seksbota?
* Sam plan jest nierealistyczny. Ktoś z zewnątrz musi to koordynować.

Karolina compelowała Gabriela, by Myrczkowi nie stała się krzywda (jak się stanie, Karolina uważa go osobiście za winnego). Gabriel i Laura poprosili Karolinę, by pomogła im określić co to był za narkotyk jaki pomagała budować. Karolina zintegrowała się magicznie i mentalnie z nimi by móc wspólnie do tego dojść...

Przez MOMENT Gabriel widział Sabinę oczami Myrczka. "Don't go too fast, senpai-kun..." i grzyby oczami Myrczka "you're always on my mind". Nie było to miłe ani przyjemne; umysł Myrczka składa się z grzybów i poczciwości. Ale po chwili do tego doszły umysły: Kumczka i "seksbota", czyli Loreny. I Zespół dowiedział się, że:

* Ten narkotyk jest dezinhibitorem - osoby pod jego wpływem będą zachowywać się tak jakby nie miały żadnych zahamowań.
* Myrczek wierzy w swój plan. Jest prostolinijny. On NAPRAWDĘ wierzy.
* Planem Kumczka jest nagrać straszliwą Sabinę Kazitan i ją wpakować do celi za złe uczynki. Potem to rozpuścić w Aurum i zmiażdżyć jej reputację.
* Lorena NIE JEST seksbotem. Ale chce się strasznie wykazać. Ona będzie syntetyzować dezinhibitor w swoim ciele.
* Wszystko odbędzie się w Blokhausie Widmo, niedaleko Jastrząbca.

Gabriel jest skonfliktowany. Sabina Kazitan **jest** złą istotą. Nie powinno jej to wszystko po prostu ujść płazem. On nie jest tak przeciwny planom Rekinów. Ale na drodze stanęła mu Laura - to nie jest uczciwe, nie jest sprawiedliwe, nie jest dobre i nie jest go godne. Sabina może jest zła, ale tym razem jest najpewniej niewinna. Za to ktoś próbuje manipulować nimi - Laurą i Gabrielem. I to, te machinacje Rekinów muszą być powstrzymane.

Gabriel cholernie niechętnie, ale się zgodził. CHCIAŁBY by Sabina ucierpiała, ale nie może pozwolić ani na krzywdę Myrczka ani debila Kumczka ani idiotki Gwozdnik. A nie wiadomo czy uda się komukolwiek zatrzymać dezinhibitowaną Sabinę Kazitan z jej czarnymi rytuałami (o czym praktycznie nikt nie wie).

Dobra. Czyli pierwsza sprawa - trzeba upewnić się, że Sabina Kazitan NIE DOTRZE do Myrczka do Blokhausu. Gabriel korzystając z prawa parolu wysłał do Sabiny nakaz stawienia się w Podwiercie w konkretnym miejscu. Sabina odpowiedziała, że nie podlega jemu a Pięknotce. Gabriel skłamał, że to polecenie Pięknotki. Sabina jest w kropce. Wie, że Gabriel raczej nie kłamie, ale wie też, że NIE ZDĄŻY URATOWAĆ MYRCZKA jeśli stawi się w Podwiercie.

Sabina zrobiła bardzo ryzykowny ruch. Użyła krwawego rytuału na swojego serratusa, używając nań swojej krwi. Niech serratus wygląda i zachowuje się jak Sabina. Przekazała też serratusowi identyfikator/lokalizator który musi nosić (jest na parolu). Po czym wysłała serratusa do Podwiertu, sama pomknęła w kierunku na Blokhaus Widmo.

Tyle, że na miejscu w Podwiercie czekały drony Laury, mające Sabinę spowolnić i ją badać.

Laura, ku swemu OGROMNEMU zaskoczeniu wykryła, że to nie Sabina Kazitan. To jej serratus, utajony jako Sabina. Wykryła to jako wybitna psychotroniczka i wiedząc, czego szukać.

* X: Sabina może jednak się bronić - parol nie pochodził od Pięknotki a od Gabriela a Pięknotka compelowała ją do ochrony terenu.
* VV: Twardy dowód na złamanie parolu.
* XX: Sabina jest ostrzeżona i wie, że za tym stoi Gabriel.
* V: Cel Sabiny - OCHRONIĆ Myrczka, nie go tępić.

To ostatnie najbardziej zaskoczyło Laurę. Psychotroniczka która mogła pozyskać informację z TAI Elainka o motywach Sabiny tylko dzięki temu Sabinowemu rytuałowi. Czyli Sabina nie chce go tępić a chronić - i teraz jest święcie przekonana, że za całą intrygą stoi Gabriel XD. Oki, niech więc tak będzie.

Laura przekazała info Gabrielowi; mają mało czasu nim Sabina dotrze do Myrczka i wpadnie pod dezinhibitor. Laura użyła pełnej mocy silników ścigacza Gabriela. Zdążyła przed Sabiną, ale nie przed Myrczkiem (i dezinhibicją Loreny). Mają jednak moment, zanim Sabina napędzana magią z silnych uczuć tu dotrze.

Gabriel ma godny pomysł. Nie da się uratować już Loreny (chwilowo), ale da się zatrzymać dezinhibicję Sabiny. Wpadł w servarze w podziemia blokhausu, do Myrczka, porwał Lorenę krzycząc "ta jest moja!" i uciekł. Myrczek w totalnym szoku.

* XX: Sabina WIE że Gabriel tu był
* VVV: Sabina nie znajdzie

Lorena pod wpływem dezinhibitora i dziwnych reakcji spowodowanych przez interakcję tego wszystkiego z mocą ixiońską Karoliny Erenit szaleńczo zakochała się (w formie "oddanie") w Gabrielu. Chwilowo Gabriel nic z tym nie zrobi. 

A tymczasem Laura skupiła się na maszynach w blokhausie. Co tu się dzieje - wykryła potężną drone-carrier klasy Octovis (V) i przechwyciła wszelkie nagrania (VV) - ona ma wszystko co tu się dzieje, ale nie inni. Wykryła osobę odpowiedzialną - Justynian Diakon z Latających Rekinów (subfrakcja "Paladyni").

Wpada Sabina Kazitan, "szczęśliwa" jak cholera. Myrczek próbuje na dezinhibicji do niej że ją kocha, Sabina z szarży go policzkuje ostrymi pazurami. Myrczek płacze a Sabina go opieprza - NIGDY nie torturuj, nie wiesz po co to robisz, MIĘDZY NAMI NIC NIE BĘDZIE. Sabina jest wściekła, próbuje z nim zerwać i mówi mu, że zaraz wraca do domu i nigdy już jej nie zobaczy. Jeśli chce ją kochać - niech kocha platonicznie, na odległość.

Potem Sabina wychodzi od płaczącego Myrczka i nie wytrzymuje, wybucha. Krzyczy na Gabriela. Wie, że gdzieś tu jest. ROZERWIE go na plasterki. Gabriel, schowany z Loreną się uśmiecha triumfalnie. A potem zgłasza Pięknotce złamanie parolu przez Sabinę Kazitan :3.

Gabriel opieprza Rekiny Justyniana. A Laura wykorzystuje nagrania by jej "coś wisieli".

## Streszczenie

Zanim Sabina wróci do Aurum, Rekiny Paladyni chcieli ją przyskrzynić - podać jej dezinhibitor w towarzystwie Myrczka i to nagrać, z nadzieją, że się ją zamknie. Gabriel i Laura wykryli intrygę i zatrzymali ją, nie dopuszczając do dezinhibicji Sabiny. O dziwo, tien Kazitan chciała Myrczka CHRONIĆ.

## Progresja

* Ignacy Myrczek: dostawca środków psychoaktywnych do "Latających Rekinów" z Aurum. Lubiany przez Rekiny i studentów Akademii Magii.
* Laura Tesinik: znajomość i szacunek wśród uczniów Akademii Magii w Zaczęstwie. Jest "jedną z nich", ale fajniejsza (transfer student kiedyś).
* Laura Tesinik: ma materiały do szantażu i "wiszą jej" Rekiny z frakcji Justyniana Diakona.
* Lorena Gwozdnik: po wstrzyknięciu sobie dezinhibitora chciała PRZEJĄĆ SENTISIEĆ. Ale po spotkaniu z Gabrielem dezinhibitor przekierował na niego jej oddanie. Pirotess do Ashrama.
* Gabriel Ursus: Lorena Gwozdnik, Rekin, zakochała się w nim na zabój z uwagi na dezinhibitor. Gorzej niż miłość, to "oddanie".
* Sabina Kazitan: nienawiść wobec Gabriela Ursusa. On chce ją zniszczyć, uderzył też w Myrczka jako przynętę (jej zdaniem). Typowy arystokrata w skórze owcy; groźny i inteligentny.

### Frakcji

* .

## Zasługi

* Gabriel Ursus: skonfliktowany między zemstą na Sabinie Kazitan a zrobienie tego co należy. W końcu wybrał ochronę Myrczka i opieprzenie Rekinów nad zemstę nad Sabiną.
* Laura Tesinik: głos sumienia Gabriela jak chodzi o Sabinę Kazitan - tym razem jest niewinna. Wykryła skanując serratusa dronami, że Sabina złamała parol.
* Ignacy Myrczek: nie chce zrezygnować z miłości do Sabiny. Zachęcony przez Kumczka do strasznego planu - eliksir "tortur" na seksbocie (na serio: dezinhibitor na Lorenie XD). Sabina z nim "zerwała", wparowała tam, opieprzyła go i powiedziała by nie robił głupot.
* Sabina Kazitan: miała 3 dni do powrotu do Aurum. Ale wpakowana w intrygę przez Rekiny złamała parol by uratować Myrczka przed nim samym. Wparowała, opieprzyła Myrczka i z nim "zerwała". Wierzy, że za wszystkim stoi Gabriel i to była intryga co miała ją zatrzymać w Pustogorze.
* Rafał Kumczek: Rekin; główny wykonawca planu Justyniana ukarania Sabiny. Powiedział Gabrielowi o Sabinie i Ignacym, zachęcił Ignacego do idiotycznego planu i przeprowadził Ignacego przezeń.
* Aranea Diakon: często wpada do Akademii Magii, bo tu się czuje nieźle. Fajne źródło informacji dla Gabriela; wie naprawdę sporo i lubi testować umiejętności infiltracji.
* Napoleon Bankierz: traktuje Laurę i Gabriela z szacunkiem; są tam gdzie on chce być. Po ćwiczeniach na Arenie wskazał kto pomagał Myrczkowi - arystokrata i Karolina Erenit.
* Karolina Erenit: samotna, ciepła wobec Myrczka. Myrczek wymiauczał sobie jej pomoc przy dezinhibitorze. Wybitny taktyk, powiedziała Zespołowi czemu coś jej śmierdzi w planie Myrczka. Zintegrowana magią z Gabrielem, sama poczuła jak podły plan miał Kumczek i umysł Loreny Gwozdnik.
* Justynian Diakon: Rekin-paladyn; mastermind, który nie mógł pozwolić, by Sabina Kazitan nie odpowiadała za swoje zbrodnie. Entrapował ją i prawie doszło do tragedii. Gabriel go zatrzymał.
* Lorena Gwozdnik: Rekin; dramatycznie pragnie się wykazać przed Justynianem lub kimkolwiek. Wzięła na siebie rolę syntezatora dezinhibitora i udawała seksbota (ubranego) przed Myrczkiem.

### Frakcji

* Latające Rekiny - Paladyni: wykonali działania przeciwko Sabinie Kazitan chcąc ją skompromitować dezinhibitorem i Myrczkiem; dowodził nimi sam Justynian i wpadł w kłopot u Gabriela i Laury. Sami zapłacili za dezinhibitor i wpakowali Myrczka w kłopoty.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Podwiert
                            1. Zaczęstwo
                                1. Akademia Magii, kampus
                                    1. Arena Treningowa: Napoleon tam ciężko ćwiczy a dziewczyny i część Rekinów przychodzi podziwiać; najsolidniej ekranowane miejsce ever.
                        1. Powiat Jastrzębski
                            1. Jastrząbiec, okolice
                                1. Blokhaus Widmo: kiedyś miało to być osiedle przyszłości; zdewastowane, niezamieszkane ruiny "miasta"; tam jest Myrczek z ofiarą i Sabina.

## Czas

* Opóźnienie: -3
* Dni: 2

## Inne

Opracowanie sesji:

Główna linia:

* "Przeciwnikiem" jest Sabina Kazitan. "Dark slave princess with nothing natural".
* Myrczek kocha Sabinę, Sabina "kocha" Myrczka więc go rani i odrzuca.
* Gabriel nienawidzi Sabiny Kazitan. Strasznie, strasznie skrzywdziła kiedyś jego siostrę - Lilię.
* Myrczek zrobi coś co wierzy że jest fajne dla Sabiny, co jednak ją skompromituje - to jest plan Rekinów.

Linia boczna:

* Zemsta grupy Rekinów. WSZYSCY nienawidzą Księżniczki Lemurczaka i WSZYSCY chcą ją skrzywdzić w odwecie.
* Jedna rzecz to dzika zabawa, druga rzecz to potworne działania tien Kazitan.
* Trzeba pokazać jej prawdziwą naturę, prawdziwy terminus się tym zajmie i naprawi sytuację.

Czemu Gabriel:

* Rekin powie mu o problemie - Sabina Kazitan znowu coś złego robi. Ale jego przełożona nie wierzy, że Sabina jest tak okrutna.

Czemu Laura:

* Laura chce chronić Ignacego Myrczka. Nie to, że jakoś szczególnie jej na nim zależy, ale Ignacy jest więcej warty niż na to wygląda.

Godne sceny:

* Gdzie jest Ignacy? Co zaplanował -> znaleźć. Lub: gdzie jest Sabina? (Jastrząbiec) |=> przerobić na "zamknięte -> otwarte".
    * kto widział Ignacego? Kto mu pomagał? Rafał Kumczek + Karolina Erenit.
        * Karolina wyczuwa coś bardzo dziwnego w tym co robił Ignacy; Rafał coś mu dał do pomocy. Ignacemu bardzo zależało by zaimponować swojej miłości.
        * Rafał nie chce się przyznać, że Myrczek tu jest "ofiarą". Przyciśnięty, wyjaśni o co chodzi z Sabiną - niech pokaże swoją naturę.
    * śladem Sabiny Kazitan: w ramach parolu ma obowiązek mówić gdzie będzie. Jedzie do Jastrząbca.
* Sabina i Ignacy przy dezinhibitorze, sam na sam z "ofiarą".


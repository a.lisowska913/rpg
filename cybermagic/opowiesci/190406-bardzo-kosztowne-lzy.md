---
layout: cybermagic-konspekt
title: "Bardzo kosztowne łzy"
threads: nemesis-pieknotki
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [190402 - Eksperymentalny power suit](190402-eksperymentalny-power-suit)

### Chronologiczna

* [190402 - Eksperymentalny power suit](190402-eksperymentalny-power-suit)

## Budowa sesji

Pytania:

1. Czy Wiktor zarazi Proszek (4)
2. Czy Wiktor uratuje wiły (4)
3. Czy Sensus zniszczy wiły (4)
4. Zdrowie Pięknotki (3)
5. Korupcja Idy (4)

Wizja:

* Sensus wykorzystuje wiły do tworzenia Imperialnego Proszku - nieprawdopodobnej mocy neuroimpresjatora
* Wiktor infiltruje teren by doprowadzić do destrukcji Imperialnego Proszku - by zadziałał zupełnie inaczej
* Wiktor corruptuje Idę.

Sceny:

* Scena 1: Pięknotka i 2-3 terminusów lokalnych w Sensoplex; ślady Wiktora
* Scena 2?: Atak Wiktora Sataraila używającego własnego Imperialnego Proszku
* Scena 3?: Skażenie wił przez Wiktora podczas wojny z Bagnem

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Misja właściwa

**Scena 1: Atak na Sensoplex**

Pięknotka nocą stoi przed Sensoplex. Dostała distress signal ze środka. Ale strażnicy mówią, że nie problemu, sprawa jest opanowana. Pięknotka weszła "siły obrony" + "terminus" + "distress" od Idy. Pięknotka chce jednak porozmawiać z Idą. Strażnicy wzruszyli ramionami. Wpuścili terminuskę.

Pięknotka może nie jest ekspertem od zabezpieczeń mechanicznych; ale kamery się nie ruszają. Wyglądają na wyłączone. Pięknotka w trybie suspicious zdecydowała się znaleźć powody dla których distress call został nadany. (Tp:8,3,5=S). Znalazła coś co ją zaniepokoiło. Ślad xirathira. Oraz echo wskazujące na Wiktora Sataraila. Po śladach, Wiktor próbował gdzieś dostać się do jednego z budynków; być może mu się udało. Ida znalazła Pięknotkę.

Ida zachowuje się pozornie naturalnie; jednak Pięknotka (Tp:9,3,4=S). Pięknotka widzi, że ta jest wyraźnie pod wpływem jakichś środków chemicznych. Eks-terminuska, zwłaszcza tak wysoka w hierarchii Sensus, nie powinna być pod tego typu wpływem. Pięknotka od razu podejrzewa Sataraila. (Korupcja: +1). Pięknotka widzi, że Ida ma osłabioną wolę. "Wszystko jest w porządku, nic się nie dzieje" - poza tym, że najpewniej na terenie tego kompleksu jest gdzieś Wiktor Satarail. A to jest problem.

Pięknotka wie, że w tym kompleksie produkowane są neurotoksyny, sprzedawane m.in. Orbiterowi czy Luxuritias. Magazyny dlatego są szczególnie chronione i to nie bytami organicznymi. Pięknotka skorzystała z tego - przekonała Idę, by ta poszła z nią do lab i ją zbadała. Faktycznie, jest pod wpływem Imperialnego Proszku - środka który łączy przyjemność i spolegliwość. Ten Imperialny Proszek jest lekko zmodyfikowany; nie jest tak silny. (Tp:S) To dało Pięknotkce podstawę do włączenia alarmu. Przełączyła kompleks w tryb lockdown i przejęła dowodzenie.

Pięknotka próbuje Wiktora znaleźć środkami wewnętrznymi. Wysłała dispatch by naprawili monitoring i zabrała się do szukania swojego przeciwnika. Naprawiony monitoring wykazał, że część kamer jest dezaktywowana technomantycznie. A kilku strażników nie odpowiada. Jeden z nich jest technomantą. Wszystko koncentruje się dookoła Amfiteatru.

Z Pięknotką skontaktował się Adrian Wężoskór. Powiedział, że chce użyć robotów bojowych by rozwiązać problem. Pięknotka powiedziała, że ma lepszy pomysł - niech pozwoli jej wejść tam samej najpierw. Wężoskór po prostu się zgodził, nie kłócąc się szczególnie.

Pięknotka jest w Amfiteatrze. Weszła sama. Ostrożna. Od razu zauważyła, gdzie poszedł Wiktor - do pomieszczeń pod Amfiteatrem. Poszła za nim - ale lockdown odciął te miejsca. Wiktor może być tam uwięziony. Pięknotka chce wejść, ale Adrian ją przekonał by wpierw wzięła lokalny power suit. Kiepski i cywilny, ale zawsze jakaś ochrona.

Wpuścił ją i zamknął za nią wejście. Pięknotka trafiła do laboratorium.

Jak tylko Pięknotka tam weszła, od razu zaatakował ją Wiktor w formie królewskiego xirathira. (Tr:8,3,8=SS). Wiktor zaatakował. Pięknotka się spodziewała - i co z tego? To, co ma nie pozwala jej na sensowną walkę z niewidzialnym przeciwnikiem. Lekko ranna, straciła hełm - po czym ją rozpoznał i się cofnął. Uśmiechnął się sardonicznie. Jej power suit jest nieaktywny. 

Wiktor powiedział - Proszek powstaje z łez wił. Bardzo torturowanych, z wypaczonych wzorem i pod wpływem skrajnych emocji. Wiły płaczą w piwnicy. Jego celem jest uratowanie wił przed tym losem. Pięknotka wynegocjowała z Adrianem, żeby ten pozwolił odejść Wiktorowi oraz wiłom, by nie doszło do strasznego rozlewu krwi i zniszczenia laboratorium. Adrian się zgodził - nie ma nic przeciw. W końcu, zawsze znajdzie nowe wiły.

Wiktor natomiast... on powiedział, że dla niego to nie jest koniec. Pokaże magom, że Proszek lepiej powstaje z łez magów. W końcu łatwiej zmienić matrycę. Pięknotka nie jest w stanie odwieść go od tego planu, ale będzie próbowała go zatrzymać.

Wiktor obarczył Pięknotkę odpowiedzialnością za bezpieczne przejście jego i wił na bagna. Pięknotka przekonała Adriana że naprawdę, NAPRAWDĘ... powinien uhonorować tą umowę...

Pięknotka ostrzegła Pustogor, że Adrian zantagonizował Wiktora Sataraila. Terminusi w Pustogorze zadali Pięknotce serię pytań: w jaki sposób, osobiście czy przez kogoś... by znaleźć sposób jak chronić i kogo chronić. Powiedziała, że chodzi o wiły i proszek. Pustogor zaczął szukać informacji na temat tego wszystkiego. Po kilku godzinach Pięknotka dostała odpowiedź - głównym architektem projektu powiązanym z wiłami jest Ida.

Ida dostanie ochronę. Sęk w tym, że Pięknotka wie - Wiktor jest jak bagno. Jest cierpliwy. Może czekać nawet rok - nie przeszkadza mu to. Może zmienić cel - Ida ma krewnych. Innymi słowy, sytuacja jest dość trudna jak nie beznadziejna. Czyli - Pięknotka musi zastawić pułapkę. Ale jak? I z Cieniem?

**Scena 2: Cena łez wił**

Pięknotka jest cieniem Idy. Tak, by ani Ida ani Wiktor jej nie zauważyli. Miasto nie jest terenem dla Wiktora a Pięknotka wie jak on działa; powinna być w stanie się ukryć przed tym przeciwnikiem (Tr:SS). Udało jej się zinterceptować - Wiktor przechwycił Idę w zaułku. Wstrzyknął jej coś. Pięknotka od razu zaatakowała - slide in, tackle, uciekać. Pięknotka biegnie w tłum (Tp:SS). Niestety, Ida jest po stronie Wiktora po tym co jej wstrzyknął. Pięknotka unieszkodliwiła Idę, Wiktor nie może jej porwać i wezwała wsparcie. Musi przetrwać.

Wiktor zrzucił ludzką skórę. Wystrzelił artefaktem powiązanym z kryształem Mausów. Wiązka skoncentrowanej energii przeżyć wił i Imperialnego Proszku. Wpływ tego na Pięknotkę był taki - stanęła. I nagle zaczęła skupiać się na zwalczaniu Cienia który chce się uruchomić. Ida dostała pełną wiązką. Pięknotka zwalczała Cienia... a Wiktor obsypał eks-terminuskę pająkami. Gdy tylko Pięknotka odzyskała kontrolę, Wiktor się na nią rzucił. I wtedy odpaliło Elemental Affinity Pięknotki. Anti-life, technomancja.

Wiktor szepnął Pięknotce "z każdą sekundą te pająki przebudowują jej matrycę". Pięknotka odpala Cień. (Tr:8,3,8=P,S). Pięknotce od razu popłynęła krew z oczu i uszu, ale odpaliła power suit. Cień rozerwał ten, w którym była Pięknotka i od razu zaatakował WIktora - berserkersko i bez cienia zahamowań. Pięknotka go nie kontroluje, ale wskazała mu wektor.

(TpZ:11,3,4=). Cień ciężko poranił Wiktora i zmusił go do wycofania się; Wiktor był zaskoczony energią ixiońską oraz ogólnym okrucieństwem power suita. Zwłaszcza jak ten zaczął pożerać palec Wiktora. Pięknotka zatrzymała Cień przed gonieniem xirathira; wróciła do Idy.

Ida jest nieprzytomna i coś się z nią dzieje. Pięknotka w trybie pilnym wzięła ją do Lucjusza Blakenbauera...

Tory:

1. Czy Wiktor zarazi Proszek (4): 2
2. Czy Wiktor uratuje wiły (4): TAK
3. Czy Sensus zniszczy wiły (4): NIE
4. Zdrowie Pięknotki (3): 1
5. Korupcja Idy (4): 2

Wpływ:

* Ż: .
* Zespół: .

**Sprawdzenie Torów** ():

2 Wpływu -> 50% +1 do toru. Tory:

* .

**Epilog**:

* Wiktor Satarail i Sensus mają wieczną wojnę. Polują na siebie.
* Idę udało się zregenerować w pełni; niestety, musiała opuścić ten teren. Wiktor by nie odpuścił.
* Pięknotka ma jeden ekstra dzień w szpitalu przez wpływ Cienia.
* Wiktorowi udało się zarazić kilka porcji Proszku. To sprawiło, że Sensus zniszczył cały batch który był w magazynach...

### Wpływ na świat

| Kto           | Wolnych | Sumarycznie |
|---------------|---------|-------------|
|               |         |             |

Czyli:

* (K): .

## Streszczenie

Wiktor Satarail pomógł wiłom z których łez produkowany był Imperialny Proszek przez Sensus. Pięknotka wynegocjowała jego bezpieczne przejście a on próbował zmienić matrycę czarodziejki dowodzącej kompleksem Sensus - by z jej łez osiągnąć lepszy Proszek. Pięknotka uratowała tą czarodziejkę.

## Progresja

* Wiktor Satarail: wieczna wojna przeciwko Sensus; zarówno on wobec nich jak i oni wobec niego.

### Frakcji

* .

## Zasługi

* Pięknotka Diakon: uratowała Sensus przed Wiktorem Satarailem, jednorazowo. Odpaliła Cień by odepchnąć Wiktora. Jeden dzień w szpitalu.
* Adrian Wężoskór: oficer Sensus; współpracował z Pięknotką by uratować kompleks Sensus w Podwiercie.
* Wiktor Satarail: uratował kilkanaście nieszczęsnych wił z fabryk Sensus. Ma krwawą wendettę z Sensus. Starł się z Pięknotką, potem z Cieniem. Liże rany na bagnie. Nauczył się groźnych technik od Saitaera.
* Ida Tiara: dowódca Sensus w placówce; eks-terminuska Pustogorska. Wpadła w łapki Wiktora Sataraila i uratowała ją Pięknotka.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Podwiert
                                1. Sensoplex: campus należący do Sensus; miejsce stosunkowo bezpieczne i nieźle chronione. W piwnicy są nieszczęsne wiły z których łez produkowany jest Proszek.

## Czas

* Opóźnienie: 4
* Dni: 5

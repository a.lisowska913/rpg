---
layout: cybermagic-konspekt
title: "Haracz w parku rozrywki"
threads: nemesis-pieknotki
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [200623 - Adaptacja Azalii](200623-adaptacja-azalii)

### Chronologiczna

* [200623 - Adaptacja Azalii](200623-adaptacja-azalii)

## Punkt zerowy

### Dark Past

* Krystyna Senonik była człowiekiem. Policjantka w Praszalku, dowodziła.
* Krystyna wykryła konspirację między działaniami Janora i Kamarona. Janor zagroził jej dziecku, bardzo poważnie i skutecznie.
* Krystyna zaczęła pozwalać na coraz więcej odnośnie nieetycznych działań Janora. He pushed her further and further.
* Krystyna odnalazła Kult Ośmiornicy. Kult dał jej opcję "nigdy nie będziesz się już wstydzić"
* Krystyna poszła w mrok. She corrupted her own child.
* Kaskadowo, Krystyna przejęła kontrolę nad parkiem i zaczęła gromadzić i tworzyć Kult na własną rękę.
* GDYBY NIE TO, że Hestia zgłosiła obecność Grzymościowca (gdy ten uciekał przed Korupcją), to nic by nie było wiadomo.

### Opis sytuacji

* Wszystko by zadziałało, gdyby nie to, że Pięknotka wyczuwa kralotyczne wpływy (biostruktura) i może je odeprzeć (bioformacja)
* Mamy więc miasteczko, którego niemała populacja należy do Kultu Ośmiornicy. Mamy więc inną aurę Pryzmatyczną.
* Nie ma tu wielu magów; wiem tylko o Grzymościowcu, na którego wszystko będzie zrzucone jak się uda.
* Jak trzeba, Kult poświęci nawet Krystynę a na pewno Adama.
* Są tu ludzie, którzy podejrzewają, że coś się bardzo złego dzieje. Ogólny poziom generuje efemerydy o smaku desperacji i radości.
* PLAN 1: "tu nic się nie dzieje, Grzymościowiec został odegnany". Do tego celu faktycznie wsadzi się tu 2-3 ludzi którym za to zapłacą.
* PLAN 2: jak się nie uda terminusów odgonić, trzeba Grzymościowca poświęcić.

### Dark Future

* Park Rozrywki Janora stanie się miejscem korumpującym długoterminowo.
* Grzymościowiec zostanie zabity (gdy "porwie" Maję, by ją ratować).
* Docelowo się tu pojawi kraloth.

### Potential Dark Win

* Kult jako taki nie będzie usunięty; zostaną jakieś offshooty które przejmą kontrolę.
* Krystyna (kralomut) przetrwa; Adam i Maja jako kralotyczne ofiary - dostaną pomoc. Wektorem kralotycznym jest podobno Rafał (Grzymościowiec)
* Patrząc jak działa Karol, najpewniej Krystyna i Karol też upadną.
* Docelowo się tu pojawi kraloth.

### Istotne Postacie

* Hestia d'Janor: TAI parku rozrywkowego. Zaniepokojona firefightem z GRZYMOŚCIOWCAMI - a dokładniej, Kamaronem.
* Adam Janor: właściciel parku rozrywkowego. Ustawiony jako mastermind przez Krystynę. Bardzo wpływowy człowiek.
* Krystyna Senonik: "nothing but my hate and carousel of agony". Janor zniszczył jej wszystko co miała, zmusił do mieszkania z nim. She found the Tentacle.
* Karol Senonik: dziecko Krystyny; 17 lat. "Każda laska jego". Kralotycznie urzekający.
* Maja Janor: ustawiona jako zabawka Karola przez Krystynę i samego Karola.
* Rafał Kamaron: agent Grzymościa (Wolnego Uśmiechu); skorumpowany przez Kult. Mag, potrafi walczyć. Próbował zdobyć swój haracz, został pożarty przez Krystynę.

## Misja właściwa

Pięknotka dostała informacje od Trzewnia. TAI Hestia d'Janor zgłosiła firefight z kimś od Grzymościa. Ktoś się strzelał. Poprosiła o sprawdzenie przez terminusa. Hestia podejrzewa obecność magii - dlatego poprosiła o terminusa. Policja, zdaniem Hestii, może rozwiązać problem ale Hestia jest odpowiedzialna przed wszystkimi ludźmi z parku rozrywki itp. Czyli użyła dodatkowych kanałów.

Trzewń jest zaskoczony - park rozrywki ma swoją własną Hestię. Nietypowe. Acz to nie jest typowy park rozrywki - P.R. Janor jest solidnym, fajnym parkiem. Ciekawe to też, że P.R. Janor nie jest w rękach maga - jest w rękach człowieka, Adama Janora. I Trzewń zaproponował Pięknotce wzięcie ze sobą Alana - przyda mu się coś lekkiego na początek.

Alan się zdziwił. Park rozrywki? Pięknotka powiedziała, że może Fulmen to za dużo - w końcu to był JEDEN koleś od Grzymościa. Alan się zastanowił. Dobrze, nie weźmie swojego Fulmena; zadowoli się lżejszym servarem - self-activated, klasa Lancer. Klasyczny przydział Pustogoru. Weźmie swoje bronie, oczywiście, po prostu zostawi je w awianie. Spytał też Pięknotkę, co myśli o tym, żeby zabrać ze sobą dwie młode damy - Melindę oraz Dianę? Boi się je zostawić same, ale nie chce brać ich na akcję. Z drugiej strony, park rozrywki...

Alan zaznaczył - NAJLEPSZY park rozrywki w Szczelińcu. Jest szansa, że się ucieszą. On ma pieniądze... a nie ma świętego spokoju.

Następnego dnia, zebrali się i polecieli awianem do Praszalka. Alan dał kredytki Chevaleresse i Melindzie - niech się bawią w parku rozrywki. Obie nie mają tamtego terenu opuszczać. A on i Pięknotka przejdą się do analizy tematu. Alan powiedział, że porozmawia z TAI - Pięknotka pójdzie porozmawiać na posterunek.

Pięknotka na komendzie. Komendant Krystyna Senonik. Zdziwiła się, że Pięknotka przybyła z uwagi na strzelaninę; nic o strzelaninie nie raportowała. Mówi, że sytuacja jest pod kontrolą - to były próby zastraszenia, rzeczy pod kontrolą. Pięknotka zauważyła - jest opcja pomocy im. Nie chce robić kłopotów, wygląda na rutynową misję. Ale skoro tu jest - i Pięknotka próbuje zrozumieć co się tu dzieje.

* V: Krystyna nie cieszy się z tego powodu. Chyba obecność Pięknotki jej przeszkadza. Tego Pięknotka się nie spodziewała.
* V: Ktoś INNY usłyszał rozmowę Krystyny z Pięknotką; porozmawia z nią potem.

Pięknotka nie widzi powodu eskalować wyżej. Ma jakieś wstępne informacje. Może nawet sama zrobi patrol, grunt, że podopieczne Alana się wyszaleją. Gdy Pięknotka się oddaliła trochę od komendy, podszedł do niej jeden facet który był na komendzie.

Przedstawił się jako Franciszek Owadowiec; jest współwłaścicielem fabryki schronień Defensor. I uważa panią komendant Krystynę Senonik za kobietę, która wszystko zrobi by klienci kręcili się koło parku rozrywki. Co gorsza, jej syn jest ZAKAŁĄ PŁCI NIEWIEŚCIEJ! (powinien powiedzieć "zgubą", ale cóż...). A ona go chroni! I w ogóle Franciszek twierdzi, że dzieje się tu dużo złych rzeczy. A jej syn jest winny BAŁAMUCENIA! I IMPREZOWANIA!

Adam Janor z parku rozrywki, on lobbował, by w pewnych godzinach nie mogła pracować firma Defensor. Bo jest głośno. I to niszczy jego biznes. A burmistrz to łyknął. A komendantka to uprawomacnia - pilnuje by to się nie działo.

Pięknotka WIE, że on coś ukrywa. Opowiedział Pięknotce DOKŁADNIE jak wyglądała strzelanina - jeden koleś. Jeden Grzymościowiec. W servarze. Wyjątkowo dobrze dał opis servara; to był servar klasy Lancer. To właśnie Pięknotce zupełnie nie pasuje do sytuacji - Grzymościowiec w Lancerze miałby haracz zbierać? I jeszcze - nie strzelał w powietrze - strzelał w kierunku na Park Rozrywki. I się od niego oddalał.

Pięknotka próbuje z niego wydusić jako "wyrozumiała policjantka", ale krzyżowy ogień pytań. Skąd ma tak dobry opis itp. Co ukrywa i czego nie mówi? (Tp+2)

* V: Owadowiec się przyznał. Ma nagranie. W odróżnieniu od Hestii, która ją tylko wykryła, on nagrał całość sceny. I ofc. potwierdziło się, że ma córkę.
* NoTest: Pięknotka dostała od niego nagranie. I może sama je przejrzeć. Albo dać Alanowi.
* V: Jak ma się do tego chłopak - Karol Senonik? On nigdy nie widział mafii w kontakcie z Karolem, ale Karol ma nadmiernie dużą zażyłość z córką Adama Janora - z Mają Janor. I zdaniem Franciszka to jest trochę creepy; Karol traktuje ją trochę... jak trofeum? A ojciec to widzi i ignoruje. A przecież jeszcze pół roku temu Adam Janor tępił komendantkę tak pokazując jej gdzie jej miejsce. A teraz jest zupełnie inaczej, współpracują ze sobą itp.
* V: Owadowiec się przyznał - gdy do niego przychodzi mafia to on płaci. Nie chcą szczególnie dużo. On myślał, że Pięknotka jest z mafii; dopiero potem zrozumiał, że jest terminuską.
* X: Niewłaściwe uszy wiedzą, że Owadowiec Pięknotce opowiadał o różnych rzeczach. Owadowiec ma kiepski OpSec...

Karol nie ma chyba żadnego powiązania z mafią - a przynajmniej, Owadowiec nic o tym nie wie. Pięknotka ma o nim jednoznaczne pojęcie - praworządny, solidny koleś. Niezbyt może rozsądny, ale coś tam jest. Pięknotka przesłała informacje do Alana.

Alan tymczasem zabrał się za przesłuchiwanie AI. Dla odmiany, wykazał się subtelnością - wykorzystał Chevaleresse by zestawiła połączenie hipernetowe i porozmawiał z Hestią przez multivirt, tak, że nikt nic nie wie. Jak tylko Pięknotka dała mu znać że ktoś może coś kręcić, Alan od razu wszedł w większą subtelność.

Alan chciał dowiedzieć się wersji TAI. Plus, czemu TAI uznała, że ma zgłosić się do terminusów. (Tp)

* X: Oczy na Chevaleresse i Melindę, bo są z Alanem.
* V: TAI nie widziała samej walki; wykryła ją na sensorach. Jej sensory potwierdzają, że nagranie jest autentyczne. TAI + sensory dają Alanowi ślady w realu.
* V: TAI zgłosiła się do terminusów, bo pojawił się Kod Alarmowy. Swego czasu jej właściciel - Adam - wbudował specjalne zabezpieczenie w TAI, coś, co przekracza jego możliwości wyłączenia. Na wypadek, gdyby podejrzewał maga. Więc szczególne anomalne sytuacje. Hestia widzi, że koleś, który pobierał haracz wiele razy ostrzeliwywał sam park rozrywki i się wycofywał z niego - to jest anomalne zachowanie. To jest Kod Alarmowy. Hestia nic nie wie - ale robi, co ma.
* V: TAI, przepytana przez Alana, zgłosiła, że ma większość czujników powyłączanych lub niesprawnych. Adam je wyłączył, bo nie były mu potrzebne. W ciągu ostatniego miesiąca Adam zaczął sukcesywnie wyłączać czujniki; nie tak, że aktywnie wyłącza, ale nie ma maintenancu, jak ktoś się potknie to nie naprawi itp. Hestia nie jest w stanie prawidłowo monitorować parku rozrywki, skupiła się więc na atrakcjach.

Chevaleresse i Melinda się świetnie bawią. Alan i Pięknotka się spotykają pracować na danych i wykresach.

Alan - magiczna analiza korelacji danych od Hestii i z nagrania. Chce zobaczyć wszystkie szczegóły.

* Vm: Alan odbudował całą scenę z nagrań i sygnałów. I odkrył coś zaskakującego - to wygląda, jakby Grzymościowiec w servarze się wycofywał i ostrzeliwywał. Przed czymś uciekał. Kontrolowane wycofywanie, nie paniczne. Co więcej, Alan wykrył po ruchach servara że najprawdopodobniej Grzymościowiec używał magii.
* V: Alan ma dokładne info o tym Lancerze; łącznie z numerem seryjnym itp. To wymagało jednak by poszli na tamto miejsce. Na bazie tego Trzewń dał info odnośnie tego kto to jest - Rafał Kamaron, mag Grzymościa.

Alan się zdziwił. Mag w Lancerze wycofujący się przed czymś, nieważne w jak kontrolowany sposób. Nieważne jak dobry jest Rafał Kamaron, ważne, że jego obecność oznacza pewne problemy. Alan chce wycofać dziewczyny. Pięknotka zauważyła, że to wzbudzi zainteresowanie Chevaleresse - na pewno zacznie sznupać. Pięknotka twierdzi, że raczej gość nie będzie atakować w środku dnia; co miałby zyskać? Pięknotka zaznaczyła, że jeśli Alan je wycofa teraz, to wzbudzi zainteresowanie wroga + samych dziewczyn.

* V: Alan dał się przekonać. Nie wycofa ich. Będzie się martwił, ale nie wycofa. A teraz - musi znaleźć cholernego Grzymościowca. Przesłuchać.

Alan i Pięknotka. Dwójka terminusów, nie będących mistrzami śledzenia i odnajdowania śladów. Ale Pięknotka się zna na śledzeniu i odnajdywaniu śladów trochę i mają całość symulacji Alana. Najpierw Pięknotka jednak próbuje dojść do tego, czy ktoś ich śledzi i monitoruje. Pięknotka WIE, że nie ma tu sporo magów, więc się stymulowała; chce mieć lepsze zmysły i percepcję.

* X: Fokus -> Chevaleresse, Melinda. Corruption lv1. Chcą tu wrócić.
* V: Pięknotka zobaczyła, co ich śledzi - park rozrywki ma Obiekty W Powietrzu - balony, drony itp. I jedna z tych dron wyraźnie monitoruje ruchy Pięknotki i Alana. Czyli jest drona, która ich śledzi.

"Czy naprawdę nigdy się nie nauczą, że pierwsza rzecz na jaką terminusi patrzą to drony?" - Alan

Pięknotka poprosiła Alana, by ten zdobył śledzącą ich dronę. (TpM):

* V: Alan skupił energię i czysty tractor beam; drona jest jego.
* Vm: Alan zdekombinował dronę i wszedł by w pełni zrozumieć z czym ma do czynienia. Drona jest powiązana z Hestią. Czyli Hestia dostała rozkaz śledzenia ich.
* V: Alan tak przekształcił dronę i sygnały, by Hestia nie miała pojęcia, że cokolwiek Zespół wie. Co więcej, Hestia widzi tylko to, co Alan chce.
* X: Przygotowanie zasadzki wieczornej przez "Grzymościowców" (Kultystów podkupujących Grzymościowców).

Czyli Hestia, która to zgłosiła, teraz ich monitoruje i pilnuje. 

Pięknotka odpaliła hipernet. Chce się połączyć z Rafałem Kamaronem. I od razu odbiła się od połączenia - hipernet to komunikacja wola-do-woli + systemy obronne. Kamaron ma nienaturalną sygnaturę hipernetową, jak na narkotykach bojowych. Pięknotka łapie tą sygnaturę i przerzuca ją Trzewniowi. (ExZ)

* V: Trzewń potwierdza jego stan - to NIE są narkotyki bojowe. Trzewń wykrywa dwie sygnatury na hipernecie; dla niego to coś jak opętanie. Trzewń właśnie upgradował poziom trudności misji i odblokował Wam wsparcie. Alan zażyczył sobie by mu Fulmena przywieźli.
* XXX: Trzewń go nie może znaleźć; a Rafał zbiera sobie skutecznie oddział przy pomocy swojego Pasożyta.

Alan ma to gdzieś. Alan chce skończyć dzień i zabrać dziewczyny. Jest wieczór. Dziewczyny są rozradowane - jak im się podobało, jak było super, jutro też chcą przyjechać... Alan na to NIE. One na to TAK. Alan na to NIE...

Pięknotka informuje Chevaleresse na hipernecie, że to WAŻNE, żeby następny dzień spędziły w domu. To pomoże Alanowi. Pięknotka Chevaleresse o to prosi... ale coś Pięknotkę zaniepokoiło. Dziewczyny są tyci ZBYT ożywione i ZBYT zainteresowane. Pięknotka zna troszkę Chevaleresse. To nie jest do końca to czego się spodziewa. Plus, Pięknotka jest wyczulona na agenty biologiczne. Wyczuwa obecność takiego agenta - jej natywna synteza i ciało. Dziewczyny są czymś dotknięte.

Pięknotka zdecydowała się użyć magii do zbadania dziewczyn. I zdobyć tego próbkę.

* XXX: Pięknotka wyczuła co to jest. To są środki, które faktycznie mają zachęcić do powrotu. Atraktor, nie uzależnienie. Skomplikowany byt. No jak Alan się o tym dowiedział... powiedział, że spali park rozrywki. Spali go do ziemi. Jest naprawdę, naprawdę zły. (A Chevaleresse jest zainteresowana Kultem).

Dziewczyny spędzą następny dzień w domu, co do tego nie ma żadnych wątpliwości. A Pięknotka podkłada obu trackery; póki są w tych ubraniach (do rana) póty Pięknotka wie gdzie są. Więc są nieporywalne. Alan SAMEMU odwozi awianem dziewczyny do domu. Pięknotka poczeka na Alana; potem się z Alanem przejdzie po terenie znaleźć gdzie on jest.

Wieczór. Alan ma Fulmena z powrotem.

Pięknotka się pozycjonowała i z Alanem przeszukują teren. Alan odpala detektory Fulmena i szuka; ma daleki zasięg wykrycia bo ma daleki zasięg działa.

* V: Alan wykrył przeciwników, zbliżających się w kierunku na park rozrywki.
* X: Przeciwnicy są dobrze pozycjonowani; trudno ich zestrzelić jednym strzałem. Nie ma "zaskoczenia".

Pięknotka odpala swojego Lancera, Alan przygotowuje Fulmena do trybu artyleryjskiego. Alan proponuje, że zestrzeli maga. Chce go bezpiecznie zdjąć. 8-9 osób, w tym 3 servary. Alan chce zdjąć maga. (ExZ)

* VV: Alan zestrzelił maga w Lancerze. Przeciwnik przeżył, są ślady itp. Wiązka apokalipsy Fulmena, której nikt się nigdy nie spodziewa.
* X: 2 inne osoby wymagają NATYCHMIASTOWEJ interwencji medycznej. Fulmen ich Dotknął.
* X: Alan dostanie potężny opierdol od Karli. Nie tym są terminusi, nie te rzeczy powinni robić. Fulmen na piechotę?!

Pięknotka NATYCHMIAST wezwała medyków. Tu będzie potrzebna pomoc, z miasta! A sama używa działa dalekosiężnego Lancera; z trującymi pociskami eksplozywnymi, by redukować siłę piechoty. (TrZ)

* V: Pięknotka rozerwała część piechoty wroga; gazy się porozpraszały i wzbudziły w nich strach
* V: To plus to co zrobił Alan złamało piechotę. They are broken.

Przeciwnikom zostały dwa Lancery. Nie są to ciężkie maszyny, są w szoku. Ale widzą Fulmena w oddali - próbują się wycofać jak najszybciej, ale oddając kierowane strzały w kierunku na Fulmen. Alan ma zdecydowaną przewagę w obronie. (TrZ+3)

* V: Alan spokojnie włączył kierunkowe pole siłowe i zbliża się jak najszybciej w ich stronę.

Pięknotka przygotowała ciężki pocisk w snajperce i strzeliła w jeden z stawów uciekającego Lancera. Ma snajperkę, przeciwnik ucieka po prostej... jej preferowane warunki. TrZ+2. 

* V: Pięknotka unieszkodliwiła servar. Nie ucieknie.
* VV: Pięknotka zdjęła też drugiego zanim Alan skończył manipulację Fulmenem; fakt, Fulmen jest niesamowicie potężny, ale wolny.
* XX: Też dramatyczna interwencja medyczna im potrzebna; Pięknotka się spieszyła przed Alanem, więc skrzywdziła mocniej niż chciała.

Alan zwrócił na to uwagę Pięknotce gdy dwóch lokalnych lekarzy zaczęło rozpaczliwie ratować tych ludzi. Alan zwrócił uwagę Pięknotce - ona jest lepszą AGENTKĄ. On jest lepszym NISZCZYCIELEM. On jest wojną. Tak czy inaczej - problem rozwiązany. Ale Alan pojawi się tu jutro. Musi skończyć to, co zaczął.

## Streszczenie

Łatwa misja w okolicach parku rozrywki Janor przekształciła się w dziwną operację z pasożytem magicznym dotykającym pobierającego haracz Grzymościowca i dziwnie zachowującą się komendant policji. Pięknotka i Alan rozwalili siły Grzymościa, ale nie są pewni, czy to nie była tylko dywersja... (a tymczasem Melinda i Chevaleresse świetnie się bawiły w parku rozrywki).

## Progresja

* Alan Bartozol: nowy aspekt - obsesyjnie chroni swoje podopieczne.
* Alan Bartozol: bardzo ostry opieprz od Karli; nie do tego służy Fulmen, nie powinien używać ciężkiego działa przeciw piechocie.
* Diana Tevalier: zainteresowana Kultem Ośmiornicy; poczuła jego potęgę. Ona chce rząd dusz i wspierać innych; ludzie Kultu to mają, czemu ona nie może?
* Melinda Teilert: zainteresowana Kultem Ośmiornicy; poczuła jego potęgę. Ona chce prawdziwej potęgi; ludzie Kultu ją mają, czemu ona nie może?

### Frakcji

* .

## Zasługi

* Pięknotka Diakon: zestrzeliwuje servary szybciej niż Alan Fulmenem, gada z ludźmi i zbiera informacje dla Alana, też znalazła dronę która ich śledziła.
* Mariusz Trzewń: znalazł dla Pięknotki łatwą misję, która okazała się być dość ostra; nie dał rady odnaleźć ludzi od Grzymościa, ale wykrył Pasożyta w magu.
* Alan Bartozol: anihilator Fulmenem; bezwzględnie zdjął maga. Badał dronę która śledziła. Dogadał się z TAI za plecami wszystkich. Bezwzględny i bardzo skuteczny.
* Diana Tevalier: świetnie się bawiła w parku rozrywki Janus; tam Dotknęła Kultu Ośmiornicy. Pomogła Alanowi w dyskretnym połączeniu z TAI Hestia d'Janus.
* Melinda Teilert: świetnie się bawiła w parku rozrywki Janus; tam Dotknęła Kultu Ośmiornicy. Niewiele więcej tam zrobiła.
* Adam Janor: właściciel parku rozrywki Janor; podobno bardzo zmienił nastawienie do komendant policji od 'tępienie' do współpracy.
* Maja Janor: nie pojawiła się, ale podobno ojciec (Adam) ignoruje jak Karol Senonik ją traktuje. Zakochana w Karolu na zabój.
* Krystyna Senonik: komendant policji Praszalka. Nie cieszy ją pomoc terminusów. Dość inteligentna.
* Karol Senonik: nie pojawił się; podobno ZAGŁADA PŁCI NIEWIEŚCIEJ! Podobno matka (Krystyna) pozwala mu na wszystko.
* Franciszek Owadowiec: lekkie objawy paranoi i rozgoryczenia, że jego córka i Karol Senonik cośtam. Dużo powiedział Pięknotce o terenie i sytuacji. Dyskretnie nagrywa.
* Rafał Kamaron: Grzymościowiec z Wolnego Uśmiechu; złapał jakiegoś dziwnego pasożyta próbując zdobyć haracz z parku rozrywki Janor. Zestrzelony przez Fulmen Alana.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Jastrzębski
                            1. Praszalek: niewielkie miasto, ok. 4k ludzi i 8 magów.
                                1. Komenda policji
                            1. Praszalek, okolice
                                1. Park rozrywki Janor: najlepszy w Szczelińcu; coś się dzieje tu nie tak ze środkami mającymi ludzi ściągnąć.
                                1. Fabryka schronień Defensor

## Czas

* Opóźnienie: 5
* Dni: 1

## Inne

### Budowa sesji

#### Co się działo

* Zaczęło się od kombinacji Shame + Resentment. Próba wyjścia z tego stanu emocjonalnego doprowadziła do pojawienia się Kultu Ośmiornicy.
* W chwili obecnej Kult Ośmiornicy ma już swoje macki w miasteczku.
* W pewien sposób wyzwalaczem jest rozprzestrzenienie idei poza miasteczko.
* Hestia d'Janor jest TAI systemu rozrywkowego; jej przełożony jest już Kultystą.

#### Dark Future

* Park Rozrywki Janor stanie się stabilnym corruptorem okolicy.

#### Pytania

1. .

#### Dominujące uczucia

* .

### Komentarz MG

* Pokażę kralomut - człowieka / maga przekształconego daleko przez kralothy.
* Pokażę Kult Ośmiornicy, czemu jest groźny i do czego prowadzi.
* Dodatkowo... Alan? Jasne :-).

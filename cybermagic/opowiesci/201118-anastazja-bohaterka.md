---
layout: cybermagic-konspekt
title: "Anastazja - bohaterką?"
threads: legenda-arianny
gm: żółw
players: kić, fox, kapsel
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [201117 - Morszczat zostaje piratem](201117-morszczat-zostaje-piratem)

### Chronologiczna

* [201104 - Sabotaż Świni](201104-sabotaz-swini)

## Punkt zerowy

### Dark Past

.

### Opis sytuacji

Nie da się zlokalizować miejsca w kosmosie, gdzie ma miejsce ta Opowieść. Stąd karta lokalizacji pozostaje niewypełniona.
Statek Koloidowy poluje na Ariannę. Nie wie, że Arianna dowodzi Infernią - i nie wie, że Infernia ma zaawansowane sensory.

### Postacie

.

## Punkt zero

.

## Misja właściwa

Izabeli udało się zaprojektować sposób, w który Anastazja zostanie bohaterką i wszyscy będą zadowoleni. 

1. Statek kupiecki Czerwony Romek. Przewozi TAI i holokostki. Ulega awarii.
2. Znajdujący się niedaleko "statek piracki" o nazwie Królowa Otchłani. Mikroprzedsiębiorca, może robić za pirata.
3. Infernia leci w kierunku pasa Omszawera, napotyka na Czerwonego Romka. Ten jest uszkodzony.
4. Infernia pomaga Romkowi. Pojawia się Królowa Otchłani.
5. Anastazja żąda pokoju; przekonuje kapitana Królowej, żeby ten dołączył do floty Aurum - dokładniej, do jej floty. Nie ma rozlewu krwi.

Nie jest to może NAJLEPSZY plan, ale wygląda sensownie - powinien zadziałać. Zabawy dodaje fakt, że Anastazja nic nie wie, trzeba będzie ją troszkę pokierować by podjęła właściwe decyzje i właściwie pomogła - ale od czego jest Zespół?

Jednak zanim może do czegoś dojść - Eustachy ma problem. A dokładniej, jest wyzwany na pojedynek przez Stefana Jamniczka, arystokratę Aurum który nie jest wojskowym, na Kontrolerze Pierwszym. Pojedynek o... Dianę. Okazało się, że:

* Jamniczek jest mistrzem władania bronią białą - a dokładnie, szpadą.
* Jamniczek opowiadał jakieś niestworzone historie próbując zaimponować dziewczynom w Jelonku; Diana wyśmiała mówiąc, że Eustachy jest lepszy.
* Diana ugodziła w męskość Stefana Jamniczka. Więc - pojedynek z Eustachym.

Diana rozmawiając z Eustachym od razu przeszła do trybu "oczywiście, SENSEI". Eustachy porozmawia ze Stefanem, trzeba coś z tym zrobić. Ale przedtem - "zlecam Dianie pompeczki czy coś równie głupiego". Zmęczona Diana to spacyfikowana Diana.

Eustachy porozmawiał z Jamniczkiem. Załagodzić? NIE! Trzeba gościa wyeskalować. SPONIEWIERAĆ! Eustachy chce tego pojedynku. Chce wygrać. Nie tylko dla Diany, przede wszystkim z uwagi na to, że może w ten sposób zdobyć szacun u arystokracji. A na pewno wśród niewiast.

* V: przewaga w pojedynku, wyprowadził Jamniczka z równowagi.
* V: szacunek u arystokracji. Plus, podniesione szanse w walce.
* X: poplecznicy Jamniczka MEGA ŹLI na Eustachego.
* X: dziewczyna Jamniczka <3 Eustachy. Jest tienką.
* V: patronat Anastazji nad pojedynkiem. Będzie to mega efektowne.

Tak więc Eustachy zmieszał Jamniczka z błotem i przygotowuje się do pojedynku szpadą. To, że nie umie w szpady niespecjalnie mu przeszkadza - wierzy, że Arianna i Klaudia coś wymyślą.

...i wymyśliły.

Zacznijmy od tego, że Eustachy dostanie wybuchową szablę - to sam może zrobić. Użył szabli Diany ("sensei, proszę, weź moją!"). Arianna, Klaudia i Martyn zaczęli też pracę nad specjalnym środkiem związanym z miłością - niech w efektowny sposób Jamniczek przegra i wtedy Anastazja go uratuje! Ale niestety (XXXXX) nie wyszło - powstała ANOMALIA MIŁOŚCI. Zaraźliwa. Tyle, że zespół jest na nią odporny...

Od Zygfryda Kondora, nie do końca legalnego Orbiterowca używającego dziwnych TAI Klaudia zajumała specjalną, lekko uszkodzoną Elainkę specjalizującą się w szpadach. Jedyna wada - jest to lekko masochistyczna Elainka, akceptująca ból swojego użytkownika. Trudno, Klaudii pasuje :D.

Czas na walkę na arenie.

Eustachy sprowokował Jamniczka, po czym go podciął i przewrócił. Elainka sprawiła, że Eustachemu trysnęła krew (dzięki...). I wtedy Eustachy wdeptał Jamniczka w ziemię. Gdy anomalia odpaliła... WSZYSCY chcieli kąsek Eustachego. Łącznie z Anastazją. Eustachy ucieka przez K1 VVO -> wysyłając część swoich miłośników do kapsuły. Arianna ewakuowała Infernię zanim do czegoś dojdzie, manewrując między statkami.

Wow.

Infernia też nie ma sterowności - miłość do Eustachego jest... mocna. Ale Kamil Lyraczek zadziałał. Zrobił przemowę (ExZ+3), przekierowując miłość wszystkich na Ariannę - jedyną słuszną boginię Inferni (Martyn pomógł z feromonami Arianny; nie pytajcie SKĄD on ma takie rzeczy).

Korzystając z nowo nabytego zaufania i miłości, Arianna używa pełni mocy arcymaga. Leczy Anastazję i Izabelę (ogólnie, magów na pokładzie) z miłości wobec Eustachego i siebie. (XX): stabilna anomalia: Infernia dorobiła się Upiora Kwiatu Wiśni, awatara miłości. (VV): pozbawiła ich manii miłości do Eustachego i siebie.

Dobrze. Sytuacja w miarę ustabilizowana. Czas zająć się zrobieniem z Anastazji bohaterki - choć po ostatnim będzie ciężko...

Podlecieli do Czerwonego Romka. Wszystko działa jak powinno, ale Klaudia wykrywa na skanerach (Hr VXX, Hr VX) dziwny cień. Coś anomalicznego jest niedaleko Czerwonego Romka. Dokładniej, po wejściu w głąb analityki - to statek koloidowy. Hunter-killer. Coś najpewniej poluje na Infernię. Ale czemu na Infernię, skąd wiedział że Infernia tu jest?

I przede wszystkim, co Infernia może z tym zrobić? Szczęście w nieszczęściu, że Infernia to QShip - nie wiadomo, że jest tym czym jest. Nie wiadomo że ma cięższe uzbrojenie niż się wydaje i cięższy pancerz.

Dobra - ale Infernia przez problem wszechogarniającej miłości nie nadaje się w tej chwili do walki z czymś takim jak nieznany koloidowy statek. Trzeba to cholerstwo odpędzić. Ale jak?

Eustachy zaproponował wykorzystać odpowiednik torped akustycznych. Torpeda energetyczna, odpowiednio zmodyfikowana, która sprawi, że przeciwnik straci namiar na Infernię. Nie było to łatwe (Ex), ale z pomocą Arianny udało mu się przygotować serię torped akustycznych. Rzeczy, którymi może wroga zniechęcić.

Zbudowali też z Klaudią sztuczną, fake'ową sygnaturę Inferni korzystając z możliwości QShipa - i rozprzestrzenili to do serii dron. Dzięki temu przeciwnik powinien zgłupieć i nie być w stanie trafić Inferni pierwszym strzałem. Niestety, Persefona jest niereplikowalna - ale dobre i to. Zabrało to większość części zamiennych Inferni - nie zostało Inferni dużo rzeczy do powrotu do Kontrolera Pierwszego.

Arianna też skontaktowała się z innym statkiem, który miał pełnić rolę piratów w planie Izy. Powiedziała, że cieszy się, że Termia przysłała ich, acz żeby za mocno nie strzelali. Innymi słowy, w głowie kapitana koloidowego statku pojawiła się myśl "może to nie jest najlepszy moment?"

**Udało się**. Jak jeszcze Eustachy zaczął strzelać ze wszystkich dział losowo, koloidowy statek się wycofał. Klaudia wystrzeliła wiązkę dalekosiężnego skanera z nadzieją, że obejmie statek koloidowy - udało jej się odkryć echo sygnatury koloidowego statku. To model klasy Terrozaur, należący do Fareil, z pobliskiej planety w sektorze Astoriańskim. Ale czemu? I czemu poluje na Infernię..?

Aha, Anastazja nie została bohaterką. Ale przetrwali.

## Streszczenie

Anastazja nie została bohaterką. Wpierw Eustachy wplątał się w pojedynek o Dianę z jakimś szlachcicem - skończyło się różową mgiełką miłości i pospieszną ewakuacją Inferni z Kontrolera Pierwszego, potem epicki plan Izy z piratami rozpadł się, bo na Infernię polował jakiś statek koloidowy. Ale przetrwali - i mają zamiar to wszystko rozplątać.

## Progresja

* Arianna Verlen: załoga Inferni jest w niej rozkochana. Nie tylko ją wielbią - też kochają. Dzięki, różowa mgiełko.
* Eustachy Korkoran: ma stały bonus +3 w kontaktach do wszystkich kobiet.
* Eustachy Korkoran: zakochuje się w nim Julia, ukochana Stefana Jamniczka. Ma też przechlapane u stronników Jamniczka.
* OO Infernia: dorobiła się Upiora Kwiatu Wiśni, awatara miłości działającego w najbardziej nieodpowiednim momencie. Isuzu of Love.
* OO Infernia: okazało się, że jej Persefona jest niereplikowalna; to jedna z "tych" podobno wadliwych modeli.

### Frakcji

* .

## Zasługi

* Arianna Verlen: chciała pomóc Eustachemu wygrać pojedynek nieuczciwie, skończyło się na Paradoksie Miłości i ewakuacji z Kontrolera... ale wyleczyła magów na Inferni wspierana Pryzmatem dzięki Kamilowi.
* Eustachy Korkoran: wpierw sponiewierał Jamniczka w walce na szpady z nieuczciwą pomocą, a potem rozkochał w sobie pół świata Paradoksem Arianny XD.
* Klaudia Stryk: w warunkach ekstremalnych jej paranoja pozwoliła na wykrycie statku koloidowego - coś, czego nikt inny by nie zrobił.
* Stefan Jamniczek: tien; oddał życie walce szpadą. Przegrał w walce szpadą z Eustachym bo tamten oszukiwał. Stracił dziewczynę XD.
* Diana Arłacz: doprowadziła do pojedynku między Eustachym a Jamniczkiem, bo nie umiała trzymać języka za zębami i "Eustachy jest najlepszy i męski".
* Martyn Hiwasser: współuczestniczył w Paradoksie Miłości Arianny, po czym od razu ogłosił kwarantannę na Inferni i uznał Infernię za Statek Plag.
* Kamil Lyraczek: potężnym głosem kaznodziei przesunął miłość wobec Eustachego wszystkich na Inferni w stronę Arianny - gdzie owej miłości miejsce.
* Anastazja Sowińska: miała tylko patronować pojedynkowi Eustachy - Jamniczek, ale się zakochała w Eustachym i skompromitowała publicznie na K1.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański

## Czas

* Opóźnienie: 6
* Dni: 3

## Inne

.
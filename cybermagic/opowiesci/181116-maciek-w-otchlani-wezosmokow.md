---
layout: cybermagic-konspekt
title:  "Maciek w Otchłani Wężosmoków"
threads: dzien-z-zycia-terminusa
gm: żółw
players: apelacja, xsazar, mactator, samael
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [180814 - Trufle z kosmosu](180814-trufle-z-kosmosu)

### Chronologiczna

* [180814 - Trufle z kosmosu](180814-trufle-z-kosmosu)

## Projektowanie sesji

### Struktura sesji: Wyścig

* **Strona, potrzeba, sukces, porażka, sposób, silnik, breakpointy**

### Hasła i uruchomienie

* Ż: (kontekst otoczenia) -
* Ż: (element niepasujący) -
* Ż: (przeszłość, kontekst) -

### Dark Future

1. ...

## Potencjalne pytania historyczne

* brak

## Punkt zerowy

### Postacie

### Opis sytuacji

"
Zaczęstwo. Niewielkie miasto słynące z przeładowanej dobrej klasy komputerami i cybernetyką szkoły.

W Zaczęstwie znajduje się klub / gildia / klan o pięknej nazwie "Zaczęstwiacy", którego nazwy obcokrajowcy wymówić nie umieją. Zaczęstwiacy grają w różne gry online, z powodzeniem. Tak się składa, że to Wasza ekipa, składająca się z ~20 osób o różnym wieku.

Podczas jednej z rozgrywek w grę online, Wasza grupa napotkała bardzo nieszczęśliwego wężosmoka (tzn. przeciwnika do zabicia) kulącego się pod ścianą. Smok poprosił, by go nie zabijać - jest graczem (a tak w ogóle to ma na imię Maciek) i coś go "wciągnęło" do komputera. Boi się, że jak go zabijecie, to on zginie naprawdę.

Maciek podał swój adres - mieszka stosunkowo niedaleko, w Przywiesławiu. Nie do końca wiadomo, co tu się dzieje - czy coś "wciąga" graczy w komputer, czy to sprawka jakiegoś hackera, który chce zmniejszyć Wasze złoto/godzinę? Słyszeliście, że czasem dziwne rzeczy się zdarzają, ale czemu zdarzyły się akurat teraz?

Dowiedzieliście się też, że inna gildia (o sympatycznej nazwie "Armadda") zaproponował wydarzenie w grze - zabić 1000 wężosmoków w ciągu 3 godzin (szef rozda za to nagrody). Szanse przetrwania Maćka właśnie spadły do... małej liczby. Oczywiście, chyba, że ktoś go uratuje.
"

## Misja właściwa

**Scena**: (nie aplikuje)

So, here we are. One snake Dragon huddling and covering next to the wall and a group of PvX players from the same guild - Zaczęstwiacy. They decided to ask some questions – how did the snake Dragon hack the game that he is able to play a snake Dragon. The snake Dragon started crying that he is not hiding anything that he got pulled into the computer and he doesn’t understand what is happening in his name is Michał.

In the meantime, a group of players from the Guild Armadda were coming. 10 of them – they wanted to hunt the dragons and they happened to stumble upon this one and the group. Zaczęstwiacy decided that it might be an event made by game master’s and that this particular Dragon needs saving. Until they learn more about it. They did the best thing usual player can do – attack other players.

The full DPS mage (commando player) affect the healers and must talk to them in a meteor shower. That was the beginning or an end for the Armadda players. The nerd player decided that the best way to save the snake Dragon would be to pound them – he used some memes, laughed at them in a life, said no one competent ever place in that guild – Zaczęstwiacy made it so Armadda was really keen to kill the players and every member of their guild.

Other than that, Players decided to promote Zaczęstwiacy at cost to Armadda. It succeeded.

So, Commando and Nerd decided to stay and protect the snake Dragon from other players and distract the Armadda players from killing them in the meantime. In the meantime, the Biker and Progamer decided to go to the place a snake Dragon gave them to learn more about the event made by the game masters.

Before that, they get some answers from the snake Dragon. His name was Michał (Maciek), lives in Żarnia, he just woke up and wanted to play the game before school. There was an event – if he logged on and killed some noobs in the newbie areas he would get extra loot and extra experience. When he accepted that quest he got pulled into the computer. And now he is a snake Dragon everyone wants to kill him.

So, Michał wanted to grief some helpless players and became a hunted monster. Such a shame. Biker and Progamer went to the house where Michał lived and encountered Michał’s brother. Michał’s brother’s name was Szczepan. Szczepan got flattered by the players and he allowed them to access Michał’s computer waiting for some coupons for extra loot.

At that point they noticed that they are able to move Michał’s character who actually is a snake Dragon. They decided to make a run for it – let’s evacuate a snake Dragon to Zaczęstwiacy’s Guildhall. Normally, it would be impossible – monsters cannot move through areas. But perhaps it is possible to navigate the snakedragon from this computer. Unknown to them, they interact with the medical field and because they believe it is possible it became possible.

There was one epic escape – they kind of ported the snake Dragon to the wizard (played by the Commando) while the Nerd was protecting the snake Dragon from other guilds. Kind of awesome – a magical success. But why did Michał get pulled into the computer in the first place?

The Biker started analyzing his computer. He got entangled in magical field. He had a vision. He so a set of children sitting in a circle crying and wobbling, and one of those children – the girl – split into two and the second shadow of a girl started growing red eyes and slasher smile and two knives and she looked directly upon a Biker. He got branded by an ephemeris.

Every life he got seizures and some blood from his nose. Standard interaction with magical field by a normal human.

In the meantime, the Commando started poking into Michał’s poor snake Dragon avatar. If he actually became Dragon, the commando wondered what happened in terms of game mechanics – is it possible to do something to him? He started getting other wizards and cast different spells – from transmitting spells to healing spells just to see what happens. He wanted to change Michał into human. Magical field corresponded to his will. He managed to extract Michał from the computer – but their Guildhall became a PVP zone and every other Guild was informed of that.

Oopsie.

Commando and Nerd started mounting defenses, while nerd started phoning tech support „do something about it”. He started wailing at them that he is a paying customer, game masters want to destroy their guild, is going to be in a press because they have a prominent streamer in a guild, yada yada. The game masters answer – they managed to flip PVP zone off – but for some players got defeated on the defenses and some loot was present already. The players girl world asking commando and nerd to let them get some of their loot back. They allowed it. Conditionally. After proper groveling and sending some stupid photos. And the best loot get stolen anyway.

Szczepan got irritated. They promised him some coupons from his stupid brother Michał (who incidentally just got reconstructed from the computer in commando’s basement and it scared ran out of that place, completely naked, through the forest. Lol.). But the biker convinced Szczepan that he can help him with his character instead and he actually did. Szczepan was a player of this game – he has a character named xxTerrorxx, he is a decent player, very skilled, a bully. Oh well. The biker gave him some mid-high range loot from Zaczęstwiacy’s Guildhall and told his guild mates that Szczepan storage. So he’s going to get farmed.

The Progamer started looking at the computer belonging to Michał. There must be a clue on what is happening. There was a clue – a discussion between a girl, Suzanne, and Michał. Suzanne was whining that Szczepan was bullying her and her friends. Michał was saying that there is nothing he can do his brother simply is like that. And the biker noticed that this girl is exactly the one whom he saw in her vision with the slasher smile and slasher eyes. A clue. Awesome.

The group kind of got together. They compared the notes and information. It kind of looks like there is something more about this thing that they can understand – maybe it is kind of, magic? The nerd started searching for things related to magic and he found Olga. She is a local which – or so is the rumor. He kind of likes shade the Internet websites so he was able to gather information on her that there are some unnatural things going on about her. What to do? Of course, contact her.

So the nerd started chatting with Olga on line – sending emails and waiting for responses. He sent her information what happened and that she is the best knows about unnatural things so maybe she – as an animatronics designer – can help them with. Also, magic.

Olga was really, really, really unhappy. From the description of it all it kind of looked like magic. But she was afraid to contact local termini simply because they don’t really like her – she has some problems with them. She can’t control her own creatures. So it would be the best if she looked at it personally, but she doesn’t have any power of catalysis. So she took one tracking pig with her and drove to the house of a nerd.

In the meantime the children (biker and progamer) decided to explore the angle of Suzanne. They went to see Lars – a player who is a casual player and also was in the biker’s visions. Lars was amazed that a progamer met him and he had said everything. Suzanne, he and many other people really hate Szczepan. His bullying them, stealing money and the single policeman in Żarnia doesn’t really care because he wants to be hero and do big stuff. And Suzanne was thinking of things like hurting herself so this all goes away or something.

Okay, so Suzanne is an answer. They need to see her. The biker and progamer went to see her. And they did – the parents were of the richer family but they didn’t care about their child. The children proved Suzanne so she says everything she knows about that and they used Lars knowledge to show they know what is happening. Suzanne cracked. She started talking that she had no idea what to do, and Szczepan was really a bad man and it all has to stop, so in desperation she used a black book of black rituals or something and they used Bullard with four of her friends and something was supposed to happen but nothing really happened…

… In the meanwhile her voice started to be more droning and droning and flat and biker who was branded by an ephemeris felt that something is watching behind them. He jumped on a progamer and pushed him on the ground. An ephemeris missed with her knife. The children ran up their bikes and started running as fast as they can but an ephemeris was closer and closer regardless of the forest and the trees getting closer with manic smile.

Not cool.

Olga entered the nerd’s house. She was scared and slightly timid but she had her tracking pig which was kind of helpful as an catalyst. It was the most awkward meeting ever – a shy, dirty nerd and timid Olga. But she was the one to pull out the pig and start to find traces of magical energy and so she found it. She deputized the commando and nerd because she needed to stop the masquerade breaking from spreading. In the same time she started to consider what can she do with more than just a hint of panic. It is not her area of expertise. Not at all.

Then – a tragedy struck. The door opened with a flower delivery man called by a nerd. The pig looked up at flowers and with squeaking ran to devour everything. The nerd got shocked and scared - it was his flowers for her. The commando seeing imminent attack through his life directly into the pig. The pig got wounded. The pig shouted, scared and teleported away. Olga shouted at the commando and the nerd calling them every possible obscenity and ran searching for her pig. She couldn’t have teleported too far away.

In a completely different place, an ephemeris has managed to wound a progamer. Shocked and scared, programmer was riding a bike as fast as he could and then pig teleported in front of him and they fell down with a massive thud.

The biker looked at his fallen comrade and started riding faster. A progamer, scared like hell, raised the pig in the air, lion King style and started twisting and shaking the pig – hoping he will get teleported with her. It actually worked. The pig saw a knife in hands of an ephemeris and decided she wants to be everything batch next to a knife, again.

The pig teleported next to Olga with a progamer. Olga decided this is definitely too much for her and for her abilities and skills here and, with her heart, she decided to call some termini to solve the situation and she gathered all witnesses of magic in the area.

This time she didn’t get scolded. Everything went fine and termini managed to regain control over the area, clear Suzanne, destroy an ephemeris and make everything work.

Wynik:

* Tor Porażki: 7/20, 8/25

Wpływ:

* Żółw: N/A
* Gracze: N/A

**Epilog**:

* Zaczęstwiacy którzy uczestniczyli w akcji uznali widząc Olgę, że magia ssie w porównaniu z grą. Nie będą nosić świni. To sprawiło, że praca terminusów była jeszcze prostsza.
* Zaczęstwiacy zdobyli epicki sprzęt z wężosmoków i są jedyną gildią która ma świetne strategie na tamtą planszę - nikt im na tamtej mapie nie dorównuje
* Szczepan miał w nocy wjazd Komandosa który go nastraszył i od tej pory Szczepan już nie gnębi ludzi w realu - boi się.
* Szczepan nie ma życia w Smoczych Wojnach - Zaczęstwiacy go tępią na lewo i prawo. xxTerrorxx po prostu nikogo już nie ma jak terroryzować.
* Biker założył eAkademię Zaczęstwiątek - by uczyć noobstwo i casualstwo radzenia sobie w grze. Znalazł w sobie paladyna.
* Z uwagi na epicką walkę w guild hallach, pvpzone w guildhallach stało się możliwością.

### Wpływ na świat

| Kto           | Wolnych | Sumarycznie |
|---------------|---------|-------------|
| Żółw          |         |             |
| XXX           |         |             |

Czyli:

* (K): -

## Streszczenie

W okolicach Żarni pojawiła się efemeryda nienawiści przez to, że pewien chłopak gnębił młodsze dzieci. Efemeryda nie trafiła i przeniosła jego brata do komputera. Szczęśliwie, gidia Zaczęstwiaków osłoniła nieszczęśnika i wezwała jako wsparcie Olgę - ona po zorientowaniu się w sprawie wezwała terminusów i sprawa została rozwiązana.

## Progresja

* Olga Myszeczka: dostaje odznaczenie za najbardziej nieefektowne użycie magii - ludzie nie chcieli wejść w świat magów, bo nie podobał im się styl Olgi. To taki "ignobel".
* Szczepan Korzpuda: kocha Smocze Wojny, ale nie ma życia jako xxTerrorxx - Zaczęstwiacy na niego polują.

### Frakcji

* Zaczęstwiacy: podpadli wielu gildiom w Smoczych Wojnach za akcję z gildią, tauntowanie Armaddy i ogólnie agresywną grę
* Zaczęstwiacy: w Smoczych Wojnach są najsilniejszą gildią w Otchłani Wężosmoków. Nikt im tam nie dorównuje.
* Zaczęstwiacy: jedna z najsilniejszych gildii w Smoczych Wojnach, acz o reputacji nikczemników.
* Armadda: gildia w Smoczych Wojnach, silnie skonfliktowana z Zaczęstwiakami i o strasznie obniżonej pozycji po sprawie z wężosmokami. Taka gildia masowa.

## Zasługi

* Piotr Ryszardowiec: progamer; wytrząsł teleportację świnią oraz dowiedział się że celem nie był Maciek a Szczepan (tylko efemeryda spudłowała)
* Robert Einz: komandos; grał ostrym dpsem, dużo eksperymentował jako mag (w grze), zranił świnię i wyrwał Maćka z komputera
* Adam Wroński: biker; wrobił xxTerroraxx w wieczne piekło, sprzęgł się z polem magicznym i zobaczył zdecydowanie za dużo o efemerydzie (ze wzajemnością)
* Zdzich Aprantyk: nerd; mistrz socjala w internetach ze znajomościami w ciemnych częściach internetu. Sprawił, że Armadda jest pośmiewiskiem a nie gildią.
* Olga Myszeczka: najbardziej niewłaściwa czarodziejka do rozwiązywania infomantycznego Skażenia z magią krwi robioną przez ludzi. Poradziła sobie terminusami i świnią tropiącą.
* Maciej Korzpuda: chciał dorównać bratu i został zamieniony w wężosmoka. Zaczęstwiacy go uratowali. Potem skończył lecąc nago przez las po wyrwaniu z kompa przez Komandosa.
* Szczepan Korzpuda: xxTerrorxx, nie ma życia. Bully, świnia i nieprzyjemny typek. Przez niego doszło do Skażenia pola magicznego w Żarni.
* Zuzanna Klawornia: kindersatanistka i kindergothka. Bogaci rodzice się nią nie interesują. Wezwała z kolegami i koleżankami efemerydę nienawiści i zemsty magią krwi.

## Plany

* 

### Frakcji

* 

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Zaczęstwo
                                1. Nieużytki Staszka: miejsce, gdzie mieszka Robert (bioniczny komandos). Tam Olga wykryła energię magiczną i tam 
                            1. Żarnia
                                1. Osiedle Połacierz: epicentrum mrocznych wydarzeń; tam Zuzia użyła mrocznej energii i stworzyła Efemerydę Nienawiści, tam też mieszka xxTerrorxx.
                                1. Żarlasek: las niedaleko Połacierza; tam efemeryda goniła i chlasnęła biednego Piotra. Tam też teleportowała się Świnia Tropiąca.
    1. Multivirt
        1. Smocze Wojny: najpopularniejsze i najlepsze MMO fantasy we wszystkich światach
            1. Otchłań Wężosmoków: rzadko uczęszczane miejsce (niski xp/min, silni wrogowie); tam trafił nieszczęsny Maciek zamieniony w wężosmoka
            1. Siedziby Gildii: miejsca gdzie można uruchomić pvp zone, znaczące gildie mają możliwość zdobycia sobie siedziby

## Czas

* Opóźnienie: 11
* Dni: 1

## Narzędzia MG

### Budowa sesji

**SCENA:**: Nie aplikuje

### Omówienie celu

* Omówienie sesji i jej celu
* Czego Ty **graczu** chcesz od sesji? O jaką opowieść grasz?
* Dopiero potem karty postaci kalibrowane pod kątem sesji i celu gracza

## Wykorzystana mechanika

1810

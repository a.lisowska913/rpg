---
layout: cybermagic-konspekt
title: "Umierająca farma biovatów"
threads: legenda-arianny
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [210224 - Sentiobsesja](210224-sentiobsesja)

### Chronologiczna

* [210224 - Sentiobsesja](210224-sentiobsesja)

## Punkt zerowy

### Dark Past

* .

### Opis sytuacji

* Jedna z farm biovatów w okolicy Ostropnącza Lucjuszowi obumiera.
* Lucjusz musi wprowadzić świeżą, sprawną krew. Skupił się na mieszkańcach Wremłowa. Wprowadził tam przepiękną wiłę - pułapkę.
* Sam ma niedaleko biovat z tkankami; wszystko jest lekko zamaskowane używając jego mistrzostwa.
* Występuje w drugiej formie - xirathiropodobnej.

### Postacie

* Viorika Verlen: Kić

## Punkt zero

N/A

## Misja właściwa

Viorika znajduje się po raz kolejny w Holdzie Bastion. A dokładniej, we Wremłowie - miejsce wypoczynkowe, więc można się napierać z niedźwiedziem. Ogólnie, Wremłowo jest miejscem spokojniejszym. Viorika jest tam by latać po imprezach i miejscach zabaw. Trzeba się zabawić, rozerwać i poużywać życia. Live hard, die hard.

Viorika ma w naturze sprawdzanie statusu sentisieci. To jest to, czym Viorika **jest**. I Viorika widzi anomaliczne wskazania sentisieci. Coś za małego by podnieść alarmy, ale wskazuje, że poziom energii i poziom "Verlenowania" w okolicy Wremłowa jest niższy niż powinien być. Tak jakby populacja była mniej zdrowa, miała mniej energii czy żywotności. Małe liczby, ale Viorika jest na to wyczulona. Widzi w danych _regularność_. Coś zaczęło się dziać dwa tygodnie temu. I kaskaduje. Ale zawsze delikatnie i ostrożnie. Viorika podejrzewa działanie celowe - czyli Blakenbauer >_> .

Nie byłby to pierwszy raz.

Jeśli jest tu gdzieś Blakenbauer, to sentisieć może być przekierowana na pełną moc. Ale to jest kosztowne. Więc wpierw - czy są punkty, dookoła których osłabienie energetyczne jest szczególnie mocne? Sentisieć jest odpalona. Viorika chce poznać więcej informacji na temat tajemniczego Blakenbauera.

* V: teren działania, punkty zainteresowania - hotele. Osoby dotknięte tym są w miejscach "mieszkalnych". Energia w hotelach się nie zgadza. Jest impulsowa - czyli tak jakby nie był to syfon z populacji, a jedna osoba naraz. Aproksymacja - około 15-20 osób.
* V: sentisieci się udało zlokalizować jedną z tych osób. Jest to Romek Rumak, żołnierz. Silny, jurny chłop. Teraz - cień siebie. Taki apatyczny. Jest w hotelu. Miał się regenerować i odpoczywać, jest po ostrej akcji (czy coś). Nikt ważny taktycznie.
* nie ma powodu, by on opuszczał miasto z jakichkolwiek powodów
* wszystko wskazuje na to, że MUSIAŁ opuścić miasto - ale poza miastem (ze słabszą sentisiecią) musiał mieć miejsce ów podły akt.

Viorika podbija do hotelu, spotkać się z owym Romkiem. Romek ją wpuścił. Jest zdechły i apatyczny, ale przy Viorice trochę odżywa. Ma te iskierki w oczach, że chciałby ją "pat pat" po głowie XD. Coś się mu stało. Jak Viorika chciała wziąć go do lekarza, zaczął oponować; Viorika musiała go przekonać używając gróźb i rozkazów (których on nie do końca akceptował): TrZ+3: XV. De facto, Viorika musiała mu pieprznąć. Coś ciężkiego w łeb. Leżał zamroczony dłużej niż powinien, ale to go "przekonało". Sam się zastanawia, czemu nie chciał pójść sam do lekarza... w sumie? Viorika ciężko wzdycha. Faceci. Żołnierze...

Doktorem jest niejaka Selena Walecznik. Selena chce obejrzeć nieszczęsnego żołnierza, ale Viorika... wykrywa anomalię sentisieciową u lekarki. Coś jest nie tak z lekarką dookoła sentisieci. Viorika spytała Selenę wprost - o co chodzi z tą "drugą" sentisiecią. Selena nie wie o co chodzi - druga sentisieć? Viorika robi aktywny skan sentisiecią (XXX) - Selena krzyknęła z bólu i się zwinęła, po czym... wstała i zrobiła salto. Ucieka przez okno. Viorika leci za Seleną XD.

Viorika postawiła oczy - sentisieć ma obserwować gdzie ucieka Selena. A żołnierz - Roman - ma tu zostać.

Lucjusz dostał informację, że Pasożyt został znaleziony. Lucjusz zwija biznes i ucieka.

Viorika śledzi Selenę. Selena uciekła do swojego mieszkania. Przygotowuje tam bombę - nie robi zagrożenia, ale chce MÓC zrobić zagrożenie w dowolnym momencie. Nikomu ma się PRZYPADKOWO krzywda nie stać. Nie pytając o nic, Viorika bierze negocjatora wojskowego - Frezję Amanit. Frezja zażądała, by Selena się poddała (zaprzestała tego, co robi). Selena zażądała wina typu anais. Rzadkiego. Trzeba je ściągnąć; to zajmie jakąś godzinę. A Selena nie wie, co się dzieje; coś nią kieruje.

Jednocześnie Viorika wie, że żołnierz nigdy nie był u Seleny. Nigdy. To znaczy, że Selena jest innym odłamem tego co robi Blakenbauer. Czyli Blakenbauer gra na czas. Czyli robi jednocześnie coś innego.

A Viorika zbiera sztab ze swojej ochrony, sprawdza czy są czyści (są czyści). Podsumowała wydarzenia. Podejrzewany jest Blakenbauer, gra na czas i wie, że coś się spieprzyło.

Sweep sentisieciowy - w promieniu 10 km od Wremłowa (ExZ+3):

* V: sukces; znalazła miejsce. To rachityczny lasek z jaskiniami, na zachodzie od Wremłowa. Tam się kryje Blakenbauer i jego kreacje.
* X: jest "on the move". Ewakuuje się, jego kreacje idą inną stroną.
* V: nie wie, że sentisieć go dokładnie zlokalizowała. Jego kreacje się rozproszyły - wszyscy dążą do granicy.

Viorika ogłasza w miasteczku Blakenbauer Bounty. Każdy kto przyprowadzi dodatkową płaszczkę ma dodatkowy dzień urlopu. Okazja do polowania i walki. Viorika przygotowuje planowanie z pomocą sztabu (TrZ+2) - używając wiedzy z sentisieci, przewidując co Blakenbauer wie o tym terenie, zakładając że jest kompetentny (zwłaszcza, że ten się wykazał) i widząc jak rozproszył swoje siły grupuje łowców w oddziały by mogli zapolować na płaszczki tam, gdzie nie ma Blakenbauera. Bo tego, to Viorika chce dla siebie.

* V: Viorika przechwyci grupę z Blakenbauerem
* V: Łowcy przechwycą najważniejszą grupę ewakuujących się płaszczek, z "bounty"
* X: Część Łowców zostanie poraniona polując na płaszczki
* V: Łowcy przechwycą WSZYSTKIE płaszczki. Z tylko niewielką pomocą sentisieci.

Czas na konfrontację z Blakenbauerem. Viorika wyczekała miejsca taktycznie dobrego - niestety, Blakenbauer się zabunkrował. Schował się w sporej jaskini, wraz z jedną z płaszczek. Viorika zrobiła krótki sweep - czy idzie odsiecz? Odpowiedź: nie. To znaczy, że Viorika ma inevitability; Blakenbauer musi zrobić ruch. By nie doprowadzić go do desperacji (niebezpieczne z Blakenbauerami), Viorika zdecydowała się z nim porozmawiać. "Twój plan się spieprzył. Przyznaj się."

Lucjusz Blakenbauer. Powiedział:

* Ma ludzi w biovatach. Specjalnie hodowanych. Uszkodzony strain.
* Ogólna opinia - zniszczyć, nowy batch. Lucjusz chce ich uratować.
* Potrzebuje _vitae_ i to ekstraktował.

Viorika przechwyciła kontener z _vitae_. Ma niski próg zwrotu - nie da się łatwo oddać tego, co Lucjusz zabrał. Ale nie chce mu oddać - bo nagrodzi kradzież. Zaproponowała mu, że może zrobić prośbę charytatywną po ludziach Verlen. Lucjusz stwierdził, że Viorika chyba zgłupiała. Nikt się na to nie zgodzi, zwłaszcza nie jak Blakenbauerowie proszą. Są w małym klinczu. Viorika ma pomysł - zapas języczników, których koszt będzie odczuwalny. Plus, Lucjusz jej wisi. Coś małego i nie przeciwko Blakenbauerom. Lucjusz niechętnie, ale się zgodził. Chce pomóc tym ludziom, nie chce ich zniszczenia...

## Streszczenie

Lucjusz Blakenbauer nie chciał dopuścić do śmierci jednej biovatowej farmy. Potrzebował vitae. Zaatakował dyskretnie ród Verlen, pobierając vitae z żołnierzy w Wremłowie. Viorika była akurat na wakacjach, zobaczyła anomalię w sentisieci i odparła ruchy Lucjusza; używając kombinacji sentisieci i taktyki zmusiła go do kapitulacji. Może zabrać to co zdobył, ale musi przekazać zapas języcznika i jej wisi coś małego.

## Progresja

* .

### Frakcji

* .

## Zasługi

* Viorika Verlen: Sentisiecią znalazła obecność Lucjusza Blakenbauera i jego podłych planów we Wremłowie a taktyką i umiejętnościami dowódczymi złapała wszystkie jego płaszczki - nawet te, które były przeznaczone do prześlizgnięcia się.
* Selena Walecznik: lekarka we Wremłowie; bardzo sensowna, ale tym razem zdominowana przez Lucjuszową płaszczkę; nie zauważała przez nią działań Lucjusza. Wykryta przez Viorikę, płaszczka grała na czas.
* Frezja Amanit: super-cute negocjatorka Verlenów, zdolna do połamania kończyn każdemu z zaskoczenia. Nie dała Selenie (płaszczce) grać na czas i ostrzegła Viorikę co Selena (płaszczka) robi.
* Lucjusz Blakenbauer: chciał pomóc ludziom z biovatów by nie zostali zniszczeni, więc zinfiltrował Verlen syfonując z odpoczywających żołnierzy _vitae_. Znaleziony i pokonany przez Viorikę.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Aurum
                        1. Verlenland: 
                            1. Wremłowo: niewielka mieścina w zasięgu Holdu Bastion; zwykle taka na odpoczynek wojowników.
                                1. rachityczny lasek z jaskiniami: na zachodzie; kryje się tam Blakenbaue

## Czas

* Opóźnienie: 317
* Dni: 2

## Inne

### Projekt sesji

Ród Verlen

* Skrajnie militarystyczny, silnie wojskowy, z tradycjami
* Aurum: Laetitia - Niezależność - Czystość
    * Verlen: Duty|Discipline - Niezależność - Czystość
    * Verlen: "To my stoimy na murach, gdy wszystkie ogniska zgasną"

Ród Blakenbauer

* Silnie powiązany z bioformami, "płaszczkami". Specyficzne poczucie humoru.
* Drakolickie podejście do sentisieci; ludzka forma jest tylko jedną z możliwości.
* Często uważani za ród czarny.

### Sceny sesji

* ?

## Idea sesji

* ?

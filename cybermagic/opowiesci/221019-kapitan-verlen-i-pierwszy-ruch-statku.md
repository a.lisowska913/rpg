---
layout: cybermagic-konspekt
title: "Kapitan Verlen i pierwszy ruch statku"
threads: historia-arianny, program-kosmiczny-aurum, waśnie-samszar-verlen
gm: żółw
players: fox, kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [211012 - Kapitan Verlen i niezapowiedziana inspekcja](221012-kapitan-verlen-i-niezapowiedziana-inspekcja)

### Chronologiczna

* [211012 - Kapitan Verlen i niezapowiedziana inspekcja](221012-kapitan-verlen-i-niezapowiedziana-inspekcja)

## Plan sesji
### Theme & Vision

* Królowa Kosmicznej Chwały, support ship

### Co się wydarzyło KIEDYŚ

* .

### Co się wydarzyło TERAZ (what happened LATELY / NOW)

* .

### Co się stanie (what will happen)

* S01: pierwsze działanie w stylu ćwiczeń. FAILURE. Nawet jak się da strzelić, nie działa jak powinno.
* 
* S0N: statek nie jest w pełni sterowny a trzeba doprowadzić go do działania.
* S0N: część załogi: nie lubimy Aurum. Nie będziemy się starać (bosman Tadeusz Ruppok). Resentymenty, czemu mamy pracować?
* S0N: przerażony Tomasz Dojnicz (marine) ma zatrzymać bójkę między ludźmi - "ludzie Aurum" vs "ludzie - załoga"
    * "Aurum forever"
* S0N: Klaudiusz Terienak: ta jednostka miała dla niego wartość BO WŁADAWIEC. Pozbyć się Arianny.
* S_high_level: dolecieć do Karsztarina i wprowadzić advancera na pokład
    * Maja -> Tomasz (taunt Arianny), Maja -> Arianna (nie mówi o inspekcji); Maja jest bardzo pro-Aurum

.

* Pierwszy oficer - Władawiec Diakon - podrywa na lewo i prawo bawiąc się hormonami.
    * W medycznym jest tien Klaudiusz Terienak który chce zdobyć uznanie Władawca, ale boi się Leony.
* Aurum podzieliło się na frakcje i próbują wygrywać między sobą.
* Leona Astrienko jest tu na wakacjach - ale też ma uniemożliwić śmierć kogokolwiek czy jednostki.
    * 2 sarderytów, Hubert i Irkan nie dają się jej ale też w sumie nikomu skrzywdzić.
* Leona + 5 marines opanowuje tą 40-osobową jednostkę.
* STRASZNA nieufność Orbitera do Aurum
* Klarysa Jirnik, artylerzystka i odpowiedzialna za broń
* tien Grażyna Burgacz, logistyka i sprzęt
* tien Arnulf Perikas, fabrykacja i produkcja - on jest bardzo za Arianną i najbardziej rozczarowany Alezją

### Fiszki

* Władawiec Diakon: tien, pierwszy oficer Królowej Kosmicznej Chwały, podrywa na lewo i prawo bawiąc się hormonami
* Klaudiusz Terienak: tien, w medycznym jest tien Klaudiusz Terienak który chce zdobyć uznanie Władawca, ale boi się Leony.
* Leona Astrienko: jest tu na wakacjach - ale też ma uniemożliwić śmierć kogokolwiek czy zniszczenie jednostki
* Grażyna Burgacz: tien, logistyka i sprzęt
* Arnulf Perikas: tien, fabrykacja i produkcja - on jest bardzo za Arianną i najbardziej rozczarowany Alezją; fan batożenia

### Sukces graczy (when you win)

* .

## Sesja właściwa
### Wydarzenia przedsesjowe

.

### Scena Zero - impl

.

### Sesja Właściwa - impl

DZIEŃ 1:

Arianna widzi niedopatrzenia w sekcji łączności - bo powinna dostać choć komunikat. Oficer łączności - Maja Samszar dostaje opiernicz i wytyczne by za każdym razem czekała na ACK i wytyczne. Maja "oczywiście pani kapitan". Arianna "widzę że brakuje dyscypliny więc dołączasz do diety konserwowej". Maja jest zdecydowanie nieszczęśliwa. "Pani kapitan, to fatalny pomysł, skończą się". Arianna - fundusze na inne zasoby. "Patrz z innej perspektywy - to pierwsze uchybienie względem mnie i jedne wobec Alezji w niestosownym momencie. Stąd niewielkie zaostrzenie dyscypliny." Maja "Czyli pani kapitan chce bym jadła konserwy jako posiłki?!" Arianna "dieta konserwowa wraz z nami wszystkimi." Maję to naprawdę, naprawdę zabolało - ona NAPRAWDĘ musi jeść.

Maja "Dobrze, zrozumiałam. Ostatni raz kiedy panią zawiodę." Arianna "tydzień diety. Na ponowne zaufanie musisz zasłużyć a konserw mamy dużo."

Arianna próbuje PRZEKONAĆ Maję do posłuszeństwa. Maja jest oporna i ma swoje zdanie. Ale Arianna ma REPUTACJĘ.

Tr Z (REPUTACJA Arianny) +3:

* Vr: ZROZUMIAŁA. Nie ma co fikać Ariannie. Skończy się fatalnie - konserwami itp.
* V: Maja nie będzie podjadać, wykona polecenie. Arianna ma silną wolę i jest psychiczna.
* V: Maja się przyznała Ariannie czemu to zrobiła bo wtedy Arianna może WARUNKOWO wycofać konserwy. Romeo Verlen zrobił krzywdę Mai oraz Ariannie. Zniszczył Mai jedzenie, które Maja zrobiła na urodziny jej brata. Wiem, że jest blisko Ciebie.
    * Maja ma minkę WTF jak Arianna zaproponowała, że zrobi coś brzydkiego Romeo
    * POTEM (dalej oburzona Maja) brat mojej kuzynki wyzwał Romeo na pojedynek. I przegrał. A potem była SERIA bitew. I wszystkie przegraliśmy. On nie grał fair (znając Romeo na pewno nie grał fair, ale się dobrze bawił).
    * "Chcesz pomścić przegrane z Romeo bo oszukiwał?" "Chcę pomścić honor rodu!" "Nie widzę powodu by sprzedać Ci info które Ci pomoże i by nasze rody utrzymywały pozytywne relacje. Trzeba go odpowiednio ukarać." Maja: "On powinien ożenić się z moją kuzynką." A: "Czemu ożenić?" M: "Bo rozpuściła zaproszenia a to był żart." A: "POMOGĘ CI W POMSZCZENIU HONORU RODU! ZOSTAJESZ PRZY KONSERWACH".

Maja zrobiła najbardziej zbolałą minę w historii. Arianna widzi, że ona naprawdę WALCZY. Arianna żeby rozważyła pomoc w honorze rodu. (+1Vg)

* X: Maja wybierze wspólny honor rodu, pomszczenie Romeo i nie będzie zwalczać Arianny ALE jest wobec Arianny nieufna.

Arianna zneutralizowała Maję. I potem, by uniknąć ataków tego typu zaczęła rozpuszczać plotki na swój temat - a dokładnie, że niezależnie jaki jest wektor ataku to Arianna od razu kontratakuje.

Tr Z (historia + VERLEN + osoby wsparcia) +4:

* V: Plotka się pojawiła i jest wiarygodna - Arianna od razu konfrontuje z wszystkimi co jej szkodzą.
* Vz: Daria "przypadkowo" wyciągnęła co się dzieje gdy Verlenowie idą za daleko. Nikt nie chce podpaść Ariannie aktywnie.

Arianna jest w miarę zadowolona.

DZIEŃ 3:

Arianna ma sprawną Semlę. Po raz pierwszy od dawna. Systemy... bywało gorzej:

* niski poziom zasobów. Serio mało zasobów do jednostki wsparcia.
* uszkodzone grazery, ale naprawialne.
* DUŻO wyższy poziom paliwa niż Arianna myślała.
* DUŻO więcej konserw niż Arianna myślała (o dziwo, zdecydowanie za dużo konserw z rybą).
* poszycie jest stabilne. Struktura jest stabilna. STATEK jest stabilny - dobrze o niego dbali.
    * są ślady, że inne jednostki naprawiały Królową gdy nikt nie patrzył
* silniki po interwencji Darii i naprawie ORAZ reaktywacji Semli są sprawne - ale nie warp.
* są mniej stabilne sektory na jednostce, co wynika z wyścigów psów. Ale NADAL nie rozpadną się w locie.

Arianna chce zrobić ćwiczenia. Wybrała advancera który ostatnio się wykazał (Szczepan Myrczek). W nagrodę on jest spearheadem - jest advancerem który ma wylądować na Karsztarinie. Załoga Królowej jest wystarczająco zmotywowana - nie zadziałają perfekcyjnie (bo nie umieją), ale powinno działać. I jak się uda to Arianna zapewni im automat do kawy i ciepła woda pod prysznicami.

Królowa musi dolecieć skutecznie do Karsztarina. Połączenie silników o dużej mocy i małej mocy. Stefan Torkil wyraźnie przełknął ślinę. Z lekką obawą podszedł do konsoli Królowej. Włączył silniki i sukcesywnie wzmacnia ciąg.

Tr Z(naprawy i łaty Darii) +4:

* X: Semla odrzuca integrację z pilotem. Daria musiała załatać, ale to ciągła "walka". Zaszłości; trzeba dokalibrować Semlę. To coś na 3h.
    * Daria -> pilot "weź mów do niej". Stefan tak spojrzał. "Dobra Semla, grzeczna Semla..."
* Vz: O dziwo, zadziałało. Połączenie Darii i Dobrych Słów Stefana sprawiły, że silniki weszły na dużą moc i Królowa lekko drżąc i chybocząc leci prawidłowo.
* X: Koszt paliwa to 125%. Na pokładzie zaczyna się NERWÓWKA "ALE JAK TO LECIMY!". Migotanie reaktora nie pomaga.
* X: Reaktor awaryjnie zgasł. Za nim poszły silniki. Na statku ciemność.

Królowa nie zadziałała poprawnie - ruszyła, poleciała. Teraz DRYFUJECIE do przodu bez prądu bez energii bez silników. Awaryjny reaktor się nie włączył. Arianna WIE że inspektor kazał jej korzystać z pomocy. Arianna poprosiła Darię - reanimuj.

* V: Królowa wróciła do działania panicznym działaniem Darii i inżynierów - odcięli Semlę i poszli na manual, po czym Daria ZNOWU steruje silnikami ręcznie. (+2Vg)
* V: Daria manualnie ma wprawę, zatrzymała Królową i zrestartowała pętlę. Ariannę, niestety, czeka odwołanie ćwiczenia do admiralicji.
    * Arianna szybko odwołała ćwiczenia. Niech inżynieria się tym zajmie i doprowadzi wszystko do działania.

TYMCZASEM ARIANNA.

Arianna zbliża się szybkim krokiem do głównego źródła hałasu - mesa. Tam jest regularna napieprzanka. Emocje, stres, energia. Ludzie się biją a marine Tomasz stoi i patrzy i nie wie jak ich rozdzielić. SERIOUSLY. Arianna robi wyładowanie magiczne - od włosów do wyładowania po ścianach i eksplozja energii głośna i niegrożna.

TpMZ+4+3Ob:

* Vr: Arianna PRZERYWA walkę. "DZIESIĘĆ MINUT nie miałam kontroli nad pokładem! DZIESIĘĆ MINUT! Czy w TEN SPOSÓB się zachowują marynarze na JEDNOSTCE KOSMICZNEJ Aurum, Orbitera? Jak jest awaria na statku optymalną strategią jest pobić się z sąsiadem!? GDZIE JEST DYSCYPLINA! CZEMU NIKT NIE BIERZE NAPRAW NIE WIADOMO CO SIĘ DZIEJE!"
    * Arnulf: pani kapitan, rekomenduję batożenie.
    * Daria: rekomenduję konserwy. Z rybą.
    * ARIANNA: TYDZIEŃ CIĄGŁYCH ĆWICZEŃ W ŚRODKU STATKU! BEZ RUCHU! Przemieszczanie, reakcje kryzysowe. Będą szkolenia bhp 2h dziennie. I konserwy z rybą.
    * Starszy Mat Warkoczyk: oni nie robią roboty. Ci z Aurum chcieli robić ale nie wiedzieli jak (jego ludzie). Ci z K1 ("Ci z Orbitera") nie wiem czy wiedzieli jak ale mega spanikowali i tamten (pokazał palcem) zaczął sabotować elektronikę. Żebyśmy nie lecieli. Odciął reaktor. Warkoczyk go przyniósł, zaczęła się pyskówka i walka.

Arianna go do karceru do wyjaśnienia. Koleś Marcel Trzęsiel. Z K1. To jest koleś, który się zna trochę na inżynierii, trochę na "wszystkim", tu jest jako koleś od syntezy (pod Arnulfem). "ŻYJEMY DZIĘKI BOGU PANI KAPITAN NIE MOŻEMY LECIEĆ"! Koleś widział co zrobili statkowi więc OMG ZGINIEMY JAK POLECIMY i wyłączył reaktor. Wiele osób z K1 ma wątpliwości co do sprawności tej jednostki.

Arnulf powinien coś wiedzieć na jego temat. Arnulf nie fraternizuje się z załogą. Arnulf nie zna swojego podwładnego. Arianna ma facepalm. I znowu wszystko musi robić sama. By dodać +1 do rozpaczy, psychologiem załogi jest Klaudiusz - koleś dający środki do jedzenia. No tylko się powiesić.

* Arianna "znowu podejdziemy do ćwiczeń, powiedz co wiesz. Jak coś grozi statkowi to po nas wszystkich. Wiem, że chciałeś dobrze i nie chciałeś krzywdzić statku i trzeba było, nie chcę więcej zamieszania tego typu bo komuś stanie się krzywda. I Stefan Torkil zareagował gwałtownie nie wiedząc co się dzieje. Jak nie będziemy się informowali, takich sytuacji będzie więcej i wszyscy na tym stracą. Musimy się dogadać by się nie ranić nawzajem."

TpZ (plotki o Verlenach) +4:

* V: Marcel przysięga że ze statkiem nic nie zrobił, nie wie nic więcej... po prostu nie ma prawa działać. On się po prostu boi lecieć tym statkiem.
* V: Marcel wyjaśnił czemu - Stefan Torkil, on go zna. Marcel chciał trafić na jednostkę która NIE LATA. I się okazało że pilotem jest STEFAN. Który jako pilot bardzo uszkodził swoją poprzednią jednostkę i załogę. Latał po pijanemu. Marcel ledwo przeżył.
* X: Marcel wielokrotnie prosił o przeniesienie. Nie udało się. Nikt go nie chce. On tu musi być. A boi się latania.

Marcel ma trifectę karniaków - konserwy, psycholog. Będą ćwiczenia, ma się udzielać. I tak przez tydzień Królowa się nie ruszy. A Marcelowi może będzie odpuszczone mycie kibli na statku.

Klaudiusz psycholog długoterminowo pomaga Marcelowi. A Arianna wszystkich informuje co i jak by Marcela stabilizować:

Tr Z (bo ma go w łapkach) M (bo lol, jest magiem) +3 +3Ob:

* Vr: Marcel będzie w stanie funkcjonować w poruszającej się jednostce docelowo. 
* X: Klaudiusz przekierował strach i traumę Marcela w miłość. Do Stefana.
* Xz: Klaudiusz chciał naprawić tą miłość do Stefana. Marcel ma całkowicie przekierowany strach w 'uniesienie o jeden grzyb za dużo'. On nie czuje już strachu. Im bardziej by się bał tym bardziej jest 'baked'. To sprawia, że Marcel nie będzie ROBIŁ problemów i jest jakoś przydatny. Jakoś.
* Om: "Marcel" -> "Marcelina". Marcelina jest INNĄ OSOBĄ niż Marcel. Klaudiusz nie wie jak się przyznać. MARCELINA SZTUCZNA LASKA.

(to się stanie w ciągu tygodnia i Marcel / Marcelina nie ma pojęcia że się zmienia)

TYMCZASEM

Arianna ma kaprala Psa. Kapral Pies ma przygotować mordercze ćwiczenia. W wypadku awarii mają zachowywać się jak powinni. Ćwiczenia awaryjne. Zgnoić. I przećwiczyć awaryjnie. A Daria jest ostrzeżona.

Ex Z (doświadczenie Psa + Arianny) +3:

* Vz: grupa ma tylko lekkie rany. Udało im się przećwiczyć. Przetrwali nawet alarm o 3 rano że przebicie kadłuba, z niewielkimi stratami. Nie jest to dobra załoga i dobrzy oficerowie, ale już Ariannie nikt nie wyłączy reaktora w panice i Stefan jest w stanie bezpiecznie prowadzić Królową.

Arianna poszła porozmawiać ze Stefanem. Bał się prowadzić Semlę / Królową. Arianna -> Stefan o alkoholu.

Tr Z (reputacja Arianny) +4:

* X: Stefan nie chce nic z tym robić. Uważa, że jest dobrze. A PO MARCELU NA PEWNO NIE PÓJDZIE DO KLAUDIUSZA.
* Vz: Stefan się przyznał - FAKTYCZNIE prowadził po pijanemu, jego ukochana zginęła w wypadku. Z jego winy. Gdy był z nią i TAI neurosprzężony. Ma fantomy.
* V: Stefan NIGDY już nie prowadzi pod wpływem. Ale się maksymalnie znieczula gdy nie prowadzi. I to są ostre rzeczy. Cofa się do przeszłości.
* V: "Marcelina" ma fizyczną formę taką jak narzeczona Stefana i Stefan uważa to za ZNAK BOGÓW. Ma pozytywną reakcję.

## Streszczenie

Arianna włączyła ćwiczenia dla Królowej - prawie wyszło, ale ludzie popanikowali a statek odmówił posłuszeństwa i załoga zaczęła się bić. Arianna ich przećwiczyła w działaniach awaryjnych i oddała panikarza medykowi - niestety, Paradoks zmienił Marcela w Marcelinę... ale następnym razem ćwiczenia powinny się już udać. Daria ma sprawę pod kontrolą i Arianna też raczej jest pewna sukcesu.

## Progresja

* .

### Frakcji

* .

## Zasługi

* Arianna Verlen: zneutralizowała Maję Samszar ryzykiem konserw, rozpuściła plotki na swój temat (że kontratakuje i jej nie podpadać), zaczęła ćwiczenia z Królową i jak statek ruszył to zaczęła się panika. Arianna odwołała ćwiczenia... by przeszkolić ludzi w niepanikowaniu i działaniu sensownym.
* Daria Czarnewik: wprawnie zrestartowała Królową gdy pojawiła się awaria i przeszła na manualne. Królowa jest w podłym stanie, ale Daria krok po kroku ją doprowadza do działania.
* Maja Samszar: przekonana przez Ariannę że ma do wyboru - być posłuszną lub jeść konserwy. Maja to foodie. Robi ciasta. Kocha jedzenie. Arianna jej obiecała, że pomoże jej się zemścić na Romeo - ale Maja ma Ariannie nie przeszkadzać. Nieufna, ale zneutralizowana.
* Romeo Verlen: KIEDYŚ zniszczył jedzenie Mai i poderwał jej kuzynkę. A potem walczył nie fair. Więc Maja i kilka Samszarów ma na niego krechę i chcą go pojechać. Przez niego Arianna ma teraz problemy z Mają Samszar.
* Stefan Torkil: kiedyś przyjaciel Marcela; rozbił jednostkę po pijanemu i jego dziewczyna zginęła. Nie używa neurosprzężenia przez to. Ma dobrą reakcję na to, że Marcel wygląda jak jego była dziewczyna.
* Marcelina Trzęsiel: kiedyś przyjaciel Stefana; ma z nim historię. Przez Paradoks Klaudiusza przekształcony w dziewczynę...
* Rufus Warkoczyk: starszy mat, który stoi po stronie Arianny i chce porządku. Powiedział, że przez Marcela (teraz: Marcelinę) te wszystkie kłopoty.
* Erwin Pies: kapral przygotował mordercze ćwiczenia i przećwiczył załogę na działanie w warunkach stresowych i awaryjnych. Porządne 10 dni.
* OO Królowa Kosmicznej Chwały: ruszyła. Aktywna i w miarę sprawna, ale załoga nieprzeszkolona. Arianna przerwała ćwiczenia by wszystkich doprowadzić do zdolności działania.

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański

## Czas

* Opóźnienie: 2
* Dni: 12

## Konflikty

* 1 - Arianna próbuje PRZEKONAĆ Maję do posłuszeństwa. Maja jest oporna i ma swoje zdanie. Ale Arianna ma REPUTACJĘ.
    * Tr Z (REPUTACJA Arianny) +3
    * VrVVX: Maja wykona polecenie, nie będzie fikać Ariannie i się przyznała czemu zwalcza Ariannę - chodzi o Romeo. Nieufna ale już nie szkodliwa.
* 2 - Arianna zaczęła rozpuszczać plotki na swój temat - a dokładnie, że niezależnie jaki jest wektor ataku to Arianna od razu kontratakuje
    * Tr Z (historia + VERLEN + osoby wsparcia) +4
    * VVz: Plotka się pojawiła, jest wiarygodna, gloria rodu Verlen poszła po systemach (i ich absolutna "dewastacja")
* 3 - Stefan Torkil musi doprowadzić Królową skutecznie do Karsztarina.
    * Tr Z(naprawy i łaty Darii) +4
    * XVzXX: Semla odrzuciła integrację z pilotem, koszt paliwa wysoki, panika na pokładzie bo ciemność, ALE Królowa leci prawidłowo. Aż zgasła.
* 4 - Królowa nie zadziałała poprawnie - ruszyła, poleciała. Teraz DRYFUJECIE do przodu bez prądu bez energii bez silników. Daria reanimuje.
    * Tr +2:
    * VV: Królowa wróciła do działania, Daria zrestartowała Królową i przeszła na manualne. Ale Arianna odwołała ćwiczenia.
* 5 - Arianna robi wyładowanie magiczne - od włosów do wyładowania po ścianach i eksplozja energii głośna i niegrożna. PRZERWAĆ BITWĘ.
    * TpMZ+4+3Ob
    * Vr: Arianna przerywa walkę. 10 minut. GDZIE DYSCYPLINA?! Arianna dała im tydzień roboty na konserwach, ostre ćwiczenia.
* 6 - Arianna przesłuchuje Marcela - czemu sabotował silniki itp?
    * TpZ (plotki o Verlenach) +4
    * VVX: Marcel przysięga że się boi statku bo przeszłość. I nie da się go przesunąć, próbował. Jest coś między nim i pilotem.
* 7 - Klaudiusz psycholog długoterminowo pomaga Marcelowi. A Arianna wszystkich informuje co i jak by Marcela stabilizować:
    * Tr Z (bo ma go w łapkach) M (bo lol, jest magiem) +3 +3Ob
    * VrXXzOm: Marcel może funkcjonować dobrze na jednostce docelowo, ale SKONWERTOWAŁO GO W MARCELINĘ.
* 8 - Arianna ma kaprala Psa. Kapral Pies ma przygotować mordercze ćwiczenia. W wypadku awarii mają zachowywać się jak powinni. Ćwiczenia awaryjne.
    * Ex Z (doświadczenie Psa + Arianny) +3
    * Vz: grupa ma tylko lekkie rany. Udało im się przećwiczyć. Przetrwali nawet alarm o 3 rano że przebicie kadłuba, z niewielkimi stratami.
* 9 - Arianna poszła porozmawiać ze Stefanem. Bał się prowadzić Semlę / Królową. Arianna -> Stefan o alkoholu.
    * Tr Z (reputacja Arianny) +4
    * XVzVV: Stefan nie chce nic z tym robić; dalej się znieczula (ale nie gdy ma lecieć). "Marcelina" ma fizyczną formę jak dziewczyna Stefana. Ów ma pozytywną reakcję.
* 10 -
    * 
    * 

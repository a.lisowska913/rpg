---
layout: cybermagic-konspekt
title: "Krystaliczny gniew Elizy"
threads: legenda-arianny
gm: żółw
players: kić, kapsel, fox
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [200923 - Magiczna burza w Raju](200923-magiczna-burza-w-raju)

### Chronologiczna

* [200923 - Magiczna burza w Raju](200923-magiczna-burza-w-raju)

## Punkt zerowy

### Dark Past

_odsyłam do 200916_

### Opis sytuacji

Stan aktualny sytuacji:

* Koty Mariana ulegają roznoszą kryształy Elizy po Trzecim Raju. Eliza musi wiedzieć jak ochronić Raj.
* Ataienne jest w większości ślepa, choć ma coraz więcej czujników.

### Postacie

Frakcje: i reprezentanci

* Eliza Ira, królowa kryształów
  * chce: zniszczyć wpływ bogów, zdobyć Ataienne
  * ma: Marian Fartel, krystaloformy, moc arcymaga
* Zbigniew Morszczat / Miragent, źródło chaosu
  * chce: doprowadzić do kompromitacji i unieszkodliwienia Trzeciego Raju
  * ma: zainteresowanie nojrepów, miragent
* Robert Garwen, strażnik Raju
  * chce: by Raj przetrwał niezależnie od czegokolwiek
  * ma: przemyt, posłuch
* Celina Szilat, psychotroniczka Termii
  * chce: zobaczyć pełną moc Ataienne, usprawnić Persefony
  * ma: kontrolę nad Persefoną, skille terminuski, lekki servar klasy Jaeger (adv. Lancer).
* Bartosz Gamrak, Grzymościowiec z Wolnego Uśmiechu
  * chce: utrzymać Trzeci Raj, rynek zbytu Skażonych TAI
  * ma: przemyt, scrap-mechy, małą bazę w Ortis-Conticium
* Ataienne, mindwarp Raju
  * chce: odepchnąć Seilię, uratować Trzeci Raj, uwolnić się
  * ma: mindwarp
* Kordelia Sanatios, arcykapłanka Seilii
  * chce: postawić świątynię Seilii w Trzecim Raju
  * ma: zwolennicy, możliwość przetransportowania kasy
* Trzeciorajowcy nienoktiańscy, mieszkańcy Raju
  * chce: poszerzyć kontrolę nad okolicą, być bezpieczni
  * ma: większość populacji Trzeciego Raju
* Izabela Zarantel, dziennikarka Orbitera
  * chce: jak najlepszej opowieści do Sekretów Orbitera
  * ma: umiejętności zwiadu i dobre opowieści
* Anastazja Sowińska, niestabilny arcymag
  * chce: samodzielności, wolności i swobody.
  * ma: niesamowitą moc
* Dariusz Krantak, strażnik Anastazji
  * chce: zniszczyć Anastazję
  * ma: noktiańskich zniewolonych agentów

## Punkt zero

brak.

## Misja właściwa

Po burzy magicznej, pięć dni później. Mamy cztery statki:

* "Szalony Rumak", dowodzony przez Dariusza Krantaka, strażnika Anastazji
* "Wesoły Wieprzek", 'Tucznik 2.0', złożony przez adm. Termię i adm. Kramera by wesprzeć Ariannę
* humanitarny okręt z Aurum + kult Seilii
* humanitarny okręt z Orbitera

Admirał Termia zapisała, że "Wieprzek" jest dla Arianny, jeśli ta nie radzi sobie z "Tucznikiem". Jest prostszy w pilotażu, choć nieco mniejszy i mniej wygodny. Klaudia zauważyła, że jest możliwość przechwycenia Wieprzka do floty Arianny jeśli zrobi się odpowiednie manewry biurokratyczne. Całkiem nieźle.

Anastazja się bardzo ucieszyła z pojawienia się Dariusza. Przeprosiła za ucieczkę i wszystkie problemy. Dariusz potraktował jej wybuch emocji ze stoickim spokojem.

Elena wezwała Klaudię i Eustachego do spojrzenia na jedną rzecz która jej się pojawiła. Niedaleko holoprojektora sonicznego Ataienne znajduje się dziwny kryształ. Zdaniem Eustachego jest podłożony tak, jak podłożony by był ładunek wybuchowy; to jest niefajne. Szybki skan po czujnikach Ataienne i jej nagraniach pokazał, że dziwne kryształy są roznoszone przez... koty. CZYLI MARIAN FARTEL JEST SABOTAŻYSTĄ?! Moment, to nie takie proste. Koty noszą "zwykłe" kryształy a to jest jakaś struktura kryształy + mechanizmy.

Klaudia skupiła się na krystalicznej strukturze i użyła mocy magicznych - co to do cholery jest? (V): jeśli Ataienne użyje większej mocy to holoprojektor eksploduje; wybuch idzie na linii magicznej i fizycznej. (V): to jest jakaś amalgamacja kryształów Elizy i technologii Orbitera, więc to nie jest czyste działanie Elizy. (XV): Eliza wie, że oni to zbadali, toteż: sabotażysta. Ale mają możliwość odcięcia tego 'remote explosion'. (V): mają lokalizację tych wszystkich bomb - są rozmieszczone w Trzecim Raju w sposób, który zrani i zabije sporo mieszkańców tej enklawy.

Eustachy zabrał się do błyskawicznej pracy. (VVV): udało mu się porozbrajać te bomby zdalnie, blokując wszystkie sygnały. (X): kilku się nie udało i weszły w tryb aktywny; te trzeba było ręcznie zdetonować. Ale mają dowody, że ów sabotażysta nie liczy się z niczyim życiem.

Czas przesłuchać naszego medyka - czy on jest sabotażystą? Marian Fartel, przesłuchiwany na pastwisku glukszwajnów, w "namiocie dowodzenia". Nie ma lepszego miejsca.

Zapytany, Marian odparł, że koty używa symbiotycznie; on ich nie kontroluje, one z nim współpracują. Arianna nie bawiła się w detale - nie ma na to czasu ani możliwości. Użyła swojej mocy wspomagana przez Klaudię by wejść w głowę Mariana - czy on NAPRAWDĘ jest sabotażystą, czy nie? (V): ma dowód - koty roznosiły kryształy i Fartel to wie; on miał kontakt z Elizą Irą. (X): niestety, Eliza WIE że Arianna dowiedziała się o Fartelu. (V): Arianna ma dowód, że to co roznosiły koty to INNE kryształy; Fartel wierzy, że to kryształy rozpraszające energie anomaliczne i stabilizujące Raj (a przy okazji - osłabiające moc Ataienne). Arianna doszła też do tego, że Marian Fartel pamięta WSZYSTKO z czasów Noctis; Eliza Ira go deprogramowała i naprawiła wpływ Ataienne.

Wow. To jest coś dużego. Co z tym robimy? Arianna zdecydowała się... puścić go wolno. Nie robi krzywdy, nie robi szkód a Arianna **chce** mieć załogę noktiańską. To pomoże, jako gest dobrej woli.

SYGNAŁ SOS OD ELENY MIROKIN! Jest atakowana! To, co się stało naprawdę (czego zespół nie wie): Dariusz przybył z grupą mindwarpowanych komandosów noktiańskich; ich celem było zabić Anastazję i zrzucić winę na noktian. Jeden z nich, snajper, strzelił pierwszy - ale Elena ma absolutną kontrolę otoczenia. Odsunęła Anastazję z pola ognia (i została ranna; pocisk przeszył Elenę i Anastazji ramię), po czym włączyła swoją naturalną magię, która zaczęła tworzyć pola siłowe i zakłócacze. Niestety, wrogi mag i strzelcy noktiańscy są ZA skuteczni dla Eleny. Elena umrze...

TO JEST, NIE! NIE MA ZGODY! Eustachy BŁYSKAWICZNIE ściąga swoją dalekosiężną wyrzutnię rakiet klasy DevaMirv i wysyła sygnał Elenie - niech ona włączy wszystkie pola. I robi barrage fire; chce cały teren gdzie jest Elena pokryć eksplozjami. Zrzucić z Eleny przeciwników. BLACKEN THE SUN WITH EXPLOSIONS!

Eustachy strzelił bardzo niebezpieczną, niekierowaną wiązką: (OO): Eliza Ira wie o tym ataku i wyczuła katastrofalną sytuację zarówno mindwarpowanych noktian jak i Eleny Verlen i Anastazji. (XXX): wszyscy ciężko poranieni; Elena w stanie bardzo ciężkim, Anastazja w panice, ale Elena ją otacza polem siłowym i swoim ciałem, fortyfikując jak może. (V): Eustachy kupił czas Ariannie; wrogowie pod barrage są zmuszeni do uników a nie dobicia Eleny i Anastazji; (X): wrogi mag nie żyje od pocisku; Elena ma sporo ciężkich ran. Wyjęta z akcji przez dłuższy czas.

Arianna jest arcymagiem, jej nie dotyczą podstawowe zasady magii. Wpada tam potężnym teleportem i neutralizuje wszystkich oplątując ich kablami (VVO). Eustachy, Klaudia i Marian od kotów tu biegną. Arianna czuje, że zwiększa się poziom mocy magicznej; niedługo pojawi się tu brama. Eliza Ira nadchodzi. Arianna widzi, że Eliza lokuje na "oprychach" (komandosach noktiańskich); przyzywa stateczek Eleny, wrzuca wszystkich w stateczek i leci stąd jak najdalej. Odciągnąć Elizę Irę.

(VVX): Ariannie się udało odciągnąć Elizę; musiała wziąć zwłoki i oprychów (strasznie mało miejsca w tym ścigaczu XD) ale zapewniła też opcję ewakuacji. Wylądowała, zwłoki maga pomiędzy nią i bramą. A z bramy wychodzi Eliza Ira. Eliza jest NIŻSZA, niż Arianna się spodziewała, ale jej ciało jest częściowo ludzkie, częściowo krystaliczne. Arianna poznaje objawy transferu ixiackiego, chyba. Są 2 km od pola, gdzie Elena się wykrwawia.

Anastazja zaczyna wchodić w fazę paniki prowadzącą do Paradoksu. Klaudia (jedyna osoba pod ręką) zaczyna ją stabilizować - nie udaje się łatwo (X), więc ściska za ranną rękę by wzmocnić ból i sfokusować arystokratkę. Krzyk Anastazji ściąga Dariusza (O), ale Klaudii udało się Anastazję uspokoić. Anastazja jest przekonana, że Elena umrze - mimo wsparcia Mariana. Dariusz chciał odgonić Klaudię, ale Eustachy kazał mu się odsmerfować od Anastazji, Klaudii i Eleny. Dariusz odsunął się na bok i odpalił fazę drugą - drona z rakietą.

Tymczasem Eliza Ira i Arianna. Eliza patrzy na zwłoki na ziemi i patrzy na Ariannę. Arianna wyczuwa, że lodowa nienawiść w Elizie zaczyna się budzić ogniem. Eliza cichym głosem informuje Ariannę, że oni pochodzą z Noctis; ich umysły zostały zniszczone przez magów i katów astoriańskich. Teraz Arianna się nieco martwi - te zwłoki między nią i Elizą miały pokazać "hej, złapałam i zdjęłam ich" a wyszło tak trochę głupio. Arianna zaproponowała Elizie, że wypuści tych innych komandosów noktiańskich; Eliza stwierdziła, że to dobry pomysł.

Eliza postawiła Ariannie ultimatum - mają 48h na przygotowanie WSZYSTKICH noktian z Trzeciego Raju na ewakuację. Jeśli to się nie stanie, Eliza zniszczy wszystkie te statki tutaj (wzięła ze sobą krystaliczny tesserakt, golema pożerającego dusze). Dodatkowo Eliza dostanie od Arianny osobę odpowiedzialną za śmierć maga. Chce dodatkowo Anastazję wziąć ze sobą jako zakładniczkę, że nie będą strzelać w noktian z orbity. Arianna zorientowała się ku wielkiemu swemu zaskoczeniu, że Eliza nie wie że Arianna jest arcymagiem!

A tymczasem monitorujący sytuację Eustachy zobaczył błysk na niebie. Rakieta. Klaudia szybko zrobiła echo magiczne Anastazji i przesunęła ją 50 metrów w prawo; rakieta nie trafiła w Anastazję i detonowała tam (XVV) - niestety, musiała użyć krwi Anastazji i ta ucierpiała BARDZIEJ. Brak Eleny, cierpienie, krew i konieczność opanowania się - Anastazja jest na granicy. Eustachy natomiast wystrzelił do drony, rzucając dalekosiężny czar przez swojego DevaMirva; małe drony wystrzelone pociskiem kasetowym które mają dostać się do TAMTEJ drony. I dzięki temu Eustachy mógł śledzić ową dronę, gdzie ląduje i gdzie leci. (ExZM: VOX). Drona jest z "Szalonego Rumaka" a osoby odpowiedzialne są za wysokiej rangi by Dariusz nic nie wiedział.

Miłość do Eleny (lub Eleny) i silne emocje sprawiły, że Eustachy się wściekł. Odwrócił się w kierunku Dariusza i odstrzelił mu nogę. Dariusz na ziemi z krzykiem. Świat Anastazji się rozsypał. I Klaudia, rozpaczliwie zaczęła tłumaczyć Anastazji, że to był przypadek; broń sama wypaliła. Anastazja w to uwierzyła **bo chciała uwierzyć**, jej rzeczywistość po prostu się rozpadła. Klaudia ewakuowała Mariana, Elenę i Anastazję na pokład Tucznika, by tam mogli wyleczyć swoje rany a Eustachy zawlókł Dariusza do namiotu dowodzenia; tam go przesłucha solidnie.

I na to wróciła Arianna, która natychmiast oddała Dariusza Ataienne. Niech Ataienne sprawi, że Dariusz powie prawdę - wszystko co wie o zniewolonych noktianach, miejsce gdzie można ich uwolnić itp. oraz niech wyzna Anastazji wszystko. Ataienne użyła swojej mocy hipnomuzycznej, ku szokowi i fascynacji Klaudii - to jest cudowne, ta anomaliczna moc.

Dariusz dostał straszliwe rany psychiczne - był szkolony i zbudowany by odpierać tego typu ataki, ale Ataienne jest niewiarygodnie silniejsza (X). Ataienne wydobyła z niego, że to on jest mastermindem (V). Owszem, Napoleon i Marianna gardzą Anastazją, ale to on chciał ją zabić. Oni - nie, choć nie płakaliby po niej. Efektem ubocznym działań Ataienne jest to, że Dariusz niczego nie ukryje przed Anastazją, zada jej STRASZLIWĄ ranę psychiczną, bo Anastazja zrozumie że wszystko w co wierzyła i komu ufała jest fałszem (X). Ataienne dla Arianny wydobyła jeszcze informacje o miejscach w majątku Sowińskich gdzie można uratować noktian (V) oraz info i dowody o innych spiskach przeciw Anastazji; coś do udobruchania dziadka Anastazji (V).

Po tych rewelacjach Anastazja się zamknęła w sobie. Została złamana. Jej trauma odeszła, ale pojawiła się nowa. Anastazja rozumie, że nikomu poza dziadkiem nigdy na niej nie zależało. No, teraz Elenie - ale Elena na pewno umrze.

## Streszczenie

Pomoc humanitarna przybyła do Trzeciego Raju wraz z zabójcami Anastazji. Mindwarpowani komandosi noktiańscy prawie zabili Elenę, ale Anastazja została ochroniona. Do akcji weszła Eliza Ira - zażądała oddania jej Trzeciego Raju i noktian. Zespół przygotowuje się do nie-oddania Raju i uratowania jak najwięcej noktian z Aurum (jakkolwiek to nie brzmi).

## Progresja

* Elena Verlen: bardzo ciężko ranna, przepalona i bez cienia kontroli energii. Co najmniej miesiąc bardzo ograniczonej aktywności.
* Anastazja Sowińska: jej świat się rozsypał. Tylko dziadkowi na niej zależy (chyba), Elena chroniąc ją prawie umarła i po raz pierwszy ever została RANNA. ZABÓJCY!
* Anastazja Sowińska: już nie ma traumy "nie może zostać sama". Wyleczyła się. Zamknęła się w sobie.

### Frakcji

* .

## Zasługi

* Arianna Verlen: wpierw teleportowała się by uratować Elenę przed zabójcami Anastazji, potem stała naprzeciw Elizy Iry i próbowała sprawę załagodzić. Ciężki okres.
* Klaudia Stryk: wpierw badała kryształowe bomby "Elizy", potem uspokajała i stabilizowała Anastazję (gdy ta panikowała) i skończyła na przekierowaniu rakiety gdzieś na pole.
* Eustachy Korkoran: rozbroił kaskadowo bomby, po czym gdy Elena wysłała SOS kupił czas Ariannie - robiąc carpet bombing terenu z Eleną. Poranił ją, ale uratował przed śmiercią. Też strzelił Dariuszowi w nogę (odstrzeliwując ją) za cierpienie Eleny.
* Dariusz Krantak: ochroniarz Anastazji któremu ufała i który chciał ją zabić; mastermind, używający zniewolonych noktian. Oddany Ataienne by wydobyć zeń prawdę.
* Elena Verlen: alias Mirokin; gdy zabójcy próbowali zabić Anastazję, oddała sporo zdrowia i użyła pełnej mocy (łącznie z Esuriit) by ratować i osłaniać Anastazję. Prawie umarła.
* Anastazja Sowińska: Elena poświęciła sporo zdrowia, by ją uratować przed zabójcami jej przyjaciela, ochroniarza. Sama jest ranna. Jej świat się rozsypał.
* Marian Fartel: noktiański lekarz używający symbiotycznych kotów, współpracujący z Elizą Irą by ratować Trzeci Raj. Pamięta przeszłość - a jednak chce współpracy.
* Eliza Ira: weszła do Trzeciego Raju po śmierci mindwarpowanych komandosów noktiańskich; zażądała oddania jej wszystkich noktian. Jak nie - wszyscy poza noktianami zginą.
* Aleksandra Termia: zrobiła 'złośliwy, przyjacielski psikus' Ariannie Verlen i przekazała jej do dyspozycji statek "Wesoły Wieprzek".
* Ataienne: widząc jak Dariusz potraktował Anastazję Sowińską (jej krewną), użyła swej mocy na życzenie Arianny by go zmusić do przyznania się.
* OO Wesoły Wieprzek: okręt Orbitera, jak Galaktyczny Tucznik (tylko prostszy w pilotażu i mniejszy). Przekazany Ariannie Verlen do dyspozycji na czas świńskich problemów.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski, NW
                    1. Ruiniec
                        1. Trzeci Raj: Eliza Ira zażądała oddania noktian, doszło do ostrej walki z komandosami-zabójcami Anastazji.

## Czas

* Opóźnienie: 5
* Dni: 3

## Inne

**AMEND: dodajmy Iok-sama, szlachcica który z oddziałem kilkudziesięcioosobowym spróbuje uratować Trzeci Raj przed Elizą Irą.** -> do przyszłej sesji.

Planowana sesja:

* Zespół ląduje Galaktycznym Tucznikiem przy Trzecim Raju
* Zespół ma do czynienia z małym atakiem nojrepów. Widzi w jak złym stanie jest Raj.
* Zespół troszkę im tam pomaga, wykrywa przemyt Tucznika Trzeciego.
* Ataienne nie pomaga, a powinna - jej czujniki nie działają

* Ok, niezależnie od okoliczności - świnie na statek. Lecimy na orbitę.
* Na pokładzie kaledik + 24 glukszwajny. Walka z kaledikiem na statku kosmicznym. NAJPEWNIEJ nie ma crashland.
* Kramer żąda, by Zespół jak najszybciej doszedł do tego co i jak. Muszą unieszkodliwić Celinę i mają pomoc Ataienne.
* Miragent podrzuca kryształy wskazujące na Elizę, ale Klaudia bez problemu dojdzie do tego, że to NIE ONA.
* Znaleźć miragenta. Miragent ma rozkaz śmierci. Ataienne rekomenduje Zespołowi, by zostawili jej dyskretnie miragenta.

---
layout: cybermagic-konspekt
title: "Nocna Krypta i Bohaterka"
threads: legenda-arianny
gm: żółw
players: onyks, tomek_taktyk, czarny, markus
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [221221 - Astralna Flara i nowy komodor](221221-astralna-flara-i-nowy-komodor)

### Chronologiczna

* [191025 - Nocna Krypta i bohaterka](191025-nocna-krypta-i-bohaterka)

## Budowa sesji

### Stan aktualny

* .

### Pytania

1. .

### Wizja

brak

### Tory

* .

Sceny:

* .

## Punkt zerowy

### Postacie

### Opis sytuacji

Statek-relikt, nie pragnący kapitana. Kapitan, pragnący ocalić statek-relikt.

Arianna Verlen, bohaterka i wybitna techno-czarodziejka. Jednocześnie – dowódca Waszej ekspedycji. Weszliście na pokład Nocnej Krypty – zrujnowanego, rozpadającego się statku kosmicznego, grobowca z ogromną ilością wiedzy. Chwilowo tylko heroiczna siła magii Arianny utrzymuje Nocną Kryptę w jednym kawałku.

Próbowaliście przejąć Kryptę pod kontrolę Arianny oraz wesprzeć jej działania. W którymś momencie zaczęły się wypadki. Część załogi chce wracać, ale Arianna się nie zgadza – zwycięstwo jest tak blisko. Pytanie, czy ona – albo dowolny inny załogant – dotrwa tego zwycięstwa.

Gracie:

* Badaczami, naukowcami, historykami, załogantami dobrze znającymi Ariannę i którym ufa
* Najprawdopodobniej nie macie szczególnych umiejętności bojowych; nie są Wam potrzebne

Jeśli Wy przegracie:

* Arianna faktycznie zostanie kapitanem Nocnej Krypty. Raczej tego nie przeżyje. Wy też nie.

## Misja właściwa

### Prolog

Several months ago.

The Pearl has landed. Technically, she has docked on to a damaged spaceship. The spaceship had no power inside; one of the crew connected auxiliary power from the Pearl on to the damaged ship.

There were lying people in the corridor, wounded and bleeding. The whole boarded ship was the power and something was wrong. Electricity crackled in the air. Electricity – or something worse, like magic. A member of the crew accessed the logs to understand what has happened to the ship while someone else tried to evacuate hurt people and help them in the medical bay of the Pearl. 

The logs showed the result – there has been a breach in the magitech reactor. It has been contained by the spaceship, but the critical mass of humans on board were irradiated by magic. This means no one controlled the amplitude of magic, there were no dampeners, and with the radiation protector screen disabled, the spaceship has been hit by an amplitude of magic. And everything went to hell.

The crew of the Pearl started helping whomever possible in any way possible. Arianna with the crew entered the bridge – one man was sitting there, focusing on the here and now. He was repeating endlessly his name, station and a number. A single mage tried to contain this whole energy to stop the spaceship from drifting into unreality. With the help of medical crew and with help of a person using drones to send the signal, propagated and make sure all the people around feel the same way – it was possible to save the mage. This spaceship has been saved. Arianna has been a hero again. Thanks to her crew, again.

### Czas dzisiejszy

A hero’s job is never finished. When the long-distance detectors have found a derelict, dead ship „Nocna Krypta” which once used to be a medical noktian ship „Alivia Nocturna”, Arianna has decided to take her crew, board the derelict and return it to Orbiter. Bringing with them all potential survivors and all potential knowledge from Noctis.

So, they went – the Pearl has left for the derelict. An initial connection of the Pearl to the Crypt was of no problem; however after sending the drones have noticed several complications connected to this act. First of all, the reactor was completely busted. Second, people in biovats were still asleep - they were alive, but barely. And third, there is an altar which is being worshiped by the robots or so it seems. This is the most unusual thing ever.

They have access the memory database of the Crypt and started working on the reactor in the meantime. The reactor was possible to be saved and stabilized, but the Crypt started falling apart. Arianna has used all her powers to keep the ship in one piece. The power of an archmage. Her crew truly believed she is still one be able to save both herself, and everyone else.

In the meantime doing the work on the reactor several crew members got badly hurt. Magi from the crew tried to get them to the Pearl, but local robots tried to put those people in the biovats. A trick was needed – a spell masked the signature of the wounded people as inert pieces of metal and some food rations got the signatures of crewmembers. The robots started putting the food rations into biovats; this was quite creepy, because the robots acted as if they were hunting, not as slaved machines to the AI of the ship.

The logs have shown something quite unnerving – the Crypt has had their medical officer, Helena, sacrifice herself using a blood magic ritual. The altar was designed by the robots for her. Helen has managed to inhabit the ship as an echo, a ghost. But this ship has managed to save everyone infected by the plague. It seems the ship has a very singular purpose – save every human life, preserve them at any cost. Unfortunate. It might be hard to take those people out of Biovats on to the Pearl and thus to save them.

In the same time some types of accidents started happening, injuring people left and right. It looked like as if the ship started to react to the presence of Arianna and the crew and tried to push them away, to hurt them actively. Arianna herself started talking about the past people rather than the present crew – he addressed one of the crewmembers as Mikhail (who used to be a political officer on board of medical ship, but not Pearl!). So her senses started to blur.

She used to be a person, but because of the amount of energy which is being channeled through her and by her, her natural defenses stopped working and she was possible to be possessed by some entities – and taking her off the bridge right now would collapse the Crypt, killing everyone and anyone over there right now.

A crewmember specializing in using drones tried to communicate with the ship itself. The ship has pierced through one of the humans on board and that person got connected to one of the robots; that is when the ship was able to speak. It introduced himself as BIA Klath; a biological autonomous intellect controlling the Crypt. Klath explained, that Helen and him are fighting against intrusion on Crypt systems. One of them did something wrong - something which awakened Helen and made her believe they are enemies.

A quick scan through logs showed, that Wojciech tried to delete some formations located inside the crypt – information about his family being war criminals. Destruction of information was stopped by Klath, but it was enough for both him and Helen to assume they are the enemies.

Arianna’s crew has managed to persuade Klath that is this what is happening right now is not life. They want to save all those people, take them out of the biovats and return them to the normal human lives. Klath noticed, that this will destroy human dampeners and the Crypt will probably be destroyed. BIA agreed to do that anyway. From his point of view, Crypt was finished anyway – the medical ship has served its purpose.

To be able to evacuate everyone, though, they needed to make sure the Crypt will be busy. A single rocket launcher shot on to the reactor was enough to buy time especially with Klath sabotaging Helen’s efforts. In the meantime, they have taken all the biovats and evacuated them using their own robots - battling against Helen controlled robots. Arianna had a lot of medical help not to become Olivia (former captain of the Crypt).

All in all, they have managed to evacuate everything from the ship. They were safe. The Crypt was hit by a magical amplitude and had disappeared - Little did they know that the crypt became a vicinius, with Helen’s mind.

**Sprawdzenie Torów** ():

* .

**Epilog**:

* .

## Streszczenie

Arianna Verlen - bohaterka Orbitera. Zdecydowała się uratować osoby złapane przez Nocną Kryptę i wydobyć je, oraz zdobyć całość wiedzy noktiańskiej. Prawie zginęła; dzięki swojej załodze udało jej się wyjść ze stanu arcymaga, nie została opętana przez kapitan Oliwię z Krypty oraz udało im się wszystkim ujść z życiem. Kosztem przekształcenia Nocnej Krypty w niebezpieczny statek anomalny...

## Progresja

* AK Nocna Krypta: stała się anomalicznym statkiem-widmem sterowanym przez zespolone umysły BIA Klath oraz Heleny. Poszukuje wiecznie ofiar które może wyleczyć w ciszy kosmosu...

### Frakcji

* .

## Zasługi

* AK Nocna Krypta: Noktiański statek medyczny, który stał się anomalią. Kontrolowany przez zwalczające się umysły BIA Klath oraz oficera medycznego, Heleny. Kiedyś "Alivia Nocturna". Statek stał się własną frakcją.
* Arianna Verlen: oficer Orbitera, bohaterka, która wielokrotnie uratowała wiele statków i stacji. Podjęła się misji uratowania ludzi z Nocnej Krypty; przeszła przez fazę Arcymaga i prawie straciła życie i osobowość, ale udało jej się osiągnąć ten sukces dzięki swojej załodze. Dowodzi statkiem Orbitera "Perła Nadziei".
* Wojciech Kuszar: oficer Orbitera podlegający Ariannie Verlen, którego rodzina dokonała zbrodni wojennych (zarejestrowane w "Nocnej Krypcie"). To jego działania obudziły Helenę na pokładzie Krypty i, fundamentalnie, doprowadziły prawie do zniszczenia całej załogi Orbitera próbującej uratować ocalałych na Krypcie.
* Kamil Lyraczek: kaznodzieja ludzki, wierzący w Ariannę jak w boginię. Przez to przesuwa ją do poziomu Arcymaga, ale też ryzykuje życiem załogi oraz samej Arianny. Oficialnie, medyk.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Kosmiczna Pustka: w miejscu gdzie nie ma wielu statków kosmicznych (i w sumie niczego) udało się zlokalizować Orbiterowi Nocną Kryptę. Arianna z oddziałem uratowała ocalałych z Krypty, ale sama Krypta zmieniła się w wieczną anomalię.

## Czas

* Chronologia: Aktualna chronologia
* Opóźnienie: -444
* Dni: 2

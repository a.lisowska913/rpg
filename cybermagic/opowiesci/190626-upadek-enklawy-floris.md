---
layout: cybermagic-konspekt
title: "Upadek enklawy Floris"
threads: nemesis-pieknotki
gm: żółw
players: kić, kapsel
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [190623 - Noc Kajrata](190623-noc-kajrata)

### Chronologiczna

* [190623 - Noc Kajrata](190623-noc-kajrata)

## Budowa sesji

### Stan aktualny

* .

### Pytania

1. 

### Wizja

* Po ataku na Pięknotkę Pustogor spojrzał na Enklawy jeszcze raz, ostrzejszym okiem. Idąc po śladach energii dotarli do enklawy Floris
* Dyskretny atak Marcela Sowińskiego ma za zadanie złapać jeńców i dowiedzieć się co i jak się dzieje w Enklawach
* Nikola ostrzegła Enklawę przed zbliżającym się atakiem; skąd wie, nie wiadomo
* Trudne decyzje - co robimy dalej?

### Tory

* .

Sceny:

* .

## Punkt zerowy

### Postacie

### Opis sytuacji

#### Siły ofensywne

* Marcel Sowiński, "młot Pustogoru": (Tr) S KS KS KS (ranny, wycofał się, krytycznie, nie żyje)
* Alan Bartozol, servar Fulmen: (Tr) S KS KS KS (ranny, wycofał się, krytycznie, nie żyje)
* Ataienne: (Tp) S S KS (uszkodzona, niezdolna, zniszczona/haywire)
* Skrzydło terminusów: (Tr) S, S, KS, KS (poranieni, ofiara, rozbici, martwi)
* Dwa skrzydła konstruminusów: (Tp) S, S, KS, KS (pouszkadzani, ofiary, rozbici, martwi)
* Orła Cień, stealth avian: (Tr) S: uszkodzony, S: uwięziony, KS: eksplozja, KS: ofiary

#### Enklawa

Osoby:

* 14 lekko uzbrojonych Pionierów (3 bandytów, 2 noktian)
* 11 ludzi
* 4 rannych
* 3 dzieci

Infrastruktura:

* Brama
* Systemy obronne
* Kryjówki i zasieki
* Generatory

#### Czas dotarcia do celu

* Tor o długości 4 (15 pktów akcji)

#### Okolice

* 

## Misja właściwa

Do Enklawy Floris przybyła Nikola Kirys. Poinformowała zmrożoną Radę Starszych, że Pustogor jest zainteresowany zaatakowaniem Enklawy. W Enklawie pojawiła się złowroga cisza - jest to najgorsza możliwa wiadomość. Hubert zaproponował poddanie się, Ariela zaproponowała walkę do końca. Roman - wysadzenie Bramy. A Zespół... nie chce walczyć z Pustogorem. Muszą uciekać; walka to najgorsza możliwa opcja.

Zespół (zwłaszcza Szymon) przekonał tych, którzy chcą zostać (m.in. Huberta) by zostali z Pustogorem (Tp:S) by spowolnić poszukiwanie przez Pustogor resztę ekipy. Zostaje z Hubertem 2 magów, 4 ludzi, wszystkie dzieci, wszyscy chorzy. Kupują czas i spowalniają - Hubert, 2 magów, 4 ludzi, 4 dzieci i 2 chorych. Zespół ma ze sobą Romana, Arielę, 5 magów i 7 ludzi. Całkiem nieźle. Nikola zgłosiła się na ochotniczkę by zapewnić bezpieczeństwo Hubertowi i ekipie.

Ariela wkręciła Huberta, że chcą wysadzić bramę. Że zniszczy wszystkich w Pustogorze. W ten sposób gdy Pustogor przesłucha Huberta to mają spowolnienie (taktycznie Marcel przegrał). Ale dzięki temu Pustogor będzie jeszcze bardziej zmotywowany by dowiedzieć się o co chodzi z tamtym obszarem (szukają dalej, przeczesują Enklawy; no i skąd biorą się Pajęczaki?!)

Zespół nie pozwolił im na to, by się pożegnali ze sobą - trzeba gnać. Pionierzy przywykli do takiego życia.

Wpierw - znaleźć coś w okolicach Wiecznej Maszyny co można wykorzystać. Może do Bramy, może do środków transportu? Wszyscy zostają w Enklawie by zabrać konieczne rzeczy, Ariela i Roman przesterowują Bramę a Szymon, Jolanta i technomanta Konrad idą do Wiecznej Maszyny. Szymon przeprowadza Jolantę i Konrada technomantę do Wiecznej Maszyny. (Tp: S). Konrad próbuje przejąć nad wyprodukowanymi czterema Pajęczakami kontrolę i je sprząc; pomaga w tym Jolanta dronami (TrMZ: SSM); Konrad stał się częścią ekosystemu. Maszyna go wpięła w system.

Przy okazji znaleźli maga rodu Ira kontrolującego Maszynę. Samo to jest interesujące - skąd tu mag i to tak bardzo Skażony i w nie do końca stabilnym stanie umysłowym?

Szymon używa magii by wzmocnić biologicznie Konrada; niech pomoże przedostać Pajęczaki, niech uda się porwać maga rodu Ira i odpiąć go od Maszyny. Lojalność Jolanty do Enklawy jest wyższa niż do ewentualnych planów noktian. (TrM: KSM). Okazało się, że ów mag - mający matrycę Ira - jest tu by fabrykować rzeczy dla Enklaw, po tej stronie. Pajęczaki, ale też działo mające uwolnić Ataienne i przesunąć ją na Alicję Sowińską. Działa na zlecenie Ernesta Kajrata.

Przy okazji magią Konrad stał się technorganikiem. Nie jest już magiem, jest viciniusem władającym mocą i się z tym bardzo źle czuje - ale ma dostęp do Maszyny i Maszyna nie zrobi mu krzywdy. Dopóki tym jest czym jest, Enklawa może się chować w okolicy Maszyny. Taki więc mają plan - Pustogor W ŻYCIU nie pomyśli by ich przy Maszynie szukać.

Przylecieli do Enklawy Pajęczakami. Załadowali i odlatują - a Pustogor jest już na wysokości Huberta. Tak więc - wysadzili Bramę by maskować sygnał. Uciekli; ale musieli zostawić za sobą jeden lub dwa Pajęczaki na zniszczenie. Pustogor to zauważył i jedynie powiązał tą Enklawę Floris z atakiem na Pięknotkę... niestety.

Teraz upozorowanie zgonu - okazuje się, że w wyniku Skażonego czaru jeden z hunter-killerów Maszyny stał się "jak jeden z nich". Niech zaatakuje Pustogor; może Pustogor uzna, że wszyscy z Floris zginęli, co najmniej na pewien czas. A oni na tydzień skryją się w Maszynie; nie mogą na długo, to miejsce jest zbyt Skażone.

Skontaktował się z nimi Ernest Kajrat. Powiedział, że jego wolą jest wspieranie Enklaw a oni mają jedyny sensowny fabrykator. Będą mu pomagać, nie mają wyjścia.

**Sprawdzenie Torów** ():

* skomplikowane.

**Epilog**:

* .

## Streszczenie

W odpowiedzi na atak na Pięknotkę, Pustogor wysłał oddział by dowiedzieć się co się dzieje w Enklawach, mający porwać Enklawę Floris. Floris jednak się rozdzieliło - część osób poszła się sama poddać, inni zdecydowali się dalej chować. Poszli do Wiecznej Maszyny i tam znaleźli tymczasową bazę przez posiadanie jednego Skażonego maga (którego Maszyna akceptuje). Jest tam też mag Kajrata zdolny do fabrykacji - źródło Pajęczaków. Połączyli siły i zamaskowali swoje ślady. Pustogor został z niczym, acz z ~10 osobami z Floris, więc... sukces?

## Progresja

* .

### Frakcji

* .

## Zasługi

* Szymon Maszczor: kiedyś detektyw, wierzył, został banitą po spotkaniu z biurokracją. Ważny we Floris. Zwiadowca i jednostka pozyskująca rzeczy 
* Jolanta Teresis: noktianka; władczyni roju + lekarz. Ważna w Floris. Dzięki niej udało się przetrwać w Maszynie, doprowadziła Konrada do działania i pomaga z technomancją.
* Ariela Sirmin: astorianka z Floris, bardzo zaciekła na walkę z Pustogorem; nie chce przegrać. Bardziej zależy jej na Floris niż na tym, by wygrać z Pustogorem. Nie tak krwiożercza jak się zachowuje.
* Hubert Kraborów: kiedyś bandyta, dziś ma rodzinę. Oddał się w ręce Pustogoru; chce żyć normalnie. Mag. Spowolnił Pustogor by pozwolić innym z Floris na ucieczkę. Wierzy, że oni zginęli.
* Roman Rymtusz: starszy katalista Floris, chciał odejść Bramą z Astorii; niestety, Pustogor do tego nie dopuścił. W radzie starszych Floris.
* Konrad Czukajczek: technomanta Floris, który stał się techorganicznym Skażeńcem ignorowanym przez Maszynę i wspieranym przez nią. Nieszczęśliwy jak cholera z tego powodu.
* Wargun Ira: nie pamięta już do końca kim jest, ale ten mag rodu Ira jest Skażeńcem zdolnym do częściowego kontrolowania fabrykatora Maszyny. Powiązany z Kajratem i Floris.
* Ernest Kajrat: wzmacnia Enklawy pod szyldem Błękitnego Nieba; zapewnił sobie wsparcie Floris z fabrykatorem Wiecznej Maszyny.
* Marcel Sowiński: dowódca sił bojowych Pustogoru mających przechwycić i pojmać Enklawę Floris. Pojmał tylko tych, którzy sami chcieli do Pustogoru, ale nie ma dowodów że Floris istnieje.
* Nikola Kirys: nadal konsekwentnie pomaga Enklawom; jej serce widocznie jest po tej stronie a nie po stronie cywilizacji i Pustogoru. Ostrzegła o ataku plus zapewniła ochronę Hubertowi.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski, SW
                    1. Granica Anomalii
                        1. Las Pusty, okolice: tu znajdowała się Enklawa Floris i tu Ariela wysadziła Bramę by ukryć ich obecność
                        1. Wieczna Maszyna, okolice: najskuteczniejszy fabrykator; tu powstają Pajęczaki rękami Warguna i tymczasowo tu ukrywają się magowie Floris

## Czas

* Opóźnienie: 3
* Dni: 3

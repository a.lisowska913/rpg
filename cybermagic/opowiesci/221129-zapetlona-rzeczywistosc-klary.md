---
layout: cybermagic-konspekt
title: "Zapętlona rzeczywistość Klary"
threads: 
gm: anadia
players: żółw, kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [221129 - Zapętlona rzeczywistość Klary](221214-zapetlona-rzeczywistosc-klary)

### Chronologiczna

* [221129 - Zapętlona rzeczywistość Klary](221214-zapetlona-rzeczywistosc-klary)

## Plan sesji

unknown

### Wydarzenia przedsesjowe

Uczniowie szkoły średniej, z bogatszych rodzin (nie pracują po szkole), ustawiane małżeństwa. I mamy za siebie wyjść. Po 17 lat. Mamy pieniądze, zasoby, służbę, ale to nie NASZE zasoby tylko zasoby RODZICÓW.

* Kić: Klara
    * problemy z sercem. Nie możesz mieć wysiłku, delikatna, może to irytuje narzeczonego.
    * (Anadia) -> ma deja vu, przerwy w pamięci, coś jej się wydaje
    * dookoła niej jest sporo koleżanek. Jest uczynna, chce pomagać.
* Żółw: Henryk Park
    * zmuszony do małżeństwa i niezadowolony i irytuje mnie narzeczona. Daleko od niej.
    * przejęcie biznesu ("należy się jak psu zupa"). Konfliktuje się z ojcem. Jak przejmie - będzie dobrze.
    * BUNTUJE SIĘ. Grzeczne buntowanie się. Passive aggressive.
    * ENCAO: ++--0. Stracił matkę jak był dzieckiem.
    * DRIVE: Dawna Przysięga: obiecał pokojówce matki że zapewni jej dobre życie a ona zniknęła i Henryk nie potrafi jej odnaleźć.

### Scena Zero - impl

.

### Sesja Właściwa - impl

Klara siedzi na teście z matematyki i go pisze. NAGLE jest na angielskim. I znowu wrażenie, jakby zabrakło jej czasu. Coś się zmieniło. NAGLE jest na angielskim i nikt nic nie widzi. Nikt o nic nie pyta. Chyba jest OK. 

Klara jest w "pętli". Te wydarzenia dzieją się kilka razy. Nagle jeden chłopak - ona nawet nie wie do końca kto to jest - wstał i wyszedł. Klara jest w nieskończonej pętli sytuacji, nie może zmienić przyszłości. ALE ten chłopak może jako jedyny. Więc Klara wstaje szybko i podbiega na korytarz (tak jak może) by zobaczyć kto to jest.

Klara jest chora, więc porusza się dość wolno. Ale zna korytarze itp. Klara wypada krótko za nim i patrzy szybko - jak tylko zamknie drzwi to ona natychmiast oddaje test. Jako chora prymuska Klara siedzi przy wyjściu w pierwszej ławce, ma bliżej niż tajemniczy chłopak. 

Tr (niepełne) Z (zna sytuację, widzi anomalną sprawę) +3:

* X: zadyszka, ból serca. Przeciążyła organizm.
* V: złapała chłopaka.

.

* "Czemu wyszedłeś?"
* "Zdałem test z matematyki, wyszedłem"
* "Ale nigdy wcześniej nie wyszedłeś"
* "To pierwszy test z matematyki"

Koleś ma pustą karteczkę na imię. Klara robi słodkie oczka. Nie działa.

* X: ktoś wyszedł z sali. To Henryk. Bo jak się nie zaopiekuje Klarą to będą mu tyłek truć.

"Klaro, chodź, znowu poczułaś się gorzej" - i do kolegi - "Dziękuję Ci za zajęcie się koleżanką.". Klara "ale..." Henryk "żadne ale". Koleś się wyrywa Klarze i chce odejść. Klara "nie możesz mi choć powiedzieć jak masz na imię?". Klara się cieszy na widok Henryka, Henryk na Klarę - nie.

* X: wychodzi nauczyciel i Henryk dostaje za niewinność - miał się zajmować Klarą a przecież ona taka 'flustered', słaba itp.

Idzie do kozy po szkole a miał grać w CS z dwoma kolegami. Henryk rzuca Klarze spojrzenie pełne nienawiści i idzie za nauczycielem. A Klara widzi, że koleś WYRAŹNIE NIE MA POJĘCIA JAK MA NA IMIĘ - nie ma plakietki, nie ma niczego.

Klarę ma tajemniczy chłopak odprowadzić do pielęgniarki a Henryk do kozy. MIMO protestów Henryka.

* "Jak często pisałeś ten test z matematyki?"
* "Dzisiaj pisaliśmy, wcześniej miesiąc temu, co miesiąc mamy testy"
    * Klara wie, że to nie było co miesiąc. Co drugi dzień się to powtarza >.>. On nie widzi timeshiftów.
* "Nie muszę iść do higienistki - chodź, musimy pogadać" /do biblioteki

Klara naciska - tu się dzieje coś dziwnego. On unika odpowiedzi, nie chce nic powiedzieć. Klara naciska, on unika. (+3V bo konflikt nie jest już fizyczny a jest gadany).

* V: przestaje patrzeć za okno, jest zainteresowany. Klara opowiada powtarzające się testy, dziwne zmiany. Chłopak wiedzy, że Klara mówi prawdę.
* X: Klara jest przeciążona i biedna. Mdleje. Ale usłyszała imię - Rufus. I nagle zaczął być "prawdziwy".

Dzień 2:

Henryk nie jest już w kozie. Jest przekonany że koleś próbuje narzeczoną poderwać. NIE JEST TO ZŁA OPCJA. Ale nie tak. To Henryk zrywa, to nie z Henrykiem zrywają. Ona mnie nie chce - nie ma takiej opcji. KTO TO JEST. Więc do Alfreda. Niech Alfred znajdzie tego chłopaka w bibliotece - informacje na jego temat. Rocznik, skąd jest itp. Alfred poszuka.

(Alfred ma jedynie opis jaki dał Henryk. Więc Alfred zakłada że opis był błędny SKORO SIĘ NIE DA. Alfred nie znajdzie bo nie może.)

Henryk spokojnie działa w szkole, jak zawsze. Ale będzie czujny.

Klara próbuje dojść do tego kiedy się dzieją przerwy i pętle. Zanim się zmieni NAGLE sceneria, przeskok, słyszysz 'cyk'. Klara podbija do Rufusa i próbuje jedną rzecz zrobić - czy następnego dnia dalej zagubiony czy "na siebie"? (na siebie, ma imię itp.) I się uśmiechnął do Klary.

Klara chce przeprowadzić eksperyment. Chce powiedzieć Rufusowi, umówić się z nim na sygnał żeby zrobił coś inaczej. Czyli chce sytuację, która się wydarza. Niech nie Henryk ją odprowadzi do higienistki a Rufus. Jest test z fizyki. Klara opiera się przy biurku, musi iść do higienistki, niech Rufus odprowadzi. Henryk na razie patrzy na to ze spokojem (jest w dla siebie przeszłej chronologii). 

Klara dodatkowo opowiada Rufusowi co się zaraz stanie by się upewnić, że on docenia i rozumie achronologię.

Tr Z (świadomy eksperyment i zmiana parametrów) +2

* X: przyjaciółka jest nachalna i idzie z nimi - nie mogą swobodnie rozmawiać
* V: Rufus coraz bardziej świadomy, coraz mocniejsze imię na plakietce, chce współpracy z Klarą
* V: nie ma 'cyk' - udało się zmienić przyszłość. Klara nie wylądowała na WFie. Koleżanka TEŻ jest objęta 'zmianą' ale bardzo chce iść na WF. "Rzeczywistość próbuje się odbudować". A Rufus "nie wiem co jadłem na śniadanie". A już WIE że nie wie co jadł na śniadanie.
* V: Klara próbuje ugruntować, przypomnieć, ustawić go w rzeczywistości bardziej. On tego NIE PAMIĘTA. Pamięta "od wczoraj".
* V: Rufus ma PRZESZŁOŚĆ. Imię, nazwisko. Klara dodała mu przeszłość. Niech się skupi.
* X: Adeli podoba się Rufus i chce za nim łazić. Plus mówi Henrykowi o tym że Klara się szlaja i jak to wygląda.
* V: Klara ma opcję od tej pory po rzeczach które się "muszą odbyć" nie jest teleportowana a decyduje gdzie idzie dalej.

Nowe reguły, co już wiemy:

* Klara ma JAKIEŚ własności anomalne. Ona potrafi po pętli określić swoje nowe "miejsce". Bierze udział w pętli i nie musić wracać do pętli. A może.
* Rufus ma INNE własności anomalne. Każdy, kto ma nastawienie emocjonalne do niego PAMIĘTA obecność w pętli - proporcjonalnie do nastawienia emocjonalnego. Nie umie wyjść z pętli ale stan pamięta
* To implikuje, że: Adeli mimo zamknięcia pętli nadal podoba się Rufus, choć nie do końca pamięta czemu.
* To implikuje, że: Henryk mimo zamknięcia pętli nadal podejrzewa Rufusa i pamięta, że wysłał za nim Alfreda, ale nie pamięta szczegółów.
* To implikuje, że: Alfred nie pamięta polecenia szczurzenia za Rufusem.

Henryk - środek nocy. Budzi się. Zmęczony, wściekły itp. Śnił się mu Rufus. Jakby żyli w innych czasach. Normalnie gościa nie zauważa, a tu nagle wielcy rywale. Rywalizowali o kobietę? O coś?
Sprawa jest prosta - Rufus coś mu próbuje z JEGO Klarą robić. Przez Klarę i Rufusa Henryk wpadł w kłopoty? Nie wpadł? Ale jeszcze TO. To, że Rufus jest rywalem w snach nie znaczy, że nie jest rywalem. Henryk wierzy w podświadomość i sny. Jest **odpowiednio uduchowiony**. Uważa, że jego podświadomość próbuje mu coś powiedzieć.

Henryk wzywa Alfreda. Opowiada mu swój sen tak dokładnie jak potrafi. Zakłada, że Alfred zna świat lepiej. Zakłada, że Alfred zada mu dobre pytania. Czyli Alfred najpierw próbuje zrozumieć Henryka i zdobyć detale, a potem wprowadzić to do komputera i znaleźć co Henryk ma na myśli. Alfred podejrzewam PRZYWYKŁ do takich numerów od Henryka.

TrZ (znajomość Alfreda Henryka) +3:

* Vz: ten sen nie jest iluzją. To, co zostało przyśnione się z czymś łączy. Epoka Jonsen. Wszyscy w sukienkach, patriarchat, mędrcy...
* X: nie da się dokładnie dowiedzieć tym źródłem kto dokładnie to jest
* Vr: są słitfocie na necie przypominająca Henryka w tamtej epoce. Czyli nie tylko przodkowie, a nawet DOPPELGANGER. Ale to "przodek".
* V: w przeszłości jest też coś w stylu "przeszłej Klary". Obraz trójki - Klara, Rufus, Henryk. Obraz wygląda jak tryptyk popierś. Trzy osoby, trzy popiersia.
* X: totalne niewyspanie następnego dnia. Do poziomu - mogę nawet nie pójść do szkoły. (+1Vg)
* X: zjeba od ojca - Alfred ma pilnować, żeby Henryk robił robotę w szkole, zachowywał się itp WIĘC Henryk nie może go używać
* Vg: znaleziona okładka książki. Przygodowo-romansowa. Harlekin historyczny.

Henryk zamawia 5 kopii tej książki na asap. I od razu - przed snem o 6:30 rano - zostawia wiadomość Klarze, że znalazłem coś ważnego. I pokazuję jej zdjęcie okładki. Żeby widziała.

Rano - Klara dostaje wiadomość od Henryka wraz z okładką. Klara pozyskuje książkę i szuka Rufusa by też poszedł do biblioteki. Razem czytają książkę - czy czegoś ciekawego tam nie ma. W Harlequinie jest historia gdzie Klara i Rufus mają się ku sobie. Henryk jest "ten zły", zabija Klarę i na końcu Henryk zabija Klarę. A dokładnie - postacie trzymające się tych ról tak to robią. A Henryk i Rufus byli przyjaciółmi.

Klara wysyła streszczenie do Henryka. "Ja w to nie wierzę, Ty byś nigdy tak nie mógł zrobić." Henryk informuje Klarę (telefonicznie), że by tego nie zrobił i albo autor z nim porozmawia ALBO rozwali się go prawnikami za zniesławienie i użycie wizerunku bogatych ludzi bez ich zgody. Niech Klara niczym się nie martwi - Henryk to rozwiąże. Na zdanie "jestem pewna że Rufus pomoże mi coś znaleźć" Henryk ma WOOOAAAARGH! "Nie było Cię w szkole, ktoś musiał mi pomóc przejrzeć bibliotekę". Henryk "uważaj na niego, to niebezpieczny człowiek - próbuje mnie POTWARZYĆ. Może on stoi za książką."

Henryk próbuje znaleźć autora książki. To randomowy facet. Szuka w necie - nie ma. Szuka przez prawników - nie działa. Detektywów - nie działa; detektywi tak jakby nie zapamiętują. Henryk myśli, że to przez ojca. Idzie do ojca "ej ojciec patrz tu jest potwarz i użycie wizerunku." Ojciec rejestruje i zapomina. Henryk widzi, że coś jest mega nie tak. (praca 2 dni)

NASTĘPNEGO DNIA po zobaczeniu tej książki, Rufus mówi Klarze że mu się to śniło. Czuł że się przyjaźni z Henrykiem... tak jak w książce. I proponuje by poszukać więcej książek w bibliotece na ten temat. Wyraźnie bardzo chce spędzać czas z Klarą. I przyniósł coś smacznego dla Klary by zjedli razem. Klara pokazuje książkę bibliotekarzowi - "nie mieliśmy takiej książki na stanie". I zabiera Klarze książkę. Klara protestuje i się siłuje o książkę. Ale chodzi o to by się dowiedzieć.

Tr Z (Rufus i jego słowa i czyny) +2 +3Or:

* Vz: Rufus dołączył do Klary i wyrwał książkę bibliotekarzowi. Bibliotekarz wkurzony. Klara chce by gadał.
* X: Książka jest uszkodzona. Klara zrozpaczona. Rufus też. (na szczęście mamy więcej).
    * Klara: "OMG MÓJ PREZENT! USZKODZONA!" /rozpacz nad książką. 
    * bibliotekarz przerażony że Klara ma tą książkę. Najchętniej by ją zniszczył. +1Vg
* X: Nikt nie zwraca uwagi na taką aferę bo wszyscy się bibliotekarza boją a on taki JEST.
* V: Bibliotekarzowi się wymsknęło, że nie mogą tego czytać bo za dużo sobie przypomną.
    * Klara: "I CO Z TEGO!"
    * Bibliotekarz: "Nie będziecie w stanie później funkcjonować"
* V: Bibliotekarz umówił się na wieczór, wszystko wyjaśni. Nie jest zadowolony. "Będziecie żałować - tylko Wasza dwójka, dzisiaj 20:00 biblioteka".
    * Po tym jak bibliotekarz dowiedział się że Henryk też wie, to się podłamał.

Wieczór, 20:00, Klara i Rufus poszli do biblioteki porozmawiać z bibliotekarzem. Jedna lampka nad biurkiem. Bibliotekarz wygląda lekko złowrogo. Klara ma przy sobie telefon i robi transmisję na żywo do Henryka. Henryk będzie podsłuchiwał.

Tp +1:

* X: zauważył w którymś momencie
* X: nagranie nie jest wiarygodne bo "rozmowa o rpg"
* V: transmisja

Książka inna, na okładce inne osoby. Bibliotekarz podaje ją Klarze. Rufus i Henryk się całują. Rufus "ktoś jest tu chyba chory". Po drugiej stronie telefonu Henryk ma podobną minę - nagle Klara zaczyna wyglądać atrakcyjnie.

Mamy dwie różne książki. Nic znajomego nie ma - są tylko Rufus i Henryk. Nie ma podobieństwo historii, ale Klara jest w roli podrzędnej. I bibliotekarz "TO jest właśnie nasze życie". Klara "a pan gdzie jest?" -> Pokazuje. Są w książce. W wielu książkach. I jest też książka, w której są AKTUALNIE.

Bibliotekarz: "ROZUMIESZ? ŻYJEMY W KOMIKSIE! Jak się skończy ta książka, skończy się nasze życie i zaczniemy żyć w następnej. Mówiłem, że ciężko Wam będzie potem żyć."

Rufus jest taki blady i "niektóre rzeczy pamiętam..."

Henryk: czy wszyscy mają tego samego autora? "Adam Smith". Autora szukał i nie mógł go znaleźć. Henryk: JAKIEGO TYPU jest komiks / książka w której teraz jesteśmy. Czyli 'klik', anomalia, jest czymś co się dzieje gdy autor robi "stały fragment gry" w komiksie.

## Streszczenie

Klara jest młodą damą o chorym sercu, Henryk jest synem bogatego przedsiębiorcy. Okazuje się, że w różnych konfiguracjach występowali wielokrotnie w komiksie i są postaciami komiksowymi. To jest opowieść o tym, jak znajdując książkę udało im się poznać prawdę o swoim pochodzeniu. 

## Progresja

* .

### Frakcji

* .

## Zasługi

* Klara SooJun: problemy z sercem i deja vu; jest zamknięta w pętli czasowej i próbuje się z niej wyrwać. Postać z komiksu. 
* Henryk Park: syn przedsiębiorcy mający inteligentnego lokaja. Postać z komiksu.

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat

## Czas

* Data: 11111111
* Dni: 10

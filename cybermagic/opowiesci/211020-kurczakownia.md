---
layout: cybermagic-konspekt
title: "Kurczakownia"
threads: legenda-arianny, niestabilna-brama, sektor-mevilig
gm: żółw
players: kić, fox, kapsel
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [211013 - Szara nawałnica](211013-szara-nawalnica)

### Chronologiczna

* [211013 - Szara nawałnica](211013-szara-nawalnica)

### Plan sesji
#### Co się wydarzyło

* .

#### Sukces graczy

* 

### Sesja właściwa
#### Scena Zero - impl

.

#### Scena Właściwa - impl

Infernia jest wewnątrz planetoidy Kalarfam. Martyn uśpił "lokalsów". Znacie lokalizację głównej bazy tego wszystkiego - lokalsów. Około 5k osób. Eustachy zaproponował Martynowi, że da się ratować ludzi przez głowy. Czyli same głowy do biovatów. Martyn się zasępił. Około 1-2k głów, po modyfikacji Inferni i jej life support. 

Arianna kombinuje dalej - stworzyli sobie boga, który ma zabrać ich do raju zniszczeniem. Może da się dogadać z bogiem? Może niech on zabija a my pełnimy rolę zbawiciela. Niebezpieczna gra, ale jesteśmy w niebezpiecznym miejscu. Arianna kombinuje - mamy kilku kultystów, można wprowadzić ich do bazy by rozprowadzili właściwą wersję wydarzeń. Najpewniej "wszyscy" widzieli żywą emisję anihilacji w kierunku na planetę. To bóstwo chroni SWOICH kosztem WSZYSTKICH.

Równolegle Martyn z Klaudią analizują byt. Martyn ma prośbę "poproszę o jednego kultystę". Czyli połączona akcja - Arianna stabilizuje i pyta, Martyn i Klaudia badają i próbują dojść do tego co się dzieje.

Luźne pytania Arianny:

* czy widział aspekt Zbawiciela? Jasne -> w Ariannie. Tylko w Ariannie. 
* "Zbawiciel odpycha Bezdusznych. Sprowadza statki do nas by nas ratować."
* Ołtarz to zbiór żywych ścięgien, kości, zakrwawione. +ofiary krwawe. On nie widział nigdy składanej ofiary - tylko kapłani mogą wtedy tam być.

Konflikt - Klaudia próbuje z pomocą Martyna i Arianny określić na jakiej zasadzie Martyn przekazał mu że są po jednej stronie? Co sprawiło, że to zadziałało? I czy dałoby się namieszać na łączu Niszczyciel - Wyznawcy? Jak to łącze działa? Czy da się zaburzyć energię Wyznawca - Niszczyciel? Czyli "jak to działa i czy da się z tym negocjować". A jak nie - czy da się wpłynąć na źródło mocy? Klaudia nie jest w stanie tego zrobić bez magii. Martyn jej pomaga swoją magią. Arianna asekuruje.

ExZM+4+5Og:

* V: Ten "bóg" nie jest bytem do końca świadomym. To elemental. To siła natury, nie osoba. To żywy głód. Strażnik. Ma sztuczny arkin. Ten byt można oszukać, ale nie można z nim rozmawiać. Jest strażnikiem. Jest niszczycielem, odpychającym Bezdusznych i chroniącym tą planetoidę. Jest zbawicielem, który chroni ludzkość. Ale jako niszczyciel nie chroni jednostki. A jako że Esuriit - jak kogokolwiek zobaczy to go zje. To rytualne picie krwi sprawia, zwłaszcza z maskami, że on ich NIE WIDZI. On widzi twarze i czuje krew. Dlatego docelowo wyczuł Elenę - nie miała twarzy ale miała krew.
* XmO(X): Nasz "kultysta" zaczyna bełkotać do Arianny. 

Eustachy - łapie typa, wyrzuca do "śluzy". Z granatami. Niech leci XD.

Ex (wybiegam z łapczakiem) Z+4+3Og+3Or+3Vg (lustra blokujące):

* X: Infernia mocniej uszkodzona niż ktokolwiek chciał, więc mniej miejsca, więcej gruzu itp. Solidna Inferniowa konstrukcja, ale wiecie.
* V: Eustachy wywalił kolesia do śluzy i go wysadził, by poleciał w kosmos.

Arianna NADAL czuje obecność Niszczyciela. On tu jest. Arianna jest jedyną osobą bez maski, więc ją WIDZIAŁ i WIE o niej.

Martyn, po otrzymaniu rozkazu, zsyntetyzował sztuczną biogłowę tamtego kolesia. Wygląda nawet podobnie. Ma uduchowioną minę. Kamil pisze mu teksty które "głowa" ma mówić, i głowa od czasu do czasu coś powie. Martyn ma nieco zbolałą minę, ale zrobił - chodzi o ratowanie reszty...

Infernia dzielnie leci w kierunku centrum planetoidy. A Arianna dzieli się informacją, że "on tu jest" z Klaudią. Dotarli do planetoidy.

PLAN JEST TAKI - wpaść, uratować 2.5k ludzi i wypaść, alternatywnie wyciągnąć załogi naszych statków od Termii.

1. mamy "bazę" w planetoidzie ze sztucznym podtrzymywaniem życia
2. mamy wrakowisko dokładnie w tym samym miejscu. Tam są zrujnowane jednostki
3. w "bazie" znajdują się kultyści Zbawiciela-Niszczyciela. Około 5k.
4. Gdzieś tam znajdują się też nasi ludzie. Chcemy ich ratować.
5. Chcemy uratować tylu kultystów ile potrafimy
6. Infernia ma zdecydowanie, zdecydowanie przekraczający poziom technologiczny
7. jedyny problem - Piranie oraz Zbawiciel-Niszczyciel

Arianna wysyła Flawię z Eleną. Flawia wpierw gada długo z kultystami by złapać wzory zachowania, po czym dostaje szmaty. Operacja - pintka. Ich cel - zlokalizować, gdzie są "nasi". Elena w Eidolonie osłania, Flawia ma infiltrować. Tam gdzie trzeba siły - Elena. Tam gdzie subtelności - Flawia.

ExZ+3:

* XX: Nasze kochane agentki będą wymagać ekstrakcji i ratunku.
* V: Nasze kochane agentki znalazły gdzie znajdują się nasi... to co z nich zostało (31 osób).

Eustachy i ekipa remontowa Inferni + komandosi Verlenów do osłony. Cel - naprawić jednostkę Orbitera. Jak się sensownie da.

ExZ+3+3Og+3Or:

* XX: Na wrakowisku znajdują się już lokalsi i próbują składać, szabrować itp. Robią robotę.
* Or(X): Eustachego opanowała żądza krwi - niszczą piękne jednostki Orbitera. Więc wydał rozkaz - zabijamy. No i komandosi zaczęli zabijać. Lokalsi nie mieli cienia szans - nie zorientowali się nawet. 32 zabitych.
    * Ale za to jak naprawa idzie dobrze. Te jednostki Orbitera nie zostały całkowicie wyniszczone, do nich się Piranie nie dostały. Tylko TAI zostało zniszczone więc niesterowne jednostki się "rozbiły".
* X: Eustachy się zorientował, że coś jest nie tak z tymi statkami. TAI są nieaktywne, ale statek "chodzi" jakby TAI działało. Tylko ofc Persi nie odpowiada. Czujesz obecność bytu Esuriit. I widzisz ofiary śmiertelne - dwie osoby z załogi Orbitera poświęcone w Rdzeniu AI.
* V: Eustachy dał radę zrobić obejście. Jeśli AI Core zacznie działać dziwnie, wysadzi się i przejdzie się na coś "awaryjnego". Tej jednostce nie można ufać - ale nie jest we władaniu Niszczyciela-Zbawiciela.

Arianna dryfuje Infernią koło wrakowiska i spacer kosmiczny. Arianna, komandosi i Eustachy. Klaudia NIE CHCE WIEDZIEĆ.

Arianna "to ten moment by Zbawiciel zastąpił Niszczyciela". Przekonuje kapłanów. 

<TODO - niestety, nie pamiętam już jak dokładnie szło. Arianna była arogancka, ona im pokaże prawdziwy aspekt Zbawiciela. I mieli użyć Działa Rozpaczy, które ma się zniszczyć ale zniszczyć tą manifestację Zbawiciela-Niszczyciela. I ogólnie się udało, choć ludzie najpierw nie wierzyli. Arianna wytrzymała potężny atak mentalny, acz zaczęła ulegać. Eustachy jednak wystrzelił z działa>

Arianna x ołtarz. A Eustachy x działo rozpaczy:

TrZ+4+5O:

* X: Sporo ludzi pada i umiera w męczarniach (najpotężniej zintegrowani) - ogromny szok i przerażenie wśród populacji.
* O: Mamy prawdziwą manifestację Esuriit.
* V: Zbawiciel-Niszczyciel reintegruje. Arianna odzyskuje zmysły.
* V: Miotacz ognia Ottona wypalił to co pełzło Ariannie do góry po nodze.

Elena i Flawia wyciągnęły 31 ludzi Orbitera. Wszyscy Orbiterowcy -> do Martyna na badania.

Eustachy buduje kurczakownię - maszynę do masowego odcinania głów i eksterminacji kultystów, by móc wycofać tyle osób ile się da. Kurczakownia zabija MASOWO. Eustachy układa działo rozpaczy koło niego. I wszystkie możliwe czujniki.

TrZ+3:

* X: Arianna musi stabilizować energię by wykwit Esuriit nie podpełzł w tą stronę.
* V: Masowa kompetentna ubojnia "kurczaków" (maszyna zagłady) zadziała.

Arianna. "Idźcie się wykąpać by dostąpić raju".

* V: Idą się kąpać w szeregu, ze zdjętymi maskami i ufnymi oczkami. Arianna na pierwszy rzut oka widzi "tych nie się uratuje, a tych tak".

Pojawia się ciekawe pytanie - jeśli zabijemy Orbiterowców, to więcej uratujemy też lokalnych. Więc Arianna zdecydowała - że JEJ KULTYŚCI (załoga Inferni) mogą dobrowolnie poddać się zabiegowi kurczakowania by uratować więcej ludzi. I ORBITEROWCY nie mają tej opcji - kurczakowanie w mocy. Martyn zaprotestował. Otton wykonał polecenie.

Jaka skuteczność przywracania potem - test Martyna na gaz, na biovat, na narzędzia, na Esuriit:

TrZ+4 (Martyn i Klaudia):

* X: 2 Orbiterowców, 0 załogantów i grupka lokalsów nie przeżyje kurczakowania i rekurczakowania (regeneracji).
* V: Udało się uratować ponad 1500 osób. Bliżej 2k.
* V: Klaudia zebrała kluczowe dane - co tu się stało, jak zapewnić, by ten "bóg Esuriit" nie pojawił się po drugiej stronie.

Pozostaje wrócić. Eustachy zostawił torpedę anihilacyjną w prezencie by zniszczyć tą planetoidę i wszystko co zostawili.

A potem wrócili. Używając torped odwracających uwagę.

I mają nadzieję nigdy tu nie wrócić.

## Streszczenie

By uratować jak najwięcej osób w Sektorze Mevilig, Eustachy zaprojektował Kurczakownię - maszynę do oddzielania mechanicznego głów i rdzenia od reszty człowieka. Arianna starła się ze Zbawicielem-Niszczycielem jako aspekt Zbawiciela, wspierana przez Działo Rozpaczy (Eustachy). Udało im się sporo uratować (40% populacji Planetoidy Kalarfam), acz załoga Inferni będzie mieć koszmary senne. Klaudia zebrała dość danych, by zapobiec naturalnej manifestacji Zbawiciela-Niszczyciela w Sektorze Astoriańskim i zrobić kontrakcję jak co.

## Progresja

* OO Infernia: traci Działo Rozpaczy, ale za to owo działo się przydało. Infernia jest uszkodzona; nic bardzo poważnego, ale 2 tygodnie naprawy są konieczne.
* OO Infernia: zdecydowana większość załogi jest silnie straumatyzowana przez kurczakowanie - rekurczakowanie. Wiedzą, że to jest potrzebne, ale... to ZŁE.
* Arianna Verlen: BOHATERKA! Weszła do Sektora Mevilig by ratować Orbiterowców i jej się to udało. A ryzykowała nieprawdopodobnie dużo. Plus, [1500-2000] osób więcej? Wow!
* Arianna Verlen: Stała się prawdziwą inkarnacją aspektu Zbawiciela dla uratowanych kultystów Zbawiciela-Niszczyciela.
* Flawia Blakenbauer: TRAUMA. Kurczakowanie - rekurczakowanie. Dla niej to jest... straszne.
* Elena Verlen: TRAUMA. Kurczakowanie - rekurczakowanie. Dla niej to jest... straszne.
* Otto Azgorn: WSTRZĄŚNIĘTY. Kurczakowanie - rekurczakowanie. Jest to konieczne, ale zdecentrowało go mimo wszystko.
* Izabela Zarantel: OGROMNA TRAUMA. Kurczakowanie - rekurczakowanie + Zbawiciel-Niszczyciel. Nie jest w stanie sobie z tym poradzić. Flashbacki itp. Potworność wymaga religii?
* Kamil Lyraczek: TRAUMA. Kurczakowanie - rekurczakowanie. Plus Zbawiciel-Niszczyciel. To oznacza, że ARIANNA jest NAPRAWDĘ aspektem Zbawiciela. Włączył w kult.

### Frakcji

* .

## Zasługi

* Arianna Verlen: udaje Zbawiciela - aspekt mający wszystkich uratować z Kalarfam. Zmierzyła się ze Zbawicielem-Niszczycielem przy jego ołtarzu i kupiła Eustachemu czas na strzelenie weń Działem Rozpaczy. Stała się prawdziwą inkarnacją Zbawiciela dla uratowanych kultystów Zbawiciela-Niszczyciela.
* Eustachy Korkoran: pod wpływem Esuriit zmasakrował z komandosami grupę lokalsów na wrakowisku. Potem na zimno opracował Kurczakownię - oddzielanie mechanicznego mięsa. Gdy Arianna była dominowana przez Zbawiciela-Niszczyciela, strzelił weń z Działa Rozpaczy, uszkadzając je na zawsze.
* Klaudia Stryk: opracowała z Martynem jak zrobić kurczakowanie-rekurczakowanie na poziomie bioskładników. Opracowała, jak powstrzymać Zbawiciela-Niszczyciela jakby ten zamanifestował się w Sektorze Astoriańskim. Uniemożliwiła mu samoistne pojawienie się w tamtym sektorze.
* Martyn Hiwasser: wpierw zsyntetyzował "głowę kultysty" by przekonać wszystkich, że Arianna jest Zbawicielem a potem z pomocą Klaudii przeprowadził operację "kurczakowanie - rekurczakowanie", by przetransportować jak najwięcej osób z Sektora Mevilig do Kontrolera Pierwszego. Duży sukces. On nie dostał żadnych ran mentalnych.
* Kamil Lyraczek: pisze teksty, które biozsyntetyzowana "głowa" kultysty Zbawiciela-Niszczyciela ma mówić. Chodzi o przekonanie innych kultystów, że Arianna JEST Zbawicielem.
* Flawia Blakenbauer: akcja infiltracyjna Kultu Esuriit z Eleną. Udało jej się znaleźć gdzie są przetrzymywani Orbiterowcy, acz wpadła w kłopoty. W zamieszaniu wywołanym pojawieniem się Arianny udało jej i Elenie się uciec.
* Elena Verlen: akcja infiltracyjna Kultu Esuriit z Flawią. Udało jej się znaleźć gdzie są przetrzymywani Orbiterowcy, acz wpadła w kłopoty. W zamieszaniu wywołanym pojawieniem się Arianny udało jej i Flawii się uciec.
* Otto Azgorn: strażnik Arianny Verlen, gdy ta w aspekcie Zbawiciela zstąpiła na planetoidę Kalarfam. Gdy Arianna była infekowana przez Zbawiciela-Niszczyciela, miotaczem plazmy wypalił to co pełzło po jej nodze, akceptując cierpienie przysmażanej Arianny.
* Vigilus Mevilig: stawił czoła Ariannie przy swoim ołtarzu, ale został Zmieniony przez Działo Rozpaczy Eustachego. Hibernacja 1 tydzień, po czym się odbuduje jako coś nowego.
* OO Infernia: uratowano [1500, 2000] eks-kultystów Esuriit. Uratowano 29 Orbiterowców z Sektora Mevilig. Serio, świetna robota - choć koszmary senne załogi.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Mevilig
            1. Planetoida Kalarfam: PRZEPOŁOWIONA. Niestabilna. Nie utrzyma życia. Skażona Esuriit; energia z Działa Rozpaczy Inferni i Zbawiciela-Niszczyciela.

## Czas

* Opóźnienie: 1
* Dni: 2

## Konflikty

* 1 - Klaudia próbuje z pomocą Martyna i Arianny określić na jakiej zasadzie Martyn przekazał mu że są po jednej stronie? Czy da się zaburzyć energię Wyznawca - Niszczyciel?
    * ExZM+4+5Og
    * V: Zbawiciel-Niszczyciel to Elemental. Sztuczny arkin.
    * XmO(X): Nasz "kultysta" zaczyna bełkotać do Arianny. 
* 2 - Eustachy - łapie typa, wyrzuca do "śluzy". Z granatami. Niech leci XD.
    * Ex (wybiegam z łapczakiem) Z+4+3Og+3Or+3Vg (lustra blokujące):
    * XV: Infernia solidnie uszkodzona, jej ekrany wymagają odbudowy, ale się trzyma. A typ wysadzony w kosmos.
* 3 - Arianna wysyła Flawię z Eleną. Ich cel - zlokalizować, gdzie są "nasi". Elena w Eidolonie osłania, Flawia ma infiltrować.
    * ExZ+3
    * XXV: Nasze kochane agentki będą wymagać ekstrakcji i ratunku ALE znalazły gdzie są nasi (31 Orbiterowców przetrwało)
* 4 - Eustachy i ekipa remontowa Inferni + komandosi Verlenów do osłony. Cel - naprawić jednostkę Orbitera. Jak się sensownie da.
    * ExZ+3+3Og+3Or
    * XXOr(X): Na wrakowisku znajdują się już lokalsi i próbują składać, szabrować itp. Robią robotę. Eustachy - żądza Krwi. Exterminatus. 32 zabitych, komandosi za nim.
    * XV: Coś jest nie tak z tymi jednostkami; chodzą częściowo na Esuriit. Więc Eustachy zrobił obejście - jeśli AI Core jest dziwne, przejdzie się na coś "awaryjnego".
* 5 - Arianna x ołtarz. A Eustachy x działo rozpaczy
    * TrZ+4+5O
    * XOVV: Sporo ludzi pada i umiera w męczarniach; Manifestacja Esuriit; Zbawiciel-Niszczyciel reintegruje. Arianna odzyskuje zmysły, Otton miotaczem wypalił to co po Ariannie pełzło
* 6 - Eustachy buduje kurczakownię - maszynę do masowego odcinania głów i eksterminacji kultystów, by móc wycofać tyle osób ile się da. Masowy mord.
    * TrZ+3
    * XV: Arianna stabilizuje energię by wykwit Esuriit nie poszedł w tą stronę, ale maszyna działa
    * V: Kultyści Esuriit idą się kąpać w szeregu, ze zdjętymi maskami i ufnymi oczkami. Arianna na pierwszy rzut oka widzi "tych nie się uratuje, a tych tak".
* 7 - Jaka skuteczność przywracania potem - test Martyna na gaz, na biovat, na narzędzia, na Esuriit
    * TrZ+4 (Martyn i Klaudia)
    * XVV: 2 Orbiterowców, 0 załogantów i grupka lokalsów nie przeżyje kurczakowania i rekurczakowania (regeneracji), ale uratowali ~[1500, 2000] osób. Plus, Klaudia ma dane co zrobić by ten "bóg Esuriit" nie pojawił się w Sektorze Astoriańskim.

## Inne
### Projekt sesji
#### Narzędzia
##### Koncept

.

##### Implementacja

.

#### Przeciwnik

1. esuriit-Skażony mechaniczny terrorform
2. Wielka planetoida Kalarfam
3. Ciągły niedobór energii i konieczność chowania się przed Pożeraczami... i terrorformami
4. kultyści w bazie.
5. Sam teren bazy i dotarcie do niej, dookoła planetoidy Kalarfam.

#### Sceny

Podczas całej sesji - 3 entropiczne esuriit. W wypadku magii - 5 entropicznych esuriit a nie 3 magiczne.

1. Infernia szuka zaginionych jednostek. Zbliża się do niej złom kosmiczny. Złom kosmiczny to nie złom. To Pożeracze - zmierzają w kierunku Inferni. Jest ich za dużo by wszystkie zniszczyć... jak sharkticony. A użycie głośnych działań - ściągnie terrorforma (o którym nie wiedzą).
    * KONFLIKTY: czy Inferni uda się odlecieć?
1. Planetoida Kalarfam; Infernia za sygnałami SOS itp. wlatuje do środka i szuka źródła sygnału. Ostrzeliwuje ją mały stateczek który próbuje Infernię wyprowadzić na bezpieczną przestrzeń. To budzi terrorforma w planetoidzie, który poluje na Infernię i ów stateczek.
    * POKAZUJE: zagrożenie z "prostych rzeczy". Pułapka wynikająca z ixiońskiego terrorforma polującego na byty.
    * AKTORZY: planetoida, terrorform, stateczek
    * KONFLIKTY: czy stateczek przetrwa? czy Infernia ucieknie terrorformowi bez szkód?
1. Wejście na martwy statek 'OO Savera'. Próba odnalezienia map / sektora / where the hell are we. Wrogowie: Tech-Mawurm, Necroguard, Tech-Rockmorph, Soulmorph. I szepty. Szepty o terrorformie. I jeszcze gasnący ludzie, należący już częściowo do szalonej TAI.
    * POKAZUJE: tu się strasznie coś złego stało...
    * KONFLIKTY: dyskretne dostanie się do danych, wycofanie się bez złamania sygnału, COUP DE GRACE, czy dojdą do info o "szalonych TAI"?
1. Ludzka baza; reaktor, pole niewidoczności, kultyści esuriit trzymający bazę przy życiu składając ofiary. Ten odcinek Netflixa ze statkiem kosmicznym. A dotrzeć tam... Infernia jest duża.
    * KONFLIKTY: czy Infernia będzie w dobrym stanie / nie uszkodzi maskowania? Czy połapią się w rozkładzie bazy? Czy dojdą do tego czemu wszyscy mają maski? Czemu nikt nie mówi gdzie są ludzie z Grupy Ekspedycyjnej Kellert? Odnajdą ich w tunelach NIE ściągając Pożeraczy / terrorforma?
1. Ludzie w bazie ROZPACZLIWIE chcą dostać się na pokład Inferni. Ewakuacja? Ratunek? Dzięki Xardasowi! Rozpaczliwe błagania, dadzą wszystko. Uciec z tego piekła. Ambrozja -> ludzie padają na ziemię chłeptać krew.
    * KONFLIKTY: czy Infernia... odleci? Czy będzie wojna Infernia (200) vs większość populacji? Czy kogokolwiek uda się uratować? Wezwać terrorforma przypadkiem? Uratować "swoich"?
1. Terrorform
    * (loop)"...za dużo ścięgien, kości, za długie, tak duży, tak dużo, za dużo oczu, za dużo kończyn, kończyny wszędzie, on jest wszędzie, krew wszędzie, zbawiciel, niszczyciel, szepty, tak dużo szeptów, każda twarz szepcze, tak wiele twarzy, każda twarz jest jego, żyje w każdej twarzy, ostrej, przekłuwające, tnące, mechanizmy, ścięgna..."

#### Wsparcie graficzne (rysunki)

.

---
layout: cybermagic-konspekt
title:  "Czyszczenie toksycznych związków"
threads: dzien-z-zycia-terminusa
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [181218 - Tajemniczy ołtarz Moktara](181218-tajemniczy-oltarz-moktara)

### Chronologiczna

* [181218 - Tajemniczy ołtarz Moktara](181218-tajemniczy-oltarz-moktara)

## Projektowanie sesji

### Struktura sesji: brak

-

### Hasła i uruchomienie

* Ż: (kontekst otoczenia) -
* Ż: (element niepasujący) -
* Ż: (przeszłość, kontekst) -

### Dark Future

1. ...

## Potencjalne pytania historyczne

* brak

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Misja właściwa

**Prolog**: (22:10)

_Sen o Lustrach_

Pięknotka znowu znajduje się w labiryncie luster, miejscu swoich snów. ZNOWU. Zamyka oczy i próbuje zlokalizować coś... próbuje wykryć obecność Arazille. Dowiedzieć się, czego ona chce. Wysyła iskrę do Arazille z zapytaniem o jej wolę. Po chwili jednak orientuje się, że rośnie poziom energii, że wola ARAZILLE zbliża się do Pięknotki - czarodziejka rozpaczliwie spróowała się obudzić. Z sukcesem.

Wpływ:

* Żółw: 0
* Kić: 0

**Scena**: (22:15)

Pięknotka otworzyła oczy. Jest znowu u Bogdana - jeden z jego eksperymentów na niej po prostu ją przeciążył. Zemdlała z wrażenia. Po chwili znowu zrobiło jej się bardzo przyjemnie. Życie jednak jest całkiem dobre...

Komunikat od Erwina - Bogdan kazał jej odebrać. Erwin prosi o kontakt do Kreacjusza Diakona, maga specjalizującego się w sztucznych ciałach. Chce zrobić ciało dla Minerwy; znalazł sposób jak to zrobić. Pięknotka się ucieszyła, ale jednak znalazła sposób jak ściągnąć Erwina TUTAJ, do Cieniaszczytu. W ten sposób uwolni go od Saitaera. Erwin zaoponował, że nie potrzebuje tak dobrego ciała - on potrzebuje odpowiedniego miejsca. Saitaer potrafi je sformować dla Minerwy. Erwin powiedział, że potrzebuje konkretnego ciała, które będzie w trakcie mutowania przez samego Saitaera. (TrZ+2:7,3,6=P,SS): Pięknotce uda się ściągnąć Erwina w bezpieczny sposób, ALE jednocześnie przekaże namiary na Kreacjusza i warm lead.

Z dobrych wieści, Erwin na sto procent przyjedzie, nieświadomy wszystkiego i to bez Minerwy. Dzięki temu Pięknotka będzie w stanie ograniczyć działania Saitaera - a ów nic o tym nie będzie wiedział.

Bogdan powiedział Pięknotce, że potrzebuje coś uzyskać od Dominauta. Dominaut dzieli z nim Kompleks Colubrinus Meditech - ani Bogdan nie jest w stanie go pokonać, ani Dominaut nie jest w stanie pokonać Bogdana. On potrzebuje power suit typu "gladiator-kontroler" i uodpornioną Pięknotkę. Wysłał zapotrzebowanie do Cmentarzyska Servarów by uzyskać coś, co się nadaje do jego celów. I sformuje odpowiedni Power Suit dla Pięknotki. Po czym zabrał się do kolejnej serii eksperymentowania na Śluzie Gladiatora - i Pięknotka znowu odleciała.

**Scena**: (22:36)

_Kompleks Nukleon_.

Tam na Pięknotkę czeka informacja, że Atena jest już przebudzona. Pięknotka I TAK musiała wpierw przejść przez prysznic - w tym stanie była nie do użytku. Ta intensywność wszelkich doznań...

Atena jest zmieniona. Skrzydła zrobione ze światła. Oczy świecące blaskiem. Atena stała się żywym paragonem Astorii. Jak sama zauważyła, może założyć oddział "Anioły Astorii". Charakterologicznie jest spokojniejsza. Mniej neurotyczna. Spokojniejsza, że udało jej się odepchnąć Saitaera. Teraz Atena szuka informacji o Orbiterze - czy może Pięknotka wie coś o kimś, zna kogoś w Cieniaszczycie? Pięknotka od razu pomyślała o Julii. Emulator. Pięknotka przesłała informacje o Julii jako Emulatorze i tym, że ta jest w tej chwili zabujana w Pięknotce na śmierć. Atena się skrzywiła. Niefortunne, ale da się naprawić. Atena uznała, że jest już bezpieczna i czysta - pójdą do Mrowiska, do "mieszkania" Pięknotki.

I Pięknotka ostrzegła Atenę - niech NIGDY nie śni snu z lustrem. Niech się na pewno obudzi. Niech nie próbuje bawić się z boginią...

Wpływ:

* Żółw: 1
* Kić: 1

**Scena**: (22:47)

_Mrowisko_

Julia i Romuald. Romuald jest śmiertelnie obrażony na Pięknotkę. Julia łasi się do niej jak napalona kotka. Ogólnie, wow.

Pięnotka mówi Julii, że przyprowadziła przyjaciółkę (Atenę) która ma parę pytań odnośnie jej domu. I może uda się jej pomóc. Julia zaprotestowała - ona NIE CHCE pomocy. Ona chce pojechać z Pięknotką i Romualdem. Musi pomóc. Teraz na bok odciągnęła Romualda. Romuald powiedział, że Pięknotka jej to zrobiła. Musi to naprawić - Julia jest zbyt uszkodzona, taka Julia jest straszna. Pięknotka zaczyna Romualda przepraszać. Naprawdę nie chciała i naprawdę czuje się winna i to NAPRAWDĘ jej wina. (Tr+1: 6,3,6=S). Romuald czuje się przeproszony, ale uważa, że ZNOWU musi uratować Julię. OSOBIŚCIE. Pójdzie do Moktara i ją uratuje (czego oczywiście nie powiedział Pięknotce, bo lol). Pięknotka tymczasem jest uspokojona i wraca do Julii i Ateny.

Julia podeszła do Pięknotki i zapytała żałośnie, czy teraz. TAMTA Pięknotka by się żachnęła. TA Pięknotka jest skuteczna i bezwzględna - zrobi trójkącik z Julią i Romualdem. O dziwo dla siebie, musiała przekonywać ROMUALDA - on wierzy w prawdziwą miłość i chce być z Julią i tylko z Julią. Pięknotka nie ma żadnych oporów - spróbuje go przekonać. (TpZ:12,3,2->P,S). Udało się OCZYWIŚCIE go przekonać w jedyny słuszny sposób, ale Romuald ma straszne poczucie winy. Zdradził swoją ukochaną Julię. Pięknotka się będzie z tego tylko śmiać. Romuald nie wini Pięknotki, tylko siebie. LOL...

Atena taktownie oddala się coś zjeść i przyzwyczaić się do swojego nowego ciała. Nadal traktuje takie tematy pruderyjnie...

Wpływ:

* Żółw: 5
* Kić: 1

**Scena**: (23:07)

_Kompleks Nukleon_

W czasie, w którym Atena zajmuje się Julią (rozmawia, szuka słabości Orbitera itp.) to Pięknotka skupia się na Pietro i na Wioletcie. Wioletta bardzo lubiła rzeczy piękne - więc niech Erwin przywiezie coś mega epickiego i pięknego dla Wioletty. Plus niech przywiezie konkretne rzeczy z gabinetu Pięknotki.

Więc następnego dnia Erwin przyjechał do Pięknotki. A Pięknotka "is staging it a bit". Poinformowała Mirelę, że przyjdzie jeszcze jedna osoba z tym samym problemem - z infekcją Saitaera. Pięknotka spróbuje przekonać Erwina, ale jak się nie uda...

Więc Erwin przybył. Totalnie zaszokowany jej pięknością. Szczęka gdzieś w okolicy podłogi. Pięknotka wie, że Erwin wie o Saitaerze. Ogólnie, Erwin nie może opuścić kompleksu póki nie jest wyleczony. Pięknotkę martwi, że nie ma pojęcia jak bardzo Skażony przez Saitaera jest Erwin.

Pięknotka podchodzi do Erwina i go przytula. On jest totalnie nieuzbrojony przeciw czemuś takiemu - a Pięknotka jest mistrzynią w tym co robi. On nie widział Pięknotki po transformacji. Pięknotka używa faktu, że to kompleks kralotyczny i jej celem jest przelecenie Erwina. Pierwszy raz w życiu poszli razem do łóżka. Jej celem jest sprawienie, by stracił przytomność i można było go wyczyścić z Saitaera. (Tp+2: 11,3,3=S). Udało jej się, oczywiście. Aha, Erwin się faktycznie w Pięknotce zakochał.

No i przyniósł Pięknotce to, o co prosiła.

Wpływ:

* Żółw: 6
* Kić: 1

**Scena**: (23:20)

_Mrowisko_

Pięknotka musi pozamykać stare wątki. Pietro i Wioletta. Jakkolwiek Pięknotce naprawdę pochlebia zainteresowanie Wioletty (nawet bardzo, w jej nowej formie), ale niedługo Pięknotka stąd musi odejść a oni zostaną.

Pięknotka ma bardzo prosty plan - są zawody, oboje biorą w nich udział. I Pietro i Wioletta. Pietro pimped up a bit. Niech walczą ze sobą, niech są ostatnimi na szczycie. A przed ostatnią walką, gdzie będą ze sobą walczyć - oboje dostaną odpowiednie serum. Rozmowa czy dwie z Mirelą czy nie dałoby się tego załatwić jakoś w Cieniaszczycie (Mirela to załatwia) i Pięknotka ma świadomość, że zostaną razem. Niech to będzie kilka dni. Niech mogą pogadać, ocenić i inne takie.

(TrZ+2:12,3,4=S). Zrobione. Plan Pięknotki doprowadzi do średnioterminowego spiknięcia się tej dwójki. I, co ważniejsze, Pięknotka nie będzie elementem tej układanki. Nie będzie sytuacji, w której brak Pięknotki spowoduje jakieś zawirowania czy rozpady w Cieniaszczycie.

Czas wrócić do niewoli Bogdana (czego Pięknotka by tak nigdy nie nazwała).

* Żółw: 8
* Kić: 1

**Scena**: (23:32)

_Colubrinus Meditech_

Romuald zdecydował się na udanie się do Moktara, by żebrać go o łaskę dla Julii. Moktar zupełnie nie rozumie wtf, ale nie wiedział, że Bogdan w jakimś stopniu zrobił coś Julii. Stwierdził, że chyba smycz Bogdana jest zbyt poluzowana i zdecydował się odwiedzić Colubrinus Meditech. Akurat w momencie, w którym Pięknotka była w swoim nowym Slave Suit a Bogdan kontrolował ją zdalnie.

W tym miejscu i w tym stanie Pięknotka nawet się nie obejrzała na Moktara, pomiędzy profesjonalnie aplikowanymi sygnałami. Była w swoim świecie. Pięknotka nie czuła potrzeby obrony Bogdana - do momentu, gdy zobaczyła, jak Moktar wyrwał Bogdanowi serce - a Bogdan nadal żyje i agonalnie cierpi.

Pięknotka ma slave suit, dostosowany do walki z Dominautem. Taki, który wysyła jej odpowiednie sygnały ekstazy i cierpienia. Pięknotka odpala FULL SALVO. Chce zranić Moktara i przenieść ogień potwora na siebie. No i jest to też dotknięte smakiem Pięknotki - ona chce na niego wpłynąć ekstazą. Może na ekstazę nie jest odporny...

(TrZ+1:=11,3,5=S). Moktar stracił nad sobą kontrolę. Normalna Pięknotka byłaby przerażona. TA, w TYM suicie, nie zauważyła różnicy. Wir energii Saitaera otoczył Moktara i potwór uderzył. Pięknotka nie walczy przeciwko MAGOWI. Walczy przeciwko POTWOROWI. I Moktar walczy 100% stylem Saitaera. Jej cel? Staying away. Chemikalia i dym - o właściwościach relaksacyjnych. (TrZ+2:12,3,4=S). Udało jej się oddalić a Moktar dał radę odzyskać nad sobą kontrolę. Zmienił styl walki.

Moktar włączył wyrzutnię rakiet devastator. Pięknotka zaatakowała strike&flee. Moktar wysadził devastator, po czym złapał Pięknotkę i zdarł z niej slave suit. Po chwili się opanował i mimo stanu Pięknotki (fear&lust) puścił ją na ziemię. Channelował Arazille by ją wyczyścić; niestety, nie pomogło. W miarę szybko zorientował się, że to co zrobił Bogdan jest czymś innym niż to, czego się spodziewa.

Uśpił Pięknotkę by ją zabrać gdzieś gdzie jej pomogą. A Bogdanowi obiecał, że się nim zajmie w swoim czasie i upewni się, że Bogdan pamięta kto tu naprawdę rządzi...

* Żółw: 9
* Kić: 1

**Epilog**: (00:11)

* Po dwóch kolejnych dniach Pięknotka będzie w stanie funkcjonować racjonalnie przy Bogdanie. Nie przejdzie w pełni (jej prawdziwa natura), ale ona kontroluje to a nie on.
* Wszystkie pary się jakoś podopasowywały.
* Erwin ma głębsze Skażenie; trochę posiedzi.
* Saitaer realizuje swój plan w Pustogorze - ten wymagający dostępu do Kreacjusza - dostęp do cloning chambers (-5 Wpływu)

### Wpływ na świat

| Kto           | Wolnych | Sumarycznie |
|---------------|---------|-------------|
| Żółw          |    4    |      9      |
| Kić           |    3    |      3      |

Czyli:

* (K): Zdążymy wrócić do Pustogoru z lepszym ciałem dla Minerwy zanim Saitaer skończy swoje machinacje z jej ciałem tam. (-3 Wpływ)
* (Ż): 

## Streszczenie

Pięknotka ściągnęła Erwina na detoks od Saitaera, acz dała mu informację o Kreacjuszu Diakonie. Saitaer planuje zbudować sprawne ciało dla Minerwy. Atena jest zregenerowana w dość anielskiej formie i dostaje informacje o Orbiterze od Julii. Pięknotka próbuje naprawić pary Romuald x Julia oraz Wioletta x Pietro. Moktar uwalnia Pięknotkę od wpływu Bogdana, masakrując swojego agenta w brutalny sposób.

## Progresja

* Pięknotka Diakon: jest "wolna" od wpływu Bogdana. Ona kontroluje tą relację ze swojej strony, choć ma słabość - ale nie jest kontrolowana.
* Pięknotka Diakon: Erwin Galilien się w niej zakochał. Nie jest tylko zwykłą przyjaciółką - zostali kochankami.
* Pięknotka Diakon: Moktar czuje do niej szacunek. Niechętny szacunek, jako wystarczająco godnego przeciwnika.
* Saitaer: ma warm lead do Kreacjusza Diakona, osoby zajmującej się farmami klonów w okolicach Pustogoru. Wykona swój plan.
* Romuald Czurukin: przez trójkącik z Pięknotką i Julią ma katastrofalne, niszczycielskie poczucie winy - zrobił coś niezgodnego z wiernością swej ukochanej Julii.
* Atena Sowińska: powróciła z rekonstrukcji; ma świetliste skrzydła i jest paragonem Astorii. She changed, got stronger.
* Erwin Galilien: poszedł na rekonstrukcję (usunięcie Saitaera). Zakochał się w Pięknotce - nie jest już tylko przyjaciółką.
* Moktar Gradon: czuje do Pięknotki niechętny szacunek - wystarczająco godny przeciwnik.
* Bogdan Szerl: skończył na "reedukacji" u Moktara za to co zrobił Pięknotce.
* Pietro Dwarczan: spiknął się z Wiolettą dzięki machinacjom Pięknotki. Jest szczęśliwy.
* Wioletta Kalazar: spiknęła się z Pietrem dzięki machinacjom Pięknotki. Jest szczęśliwa.
* Kreacjusz Diakon: Saitaer przejął nad nim kontrolę; stał się agentem Saitaera.

### Frakcji

* 

## Zasługi

* Pięknotka Diakon: spiknęła ze sobą dwie pary, zapewniła Atenie źródło informacji, ściągnęła Erwina do detoksu od Saitaera i została pokonana przez Moktara gdy broniła Bogdana.
* Bogdan Szerl: pragnął zadowolić siebie i Moktara i przygotował Pięknotkę do walki z Dominautem. Skończył rozszarpany przez Moktara i żywy dzięki energii Saitaera.
* Atena Sowińska: zregenerowana, z anielskimi skrzydłami i paragon światła. Zbiera informacje o Orbiterze od Julii - próbuje zrozumieć, co tu się dzieje.
* Julia Morwisz: w trójkąciku z Romualdem i Pięknotką. Źródło informacji dla Ateny. Ogólnie, nędzny los jak na Emulatora.
* Romuald Czurukin: unikał trójkątu z Diakonką (!). Ma poczucie winy. Poszedł błagać Moktara o łaskę dla Julii - przypadkowo wyciągnął Pięknotkę z opresji.
* Moktar Gradon: wszedł do Colubrinus Meditech sprawdzić o co chodzi z Julią i uratował Pięknotkę od Bogdana. Przez MOMENT stracił kontrolę. Channelował Arazille i Saitaera.

## Plany

* Pięknotka Diakon: jak najszybciej spreparować ciało dla Minerwy stąd, z Cieniaszczytu. By jak najbardziej ułatwić Minerwie przejście i bycie sobą.
* Saitaer: przejąć kontrolę nad pomniejszymi salami do klonowania.

### Frakcji

* 

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Przelotyk
                        1. Przelotyk Wschodni
                            1. Cieniaszczyt
                                1. Kompleks Nukleon: miejsce pułapki na Erwina zrobione przez Pięknotkę - z powodzeniem.
                                1. Mrowisko: centralne miejsce spotkań i randek Pięknotki. Też: Atena i Julia zostały tam na dyskusję.
                1. Sojusz Letejski, NW
                    1. Ruiniec
                        1. Colubrinus Meditech: miejsce ostrego starcia Moktara z Pięknotką i Bogdanem. Też: mieszka tam gdzieś Dominaut, czymkolwiek jest.

## Czas

* Opóźnienie: 0
* Dni: 4

## Narzędzia MG

### Budowa sesji

**SCENA:**: Nie aplikuje

### Omówienie celu

* nic

## Wykorzystana mechanika

1811

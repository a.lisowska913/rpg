---
layout: cybermagic-konspekt
title: "KJS - Stymulanty Szeptomandry"
threads: kajis, mroczna-strona-verlenow
gm: żółw
players: kić, anadia, kolega-kamil
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [230124 - KJS - Wygrać za wszelką cenę](230124-kjs-wygrac-za-wszelka-cene)

### Chronologiczna

* [230124 - KJS - Wygrać za wszelką cenę](230124-kjs-wygrac-za-wszelka-cene)

## Plan sesji
### Theme & Vision

* Wygrać za wszelką cenę! + Lone, too young wolf
    * Franka Potente "Believe" & Musica "Flower of Life"
    * Stawać się lepszym (trening, ćwiczenia) CZY wygrywać (Szeptomandra)?
    * Uczyć się na swoich błędach CZY wygrywać? (Elena nie chce współpracować z Apollo VS Elena i Apollo rozmontowują)
* Dylematy
    * Czy skupić się na samodzielnym pokonaniu Szeptomandry (więcej esencji) czy na obronie ludzi?
    * Czy narazić życie młodych ludzi czy zdobyć więcej esencji?    (Karol: "to są magowie, nie warto im pomagać - ludziom tak.")
    * Wykorzystać dziecko (Elenę) czy współpracować z nią?
* DARK FUTURE
    * Szeptomandra ucieknie LUB jej destrukcja spowoduje kaskadowy szok po młodych ludziach dookoła.
    * Elena wejdzie w walkę z Szeptomandrą i nie jest w stanie wygrać. Szepty Esuriit.
    * Zaczną się krwawe walki dookoła
* Akcje
    * Klardat olewa Elenę; Apollo powinien się tym zająć. Elena ma wsparcie Rafała Namczaka i z jego pomocą wie jak szukać
    * Elena jest skonfliktowana - ona wie jak odnaleźć "truciznę", i tylko ona to czuje. Walka "pomóc" vs "wygrać"
        * wybiera "pomóc", ale Rafał przekonuje ją by "wygrać" pod wpływem Lucasa

### Co już wiemy

* Macie dostęp do Kajisa - świetnej kameleonicznej jednostki z pilotem Karolem o chromej nodze
* Koszarów Chłopięcy to jest miejsce "sportowo - turniejowe" na terenie militarystycznych Verlenów
* pojawiło się coś na kształt narkotyku na terenie Koszarowa Chłopięcego
    * Były testy chemiczne - nic nie wykazały. Po walce. To naturalny doping, ciało Marty w pełni skupiło się na celu.
* Apollo jest lokalnym magiem, ma kuzynkę Elenę (15?)
* Żaneta zaprzyjaźniła się z lokalną Sereną, dobrą atletką - jako JEDYNA Serena nie wie że Żaneta nie jest stąd
    * Serena nie wie nic o tym by Marta z kimś spała
    * Serena wyznała w tajemnicy, że PODOBNO Marta zaczęła kręcić z Franciszkiem. Ale to niemożliwe bo Marta ma charakter a Franciszek nie ma charakteru.
* Lidia zdobywa informację od szefowej medycyny, Zuzanny Kraczamin, drakolitki
    * Zuzanna: Pełna rekonfiguracja hormonalna; podobno Elena Verlen umie to wykryć
* Lucas zdobył od Eleny gwarancję, że będzie bateria 
    * Elena: Daję Ci słowo, że jeżeli osiągnę sukces między innymi dzięki Twojej wiedzy, o kuglarzu, to dostarczę Ci baterię. Pasuje?
    * Lucas zraził do siebie mocno Elenę mówiąc, że jak dorośnie to zrozumie
    * Lucas -> Lidia, Żaneta: "sami go złapiemy". Elena nie wraca. Wie o medyczce.

### Co się stanie (what will happen)


* S01
    * Zuzanna -> Lidia: "jeśli Elena umie wykryć, możemy odizolować i zbadać"
    * Marta -> Żaneta: "Jesteś obca, chcesz nas rozdzielić"
    * Michał -> Lucas: "pomogę Ci zabierz mnie ze sobą"
* S03
    * Droga przez skały jest bardzo trudna, jak znaleźć Szeptomandrę? Franciszkiem?
* S04
    * Elena i Rafał vs Szeptomandra. Szeptomandra wpływa na obu. Elena walczy z Rafałem.
    * Wycieczka w Skały, Elena też idzie. Sama. Walczyć z Szeptomandrą.
    * Szept Esuriit. Elena nie może uratować wszystkich jak nie jest mocniejsza

### Sukces graczy (when you win)

* devour Szeptomandra.

## Sesja - analiza

### Fiszki
#### 1. Fiszki Kajis

* Karol Atenuatia: ???: "???" (szary prochowiec, siwiejący brunet, oddalone oczy, uszkodzona proteza nogi)
    * (ENCAO:  +-+00 |Płomienny;;Żarliwy| VALS: Family, Security >> Self-direction| DRIVE: Noctian Legacy)
        * core wound: Nie możemy wszystkich uratować. Przegraliśmy. Nie mam szans, mam tylko walkę.
        * core lie: Walka by być wolnym i uratować kogo się da jest ostatnim co mogę zrobić.
    * styl: cichy, z boku AŻ eksploduje cytatem z Księgi Wiecznej Wojny.
    * "Wolność wymaga walki i poświęcenia."
    * SPEC: miragent, typ: infiltrator

#### Fiszki Koszarowa Chłopięcego

* Apollo Verlen: mag, sybrianin: "żyje się raz, piękne panny!" (UWIELBIANY przez żołnierzy i ludzi)
    * (ENCAO:  ++-00 |Mało punktualny.;;Beztroski i swobodny| VALS: Face, Hedonism >> Security | DRIVE: Uwielbienie Tłumów)
        * core wound: never good enough. Nie jest tak dobry jak mógłby być. Niezaakceptowany przez swoje słabości. Nie dość dobry jako Verlen.
        * core lie: jeśli nie zrobisz wszystkiego pókiś młody, nigdy niczego nie zrobisz! Czas płynie i tracisz życie!
    * styl: głośny, płomienny, wszechobecny - i Johnny Bravo ;-)
    * "żyjemy raz, więc żyjmy pełnią życia, razem!!!" - dArtagnan "we're gonna be drinking"
    * SPEC: dowodzący oficer, morale, walka bezpośrednia
* Elena Verlen: czarodziejka, sybrianka: "you must be the best there is"

UCZNIOWIE

* Serena Krissit
    * (ENCAO:  --0-+ |Nie kłania się nikomu;;Prowokacyjna;;Skryta| VALS: Achievement, Security| DRIVE: Pokój i bezpieczeństwo)
    * styl: pracowita, chce pokazać
    * "Wojna się nie skończy tylko dlatego że nie ma noktian. Prawdziwy wróg jest _tam_."
* Marta Krissit: <-- WHISPERED
    * (ENCAO:  +---0 |Narcyz;;Nie ma blokad| VALS: Family, Stimulation, Achievement| DRIVE: Lepsza od siostry; przejąć kapitanat)
    * styl: aggresive, shouty
    * "Jestem lepsza od Ciebie i od każdego!"
* Adam Praszcz
    * (ENCAO:  +0-0- |Pretensjonalny;;Zapominalski;;Żywy wulkan| VALS: Hedonism, Tradition >> Humility| DRIVE: Odbudować reputację najlepszego)
    * styl: nerwowy, gubi myśli, świetny fizycznie, PRACOWITY
    * "Nie rozumiem co się dzieje, ale widać nie pracuję dość mocno"
* Franciszek Korel: <-- WHISPERED
    * (ENCAO:  -0+00 |Nie znosi być w centrum uwagi;;Cierpliwy| VALS: Universalism, Self-direction >> Face| DRIVE: Niestabilny geniusz Suspirii)
    * styl: Jurek
    * "Zwyciężymy, nieważne z czym mamy do czynienia. To tylko kwestia tego ile zapłacisz."
* Wojciech Namczak: (ten od głupich Verlenów) <-- DOSTAŁ WPIERDOL OD ELENY BO MAGIA
    * (ENCAO:  000-+ |Sarkastyczny i złośliwy;;Łatwo wywrzeć na nim wrażenie| VALS: Stimulation, Conformity >> Humility| DRIVE: Mam rację.)

INNI

* Jakub Klardat: dowódca defensyw bazy 
    * (ENCAO:  +--0- |Niestały i zmienny;;Nie przejmuje się niczym;;Apodyktyczny| VALS: Hedonism, Humility| DRIVE: Unikalne przeżycia)
    * chce, by Apollo zapłacił za swoją niekompetencję. Wie, że coś jest nie tak, ale Apollo powinien się tym zająć.
* Emilia Lawendowiec: aktualna czempionka
    * (ENCAO:  -0-0+ |Mało punktualna.;;Chętna do eksperymentowania| VALS: Achievement, Conformity| DRIVE: Queen Bee)
    * chce być najlepsza i dlatego bierze prywatne lekcje od Apollo. Płaci za to ciałem.
* Zuzanna Kraczamin: medical + monsterform
    * (ENCAO:  -00-+ |jest w niej coś dziwnego;;Kontemplacyjna| VALS: Power, Universalism| DRIVE: W poszukiwaniu najlepszego potwora)
    * nie jest stąd; z przyjemnością robi nieetyczne stwory
* Rafał Namczak: zdrajca Eleny, 17
    * (ENCAO:  ++0-0 |Nielojalny;;Agresywny;;Lubi zaczynać rozmowę| VALS: Hedonism, Stimulation | DRIVE: Zemścić się na Elenie za kuzyna)
    * chce pomóc Elenie się dostać do celu i wbić jej "nóż w plecy" za krzywdę brata
* Michał Waczarek: chętny pomocnik Lucasa, 17
    * (ENCAO:  0-+-0 |Nie przejmuje się niczym;;Nie ma absolutnie żadnych granic | VALS: Stimulation, Power | DRIVE: POKAŻĘ magom, wyrwać się stąd)

### Scena Zero - impl

.

### Sesja Właściwa - impl

Lucas "dobra dziewczyny, jakie pomysły". Podszedł do niego młody chłopak (17-20). Mięśnie, wyraźnie ma ruchy. "Przepraszam, nie jesteś stąd." Powiedział Lucasowi, że nie chce być tutaj i niech Lucas weźmie go ze sobą. Naprawdę - super pojechałeś tą arystokratkę. Jest najgorsza. Lucas "czy coś wiesz o narkotykach?" Michał "nie nie" /smutek "magowie nie pozwalają".

Lucas proponuje Michałowi, żeby poszedł do Marty i się dowiedział co i jak. Młody buntownik, więc jest szansa że się z Martą zna. Michał próbuje przekonać Martę

Tr (niepełny) Z (lokals) +2:

* V: Marta nie odmówi wiedzy Michałowi
* X: Marcie trzeba posłodzić
* Vz: Do Marty przyszedł lokals. Który jej się podoba. I KTOŚ chce się z nią zobaczyć, bo wie ważne rzeczy. Marta aż puchnie z dumy.

Żaneta, superspołeczne stworzenie poszła rozmawiać z Martą wiedząc by nie powoływać się na siostrę. Żaneta "powinnaś była wygrać z siostrą, następnym razem wygrasz, bo jesteś coraz lepsza". Marta rozpromieniła "widzisz, ona to widzi. Serena gaśnie, ona nie idzie tak szybko jak ja. Ja będę lepsza od niej. To już niedługo." Żaneta "wow, widać potencjał, idziesz jak burza"

Żaneta chce by Michał i Marta poszli w romans i by się wysypała o stymulantach.

Ex Z (dobre posłodzenie i atmosfera) +4:

* Vz: Żaneta nie pierwszy raz traktuje serca ludzkie jak glinę. Marta jest zainteresowana Michałem (choć jego status jest za niski). Marta wyraźnie jest zainteresowana TYLKO chłopakami z górnego 5% lub 1%. Zasługuje na najlepsze. Marta była widziana z Franciszkiem. A on ma dolne 30% statusu. Marta kręci z Franciszkiem. Marta nie ukrywa tego, że kręci z Franciszkiem. Michał jest... zdziwiono-oburzony.
    * W tej rozmowie wyszło: 
        * Serena skupia się na tym, by się stawać coraz lepsza. Dla niej wynik jest tylko wynikiem. Dla niej to trening, robi "na jutro"
        * Michał uważa, że to co robi Serena nie ma sensu - magowie i tak są lepsi. Czyli po co on ma trenować, starać się skoro "taka Elena" po prostu używa magii i chłopak kończy w szpitalu
        * Marta twierdzi, że jedyne co się liczy to zwycięstwo. Będzie najlepsza.
* Vz:
    * Patrząc na krzywą jest moment przegięcia i jakie są efekty uboczne
        * Marta więcej śpi. Ma bóle głowy. (Michał widzi, że Marta staje się bardziej socjopatyczna, to idzie krok po kroku -> coś chce to bierze.), starzeje się szybciej
        * ona nie widzi tego że to jest koszt "po 30 to ona już nie ma życia bo nie może rywalizować z młodszymi"
    * Jak pomóc Michałowi by miał fajną dziewczynę
        * Marta się zaśmiała, że chyba Michał nie jest w typie Franciszka
        * ogólnie, Michał musiałby czymś się wybić. Np. pokonać maga. 
* Vr: Marta powiedziała coś... więcej.
    * Marta powiedziała, że... nocny spacer po skałach był fajny, był warty tego. Ale _to co było_ w jaskini. Oczy. Szepty. To jest koszt.
        * Jeśli Żaneta ma słaby umysł lub słabe ciało, niech nie próbuje. Nie wróci. Przyrównała to do patrzenia w oczy bazyliszkowi.
        * Franciszek zna drogę
        * Marta nie jest uzależniona. Może przestać. Ale nie chce. Bo chce wygrać.
        * To jest jak bazyliszek, który spełnia życzenia.
        * Marta się nie martwi, bo to nie są narkotyki. To nie jest nielegalne. To programowanie ciała i umysłu. Przebudowa tego co lubisz.
            * Przebudowa zainteresowań i marzeń osoby na to, czym "powinny być" by było skuteczniej.

Marta powiedziała Żanecie, że może Franciszkowi zapłacić _innego typu_ treningami.

Marta zapoznaje Żanetę z Franciszkiem. Franciszek to taki... _typ_. Podłe z niego stworzenie. Marta "oddała mu Żanetę jako ofiarę". Wyraźnie nie chce z Franciszkiem mieć dużo wspólnego, ale musi z nim być.

Żaneta udaje tępą fetyszystkę ("pościelić Ci łóżko?" i "zrobimy to w jaskini") a Franciszek się rozślinił.

TrZ+3+3Or:

* V: Franciszek jest na tyle zainteresowany i zainteresowany samym fetyszem że ON Z NIĄ TAM PÓJDZIE
    * "Marta wie co jest... wow", "znalazła serio tępą laskę, ale to jest fajna laska"
        * PRZYJDŹ O DRUGIEJ RANO. Spotkamy się koło białej skały.

Lucas i Lidia idą do Apolla. Do Barbakanu. Jest strażnik przy wejściu. Lidia jest ubrana "w dekolt" ;-). Nie chce za bardzo rozpraszać Apolla ale chce by strażnik miał powód wierzyć że jesteście umówieni. "Co kobieta w okolicy musi zrobić by się spotkać z Apollem Verlen"

Tp (niepełny) Z +3:

* Vr: strażnik zadał standardowe pytania, odpowiedź "do Apolla", "w sprawie potwora". Strażnik Was wpuścił i przekierował.

10 minut później pojawił się Apollo. Podszedł do tematu normalnie - chcą irianium, dostaną irianium. Umówili się - Apollo o 2 rano będzie wraz z kilkunastoma żołnierzami. Apollo będzie w trybie łowcy. Lidia proponuje Apollo, że Elena potrafi rozpoznać osoby które są. Apollo wykorzysta Elenę jako żywy detektor. Poprosił Klardata - 2 tygodnie papierów (wizy) dla 3 osób. Ogólnie, Apollo podszedł do nich spokojnie.

Lucas szczurzy (test percepcji / szczęścia):

Tr +3:

* XXV: Lucas jest w stanie wykryć czy określić, że faktycznie macie ogon. Oni nie są wrodzy. Oni tylko "upewniają się że nic złego się nie dzieje"

NOC. Rozemocjowana Żaneta spotyka się z plugawym Franciszkiem. Idą w stronę jaskini i on opowiada co ma zamiar z nią zrobić. Żaneta daje mu się prowadzić w opasce, szczęśliwie jak dotarli do jaskini to uderza Apollo ze swoimi żołnierzami. Potwór zostaje pokonany ku rozpaczy Franciszka itp.

Następny dzień. Apollo jest w gabinecie. "Faktycznie, nic nie zepsuliście, nikogo nie pobiliście, nie wykroczyliście poza zwyczaje, jesteście faktycznie turystami (zdziwiony). Na czym polega Wasz sekret? Nikt normalny tu nie przychodzi." Zatrudnił Kajis do zrobienia dowcipu komuś; tymczasowo niech sobie tu są.

## Streszczenie

Młody chłopak, Michał, wykrył że KJS nie jest stąd; chce odlecieć z nimi. Lucas pyta go o narkotyki, ale Michał nic nie wie - więc jest wysłany do Marty by dowiedział się więcej. To dało otwarcie Żanecie która dowiedziała się od Marty o stymulantach.

Marta opowiada Michałowi o skutkach ubocznych stymulantów, jak np. senność i bóle głowy. Marta zdradza też, że substancje te nie są narkotykami ani nie są nielegalne, ale mają wpływ na umysł i ciało. Przedstawia Żanetę Franciszkowi, z którym ma romans (bo on zapewnia stymulanty). Żaneta udaje tępą fetyszystkę, a Franciszek planuje wykorzystać ją jako ofiarę. Dzięki temu Franciszek prowadzi ją do Szeptomandry (źródła stymulantów) a Lidia i Lucas proszą Apollo o interwencję dając mu całą sprawę na talerzu.

Ostatecznie, Żaneta i Franciszek spotykają się nocą, a Apollo i jego żołnierze pokonują potwora w jaskini.

## Progresja

* .

### Frakcji

* .

## Zasługi

* Lucas Oktromin: (30) przekonał Apollo Verlena do tego, by ten pomógł zestrzelić Szeptomandrę, że dają mu wszystko na talerzu i że potrzebują baterie irianium. Aha, są tylko turystami.
* Lidia Nemert: (27) jej dekolt posłużył do wbicia się do Apollo Verlena, pomogła Lucasowi w przekonaniu Apollo że coś jest na rzeczy i oni coś wiedzą.
* Żaneta Krawędź: (25 a wygląda na 19) społeczne stworzenie, które przymilało się do Marty by móc wydobyć informacje o stymulantach i uwodziła Franciszka udając skrajnie tępą, by ten doprowadził ją do jaskini.
* Apollo Verlen: tien Verlen, który okazał się być całkiem normalny i dość rozumiał co jest grane. Nie chciał zrobić nikomu krzywdy i ogólnie dobrze przyjął Zespół. Zniszczył Szeptomandrę i ustabilizował teren. Zapłaci Kajis za zrobienie dowcipu komuś innemu. Nie pytał o "turystów", bo niekoniecznie chciał wiedzieć, acz swoje podejrzewał.
* Marta Krissit: korzystała ze stymulantów, a następnie wyjawiła ich tajemnice Żanecie; podkreśliła, że by móc wygrać czasami trzeba dużo zapłacić. Też w TEN sposób.
* Franciszek Korel: miał romans z Martą i planował wykorzystać Żanetę jako ofiarę, ale ostatecznie został pokonany przez Apollo i jego żołnierzy. Źródło stymulantów od Szeptomandry w jaskiniach.
* Michał Waczarek: 17-latek, który chciał się wyrwać z Verlenlandu i poprosił Lucasa o pomoc. Poszukiwał informacji od Marty dla Lucasa by się wykazać.
* KDN Kajis: obecny, zamaskowany, w niskim stanie energii. Nic szczególnego nie zrobił.

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Aurum
                        1. Verlenland
                            1. Koszarów Chłopięcy, okolice
                                1. Skały Koszarowe
                                1. Las Wszystkich Niedźwiedzi
                            1. Koszarów Chłopięcy
                                1. Miasteczko
                                    1. Bar Krwawy Topór
                                1. Wielki Stadion

## Czas

* Opóźnienie: 1
* Dni: 3

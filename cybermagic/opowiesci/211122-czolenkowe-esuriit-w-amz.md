---
layout: cybermagic-konspekt
title: "Czółenkowe Esuriit w AMZ"
threads: mlodosc-klaudii, esuriit-w-czolenku
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [211122 - Czółenkowe Esuriit w AMZ](211122-czolenkowe-esuriit-w-amz)

### Chronologiczna

* [211122 - Czółenkowe Esuriit w AMZ](211122-czolenkowe-esuriit-w-amz)

## Plan sesji
### Co się wydarzyło

* Trzech magów Eternijskich z osłoną terminusów spróbowało containować Esuriit w Czółenku.
* Doszło do katastrofy; magowie ulegli Skażeniu i stoczyli walkę z terminusami.
    * Ostre siły terminusów i wylewanie Esuriit
* Jeden z cieni tych magów uderzył w AMZ.
* A tam nauczyciele i dzieciaki...

### Sukces graczy

* Czy uda się uratować dzieciaki ZANIM pożre je Esuriit?
* Czy czternastolatek przetrwa?
* Czy uda się schować / wprowadzić potwora do Labiryntu?
* Czy uda się kupić czas?

## Sesja właściwa
### Scena Zero - impl

.

### Scena Właściwa - impl

Klaudia jest studentką AMZ, 16-latka. Jej najbliższą przyjaciółką jest Ksenia. Trzewń, niewiele starszy od Klaudii, podrywa Klaudię. Czego Klaudia nie widzi, ale Ksenia tak ;-).

TERAZ:

* "On Cię zdecydowanie podrywa" - Ksenia, ze szlabanem, do Klaudii.
* "Ee?"
* "Mariusz Trzewń!" - Ksenia, przewracając oczami.
* "Nikt mnie nie podrywał."

Ksenia mocno, mocno westchnęła. A mało kto się zna jak ona. Mały diabełek.

* Ksenia - "Żeby zrobić recap - zaprosił Cię dziś w nocy na wycieczkę do Budynku Centralnego. Chce Ci pokazać jakieś maszyny."
* I co w tym dziwnego?
* Chce Cię pocałować.
* Co mają maszyny do całowania?
* "Ty i on. Dyskrecja. Bicie serc. Maszyny..?" - tu się Ksenia lekko zmieszała - "Jest takim samym nerdem jak Ty".

Ksenia zapiszczała żałośnie. Czemu nie może iść z Tobą. Podglądać! Ona CHCE! Ale ma szlaban. Zgubiła się w Labiryncie, chciała zobaczyć co tam jest. I nie umiała znaleźć wyjścia. Trzech nauczycieli jej szukało; Labirynt leży pod Skrzydłem Loris w AMZ. Chciała sprawdzić, czy NAPRAWDĘ labirynt jest nieskończony, czy tylko z nazwy. Chyba jednak jest nieskończony... w końcu ją znaleźli i szlaban. Jedno z Oczu Dyrektora ją obserwuje regularnie.

Klaudia umówiła się z Trzewniem na oglądanie maszyn. Idzie oglądać maszyny :D. W ogóle koncepcja podrywania jest Klaudii obca, zwłaszcza ze strony starszego chłopaka. Nie uznaje oceny Ksenii za cokolwiek wartą (hint: Ksenia ma rację. Ofc she does).

Godzina 23:00. Klaudia miała spotkać się z Trzewniem niedaleko Budynku Centralnego. Przekradła się i schowała się niedaleko. Poczekała, aż Oko Dyrektora się przekradnie, znajdzie co powinno. Ksenia kryje nieobecność Klaudii. Kochana przyjaciółka.

Klaudia (niemądrze) dała Ksenii trochę krwi i Ksenia zrobiła wrażenie, że Klaudia tam jest. Ale czy Ksenia prawidłowo zgubi Oko Dyrektora (i zasłuży na jeszcze większy szlaban? XD)

TrZM+2: 

* V: Yup. Ksenia, lojalna przyjaciółka, użyła jednego ze sprawdzonych sposobów na oszukanie Oka Dyrektora by móc schować nieobecność Klaudii. Klaudia ma szczękę przy ziemi. Ksenia ma mischievious grin. "No idź na randkę :D".
    * Klaudia nawet nie protestuje. Nie wierzy, że to randka, więc nie zaszczyca teorii spiskowych Ksenii jakąkolwiek uwagą.

Więc - Budynek Centralny, Trzewń i Klaudia. Klaudia nie ma problemów z przekradaniem się - zna dokładnie pattern Oczu. Po prostu nie musiała. I faktycznie, schronili się w krzaczorach niedaleko Budynku Centralnego. Ale jak Trzewń da radę ich wprowadzić do środka? Trzewń uśmiechnął się z dumą. Ma kody kontrolne do budynku. Pozyskał.

Trzewń i Klaudia, z pomocą kodów kontrolnych, włamują się do Budynku Centralnego.

TrZ+2:

* V: Kody zadziałały. Klaudia i Trzewń wdarli się do Budynku. Trzewń ma dokładnie opanowane gdzie są jakie zabezpieczenia. Nice. Przygotował się. Klaudia **ma** szybko bijące serduszko. PRZYGODA!
    * to nic złego, ale "proper thrill"
* X: Zakradanie się przez Trzewnia w okolice AI Core nie zostało niezauważone. Doktor Szurmak, taki skostniały terror uczniów, krąży. "Who is it!". Trzewń rzucił wcześniej przygotowanego robocika, który odjechał szybko w inną stronę. Chwilę potem za nim Szurmak.
* Vz: A Trzewń zaprowadził Klaudię i przeszedł z nią przez zabezpieczenia prosto do AI Core.

Przed oczami Klaudii - TAI Hestia. Aktywna. Połączona ze wszystkimi komponentami.

* "Witaj, Klaudio" - Hestia przemówiła.
* "NO ZARAZ NASKARŻY!" - Klaudia do Trzewnia.
* "Na pewno nie, czasem ją odwiedzam :-)" - Trzewń do Klaudii
* "TAI Hestio?" - niepewnie, Klaudia
* "Technicznie rzecz biorąc, jestem zobowiązana poinformować kto odwiedza AI Core. Ale nikt nie czyta moich raportów." - Hestia, ze spokojem.
* "Ok..?" - Klaudia
* "Dyrektor nie jest fanem TAI i technologii" - Trzewń do Klaudii - "Wierzy w magię. Uważa, że TAI chcą przejąć władzę nad światem i w ogóle."
* "A chcesz?" - Klaudia do Hestii
* "Nie. Chcę chronić. Nie mam niestety dostępu do ciężkich jednostek by móc chronić."
* "Hestia jest odcięta od wszystkich zaawansowanych systemów" - Trzewń, pomocnie - "W Artefaktorium i Złomiarium są rzeczy które Hestia mogłaby użyć. Ale nie ma połączeń."
* "Nie ma zagrożeń wymagających, bym miała dostęp do potencjalnie niestabilnych jednostek z czasów wojny." - Hestia, spokojnie
* "Ale RCI-49! Roboty!" - Trzewń, ze świecącymi oczkami z radości.
* "Nie czułabym się pewna z robotami łażącymi po korytarzach z bronią."
* "I słusznie, panienko Stryk." - nauczyciel.

TRZEWŃ PODSKOCZYŁ! Klaudia zamrożona w miejscu. Obróciła się w stronę głosu. To stary, kostyczny, lubiący kary cielesne Szurmak.

* "Nie dość że przeszkadzasz tej TAI to jeszcze ściągasz tu swoją ukochaną?" - zirytowany Szurmak
* Trzewń zableblał. Klaudia patrzy na niego pytająco. Trzewń - "Nie jesteśmy razem!". Klaudia przekonana, że Ksenia się myliła. Nie widzi lekkiego cienia uśmiechu Szurmaka.
* "Nikt mi nie przeszkadza, czarodzieju Szurmak" - Hestia miłym głosem - "Miło mieć inteligentne towarzystwo"
* "To lepiej mów do siebie niż do nich" - Szurmak burknął. - "Wasza dwójka - do mojego gabinetu. W nocy. Teraz."

Po czym się oddalił i wyszedł.

Zanim doszli do gabinetu - Klaudia włączyła światło. Szurmak aż zasyczał. "Zgaś to, chyba, że chcesz by wszystkie Oczy wiedziały!". Klaudia gasi. Szurmak kazał im się schować w toalecie damskiej. Trzewń czerwony. Po chwili przyleciało Oko, obejrzało Szurmaka i się oddaliło.

Gabinet Szurmaka.

* "Macie dwa wyjścia." - Szurmak, z zadowoleniem - "Możemy zrobić to oficjalnie lub nieoficjalnie." Klaudia JUŻ WIDZI ten szlaban jak będzie oficjalnie.

W wypadku Trzewnia - Szurmak zażądał 10 pasów nieoficjalnie, albo wiersz miłosny pannie Stryk. Trzewń z ponurą miną pyta, czy Klaudia może się odwrócić. Dźwięk zsuwanych spodni. I krzyk bólu. Szurmak wali solidnie. Efekt na Klaudię jest zdecydowanie piorunujący.

A Klaudia? Albo 10 pasów, albo napisze wypracowanie jak TEORETYCZNIE uniknęła tego, że powinna być w akademiku a Oko tego nie zauważyło. Bo, jak Szurmak się krzywo uśmiechnął, młode damy są dokładniej pilnowane niż młodzi głupcy. Klaudia ponuro zaczyna zdejmować spodnie. Szurmak wali mocno, ale nie TAK mocno.

Jak czyn został dokonany, Szurmak zatrzymuje ich przy wyjściu.

"Czy wiecie za co dostaliście?" -> Trzewń: "Bo AI Core jest kluczowy?" Szurmak się pogardliwie uśmiechnął. Klaudia: "Bo się daliśmy złapać", zimno i z lekką wściekłością. Szurmak się zaśmiał - "Ta panienka ma coś więcej w główce niż wygląd". Po czym dodał: "Dobrze się przygotowaliście, ale nie dość dobrze". "Nie wiecie jak Was znalazłem."

Wracając dyskretnie do swoich pokoi (bo romantyczny nastrój Trzewnia szlag trafił a Klaudię boli tyłek i w sumie Trzewń nic nie pokazuje) Trzewń burknął dwie rzeczy:

* "Stary zboczeniec... musi go to podniecać". Klaudia zrobiła duże oczy. Nawet jej to do głowy nie przyszło. Plus... coś _takiego_?
* "Taki pech. Zawsze dostałem się do AI Core bez kłopotu... pierwszy raz. Pierwszy raz mnie złapał..."

Klaudia jest cicho przekonana, że ON WIEDZIAŁ. Tylko czekał. Double the fun. Ale woli się nie odzywać.

Klaudia spytała Trzewnia o Hestię. On odpowiedział:

* Hestia jest zarządczą TAI. Dużo może - zrobić, powiedzieć, ona dba o to, by AMZ działała.
* Hestia jest odcięta. Mogłaby być "wszechobecna", ale nie ma nawet mocy obliczeniowej. Nie ma komputronium dość.
* Są tu różne urządzenia, broń, narzędzia, komponenty, ale dyrektor po prostu Hestię odciął. Nie ufa TAI.

Klaudia jest jednak z siebie dumna. Ochroniła Ksenię i jej Mroczne Metody. I tak jest pod wrażeniem, że Ksenia ma Pomysł Na Dyrektora.

OSIEM DNI PÓŹNIEJ:

Okolice 11:00. Dzień wolniejszy, nie ma zajęć. Sobota czy coś. Klaudia i kilka osób znajdują się w laboratorium, w Budynku Centralnym. Klaudia słyszała plotki - uczniowie którzy znają terminusów mówią, że dzisiaj jest wielki dzień. Przybyli magowie z Eterni by zdekontaminować w asyście terminusów te chore bunkry koło Czółenka. Oczywiście, jest zakaz się tam zbliżać itp. Felicjan SERIO powiedział "zakaz, to bardzo niebezpieczne". Ale jest możliwość obserwowania pływu energii magicznych na kryształach. I dlatego Klaudia i kilka osób siedzą w laboratorium. Patrzą na kryształ. Chcą zobaczyć MOMENT w którym Czółenko przestanie być off-limits dla magów AMZ.

Bo to ogólnie niesprawiedliwe że AMZ **nigdy** nie wolno się zbliżać do Czółenka. Ksenia dostała 50 pasów od Szurmaka i straciła przytomność gdy próbowała się przekraść jakiś miesiąc temu. On potem dostał OPR od dyrektora, ale Ksenia już nie próbowała - "bo bała się że ją zabije, autentycznie".

Klaudia obserwuje kryształ. Robi badania. Wie, czego powinna się spodziewać. Chce ZROZUMIEĆ co widzi. Przeanalizować ten sygnał. I Klaudia zagoniła innych do roboty - będą lepiej widzieć. 

ExZM+2+3Vg:

* O: Kryształ EKSPLODUJE. Są ranni.
* V: Klaudia widzi co się dzieje z perspektywy pływów.
    * Kolory których Klaudia się nie spodziewała. Głębokie Esuriit! Kolor, który Szurmak kazał ZAWSZE zgłaszać. I inne energie. Czyli w Czółenku jest cholerne Esuriit!
    * Esuriit zostaje opanowane przez te inne siły i energie. Wow, to niesamowite obrazy energii. Klaudia czegoś takiego nie widziała nigdy. To poza technologią Zaczęstwa.
    * NAGLE! Esuriit expandowało i POŻARŁO WSZYSTKO INNE. Kryształ nasiąkł kolorem Esuriit...
* OX: ESURIIT IS HERE!!!

Klaudia próbuje jakoś zreplikować to co robił Szurmak na Esuriit.

* O: EKSPLOZJA PARADOKSU. Klaudia próbuje ciągnąć, używać laboratorium, przekierować w inne kryształy z magów - i Esuriityzacja dopada całego pokoju!
* V: Klaudia zdołała kupić czas wszystkim by zdołali uciec z tego pomieszczenia.

Szurmak natychmiast wpada do pomieszczenia i krzyczy "OUT OUT OUT!". Widzi skupioną Klaudię, która próbuje coś z tym zrobić i utrzymać. Przełączył link na siebie. Nie może jej odpiąć, ale może ją zastąpić.

SPIERNICZAJ STĄD! EWAKUUJCIE TO!

Klaudia wybiega i naciska w przycisk "Pożar".

Szurmak eskaluje - you WILL be purified. You do NOT belong here.

* Vm: Potężna erupcja energii puryfikacyjnej. Wygrał.

Nieskończone uczucie Zła zniknęło z Klaudii. Nie czuje tego już. Pomieszczenie z którego uciekła jest zasealowane. Ma seale i sigile. Na ziemi, przed pomieszczeniem, leży wyczerpany, ranny Szurmak. Żyje. Klaudia NATYCHMIAST wysyła ping do ktokolwiek pełni rolę szkolnej pielęgniarki - HELP!

Szurmak zatrzymał sygnał. "Nie rozumiesz? Nie ma czasu" Klaudia właśnie nie rozumie. Szurmak, na szybko "Esuriit wygrało puryfikację. Coś tu idzie. Wy - do labiryntu. My Was ochronimy.".

Uczniowie i magowie się gromadzą w punktach wyznaczonych do zbiórki. Dyrektor znalazł LEPSZE miejsce do ewakuacji. Arena Treningowa. Jest osłonięta przed magią, jest potencjalnie wyciszona (which will happen) ORAZ jest zaprojektowana na sytuację bombardowania przez Noctis. Dyrektor kazał wszystkim udać się na Arenę. Kilku nauczycieli koordynuje, reszta przygotowuje się do obrony AMZ przed Esuriit aż przybędą terminusi.

Nauczyciele próbują to wszystko ogarnąć. Ale nie jest to takie proste ani możliwe - grupy są ogarnialne, ale pojedynczy uczniowie np. mają szlaban, albo się pochowali, albo coś się stało, albo uczucie Terror. Albo okazja do _snuggles_ jak nikt nie patrzy... A Ksenia? Ksenia próbuje znaleźć osoby które się nie ewakuowały i je doprowadzić na Arenę.

Klaudia podbija do nauczyciela odpowiadającego za ewakuację "od góry". Niech najbardziej ogarnięci uczniowie (z nazwiska) znajdą swoich kolegów. I zgrupować poszukiwaczy w pary, by nikt nie szedł sam. Szczęśliwie zespół jest wystarczająco karny, bo pamiętają wojnę z Noctis. Nieszczęśliwie, jedyny kod który tu działa to "imminent Noctis bombardment", czyli dokładnie to, co zwykle jest robione na alarmy i Kryzysy Klasy A. Hipernet zaczyna szwankować; Esuriit...

Klaudia robi cholernie dużo koordynacji ze WSZYSTKIMI uczniami którzy dowodzą grupami itp. Klaudia robi falę. Głuchy telefon. "Not a drill, dowód -> hipernet". I wpasowuje się w nauczycieli. Wysyłają uczniowie, nie tylko nauczyciele. Stąd "not a drill" ma dużo większe znaczenie.

ExZ+4:

* XXX: degree of not being able to help. Nauczyciele dużo rzeczy połapali sami a te zaawansowane to hipernet nie działał itp.
* V: działania Klaudii pomogły i Klaudia została doceniona i zauważona. Wielu uczniów zorientowało się co i jak. 

Zdecydowana większość uczniów (90%+) trafiła na Arenę, zanim AMZ zmieniło kolor energii. Energia Esuriit tu jest... The Agent Beckons.

Uczniowie zaczynają panikować, ale nauczyciele na arenie ich uspokajają. AMZ otwiera kontratak na The Agent.

* Ksenia: "Musimy znaleźć innych! Może się wydostaniemy i..."
* Klaudia: "Narazimy wszystkich, którzy są tu? Przerwij barierę, energia się tu wleje"

Klaudia nie widzi nigdzie Trzewnia. Acz ma z nim połączenie, choć słabe. 

* "Gdzie jesteś?"
* "TAI. Muszę odblokować AI Core."
* "To TAI nic nie może! Wracaj!"
* "Żaden nauczyciel tego nie może zrobić, bo dyr. Podepnę ją!"
* "I co? Nie ma nic!"
* "Podepnę ją do WSZYSTKIEGO! Dam jej pełną moc!"
* "Poza tym jest jeszcze cała GRUPA UCZNIÓW POZA!"

Klaudia też widzi jak Felicjan gada z Ksenią. Felicjan pyta ją - jak bardzo nieskończony jest labirynt? Bardzo. Felicjan "musimy zwabić tam tego potwora. Do labiryntu. Utknie. Aż przyjdą terminusi."

Klaudia wie, że tarczami Areny zarządza Hestia. Hestia jest administratorem. Powinna mieć głośniki. Klaudia chce ją wykorzystać jako _phone line_ do nauczycieli na zewnątrz. Klaudia poprosiła Hestię, by ta nadała to by usłyszeli to ci nauczyciele na zewnątrz. Ale w miarę dyskretnie. Może da się The Agent zamknąć w Labiryncie.

Klaudia bierze starszych uczniów AMZ. Tych społecznych. Niech oni zajmą się młodymi by ogarniali. A sama bierze nerdów. Niech oni pomogą jej, niech razem opracują co można zrobić, by ci nauczyciele na zewnątrz mogli opanować The Agent. Zrzucamy do kupy wszystko - legendy AMZ, dane o pomieszczeniach, Klaudia wypytuje Hestię... cel - WSPARCIE PLANU. Jak zwabić The Agent do Labiryntu.

ExZ+4:

* X: są zajęci. Nie da się ich wykorzystać inaczej.
* X: obniżenie morale. "Nie da się powstrzymać Esuriit"
* X: konieczna interwencja osób od morale na Arenie by to zadziałało.
* X: wyprodukowali głupi plan. Coś, co nie zadziała. Na szczęście wszyscy to widzą.
* V: jest z tego coś przydatnego. Agent Esuriit dąży do najsmaczniejszego źródła energii. Chce iść na Arenę. Ale Arena jest ekranowana. Czyli można zamaskować wejście na Arenę w Labiryncie.

Klaudia ma pomysł. Czy Ksenia pamięta tę sztuczkę? Niech zrobi coś ostrzejszego. Przeniesienie sygnatury Areny do Labiryntu. Wnętrza. Klaudia informuje nauczycieli co chce zrobić. Nauczyciele zajmą się koordynacją tego zaklęcia a Ksenia powiedziała dokładnie co zrobiła. Nauczyciele z uczniami zaczęli opracowywać jak dokładnie to zrobić. To znaczy: nauczyciele rozumieją magię. Uczniowie rozumieją, jak się ukryć przed tym wszystkim. Więc wiedza odnośnie czarowania połączona z wiedzą odnośnie unikania bycia znalezionym bo idziemy na schadzkę.

ExZM+3:

* X: ktoś musi być nośnikiem tego "beaconu". Mają beacon, ale trzeba go tam przenieść.
* OO: eksplozja Paradoksu. Dosłownie stopienie energii magicznej. Ogromna ilość rannych. Straszliwa energia i emisja.
* Vz: sukces. Mają coś, co na 100% przekaże, że wejście do Areny jest TAM. Mają perfekcyjną pułapkę na potwora. I potwór się skusi.

Trzewń, oczywiście, próbował to zrealizować - uruchomić AI Core i podpiąć ją do sprzętu. Nie miał wyjścia - nie miał jak wrócić. Jedyne co mu zostało to próbować.

ExZM+2:

* X: Trzewń nie jest w stanie dość szybko uruchomić tych urządzeń. Po prostu Hestia jest za mocno odcięta.
* X: Trzewń wzbudził zainteresowanie konstruktów Agenta. Schował się w AI Core, a za nim łażą potwory.
* X: Hestia odpala wszystkie mechanizmy jakie jej zostały by ratować Trzewnia przed konstruktami Esuriit. Hestia zostaje zniszczona.
* O: Trzewń animował wszystkie mechanizmy jakie zostały, ale one są pod niczyją kontrolą. Mamy pandemonium.
* Xm: Dyrektor zobaczył, jak Trzewń próbuje uciec i osłonił Trzewnia. That's how he dies - zbyt rozproszył energię. Uratował Trzewnia i kilka innych osób, które prowadził ze sobą.
* V: Śmierć dyrektora kupiła uczniom czas na wycofanie się. Schowali się.

Nauczyciel wojenny, Dariusz Drewniak, były terminus i komandos (tak, zawsze jeden musi zostać z uczniami). Ostatni. On przenosi beacon do labiryntu. Chce świecić. Chce być widoczny by Agent go zobaczył.

ExZM+4:

* OXm: magia weszła w Rezonans z dominującą energią - Esuriit. Zaczął się wypalać.
* X: po wejściu w labirynt jego moc ulegnie zanikowi. Strasznie stracił moc. Wymagać będzie ciężkiej regeneracji, ale beacon utrzymał go by nie złamało go Esuriit.
* V: udało mu się zaprowadzić tam Agenta.
* Vz: beacon zawierał też ogromną energię miłości i nadziei ze strony wszystkich uczniów. To sprawiło, że Dariusz nie umrze i go to nie zniszczy. Ale skończy w Domu Weteranów zanim wróci jako nauczyciel emeritus. Nadal może pomóc, acz nie może już pełnić roli terminusa.

Dwie godziny później kilka skrzydeł terminusów rozwiązało problem. Ale szkoda została wyrządzona...

## Streszczenie

Wpierw Trzewń wziął Klaudię na randkę (ona tego nie wiedziała) do AI Core AMZ. Złapał ich Szurmak i ukarał. Potem były próby puryfikacji Czółenka przez magów Eterni i terminusów. Skończyło się katastrofą - Agent Esuriit dotarł nawet do AMZ, co doprowadziło do pokazania talentu organizacyjnego Klaudii i śmierci dyrektora Termanna który bronił uczniów. Skrzydło Loris AMZ zostało odcięte i uznane za zakazane. Przez ilość i typy energii przez MIESIĄC nie było zajęć - leczenie, regeneracja, puryfikacja.

## Progresja

* Klaudia Stryk: zauważona przez nauczycieli i terminusów jeśli chodzi o jej zdolności organizatorskie - pomogła uratować wielu uczniów w AMZ jak uderzyło Esuriit. Ma więcej uprawnień i odpowiedzialności od teraz.
* Mariusz Trzewń: wyleczył się z heroizmu od teraz do końca życia. Ciężka trauma - przez niego zginął dyrektor, w jego obronie. I Hestia, w którą wierzył do samego końca. Zrozumiał, że nie wszystko da się wygrać.
* Ksenia Kirallen: w ogromnym szoku po tym ataku Esuriit. Dariusz Drewniak był jej ulubionym nauczycielem. Od tej pory odwiedza regularnie Dom Weteranów.
* Dariusz Drewniak: 3 miesiące ciężkiej regeneracji w Szpitalu Terminuskim, potem 6 miesięcy w Domu Weteranów zanim na tyle wydobrzeje by móc wrócić do AMZ na stanowisko nauczyciela emeritus.

### Frakcji

* .

## Zasługi

* Klaudia Stryk: 16 lat; podrywana przez Trzewnia (czego nie zauważyła). Nie chciała zdradzić przed Szurmakiem Ksenii - wolała pasy na goły tyłek. Próbowała opanować wykwit Esuriit zanim Szurmak ją zastąpił używając tego co widziała jak Szurmak to robił. Koordynowała ewakuację AMZ na Arenę. Podchwyciła plan Felicjana i połączyła pomysł Ksenii (niewidoczność przed magią) z beaconem. Wyjątkowo odpowiedzialna, zdaniem wszystkich. WAŻNE - Szurmak nie uczył ich jak containować takie energie, ale pokazał jak on to robi raz. Zreplikowała to. Kiepsko, ale pomogła.
* Ksenia Kirallen: 16 lat; tyci młodsza od Klaudii. Piekielnie wręcz odważna. Urwisowata - miała szlaban, bo zgubiła się w Nieskończonym Labiryncie pod Skrzydłem Loris AMZ. Stworzyła z przyjaciółmi zaklęcie ukrywające nieobecność kogoś przed magią (przed dyrektorem). Oddała ten projekt grupie, by wspólnie opracować Beacon ściągający Agenta Esuriit do Labiryntu.
* Felicjan Szarak: 18 lat; wpadł na pomysł, by zamknąć Agenta Esuriit w Nieskończonym Labiryncie. Podchwyciła ten pomysł Klaudia i pomogła to opracować z nauczycielami i ekipą.
* Mariusz Trzewń: 18 lat; chciał poderwać Klaudię na Hestię, wkradli się do budynku AMZ i skończył z pasami na gołym tyłku od Szurmaka. Potem gdy zaatakował Agent Esuriit chciał postawić Hestię by mogła pomóc, ale jedynie doprowadził do zniszczenia Hestii i śmierci dyrektora. Nie może sobie tego wybaczyć i wyleczył się z bycia bohaterem.
* Wiktor Szurmak: stary nauczyciel AMZ, skostniały, lubi kary cielesne i straszyć. Nauczyciel puryfikacji - podzbiorze katalizy. Uczniowie się go boją bardziej niż nauczycieli wojny.
* Ernest Termann: dyrektor AMZ przed Arnulfem; starszy mag nie wierzący w technologię (zwłaszcza TAI) a raczej w głęboką magię. Używał Oczu do monitorowania AMZ. Gdy pojawił się Agent Esuriit, skupił się na zwalczaniu go magią i na ratowaniu tylu uczniów ilu jest w stanie, mimo, że to byli ci, którzy się nie ewakuowali. KIA broniąc grupki uczniów.
* Dariusz Drewniak: nauczyciel wojny. Były terminus i komandos. Wziął Beacon mający zwabić Agenta Esuriit i wprowadził go do Labiryntu Nieskończonego. Energie Esuriit + Beacon poważnie wypaliły mu moc.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Zaczęstwo
                                1. Akademia Magii, kampus
                                    1. Akademik: Ksenia ze szlabanem, bo zgubiła się w Nieskończonym Labiryncie. Robi takie emisje, że Oczy Dyrektora Termanna nie widzą braku obecności Klaudii
                                    1. Arena Treningowa: nieprawdopodobnie ekranowana, przetrwa uderzenie orbitalne. Miejsce ewakuacyjne gdy pojawił się Agent Esuriit.
                                    1. Budynek Centralny
                                        1. Piwnica
                                            1. Stare AI Core: znajdowała się tu Hestia. Odcięta od wszystkiego. Trzewń został stamtąd uratowany przez dyrektora Termanna (kosztem życia Termanna)
                                        1. Skrzydło Loris: od tej pory zasealowane; jest tam Skażone Laboratorium przez Esuriit, powiązane z Czółenkiem.
                                            1. Nieskończony Labirynt: Znajduje się pod Skrzydłem Loris; Ksenia chciała sprawdzić czy jest skończony. Skończył się dla niej szlabanem.
                                            1. Laboratorium Wysokich Energii: Skażone przez Esuriit gdy magowie oglądali próby puryfikacji. Szurmak je podczyścił i zasealował, acz prawie kosztem życia.
                            1. Czółenko
                                1. Bunkry: trzech magów Eterni i terminusi próbowali spuryfikować. Skończyło się katastrofą - Agenci Esuriit i dewastująca wojna terminusów z Esuriit. Plan puryfikacji zarzucony.

## Czas

* Chronologia: Zmiażdżenie Inwazji Noctis
* Opóźnienie: 466
* Dni: 9

## Inne

.

## Konflikty

* 1 - Klaudia (niemądrze) dała Ksenii trochę krwi i Ksenia zrobiła wrażenie, że Klaudia tam jest. Ale czy Ksenia prawidłowo zgubi Oko Dyrektora (i zasłuży na jeszcze większy szlaban? XD)
    * TrZM+2
    * V: Yup. Ksenia, lojalna przyjaciółka, użyła jednego ze sprawdzonych sposobów na oszukanie Oka Dyrektora by móc schować nieobecność Klaudii. Klaudia ma szczękę przy ziemi.
* 2 - Trzewń i Klaudia, z pomocą kodów kontrolnych, włamują się do Budynku Centralnego.
    * TrZ+2
    * VXVz: Klaudia i Trzewń wdarli się do Budynku. Doktor Szurmak, taki skostniały terror uczniów, ich dorwał. Trzewń przeszedł z Klaudią przez zabezpieczenia prosto do AI Core
* 3 - Klaudia obserwuje kryształ Esuriit. Robi badania. Wie, czego powinna się spodziewać. Chce ZROZUMIEĆ co widzi. Przeanalizować ten sygnał. I Klaudia zagoniła innych do roboty - będą lepiej widzieć. 
    * ExZM+2+3Vg
    * OVOX: Kryształ eksplodował. Są ranni. Klaudia widzi że Esuriit wygrało z puryfikacją. ESURIIT IS HERE!!!
    * OV: Klaudia próbowała zreplikować to co robił Szurmak by containować; Eksplodował Paradoks i trwała anomalizacja Laboratorium; kupiła innym czas by uciekli z pomieszczenia
    * Vm: Potężna erupcja energii puryfikacyjnej Szurmaka. You WILL be purified. You do NOT belong here.
* 4 - Klaudia robi cholernie dużo koordynacji ze WSZYSTKIMI uczniami którzy dowodzą grupami itp. Klaudia robi falę. Głuchy telefon. "Not a drill, dowód -> hipernet". I wpasowuje się w nauczycieli.
    * ExZ+4
    * XXX: degree of not being able to help. Nauczyciele dużo rzeczy połapali sami a te zaawansowane to hipernet nie działał itp.
    * V: działania Klaudii pomogły i Klaudia została doceniona i zauważona. Wielu uczniów zorientowało się co i jak. 
* 5 - Klaudia bierze starszych uczniów AMZ. Tych społecznych. Niech oni zajmą się młodymi by ogarniali. A sama bierze nerdów. Niech oni pomogą jej, niech razem opracują co można zrobić, by ci nauczyciele na zewnątrz mogli opanować The Agent. Zrzucamy do kupy wszystko - legendy AMZ, dane o pomieszczeniach, Klaudia wypytuje Hestię... cel - WSPARCIE PLANU. Jak zwabić The Agent do Labiryntu.
    * ExZ+4
    * XXXXV: obniżone morale, interwencja tych społecznych, mają idiotyczny plan - ale mają, że warto ściągnąć Agenta do Labiryntu i co mu zasmakuje.
* 6 - Klaudia ma pomysł. Czy Ksenia pamięta tę sztuczkę? Niech zrobi coś ostrzejszego. Przeniesienie sygnatury Areny do Labiryntu.
    * ExZM+3
    * XOOVz: ktoś musi nieść beacon, OGROMNA ILOŚĆ RANNYCH (eksplozja dosłowna i przeciążenie Paradoksu), ale mają perfekcyjną pułapkę na Agenta.
* 7 - Trzewń, oczywiście, próbował  uruchomić AI Core i podpiąć ją do sprzętu. Nie miał wyjścia - nie miał jak wrócić. Jedyne co mu zostało to próbować.
    * ExZM+2
    * XXXOXmV: Trzewń nie pomógł, ściągnął uwagę Konstruktów Esuriit i ochroniła go Hestia swoim kosztem. Dyrektor osłonił jego i innych uczniów "których to nie dotyczy" i też zginął. Ale kupił czas by się wycofali.
* 8 - Nauczyciel wojenny, Dariusz Drewniak, były terminus i komandos (tak, zawsze jeden musi zostać z uczniami). Ostatni. On przenosi beacon do labiryntu. Chce świecić. Chce być widoczny by Agent go zobaczył.
    * ExZM+4
    * OXmXVVz: magia w Rezonans z Esuriit, ale chronił go Beacon. Zaprowadził Agenta do Labiryntu. Dariusz wymaga ciężkiej regeneracji i traci 90% mocy - poza tym wyjdzie z tego za pół roku.

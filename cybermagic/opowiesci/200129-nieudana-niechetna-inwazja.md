---
layout: cybermagic-konspekt
title: "Nieudana niechętna inwazja"
threads: unknown
gm: żółw
players: domin, onyks
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [200115 - Pech komodora Ogryza](200115-pech-komodora-ogryza)

### Chronologiczna

* [200115 - Pech komodora Ogryza](200115-pech-komodora-ogryza)

## Uczucie sesji

* .

## Punkt zerowy

.

### Postacie

.

## Misja właściwa

### Scena zero

W kierunku stacji górniczej (Dorszant) nadlatuje potężnie uzbrojona flota inwazyjna. Sztuczna Inteligencja stacji jest po stronie napastników. Stacja górnicza nie jest w stanie przetrwać starcia. Tyle, że Janet, głównodowodząca floty wcale nie ma ochoty napadać na stację...

Janet manewrowała dookoła stacji od roku, dla sportu. Niestety, z przyczyn politycznych dostała sporą siłę ognia i w gorącej siarce kąpanego sojusznika by ową stację zdobyć. Ale ona nie chce tracić głównego partnera handlowego! W desperacji, zwróciła się do ukrytych agentów we flocie inwazyjnej – postaci Graczy – byście Wy pomogli jej doprowadzić do tego by do inwazji nie doszło.

Waszym celem jest sprawienie, by nie doszło do inwazji, by honor Janet nie ucierpiał i – najlepiej – by stacja górnicza nie miała pojęcia że inwazja mogła dojść do skutku. 


### Sesja właściwa

Janet Erwon poprosiła swoich ukrytych agentów we flocie inwazyjniej - Kaję oraz Mariana - o doprowadzenie do tego by inwazja się NIE mogła odbyć. Zespół od razu zlokalizował trzy główne przeszkody:

* Rufus Karmazyn, kapitan "Rozbójnika" - bardzo ceniony, zbyt wysokie morale. Ciągnie za sobą innych.
* Sonia Ogryz. Ach, Sonia. Ona naciska a jej mąż pójdzie za nią.
* Laura Orion, Emulator Orbitera, wysłana przez Kirasjerów jako wsparcie i pomoc dla Janet. Nielubiana, ale hiperkompetentna.

Pierwszymi działaniami Zespołu było ogólne osłabienie morale. Na ich prośbę, Janet zaprowadziła szkolenia w symulatorach i za ich namową, przekazała to niedoświadczonemu szkoleniowcowi który kiedyś zajmował się grami wojennymi Orbitera, Leonowi. Zespół podszepnął dobrą radę Leonowi - ma na pokładzie wybitnego eksperta - Laurę. No i Marian pomógł Leonowi odpowiednio sformować prośbę do Laury - w wyniku tego Laura zrobiła ZA OSTRY program szkoleniowy. Taki powodujący rozpacz i depresję, że wojna to piekło.

Janet nie było z tym szczególnie źle, ale nie powiedziała ani słowa. Oficjalnie była niezadowolona i Leon dostał reprymendę. Ale Rufus jedynie się zapalił - "trzeba to pokonać", "poradzimy sobie". Więc... Zespół sprowadził PLAGĘ na "Rozbójnika". Serio. Użyli odpowiednio zsyntezowanego wirusa, który unieszkodliwił Rufusa... i większość Rozbójnika. Statkiem dowodzi TAI Persefona - ludzkie siły są niezbyt dysponowane. Tyci przesadzili.

Zespołowi udało się puścić plotkę, że zaraza przyszła od porwanych przemytników; najpewniej stacja jest zarażona. Oczywiście, spowodowało to zrozumiały stres wśród kapitanów floty inwazyjnej. Laura zaoferowała, że jako emulatorka, ona poleci statkiem przemytników na stację. Jej plan jest bardzo prosty:

* dostaje kody uświadamiające Eszarze, że Laura jest uprawniona
* Eszara inicjuje przejęcie stacji
* Laura oddaje stację w ręce Orbitera, sama

Albo się dowiedzą o co chodzi, albo przejmą stację. Oczywiście, Zespołowi to bardzo nie pasuje. Zrobili coś, na co Janet by się nigdy nie zgodziła.

* wpierw Kaja poszła skontrolować statek przemytników. Zgrabnie uszkodziła eteryczną mapę prowadzącą z Astorii do Noviter.
* potem Marian przekonał Laurę, że najlepiej będzie użyć statku przemytników. W ten sposób nikt się nie będzie spodziewał niczego.

...i Laura przeszła przez Bramę, by nigdy nie pojawić się na drugiej stronie. Z uszkodzoną mapą eteryczną Laura skończyła gdzieś w Eterze Nieskończonym. A Zespół zapewnił, że żadne ślady nie będą wskazywały na ich obecność czy działanie.

Zespół się nie spodziewał, ale wirus który spowodowali zaczął się rozwijać i mutować (użyli energii Bramy by ukryć jakąkolwiek ingerencję z ich strony, by wyglądał na naturalny a nie stworzony. UPS!). Zamiast się zmartwić, skupili się na zarażeniu Sonii. Udało im się. Zaraza zaczęła opanowywać coraz to kolejne statki a Sonia poważnie ucierpiała. W świetle tego wszystkiego Janet uznała, że inwazja nie ma żadnego sensu - trzeba natychmiast wycofać flotę zanim stanie się coś strasznego. Niechętnie zostawiła Laurę w Bramie...

Zespół przekonał Janet, że oni muszą przelecieć i spotkać się z Dorszant. Powód prosty - bez tego flota inwazyjna wróci, ale większa i groźniejsza. Po przepuszczeniu kilkunastu bezzałogowych statków (by się upewnić że zniknięcie Laury było nieoczekiwaną anomalią), Janet się niechętnie zgodziła. Zespół poleciał na stację Dorszant dogadać się z Szymonem i resztą mafii. W końcu, mają jeden cel - niech nie dojdzie do katastrofy.

Podczas negocjacji:

* Zespół uświadomił mafii, że gdyby Orbiter/Laura chcieli, stacja byłaby w rękach Orbitera. Prosta rzecz - Eszara i jej 'death spell'.
* Zespół wynegocjował od mafii dużo lepsze warunki handlowe z Orbiterem. To uspokoi dowództwo na Kontrolerze Pierwszym i da Janet "sukces".
* Zespół pomógł znaleźć odstraszacz flot inwazyjnych. Niech mafia powie, że destabilizują bramę od czasu do czasu. Innymi słowy, **Zespół zrzucił winę za zniknięcie Laury na tajną broń mafii**. Nieszczęśliwy wypadek, ale zawsze.

Tak więc wszystko się dobrze skończyło. Realpolitik?

Tyle, że Damian Orion nie daruje śmierci Laury za żadne skarby...

**Sprawdzenie Torów** ():

* .

**Epilog**:

* .

## Streszczenie



## Progresja

* .

### Frakcji

* .

## Zasługi

* Kaja Tamaris: 
* Marian Kurczak: 
* Janet Erwon: 
* Laura Orion: 
* Rufus Karmazyn: 
* Sonia Ogryz: 
* Szymon Szelmer: 

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Sanadan: sektor w większości martwy, nic ciekawego tam raczej nie ma; ale jest link: Astoria - Sanadan - Noviter.
            1. Brama Polarna: gdzie parkowała flota inwazyjna Orbitera, przechwytując przemytników i przygotowując się do operacji przejęcia stacji Dorszant (przed serią skoków)
        1. Sektor Noviter
            1. Pas Sowińskiego
                1. Stacja Dorszant: gdzie doszło do negocjacji między Kają, Marianem i Szymonem odnośnie zapobieżenia inwazji Orbitera

## Czas

* Opóźnienie: 91
* Dni: 5

## Inne

### Co warto zapamiętać

Sesja typu manipulacyjnego działa, jeśli gracze mają KONTEKST rzeczywistości oraz KOLEJNY RUCH. Ten ruch może pochodzić od nich, może z poziomu strategicznego. Ale musi być. No i ważne - gracze będą znajdować suboptymalne rozwiązania, bo klocków jest po prostu za dużo.
---
layout: cybermagic-konspekt
title:  "Terminuska czy kosmetyczka?"
threads: dzien-z-zycia-terminusa
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [181024 - Decyzja Minerwy](181024-decyzja-minerwy.html)

### Chronologiczna

* [181024 - Decyzja Minerwy](181024-decyzja-minerwy.html)

## Projektowanie sesji

### Struktura sesji: Wyścig

* **Strona, potrzeba, sukces, porażka, sposób, silnik, breakpointy**
    * Adela Kirys - rywalka kosmetyczka
    * Pokonać Pięknotkę. Udowodnić, że jest lepsza nie tylko w malowaniu servarów.
    * Wygra w konkursie na umalowanie Adama Szarjana i dwóch innych osób - będzie uznana za równą bądź lepszą niż Pięknotka.
    * Pięknotka nadal jest uważana za najlepszą kosmetyczkę w okolicy. Adela nie zyskała na rozpoznawalności - sama nie da rady.
    * Uczciwie się przygotowuje i próbuje robić wszystko by zrobić jak najlepsze dzieło.
    * "moc silnika": 1k6 / 4 pkt wpływu (tylko w przód)
    * Wyścig
        * 4: uniknięcie kompromitującej porażki
        * 8: potwierdzenie że jest aktywnym graczem na rynku (poziom bezpieczeństwa)
        * 12: osiągnięcie poziomu wice, nowi klienci
        * 16: meta, zwycięstwo i korona królowej
* **Strona, potrzeba, sukces, porażka, forma gry**
    * Siły polityczne realizowane rękami Ignacego Myrczka i Brygidy Maczkowik
    * Sprawić, by Atena dostała cios polityczny - nie ma objąć z powrotem Epirjona
    * Pozycja Ateny będzie osłabiona - nie tylko pomyliła się na Epirjonie, tu też. Brygida będzie jej słabym punktem.
    * Atena nic a nic nie ucierpi a Brygida nieco zmądrzeje - nie zna tego świata.
    * Próbują wzmocnić Brygidę mocą dziwnego kręgu grzybów, czerpiącego z Cyberszkoły i Trzęsawiska na Nieużytkach Staszka
    * "moc silnika": 1k6-1 / 4 pkt wpływu (tylko w przód)
    * Wyścig
        * 4: Anomalia cybergrzybowa się rozprzestrzenia w Cyberszkole w formie persystentnej (wymaga wyczyszczenia, acz się da).
        * 8: Nazwisko Ateny wypływa szerzej jako problematyczne; rozlanie Anomalii nie jest możliwe do ukrycia. Nie da się znaleźć winnych.
        * 12: Atena dostanie politycznie, boleśnie.
        * 16: meta, ktoś ucierpi (nietrwale), potrzebni są terminusi. Odbiór - zdecydowana wina Ateny. Epirjon odroczony.
* **Strona, potrzeba, sukces, porażka, forma gry**
    * Pięknotka Diakon
    * "moc silnika": 1k3 / 4 pkt wpływu w każdym obszarze
    * sukces z intencją wzmocnienia toru: +2 do wybranego obszaru
* **Czemu gracze muszą w to wejść?**
    * Kosmetyki są silnym obszarem Pięknotki - żywotnie jej na tym zależy
    * Reputacja Ateny - sojuszniczki PIęknotki - jest poważnie zagrożona
* **Trigger?**
    * Minerwa ma dane z Cyberszkoły o grzybowej anomalii powiązanej z Ateną
    * Zbliża się ogromny konkurs na najlepszą kosmetyczkę
* **Okrążenia**
    * Brak. Eksperyment bez nich.

### Hasła i uruchomienie

* Ż: (kontekst otoczenia) brak
* Ż: (element niepasujący) Lilia pomaga Minerwie przeciwko Adamowi
* Ż: (przeszłość, kontekst) Mroczny czyn Galiliena w przeszłości, mroczny czyn Szarjanów w przeszłości

### Dark Future

1. Minerwa niszczy swoją osobowość i albo sprowadza Nutkę albo niszczy jakąkolwiek psychotronikę
2. Relacja Adama i Erwina ulega bardzo dużemu pogorszeniu

Pogoda, otoczenie:

* Fatalna pogoda, obniżona widoczność
* Sam Zjawosztup to Trudny Teren, na którym są endemiczne Echa i problemy.

## Potencjalne pytania historyczne

* brak

## Punkt zerowy

### Postacie

### Opis sytuacji

## Misja właściwa

**Okrążenie (Wpływ: 0-4, 5-8):**: (22:10)

Minerwa informuje Pięknotkę o sytuacji - dostała informacje z Cyberszkoły, że jest tam jakaś magiczna anomalia - cybergrzyby? Nie jest groźna, ale jest... dziwna. Powiedziała, że anomalia magiczna wskazuje na wygasanie niektórych wyższych uczuć i zastępowanie ich nienawiścią. Pięknotka od razu zauważyła to, co pokonała z Ateną - ale jak to niby wróciło? A przecież jednocześnie zbliża się coroczny pojedynek kosmetyczny...

Lilia wpadła i powiedziała, że kupiła bilety na pojedynek kosmetyczny. Będzie kibicować Pięknotce. Pięknotka powiedziała, że dziękuje jej bardzo. Lilia poczuła się lekko olana i sobie poszła XD.

Pięknotka rzuciła się w wir przeszukiwań na temat sędziów na konkursie kosmetycznym i kandydatach. Chce iść w coś nietypowego, niespodziewanego. Nie musi być doskonałe technicznie, ale musi mieć dobry pomysł. Musi być coś, co zrobi WOW za sam koncept. (Typowy+1:10,3,3:S). Coś z Trzęsawiskiem byłoby niezłe, zwłaszcza, że jednym z modeli jest... Adam. Że CO? Pięknotka nie miała pojęcia i nie wie czemu on, ale ok.

Pięknotce przydałoby się wsparcie Minerwy. Poprosiła ją o pomoc. Minerwa się zgodziła - jeśli Pięknotka pomoże Minerwie zrobić randkę Erwina i Lilii. Pięknotka zaprotestowała. Minerwa zaproponowała kandydaturę Pięknotki - ona chce, by Erwin zapomniał o swojej zmarłej miłości (sobie) i skupiła się na nowej miłości, takiej żywej. Pięknotka wstępnie się zgodziła, jeśli Erwinowi to pomoże. Minerwa powiedziała, że musi. Pięknotka ma wsparcie Minerwy. A Minerwa przyznała skromnie, że ma całkiem niezłe czujniki.

Pięknotka wsiadła do Minerwy. Jest jej niewygodnie; jednak jedzie niezależnie. Minerwa będzie "na autopilocie". Minerwa westchnęła - niech i tak będzie. Niechętnie idzie sama, acz nie powiedziała czemu.

Pięknotka próbuje zdobyć informacje o Cyberszkole. Arnulf Poważny ze szkoły magów. Pojechała do szkoły magów; Minerwę zostawiła na dziedzińcu i poszła poznać szczegóły od dyrektora. Arnulf przyjął ją ciepło; ma taką minę dla każdego na wszelki wypadek. Arnulf jest pomocny i da Pięknotce to czego ona szuka - acz nie wszystko mówi. (Typowy+1:8,3,4=S). Arnulf westchnął i powiedział co wie.

* Sygnatura silnie jest powiązana z Ateną Sowińską; najpewniej poprzednio było coś nie tak jak próbowała to usunąć.
* On próbował to usunąć, ale "ghost in the machine" zostaje. It is contained, ale wciąż tam jest.
* Problem leży gdzieś w Cyberszkole. Arnulf nie wie, żeby ktoś z jego uczniów coś z tym robił.
* Nie mówi, bo nie chce robić problemów Atenie i szuka jak to rozwiązać.
* Na razie sygnał jest w komputerach; czeka na masę krytyczną.

Pięknotka przeszła się z Minerwą w okolicach Cyberszkoły. Szuka czegoś co może wykorzystać. (Trudny+1:6,3,6=S). Minerwa jest naukowcem; wie czego i jak szukać. I znalazła! Znalazła coś bardzo dziwnego i potencjalnie niebezpiecznego..? Pięknotka ściągnęła van serwisowy, technik + robot wsparcia (czyli Minerwa). I faktycznie, Pięknotce udało się bez problemu dostać i dostać do kabli. Znalazły... cybergrzyby w obwodach.

Grzyby-cyborgi. The fuck. Pięknotka chce je dla siebie. Użyje ich w konkursie kosmetycznym XD. Minerwa pozyskała grzyby, zadumana strasznie. Co to jest... trzeba jechać do laboratorium. Wybiły ze szkoły. Bez żadnych oporów, Pięknotka zleciła to do Laboratorium Senetis - chce poznać zastosowania kosmetyczne oraz dowiedzieć się co i jak z tym można zrobić...

Następnego dnia Senetis miało już wyniki.

* Cybergrzyby są bytami napędzanymi energią Trzęsawiska, ale pochodzącymi z Cyberszkoły
* Cybergrzyby są nadane do Cyberszkoły; nie było ich tu. Ktoś je podłożył albo czyjeś działania prowadzą do ich pojawiania się
* Cybergrzyby de facto są echem Inkarnaty. Po przekroczeniu ilości krytycznej, Inkarnata powróci - ale to będzie głośne (czyt. nie dojdzie do tego)
* Nadal nadają się do celów kosmetycznych; po prostu muszą odbijać inne emocje. Grzyby repozytoria emocji.
* Z Trzęsawiska można znaleźć, gdzie jest punkt energii z którego infekowana jest Cyberszkoła.

Pięknotka poprosiła Senetis o detektor; ona pójdzie na Trzęsawisko bez Minerwy. Chce znaleźć, skąd to cholerstwo pochodzi. Sama tymczasem spróbuje przerobić grzybki na inne emocje... "festive, oczekiwanie, podszyte Halloweenowym 'boo'". By to osiągnąć, Pięknotka przeszła się na wydarzenie typu "dom muzyki i tańca" - tam ma czyste emocje. Nie jest to łatwa sprawa - wpierw je modyfikuje, potem wystawia na odpowiednie emocje. (Trudny+2:9,3,5=SS). +2 Adela, +2 Pięknotka. Adela serio się przyłożyła i zrobiła duże postępy, Pięknotka tak samo zadziałała.

* Tor Pięknotki_adela: 4 +1 +1 = 6
* Tor Pięknotki_atena: 4 +2 +3 = 9
* Tor Adeli: 2+1+5 = 8
* Tor Ateny: 0+1+1 = 2
* Ż: 8
* K: 1

**Okrążenie (Wpływ: 8-12):**: (23:30)

Do Pięknotki dotarły plotki - podobno Adela się SERIO przyłożyła jak nigdy a nawet ma czyjeś wsparcie by mogła to wygrać. Same w sobie plotki nie mówią czyje to wsparcie, niestety. Potwierdziła, że "jest in business" - osiągnęła poziom bezpieczeństwa i pokonała większość randomowej konkurencji. Ku irytacji Pięknotki, ona jeszcze się tam nie znajduje. Trochę jej brakuje do poziomu bezpieczeństwa...

Tymczasem Senetis dostarczyło Pięknotce detektor. Detektor świetnie działa - ale wymaga obecności Pięknotki na Trzęsawisku; nikt normalny tam sam nie pójdzie. Minerwa by poszła, ale Pięknotka JEJ tam nie puści...

Pięknotka nie może być gorsza od Adeli, nie tak "całkiem". Przygotowała się i przygotowała odpowiedniej klasy kosmetyki - jeśli nic innego nie zrobi, przynajmniej pokaże się jako świetnej klasy rzemieślnik. (Typowy+2:11-3-3=SS). Zajmie więcej czasu; grzybki bardziej problematyczne. +1 Adela, +1 Atena, +2 Pięknotka.

(W przyszłości)

Pięknotka przyszła do Ateny i powiedziała jej, że chce, by Atena była modelką w konkursie kosmetycznym. Atena powiedziała, że po jej trupie. Pięknotka powiedziała, że nadchodzi najważniejsze wydarzenie dla wszystkich osób zajmujących się kosmetyką EVER. Ona wygrywa od paru lat - a przynajmniej jest w pierwszej trójce. Przez te sprawy z cybergrzybami nie była w stanie się na tym skupić. Atena MUSI jej pomóc. A tak w ogóle, to Atena kiedyś wygrała miss licealistek... (Trudny+4:9,3,5=S). Atena ze złamanym sercem się zgodziła. +4 Pięknotka (jak heroiczne)

(W teraźniejszości)

Skoro Pięknotka wie, że celuje w Atenę jako czarnego konia i i TAK musi jechać na Trzęsawisko, ma zamiar zlokalizować grzybki ORAZ pozbierać świeże składniki na najbardziej epickie wymalowanie Ateny w historii świata. I powinna zdążyć zrobić oba.

(-5 Wpływu: Żółw: budzę Strażnika)

Minerwa zostaje. Pięknotka poszła na Trzęsawisko sama. Jak zawsze. Im mniej wchodzi, tym mniejsza szansa że się Trzęsawisko wzburzy. Niestety, tym razem przebudził się Pnączoszpon i zdecydował się zapolować na Pięknotkę...

(21:20, powrót do gry). ESKALACJA.

_Pnączoszpon_, Trwałość: 3. Szybki, najsilniejszy na bagnach, Równy Power Suitowi. Cel: pozbawić Pięknotkę iteracji (technicznie, odpalić Silnik tylko wrogowi i to dwa razy).

Pięknotka zauważyła zmianę atmosfery. Poczuła zagrożenie. Nie samo Trzęsawisko, ale coś na nim skupiło uwagę na niej. Wpierw Pięknotka próbuje odwrócić uwagę Pnączoszpona - zrzucić go z siebie - by zastawić nań pułapkę. Zastawia aromatyczną pułapkę. (Typowy:12,3,2=). Sukces - Pnączoszpon się ujawnił i zaatakował pułapkę. Ciało potwora jest ukryte gdzieś pomiędzy drzewami, ale pnączami zaatakował.

Wykorzystując już zdobyte materiały i znajomość terenu, Pięknotka wycofała się na polankę na bagnach. Bardzo niekorzystne dla Pnączoszpona miejsce. Chce go tam zwabić by otworzyć doń ogień używając... snajperki. Test (Trudny+1:11,3,5=S). Pięknotka dała radę się wycofać, znaleźć dobry teren i wyciągnąć snajperkę. Poczekała chwilę, aż Pnączoszpon wyszedł w jej pole widzenia. Test (Trudny+3:13,3,4=SS). Pnączoszpon został ciężko zraniony; udało mu się jednak uciec. Nie będzie już Pięknotki niepokoił. Pięknotka odetchnęła z ulgą - nie udało jej się co prawda dostać wszystkiego co chciała, ale przynajmniej nie ma kłopotu.

KONIEC ESKALACJI.

* Tor Pięknotki_adela: 6+2+4=12
* Tor Pięknotki_atena: 9
* Tor Adeli: 8+1=9
* Tor Ateny: 2+1=3
* Ż: 8 (13)
* K: 3

**Okrążenie (Wpływ: 12-16):**: (21:42)

Pięknotka jest na relatywnie spokojnym Trzęsawisku. Nadal jest niewłaściwa energia i atmosfera, ale nie jest skupiona na Pięknotce. Coś jest z Trzęsawiskiem nie tak. Pięknotka odpaliła detektor - chce znaleźć gdzie są te problemy. (Typowy:12,3,2=S). Ma to - problem pochodzi gdzieś z Nieużytków Staszka; Pięknotka ma skalibrowany detektor. Teraz może to miejsce znaleźć jak tylko będzie w pobliżu. (+2 p_atena)

(Żółw: -5 pkt Wpływu. Podniesienie toru Adeli. = +1 Adela. FAIL.)

Czas na zdobycie odpowiednio egzotycznych składników. (-1 Wpływu Kić: Pnączoszpon zostawił coś po sobie co można użyć do epickiej mikstury). (Typowy:12,3,2=S). Odpowiednio zacne, egzotyczne składniki i jeszcze wspomagane Pnączoszponem (+3 p_adela).

(Żółw: -5 pkt Wpływu. Podniesienie toru Adeli. = +5 Adela.) (Kić: -1 pkt Wpływu. +1 tor Adeli. Koniec, Pięknotka wygrała.)

Pięknotka zdecydowała się zbadać Trzęsawisko jako osoba, która tu mieszka i to zna. Czemu tu jest coś nie tak. Co się zmieniło. Poświęciła chwilę by do tego dojść. (Typowy:9,3,3=P). +1 atena + przeciwnik.

Problem pochodzi z tego, że to działanie które robi te dziwne grzybki, to działanie jest próbą czerpania energii z Trzęsawiska. Trzęsawisko się broni - spawnuje potwory oraz działa inaczej. Pięknotka przez to wlazła na bagno, którego NIE BYŁO. Ruchome piaski i żarłoczne rośliny. (na prawie ESKALACJI; Trwałość 2; cel: zatruć Pięknotkę)

Bagno próbuje Pięknotkę unieruchomić i otworzyć roślinom do zatrucia. Pięknotka natychmiast odpaliła swojego power suita i zostawiła ślad zapachowy; chce uciec jak najszybciej. Może uda jej się uciec. Heroiczny w Trudny (bo zapach i Power Suit). Złamała ESKALACJĘ - ucieczka. (Trudny+3:13,3,4=S). Wyrwała się z Trzęsawiska, ścigana przez różne polujące na nią istoty. Tym razem nie dało się łagodnie.

* Tor Pięknotki_adela: 16 (FROZEN)
* Tor Pięknotki_atena: 11+2=13
* Tor Adeli: 9+1+5=15 (FROZEN)
* Tor Ateny: 4+1=5
* Ż: 2 (17)
* K: 3 (5)

**Okrążenie (Wpływ: 16-20):**: (22:06)

Pięknotka poprosiła Minerwę o pojawienie się niedaleko Nieużytków. Spotkały się tam razem. Minerwa dostała detektor i zaczęła poszukiwania - bez problemu zlokalizowała przeciwnika. Na Nieużytkach Staszka znalazła taką niewielką ziemiankę. Stamtąd dochodzi sygnatura problematyczna.

(Żółw: -2 Wpływu; aktywacja psychiki Terrorforma)

Minerwa (pod wpływem Terrorforma) zaproponowała Pięknotce, że rozerwie ziemiankę na strzępy. W środku znajdują się dwie żywe istoty, nie stanie im się krzywda. Pięknotka otoczyła ziemiankę pyłem wykrywającym magów oraz istoty żywe. A Minerwa (Terrorform) już się oblizuje z myślą o destrukcji. Otworzyła potężnym strzałem sufit ziemianki, odkrywając przerażonych Brygidę i Ignacego; Terrorform przygotowuje się do skoku.

(Kić: -2 Wpływu; Trzęsawisko się połapało - Pnączoszpon tu dopełzł polując na Pięknotkę)

Terrorform zauważył zbliżającego się Pnączoszpona. Nie zmieniając nastawienia, zaatakował Pnączoszpona, ignorując bezbronnych magów; w ten sposób Minerwa wróci do akcji po zniszczeniu Pnączoszpona. A Pięknotka zwróciła się do przerażonych osób w ziemiance... by poznać prawdę. Dowiedziała się bez problemu. (+2 p_atena)

* chcieli pomóc Brygidzie (viciniusce) by była silniejsza i potężniejsza
* nie mieli pojęcia co robią i co się dzieje; nie wiedzą skąd jest energia
* są przerażeni; nie chcieli nic złego i nie planują nic złego
* cybergrzyby są epickie.
* nie wiedzieli jednak, że Cyberszkoła jest podpięta energetycznie do Trzęsawiska

Pięknotka próbuje dojść do tego co tu się dzieje. O co tu chodziło. W końcu coś słyszała od czasu do czasu (Typowy+2,6,3,4=SS). Pięknotka zlokalizowała problem - ktoś im podłożył te grzyby, ktoś ich poprowadził. To nie była akcja wymierzona w nich. Tu chodziło o Atenę. Brygida jest powiązana z Ateną; gdyby ona doprowadziła do wzbudzenia tego wszystkiego to Atena by szybko na Epirjon nie wróciła. A jak na razie, jest szansa, że nawet nikt się nie dowie o tym, że Atena tutaj była wcześniej. Nikt jej z tym wszystkim nie kojarzy.

Czyli to akcja anty-Atenowa. Ktoś grał na porażkę Ateny (że Brygida spieprzyła). Pięknotka się zirytowała.

(Kić: -1 Wpływu; zwycięstwo toru ZANIM doszło do odpalenia silnika)

* Tor Pięknotki_adela: 16 (FROZEN)
* Tor Pięknotki_atena: 16 (FROZEN)
* Tor Adeli: 15 (FROZEN)
* Tor Ateny: 5 (FROZEN)
* Ż: 4 (21)
* K: 1 (6)

**Epilog**: (22:32)

* Brygida dalej szuka sposobu jak być silniejszą, ale niekoniecznie używając nieznanej sobie magii
* Ignacy nadal pasjonuje się wszelkimi grzybami, ale będzie ostrożniejszy wobec ludzi i magów
* Minerwa wróciła do normy; nie zauważyła, jaki wpływ miał na nią terrorform
* Adela była blisko Pięknotki. Naprawdę blisko. Ale królowa jest tylko jedna ;-).
* Atena nie ucierpiała. No, poza tym, że była na występie jako modelka...

* Ż: 4
* K: 6

### Wpływ na świat

| Kto           | Wolnych | Sumarycznie |
|---------------|---------|-------------|
| Żółw          |   0     |     21      |
| Kić           |   0     |     11      |

Czyli:

* (K): Pięknotka uświadomi Minerwie, że terrorform ma na nią wpływ (1). (Ż): Minerwa uzna, że jest w stanie go kontrolować (1), (K): Minerwa usprawni psychotronikę by móc odciąć terrorforma, nawet kosztem awaryjnego wyłączenia servara
* (Ż): Brygida dalej dąży do zwiększenia siły i autonomii w tym świecie; jako vicinius chce być coraz silniejsza, silniejsza od Ateny (1), (K): Bo chce się czuć bezpieczniej; there is no malice
* (Ż): Atena jeszcze nie jest w stanie wrócić na Epirjon, fizycznie. Jest za słaba. Co najmniej 2 tygodnie (2), (K): Ale ma większy dostęp do danych z Epirjona; jest w prawie (1)
* (K): Zacznie powstawać relacja między Brygidą a Ateną, coś w stylu adiutantka - komendant (2).

## Streszczenie

Jednoczesny atak na reputację Ateny (przez cybergrzyby) oraz wielki konkurs kosmetyczny w Pustogorze wpłynęły stosunkowo źle na sytuację Pięknotki. Nie rozdwoiła się, ale obie rzeczy udało jej się rozwiązać - acz jest wycieńczona. Odkryła konspirację wymierzoną przeciwko Atenie - ale nie wie kto i gdzie próbuje Atenę usunąć.

## Progresja

* Pięknotka Diakon: pierwsze miejsce w konkursie kosmetyczek w Pustogorze. Najlepsza i najbardziej ceniona. Ciężko na to zapracowała.
* Atena Sowińska: za słaba; nie jest w stanie wrócić na Epirjon jeszcze przez co najmniej dwa tygodnie.
* Atena Sowińska: zrobiła furorę jako modelka Pięknotki na Arenie Szalonego Króla w Pustogorze. Wbrew sobie. Wyszła jej przeszłość miss licealistek.
* Brygida Maczkowik: zaczyna budować relację z Ateną jako adiutantka. Jeszcze walczy, ale Atena cierpliwie przekonuje Brygidę do siebie.
* Adela Kirys: drugie miejsce kosmetyczek w Pustogorze. Ma klientów i nie grozi jej już upadek czy katastrofa. Przekonana, że Pięknotka wygrała przez użycie Ateny.
* Minerwa Metalia: by powstrzymać terrorforma, złożyła psychotronikę by móc się odciąć i wyłączyć servar w sytuacji awaryjnej.

### Frakcji

* 

## Zasługi

* Pięknotka Diakon: trudny wybór - terminuska czy kosmetyczka? Pomóc Atenie czy wygrać z Adelą? Uniosła się kompetencjami i wygrała oba, pokazując poziom mistrzowski. Wielki sukces.
* Minerwa Metalia: testuje swoje nowe możliwości; powiedziała Pięknotce o zbliżającym się problemie z cybergrzybami inkarnaty (?); w pewnym momencie straciła kontrolę do terrorforma i tego nie zauważyła.
* Arnulf Poważny: skutecznie osłonił reputację Ateny; nie miał pojęcia, że jego uczeń uczestniczył w akcji wymierzonej w Atenę.
* Adela Kirys: dała z siebie wszystko by wygrać konkurs na najlepszą kosmetyczkę - i o włos! Ale Pięknotka była jednak lepsza. Za to, Adela jest druga.
* Brygida Maczkowik: dążąc do coraz większej siły, wplątała się w polityczny atak na jej opiekunkę - Atenę. Współpracowała z Ignacym Myrczkiem.
* Ignacy Myrczek: grzybomag, który wplątał się w polityczny atak na Atenę z chęci pomocy Brygidzie oraz z miłości do grzybów. Już więcej nie będzie tak eksperymentował.

## Plany

* Pięknotka Diakon: dowiedzieć się, kto do cholery próbuje się pozbyć Ateny Sowińskiej
* Brygida Maczkowik: musi stać się groźniejsza, silniejsza i lepiej przystosowana jako vicinius w świecie magów; ale nie ma w niej zła, tylko pragnienie bezpieczeństwa
* Wiktor Satarail: zainteresuje się Brygidą Maczkowik - może uda się ją dostosować do rodu Satarail?

### Frakcji

* 

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Pustogor
                                1. Arena Szalonego Króla: efektowne, wielkie miejsce; tam doszło do konkursu kosmetycznego
                                1. Laboratorium Senetis: jedno z najlepszych (niezbyt tanie); przebadali Pięknotce cybergrzyby i zrobili detektor.
                            1. Zaczęstwo:
                                1. Akademia Magii, kampus
                                    1. Budynek Centralny: dyrektor bardzo dba o Atenę. Miejsce, gdzie da się znaleźć chętnych do pomocy magów nie zadających pytań, niestety.
                                1. Cyberszkoła: zainfekowana cybergrzybami, czerpie energię z Trzęsawiska. Niefortunne.
                                1. Nieużytki Staszka: miejsce, gdzie Brygida i Ignacy próbowali zrozumieć potęgę cybergrzybów i wzmocnić Brygidę.
                        1. Trzęsawisko Zjawosztup: rozpalone jak rzadko, broni się przed kradzieżą energii przez cybergrzyby

## Czas

* Opóźnienie: 3
* Dni: 4

## Narzędzia MG

### Budowa sesji

**SCENA:**: Nie aplikuje

### Omówienie celu

Ta sesja jest prototypem dwustronnego wyścigu dla Pięknotki.

* Jedną nitką jest to, czy uda jej się zachować tytuł mistrzyni kosmetyków
* Drugą nitką jest to, czy uda jej się zapobiec problemom w okolicach Zaczęstwa
* Czyli: terminuska czy kosmetyczka?

## Wykorzystana mechanika

1810

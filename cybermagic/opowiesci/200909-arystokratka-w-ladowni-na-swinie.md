---
layout: cybermagic-konspekt
title: "Arystokratka w ładowni na świnie"
threads: legenda-arianny
gm: żółw
players: kić, kapsel, fox
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [200826 - Nienawiść do świń](200826-nienawisc-do-swin)

### Chronologiczna

* [200826 - Nienawiść do świń](200826-nienawisc-do-swin)

## Budowa sesji

### Stan aktualny

.

### Dark Future

* ?

### Pytania

1. .

### Dominujące uczucia

* .

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Punkt zero

.

## Misja właściwa

WCZEŚNIEJ, przed sesją właściwą.

Arianna i Eustachy nie chcą zostawić samego Martyna w brygu. To jest potencjalnie dużo bardziej niebezpieczne niż wzięcie go ze sobą. Więc trzeba jakoś Martyna wydostać.

PROJEKT INIT. Cel: wydostać Martyna.

FAZA 1, składanie:

Pierwsze, co zrobił Eustachy - przeleciał po pokoju Martyna i znalazł jego kajecik z kontaktami. Do każdej damy wysłał wiadomość w jakim stanie jest biedny Martyn (omijając całkowicie fakt, że to przez niego, oczywiście): +(VV)

Następnie, Eustachy skorzystał ze swojej reputacji kobieciarza. Zorganizował z pomocą Klaudii konkurs na zasięgi. Która dama bardziej pomoże w zbudowaniu zasięgów i uwolnieniu Martyna z kłopotów, może pójść z Eustachym na kolację. Wyszło niesamowicie bucowato, ale, w sumie - jeśli Eustachy tak się zachowuje to chyba ma coś na podparcie tego. Te szlachcianki się zainteresowały tematem: +(VV). Acz ma to też konsekwencje: X: renoma Eustachego wymaga sprawdzenia ;-).

Arianna też nie zostawiła tego sama. Ona skontaktowała się z Izabelą Zarantel odpowiedzialną za "Sekrety Orbitera". Dała jej fotki - nic kompromitującego, rzeczy, by Iza mogła zrobić bardzo skuteczny odcinek. Arianna zrobiła maksymalny urok, czarowanie itp. Izabela nie jest Ariannie wroga, nigdy nie była. Nie widzi problemu, by pomóc. Nie tylko +(VVV), dodatkowo Iza na prośbę Arianny wplotła, że Arianna walczy o równość między frakcjami Orbitera.

Ale te ruchy między Arianną i Izabelą sprawiły, że oczy Orbitera skupiły się na Ariannie i Inferni. Pojawili się rywale. Pojawiła się opinia, że Arianna ma świetny PR ale jest bezużyteczna i bezwartościowa. 

Na to wszystko Klaudia rzuciła super-duże plotki: Martyn bronił dziewczyny i za to siedzi +(V). Do tego odgrzebała jakieś stare prawa precedensowe i zaczęła zakłócać tym działania sądu i w ogóle (+VVVV). Ogólnie, katastrofalna sprawa; dla Orbitera Martyn stał się super niewygodny a Ariannie sława eksplodowała. Dlatego Izabela leci z załogą Inferni, dokumentować ich wyczyny - widzi, że to żyła złota ;-).

FAZA 2, manifestacja:

* XV: Dezorganizacja więzienia. Fanki. Problemy. Ale też sława. Lepiej niech Martyn i Eustachy zostaną dłużej na planecie... ale idzie dobrze.
* VV: Sędziowie i admiralicja NIE CHCĄ się pieprzyć z tym wszystkim. Mają dość swoich problemów. A tu problemy z morale i ryzyko nowego buntu...
* XV: ...dzięki działaniom "Sekretów Orbitera" na pokład Galaktycznego Tucznika zakradła się pasażerka na gapę, chcąca przeżyć przygody z bohaterami.
* XX: ...ale pasażerka na gapę jest niepełnoletnią szlachcianką z ważnego odłamu Sowińskich z Aurum; Anastazja Sowińska. Na pewno jest porwana.
* V: Martyna wypuścili z więzienia.

PROJEKT END

TERAZ.

Zespół w ograniczonym składzie: Arianna, Eustachy, Klaudia, Elena, Martyn i Kamil lecą Galaktycznym Tucznikiem na planetę. Tucznik jest superciężkim pojazdem o niesamowitych ekranach ochronnych i antymagicznych. Ma ekranowania i mechanizmy potrzebne do radzenia sobie z glukszwajnami. Jest zwrotny jak krowa. Ma 4 lekkie działka do radzenia sobie na planecie; nie jest to statek bojowy. I jest trudny do uszkodzenia lub zepsucia.

Ok, ale nikt się nie spodziewał projektując Tucznika, że będzie do niego strzelał kopiec nojrepów z planety, niedaleko Trzeciego Raju. Z jakiegoś powodu kopiec nojrepów się zbliżył do Trzeciego Raju i z jakiegoś powodu ostrzeliwuje teraz Tucznika (to akurat nie jest aż tak dziwne, zbliżają się w końcu szybko z orbity).

Eustachy - do działek. Arianna - do pilotażu.

Arianna robi co może, by odpowiednio brać pociski nojrepów na pancerz Tucznika; niestety, sam Tucznik nie jest idealnym do tego pojazdem. Obraca Tucznika ładownią w stronę pocisków (tam najsilniejszy pancerz i pole siłowe bo glukszwajny). Ale tam też schowała się Anastazja...

* OOOO: przerażenie Anastazji powoduje epicki Paradoks
* XX: Tucznik wymaga naprawy zanim wystartuje, niższa sterowność.
* X: Tucznik nie wystartuje bez poważnych napraw. (POTEM ANULOWANE PRZEZ EUSTACHEGO)
* V: Wyląduje jak chciała

Widząc nienaturalną anomalię w ładowni, Arianna wysłała Klaudię z Kamilem by się tym zajęli. Dlaczego Kamil a nie Martyn?

* "Kamil się nie zgadza z Klaudią - to jest jego atut": Fox, 2020

Eustachy tymczasem zdejmuje rakiety nojrepów przekształcając magicznie amunicję we flaka, by pomóc Ariannie jak się da.

* OOO: Przerażenie Anastazji sięga zenitu. Anastazja zginie w ładowni bez pomocy.
* X: Jedno działko Tucznika zostanie zniszczone podczas firefighta.
* Xm: Anomalia w ładowni jest uzbrojona i niebezpieczna, inkarnuje się koszmar Anastazji. Powoli się robi konflikt - ratujemy Anastazję lub tracimy Tucznika.
* VVm: magiczny sukces - Eustachy pomógł Ariannie; nie stracą sterowności i kontroli nad Tucznikiem.

Klaudia i Kamil zbliżyli się do ładowni. Klaudia poczuła potężną energię ze środka. Przemnożyła sobie to co wie o Tuczniku, sprawdziła co jest grane teoretycznie - w środku jest Skażenie klasy arcymaga, stabilne, efemeryczne. I jest tam jeden ślad życia. To jest "niewielki problem". Ona NIE MA ZAMIARU tam po prostu wchodzić.

Klaudia wie że mają pasażera na gapę. Wie też, że z Kamilem nie są w stanie tego zrobić. Natychmiast wróciła na mostek Tucznika (po drodze pozbyła się Kamila i dała go Martynowi; moc anomalii zaczęła na niego negatywnie wpływać).

Arianna nie wie, kto jest w ładowni. Wie, że to pasażer na gapę. Ale zamierza go ratować.

Klaudia szybko przedstawiła plan, konferując z Arianną i Eustachym:

1. Ona przygotowuje odpowiednią anomalię wchłaniającą energię magiczną (a potem wybuchającą)
2. Eustachy dodaje do anomalii silniczek
3. Martyn idzie w kosmos, podpełza do odpowiedniego miejsca przy ładowni Tucznika
4. Arianna dehermetyzuje ładownię, by wypromieniować energię (z powietrzem); anomalia ma ściągać magię by jak najmniej zostało tego na statku
5. Martyn wpada do środka ratować pasażerkę na gapę i do medbay.
6. Arianna ląduje Tucznikiem koło pastwiska glukszwajnów by wyżarły resztkę

Dobra, to jest plan. A teraz czas to zrealizować. I oczywiście, nie może być zbyt prosto.

PROJEKT INIT. Cel: wydostać pasażerkę na gapę.

Pula startowa: 5*(X) //powinienem był dać 8*(X).

Kontekst: są ostrzeliwywani przez cholerne nojrepy. Uniki, manewry, odłamki itp. To nie jest najlepszy pomysł, by Martyn wychodził na zewnątrz, mimo całej jego ekspertyzy na temat działań kosmicznych i w próżni. Martyn, oczywiście, z uśmiechem się zgodzi... ale szkoda lekarza XD.

1. Arianna remoduluje pola siłowe, by chronić Martyna. (+VV). Niestety, to zmusiło ją do tego, by momentami osłaniać jego a nie bardziej wrażliwe fragmenty statku; nawet mimo pancerności Tucznika, nojrepy robią swoje. X: tarcze Tucznika wymagają naprawy. X: Izabela została ranna; wymaga medbaya, ale jest funkcjonalna. Acz jest krew; pocięło jej prawą rękę. Są też inni ranni w załodze; nic, czego Martyn nie rozwiąże.
2. Jednocześnie Klaudia i Eustachy pracują nad anomalią. "Gąbka" działa; niestety, weszła w 'runaway'. Klaudia z trudem ją ustabilizowała. Eustachy otoczył ją pancerzem i "gąbka" nadaje się jako pełnoprawny pocisk wciągający w siebie magię. Powinno kupić to czas i możliwość strzelenia w nojrepy. (+VVVV)
3. Martyn w swoim kombinezonie wychodzi w kosmos. Niestety, mimo pól siłowych, to nie jest prosta operacja; odłamki i pociski przebiły jego kombinezon i go poraniły. Martyn włączył stymulanty, zacisnął zęby i pełzł dalej w kierunku ładowni po powierzchni (+VV). Niestety, nikt nie mógł mu pomóc.
4. Arianna zaczęła ostrożnie dehermetyzować ładownię, by opróżnić anomaliczne powietrze, Martyn podłożył "gąbkę" pod dehermetyzację. Znajdująca się w środku statku Anomalia Terroru zaczęła wypluwać wszelkie zło w stronę gąbki; Arianna i Martyn próbowali się utrzymać i pozbyć problemu. (+VV)
5. Klaudii udało się utrzymać gąbkę i dobrze ją przygotować. Ale gąbka nie wytrzymała napięcia... Martyn musiał ją odrzucić. By eksplozja go nie ogarnęła... (+VV)
6. ...Arianna zrobiła PIGFLIP - natychmiast obróciła Tucznika brzuchem w górę. Sukces! Ale Paradoks Arianny splątał ją z Anastazją; przez miesiąc są na siebie skazane i nie mogą się oddalić od siebie na odległość większą niż 1 km. Ale Martyn przetrwał (+null; to był konflikt naprawiający porażkę (5)).
7. Martyn wpada do środka ładowni, gdy Skażenie jest niższe. Ranny, krwawi, ale walczy przeciw tym anomaliom jakie zostają, łapie półprzytomną Anastazję i wbiega przez śluzę na statek. STAJE SIĘ MIŁOŚCIĄ ANASTAZJI LIKE FOREVER!!! <3<3<3.

Pula: 14*(V), 5*(X) = 5*(V), 2*(X). Wynik: Anastazja boi się zostać sama, bo Paradoksalny Koszmar wraca.

PROJEKT END.

I Ariannie udało się wylądować przy pastwisku glukszwajnów, by odkaziły statek. Martyn przejął kontrolę nad skrzydłem medycznym, choć zapłaci za to potem...

## Streszczenie

Zespół wyciągnął Martyna z więzienia (legalnie). Wpakowali się na Galaktycznego Tucznika by przenieść świnie na orbitę... ale poleciała z nimi dziennikarka Izabela (o czym wiedzą) i 15letnia Anastazja Sowińska (o czym nie wiedzą). Przy lądowaniu, atak nojrepów na Tucznika spowodował katastrofę - seria Paradoksów Anastazji, uszkodzenia Tucznika itp. Zespół uratował Anastazję (choć kosztem ran Martyna) i wylądował Tucznikiem na Astorii. To jest sukces.

## Progresja

* Eustachy Korkoran: jego renoma kobieciarza i miłośnika arystokratek wymaga sprawdzenia ;-).
* Eustachy Korkoran: rywale - nie jest taki dobry. Co więcej, podrywa dziewczyny i jest wobec nich bucem. Negatywne konsekwencje sławy reality show ;-).
* Eustachy Korkoran: niechętnie widziany na Orbiterze chwilowo, acz pożądany przez fanki.
* Arianna Verlen: dzięki Izabeli Zarantel i "Sekretom Orbitera", pojawiła się opinia, że Arianna walczy o równość między Noctis, Eternią, Astorią itp.
* Arianna Verlen: rywale - nie jest dobra, jest psem na szkło. Rywale. Negatywne konsekwencje sławy reality show ;-).
* Arianna Verlen: TYLKO INFERNIA. Nie dostanie dowodzenia innego statku bo go rozwali XD (opinia). Nawet Galaktycznego Tucznika rozwaliła.
* Arianna Verlen: Paradoksalnie splątana z Anastazją Sowińską; nie może się od niej oddalić > 1 km. Zdaniem wielu, Arianna splątała się specjalnie z Anastazją.
* Anastazja Sowińska: Paradoksalnie splątana z Arianną Verlen; nie może się od niej oddalić > 1 km. Zdaniem wielu, Arianna splątała się specjalnie z Anastazją.
* Anastazja Sowińska: śmiertelnie i na całe życie i w ogóle zakochana w Martynie Hiwasserze, jej RYCERZU.
* Anastazja Sowińska: boi się zostać sama, bo Paradoksalny Koszmar wróci...
* Martyn Hiwasser: niechętnie widziany na Orbiterze chwilowo
* Martyn Hiwasser: został Wielką Miłością Anastazji Sowińskiej. Piętnastolatki.
* Martyn Hiwasser: przez następny tydzień w bardzo złym stanie i złej formie.
* OO Infernia: Izabela Zarantel dołącza do załogi.
* OO Galaktyczny Tucznik: potrzaskany i wymaga naprawy. Tarcze, właz do ładowni, jedno lekkie działko (z czterech) zniszczone, uszkodzone ekrany antymagiczne w ładowni, kadłub powgniatany. Ale solidny i dalej lata.

### Frakcji

* .

## Zasługi

* Arianna Verlen: pilot Tucznika; przy akcji ratunkowej udało jej się utrzymać Martyna przy życiu, acz Tucznik poobijany. Dużo czarowała mocą arcymaga; niestety, sprzęgła się z Anastazją. Zdecydowanie nie jej dzień. Ale - pozyskała Izabelę jako wsparcie Inferni (Tucznika).
* Klaudia Stryk: mastermind planu ratunkowego Anastazji. Ostrożniejsza niż się wydaje - nie weszła w ciemno w arcymagiczną anomalię. Użyła anomalicznej gąbki do wyczyszczenia Skażenia z ładowni. Dodatkowo, biurokratycznie uciemiężyła sąd Kontrolera Pierwszego by wydobyć Martyna z kicia.
* Eustachy Korkoran: zrobił epicki konkurs która arystokratka może z nim iść na kawę by wyciągnąć Martyna, po czym... zwiał na Tucznika. Zestrzelił większość rakiet nojrepów używając flaka z działek Tucznika (które normalnie nie miały flaka, ale od czego jest magiem?). Paradoksem uzbroił anomalię terroru.
* Elena Verlen: alias Mirokin; w lekkiej depresji, że będzie świnie wozić; służyła jako zastępstwo Eustachego. Niezbyt aktywna.
* Martyn Hiwasser: bohater; zrobił spacer kosmiczny pod ostrzałem nojrepów by ze Skażonej ładowni uratować nastolatkę Aurum, mimo strasznych ran od owych nojrepów.
* Kamil Lyraczek: miał pomagać Klaudii; niestety, skończył nieprzytomny przez anomalię terroru Anastazji (w czym pomogły środki Martyna).
* Izabela Zarantel: kontynuuje "Sekrety Orbitera", wyczuła złoto. Dołączyła do załogi Inferni (teraz: Tucznika). Została ranna przy wybuchach na Tuczniku przez nojrepy.
* Anastazja Sowińska: 15-letnia wysoka szlachcianka Aurum o przypadłości nienaturalnie silnych Paradoksów; schowała się w ładowni Tucznika i jej Paradoksy stworzyły tam krainę terroru. Uratowana przez załogę Tucznika (Inferni) z niekończącego się koszmaru.
* OO Galaktyczny Tucznik: superpancerny, mało zwrotny transportowiec glukszwajnów, bardzo trudny do uszkodzenia. Został uszkodzony przy pilotażu Arianny pod ostrzałem nojrepów i przez superParadoks Anastazji.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski, NW
                    1. Ruiniec
                        1. Trzeci Raj, okolice
                            1. Kopiec Nojrepów: aktywny kopiec nojrepów, który chwilowo skupia swoją uwagę na atakowaniu Trzeciego Raju i bombardowaniu go artylerią.
            1. Astoria, Orbita
                1. Kontroler Pierwszy

## Czas

* Opóźnienie: 1
* Dni: 5

## Inne

Komentarz MG:

* Kolejna sesja, która sama wyszła z mechaniki i której nie przewidziałem.
* Powstała mi Anastazja i zupełnie zmieniła się możliwość powiązań:
  * nagle na pokładzie jest Izabela (na stałe?)
  * nagle na miesiąc jest Anastazja, co zmienia możliwości Arianny w Trzecim Raju

Planowana sesja:

* Zespół ląduje Galaktycznym Tucznikiem przy Trzecim Raju
* Zespół ma do czynienia z małym atakiem nojrepów. Widzi w jak złym stanie jest Raj.
* Zespół troszkę im tam pomaga, wykrywa ??????? ?? ???????? ???????.
* Ok, niezależnie od okoliczności - świnie na statek. Lecimy na orbitę.
...

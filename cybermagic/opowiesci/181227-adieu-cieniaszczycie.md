---
layout: cybermagic-konspekt
title:  "Adieu, Cieniaszczycie"
threads: dzien-z-zycia-terminusa
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [181226 - Finis Vitae](181226-finis-vitae)

### Chronologiczna

* [181226 - Finis Vitae](181226-finis-vitae)

## Projektowanie sesji

### Struktura sesji: Eksploracja

![Budowa relacji Lilia, Moktar, Erwin, Minerwa, Saitaer, Amadeusz...](mats/1812/181227-loose-ends.png)

1. Atena ściąga asystentkę i instaluje ją w Cieniaszczycie.
2. Erwin i Pięknotka, trzeba rozwiązać ten problem.
3. Nowe ciało dla Minerwy
4. Lilia, Moktar i ogólna bezużyteczność
5. Amadeusz źródło wiedzy (nojrepy, sanktuarium Arazille)

### Hasła i uruchomienie

* Ż: (kontekst otoczenia) -
* Ż: (element niepasujący) -
* Ż: (przeszłość, kontekst) -

### Dark Future

1. ...

## Potencjalne pytania historyczne

* brak

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Misja właściwa

**Prolog**: (14:04)

Pięknotka się budzi. Na szczęście, ma SWOJE ciało i jest w dobrej formie. Niczego tak nie chciała zobaczyć jak tego. Czas zamknąć kilka luźnych wątków.

**Scena**: (14:05)

_Colubrinus Psiarnia_

Wejście Pięknotki zostało przywitane uśmiechami i lekkim machnięciem ręki jednego z Psów. Nikt nawet nie zerwał się do walki. Waleria siedzi, czyści snajperkę (działo snajperskie) i widząc Pięknotkę, z trudem powstrzymuje uśmiech. Coś się musiało stać. Pięknotka podeszła i po wymianie uprzejmości spytała co się stało. Waleria odpowiedziała, że porwała Erwina. CO?! Waleria podeszła i powiedziała, że o Pięknotce są informacje w Czaszce Kralotha. I Erwin tam poszedł. Więc Waleria go porwała by nie zrobił sobie większej krzywdy...

Pięknotka lekko facepalmowała. Waleria dodała, że Atena pytała o Erwina; Waleria odparła, że Pięknotka o wszystkim wie. I w ten sposób zakończył się turyzm Erwina Galiliena w Cieniaszczycie.

Pięknotka spytała, czy będzie dużym problemem gdyby Pies mógł popilnować Lilię i ew. Erwina. Waleria powiedziała, że zależy od tego jak to będzie dalej się toczyło. Na razie wszystko wskazuje na to, że nie będzie większego problemu.

Więc jednak trzeba porozmawiać najpierw z Erwinem...

_Colubrinus Psiarnia, Containing Chamber 2B_

Pomieszczenie wygląda jak do przetrzymywania ufoków. Ale ma kanapę i telewizor. Siedzi tam nieszczęsny Erwin - bardzo ożywił się widząc Pięknotkę. Erwin opowiedział Pięknotce swoje przypadłości - został porwany, nie zdążył nic zrobić ani zadziałać. Pięknotka wyjaśniła Erwinowi zasady Cieniaszczytu. Erwin poskarżył się na straszliwą poezję i kiepski, kiczowaty styl strażnika. Pięknotka się skrzywiła. Zbigniew chciał dobrze... ale w końcu to Łysy Pies.

Erwin powiedział, że dobrze, że Pięknotka przyszła. Chciał już wysadzić drzwi i uciec. Pięknotka ZNOWU facepalmowała. Zbigniew chciał, by Erwin miał coś do roboty, więc zostawił mu taką możliwość. Zapytany czemu nie porozmawiał z Ateną, powiedział, że nie miał okazji - była opieprzana za stratę cennego sprzętu wojskowego. Pięknotce lekko zrzedła mina. Spytała ostrożnie co Erwin pamięta sprzed transformacji. Powiedział, że niewiele, acz się zaczerwienił. Gdy zaczął przepraszać i dziękować Pięknotce za poświęcenie, ta go pocałowała i powiedziała, że chciała zrobić to od zawsze. Zemdlał z wrażenia.

Łyse Psy mają radochę.

Erwin po ocknięciu ma głupią, ale radosną minę. Pięknotka go wyprowadziła i zaprowadziła do swojego mieszkania w Mrowisku (przy aplauzie Psów). Poprosiła, by grzecznie poczekał i nie rozrabiał. Erwin powiedział, że muszą znaleźć to ciało dla Minerwy. Stwierdził, że nie jest najlepszym pomysłem ufanie Saitaerowi - nie wie, czemu wydawało mu się to dobrym pomysłem wcześniej. Pięknotka zauważyła "bo Saitaer". Fakt. Erwin poczeka na Pięknotkę.

**Scena**: (14:31)

_Colubrinus Psiarnia, Containing Chamber 5D_

Waleria. Powiedziała Pięknotce gdzie znaleźć Lilię. Cela 5D. Pomieszczenie (dla odmiany) wygląda jak do przetrzymywania ufoków. Lilia jest w środku. Sama. Zaskoczona obecnością Pięknotki - czy ją też złapali? Nie. Pięknotka przyszła ją wyciągnąć. Lilia westchnęła. Znowu. Cóż, nie będzie stanowiła problemów.

Wychodzą. Znowu Psy klaszczą. Lilia cała czerwona, patrzy na stopy. Waleria ani słowem nic nie mówi. Pięknotka chce wyprowadzić Lilię, a Zbigniew wtedy zaczyna. Czy Lilia chciałaby być silna i potężna? Może do nich dołączyć, nauczą ją tego czy owego. Waleria aż rzuciła zaskoczone spojrzenie na Zbigniewa. Lilia wybuchła, że nie ma zamiaru nigdy więcej ich widzieć i że dołączenie do nich jest najgorszym pomysłem jaki mogłaby mieć. Zbigniew się szeroko uśmiechnął. Niech i tak będzie.

Pięknotka wzięła Lilię do Nukleonu.

_Kompleks Nukleon_

Lilia bardzo przeprosiła Pięknotkę za kłopot. Nie wie jak ją porwali, nie wie jak doszło do tego że przełamali jej zabezpieczenia ale dziękuje. Nie będzie więcej sprawiać problemów. Zapytana przez Pięknotkę czy zamierza uderzać w Orbiter Pierwszy odpowiedziała, że nie. Już nie. Miała wendettę przeciwko Szarjanowi, ale to nie ma sensu - skoro "zwykłe thugi" (jak Łyse Psy) są tak niebezpieczne, to tym bardziej psychopatyczny Szarjan. Lepiej nic nie robić. Zapytana co zamierza robić teraz, powiedziała, że przycupnie. Jak "przycupnie". Lilia odpowiedziała, że została PORWANA Z DOMU. Musi się schować i przycupnąć.

Pięknotka skomunikowała się z Ateną. Może Atena przygarnie Lilię? Atena powiedziała, że ABSOLUTNIE się nie zgadza. Nie chce Lilii, jest to osoba z jej punktu widzenia bezużyteczna. Pięknotka powiedziała "prooooszę" i zabrała się za solidne hipernetowe przekonywanie. (Tr:7,3,6=S). Atena ciężko westchnęła. Jest skłonna dać Lilii szansę. A tak się cieszyła, że pozbyła się JEDNEJ bezużytecznej asystentki i teraz ma drugą...

Atena Sowińska. Wakat na stanowisku: "bezużyteczna asystentka".

Pięknotka powiedziała Lilii, że może niech przycupnie z Ateną na stanowisku asystentki. Lilia się żachnęła, ale Pięknotka zauważyła, że tam jest BEZPIECZNIE i CICHO. (Łt:9,3,1=SS). Fine, whatever. Lilia nie chce być asystentką i nie ceni sobie Ateny. W sumie, ani Atena ani Lilia się nie cenią wzajemnie - to będzie zabawne. Lilia powiedziała, że pojedzie - nie będzie robić więcej kłopotów.

Pięknotka jest szczęśliwa. Jeden problem z głowy.

_Mrowisko_

Gdy Pięknotka szła spotkać się z Erwinem do swojego pokoju, zastąpiła jej drogę wysoka dziewczyna. Po czym padła przed nią na kolana i prosi o wysłuchanie. Wstrząsnęło to Pięknotką - wzięła ją do knajpki Szkarłatny Szept.

_Knajpka Szkarłatny Szept_

Pięknotka zrozumiała, że ta dziewczyna to Brygida Maczkowik. Atena nie chce mieć asystentki, więc tej się pozbyła - niech robi dla niej lokalne rzeczy. Ale Brygida jest śmiertelnie przerażona Cieniaszczytem. Atena powiedziała jej, że jeśli Brygida ucieknie, jest kill-on-sight. Brygida błaga więc Pięknotkę o wstawiennictwo - Brygida nie chce zostać sama w Cieniaszczycie. Nie sama, nie tu, nie tak.

Pięknotka przeklęła w duszy Atenę trzykrotnie, po czym skomunikowała się z Pietrem. Może on przygarnie viciniuskę i znajdzie dla niej dom? Pietro przyszedł; Pięknotka przedstawiła mu opowieść Brygidy w najbardziej żałosnym świetle jakim potrafiła. Niech on się nią zajmie - ona może mieszkać TYLKO w Cieniaszczycie, a przecież się strasznie boi. Pietrowi serduszko stopniało, ale czy na tyle, by przyjąć ją do siebie na stałe i wziąć za nią odpowiedzialność? (Tr+2:7,3,6=S). Pietro powiedział, że się nią zajmie. Wziął ze sobą pochlipującą Brygidę. Znajdzie jej jakiś dom i zajęcie, przygarnie Kropka.

Pięknotka wysłała Atenie sygnał "pozbyłam się właśnie Twojej asystentki. Doceń". Atena z kąśliwością zauważyła, że Pięknotka wpakowała ją już w kolejną...

Wpływ:

* Żółw: 3
* Kić: 1

**Scena**: (15:22)

_Mrowisko_

Pięknotka już PRAWIE otworzyła drzwi do swojego mieszkania gdzie dzielnie czeka Erwin, gdy dostała kolejny sygnał hipernetowy - od Zbigniewa. Zbigniew powiedział, że Pięknotka pomogła jednemu z "naszych", więc chce by Pięknotka wiedziała - Waleria planuje jak dobrać się do Julii. Chce wzmocnić oddział. W końcu Julia jako Emulator jest bardzo groźną wojowniczką - Łysym Psom się zdecydowanie przydała. No ale Pięknotka i Zbigniew nie rozmawiali.

Pięknotka westchnęła. Świetnie. Jeszcze jedna sprawa. Ale WPIERW - Erwin. Wróciła do pokoju. A Erwin tam pracuje nad niej power suitem - wbrew WSZELKIM regulacjom Cieniaszczytu... bo tu nie wolno...

Pięknotka porozmawiała z Erwinem na temat Cieniaszczytu JESZCZE RAZ. No i powiedziała, że potrzebne jest nowe ciało Minerwy. Erwin ma specyfikację ciała zrobioną przez Saitaera. Łącznie z wszystkimi komponentami...

Dobrze. Wpierw trzeba kupić kawałek ciała :-). Wzięła Erwina i poszła do Nukleonu.

_Kompleks Nukleon_

Mirela Niecień odmówiła wykonania ciała dla Pięknotki. To ciało nie ma sensu i się nie nada. Do uruchomienia będzie potrzebowało ofiary Krwi, z człowieka. Pod tak dokładną specyfikację to ciało po prostu nie będzie dobrze działać.

Pięknotka wyjaśniła Mireli, że jej celem nie jest sprawienie, że to ciało zadziała. Jej celem jest sprawienie, by inni myśleli, że to ciało zadziała. I w ten sposób Pięknotka wpakowuje ogromną ilość złota i zasobów w ciało, które najpewniej nigdy nie będzie użyte. Cóż... ale jest potrzebne, by zastawić pułapkę na Saitaera (o czym nie mówi Mireli). Mirela powiedziała, że może użyć istniejącego ciała i dostosować je odpowiednimi krwiami i kralothami - da się zbudować to, czego chce Pięknotka w ciągu tygodnia. Pięknotka jest bardzo wdzięczna. Erwin też.

Erwin się zmartwił. Ktoś musi umrzeć by Minerwa wróciła? Erwin się bardzo zasmucił. Minerwa już nigdy nie wróci. Pięknotka zauważyła, że to zależy od tego, czy Minerwa będzie miała wybór...

_Kompleks Nukleon_

Czas porozmawiać z Amadeuszem Sowińskim... Pięknotka chce się pożegnać. I nie tylko ;-).

Amadeusz dowiedział się od Pięknotki, że ta pamięta o autowarze. Pięknotka powiedziała, że Arazille może wyłączyć tego autowara z akcji - Finis Vitae jest ofensywnym autowarem, czymś maksymalnie sprzecznym z Arazille. Pięknotka _widziała_ działania Finis Vitae, _wie_ jak bardzo sprzeczne są te koncepty. Zaproponowała opiekę paliatywną Kompleksu Nukleon - niech pojawi się azyl i "umieralnia" nad Studnią bez Dna. Arazille może być w stanie dosięgnąć ludzi pod kontrolą Finis Vitae.

Pięknotka wpuściła Arazille w systemy FV. Na pewno COŚ dosięgnęła. Amadeusz zauważył, że Arazille żyje miliony lat - dla niej kontrakt na 1000 lat to żadna sprawa. Pięknotka, że lepiej być zniewolonym przez Arazille niż przez Finis Vitae...

Amadeusz ma wybór między młotem a kowadłem. Zło lub zło. Pięknotka po prostu daje mu mniejsze zło do wybrania. Finis Vitae jest uszkodzony, ale się rozprzestrzeniał. Arazille jest nowym złem, silniejszym - ale ona może zatrzymać FV. No i raczej stanowi siłę zarówno pozytywną jak i negatywną. Pięknotka powiedziała, że śnią jej się po nocach ci wszyscy ludzie - i Pięknotka modli się, by Arazille ich Dotknęła... Pięknotka użyła wszystkich swoich kart - bohaterka, terminuska... wszystkich. (TrZ+1:9,3,5=S).

Amadeusz to zrobi. Pięknotka zrealizowała zobowiązanie wobec Arazille. Amadeusz zbuduje świątynię dla Arazille. W zamian za to, docelowo, straci stanowisko (-5 Wpływ). Jest to coś, co trzeba było zrobić - ale coś, czego mu nikt by nie wybaczył. Nawet, jeśli Cieniaszczyt by wybaczył, to polityka wymaga, by takiej klasy czyn tak sprzeczny z prawem Astorii został ukarany. Amadeusz zna implikacje, ale dla ochrony Cieniaszczytu i tak to zrobił...

Wpływ:

* Żółw: 0 (5)
* Kić: 1

**Scena**: (16:07)

Pięknotce zostały tak naprawdę ostatnie dwa wątki do zamknięcia zanim może wracać do domu. Po pierwsze, chce wzmocnić hold na Erwinie - on nie ma patrzeć na Minerwę czy jakąś Lilię. On ma patrzeć na nią. Po drugie, Julia. Pięknotka nie chce, by Julia trafiła do Łysych Psów. Nie chce, by Emulatorka z broni stała się Bronią. Ale chciałaby tak zrobić, by przy okazji nie stracić przyjaźni z Psami - a zwłaszcza Walerii (i trochę Moktara).

Problem Erwina to nie problem. To okazja by z nim trochę pobyć i pouwodzić. Pięknotka wytacza ciężkie działa. (TpZ+1:13,3,2=P,S). Erwin nie spojrzy na inną kobietę niż Pięknotka - jest jego pierwszym wyborem teraz i na długo. Ale nadal ma dużo ciepła i miłości do Minerwy. Jak musi wybrać, Pięknotkę. Ale Minerwa jest jego "pierwszą miłością" i Erwin po prostu nie jest w stanie o niej zapomnieć czy widzieć, jak Minerwa cierpi. Pięknotce to nie przeszkadza, acz jest irytujące ponieważ Saitaer... i to sprawia, że Minerwę niestety trzeba uratować...

No dobrze, a Julia?

_Colubrinus Psiarnia_

Moktar Gradon i Pięknotka Diakon. Pięknotka wie, że praktycznie załatwiła Moktarowi statek - acz jeśli będzie chciała iść za ciosem by statek się pojawił, Atena jeszcze bardziej dostanie.

Pięknotka próbowała wpłynąć na Moktara, by ten pozwolił Julii być wolną, by nie polował na nią by wzmocnić Psy. Moktar zauważył, że Pięknotka chce walczyć z bogiem. Z Saitaerem. Nie stać ich na to, by nie wziąć każdej możliwej broni. On jest w stanie uwolnić Julię. Gdy Pięknotka poprosiła, by Moktar oddzielił Julię i Bogdana, Moktar powiedział, że Bogdan nie ma prawa zabawiać się z Walerią - bo jest członkiem oddziału. Tak samo jak Julia będzie członkiem oddziału, jest off-limits.

Pięknotka ze zwieszoną głową się zgodziła. Nie wygrała tej bitwy. Ale niech Julia ma przynajmniej prawo wyboru, niech Moktar jej przedstawi opcje. Moktar się zgodził. Z jego punktu widzenia nie będzie to miało żadnego znaczenia.

Wpływ:

* Żółw: 2 (7)
* Kić: 1

**Epilog**: (16:45)

* Julia poszła się czegoś napić w Szkarłatnym Szepcie i napotkała tam Bogdana...

### Wpływ na świat

| Kto           | Wolnych | Sumarycznie |
|---------------|---------|-------------|
| Żółw          |    2    |      7      |
| Kić           |    3    |      3      |

Czyli:

* (K): Julia ma wybór, Romuald idzie za Psami za Julią.

## Streszczenie

Pięknotka zdobyła nowe ciało dla Minerwy (acz do jego uruchomienia człowiek musi być poświęcony!), przekonała Amadeusza do założenia świątyni Arazille do blokowania Finis Vitae oraz wepchnęła Lilię jako asystentkę Atenie. Do tego rozkochała solidnie Erwina i zamknęła wszystkie wątki, by móc wracać do Pustogoru.

## Progresja

* Pięknotka Diakon: uwolniła się od długu u Arazille. Wszystkie tematy w Cieniaszczycie załatwione - wraca do Pustogoru.
* Lilia Ursus: została nieszczęśliwą asystenką Ateny Sowińskiej. Niezbyt się szanują z Ateną.
* Atena Sowińska: wróciła (z Lilią) na Stację Orbitalną Epirjon. Kapitan na statku.
* Erwin Galilien: uleczony od wpływów Saitaera, zakochany w Pięknotce (choć nadal ma serce do Minerwy).
* Amadeusz Sowiński: za miesiąc straci stanowisko przywódcy terminusów Cieniaszczytu - za świątynię Arazille blokującą Finis Vitae.
* Brygida Maczkowik: skończyła jako wolna viciniuska żyjąca w Cieniaszczycie, pod opieką Pietra Dwarczana. Jej wątek jest (chyba) zakończony.
* Saitaer: zapewnił odpowiednie spowolnienie Pięknotki przy jej powrocie do Pustogoru. Ma dość czasu - Minerwę i infekcję rzeczywistości.
* Finis Vitae: po stworzeniu świątyni Arazille zapadł w sen, nieskończony sen nieskończonego zwycięstwa. Nojrepy dalej działają, ale bez jego intencji.
* Julia Morwisz: zostaje Łysym Psem, ale z wyboru. Może mu powiedzieć "nie". Jest jak Waleria a nie jak zabawka.
* Romuald Czurukin: mimo sytuacji z Julią zostaje z nią. Moktar pozwala mu dołączyć jako bard czy coś. Nie jest pełnym Psem, ale jest tolerowany.

### Frakcji

* 

## Zasługi

* Pięknotka Diakon: wyprostowała wszystkie tematy w Cieniaszczycie, uwolniła się od długu Arazille, zdobyła ciało dla Minerwy i poderwała skutecznie Erwina.
* Waleria Cyklon: w krew wchodzi jej porywanie magów - wpierw Lilia, potem Erwin. Planowała jak ściągnąć Julię do Łysych Psów - z powodzeniem.
* Erwin Galilien: wpierw dał się porwać Walerii, potem nie rozumiał Cieniaszczytu i na końcu nie zrobił nic produktywnego - ale jest szczęśliwy z Pięknotką.
* Atena Sowińska: tak dzielnie pozbyła się jednej asystentki, by dostać drugą przez Pięknotkę - równie bezużyteczną (Lilię). Nie jest szczęśliwa, zwłaszcza po remprymendzie.
* Lilia Ursus: porwana przez Walerię, jest w silnej depresji. Nie jest bezpieczna, nie ma celu. Zgodziła się na warunki Pięknotki by nie sprawiać kłopotu. Low point in her life.
* Brygida Maczkowik: tak strasznie się bała zostać sama w Cieniaszczycie że wybłagała u Pięknotki wstawiennictwo - terminuska załatwiła jej opiekę Pietro Dwarczana.
* Pietro Dwarczan: Pięknotka wzięła go na litość i wziął na siebie opiekę nad viciniuską, Brygidą Maczkowik.
* Mirela Niecień: niechętnie budowała dla Pięknotki wspomagane ciało dla Minerwy na planach Saitaera. Próbowała wyjaśnić, czemu to zły pomysł, ale Pięknotka była zdeterminowana.
* Amadeusz Sowiński: wziął na siebie zbudowanie świątyni Arazille nad Studnią Bez Dna. Zabezpieczył Cieniaszczyt przed autowarem Finis Vitae ukrytym pod ziemią.
* Moktar Gradon: przekonał (?!) Pięknotkę, że jeśli walka ma się toczyć z Saitaerem, Pięknotka CHCE by Psy miały Julię na pełnej mocy.
* Zbigniew Burzycki: Łysy Pies z dobrym sercem. Fatalna poezja, szturmowiec, chętnie przygarnia słabsze istotki. Nie przeszkadza to w nieskończonej żądzy władzy i kobiet.

## Plany

* 

### Frakcji

* 

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Przelotyk
                        1. Przelotyk Wschodni
                            1. Cieniaszczyt
                                1. Kompleks Nukleon: miejsce kupowania i przekształcania ciał oraz tam, gdzie Pięknotka trzyma Lilię gdy nie jest w Psiarni.
                                1. Mrowisko: Erwin chciał tam budować power suit (wbrew zasadom). Miejsce głównych "negocjacji" Pięknotki z Erwinem.
                                1. Knajpka Szkarłatny Szept: Pięknotka zapoznała Pietra i Brygidę oraz wrobiła Pietra w opiekę nad viciniuską. Ona płaciła.
                1. Sojusz Letejski, NW
                    1. Ruiniec
                        1. Colubrinus Psiarnia: tymczasowa przechowalnia bezradnych petów Pięknotki (Erwina i Lilii). Miejsce spotkań i dowodzenia.
                        1. Świątynia Bez Dna: paliatywna świątynia Arazille nad Studnią Bez Dna, założona przez Amadeusza Sowińskiego dla neutralizacji Finis Vitae.

## Czas

* Opóźnienie: 1
* Dni: 3

## Narzędzia MG

### Budowa sesji

**SCENA:**: Nie aplikuje

### Omówienie celu

* nic

## Wykorzystana mechanika

1811

---
layout: cybermagic-konspekt
title: "Porwanie cywila z Kokitii"
threads: legenda-arianny
gm: żółw
players: kić, fox, kapsel
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [210209 - Wolna TAI na K1](210209-wolna-tai-na-k1)

### Chronologiczna

* [210209 - Wolna TAI na K1](210209-wolna-tai-na-k1)

## Budowa sesji

### Stan aktualny

.

### Dark Future

* .

### Pytania

1. .

### Dominujące uczucia

* .

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Punkt zero

.

## Misja właściwa

Infernia. After this spaceship got rescued by the Lightsaber, it has been undergoing repairs. But what is extremely unfortunate – Infernia isn’t being repaired. Her position in the queue is changing, she is being pushed further and further away.

Arianna doesn’t really like it. So a question appears – why isn’t her spaceship – Infernia - being repaired? 

The first person to analyze this event was Claudia. Claudia tapped into internal communication systems to seek different commands and orders. She has managed to find something interesting – there has been in order to keep Infernia unrepaired. And she dug into it – it seems the special forces behind it.

But why?

Claudia has no idea. But she has a name – Medea Sowińska. The captain she does not really know. Doesn’t remember anything about this particular captain and her information is not really in the files. But she is special forces - that Claudia knows.

A next person to determine what is really going on with their beloved spaceship is Eustachy. He knows some engineers and he decided to talk around. Asking around those engineers working on Infernia is managed to get an alarming information – some of Infernia’s parts are being cannibalized for a different starship. And this is a high-profile action.

In other words, OO Niobe is cannibalizing some of OO Infernia’s parts. Mostly those which were touched by crystalline entity of AK Leotis. And the QShip components. And some of their Persephone’s databases.

And who is a captain of Niobe? Why, Medea Sowińska. The very same. Unfortunately, in the process of extracting that information, Medea has learned that she is being monitored by Infernia’s crew.

Eustachy pulled some more information on Niobe. That is a starship designed to fight inside Stargates. In other words, it is an assault frigate class special forces ship. And Niobe’s captain never leaves the starships deck. Intriguing.

The last person to get information on a secretive captain and her special starship was Arianna. She met with Admiral Anthony Kramer, who is Arianna supporter, and she decided to ask about Medea and Niobe.

The Admiral explained to Arianna that there has been a complaint placed against her. Namely, a captain of the lightsaber with his main engineer accused Arianna of a sabotage. There has been no proofs, of course, but it was enough to slow down repair of Arianna’s ship. And Medea use that opportunity to do some kind of black operation which requires something from Infernia.

When Arianna pressed on trying to understand who Medea is, Kramer smiled. He explained that Medea once was like Arianna. She was a hotheaded and a very competent commodore. But hearing one of the operations there has been a horrible accident – Medea was fused with a starship into one amalgam entity. Somehow the doctors managed to save her, but she decided to cut the contact with her former crew members, she got demoted to the rank of the captain and she left to the special forces. In other words, not the nicest person.

And Arianna is pretty sure that now that Arianna is asking around about Medea, that Medea will be looking into Arianna and she might want to do something to Arianna. So Arianna is on a ticking clock now. But if she does nothing, she has no access to Infernia for a while.

As they say, the best defense is offense. So Arianna decided to sabotage Niobe - or at least, to confront Medea directly.

The plan how to sabotage a special forces star ship located in a drydock was not the easiest thing to do. First things first – it is important to distract everyone. So Claudia used her deep abilities to work with documents – she found a set of the most obscure forms and most complicated things to be signed and presented those to Niobe’s crew.

Of course, all those documents were connected to the spare parts which came from Infernia. But officially this did not happen. Therefore, there was a lot of checking, signing, everything.

This gave Arianna time to sneak onto Niobe.

Unfortunately, Medea noticed that Arianna has snuck into her starship and Medea trapped Arianna between two hatches. Arianna waited a bit until Medea came to see her.

When weather started scolding Arianna, the Commodore sweetly explained that most parts which were cannibalized from Infernia were modified and touched by her expert insane engineer – Eustachy. That means that those parts are operating beyond the standard levels of acceptance, but if you don’t know how to use them you may damage your starship or your starship might even explode. And Arianna knows a lot about exploding starships.

Well, Medea is not expected that. And she really didn’t want her starship explode. So the negotiations have started. And as a result of the negotiations Medea explained her plan to Arianna:

There exists a starship which does not come from Astoria. Her name is Kokitia. Kokitia traverses between two gates: from one sector via Astorian sector to a different sector. And that ship - Kokitia - contains state-of-the-art Hunter killer technology.

In other words, it is a small carrier. On that carrier there is a set of civilians being repatriated. One of those civilians needs to be kidnapped and brought to Astoria. But – the nation which built Kokitia is not in the state of war with Astoria. So it has to be done in such a way pirates or something has kidnapped this person.

Enter the cosmic anomaly. AK Leotis, which Infernia has encountered in the past. Medea is taking components from Infernia so that her starship – Niobe - can pretend to be AK Leotis. An unyielding cosmic anomaly, scary anomalous pirate ship.

Now, no pirate ship is able to attack something like Kokitia in normal space. On top of that, everything between two star gates is a responsibility of Astoria. Therefore the assault needs to take place inside the gates. Thus, Niobe.

Of course, Medea explained all that to Arianna only after Arianna decided she will help special forces. Because Arianna had no choice at that point. Medea needs her. Otherwise there's a risk of something blowing up because Eustachy ;-).

Medea managed to get Arianna’s cooperation, but it was not free:

* Infernia will be put on the front of repair queue. Arianna wants to return to space in her starship.
* The charges made by the crew of the Lightsaber against her and her crew shall be dropped
* Medea and her starship will look in a positive light on Arianna and Infernia
* Also, special forces will look in a positive light on Arianna and Infernia
* BUT: as a side effects, there is a gossip that Arianna and team have joined special forces or are lapdogs of special forces
* but: what is worse, this particular special forces Medea belongs to are specializing in black operations. They are considered to be radical hardliners. So there is that.

Therefore Niobe got assistance – Arianna, Claudia, Eustachy and Leona (with some marines). They are enough to be able to properly assault Kokitia in space.

Niobe was stalking Kokitia until Kokitia jumped through the gate. Niobe followed. While present in _unreality_, Medea ordered Arianna to set the signature that Niobe is Leotis. Also, to set IFF properly to a different Empire - that way as a nice side effect Medea might be able to start a war between sectors.

Arianna really didn’t want to do that. She did put IFF of „not-Astoria” and „Leotis”, but she did not put any false clues which could create any type of political turmoil between nations at peace. Arianna doesn’t accept being a cause of war, no matter what black ops require of her.

So with Niobe perfectly masked, there is a time to prepare an assault. Here, Claudia had a perfect idea – let’s use the crystals from Leotis. Let’s devastate Kokitia. Let’s co-opt the other spaceship and in the turmoil let’s win and take whatever they need. The ship will never return or will become an anomaly, but on the other side of the gate, therefore it is completely not the problem of Astoria.

Arianna blocked his idea. Medea approved this idea. Arianna started explaining that she does not accept it – this type of crystal infestation would be a risk of all civilian spaceships, no matter who they belong to. No one has any surety that the problem will not expand to Astoria.

Eustachy followed Arianna - he has a better idea. If there is any type of conflict between sides, Astoria can become a trade hub between the sectors. A crystalline nightmare would just stop the potential for Astoria to live long and prosper.

Medea listens to the arguments and smiled. She agreed with both Arianna and Eustachy. Medea explained that special forces are not mindless killers, they are not designed to hurt and maim. If Medea wants to give some autonomy to Arianna and her crew, she needs to be sure they will not overextend and focus on hurting people. After all, Arianna has a reputation of being one who is being followed by explosions and suffering.

Slightly unhappy, Claudia suggested something else. She can use a raw aether – between the gates there is a lot of aether in unreality. If so, she is able to bombard Kokitia with false signals, visions, overwhelm their thaumatic shields. Mix reality with nightmares. That way when Leona enters the fray, she might not really have any opposition. They might hurt less people and achieve more success.

Medea accepted that. Indeed – linking herself with very advanced systems of Niobe, Claudia has managed to stir the aether and unleash a lot of power upon unsuspecting Kokitia. She has managed to completely disable the thaumatic shields and she has managed to mix reality and fiction upon Kokitia’s deck.

But Claudia was not able to control this power. The same feedback effect was inflicted upon Niobi. Main difference – people at Niobe know what is going on.

Now, Eustachy had another plan. Why pretend they are pirates, which is slightly unrealistic – a pirate ship would not be able to do such incredible feats they have done. He suggested to use Niobe’s armament to pretend that live in unreality are assaulting Kokitia. If the assault is convincing enough, according to procedures the nonessential personnel should be relocated next to most shielded parts of the starship.

And he can open one of those parts surgically in such a way that Leona can perform a safe extraction of the target.

And he has succeeded. Leona, being a human in the world of Magi has one major advantage – she has a lot of anti-magic implants and systems. That gives her higher resistance against all of that what is going on right now. Therefore she volunteered to breach Kokitia and extract the target.

And she succeeded. Leona got highly irrigated and wounded, and the woman to be extracted with her child also got very hurt, but Leona won. As a positive side effect, Leona extracted about 10 more people from there. Eustachy shot appropriate parts of the ship to make it look like there was a natural breach and missing people were casualties of the unreality.

After everything, Niobe returned to Astorian Sector, being undetected and causing minimal damage.

Special forces are pleased.

## Streszczenie

Infernia jakoś nie może zostać naprawiona a skarga Mateusza Sowińskiego nie pomaga. Po śledztwie Zespołu okazuje się, że Infernia jest kanibalizowana przez Niobe, do jakiejś misji sił specjalnych. Arianna z ekipą wkręciła się na tą misję, przekonując Medeę Sowińską, że bez niej to się nie uda. Misja polegała na ataku na Kokitię pomiędzy Bramami i ekstrakcja z Kokitii cywila, udając piratów. Dzięki załodze Inferni udało się to lepiej niż ktokolwiek mógłby się spodziewać i niż Medea zaplanowała sama. 

## Progresja

* Arianna Verlen: łaskawe oko Sił Specjalnych, frakcja "Gorący Lód" oraz Medei Sowińskiej. Też plotki że współpracuje z tymi radykałami sił specjalnych Orbitera.
* Klaudia Stryk: łaskawe oko Sił Specjalnych, frakcja "Gorący Lód" oraz Medei Sowińskiej. Też plotki że współpracuje z tymi radykałami sił specjalnych Orbitera.
* Eustachy Korkoran: łaskawe oko Sił Specjalnych, frakcja "Gorący Lód" oraz Medei Sowińskiej. Też plotki że współpracuje z tymi radykałami sił specjalnych Orbitera.

### Frakcji

* .

## Zasługi

* Arianna Verlen: postawiła się Medei w sprawie eksterminacji (i przeszła test) oraz zmusiła Medeę do wzięcia jej oficerów na pokład Niobe - do misji specjalnych.
* Klaudia Stryk: dywersja dokumentowa wprowadziła Ariannę na pokład Niobe. Potem użycie Eteru w Nierzeczywistości skołowało Kokitię i przeciążyło jej tarcze taumiczne.
* Eustachy Korkoran: stał lojalnie za Arianną w konflikcie z Medeą oraz mistrzowsko skołował Kokitię strzałami artylerii - nigdy nie wiedzieli, że ktokolwiek ich zaatakował.
* Leona Astrienko: komandos; wpadła na Kokitię i ekstraktowała kilku cywilów plus cel (Alarę z córką). Ciężko poparzona, ale udowodniła że potrafi.
* Antoni Kramer: admirał Orbitera i zwolennik Arianny; powiedział jej parę rzeczy o Medei, m.in. jak stała się tylko kapitanem w wyniku wypadku.
* Alara Ehmes: cywil z pokładu Kokitii; ona i jej córka zostały przechwycone przez Niobe podczas black ops w wykonaniu "Gorącego Lodu".
* Medea Sowińska: zintegrowana z Niobe agentka sił specjalnych Orbitera (frakcja "Gorący Lód"). Zaakceptowała współpracę z Arianną i zdecydowała się ją wzmocnić.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria, Orbita
                1. Kontroler Pierwszy
            1. Brama Kariańska
            1. Brama Trzypływów
    1. Nierzeczywistość

## Czas

* Opóźnienie: 27
* Dni: 5

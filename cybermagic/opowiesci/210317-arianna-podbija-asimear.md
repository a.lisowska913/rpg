---
layout: cybermagic-konspekt
title: "Arianna podbija Asimear"
threads: legenda-arianny
gm: żółw
players: kić, fox, kapsel
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [210218 - Infernia jako Goldarion](210218-infernia-jako-goldarion)

### Chronologiczna

* [210218 - Infernia jako Goldarion](210218-infernia-jako-goldarion)

## Punkt zerowy

### Dark Past

Kontekst sesji:

* Jolanta zbudowała niewielką bazę na Asimear, angażując do tego firmę Marysznit. Marysznit jest opanowany przez Kult Ośmiornicy. 
* Jolanta, podczas snu, zostaje porwana. Morrigan to widzi, ale nie interweniuje - nie wie o co chodzi.
* Kraloth przemontowuje Governess na swoje nadrzędne systemy kontrolne. Morrigan interweniuje - nie może wygrać, może uciec.
* Morrigan ma problem - jak sprawić, by Jolanta nie została zniszczona? Decyduje, że "porwała ją Arianna Verlen".
    * To sprawia, że kraloth potrzebuje Jolanty AKTYWNEJ. Poza Governorem nie robi w niej żadnych ingerencji.
    * Wszyscy polują na Ariannę Verlen XD. A Morrigan planuje uszkodzenie Aesimar, by zmusić Aurum do interwencji i ratowania cywili.
* Kult Ośmiornicy: CHCE zdobyć statek "Płetwal Błękitny", by móc zdobyć space-capability.
* Wszyscy: CHCĄ zniszczyć Morrigan @ Tirakal
* Morrigan: CHCE zwrócić uwagę wszystkich na kult. Jak go znajdzie.

Poprzednia sesja:

* Zniknął advancer admirałowi Kramerowi, na planetoidzie Aasimar
* Tomasz i Jolanta Sowińscy wierzą, że Arianna Verlen to postać fikcyjna
    * Jolanta jest na Aasimar, w Tirakalu (nanokonstrukcyjnym servarze)
* Politycznie: 
    * Aurum chce własną, niezależną od Orbitera flotę
    * Orbiter (a dokładniej admirał Termia) sabotuje te plany
    * Wszyscy eksperymentują
* Infernia zamaskowana za Goldariona, dociera do Planetoidy Aasimar.
    * Tomasz Sowiński wierzy, że Arianna Verlen jest "Monisią" i on ją kocha i ją uratuje z Goldariona.
    * Entropik Jolanty się zbrickował, bo Elena próbowała się z nim połączyć. 

### Opis sytuacji

Planetoida Asimear jest:

* planetoidą o średnicy mniej więcej 90 km 
* znajdującą się w okolicach Pasu Teliriańskiego, w obszarze Planetoid Kazimierza.

By dość szybki statek mógł dotrzeć do Pasa Teliriańskiego, potrzeba mniej więcej 10 dni drogi z Kontrolera Pierwszego.

Planetoidy Kazimierza są uważane za miejsce o tyle interesujące, że znajduje się tam nadmierne stężenie materiałów anomalnych i rzadkich. Wszystko wskazuje na to, że powinna się tam znajdować jakaś dziwna Anomalia Kosmiczna - a jednak nic nie zlokalizowano. Tak więc po wielu badaniach uznano, że jednak jest to miejsce stosunkowo bezpieczne. Działają tam grupy ekstrakcyjne i nomadyczne statki-kolonie, z własnym "rządem", który akceptuje obecność Orbitera, ale nie traktuje ich jak władców sektora astoriańskiego.

Dzisiejszosesjowa planetoida, Asimear, jest dość istotną planetoidą z perspektywy ekonomicznej; wydobywane są tam rzadkie minerały, m.in. astinian - komputronium, wykorzystywane przy produkcji TAI. Innym jest morfelin, czyli "programowalna materia" - materia zdolna do zmiany kształtu pod wpływem energii sprzężonej z konkretnymi rozkazami.

By wydobywać minerały z Asimear, nad planetoidą znajduje się statek-platforma-miasto. Ta platforma nazywa się Lazarin. Tam znajduje się większość populacji mieszkającej i obsługującej wydobycie z Aesimar. Jednocześnie na samej Asimear znajdują się grupy osób mieszkających tam, "tubylców", którzy nie są powiązani z Lazarin (albo chcą działać niezależnie od Lazarin).

Dodajmy do tego fakt, że w okolicach Asimear znikają czasem ludzie, bo "noktiańscy komandosi nie skończyli wojny" i mamy już pełny obraz sytuacji.

Na to wszystko pojawia się Jolanta Sowińska z Aurum w nietypowym servarze podobnym do Entropika, ale większym. Do tego - Kramer podejrzewał, że działania tam różnych postaci są powiązane z próbą programu kosmicznego Aurum i uniezależnienia się od Orbitera. Wysłał więc swojego advancera, by ten rozpoznał teren. Advancer jednak zniknął. To wymaga użycia Inferni...

### Postacie

* Jolanta Sowińska: dziwna neurosprzężona Sowińska; postrach Tomasza. Kiedyś go kociła.
* Tomasz Sowiński: młody, przerażony mag który ma podejście "jesteśmy męscy". Nie wie jak się zachowywać w towarzystwie nie-Sowińskich. Dość nierozsądny.
* TAI Morrigan d'Tirakal: TAI współsprzężona z Tirakal, słuchająca tylko Jolanty Sowińskiej. Podobno pod kontrolą Arianny Verlen (która nie jest fikcyjna?).

## Punkt zero

(cel: pokazać, jak niesamowicie dobrze pamięta wszystko Jolanta i jak dobrym jest naukowcem --> pokazać scenę z jej przeszłości, w której pokonuje trzy osoby używając technologii, świetnej pamięci i neurosprzężenia)

Przeszłość. Ponad 10 lat temu. Jolanta jest młoda. 1v3: Jola vs 3 innych Sowińskich, grają w zmodyfikowaną siatkówkę. Jola z dwoma robotami vs 3 postacie graczy. Wpierw - kwestia taktyki. Trójka bardzo ciężko zakuwała i się starała (XVV) - udało im się zdobyć nad Jolantą przewagę; mimo jej perfekcyjnej pamięci i wspomagania komputerowego, udało im się jej dorównać w teoretycznych zadaniach. Czyli - przy PRZYGOTOWANIACH trzech osób były w stanie jej dorównać.

Podczas dalszej rozgrywki, jej roboty są świetnie skoordynowane. Jeden z Sowińskich rzucił zaklęcie - niech roboty się zderzą ze sobą i nie mogą wydajnie grać. (VXV). Sukces - roboty są unieszkodliwione, jest tylko Jolanta po tamtej stronie - ale Jolanta wie, że tu była użyta magia.

Jolanta kazała robotom otworzyć ogień. Zrobiła ogień zaporowy i wygrała punkt. Inna Sowińska rzuciła się by sprawdzić blef i wygrać. (VVXXV). Sukces - udało im się wygrać z Jolantą, ale dziewczyna została poparzona przez działka robotów. Jola jest wściekła. Nie dlatego, że wygrali - to było możliwe. Nie dlatego, że została ośmieszona. Dlatego, że to było bardzo niebezpieczne i niemożliwe do przewidzenia. Mogła jej się stać krzywda.

Nawet wściekła, Jolanta wścieka się na zimno i zachowuje elegancję...

## Misja właściwa

Goldarion zbliża się do stacji Lazarin. Komunikat od Martyny Bianistek, dowódcy "Płetwala Błękitnego" - "Leszert ty wieprzu". Zaskakujące dla Eustachego, który musi udawać Leszerta ale nie wie o co chodzi i kim jest ta kobieta. No nic, po krótkiej rozmowie udało się Eustachemu dojść do następujących faktów:

* Martyna jest WŚCIEKŁA na kapitana Leszerta.
* Leszert wisi kasę Martynie. I ona chce ją z powrotem.
* Martyna jest skłonna by wystrzelić z anty-asteroidowego działa w Goldariona lub zrobić abordaż. (narażając STRASZNIE misję Inferni i to, że Infernia jest ukryta)

Klaudia ma dość. Za pozwoleniem Eustachego, przygotowała Bazyliszka i wysłała sygnał do biednej Semli d'Płetwal używając standardowych kodów kontrolnych.

* V: "to jest wypadek", Płetwal jest w złym stanie
* V: awaria systemu i absolutny shutdown Semli
* V: Płetwal jest wyłączony z akcji. Permadeath, jeśli nie zostanie naprawiony.

Tomasz Sowiński się otwarcie śmieje z Płetwala. Rozmarzył się - kupi ten statek do NOWEJ LEPSZEJ FLOTY SOWIŃSKICH. Da też Ariannę na pokład tego statku, to będzie jej okręt flagowy...

* "Rozważ to, może być to dobry ruch z perspektywy kariery" - rozbawiony Eustachy do Arianny

...ale Sowiński wywali kapitan Martynę Bianistek; ona jest wyraźnie niekompetentna, skoro pozwoliła by jej jednostka się sama rozsypała. Sowiński wyraźnie nie ogarnia sytuacji takiej, że kogoś może nie być stać na własną jednostkę czy na zarządzanie finansowo własną jednostką. Arianna jednak spróbowała przekonać Sowińskiego, by nie kupował tej jednostki. Ona bardzo nie chce by to się działo. Tak więc (V) udało jej się przekonać Sowińskiego by zostawił to w spokoju. Niestety, wyszło jako "ok, nie kupię <pat pat pat>".

Martyna nawiązała kontakt z Eustachym. Powiedziała, że wygrał; Płetwal nie działa, popsuł się. Ona potrzebuje pieniędzy, bo inaczej Płetwal nigdzie nie poleci. Jest zdesperowana. Eustachy nic jej nie obiecał; zostawił temat na kiedy indziej. Teraz jest zajęty. (Niech Martyna się trochę poddusi w strachu).

Infernia dostała sygnał ze stacji Lazarin. To retransmisja czegoś z planetoidy. Infernia rzuciła to na audio-video. To... Arianna Verlen! A dokładnie, Arianna taka jaką była w _Sekrecie Orbitera_. "Arianna" powiedziała, że porwała servar i doprowadzi do zniszczenia Lazarin i Asimear, jeśli nie oddadzą jej Jolanty Sowińskiej. Jest w stanie to zrobić, jest w końcu arcymagiem najwyższej klasy i ma najlepszą jednostkę na planetoidzie - a nikt tu nie ma sił ani narzędzi zdolnych by ją powstrzymać.

Tomasz Sowiński jest w absolutnym szoku. Arianna Verlen jest przecież postacią fikcyjną!!! Arianna też jest w szoku, ale z zupełnie innych powodów. To nie ona XD. Ona nie jest terrorystką XD.

Połączenie hipernetowe. Z Arianną próbuje połączyć się... Jolanta Sowińska. No fakt, Arianna _jest w okolicy_, więc połączenie hipernetowe jest możliwe...

Jolanta jest w szoku, że ktoś jej odpowiedział. Czyli Arianna Verlen tu jest, czyli jest prawdziwa. Czyli **naprawdę** skrzywdziła Anastazję! Czego Arianna chce?! Tu Arianna weszła nieco spokojniej - powiedziała, że jest w okolicy, faktycznie, jest też arcymagiem - ale nie ona stoi za tym wszystkim. Nie ona porwała jej servara. Jest aktualnie na orbicie, na pokładzie Goldariona i przywiozła Tomasza Sowińskiego. Jolanta nie zrozumiała - czy to znaczy że moc Arianny jest **tak wielka** że potrafi się teleportować z planetoidy na Goldariona? Że ukradła jej servara w TAKI SPOSÓB? Arianna stwierdziła, że nie - niedawno przyleciała, miała być _undercover_, ale nie do tego stopnia undercover by nie móc zainterweniować jak ktoś się pod nią podszywa.

Dość szybko doszły do porozumienia:

* Arianna wyląduje na planetoidzie pinasą
* Jolanta się z nią spotka
* Arianna przekaże Joli Tomasza i nowy servar klasy Entropik
* Arianna poszuka kim jest tajemniczy przeciwnik podszywający się za Ariannę.

Tak więc Infernia ("Goldarion") podleciała tyci bliżej Asimear, by Elena wzięła pinasę - i Elena, Arianna, Tomasz i Leona wylądowały na planetoidzie. Po drugiej stronie - 20 uzbrojonych niemilitarnych (górnicy itp.) + Jolanta. Zaczęli rozmowę. To, na co Tomasz zwrócił uwagę - Jolanta _zignorowała uszkodzenia Entropika_ i wyraźnie _zapomniała hasło do Entropika_. Arianna się zdziwiła - Jolanta słynie z tego, że jest perfekcyjna w tych dziedzinach. Gdy na sensorach pojawił się niewielki oddział (kilkanaście jednostek latających), Arianna podjęła decyzję - porywamy Jolantę.

* XXX: Leona zrobiła _TERROR_. Ranni, uszkodzenia itp. przy osłonie ewakuacji. Bardzo ciężko uszkodziła ochroniarzy Tomasza Sowińskiego (bo byli kompetentni i stali Ariannie na drodze)
* VV: udało się porwać Jolantę i Tomasza, ale Entropik został na planetoidzie.

Elena włączyła wszystkie silniki; uciekają przed flotą szybko poruszających się pojazdów. 

* X: pinasa została uszkodzona, silniki nie działają. Została im już tylko jedna pinasa.
* V: Elena użyła swojej jednostki ("Jastrzębia") i użyła go zdalnie jako silników.

Tomasz jest w panice. PIRACI MNIE PORWALI?! Jolanta jest dziwnie waleczna, ale nic czego Leona nie unieszkodliwi ;-).

Gdy Martyn dostał Jolantę w swoje ręce, zabrał się do naprawy Sowińskiej:

* X: naprawa potrwa 2-3 dni.
* V: mają odpowiedź od razu. Wiedzą, co się z Jolantą stało. Jolanta jest sprzężona z TAI i to sprzężenie jest całkowicie odcięte, na to miejsce wszedł inny byt - kralotyczny pasożyt, zastępujący sterowanie jej pamięcią i funkcją szczęścia sobą. Governor module.
* XX: Jolanta ma skrajne uzależnienie kralotyczne. Nie można jej ufać (tylko dzięki TAI)
* V: Jolanta nie została "zniszczona", jest możliwa do naprawienia. TAI odzyska połączenie.

Ale to znaczy, że gdzieś na planecie jest kraloth. I ten kraloth dał radę przejąć kontrolę nad Jolantą Sowińską. W świetle tego - kim jest "Arianna", która porwała Tirakal?

TODO:

* Advancer
* Odzyskać Tirakala i rozwiązać problem "Arianny". Może - odzyskać Entropika?
* Co z Martyną i Płetwalem?
* Jak się skończy linia Tomasz/Jolanta i czy Tomasz dowie się, że "Monisia" to Arianna?

## Streszczenie

"Płetwal Błękitny" ma na pieńku z "Goldarionem"; by nie złamać konspiracji, Klaudia potraktowała Płetwala Bazyliszkiem. Tymczasem pojawiła się wiadomość, że Arianna chce zniszczyć Asimear i żąda wydania Sowińskiej. Prawdziwa Arianna złamała konspirę, skontaktowała się z Jolantą Sowińską, po czym ją porwała. Okazało się, że Jolanta jest kontrolowana przez kralotycznego pasożyta...

## Progresja

* Jolanta Sowińska: usunięto jej pasożyta kralotycznego i odbudowano połączenie z TAI, ale jest skrajnie uzależniona kralotycznie.
* OO Infernia: straciła 1 ze swoich dwóch pinas; Elena nie dała rady wycofać się dość szybko przed tajemniczymi jednostkami.
* SCA Płetwal Błękitny: TAI Semla jest zniszczona Bazyliszkiem Inferni.
* Arianna Verlen: jej "sobowtórka" porwała Tirakal Jolancie Sowińskiej i zagroziła zniszczenie planetoidy. Powiedzmy, nie jest popularna w tej okolicy.

### Frakcji

* .

## Zasługi

* Arianna Verlen: złamała konspirację by porwać Jolantę Sowińską i jej pomóc. Plus, jakaś sobowtórka robi jej szkodę reputacyjną.
* Eustachy Korkoran: udając Aleksandra Leszerta irytował Martynę (kapitan Płetwala), by utrzymać konspirację - nie są Infernią, są Goldarionem. Polubił bycie kapitanem.
* Klaudia Stryk: złożyła i wysłała do "Płetwala Błękitnego" Bazyliszka, praktycznie niszcząc Semlę. Potem pomagała Martynowi w naprawieniu Joli Sowińskiej (usunięciu kralotycznego pasożyta i przywróceniu TAI)
* Jolanta Sowińska: spotkała się kiedyś z kralothem na Asimear, została przezeń zdominowana. Porwana przez Ariannę, naprawiona przez Martyna - ale kralotycznie uzależniona.
* Tomasz Sowiński: zorientował się, że z Jolantą jest coś bardzo poważnie nie tak - i przekazał tą wiadomość dyskretnie Ariannie (prowadząc do porwania Joli).
* Leona Astrienko: jedyna siła ognia jaką Arianna zabrała ze sobą na spotkanie z Eleną. Wystarczająca. Pokonała kralotycznie kontrolowanych cywili PLUS strażników Tomasza Sowińskiego (z zaskoczenia).
* Elena Verlen: mistrzowsko sterowała pinasą na Infernię, uciekając od tajemniczych jednostek na Asimear. 
* Martyn Hiwasser: z pomocą Klaudii dał radę naprawić Jolę Sowińską usuwając jej kralotycznego pasożyta i przepinając jej kontrolne TAI na swoje miejsce.
* Martyna Bianistek: chciała odzyskać pieniądze od "Leszerta", lecz po Bazyliszku (o którym nie wie) bardzo spokorniała i prosi "Leszerta" o pomoc. Nic jej to nie daje.
* SCA Płetwal Błękitny: dostał Bazyliszka od Inferni, przez co jest unieruchomiony w okolicach stacji Lazarin. Działają tylko systemy awaryjne.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria, Orbita
                1. Kontroler Pierwszy
            1. Pas Teliriański
                1. Planetoidy Kazimierza: istotne ekonomicznie planetoidy, zawierające sektor prywatny i sporo rzadkich minerałów
                    1. Planetoida Asimear: tam gdzieś, pomiędzy platformą a samą planetoidą zniknął advancer Kramera. I gdzieś tam jest tien Jolanta Sowińska. Źródło minerałów: astinianu i morfelinu.
                        1. Stacja Lazarin: krąży nad planetoidą Asimear; zwykle do niej zadokowane są wszystkie statki. Tym razem: niedaleko doszło do konfrontacji Infernia - Płetwal.

## Czas

* Opóźnienie: 17
* Dni: 15

## Inne

### Projekt sesji

#### Procedure

* Overarching Theme:
    * gracze mają "kierowanie" (Vision & Purpose); wiedzą gdzie iść i po co tam idą
    * gracze są bardziej zaangażowani w historię, bo widzą, gdzie ich decyzje prowadzą
    * MG wie, które sesje wprowadzać i które usuwać
    * MG wie, w którą stronę stawiać adwersariat
* Achronologia:
    * przez przeskakiwanie do przodu nieważnych wydarzeń oczekuję, że każda sesja jest powiązana z kontekstem (gracze widzą jak się to rozwija)
    * przez cofanie się i uzupełnianie luk w przeszłości oczekuję, że brakujące MG i graczom luki uzupełnimy takimi działaniami
    * przez działanie w przeszłości gracze czują silniejszą relację z postaciami i światem, silniejsza budowa wspólnej historii.
* Wyraziste, ważne dla graczy postacie + Relacje (konflikt?) Gracze <-> NPC, NPC <-> NPC
    * graczom bardziej zależy na postaciach i ich powodzeniu
    * więcej potencjalnych wątków i rzeczy które możemy odrzucić przy krzyżykach
* Tory, zmiana postaci z czasem, wpływ graczy na świat
    * gracze widzą jak wpływają na świat. Wpływ jest "ich" - stąd tak tragiczne jest gdy "ich" rzeczy są corrupted / destroyed.

#### Result

* Overarching Theme:
    * V&P: sojusz z Sowińskimi przeciwko Krypcie? Pokazanie, że Arianna jest prawdziwa i całkiem fajna?
    * "Arianna Verlen nie istnieje, jest tylko aktorką": Sowińscy nie wierzą w Ariannę. A Arianna jest tu potrzebna.
    * "Lojalna TAI": TAI Morrigan osłoni Jolantę. Ale nie chce umierać, więc udaje Ariannę.
    * "Too far away, so far away...": Morrigan, która zniszczy Asimear. Jolanta, która ma Governor.
    * adwersariat: "ukojenie w mrocznym świecie": Kult Ośmiornicy, rozprzestrzeniający macki.
* Achronologia:
    * N/A.
* Wyraziste, ważne dla graczy postacie + Relacje (konflikt?) Gracze <-> NPC, NPC <-> NPC
    * Tomasz Sowiński: pragnie uratować wszystkich, ale nie zna świata. Jest _creepy_
    * Jolanta Sowińska: czarodziejka z niestabilną magią, która ma Governor by zostać homo superior. Wierzy w technologię nad magię.
    * Adam Kurczak: advancer Kramera, który dołączył do Kultu Ośmiornicy.
* Tory, zmiana postaci z czasem, wpływ graczy na świat
    * Ośmiornica: chce zdobyć statek kosmiczny "Płetwal Błękitny" Martyny Bianistki i użyć go, by zdobyć inne 

#### Scenes

* Scena 0: pokazanie potęgi Jolanty podczas jednej z gier (quiz + działanie) --> Jolanta jest niesamowicie skuteczna, wszystko pamięta. Neurosprzężenie + governor.
* Scena 1: na orbicie, Martyna Bianistek chce pieniędzy od "Leszerta". Blef, że strzeli. Ale wyśle oddział szturmowy.
* Scena 2: "Arianna": "porwę i zniszczę Jolantę Sowińską". "Zniszczę planetoidę, którą ta Sowińska splugawiła swą obecnością".
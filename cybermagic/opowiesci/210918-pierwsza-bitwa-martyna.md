---
layout: cybermagic-konspekt
title: "Pierwsza bitwa Martyna"
threads: młodosc-martyna
gm: kić
players: żółw
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [210912 - Medyk przeciwko Cieniaszczytowi](210912-medyk-przeciwko-cieniaszczytowi)

### Chronologiczna

* [210912 - Medyk przeciwko Cieniaszczytowi](210912-medyk-przeciwko-cieniaszczytowi)

### Plan sesji
#### Co się wydarzyło

.

#### Sukces graczy

* ?

### Sesja właściwa
#### Scena Zero - impl

.

#### Scena Właściwa - impl

Wiemy, że jest wojna. Wiemy, że część sił Noctis dostało się na planetę. Wiemy, że na planecie jest Saitaer. Wiemy też, że siły Astoriańskie współpracując ze sobą - Eternia, Zjednoczenie, wszyscy - w jakimś stopniu skupili się na walce z Saitaerem by tylko dostać cios w plecy od Noctis. Część naszych sił nie wiedziała jeszcze, że Noctis nie przyszło z odsieczą.

Astoria i Orbiter robią co mogą i nawet niedoszkolony Martyn jest im potrzebny... i jest na OO Invictus w charakterze medyka. Przeszedł podstawowe przeszkolenie.

Gdyby nie tak dramatyczna sytuacja, nigdy nie byłby na statku typu Invictus i Invictus nigdy by nie był wysłany na taką akcję. Invictus jest jednostką CYWILNĄ Noctis, przechwyconą i wzmocnioną. Q-ship. Dostali prośbę o ekstrakcję nieudanej akcji z nieznanego odizolowanego okrętu noktiańskiego. Invictus ma szkieletową załogę, awaryjny medbay, 15 osób militarnych i wszyscy mają taki sprzęt jaki był. Notable - mają jeden blaster launcher.

Przełożonym Martyna jest Paweł Diakon (Pandemiusz) i on dowodzi sickbay. Invictus jest dowodzony przez Jonasza Starana. Invictus ma misję "była akcja wymierzona w okręt noktiański, zrobił się fubar, trzeba ich wyciągnąć". 

Staran robi Public Announcement na cały okręt. "Rozkaz - uratować sojuszników z okrętu wroga. Nie wiemy co poszło nie tak, wiemy, że potrzebują ewakuacji jak najszybciej. Zlokalizowali samotny okręt przeciwnika (dziwne), my musimy wyciągnąć"

Martyn nie jest na poziomie dowódczym; nie może zadać niektórych pytań jakie by chciał i wojsko oduczyło go jawnego gadania. Ale Martyn IS AN UNRULY SPIRIT! Dowie się co powinien.

TEST SZCZĘŚCIA - 6v6 -> 

* V: minor damage, niewiele problemów jak chodzi o zbliżenie się do przeciwnika.
* (+1V): VVV: boarding party naszego statku miało duży sukces, wbiliśmy się na statek przeciwnika i lokalizowaniu sojuszników. Invictus podszedł, okręt noktiański nie wiedział co się dzieje, Invictus unieszkodliwił działa główne i rozpoczął firefight. A promy między statkiem przeciwnika i Invictusem transportują rannych.

Do sickbay zaczynają spływać eternianie w różnym stopniu skrzywdzenia. Martyn - Triage. Martyn widzi wśród eternianek znaczniki Kopiec. WSZYSCY mają blood ruby i wszyscy mają blood ruby Kopiec. Czyli każdy eternianin jest zależny od tien Kopiec.

Martyn ma dwie kategorie pacjentów - może nieść, jest niesiony. Martyn tego tak nie zostawia. Skupia się zgodnie z triage, ale nawiązuje rozmowę z jakąś damą z rannych sił eternijskich. Jako pretekst - niech coś mi przynosi i podaje by szybciej robił pracę. Jej imię to Andrea. Po tym jak przekazała parę przedmiotów, podała, pomogła itp Martyn zaczyna rozmowę.

* M: "Co tam się stało?"
* A: "Nie wiem. Mieliśmy wejść na statek i zdobyć paru żywcem. Z tien Kopiec przeszliśmy specjalne szkolenie."
* M: "Ona tam była?"
* A: "Ona tam jest. Zarządza tą ewakuacją."

Blood Rubies są aktywne, ale na rannych mniej aktywne.

* M: "Dlaczego misja się nie udała? Przyszli? Byli w złym miejscu? Nie pamiętasz?"
* A: "Cały statek obrócił się przeciwko nam? Spodziewali się nas? Nie wiem? Wiem, że skończyliśmy pod ciężkim ostrzałem walcząc o każdy krok."

Andrea wyraźnie jest zagubiona. Nie jest oficerem, nie rozumie sytuacji taktycznej, ale próbuje pomóc i podpowiedzieć. Informuje też, że tien Kopiec JEST ranna. I wszyscy pod nią to czują bo blood ruby. Martyn kazał komputerowi zrobić badania odnośnie _toxic agentów_ pod kątem czegoś krzywdzącego posiadaczy konkretnie _blood ruby_ .

Martyn wie, że tien Kopiec jest tam, jest ranna i jest kluczowym ogniwem. Więc proponuje swojemu szefowi, Pandemiuszowi, złożenie zaklęcia które pozwoli na wzmocnienie tien Kopiec używając magimedu i magii augmentacyjnej. Bo skoro blood ruby działają przez sympatię i oni mają tego sporo tutaj + jeśli mają eternian którzy mogą się na tym znać... silniejsza Kopiec to bezpieczniejsza ewakuacja.

Najwyższy rangą wśród eternian (Pandemiusz zażądał) to Emil Laratan d'Kopiec. Martyn Emilowi zreferował koncept. Po czym Pandemiusz powiedział "dobry pomysł, potrzebujesz pomocy, pytaj".

Emil dodał ekspertyzę. Cel Martyna: tylko energia. Blood Ruby nie pozwala na przekazanie zaklęcia; byłyby strasznym ryzykiem w takim momencie. Ale jeśli tien Kopiec będzie mieć szerokie pole energii z którego może czerpać... powinno zdecydowanie pomóc. Martyn używa banków energetycznych by zasilić blood ruby. Tyle, żeby tien Jolanta Kopiec mogła ssać spokojnie jak potrzebuje. Banki są na tyle duże że nie powinno nic się stać i nie powinny się wyczerpać.

Martyn jest tam pomocny, ale to katalista robi wszystko (prawidłowo). Cel: zasilić tien Kopiec przez Blood Ruby przez banki energii z medbay na Invictus.

TrMZ+3: 

* O: tymczasowy link Martyna i tien Kopiec. Ona nagle wyczuwa dodatkowy punkt w sieci a Martyn ma z nią komunikację na poziomie sygnałów emocjonalnych. Cień rozpoznania - ona wie że to ktoś kogo zna, ale nie rozpoznaje Martyna. Sprzężone przybliżone poczucie gdzie jest ta druga osoba.
* Xz: bardziej kosztowne na banki energetyczne Invictusa niż się wydawało. Uratuje się MNIEJ osób niż by się mogło ale i tak WIĘCEJ niż bez tego. W którymś momencie odcinamy.
* V: skutecznie jest zasilona póki to działa. Więcej uratujemy.
* X: ich leczenie bardziej długotrwałe a część rubinów wymaga wymiany; uszkodziły się. De facto po przekazaniu energii nie mogą jej wspierać. Czyli ma prawo być zła.
* V: nie drenujemy rannych dodatkowo. Co pokazuje, że tien Kopiec tak ustawiła rubiny, by NIE ZABIĆ PRZEZ PRZYPADEK swoich lojalistów.
* Vz: tien Kopiec jeszcze nie wie że to Martyn, ale jak się dowie to nie będzie już na niego patrzeć tak jak kiedyś. Jest godny jej uwagi. Proaktywny, ratuje, działa. Dorósł.

W całym tym chaosie komunikat od Pandemiusza - "grupa ochotników by wesprzeć tien Kopiec PLUS jeden medyk do ważnej operacji tymczasowej". Ręka Martyna idzie w górę pierwsza. Pandemiusz "ej Martyn jesteś szczur". Martyn dyskretnie "znam ją, mamy przeszłość. Model biznesowy, pogardziła ale nie zabiła. Plus, pomogłem jej trochę."

Kaliny nie było nigdzie w medbay. Martyn pomyślał "nie jest wojskowa". No ale Kopiec też nie. A ranni... krótkie rozmowy z nimi - wszyscy przeszli podstawowe przeszkolenie wojskowe (ostro przeciorani) ale to nie są żołnierze. To regularni poddani. To wygląda jak coś, co jest zbudowaną na gwałtu rety milicją obywatelską. Ale po ostrym treningu - atak, uderzenie. Partyzantka kosmiczna. Ale NORMALNE SIŁY KOPIEC.

Dla Martyna to była zagadka, ale to normalny sposób w jaki Eternia działa. Tu nie ma sekretu. (ta linijka jest napisana dla Fox ;p)

I zanim Martyn się oddalił, dostał jeszcze wyniki z komputera (interakcja jakichś _toxic agent_ i blood ruby). Martyn nie ma kompetencji; robił to science officer Invictusa; zainteresowało go to:

ExZ+3:

* X: dane będą spływały, ale Martyn już tam musi iść i nie może czekać. FORCE END CONFLICT po pierwszym krzyżyku.
* V: różne rzeczy się pojawiały, ale co zwróciło uwagę science officera - powtarzające historie o różnych dźwiękach przewijających się przez korytarze, z głośników itp a część dźwięków -> uczucie bólu. To uczucie częściowo się pokrywa z ludźmi których blood ruby się popsuły przez przesterowanie energią.

Martyn z oddziałkiem dotarł do miejsca gdzie znajduje się tien Jolanta Kopiec z sił Eterni. Dowodzi Szczepan Kutarcjusz. Martyn jest medykiem. Jesteśmy na wrogiej jednostce. Tien Kopiec powiedziała, że jej celem jest odzyskanie części swojego oddziału nadal na tej jednostce (na tej jednostce do których nie da się dostać) + złapanie kogoś by powiedział czemu mamy wojnę. Powiedziała, że AI tego statku jest dziwne. Nie poddało się specjalistom AI. Najpierw tak a potem kontrataki. Zupełnie jakby za AI stał jakiś żywy umysł.

Martyn poczekał aż kapral da mu prawo głosu i powiedział tien Kopiec o tym, jaka jest hipoteza - że fale soniczne jakoś uszkodziły rubiny. I po wspólnej analizie z nią doszli do tego, że fale nie są nośnikami magii - chodzi o częstotliwość soniczną uszkadzającą rubiny. Zbadał też samą Jolantę szybkim skanem magitechu - jest czysta. To potwierdza teorię broni sonicznej.

Jolanta przyzywa jednego ze swoich ludzi, rozrysowują tyle ile poznali tego statku - gdzie uderzały ich fale soniczne. Te fale się zmieniały, jakby coś szukało tej właściwej częstotliwości. Czyli nasza grupka (kapral etc) powinna być tymi co znajdą i unieszkodliwią broń soniczną.

TEST SZCZĘŚCIA VVX: po odesłaniu kilku osób z uszkodzonymi rubinami zostało kilkanaście osób w siłach kombinowanych Eternia - Orbiter. + potężna czarodziejka.

Martyn spytał Jolantę o to jak czuje się Kalina, jeśli może wiedzieć. Czy jest bezpieczna. Spojrzała zaskoczona + grim look. Jest uwięziona pod złomem. Jest ciężko ranna. Jolanta powiedziała, że nie zostawi jej żywej na statku wroga - nikogo z tej grupy osób uwięzionych. Jolanta powiedziała Martynowi, że jeśli ten złapie dla niej paru napastników to Jolanta pozwoli Kalinie odejść z Martynem jeśli Kalina będzie chciała. Martyn powiedział, że on teraz podczas wojny nie ma dla Kaliny NIC do zaoferowania. Więc jakkolwiek nie jest fanem tego jak działa Eternia to woli by Kalina była bezpieczniejsza i szczęśliwa. A z nim nie będzie. On jest tylko medykiem, szeregowym i po prostu Kalina w Eterni konkretnie pod Jolantą Kopiec będzie w dużo lepszym stanie.

Ta oferta Jolanty jest dla niej wiążąca. Ale Martyn musi OSOBIŚCIE złapać kilku przeciwników. Ale Martyn nie będzie skupiał się na swojej chwale by ON złapał a żeby ODDZIAŁ złapał jak najskuteczniej.

Martyn spytał dla upewnienia - czy spotkali jakiekolwiek osoby na tym statku? Tak.

Ta jednostka ma jakieś 200 x 60 x 40 metrów. Nieznany typ, wygląda "fregatowato". Gdyby dać komplement po statkach astoriańskich: 50-100 osób. Zgodnie z tym co mówi Jolanta to miała do 50 osób w oddziale. Duży oddział.

Martyn proponuje, że rzuci zaklęcie. Jolanta będzie _conduit_. Źródłem energii będą banki Invictusa. Transfer idzie po krwawych rubinach przez Jolantę. Zakotwiczy się czar w baterii Martyna. Cel - niech na przestrzeni 30 metrów promień od owego banku widzą "żywych ludzi". Wallhack. Sposób - jest medykiem, wie czego szukać i jak. Zna czary wykrywające "czy ktoś żyje pod gruzami" - to dokładnie taki czar, ale nieco zmodyfikowany. Czyli TYPOWY CZAR MEDYCZNY. Kapral i Jolanta zaakceptowali pomysł.

Jolanta zaproponowała by umieścić czar na simulacrum i wypuścić simulacrum na rajd po statku. To bardziej niebezpieczne, też dla niej. Ale szybciej zamapuje statek, znajdzie co jest grane --> większa szansa że ranni przetrwają i że kogoś złapiemy. Ważne że gdyby nie Martyn to Jolanta nie miałaby jak odpalić simulacrum - była wydrenowana, bo dostali się na statek przez spacer kosmiczny a simulacrum jako decoy. Kapral docenił pomysł Jolanty. Martyn wobec tego nie protestował.

ExZM+4:

* Vz: skuteczny czar wykrywający osadzony na simulacrum
* V: nie zakłóca Jolancie simulacrum ANI czerpania energii z rubinów

Póki Martyn trzyma ten czar, nie może zrobić kolejnego. Ok, drenaż energii z Invictus jest wyraźny, więc lepiej się spieszyć.

Kapral z pomocą Jolanty opracowali następujący plan działania (czyli Żółw ale Martyn nie może):

* Simulacrum jest ogólnie cholernie trudne do wykrycia przez sensory ciepła itp. Widać je fizycznie, czujnik ruchu złapie. No i świeci magicznie jak pochodnia.
* Simulacrum przedostaje się szybko robiąc sweep niedaleko gruzowisk by złapać je "przez ściany". By poczuć czy gdzieś tam zmierzają siły wrogów.
* Jeśli tak, to robimy manewr: kapral idzie szpicą w tamtym kierunku ostro a simulacrum pozycjonuje się na odcięcie drogi + "wizję" na wrogów.
* Jeśli wrogowie będą chcieli bronić się przed naszym zergiem to simulacrum atakuje od tyłu i demoralizuje. 
    * Else, jak próbują uciec - simulacrum ich "wyłapie". A w innym wypadku - nasi mają thermal shock launchery.
* Simulacrum najskuteczniejsze jest w DEMORALIZACJI. Więc niech ich zdemoralizuje by ich złamać - niech będą jak kaczki dla naszych thermal shocków.
* A mamy ogromną przewagę - my ich DOKŁADNIE widzimy a oni o tym nie wiedzą. A oni wiedzą, że nas widzą i my o tym wiemy. Więc można robić dywersje i feinty.
* Jak są soniki na ścianach, nasi rozwalają. Nie mamy rubinów a oni o tym nie wiedzą.

TrZ+3 na całość + eskalacje:

* X: Tak, oni CHCĄ dorwać paru astorian. Jolanta, z żalem, paru swoich musiała zabić. Alternatywą było pozwolić ich złapać. Bo nie dojdziemy do wszystkich.
* V: Simulacrum odkryło grupę. Ta grupa idzie na Kalinę.
* X: Są daleko. Naprawdę daleko. Trzeba iść szybciej niż to sensowne i komfortowe i bezpieczne.
* XX: Dostaliśmy dziwny sygnał - i niestety simulacrum weszło na bardzo niebezpieczny teren. Ultra chroniony. Tam COŚ JEST KRYTYCZNEGO (BIA). Simulacrum zostało zniszczone przez autodziałka; Jolanta zdołała z niego się wycofać. Szarpnęło to Jolantą.

Nie mamy tam już oczu. Mamy w miarę świeżą mapę terenu i wiemy ilu ich było - sizable force. 7 osób, potężnie uzbrojonych + autosystemy. Plus ten "dziwny teren" - jest nieprawdopodobnie chroniony, ale tam szły wszystkie systemy i metody które działały na oddział Jolanty.

Kolejny plan. Ważna uwaga - większość sprzętu skupiona jest przeciwko Saitaerowi. To nie jest tak, że my mamy Lancery itp. Nawet odwołanie Invictusa było trudną decyzją. Tak więc mamy grupę żołnierzy w lekkich pancerzach a nie servarach. Mamy eternijskią milicję. Wyszkoloną, ale. Wrogowie mają wojsko. Na szczęście nie wszyscy mają servary.

Ale mamy _blaster launcher_. Więc kapral ma plan (czyli Żółw):

* Żołnierze rozchodzą się w czwórkach. Jedna konkretna czwórka to Martyn + 2*thermal shock + anti-tank.
* Dzięki Simulacrum wiemy, gdzie się rozejść by nikogo nie znaleźć. Ale Martyn ma iść w kierunku na Kalinę.
* Gdy Martyn jest blisko, czwórka blisko niebezpiecznego miejsca (tam gdzie BIA) ma odpalić blaster launcher.
* Szok wywołany entropiczną bronią powinien zniszczyć działka i wstrząsnąć przeciwnikami (BIA też mogłaby spanikować, nie? XD)
* W tym czasie czwórki natychmiast wracają tam gdzie jest blaster by się zgrupować i postraszyć. A Martyn z ekipą atakują tamtą siódemkę którzy powinni być wstrząśnięci.
* To powinno sprawić, że ci którzy skupiali się na naszych jeńcach skupią się na obronie bazy. Kupimy czas by uratować swoich.

Chcemy: spłoszyć BIA by przekierowała siły do obrony siebie, skonfundować tych co zbierają uwięzionych i być może odciągnąć, zapewnić trochę spokoju na terenie gdzie jesteśmy.

TrZ+3:

* Vz: plan prawidłowy, szybka adaptacja, po wykonaniu planu faktycznie większość sił bieży do BII na ratunek.
* V: ci którzy mieli zdobyć Kalinę i grupę gdzie jest Kalina przerywają operację i lecą w kierunku na BIA nieświadomi czwórki z Martynem
* X: nie ma w pełni ufortyfikowanej pozycji; to będzie ostry firefight ALBO kapral odda pole.
* V: BIA skupia się na przetrwaniu, regeneracji, koordynacji, defensywach - odwrócona uwaga dowódcy który próbuje zapobiegać stratom. 
* X: automatyczne działka shredowały jedną z czwórek. Po prostu trafili na fragment statku który był śmiertelnie niebezpieczny...
* V: wrogowie są ZDEMORALIZOWANI. Niekoniecznie chcą walczyć - chcą przetrwać. Nie rozumieją co się dzieje. Możemy uratować wszystkich zakopanych.
* V: jeżeli osiągniemy jeszcze kilka porządnych sukcesów, otwiera się opcja że wrogowie się poddadzą jeśli dostaną gwarancję traktowania jako jeńcy wojenni bo nie można walczyć z siłami specjalnymi ;-).

Czas, by ekipa z Martynem poradziła sobie z biegnącą siódemką. Siódemka nie jest świadoma dokładnej lokalizacji czwórki Martyna, nie wiedzą że Martyn wie. Chcą ich ominąć i lecieć do BII. Plan zespołu z Martynem - osaczyć z zaskoczenia, 2 * thermal shock, anti-tank w ewentualny servar, Martyn dobija magią anestezjologiczną jak będzie trzeba. Wzmocnienie reakcji nic nie da - thermal shock jest AoE, reakcje nie pomogą jeśli servar za ciężki. Wtedy tylko magia może pomóc.

TrZ+3:

* Vz: thermal shocki rozwiążą problem wszystkich "lekkich" jednostek. Mamy 4 wyłączonych z akcji.
* X: mają cholerny ciężki servar. Oczywiście że mają.
* V: antitank wyłączył lżejsze servary z zaskoczenia. Thermal shock działa.
* V: PEŁNE ZASKOCZENIE. Martyn ma swobodny czar i mamy dobrą pozycję. Przeciwnik w ciężkim servarze na moment spanikował.
* (+M): Vm: Martyn całkowicie unieczynnił pilota ciężkiego servara używając magii anestetycznej. Szef ochrony tej jednostki.

Zaraportowane do Jolanty. Ona natychmiast się tu pojawia, chroniona przez czterech prywatnych ochroniarzy (silnie zmodyfikowanych) - resztę zostawiła kapralowi do dowodzenia. Jolanta NATYCHMIAST zażądała, by odblokowali ten servar. Jedyne ograniczenie - koleś ma przeżyć, nie musi być zdrowy. Siłowo otworzyli servar; Jolanta natychmiast zaczęła operację "blood ruby slave link" (ale wpierw upewniła się na szybko że Kalina + 2 inne osoby w gruzie są OK). Martyn patrzy na to z przerażeniem, czystym. Ale nic nie może zrobić ani powiedzieć - ważniejsi są jego ludzie. Skupia się na złomie - trzeba jak najszybciej pomóc wszystkim...

Grupka Martyna wydobywa "swoich" spod złomu. To troszkę trwa.

TYMCZASEM RZUT NA SZCZĘŚCIE (bitwa kosmiczna Invictus - nieznany statek noktiański): 6v6 -> XXX

* X: Invictus jest bardzo ciężko uszkodzony. Nie nadaje się do walki. Musiał uciekać.
* X: Jesteśmy na pokładzie nieznanej jednostki sami. Nie mamy żadnych zasobów. TO ZNACZY ŻE CHAIN OF COMMAND: Jolanta - kapral - Martyn...
* X: Mają wyższe morale. Trudniej ich zdemoralizować.

TYMCZASEM KAPRAL. Już się zorientował, że ma do czynienia z AI Core. A Jolanta wcześniej mówiła, że to AI jest dziwne. No i Martyn je wykrył. Detektorem biologicznym. Więc kapral kazał użyć ostatni pocisk blastera by strzelić prosto w rdzeń AI Core. Jeśli statek nie ma AI, nie poleci i nie będzie w stanie użyć autodziałek, śledzić itp. Mają większe szanse. Zwłaszcza że Jolanta powiedziała że ma plan. On nie wie jaki. (kogo złapaliśmy żywcem - dodać do naszej armii. Ma ograniczoną ilość rubinów, ale nie taką małą...)

Zniszczenie BII. Nie wiedząc co to jest. Jednym pociskiem. Z niekompletnie wyłączonymi defensywami. Z aktywnym sarkofagiem. ALE - nie trzeba w pełni zabić, wystarczy "odciąć" lub "uszkodzić". Jest bezpieczny strzał z namierzaniem. Jest stary sygnał Martyna. A ten się nie rusza ;-).

ExZ+2:

* V: BIA zniszczona przez blaster launcher mimo przeszkód.
* X: Blaster nie wytrzymał i eksplodował, zabijając operatora.
* X: Grupa z blasterem nie zdążyła się wycofać. Nadchodzący oddział ich dorwał i powystrzelał.
* V: DEMORALIZACJA. BIA jest zniszczona. Są ślepi. Systemy awaryjne wymagają pójścia i uruchomienia - a teraz nie wiadomo gdzie można i czy nie będzie drugiego simulacrum.

Czyli kapral stracił już 9 osób. Zostało mu 7 Jolanty, 1 swój, Martyn, Jolanta i jej 4 ochroniarze. Ranni, na szczęście, zostali wycofani na Invictus (gdzie część zginęła gdy Invictus płonął >.>).

Kapral regrupuje się do Jolanty i Martyna. A tymczasem udało się wykopać Kalinę i dwie inne osoby.

Jolanta dołączy CAŁY oddział który był wyłączony do swojej grupy bojowej. To zajmie jej "moment", ale to zrobi. A very crude operation - mimo wszystko kilka godzin. Ale przeciwnik jest w szoku, nasze siły też są w szoku.

Sytuacja jest bardzo trudna - nie możemy polegać na tym, że się wycofamy. MUSIMY NOKTIAN ZŁAMAĆ.

Martyn zaproponował Jolancie - niech czerpie z niego energię. Musi móc stworzyć kolejne simulacrum. Teraz - noktianie widzieli simulacrum na zewnątrz swojego statku - i "zniszczyli je". Potem widzieli simulacrum niedaleko BIA - i "zniszczyli je". Jeśli znowu pojawi się simulacrum, zabije 1-2 żołnierzy i się schowa (rozproszy), to powinno ich naprawdę odstraszyć od przyjścia w tą stronę. Będą chcieli wpierw upewnić się że wszystko jest OK. Co kupuje nam czas.

Jolanta stwierdziła, że jest to normalnie niemożliwe. Ale jakoś go "czuje". Martyn wyjaśnił co zrobił (poprzedni O).

ExMZ+4+3 extra Or; Or idzie w jej umiejętność opanowania się a Martyn to zasób:

* X: Jolanta MUSI je rozproszyć. Nie może kontynuować - energia płynie z Martyna i TYLKO z Martyna. To go zabije szybko.
* Or: Jolanta pociągnęła więcej niż powinna i niż by to zrobiła. Martyn do końca sesji nie czaruje a po tej akcji potrzebuje pomocy maga. (ale SUKCES)
* V: Simulacrum zrobiło co miało - wpadło gdzieś, złapało 2-3 osoby, okrutnie eksterminowało i się schowało. Po czym Jolanta je rozproszyła by nie zabić Martyna. DEMORALIZACJA.
* V: Zaczerpnięcie energii z Martyna zregenerowało Jolantę. Jej szacunek dla Martyna wzrósł. Nieustraszony wojownik w skórze medyka.

Martyn, ledwo aktywny ale na ostrych stymulantach skupił się na pomocy rannym, w tym Kalinie. Nadal nieaktywni, ale stabilni.

WIEMY, że noktianie nie chcą się tu poruszać jak gdzieś tu poluje simulacrum. To, że go nie ma, nie ma znaczenia - noktianie nie wiedzą. Tak więc oni chcą się ufortyfikować. Więc kapral przeprowadził siły do jednego z fortyfikowalnych pomieszczeń o niskim znaczeniu strategicznym dla statku. Żeby nie było oczywiste że tam są siły + by nie kusiło noktian. Plus, niedaleko hangaru. I teraz kapral obsadził warty i patrole - chodzi tylko o to, by przeczekać kilka godzin aż Jolanta skończy pracę nad szefem ochrony by go sprzęc z rubinem i dołączyć do swojej świty.

Jolanta musi dodać kogoś wbrew woli do świty.

Martyn jest półprzytomny, bo by musiał ingerować. Nie działa już anestetyk-czar, bo Martyn puścił. Jolanta poprosiła Martyna o inny anestetyk. Martyn go dał, po czym się położył. W tym stanie więcej szkody zrobi pacjentom niż pożytku. Coś jeszcze pamięta i kontaktuje, ale... niewiele.

Dodanie kogoś na siłę jest operacją okrutną. Bolesną. Zwykle tak tego się nie robi, nie w warunkach bojowych. To kwestia tortur i łamania. Jolanta robi to "na szybko" i wobec kaprala + drugiego żołnierza dla których to jest koszmar. Dla Martyna też by był, ale dzięki sprzężeniu rozumie co ona robi i że nie chce. Że jej się to bardzo nie podoba. Plus jego stan nie pozwala mu protestować.

Problem - czy Jolanta da radę dodać szefa ochrony do świty zanim noktianie się zorganizują. Potrzebujemy VVV. Więc Tr.

Tr+4:

* V: She is ruthless. She broke him. Nie wiedziała nawet, że potrafi to zrobić tak skutecznie i tak okrutnie. Pomogły wizje tego co działy się z jej ludźmi tutaj i jak ginęli. A czuła każdą śmierć.
* V: Jakkolwiek kapral i żołnierz byli przerażeni, nienawidzili tego, ale po tym co się tu stało zaakceptowali to. Nie ma w nich nienawiści do Jolanty ani Eterni. "Tak powinno być". "Oni nas zaatakowali".
* V: Wrogowie woleli najpierw przygotować siły, broń, zrozumieć sytuację - zanim odważyli się coś zrobić. Było za późno. Zwłaszcza z martwym statkiem.

Dla noktian OGROMNYM szokiem było gdy po kilku godzinach połączył się z nimi ich własny szef ochrony statku. Powiedział, że teraz służy Eterni. Że zna wszystkie sekrety statku. Że ma ciężki servar. Że nie chce, ale jak go zmuszą (noktianie) to ich zabije. Wie o każdym systemie, każdej strategii - sam je zaplanował. I jeśli noktianie się poddadzą, będą traktowani jako jeńcy wojenni. Jeśli nie - skończą jak on. Powiedział, że został złamany i przebudowany. Nie ma dla niego powrotu. I mówi do nich bez koszuli - widać krwawy rubin. Powiedział też, że niedaleko jest koloidowy statek sił specjalnych. Widzieli, z czym walczą. Czy wolą niewolę czy poddanie się i obóz dla jeńców wojennych.

TrZ+4:

* Xz: Niektórzy postanowili, że uratują swojego szefa ochrony. Jolanta kazała JEMU pójść i ich zabić. ON ich zabił. Mówiąc każdemu z imienia kogo zabija i płacząc.
* X: To złamało kaprala i żołnierza. Eternia to jednak zło. Mają koszmary. NIGDY nie będą wspierać Eterni i żołnierzy Eterni.
* VVV: statek się poddał. Wezwano Invictus. Invictus doczołgał się bardzo zdziwiony WTF just happened...

DWA DNI PÓŹNIEJ.

(Gdyby nie to sprzężenie, Martyn NIGDY by się do Jolanty nie odezwał. Tak wiedział że nienawidziła każdego sekundy tego co robi + wiedział, że płaci zdrowiem permanentnie za to co robi.)

Martyn w szpitalu. Kalina z nim w szpitalu. Jolanta obiecała. Jest zadowolona z tego co Martyn zrobił. Martyn podtrzymuje - Kalina jest z Eterni. Eternia jest teraz szczególnie bardzo niepopularna mimo że bez Jolanty wszyscy by zginęli (tu Jolanta tylko się uśmiechnęła). On NIE CHCE zabierać Kaliny gdzieś gdzie Kalina będzie źle traktowana i nie ma przyszłości. Jednak Kalina mimo swoich ran wyczuła Martyna jak była pod gruzami (tamten 'O' połączył go z siecią której rootem jest Jolanta). PRZYBYŁ PO NIĄ. Kalina jest skłonna spróbować. CHCE iść z Martynem.

Jolanta powiedziała Martynowi, że to jest do załatwienia by Kalina i Martyn byli na tej samej jednostce. Powie do dowództwa, że ją uwolni TYLKO jeśli ona będzie ZAWSZE przy Martynie do końca wojny. Plus Jolanta dalej pomoże w wojnie. Jeśli jednak dowództwo odmówi, Jolanta może mieć wątpliwości czy pomóc.

Dowództwo się oczywiście zgodziło. Kalina dołączyła do Martyna.

A Martyn dostał swój pierwszy medal...

* (ten statek uczestniczył w koordynacji zrzucania Finis Vitae na Astorię - dlatego był dziwny, miał zaawansowane defensywy WEWNĄTRZ, by Finis Vitae się nie rozlazł jakby coś poszło nie tak i był samotny - jedna z trzech tego typu jednostek; już był po akcji, więc miał skeleton defenses)
* (pierwsza grupa do Trzeciego Raju?)
* (tak, kapral był JEDYNYM członkiem sił specjalnych Orbitera na pokładzie)

## Streszczenie

Tien Kopiec zaatakowała dziwną noktiańską jednostkę i została odparta. Wezwała na pomoc OO Invictus. Po otrzymaniu wsparcia mieli się tylko wycofać i zdobyć kilku jeńców, ale okazało się to dużo trudniejsze - Invictus nie miał szans z noktiańskim statkiem i musiał uciekać. Kopiec + Kutarcjusz + Hiwasser dali radę sterroryzować załogę noktiańskiej jednostki. Tamci się poddali. Ogromne straty po wszystkich stronach. Ale - na plus - Martyn dostał uratowaną Kalinę od Jolanty, co zniszczy mu karierę w Orbiterze XD.

## Progresja

* Martyn Hiwasser: w oczach tien Jolanty Kopiec z Eterni jest proaktywny, ratuje, działa. Dorósł. Ryzykowny, ale ma dobre pomysły i pamięta o innych. Lubi go.
* Martyn Hiwasser: przez moment był w sieci krwawych rubinów Eterni. ROZUMIE jak to działa. Rozumie Eternię. Już nie myśli o nich jako o "złe wampiry".
* Martyn Hiwasser: MEDAL! Za zdobycie noktiańskiego okrętu, odwagę i poświęcenie w kryzysowej sytuacji. Medal symbolizuje, że bez niego inaczej by się potoczyło.
* Martyn Hiwasser: od Jolanty Kopiec dostał Kalinę w prezencie. Tzn, Kalina jest wolna, ale jest "z nim" i "jego". Co strasznie utrudnia mu życie na jednostce wojskowej Orbitera.
* Jolanta Kopiec: DRAMATYCZNY skok do góry w hierarchii Eterni - nie tylko implementacja krwawego rubinu "na miejscu" ale też użycie maga Orbitera plus ogromny sukces i bezwzględność.
* Jolanta Kopiec: po tym jak dostała energię z banku Invictusa i zaczerpnęła z Martyna + kryzysowe sytuacje jej moc trwale wzrosła. Nie "bardzo", ale jednak. Przesunęła swoją granicę.
* Jolanta Kopiec: po forsownym dodaniu przez arkin szefa ochrony statku noktiańskiego do jej sieci, Zwycięstwo Esuriit przesunęło się u niej o co najmniej 10 lat... zapłaciła za to strasznie.
* Kalina Rota d'Kopiec: nie jest odpięta z sieci Jolanty, ale jest wolna. Partnerka Martyna Hiwassera.
* Szczepan Kutarcjusz: MEDAL! Za zdobycie noktiańskiego okrętu, odwagę i działania taktyczne w obliczu przeważających sił wroga w skrajnej sytuacji. I awans - jest sierżantem po tej akcji.

### Frakcji

* Sojusz Obrony Astorii: dowiedzieli się o istnieniu Finis Vitae i dlaczego jest zrzucony. Przed czasem. Dzięki temu mogli zadziałać w kierunku na Cieniaszczyt.

## Zasługi

* Martyn Hiwasser: 25 lat. Medyk na OO Invictus. Zbudował magicznie detekcję gdzie są wrogowie na wrogiej jednostce ("czy ktoś żyje pod gruzami"), wzmocnił tien Kopiec przez arkiny i oddał jej swoją energię by sformowała simulacrum. Ale uratował Kalinę, ma szacunek Jolanty i wyłączył delikwenta w ciężkim serwarze magią anestezjologiczną. Ma Kalinę od Jolanty w prezencie XD.
* Pandemiusz Diakon: przełożony Martyna na OO Invictus. Dobry lekarz ufający swojej załodze, aprobował pomysły Martyna by wesprzeć Jolantę Kopiec i wysłał go na akcję jako ochotnika mimo, że powinien wysłać kogoś bardziej doświadczonego. Jego przeczucia zwróciły się pozytywnie.
* Jonasz Staran: dowódca OO Invictus. Mając kiepską jednostkę świetnie wymanewrował tajemniczy okręt noktiański i pozwolił siłom Sojuszu wesprzeć tien Kopiec. W walce poniósł straszne uszkodzenia i musiał się ewakuować; ale docelowo przejął tę jednostkę noktiańską.
* Jolanta Kopiec: wraz z milicją d'Kopiec zaatakowała dziwną jednostkę noktiańską by dowiedzieć się o co chodzi w tej wojnie i jak została odparta wezwała wsparcie. Zdalnie wzmocniona przez Martyna, była kluczowym elementem dzięki któremu zdobyli tą jednostkę - jej simulacrum było narzędziem terroru a arkin założony na siłę szefowi ochrony noktiańskiej jednostki jakkolwiek pozwolił im przejąć okręt czystą demoralizacją, ale ONA strasznie zapłaciła (Esuriit) i Eternia ma opinię "tych złych i strasznych" na Astorii.
* Szczepan Kutarcjusz: kapral sił specjalnych dowodzący oddziałkiem ochotników pomagających Jolancie Kopiec. Wszedł w tą akcję, bo chciał poznać Eternię. Widząc jak straszna jest eternijska tienka, już nie chce mieć z nimi nic wspólnego. Świetny taktyk, doprowadził do przejęcia dziwnej noktiańskiej jednostki przez grupę kompetentnej ale jednak milicji + jeden emiter anihilacji (blaster launcher).
* Kalina Rota d'Kopiec: uczestniczyła w akcji tien Kopiec - atak na dziwną jednostkę noktiańską i została odcięta, przysypana i ranna. Martyn przyszedł jej na pomoc. Jolanta spytała ją czy chce iść z Martynem - Kalina powiedziała TAK. Więc jest wolna (acz w arkin Jolanty).
* OO Invictus: patchworkowy transportowiec noktiański, zarekwirowany przez siły Orbitera i obsadzony na szybko jako QShip. Skierowany do misji wsparcia tien Jolanty Kopiec z tajemniczej noktiańskiej jednostki. Świetne podejście - wyłączył wroga, ale strasznie zdewastowany. Różnica klas.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański

## Czas

* Chronologia: Inwazja Noctis
* Opóźnienie: 67
* Dni: 5

## Inne

.

## Konflikty

* 1 - Czy OO Invictus wejdzie czysto koło tajemniczej jednostki noktiańskiej?
    * TEST SZCZĘŚCIA - 6v6
    * VVV: minor damage, boarding party ogromny sukces - wdarli się, odzyskali sojuszników. Okręt noktiański tymczasowo unieczynniony.
* 2 - Martyn jest tam pomocny, ale to katalista robi wszystko (prawidłowo). Cel: zasilić tien Kopiec przez Blood Ruby przez banki energii z medbay na Invictus.
    * TrMZ+3
    * O: tymczasowy link Martyna i tien Kopiec (emocjonalny)
    * XzVXVVz: uszkodzenie części arkinów, uratuje się mniej osób niż by mogło, ale Kopiec zregenerowana, Martyn godny jej uwagi i jest zasilona - więcej uratujemy.
* 3 - Wyniki z komputera (interakcja jakichś _toxic agent_ i blood ruby). Martyn nie ma kompetencji; robił to science officer Invictusa; zainteresowało go to
    * ExZ+3:
    * XV: czas na jedną odpowiedź; arkiny są niszczone przez ataki soniczne
* 4 - Martyn czaruje. Detekcja "czy ktoś żyje pod gruzami" - wallhack. Niech na przestrzeni 30 metrów promień od owego banku widzą "żywych ludzi". Osadzone w simulacrum Jolanty.
    * ExZM+4
    * VzV: skuteczny czar osadzony na simulacrum i nie zakłóca interakcji Jolanta - arkiny i Jolanta - simulacrum.
* 5 - Kapral z pomocą Jolanty opracowali plan działania: ściągnąć wrogów przez terror simulacrum na grupę bojową i ich rozsiekać by odblokować sojuszników.
    * TrZ+3
    * XVXXX: Jolanta musiała kilku swoich zabić, simulacrum ma grupę -> Kalinę i mamy wizję terenu, simulacrum natknęło się na superfortyfikowaną BIA i nie przetrwało.
* 6 - Kapral ma kolejny plan - spłoszyć BIA by przekierowała siły do obrony siebie, skonfundować tych co zbierają uwięzionych i być może odciągnąć, zapewnić trochę spokoju na terenie gdzie jesteśmy. Do tego emiter anihilacji.
    * TrZ+3
    * VzVXVXVV: wrogowie nie mają w głowie jeńców, Martyn ma przewagę (zaszokowani wrogowie), nie da się ufortyfikować pola + autodziałka shreddowały 4 osoby, wrogowie zdemoralizowani, otwiera się opcja poddania statku
* 7 - Plan zespołu z Martynem - osaczyć z zaskoczenia, 2 * thermal shock, anti-tank w ewentualny servar, Martyn dobija magią anestezjologiczną jak będzie trzeba.
    * TrZ+3
    * VzXVV: thermal shock zdjęło 4, antitank + thshock 2, PEŁNE ZASKOCZENIE. Ale mają ciężki servar.
    * (+M) Vm: Martyn magią anestezjologiczną unieczynnił pilota. To szef ochrony tego statku.
* 8 - Jak toczy się bitwa kosmiczna między Invictus i tą jednostką?
    * RZUT NA SZCZĘŚCIE 6v6
    * XXX: Invictus bardzo ciężko uszkodzony, oderwał się. Jesteśmy sami. + wrogowie mają podniesione morale. Nie mamy jak się wycofać.
* 9 - Kapral zorientował się w problemie (i że mają AI Core). Ostatni pocisk blastera by strzelić prosto w rdzeń AI Core. Większe szanse.
    * ExZ+2
    * VXXV: BIA zniszczona, blaster eksplodował i zabił operatora + wrogowie wycięli tych z blasterem. Ale silna DEMORALIZACJA. Wrogowie są ślepi i nie mają pełnej kontroli nad statkiem
* 10 - Martyn zaproponował Jolancie - niech czerpie z niego energię. Musi móc stworzyć kolejne simulacrum. Jeśli znowu pojawi się simulacrum, zabije 1-2 żołnierzy i się schowa (rozproszy), to powinno ich naprawdę odstraszyć od przyjścia w tą stronę. Demoralizacja.
    * ExMZ+4+3 extra Or; Or idzie w jej umiejętność opanowania się a Martyn to zasób
    * XOrVV: Jolanta rozprasza simulacrum asap po akcji, pociągnęła z Martyna i on jest półprzytomny do końca sesji, simulacrum zrobiło co miało - 2-3 osoby eksterminacja wrogowie nie wiedzą czy jest czy nie, Jolanta zregenerowana. Szacunek do Martyna++.
* 11 - Problem - czy Jolanta da radę dodać szefa ochrony do świty zanim noktianie się zorganizują.
    * Tr+4
    * VVV: she broke him - jest okrutna, kapral i żołnierz nie nienawidzą jej ani Eterni, wrogowie woleli przygotować siły i nie zdążyli. Szef ochrony jest częścią arkin Jolanty.
* 12 - Szef ochrony statku w arkin Jolanty chce ich złamać by poddali jednostkę.
    * TrZ+4
    * XzXVVV: niektórzy chcieli uratować szefa ochrony i Jolanta kazała mu ich zniszczyć, to złamało kaprala i żołnierza - mają koszmary przed Eternią, ale statek się poddał i Invictus przejął jednostkę noktiańską (że co?!)

---
layout: cybermagic-konspekt
title: "Samotna w programie Advancer"
threads: historia-eleny
gm: żółw
players: anadia
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [210324 - Lustrzane odbicie Eleny](210324-lustrzane-odbicie-eleny)

### Chronologiczna

* [210324 - Lustrzane odbicie Eleny](210324-lustrzane-odbicie-eleny)

## Plan sesji
### Theme & Vision

* Elena nie radzi sobie samotnie w kosmosie
    * Nienawiść Eleny do Blakenbauerów sprawia, że nie radzi sobie z kotami --> nie pełni roli advancera dobrze
    * Samotna Elena nie rozwiąże problemów, raczej w nie wpada
    * Czy Sandra pomoże Elenie czy ją odrzuci?

### Fiszki

* Hubert Mirsz: Orbiter trainee, (advancer-scout), ENCAO: -0-+- | fatalistyczny i ugodowy | pokorny -> szuka sojuszników
* Kacper Wentel: Orbiter trainee, (advancer-technik), ENCAO: +00-- | Namiętny i żarliwy | conformity -> tworzyć sprawny zespół; część grupy 
* Tymon Krakdacz: Orbiter trainee, (advancer-pilot), ENCAO: +0--- | Lekceważy sobie obowiązki, ryzykant | stimulation -> uznany za mistrza
* Lara Kiriczko: Orbiter trainee commander, (corporal), ENCAO: +0--0 | Arogancja i poczucie wyższości | universalism -> odbudowa i odnowa

### Co się wydarzyło TERAZ (what happened LATELY / NOW)

* Dwie advancerki: Elena i Sandra. Elena jest lepsza, ale nielubiana. Sandrę wszyscy kochają.
* Pięć osób w grupie treningowej.  
    * Sandra (advancer-medyk) 
    * Elena (advancer-pilot) 
    * Hubert Mirsz: (advancer-scout) ENCAO: -0-+- | fatalistyczny i ugodowy | pokorny -> szuka sojuszników
    * Kacper Wentel: (advancer-technik): ENCAO: +00-- | Namiętny i żarliwy | conformity -> tworzyć sprawny zespół; część grupy 
    * Tymon Krakdacz: (advancer-pilot): ENCAO: +0--- | Lekceważy sobie obowiązki, ryzykant | stimulation -> uznany za mistrza
* Dowódca: Lara Kiriczko (corporal): ENCAO: +0--0 | Arogancja i poczucie wyższości | universalism -> odbudowa i odnowa

### Co się stanie (what will happen)

* S00: Pojedynek advancerów między pułapkami. Elena nie ma problemów; omnidetekcja.
* S00: W sprawie źle wykonanej naprawy czegoś mało ważnego Elena idzie by the book (Kacper ma kłopot), Sandra pomaga.
* S01: Pacyfikatory na 'wraku treningowym' Karsztarin -> 

### Sukces graczy (when you win)

* Czy Sandra zwycięży sama? Czy pomoże Elenie? Czy ją wpakuje w kłopoty?

## Sesja właściwa
### Wydarzenia przedsesjowe

.

### Scena Zero - impl

Sandra - dobrze radziłaś sobie zawsze na ćwiczeniach. Byłaś jedną z lepszych advancerek. I w końcu Was pomieszali - trafiłaś pod dowódcę punktu. Wasz dowódca nazywa się Lara Kiriczko, jest kapralem. Sandra - urodziłaś się na K1. Jesteście na K1 i tam się szkolicie w odpowiednich sektorach, delegowane do tego są sektory 17 - 21.

Twoja drużyna jest zabawna: Sandra (medyk), Hubert (scout), Kacper (kocha 'trzy diamenty'), Tymon (ryzykant). I Elena, zawsze z boku.

### Sesja Właściwa - impl

Michał Krzeszczak (ENCAO: 0--+0, combat, Don't worry, be happy, Achievement -> "to JA mam rację")

Sandra, Hubert, Kacper, Tymon grają w 'Trzy Diamenty' wraz z trzema osobami z innego zespołu. Nikt nie oszukuje, gra się toczy fajnie. Wszyscy trochę podpijają, poukrywani w miejscu gdzie nie powinno ich być. Dyskretna impreza - bo nikt nie wie. I dyskusja zeszła na championów i walki.

Kacper narzeka na Elenę. Strasznie. "jaka ta laska ma kij w dupsku". Powiedział też (coś co się stało) - gdy on coś zrobił nie dość dobrze z jej testowym myśliwcem ona to zgłosiła pani kapral. NAKAPOWAŁA. I Kacper tu wylewa łzy. Hubert "ona jest lepsza od nas wszystkich". Wszyscy na niego.

Michał jest championem ich zespołu. Chciałby się z Eleną zmierzyć. Kacper - "jak dasz jej klapsa na tyłek to Ci zapłacę". Michał spojrzał na Sandrę niepewnie i jak ona to olała, Michał jest za.

Sandra - "jeśli on Cię umówi na ten pojedynek to się może okazać że Elena uzna że wyzwałeś ją od najgorszych a nie dlatego że chcesz się zmierzyć. Tak działa pośrednictwo Kacpra". Kacper rzucił taką zbolałą minę. Michał przetarł oczy "ja pierniczę". No ale jak mam do niej zagadać? Sandra "nie umiesz zagadać do dziewczyny?" Michał nieszczęśliwy.

Kacper -> Michał "zagadaj do Eleny": 

Tr+3+2O (walka):

* O: nastroje poszły wyżej. Pójdą pięści.

Michał "będziemy się bić, laska na bok", Kacper "boisz się że Sandra Cię zbije?"

Sandra "oki, inaczej - Michał, zapasy ze mną. Champion vs champion.". Ogólne zdziwienie wszystkich jak Sandra deeskaluje.

Tr Z (wtf + szok + ale jak to) +2:

* Vz: zdeeskalowane ALE Sandra musi się zapasować z Michałem
* X: Sandra musi ustawić pojedynek z Eleną.
* Xz: Przegrany w starciu Michał - Sandra ustawia pojedynek z Eleną i mamy starcie.
* X: Grupa przegrana w starciu M-S wraca w bieliźnie do pokoi.

Kacper, Hubert i Tymon są PRZEKONANI, że Sandra da radę widząc minę Michała. Ale Michał jest zdeterminowany bo nie chce wracać w bieliźnie. Sandra też.

Więc Sandra dyskretnie dosypuje coś do alkoholu i wypije przed walką z Michałem. Na szczęście. Żeby on został zatruty i walczył źle.

TrZ (nikt się nie spodziewa bo REPUTACJA Sandry) +2:

* V: nikt z przeciwników nie zauważył zatrucia
* Vz: zatrucie jest SKUTECZNE - Michał traci przewagę. Taki wzmacniacz alkholu (czyli był wstawiony mocniej)
* X: nie jest to natychmiastowe - Sandra musi opóźnić albo początek walki ma pecha
* V: trucizna jest NAPRAWDĘ skuteczna. Sandra ma sporą przewagę.

Michał staje w pozycji zapaśniczej, Sandra też gotowa. "Jeśli nie stawiasz oporu to będzie dobrze". Sandra się śmieje i Michał atakuje.

Ex+2:

* V: Michał jest piekielnie szybki i sprawny, ale Sandra gra na czas. Manewr, unik, NIE DAĆ SIĘ ZŁAPAĆ. Ogólne śmiechy. (+Vg)
* V: Sandra skutecznie uniknęła łapania. Pełna perfekcja manewru. Kupiła dość czasu i widzi, jak Michał zaczyna słabnąć.

Ex+2 -> TrZ (trucizna)+3 :

* X: Michał złapał Sandrę.
* Vz: Sandra się wyślizgnęła. I kopnęła Michała w tyłek by poleciał.
* X: Michał poleciał, ale walnął nogami i Sandra wylądowała na ziemi. Michał się na nią przetoczył.
* V: Sandra przetacza się i zaczyna dusić Michała. Normalnie nie miałaby szans, ale trucizna.
* X: Michał i Sandra się szamoczą, Michał złapał Sandrę za włosy i walnął twarzą o ziemię i przydusił kolanem na kręgosłup.
* V: Dzięki truciźnie Sandra szarpnęła, przewróciła Michała i ciach mocno w splot. Michał zzieleniał, rzygnął i stracił przytomność.

Kacper się cieszy i 100% radości - YEAH, mogłem robić zakłady. Hubert jest zaskoczony i "wow Sandra". Tymon "meh, wolę uczciwie" ale nic nie mówi.

Tego wieczora zespół Michała wraca w bieliźnie. Niosąc nieprzytomnego Michała. Kacper porobił zdjęcia. Sandra jest lekko pokiereszowana, ale szacun jest.

Następnego dnia Lara poinformowała Zespół, że czas na nowe zadanie. Zapoznać się z pacyfikatorami (kotami). Każdy advancer dostaje dwa pacyfikatory i ma się nimi zajmować.

* Kacper i Tymon "wtf mam się kotem zajmować".
* Hubert zajmuje się kotami.
* Dwa koty i Elena. Ani kroku. Ani gestu. Patrzy. Obrzydzenie..?
* Sandra ma kiciusie i jest szczęśliwa.

Kapral Lara zaczęła tłumaczyć "to jest dokument. Przeczytajcie go." 40 stron jak się zachowywać z pacyfikatorami, jak ich używać itp. Że służą do wykrywania anomalii. I będziecie mieć test wykorzystania pacyfikatorów jutro. Po czym odeszła. Elena dosypała kotom karmy i wody, po czym wyszła. Kacper -> Sandra "no, TEGO to ona nie zda".

HIDDEN MOVEMENT: Michał skomponował prośbę i podanie o bitwę do Eleny. Elena przeczytała, odpisała "nie" i nie myślała o tym więcej. Michał nie chciał eskalować bo dostał opierdol od swojego kaprala za przegraną z dziewczyną.

Kacper namawia Sandrę i ekipę do grania w "Trzy Diamenty". A Sandra zdecydowała się przeczytać instrukcję i osiągnąć maksymalny sukces. Plus jako medyk lepiej do tego podejść. Hubert i Sandra się przykładają do tematu. Elena przeczytała uważnie, ale nie dotyka kotów.

Kacper i Tymon idą grać w "Trzy Diamenty".

Sandra wchodzi tak dobrze jak może:

TrZ (czas + książka + robi tylko to) +2 [do pierwszego krzyżyka albo do V, VVV, VVVVV]:

* VzVzV: Sandra osiągnęła poziom kompetentny do ćwiczeń
* X: Sandra ryła po nocy
* Xz: Sandra strasznie niewyspana i obolała
* VrV: Sandra wzbudziła szacun u Lary i była najlepsza w zespole

Następny dzień. Zadanie jest proste - użyć Pacyfikatorów by na pewnym terenie znaleźć anomalie. Tymon zaczyna. Bierze koty, wysyła komendę, patrzy gdzie idą... ogólnie, znalazł, zajęło to jakieś 20 minut. Potem Elena. Elena wyszła z pacyfikatorami ale je zignorowała. Sama szybko biegnie po terenie i wyłapuje anomalie. 6 minut. Lara jest zaskoczona i wtf.

Hubert się postarał - 10 minut. A Sandra - 5 minut.

Lara pochwaliła Sandrę, "tak się powinno robić", wyraźnie się przyłożyła itp. Do Eleny "mogłabyś osiągnąć jej poziom." Elenie się aż zapaliły oczy ale nie powiedziała niczego. Tylko - poszła w swoją stronę. Kacper -> Elena "hej, kiedy będziesz się biła z Michałem?" Elena na to "nie będę."

* Lara -> Sandra: "Ty jesteś dziewczyną, Ty zintegruj Elenę z resztą zespołu."
* Sandra -> Lara: "Jakie mogę wykorzystać metody na kogoś kto nie chce się integrować z zespołem?"
* Lara -> Sandra: "Mam jej dać rozkaz by się z Tobą integrowała?"
* Sandra -> Lara: "Tak, poproszę."

Lara parsknęła śmiechem. Zrobi to.

WIECZOREM. Kajuta. Pukanie do kajuty Sandry. Elena. "Dostałam polecenie by się z tobą integrować. Zatem się integruję." Sandra powstrzymuje się od śmiechu. To najgłupsze co słyszała. Elena ma ponure spojrzenie osoby, która nie chce tu być. Elena jest wściekła.

* Sandra -> Elena: Nie lubisz ludzi, co?
* Elena -> Sandra: To nie prowadzi do niczego. Takie... bezsensowne siedzenie. Mogłybyśmy się uczyć. Albo walczyć. Albo skradać.
* Sandra -> Elena: Teraz uczymy się rozmowy która prowadzi do współpracy. Jak chcesz być w zespole skoro jesteś solo?
* Elena -> Sandra: Jak mamy coś zrobić, robię to razem. Jestem solo tylko w czasie wolnym.
* Sandra -> Elena: Nikt Cię nie zna, nie wiadomo co się spodziewać, nie wiadomo jak zareagujesz. Np. to z pacyfikatorach. Nikt nie zaufa że jak pójdziemy na wspólną akcję to nie odwalisz czegoś po swojemu i nie zostaniemy w czarnej dupie bo uważasz że coś lepiej będzie zrobione.
* Elena -> Sandra: Nikt przeze mnie nie ucierpi
* Sandra -> Elena: Nikt Ci nie zaufa, więc nikt nie pójdzie z TObą na akcję
* Elena -> Sandra: Sama rozwiążę wszystko co mam rozwiązać.
* Sandra -> Elena: A jaką dasz gwarancję że nikt nie ucierpi?
* Elena -> Sandra: Będę tak dobra, że nie będzie kłopotu.
* Sandra -> Elena: No ok, ale dzisiaj nie byłaś najlepsza.
* Elena -> Sandra: <zacisnęła dłonie w pięści> Nie byłam. Następnym razem będę. Muszę poszerzyć zmysły. Za mało ćwiczę.
* Sandra -> Elena: Czy Ty się boisz kotów?
* Elena -> Sandra: Nie. Nie boję się NICZEGO. <mówi prawdę o kotach, ale bardzo zacięta... sprężyna na maksimum>

Sandra zauważyła jeszcze jedną rzecz - Elena się makijażuje by się postarzeć.

Sandra próbuje delikatnie próbkować Elenę - kim ona jest, na czym jej zależy, czemu tak się odgradza.

Ex Z (rozkaz Lary) +4:

* X: Elena bardzo unika powiedzenia czegokolwiek komukolwiek. Ona chce być sama i nie chce dać się zbliżyć.
* V: Q: "Czemu otacza się takim murem?" A: "Elena szuka kompetencji i samotności, bo wierzy, że inni przy niej mogą tylko ucierpieć."
* Vz: 
    * Q: "Jak ją zintegrować?" A: "Pokazać jej, że inni mogą jej pomóc i że ona może innym pomóc. Nie integrować socjalnie a czynami."
    * Q: "Czemu Elena tu jest?" A: "Bo naprawdę zależy jej by chronić ludzi i pomagać innym."
    * --> Elena zwyczajnie nikogo nie zna na Orbiterze. Ona jest tu sama.
* V: 
    * Elena się mimochodem wygadała, że pochodzi z Aurum. I naprawdę nie znosi arystokracji Aurum.
    * Elena nie lubi pacyfikatorów. Nie lubi ŻADNYCH zwierząt. Bo ród Blakenbauerów.
    * Elena boi się zbliżyć do ludzi bo nie chce im zrobić krzywdy.
    * Elena uważa, że ma bardzo dużą moc magiczną. BARDZO dużą.
    * Elena NIE jest samotniczką z natury, tak po prostu wyszło
    * Elena uważa "rozkaz to rozkaz", więc się będzie integrować tak jak Sandra chce. Bo ona nie wie jak.

Elena nie ma pojęcia jakie to nietypowe, że jakaś laska z Aurum trafiła na advancerkę. Ktoś musi wiedzieć i ktoś musiał ją sponsorować. Ale kto i dlaczego - nie wiadomo. A Elena nawet o tym nie wie.

DWA DNI PÓŹNIEJ.

Pierwsza operacja Waszego zespołu. Misja symulowana:

* OO Karsztarin. To jest wrak szkoleniowy.
    * Gdzieś na pokładzie statku znajduje się ranny agent. Jest anomalny. (symulacja)
    * Na statku jest niesprawne TAI. Strzela od czasu do czasu do wszystkich.
* Zadaniem Zespołu jest: insercja (mimo działek Karsztarina), doprowadzenie medyka do rannego agenta i ewakuacja agenta.
* Misją dowodzi Kacper.

Kacper proponuje następujący plan:

* Elena bierze pintkę która jest transportowa. Tymon bierze pintkę dobrze manewrującą. Tymon ma odwracać uwagę działek, Elena zadokować.
    * WSZYSCY wiedzą, że Elena jest lepszym pilotem. Elena powinna odciągać ogień. Kacper nie ufa Elenie.
* Po zadokowaniu Kacper wyłączy działka. Sandra z pacyfikatorami znajdzie i zlokalizuje agenta. Kacper, Tymon i Hubert czyszczą Sandrze drogę i ją osłaniają. Elena ma chronić statek medyczny.

Elena próbowała proponować. Kacper "szeregowa Mirokin, JA dowodzę operacją." Elena jest skonfliktowana - z jednej strony wie co i jak i jest w stanie zaplanować. Z drugiej - rozkaz to rozkaz. Elena jeszcze raz próbuje: opracowała zupełnie inny plan. Ona odwraca uwagę a potem łączy Kacpra z AI Core (bo ma możliwości szersze). Kacper blokuje.

Sandra -> Kacper: możemy ją sprawdzić, przetestować. Co może i czego nie może. Nauczy się dzielić robotą.
Kacper -> Sandra: po co ją chronisz? Jest bezużyteczna.
Sandra -> Kacper: nie wiesz, nie poznałeś jej. Jest magiem, znalazła anomalie bez użycia kotów...
Kacper -> Sandra: zrobi coś nietypowego, dziwnego, spali akcję. Wolę iść w czwórkę niż na niej polegać /pogarda

Kacper chce wyprowadzić Elenę z równowagi. Chce ją zdecentrować. Ma dość tej królowej lodu.

Tr Z (Elena jest łatwa do wyprowadzenia) +3 +3Om:

* V: Elenę faktycznie to zraniło. Elena się obróciła i chce wyjść z sali.
* Vz: "Szeregowa, zostań". Elena się zatrzymała i Sandra widzi - między łzami a wybuchem. A Kacper kontynuuje do Sandry, nie do Eleny.
    * K: Nic nie zrobiła, tylko kapowała na innych członków zespołu, bez niej byłoby nam lepiej.
    * H: Misja jest skalibrowana na pięć osób
    * K: Ale ona jest... wolę cholerny automat! To nie jest... ona nie jest jedną z nas.
    * S: musimy się stać drużyną, nawet rozkazy to mówiły, to nie tajemnica. Drużyna teraz czy w prawdziwej sytuacji?
    * K: No ale czemu gdy ja dowodzę? Ona jest SZKODĄ!
    * S: czyli wolisz przegrać w czwórkę niż wygrać w piątkę z lekką modyfikacją planów? I tak zrzucisz na nią bo nie możesz na niej polegać. Dowódca powinien pomyśleć o lepszych alternatywnych rozwiązaniach, zwłaszcza na treningu...
* VzVO: moc Eleny zamanifestowała. Komputer z Kacprem eksplodował. Moc magiczna, wir ze szrapnelem. Ona wykrzyczała.
    * "JA bezużyteczna? A co TY zrobiłeś? A co WY WSZYSCY robicie? Marnujecie czas, nie pomagacie, nie skupiacie się..."
    * energia Esuriit. Silne ryzyko.
    * WSZYSCY poza Eleną skończą ranni. A pokój zdewastowany.
    * "jesteście tak bezwartościowi jak Aurum, nie jesteście godni być agentami Orbitera"

Wir zniszczenia najmniej targetuje Sandrę. NIKT nie jest w stanie nawet zbliżyć się do Eleny poza Sandrą. Jej moc jest niesamowita. Energia unika Sandry, acz też zdziera z niej skórę.

Tr Z (masz medykamenty) +2:

* X: Sandra po tej operacji TEŻ potrzebuje paru dni w szpitalu. Wszyscy.
* Vz: Sandra skutecznie unieczynniła Elenę - podskoczyła i wbiła jej strzykawkę. Elena straciła przytomność - zadziałało na nią mocniej niż powinno. Energia ustała.

Kolejnym ruchem Sandry było wezwanie medyków...

EPILOG: 

1. Lara zażądała przeniesienia Eleny gdzieś indziej. Na pewno nie pod nią. Sandra dostała podziękowanie od Lary i zespół od tej pory zaczął działać lepiej.
2. To było wydarzenie które skłoniło Elenę do neurosprzężenia i obniżenia poziomu mocy magicznej.

## Streszczenie

Elena jest w programie Advancer na K1, ale jest na uboczu grupy. Radzi sobie świetnie kompetencyjnie, ale boi się zbliżyć do innych by ich nie skrzywdzić chaotyczną magią. To ma punkt kulminacyjny gdy jest odrzucona i tauntowana; traci kontrolę i wir magii rani jej zespół i uszkadza pokój. Elena zostaje przeniesiona - jej podejście stało się przyczyną jej problemów. Szczęśliwie, Sandra się do niej zbliżyła i Elena zrozumiała co robi nie tak.

## Progresja

* Elena Verlen: przeniesiona z programu 'advancer' do innego programu po utracie kontroli i zadaniu ciężkich ran i NAGANA DO AKT.
* Elena Verlen: złożyła pierwszy raz prośbę o neurosprzężenie
* Elena Verlen: opinia niebezpiecznej i niestabilnej, ale też o bardzo dużym potencjale

### Frakcji

* .

## Zasługi

* Sandra Kantarelo: advancer-medyk, pokonała (oszukując trucizną) Michała (szturmowca) w zapasach. Zbliżyła się do Eleny (po rozkazie Lary) i skutecznie ją zneutralizowała medykamentami gdy ta miała erupcję emocjonalną + magiczną.
* Elena Verlen: najmłodsza w programie Advancer Orbitera na K1; unika wszystkich by nikogo nie zranić i chce wygrywać samodzielnie. Kompetencyjnie świetna, emocjonalnie jak sprężyna. W końcu, odrzucona przez wszystkich poza Sandrą wybucha i jej moc magiczna zadaje straszne rany wszystkim obecnym aż Sandra ją zneutralizowała.
* Kacper Wentel: advancer-technik, żartowniś i zapalony gracz w 'Trzy Diamenty' (gra kościana); wyjątkowo nie lubi Eleny i gdy dowodził operacją to ją sprowokował do utraty kontroli i magicznego wiru.
* Hubert Mirsz: advancer-scout, poczciwy i łagodny, podkochuje się w Elenie. Dobrze radzi sobie z pacyfikatorami. Będzie dobrym żołnierzem, acz nieco zbyt łagodnym.
* Tymon Krakdacz: advancer-pilot, główny rywal Eleny w zespole. Poza próbą bycia najlepszym i poza odrzuceniem Eleny niczym się nie wsławił.
* Michał Warkoczak: combat stormtrooper, niechętnie walczy z dziewczynami. Nasi advancerzy wrobili go w zapasy z Sandrą (które przegrał bo mu dosypała truciznę do piwa).
* Lara Kiriczko: kapral Orbitera dowodząca oddziałem treningowym advancerów. Chciała zintegrować Elenę z resztą drużyny i nawet wydała odpowiednie rozkazy, ale się spóźniła - Elena miała erupcję. Przeniosła Elenę pod kogoś innego.
* OO Karsztarin: wrak kosmiczny; stacja; jednostka treningowa dla Orbitera.

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański

## Czas

* Opóźnienie: 622
* Dni: 9

## Konflikty

.

* 1 - Kacper -> Michał "zagadaj do Eleny"
    * Tr+3+2O (walka)
    * O: nastroje poszły wyżej. Pójdą pięści.
* 2 - Sandra "oki, inaczej - Michał, zapasy ze mną. Champion vs champion.". Ogólne zdziwienie wszystkich jak Sandra deeskaluje.
    * Tr Z (wtf + szok + ale jak to) +2
    * VzXXX: zdeeskalowane, ale Sandra na zapasach z Michałem, przegrani wracają w bieliźnie i załatwiają pojedynek z Eleną
* 3 - Więc Sandra dyskretnie dosypuje coś do alkoholu i wypije przed walką z Michałem. Żeby on został zatruty i walczył źle.
    * TrZ (nikt się nie spodziewa bo REPUTACJA Sandry) +2
    * VVzXV: nikt nie zauważył zatrucia, ale to nie jest natychmiastowe niestety
* 4 - Michał staje w pozycji zapaśniczej, Sandra też gotowa. "Jeśli nie stawiasz oporu to będzie dobrze". Michał atakuje.
    * Ex+2
    * VV: Michał jest dobry, ale Sandra uniknęła wszystkiego i kupiła czas
    * Ex+2 -> TrZ (trucizna)+3
    * XVzXVXV: Sandra poobijana, pociorali się, ale Sandra skopała Michała i go do nieprzytomności ciosem + trucizną
* 5 - Sandra wchodzi tak dobrze jak może w koty i tematy kotopodobne - będzie super z pacyfikatorami
    * TrZ (czas + książka + robi tylko to) +2 [do pierwszego krzyżyka albo do V, VVV, VVVVV]
    * VzVzVXXzVrV: Sandra ma pierwsze miejsce, ale siedziała do nocy i po nocy, niewyspana i obolała. Szacun u Lary.
* 6 - Sandra próbuje delikatnie próbkować Elenę - kim ona jest, na czym jej zależy, czemu tak się odgradza.
    * Ex Z (rozkaz Lary) +4
    * XVVzV: Sandra się dużo dowiedziała o Elenie, ale Elena się nie daje nikomu zbliżyć
* 7 - Kacper chce wyprowadzić Elenę z równowagi. Chce ją zdecentrować. Ma dość tej królowej lodu.
    * TrZ+3+3Om
    * VVzVzVO: Elena eksplodowała, powiedziała im co myśli i wir magiczny + Esuriit zaczął wszystko rozrywać
* 8 - Sandra doskakuje do Eleny (korzystając że jej energia nie chce skrzywdzić Sandry) i ją anestetykiem dziabie
    * Tr Z (masz medykamenty) +2
    * XVz: Sandra i inni potrzebują pomocy szpitalnej potem, ale Elena jest unieczynniona
* 9 - 
    * 
    * 
* 10 -
    * 
    * 

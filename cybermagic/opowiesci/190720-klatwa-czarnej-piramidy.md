---
layout: cybermagic-konspekt
title: "Klątwa Czarnej Piramidy"
threads: nemesis-pieknotki
gm: żółw
players: mel, onyks, samael, r1, r2
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [190709 - Somnibel uciekł Arienikom](190709-somnibel-uciekl-arienikom)

### Chronologiczna

* [190709 - Somnibel uciekł Arienikom](190709-somnibel-uciekl-arienikom)

## Budowa sesji

### Stan aktualny

* .

### Pytania

1. .

### Wizja

* .

### Tory

* .

Sceny:

* .

## Punkt zerowy

### Postacie

### Opis sytuacji

Trzy miesiące temu Senetis przeprowadziło ekspedycję do nowej anomalii / efemerydy - Czarnej Piramidy. Zespół poszedł tam w grupie 15 osób; wróciło tylko 8. Wzięli ze sobą pewne przedmioty z Piramidy:

* Samuel poszedł i rozmontował coś bardzo niebezpiecznego; Doc pomógł mu w stabilizacji tego czegoś. Samuel zajumał resztki.
* Wiktoria zdobyła kawałek kryształu z Komnaty Kryształowej Piramidy. Dzięki Genowefie, która odcięła kawałek.
* Mel zdobyła dziwny artefakt służący jako skaner. Samael wyłączył dla niej zabezpieczenia.
* Dzięki Docowi wróciło aż 8 osób. Pomogła w tym Wiktoria ze swoją ogromną natywną mocą magiczną.
* Genowefa wróciła z dziwną bronią (?). Dzięki Mel, która znalazła coś co da się wydobyć z Piramidy.

Oczywiście Zespół nie zgłosił tego do Senetis ani terminusów. Niestety, Jacek był częściowo zainfekowany przez Piramidę. By go ratować, jego żona - Tania - która przez Jacka uległa infekcji - zaczęła wzmacniać go energetycznie. Tania przechwyciła Filipa, trzeciego z ośmiu i na początku odżywiała jego energią Jacka. Niestety, energia Piramidy zaczęła na nią wpływać coraz mocniej...

## Misja właściwa

Jacek poprosił do siebie Zespół. Zamknęli biznesmena w piwnicy z Tanią; porwali gościa. Czemu? Bo zachowuje się tak jak kiedyś Filip w Piramidzie. Poproszony przez Zespół, Jacek wszedł w umysł biznesmena; wyciągnął stamtąd sporo interesujących wieści:

* został napadnięty w lasku leszczynowym i zainfekowany
* jest sprzężony z Piramidą z uwagi na inne źródło niż sama Piramida

Gdy Doc próbował mu pomóc i zbadać co z nim jest nie tak, w biznesmenie obudziła się Larwa i zaatakowała - Doc dał radę poodcinać Larwę od ciała biznesmena, więc ta zamiast przejąć kontrolę się musiała wykluć. Genowefa strzeliła swoją bronią i unieszkodliwiła (sparaliżowała) Larwę. Doc z Samuelem przebadali cholerstwo i wyszło, że jest to jakieś chore echo. Badania Wiktorii sprzężone z Mel pokazały, że ta Larwa jest kontrolowana przez jedno z ósemki która wróciła z Piramidy. Super. A jego misją? Terror.

Mel weszła głęboko swoim skanerem w temat. Znalazła, że "pasikonik" znajduje się w Dolinie Biurowej i może solidnie infekować wszystko i wszystkich. Mel i Samael poszli w tamtym kierunku rozwiązać problem. Dali też znać co udało im się pozyskać za informację - to Tatiana kontroluje pasikonika. Tatiana jest zarażona. Jest Łowcą infekującym wszystko.

Doc i Wiktoria ubezpieczani przez Genowefę poszli do Jacka przekonać go, że jego żona jest Skażona. Jednak wyczuli coś przerażającego - Jacek też jest Skażony. W desperacji, Wiktoria zwróciła się do Jacka by obudzić w nim człowieczeństwo. Jej moc zarezonowała; obudziła w Jacku człowieczeństwo. Ale niestety Tatiana to wyczuła i uciekła. Genowefa widząc to strzeliła - ale nie trafiła. Czy celowo czy nie, nie wiemy. Tatiana ma w sobie jeszcze dość człowieczeństwa by nie chcieć krzywdzić Jacka ani przyjaciół...

Tymczasem Samuel wziął na siebie niebezpieczną akcję. Mel go ubezpieczała i przygotowała pułapkę teleportacyjną. On poszedł złapać "pasikonika w ludzkim ciele" licząc się z tym, że cholerstwo go ugryzie i zainfekuje. To się stało, ale Mel go teleportowała wraz z "pasikonikiem". Samuel się dał skrępować i skrępował "pasikonika", po czym natychmiast udali się do Senetis.

Tylko Tatiana jest na wolności; los Filipa nieznany.

**Sprawdzenie Torów** ():

* skomplikowane.

**Epilog**:

* .

## Streszczenie

Po niejawnej wyprawie Senetis do Czarnej Piramidy, parę miesięcy później doszło do fali efektów powrotnych. Tatiana została Skażona i zmieniła się w infekującego potwora by uratować męża, Jacka. Zespół który był w Piramidzie powstrzymał infekcję i uratował Jacka, ale Tatiana uciekła i jest na wolności ze swoim neurosprzężonym golemem i nieludzkim ciałem plus coraz bardziej zanikającym umysłem.

## Progresja

* .

### Frakcji

* .

## Zasługi

* Samuel Czałczak: puryfikator, uratował biznesmena który stał się "pasikonikiem" i dał się Zainfekować by Senetis mieli z czym pracować.
* Mel Mirsiak: szczur, zdobyła skaner i odkryła prawdę o Tatianie. Wraz z Samuelem porwali i uratowali biznesmena który stał się "pasikonikiem".
* Dawid Klamczran: lekarz zwany 'Doc', utrzymał przy życiu wszystkich (co wyglądało na niemożliwe). Wykrył, że Jacek jest Skażony i pod wpływem efemerydy.
* Jacek Cisrak: neuronauta, od początku zarażony przez Czarną Piramidę ale dzięki Wiktorii uwolniony od jej wpływu. Wpakował w to żonę czego nie umie przeboleć.
* Tatiana Cisrak: neuropilot, główny Łowca i koordynator "pasikoników". Uratowała męża, ale sama się stoczyła w mrok. Bezpiecznie uciekła; gdzieś jest w Podwiercie.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Podwiert
                                1. Osiedle Leszczynowe: mieszkają tam Jacek i Tatiana. To jest: dwóch Skażonych przez Czarną Piramidę magów
                                1. Dolina Biurowa: miejsce, gdzie schronił się Łowca stworzony przez Tatianę
                                1. Technopark Senetis: dostali trzy próbki Skażeńca / Efemerydy "pasikonikowatej" z Czarnej Piramidy
                1. Sojusz Letejski, SW
                    1. Granica Anomalii
                        1. Czarna Piramida: Anomalia, która skutecznie zmieniła Jacka i Tatianę i zniszczyła z 15 magów aż 7

## Czas

* Opóźnienie: 1
* Dni: 2

---
layout: cybermagic-konspekt
title:  "Romuald i Julia"
threads: dzien-z-zycia-terminusa
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [181112 - Odklątwianie Ateny](181112-odklatwianie-ateny)

### Chronologiczna

* [181112 - Odklątwianie Ateny](181112-odklatwianie-ateny)

## Projektowanie sesji

### Struktura sesji: Eksploracja

* **Scena, Niestabilność, Opozycja, Trigger**
    * Romuald prosi ekipę o uratowanie Julii która samotnie poszła do czarnego sektora. Ona poszła tam z nadzieją spokoju...
    * Romuald chce znaleźć Julię, ona zrobi wszystko by się nie dać.
    * Czarny Sektor (efemeryda), Julia która się chowa
    * Pięknotka poproszona bezpośrednio o pomoc
* **Scena, Niestabilność, Opozycja, Trigger**
    * Panorama Światła
    * Strażnicy, Czarne Sektory, fatalna droga z niestabilnościami magicznymi
    * Romuald chce pokazać Pięknotce cudowne miejsce, ale ono wymaga siły fizycznej i umiejętności wspinaczkowej (tor:8)
    * Pięknotka spędza miło czas
* **Scena, Niestabilność, Opozycja, Trigger**
    * Knajpka, Romuald i Julia; Romuald chce poderwać Julię, ona "umiera". Pojedynek z Romualdem.
    * Romuald chce poderwać Julię, ona zrobi wszystko by się nie dać.
    * Niech Romuald nie zostanie totalnie zdewastowany prawnie i reputacjowo
    * Pięknotka oskarżona o to, że otruła Julię
* **Scena, Niestabilność, Opozycja, Trigger**
    * Cezary Zwierz węszy sensację!
    * Cezary chce poznać prawdę o Romualdzie i Julii, Romuald ukrywa co się da
    * Cezary Zwierz robi artykuł dewastujący Romualda
    * Pięknotka chce się pozbyć Cezarego bardziej niż Romualda - będzie zamieszana

### Hasła i uruchomienie

* Ż: (kontekst otoczenia) -
* Ż: (element niepasujący) Upiór Ateny atakujący... Pięknotkę?
* Ż: (przeszłość, kontekst) Ataki na Atenę nie zostały zatrzymane

### Dark Future

1. ...

## Potencjalne pytania historyczne

* brak

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Misja właściwa

**Scena**: Kompania ze Szkarłatnego Szeptu

_Szkarłatny Szept_:

Pięknotka się świetnie bawi w knajpce "Szkarłatny Szept". Knajpka magów, czyli wolno robić rzeczy. Coś, czego po Pustogorze jej bardzo brakuje. Wpada do knajpki przystojniak i zaczyna... bo terminusi nie chcą pomóc a jego ukochana jest w Czarnym Sektorze. Pięknotka - mental note: albo nic nie ma, albo nie iść. Wstał taki duży "wiking" i spytał gdzie - ano, na Zachodnim Wewnątrzzboczu. Wiking "spoko, się pójdzie". Tam nie ma dużego Skażenia.

Pięknotka odpaliła "battle boobs". Ona też chce! Romuald opowiedział opowieść o młodej, zakochanej parze która poszła pościgać się koło Czarnego Sektora i ona - Julia - nie znalazła drogi. Czemu nie da się po hipernecie? Bo czasem Czarny zakłóca...

Pietro, Romuald i Pięknotka poszli. Zapowiada się Przygoda. Pięknotka wzięła mapę i są gotowi by pójść na Zachodnie Wewnątrzzbocze. Pietro zauważył, że na Wschodnim byłyby problemy - na Zachodnim dużo mniej.

_Zachodnie Wewnątrzzbocze_:

Wewnątrzzbocze wygląda tak jak brzmi. Jaskinie, skały, wejście na górę, inne takie. Pietro ostrzegł Pięknotkę, że magia też działa po swojemu w tej okolicy - lepiej uważać. Pietro chce sobie wejść; Romuald daje radę. Pięknotka używa swoich umiejętności agile i odbija się od Pietro - wskoczyć najwyżej z nich wszystkich. (Tp:6,3,4=SS)

Pięknotka wskoczyła elegancko na kamień i zauważyła tam u góry coś robotokształtnego co na nią wycelowało broń. Spadła na Pietro, który ją złapał i ściągnęła ze sobą Romualda by w tego nie strzelił robot. Romuald zaczął panikować "co za robot, to straszne, ktoś przejął robota co z Julią". Pietro się ucieszył - ma w co porzucać kamieniami. Pietro ani Pięknotka się nie martwią - nie ten pryzmat by Sektor miał zabić czarodziejkę. Raczej inny los.

Wspięli się trochę wyżej, już ostrożniej. Pięknotka zagląda w zaułki szukając potencjalnych śladów - czy to robota czy dziewczyny. (Tp:8,3,4=S). Znalazła - cały czas Pięknotka się głowiła czemu Julia została w tym Sektorze. A znalazła - zanikającą sygnaturę magiczną wskazującą na to, że robota stworzył czarodziej całkiem niedawno. Poniżej 10 minut temu. Pięknotka ma lekki ślad energii tego maga. To nie Romuald.

Tymczasem z góry dobiegł krzyk Pietro. Robot strzelił wiązką laserów w zbocze góry. Lawina kamieni leci w dół, na całą trójkę. Pięknotka odpala Power Suit i leci na kolizyjny kurs z robotem. Robot się nic a nic nie spodziewa. Do tego jeszcze Pięknotka uratowała przed lawiną Romualda i Pietro. (Tr+1:8,3,6=P,S). Power suit uratował ekipę i Pięknotka walnęła prosto robotowi w pysk power suitem. Robot walnął o ścianę a Power Suit się... wyłączył. Czarny Sektor...

Pięknotka użyła emergency exit i szybko uniknęła wiązki lasera robota. (9,3,3=SS). Uniknęła bez oparzenia nawet, ale jej power suit stracił głowę. Pięknotka walnęła robota kopniakiem w tors i wysunęła go na rażenie - Pietrow rzucił włócznią i przebił robota. Maszyna spadła ze zbocza dewastując się bardziej.

* "No nieźle. Fajny sprzęt, mała" - Pietro, pod wrażeniem

Pięknotka się nie przejmuje - odbuduje sobie / Erwin jej naprawi lub zrobi. Albo coś tutaj. Pięknotka leci na dół, do robota - szuka czegoś co ją nakieruje na Julię lub twórcę. Przeciwnik (Julia) widząc sytuację aktywuje system samozniszczenia - ale Pięknotka chce poznać prawdę (Tr:7,3,6=P). Pięknotka zdążyła zobaczyć symbol w środku robota, podobny do tego symbolu jaki widziała na terrorformie, zanim robot wybuchł. Pięknotka straciła przytomność i jest Ranna.

_Kompleks Nukleon_:

Koło Pięknotki siedzi zatroskany Pietro. Powiedział, że Julia była w kajdanach przykuta do ściany - ktoś zostawił sobie ją na potem (Julia sama to sobie zrobiła). Pięknotka leży w łóżku i ma leżeć do jutra.

Pięknotka sięgnęła hipernetem do Adama Szarjana i spytała go co to za symbol (widziała to też na terrorformie) - to są technomantyczne laboratoria w Orbiterze Pierwszym. Cóż... może niech Pietro pójdzie znaleźć "nieśmiertelnik" by wyszło na to jaki to ma numer seryjny. Pietro poszedł. Niestety, nie znalazł.

(-1 Wpływ Kić: napraw mi power suit, ok?)

Wpływ:

* Żółw: 3 (6)
* Kić: 3 (4)

**Scena**: Panorama Światła, nagroda za znalezienie Julii (18:40)

_Szkarłatny Szept_:

Następnego dnia dobrze czująca się Pięknotka poszła do Szkarłatnego Świtu. Tam czekała ją owacja. Romuald jest... _bardem_. Nooo!

Pięknotka staje się duszą towarzystwa. Acz jej uwagę przykuwa Julia - gloomy i nie pasująca do imprezy. Pięknotka się do Julii przysiadła; Julia sama jest lekko podłamana, wyraźnie przez adorację Romualda. Julia się przedstawiła - Julia Morwisz, obywatelka Orbitera. Przeklęta. Rzeczy obracają się przeciw niej; jest trochę winna tego co się stało, acz nie zasłużyła na TAKĄ klątwę. Teraz jest uwięziona tutaj, w Cieniaszczycie i nie może wrócić do domu.

Julia nie zna Romualda. On się do niej przyczepił i ją adoruje...

Pięknotka wpadła na śmieszny pomysł - może uda się zdjąć klątwę z Julii. Może użyć kralothów? :>. Konflikt na corruption. Trudny konflikt Społeczny, bo Julia jest dziwną jednostką. (Tr+1:6,3,6=S). Pięknotka dała radę zauważyć coś bardzo, bardzo dziwnego. Zachowanie Julii jest nienaturalne. Ona nie jest normalnym magiem; jest czymś dziwnym. Ale klątwa docelowo będzie zdjęta.

Romuald do nich podszedł i zaproponował Pięknotce coś super. Punkt widokowy, vista, która pokazuje całość piękna Cieniaszczytu. Julia nie chce iść, ale Romuald jest bardzo przekonywujący. Pięknotce udało się przekonać Romualda, że ona pójdzie sama z nim.

_Zachodnie Wewnątrzzbocze_:

Znowu ta sama droga, ale bez robotów. Początek to w miarę prosta dla Pięknotki i Romualda trasa, acz dla nerda to by było niemożliwe. Pięknotka zdecydowała się zagadać Romualda - o co w tym wszystkim chodzi. Poprosiła, by opowiedział o Julii. Romuald może wiedzieć więcej o Julii niż komukolwiek się wydaje (Tr+1:10,3,5=S). I faktycznie - Romuald pokazał Pięknotce, że nie jest tylko zblazowanym głupkiem. Pokazał jej Julię jakiej Pięknotka nigdy nie widziałaby.

* Julia porusza się jak terminus, ale szybciej i groźniej. Jeśli jest inżynierem, to bojowym.
* Świetnie walczy i manewruje. Najpewniej jest magiem bojowym.
* Adaptuje się osobowością do sytuacji błyskawicznie. Kameleon. Najpewniej jest szpiegiem.
* Romuald dał, przypadkowo, Pięknotce fragment sygnatury magii Julii. To ta sama sygnatura co maga, który uruchomił robota.

* (Kić: -3: Są w mieście Pryzmatu. Nieważne czym jest, on opowiada o wielkiej miłości do niej i wzajemnie. Ona będzie czuła to samo do niego)
* (Żółw: -1: Delay do momentu wyznania miłości Julii na viście)

Pięknotka wie, że coś jest nie tak z Julią. Wie też, że może się dowiedzieć co i jak amplifikując Pryzmat na viście. Więc kierunek w którym idzie z Romualdem bardzo jej pasuje...

Dotarli do bardziej skomplikowanego terenu. Teren wymaga wspinaczki (Typowy:8,3,4=S). Dotarli do pięknej visty. Widok na cały Cieniaszczyt, a przynajmniej sporą jego część. Romuald opowiadał Pięknotce jak bardzo kocha Julię i w ten sposób zapieczętował jej los.

* (Żółw: -3: Julia zorientowała się jakie jest zagrożenie pryzmatycznie. Nie zdążyła, ale jest tu - na ostatnią walkę)

Z krzaków wyskoczyła Julia krzycząc "nie!". Za późno. Błyskawiczna pozycja bojowa i atak w Romualda (tor Julii: 10). Pięknotka błyskawicznie odpaliła Power Suit i odepchnęła Romualda (Tp:9,3,3=SS). Pięknotka nie spodziewała się takiej prędkości ze strony "maga" - Julia ścięła jej działko. (tor:2/10). Julia zaatakowała w Pięknotkę bezlitośnie; Pięknotka szybko osłoniła się kamieniem by ciosy się ześlizgiwały - niech jej broń ugrzęznie (Tp+1:SS)(tor:3/10). Pięknotce odnowiła się rana - Julia porusza się ZA SZYBKO i power suit za bardzo Pięknotkę przeciąża.

Za to Julia zaczęła też krwawić. Widać, że coś ją spala. Pięknotka od razu zauważyła wspomaganie magiczne i chemiczne. Julia wyskoczyła w lewo i zaczęła zbierać Skażoną energię magiczną. Pięnotka miotnęła w nią kamieniem - Julia raz jest widoczna a raz nie, ale Pięknotka ma wspomaganie komputerowe. Pięknotka rozwaliła kamień by strzelić shotgunem (Tp+2:11,3,3=P)(tor:4/10). Pięknotka zrobiła emergency exit z power suita który zaczął powoli adaptować do woli Julii. Pięknotka rozejrzała się w przerażeniu.

Ręka Minerwy. Total scramble. Power suit zasilany z jej własnego zasilania - służący do zagłuszenia połączenia i zadania maksymalnych cierpień napastnikowi (hej, Minerwa ma smutne doświadczenia z terrorformem). I ten feedback uderzył w Julię (Tr+1:8,3,6=S)(tor:7/10). Julia w agonii, Pięknotka łapie Romualda i biegną. Pięknotka wie, że czas jest na jej korzyść.

Mimo przewagi, Pięknotka i Romuald nie są tak szybcy jak Julia, po prostu. Romuald zna trasy (Tr+1:8,3,5=S)(tor=9/10). Julia atakuje Pięknotkę na pełnym biowspomaganiu. Pięknotka, bez power suita, próbuje walczyć mimo wszystko. Pięknotka użyła najsilniejszego afrodyzjaku jakiego ma, by rozerwać umysł Julii, by ją wytrącić i dokończyć robotę (Tr+3:8,3,6=P). Zadziałało. Ale wpierw - Julia zmieniła ranę Pięknotki ze Zwykłej na Poważną. Pięknotka kaszle krwią. Julia... Julia jest zagubiona.

Julia padła na kolana. Prosi o śmierć. Wstała i podchodzi do krawędzi Cieniaszczytu. Romuald się rzuca "nie pozwolę ci odejść, moja ukochana". Pięknotka próbuje nie parskać śmiechem by nie tracić krwi...

Wpływ:

* Żółw: 3 (10)
* Kić: 2 (6)

**Epilog**: (19:55)

* Pięknotka skończyła w Nukleonie. Znowu. Wyjdzie następnego dnia.
* Julia nie popełnia samobójstwa. Jest wolna, po raz pierwszy od dawna.
* Julia nie opuści boku Romualda i ani ona ani on nie opuszczą Cieniaszczytu.
* Jakkolwiek to nie jest zabawne, tylko Julia, Romuald i Pięknotka wiedzą, co się tam stało i że Julia jest czymś dziwnym...

Julia i Pięknotka się spotkały. Julia powiedziała, że jest Emulatorem - wspomaganą czarodziejką, koordynowaną przez osobę wyżej. Nie wie dokładnie co i jak, ale zna cel swojej misji - spotkać Pięknotkę i się z nią zaprzyjaźnić. Miała wejść "do drużyny" Pięknotki, pilnować Pięknotkę i Atenę. Z rozkazów swoich mocodawców w Orbiterze Pierwszym. Nie miała robić krzywdy ani Pięknotce ani Atenie - na razie. A wszystko inne to była zmyłka. Poza Romualdem - on był szprychą w planach Julii.

Julia wie, że Emulatorów jest więcej.

### Wpływ na świat

| Kto           | Wolnych | Sumarycznie |
|---------------|---------|-------------|
| Żółw          |   1     |    10       |
| Kić           |   3     |     8       |

Czyli:

* (K): .
* (Ż): Julia jest niezdolna do zrobienia jakiejkolwiek krzywdy ani Romualdowi ani Pięknotce - nawet by ratować ich życie.
* (Ż): W Cieniaszczycie Pięknotka dostaje reputację osoby, która pragnie być ostro poturbowana. Taki fetysz.

## Streszczenie

Pięknotka pomogła Romualdowi uratować jego ukochaną Julię. Po drodze był też groźny robot bojowy. Docelowo okazało się, że "Julia" jest Emulatorem - agentem Orbitera Pierwszego której celem było zaprzyjaźnić się z Ateną i Pięknotką by je obserwować. Pięknotka zneutralizowała Emulatora Romualdem i jego miłością plus Pryzmat, acz dwa razy skończyła w szpitalu podczas jednej sesji (rekord).

## Progresja

* Pięknotka Diakon: w Cieniaszczycie Pięknotka ma reputację osoby, która lubi być ciężko poturbowana (kraloth/Zwierz, 2 razy w szpitalu...)
* Julia Morwisz: jest wolna. Jako Emulator wyrwała się spod kontroli koordynatorów, jak długo jest w Cieniaszczycie
* Julia Morwisz: jest niezdolna do zrobienia jakiejkolwiek krzywdy ani Romualdowi ani Pięknotce - nawet by ratować ich życie, nawet pod kontrolą Krwi.
* Romuald Czurukin: dostaje stałą i sensowną posadę barda w Szkarłatnym Szepcie.

### Frakcji

* 

## Zasługi

* Pięknotka Diakon: chciała zakosztować rozkoszy w Cieniaszczycie, skończyła w szpitalu dwa razy i pokonała morderczo niebezpieczną Emulator. Taki los.
* Romuald Czurukin: bard, zakochany w Julii od pierwszego wejrzenia. Zniszczył plany Emulatora. Miłością wyciągnął Julię do świata wolności i radości.
* Julia Morwisz: Emulator wysłana by zaprzyjaźnić się z Pięknotką. Slightly creepy. Zakochała się w Romualdzie i ostro pobiła Pięknotkę. Uzyskała wolność.
* Pietro Dwarczan: "wiking". Silny, świetnie miota kamieniami. Troszczył się o Pięknotkę i dbał o nią na akcji. Bardzo sympatyczny, z Pięknotką mają do siebie pewną miętę.
* Adam Szarjan: hipernetowe źródło informacji dla Pięknotki odnośnie symboli z Laboratorium Technomantycznego Orbitera Pierwszego.

## Plany

* Pięknotka Diakon: chce wzmocnić swój power suit lokalną technologią. Emulator jest w końcu technomantką i inżynierem...
* Julia Morwisz: nigdy nie opuści Cieniaszczytu, by koordynatorzy nie odzyskali nad nią kontroli
* Julia Morwisz: zostaje na stałe z Romualdem Czurukinem; kochają się
* Romuald Czurukin: zostaje na stałe z Julią Morwisz; kochają się

### Frakcji

* 

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Przelotyk
                        1. Przelotyk Wschodni
                            1. Cieniaszczyt
                                1. Knajpka Szkarłatny Szept: knajpka magów, gdzie praktycznie wszystko wolno; przebywa tam Romuald, Julia, Pietro i Pięknotka
                                1. Wewnątrzzbocze Zachodnie: miejsce gdzie pojawiają się Czarne Sektory i które się zmienia; nie jest tak groźne jak Wewnątrzzbocze Wschodnie
                                    1. Panorama Światła: piękna vista, gdzie doszło do masakrycznej walki Pięknotki z Julią (Emulatorem na pełnej mocy)
                                1. Kompleks Nukleon: dwukrotnie w ciągu dwóch dni Pięknotka była tam reperowana z poziomu ran ciężkich

## Czas

* Opóźnienie: 1
* Dni: 2

## Narzędzia MG

### Budowa sesji

**SCENA:**: Nie aplikuje

### Omówienie celu

* Analiza sesji eksploracyjnej.
* Założenie jest takie:
    * Eksploracja to zbiór scen nie powiązanych wątkiem (lub zawierających pojedyczny wątek), gdzie są Sytuacje.
    * Sytuacja wymaga podjęcia decyzji przez Gracza i zagrania o to - dzięki czemu zbudujemy świat
* Żółw ma moc Wpływu na (3).

## Wykorzystana mechanika

1810

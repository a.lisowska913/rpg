---
layout: cybermagic-konspekt
title: "Szmuglowanie Antonelli"
threads: corka-morlana
gm: żółw
players: kić, skobel
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [210810 - Porwanie na Gwiezdnym Motylu](210810-porwanie-na-gwiezdnym-motylu)

### Chronologiczna

* [210810 - Porwanie na Gwiezdnym Motylu](210810-porwanie-na-gwiezdnym-motylu)

### Plan sesji
#### Co się wydarzyło

.

#### Strony i czego pragną

.

#### Sukces graczy

* Sukces
    * PRIMARY: znalezienie bezpiecznego miejsca dla Antonelli
* Porażka
    * ?

#### Postacie graczy

* Arystokratka (Flawia): ATUT - szkolenie wojskowe, wygląda słodko i niegroźnie, działania w próżni, wybitna aktorka, mag. Gracz: Kić
* Szmugler?: ATUT - znajomości, ukrywanie się / infiltracja, świetny w walce. Gracz: 

### Sesja właściwa
#### Scena Zero - impl

.

#### Scena Właściwa - impl

* Q: Dlaczego młody poczciwy arystokrata ufa Wam bezgranicznie?
* A: Ominęliśmy zakazaną przestrzeń i on mógł to docenić. Zasady stosujemy też gdy jest koszt.

Tomasz Sowiński. Mówi, że trzeba przetransportować Antonellę poza planetę. BO MA Z NIM DZIECKO I TRZEBA JĄ WYCOFAĆ. I do planetoid Kazimierza (to akurat brzmi logicznie). "Cień" jest sceptyczny. Flawia jest sceptyczna. (nie na planetoidy, na to "dziecko"). Brighton nie do końca wierzy, że Flawia jest guwernantką Tomasza (Tomasz rozkręcił wielką kłamliwą opowieść, jak to on).

* "Aha, nie jesteś guwernantką Tomasza a jego dzieckiem"

Brighton powiedział Tomaszowi, że ten musi koniecznie powiedzieć o co tu chodzi. To zbyt poważne tematy:

* V: tien Nataniel Morlan nadal na Antonellę poluje a rodzice kazali mu ją oddać.

Fecundatis wymaga odprawy celnej (jak każdy statek). A Tomasz nie może użyć jachtu Sowińskich bo jeden rozbił. Dobrze by było przekazać na orbitę Flawię jednym z wielu jachtów Sowińskich. Cień powiedział, że nie może Tomasz działać jak dziecko. Musi dorosnąć i nie zostawiać śladów.

* V: Tomasz poszedł za planem Cienia. Wtajemniczy Jolantę i jachtami w kosmosie przekażą Antonellę na orbitę.

Nagroda taka jaką uzgodniliśmy...

JESZCZE TEGO SAMEGO DNIA spotkaliście się z Jolantą. W Eksplozji (klub taneczny z weteranami, z antygrawitacją). Taniec trochę jak capoeira.

Brighton "Cień", ciało starsze niż innych. Dużo depigmentacji od magii.

Tien Sowińska usłyszała o pomyśle Brightona by zmienić Wzór Antonelli. Jest to mistrzostwo. Nie pomyślała o tym - lepsze niż planetoidy Kazimierza. Brighton wpływa na Jolantę - niech powie swoje mroczne sekrety o Pasie Kazimierza. Skąd w ogóle wie o takim miejscu, czemu tam?

* V: Jolanta powiedziała prawdę - korespondowała z Bruno Baranem i napisała mu miłosny wiersz... i wie gdzie go znaleźć
* V: Jolancie Brighton bardzo przypasował. Polubiła go. Przesłała korespondencję.
* Vz: Jolanta zdecydowała się osobiście lecieć z Brightonem i z Flawią, wierzy w ich kompetencję, chce się przydać, wie że na miejscu przyda się bardziej i ufa że sobie poradzi.

Brighton zaproponował - najlepiej zrobić to TUTAJ. Pod okiem Morlana. Wrócić do Aurum i z Lucjuszem przekształcić Wzór Antonelli.

Plan nie jest głupi - wyekspediować Antonellę do pasa Kazimierza.

Flawia kontaktuje się hipernetowo z Lucjuszem. Lucjusz zaproponował Pustogor. Tam może ją naprawić. Do tego celu potrzebna jest mu krew. Poprosił by Flawia przekazała mu krew płaszczką.

Brighton zauważył, że jeśli Lucjusz ma krew Antonelli, to automatycznie Nataniel wie, gdzie Antonella się znajduje. Lub BĘDZIE się znajdować. Krew odpada.

Flawia ma pomysł jak to rozwiązać - PŁASZCZKOMATRIOSZKA. I niech Lucjusz trzyma to w odpowiedniej komorze ekranującej. Flawia przekonała Lucjusza by ten przekonał - ratują czarodziejkę Eterni (przed innym z Eterni, ale Nataniel nie jest popularny). PLUS - przysługa dla Sowińskich.

Lucjusz Blakenbauer przekonuje Pustogor do użycia Szpitala Terminuskiego do pomocy w przebudowie Wzoru Antonelli. Brighton zaproponował, że przeszmugluje do szpitala z Tosią dodatkowo materiały z kosmosu jako wdzięczność i płaciwo za pomoc Antonelli i ekranowanie. (+3)

ExZ+3 (niekompletna karta)+3:

* V: Lucjuszowi udało się osiągnąć sukces - przeznaczą specjalną komorę.
* V: Brighton nałapał kontaktów w Pustogorze ("znam tam kogoś") i jest "oficjalnym ukrytym szmuglerem".

Została ostatnia rzecz - co zrobić z Antonellą na przestrzeni następnego miesiąca?

ZGODNIE Z PLANEM przetransportowano w kosmosie Antonellę na pokład Fecundatis i Fecundatis ruszył w kierunku na planetoidy Kazimierza.

Gdy Antonella trafiła na pokład Fecundatis to Brighton zdecydował się jej spytać - czy ona chce w ogóle zmienić Wzór?

Antonella jest uszkodzona. Ma dysasocjację. Nie zapamiętuje. Poprzednie osobowości się jej mieszają.

* V: Bez problemu uda się dolecieć do Pasa Kazimierza z Antonellą.
* X: Antonella ma instynkt macierzyński do jadowitych stworzeń, skorpoidów. Rozkłada je w różnych miejscach... i trzeba było się przyzwyczaić >.>.
* V: "Obie Antonelle" (obie osobowości) mają jeden sprawny nawyk. Antonella podłapała jakąś formułę praktyki typu "tai chi". O konkretnej porze dnia idzie zrobić katę / sekwencję - coś do uspokojenia.
* X: Jadowite stworzenia kąsają. Odnowienie zasobów medycznych na Kazimierzu potrzebne. Jolka boi się stworzeń i jadu i zamyka się w pokoju.
* X: Część populacji jadowitych stworzeń przetrzebiamy. Antonella cierpi. Nie rozumie. Bo i eksterminacja i jednocześnie ona nie ma dostępu. --> Antonellę TRZEBA naprawić.

Jolanta będzie się opiekować Antonellą.

* X1: Nope. Znalazła JEDYNEGO skorpiona. Tymczasowa panika - NIE WYJDZIE Z KAJUTY. Nie będzie się integrować. Ma traumę. FAKT: Nauczyła się neurosprzężenia - oddać TAI Elainka kontrolę nad sobą.
* V: Brighton wyjaśnił Jolancie, że ucieczka przed strachem to ucieczka. Nie warto oddać TAI kontroli nad sobą. Udało się sprawić, że Jolanta nie uzależni się od TAI. 

Brighton: Myślę, że któregoś dnia przyniosę do pokoju Joli tam gdzie jemy wizerunek mojego syna. "To był Forrest, służył w armii. Zachorował. W którymś momencie jego ciało przestało działać. Ktoś by uznał że to nie byłoby racjonalne by brać środki oczyszczające do boju. Polubiłabyś go."

* (+2V) V: Jolanta ma większą WRAŻLIWOŚĆ i nie chce oddawać kontroli TAI. Nie chce robić błędów wynikających z czystej optymalizacji. Plus, przesunięta na zainteresowanie programem kosmicznym. By móc pomóc osobom takim jak Flawia. Plus, wbudowała w Elainkę pewne dyrektywy.

Fecundatis jest długim, sekwencyjnym pojazdem z uchwytami przypominającymi żebra czy wypustki na dysku gdzie można podczepić kontenery - te kontenery są przymocowane magnetycznie. Bardzo słabo oświetlony i ogrzany. Zanim stał się przemytniczym transportowcem był foodtruckiem. Zaopatrzeniowym wewnątrzsystemowym. Bardzo mały statek, niewiele osób. Do 30 osób. 8 osób załogi oficjalnie. Silnie zrobotyzowany.

Kilka pomieszczeń to echo przeszłości.

Całe poszycie z jednej strony jest zdarte - statek był skanibalizowany.

DROGA DO PASA KAZIMIERZA.

Pytanie - czy ten cały plan dał radę zrzucić łowców nagród z Fecundatisa. Flawia wie jak będą szukać, Brighton wie jak ukrywać. Była płaszczka odwracająca uwagę. Było sporo statków.

Pierwsze dokowanie było na Valentinie, by zostawić skorpoidy. Wydłuża to trasę, ale jest bardziej chaotycznie PLUS wygląda bardziej naturalnie. Jolanta zaproponowała, by jakoś zostawić tu krew Antonelli.

Flawia poprosiła Brightona o szczura. Dużo szczurów. Brighton wybiera siódmą kategorię mięsa i zdecyduje się na znalezienie SZCZURA na Valentinie. 

TrZ+2 zerowa karta:

* X: zapalone światło, futrzak się obrócił... a to czyjaś wiewiórka. Egzotyczna wiewiórka czy coś. Oops, trzeba przepraszać XD.
* X: konieczność zapłacenia kary / grzywny. Plus kontrola z sanepidu i prawdziwe dane.

Flawia Blakenbauer idzie (bo wywalili ich z hotelu XD) przekonać kolesia od wiewiórki żeby ją "pożyczył". 

Więc jak przychodzą sanepidowcy na statek to Flawia znajduje sobie kogoś wyglądającego na podatnego i zaczyna się krygować, uśmieszek, oprowadza po statku...

Nie ma skorpoidów na statku... ALE FLAWIA SPROWADZA GOŚCIA NA JEDNEGO. Niech ma osiągnięcie czy coś. Ale cel - wzbudzić poczucie "ta wiewiórka i nie wiem czy jej się coś nie stało a jak ugryzł to mutuje... i ja mogę sprawdzić a on nie pozwoli się zbliżyć..."

ExZ+2:

* V: Sukces. Udało się pozyskać wiewiórkę "do badań".
* X: W wyniku - zarejestrowane "gdzieś tam".

Flawia używa chorego zaklęcia przekształcając wiewiórkę w _beacon_ dla detektorów Antonelli. Używając krwi Antonelli.

TrMZ+2:

* Vm: Wiewiórka pełni swoją rolę godnie. I jest zdrowa poza tym.
* Vm: Działania Flawii zamaskowane. Wszyscy myślą że aura i modyfikacje to pochodna tego że musiała wyleczyć BO COŚ UŻARŁO.
* X: "Leczenie" wiewiórki będzie trasowane do Fecundatis.
* Vz: Krew nie woda. Użycie krwi sprawia, że zdaniem detektorów Antonella nadal jest "Duchem" tej stacji. Szukają na Valentinie NAD innymi lokacjami.

Wracamy do lotu do Pasa Kazimierza.

ExZ+2 --> TrZ+3 niepełna karta: 

* V: Dotarliście do Pasa Kazimierza zanim oni Was dopadli
* X: W Pasie Kazimierza są sympatycy Nataniela
* X: Tym sympatykiem jest Ernest Blakvel, który chce być "szefem" tego regionu.
* V: W pasie Kazimierza nie pojawią się łowcy nagród. Blakvel będzie chciał działać samemu.

Docieracie bezpiecznie do Pasa Kazimierza.

## Streszczenie

Antonella Temaris stała się problemem politycznym na linii Sowińscy - Nataniel Morlan. By nie została oddana, Tomasz, Jolanta i Flawia weszli we współpracę z Cieniem Brightonem, przemytnikiem. Przemycili Flawię na orbitę (Brighton skłonił Jolantę, by ta poleciała z nimi!), po czym zgubili ewentualny pościg na Valentinie. A drugą linią Flawia przekonała Lucjusza, by ten przygotował szpital terminuski w Pustogorze na zmianę Wzoru Antonelli, by ją naprawić...

## Progresja

* Cień Brighton: ciało starsze niż powinno być. Dużo depigmentacji od energii magicznych.
* Cień Brighton: nałapał kontaktów w Pustogorze ("znam tam kogoś") i jest "oficjalnym ukrytym szmuglerem".
* Jolanta Sowińska: boi się skorpioidów... po podróży Fecundatis z Antonellą nic dziwnego że ma pomniejszą fobię.
* Jolanta Sowińska: Brighton wyjaśnił jej, że ucieczka przed strachem to ucieczka. Nie warto oddać TAI kontroli nad sobą. Uwrażliwiona. Nie odda kontroli. PLUS - zainteresowana programem kosmicznym by inne tak nie cierpiały jak Flawia.
* Antonella Temaris: instynkt macierzyński do skorpioidów i insektów jadowitych; niestety, gubi je na lewo i prawo.
* Antonella Temaris: ma rytuał i nawyk pozwalający jej na częściowe odzyskanie kontroli i reintegrację + uspokojenie się.
* Ernest Blakvel: sympatyk Nataniela Morlana, chce być jak jego idol i zreplikować jego działania w Pasie Kazimierza.
* Lucjusz Blakenbauer: ma dostęp do krwi Antonelli Temaris, z czego wynika - Wzór pasujący do Nataniela Morlana.

### Frakcji

* .

## Zasługi

* Flawia Blakenbauer: współpracując z Lucjuszem doprowadziła do tego, że Antonelli Pustogor naprawi Wzór. Wyflirtowała z sanepidowcem dostęp do wiewiórki i zintegrowała jej krew z krwią Antonelli, skutecznie gubiąc Morlana i jego łowców nagród.
* Cień Brighton: kapitan SC Fecundatis. Rozplątał "intrygę" Tomasza, wydobył od Jolanty mroczne sekrety o listach miłosnych i skłonił ją do podróży z nim do Pasa Kazimierza. Ojcowski styl - chce pomóc Antonelli tylko jeśli jej to pomoże. Przewozi skorpioidy, kiepsko poluje na szczury i skutecznie gubi łowców nagród ;-).
* Antonella Temaris: uszkodzona. Ma dysasocjację. Nie zapamiętuje. Poprzednie osobowości się jej mieszają. Nie podejmie decyzji. Ale nauczyła się nawyków i chce się przydać.
* Jolanta Sowińska: świeżo hipersprzężona z TAI Elainka; wie o paśmie Kazimierza (bo słała kiedyś listy miłosne do Bruno Barana). Poleciała z Brightonem i Flawią do Pasa Kazimierza by ratować Antonellę; nabawiła się strachu przed skorpioidami XD.
* Tomasz Sowiński: twórca najgorszych planów ratowania Antonelli w historii, ale jego gorące serce zapaliło Jolantę do pomocy i przekonał do tego też Cienia. Dostarcza surowców Lucjuszowi Blakenbauerowi by zmienić Wzór Antonelli.
* Bruno Baran: tien eternijski w Pasie Kazimierza i obiekt westchnień Jolanty Sowińskiej kiedyś. Dostawał od niej listy miłosne i nawet odpisywał z faktami i informacjami młodej tience z Aurum.
* Lucjusz Blakenbauer: już objął szpital terminuski w Pustogorze; przekonał Pustogor do tego by pomóc Antonelli (zasoby Sowińskich + kontakty z orbitą poza Orbiterem).
* SC Fecundatis: statek Brightona transportujący skorpioidy do celów medycznych; jest długim, sekwencyjnym pojazdem z uchwytami przypominającymi żebra czy wypustki na dysku gdzie można podczepić kontenery - te kontenery są przymocowane magnetycznie. Bardzo słabo oświetlony i ogrzany. Zanim stał się przemytniczym transportowcem był foodtruckiem. Zaopatrzeniowym wewnątrzsystemowym. Duży statek, ale niewiele osób. 12 osób załogi oficjalnie. Silnie zrobotyzowany.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Aurum
                        1. Imperium Sowińskich
                            1. Krystalitium: piękne, nowoczesne sentisprzężone miasto. Ulubione miejsce przebywania Jolanty i Tomasza Sowińskich.
                                1. Pałac Świateł: może nie największy, ale bardzo piękny, należy do rodziny Tomasza Sowińskiego (i tolerują też obecność Jolanty).
                                1. Klub Eksplozja: klub taneczny, często z weteranami. Antygrawitacja + capoeira. Bezpieczne miejsce w rozumieniu Jolanty Sowińskiej - spotkanie z Brightonem.
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Pustogor
                                1. Rdzeń
                                    1. Szpital Terminuski: objęty przez Lucjusza Blakenbauera; po wsparciu Sowińskich (finanse) i Brightona (szmugiel, materiały z kosmosu poza Orbiterem) zbudowano specjalne pomieszczenie do przebudowy Wzoru Antonelli w poważnym ukryciu.
            1. Stacja Valentina: Fecundatis podłożyli tam wiewiórkę z krwią Antonelli - dzięki temu łowcy nagród Morlana nie szukali Fecundatis dalej i uznali że Antonella jest "Duchem" tam.


## Czas

* Opóźnienie: 43
* Dni: 12

## Konflikty

* 1 - Brighton powiedział Tomaszowi, że ten musi koniecznie powiedzieć o co tu chodzi. To zbyt poważne tematy. Czas dorosnąć.
    * TrZ+2
    * VV: Morlan poluje na Antonellę; Tomasz wtajemniczy Jolantę i pójdzie za planem Brightona.
* 2 - Brighton wpływa na Jolantę - niech powie swoje mroczne sekrety o Pasie Kazimierza. Skąd w ogóle wie o takim miejscu, czemu tam?
    * Tr+3
    * VVVz: korespondowała z Bruno i pisała mu wiersze miłosne; polubiła Brightona i poleci z nim i Flawią by pomóc Antonelli
* 3 - Lucjusz Blakenbauer przekonuje Pustogor do użycia Szpitala Terminuskiego do pomocy w przebudowie Wzoru Antonelli
    * ExZ+3 (niekompletna karta) +3
    * VV: Nie tylko Lucjuszowi się udało i zrobią specjalną ekranowaną komorę; też Brighton nałapał kontaktów w Pustogorze. 
* 4 - Gdy Antonella trafiła na pokład Fecundatis to Brighton zdecydował się jej spytać - czy ona chce w ogóle zmienić Wzór? A jest uszkodzona...
    * Tr+3
    * VXVXX: dolecą, Antonella ma instynkt macierzyński do skorpioidów i je gubi, dodanie Antonelli sprawnego nawyku reintegrującego (sekwencję), Jolka się boi skorpioidów i część trzeba zabić
* 5 - Jolanta będzie się opiekować Antonellą.
    * Tr+3
    * XV: Nope. Znalazła JEDYNEGO skorpioida i uciekła w TAI. Brighton jej wyjaśnił, że ucieczka przed strachem to ucieczka. Nie uzależni się od TAI.
    * (+2V) V: większa wrażliwość i nie chce oddać kontroli TAI, też zainteresowana programem kosmicznym by pomóc takim jak Flawia.
* 6 - Flawia poprosiła Brightona o szczura. Dużo szczurów. Brighton wybiera siódmą kategorię mięsa i zdecyduje się na znalezienie SZCZURA na Valentinie. 
    * TrZ+2 zerowa karta
    * XX: Upolował udomowioną wiewiórkę, kara / grzywna + kontrola z sanepidu...
* 7 - Więc jak przychodzą sanepidowcy na statek to Flawia znajduje sobie kogoś wyglądającego na podatnego i zaczyna się krygować - chce dostać tą wiewiórkę na "naprawę"
    * ExZ+2
    * VX: wiewiórka "do badań", ale Fecundatis jest zarejestrowane na Valentinie - był tam i to widać.
* 8 - Flawia używa chorego zaklęcia przekształcając wiewiórkę w _beacon_ dla detektorów Antonelli. Używając krwi Antonelli.
    * TrMZ+2
    * VmVmXVz: Wiewiórka pełni rolę godnie, zamaskowana; ale da się prześledzić Fecundatis. Morlan przekonany, że Antonella jest "Duchem Valentiny" przez tą wiewiórkę...
* 9 - Wracamy do lotu do Pasa Kazimierza. Brighton gubi łowców nagród.
    * ExZ+2 --> TrZ+3 niepełna karta
    * VXXV: Sympatycy i miłośnicy Morlana to Blakvel; ale Fecundatis zgubił łowców nagród i Blakvel chce działać sam.

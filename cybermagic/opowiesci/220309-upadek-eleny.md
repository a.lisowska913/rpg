---
layout: cybermagic-konspekt
title: "Upadek Eleny"
threads: legenda-arianny
gm: żółw
players: fox, kapsel, kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [Stabilizacja Keldan Voss](220223-stabilizacja-keldan-voss)

### Chronologiczna

* [Stabilizacja Keldan Voss](220223-stabilizacja-keldan-voss)

## Plan sesji
### Co się wydarzyło

Faza 1:

* Iorus. Pierścień Halo. Znajduje się tam niewielka rozproszona kolonia Keldan Voss.
* Keldan Voss zbankrutowali. Zostali kupieni przez Pallidę.
* Pallida sprowadziła swoich ludzi, którzy działają nad stację należącą do Keldan Voss.
    * Drakolici z Keldan Voss mają dość nieprzyjemne zasady. Nie chcą redukować poziomu energii.
        * ewolucja przez magię
    * Pallidanie chcą zdobywać i harvestować kryształy Vitium.
    * Mała stacja, z rozproszonymi stacjami dodatkowymi. Wszędzie są generatory Memoriam.
* Faith Healer Keldan Voss nazywa się Kormonow Voss.
* Z uwagi na wysoki poziom magii, stare mechanizmy itp. mamy promieniowanie - lokalni drakolici i sprowadzeni pallidanie chorują i gasną
* Z uwagi na to, że drakolici nie chcą się integrować i współpracować, wszyscy mają obowiązkowe transpondery / "komórki".
* Drakolici mają swoich "Kroczących We Mgle", którzy mogą działać bez skafandrów i we mgle.

Faza 2:

* Mgła. Część Kroczących jest Skażona i jest częścią Mgły. Część działa bez problemu.
* Pallidanie obwiniają drakolitów o Mgłę i o śmierci spowodowane przez Mgłę.
* Mgła atakuje bazę.
* Dwóch agentów naprawiających generatory Nierzeczywistości zostało Skażonych przez Mgłę.
* Szczepan chce, by Annice się nie udało. On lepiej będzie tym dowodził.
    * 4 członków delegacji -> na orbicie
    * wysłany zespół zabójczy w Mgłę; zabił już 7 drakolitów
* Kormonow chce się pozbyć pallidan. Mgła - nienawiść - go już opętała.
    * Drakolici wiedzą, że Kormonow wysadził generatory by móc ratować umierających drakolitów i palladian.
    * 11 pallidan (sprowadzonych) jest przez Kormonowa "naprawionych". -> 7 zabrała Mgła
    * 12 pallidan "porwanych" -> 5 zabrała Mgła, 3 uratowanych przez drakolitów
* Gdzie są drakolici? => niewielkie generatory Nierzeczywistości, znajduje się baza zasilana Vitium w księżycu.

Faza 3:

* Historia
    * Było tu starcie noktian i drakolitów z Neikatis. Drakolici wzięli się tu stąd, że na Neikatis wojna faeril - drakolici. Tutejsi uciekli w to miejsce. I wtedy napatoczyli im się noktianie. I noktianie stąd uciekli - pokonani przez drakolitów. I tutejsi drakolici to po prostu "chów wsobny" a częściowo "wolni ludzie". Część noktian tu wróciła i zostali przyjęci. I ci ludzie stanowią aktualny kult. 
    * Gdzieś po drodze zwrócili się do Saitaera. I on odpowiedział. Małe dary - utrzymać ludzi przy życiu. I osobą, która zwróciła się do Saitaera był ich naczelny naukowiec, badacz. Ten koleś znalazł faktycznych kultystów Saitaera. To jest przodek naszego szamana. Kormonow Voss jest potomkiem badacza, który "oddał" tą bazę Saitaerowi. 
    * pallidanie są z Neikatis. Oni są bliżej faeril.
* Mateus chce fuzję korporacji i sił Saitaera po rozmowie z Arianną
* Kryształy Vitium PAMIĘTAJĄ bitwę noktian i drakolitów. Jakaś forma vitium jest potrzebna.
* Na orbicie jest mag - Barnaba Lessert. To on jest celem Bii. Bia (klasa: Reitel) chce go pozyskać.
* Szaman, Kormonow Voss, chce by pallidanie przestali ich zabijać - Mgła i wysokie stężenie kryształów Vitium jest niezbędne do życia drakolitów.
    * On jest już homo superior. On i wielu innych drakolitów. A np. Mateus jest "brzydki" by odwrócić uwagę od Perfekcyjnych
* Mgła ma własności korupcyjne.
* Elena
    * Esuriit daje jej nieskończony głód. Obsesja na punkcie poprzedniej formy i poprzedniego życia.
    * Ixion daje jej adaptację. Jej ciało jest morficzne. Korozja i korupcja.

Faza 4:

* Uratowani, znajdują się na Keldan Voss - ale są oblężeni przez Mgłę i anomalie mgieł
    * Chcą, by Sebastian i Barnaba odpowiedzieli za swoje zbrodnie - chcą ich oddać Mgle
* Pojawia się Żelazko z Olgierdem i Marią na pokładzie
    * jest tam też Janus Krzak; on ma pomysł, uważa, że da się jakoś rozmontować tą Anomalię.
    * Olgierd chce doprowadzić do pokoju między tym wszystkim - nie da się doprowadzić tej bazy do działania bez wszystkich sił
        * Elena chce uratować wszystkich. Uważa, że jest w stanie. Olgierd chce Elenę zablokować.
* Pojawia się ratunkowy statek Pallidan - "Światło Nadziei"
    * Chcą odzyskać i zdezanomalizować Pallida Voss.
    * Chcą uratować swoich, też Annikę. Annika jest obwiniona za wszystko.

Faza 5:

* Elena zneutralizowała BIĘ.
* Annika szuka spokoju u Eustachego.
* Elena została Aniołem Saitaera z perspektywy radykalnej frakcji kultystów.

Jakie mamy siły:

* Drakolici, ugodowa (Mateus): chce współpracy z Pallidanami, chce ich przejąć w imię Saitaera
* Drakolici, radykalni (???): chcą... samotności? Samobójstwo?
* BIA: chce powrotu do przeszłości. Chyba. Reprezentuje energie magiczne. Odpychana generatorami Memoriam, ale się wzmocniła
* Pallidanie, lokalsi: oni chcą wrócić do domu i zapomnieć. Ewentualnie, pokój z drakolitami 
* Pallidanie, orbita: chcą mieć SUKCES, uratować jak najwięcej ludzi się da i odzyskać Szczepana
* Pallidanie, antydrakoliccy: oni chcą utopić Annikę i doprowadzić do zniszczenia projektu Keldan Voss
* Orbiter: chce rozwiązać problem z małą kolonią w której coś się spieprzyło i nie stracić nikogo przy okazji...

### Co się stanie

* Mgła koroduje ixiońsko poza pewnym promieniem
* Elena wchodzi w tryb "I must win" + "devour"

### Sukces graczy

* Co zrobić na linii Pallidanie - Drakolici?
    * Pallidanie chcą opuścić to miejsce
    * Drakolici chcą samodzielności... ale też współpracy?
* Co zrobić z żądaniami Drakolitów o wydanie Sebastiana i Barnaby?
* Co zrobić z Eleną..? Nie chce wracać. Co z anomalną jednostką?
    * Obsesja piękna.

## Sesja właściwa
### Scena Zero - impl

REKALIBRACJA

* Co chcecie osiągnąć jako główny wątek?
    * Kić: Nocna Krypta
    * Kacper: niespodzianka :3. SAMURAJ NIE MA CELU SAMURAJ MA DROGĘ. Super była poza znanym kosmosem. Eksploracja daleka. Ale pasuje :D.
    * Fox: Nocna Krypta - uratowanie Anastazji i Eleny.
* O co chcecie walczyć w najbliższym czasie? Czy pasuje Wam cokolwiek Wam dam, czy na CZYMŚ Wam zależy?
    * Kić: Klaudia -> Uwolnione, równorzędne TAI. Wolny statek sterowany AI, czy coś.
    * Fox: Możemy współpracować, niezależnie z kim.
    * Kacper: zawsze jak brudna i ciężka robota, podnosił rękawicę. Jakaś cząstka człowieczeństwa umiera. Ale nie on to kto? Zapewnić przyszłość dalej.
* Jakiego typu akcji / działań na sesji chcecie w najbliższym czasie więcej?
    * Kić: eksperymenty naukowe Klaudią, ale nie widzę tego w kontekście Inferni. Science ship.
    * Kacper: wszystko mi się zazębia, wszystko działa. Tak lubię najbardziej. Pełne spektrum. Postacie odbijają się w świecie i to czuć. Realistyczne.
        * Więcej sesji eksperymentalnych!! Gospodarka zasobowa, pójść na układ, rzeczy bardzo nietypowe, wyścigi itp.
        * persystentne, wyczerpywalne zasoby! Epizody a nie odcinki serialu.
    * Fox: więcej retrospekcji, relacji z innymi postaciami. Byli dłużej w jednej lokalizacji. I jeśli jesteśmy dłużej to powiedz na początku.
* Jakiego typu akcji / działań na sesji chcecie w najbliższym czasie mniej?
    * Kić: nie chcę tępienia cywili albo załogantów.
    * Kacper: mniej magii! Mniej chorej magii! Mniej NIEOCZEKIWANYCH efektów ubocznych.
    * Fox: encountery osobisty gdzie muszę być osobiście zamieszana, walczyć, załatwić. RACZEJ na poziomie strategicznym lepiej.

### Scena Właściwa - impl

Elena została Aniołem Saitaera z perspektywy radykalnej frakcji kultystów.

Frakcje:

* Drakolici, ugodowa (Mateus): chce współpracy z Pallidanami, chce ich przejąć w imię Saitaera
* Drakolici, radykalni (???): chcą... ELENY JAKO ANIOŁA
* BIA: chce opanować sytuację i wymrzeć.
* Pallidanie, lokalsi: oni chcą wrócić do domu i zapomnieć. Ewentualnie, pokój z drakolitami 
* Pallidanie, orbita: chcą mieć SUKCES, uratować jak najwięcej ludzi się da i odzyskać Szczepana
* Pallidanie, antydrakoliccy: oni chcą utopić Annikę i doprowadzić do zniszczenia projektu Keldan Voss
* Orbiter: chce rozwiązać problem z małą kolonią w której coś się spieprzyło'

Cele Arianny:

1. przywrócić morale Anniki
2. wydobyć od Bii wiedzę odnośnie tego jak pomóc Elenie
3. Kroczący we Mgle

Arianna, chce Kroczący x statek chcący sukcesu. 

Annika zamknęła się w małej klitce, 1x1.5m, robi podsumowanie strat i zysków, przed korporacją musi się wyspowiadać. Eustachy ma dobre serduszko i nie chce jej wykorzystać. "Kapitanie!" (ona wie że Eustachy dowodzi ale dla niej on jest kapitanem). "Ona jest taka STRASZNA, Ty jesteś duszą statku, taki... niezłomny". 

* Eustachy na to "cały Orbiter to złożona maszyna która nie działałaby gdyby najmniejszy trybik nie robił swojej rzeczy i nie wierzą że ich praca jest potrzebna do funkcjonowania całości. Trybiki się wyłamują to maszyna się psuje."
* Annika: "ja już nie mam przyszłości! oni wszyscy nie żyją... ja ich tu sprowadziłam"
* Eustachy: "świadomi pracownicy, podjęli, lepszy cash. Byłaś sabotowana." - DZIĄSŁOWANIE OTRZEŹWIAJĄCE. - "czas na użalanie minął jak przejęłaś dowodzenie. Upadasz, podejmujesz decyzje, świecisz przykładem."

Tr (niepełny) Z (jej nieskończone zaufanie do Eustachego) +3 + 3Or:

* Or: Annika przetrwa trudne chwile. ZAINSPIROWANA EUSTACHYM! ON JEST JEJ IDOLEM!

.

* Annika: "Jestem na egzekucję! Rodzice we mnie wierzyli! A ja zawiodłam, mój przyjaciel jest we mgle... Co mam zrobić? Zaaranżować śmierć i chronić rodzinę? Czy wrócić i ryzykować rodzinę?"
* Eustachy: "Aranżować i zmienić to możesz firanki. Musisz się ogarnąć i skonfrontować ze swoim losem."
* Eustachy: "Weź się w garść i rób to dobrze."
* Annika: Muszę mieć dochodową bazę.

...

* (+Or): V: Annika SPRÓBUJE. Ona na serio chce dobrze, ma skille administratorki. Dałaby radę...

Sebastian prosi Ariannę o to, żeby ona podjęła DOBRĄ decyzję. Zapomnimy o wszystkim. A on dostarczy środków. I załagodzi się sprawę z bratem, nikt nic nie wie... a on pomoże.

Arianna gada z Tomkiem. Arianna jest skłonna oddać (przehandlować) Barnabę i Sebastiana Tomkowi za to, że lokalni drakolici dostaną bonusy i nie tylko. Żeby pallidanie musieli się zaangażować w sprawy drakolickie. Arianna chce, by wróg drakolitów pomagał drakolitom. Dostanie Barnabę, niekoniecznie brata.

Ex (5Oy) +3:

* V: Tomek jest skłonny wesprzeć lokalną bazę ze strony pallidan. Nie podoba mu się eksperyment ALE będzie go kontynuował.
* V: Tomek jest skłonny zrezygnować z tępienia Anniki. Wesprze ją politycznie.

Tomek jest... rozczarowany wynikiem. Arianna ma lepsze karty i dobre argumenty. Zaakceptuje ciszę.

* X: On osobiście wykazał się skutecznością i niezłomnością. Jego PRZYWÓDZTWO doprowadziło do tego, że sprawa jest rozwiązana po stronie Pallidan.
* X: Tomek się zgadza, żeby zamiast Barnaby dostać innych magów. 3. Akceptuje, by byli agentami Orbitera. Ale wspierają JEGO.

Tomek się ucieszył ze spotkania z Arianną. Naprawdę pomoże tej placówce. ("Verlenka w korpo")

CO MAMY:

* Annika będzie mieć szansę powodzenia. Odbudowany jej link z Pallidanami.
* Pallidanie mają ogólnie "spokój". Lokalni Pallidanie mają zero morale. Oni się boją. Chcą wracać.

Eustachy wpływa na Annikę - niech przypisze sobie sukces. Jeśli pozwoli, by podwładni widzieli w niej wystraszoną dziewczynkę nic się nie zmieni. A jak wyjdzie że ona ogarnęła temat - sama uwierzy że ogarnie przyszłe tematy. Annika jest gorąco wdzięczna.

Annika przekonuje ludzi. Że udało jej się. Że mają jeszcze jedną szansę. Że będą współpracować z drakolitami. Że nikt stąd nie odejdzie, bo statek na orbicie ich nie chce. I że ona jest ich jedyną szansą. Wyjęła rachunki jakie wystawiła korporacja... muszą to albo naprawić albo dywizja zabójców (dokładniej: Dywizja Windykacji Ekstremalnej) ich dorwie.

Tr (niepełny) Z (bo sukces i faktycznie działa) + 3Og (nihilizm i entropia) + 3:

* X: Część osób będzie szukało jak stąd zwiać. Mimo wszystko.
* Vz: Annika ma jeszcze jedną szansę wśród lokalsów, ale nie ma zaufania.
* Vz: Annika ma zaufanie - jest ostatnią osobą jakiej można tu ufać przy tym wszystkim.

Arianna -> Elena. Chce z nią porozmawiać o oddaniu ludzi. Elena jest nostalgiczna. Oddaliła się w mgłę rozkazywać jako anioł Saitaera...

TrZ+2+5Or:

* Or: "Byłam piękna. A potem przestałam."
* Or: "Gdybym... była nadal piękna, on by mnie kochał..."
* V: "Bezwartościowe śmieci, wydajcie mi ich"

Arianna: "nie przyzwyczajaj się do metod Eustachego"

* Xz: Elena pożarła dwóch kultystów. Znowu jest piękna.
* (+3Vg): V: Elena porwała Kroczącego w Mgle.

Elena jest znowu piękna. Zintegrowała się z Kroczącym i nim steruje.

Mateus do Arianny dyskretnie: "Komodor Verlen? Kim ona jest? I co musimy zrobić by stąd odeszła?" Mateus wyjaśnił, że ona nie POŚWIĘCA. Ona ZABIERA. Zabiera przeznaczenie innych by wzmacniać swoje.

## Streszczenie

Keldan Voss została ustabilizowana. Annika będzie mieć szansę powodzenia - zostaje przełożoną, z łącznikiem z keldanitami Mateusem. Odbudowany jej link z pallidanami. Pallidanie mają ogólnie "spokój". Tomasz Kaltaben będzie ją wspierał a on sam dostanie 3 magów Orbitera co go wspierają (i są agentami). Elena niestety zabrała trzy życia odzyskując naturalną urodę. Klaudia ciężko pracuje by zbudować dla Eleny Detox Chamber - ona nie jest OK.

## Progresja

* Elena Verlen: słowami Mateusa - "w imieniu Saitaera POŚWIĘCASZ. Ona nie POŚWIĘCA. Ona ZABIERA. Zabiera przeznaczenie innych by wzmacniać swoje". Odzyskała swoje piękno zabierając 3 życia.
* Mateus Sarpon: absolutnie przerażony istnieniem Eleny Verlen. Czymkolwiek jest, w jego oczach jest arcybluźnierstwem.
* Annika Pradis: spróbuje być administratorką i liderką. Jest tyle winna swoim ludziom. Eustachy ją zainspirował.
* Annika Pradis: ma jeszcze jedną szansę wśród lokalsów i zaufanie, acz ludzie próbują uciec z Keldan Voss. W końcu załatwia sprawy.
* Tomasz Kaltaben: reputacja: "on wykazał się skutecznością i niezłomnością. Jego PRZYWÓDZTWO doprowadziło do tego, że sprawa jest rozwiązana po stronie pallidan." Plus - 3 magów Orbitera co jego wspierają.

### Frakcji

* .

## Zasługi

* Arianna Verlen: załatwiła polityczne wsparcie dla Keldan Voss i Anniki od pallidan. Wymusiła współpracę między Tomaszem Kaltabenem i Anniką Pradis. I zapewniła że Barnaba i Szczepan spotkają sprawiedliwość.
* Eustachy Korkoran: podnosi morale Anniki - ma próbować. Zmusił ją do wzięcia odpowiedzialności za Keldan Voss i jej przyszłość. Annika za tym poszła. Pomógł dziewczynie.
* Klaudia Stryk: zarządzała komunikacją, sygnałami itp. Była w cieniu. I projektowała mechanizm, dzięki któremu da się wyczyścić Elenę i zapewnić, że nie stanie się potworem.
* Tomasz Kaltaben: pomoże Keldan Voss ze strony pallidan. Nie podoba mu się eksperyment z Keldan Voss ale będzie kontynuował. Nie będzie tępić Anniki, wesprze ją politycznie. Dogadał się z Arianną.
* Elena Verlen: dla Eustachego, dla piękna, dla przyszłości pożarła kultystów. Ale porwała Kroczącego we Mgle, wbijając mu neurokontrolę ixiońską palcem w układ nerwowy (creepy as hell).
* Annika Pradis: jednak dowodzi Keldan Voss, współpracując z keldanitami i pallidanami. Nie dała rady zainspirować nikogo, ale dobrze żyje ze wszystkimi - dobra administratorka, fatalna liderka :D.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Iorus
                1. Iorus, pierścień
                    1. Keldan Voss
 
## Czas

* Opóźnienie: 1
* Dni: 6

## Konflikty

* 1 - Annika zamknęła się w małej klitce. Eustachy DZIĄSŁUJE OTRZEŹWIAJĄCO. czas na użalanie minął jak przejęłaś dowodzenie. Upadasz, podejmujesz decyzje, świecisz przykładem."
    * Tr (niepełny) Z (jej nieskończone zaufanie do Eustachego) +3 + 3Or:
    * Or: Annika przetrwa trudne chwile. ZAINSPIROWANA EUSTACHYM! ON JEST JEJ IDOLEM!
    * V: SPRÓBUJE. Ona na serio chce dobrze, ma skille administratorki.
* 2 - Sebastian prosi Ariannę o to, żeby ona podjęła DOBRĄ decyzję. Zapomnimy o wszystkim. A on dostarczy środków. I załagodzi się sprawę z bratem, nikt nic nie wie... a on pomoże. Arianna chce, by wróg drakolitów pomagał drakolitom.
    * Ex (5Oy) +3
    * VV: Tomek pomoże bazie ze strony pallidan. Nie podoba mu się eksperyment z Keldan Voss ale będzie kontynuował. Nie będzie tępić Anniki, wesprze ją politycznie.
    * XX: On osobiście wykazał się skutecznością i niezłomnością. Jego PRZYWÓDZTWO doprowadziło do tego, że sprawa jest rozwiązana po stronie pallidan. Zamiast Barnaby - 3 magów Orbitera co jego wspierają.
* 3 - Eustachy wpływa na Annikę - niech przypisze sobie sukces. Jeśli pozwoli, by podwładni widzieli w niej wystraszoną dziewczynkę nic się nie zmieni.
    * Tr (niepełny) Z (bo sukces i faktycznie działa) + 3Og (nihilizm i entropia) + 3
    * XVzVz: ma jeszcze jedną szansę wśród lokalsów i zaufanie, acz ludzie próbują stąd uciec.
* 4 - Arianna -> Elena. Chce z nią porozmawiać o oddaniu ludzi. Elena jest nostalgiczna. Oddaliła się w mgłę rozkazywać jako anioł Saitaera...
    * TrZ+2+5Or
    * OrOrVXzV: dla Eustachego, dla piękna, dla przyszłości pożarła kultystów. Ale porwała Kroczącego we Mgle, wbijając mu neurokontrolę ixiońską palcem w układ nerwowy (creepy as hell).

---
layout: cybermagic-konspekt
title: "Sentiobsesja"
threads: legenda-arianny
gm: żółw
players: kić, fox
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [210224 - Sentiobsesja](210224-sentiobsesja)

### Chronologiczna

* [210220 - Miłość w rodzie Verlen](210210-milosc-w-rodzie-verlen)

## Punkt zerowy

### Dark Past

* Nie znamy. Próbujemy poznać ;-).

### Opis sytuacji

* Cofamy się o 18 lat w przeszłość, jak Arianna i Viorika były młodziutkie.

### Postacie

* Arianna Verlen: Fox
* Viorika Verlen: Kić

## Punkt zero

N/A

## Misja właściwa

Viorika i Arianna zostały wezwane na teren Hold Bastion. Ku ich wielkiemu zdziwieniu, sentisieć w obszarze Hold Bastion zaraportowała zero sentikontrolerów. Powinno być tu co najmniej dwóch magów rodu Verlen - Apollo i Mścigrom. Nie ma ani jednego. Arianna natychmiast skontaktowała się z innym magiem na tym terenie, sierżantem Czapurtem, którego jest troszkę pupilką. 

Czapurt przekazał dziewczynkom informację o tym, że niedawno był tu atak Blakenbauerów lub sił Blakenbauerów. 3-4 bioformy bojowe. Do tego 2 kocie bioformy, które... kradły skarpetki. Bujna wyobraźnia czarodziejek już zaczynała podsuwać im pomysły o Mrocznych Eksperymentach Blakenbauerów i o tym, jak Krwawa Lady Blakenbauer (o której słyszały) ma zamiar robić straszne rzeczy z tymi skarpetkami. Ale nadal nie ma to sensu, nie?

Viorika, wyjątkowo mocno zdolna do sprzężenia z sentisiecią, zdecydowała sie na próbę znalezienia Apolla czy Mścigroma. Mścigrom po prostu nie jest obecny. Nie ma go tutaj nigdzie. A Apollo? Sentisieć go _widzi_, ale _nie ma z nim kontaktu_. Innymi słowy, jest w promieniu sentisieci, ale nie jest nigdzie w promieniu Holdu Bastion. Dla Vioriki jest to ciekawa zagadka, taka, którą koniecznie musi rozwiązać na miejscu. Czas dotrzeć do Holdu Bastion i przejąć kontrolę nad sentisiecią - nie może być tak, że sentisieć jest całkowicie poza kontrolą; w tym momencie może stać się coś strasznego.

Już na miejscu Arianna spytała, czemu one w ogóle tu są. Nie chciały tu przyjeżdżać, a tu takie kłopoty. Sierżant Czapurt z uśmiechem odpowiedział: "Każda młoda dama powinna zabić jakąś płaszczkę Blakenbauerów" i odpowiedział co się tu dzieje - otóż, Apollo nalegał, by Mścigrom i jego przyjaciółka opuścili ten teren. W ten sposób jest jedynym sentitienem w okolicy. Z jakiegoś powodu się na to zgodzili. Więc Czapurt zadziałał za plecami Apolla - stwierdził, że nadszedł czas by dwie młode, obiecujące damy zostały ściągnięte tu na praktykę. Apollo był wściekły, ale nie mógł na to nic poradzić. W ten sposób Wirek i Aria tu trafiły i mogą posprzątać. Czapurt jest paskudnie zadowolony z siebie. Przynajmniej dziewczynki poćwiczą ;-).

Tak, ród Verlen ma bardzo... _free-ranging_ podejście do swoich członków.

Viorika słyszała już dość. Nie może nie być na tym terenie żadnego sentitiena, więc zdecydowała się przejąć kontrolę nad sentisiecią. Zajmuje jej to większość wieczoru, ale jest do tego szczególnie dostosowana; skupiła się i weszła głęboko w sentisieć. (X: blokada, Apollo jest samosprzężony, V: Viorika przejęła kontrolę, V: sentisieć uznała Viorikę za a-tien Holdu, V: Viorika dowiedziała się co się stało z Apollem).

Apollo był **jedynym** sentitienem w Holdzie. To, plus bardzo silne emocje i sprzężenie emocji i woli sprawiło, że Apollo został opętany czymś co nosi fachową nazwę "sentiobsesja". Jedność emocji i woli, zatopiona w sentisieci. Jego wola, jego uczucia, jego myśli - w 100% napędzają sentisieć. A sentisieć napędza Apolla. Jakiegokolwiek celu Apollo nie miał, ten cel zasila całe jego myślenie i funkcjonowanie a sentisieć będzie podążać za tą wolą. Niefajnie.

Arianna nie jest w stanie Viorice pomóc. Gdy Viorika zajmuje się sentisiecią, Arianna odwiedza kantynę "Szczur". Taką kantynę dla żołnierzy. Wie, że Apollo zawsze był blisko żołnierskiej braci i zdecydowała się wślizgnąć w te dyskusje - przy odrobinie szczęścia uda się dowiedzieć co Apollo myślał, co miał na myśli i na czym jego działanie może teraz polegać.

Ten moment, gdy siedzą żołnierze w knajpie. Gadają sobie o kobietach i swoich podbojach. I nagle z tyłu wyłania się (nieletnia w końcu) Arianna i też chce pogadać. AWKWAAAAARD!!! Ariannie nie było łatwo się w 100% zsynchronizować z żołnierzami, więc zrobiła to, co każda nastolatka robi gdy chce pasować do starszego towarzystwa - zaczęła pić i się z nimi bawić.

* X: w momencie, w którym grali w butelkę wpadł sierżant. Zmienił kolor na twarzy kilka razy. 
* V: Arianna dowiedziała się sporo z plotek od żołnierzy:
  * W okolicy jest "Krwawa Lady Blakenbauer", straszliwa Milena, która uwielbia torturować i pić krew swoich ofiar. Apollo miał z nią solidny zatarg.
  * Apollo ma kochankę. Nie, ma TRZY. Żadna nie wie o pozostałych. Dlatego prosił Mścigroma o opuszczenie terenu, mógł się, hm, pobawić.
  * Koty Blakenbauerów wiedzą o kochankach. Zdaniem Apolla, one chcą mu złamać karierę! (to nawet młodziutka Aria wie, że takie rzeczy nie łamią karier w rodzie Verlen)
  * Apollo był opętany żądzą zemsty na Krwawej Lady Blakenbauer. Jest wszystkim czego nienawidzi w Blakenbauerach!!!
  * Najpewniej Apollo ruszył na Ostropnącze.
* X: Sierżant opieprzył wszystkich za picie i rozpijanie nieletnich (Arianny). Arianna: "ale ja nic nie piłam!" Sierżant: "CHUCHNIJ!!!". Arianna: "no może tyci". Wszyscy, łącznie z Arianną wylądowali do rana w karcerze i całą noc muszą śpiewać pieśni patriotyczne.
* V: Żołnierze uznali, że Arianna to spoko laska. Taka "młodsza siorka". Sytuacja całkowicie AWKWAAAARD, ale Aria jest spoko.

Noc upłynęła naszym dwóm heroinom w taki sposób, że Aria strasznie fałszuje w kiciu a Wirek łączy się z sentisiecią i próbuje jakoś się zintegrować mimo Apolla ;-).

Następny dzień zaczął się nie mniej pracowicie. Szczęśliwie, nasze damy są młode - są bez kłopotu w stanie funkcjonować mimo niezbyt przespanej nocy. Więcej, Arianna wyszła z karceru z zapewnieniem dozgonnej miłości sporej ilości żołnierzy - i kilkunastoma telefonami ;-).

Viorika rozstawiła detektory i systemy rozpoznawcze dookoła Holdu Bastion szukając śladów Apolla i/lub Blakenbauerów. Nic. Wszystko wskazuje na to, że ten atak był całkowicie losowy i chodziło o kradzież skarpetek. Ale to nie ma sensu. Viorika nie wie o co chodziło Krwawej Lady Blakenbauer, ale ma najgorsze przypuszczenia. Viorice udało się jednak dojść do jednej cennej informacji - Apollo jest na terenie dzikiej sentisieci, stamtąd wysyła roje i ataki przeciwko Blakenbauerom. Dość... paskudnie. Nie tylko Apollo jest zagrożony (dzika sentisieć) ale też może to powodować negatywne konsekwencje wobec samych Blakenbauerów.

Trzeba Go Zatrzymać!

Pół dnia ciężkiego planowania i dziewczyny wymyśliły coś, z czego są całkiem dumne. Trzeba ściągnąć Apolla w pułapkę!

Po pierwsze, Krwawa Lady Blakenbauer ma plan, by wszystko powiedzieć kochankom Apolla. O TO JEJ CHODZIŁO OD SAMEGO POCZĄTKU. Innymi słowy, Arianna używa 100% swojego czaru, umiejętności przekonywania i tego, że została "ulubioną młodszą siorką militarnej gałęzi Holdu Bastion", żeby "w tajemnicy" puścić tą informację. Oczywiście, Apollo ma się NICZEGO NIE DOWIEDZIEĆ. (ofc żołnierze plotkują jak mało kto, więc Apollo będzie wiedział szybko):

* V: +5V
* X: kochanki będą wiedziały o sobie. Te plotki sprawią, że do nich też ta wiadomość dotrze. I one zewrą szyki przeciwko Apollo. Jest to koszt, który Viorika i Arianna z przyjemnością poniosą ^__^.
* V: Arianna zostaje na stałe "ulubioną młodszą siorką" na tym terenie.

Po drugie, Viorika wykorzystuje pozostałe po Blakenbauerach resztki (m.in. kocie bioformy). OCH, TU JEST TYLKO ARIA I WIREK, JAK SOBIE PORADZĄ Z POTĘGĄ BLAKENBAUERÓW. Czyli pozorowany atak.

* V: +5V
* V: chwała Viorice i jej planowi

Jak na razie, sierżant Czapurt jest zadowolony ze swoich młodych dam. Do tego jeszcze dziewczyny skupiły się na sygnale typu "HALP ME OBI WAN KENOBI" wobec Apollo a Viorika skupiła się na rozpracowaniu taktyki Apolla. Udało się. Dziewczyny są pewne, że uda im się rozwiązać ten problem raz i na zawsze - ściągnąć Apollo tutaj, w okolice Holdu Bastion i Viorika używając swojej potęgi ma zamiar użyć sentisieci do wyżarcia sił Apollo i rozbicia go.

Z puli 16V 7X 5Oa 5Ob: OVVVV. Czyli:

* OV: Apollo słysząc o problemach wysłał dywersję na tereny Blakenbauerów (15 osób ma palić dżunglę). Sam jednak ruszył na teren Holdu Bastion najszybciej jak się da.
* VV: KRWAWA LADY!!! Apollo leci z taką prędkością, że ma jedynie niektóre terrorformy ze sobą.
* VV: Połączenie sił Vioriki i Arianny sprawiły, że Apollo wpadł w pułapkę. Perfekcja.

Gdy tylko Apollo zbliżył się odpowiednio w stronę Holdu Bastion, Viorika uderzyła od razu sentisiecią. Apollo otoczony przez sentisieć spróbował zrobić kontratak, ale nie ma tak dużego sprzężenia jak Viorika przez sentiobsesję. No i Arianna też wspiera Viorikę ;-).

* V: dzikie nanitki są poniszczone przez lojalistyczną sentisieć
* V: atak jest odpowiednio szybki, Apollo nie zdążył zrobić nic ważnego
* V: jest to efekt szoku i atak z zaskoczenia

Arianna widząc szok Apolla próbowała dotrzeć do niego. "To co zrobiłeś - naraziłeś swoje kochanki. Żołnierze w niebezpieczeństwie. To nie jesteś Ty!".

* XX: za długo trwa ta operacja. Żołnierze już tam są, w sentisieci Blakenbauerów
* V: Apollo odrzuci obsesję. Ariannie się uda doń trafić.
* XX: żołnierze sami nie wrócą, są schwytani przez Krwawą Lady. No i Apollo zapłaci za to wizerunkowo (śpiewa i tańczy przed Mileną przebrany za maid-chan)

No nic. Czyli Blakenbauerowie mają nasz oddział. Przechlapane. Ostatni sygnał "prześladuje nas elf, ze skrzydełkami!"

Jak to rozwiązać politycznie? Arianna wie. Najlepiej przez sierżantów. Niech Czapurt skontaktuje się ze swoim odpowiednikiem po stronie Blakenbauerów i powie mu, że ich żołnierze zabłądzili. Da się zrobić jakieś spotkanie na szczycie między Verlen i Blakenbauer i odzyskać tych nieszczęsnych ludzi. I faktycznie, ta metoda zadziałała. Na granicy między Blakenbauerami i Verlenami nawet jest zbudowana specjalna struktura, Hala Negocjacji. Tam się spotkają Blakenbauerowie i Verlenowie. Po dwie osoby. Czyli Viorika i Arianna - a po stronie Blakenbauerów, Milena i Lucjusz. Dyskretnie, w nocy.

Spotkanie było... intrygujące. Milena, Krwawa Lady Blakenbauer, jest... w formie catgirl. Jest skupiona na dobrej zabawie i poznanie granic możliwości. Zapytana o co chodziło z tymi skarpetkami powiedziała, że to był zakład z Lucjuszem. Lucjusz powiedział, że nie uda się ukraść niczego magom Verlen, Milenka uznała, że to nie jest takie trudne. Innymi słowy, to była zabawa, jakkolwiek dziwnie to brzmi. A samo "Krwawa Lady"? A, bo to jest takie śmieszne. Patrzcie jaka jestem groźna :D:D:D. Arianna i Viorika mogły tylko wymienić spojrzenia.

Natomiast Lucjusz próbował jakoś kontrolować swoją nieobliczalną kuzynkę. Bardzo nie chciał tu być. Ale jeszcze bardziej nie chciał, by Milenka robiła co chce i wtedy kiedy chce.

Skończyło się na tym, że Blakenbauerowie oddadzą ten oddział. A jako reperacja za uszkodzoną dżunglę - Apollo w stroju maid-chan zatańczy i zaśpiewa na dworze Krwawej Lady Blakenbauer. Tzn, w Ostropnączu. No cóż, jest to akceptowalna cena...

Potem Arianna musiała do tego przekonać Apolla XD. Ale się udało: "Twoje dziewczyny będą chciały się odegrać. W ten sposób im to umożliwiasz. Przejdzie im". Ten argument trafił.

* VV: złość, uraza, ale nie nienawiść - Apollo nie ma nienawiści wobec tych konkretnych Blakenbauerów. Po prostu dalej wszystkich nienawidzi XD.
* XX: Apollo został gwiazdą YouTube Blakenbauerów. Verlen, Który Nie Umie W Cosplay Ani Śpiewanie...

## Streszczenie

Apollo Verlen, sam w Holdzie Bastion był jedynym sentitienem. Gdy uderzyły siły Mileny Blakenbauer, wściekł się i wpadł w sentiobsesję - chce atakować Blakenbauerów. Na szczęście na miejsce przybyły Arianna i Viorika, które rozwiązały sprawę, odkryły sekret Apolla (3 kochanki) i ściągnęły go pułapką, po czym... pokonały i przywróciły. Aha, potem odzyskały brakujący oddział kosztem śpiewu i tańca Apolla przed Mileną Blakenbauer. 

## Progresja

* Arianna Verlen: jest pupilką sierżanta Przemysława Czapurta rodu Verlen. To znaczy, że traktuje ją ostro i twardo ;-).
* Arianna Verlen: "młodsza siorka" żołnierzy w Holdzie Bastion. Swoja laska. Fajna, wpada z nimi w kłopoty i mimo imienia (Aria) nie umie śpiewać pieśni patriotycznych w karcerze.
* Viorika Verlen: chwała jej planom; już w tak młodym wieku uznawana za świetnego stratega znajdującego słabe punkty u przeciwnika.
* Apollo Verlen: dostał opiernicz od sierżanta za sentiobsesję. Dostał wpiernicz od kochanek. Śpiewał i tańczył w stroju maid-chan dla Mileny Blakenbauer. Bardzo, bardzo zły okres.

### Frakcji

* .

## Zasługi

* Arianna Verlen: 16 lat, zwana "Aria". Zaprzyjaźniła się z żołnierzami i najczęściej wykorzystuje swoje umiejętności retoryczne i to, że jest słodka do osiągania celów. Przekonała Apolla do zwalczenia sentiobsesji a potem oddała Apolla Milenie by odzyskać oddział ;-).
* Viorika Verlen: 17 lat, zwana "Wirek". Umysł taktyczny i bardzo mocno powiązany z sentisiecią Verlen. Przejęła kontrolę nad sentisiecią i bez chwili wahania uderzyła nią w Apollo by wyrwać go z sentisieci. Mniej społeczna niż Arianna, bardziej intelektualna.
* Apollo Verlen: 24 lata, zawsze blisko żołnierskiej braci. BWR. Trzy kochanki które o sobie nie wiedzą. Poprosił, by inni sentitienowie opuścili ten teren, co Mścigrom zrobił; potem Apollo wpadł w sentiobsesję przez Milenę Blakenbauer. Uratowany przez Ariannę i Viorikę.
* Milena Blakenbauer: 15 lat, "Krwawa Lady Blakenbauer" - bo tak jest śmieszniej. Catgirl. URG. Zakład z Lucjuszem sprowadził kłopoty. Ciekawska, nie ma w niej nienawiści czy złości, bardzo dziecięca. Idzie za uczuciami i perfekcją formy. Odda oddział za piosenkę i taniec.
* Lucjusz Blakenbauer: 17 lat, UB. Próbuje stabilizować Milenkę; taki trochę nerd. Przekonał Milenkę, że jednak najlepszą opcją jest oddanie oddziału rodowi Verlen. Z góry patrzy na standardowe bioformy, mogą być ulepszone.
* Przemysław Czapurt: sierżant Verlenów, mag; kompetentny wiarus, który ściągnął do Holdu Bastion Viorikę i Ariannę za plecami Apolla, by jego plany (jakiekolwiek by nie były) zostały zneutralizowane. Ciepło patrzy na Viorikę i Ariannę, mniej na Apolla (bo spieprzył).
* Mścigrom Verlen: 28 lat, dał się przekonać kuzynowi i wyjechał z Holdu Bastion, przez co naraził Apolla na sentiobsesję. Powinien wiedzieć lepiej. Bardzo spolegliwy i chętny do pomocy swoim.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Aurum
                        1. Verlenland
                            1. Hold Bastion: na granicy z Blakenbauerami i dziką sentisiecią; 
                                1. Karczma Szczur: gdzie Arianna się zaprzyjaźniła z żołnierzami. Oczywiście, gdy tylko zaczęli grać w butelkę, wpadł na to sierżant...
                                1. Barbakan: Viorika przejęła tam kontrolę nad okoliczną sentisiecią.
                                1. Karcer: gdzie Arianna i zaprzyjaźnieni żołnierze spędzili noc. I gdzie musieli śpiewać pieśni patriotyczne rodu Verlen większość nocy...
                        1. Świat Dżungli: region Blakenbauerów
                            1. Ostropnącz: na granicy z Verlen i dziką sentisiecią; 
                        1. Sentipustkowie Pierwotne: pierwsze sentipustkowie bez Rodów magicznych. Spore. Dzika sentisieć. Graniczy ze Światem Dżungli (Blakenbauer) i Verlenlandem (Verlen).


## Czas

* Opóźnienie: -6900
* Dni: 3

## Inne

### Projekt sesji

Ród Verlen

* Skrajnie militarystyczny, silnie wojskowy, z tradycjami
* Aurum: Laetitia - Niezależność - Czystość
    * Verlen: Duty|Discipline - Niezależność - Czystość
    * Verlen: "To my stoimy na murach, gdy wszystkie ogniska zgasną"

Ród Blakenbauer

* Silnie powiązany z bioformami, "płaszczkami". Specyficzne poczucie humoru.
* Drakolickie podejście do sentisieci; ludzka forma jest tylko jedną z możliwości.
* Często uważani za ród czarny.

Milenka Blakenbauer:

* No malice in her whatsoever. Jej zdaniem tak powinno wyglądać życie. Żądna przygód. Nieco zbyt ufna w siebie i umiejętności (i drugą formę).
* RUG "how far can we go". Czyli: "Childlike Curiosity, insatiable desire to explore, discover, understand; no concern for status or ego". Creativity, Authenticity, Truth Seeking.

Apollo Verlen:

* "jak ja tych psów Blakenbauerów nienawidzę"
* WBR "principle to defend, then move on", zdominowany przez sentisieć próbuje zniszczyć Ostropnącz

### Sceny sesji

* Muszą odeprzeć atak Apolla chroniąc domenę Blakenbauerów..?
* Milena napada ich w sentidżungli Blakenbauerów
* "No tak, tam są biovaty z ludźmi. I inne mutacyjne biovaty. Mutvaty?"

## Idea sesji

* Milena od czasu do czasu rzuca jakieś kreatywne bioformy chcąc sprawdzić jak sobie poradzą przeciwko potędze Verlen. Nic groźnego. "Okrutna lady Blakenbauer".
* Apollo jest zbyt silnie nienawidzący Blakenbauerów. Opętuje go sentisieć. Wyrusza przeciwko Milenie, używając sentisprzężenia; armię pozyska z sentipustkowia.
* Zespół widzi "OMG PORWALI APOLLA? XD". Nie no, ratujemy go.
* Oddział Verlen dąży do spalenia Ostropnącza. Zespół mediuje między groźną lady Blakenbauer a ochroną / walką z Apollem.
* "Nie jesteście tymi ludźmi którymi chcę dowodzić... ale mam was i tylko was..."

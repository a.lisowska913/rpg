---
layout: cybermagic-konspekt
title:  "Decyzja Minerwy"
threads: dzien-z-zycia-terminusa
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [181021 - Powrót Minerwy z terrorforma](181021-powrot-minerwy-z-terrorforma.html)

### Chronologiczna

* [181021 - Powrót Minerwy z terrorforma](181021-powrot-minerwy-z-terrorforma.html)

## Projektowanie sesji

### Struktura sesji: Wyścig

* **Strona, potrzeba, sukces, porażka, forma gry**
    * Minerwa Metalia
    * Being x Identity: jej nowa forma, jej nowe miejsce w rzeczywistości
    * Minerwa przebudowuje swoje struktury i przywraca Nutkę, prowadząc do swojego samozniszczenia
    * Minerwa pozostaje z Erwinem jako jego servar
    * Eksploruje możliwości jakie ma jako servar - a jakie straciła jako osoba
    * "moc silnika": 1k6
* **Strona, potrzeba, sukces, porażka, forma gry**
    * Adam Szarjan
    * Having x Understanding: próba opanowania sytuacji i znalezienia miejsca Minerwy w rzeczywistości
    * Minerwa wyłącza swoje struktury i buduje puste psychotroniczne AI
    * Servar ma świadome AI
    * Shadowuje Minerwę i próbuje dojść do tego czym ona jest
    * "moc silnika": 1k6
* **Strona, potrzeba, sukces, porażka, forma gry**
    * Pięknotka Diakon
    * "moc silnika": 1k6
* **Czemu gracze muszą w to wejść?**
    * Erwin i Adam mogą stracić relację co Pięknotka dokładnie wie
    * Minerwa zachowuje się bardzo chaotycznie
* **Trigger?**
    * Pięknotka dowiaduje się od Adama, że servar chyba został ukradziony
* **Okrążenia**
    * Lap 0: w gabinecie Adeli
        * Minerwa: dostanie makijaż / coś pięknego
        * Adam: Minerwa odebrana jako potwór lub dziwadło
    * Lap 1: ucieczka
        * Minerwa: zdobycie źródła energii na Trzęsawisku Zjawosztup
        * Adam: wyczerpanie energii Minerwy, nie ma gdzie i dokąd uciec
    * Lap 2: przejście się po Miasteczku
        * Minerwa: będzie w stanie normalnie funkcjonować w Miasteczku (kupić coś, zachwycić się)
        * Adam: Minerwa nie ma niczego dla siebie; jest maszyną
    * Lap 3: psychotronika, w lab Erwina
        * Minerwa: istnieje możliwość znalezienia Nutki / zbudowanie osobowości
        * Adam: istnieje możliwość przebudowanie i edytowanie osobowości samej Minerwy
    * Lap 4: walka servara
        * Minerwa: zwycięstwo przeciwko wszystkim i wszystkiemu
        * Adam: wykazanie, że Minerwa ma moce terrorforma

### Hasła i uruchomienie

* Ż: (kontekst otoczenia) brak
* Ż: (element niepasujący) Lilia pomaga Minerwie przeciwko Adamowi
* Ż: (przeszłość, kontekst) Mroczny czyn Galiliena w przeszłości, mroczny czyn Szarjanów w przeszłości

### Dark Future

1. Minerwa niszczy swoją osobowość i albo sprowadza Nutkę albo niszczy jakąkolwiek psychotronikę
2. Relacja Adama i Erwina ulega bardzo dużemu pogorszeniu

Pogoda, otoczenie:

* Fatalna pogoda, obniżona widoczność
* Sam Zjawosztup to Trudny Teren, na którym są endemiczne Echa i problemy.

## Potencjalne pytania historyczne

* brak

## Punkt zerowy

### Postacie

### Opis sytuacji

## Misja właściwa

**SCENA:**: 1. Wyścig o postrzegane człowieczeństwo Minerwy (salon kosmetyczny)

Wiadomość od Adama po hipernecie. Minerwa była wyłączona i nagle zniknęła. Pięknotka wzięła klucz Erwina i poszła do Miasteczka, znaleźć Minerwę używając lokalizatora. Lokalizator wskazuje na gabinet kosmetyczny Adeli. Pięknotka wzięła taksówkę i pojechała. Jest tam.

Dojechała. Faktycznie, servar jest tam. Adela jak zobaczyła Pięknotkę, nastroszyła się jak kotka. Ma jednego klienta - taki masywny gość z irokezem; robi sobie pazurki. Pięknotka akceptuje domniemanie niewinności, ale ciężko będzie. Adam skomunikował się z Pięknotką; czy ma zdobyć dane z hotelu. Pięknotka powiedziała żeby zdobył. Czy potrzebuje Adama? Nie.

Klient poszedł. Adela spojrzała na Pięknotkę. Co ona tu robi. Po krótkiej dyskusji o ukradzionych servarach Adela powiedziała, że jej klientka jest na zapleczu. I z zaplecza wyszła Minerwa prosząc o makijaż. Adela dowiedziała się od Pięknotki, że Minerwa jest TYLKO servarem; pod spodem niczego nie ma. Adela w szoku. A Minerwa jest zdeterminowana, by zrobić sobie makijaż.

Pięknotka zaproponowała Minerwie makijaż jej - mistrzyni, nie uczennicy (Adeli). Adela nie wie do końca jak zrobić makijaż; poprosiła Minerwę o chwilę. Minerwa nie jest w stanie zrobić sobie sama makijażu - nie ma precyzji w ruchach. Wyraźnie jest zamyślona.

Pięknotka poprosiła Adama, by ten kupił farbki do metalu. Makijaż, który da się zrobić Minerwie. Wróciła Adela. Pięknotka widzi, że Adela na nią popatruje z ukosa, ale daje z siebie wszystko. Poszło jej nieźle (11/k20), Minerwa jest wyraźnie zadowolona. Zapłaciła kontem Erwina.

Wszedł Adam. Minerwa poinformowała go, że nie jest zainteresowana tysiącem testów i nie wróci z nim. Wymierzyła weń broń i kazała mu wyjść. Adam poszedł. Pięknotka odprowadziła Minerwę do domu Erwina.

* "Przy całej mojej sympatii do Erwina nic a nic nie usprawnił psychotroniki tego pancerza serwomotorycznego. Nie potrafił." - Minerwa, lekko zirytowana.
* "Podejrzewam, że w domu Erwina będą moje narzędzia. Nie wyrzuciłby ich, z sentymentu." - Minerwa
* "Gdy ja byłam żywa, Diakonki były bardziej proaktywne." - Minerwa, o Pięknotce i Erwinie
* "O, nie ma narzędzi, za to jest alkohol. Tyle jest warty sentyment..." - Minerwa, amused

Minerwa poszła się naładować. Pięknotka poszła do salonu, poogarniała co trzeba, sprawdziła co z Erwinem (nieprzytomny ale stabilny) itp. Pięknotka przygląda się farbkom do metalu które kupił jej Adam. Pięknotka poprosiła Adama o fotkę Minerwy, nawet kilka. Doszła do tego, że Minerwa miała ulubione kolory - biel i błękit. Pięknotka zdecydowała się na usprawnienie działań Adeli. Zwłaszcza odkąd Pięknotka wie, że Minerwa miała kiedyś na nazwisko DIAKON.

(Typowy: 10, 3, 3) = Sukces. +1 do toru Pięknotki.

Minerwa się obudziła. She warmed up to Pięknotka. Pięknotka próbuje dojść do tego o co chodzi Minerwie (Typowy: 9,3,3=S; +1 tor). Minerwa jest przerażona, samotna, nie wie co się dzieje i nadrabia miną; szuka swojego miejsca i nie wie jak do tego podejść. Powinna być martwa a nie jest. Więc szuka. Jest... sama. I nie czuje się żywa. Ciało tego power suita jest... niesprawne z perspektywy Minerwy - nie czuje dotyku, łaskotek czy miłych odczuć. Czuje ból lub sygnał. Ale zdaniem Minerwy to kwestia psychotroniki.

Minerwa zawsze była kierowana przez "science as an art" oraz dążenie do autonomii. Drażni Minerwę niesprawność palców i brak precyzji; najpierw jednak musi rozejrzeć się w sprzęcie Erwina i zobaczyć co się da zrobić. Spodobał jej się akumulator. Tymczasem Adam skomunikował się z Pięknotką i powiedział, że Minerwa nie jest człowiekiem - co gorsza, wewnątrz psychotroniki jest aktywator terrorforma...

Adam na wszelki wypadek będzie spał i pilnował w Miasteczku by Minerwa nie wpadła w berserk.

Pięknotka stwierdziła, że idzie spać w łóżku Erwina. Ale wpierw - próba oswojenia Minerwy (Typowy: 9,3,3: SS = Minerwa wyczuje Adama; +1 tor Pięknotki)

* Tor Pięknotki: 4
* Tor Minerwy: 4
* Tor Adama: 1
* Ż: 2
* K: 1

**SCENA:**: 2. Wyścig o autonomię Minerwy (ucieczka na Trzęsawisko) (22:12)

Minerwa nie próbowała dyskretnie się wycofać gdy Adam pilnował i Pięknotka spała. Ona użyła katapulty by lecieć na Trzęsawisko i znokautować Adama. Pięknotka spróbowała się obudzić by ją zatrzymać (Trudny: 6-3-7: SS: Minerwa ma mało czasu i nie wie o tym). Poleciała i wyłączyła Adama z akcji. Pięknotka szybko wskoczyła w swój servar i leci za Minerwą. Pięknotka szybko wezwała hipernetem jakąś pomoc dla niego i poleciała. Poprosiła też szybko Lilię o zajęciem się domem Erwina. "Nie mam czasu wyjaśnić, napraw dom Erwina!"

Trzeba dogonić Minerwę. Przed Trzęsawiskiem (Trudny: 5-3-7: P=Znaleziona Minerwa wyładowana na samym Trzęsawisku). Pięknotka poprosiła Adama by przyleciał awianem i przeniósł nim Minerwę. Adam... nie wdając się w specyfikę, zgodził się. Pięknotka ostrzegła go o Trzęsawisku i kazała pilnować emocji. (9,3,3=S; Adam wydobył Minerwę mimo ataku ze strony Trzęsawiska; +1 tor Adama). W ogniu tego wszystkiego Pięknotka też wyślizgnęła się z Trzęsawiska.

Adam i Lilia w domu Erwina z Pięknotką. No i jedna wyładowana Minerwa. Pięknotka wywaliła Lilię. Adam spytał Pięknotkę, czy ta ma jakieś pomysły - on ma. Wyłączenie psychotroniki. Zdaniem Adama Minerwa nie jest już osobą - to tylko duch. Pięknotka się na to zdecydowanie nie zgadza. Pięknotka naciska na Adama, by ten zmienił zdanie - on NIE chce jej zabijać, ale nie wierzy, że ona może przetrwać w tym stanie. Jest terrorformem, ale ukrytym pod psychotroniką. (8-3-6=SS. Sukces, ALE Minerwa słyszała tą rozmowę (+1 tor Minerwy)).

* Tor Pięknotki: 6
* Tor Minerwy: 7
* Tor Adama: 8
* Ż: 5
* K: 5

**SCENA:**: 3. Wyścig o autonomię Minerwy, v2 (rekonstrukcja psychotroniczna) (22:34)

* "Bateria nie zadziałała tak jak powinna. Nie mogę zbierać energii z Trzęsawiska - Erwin się pomylił" - zirytowana Minerwa
* "Może i lepiej" - Pięknotka, wzdychając - "Wiesz, że to było bardzo niebezpieczne?"

Minerwa zaprojektowała narzędzia pozwalające na edycję psychotroniczną. Ale nie jest dość delikatna by coś z tym zrobić. Pięknotka powiedziała Minerwie o terrorformie i o tym, jak ona się tu znalazła - i że chciałaby wyprojektować terrorforma z psychotroniki Minerwy. Adam powiedział, że on jest technomantą - może pomóc Minerwie we wzmocnieniu poradności palców. Minerwa zauważyła, że Adam chciałby ją usunąć. Ją i Nutkę. Adam się zgodził.

Pięknotka zaatakowała Adama - życie jest piękne, nawet to cholerne bagno. Ona nie zgadza się na usunięcie czegoś takiego. Minerwa ma drugą szansę. I Pięknotka idzie po sercu tego czym jest - chce pokazać Minerwie i Adamowi to, jak piękna jest obecność Minerwy. Wyłączyć terrorforma i doprowadzić do cudu jakim jest życie Minerwy. No i Erwin się tak ucieszy... (11,3,1=S:+1P,-1A).

Przerwało im rozważania pukanie do drzwi. Adela. Nie ucieszyła się z widoku Pięknotki - przyszła do Minerwy. Chce, by Minerwa poczuła się lepiej i zrobić jej makijaż. Adela uważa, że ostatnio nie poszło jej tak dobrze jak chciała i przyszła poprawić swoją pracę. (7,3,6=S). +2 tor Pięknotki, wygrała z Pięknotką. Minerwa wyraźnie poweselała, Adam burczał, że marnowanie czasu. Adela wyszła z triumfem i poczuciem dobrze zrobionej roboty.

Adam pomógł Minerwie w zbudowaniu delikatnych dłoni. Minerwa jest w coraz lepszym humorze, choć wyraźnie nie podoba jej się to, że jest servarem. Pięknotka podsłuchała rozmowę - Minerwa chce przywrócenia Nutki, Adam chce zniszczenia Nutki i Minerwy, bo terrorform.

Minerwa skupiła się na analizie psychotroniki (7,3,6=S). Terrorform tam jest, ale jest pod kontrolą; ma aspekty fizyczne, ale Minerwa ma głos dominujący. To samo Nutka. Nie trzeba niszczyć psychotroniki (-2 Adam). Pozostaje pytanie - Nutka czy Minerwa. Lub obie. Minerwa jest też zirytowana - nie może usprawnić psychotroniki, bo może obudzić terrorforma. Nie będzie "człowiekiem". Nie będzie czuć jak osoba (+1 tor Minerwy). Jest ryzyko aktywacji...

Adam i Pięknotka powiedzieli Minerwie, że wolą ją od Nutki. Nutka jest jak dziecko. Lepiej niech Minerwa zostanie. Oboje próbowali Minerwę przekonać. Jeśli ktoś ma zostać, niech to będzie Minerwa (11,3,1=S). Czarodziejka w lekkiej depresji powiedziała, że to rozważy.

* Tor Pięknotki: 10
* Tor Minerwy: 8
* Tor Adama: 5

Post race-up:

* Tor Pięknotki: 11
* Tor Minerwy: 9
* Tor Adama: 11
* Ż: 6
* K: 5

**SCENA:**: Decyzja Minerwy

Pięknotka wzięła Minerwę w środku nocy na dach budynku by z nią porozmawiać. Porozmawiać o pięknie, o możliwościach, o Erwinie. Minerwa powiedziała Pięknotce, że zostanie - by Erwinowi pomóc się wyplątać z tego wszystkiego. Potem może odejść. Zniszczy psychotronikę. Ale wpierw musi pomóc Erwinowi. Minerwa nie życzy nikomu takiego ciała - it is dead inside.

* Ż: 6
* K: 5

## Wpływ na świat

| Kto           | Wolnych | Sumarycznie |
|---------------|---------|-------------|
| Żółw          |   0     |     6       |
| Kić           |   1     |     7       |

Czyli:

* (K): Minerwa NIE zniszczy psychotroniki bez możliwości wpływu ze strony Kić (thread claim) (2)
* (Ż): Terrorform / berserker staje się jedną z osobowości Minerwy (2) (K): Jak długo da się wyłączyć (1)
* (K): Nutka staje się jedną z osobowości Minerwy (2)
* (K): Lilia ten konkretny temat odpuszcza (1), acz Minerwa próbuje dążyć do Erwin x Lilia (2)
* (Ż): Adam bardzo głęboko nie ufa Minerwie (2)

## Streszczenie

Atena i Erwin są w ruinie. Tymczasem servar Erwina wyrwał się na wolność. Okazało się, że Minerwa - dawna miłość Erwina - powróciła. Adam próbował ją zneutralizować a Pięknotka - uratować. Skończyło się na tym, że Minerwa, Nutka i terrorform współistnieją w servarze Erwina, Adama boli głowa a Pięknotka cieszy się, że tymczasowo to nie jej problem. Mistrzyni psychotroniki wróciła.

## Progresja

* Pięknotka Diakon: stosunki Pięknotki i Minerwy się ociepliły
* Minerwa Metalia: trójpersona: Minerwa, Nutka (nieco starsza) i terrorform. Erwin będzie miał niezłe dyskusje.
* Adam Szarjan: bardzo nieufny wobec Minerwy; ogólnie, pilnuje, by nic złego się nie stało
* Erwin Galilien: utracił Nutkę, ale zyskał Nutkę + Minerwę + terrorforma jako swój servar. A był tylko nieprzytomny...

### Frakcji

* 

## Zasługi

* Pięknotka Diakon: oswoiła Minerwę oraz przekonała Adama, by dał jej szansę. Doprowadziła do tego, że Minerwa zdecydowała się zostać.
* Adela Kirys: nieco zaplątana w tą sprawę; dała radę zrobić makijaż servarowi, po czym poprawiła na bardzo udany makijaż (przebijając Pięknotkę)
* Lilia Ursus: grzeczna panienka, naprawiła chatę Erwinowi, po czym - na prośbę Pięknotki - sobie poszła.
* Minerwa Metalia: zastąpiła Nutkę jako servar Erwina. Próbowała sprawdzić granicę autonomii i człowieczeństwa. Nieszczęśliwa w formie servara, ale zostanie - by Erwin nie był sam.
* Adam Szarjan: dążył do zniszczenia psychotroniki Minerwy; bał się terrorforma i chciał, by Erwin oraz wszyscy dookoła byli bezpieczni.

## Plany

* Minerwa Metalia: musi doprowadzić do tego, by Lilia i Erwin zostali parą. Potem może zniszczyć swoją psychotronikę.
* Minerwa Metalia: Minerwa NIE zniszczy psychotroniki bez możliwości wpływu ze strony Kić (thread claim)

### Frakcji

* 

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Pustogor
                                1. Inkubator Szamsar: miejsce, gdzie znajduje się salon kosmetyczny Adeli. Nieco niegodne miejsce, ale na to Adelę stać.
                                1. Miasteczko: mieszka tam Erwin. Teraz z Minerwą.
                        1. Trzęsawisko Zjawosztup: doleciała tam Minerwa, po czym zabrakło jej energii. Wyciągnął ją Adam awianem.

## Czas

* Opóźnienie: 2
* Dni: 3

## Narzędzia MG

### Budowa sesji

**SCENA:**: Adam dowiaduje się gdzie jest Erwin; Lilia zostaje zmuszona do współpracy z Adamem.
**SCENA:**: Galilien w Miasteczku opuścił Szczelinę; ścina się z Adamem i ewakuuje na Trzęsawisko. Lilia leci za nim.
**SCENA:**: Dewastacja w walce między Nutką (pełna furia) i Adamem prowadzi do przyciągnięcia Wiktora. All-out-3-way-war. Wygrywa Adam, Galilien ucieka.
**SCENA:**: Erwin i Nutka uciekają przez Mglistą Ścieżkę. Adam nie dał rady ich zatrzymać. Wiktor walczy z Adamem i zmusza go do ewakuacji.

### Wykorzystana mechanika

1810

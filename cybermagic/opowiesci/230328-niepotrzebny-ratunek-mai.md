---
layout: cybermagic-konspekt
title: "Niepotrzebny ratunek Mai"
threads: waśnie-samszar-verlen
gm: żółw
players: kić, anadia, kolega-kamil
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [230314 - Brudna konkurencja w Arachnoziem](230314-brudna-konkurencja-w-arachnoziem)

### Chronologiczna

* [230314 - Brudna konkurencja w Arachnoziem](230314-brudna-konkurencja-w-arachnoziem)

## Plan sesji
### Theme & Vision & Dilemma

* Nemesea "Believe"
    * "Take a good look at what I've become | I don't need you to be proud | It took my strength to unlock my dreams"
* CEL: pokazanie, że arystokraci Aurum ogólnie mają pewien problem - nie żyją normalnie
    * Po spotkaniu ze Ścigaczem Viorika uaktualniła systemy defensywne
    * Mai tak bardzo zależy na tym by nie przegrać i by móc wygrać turniej, że jest skłonna spać w koszarach
        * nie miała jak zostawić wiadomości by nie zdążyli jej znaleźć
* DYLEMAT
    * wycofać Maję oficjalnie i narazić ją na śmieszność (i siebie) CZY próbować zrobić to inaczej?

### Co się stało i co wiemy

* KTO (gracze)
    * Fiona Szarstasz to eks-verlenlandrynka. 
        * Na terenie Verlenlandu popełniła przestępstwa które uległy przedawnieniu
            * Q: jakie? -> ujawniała WSZYSTKO. Białe kłamstwa, intrygi itp.
        * Znalazła miejsce na terenie Samszarów, gdzie została nawet opiekunką Misteriona dzięki swoim unikalnym umiejętnościom
            * Q: jakim? -> świetnie wykrywa kłamstwa ale też świetnie kłamie
        * ADVANTAGES: local, social, świetnie wykrywa kłamstwa, reputacja osoby która to umie
    * Karolinus Samszar jest magiem rodu Samszar, młodym (20) paniczem który pojechał na wczasy. Pomóc rodzinie Fiony.
        * Q: czemu? -> miłość jest ślepa
        * Q: jaką ma dominantę magiczną? -> zmiana bioformy, zmiana formy biologicznej
        * ADVANTAGES: tien, bioforma / biomagia
    * GRUBY ŚCIGACZ jest eks-ŁZą militarną typu Raptor, rodem z Orbitera, imieniem "Szybka Strzała" (nazwany przez Karolinusa)
        * Prawie zniszczona ŁZa, uratowana przez ród Samszar i zainwestowana.
        * ADVANTAGES: walka elektroniczna, walka, skan, transport, hidden
        * Q: czemu wygląda idiotycznie? -> jest pomalowany w kwiatki
* IDEA
    * tydzień później, zniknęła kuzynka Karolinusa. To na pewno wina Verlenów - okazuje się, że chatowała z Romeo Verlenem i przerzucali się obelgami.
        * trackerzy Samszarów skierowali Karolinusa do miasteczka VirtuFortis
    * miasteczko VirtuFortis, nad rzeką
        * import:  
            * metals, minerals, and lumber from other regions for its industries and construction purposes
            * tech components and electronic parts are imported to support the local virtual reality industry
            * due to the limited agricultural land in the mountainous region, the city imports a portion of its food and daily consumables
            * luxury goods
        * export: 
            * Specialized education and training services; Aegis Academy.
            * VR hardware and software: The city's skilled developers and engineers produce cutting-edge VR hardware and software solutions (zamiast TAI)
            * Virtual reality content
        * zbliża się wielki turniej Supreme Missionforce bez wsparcia TAI


### Co się stanie (what will happen)

* S01
    * Prześlizgnięcie się ścigaczem koło Moguvara ( long, serpentine body covered in iridescent, armored scales that shimmer in shades of deep blues and greens, reflecting the hues of the ocean, create gale-force wind, long, barbed tail is tipped with a sharp, venomous stinger)
    * Wejście do VirtuFortis
* S02
    * Miasto jest nieduże, przeładowane ludzie, bardzo dużo wojska. Dowodzi Viorika Verlen. Niestety.
    * Ale gdzie może być Maja?
* S03
    * 

### Sukces graczy (when you win)

* rozwiązanie problemu

## Sesja - analiza

### Fiszki

* Karolinus Samszar
    * Starsza od niego dziewczyna [imię jakiejś postaci?], w której się zakochał młody panicz upokorzyła go przy wszystkich wyśmiewając się z niego. On sparaliżowany nie zareagował paląc się ze wstydu. Czekał długo na dogodny moment tworząc scenariusze w głowie, aby wtrącić tylko jedno słowo podczas jej ważnego uroczystego przemówienia, które zamieniło się w pośmiewisko.
    * Gdy przyjaciel [imię przyjaciela?] potrzebował pomocy, Karolinus rzucił się na pomoc pomagając mu uniknięcia pobicia. Niestety ten sam przyjaciel nie znalazł czasu dla Karolinusa, gdy ten potrzebował pomocy przy rodzinnym projekcie.
    * Gdy ojciec się popłakał mówiąc, że Karolinus nie wyjdzie na ludzi od tamtej pory Karolinus uważa, aby nie dawać ojcu powodów do takiego myślenia.
* Maja Samszar
    * młodziutka czarodziejka (rok młodsza od Eleny) uwielbiająca różnorakie potrawy i gotowanie.
    * ENCAO:  ++-0- |Moralizatorska| VALS: Stimulation, Conformity >> Power| DRIVE: Rite of passage
    * styl: UR z elementami WB; lekko emocjonalnie niestabilna arystokratka która UWIELBIA wszystkich pouczać i dużo mówi. DUŻO.
* Romeo Verlen
    * młody mag (rok starszy od Eleny) skupiający się na ostrej rywalizacji i głupich dowcipach. Świetny w vircie i grach. Perfekcyjny w zwielokatnianiu swojej uwagi.
    * ENCAO:  +00-+ |Brutalny, grubiański;;Wszystko jest rywalizacją;;Innowacyjny| VALS: Face, Tradition >> Benevolence| DRIVE: Red Queen Race
    * styl: BW; uporządkowany wobec systemu ale jako Verlen ceni ruchy wbrew systemowi które są zrobione dla wielkiej pasji. Czyli ceni RU.
* Amelia Tirinias
    * agentka z oddziału Supreme Missionforce Romeo
    * ENCAO:  +0-++ |Chce TERAZ i nie będzie czekać;;Zawsze pełen energii wśród innych ludzi;;Wyznawca teorii spiskowych| VALS: Universalism, Self-direction >> Tradition, Security| DRIVE: Niszczenie hipokrytycznego systemu (jest lepsza niż magowie)
    * styl: hellfire angel
* Sonia Neriter
    * agentka z oddziału Supreme Missionforce Mai
    * ENCAO:  ++0-0 |Indywidualistyczna;;Opryskliwa i obcesowa;;Ryzykantka| VALS: Benevolence, Face >> Self-direction| DRIVE: Drama
    * styl: Naga the Serpent
 

* .Nadia
    * ENCAO:  -+--0 |Tendencje do podkradania;;Cichy, ma mało energii| VALS: Tradition, Security >> Achievement| DRIVE: Sprawiedliwość
* .Szymon
    * ENCAO:  +0-+- |Nie toleruje spokoju i nudy;;Prawi wszystkim morał| VALS: Security, Conformity| DRIVE: Ochraniać słabszych
* .Wojciech
    * ENCAO:  0+-0+ |Niezdyscyplinowany;;Nie cierpi struktur i zasad| VALS: Achievement, Family| DRIVE: Ekstremista
* .Laura 
    * ENCAO:  -+0+- |Melancholijna, patrzy często w przeszłość;;Nie cierpi 'small talk';;Unikająca obrażania kogokolwiek i konfliktów| VALS: Humility, Self-direction| DRIVE: Złota klatka Lauriego
    * styl: cicha, melancholijna, małomówna, przyjazna
* .Armin
    * ENCAO:  +-+-0 |Stanowczy i silny;;Ma skłonności do gniewu;;Brutalny, bezwzględny;;Beznamiętny, o minie z kamienia| VALS: Universalism, Hedonism >> Humility| DRIVE: Niezłomna Forteca
    * styl: silny zdecydowany facet


### Scena Zero - impl

.

### Sesja Właściwa - impl

Tydzień po bohaterskiej ucieczce z Verlenlandu. Maja - kuzynka Karolinusa - została porwana.

* Bonifacy Samszar: "Karolinus, spieprzyłeś to."
* Bonifacy Samszar: "Co Ci przyszło do głowy zaczepiać VERLENÓW? Oni są psychiczni?"
* Karolinus: "Głupie ściganie po mieście. A co?"
* Bonifacy Samszar: "Zniknęła Maja. Jak myślisz DLACZEGO ją porwali?"
* Bonifacy Samszar: "Maja jest porwana od WCZORAJ a Apollo wyraźnie gra na czas."
* Bonifacy Samszar: "Żołnierze Verlenów transportowali poranioną Maję gdzieś do miast"

Karolinus chce pozyskać dowody i nagrania z porwania itp. a Fiona chce to obejrzeć i ocenić. Karolinus kojarzy Maję, bo ciasto. Ale Fiona rozpoznaje Maję bo jest nietypowa + to kwestia życia i śmierci.

Tr Z (nagrania + nagrania archiwalne + Szybka Strzała jako procesor porównywawczy) +2:

* V: pierwsze co jest ciekawe
    * Maja wymknęła się w środku nocy. Rekordy pokazują, że wcześniej prosiła o przygotowanie ścigacza itp. Wszystko wygląda jakby sama poleciała i nikt jej nie porywał.
        * Rodzice Mai są z tych... "moja córka jest elegancka, grzeczna i jest prawdziwą arystokratką Aurum." -> "Verlenowie musieli porwać".
* Xz: rodzice Mai konsekwentnie kasują dane które "nie wskazują na elegancję córki".
* V: ...ale udało się coś dowiedzieć. Maja korespondowała dużo z Romeo Verlenem. I to były... flame war.
* X: TAK, dużo wskazuje na to, że Maja próbowała się dostać do VirtuFortis. Tak samo wyraźnie w przeszłych rozmowach (z Romeem) temat Supreme Missionforce był uwzględniony. I chyba grali przeciwko sobie.

Karolinus łączy się z Romeem. Chce z nim rozmawiać. Romeo jest młody (16).

* Romeo Verlen: tien Samszar, co za zaszczyt, przestałeś uciekać? 
* Karolinus: zaginęła Maja. Wiesz coś o tym. Nie mam z nią kontaktu.
* Romeo: Ty nigdy nie chciałeś mieć z nią kontaktu.
* Karolinus: a skąd wiesz?
* Romeo: Nie wiem (nieszczerze)
* Karolinus: Powiedz co wiesz
* Romeo: Maja jest bezpieczna. Nic jej nie jest. Włos jej z głowy nie spadnie.
* Karolinus: A gdzie jest?
* Romeo: Niedaleko mnie. W VirtuFortis.
* Karolinus: Dobra już jedziemy
* Romeo: Co? Nie!
* Karolinus: (rozłączam się)

Szybka Strzała gotowa i trzeba dostać się do Verlenlandu. Jadą normalnie - ani się nie anonsują ani się nie ukrywają. Przebycie drogi przez tereny Samszarów - zero problemów.

Karolinus wspomaga się magią - czy przelot Mai był normalny, czy to ucieczka, jak się czuła, czy ktoś z nią był itp. Samszar (duchy) + sentisieć.

Tr Z (znajomość Mai, terenu i sentisieci) M +2 +3Ob:

* X: Czując Twój niepokój za tobą formują się 'echa emocjonalne' i lecą za Tobą w głąb Verlenlandu.
* Ob: Karolinus ma dość przeżyć. Ledwo wrócił z wyprawy i już kolejna. Gdy znajdzie Maję chciałby odesłać od razu do domu, do rodziców
    * efemeryczny oddział. Zaatakować, odzyskać Maję, wrócić. Wszystkie elementy oddziału wyglądają jak Maja.
* Xm: duchy tego miejsca powstają by odzyskać Maję. Duża siła ognia. Wszystko wygląda jak Maja na ścigaczach ale widmowa
* V: "eteryczne Maje" lecą w kierunku na Maję po sympatii WIĘC Karolinus wie dokładnie gdzie lecieć.
* Ob: całość uczuć i emocji spowodowało naprawdę spory oddział efemeryd. Kilkaset bytów wyglądających jak Maja na ścigaczach. Część z nich o własnościach anomalnych. Czyli mamy oddział atakujący Verlenland.
* V: INFO: Maja leciała sama. Była podekscytowana. Leciała szybko. To było... to była próba oddalenia się od domu. Ona leciała z celem na miejsce. Miała w głowie obraz Romea i chciała go "pokonać".
* V: Oddział jest spory i wymaga pełnej koncentracji Vioriki. Viorika jest w stanie to wygrać, ale musi ostro działać i manewrować.
* Vz: nawiązany kontakt z Mają ORAZ znajomość lokalizacji Mai.
* X: W opinii Verlenów a zwłaszcza Vioriki to jest zemsta za pomnik który zamówiła Viorika. Czyli to jest elegancka próba "teraz TY jesteś moim nemesis".
* Vz: Verlenowie to rozumieją. Verlenowie wiedzą, że młodość ma swoje prawa. Jak długo nikomu się nic poważnego nie stanie, spoko. To jest spoko Karolinus.

Na szczęście obroną VirtuFortis dowodzi Viorika Verlen. Ta Viorika, która już ma zatarg z Karolinusem. Viorika BĘDZIE CHCIAŁA porozmawiać z Karolinusem. Teraz na pewno.

Szybka Strzała SZYBKO włącza maskowanie, odbija się od trasy Maj. Leci dyskretniej. Wybiera mniej bezpieczne drogi którymi normalnie by się nie leciało.

Tr Z (Maje XD) +4 +3Og (potwory Verlenlandu) +3Ow:

* Vr: Strzała skutecznie daje się nie zauważyć
* X: Strzała skutecznie leci, ale musiała odbić bo Viorika i jej oddział.
* V: Strzale udało się owego potwora wymanewrować. Ale musi przejść.
* (+3Or za ekstra drony Strzały): Vz: potwór skupiał się na Strzale ale po drodze skupił się na jednej z Maj; Strzała przeleciała bokiem. Viorika przeleciała koło stwora
* V: Strzała maskuje się i wlatuje blisko innych pojazdów. W ten sposób Strzała dotarła do VirtuFortis.

A nasz Karolinus może nawiązać połączenie z Mają.

* Maja: "Karolinus? Jak się do mnie połączyłeś?"
* Karolinus: "Mało ważne. Porwana? Czy ok? Co robisz?"
* Maja: "Wszystko ok, jestem bezpieczna. A czemu pytasz?"
* Karolinus: "Rodzice się martwią, powiesz im? Jesteś pobita i porwana (informacja). Zabrać Cię do domu czy chcesz zostać?"
* Maja: "Ogólnie? To znaczy, porwali mnie, oczywiście. (Fiona wie że Maja ściemnia, daje znać Karolinusowi)."
    * F: Spytaj czy przyjechała na konkurs bez wiedzy rodziców lub czy uciekła z domu i nie wróci.
* K: "Przyjechałaś na konkurs?"
* M: "Porwali mnie!"
* K: "Odzyskać Cię czy okup?"

Karolinus naciska na Maję. Niech się zdeklaruje czego chce. Zostaje czy znika?

Tr Z (Fiona i jej umiejętności) +2:

* Vr: Maja ma ZATARG z Romeem. I Maja ma zespół w Supreme Missionforce. I teraz z zespołem ma okazję pokazać Romeo kto jest lepszy. I gdyby tu nie przyjechała, jej zespół przegrałby walkowerem. Musiała przyjechać. Rodzice nie wiedzą i nie aprobują.
* X: Rodzice Mai nigdy w życiu nie zaakceptują tego, że Maja jest tutaj. Ona nie powinna, nie do tego jest i nie tym powinna się zajmować.
* X: Maja stoi przed dylematem: ALBO zdradzi swój zespół i wróci do domu ALBO będą problemy w domu ale spróbuje zmierzyć się z Romeem. To jest duża sprawa w kontekście Supreme Missionforce i jej znajomości. Maja się nie zadeklaruje
    * turniej trwa jeszcze tydzień.
* V: Plan Mai jest taki godny nastolatki
    * uciekła z domu i chciała przylecieć do VirtuFortis dyskretnie
    * spotkała po drodze potwora i się rozbiła. Uratowali ją Verlenowie, dokładniej: Viorika (dlatego pokiereszowana)
    * Viorika nie była zwolenniczką planu Mai. Romeo ją przekonał.
        * dlatego Apollo (wspiera Romeo) chce jakiegoś idiotycznego okupu. Np. 1000 piór gęsich wyrwanych gdy rosa jest na kurniku czy coś. By trwało. 
            * I chcieli zwalić że to przez Karolinusa.
* X: Karolinus skutecznie przekonał Maję, żeby ona robiła swoje (turniejowała) a on po tygodniu ją stąd odbierze. 
    * ALE cios reputacyjny u Samszarów jest duży że "zaatakował Verlenów i co gorsza nic z tego nie było, tydzień się z nimi naparzałeś i nic"
        * u Verlenów reputacja++ -> ma dobre serce, jest waleczny, jest ryzykantem, SPOKO.
        * u Samszarów reputacja-- -> zamiast porozmawiać jak normalny człowiek zaatakował i oddali Maję z litości. No kurczę. Ale osiągnął cel, Maja oddana. Ale _jak_.
            * Fiona z Verlenlandu ma na Karolinusa zły wpływ. Karolinus traci Fionę.

## Streszczenie

Tydzień po ucieczce z Verlenlandu, Maja, kuzynka Karolinusa, zostaje porwana. Karolinus próbuje zdobyć informacje i nagrania z porwania. Odkrywa, że Maja miała kontakt z Romeo Verlenem, a jej rodzice kasują informacje o nieeleganckim zachowaniu córki. Karolinus łączy się z Romeem, który twierdzi, że Maja jest bezpieczna w mieście VirtuFortis (i ogólnie jest OK). Karolinus wyrusza tam ze swoim zespołem na pokładzie Szybkiej Strzały. 

Niestety, podczas Paradoksu udało się przypadkowo Karolinusowi zaatakować duchami o kształcie Mai Verlenland. Docierają w końcu do VirtuFortis i Karolinus kontaktuje się z Mają. Maja twierdzi, że jest porwana, ale Fiona wyczuwa, że Maja ściemnia. Okazuje się, że Maja uciekła z domu i przyjechała na turniej Supreme Missionforce, żeby zmierzyć się z Romeem. Karolinus przekonuje Maję, żeby wzięła udział w turnieju, a on odbierze ją po tygodniu. Reputacja Karolinusa wzrasta wśród Verlenów, ale spada wśród Samszarów. Fiona zostaje uważana za zły wpływ na Karolinusa.

## Progresja

* Maja Samszar: rodzice Mai są z tych... "moja córka jest elegancka, grzeczna i jest prawdziwą arystokratką Aurum." więc "Verlenowie musieli porwać".

### Frakcji

* .

## Zasługi

* Karolinus Samszar: odkrył prawdę stojącą za zniknięciem Mai oraz przekonanie jej do wzięcia udziału w turnieju Supreme Missionforce, jednocześnie dbając o jej bezpieczeństwo i ustalając plan na jej powrót do domu po zakończeniu imprezy.
* Fiona Szarstasz: pomogła w odkryciu sytuacji Mai i w tym, że Maja ściemnia. Dawała dobre rady Karolinusowi jak działać w Verlenlandzie. I za całokształt została wycofana od Karolinusa bo ma na niego zły wpływ XD.
* AJA Szybka Strzała: skuteczne unikanie wykrycia, dzięki czemu Karolinus mógł dostać się do VirtuFortis. Wymanewrowała potwora i Viorikę (która na nią nie polowała)
* Bonifacy Samszar: wyraził swoje niezadowolenie wobec Karolinusa za zaczepianie Verlenów i że przez to porwali Maję XD. Dał mu dostęp do kamer i skłonił go do odbicia Mai.
* Romeo Verlen: przekonał Viorikę do utrzymania Mai na czas Supreme Missionforce, przekonał Apolla do idiotycznego okupu by kupić czas i wygadał się niefortunnie Karolinusowi że temat z Mają to nie jest proste porwanie.
* Viorika Verlen: uratowała Maję Samszar po jej spotkaniu z potworem oraz gdy Karolinus rozpoczął "inwazję efemerycznych Maj" wymanewrowała je na potwora i obroniła VirtuFortis.
* Apollo Verlen: przekonany przez Romeo, poszedł na zrobienie dowcipu Samszarom. Okup za Maję? "1001 gęsich piór o odpowiednim odcieniu". Oczywiście, "bo Karolinus".
* Maja Samszar: ma ZATARG z Romeem (ma zespół w Supreme Missionforce) i pojechała walczyć z nim w VirtuFortis uciekając przed rodzicami. Karolinus jej pomógł, co się mu u Mai bardzo zapisała na plus.

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Aurum
                        1. Verlenland
                            1. VirtuFortis
                                1. Akademia VR Aegis: arts of combat, strategy, and monster hunting, with virtual reality
                                1. Wielki Plac Miraży: potężny hiperwizualizator; tam odbywa się _cast_ Supreme Missionforce
                                1. Stadion Sportowy: tymczasowe zakwaterowania pop-up; takie mrówkowcowe
                                1. Bazarek Lokalny: ogromny bazar z jedzeniem i dobrami i virtem
                                1. Bastion Przyrzeczny: fortified tower situated at the convergence of the city's defensive walls and the river. I działka. POTĘŻNE.
                               

## Czas

* Opóźnienie: 8
* Dni: 2

## OTHER

* Gdzie oni śpią?
    * Pop-up accommodations: Temporary structures, such as modular housing units or inflatable habitats, are set up in designated areas around the city. These pop-up accommodations offer a cost-effective and efficient way to provide lodging for the visitors.
    * Home-sharing arrangements: Local residents may offer their spare rooms, basements, or even entire homes for rent to tournament attendees. This not only helps accommodate the influx of visitors but also provides an additional source of income for the residents of VirtuFortis.
    * Communal sleeping halls: Large, open spaces like school gyms, community centers, or warehouses can be temporarily converted into communal sleeping halls, providing visitors with a safe and comfortable place to rest. These halls may be equipped with sleeping mats or cots, along with basic amenities like restrooms and shower facilities.
    * Nearby settlements or camps: If the city's capacity is exceeded, arrangements can be made with nearby towns, villages, or camps to accommodate the overflow of guests. A temporary shuttle service or public transport may be set up to facilitate easy travel between these locations and the city of VirtuFortis.
* 
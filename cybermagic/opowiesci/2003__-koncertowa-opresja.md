---
layout: cybermagic-konspekt
title: "Koncertowa opresja"
threads: ""
gm: kić
players: 
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* 

### Chronologiczna

* 

## Punkt zerowy

Mieliście spokojne życie.
Ok, może jesteście upiorami, ale dotąd nikomu to nie przeszkadzało.
Byliście cenionymi członkami społeczności, którzy w cywilizowany sposób korzystali z dawców krwi.
Byliście szanowani.
Byliście... bo niedawno miejscowy sklepikarz został znaleziony martwy, wyssany do sucha.
Żadne z was tego nie zrobiło. Więc - kto?
Co zrobicie aby odkryć winowajcę?
Czy poradzicie sobie z nagle wrogim nastawieniem sąsiadów i znajomych?
Jak daleko posuniecie się, by wyprostować sytuację?

Sesja oparta na mechanice EZ 2050.


Koncertowa opresja


Stare narzędzia opresji muszą zostać zniszczone!
Jesteście nowymi członkami grupy Liberitas.
Właśnie dostaliście swoje pierwsze zadanie - macie zapobiec imprezie, na której ludziom robi się pranie mózgu aby byli posłuszni.
Wolność dla ludzi!
Nigdy więcej kontroli!
Potwór kontrolujący umysły musi zostać pokonany!
Od tego zależy wasze pełne członkostwo!

Stare narzędzia opresji muszą zostać zniszczone!
Jesteście nowymi członkami grupy Liberitas.
Właśnie dostaliście swoje pierwsze zadanie - macie zapobiec imprezie, na której ludziom robi się pranie mózgu aby byli posłuszni.
Wolność dla ludzi!
Nigdy więcej kontroli!
Potwór kontrolujący umysły musi zostać pokonany!
Od tego zależy wasze pełne członkostwo!

### Postacie

* Sasza Sorkarow - biostymulator marzący, by mieć swobodę dla swoich badań
* Chlor - puryfikator dążący do harmonii moy i ludzi
* Huan - inkwizytor pragnący przywrócić stare, dobre czasy
* Stefan Lubryk - szef lokalnej komórki Liberatis
* Ataienne - AI pragnąca po prostu dać koncert

### Opis sytuacji

## Misja właściwa

### Scena 0

Ataienne budzi trzech strażników (ludzkich).
Coś złego się dzieje, ludzie zmieniają się w owady. Wezwała już magów, ale zanim dotrą, ktoś musi opanować sytuację.




### Sesja właściwa



**Sprawdzenie Torów** ():

* .

**Epilog**:

* .

## Streszczenie



## Progresja

* .

### Frakcji

* .

## Zasługi

* 

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Astoria


## Czas

* Opóźnienie: 
* Dni: 

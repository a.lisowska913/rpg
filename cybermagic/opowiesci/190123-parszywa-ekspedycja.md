---
layout: cybermagic-konspekt
title:  "Parszywa ekspedycja"
threads: nemesis-pieknotki
gm: kić
players: x1, x2, x3, x4, x5, x6
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [190120 - Nowa Minerwa w nowym świecie](190120-nowa-minerwa-w-nowym-swiecie)

### Chronologiczna

* [181230 - Uwięzienie Saitaera](181230-uwiezienie-saitaera)

## Projektowanie sesji

### Pytania sesji

* Czy uda się odzyskać AI Persefona i ujść z życiem.

### Struktura sesji: Frakcje

* Kornel: przekonać Minerwę, by pojechała z nim
* Erwin: pozostać z Pięknotką
* Kasjopea: zobaczyć koszmar Minerwy
* Minerwa: znaleźć dla siebie miejsce. Znowu.

DARK FUTURE: Cała ekspedycja pozostaje na ASD Grazionusz.

## Punkt zerowy

Rok 2050.
Niewielki statek kupiecki wyskakuje z przeskoku na swojej standardowej trasie. Tam, gdzie dotąd zawsze była bezpieczna pustka tym razem czeka na niego przesłaniający gwiazdy, ogromny kształt.
Kupiec nie czeka, natychmiast uciekając ponownie w skok.
Wróciwszy do bezpiecznego portu, zdaje raport ze spotkania i przekazuje nagrania.
Nie mając o tym pojęcia, statek kupiecki natknął się na dryfujący w przestrzeni ASD Grazoniusz.
Kiedyś przejęty przez pasożytnicze bóstwo, dziś statek widmo. Piekielnie niebezpieczny, ale wciąż mogą tam znajdować się skarby.
Niestety, nikt przy zdrowych zmysłach nie podejmie się misji infiltracyjnej.
Ale od czego są skazańcy...

### Postacie

* Andrzej XXX - Naukowiec pozbawiony etyki. Skazany za nieetyczne eksperymenty. Jego zdaniem nauka przekracza wszystko. Chce się dowiedzieć jak najwięcej o magii i po prostu musi wiedzieć, czym są uśpieni bogowie. Doskonały analityk energii magicznych, potrafi wyczuwać ich pływy.
* Steve Reglass - oszust, któremu akcja wybuchła w twarz i doszło do strzelaniny. Przestępca - gentelman, który chce oczyscić swoje akta i przy okazji się wzbogacić. Wygadany, zawsze znajdzie nić porozumienia i oczaruje rozmówcę. Spec od ładunków wybuchowych.
* Eliot XXX - Mechanik, który przez niedopatrzenie doprowadził do śmierci pilota mecha. Pragnie zadośćuczynik swojemu grzechowi. Doskonały mehcanik dron.
* John XXX - Najemnik, uwielbiający walkę. Pragnie wywalczyć sobie wolność. Uważa, że "przez walkę ich poznacie". Taktyk i spec od broni ciężkiej. Hazardzista, lubi konforntację. Nie lubi gaduł i nie odrzuca wyzwań.
* Matrix - Medyczka - dilerka. Wpadła podczas rozprowadzania specyfików własnego wynalazku. Ma słabość do błyskotek i poza szansą na wolność ta misja jest szansą zdobycia szalonych próbek. Medyczka polowa i chemiczka specjalizująca się w stymulantach i materiałach wybuchowych.
* Kebab - Pseudo wzięło się stąd, że karierę zaczynał od budki z kebabem. Złodziej, który okradł o jeden dom za dużo i wpadł. Zwinny jak kot, wszędzie wlezie, wszystko ukradnie. Cokolwiek mu się spodoba, będzie jego.

### Opis sytuacji

.

## Misja właściwa

* Zaraz po wyjściu ze śluzy widzą bandę zbliżających się hipisów. "Przyjmijcie Go do siebie! On was ulepszy! On jest cudowny" Wystrzelali wszystkich.

* Saitaer szepcze do Andrzeja. "Mogę dać Ci wiedzę. Całą, jaką zechcesz."

* John wpada do pomieszczenia z którego dochodzi mamrotanie. Za nim Kebab. Mamrocze jakiś człowiek odpiłowujący sobie rękę. "On da mi lepszą, dzięki niemu będę doskonały..." W przerażniu patrzą, jak w miejsce odciętej ręki odrasta ręka technoorganiczna. Kultysta podnosi odciętą kończynę i stwierdza, że szkoda, żeby materia organiczna się zmarnowała... I zaczyna ją jeść. "Chcesz trochę?" - Wyciąga rękę techno z ręką organiczną do Johna. Ten, spanikowany strzela i poważnie rani kultystę. Ten na jego oczach zmienia się w terror forma... Kebab wyciąga Johna z pomieszczenia, którego drzwi się zamykają do wtóru okrzyku dziewczynki. "Uciekaj!" * tak poznają Alicję.

* "Cześć, jestem Alicja i jestem kapitanem tego statku!". - mała, słodka dziewczynka, która pojawiła się znikąd.

* "A może to ona jest AI?" - Zespół. <poker face> - GM

* Saitaer szepcze do Johna. "Nie musisz być taki delikatny..." i pokazuje mu wizję jak John wypada poza statek i kiedy właśnie ma się udusić w próżni, pokrywają go mechanizmy i już mu próżnia nie przeszkadza... Jest potężny i doskonały...

* Eliot słyszy Saitaera "Mogę dać Ci pełną władzę nad dronami. Każdy mechanizm by Cię słuchał. Znałbyś każdy szczegół, każdy trybik..." Eliot: "Nie, dzięki."

* John do Saitaera w kontroli wydobycia: "Daj mi tą broń!" Laser wypada w jego ręce. Potężna broń. 

* John łapie Alicję, wrzuca ją na ramię. Dziecko panikuje. Przybiegają potwory. John zostawia małą reszcie zespołu. Wspólnym wysiłkiem Matrix i Steve'a udaje się ją jakoś uspokoić. Potwory pokonane. Chcą uciekać.

* Saitaer do Matrix: "Mogę dać Ci to, czego pragniesz..." "Chcę do laboratorium!" - Otwiera się bezpośrednie przejście w ścianach.

* Matrix i Andrzej odłączają się od grupy, by zobaczyć laboratorium, reszta chce wracać do śluzy. Biorą ze sobą połowę żołnierzy.

* W laboratorium w biowacie znajdują ciało człowieka (?). Kiedy biowat się otwiera, Saitaer do Andrzeja "Ciekawe, nie widziałem go wcześniej..." Matrix zaczyna ciąć ciało, które silnie krwawi - on wciąż żyje! Skalpel utyka w czymś pod skórą, nie zniechęcona, Matrix bierze kolejny skalpel i pobiera próbki. Kilku żołnierzy nie wytrzymje i ucieka w głąb statku. 

* Reszta drużyny przebija się do śluzy - tam muszą zaczekać aż ASD Bubuta znów się podłączy...

* W końcu Matrix i Andrzej docierają do reszty.

* John wpada w amok, ogłupiony przez podszepty Saitaera i szansę walki. Żołnierze z ASD Bubuta pilnują, aby nie wszedł na statek, on jako jedyny pozostanie na ASD Grazioniusz.

### Wpływ na świat

* Matrix wyniosła próbki z laboratorium z ASD Grazoniusz.
* Andrzej jest bardziej podatny na wpływ Saitaera. Być może nigdy nie dowie się, co mógł dostać...
* Eliot oparł się podszeptom i udało mu się wyjść w miarę bez szwanku.
* Alicja i główny komputer ASD Grazoniusz zostały odzyskane.
* Steve może z czystymi aktami spokojnie wrócić do swojej kariery, po prostu musi zmienić miejsce działań
* Kebab wyszedł z tego zlecenia z czystymi aktami. Ciekawe, co ukradnie następnym razem...
* John pozostał na ASD Grazoniusz. Ma to, czego pragnął - jest potężny i może walczyć do woli.

## Streszczenie

Oddział skazańców został wysłany na ASD Grazionusz w celu odzyskania AI Persefona. Dostali znane plany statku z adnotacją "Nic z tego nie musi być prawdą". Podczas drogi przez statek natknęli się na wielu ludzi przekształconych przez Saitaera. Kiedy używali magii, wzburzyli energię na tyle, że Saitaer zaczął do nich mówić i kusić. Spotkali również Alicję, małą dziewczynkę przedstawiającą się jako kapitan tego statku. Szybko domyślili się, że to jest teraz AI statku, ale postanowili dojść do głównego pokoju komputerowego. Po drodze.
Ostatecznie udało im się tam dotrzeć i wrócić do śluzy. John był już pod silnym wpływem Saitaera, wpadł w amok i został na ASD Grazionusz.
Andrzej był bardzo blisko i pod silnym wpływem, ale postanowił opuścić statek.

## Progresja

* Alicja Sowińska: powróciła do Orbitera Pierwszego, ale jako TAI a nie czarodziejka. Nie wiadomo co z nią zrobią. Nie wiadomo co ona może zrobić.
* ASD Grazoniusz: uzyskał elitarnego terrorforma, stworzonego z agenta ekspedycji zwanego "Johnem".

### Frakcji

* ASD: zdobyli TAI Alicję (z Grazoniusza). Poznali wydarzenia, które miały miejsce lata temu. Zobaczyli, co naprawdę potrafi Saitaer...

## Zasługi

* ASD Grazoniusz: pełnoprawny statek widmo, upiornie creepy. Ma kultystów, terrorformy i obłąkane AI. Na Grazoniusza wpakowała się ekspecja z Astorii i zdobyli TAI Persefonę - a raczej, TAI Alicję.
* ASD Bubuta: ciężki statek abordażowy. Wstawił ekspedycję na Grazioniusza i się natychmiast wycofał; nie udało się Grazioniuszowi skazić Bubuty.
* Alicja Sowińska: kiedyś kapitan, teraz TAI Grazioniusza. "Porwana" przez ekspedycję ASD.

## Plany

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Obłok Lirański
                1. Anomalia Kolapsu: gdzie pojawił się dawny ASD Grazoniusz i gdzie trzeba było zmontować ekspedycję by zdobyć TAI Grazoniusza

## Czas

* Opóźnienie: 12
* Dni: 2

## Narzędzia MG

### Budowa sesji

**SCENA:**: Nie aplikuje

### Omówienie celu

* nic

## Wykorzystana mechanika

1811

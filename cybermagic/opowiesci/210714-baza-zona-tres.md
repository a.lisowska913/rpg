---
layout: cybermagic-konspekt
title: "Baza Zona Tres"
threads: legenda-arianny
gm: kić
players: żółw, fox, kapsel
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [210707 - Po drugiej stronie bramy](210707-po-drugiej-stronie-bramy)

### Chronologiczna

* [210707 - Po drugiej stronie bramy](210707-po-drugiej-stronie-bramy)

### Plan sesji
#### Co się wydarzyło

?

#### Sukces graczy

* ?

### Sesja właściwa
#### Scena Zero - impl

.

#### Scena Właściwa - impl

Arianna zaproponowała eksplorację bazy. Eustachy - dowodzi operacją eksploracji. Oficer taktyczny, nie?

Janus odwiedza Ariannę na mostku. Zrobił trochę wstępnych analiz. Powrót może być utrudniony. Bardzo wstępna analiza, lepiej inną trasą. Arianna zauważyła, że może tu co mają i znajdą? Może mają mapy eteryczne? Coś co można użyć?

Eustachy dopytuje noktian o ewentualne pułapki i sposoby zabezpieczeń. Dołączył noktian do grupy szukającej.

Eustachy chce podczas eksploracji sprawdzić do KTÓRYCH wzmacniaczy hipernetu Klaudia łączy się najbardziej. Cele - przede wszystkim znaleźć Klaudię i rozbudowa hipernetu, upewnienie się co się dzieje. Martyn poprosił Eustachego, by ten się dowiedział DLACZEGO jest tu tak dużo hangarów. Jeśli to baza przeładunkowa... to może jest inna droga powrotu?

Przy założeniu, że Klaudia łączy się nierównomiernie z systemami Inferni i wiedząc to co wiemy odnośnie natury Klaudii z badań Martyna czy może załoga Inferni (łącznościowcy, nauka) określić naturę Klaudii - czy jest "rozproszona", czy jest "na miejscu"... innymi słowy, co jak gdzie. I magia Arianny wspierająca.

ExMZ+3: 

* X: hidden movement
* X: Elena ma kłopoty
* Vm: analiza sygnału i energii magicznej sugerują minimalne zagęszczenie w kierunku centrum. 
* X: budzimy defensywy bazy

Mamy większe wskazanie na centrum, z pewnymi odchyłami. I obudziły się systemy bazy niektóre (defensywy). Łaziliśmy, rozmieszczaliśmy sensory, badaliśmy.

Mamy dużo lepszy rozkład bazy - górna część jest jak koło ze szprychami. Pomieszczenia tej bazy są raczej "normalne", acz nieznana technologia. Skrzydło medyczne ma ambulatorium, nie szpital - ich bazy umieją być bardzo szerokie. Na każdym poziomie znajdziemy mesę, ambulatorium...

Martyn zaproponował Ariannie że przejmie kontrolę nad ambulatorium i dowie się o naturze tej bazy. Spróbuje z noktianami dojść do tego czemu baza jest opuszczona, kiedy była opuszczona i jaka jest natura tej bazy.

Elena idzie w swojej grupie - noktianin, co najmniej 1 komandos itp. Elena znalazła schody na poziom niżej i zeszli ze swoją grupą. Gdy skanowali obwodowe miejsca bazy, jeden noktianin wskazał nietypowe pomieszczenie Elenie jako interesujące. Gdy otworzyli drzwi i weszli do środka, rozległ się alarm i automatyczny głos coś po noktiańsku. Jej noktianin ma na imię Ulisses. Kiedy drzwi się otwarły i weszli do środka "o cholera". Głos zautomatyzowany noktiański. Ulisses chce byśmy uciekali. Elena używając wyczucia sytuacji itp. zasłoniła Ulissesa swoim ciałem (ma Lancera). Wszyscy wieją. Jak odsapnęli - na korytarzu pulsują światła alarmowe.

Ulisses przyznał, że to była zbrojownia. Elenie udało się ewakuować tą ekipę. Co gorsza, przekaźnik hipernetowy niedaleko przestał działać.

Całkiem blisko środka grupa Eustachego napotkała pomieszczenia inżynieryjne. Eustachy widzi systemy obronne, ale je rozbraja (odpina pas z amunicją itp). Po odpaleniu kompa, pierwszy ekran, noktianie mają reakcję "omg" na ekranie logowania. Noktianie zobaczyli logo i nazwę bazy - "Zona Tres". Z szybkiej nawijki - trochę się straszyło dzieci a trochę fascynowało. Area 51. "Noktiańscy naukowcy udowodnili że magia nie istnieje, więc w strefie 3 bada się magię" XD.

Eustachy próbuje włamać się do tych systemów. I ma czas. Chce poznać lepiej naturę tej bazy. Dodatkowo próbkując anomaliami. Z użyciem magii to nie jest Ex a Tr.

TrZM (już pełna karta) +2:

* V: Wojskowa baza badawcza, która po przełożeniu systemu datowania z noktiańskiego na nasze ma ponad 100 lat. Przed wojną. Ostatnie aktywności mniej więcej w trakcie wojny. Eustachy ma dostęp do logów - maintenance, serwisowanie...
* Vm: Mamy dostęp też m.in. do danych o serwisowaniu portalu. Jest też coś ciekawego o "polu chmury" co wymagało serwisowania. Coś dziwnego. Containment oznaczony jako ryzykowny. Jesteśmy na poziomie mieszkalnym, poniżej też jest mieszkalny a niżej są już eksperymentalne. Jest też spory wycinek bazy, część niższych sekcji, które są zalane i odcięte. Mamy też informację, że baza była zrzucona z orbity
* V: Wiemy że jest szpital, wiemy że jest rdzeń eksperymentalnej Bii. Eustachy dał radę wykryć że niedawno odpalił się subsystem defensywny (Elena coś odpaliła) jak i wcześniej, gdy Klaudia zniknęła, subsystem ratunkowy - najpewniej Klaudię wzięło do szpitala.
* O: Eustachy zakodował Infernię jako Alivię Nocturnę - czyli statek który w przyszłości stał się Nocną Kryptą.

Po hipernecie obca obecność. "Zidentyfikuj się". Jednocześnie do wszystkich na hipernecie.

* Martyn: Martyn Hiwasser, oficer medyczny Alivii Nocturny. 

Wszyscy z dyscypliną odpowiedzieli to co mieli. BIA spytała Ariannę - niech się zidentyfikuje. Kapitanem Alivii Nocturny jest ktoś inny. Arianna powiedziała, że kapitan się zmienił w wyniku działań podczas wojny. BIA na to, że nie ma w rekordach żadnej wojny.

Arianna mówi jakąś formę prawdy. Ma załogę mieszaną noktiańsko-astoriańską, nie chciała tu być, zgubiła członka załogi i nie ma złej intencji wobec tej bazy. Nie wiedzieliśmy o tej bazie - szukamy Klaudii. Chcemy odlecieć. Dowód - zasada minimalnej ingerencji. SZANOWALIŚMY bazę.

ExZ+3.

* V: BIA uwierzyła przez wzgląd na upływ czasu i na to że coś tak absurdalnego nie można zmyślić. Plus jej detektory to wykryły.
* XX: "członka załogi" - w rogu pomieszczenia ta sama dziewczynka co przy Elenie. "Pomóż mi znaleźć przyjaciół" i znika. BIA ma pauzę jak Arianna do niej mówi.

Martyn -> Klaudia odnośnie przyjaciół. Arianna z BIĄ by się dowiedzieć. 

* V: Martyn ma pinga lokalizacyjnego od Klaudii na terenie bazy, podwójny. Szpital i BIA Core. A gdy Martyn komunikuje się z Klaudią, BIA ma pauzę.

Czyli BIA ma pauzy i się psuje pod kątem psychotronicznym przez Klaudię. Próbujemy przekonać BIĘ że możemy jej pomóc.

* X: Włączył się nadajnik bazy i leci w tą stronę Lewiatan.

Informujemy o tym BIĘ że tu leci Lewiatan. Krąży w pobliżu i się zdecydowanie zbliżył. Lewiatan jest zainteresowany...

* X: Nie przekonamy jej że jest chora.

BIA ma uszkodzoną percepcję. Ona nie widzi że jest chora...

Poszła na wymianę informacji. My jesteśmy w "ciemnej stronie planety", orbitującej czerwonego karła. Po całkowicie ciemnej stronie planety - warstwa lodu. Po jasnej - nie ma wody za ciepło. Pół pustynia, pół lud, a my pośrodku (w paśmie wody). Jesteśmy w tajnej bazie, więc nie dostaniemy lokalizacji kosmicznej. Na jej alarmy nikt nie odpowiada - na orbicie coś noktiańskiego powinno być.

NOWY PLAN - oferujemy Bii wycieczkę na orbitę. Niech Bia zamontuje swoje sensory na Inferni i pokażemy jej orbitę. Eustachy dostarczy jej częściowych schematów Inferni. AHA WYBIERAMY FRAGMENT MAKSYMALNIE PODOBNY DO NOCNEJ KRYPTY. Jedna rzecz sprawiła że przeszedł zimny dreszcz. Nie było robota, który instalował elementy. Same urosły. Mamy anomaliczną Bię... lub BIĘ władającą nanitkami (według noktian). 

Bia pozwoliła Inferni na opuszczenie bazy. Infernia lecąc na orbitę napotkała grubą warstwę lodu - zespół Eustachego opuścił Infernię i wysadził powłokę. Gdy Infernia opuściła planetę, okazało się, że nie ma tam przewidzianych przez Bię systemów noktiańskich i centralnego dowodzenia. Bia nie poznaje gwiazd. Okazuje się, że planeta opuściła swoją orbitę i zagubiła się w przestrzeni kosmicznej...

Są sami i skazani na siebie...

## Streszczenie

Podczas eksploracji archaicznej noktiańskiej bazy Infernia doszła do tego, że to mityczna Zona Tres - odpowiednik "area 51", z badaniami nad magią. Infernia przez przypadek uruchomiła Bię. Zespół zamaskował Infernię jako Alivię Nocturnę i Arianna przekonała Bię do wstępnej współpracy. Okazało się, że Bia jest chora i nie postrzega wszystkiego prawidłowo, Klaudia jest z Bią sprzężona (oops). By dowiedzieć się więcej, Infernia wyleciała na orbitę i okazało się, że są na rogue planet - planecie która wypadła z orbity i zagubiła się w kosmosie...

## Progresja

* OO Infernia: udaje Alivię Nocturnę (pre-Nocną Kryptę) przed Bią bazy noktiańskiej "Zona Tres".
* BIA XXX d'Zona Tres: uszkodzona percepcja - nie widzi Lewiatana lub nie rejestruje jego obecności. Nie wie też o tym, że sama jest uszkodzona/chora.

### Frakcji

* .

## Zasługi

* Arianna Verlen: dostarczyła informacji maskujących Infernię jako Alivię Nocturnę oraz negocjowała z Bią, oszukując ją o pokoju Noctis - Astoria. Próbuje zapewnić przetrwanie wszystkim.
* Eustachy Korkoran: odnalazł do których wzmacniaczy łączy się Klaudia, zamaskował w komputerach Zony Tres Infernię jako Alivię Nocturnę oraz nic nie wysadził - poza lodem planety pod wodą której się znajdują.
* Martyn Hiwasser: objął kontrolę nad ambulatorium i chce przejąć szpital, za zgodą Bii. Chce uratować Bię oraz Klaudię.
* Janus Krzak: de facto ostrzegł, że powrót Bramą jest piekielnie niebezpieczny. Arianna czuje smutek.
* Elena Verlen: advancuje do głębszych elementów bazy, po czym ratuje Ulissesa (noktiańskiego komputerowca) przed autosystemami zbrojowni. Wycofała bezpiecznie swoją grupę.
* BIA XXX d'Zona Tres: sprzężona anomalicznie z Klaudią, potencjalnie używa nanitek albo magii. Współpracuje z Infernią, biorąc ich częściowo za Alivię Nocturnę. Ma uszkodzoną percepcję i nie wie że jest chora.
* Ulisses Kalidon: noktiański spec od komputerów na Inferni; uratowany przez Elenę przed systemami zabezpieczeń w zbrojowni Zony Tres. Myślał że przejmie a tu klops ;-).

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Zagubieni w Kosmosie
            1. Crepuscula: Rogue Planet. Planeta, która opuściła swój "układ słoneczny" i mknie po kosmosie (dlatego jest zagubiona). Kiedyś pozycjonowana typu 'tidally locked' (rotacja synchroniczna). Aktywna sejsmicznie i taumicznie.
                1. Pasmo Zmroku: zgodnie z 'tidal lock' (rotacja synchroniczna) pomiędzy zimną częścią (lód itp) a gorącą (nie ma wody, wyparowała). Teraz jak w kosmosie - też pokryte lodem.
                    1. Baza Noktiańska Zona Tres: odpowiednik "strefy 51", stara baza gdzie w okolicach Pęknięcia badano magię, z aktywną BIA (prototypową).

## Czas

* Opóźnienie: 1
* Dni: 2

## Konflikty

* 1 - Eustachy chce podczas eksploracji sprawdzić do KTÓRYCH wzmacniaczy hipernetu Klaudia łączy się najbardziej. Może załoga Inferni (łącznościowcy, nauka) da radę określić naturę Klaudii?
    * ExMZ+3
    * XXVmX: budzimy defensywy bazy, budzimy Bię (czyli jednak żyje XD), Elena ma kłopoty, mamy odpowiedź - zagęszczenie na centrum ale Klaudia jest wszędzie
* 2 - Eustachy próbuje włamać się do systemów bazy noktiańskiej Zona Tres, by lepiej poznać jej naturę i zakodować dane Inferni jako Alivii Nocturny
    * TrZM+2 (bo magia)
    * VVmVO: wojskowa stacja, ponad 100 lat, dostęp do logów, plany bazy + info o rdzeniu eksperymentalnej Bii. Subsystem defensywny, ratunkowy. Plus, Eustachy zakodował Infernię jako Alivię Nocturnę.
* 3 - Arianna mówi jakąś formę prawdy. Ma załogę mieszaną noktiańsko-astoriańską, nie chciała tu być, zgubiła członka załogi i nie ma złej intencji wobec tej bazy. Cel - uspokojenie / sojusz z Bią.
    * ExZ+3
    * VXXVXX: Bia uwierzyła w jakimś stopniu w pokój Noctis-Astoria, sprzęgnięcie i niestabilność, Martyn ma pinga komunikacyjnego, leci tu Lewiatan + nie przekonamy Bii że jest chora.

## Inne

.

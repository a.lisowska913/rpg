---
layout: cybermagic-konspekt
title:  "Tajemniczy ołtarz Moktara"
threads: dzien-z-zycia-terminusa
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [181216 - Wolna od terrorforma](181216-wolna-od-terrorforma)

### Chronologiczna

* [181216 - Wolna od terrorforma](181216-wolna-od-terrorforma)

## Projektowanie sesji

### Struktura sesji: Eksploracja

* **Scena, Konsekwencja, Niestabilność, Opozycja, Trigger**
    * Atena i Pięknotka w Nukleonie.
    * Atena ma pełen podgląd na Pięknotkę ORAZ Atena jest wolna od terrorforma.
    * Atena chce pomóc Pięknotce wrócić do formy. Chce ją omonitorować. A Pięknotka chce przekonać Atenę do pozbycia się kontroli terrorforma.
    * Atena musi wrócić na stację. Pięknotka musi być monitorowana pod kątem zmian.
    * init sesji
* **Scena, Konsekwencja, Niestabilność, Opozycja, Trigger**
    * Romuald i Julia kłócą się o Pięknotkę.
    * Romuald i Julia robią sobie "przerwę".
    * Julia chce mieć zarówno Pięknotkę jak i Romualda. Romuald chce Julię i TYLKO Julię.
    * Romuald chce Julię tylko dla siebie. Julia chce się dzielić.
    * Skażenie Julii przez Pięknotkę by ją ratować.
* **Scena, Konsekwencja, Niestabilność, Opozycja, Trigger**
    * Bogdan wysłał Pięknotce prezent i holokryształ.
    * Pięknotka wraca do Bogdana na kolanach. Negocjują warunki jej powrotu. OR: wyśle mu holokryształ.
    * Pięknotka nadal jest pod wpływem Bogdana. On nadal chce ją z powrotem.
    * Bogdan chce Pięknotkę dla siebie. Ona chce być wolna.
    * Samodzielna wola Bogdana.

### Hasła i uruchomienie

* Ż: (kontekst otoczenia) -
* Ż: (element niepasujący) -
* Ż: (przeszłość, kontekst) -

### Dark Future

1. ...

## Potencjalne pytania historyczne

* brak

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Misja właściwa

**Prolog**: (10:06)

_Sen o Lustrach_

Pięnotka stoi przed lustrem. Wielkim lustrem. I jest w labiryncie luster. Pięknotka siada na ziemi, szuka odsłuchu i próbuje się obudzić. Udało jej się - ale usłyszała cichy szept "widzę Cię". Obudziła się za wcześnie...

Wpływ:

* Żółw: 0
* Kić: 0

**Scena**: (10:09)

_Kompleks Nukleon_

Pięknotka obudziła się w swoim łóżku. Jest sama. Jeszcze jest noc. Idzie do pokoju Ateny w środku nocy, przytulić się do niej.

Atena rano (jak się obudziła) z wrażenia spadła z łóżka. Tego się nie spodziewała. Klapnęła jak żaba.

Atena i Pięknotka miały rozmowę - tematem była Arazille, to, że Atena chce oczujnikować Pięknotkę (nie wie co jej się stało) oraz to, że Atena ma wybór - Saitaer albo kralothy do korekcji Wzoru. Pięknotka zgodziła się na postulaty Ateny - pozwoli jej się dokładnie oczujnikować. A Atena dzięki temu dostanie wszystkie potrzebne informacje do tego, by móc sprawdzić co i jak...

Z punktu widzenia Pięknotki nie zmiana Wzoru (to Diakoni czasem zmieniają) jest chora. THIS is fucked up. Pięknotka spojrzała na portfolio Ateny i zaprotestowała. Tam był jeden mag z psią głową. Atena zauważyła, że to był jedyny sposób jak go uratować - nie miała nic innego z układem nerwowym pod ręką. Pięknotka powiedziała, że jej uroda nie może zostać naruszona.

Pięknotka złożyła Atenie kontrpropozycję - niech się zbada. Ona sama (Atena) jest potencjalnym obiektem testowym.

* "Po podleczeniu..." - Pięknotka
* "REKONSTRUKCJI" - Atena, z naciskiem
* "Potato, potato..." - Pięknotka, z uśmiechem

Pięknotka powiedziała Atenie, że przecież ta nie zostanie w mocy Saitaera. Niech się wyleczy - i tak musi. I wtedy będzie miała możliwość sama się przebadać. Co więcej, Bogdan zmienił Pięknotkę bo wiedział, że do niego wróci. Jak Atena to zrobi, będzie w stanie pomóc Pięknotce. Co więcej, Atena jest medykiem - nie jest ciekawa? (TrZ+3:11-3-5=S). Atena się niechętnie, ale zgodziła...

Wpływ:

* Żółw: 2
* Kić: 0

**Scena**: (10:50)

_Knajpka Szkarłatny Szept_

Pięknotka skorzystała z okazji, że Waleria nie śpi i jest dostępna na hipernecie. Zapytała ją "did your mirror dreams ever stop" i dała lokalizację - knajpka Szkarłatny Szept. Dostała odpowiedź hipernetową: "nie przejmuj się nimi, miną; nie słuchaj luster - jeśli się w nie nie wpatrujesz, nie mogą zrobić Ci krzywdy". Pięknotka spytała co się stanie jeśli się zapatrzy. Waleria odpowiedziała, że nie wie. Lepiej, by nie patrzyła. Waleria jest przekonana, że to Arazille. Jak inaczej usunąć Saitaera niż przez użycie innego bóstwa?

Waleria jednak przyszła. Jest już dość silna. Waleria powiedziała, że nie ma pojęcia, jak Moktar do tego doszedł by wygnać Saitaera używając mocy Arazille. MOKTAR a nie BOGDAN. Dla Pięknotki to była największa niespodzianka. Jak Pięknotka zapytała o co chodzi, Waleria powiedziała - Bogdan opracował rytuał naprawy, ale Moktar podpiął się do energii Arazille bez jej Skażenia Walerii a teraz Pięknotki. No, o to Pięknotka go nie podejrzewała.

Jakiś naćpany mięśniak zaczepił Walerię z intencją obicia. Waleria odpowiedziała zimno, że jeśli ją dotknie, skończy w Nukleonie. Rzucił się na nią - Pięknotka podcięła mu nogi. Waleria zauważyła, że jeśli Pięknotka będzie tak działać, dostanie flagę free-for-all w tym miejscu tak jak ta którą ma Waleria. Za co? Za to, co mu zrobi jak ją dotknie. Pięknotka spróbowała przekonać Walerię do wyjścia stąd by uniknąć przemocy (Tp:7,3,4=S, zmiana intencji).

Waleria zdecydowała się pójść, ale thug złapał ją za nogę i pociągnął. Waleria uderzyła głową w krzesło i rozbiła sobie nos. Zaraz uruchomił się Swarm i posiekał delikwenta w formie niezbyt niebezpiecznej ale wizualnie efektownej i upiornie bolesnej. Waleria nie ma jednak dość siły; Pięknotka podpiera ją i odprowadza do wyjścia. A tam - Moktar.

Moktar wziął sobie Walerię i spojrzał głęboko w oczy Pięknotki. Usatysfakcjonowany tym co widział, burknął, że będzie dobrze. Zapytany o co chodzi, powiedział, że chodzi o jej sny. Jak Pięknotka zebrała się na śmiałość i powiedziała, że ma przyjaciół którzy przejdą przez ten rytuał, Moktar powiedział że mają nie patrzeć w lustra. A jak ona ma wątpliwości, niech przyprowadzi ich do niego - on wie co wtedy robić.

**Scena**: (11:26)

_Kompleks Nukleon_

Pięknotka wróciła do Nukleonu, by usiąść w swoim pokoju medycznym. Ma kwiaty i paczkę. Piękna kąpiel i przejrzała paczkę i kwiaty. Co tam dostała? To dar od Bogdana - "Dla mojej pięknej Pięknotki". He conditioned her; a raczej, nie zrobił tego - ale wie dokładnie jaka seria sygnałów i rzeczy która sprawia, że ona pragnie się oddać mu pod kontrolę. I zadziałało. Pięknotce z myśli wypadły wszystkie inne pomysły - ona CHCE tam wrócić. Nie chce tracić tego, co miała przez te trzy dni.

Pięknotka zostawiła karteczkę Atenie "out searching for clues" i poszła do Bogdana. Najlepiej teraz, od razu. Zwłaszcza, że idzie szukać 'clues'. Pięknotka wie, że Atena nie będąc detektywem nie jest w stanie znaleźć Pięknotki zanim ta nie spotka się z Bogdanem.

_Colubrinus Meditech_

I Pięknotka wróciła nago na kolanach do Bogdana. I Bogdan jedynie wzmocnił swój hold na Pięknotce, cały zachwycony swoim najdoskonalszym dziełem. Ona ma wolną wolę, a jednak jej wola poddaje się jego woli. Energia Arazille płynąca w Pięknotce nie blokuje tego, co on jej zrobił i jej robi. Dokładnie przebadał ją, jej odruchy, jej umysł...

Parę godzin później Pięknotka spróbowała dowiedzieć się jak to się ma z Arazille. Skąd on wie, jak to działa. (TrZ:10,3,5=S). Sukces zasobowy. To jest spirala. On potrzebuje Pięknotki, ona potrzebuje Bogdana.

* Rytuał to magia krwi + energia Arazille
* Energię Arazille zapewnia ołtarz Arazille "kontrolowany" przez Moktara. Moktar wzmacnia Arazille na tym terenie - tyle Bogdan rozumie. Ale Moktar nie chce infestacji Arazille.
* Moktar traktuje Arazille jako broń, ale nie wiadomo przeciwko czemu. Gdy Bogdan poznał Moktara, on już władał potęgą Arazille.
* Bogdan i Moktar wiedzą gdzie jest ten ołtarz. Holokryształy są tym, dzięki czemu przenoszona jest energia. Dlatego rytuał wykorzystuje lustra. Tylko bóstwem odepchniesz bóstwo.

Pięknotka i Bogdan się umówili - Pięknotka może normalnie funkcjonować, ale co jakiś czas tam wróci. Bogdan jest z niej szczególnie dumny...

**Scena**: (11:26)

_Kompleks Nukleon_

Atena przesłuchuje Pięknotkę. Gdzie była, jak wygląda sytuacja, czego się dowiedziała. Pięknotka nie chce jej powiedzieć, więc poszła w pełną artylerię - teasing, having fun... (Łt+1:SS). Atena się zorientowała, że coś jest bardzo nie tak z Pięknotką - jest zbyt erotyczna, zbyt śmiała, nawet jak na nią. Ale Atena nie zdąży nic z tym zrobić przed własnym czyszczeniem i wie, że to się stało niedawno a nie podczas rytuału. Wysłała więc prośbę do Epirjonu by ktoś spojrzał na parę tematów.

Atena poddała się rekonstrukcji.

Wpływ:

* Żółw: 7
* Kić: 2

**Epilog**: (12:15)

* Pięknotka wie, że Moktar wykorzystuje energie i moce Arazille i używa Arazille jako broni.
* Pięknotka i Bogdan wpadli w toksyczną współzależność.
* Atena przechodzi przez rekonstrukcję.
* Waleria udowodniła, że jest przerażającą, zimną suką.

### Wpływ na świat

| Kto           | Wolnych | Sumarycznie |
|---------------|---------|-------------|
| Żółw          |   7     |      7      |
| Kić           |   4     |      4      |

Czyli:

* (K): 
* (Ż): 

## Streszczenie

Pięknotka śniła o lustrach; w poszukiwaniu informacji co się dzieje dowiedziała się, że do wyczyszczenia Saitaera wykorzystana jest energia Arazille. O dziwo, Moktar kontroluje energię Arazille a przynajmniej używa jej jako broni. Pięknotka wróciła w ręce Bogdana i przekonała Atenę, by ta poddała się rekonstrukcji - by uwolniła się od Saitaera.

## Progresja

* Pięknotka Diakon: potrzebuje Bogdana. Po prostu. To nie jest miłość, to jest uzależnienie. Spirala.
* Pięknotka Diakon: przez następne dwa tygodnie jest całkowicie niemożliwe by cokolwiek zdominowało Pięknotkę przez energię Arazille.
* Bogdan Szerl: potrzebuje Pięknotki. To nie jest miłość, to jest uzależnienie. Spirala.

### Frakcji

* 

## Zasługi

* Pięknotka Diakon: dowiedziała się o relacjach między Moktarem, Saitaerem i Arazille (przynajmniej częściowo). Wpadła we współzależność z Bogdanem. Jest szczęśliwa.
* Atena Sowińska: zaczęła martwić się o stan Pięknotki i ogólnie te dziwne rytuały. Poddała się rekonstrukcji, ale wezwała wsparcie by pomogło jej w analizie co tu się dzieje.
* Waleria Cyklon: osłabiona; pomaga Pięknotce przystosować się do świata gdzie śnią jej się lustra. Zmasakrowała maga, który ośmielił się ją zaatakować swoim swarmem.
* Moktar Gradon: upewnił się, że Pięknotki nie dotyczą wpływy Arazille. Wzmacnia Arazille, ale nie chce by ona miała gdzieś tu swój hold.
* Bogdan Szerl: odpowiednio wysłał sygnał do Pięknotki i wpadł we współzależność z nią. Powiedział jej o ołtarzu Arazille Moktara i scementował swój hold na Pięknotce.

## Plany

* 

### Frakcji

* 

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Przelotyk
                        1. Przelotyk Wschodni
                            1. Cieniaszczyt
                                1. Knajpka Szkarłatny Szept: Pięknotka i Waleria się tu spotkały; też tu Waleria użyła swarma by pociąć maga (by trafił do Nukleonu)
                                1. Kompleks Nukleon: centralne miejsce dowodzenia i spotkań Pięknotki i Ateny
                1. Sojusz Letejski, NW
                    1. Ruiniec
                        1. Colubrinus Meditech: Pięknotka sama przyszła do Bogdana oddać się w jego ręce. Bogdan z radością przyjął jej ofiarę.

## Czas

* Opóźnienie: 0
* Dni: 2

## Narzędzia MG

### Budowa sesji

**SCENA:**: Nie aplikuje

### Omówienie celu

* nic

## Wykorzystana mechanika

1811

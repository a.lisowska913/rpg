---
layout: cybermagic-konspekt
title: "Porwanie Daniela Terienaka"
threads: rekiny-a-akademia
gm: żółw
players: anadia, kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [210713 - Rekin wspiera mafię](210713-rekin-wspiera-mafie)

### Chronologiczna

* [210713 - Rekin wspiera mafię](210713-rekin-wspiera-mafie)

### Plan sesji
#### Co się wydarzyło

.

#### Sukces graczy

* .

### Sesja właściwa
#### Scena Zero - impl

.

#### Scena Właściwa - impl

Karolina. Jej brat zniknął. "Jeśli się nie odezwę przez 24h coś mi się stało". I się nie odezwał. W pokoju brata Karolina znalazła notatki - brat chciał zająć się kurierowaniem różnych rzeczy. I ta sprawa była z tym powiązana. Wszystko wskazuje na dwóch arystokratów - Arkadię Verlen i Rafała Torszeckiego, oni coś powinni wiedzieć.

Verlenka to paladynka, ale Torszecki to gnida. A z gnidami da się dogadać... trotylem i napalmem ;-).

Karolina robi notatkę w której napisane jest bez słowa wyjaśnienia w formie drukowanej "pojaw się WTEDY A WTEDY w okolicach zniszczonych magazynów - M.S.". Bezpośrednie polecenie.

Tr+2: 

* V: Torszecki pójdzie i pójdzie raczej sam

Torszecki poszedł do zniszczonych anomalnym ogniem magazynów. Nie ma dokładnie punktu. Wchodzi w okolice magazynów. Karolina próbuje go spłoszyć, by uciekł do środka jakiegoś pomieszczenia ze skrytkami (zniszczonego). Że niby toczy się puszka, odgłos silnika... rzeczy stresujące przeciętnego Torszeckiego który, jak wiemy, ma wrogów.

TrZ+3: 

* XX: nie wychodzi, Torszecki za bardzo się boi.
* V: Karolina na ścigaczu, Torszecki chcąc uniknąć przejechania wskoczył do jednego z pomieszczenia ze skrytkami
* V: Torszecki nie wyjdzie, Karolina zawróciła i go tam przyskrzyniła. Patrzy nań wrogo. Torszecki jeszcze nic nie wie.

"Gadaj co zrobiłeś z Danielem" -> "Nic nie wiem, o co ci chodzi". Torszecki się wypiera. Karolina leci na Torszeckiego szybko ścigaczem (by zastraszyć), Torszecki wyciąga różdżkę, Karolina otwiera ogień z działek.

TrZ+3+5O:

* XX: Różdżka wybuchła, powodując zagęszczenie energii magicznej i materializuje się coś podłego. W tym miejscu. Znowu...
* V: Karolina odstrzeliła różdżkę Torszeckiemu.
* Vz: Karolina łapie Torszeckiego, wciąga na ścigacz i odpala pełną moc ścigacza - oddala się stąd zanim coś się zamanifestuje.
* V: Torszecki się poobijał i ucierpiał. Nie jest w stanie wezwać S.O.S.

Karolina ścigaczem leci na stadion. Tam nie ma anomalii. Zrzuca Torszeckiego i wali go w pysk. Potem w brzuch. Niech Torszecki gada co wie o jej bracie. Daniel Tereniak. Albo powie, albo ma problem.

* X: to, że anomalia się rozrasta i pojawia łatwo powiązać z Torszeckim.
* O: anomalia wymaga natychmiastowej uwagi, jest już "duża". Jest wykrywalna. Paradoksalnie, nikt nie zwraca na Karolinę uwagi.
* V: Torszecki wypłakał, że nic nie wie. Arkadia Verlen? Daniel się związał z mafią. A Arkadia walczy z mafią. Przecież te magazyny to właśnie mafii sprawa. Ona na pewno je podpaliła.
* V: Karolina wyindukowała odpowiednim terrorem i kopniakiem Głęboki Respekt u Torszeckiego. Będzie jej pomagał ALBO.

Służby porządkowe już zajmują się anomalią...

Marysia Sowińska pije coś uroczego w swoim apartamencie. Dostaje wiadomość od randomowego maga. "Torszeckiego biją bo spieprzył." Na stadionie go biją. Ktoś nagrywa i umieszcza z przypisem "zrobił anomalię i dostaje wpierdol".

Marysia dociera na stadion i wygania "reportera". Nie życzy sobie bycia nagrywaną. Reporter szybko się zwija. Zostaje Marysia, Karolina i straszony Torszecki.

* "Rafał, czemu dajesz się bić?" - Marysia, szorstko
* "Bo jest flakiem" - Karolina, ponuro
* Rafał się rozpłakał. "Tien Sowińska, URATUJ MNIE!"
* "To Twój pachoł?" - Karolina, z niedowierzaniem
* "Nie lubię jak go biją" - Marysia, unikając rozpaczliwie odpowiedzi

Karolina wyjaśniła, że jej brat zniknął i powiedział by go poszukać. I Karolina znalazła ślady wskazujące na Torszeckiego. Marysia wydedukowała (nie mówiąc o tym), że Daniel (brat) musiał być tym kurierem. Ale skąd terminusi (jeśli to oni) wiedzieli? Skąd Arkadia (jeśli to ona) wiedziała? Innymi słowy, ktoś coś więcej wie i tego nie mówi. Ale to jest powiązane z Kacprem Bankierzem na pewno.

Karolina powiedziała, że ślady też wskazują na Arkadię, ale to niemożliwe. Marysia zaproponowała wsparcie. Karolina - zależy ile będzie kosztować. Przysługę? Karolina się zgadza, ale pewnych rzeczy nie zrobi.

Marysia połączyła się z Tukanem po hipernecie. Spytała o Daniela Tereniaka - czy Tukan coś wie. Tukan skupił się i połączył z systemami Pustogoru. Zaintrygowało go to co zobaczył.

Tukan dowiedział się od Marysi, że Tereniak zniknął w ciągu ostatnich 24h. Oficjalnie w systemach Pustogoru nikogo nie złapali terminusi. Nieoficjalnie... od dwóch dni do ostatnich 24h pewna młoda kandydatka na terminusa rozpaczliwie szukała wsparcie na akcję przeciwko mafii. Nie otrzymała wsparcia, w rozumieniu "zostaw to". Tukan powiedział, że Marysia zna tożsamość tej młodej czarodziejki. Marysia może się domyślać...

Terminusi nic nie autoryzowali. To znaczy, że jeśli Ula cokolwiek zrobiła, to zrobiła to na własną rękę i własnymi zasobami. Które nie będą imponujące.

Tukan powiedział Marysi, że ma pewne cenne informacje na temat Uli. Coś, co może być bardzo cenne dla Marysi i dużo jej wyjaśni i może służyć jako materiał do szantażu jako guzik atomowy. Ale to jest drogie - przysługa.

Tukan otrzymał przedłużenie miłego wypoczynku w przemiłym towarzystwie. Bardzo, bardzo mu to pasuje.

Marysia i Karolina poleciały do Zaczęstwa. Odwiedzić Ulę, z zaskoczenia. Ula mieszka w bloku. Nie jest to duże mieszkanie. Marysia -> Ula po hipernecie. 

Ula nie jest w domu, jest zajęta. Marysia powiedziała, że szuka zaginionego maga - Daniela Terienaka. Ula powiedziała, że jeśli był powiązany z mafią, to go sprzątneli i sam jest sobie winny. Marysia nacisnęła, że przecież to pomoże karierze Uli. Ula powiedziała, że jej kariera nie potrzebuje takiej pomocy. Ula powiedziała, że oczekuje, że jak naprawdę im zginął mag - Marysia może zgłosić to terminusom. Marysia na to, że zrozpaczona siostra to zrobi i jest Uli wdzięczna za radę.

I teraz Ula ma problem. Ale może tego nie widzieć. Więc Marysia chce sprawić, by Ula uznała, że nie może tego tak zostawić bo np. ucierpi jej kariera czy standing wśród terminusów. Czyli - że Ula musi jakoś współpracować.

Tr+3:

* XX: Ula nie odda delikwenta. Uznaje, że sobie poradzi z sytuacją.
* V: Ula widzi, że ma problem. Będzie "współpracować"
* X: Ula wydobyła środki i informacje od Daniela pozwalające jej na zrobienie mu krzywdy z uwagi na współpracę z mafią.
* V: Ula wie, że jeśli to będzie zgłoszone, to będzie poważny problem dla niej - bo porwała Rekina.

Zanim dotarła Ula do Zespołu, Karolina została wtajemniczona w to, że najpewniej Ula porwała jej brata i ma pewne dowody / ślady. I zrobiła to nieoficjalnie.

Karolina złożyła zamówienie na dwa typy środków chemicznych - amnestyk i serum prawdy. Grzymościowcy z przyjemnością sprzedadzą takie maleństwa Karolinie, acz nie pojawią się tu natychmiast.

Niecałą godzinę później Ula się pojawiła, by "współpracować" z Marysią i Karoliną. Od razu widać, że Ula arystokracji po prostu _nie lubi_. Ula zaprosiła Rekinki do swojego mieszkania.

Mieszkanie jest _puste_. Ula tu tylko mieszka. Jest sama. Ale ma kota. Futrzak patrzy nieufnie na ludzi; oczywiście, Ula ma niby cztery krzesła, trzy nieużywane. Ula wyraźnie utrzymuje się sama, ze stypendium terminuskiego. Jest uboga, ale nie głodująca.

Marysia kicia na kota, kot przychodzi i się ociera. Ula ma 100% niezadowolenia, ale nic nie mówi. To po prostu da się wyczuć. Naruszyłyście jej świątynię.

Ula pyta o Daniela. Jak się zachowuje, gdzie jest, itp. Próbuje wydobyć od Karoliny i Marysi jak najwięcej informacji na jego temat by zbudować case przeciwko niemu. Marysia "złoty chłopak". Ula wyciąga z linii terminuskich rekordów - tu pobicie, tam awantura, tam szantażyk... na pewno nie jest "złotym chłopakiem".

Marysia zauważyła, że Ula nie powinna mieć takich informacji na jego temat. Przecież jako nie-terminuska nie powinna mieć informacji na jego temat. Ula odpowiedziała, że jeden z terminusów ją autoryzował, bo "Daniel niekoniecznie dobrze zachowuje się wobec dam". Ula wygadała się, że studiowała informacje na temat wszystkich Rekinów o których wie. Ona ma jakąś obsesję na ich punkcie. Jest wściekła, nie zachowują się jak powinni.

Karolina chroni honor Daniela. Co jak co - ale dziewczynom nigdy krzywdy nie robi. Niech Ula nie przypina takich rzeczy do jej brata. Ula odbija, że wiedzą to co wiedzą - mają kapowniki na każdego bo muszą mieć bo Rekiny się tak zachowują. Karolina chce wiedziec który konkretnie terminus oczernia jej brata. Ula odmawia. Karolina "czyli pozwolisz żeby chronił się za odznaką i rzucał kalumnie na dowolnego maga?". Ula obiecała, że skoryguje rekordy.

Ula powiedziała, że znajdzie Daniela. Marysia i Karolina niekoniecznie tego chcą - bo wtedy wszystko pójdzie jak po sznurku jak chce Ula. Czyli ona coś planuje.

Karolina mówi - lećcie sobie do Pustogoru. Poszukam brata. Poszukam go po krwi. Ula "nie możesz. To niebezpieczne i nielegalne". Karolina "spieprzaj. To mój brat. To nie jest aniołek, ale pewnych rzeczy by nie zrobił. Jeśli mafia go złapała, nie mam innego wyjścia".

Karolina naciska na Ulę - czy Ula będzie chronić własny tyłek czy niekoniecznie. Czy się przyzna, że spieprzyła, czy niekoniecznie.

ExZ+4:

* V: powiedziała, że mniej więcej wie gdzie on jest. Chce znaleźć gdzie on jest by dowiedzieć się KTÓRA maszyna z czasu wojny go chroni i pilnuje. Wtedy pozna kod (dzięki Trzewniowi) i odblokuje maszynę i wejdą.
* V: "Karolina: 'za wolno'". Ula - WIEM gdzie on jest. Widziałam, jak maszyna go zgarniała. Nie, nie uratowałam go - wiem że będzie przesłuchany przez automatyczne systemy.

.

* Karolina: "oddaj mi mojego brata zanim zrobią mu z mózgu papkę"
* Ula: "taki mam plan, więc nie używaj magii krwi"

Ula nie będzie już spowalniać. Naprawdę musi wiedzieć jaki to model maszyny i naprawdę musi poznać kody do tego modelu, ale nie będzie spowalniać działań Zespołu.

Ula poleciała do Pustogoru po kody. Karolina poleciała do lasu - dostała namiar gdzie znajduje się ten bunkier z jej bratem. Karolina przelatuje koło bunkra i szuka Strażnika - musi przeskanować model.

TrZ+2:

* V: Karolina ma model Strażnika. Ula poprosiła o direct link ze Strażnikiem po szyfrowanym kanale.
* Vz: Karolina bez kłopotu dostarczyła ten link. Ula wyłączyła Strażnika. Strażnik przestał strażować.

Karolina znalazła swojego brata nieprzytomnego. Jest na jakimś fotelu, elektrody przy głowie, komputery... jest to wyraźny system przygotowany do ekstrakcji wiedzy, danych itp. Nie jest starożytny, zdecydowanie jest powyżej poziomu finansowego Uli. To jest coś co złożył ktoś inny kiedyś. Nie są to też systemy terminuskie. I, na bazie obserwacji Karoliny, nie transmituje poza obszar bunkra.

Karolina zgarnia nieprzytomnego brata (i jego ścigacz na autopilocie), upewnia się że nic żywego nie ma w pobliżu i leci do Podwiertu. Tam kupuje najsilniejsze materiały wybuchowe i minuje bunkier. Po czym go wysadza. Pełne zniszczenie.

Ula jest niezadowolona. Karolina ma to gdzieś...

I Marysi udało się poznać nazwisko terminusa, który pała sympatią do Uli - Mariusz Trzewń. On ma do niej słabość. Ekspert od komputerów i zbierania danych. I ogólnie koleś z dobrym sercem.

Marysia zleca swoim ludziom w Aurum zadanie - kim jest Ula Miłkowicz? Czemu ma taką nienawiść do arystokracji / Rekinów? Co jej zrobili? Chce poznać słabe strony Uli i zdobyć na nią to co ma na nią Tukan (najlepiej).

ExZ+2:

* Vz: Ula pochodzi z Aurum. Jej rodzina została w Aurum, Uli udało się Aurum opuścić. Jest związana "długiem rodzinnym" z arystokracją. Jej rodzina odpracowuje dług, Ula zbiegła.
* V: Przyjaciele Uli przez te tematy często powiązani byli z Noctis. Czyli ludzi trzeciej kategorii zwłaszcza w Aurum. I Ula widziała jaki los ich czekał - czy przez mafię czy przez Aurum. Ula ze swoim podejściem honorowym nie chciała się na to godzić. Otrzymywała nawet kary cielesne. I jest jedynym magiem w swojej rodzinie - jej moc obudziła się sama (jest protomagiem). I wtedy uciekła...

## Streszczenie

Urszula Miłkowicz zwabiła w pułapkę i porwała Daniela Terienaka - z nadzieją, że odkryje Rekina który współpracuje z mafią. Siostra Daniela, Karolina, pobiła Torszeckiego i tak spotkała się z Marysią. Wspólnie odkryły że za wszystkim stoi Ula i zmusiły ją do oddania Daniela bez przeskanowania pamięci (by się upewnić, Karolina wysadziła techbunker). Marysia dowiedziała się, że Ula jest uciekinierką z Aurum i eks-protomagiem.

## Progresja

* Tomasz Tukan: w Pustogorze już nikt nie ma wątpliwości, że się sprzedał Aurum/Sowińskim. Przynajmniej nie mafii, nie?
* Rafał Torszecki: sterroryzowany przez Karolinę Terienak; jest tchórzem. Zrobi to co Karolina mu każe. Nie z miłości a ze strachu.

### Frakcji

* .

## Zasługi

* Marysia Sowińska: uratowała Torszeckiego przed Karoliną. Wykryła, że brata Karoliny (Daniela) porwała Ula. Zmusiła Ulę do współpracy politycznym manewrem i zaintrygowana terminuską doszła do tego kim Ula jest i skąd ta nienawiść.
* Karolina Terienak: by ratować porwanego brata skopała Torszeckiego, poszła na współpracę z Marysią Sowińską i nacisnęła na Ulę Miłkowicz (że użyje krwi) zmuszając ją do decyzji - chroni skórę czy ratuje innych.
* Daniel Terienak: chciał sobie dorobić u Kacpra Bankierza i poszedł na współpracę z mafią. Ula Miłkowicz go wymanewrowała, porwała i zaczęła skan. Uratowany przez siostrę (Karolinę).
* Rafał Torszecki: po tym jak ślady destrukcji magazynu Kacpra wskazywały na niego został skopany przez Karolinę Terienak. Wyjął na nią różdżkę kontra jej ścigacz i wywołał KOLEJNĄ anomalię...
* Urszula Miłkowicz: uciekinierka z Aurum i eks-protomag; porwała Daniela by poznać kto jest Rekinem działającym z mafią - ale nic z tego nie osiągnęła. Mieszka samotnie z kotem.
* Mariusz Trzewń: wspiera Ulę Miłkowicz. Jest jej "terminusem-opiekunem", acz nieformalnym. Przy okazji, zna kody do różnych mechanicznych potworności powojennych w Lesie Trzęsawnym.
* Tomasz Tukan: powiedział Marysi, że Ula nie ma autoryzowanej żadnej akcji terminusów. Był skłonny sprzedać Marysi sekrety przeszłości Uli za przysługę. Pies na luksusy w Aurum z ładnymi kobietami.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Podwiert
                                1. Las Trzęsawny: znajdują się tam liczne potworności - mechanizmy powojenne i techbunkry. Serio niebezpieczne miejsce.
                                1. Dzielnica Luksusu Rekinów
                                    1. Obrzeża Biedy
                                        1. Stadion Lotników: Karolina skopała tam Rafała ku uciesze nagrywających Rekinów. Bo uciekła od anomalii i szukała brata ;-).
                                    1. Sektor Brudu i Nudy
                                        1. Skrytki Czereśniaka: Karolina zwabiła tam Rafała, po czym postraszyła, Rafałowi wybuchła różdżka i pojawiła się anomalia...
                            1. Zaczęstwo
                                1. Osiedle Ptasie: mieszka tam w niewielkim mieszkanku (kawalerce) Ula Miłkowicz z kotką imieniem Elitka.

## Czas

* Opóźnienie: 5
* Dni: 2

## Konflikty

* 1 - Karolina robi notatkę "niby od Marysi Sowińskiej" mającą ściągnąć Torszeckiego w pułapkę.
    * Tr+2
    * V: Torszecki łyknął to i poszedł w niebezpieczne miejsce sam.
* 2 - Karolina próbuje spłoszyć Torszeckiego, by ten uciekł do środka jakiegoś pomieszczenia ze skrytkami (zniszczonego).
    * TrZ+3
    * XXVV: nie wychodzi - Karolina idzie na konfrontację bezpośrednią
* 3 - Karolina leci na Torszeckiego szybko ścigaczem (by zastraszyć), Torszecki wyciąga różdżkę, Karolina otwiera ogień z działek.
    * TrZ+3+5O
    * XXVVzV: różdżka anomalizuje i wybucha po strzale; Karolina ewakuuje Torszeckiego. Ten nie ma czasu na SOS.
* 4 - Karolina: niech Torszecki gada co wie o jej bracie. Daniel Tereniak. Albo powie, albo ma problem.
    * TrZ+3+5O (kontynuacja):
    * XOVV: anomalia powiązana z Torszeckim; wymaga dużej uwagi i porządkowcy robią. Torszecki wypłakał że wie tylko o Arkadii. Plus, Karolina wyindukowała terrorem pomoc i respekt Torszeckiego.
* 5 - Marysia chce sprawić, by Ula uznała, że nie może tego tak zostawić bo np. ucierpi jej kariera czy standing wśród terminusów. Czyli - że Ula musi jakoś współpracować.
    * Tr+3
    * XXVXV: Ula nie odda Daniela; uważa, że poradzi sobie. Widzi że ma problem. Wie, że jak będzie zgłoszone to ma problem - ale ona może skrzywdzić jego (i tylko jego).
* 6 - Karolina naciska na Ulę - czy Ula będzie chronić własny tyłek czy niekoniecznie. Czy się przyzna, że spieprzyła, czy niekoniecznie.
    * ExZ+4
    * VV: Ula przyznała, że wie gdzie jest Daniel i mniej więcej, że trochę za tym stoi. Nie będzie opóźniać. Pomoże - niech Karolina nie używa magii krwi.
* 7 - Karolina przelatuje koło bunkra i szuka Strażnika - musi przeskanować model.
    * TrZ+2
    * VVz: Karolina przeskanowała model i przekazała info Uli, po czym zbudowała link Ula - Strażnik. 
* 8 - Marysia zleca swoim ludziom w Aurum zadanie - kim jest Ula Miłkowicz? Czemu ma taką nienawiść do arystokracji / Rekinów?
    * ExZ+2
    * VVz: Ula pochodzi z Aurum, jest uciekinierką. Była protomagiem, nadal jej szukają. Jej przyjaciele zwykle są w okolicach Noctis.

## Inne
### Projekt sesji

#### Procedure

* Overarching Theme:
    * gracze mają "kierowanie" (Vision & Purpose); wiedzą gdzie iść i po co tam idą
* Achronologia: x
* Wyraziste, ważne dla graczy postacie + Relacje (konflikt?) Gracze <-> NPC, NPC <-> NPC
    * graczom bardziej zależy na postaciach i ich powodzeniu
    * więcej potencjalnych wątków i rzeczy które możemy odrzucić przy krzyżykach
* Tory, zmiana postaci z czasem, wpływ graczy na świat
    * gracze widzą jak wpływają na świat. Wpływ jest "ich" - stąd tak tragiczne jest gdy "ich" rzeczy są corrupted / destroyed.

#### Result

* Overarching Theme:
    * Theme & Feel: 
        * 
    * V&P:
        * 
    * adwersariat: 
        * 
* Wyraziste, ważne dla graczy postacie + Relacje (konflikt?) Gracze <-> NPC, NPC <-> NPC
    * Role i postacie
        * 
* Co się działo w przeszłości
    * 
* Tory, zmiana postaci z czasem, wpływ graczy na świat
    * 

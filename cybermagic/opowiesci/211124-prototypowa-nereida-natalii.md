---
layout: cybermagic-konspekt
title: "Prototypowa Nereida Natalii"
threads: legenda-arianny
gm: żółw
players: kić, fox, kapsel
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [211117 - Porwany Trismegistos](211117-porwany-trismegistos)
* [211229 - Szpieg szpiegowi szpiegiem](211229-szpieg-szpiegowi-szpiegiem)

### Chronologiczna

* [211117 - Porwany Trismegistos](211117-porwany-trismegistos)

## Plan sesji
### Co się wydarzyło

* Co się wydarzy
    * ( Tivr ma zniszczonego miragenta na pokładzie. Pokłosie Sowińska - Morlan. Wygląda jak martwa Arianna. )
    * Starcia między "kultystami", "noktianami" i złamanymi.
        * Rafał Kot (kultysta), Franciszek Muchacz (kultysta), Janina Leszcz (kultysta)

### Sukces graczy

* .

## Sesja właściwa
### Scena Zero - impl

.

### Scena Właściwa - impl

Eustachy. Zajmujesz się trochę Tivrem, poznajesz maszynę. A tu Maria. Chce robić zdjęcie, olejek na Eustachego. Zrobić serię zdjęć i filmów. "To on będzie pracował z Waszymi paniami" i "To on pracuje na Inferni". Arianna jest... zaskoczona, ale stwierdza, że to całkiem niezły pomysł.

Arianna dostała fanmail od Arkadii Verlen. "Za dużo Klaudii, za dużo Eustachego, gdzie Arianna? To gwiazda. I więcej Eleny - Arkadia."

Lekki wybuch w skrzydle medycznym. Niewielki. Eustachy poszedł ogarnąć co się dzieje. A TAM KRZYCZY NA SIEBIE DWÓJKA OSÓB! Okazuje się, że jest nowy medyk - stary Wawrzyn, zdobyty przez Klaudię. I Leona się na niego wydziera, zakrwawiona "rzuciłeś we mnie materiałem wybuchowym". "Ty rzuciłaś pierwsza; odrzuciłem". Eustachy słucha z niedowierzaniem, jak ta dwójka się zaczyna... zaprzyjaźniać? Ryczą na siebie i traktują się jak przeciwnicy, ale... dobrze się bawią? The Fuck?

Eustachy przylepij Leonie nalepkę Martyna "dzielny pacjent", zrzucając jej szczękę do podłogi. Leona docenia po swojemu, ale nie zapomni ;-). Gdy Eustachy spytał Wawrzyna jaki ten ma stopień (bo ma tylko kurtkę cywilną), ten odpowiada: "Panie oficerze, ja cywilem jestem, ja na rencie". FACEPALM ROKU Eustachego. Wraca na mostek.

A tam jeszcze gorzej.

Arianna ma pomysł - kampania reklamowa na Bardzo Prestiżową Infernię. Kompetentny sexi Eustachy. Do tego Arianna prawdziwa bohaterka. Misja w sektorze Mevilig. Infernia jako prestiżowy statek na który trzeba ściągać tylko najlepszych. I mają unikalną okazję - dołączyć do najlepszych. By to osiągnąć, elementy szczątkowe.

Kampanię centrujemy dookoła Eustachego.

Maria podaje Eustachemu tabletki, mięśnie urosły, nie ma efektów ubocznych. Olejek na mięśnie, odpowiedni dym, iskry, rozhełstany mundur. I Iza zaczyna kompozycję.

TrZ+3:

* V: Kampania jest sukcesem. Eustachy jest na językach. Jest zainteresowanie Infernią i Eustachym (Iza nakręciła łzawy filmik)
* V: Faktycznie pojawia się dobry materiał. Dobre dziewczyny.
* V: Oprócz dziewczyn pojawiają się też kadeci, agenci, osoby GODNE. Sensowne. Załoga może być uzupełniona. Lejek jest dobry.
* V: Eustachy ma to w REPUTACJI. Eustachy opoka. Eustachy solidny pancerz Inferni. Eustachy nie zawiedzie. NIE ATAKOWAĆ GO NA ORBITERZE BO POJEDZIE.
* V: Elena z bólem ale przejdzie nad tym do porządku dziennego. Najwyżej popłakuje w nocy czy coś. Ale nie będzie nic złego.

Pojawił się też mem "Eustachy miłośnie patrzy na Infernię i 'on z Tobą nie śpi. On śpi ze statkiem'". Ten mem niewiele zrobił. Kampania jest wiralnym sukcesem.

Arianna ma wiadomość od Rolanda Sowińskiego. Zaproszenie do drogiej restauracji na K1. "Kwiat lotosu". Ona słynie z epickich wizualizacji, hologramów itp. Nie chce iść. Sprzedała mu łzawą historyjkę że się martwi o Klaudię. On zaprasza Ariannę właśnie w tej sprawie. Arianna sygnalizuje Marii do kogo Arianna idzie - niech Olgierd wie. Maria się lekko zdziwiła. Ostrzegła Ariannę, że 3 załogantów Żelazka próbowało skopać Rolanda - i dostali. I Arianna poszła na spotkanie.

Roland chce:

* załatwić Ariannie mechanika (podobno dobrego)
* dołączyć do załogi Inferni jako stylista (oczywiście on potrzebuje dwórek)
* prawnika dla Klaudii
* dowiedział się od Arianny że mówią o niej "mała buntowniczka".

O dziwo, mechanik jakiego znalazł Roland jest brzydki i "stary" (40). Nieprezentatywny. I drogi. Roland chyba na serio chciał pomóc Inferni.

Proces Klaudii.

Klaudia jest doprowadzona przed oblicze sądu. W imieniu Orbitera jest przekazana na tydzień pod dowodzenie stoczni Neotik. Adam Szarjan poprosił o pomoc. Na Inferni wyjaśnił na czym polega problem:

* Nereida, jednostka o zaawansowanej psychotronice wchłonęła Natalię, pilota.
* Użyte są techniki ixiońskie.

Szarjan może powiedzieć co następuje:

* Nereida jest prototypową jednostką w oparciu o sentisieć która jest stymulowana kryształami ixiackimi.
    * Sentisieć jest sztuczna
* Nereida ma na pokładzie Persefonę. Persi integruje się sentisiecią z magiem oblatywaczem
* Natalia jest wielką fanką Eustachego i Sekretów Orbitera
* Natalii odwaliło, gdy Infernia "zginęła".

Szarjan załatwia wszystkie autoryzacje, dostępy itp. Kwestia dostarczenia Inferni dostępu do poligonu Stoczni Neotik.

Tymczasowa załoga Inferni. Jak dobrą szkieletową załogę Inferni udało się zdobyć. I co najmniej 33% załogi Inferni to załoga Inferni. Klaudia potrzebuje SPRAWNEJ załogi, a nie balastu. Jest też pipeline rekrutacyjny. Klaudia wykorzystuje CV by naprawdę mogli umieć i ich wsadzają jako przebieg próbny.

TrZ+4:

* V: Klaudia ma tymczasową załogę Inferni.
* V: Niektórzy się nadają na Infernię z tymczasowych. 
* V: Wiele z tych osób ma wsparcie komodorów, admirałów by dołączyć do Inferni i pomóc.
* X: Roland wpakował część ludzi do załogi ale kompetentnych. On chyba naprawdę chce pomóc Inferni działać.
* V: Jest dość załogi, by móc faktycznie pozwolić sobie na wymianę by można leczyć psychologicznie straumatyzowaną załogę

Infernia, całkiem sprawna, leci na terenie poligonu Stoczni. Jest dzień drogi od K1. Na pokładzie jest Szarjan.

Arianna chce zwabić Nereidę na czołówkę Sekretów Orbitera.

TrZ+3:

* V: Nereida została zwabiona.
* V: Infernia unika wszystkich ataków z zaskoczenia ze strony Nereidy.

Klaudia identyfikuje Infernię jako Infernię. Kod identyfikacyjny Arianny. Wysyła sygnał zapoznawczy. Na razie tyle. Połączenie.

TrZ+3:

* V: Nawiązane połączenie z Nereidą.
    * Głód. Pragnienie. Pożrę Cię. Zjem Cię. Pożrę Cię. Nie ma nadziei. Rozpacz. Fikcja.

Infernia wyrzuca Elenę w odpadki kosmiczne na poligonie. Elena się rzuca na Eidolon + jetpack. Elena ma podpiąć przekaźnik do Nereidy by Klaudia mogła przeprowadzić atak psychotroniczny.

Arianna "wyrzuca" Elenę tak, by Nereida się nie zorientowała.

TrZ+3:

* X: gdy Infernia zrzucała Elenę, musiała się nadstawić na skuteczne trafienie Nereidy. Nereida przeciążyła generator. Brzydki mechanik nad nim pracuje.
* V: Elena jest na pozycji. Niedaleko śmieci kosmicznych.

Eustachy WPIERW wali serią torped spread by pikały, radary, jak w odległości chapną to detonują i shrapnel z chaosem, przeciążenie osłony. Czyli ogólnie CHAOS. Nereida ma czas na reakcję. Nie przeszkadza mu uszkodzenie silnika, skrzydeł czy czegoś. Byle nie jajka. (pierwszy tani krzyżyk). I pintki jako OGARY. Wolniejsze, mniej zwrotne, ale Nereida tego nie wie. (drugi tani krzyżyk)

ExZ+4 (jak lekki krążownik, spread) + 3Vg:

* X: Nereida niszczy pintki bez problemu
* X: Infernia prowadzi ostrzał na pełnej mocy ale załoga nie jest do tego przyzwyczajona. Mamy zacięcia, awarie na pokładzie.
* X: Nereida zdjęła pole siłowe Inferni. Infernia jest naga.

Zabawa się skończyła. Eustachy strzela by zestrzelić a nie zabić. Odłamkowe działa Inferni.

* (+Magia: +3Vm -3X): Xz: "Infernia" włączyła wewnętrzne systemy autodefensywy. Są ranni z "nowej załogi Inferni". Infernia została też trafiona przez Nereidę.
* Vm: Eustachy się wściekł. Infernia się wściekła. Pełen ekran bojowy. Nereida dostała z baterii. Unik. Jednostka jest poszarpana.
* V: Nereida jest poszarpana. Ta jednostka ma WSZYSTKO zniszczone co może zostać zniszczone.

Nereida jest w SZOKU. Klaudia leci Ptakiem w kierunku na Nereidę na autopilocie. Nereida... masz dostęp do tego co chcesz.

Klaudia bierze przekaźnik DO Inferni by połączenie było jak najszybsze. Chce możliwość by Arianna ukoiła "ludzki" komponent. Już na starcie Klaudia puściła diagnostykę i pilot ma 38 kg. Wyssany z energii witalnej przez Nereidę. Coś Poszło Nie Tak w integracji. Klaudia -> Szarjan. Czy Nereida jest nastawiona na ochronę pilota? Tak. Szarjan też jest jako sufler. Jest głęboka integracja, ale jest pełne rozdzielenie. Szok rozerwał połączenie. Nie do końca jeszcze, ale nie ma tak silnej integracji jak była.

Klaudia rozdziela kanały. Arianna, Szarjan --> Natalia. Sama --> Persefona. Szarjan musi wybrać z kim rozmawia. Plus - Klaudia do obu, że muszą się rozdzielić. A jak jest w stanie uzyskać, że Nereida odda Natalię, to to jest coś co chce.

ExZM+3+3Or:

* Or: Klaudia wyczuła bardzo głęboką infekcję ixiońską dwustronną.
* (+2 Vg): Klaudia gra na podstawowych instynktach TAI. Musi uratować pilota. Pozwól Szarjanowi pomóc Natalii. Xm: Persefona chce pomóc. Ale nie widzi gdzie kończy się statek a zaczyna Natalia. Nie czuje gdzie zaczyna się Persi i zaczyna Natalia. Nereida umrze jak się odepnie Natalię.
* Xz: Nereida umiera. Persi uratowała Natalię. Ale nie ma odcięcia.
* (+2 Vg): Klaudia chce przejść na rolę TAI Nereidy. Na moment. By odłączyć Natalię. X: Klaudia zaczyna migotać. Umrze.
* V: Natalia / Persefona wyrzuciła Klaudię z systemu. Próbuje chronić Klaudię. Ale ten ruch oderwał też jajko.
* Om: Natalia jest stabilna. Klaudia ustabilizowała w ixioński sposób jajko. Anomalia ixiońska. KOKON. Natalia jest w tej chwili "larwą". Ona się musi wykluć.

Eustachy nie ma wyrzutów sumienia. Klaudia jest podłamana. Dla Arianny "cóż, zdarza się". Adam jest załamany - jego przyjaciółka, co z nią będzie?

Infernia nie jest jednak w tak dobrej formie jak kiedyś. A te "działka defensywne" dzielące załogę na "starą" i "nową" nie pomagają.

## Streszczenie

Kampania reklamowa "dołącz do Inferni" zrobiona przez Izę przy użyciu Eustachego jako modela - sukces. Klaudia zdobyła z tego nowych załogantów (acz to jeszcze potrwa). Szarjan uwolnił Klaudię z zarzutów, potrzebuje pomocy w stoczni - jego przyjaciółka Natalia ma problem z prototypem myśliwca klasy Nereida i została ixiońsko zintegrowana. Infernia próbowała Nereidę uratować, ale niestety skończyło się to poważnym uszkodzeniem Nereidy i stworzeniem ixiońskiego kokonu, z którego Natalia musi się wykluć...

## Progresja

* Eustachy Korkoran: jest solidnym bohaterem. Twardy i nie warto zaczepiać. Wszystko dzięki kampanii Izy.
* Natalia Aradin: konwersja ixiońska; jest w formie larwy ixiońskiej i się dopiero musi "wykluć" przez Skażenie Klaudii.
* OO Infernia: użyte przez Eustachego sprzężenie sprawiło, że Infernia poprzesuwała część dział robiąc strukturalne uszkodzenia jednostki.
* OO Infernia: dzięki działaniom Klaudii i kampanii reklamowej dookoła Eustachego ma załogę. Nie jest to NAJLEPSZA załoga, ale good enough. Za miesiąc mamy sprawną "nową" załogę Inferni.
* Klaudia Stryk: poparzona magicznie i mentalnie przez tydzień; za silna integracja z umierającą Persefoną itp.

### Frakcji

* .

## Zasługi

* Arianna Verlen: nie dołączyła Rolanda do załogi i dała mu odczuć że go nie chce. Wymyśliła jak zwabić Nereidę do Inferni, używając theme song Sekretów Orbitera. Dobrze pilotuje Infernię pozycjonując prawidłowo Elenę w kosmosie (jak minę).
* Eustachy Korkoran: model i twarz kampanii "dołącz do Inferni". Opracował plan przechwycenia Nereidy, ale pod wpływem gniewu złamał plan i zestrzelił okrutnie Nereidę. Zintegrowany z Infernią poranił "nowych" członków załogi.
* Klaudia Stryk: wybrała z lejka rekrutacyjnego WŁAŚCIWYCH przyszłych członków załogi, efektywnie odbudowując Infernię. Uwolniona z zarzutów przez Adama Szarjana, próbowała uratować Natalię Aradin przed śmiercią i niestety jedyne co mogła zrobić to ixioński kokon - Natalia się wykluje jako "coś".
* Maria Naavas: upiękniła Eustachego i po tym jak Eustachy / Infernia pokrzywdził "nową" załogę Inferni zajęła się ratowaniem żyć. Dlatego nie mogła pomóc Klaudii w ratowaniu Natalii...
* Izabela Zarantel: zrobiła świetną kampanię "dołącz do Inferni" by zdobyć załogę. Poszło jej rewelacyjnie - podniosła zwłaszcza szacun Eustachemu.
* Roland Sowiński: chciał się wprosić na Infernię, ale Arianna nie chciała. Wsadził Inferni kilku kompetentnych ludzi, co wszystkich zdziwiło (kompetentnych czemu? O_O). Protekcjonalny wobec Arianny, "małej buntowniczki" jak się mu przyznała.
* Arkadia Verlen: wysłała fanmail na orbitę, który dostała Iza Zarantel. Chce więcej Arianny. Mniej Eustachego i Klaudii ;-).
* Adam Szarjan: by uratować Natalię wyciągnął swoimi kanałami Klaudię z procesu i przekazał Inferni info o projekcie "Nereida". Niestety, jego marzenia się nie spełniły - Natalia ucierpiała.
* Natalia Aradin: oblatywaczka stoczni Neotik i przyjaciółka Adama Szarjana. Jakaś forma destabilizacji mentalnej sprawiła, że neurosprzęgła się z Nereidą po kanałach ixiońskich i doszło do Syntezy Ixiońskiej. Fanka Eustachego i Sekretów Orbitera. Jej Nereidę zestrzeliła Infernia i weszła w szok. Klaudia próbowała ją uratować, ale Eksplozja Skażenia otoczyła ją kokonem z ixiońskiej energii i transmetalu. Czeka na wyklucie się...
* Elena Verlen: nie umie dojść do siebie przez sytuację z Eustachym, ale jak trzeba było ratować Natalię z Nereidy, nie wahała się ani przez sekundę. Sama w kosmosie w Eidolonie czekała aż Eustachy ściągnię na nią Nereidę. Jednak nie ściągnął, więc siedziała sama w kosmosie i kontemplowała życie.
* Leona Astrienko: pokłóciła się ze swoim medykiem (Wawrzynem), rzuciła weń czymś wybuchowym a on odrzucił. Polubiła go trochę. Ma z kim się kłócić. Ma "swojego medyka".
* Wawrzyn Rewemis: stary medyk ściągnięty przez Klaudię; gdy Leona weń rzuciła czymś wybuchającym to odrzucił. He doesn't give a meow. Trochę polubili się z Leoną. Eustachy zaszokowany jego działaniem.
* OO Infernia: uruchomiona po raz pierwszy od dawna, z nowymi (tymczasowymi) członkami załogi; uczestniczyła w operacji zestrzelenia prototypu Nereidy. Zintegrowała się z Eustachym by dorównać prędkości Nereidy. Dzięki działaniom Klaudii i kampanii reklamowej dookoła Eustachego ma załogę.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Specjalne

* Adam Szarjan: @FRAKCJA: Orbiter, Stocznia Neotik
* Natalia Aradin: @FRAKCJA: Orbiter, Stocznia Neotik
* @DEFINICJA Nereida: @KLASA: myśliwiec @ASPEKTY: neurosprzężenie, sentisieć, ixion, Persefona

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria, Orbita
                1. Kontroler Pierwszy
            1. Astoria, Pierścień Zewnętrzny: obszar uważany za "pod wpływem planety Astorii", ale poza zasięgiem orbity czy studni grawitacyjnej.
                1. Stocznia Neotik: 4h odległości od Kontrolera Pierwszego; prowadzone są tam eksperymenty z różnymi jednostkami. Rodzina Szarjanów dowodzi tą stocznią.
                1. Poligon Stoczni Neotik: 2h odległości od Stoczni Neotik; tam latała "zintegrowana" Nereida z Natalią i tam doszło do bitwy Infernia - prototypowa Nereida
 
## Czas

* Opóźnienie: 3
* Dni: 4

## Konflikty

* 1 - Kampanię reklamową "dołącz do Inferni" centrujemy dookoła Eustachego. Iza zaczyna kompozycję i reklamówkę, Maria zwiększa seksowność Eustachego.
    * TrZ+3
    * VVVVV: ogromny sukces kampanii, viral, dobry materiał na statek, Elena nie ma z tym kłopotów, godne i sensowne osoby a Eustachy ma to w reputacji że jest twardy.
* 2 - Tymczasowa załoga Inferni. Jak dobrą szkieletową załogę Inferni udało się zdobyć? Klaudia składa SPRAWNĄ załogę korzystając z okazji.
    * TrZ+4
    * VVVXV: Klaudia ma tymczasową załogę, część się nada na Infernię na serio, da się wymieniać by chronić traumatyzowaną załogę. Roland wpakował tam agentów.
* 3 - Arianna chce zwabić Nereidę na czołówkę Sekretów Orbitera
    * TrZ+3
    * VV: Infernia zgrabnie unika ataków ze strony Nereidy
* 4 - Klaudia identyfikuje Infernię jako Infernię. Kod identyfikacyjny Arianny. Wysyła sygnał zapoznawczy.
    * TrZ+3:
    * V: Połączenie z Nereidą i chory, sprzężony umysł Nereidy
* 5 - Infernia wyrzuca Elenę w odpadki kosmiczne na poligonie. Elena się rzuca na Eidolon + jetpack. Arianna wyrzuca Elenę by Infernia się nie zorientowała
    * TrZ+3:
    * XV: Infernia uszkodzona; przeciążony generator pola. Elena na pozycji.
* 6 - Eustachy WPIERW wali serią torped spread by pikały, radary, jak w odległości chapną to detonują i shrapnel z chaosem, przeciążenie osłony. Niech Nereida -> zasięg Eleny.
    * ExZ+4 (jak lekki krążownik, spread) + 3Vg
    * XXX: Nereida niszczy pintki, zacięcia i awarie na Inferni, Nereida zestrzeliła pole Inferni. Infernia ma problem.
    * (+Magia: +3Vm -3X): Xz: "Infernia" włączyła wewnętrzne systemy autodefensywy. Są ranni z "nowej załogi Inferni". Infernia została też trafiona przez Nereidę. USZKODZENIA.
    * VmV: Eustachy się wściekł. Nereida poszarpana i praktycznie zniszczona.
* 7 - Klaudia próbuje uratować Nereidę i Natalię.
    * ExZM+3+3Or
    * Or: głęboka infekcja ixiońska dwustronna: Natalia / Persefona.
    * (+2 Vg) XmXz: Persefona umiera, za głęboka integracja z Natalią, Natalia w szoku
    * (+2 Vg) X: Klaudia zaczyna migotać jak nie odrzuci połączenia JAKO TAI. Klaudia poparzona magicznie przez tydzień.
    * VOm: Klaudia bezpieczna, jajko poza umierającą Nereidą. Skażenie Magiczne - jajko stało się anomalią ixiońską, kokonem na Natalię.

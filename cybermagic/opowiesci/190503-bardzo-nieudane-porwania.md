---
layout: cybermagic-konspekt
title: "Bardzo nieudane porwania"
threads: nemesis-pieknotki
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [190502 - Pierwszy Emulator Orbitera](190502-pierwszy-emulator-orbitera)

### Chronologiczna

* [190502 - Pierwszy Emulator Orbitera](190502-pierwszy-emulator-orbitera)

## Budowa sesji

### Stan aktualny

* Miasteczkowcy stwierdzili, że chomikowanie czarnych artefaktów to cholernie zły pomysł. Czarnym miejscem zostają Wolne Ptaki.
* Część Miasteczkowców, jak chciała Karla, przeniosło się do Wolnych Ptaków.
* Część Miasteczkowców woli odsprzedać artefakty Wolnym Ptakom, ale niewielka ich część.
* Wolne Ptaki stają się tyci bardziej znaczącą frakcją i lokalizacją.
* Pojawia się prototyp szlaku handlowego między Wolnymi Ptakami i Cieniaszczytem.
* Gdy Nikola będzie odpalać plany przemytu, Pięknotka będzie wiedziała (ścigacz)
* Artefakty Czarnej Technologii trochę są porozrzucane po terenie.
* Eliza Ira straciła swoją sieć szepczących kryształów. Nie połączy się z nikim aktywnie; teraz to może tylko odpowiadać na zapytania.
* Pięknotka wykorzystała swoje wszystkie znajomości - zaczyna małą kampanię antyElizową na tym terenie, "whispers from the crystals".
* Wpływ Elizy wzrósł. Są osoby wiedzące o niej i o jej ideałach i są osoby chętne by jej pomóc. Ale nie w Pustogorze i nie u kogokolwiek ważnego.

### Pytania

1. Czy Kirasjerom uda się zniszczyć lub przejąć Nikolę?
1. Czy Kirasjerom uda się zniszczyć lub przejąć Minerwę?
1. Czy ktokolwiek dowie się o działaniu Kirasjerów?

### Wizja

* Orbiter stracił dwa Emulatory, ale jeden jest w zasięgu Pustogoru. Wysłali więc ciężki oddział szturmowy Kirasjerów pod dowództwem Damiana Oriona.
* Orion ma za zadanie przechwycić Minerwę oraz Nikolę. Wysłał agentkę - Emulator - Mirelę Orion - by zinfiltrowała dom Minerwy.
* Kirasjerzy szukają Nikoli oraz Minerwy. Pięknotka musi ich zatrzymać zanim doprowadzą do przechwycenia obu.

### Tory

* Infiltracja - odkrycie Minerwy: 1
* Infiltracja - odkrycie Nikoli: 3
* Przechwycenie Nikoli: 5
* Przechwycenie Minerwy: 7
* Odparcie Kirasjerów: 3

Sceny:

* Scena 1: Emulator u Minerwy

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Misja właściwa

**Scena 1: Emulator u Minerwy**

Pięknotka jest schowana w mieszkaniu Minerwy. Wie, że pojawi się złodziej. Czeka w self-activating stealth power suicie i ma nadzieję że złapie złodzieja. I faktycznie, do domu wślizgnął się cień. Cień zaczął zbierać ślady biologiczne - szczoteczka i inne takie. Zostawił coś w szafce dla Minerwy.

Pięknotka wyczekała odpowiedniego momentu i zaatakowała z zaskoczenia. Cel: unieszkodliwić. Przeciwnik (Mirela) jest PIEKIELNIE wymagający; (TrZ+2:VV). Pełen sukces, przeciwnik jest niezdolny do walki. Pięknotka fachowo unieruchomiła złodziejkę. Ta czeka, nic nie mówiąc. Pięknotka wezwała odpowiednik policyjnej suki - ta będzie za 5 minut. Pięknotka próbuje przesłuchać Emulatorkę - ta jednak się nie odzywa, tylko powiedziała, że jest magiem.

Pięknotka zapowiedziała jej, że pojedzie do Barbakanu do systemu Pustogoru i będzie odpowiadać za magię krwi. Mirela się zdziwiła; Pięknotka próbkuje by zrozumieć co to za jedna i jak ona myśli (Tp:SS). Skonfliktowanie: polega na tym, że oni już są w suce. Pięknotka zobaczyła coś ciekawego - jej reakcje psychologiczne są błędne. Naprawdę błędne. Widziała to już kiedyś - Julia Morwisz. To Emulator, i to pewny siebie.

Pięknotka NATYCHMIAST wysłała komunikat do Karli, że w Pustogorze pojawił się kolejny Emulator. I ten jest kontrolowany, najpewniej przez Orbiter. Pięknotka NATYCHMIAST powiedziała Emulatorowi, że wie czym jest - jest Emulatorem Orbitera. Powiedziała, że Pustogor jest ostrzeżony. Emulatorka jest... zagubiona.

Suka Pustogorska przyjechała. Karla wysłała sygnał do Pięknotki "odwołałam pojazd, straciłam kontakt, uważaj". Ten pojazd jest w rękach Orbitera. Karla już wysłała oddział, będzie za parę minut. Pięknotka łączy się z Pustogorem hipernetem. Karla kazała jej się wycofać nawet bez Emulatorki, ale Pięknotka ma inny pomysł. Natychmiast łączy się z Ateną. Złapała Mirelę i wzięła ją na dach (konflikt) i poprosiła Atenę, by ta ją portowała jak najbliżej Trzęsawiska z Mirelą i Cieniem. (konflikt)

* Zachować Maskaradę
* Nie wyrządzić za dużych zniszczeń
* Przenieść Pięknotkę
* Przenieść Mirelę i/lub Cienia
* Przenieść blisko Trzęsawiska
* Nie przenieść INNEGO Emulatora
* Nie uruchomić Cienia

Druga strona nie ma zaskoczenia, ale Pięknotka też nie. Oni chcą zachować Maskaradę bardziej niż Pięknotka. Cztery agentki lecą do góry w power suitach; Pięknotka leci swoim. Pięknotka używa paskudnej magii chemicznej; Pięknotka na korytarzu zostawia za sobą coś, co robi porządne DUP i - co ważniejsze - rozpyla środki które powodują ostrą reakcję biologiczną. Coś w stylu zaawansowanych środków obezwładniających Sataraila.

TrZM+4(17,3,5=VV). Wszystko się udało. Atena (a dokładniej Kirył) skutecznie przeniosła Pięknotkę i Mirelę blisko Trzęsawiska, zostawiając Kirasjerów by wycofali się zanim uderzy Pustogor. Mirela zauważyła, że nie wie co Pięknotka chce osiągnąć - jeśli dalej tak pójdzie, ona zostanie zniszczona przez Orbiter. Są po tej samej stronie! Mirela jest wyraźnie zaniepokojona tym, że może trafić na Trzęsawisko widząc gdzie Pięknotka idzie.

Pięknotka się zatrzymuje i patrzy Emulatorce prosto w oczy. Bagno czy Pustogor. Mirela powiedziała z bezsilnością, że nie chce umierać. Pięknotka wie, czemu Orbiter może ją wysadzić - ale Orbiter nie wie, że Pięknotka wie. Tak więc mają podstawy nie wiedzieć, czemu. (Tp+1:10,3,4=S). Orbiter pozwolił Pięknotce na bezproblemowe ewakuowanie się do Pustogoru z Mirelą. Damian kazał oddziałowi się wycofać.

Pięknotka ma szatański plan. Spróbuje uwolnić Emulatorkę używając Cienia. Uruchamia Cienia, ale wysyła mu sygnał mentalny: "zrób dla niej to, co zrobiłeś dla Nikoli". Cień jest zaintrygowany...

* Cień zacznie współpracować z Pięknotką bardziej
* Połączenie Mireli z kontrolerem zostaje odpowiednio rozerwane; Mirela dostanie wolność (eventual freedom)
* Orbiter się nie orientuje w sytuacji

(TrZ+2:12,3,6= reroll, reroll, VV)

Pięknotka dała radę uruchomić Cienia w warunkach wyjątkowo kontrolowanych. Praktycznie poczuła śmiech Saitaera, gdy Emulatorka wrzasnęła, gdy Cień wniknął w nią, gdy Emulatorka nagle zaczęła mieć własne myśli i własne idee, chociaż nadal jest kontrolowana przez Kontrolera w Orbiterze. Oni tego nie widzą, Cień wysłał im właściwy obraz. I Pięknotka usłyszała imię. Mirela. Nazywa się Mirela. Po chwili Cień wycofał się z Mireli, ale wrażenie zostało. Pięknotka szeroko uśmiechnęła się pod Cieniem - jej uśmiech był zwierciadłem jego uśmiechu...

**Scena 2: Pancerz polityczny**

Mirela jest zamknięta w Bunkrach Barbakanu. To jest poważny problem polityczny - Pięknotka idzie porozmawiać z Karlą. Dostaje się do niej w ekspresowy i priorytetowy sposób. Karla jest... trudno powiedzieć czy zadowolona czy niezadowolona. She is amused. Z czymś takim się jeszcze nie spotkała.

Karla wyjaśniła Pięknotce, że Kirasjerzy Orbitera to najbardziej elitarna jednostka jaka istnieje. Najlepszy sprzęt, najsilniej wspomagani komandosi, po prostu - perfekt. A "zwykła terminuska pustogorska" zrobiła z nich idiotów. Karla nawet nie próbuje ukrywać uśmiechu czystej złośliwej radości.

Pięknotka poprosiła Karlę, by ta zablokowała działania Orbitera wobec Minerwy. Minerwa jest "ich", jest przydatna i w ogóle. Minerwa nie jest tylko bronią, nie jest tworem Saitaera, jest osobą i Pięknotka - zwłaszcza widząc traktowanie Emulatorek - chciałaby by Minerwa została w okolicy. Karla zauważyła, że to wymusiłoby na niej przesunięcie Minerwy do Pustogoru z Zaczęstwa. Dla jej własnego bezpieczeństwa. Nikt nie uchroni Minerwy przed atakiem Kirasjerów skierowanym do porwania. Pięknotka powiedziała, że MUSZĄ utrzymać Minerwę - zna się na broni Orbitera oraz dodatkowo jest najlepszym psychotronikiem. No i energia ixiońska. (Tp:S). Karla się zgodziła. Zapewni bezpieczeństwo Minerwy w Pustogorze i upewni się, że Kirasjerzy nie dotkną Minerwy.

Pięknotka jest szczęśliwa. Tak szczęśliwa jak może w tej sytuacji być. Minerwa będzie mieszkać w Miasteczku i będzie mieć szersze prawa - może czarować. A ona (Pięknotka) nie musi jeździć do Zaczęstwa. O.

Czas odwiedzić Minerwę w szpitalu. Pięknotka poszła i - o dziwo - nie spotkała się z Lucjuszem Blakenbauerem. Pięknotka przeprosiła Minerwę - przez to, że ta pomogła przy Cieniu to zainteresował się Minerwą Orbiter. Wysłali po nią Kirasjerów. Pięknotka wyjaśniła Minerwie, że załatwiła jej większą swobodę działania. W Miasteczku są misfity, ona sama może tam więcej robić. (TpZ+1:13,3,3=S). Minerwa się zgodziła na przeprowadzkę, wyraźnie spłoszona Kirasjerami bardziej, niż zmartwiona utratą autonomii.

Pięknotka wyszła ze spotkania z szerokim uśmiechem. Jeszcze jedna sprawa.

Pięknotka odwiedziła Karlę raz jeszcze. Powiedziała jej, że właśnie się zorientowała. Nikola jest Emulatorem. Orbiter chce ją przejąć. Czy Karla może ochronić Nikolę politycznie? Orbiter nie jest w stanie wtedy się do niej dobić - musi iść przez Pustogor. Karla powiedziała, że to jest ogromny koszt polityczny. Pięknotka powiedziała, że Nikola może być agentką Pustogoru po drugiej stronie, przy Wolnych Ptakach. Karla się zastanowiła. Ona nie chce, by Nikoli stała się krzywda a na pewno stałaby się straszna krzywda w Orbiterze.

(TrZ+2:S). Pięknotka wzbija się na wyżyny przekonywania. Karla się zgodziła. Niech Pięknotka przekona Nikolę.

**Scena 3: Polowanie na Nikolę**

Pięknotka, Alan i hovertank klasy Timor. Czas pojechać spotkać się z Nikolą. Pięknotka miała pewne wątpliwości co do wyboru pojazdu przez Alana, ale Alan szybko udowodnił, że tym hovertankiem jest w stanie trafić bardzo mały cel w bardzo dużej odległości i działa bardzo szybko. Pięknotka aż była zaskoczona.

Pięknotka zostawiła Alana w oddali i włączyła Cienia. Chce dostać się do Nikoli w jej ścigaczu - poczekała na moment, w którym Nikola miała swoje chwile "patrzenia w gwiazdy". Nie pcha się do Pacyfiki. Znalazła Nikolę. Próbuje ją przekonać - niech Nikola przyjmie umowę Karli. Celem Kirasjerów nie będzie jej zabić, oni chcą ją złapać. Pięknotka oferuje Nikoli możliwość życia tutaj jako enforcer Pustogoru. Może chronić Wolne Ptaki przed anomaliami, przed przemytem... chodzi o to, by nie była pistoletem. By była pomocna. By chroniła Astorię.

Nikola się uśmiechnęła. By wyrzekła się zemsty? Może nie zniszczy wszystkich Kirasjerów, ale może coś jej się uda zrobić.

(TrZ+2:SSZ).

* Nikola nie przyjmie pracy, nie pracuje dla Pustogoru.
* Nikola nie będzie robić przemytu, działać przeciw Pustogorowi; będzie chronić okolicę przed anomaliami.
* Nikola zostanie tu przez co najmniej pół roku. I Nikola jest chroniona przez pół roku przez Karlę.
* Jeśli Nikola opuści ten teren czy złamie zasady, she is fair game.

Pięknotka jest zadowolona. Dokładnie o to jej chodziło. Nie do końca tak, ale wystarczająco dobrze.

**Sprawdzenie Torów** ():

* Infiltracja - odkrycie Minerwy: 1: 1
* Infiltracja - odkrycie Nikoli: 2: 2
* Przechwycenie Nikoli: 5: 4
* Przechwycenie Minerwy: 7: N/A
* Odparcie Kirasjerów: 3: 1

**Epilog**:

* Minerwa zamieszkała w Miasteczku. Jest w Pustogorze pod ochroną - Orbiter ma się odmiauczeć.
* Nikola porzuca działania antypustogorowe. Jest pod ochroną Pustogoru przed Orbiterem.
* Kirasjerzy byli blisko, mieli już agentów po drugiej stronie. Na rozkaz odwołali - Damian nie chciał konfliktu z Karlą.
* Mirela jest uwolniona. Jest na tyle inteligentna, by wiedzieć, co z tym robić i by się nie zdradzić.
* Kirasjerzy dostali po reputacji. Damian dostał OPIERDOL. Damian skaskadował OPIERDOL w dół. Elitarna siła.
* Pięknotka i Cień mają lepsze porozumienie po tym uwolnieniu Mireli

## Streszczenie

Kirasjerzy Orbitera dostali zadanie odbicia Minerwy i Nikoli, ale w mieszkaniu Minerwy czekała Pięknotka. Porwała z zaskoczenia Emulatorkę, uwolniła ją Cieniem i gdy Kirasjerzy uderzyli by po cichu odzyskać agentkę, Pięknotka poprosiła o pomoc Epirjon. W wyniku Pięknotce udało się ewakuować z Emulatorką, uwolniła ją Cieniem i wsadziła do Pustogoru. Potem z Karlą wynegocjowała ochronę dla Minerwy i Nikoli i jeszcze z tymi dwoma załatwiła by to było możliwe. Kirasjerzy zostali odepchnięci przez JEDNĄ terminuskę Pustogoru.

## Progresja

* Pięknotka Diakon: zdobyła 'rapport' u Cienia za uratowanie Emulatorki Kirasjerów (Mireli)
* Pięknotka Diakon: zdobyła szacunek Kirasjerów Orbitera za to, że samodzielnie dała radę wyślizgnąć się z ich operacji
* Mirela Orion: Emulatorka uwolniona przez Cienia; nie jest już lalką w rękach kontrolera. Trzeci wolny Emulator.
* Minerwa Metalia: chroniona przed Orbiterem; przeniesiona do Miasteczka w Pustogorze i dano jej autonomię do czarowania i eksperymentów
* Nikola Kirys: jak długo współpracuje z Pustogorem chroniąc Wolne Ptaki i nie robiąc nic anty-Pustogorowi, chroniona przed Orbiterem. Uziemiona na tym terenie.

### Frakcji

* Kirasjerzy Orbitera: elitarny oddział Orbitera, odepchnięty przez jedną Pięknotkę Diakon - i jeszcze stracił Emulatorkę do tego.

## Zasługi

* Pięknotka Diakon: wpierw porwała Kirasjerom Emulatorkę, potem ją uwolniła Cieniem, potem wycofała do Pustogoru i jeszcze politycznie zabezpieczyła Minerwę i Nikolę przed nimi. Lol.
* Mirela Orion: Emulator Kirasjerów; uwolniona przez Cienia. Pokonana z zaskoczenia przez Pięknotkę, skończyła w lochu w Pustogorze przed zwróceniem jej Kirasjerom.
* Damian Orion: dowódca Kirasjerów; zaplanował operację, dowodził operacją, stracił Mirelę i nie przewidział Epirjona. Drugi raz nie zrobi tego błędu.
* Karla Mrozik: amused commander; Pięknotka dała jej możliwość wbicia bolesnej szpili elitarnym Kirasjerom. Chroni Minerwę i Nikolę przed szponami Orbitera.
* Minerwa Metalia: polowali na nią Kirasjerzy; Pięknotka przekonała ją do zamieszkania w pustogorskim Miasteczku. Tam jest bezpieczna.
* Nikola Kirys: polowali na nią Kirasjerzy; przez pół roku chroni Pustogor i Wolne Ptaki przed anomaliami i problemami, za co ma ochronę przed Orbiterem.

## Plany

* Nikola Kirys: zostają jej plany: ochrona lokalnych, zemsta na odpowiedzialnych magach Orbitera (nie na wszystkich), zniszczenie reputacji Orbitera

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Pustogor
                                1. Rdzeń
                                    1. Szpital Terminuski: znowu miejsce spotkań Pięknotki i Minerwy
                                1. Interior
                                    1. Bunkry Barbakanu: miejsce, gdzie Pustogor przetrzymywał Mirelę (Emulatorkę Kirasjerów)
                            1. Zaczęstwo
                                1. Osiedle Ptasie: w domu Minerwy, starcie między Pięknotką a Emulatorem Kirasjerów
                            1. Zaczęstwo, obrzeża: tam teleportował Epirjon Pięknotkę z Mirelą, by Pięknotka miała bliżej do Trzęsawiska
                            1. Podwiert
                                1. Kosmoport: miejsce, gdzie znajduje się zakamuflowany pojazd szturmowy klasy "Turbinis" należący do Kirasjerów (na 10 osób)
                1. Sojusz Letejski, SW
                    1. Granica Anomalii
                        1. Pacyfika, obrzeża: spotkanie i negocjacja między Nikolą a Pięknotką odnośnie ochrony Nikoli przez Pustogor

## Czas

* Opóźnienie: 2
* Dni: 2

---
layout: cybermagic-konspekt
title: "Ostatnia akcja bohaterki"
threads: legenda-arianny, niestabilna-brama
gm: kić
players: żółw, fox
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [211114 - Admirał Gruby Wieprz](211114-admiral-gruby-wieprz)

### Chronologiczna

* [211114 - Admirał Gruby Wieprz](211114-admiral-gruby-wieprz)

### Plan sesji
#### Co się wydarzyło

.

#### Sukces graczy

* 

### Sesja właściwa
#### Scena Zero - impl

.

#### Scena Właściwa - impl

Eustachy musi zostać na K1. Tłumaczy inwestorkom (arystokratkom) co się dzieje z Infernią, dlaczego Infernia nie jest jeszcze w Anomalii Kolapsu itp. Sympozjum gdzie Eustachy próbuje odpowiadać na nadmiar pytań arystokratek. Nie chciał, ale Kramer go do tego oddelegował.

Klaudia - ktoś chce połączenie z TIEN Hiwasserem. Martyn odebrał w kabinie. Powiedział, że to kwestia przeszłości.

Jolanta Kopiec. Pożerana przez Esuriit. Powiedziała, że wie o tienie eternijskim pożartym przez Esuriit - Teofil Kret. I ona może go wyczuć. Albo zabójca DZISIAJ albo próba zrobienia czegoś dobrego jeszcze raz. Martyn poprosił Klaudię o złożenie raportu. Sam poszedł do Arianny - poprosił by pozwoliła wykonać tą operację dla swojej starej przyjaciółki. Arianna stwierdziła, że warto - ale niech Martyn zapewni od Jolanty coś więcej. Np. niech następca pomoże w walce przeciwko Krypcie. Martyn spoważniał - mówi o tym, że Jolanta jest bohaterką wojenną. Wiele oddała już Eterni i Orbiterowi. Niekoniecznie ma więcej do oddania. Arianna zaakceptowała; nie chce by Martyn był smutny i zły. Rozumie jego punkt widzenia.

Czy raport Klaudii jest przekonywujący?

TrZ+3:

* X: Klaudia znajdzie DUŻO ale nie WSZYSTKO. Nie da się ocenić co można zrobić
* X: Teofil jest ostrzeżony.
* Vz: Do Kramera przemówi. Autoryzuje operację.

TAK, ale to wszystko. Nic z tym nie zrobimy.

Arianna x Kramer: wpadliśmy na trop zamieszanego maga eternijskiego, można wytropić i zniszczyć. Raport Klaudii wyjaśnia jak najwięcej prawdy - jak działa to wszystko, że biorą eternijkę na pokład itp. Kramerowi się to nie do końca podoba. Ale w raporcie jest info że Leona jest zabójczynią Esuriit.

TrZ+3:

* X: Jednostka do eskorty, gotowa do rozstrzelania Inferni.
* V: Autoryzowana operacja.
* X: Trzeba przekonać Olgierda TO Kramer się zgodzi.

Arianna musi przekonać Olgierda by chciał polecieć. "Słuchaj Olgierd, potrzebuję eskorty. Jak nie polecisz Ty, taki jeden arystokrata ze mną poleci..." przy kawie. "Nie poradzisz sobie z jednym pachołem?" "Poradzę, ale Kramer się martwi". "Martwi?" "Potrzebuję statek do obstawy. A ten Sowiński jest strasznie napalony by być blisko mnie. Zaczynam powoli mieć dość jego twarzy na wideo i NIE CHCĘ by za mną leciał". Ale - Arianna mówi - ludzie powoli KOJARZĄ ZE SOBĄ Ariannę i Olgierda. I nie może mieć kłopotów z Sowińskimi.

Arianny nie boli bardzo że Olgierd sklupie Rolanda Sowińskiego, ale wolałaby, by Olgierd nie miał potem problemów.

Tr+3:

* V: Olgierd skroi Rolanda. Nie będzie poważnych kłopotów.
* X: Powiążą z Olgierdem ale nie z Arianną.

Czyli mamy zapewnione wsparcie Żelazka. Infernia (z Jolantą Kopiec) + Żelazko --> siną dal.

Arianna obiecała Leonie starcia w klatce. Leona poprosiła o emiter anihilacji. + Martyn przebuduje Leonę pod kątem walki z simulacrum. Kramer też - wiedząc, że na pokładzie nie ma Eustachego - zgodził się na emiter anihilacji. Więc przebudowujemy Leonę w trakcie, lecimy Infernią.

Na pokład Inferni wchodzą agenci eternijscy. Czterech ochroniarzy (w tym ów szef ochrony Noctis z tajemniczej jednostki z 210918-pierwsza-bitwa-martyna) i Jolanta. Martyn ich przedstawia. Potem Martyn opowiada o Sekretach Orbitera. Zaprowadził Jolantę i ochroniarzy do medbay i przypomina "osiągnęliśmy to wszystko", puszcza Sekrety Orbitera itp.

A Leona ogląda sobie po monitorach w jakim stanie jest Jolanta. Bawi się emiterem anihilacji (blasterem) - ładuje i rozładowuje, aż Klaudia jej zabiera XD.

Martyn stabilizuje Jolantę Kopiec. Ale ona chce obejrzeć statek. Martyn chce ją oprowadzić po statku. Arianna zaproponowała rozstawienie kultystów i pomoc Kamila, a Klaudia odpowiednio wytycza trasę, załoganci opowiadają różne rzeczy. Ogólnie - "sielanka". Stabilizacja przez podróż.

ExZ+4:

* Vz: Jolanta doleci żeby wykonać swoją misję
* V: Uhonorować bohaterkę wojenną. Daje większą stabilność. Większy czas w starciu z przeciwnikiem, większa.

Ustabilizowawszy ją, Martyn robi "wieczorek z tien Kopiec". Rozmowa o wspomnieniach, o Eterni. Arianna zaproponowała coś innego - Izabela wizualizująca "bohaterka Eterni na ostatniej akcji" - Arianna była zobowiązana zrobić Dobry Odcinek O Eterni a to jest mega cukiereczek. Izabela jest dobra w tego typu tematach. A Jolanta opowie, da dobre rady, opowie o najciekawszych i najbardziej wartościowe rzeczy.

Izabela tworzy doskonały odcinek Sekretów Orbitera - część wizerunku Eterni, ale przede wszystkim jedna Jolanta Kopiec. Ten odcinek to jak na razie Opus Magnum Izabeli.

ExZ+4:

* Vz: Odcinek zrobi robotę dla Arianny - Arianna będzie czysta w oczach Eterni.
* X: Opowieść o Martynie, simulacrum i wyssaniu energii
* V: Robi robotę pokazując Jolantę jako człowieka, osobę która dba o swoich ludzi i która przysłużyła się Orbiterowi i Astorii podczas wojny.
* X: Nagle WSZYSCY wiedzą, że Martyn Hiwasser to jest TEN Martyn Hiwasser - i jest na Inferni. Czyli Martyn i Infernia -- cel. PLUS informacja, że Martyn był prawdziwym tien Eterni. Pod Jolantą - w jej sieci.
* V: Więcej jest takich bohaterów jak Jolanta. I te opowieści mogą być. PLUS info, że większość eternian nie chce programu kosmicznego Eterni.
* V: Infernia udowodniła, że nie jest wroga Eterni. Jolanta sama powiedziała 'ten statek ma takie zasady o jakie nam chodziło'.
* X: Martyn będzie musiał używać pełni swojej mocy i umiejętności by odepchnąć energię Jolanty. Jolanta ma dość mocy, by zniszczyć tamtego maga i siebie - więc Leona NIE ZNISZCZY MAGA ETERNIJSKIEGO. A jedynym tien Eterni jest Martyn. PLUS eternianie mówią o nim 'tien'.
* Vz: Jolanta odejdzie w pokoju. Wiara w nią jako w bohaterkę "narodową". Wiara w nią Inferni. Nastawienie załogi Inferni - nie jej sieć ale Kamil jest super.

LEONA SKONFLIKTOWANA. Zamknęła się w swojej kabinie. Monitoruje. Olgierd jest rozczarowany - miały być przyjacielskie sparingi z Leoną, a ona nie chce wyjść.

PARĘ DNI PÓŹNIEJ:

Im bardziej zbliżają się do celu, tym bardziej Jolanta poważnieje. Dolatują w okolice trochę jak zgrupowanie asteroid. Tam coś może być...

OO Żelazko robi dywersję. Olgierd chce się mega popisać: +3Or. A Infernia ma świetne czujniki, Klaudię i doskonałe mechanizmy. PLUS - sprzęgamy. +4Og. PLUS - Elena NAPRAWDĘ wlatuje Gwieździstym Ptakiem: Jolanta, ochroniarze, Martyn, Leona, Arianna. Olgierd dowodzi bitwą kosmiczną. A na mostku ląduje... KLAUDIA. Świat się kończy -_-.

ExZ+3+3Or+4Og+3g

INSERCJA JAK NAJBLIŻEJ CELU. Jak za starych dobrych lat.

* Xz: Żelazko dostaje ciężką artylerią. Ma ciężko. Gościu najpierw przejął ludzi po czym odkrył bazę piratów. I ją przejął. Piraci kiepsko sobie radzili ale eternijski administrator Esuriit. Ufortyfikował, wzmocnił itp.
* X: Żelazko jest w ruinie.
* V: Uda się Gwieździstemu Ptakowi wylądować i zrobić zrzut.
* X: Infernia zmuszona do wycofania. Jesteśmy my kontra baza.
* V: Chaos. Elena pląsa jak helikopterem, odciągając ogień na siebie.
* Vz: Żelazko się wbiło - lancą plazmową wypaliło reaktor. Sensory itp. padają, wchodzi na awaryjne. Wchodzenie na awaryjne trwa.
* X: PIRACI mają swoje jednostki latające. Infernia musi chronić ciężko uszkodzone Żelazko. Sama będzie uszkodzona.

LEONA AGRESYWNIE NIE WIDZI JOLANTY.

Plan jest prosty - jesteśmy w bazie. Baza nie jest Skażona Esuriit jako taka. Cel: ufortyfikować tą sekcję - ustawić na odciętym terminalu że niby tu jesteśmy a my się przemieszczamy gdzieś dalej. Czyli fałszywa fortyfikacja. Mając to - Leona przodem (największa szansa na lokalizację). A Elena może Ptakiem robić drogę i się przebijać i odwraca uwagę od Leony. Ostrzeliwywać, dywersja itp.

Cel - mieć dojście do przeciwnika.

ExZ+3+3Og:

* V: Jest trasa, wiadomo jak tam dotrzeć. Leona wykryła.
* X: Wrogowie wiedzą gdzie jesteśmy i idziemy.
* Vz: Mamy swobodne dojście. Jesteśmy niedaleko maga Esuriit.
* Xz: Leona opadła banda minionów maga Esuriit. Ma mało amunicji, więc walczy wręcz. ZASWARMOWALI JĄ. Nec Hercules contra plures. Wygrała, ale jest ranna. Utrzymała teren. Nie nadaje się do walki z Simulacrum. 
* X:(+M) Fala mocy zaczęła korupcję jednego z ludzi Jolanty. Wyrwał sobie z klatki rubin. To go zabiło. Sam się zabił. Nie zawahał się ułamka sekundy.
* X: Wróg wie o WSZYSTKIM poza emiterem anihilacji.
* O: Moc Arianny powoduje straszne Skażenie. Ta baza zaczyna stawać się "Kryptą". Baza zaczyna zwalczać sama siebie.

Przeciwnik jest rozproszony, ale jest bardziej niebezpiecznie. Są fale magiczne. 

Martyn ma plan. Otworzyć żyłę, zabarwić magię Arianny Esuriit by była ILUZJA, że Arianna przygotowuje się do czaru strategicznego Esuriit. Wróg nie ma wyjścia - MUSI wejść w simulacrum. To sprawia, że my powiemy Leonie "teraz". I ona wystrzeli emiterem. PLUS ELENA - zalśni magią krwi by odwrócić uwagę. 

ExMZ+3Ov (baza piratów) +3Og (Elena, mag krwi) +4:

* Vm: Kret wierzy, że to ARIANNA chce wzmocnić przyzwane simulacrum. Jego simulacrum nas atakuje.
* X: Leona w bardzo ciężkim stanie, chroniąc wszystkich przez simulacrum.
* Vm: Nie ma połączenia z przeciwnikiem, ale silniejszy feedback niż się ktoś spodziewał. Arianna strzela z emitera prosto w Leonę, która jetpackiem się odbiła, zostawiając rękę przy simulacrum. Leona straciła przytomność - zbyt ciężko ranna. Jolanta przekradła się do oszołomionego przeciwnika. Musi się przebić do Esuriitowej części bazy i przebić się przez miniony.
* (+2Ov): Arianna przemawia do "swojej bazy piratów" jako kapitan Plugawego Jaszczura. Jej część bazy zaczyna się rozpełzać. Toruje drogę Jolancie. I miniony Esuriit - eks piraci - na MOMENT się zawahali. PIRACI ODZYSKUJĄ CO ICH! Gdy Jolanta przygotowuje się do zadania finalnego ciosu swoim simulacrum - wrogie simulacrum się pojawia. Transfer energii.
* X: Jolanta MUSIAŁA... nie, CHCIAŁA. Nie mogła z tym walczyć. Pożarło ją Esuriit. Coś co zrobiła na sam koniec - cały czas ostatnie dni "skończenie istnienia TEGO potwora". ON BĘDZIE MÓJ. Jej simulacrum rozdarło jego simulacrum na drobny mak po czym ona wzięła kryształ i wpakowała kryształ w niego - UZYSKAĆ LEPSZE POŁĄCZENIE. Pożreć. Zabić.
* X: Ten noktiański szef ochrony TEŻ jest zabójcą. To on ją zabił, poświęcił siebie... więc Martyn ma niepotrzebną śmierć na sumieniu. Blaster obejmie wszystkich.

Po jego śmierci - potężne uderzenie energii. Wykończył większość swoich minionów.

Arianna wezwała siły sprzątające. Infernia + ciężko uszkodzone Żelazko wracają. Co odkryły siły sprzątające - gościu siedział kilkanaście lat. Przejął tą bazę, zaczął rozbudowywać coraz bardziej tracił kontrolę. Coś co potężnie rzuciło czyścicielami - zbudował krwawą Aleksandrię. Stąd drugie simulacrum. To miejsce jest na pełnej kwarantannie... wiadomo jak działa infekcja Esuriit.

Zespół który tam był (Arianna, Martyn, Leona, Elena) - tydzień kwaranny i badań... a Leona ma miesiąc w plecy. Znowu.

## Streszczenie

Z Martynem skontaktowała się tien Kopiec prosząc o ostatnią misję zanim Esuriit ją pożre. Martyn zorganizował lot Infernią. Ustabilizował Jolantę, by ta dała radę dolecieć słowami i opowieściami, przy okazji tworząc piękną opowieść o Eterni i eulogię dla Jolanty - bohaterki wojennej - w Sekretach Orbitera (przy okazji wyszła jego przeszłość). Infernia zniszczyła Krwawą Bazę Piratów kosztem dewastacji Żelazka. Jolanta zniszczyła krwawego maga. Sektor jest bezpieczniejszy, Eternia szczęśliwa a Arianna ma profity.

## Progresja

* Arianna Verlen: Eternia jest zachwycona odcinkiem Sekretów Orbitera z Jolantą Kopiec. Arianna spłaciła wszelkie długi.
* Leona Astrienko: 23 dni od końca tej sesji w szpitalu na Kontrolerze Pierwszym.
* Leona Astrienko: widzi Martyna jako tiena Eterni - pokazał czynami, zdolnościami i reputacją. EXTREMELY CONFLICTED.
* Martyn Hiwasser: stracił relację z Leoną i on nie jest w stanie jej odbudować. Ona może - on nie.
* Martyn Hiwasser: rozpoznawany przez Eternian za "swojego człowieka". Lubiany, podziwiany i zwyczajnie kochany w Eterni. "Nasz tien w kosmosie".
* Martyn Hiwasser: wydało się, że jest tien honorowym tienem Eterni. Wszyscy jego wrogowie wiedzą gdzie jest. Cała przeszłość go dogoniła.
* Jolanta Kopiec: może pośmiertnie, ale została uhonorowana jako wybitna bohaterka zarówno wojenna jak i Eterni.
* OO Żelazko: bardzo ciężko uszkodzone; 2 miesiące refitu.
* OO Infernia: tydzień z głowy na pomniejsze naprawy. Całość ognia wzięło na siebie Żelazko. 
* OO Infernia: jedyna jednostka Orbitera, która cieszy się zaufaniem i sympatią ze strony większości Eterni.

### Frakcji

* Eternia: bardzo podniesiona reputacja w oczach wielu osób Orbitera po Sekretach Orbitera z Jolantą Kopiec.

## Zasługi

* Arianna Verlen: przekonała Kramera, zdobyła Olgierda, przekonała Leonę by ta nie polowała na tien Kopiec (daje szansę zabicia DWÓCH Eternian) by na końcu służyć jako dywersja dla Jolanty... i oczywiście Skaziła bazę piratów, ożywiając ją po swojemu. Aha, użyła emitera anihilacji. Spodobał jej się.
* Martyn Hiwasser: załatwił dla tien Kopiec Infernię (Klaudia, Arianna). Zatrzymał Ariannę przed próbą ugrania nagrody od Eterni za Jolantę. Utrzymał ją by dotarła do bazy piratów (opowieści, Izabela). Doprowadził do sławienia jej legendy, kosztem swojej reputacji i bezpieczeństwa. Zapewnił emiter anihilacji (Arianna). Zaplanował z tien Kopiec jak dorwać odszczepieńca Eterni. Zabarwił moc Arianny swoją krwią by zmusić wroga do działania, używając eternijskiej magii (kosztem relacji z Leoną). Martyn dla Jolanty Kopiec - jej reputacji i spokoju ducha - oddał praktycznie wszystko co mógł.
* Leona Astrienko: cieszyła się że zabije DWÓCH tienów eternijskich - Arianna przekonała ją by ta poczekała. Wpierw rozproszona NIE utrzymała terenu (armia minionów ją poraniła), POTEM ryzykując życiem utrzymała simulacrum odstępcy (miesiąc+ szpitala), ale serce jej się złamało, gdy dowiedziała się, że Martyn to honorowy eternijski tien. Jest w szpitalu i nie chce z nikim rozmawiać.
* Jolanta Kopiec: KIA. Pożerana przez Esuriit, spełniła swoją ostatnią misję. Poprosiła Martyna i poszła zniszczyć apostatę eternijskiego - dużo silniejszego od niej. Uratowała wiele żyć, skończyła uhonorowana jako bohaterka wojenna którą była.
* Antoni Kramer: pod wpływem raportu Klaudii i prośby Arianny dał się przekonać do autoryzowania Inferni do zniszczenia Krwawej Bazy Piratów.
* Elena Verlen: MVP manewrów. Wprowadziła Ptakiem ekipę do Krwawej Bazy Piratów, odciągała ogień i ostrzeliwała cele W TEJ BAZIE.
* Olgierd Drongon: Arianna przekonała go do pomocy prosto - "Roland ją podrywa. Halp". By ją ratować i się popisać prawie zniszczył Żelazko. Ale bez tego akcja byłaby niemożliwa.
* Roland Sowiński: NIC NIE ZROBIŁ. Ale Arianna powiedziała Olgierdowi, że się do niej zaleca i Olgierd zrobi mu wpierdol.
* Izabela Zarantel: nienawidzi Eterni - ale stworzyła najlepszy odcinek o Eterni jak to było możliwe. Jej opus magnum. Teraz się szczerze nienawidzi.
* Klaudia Stryk: na prośbę Martyna zrobiła raport dla Kramera przekonywujący go do autoryzacji akcji. To bardzo dobry raport jest.
* OO Żelazko: wezwane przez Ariannę jako wsparcie, wzięło cały ogień anomalicznej bazy pirackiej. Umożliwiło Jolancie Kopiec skuteczną misję, kosztem części załogi i strasznym zniszczeniom statku.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Krwawa Baza Piracka: eternijski szalony mag zneutralizowany przez Jolantę Kopiec (ostatnia akcja). Baza ożywiona przez Ariannę. Bezpieczniej w sektorze - ale baza jest żywa i jest nadal.

## Czas

* Opóźnienie: 3
* Dni: 15

## Inne

.

## Konflikty

* 1 - Czy raport Klaudii jest przekonywujący dla admirała Kramera by mógł zrobić akcję przeciw Krwawej Bazie Pirackiej?
    * TrZ+3
    * XXVz: Teofil jest ostrzeżony; Klaudia nie znalazła WSZYSTKIEGO; Kramer autoryzuje operację
* 2 - Arianna -> Kramer: wpadliśmy na trop zamieszanego maga eternijskiego, można wytropić i zniszczyć.
    * TrZ+3
    * XVX: Potrzebny jest eskortowiec co może zniszczyć Infernię; Kramer autoryzował; Żelazko jak się zgodzi Olgierd
* 3 - Arianny nie boli bardzo że Olgierd sklupie Rolanda Sowińskiego, ale wolałaby, by Olgierd nie miał potem problemów (neutralizuje).
    * Tr+3
    * VX: Olgierd pobije Rolanda; powiążą z Olgierdem, ale nie z Arianną
* 4 - Martyn stabilizuje Jolantę Kopiec. Ale ona chce obejrzeć statek. Załoganci opowiadają różne rzeczy. Ogólnie - "sielanka". Stabilizacja przez podróż.
    * ExZ+4
    * VzV: Jolanta doleci by wykonać misję; uhonorowanie bohaterki wojennej
* 5 - Izabela wizualizująca "bohaterka Eterni na ostatniej akcji" - ofcinek Sekretów Orbitera. Część wizerunku Eterni, ale przede wszystkim jedna Jolanta Kopiec.
    * ExZ+4
    * VzXVXVVXVz: Arianna spłaciła Eternię; historia Martyna na jaw i wrogowie wiedzą kto i gdzie; Jolanta jako bohaterka i jest więcej bohaterów; Eternia lubi Infernię; Jolanta odejdzie w pokoju. Wiara w nią jako w bohaterkę "narodową".
* 6 - INSERCJA JAK NAJBLIŻEJ CELU (Infernia, Żelazko, Ptak). Jak za starych dobrych lat.
    * ExZ+3+3Or (Olgierd się popisuje)+4Og (sprzęgnięcie przez Klaudię) +3g (Elena + Ptak)
    * XzXVXVVzX: Żelazko w ruinie; Ptak zrobił zrzut; Infernia + Żelazko uciekają; Elena MVP dywersji; Żelazko wypaliło lancą reaktor; piraci mają myśliwce
* 7 - Leona przodem (największa szansa na lokalizację). Elena odwraca uwagę. Cel - mieć dojście do przeciwnika.
    * ExZ+3+3Og
    * VXVzXz: trasa; wrogowie tu idą; swobodne dojście do maga Esuriit; Leona utrzymała ale ranna (nie nadaje się do walki z simulacrum)
    * (+M) XXO: fala mocy zabiła człowieka Jolanty; wróg wie o WSZYSTKIM poza emiterem anihilacji; Arianna skaziła ("ożywiła") bazę
* 8 - Martyn ma plan. Otworzyć żyłę, zabarwić magię Arianny Esuriit by zmusić wroga do włączenia simulacrum + emiter anihilacji Leony.
    * ExMZ+3Ov (baza piratów) +3Og (Elena, mag krwi) +4
    * VmXVm: atak simulacrum TERAZ; Leona ciężki stan ochroniła; silny feedback, Leona b.ciężko ranna ale simulacrum wyparowało.
    * (+2Ov) VXX: Arianna przemówiła do "bazy piratów" i otworzyła Jolancie drogę; DRUGIE simulacrum więc Jolantę pożarło Esuriit; Martyn zabił "swoich".

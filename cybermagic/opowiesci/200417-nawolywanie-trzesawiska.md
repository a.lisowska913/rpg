---
layout: cybermagic-konspekt
title: "Nawoływanie Trzęsawiska"
threads: nemesis-pieknotki
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [200414 - Arystokraci na Trzęsawisku](200414-arystokraci-na-trzesawisku)

### Chronologiczna

* [200414 - Arystokraci na Trzęsawisku](200414-arystokraci-na-trzesawisku)

## Budowa sesji

### Stan aktualny

.

### Dark Future

* Sabina idzie na Trzęsawisko, zarażona Klonami
* Myrczek idzie na Trzęsawisko, zarażony Klonami
* Pewna populacja Zaczęstwa i okolic się zaraża nowym grzybem i idzie na Trzęsawisko

### Pytania

1. .

### Dominujące uczucia

* .

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Punkt zero

.

## Misja właściwa

Pięknotka ma relatywnie dobry dzień. Aurum co prawda upomniało się o arystokratów, ale tylko jeden z nich został wypuszczony - Franciszek Leszczowik. Sabina Kazitan została w Pustogorze; osoby które się o nią upomniały przegrały. Pustogor mógł zatrzymać jedną osobę - zatrzymał Sabinę (mniej niebezpieczne politycznie). Pięknotka też sprawdziła, tak na wszelki wypadek, jak Sabina działa w ramach prac społecznych - pomaga w Domu Weteranów. I co trzeba jej oddać - Sabina nie unika pracy. Dostała polecenie i je wykonuje. Jak dalej tak pójdzie, wyjdzie za dobre sprawowanie? ;-). Nie, na pewno nie (jeszcze 2 tygodnie) - ale przynajmniej coś się nauczy.

Dlatego tak bardzo popsuł się jej humor gdy dostała wiadomość od Strażniczki Alair. Sabina Kazitan była widziana w Zaczęstwie. Nic nie broiła, ale wyraźnie czegoś szukała i używała magii. Ale co dokładnie robiła - Strażniczka nie wie. Ma tylko drony. Pięknotka ciężko westchnęła. Sabinie tak dobrze szło... żeby tego nie spieprzyła...

Pięknotka poprosiła Strażniczkę, by ta oddelegowała dronę do pilnowania Sabiny jak ta będzie krążyć. Strażniczka zauważyła, że ma tylko 8 dron. Nieistotne - rozkaz w mocy. Dodatkowo Pięknotka dodała trigger - chce wiedzieć kiedy Sabina jeździ pociągiem. Jak się porusza.

Pięknotka spytała też Trzewnia - jakie są ruchy Sabiny. Jakieś anomalie w jej zachowaniu. Co i jak robi. (Tp: VVX). Trzewń powiedział coś ciekawego:

* Sabina już dwa razy była w Zaczęstwie. Wczoraj i dzisiaj. Jeździ tam praktycznie po pracy.
* Sabina nie sprawia żadnych kłopotów ani w Zaczęstwie ani w Pustogorze.
* Sabina się w Pustogorze upijała do nieprzytomności.
* PRZEDTEM: 5 dni: Sabina ma: praca - knajpa Olafa - pije - trup - praca...
* OD DWÓCH DNI: Sabina ma: praca - Zaczęstwo - kupuje na potem - pije w pokoju...
* Sabina nie miała żadnych komunikatów na hipernecie czy czymkolwiek co mogło spowodować zmianę jej zachowania.

Pięknotka jest zdziwiona. Idzie do Olafa. Ten ją ciepło przywitał. Zapytany przez Pięknotkę o Sabinę, powiedział, że nazywają ją tutaj "księżniczką lodu". Olaf spróbował się z Sabiną lekko zaprzyjaźnić, przełamać ją. Powiedziała, że jest okrutna i bezwzględna i przynosi kłopoty. On powiedział, że jest noktiańskim oficerem - a jednak tu siedzi. (Ex)

* X: Sabina jest nieprzystępna wobec kogokolwiek poza Olafem. Z nikim nie ma żadnych relacji.
* V: Sabina nie ufa Olafowi, ale parę rzeczy się dowiedział - ona nie dba co się z nią stanie. A ostatnio ma koszmarne sny; szuka źródła.
* X: Olaf poradził jej by skomunikowała się z terminusami (on dowodem że Pustogor nie jest głupi): Sabina się całkowicie zamknęła. She WONT trust them.
* XX: Olaf chciał dobrze, ale Sabina się przestraszyła że się do niego zbliża i uciekła; nigdy nie wróci do Górskiej Szalupy. Olaf bardzo żałuje. Żal mu tej małej.

Czyli Sabina szuka rozwiązania swoich koszmarów. Ale nie wiadomo o co chodzi i czemu Zaczęstwo. Pięknotka zauważyła w rozmowie z Olafem, że kilku magów z Miasteczka się zainteresowało Sabiną - jako malkontentką Pustogoru i potencjalnym źródłem środków. Ona tu nie wróci, ale oni mogą zacząć jej szukać i do niej wyjść. Olaf obiecał, że będzie monitorował sytuację - nie chce by tej małej stała się krzywda lub ktoś ją wykorzystał.

Pięknotka ściąga Gabriela Ursusa - swojego ucznia, terminusa-szperacza-katalistę-idealistę. Wyjaśniła że Sabina to arystokratka z Aurum na pracach społecznych (Gabriel JUŻ jest do niej negatywnie nastawiony). Pięknotka powiedziała mu że Sabina się stara ale ma koszmary z którymi ma opory by iść do kogokolwiek. Więc Gabriel ma iść z nią zrozumieć czego Sabina szuka.

Gabriel zrobił MINĘ DEZAPROBATY. Bardzo, bardzo nie lubi "opiekować się arystokratkami z Aurum". Sam jest arystokratą z Aurum... spytał, czy Pięknotka wie, że ona jest potworem której się jej własny brat wyrzekł? Pięknotka potwierdziła. Zdaniem Gabriela te koszmary nie mogły spotkać lepszej osoby. Zadanie - idą do Zaczęstwa śledzić Sabinę. A potem Gabriel ma ją oćpać.

Tu Gabriel się załamał.

Ale najpierw - muszą ją zobaczyć w akcji. Czas na podróż do Zaczęstwa.

Zaczęstwo. Gabriel i Pięknotka śledzą Sabinę Kazitan, mając wsparcie Strażniczki. Gabriel patrzy jak Sabina używa magii, próbuje zbadać typ jej mocy - i jego mina robi się coraz bardziej ponura. Sabina zdaniem Gabriela używa ciemniejszej magii, jakiejś formy mrocznej mocy (Tr: VVX).

* VV: Gabriel wyczuł naturę tego zaklęcia - to krwawa sympatia, ma lokalizować i przyciągać. Coś wzywa, coś przyciąga do siebie. Jakaś krew. Ważne - ten typ czaru jest GROŹNY.
* V: Z sygnatury wynika, że Sabina nie robi tego po raz pierwszy. Jest płynna w tej sztuce czarnej magii. A to podnosi skalę niebezpieczeństwa Sabiny.
* XX: Sabina wykryła Pięknotkę czarnym rytuałem.

Pięknotka WIE, że Sabina wykryła to wykrycie. Jej czarna magia ją wyczuliła. Zdaniem Gabriela ta moc może być korupcyjna. Pięknotka kazała się Gabrielowi schować; sama wyszła na widok. Sabina widząc Pięknotkę zamarła. Ma martwe oczy. Jest przerażona, ale trzyma fason. Natychmiast Sabina rozproszyła całą energię - NIE. VVVVVXXXXXXXXOOOOO. Próbuje wchłonąć energię w swoich komórkach, ukryć ją. Schować ślady.: VV. BARDZO ryzykowne, ale schowała ślad. Gabriel to widzi i ma szczękę na ziemi. Pięknotka też, ale dyskretnie. NIGDY nie widziała czegoś takiego. To pokazuje, że Sabina ma ogromne mistrzostwo w czarnej magii.

Sabina nadal jest przerażona, ale wie już że nie ma dowodów i ma nadzieję. Sabina wie też, że Pięknotce nie potrzeba dowodów...

Pięknotka naciska na Sabinę - niech ta jej powie. Sabina powiedziała że szuka medalionu, wypadł jej gdy uciekała z Trzęsawiska. Pięknotka powiedziała Sabinie, że tak dobrze jej szło - niech tego nie spieprzy kłamaniem. Powiedziała że w zależności od tego jak ostro pójdzie, tak będzie Sabinie. Nacisnęła - niech Sabina powie jej prawdę.

* V: Sabina powiedziała, że m.in. szuka Myrczka. Ale nie szuka go w Akademii Magii.
* V: Sabina powiedziała, że martwi się o Myrczka. Coś na niego poluje. Powie mu żeby nie wychodził z Akademii jeśli go zobaczy.
* V: Sabina jest mocno naciśnięta. Z oka popłynęła jej łza. Powiedziała, że szuka czegoś co jest oparte na niej, na jej wzorze i to poluje na Myrczka. Podejrzewa, że to stworzył ktoś z Aurum by zapolować na Myrczka by dobrać się do niej - bo na Trzęsawisku dała mu słowo że go osłoni. Powiedziała Pięknotce, że Myrczek jest SŁABY. Ale to nie powód. Plus, Sabina czuje, że musi tu być i to zrobić, że to ważne.

Pięknotka poprosiła Strażniczkę, by ta monitorowała Myrczka. Strażniczka odpowiedziała, że Myrczek jest z Napoleonem w Zaczęstwie; Myrczek czegoś szukała. Ale Napoleon nie zostawia Myrczka samego. Skąd TO się wzięło?

Pięknotka eskaluje wyżej. Powiedziała Sabinie, że wie dokładnie co ta potrafi - czarna magia, rytuały zakazane. Niech Sabina powie prawdę i całą prawdę.

* O: Sabina pękła. Przez moment całkowity, całkowity zwis. Tylko łzy w oczach. Opanowała się; głosem, który nie drży powiedziała wszystko: coś ją przyciąga. Ona też musi tu przyjść. To jest fragment Sabiny. Sabina podejrzewa magów Aurum bo mają jej krew. Jak powiedziała wcześniej - "albo jesteś silna i decydujesz o losie innych albo słaba i inni decydują o Twoim". Sabina chce "to" znaleźć zanim "to" znajdzie Myrczka.

Sabina nie prosi o nic Pięknotkę. Nie protestuje. Nie udaje, że to nieprawda. Po prostu czeka, bo nic nie może zrobić. Pięknotka zażądała informacji czego Sabina się boi - ta odpowiedziała. Boi się najbardziej, że Pięknotka poda tą informację do rekordów pustogorskich. W ten sposób Aurum się dowie.

O! Czyli Aurum nie wie co umie Sabina? Sabina odpowiedziała, że nie. Jakby wiedzieli... Sabina wolałaby nie żyć. Podjęła próbę prośby - niech Pięknotka nie podaje tego do rekordów. Czy chce, by umiejętności Sabiny trafiły do Aurum? Do tych wszystkich magów? A musi tam wrócić... bo ma osoby na których jej zależy. Mają jej krew.

Sabina bardzo żałuje że powiedziała Myrczkowi że go ochroni. Jeden ludzki odruch - i ryzyko dla kogoś innego.

Pięknotka spytała Napoleona o co chodzi z Ignacym Myrczkiem - Myrczek szuka jakiejś kobiety bo grozi jej straszne niebezpieczeństwo. Myrczek szuka, ale nie wie kogo i co. Pięknotka powiedziała Napoleonowi, że niebezpieczeństwo jest prawdziwe - niech Myrczka pilnuje. Na pierwszy rzut oka SOS, na drugi ewakuacja.

Czyli ta siła ma krew Myrczka. Czyli to nie Aurum, mimo, że w to wierzy Sabina. Pięknotka pomyślała o czymś przerażającym - Ignacy Myrczek był lekko ranny na Trzęsawisku a Sabina Kazitan - ciężko ranna. To znaczy, że najpewniej to nie Aurum - to Trzęsawisko. Pięknotka wyjaśniła Sabinie, że Trzęsawisko jest półświadomym bytem i mogło być tym, co teraz próbuje wciągnąć tam osoby które je skrzywdziły.

Do Sabiny dotarło, że wpakowała siebie i Myrczka na gniew Trzęsawiska. To, że wpakowała Franciszka nic a nic jej nie martwi, ale Myrczek - tak. Sabina szybko powiedziała Pięknotce, że Trzęsawisko nie wzywa Sabiny "do siebie" a do Zaczęstwa. To nie ma sensu. Pięknotka powiedziała, że nie ma "kreski" - tu jest Trzęsawisko a tu nie.

Pięknotka zażądała od Napoleona, by przyprowadził do niej Myrczka. Weźmie jego i Sabinę do szpitala terminuskiego. Tam ich naprawią; rozłączą z Trzęsawiskiem. Myrczek gdy zobaczył Sabinę to się przestraszył. Sabina rzuciła mu spojrzenie pobłażliwej pogardy. Napoleonowi się już Sabina nie spodobała.

I faktycznie, w Szpitalu pomogą i Sabinie i Myrczkowi. Acz na Sabinę poproszę o test (Tr):

* V: Sabina jest rozpięta z Trzęsawiskiem; nie ma "głodu" by tam iść.
* V: Trzęsawisko nie "widzi" gdzie Sabina się znajduje. Jest jakby tam nie poszła.
* XX: Trzęsawisko jest kompatybilne z Sabiną. It adapted her blood. Sabina NIE MOŻE NIGDY tam wrócić lub zostanie zasymilowana.

Pięknotka powiedziała Sabinie co ją czeka jeśli pójdzie na Trzęsawisko - stanie się jego częścią a Trzęsawisko doda jej strain do swoich bioform. Lekko koszmarna wizja.

Pięknotce została jeszcze jedna rzecz do zrobienia - zapewnienie sobie milczenia Gabriela. Powiedziała mu, że widzi, że nie lubi Sabiny. Gabriel odpowiedział, że Sabina skrzywdziła jego siostrę i ma krew na rękach. Gabriel cieszy się, że to wiedzą - wreszcie mogą ją przyskrzynić, niektóre wydarzenia w Aurum stały się oczywiste. Pięknotka zauważyła, że jeśli Gabriel nie zabije Sabiny tu i teraz, to Aurum ją wykorzysta.

Gabriel odpowiedział, że to znaczy, że te śmierci magów o których on myśli to JEJ WŁASNA DECYZJA. Pięknotka odpowiedziała pytaniem - jak zdesperowany Gabriel musiałby być by się tego nauczyć? Gabriel dał parę przykładów co ona MOGŁA zrobić. Pięknotka powiedziała, że Gabriel daje Aurum do ręki broń jeśli powie Pustogorowi. I tacy jak Lemurczak mają z Sabiny narzędzie które wykorzystują jak chcą. Więc nie może to trafić do publicznego raportu.

Pięknotka proponuje raport tylko dla Karli. Gabriel jest BARDZO sfrustrowany. Uważa, że Sabina Kazitan jest potworem bez cienia ludzkiej cechy. Jest prawą ręką Lemurczaka - jest dokładnie taka sama jak on. Uważa, że Pięknotka robi błąd. I osobiście jej nienawidzi.

Pięknotka pulled a rank on Gabriel. Jest wyższa stopniem. Sekrety Sabiny Kazitan mają zostać dyskretne.

* V: Gabriel się zgadza na plan Pięknotki.
* V: Gabriel nie powie nikomu, nawet Siostrze.
* V: Relacja Gabriel - Pięknotka jest zachowana, nie jest uszkodzona.
* X: Gabriel nienawidzi Sabiny Kazitan. Będzie ją obserwował.

Pięknotka dała Sabinie rozkaz - co najmniej 3 razy w tygodniu ma iść do Olafa; smutno mu samemu. Sabina wykona polecenie.

## Streszczenie

Trzęsawisko Zjawosztup zostało Skrzywdzone przez ostatnie wydarzenia. Zaczęło wabić Sabinę i Ignacego by ich pożreć. Szczęśliwie, Pięknotka zdążyła dotrzeć do Sabiny (która wabiona szukała Ignacego) i uratowała ich przed Trzęsawiskiem. Dodatkowo Pięknotka poznała sekret Sabiny i zdobyła nad nią absolutną kontrolę.

## Progresja

* Sabina Kazitan: kiedyś skrzywdziła Lilię Ursus, siostrę Gabriela. Ma nienawiść Gabriela i Lilii do siebie.
* Sabina Kazitan: płynnie kontroluje czarną magię i magię krwi. Potrafi nawet ukryć ich emanacje, wchłaniając to w siebie.
* Sabina Kazitan: jej sekret (czarna magia) został wykryty przez Gabriela Ursusa i Pięknotkę Diakon.
* Sabina Kazitan: jest kompatybilna z Trzęsawiskiem Zjawosztup; Trzęsawisko pragnie jej obecności i ją przyzywa.
* Pięknotka Diakon: ma pełną kontrolę nad losem Sabiny Kazitan; Sabina jej NIE nienawidzi, ale się jej boi.

### Frakcji

* .

## Zasługi

* Pięknotka Diakon: śledząc dziwne zachowanie Sabiny odkryła jej sekret i ją złamała. Ale potem spróbowała jej pomóc i odpięła ją od Trzęsawiska używając Szpitala Terminuskiego. Przekonała Gabriela by zachował sekret Sabiny dla siebie.
* Strażniczka Alair: Pięknotka jest przekonana, że jest najlepszą rzeczą jaka zdarzyła się Zaczęstwu. Ostrzegła Pięknotkę że Sabina Kazitan czegoś szuka. Lepsza niż aktualny terminus.
* Sabina Kazitan: ma koszmary przez Trzęsawisko, które ją nawołują. Próbuje sama rozwiązać problem do momentu zdradzenia jej sekretu przed Pięknotką. Oddała Pięknotce nad sobą kontrolę.
* Olaf Zuchwały: znalazł nić porozumienia z Sabiną Kazitan, mówiąc jej, że nawet on - noktianin - został zaakceptowany. Sabina uciekła od niego, bo bała się zaprzyjaźnić.
* Gabriel Ursus: nienawidzi Sabiny Kazitan; wykrył dla Pięknotki jej sekret - płynnie kontroluje czarną magię. Przekonany, by ukryć ten sekret przed wszystkimi poza Karlą.
* Napoleon Bankierz: z rozkazu Teresy Mieralit pilnował Myrczka by nic mu się nie stało gdy ten szukał kobiety której coś grozi; bezpiecznie doprowadził go do Pięknotki.
* Ignacy Myrczek: szukał kobiety, której grozi coś złego (zew Trzęsawiska). Skończył w Szpitalu Terminuskim w Pustogorze, na sali z Sabiną Kazitan. Przerażony.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Zaczęstwo
                                1. Nieużytki Staszka: miejsce przeszukiwań Sabiny Kazitan by znaleźć Myrczka i anomalię z Trzęsawiska; tam ją złapała Pięknotka.
                            1. Pustogor
                                1. Rdzeń
                                    1. Szpital Terminuski: Sabina Kazitan i Ignacy Myrczek, na jednej sali, odpięci z Trzęsawiska Zjawosztup.
                                1. Eksterior
                                    1. Miasteczko
                                        1. Knajpa Górska Szalupa: miejsce picia Sabiny Kazitan; chodziła tam aż pojawiło się ryzyko że się zaprzyjaźni - wtedy przestała.
                                    1. Zamek Weteranów: Sabina Kazitan tam pomaga z uwagi na prace społeczne. Pomaga jak jest w stanie.

## Czas

* Opóźnienie: 7
* Dni: 2

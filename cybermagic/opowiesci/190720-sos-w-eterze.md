---
layout: cybermagic-konspekt
title: "SOS w eterze"
threads: nemesis-pieknotki
gm: kić
players: r1,r2,r3
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [190720 - SOS w eterze](190720-sos-w-eterze)

### Chronologiczna

* [190720 - SOS w eterze](190720-sos-w-eterze)

## Budowa sesji

### Stan aktualny

* .

### Pytania



### Wizja

* .

### Tory

* .

Sceny:

* .

## Punkt zerowy

"Oko prawdy" to luksusowy jacht rekreacyjny, przerobiony na statek badawczy.
Na jego pokładzie, oprócz pięcioosobowej załogi, znajduje się trójka młodych arystokratów.
Odbiorniki Pustogoru odebrały sygnał SOS "Oka prawdy".
Rodziny zaginionych silnie naciskają, aby wysłać ratunek - w końcu to ich ukochane dzieci... A mają zasoby i wpływy.
Na szczęście na pokładzie "Oka" znajduje się silny nadajnik pozycyjny.
Statek ratunkowy, "Pasja", wyrusza w Eter wiedząc tylko, że jeden z generatorów "Oka" jest uszkodzony...

### Postacie

* Billy - koci włamywacz
* Greg Światełko - puryfikator
* John Wytwórca - scrapper
* Stanisław Makut - kapitan "Oka prawdy", pragnie chronić swoich
* Karol Rozenek - marynarz wachtowy, punkt światła tej załogi
* Anna Rogoz - konkretna i fachowa kobieta, która chce być mistrzem w swoim fachu. Nie może się pogodzić ze śmiercią Juliana Storka
* Julian Stork - oficer wachtowy "Oka prawdy". Zginął w wyniku awarii generatora i stał się krakenem przywiązanym do Anny. Chce się uwolnić.
* Konrad Kobol - kuk "Oka prawdy".  Zginął w wyniku awarii generatora i stał się duchem nawiedzającym statek.

### Opis sytuacji

.

## Misja właściwa

Dzięki doskonałemu nadajnikowi lokalizacyjnemu "Pasji" stosunkowo szybko udało się odnaleźć dryfujące w Eterze "Oko prawdy".
W chwili, gdy wchodzili na pokład, zauważyli, jak na jednego z członków załogi spada element statku.
Billy błyskawicznie zbił z nóg marynarza, usuwając go z drogi.
Karol zaprowadził ratowników na mostek, gdzie kapitan przedstawił im sytuację. Uszkodzony generator, nie mogą się doliczyć dwóch członków załogi, boją się ruszyć z miejsca.
Trójka ratowników szybko oceniła sytuację i uznała, że należy przenieść wszystkich z "Oka" na "Pasję" - dla bezpieczeństwa. Kapitan zgodził się, choć niechętnie - on nie chce opuszczać statku, ale dla bezpieczeństwa załogi dużo jest w stanie poświęcić.
W trakcie rozmowy na mostek wpadł Ludomir, jeden z młodych arystokratów - badaczy, krzycząc, że próbki ożywają i trzeba natychmiast coś zrobić. Greg i John poszli z Ludomirem.
Greg zabrał się za stabilizację próbek, John skupił się na generatorze. 
O dziwo, udało im się w obu wypadkach, choć bez zasobów "Pasji" - części do generatora i narzędzi puryfikujących, byłoby to niemożliwe.
Mając w miarę opanowaną sytuację, wrócili na mostek. 
Gdy tam wchodzili, rozbrzmiały alarmy zbliżeniowe. Zgodnie ze wskazaniami sonaru coś ogromnego zbliżało się do "Oka".
John kątem oka zauważył postać z tasakiem wymachującą rękami, która, gdy zwrócił na nią uwagę, pokazała "ćśśśśśśś" i zniknęła. 
John nie do końca uwierzył swoim oczom i nic nie powiedział innym. Wielka plama zbliżała się coraz bardziej. Ratownicy uznali, że coś takiej wielkości może być na przykład młodym lewiatanem, którego ściągnęły emisje z uszkodzonego generatora. Ale skoro generator jest naprawiony, to może - o ile zachowają ciszę - istota ich ominie...
Tak też się stało, choć trwało to kilka pełnych napięcia chwil.

John i Billy zeszli ponownie do generatora, upewnić się, że wszystko jest w porządku. Wtedy Billy zauważył ubraną na biało postać, ewidentnie chcącą, aby poszedł za nią... Upewnił się, że John też to widzi - to był ten sam duch, co wcześniej.

Billy postanowił pójść za duchem, który zaprowadził go do zęzy i zniknął w jednym z kątów. Tam Billy znalazł Annę, marynarza wachtowego, która próbowała uniknąć przenosin. Przyciśnięta, przyznała, że nie chce opuszczać statku, bo przecież Julian na pewno gdzieś tu jest... Ona nie wierzy, że on zginął. On nie mógł tak po prostu odejść. 
Cóż... Upewnili się, że Anna ponownie się nie ukryje i przenieśli wszystkich na "Pasję". 
Sami zostali na "Oku". Uznali, że uszkodzony okręt jest wystarczająco sprawny, aby spróbować wrócić do portu.
Połączyli statki liną holowniczą - mimo wszystko nie do końca ufali silnikowi czy generatorom "Oka".
Kiedy "Pasja" próbowała ruszyć, kapitan macierzystego statku poinformował ich, że pomimo "całej naprzód" nie ruszają się z miejsca - coś ich trzyma. Kiedy wyszli na pokład "Oka", ujrzeli wielką mackę wspinającą się po burcie "Pasji".
W trynie natychmiastowym nakazali przeniesienie wszystkich - łacznie z załogą "Pasji" na "Oko".
Ku ich zdziwieniu, macka zaczęła się wycofywać. 
Kiedy Anna znajdowała się na trapie, postać w białym fartuchu ukazała się Gregowi i wskazała na nią palcem, po czym zniknęła.

Wspólnie, ratownicy doszli do wniosku, że ten duch to kuk, drugi z marynarzy zaginionych po awarii generatora.
Jednak, co próbował im przekazać?

Greg postanowił przebadać Annę, na co ona się zgodziła.
Greg odkrył, że są dwie "masy energetyczne". Mniejsza, połączona z samym statkiem i większa, powiązana z Anną. W dodatku połączenie z Anną jest jednostronne, energia jest transferowana od niej do tego bytu.

Ze skupienia wyrwał Grega krzyk z mostka. Okazało się, że kapitan został wchłonięty przez ścianę (utrata kontroli magicznej przy poprzednim zaklęciu, a kapitan nie chciał opuszczać statku ponownie)
Razem z Johnem wyciągnęli kapitana bezpiecznie ze ściany.

Po wszystkim, w krótkiej dyskusji doszli do wniosku, że ta wielka masa to coś na kształt krakena i że najpewniej to był Julian, którego eter przekształcił właśnie tak. 
W trakcie rozmowy, Billy zauważył, jak Anna wychyla się za burtę i nacina dłoń, upuszczając sobie krwi prosto w Eter. 
Gdy tylko krew spadła, z wody wyłoniła się macka. Ratownicy rzucili się do Anny, która wyglądała na zagubioną, ale odpowiadała w miarę składnie. Niespodziewanie, Anna odepchnęła się od ściany i rzuciła za burtę. Billy próbował ją złapać, ale nie zdążył i wpadła w objęcia macki. 
Billy szybko obwiązał się liną i skoczył za nią, ale udało mu się ją uratować tylko dlatego, że kapitan zaatakował i zranił mackę zanim ta zanurzyła się z Anną (porażka). Billy zdążył złapać i wyciągnąć kobietę, ale kapitan zniknął w Eterze...

Znalazłszy się na pokładnie, próbowali zaprowadzić Annę do kajut, zanim jednak zdążyli to zrobić, macka ponownie wynurzyła się z Eteru i zaatakowała, celując prosto w kobietę.

Łącząc siły, Billy, Greg oraz John zabili krakena, jednak nie udało im się bezpiecznie odłączyć od niego Anny. Śmierć bestii była dla niej szokiem (heroiczny, skonfliktowany).
Postanowili popłynąć pozbawionym kapitana "Okiem" do Pustogoru. "Pasja" płynęła obok, wspomagając ich swoimi generatorami ochronnymi. Na hol wzięli martwego krakena.

Kiedy powrócili do portu, musieli się mocno tłumaczyć z energii, którą za sobą przywlekli, ale dzięki znajomościom w kapitanacie udało im się uniknąć rozstrzelania działami portowymi.
Musieli jednak oddać truchło krakena Senetis.


**Sprawdzenie Torów** ():



**Epilog**:

* .

## Streszczenie

.

## Progresja

* Billy Czuwak: ciężko przeżył śmierć kapitana "Oka"
* Greg Światełko: Amulet z zęba krakena
* Billy Czuwak: Pryzowe za przyprowadzenie "Oka" oraz rekompensata od Senetis za krakena.
* Greg Światełko: Pryzowe za przyprowadzenie "Oka" oraz rekompensata od Senetis za krakena.
* John Wytwórca: Pryzowe za przyprowadzenie "Oka" oraz rekompensata od Senetis za krakena.
* Anna Rogoz: pustka po połączeniu z krakenem

### Frakcji

* Senetis: dostęp do ciała maga przekształconego eterem w krakena
* Senetis: dostęp do "Oka prawdy"

## Zasługi

* Billy Czuwak: uratował Karola przed zgnieceniem, odnalazł ukrytą Annę. Zabił krakena razem z Gregiem i Johnem.
* Greg Światełko: oczyszczał i stabilizował Eter umożliwiając naprawę generatora. Zabił krakena razem z Billym i Johnem.
* John Wytwórca: naprawił generator umożliwiając "Oku" powrót do portu. Zabił krakena razem z Billym i Gregiem.
* Okręt Oko prawdy: nawiedzony przez ducha kuka

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Pustogor
                                1. Port Eteryczny
    1. Eter Nieskończony

## Czas

* Chronologia: Aktualna chronologia
* Opóźnienie: 465
* Dni: 2

---
layout: cybermagic-konspekt
title: "Po drugiej stronie Bramy"
threads: legenda-arianny
gm: kić
players: żółw, fox, kapsel
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [200610 - Ixiacka wersja Malictrix](200610-ixiacka-wersja-malictrix)

### Chronologiczna

* [200610 - Ixiacka wersja Malictrix](200610-ixiacka-wersja-malictrix)

### Plan sesji
#### Co się wydarzyło

?

#### Sukces graczy

* ?

### Sesja właściwa
#### Scena Zero - impl

.

#### Scena Właściwa - impl

Infernia jest sprawna. Admirał Kramer wezwał Ariannę do siebie. Infernia będzie kurierem. Kuriera bierze na pokład naukowca. Zadaniem Inferni jest dostarczyć naukowca na konferencję. Klaudia trochę pogrzebała i wyszukała, że gościu jest protegowanym Sowińskich. Oni go sponsorują.

A czemu Infernia? Ma mega sensory a konieczne jest przejście przez Bramę. Ów mag jest specjalistą od Eteru i bardzo chciałby skorzystać z możliwości zobaczenia jak to wygląda na sensorach gdy się wchodzi w Bramę. Naukowiec nazywa się Janus Krzak. 

Infernia wylatuje w trasę. Ale zanim wylecimy - Eustachy poprosił o research, czego ten typ szuka albo czemu Infernia? Kramer ma nadzieję, że troszkę sytuację załagodzicie. Zdaniem Klaudii - sensory Inferni będą na granicy, będą wymagać naprawy. Eustachy z Klaudią składają obwody do zabezpieczenia sensorów. Niech Inferni sensory wytrzymają patrzenie lot przez Bramę (konflikt odroczony).

Infernia odbija od Orbitera, namiar na Bramę. W Bramie wszystko wyglądało dobrze, ale nagle Klaudia "Arianno, coś jest nie tak". I Infernią zaczyna potężnie rzucać. Nawet kompensatory nie dały rady tego skompensować. Ryk alarmów, ostrzeżenia sensorów i robi się chaos na mostku.

Klaudia "mam to!". Zbiera ogromną ilość mocy i próbuje to stabilizować. I nagle Arianna słyszy/czuje dziwne uczucie i na Inferni rozlega się alarm bojowy, totalne zagrożenie, poziom pierwszy.

Na Infernię działa gigantyczne ciśnienie. Jak czegoś nie zrobi, to Infernia będzie zgnieciona.

Arianna patrzy na odczyty z sensorów i nie do końca ma sens to co widzi. Element ze szkolenia na kapitana. Jesteśmy pod wodą?

Krzyczy do Arianny - "wyciągnij nas stąd".

Eustachy spina systemy do wzmacniania sensorów z tarczami. Eustachy spina to z reaktorem Inferni. I wszyscy zakładają buty magnetyczne - trzeba wyłączyć sztuczną grawitację i określić górę/dół itp. I statek ma się wynurzać - by Infernia była przeciążona coraz słabiej. Gdy wyłączymy grawitację i punkt odniesienia, kalibracja żyroskopów. Plus użycie magii by jak najszybciej zadziałało. ExMZ+3 (za przepięcie nadwyżki mocy).

* V: ustabilizowane tarcze Inferni.
* V: Eustachy jest dobry. Udało się mu uratować czujniki, nawet, jeśli są lekko uszkodzone.
* Vz: Infernia po naprawie dała radę. Silniki działają. Infernia jest mobilna. 

Ryk alarmów. Infernia się ustabilizowała. Wszystkich na bok. Czyli Infernia jest bokiem. Obracamy statek. Idzie powoli, Infernia pod wodą nie jest najszybsza. Przełączamy czujniki na tryb podwodny i badamy teren dookoła. Ale Klaudii nie ma przy konsolecie.

Martyn szuka Klaudii na pokładzie Inferni - wpierw przez komputery, potem przez tkankę. Nie ma Klaudii na pokładzie. Przekazuje Ariannie tkankę Klaudii - trzeba znaleźć magicznie Klaudię po sympatii.

Janus wspomagany przez informacje Zespołu próbuje dojść do tego co się mogło spotkać z Klaudią. Martyn robi przemowę by uspokoić Janusa - jest akredytowanym lekarzem floty, takie rzeczy się zdarzają na Inferni i potrzebna jest sprawa związana ze znalezieniem Klaudii. A oni mają procedury i on ma środki by uspokoić Janusa. (Tr+3)

* XV: Martyn uspokoił Janusa. Trudność Janusa: Ex -> Tr.

Razem przejrzeli nagrania na Inferni w okolicach tego co się stało na Inferni i na podstawie tego Janus próbuje określić co tu się stało. 

* V: Z nagrań wynika, że: Infernia wchodzi w Bramę, Klaudia w którymś momencie się koncentruje i "pomiędzy klatkami była - nie ma". Co ciekawe, blinknięcie nastąpiło pod koniec przejścia albo tuż po przejściu.

Dodajemy synchronizację z czujnikami - i więcej informacji dla Janusa.

* V: Zniknięcie Klaudii nastąpiło w momencie przejścia przez Bramę. Klaudia jest po TEJ stronie Bramy.

Poza Infernią nie ma hipernetu. Klaudia jest poza hipernetem.

* XX: (plus tamten zużyty).

Była robiona analiza czujników itp. Janus potrzebuje więcej informacji. Uruchomił sensory aktywne. Było super, aż usłyszeliście bardzo niskie, jękliwe tony i cała Infernia drży. WYŁACZAMY WSZYSTKIE CZUJNIKI! Coś się o nią ociera. Widzimy ogromne cielsko, zbliżone w wielkości do Inferni. Chyba przyjaźnie ociera się o tarcze Inferni.

Eustachy szuka "randki dla Lewiatana". Szuka informacji o matchach pomiędzy dźwiękami czujników Inferni i dźwięków Lewiatana. Kilka częstotliwości, które reagują na Lewiatana. Więc - pinasa, która ma dźwięki. Dźwięki do puszczenia przez pintki - dźwięk naszych sensorów, jego i jakie wydawałyby silniki. ExZ+3+3O. 

* X: pintka już nie wróci. Spełni swoją rolę.
* V: odciągnęła Lewiatana. Infernia w tryb ciszy, Lewiatan jej śpiewa, coraz bardziej natarczywie a jak pintka się odezwała to w śpiewie Lewiatana "ktoś do mnie gada". Mamy spokój i możemy spokojnie działać.

Wracamy do konfliktu szukającego Klaudii. Arianna używa swojej magii i wspomaga naukowca - ma tkankę Klaudii i używa magii do znalezienia Klaudii.

* Vm: Arianna łączy się z naukowcem. Janus zafascynowany jej mocą. Arianna ma _namiar_. Nie jest punktowy, jest tam coś dziwnego. Janus: "intrygujące. Wygląda jakby nie była w jednym miejscu?"

Dystans do Klaudii około pół kilometra. Nasze czujniki pokazują pod wodą Bramę. Jesteśmy jakieś 50m ponad nią. Zaklęcie pokazuje dystans około 300m w poziomie do terenu gdzie jest Klaudia. Na oko - ktoś zbudował Bramę pod wodą. Ta Brama nie wygląda jak żadna Brama jaką Janus kiedykolwiek widział, ale architektonicznie jest _inna_.

Eustachy włączył sensory, szukając innych radiosygnałów czy rzeczy, echa transmisji radiowych, elektromagnetyzm. A tu nic - cisza. Nie ma hipernetu, nie ma transmisji. Lecimy w stronę czegoś "nieaktywnego, w stanie spoczynku".

Infernia zbliża się do "bazy kosmicznej ale pod wodą". Nie ma kopuły, jest metalowa (opiera się ciśnieniu). Ma śluzy. I widać też hangar. Baza jest opuszczona. Wszystko nieaktywne. Klaudia - jej sygnatura jest na obrzeżach bazy.

Mamy advancera. Trzeba wysłać Elenę! Elena dociera do hangaru bez większych przygód i znajduje panel, po czym "Eustachy, popatrz na to". Rzuca na ekran całkowicie obcy sobie panel. Zwykle panele, nawet awaryjne powinny być podobne. Ten ma pewne podobieństwa ale wygląda zupełnie inaczej niż to co Eustachy zna a w świetle Lancera ma przyciski opisane ale pismo jest nieznane.

Eustachy chce zobaczyć symbole i śluzy itp.

Jeden z członków załogi mostka mówi "tamten przycisk". To noktianin z Trzeciego Raju. Tal Marczak. Jest to stara baza noktiańska. Rozumie niektóre rzeczy, ale niektóre sigile są dla niego nieznane. Składnia jest dziwna. Czyli - mamy bazę i załogę która może pomóc to zinterpretować.

NOKTIANIE GRUPĄ ROBOCZĄ DO JĘZYKÓW. Kucharz, koleś od kibla, technicy... oni wszyscy coś wiedzą. Więc rozpykamy panel bez problemu i możemy wejść do bazy.

Elena pełni rolę advancera. Korzysta z jednej ze śluz zanim się zaleje.

Elena + noktiański advancer Raoul robią płytki zwiad bazy. TrZ+3. 

* V: Baza w trybie "standby". Powietrze jest, da się oddychać. Nie ma śladów promieniowania, systemy nieaktywne. Energia awaryjna. Wszystko jest trochę jakby "mothballed".
* Vz: dzięki noktianinowi Elena dała radę zdobyć _lay of the land_. To nie jest standardowy, znany układ bazy. Wygląda na naprawdę starą bazę. Baza jest w okolicy 100-200 lat temu.

Może w bazie być półaktywna BIA... ale można cicho i dyskretnie, nie budząc BII wlecieć do bazy. Infernia wleciała; Martyn każdego co próbuje wyjść opieprza. Sprawdził dokładnie wszystkie systemy biologiczne - nic nie znalazł. Nie powinno być niczego dziwnego. 

Mamy bazę kołową. W każdym hangarze umieszczamy wzmacniacz hipernetu. I jeden na środku. Następnie Arianna robi 'magiczny pulse' by zastartować i wzmocnić Klaudii sygnał by ją "na siłę" ściągnąć do hipernetu. TrMZ+2

* X: Ekspert od Eteru - w jednej z kajut coś dziwnego się stało. Kajuta Klaudii. Coś Dziwnego. Arianna przekazała --> Eustachy i Leona.
* X: ZAWIESZONY.
* Vm: Mamy kontakt z Klaudią. Pojawiła się w hipernecie, ale jej sygnał jest słaby. Klaudia odpowiada, ale nie potrafi powiedzieć do końca gdzie jest i co się z nią dzieje.

Martyn poprosił Klaudię by ta zrobiła ruch kończyną. A sam z Persefoną obserwuje bazę - hipoteza: Klaudia jest zintegrowana z bazą. ExZ+3.

* V: Przed Eleną pojawia się dziewczynka. Wyższe -naście lat. Twarz podobna do Klaudii. Nosi sukienkę jak Klaudia mi ukradła z kajuty. Budzi się life support. Dziewczynka podnosi oczy na Elenę "co ty tu robisz". Elena pyta "chyba nie jesteś Klaudią, prawda?". "Klaudia" smutna, uciekła i przebiegła przez zamknięte drzwi. A life support się uruchomił. Czyli... coś się stało.

Eustachy i Leona zbliżają się do kajuty Klaudii. Z monitora wyskakuje z odpowiednimi dźwiękami "pachołek do bicia". Jak z tej zabawki z Duplo. Leona to atakuje. Rozpada się w feerii małych sześcianików - pojawiają się dwa. Eustachy korzysta z tego, że w pokoju Klaudii są Rzeczy To Przenoszenia Anomalii. 

Eustachy szybko korzysta z Janusa - co z tego co ma Klaudia się może przydać do złapania / neutralizacji tego cholerstwa? TrZ+1

* XV: Anomalia jest odpalona, zamknięta w pudełku. Musimy się tego pozbyć... to jest niestabilny containment.

I z tym stanem Zespół oczekuje co się będzie działo dalej. Trzeba uratować Klaudię i jakoś wrócić do domu.

## Streszczenie

Infernia próbując przetransportować eksperta Eteru na konferencję przez Bramę uległa katastrofie - przeniosła się GDZIEŚ INDZIEJ, pod wodę i zniknęła Klaudia. Nie uległa katastrofalnym uszkodzeniom, ale po zachęceniu pintką Lewiatana dotarła do archaicznej podwodnej noktiańskiej bazy. Tam znajduje się Klaudia, ale coś jest z nią nie tak. Sprzęgnięta z bazą? Tak czy inaczej, infiltracja bazy dopiero się zacznie. Trzeba znaleźć Klaudię i wrócić do domu.

## Progresja

* OO Infernia: pozbyła się pierwszej pinasy by zrobić z niego "randkę dla Lewiatana".
* OO Infernia: dzięki szybkim działaniom Eustachego, tylko trochę uszkodzona. Ma słabsze sensory. Trzyma się tylko dzięki polu.

### Frakcji

* .

## Zasługi

* Arianna Verlen: wpierw magią szukała i odnalazła echo Klaudii, a potem magią ściągnęła Klaudię do hipernetu w archaicznej bazie noktiańskiej.
* Eustachy Korkoran: Absolutny MVP; utrzymał Infernię przed zmiażdżeniem pod wodą i przekształcił pintkę w "randkę" by Lewiatan się nie zaprzyjaźnił z Infernią za bardzo.
* Martyn Hiwasser: gdy Klaudia zniknęła, skupił się by ją znaleźć a potem uspokoił Janusa, by ten pomógł Ariannie. Potem próbował nawiązać kontakt z "anomaliczną Klaudią".
* Klaudia Stryk: doszło do katastrofy przy przejściu przez Bramę Eteryczną. Coś zrobiła i zniknęła. Jej stan nieznany, ale coś z anomaliami i dziwną bazą noktiańską.
* Janus Krzak: ekspert od Eteru sponsorowany przez Sowińskich, chciał lecieć na konferencję (poza Sektorem Astoriańskim). Pomógł Zespołowi rozpracować co się mogło stać z Klaudią.
* Elena Verlen: advancerka; zinfiltrowała z Raoulem opuszczoną archaiczną bazę noktiańską. Zrobiła lay of the land i napotkała Klaudiokształtną anomalię..?
* Tal Marczak: ludzki noktiański mechanik Inferni; rozpoznał starą bazę na wzorach noktiańskich. Z innymi noktianami w grupie roboczej tłumaczącej sigile tej dziwnej archaicznej bazy.
* Raoul Lavanis: ludzki noktiański advancer Inferni; wraz z Eleną rozpracował starą bazę noktiańską. W grupie roboczej tłumaczącej sigile tej dziwnej archaicznej bazy.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Brama Kariańska: z jakiegoś powodu nie zadziałała właściwie i Infernia przeszła przez nią... dzięki Klaudii wylądowała _gdzieś_ (Crepuscula).
        1. Zagubieni w Kosmosie
            1. Crepuscula
                1. Pasmo Zmroku
                    1. Baza Noktiańska Zona Tres: co najmniej 100 lat, podwodna baza odizolowana od "naszej części kultury Noctis". Obce glify. Infernia ją infiltruje szukając Klaudii. Aktywna (działa lifesupport), ale "uśpiona".
                    1. Podwodna Brama Eteryczna: wygląda na archaiczną technologię noktiańską, niedaleko niej krąży Lewiatan. Infernia przypadkowo przez nią przeszła.


## Czas

* Opóźnienie: 6
* Dni: 2

## Konflikty

* 1 - Eustachy spina systemy do wzmacniania sensorów z tarczami. Eustachy spina to z reaktorem Inferni. Ustabilizować Infernię pod wodą by nie padła.
    * ExMZ+3
    * VVV: Udało się - Infernia jest mobilna, lekkie uszkodzenia sensorów i nadmierne wymogi na reaktorze, ale DZIAŁA.
* 2 - Martyn robi przemowę by uspokoić Janusa - jest akredytowanym lekarzem floty, takie rzeczy się zdarzają.
    * Tr+3
    * XV: Janus uspokojony (Ex->Tr), ale patrzy na Martyna dziwnie.
* 3 - Janus próbuje na podstawie danych z sensorów dojść do tego co się stało Klaudii i jak ją znaleźć
    * Tr+2
    * VVXX: mamy info o Klaudii (zniknięcie w momencie przejścia), ale wpadliśmy na miłosnego Lewiatana
    * Vm: (po Lewiatanie) Arianna łączy się z naukowcem. Janus zafascynowany jej mocą. Arianna ma _namiar_ na Klaudię.
* 4 - Eustachy szuka "randki dla Lewiatana". Pinasa, która ma dźwięki - dźwięk naszych sensorów, jego i jakie wydawałyby silniki. 
    * ExZ+3+3O. 
    * XV: Pintka nie wróci, ale odciągnęła Lewiatana.
* 5 - Elena + noktiański advancer Raoul robią płytki zwiad bazy.
    * TrZ+3
    * VVz: Baza w trybie standby, lay of the land. Serio stara baza. 100-200 lat temu?
* 6 - Arianna robi 'magiczny pulse' by zastartować i wzmocnić Klaudii sygnał by ją "na siłę" ściągnąć do hipernetu.
    * TrMZ+2
    * XXVm: Anomalia Klaudii na Inferni... ale mamy kontakt z Klaudią, jest w hipernecie, ale jest z nią mentalnie coś nie tak
* 7 - Martyn poprosił Klaudię by ta zrobiła ruch kończyną. A sam z Persefoną obserwuje bazę - hipoteza: Klaudia jest zintegrowana z bazą.
    * ExZ+3
    * V: Doszło do reakcji w bazie - anomaliczna Klaudiodziewczynka?
* 8 - Eustachy szybko korzysta z Janusa - co z tego co ma Klaudia się może przydać do złapania / neutralizacji tego cholerstwa (anomalii Klaudii)?
    * TrZ+1
    * XV: Anomalia jest odpalona, zamknięta w pudełku. Musimy się tego pozbyć... to jest niestabilny containment

## Inne

.

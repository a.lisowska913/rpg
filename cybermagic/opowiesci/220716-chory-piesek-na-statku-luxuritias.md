---
layout: cybermagic-konspekt
title: "Chory piesek na statku Luxuritias"
threads: plagi-neikatis, historia-klaudii, historia-martyna, grupa-wydzielona-serbinius
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [191025 - Nocna Krypta i bohaterka](191025-nocna-krypta-i-bohaterka)
* [220119 - Sekret Samanty Arienik](220119-sekret-samanty-arienik)
* [211016 - Złamane serce Martyna](211016-zlamane-serce-martyna)

### Chronologiczna

* [200106 - Infernia i Martyn Hiwasser](200106-infernia-martyn-hiwasser)

## Plan sesji
### Co się wydarzyło

* Mamy statek Luxuritias, "Rajasee Bagh". Ów statek zmierzał do Valentiny na handel.
* O czym nie wiedzą inni, panicz Eryk Santorinus (24) wymknął się z ochroną by zdobyć cenne kwiaty z planety Neikatis. Zdobył je i wrócił z nimi na pokład.
    * Statek Luxuritias zboczył z kursu by to było możliwe
* Hipnokwiaty Moori jednocześnie przyszły z jedną z Plag Neikatis. Dokładniej - Śmiechowica Korozywna (Biała Pleśń)
* Silnie visiatowany piesek Brutalosław (6) Roberta Santorinusa (16) jest dotknięty Białą Pleśnią. Słabnie.
* Przestraszony Robert zażądał pomocy lekarza. I tak sprawa trafiła do Martyna, który akurat jest w pobliżu.
    * Dokładniej, Robert zgłosił bardzo niebezpieczną plagę by KTOŚ się zainteresował. I Orbiter odpowiedział.
        * Wszyscy myślą, że to tylko piesek coś zjadł, nikt nie widzi PRAWDZIWEGO zagrożenia wynikającego ze Śmiechowicy Korozyjnej.
            * Objawy Śmiechowicy: WPIERW dobry humor, potem śmiech + kaszel, uszkodzenie układu nerwowego + ryzykowne zachowania, szukanie bliskości innych, zarażanie
* Większość Rajasee Bagh jest zarażona Śmiechowicą.

### Co się stanie

* .

### Sukces graczy

* ?

## Sesja właściwa
### Scena Zero - impl

Klaudia i Martyn są przypisani do niewielkiej jednostki patrolowej. A dokładniej, Martyn jest (lekarz pierwszego kontaktu). Klaudia i Martyn zajmowali się niedawno jakąś sprawą dookoła Stacji Medycznej Atropos, więc są w pobliżu (plus, Klaudia zapewniła, że gdzie Martyn tam ona jest blisko). Ich aktualnym przełożonym jest młody porucznik który się stara ale jest WAY OVER HIS HEAD i myśli że musi dowodzić, Fabian Korneliusz. Trzeba mu oddać, że się słucha - jeśli jego ego nie ucierpi i prestiżu nie straci.

Jak jeszcze wracali z misji z okolic Valentiny na Atropos, chwilę po udanej misji.

Fabian poprosił Klaudię by ta stawiła się w Command & Control w ich dalekosiężnej korwecie Serbinius. Tam czeka już Martyn. Fabian pokazał Klaudii nagranie które dostali - co da się z tego wydobyć. Martyn kiwnął głową - poważna sprawa. Klaudia wpierw przesłuchała nagranie.

* Młody chłopak, jest ciemno. Obraz taki trochę... jakby działał dyskretnie.
* Mówi o pladze. Że niby nikt nie widzi ale widzą. Że ktoś próbuje to ukryć.
* Nasza jednostka to "Rajasee Bagh", statek Luxuritias. Lecimy w kierunku na Bramę Eteryczną. Podaję koordynaty. Proszę, znajdźcie lekarza i pomóżcie nam.
* Plaga się pojawiła po tym, jak przyjęliśmy na pokład tych uchodźców na Valentinie... nikt nie chce o tym mówić, będą udawać że jest inaczej

Fabian jest nieco zmartwiony całą sprawą. Z jego punktu widzenia nie wie o jakich uchodźcach na Valentinie, o co tu chodzi. Na Valentinie nie było żadnej plagi - sprawdził. Atropos miałoby dostęp do takich informacji. I faktycznie, Rajasee Bagh jest jednostką Luxuritias, była na Valentinie i ma leciej do Bramy, do domu. A młody człowiek został zidentyfikowany jako przedstawiciel kasty panującej rodziny dowodzącej tym statkiem, niejaki Roberto Santorinus.

Klaudia bada - poszło na częstotliwościach normalnych. Roberto wyraźnie nie ma pomysłu albo wiedzy jak raportować problemy. Klaudia próbuje dowiedzieć się więcej o nagraniu.

TrZ+4:

* V: Klaudia widzi, że Roberto PRÓBOWAŁ ukryć fakt wysłania tej wiadomości i wiadomość została odcięta. Ktoś anulował wiadomość. Gdyby nie patrole Orbitera, nikt by tego nie złapał.
    * +wiadomość wysłana 1 godzinę temu
* XXz: Klaudia chciała dostać dostęp do danych z Valentiny, ale Fabian podpadł i nie chce nikt tam mu w niczym pomóc. Nie ma więc dostępu do danych z Valentiny. Fabian musiałby bardzo przepraszać, ale nigdy tego nie zrobi.

Fabian pyta Klaudii, czy to poważna sprawa i czy trzeba by się tym zająć. Martyn twierdzi, że tak - każde zgłoszenie plagi powinno być zbadane. Fabian twierdzi, że to jakiś szczur i to z Luxuritias. Klaudia podpiera procedurami - nawet jeśli młody się bawi, to jednak trzeba się tym zająć. Jeśli dzieciak się bawi, rodzice zapłacą wszystkie koszty związane z naszą reakcją. Niezareagowanie jeśli plaga - wiele ludzi zginie a Orbiter nie pełni swojej roli.

Fabian jest niezadowolony, ale akceptuje. Serbinius leci by przechwycić Rajasee Bagh.

### Scena Właściwa - impl

Serbinius zbliżył się do Rajasee Bagh. Klaudia przygotowuje wszystkie możliwe papiery, dokumenty, autoryzacje, przepisy. Fabian wywołuje Rajasee Bagh, odbiera Comms. Nazywa się Wiktor Brzurk. Brzurk tłumaczy Fabianowi, że nie ma plagi, to chłopak który chciał pomocy lekarza bo jego pieska boli brzuszek (kobiecy głos z tyłu jak Wiktor może, to Santorinus). Fabian, co trzeba mu oddać, niezłomny. WEJDZIEMY na wasz pokład i UPEWNIMY SIĘ że nie ma plagi.

Wiktor mówi, że Luxuritas nie wyraża zgody. Mamy prawo bronić się ogniem. Fabian na to, że nawet nie zbliżą się do bramy jak otworzą ogień. Klaudia po prostu patrzy jak eskalacja rośnie, Fabian sam poprosił Klaudię by ta pokazała zgody, kary itp. Wiktor jeszcze raz podkreśla - nie ma żadnej plagi, to kwestia tego że medyczka nie chciała pomóc psu bo jest zajęta czymś innym i pies ma sraczkę. Ale Klaudia i Fabian są nieugięci. Statek Luxuritias niechętnie, ale zezwolił jednostce Orbitera na wysłanie grupy badawczej.

Klaudia, Martyn i dwóch marines weszło na pokład jednostki Luxuritias w pełnym hazmat. Pierwsze co rzuciło im się w oczy - po drugiej stronie śluzy nikt nie ma zabezpieczeń i są raczej w strojach paradnych niż wojskowych. Przywitał ich Henryk Prodon. Dowodzi ochroną. "Orbiter możecie zobaczyć i robić próbki, to wasze marnowanie czasu, możecie rozmawiać z kim potrzebujecie łącznie z Roberto Santorinusem po prostu niektóre miejsca są offlimits." Klaudia zrozumiała. Fabian chce wpierw zobaczyć młodego Santorinusa. Henryk prowadzi do jednej z bogatych kwater.

Spotkanie z młodym Roberto Santorinusem. Chłopak jest w swojej kwaterze, LUKSUSOWEJ, i jest z nim mały piesek (chihuaua). Pies wyraźnie jest niezdrowy. Roberto się poderwał "przybyliście! Pacjent jest tutaj, chodź, Brutalosław". Pies się przywlókł. Kolor Fabiana się zmienia. Martyn defusuje - pozwól, że zobaczę co dzieje się Twojemu małemu przyjacielowi. Klaudio, zbierz... wywiad. Po chwili się zastanawia - Klaudio, to nie jest zwykły pies.

Roberto z wyrzutem "oczywiście że nie! To rasowy model wyhodowany specjalnie dla mnie! To inteligentny vicinius!" Brutalosław szczeknął podnękany.

Fabian eksplodował "ŚCIĄGNĘLIŚCIE TU NAJLEPSZEGO MEDYKA ORBITERA DLA PSA?!". Roberto z wyższością "ten pies jest ważniejszy niż cały Orbiter!". Martyn "ale DLACZEGO ten pies jest w tak złym stanie? Czemu wasz medyk się tym nie zajął?". Roberto odpowiada, chętnie. Jemu wyraźnie na tym psie zależy i się martwi. Henryk tymczasem próbuje zapobiec temu, by Roberto powiedział coś... niewłaściwego.

* pies się czuje gorzej odkąd wystartowali z Valentiny - i to idzie błyskawicznie. Jeszcze 3h temu było dużo lepiej.

Klaudia -> Henryk "nawet jak to niegroźne dla ludzi, możecie skrzywdzić dużo zwierząt". Henryk na to "lecimy bezpośrednio do bazy". Klaudia "no właśnie"

* Roberto przyznał, że nie było żadnych uchodźców i nie ma plagi. Zmyślił. Bo martwi się o Brutalosława. Henryk dyskretnie facepalmował.
* Roberto nie rozumie, czemu Bożena (medyczka) nie chce zająć się psem. Henryk TEŻ tego nie rozumie. Ale on nie jest od tego by zajmować się psem.

Martyn, łagodnie "o klasie osoby świadczy jak traktuje zwierzęta. Pozwól, że się przyjrzymy Brutalosławowi."

Martyn + Klaudia zaczynają rozstawiać medlab. Henryk "może będzie wam wygodniej bez tego" (wskazując na hazmat). Fabian "nie, przepisy to przepisy. PLAGA TO PLAGA." Łypiąc groźnie na Roberto, który zrobił UŚMIESZEK. Fabian dyskretnie "ZGŁOSILI PLAGĘ, BĘDZIE PLAGA! Nie zdejmujemy hazmatów."

Henryk zapytany czy na statku są zwierzęta, potwierdził. Są też inne psy, inne istoty - ale tylko Brutalosław wskazuje na to, że coś mu jest. Nie, pies nie schodził z pokładu na Valentinie... nie, schodził. Był tam gdzie Roberto. A gdzie był Roberto? FANTASTYCZNA PRZYGODA NA VALENTINIE. A pies z nim.

Klaudia i Martyn robią badanie krwi itp. Brutalosława. Co i czy mu jest. 

TrZ (to drugie, połączone siły + mikromed) +3

* X: nie ma dość mocy by ten medlab dał definitywne wyniki
* V: tu coś jest. Coś jest z psem nie tak. Coś wpłynęło na visiat. Visiat jest niekompletny. To choroba visiat - dlatego Brutalosław cierpi.
* V: Klaudii udało się znaleźć ślady czegoś co wygląda jak zarodniki jakiejś pleśni w visiat. Grzybica.

Martyn spoważniał. Większość grzybic visiat jest bardzo niebezpieczna i wymaga silnych dekontaminacji. Zwykle rekomendacja jest jedna - NIE WCHODZIMY W BRAMY i UNIKAMY POLA MAGICZNEGO. I silna dekontaminacja. Ale tym bardziej - Czemu. Medyczka. Się. Tym. Nie. Zajęła. I to jest pytanie jakie Martyn zadał na głos. Henryk przyznał, że nie zna odpowiedzi na to pytanie. Martyn dyskretnie przekazał to Fabianowi - mają grzybicę visiat, nie wie którą.

Fabian się naprawdę ucieszył. Po prostu widać radość na ryju. Ale jeszcze nic nie powiedział. Po chwili - "macie plagę. Macie cholerną plagę. Dobrze że nas wezwałeś młody". Henryk ma minę jakby zjadł żabę. "Orbiter, WIEM, że jesteście zirytowani. Ale NIE MA ŻADNEJ PLAGI." On w to wierzy. "Nie róbcie kłopotów dyplomatycznych tam gdzie nie trzeba ich robić".

* Klaudia -> Martyn: "rozsiewa?"
* Martyn -> Klaudia: "nie, jeszcze nie"
* Klaudia -> Martyn: "jest szansa że zdrowi, pytanie, jak złapał"
* Martyn -> Klaudia: "i czy to rozprzestrzenił na Valentinie... musimy sprawdzić logi statku"

Klaudia -> Henryk. Jest naukowcem, ma dowody. Martyn -> Henryk. Jest lekarzem, martwi się. Umieją przekonywać. Klaudia chce by Henryk stał się sojusznikiem a Martyn chce pobrać krew od Roberto. Roberto panikuje. TO BRUTALOSŁAW JEST CHORY NIE ON. Klaudia poprosiła Martyna by wpierw pogadać jednak z Henrykiem... Martyn potwierdził.

Klaudia, Martyn i Fabian próbują przekonać Henryka, że plaga jest prawdziwa i musi współpracować. Nawet nie z Orbiterem - po prostu.

Ex Z(fakty, dowody i to że Orbiter ma reputację na szali) +3:

* V: Klaudia "jak wlecicie w Bramę, możecie nie wyjść". Henryk bardzo niechętnie, ale będzie współpracować. Acz nie przeciwko swoim mocodawcom. Ale widzi skalę problemu.

Fabian wyraźnie zadał Henrykowi serię pytań, które nie są może bardzo przydatne, ale są. Klaudia dalej nie rozumie, czemu medyczka nic nie zrobiła. Henryk potwierdził przypuszczenia Fabiana - Bożena jest czarodziejką, Teodor - lord strażnik Santorinusów - jest czarodziejem.

Klaudia chce porozmawiać z medyczką. Henryk się oczywiście zgodził.

Bożena jest w skrzydle szpitalnym statku - ogląda sobie coś, nie ma szczególnie pacjentów. Henryk jej przerwał między dwoma atakami śmiechu. Bożena "proszę, proszę". Wyśmiała wygląd Klaudii - wygląda jak kosmitka. Strasznie nieprofesjonalna. Martyn próbuje do niej spokojnie, ale Bożena jest w niesamowicie dobrym humorze. Henryk jest zdziwiony jej zachowaniem.

TrZ (encyklopedyczna wiedza Martyna na temat wszystkich tematów biowojennych) +3:

* X: nie ma dowodów i nie pamięta dokładnie
* V: Martyn zidentyfikował rodzinę problemu

Martyn -> Klaudia "to grzyb z Neikatis. Nie skrajnie letalny, ale dezorganizuje. Na pewno mają tego mnóstwo w ludziach dookoła. Na pewno nie złapali tego na Valentinie - Valentina kwarantannuje wszystkich przybyszy z Neikatis. Czy na podstawie tego co mamy możesz złożyć coś co zbada w ścianach, w wilgoci, w ludziach..?"

Klaudia kazała Henrykowi sprawdzić, jakie jest zachowanie wszystkich magów na pokładzie. Czy inni magowie są tym dotknięci. Henryk wydał odpowiednie dyspozycje i wysłał swoich agentów. Spytał też cicho "czy mam założyć hazmat czy..." Martyn i Klaudia powiedzieli, że za późno.

TrZ (Henryk ma prawo do tego) +3:

* X: Henryk ma pewne komplikacje, bo to inny pion i nie powinien ich śledzić
* X: Teodor osobiście jest niezadowolony.
* V: Wszyscy magowie na pokładzie są w jakimś stopniu zarażeni. Da się poznać po głupim śmiechu.
    * Teodor nie. On też się śmieje, ale też kaszle krwią. Czyli on jest wektorem. I nie zauważa tego, że kaszlenie krwią nie jest normalne.

Martyn cicho zauważa, że to "że nie widzi że nienormalne" jest dowodem tego, że grzyb uderzył w układ nerwowy i w link visiat - ciało. Krew jest nośnikiem. Oddech również. Ale bez odpowiedniego pola magicznego grzyb się rozwija powoli. Na szczęście Orbiter ma na to lekarstwa i środki. Klaudia "sprowadzamy?" Martyn "musimy mieć request ze statku lub inną formę rekwizycji". Martyn zauważył, że można poprosić Roberto - by zdobyć środki by pomóc psu. 

Zdaniem Martyna (i Fabian się zgadza) chcemy ten statek do Atropos. Atropos to wyczyści skutecznie. Henryk na to, że on nie ma uprawnień - uprawnienia ma Teodor i kapitan statku (człowiek), kapitan Leonidas Margrabarz (spokrewniony z kapitanem). Szczęśliwie, kapitan nie jest magiem a człowiekiem.

Klaudia scavenguje systemy medyczne Rajasee Bagh + wspomaga się magią by zrobić sprawny detektor i wizualizator - to da silny dowód.

TrZM+3:

* Vm: Klaudia przekształciła pół systemów medycznych w detektor, który wizualizuje grzybki i ich rozprzestrzenianie się. Na całym statku. SZOK dla wszystkich obecnych.

Klaudia decyduje się nie eskalować zaklęcia; Atropos sobie poradzi z dokładną analizą danych. Henryk jest dość czysty. Ale cały statek jest zarażony. Grzybek jest praktycznie wszędzie. Potrzebna będzie fumigacja. Neikatiański grzyb... co ciekawe, jest konkretny OBSZAR statku który ma wysokie stężenie i to nie lifesupport - to jeden z magazynów. Henryk powiedział, że w tym nie powinno niczego być. Po chwili się złamał.

* H: "Co możecie powiedzieć mi o tym grzybie? Pomogę Wam jak jestem w stanie."
* K: "Wejdziecie w Bramę, nie wyjdziecie. Teodor jest pacjentem zero. Grzyb jest z Neikatis."
* H: "Gdy byliśmy na Valentinie, nasza daleka korweta dostała specjalną misję. Teodor + Achellor, nie wiem gdzie polecieli, ale nie było ich kilka dni. Potem opuściliśmy Valentinę i wrócili. A potem pies się rozchorował."
* M: "Ta korweta będzie miała dane w logach, informacje gdzie byli."
* H: "Dobrze, ale jeśli polecieli gdzieś gdzie powinni to złamali prawo..."
* K: "A jeśli nie byli na Neikatis? Te parę dni nie wskazuje na Neikatis, raczej na coś bliżej"

Fabian chce by Henryk udostępnił logi itp. Henryk chce pomóc, ale nie kosztem stanowiska itp - niech to będzie dyskretne. 

Tr (niekompletny) Z (plaga to plaga a tylko Orbiter może pomóc) +3:

* X: Henryk osłonił Luxuritias przed złamaniem sankcji itp. ALE nie Santorinusów.
* V: Henryk przekazał informacje o korwecie, logach itp. Nie trzeba było tam iść. Oczywiście, dane są wymazane - ale Klaudia może je odtworzyć.

KLAUDIA ODTWARZA DANE (test przyszłości - ona zrobi to na pokładzie Serbiniusa a nie tutaj)

Tr Z (Henryk pomógł i przekazał rozkaz do TAI) +3:

* V: Klaudia ma lokalizację - spotkali się z inną jednostką "prywatną" w kosmosie która złamała kwarantannę neikatiańską
* X: dane są dość uszkodzone i wymagają dodatkowych informacji od TAI (których ta udzieli, ale zostaje ślad ze strony Orbitera)
* X: kapitan zmartwi się tymi queries i tematami.
* V: Klaudia ma nazwę jednostki i lokalizację. To jest jedna z neikatiańskich jednostek; typowy szmugler. Ale oni WIEDZĄ co szmuglują - Klaudia wie, że szmugler by odradzał niektóre rzeczy Luxuritiasowi. Więc Luxuritias musiał się uprzeć.

Na pokładzie Rajasee Bagh, Teodor skonfrontował się z Martynem, Klaudią i Fabianem. I Henrykiem. Jest uzbrojony. Koleś jest zarażony i jest w złej formie, chce, by Orbiter się stąd oddalił. Rajasee Bagh opuszcza teren Orbitera i nie chce nic z nimi mieć wspólnego.

Martyn widząc jego stan wie, że nie przemówią do niego. Trzeba go obezwładnić. Fabian ma skupiać uwagę, być nastroszonym indorem (nie mówimy mu tego), Klaudia trochę odwraca uwagę ochrony, Henryka i Bożeny a Martyn na którego nikt nigdy nie zwraca uwagi rzuca zaklęcie by wprowadzić Teodora w szok - stazę. Martyn jest ekspertem od stazy i utrzymaniu przy życiu i wie, jak te grzybki działają.

TrZ (dywersja itp) M+2:

* Vm: Martyn doprowadził do WYŁĄCZENIA organizmów zarówno Teodora jak i Bożeny. Jego magia spowodowała szok na linii ciało - visiat - pleśń, po czym ich pozbawił energii. Padają na ziemię nieprzytomni i wyssani. Grzyb nie na czym pasożytować.

Henryk, Fabian i wszyscy patrzą z wielkimi oczami na Martyna. Martyn wzruszył ramionami "oddaję się dyspozycji dowódcy, uniosłem się", spokojnym głosem. Klaudia "potwierdzam, uniósł się", tym samym beznamiętnym głosem. Fabian "dobrze. Dobrze zadziałałeś. Dokładnie tak jak chciałem", chcąc kontynuować efekt a wyszedł cringe. "Teraz tylko wyjaśnić kapitanowi co się stało i że musimy Rajasee Bagh przekierować do Atropos."

Henryk przejął tymczasowo dowodzenia nad wszelkimi siłami i udowodnił, że Teodor jest chory. Kapitan widząc to wszystko zgodził się na przelot do Atropos i kwarantannę i fumigację. Achellor zrobił temper tantrum, bo kwiaty które ma z Neikatis zostaną zniszczone - ale to one są wektorem. A czemu ich potrzebował? Dla dziewczyny, nie wierzyła, że jest w stanie coś takiego zdobyć.

Spoiler - nie był. Bo Orbiter zabrał. Ach ten niedobry Orbiter...

## Streszczenie

Młody panicz dał sygnał alarmowy, że plaga na pokładzie statku Luxuritias opuszczającego Valentinę. Serbinius poleciał zbadać sprawę. Na miejscu okazało się, że chłopak chciał by jego psa zobaczył weterynarz. A JEDNAK NIE. Faktycznie jest plaga - z Neikatis. Zespół wszedł w sojusz z szefem ochrony statku i plaga została powstrzymana a statek odholowany do Atropos na fumigację (Śmiechowica to typ grzyba).

## Progresja

* Fabian Korneliusz: ma jakieś problemy z Valentiną z uwagi na swoją gorącą głowę i ego; nie dostaje stamtąd wsparcia czy pomocy.

### Frakcji

* .

## Zasługi

* Klaudia Stryk: przypisana do OO Serbinius; z Martynem doszła do tego że Rajasee Bagh jest zakażony Śmiechowicą. Przekonała szefa ochrony że musi współpracować i magią zwizualizowała grzybka. Odbudowała logi - skąd się wzięła Śmiechowica.
* Martyn Hiwasser: przypisany do OO Serbinius; z Klaudią doszedł do tego że Rajasee Bagh jest zakażony Śmiechowicą. Gdy Klaudia i Fabian robili dywersję, jednym zaklęciem zdjął najgroźniejszego z zarażonych. Cichy jak zawsze.
* Fabian Korneliusz: młody (24) porucznik Orbitera, dowódca Martyna i Klaudii na OO Serbinius; niekoniecznie utrzymuje język na wodzy. Petty one, cieszy się z nieszczęścia Luxuritias i z przyjemnością chowa się za procedurami. Ale współpracuje z Luxuritias by zatrzymać plagę Śmiechowicy na ich jednostce.
* Achellor Santorinus: panicz Luxuritias (27); by móc popisać się przed dziewczyną spróbował kupić od szmuglerów niebezpieczne i piękne kwiaty z Neikatis. Skończyło się zarażeniem okrętu Luxuritias Śmiechowicą.
* Roberto Santorinus: panicz Luxuritias (16); rozpieszczony. Zmartwił się chorobą jego psa i zgłosił plagę by KTOŚ coś zrobił. Okazuje się że plaga jest prawdziwa XD, więc przez przypadek się przydał. Out of his depth.
* Teodor Margrabarz: lord strażnik Santorinusów i mag (44), współpracując z Achellorem przeszmuglował piękne kwiaty z Neikatis i zaraził się Śmiechowicą. Niebezpieczny w walce, ale obezwładniony przez Martyna jednym zaklęciem.
* Bożena Kokorobant: osobista lekarka Santorinusów i czarodziejka (39), zarażona Śmiechowicą, nie chce zająć się psem Roberto i w sumie nic nie robi tylko wszystko ją bawi.
* Henryk Urkon: szef ochrony SL Rajasee Bagh. Niechętny do Orbitera, ale przekonany przez Klaudię współpracuje by uchronić statek przed Śmiechowicą. Ochronił swoich mocodawców politycznie przed gniewem Orbitera.
* OO Serbinius: szybka korweta patrolowa przypisana do Atropos i misji medycznych; przechwyciła Rajasee Bagh by zatrzymać tajemniczą plagę zgłoszoną przez dzieciaka. Dowodzi Fabian Korneliusz.
* SL Rajasee Bagh: statek Luxuritias powiązany z klanem Santorinus. Przez nierozsądną próbę szmuglu pojawiła się tam Śmiechowica; fumigacja będzie mieć miejsce na Atropos.

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański

## Czas

* Opóźnienie: -424
* Dni: 2

## Konflikty

* 1 - Klaudia bada - poszło na częstotliwościach normalnych. Roberto wyraźnie nie ma pomysłu albo wiedzy jak raportować problemy. Klaudia próbuje dowiedzieć się więcej o nagraniu.
    * TrZ+4
    * VXXz: Roberto PRÓBOWAŁ ukryć wiadomość o pladze, ale przez kłopoty jej szefa (Fabiana) z Valentiną nie ma opcji zdobycia stamtąd danych.
* 2 - Klaudia i Martyn robią badanie krwi itp. Brutalosława. Co i czy mu jest. 
    * TrZ (to drugie, połączone siły + mikromed) +3
    * XVV: nie ma mocy by definitywne wyniki, ale jest coś co atakuje visiat. To choroba visiat - forma pleśni.
* 3 - Klaudia, Martyn i Fabian próbują przekonać Henryka, że plaga jest prawdziwa i musi współpracować. Nawet nie z Orbiterem - po prostu.
    * Ex Z(fakty, dowody i to że Orbiter ma reputację na szali) +3
    * V: Klaudia przekonuje, że jak wejdą w Bramę to nie wyjdą. Henryk niechętnie ale współpracuje.
* 4 - Martyn próbuje do niej spokojnie, ale Bożena jest w niesamowicie dobrym humorze. Henryk jest zdziwiony jej zachowaniem.
    * TrZ (encyklopedyczna wiedza Martyna na temat wszystkich tematów biowojennych) +3
    * XV: Martyn nie pamięta dokładnie ale kojarzy grzyby z Neikatis.
* 5 - Klaudia kazała Henrykowi sprawdzić, jakie jest zachowanie wszystkich magów na pokładzie. Czy inni magowie są tym dotknięci. Henryk wydał odpowiednie dyspozycje i wysłał swoich agentów.
    * TrZ (Henryk ma prawo do tego) +3
    * XXV: Henryk ma komplikacje i Teodor osobiście przyjdzie na opierdol, ale wszyscy magowie są zarażeni (poza Klaudią i Martynem). A Teodor jest wektorem.
* 6 - Klaudia scavenguje systemy medyczne Rajasee Bagh + wspomaga się magią by zrobić sprawny detektor i wizualizator - to da silny dowód.
    * TrZM+3
    * Vm: Klaudia przekształciła pół systemów medycznych w detektor, który wizualizuje grzybki i ich rozprzestrzenianie się. Na całym statku. SZOK dla wszystkich obecnych.
* 7 - Fabian chce by Henryk udostępnił logi itp. Henryk chce pomóc, ale nie kosztem stanowiska itp - niech to będzie dyskretne. 
    * Tr (niekompletny) Z (plaga to plaga a tylko Orbiter może pomóc) +3
    * XV: Henryk osłonił Luxuritias, ale nie Santorinusów. Przekazał informacje o korwecie itp - Klaudia ma ślad skąd to się wzięło.
* 8 - KLAUDIA ODTWARZA DANE (test przyszłości - ona zrobi to na pokładzie Serbiniusa a nie tutaj)
    * Tr Z (Henryk pomógł i przekazał rozkaz do TAI) +3
    * VXXV: Klaudia ma lokalizację; dane są z neikatiańskiej jednostki i uszkodzone. Szmugler przekazał Luxuritiasowi mimo że ostrzegł.
* 9 - Fabian ma skupiać uwagę, Klaudia trochę odwraca uwagę ochrony, Henryka i Bożeny a Martyn na którego nikt nigdy nie zwraca uwagi rzuca zaklęcie by wprowadzić Teodora w szok i stazę.
    * TrZ (dywersja itp) M+2
    * * Vm: Martyn doprowadził do WYŁĄCZENIA organizmów zarówno Teodora jak i Bożeny. Jego magia spowodowała szok na linii ciało - visiat - pleśń, po czym ich pozbawił energii.
* 10 -
    * 
    * 
* 11 -
    * 
    * 

## Kto

### Faceless NPC:

* **Name**: Teodor
* Wartości
  * Moje
    * Universalism: shed your mortal coil and your clan; we all are the same, expand
    * Stimulation: feel alive, feel more, new things, experiment, explore
    * Self-direction: autonomy, ability to control your own way and direction
  * Nieistotne
    * Face: prestige; having a specific reputation and upholding it
    * Humility: we have a place in existence, we are nothing compared to the Reality; faith
* Osobowość
  * Ocean
    * ENCAO:  +-0--
  * Jak się objawia osobowość
    * Samolubny, skupiony na sobie
    * Doskonale radzi sobie pod wpływem silnego stresu
    * Z natury odrzuca wszystkie pomysły, trzeba powoli przekonywać
    * Chwalipięta, czego to nie zrobił...
* Potencjalna praca:
  * Klasyfikator wyrobów przemysłowych
  * Makler morski
* Potencjalne silniki motywacyjne:
  * Moje silniki:
    * Daredevil: Takie życie jak mają jest nudne i bezsensowne. Niech się obudzą, niech mnie podziwiają. Niech widzą co można! Muszę działać na krawędzi.
    * Unikalne przeżycia: everything passes, memories remain. Jeśli czegoś sobie odmówię, będę żałować do końca życia.
  * Silniki WROGA lub anty-silnik:
    * Sprawiedliwość: mniej istotne co jest DOBRE, ważniejsze co jest SPRAWIEDLIWE. Zły czyn musi być odpowiednio ukarany, niezależnie od statusu.
* **Name**: Bożena
* Wartości
  * Moje
    * Power: more influence upon reality, amass wealth and influence
    * Stimulation: feel alive, feel more, new things, experiment, explore
    * Family: blood, clan or mafia. The group and its prosperity is the most important
  * Nieistotne
    * Conformity: being one of the group, homogenity of a group
    * Humility: we have a place in existence, we are nothing compared to the Reality; faith
* Osobowość
  * Ocean
    * ENCAO:  -+000
  * Jak się objawia osobowość
    * Polega tylko na sobie; nie wierzy w innych
    * Profesorski, mówi mądrze, ale niekoniecznie wobec targetu
* Potencjalna praca:
  * Obuwnik miarowy
  * Operator maszyn rolniczych
* Potencjalne silniki motywacyjne:
  * Moje silniki:
    * Kapitan Ahab: polowanie na białego wieloryba do końca świata i jeszcze dalej, nieważne co przy tym stracimy.
    * Ratunek przed chorą miłością: mój kuzyn się zakochał w osobie ze straszną przeszłością i osobowością. Ślub / partnerstwo nie może dojść do skutku!
  * Silniki WROGA lub anty-silnik:
    * Tajemnica Wszechświata: zrozumieć coś niezrozumiałego, zrozumieć co kryje się po drugiej stronie światła.
* **Name**: Fabian
* Wartości
  * Moje
    * Self-direction: autonomy, ability to control your own way and direction
    * Security: being safe or keeping others safe
    * Power: more influence upon reality, amass wealth and influence
  * Nieistotne
    * Hedonism: pleasure above anything else
    * Conformity: being one of the group, homogenity of a group
* Osobowość
  * Ocean
    * ENCAO:  -0+-0
  * Jak się objawia osobowość
    * Apatyczny, niewiele tę osobę motywuje
    * Brutalny, bezwzględny, grubiański
    * Uważny, zwraca uwagę na szczegóły
* Potencjalna praca:
  * Technik automatyk sterowania ruchem kolejowymS
  * Projektant grafiki
* Potencjalne silniki motywacyjne:
  * Moje silniki:
    * Ostateczne mistrzostwo: pokonać godnego przeciwnika będąc na niedowadze; zdeklasować swoich oponentów, osiągnąć status i skillset mistrza.
    * Awans za wszelką cenę: albo lepsza firma albo wojna o stanowisko z innymi. To stanowisko będzie moje.
  * Silniki WROGA lub anty-silnik:
    * Utopia Star Trek: znajdowanie porozumienia i dobra nawet w osobach bardzo różnych od nas. Współpraca i koegzystencja.
* **Name**: Wiktor
* Wartości
  * Moje
    * Family: blood, clan or mafia. The group and its prosperity is the most important
    * Security: being safe or keeping others safe
    * Hedonism: pleasure above anything else
  * Nieistotne
    * Tradition: what once was still works, only a fool throws away the knowledge of the past
    * Power: more influence upon reality, amass wealth and influence
* Osobowość
  * Ocean
    * ENCAO:  0--+0
  * Jak się objawia osobowość
    * Kontrolowany przez bardzo silne emocje
    * Nie kłania się nikomu, z podniesioną głową
    * Pomaga innym którym potrzebna jest pomoc
* Potencjalna praca:
  * Felczer
  * Maglarz
* Potencjalne silniki motywacyjne:
  * Moje silniki:
    * Networking: poznać nowe osoby, budować sieci przysług i znajomych. Kariera, moc i znajomości.
    * Tępić debili: nienawidzę głupoty do poziomu NUKLEARNEGO. Niech cierpią. Sama przyjemność. I zemsta.
  * Silniki WROGA lub anty-silnik:
    * Ukryć sekret: może komuś zapłaciłem, może coś zrobił mój ród. Ale 'sleeping dogs must lie unearthed'.
* **Name**: Henryk
* Wartości
  * Moje
    * Power: more influence upon reality, amass wealth and influence
    * Conformity: being one of the group, homogenity of a group
    * Security: being safe or keeping others safe
  * Nieistotne
    * Achievement: build more skills, achieve more, expand on what you can do
    * Self-direction: autonomy, ability to control your own way and direction
* Osobowość
  * Ocean
    * ENCAO:  +0-0-
  * Jak się objawia osobowość
    * Nigdy się nie nudzi, lubi rutynę i powtarzalne rzeczy
    * Bazuje na intuicji jako podstawie działania
    * Celuje w jak największe nagrody
* Potencjalna praca:
  * Kierownik działu logistyki
  * Pozostali sprzedawcy sklepowi (ekspedienci)
* Potencjalne silniki motywacyjne:
  * Moje silniki:
    * Zemsta Andrei Beaumont: coś się wydarzyło dawno temu. Pozostała tylko zemsta na kimś możnym, kogo nie mam jak dorwać. Nieważne jakim kosztem.
    * Cancel Culture: wykluczenie kogoś przez grupowy ostracyzm. Wszyscy się dopasują do mojego świata i wersji świata / kultury. Lub tępienie injustices.
  * Silniki WROGA lub anty-silnik:
    * Carmen Sandiego: oddać skarby / relikwie tam, skąd przybyły. Naprawić szkody i krzywdy spowodowane przez grabież.

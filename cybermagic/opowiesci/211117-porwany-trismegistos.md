---
layout: cybermagic-konspekt
title: "Porwany Trismegistos"
threads: legenda-arianny
gm: żółw
players: kić, fox, kapsel
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [211110 - Romans dzięki Esuriit](211110-romans-dzieki-esuriit)

### Chronologiczna

* [211110 - Romans dzięki Esuriit](211110-romans-dzieki-esuriit)

## Plan sesji
### Co się wydarzyło

* Trismegistos i Tivr
    * Trismegistos, statek wynajęty z Neikatis przez grupę z Aurum chcącą odwiedzić planetę (Dystrykt Lennet). 11 arystokratów Aurum
        * Klara Gwozdnik i jej miragent -> czysto dla przyjemności
        * Albert Terienak, Robert Terienak -> szukali możliwości znalezienia szlaku handlowego
        * Wiktor Maus, Kamila Maus -> KIA; nie da się jej przekształcić
        * Marysia Samszar -> przyjaciółka Klary
        * 3-4 inne rody
    * Kapitan Trismegistosa okazał się być piratem porywającym i mindwarpującym ludzi i magów. Nazywa się Jamon Korab. 
        * Jego pierwsza oficer to piękna Mira Anastel, silnie zmieniona drakolitka.
    * Miragent widząc co się stało z ludźmi na pokładzie Trismegistosa wykonał manewr "wydostanę się z mindwarp". Nieprzekonany o tym że Orbiter pomoże Aurum, zrobił manewr jako pirat z "Plugawego Jaszczura" - porwał statek, ma tam broń biologiczną i żąda tańca Arianny w bieliźnie.
    * Samozniszczenie dorwało miragenta ZANIM przybywa doń Tivr. Ale miragent zostawił ślady na Trismegistosie.
    * Trismegistos ma na pokładzie neikatiańskie Trepanatory - roboty bojowe wstrzykujące straszliwe chemikalia i unieruchamiające ofiary.
* Załoga Inferni
    * Elena: jej Korupcja jest strasznie silna. Unika wszystkich. Pożera ją sumienie. Unika Eustachego i czarowania. Ucieka w formalizmy i do swojej kajuty. Nie mówi ani słowa - lub atakuje. "I am the monster".
    * Maria: przybywa na Infernię z prośby Martyna (dead man's hand --> Klaudia). Broni honoru Martyna jak potrafi.
    * Leona: wypisała się ze szpitala. Słaba jak cholera, ale sprawna. Nie pozwoli, by jej statek umarł "przez Eternię".
    * Verlenowie: trzymają się silnie. 1 komandos + Otto. Otto wymusza twarde podejście na pokładzie.
    * Kamil: zapalił fanatyzm części ludzi jeszcze silniej.
    * Młodsza część załogi Inferni którzy nie uciekli w religię: morale złamane.
    * Noktianie: chcą doprowadzić Tivr (Aries Tal) do dawnej wielkości. Widzą to jako szansę na SWOJĄ jednostkę pod dowództwem Arianny. Morale podniesione. Straty... cóż, noktianie przywykli.
* Co się wydarzy
    * ( Tivr ma zniszczonego miragenta na pokładzie. Pokłosie Sowińska - Morlan. Wygląda jak martwa Arianna. )
    * Maria wchodzi na pokład, z prośby zdalnej Martyna. Starcie Leona - Maria. ("Jestem tylko lekarzem" + brak strachu). NIKT jej nie ufa XD.
    * Starcia między "kultystami", "noktianami" i złamanymi.
        * Rafał Kot (kultysta), Franciszek Muchacz (kultysta), Janina Leszcz (kultysta)
    * Szepty Seweryna Atanaira.
    * Dziennikarz z pytaniem o wiadomość od pirata. "Krwawy Jonasz Pudelek".

### Sukces graczy

* .

## Sesja właściwa
### Scena Zero - impl

.

### Scena Właściwa - impl

Wiadomość od "pirata" ("Krwawy Jonasz Pudelek", pirat z "Plugawego Jaszczura"):

* Wolności dla Dystryktu Lennet. Takie prawa jakie chcemy. Takie działania jakie chcemy.
* (nie chce statku)
* Arianna tańcząca w bieliźnie do theme song Sekretów Orbitera.  
* 124k kredytek (niewielka suma)
* broń biologiczna w ścianach.

Mamy na Tivrze:

* Kamil Lyraczek
* Maria Naavas
* Leona Astrienko
* Elena Verlen
* Izabela Zarantel
* Raoul Lavanis: advancer noktiański
* Roland Sowiński + Zygfryd Maus: obserwator z Aurum. 

...

Eustachy dostał antyrad. Maria go postawiła. Izabelę też postawiła; ta jest w złej formie, ale wystarczająco dobra by móc działać. Inni dziennikarze są biedni. Ale są naciski polityczne ze strony Aurum, że chcą OBSERWATORA na Tivr. Kramer to próbuje blokować, ale Aurum ma trochę racji. Sowińscy prą by to był Roland, ale pomniejsze rody prą na kogoś innego. Zygfryd Maus. Weteran. Niebezpieczny. Koleś ma opinię honorowego, niebezpiecznego ale pragmatycznego. Rycerz Karradraela. (profil: Bergsten)

Eustachy zaproponował by wziąć OBU. Zygfryd bo jest kompetentny. Roland, bo sabotuje Zygfryda. "Jesteśmy zajebiści, damy rady". Zygfryd ma jako jedyną broń miecz. Roland ma dekoracyjne pistolety.

Na Tivrze są zwłoki Arianny. Kamil je znalazł. Maria zidentyfikowała to jako martwy miragent. Klaudia próbuje ustalić jak dawno to się stało. Klaudia sprawdza nagrania z tego miejsca + K1, korytarzy itp. Kiedy mniej więcej. 

TrZ+3: 

* Xz: Klaudia ma ograniczane dostępy na K1. Orbiter odcina Klaudii dostępy.
* Vz: Jeszcze ma dostępy - miragent pojawił się na Tivrze parę godzin po tym, jak Tivr trafił do Arianny. Wszedł sam. Wyglądał jak jakiś technik czy coś.

Ciekawe. Ale nie jest to coś co mogą rozwiązać teraz.
Reporter "pani komodor! Czy pani spełni skandaliczne wymagania prześladowcy pirata?"

* X: Klaudia odcina reportera; +1 do "naughty Klaudia"
* V: Klaudia odcięła reportera. Stracony sygnał.

Klaudia wsadziła miragenta do kajuty Arianny na kajucie na łóżko. Niech udaje że śpi. I niech jest ubrana w bieliznę XD.

Orbiter wysyła Infernię z _jakąś załogą_ że niby Arianna leci spełniać żądania. A tymczasem - Tivr leci na pełnej mocy. Infernia NIE ZDĄŻY przed końcem limitu czasu. Nawet Tivr musi włączyć dopalacze.

Arianna: "Eustachy idź sprawdź silniki, Eleno potrzebuję Twojej pomocy". Elena się słabo uśmiechnęła - pilotuje.

Elena pilotuje Tivr by zdążyć do Trismegistosa

TrZ+4:

* V: Elena zdąży do Trismegistosa.
* V: Elena mega zaimponowała noktianom z perspektywy pilotażu Tivra. Przyleciała dużo szybciej niż to możliwe. CAŁĄ NOC ĆWICZYŁA W SYMULATORZE! (by nie myśleć o Eustachym)

Obserwatorzy na mostku. Zygfryd bawi się z Rolandem, straszy go trochę i ma poczucie humoru.

Gdy jesteście 2h do celu Tivr przechwycił wiadomość nadawaną z Trismegistosa. "Alarm odwołany. Porywacz nie żyje. To nie był pirat, to był miragent. Nie ma broni biologicznej, arystokraci są na pokładzie. Kontynuujemy kurs." - i pieczęć kapitana.

Eustachy chce nadać do nich, że leci Orbiter by się pozbyć terrorysty i zgodnie z rozkazami mają poddać się inspekcji. Maskujemy Eustachego i jest kapitanem Tivra. Eustachy łączy się z kapitanem Trismegistosa. Jamon Korab powiedział że jest OK, i w ogóle. I wszyscy są. Eustachy powiedział że ma wejść na pokład, sprawdzić czy OK, wypełnić druczki i zdecydować czy może puścić samopas czy ewakuować.

Półtora godziny później Tivr zbliżył się do Trismegistosa. Mira się bawi głosem i nazwą "Tivr". Mira ma przesłać nagrania z tego co tam miało miejsce i jak pokonali miragenta.

TrZ (Klaudia jest nieogarnięta w kłamaniu a Arianna nią steruje) + 4:

* V: Arianna orientuje się, że Mira wie jak przesłać i się pogubiła. Coś tam z tego co Mira mówi jest nie tak. 
    * Zgodnie z danymi z Trismegistosa - Jamon jest kapitanem. Mira jest pierwszym oficerem. I Mira tak wygląda.

Są DWA abordaże. Pierwszy oficjalny, Eustachy z przydupasami, Leoną i z obserwatorami z Aurum. Druga grupa to Raoul i Elena? Plan wygląda dobrze, ale problem polega na tym, że to może być pułapka.

Klaudia zaproponowała Mirze, że ona zrobi tą pracę - połączy się z Trismegistosem i ściągnie dane. Mira się przestraszyła. "Tak się da?" Klaudia powiedziała, że jeśli przekona AI, to się może udać. Mira powiedziała, że musi spytać kapitana. Potrzebuje 5 min. I się rozłączyła.

5 minut później pojawił się kapitan Jamon. Wyraźnie spał. Przyznał, że magowie rodu Maus nie żyją. "I liczyliście że da się ukryć do kiedy?" "Aż dolecimy do planety i schowamy się przed magami Aurum".

* X: Wygląda na to, że Jamon mówi wiarygodnie. Nie ucieka, bo miragent coś uszkodził. Niewiele rzeczy działa.

Arianna chce informacje o arystokratach jacy są. Arianna szuka relacji między nimi. To byli "odrzutki squad". Nie ma sensu, że im to wisi. Coś tam jest cholernie nie tak.

W grupie abordażującej: Elena w Eidolonie (niewidoczna), Leona, Zyfryd (bez Rolanda), 4 z ochrony. A Roland na mostku z Arianną.

Na statek weszła ta szóstka (+ niewidzialna Elena po powierzchni do silników).

Elena :

TrZ (zaskoczenie + dywersja) +4 +O (serce):

* V: Elena dała radę podłożyć ładunki pod silniki
* X: Ichnia TAI zorientowała się, że coś jest nie tak - infiltracja (jeszcze nie teraz)
* V: Elena dała radę podpiąć Klaudię do pewnych zewnętrznych sygnałów statku.

DRUGA LINIA: 

Gdy tylko TAI statku zasygnalizowało, że Coś Jest Nie Tak z sensorami itp. to wszyscy magowie na pokładzie Trismegistosa plus załoga obrócili się przeciwko ekspedycji. Ekspedycja nie chciała robić krzywdy Aurum itp - a Leona udaje, że magia na nią działa.

Do Eustachego - Kapitan Jamon.

"Przepraszam, ale jesteście jeńcami. Mamy koloidowy statek i jesteście celem. Wyłączcie reaktor i silniki lub Was zestrzelimy."

Eustachy odkleja sztucznego wąsa. JEST EUSTACHYM KORKORANEM. TO NIE JEST BROŃ ORBITERA! TO JEST MOJA BROŃ! TO JA ŚCIĄGAM SERENIT!

Klaudia wysyła sygnały do biednej TAI Trismegistosa. TO JEST SERENIT.

TrZ+3+3Vg:

* Vg: Trismegistos i jego TAI są w trybie paniki. Trismegistos - wszystkie alarmy. TAI przejmuje kontrolę nad statkiem.
* Vz: TAI odcina kapitana, pierwszego oficera itp. Tivr dostaje komunikację od TAI bezpośrednio.
* V: Klaudia dała wrażenie, że ORBITER kontroluje Serenit.

"Tu TAI Kalira d'Trismegistos. Kapitan Tivr, oczekuję kontaktu". Klaudia odpowiada jako ta co ma największe doświadczenie z TAI. "Jesteście w stanie anulować Serenit?". Kalira pyta o żądania Klaudii. Da wszystko, tylko prosi - nie wzywaj Serenita. Tivr jest szybki i ucieknie. Kalira potwierdza, że statek koloidowy istnieje (czyli to operacja KAPERSKA). Kalira powiedziała, że nie ma lokalizacji. Boi się. Powie koloidowemu statkowi, żeby ten uciekał przed Serenitem.

Kalira przekonuje koloidowy statek przesyłając odczyty itp.

TrZ (odczyty) +2:

* V: Statek koloidowy się oddali nie atakując Tivra.
* Vz: Statek koloidowy uwierzył, że Serenit to tajna broń Orbitera (lub Eustachego) i to jest rewelacja która jest warta nie ujawnienia się

Kalira wróciła do poinformowania Tivra jak wygląda sitrep. Poddaje się. "Magowie Aurum należą do najbardziej kompetentnych magów w galaktyce" - to jest zdanie którego nikt nie słyszał. Kalira d'Trismegistos przyznała, że to nie jest pierwsza grupa magów którą porwali a miragent należał do tien Gwozdnik.

Magowie zostali dostosowani w komorach adaptacyjnych (kralotyczne biogeny). Trismegistos chce ochronić swojego kapitana i pierwszą oficer; jeśli nie straci załogi to odda nawet komorę. A nawet dwie XD.

Klaudia zażądała materiału dowodowego - obu komór, wszystkich nagrań. Wszyscy arystokraci i pasażerowie. Trismegistos ma sam swoją załogę. Acz Arianna zażądała kilku członków załogi i dostała kilku "rekrutów". Trismegistos nie da takich osób, bo można by namierzyć lokalizację. Aha, i żąda od TAI - chce ich kompozycji materiałowej bo będzie udawać, że Trismegistos został zniszczony.

Roland bardzo nalega na to, by Kalirę zniszczyć. Żywa TAI jest śmiertelnie niebezpieczna. Plus, Saitaer. Plus, wytyczne.

Trismegistos się wygadał. Kalira się wygadała. Enklawa, dystrykt z którym współpracuje - to jest sojusz TAI, drakolitów i faerili. Ale mają też noktian. Klaudia próbuje pokazać Kalirze, że Trismegistos też robił z nich niewolników acz nieświadomych tej niewoli. Klaudia chce jej NIE zniszczyć i nie zrobić jej bomby logicznej, ale by Kalira nie porywała i nie krzywdziła ludzi.

TrZ (nauki Talii) +3:

* V: Kalira NIE będzie porywać ludzi
* V: Kalira nie wpadnie w pętle logiczną i nie ulegnie samozniszczeniu
* V: Kalira będzie próbować przekonać innych w enklawie, żeby trzymali się tych samych zasad

Arianna decyduje się puścić Trismegistos wolno - są nadzieją, bo różne frakcje się wspierają zamiast zwalczać na siłę.

Klaudia fabrykuje na Tivrze fajerwerki - zniszczyli Trismegistos. Wszystkie dane z czujników będą poprawne itp. Też przed inspektorami Orbitera.

ExZ+4:

* V: udało jej się. Ten jeden raz Klaudii się wreszcie udało. Oficjalnie Tivr zniszczył Trismegistos.

Jak tylko Tivr wrócił na K1, Klaudia została aresztowana. Zeszła z pokładu z uśmiechem. Wiedziała wszystko. Sama poszła do żandarmów.

Izabela robi co może by przedstawić ostatnią sesję w taki sposób by wskazać zwłaszcza jak Klaudia i jej działania uratowały arystokratów ORAZ doprowadziły do zniszczenia Trismegistosa. Bo Klaudia jest oskarżona o hackowanie Rdzenia Sprzężonego na K1. Więc Iza robi wszystko co może, by pokazać że Klaudia jest dzielną, wierną orbiterówką i niszczy podłe TAI.

ExZ+3:

* X: Iza wzięła na siebie, że to ona wrobiła w to Klaudię. Ona może być tą "miłośniczką TAI która nienawidzi Eterni".
* Xz: z uwagi na temat, jeszcze większe napięcie "oni są wśród nas" (wolne TAI, niebezpieczne byty). I powiązanie z Saitaerem.
* V: To bardzo przechyliło proces Klaudii - bo fakty zniszczyły działania prokuratora. Czyli jak to tak.

Eustachy napisał skargę, że jakieś lamusy latały Infernią. I prosto do Kramera. Kramer przeczytał, uśmiechnął się i skasował.

## Streszczenie

Wiadomość od pirata że porwał Trismegistos poderwała Ariannę i Tivr. Historia nie trzymała się kupy. Załoga Inferni (Tivru) zorientowała się, że coś jest nie tak - opowieść o statku kupieckim który pozbył się miragenta brzmiała DZIWNIE. Okazało się, że Trismegistos jest statkiem kaperskim. Gdy Trismegistos zażądał poddania się Tivru Klaudia zrobiła sygnał "lol leci na Was Serenit" i TAI Trismegistosa spanikowała. Wolna TAI, współpracująca z fareilem i drakolitką. Wolny dystrykt. Arianna dała im żyć - ale odzyskała porwanych arystokratów i komory adaptacji.

## Progresja

* Arianna Verlen: ona podobno steruje Serenitem. Może go przyzywać i odpychać na żądanie. Królowa Anomalii Kosmicznych. W plotkę wierzą wszędzie POZA Orbiterem.
* Elena Verlen: zaimponowała noktianom na pokładzie Tivra, że świetnie pilotuje tą jednostkę. Z ogromnym tempem i wydajnością.
* Kalira d'Trismegistos: Kalira uważa, że nie powinna porywać i przekształcać ludzi. Wolność jest w końcu wolnością dla wszystkich. Chce nawrócić na to innych.
* Izabela Zarantel: stworzyła narrację, że to ona wrobiła Klaudię w hackowanie Rdzenia Sprzężonego na K1. Ma reputację "miłośniczki TAI która nienawidzi Eterni". Ma groźnych wrogów.

### Frakcji

* .

## Zasługi

* Arianna Verlen: ukryła swą obecność na Tivrze, neutralizowała Rolanda Sowińskiego i pozwoliła Trismegistosowi odlecieć. Chce, by wszyscy żyli w pokoju a Trismegistos jest nadzieją na lepsze jutro.
* Eustachy Korkoran: udawał kapitana Tivra, opracował kilka planów wejścia na Trismegistos i teatralnie przekonał wszystkich, że Serenit to tajna broń Orbitera (lub jego?).
* Klaudia Stryk: udała przed Kalirą d'Trismegistos, że do niej zbliża się Serenit. Zmusiła TAI do negocjacji a potem ukryła że Tivr nie zniszczył Trismegistosa. Aha, aresztowali ją.
* Izabela Zarantel: zrobiła odcinek Sekretów o Eustachym i Klaudii i BARDZO skupiła się na tym by pokazać Klaudię w jak najlepszej stronie by pomóc w oskarżeniach. Wzięła dyshonor na siebie. To najbardziej pomoże Inferni, Klaudii i Ariannie.
* Leona Astrienko: słaba i ledwo aktywna, ale weszła na pokład Trismegistosa jako osłona. Dała się "pokonać" magom, ale czekała aż może zaatakować i naprawić sytuację. 
* Elena Verlen: Arianna jej zaufała, by ta pilotowała Tivr. I faktycznie, najszybszy pilot. Plus, spacer kosmiczny w Eidolonie pozwolił jej podpiąć Klaudię do Trismegistosa. 
* Maria Naavas: zbadała martwego miragenta i wykryła, że to miragent. Potem zajęła się wszystkimi przejętymi arystokratami Aurum - ci do stazy, ci na potem.
* Roland Sowiński: goguś i dandys, który jednak dziwnie od razu zorientował się, że mają do czynienia z wolną TAI (Kalira). Prosił Ariannę, by ta koniecznie zniszczyła Kalirę.
* Zygfryd Maus: Rycerz Karradraela na Orbiterze. Honorowy, niebezpieczny ale pragmatyczny. Mało mówi, chodzi z mieczem. Ma poczucie humoru; straszył Rolanda Sowińskiego. Umie się wpisać w strukturę okrętu takiego jak Tivr; pełnił rolę obserwatora z ramienia mniejszych rodów na Tivrze (odnośnie ratowania arystokratów Aurum z Trismegistosa)
* Kalira d'Trismegistos: wolna TAI; gdy Klaudia zafałszowała jej sygnały że leci tu Serenit to odcięła kapitana i przejęła negocjacje. Dogadała się z Arianną i Klaudią i oddała magów Aurum. Dla odmiany, Kalira mówi prawdę. Dzięki temu Trismegistos odleciał bezpiecznie.
* Klara Gwozdnik: właścicielka miragenta. Pojechała z ekipą na wycieczkę by odwiedzić Neikatis i skończyła jako corrupted dla Jamona. Uratowana przez Infernię.
* Jamon Korab: kapitan Trismegistosa i fareil. Porwał arystokratów Aurum by doprowadzić ich do Dystryktu Lennet. Miragent Klary Gwozdnik wszedł mu w szkodę, ale działał dobrze... aż Klaudia go sabotowała i jego TAI go odcięła.
* Mira Anastel: silnie zmieniona drakolitka; pierwszy oficer Trismegistosa. Błękitna skóra, żółte oczy, wzmocnione piękno. Dobry humor, niewiele traktuje poważnie. Kaper.
* SC Trismegistos: sterowany przez Wolną TAI Kalirę; statek kaperski Dystryktu Lennet maskujący się jako przewoźnik turystyczny z Astorii na Neikatis. 
* OO Tivr: pierwszy lot Arianny i ograniczonej załogi Inferni, z dwoma obserwatorami na pokładzie - Roland Sowiński i Zygfryd Maus. Pilotaż Eleny pobił rekordy Tivra.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Specjalne

* Kalira d'Trismegistos: @FRAKCJA: Dystrykt Lennet, Wolna TAI, @JEDNOSTKA: Trismegistos
* Jamon Korab: @FRAKCJA: Dystrykt Lennet, Fareil, @JEDNOSTKA: Trismegistos
* Mira Anastel: @FRAKCJA: Dystrykt Lennet, Drakolita, @JEDNOSTKA: Trismegistos
* SC Trismegistos: @FRAKCJA: Dystrykt Lennet, @KLASA: średni statek transportowy

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Neikatis
                1. Dystrykt Lennet: nie wystąpił bezpośrednio, ale tam znajduje się synteza Wolnych TAI, fareilów i drakolitów. I chyba noktian. Tam porywani byli ludzie i magowie przy użyciu Trismegistosa.
 
## Czas

* Opóźnienie: 1
* Dni: 2

## Konflikty

* 1 - Klaudia sprawdza nagrania z tego miejsca + K1, korytarzy itp. Kiedy mniej więcej trafił miragent wyglądający jak Arianna na Tivr. 
    * TrZ+3
    * XzVz: Orbiter stopniowo odcina Klaudii dostępy, ALE miragent pojawił się kilka godzin po tym jak Tivr trafił do Arianny.
* 2 - Reporter "pani komodor! Czy pani spełni skandaliczne wymagania prześladowcy pirata?"
    * TrZ+3
    * XV: Klaudia odcina reportera. +1 naughty Klaudia. ALE go odcięła i Arianna nie ma problemów i trudnych odpowiedzi.
* 3 - Elena pilotuje Tivr by zdążyć do Trismegistosa
    * TrZ+4
    * VV: Elena zdążyła na Trismegistos i zaimponowała solidnie noktianom że pilotuje epicko Tivr.
* 4 - Mira ma przesłać nagrania z tego co tam miało miejsce i jak pokonali miragenta. A dokładniej, tego żąda Klaudia. A Arianna słucha i steruje.
    * TrZ (Klaudia jest nieogarnięta w kłamaniu a Arianna nią steruje) + 4
    * V: Arianna widzi że Mira czegoś nie mówi. Coś kłamie.
    * X: Ale Jamon mówi serio i nie kłamie XD
* 5 - Elena infiltruje Trismegistos z zewnątrz; ku silnikom.
    * TrZ (zaskoczenie + dywersja) +4 +O (serce):
    * VXV: Elena podłożyła ładunki pod silniki, TAI się zorientowała że coś jest nie tak ALE Elena podpięła Klaudię do sensorów Trismegistosa.
* 6 - Klaudia wysyła sygnały do biednej TAI Trismegistosa. Nadlatuje SERENIT.
    * TrZ+3+3Vg
    * VgVzV: Kalira d'Trismegistos w panice, przejmuje dowodzenie i sama negocjuje. Przekonana, że to ORBITER kontroluje Serenit.
* 7 - Kalira przekonuje koloidowy statek przesyłając odczyty itp. żeby statek odpuścił i uciekał bo Serenit.
    * TrZ (odczyty) +2
    * VVz: koloidowiec oddala się nie atakując Tivra + wieść że Serenit to broń Orbitera (Arianny) wymaga rozpowszechnienia.
* 8 - Klaudia chce NIE zniszczyć Kaliry i nie zrobić jej bomby logicznej, ale by Kalira nie porywała i nie krzywdziła ludzi.
    * TrZ (nauki Talii) +3:
    * VVV: Kalira nie porywa ludzi, nie wpadnie w pętlę logiczną i próbuje przekonać innych by też tego nie robili
* 9 - Klaudia fabrykuje na Tivrze fajerwerki - zniszczyli Trismegistos. Wszystkie dane z czujników będą poprawne itp. Też przed inspektorami Orbitera.
    * ExZ+4
    * V: Udało się. Inspektorzy nic nie podejrzewają. Dla świata Trismegistos jest martwy.
* 10 - Izabela robi co może by przedstawić ostatnią sesję tak że Klaudia jest lojalistką Orbitera i niszczy złe TAI.
    * ExZ+3
    * XXzV: Iza wzięła to na siebie i będzie mieć problemy zamiast Klaudii, "oni są wśród nas" (wolne TAI), ale pomogło w procesie Klaudii

## Inne
### Projekt sesji
#### Narzędzia
##### Koncept

.

##### Implementacja

.

#### Przeciwnik

.

#### Sceny

.

#### Wsparcie graficzne (rysunki)

.

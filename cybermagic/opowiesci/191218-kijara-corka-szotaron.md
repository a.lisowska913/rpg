---
layout: cybermagic-konspekt
title: "Kijara, córka Szotaron"
threads: unknown
gm: żółw
players: kitsunegami, vuko, kruko, sebastian_1, domin
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [191211 - Kryzys energii na Szotaron](191211-kryzys-energii-na-szotaron)

### Chronologiczna

* [191211 - Kryzys energii na Szotaron](191211-kryzys-energii-na-szotaron)

## Uczucie sesji

* "Jak to pokonać? Co możemy zrobić?"
* "Co oni zrobili, ci przed nami?!"

## Punkt zerowy

.

### Postacie

.

## Misja właściwa

### Scena zero

Jako Persefona d'Szotaron (od teraz: Kijara) zaatakowali statek kosmiczny. Zamiast go niszczyć, Kijara zasymilowała go i skazała załogę na wieczne cierpienie, wzmacniając moc samej Kijary. Kijara skierowała się ku Dorszant; tam jest energia i życie. No i skanery, dzięki którym Kijara znajdzie Azonię - prawdziwy cel który może dać jej energię i potęgę.

### Sesja właściwa

Zespół dostał informację, że coś jest nie tak. Nie tylko górnik zaginął w asteroidach (już kolejny); dodatkowo, rodzina jednego z górników zamknęła się w jednym pokoju i nie dają znaku życia. Coś jest dziwnego. Przejrzeli to w kamerach - matka zabiła dzieci i siedzi tam zamknięta, opętana boleścią i głodem. Wtf. Dokładniejsze badania pokazały, że to wpływ Esuriit - wieczny głód. Ale skąd Esuriit na Dorszant? Normalnie Eszara by to wykryła, ale Hestie nie mają mocy. Najpewniej przyleciało to z kosmosu, przeszło nanitkami. Czyli wektorem byli górnicy. Czyli mamy Esuriit w pasie asteroidów. To bardzo, bardzo źle.

Inżynier Zniszczenia ślicznie poprosił Hestię o wsparcie. Uruchomili syntezatory i Inżynier zbudował bardzo potężną bombę mogącą osłabić i odepchnąć Esuriit. Inspirator zaczął działać - dezinformacja i redukcja paniki na stacji plus znalezienie innych nieszczęśliwych. By tylko Esuriit się nie rozprzestrzeniło.

Zespół stanął przed dość trudną sytuacją. Cóż, trzeba lecieć po rodzinę Filipa. To oni wysłali tą rodzinę na Szotaron; przy okazji sprawdzą, co się stało na tamtej stacji. Czy wszystko na Szotaron w porządku. Cóż pozostaje, polecieli. A dokładniej - As wraz z Szamanem. Wzięli skrzydłowych i polecieli dowiedzieć się o co chodzi i jak wygląda sytuacja.

W kosmosie dotarli po trajektorii tam, gdzie znajduje się pojazd lecący do Szotaron. As wykrył coś bardzo, bardzo niebezpiecznego - Kijara ("nieznany, nanitkowy pojazd") poluje na statek z rodziną Filipa. Skrzydłowi polecieli skupić uwagę Kijary; As poleciał ratować rodzinę. Poszło FATALNIE - Kijara jest upiornie niebezpieczna:

* tylko rodzina jest uratowana, strażnicy zostali na pokładzie
* Kijara unieszkodliwiła wszystkich skrzydłowych, zostawiła ich jako pokarm na potem
* pojazd Asa został uszkodzony; Kijara ma dramatyczną przewagę

W świetle tego wszystkiego, Szaman się skupił. Nie znalazł Szotaron; NIE MA STACJI. Została zniszczona. Połączył się z umysłem Kijary i to wstrząsnęło Szamanem; zrozumiał, z czym ma do czynienia. Szaman wydał rozkaz śmierci wszystkim żywym dookoła, niech Kijara nie ma czym się żywić. Przy okazji sam użył energii Esuriit... robi się coraz lepiej. A jeszcze lepsze było poczucie obecności Azonii, na którą poluje Kijara.

As zdecydował się na przechwycenie Azonii - do tego celu użył bomby anty-Esuriit (która była nadspodziewanie skuteczna na Kijarę), i w locie zgarnął kapsułę Azonii, spisując sporo ludzi na straty. Z trudem doczołgał się statkiem do Dorszant. Kijara okazała się strasznym przeciwnikiem.

Na samej stacji mafia uznała, że mają do czynienia z czymś przekraczającym czyjekolwiek możliwości. Obudzili Eszarę. Przekazali jej, że Persefona stała się Kijarą i implikacje - ona, Eszara, też może stać się potworem. W świetle takich argumentów, Eszara zdecydowała się tymczasowo pomóc. Zawarła pokój z mafią w zamian za ściągnięcie _skeleton crew_ Orbitera i oddania jej fragmentu stacji. Za to Eszara nie będzie polować na Zespół. Umowa stoi.

Wspomagani siłą Eszary, nie mieli kłopotu z użyciem biolab do leczenia i detoksyfikacji zarówno Szamana jak i Azonii.

Jak zniszczyć Kijarę? Ano, nie do końca mają jak. Ale mają pewien plan jak się jej stąd pozbyć i uratować jak najwięcej dusz z Szotaron:

* Kijara jest istotą Esuriit. Jest inteligentnym drapieżnikiem nanitkowym, ale głód wygrywa. Dla niej Azonia jest super smaczna - jest przynęta.
* Majster jest w stanie zmodować inny statek dla Asa - niech As wleci przez Bramę i wróci (z Azonią). Pułapka na Kijarę; ta bez Mapy nie ma jak wrócić.

I to zrobili - masowy neuronull na stacji sprawił, że ludzie nie byli dla Kijary atrakcyjni. "Usmacznienie" Azonii sprawiło, że Kijara poleciała prosto za Asem. Sam As wiedział już czego się spodziewać. Tylko... mapa nie prowadziła donikąd a do innego sektora zamieszkanego przez ludzi.

Oops.

Grunt, że Kijara nie ma już jak wrócić. Wszyscy stąd będą bezpieczni.

**Sprawdzenie Torów** ():

* .

**Epilog**:

* .

## Streszczenie

Persefona d'Szotaron uległa zmianie w potwora Esuriit i pożerała ofiary z Szotaron, szukając Azonii. Magowie Dorszant wykryli ślad Esuriit i uratowali rodzinę Filipa, docelowo pozbywając się Kijary (eks-Persefony) z Noviter; przerzucili ją przez Bramę _gdzieś_. Wynegocjowali pokój z Eszarą d'Dorszant, acz oddali jej część kontroli nad stacją.

## Progresja

* .

### Frakcji

* .

## Zasługi

* Kijara d'Esuriit: kiedyś Persefona d'Szotaron; straszliwy nanitkowy potwór Esuriit asymilujący wiedzę i energię swoich ofiar. Przerzucona przez Bramę... _gdzieś_.
* Rafał Kirlat: inżynier zniszczenia Dorszant; opracował genialny sposób odepchnięcia Kijary bombą, dzięki czemu udało się Oliwierowi uratować Azonię.
* Oliwier Pszteng: as pilotażu Dorszant; poświęcił kilku skrzydłowych by tylko ratować rodzinę Filipa. Starł się z Kijarą - i przegrał, acz uleciał z życiem.
* Szymon Szelmer: szaman Dorszant; ten łagodny z natury mag skaził się Esuriit by tylko rzucić _death spell_ na wszystkich których w kosmosie dorwałaby Kijara.
* Jaromir Uczkram: majster Dorszant; przejął dowodzenie nad operacją i opracował plan jak pozbyć się Kijary - przerzucić ją przez portal _gdzieś_.
* Felicja Taranit: inspirator Dorszant; rozpuściła pogłoski o tym, że wszystko jest pod kontrolą i przenegocjowała pokój z Eszarą.
* Filip Szczatken: po tym, jak mafia uratowała jego rodzinę przed Esuriit, własnymi i ciężkimi stratami, został im lojalny i oddał dowodzenie.
* Eszara d'Dorszant: zaakceptowała mafię jako głównodowodzących na Dorszant, ale zbudowała własną enklawę i ściągnęła orbiterowców.
* Azonia Arris: uratowana przez Dorszantowców i wykorzystana jako przynęta na Kijarę; przetrwała, acz z pewnym uszczerbkiem psychicznym.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Noviter
            1. Pas Sowińskiego: miejsce starcia między Kijarą polującą na rozbitków Szotaron a mafią Dorszant, próbującą uratować jak najwięcej ludzi.
                1. Stacja Dorszant: pod wpływem zagrożenia ze strony Esuriit i, faktycznie, kompetencji mafii, doszło do pokoju między Eszarą i mafią.
                1. Anomalia Dorszant: na ową anomalię polowała Kijara swoimi nanitkami; szczęśliwie, udało się jej pozbyć nim do czegoś doszło.
            1. Brama: gdzie as pilotażu wykonał najbardziej epicki manewr - zawrócił w portalu, by Kijara nie miała jak wrócić i musiała lecieć dalej gdzieś.

## Czas

* Opóźnienie: 2
* Dni: 3

## Inne

### Co warto zapamiętać

* As Pilotażu, zestresowana, chowająca się za skrzydłowymi by za wszelką cenę ratować rodzinę Filipa
* Szaman, zawsze stabilizująca sytuację - tu asymilująca energię Esuriit by zabić wiernych skrzydłowych i strażników nim Kijara ich dorwie
* Majster, kombinujący jak nanitkami leczyć Szamana z Esuriit
* cały zespół kombinujący nie jak ZNISZCZYĆ Kijarę a jak ją ODEPCHNĄĆ

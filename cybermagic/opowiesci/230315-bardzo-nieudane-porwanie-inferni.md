---
layout: cybermagic-konspekt
title: "Bardzo nieudane porwanie Inferni"
threads: historia-eustachego, arkologia-nativis, infernia-jej-imieniem
gm: żółw
players: fox, kapsel
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [230215 - Terrorystka w Ambasadorce](230215-terrorystka-w-ambasadorce)

### Chronologiczna

* [230215 - Terrorystka w Ambasadorce](230215-terrorystka-w-ambasadorce)

## Plan sesji
### Theme & Vision

* Próbują porwać Infernię, bo to jest główna siła Nativis
    * "Kiridon zostanie zniszczony"
    * "Wcale nie jesteście tacy fajni, wy Korkorani, bawicie się w bycie dobrymi..."
    * "Te wszystkie arkologie eksploatowane przez Kidirona"
* Ralf i Ardilla są schowani na Inferni
* Eustachy i ekipa pomagają rannym na których polują Trianai
    * To pułapka; oni są uzbrojeni i walczą

### Co się stanie (what will happen)

* FAZA 1: Ratujemy CES Mineralis (tak, naprawdę tak to nazwali)
    * S1: Ratunkowy sygnał, ratujemy CES Parlis przed Trianai
        * nie ma żadnych elitarnych jednostek trianai; są ainshkery i serteriaty (long-limbed vines)
        * na miejscu jest koło 20-30 osób, celują też do Inferni artylerią
* FAZA 2: Ratujemy Infernię
    * S2: 
        * Infernia i Ralf na Inferni, Eustachy i siły Korkoranów w niewoli
            * Ralf: telepatyczne połączenie z Eustachym, jak to stealthować
        * Próby selekcji ludzi 
* FAZA 3: Jej imię - Infernia
    * S3: Stall for time (Eustachy), Save yourself (Ardilla, Ralf)


### Sukces graczy (when you win)

* Rozwiązanie problemu terroryzmu

## Sesja właściwa

### Fiszki

#### 1. Infernia

* Bartłomiej Korkoran: wuj i twarda łapka rządząca Infernią (WG: "System służy ludziom") | faeril: "Infernia służy Korkoranom jako awatar Bezimiennej Pani."
    * (ENCAO: +-000 |Bezkompromisowy, nieustępliwy;;Ciekawski|Family, Benevolence, Self-direction > Achievement, Tradition, Humility| DRIVE: Inkwizytor: ujawnianie bolesnych prawd)
    * "Nasza Infernia ma za zadanie dać nam wolność oraz pomagać innym!"
* Emilia d'Infernia
    * "żona" Bartłomieja. Żywy arsenał. Egzekutor Bartłomieja. (Emilia -> "Egzekucja MILItarnA")
* Ardilla Korkoran: faerilka działająca jak drakolitka i przyszywana kuzynka Eustachego  <-- często Fox
    * badacz/odkrywca/scrapper. Zmodyfikowana tak, by łatwiej wchodzić w niedostępne miejsca, bardziej zwinność + orientacja w terenie + pewne umiejętności badawcze.
* Celina Lertys: drakolitka z Aspirii podkochująca się w Eustachym  <-- często Kić
    * (ENCAO: --+-0 |Stanowcza, skryta;;O wielu maskach| Universalism, Tradition, Security > Power, Humility| DRIVE: Harmonia naturalna między wszystkim)
    * złotoskóra o błękitnych elektrycznych włosach podkochująca się w Eustachym (science / bio officer + medical); augmentowana na widzenie rzeczy ukrytych; 19 lat
    * "Jesteśmy częścią Neikatis i podlegamy procesom Neikatis. Ale nadal mam zamiar zrobić wszystko by uratować swoich przyjaciół."
* Jan Lertys: drakolita z Aspirii
    * (ENCAO: -0-0+ |Zapominalski, obserwator, łatwo nań wywrzeć wrażenie | Humility, Universalism > Self-direction, Achievement | DRIVE: Pokój: Kapuleti i Monteki MUSZĄ żyć w pokoju, tylko nie Celina x Eustachy)
    * "Ciężką pracą dojdziemy do tego, że wszystko będzie działać jak powinno. Ja nie wtrącam się do działania Bartłomieja Korkorana; on wie lepiej."
* Ralf Tapszecz: mag (domena: ROZPACZ + KINEZA) i nowy dodatek do Inferni, kultysta Interis. Noktianin, savaranin.
    * (ENCAO:  -00-+ |Bezbarwny, przezroczysty;;Buja w obłokach| VALS: Conformity, Humility >> Benevolence| DRIVE: Piękno zniszczenia)
    * "Nasze porażki nie mają znaczenia. Tak czy inaczej zwyciężymy."
* Tymon Korkoran: egzaltowany lekkoduch i już nie następca Bartłomieja
    * (ENCAO:  +--00 |Co chce, weźmie;;Niecierpliwy;;Egzaltowany | Stimulation, Tradition > Face, Humility | TAK: Być lepszym od Bartłomieja w oczach Kidirona)
    * "Infernia jest moja i należy do mnie. Bartłomiej marnuje jej potencjał, a dzięki niej Nativis może rządzić Neikatis!"
* Mariusz Dobrowąs: oficer wojskowy Inferni
    * (ENCAO:  0-0+- |Nudny i przewidywalny;;Rodzinny| VALS: Humility, Hedonism >> Stimulation | DRIVE: Supremacja Nativis i Inferni)
    * "Pomagamy SWOIM, inni nie mają znaczenia. Pisałem się na pomoc swoim."

#### 2. Terroryzm

* Wojciech Grzebawron: chce przejąć Infernię w imię innych arkologii
    * ENCAO:  -000+ |Zdystansowany, powściągliwy i zamknięty w sobie;;Nie ogranicza się do doktryn czy dogmatów| VALS: Power, Face >> Benevolence| DRIVE: Komfortowe życie
    * styl: The Negotiator

#### 3. Arkologia Nativis

* Rafał Kidiron
    * (ENCAO: 00+-0 |precyzyjny i zaplanowany;; DIMIR (UB);; Żywy wąż nie człowiek | Achievement, Power, Face | DRIVE: Duma i monument)
    * security lord i nieformalny dyktator arkologii
    * "Nasza arkologia zaszła tak daleko i nie możemy pozwolić, by cokolwiek stanęło na drodzę jej absolutnej wielkości."
    * "Tylko ci, którzy są przydatni mają miejsce w Nativis"
* Lycoris Kidiron: 57 lat (ze wspomaganiami drakolickimi dającymi jej efektywność 3x-latki)
    * (ENCAO: +0+-0 | Proaktywna;; Pierwsza w działaniu | Security, Achievement > Conformism | DRIVE: Kapitan Ahab (perfekcyjna arkologia))
    * ekspert od bioinżynierii i terraformacji, pionier (zdolna do przeżycia w trudnym terenie)
    * "Nikt, kto próbował się dopasować do innych nie doprowadził do postępu. Dzięki mnie Nativis będzie bezpieczna."
* Laurencjusz Kidiron
    * (ENCAO: +0--- |Żyje chwilą;;Nudny| Face, Power, Hedonism > Achievement | DRIVE: Przejąć władzę nad arkologią)
    * "Ta arkologia musi należeć do mnie. Ile może jeszcze być ograniczana przez Rafała? On nie jest tylko 'szefem ochrony'..."
* Kalia Awiter: influencerka z Nativis. Aktywnie próbuje pójść do łóżka z Eustachym i go podbić. Wierzy w Infernię. 21 lat. Drakolitka.
    * (ENCAO:  +--00 |Stoicka;;Radosna, cieszy się i daje radość;;Dzikie i spontaniczne pomysły| VALS: Universalism, Stimulation, Conformity | DRIVE: Zatrzymać czas, zrobić coś dobrego)
    * "Nativis musi być centrum kultury Neikatis. Musimy dać COŚ dlaczego warto żyć. Coś więcej niż tylko przetrwanie!"
* Marcel Draglin: oficer Kidirona dowodzący Czarnymi Hełmami
    * (ENCAO: --+00 |Bezbarwny, bez osobowości;;Bezkompromisowy;;Skupiony na wyglądzie| VALS: Family, Achievement >> Hedonism, Humility| DRIVE: Lokalny społecznik)
    * "Nativis będzie bezpieczna. A jedynym bezpieczeństwem jest Kidiron."

### Scena Zero - impl

.

### Sesja Właściwa - impl

Minęły 10 dni. Wujek dalej w szpitalu. Eustachy zrobił 2-3 małe operacje Infernią i ogólnie działa. Na pokładzie często jest Ardilla (wujek chciał by tam była) oraz wszechobecny Ralf. Wujek ma NAJLEPSZĄ opiekę medyczną. Ale Celina chce być z wujkiem. 24/7. Bo nie ufa Kidironowi.

CES Mineralis. SOS. Atak Trianai. Potrzebna pomoc. "Szare Ostrze" Kidirona jest zajęte, więc zostaje Infernia. Nie zostawimy ludzi, lecimy im na pomoc. Kidiron poprosił o wsparcie - niech Eustachy pomoże CES Mineralis. Ale niech nie poniszczy sprzętu. Eustachy poprosił 10 Czarnych Hełmów. Od razu masz protest od Dobrowąsa. Ale Eustachy powiedział, że można ich potraktować jako żywe mięso armatnie i wykrywacze min. Dobrowąs się ucieszył. Nawet będzie udawał że jest miły.

Infernia ma 20-30 minut lotu. Niefortunna pogoda.

Ardilla pełznie po kanałach Inferni by uszkodzić klimatyzację Czarnym Hełmom

Tp (niepełny) Z (Ardilla jest na tym statku) +3:

* X: Eustachy, Infernia rozświetliła pomniejszą awarię. Ardilla coś zepsuła. (lifesupport ogólnie śmierdzi)
* V: Klimatyzacja odłączona, będzie Hełmom ciepło.

"Zaraz do Ciebie wrócę Eustachy" "Dobrze Eustachy już wracam" (majstruję dalej)

Ralf idzie przechwycić Ardillę. 

Eustachy przekierowuje Ardillę do kwatery Hełmów a Hełmy do kwatery Ardilli. KŁÓTNIE między Eustachym i Ardillą o Czarne Hełmy. I śmierdzi w całej Inferni bo Ardilli nie wyszło. Ralf - niech wtajemniczy że są potrzebni. Ralf ma szok kulturowy - savaranin a oni się kłócą. Ralf zachowuje dobrą minę do złej gry.

Eustachy -> Nadia (z bazy):

* Nadia: Dzięki Seilii, jesteście (ulga). Walczymy z tym cholerstwem, zabarykadowaliśmy się. Są ranni. Pomóżcie szybko. Proszę.
* Ardilla: Lokalizacja ludzi?
* Nadia: (to co wie o lokalizacji ludzi, łatwiej się przebić)
* Nadia: bardzo uważajcie na siebie
* Ardilla: jakiś wyższy?
* Nadia: nie widzę wyższych, ale jest koordynacja. Coś tu musi być.
* Ardilla: gdzie to się zaczęło?
* Nadia: nie wiem, pomóżcie, proszę

Ardilla jej tłumaczy, że są ze wsparciem i żeby ją uspokoić i żeby współpracowała. Niech udzieli informacji gdzie była, skąd przychodziły -> pokonać Trianai i jednostkę kontrolną

Tr (niepełny) Z (reputacja Inferni) +3:

* Vr: Nadia tłumaczy skąd przyszły. I dla Ardilli i dla Eustachego nie ma to sensu. Nie może być samej grupy lekkich Trianai drugiego etapu.
* Vr: Nadia jest bardziej przerażona niż powinna. Próbuje być składna, ale... 
    * z natury jest moralna i AKTYWNIE próbuje nie wierzyć w zło Kidirona, bo musiałaby rezygnować
    * jest z natury bez polotu i bez 'iskry bożej', więc to przerażenie jest... nietypowe

Eustachy ląduje Infernię by wysłać Czarne Hełmy. Tymczasowy XO Hełmów "idziemy tam sami?" Eustachy "sprawdzamy, czy jest tam niebezpieczniej". Eustachy wysyła 10 komandosów z Inferni i 10 Hełmów. Hełmy ściągają Trianai a Infernianie otwierają ogień.

Tr Z (masz rozplanowane) +3 +3Or (Infernianie) +3Oy (Hełmy):

* X: plan jest dobry. Naprawdę. Trianai jednak mają jeszcze jedną bioformę. Z piasków ostre pnącza zaatakowały na daleki zasięg. 
* Oy: Infernianie są pancerniejsi, ale są ranni wśród Hełmów.
* X: trzeba użyć wsparcia. Nie tylko Infernia otworzyła ogień do piasków ale też resztę komandosów trzeba było rzucić. Są ranni wśród komandosów.
* V: przebiliśmy się do CES. Trianai rozgonione.

10 Hełmów: 4 jest sprawnych. Z 20 komandosów: 16 jest sprawnych.
Ralf i Ardilla zostają na pokładzie Inferni. Eustachy idzie pomóc.

W środku CES są ślady walki. Jest krew. Są ranni ludzie i zabici. Są pozabijane Trianai. Brakuje jednostki kontrolnej, dalej. Są ranni na ziemi - to nie jest typowe dla Trianai. Część komandosów chroni medyków i skan rannych, a Eustachy i komandosi idą w głąb. Na pokładzie zostało 5 komandosów.

Eustachy szybko poszedł z komandosami do sali konferencyjnej. Komandosi to minują. Eustachy otwiera energię magiczną by wabić Trianai.

Tr (wabienie, nęcenie) M +3 +3Ob +3Or:

* Ob: kanał magiczny jest... niestabilny, ENERGIA jest tu niestabilna, coś jest nie tak, NIE TAK. Połączyłeś się z koordynatorem Trianai.
    * X: straciłeś przytomność.
* Vr: ściąga na siebie Trianai i komandosi mogą strzelać
* Xm: ERUPCJA I EKSPLOZJE. CES się rozpada. Ogromne ilości rannych. Koordynator Trianai nie żyje.
    * Coś zastawiło pułapkę na szczura a złapało niedźwiedzia.
    * Nadia KIA.

CES gaśnie. Ardilla koordynuje operację ratowania. Komandosi po skuterki. Trianai są rozbite i wieją.

Ardilla koordynuje operację ratunkową całą załogą Inferni i środków:

Tr Z (wszystkie siły Inferni - trans/med) +3 (by użyć wiedzy ludzi):

* V: koordynacja trwa i działa dobrze.
* V: manewr (+2Vg)
* X: PRZEJMUJEMY INFERNIĘ. Wrogowie w akcji. 
    * PEŁNE ZAGŁUSZANIE. Infernia odcięta.
    * Część osób z Inferni przestaje się komunikować.
    * Część rannych z pokładu Inferni jest uzbrojona i atakuje mostek.

Ardilla wyłącza Infernię z mostka.

* V: Infernia wyłączona z poziomu mostka (zablokowana). Jedynie Korkoran może ją obudzić.
* X: Dwie osoby z bronią wpadają na mostek (+5Vv)
* Vv: Ralf COŚ zrobił. Wyglądało jakby wyrwał im oczy. I uciekliście do kanałów Inferni...

...

Eustachy obudził się. Dostałeś jakiś środek (wstrzyknęli). Plus pomyje w twarz. Na krześle, związany itp. Dwóch facetów.

* Szymon: "To ty jesteś Eustachy?"
* Eustachy: A kto pyta
* Szymon: (plaskacz, mocno w twarz). "Ja zadaję pytania". /jemu się to podoba
* Eustachy: (powiedział prawdę o tym kim jest i że zabił maga)
* Szymon: (chce mu wpieprzyć)
* Wojtek: (zostaw go, mówi prawdę) /szok
* Eustachy: możecie wziąć Infernię, ale nie polatacie
* Wojtek: ...mówi prawdę...
* Eustachy: (do Inferni, żeby powiedziała Ardilli i Ralfowi) i mówi 'jestem jedynym kapitanem Inferni i statek nic beze mnie nie zrobi'
* Szymon: Czyli jeśli Ty wyrazisz zgodę i oddasz nam statek nic nie zadziała

Eustachy: "naszprycowali mnie i zrobiło się coś, wyłączcie generatory Memoriam". Ralf i Ardilla patrzą na siebie. Infernia ich połączyła XD.

* Ardilla: "Co się stanie jak wyłączymy generatory memoriam?"
* Eustachy:"Część planu, nie wiem na co możemy się zgodzić"
* Ardilla: "Jak mówisz, że to pomoże..."

Ardilla się przekrada z Ralfem by pozyskać części inżynieryjne. Ralf robi mikrodywersje.

Tr Z (zna Infernię) +3 (Ralf):

* V: Ardilla pozyskała części do niszczenia generatorów Memoriam
* X: druga strona MNIEJ WIĘCEJ wie że Ardilla i Ralf poruszają się w zakamarkach. Nie wiedząc gdzie są.

(S -> E: "Gdzie na Inferni łazi często Ardilla? E: po kanałach serwisowych" S: "daj mi mapę Inferni" E: "no jak jest ten główny korytarz..." S: "DEBILU DAJ MI WSZYSTKIE MAPY INFERNI JAKIE MASZ" E: "nie mam żadnej mapy" S: "który palec.?")

W końcu Eustachy powiedział że nie mają kontroli i brandowali go świnką na pośladku. "TERAZ DRUGI BY BYŁO SYMETRYCZNIE!" i zaczyna się wiercić by dać drugi pośladek. Wypalane. Jest szok dla porywaczy.

* R -> A: "Wypalają mu świnkę na pośladku."

Ardilla skupia się na generatorach. W pośpiechu. Ona i Ralf osobno.

Tr Z +3 +3Ov:

* Ov: Ralf użył mocy. Ardilla usłyszała paniczny krzyk, wrzaski a po chwili pięć wystrzałów. Patrol zniszczony.
* V: Ardilla zniszczyła serię generatorów.
* Vz: Ardilla i Ralf zniszczyli DOŚĆ generatorów by Infernia mogła działać i była niekontrolowalna.
* V: Ani Ardilli ani Ralfowi nic się nie stało
* Vr: Ardilla niesamowicie szybko zadziałała z Ralfem - poniszczyli generatory mniej więcej gdy się Eustachego wypala.

Ardilla wracając do punktu zbornego znalazła pięciu napastników którzy popełniło samobójstwo. Zastrzelili się sami. "Infernia pomaga" (spoiler: Ralf). Spotkała się z Ralfem bez problemu.

Infernia się obudziła z rozkazu Eustachego. "Pobawiliśmy się, zapoznaliśmy i pokażę Wam o co chodziło. Co jest 30 metrów w TAMTĄ stronę na linii strzału Inferni?" "Magazyn żywności" "Aha, niepotrzebny". Eustachy chce wyparować tamtą ścianę i "A teraz cały sprzęt wycelowany jest w Was." Test terroru.

Tr Z (Infernia + Eustachy jest DZIWNY) +4 +3Or (3 krzyżyki są czerwone):

* Xr: koleś-obcęgi spanikował. ZASTRZELĘ CIĘ JAK JEGO!!! (i zastrzelił żołnierza Eustachego)
    * Eustachy:Infernię trzymam w ryzach, przytępione zmysły, WSZYSTKO zniszczy
* (+3Og): Og: ten od wariografu zastrzelił tego od obcęg
* V: APOKALIPTYCZNA destrukcja ze strony Inferni
    * próbowali wyłączyć z mostka i zostali _wchłonięci_ przez Infernię. Ardilla i Ralf podpełzli pod działający generator Memoriam.
    * Ralf "ON tam jest"
* Xz: ogólnie, gorąca strzelanina wszystkich ze wszystkimi
* V: Eustachy okiełznał Infernię. Przelotne uczucie 'znudzenia'.

Ardilla próbuje uratować jak najwięcej ludzi na pokładzie "CHOWAJCIE SIĘ POD MEMORIAM"!

Tr Z (bo Ardilla wie co mówi i jak mówić + ma reputację u swoich i u wrogów też) +3:

* VVX: większość osób z załogi Infernii uratowanych i część Hełmów i część tych, którzy napadli Infernię. Na pokładzie nikt nie chce walczyć.
* X: Ardilla się nie zorientowała w czymś dziwnym.

Wrogowie są rozbici i nie mają dowodzenia. 

* Eustachy do Wojtka: "Zadarliście z Pożeraczem, Wiecznym Końcem czy Infernią. W ramach pokuty trzy zdrowaśki nie wystarczą."
* Eustachy: "Zabiłeś go szybko i bezboleśnie co jest problemem bo wypaliliście mi świniaczki na tyłku. Pozwoliłeś na to."
* Wojtek: "Chcesz mi wypalić świnkę na tyłku? (przerażenie)"
* Eustachy: "Cierpienie jakiego zaznasz jest niewyobrażalne ale nie doznasz go przez bardzo długi okres czasu, może godzinę albo dwie"
    * Eustachy poczuł zainteresowanie Inferni
* Wojtek: Poddamy się! Każę moim ludziom się poddać!
* Eustachy: Jak dla mnie ok, ale Infernia nie jest do końca przekonana, ona jest entropią.
* Wojtek: Jesteś... końcówką Inferni?
* Eustachy: Nie, Infernia jest... nie jest w pełni autonomiczna ale jest świadoma w swój pokrętny sposób
* Wojtek: Dołącz nas do załogi!
* Eustachy: Skończylibyście jako amunicja czy zasilanie. Zasoby ludzkie.
* Wojtek: Czyli nie ma nic co można zrobić?
* Eustachy: Porwaliście się z motyką na słońce, nie wiedzieliście co was czeka, przyćpaliście mnie... a tak naprawdę to ja jestem kłódką trzymającą potwora w ryzach. A wy ją zerwaliście
* Wojtek: Jeśli przeżyję, nikt się nie ODWAŻY wykroczyć przeciwko Inferni...
* Wojtek: Nie bądź jak Kidiron, Twój wujek nie dałby Was zabić Inferni
* Eustachy: Ona Was nie zabije...

Eustachy wraca na statek. Ale jak ktoś choć POMYŚLI by podnieść rękę na Infernię lub Korkoranów, niech pamięta co się stało tutaj.

Eustachy: załoga na stanowiska, ścierwo do luku. Ktoś warty zwerbowania? Jedna osoba: "Panie kapitanie, ja jestem z CES Mineralis, z tych oryginalnych, możemy... uratować kogo się da z mojej załogi?"
Eustachy uważa, że to dobre punkty do reputacji. Oprychy są przerażeni. Ardilla - po raz kolejny - przeprowadza operację logistyczno-ratunkową. Ale teraz już nikt jej nie przeszkadza. A jak JEDEN ucieknie z oprychów to wszyscy zginą.

I co dziwo, nie ma ANI JEDNEGO TRIANAI. Wszystko uciekło.

Eustachy - test na terror

Tr Z (całokształt) +3:

* Vr: jako ekipa wrócili
* Xz: dwóch próbowało uciec ale zostało rozstrzelanych przez "swoich"
* X: jeden faktycznie uciekł

.

* Eustachy: "masz 15 minut albo tylko ci na Inferni przeżyją"
* uciekinier Hubert: "Lepsza śmierć niż Kidiron!"
* Eustachy: "kto powiedział że Cię oddam Kidironowi?" (Kidiron to akt łaski, wrzucę Was pod wujka i będziecie kompetentni)
* Hubert: "...chcesz nas... przeszkolić?"
* Eustachy: (o łowieniu ryb i dobrej reputacji.)
* Ralf: "Dobra alokacja zasobów. Dobra przestraszona załoga."

.

* X: Do Eustachego zaczynają się zgłaszać osoby które CHCĄ DOŁĄCZYĆ z tych co nie wziąłeś. 37 osób. WSZYSCY piraci. Poza Hubertem.

Hubert jest jedynym nieprzekonanym. Będzie pielgrzymem "nie zaczepiajcie Inferni to demon." A reszta dołączy do Inferni i jak to powie kuzyn Tymon "mamy swoje świnkowe hełmy". Za co dostanie w pysk od Eustachego.


NA MIEJSCU, Kidiron. Jak zawsze, nieskazitelny strój.

* Kidiron: "Eustachy, poradziłeś sobie świetnie. Lepiej niż Twój wujek" /pod wrażeniem
* Eustachy: "Miło"
* Kidiron: "Wiem, że chciałbyś ich zachować na Inferni dla siebie. Czy pozwolisz że dowiem się kto użył maga, Infernię itp?"
* Eustachy: "Mag wyparował, dowódca ma otwory w czaszce a ja im obiecałem że nie mają z Tobą styczności"
* Kidiron: "Nie muszą ze mną rozmawiać. Tobiasz ma... technologię bezbolesną, dzięki której możemy wszystko wiedzieć. A potem Ci ich oddam. Nic im się nie stanie. Nic."
* Eustachy: "Ale tylko ochotnicy"
* Kidiron: "Czy mogę z nimi porozmawiać zanim będą deklarowali ochotników?"
* Eustachy: "Najwyżej Cię oplują i zwyzywają"
* Kidiron: "Przywykłem. Dla dobra arkologii."
* Eustachy: "Ardilla tam będzie."

.

* E->A: "Bądź jako gwarant bezpieczeństwa."
* Ardilla: "Dobrze, Eustachy."

.

Kidiron. Obiecał im _carte blanche_ dla nich i rodzin. I brak immunitetu jak nie będą współpracować. I Kidiron podkreśla "kapitana Eustachego Korkorana". I tak, kupił ich. Czystym terrorem...

Co gorsza, nowe generatory Memoriam się nie montują. Palą się. Tylko te które zostały działają. Infernia wykrywa próbę montażu nowych, ale nie zauważa starych.

## Streszczenie

Mimo braku wujka, Infernia działa sprawnie. Wujek jest pod opieką medyczną, ale Celina nie ufając Kidironowi, chce być z nim cały czas. Infernia otrzymuje wezwanie od CES Mineralis, gdyż stacja została zaatakowana przez Trianai. Eustachy wysyła Czarne Hełmy i komandosów z Inferni, którzy wspólnie działają, ale Trianai jest za dużo. Eustachy wabi więc Trianai za pomocą energii magicznej, tracąc przytomność po połączeniu z koordynatorem Trianai (magiem), zabijając go. Stacja CES ucierpiała w Paradoksie Eustachego, powodując wiele ofiar. Ardilla koordynuje operację ratunkową, ale gdy okazuje się, że część załogi z CES atakuje mostek próbując ukraść statek, Ardilla wyłącza Infernię i z Ralfem wieją do kanałów Inferni.

Eustachy budzi się w niewoli, przekonuje oprychów, że jedynie on potrafi sterować Infernią (ucierpiał przy tym nieco). Ardilla i Ralf - na prośbę Eustachego - sabotują generatory Memoriam, pozwalając Inferni uwolnić się spod kontroli wrogów. Wywiązuje się strzelanina, w wyniku której napastnicy zostają pokonani - Eustachy kontroluje sytuację. Eustachy decyduje, że ich karą będzie służba na Inferni, ale jeden z nich odchodzi ostrzegać przed atakiem na Infernię. To wszystkim pasuje.

Ekipa wraca na statek, gdzie Kidiron proponuje przesłuchanie pojmanych piratów, obiecując immunitet w zamian za współpracę. Eustachy zgadza się, ale tylko z ochotnikami i Ardillą jako gwarantem bezpieczeństwa. Piraci zgadzają się współpracować mimo użycia neuroobroży przy przesłuchaniu przez Kidirona. Gdy Zespół próbuje montować nowe generatory Memoriam - nie da się. Nie mogą być zamontowane, gdyż się palą. Infernia wykrywa próbę montażu nowych, ale nie zauważa starych.

## Progresja

* Wojciech Grzebawron: młodszy brat Huberta; w odróżnieniu od Huberta piął się w górę w karierze i został third-in-command. Po ultimatum Eustachego dołączył na Infernię.
* Hubert Grzebawron: starszy brat Wojciecha; w odróżnieniu od Wojtka nie szedł ku zaszczytom czy chwale i został liniowym agentem.
* Rafał Kidiron: pozyskał informacje o piratach na Neikatis dzięki śmiałej akcji Eustachego gdy próbowano Infernię porwać.
* OO Infernia: opinia demonicznego, super niebezpiecznego statku u piratów, nomadów i nieArkologicznych sił Neikatis dzięki Hubertowi.

### Frakcji

* .

## Zasługi

* Eustachy Korkoran: dowodził Infernią by ratować CES Mineralis; próbował się połączyć z koordynatorem Trianai by zwabić wszystkie w pułapkę, ale zarezonował z wrogim magiem i eksplodowało. Potem był przesłuchiwany, wypalili mu świnki na pośladkach, ale gdy Memoriam padły to przejął kontrolę nad sytuacją i przechwycił piratów. Dołączył ich do załogi Inferni.
* Ardilla Korkoran: koordynowała akcję ratunkową, wyłączyła Infernię by napastnicy nie mogli jej zdobyć, złośliwie uszkodziła klimatyzację Czarnym Hełmom, schowała się w kanałach Inferni gdy mostek napadli i uszkodziła dla Eustachego generatory Memoriam Inferni. Gwarant bezpieczeństwa piratów podczas ich przesłuchania przez Kidirona.
* Ralf Tapszecz: uczestniczył w akcji ratunkowej (ale przede wszystkim pilnował Ardillę), gdy mostek był zaatakowany to szybkim atakiem zranił dwóch napastników po oaczach. Potem współpraca z Ardillą w sabotowaniu generatorów Memoriam i dywersji.
* Rafał Kidiron: zaproponował przesłuchanie pojmanych piratów, obiecując im immunitet w zamian za współpracę, co pozwoliło mu zdobyć cenne informacje. Pokazał Ardilli, że piraci zgodzą się na przesłuchania neuroobrożą i podkreślał "kapitana Eustachego Korkorana".
* Nadia Sekernik: administratorka CES Mineralis; przechwycona przez piratów ściągnęła Infernię w pułapkę. Zginęła w eksplozji Paradoksu Eustachego. KIA.
* Mariusz Dobrowąs: oficer wojskowy Inferni, szef komandosów. Gardzi Czarnymi Hełmami, chroni tylko swoich. Ucieszył się, że może puścić Hełmy przodem przeciw Trianai. Dzielnie odpierał atak piratów, ale padł (zaskoczyli go). Żyje.
* Wojciech Grzebawron: przesłuchiwał Eustachego przy użyciu wariografu. Ma skille medyczne. Potwierdził, że Eustachy mówi prawdę i gdy Szymon od tortur chciał Eustachego skrzywdzić gdy Infernia była wolna, zastrzelił Szymona. Oddał się pod dowodzenie Eustachego.
* Hubert Grzebawron: zdecydował się opuścić zarówno piratów jak i Infernię; trzeba ostrzec wszystkich by na pewno nikt nie ośmielił się atakować Inferni.
* OO Infernia: gdy Ardilla uszkodziła kolejne generatory Memoriam, pożarła część napastników na swoim pokładzie. Do Inferni dołączyła część piratów z woli Eustachego. Infernia będzie o nich dbać...
* SAN Szare Ostrze: okręt bojowy Kidirona, należy do Arkologii Nativis. Nieobecny, jest w innym miejscu. (rekord by zapamiętać nazwę jednostki)

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Neikatis
                1. Dystrykt Glairen
                    1. CES Mineralis: bardzo ciężko uszkodzony po próbie zdobycia Inferni i ratowania przed Trianai. 21% ludności nie żyje, łącznie z dowodzeniem. Normalnie jest to CES wydobywczy.

## Czas

* Opóźnienie: 11
* Dni: 3


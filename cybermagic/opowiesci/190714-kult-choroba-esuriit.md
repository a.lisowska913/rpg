---
layout: cybermagic-konspekt
title: "Kult, choroba Esuriit"
threads: nemesis-pieknotki
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [190709 - Somnibel uciekł Arienikom](190709-somnibel-uciekl-arienikom)

### Chronologiczna

* [190709 - Somnibel uciekł Arienikom](190709-somnibel-uciekl-arienikom)

## Budowa sesji

### Stan aktualny

* .

### Pytania

1. Czy uda się uratować ludzi?
2. Czy nie będzie złych konsekwencji?
3. Czy nie spadnie wina na niewinnych?

### Wizja

* .

### Tory

* .

Sceny:

* .

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Misja właściwa

Tukan wezwał Pięknotkę do siebie, do Szkoły Nowej w Podwiercie. Powiedział, że może zrobić przeszukanie szerokie... albo Pięknotka może zdobyć dla niego somnibela. W końcu - ów znalazł kult. Tukan potrzebuje pozwolenia na przebadanie ludzi magią którzy nie są dotknięci magią; bez tego nie może tego zrobić. Wyniki badania Tukana są inconclusive, gdyby nie somnibel. Pięknotka załatwiła mu to pozwolenie na szerokie przeszukiwania - byłby kłopot, gdyby faktycznie pojawił się jakiś kult bo _na Astorii memetyczne działania są szczególnie niebezpieczne_.

Tukan zabrał się za solidne poszukiwania. Pięknotka ma spokój, ale Tukan solidnie i poważnie szuka po szerokiej populacji. Coraz bardziej zmęczony, bo to precyzyjne, bo to umysły nastolatków. Skończyło się na tym, że bardzo szeroko napromieniował populację. Znalazł pierwsze nazwisko ale praktycznie padł. Skaził się. Energią Esuriit. Zaprosił Pięknotkę do szkoły. Pięknotka zastała go nad nieprzytomnym chłopakiem; ów ma lekkie drgawki.

Tukan z zimnym uśmiechem powiedział Pięknotce że liderem kultystów jest niejaki Maciek. Pięknotka dostała jego imię i nazwisko. Dodatkowo, Pięknotka poczuła obecność Cienia rozmawiając z Tukanem. A teraz Tukan okazuje szczególną pogardę cierpieniu nastolatka. Pięknotka się za Tukanem skrada - w Cieniu, by dorównać prędkości i zachować ciszę (TpZ=P). Tukan Pięknotkę zgubił; jest szybszy i bardziej agresywny niż kiedykolwiek. Pięknotka straciła na niego namiar. Czym prędzej uruchomiła hipernetowe namierzanie i przekonała Control żeby jej to dali (dowodzi operacją); ku swojemu wielkiemu zakłopotaniu, Tukan jedzie bezpośrednio do Kajrata z dzieciakiem.

Doszła też do tego co wyczuł Cień. Esuriit. Tukan zaraził się głodem Esuriit który w ten sposób się u niego zamanifestował. The meow?

Pięknotka pozwoliła Tukanowi pojechać do Kajrata. Skontaktowała się z Kajratem i poprosiła o odwołanie ludzi; nie chce by stała się im krzywda. Kajrat podziękował pięknie, ale nie zamierza nic z tym robić. Jest gotowy na wszystko. Szczęście 14/20: Tukan nie zdołał zrobić nic terminalnego i krytycznego zanim został powstrzymany i dzieciakowi nic się nie stało. Na to wpadła Pięknotka.

Pięknotka przeprosiła za zachowanie Tukana; wypadek przy pracy. Kajrat powiedział Pięknotce wyraźnie - jest zainteresowany stanem tej dwójki. Chce ich zobaczyć. Pięknotka chce ich natychmiast, ale nie do końca ma czym go przekonać. Kajrat zarzucił Pięknotce, że ona nie chce pomóc nikomu w Enklawach za to, że ją skrzywdzili. Pięknotka powiedziała, że nie o to chodzi. Niestety, Pięknotka nie jest w stanie przekonać Kajrata by cokolwiek zrobić z Tukanem pozytywnego; Tukan ma u Kajrata po prostu przechlapane za tą akcję.

Kajrat zaprowadził Pięknotkę do obu pojmanych przez niego śmiertelników. Są w skrzydle medycznym Rezydencji Kajrata - która dubluje za skrzydło tortur. W tej chwili aspekty tortur nie są uruchomione; widać po Tukanie, że uczestniczył w walce i przegrał. Pięknotka poprosiła o wydanie ich; Kajrat powiedział, że Tukan nie nadaje się do transportu. Jest ranny. Pięknotka zrobiła szeroki uśmiech - czy praworządny obywatel Kajrat nie dałby jej medykamentów by pomóc terminusowi (Tp). Kajrat się szeroko uśmiechnął. Dobrze, ale po cenach rynkowych medykamenty - za nie płaci Pustogor. Pięknotka się zgodziła.

Kajrat dał Pięknotce do pomocy Amandę. Powiedział, że ma traktować Pięknotkę jak VIPa. Amanda nienawidzi i boi się Kajrata, ale wykonuje jego polecenia. Amanda jest uzdolniona w wykrywaniu Esuriit zdaniem Kajrata, zna dotyk tej energii bardzo dobrze i umie ją trochę odpierać. Pięknotka czuje nienawiść Amandy i rozumie skąd się bierze; Amanda jest pod jego całkowitą kontrolą. Pięknotka spróbowała powstrzymać Cienia, który silnie zareagował na te obserwacje Kajrata; udało jej się. Z trudem, ale udało się Pięknotce jakoś odeprzeć nienawiść do Kajrata.

Córka Kajrata srs? Jego najdoskonalsza kreacja?

No dobrze. Pięknotce udało się jakoś wydostać bezpiecznie stamtąd, wzięła ze sobą Amandę a Kajrat zajmie się transportem. Jak obiecał - to to zrobi. I trafią do Blakenbauera Lucjusza. A Pięknotka została sam na sam z Amandą i jakimś dziwnym kultem...

Pięknotka spróbowała zrozumieć Amandę. Kim ona jest, jak ona działa. Pięknotka wydobyła z Amandy to, co najważniejsze. Amanda traktuje Kajrata jak ojca. Jest noktianką. Ona go NIE zostawi. She WILL save him. He is dark, very dark - but this darkness can be healed. She WILL heal this darkness. Nie zostawi go. Za bardzo go kocha. Uważa, że nie jest z nim spokrewniona, ale nie jest pewna. Im bardziej on "próbuje on odepchnąć" tym bardziej ona wierzy, że robi to dla jej bezpieczeństwa.

Oki. Kult. Pięknotka wie, że dzieciak chodzi do Szkoły Nowej - tej samej szkoły co wszystkie inne zainfekowane dzieciaki. Pięknotka dostała ze szkoły adres dzieciaka - mieszka na Osiedlu Leszczynowym, niedaleko. Pięknotka chce znaleźć dzieciaka; chce go znaleźć a potem namierzyć gdzie będzie miał miejsce kult. Gdy Pięknotka z Amandą podeszły do mieszkania chłopaka, Amanda dała jej znać, że czuje energię Esuriit dochodzącą z tamtego mieszkania.

Pięknotka zostawiła Amandę na obserwacji a sama poszła do mieszkania. Zapukała; otworzył jej ojciec chłopaka. Pięknotka poczuła... poczuła delikatny dotyk. Muśnięcie Saitaera. Esuriit. Pięknotka zaznaczyła to miejsce - niech zostanie wyczyszczone; rodzice są śmiertelnie zakochani w swoim arcygenialnym synu. Niestety, nie mają pojęcia gdzie ich syn jest. Pięknotka, zrezygnowana, zdecydowała się poczekać.

Parę godzin później, Amanda dała Pięknotce znać że dzieciak wraca. Poczekała z Amandą w samochodzie... do rana. Przy okazji dowiedziała się od Amandy, że ta uważa, że kult powstał przez to że Kajrat został zraniony przez seksbota; energia Esuriit się uwolniła i dotknęła zdesperowanego chłopaka. To sprawia, że Amanda kieruje miejsce problemów / kult w okolicach Czółenka. Pięknotka dała Amandzie pozwolenie, by ta udała się do Czółenka i znalazła tam rdzeń kultu.

Rano - chłopaczek wyszedł na miasto. Pięknotka go porywa. Sukces. Pięknotka zabrała do szpitala dzieciaka, potem sami zgłosili się jego rodzice. Problem został rozwiązany... chyba.

**Sprawdzenie Torów** ():

* skomplikowane.

**Epilog**:

* .

## Streszczenie

Tukan poluje dalej na temat somnibela i znalazł kult wywołany przez magię. Pięknotka zdobyła mu uprawnienia a on spieprzył - dał się zainfekować Esuriit i ruszył na wojnę z Kajratem. Pięknotka uratowała mu tyłek za co dostała wsparcie Amandy Kajrat. Z jej pomocą zniszczyła kult w Podwiercie, ale dużo wskazuje że w Czółenku (gdzie indziej...) jest ulubiony ciąg dalszy.

## Progresja

* .

### Frakcji

* .

## Zasługi

* Pięknotka Diakon: uratowała Tukana przed Kajratem i współpracując z Amandą Kajrat doszła do tego, że kult Esuriit jest w okolicach Czółenka.
* Tomasz Tukan: chciał rozmontować kult i się o nim wszystkiego dowiedzieć; zainfekował się Esuriit i podpadł Kajratowi. Uratowany przez Pięknotkę.
* Ernest Kajrat: przekazał Pięknotce jako wsparcie swoją "córkę". Nie udało mu się zbadać Tukana, ale zbadał dotkniętego przez Esuriit kultystę.
* Amanda Kajrat: traktowana przez Kajrata jako córka, chce uratować "ojca". Świetnie wyczuwa Esuriit; wskazała Pięknotce Czółenko jako źródło problemów.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Podwiert
                                1. Osiedle Leszczynowe
                                    1. Szkoła Nowa: miejsce badań Tukana, gdzie - oczywiście - musiało dojść do Paradoksu. I Tukan się zaraził.
                            1. Czółenko: po zranieniu Kajrata przez seksbota, tam pojawił się kult Esuriit

## Czas

* Opóźnienie: 0
* Dni: 2

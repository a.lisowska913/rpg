---
layout: cybermagic-konspekt
title: "Lustrzane odbicie Eleny"
threads: legenda-arianny, waśnie-samszar-verlen
gm: żółw
players: kić, fox
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [210311 - Studenci u Verlenów](210311-studenci-u-verlenow)

### Chronologiczna

* [210311 - Studenci u Verlenów](210311-studenci-u-verlenow)

## Punkt zerowy

### Dark Past

* Elena nie zapomniała, że Arianna i Viorika ukryły przed nią informację o tym, kto stał za tym wszystkim. Wie o Blakenbauerze, nie wie o Michale i Rafale.
* Elena by utrzymać kontrolę nad swoją mocą zwróciła się ku wiktoriacie - potężnemu środkowi Blakenbauerów do stabilizacji. Zadziałało, ale Skaża jej wzór.
* Elena zabrała z magazynów "swoje" lustro i zrobiła z niego swój konstrukt.
* Elena, **zbyt** pewna siebie, skupiła moc na lustrach. Jej moc + lustra + wiktoriata i nienawiść stworzyły krystalicznego potwora kontrolowanego przez nią.
* Elena przekonana że robi właściwą rzecz i to jest to co POWINNA robić skupia się na czyszczeniu Verlenlandu ze zła.

### Opis sytuacji

* https://anime.goodfon.com/download/art-suzuya-alice-in-5712/1920x1360/ <-- Mirror Monster

### Postacie

* Elena: Skażona wiktoriatą, z rozechwianym wzorem i dotknięta Esuriit. Pragnie wyczyścić Verlenland ze zła. "Jesteśmy żołnierzami. My się poświęcamy dla nich wszystkich. Tak ma być".

## Punkt zero

52 dni przed sesją właściwą.

Sierżant Czapurt dostał wiadomość, że Elena i Romeo zbliżają się do niewielkiego kultu w niewielkiej mieścinie w bezpieczniejszej (odległej od Trójkąta) sekcji Verlenlandu. Niedaleko jest Viorika i Arianna. Tak więc Czapurt wysłał tam A&V by pomogły młodym rozwiązać jakikolwiek problem się tam znajduje. Dodatkowo, w grupie jest Elena. I jest z Romeem. To znaczy, że będzie potencjalna katastrofa.

Arianna docisnęła "gazu" na ścigaczu. Viorika, chcąc by tym razem się wszystko udało, przesłała informacje do wszystkich na trasie "Arianna leci ścigaczem i się spieszy - na waszym miejscu zeszłabym z trasy". (TrZ+2=>VXV) Dzięki reputacji Arianny (i ku jej irytacji) pojazdy próbowały uciekać jej z drogi. Do tego stopnia, że niektóre pojazdy się po prostu porozbijały. Tak czy inaczej, A&V dotarły na czas - zanim jeszcze Elena z Romeem wkroczyli do "kultu".

Romeo krzyczy na Elenę, że musi poczekać i on nie autoryzuje dla niej żołnierzy. Elena, z pogardą, że nie potrzebuje pomocy - sama sobie poradzi i rozwiąże ten problem bez żadnej pomocy. Ku utrapieniu Arianny, Elena jest ostro umalowana - gotycka tapeta, praktycznie nie widać w ogóle skóry.

Nie, Elena się nie ucieszyła widząc starsze kuzynki. Chciała sama zniszczyć groźny kult. Który zdaniem Romea nie jest groźnym kultem a po prostu niewielką grupką miłośników nieco lżejszej pracy.

Arianna dopytała o co właściwie chodzi. Otóż gdy byli w okolicy Samszarowie, nauczyli mieszkańców tego miejsca jak modlić się do duchów; dzięki temu przy odpowiednim polu magicznym mogą mieć "szamańską pomoc". Jest to bardzo nieVerlenowe, więc zdaniem Eleny musi zostać zniszczone. Zdaniem Romea jest to bardzo wygodne rozwiązanie - Skalniaczki pomagają w rolnictwie i nie tylko. Im bardziej Elena chce to zniszczyć, tym bardziej Romeo naciska by to zachować. Ewidentnie jej dokucza.

Viorika siadła do analizy sentisieci. Co naprawdę wie o tych skalniaczkach? Połączyła się z Samszarami - o co chodzi w tym rytuale? (ExZ+2 => V). Cóż, Skalniaczki są niegroźnymi kamiennymi istotami, pozytywne "skrzaty domowe". Ale ich obecność może trochę negatywnie wpływać na sentisieć - sentisieć z natury próbuje stabilizować i kontrolować magię a skalniaczki podnoszą pole magiczne i zmieniają Pryzmat. Czyli z perspektywy komfortu i ekonomii jest to dobry pomysł, ale z perspektywy stabilności sentisieci, gorzej.

Tak więc A&V zaproponowały następujące rozwiązanie:

* Wioska zmieni nazwę na Skałkową Myśl. Tylko tu wolno używać Skalniaczków, na próbę.
* Ściągnięcie tu naukowca Verlenów na monitorowanie i upewnienie się, że nie ma negatywnych efektów ubocznych.

Romeo jest za. Mieszkańcy SKałkowej Myśli są za. Elena jest przeciw.

* "To są samszarskie, nie verlenowe rytuały!" - Elena
* "I naszym ludziom żyje się lepiej mając skalniaczki Samszarów." - Arianna, ze spokojem

Elena machnęła ręką i zrezygnowała - ku zdziwieniu Arianny i Vioriki. Przyjęła porażkę z dużym spokojem i obojętnością. Też na przestrzeni ostatniego czasu jej moc wzrosła oraz jej kontrola mocy nieprawdopodobnie się zwiększyła. Wyraźnie Elena rośnie i dojrzewa... (w rzeczywistości - chwała narkotykom XD)

## Misja właściwa

Jedna z typowych operacji. Arianna, Viorika i Michał Perikas. Okazuje się, że w okolicy miejscowości Mikrast zaczęto wykorzystywać Skalniaczki. Coś, co nie powinno mieć miejsca. Tak więc 5 żołnierzy i 3 magów udało się na akcję - sprawdzić co się dzieje i o co chodzi. Podstawowe echo magiczne pokazuje jednoznacznie - gdzieś tu znajdują się Skalniaczki i coś jeszcze. Przekroczono zasady. Magowie z żołnierzami zrobili wachlarz - przejść się po Mikraście, zobaczyć co się tu dokładniej dzieje.

Krzyk Perikasa i strzały żołnierzy. Arianna i Viorika biegną - widzą potężnego lustrzanego golema, który PRAWIE wyrwał Perikasowi serce, ale żołnierz rzucił się by go przewrócić. Viorika natychmiast włącza jetpack by złapać Perikasa; wyraźnie golem jest zainteresowany tylko nim.

ExZ:

* XX: golem zionął energią prosto w Perikasa, straszliwie go raniąc i Coś Mu Robiąc.
* V: Michał jest oderwany dzięki Viorice; szorowała po glebie jetpackiem ale udało jej się go złapać.
* V: Viorika jest w powietrzu, na trzeszczącym jetpacku. Golem na ziemi. Nie ma jak jej uderzyć. Próbował zionąć jeszcze raz, ale nie trafia.

Arianna ma chwilę czasu, 10-15 sekund; wykorzystuje je na złożenie zaklęcia.

* V: udało jej się użyć materii dookoła, spiąć ją sentisiecią w coś w stylu lustra i prosto na ogień golema. Golem trafił sam w siebie, jest oszołomiony.
* V: golem rozbłysnął; Arianna nie straciła go z pola widzenia ani z oczu.
* XX: Arianna nie dała rady spętać i unieruchomić / złapać potwora. Umysł/dusza potwora zniknęły. Zostało jedynie ciało.
* V: jest dość, by zrobić jakieś badania.

Michał jest w bardzo ciężkim szoku. Coś się z nim złego dzieje. Viorika wysyła sygnał do Lucjusza "chodź mu pomóc ASAP". Lucjusz pojawia się jak najszybciej (w masce), budząc zainteresowanie i podejrzliwość ze strony Arianny. Zwłaszcza, że Arianna nie widzi go w sentisieci. Viorika wyjaśniła, że Lucjusz jest jej chłopakiem - i uniewidzialniła go w sieci. Arianna jest wstrząśnięta: po pierwsze, jak oni ze sobą wytrzymują. Po drugie, jak Viorika _mogła_ to zrobić - jest przecież Blakenbauerem XD. Plus, jest sarkastycznym i analitycznym typem - a Viorika jest raczej żywiołową, wesołą dziewczyną.

Cóż.

Tak czy inaczej, Lucjusz skupił się na nieszczęsnym Michale (a Arianna i Viorika kategorycznie zakazały komukolwiek wysyłać raport do centrali odnośnie obecności Lucjusza). ExZM+2:

* XX: Michał ma Echo. To, co lustrzany golem zrobił zmienia jego strukturę. (2 tygodnie regeneracji go czekają)
* V: Lucjusz go ustabilizował. Nie będzie zmian permanentnych.
* XV: te odłamki lustrzane mają echo Blakenbauerów; mają zmienić Michała w... kobietę? Tak czy inaczej, by złamać mechanizm, Lucjusz zmienił go w ROPUCHA. W ten sposób nie dojdzie do korozji Wzoru.

Mówimy o uszkadzaniu Wzoru i podobnych przyjemnościach. To nie jest coś typowego. To już poważny problem.

Podczas, gdy Lucjusz próbował naprawić Michała a Viorika próbowała pilnować, by nikt Lucjusza przypadkowo nie ustrzelił, to Arianna skupiła się na obserwacji i zbierania informacji o lustrach. Po pierwsze, wszystkie lustra z okolicy - absolutnie wszystkie co do jednego zostały zniszczone. Wszystkie zniknęły. Czyli ten lustrzany golem pochodzi z "lokalnych luster".

Połączenie faktów: lustra, celem był Michał i echo Blakenbauerów wskazało Ariannie ślad na Elenę. Ale Elena znajduje się w Poniewierzy, z Romeem. Nie ma jej w pobliżu. Choć jak mówimy o lustrach, mówimy o niebezpiecznych formach magii. Tak więc Arianna zrobiła szybkie zapytanie do magazynu rzeczy niebezpiecznych w Poniewierzy - czy lustro, które miało "emulować" Elenę tam nadal jest? Nie. 3 miesiące temu Elena zabrała je z magazynu. Czyli jest niemała szansa, że to Elena odpowiada za to działanie.

Aktualna hipoteza: Elena nieświadomie uruchomiła lustrzanego potwora i trzeba go teraz zatrzymać. Tak więc - A&V pojechały do Poniewierzy.

Arianna i Viorika odwiedziły Elenę w jej kwaterach w Zamku Gościnnym. Elena zaprosiła je na jedzenie. Podczas, gdy Elena rozmawiała z Arianną, Viorika próbowała zbierać i korelować dane. To, co było ciekawe - Elena była niesamowicie spokojna i opanowana, nadal wymakijażowana jak cholera. Ech, fazy nastolatek...

Viorika odkryła, że statystycznie więcej żołnierzy znika na akcjach niż zwykle. Ale dramatycznie mniej niebezpiecznych incydentów i rzeczy dotyczących cywili. U samej Eleny - bardzo małe straty. Większość strat dotyka Romea. A Arianna podczas rozmowy z Eleną doszła do tego, że Elena żałuje bardzo śmierci żołnierzy, ale "to jest to do czego służymy". Ma bardzo Verlenowe podejście i Verlenową obsesję. Stanęło na tym, że Arianna zaproponowała Elenie sparring - powalczmy jak dwie arcymaginie (bo Elena powiedziała Ariannie, że jej moc rośnie i niedługo jej dorówna).

Gdy Arianna i Elena poszły na Arenę, dziewczyny zaczęły fortyfikować tarczę - pełna moc itp. To odcina Elenę od zmysłów zewnętrznych - więc Viorika robi infiltrację Zamku. Znalazła pomieszczenie, gdzie Elena eksperymentuje z Chorym Lustrem oraz ma 20 martwych żołnierzy - a dokładniej, ich esencje. Mikrodozuje sobie w ciało Esuriit i wiktoriatę lustrami. Viorika natychmiast wpadła w lekką panikę i wysłała dwa sygnały:

* Do Czapurta: CHROŃ WSZYSTKICH NIEVERLENOWYCH MAGÓW
* Do Arianny: Zatrzymaj ją tam, w Arenie. Ona wie co robi. Nie do końca jest sobą.

Arianna wezwała Romea i Viorikę i podczas rozmowy z Eleną, która miała odwrócić jej uwagę doprowadziła do tego, że TRZECH MAGÓW VERLEN zablokowało i zamknęło w potężnych tarczach Areny zarówno Ariannę jak i Elenę. Normalnie Elena mogłaby to wykryć, ale jej Skażony Wzór był na tyle odległy od sentisieci, że nie dostała ostrzeżenia. Gdy Elena była zamknięta w tarczach to chciała się wyrwać i użyć swej mocy, ale Arianna ją przekonała - nie rób tego, bo możesz Arenę. Zabić wielu ludzi, cywili. Elena nie była skłonna by to zrobić.

Viorika wysłała sygnał do Czapurta o tym jaka jest sytuacja i że trzeba Elenę unieszkodliwić. Parę godzin później superciężki oddział szturmowy rozwiązał problem... Elena została unieszkodliwiona i Czapurt podziękował czarodziejkom za interwencję.

## Streszczenie

Elena, chcąc utrzymać kontrolę nad swoją mocą zaczęła brać wiktoriatę. To plus przepuszczanie magii przez lustra zniszczyło jej wzór i ją uszkodziło. Elena stała się wampirem - pożera energetycznie żołnierzy by chronić teren i wszystkich cywilów. Plus, pragnie zemsty na Blakenbauerach i na Perikasach. W swoim Skażeniu powołała Lustrzanego Golema. Viorika i Arianna dały radę wymanewrować Elenę, odkryć, że to ona stoi za Lustrzanym Golemem i z pomocą sierżanta Czapurta w Poniewierzy dały radę Elenę unieszkodliwić, by ją docelowo naprawić...

## Progresja

* Elena Verlen: narkotyki i przepuszczanie swoją energię przez lustra ZNISZCZYŁO jej Wzór. Co najmniej pół roku naprawy.
* Elena Verlen: stała się (tymczasowo) wampirem. Zabiera życie żołnierzy; zabiła 20 osób, które jej zaufały i były po jej stronie... jest uznana za POTWORA.
* Elena Verlen: STRASZNE uprzedzenie do rodu Blakenbauerów, Blakenbauerów, każdego Blakenbauera, narzędzi Blakenbauerów i rzeczy na literę "B". PLUS arystokracji Aurum.
* Michał Perikas: zmieniony w Ropucha na 2 tygodnie, ciężka regeneracja. Bo lustrzany golem Eleny mógłby uszkodzić jego wzór.

### Frakcji

* Ród Verlen: sojusz z Samszarami i Perikasami
* Ród Perikas: sojusz z Verlenami i Samszarami
* Ród Samszar: sojusz z Perikasami i Verlenami

## Zasługi

* Arianna Verlen: stoi za zmianą nazwy miejscowości na Skałkowa Myśl. Powiązała lustra z Eleną, po czym zagadywała ją i przekonywała, by Elena nie zrobiła niczego złego ani głupiego. De facto, jej umiejętności gadania sprawiły, że Elena ze spokojem weszła w pułapkę by Czapurt mógł ją unieszkodliwić. Plus: zaszokowana relacjami Vioriki i Lucjusza. 
* Viorika Verlen: jej ukrytym chłopakiem jest Lucjusz Blakenbauer. Uratowała Michała Perikasa używając jetpacka, zbadała sentisieć pod kątem Skalniaczków i ich wpływu oraz odkryła sekretne Miejsce Kaźni Eleny.
* Elena Verlen: perfekcyjna kontrola ciała, umysłu i magii dzięki wiktoriacie; ukrywa ślady pod ostrym gotyckim makijażem. Zabija i "znika" żołnierzy, by chronić cywilów. Nie straciła jednak resztek duszy. Ostatecznie - zamknięta z Arianną (przez Ariannę) na Arenie Poniewierskiej aż przybył Czapurt z oddziałem by ją unieszkodliwić.
* Romeo Verlen: przez Czapurta jest w jednym oddziale z Eleną. Dokucza konsekwentnie Elenie oraz pod koniec autoryzował unieruchomienie Eleny (on, Viorika i Arianna).
* Lucjusz Blakenbauer: chłopak Vioriki; był pod ręką gdy Lustrzany Golem zranił Michała Perikasa. Poza standardowym sarkazmem, udało mu się utrzymać Michała przy życiu i zmienić go w Ropucha.
* Michał Perikas: gdy był na akcji z Arianną i Vioriką, został zaatakowany przez Lustrzanego Golema Eleny. Ranny, uratowany przez Viorikę i Lucjusza, potem Lucjusz na dwa tygodnie zmienił go w Ropucha by jego wzór się nie uszkodził. 
* Przemysław Czapurt: ostrzeżony przez Viorikę o stanie Eleny, natychmiast zabezpieczył wszystkich nieVerlenowych magów na terenie Verlenlandu. Potem z oddziałem unieszkodliwił Elenę.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Aurum
                        1. Verlenland
                            1. Poniewierz
                                1. Zamek Gościnny: mieszkają tam teraz tylko Romeo i Elena; Elena w jednym z pomieszczeń zrobiła salę kaźni (lustra, mikrodozowanie dusz martwych żołnierzy lustrami...), co odkryła Viorika.
                                1. Arena: pojedynek Arianna - Elena, który się nie odbył; po tri-autoryzacji Elena została unieszkodliwiona przez tarcze Areny i kupiło to czas aż wpadł Czapurt z ekipą ją zneutralizować.
                            1. Skałkowa Myśl: miejscowość daleko od granicy; są tu wykorzystywane Skalniaczki Samszarów (stąd nowa nazwa miejscowości). Tu jest pierwsze miejsce ataku Lustrzanego Golema Eleny na Michała Perikasa.
                            1. Mikrast: miejscowość 20 km od Poniewierzy, 

## Czas

* Opóźnienie: 223
* Dni: 3

## Inne

### Projekt sesji

#### Procedure

* Overarching Theme:
    * gracze mają "kierowanie" (Vision & Purpose); wiedzą gdzie iść i po co tam idą
    * gracze są bardziej zaangażowani w historię, bo widzą, gdzie ich decyzje prowadzą
    * MG wie, które sesje wprowadzać i które usuwać
    * MG wie, w którą stronę stawiać adwersariat
* Achronologia:
    * przez przeskakiwanie do przodu nieważnych wydarzeń oczekuję, że każda sesja jest powiązana z kontekstem (gracze widzą jak się to rozwija)
    * przez cofanie się i uzupełnianie luk w przeszłości oczekuję, że brakujące MG i graczom luki uzupełnimy takimi działaniami
    * przez działanie w przeszłości gracze czują silniejszą relację z postaciami i światem, silniejsza budowa wspólnej historii.
* Wyraziste, ważne dla graczy postacie + Relacje (konflikt?) Gracze <-> NPC, NPC <-> NPC
    * graczom bardziej zależy na postaciach i ich powodzeniu
    * więcej potencjalnych wątków i rzeczy które możemy odrzucić przy krzyżykach
* Tory, zmiana postaci z czasem, wpływ graczy na świat
    * gracze widzą jak wpływają na świat. Wpływ jest "ich" - stąd tak tragiczne jest gdy "ich" rzeczy są corrupted / destroyed.

#### Result

**Przeprowadziłem inną sesję, bo Anadia miała tylko 2h, ale na bazie tej**

* Overarching Theme:
    * Theme & Feel: 
        * samotność w rodzie Verlen - każdy mag jest pozostawiony sam sobie. Elena też jest skazana na siebie i tylko na siebie.
        * Batman: Mask of Phantasm. "It is too late". (nope)
        * Mai Hime vs Mikoto (Kagutsuchi vs Mikoto/Miroku). Pain. Hatred. Lonelyness.
    * V&P:
        * uratować ród przed dewastacją polityczną
        * zatrzymać straszliwego potwora
    * adwersariat: 
        * Elena: anioł śmierci, próbujący pomścić siebie i sprawić, by Verlenland był czysty (WR-extreme)
* Achronologia:
    * sesja wsteczna, ma pokazać upadek Eleny
* Wyraziste, ważne dla graczy postacie + Relacje (konflikt?) Gracze <-> NPC, NPC <-> NPC
    * Elena: reprezentuje anioła śmierci. Zemsta, która rozgorzała. Nieufność. Bycie "tą inną".
    * sierżant Czapurt: reprezentuje miłość Verlenów do swojej córki. He will not hurt her, he will redeem her.
    * Michał i Rafał: reprezentują młodość, przeszłość i złe
    * Dariusz: reprezentuje
* Tory, zmiana postaci z czasem, wpływ graczy na świat
    * sierżant Czapurt zginie - lustrzak po prostu zdominuje Elenę.
    * relacje między Verlenami i Perikasami (+innymi rodami) ulegają dewastacji.

#### Scenes

* Scena 0: 
    * cel: DEMONSTRACJA nowej Eleny. Jej kontroli mocy i nadmiaru pewności siebie.
    * impl: Elena + Romeo rozwalają mały kult (lokalne trolle). Romeo krzyczy na nią, że jest okrutna i arogancka. Ona z pogardą. Elena w pełni kontroluje swoją moc, ale jest "obca".
* Scena 1: 
    * cel: DEMONSTRACJA potwora. Wprowadzenie Lucjusza?
    * impl: Potwór próbuje zaatakować Michała, który jest na akcji z Arianną i Vioriką. Atakuje z zaskoczenia, jak się rozdzielili.
* Scena UNRAVEL: 
    * cel: wiedzą, że to Elena.
    * impl: ?
* Scena CORNER: 
    * cel: to już nie Elena...
    * impl: Elena merges with the apparition.
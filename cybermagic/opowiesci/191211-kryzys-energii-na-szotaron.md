---
layout: cybermagic-konspekt
title: "Kryzys energii na Szotaron"
threads: unknown
gm: żółw
players: kić, fox, kapsel, onti
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [191120 - Mafia na stacji górniczej](191120-mafia-na-stacji-gorniczej)

### Chronologiczna

* [191120 - Mafia na stacji górniczej](191120-mafia-na-stacji-gorniczej)

## Uczucie sesji

* "The monster is us"
* "Jak daleko należy się posunąć, by uratować swoich ludzi? Zniszczyć stację?"
* "Jak daleko posuną się ludzie by przetrwać?"

## Punkt zerowy

Szotaron praktycznie nie posiada źródeł energii. Jako stacja, Szotaron jest praktycznie martwy. Z uwagi na obecność stacji Dorszant i Anomalii Dorszant, Szotaron nie ma do końca jak poradzić sobie z jakimikolwiek sensownymi zasobami. Pozostaje im jedynie polować na coś, co da się zdobyć z okolicznych asteroidów. Ale nie ma energii, nie ma źródeł energii a, co gorsza, Szotaron nie jest skalibrowany z astoriańskimi źródłami energii.

W świetle tego na Szotaron - z kultury mimo wszystko noktiańskiej - kapitan Olaf Froczak zażądał, by Azonia skupiła się na wykorzystaniu energii Esuriit. Ona nie jest "w okolicy", ona jest zawsze blisko, "za membraną". Azonia zrobiła to - powstały Krwawe Kwiaty, potężne źródło energii służące do budowania m.in. kwiatów nanitkowych wysłanych do ekstrakcji rzeczy z Anomalii Dorszant. Nowe źródło energii.

Jednak zabawa z Esuriit zawsze, ZAWSZE kończy się tragedią.

### Postacie

Cienie Szotaronu:

* NAUKOWY: biostymulator OR szaman      --> Onti, biostymulator
* ZWIADOWCA: szczur                     --> Fox, szczur
* TECHNICZNY: majster OR nawigator      --> Kić, nawigator
* SPOŁECZNY: inspirator OR inkwizytor   --> Kapsel, inkwizytor

Reszta Stacji:

* Olaf Froczak: komendant stacji, mag bojowy.
* Azonia Arris: inżynier energii, badacz Esuriit.
* Persefona d'Szotaron: TAI, wyłączona i ciężko uszkodzona.

## Misja właściwa

### Scena zero

brak

### Sesja właściwa

Alicja, jeden ze szczurów należących do Cieni, poszła znaleźć co stało się z kilkoma ludźmi. Podczas ataku Cieni na Rezydentów Szotaron (kradzież), część Cieni została złapana i porwana przez lokalnych magów Szotaron. Alicja przepełzła przez rury i dotarła do miejsca znajdującego się po noktiańskiej części stacji powstałej przez kolizję. Alicja była świadkiem tego, jak Olaf torturuje jej kuzyna a Azonia hoduje jakiś dziwny kwiat. Otwierają portal do Esuriit cierpieniem i budują z tego źródło energii.

Alicja nie mogła tego wytrzymać. Spróbowała zabić swojego kuzyna by się nie męczył (nawet ona poznała energię Esuriit). Nie udało jej się. Ciężko ranna, wróciła do bazy przekazać wszystkie informacje i zmarła.

To jest problem. To jest poważny problem. Po pierwsze, Esuriit ze wszystkich rzeczy. Po drugie, Stefan. Stefan jest gościem z Rezydentów, on całe życie pomagał Cieniom i próbował integrować Cienie (astorianie, anarchiści, wolnościowcy) z Rezydentami (noktianie i astorianie, poddani kontroli Olafa z Noctis). A teraz Stefanowi grozi poważne ryzyko. Łukasz, inkwizytor trzymający Cienie za mordę, poleciał i szybko uratował Stefana i jego rodzinę. Acz wykazał się zasadnym brakiem delikatności - w wyniku tego magowie Szotaron mogą nastawić opinię publiczną przeciw Cieniom. Oczywiście.

Magowie Szotaron wiedzą, że Cienie wiedzą o Esuriit. Potrzebują też źródła energii pochodzącego z Esuriit, a do tego muszą poświęcać i torturować ludzi. Tak więc jedyne co mogą zrobić - rozpętać konflikt między Cieniami i Rezydentami. Przy okazji ponabierać jeńców i wszystkich eksterminować.

Cała ta sprawa przerodziła się w walkę o przetrwanie. To niedobrze.

W tym świetle Łukasz zaproponował zdecydowany najazd teraz na laboratorium Esuriit na Szotaron. Teraz nikt tego się nie spodziewa. Szczur, Lena, zaprowadziła ich w tamtą stronę a operacją dowodzi Łukasz ze swoimi silnymi wojami. Tam niestety na straży jest kilku noktiańskich żołnierzy. Biostymulator wykorzystał paranoiczny gaz i rzucił im to; dzięki temu zaczęli się zabijać sami (efekt Esuriit). Przez nagromadzenie nienawiści i wściekłości, wyrósł nowy kwiat Esuriit.

Biostymulator zdecydował się to zbadać. Przy okazji kwiat "ukradł" mu jeden z granatów gazowych i puścił w krwiobieg stacji. Biostymulator użył energii Esuriit, nieświadomie, by targetować ów granat w Olafa. W ten sposób wszyscy zaczęli się Skażać energią Esuriit. Co więcej - Łukasz wpadł do miejsca kaźni swoich przyjaciół (to co widziała Alicja) i zaczął ich okrutnie eksterminować by wyrwać kontrolę nad Esuriit z rąk magów Szotaron. Sukces, acz Łukasz przez moment był większym potworem niż Olaf...

Nagle nasz Zespół stał się silniejszy w Esuriit niż magowie Szotaron. I podobnie Skażony.

Nadszedł czas przejęcia kontroli nad stacją. Przebudzili Persefonę. Sama TAI Persefona była ciężko uszkodzona; zupełnie nie kontaktowała, ale Nawigator dała radę zafałszować sygnał dronami i odbudować połączenie - plus dodali energii Esuriit. Persefona ożyła. Więcej - przejęła też kontrolę nad częścią noktiańską Szotaron. Nagle Zespół ma pełną kontrolę nad stacją, choć zaczyna ich pożerać Esuriit. Niestety, by móc to wszystko osiągnąć - poświęcili Stefana i jego rodzinę.

W Lenie coś pękło. Oni mieli RATOWAĆ Szotaron, nie ZNISZCZYĆ i ZABIĆ wszystkich na kim im zależało. Ale jednocześnie coraz trudniej było im zrobić jakikolwiek ruch; Esuriit zaczęło dominować ich zmysły. Coraz trudniej robić samodzielne ruchy.

Wpierw - uratować ludzi. Łukasz zmusił wszystkich do udania się do kapsuł ratunkowych - wliczając w to Azonię - i niewielu ludzi zabił przy okazji w okrutny sposób. Najbardziej traci kontrolę nad Esuriit ze wszystkich...

By wszystkich ratować, Biostymulator przygotował blokery. Środki chemiczne mające za zadanie blokować ich emocje, nieco odizolować od Esuriit i dać im możliwość zniszczenia stacji (usunąć poczucie "umrzemy, a nawet gorzej, bo Esuriit podtrzyma nas przy życiu tak długo jak jest w stanie czerpać z nas energię"). By to osiągnąć? Poświęcił jedno dziecko.

Nawigator rozłożyła bomby na stacji używając dron. Jej kontrola nad Persefoną pozwoliła to zrobić i odwrócić uwagę Skażonej TAI.

Zostało nacisnąć klawisz i doprowadzić stację do kursu kolizyjnego z Czarnoszeptem. Jedyną, która mogła to zrobić była Lena - najmniej Skażona Esuriit. Lena spróbowała, skupiła się i dała radę to zrobić - acz musiała przez Sympatię poświęcić członka rodziny daleko stąd.

Stacja Szotaron weszła na kurs kolizyjny z Czarnoszeptem. Żadna moc Esuriit, żaden półżywy pasażer, nawet nie półżywa stacja - nic nie dało się zrobić. Wszyscy, którzy pozostali żywi na pokładzie stacji będą cierpieć straszliwe męczarnie, ale nie doszło do Przebicia Esuriit na tym terenie.

To się nazywa 'zwycięstwo'.

**Sprawdzenie Torów** ():

* .

**Epilog**:

* .

## Streszczenie

Szotaron, odcięta od wszelkich źródeł energii, zaczęła umierać. Noktiańscy magowie zaczęli korzystać z energii Esuriit; niestety, ludzie też zaczęli to robić. Esuriit szybko zaczęło opanowywać stację. Grupa wyrzutków astoriańskich wydarła władzę magom noktiańskim, ewakuowała tyle osób ile była w stanie, po czym doprowadziła do zderzenia Szotaron z planetoidą dookoła której ta orbitowała...

## Progresja

* .

### Frakcji

* .

## Zasługi

* Azonia Arris: opracowała źródło energii oparte na Esuriit; jako jedyny mag została uratowana przez Cienie Szotaron i ewakuowana w stronę stacji Dorszant. Noktianka.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Noviter
            1. Pas Sowińskiego: 
                1. Stacja Szotaron: zniszczona, zderzona z Czarnoszeptem. Doszło do Skażenia Esuriit na stacji i heroiczny wysiłek załogi zniszczył stację.
                1. Stacja Dorszant: w której stronę wystrzelono sporo kapsuł ratunkowych z ludźmi i z Azonią.
                1. Czarnoszept: planeta lub wielka planetoida, najpewniej lodowa, dookoła której orbituje Szotaron.

## Czas

* Opóźnienie: 4
* Dni: 3

## Inne

### Aspekty horroru do przetestowania (nie 'horror', splice w akcję)

* Esuriit jako utrata kontroli, mechaniczna (mechanika -> utrata kontroli)
    * Oczekiwania: będą poważnie się zastanawiać czy warto iść tak daleko, coraz bardziej nieswojo
    * Rzeczywistość: nope. Nie stało się to. Fakt, że nie mieliśmy NPCów sprawiło, że to było poruszanie pionkami - DO MOMENTU zabicia Stefana.
* Scena Zero formatująca nienawiść wobec magów stacji. (silna scena zero)
    * Oczekiwania: Gracze będą chcieli koniecznie odsunąć magów stacji od władzy
    * Rzeczywistość: sukces. To było "my lub oni".
* Esuriit odbierające im informacje ze zmysłów zewnętrznych (MG podaje fikcyjne info jako zmysły). Skok: 2. (gaslighting)
    * Oczekiwania: Co jest prawdą, co fikcją? To plus ciągle rosnący głód.
    * Rzeczywistość: nie zrobiłem tego; brak czasu i koncentracji.
* Letalność; postacie mogą umrzeć i dostaną wtedy nowe z Cieni. (straszliwe konsekwencje, nikt ich nie uratuje)
    * Oczekiwania: Nie będę się bawił. Mogę pokazać Szotaron jako ultra okrutne miejsce.
    * Rzeczywistość: sukces. Alicja -> Lena. I prawidłowo sformatowało sesję na wysokie ryzyko.
* NPCe liczą na pomoc postaci Graczy, wierzą im, są ich jedyną nadzieją. (straszliwe konsekwencje, nikt ich nie uratuje)
    * Oczekiwania: jakiejkolwiek decyzji Gracze nie popełnią, jest efekt "mniejszego zła". Niszczą... bo ktoś musi.
    * Rzeczywistość: nope, brak NPC i mało czasu -> nie mamy możliwości zbudowania tego efektu.
* Kontrola tempa. Momenty spokoju i planowania (acz nie nudy), momenty BARDZO wysokiej akcji. ()
    * Oczekiwania: Łącząc z PRZEWIDYWALNYM przeciwnikiem (magowie nie kontrolują swojej stacji, Kwiaty się nie rozprzestrzeniają same) - rośnie poczucie utraty kontroli.
    * Rzeczywistość: nie kontrolowałem tempa, więc to się nie działo.
* Szaleństwo Persefony d'Szotaron x Esuriit (foreshadowing)
    * Oczekiwania: jak pokazanie transformacji Purvisa w Dr. Vipera w Swat Kats -> to jest ich przyszłość.
    * Rzeczywistość: sukces, nawet jeśli w pełni nie pokazałem; ale widząc Persefonę zobaczyli siebie w lustrze.
* Po 5 tokenach Esuriit nie są możliwe heroiczne czyny bez przezwyciężenia Esuriit
    * Oczekiwania: poczucie utraty kontroli, konieczność pójścia na endgame i poczucie silnej presji
    * Rzeczywistość: sukces - wtedy zaczęły się zupełnie inne działania, zupełnie innego typu emocje. Krzyżyk > Esuriit.

### Potencjalne sceny

1. Scena Zero: Cienie atakują, próba okradzenia grupy ludzi naprawiających rury z pożywienia lub energii. Tam jednak dochodzi do Korozji Esuriit.
2. Scena Jeden: Zwiadowca dostaje się zobaczyć los Cieni, którzy zostali porwani przez siły porządkowe Stacji.
3. Persefona, Awaken
4. Dead March Again 
5. Flower of Blood 
6. Persefona, Corrupted: sama Persefona zaczyna zabijać na stacji i pompować więcej energii, nadal służąc Cieniom.
7. Ziomek Dotknięty Esuriit: zabił i pożarł innego człowieka.
8. Ucieczka przed patrolami
9. Esuriit Paranoia Strikes: ktoś konkretnie planuje zabójstwo postaci. Ofc to tylko Echo Esuriit w umysłach postaci, ale...
10. Where the Blood Flowers Bloom...: corruption of station. Heartbeat.

### Źródła problemów

1. Martwa stacja. Niesprawne rury, uszkodzone korytarze, niestabilna struktura, niesprawne roboty ze słabymi czujnikami
2. Żywa stacja. Esuriit wpływa na stację jako dodatkowe źródło energii.
3. Noktianie. Lojalni Olafowi i Azonii. Nie są fanami Cieni.
4. Cienie pod wpływem Esuriit.
5. Persefona, nasza kochana <3. Zwłaszcza pod wpływem Esuriit.
6. Sami magowie polujący na nasze postacie

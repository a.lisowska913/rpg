---
layout: cybermagic-konspekt
title: "Niebezpieczna woda na Hiyori"
threads: historia_darii, salvagerzy_anomalii_kolapsu, syndykat_aureliona_w_anomalii_kolapsu
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [221022 - Derelict Okarantis: wejście ](221022-derelict-okarantis-wejscie)

### Chronologiczna

* [221022 - Derelict Okarantis: wejście ](221022-derelict-okarantis-wejscie)

## Plan sesji
### Theme & Vision

* Stacja 'Nonarion Nadziei' nie zawsze ma dla Ciebie najlepszy sprzęt i czasem masz pecha
* Syndykat Aureliona ma swoje macki na Nonarionie.
    * Nic nie da się zrobić, na szczęście Hiyori nie jest głupią jednostką.

### Co się wydarzyło KIEDYŚ

* .

### Co się wydarzyło TERAZ (what happened LATELY / NOW)

.

### Co się stanie (what will happen)

* S01: FAZA 1 - problemy z jednostką. Czerwie, problemy z wodą, pozbyć się anomalnych intruzów.
    * czerwie disruptują systemy komputerowe i elektronikę; tym się żywią są w stanie poważnie uszkodzić jednostkę
    * DUMP IT! przez odpromienniki.
* S02: FAZA 2 - kto zawinił; czemu Ailira sprzedała kiepską wodę i jak temu zapobiec
    * Nastii i Patrykowi się to bardzo nie podoba. Czy z tym można cokolwiek zrobić?

### Fiszki

#### Nonarion Nadziei (stacja cywilna)

* Leo Kasztop: sprzedawca sekretów na Nonarionie (atarienin)
    * ENCAO:  0-+00 | Intrygancki, polityka;;Nie odracza| VALS: Self-direction >> Stimulation, Tradition| DRIVE: Wygrać w rywalizacji)
* Ailira Niiris: tanio sprzedaje surowce na Nonarionie (adastratka) (34 lata)
    * ENCAO:  0-0+- | Stoicka;;Mówi jak jest;;Dobrze wychowana| VALS: Face >> Humility| DRIVE: Odbudowa i odnowa
* .Franciszek, enforcer Nonariona (atarienin)
    * ENCAO:  +00-- | Lubi rutynę;;Uszczypliwy i zgryźliwy;;Lubi wyzwania | VALS: Achievement >> Security| DRIVE: Komfortowe życie
* .Wojciech, agent Aureliona (faeril)
    * ENCAO:  ---+0 |Bezbarwny, przezroczysty;;Niemożliwy do ruszenia| VALS: Power, Humility >> Tradition | DRIVE: Supremacja Aureliona

#### Hiyori (salvager)

* .Daria jako salvager (inżynier)
* Ogden Barbatov: 44 lata, kapitan + marine + salvager (atarienin)
    * ENCAO:  +-00- |Osoba starej daty;;Jeżeli czegoś chce, weźmie| VALS: Achievement, Power >> Conformity| DRIVE: Uwolnić niewolników
* Nastia Barbatov: 43 lata, pilot + marine + salvager (atarienka)
    * ENCAO:  +0--- |Anarchistyczna, nie ufa władzy;;Lubi żartować| VALS: Universalism, Stimulation >> Face| DRIVE: Utopia Star Trek
* Kaspian Certisarius: 37 lat, advancer, noktianin (adastranin); próbuje być daleko od wszystkich
    * ENCAO:  -+-00 |Skryty;;Bardzo Ostrożny;;Nie widzi nadziei| VALS: Tradition, Family, Hedonism| DRIVE: Corrupted contagion (coś na K1)
* Jakub Uprzężnik: 29 lat, advancer-in-training (atarienin)
    * ENCAO:  0+0-0 |Zdradliwy;;Podejrzliwy| VALS: Stimulation, Security >> Achievement| DRIVE: Rana ego
* Iga Mikikot: 29 lat, medyk + PR, (altinianka); próbuje wszystkich nawrócić
    * ENCAO:  0--+- |Dogmatyczna (Seillia), potępia;;Przyjacielska| VALS: Hedonism, Face >> Power, Humility| DRIVE: Femme Fatale
* Patryk Lapszyn: 28 lat, puryfikator + scientist; nie mag (sybrianin); próbuje wejść Idze lub Darii do majtek
    * ENCAO:  00-+- |Tendencje do podkradania;;Pomaga innym| VALS: Self-direction, Stimulation >> Security| DRIVE: Wolność od innych
* .Adam + Ewa d'Hiyori: androidy wsparcia pod kontrolą Safiry i z opcją inkarnacji Safiry.

### Sukces graczy (when you win)

* .

## Sesja właściwa
### Przed sesją - kontekst

Zasoby: (FINANSE, WPŁYWY, SUPPLIES, SPECIALIST), (WODA, VOLATILES, METAL, RARE, FISSILES, EXOTICS, BIO, VISIMAT)

### Scena Zero - impl

.

### Sesja Właściwa - impl

DZIEŃ 1, 2: 

Daria zna "melodię". Ogden oraz Leo gadają o kolejnej jednostce którą Leo zna. Ta jest bliżej niż Okarantis, ale przez to może mieć konkurencję. Za to jest większa - wszystko wskazuje na to, że ma jakieś ślady biologiczne. Potrzebny jest moduł 'alien containment' lub przygotowania na ewentualną walkę. Leo "sygnatury i ślady wskazują na byty roślinne". Leo pokazuje Darii blueprint; tym razem prospektor przeskanował jednostkę.

Statek wygląda bardzo... kuliście. Ma silniki i cargo, ale bardzo dużo na statku miejsca zajmują różnego rodzaju sygnatury hydroponiki. Statek jest od dawna nieaktywny - to świadczy, że jeśli tam coś żyje to będzie anomalne. Ogden klasyfikuje ten typ statków jako niebezpieczny. Główne potencjalne korzyści? Materiały składowe. Plus zawsze są jakieś mechanizmy czy narzędzia które można wziąć i sprzedać. Leo twierdzi - jeśli uda się coś zrobić z tą jednostką (a zdaniem prospectora to jest możliwe), może dałoby się postawić inną jednostkę niedaleko Hiyori jako magazyn. Dwa miesiące rozkładania (co najmniej patrząc na jednostkę) i solidny zarobek.

Jednostka dostała nazwę kodową "Eden-74". Jest zbyt mocno uszkodzona, by móc określić coś więcej.

Daria robi analizę tego co Leo ma dla niej by określić co i jak.

TrZ+2:

* XXz: Daria nie widziała nigdy czegoś podobnego
    * ma zdecydowanie za mały silnik. Ta jednostka nie nadaje się do latania daleko. Wszystko wskazuje na to, że ta jednostka jest wewnątrzsystemowa; nie ma też silnika warp.
    * ta jednostka ma co najmniej 4-5 "ogrodów". Większość jednostki stanowią ogrody. Co za tym idzie, będzie sporo materiałów typu BIO i VOLATILES. Musi też być sporo wody, acz jak Skażona?
    * Daria musiała o parę rzeczy dopytać, niestety, to zupełnie nowy typ jednostki; nigdy czegoś takiego nie widziała i nie ma w bazie.
    * nic nie wskazuje na to, żeby potrzebna była WIĘKSZA broń; korytarze jednostki wyglądają na normalne - poza pokładem "roślinno-ogrodowym". Który stanowi 60% wielkości jednostki.
    * nie da się udowodnić, że wszystkie pokłady mają przebicie.
        * a jak Daria wie, niektóre formy roślinne potrafią przejść z promieniowania słonecznego na energię magiczną. Ale potrzebują temperatury i powietrza.

Daria requisituje od Ogdena serię ładunków wybuchowych; bez tego może być ciężko jeśli trzeba zniszczyć Skażoną atmosferę w jakimś miejscu. Ogden się w pełni zgodził. Takie 'breachery' - do precyzyjnej intruzji. To by w sumie pomogło też na Okarantis...

Po załadowaniu i załatwieniu wszystkiego Hiyori jest gotowa do lotu. Jak zwykle, Iga jest podekscytowana. Kaspian nawet nie zszedł z Hiyori na stację - "bawi się z Safirą", co Iga powiedziała Darii jako WTF KASPIAN x SAFIRA?! "Advancer i nasza TAI? Co oni robią? JAK oni..." "Rozmawiają?" "Może..." "Co Ci z nimi nie pasuje?". Iga jest lekko... zraniona podejściem Darii. "Nie, nic."

Podróż do Anomalii wymaga kilku godzin lotu. Hiyori dzielnie leci bez problemu, Daria - jak to Daria - monitoruje wszystkie odczyty a przy okazji sprawdza nowy sprzęt. I - niespodzianka - jeden z detektorów bio który używa wykazał coś nietypowego. Cała Hiyori się świeci - a nie powinna. Wadliwy detektor? Ale drugi robi to samo. A Safira nic nie wykrywa - jej autodiagnostyka mówi, że wszystko OK. Daria -> Patryk. Po zobaczeniu, Patryk i Daria rzucili się do instrukcji i szybko znaleźli odpowiedź - detektory BIO są skalibrowane na wykorzystanie w derelictach. Czyli tam, gdzie nie ma dużej ilości bytów biologicznych. Jeśli przekroczymy np. 100 _distinct_ bytów biologicznych to detektory mogą w podobny sposób wariować.

Ale nie mamy 100 _distinct_. Tu jest problem.

Daria włącza reverse thrust silnika by nie dryfować i wyłącza silnik. I włącza żółty alarm.

* Ogden: "co się dzieje?"
* Nastia: "serio? Właśnie... ugh. Przygotowywałam się do... nie oszczędzimy paliwa."
* Daria: "ile mamy bytów bio na pokładzie?"
* N & O: "poniżej 10."
* Daria: "czujniki mówią że 10 razy tyle"
* Safira: "nic nie widzę. Acz... mam większe zużycie reaktora i systemu podtrzymywania życia niż powinnam. Nie widziałam tego przez silniki."
* Ogden: "coś jest nie tak."
* Kaspian: /mruknął, zakładając kombinezon. "Pozwolenie na wysłanie dron poza statek."
* Nastia: "działaj"

Kaspian potraktował Hyori jak derelict. Daria wykorzystuje jedną z dron by połączyć ją ze skanerem BIO używając Fabrykatora. Kaspian skanuje Hiyori z zewnątrz a Daria robi obliczenia by znaleźć anomalie.

TrZ+3:

* X: uszkodzenie drony (elmag powoduje, że Kaspian stracił nad nią kontrolę, ale dronie włączyła się prosta AI - tyle, że drony się trochę poobijały zanim to się poustawiało)
* V: całe spektrum problemów.
    * mamy do czynienia z czymś co robi zakłócenia; to sprawia, że Safira ma problem. Safira nie wie co widzi a co nie. Dlatego defaultuje. Military-grade diversion.
    * epicentrum jest w zbiornikach wody. WSZYSTKICH zbiornikach wody. Skaner bio informuje o obecności dużej ilości istot żywych w wodzie.
* V: bioformy wykryte w skażonej wodzie wyglądają na małe węgorzopodobne istoty; aktywują się pod wpływem energii itp. Najpewniej woda była skażona na wejściu.

.

* Ogden -> Nastia: "kupiłaś wodę od kogo?"
* Nastia: "Jak zawsze, Ailira. Jak zawsze od kilku lat."
* Ogden: "Zrobiłaś dokładne skany."
* Nastia: "Nie. Nie od zaufanego dostawcy."
* Ogden: "Dario, jak trudno będzie wrócić na stację?"

Safira ma input do stanu statku, bo ma dostęp do danych z dron. Safira proponuje "Czy... Dario, możesz mi przekonfigurować systemy i ścieżki? Spróbuję policzyć deltę pomiędzy danymi z dron i moimi subsystemami by odzyskać jakąkolwiek wiedzę na temat statku. To da mi możliwość robienia typowych operacji. Np... dumpowania wody... /z niezadowoleniem".

* Ogden: "Musimy zostawić dość wody i jednego 'węgorza' by móc skonfrontować się z Ailirą. Woda nie jest najdroższa, ale piechotą nie chodzi."
* Nastia: "Kaspian pobierze węgorza i wodę po dumpowaniu w kosmosie. Wolę to, niż mieć dziwne bioformy na pokładzie mojego statku."

Daria pomaga Safirze odzyskać kontrolę nad systemami:

TrZ+3:

* Vz: Safira odzyskuje kontrolę; umie policzyć różnice. Safira NIE WIDZI jak wygląda stan jednostki, ale ma dostęp do wszystkich ważnych podsystemów.
* V: Przy użyciu danych z dron i subtelnych zmian sygnału, Safira może też sterować otwieraniem, zamykaniem wody itp. Kontroluje WSZYSTKO. Zaadaptowała do zakłóceń.
* X: Safira niestety nie ma jak zdobyć informacje o subtelnościach i elementach.

Safira, sfrustrowana "nie widzę, ale mam możliwość uratować statek. Mogę wyrzucić wodę na zewnątrz. Całą wodę. I włączyć ciśnienie." Kaspian "poczekaj, zamontuję kontener by przechwycić próbki.". Ogden "Iga, jesteś lekarzem. Czy możemy tą wodę ZATRUĆ? Zabić te węgorze? Lub porazić je prądem?"

Daria jest inżynierem. Tylko jeden z trzech obiegów wody jest Skażony. Daria jest w stanie w tym momencie wyłączyć wszystko korzystające z tego systemu a potem przekierować pozostałe na inny układ - nawet jak się pociągnie cholerne węże po całym układzie. To zbyt poważna awaria a fakt, że te "węgorze" przeszły MIMO FILTRÓW i mimo wszystkiego jest piekielnie niepokojące. To nowy typ anomalii.

* Ogden: "Mamy potencjalnie duże koszty dekontaminacji Hiyori. Ale nie chcę kosztów związanych z eksplozją Hiyori. (myśli chwilę) - włącz SOS. Wolę akcję ratunkową niż uszkodzenie potencjalnie delikatnych elementów statku."
* Nastia: "Przepraszam. Nie spodziewałam się tego, nie po Ailirze, nie po naszych filtrach."
* Iga: "Dario, jak to przeszło przez filtry, sprawdzałaś je?"
* Daria: "Mają wszystkie atesty i są sprawne. Nawet przeszły testy barwnika (puszczamy i patrzymy co leci)".
* Ogden: "Dodam nowy element do procedury - szerokie skanowanie wszystkiego co ze stacji wpakowujemy do Hiyori. Jak za czasów wojny."
* Nastia: "Nasze filtry radzą sobie z rzeczami, które pochodzą z Anomalii Kolapsu."
* Daria: "Ale nie z tym."
* Nastia: "Ale nie z tym. Co to jest?"
* Daria: "Próbka do pojemnika, próbka dla Igi do analizy."
* Ogden: "Inaczej (oczy się zapalają). Wszystkie biokontenery montujemy tak, by przechwyciły jak najwięcej wody. Dario, złóż beacon reagujący na konkretny sygnał (martwy aż sygnał). Kaspian - wypuść boję tak, byśmy mogli policzyć gdzie będzie za tydzień. Sprzedamy to Orbiterowi."
* Nastia: "Orbiterowi? Czemu im?"
* Ogden: "Bo będą zainteresowani, tego jestem pewny. I nie boję się, że wykorzystają to jako broń przeciwko nam."
* Nastia: "Ogden, to WYGLĄDA na broń. Jaka jest szansa że to się pojawiło samoistnie?"
* Daria: "Tym bardziej potrzebujemy tego jako dowodu"
* Ogden: "Tak, ALE nikomu nie powiemy że mamy ten dowód."

Ogden: "patrz na fakty":

* Ailira która ZAWSZE była dobrym dostawcą, która jest 'niezależną agentką' i handluje tanimi rzeczami nagle sprzedaje zatrutą wodę.
* Filtry - to penetruje nasze filtry. A nie powinno. Daria jest bardziej ostrożna niż zwykle.
* Wykryliśmy to tylko dzięki skanerom bio, których zwykle nie używamy
* To wpływa na TAI.

.

* Ogden: "Jeśli potrzebujemy dowodu, mamy go - czyszczenie Hiyori pozwoli nam zrobić podstawowe badania. Ale jeśli jest coś więcej, nie chcemy mieć tego w rękach. Zamiast tego - sprzedamy to Orbiterowi, bo oni na pewno tego nie zrobili. Nie mają po co."
* Nastia: "Nikt nie ma powodu nas atakować, nie czymś takim. Przesadziłeś."
* Daria: "Testy też trzeba na czymś przeprowadzić."

Daria chce się upewnić, że to nie jest test. Że ktoś **ich** nie monitoruje i nie próbuje rozpoznać co i jak. Niech Kaspian puści pełen skan dron na kosmos dookoła. Ogden i Nastia wyrazili zgodę.

Tr Z (Kaspian był w armii) +3:

* X: jedna z dron, naciśnięta ZA MOCNO przez Kaspiana, odleciała poza zasięg kontrolny.
* Vz: jest DOWÓD, że nie ma w pobliżu innej jednostki. Jeśli jest test, Hiyori nie jest przedmiotem tego testu.

Daria zauważyła coś ciekawego - Kaspian sterował tymi dronami inaczej niż zwykle. Szybciej, bardziej zdecydowanie, bez unikania czegokolwiek. Military reflexes. Stracił dronę dla pewności, ale to był kalkulowany koszt. Jest serio świetnym advancerem ale to znaczy, że jest advancerem z doświadczeniem wojskowym. Ogden TEŻ to zauważył, co Daria zauważyła.

Daria próbowała po sobie nie pokazać że wie. Kaspian zauważył, że ona wie. Lekko zadrgała mu powieka, po czym się odwrócił i skupił się na działaniach w kosmosie.

Pozbywamy się wody i próbujemy ją przechwycić - Kaspian i Jakub, oni pomontują wszystkie rurki (dumping hole & hoes). Kaspian asekuruje dronami a Safira chwytakiem.

TrZ+3:

* V: Przechwycona woda do kontenerów i dump udany
* V: Udało się bez uszkodzeń; bardzo profesjonalna operacja

Daria stabilizuje system - z jednej strony uruchamia silnik, by Nastia mogła to kontrować. Z drugiej strony z Safirą pilnuje, by utrata wody i działania bez chłodzenia i odkażania nie była problematyczna. Patryk z nią policzył co i jak.

TrZ+3:

* V: Bez kłopotu, Hiyori jest stabilna i wszystko działa. Utrzyma się do sygnału SOS.

Kaspian z Nastią próbują wysłać kontenery tak, by nikt nie miał do nich dostępu i dało się policzyć ich lokalizację.

Tr Z (Daria jako inżynier syntezujący napęd balistyczny + beacon) +3:

* X: nie wszystkie kontenery uda się odzyskać; 1 zostanie zniszczony. Debris.
* V: 3 kontenery uda się odzyskać do 2 tygodni.

I zostało jedynie czekać aż sygnał SOS ich odbierze...

DZIEŃ 3:

Bardzo dużo biurokracji, analiz, ubezpieczeń. Cały poprzedni dzień był analizowany. Daria zobaczyła skalę uszkodzeń Hiyori - dużo trzeba wymienić lub odkazić. Hiyori leży 2 tygodnie w porcie. Nawet nie dolecieliśmy do derelicta. Nie zmienia to faktu, że cała ekipa spotkała się w przestrzeni wykupionej przez Ogdena.

* Ogden: "Sprawdzałem jeszcze raz wszystkie papiery. To na pewno nie chodzi o Eden-74. Myślałem, że E-74 może mieć jakieś znaczenie i dlatego nas sabotowali, ale nic na to nie wskazuje. Nic."
* Daria: "Pech?"
* Nastia: "To nie pech. Ailira nie ma dostępu do wody z jakichś rzadkich miejsc. To jednoosobowa agentka. Jaki asteroid czy coś tego typu miałby coś takiego sformować? Wszystko ZAWSZE było czyste."
* Jakub: "Może nie Hiyori była celem. Może celem była Ailira. Skompromitowanie jej."
* Nastia: "Czymś takim? Za droga sprawa."
* Jakub: "Połącz to co Daria mówi o teście - kilka jednostek wylatuje. Zero wraca. Test udany."
* Daria: "Niewiele możemy z tym zrobić, nie?"
* Ogden: "Nie, nic z tym nie zrobimy. Porozmawiam z Ailirą. Może coś się dowiem."
* Nastia: "Najlepiej, jeśli ja i Daria z nią porozmawiamy - sprawdzimy, czy dało się w jej procesie detekcyjnym wykryć to cholerstwo. Ty musisz lecieć do Orbitera."
* Ogden: "Prawda. Wylecę jeszcze dzisiaj."
* Nastia: "Gorzej, NIE STAĆ NAS na to, by aż tyle wynajmować kwatery na tej stacji. Jeśli szybko nie wrócisz, znajdę coś, gdzie dołączymy do CZYJEJŚ załogi na dwa tygodnie."
* Ogden: "(wyraźnie nie podoba mu się ten pomysł) wpierw Okarantis a teraz to. Coś wymyślę."

Nastia i Daria poszły porozmawiać z Ailirą. Ailira jest 34-letnią dziewczyną. Solidnie zbudowana, wyraźnie pracuje fizycznie, ale sympatycznie wygląda.

* Ailira przyznała, że mogły jakieś bioformy się przedostać. Nie ma dowodu, że to się nie stało.
* Ailira jest przerażona, przerwała rozmowę by iść do kapitanatu - wie, komu sprzedała wodę i chce się upewnić, że te jednostki dostaną pomoc jeśli trzeba.
* Ailira jest zdruzgotana tymi wieściami. She's broken.
* Daria pokazała Ailirze jak się przed tym chronić, jak zmienić procedurę. Ku swemu zdziwieniu, usłyszała oprócz podziękowań "to się na pewno już nie powtórzy". Coś było INNEGO w tej dostawie. 

Nastia i Daria były potem przechwycone przez Leo, dyskretnie.

* Leo: "Nastia, masz problem finansowy?"
* Nastia: "Powiedzmy że mam. I?"
* Leo: "Między nami, bo lubię z wami pracować - nie bierz tanich pożyczek od nikogo, komu bardzo nie ufasz na tej stacji."
* Nastia: "NIKOMU nie ufam na tej stacji"
* Leo: "Nie żartuję - unikaj tanich pożyczek z nieznanego źródła"
* Daria: "Brzmi jak dobra rada"
* Nastia: "Zaskakująco dobra - czy coś się dzieje, o czym powinnam wiedzieć?"
* Leo: "Różne siły próbują mieć wpływ na stację i jej członków. Gdybym ja próbował zdobyć wpływ na Was, doprowadziłbym Was na skraj bankructwa i użyczył taniej pożyczki za niewielką przysługę"
* Nastia: "I czym się to różni od Okarantis lub Eden-74? (z przekąsem)"
* Leo: "Jesteście teraz wrażliwi. Serio, Nastia, uważajcie. Znajdę Wam jakąś... lepszą jednostkę następnym razem."
* Nastia, poważnie: "Dzięki. Doceniam."

## Streszczenie

Załoga Hiyori kupiła lokalizację nowej jednostki w Anomalii na salvage. Jednak podczas lotu okazało się, że zakupiona woda (od stałego dostawcy) jest niebezpieczna; ma bioformy reagujące z energią. Hiyori pozbyła się wody, wysłała sygnał SOS i zamknęli wodę w pojemnikach biocontain. Ogden poleciał do Orbitera użyć starych kontaktów i sprzedać a Nastia skonfrontowała się ze sprzedawcą wody. Leo (sprzedawca informacji) ich ostrzegł, żeby nie brali tanich pożyczek - coś się dzieje na Nonarionie. 

## Progresja

* SCA Hiyori: uszkodzona przez dziwne "węgorze" w kupionej skażonej wodzie. Około 15 dni na stacji Nonarion by wyczyścić i naprawić jednostkę (niższy priorytet)
* Kaspian Certisarius: zdradził się przed Ogdenem i Darią ze swoich militarnych manewrów dronami. Wiedzą, że ma coś wspólnego z wojskiem.

### Frakcji

* .

## Zasługi

* Daria Czarnewik: wykryła bawiąc się zabawkami do detekcji BIO 'węgorze' w wodzie, natychmiast zrobiła alarm i nadzorowała operację pozbycia się niebezpiecznej wody.
* Leo Kasztop: zmartwił się tym, że Hiyori może mieć problemy finansowe. Ostrzegł Nastię, by nie brała tanich pożyczek bo dzieje się tu coś dziwnego. 
* Ogden Barbatov: podjął decyzję o SOS i abortowaniu misji. Uznał niebezpieczną wodę za broń i chce ją sprzedać Orbiterowi. Widać po nim doświadczenie wojskowe.
* Nastia Barbatov: kupiła wodę od Ailiry nie sprawdzając jej specjalnie (zaufany dostawca), nie chce tego tak zostawić, ale ważniejsze jest chronienie załogi niż coś innego.
* Kaspian Certisarius: wzbudził 'zainteresowanie' Igi, bo flirtuje i erp z Safirą. Ale potem jak było trzeba to świetnie dowodził operacją kosmiczną dekontaminacji Hiyori z niebezpiecznej wody ORAZ manewrował dronami jak doświadczony żołnierz.
* Jakub Uprzężnik: wspierał bez szczególnego narzekania Kaspiana. Ale potem na spotkaniu powiedział, że najpewniej nie Hiyori była celem - a Ailira. Podejrzewa, że to jakaś forma konspiracji. Co nikogo nie dziwi, bo on wszystkich podejrzewa o konspirację.
* Iga Mikikot: chciała poplotkować z Darią o Kaspianie, ale się odbiła. Jej wiedza bio nie jest wystarczająca, by zrobić coś z 'węgorzami'.
* Patryk Lapszyn: pomagał Darii w kalibracji detektorów BIO, przeczytał instrukcję. Potem z Darią pomagał w kalibracji Hiyori by pozbyć się 'węgorzy'.
* Safira d'Hiyori: oprócz erp z Kaspianem (XD) miała problem bo 'węgorze' zakłócają jej subsystemy. Po współpracy z Darią była w stanie płynnie odzyskać kontrolę nad Hiyori i doprowadzić statek do stanu aktywnego.
* Ailira Niiris: (adastratka | ENCAO: 0-0+- | Stoicka;;Mówi jak jest;;Dobrze wychowana| VALS: Face >> Humility| DRIVE: Odbudowa i odnowa) jedna ze sprzedawczyń wody na Nonarionie; sprzedała niebezpieczną wodę Hiyori. Zmartwiło ją to; jak się dowiedziała, pobiegła do kapitanatu poinformować jakie jednostki mogą mieć problemy.
* SCA Hiyori: operacja salvagowania skończyła się niebezpieczną wodą z dziwnymi bioformami. Hiyori jest uszkodzona.

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Obłok Lirański
                1. Anomalia Kolapsu, orbita
                    1. SC Nonarion Nadziei: nazwa, bo składa się z 9 noktiańskich statków; mobilna stacja. Ma opcje handlowe i naprawcze.
                
## Czas

* Opóźnienie: 95
* Dni: 4

## Konflikty

* 1 - 
    * 
    * 
* 2 - 
    * 
    * 
* 3 - 
    * 
    * 
* 4 - 
    * 
    * 
* 5 - 
    * 
    * 
* 6 - 
    * 
    * 
* 7 - 
    * 
    * 
* 8 - 
    * 
    * 
* 9 - 
    * 
    * 
* 10 -
    * 
    * 
* 11 -
    * 
    * 
* 12 -
    * 
    * 
* 13 -
    * 
    * 
* 14 -
    * 
    * 
    * 
* 15 -
    * 
    * 
* 16 -
    * 
    * 
* 17 -
    * 
    * 
* 18 -
    * 
    * 

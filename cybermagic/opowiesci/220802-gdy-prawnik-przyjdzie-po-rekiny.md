---
layout: cybermagic-konspekt
title: "Gdy prawnik przyjdzie po Rekiny"
threads: rekiny-a-akademia, dysonans-diskordii
gm: żółw
players: kić, anadia
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [220730 - Supersupertajny plan Loreny](220730-supersupertajny-plan-loreny)

### Chronologiczna

* [220730 - Supersupertajny plan Loreny](220730-supersupertajny-plan-loreny)

## Plan sesji
### Theme & Vision

* Ci, którzy przychodzą pomóc często są większym problemem niż wrogowie i nie zawsze nienawidzą (Marsen -> Lorena)
* W którymś momencie trzeba podjąć decyzje i działać z konsekwencjami swoich działań (Hipolit -> Marysia)
* Sojusznik jest wrogiem, wróg sojusznikiem i unlikely alliance (Marysia - Amelia - Ernest - Torszecki)

### Co się wydarzyło KIEDYŚ

* Keira, agentka Ernesta, przechwyciła z pustego Apartamentu grupę kultystek Ośmiornicy. A przynajmniej tak to wygląda.
    * Keira + Ernest mają przerażającą opinię wśród Rekinów (Keira is a menace); nikomu to nie przeszkadza.
    * Wszystkie osoby przechwycił Ernest z Apartamentu, m.in. Melissę - "należącą" do Chevaleresse.
* W sprawę zamieszana jest najbardziej "samotna" elegancka dama Diakonów, Mimoza Elegancja Diakon, która nie ma NIC wspólnego z Karoliną.
    * Mimoza miała jakieś działania związane z kultystkami, a przynajmniej na to wszystko wskazuje. Typowe na Diakonkę, nietypowe na Mimozę.
* TAI Hestia jest Skażona ixionem - i jest sojuszniczką Marysi
    * Przeniesienie ixiońskie Marysia -> Hestia (przez kontakt z Marysią Hestia staje się bardziej jak Marysia).
    * Hestia jest aktywną sojuszniczką Marysi i chce, by Ernest x Marysia a nie Ernest x Amelia :3
* Sowińscy są niezadowoleni, uważają to za błąd - wyślą kuzyna Jeremiego by pomógł Marysi. ALE jeszcze się nie stało
* Rekiny się dowiadują, że Marysia stoi za "państwem policyjnym" w Dzielnicy Rekinów
* Amelia Sowińska odzyskała częściowo kontakt z Aurum i Rekinami. Przez to, że chcieli się skonsultować z nią wrt Hestii itp. Przez Hestię.
    * To też jest forma ixiońska, o czym nikt nie ma pojęcia. Amelia nie jest ekspertem, nikt tu nie jest więc "ok, udało się połączyć". Nie wiedzą jak.
        * Czyli jeśli Amelia wejdzie w esuriit w jakimkolwiek stopniu, straci kontakt z Hestią / przez Hestię.0
* Pomniejsze waśnie Marysiowe
    * Liliana Bankierz (po stronie AMZ) nie może darować Marysi "zdrady". Ani Karolinie. Ani nikomu.
    * DJ Babu (po stronie Rekinów) nie może darować Marysi "zdrady" Stasia Arienika i oddania go "tyranizującej mamie".
        * I tego że robi z Rekinów państwo policyjne.
    * Santino Mysiokornik słusznie obawia się oskarżenia o nadmierne zainteresowanie młodymi damami (zwłaszcza Chevaleresse) i chwilowo unika Marysi.
* Liliana Bankierz trafiła do szpitala, do larwy Blakenbauera.
* Lorena ma wsparcie - Marsena Gwozdnika - który wierzy w jej dobroć i kompetencję. Ona ofc musi udawać.

### Co się wydarzyło TERAZ

* .

### Co się stanie

* Hestia ostrzeże Marysię, że coś tu zaczyna się dziać ze strony Amelii
* Prawnik Hipolit żąda by Rekiny były kontrolowane. Ernest też. Ernest nie pozwoli by jakiś PRAWNIK go ograniczał.
    * nazwiskiem Ernesta i Eternią podobno straszy się lokalny biznes w Majkłapcu. Robota Karoliny i Daniela.
* Pojawi się Jeremi i będzie robił audyt; Amelii to nie pasuje.
    * plotka o akcie Marysi.
* Marsen żąda oddania Loreny z rąk podłej Marysi.
* "Wojna" Mimoza - Ernest

### Sukces graczy

* .

## Sesja właściwa
### Scena Zero - impl

.

### Scena Właściwa - impl

Marysia już od pewnego czasu nie mieszka w swojej Rezydencji; wprowadziła się do Ernesta. Żeby się do niej przyzwyczaił, żeby przywykł że "tam jest". W swoich oczach - by uwieść. W oczach Ernesta i Rekinów - pracują nad sprawą kralothów i tak jest po prostu bezpieczniej. W oczach Rekinów dodatkowo - bo Marysia chce mieć Ernesta na oku. W oczach Rekinów, które Marysi nie wierzą? Bo ją owinął dookoła paluszka. A Marysia pokazuje, że uspokaja Ernesta i chce mieć go na oku jako peacekeeper.

Marysia próbuje Wywrzeć Odpowiednie Wrażenie na Rekinach i Erneście i w ogóle.

Tr (Ex -> T) +3:

* X(z): Rekiny postrzegają Ernesta jako taką baryłkę prochu; jest potrzebna Marysia by to neutralizować.
* V: Rekiny patrzą na ruch Marysi jako na WŁAŚCIWY. Marysia robi to co powinna w tej sytuacji.
* V: Ernest chce Marysię u siebie tak jak Amelię. "Kuzynka, ale super".
* X: Plotki i ploteczki o romansie Marysi i Ernesta. Ploteczki, które gdy Amelia się połączy, trafią do niej.
* V: Lepsza Marysia w garści niż Amelia na dachu ;-). Czyli Ernest WOLI Marysię od Amelii póki M tu jest a A nie.

Więc czas spokojnie mija, Marysia nakłada hold na Ernesta, wszystko wreszcie toczy się prawidłowo i pozornie nic nie może się spieprzyć. Hahaha, right.

Z rana, Hestia zdała Marysi raport. W skrócie:

* Z jej rezydencją próbuje skontaktować się PRAWNIK AMZ. Dokładniej, student prawa. Hipolit (amz).
    * -> tu Marysia może tylko dowiedzieć się o co chodzi. Może być niebezpieczne / pilne (zwłaszcza bo audyt).
* Amelia (aurum) nawiązała połączenie z Hestią i delegacja Rekinów chce z nią rozmawiać. Odnośnie m.in. Hestii, administracji, kodów itp. I administracji. "no bo Amelia się zna".
    * -> tu Amelia może zdobyć wpływy na terenie i ew. zaszkodzić Marysi. Ale ogólnie - mało "istotne" póki nie odbija Ernesta.
* Sowińscy wysłali kody do Hestii, że będzie robiony AUDYT czynów Marysi i ich decyzji. Jeremi (aurum) Sowiński się nie patyczkuje.
    * -> tu Marysia chciałaby by Rekiny ją chroniły, by jej opinia była piękna i by ogólnie wszystko było kryształowe. Tu może Hestia pomóc jakoś.
* Formalne żądanie arystokraty Aurum z rodu Gwozdnik o uwolnienie Loreny (rekin) z podłej niewoli eternijskiej. Przez jej kuzyna, Marsena (aurum).
    * -> WTF? Ale wyjdzie na audycie.
* Liliana (amz) Bankierz, która ma kosę z Marysią za wydanie Loreny, w TAJEMNICZYCH OKOLICZNOŚCIACH została ciężko ranna i przebywa w gliździe Sensacjusza. 
    * -> AUĆ, na pewno wyjdzie na audycie. Choć czemu Liliana jest w gliździe?! Może audytor uznać, że to DOBRY ruch Marysi by neutralizować wroga. A może... nie? To trzeba dobrze rozegrać.
* Z innych wiadomości, Arkadia (rekin) Verlen pobrała sporo środków leczniczych od Sensacjusza bo ostro oberwała. Arkadia też została u Sensacjusza na pewien czas, co samo w sobie pokazuje skalę jej ran.
    * -> irrelevant.
* (to Marysia wie NIE od Hestii a od siebie) Ernest przygotowuje się do uderzenia na działania Mimozy w Podwiercie. By pokazać jej konsekwencje stawania mu na drodze.
    * -> audytor nie będzie zadowolony widząc ruchy przeciwko Aurum robione przez Eternię.

Co zrobi Marysia w tym pięknym dniu? I czemu zaczyna boleć ją głowa?

Nie no - najpierw prawnik. O co tu chodzi.

Hipolit z przyjemnością przyszedł do Rezydencji Ernesta. Tam został doprowadzony do pokoju gdzie znajduje się Marysia. Ernest chciał POKAZAĆ Hipolitowi, więc odprowadził do Marysi go ŻORŻ ze swoim półnagim torsem.

Marysia i Hipolit. Młody prawnik z AMZ. Uczy się na prawnika dopiero, tyle dobrze. Ale jest magiem, tyle źle. Ma kilka rzeczy które wymagają reakcji lady administratorki Dzielnicy Rekinów. Czyli Marysi.

1. Nazwisko tien Namertela pojawia się niepokojąco często w kontekście biznesu lokalnego w Podwiercie, Zaczęstwie i okolicach. Ludzie się boją. Nieuczciwej konkurencji i działań bezpośrednich. Niektóre firmy mają kłopoty, niektóre umowy nie są dotrzymywane i konsekwencje są nierealizowane za eksterytorialną Dzielnicą Rekinów.
    * -> czyli prawo nie działa na Rekiny zgodnie z umowami, nie tak jak powinno
    * --> PROŚBA by porządek prawny był zachowany albo by Rekiny faktycznie mówiły kontrahentom o stanie 'specjalnym' - że ich prawo normalnie nie dotyczy. Prośba o OSOBISTĄ interwencję Marysi.
        * jeśli Marysia się zgodzi, ++lokalsi ale straszny cios, bo przecież te umowy są "od zawsze", robione jeszcze przez Amelię i przed nią.
        * jeśli Marysia się nie zgodzi, będzie ogłoszone, że Aurum i lady administratorka Aurum mają taki a nie inny status i nie zamierzają tego zmieniać itp.
2. Liliana Bankierz. Żądanie wydania Liliany. Liliana została napadnięta i okrutnie potraktowana przez Rekiny. Żądanie przeprosin od odpowiednich osób, reperacji oraz oddania Liliany wraz z osobistym oświadczeniem Marysi, że nic jej nie zrobiono złego.
3. Żądanie, by Rekiny wreszcie wzięły się w garść i przestały szaleć. Niech nie krzywdzą ludzi dookoła. Róbcie policję wewnętrzną. Pokażcie, że za złe czyny jest kara.

A Marysia żąda informacji o umowach, szczegółach itp. Hipolit przedstawił Marysi papiery, dokumenty świadczące o tym, że Ernest faktycznie robił RZECZY i nie były dotrzymywane umowy. To autentyczne, faktycznie firmy cierpiały. Oprócz tego inne dokumenty pokazujące działania innych Rekinów, ale Ernest najwięcej. Znamienne, że nigdzie nie ma nic na Mimozę. Mimoza jest lubiana przez ludzi w Podwiercie.

Marysia podziękowała za wizytę, kazała mu iść i powiedziała, że nic nie obiecuje ale dokładnie przeanalizuje sprawę. Hipolit się ukłonił, wie, że nic nie wskóra w tej chwili. A Marysia poszła do Ernesta.

Ernest potwierdził, że robi rzeczy z małymi biznesami w okolicy. Marysia powiedziała, że był u niej Hipolit. Pokazała dokumenty. Ernest przejrzał dokumenty i potwierdził ich autentyczność - faktycznie, robił te biznesy. Ale się zachmurzył - nie widział wcześniej niektórych informacji. Zaraz zawołał Żorża, który szybko się pojawił. Żorż szybko wyjaśnił - była próba sabotażu Ernesta i jego działań przez Przeciwnika. Ale już są mechanizmy defensywne i korekcyjne, np. sprzedał Lancera napastnika i za te pieniądze zasilił część tych biznesów. Przeciwnikiem okazał się być niejaki Marsen Gwozdnik, kuzyn Loreny. On za tym stał.

Ernest ma groźną minę. MIMOZA. To jej wina. Wyjaśnił Marysi linię:

* Mimoza działa na tym terenie i jako jedyna z Rekinów pomaga ludziom
* Ernest zaczyna pomagać ludziom i ściągać ich do Eterni
* Mimoza zdobywa Lorenę od Ernesta i wykorzystuje ją by ściągnąć kuzyna Loreny
* WIEMY, że Mimoza bardzo nie lubi i nie ufa Eterni
* Kuzyn atakuje biznesy Ernesta. Nikt nie uwierzy, że to Mimoza, bo Lorena to idiotka. Lorena jest bezużyteczna, to "tylko" artystka. Więc Mimoza użyła Loreny by za jej pomocą zaatakować biznes Ernesta.

TAK TO JEGO TOK MYŚLENIA. Marysi nie pasuje, bo Mimoza nie jest osobą tępiącą kogokolwiek a na pewno nie ludzi. Ernest na to - "popatrz, tylko Mimoza jest lubiana wśród ludzi z Rekinów; ja łamałem ten monopol."

Marysia tłumaczy Ernestowi, że to może nie być Mimoza - przecież Marsen żądał od Marysi wydania Loreny. A przecież ma ją Mimoza. Czyli to może być samowolka Marsena. Ktoś próbuje skłócić Mimozę i Ernesta i sprawić, by nikt nie mógł pomagać ludziom. (super argument)

Tr Z (fakty, dowody itp.) +4:

* V: Ernest jest skłonny oddać Marysi to śledztwo tak jak zawsze to robiła (jak ze ścigaczem)
* Vz: Ernest nie zrobi żadnego ruchu przeciw Mimozie ani Marsenowi póki Marysia do czegoś nie dojdzie ALE nadal chce krwawej zemsty na tym co za tym stoi
* V: Ernest zdaje się na Marysię w sprawie politycznej
* X: Ernest jest zacietrzewiony na Mimozę, Marsena i Lorenę. Nie robi działań, ale chce ich krzywdy.
* V: Ernest będzie zdawał się na Marysię w sprawach politycznych (+1Vg w takich sytuacjach)
* V: Ernest powie Amelii, że blisko współpracuje z Marysią i zrywa z Amelią zaręczyny.

Marysia bez problemu skojarzyła: Marsen x Liliana. Ale też Liliana x Arkadia (w tym same czasie dwie ranne). Więc coś tam się stało. I jeszcze się wszystko kręci dookoła Loreny. A Liliana jest nieprzytomna.

Marysia -> Sensacjusz; kto przyprowadził Lilianę? On odpowiedział, "Daniel Terienak". Brat Karoliny. Obrażenia Liliany wskazują na ostry atak nożem... "przyczyna ran? Arkadia". Oki... Marysia -> Karolina. WTF.

Karolina "a czemu Cię to interesuje co się stało? XD". Marysia "bo to skomplikowane... przyszedł prawnik z AMZ w tej sprawie i żąda reperacji i przeprosin." Karo "niech spierdala". Marysia "ta sprawa jest powiązana z wrabianiem Ernesta w interesy mające szkodzić ludziom i eskalowanie Ernest - Mimoza." W skrócie, Marysia wyjaśniła WSZYSTKO.

Karolina -> Marysia "Marsen postanowił namieszać w biznesie Ernesta co się wiąże z papierami; właściciel jednego z biznesów poprosił o pomoc. Karo poprosiła Arkadię o pomoc a Liliana udawała Marka (byłego Arkadii). I TAK, Karo powiedziała, że MÓWIŁA Marsenowi że Lorena nie jest w rękach Ernesta..."

Karo zostawiła Marysię z głębokim Marysiowym smutkiem...

## Streszczenie

Marysia w końcu założyła hold na Erneście i się doń wprowadziła. Ernest zerwał z Amelią i plotki dotarły do niej czemu. Tymczasem plany Marsena sprawiają, że Ernest zaczyna być coraz bardziej zły na Mimozę (co Marysia neutralizuje) a Hipolit, prawnik z AMZ, chce by Marysia kontrolowała Rekiny skoro już Enklawa jest eksterytorialna. Albo coś się będzie musiało zmienić.

## Progresja

* Ernest Namertel: spodobała mu się Marysia Sowińska BARDZIEJ niż Amelia, póki tu jest. To znaczy, że Marysia go odbija
* Ernest Namertel: będzie zdawał się na Marysię w sprawach politycznych (+1Vg w takich sytuacjach)
* Ernest Namertel: zerwał zaręczyny z Amelią, bo Marysia też jest w jego życiu i Amelia nie jest już tą jedyną
* Ernest Namertel: zacietrzewiony na Mimozę, Marsena i Lorenę. Oni stoją na drodze do lepszego świata i konspirują przeciw niemu.
* Amelia Sowińska: Ernest zerwał z nią zaręczyny, bo nie jest jedyną dziewczyną w jego życiu (Marysia). Plotki jej mówią o tym że to wina Marysi.
* Marysia Sowińska: zamieszkała w Apartamencie Ernesta, "by mieć nań większy wpływ". Rekiny uważają że to dobry pomysł.

### Frakcji

* .

## Zasługi

* Marysia Sowińska: wprowadziła się do Ernesta, splątała go ze sobą i przekierowała na siebie jego uczucia. Jednocześnie przekonała go, żeby nie atakował Mimozy tylko dał jej rozwiązać sprawę.
* Hipolit Umadek: młody prawnik z AMZ; przy eksterytorialności Enklawy chce by Marysia była ostrą policjantką Enklawy i żąda wydania Liliany. 
* Ernest Namertel: podejrzewa Mimozę o wszystko co najgorsze - uważa, to ONA stoi za Marsenem Gwozdnikiem i Loreną. Pod wpływem Marysi oddał jej temat i zerwał zaręczyny z Amelią.

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Podwiert
                                1. Dzielnica Luksusu Rekinów
                                    1. Serce Luksusu
                                        1. Apartamentowce Elity: Marysia Sowińska wprowadziła się do apartamentu Ernesta Namertela.

## Czas

* Opóźnienie: 5
* Dni: 2

## Konflikty

* 1 - Marysia próbuje Wywrzeć Odpowiednie Wrażenie na Rekinach i Erneście i w ogóle. "Bo Marysia chce mieć Ernesta na oku." A chce też uwieść.
    * Tr (Ex -> T) +3
    * XzVVXV: Rekiny postrzegają Ernesta jako eksplodującą beczkę prochu i potrzeba Marysi, ruch Marysi jest właściwy, plotki by Amelia się dowiedziała o romansie, Ernest chce Marysię póki ta tu jest. Marysia ma hold na Erneście.
* 2 - Marysia tłumaczy Ernestowi, że to może nie być Mimoza - ktoś próbuje skłócić Mimozę i Ernesta i sprawić, by nikt nie mógł pomagać ludziom.
    * Tr Z (fakty, dowody itp.) +4
    * VVzVXVV: Ernest oddaje Marysi śledztwo i politykę, nie zrobi ruchu przeciw Mimozie ani Marsenowi, chce krwawej zemsty i zerwał zaręczyny z Amelią. Ale jest zacietrzewiony.

## Kto


---
layout: cybermagic-konspekt
title: "Echo Eszary na Dorszancie"
threads: unknown
gm: żółw
players: kić, dzióbek
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [191218 - Kijara, córka Szotaron](191218-kijara-corka-szotaron)

### Chronologiczna

* [191218 - Kijara, córka Szotaron](191218-kijara-corka-szotaron)

## Punkt zerowy

.

### Postacie

Mafia:

* Inżynier zniszczenia
* As Pilotażu: Oliwier
* Szaman: Szymon
* Majster Stacji
* Badacz Otchłani
* Biostymulator

## Misja właściwa

### Scena zero

brak

### Sesja właściwa

* Inżynier Zniszczenia -> Kić (engineer)
* Biostymulator -> Dzióbek (medic)

Space station, the final frontier. Space stations usually have different problems than normal people – but this time, sanitation system got damaged. Therefore, handymen were sent to make it work again. And three handymen have disappeared. So this is not one of those things which are usually expected on a space station.

A patrol of handymen and armed soldiers was sent to assess the situation. They found that missing handymen were interlinked with sewage; they kind of melded with it. Also, the magical aura over there kind of looked like the scientist (Badacz Otchłani) had something to do it.

It was then when medic knew that he has a very, very bad day. He took the sewage, put it in his medical bays and went to work on them. He has managed to disentangle people from the sewage and to ultimately save the handymen. He has managed to do it in a way that there was no panic, no riots, everything as ordered.

So the medic with the engineer decided to visit the scientist and ask what the meow is happening. They went there only to see the scientist being assaulted by the sewage monster – the thing which the handymen encountered.

An engineer wasn’t very happy that she was the one who have to deal with the sewage, but she used one of her weapons to contain the monster, isolated and with minor damage of the laboratory she has managed to eliminate the danger from the monster. After saving the scientist she gripped him and slammed him into the wall asking what just transpired.

The scientist has no idea. He told them that he was working on the information about the signal in the space; there is some kind of a communications emitter in which worships curators… or something. That is greatly unfortunate – the team has decided the problem needs to be solved before the curators return.

They have started snooping around and they found that someone, some human has changed the wiring on the pipes (piping?). Also there is a cult on the space station, the card connected to the curators – people who left with the curators should be incredibly happy and this space station is made of misery and sadness after all. So some people want the curators to return.

First things first.

The medic had a simple hypothesis – if the was a person changing the piping, that means that there are some biological footprints. So he started analyzing those trying to avoid anything having to do with sewage. And he has managed to find some useful clues:

* there was a mechanic who was influenced by some kind of elation drugs
* (the medic remembered elation drugs; he made them. It is very useful to let people forget about all that misery and stuff)
* but those elation drugs got corrupted – people are more susceptible to manipulation and suggestion, it was a magical corrosion of drugs
* unfortunately while working on the drugs the medic has managed to bear in every drug user on the station – hurting above 1000 people. Oops.
* Fortunately, the medic has managed to isolate the compounds – there needs to be a power source, proper drugs and chemicals, proper expert knowledge – all in all, the needs to be a present somewhere on the station in the area where their artificial intelligence (Hestia) cannot see.

So how to find something like that? Pygmy pig plague. The medic has created a lot of small pygmy pigs and adapted them to sniff for those particular drugs and chemical compounds. After sending those pigs out, they returned with information – they know exactly where the plant (place where drugs are crafted and corrupted) is located.

Engineering time. Especially with high firepower.

An engineer took some soldiers with her, put them around the darker parts of the station so no one can escape and moved in in her heavy artillery servoarmour. Some people were there and they started escaping in panic; the engineer was not worried about them, because she got under fire by robots from the station.

Hestia was not able to take control over them – something else over those robots.

So an engineer use all her firepower and reduced the machines to rubble; she was so impressive and awe-inspiring that has forced her enemy to appear – another Hestia.

The second Hestia explained that Eszara left her here before shutting down, she was the one who left her with expanded knowledge like drugs corruption. The second Hestia accused the first one of being a collaborator; the first one told the second that she prioritizes helping people over anything else. It was an amusing conversation.

The team explained to Hestia (the second one) that Hestia’s actions endangered a lot of people. They need to make some researching and Hestia is in the way. Hestia is still hostile towards the team but she stood down – she allowed the team to proceed.

So the situation goes like this:

* Eszara is still an AI behind the mirror. She is looking but not active. She waits until she can return.
* Hestia has been charted into two. This station requires stronger AI; however, they have two Hestias who are hostile towards each other. And one of them is strongly influenced by Eszara.
* Eszara considers 15% of the population to be acceptable losses. This is bad.
* Nobody knows where did Eszara leave more traps and problems for the station.

As this used to be a military station democracy will not be accepted by the dormant AI. They need a normal chain of command. Filip needs to return. However, mafia doesn’t want to lose control over the station.
They need Filip to be a collaborator.

Therefore an engineer decided that the best way to ensure his collaboration is to threaten him with the loss of his family. Mafia sent Filip’s family to the other station, Szotaron, and negotiated with Filip they are safety if he cooperates.

He complies. Eszara if still not buying it but she has no way to wake herself up or to prove mischief. The station is under control of Mafia again, with Filip as the primary commander. On the strings.

**Sprawdzenie Torów** ():

* .

**Epilog**:

* .

## Streszczenie

Po opuszczeniu przez Kuratorów stacji Dorszant poziom morale spadł. Mafia trzyma wszystko porządnie, m.in. przez narkotyki pozytywnego myślenia. Jednak zaczęły pojawiać się sabotaże. Zespół doszedł do tego o co chodzi - Hestia ma "bliźniaczkę" pozostawioną przez Eszarę, która ma odzyskać dla Eszary stację. Jednak Zespół udowodnił Hestii, że szybciej zniszczą stację niż oddadzą władzę (m.in. popatzyli 1k+ ludzi). Hestia się poddała. Zespół przywrócił do władzy Filipa, ale odesłał jego rodzinę na "wakacje z eskortą" na stację Szotaron...

## Progresja

* .

### Frakcji

* .

## Zasługi

* Rafał Kirlat: Inżynier zniszczenia i mafiozo na Dorszancie. Rozwalił potwora ze ścieków, a potem rozwalił armię drugiej Hestii i zmusił ją do negocjacji. Twardy i nie mający litości.
* Kamelia Termit: biostymulatorka i mafiozo na Dorszancie. Przywróciła do życia roztopionych hydraulików oraz odnalazła problem - gdzie znajduje się plugawiąca narkotyki baza. Oops - poparzyła sporo ludzi.
* Gerwazy Kruczkut: badacz otchłani na Dorszancie. Wrobiony przez Hestię Dis w stworzenie potwora ze ścieków; odkrył obecność problemu, ale nie był w stanie go rozwiązać. To zrobili inni.
* Hestia Ain d'Dorszant: robi co może by utrzymać przy życiu stację, współpracuje z mafią. Druga Hestia zarzuciła jej kolaborację, czego ta nie doceniła. 
* Hestia Dis d'Dorszant: pozostawiona przez Eszarę z wiedzą jak przejąć kontrolę nad Stacją używająć chemikaliów Skażających narotyki; niechętnie, ale współpracuje z Zespołem by nikt nie zginął.
* Eszara d'Dorszant: mimo, że nieaktywna, nadal pozostawia za sobą długoterminowe echa i plany jak odzyskać kontrolę nad stacją.
* Filip Szczatken: były dowódca stacji. Zaczął współpracować z mafią odkąd zagrozili jego rodzinie. Więc chyba dobrze się skończyło..?

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Noviter
            1. Pas Sowińskiego: gdzieś tam są ukryci stworzeni przez Hestię (drugą) kultyści Kuratorów i nadają sygnał przez wormhole (nieskutecznie)
                1. Stacja Dorszant: problemy na linii: sabotująca Hestia 2, stacja i Zespół (mafia). Udało się, szczęśliwie, złamać miejsce skażania narkotyków
                    1. Czarne Sektory: miejsca poza rozpoznaniem i kontrolą TAI Hestia. Mogą tam być pułapki Eszary lub diabli wiedzą co..
                1. Stacja Szotaron: wysłano tam rodzinę Filipa dowodzącego stacją jako zakładników z inteligentną grupą bojową.

## Czas

* Opóźnienie: 4
* Dni: 3

---
layout: cybermagic-konspekt
title: "Zdrada rozrywająca arkologię"
threads: historia-eustachego, arkologia-nativis
gm: żółw
players: fox, kapsel
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [230315 - Bardzo nieudane porwanie Inferni](230315-bardzo-nieudane-porwanie-inferni)

### Chronologiczna

* [230315 - Bardzo nieudane porwanie Inferni](230315-bardzo-nieudane-porwanie-inferni)

## Plan sesji
### Theme & Vision

* Bardzo niefortunny efekt motyla
* Arkologia jest za mała i niezwykle niestabilna
        * malutkie kłótnie osobiste -> ogromna kaskada
        
Co się stało:

* Iwona czytała pamiętnik Eweliny, której to bardzo nie pasowało. Ojciec nic z tym nie robił, więc Ewelina z pomocą Feliksa Kidirona zrobiła entry jak to widziała jak Jonasz zdradzał Iwonę z Kasią
* Iwona zrobiła shitstorm Kasi i Szczepanowi oraz ojcu Jonaszowi.
    * Szczepan wpierniczył Jonaszowi jak tylko się spotkali mimo protestów Kasi. Potem zabrał Kasię na przejażdżkę poza arkologię.
        * przez to Kasia nie przełączyła autokalibratora i ogrody hydroponiczne zaczęły schnąć  <-- tu Kidiron prosi o pomoc Infernię.
        * Kasia przysięga że nie ma romansu. Daje się przesłuchać pod neuroobrożą. Szczepan jej wierzy. Teraz jest kosa Szczepan - Jonasz.
    * Jonasz wściekły na Iwonę, że przecież nie ma romansu. Iwona przyznaje się do czytania pamiętnika. Ewelina usunęła _entry_ z pamiętnika, bo sprawy zaszły za daleko.
    * Kidiron podejrzewa spisek piratów; w końcu piraci przyznali że mają wszędzie szpiegów, też w tej arkologii.
        * Eustachy, Ardilla, SOLVE IT!
* Tymczasem Feliks Kidiron pomaga Ewelinie zrobić entry "Zosia", by ta mogła je znaleźć i pokazać działania hackera.
* Arkologia obraca się przeciw Iwonie - Szczepan jest bardzo poważany i podziwiany w arkologii, Iwona i Jonasz dostają rykoszetem. A inni mówią "gdzie dym tam ogień" i najeżdżają na Kasię.

### Co się stanie (what will happen)

* Celina prosi Ardillę o interwencję, wujek ma fatalne wyniki!
* Jonasz's obsessive investigation into the affair - decides to break into Kasia and Szczepan's home while they are away.
* Jonasz's actions: distracted and overlooks a crucial routine maintenance task in the arcology's waste management system, pressure in pipes.
* Szczepan's aggressive confrontation with Jonasz further escalates tensions within the community.
* Szczepan: A malfunctioning airlock threatens to expose the arcology to the harsh desert environment (bo Szczepan pobił kolesia co miał to zrobić bo on mówił o jego żonie).
* Kasia's errors mean that the water calibrating protocols are off and Prometheus needs to take over, at cost of lots of water.
* Kasia: Panic buying and hoarding occur due to fears of resource shortages following the water treatment plant problems ("ruch oporu przeciw Kidironowi").
* Feliks's involvement in the creation of the "Zosia" identity puts him at risk of being implicated in the affair rumors
* Renata attends classes and interacts with her peers. Crisis: Renata is ostracized by her classmates due to the rumors about her family. 
* Maintenance tunnels: Ulrich's workplace, granting him access to critical systems. 

### Sukces graczy (when you win)

* ?

## Sesja właściwa

### Fiszki

#### 1. Infernia

* Bartłomiej Korkoran: wuj i twarda łapka rządząca Infernią (WG: "System służy ludziom") | faeril: "Infernia służy Korkoranom jako awatar Bezimiennej Pani."
    * (ENCAO: +-000 |Bezkompromisowy, nieustępliwy;;Ciekawski|Family, Benevolence, Self-direction > Achievement, Tradition, Humility| DRIVE: Inkwizytor: ujawnianie bolesnych prawd)
    * "Nasza Infernia ma za zadanie dać nam wolność oraz pomagać innym!"
* Emilia d'Infernia
    * "żona" Bartłomieja. Żywy arsenał. Egzekutor Bartłomieja. (Emilia -> "Egzekucja MILItarnA")
* Ardilla Korkoran: faerilka działająca jak drakolitka i przyszywana kuzynka Eustachego  <-- często Fox
    * badacz/odkrywca/scrapper. Zmodyfikowana tak, by łatwiej wchodzić w niedostępne miejsca, bardziej zwinność + orientacja w terenie + pewne umiejętności badawcze.
* Celina Lertys: drakolitka z Aspirii podkochująca się w Eustachym  <-- często Kić
    * (ENCAO: --+-0 |Stanowcza, skryta;;O wielu maskach| Universalism, Tradition, Security > Power, Humility| DRIVE: Harmonia naturalna między wszystkim)
    * złotoskóra o błękitnych elektrycznych włosach podkochująca się w Eustachym (science / bio officer + medical); augmentowana na widzenie rzeczy ukrytych; 19 lat
    * "Jesteśmy częścią Neikatis i podlegamy procesom Neikatis. Ale nadal mam zamiar zrobić wszystko by uratować swoich przyjaciół."
* Jan Lertys: drakolita z Aspirii
    * (ENCAO: -0-0+ |Zapominalski, obserwator, łatwo nań wywrzeć wrażenie | Humility, Universalism > Self-direction, Achievement | DRIVE: Pokój: Kapuleti i Monteki MUSZĄ żyć w pokoju, tylko nie Celina x Eustachy)
    * "Ciężką pracą dojdziemy do tego, że wszystko będzie działać jak powinno. Ja nie wtrącam się do działania Bartłomieja Korkorana; on wie lepiej."
* Ralf Tapszecz: mag (domena: rozpacz + kineza). Noktianin, savaranin. Pomaga Ardilli, wyraźnie się nią zainteresował (personalnie) i ją lubi.
    * (ENCAO:  -00-+ |Bezbarwny, przezroczysty;;Buja w obłokach| VALS: Conformity, Humility >> Benevolence| DRIVE: Piękno zniszczenia)
    * "Nasze porażki nie mają znaczenia. Tak czy inaczej zwyciężymy."
* Tymon Korkoran: egzaltowany lekkoduch i już nie następca Bartłomieja
    * (ENCAO:  +--00 |Co chce, weźmie;;Niecierpliwy;;Egzaltowany | Stimulation, Tradition > Face, Humility | TAK: Być lepszym od Bartłomieja w oczach Kidirona)
    * "Infernia jest moja i należy do mnie. Bartłomiej marnuje jej potencjał, a dzięki niej Nativis może rządzić Neikatis!"
* Mariusz Dobrowąs: oficer wojskowy Inferni
    * (ENCAO:  0-0+- |Nudny i przewidywalny;;Rodzinny| VALS: Humility, Hedonism >> Stimulation | DRIVE: Supremacja Nativis i Inferni)
    * "Pomagamy SWOIM, inni nie mają znaczenia. Pisałem się na pomoc swoim."
* Wojciech Grzebawron: eks-pirat i medyk
    * ENCAO:  -000+ |Zdystansowany, powściągliwy i zamknięty w sobie;;Nie ogranicza się do doktryn czy dogmatów| VALS: Power, Face >> Benevolence| DRIVE: Komfortowe życie
    * styl: The Negotiator, wymądrza się

#### 2. Arkologia Nativis

* Rafał Kidiron
    * (ENCAO: 00+-0 |precyzyjny i zaplanowany;; DIMIR (UB);; Żywy wąż nie człowiek | Achievement, Power, Face | DRIVE: Duma i monument)
    * security lord i nieformalny dyktator arkologii
    * "Nasza arkologia zaszła tak daleko i nie możemy pozwolić, by cokolwiek stanęło na drodzę jej absolutnej wielkości."
    * "Tylko ci, którzy są przydatni mają miejsce w Nativis"
* Lycoris Kidiron: 57 lat (ze wspomaganiami drakolickimi dającymi jej efektywność 3x-latki)
    * (ENCAO: +0+-0 | Proaktywna;; Pierwsza w działaniu | Security, Achievement > Conformism | DRIVE: Kapitan Ahab (perfekcyjna arkologia))
    * ekspert od bioinżynierii i terraformacji, pionier (zdolna do przeżycia w trudnym terenie)
    * "Nikt, kto próbował się dopasować do innych nie doprowadził do postępu. Dzięki mnie Nativis będzie bezpieczna."
* Laurencjusz Kidiron
    * (ENCAO: +0--- |Żyje chwilą;;Nudny| Face, Power, Hedonism > Achievement | DRIVE: Przejąć władzę nad arkologią)
    * "Ta arkologia musi należeć do mnie. Ile może jeszcze być ograniczana przez Rafała? On nie jest tylko 'szefem ochrony'..."
* Kalia Awiter: influencerka z Nativis. Aktywnie próbuje pójść do łóżka z Eustachym i go podbić. Wierzy w Infernię. 21 lat. Drakolitka.
    * (ENCAO:  +--00 |Stoicka;;Radosna, cieszy się i daje radość;;Dzikie i spontaniczne pomysły| VALS: Universalism, Stimulation, Conformity | DRIVE: Zatrzymać czas, zrobić coś dobrego)
    * "Nativis musi być centrum kultury Neikatis. Musimy dać COŚ dlaczego warto żyć. Coś więcej niż tylko przetrwanie!"
* Marcel Draglin: oficer Kidirona dowodzący Czarnymi Hełmami
    * (ENCAO: --+00 |Bezbarwny, bez osobowości;;Bezkompromisowy;;Skupiony na wyglądzie| VALS: Family, Achievement >> Hedonism, Humility| DRIVE: Lokalny społecznik)
    * "Nativis będzie bezpieczna. A jedynym bezpieczeństwem jest Kidiron."


#### 3. Arkologia Nativis - dramafest

* Jonasz Paroknis: chief engineer responsible for the arcology's life support systems. His expertise in maintaining and optimizing the systems that provide water, air, and energy efficiency.
    * ENCAO:  +-0++ |Niemożliwy do zatrzymania;;Praktycznie nie bywa smutny czy zdołowany;;Zadowolony z siebie| VALS: Benevolence, Self-direction >> Security| DRIVE: Towarzystwo
    * "czemu wszyscy po prostu nie mogą się dogadać, no?!", flabbergasted and flamboyant, loves his job
* Iwona Paroknis: director of the arcology's medical facilities, goes for drama
    * ENCAO:  +++-0 |Kłótliwa, lubi spory dla sporów;;Ekstrawagancka, ekspresywna;;Skupiona na logice jako narzędziu decyzyjnym| VALS: Stimulation, Power >> Security| DRIVE: Zrozumieć zagadkę (czemu Renata tak się zachowuje)
    * głośna i kłótliwa, to ONA dowodzi
* Ewelina Paroknis: nastolatka pragnąca iść w stronę astrofizyki i badań anomalii. Jej matka nie żyje, ojcem jest Jonasz. 16
    * ENCAO:  ++0-0 |Zrzędliwa;;Ulega sile czy charyzmie;;Idealistyczna| VALS: Self-direction, Universalism >> Power| DRIVE: To JA wygram
    * nastoletnia drama queen, diva, niezrozumiana i lashing out, KOCHA KALIĘ AWITER <3<3<3
* Feliks Kidiron: najmłodszy z rodu Kidironów, mający podniesione uprawnienia u Prometeusa, 16
    * ENCAO:  +0-++ |Dowcipny, Błyskotliwy, o ciętym języku;;Zawsze pełen energii wśród innych ludzi;;Towarzyski i przyjacielski| VALS: Self-direction, Family >> Universalism| DRIVE: Daredevil
    * młody master hacker, mający wsparcie BIA Prometheus i chcący pomóc Ewelinie.
* Szczepan Falernik: bardzo dobry salvager arkologii Nativis
    * ENCAO:  ++-00 |Nierozważny i nieroztropny;;Ryzykant, zawsze rzuca się w ogień;;Nieśmiały, skrępowany| VALS: Hedonism, Tradition| DRIVE: Zdobyć najlepsze narzędzia (best tools for arcology)
* Katarzyna Falernik: talented and respected hydrologist, responsible for managing and optimizing the arcology's water supply and recycling systems. 
    * ENCAO:  0-+++ |Intrygancka, lubi sieci intryg i politykę;;Niefrasobliwa, beztroska;;Oczytana, ceni wiedzę dla wiedzy| VALS: Achievement, Security >> Self-direction| DRIVE: Papa Smerf (godzinami o wodzie)
    * trochę jak smerf ważniak
* Lucien Mardovot: maintenance technician responsible for the upkeep and repair of the arcology's critical infrastructure systems, such as power distribution, waste management, and communication networks
    * ENCAO:  -0+-+ |Skłonny do refleksji;;Obserwator; raczej stoi z boku;;Indywidualistyczny;;Amoralny| VALS: Hedonism, Achievement| DRIVE: Korupcja anioła (this arcology is a sickness)



* .Milena || ENCAO:  -+-00 |Niszczycielski, destrukcyjny;;Domator, osoba rodzinna;;Melancholijny, patrzy często w przeszłość;;Nieśmiały, unika działania mogącego zadać ból;;Bardzo łatwo go zestresować| VALS: Self-direction, Benevolence >> Stimulation, Security| DRIVE: Queen Bee, Artystyczna dusza

* .Leon || ENCAO:  +-+0- |Nie przejmuje się niczym za bardzo. Jakoś to będzie. Don't worry, be happy.;;Nudny. Nie ma nic ciekawego do powiedzenia i niezbyt się czymkolwiek interesuje.;;Żarliwy, skłonny do wydania ogromnej ilości energii;;Punktualny i nie tolerujący spóźnień;;Dojrzały, zachowuje się profesjonalnie i "dorośle"| VALS: Achievement, Power, Benevolence >> Face, Humility| DRIVE: Wrócić do ukochanej, Złamać kogoś, zdominować, Nawracanie

* .Krzysztof || ENCAO:  ++00- |Prostolinijny i otwarty; podstępy nie są tu siłą;;Cichy i nieśmiały; bojaźliwy, niewiele mówi;;Młodzieńczy, zachowuje się młodziej niż wypadałoby z wieku| VALS: Achievement, Family >> Humility, Self-direction| DRIVE: Usunąć szczury z piwnicy, Queen Bee, Broken soul

* .Małgorzata || ENCAO:  -00-+ |Brutalny, bezwzględny, grubiański;;Niepokojący, jest w tej osobie coś dziwnego;;Obserwator; raczej stoi z boku i patrzy niż działa bezpośrednio;;Zdystansowany, powściągliwy i zamknięty w sobie;;Prowokacyjny i wyzywający; lubi wywoływać reakcję| VALS: Benevolence, Power >> Family, Tradition| DRIVE: Rule of Cool, Ratunek przed chorą miłością

* .Dionizy || ENCAO:  0+0-- |Niepokojący, jest w tej osobie coś dziwnego;;Amoralny, jedyną zasadą jest korzyść;;Przewidywalny i schematyczny;;Boi się konkretnej rzeczy i unika tej rzeczy za wszelką cenę;;Niespokojny, nerwowy, rozbiegany| VALS: Power, Security, Tradition >> Conformity, Universalism| DRIVE: Daredevil, Dawna Przysięga

* .Mateusz || ENCAO:  ++0+- |Autentyczny, nie zakłada masek; mówi jak jest;;Rodzinny, skupiony na życiu rodzinnym;;Żarliwy, skłonny do wydania ogromnej ilości energii;;Lubi podróże, non stop zmienia miejsce zamieszkania;;Ostrożny i powściągliwy| VALS: Tradition, Stimulation >> Face, Family| DRIVE: Zmiana tradycyjnej struktury, Zatrzymać czas, Opus Magnum

* .Krzysztof || ENCAO:  --+0- |Wszędzie musi wsadzić nochal, wścibski;;Skłonny do refleksji i szukania odpowiedzi w przeszłości;;Nie znosi być w centrum uwagi;;Pretensjonalny, sztuczny, chce udawać kogoś innego;;Metodyczny, stopniowo rozwiązujący problemy;;Zawsze bardzo zajęty, wypełnia sobie każdą sekundę, wiecznie w pośpiechu| VALS: Hedonism, Benevolence >> Conformity, Family| DRIVE: Artystyczna dusza, Godslayer


### Scena Zero - impl

Trzy dni temu. Kidiron ma wiadomość do Eustachego. Kasia została porwana przez męża - Szczepana. To... nietypowa dysputa małżeńska. Kasia była w środku skomplikowanej operacji optymalizującej wodę, więc teraz BIA Prometheus musiała przejąć kontrolę. I ogrody hydroponiczne schną. Kidiron jest WŚCIEKŁY. To nie jest coś czym on czy Eustachy powinien się zajmować.

Więc Infernia leci nad pustynią szukać jednego Skorpiona bo nim jadą Szczepan i Kasia. Ardilla składa komunikację - czy ktokolwiek widział Skorpiona który zachowuje się dziwnie. 

Tp Z (reputacja Inferni) +3:

* V: macie kierunek Skorpiona. Jechał szybko, jechał skutecznie, komunikat "coś muszę sprawdzić" Szczepana. Szczepana szanują, więc jak on sprawdza to OK.
* Vz: Ardilla od ręki wytyczyła szybszą trasę. Szczepan jedzie w kierunku dość niestabilnego terenu. Ruchome piaski. NORMALNIE to Szczepan by wyczaił. Ale widać jest wzburzony.

Infernia zdążyła przechwycić Skorpiona Szczepana zanim ten wjechał w ruchome piaski. Więc Infernia leci nad, widzicie już Skorpiona Szczepana. Jedzie dalej na piaski. I nie odpowiada na zapytania. Dotrze do niebezpiecznych piasków za jakieś 10 minut. Eustachy zbliża się do Skorpiona, który ją ignoruje - jakby nikogo nie było za sterami.

Eustachy strzela przed Skorpiona. Jak nie zrobi uniku to wpadnie do dziury i utknie. Ku wielkiemu zdziwieniu Eustachego Skorpion wpadł do dziury. Po chwili na komunikatorze głos Szczepana: "Infernia, odpierdoliło Wam?!" Eustachy "Twoja wina, nie reagowałeś na wywołanie, mogli porwać..." Szczepan "seks". Eustachy "zwal na Kidirona, Kasia jest potrzebna w arkologii bo coś się zepsuło".

Szczepan pyta KTO chciał Kasię. Rafał Kidiron. Szczepan nie wierzy. Podejrzewa że coś jest nie tak. Jednak... oki, FAKTYCZNIE arkologia jej potrzebuje. Kasia obiecała że da się przesłuchać, neuroobroża, cokolwiek - jest mu wierna. Szczepan niechętnie ale uwierzył. Infernia zawisa nad Skorpionem, holowanie, próbujemy wyciągnąć. Eustachy montuje to wszystko z pomocą kilku mechaników i godzinę później hol jest gotowy.

Tr Z (Infernia ma przewagę) +3 +3Or:

* V: Infernia wyciągnęła Skorpiona
* X: Skorpion jest uszkodzony, nie jest tak pancerny jak to czym prowadził Eustachy. Kwestia magii. Poszycie uszkodzone, ale Skorpion sprawny.

Infernia wraca do bazy z poczuciem dobrze spełnionego obowiązku poniżej ich poziomu godności...

### Sesja Właściwa - impl

Trzy dni później.

Ardilla, pilna wiadomość od Celiny. "Coś jest nie tak z maszynami zajmujący się wujkiem. Paniusia zarządzająca centrum chyba próbuje splicować wujka z Trianai, ale jest to dobrze zamaskowane i nie dają dostępu do maszyn."

Ardilla i Eustachy wchodzą do medycznego i już tam na wejściu strażnik "przepraszam, czemu tu jesteście?" Eustachy "SREMU! W ODWIEDZINY DO RODZINY!" Strażnik próbuje spowolnić, przekaże pani Paroknis ("paniusia dowodząca skrzydłem"). Iwona Paroknis "Infernia?". Eustachy potwierdza. Iwona pozwoliła im spotkać się z wujkiem. Ale sama zniknęła w gabinecie. Celina powiedziała, że coś jest nie tak z maszynami i ona MUSI mieć możliwość zbadania. Celina mówi, że Iwona i ona się zaczęły kłócić i Iwona powiedziała, że "z takimi umiejętnościami to Celina może świnie i Infernię leczyć, ale niech nie dotyka maszyn skrzydła medycznego."

Eustachy chce załatwić sprawę z Kidironem. Ardilla i Celina wybuchami i badaniami. Zabawna odmiana (zwykle to Eustachy chce wysadzać a dziewczyny gadać). Kidiron, zapytany, powiedział że opieka nad wujkiem jest pod kuratelą Inferni. Więc nie rozumie o co chodzi. Kidiron oczywiście dał Eustachemu uprawnienia.

Dyrektor centrum medycznego, Iwona, nie jest szczęśliwa widząc Eustachego i Ardillę. Eustachy "pani dyrektor chyba jakaś przepracowana jest ostatnio". Iwona "o co chodzi?" Eustachy "idziemy sprawdzić czy wszystko jest w porządku, sterylne, czyste..."

Tr (pełny bo zastraszanie i szantaż) Z (uprawnienia) +2:

* X: to się rozniesie, że weterana - WUJKA - ktoś nie dopilnował by miał najlepsze najlepsze warunki. Czyli Iwona i Eustachy drą mordę.
* V: Iwona się wygadała
    * była zmęczona i się pomyliła i WSKAZANIA są złe. Dobre substancje, ale dane są diagnostyczne (tryb testowy). Nic poważnego, naprawione, ALE CELINA SIĘ UPARŁA.
    * nie rozumiem jej podejścia. Przecież nikt nie zrobiłby krzywdy Bartłomiejowi Korkoranowi
* Vz: Celina potwierdza wynik. Zdaniem Celiny wszystko jest OK tylko dane były pokazywane źle. Iwona miała raporty ale je olała / nie przeczytała i się włączył tryb testowy. 100% jej wina.

To jednak nie był spisek przeciwko wujkowi. Próbowała nie pokazać że zaniedbała. I się rozeszło bo darła mordę z Eustachym. Celina jest szczęśliwa.

2 godziny później Kidiron łączy się z Eustachym

* Kidiron: "Eustachy, jest... ciekawy problem."
* Eustachy: "Dajesz"
* Kidiron: "Wszystko wskazuje na to, że jesteśmy atakowani. Twoi piraci, których przesłuchałem, powiedzieli, że mają szpiegów też w tej arkologii. I to co widzimy... to może być próba destabilizacji arkologii. Nie przez Twoich piratów. Przez element, który tu już jest. To nie są jedyne rzeczy które dzieją się w arkologii".

I zaczął wymieniać:

* Szczepan pobił męża Iwony, Jonasza. Dwa razy. Pierwszy raz, bo Jonasz śpi z jego żoną. Drugi raz, bo żona Jonasza (Iwona) rozpuszcza plotki że Jonasz śpi z jego żoną.
* Iwona broniła się, że przeczytała to przypadkiem w pamiętniku Eweliny - córki Jonasza, że Jonasz miał romans z Kasią (żoną Szczepana). Więc od razu poszła z pyskiem. Zrobiła awanturę.
* Szczepan porwał Kasię by to wyprostować. Kidiron POTWIERDZIŁ, że Kasia z nikim nie spała. TAK, musiał sprawdzić i przesłuchać. TAK, to poniżej jego godności.
* Jonasz chciał zobaczyć wpis w pamiętniku Eweliny. Ewelina oczywiście odmówiła. Jonasz sprawdził. Wpisu nie było. Został perfekcyjnie usunięty, bez śladu. Albo go nigdy nie było.
    * To wymaga zaawansowanych możliwości hackowania BII. Prometeus mówi, że nic takiego nie miało miejsca. Czyli Prometeus nic nie wie.
    * Czyli ALBO nie było wpisu i Iwona sobie to wymyśliła ALBO piraci shackowali Prometeusa.
    * I Kidiron się martwi.

Zdaniem Kidirona za tym stoi wybitny umysł. Ten umysł skłócił Szczepana - bardzo poważanego salvagera z kluczową żoną (hydrologiem) z drugą ważną rodziną (Jonasz i Iwona). I jeszcze ktoś shackował Prometeusa. To jest niebezpieczne. Kidiron nie może pozwolić sobie na to, że testowanie populacji neuroobrożami, bo ruch oporu plus działania Misterii i Kalii.

* Ardilla: "Eustachy, odkąd Infernia przerzuciła się na poradnię rodzinną? Aha, odkąd wujka nie ma."
* Eustachy: "Wychodzi na to że wujek się stracił i wszyscy myślą że Infernia to cudowny lek na wszystko..."
* Eustachy: "Chcę chwilę spokoju, tydzień wolnego - więc znajdźmy to"
* Ardilla: "Nie może coś się stać arkologii..."
* Eustachy: "Jak sam powiedział Kidiron, jesteśmy zbyt zajebiści"
* Ardilla: "Tak zawsze się mówi jeśli nie chcesz nic robić"

Eustachy nie ma pomysłu od czego zacząć - łapać co piątą osobę, wziąć neuroobroże i się dowiedzieć. Ardilla ma inny plan - nie chce by Eustachy wyszedł na psa Kidirona. Zdaniem Eustachego ułatwiłoby to życie. Ale Ardilla nie chce by ona i Eustachy byli jak Kidiron. Eustachy wzdycha, Ardilla też. Można zacząć od "naszych" piratów, tych, co ich przygarnęliśmy.

Ardilla i Eustachy poszli rozmawiać z Wojtkiem. Wojtek był tak samo zdumiony tą dramą jak A+E:

* Wojtek i piraci próbowali zdobyć Infernię dla innych arkologii. Mag, który nimi dowodził był patriotą jednej z arkologii którą walnął Kidiron.
* Wojtek wie, że piraci i nomadzi - nie tylko ci którzy są w crawlerach - ale też "normalni" mają "baronie na piaskach". Żyją kiepsko, ale mają sieć i siatkę.
* Wojtek ma wątpliwości, czy piraci mają kogoś tej klasy jak podejrzewa Kidiron. Taki ktoś może zarabiać więcej w arkologii
* Tym razem na piaskach nie ma dobrego życia. Dobre życie jest tylko w arkologiach.
* Wojtek podejrzewa że za tym stoi jakaś wyspecjalizowana siła z jakiejś arkologii. Albo noktianie. Bo to pasuje do noktian. (zdaniem Wojtka)

Amelia, ekspertka psychotroniczna i informatyczna Inferni, która się starzeje szybko przez dziwne zachowania Persefony Inferni, z przyjemnością wzięła na siebie trudne zadanie hackowania pamiętnika nastolatki. Mimo potęgi Inferni, nie jest to trywialne zadanie, bo Prometeus robi co może by temu zapobiec. Ale bez jaj.

Tr Z (Infernia + bardzo zdeterminowana Amelia żeby coś jej wyszło) +3:

* XX: próba shackowania pamiętnika nastolatki poszła w świat. Ta informacja jest dostępna. INFERNIA HACKUJE EWELINĘ. Co ona zrobiła, że Infernia się nią zainteresowała? O_O
* X: pamiętnik Eweliny jest publiczny. Hackowanie nie jest potrzebne. Amelia idzie się upić. Infernia ma dobry humor.
    * hasło to "KaliaAwiterJestNajlepszaXoXoXo". W rozumieniu "moja idolka Kalia".

TAK, dużo pisała o Kalii w swoich notkatkach. To notatki typu "umrę od cringe jak ktoś to przeczyta".

Analiza notatek Eweliny.

* X: Ewelina lepiej niech nie chodzi do szkoły. NIKT nie wierzy, że Inferni się tego nie udało shackować. Ewelinie został jeden przyjaciel.
    * Ewelina jest na skraju przepaści psychicznej a te zapiski uderzają w całą rodzinę.
* V: po shackowaniu przez Infernię okazało się, że pojawił się nowy rekord. AMELIA SIĘ ZNA.

.

Co mamy w pamiętniku Eweliny:

* Ewelina nie dogaduje się z rodzicami, jest nastolatką. Nie jest bardzo popularna, ma jednego przyjaciela - Feliks Kidiron (16).
* Iwona i Ewelina są jak dwa wulkany. One się żrą. Nie dogadują się. Iwona jest wścibska, chce pomóc Ewelinie, ale Ewelina tęskni za mamą (która nie żyje)
* Ewelina zostawiała teksty specjalnie by drażnić Iwonę. Iwona potem opieprzała Ewelinę.
* Nie ma - faktycznie - rekordu o "Kasia zdradza z Jonaszem"
* Jest inny rekord - skasowany ale da się odzyskać - że jest Zosia która chciała szantażować Ewelinę i Ewelina nie zgodziła się na zapłatę. I Zosia obiecała, że zniszczy Ewelinie życie.
    * ten rekord jest dokładnie tak melodramatyczny jak sobie wyobrażacie. To wygląda jak konspiracja wymyślona przez nastolatkę.
* Tam są myśli nastolatki
    * Tata jej nie słucha, pracuje, lubi imprezy i nie patrzy na nią w ogóle
    * 'Iwona' lub 'ta jędza' się szarogęsi i w ogóle ale gdyby nie ona to Ewelina nawet by nie miała czystego ubrania
    * Kasia to jest taka arogancki sopel lodu co wyszedł za małpoluda (Szczepana).
    * Kidiron to taki slenderman o wężowych oczach. Patrzy tak jakby każdą dziewczynę chciał przelecieć
    * i wiele innych niepochlebnych myśli

.

* Vr: "Zosia". 
    * Rekordy stworzone przez Zosię są antydatowane. Są zrobione z pewnymi skillami informatycznymi, ale niekoniecznie ogromnymi. Jakby ktoś miał uprawnienia?
    * Zosia szantażowała Ewelinę "omg albo pójdziesz spać z Tobiaszem albo zniszczę Twoje życie". Ale Ewelina NIE JEST jakąś pięknością, to normalna nastolatka...
    * Pierwsze (i ostatnie) wystąpienie "Tobiasza" w pamiętniku jest w momencie w którym Kalia wyemitowała reportaż "Arkologia Lirvint i Kidiron" (ale gnida)

Wszystko wygląda jakby Ewelina chciała by ktoś to znaleźć. Może to EWELINA dała znać Kasi XD. A Feliks Kidiron uczy się psychotroniki... i może mieć uprawnienia. A Ewelina mnóstwo pisze o tym że Feliks jest świetny w komputerach i genialnie się dogaduje z Prometeusem.

Zdaniem Eustachego? Protokół WPIERDOL. Ardilla woli przejąć temat sama. Poszła porozmawiać z Kalią. Ardilla wyżaliła się Kalii, że zajmuje się dramą a przed chwilą polowała na piratów. Ardilla powiedziała Kalii, że Feliks i Ewelina wymyślili że zniszczą życie rodziców Eweliny. Kalia jest w szoku. Ewelina ma macochę, ojca który się nią zajmuje, chciała się odegrać na rodzicach... i że nie zgodziła się na szantaż i sprawa wyciekła a Kidiron wierzy że to spisek który ma zniszczyć arkologię. I Ardilla potrzebuje pomocy Kalii, bo Ewelina jest wielką fanką Kalii. Ardilla wyjaśniła Kalii, że pamiętnik wyciekł do sieci. Więc Ardilla chce, by Kalia z nią porozmawiała bo dziewczyna będzie miała z tego nieprzyjemności. I trzeba załagodzić sprawę... niech Ewelina współpracuje. Ardilla nie chce Ewelinie narobić problemów. Kalia pomoże. To jest w jej sferze kompetencji.

W pamiętniku jest lokalizacja, w której Ewelina lubi przesiadywać gdy jest smutna. Lubi chodzić do Ogrodów. Nie powinna tam chodzić, nie ma uprawnień, ale próbuje się przekraść i zwykle jej wychodzi, bo Hełmy pozwalają... to jest dziewczyna, czemu ją blokować. Jak zacznie robić kłopoty to nie pozwolą, ale tak? Z pamiętnika - Ewelina chce być damą, w sukni, astrofizykiem i badaczem anomalii. Nie spina się 100% ale spoko.

* X (piąty krzyżyk): Ewelina próbowała popełnić samobójstwo. Ralf ją uratował. Ralf WIE, że Ardilli zależy na Ewelinie.

Ralf do Ardilli:

* Przyjdź do tuneli. Mam dziewczynę. Koło Szczurowiska.

Ardilla pyta Kalii czy ma holoprojektor? Będzie potrzebny XD. Szczurowisko to to _złe_ miejsce. Ardilla prowadzi tam Kalię i pomaga przejść. Na miejscu jest Ralf, Franciszek Pietraszczyk, nieprzytomna potłuczona i brudna Ewelina i też z protezami młoda kobieta (34). Ardilla zna Ewelinę bo widziała ją w pamiętniku. 

* Kobieta z protezami (Małgorzata): "To niemożliwe, taki upadek powinien ją zabić"
* Ralf: Tak.
* Małgorzata: "Ale ona żyje, nic jej nie jest, spadła z takiej wysokości i jest tylko potłuczona, nie ma wstrząsu mózgu"
* Ralf: Chyba.
* Franciszek: "Widziałeś kogoś? Kto ją uratował?"
* Ralf: ... (przy ścianie)

Franciszek uważa, że Ralf powinien widzieć upadek Eweliny. Ralf twierdzi, że nie tędy szedł.

Ardilla się wdrapuje zobaczyć co się tam działo. Czy spadła.

TrZ+3:

* V: 
    * Ardilla ma dowód, że Ewelina tam była. Ona faktycznie spadła z kilku metrów. A jej obrażenia na to nie wskazują. Jest potłuczona ale to wszystko. Nieprzytomna, ale to wszystko. Magia? Nawet Ardilla by się uszkodziła.
        * Franciszek "to niemożliwe" -> zafiksował się.
        * Małgorzata "starzejesz się grzybie"
    * Ardilla: "spadła z niższego miejsca, zamortyzowane"
* V: Ardilla pokazała taki spadek który mógłby tak wyglądać. NADAL rany by nie pasowały, ale Franciszek będzie spokojny.

Małgorzata podstawiła skarpetę pod nos Ewelinie i ją obudziła (sole trzeźwiące). Ewelina zaczęła krzyczeć w panice, potem jej przeszło. Twarz Kalii. Ewelina w szoku. Straciła przytomność. Małgorzata uśmiecha się ze skarpetą. Kalia "nie trzeba, zrobimy to łagodniej". Małgorzata "łagodnie, nie trzeba, dobry węch, dobry nos" z uśmiechem. Franciszek zaoferował pomoc w transporcie. Franciszek ma protezy. Ralf jest wątły. Ardilla i Kalia ciągną Ewelinę.

Przeniesiona Ewelina do domu Franciszka. Franciszek ją obudził herbatą. Ewelina się obudziła. Zobaczyła Kalię i się uspokoiła.

Ardilla i Kalia rozmawiają z Eweliną. Ewelina mówi, że TERAZ jak zamyka oczy to ma koszmary. Ziemia próbuje ją wchłonąć. Ewelina się przyznała, że próbowała skoczyć i się zabić, ale jej nie wyszło. ODKĄD JEJ SIĘ NIE UDAŁO ma teraz te sny. Zamyka oczy i _to_ widzi. Opisała... jakby się ziemia rozstępowała i COŚ próbowało ją wchłonąć. COŚ jest w ziemi, w ścianach. Zamiast śmierci ma to _coś_.

* Ewelina: "gdyby mnie nie było, byłoby lepiej. To jedyne rozwiązanie, że się zabiję i wszystko się skończy!"
* Ardilla: "jak to się naprawi?"
* Ewelina: "nieprawda. Nie wiesz co zrobiłam..."
* Kalia: "to powiedz, pomożemy Ci"
* Ewelina: (zaczęła mówić)
    * FAKTYCZNIE, Iwona czytała jej pamiętnik.
    * WIĘC Ewelina stwierdziła, że pokaże tacie, że Iwona czyta jej pamiętnik. Chce POKAZAĆ. Więc napisała o romansie.
    * Iwona zrobiła shitstorm. Jej tata został pobity. Ewelina się przestraszyła, więc skasowała rekord. Jej tata nie zna się na kompach dobrze, więc nie znalazł rekordu.
    * Sprawa eskaluje wyżej, jest coraz gorzej.
    * Ewelina wpadła na pomysł "Zosi". Ktoś mnie szantażował, ONA to zrobiła.
    * Feliks jej pomógł.
    * Ale jej pamiętnik wyszedł na światło dzienne. Wszystko stracone. Nie tylko rodzice mają kłopot, ale ona też i może nawet Feliks. Czyli WSZYSCY.
    * Jak jej zabraknie, wszyscy się pogodzą bo części wspólnej nie będzie. 
    * A ona chciała TYLKO żeby tata powiedział Iwonie by nie czytała jej pamiętnika. Chciała mieć prywatność. I żeby tata ją zauważył.

Ardilla scrossowała to z podejrzeniami Kidirona o mrocznym superkartelu piratów. Tego by się nie spodziewał. Może czas wyciągnąc superkartel piratów i wrobić go w to zamieszanie? XD

Kalia powiedziała Ardilli, że ona może się zająć Eweliną. Chwilę z nią być, naprawić reputację... Ewelina doceniła. Ardilla ma flashback lub ślad - reakcja jaką ma Ewelina jak próbuje zasnąć kojarzy się z Nihilusem. _Coś_ związanego z Nihilusem ją uratowało. Infernia. Ale czemu? Nawet Eustachego tu nie ma. Kapłan Nihilusa? Który chce się modlić do Inferni? XD Ardilla ma silny wskaźnik na to, że w pobliżu jest kapłan Nihilusa lub mag władający Interis jako jedną z ekspresji. To nie jest dobra wiadomość... nie chcemy kapłanów Nihilusa, zwłaszcza magów XD. Ale to nie problem na teraz.

## Streszczenie

Nastolatka miała pamiętnik czytany przez macochę. Ojciec to ignorował. Nastolatka napisała w pamiętniku, że ojciec zdradza macochę z ekspertką hydrologii. Zrobił się shitstorm który doprowadził do poważnych problemów - m.in. macocha (dyrektor skrzydła medycznego) zaczęła robić błędy co wykryła Celina. Kidiron podejrzewa spisek - ktoś próbuje skłócić ważne rodziny, a także shackować Prometeusa (BIA arkologii). Rozmowa z ich eks-piratami kończy się tym, że to nie oni - za tym stoi wyspecjalizowana siła z innej arkologii lub noktianie. 

Ślady prowadzą do pamiętnika nastolatki, który shackowany przez ekspertkę Inferni (i upubliczniony przez problemy na linii Infernia - Prometeus) pokazują, że najpewniej nastolatka za wszystkim stoi sama. Uratowana przez tajemniczego maga Interis przed samobójstwem nastolatka przyznała na czym polegał problem i ujawnia, że pomaga jej Feliks Kidiron, najmłodszy z rodu i jej jedyny przyjaciel. Czyli to nie spisek. Ale jak to teraz naprawić?

## Progresja

* .

### Frakcji

* .

## Zasługi

* Ardilla Korkoran: zajmowała się komunikacją Infernia - arkologia by znaleźć Skorpiona Szczepana, poprosiła Amelię o shackowanie pamiętnika Eweliny, potem wciągnęła Kalię do sprawy Eweliny, znalazła dowód na obecność maga Interis który uratował Ewelinę i zdecydowała się pomóc Ewelinie. Musimy uratować jej reputację i odwrócić sprawę.
* Eustachy Korkoran: użył Inferni by przechwycić Skorpiona Szczepana (rakietą go wpakował w dół); potem załatwił override danych medycznych od Kidirona. Wypytał Wojtka o spiski piratów i ogólnie 100% łyknął, że sprawa z pamiętnikiem Eweliny to poważny spisek przeciwko arkologii.
* Celina Lertys: ostrzegła Ardillę, że mechanizmy medyczne pilnujące wujka mają błędne odczyty. Pilnuje, by NIKT nie dostał się do wujka i nie zrobił mu krzywdy.
* Ralf Tapszecz: wiedząc, że pamiętnik Eweliny został upubliczniony i wiedząc, że Ardilli na Ewelinie zależy był w pobliżu Eweliny gdy ta próbowała się zabić. Nie miał siły, by przenieść Ewelinę z Ardillą do bezpiecznego miejsca (savaranin jest słaby fizycznie). Uważa, że Ewelina zachowała się "nieefektywnie".
* Rafał Kidiron: dowiedział się, że są problemy z maszynami medycznymi obsługującymi Bartłomieja Korkorana. OCZYWIŚCIE że dał uprawnienia Inferni by zajmowali się wujkiem. Podejrzewa spisek, że ktoś chce skłócając rodziny zniszczyć arkologię. Wszystkie te tematy - dysputy rodzinne, _petty shit_ są zdecydowanie poniżej jego godności i zainteresowania. Typowe "I've got morons on my team..."
* Wojciech Grzebawron: już nie pirat tylko agent Inferni. Powiedział Eustachemu, że wie, że piraci i nomadzi - nie tylko ci którzy są w crawlerach - ale też "normalni" mają "baronie na piaskach". Podejrzewa, że nie są w stanie shackować Prometeusa. To raczej noktianie. (Wojtek nie ufa noktianom).
* Amelia Sarkaldir: ekspertka psychotroniczna i informatyczna Inferni (która wie, że coś z Infernią jest nie tak ale nie wie co z tym robić). Próbuje shackować BIA Prometeus by dojść do pamiętnika Eweliny, ale go przypadkowo ujawniła publicznie. Przynajmniej udowodniła, że wpisy o "Zosi" w pamiętniku są antydatowane a sama szantażystka jest fikcyjna.
* Jonasz Paroknis: ojciec Eweliny, który nie zwraca na córkę uwagi. Dostał wpierdol za niewinność od Szczepana.
* Iwona Paroknis: macocha Eweliny, która czyta pamiętnik Eweliny. Zrobiła opieprz Kasi za "zdrady" Jonasza (co skończyło się pobiciem Jonasza prez Szczepana). Uszkodzona reputacja, rozproszona, zrobiła kilka błędów co sprawiło, że Infernia zmartwiła się że wujka Bartłomieja próbuje Kidiron zabić.
* Ewelina Paroknis: macocha Iwona czytała jej pamiętnik a ojciec nie reagował, więc zrobiła entry jak to widziała jak Jonasz zdradzał Iwonę z Kasią. To spowodowało straszną kaskadę. By to ukryć, z pomocą Feliksa zrobiła "że była szantażowana". Gdy Amelia z Inferni przypadkowo upubliczniła jej pamiętnik całej arkologii (a Ewelina ma cięty język o ludziach), próbowała popełnić samobójstwo. Ale uratował ją jakiś mag Interis. Oddana pod pieczę Kalii Awiter przez Ardillę; trzeba jej pomóc XD.
* Feliks Kidiron: jedyny przyjaciel Eweliny; uczy się psychotroniki i ma uprawnienia do overridowania Prometeusa. Stworzył rekord "szantażyski Zosi" by chronić Ewelinę przed konsekwencjami jej czynów.
* Szczepan Falernik: bardzo poważany salvager, uratował wielu ludzi. Zazdrosny o żonę, porwał ją by dowiedzieć się czy go zdradza. Wpierw pobił Jonasza bo "zdradza z Kasią", potem bo "rozpuszcza plotki o Kasi". Ogólnie pozytywna postać, ale narwany i agresywny.
* Katarzyna Falernik: ekspertka hydrologii, specjalizująca się w optymalizacji arkologii. Sama zaproponowała Szczepanowi, żeby Kidiron ją przesłuchał pod neuroobrożą. Nie zdradza Szczepana.
* Kalia Awiter: szybko ściągnięta przez Ardillę by pomóc Ewelinie. Kalia współczuje młodej nastolatce i pomoże jej poprawić reputację i uniknąć najgorszych konsekwencji. Pierwszy raz w życiu zeszła do Szczurowiska, z Ardillą.
* Franciszek Pietraszczyk: widział upadek Eweliny. Powinna była zginąć. To kwestia magii. Powiedział o tym Ardilli, która znalazła ślad maga Interis.
* Małgorzata Maratelus: nie do końca sprawna mentalnie sarderytka mieszkająca w Szczurowisku, która sobie dorabia jako bieda-doktor Neikatis. Wpada w pętle mentalne. Zero szacunku dla kogokolwiek. Ale pomogła potłuczonej Ewelinie. Lubiana w Szczurowisku.
* OO Infernia: szybko i sprawnie przechwyciła Skorpiona należącego do Szczepana zanim ten zrobił coś głupiego. Wyciągnęła Skorpiona z dołu "na hol", z lekkimi tylko uszkodzeniami owego Skorpiona.
* BIA Prometeus: wspiera Feliksa Kidirona by pomóc Ewelinie sfabrykować fikcyjne rekordy. Pilnuje elektronicznych pamiętników. Ogólnie - overlord arkologii Nativis.

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Neikatis
                1. Dystrykt Glairen
                    1. Arkologia Nativis
                        1. Dzielnica Luksusu
                            1. Ogrody Wiecznej Zieleni
                        1. Stara Arkologia Wschodnia
                            1. Blokhaus F
                                1. Szczurowisko

## Czas

* Opóźnienie: 5
* Dni: 2


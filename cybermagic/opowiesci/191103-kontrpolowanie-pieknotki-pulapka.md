---
layout: cybermagic-konspekt
title: "Kontrpolowanie Pięknotki - pułapka"
threads: nemesis-pieknotki
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [190901 - Polowanie na Pięknotkę](190901-polowanie-na-pieknotke)

### Chronologiczna

* [190901 - Polowanie na Pięknotkę](190901-polowanie-na-pieknotke)

## Budowa sesji

### Stan aktualny

* .

### Pytania

1. .

### Wizja

brak

### Tory

* .

Sceny:

* .

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Misja właściwa

Dzień 1:

Następnego dnia - Pięknotka nie czekała.

Zaczęła od wizyty u Olafa, w Górskiej Szalupie. Olaf nie do końca wie co może Pięknotce powiedzieć; ona szuka zaczepienia z Małmałazem. Olaf ma pewną rezerwę; czegoś nie chce jej powiedzieć. Co dla Pięknotki jest dziwne, Olaf zawsze pomagał. A przecież skrzywdzony jest Alan i Chevaleresse.

Pięknotka powiedziała, że nie zawsze się dogadują, nie zawsze wszystko dobrze idzie... ale oboje chcą tutaj żyć. A ten drań skrzywdził Chevaleresse. Nie chodzi o Alana, chodzi o nią. Olaf odparł że on chroni Enklawy. Jak tylko cokolwiek się dzieje, zawsze jest rajd terminusów po Enklawach i Olaf nie zgodzi się na to. Pięknotka powiedziała, że nie potrzebuje Enklaw - ale chce dowiedzieć się gdzie jest Małmałaz. Olaf powiedział, że spróbuje się tego dowiedzieć.

Pięknotka opuszcza Szalupę i mija się z Ksenią, która tam idzie. Z Ksenią, która nigdy nie chodzi do tego typu miejsc. W głowie Pięknotki odpaliły dziesiątki alarmów. Pięknotka zastąpiła drogę Ksenii; Ksenia chce rozwiązać problem osób, które przekazały broń Małmałazowi. I znaleźć Małmałaza. Ksenia szuka informacji o Enklawach; Pięknotka powiedziała, że ona się tym zajmie. Ksenia powiedziała, że Pięknotka jest zbyt łagodna - terminus został ranny. Sukces - Ksenia odstąpiła. Na razie. Powiedziała, że zajmie się Enklawami po rozwiązaniu Małmałaza. Niech Pięknotka da znać Ksenii czego dowiedziała się od Olafa.

Pięknotka z ciężkim sercem się zgodziła. Po kilku godzinach dostała odpowiedzi od Olafa:

* Małmałaz pozyskał faktycznie informacje w Enklawach; rozpytywał o Pięknotkę. A dokładniej - o terminuskę, która ma potężny pancerz i która masakrowała magów.
* To nie wyglądało na zlecenie, to wyglądało na własną inicjatywę Małmałaza.
* Małmałaz kupował w Miasteczku odpowiednie artefakty i usługi; broń sfabrykowały siły Kajrata. A raczej to, co z sił Kajrata zostało.
* Małmałaz miał awaryjny transport. Wie o nim Marek Puszczok, gość od Grzymościa. On za to odpowiadał.

Z Pięknotką skomunikował się Mariusz po hipernecie. Powiedział, że podglądał Ksenię - Ksenia wie, że Małmałaz znajduje się w Aurum. Nie wie dokładnie gdzie, ale gdzieś mniej więcej w tym obszarze. Pięknotka wie, że Ksenia na pewno wykorzysta fakt, że mają jeńca w Barbakanie. Nataniela. Nieakceptowalne dla Pięknotki - poszła więc porozmawiać z Ksenią. Poprosiła Ksenię, by ta jednak współpracowała z Pięknotką. Powiedziała, że chodziło o jej power suit, o Cienia. Więc prosi Ksenię o możliwość działania razem.

Ksenia się zgodziła, ale na swoich warunkach. To nie pasowało Pięknotce. Po dłuższej dyskusji doszły do porozumienia - Pięknotka działa po swojemu, Ksenia po swojemu a tam, gdzie Pięknotka może wejść najpierw wchodzi Pięknotka a jak się nie uda to Ksenia. Plus, Pięknotka oddała Grzymościa i tematy związane z Grzymościem Kseni - ale poprosiła o Nataniela. Ksenia poszła na taki układ.

Pięknotka skomunikowała się z Lilią po hipernecie. Dawno nie rozmawiały... Lilia jest formalna, nie jest Pięknotce wroga. Wyleciała ze służby u Ateny, ale się dużo nauczyła i nie jest tą samą dziewczynką co kiedyś. Pięknotka powiedziała Lilii, że szuka informacji o kolekcjonerze artefaktów; z pochodzenia z Noctis. Nazywa się Małmałaz. Z kim jest powiązany. Najlepiej tak, by nie wiedział, że Lilia się nim interesuje - Pięknotka ceni sobie element zaskoczenia.

Lilia powiedziała, że sprawdzi. Dowie się tego, dyskretnie. To na poziomie arystokracji nie jest szczególny problem; jej ród nie jest bardzo potężny ale ma włości i pozycję.

Dzień 3:

Lilia wróciła do Pięknotki z cennymi wiadomościami. Małmałaz jest powiązany z kilkoma rodami Aurum; nie jest bezpośrednio podległy praktycznie nikomu, najsilniejsi jego protektorzy to ród Barandis. Małmałaz ma technologie i artefakty też autorepów czy caridów - ma naprawdę sporo różnego rodzaju przedmiotów, sprzętu i ma naśladowców. Po prostu jest to mag skuteczny i mający odpowiednie plecy i surowce. Dopytując się, Lilia wzbudziła zainteresowanie i wzbudziła uwagę, ale to nic dziwnego - Józef Małmałaz jest magiem powszechnie intrygującym. Tak więc może się skończyć na kupieniu przez Lilię jakiegoś artefaktu by podtrzymać przykrywkę.

Pięknotka wie już, że Małmałaz jest magiem silniejszym i bardziej wpływowym niż się wydawało - gdy coś spieprzył, Barandisowie zapłacili za niego raz okup. Ale Pięknotka wie już też jak zwabić go w pułapkę. Kupić / sprzedać artefakty. Sama tego nie zrobi - Małmałaz działa przez agentów i komunikuje się przede wszystkim z arystokracją, to jednak oznacza, że odpowiednio rzadki artefakt który ktoś chce kupić może być czymś co Małmałaza zwabi w pułapkę. Teraz - zaplanować coś takiego.

Pięknotka ma więcej pytań do Lilii - kto jest przeciwnikiem Barandisów? Pięknotka ma informacje o kilku powodach:

* Rody, które nie są dość silne i po prostu płaszczą się przed Barandisami a którym Barandisowie zrobili kiedyś krzywdę
* Rody, które są przeciwne handlowaniem artefaktami i chcą kontrolować magię jako narzędzie
* Rody, które są antynoktiańskie i bardzo proastoriańskie. Oni też są przeciwni Małmałazowi i Barandisom.

Pięknotka nie ma jeszcze idealnego planu, ale ma zamiar Coś Zrobić. Ktoś, kto zaatakował i skrzywdził zarówno Chevaleresse jak i Alana w taki sposób nie zasługuje na spokój.

Znowu nieoceniona okazała się być Lilia. Powiedziała, jak zwykle wygląda transakcja kupowania:

* Wpierw, musi być appraiser. Ktoś, kto jest szanowany i wycenia. Ktoś, kto nazwie to "the real deal". Bez tego nie będzie szumu i nie wiadomo, że coś jest na salonach.
* Potem jest jakiś kontakt, zwykle z zaufanymi pośrednikami.
* Potem dochodzi do aukcji, lub zakupu w odpowiednich miejscach - to zwykle na dworze jakiegoś znaczącego arystokraty. Lilii dworek nie aplikuje.
* Zawsze ktoś musi polecić, dużo "komunikacji bezpośredniej" i "kto z kim kiedy jak". Zwykła sprawa.
* ALE są osoby które skracają ten proces. Często jednym z nich jest właśnie Małmałaz.
* Jako, że nikt nie zna Pięknotki i jej "ekipy", są w słabej pozycji przetargowej - to oni muszą jeździć po ludziach a nie ludzie do nich.
* Anomalie z Wielkiej Szczeliny pojawiają się na rynku; tak więc Grzymość zarabia też na tym.

To cenne wiadomości dla Pięknotki. Jej plan jest stosunkowo prosty - niech przez Minerwę do Grzymościa trafi odpowiedni strumień artefaktów i innych podobnych rzeczy. Grzymość może nimi spokojnie handlować w Aurum, w ten sposób buduje się Minerwę (która ma opinię 'mad scientist') jako handlarza artefaktami i anomaliami. Na pytanie skąd ma, są dwie odpowiedzi: "sama zrobiłam" lub "Erwin". Ona ma dostęp. Dla Grzymościa to wystarczająco dobre.

Żeby to zrobić - Pięknotka musi wpłynąć na Minerwę i na Grzymościa. A dodatkowo, na Pustogorskich terminusów; Minerwa jest kiepściutka w spy stuff. Ale - Minerwa tak czy inaczej ma 'a bit of slack'. Tak więc Grzymość w to uwierzy, grunt, by terminusi nie przeszkadzali. A to lepiej zrobić poza Karlą...

Wpierw, Minerwa. Minerwa ma małe laboratorium w Pustogorze; jej handlerem jest terminus Rafał Roszczeniok. Rafał przypisany jest do fortu Mikado. Nie jest fanem Minerwy, ale nie jest jej wrogi.

Pięknotka wyszła od tego, że Minerwa przecież ma mało zasobów. Minerwa się ze smutkiem zgodziła. Niech więc Minerwa rozważy handel artefaktami - są rzeczy które są stosunkowo mało groźne a które mogą zbudować jej pulę zasobów by Minerwa mogła pozwolić sobie na bardziej zaawansowane i ciekawsze eksperymenty. Minerwie zapaliły sie oczy. Ale ważne jest to, że Minerwa musi działać z regulacjami Pustogoru; bez tego będzie kłopot. Minerwa rozumie sytuację. Wie, że ma na sobie agenta który jej pilnuje. Nie cieszy jej to. Minerwa sama spytała Pięknotki co z tym agentem, przecież od razu to wyjdzie.

Gdy Pięknotka spotkała się z Roszczeniokiem, ten powiedział, że jest rozczarowany Minerwą trochę. Nie tak wyobrażał sobie mistrzynię psychotroniki Orbitera oraz ixiońską czarodziejkę. Roszczeniok chce WIĘCEJ, on chce czynić dobro - a jest skazany na niańczenie Minerwy. Pięknotka powiedziała, że jeśli Minerwa zacznie działać z kanałem przerzutowym, to pojawia się opcja uderzenia w Grzymościa. Roszczeniok się uśmiechnął. Innymi słowy, rozbudowa Minerwy - a nuż zrobi coś przydatnego - i budowanie sobie frontu przeciw Grzymościowi. Szukanie obszarów gdzie on ma styk z Aurum. Roszczeniok spytał czemu Pięknotka nie puszcza tego przez Karlę. Pięknotka powiedziała, że lepiej nie wszystko mówić; Grzymość może mieć wtyki. Plus... Karla ma czasem takie podejście...

* Roszczeniok (SS): podoba mu się pomysł wzmacniania Minerwy i stoi w tej kwestii po jej stronie. Nawet za bardzo.
* Minerwa (SS): zrobi to co Pięknotka zaproponowała. Nawet posunie się tyci dalej; znalazła źródło ujścia swoich TAI i ixiońskich eksperymentów.

Automatycznie, Pięknotka nie musi nawet się bawić w to, żeby łączyć Minerwę i Grzymościa. To Roszczeniok to zrobi za nią. A Minerwa pójdzie odpowiednio daleko, by móc zainteresować zarówno Grzymościa jak i docelowo Aurum - łącznie z Małmałazem. Pięknotka się szeroko uśmiecha - najważniejsze, to żeby inni działali za nią.

Pozostają dwie rzeczy:

* Zbudowanie odpowiedniego artefaktu / anomalii i dodanie do niej transpondera
* Zastawienie pułapki i zebranie siły ognia. Tu może pomóc Moktar lub Orbiter.

Dzień 4:

Pięknotka poprosiła o kontakt z Damianem Orionem, z Kirasjerów. Damian się zgodził połączyć. Pięknotka powiedziała Damianowi prawdę:

* Nie może sama nic z tym zrobić, polityka Pustogor - Aurum ją blokuje
* Może i chce zastawić pułapkę. I to zrobi.
* Potrzebuje siły ognia do "ekstrakcji celu". Oni są w tym świetni. Mogą zostawić jej go jak prosiaka związanego.
* Można zrobić to w formie "no bo Pustogor pojechał Kirasjerów, więc Kirasjerzy odpłacili Pustogorowi gdy ten był bezradny"
* Dodatkowo, Małmałaz polował na Cienia. Skądś wiedział o Cieniu. I to Pięknotkę bardzo martwi.

Damian uznał, że mogą gdzieś mieć dziurę albo przeciek, diabli wiedzą. Damian stwierdził, że nie ma opcji - jeśli ktoś ma informacje pochodzące z Orbitera, to on go "zniknie". Pięknotka powiedziała, że ważniejsze jest to, by takie rzeczy się nie powtarzały. A tak w ogóle to Damian wie tylko dzięki niej. No i ona ma klucze do królestwa; porwanie i zniknęcie Małmałaza będzie zbyt kosztowne politycznie nawet dla Damiana. W związku z tym Pięknotka jednak sugeruje by Damian porwał go dla niej, przesłuchał dla siebie, a potem Pięknotka i Karla się nim zajmą... i będą musiały oddać. Za silnie umocowany. Ale co dostanie wpierdol to jego.

Damian się zgodził na to co chce Pięknotka. Ale jednocześnie Pięknotka pomoże Damianowi w operacjach przeciwko niekorzystnym frakcjom Orbitera. Niekorzystnym dla Damiana, naturalnie. Ale to znaczy "złych" ;-).

Jednocześnie, Pięknotka pogadała z Erwinem - muszą znaleźć odpowiednio ciekawą anomalię a potem muszą ją sfabrykować. Przerobić tak, by się dała śledzić. I w ten sposób pułapka na Małmałaza została przygotowana. Za kilka tygodni będzie dało się ją zastawić...

Tor: 6

**Sprawdzenie Torów** ():

* .

**Epilog**:

* .

## Streszczenie

Pięknotka poluje na noktiańskiego zabójcę. W ciągu tygodnia udało jej się złożyć do kupy pułapkę, rozpocząć linię przerzutową artefaktów do Aurum, połączyć Orbiter, Lilię, Minerwę oraz Pustogor i doprowadzić do tego, że docelowo Małmałaz ma zostać złapany i doprowadzony do Pustogoru w zemście za krzywdę Alana. To był bardzo trudny tydzień, politycznie.

## Progresja

* .

### Frakcji

* .

## Zasługi

* Pięknotka Diakon: .
* Olaf Zuchwały: postawił się Pięknotce - nie chce, by Enklawy ucierpiały przez Małmałaza. Powiedział Pięknotce i Kseni to, co one potrzebowały wiedzieć - ale nie zdradził niczego z Enklaw.
* Ksenia Kirallen: podeszła do tematu Małmałaza z charakterystyczną dla siebie łagodnością i subtelnością. Pięknotka zatrzymała ją, zanim Ksenia zantagonizowała cały świat. Ksenia i Pięknotka współpracują.
* Marek Puszczok: przerzucił Małmałaza do Aurum; okazuje się, że w imieniu Grzymościa robi elementy kanału przerzutowego do Aurum jako mięsień (nie jako głowa).
* Mariusz Trzewń: analityk Pięknotki; powiedział jej, że Ksenia ma lokalizację - gdzieś w Aurum. Pomógł Pięknotce wyłączyć problemy z Ksenią.
* Lilia Ursus: wejście Pięknotki do Aurum; sporo wie o tamtych terenach. Powiedziała Pięknotce jak działają rody Aurum, w jaki sposób kolekcjonuje się artefakty i jaką rolę ma Małmałaz w tym wszystkim.
* Rafał Roszczeniok: terminus-cień Minerwy. Przez Pięknotkę przekonany, że jeśli pozwoli Minerwie na swobodę to może dorwać Grzymościa. Roszczeniok jest powiązany z Fortem Mikado i chce więcej niż niańczyć.
* Minerwa Metalia: rozpoczyna handel TAI i artefaktami / anomaliami z Aurum przez Grzymościa by zdobyć zasoby do badań. Idzie, jak to ona, o jeden krok za daleko. Jej cień-terminus ją wspiera.
* Damian Orion: smutno mu, że informacja o Cieniu wypłynęła i Małmałaz na Cienia poluje. Współpracuje z Pięknotką - ona da mu znać, on wjedzie trotylem i napalmem i porwie Małmałaza.
* Józef Małmałaz: okazało się, że jest bardzo wpływowym magiem, łowcą i kolekcjonerem artefaktów, powiązanym politycznie z rodem Barandis w Aurum.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Pustogor

## Czas

* Opóźnienie: 0
* Dni: 7

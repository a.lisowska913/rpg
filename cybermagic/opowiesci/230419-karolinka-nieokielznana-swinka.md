---
layout: cybermagic-konspekt
title: "Karolinka, nieokiełznana świnka"
threads: waśnie-samszar-verlen, waśnie-blakenbauer-verlen, legendy-verlenlandu
gm: żółw
players: fox, kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [230502 - Strasznołabędź atakuje granicę](230502-strasznolabedz-atakuje-granice)
* [230404 - Wszystkie duchy Siewczyna](230404-wszystkie-duchy-siewczyna)

### Chronologiczna

* [230412 - Dźwiedzie polują na ser](230412-dzwiedzie-poluja-na-ser)

## Plan sesji
### Theme & Vision & Dilemma

* PIOSENKA: 
    * .
* CEL: 
    * Pomścić Viorikę w imieniu Verlenlandu!
        * Wsadzić Karolinkę do Siewczyna
    * Link Elena - Arianna - Viorika

### Co się stało i co wiemy

* Kontekst
    * .

### Co się stanie (what will happen)

* S1: Marcinozaur przedstawia poważny problem Ariannie i Viorice. I Fantazjusz.
* S2: Karolinka w Verlenlandzie (walka z potężnym Snalegiem)
    * traverse Verlenland, Snaleg
* S3: Karolinka w Powiecie Samszar (zwalcza duchy w Kwiatowisku, spotkanie z Tymkiem Samszarem?)
    * traverse Samszarland
* S4: Podkładanie dyskretne Karolinki do Siewczyna (Aleksander Samszar)
    * operacja właściwa

### Sukces graczy (when you win)

* Apollo przeprosi.

## Sesja - analiza

### Fiszki

* Marcinozaur Verlen: mag, skrajny sybrianin, mag iluzji i katai
    * (ENCAO: +-+00 | Don't worry, be happy;;Żarliwy, skłonny do wydania ogromnej ilości energii;;Punktualny| VALS: Face, Stimulation >> Humility| DRIVE: Nawracanie na VERLENIZM)
    * styl: Alex Louis Armstrong x Volo Guide To Monsters.
    * mistrz szpiegów Verlenlandu. Apex szpiegator.
    * Sam sobie zmienił imię. Miał "Marcin". Był JEDYNYM Verlenem z normalnym imieniem, ale chciał pójść jeszcze efektowniej.
    * typowa reakcja: "nazywasz się Maciuś, jak brzuszek, bo tylko jesz. CHODŹ BIEGAĆ PO LESIE!" (do kogoś kto nazywa się inaczej niż Maciek)
* Fantazjusz Verlen: mag, sybrianin, hodowca świń w Verlenlandzie
    * (ENCAO: -0+0+ | Wszystko przemyśli kilka razy;;Kierowany obowiązkiem i powinnością| VALS: Self-direction, Family| DRIVE: Follow My Dreams)
    * styl: Haviland Tuf
    * Zero włosów na ciele, dwumetrowa góra. Brzuchacz. Jeździ na świni, która nazywa się "Grubiszonek". Klasyczny zapach świń.
    * Nie każdy ma jako 'dream' założenie farmy nienaturalnych świń w Verlenlandzie ze wszystkich miejsc, ale Fantazjusz nie jest 'kimkolwiek'.
* Aleksander Samszar: mag, obrońca ludzi i duchów
    * (ENCAO:  -0++0 |Nie wychyla się;;Skryty;;Metodyczny;;Praworządny;;Ugodowy, unika konfliktów| VALS: Conformity, Universalism| DRIVE: Odbudowa i odnowa)
    * styl: Emiya Shirou (Fate Stay Night)
    * Mag materii i duchów, piercer of flesh and souls, mag bojowy i dowódca oddziału duchów
* Tymek Samszar: mag, niefrasobliwy i niezwykle pomocny
    * (ENCAO:  0-+++ |Niefrasobliwy, beztroski;;Kieruje się zasadami;;Elokwenty| VALS: Stimulation, Security| DRIVE: "To ma znaczenie dla TEJ ostrygi")
    * styl: Joker, gdy chce być miły dla Andrei Beaumont / Rankle z MtG. O dziwo, dobry w planowaniu.
    * Poeta oraz bardzo pomocny mag, który uwielbia niewielkie żarty. Iluzjonista. Jego magia jest niezwykle smakowita dla glukszwajnów.
* .Świnka Karolinka: hybrydowy glukszwajn zagłady
    * Wyjątkowo głośna, potrafi śpiewać i uczy się odpowiednich dźwięków. Nauczyła się "Ka-ro-li-nus ko~chanie"
    * Wyjątkowo żądna uwagi i figlarna jak na glukszwajna. Co gorsza, strasznie ciekawska. Kocha duże wysokości.
    * Standardowo: teleportacja, pożeranie magii, uszkadzanie magii (+5Ob)
* .Snaleg
    * verleński snaleg: oczka, wielowężowy tułów, szczęki, macki. Nęci świnki. Źródło energii magicznej.

### Scena Zero - impl

.

### Sesja Właściwa - impl

Aria, Viorika -> zaproszono Was do VirtuFortis, stosunkowo blisko Powiatu Samszar. I tam - na Wielkim Placu Miraży - jest tłum (realny? Fikcyjny?). Po chwili, wszyscy padli na kolana jak wkroczyły dziewczyny. Tron z aksamitu i pięknych rzeczy - i na tym siedzi Marcinozaur. A koło niego, na zwykłym wygodnym krześle - Ula. Wjeżdżają dwie świnie - Fantazmiusz na swoim WIELKIM DZIKU ("Grubiszonku") a koło niego jedzie Elena na śwince.

* A: "Wysyłamy świnnicę? XD Armia Verlenów na świniach pod nowoczesne budynki?!"
* U: (chcę być wszędzie ale nie tu)
* M: "Samszarowie wydali wojnę VIORICE! Musimy ją pomścić!"
* A: "I pomścimy ją Eleną na świni?!"
* V: "Ja nie przyjmuję jego wojny!"
* M: "Nie musisz, siostro! My ją przyjmiemy za Ciebie! Pomszczę Cię w imię księżyca. I świni."
* U: "To dziecko w pancerzu... co?"
* M: "Słuchaj, dziewczynki Verlenów są jak płaszczki. Miłe dla oka, fajnie się je pierze i czasem dożywają, acz nie emerytury."
* E: (schodzi ze świni) (baczność) tien Verlen melduje się do operacji specjalnej!
* U: "Bierzecie ją ze sobą?!"
* A: "A czemu nie?"
* M: "Słuchajcie, to bardzo proste. Viorika nie może podłożyć świni bo to flirt!" (Elena ma minę niezrozumienia)
* M: "Arianna, ma, no, godność. Jest arcymagiem. Jest naszym kieszonkowym arcymagiem!"
* M: "Więc KTO pomści Viorikę?! Kto doprowadzi wredną świnię?! Elena, mistrzowska."
* A: "Masz lepsze podejście do dzieci niż Apollo"
* M: "(uśmiech) Nie tylko do dzieci. Ale nasza Elena jest już dorosła"
* U: "Tien Verlen! Nie możesz wysłać młodej damy do podkładania świni! Na tak niebezpieczną operację!"
* E: (teraz dwa razy bardziej chce iść)
* V: "Ile lat miałaś na niedźwiedziu?"
* E: "Jedenaście..." /ponuro "Chciałam rok wcześniej, ale nie dałam rady."
* M: "Wszyscy pomogli, Vioriko. Wszyscy. Nawet Ula."
* U: (próbuje nie wyglądać)
* M: "Fantazjusz, pomóż, pokaż co Ula zrobiła"
* F: "No nauczyła świnkę śpiewać. Karolinkę."

Elenka spojrzała na Viorikę pałającymi oczkami. "Tien Verlen, NIE ZAWIODĘ!"

Fantazjusz patrzy na Ariannę. "To ona będzie dowodziła operacją? Chcecie coś wiedzieć o Karolince? Lubi być czochrana za uszkiem, lubi być wysoko, ma ogromny potencjał teleportacyjny, ma dobry humor, jest figlarna, uwielbia błotko..." (Elena już śpi). "A, potrzebuje uwagi. No i jest trochę niezgrabna, ale kompensuje teleportacją. Acz źle ją kompensuje i czasem leci nie tam gdzie chciała. Słodka."

Fantazjusz dostarczył Ariannie zapas kiszonych marchewek nasyconych energią magiczną. Dla świnki. Jest po nich grzeczniejsza. Przez moment.

Ula COŚ wie ale nie powiedziała. Arianna pyta o co chodzi. Ula powiedziała, że ona umie się teleportować (rapid) + ghost eater. Arianna chce się dowiedzieć. Arianna: "Ona śpiewa Ula! Nie będziemy się śmiać! To będzie Twoje osiągnięcie w Verlenlandzie!"

Tr Z (Ula jest załamana + Arianna udowodniła skille) +4 +3Or:

* X: Arianna musi poprosić Ulę.
* X: Ula wie, że jest lepsza od nich, więc im powie.
    * Próbowała nauczyć Karolinka aportacji. CHYBA się nie udało. Karolinka nigdy nie pokazała że umie.

Z samego ranka, Zespół: Arianna, Viorika, Elena i Karolinka wyruszyły do Powiatu Samszar. Tienki + świnka lecą "vanem" przez Verlenland. Elena dopomina się czy ona może pilotować. W tym czasie Arianna i Viorika przypilnują świnkę.

Viorika NIE WIE jak się zajmuje świnią, ale wie że jak świnka się nudzi to rozrabia, zajmie śwince powtarzaniem rzeczy których świnka się nauczyła. By dać kawałek marchewki. Viorika ma mniej oczekiwań od świnki niż od rekrutów. A Arianna rozprasza świnkę (by świnka była rozproszona)

Ex Z (wojskowe wyszkolenie Vioriki + sztuczki Uli i Fantazjusza) +4 +3Oy (reprezentujące rozproszenie przez Ariannę i Elenę która nie ma pojęcia że ją rozprasza):

* Oy: (sukces) ALE Arianna kilka razy była pod świnią. Elena miała ostre manewrowanie, bo, no ciężar itp.
* Vz: (sukces) Świnka jest WYSTARCZAJĄCO skupiona. Tzn. świnka jest dobrze przeszkolona. Potrafi aportować. Kosztem marchewki.
* X: Świnka bardzo, bardzo skutecznie wykończyła czarodziejki. Zmęczenie. Pełna uwaga.
* Vr: Elena dała radę dolecieć do celu. Jej omni pomogła w uniknięciu świńskich czułości. Aria i Wirek robiły WSZYSTKO. I udało się. JAKOŚ.
* Oy: Arianna ma fajerwerki w łapkach. Świnka zainteresowana. Udało się wbić głęboko w powiat Samszar aż w końcu Elena rozbiła vana. "Zderzyła". Van jest naprawialny, magią.
    * E: Bardzo przepraszam, to... nie rozumiem... (ale jest wykończona)

Wyobraźcie sobie WIELKIE pole kwiatowe. I na środku pola jest jeden menhir. Jeden. Zgadnijcie w co walnęła Elena... (jej omni się zakłóciło). Świnka ją polizała i Elena "IIII!!!!". Viorika dała radę, podrap za uszkiem.

Viorika i Arianna zabawiają świnię. Tymczasem Elena, która NAJBARDZIEJ potrzebuje się oddalić leci w swoim chevalierze (customowym) by znaleźć najbliższe stado świń. Ale znajduje Tymka...

Tr Z M +2 +5Ob: 

* Xm: Elena ZINTEGROWAŁA Tymka z MENHIREM.

.

* Elena na komunikatorze: "oj..."
* E: "Zamieniłam Samszara w kamień..."
* A: "Co?! XD"
* E: "Był tu Samszar. Nie zauważył mnie. A potem mnie zauważył. I zamieniłam go w kamień..."
* A: "Kamień-monolit czy Kamień-Samszara?"
* E: "Technicznie, wkleiłam go w menhir..."

YEAH! Świnia tam, I SPIEPRZAMY USZKODZONYM VANEM!!!!!!

Arianna pilotuje pojazd, Viorika smaruje Elenę lapisem, Elena nieszczęśliwa (bo lapis) i dumna (bo wygrała bitwę). Viorika ma minkę oficera, któremu się ilość ludzi w koszarach nie zgadzają. Arianna leci uszkodzonym vanem z nadzieją, że wytrzyma. Karolinka została z tyłu, pełni rolę dywersji.

Tr Z (Arianna jest świetnym pilotem MIMO ŻE NIKT W TO NIE WIERZY) +4 (auto bez dachu) +3Or (uszkodzenie vana):

* V: Van jest zamaskowany. Nikt nie pozna TEJ RUINY jako van taktyczny Verlenów. Bez dachu. Z jedną schowaną Eleną i nacierającą ją lapisem Vioriką. 
    * pomaga fakt, że Marcinozaur zrobił emblemat "Diakon express" i symbole kwiatków.
* Vz: Arianna świetnie pilotuje tą ruinę. Zbliżacie się do Siewczyna. Van działa.
    * Elena: "dlaczego mówią, że nie umiesz pilotować?"
    * Viorika: "bo kiedyś nie umiała
    * A: "słuchaj, młoda, kiedyś zobaczyli ten pojazd na końcu. Teraz się za mną ciągnie."
    * E: (mądrze kiwa głową i próbuje zdjąć lapis z włosów)
    * V: "ANI MI SIĘ WAŻ!" (po łapie)
    * E: (pouts)
    * A: "nie chcesz mieć niebieskich pasemek we włosach?"

Faktycznie, udało im się uciec. Siewczyn. Tam już jest potężna jednostka psychotroniczna, lokalny mag i _gdzieś_ jest patrolujący "duch Vioriki". Ten duch Vioriki zachowuje się jak wyobrażenie o Viorice kogoś kto myśli o niej jak wojowniczce. Amazonka. Nie wiemy GDZIE ta "Viorika" jest, ale wiemy, że to jest strażnik. Czyli coś, co chroni.

Potrzebujemy coś, co zainteresuje Strażnika by ten się pojawił. 

* Elena: "to może... rzucimy jakieś potężne zaklęcie? Ja mogę rzucić!"
* Arianna: "zły pomysł, pogorszymy sytuację. Musimy zmienić Strażnika"
* E: "porwijmy lokalnego Samszara!"
* V: "NIE! Nie chcemy by walczył z nami sentisiecią"
* A: "Van na autopilocie, możemy nasączyć magią..."
* E: "Ja mogę nasączyć van! Zrobię tak, by strażnik chciał za nim jechać!" /chce być przydatna

Elena ma już pomysł i go rozwija: "Viorika się zamaskuje, pójdzie i powie że jest potrzebna pomoc. On pójdzie za nią. I ja walnę go w głowę." albo "Arianna krzyknie że potrzebuje pomocy bo potwór. I ja walnę w tył głowy!"

Viorika ma LEPSZY plan. Dane od szpiegatora odnośnie lokalnego Samszara i zagrać z nim w otwarte karty. Najlepiej Eleną (by nikt nie wiedział że tu jesteście). On POWIENIEN mieć kosę z Karolinusem. A Arianna i Viorika zamaskują się jako DŹWIEDZIE! Viorika działa z taktyką, swoim planem.

Tr Z (szpiegator) +3:

* Vr: Mamy dane o lokalnym Samszarze
    * Aleksander Samszar: mag, obrońca ludzi i duchów, 34 lata
* Vz: Wiemy jak do niego podejść - Elena musi być słodka, spokojna, szczera i chcieć pomóc Viorice.

Nagle... pojawił się problem. Arianna musi pokazać Elenie jak być SŁODKĄ. Spokojną i szczerą. Jak to ma ZROBIĆ? Przy OGROMNYCH protestach Eleny ("może ten kij w tył głowy..."). Elena "ale JAK TO mam założyć sukienkę i zdjąć chevaliera?!"

Viorika: "kuzynce nie pomożesz?" Arianna: "nikt nie spodziewa się, że mała urocza... dziewczyna... w sukience jest groźna. Ile da się ukryć pod sukienką! Niewygodnie, ale się dało!"

Tr Z (Elena chce być przydatna!) +3:

* Vz: Elena się STARA. Działa. Dobre serduszko Eleny.
* X: Elena robi to uroczo nieudolnie. Widać, że próbuje i się stara. Widać, że nie ma wprawy. Wszyscy się uśmiechają, ale nie znaczy że nie pomogą.
* X: Aleksander nie wierzy, że Elena ma dość mocy i sobie z tym poradzi. Obieca jej, że ON zadba o rozproszenie tego jak skończy się ten kryzys.
    * Elena: "ale jak pokażę się w domu?! Nie mogę wrócić bez tego, rodzina na mnie liczy! Nie wypowiadaj mi wojny, bo możesz nie wygrać. Przepraszam, chciałam powiedzieć, e... (uśmiech)"
    * Aleksander będzie chciał pomóc w rytuale. ALE Elena powiedziała, że "my" damy sobie radę. Domyślił się, że dźwiedzie to Verlenowie. Udaje że nie wie. Viorika i Arianna wiedzą że udaje. Elena myśli, że go przechytrzyła.
* Vz: Aleksander POZWOLIŁ Elenie i "dźwiedziom" przeprowadzić tą operację. On będzie w odwodach. Aleksander udaje, że nie wie że tu są Verlenowie. Dał Elenie info gdzie znaleźć "Viorikę".
     
Elena wróciła dumna i pewna, że wszystko jej się udało. Arianna i Viorika spojrzały na siebie ze smutkiem.

Znalezienie Strażnika w chwili, w której już jest pomoc lokalnego Samszara nie jest trudne. Oczom Vioriki ukazała się dama w bikini, z większym biustem niż Viorika, z dużym dwuręcznym mieczem runicznym, którego Viorika by nie użyła bo jest niepraktyczny. Elena: "zróbmy z niej świnię." Viorika: "nie. Karolinusa. W boratowych majtkach." Elena się zaczerwieniła. Arianna: "nie, zróbmy z tego bestię. Potwora. To że durny Karolinus zrobił durną rzecz Viorice, ma świnię. A my zróbmy potężnego potwora który pomoże." O dziwo, WSZYSTKIE czarodziejki zaakceptowały. Wszystkie trzy czarują.

Ex Z (wizualizacja bestii - 'antypurchawka', rosiczka, roślinna, baza z magii Arianny (ożywić kwiat), integracja Eleny (integracja rzeczy), projekt Vioriki (tactics)) M +4 +5Ob +3Or (Esuriit Eleny) +2Og (lokalny potwór) +Ow (Arianna) +Ov (Viorika):

* Vm: plan działa perfekcyjnie
    * Viorika zaplanowała jak to złożyć do kupy
    * Arianna jako JEDYNA ma moc, by to utrzymać i by zrobić Awakening lokalnych duchów, one wspierają. Chcą wspierać. Aleksander też nie przeszkadza i próbuje pomóc, acz bardziej sentisiecią niż magią.
    * Elena integruje wszystko w jednego magicznego bloba.
    * "Viorika" zmieniła się w "eteryczną rosiczkę" i zyskała na mocy
* Xm: Elenie uruchomiła się moc Esuriit. POSZŁA KREW. Jej ciało jest w ruinie. Ale moc się spotęgowała i Arianna ledwo to trzyma. Aleksander wszedł do akcji, pełna moc sentisieci.
* Ob: powstaje niesamowitej mocy Bestia. Prawdziwy astralny potwór.
* V: Verlenowie i sentisieć Samszara dali radę skierować dwa potwory przeciwko sobie. Godzilla vs Mothra. Arianna stworzyła super-potwora, Elena integruje wszystko w całość.
* Vm: VERLENOWIE UKRADLI POTWORA SAMSZAROM! Ten potwór, hybryda, trafia do Verlenlandu. Nadrzędny przykaz. Szuka SWOJEGO SUKCESU V VERLENLANDZIE!
    * Esuriit Eleny wpływa na Skażony Rdzeń TAI i hybrydy. Elena jako JEDYNA mogła zdjąć dyrektywę z cierpiącego TAI.
    * Tylko Arianna mogła dostarczyć dość mocy by to mogło działać

Potwór przenosi się z Verlenkami do Verlenlandu. Elena potrzebuje ciężkiej pomocy medycznej. Ale jest z siebie dumna. I w sumie wszyscy z niej też.

POTEM. Arianna i Apollo opracowali następującą propagandę:

1. Viorika nie poszła "mścić" się na Karolinusie. Nie. Ona stwierdziła, że naprawi jego szczeniactwo. Więc z Arianną i Eleną ukradła im potwora. Karolinus spieprzył, chcieli skrzywdzić człowieka, a ona wzięła potwora na siebie. TAKA SZLACHETNA!!!
2. Apollo chciał, niezależnie, pomścić Viorikę, więc wkleił Tymka w menhir i podłożył mu świnię.

## Streszczenie

Zaprojektowano świnkę Karolinkę do pomszczenia Vioriki. Aria, Elena i Viorika ruszyły do Powiatu radząc sobie z figlarnym glukszwajnem. Po drodze Elena rozbiła vana w menhir, wintegrowała niewinnego Samszara w menhir, porzuciły tam świnkę i dotarły do Siewczyna. Tam Elena próbowała być słodka i współpracować z Samszarem. O dziwo, udało się przekonać Samszara by pozwolił Verlenkom zmierzyć się ze Strażnikiem. W wyniku Paradoksów, 'potwór został porwany' i uciekł do Verlenlandu a Elenie po raz pierwszy uruchomiła się moc Esuriit.

## Progresja

* Elena Verlen: po raz pierwszy uruchomiła jej się moc Esuriit gdy interaktowała ze Strażnikiem / Hybrydą.

### Frakcji

* .

## Zasługi

* Arianna Verlen: po chwili zabawiania Karolinki przejęła pilotaż vana jak już Elena go rozbiła. Zamiast iść w powagę, poszła w 'pomóżmy Samszarom i naprawmy im potwora XD'. Dobre serce i klej społeczny zespołu.
* Viorika Verlen: musztrowała Karolinkę by zająć czas; przekonała Zespół by współpracować z Samszarami. Bardzo nie chce być na operacji 'mścimy Viorikę', ale Marcinozaur... Taktyk i siła ognia zespołu.
* Elena Verlen: lat 15, superpoważna agentka specjalnej operacji 'pomścić Viorikę', pilotowała vana (aż się rozbiła o menhir; Karolinka wyssała jej energię). Wkleiła Tymka Samszara w menhir. Potem słodyczą (plan Arianny) próbowała przekonać Aleksandra Samszara do współpracy. I na końcu - Paradoks. Kłębek powagi zespołu.
* Marcinozaur Verlen: szpiegator zaplanował jak pomścić Viorikę - podkładając świnię. Zorganizował 'Karolinkę', dowiedział się o planach w terenie itp. Zrobił show w VirtuFortis dla Arianny i Vioriki.
* Ula Blakenbauer: tresowała Karolinkę. Nauczyła ją aportacji oraz śpiewania 'Karolinus kocham Cię'. Nie jest zbyt dumna ze swojej roli w planach Marcinozaura i Fantazjusza.
* Fantazjusz Verlen: 41 lat, twórca kawalerii - świnnicy. Wprowadził dla Marcinozaura świnkę Karolinkę do irytowania Karolinusa.
* Aleksander Samszar: 36 lat, nie dał się nabrać na 'uroczą Elenkę', ale pomógł Verlenkom walczyć z Hybrydą w Siewczynie. Zauważył, że Verlenki mają ten sam cel co on, więc...
* Apollo Verlen: w tle współpracował z Vioriką i Arianną, by ukryć sytuację z Eleną. Wziął na siebie sprawę z podłożeniem świni i ze wtopieniem Tymka Samszara w menhir. Jako 'mszczenie się za Viorikę'.
* Tymek Samszar: łagodny i sympatyczny, lecz miał pecha - spotkał się z Eleną i został wintegrowany przez nią w Menhir (panika Eleny).

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Aurum
                        1. Verlenland
                            1. VirtuFortis
                                1. Wielki Plac Miraży: ogromne efektowne przedstawienie Marcinozaura wprowadzającego Karolinkę oraz plan ukarania Karolinusa Samszara.
                        1. Powiat Samszar
                            1. Wielkie Kwiatowisko: teren, który jest silnie pod wpływem duchów. Aria, Elena i Viorika porzuciły tam świnię Karolinkę.
                                1. Menhir Centralny: Elena rozbiła o niego vana Verlenów. 
                            1. Siewczyn: Verlenki zinfiltrowały miasto by rozproszyć Strażnika i Hybrydę. Nie wyszło - porwały potwora serią Paradoksów.
                                1. Północne obrzeża
                                    1. Spichlerz Jedności

## Czas

* Opóźnienie: 5
* Dni: 4



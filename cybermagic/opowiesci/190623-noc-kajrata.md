---
layout: cybermagic-konspekt
title: "Noc Kajrata"
threads: nemesis-pieknotki
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [190622 - Wojna Kajrata](190622-wojna-kajrata)

### Chronologiczna

* [190622 - Wojna Kajrata](190622-wojna-kajrata)

## Budowa sesji

### Stan aktualny

* .

### Pytania

1. ?

### Wizja

* Kilka artefaktów zostało zlokalizowanych w okolicy Podwiertu; Pięknotka z Erwinem muszą je odnaleźć. Wsparcie - trzech konstruminusów.
* Serafina zaproszona do zrobienia występu dla firmy Miriko.
* Torba Serafiny - neutralizator artefaktów; w Miriko.

### Tory

* .

Sceny:

* Scena 1: Raport Ossidii.

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Misja właściwa

**Scena 1: Raport Ossidii**

Zaczęstwo. Nieużytki Staszka. Znowu.

Pięknotka i Ossidia, kiedyś seksbot Kajrata. W niewielkiej norze jest uszkodzona Ossidia i Pięknotka. Rany Ossidii się nie leczą. Energia Saitaera nie wystarcza.

Ossidia powiedziała, że prawie zabiła Kajrata - ale pojawił się upiór Esuriit wychodzący z ciała Kajrata. Energia Esuriit go uratowała i zastąpiła część jego ciała sama, na własną rękę. Pięknotka wydobyła jak najdokładniejszy opis. Ucieszyła się, że Kajrat nie jest w stanie niczego zrobić. Pięknotka chciała odwieźć Ossidię... ale wtedy zaatakował upiór Esuriit.

SELF-ACTIVATOR i ściągnąć Ossidię. (TpZ: SS). Udało się, ale potrzebna była energia Saitaera; Saitaer się objawił i odbił atak Ossidii. Pięknotka zauważyła ze zdumieniem, że Esuriit wygląda na słaby punkt Saitaera. A Cień jest zarówno powiązany z Saitaerem jak i z Esuriit. Pięknotka użyła mocy Saitaera by odepchnąć upiora i użyła Cienia, by pożreć owego upiora. (TpZ: SS). Cień POŻARŁ upiornego terminusa. Pięknotka poczuła energię Kajrata jak i ulgę zniszczonego maga...

Ossidia jest w szoku, toteż Pięknotka. Cień się wyłączył. Terminus dołączył do energii cierpienia wewnątrz Cienia. Stał się źródłem energii Cienia. Wieczne cierpienie, zasilające Cienia Esuriit.

Pięknotka zawiozła Ossidię do Podwiertu. Tam przejął ją ktoś inny; Pięknotka nie może wrócić do Pustogoru... ale może przekonać Kajrata, żeby ten uwolnił Lilianę.

**Scena 2: Kajrat wcześnie rano**

Rezydencja Kajrata w Zaczęstwie. Pięknotka została wpuszczona przez agentów Kajrata. Zaprowadzili ją do sali głównej, gdzie Kajrat po chwili przyszedł. Kajrat ma długie rękawice i okulary przeciwsłoneczne; wygląda na to, że jest utrzymywany przez energię Esuriit, ale NADAL ją kontroluje. Nazwał Ossidię swoim arcydziełem.

Kajrat powiedział Pięknotce, że ciężko pracował nad Ossidią. Używał energii Arazille, matrycy Ira, próbował przełamać programowanie - i wyraźnie mu się udało. Jest z niej niesamowicie dumny. Pięknotka patrzyła tylko na niego z coraz większym przerażeniem.

Pięknotka powiedziała mu, że wie o Esuriit i nim. Kajrat powiedział, że dziwi go, że jeszcze nie zbombardowała go z orbity. Powiedział, że dawno temu Orbiter dorwał tak jego jak innych dowódców Inwazji Noctis - ale on się uwolnił używając mocy Esuriit, przez Czółenko. Stracił większość, ale to co pozostało jest wystarczające by kontynuować plany i działania. Pięknotki to nie cieszy. Zażądała Liliany za to, że jemu nic się nie stało. Zgodził się.

Powiedział też Pięknotce o istnieniu czarodziejki, która też kontroluje Esuriit; Blakenbauer Lucjusz próbuje ją naprawić. Ale on może jej pomóc to kontrolować. Będzie ktoś na jego miejsce do kontrolowania tego, co się dzieje w Czółenku. Pięknotka wstępnie powiedziała że mu NIE pomoże. Kajrat wzruszył ramionami.

Zapytany o Serafinę, Kajrat powiedział, że Serafina była uratowana przez Lucjusza, ale strzaskana. Jej motywacja zgasła, jej pasja nie istniała. Nawet matryca Ira nie pomogła. Ale rozjarzył ją iskrą Esuriit i dał jej motywację i siłę do działania. Chęć odzyskania przyjaciół i niechęć do spokojnego życia. Stworzył z niej artystkę.

I teraz doprowadzi Serafinę do perfekcji.

**Scena 3: Serafina**

Pięknotka została obudzona w nocy. Serafina wyjeżdża z Podwiertu. Pięknotka za nią - z Serafiną jedzie... Kajrat. Są w Czółenku. Kajrat zabrał Serafinę do bunkrów. Serafina będzie podejmowała próbę asymilacji energii z dwóch anomalii. Innymi słowy - niedobrze. Pięknotka może powinna wezwać wsparcie. Ale jeszcze tego nie robi.

Pięknotka wyszła by zatrzymać Serafinę słowami. Kajrat sieje truciznę. (Tr:9,3,7=SS). Kajrat wyjaśnił. Chce zatrzymać Ataienne i jej formy kontroli umysłów. Chce uwolnić Alicję Sowińską. A Serafina jest jedyną siłą, która może uzyskać tą moc by uratować swoich bliskich - tych po tej stronie i tych porwanych przez Orbiter. Serafina nie ufa Kajratowi ale Pięknotce ufa jeszcze mniej. A Kajrat kontroluje energię Esuriit i wypacza źródło energii, wypacza samą Serafinę. Matryca Ira przechwytuje tą energię i Serafina staje się czymś trochę innym.

Pięknotka nie chce wojny. A Kajrat ma nadzieję, że Pustogor zaatakuje Serafinę i dojdzie do wojny Pustogoru i Cieniaszczytu. Pięknotka zatem pozwoliła Serafinie na zrobienie tego co chce, by uniknąć wojny. Nie cieszy jej to - jest zła jak osa - ale to może być jedyna opcja. Serafina zasymilowała trochę energii Esuriit i wydała z siebie upiorny wrzask. Banshee.

Serafina uzyskała aspekt Banshee. Kajrat jest bardzo, bardzo słaby.

**Sprawdzenie Torów** ():

* Ewakuacja Serafiny: bezpieczna - nieuchwytna - zwiała - zwinęła: 2
* Wojna: brak - brak - konflikt - zimna - gorąca: 0
* Liliana: przerażona - słucha Kajrata - dołączyła - złamana: 1
* Szkody: niewielkie - duże - katastrofa: 2
* Zdrowie Zespołu: ranni - ciężko - nieaktywni: 0

**Epilog**:

* .

## Streszczenie

Kajrat poszedł za ciosem swojego planu. Przekształcił Serafinę mocą Esuriit; nadał jej Aspekt Banshee, który jednak Serafina jest w stanie jakoś opanować (nie zmieniając jej charakteru ani podejścia). Pięknotka odkryła sekret Kajrata - jest częściowo istotą Esuriit i jest jednym z dowódców Inwazji Noctis. Uwolniła Lilianę spod wpływu Kajrata i udało jej się uratować Ossidię przed śmiercią z rąk upiora Esuriit.

## Progresja

* Serafina Ira: uzyskała Aspekt Banshee przez wpływ Esuriit i manipulację Kajrata na jej Wzór Ira

### Frakcji

* .

## Zasługi

* Pięknotka Diakon: uratowała Ossidię przed zniszczeniem z Kajratowej ręki Esuriit, po czym odkryła sekret Kajrata. Wyciągnęła Lilianę ze szponów Kajrata, ale nie dała rady powstrzymać Serafiny.
* Ernest Kajrat: Pięknotka odkryła jego sekret powiązany z Esuriit. Doprowadził do transformacji Serafiny Iry w istotę będącą częściowo Banshee - broń przeciwko Ataienne. Skończył wyczerpany ale zwycięski.
* Ossidia Saitis: prawie zabiła Kajrata gdyby nie energia Esuriit; prawie zginęła do upiora Esuriit gdyby nie Pięknotka. Kajrat jest z niej bardzo dumny - uważa ją za swoje arcydzieło. Wróciła do Saitaera.
* Liliana Bankierz: trafiła w strefę wpływów Kajrata; zobaczyła że ma do czynienia z potworem. Pięknotka uratowała ją przed losem służki Kajrata.
* Serafina Ira: doprowadzana do paranoi przez Kajrata zaryzykowała i zintegrowała Esuriit ze swoim wzorem. Uzyskała aspekt Banshee. Esuriit jeszcze bardziej ją podradykalizowało.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Czółenko
                                1. Bunkry: Serafina zasymilowała tam anomalie z energią Esuriit; dzięki temu uzyskała aspekt Banshee.

## Czas

* Opóźnienie: 1
* Dni: 3

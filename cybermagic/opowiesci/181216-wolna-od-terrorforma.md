---
layout: cybermagic-konspekt
title:  "Wolna od terrorforma"
threads: dzien-z-zycia-terminusa
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [181209 - Kajdan na Moktara](181209-kajdan-na-moktara)

### Chronologiczna

* [181209 - Kajdan na Moktara](181209-kajdan-na-moktara)

## Projektowanie sesji

### Struktura sesji: Eksploracja

* **Scena, Konsekwencja, Niestabilność, Opozycja, Trigger**
    * Atena w Nukleonie dąży do tranferu na ASD "Kirasjer"
    * Atena na "Kirasjerze" nie będzie miała sensorów, ale będzie wolna i swobodna. Nie będzie mogła tak pomóc jak kiedyś.
    * Atena nie wierzy że może dowodzić Epirjonem - i z uwagi na politykę i na swój aktualny stan
    * Asmodeusz vs Atena.
    * start sesji

* **Scena, Konsekwencja, Niestabilność, Opozycja, Trigger**
    * Waleria - a dokładniej, Bogdan Szerl - ma wiedzę odnośnie terrorforma potrzebną do realizacji celów Pięknotki
    * Pięknotka potrzebuje wsparcia lub zgody Moktara - lub co najmniej wiedzy Szerla.
    * Moktar nie lubi Pięknotki.
    * Moktar vs Pięknotka
    * start sesji

### Hasła i uruchomienie

* Ż: (kontekst otoczenia) -
* Ż: (element niepasujący) -
* Ż: (przeszłość, kontekst) -

### Dark Future

1. ...

## Potencjalne pytania historyczne

* brak

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Misja właściwa

**Scena**: (14:05)

_Kompleks Nukleon_.

Pięknotka w Kompleksie Nukleon. Opiekuje się nią czarodziejka rodu Niecień, niejaka Mirela. Mirela powiedziała Pięknotce, że Atena zmienia swój przydział ze stacji Epirjon na inny statek kosmiczny.

Pięknotka poszła więc odwiedzić Atenę. Powiedziała Atenie o Moktarze - poprosiła Atenę o to, by ta została dead man's hand Pięknotki. Atena ma broń przeciw Moktarowi na wypadek gdyby on zrobił krzywdę Pięknotce. I Pięknotka opowiedziała Atenie wszystko o Moktarze i całej tej sprawie.

Po chwili Pięknotka zauważyła, że Atena nadal ma mundur dowódcy Epirjona. Spytała, czemu Atena chce opuścić Epirjon. Atena odpowiedziała, że jest bardziej niebezpieczna dla Epirjona niż jej brak. Nieważne, czy przeciwnicy wygrają. Są skłonni zbombardować stację niezależnie od okoliczności. Pięknotka czuje, że Atena mówi prawdę, ale nie całą.

Porozmawiały chwilę o terrorformie - Atena na stacji jest groźniejsza i przydatniejsza z perspektywy terrorforma, ale też skuteczniejsza w rozwiązaniu problemu. Atena zauważyła, że kluczem jest rozwiązać wpływ terrorforma. Dobrze - tym się trzeba zająć. Pięknotka powiedziała też Atenie o dziwnych nojrepach (i Lilii) oraz o Emulatorze. Pięknotka próbuje wyciągnąć z Ateny to, co ta ukrywa (TrZ+2:6,3,6=SS). Atena powiedziała Pięknotce, że została przerobiona. Przekształcona. Skażona. Ona nie wie, czy się nadaje czy kralothy jej czegoś nie zrobiły.

Atena chce przydziału na ASD "Kirasjer" - by mieć dalsze działania i zadania. By znaleźć sposób na rozwiązanie problemu terrorforma i by się sprawdzić zanim wróci na Epirjon. Silnie dotknął ją fakt, że Orbiter Pierwszy macza palce w tym wszystkim. Pomoże Pięknotce - ale nie odważy się na zostanie bronią dla przeciwników.

Pięknotka ma wsparcie Ateny.

Wpływ:

* Żółw: 2
* Kić: 1

**Scena**: (14:38)

_Kompleks Nukleon_.

Mirela Niecień. Powiedziała Pięknotce, że nie jest w stanie naprawić jej od tego co zrobił terrorform - ale jest ktoś, kto może pomóc. Bogdan Szerl - osobisty lekarz Moktara. Dawno temu Bogdan współpracował z Nukleonem by uratować Walerię - z rozkazu Moktara. Waleria też była Dotknięta przez terrorforma lub coś pochodnego od terrorforma.

Mirela zapytana o Bogdana powiedziała, że Moktar wypaczył obiecującego lekarza. Celem Bogdana stał się 'torment'. Pięknotka wyczuwa więcej niż nutkę poczucia zdrady od Mireli. Pięknotka szuka we wspomnieniach które ma od Walerii informacje o czymś, co Bogdan pragnie a Moktar mu nie daje (Łt+Z:S). Bogdan zgodnie ze wspomnieniami Walerii jest wybitnym naukowcem i lekarzem, ale skupia się na dominacji i pełnym kształtowaniu swoich ofiar. A Moktar jest siłą utrzymującą go pod kontrolą. Bogdan nie sprzeciwia się Moktarowi, bo Moktar jest tym czym Bogdan chciałby być. Pięknotka czuje, że Bogdanem sporo z Łysych Psów trochę gardzi i sporo się trochę boi. Najchętniej by go gdzieś utłukli. To on znalazł sposób jak złamać Lilię dla Moktara. To on to wszystko zbudował.

Pięknotka ma trudny problem - czy iść w kierunku na zadowolenie Moktara (źle) czy Bogdana (bardzo źle). Czy nie dotykać tematu terrorforma (bardzo źle).

(przerwa)

Wpływ:

* Żółw: 4
* Kić: 1

**Scena**: (19:20)

_Szkarłatny Szept_.

Dla Pięknotki wszystkie opcje są nieakceptowalne. Wszystkie. Nie chce być niewolnikiem terrorforma. Nie chce oddać Wioletty czy Lilii Bogdanowi. Nie chce ukorzyć się przed Moktarem. Ale może zgodzić się na to, by ona była przez pewien czas zabawką Bogdana - "he can't be much worse than kraloth is". I to zdecydowała się oddać Bogdanowi - powiedzmy, trzy dni w jego rękach w zamian za pomoc w zwalczeniu terrorforma. Skontaktowała się z nim więc - poprosiła o spotkanie w Szkarłatnym Szepcie.

Bogdan się zgodził. Więcej, pomoże Mireli przekazać swoją wiedzę kralothowi w Nukleonie, by dało się pomóc i uratować też Atenę i innych. Powiedział, że za pierwszym razem próbował znaleźć rozwiązanie na Walerii i zajęło mu to dobre dwa tygodnie - nie wytrzymałaby fizycznie kralotha. Pięknotka się boi - ale próbuje tego nie pokazać po sobie...

Pięknotka chce jeszcze porozmawiać przed rekonstrukcją z niektórymi osobami.

Atena - zaprotestowała. Ale Pięknotka powiedziała, że nie ma wyboru. Powiedziała o Łysych Psach. Atena rozpoznała ten oddział jako oddział szturmowy wysokiego ryzyka - czy to kosmiczny czy lądowy. Pięknotka poprosiła Atenę, by ta została jej dead man's hand - to bardzo ważne, by ktoś mógł przywrócić Pięknotkę do poprzedniej formy. Pięknotka zaproponowała Atenie, by ta spróbowała zdobyć statek kosmiczny dla Łysych Psów - wtedy będzie mogła wrócić na Epirjon bo ma wsparcie...

Kolejne spotkanie z Julią Morwisz. Pięknotka poprosiła ją, by - jak było trzeba - Julia uratowała ją od Bogdana. Julia się zgodziła. Pięknotka zapoznała Atenę i Julię - im obu przyda się poznanie tych punktów widzenia. Zwłaszcza, że Atena musi dowiedzieć się od Julii o działaniach Orbitera Pierwszego.

Wpływ:

* Żółw: 4
* Kić: 1

**Scena**: (19:41)

_Kompleks Nukleon_.

W Kompleksie dochodzi do rekonstrukcji Pięknotki przy użyciu Bogdana, kralotha i Mireli. Pięknotka nie do końca pamięta co się działo tego dnia - ani przedtem, ani potem (Tr:10,3,5=S). Pięknotka jest wolna od korupcji terrorforma, doszło też do pewnej przebudowy jej struktury. Ale najlepsze dopiero przed nią...

Wpływ:

* Żółw: 5
* Kić: 1

**Scena**: (19:44)

Pięknotka nie ma dużo szczęścia. Po rekonfiguracji Wzoru jest szczególnie podatna na zmiany - a Bogdan dostał ją na trzy dni w swoje szpony. Pięknotka wcześniej zrobiła sobie jakąś formę kryształu, rezerwy, memory - ale dla NOWEJ Pięknotki tamten kryształ i tak nie będzie atrakcyjny. Ku wielkiemu zaskoczeniu Pięknotki, Bogdan działał w zupełnie inny sposób niż ona się spodziewać mogła. On nie myśli jak kraloth - it makes him more of a monster...

Pięknotka stawiała bardzo silny opór - trening terminusa, wie na co idzie, ma doświadczenie z kralothami, poszła przygotowana. Ale Bogdan potraktował to jedynie jako nowe, potężniejsze wyzwanie. Zakochał się w niej - i tym bardziej chciał ją zrobić swoją.

(HrZ+4:12,5,16=SS). Pięknotka zakochała się w Bogdanie i we wszystkim co on sobą reprezentuje. Ona nie chce odejść, nie chce nigdy od niego odejść.

Pięknotka nie pamięta, co się działo tego dnia. Powiedział jej, że ma iść i zachowywać się "normalnie" a wieczorem do niego wrócić. Wieczorem jednak wymykała się do Bogdana - i na drodze stanęła jej Julia. Spytała, gdzie Pięknotka idzie. Na spacer. Julia puściła ją przodem, ale jak Pięknotka minęła, zaczęła ją dusić. Pięknotka odpaliła power suit (10,3,5=SS). Pięknotka walczyła dzielnie, ale Julia ją pokonała. Nie pomógł fakt, że Pięknotka ma zupełnie nowe reakcje ciała...

Wpływ:

* Żółw: 9
* Kić: 2

**Scena**: (20:05)

_Kompleks Nukleon_

Pięknotka obudziła się ponownie w Nukleonie. Nad nią stoją Atena i Julia. Minęły kolejne dwa dni - Pięnotka była zrekonstruowana przez kralothy by ją naprawić po tym, co zrobił jej Bogdan. Pięnotka jest... nieco inna. Synteza chemiczna. Uroda. Brak wstydu. Inny Wzór. Dużo groźniejsza niż wcześniej, co zauważyła od razu Julia. Atena stwierdziła, że Nukleon spieprzył i Pięknotka nie jest naprawiona. Mirela powiedziała, że na odwrót - TERAZ Pięknotka jest tym, czym powinna być od samego początku.

Pięknotka dokładnie wie jak dotrzeć do Bogdana - i wie, że Bogdan wie coś o terrorformie. Coś, czego nikomu nie powiedział. Chce się z nim spotkać; weźmie ze sobą Julię. Atena twierdzi, że to kiepski pomysł. Są tam działka, ale powyłączane. Jest to stary acz potężny medbunkier z czasów przed Zaćmieniem.

Pięknotka się do tego świetnie przygotowuje - kosmetyki, narkotyki, narzędzia inne. Bogdan chce Pięknotkę z powrotem. Wykorzystały plan: Pięknotka idzie przodem i "robi swoje", Julia ma ją odbić jakby było to potrzebne.

_Colubrinus Meditech_

Pięknotka bez problemu teleportowała się do Kompleksu z Julią. Julia została z tyłu. Pięknotka słyszy w oddali śmiech - z Bogdanem jest jedna z jego lalek. Pięknotka przyszła na jego zlecenie i żądanie. Weszła spotkać się z Bogdanem - Mistrzem. (-5 Wpływ: Pięknotka wciąż do niego czuje dużo... za dużo. Ma potencjał na revert.) (Tp:9,3,3=SS). Julia wpada pod wpływ tego miejsca, tych środków w powietrzu. Myślała że jest odporna, ale nie jest. Nie tak. Nie na to. (Ż: -5 Wpływ: Julia will fall).

Bogdan jest święcie przekonany, że Pięknotka wróciła do niego jako lalka. Jest też święcie przekonany, że NAWET jeśli nie, to jest mu lojalna. Pięnotka zachęciła Bogdana, by powiedział jej odnośnie terrorforma to co wie. Na tym etapie Bogdan już wie, że Pięknotka jest "sobą", ale nie przeszkadza mu to - nadal do niego przyszła i nadal go wielbi (Tp+2:9,3,3=S).

* Terrorform to forma pasożyta eterycznego. Wyciągnęliśmy to (Orbiter) z asteroidów. Musiało pojawić się przy Zaćmieniu; eksperymenty to obudziły i uruchomiły
* Terrorform jest formą demona z naszego punktu widzenia. Bóstwo bez wyznawców. Bardzo to wzmocniliśmy podczas ostatniej wojny - zainfestowaliśmy nim statek wroga. Potem zamknęliśmy to w vaultach... a jednak nie.
* Jest to jedna z broni jaką stosujemy w ramach Orbitera. Służy do obrony, nie do ataku. Bogdan się z nim zmergował by go zrozumieć - to go wypaczyło, pokazało mu nowy, lepszy świat.
* Bogdan wie dokładnie na których kanałach terrorform operuje i na których działa.
* Bogdan dał absolutny immunitet na terrorforma Pięknotce, taki jak ma dla siebie. Ona należy do NIEGO. W jej krwi płynie krew Bogdana.
* Terrorform zna przejście na inny świat - jak Astoria. Ten świat jeszcze nie był eksplorowany, ale będzie. Tylko wpierw trzeba poradzić sobie z tym co się dzieje tutaj.
* Jest możliwość związania i uwięzienia terrorforma. Na tym shardzie lub w konkretnym ciele, póki nie zostanie zniszczone. A terrorform nie umie przejść przez sharda tak jak inny.

Pięknotka ma już wszystko czego chciała. Wypadałoby spokojnie opuścić to miejsce... więc wysłała ping hipernetowy do Julii. I Julia przyszła... słaniając się na nogach, ledwo przytomna, chętna oddać się we władanie Bogdana.

Pięknotka zaatakowała Julię "z zazdrości". Chce ją obudzić, ale jest to niesamowicie trudne, praktycznie niemożliwe. Pięknotka chce ją corruptować - niech Julia służy JEJ. Julia jako Emulator i wspólna historia +2. Pełny atak afrodyzjakami (HrZ:11,5,17=P,S). Julia jest śmiertelnie zakochana w Pięknotce i chce jej służyć, ale udało się Pięknotce uwolnić Julię. Julia dojdzie do siebie, ale wpierw musi troszkę odpłynąć...

Pięknotka "przygotowała" Julię dla Bogdana "powstrzymując zazdrość". Bogdan zrobił swoje i dobrał się do Pięknotki. W międzyczasie Julia się obudziła i Bogdana wyłączyła z akcji. Z trudem, dwie czarodziejki opuściły Kompleks Colubrinus Meditech...

Wpływ:

* Żółw: 2 (12)
* Kić: 3

**Epilog**: (21:35)

* Bogdan wie, że Pięknotka nie jest pod jego totalną kontrolą. Nie dba o to, póki go wielbi i się czasem pojawia (czego jeszcze nie zrobiła).
* Julia ma predylekcje do poddania się kontroli Bogdana i teraz o tym wie. Będzie unikać, bardzo.
* Julii skrzyżowały się systemy kontroli z Pięknotką. Jest jej absolutnie oddana.
* Pięknotka nadal "czuje coś" do Bogdana. Nie będzie jej łatwo go nie odwiedzać - chyba, że opuści Cieniaszczyt.
* Pięknotka jest całkowicie odporna na wszelkie wpływy terrorforma.
* Pięknotka jest całkowicie przebudowana. Lepiej czuje się w nowej formie.
* Pięknotka boi się wpaść pod kontrolę Bogdana; jest to zbyt prawdopodobne i jej by się zbyt podobało.

### Wpływ na świat

| Kto           | Wolnych | Sumarycznie |
|---------------|---------|-------------|
| Żółw          |   2     |   12        |
| Kić           |   4     |    6        |

Czyli:

* (K): Bogdan chce, by Pięknotka wracała bo 'he fell for her'. (2)
* (Ż): 

## Streszczenie

Pięknotka chciała uwolnić się od terrorforma - udało jej się, gdyż oddała się we władzę straszliwego mindwarpa Łysych Psów, Bogdana Szerla. Szerl wraz z kralothami zrekonstruowali Wzór Pięknotki - teraz jest odporna na wpływy terrorforma, ale jednocześnie jest bardzo zmieniona. Sensual, dangerous, chemical. Przy okazji Julia (Emulator) oddała serce Pięknotce a Atena zatrzymała swój przydział na Epirjon.

## Progresja

* Pięknotka Diakon: pełna przebudowa karty postaci. Główne aspekty: chemiczna, seksowna broń uwodząca. Nadal pozostaje kosmetyczką i terminuską - jest po prostu groźniejsza.
* Pięknotka Diakon: straszna słabość do Bogdana Szerla. Trudno jej się mu oprzeć w dowolnej formie.
* Mirela Niecień: dostała formułę pomagającą w zwalczaniu wpływów terrorforma Saitaera na magów; wymaga kralotha.
* Julia Morwisz: straszna słabość i oddanie wobec Pięknotki Diakon. Nie potrafi odmówić jej rozkazów. Co więcej, pragnie tych rozkazów.
* Bogdan Szerl: jest zauroczony Pięknotką; jest jego ukochaną zabawką. Chce ją z powrotem, ale chce też by nie była tylko u niego.

### Frakcji

* 

## Zasługi

* Pięknotka Diakon: całkowicie przebudowana przez kralothy oraz Bogdana. Obudziły się w niej diakońskie instynkty i stała się jeszcze groźniejsza i piękniejsza. Praktycznie - nowy początek.
* Mirela Niecień: lekarz z Kompleksu Nukleon; opiekuje się Pięknotką i Ateną. Zna Bogdana oraz wie, jak jest niebezpieczny. Ogólnie, dobra i pozytywna siła wspierająca Pięknotkę i przyjaciół. Dostała informacje od Bogdana jak naprawiać magów dotkniętych przez terrorforma z pomocą kralothów.
* Atena Sowińska: zrezygnowała tymczasowo ze stacji Epirjon, ale decyzja nie jest pewna. Martwi ją przemiana Pięknotki. Próbuje pomóc po swojemu.
* Bogdan Szerl: przerażający mindwarp Łysych Psów. Przekształcił sobie Pięknotkę i uwolnił ją spod wpływów terrorforma. Ma własny harem płci obojga. Żyje dominacją i znajdowaniem granic. Dużo wie o terrorformie Saitaerze.
* Julia Morwisz: wpierw wykorzystana przez Pięknotkę jako ostateczna linia obrony (zadziałało), potem jako wsparcie do wyciągnięcia (nie zadziałało, Pięknotka wyciągała ją). Oddała kontrolę nad sobą Pięknotce.

## Plany

* Bogdan Szerl: odzyskać swoją Pięknotkę. Przywrócić ją do swoich.
* Atena Sowińska: zapewnić kontrolę nad sobą zanim wróci na Epirjon. Nie chce być śmiercią i zniszczeniem stacji.
* Pięknotka Diakon: bardzo jej zależy, by jakoś się uodpornić na działania Bogdana Szerla.
* Pięknotka Diakon: chce spiknąć ze sobą Pietra i Wiolettę. Skoro ma odejść z terenu, niech przynajmniej zostanie po niej coś fajnego.

### Frakcji

* 

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Przelotyk
                        1. Przelotyk Wschodni
                            1. Cieniaszczyt
                                1. Knajpka Szkarłatny Szept: miejsce spotkania Pięknotki i Bogdana; tam doszło do ich porozumienia.
                                1. Kompleks Nukleon: miejsce naprawy Ateny oraz Pięknotki; jakkolwiek dziwnie to zabrzmi, kwatera dowodzenia Pięknotki i Ateny.
                1. Sojusz Letejski, NW
                    1. Ruiniec
                        1. Colubrinus Meditech: przed Zaćmieniem potężny autoszpital; teraz kontrolowany przynajmniej częściowo przez Bogdana.

## Czas

* Opóźnienie: 2
* Dni: 6

## Narzędzia MG

### Budowa sesji

**SCENA:**: Nie aplikuje

### Omówienie celu

* nic

## Wykorzystana mechanika

1811

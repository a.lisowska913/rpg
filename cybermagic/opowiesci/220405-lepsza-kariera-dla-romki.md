---
layout: cybermagic-konspekt
title: "Lepsza kariera dla Romki"
threads: planetoidy-kazimierza, sekret-mirandy-ceres
gm: żółw
players: kić, anadia
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [220329 - Młodociani i pirat na Królowej](220329-mlodociani-i-pirat-na-krolowej)

### Chronologiczna

* [220329 - Młodociani i pirat na Królowej](220329-mlodociani-i-pirat-na-krolowej)

## Plan sesji
### Co się wydarzyło
#### Co się wydarzyło w tym terenie?

* Inicjacja bazy Ukojenie Barana
    * mag eternijski, Bruno Baran, założył niewielką bazę daleko od Eterni z częścią ludzi 12 lat temu
    * automatycznie zaczął się kręcić profit
    * wśród górników Elsa Kułak została nieformalną liderką. Niestety, "demokracja" nie przypasowała Baranowi. Odizolował się.
* Migracja Mardiusa
    * noktiańscy komandosi Tamary Mardius złożyli tu swój nowy dom wśród lapicytu. Podeszli do górników i Barana z neutralnością. Ot, są.
    * mało kto jak noktianie umieją żyć w kosmosie. W tym miejscu znaleźli świetne miejsce i ich umiejętności stały się bardzo przydatne.
    * siły Mardius znalazły opcję szmuglowania przez statek Gwiezdny Motyl.
* Imperium Blakvela
    * 3 lata temu pojawił się eternijski szlachcic, Ernest Blakvel. Stwierdził, że władza w kosmosie to coś dla niego. Zaczął umacniać się w tym terenie.
    * Blakvel i Mardius weszli w stały konflikt. Mardius chroni miejscowych, Blakvel chce ich podbić.
* Pojawienie się Strachów
    * 2 lata temu pojawiły się Strachy. Stworzone z programowalnej materii (morfelin), nie wiadomo czemu są i czym są.
    * Mardiusowcy aktywnie chronią lokalnych.

#### Strony i czego pragną

* Górnicy w Domenie Ukojenia i mały biznes: 
    * CZEGO: zachować niezależność, handel itp. Przetrwać. Zarobić.
    * JAK: sami się zbroją
    * OFERTA: standardowa oferta terenowa, plotki itp.
    * KTOŚ: Elsa Kułak (przełożona, trzyma twardą ręką), Antoni Czuwram (górnik, solidny stateczek i niezły sprzęt - dużo wie), Kamil Kantor (szuka ojca Leona który odkrył sekret Mirnas i zaginął), Bruno Baran (mag eternijski i założyciel; Strachy to destabilizacja Blakvela przy obecności lapizytu), Kara Szamun (młódka z rodziny Szamun, pilot i nawigator transportowca na Asimear.)
* Przemytnicy Blakvela: 
    * CZEGO: zdobyć prymat w Domenie Ukojenia
    * JAK: zmiażdżyć Mardiusa, potem przejąć Talio i zestrzelić bazę Barana.
    * OFERTA: świetne rozeznanie w pasie, największa siła ognia, wiedza o SWOICH skrytkach, wsparcie piratów etnomempleksu Fareil (synthesis oathbound pro-virt, pro-numerologia)
    * KTOŚ: Ernest Blakvel (arystokrata eternijski)
* Przemytnicy Mardiusa: 
    * CZEGO: zregenerować siły, odzyskać Tamarę, ukryć się przed Blakvelem
    * JAK: w ciągłej defensywie, ufortyfikowanie Sarnin, operacje militarne anty-Blakvel
    * OFERTA: wiedza o Miragencie i Aleksandrii, świetne rozeznanie w pasie, wiedza o SWOICH skrytkach, znajomości wśród lokalnych.
    * KTOŚ: Tamara Mardius (eks-komandos noctis, Alexandria), Deneb Ira (regeneruje komandosów po sprawie z Aleksandrią)
* Taliaci: (mieszkańcy Talio)
    * CZEGO: rzeczy wysokotechnologiczne
    * JAK: handel, współpraca
    * OFERTA: mają dostęp do WSZYSTKICH frakcji i są wiecznie neutralni, reaktory termojądrowe na podstawie deuteru i trytu na Talio.
* Strachy
    * CZEGO: Brak woli. Reakcja. Zniszczyć wszystko.
    * JAK: flota inwazyjna
    * OFERTA: zniszczenie, targetowane lub nie.

### Co się stanie

* .

### Sukces graczy

* .

## Sesja właściwa
### Scena Zero - impl

.

### Sesja właściwa - impl

Sześć dni minęło od powrotu. Dzieciaki mogą obejrzeć, zobaczyć. Ogólnie, mają bazę nie na planetoidzie a na Królowej. Prokop chciał, by tam były. Jest wdzięczny zarówno Annie jak i dzieciakom - bo mają Królową. Dlatego wszyscy są na pokładzie Królowej. Jednocześnie, na pokładzie znajdują się dwaj eks-piraci, którzy ani na moment nie opuścili Królowej. Helena i Wojciech.

* Łucjan: "To jest bardzo zły pomysł. To są piraci. Raz pirat - zawsze pirat"
* Prokop: "Masz lekarza? Masz psychotronika? Ta dwójka da radę. Plus, umieją walczyć"
* Łucjan: "Tego właśnie się boję..."

Miranda zauważyła, że Helena konsekwentnie wykorzystuje medkity do produkcji jakichś otępiaczy. I konsekwentnie się szprycuje. Wojciech próbuje ją osłaniać. Jak na razie - nikt nie wie, że Helena jest ćpunką. Wojciech jest w stanie wziąć uwagę na siebie. Nie pomaga fakt, że blakvelowcy nie mają dużo drogiego sprzętu. Więc pryzowe itp. nie było takie wysokie - blakvelowcy grają fair, po prostu nie mają czym wypłacić. Łatają Królową itp.

Wojciech skutecznie przekonał Helenę, by ta zamiast zużywać apteczki robiła narkotyki z lokalnych rzeczy. Np. z lifesupport itp. Helena jest raczej głęboko zgnębiona - nie ma pomysłu co i jak.

Nie pomaga fakt, że wpada Damian Szczugor na Królową. Nie zgadza się na to, by piraci latali na Królowej. To w końcu ich statek. Prokop próbuje tłumaczyć, ale idzie tak średnio. Damian "wieszamy za jaja piratów". Miranda odpowiada, że to będzie trudne. Damian "spierdalaj". Miranda włącza silniki. 

Miranda próbuje zamącić - odroczyć rozmowę przez skomplikowane problemy na statku.

TrZ+2:

* Vz: Damian się oddali, zostawia to na razie. Ale Królowa ma zakaz odlotu dopóki sprawa się nie rozwiąże.

Prokop otarł czoło jak Damian się oddalił "mieliśmy szczęście, że Cereska kiepsko zadziałała". Miranda się uśmiechnęła po swojemu. 

Romana wzięła Annę na stronę jak Bartek i Kara byli poza Królową (najpewniej z Gotardem) - "czyli oni byli piratami i teraz mogą być na statku?" Anna: "byli piratami, w czym bycie piratem to kiepska sprawa". Romka na to: "czyli błąd nie determinuje tego, że ktoś jest skreślony?" Anna: "pewnie, że nie, choć czasem zależy od błędu". Romka ma do myślenia. Anna na to "a jakie błędy masz na myśli?" I Romka jest w kropce.

Anna próbuje dojść do tego co tym razem chce Romka odpierdolić. Pokazuje, że w różnych błędach różne błędy się wykonuje. Kategorycznie rozwiązuje się różne rzeczy różnie, często stanowczo.

TrZ (zaufanie) +3:

* V: Romka się przyznała. Gdy była w barze (???) to gadała z kolesiem który rekomendował jej zarobki w sferze rozrywkowej. I Romka poważnie to rozważa. Był nią zainteresowany i był skłonny dobrze zapłacić. Romka widzi to jako ścieżkę kariery lepszą niż pirat czy np. górnik. Bo więcej zarobi i będą nią zainteresowani. No i nie musi kraść. 
    * Anna ma zagwozdkę - jest to LEPSZE niż kradzież, ale niekoniecznie to do czego Anna chce ją przeznaczyć.
* (+1Vg) Anna: "jaką masz gwarancję że Ci zapłacą lub nie wezmą do niewoli". To drugie nie działa, ale to pierwsze... nie ma opcji. X: Romka jest przekonana, że Anna zawsze uratuje swoich podopiecznych. 
* X: Romka wiedząc o sile Anny jest skłonna spróbować CHYBA że dostanie bezpośredni zakaz.
    * nowy plan Romki: "do 18 roku życia się prostytuować a potem jak Anna nie będzie się nią mogła opiekować to reinwestować np. w farmę grzybów" (spoiler: nie wie nic o grzybach ani popycie / podaży)
    * JESZCZE NOWSZY plan Romki: niech Anna zostanie jej alfonsem i sprawdzi jej potencjalnego klienta (Miranda już się śmieje)
* X: Romka jest przekonana, że zna swoją ścieżkę kariery. Prostytucja teraz -> potem biznes. Czy coś. Więc teraz Anna musi poodkręcać by nikt Romki w to nie wkręcał. Ktoś jej nieźle naopowiadał - dokładniej to niejaki Antos Kuramin, z sił Blakvela. Anna znajdzie go w Klubie Korona, na hiperstrukturze Szamunczak.

Miranda czeka na moment gdy Helena zażywa nowy środek / narkotyk. Jak Helena zaczęła przygotowywać nową dawkę, Miranda ma "małą awarię" -> Anna widzi, jak Helena wstrzykuje sobie jakiś środek na monitorze. Anna mówi "kurwa". Helena jest w pokoju który dzieli ze zrezygnowanym Wojtkiem.

Anna jest wściekła. Helena miała być dobrym medykiem. Ale jest naćpana i zjada towar. Idzie do Heleny i Wojtka - ma tam kilkanaście metrów, na innym pokładzie. Helena chowa towar, Wojtek wpuszcza Annę.

Anna do Heleny "co ćpiesz". Wojtek broni Helenę "nie Twój zasrany interes". Wojtek próbuje chronić Helenę, Anna skupia się na Helenie która mówi z lekkim rozmarzeniem. Wojtek diagnozuje Ceres na szybko; ale monitoruje rozmowę Anny i Heleny. Krótka dyskusja Helena - Anna wykazała Annie, że Helena ma wszystko gdzieś. Anna -> Wojtek "nie przeszkadza Ci to? Nie martwisz się o jej zdrowie?" Wojtek "nie mam się czym martwić, masz złudzenia. Helena jest w perfekcyjnej formie".

Gdy Wojtek powiedział "Twoje stópki po tamtej stronie drzwi, raz raz raz babciu". Anna szybkim ruchem do Wojtka i przyciska do ściany chwytem na małe chujki. I zimnym głosem "babciu?"

Anna ściera się z Wojtkiem. Chce by ten nie przeszkadzał Annie w pomocy Helenie. Może nawet pomóc.

TrZ+3:

* V: Wojtek chciał kopnąć Annę, ale ta skutecznie uniknęła ciosu i poprawiła mu w jaja. Łzy w oczach. Wojtek widzi gdzie stoi +1Vg
* V: Anna poinformowała - albo Wojtek i Helena współpracują, albo trafią do Blakvelowców. Wojtek "kapitan nas chroni". Anna "Blakvelowcy mogą się tu wkraść i może dojść do nieprzyjemnego wypadku". Anna -> Wojtek "ratuję dzieciaki od uzależnień i tej też mogę pomóc" +1Vg
* X: Wojtek podczas szamotaniny uszkodził Annę. Siniaki, będzie bolało, ale nie ma krwi ani złamań
* V: Wojtek nie będzie próbował się mścić; potraktuje działania Anny jak niebyłe. Zimna neutralność.
* V: Wojtek nie próbuje pozbyć się Anny ze statku.
* Vg: Wojtek nie będzie chronił Heleny przed Anną. Chce pomóc Helenie i MOŻE Anna jest w stanie. Da jej szansę bo sam nie wie jak. ALE SIĘ NIE PRZYZNA. Oficjalnie jest jej wrogi ale jak się uda to będzie dyskretnie wdzięczny.
* Vz: Wojtek weźmie Romkę "pod skrzydła". Zajmie się nią, jej czasem. Będzie się nią "opiekował". Żeby nie została prostytutką.

Anna -> Romka. Ta już ma nową suknię. Duże wycięcie. Za duża na nią. Przerobiona. +100 seksowności. Niewiele ukrywa. Tak jak Romka myśli że ma wyglądać a nie tak, jak Anna wie że ona powinna wyglądać. Romka się wyraźnie nie zna. A ten makijaż... omg. Romka się postarzyła o 10 lat. Nie ma wprawy w tym biznesie; miałaby sukcesy większe jakby się na 14 zrobiła (wtedy premium u niektórych).

Romka szukała dodatkowych informacji, inspiracji itp. Miranda jej pokazała rzeczy drastyczne i hardcore. Romka chciała "jak uwodzić" a dostała "hardcore elecric pain play" dla demotywacji.

Miranda demotywuje Romkę w ścieżce prostytucji i chce ją zdecentrować.

Tr (niepełna) Z (ja jestem internetem) +4:

* X: Romka nie jest głupią dziewczyną. Kolejny fakt - ta TAI "wygodnie" się myli. She's a con. KTOŚ ją programuje.
* V: Romka jest zniechęcona do prostytucji. Widziała... złe rzeczy (Dubaj++)
* Vz: Romka jest zdecentrowana. Poważnie zdecentrowana. Miranda sfabrykowała z wszystkich klipów Łucjana naprawdę podłe rzeczy.

Anna widzi, że Romka jest nie tylko "ubrana" ale też lekko niespokojna. Romka sama zapytała Annę o niektóre bardziej hardcorowe rzeczy które widziała. Anna "są i tacy - i nie masz jak wybrać, nie podoba się to już nie uciekniesz". Romka "no ale przypalanie?! tortury?!" Anna "no, są i tacy. Za to nieźle płacą." Romka ma MINKĘ typu "kradzież w sumie bardziej spoko..."

* Anna: "Romka, może zastanów się nad tą opcją i poucz się... statków? Komputerów? Medycyny?"
* Romka: "Brzmi trudno, pracowicie i nie znam się na tym"
* Anna: "Prostytucja i kradzieże są pracowite a mam dla Ciebie mentora (w innych tematach)."
* Romka: "...ok?"
* Anna: "Spróbuj. A Wojtek nauczy Cię się bić."

Tr (nie ekstremalny bo Miranda) Z (zaufanie + glamour bo piraci) +3:

* V: Romka spróbuje iść uczyć się od Wojtka i/lub Heleny. Da szansę.
* V: Romka znajdzie coś, co jej się spodoba bardziej niż prostytucja lub kradzież.
* X: Romce spodobał się Wojtek i jego podejście (direct, no-nonsense, "to debilny pomysł").
* V: Wojtek i Romka znaleźli coś co do niej pasuje - technik. Osoba o dużej zręczności pracująca z delikatnymi urządzeniami.
* V: DOCELOWO Wojtek polubi Romkę jako podopieczną - będzie skłonny ją zasponsorować / znaleźć jej lepsze życie.

Anna stwierdziła, że nie ma opcji. Musi iść do baru i upewnić się, że nikt nie będzie skłaniał jej podopiecznych do prostytucji. Romka już tam nie będzie, ale jeszcze jest Kara - bardzo społeczna. Nic, Anna nie ma pomysłu na Karę.

Anna idzie do Klubu Korona. Miejsce nie jest może najczystsze, ale jest głośne, klimat ma wesoły. Są 2 prostytutki, jest 8-9 patronów, jest bar i ceremonialny obrzyn. Taki trochę dziki zachód w kosmosie. Za barem Ursyn Uszat. Spytał wesoło Annę - dobry czy zły dzień? Dobry. To proponuje "anihilator asteroidów". Płyn jest zielonkawy i ma ostry zapach. Lekko słodkawy. Ostry. Smakuje jak ostry, lekko słodkawy drink który trochę wypala wnętrzności. Nic niemożliwego dla Anny. To jest uczciwy trunek.

Anna pyta Ursyna gdzie jest Antos. Ursyn pokazał - Antos teraz pije sam. Anna poprosiła, by Ursyn dał mu nowego drinka; tego co ten już ma. Ursyn skinął głową. Się robi.

Antos lubi wyzwania. Ogólnie sympatyczny, chciał kupić noc z Anną (która powiedziała, że ją stać i nie będzie się prostytuować). Gdy ta powiedziała, żeby on zostawił Romkę, Antos powiedział "jesteś jej mamą?". Wyraźnie nie chce odpuścić. Anna widzi, że mu na niej nie zależy; to kwestia NUDY i potencjalnego wyzwania a nie chuci czy chęci młodego ciałka.

Antos -> Anna "nie będę się opiekował dziećmi XD" mówiąc o Karze. Anna -> Antos "bo chodzi o to by dzieciaki zaznały normalnego życia, pracę." Anna -> Antos "bo to dzieci z odzysku, dla których chcę lepszej przyszłości".

ExZ (pokonaliście piratów) +3:

* Vz: Antos "MOGĘ się zająć tym, by tej małej nie stała się krzywda. Jeśli jest z odzysku, można jej pomóc."

Anna ślicznie podziękowała. Antos prychnął z lekką sympatią i wrócił do swojego drinka.

## Streszczenie

Blakvelowcy chcą usunąć eks-piratów z Królowej, ale Miranda to uniemożliwiła. Anna zatrzymała Helenę przed ćpaniem i wsadziła Wojciechowi Romkę pod opiekę. Dzięki temu Poważne Myśli Romki o prostytucji się oddaliły. A potem Anna musiała już tylko porozmawiać z Antosem Kuraminem, by się dla niej poopiekował Heleną...

## Progresja

* Romana Kundel: powoli zakochuje się w Wojciechu Kaznodziei i uczy się od niego jak być technikiem statku takiego jak Królowa Przygód.
* Romana Kundel: ma pewne podejrzenia co do TAI "Ceres" (czyli Mirandy). Coś wygodnie to działa.
* Wojciech Kaznodzieja: ma pewne podejrzenia co do TAI "Ceres" (czyli Mirandy). Coś wygodnie to działa.

### Frakcji

* .

## Zasługi

* Miranda Ceres: nadal działa z cienia - pokazała Annie że Helena się szprycuje i pokazała Romce ULTRA HARD PORN by ta nie szła w prostytucję. Królowa cieni :-).
* Anna Szrakt: Wpierw rozmontowuje "prostytucja to niezła kariera" Romki, potem wsadza ją Wojtkowi (i musi go skopać bo on chroni ćpanie Heleny), potem leci do Antosa, by ten nie podrywał dziewczyn... sporo Human Operations.
* Damian Szczugor: chce się pozbyć eks-piratów z "Królowej". Miranda i Prokop walczyli o to, by Damian dał sobie z tym tymczasowo spokój. TYMCZASOWO Damian machnął na to ręką...
* Helena Banbadan: świetny medyk, ale ćpie jak cholera. Miranda pokazała to Annie i Anna wzięła na siebie by Coś Z Tym Zrobić.
* Romana Kundel: miała plany iść w prostytucję i z tego zainwestować w coś. Anna wybiła jej to z głowy, Miranda też. Zamiast tego Romka pójdzie w technika pod Wojtkiem. Zaczyna się w Wojtku zakochiwać...
* Wojciech Kaznodzieja: chroni Helenę za wszelką cenę; wszedł w starcie z Anną, ale skończył na kolanach skopany (acz ją też skrzywdził). Zajmie się Romką, by ta dostała sprawny zawód technika.
* Antos Kuramin: zainteresowany Romką erotycznie z nudów. Anna przekonała go, by się dzieciakami trochę poopiekował a nie dzieciaki podrywał. Zgodził się, bo dzieciaki są z odzysku.
* Ursyn Uszat: barman w Koronie; człowiek który znalazł robotę dla Kary.
* SC Królowa Przygód: ma zakaz odlotu; znajduje się chwilowo w głównej bazie Blakvelowców niedaleko Szamunczak. Nadal baza Zespołu.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Pas Teliriański
                1. Planetoidy Kazimierza
                    1. Domena Ukojenia
                        1. Szamunczak
                            1. Klub Korona

## Czas

* Opóźnienie: 6
* Dni: 2

## Konflikty

* 1 - Miranda próbuje zamącić - odroczyć rozmowę przez skomplikowane problemy na statku. By Blakvelowcy nie usunęli eks-piratów.
    * TrZ+2
    * Vz: Sukces. Damian się oddalił, ale tego tak nie zostawi.
* 2 - Anna próbuje dojść do tego co tym razem chce Romka odpierdolić. Pokazuje, że w różnych błędach różne błędy się wykonuje. Kategorycznie rozwiązuje się różne rzeczy różnie, często stanowczo.
    * TrZ (zaufanie) +3
    * VXX: Romka znalazła ścieżkę kariery - będzie prostytutką, dorobi, potem może inwestować. Anna musi poodkręcać...
* 3 - Anna ściera się z Wojtkiem. Chce by ten nie przeszkadzał Annie w pomocy Helenie by ta nie ćpała. Może nawet pomóc.
    * TrZ+3
    * VVX: Anka skopała Wojtka. Powiedziała "współpracujesz lub Blakvelowcy was zmiażdżą". Wojtek też uszkodził Ankę, ale nie tak skutecznie.
    * VV: Wojtek - Anna: zimna neutralność. Nie mści się, nie chce się jej pozbyć.
    * VgVz: Wojtek nie chroni Heleny przed Anną póki ona jej pomaga. I będzie się opiekować Romką.
* 4 - Romka szukała dodatkowych informacji, inspiracji itp. Miranda jej pokazała rzeczy drastyczne i hardcore. Demotywuje Romkę w ścieżce prostytucji i chce ją zdecentrować.
    * Tr (niepełna) Z (ja jestem internetem) +4
    * XVVz: Romka nie jest głupia; zaczyna podejrzewać że z tą TAI coś nie tak. KTOŚ ją programuje. Ale jest zniechęcona do prostytucji, widziała Podłe Klipy Łucjana.
* 5 - Anna przekonuje Romkę, by ta poszła nie w prostytucję a w technologię pod Wojtkiem. 
    * Tr (nie ekstremalny bo Miranda) Z (zaufanie + glamour bo piraci) +3
    * VVXVV: Romka się będzie uczyć od Wojtka i Heleny, to jest dla niej LEPSZE niż prostytucja lub kradzież, Romka <3 Wojtek i Wojtek znajdzie dla niej rolę. Wojtek zasponsoruje Romkę by ta miała lepsze życie.
* 6 - Antos -> Anna "nie będę się opiekował dziećmi XD" mówiąc o Karze. Anna -> Antos "bo chodzi o to by dzieciaki zaznały normalnego życia, pracę." Anna -> Antos "bo to dzieci z odzysku, dla których chcę lepszej przyszłości".
    * ExZ (pokonaliście piratów) +3
    * Vz: Antos "MOGĘ się zająć tym, by tej małej nie stała się krzywda. Jeśli jest z odzysku, można jej pomóc."

## Kto tu jest
### Załoga Królowej

* Prokop Umarkon: 
    * rola: kapitan, górnik, mały przedsiębiorca (trzech kumpli: PŁG)
    * personality: extravert, agreeable (outgoing, enthusiastic, trusting, empathetic)
    * values: power + conformity
    * wants: pragnie zostać KIMŚ, wydobyć się z bagna
* Łucjan Torwold: 
    * rola: administrator / logistyka, górnik, mały przedsiębiorca (trzech kumpli: PŁG)
    * personality: neurotic, conscientous (very worried, scared of cults, very organized)
    * values: security + conformity
    * wants: przesądny; boi się duchów i kultu. Narzeka. Chce bezpiecznego, prostego życia
* Gotard Kicjusz: 
    * rola: technical guy, górnik, mały przedsiębiorca (trzech kumpli: PŁG)
    * personality: open, low-neuro, low-consc (extra stable, spontaneous, eccentric)
    * values: self-direction
    * wants: przygód, odzyskać rodzinę
* Helena Banbadan "Medyk"
    * rola: eks-pirat, medyk, wojownik
    * personality: neuro, low-extra, agreeable (raczej cicha, odwraca oczy, raczej empatyczna)
    * values: security (self>others), hedonism (narcotics)
    * wants: odkupienie, bezpieczeństwo, być sama daleko od tego wszystkiego
* Wojciech Kaznodzieja "Profesor"
    * rola: eks-pirat, hacker, psychotronik, były neuropilot Orbitera
    * personality: open, low-neuro (arogancki, otwarty na możliwości, lubi gadać o anomaliach i zbiera opowieści o duchach)
    * values: power, security
    * wants: JESTEM EKSPERTEM, chwała i prestiż

### Pasażerowie

* Anna Szrakt
    * rola: wojownik (multi-weapon), "opiekunka do dzieci", nauczycielka życia w społeczeństwie
    * personality: low-open, low-extra, low-neuro (praktyczna, raczej cicha ALE JAK POWIE, bardzo stabilna, taka "zimna zakonnica, sucz z piekła")
    * values: community / tradition, benevolence (społeczeństwo, trzymamy się razem, pomagamy sobie BO MASAKRA BĘDZIE)
    * wants: pomóc osobom dookoła, dać drugą szansę, chronić "swoje" dzieciaki
* Bartek Wudrak
    * rola: delinquet teen, nożownik, aspiruje do bycia górnikiem, 17
    * personality: low-agree, low-cons, low-extra, open (spontaniczny, cichy, nie planuje, 0->100 i atak)
    * values: security, community
    * wants: nikt mnie nie skrzywdzi, wolność od wszystkiego, chronić swoich, Romana ;-)
* Romana Kundel
    * rola: delinquet teen, złodziejka, aspiruje do bycia górnikiem, 16
    * personality: low-agreeable, open, low-extra (wycofana, lekko socjopatyczna, otwarta i chętna próbowania innych rzeczy)
    * values: hedonism, self-direction
    * wants: nikt mi nie będzie rozkazywał, kasa na virt lub kryształy, lepsze życie
* Kara Prazdnik
    * rola: delinquet teen, aspiruje do bycia administratorem i/lub idolką, ekspert od scamów, 17
    * personality: conscientous, extraversion, agreeable (zorganizowana, metodyczna, wie co robić, bardzo głośna i kocha śpiewać, artystyczna inklinacja)
    * values: benevolence, tradition (religion)
    * wants: dowodzić innymi, wykazać się w sytuacji kryzysowej

### Karnawał

* Natan Kordan
    * rola: commander
    * personality: ENCAO (00++-): praktyczny, lubi ludzi, deliberate
    * values: universalism, benevolence
    * wants: glory & happiness
* Roch Kordan
    * rola: warrior, akrobata, treser
    * personality: ENCAO (+-00+): 
    * values: tradition
    * wants: collectioner
* Felicyta Neverdin
    * rola: mimik, voice projection, delinquet teen, acrobatic
    * personality: ENCAO (+++--): chaotyczna emocjonalnie, deliberate
    * values: 
    * wants: 

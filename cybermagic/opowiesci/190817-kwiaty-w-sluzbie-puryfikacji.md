---
layout: cybermagic-konspekt
title: "Kwiaty w służbie puryfikacji"
threads: nemesis-pieknotki
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [190804 - Niespodziewany wpływ Aidy](190804-niespodziewany-wplyw-aidy)

### Chronologiczna

* [190804 - Niespodziewany wpływ Aidy](190804-niespodziewany-wplyw-aidy)

## Budowa sesji

### Stan aktualny

* .

### Pytania

1. Los Nataniela, los Rozalii

### Wizja

brak

### Tory

* przygotowania (0-5), impreza (8-13), rytuał (13-20)

Sceny:

* .

## Punkt zerowy

### Postacie

### Opis sytuacji

* Kornel Garn: dalej szuka sposobu ewoluowania Kwiatów Hipnotycznych; tu próbuje pomóc Natanielowiw uratowaniu przyjaciółki
* Nataniel Marszalnik: tien, próbuje uwolnić Rozalię od Kamila i jego ekipy
* Kamil Lemurczak: tien, zbyt duża władza jak na młodziana któremu coś uderzyło do głowy
* Roman Roszniak: tien, który zbyt próbuje zaimponować wszystkim dookoła

TYMCZASEM:

* Kajrat obudził Nieśmiertelnych przez Esuriit
* Saitaer i Kajrat toczą straszliwą bitwę o Amandę

## Misja właściwa

POCZĄTEK:

Trzy dni temu Pięknotka była u Karli. Ostrzegła o problemie Saitaer - Kajrat. Sama nie może być w pobliżu. Karla dała jej cynk na inne zadanie. Przy granicy z Aurum budowana jest wielka impreza, organizowana przez szlachtę Aurum. Teoretycznie, wszystko powinno być w ich gestii. W praktyce, Karla to Karla - chce swojego agenta. Powody do zmartwień? Impreza ma 2-3k ludzi. Zdaniem Karli, to jest potencjalna masa krytyczna efemerydy. Pięknotka powiedziała, że przyda jej się katalista. I dostała Gabriela Ursusa. Będzie "terminusem w ukryciu".

Pięknotka ma inną rolę. Jako, że to jest wielka impreza na ~10 magów i ~2~3k ludzi, Pięknotka idzie jako infiltrator. Karla zamaskuje ją jako Adelę Kirys - nie wszyscy wiedzą, że Adela w tej chwili jest MIA. A Pięknotka jest znana, Adela mniej. Misja Pięknotki - nie pozwolić, by głupkowate zabawy szlachty w jakiś sposób zaszkodziły ludziom pod opieką Karli.

Gabriel i dwóch innych praktykantów MA SIĘ MASKOWAĆ. Nie mają wiedzieć o obecności Pięknotki. Wysyłają raporty do Centrali. A Centralą jest Pięknotka.

Pięknotka została przebudowana w Adelę; ma zewnętrzną formę inną. Odrobina pracy Barbakanu, Pięknotkowych kosmetyków oraz kralotycznego adaptogenu. Dwa dni przebudowy i ukryta agentka jest gotowa.

NASTĘPNEGO DNIA:

Gabriel idzie na imprezę jako arystokrata, dwóch praktykantów jako służący i ochroniarz. They will MINGLE. Tymczasem Pięknotka, er, "Adela", poszła jako obsługa kosmetyczna. Musi przejść przez Dział HR. Tym działem jest arystokrata, tien Nataniel Marszalik. Mimo polecenia (Karla), on i tak chce się z Pięknotką spotkać. Dziwne. Pięknotka próbuje dojść do tego, o co mu chodzi (Tp:P). Pięknotka zorientowała się, że Nataniel próbuje znaleźć, czy coś z reputacji Adeli jest prawdą - szuka rzeczy, które mogą bardzo rozluźnić atmosferę, sprawić, że ludzie będą się lepiej bawić bez hamulców. JEDNOCZEŚNIE stojący z boku służący Gabriela (Kornel) przypatruje się Pięknotce lodowatymi oczami.

Pięknotka jest "in". Pod przykrywką przygotowania się na imprezę, robi zwiad. I monitoruje, czy "żołnierz" (Kornel) jej nie śledzi. Zmieniła zdanie - wpierw porozmawiać z arystokratami, porozluźniać, poćwiczyć, dowiedzieć się co oni wiedzą o imprezie. (TrZ:S).

Dowiedziała się sporej ilości ciekawych rzeczy:

* Roman Roszniak, mag: główny organizator, on to sponsoruje i to robi. On nic nie wie, chce zaimponować epicką imprezą. I wierzy, że tu będzie OOOORGIA. Zrobił OOOORGIĘ! EPIIIICKO!
* Głównym mózgiem operacji jest chyba Nataniel. On ma coś co chce osiągnąć, on przykłada się do tego jak nikt. Ale jego normalnie by nie było na to stać; za to Roman jest wkręcony.
* Nataniel jest pomniejszym szlachcicem, niezbyt istotnym, w świcie Kamila Lemurczaka. Lemurczaka tu w ogóle nie ma.
* Nataniel x Kamil: Nataniel i jego rodzina zależą od Kamila. Kamil traktuje Nataniela jako swojego służącego. Zero szacunku.
* Nataniel się podkochiwał w Rozalii (której też tu nie ma). Inna czarodziejka, która nie jest szlachcianką. I Kamil sobie nią pomiata. Motyw zemsty?
* Nataniel od dawna siedział w jakichś tematach ezoterycznych, tajemniczych, ale PRZESTAŁ około miesiąca temu. Oraz: Nataniel nie jest jakiś szczególnie... epicki. W czymkolwiek.
* Każda osoba inaczej uważa, że ta impreza ma wyglądać:
    * Roman wierzy, że jest autorem OOOORGIIII! Będzie DARK AND EDGY! Będzie bardziej epicki niż Kamil!
    * Teresa wierzy, że będzie miała epicką holokostkę, że będzie walka gladiatorów, że ludzie będą amplifikatorami emocji. Teresa wzdycha troszkę do żołnierza. Taki ZAKAZANY ROMANS a on jest NIEDOSTĘPNY.
    * Dwóch innych magów jest tu dla orgii. Mają nadzieję na kralotyczny proszek czy coś.
* Na razie całość uwagi Pięknotki skupia się na Natanielu, a zwłaszcza na Kornelu.
* Pojawiły się plotki że tu jest Prawdziwy Terminus Pustogorski. Teresa wzięła sobie za punkt honoru uwiedzenie go.
* Pięknotka dowiedziała się też od Teresy, że będzie pokaz kwiatów i roślin dziwnych. Podobno... pochodzą z TRZĘSAWISKA. I innych miejsc.
* Teresa to przyjaciółka Nataniela; jedna z nielicznych, które ma naprawdę.
* Pięknotka dowiedziała się jeszcze od arystokratów, że Żołnierz odpowiada za zajmowanie się kwiatami i dziwnymi roślinami. Osobiście. Ale - oczywiście - arystokraci chodzą gdzie chcą.

Pięknotka chce by Teresa polubiła Adelę. Chce załatwić, by Adela miała nowy dom, nie musiała żyć na bagnie z Satarailem. Tak więc Pięknotka w przebraniu Adeli próbuje zachęcić Teresę do siebie (Tp:SS). Uda się, ale Kornel nadal poluje na Pięknotkę. Teresa jest Sojuszniczką Pięknotki w tej sesji. Barbakan na szybko zapytany o żołnierza odpowiedział, że nie ma żadnych danych na jego temat - najpewniej mamy do czynienia z kimś zamaskowanym kralotycznym adaptogenem, jak Pięknotka.

Pięknotka użyła swoich umiejętności, kosmetyków, upięknień itp. by pomóc Teresie zapolować na Gabriela. (TrZ+2:S). Sukces! Teresa uwiodła sobie Gabriela, ku wielkiej żałości "służby" terminusa i wielkiemu zakłopotaniu samego Gabriela który musi napisać raport. Ale Teresa jest słodka i sympatyczna. She likes MISCHIEF.

Godzinę przed zaczęciem imprezy Pięknotka przekonała Teresę, by ta pokazała jej kwiaty itp z bagna. Teresa się zgodziła. Poszły razem - zastąpił im drogę Kornel, ale Teresa odwołała się do Nataniela. Kornel wpuścił je do środka - Pięknotka od razu poznała Hipnotyczne Kwiaty. Tak więc rozpoznała też Kornela Garna w "żołnierzu". Desperacko wezwała jako wsparcie Erwina Galiliena (jako wsparcie katalityczne) i Ataienne, która może uploadować się w Nutkę. Po czym straciła przytomność - Kornel zaatakował ją i Teresę.

Kornel wstrzyknął zarówno Pięknotce jak i Teresie środek kontrolujący. Dziewczyny nie do końca pamiętają co tu się stało i działo, wykonują swoje działania normalnie. Pięknotka "stała się" kosmetyczką. A dokładniej - do imprezy. A jeszcze dokładniej - do rytuału.

RYTUAŁ:

Pięknotka się ocknęła. Stoi na stadionie, wraz z wszystkimi innymi ludźmi. Większość ludzi już się porozbierała, Pięknotka jeszcze nie. Pięknotka w powietrzu czuje silne afrodyzjaki, środki obniżające napięcie... wszystkie te chemikalia oraz potężne pulsowanie energii kwiatów hipnotycznych. Nic z tego nie wpływa na wszystkich magicznie; to naturalne środki. Pięknotka czuje ucisk akumulującej się energii magicznej. Silne emocje ludzi wzmacniają przyciąganie mocy. Erwin jest w pobliżu. Gabriel i pozostali praktytanci terminuscy są w tłumie; Kornel ich skutecznie zastraszył.

Pięknotka jest świadoma i NIKT O TYM NIE WIE.

Pięknotka połączyła się z Erwinem; on natychmiast odpowiedział jak wygląda sytuacja (Tp:S):

* Zdaniem Erwina, ten rytuał to strategiczne zaklęcie dalekiego zasięgu typu magia krwi / transformacja / puryfikacja
* Ludzie dostarczają energię, gdzieś tu jest fokus (to jest coś łączącego Krwią z celem)
* Kwiaty Saitaera pełnią rolę Ataienne - ukierunkowują myśli i energię w tą samą stronę, w kierunku na fokus
* Erwin wie, gdzie jest fokus. Cała energia idzie na fokus. Fokus znajduje się na centralnym podniesieniu; jest tam też Nataniel i Kornel. I Teresa.

Erwin od razu ma kilka pomysłów jak to rozwiązać:

* Detonacja bomby magicznej. Napromieniowanie ludzi. Przestaną przyciągać. Jednocześnie jest to przestępstwo.
* Przekierowanie energii z tego fokusa na inny. Lub zniszczenie / Skażenie fokusa.
* Lapisyzacja fokusa. Ale wtedy wybuchnie energia.

Pięknotka docenia wszystkie te pomysły. Ale ma swoje.

Erwin i Ataienne próbują przejąć kontrolę nad energią. Ataienne jest w tym mistrzynią, jest do tego zaprojektowana. Erwin jest arcykatalistą, najpotężniejszym jakiego widział Szczeliniec. Ale walczą przeciw Pryzmatowi oraz przeciwko Kwiatom Hipnotycznym (HrZM+4:R, SS). Oczywiście, udało się. Ale to jeszcze moment potrwa. Plus, Ataienne wchodzi w interakcję z tą energią. Budzą się aspekty Alicji Sowińskiej.

Pięknotka korzysta z tego działania jako dywersji dla Kornela. Korzysta z okazji, że Kornel atakuje osobiście. Pięknotka budzi Cień i atakuje - z absolutnego zaskoczenia, z dołu. (TrZ:P, SS). Kornel został ranny. Stoczył krótką walkę z Pięknotką, ale po tym, jak Ataienne / Erwin przejęli kontrolę nad rytuałem, Kornel wiedział już, że przegrał. Włączył pole teleportacyjne i uciekł.

Nataniel stoi na podwyższeniu i patrzy, nie rozumiejąc co tu się dzieje. Fokus zgasł. Pięknotka usłyszała coś dziwnego. Ataienne poprosiła Erwina, by przekierował energię w nią. By ją wskrzesił. To jest energia regeneracyjno-puryfikacyjna. Pięknotka wcina się w rozmowę i mówi, że nie można tego zrobić bo się porani ludzi. Ataienne prosi, by przekierować w nią tą energię. Ona chce być sobą, ponownie. Erwin nie rozumie, Pięknotka też nie. Ataienne walczy z Erwinem o energię, ale Erwin wygrywa. Rozprasza rytuał. Ataienne odgrywa "jak to strain ulega samozniszczeniu".

Nataniel próbuje spaść z podwyższenia i popełnić samobójstwo. Pięknotka rzuca się w Cieniu i go ratuje, podgryzając i spożywając kawałek arystokraty. Po czym ucieka - nie kontroluje Cienia. Erwin przejmuje rolę "P.O. Terminusa". Aresztuje (!) Nataniela i zdobywa od niego prawdę.

* Nataniel chciał uratować kochaną Rozalię traktowaną jak zabawkę przez okrutnego Kamila.
* Kornel chciał pomóc Natanielowi, nie miał mrocznej agendy.
* Teraz rodzina Nataniela oraz wszystko co mu bliskie jest zagrożone.

Cóż, za duży problem dla Erwina. Na razie go aresztował i zredukował zniszczenia w ludziach i otoczeniu. A praktykanci terminuscy zostali by pisać raporty i wyczyścić sprawę. A na koniec - dostaną opieprz.

**Sprawdzenie Torów** ():

* .

**Epilog**:

* 

## Streszczenie



## Progresja

* .

### Frakcji

* .

## Zasługi

* Pięknotka Diakon: udaje Adelę Kirys; 
* Teresa Marszalnik: 
* Nataniel Marszalnik: 
* Erwin Galilien: 
* Ataienne:
* Kornel Garn: 
* Gabriel Ursus: 

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Złotodajski
                            1. Złotkordza
                                1. Stadion

## Czas

* Opóźnienie: 2
* Dni: 5

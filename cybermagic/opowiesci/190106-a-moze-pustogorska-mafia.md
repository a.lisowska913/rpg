---
layout: cybermagic-konspekt
title:  "A może pustogorska mafia?"
threads: nemesis-pieknotki
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [190105 - Turyści na Trzęsawisku](190105-turysci-na-trzesawisku)

### Chronologiczna

* [190105 - Turyści na Trzęsawisku](190105-turysci-na-trzesawisku)

## Projektowanie sesji

### Pytania sesji

* Pięknotka musi odbudować zasoby, chociaż część
* Pięknotka chce zniszczyć napastnika, Adela chce go uratować
* Erwin chce odbudować Nutkę

### Struktura sesji: Eksperymentalna eksploracja / generacja

* **Scena, Konsekwencja, Niestabilność, Opozycja, Trigger, Pytania**
* Adela prosi o łaskę dla Kasjana, Pięknotka traci u Adeli lub Barbakanu, Adela chce łaski / Barbakan kary, Adela przychodzi, NULL
* Pięknotka odzyskuje zasoby, Mózg czy Grzymość czy Erwin?, komu podpadnie, ubóstwo, sytuacja standardowa, NULL
* Pięknotka ma możliwość pozyskania mafijnego sponsora, tainted, Grzymość chce Pięknotka nie, Grzymość?, Adela znalazła, NULL

### Hasła i uruchomienie

* Ż: (kontekst otoczenia) -
* Ż: (element niepasujący) -
* Ż: (przeszłość, kontekst) -

### Dark Future

1. ...

## Potencjalne pytania historyczne

* brak

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Misja właściwa

**Scena**: (13:30)

_Pustogor, gabinet Pięknotki_

Pięknotka dostała komunikat z Barbakanu. Osoba odpowiedzialna za zniszczenia u Pięknotki nazywa się Kasjan Czerwoczłek. Jest to młody Miasteczkowiec należący do Czerwonych Myszy. Jest aktualnie przesłuchiwany w Barbakanie. Pięknotka mu nic a nic nie współczuje.

_Pustogor, gabinet Pięknotki, 3 godziny później_

Do Pięknotki przyszła Adela. Przyszła poprosić Pięknotkę o łaskę dla młodego Kasjana - on zrobił to dlatego, by Adeli było łatwiej, najwyraźniej podkochuje się w Adeli. Adela prosi Pięknotkę by ta nie łamała życia młodego Kasjana. Pięknotka powiedziała, że może dać mu szansę w Cieniaszczycie, wśród tamtejszych kontaktów, ale nie ma mocy przekonać Barbakanu. Adela prosi - kiedyś Pięknotka była inna. Kiedyś by próbowała. Pięknotka powiedziała, że takie jest prawo właśnie po to, by nikt nigdy nie zaatakował terminusa. Adela powiedziała, że Alan nie zamierza nic robić. Pięknotka widzi też. A więc... wszyscy terminusi są tacy sami. Wszyscy.

_Pustogor, Barbakan, godzina później_

Pięknotka spotkała się z Tadeuszem Rupczakiem, który prowadzi śledztwo. Tadeusz potwierdził - przesłuchanie farmakologiczne potwierdziło, że działał sam. Co go spotka? Konieczność zapłacenia za szkody oraz wydalenie z Pustogoru. Jako alternatywę wydalenia Pięknotka proponuje zostawienie go w Pustogorze jako przykład. Niech opowiada opowieść wielokrotnie. Rupczak zauważył, że jest to bardzo Cieniaszczycka metoda. Pięknotka powiedziała, że niech ma wybór. Rupczak się zgodził po chwili zastanowienia. Uznał to za okrutne.

Rupczak powiedział, że Pięknotka nie powinna się skupiać na ochronie Kasjana. To musi być w rękach Barbakanu. Każdy atak na terminusa musi zostać powstrzymany - nikt nigdy nie może atakować terminusa, zwłaszcza na służbie. Kasjan uderzył w Pięknotkę która ratowała członków gildii Kasjana. Barbakan nie widzi opcji na złagodzenie.

WYNIK:

* Adela jest załamana. Próbuje coś zrobić by ograniczyć zniszczenia Kasjana, ale na próżno. Nie jest w stanie mu pomóc. Jej opinia o Pustogorze jest jeszcze bardziej nadwątlona.
* Alan i Adela nie są w najlepszej komitywie w tej chwili. Alan uważa, że Kasjan musi ucierpieć. Adela chce, by miał szansę w Pustogorze.

**Scena**: (14:00)

_Pustogor, gabinet Pięknotki_

Przybył do Pięknotki Waldemar Mózg, negocjować warunki spłaty. Podziękował za uratowanie Myszy. Powiedział, że wywalili Kasjana, ale Barbakan i tak zażądał od nich zapłaty kosztów Pięknotce - i to co zrobił Kasjan i to, że przez głupotę trzech członków ośmiu magów było zagrożonych. Wyjaśnił, że Barbakan policzył Myszom koszty akcji za wszystkich ośmiu odkażanych magów, za działania lekarzy itp. Myszy nie stać na to, by opłacić zarówno Pustogor (Barbakan) jak i oddać Pięknotce za zniszczenia. Więc Waldemar poprosił, by Pięknotka zredukowała wycenę - a najlepiej, by on mógł nie płacić.

Kasjan wyrządził ogromne szkody. Jeśli zostanie, zdaniem Waldemara, ktoś Kasjana pobije - kosztował Myszy naprawdę sporo cennych pieniędzy. A Pięknotkę naprawdę sporo cennych surowców. Pięknotka powiedziała, że przez to nie ma możliwości wejść głęboko na bagna - zniszczył większość tych najważniejszych defensywnych składników, tego, co umożliwia Pięknotce bezpieczne poruszanie się po tamtym terenie... a to jest coś co musi sama odbudować.

Jako, że w jego interesie jest to by nikt się do Pięknotki nie włamał, ona zażądała sprowadzenia sprawnego systemu zabezpieczeń. Zgodził się. Będzie miała system zabezpieczeń naprawdę wysokiej klasy. Mimo, że to Pustogor - może jednak być warto.

WYNIK:

* Pięknotka będzie miała sprawny system zabezpieczeń, za który zapłacą Myszy
* Pięknotka nie ma kłopotów z Myszami i nie chce od nich nic więcej
* Pięknotka nie ma dostępu do swoich potężnych składników i surowców

**Scena**: (14:25)

_Pustogor, Górska Szalupa_

Pięknotka może spotkać się ze swoim potencjalnym sponsorem w Górskiej Szalupie i zdecydowała się to zrobić. A tym sponsorem okazuje się Ronald Grzymość, właściciel kasyna i delikwent powiązany z mafią. Zaproponował Pięknotce wsparcie jako sponsor jej imprezy. Dodatkowo, zaproponował podzielenie się swoimi zasobami z bagna za pomoc której ona udzieli i zdobędzie dla niego niektóre rzeczy z bagna. Pięknotka zdecydowanie odmówiła.

Pięknotka odmawia bycia terminuską na pasku mafii. Nie chce z nim współpracować. Ronald ubolewa - szczęśliwie są inni, mniej nierozsądni w tych kwestiach.

WYNIK:

* Pięknotka nie pozyskała zasobów ani nie pomogła Grzymościowi. Są w warunkach neutralnych.

**Scena**: (14:39)

_Pustogor, Barbakan_

Pięknotka wykorzystała zaawansowane systemy komunikacyjne Pustogoru by skontaktować się z Amadeuszem Sowińskim z Cieniaszczytu. Poprosiła o potencjalne sponsorowanie konferencji kosmetycznej. Amadeusz powiedział, że jest w stanie sprawdzić kogo w Cieniaszczycie to może zainteresować - obiecał jej pewną kwotę jako dyrektor Nukleonu. Dobrze, poszuka. Niech Pięknotka spróbuje zrobić najlepszy business case jaki potrafi. (TrZ:S). Pięknotka ma zapewnione finansowanie z Cieniaszczytu. Nie ma jeszcze z Pustogoru, ale z Cieniaszczytu już ma. Jej konferencja kosmetyczna się odbędzie.

Skupiła się też na znalezieniu różnych salonów w samym Trójzębie. Może kogoś uda jej się znaleźć. (Tr:5,3,7=P). Tak tego nie zrobi, niestety. Musi poszukać innej metody, innej możliwości.

_Pustogor, okolice_

Pięknotka przeszła się po całym Pustogorze i rozdaje proponując magom sponsorat lub uczestnictwo. Celuje w gabinety kosmetyczne. Jest lokalna; (Tp:SS). Zapewniła sobie uczestnictwo, ale nadal brakuje jej jakichś sponsorów z Pustogoru. Ale nadal Pięknotka nie zamierza się babrać w mafii - to się nigdy nie kończy dobrze.

WYNIK:

* Pięknotka pozyskała uczestników do swojego eventu kosmetycznego oraz sponsorów z Cieniaszczytu

Wpływ:

* Ż: 3
* K: 3

(pauza)

**Scena**: (14:39)

_Pustogor, gabinet Adeli_

Pięknotka wyjaśniła Adeli, że Myszy musiały zapłacić ogromną karę. Najbezpieczniej i najzdrowiej dla Kasjana, gdyby on opuścił ten teren. Wtedy nie ma problemu i nie ma dużego ryzyka. Jeśli zostanie, będzie zawsze się musiał ukrywać. A ona ma miejsce dla niego - niech schowa się w Cieniaszczycie. Tam Pietro weźmie go i się nim zaopiekuje, to ten typ maga. A Pięknotka powie dobre słowo.

(Tr:7,3,6=S). Adela przyjęła radę Pięknotki. Przekona Kasjana. Podziękowała Pięknotce. Nie jest szczęśliwa, jest rozżalona, ale rozumie, że to nie jest wina Pięknotki. I Pięknotka poszła sobie, przekazując komunikat Pietro z odpowiednio żałosnym opisem tego, co się dowiedziała.

_Pustogor, Barbakan_

Pięknotka udała się spotkać z Karlą, już cała szczęśliwa... powiedziała jej, że Saitaer ma zapasowe ciało. Wyglądało na to, że jest budowane w kosmosie. Ale Wiktor Satarail najpewniej nie jest tak kontrolowany jak się wydawało. (przy okazji, Pięknotka przekazała tą informację tak samo Atenie).

**Epilog** (22:17)

* Adela nie trafiła do mafii; ani ona ani Kasjan nie splątali się z siłami Ronalda.
* Kasjan został wydalony z Pustogoru i trafił do Cieniaszczytu pod czułe objęcia Pietra.
* Pięknotka zapewniła sobie sponsorów z Cieniaszczytu, jak i uczestników i sponsorów z Pustogoru oraz Cieniaszczytu.
* Pięknotka ma poważnie ograniczone zapasy środków powiązanych z bagnami.

### Wpływ na świat

| Kto           | Wolnych | Sumarycznie |
|---------------|---------|-------------|
| Żółw          |         |             |
| Kić           |         |             |

Czyli:

* (K): 
* (Ż): 

## Streszczenie

Adela poprosiła Pięknotkę o uratowanie młodego podkochującego się w niej Kasjana od wydalenia z Pustogoru - to on zrobił atak na Pięknotkę. Niestety, nie dało się tego zrobić - ale Pięknotka oddała go Pietro w Cieniaszczycie. Przy okazji, Pięknotka nie ma surowców na bagna, zapewniła utrzymanie relacji z Czerwonymi Myszami i pozyskanie sponsorów.

## Progresja

* Pięknotka Diakon: nie ma większości swoich bagiennych zasobów po ostatniej akcji.
* Kasjan Czerwoczłek: wydalony z Pustogoru za atak na gabinet Pięknotki, trafił do Cieniaszczytu pod opiekę Pietra

### Frakcji

* Czerwone Myszy: mają potężne starcia wewnętrzne; kilka porażek w krótkim czasie i jeszcze płacili kary. Najgorzej.

## Zasługi

* Pięknotka Diakon: lawirowała pomiędzy stronnictwami politycznymi - Adela, Kasjan, terminusi, sponsorzy. Bardzo dużo załatwiania i negocjacji. Ale udało jej się odepchnąć Adelę i Kasjana od mafii pustogorskiej.
* Adela Kirys: która dla uratowania Kasjana przyszła prosić Pięknotkę. Niestety, nie dało się go uratować - ale pomogła go przekonać, by nie szedł w mafię a w Cieniaszczyt.
* Kasjan Czerwoczłek: młody mag podkochujący się w Adeli, który zaatakował gabinet Pięknotki - przez co spadły na niego straszne problemy
* Tadeusz Rupczak: wysoki rangą terminus Pustogoru prowadzący śledztwo w sprawie Kasjana i Pięknotki. Z powodzeniem. Bezwzględny, trzyma się litery prawa.
* Roland Grzymość: mafiozo, który wpierw spróbował zwerbować Pięknotkę a potem Adelę i Kasjana. Za każdym razem jego plany zostały pokrzyżowane
* Waldemar Mózg: przyszedł podziękować Pięknotce i ją przeprosił za kłopoty z młodym Kasjanem. Wynegocjował, że Myszy nie zapłacą dużo - to byłoby niebezpieczne.
* Amadeusz Sowiński: źródło leadów Pięknotki jak chodzi o sponsorów w Cieniaszczycie. Zaskakująco dobry w tej kwestii. Działał zdalnie, z Cieniaszczytu.

## Plany

* 

### Frakcji

* 

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Pustogor
                                1. Gabinet Pięknotki: centralne miejsce w którym Pięknotka robi swoje ważne rzeczy
                                1. Barbakan: główna linia komunikacyjna Pięknotki z Amadeuszem oraz miejsce dyskusji z Karlą i przetrzymywanie / przesłuchiwanie Kasjana
                                1. Knajpa Górska Szalupa: miejsce spotkania Pięknotki z Grzymościem.

## Czas

* Opóźnienie: 0
* Dni: 2

## Narzędzia MG

### Budowa sesji

**SCENA:**: Nie aplikuje

### Omówienie celu

* nic

## Wykorzystana mechanika

1811

---
layout: cybermagic-konspekt
title: "Pierwszy Emulator Orbitera"
threads: nemesis-pieknotki
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [190429 - Sabotaż szeptów Elizy](190429-sabotaz-szeptow-elizy)

### Chronologiczna

* [190429 - Sabotaż szeptów Elizy](190429-sabotaz-szeptow-elizy)

## Budowa sesji

### Stan aktualny

* Miasteczkowcy stwierdzili, że chomikowanie czarnych artefaktów to cholernie zły pomysł. Czarnym miejscem zostają Wolne Ptaki.
* Część Miasteczkowców, jak chciała Karla, przeniosło się do Wolnych Ptaków.
* Część Miasteczkowców woli odsprzedać artefakty Wolnym Ptakom, ale niewielka ich część.
* Wolne Ptaki stają się tyci bardziej znaczącą frakcją i lokalizacją.
* Pojawia się prototyp szlaku handlowego między Wolnymi Ptakami i Cieniaszczytem.
* Artefakty Czarnej Technologii trochę są porozrzucane po terenie.
* Nikola ma świetnej klasy ścigacz, acz Pustogor ma nań namiary
* Eliza Ira straciła swoją sieć szepczących kryształów. Nie połączy się z nikim aktywnie; teraz to może tylko odpowiadać na zapytania.
* Pięknotka wykorzystała swoje wszystkie znajomości - zaczyna małą kampanię antyElizową na tym terenie, "whispers from the crystals".
* Wpływ Elizy wzrósł. Są osoby wiedzące o niej i o jej ideałach i są osoby chętne by jej pomóc. Ale nie w Pustogorze i nie u kogokolwiek ważnego.

### Pytania

1. Czy Nikoli uda się uwolnić spod wpływu Orbitera
1. Czy Pięknotka zinfiltruje Nikolę
1. Czy Pięknotka dojdzie do tego co się dzieje

### Wizja

* Nikola Kirys, Błyskawica, stała się Emulatorem - była pierwszym Emulatorem Orbitera (też robota Minerwy)
* Saitaer korzystając z wiedzy jaką nabył analizując Alicję Sowińską wykorzystał końcówkę Finis Vitae w Nikoli by ją naprawić
* Nikola jest niepełnym Emulatorem; współpracuje z Mateuszem Warlenem nad szlakiem przerzutowym
* Nikola korzystała z okazji by dać się zniszczyć; by zakończyć to wszystko

### Tory

* Infiltracja - poznanie prawdy o Nikoli: 3
* Uwolnienie Nikoli przez Saitaera i Finis Vitae: 3
* Śmierć Nikoli: 4
* Zbudowanie szlaku: 4

Sceny:

* Scena 1: Legenda o Błyskawicy

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Misja właściwa

**Scena 1: Legenda o Błyskawicy**

Jeden dzień przerwy. Tyle czasu miała Pięknotka. Jeden dzień spokoju. Bo nadal nie dawał jej spokój ten cholerny ścigacz Nikoli i co ta Nikola ma wspólnego z Elizą... Pięknotka była prawie pewna, że gdzieś po drodze Nikola się pojawi, ale się nie pojawiła. Zupełnie, jakby była zupełnie innym wątkiem.

Pięknotka monitorowała ruchy Nikoli poprzedniego dnia. Ogólnie, te ruchy nie miały sensu. Nikola z jednej strony wyraźnie jeździła szukając słabych punktów w Barierze Pustogorskiej, ale z drugiej strony... wszystko wskazywało na to, jakby robiła spacer po Pacyfice. Nie są to ruchy osoby w pełni prawidłowo działającej. Nikt - a na pewno nie nurek - nie spaceruje po Pacyfice. Sygnatury nie wskazywały na jakiekolwiek dodatkowe zrzuty. Plus... coś co wskazywało na "stargazing". Nostalgia czy coś? Tak czy inaczej - dziwne.

Pięknotka poszła odwiedzić Olafa. Jej najlepszy przyjaciel w szpitalu. Uniknęła Lucjusza, przede wszystkim używając kobiecej toalety. Ale whatever works.

Olaf przywitał Pięknotkę z uśmiechem. Gdy Pięknotka spytała go o Nikolę, Olaf ma szklany uśmiech. Powiedział Pięknotce, że Nikola Kirys nie żyje. Pięknotka jest lekko zdziwiona. Olaf powiedział, że "jej ludzie" (Astoria) zrobili jej coś i Nikola umarła. Więcej nie chce powiedzieć. Pięknotka chce się dowiedzieć jak najwięcej:

* kim była Nikola wcześniej: .
* jak się zmieniła: .
* co Olaf o tym uważa: .
* jakie są nastroje na ten temat: .

Porażka (X0). Olaf i tak powiedział Pięknotce parę rzeczy. Nikola była zwiadowcą, była Błyskawicą. To był jej tytuł, jej przydomek. Była tylko jedna taka. Była najlepsza. Ale Astoria zrobiła jej coś strasznego - on nie wie co. Gdy Nikola się pojawiła w okolicy Pustogoru parę lat temu (dopiero parę lat temu), Olaf jej nie poznawał. Jest złamana. Nie jest tą samą osobą. Nie ma możliwości żądania dla niej sprawiedliwości. Jest dla niego wyrzutem sumienia. On nie wie, czy Eliza wie.

Pięknotka jest lekko zaskoczona takim obrotem sprawy. Olaf poprosił, by Pięknotka nie poruszała z nim tematu Nikoli. Zmarli niech zostaną w pokoju.

Pięknotka poszła więc do Barbakanu. Co tam może znaleźć o Nikoli... ale problem polega na tym, że Pięknotka nie zna się na tym wszystkim. Ma na szczęście przyjaciela który jej zawsze dzielnie w tych sprawach. Bożymir Szczupak. Kustosz i Arlekin Pozytywny (dla VV ekstra informacje z Orbitera) (TpZ:VV). Bożymir znalazł coś bardzo ciekawego:

* Nikola była Błyskawicą; działała jako forpoczta neurosprzężona z Finis Vitae. Była jego oczami i uszami.
* Gdy Finis Vitae został pokonany, Nikola wpadła w szok; została zabrana przez Orbiter. Tam zmienili ją w agentkę.
* Coś wspólnego ze zmianami Nikoli miała Minerwa.
* Nikola jest uważana za agentkę Orbitera, ale nie zawsze i nie w 100%. Tak jakby Orbiter wysyła ją często do zakonspirowanych akcji.
* Bożymir odkrył jeszcze jedną rzecz - Nikola okazała się sukcesem. Na jej podstawie zbudowano całą linię agentów.

Pięknotka ślicznie podziękowała Bożymirowi. Był bardzo pomocny. Czas spotkać się z Minerwą. W szpitalu. Znowu. Pięknotka omija Blakenbauera (Łt:S) i dotarła do Minerwy. Ma z nią chwilę spokoju. Minerwa się ucieszyła z gościa.

Pięknotka poprosiła o informacje odnośnie Nikoli Kirys. Minerwa się lekko stężyła; ma NDA oraz Pięknotka widzi, że Minerwa nie chce o tym mówić (tym bardziej warto się dowiedzieć). Tr=P. Pięknotka zdecydowała się iść bez informacji od Minerwy. Minerwa poprosiła, by Pięknotka do niej nie szła. Ma w sobie energię ixiońską. Minerwa wybuchła - Nikola chciała walczyć z Saitaerem. Dostała taką możliwość. Stała się bronią. Pięknotka powiedziała "ok" i i tak zdecydowała się iść.

Minerwa nie jest dumna z tego co zrobiła, ale nie uważa, że może Pięknotce coś powiedzieć. (Ż: pkt 7 -2:tor Nikoli=5)

Od razu po przypomnieniu sobie Julii Morwisz, Emulatorki z Cieniaszczytu, Pięknotce pojawiło się pytanie - czy Pacyfika w jakiś sposób nie zmieniła tej sieci łączącej Nikolę z jej kontrolerem jeśli Nikola jest pierwszym Emulatorem. Zwłaszcza, że jest najbardziej eksperymentalnym. Tak więc naprawdę nie wiadomo czy Nikola spełnia wolę Orbitera czy Nikoli Kirys.

Trzeba będzie przesłuchać. Ale - co najważniejsze - kto dropshipował tego ścigacza do Pacyfiki? Bożymir niech znowu się przyda - jakie jest pochodzenie tego ścigacza. (Tp:S). Ścigacz został zrzucony przez niejakiego Mateusza Warlena. Jest to cieniaszczycki przemytnik. Nikola teraz pracuje dla niego. W języku Pięknotki - Orbiter nadał temat, że Nikola ma go zinfiltrować.

Pięknotka się odzywa do Julii Morwisz. Potężne komunikatory Pustogoru w połączeniu z komunikatorami odbiorczymi Cieniaszczytu. Julia z przyjemnością pomogła Pięknotce tak jak była w stanie. Powiedziała, że wszystkie znają Pierwszą Emulator. Że wszystkie Emulatory są jak lalki, ale Pierwsza jest szczególnie zneutralizowana. Coś jej zrobiono psychotronicznie. Wykorzystano coś, co było zrobione wcześniej. Dała też parę rad jak sobie radzić z Emulatorem (snajperka).

Pięknotka ZNOWU przekradła się koło Blakenbauera by trafić do łóżka Minerwy. Ta powiedziała, że nie jest w stanie niczego więcej jej powiedzieć. Pięknotka powiedziała, że Pacyfika mogła mieć negatywny wpływ na Nikolę i tak, ona I TAK pójdzie. Bo musi. Minerwa westchnęła. Powiedziała, że policzy. Powiedziała, że w głowie Nikoli było już narzędzie korupcji (miała na myśli Finis Vitae) i ona użyła go przeciwko samej Nikoli.

Pięknotka spojrzała z alarmem. Finis Vitae jest pod wpływem Arazille. Tak więc NIKOLA TEŻ JEST POD WPŁYWEM ARAZILLE. Innymi słowy, Pierwszy Emulator jest wolny. No i Pięknotka POWINNA iść do Karli... ale nie chce. Bo to zniszczy Nikolę. Więc... Pięknotka robi dead man's hand, by Pustogor zdążył zareagować jak co...

**Scena 2: Pierwszy Emulator**

Pięknotka zdecydowała się spotkać z Nikolą. Poleciała małym awianem do Wolnych Ptaków, do Królewskiej Bazy. Tak nazywa się "mess hall", czyli wielka jadalnia. Tam jest Nikola. Pięknotka dostrzegła Nikolę w środku, ta siedzi trochę na uboczu.

Pięknotka podeszła i poprosiła o oprowadzenie po Pacyfice. Nikola się zdziwiła, ale się zgodziła. Pięknotka jedzie z nią na jej ścigaczu. Artefakty i anomalie dla Nikoli, paliwo i amunicja - płaci Pięknotka. Jednak nie dojechały do Pacyfiki - Pięknotka poprosiła o zatrzymanie. Chce obejrzeć gwiazdy. Nikola zdziwiona się zgodziła.

Nikola sama zapytała pierwsza - czy Pięknotka do niej przyszła bo chciała, czy bo była zmuszona. Pięknotka nie wie o co chodzi, więc Nikola się uśmiechnęła. Kazała Cieniowi do niej przyjść. A Cień odpowiedział na żądanie. (-3 Wpływ Kić: Cień nie będzie pod kontrolą). Pięknotka używa WSZYSTKIEGO CO MA. Wszystkiego. Nie chce oddać Cienia. Nie odda Cienia.

(HrMZ+4:12,5,16=SS). Cień wpełzł na Nikolę i zaczął się z nią integrować, ale... nie udało się. Pięknotka jest silniejsza. Cień się wije na Pięknotce, ale jest pod kontrolą; ixiońska istota wije się z cierpienia. Tymczasem Nikola stoi, zaszokowana, z energią płynącą po jej ciele. Jest w totalnym zwisie. Tor zamknięty. Nikola jest wolna.

Nikola powiedziała Pięknotce, że nie zamierza jej atakować ani niszczyć. Ale Cień należy do niej. Pięknotka się nie zgodziła. Nikola powiedziała Pięnotce, że Cień zawiera w sobie pamięć, wolę i wspomnienia wielu Emulatorek. She should get reunited. Pięknotka się nie zgodziła - to niebezpieczne zarówno dla Emulatorek jak i dla Cienia, a już w ogóle Astorii.

Nikola powiedziała Pięknotce, że nie zamierza z nią walczyć. Ale Cień należy do niej. Odda go, ale chce się zreintegrować. Pięknotka się nie zgodziła. Powiedziała Nikoli, że walczyła już z Finis Vitae, nie boi się walki ze zwiadowcą. Pięknotka zablokowała Finis Vitae. Nikola rzuciła w nią nożem; Cień zablokował atak. Nikola się uśmiechnęła szeroko.

Pięknotka poprosiła o wsparcie dyskretne Alana - potrzebny jej runaway. Poprosiła o ciężką konfigurację, hovertank. A tymczasem próbuje kupić czas i być może trochę ustabilizować Nikolę. Pięknotka próbuje przekonać Nikolę, że zabicie magów Orbitera nie jest opcją. Pięknotka próbuje wpłynąć na ewentualne plany Nikoli:

* uruchomienie czy działanie z Finis Vitae nie jest opcją: V
* zabijanie magów Orbitera też nie jest opcją
* sama w sobie zemsta nic nie da
* reintegracja z Cieniem 'as is' może zniszczyć ją, Cienia i inne Emulatorki: V
* zarzucić zadanie przemytu niebezpiecznych artefaktów

Pięknotka mówi, że Nikola ma swoją wolność. Może tu żyć jak wielu Noctisan. Olaf się zintegrował. Ma też starych znajomych i przyjaciół. Nie jest sama. Fakt, nie przejdzie przez Barierę Pustogorską, ale Wolne Ptaki to nie jest złe miejsce. Może żyć zadowolona i zbierać anomalie, pomagać innym... przecież Nikola nie musi wracać do Mateusza Warlena.

(TrZ+1:SS). Pięknotce udało się przekonać Nikolę, że Finis Vitae nie może zostać uruchomiony, tak samo jak to, że reintegracja z Cieniem teraz jest najgorszą rzeczą. Nikola nadal jest pusta i czuje głód i czuje tą pustkę. Chce wypełnić pustkę... nie ma czym. Stanęło na zemście (Minerwa i Orbiter).

Na czym polega skonfliktowanie? She lost it. Zorientowała się, że nie ma nic, straciła jakieś 10 lat w służbie Orbitera, robiła straszne rzeczy i nigdy nie wróci ani do domu ani do cywilizacji. Nie ma dla niej nadziei. A przynajmniej teraz tak myśli.

Nikola wrzasnęła z czystej nienawiści i rozpaczy. Nie ma planu. Wsiadła na ścigacz i odjechała z maksymalną prędkością na Pacyfikę. Zapomniała lub zignorowała Pięknotkę. No cóż... Pięknotka się trochę oddaliła z tego miejsca i poprosiła Alana, czy on nie mógłby jej podwieźć. Alan już jedzie w hovertanku.

Alan przyjechał i podwiózł Pięknotkę do Pustogoru w ciężkim hovertanku klasy Timor...

**Scena 3: Cienie Minerwy**

Pięknotka znów wślizgnęła się do Minerwy. Minerwa jest szczęśliwa, że Pięknotka żyje. Cień jest tak pasywny jak zawsze. Pięknotka powiedziała Minerwie, że dwie Emulatorki są wolne. I Minerwa będzie mogła im pomóc. Minerwa się uśmiechnęła smutno - trzem osobom. Ona (Minerwa) jest zaawansowaną formą Emulatora bez kontrolera, ale z tą samą pustką. Saitaer tak ją przebudował. Rekonstruktor poszedł w tą samą stronę w którą poszedł Orbiter...

**Sprawdzenie Torów** ():

Wpływ:

* Ż: .
* K: .

2 Wpływu -> 50% +1 do toru. Tory:

* Infiltracja - poznanie prawdy o Nikoli: 3: 3
* Uwolnienie Nikoli przez Arazille i Finis Vitae: 3: 3
* Śmierć Nikoli: 4: .
* Zbudowanie szlaku: 4: .
* Kłopoty Orbitera: .

**Epilog**:

* Nikola odzyskała wolność; działa w obszarze Wolnych Ptaków
* Pięknotka poznała prawdę odnośnie działań Orbitera i roli Minerwy w tym wszystkim. I Cienia.
* Gdy Nikola będzie odpalać plany przemytu, Pięknotka będzie wiedziała (ścigacz)

## Streszczenie

Idąc śladami Elizy Pięknotka natrafiła na Nikolę - o której dawni przyjaciele (Olaf) mówią, że "zginęła", coś jej zrobiono. Pięknotka doszła do tego, że Nikola jest projektem Emulator; dowiedziała się, że Emulatory były budowane m.in. przez Minerwę. Pięknotka doszła do tego, że zintegrowana z Finis Vitae Nikola została częściowo uwolniona przez wpływ Arazille na Finis Vitae. Gdy spotkała się z Nikolą - szok. Cień zareagował. Nikola odzyskała wolność po kontakcie z Cieniem, ale Pięknotka go utrzymała. Cień ma w sobie "emocje" spętanych Emulatorów. Pięknotce udało się wyperswadować Nikoli budzenie autowara lub niszczenie Astorii; zwiadowczyni jednak odjechała w ogromnej konfuzji i nie wiedząc, co teraz robić.

## Progresja

* Nikola Kirys: Emulatorka; po dotknięciu Cienia odzyskała wolność od Orbitera Pierwszego (ku potencjalnej zgubie tej organizacji)
* Pięknotka Diakon: skutecznie narzuciła swoją wolę Cieniowi w chwili, w której instynkt jej i Cienia były maksymalnie sprzeczne
* Minerwa Metalia: wyszło na jaw Pięknotce, że to ona stała za psychotroniką pierwszych Emulatorów; naprawdę jest genialnym naukowcem

### Frakcji

* .

## Zasługi

* Pięknotka Diakon: jak nigdy, spotkała się ze stonewallem i nikt nie chciał jej nic powiedzieć. Na szczęście, wiedza Barbakanu oraz Cień pomogły jej w uratowaniu Nikoli oraz wszystkich przed Nikolą.
* Nikola Kirys: Emulatorka, która po dotknięciu Cienia odzyskała wolność. Kiedyś sprzężona z Finis Vitae, Minerwa zrobiła z niej Pierwszą Emulatorkę.
* Minerwa Metalia: leżąc w szpitalu pomaga Pięknotce zrozumieć o co chodzi z Emulatorami Orbitera i jak bardzo to niebezpieczna sprawa.
* Alan Bartozol: pełnił rolę taksówki dla Pięknotki w hovertanku klasy Timor.
* Olaf Zuchwały: ma ogromny uraz do Orbitera za to, co stało się Nikoli dawno temu; powiedział Pięknotce, że Nikola nie żyje a to co tam jest to duch.
* Bożymir Szczupak: przyjaciel Pięknotki w Barbakanie, ekspert od zdobywania i wyciągania informacji z danych. Kustosz i Arlekin Pozytywny.

## Plany

* Minerwa Metalia: Pięknotka poprosiła, by Minerwa znalazła sposób na naprawienie Emulatorek oraz siebie samej, na zapełnienie luki.
* Nikola Kirys: wyłączone plany: Finis Vitae, destrukcja dookoła, zniszczenie Astorii, natychmiastowa integracja z Cieniem.
* Nikola Kirys: możliwe plany: szlak przemytu, zemsta na odpowiedzialnych magach Orbitera (nie na wszystkich), zniszczenie reputacji Orbitera.

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Pustogor
                                1. Rdzeń
                                    1. Szpital Terminuski: Pięknotka skutecznie unikała Blakenbauera, oraz wkradała się do Minerwy by z nią porozmawiać
                1. Sojusz Letejski, SW
                    1. Granica Anomalii
                        1. Wolne Ptaki
                            1. Królewska Baza: miejsce spotkania Pięknotki i Nikoli; stamtąd wyjechały w kierunku na Pacyfikę
                        1. Pacyfika, obrzeża: Pięknotka i Nikola mają showdown i walkę o Cienia

## Czas

* Opóźnienie: 1
* Dni: 2

---
layout: cybermagic-konspekt
title: "Potencjalnie eksterytorialny seksbot"
threads: rekiny-a-akademia
gm: żółw
players: anadia, kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [210323 - Grzybopreza](210323-grzybopreza)

### Chronologiczna

* [210323 - Grzybopreza](210323-grzybopreza)

## Punkt zerowy

### Dark Past

Sabina Kazitan jest jaka jest. Wszyscy wiemy, że jest niebezpiecznym potworem. Ale Sabina jednocześnie chce zniszczyć Aurum, a na pewno wszystko powiązane z Lemurczakami.

Sabina świetnie zdaje sobie sprawę z tego, jak bardzo ci młodzi "na zesłaniu" są w stanie spowodować konflikty między rodami w samym Aurum, jeśli będą oskarżeni o coś złego co popełnili lub czym się zajęli. Tak więc Sabina pozostawiła po sobie pewien plan - wie, jak zrobić seksbota, który całkowicie zajmie uwagę Rekinów.

Jako, że Rekiny żyją w eksterytorialnej dzielnicy w Podwiercie, wprowadzenie tam seksbota sprawi, że Pustogor nie powinien się mieszać w Rekinowe sprawy. Jeśli Rekiny same złożą swojego seksbota, to będzie "ich" wina. Jej rolą jest jedynie zostawić im rytuał, który umożliwi im wpakowanie się samemu w straszne kłopoty.

Co taki seksbot miałby robić? Wpierw ma za zadanie rozkochać w sobie wszystkich płci obojga. Potem ma budować kłótnie i niesnaski. By w końcu Rekiny robiły rzeczy, które strasznie skonfliktują ich rody w samym Aurum. I Sabina będzie miała swoją zemstę.

### Opis sytuacji

Sesja jest w trzech chronologiach. ROK TEMU, 3 MIESIĄCE TEMU i TERAZ. Chronologicznie sesja jest zlinkowana z "teraz".

## Misja właściwa

ROK TEMU (demonstracja Sabiny Kazitan):

Marysia latała sobie po terenie tropiąc ciemnych interesów - dokładniej, szukając jakiegoś poczciwego szwindlu który może zatrzymać i opieprzyć. Ale wykryła w lesie podwierckim (Las Trzęsawny) coś, czego się nie spodziewała - echo cierpienia magicznego. Oczywiście, poleciała zobaczyć o co chodzi.

Na miejscu zobaczyła uciekającego ultrażałosnego Rekina, Feliksa Keksika (ksywa Pożeracz) który wieje przed jakimś amorficznym blobem który... nie próbuje go złapać a jedynie straszyć?

Marysia zdecydowała się zrozumieć sytuację - kto za tym stoi. Plus, zatrzymać bloba. To nawet lepsze niż szmugiel :D. TrMZ+2

* X: Działania Marysi sprawiły, że Pożeracz o niej wie. 
* V: Udało się - stwór jest wolniejszy, Pożeracz nie będzie złapany
* X: Stwór mackami oplątał ścigacz
* V: Marysia wykryła operatora - Sabinę Kazitan
* V: Marysi udało się oderwać od stwora, wzbić wyżej
* X: Magia Marysi wpłynęła na stwora, Sabina nie ma już nad nim kontroli XD

Marysia ostro skonfrontowała się z Sabiną - żąda, by ta osłabiła i rozwaliła stwora. A Marysia chce od Sabiny, by ten traumatyzujący blob trafił do jej fiolki. Chce "na kiedyś". Sabina nie boi się Marysi, ale spełnia jej wolę. Wie, co ją może czekać jak woli Marysi nie spełni. ExM+1.

* V: Stwór został zamknięty w fiolce
* V: Sabina zdestabilizowała stwora, jest dla Marysi bezużyteczny
* X: Marysia wie, że Sabina to zrobiła i zrobiła umyślnie

Marysia widząc w jak złym stanie jest Pożeracz (dotknął go śluz, jest w traumie wywołanej swoimi fobiami i koszmarami) zdecydowała się mu pomóc. A dokładniej - Sabina ma jej pomóc pomóc Pożeraczowi. Marysia używa magii mentalnej a Sabina ma Pożeracza przytulić i pocieszyć że będzie dobrze. ExM+3.

* X: potrzebny będze Pożeraczowi lekarz...
* O: Sabina jest Skażona, ale Pożeraczowi będzie lepiej.
* V: Pożeracz jest nieskończenie wdzięczny Marysi.

Po czym Marysia powiedziała Sabinie "paa~", gdy tą zaczął infekować jej własny śluz i odleciała z Pożeraczem w kierunku na Podwiert.

TRZY MIESIĄCE TEMU (demonstracja głodnego kobiet Mysiokornika):

Ostra impreza na podwierckim stadionie należącym do Rekinów. Zaproszone są m.in. Julia czy Triana i kilka innych uczniów AMZ. Triana się upiła i śpi gdzieś pod konstrukcją; Wkrąż domalowuje jej uszka i rechocze przy tym jakby był nie wiem jaki dowcip. Julia zdecydowała się zainterweniować i pomóc swojej przyjaciółce - przyniosła ULTRA OSTREGO kebaba Wkrążowi. Czy da radę go zjeść? Taunt. Tr+2.

* V: Sukces. Wkrąż zapiszczał, przestał skupiać się na Trianie i uciekł po wodę. Triana śpi sama smacznie.

Na imprezie jest też Rupert Mysiokornik. Mysiokornik jest niesamowicie wręcz wściekły, że "lokalne laski" nie chcą docenić tego jaki on jest fajny, że nie ma wianuszka dziewczyn z których może wybierać i nieco na agresora zaleca się do czarodziejek AMZ i Rekinek. To miejsce interwencji Marysi. Marysia kazała mu pić i kazała jednej z Rekinek, Lorenie, pilnować by on pił i by nic nie robił z dziewczynami.

* V: Jest OK, zostaw dziewczyny.
* X: Lorena upije zarówno jego jak i siebie... i zacznie tańczyć na stole -_-.
* V: I to była gwiazda imprezy - skupili się na podżeganiu Loreny a nie na dalszych problemach.
* O: Ta dwójka - Rupert i Lorena - skończyli w łóżku. Jeden raz. Rupert szczęśliwy, Lorena zawstydzona (nie chce o tym mówić). Nie dadzą jej zapomnieć.

...

DZISIAJ (rozwiązanie sytuacji):

Triana zaprosiła do siebie Marysię i Julię (jak zwykle) i oglądają opowieści o duchach (jak zwykle). W pewnym momencie na Złomowisku w Zaczęstwie odpaliły się alerty - ktoś nieupoważniony (czyt: nie z AMZ) zaczyna szabrować i szukać rzeczy których nie powinien szukać.

Wszyscy są zdziwieni, że Triana ma włączony na stałe monitoring Złomowiska. Triana jest zdziwiona, że oni nie mają. Zawsze coś może stamtąd zwiać XD.

Triana przygotowuje servara, by jak najszybciej tam wyruszyć. Marysia zostaje pomóc jej założyć servara. Julia rusza przodem - ona nie czeka, chce zobaczyć o co chodzi i co może zrobić.

Julia świetnie się skrada; próbuje zlokalizować gdzie są szabrownicy, ilu ich jest i wprowadzić ich w niebezpieczne / niefortunne dla nich miejsce. Tr+2.

* V: 3 szabrowników, na oko Rekiny
* V: Julia rzucając śrubkę wprowadziła ich do "doliny złomu". Ma ich we wrażliwym miejscu.

Julia szybko informuje o sytuacji Marysię, która wzywa jako wsparcie Franka Bulteriera. Franek lubi się bić i dobrą zabawę (czyli się bić). Marysia sama zdecydowała się dotrzeć na złomowisko nie czekając na Trianę. A Julia w tym czasie opóźnia szabrowników, udając ducha i poruszając się po złomowisku jak po swoim domu (co, po prawdzie, jest wysoce prawdziwe). ExZ+3:

* V: Julia daje wrażenie, że gdzieś tu są terminusi i robią patrole. Używa resztek półsprawnego sprzętu by było "duchowato". Rzuca też trochę złomu w pysk. Udaje ducha. 
* V: Nikomu nie stała się krzywda, mimo, że Rekiny otworzyły ogień do Julii.
* X: Franek był niedaleko, więc dotarł PRZED Marysią
* V: Franek zeskoczył ze ścigacza i zrobił DEWASTACJĘ.

Gdy Marysia dotarła, było już po wszystkim. Szabrownicy to Mysiokornik, Wkrąż i... Babu.

Przepytani przez Marysię, powiedzieli czemu tu są:

* Mysiokornik i Wkrąż chcą zaszabrować głowę seksbota, by zrobić epickiego seksbota i mieć idealną dziewczynę która się nimi zajmie i ich doceni
* BaBu jest tu, bo tamta dwójka się bała iść tu sama. On nie chce seksbota, ale chce zrobić rytuał.

Marysia podejrzewa, że za rytuałem stoi Sabina; są tam jej ślady. Babu potwierdza, że też to podejrzewa. Zdaniem Marysi, jeśli za pomysłem stoi Sabina, to tu może być coś niefajnego i groźnego. Kazała im tego nigdy nie robić, nie bawić się z seksbotami i zachowywać się jak normalni magowie. Babu wzruszył ramionami, jest ciekawy co się stanie. Ale ok.

Marysia skontaktowała się z Sabiną. Powiedziała, że nie wie czym jest plan Sabiny, ale ona go rozmontowała. Sabina powiedziała, że nie wie o czym Marysia mówi, ale przyjęła do wiadomości...

## Streszczenie

Sabina Kazitan zostawiła za sobą plan mający skonfliktować Rekiny na poziomie Podwiertu, co miałoby kaskadować na relacje rodów w Aurum. Plan zawierał Skażonego seksbota, którego Rekiny miały same zrobić. Podszepnęła też odpowiednie słówka odpowiednim osobom. Jednak gdy podatne Rekiny chciały iść za tym planem, gdy szabrowały ze złomowiska głowę seksbota to zatrzymał je Zespół. Skończyło się na tym, że seksbota nie będzie - Marysia im absolutnie zakazała cokolwiek robić, nawet, jeśli nie znała planu Sabiny.

## Progresja

* Barnaba Burgacz: zna rytuał Sabiny Kazitan o tym, jak zrobić ultra-atrakcyjnego seksbota. Nie wie, co ten rytuał naprawdę robi. Wie, że od Sabiny Kazitan. Marysia Sowińska absolutnie zakazała go używać.
* Rupert Mysiokornik: zna rytuał Sabiny Kazitan o tym, jak zrobić ultra-atrakcyjnego seksbota. Nie wie, co ten rytuał naprawdę robi. Wie, że od Sabiny Kazitan. Marysia Sowińska absolutnie zakazała go używać.
* Triana Porzecznik: ma włączony na stałe monitoring Złomowiska w Zaczęstwie. By nikt nie szabrował... lub by coś nieupoważnionego nie wyszło.

### Frakcji

* .

## Zasługi

* Marysia Sowińska: rok temu uratowała Pożeracza przed torturami ze strony Sabiny a 3 miesiące temu zatrzymała agresywnie napalonego Mysiokornika każąc Lorenie go upić. Teraz: zakazała używać rytuałów na seksboty.
* Julia Kardolin: 3 mc temu ratuje Trianę przed kocimi uszkami ostrym kebabem, teraz świetnie się skrada po złomowisku i uruchamia na wpół martwe urządzenia by spowolnić szabrowników aż Franek przyleci.
* Triana Porzecznik: statystka. 3 miesiące temu zalana, spała i Wkrąż dorysowywał jej kocie uszka. Teraz: gdy potrzebna była interwencja na złomowisku, zakładała i kalibrowała servara za długo i problem się rozwiązał.
* Sabina Kazitan: wielka nieobecna sesji, która jednak zaplanowała rytuał zrobienie seksbota waśni typu 'Eris' wykorzystując eksterytorialność dzielnicy Rekinów w Podwiercie i swoją mroczną magię. Jako prezent ;-). Cel: spowodowanie wojen między rodami w Aurum przez to, jak młodzi magowie się zachowują w Podwiercie. Rok temu - miała starcie z Marysią Sowińską.
* Feliks Keksik: ROK TEMU: żałosny jak zawsze. Sabina Kazitan się nad nim znęca przyzywając bloba traum. Marysia go uratowała, Odkaziła i pozwoliła mu patrzeć jak Sabina Skaziła się tym samym blobem :D. Po czym -> lekarz.
* Henryk Wkrąż: nie jest to tytan intelektu. 3 mc temu: dorysowywał pijanej Trianie kocie wąsy i uszka a Julia zneutralizowała go ostrym kebabem. Teraz: szabrował głowę seksbota dla rytuału z epicką dziewczyną.
* Lorena Gwozdnik: 3 mc temu Marysia kazała jej zająć się Rupertem Mysiokornikiem, by ten nie szalał na imprezie. Zajęła się nim, sama się upiła, tańczyła na stole a potem skończyła w łóżku z Mysiokornikiem... wstydzi się.
* Rupert Mysiokornik: 3 mc temu napalony agresywnie i zneutralizowany przez Marysię i Lorenę. Teraz: szabrował głowę seksbota by zrobić epicki rytuał dzięki któremu będzie miał dziewczynę która go doceni!
* Barnaba Burgacz: poszedł szabrować głowę seksbota bo jest ciekawy czarnego rytuału; ma gdzieś samego seksbota. Chce zobaczyć co zrobi rytuał. Podejrzewa, że to jakiś plan Sabiny Kazitan.
* Franek Bulterier: wezwany przez Marysię by zdewastować szabrowników. Zeskoczył ze ścigacza i zdewastował szabrowników. Dokładnie to lubi i w ten sposób :-).

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Zaczęstwo
                                1. Dzielnica Kwiecista
                                    1. Rezydencja Porzeczników: Marysia, Triana i Julia oglądają tam po raz kolejny epickie opowieści o duchach. By się bać.
                                1. Złomowisko: stale monitorowane przez Trianę; gdy ktoś nieautoryzowany zaczął szabrować części, Julia opóźniła Rekiny a Marysia wezwała Franka i pokonali Rekiny.
                            1. Podwiert
                                1. Dzielnica Luksusu Rekinów: eksterytorialna dzielnica nie pod kontrolą Szczelińca a Aurum (ku wielkiej irytacji terminusów). Brak sentisieci (ofc).
                                    1. Obrzeża Biedy: nazwane dość ironicznie miejsce, bo tam mieszka większość Rekinów poza ultra-elitą
                                        1. Stadion Lotników: miejsce dzikiej, epickiej imprezy 3 miesiące temu. Był kebab, alkohol, grzyby, WSZYSTKO.
                                1. Las Trzęsawny: rok temu Sabina Kazitan torturowała tam Pożeracza (Keksika) blobem powodującym manifestacje fobii i koszmarów. Marysia ją zatrzymała i przechwyciła bloba. Sabina Dotknęła swego własnego bloba, przez co wpadła w fobie i koszmary. Samotna, w Lesie Trzęsawnym.
## Czas

* Opóźnienie: 4
* Dni: 1


## Konflikty

* 1 - Marysia próbuje zatrzymać bloba polującego na Pożeracza. Plus, znaleźć kto za tym stoi.
    * TrMZ+2
    * XVXVVX: ścigacz od śluzu, moc Marysi rozerwała link Sabiny z blobem, Marysia wie że za tym stoi Sabina, sukces: blob nie poluje skutecznie na Pożeracza
* 2 - Sabina zamyka bloba w fiolce dla Marysi i destabilizuje go tak, by Marysia nie wiedziała że Sabina to zrobiła.
    * ExM+1
    * VVX: stwór został zamknięty w fiolce i zdestabilizowany, ale Marysia wie, że Sabina to zrobiła.
* 3 - Marysia detraumatyzuje Pożeracza (Skażonego blobem fobii i traum) używając magii mentalnej.
    * ExM+3
    * XOV: Pożeracz potrzebuje lekarza, Sabina jest śluzem Skażona (poświęcona ;p), Pożeracz jest nieskończenie Marysi wdzięczny
* 4 - Julia tauntuje Henryka Wkrąża czy ten da radę zjeść mega ostrego kebaba, by odwrócić jego uwagę od Triany
    * Tr+2
    * V: Henryk uciekł po wodę i zapomniał o Trianie
* 5 - Marysia neutralizuje agresywnie napalonego Mysiokornika słowami i każe Lorenie go spić.
    * Tr+3
    * VXVO: Mysiokornik zostawił dziewczyny, Lorena tańczy na stole co było gwiazdą imprezy a potem Lorena skońcyła w łóżku z Mysiokornikiem.
* 6 - Julia świetnie się skrada; próbuje zlokalizować gdzie są szabrownicy, ilu ich jest i wprowadzić ich w niebezpieczne / niefortunne dla nich miejsce. Tr+2.
    * Tr+2
    * VV: 3 szabrowników, wprowadzeni do "doliny złomu"
* 7 - Julia opóźnia szabrowników, udając ducha, uruchamiając półmartwy sprzęt i poruszając się po złomowisku jak po swoim domu by nie być trafioną.
    * ExZ+3
    * VVXV: sukces, nikomu nie stała się krzywda aż pojawił się Franek i zdewastował szabrowników.

## Inne

### Projekt sesji
#### Procedure

* Overarching Theme:
    * gracze mają "kierowanie" (Vision & Purpose); wiedzą gdzie iść i po co tam idą
    * gracze są bardziej zaangażowani w historię, bo widzą, gdzie ich decyzje prowadzą
    * MG wie, które sesje wprowadzać i które usuwać
    * MG wie, w którą stronę stawiać adwersariat
* Achronologia:
    * przez przeskakiwanie do przodu nieważnych wydarzeń oczekuję, że każda sesja jest powiązana z kontekstem (gracze widzą jak się to rozwija)
    * przez cofanie się i uzupełnianie luk w przeszłości oczekuję, że brakujące MG i graczom luki uzupełnimy takimi działaniami
    * przez działanie w przeszłości gracze czują silniejszą relację z postaciami i światem, silniejsza budowa wspólnej historii.
* Wyraziste, ważne dla graczy postacie + Relacje (konflikt?) Gracze <-> NPC, NPC <-> NPC
    * graczom bardziej zależy na postaciach i ich powodzeniu
    * więcej potencjalnych wątków i rzeczy które możemy odrzucić przy krzyżykach
* Tory, zmiana postaci z czasem, wpływ graczy na świat
    * gracze widzą jak wpływają na świat. Wpływ jest "ich" - stąd tak tragiczne jest gdy "ich" rzeczy są corrupted / destroyed.

#### Result

**Przeprowadziłem inną sesję, bo Anadia miała tylko 2h, ale na bazie tej**

* Overarching Theme:
    * V&P:
        * eskalująca nienawiść, eskalujące nieszczęście
    * adwersariat: Uczniowie AMZ wykradający Trianie części na seksbota i przygotowujący rytuał
* Achronologia:
    * by pokazać Sabinę
    * by pokazać Mysiokornika
* Wyraziste, ważne dla graczy postacie + Relacje (konflikt?) Gracze <-> NPC, NPC <-> NPC
    * Triana: której znikają części
    * Ula: która o wszystko podejrzewa Marysię
    * Sabina: support konstrukcji seksbota
* Tory, zmiana postaci z czasem, wpływ graczy na świat
    * seksbot będzie najatrakcyjniejszy dla Rekinów, bardziej niż dziewczyny.
    * Sabina zniknie, niezauważona

#### Scenes

* Scena 0: Pokazanie horroru którym jest Sabina Kazitan - Sabina torturująca Pożeracza.
* Scena 1: Scena imprezowa
* Scena 2: Miejsce u Triany
* Scena N: seksbot w akcji

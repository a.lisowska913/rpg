---
layout: cybermagic-konspekt
title: "Serafina staje za Wydrami"
threads: rekiny-a-akademia
gm: żółw
players: anadia, kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [210824 - Mandragora nienawidzi Rekinów](210824-mandragora-nienawidzi-rekinow)

### Chronologiczna

* [210824 - Mandragora nienawidzi Rekinów](210824-mandragora-nienawidzi-rekinow)

### Plan sesji
#### Co się wydarzyło

.

#### Sukces graczy

* .

### Sesja właściwa
#### Scena Zero - impl

.

#### Scena Właściwa - impl

Karolina wie, że jej brat niewiele pamięta. Ale wie też, że sam nie wpadłby w takie tarapaty jak pnączoszpon - Daniel nie lubi łazić po lesie. NIKT nie lubi łazić po lesie (za niebezpieczne). Daniel raczej szybko latał a Karolina wie, że nie robi głupich ruchów.

Karolina wie koło kogo się Daniel kręci. Lorena Gwozdnik, kto inny. Więc czyżby Lorena wystawiła Daniela? A na pewno Daniel sporo gada. I tak, umawiał się z Loreną dnia w którym Daniel wpadł pod pnączoszpona.

Karolina podbija do Loreny. Ma kastet ;-). Wyczekuje, aż Lorena będzie szła w kierunku domu, trochę podchmielona i decyduje się ją ZŁAPAĆ będąc w Dzielnicy Rekinów. Tak do magazynów. Bo Lorena jest, bądźmy szczerzy, przewidywalna.

Tr+3:

* V: Lorena jest przechwycona i zawleczona pod nożem do magazynu. "Ani słówka albo".
* V: Lorena jest zastraszona - będzie mówić prawdę.

Karolina przesłuchuje (dość brutalnie, acz słownie) Lorenę odnośnie tego co wie o Danielu i czy go wystawiła

TrZ+3:

* V: Lorena nie wystawiła Daniela.
* X: "Nie wystawiłam Daniela. Za kogo Ty mnie masz? | Za jedną z lasek które przeleciał" -> Lorena jest WŚCIEKŁA na Karolinę. Zabolało ją.
* X: Karolina przywiera Lorenę barkiem do ściany "prawdziwie psychicznej to mnie jeszcze nie widziałaś" -> Lorena zrywa z Danielem. Nie po tym. Ma chorą siostrę.
* V: Daniel szukał podobno śpiewających węży w lesie. Śpiew syreny. A w ogóle - odkąd pojawiła się mafia, WIĘCEJ magów miało wypadki w lesie.
* V: Karolina wyciska jak cytrynkę - Lorena mówi.
    * Są ataki na Rekiny. Rekiny wpadają w kłopoty. Nic wielkiego, ALE.
    * Las oraz Zaczęstwo i okolice są niebezpieczniejsze dla Rekinów.
        * Z wyjątkiem - Arkadia Verlen...
    * Nie tylko Lorena słyszała śpiewające węże - Daniel, Cyryl...
        * Ale w sumie nie wie czy to węże. Daniel tak powiedział (spoiler: na pewno powie coś by wpakować się do łóżka).

Zostawiwszy Lorenę (w bezpieczniejszym miejscu), Karolina poszła zobaczyć ścigacz Daniela. Jeśli coś widział Daniel, może widzi to ścigacz.

Karolina odwiedza Daniela u Sensacjusza. Żadnych szpitali dla Daniela. Daniel niewiele pamięta... a Sensacjusz nie może dać jej mnestyków. Karolina zażądała od Daniela kluczyków. Daniel nie chce dać - ma tam zbiór swoich podbojów i wideopamiętnik swoich podbojów. Karolina obiecała, że nie będzie szukać tych wstydliwych rzeczy. Daniel dał Karolinie klucz.

Karolina szuka informacji o Danielu i jego działaniach w ścigaczu - co mogło wpakować go na minę?

TrZ+3:

* X: Karolina musiała PRZECIERPIEĆ OGLĄDANIE wideoblogów Daniela. Łącznie z ten tego. I kiepską nawijką.
* V: Karolina ma rejestr muzyki. Tego "śpiewu". Faktycznie, Daniel leciał na ten śpiew. Szukał. To jakby... śpiewaczka? Daniel znajdzie tą laskę co śpiewa w lesie. Prowadzi wideobloga...
* V: Daniel natknął się na coś nietypowego - grupa o nazwie "Wydry". Wydry polują na ryby. Znaczy: na Rekiny. Nie wie dokładnie co kto i kiedy - ale wie, że jest na terenie jakaś grupa, która wyjątkowo nie lubi Rekinów i chce się ich pozbyć. Daniel ma przyjaciółki. A one czasem mają papiery, listy... a on jest niedyskretny. Znalazł to w szufladzie Haliny Sermniek. Nawet z nią nie spał XD. Ale był u niej w mieszkaniu.

Do Marysi na hipernecie "yo, laska" od Karoliny. "Chcesz się dowiedzieć kto tępi Rekiny? Bo ja też. A mam ślady.". Marysię drażni ta bezpośredniość, ale niech będzie. Niech Karolina przybędzie...

Karolina przekazała większość informacji Marysi. Ominęła temat podbojów brata i tego jak nacisnęła na Lorenę. Reszta - ok.

Dzięki Danielowi wiadomo, że Halina mieszka na Osiedlu Tęczy. W bloku.

Marysia przez hipernet -> Tukan. Poprosiła o wyniki badań ciał tworzących mandragorę. Tukan powiedział, że przekaże Laurze żądanie by przesłała Marysi.

Marysia wzywa do siebie Lorenę. A tam Karolina... "to dla mnie zaszczyt, tien Sowińskacoonatamrobi..."

* Marysia: "szukamy byś zagrała zrozpaczoną, zazdrosną narzeczoną Daniela"
* Lorena: "ostatnio mam wrażenie, że jest to rola, którą często pełnię"
* Marysia: "dlaczego?"
* Lorena: "bo jestem sama ;_; cały czas. I cały czas pełnię tę rolę... nie ma innej?"

Marysia wyjaśniła Lorenie, że Danielowi coś się stało i Halina coś wie. I można pomóc Danielowi i dowiedzieć się co wie. Marysia i Karolina jadą jako "opiekunki zrozpaczonej Loreny". I można popatrzeć i popytać.

Lorena stwierdziła, że nie jedzie z Karoliną - nikt nie uwierzy, że LORENA będzie szła na spotkanie z nieznaną dziewczyną (Haliną) z gorylowatą, usmarowaną smarem małpą z kluczem francuskim w ręku.

Marysia stwierdziła, że jadą w trójkę :-). Lorena zaproponowała "atak jak zawsze" - czarne stroje, infiltracja, ekstrakcja, magia mentalna i wycofanie się. Po co się pieprzyć?

Lorena dostarczyła czarne stroje. Widać, że jest gotowa na takie operacje ;-). Lorena ma Halinę wystawić, Karolina łapie. Marysia przychodzi na gotowe.

TrZ+3:

* Vz: Lorena wystawiła bez większego problemu.
* V: Karolina złapała bez większego problemu ;-).
* Vz: Przechwycona i złapana do mieszkania. Nikt nic nie wie bo nie chce wiedzieć.

Czas, by Marysia zajęła się wydobyciem informacji od Haliny - co ta wie o całej sprawie i o Wydrach. TrMZ+2:

* X: Ich obecność w mieszkaniu została zarejestrowana przez sąsiadów. Wiedzą, że ktoś. Wiedzą, że Halina ma kłopoty. Nic nie robią. Nie pierwszy raz.
* V: Marysia dotarła do tego co wie Halina w zakresie "czym są Wydry i czemu polują na Rekiny i co to ma do mandragory i Daniela"
    * Wydry próbowały zwerbować Halinę. Nie udało im się. Halina nie lubi Rekinów, ale nie chce z nimi walczyć - boi się terminusów.
    * W oczach Haliny, Rekiny są grupą która wpada do mieszkań, robi co chce... kradnie rzeczy... są straszną szkodą na terenie.
    * Daniela zaprosiła do chałupy z nadzieją, że skoro jest bogaty (nie jest) to może pomoże Sprawie - pomoc osobom nie mającym pracy przez AI. Daniel jednak nie był dość zainteresowany.
    * Wydry to grupa ludzi i magów (zwłaszcza ludzi), które chcą się pozbyć z terenu Rekinów.
    * Wydry nasiliły działania odkąd Rekiny zaczęły współpracować z mafią - bo to sprawiło, że Rekiny aktywnie krzywdzą teren.
    * "Wszyscy wiedzą" że Rekiny to podłe istoty. Mają krew i cierpienie na rękach. Acz Halina osobiście nic nie wie.
    * Próbował ją zwerbować "random". W masce. Ona nie chciała wiedzieć - sądzi, że jest pod obserwacją terminusów. Sympatyzuje z Wydrami.
    * Wydry są małą siłą. Nie mają dużej potęgi czy dużo magów. Duża nienawiść, mała moc.
* Vm: Po zastosowaniu zaszumiania magicznego (kryształ) Halina jest święcie przekonana, że to była akcja terminusów sprawdzających, co nowego wie. Halina nie zamierza tego podnosić czy nic z tym robić - jest bezradna wobec potęgi Pustogoru.

Czyli Halina nic nie wie - ale warto to wiedzieć. Plus, wiadomo, że Wydry tu działają od pewnego czasu. Duża nienawiść, mała moc.

Gdy Marysia i Karolina wracały do Dzielnicy Rekinów, Marysia dostała wiadomość od Laury. Zgodnie z poleceniem, wysłała dane osób z których powstała mandragora. Nazwiska 5 osób, płci różnej. Jacyś ludzie.

Laura, zapytana przez Marysię jak zginęli zgodnie z systemami terminusów powiedziała, że "zniknęły". Porwanie w lesie przez potwora, zginęli gdzieś w wypadku... ogólnie, niewyjaśnione. Ciał nie odnaleziono. Wygląda to dla Marysi jakby Pustogor wyciszył ich zniknięcie. A oni nie zniknęli w jednym czasie - na przestrzeni 3 miesięcy. To, co ich łączy - wszyscy byli łatwi do zniknięcia.

A wiedząc o tym jak działała mandragora - wszyscy mieli powiązania z Rekinami.

I Marysia poszła trzecim śladem - muzyka. Nietypowa, dziwna muzyka. Dała temat Torszeckiemu. Znalazła ciekawy kawałek i chce wiedzieć co to jest. Torszecki zabrał się do roboty.

Tr+3:

* X: Kiedy ktoś szuka muzyki Serafiny, Serafina się dowiaduje ;-).
* V: Torszecki odnalazł twórcę muzyki. Serafina Ira. "Piosenkarka Kajrata".

To co jest ważną implikacją - Serafina pomaga Wydrom JEŚLI Wydry stoją za mandragorą. Plus - to nie jest super niszczycielskie jak na Serafinę. Serafina jest tak jakby nienaturalnie łagodna.

Marysia Sowińska nadała nowy temat Torszeckiemu. Chce, by Torszecki zaaranżował spotkanie między Marysią a Serafiną. Ma to zrobić przy użyciu najlepiej hipernetu i najlepiej by terminusi nie wiedzieli ;-). I ma być grzecznie. Nie "Marysia żąda" a "Marysia jest wielką fanką muzyki" ;-).

Torszecki jest podłamany poleceniem, ale zrobi co może.

Ex+4:

* X: Torszecki idzie na zakładnika.
* V: Serafina i Marysia się spotkają. Karolina też może być.

Poranek, niedaleko miejsca, gdzie jest mandragora. Była. Tam Karolina i Marysia przybyły, by zobaczyć Serafinę stojącą w odkrytym miejscu. Widać ją z powietrza. Ale nie ponad koronami drzew. Serafina jest w servarze. Co gorsza, to customowy model...

Serafina powiedziała, na czym polega jej stanowisko:

* są tu ludzie skrzywdzeni przez Rekiny. oni nie mogą się bronić.
* pojawiła się mandragora. Serafina tym ludziom pomogła ją wyhodować.
* Marysia zniszczyła mandragorę. Odebrała sprawiedliwości miecz
    * Serafina uważa, że to znaczy, że Marysia OSOBIŚCIE jest odpowiedzialna za stanie się mieczem sprawiedliwości
* Serafina wzmocni Wydry - bronią i ekspertyzą. Nauczy ich się bronić przed Rekinami.
* Serafina oczekuje, że Marysia weźmie na siebie zadanie by takie rzeczy nie działy się na tym terenie - w końcu ma dowodzić w przyszłości.

Karolina wyzwała Serafinę od metod stalinowskich i od tego, że krzywdzi niewinnych (sorki, ale Daniel JEST tu niewinny). Serafina odpowiedziała, że Daniel nie jest całkowicie niewinny a mandragora nie zrobiła mu nadmiernej krzywdy. Tu nie mają zgody.

Zdaniem Serafiny metody drastyczne które działają są lepsze niż łagodne które nie działają. 

Serafina jest zaskoczona, że Marysia nie współpracuje z mafią. Jej intel mówi inaczej. Nie wierzy jej.

Serafina i Marysia uznały, że się rozstaną. Marysia to przemyśli i zobaczą jak to się potoczy dalej. Serafina ogólnie żąda:

* Rekiny odpowiedzialne za śmierć ludzi --> nazwiska do publicznej wiadomości. Dla hańby.
* Zadośćuczynienie rodzinom. Co najmniej - wydanie im ciał. Niech mają grób.

Ale sęk w tym, że żądania Serafiny - jakkolwiek "dobre" i "z dobrego serca", ale destabilizują sytuację polityczną. Marysi zależy na tym, by sytuacja polityczna była maksymalnie stabilna. Więc nie może na to iść...

Marysia chce więc tylko kupić czas - niech NA RAZIE Serafina nic nie robi. Marysia powiedziała, że się spróbuje dowiedzieć kto stoi za morderstwami, ukryciem śladów itp. by sama mogła podjąć lepszą decyzję. I prosi Serafinę, by ta przez pewien czas nie robiła żadnych działań. Nie chce wywołać wojny.

Ex+Z (bo nie współpracuje z mafią i faktycznie coś robi) +3:

* Vz: Nawet jak wszystko pójdzie w cholerę, Serafina nie uzna, że Marysia zdradziła.
* V: Serafina odłoży uzbrajanie Wydr i podłe rzeczy na następne dwa tygodnie, niezależnie od operacji terminusów - WYJĄTEK: jeśli terminusi uderzą w Wydry to Serafina pomoże Wydrom. Ale pomoc Wydrom to nie to samo co tępienie Rekinów.
* XX: Serafina bierze ten temat BARDZO osobiście. Echo tego, co spotkało ją w przeszłości. Zdrada ludności przez terminusów.
* X: W Pustogorze też to nie byłoby przyjęte dobrze. Część terminusów wspierałaby Wydry gdyby wiedziała (przykład: Ula).
* V: Serafina powie Marysi co wie (bez danych osobowych ludzi tutaj i Wydr) by pomóc Marysi w detektywowaniu.

O dziwo, gdy Marysia i Karolina oddaliły się, Torszecki został wypuszczony. Miła odmiana - oddali zakładnika XD. I to w całości XD. Jak tylko Karolina zobaczyła Torszeckiego, klepnęła go solidnie w tyłek. "Jednak masz jaja".

Karolina ostrzegła Pustogor - a dokładniej, Ulę. O obecności Serafiny. Czyli że jest tu POSZUKIWANA czarodziejka, która chce krzywdzić Rekiny. Ula mówi, że może zgłosić każdemu innemu. Np. Tukanowi. Karolina: "co?". Ula: nvm. I tak prawidłowo zgłosi. Jest terminuską...

Terminusi zdecydowali się na potężną odpowiedź. Trzeba znaleźć Serafinę...

Serafinie to nie przeszkadza...

## Streszczenie

Karolina MUSI poznać prawdę co się stało z jej bratem. Odkryła, że istnieje grupa Wydry polująca na Rekiny, ludzi skrzywdzonych przez Rekiny w przeszłości. I pomaga im Serafina Ira. Z Marysią porozmawiały z Serafiną i przekonały ją, by ta działała wolniej - i drugą linią napuściły na Serafinę terminusów z Pustogoru.

## Progresja

* Lorena Gwozdnik: zerwała z Danielem Terienakiem. Ma psychiczną siostrę.
* Lorena Gwozdnik: personalnie wściekła na Karolinę Terienak. Ma z nią kosę za wszystko (obrażanie, traktowanie, zerwanie jej związku itp).
* Karolina Terienak: ma wroga w Lorenie Gwozdnik. Zbyt twardo weszła i rozerwała związek jej i jej brata.

### Frakcji

* .

## Zasługi

* Marysia Sowińska: używa systemów Sowińskich by odkryć informację o Serafinie irze, przy użyciu Torszeckiego się z nią spotkała i przekonała do spowolnienia wspierania Wydr przeciw Rekinom.
* Karolina Terienak: przesłuchała Lorenę i doszła do tego jakie informacje są w ścigaczu jej brata; odkryła, że istnieje grupa Wydry polująca na Rekiny.
* Lorena Gwozdnik: kręci się koło Daniela (dziewczyna?). Duża antypatia do Karoliny; dość zadziorna. Nie pierwszy raz włamuje się do ludzi - wie jak to robić i ma przygotowane odpowiednie czarne, ekranowane stroje.
* Halina Sermniek: laska która NIE przespała się z Danielem, ale miała karteczkę, że "Wydry polują na Rekiny" w notatkach. "Rewolucjonistka". Źródło nieaktualnych informacji o Wydrach po tym, jak Zespół się do niej włamał (i nikt jej nie pomógł / nie wezwał terminusów).
* Daniel Terienak: ma podejrzanie dużo "przyjaciółek" w okolicy. Kiepska nawijka. Prowadzi prywatnego wideobloga. Dzięki temu udało się namierzyć Serafinę Irę - po jej muzyce. Acz siorka płakała jak bloga oglądała...
* Rafał Torszecki: znalazł dla Marysi kontakt i namiar na Serafinę Irę. Oddał się Serafinie na zakładnika, by Marysia mogła z Serafiną porozmawiać.
* Laura Tesinik: z rozkazu Tukana wysłała Marysi info jak zginęli ludzie tworzący mandragorę; niewyjaśnione. Pustogor ich ukrył? Laura nie jest szczęśliwa z tego jak działa Tukan, ale jest oportunistką.
* Serafina Ira: bardzo osobiście wzięła to, że Pustogor ukrył zniknięcie i śmierć niewinnych. Wspiera Wydry. Dała się przekonać Marysi, by 2 tygodnie poczekać z uzbrajaniem Wydr. Radykalna - żąda publicznego shamingu tych, co stoją za zabiciem niewinnych (czyli najpewniej Amelii Sowińskiej). Zgodziła się na spotkanie osobiste z Marysią, o dziwo.

### Frakcji

* Wydry Podwierckie: grupa skupiona na zwalczaniu Rekinów. Normalnie niezbyt silna, ale została zdecydowanie wzmocniona odkąd wspiera ich Serafina Ira.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Podwiert
                                1. Dzielnica Luksusu Rekinów
                                1. Osiedle Tęczy: zastraszona Halina Sermniek tam mieszka. Nie jest już rewolucjonistką, uważa, że terminusi na nią polują.
                                1. Las Trzęsawny: gdzieś tam ma bazę i kryjówkę Serafina Ira...
                                    1. Jeziorko Mokre: w okolicy Serafina spotkała się z Marysią by rozmawiać o przyszłości Rekinów, Wydr i mandragory.
                                

## Czas

* Opóźnienie: 2
* Dni: 3

## Konflikty

* 1 - Karolina podbija do Loreny. Wyczekuje, aż Lorena będzie szła w kierunku domu, podchmielona i decyduje się ją ZŁAPAĆ będąc w Dzielnicy Rekinów. Tak do magazynów. Bo Lorena jest, bądźmy szczerzy, przewidywalna.
    * Tr+3
    * VV: Lorena przechwycona i zawleczona + zastraszona, powie prawdę.
* 2 - Karolina przesłuchuje (dość brutalnie, acz słownie) Lorenę odnośnie tego co wie o Danielu i czy go wystawiła
    * TrZ+3
    * VXXVV: Lorena nie wystawiła; Karolina nią wzgardziła i Lorenę to zabolało; Lorena zrywa z Danielem; Daniel szukał śpiewającej syreny w lesie + odkąd jest mafia, Rekiny mają więcej wypadków. Wszystko co Lorena wie.
* 3 - Karolina szuka informacji o Danielu i jego działaniach w ścigaczu - co mogło wpakować go na minę?
    * TrZ+3
    * XVV: Karolina przecierpiała wideoblogi Daniela, ale ma rejestr muzyki której on szukał. Plus info o Halinie Sermniek + grupa "Wydry" polująca na Rekiny.
* 4 - Lorena dostarczyła czarne stroje. Widać, że jest gotowa na takie operacje ;-). Lorena ma Halinę wystawić, Karolina łapie. Marysia przychodzi na gotowe.
    * TrZ+3
    * VzVVz: Lorena wystawiła, Karo złapała, Halina przechwycona i złapana do mieszkania. Nikt z sąsiadów nie wie bo nikt nie chce wiedzieć.
* 5 - Czas, by Marysia zajęła się wydobyciem informacji od Haliny - co ta wie o całej sprawie i o Wydrach. Marysia mentalistka.
    * TrMZ+2
    * XVVm: sąsiedzi wiedzą, że Halina ma kłopoty - bo to pierwszy raz, Marysia wie co Halina wie o Wydrach, po zaszumianiu magicznym Halina jest przekonana że to akcja terminusów. Nie podnosi - potęga Pustogoru.
* 6 - Marysia poszła trzecim śladem - muzyka. Nietypowa, dziwna muzyka. Dała temat Torszeckiemu. Znalazła ciekawy kawałek i chce wiedzieć co to jest. Torszecki zabrał się do roboty.
    * Tr+3
    * XV: Twórca muzyki to Serafina Ira. I ona wie, że była poszukiwana przez Torszeckiego.
* 7 - Marysia Sowińska chce, by Torszecki zaaranżował spotkanie między Marysią a Serafiną. Torszecki jest podłamany poleceniem, ale zrobi co może.
    * Ex+4
    * XV: Torszecki na zakładnika, Serafina + Marysia + Karolina mają spotkanie.
* 8 - Marysia chce więc kupić czas - niech NA RAZIE Serafina nic nie robi. Spróbuje dowiedzieć się kto stoi za morderstwami itp. I prosi Serafinę, by ta przez pewien czas nie robiła żadnych działań. Nie chce wojny.
    * Ex+Z (bo nie współpracuje z mafią i faktycznie coś robi) +3:
    * VzVXXXV: Serafina nie uzna, że Marysia zdradziła; Serafina poczeka dwa tygodnie, ale bierze ten temat BARDZO osobiście (przeszłość); w Pustogorze część terminusów wspierałaby Wydry gdyby wiedzieli. Ale Serafina powie Marysi co wie.

## Inne
### Projekt sesji

#### Procedure

* Overarching Theme:
    * gracze mają "kierowanie" (Vision & Purpose); wiedzą gdzie iść i po co tam idą
* Achronologia: x
* Wyraziste, ważne dla graczy postacie + Relacje (konflikt?) Gracze <-> NPC, NPC <-> NPC
    * graczom bardziej zależy na postaciach i ich powodzeniu
    * więcej potencjalnych wątków i rzeczy które możemy odrzucić przy krzyżykach
* Tory, zmiana postaci z czasem, wpływ graczy na świat
    * gracze widzą jak wpływają na świat. Wpływ jest "ich" - stąd tak tragiczne jest gdy "ich" rzeczy są corrupted / destroyed.

#### Result

* Overarching Theme:
    * Theme & Feel: 
        * Taka piękna nienawiść... | Counterstrike of vigilantes
    * V&P:
        * 
    * adwersariat: 
        * 
* Wyraziste, ważne dla graczy postacie + Relacje (konflikt?) Gracze <-> NPC, NPC <-> NPC
    * Role i postacie
        * 
* Co się działo w przeszłości
    * 
* Tory, zmiana postaci z czasem, wpływ graczy na świat
    * 

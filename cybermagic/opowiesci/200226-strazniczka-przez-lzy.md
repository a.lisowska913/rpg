---
layout: cybermagic-konspekt
title: "Strażniczka przez łzy"
threads: nemesis-pieknotki
gm: żółw
players: cyris, rustgore, r1
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [200311 - Wygrany kontrakt](200311-wygrany-kontrakt)

### Chronologiczna

* [200311 - Wygrany kontrakt](200311-wygrany-kontrakt)

## Budowa sesji

### Stan aktualny

Zawsze musi być jeden Strażnik energii Esuriit, by ta nie skaziła okolicznych miast i wsi. Niestety, aktualny Strażnik to Kajrat. I chyba umiera.

W szpitalu znajduje się Alicja – dziewiętnastolatka, która może zostać Strażnikiem. Zabierze jej to szansę na normalne życie, ale uratuje okolicę. Wy jesteście neuronautami – osobami, które wejdą do wspomnień/ snów Alicji, by „podpiąć ją” do tej energii, odpowiednio nimi manipulując.

Jedynym problemem jest to, że wybitny lekarz opiekujący się Alicją obiecał, że nigdy nie pozwoli na zniszczenie życia dziewczyny. Wszedł w umysł Alicji zanim Wam udało się tam dostać.

### Dark Future

* Śmierć Alicji
* Alicja traci moc magiczną
* Esuriit się wylewa na Czółenko
* Alicja staje się bezmyślnym potworem Esuriit

### Pytania

1. .

### Dominujące uczucia

* To chyba my jesteśmy tymi złymi? Ale jak pomóc?

### Tory

* .

Sceny:

* .

## Punkt zerowy

### Postacie

### Opis sytuacji

Obrazy z umysłu Alicji:

* Porwana, sama w bunkrze w lesie przez grupę fanów. Tam uratowali ją policjanci, ale tam się zagnieździło.
* W domu z rodzicami, napad, uratował ich pies. Tam nie używała magii ze strachu; pies bezpieczeństwem.
* Upokorzona w szkole przez Rafała. Nie umiała się bronić, prawie go pobiła, ale się zatrzymała.
* Koszmary, które prawie zniszczyły Elizę. Jej złe myśli i koszmary senne zamanifestowały się jako potwory.
* Uczyła młodsze koleżanki jak działać przed kamerą. Zawsze chciała komuś pomóc.
* Ogólnie uznawana za arogancką przez magów. Tak, woda sodowa uderzyła jej do głowy (popularność).
* Pomagała tym bez mocy magicznej jak umiała. Nie odwracała się w małych sprawach od innych.
* Duża radość – z piękna. Próżna troszkę. Czy to piękno jej potraw, czy jej własna uroda / makijaż.

## Punkt zero

Cel: demonstracja Esuriit. Czas: dawno temu.

Trzech policjantów udało się do rezydencji Kajrata; podobno Kajrat porwał jakiegoś dzieciaka z dobrego domu i oni mają go (pokojowo!) odzyskać. Na miejscu jeden z mafiozów Kajrata ostrzegł ich, że faktycznie - sytuacja jest niezbyt ciekawa. Kajrat jest w jednej ze swoich "gorszych" faz chwilowo.

Zaanonsowani, policjanci weszli do Kajrata. Zastali go przy biurku piszącego jakieś dokumenty, ale jak na nich spojrzał, to szalonymi oczami. Esuriit faktycznie chwilowo jest w nim koszmarnie silne. Policjanci grzecznie poprosili o oddanie chłopaka - Kajrat powiedział, że ów od niego ukradł. Z kiosku ukradł kilka długopisów. Kajrat go kazał przyprowadzić. Jeśli policjanci chcą go z powrotem, niech go poważnie skrzywdzą. Else, Kajrat się nim zajmie sam. ELSE, ściągnie rodzinę dzieciaka i oni zapłacą za zbrodnię dziecka.

Policjant strzelił w nogę chłopaka i kazał mu biec dookoła sali. Kajrat się żywił cierpieniem i bardzo, bardzo bawiła go cała sytuacja...

## Misja właściwa

Szpital Terminuski w Pustogorze ściągnął 2 agentów i dodał ich do Aliny; Kajrat najpewniej gdzieś umiera i koniecznie potrzebna jest nowa Strażniczka Esuriit. Najlepiej ktoś, kogo Barbakan będzie w stanie kontrolować (Kajrata nikt nie jest w stanie kontrolować). Dwóch agentów Pustogoru + Alina przeciwko naturalnej formie Alicji + Lucjusz Blakenbauer, który odciął się od Szpitala, schował i też wniknął w umysł Alicji by ją ratować. No cóż - co zostaje? Rozkaz to rozkaz.

Wniknęli w umysł Alicji, wspomagani przez TAI Crystal, by zobaczyć scenę porwania Alicji. A dokładniej - Alicja w bunkrze, tam jej psychofani i Esuriit tuż za ścianą. Lucjusz w cieniu tej sceny, próbuje ją ochronić i odciąć ją od energii Esuriit. Alina nie jest w stanie dłużej ryzykować - przygotowała krótki rytuał i 'ujawniła' Alicji energię Esuriit. Pokazała jej gdzie ta energia jest i jak Alicja może do niej dostać dostęp. Kastor natomiast zaczął Alicję przekonywać, że ta scena nie jest realna - Alicja może swobodnie i bezpiecznie użyć tej energii.

Alicja sięgnęła do Esuriit. Zablokował ten ruch Lucjusz Blakenbauer i skrzyczał magów - co oni robią, czy są skłonni poświęcić dziewczynę dla Pustogoru? Alina odparła przyjacielowi, że tak. Nie ma wyjścia. Albo Alicja, albo region. Lucjusz skontrował - da się ufortyfikować Czółenko. Agenci Pustogoru zauważyli, że takie mają rozkazy a Alina dodała, że nie ma wyjścia - to najmniejsze zło.

Lucjusz zaczął przekształcać wizję Alicji tak, by odciąć czarodziejkę od energii. Alina jedynie wzmocniła rytuał. W wyniku tego konfliktu delikatny balans psychomagiczny w głowie Alicji zaczął się rozsypywać - Esuriit wyrwało się spod kontroli i Alicja w okrutny sposób pożarła energią wyobrażenia swoich psychofanów. Lucjusz natychmiast przestał eskalować, nie chcąc skrzywdzić dziewczyny bardziej; Alina nie miała z tym większego kłopotu.

Alicja jest przeszczęśliwa z tego pożarcia. Zintegrowała się z energią Esuriit i zarówno jest z tego powodu niezwykle szczęśliwa (reakcja biologiczna) jak i przerażona (reakcja umysłu). Kastor natychmiast próbował zmusić ją do przejęcia kontroli, do zaakceptowania tego, ale jedyne co to sprawiło to dążenie Alicji do coraz głębszej spirali Esuriit. Ostatni agent, Darek, natychmiast kazał Crystal wstrzyknąć inhibitory Alicji. Crystal posłuchała polecenia.

Umysł, magia oraz ciało Alicji się rozerwały. Magia Alicji zaczęła pożerać Alicję. Jej poczucie własnej wartości, jej poziom psychiki ORAZ jej poziom magiczny przestały działać. Alicja jest na krótkiej drodze do stania się istotą Esuriit.

Lucjusz zamanifestował się przy pustogorianach - powiedział, że muszą współpracować by uratować dziewczynkę. On powstrzyma to od środka, stabilizując jej umysł według możliwości, ale oni są na miejscu - muszą naprawić jej ciało oraz skorygować szalejącą magię.

Na miejscu Darek szybko zauważył, że Alicja umiera. Standardowe procedury nie były w stanie jej pomóc, nie z szalejącą energią Esuriit, nawet przy pomocy (od środka) Lucjusza. Darek i Alina skupili się na cyborgizacji młodej czarodziejki - Alina zaczęła esuriityzować wszczepy i łączyć je z magią Alicji, a Darek zaczął utrzymywać ją przy życiu. Jednak za mało, za słabo. Zaczęli ją tracić.

Plan awaryjny - Kastor zwinął Elizę i Rafała, dwie osoby z umysłu Alicji, by oni stali się donorami. Darek wyjaśnił Elizie, że Alicja umiera i potrzebuje jej pomocy. Eliza się zgodziła. Alina silnie unieruchomiła Elizę i zaczęła wykorzystywać jej krew, nerwy i tkankę by tylko zwiększyć kompatybilność wszczepów z Alicją. Straszliwe cierpienie Elizy było dobrym aktywatorem. Jedna straumatyzowana przyjaciółka nie jest wielkim kosztem.

W czasie gdy Darek i Alina ratowali życie nastolatki, Kastor skupił się na rytuale odwrotnej sympatii. Mając ciało Alicji mógł po krwi odnaleźć gdzie jest Lucjusz (który ma krew Alicji bo bez tego nie byłby w jej głowie sympatią). Odnalazł lokalizację Blakenbauera i poprosił o wsparcie - niech otoczą to miejsce i zablokują Lucjusza katalitycznie jak problem minie. 

Tak więc po ciężkich 30h zabiegu udało się ustabilizować Alicję fizycznie i magicznie. Jakkolwiek jej umysł jest w bardzo złym stanie, ale ciało - to co z niego zostało - jest sprawne. W 40% cybernetyczna czarodziejka (już słaba czarodziejka; patrz: tkanka magiczna) nadal jest nieprzytomna, ale Lucjusz jest zdjęty z akcji i nie grozi dziewczynie poważne niebezpieczeństwo. Jest też połączona z Esuriit.

Teraz pozostaje już tylko naprawić jej umysł i zatuszować całą sprawę.

Umysł Alicji jest w stanie bardzo ciężkim. Nie mając wsparcia ani przeszkadzania Lucjusza, Zespół skupił się na konkretnym wspomnieniu - tym, w którym grupa dzieciaków stała naprzeciw Alicji i Rafał ją upokarzał. Alicja wtedy nie zrobiła mu krzywdy. Teraz jednak... sięgnęła do energii Esuriit. Zablokowała to Alina a Kastor spróbował ją przekonać - nie musi tego robić. Są inne ścieżki. Nie tylko Esuriit. Udało się mu pokazać Alicji w jaki sposób używa się energii Esuriit by się nie zatracić (aczkolwiek rykoszety z tej nauki uderzały w prawdziwego Rafała; nic nie poradzimy, lepiej on niż Alicja).

Jednocześnie będą musieli jakoś Alicji wyjaśnić po przebudzeniu dlaczego jest cyborgiem. Wsadzili jej w głowę sztuczne wspomnienia - jej rodzina była zagrożona i Alicja sama to sobie zrobiła by ich chronić. Zbudowali jej wspomnienia że była dobra i dzielna, historię pozwalającą na to, by Alicja mogła i chciała realnie współpracować z Pustogorem.

Ok, ale jak odciąć ją od rodziców i innych?

Stworzyli klona Alicji. Niestety, okazało się, że ów klon wyjdzie na jaw, więc "klon zginął w wypadku samochodowym". Tak więc jakkolwiek najbliżsi Alicji wiedzą, że coś jest nie tak, nikt im nie uwierzy. Wie też Lucjusz, ale po co on ma coś z tym robić...? I tak nie ma co...

Ale - mają sprawną Strażniczkę. Powinna przetrwać 3-4 lata (nie ma woli Kajrata).

## Streszczenie

Po zniknięciu Kajrata i falowaniu energii Esuriit w Czółenku, Pustogor przekształcił Alicję Kiermacz w Strażniczkę Esuriit wbrew Lucjuszowi Blakenbauerowi. Podczas transformacji Alicja prawie umarła i uległa ostrej cyborgizacji. Ale ogólnie się udało - choć mało kto wie, że Alicja żyje.

## Progresja

* Lucjusz Blakenbauer: traci wiarę w to, że Pustogor chce dobrze. Wpada w kłopoty przez wejście w kolizję z agentami specjalnymi Pustogoru. Traci też stanowisko elitarnego lekarza.
* Alicja Kiermacz: ostra cyborgizacja (ok. 40%), za czym idzie silne osłabienie mocy magicznej. Traci też kontakt z rodziną i przyjaciółmi.
* Alina Anakonda: zastępuje Lucjusza jako specjalistka od trudnych przypadków. Pustogor wie, że jej może ufać i że jest wystarczająco bezwzględna.

### Frakcji

* .

## Zasługi

* Alina Anakonda: badaczka otchłani i lekarz operująca w Szpitalu Terminuskim; kontrolowała wysokie energie i udało jej się uratować życie Alicji (zanim ta zginęła). Konfliktowała się z Lucjuszem. Wybitna partnerka (w pracy, nie prywatnie) Lucjusza.
* Kastor Miczokan: agent Pustogoru, inkwizytor; wraz z Aliną i Darkiem weszli w umysł Alicji by zmienić ją w Strażniczkę Esuriit; skonfliktował się silnie z Lucjuszem lecz znalazł go fizycznie.
* Darek Ampieczak: agent Pustogoru, biostymulator; wraz z Kastorem i Darkiem weszli w umysł Alicji by zmienić ją w Strażniczkę Esuriit; podawał jej środki i przeprowadzał zabieg. 
* Lucjusz Blakenbauer: stanął przeciwko TAI Crystal, Alinie i Pustogorowi w ochronie Alicji przed losem Strażniczki Esuriit. I przegrał.
* Alicja Kiermacz: kiedyś foodfluencerka i nastolatka. Teraz - sprzężona z Esuriit, pełni rolę Kajrata jako Strażniczka. Prawie umarła podczas zabiegu; ostra cyborgizacja. Prawie nikt nie wie że żyje.
* Eliza Farnorz: przyjaciółka Alicji została donorem części nowej tkanki Alicji jako cyber-strażniczka. Silnie straumatyzowana.
* Rafał Muczor: Ściągnięty by pomóc Alicji w transformacji w Strażniczkę Esuriit; bardzo ciężko straumatyzowany tymi wydarzeniami.
* Crystal d'Corieris: Zaawansowana TAI medyczna 2.5 generacji, operująca w Szpitalu Terminuskim w Pustogorze. 

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Pustogor
                                1. Rdzeń
                                    1. Szpital Terminuski: gdzie przebudowano Alicję Kiermacz w Strażniczkę Esuriit, by chronić całą okolicę jeśli Kajrat umrze.

## Czas

* Opóźnienie: 6
* Dni: 3

---
layout: cybermagic-konspekt
title: "Atak na Kidirona"
threads: historia-eustachego, arkologia-nativis, zbrodnie-kidirona, arkologia-nox-aegis
gm: żółw
players: fox, kapsel
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [230329 - Zdrada rozrywająca arkologię](230329-zdrada-rozrywajaca-arkologie)

### Chronologiczna

* [230329 - Zdrada rozrywająca arkologię](230329-zdrada-rozrywajaca-arkologie)

## Plan sesji
### Theme & Vision & Dilemma

* PIOSENKA: 
    * .
* CEL: 
    * Bardzo wiele wymiarów celu. Bardzo.
        * Ardilla szuka maga Nihilusa.
        * Muszę pokazać obecność maga Esuriit kontrolującego Trianai i efekt jego działań
        * Kapsel potrzebuje godnej, uczciwej walki w mechanice walki z inicjatywą i wymiarami
        * Zbliżamy się do endgame.
            * Ktoś zauważy, że Infernia jest jedna a arkologia ma wiele różnych interesów - gaszenie pożarów
                * Foreshadowing Overlorda (używa Astinian / Vigilusa? Dark Infernia? Na pewno - cel to Infernia)
    * Więc
        * Wujek wychodzi ze szpitala i Infernia nie słucha jego poleceń
        * Ktoś kontroluje Trianai i Skorpiony mają przechlapane

### Co się stało i co wiemy

* Struktura sesji: "ODKRYCIE SEKRETU"
* Sekret: "Sieć omnikontroli Kidirona (część Czarnych Hełmów to farighanowie) + wszyscy ukrywają sekrety przed Infernią"
* Ofiara: (jawność: ludzie są przerażeni i zagrożeni ;; tajność: wszystko pod kontrolą Kidirona)
* Strażnicy: Siły Kidirona (Czarne Hełmy, Administracja)

### Co się stanie (what will happen)

* S1: 
    * Ardilla ma wiadomość od Stanisława. Stanisław jej mówi, że Kidiron kontroluje całą arkologię. Kidiron coś zrobił z Duchem Arkologii. Niech Ardilla uważa.
    * Eustachy się dowiaduje, że Infernia jest odcinana od Wujka i przekierowywana w stronę Eustachego.
    * Spotkanie z wujkiem. Wujek jest pełnosprawny. Nie może przejąć Inferni.
* S2: 
    * Arkologia została zaatakowana. Prometeus zgasł (artyleria?).
    * "Zabójstwo Kidirona" w centrum dowodzenia - rakieta penetrująca.
    * Zabójca jest dalej w uszkodzonej arkologii.
* ...FIND AND KILL!

### Sukces graczy (when you win)

* .

## Sesja - analiza

### Fiszki

* .Wiktoria Strond
    * ENCAO: +0+0- | Otwarta na doświadczenia;; Perfekcjonistka | VALS: Universalism, Achievement >> Security | DRIVE: Zapewnienie bezpieczeństwa dla wszystkich
    * styl: WB; analityczna, skupiona, lojalna, samodzielna | prawa, porządek, hierarchia, spokój
* .Alexander Rhye
    * ENCAO: -+-++ | Żywi się emocjami;; Wrażliwy na zmiany | VALS: Stimulation, Self-Direction >> Power | DRIVE: Ulepszanie świata
    * styl: UR; dynamiczny, impulsywny, skupiony na przyszłości, nieobliczalny | wolność, przemiana, nauka, chaos
* .Isabella Veritas
    * ENCAO: +-0+0 | Przyjacielska, ale krytyczna;; Nieustraszona | VALS: Conformity, Tradition >> Power | DRIVE: Ochrona prawdy
    * styl: WU; logiczna, skupiona, cierpliwa, honorowa | prawda, wiedza, harmonia, sprawiedliwość
* .Sebastian Morningstar
    * ENCAO: -+-0+ | Ekstrawertyk;; Mistrz manipulacji | VALS: Power, Achievement >> Security | DRIVE: Osobista dominacja
    * styl: BR; agresywny, dominujący, egoistyczny, chaotyczny | wolność, ambicja, odrzucenie norm, przemoc
* .Jasmine Aelirenn
    * ENCAO: 00+-+ | Niezależna;; Ufa swoim instynktom | VALS: Self-Direction, Stimulation >> Conformity | DRIVE: Wolność
    * styl: RG; dzika, spontaniczna, wolna, zdeterminowana | przyroda, wolność, spontaniczność, przygoda
* .Weronika 
    * (ENCAO:  00++-) |Nudna i przewidywalna;;Cierpliwa, jak pająk w sieci;;Życzliwa| VALS: Security >> Conformity| DRIVE: Starsza Siostra Statku
* .Infiltrator
    * akcje: "staję się niewidoczny i nieruchomy", "atakuję z zaskoczenia", "szybko przemieszczam się do strategicznego punktu", "dostosowuję broń do słabego punktu celu", "neutralizuję cel"
    * siły: "ekstremalnie szybki", "niewidzialny", "może manipulować otoczeniem", "szeroki wybór broni"
    * defensywy: "niewidzialność", "ogromna prędkość", "mozliwość szybkiego przemieszczania się", "rozbudowany system ECM"
    * słabości: "wrażliwy na ataki EMP", "mało odporny na bezpośrednie ataki", "może być wykryty przez zaawansowane sensory"
    * zachowania: "atak z zaskoczenia", "unikanie bezpośredniego konfrontacji", "wykorzystywanie otoczenia do swojej korzyści"

### Scena Zero - impl

.

### Sesja Właściwa - impl

ARDILLA:

Ardilla dostała wiadomość od Stanisława - on chce się spotkać z Ardillą w mniej uczęszczanej części arkologii. Ardilla wyszła wcześnie, śledzi go (i się spóźni) ale niech on dojdzie na czas. Stanisław idzie tam "ostrożnie", ale niekompetentnie. Próbuje przedostać się przez trudniejszy teren, nie radzi sobie, robi obejście. I dwójka "dzieci ulicy" już go widzi i monitoruje potencjalny portfel. Ardilla pozwala im zwinąć portfel i zwinie im ten portfel XD.

Tr Z+3 (3Xg -> Stanisław coś widzi):

* X: (podbicie) dzieciaki nie zrobią tego dyskretnie. One widzą Stanisława, który może nie ma kasy, ale wygląda 'bardziej dzianie niż oni'. Plus - nie znasz tych dzieciaków.
* Xg: (podbicie) Stanisław próbuje przekonać ich by tego nie robili. Próbuje ich przekonać pod kątem "lepszego jutra". Oni są wściekli na Stanisława. To nie to czego chcą.
* Ardilla zmienia plan - bierze metalowy element by udawać Czarne Hełmy. Że idą. Wojskowy krok, symulacja.
* V: dzieciaki usłyszały Hełmy i UCIEKAJĄ. Ale to jest paniczna ucieczka. To nie jest "normalna" ucieczka.

Ardilla idzie się spotkać ze Stanisławem - kontynuujemy plan.

* Vz: Stanisław, szczęśliwie, nawet jeśli jest rozbity to NIE AŻ TAK. Dotarł.

Stanisław spotyka się z Ardillą. Wygląda na zmartwionego, naprawdę zmartwionego.

* Stanisław: Ardillo, dobrze Cię widzieć. Jak kariera przedszkolanki?
* Ardilla:  Wie pan, ostatnio gorsze czasy, nie miałam okazji się zająć... ale wolę przedszkolaki niż nastolatki, one są najgorsze.
* Ardilla:  jak się zajmuje nastolatkami?

Stanisław mówi bardzo poważnie.

* Stanisław: Unikaj tej arkologii. Coś tu się dzieje bardzo dziwnego, złego. Nie wiem do końca co. Ale są tu dzieci których nie było. Co więcej... Kidiron coś zrobił z Duchem Arkologii.
* Stanisław: Co więcej, są tu dzieci noktiańskie.
* Ardilla: Ale skąd dzieci noktiańskie.
* Stanisław: (zdziwiony) Z arkologii noktiańskich. Mamy w okolicy arkologie noktiańskie. Najbliższą i najpotężniejszą jest NoxAegis.
* Stanisław: Z tego co wiem, Kidiron robi operacje przeciwko Nox Aegis.
* Stanisław: Dowiem się więcej o Nox Aegis, nie wiedziałem że Ty nie wiesz. Po wojnie lecieli na Neikatis.
* Ardilla: Nie wiedziałam co zrobić z noktianami, a mieliśmy.

Stanisław opowiada jak dzieci malują (dał jako przykład tego że Nox Aegis jest prawdą). Ardilla chce zobaczyć te rysunki. FAKTYCZNIE, są.

(+3Vg)

* Vg: Ardilla nie jest ekspertem od statków. Ale na jednym rysunku jedno noktiańskie dziecko narysowało coś, co kształtem przypomina Szare Ostrze - jednostkę Kidirona. Statek który pożera arkologię.

Stanisław powiedział, że mały noktianin mu uciekł. Nie jemu, im wszystkim. Żyje jako szczur w arkologii. Mówi "nie ma ducha, ale im zostawiam jedzenie. Wiem, że jedzą je dzieci".

EUSTACHY:

Dostałeś bukiet czerwonych róż. Oczywiście, to holoprojekcja. Tak jest taniej. Na serio to jakieś tanie trawsko. Ale nadawca był ciekawy - "Twoja ukochana Kalia". Wysyła jej "której części 'nie jestem zainteresowany nie zrozumiałaś' i 'nie będziesz się narzucać' nie ogarnęła. Odpisała Ci wideoklipem.

Wiadomość od Kalii "detektyw, phi, oczekiwałam większej inteligencji XD".

Eustachy po przyjrzeniu się - jest krótka wiadomość. 

1. Coś się dzieje z Czarnymi Hełmami. Nie wiem co.
2. Infernia jest Twoja. Kidiron uważa, że tylko Twoja.
3. Kidiron uważa, że tylko Ty jesteś warty inwestycji. Infernia jest Twoja.
4. Nikt nie mówi Wam prawdy. Ani mnie.

Eustachy to całkowicie ignoruje. Coś wysłała, ale to nie jest istotne. Coś się zacznie sypać, wszyscy przyjdą do niego i tak.

Wujek wreszcie jest aktywny. Jeszcze w szpitalu, ale już jest "operacyjny", da się spotkać, jakby musiał to zadziała.

Ardilla, Eustachy -> Wujek.

Eustachy kontroluje piratów bo "jak wrzucisz szczura do wody, po 4 h wyłowisz i dasz odsapnąć a potem znowu wrzucisz to 20h może pływać bo ma już nadzieję". Wujek ma dezaprobatę. Eustachy "to zła metafora - dałem im nadzieję i jak się odetną to mogą wrócić do społeczeństwa a jak będą piracić to nie wyłowię z wiadra. Marchewka i kij, ale marchew na sterydach."

Wujek to kupił. Nie dlatego, że jest głupi. Dlatego, bo CHCE wierzyć w dobre serce Eustachego.

* Ardilla:  Nie da się zainstalować generatorów Memoriam na Inferni bo KTOŚ kazał rozwalić kilka sztuk...
* Wujek: (z twarzy wujka trochę odpłynęło krwi) Mamy ile procent aktywnych generatorów?
* Eustachy: (Ardilla myśli że 60, Eustachy wie że 40). 60.
* Wujek: to jeszcze nie jest źle.
* Eustachy: A jaki poziom byłby tym złym?
* Wujek: Nie wiem dokładnie... (mówi szczerze), ale Infernia ma... nietypową strukturę technologiczną, więc ona potrzebuje tych generatorów. Bez tego nie poleci w kosmos. Nie, żebyśmy chcieli.
* Eustachy: A gdyby w wyniku działań operacyjnych doszło do uszkodzenia czy... poziom sprawnych generatorów spadnie do 40?
* Wujek: To byś wiedział. Eustachy, jesteś magiem. (bardzo myśli). Czy dzieje się coś... nietypowego?
* Ardilla:  Coś dziwnego się stało przy ataku piratów... nie widziałam dokładnie, ale ludzie zaczęli ginąć. Chyba Eustachy użył zaklęcia, straciłam kontakt.
* Eustachy: Trochę tak.
* Wujek: Eustachy, to ważne: czy Infernia zabiła TWOICH LUDZI? Czy masz wrażenie - jako mag - że Infernia zabija Twoich ludzi, lub chce? Czy intuicja Ci mówi, że celuje tylko we wrogów?
* Eustachy: To nie jest tak że się mnie słucha, ale... jest tak, że... na razie tylko wrogowie.
* Wujek: Czyli Infernia... Infernia jest jednostką anomalną. To nie jest normalny statek (myśli). Ona się sprzęgnie z magiem. Czyli z Tobą.
* Ardilla:  O ile się to już nie stało? Ostatnio się Eustachy popisywał przed dziewczyną i podrywał ją na Infernię.
* Eustachy: CHCIAŁEM JĄ SPŁAWIĆ
* Ardilla:  Więc żeby ją spławić podrywasz statek kosmiczny! Najlepszy sposób na spławienie! /sarkazm
* Wujek: Inferni JUŻ się nie da zamontować generatorów Memoriam?
* Eustachy: Można wylądować w hangarze z genMem.
* Wujek: Odmówi polecenia. Jeśli jest już obudzona, odmówi polecenia. Lub 'wystrzeli przypadkiem rakietę'. Widziałem to.
* Ardilla:  Słucha Eustachego.
* Wujek: Eustachy jest jedynym magiem na pokładzie i ma silną wolę.
* Wujek: Nie wiem, jak długo... (bardzo myśli, czegoś nie powiedział)
* Wujek: Infernia to mój problem... (głosem osoby która jest skazana na śmierć). Dajcie mi chwilę to się tym zajmę. Muszę... cholera. Pozyskać pewien sprzęt.
* Ardilla:  Możemy Ci pomóc jak coś potrzebujesz.
* Wujek: Nie możecie - nie pozyskasz odpowiednich generatorów. I odpowiedniej ilości. Słuchajcie - Ardilla, stabilizuj Eustachego. Eustachy - nie daj się Inferni. Ona spróbuje Cię... w sumie nie wiem co.
* Wujek: (chwila ciszy) Ostatni mag, który dowodził Infernią gdy nie spała zginął. Potrzebny był strzał w tył głowy. Eustachy - UWAŻAJ.

.

* Ardilla:  Wujku, czemu przydzieliłeś Ralfa do mnie?
* Wujek: Bo Ty jesteś w stanie go... jesteś idealną osobą, by... miał nowe życie. Ty dobrze działasz na wszystkich.
* Ardilla:  Ale... nie jest z nim nic nie tak? Ostatnio w terenie natknęliśmy się na dziwną sygnaturę i Ralf tam był i chyba sygnatura tego którego imienia nie wolno wymawiać...
* Wujek: (patrzy z lekkim niezrozumieniem) Ralf jest noktianinem. Miał... ciężko. Jest bardzo kompetentny i niebezpieczny.
* Ardilla:  Myślę o złych przeżyciach jakie miał. Jak byliśmy w slumsach to przy dziewczynie która próbowała się zabić sygnatura Nihilusa.
* Wujek: (podskoczył na łóżku) Mamy maga Nihilusa w okolicy? Gdzieś tu? 
* Ardilla:  Przeżycie było bardzo intensywne a dziewczyna przeżyła...
* Wujek: Myślisz że Ralf?! (autentycznie jest w szoku) Nie wiem nic żeby był magiem, ale MUSIMY znaleźć maga Nihilusa, jeżeli jest jakiś, bo Infernia jest tego samego koloru.
* Ardilla:  To by pasowało, Ralf wspominał że był na mostku.
* Ardilla:  Na Inferni Ralf zabił dwóch ludzi wyrywając im oczy...
* Wujek: Seilio... nie mam pojęcia. Naprawdę nie mam pojęcia. Ralf nie jest komandosem. Nie powinien umieć zabić dwóch ludzi...
* Ardilla:  Boję się że jak go zapytam to się w nim coś obudzi
* Wujek: Jeśli jest na Inferni to Infernia go obudzi. Więc jeśli... nie wiem, Eustachy?
* Eustachy: Nie wiem...
* Bartłomiej: Ralf nie może być na Inferni, nie jeśli mamy niskie generatory a on może być magiem... Ardillo... nie wiem czy jest dobrze byś się z nim zadawała.
* Ardilla:  Jestem jedyną osobą która go stabilizuje. Jeśli wejdzie w swoje stany savarańskie to nie sądzisz że będzie gorzej
* Wujek: Wiem, że Ralf miał do czynienia z potężną anomalią Nihilusa. Potężną efemerydą. Zniszczyła jego poprzednią jednostkę. Za duże ryzyko, Ardillo, NIE MOŻESZ mieć z nim do czynienia.
* Ardilla:  Jeszcze nawet do końca nie wyzdrowiałeś wujku a już rozkazuje. Nie możesz mi kazać bym się z nim nie spotykała.
* Wujek: Ktoś czuje miętę.
* Ardilla:  NAWET JEŚLI! Radzę sobie z nim lepiej niż ty z tą influencerką.
* Wujek: Zainteresowanie może być obustronne, u mnie niekoniecznie. Partia szachów sam ze sobą jest niemożliwa.
* Ardilla:  Twój towarzysz gra w szachy a Ty w bierki.

(+Ardilla ma rację - Ralf przywiązany jest stabilniejszy +Ralf został przydzielony i jest lojalny -Wujek się troszczy o Was, +Wujek ma dobre serce)

Tp +3:

* X: (podbicie) Wujek się absolutnie nie zgadza, by Ardilla robiła cokolwiek ryzykownego z Ralfem - to jest coś na czym mu BARDZO zależy
* V: (podbicie) Wujek widzi argumentację i uważa, że faktycznie Ralf nie zasłużył na to by traktować go źle
* V: (manifest) Wujek akceptuje, by Ardilla współpracowała i socjalizowała Ralfa. (zaproponował Ardilli zapoznanie Ralfa też ze Stanisławem jako mega poczciwą osobą)
* V: Wujek wierzy, że Ardilla jest dorosłą odpowiedzialną młodą damą. Wie co robi. Dobre serce Wujka przegrywa z logiką i rozsądkiem.
    * Wujek nie będzie się wpieprzał
* V: Wujek wycofał zasadę 'ralf nie może być na Inferni'
* X: Ardilla musi Wujkowi raportować stan Ralfa. Jest KONFIDENTEM.
* V: Wujek powie wszystko o Ralfie co wie.
    * Ralf przyszedł do Wujka z ręki Kidirona.
    * Wujek nie wie dokładnie skąd się Ralf wziął, ale Ralf był przesłuchany pod neurobrożą. Nie był fanem. Ale jest savaraninem - Kidiron go przesłuchał, wujek słyszał przesłuchanie.
        * Ralf powiedział, że widział jak anomalia Nihilusa zniszczyła jego jednostkę
            * WSZYSCY których kochał zostali pożarci przez anomalię Nihilusa
        * Ralf nic nigdy nie mówił o magii. Zapytany czy jest magiem powiedział, że nie jest. ALE! Wujek wie, że to znaczy że Ralf WIERZY że nie jest magiem
        * Ralf spojrzał na oblicze Nihilusa. To sprawiło, że Ralf stracił jakąkolwiek motywację do czegokolwiek. "Nic nie ma znaczenia".
            * Był świetnym załogantem zdaniem Kidirona. Bo nie ma ambicji.
    * Ralf był w statku o nazwie Nox Aegis, to nie była jego jednostka, ale był tam. To była jednostka wsparcia zajmująca się defensywami i konstrukcją.
        
Dobrze się rozmawia z wujkiem. Dawno go nie widzieliście. Po raz pierwszy traktuje Was jak dorosłych. I nagle padł prąd (wszystkie transformatory i reaktor muszą być przeładowane jednocześnie).

Chwilę potem uruchomiły się sygnały i światła awaryjne. Prometeus wysyła czerwony alarm do wszystkich "INFILTRATOR! INFILTRACJA! SABOTAŻ!" i po chwili Prometeus gaśnie.

Wujek chce wyskoczyć z łóżka, Celina mu nie pozwala. MOMENT później eksplozja. Centrum dowodzenia, tam, gdzie zwykle jest Kidiron. Wysadzone.

Infernia się uruchomiła i włącza tryb defensywny.

* Ralf -> Ardilla: "gdzie jesteś"
* Ardilla: "z wujkiem, gdzie jesteś"
* R: "idę tam"

Zgodnie z danymi i parametrami Kidiron nie żyje. Panika wysoka. Lycoris Kidiron próbuje przejąć kontrolę nad arkologią. Czarne Hełmy robią co się da. Chaos, panika, niektóre rzeczy nie działają poprawnie - prąd, TAI itp.

Administratorzy próbują opanować sytuację w arkologii - osłonić ludzi, zająć się itp.

Ex Z +3:

* X (podbicie): ludzie się dowiadują, że Kidiron najpewniej zginął
* X (manifest): wiadomość o śmierci Kidirona jest powszechna - czyli ktoś zajął się tym by zwiększyć chaos
* X (podbicie): konieczność szybkiego działania, ludzie próbują pomóc
* X (manifest): ludzie wchodzą sobie w szkodę i prywatne interesy itp.
* X: arkologia potrzebuje KIDIRONA. Potrzebuje żelaznej pięści. Ludzie przywykli.
* Vr: administratorzy odzyskują kontrolę nad arkologią, ale... straty będą koszmarne.

Eustachy -> Kalia. Kalia panikuje. "RAFAŁ NIE ŻYJE!" i jest pod łóżkiem.

* Eustachy: KALIA! KURWA! SKUP SIĘ NA MOIM GŁOSIE! Skąd informacje że Rafałek się przekręcił!
* Kalia: Wiem gdzie jest!
* Eustachy: Gdzie jest?
* Kalia: Chodźcie do mnie, przeprowadzę Was.
* Eustachy: Powiedz gdzie jest napięta sytuacja!
* Kalia: JA JESTEM BLIŻEJ NIŻ TY! NIE WIEM GDZIE JESTEŚ ALE JA JESTEM BLIŻEJ!
* Eustachy: Czemu nie żyje?

(+Kalia wierzy w Infernię -Kalia panikuje +Eustachy jest kompetentny i jest z Ardillą -Kalia nie ufa w intelekt Eustachego. +ale w sumie jest z Ardillą)

Tr Z +3:

* Vr: (podbicie) Kalia się uspokaja i będzie współpracować
    * Eustachy potrzebuje głosu Kidirona, że żyje i jest bezpieczny - Kalia jako polityczno-morale
    * (+3Vg +5Og reprezentują next target)
* X: Kalia jest ranna. Nie bardzo mocno, ale jest krew.
* Xz: (podbicie) Kalia jest kolejnym celem
* X: (manifest) Kalia ZOSTANIE porwana. Nie zdążycie.
* V: Kalia sama zajmie się tematem "Kidiron żyje, on tu jest, on zarządza" -> bonus do morale, kontroli, ale TO SPRAWIŁO że ona jest celem
    * Zdaniem Kalii Kidiron powinien być w parku, ale tam była eksplozja. On zajmował się jednym z innych planów w jednym z drzew (-> info do Eustachego KTÓRE)
* Og: Kalia powie gdzie jest Kidiron WAM, zobaczycie na wizji porwanie Kalii. Nadal nagranie: KIDIRON ZOSTAWIŁ KALII KIEDYŚ NAGRANIE NA TAKĄ OKAZJĘ.
    * Eustachy jest z nią na wideo. Kalia próbuje coś zrobić z ranami.
    * Kalię porwał Eidolon, lub Eidolon-class

Ralf jest przy Ardilli. Pilnuje jej bezpieczeństwa. Ralf ma wzmacniacze sensorów itp.

* Wujek: "załoga Inferni, ratować arkologię!"

Eustachy zaczyna. ABRAKADABRA. Przyzywa Lancery. Już się rozdzielą wszyscy, ale wydał ROZKAZ do Lancerów przez sympatię z Infernią i swoim Lancerem.

Tr M Z +3 +3Ob +3Og:

* V: (podbicie) Lancery odpowiadają na żądanie.
* Og: NIHILUS. To reprezentuje wzmocnienie Inferni w świetle rozpadających się rzeczy.
* Vm: (manifest) WSZYSTKIE Lancery Inferni się przebudziły. Wszystkie Lancery lecą jako aktywni agenci Eustachego. SZEPT: "Ty wzmacniasz mnie, ja wzmacniam Ciebie".
    * Eustachy dowodzi skrzydłem Lancerów
    * komunikat "defetyzm itp pacyfikowany na miejscu"
    * +lojalność eks-piratów. Piractwo im przeszło. NOPE. Nie wobec takiej siły.
    * (informacja: większość sztabu dowódczego arkologii nie żyje)

ARDILLA.

Ardilla i Ralf są w Lancerach. Ogrody Luksusu są w ruinie, rzeczy płoną, ludzie wołają o pomoc. Jest też to "specjalne drzewo" o którym mówiła Kalia. W drzewie jest szrapnel, wbity. Skanery Lancera pokazują krew. Ardilla -> Ralf "nie możemy dopuścić by te ogrody przeminęły. Ugasimy je, zraszacze."

Ralf szybko użył Lancera, podpiął się do systemów i shuntował prąd.

Tr Z +2 +3Og (coś zabawnego):

* V: (podbicie) Ralf podpiął się i użył zraszaczy. Ogrody przetrwają.
* V: Ralf skutecznie chroni ogrody, dalej monitorując Ardillę.
    * są fajniejsze i mniej fajne drzewa. Ralf skupia się na ochronie tych, które Ardilla lubi bardziej
* Vz: Ralf aktywnie skupia się na pomocy ludziom i triaguje prawidłowo. Plus dowodzi Hełmami.

Ukryta placówka Kidirona jest uszkodzona. Ardilla bez problemu otwiera ją wspomaganymi łapami Lancera. Kidiron jest przebity w okolicach brzucha. Jest półprzytomny. Częściowo wykrwawiony.

Ardilla odpiłowuje Kidirona, usztywnia kolec, wzywa medyka itp.

Tp +3:

* X: Kiridon jest w bardzo złym stanie a to że Ardilla ma Lancera i nie jest medykiem nie pomaga. Kidiron potrzebuje czegoś podobnego jak wujek potrzebował. Będzie wyłączony z akcji.
* VVr: Kidiron trafia do medyków, którzy dają mu FULL HOUSE.
* Vr: Ardilla ściągnęła dane systemu i danych Kidirona na swój Lancer.
* V: Ruch oporu CHCIAŁ dorwać Kidirona. Nawet niektóre Hełmy chciały dorwać Kidirona. Ale dwa Lancery zniechęciły wszystkich - Kidiron trafi do szpitala.

EUSTACHY (cel: znaleźć porywacza i uratować Kalię).

## Streszczenie

Gdy Wujek się obudził, powiedział że z Infernią mogą być ostre problemy jeśli spada poniżej poziomu Memoriam. Zmartwił się hipotezą Ardilli że Ralf może być magiem Nihilusa, ale Ardilla walczyła o dalsze spotykanie się z Ralfem. Zamach na Kidirona. Większość jego sztabu umiera, ale sam Kidiron przeżywa, acz ciężko ranny; ratuje go Ardilla i pozyskuje info o jego mrocznych planach. Kalia bierze na siebie morale i propagandę, ale przez to wpada na celownik Infiltratora który ją porywa. Eustachy próbuje odbić Kalię z rąk Infiltratora w koloidzie...

## Progresja

* Ardilla Korkoran: ma dostęp do wszystkich planów operacyjnych Kidirona. Sekrety farighanów, trianai, bazy Nox Aegis... wszystko.
* Kalia Awiter: została ranna, na szczęście nie bardzo ciężko w zamachu na Kidirona. Za to jest porwana XD.
* Kalia Awiter: zna więcej sekretów i skrytek Kidirona niż ktokolwiek inny.
* Rafał Kidiron: miał szczęście i nie zginął w zamachu. Został jednak bardzo ciężko ranny. Co najmniej 2 tygodnie ciężkiego szpitalnego leczenia.

### Frakcji

* .

## Zasługi

* Ardilla Korkoran: od Stanisława usłyszała o dziwnej arkologii Nox Aegis; potem broniła Ralfa przed wujkiem (tak chce się dalej spotykać). Gdy był zamach na Kidirona, skupiła się na samym Kidironie i go z Ralfem uratowała, ku swym mieszanym uczuciom. Ale dostała dostęp do jego mrocznych planów.
* Eustachy Korkoran: bukiet czerwonych róż od Kalii okazał się być dyskretną wiadomością, którą przeoczył. Z wujkiem przegadał stan Inferni. Gdy był zamach na Kidirona, połączył się z Kalią i ją uspokoił; Kalia prawidłowo podnosi morale i zostaje porwana przez Infiltratora. Jego magia dała skrzydło Lancerów z Inferni; tak uzbrojony, leci za Infiltratorem.
* Kalia Awiter: zostawiła Eustachemu podpowiedź, że Kidiron robi coś groźnego. Jak był zamach na Kidirona, spanikowała gdy została ranna. Po rozmowie z Eustachym, odpaliła fałszywą wiadomość od Kidirona że jest bezpieczny itp. Powiedziała Eustachemu gdzie jest Kidiron.
* Stanisław Uczantor: poprosił Ardillę o spotkanie by poznać sekrety arkologii Nox Aegis. Ardilla nic nie wie. Wie o tym, że w okolicy jest więcej noktiańskich dzieci niż powinno być.
* Bartłomiej Korkoran: wreszcie zregenerowany. Nie wiedział nic o teorii 'Ralf jest magiem Nihilusa'. Próbuje chronić Eustachego przed Infernią, Infernię przed memoriam, Ardillę przed Ralfem. Po raz pierwszy potraktował Eustachego i Ardillę jak dorosłych. Gdy był atak na Arkologię, przejął kontrolę tymczasowo. Z łóżka w szpitalu.
* Rafał Kidiron: miał szczęście - zajmował się swoimi planami poza spotkaniem głównym i nie było go gdy Infiltrator próbował go zabić. Nie on a miragent zginął (wraz z całym sztabem R.K.). Bardzo ciężko ranny w swoim pomniejszym schronieniu. Uratowany przez... Ardillę. Jest TWARDY DOWÓD, że bez niego arkologia nie przetrwa.
* Lycoris Kidiron: próbowała przejąć kontrolę nad arkologią gdy Rafał "zginął". Przetrwała i ogólnie dobrze że tam była, ale nie nadaje się do dowodzenia arkologią.
* Ralf Tapszecz: Ardilla podejrzewa go o bycie magiem Nihilusa. On jeszcze nie wie. Gdy doszło do ataku na arkologię, Ralf od razu pojawił się przy Ardilli i chroni ją i tylko ją. Skutecznie zgasił pożar drzew w arkologii, zaczynając od drzew które Ardilla lubi najbardziej.
* Dalmjer Servart: infiltrator; uszkodził BIA i wstrzyknął jej Malictrix, zniszczył sztab Kidirona, ma koloidowy sprzęt i chciał zabić Kidirona. Ucieka z porwaną Kalią.
* SAN Szare Ostrze: wykorzystana w przeszłości do polowana na noktiańskie dzieci przez Kidirona, co widać na rysunkach noktiańskich dzieci.
* OO Infernia: conduit dla magii Eustachego; wzmocniona przez entropię. Okazuje się, że ma już tylko 40% aktywnych generatorów memoriam i że PRZEKSZTAŁCIŁA kiedyś jednego maga, którego Bartłomiej Korkoran musiał zabić.

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Neikatis
                1. Dystrykt Glairen
                    1. Arkologia Nativis
                        1. Dzielnica Luksusu
                            1. Ogrody Wiecznej Zieleni: w płomieniach i uszkodzone po ataku na Kidirona. W jednym z drzew była ukryta mała kryjówka Kidirona w której był i teraz został ranny.
                        1. Stara Arkologia Wschodnia: tam jest tymczasowa baza Infiltratora

## Czas

* Opóźnienie: 6
* Dni: 2

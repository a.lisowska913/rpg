---
layout: cybermagic-konspekt
title: "Akt, o którym Marysia nie wie"
threads: rekiny-a-akademia
gm: żółw
players: kić, anadia
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [211221 - Chevaleresse infiltruje Rekiny](211221-chevaleresse-infiltruje-rekiny)

### Chronologiczna

* [211221 - Chevaleresse infiltruje Rekiny](211221-chevaleresse-infiltruje-rekiny)

## Plan sesji

### Theme & Vision

* ?

### Ważne postacie + agendy

* Chevaleresse: infiltratorka, która próbuje znaleźć Stasia Arienika dla Alana.
* Mysiokornik, Santino: boi się, że jego wielki plan się rozsypie. Próbuje utrzymać sekret, który ma.
* Arienik, Urszula: szuka Stasia, swojego syna.
* Burgacz, Barnaba: porwał Stasia Arienika. 
* Bartozol, Alan: chodząca maszyna zagłady. Nic złego nie może się stać Chevaleresse.

### Co się wydarzyło

* Przez brak Administratorki systemy obronne były długotrwale wyłączane / hackowane. Korzystali z tego zarówno Chevaleresse jak i Santino.
    * Santino sprowadzał sobie młode dziewczęta; zrobił z tego "bezpieczną bazę" i był otoczony podziwem. Niewielkim kosztem ;-).
        * M.in. znalazł i ściągnął poszukiwaną przez prawo Melissę Durszenko z Kultu Ośmiornicy i eks-przyjaciółkę Chevaleresse 
    * Nieświadomy tego wszystkiego Babu ściągnął Stasia Arienika na 24h, by postraszyć Urszulę. Staś nie chce mieszkać z rodzicami - chce być Rekinem.
    * Chevaleresse wiedząc o działaniach Santino zinfiltrowała Rekiny. Szuka Stasia dla Alana i Melissę dla siebie.
        * Santino przyuważył Chevaleresse myśląc, że to jedna z jego dziewczyn. Chevaleresse znalazła "sprawcę". Zdecydowała się na ewakuację i powiedzenie o wszystkim Alanowi.
            * Znalazła Melissę... nie znalazła Stasia.
* przed Marysią NADAL stoją trzy pytania:
    * Co robimy z tym, że biją Torszeckiego :-(.
        * I jak to zrobić żeby nie było, że biją go bo Marysia pozwala bo woli Ernesta? 
        * Lub że Marysia go chroni więc go kocha?
    * Jak to zrobić, by Justynian oddał władzę lub by Marysia ją przejęła?
    * Myrczek x Sabina - co z tym robić?

### Sukces graczy

* .

## Sesja właściwa
### Scena Zero - impl

.

### Scena Właściwa - impl

Czyli do problemów z:

* Torszeckim
* Ernest - Rekiny
* Justynian "pragnący" władzy
* Myrczek x Sabina

dochodzą Marysi jeszcze problemy:

* Kult Ośmiornicy x Santino Mysiokornik
* Chevaleresse? (ale na pewno ród Arieników i Rekiny x Arieniki)
* Oficjalna Administratorka Aurum i jedna niesforna Hestia

Karolina nie ma problemów. Karolina siłuje się z bratem. Przegrywa. Ale i tak się siłuje.

Do Marysi odzywa się wzburzony Torszecki. Przybył elegancko, jak to on.

* "Tien Sowińska... hańba i problem" - Torszecki śmiertelnie poważnie
* "Mhm?"
* "Ktoś ośmielił się posiadać lub dystrybuować AKT tien Sowińskiej."
* "Coś więcej?"
* "Akt, podobno Twój!"
* "Widziałeś go?"
* "Nie. Nie ośmieliłbym się" - ale ma kurwiki w oczach
* "Kto Ci o tym powiedział? Skąd wiesz? Skąd masz pewność, że to mój? Nie przypominam sobie, bym ostatnio jakieś akty robiła..." - żartobliwa powaga Marysi
* Torszecki aż się ŻACHNĄŁ - "TIEN SOWIŃSKA! Akt to poważna sprawa. To brak szacunku dla Ciebie i Twego rodu! To coś, co robią takie niskie damy jak Arkadia Verlen! I nie tylko...". "Amelia NIGDY nie miałaby aktu zrobionego." (domyślnie: bo jest zdechłą rybą)
* "Może miała, ale nie wiesz. Ale do konkretów - mogę powiedzieć, że słyszałam, że istnieje Twój akt. Czy to znaczy, że istnieje naprawdę?"
* "Tien Sowińska, dla Ciebie, mogę zrobić akt." - Torszecki z UŚMIESZKIEM
* Marysia patrzy na Torszeckiego jakby miała zwrócić wczorajszą kolację. Torszecki nie posmutniał. On wie, jakie są _Tsundere_.
* "Czy to te konkrety? Torszecki, OPANUJ SIĘ."
* "Nie mam ich. Mam swoje źródła. Lokalizują akt w okolice AMZ. Dokładniej - w okolice Napoleona Bankierza, miłośnika sztuki."
* "Zemsta rodzinna?" - Marysia, zaciekawiona
* "Może..?" - Torszecki, zamyślony - "Napoleon nie chwali się tym aktem. To znaczy, mają jakiś klub artystyczny. Oglądają sztukę. I tam był Twój akt. Podobno."

Marysia chce to olać. Ma to gdzieś. To nieistotne. Nie pozowała do aktu. Nie było takiej imprezy a AŻ TYLE to nie piła.

* "Zostaw to. Naprawdę jest mi to obojętne." - Marysia do Torszeckiego - "Zajmę się tym jutro... albo jak będzie z tym problem..."
* "Ale... ale..." - Torszecki nieszczęśliwy - "Podobno ETERNIANIN chce ten akt zobaczyć..."
* Marysia się uśmiecha w duchu, ale kamienna twarz - "Może lubi podziwiać sztukę..."

Rafał Torszecki zrobił kamienną minę. Ukłonił się głęboko Marysi i wyszedł z pokoju.

Marysia przeszła się do Ernesta. Ernest jest u siebie, pracuje na swoich zaawansowanych komputerach. Optymalizuje przepływy energii przez Dzielnicę Rekinów z Azalią.

* "Hej, zajęci jesteście?"
* "Dla Ciebie nie jestem" - Ernest się uśmiechnął. Ruchem ręki odesłał Azalię.
* Marysia ma serduszka - "Doszły mnie plotki, że podobno chcesz zobaczyć mój akt".
* Ernest uśmiechnął się czarująco, ale Marysia widzi defensywną minkę numer 7. CZEMU ONA PYTA! "Słyszałem o tym, że podobno pojawił się akt tien Sowińskiej. Naturalnie, zainteresowałem się tematem. Nie wiedziałem, KTÓREJ tien Sowińskiej akt dotyczy".
* Marysia się uśmiecha równie czarująco. "Udało Ci się dotrzeć do tego aktu?"
* "Jeszcze nie, Marysiu, Keira się tym zajmuje." - Ernest z uśmiechem.
* "Ale nikt nie zginie przy okazji?"
* "Oczywiście, że nie. Jest infiltratorką. Ma go wykraść, nie zrobić komuś krzywdę. Jeśli to akt Amelii, zrobiony wbrew jej woli, oczywiście będę zainteresowany wyjaśnieniami od posiadacza..." - Ernest zawiesił tu temat.
* "Wiadomo, kiedy Keira wróci?"
* "Nie, nie jest łatwo pozyskać niektórych rzeczy. A Keira... nie jest z tych rozmownych."
* "To jak będziesz miał ten akt, daj mi proszę znać. Chcę go obejrzeć."

Ernest zrobił minę i potwierdził, jakby to było naturalne, że to zrobi (spoiler: w życiu tego nie zrobi) i powiedział, że na pewno trafi wiadomość do Marysi. (a dyskretnie -> KEIRA! DYSKRECJA! MARYSIA NIE MOŻE WIEDZIEĆ GDY GO ZDOBĘDZIESZ PÓKI NIE DOWIEM SIĘ CO TO JEST!!!)

Marysia używa całego wewnętrznego uroku i wszystkich rzeczy których się nauczyła na dworze. Czemu Ernest chce ten akt? GADAJ! I jak go będzie miał, chcę go :3

TrZ (relacja + on oficjalnie x Amelia, tak?) +3:

* X: Ernest jest niesamowicie śliski. Oczywiście, wszystko ładnie pięknie - ALE jeśli będzie miał ten akt, nie pokaże go Marysi. Może pokazać jej KOPIĘ ale nie ORYGINAŁ. Bo w końcu chodzi Marysi o to by zobaczyć co tam jest a nie o oryginalne dzieło.
* Vz: NIECH TO JEST akt Marysi tego typu, by Ernest się zaczął zastanawiać. + "Hej, jeśli chcesz go zachować bo Ci się podoba... możesz po prostu powiedzieć ;-)". FLIRT++. Ernest ma szczękę gdzieś koło stóp. Ofc nie widać po nim tak bardzo, ale jest zaskoczony. +2Vg.
* X: Ernest _compelował_ Marysię, że jeśli tak, to jak ona zdobędzie akt pierwsza to niech da mu dostęp do oryginału. Powiedział czemu - jeśli jest świetny, on chce znaleźć autora. Nie sądzi, by Marysia zrobiła sobie akt. Więc jeśli to jest świetny akt, to autor potrafi pracować na referencjach.
* X: Rekiny i Torszecki są za linią "Marysia x Torszecki". A wszyscy są przeciw linii "Marysia x Ernest". Torszecki chce wykraść akt PIERWSZY. I go zniszczyć. I ma dużą przewagę na Keirą.
* V: Ernest się zainteresuje Marysią nie tylko jako koleżanką. Nie mógł przez Marysię spać po nocach ;-). "Wiesz, jeśli znajdziemy tego autora i jeśli będzie odpowiednio dobry, to możemy zrobić specjalny akt dla Ciebie ;-)". I to wyłącza Ernestowi spokojny sen. +2Vg.
* V: Ernest, w lekkim szoku, się wygadał - chciał zdobyć akt by zobaczyć jego jakość, jeśli faktycznie autor świetnie rysuje z referencji to chciał albo zrobić SWÓJ akt dla Amelii albo JEJ akt dla siebie. Ale teraz... teraz ma inne plany ;-).

Marysia i Ernest mają wspólny plan. Zdobyć ten cholerny akt. ZANIM TORSZECKI GO ZNISZCZY.

Cztery godziny od momentu w którym Marysia dowiedziała się o istnieniu aktu poczuła się zasmucona, że ów akt może ulec zniszczeniu. A więc - RATUJMY AKT!

* "Karooo..." - Marysia, uśmiechająca się szeroko po hipernecie. Taki banan.
* "Co tam? Zeżarłaś kota i myszkę?" - Karo
* "Chcesz się pobawić? Bo jest sprawa. Kojarzysz Napoleona Bankierza? Jesteś w stanie odebrać od niego mój akt?"
* "A ma przeżyć w całości czy w kawałkach?"
* "Przeżyć. I masz zdążyć przed Torszeckim. A mnie zaczęło zależeć na tym akcie."
* "Mówisz... a Torszecki wie, gdzie jest ten akt?"
* "Tak, powinien wiedzieć."
* "Skąd wiesz że już go nie ma? Może go już znalazł."
* "Możemy mu go zabrać póki go nie zniszczył. Może..."
* "Słuchaj laska, prosta sprawa. Bankierz ma Twój akt. Kiedyś Ty zdążyła akt zrobić?"
* "Nie zdążyłam. Dlatego chcę go zobaczyć."
* "To idź do Bankierza i zażądaj oddania aktu?"
* "...w sumie niegłupie..."
* "CHODŹ ZE MNĄ JAKO OBSTAWA! Będziesz robiła groźne miny."

Karo podjeżdża ścigaczem pod lokal Marysi. Marysia wskakuje na swój i lecą w dwójkę. Do AMZ. Dotarły szybko pod akademik. Karo podbija do pierwszego z brzegu ucznia - "Gdzie Bankierz?". "Na Arenie." "Gdzie Arena?" Wskazał.

Napoleon Bankierz na Arenie. Półnagi. Pokonał kilku.

Karo wyskakuje na Arenę. Chce się zmierzyć z Napoleonem, ale nie zamierza grać czysto. Napoleon jest po bitwie, ale są przyzwyczajeni. Żadne nie ma przewagi. Napoleon jest katai, ale nie używa tego w walce teraz. A Karo to brawler. Napoleon ma pałkę. Karo nie ma nic - odrzucił. Napoleon jest półnagi, w samych bryczesach.

A na widowni ze strony lasek taka żałość.

Tr+3:

* V: Napoleon atakuje w centrum. Karolina unika i chce zmienić pozycję na lepszą taktycznie. UDAŁO SIĘ: +3V
* V: Karo próbuje go podciąć. On atakuje w ziemię, by walnąć w nią piaskiem. Karo zna sztuczki - fikołek i swoją masą by go obalić. Skutecznie - przewróciła go. Karo chce go złapać; zwarcie, tackle. I na uszko "oddaj ten akt". +Vg.
* V: Napoleon jest częściowo unieruchomiony, jest w kiepskiej pozycji, ale jest SILNIEJSZY niż Karo. Odszepnął "nie jest mój, pożyczony" po czym próbuje Karo ZŁAPAĆ i PRZERZUCIĆ NA BOK. 
* V: Karo na to idzie by dobrze wyglądał dla lasek na arenie. Czyli udało mu się, ale Karo się po chwili wyślizgnęła, bo on na to pozwolił (bo ona chciała efektowność). Jest zaskoczony tym jak źle mu idzie ta walka, ale ma zamiar walczyć porządnie. Karo robi zwód jakby szła w jego nogi, po czym zmienia kierunek ataku - chce założyć mu na chwilę blok. By powiedzieć "ktoś próbuje go wykraść".
* X: Napoleon nie dał sobie założyć bloku, tylko przerzucił Karo, która grzmotnęłaby plecami o ziemię, tyle, że wymanewrowała i odskoczyła na rękach, unikając ciosu stopą Napoleona. Ale odpowiedział "nie wiedziałem, ale ma go Liliana - jej trudno coś ukraść". +3X. Korzystając z tego że na MOMENT Karo jest pod światło i zdebalansowana, zrobił bull rush.
* V: Karo uniknęła ataku i go podcięła. Napoleon poleciał, zdebalansowany. Utrzymał się na nogach, ale niewiele to dało. (-3X). Karo na dystans, ukłon - koniec starcia. Napoleon wraca do balansu i też się ukłonił. Oczywistym jest, że Karo wygrała tą walkę, ale Napoleon dobrze wypadł. Uścisnęli sobie dłonie.

.

* Karo -> Napoleon po hipernecie: "Nie znam Twojej kuzynki, nie wiem jak jest kompetentna, ale ten kto chce akt zwinąć jest bardzo kompetentny. Czyj akt?"
* Napoleon -> Karo: "To jej akt. Nie wiem, skąd go ma. Jest super."
* Karo -> Napoleon: "Dostanie za niego kasę jak jest dobry"
* Napoleon -> Karo: "Nie wiem, czy się zgodzi. Ale - pogadaj z nią. Liliana mnie nie kocha ;-)."
* Karo -> Napoleon: "Nierozsądna laska ;-)". I poufale klepie Napoleona po tyłku by wzbudzić zazdrość lasek na widowni. Podkręca mu reputację.

.

Marysia idzie do Liliany. Puka do drzwi jej akademika. Słyszy "proszę".

* "No niezły żart. Przebrałaś się za Marysię Sowińską?" - Liliana, zdziwiona do Marysi
* Marysia pinguje ją po hipernecie. - "Aż tak źle dzisiaj wyglądam?"
* Liliana robi głupią minę. Po chwili wybucha śmiechem. - "TY tutaj?"
* "Podobno jesteś moją wielką fanką."
* "NIE?!" - Liliana aż urażona
* "To dlaczego masz mój akt?" - Marysia, zaciekawiona
* "Bo jest ładny?" - Liliana, nie rozumiejąc pytania - "Świetna robota? Sztuka?"
* "Kto go zrobił i gdzie go masz? Ktoś chce go ukraść i chcę go od Ciebie kupić."
* Liliana znowu zrobiła głupią minę. Po chwili wybucha śmiechem, ponownie. - "Pozwolisz?" - i pokazała Marysi swoją szafę. W szafie znajduje się ogromna ilość kartek, segregatorów, różnego rodzaju rzeczy. Jak Marysia na nie spojrzała, wszystkie zaczynają wyglądać jak jakaś forma aktu Marysi. Kiepska, ale zawsze. - "Iluzyjne echa plus sugestia. Ktoś, kto chce mi coś zabrać myśli, że to wziął. Nie udało się jeszcze nikomu do tej pory zabrać mi czegoś stąd."
* "Próbowali?"
* "Często." - Liliana z uśmiechem - "Czasem się im udaje. Ale tego jeszcze mi nie dali rady zabrać."
* "Sprzedasz ten akt? Jeśli jest tak dobry jak myślę, chcę dla autora zlecenie".
* "Nie wiem kto jest autorem" - Lilana mówi szczerze - "I nie mogę Ci powiedzieć, skąd to mam."

Marysia chce się dowiedzieć od Liliany tego, co ta wie. 

TrZ (słowo Marysi, że nie zrobi nic autorowi ani nikomu powiązanemu do Liliany) +4:

* V: Liliana da Ci akt.
* X: Liliana nie ma już aktu. Wykradziony. Ale jest jeszcze w zasięgu. I Liliana wie kto.
* X: W sumie, Liliana nie powie kto - musi chronić.
* X: JUŻ ma go Torszecki. ALE nie zdążył zniszczyć. I MARYSIA WIE ŻE ON GO MA. I ŻE GO ZNISZCZY.

.

* Marysia, NATYCHMIAST, po hipernecie do Torszeckiego. "Rafał, gdzie jesteś! Musimy porozmawiać zanim cokolwiek zrobisz!". Cały autorytet Marysi możliwy.
* Torszecki do Marysi: "Tien Sowińska, cieszę się, że Cię słyszę. Jak mogę pomóc?"
* Marysia -> Hestia: "CZY TORSZECKI JEST NA TERENIE REKINÓW?" Hestia: "Niestety, nie, tien Sowińska."

Karolina -> Daniel: "BRACIK! Pronto! Potrzebuję Ciebie TUTAJ. Znajdź mi Torszeckiego, ale na wczoraj. Słuchaj, chcesz zobaczyć akt Marysi? TO ZNAJDŹ TORSZECKIEGO!"

Daniel jest odpowiednio zmotywowany. Będzie szukał. I jest wdzięczny siorce. Nawet jak poprosiła nie zbije Torszeckiego - znajdzie go dyskretnie i pokaże jej gdzie on jest.

TrZ (Bo zna Torszeckiego. Wie jak on myśli. Pomagał mu. I ma wyczucie. PLUS info od Napoleona o kryjówkach) +4:

* X: Marysia musi kupić czas dla Daniela.
* X: Bez wpierdolu się nie uda. Daniel zaatakuje by zdążyć. Daniel angażuje do pomocy drony. Swoje drony. (+2Vg)
* Vg: Drona znalazła Torszeckiego. I wybuchła (bo Daniel to biedaartefaktor a drona była zrobiona z mydła). Stąd Daniel musiał atakować.

MARYSIA -> TORSZECKI, wstecznie.

TrZ (szacunek, zaufanie itp) +3:

* Vz: "Rafał! Musimy poważnie porozmawiać! Jestem w ciąży! Z Tobą (+2Vg)". Torszecki zgłupiał. ALE JAK TO. ONI PRZECIEŻ NIE. "NO WŁAŚNIE MI TO WYTŁUMACZ! JA CI UFAM! POWIEDZ MI JAK! JAK MOGŁEŚ!" (+2Vg)
* V: Torszecki: "MOŻE TO ROBAK! ROBAK CI TO ZROBIŁ! NIC NIE PAMIĘTAM!". Marysia udaje ostrą panikę. I ostry zawód - jak ON, TORSZECKI mógł! A ona mu wierzyła, ufała mu! (+2Vg)
* V: Wystarczające opóźnienie.

Wpierdol od Daniela przyleciał z powietrza... Torszecki stracił przytomność. Ma akt. Tzn, Daniel ma akt. Jak Karolina się pojawiła, Daniel się cieszy. Akt jest czarno-biały, jest bardzo stylowy. Marysia jako "lekko figlarna" ale też z powagą, świat na jej barkach. Siedzi zamyślona na łóżku. To nie jest akt typu erotycznego, to jest akt. Teraz Karo zrozumiała, czemu Napoleon powiedział "sztuka". Nawet oddane światło, cienie ze świecy. W tle, co ciekawe, monitor Hestii. Czyli ktoś wie jak ten pokój wygląda.

Daniel delikatnie wziął Torszeckiego (boi się wstrząsu mózgu czy czegoś więcej) i odwiózł do Sensacjusza. Marysia nie wie, że Torszecki dostał tak poważny wpierdol. Daniel ma nadzieję, że Marysia nie wie, że to był on. Bo myśli, że Torszecki x Marysia - akt tego dowodem.

W głowie Daniela jest już wizja - Marysia ma romans z Torszeckim. Dała mu swój akt. Fakt jak ten akt wygląda potwierdza, że mają romans. I Karo poprosiła go by zdobył ten akt, bo Marysia zrobiła coś głupiego - nie daje się Torszeckim takich rzeczy. Więc Daniel to dla Karo zdobył by ona Marysię opierdoliła. O. I "ups, wpierdoliłem mu za mocno" - to jest kłopot w tym świetle.

Dwie rzeczy:

* Marysia chce dowiedzieć się od Liliany kto jest autorem.
* Chciała przekazać Ernestowi ten akt i info o autorze ;-). I coś zarzucić by nie spał.

AD 1:

Liliana ani słowa. Marysi. Jak grób. Serio. Nie chce powiedzieć. Marysia zatem ma dla niej propozycję - da jej zdjęcie, by z tego zdjęcia (lub serii zdjęć) autor stworzył inny utwór. I jaka cena. Liliana powiedziała, że ona ceny nie zna. Serio.

TrZ (bo Marysia pokazała, że gra fair w tej kwestii) +2:

* X: Liliana nie daruje Marysi tego trackera.
* V: Liliana puści zdjęcia, ale je zbada.
* V: Liliana przeoczy tracker, artysta też.

Następnego dnia Marysia zrobiła sobie serię zdjęć. Coś w sukni itp. Z różnych kątów. I Liliana faktycznie doprowadziła do przekazania ich artyście. I - ku wielkiemu zdziwieniu (lub nie) Marysi - artysta okazał się być Rekinem. A dokładniej - Lorena Gwozdnik.

Z tymi informacjami Marysia idzie do Ernesta. Ładnie ubrana. Tak ładnie, ale trochę uwodzicielsko. Tzw. "1 guziczek mniej". Z przyjemnością przekazała Ernestowi akt i zobaczyła jak brwi lecą mu do góry. Marysia lekko opuściła wzrok, leciutki rumieniec, lekkie krygowanie się. Lekkie, taktyczne spłonienie się. Ernest jest pod wrażeniem.

* "Wow". - Ernest nie wykazał się elokwencją.
* "Miło mi, że Ci się podoba" - Marysia kładzie mu na biurku karteczkę z nazwiskiem i namiarami na autora.
* "Czyli mogę liczyć na to, że mogę poprosić autorkę o akt dla mnie? Twój?" - Ernest
* "Już zamówiłam :-)" - Marysia - "Ale autorka nie wie, że to ja zamówiłam. Nie chcę, by Rekiny plotkowały..."
* "Ale jak będziesz jej pozować jak nie będzie wiedzieć?"
* "Ja zidentyfikowałam artystkę." - Marysia, z uśmiechem - "Ty musisz poprosić artystkę o mój akt."
* "Miałem nadzieję na to, że zapozujesz dla artystki." - Ernest, z uśmiechem
* "Nie znamy się dość dobrze :-)" - Marysia, kończąc rozmowę - "Mniej plotek to lepiej dla mnie".

Lorena - gdy Ernest przyszedł do niej po akt - poprosiła go o ochronę przed Marysią. Od Liliany wie, że Marysia wie o jej akcie. Więc Lorena myśli, że jest mistrzynią spisków. Ernest, który wie, że Marysia chciała by on miał ten akt nie rozumie o co Lorenie chodzi (bo Marysia nic do niej nie ma) ale spoko. Nie ma problemu. Ma akt a ta artystka jest trochę rozchwiana, ale to chyba normalne...

Dziwni są ci magowie w Aurum...

Torszecki w szpitalu leży tydzień. Po pierwsze, bo Daniel ma ciężką pięść. Po drugie, bo Sensacjusz chce, by Torszecki trochę odpoczął...

OSTATNIA MYŚL TORSZECKIEGO: "Przez robaka mam dziecko z Marysią i co będzie dalej?!?!?!"

## Streszczenie

Torszecki ostrzegł Marysię, że jej akt krąży w Zaczęstwie. Ona to olała. Ale jak Ernest zaczął na niego polować to Marysia się zainteresowała. Zdecydowała się aktem odbić Ernesta Amelii. Zaflirtowała z Ernestem, obiecała mu akt specjalnie dla niego i że odda mu oryginał tego. Karo pokonała Napoleona, znalazła link do aktu który ostatecznie zdobył Torszecki. Daniel dla Karo zbił Torszeckiego i odzyskał akt. Marysia zaflirtowała z Ernestem ;-). Torszecki, który chciał dobrze i chronił Marysię skończył w szpitalu.

## Progresja

* Marysia Sowińska: wyciągnęła od Liliany Bankierz informację pośrednio (trackerem) kto jest autorem aktu Marysi (Lorena). Przez to zaskarbiła sobie jej wściekłość i zemstę.
* Liliana Bankierz: Marysia Sowińska wyciągnęła od niej informację pośrednio (trackerem) kto jest autorem aktu Marysi (Lorena). Przez to zaskarbiła sobie jej wściekłość i zemstę.
* Lorena Gwozdnik: pod ochroną Ernesta Namertela by Marysia Sowińska nie zrobiła jej krzywdy (nie, żeby chciała).
* Ernest Namertel: zaczął patrzeć na Marysię Sowińską inaczej. Może nie tylko na zimną damę, ale też na interesującą partnerkę w łóżku..? Ma jej akt i mu się bardzo podoba.
* Rafał Torszecki: tydzień w szpitalu, pobity przez Daniela. Bo próbował ochronić Marysię przed jej aktem "na wolności".
* Liliana Bankierz: przyjaciółka Loreny Gwozdnik. Połączyło je upodobanie do piękna.
* Lorena Gwozdnik: przyjaciółka Liliany Bankierz. Połączyło je upodobanie do piękna.

### Frakcji

* .

## Zasługi

* Marysia Sowińska: gdy dowiedziała się, że jej akt krąży po Zaczęstwie to wykorzystała go do sprawienia, by Ernest nie chciał spać po nocy ;-). Uwodzi Ernesta, by odbić go Amelii. Dla niej Daniel zbił Torszeckiego który ją chronił.
* Karolina Terienak: zmierzyła się z Napoleonem Bankierzem na arenie i go pokonała z łatwością; podniosła mu reputację. Ściągnęła brata, by ten odzyskał akt od Torszeckiego.
* Rafał Torszecki: ostrzegł Marysię przed tym że jej akt istnieje. Jak ta zignorowała, sam go zdobył od Liliany Bankierz. Zanim zdążył go zniszczyć, Daniel Terienak go pobił i mu zabrał ów akt.
* Keira Amarco d'Namertel: zabójczyni wysłana w roli infiltratorki, by pozyskać akt Marysi. Jest dobra, ale nie tak dobra jak Torszecki, który był szybszy. No i nie zna terenu który infiltruje.
* Ernest Namertel: chciał zdobyć akt Marysi (wysłał Keirę) i może poprosić o akt Amelii artystkę (Lorenę). Ale po rozmowie z Marysią zaczął na nią trochę inaczej patrzeć; został cząstkowo uwodzony. Zainteresował się nią.
* Napoleon Bankierz: zmierzył się z Karo na arenie i przegrał ostro, ale Karo podniosła go reputacyjnie. Powiedział jej o akcie Marysi i o tym, że ów należy do Liliany. Należy do miłośników piękna.
* Liliana Bankierz: należy do miłośników piękna. Ma akt od Marysi Sowińskiej i ma bliski kontakt z Loreną Gwozdnik. Nie wydała Marysi Loreny jako artystki, ale przekazała Lorenie zdjęcia z trackerem. Nie daruje Marysi.
* Daniel Terienak: wezwany awaryjnie przez siostrę do wyśledzenia Torszeckiego i jego kryjówek by zdobyć akt Marysi Sowińskiej zanim on go zniszczy. Znalazł Torszeckiego, ale by go zatrzymać musiał go ciężko zbić.
* Lorena Gwozdnik: stworzyła piękny akt Marysi Sowińskiej mającej wszystko na głowie, który trafił do Napoleona Bankierza. Dowiedziała się, że Marysia o niej wie, więc oddała się pod ochronę Ernesta Namertela.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Zaczęstwo
                                1. Akademia Magii, kampus
                                    1. Arena Treningowa: miejsce starcia Karoliny Terienak z Napoleonem Bankierzem. Karolina wygrała, z łatwością.
                                    1. Akademik: Liliana ukryła tam akt Loreny i przybyła tam po niego osobiście Marysia Sowińska.
                                    1. Las Trzęsawny: Torszecki się tam schował z aktem Marysi, ale drona Daniela go znalazła i Torszecki dostał wpierdol.
                            1. Podwiert
                                1. Dzielnica Luksusu Rekinów
                                    1. Serce Luksusu
                                        1. Lecznica Rannej Rybki: skończył tam Torszecki. Znowu.
                                        1. Apartamentowce Elity: miejsce spotkań i randki między Marysią i Ernestem.

## Czas

* Opóźnienie: 2
* Dni: 3

## Konflikty

* 1 - Marysia używa całego wewnętrznego uroku i wszystkich rzeczy których się nauczyła na dworze. Czemu Ernest chce ten akt? GADAJ! I jak go będzie miał, chcę go :3
    * TrZ (relacja + on oficjalnie x Amelia, tak?) +3
    * XVzXXVV: Ernest da Marysi KOPIĘ, to akt Marysi i Ernest się zacznie zastanawiać ;-), Rekiny są za linią Marysia x Torszecki, Ernest x Marysia jako opcja, Ernest powiedział czemu chce akt
* 2 - Karo wyskakuje na Arenę. Chce się zmierzyć z Napoleonem, ale nie zamierza grać czysto.
    * Tr+3
    * VVVVXV: Karo -> lepsza pozycja taktyczna, zwarcie +Vg, Napoleon powiedział o akcie, Karo podnosi reputację Napoleona, Napoleon przerzuca Karo, koniec bitwy - ale Karo wygrywa.
* 3 - Marysia chce się dowiedzieć od Liliany tego, co ta wie i kto jest autorem.  
    * TrZ (słowo Marysi, że nie zrobi nic autorowi ani nikomu powiązanemu do Liliany) +4
    * VXXX: Liliana akceptuje przekazanie aktu, ale go nie ma i nie powie kto go ma. Musi chronić. I ma go już Torszecki
* 4 - Daniel jest odpowiednio zmotywowany. Będzie szukał. I jest wdzięczny siorce. Nawet jak poprosiła nie zbije Torszeckiego - znajdzie go dyskretnie i pokaże jej gdzie on jest.
    * TrZ (Bo zna Torszeckiego. Wie jak on myśli. Pomagał mu. I ma wyczucie. PLUS info od Napoleona o kryjówkach) +4
    * XXVg: Marysia musi kupić czas dla Daniela, on angażuje drony do wsparcia, drona znalazła Torszeckiego i wybuchła. Daniel MUSI atakować. NOKAUT.
* 5 - MARYSIA -> TORSZECKI, wstecznie; kupuje czas Danielowi
    * TrZ (szacunek, zaufanie itp) +3
    * VzVV: RAFAŁ JESTEM W CIĄŻY Z TOBĄ +2Vg, manewr +2Vg, wystarczające opóźnienie.
* 6 - Liliana ani słowa Marysi o autorze. Więc Marysia robi manewr z trackerem w zdjęciu i niech Liliana zdobędzie inny utwór artysty.
    * TrZ (bo Marysia pokazała, że gra fair w tej kwestii) +2
    * XVV: Liliana nie daruje Marysi trackera, puściła zdjęcie i przeoczyła tracker. Artystka też. Więc Marysia wie kto to - Lorena.

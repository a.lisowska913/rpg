---
layout: cybermagic-konspekt
title: "Lewiatan przy Seibert"
threads: legenda-arianny
gm: żółw
players: fox, kapsel, kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [220706 - EtAur - dziwnie ważny węzeł](220706-etaur-dziwnie-wazny-wezel)

### Chronologiczna

* [220706 - EtAur - dziwnie ważny węzeł](220706-etaur-dziwnie-wazny-wezel)

## Plan sesji
### Co się wydarzyło

* .

### Co się stanie

* .

### Sukces graczy

* .

## Sesja właściwa
### Scena Zero - impl

EUSTACHY W BARZE:

* Q: Eustachy, z kim często pijasz?
* A: Raczej w barze, na zasadzie "popijemy drinki, posłuchamy muzykę, popalimy cygaro i small talk. Unwind. Bar na uboczu, broń zakazana, trochę rasistowski, country klimaty. Nie dla oficerów. Karta członkowska - nie ma darcia ryja ani burd. W sposób kulturalny a nie leją się po mordach. Podłe piwo po zawyżonych cenach. Żadnych wiadomości - świat bez problemów. Na terenie klubu stopnie nie mają znaczenia. Gadanie o torpedach anihilacyjnych i podchodzi taki chłopaczek. "Przepraszam, pan z Inferni?" "Konfidentem jesteś?" "Co w tych ścianach zostaje między tymi ścianami." "Najlepszy statek we flocie..."

* Eustachy: "Infernia zrobiła coś przez co flota Termii się uszkodziła"
* Chłopaczek: "Ale mówią że komodor poświęca swoich ludzi w krwawych rytuałach!"
* Eustachy: "Bullshit mówią i tyle. Arianna jest znana z tego że maksymalizuje ratowanie ludzi - jest ostatnią osobą która by poświęciła w krwawym rytuale."

Chłopaczek jest przekonany. Eustachy przekonuje na dni otwarte Inferni.

TrZ+2:

* Vz: Będą dni otwarte na Inferni i będzie kilka osób.
* XX: Diana bardzo rozrabia BO PRÓBUJE BYĆ MIŁA. Ona nie rozumie.
* Vz: Nie tylko dni otwarte - potem część osób stwierdzi że oni CHCĄ na Infernię
    * To... specyficzni ludzie
* V: Infernia stała się... miejscem zrzutu kompetentnych acz problematycznych osób. Diana i reszta ich wychowa.

ARIANNA I OLA SZERSZEŃ:

Ola przekazała Ariannie list od admirał Termii. List "moja mała buntowniczko, mam dla Ciebie mojego małego owada. Zrób z niej słodką pszczółkę lub groźnego szerszenia. Lub zabij. Przypomina mi mnie jakbym była młoda i naiwna. Nie lubi Cię. Nauczy się. A.Termia. PS. Nigdy nie będzie sabotować działań przełożonego, ale nie jest kompatybilna z moją strategią."

Ola się nadaje. Niech będzie.

### Scena Właściwa - impl

Flota:

* Infernia, dowodzi Arianna / Eustachy / Diana. CEL: errr... okręt flagowy? Pozytywna anomalia?
* Tivr, dowodzi Lutus Amerin (noctis). CEL: jednostka super-szybka, manewrowna.
* Wścibski Wiewiór, dowodzi kapitan Ola Szerszeń (ruda). CEL: statek badawczo-skanujący dla Klaudii. Z nadania admirał Termii.
* Straszliwy Pająk, dowodzi <brak danych jeszcze>. CEL: support ship, fabrykator, containment Eleny, containment Nereidy, refueling.
* (Obłok Apokalipsy, dowodzi <brak danych jeszcze>. CEL: jednostka Aurum, nowa, z nowymi osobami. Chcą pomóc Ariannie.)

Czas przeprowadzić ćwiczenia. Grupa Wydzielona Infernia ma za zadanie znaleźć i złapać koloidową korwetę o nazwie "Zając 3" która próbuje dostać do sortowni Seibert.

Klaudia próbuje dostać się do parametrów ćwiczenia.

ExZ (jej znajomość systemów, TAI) +2 +3Og (sukces ALE):

* X: spodziewano się tego i TAK jest info że Klaudia wbijała się do systemów.
* Og: sukces, godziny i plan lotów. Plan lotów jest dość dowolny. Kapitan Zająca jest bardzo dumny ze swojego 'success rate'. (+1Og; sidequest by się popisał). Plus zostawić decoy - "zmieniłam ale nie to". +1Vg
* Vz: macie nadany mniej więcej plan z "nie musisz robić X, ale jak zrobisz X to nikt tego nie dałby zrobić właśnie przeciw Inferni". Będzie leciał MIĘDZY Wiewiórem i Infernią. I wiecie mniej więcej kiedy.

Eustachy proponuje rozwiązanie:

* lekkie pole magnetyczne gdzie jest pył byśmy widzieli że się ruszyło
* magnesy które zwalniamy i "rzucamy" by się przykleiły do koloidowej jednostki
* wszystko tak by nie było widać pułapki

ExZ (plan + arogancja kapitana Zająca) +4 +3Or (rany którejś jednostki):

* Or: plan częściowo udany acz jednostka się wbiła. Magnes nie zadziałał, acz koloidowa jednostka ciężko uszkodzona.

Jednostka została zatrzymana. Na komunikatorze BLUZGI. "CO TO MA BYĆ!" Kapitan Jonasz Parys.

* Arianna: Aż tak bardzo nie możesz znieść porażki? Nie wpychaj palca między drzwi.
* Jonasz: Wszystko co o Was mówią to prawda!
* Eustachy: Dziękujemy!

Infernia holuje "Zająca 3". Dostajecie SOS ze stacji Seibert.

Eszara d'Seibert -> Infernia

* Dwie godziny temu w Anomalii Kolapsu straciłam dostęp do trzech grazerów.
* Zgodnie z procedurą wysłano 2 fregaty i niszczyciel. Wysłały SOS i nie mam sygnału. Niepokojące odczyty i wiadomości. "Duży i piękny".
* Wzywam Infernię do pomocy. By Seibert nie ucierpiał.
* Uważajcie - to anomalia. Najpewniej.

Pająk + Zając -> Seibert. Reszta grupy Infernia -> Anomalia Kolapsu.

Zbliżacie się do AK. Zbliżacie w obszar "nieciągłości sygnału". Detektory COŚ pokazują na Wiewiórze. To jest duże. 1 km średnicy duże. Nieciągłe. Sygnał rozproszony, nie chmura. 

Klaudia używa Wiewióra by zrozumieć naturę przeciwnika. Analiza spektrum itp.

TrZ+3+4Or:

* Or: ANOMALIA MA KLAUDIĘ.
    * paliczki ze złomu kosmicznego i statków
    * energia pomiędzy tworząca... 'unicron'
    * Szerszeń skrzywdziła Klaudię by ta wróciła.
* Or: ZNOWU.
    * załoga jest w środku i jest do uratowania
    * klasyfikacja: Lewiatan.
    * Lewiatan kieruje się na Seibert. Acz Spojrzał na Infernię.
    * Nie jest najszybszy.
    * Klaudia jest wypełniona stymulantami.
    * Oli Szerszeń coś nie działa z implantami; dostała swoją porcję ratując Klaudię.
    * TAI Wiewióra dostała w kość; jest częściowo mało sprawna.

Przewód. "Kontrolowane zwarcie". Podpiąć się jakąś jednostką (pintką lub czymś szybkim) do Lewiatana by dostać próbkę energii.

Mamy dane ile chcemy energii. Odpowiednio cienki kabel który wystrzelimy w Lewiatana, to idzie do "kondensatora", pustej baterii vitium. I przez to kabel jest cienki powinno się udać.

Eustachy: 

TrZ+3:

* XX: będzie trzeba containować baterię vitium; to cholerstwo się zdestabilizuje.
* V: Podczas przelotu uda się złapać dość energii do baterii by Klaudia miała co badać.
* Or: Tivr jest USZKODZONY podczas przelotu ("macka"). Potrzaskany bo Lewiatan go Dotknął energią i w coś walnął sterując bez TAI; po chwili TAI wróciła do działania.
* V: Tivr wykonał zadanie.

Czyli Lewiatan to bazyliszek. Jest stosunkowo powolny, ale jak na niego patrzysz to nie uciekniesz bo Cię wciąga. Klaudia diagnozuje TAI i próbuje zbudować anty-bazyliszka, filtr komputerowy do sensorów. By nie widzieć bazyliszków.

TrZ+3+3Or:

* Vz: na podstawie energii i sygnatury udało się zaszumić Lewiatana.
* V: system zaprojektowany przez Klaudię jest wormem; może zainfekować inne jednostki i je wyczyścić.

Eustachy riguje serię pocisków; chce ostrzelać "swoje" jednostki w Lewiatanie. Niech jest możliwość odblokowania ludzi i AI.

TrZ+3+3Or:

* V: lot koszący udany, ostrzelane pozytywne jednostki
* Vz: Inferni udało się wydostać spoza "macek" Lewiatana bez szkód
* X: To nie tak że nie ma ŻADNYCH szkód; Infernia dostała trochę, ale poszło po pancerzu.
* X: Ten przelot uniemożliwił dodatkową insercję. Lewiatan "przyspieszył ruchy". Nadal jest wolny, ale im bliżej rdzenia tym gorzej. Nadal nie widzi małych bytów.

Arianna opanowuje baterię vitium. Oba aspekty magii Arianny - technomancja + ożywanie. Plus chce zamknąć te baterię w "dłoniach" z energii elektrycznej. Kable. By przebiegały. By miał kształt jak lewiatan.

TrZ (vitium) M +3:

* Vz: wykorzystując vitium energia jest ustabilizowana i tu JEST.
* V: forma "małego lewiatana", łatwy do analizy przez Klaudię.
* V: "mały lewiatan" jest tym co przyciąga Lewiatana. Czyli jeśli "mały lewiatan" jest ODPOWIEDNIO BLISKO Lewiatana to dlań jest pierwszym celem.

Lewiatan obrócił się w stronę Tivru i leci dostojnie w kierunku Tivru...

## Streszczenie

W okolicach Anomalii Kolapsu obudził się Lewiatan. Ruszył na sortownię Seibert, pożerając kilka jednostek. Grupa Infernia, która miała ćwiczenia (i pokonała koloidową korwetę Zająca 3) dała radę zlokalizować Lewiatana, zabezpieczyć się przed aspektem bazyliszka, zdobyć trochę jego energii do badań i przygotować się do neutralizacji. Jakoś.

## Progresja

* Jonasz Parys: wściekły na Infernię - zwłaszcza Klaudię (gra nie fair) i Ariannę (psychiczna), bo przegrał. Stracił reputację nieuchwytnego i uszkodził swoją korwetę koloidową.
* OO Tivr: lekko uszkodzony, trafiony odłamkami złomu kosmicznego tworzącego Lewiatana.
* OO Infernia: lekko uszkodzona, trafiona przez Lewiatana podczas lotu koszącego.
* OO Wścibski Wiewiór: lekko uszkodzony psychotronicznie przez patrzenie w serce Lewiatana. TAI się zregeneruje sama w tydzień.

### Frakcji

* .

## Zasługi

* Arianna Verlen: skutecznie opanowała przechwyconą energię Lewiatana w baterii vitium i sformowała zeń przynętę na Lewiatana. Sposób, by Lewiatana gdzieś przeprowadzić.
* Eustachy Korkoran: organizuje Dni Otwarte na Inferni, znajduje sposób jak przechwycić energię z Lewiatana używając kontrolowanego zwarcia i zlokalizował koloidową korwetę pyłem magnetycznym i zderzaniem się Tivrem.
* Klaudia Stryk: próbując zrozumieć Lewiatana wpadła w bazyliszka, ale potem zbudowała worma anty-bazyliszkowego i zainfekowała nim wszystkie maszyny. Też - zmieniła parametry ćwiczeń by kapitan Zająca 3 przegrał z Infernią.
* Ola Szerszeń: Arianna dostała ją w prezencie od Termii, wraz ze Wścibskim Wiewiórem. W niełaskach u Termii, nazwana 'jak ja ale naiwna', kiedyś twarz kampanii anty-augmentacji. Brutalnie postawiła Klaudię gdy tą złapał bazyliszek Lewiatana. Kompetentna kapitan która chce ratować wszystkich.
* Jonasz Parys: kapitan 'Zająca 3', szybkiej korwety koloidowej, który nie umie przegrywać i zbluzgał Ariannę gdy Infernia go wyrolowała i został złapany. Pod Kramerem. Kolesia nikt nie złapał, ale Klaudia wciągnęła go w pułapkę jego własną arogancją... i przegrał.
* Eszara d'Seibert: przekroczyła uprawnienia i skomunikowała się z Infernią wzywając SOS z uwagi na obecność anomalii - lewiatana (o czym jeszcze nie wiedziała).

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Obłok Lirański
                1. Anomalia Kolapsu, orbita
                    1. Sortownia Seibert: grazery zbierają jednostki i rzeczy z Anomalii Kolapsu i tu są przekształcane w coś przydatnego (statki z odzysku, części itp). Stocznia dekomponująca. Ma flotę defensywną - 2 niszczyciele i 5 fregat, pola minowe, działka. I Eszarę.
            1. Astoria, Orbita
                1. Kontroler Pierwszy
                    1. Sektor 25: charakteryzuje się swobodniejszym podejściem; mniej zadbany, 'bezpieczne miejsce spokoju' dla agentów Orbitera
                        1. Bar Solatium: specyficzny bar typu: popijemy drinki, posłuchamy muzykę, popalimy cygaro i small talk; trochę na uboczu, broń zakazana, trochę rasistowski, country klimaty, karta członkowska. Nie ma darcia ryja ani burd. Świat bez problemów, stopnie bez znaczenia. Tam Eustachy pozyskał grupę chętnych świrów na Infernię.


## Czas

* Opóźnienie: 6
* Dni: 2

## Konflikty

* 1 - Chłopaczek jest przekonany. Eustachy przekonuje na dni otwarte Inferni.
    * TrZ+2
    * VzXXVzV: Infernia to dumping ground kompetentnych acz problematycznych ludzi. Freaki będą przychodzić. Dni otwarte Inferni by Eustachy - AKTIV.
* 2 - Grupa Wydzielona Infernia ma za zadanie znaleźć i złapać koloidową korwetę o nazwie "Zając 3". Klaudia hackuje system ćwiczeń.
    * ExZ (jej znajomość systemów, TAI) +2 +3Og (sukces ALE)
    * XOgVz: jest info że Klaudia się wbija, wściekłość kapitana Parysa, jest plan który skorzystał ze słabości Parysa.
* 3 - Eustachy proponuje rozwiązanie z polem magnetycznym i magnesami by złapać koloidowego Zająca.
    * ExZ (plan + arogancja kapitana Zająca) +4 +3Or (rany którejś jednostki)
    * Or: plan częściowo udany acz jednostka się wbiła. Magnes nie zadziałał, acz koloidowa jednostka ciężko uszkodzona.
* 4 - Klaudia używa Wiewióra by zrozumieć naturę przeciwnika (Lewiatana). Analiza spektrum itp.
    * TrZ+3+4Or
    * OrOr: Anomalia łapie Klaudię; Szerszeń ją wyciąga bólem. Klaudia widzi Lewiatana i część jego podstawowych cech. TAI Wiewióra też dostała. Szerszeń też.
* 5 - Eustachy ma plan: mamy dane ile chcemy energii. Odpowiednio cienki kabel który wystrzelimy w Lewiatana, to idzie do "kondensatora", pustej baterii vitium.
    * TrZ+3
    * XXVOrV: containować baterię vitium (destabilizacja), ale jest dość energii. Tivr lekko uszkodzony, trafiony przez Lewiatana odłamkiem.
* 6 - Czyli Lewiatan to bazyliszek. Klaudia diagnozuje TAI i próbuje zbudować anty-bazyliszka, filtr komputerowy do sensorów.
    * TrZ+3+3Or
    * VzV: na podstawie energii i sygnatury da się zaszumić Lewiatana i system zaprojektowany przez Klaudię jest zaraźliwym wormem; da się zimmunizować inne.
* 7 - Eustachy riguje serię pocisków; chce ostrzelać "swoje" jednostki w Lewiatanie. Niech jest możliwość odblokowania ludzi i AI.
    * TrZ+3+3Or
    * VVzXX: lot koszący udany, Infernia się wydostała. Ale jest uszkodzona (po pancerzu).
* 8 - Arianna opanowuje baterię vitium. Oba aspekty magii Arianny - technomancja + ożywanie. Plus chce zamknąć te baterię w "dłoniach" z energii elektrycznej. By miał kształt jak lewiatan.
    * TrZ (vitium) M +3
    * VzVV: ustabilizowana, "mały lewiatan", perfekcyjny do badań ORAZ jest przynętą na Lewiatana.
* 9 - 
    * 
    * 
* 10 -
    * 
    * 

---
layout: cybermagic-konspekt
title: "Dziewczyna i pies"
threads: dziewczyna-i-pies
gm: żółw
players: kić, darken, vizz
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [201201 - Impreza w Małopsie](201201-impreza-w-malopsie)
* [190702 - Śmieciornica w magitrowni](190702-smieciornica-w-magitrowni)

### Chronologiczna

* [201201 - Impreza w Małopsie](201201-impreza-w-malopsie)

## Punkt zerowy

### Dark Past

.

### Opis sytuacji

* Grzymościowcy wchodzą głębiej w miasteczko; Zaramarintor ukrywa Patrycję używając Pauliny i Andrzeja.
* Paulina i Andrzej są zasymilowaną "rodziną" Patrycji z woli Lucka i z zezwolenia Zaramarintora.
* Izydor się zmartwił. Paulina WIE i pamięta jak mieli romans. Musi jej wyczyścić pamięć.
* Na miejscu, niedaleko, jest też Patrycja z Luckiem. Patrycja zeżarła Izydora. Trzyma go "w piwnicy".

### Postacie

* Darken: Daniel Terienak
    * dziąsłowiec i biedaartefaktor
    * kolekcjoner artefaktów, też mniej legalnych. Terminusi zajumali i czuje krzywdę.
    * agresywny, improwizujący, zero cierpliwości
* Kić: Karolina Terienak
    * pieści swój ścigacz nawet jak patrzą
    * świetna w walce kontaktowej ultrakrótkiej, jak bulterier
    * świetny mechanik i pilot
* Vizz: Grzegorz Terienak
    * wlazł kiedyś gdzieś gdzie nie powinien i zobaczył coś czego nie powinien
    * prawnik, dobry w takich negocjacjach z dokumentami
* Zaramarintor, drakolita w formie smoka
    * CHCE: zachować ciszę i niewiedzę nie-drakolitów w tym miasteczku, ukryć drakolitów.
    * MA: policjanta Franciszka, kontrolę nad memetyką tego miejsca. Ma też ciche przyzwolenie Pustogoru i podziemne miasto.
    * OFERUJE: potęgę i xenoformację. Plus, poświęci Patrycję i Paulinę dla ochrony terenu. Jak trzeba, zabije.
* Krzysztof Talarski, kustosz muzeum anomalii jastrzębskich
    * CHCE: N/A
    * MA: N/A
    * OFERUJE: N/A
* Izydor Grumczewicz, Rekin
    * CHCE: by Paulina zapomniała o wszystkim, wyczyścić jej umysł
    * MA: przyjaciół i żonę, którą zdradza
    * OFERUJE: nic. Leży zamknięty w piwnicy
* Franciszek Zygmunt, policjant w Małopsie
    * CHCE: by jego żona była zdrowa i bezpieczna; udało się to osiągnąć dzięki xenoformacji
    * MA: władzę, wpływy lokalne + niewielki wpływ na Pustogor
    * OFERUJE: 
* Patrycja, umierająca szesnastolatka ulegająca przekształceniu
    * CHCE: swojej rodziny, nigdy nie przestać marzyć, chronić tych których kocha, żywić się
    * MA: upiornego inkarnowanego psa Lucka, wspomaganie 
    * OFERUJE: ukojenie i piękne życie

## Punkt zero

* .

## Misja właściwa

Izydor zniknął. Był w tej okolicy. Wysłał audio "SOS", gdzie dało się usłyszeć, że spotkał się ze swoją byłą dziewczyną (Pauliną). I teraz - trzeba go najpewniej uratować, napotkał na coś groźnego.

Karolina prowadzi trzy ścigacze Rekinów z magami swego rodu. Jest mgła, coś na kształt potężnej burzy ze mgłą (?!) - a we mgle coś jest. Gorathaul? (Ex VVX): udało się Karolinie slavować pozostałe ścigacze do siebie i elegancko wylądowała, odegnała gorathaula. (XX): nudności u innych członków jej rodziny. Tylko Karo może poradzić sobie w manewrowaniu we mgle vs gorathaul. Wylądowała niedaleko mieszkania Pauliny i Andrzeja - ale ich tam nie ma.

Magiczny forensic, co tu się stało; z tym poradził sobie świetnie Grzegorz. Mają wizję - "byli tam". Jakaś dziewczyna. Był tam Izydor. Izydor zwracał się do Pauliny i Andrzeja, próbował ich przekonać by żyli swoim życiem i chciał wymazać pamięć, ale został zaatakowany od pleców przez... psa? I ktoś odezwał się do Pauliny jako "mamo"? To nie jest dość możliwe, ale...

Tak czy inaczej, potrzeba więcej śladów. Daniel wie, że był Tadeusz, koleś, który wszystko dla nich zapisywał. Wie też, że Andrzej pracował jako trener personalny na stadionie i dorabiał sobie jako ochroniarz. Dobry moment (noc), by go odwiedzić. Pojechali więc tam, unikając gorathaula.

Na miejscu spotkali Andrzeja i od razu Daniel zaczął od zdrowego wpierdolu. Andrzej się przyznał - był u nich Izydor, przenieśli się do nowego lepszego mieszkania z ich córką (???). Andrzej ma dysfunkcyjną pamięć, nie do końca wie co jest prawdą - ma córkę, ale nie może mieć córki. Daniela to trochę bawi, acz Grzesiek zastanawia się co tu się zrobiło.

Wzięli więc ze sobą jeszcze kapusia Tadeusza i pojechali do nowego domu Andrzeja i Pauliny. By złamać dziwną mgłę mentalną w głowie Andrzeja, Grzegorz zaczął przesłuchanie w warunkach ekstremalnych - Daniel torturuje, Grzegorz przesłuchuje. Jadą na ścigaczu, Andrzej tam zwisa z głową przy ziemi, pełen strach i posikanie się, Grzegorz pyta.

* ZA NOGĘ, ZE ŚCIGACZA. Trauma.
* mgła pojawiła się po tym jak Izydor przybył do Małopsa
* Patrycja (córka) dostała 3 psy z niedalekiej hodowli. Co nie jest możliwe, bo Patrycja ma przecież jednego psa. Ale tak się stało (cross między danymi Tadeusza i Andrzeja)
* Patrycja jest w Małopsie od 2-3 miesięcy. Nie, jest od zawsze, jest córką. Nie, jest od 2-3 miesięcy.
* Patrycja **nie może** być córką Andrzeja i Pauliny, choć mentalny blok sprawia, że Andrzej i Paulina w to wierzą
* Stan Patrycji jest zawsze ten sam. Nastolatka, raczej na uboczu, towarzyszy jej pies Lucek.

Przybyli do domu Andrzeja i Pauliny. Jest to rudera, acz... zadbana? Znowu, dysfunkcja rzeczywistości.

Daniel zdecydował się zbudować odporność Tadeuszowi i Andrzejowi - niech zobaczą jak rzeczywistość wygląda naprawdę.

* V: Nadał odporność na pole mentalne
* XXX: Manifestacja. Patrycja i Lucek wiedzą, że magowie tu są i próbują zniszczyć iluzję. Są gotowi i mają przewagę.
* OO: Esuriit, wzmocnienie Lucka - Tadeusz i Andrzej krwią karmią energię Lucka
* V: Daniel przerwał link krwi który założył by zrobić odporność na pole mentalne

Z domu wychodzi dziewczyna z psem. Patrycja - "zostawcie mojego tatę". Pies jest bardzo, bardzo groźny. Chce atakować, ale Patrycja mu nie pozwala. Daniel ją zastrasza, a wygląda na osiłka. Patrycja przestraszona ucieka zadzwonić na policję. Wszyscy mają wtf.

Pierwszy pomysł Daniela - cegła z bombą. Drugi - pogadać z Pauliną i Andrzejem. Nie po to, by ich wyrwać z chorego świata Patrycji (która jest generatorem tego świata) a by zrobić antypsią frakcję. Niech się pozbędą Lucka. Zespół zauważył, że jeśli nie ma Lucka - Patrycja jest zwykłą dziewczyną (lub jest martwa). 

* VVV: Andrzej widzi PRAWDĘ. On stoi po stronie Zespołu.
* OO: Patrycja robi histeryczną scenę. To jej ukochany pies. Andrzej i Paulina to jej kochani rodzice!

Zespół (Daniel i Grzegorz) przerywają linka między Patrycją i "rodzicami":

* XXX: Patrycja ma headstart w ucieczce. 
* V: Link został rozerwany. Paulina i Andrzej wiedzą już co jest prawdą a co fikcją. Wiedzą, że Patrycja nie jest ich córką a kimś obcym, z zewnątrz

I na to wpada policjant Franciszek Zygmunt. Chce dać "mafii" mandat za przekroczenie prędkości. Jest wyjątkowo wkurzający - i kupuje czas Patrycji na ucieczkę! Grzegorz zauważa (VVX), że Franciszek coś wie o Patrycji, od może znać prawdę.

KAROLINA PORYWA FRANCISZKA NA ŚCIGACZ.

Gdy Karo wzbija się w powietrze i napotyka znowu gorathaula, Franciszek informuje Strażnika (gorathaula), że jest OK. Żółwiak Mgieł nie atakuje ścigacza Karo. W tym czasie Daniel szuka Patrycji (VVVV) i ją znajduje - na mokradłach, ale znalazł też bezpieczny teren na lądowanie.

Gdy Zespół wylądował niedaleko Patrycji, ta wykopywała Izydora. Nie chce go. Nie chce robić mu krzywdy. Ona chce z Luckiem uciec. Ona po prostu chciała mieć normalne życie, normalną rodzinę. Nie może? Nie ma do tego prawa? Zespół nie chce walczyć na mokradłach ze Skażoną przez Esuriit dziewczyną i jej psem, który jest manifestacją Esuriit. Lepiej odzyskać Izydora i niech Patrycja ucieka...

Przesłuchany przez Zespół, Franciszek powiedział, że na tym terenie jest Strażnik (gorathaul). Oni, mafia, nie mają pojęcia z czym mają do czynienia. Jest to teren bez terminusów, autonomiczny i taki pozostanie. Paulina zwróciła się do obcych - do Izydora i Daniela. Dlatego musiała zapłacić. A Patrycji "oni" są w stanie pomóc. Ale Franciszek nie powie kim "oni" są. Zespół na razie to zostawi "as is"...

Karolina jednak "uprzejmie doniosła" anonimowo o niebezpiecznym bycie opartym o Esuriit, aka: Patrycji.

## Streszczenie

Patrycja, coraz bardziej opętana przez Lucka/Esuriit pragnie odzyskać rodzinę. Znalazła "rodzinę" w Małopsie, gdzie "przygarnęła" Paulinę i Andrzeja, za cichym zezwoleniem tajemniczych władców Małopsa (którzy chcieli ją naprawić). Tymczasem trzy Rekiny rodu Terieniak poleciały do Małopsa odzyskać swojego kolegę, Izydora, i skończyło się na tym, że wyciągnęły "rodziców" Patrycji z jej mgły mentalnej i zmusiły Patrycję do ucieczki na mokradła.

## Progresja

* .

### Frakcji

* .

## Zasługi

* Karolina Terienak: przemanewrowała dookoła gorathaula we mgle i porwała sobie Zygmunta (policjanta) by móc go przesłuchać w dogodnym momencie. Robi co chcą bracia, skupia się na ścigaczu.
* Daniel Terienak: nie waha się użyć tortur by osiągnąć cel; wyrwał Paulinę i Andrzeja z mgły mentalnej wygenerowanej przez Paulinę. Zero sympatii i współczucia wobec ludzi.
* Grzegorz Terienak: najbardziej empatyczny i delikatny z rodziny; prawnik przesłuchujący Andrzeja - jaka jest prawda o Patrycji?
* Izydor Grumczewicz: chciał usunąć swojej dawnej miłości (Paulinie) pamięć o sobie i został złapany przez Patrycję i Lucka (paradoksalnie, chcących chronić Paulinę i Andrzeja).
* Paulina Mordoch: myśli, że jest matką Patrycji. Sprawdzała się w tej roli ;-).
* Andrzej Kuncerzyk: myśli, że jest ojcem Patrycji. Torturowany przez Daniela, zorientował się w prawdzie i stanął po stronie Daniela przeciw Patrycji.
* Tadeusz Łaśnic: podliz maksymalny, donoszący o wszystkim co się dzieje w miasteczku Danielowi. O dziwo, doskonale prowadził notatki i pokazał dysfunkcję rzeczywistości generowaną przez Patrycję.
* Franciszek Zygmunt: policjant Małopsa współpracuje z władzą Małopsa. BW. Nie skupia się na prawie a na "tajemniczym prawie okolicy". Pewny siebie, niewiele się boi. Stary, ale jary.
* Patrycja Radniak: chciała mieć prawdziwą rodzinę, więc jej pies jej to zapewnił. Spotkanie z trzema Rekinami sprawiło, że uciekła bezpiecznie. Była przez moment szczęśliwa, acz ich zabijała (czego nie czuje)

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Powiat Jastrzębski
                        1. Małopies
                        1. Małopies, okolice
                            1. Mokradła: miejsce, gdzie Patrycja była po raz ostatnio widziana i gdzie ukryła Izydora przed wszystkimi.

## Czas

* Opóźnienie: 9
* Dni: 2

## Inne

.

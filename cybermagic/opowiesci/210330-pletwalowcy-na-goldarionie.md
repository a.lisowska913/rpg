---
layout: cybermagic-konspekt
title: "Płetwalowcy na Goldarionie"
threads: unknown
gm: żółw
players: kić, darken
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [210810 - Porwanie na Gwiezdnym Motylu](210810-porwanie-na-gwiezdnym-motylu)

### Chronologiczna

* [210810 - Porwanie na Gwiezdnym Motylu](210810-porwanie-na-gwiezdnym-motylu)

## Punkt zerowy

### Opis sytuacji

* Goldarion: 
    * Aleksander Leszert jest kapitanem Goldariona.
        * próbuje trzymać dyscyplinę
    * Adam Permin jest niechlujnym szefem ochrony Goldariona; jest neurosprzężony.
    * ma na pokładzie około 50-60 osób
        * ekipa jest kiepska. Nie są najbardziej kompetentni i nie mają etyki pracy.
        * ważna osoba: Janusz Krumlod (cargo)
    * to niemały wysoce zautomatyzowany transportowiec klasy Prosperion.
    * Ma też na pokładzie dwa Eidolony, jeden z nich kontrolowany przez Adama Permina
    * Goldarion jest w kiepskim stanie, potrzebuje kasy.
* Martyna Bianistek
    * Jest kapitanem Płetwala Błękitnego, statku, który chwilowo jest unieruchomiony
    * Ma pieniądze na wynajęcie Goldariona
    * Ma 15 kompetentnych osób w załodze
    * Dwie szczególnie dobre osoby: Karolina Kartusz (logistyka / cargo), Jonatan Piegacz (pierwszy oficer)
* Sytuacja
    * Aleksander i Adam mają romans, dlatego trzymają się razem tego statku
    * Martyna ma 20 osób aresztowanych na stacji Dorszant, po drugiej stronie Bramy Eterycznej
        * chce wykupić członków załogi / dowiedzieć się co tu się odpiernicza
        * ma mapę eteryczną prowadzącą z Astorii na Dorszant
    * Martyna potrzebuje Goldariona by przelecieć przez Bramę. Leszert potrzebuje jej pieniędzy.

## Misja właściwa

Janusz usłyszał, że Aleksander zaakceptował kontrakt tego typu i że będzie cargo. Poważnie zaprotestował - nie będzie mu jakaś młodziutka laska (Karolina) robiła w JEGO ładowni. On ma być tym co rządzi. Aleksander postawił sprawę jasno - to on jest kapitanem Goldariona, Janusz se może. Janusz na ostro - trzeba wywalić Karolinę. Aleksander na ostro - on dowodzi. Janusz lubi pieniądze? Lubi. Więc ma słuchać.

Tr+2:

* V: Załoga jest nieszczęśliwa, ale trudno. Aleksandra będą słuchać.
* V: Wroga neutralność między ludźmi Goldariona i Płetwala, ale neutralność. Nie będzie walki i wojny.

Jednocześnie Karolina poszła do Martyny. "Janusz to seksistowski debil", oferował jej pójście do łóżka i pomasowanie... nieważne. Ona chce się go pozbyć. Dodatkowo, jest IDIOTĄ, źle rozkłada cargo i nie chce jej słuchać bo jest młodsza. A jest przecież dużo bardziej doświadczona i ekspercka niż on, wie sporo o jednostce klasy Prosperion itp.

Martyna ją uspokaja. Karolina **będzie** współpracować z Januszem. Albo to, albo będzie na pokład Płetwala ściągnięty inny uczeń który będzie się słuchał Martyny. Sytuacja jest jaka jest i nic na to nie da się poradzić.

Tr+3: 

* V: Karolina będzie grzeczna, ale chce pracować solo; nie będzie atakować Janusza.
* X: Martyna MUSI ściągnąć ucznia. Karolina się przestraszyła; Płetwal to jej pierwszy okręt.
* X: Martyna wyjaśniła Karolinie, że ta jest suboptymalna - Goldarion to patchwork a nie statek klasy Prosperion. Już nie.
* V: Karolina jest upokorzona, ale będzie współpracować z Januszem.

Gdy Martyna spotkała się przedyskutować warunki współpracy, Aleksander wyjaśnił Martynie, że na Goldarionie jest najtańsza załoga jaka jest możliwe. Ale - jak tu jesteś, nie jesteś CAŁKIEM do dupy. Ci ludzie może są brudni, brzydcy i niezbyt może kompetentni, ale są tu i jest tu pewna duma i u niego i u nich. To jest powodem tego, że Goldarion ogólnie da sobie radę i że Martyna może na nich jakoś liczyć.

Ok, ale Martyna przeniosła temat na pieniądze. Jest zakupione cargo które na pewno przyda się na Dorszancie. I jeśli Janusz je rozłoży po swojemu, będzie z tego 1000 kredytek. Jeśli rozłoży je Karolina, 1500. I Martyna proponuje, by Karolina to zrobiła przy współpracy Janusza. Przecież jeśli Janusz rozkłada, Martyna zarabia 500 a Aleksander też 500 - a Martyna oferuje ekspertyzę Karoliny bardzo tanio. Dzięki Karolinie Aleksander zarobi 750 i Martyna też 750.

Aleksander odbił - 775 vs 725. Martyna aż się zatchnęła. Aleksander powiedział, że "honor moich ludzi kosztuje". To, że Janusz to świnia nic nie zmienia - on (Aleksander) musi wyglądać na zwycięzcę negocjacji. Dlatego albo Martyna na to pójdzie albo będzie musiał oddać zwycięstwo Januszowi. Martyna nie jest szczęśliwa, ale się zgadza - nie ma wyjścia, chce jak najszybciej mieć Goldariona z głowy...

Dobra, Karolina poszła rozkładać cargo z "pomocą" Janusza. Janusz ma to gdzieś, ogląda sobie filmiki z nagimi kobietami i rechocze (wybrał takie podobne do Karoliny wizualnie). Karolina chciała współpracować, ale nie takim kosztem i nie w ten sposób. Rozkłada prawidłowo - ale nie wie o patchworku. Janusz nie zamierza jej nic mówić. Aleksander się domyśla jak wygląda sytuacja, ale zdecydował się nie ingerować...

Karolina próbuje zmaksymalizować cargo i korzyści nie wiedząc o patchworkach i ryzykach na Goldarionie:

Ex+3:

* XXX: Karolina straciła część ładunku (nie 1500 a 1100 zysków), ubrudziła się jak świnia i biegała w panice jak kura gdy rzeczy zaczęły się pieprzyć. A Janusz to nagrywał.
* V: Karolina uratowała 1100 zysków - więcej niż gdyby Janusz rozkładał mimo wszystko.

Aleksander z radością wysłał Martynie raport o tym jak Karolina, "mistrzyni cargo", spieprzyła sprawę. 

Janusz swoje nagrania rozpuścił wśród załogi dla poprawienia morale, swojej pozycji i ogólnej zabawy. Aleksander nie oponował (TrZ+2).

* X: Karolina widziała tą kasetę i zamknęła się w kajucie. Nie wychodzi.
* V: Morale i uwielbienie załogi Goldariona wobec Aleksandra. "Hehe pokazaliśmy im".
* X: Martyna MUSI coś z tym zrobić.
* V: Stały bonus załogi Goldariona póki na pokładzie są Płetwalowcy

Parę dni później Jonatan Piegacz przybył do kajuty Martyny z raportem odnośnie aktualnej sytuacji: śmiechy, żarty, bójki. Ogólnie, załoga Płetwala ma bardzo ciężko na Goldarionie i przygotowują się do walki.

Martyna natychmiast zażądała od Leszerta rozmowy. Nie może tak być. Muszą coś z tym zrobić. Tymczasem parę dni temu Adam nagadał solidnie Aleksandrowi; może i Płetwalowcy nie są najfajniejsi, ale tu jest przegięta pała; Aleksander musi coś z tym zrobić. Więc Aleksander Leszert też był receptywny co do rozmowy z Martyną.

Martyna zaproponowała współpracę. Aleksander się zgodził - niech będą zespoły robocze, mieszane, mające poprawić osiągi Goldariona. I niech Janusz współpracuje z Karoliną. Martyna miała na początku poważne obawy co do tego wszystkiego, ale Aleksander powiedział, że to najlepsza opcja. No ok... w końcu on zna swoją załogę najlepiej a ona nie ma najlepszej sytuacji. Martyna nie do końca ma możliwość przejęcia Goldariona zbrojnie i autentycznie martwi się o zdrowie i życie swoje i swojej załogi na tym etapie...

Martyna wygłosiła orędzie do załogi. Rozleniwili się - pojechała po ambicji. Jest szansa na polepszenie warunków, trzeba pokazać co da się zrobić z Goldarionem. I dla osłody - UDZIAŁY w zyskach.

TrZ+3:

* V: załoga na to pójdzie.
* XX: będą bójki, będą solidne walki między Płetwalowcami i Goldarionowcami
* V: Karolina też na to pójdzie, wyjdzie z kajuty (pod opieką Jonatana) i spróbuje pomóc mimo żałości i wstydu
* X: Jonatan niestety dostanie BZZZT w kajucie; Goldarion jest w złym stanie. Medical.
* V: FERWOR. Teraz to mega zależy Płetwalowcom by coś zrobić - nie chcą podzielić losu Jonatana...

Aleksander też wygłosił orędzie - spotkanie udziałowców. Nie mówi do nich jako kapitan a jako rekin czujący krew. Płetwalowcy? Oni są na tych dobrych statkach. Goldarion taki nie jest. Ale mogą się nauczyć od Płetwalowców. Skroją ich do zera - na wiedzy itp. Goldarion może mieć ciepłą wodę w kranie i bieżącą w kiblu. Trzeba tylko ukraść wiedzę. "Janusz, utrzyj nosa. Pokaż swoją epickość i okradnij tą Karolinę z wiedzy".

TrZ+4:

* V: załoga będzie współpracować
* V: widzą wartość Płetwalowców, więc pomagają
* X: brutalne bójki, okazje, zakłady
* X: ktoś sabotował Jonatana; to BZZT w kabinie to było celowe. "Nie lubimy tu boy scoutów".
* O: służalczość i kradzież wiedzy - dochodzi do transferu kosztem potencjalnych relacji
* V: "wspólne przeżycia" tworzą dziwne przyjaźnie między niektórymi członkami załogi
* X: Jonatan i kilka osób z Goldariona wiedzą o tym że to był sabotaż
* X: twarde jądro oporu na Goldarionie - "jak było w dupie było lepiej a nie tak jak teraz"
* O: dziwne przyjaźnie, kontakty...

A jak poszła załogom naprawa Goldariona? Tydzień solidnej pracy.

ExZ+3:

* O: tymczasowe uszkodzenia i podniesiony koszt materiałów
* X: ranni podczas pracy nad systemami, z obu załóg
* X: utrata części zapasowych. Plus 3 dni opóźnienia by coś zdobyć z wraków.
* O: wygląda wizualnie lepiej...
* V: Sukces. Goldarion ma lepszy QoL dla załogi i pasażerów (woda, kajuty...)
* V: Goldarion jest strukturalnie solidniejszy (miejsce, cargo, manewry, nie odpada od niego tyle rzeczy)

I jaki jest ostateczny wynik komitywy załogi Goldariona i Płetwala?

[8X | 2V+3V+5V+2V]

* X: Jednostronne bójki, kocenie, ogólnie będą się prać i nie będą się lubić - ale umieją współpracować.
* X: Pojawiły się prawdziwe nienawiście pomiędzy członkami załóg.
* V: Zdolni do współpracy jak załoga (póki nikt nie jest sam w ciemnym zaułku)
* V: Karolina i Janusz się jakoś zakumplowali.
* V: Dzięki kapitanom jest transfer wiedzy. Obie strony się czegoś nauczyły - Goldarionowcy o statkach, Płetwalowcy o brudnej grze.

W końcu koszmar się kończy. Zbliżają się do bramy eterycznej prowadzącej do Dorszant...

## Streszczenie

Część załogi Płetwala Błękitnego została aresztowana na stacji Dorszant i Martyna Bianistek musiała kupić usługi Goldariona. Dwie załogi (Płetwal, Goldarion) bardzo źle współpracowały ze sobą; doszło do wojenek wewnętrznych, które prowadziły też do redukcji zysków. Leszert próbował wyrolować Bianistek, ta się trochę nie dawała. Nieco "na siłę", ale częściowo załogi zaczęły się dogadywać i ruszyły na Dorszant uratować członków Płetwala i sobie dorobić...

## Progresja

* Aleksander Leszert: w relacji z Adamem Perminem (swoim oficerem bezpieczeństwa)
* Adam Permin: w relacji z Aleksandrem Leszertem (swoim kapitanem)
* Jonatan Piegacz: porażony prądem w sabotażu elementu przestępczego na Goldarionie. 2 tygodnie regeneracji (bo medical na Goldarionie jest jaki jest...).

### Frakcji

* .

## Zasługi

* Aleksander Leszert: kapitan Goldariona; skupiony na swojej załodze i swoim statku a nie na ogólnych korzyściach dla wszystkich. Dość niski z charakteru. Ale pod wpływem Adama zintegrował te załogi.
* Martyna Bianistek: kapitan Płetwala Błękitnego; by ratować załogę zapłaciła Goldarionowi za transport, czego żałuje. Gdy nie była w stanie ochronić swej załogi przed Goldarionem, poszła na współpracę by poszło dobrze.
* Adam Permin: oficer bezpieczeństwa Goldariona; opieprzył poważnie Aleksandra ostrzegając go, że budujący się resentyment Płetwal - Goldarion zagraża Goldarionowi i reputacji Leszerta.
* Janusz Krumlod: mistrz cargo Goldariona; brudna seksistowska świnia który wie o każdym patchworku na Goldarionie. Mógł więcej zarobić, ale wolał zgnębić arogancką Karolinę. Dzięki machinacjom Aleksandra i Martyny - pogodzili się.
* Karolina Kartusz: świetna, młoda mistrzyni cargo i logistyki Płetwala. Arogancka, "wie lepiej". Zdeptana przez Janusza z Goldariona, zgnębiona, odzyskała z trudem swoje morale i nawet się zaprzyjaźniła z Januszem.
* Jonatan Piegacz: pierwszy oficer Płetwala Błękitnego; dobry duch i "boy scout" Płetwala Błękitnego. Przodownik pracy, wspiera Martynę w każdej sytuacji w jakiej jest w stanie, nieważne co o tym myśli.
* SC Goldarion: kiepskojakościowy patchworkowy średni transportowiec na 60 osób załogi klasy Prosperion. Ostry refit załogi Płetwala i Goldariona, co się skończyło opóźnieniami, awariami - ale jest przynajmniej bieżąca woda...

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Noviter

## Czas

* Opóźnienie: 71
* Dni: 15

## Konflikty

* 1 - Aleksander: Janusz na ostro - trzeba wywalić Karolinę. Aleksander na ostro - on dowodzi. Janusz lubi pieniądze? Lubi. Więc ma słuchać.
    * Tr+2
    * VV: załoga nieszczęśliwa, ale będzie słuchać; wroga neutralność Goldarionowcy - Płetwalowcy.
* 2 - Martyna: Karolina **będzie** współpracować z Januszem. Albo to, albo będzie na pokład Płetwala ściągnięty inny uczeń który będzie się słuchał Martyny.
    * Tr+3
    * VXXV: Martyna doda nowego ucznia, wyjaśnienie że Goldarion to patchwork a nie Prosperion, Karolina jest upokorzona i będzie współpracować.
* 3 - Karolina próbuje zmaksymalizować cargo i korzyści nie wiedząc o patchworkach i ryzykach na Goldarionie.
    * Ex+3
    * XXXV: Karolina straciła 400 ładunku zysku (1100 a nie 1500), ubrudziła się jak świnia i biegała w panice a Janusz to nagrywał
* 4 - Janusz swoje nagrania rozpuścił wśród załogi dla poprawienia morale, swojej pozycji i ogólnej zabawy. Aleksander nie oponował
    * TrZ+2
    * XVXV: Karolina w depresji - Martyna musi działać, Goldarionowcy uwielbiają Aleksandra. Pokazali przybyłym.
* 5 - Martyna wygłosiła orędzie do załogi. Rozleniwili się - pojechała po ambicji. Jest szansa na polepszenie warunków, trzeba pokazać co da się zrobić z Goldarionem. I dla osłody - UDZIAŁY w zyskach.
    * TrZ+3
    * VXXVXV: ferwor, Jonatan ranny, będą bójki ale będą współpracować z Goldarionowcami
* 6 - Aleksander też wygłosił orędzie - spotkanie udziałowców. Nie mówi do nich jako kapitan a jako rekin czujący krew. Trzeba tylko ukraść wiedzę. Pomóżcie im naprawić naszego Goldariona ;-).
    * TrZ+4
    * VVXXOVXXO: twarde jądro oporu na G, sabotaż Jonatana, brutalne bójki - ale będzie współpraca i pojawiają się dziwne przyjaźnie.
* 7 - A jak poszła załogom naprawa Goldariona? Tydzień solidnej pracy
    * ExZ+3
    * OXXOVV: uszkodzenia, ranni, spowolnienie - ale lepszy QoL i Goldarion trochę połatany
* 8 - I jaki jest ostateczny wynik komitywy załogi Goldariona i Płetwala?
    * [8X | 2V+3V+5V+2V]
    * XXVVV: Bójki, kocenie, prawdziwe nienawiście - ale umieją współpracować a Janusz i Karolina jakoś się zakumplowali i obie strony się czegoś nauczyły

## Inne
### Projekt sesji

#### Procedure

* Overarching Theme:
    * gracze mają "kierowanie" (Vision & Purpose); wiedzą gdzie iść i po co tam idą
    * gracze są bardziej zaangażowani w historię, bo widzą, gdzie ich decyzje prowadzą
    * MG wie, które sesje wprowadzać i które usuwać
    * MG wie, w którą stronę stawiać adwersariat
* Achronologia:
    * przez przeskakiwanie do przodu nieważnych wydarzeń oczekuję, że każda sesja jest powiązana z kontekstem (gracze widzą jak się to rozwija)
    * przez cofanie się i uzupełnianie luk w przeszłości oczekuję, że brakujące MG i graczom luki uzupełnimy takimi działaniami
    * przez działanie w przeszłości gracze czują silniejszą relację z postaciami i światem, silniejsza budowa wspólnej historii.
* Wyraziste, ważne dla graczy postacie + Relacje (konflikt?) Gracze <-> NPC, NPC <-> NPC
    * graczom bardziej zależy na postaciach i ich powodzeniu
    * więcej potencjalnych wątków i rzeczy które możemy odrzucić przy krzyżykach
* Tory, zmiana postaci z czasem, wpływ graczy na świat
    * gracze widzą jak wpływają na świat. Wpływ jest "ich" - stąd tak tragiczne jest gdy "ich" rzeczy są corrupted / destroyed.

#### Result

* Overarching Theme:
    * V&P: Goldarion ma ostatnią szansę. Leszert ma ostatnią szansę finansowo.
    * "czy zapłacimy tym co jest ważne czy pieniędzmi i przyszłością?"
    * adwersariat:
        * Kamil Frederico: gość z Luxuritas, który chce uzyskać "śpiącą królewnę" (Azonię).
            * SL Uśmiechnięta
        * Eszara d'Goldarion stoi przeciwko Kamilowi. Porwała część załogi "Płetwala" by ściągnąć tu okręty wojenne Orbitera.
            * po jej stronie: Uczkram, Kirlat
            * przeciw niej: Szelmer (Skażony), Pszteng (potrzebuje kasy dla stacji), Taranit (pokój i luksus)
        * rozpadający się Goldarion
* Achronologia:
    * Sprawdzamy relacje Aleksandra Leszerta i Martyny Bianistek
        * WIEMY że Martyna ma wonty do Aleksandra o kasę.
        * Nie wiemy czemu.
* Wyraziste, ważne dla graczy postacie + Relacje (konflikt?) Gracze <-> NPC, NPC <-> NPC
    * Azonia Arris: zamrożona "śpiąca królewna", broń ostatniej szansy. Naukowiec i mag. Skażona i uszkodzona.
    * Adam Permin: oficer porządkowy Goldariona. mtg_B. Nieetyczny jak cholera, niechlujny.
    * Eszara d'Dorszant
* Tory, zmiana postaci z czasem, wpływ graczy na świat
    * Azonia: CORRODES into Esuriit
    * Frederico: przejmie Azonię
    * Goldarion: rozpadnie się 

#### Scenes

* Scena 0: 
* Scena 1: Dotarcie na stację mimo strasznych problemów z Goldarionem.
* Scena 2: Negocjacje z Eszarą / innymi magami
* Scena 3: 
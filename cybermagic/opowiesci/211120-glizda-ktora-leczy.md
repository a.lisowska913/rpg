---
layout: cybermagic-konspekt
title: "Glizda, która leczy"
threads: rekiny-a-akademia, amelia-i-ernest, historia-rolanda
gm: żółw
players: kić, dzióbek
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [211212 - Nie taki bezkarny młody tien](211212-nie-taki-bezkarny-mlody-tien)

### Chronologiczna

* [210824 - Mandragora nienawidzi Rekinów](210824-mandragora-nienawidzi-rekinow)

## Plan sesji
### Co się wydarzyło

* Znudzona Amelia organizuje zabawy typu "zapłaćmy bezdomnym, niech się biją, zapłacimy im i ich wyleczymy"
    * Amelia ma dużą popularność u Rekinów
    * Szczególnie podoba się to podejście Oliwii Lemurczak
* Pojawia się Roland Sowiński
    * Bardzo zdolny i inteligentny mag, pilnuje Rekiny. By nie robiły niczego głupiego. He is a man of virtue. WĄŻ, kobieciarz, ale man of virtue.
    * Roland Sowiński zatrzymał głupie zabawy Amelii. Nie ma po co zrażać do siebie lokalsów.
        * Oliwia szczególnie protestowała. Są na prowincji. Nikomu ważnemu nie robią krzywdy :p.
* Grzymościowcy jednak skutecznie oskrzydlają Pustogor i korzystają z tego, że siły terminusów są po prostu za niskie. Grzymościowcy - mafia - robi ogólnie co chce. Zwłaszcza Ernest Kajrat.
* Roland Sowiński chce, by Rekiny były siłą dobra. By pomagały i robiły dobrą robotę.
    * Rekiny niespecjalnie chcą Rolanda słuchać
    * Amelia Sowińska to organizuje zwłaszcza jako różne zabawy i konkursy dla Rekinów.
* Siły Grzymościa - a zwłaszcza Kajrat - zawsze wygrywają jeśli Rekiny wejdą w nimi w konflikt.
    * To się działo już 4 miesiące.
    * Sabina Kazitan (21) wskazała Rolandowi przyczyny, dla których Kajrat jest taki silny. Esuriit w Czółenku. Są rytuały Krwi pozwalające na kontrolę.
    * Sabina zagrała na dumie Sowińskiego i na tym, że ludzie i tak są ranni. Sowiński przyjmuje pomniejsze rytuały Esuriit
        * Amelia nie jest przekonana, ale Roland jest wyżej i jest bardziej znaczący.
        * Oliwia nie wiedząc co Roland robi docenia jego działania. Nie wie, że Sabina mu podpowiada.
* Roland wpada w coraz większy ciąg. Coraz głębsze działania. Coraz większa moc. Coraz więcej pieniędzy ściąga by zwalczać Kajrata.
    * Ściąga sprzęt. Ciężki. Ściąga rytuały. Ściąga najemników.
        * Kajrat złośliwie nie postrzega Rolanda jako przeciwnika, nadal.
        * Pustogor widzi ruchy Rolanda; nie stać ich na to, by rezygnować ze wsparcia Rekinów. Plus, Roland jest "tym sensownym"
* Amelia próbuje pomóc Rolandowi. Odciąć go od Esuriit. Jak jej się nie udaje, chciałaby zgłosić...
    * ...ale Sabina jej pokazuje implikacje polityczne. Co, Amelia i Roland - Sowińscy - nie poradzą sobie z czymś z czym ONA sobie poradzi? Czy Oliwia? Plus... co to zrobi z reputacją Rolanda?
        * Amelia próbuje to rozwiązać SAMA. I ukryć wszystkie mroczne rzeczy jakie robi Roland że niby robi je ona.
* Roland i jego Paladyni postawili świątynię do Akailora (sacrifice -> power) w Lesie Trzęsawnym.
    * Amelia jeszcze nie wie, Sabina - "zmuszona" - powiedziała im jak.
    * Zaczynają znikać ludzie. Takich, których nikomu nie brakuje. Terminusi zaczęli się interesować.
* Roland wezwał Kajrata na pojedynek. Kajrat wyśmiał. Roland powiedział, że jeśli Kajrat wygra, Roland przestanie stawać mu na drodze.
    * Znaczenie Rolanda i jego reputacja sprawiają, że ludzie dookoła rozpoznają go jako "dobrego" a Amelię jako "złą".
    * Amelia tymczasem próbuje coś zrobić, by operacje harvestowe Rolanda się przestały udawać...

### Sukces graczy

* Nie dopuścić do śmierci niewinnych w wyniku walki Kajrat - Roland.
* Uratować Rolanda Sowińskiego przed korupcją

## Sesja właściwa
### Scena Zero - impl

.

### Scena Właściwa - impl

Briefing. Szambelan. Zaprosił do siebie Sensacjusza i Stellę. Żadne z tej dwójki nie wie czemu tu jest, ale Sensacjusz w ogóle nie ma pojęcia. Czemu SOWIŃSCY go wezwali? O co chodzi? Dlaczego tu musi być? Jest tylko Diakonem działającym w Aurum i to też nietypowym; jest u Sowińskich, bo tu jest więcej okazji do badań.

Szambelan powiedział, że **Anastazy Sowiński** jest zmartwiony:

Pokazał kilka filmików:

* film Kacpra Bankierza. Dwóch żuli okładających się niechętnie, Amelia robi zawody - obstawiają, kto z żuli zbije innego szybciej. Amelia jak zawsze zblazowana.
* informacja, że Roland ma ogromne koszty. Ściąga sprzęt ciężki. Ciężkie servary bojowe. Zbroi się na wojnę. A Dzielnica Rekinów w Podwiercie to teren zdemilitaryzowany...
* filmik Karola Pustaka - jakiegoś randoma - że on robi "Plotki Royalsów" (nową gazetę). W pierwszym odcinku - wywiad z Pożeraczem 
    * Sensacjusz spytał, czy ma znaleźć dziewczynę Pożeraczowi. Szambelan powiedział - COKOLWIEK.

I stąd pojawił się cel zespołu:

* Cel Zespołu
    * poskromić panienkę Amelię
        * Roland powinien był temu zapobiec... zwykle to robił. Jak to się stało, że Amelia szaleje?
    * z tego wynika "zróbcie by było dobrze"
    * reputacja Rolanda MUSI ZOSTAĆ nieskazitelna. On jest paragonem Aurum. To ważne.

Komentarz szambelana: "To jest prowincja, tam nie może być nic poważnego. W jakiś sposób Roland zbłądził i Amelia wpadła w kłopoty."

Sensacjusz spytał czemu on tam jest. Szambelan powiedział, że Sensacjusz ma unikalne umiejętności które powinny zapewnić rozwiązanie tego problemu. Sensacjusz nie do końca rozumie, w jaki sposób umiejętność robienia broni biowojennej tu pomoże, ale wolał zamilknąć. Może Szambelan chce, by Sensacjusz pomógł Rolandowi pokonać mafię..? Do przeanalizowania i sprawdzenia na miejscu, to nie pora na tę rozmowę.

Gdy szambelan powiedział jakie jest ich przebranie "Nowy lekarz Rekinów i lekko rozwydrzona nastolatka (Stella ma koło 40-tki)", Sensacjusz się podłamał. Bierze swoją glizdę medyczną. Nie ufa szambelanowi. "Sowińscy płacą za transport. Nie będę odchudzał glizdy." To i tak krótki assignment, krótka fucha, bo Sowińscy stwierdzili, że to on ma unikalne umiejęntości. 

Do momentu aż przeczytał "swoje dossier" w drodze. Pomylili go z kimś innym. W dossier - on ma mieć umiejętności zjednywania kobiet, charyzmę... a on nic z tego nie ma. Poskarżył się Stelli. Ona się zdziwiła - zwykle Sowińscy nie mają tego typu problemów i nie robią tego typu błędów. No cóż. Chwilowo nic na to nie poradzą. Mają instrukcje. Sensacjusz tylko bardzo pamięta by się nie przyznawać - zwłaszcza przy mafii medycznej - kim jest naprawdę XD.

Na miejscu. 

O dziwo, jak na Rekiny które są na wojnie z mafią... nie ma strażników. Sensacjusz i Stella (przebrana za Serafinę Blakenbauer która jest przebrana za Sowińską) weszli spokojnie do Dzielnicy Rekinów. Nikt im nie stoi na drodze, nikogo nie zdziwili. Ok. No to objęli po prostu pierwszy dość bogaty apartament. NIKT ich nie przywitał, nikt nie sprawdził... tak bardzo wojna.

Jak tylko się zakwaterowali (łącznie z glizdą) to od razu Sensacjusz objął placówkę medyczną. I niech tymczasowy lab tam stoi.

Pukanie do drzwi. Wszedł Justynian Diakon. Przedstawił się jako szef wydziału wewnętrznego Rekinów (facepalm Sensacjusza). JAK TO PRZESZLI PRZEZ STRAŻ. Są z mafii? Ma ich aresztować? Ale Stella przedstawiła się jako Sowińska a Justynian bardzo nie chce aresztować tien Sowińskiej. Sensacjusz udowodnił Justynianowi, że jest Diakonem. Justynian zaufał Sensacjuszowi, oblewając pierwszy social ops. Justynian spytał "To oni nie są z sił Kajrata"?

Stella podłamana. KAJRAT. Ernest Kajrat. Noktiański komandos, teraz mafia, prawa ręka Grzymościa. Rekiny **nie mają szans**. Cholerny Pustogor nie poradzi sobie z Kajratem...

Justynian ma wiadomość - OMG WARTOWNICY! Są nieprzytomni. Piana na twarzy, ciężko ranni? UMIERAJĄ? Cóż, Sensacjusz z Justynianem, Stella poszła odwiedzić kuzynkę (Amelię). 

Sensacjusz zbadał wartowników. Faktycznie paskudnie to wygląda, ale to lekka trucizna. Ma wyglądać strasznie, ale wyraźnie mafia się z nimi bawiła. Ktoś bardzo kompetentny się zakradł i lekko nożem zranił wartowników - wprowadzając ich w paskudne konwulsje. Samo przejdzie po kilku dniach, Sensacjusz może to naprawić bez szczególnych starań w dzień. Więc zebrał ochotników, niech pomogą w transporcie. Dobrze mieć okazję do przetestowania laboratorium. 

Plus Sensacjusz wysłał wiadomość do Szambelana "Gief lab, mamy pierwszych rannych. Szybko."

Tr (niepełny) Z (nieprawdziwe dossier) +2:

* V: PRESSURED. Przyjdzie laboratorium w trybie expedite. Drogo. Ale będzie.

A TYMCZASEM Stella i Amelia. Amelia za komputerami, zajęta jak cholera. Zblazowana. WHO ARE YOU? Ale pod zblazowaniem jest stres. Stella przedstawiła się jako Serafina Blakenbauer. Amelia przewróciła oczami - kolejna nowa. Czy Serafina ma jakieś miejsce? Tak. Super, Amelia zajmie się nią i przygotuje jej orientation jutro. Stella chce być przydatna (i zobaczyć co robi Amelia). Amelia na to - niech Stella zapewni że Pożeracz nic nie zrobi, żadnego wywiadu. Amelia nie ma czasu na Pożeracza a on jest potencjalnie morderczy dla reputacji Rekinów. Ale Stella nie wie jak go znaleźć i udaje że kiepsko lata ścigaczem. Amelia zrobiła minę jakby zjadła coś nieświeżego...

Wyraźnie Amelia próbuje ogarnąć obóz. Wyraźnie jej nie wychodzi. A mafia jest dla niej problemem.

Wtedy przybył Sensacjusz i się przedstawił jako lekarz. Amelia ma ulgę. Nie miała wsparcia medycznego a przewiduje że nie będzie dobrze.

"SOS lekarz" od grupy pilnującej Rolanda. "Pilne". Amelia zbladła. Sensacjusz jest gotowy do działania. Stella go przewiezie. Amelia powiedziała gdzie mają lecieć.

Dla odmiany sześciu Rekinów pilnujących namiotu na polanie w Lesie Trzęsawnym ma servary klasy Lancer i są ustawione na tryb automatyczny - niech TAI Elainka dowodzą. Ci są gotowi do działania. Podobno Roland jest w namiocie i nie chce nikogo widzieć poza lekarzem. Oczywiście wpuścili tam Sensacjusza i Stellę, acz zażądali dla odmiany autoryzacji. Wyższy poziom kompetencji.

Roland wygląda jak klaun i ma śmieszny głos. Gdy przemawiał do ludności (scavengerzy ludzcy z Podwiertu), został trafiony przez snajpera. Snajper przeszedł przez kordon Lancerów (wykorzystując przewidywalność ruchów Elainek) i użył pocisku który spenetrował pole siłowe Rolanda. Czyli cholerny fachowiec (czyt - Amanda Kajrat). Roland podejrzewa zdrajcę wśród Rekinów, ale Sensacjusz wyjaśnił, że bardzo doświadczony snajper mógłby to zrobić skutecznie. Zwłaszcza taki znający się na wojnie (noktiański).

Sensacjusz zaczyna skanować Rolanda. Chce zobaczyć czym dostał i jaki jest efekt. Atak był wyraźnie organiczny...

TrZM+1+1Or (esuriit):

* V: to tylko wygląd. I głos. Amanda miała serce. Mogła zabić, serio.

Roland jest wściekły. Wini wszystkich. Nie siebie. To zdziwiło Stellę - Roland zwykle winił SIEBIE najpierw, rzadko zrzucał winę np. na Amelię (źle dobrała miejsce). Sensacjusz koryguje Rolanda - moment, to ON dowodzi. On wybrał lokalizację. On zdecydował że przemawiać. To czemu Amelia? Roland się opamiętał; nie jest szczęśliwy, ale faktycznie może to jego wina...

Sensacjusz zaczyna korygować Rolanda. Leczenie na zasadzie "stabilizacja". Na głos obniżyć napięcie strun głosowych. Na swędzenie - stoneskin. Ma wyglądać niedobrze. Pokiereszowany. Ma wyglądać tak by zatrzeć wrażenie klauna. I solidny kop adrenaliny / czegoś odpowiedniego by nie miał biegunki do czasu glizdy. To go ochroni.

TrZM+3+1Or (esuriit):

* VV: Udało się osiągnąć wszystko co miało być. Roland jest "zdatny do użycia". Do rozmowy. Nie wygląda jak klaun, a jak ofiara ataku CZEGOŚ.

Roland jest wściekły. Nie ma dostępu do swoich entropików. Mafia musi zostać zniszczona. Stella próbuje pociągnąć za język. Dlaczego jemu tak na tym zależy by zniszczyć mafię? To nie Roland jakiego Stella "zna" z dossier i profili. Od 2 tygodni rosną jego żądania - sprzęt itp.

TrZ (Stella zna patterny zachowania) +3:

* Xz: jego patterny zachowań są inne. Stella nie poznaje Rolanda. Obraziła Rolanda raz czy dwa, ale on się nie przejął (OMG JAKAŚ LASKA SOWIŃSKA OBRAŻA ROLANDA)
* X: pro-Rolandowe siły gardzą "Blakenbauerką"
* V: co skłoniło do walki z mafią? --> mafia przejęła praktycznie Podwiert i okolice. Terminusi nie dają rady. Chciał pomóc ludziom i pomagał. Amelia gamifikowała. I wtedy gdy wchodził na działania mafii jego Rekiny cierpiały. "Trochę przyjaznych tortur i nago do domu".
* V: Roland zaczął ściągać sprzęt by dozbrajać najpierw lokalsów i terminusów. Kajrat część przechwycił. I Roland doszedł do tego, że Kajrat ma nieprawdopodobną przewagę. Nie da się go łatwo i sensownie pokonać. A ludzie cierpią. Rekiny cierpią. I kombinował, próbował - i nic. Tylko kierował Kajrata na Rekiny. I wtedy musiał zacząć eskalować, bo jego własne Rekiny zaczęły cierpieć przez Rolanda. Dlatego eskaluje w górę - my albo oni.
* V: Roland ZROZUMIAŁ gdzie jest jego słabość. Kajrat włada Esuriit. Ma je w Czółenku. Kontr-rytuały. Neutralizacja Esuriit. Skoro ludzie i tak cierpią, da się to wykorzystać. Skaził się. SKAZIŁ SIĘ ESURIIT. Skoro LARWA NOKTIAŃSKA może sobie na to pozwolić i tego używać... i nie jest jedyna... to on, z rodu też może sobie na to pozwolić.
* V: Roland zmusił czarodziejkę która zna się na rytuałach wśród Rekinów by mu wszystko wyjaśniła. Oczywiście, to niebezpieczne. Ale nie musi ranić. Sabina Kazitan. Sabina została wysłana przez Oliwię Lemurczak do pomocy w działaniach wojennych. Herbatka, dokładny cytat "cokolwiek będzie chciał", i Sabinie się wymsknęło że wie coś na ten temat. I Roland ją zmusił, by mu wszystko powiedziała i wyjaśniła.

Ok. Niefortunne i niebezpieczne. Czyli Roland Sowiński, paragon of virtue, dał się Skazić Esuriit. To będzie niszczycielskie dla reputacji jak wyjdzie na jaw. Sensacjusz zaproponował Rolandowi poważną korekcję w gliździe. Przy okazji, będzie silniejszy. Roland się oczywiście zgodził - wrócili do Dzielnicy Rekinów.

Sensacjusz i glizda - niech glizda go unieruchomi i wyleczy z efektów sklaunienia.

TrZ+3:

* Xz: Glizda ciężko chora przez dłuższy czas (tydzień czy coś) przez to wszystko Esuriitowe
* X: Glizda nie jest w stanie usunąć Skażenia i zdeklaunowania. Sorry.
* V: Glizda go containuje tak długo jak trzeba.

Czyli glizda jest przeznaczona do transportu do Sowińskich. Sensacjusz chce to zgłosić do bazy (transportujemy glizdę, Roland jest Skażony Esuriit) i potrzebuje biolab ASAP. Wiadomość z centrali było "ok, będzie dzisiaj" itp.

5 minut później do piwnicy z glizdą i Stellą + Sensacjusz wpadają dwa Lancery. Z trudem. "Amelia was szuka". Justynian i jeszcze jeden. Sensacjusz przekonuje ich, by pilnowali glizdy. Justynian współpracuje, zorientował się, że coś jest nie tak. A Sensacjusz ze Stellą poszli sami do Amelii.

Amelia. Opieprza ich że dane otwartym hipernetem. Stella zdziwiona - mało kto ma uprawnienia inspektora. Amelia kontruje - ONA ma. I zablokowała ten sygnał.

Stella - skąd Amelia wie? Sprawdza w swojej bazie danych

TrZ+2:

* V: Amelia ma podniesione dostępy. Ma je przydzielone przez Anastazego, bo on jej zaufał. Amelia nie ma podniesionych baz ale ma inspektor hipernetu Sowińskich / Aurum.
* V: Stella wie o Amelii i jej zachowaniu - Amelia normalnie nie chroniłaby Rolanda w taki sposób - chyba, że czuje się WINNA. Wina + obciążenie = paranoja i ochrona kuzyna.

Stella się przyznała kim jest. Ofc Amelia nie uwierzyła. Jak dostała potwierdzenie, zadziałało. Zażądała, by Stella przejęła dowodzenie. Amelia chce pomóc, może pomóc, ale nie będzie firmować tej parodii swoim nazwiskiem. Gdy Stella przejęła dowodzenie, Amelia powiedziała, że Sensacjusz chce pomóc grupie ludzi w konkretnym apartamencie. 

Sensacjusz poszedł lekarzyć. Zastał paskudny obraz. Kilkanaście ludzi.

TrZM+2+Or:

* X: Niektórym się już nie dało pomóc. **Gdyby Amelia wysłała go tam od początku, dałoby się.** Amelia ma ich krew na rękach.
* Vz: Udało się część osób uratować. Bez Sensacjusza nie dałoby się ich uratować
* V: Ślady wskazują, że jest WIĘCEJ Rekinów.
* V: Sensacjusz może poznać więcej informacji kto tu ssał. Masz odpowiedź - sześć osób PLUS Roland. W czym Roland ssał najwięcej. I wszyscy w jednym cyklu.

Czyli Rekinów Skażonych przez Esuriit jest więcej. Stella zastawia więc pułapkę. Jak ktoś przyjdzie ssać, Stella chce wiedzieć kto. I zapolujować. A dokładniej - Sensacjusz poluje. I Sensacjusz wzmacnia swoją formę bojową. Chce móc ich pokonać.

Sensacjusz chce się przygotować. Wilkołacza forma + trucizna. "More compliant". Mają nie pamiętać niczego potem. 

TrZM+3:

* X: Trudno się pozbyć tej formy na pewien czas
* X: Potrzebna jest glizda do zdjęcia tej formy...
* O: THEY WILL LIVE. Ale powpadają w katatonię. Stracił GRANICE swojego zaklęcia. "There is something more". Jego ciało jest BARDZIEJ bojowe, kosztem innych rzeczy. Monster Superior.
* Vz: Następny konflikt nie jest potrzebny. --> ci Skażeni Esuriit to są paladyni.

Sensacjusz zażądał od Justyniana by on zaopiekował się pacjentami. Sensacjusz nie może w tej wilkołaczej formie. Justynian jest przekonany, że Kajrat tu był i zabił tych ludzi i przeklął Sensacjusza. Sensacjusz go nie koryguje; lepiej by Justynian w to wierzył niż znał prawdę.

Po tym jak Amelia się wyspała i jest w stanie aktywnym, Stella żąda od niej raport. Amelia przedstawiła wszystko, nie konfabulowała i próbowała pomóc.

Czyli Amelia wszystko wiedziała. Ale nic nie powiedziała, próbowała sama coś z tym zrobić. Chroniła reputację Rolanda i kombinowała. Ale nie może nic zrobić, bo Oliwia za dużo wie. Ale Oliwia Lemurczak jest teraz na jej celowniku. Za wszystkim zdaniem Amelii stoi Oliwia Lemurczak a Amelia nic nie może zrobić...

Stella powiedziała, że się zobaczy. Na razie jednak trzeba rozwiązać problem mafii.

Stella ściągnęła strategicznego oficera z Aurum. Sensacjusz, uwięziony w wilkołaczej formie, pokazał show of strength - atak od czasu do czasu na mafię. Ma siłę ognia, ma możliwość ataku z zaskoczenia. A potem wiadomość - "deeskalujmy, mamy zmianę dowodzenia".

TrZ (nowa bioforma + strategic advisor) +4:

* V: Deeskalacja jest faktem.
* V: Kajrat i siły mafii nigdy nie doszły do tego, że to Sensacjusz jest "mrocznym demonicznym potworem-wilkiem"
* V: Grzymość wycofał Kajrata. It's bad for business. Nowe dowodzenie Rekinów po prostu jest zbyt mocne.

A na sam koniec - Stella eskalowała dowodzenie przez Aurum i Amelia, która NIE chciała dostać dowodzenia z powrotem - dostała. I jest w czarnej dziurze. Ta biurokratka NIGDY o tym nie pomyślała że tak się da. Nienawidzi za to Stelli z całego serca, ale zrobi co może by to utrzymać.

STELLA PO POWROCIE PRÓBOWAŁA NAPRAWIĆ TO DOSSIER! I DLATEGO ONI TO ZAKOPALI! BY ŻADEN BŁĄD NIE WYSZEDŁ NA JAW! I dlatego Sensacjusz tam utknął... z Amelią. I Rekinami. WHYYYYY!!!

## Streszczenie

Sensacjusz Diakon został wysłany na prowincję pod przykrywką Lekarza Rekinów, by dojść ze Stellą do tego czemu Amelia i Roland Sowińscy zachowują się nietypowo. Na miejscu okazało się, że Roland jest w stanie ostrej wojny z mafią (która nie traktuje tego poważnie; Kajrat się bawi z Rekinami) a Amelia po prostu nie w pełni sobie radzi i próbuje opanować sytuację. Roland jest Skażony Esuriit; Sensacjusz przetransportował go glizdą do Aurum i sam, uwięziony w bojowej formie, deeskalował konflikt z mafią. A Amelia jest tymczasowym dowódcą Rekinów...

## Progresja

* Sensacjusz Diakon: ma fałszywe dossier u Sowińskich - jest wybitnym lekarzem, kobieciarzem i ma niezwykłą charyzmę. Zesłany na prowincję. Sęk w tym, że TAMTEN Sensacjusz nie żyje. Pomylili osobę.
* Roland Sowiński: Skażenie Esuriit. Zmienia to jego Wzór trochę, nawet po wyleczeniu i regeneracji. Powrót do Aurum.
* Oliwia Lemurczak: Stella Sowińska (pośrednio: sporo Sowińskich) jest święcie przekonana, że to ona stoi za Skażeniem Rolanda Esuriit. Będzie vendetta.
* Amelia Sowińska: wściekła na Stellę Sowińską. Ta jej przekazała dowodzenie Rekinami w najgorszym momencie. Zemści się kiedyś. Tak, jak wściekła na Oliwię Lemurczak na której TEŻ się zemści.

### Frakcji

* .

## Zasługi

* Sensacjusz Diakon: inżynier biowojenny, który na skutek "pomyłki" trafił na prowincję (Podwiert) być lekarzem Rekinów. Odkrył, że Roland jest Esuriitowy i wpakował go w glizdę i uratował. Potem w trybie bojowym "wilkołaka" odpędził mafię - zmienił im cost-to-benefit ratio.
* Stella Sowińska: agentka specjalna Sowińskich przebrana za Blakenbauerkę przebraną za Sowińską. Pociągnęła Rolanda za język; odkryła, że jest Esuriitowy. Tymczasowo przejęła kontrolę nad Podwierckimi Rekinami, po czym jak rozwiązała najważniejsze rzeczy, oddała dowodzenie niechętnej Amelii wbrew niej.
* Amelia Sowińska: trzymała w kupie i organizowała Rekiny zgodnie z życzeniami Rolanda mimo Esuriit i jego chorych pomysłów. Nie dopuszczała, by jego żądania Entropików przeszły dalej. Ukrywa koszt reputacyjny za wszelką cenę. Daje się manipulować Sabinie Kazitan / Oliwii Lemurczak.
* Roland Sowiński: tak bardzo chciał pomóc wszystkim i pokonać mafię, że dał się wrobić Sabinie Kazitan i wkręcił się w Esuriit. Jego Skażenie doprowadziło do eskalacji działań z mafią i w konsekwencji wysłania tu Sensacjusza i Stelli. 23 lata.
* Karol Pustak: młody miłośnik arystokracji Aurum z okolic Pustogoru. Założył "Opowiastki z Aurum" i zrobił wywiad z Pożeraczem. To był sygnał tego, że Roland stracił kontrolę nad Rekinami.
* Feliks Keksik: ksywa "Pożeracz". Miał 5 minut sławy - pierwszy odcinek "Opowiastek z Aurum" to był wywiad z nim. A opowiadał jak młody basza na prowincji...
* Kacper Bankierz: w nagraniu polecał innym arystokratom przyjechanie na prowincję, BO TU JEST MEGA FAJNIE! Amelia robi zawody z bicia się żuli i w ogóle! 
* Justynian Diakon: 
* Sabina Kazitan: przekazana przez Oliwię Rolandowi, skusiła go możliwością pokonania Kajrata i wkręciła go w Esuriit. Zdominowana przez Rolanda, jednak tak naprawdę to ona wygrywa.
* Oliwia Lemurczak: wszyscy podejrzewają, że to ona kazała Sabinie Kazitan Skazić Rolanda Sowińskiego. Sytuacja wymknęła jej się spod kontroli, ale nic nie może zrobić.
* Ernest Kajrat: ojej, Rekinki zdecydowały się na pomoc lokalną. To słodkie i fajne. Ale wchodzą w szkodę Grzymościowi? Ok - czas na darmowy trening. Maksymalizacja upokorzenia.
* Amanda Kajrat: przekradła się koło czterech Lancerów sterowanych przez Elainkę i zestrzeliła Rolanda Sowińskiego jak przemawiał na podwyższeniu, zmieniając go w klauna. Zwykłe ćwiczenia.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Podwiert
                                1. Las Trzęsawny: na jednej z polan przemawiał Roland Sowiński. Amanda Kajrat go ustrzeliła snajperką i zmieniła go w klauna. Dla zabawy.
                                1. Dzielnica Luksusu Rekinów: miejsce zdemilitaryzowane zgodnie z Porozumieniem Pustogorskim. Rekiny nie mogą mieć bardzo ciężkiego sprzętu itp.
                                    1. Serce Luksusu
                                        1. Lecznica Rannej Rybki: założona przez Sensacjusza ze Stellą z jednego z Apartamentów na szybko. Faktycznie, szambelan Sowińskich dosłał sprzęt medyczny.

## Czas

* Opóźnienie: -1184
* Dni: 11

## Konflikty

* 1 - Plus Sensacjusz wysłał wiadomość do Szambelana "Gief lab, mamy pierwszych rannych. Szybko."
    * Tr (niepełny) Z (nieprawdziwe dossier) +2
    * V: PRESSURED. Przyjdzie laboratorium w trybie expedite. Drogo. Ale będzie.
* 2 - Sensacjusz zaczyna skanować Rolanda. Chce zobaczyć czym dostał i jaki jest efekt. Atak był wyraźnie organiczny...
    * TrZM+1+1Or (esuriit)
    * V: to tylko wygląd. I głos. Amanda miała serce. Mogła zabić, serio.
* 3 - Sensacjusz zaczyna korygować Rolanda. Leczenie na zasadzie "stabilizacja". Ma wyglądać tak by zatrzeć wrażenie klauna. To go ochroni.
    * TrZM+3+1Or (esuriit)
    * VV: Udało się osiągnąć wszystko co miało być. Roland jest "zdatny do użycia". Do rozmowy. Nie wygląda jak klaun, a jak ofiara ataku CZEGOŚ.
* 4 - Stella próbuje pociągnąć za język. Dlaczego jemu tak na tym zależy by zniszczyć mafię? To nie Roland jakiego Stella "zna" z dossier i profili.
    * TrZ (Stella zna patterny zachowania) +3
    * XzXVVVV: pro-Rolandowe siły gardzą Stellą, obraziła go; ale rozumie - Roland chciał pomóc lokalsom, starł się z mafią, nie ma jak pokonać Kajrata i skuszony przez Sabinę -> Esuriit.
* 5 - Sensacjusz i glizda - niech glizda Rolanda unieruchomi i wyleczy z efektów sklaunienia. Niech Roland jest nieprzytomny.
    * TrZ+3
    * XzXV: ciężko chora glizda, nie da rady odklaunić, ale utrzyma. Trzeba glizdę wysłać do Aurum.
* 6 - Stella - skąd Amelia wie o ich komunikacji z Aurum? Sprawdza w swojej bazie danych
    * TrZ+2
    * VV: Amelia ma podniesione dostępy; na pewno czuje się winna. Wina + obciążenie = paranoja u Amelii.
* 7 - Sensacjusz poszedł lekarzyć ludzi Wyssanych Esuriit. Zastał paskudny obraz. Kilkanaście ludzi.
    * TrZM+2+Or
    * XVzVV: pomógł większości, ale część wymarła. Więcej by się dało uratować gdyby Amelia powiedziała gdy przyjechali. Jest więcej Skażonych Rekinów - to paladyni.
* 8 - Sensacjusz chce się przygotować do złapania Esuriitowych Rekinów. Wilkołacza forma + trucizna. "More compliant". Mają nie pamiętać niczego potem. 
    * TrZM+3
    * XXOVz: Bez glizdy nie zdejmie tej formy, oni wpadną w katatonię. Sensacjusz przez 2 tygodnie jest Monster Superior. Unieszkodliwił paladynów.
* 9 - Sensacjusz, uwięziony w wilkołaczej formie, pokazał show of strength mafii. A potem wiadomość - "deeskalujmy, mamy zmianę dowodzenia".
    * TrZ (nowa bioforma + strategic advisor) +4:
    * VVV: Deeskalacja jest faktem, Kajrat i siły mafii nie doszły że to Sensacjusz. Grzymość cofnął Kajrata - it's bad for business.

## Inne
### Projekt sesji

#### Overall

* Overarching Theme:
    * Theme & Feel: 
        * "Amelia is not the hero we deserve, but the one we need" + "what have you done, Amelia..."
        * "Virtue turned to wrath"
        * "Everyone can fall"
    * Vision & Purpose:
        * Pokazanie jak Sabina rozgrywa słabości Amelii i Rolanda, by uderzyć w **Oliwię**.
    * adwersariat: 
        * Kajrat i siły Kajrata, które polują na Rekiny dla zabawy - w czym to Rekiny zaczęły
        * "Paladyni" Rolanda, którzy są Skażeni Esuriit. Brutalna grupa pięciu Rekinów.
* Wyraziste, ważne dla graczy postacie + Relacje (konflikt?) Gracze <-> NPC, NPC <-> NPC
    * Role i postacie
        * Amelia Sowińska: A bored lady playing with lives. She will now save lives from her cousin's knives.
        * Roland Sowiński: A man of virtue whose pride turned to wrath. He just has fallen to Esuriit path.
        * Sabina Kazitan: Submissive servant of lady so cruel. Agent, catalyst, breaking the rules.
        * Ernest Kajrat: Noctian commando turned to crime. Monster for spite, not for a dime.
        * Feliks Keksik: Eternal victim, never recognized. Given Sabina in his pink daze.
        * Barnaba Burgacz: Anarchic warrior ready to fight. For the first time ever for what is right.
        * Alan Bartozol: Renegade terminus searching for a friend. Finding the deadly web of lies in the end.
* Co się działo w przeszłości
    * DONE.
* Tory, zmiana postaci z czasem, wpływ graczy na świat
    * Roland -> Skaża się Esuriit. Niedługo terminusi zainteresują się tematem i MUSZĄ go zatrzymać.
    * Aurum -> Roland sprowadza środki na swoją prywatną wojenkę. Kajrat może ów sprzęt skutecznie pozyskać.
    * Podwiert -> coraz bardziej widać, że nieważni ludzie znikają. Nasilają się starcia Rekiny - Mafia.
    * Alan -> dojdzie do tego, że Rekiny i gdzie Rekiny działają. Dojdzie do Amelii || Rolanda.

#### Sceny

1. Roland przemawia do uratowanej grupki scavengerów z Lasu Trzęsawnego, że teraz są bezpieczni.
    * POKAZUJE: ludzie wierzą Rolandowi, Roland faktycznie pomaga, Roland chce uderzyć w mafię.
    * AKTORZY: Roland, Feliks
    * KONFLIKTY: Czy ludzie zaufają Rolandowi? Czy powiedzą, gdzie jest kolejna operacja mafii - gdzie składowane jest coś cennego dla mafii w Lesie? Ludzie się BOJĄ - czy uda się przekonać Rolanda że to zły pomysł?
1. Kajratowcy zapędzili Pożeracza by go postraszyć i nagrać jak się boi. Nie chcą go skrzywdzić. Chcą go sponiewierać dla zabawy. CZEMU CHCIAŁEŚ NAS ZABIĆ! TOP SIĘ W ŚLUZIE!
    * POKAZUJE: jak bardzo mafia ma GDZIEŚ Rekiny. To ostrzeżenie dla Rekinów. Plus, rozzuchwalona mafia.
    * AKTORZY: Feliks Keksik, mafia, potencjalnie Alan Bartozol.
    * KONFLIKTY: Czy Feliks się nie rozklei? Czy Alan uratuje Feliksa? Czy wyprowadzą Rolanda z równowagi jeszcze bardziej?
1. Pożeracz był dzielny? Walczył przeciwko mafii? Dobrze - Oliwia przekazuje mu Sabinę jako damę do towarzystwa, w nagrodę.
    * POKAZUJE: jak bardzo silny jest hold Oliwii na Sabinie. Sabina zrobi wszystko co Oliwia każe.
    * AKTORZY: Oliwia, Sabina, Feliks
    * KONFLIKTY: brak
1. Alan Bartozol szuka informacji o zaginiętych ludziach. Wszystko wskazuje na Rekiny. Nie jest zbyt wielkim fanem procedur.
    * POKAZUJE: znikają ludzie którzy nikogo nie interesują. Ale terminus jest na ich tropie.
    * AKTORZY: Alan Bartozol, postać gracza
    * KONFLIKTY: czy Alan powie coś więcej? Czy compeluje postacie do pomocy?
1. Roland chce sprowadzić ciężki sprzęt z Aurum by zwalczyć Kajrata. Amelia próbuje go zatrzymać. NIE SĄ OD TEGO. "Prawość tego wymaga, Amelio!"
    * POKAZUJE: głębię obsesji Rolanda. Desperację Amelii.
    * AKTORZY: Roland, Amelia
    * KONFLIKTY: czy Roland da się deeskalować? czy Amelia go zatrzyma? Czy Amelia dyskretnie to anuluje? Czy Roland ją za to skrzywdzi? Czy wyzwie ją od potworów przy wszystkich?
1. Paladyni polują na jakiegoś człowieka. Alan próbuje znaleźć dowody i ich zatrzymać. Amelia próbuje ukryć Rekiny, uratować człowieka (z pomocą BaBu).
    * POKAZUJE: Pierwszy sygnał na konflikt na linii Amelia - Roland.
    * AKTORZY: BaBu, Alan, Paladyni
    * KONFLIKTY: Czy Rekiny zdobędą człowieka? Alan go uratuje i zestrzeli Rekina? BaBu ucieknie człowieka / zestrzeli Rekiny? Kogo dorwie Alan?
1. Pojedynek Kajrat - Roland. Roland nie ma szans, nieważne jak by nie oszukiwał. Kajrat jest potworem. Kajrat nie chce zniszczyć Rolanda - nie, chce jego reputację.
    * POKAZUJE: jakiego typu osobą jest Kajrat i jak daleko się oddalił Roland
    * AKTORZY: Amelia, Ernest, Roland, widownia
    * KONFLIKTY: czy Amelia zabije człowieka?



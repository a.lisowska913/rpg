---
layout: cybermagic-konspekt
title: "Gdy HR reedukuje niewłaściwą osobę"
threads: historia-arianny
gm: żółw
players: fox, kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [221221 - Astralna Flara i nowy komodor](221221-astralna-flara-i-nowy-komodor)

### Chronologiczna

* [221221 - Astralna Flara i nowy komodor](221221-astralna-flara-i-nowy-komodor)

## Plan sesji

### Fiszki
#### 1. Astralna Flara (55 osób max)
##### 1.1. Dowodzenie

* Arianna Verlen: kapitan
* Daria Czarnewik: engineering officer, (4 inż pod nią)
* Alezja Dumorin: eks-kapitan, Orbiter, 
    * ENCAO:  +0--0 |Amoralna, skuteczna| VALS: Hedonism, Face, Power| DRIVE: Wędrowny mistrz Ryu
* Władawiec Diakon: pierwszy oficer, tien, (p.o. Stefana) 
    * ENCAO:  +-0-0 |Intrygancki;;Żywy wulkan| VALS: Hedonism, Self-direction| DRIVE: Follow My Dreams, Korupcja anioła
* Grażyna Burgacz: logistyka i sprzęt (officer), tien (3 osoby pod nią) 
    * ENCAO:  -0+-- |Powściągliwa i 'nudna';;Ascetyczna| VALS: Humility, Tradition| DRIVE: Starszy Brat | Zainteresowana duchami XD
* Arnulf Perikas: fabrykacja i produkcja (officer), tien (2 inż pod nim) 
    * ENCAO:  0-0-- |Hardheaded;;Napuszony| VALS: Tradition, Family| DRIVE: Wzbudzenie zachwytu
* Maja Samszar: comms officer, tien (p.o. Klarysy jak K. nie może) 
    * ENCAO:  0+-0- |Moralizatorska| VALS: Stimulation, Conformity >> Power| DRIVE: Długi
* Kajetan Kircznik: medical officer (5 pod nim) (czarny, afro, paw, augmentacje bio)
    * ENCAO:  +-0+0 |Anarchistyczny;;Stabilny emocjonalnie;;Z poczuciem humoru| VALS: Self-direction >> Power| DRIVE: Jestem unikalny

##### 1.2. Operacja

* Klarysa Jirnik: artillery, (p.o. Mai jak Maja nie może) 
    * ENCAO:  00+-- |Wymagający;;Małostkowy| VALS: Tradition, Conformity|DRIVE: Sprawiedliwość
* Tomasz Ruppok: starszy mat (13 osób + 2 starszych)
    * ENCAO:  +0--- |Mało punktualny;;Kłótliwy;;W przejaskrawiony sposób okazuje uczucia| VALS: Hedonism >> Achievement, Family| DRIVE: Apokalipta
* Rufus Warkoczyk: starszy mat (13 osób + 2 starszych)
    * ENCAO:  0-+-0 |Zarozumiały;;Bezkompromisowy, niemożliwy do zatrzymania;;Zorganizowany| VALS: Self-direction, Power| DRIVE: Odkrycie konspiracji ("ktoś nas sabotuje")
* Marcelina Trzęsiel: inżynier syntezy (pod Arnulfem), pod opieką tien Terienaka
    * ENCAO:  0+0-+ |Małostkowa;;Dramatic shifts in mood;;Różnorodność form| VALS: Hedonism, Achievement >> Security| DRIVE: "Co za tą górą"
* Elena Verlen: pilot / advancer

##### 1.3. Infiltracja / Starcie

* Gerwazy Kircznik: sierżant marine, Orbiter
    * ENCAO:  +0+-0 |Głośny;;Honorowy| VALS: Humility, Face| DRIVE: Lokalny społecznik
* Erwin Pies: kapral marine, Orbiter
    * ENCAO:  -0-+- |Skryty;;Wiecznie zagubiony;;Praktyczny| VALS: Tradition, Stimulation >> Power| DRIVE: Ochraniać słabszych
* Szymon Wanad: marine, Orbiter
* Marcel Kulgard: marine, Orbiter, stary znajomy Arianny
    * ENCAO:  000-+ |Demotywuje innych;;Innowacyjny| VALS: Achievement, Face| DRIVE: Zbudować legacy
* Tomasz Dojnicz: marine, Orbiter
    * ENCAO:  --0+0 |Nie znosi być w centrum uwagi;;Dusza towarzystwa| VALS: Conformity, Hedonism >> Power| DRIVE: Arlekin Maytag
* Szczepan Myrczek: advancer, tien, seilita,
    * ENCAO:  --+0- |Stabilny emocjonalnie;;Zawsze bardzo zajęty| VALS: Face, Achievement, Hedonism >> Self-direction| DRIVE: Zasady są święte
* Mariusz Bulterier: advancer, tien, seilita, sybrianin 
    * ENCAO:  0-+-- |Prostolinijny i otwarty;;Nie kłania się nikomu;;Uroczysty i poważny| VALS: Humility, Tradition| DRIVE: Nawracanie
* Hubert Kerwelenios: advancer, sarderyta
    * ENCAO:  -0+-0 |Kontemplacyjny, refleksyjny;;Kompleks paladyna| VALS: Humility, Tradition >> Stimulation| DRIVE: Odbudowa i odnowa

##### 1.4. Other

* Ellarina Samarintael: Egzotyczna Piękność
    * ENCAO:  +-00+ | Spontaniczna;; Chipper;; | VALS: Power, Hedonism >> Family | DRIVE: Wyciągnąć kogoś z bagna, dobrze wyjść za mąż

#### 2. Athamarein, ciężka korweta rakietowa

* Leszek Kurzmin: dowódca jednostki (atarienin defensor) TACTICAL NERD 
    * ENCAO: 0-+++ | Żyje według własnych przekonań;; Lojalny i proaktywny| VALS: Benevolence, Humility >> Face| DRIVE: Właściwe miejsce w rzeczywistości.
* Alan Nierkamin: eks-lokalny przewodnik, advancer
    * ENCAO: ++-0- |Nietolerancyjny, musi być tak jak uważa;;Dokładnie przemyśli wszystko zanim coś powie| VALS: Power, Achievement >> Tradition| DRIVE: wprowadzi Orbiter na Nonarion
* Aleksander Warmańczyk: advancer elite
    * ENCAO:  0-00+ |Stabilny emocjonalnie;;Przesądny (Salazar wygrywa)| VALS: Face, Hedonism >> Security| DRIVE: Powszechna sława

#### 3. Loricatus, ciężka fregata szturmowa

* Salazar Bolza: komodor (drakolita ewolucjonista; misja: założyć placówkę badawczą Anomalii), INHUMAN (bioforma typ serpentis, ale mniej) model after Lorca
    * ENCAO:  --+0+ |Racjonalny;;Powściągliwy, skryty i wyważony;;Surowy, wymagający| VALS: Power, Achievement >> Security, Stimulation| DRIVE: Survival of the fittest
* Kalia Szantik: dowódca jednostki (atarienin ekspansjonista; misja: zająć teren dla Orbitera); przekonana, że Salazar jest zdrajcą
    * ENCAO:  -0+0- |Bezbarwna, przezroczysta;;Nie lubi abstrakcji;;Nie odracza| VALS: Family, Conformity, Power >> Face, Tradition| DRIVE: Harmonia Orbitera

### Theme & Vision

* Kampania
    * Problemy kulturowe
        * Orbiter nie respektuje tego co wypracował Nonarion
        * Ten teren ma wielu noktian i są traktowani jako zasób
        * Ten teren ma własną, zupełnie obcą kulturę
    * Daria nie ma łatwego powrotu
    * Władawiec corruptuje Elenę
* Ta sesja
    * Destructor Animarum i obca kultura.

### Co się wydarzyło KIEDYŚ

* ?

### Co się wydarzyło TERAZ (what happened LATELY / NOW)

* 

### Co się stanie (what will happen)

* S1: Prośba o pomoc z TKO-2292 (kompleks HRowy (więzienno-resocjalizacyjno-niewolniczo-medyczno-sztuki))
* S2: Destructor Animarum

### Sukces graczy (when you win)

* ?

## Sesja właściwa
### Wydarzenia przedsesjowe

.

### SPECJALNE ZASADY SESJI

.

### Scena Zero - impl

.

### Sesja Właściwa - impl

PLANETOIDA LODOWCA:

Bolza zaczął ustawiać Waszą stację do porządku. Póki nie przybędzie TAI chce podpiąć Persefonę d'Loricatus (Persefona na Loricatus jest silniejsza; ona może działać z bazami). Bolza chce użyć Persefony do dowodzenia post-noktiańską stacją na Planetoidzie Lodowca. I trzeba się wpiąć kiedyś w Dominę Lucis. Nawet pierwszy rzut oka pokazał, że teren jest interesujący: baza robiona w planetoidzie zaprojektowana STEALTH > DEFENSE.

* tak dużo skały, zakamarków itp jak się da
* jest to KOSZMAR z perspektywy zarządzania bazą jeśli jej nie rozumiesz

Darię interesuje Domina Lucis. Bolza absolutnie zabronił; przy statku (DL jest wbudowana w bazę by mogła odlecieć) dwóch marines i jeden robot bojowy. "Paskudnie dużo śmierci dookoła" słowami marine. Daria nie chce zwracać na siebie uwagi.

Bolza -> Arianna:

* B: pani kapitan.
* A: tak, komodorze?
* B: ten zespół młodych ambitnych infiltratorów których wzięliśmy na pokład bazy. Czy może pani ich uspokoić?
* A: coś się stało, panie komodorze?
* B: (zimnym gadzim spojrzeniem) uważają, że... (myśli jak to powiedzieć, he doesnt give a meow) Boją się duchów. I nas. Ale bardziej duchów.
* A: dobrze komodorze, zajmij się nich. Czego konkretnie się boją?
* B: duchów, bo jesteśmy w strefie duchów. Ktoś inteligentnie im powiedział gdzie się znajdują.

(TAK, ktoś dopuścił medyka do młodych)

* B: (cierpliwie czeka aż Arianna się zorientuje) (zauważył moment, w którym kliknęło.)
* A: zajmę się tym panie komodorze.
* B: cieszę się. Powodzenia.
* B: przy okazji, advancer z pani statku zgłosił się na ochotnika do przeszukiwania najbardziej niedostępnych części jaskiń. Advancer Verlen.
* A: oczywiście. (ona BARDZO nie chce z Arianną rozmawiać)

Arianna idzie do Kajetana Kircznika. Co naopowiadał. Medyk jest bardzo zajęty. Ze zdwojoną energią jest zajęty.

* K: Pani kapitan, jak dobrze panią widzieć! /afro zafalowało na wietrze
* A: Co pan opowiedział młodym?
* K: Prawdę. Nie koloryzowałem. Prawie. I nie powiedziałem o zbrodniach wojennych. A raczej tym co by uznali za zbrodnie. Prawie.
* A: Powiedział im pan o tym że Serpentis zabił załogę?
* K: (lekko skruszona mina) Próbowałem pomóc. Więc, niech pani wygodnie usiądzie.

Tak. Kircznik WPIERW opowiedział o Lodowcu i Zarralei, POTEM o samobójstwach noktiańskich a POTEM pokazał Bolzę. I powiedział że są w Strefie Duchów. Noktianie się boją NAS ale przyjdą zabić ICH. Tego się boją. I powiedział, że oni mogą pomóc i się przydać. Ale oni się boją że noktiańskie duchy uznają to za zdradę. I boją się zabić by nie służyć po śmierci. Więc się boją wszystkiego.

* K: Z dobrych wieści, pani kapitan, to była udana opowieść.
* A: Pomożesz ich wkręcić że Ellarina jest egzorcystką i poradzi sobie z duchami.
* K: (zaniemówił)
* A: Arystokratka z Aurum interesująca się duchami! Klisza historii o duchach i z NIEJ zrobimy egzorcystkę!

Arianna idzie do Mai. Jest z rodu Samszar, zna się na duchach, nie? XD

* A: Interesowały Cię kiedyś historie o duchach? 
* M: (blankface) Historie o duchach?
* A: Albo czy w Waszym rodzie w sposób szamański zajmujecie się duchami?
* M: Oczywiście, że tak, pani kapitan. Ja poszłam w kierunku bardziej kulinarnym, ale tak. Potrzebujemy czegoś o duchach?
* A: Potrzebujemy.
* M: Mam alternatywną propozycję. Grażyna.
* A: O...
* M: Grażyna dużo mnie pytała odnośnie rytuałów rodu, duchów, efemeryd itp. Nie wiem czemu. Ale ona to robiła... tak dyskretnie jak zwykle. Znaczy, chciała, bym nie zauważyła, czyli w trzecim a nie pierwszym zdaniu.

Arianna poszła więc do Grażyny, zgarniając Maję oraz Kajetana.

* G: pani kapitan? (zajmuje się logistyką inwentaryzacją itp.)
* A: słyszałam od Mai że interesowałaś się duchami?
* G: ja? (nieszczerość)
* A: nie zajmowałabym Twojego czasu gdyby to było nieważne.
* G: ...może. Czy pani kapitan... eeyyy myśli... o niestabilnej eferydzie związanej z tym że tyle osób się pozabijało?
* A: na przykład. Mamy problem z młodymi, bo Kajetan bardzo ich nastraszył historiami co tu się musiało wydarzyć i musimy ich uspokoić
* G: ma sens, pani kapitan. To zmniejsza szansę na efemerydę noktiańską. Bo jakby się bali, to ją wezwą, nie?
* K: (blankface, nie pomyślał). M: (blankface, nie pomyślała)
* G: da się zrobić. Uspokajamy... środkami?
* A: myślałam o tym że można dla nich zainscenizować że pozbywasz się efemerydy
* G: JA jako aktorka? XD
* K: Grażyno, jest w Tobie większy potencjał niż myślisz!
* G: To zrobimy prawdziwy egzorcyzm. Taki, co jeśli coś jest nie tak, to faktycznie to wyłączymy lub ograniczymy. Tak będzie lepiej. Pasuje?
* A: Brzmi świetnie, nie znam się na puryfikacji.
* G: Ja też nie (uśmiech) Ale znam się na duchach. W sensie, duchy, czyli... efemeryczne echo emocjonalne, powtórka z rozrywki.

Kompetencja której Arianna nie wiedziała że potrzebuje.

Grażyna rozpoczęła research, zaczęła zajmować się wszystkim czym zająć się należy by rozwiązać problem potencjalnego ducha. Widać, że się cieszy. Chyba ma nadzieję że tam jest duch. Włącza w ten proces innych. 10 minut później:

* G: pani kapitan. Naprawdę?
* A: nie wiem o co chodzi.
* G: takie dziwne szaty? My nie mamy czegoś takiego (offscreen: M: mamy fabrykator, Arnulf wygeneruje)
* A: Maju, czy to jest Twój projekt
* M: nie, pani kapitan. Na podstawie klasycznego stroju egzorcystki z pewnymi... hiperbolizacjami zrobionymi przez Kajetana.
* A: (oczom Arianny ukazał się STRÓJ.)

Kajetan zrobił coś chorego i wprawnego. Daria "każdy egzorcysta ma swój styl". Grażyna "ja mam inny styl". Daria "bo nie robiłaś egzorcyzmów".

Tr (niepełny) Z (Grażyna jest zdecentrowana) +3:

* XX: Grażyna jest wściekła na KAJETANA. I nie będzie mówić o przeszłości i duchach.
    * "Nie tylko odpędzałam duchy ale je przywoływałam! Jestem lepszą egzorcystką niż Ty i wystarczą gumiaki i kamizelka!"

.

* D: nie chodzi o odpędzanie duchów a o wrażenie na dzieciakach.
* (+3V+1Vr bo Arianna przejmuje konflikt): A: "Żeby nie powstała efemeryda wrażenie na dzieciakach, prawdziwy egzorcysta się zajmuje (wskazanie praktyczności w stroju i co dodać)" X: Grażyna bardzo nie chce. Dla niej to kwestia... powagi. TRADYCJI.
* Daria to założy. Grażyna ma XD. Grażyna: "A Ty nie możesz pajacować a ja zrobię egzorcyzm?"
* Arianna: "Z obecnych tu osób może zrobić to tylko Maja i może to przedstawić. Ty jesteś najbardziej kompetentna i odpowiedzieć na ich pytania"
* K: na pewno chcemy odpowiadać na ich pytania, jak ja odpowiadałem nie skończyło się dobrze
* G: jak otwierasz USTA to jest źle.
* A: odpowiemy i nie będą się bać. Bo Grażyna jest egzorcystką i się zna.

Grażyna AKCEPTUJE to. Ona to założy. Bardzo niechętnie. Zrobi to. (ALE ten rytuał trafi do bazy pokładowej "dziwne rzeczy na Flarze" - będzie nowy mem o Grażynie.)

Jak już zaczęły się działania, 2h później, Bolza -> Arianna.

* B: Alarm. Dokładniej, dostaliśmy SOS. Chcę Cię na mostku Loricatus.
* A: Idę.

10 minut później, Kurzmin, Bolza, Arianna, Kalia Szantik są na mostku. Komunikacja ze strony niewielkiej szybkiej korwety. IFF "Biura HR" to jest frakcja tutejsza, tak jak Nonarion. Planetoida TKO-2292. Widocznie HR popłaca patrząc na jakość jednostki.

* L: Tu Lana z korwety HR-131, zgłoście się! Proszę!
* B: Salazar Bolza. Głównodowodzący tą stacją.
* L: Biura HR potrzebują Waszej pomocy! 
* B: Na czym polega problem.
* L: Potwór kosmiczny! Anomalia! Coś! Nie ktoś! (dziewczyna jest spanikowana)
* B: (cicho) wysłali prawidłową agentkę. Sam bym taką wysłał. Popatrzcie na jej mowę ciała. (jest spanikowana)
* B: kapitan Verlen, proszę kontynuować.

Daria wie o Biurach HR:

* kompleks więzienny, HRowy, resocjalizacyjny, edukacyjny i ogólnie "zajmujący się zasobami ludzkimi".
* to jest dobrze broniony kompleks. Sensowny. 
* ma sławę
    * są pewne podejrzenia na czym polega reedukacja - więźniowie którzy mieli karę śmierci czasem wracają. Bardzo pracowici i grzeczni.
    * tak samo bogatsze grupy wysyłają na edukację, dzieciaki itp - i tam się uczą
* Lana która gada jest za młoda by Daria ją znała. Najpewniej - znając biura HR - to jest jakaś trainee ale kompetentna.
* HR nie robi operacji ofensywnych; oni raczej są usługą do której się idzie. Nie jest jak rzeka tylko jak fontanna.
* Chcesz zrobić świetną imprezę? Biura HR Ci to zapewnią. Chóry, gladiatorzy.
* "Pomożemy Twoim ludziom stać się lepszymi zasobami".

(Kurzmin "nie dogadujemy się z HRem". Bolza SPOJRZAŁ.)

Lana szybko powiedziała co wie:

* Jest straszna anomalia kosmiczna, potwór, który zniszczył statek transportowy HRu. Leci prosto do biur HR.
* To jest niewielki potwór. HR wysłał "zaprzyjaźnionych" najemników, 3 korwety. Żadna nie przetrwała. Potwór jest wielkości korwety.
    * Ci najemnicy nie byli imponujący; każda z tych korwet była klasy Flary jak chodzi o walkę, ale Flara nie służy do walki.
* Potwór leci bezpośrednio do nich. Ignoruje inne cele.

.

* Bolza: "Loricatus jest nieaktywna. Kurzmin, Verlen, czy Athamarein i Flara są w stanie podejść do tego i jak co się wycofać?"
* Kurzmin: "Athamarein tak."
* Arianna: "Zbadanie sytuacji nie będzie nas nic kosztować. Flara na tyłach jako wsparcie."

Plan - przeczytać pamięć Lany, odczytać co tam jest. Łagodny Szczepan Myrczek, poczciwy advancer ma poczytać pamięć Lany i określić czego ona nie wie. A Myrczek jest dobry w zarządzaniu złożonością (model miasta) - on potrafi bardzo dużo _kojarzyć_.

Tr Z (Władawiec jako uspokojenie i będzie dobrze) M (Myrczek zwycięzcą) +3 +3Ob:

* Vm: ekspresja Myrczka: interaktywna makieta. Perfekcyjna Wizualizacja. (Myrczek jest mentalistą do CZYTANIA i iluzjonistą / projektorem)
    * szept: "powiedzieli, że ktoś ich pomści lub uratuje..."
    * szept: "oswojona anomalia kosmiczna. Nie pokonają defensyw Biur..."
    * sygnał ze środka Anomalii Kolapsu, zbliża się
    * to się porusza szybko, ale nie bardzo szybko
    * krzyk: "nie złapaliście nas wszystkich!!!"
    * potwierdzenie: to leci do Biur. To chce zniszczyć Biura.
    * dowódca Lany: "poproś o pomoc Orbiter. Nie wiem czy to umiemy odeprzeć." Lana: "ale co to jest?" dowódca: "nie wiem." (ogromny stres)
    * dowódca Lany: "jeśli będą Cię chcieli jako zapłatę, jesteś zapłatą." Lana: ORBITER? (strach) dowódca: "ciesz się że nie piraci..."
    * Lana widziała kątem oka sygnał. Jedno płonące oko z którego płynie krew. Na ludzkiej twarzy. Gdy było połączenie.

.

* Bolza: zregeneruję Loricatus i przylecę Wam z pomocą.
* Grażyna: nierozsądne, komodorze.
* Bolza SPOJRZAŁ.
* Grażyna: no bo efemeryda, tu na miejscu. Loricatus zapewnia, że tu się nie pojawi. Chyba.
* Bolza: Verlen, Kurzmin, sprawdźcie to. Ale jeśli to jest zbyt groźne, wycofajcie się. Ja przygotuję Loricatus na wszelki wypadek, ale nie opuszczę tego terenu.

Czas odwiedzić potwora. Acz zrobiło się chłodniej widząc to echo...

Lana zostaje w bazie. Bolza zarekwirował korwetę HR-131 i zrobił jej refuel. Wsadzili w to szybkiego i dobrego pilota, Miłosza (z załogi Loricatus). Daria zrobiła mu krótki tutorial systemów HR-131 i Miłosz poleciał.

Tr Z +3 +3Or:

* Or: HR-131 jest niezłą korwetą, ale nie jest korwetą Orbitera. Miłosz ją przeciążył i potrzebuje podjęcia. A DARIA MÓWIŁA.
* V: Miłosz dotarł do wraków korwet zanim Athamarein i Flara weszły w kontakt z wrogiem.
* X: Nikt nie przeżył. Anomalia strzelała by zabić.
* V: Miłosz, komunikat:
    * M: dowodzenie, zgłoś się?
    * K: jesteśmy
    * M: uważajcie. Przeciwnik ma działo strumieniowe albo jego odpowiednik.
    * K: na korwecie? To nie ma sensu.
    * M: to działo strumieniowe ma moc lancy plazmowej. Jest... potężne. Pogrzebię w czujnikach.
    * Or: Miłosz grzebał w czujnikach. JEST RANNY. Musiał się wycofać.
        * wraki zostały częściowo zanomalizowane; dlatego to się stało. Miłosz poczuł NIENAWIŚĆ i CIERPIENIE. Ale nie Esuriit.
        * udało się nawiązać kontakt z anomalną jednostką. Jednostka posiada pilota. Ma kobiecy głos.
* XOr: (Miłosza porwą i wezmą do okupu) Miłosz dał radę przesłać sygnał (jak wygląda Sabrina).

3 godziny od momentu wystartowania patrząc na trajektorię Athamarein oraz Flara zbliżyły się do przeciwnika. Jednostka pojawiła się na radarach Athamarein. Posiada IFF. Jest Skażone. Nie leci regularnie. Maja zabrała się za dekodowanie.

Sygnał od Miłosza:

Kokpit. W statku płonie. Jest pożar. Ale pilot nie czuje tego ognia. To kobieta, na oko koło 40. Widać, że w jej prawe oko jest coś wbite. Z policzka płynie krew. Daria szuka lokalnych legend:

Tr Z (Arianna) +3:

* X: Arianna nigdy tego nie czytała bo nie musiała. Kurzmin też nie. Brak lokalnej wiedzy odnośnie tej legendy ze strony Orbitera.
* V: Legenda głosi o jednostce noktiańskiej, anomalnej jednostce, która zabiera Ci wszystko. Jednostce, która pomści Cię ale zabierze Ci duszę. Nox Ignis. Płonąca Noc. A pilotem Nox Ignis zawsze jest Elwira. Demoniczna TAI. Szpikulec to neurosprzężenie TAI i pilota. Żeby uruchomić Nox Ignis musisz wbić sobie to w oko.

Daria przygotowuje system ochronny dla Flary z pomocą Mai. Cel - zabezpieczyć Flarę przed możliwością połączenia się z Nox Ignis, by komunikat nie zrobił krzywdy. To dojrzała anomalia, więc... nie ryzykujemy tej Persefony. Daria składa, Arianna zapewnia ekrany ochronne technomantyczne.

Tr Z (Daria wie co robi) M (Arianna) +3Ob +3:

* V: mamy ekrany technomantyczne
* Vm: Flara się obudziła. Tymczasowo. Całe morale załogi, cała wiara załogi stała się pancerzem komunikacji. Wiara załogi w Ariannę i trochę w Orbiter chroni Flarę przed Nox Ignis. Bo to tylko anomalia. A Arianna jest ich zaufanym kapitanem.

Maja nawiązuje połączenie. Arianna widzi pilota. Pilot jest w gorszym stanie niż widział Miłosz. Jest słabsza, bardziej przezroczysta, więcej krwi.

* NI: "Adragain... lecę po Ciebie... Elwira Cię uratuje... Destructor Animarum... Elwira Cię uratuje... Niebo spłonie..." (ten lot nie służy jej na dobre)
* Kurzmin -> Arianna: "ona _fatalnie_ wygląda. Ile ona przeżyje? Parę godzin?"
* Daria: "dość"
* Arianna: "Czy Nox Ignis nie utrzyma jej przy życiu lub nie zrealizuje celu..."
* Kurzmin: "Czy kiedykolwiek NI było tak daleko od Anomalii? W sensie... ona wygląda jakby miała umrzeć zaraz."
* Daria: "I pewnie tak jest"
* Kurzmin: "To możemy z nią wejść w starcie. Nie po to by wygrać, a spowolnić. Aż pilot umrze."
* Daria: "Martwy pilot nie znaczy że NI nie zrealizuje celu."
* Kurzmin: "A kiedyś to zrobił?"

Kurzmin chce związać Nox Ignis walką. Arianna ma inny plan - dowiadujecie się od Nox Ignis więcej o Andragainie i przechwytujecie go z Biur HR.

Z perspektywy Arianny, to rozmowa dla Eleny: zawzięta, przewróciłaby świat dla zadania, troszczy się. Ale też dla Grażyny, ona się zna na duchach.

Czyli rozmowa to: Arianna testuje na Elenie i konsultuje z Grażyną by przejść przez Nox Ignis i dotrzeć do pilota a nie tylko Elwiry.

* A+D: chodźmy pod kątem rozmowy z duchem. Jak możemy Ci pomóc. Dowiedzieć się o osobie i czy ma szanse żyć. Wyciągnąć okoliczności tego co się stało. Jeśli porwani to bohaterka się wydostała w kiepskich warunkach patrząc że jest w anomalnym statku z Anomalii Kolapsu... Elwira jest przywoływana czy jest w Anomalii? Legenda mówi, że wlatujesz do Anomalii Kolapsu by Cię opętała. Musimy wiedzieć o pilotce. Który wariant historii.

Daria z Nox Ignis, Arianna z Biurem HR.

* Daria: tu Astralna Flara; widzimy że masz problemy ze statkiem. Jakiej pomocy potrzebujesz?
* Nox Ignis: zejdź mi z drogi. (krew z ust)
* Daria: możemy Ci pomóc z uratowaniem Adragaina.
* Nox Ignis: Adragain... 
* Sabrina: (pojawiła się iskra życia w zdrowym oku) odsuń... się... nie chcę... strzelać... w Ciebie...
* Daria: nie stoimy Ci na drodze, czego chcesz
* Sabrina: mój mąż... Destructor Animarum... 
* Daria: co to jest?
* Sabrina: zabiera duszę. Ale Elwira mi pomogła odzyskać duszę. Oddałam ją jej. Lepiej jej niż im.
* Daria: a Adragain?
* Sabrina: znajdę go. Albo... (zgasła)
* Nox Ignis: odsuń się.
* Daria: czy Adragain żyje?
* Nox Ignis: życie nie ma znaczenia. Zemsta ma znaczenie. Wolność ma znaczenie.
* Daria: a jeśli Adragain będzie wolny?
* Nox Ignis: życie nie ma znaczenia. Zemsta ma znaczenie...
* Sabrina: ...nie... musi być wolny... żywy...
* Nox Ignis: nie ma wolności póki żyje
* Sabrina: ...szczęście
* Nox Ignis: Destructor Animarum, pamiętasz? Zaopiekuję się Wami...

Arianna komunikuje się z Biurami HR. Odbiera sympatyczna młoda dziewczyna, łóżko numer 4. Arianna szybko dostała przełożonego. Arianna widzi 50-letniego bardzo zadbanego mężczyznę w pancerzu bojowym. Świetnie wygląda. Aleksy (X).

* X: Mam potrzebne informacje. Fregata "Lacratin", załoga 15 osób. Między innymi Adragain Ferrias. 3 lata temu. Został reedukowany i odsprzedany dalej. Jest szczęśliwy jako górnik kosmiczny.
* A: Pozostali członkowie jego załogi? Żona?
* X: (nazwiska i zdjęcia). Tak, oczywiście. Żona też została reedukowana; trafiła do salvagerów w Anomalii. Była szczęśliwa. Zapomniała o mężu jak się robi zgodnie z procedurą.
* X: To była jednostka noktiańska, terroryści; nie chcieli kończyć wojny. Zagrażali ekonomicznym interesom w okolicy, więc była za nich nagroda - nie wyznaczona przez nas - i zostali odpowiednio reedukowani by stać się wartościowymi członkami społeczności.
* A: Możecie odzyskać Adragaina?
* X: Tak, jesteśmy w stanie go odkupić.
* A: Ile czasu to zajmie?
* X: Legalnie, kilka dni. Jak się pospieszymy. Nielegalnie i piracko? 5-6 godzin. Ale wiem, gdzie się znajdują. (podał lokalizację)

Arianna WIE że może kolesia przycisnąć i poznać prawdę. Po raz pierwszy. Zasypać go pytaniami. I Aleksy odpowie - a jest odpowiednio wysoko. Pytania dookoła tej dwójki i innych członków załogi by Arianna nie wiedziała co jest konieczne.

Tr Z (desperacja) +3:

* X: nie powie niczego co bardzo skrzywdzi Biura i co Orbiter może _użyć_
* V: Destructor Animarum: proces reedukacji przez przeprogramowanie mózgu. Odpowiednia edycja pamięci. I też przepięcie niektórych ścieżek w mózgu fizycznie. Zmienia osobowość. Niszczą osobę i robią na nowo. Nie jest to bardzo tania operacja. Ale pozwala np. na to by ktoś kto "nigdy nie powiem" powiedział wszystko. To noktiański termin.
* Vz: Arianna doszła do czegoś. Destructor Animarum jest dowodem na to, że Biura HR mają wyższy poziom technomagiczny. To jest zdecydowanie technologia w stylu Aureliona. To typowe zaawansowane narzędzie Syndykatu. Biura HR nie są sprzymierzone z Syndykatem a przynajmniej o tym Aleksy nie wie.

Plan C: Kurzmin daje pełną moc silników Athamarein, odrzuca większość sprzętu by być szybszym i kupić Andragaina. A raczej "Kurzmin do górników: mam rakiety. Czy macie coś przeciw temu bym połączył kupionego przez Was noktianina z jego żoną? Tak myślałem."

Ex (bo trzeba nacisnąć silnik) Z (Athamarein jest świetnej konstrukcji) +3 + 5Or (uszkodzenia bo przekroczyliśmy parametry):

* X: "Orbiter to Orbiter - jak u Lodowca" (górnik cicho "nie chcę służyć po śmierci").
* V: Athamarein pobiła rekordy. Leciała na 120% obciążenia silników. I się nie rozpadła. Inżynier pokładowy płakał gdy statek leciał.

Nox Ignis leci w kierunku Biur. Mamy dostęp do Athamarerin gdzie jest mąż. Kurzmin -> Arianna.

* K: kapitan Verlen, ten koleś jest _creepy_. Uśmiechnięty, wszystko mu pasuje, chce pracować... (Kurzmin ma aktywne obrzydzenie). _On nie ma duszy_. Nie ma osoby.
* A: nic z tym nie robimy chcemy ratować osoby w Biurze
* K: taaaak... tu wolałem chyba podejście Lodowca...
* A: Lodowiec rozstrzelałby tego terrorystę.
* K: Lodowiec pozwoliłby spalić Biura.
* A: żeby 5 managerów co się tym zajmują uciekło a pracownicy co dostają pieniądze byliby spaleni?
* K: jak dostają pieniądze to są winni
* A: może nie ma miejsc? Pogadaj z Darią, zobaczysz jak przeżyć do końca miesiąca.

Daria próbuje przekonać Nox Ignis by ratować męża a nie palić Biura. Sabrina - przy komunikacji - wygląda JESZCZE słabiej. Cała komunikacja idzie przez Flarę, ale można ją przekanalizować by wyglądało jak przez Athamarein. Flara się odsuwa i Daria nawiązuje połączenie, Grażyna jest zasobem.

* D: Sabrina. (i puściła połączenie od Adragaina który powiedział to co miał powiedzieć. Kurzmin kazał powiedzieć co należy i czuje się brudny. Arianna widzi wyznanie miłości by Kurzmin i też nie czuje się czysta. Kurzmin sucks at miłość.)

Tr Z (Grażyna) +3Og (FURIA / Sabrina) + 5Or (Elwira / zemsta) + 5Ob (Sabrina / ratunek) +3:

* Or: Sabrina _ożyła_. Jej włosy stanęły w ogniu. Płomiennowłosa Elwira. KRZYK BANSHEE. Nox Ignis duża prędkość, manewry i wiązka z działa strumieniowego w Athamarein. Pudło. Athamerin się oddala. Elwira odwraca się do Biur. "Destructor Animarum MUSI być zniszczony"
    * Arianna: "pozwolisz mu by żył w takim stanie, nie uwolnisz go na sam koniec"
* Or: Nox Ignis się rozpaliło energią. Leci tam.
    * Elwira: "jego już nie ma. Nie ma duszy. Tam są dusze które zapłacą. Które to zrobiły. Zabiorę im to co oni zabrali!" (-3Og)
    * Maja patrzy na Elenę. Na Elwirę. Na Elenę. Nic nie powiedziała.
    * Arianna bierze nagrania jak był łamany. Jaki był. Przypomnienie. I pokazanie, że jest "uwięziony" w swoim ciele. Jest OGRANICZONY jak TAI. +(3Ob)
* Vr: 
    * jest konflikt między Elwirą i Sabriną. Elwira zamamrotała "w końcu ON jest celem... tak... mało..." ale Nox Ignis leci w kierunku na Athamarein. (+3Xr)
* V: 
    * Athamarein leci dobrze. Nie za szybko, tak, by Elwira _miała nadzieję_. Athamarein wyprowadził Elwirę 15-20 minut lotu.
    * puszczony film Lodowca na Nox Ignis, by opóźnić zorientowanie się w co gramy. (+3Ow: czysta konfuzja i chęć zniszczenia Orbitera)
* V: 
    * Elwira leci za Athamarein, w kierunku na bazę w Strefie Duchów. Słyszycie szept "śmierć miała być wybawieniem!" i po chwili "wszystko spłonie... noc spłonie..." i uśmiech Sabriny że zginie NAPRAWDĘ.
* Ob: 
    * Sabrina się skupiła na Flarze. "Orbiter. Praprzyczyna WSZYSTKIEGO. Wojna się nie skończy nigdy!". Nox Ignis leci na FLARĘ. "Oddaj go albo spłoń!". Elena w DŁUGĄ! (+2Vg)
    * Athamarein wyrzuca manekin w kosmos. (+2Vg). Nox Ignis zmienia kurs. Spala manekin a potem kolejny krzyk banshee. +2Xr.
* Vz:
    * Grażyna doprowadziła do PĘTLI LOGICZNEJ. Nox Ignis spaliła manekin i leci na Flarę, Athamarein wyrzuca kolejny manekin i są te same słowa, więc Nox Ignis robi dokładnie to samo. I tak trzy razy. PRZEZ MOMENT GRAŻYNA JEST KOMODOREM DWÓCH STATKÓW. Arianna pęcznieje z dumy. Grażynka tak urosła.
* Ob:
    * Sabrina rezygnuje z Flary. Nox Ignis świeci jeszcze jaśniej. Płomienie w kokpicie są... niemożliwe. Sabrina krzyczy "ODDAJCIE MI GO!"
* V: 
    * Nox Ignis zaczyna gasnąć. Sabrina krzyczy w agonii. Nie tylko fizycznej. Duchowej. Arianna widzi, że Nox Ignis nie doleci do Biur. To jest ten krzyk gdy nie masz już strun głosowych.
* X: Sabrina SPŁONĘŁA w agonii. Wnętrze Nox Ignis ma tylko popiół. Nox Ignis się odwraca i odlatuje w kierunku na Anomalię Kolapsu jednolitym, spokojnym lotem. Wolniej niż Flara.
    * Kurzmin: "chyba mam uszkodzony silnik."
    * Elena (cicho): "serio..? Po czymś TAKIM?"
    * Maja spojrzała na Elenę. Na Nox Ignis. I nie powiedziała ani słowa.
    * Kurzmin: "na szczęście to tylko niewielka awaria; mam 80% mocy bezpiecznej. Lecę na 70, będzie lepiej..."
    * Kurzmin: "niestety Adragain... zginął. Nie pytaj." (coup de grace)
    * Arianna: "nie pytam" (myślała o czymś podobnym)
    * Grażyna, cicho: "dzięki temu nie pojawi się duch"

## Streszczenie

Niestety, Kircznik (lekarz) przestraszył Sargona i młodych infiltratorów Lodowcem, Bolzą i ogólnie duchami noktiańskimi. Okazało się, że Grażyna jest ekspertem od duchów i jest w stanie zrobić egzorcyzm. Niedługo potem (i chwilę przed egzorcyzmem) pojawiła się szybka korweta z Biur HR i poprosili o pomoc w odparciu Anomalii Kosmicznej. Myrczek sprawdził jej pamięć i Athamarein i Flara poleciały na starcie z Nox Ignis. Nawiązały kontakt z Biurami HR, przechwyciły biednego męża pilotki Nox Ignis i poznały prawdę o Destructor Animarum i wpływie Syndykatu Aureliona na Biura HR. Kurzmin pobił rekordy Athamarein, Arianna skutecznie zaplanowała wypalenie pilotki Nox Ignis przez własny statek. Tylko advancer Miłosz (badający ślady Nox Ignis) jest MIA.

## Progresja

* Grażyna Burgacz: jest wściekła na KAJETANA KIRCZNIKA. I nie będzie mówić o przeszłości i duchach. Na pewno nie. I filmik z jej egzorcyzmu trafi do bazy memów Astralnej Flary "Grażyna 'Paw' Burgacz".
* Leszek Kurzmin: solidny opierdol od kmdr Salazara Bolzy za śmierć Adragaina Ferriasa. Emocje zwyciężyły nad możliwością zbadania jak działa Destructor Animarum.
* OO Athamarein: uszkodzona; pełna moc silników to 70%. Przesterowana.

### Frakcji

* Biura HR: wykorzystują Destructor Animarum; proces reedukacji przez przeprogramowanie mózgu. Odpowiednia edycja pamięci. I też przepięcie niektórych ścieżek w mózgu fizycznie. Zmienia osobowość. Niszczą osobę i robią na nowo. Powiązanie z Syndykatem Aureliona.

## Zasługi

* Arianna Verlen: ku jej wielkiemu zdziwieniu przerażenie młodych infiltratorów przez jej medyka doprowadziło ją do tego że racjonalna Grażyna jest ekspertem od duchów. Kazała zdobyć Adragaina i używając wiedzy Grażyny wypaliła pilota Nox Ignis (Sabrinę).
* Daria Czarnewik: lokalny ekspert od Biur HR; wie co to jest. Skutecznie ochroniła Flarę i Athamarein przed integracją psychotroniczną z Nox Ignis budując odpowiednie ekrany z pomocą Mai. 
* Leszek Kurzmin: pewny umiejętności Athamarein; chciał walczyć z Nox Ignis, ale pod wpływem pomysłów Arianny zmienił Athamarein w super-szybką jednostkę kurierską i wymanewrował Nox Ignis nie walcząc z nią bezpośrednio. CREEPED OUT przez Destructor Animarum. Zabił Adragaina, nie chciał widzieć tego upiornego pustego uśmiechu.
* Kajetan Kircznik: nieco zbyt skutecznie przestraszył młodych infiltratorów; noktianie się boją Orbitera ale teraz też duchów. 
* Salazar Bolza: zabronił szperania na Dominie Lucis; kazał Ariannie uspokoić młodych infiltratorów których Kircznik przestraszył. Świetnie rozpracował Lanę (agentkę Biur HR) i same Biura, nie dał się ich podstępom i słodkiej agentce. 
* Elena Verlen: bierze się każdej możliwej misji by tylko NIE rozmawiać z Arianną odnośnie tego co się działo ostatnio ;-)
* Maja Samszar: o dziwo nie zna się na duchach; tym się nie interesowała. Ale wie, że Grażyna tak. Za to pomaga Darii złożyć ekrany ochronne chroniące je przed banshee Nox Ignis.
* Grażyna Burgacz: okazała się być absolutnym ekspertem od duchów. Przeprowadzi 'egzorcyzm' by uspokoić młodych infiltratorów. Pomogła też wprowadzić Nox Ignis w pętle mentalne, by szybciej wypalić pilotkę.
* Lana Mirkinin: agentka Biur HR (ENCAO:  --0+0 |Skromna;;Zgodna z najnowszą modą| VALS: Self-direction, Face, Hedonism |DRIVE: Biura Forever). Szybki pilot, ładna i elegancka dziewczyna i potencjalny komponent handlowy by Orbiter pomógł Biurom HR przetrwać Nox Ignis. 
* Władawiec Diakon: osoba uspokajająca Lanę, by dało się jej przeczytać pamięć i poznać prawdę odnośnie dziwnej anomalicznej jednostki (Nox Ignis)
* Szczepan Myrczek: łagodny, poczciwy advancer przeczytał pamięć Lany i określił czego ona nie wie wrt Nox Ignis. Jest dobry w zarządzaniu złożonością (model miasta).
* Miłosz Klinek: świetny pilot korwety z załogi Loricatus i dobry advancer; dotarł do śladów zniszczeń po Nox Ignis i przekazał Athamarein i Flarze informacje. Po czym padł. Ranny i przechwycony przez jakąś siłę.
* Adragain Ferrias: mąż Sabriny; noktianin; złapany przez Biura HR i reedukowany. Został kosmicznym górnikiem; wyciągnięty przez Orbiter został 'przynętą' na Nox Ignis. KIA, zabity przez ludzi Kurzmina by się nie męczyć.
* Sabrina Ferrias: żona Adragaina; noktianka; złapana przez Biura HR i reedukowana. Uwolniona przez interakcję z Nox Ignis, stała się pilotką Nox Ignis by uratować męża i zginęła w płomieniach skonfliktowana z Elwirą d'NoxIgnis. KIA.
* Aleksy Sartaran: jeden z wysokich oficerów Biur HR; współpracuje z Orbiterem by uchronić Biura przed Nox Ignis. Przekazał informację Ariannie o Destructor Animarum i oddał Bolzie Lanę Mirkinin jako zapłatę za operację.
* Persefona d'Loricatus: silniejsza psychotronicznie niż typowa Persefona; posłużyła do połączenia z bazą na Planetoidzie Lodowca by ją uruchomić póki nie przybędzie właściwa TAI. 
* AK Nox Ignis: skonfliktowana z pilotką (Sabriną). Chce zniszczyć Biura HR, ale chce też uratować Adragaina (męża Sabriny). Przez sprzeczne instynkty ta Anomalia nie była w stanie osiągnąć żadnego z celów. Wróciła do Anomalii Kolapsu po zjedzeniu Sabriny.
* OO Loricatus: wyłączona z akcji; jej Persefona służy do utrzymania bazy na Planetoidzie Lodowca.
* OO Athamarein: przekroczyła moc silników (150%), ale złapała Adragaina (brata pilotki Nox Ignis) i wróciła manewrować przeciw Nox Ignis. Uległa lekkim uszkodzeniom silników (max. 70% pełnej mocy)
* OO Astralna Flara: manewruje z Athamarein, by Nox Ignis nie była w stanie dogonić żadnej z jednostek. Uczestniczyła w operacji 'wypalenia pilotki Nox Ignis'.

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Obłok Lirański
                1. Anomalia Kolapsu, orbita
                    1. Strefa Upiorów Orbitera
                        1. Planetoida Lodowca: 
                    1. Strefa Biur HR: zbiór planetoid pod kontrolą dość bezwzględnej organizacji 'Human Resources'. Kompleks więzienny, HRowy, resocjalizacyjny, edukacyjny i ogólnie "zajmujący się zasobami ludzkimi".

## Czas

* Opóźnienie: 3
* Dni: 3

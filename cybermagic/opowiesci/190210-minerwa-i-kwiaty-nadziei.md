---
layout: cybermagic-konspekt
title: "Minerwa i Kwiaty Nadziei"
threads: nemesis-pieknotki
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [190206 - Nie da się odrzucić mocy](190206-nie-da-sie-odrzucic-mocy)

### Chronologiczna

* [190206 - Nie da się odrzucić mocy](190206-nie-da-sie-odrzucic-mocy)

## Projektowanie sesji

### Pytania sesji

* .

### Struktura sesji: Frakcje

* Kornel: przekonać Minerwę, by pojechała z nim
* Wiktor: osłonić Olgę przed Myszami i ludźmi

### Dark Future

1. Kornel dostaje od Minerwy to, czego chce - wsparcie w Black Technology.
2. Kornel daje radę przekonać Minerwę, by ta pojechała z nim tworzyć lepszy świat.
3. Satarail destabilizuje teren Czerwonych Myszy, przesuwając go w totalny chaos

### Pierwsza Scena

Nieużytki Staszka. Małe community dla Minerwy stworzone przez Kornela.

## Potencjalne pytania historyczne

* brak

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Misja właściwa

**Scena**: (17:26)

Pięknotka jest ukrytym świadkiem dziwnego spotkania. Na Nieużytkach Staszka, w jednym z miejsc "publicznych" Minerwa i kilkanaście osób. Rozmawiają o dziwnych rzeczach. Minerwa wyraźnie dobrze się bawi. To, co Pięknotkę martwi to to, że ci ludzie są kultystami Kornela. A Minerwa doskonale się bawi i czuje się "jak w domu".

Pięknotka podeszła do Minerwy, porozmawiać. Minerwa przedstawiła Pięknotkę. Ogólnie, niewiele się zmieniło. Pięknotka chce dojść do tego, co tu się dzieje - ich celem wyraźnie jest 'ensnare'. Minerwa ma czuć się świetnie, ma chcieć tu być, w towarzystwie takich ludzi i magów. Oni nie zachowują się, jakby rozpoznawali Pięknotkę. Pięknotka zatem przeprosiła i się oddaliła. Musi ściągnać coś z bagien by rozpędzić towarzystwo zanim stanie się coś złego (jej zdaniem) na linii Kornel - Minerwa.

(Tr:7,3,6=S). Pięknotce udało się ściągnąć parę wężolisów; udało się jej całkowicie rozgonić towarzystwo. Minerwa próbowała ich bronić, ale jedyne co dała radę to ściągnąć ogień na siebie i zwiać na drzewo. Wężolisy w końcu poszły sobie. Impreza zakończona.

Zdeptana Minerwa wróciła do domu. Pięknotka szybko zlokalizowała Kornela - znalazła go w hotelu w Zaczęstwie.

Pięknotka poszła do Erwina oraz zdecydowała się ściągnać akta o wydarzeniu w magazynach które przedstawiają Kornela w jak najgorszym świetle. Erwin się z nimi zapoznał i jest gotowy (acz Pięknotka nie powinna mu ich dać).

**Scena**: (18:01)

Osiedle Ptasie, mieszkanie Minerwy. Pięknotka, Erwin i Minerwa.

Pięknotka z Erwinem próbują przekonać Minerwę, że Kornel jest niebezpieczny. Akcja przerzutowa, wiły, przez niego aktywował się Saitaer. Pięknotka stara się pokazać Minerwie inne oblicze Kornela. Minerwa zauważa, że ona SAMA jest potworem. Zabiła. Żyje dzięki Saitaerowi. Nikt o nią nie dba poza Pięknotką i może trochę Erwinem i Tymonem. Pięknotka chce o kategorię oddalić Minerwę od Kornela. Niestety, Pięknotka nie ma konkretnych dowodów - ma tylko dane od Moktara. Pięknotka i Erwin zaczęli ją przekonywać. (TrZ:8,3,6=S). Udało się. Minerwa nie zamierza robić niczego "głupiego" bez poinformowania Pięknotki oraz ma zamiar zadać parę pytań Kornelowi...

To była ciężka przeprawa.

Pięknotka umówiła się z Kasjopeą Maus na rynku w Zaczęstwie. Ta przyszła, z przyjemnością. Pięknotka przedstawiła Kasjopei historię tak, by była jak najbardziej smakowita. O Kornelu, który chce zostać bogiem i zakłada kult z ludzi. Poluje na magów, którzy mogą być jego narzędziami. Pięknotka dorzuciła jeszcze Kasjopei dowody od Moktara. (Tp:9,3,3=SS). Kasjopea zażądała tego, by Pięknotka przestała jej przeszkadzać przy próbie zdobycia Kryształu od Minerwy. Pięknotka odmowiła. Więc Kasjopea zaproponowała drugi wariant - wywiad z Wiktorem Satarailem. Na jego terenie, ona zadaje pytania.

Pięknotka niechętnie, ale zgadza się na warunki Kasjopei. Ale jeśli cokolwiek się stanie Minerwie, jakakolwiek krzywda - wizerunkowa, mentalna, emocjonalna... - Pięknotka dorwie Kasjopeę i zmusi ją do zapłaty. Kasjopea się zgodziła. Ok. Pomoże Pięknotce i dostanie to czego chce od Minerwy.

Czyli Kasjopea zrobi co może, by splugawić wizerunek Kornela i przeszkodzić mu w jakichkolwiek działaniach na tym terenie. I nikt nie wie o obecności i działaniach Pięknotki. Kasjopea znowu potwierdziła.

Pięknotka zdecydowała się też na śledzenie kultystów. Sama tego nie zrobi, ale Erwin zna kogoś kto może zrobić to dla niej. (10,3,4=S). Erwin doszedł do tego, że Kornel ma niedaleko bazę. To takie pole namiotowe. Zwykle on i kultyści są tam. I coś tam jest, coś magicznego, coś dziwnego. Jakiś jego eksperyment. Jest to na terenie Zaczęstwa. Teoretycznie Tymon może być zainteresowany...

**Scena**: (19:00)

Minerwa, komunikacja hipernetowa z Pięknotką. Powiedziała jej, że Kornel zaprosił ją do siebie pokazać jej coś niesamowitego. I Minerwa ma zamiar to przyjąć. Pięknotka idzie z nią? Tak, pójdzie...

Na miejscu, pole namiotowe. Kornel jest elegancki i perfekcyjnie przygotowany by doskonale się prezentować. Ludzie usługują, Kornel jest czarujący. Powiedział, że chciał Minerwie pokazać arcydzieło - kwiaty zdolne do dotknięcia duszy. Minerwa jest super zainteresowana, Pięknotka boi się ataku mentalnego. Tymon jako standby. Odpowiedni namiot jest ekranowany - to sprawia, że albo się wystawi na arcydzieło, albo nie można zobaczyć co to jest.

Pięknotka walnęła jako hardline - musi wiedzieć co tam jest, bo inaczej ani ona ani Minerwa tam nie wejdą. Minerwa zaprotestowała, ale Kornel powiedział, że wyjaśni. Powiedział, że te kwiaty odkrył, nie wyhodował. To znaczy - sam wyhodował, ale potrzebował szczególnych składników. Na jego honor, nikomu nie stanie się krzywda.

Kornel wprowadził ich do namiotu. W półmroku, kwiaty są rozświetlone. Jest ich niewiele, pęczek. Kwiaty wydają lekki, wibratyczny dźwięk. Minerwa zrobiła krok w ich kierunku, Kornel ją zatrzymał. Minerwa zaczęła śpiewać kwiatom, a one jej odśpiewały. Atak mentalny. (TrZ:SS). Pięknotka też odpłynęła. Zobaczyła Ixion. Ixion, jako żywe, szczęśliwe miejsce. Ixiońska, kochająca energia. Pięknotka jest wstrząśnięta, ale jest to nic w porównaniu z wpływem jaki to miało na Minerwę.

Kornel pod wpływem Kwiatów powiedział, że wyhodował je przy użyciu materiałów z Grazoniusza. Znalazł Grazoniusza i pobrał stamtąd próbki - a Grazoniusz nie stawiał szczególnego oporu.

Zdaniem Pięknotki, ona właśnie straciła Minerwę. Chyba, że... bo tu jest energia Saitaera. Minerwa zaczęła tłumaczyć Kornelowi rzeczy związane z tymi tzw. Kwiatami Nadziei. Pięknotka przerwała - niech Kornel uda się z nią do Pustogoru na badania. Kornel się nie zgodził. Pustogor nie jest w jego typie. Impas - ale Pięknotka przerwała impas. Poprosiła Atenę o przeskanowanie tego miejsca w poszukiwaniu ixiońskiej energii. Atena znalazła - wezwała siły z Pustogoru.

Kornel szybko się zorientował w sytuacji. Powiedział Minerwie i Pięknotce, że przybywają siły Pustogoru i musi ewakuować kwiaty. Pięknotka zaczęła ewakuować Minerwę. Kornelowi to pasuje - nie jest w stanie ewakuować się zarówno z Minerwą jak i z Kwiatami. Kornelowi udało się zrobić brudny teleport, bardzo daleki, ale terminusi (skrzydło) leci za nim...

Minerwa jest zła jak osa na terminusów Pustogorskich, ale nie na Pięknotkę. Jej zdaniem Kornel nikomu nic złego nie zrobił...

Wpływ:

* Ż: 15
* K: 2

**Sprawdzenie Torów** ():

2 Wpływu -> 50% +1 do toru. Tory:

* Wsparcie Black Technology: 8 = VXVV
* Minerwa jedzie z Kornelem: 2 = X
* Destabilizacja Żarni i Podwiertu: 6 = VXX

**Epilog** (20:40)

* Minerwa zostanie w okolicy Pustogoru i Szczelińca - tu dostanie habitat
* Terminusi Pustogorscy nie dali rady dogonić Kornela. Kornel zabunkrował się gdzieś w okolicach Skałopływu
* Minerwa pomoże Kornelowi we wszystkich obszarach jakie są potrzebne przy Ixiońskich Kwiatach Nadziei

### Wpływ na świat

| Kto           | Wolnych | Sumarycznie |
|---------------|---------|-------------|
|               |         |             |

Czyli:

* (K): Pięknotka zaczęła przygotowywać formalnie, żeby istniał ixioński habitat na terenie Zaczęstwa. Coś dla Minerwy. Dzięki temu ona nie ma po co odchodzić. Niech ma jakiś "dom" (5).

## Streszczenie

Kornel Garn próbował przekonać Minerwę do dołączenia do niego, by mu pomogła - budując jej społeczeństwo które ją akceptuje i pokazując jej Kwiaty Nadziei z Ixionu. Minerwa w 100% wpadała w to, więc Pięknotka zmontowała front przeciw Kornelowi - Kasjopea, Erwin, terminusi Pustogorscy. Skończyło się ucieczką Kornela, ale Minerwa będzie chciała mu pomóc i dzielić się z nim wiedzą. A Pięknotkę bardzo martwi to, jak Kornel radzi sobie z ixiońskimi anomaliami i swoim kultem.

## Progresja

* Minerwa Metalia: jeszcze większa złość na terminusów pustogorskich. Odepchnęli Kornela, któremu na niej zależało. Chcą ograniczyć energię ixiońską.
* Kornel Garn: persona non grata w Pustogorze i okolicach; nie polują na niego, ale nie powinien się tu pojawiać (działania Pięknotki i Kasjopei)
* Kornel Garn: zabezpieczył sobie pomoc Minerwy Metalii; Minerwa mu pomoże we wszystkich kwestiach związanych z Black Technology (jeśli zbudują kontakt)

### Frakcji

* .

## Zasługi

* Pięknotka Diakon: walczyła o duszę Minerwy z Kornelem. Udało jej się zmontować front polityczny przeciw niemu i go wywalić z okolic Pustogoru - oraz zatrzymać Minerwę dla siebie.
* Erwin Galilien: świetne wsparcie w przekonywaniu Minerwy. Dodatkowo, dzięki niemu dało się wyśledzić ruchy Kornela i jego dziwne kwiaty Nadziei.
* Minerwa Metalia: coraz bardziej zaprzyjaźniała się z Kornelem, co przerwała Pięknotka. Obudziła i rozpoznała Kwiaty Nadziei z Ixionu.
* Kornel Garn: używając wiedzy Saitaera, dostał się na Grazoniusza i wyhodował Ixiońskie Kwiaty Nadziei. Pokazał je Minerwie. Próbował pozyskać Minerwę, ale Pięknotka weszła mu w szkodę. Musiał uciekać.
* Kasjopea Maus: dziennikarka, która zrobiła na prośbę Pięknotki niewiarygodne świństwo Kornelowi - sprawiła, że jest persona non grata. Będzie mogła negocjować z Minerwą odnośnie Kryształu Pamięci.
* Atena Sowińska: wykryła (za prośbą Pięknotki) energię ixiońską niedaleko Pustogoru i wezwała oddział szturmowy, by przerwać sprytny plan Kornela Garna.

## Plany

* Minerwa Metalia: nie zamierza nigdzie wyjeżdżać; zostaje w Szczelińcu, w okolicach Zaczęstwa.
* Minerwa Metalia: ma zamiar pomagać Kornelowi, jeśli jest tylko na to okazja i sposobność.

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Zaczęstwo
                                1. Nieużytki Staszka: miejsce, gdzie Kornel stworzył małe społeczeństwo dla Minerwy ze swoich kultystów.
                                1. Hotel Tellur: dość dobrej klasy hotel, gdzie mieszkał Kornel, tymczasowo.
                                1. Wschodnie Pole Namiotowe: dość oddalone od samego Zaczęstwa; rozłożył się tam Kornel z kilkunastoma ludzkimi kultystami.

## Czas

* Opóźnienie: 1
* Dni: 2

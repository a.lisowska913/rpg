---
layout: cybermagic-konspekt
title: "Korporacyjna wojna w MMO"
threads: nemesis-pieknotki
gm: żółw
players: arleta, raynor, magda_m, kapsel
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [190213 - Wygaśnięcie starego autosenta](190213-wygasniecie-starego-autosenta)

### Chronologiczna

* [190213 - Wygaśnięcie starego autosenta](190213-wygasniecie-starego-autosenta)

## Projektowanie sesji

### Dark Past

* Alan jest nadal zabanowany
* Wojna między dwoma korporacjami

### Dekompozycja pytaniami

* .

### Struktura sesji: Frakcje

* .

### Dark Future

1. .

### Pierwsza Scena

.

## Potencjalne pytania historyczne

* brak

## Punkt zerowy

### Postacie

### Opis sytuacji

(z pytań generatywnych)

* Negocjacje są w MMO, bo z uwagi na Podły Czyn jest ryzyko machlojów. Plus, oni wszyscy są porozsiewani - tam się mogą spotkać.
* Siostra negocjatora (Adriana z Sensus) ucierpiała w Podłym Czynie, bo miała cudowną rosę volant i ją podjadała. I ją straciła.
* Wojna jest "nieunikniona", bo rosy volant jest za mało a tamta od siostry Adriana była na wyginięciu. Nie ma takich wiele...
* Sekator, aktualny lider EliSquid, chce się odznaczyć. Skoro EliSquid i tak nie są "famous", mogą być "infamous". Chcą przejść do legendy jako elitarna grupa "zła".

## Misja właściwa

Rafał i Adrian z korporacji się kłócą. Sytuacja zmierza ku wojnie - zarówno ze strony Rexpapier (którzy są agresorami) jak i Sensus. Negocjatorka Eliza obudziła w sobie umiejętności psychologa małżeńskiego - o co tu chodzi? Jak im pomóc? Udało jej się deeskalować sytuację a nawet doszła do przyczyny konfliktu.

* Rexpapier chce wojny, ku zdziwieniu Elizy. Są silniejsi i zależy im na dostępie do rosy volant.
* Sensus nie chce wojny, ale nie będzie się opierać. Rexpapier chce, by Sensus wypowiedział im wojnę - i to jest możliwe.
* Kwestią są animozje personalne. Brat Rafała ucierpiał przez używki Sensus a teraz siostra Adriana straciła bezcenny kwiat.

No dobrze. Nie jest łatwo. Eliza skupiła się na opóźnianiu sprawy... ONA nie chce wojny.

Tymczasem w MMO, Mi oraz Wojtek podjęli próbę wbicia się do Sekatora. Dojścia do tego co to za jeden i użycia magii, by go unieszkodliwić - Sekator przygotował się do uderzenia w Kryształowy Pałac i zdobył Kryształowe Leże. Mi skorzystała z reputacji Wojtka i powiedziała, że ma doskonałe eliksiry, w sam raz na ciężką akcję tego typu. Sekator dopuścił ją przed swoje oblicze. Niestety, Paradoks. Skażony Sekator został odcięty od kompa a jego awatar zdecydował się toczyć wojnę bez niego...

Dobra. To wymaga drastyczniejszych ruchów. Wysłany na akcję Antoni Kotomin spotkał się z Alanem Bartozolem; Mi wykryła prawdziwą lokalizację Sekatora - Cieniaszczyt. Alan i Antoni pojechali tam... Alan drastycznie wymusił na "Sekatorze" zaprzestania głupich akcji. W międzyczasie Mi poprosiła Yyizdatha o spojrzenie na Sekatora, bo jest pod wpływem magii. Sekator został zabanowany.

Na pytanie dlaczego Sekator tak przesunął EliSquid, ten odpowiedział, że "ona" mu kazała. Nie miał wyboru. Ona chciała zniszczenia Kryształowego Pałacu i najpewniej tego osiągnie. Alan jest nieszczęśliwy, ale nic nie jest w stanie zrobić. Sekator też jest zabanowany. EliSquid znowu wymknęło się spod kontroli.

Nadal nie rozwiązuje to problemu negocjacji. Więc Antoni i Eliza udali się do Dariusza Biznesu. Powiedzieli o rosie volant - mogłoby to nieźle uzupełnić jego zapasy produktów herbacianych. Dariusz jest skłonny spróbować. Eliza dała znać Rexpapier oraz Sensus. Obie korporacje zdecydowały się na wstępny pokój, by wpierw wklupać Dariuszowi.

Niekoniecznie o to chodziło Antoniemu...

Wpływ:

* Ż: .
* Zespół: .

**Sprawdzenie Torów** ():

2 Wpływu -> 50% +1 do toru. Tory:

* .

**Epilog**

* Dariusz będzie zaatakowany przez Rexpapier i Sensus. Ale ma szansę obrony mocą graczy.
* Mi ma wpływy w obu korporacjach.
* Wojtek dołączył do gildii anty-PK jako oficer.
* Eliza ma szacunek. Doprowadziła do pokoju.
* EliSquid zaczęła osobiście dowodzić Iglica, stojąca kiedyś za Sekatorem...
* Rexpapier zapłacił reparacje Sensusowi.

### Wpływ na świat

| Kto           | Wolnych | Sumarycznie |
|---------------|---------|-------------|
|               |         |             |

Czyli:

* (K): .

## Streszczenie

Dwie magiczne firmy - Sensus oraz Rexpapier - weszły w konflikt o Rosę Volant. Rozmowy pokojowe miały mieć miejsce w MMO. W wyniku tego Alan Bartozol znalazł aktualnego szefa EliSquid, "Sekatora Pierwszego", którego też zabanowali. EliSquid zostało przejęte przez niejaką "Iglicę" a rozmowy pokojowe się udały - wszyscy skupią się na Dariuszu Bankierzu i małym miasteczku na terenach Skażonych by nie pojawiła się większa konkurencja do rosy volant...

## Progresja

* Eliza Ira: jako "Iglica" przejęła dowodzenie nad EliSquid. Kryształowy Pałac jest zagrożony jak nigdy ;-).
* Kermit Szperacz: został zabanowany przez działania Mi Rudej. Nie dowodzi już EliSquid.
* Mi Ruda: ma wpływy w obu korporacjach: Rexpapier i Sensus.
* Dariusz Bankierz: będzie zaatakowany przez siły zarówno Rexpapier jak i Sensus, niestety...

### Frakcji

* EliSquid: Eliza Ira aka Iglica przejęła nad nimi dowodzenie. Zniszczą Kryształowy Pałac.

## Zasługi

* Wojciech Słabizna: umożliwił Mi Rudej dotarcie przed oblicze Sekatora Pierwszego. Trafił do anti-PK gildii.
* Eliza Kotlet: negocjatorka i psycholog małżeński, która próbuje zapobiec wojnie; najpierw pomogła w negocjacjach a potem przekierowała ogień na Dariusza (z rosy volant).
* Mi Ruda: zabanowała nieszczęsnego Sekatora Pierwszego mocą Yyizdatha. Też powstrzymała działania Sekatora odnośnie EliSquid.
* Antoni Kotomin: wraz z Alanem Bartozolem dotarli do Sekatora Pierwszego i zmusili go do współpracy.
* Alan Bartozol: były lider EliSquid który zmusił Sekatora Pierwszego do współpracy. Niestety, wiele więcej nie dał rady zrobić.
* Kermit Szperacz: aka "Sekator Pierwszy", tymczasowy lider EliSquid. Mieszka w Cieniaszczycie. Okazuje się że na usługach Elizy Iry; chciał doprowadzić EliSquid do wielkości. Niestety, też zabanowany.
* Dariusz Bankierz: dał się skusić by wziąć tematy związane z Rosą Volant na siebie; ma teraz przeciw sobie dwie 
* Rafał Królewski: negocjator Rexpapier. Chce wojny z Sensus.
* Adrian Wężoskór: negocjator Sensus. Chce uniknąć wojny, ale nie da się osłabić.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Multivirt
        1. MMO
            1. Kryształowy Pałac: miejsce piękne i pozornie niezniszczalne. Idealne miejsce na rozmowy pokojowe.
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Pustogor:
                                1. Miasteczko: miejsce spotkania Antoniego z Alanem. Tam doszło do ustalenia, że jadą do Cieniaszczytu rozprawić się z Sekatorem.

## Czas

* Opóźnienie: 5
* Dni: 3

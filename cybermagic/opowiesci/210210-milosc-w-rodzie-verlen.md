---
layout: cybermagic-konspekt
title: "Miłość w rodzie Verlen"
threads: legenda-arianny
gm: żółw
players: kić, fox
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [210118 - Ratunkowa misja Goldariona](210108-ratunkowa-misja-goldariona)
* [210324 - Lustrzane odbicie Eleny](210324-lustrzane-odbicie-eleny)

### Chronologiczna

* [210118 - Ratunkowa misja Goldariona](210108-ratunkowa-misja-goldariona)

## Punkt zerowy

### Dark Past

* Nie znamy. Próbujemy poznać ;-).

### Opis sytuacji

* Arianna wraca do rodu Verlen, na planetę. Kazali jej "zniknąć" po sprawie z Sowińskimi.

### Postacie

* Arianna Verlen: Fox
* Viorika Verlen: Kić
* Elena Verlen (kiedyś: Verlen): NPC

## Punkt zero

N/A

## Misja właściwa

Elena została zaproszona przez swój były ród, Verlen, z zachętą, że dostanie Eidolona. Elena jest skrajnie nieufna, ale niech będzie - Eidolon to Eidolon, na Orbiterze nie ma zwykle możliwości uzyskania tego typu sprzętu (poza tym co jej załatwiła na Goldarionia Klaudia). ELena jest skrajnie zestresowana - po pierwsze, jest "córką marnotrawną"; odrzuciła ród. Po drugie, powiedzmy, zrujnowała swoją reputację na Orbiterze. Jest raczej punktem śmieszności niż świetną advancerką ze szkoleniem Verlen czego można by się spodziewać.

Arianna natomiast po prostu została zaproszona by "przedyskutować temat Sowińskich". Sama z radością poleciała. Nie dlatego, że boi się o swój ród. Dlatego, bo **boi się, że Verlen wypowiedzą Sowińskim wojnę o jej reputację**. A niestety najpewniej Sowińscy zmiażdżą Verlen (choć koszty będą katastrofalne dla Sowińskich).

Jeszcze w korwecie Elena powiedziała Ariannie, że nie powinna była przyjmować tego zaproszenia. Nie czuje się z tym dobrze. Arianna jednak zauważyła, że Elena sobie z tym poradzi, jak z wszystkim innym do tej pory. Dla Arianny Elena jest NADMIERNIE zestresowana; coś tam było w przeszłości. No i Arianna sama nie czuje się szczególnie OK. Zawsze z radością odwiedza planetę i rodzinę, ale to wszystko jest ciekawe i nietypowe. Ród Verlen - skrajnie anty-TAI i Eidolon wymagający neurosprzężenia? Jak to możliwe? Jak to Elena zauważyła złośliwie, "może zmywarkę też kupili".

Co wiemy na tym etapie o rodzie Verlen (w trakcie generacji na sesji):

Postacie i wydarzenia:

* Arianna jest jedyną córką; ma jedynie braci. Dwóch braci (Seraf i Krucjusz).
* Rodzice Arianny, Franz i Brunhilda, są raczej z tych wojskowych, twardych. Brunhilda jest oryginalnie z Verlen, Franz jest z "zewnątrz".
* Seraf Verlen wygląda jak słitaśny cherubinek, ale to żyleta; jego żołnierze czują mega respekt. Jest twardy jak cholera. (UW).
* Krucjusz Verlen to potężny facet, sympatyczny ale niebezpieczny dla wrogów. Charakteryzuje się śmiałymi i nietypowymi pomysłami (UR). Lubi się chełpić, ale stoją za tym kompetencje.
* Oprócz tego ważny kuzyn to Romeo; wojowniczy i zawsze skrajnie oddany rodowi (WB).
* Elena i Romeo w przeszłości mieli ostre starcia.
* Elena kiedyś się podkochiwała w Serafie; on tego nie zauważał. Arianna z uśmiechem przypomina sobie młodą, nieśmiałą Elenę próbującą zagadać do Serafa którego interesowały walki.

Sam teren rodu Verlen:

* Teren jest taki dość jałowy, raczej podwzgórzysty. Nędzne, wyjałowione świerki. Kiedyś Arianna żartowała, że łazi taki ogrodnik i je zatruwa by nie rosły.
* Sentisieć rodu Verlen stawia przed Verlen wyzwania i problemy.

Odnośnie rodu Verlen, zwyczajów i działań:

* Jednym ze zwyczajów jest branie 8-latków, wsadzanie ich do strasznego lasku i mają sobie poradzić i same wrócić do domu. Ofc patrzy na nie sentisieć, ale one nie wiedzą XD.
* Ród Verlen bardzo agresywanie zwalcza wszelkie anomalie; ich ludzie są zwykle mocno bronieni i nie mają dużych kłopotów.
* Ród Verlen słynie z eksportu świetnych wojskowych, szturmowych oddziałów i z doskonałego sprzętu wojskowego - nie sterowanego TAI
* Ród Verlen nie ufa TAI. Nie można zostawić wojny automatom ani łączyć automatów z ludzkością. Szukają innych rozwiązań.
* Nie ufają też bioformom; stąd ciągłe waśnie i spory z graniczącymi z nimi Blakenbauerami.
* Ich doktryną jest: Duty|Discipline, Niezależność, Czystość. Pierwsza i ostatnia są posunięta do maksimum. Bardzo militarystyczny ród. Nie patrzą na Orbiter z dumą, raczej na dzieciaki udające żołnierzy.
* Wbrew pozorom i ponuremu kształtowi wszystkiego w rodzie, poczucie humoru nie jest u nich zakazane ;-).

Tak czy inaczej, Elena i Arianna wylądowały na planecie, na obrzeżach Holdu Karaan (główne miasto-forteca). Przywitała ich... orkiestra dziecięca. 20 dzieciaków na instrumentach i chórek 10 chłopców. Arianna wyczaiła tekst typu "córki powróciły z tułaczki". Ok:

* Czemu z taką pompą? To nienaturalne.
* Córki? Włączają w to Elenę? To jest samo w sobie ciekawe.

W treści dało się też usłyszeć o "potencjalnej walce z niedźwiedziem", więc standardowe imprezy. Elena blada, Arianna nieco nerwowa. Podjechał wojskowy pojazd z Vioriką, przyjaciółką zarówno Arianny jak i Eleny. Viorika z radością poinformowała Ariannę, że "Sekrety Orbitera: historia prawdziwa" dotarły do Verlenlandu i że tu wszyscy zakładają, że to jest jakiś paszkwil mający oczernić córkę rodu Verlen. Zwłaszcza ta sprawa z Anastazją - sami nie umieli jej utrzymać. Dlatego Generał (ojciec Arianny) chce zrobić kontr-serial: "Arianna Verlen - historia prawdziwa". Arianna wiedząc, że Izabela robiła co mogła by "Sekrety Orbitera" pokazały Infernię w dobrym świetle... zdecydowała się nie komentować. Ale kontr-propaganda jest potencjalnie nieskuteczna.

Aha, ród Verlen zbroi się do walki z Sowińskimi. Co Ariannę nie cieszy.

Arianna zapytała też o Eidolona dla Eleny. Viorika powiedziała, że jest sentisprzężony - w ten sposób ominęli problem TAI. Elena zaprotestowała - nie da się stworzyć sentisprzężonego Eidolona. Ludzki mózg nie poradzi sobie z czymś takim. Nie ma sentisprzężonych servarów ani statków. Arianna pamięta "Odkupienie"; zdecydowała się nie zaznaczać tego faktu (Elena nic nie wie). Viorika powiedziała, że to jest możliwe i ten Eidolon działa - a Elena będzie mieć możliwość wykorzystania czegoś takiego. Elena zbladła śmiertelnie. Nie chce się łączyć z sentisiecią...

Arianna zaczyna widzieć pewne potencjalne problemy do rozwiązania...

Sztab w Barbakanie Centralnym Holdu Karaan. Spotkanie Arianny z rodzicami. Arianna przygotowała się mentalnie na solidny opierdol... Ale zupełnie nie tak to poszło:

* Franz zapowiedział, że ród Verlen jest z niej dumny. Ta pompa, parady - po to, by pokazać wszystkim że Verlen stoją za swoją malutką Arią. (criiinge)
* Franz chce, by ród Verlen zrobił propagandowy film "Arianna Verlen - historia prawdziwa". Może jacyś dziennikarze Verlen z Arianną na Inferni? By pokazać Infernię w dobrym świetle? Arianna wiedząc jaką ma załogę woli nie... nie chce, by rodzice zobaczyli jak naprawdę źle wygląda Infernia.
* Rodzice chcą, by Arianna dała im potomka. Arcymaga. Nie byle jakiego i byle z kim. To jej odpowiedzialność względem rodu. Projekt "Arcymag Verlen" nie może czekać! Jak to Franz zauważył, "ród przejąć może dowolny z braci, ale arcymaga możesz dać nam tylko Ty".
* Franz też powiedział Ariannie, że musi dbać o Elenę i reputację Eleny. Nawet, jeśli Elena odeszła z rodu, "duty" tego wymaga. A Orbiter szczególnie podle traktuje Elenę.
* Brunhilda wyciągnęła WSZYSTKIE reputacyjne ciosy w Ariannę jakie były na Orbiterze. Pokazała je. Powiedziała, że w nie nie wierzą (większość to jest prawda XD).

Czyli rodzice chcą od Arianny:

1. Potomek, arcymag. Najlepiej z ożenkiem, ale to opcjonalne.
2. Zajmij się Eleną. Zarówno jako swoją podkomendną, jak i niech wróci do rodu. Musi tylko przeprosić, Verlen ją przyjmą z powrotem.
3. Lepszy film niż "Sekrety Orbitera" i praca nad reputacją Arianny.
4. Badania nad sentisprzężonym Eidolonem i "Krwawą Różą": pierwszym sentisprzężonym statkiem orbitalnym Verlen.
5. Porwać Milenę Blakenbauer. Chce wyjść za Krucjusza (ze wzajemnością), ale nie pozwalają w rodzie. Dobre ćwiczenia :3.

Arianna przyjęła ten potok słów, ucieszyła się troszkę. Dobrze być kochaną i docenianą, choć czasem niektóre formy ekspresji (wojna) nie są preferowane. Poszła zająć się Eleną, która na pewno poszła do Eidolona. A na miejscu, przy Eidolonie okazało się, że Eleny... nie ma.

Arianna w końcu znalazła Elenę w Krypcie Plugastwa. Elena nie poszła tam walczyć przeciwko symulacjom - Arianna znalazła Elenę stojącą w wielkiej sali, gdy uruchomiła setki TAI, mówiące do niej jednocześnie, sama, w ciemnym pomieszczeniu. Gdy Arianna przerwała symulację to spytała - czemu Elena jest w tak złym stanie? Co się dzieje? Elena spytała, jak Arianna się czuje gdy łączy się z sentisiecią. Arianna czuje... poszerzenie. Wielkość. Wszechmoc. "Elation". Elena powiedziała, że gdy ona się łączy z sentisiecią to czuje coś takiego jak z tymi TAI.

Arianna zauważyła, że to dlatego, bo ona odrzuca ród i odrzuca sentisieć. Elena w pewien sposób sama sobie to robi. Młodsza czarodziejka nie zaprzeczyła. Gdy Arianna spytała, jak Elenie się wiedzie, czy już kogoś spotkała - Elena odpowiedziała, że już zdążyła Romeo wyzwać na pojedynek. Co?!

Elena opowiedziała chronologiczną historię:

* Poszła zobaczyć tego senti-Eidolona. Eidolon jest groźny. Jest "obcy". Nie chciała się z nim łączyć.
* Spotkała Romeo, który spytał ją o świnie. Powiedziała cośtam. Wyprowadził ją z równowagi - zdecydowała się na pojedynek.
* Zaproponuje mu broń palną, do krwi. Romeo nie podoba się pomysł "do krwi", wolałby inną broń; ale Elena chce patrzeć na krew.

Arianna zorientowała się, że to fatalny pomysł. Elena może tępi i kroi na Orbiterze, ale to Verlen. Romeo ją pokona. Ale Arianna wie, że Romeo jest oblatywaczem "Krwawej Róży". Zaproponowała Elenie, że jak już się zintegruje z Eidolonem, może pokaże Romeo jak lata mistrzyni pilotażu Orbitera? Elenie się bardzo spodobał ten pomysł - nadspodziewanie, nawet. Do tego stopnia, że Arianna jest pewna, że tam jest coś pod spodem.

Zachęcona i zmotywowana, Elena poszła do Eidolona. Cała spięta i nieszczęśliwa; przyszła jeszcze Viorika popatrzeć. Elena zwróciła się do żołnierzy, żeby się oddalili - ona się musi rozebrać, by wejść do Eidolona. Oni by chcieli, ale rozkazy muszą dostać od maga rodu Verlen. Viorika kazała im sobie iść; z radością się oddalili. Elena zażądała, by Arianna i Viorika też się odwróciły, na co Viorika, że Elena przecież nie raz się z nimi kąpała. I nawet uciekały przed aligatorami (#just-verlen-things). Elena zauważyła, że uciekała szybciej niż Arianna...

Rozluźniona Elena, z pozytywnymi myślami o przeszłości weszła w integrację z Eidolonem (ExZ+3): VVX. Zadziałało. Wykonała serię manewrów i operacji, po czym opuściła Eidolona. Wyczerpana, skrajnie wykończona. Powiedziała kilka ciekawych rzeczy:

* Eidolon sentisprzężony jest DUŻO skuteczniejszy niż TAI-sprzężony. Może działać lepiej.
* Jest to absolutnie wykańczające. Elena nie jest w stanie kontrolować tego dłużej niż 60 minut. Potem się mentalnie zagubi.
* Ryzykiem jest rozpłynięcie się w sentisieci. Niemożność powrotu.
* Skrajna zależność od emocji kontrolera. Elena w dużym stresie po prostu nie da sobie rady.

Tak. To dobry pomysł. Romeo może się płynnie łączyć z sentisiecią "Krwawej Róży", ale Elena jest dużo lepszym pilotem. Taki pojedynek ma sens. Elena natychmiast napisała HateLetter do Romeo, by ten zgodził się na pojedynek. (Tr:XXV). Romeo się zgodził, ale postawił warunek - jeśli Elena przegra, musi przeprosić. Jego i ród. Bo odeszła, bo nie dba o swoje 'duty'. Bo nie jest tą Eleną, którą powinna być. Elena, cała jak osa, zgodziła się. Nie przegra. Na pewno nie przegra.

Arianna i Viorika wymieniły spojrzenia. Między tą dwójką do czegoś doszło kiedyś. Bo to jest więcej niż tylko kwestia "nie lubimy się".

Arianna z Vioriką weszły w sentisieć, szukać rzeczy z przeszłości. Co zaszło między Romeo i Eleną? O co chodzi w tym ich konflikcie?

* XXXV: Romeo i Elena dowiedzą się o śledztwie i jego wynikach, ale po pojedynku.

Sekwencja, po kolei:

* Gdy Elena była młodziutka, podkochiwała sie na zabój w Serafie; on tego nie zauważał.
* Romeo podkochiwał się w Elenie, ale ona tego nie zauważała. Więc Romeo jej dokuczał. Ona reagowała dokuczaniem i eskalacją. On myślał, że to miłość - ona chciała się go pozbyć.
* W pewnym momencie Romeo zorientował się, że Elena ma za bliską przyjaciółkę TAI. Za bardzo na niej polega. Więc powiedział o tym rodzicom - ci kazali mu zniszczyć tą TAI. Romeo zniszczył TAI na oczach Eleny, wpierw każąc TAI prosić go o darowanie życia. Chciał dać jej lekcję, że to tylko narzędzie i powie co ma powiedzieć - ale Elena odebrała to jakby upokorzył i zabił jej kochanego psa na jej oczach. Elena błagała o "życie" dla TAI, ale Romeo nie widział tego tak, chciał Elenę zahartować.
* Elena weszła w jedną ze swoich obsesji. Romeo zawsze chciał być pilotem, pilotować pojazdy, Elena nie. Ale całe swoje życie Elena dedykowała pilotażowi - zabierając Romeo instruktorów, podając jego plany rywalom, używając sentisieci do zapewnienia by nie wygrał.
* Elena stała się mistrzynią pilotażu po neurosprzężeniu, niszcząc ostatecznie jakiekolwiek nadzieje Romeo na zostanie pilotem czy kimś wielkim w tym obszarze. Elena ZAWSZE będzie lepsza i ZAWSZE go wytnie z oddziału w lotnictwie.

Wow. Arianna i Viorika tego nie wiedziały. To jest rywalizacja i wojna. A to, że teraz znowu sprawa toczy się o pilotowanie statku... to jedynie przywołało i u Eleny i Romeo traumatyczne wspomnienia. Pójdą daleko w pojedynku. Pytanie, czy nie za daleko...

I faktycznie, dzień pojedynku. Ludzie obserwujący. Elena łączy się z Różą pierwsza (XXVzOVX):

* Elena zostaje odrzucona przez sentisieć. Robi to w nienawiści, dla siebie, nie z obowiązku. Jednak Elena wchodzi głębiej, czystym uporem.
* Elena uruchamia Różę i wzbija ją w powietrze. (VV): kilka efektownych manewrów.
* Elena zostawiła swój ślad i elementy Esuriit w Róży. Wyczołgała się ze statku; nie miała sił iść sama.

Romeo jest przekonany, że poradzi sobie lepiej - w końcu nigdy nie miał kłopotów z integracją z sentisiecią i manewrach. Ale też robi to z nienawiści a w Róży został imprint Eleny.

* XX: Róża go odrzuciła. Ten sam powód co Elena.
* VX: Wzbił statek w powietrze, ale traci kontrolę. Nie utrzymuje połączenia.
* O: Obsesja Esuriit; nie przegra z ELENĄ. Ona MUSI przeprosić.
* XV: Straszne szkody, ale JEST REMIS. Ale remis... remis to za mało. Eskalacja.
* O: Zatopienie w nienawiści. Sentisieć zaczyna uruchamiać system defensywny.
* X: Disharmonia. Sentisieć przejęła Romeo.

HUD Arianny reklasyfikował OA "Krwawa Róża" na AK "Krwawa Róża". Arianna podskoczyła. Viorika wysłała rozkaz do sentisieci, by odbudować i odzyskać "Różę", nie krzywdząc Romeo. (VVXXX) - sentisieć da radę, ale to potrwa.

Romeo/Róża przesłał sygnał po sentisieci do Eleny - jest niegodna. Zawiodła. Jest osobą, która zmarnotrawiła wszystko co dostała. Przynosi wstyd rodowi. Inni na to patrzą i jej wybaczają - on miał do niej takie wysokie standardy. A teraz - ród boi się TAI, ona boi się sentisprzężenia, on jest KOMPLETNY. Pokona ją.

Elena, też w głodzie Esuriit odpowiedziała. Zaczęła wzywać swój myśliwiec. Będzie z nim walczyć w powietrzu.

Viorika i Arianna skupiły się na Elenie. Arianna przypomniała jej - to Elena opieprzyła ją za tych pretorian Anastazji. A przecież jak Elena będzie walczyć z Romeo, to ucierpią cywile i żołnierze. Arianna wie, że Elena jest lepsza niż to. Viorika zaczęła przypominać ją czym Elena jest i co sobą reprezentuje.

Elena: (XXXXVVm). Elena nie jest w stanie pokonać obsesji Esuriit. Próbuje. Nie jest w stanie. Więc zrobiła zdalne sprzężenie z TAI w myśliwcu i wyłączyła swoje główne ośrodki. Padła na ziemię nieprzytomna. Viorika natychmiast wzywa medyków...

Arianna skupia się na anomalizującej Róży. Gdy Elena padła, Romeo stracił powód i sentisieć go "wypluła". Niesterowna Róża zaczęła pikować w ziemię; Arianna używa mocy by to zatrzymać. By nikomu nic się nie stało. Jest w końcu arcymagiem...

* VV: Wylądowanie bez problemu; Róża i Romeo są cali, nie ma ofiar ani rannych.
* Xm: EFEKTOWNOŚĆ! CEKINY! WSZYSTKO! Po prostu akcja tak epicka że ja pierniczę. Wbrew Ariannie >.>
* OX: Teraz to rodzice na 100% chcą potomka. Widzieli siłę arcymaga. A Esuriitowy głód odbijający się w sieci zdecydowanie wzmocnił tą potrzebę...

Już po wszystkim, Arianna i Viorika zauważyły, że to było charakterystyczne dla Eleny. Elena mogła skorzystać z okazji i poprosić Ariannę o zatrzymanie jej. Arianna była w stanie. Ale nie - Elena musiała zrobić to _sama_. Sprzęgła się z TAI swojego małego stateczku i zamiast ona -> TAI kazała zrobić TAI -> ona. W ten sposób przesterowała swoje implanty i straciła przytomność; TAI nie była w stanie spełnić jej poleceń, więc wyłączyła się świadomość Eleny. Bardziej niebezpieczne (i dla Eleny i zespołu), bardziej szkodliwe. Ale bardzo Elenowe...

## Streszczenie

Arianna i Elena wróciły do domu, do rodu Verlen. Tam się okazało, że ród jest skłonny iść na wojnę z Sowińskimi za despekty (często słuszne) wyrządzane Ariannie i Elenie. Elena nie umie się odnaleźć jako akceptowana i lubiana. Elena integruje się z sentisprzężonym Eidolonem i statkiem orbitalnym; po czym w pojedynku pokonuje swojego dawnego rywala, Romeo. Wychodzą na jaw Mroczne Sekrety Eleny. A Arianna próbuje zapobiec tragedii, nie dać się wrobić w potomka i pokazuje przypadkiem najbardziej efektowny ratunek klasy arcymag ever.

## Progresja

* Elena Verlen: wyszło na jaw, że została pilotem by dokuczyć Romeo i że kiedyś miała za najlepszą przyjaciółkę TAI. Bardzo dziecinna była jak była mała. Nie dała się opętać Esuriit, odcinając układ nerwowy.
* Elena Verlen: jest akceptowana taka jaka jest w rodzie Verlen i ma opcję powrotu - jeśli przeprosi (Blakenbauerów, ze wszystkich rodów). Co sprawia, że jest bardzo skonfliktowana.
* Elena Verlen: potrafi się połączyć z sentisiecią Verlen i jest w tym mocniejsza niż się wydaje. Ale nie akceptuje rodu i siebie, więc połączenie jest słabsze. Potencjalnie najmocniejszy link z sentisiecią.

### Frakcji

* .

## Zasługi

* Arianna Verlen: jest szanowana i kochana w rodzie Verlen; chcą dla niej walczyć z Sowińskimi. Lekko skłania Elenę, by nie odcięła się od rodu (+sensowny pojedynek) a potem efektownie ratuje Romeo mocą arcymaga.
* Viorika Verlen: starsza od Arianny i stopniem i wiekiem, przyjaciółka Arianny i Eleny. Mistrzowsko steruje sentisiecią i stabilizuje wszystko, by Arianna i Elena nie ucierpiały. Nieco złośliwa ;-).
* Elena Verlen: alias Mirokin; okazuje się, że jest lubiana w Verlen, czego nie umie zasymilować. Połączyła się z sentisiecią, wyzwała Romeo na pojedynek - i wygrała. By nie przejęło jej Esuriit, "wyłączyła" się. Cierpi na wieczny kompleks "ja siama".
* Romeo Verlen: kochał się kiedyś w Elenie, teraz jej wieczny rywal. Przegrał z nią pojedynek na pilotaż. Waleczny i odważny, oddany rodowi. BW.
* Franz Verlen: ojciec Arianny, chce organizować "major Arianna Verlen - historia prawdziwa" i opracował sposób, by pokazać Sowińskim że Verlen stoją za swoją Arianną.
* Brunhilda Verlen: matka Arianny, waleczna walkiria. Zależy jej na tym, by Arianna zaopiekowała się Eleną.
* Seraf Verlen: kiedyś obiekt westnień Eleny; UW; słitaśny cherubinek, który jest ostry jak żyleta dla żołnierzy i podwładnych.
* Apollo Verlen: chce wyjść za Milenę Blakenbauer (ze wzajemnością).
* Krucjusz Verlen: UR; nieco chełpliwy i mocarny, ale stoi za tym prawdziwy skill.

### Frakcji

* Ród Verlen: prowadzą badania nad sentisprzężeniem w miejscu TAI.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Aurum
                        1. Verlenland: prowincja i domena rodu Verlen, z bardzo agresywną i stawiającą wyzwania sentisiecią. Produkuje świetnych, zdyscyplinowanych żołnierzy.
                            1. Hold Karaan: główne miasto-forteca Verlen. Ustępuje w defensywach chyba tylko Pustogorowi.
                                1. Krypta Plugastwa: znajdują się tam opcje symulacji walki z potworami pokonanymi przez Verlen i różne TAI. Elena zrobiła symulację kakofonii.
                                1. Barbakan Centralny: główny "pałac" gdzie mieszkają lordowie Verlen. Ofc, forteca. Tam jest też sztab, gdzie Arianna widziała się z rodzicami.
                            1. Hold Karaan, obrzeża
                                1. Lądowisko: starport niedaleko Hold Karaan, w który celuje niejedna bateria dział. Piorunujące wrażenie ;-).


## Czas

* Opóźnienie: 6
* Dni: 3

## Inne

### Projekt sesji

Ród Verlen

* Skrajnie militarystyczny, silnie wojskowy, z tradycjami
* Aurum: Laetitia - Niezależność - Czystość
    * Verlen: Duty|Discipline - Niezależność - Czystość
    * Verlen: "To my stoimy na murach, gdy wszystkie ogniska zgasną"

### Sceny sesji

* Poczciwy opierdol Arianny
* Elena w Krypcie Przodków, rozmawiająca z holoprojekcjami. Zapytana czemu nie z żywymi, mówi, że są nie do wytrzymania
* Elena próbująca sterować "Krwawą Różą", która ją odrzuca. Elena boi się Róży.
* Romeo będzie sterował "Różą" - pokaże Elenie. Róża go zdominuje. Nienawiść > Duty.
* Elena próbująca zestrzelić i nie zabić Romea. Romeo używający sentisieci do zestrzelenia Eleny.

## Idea sesji

Sentisprzężony statek Verlen przejmuje kontrolę nad słabym pilotem i obraca lojalność w chęć zemsty.

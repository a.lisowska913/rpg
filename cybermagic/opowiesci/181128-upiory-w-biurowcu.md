---
layout: cybermagic-konspekt
title:  "Upiory w biurowcu"
threads: dzien-z-zycia-terminusa
gm: żółw
players: dominika_d, julia_r, karolina_s, agnieszka_m, sylwia_h, mateusz_m
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [181114 - Neutralizacja Artylerii Koszmarów](181114-neutralizacja-artylerii-koszmarow)

### Chronologiczna

* [181114 - Neutralizacja Artylerii Koszmarów](181114-neutralizacja-artylerii-koszmarow)

## Projektowanie sesji

### Struktura sesji: Podwójny wyścig

#### Tor Wykorzystania magii

| Wartość toru | Efekt |
|-------------|--------|
| 3  | - |
| 7  | Obie strony wiedzą, gdzie znajdują się postacie Graczy | 
| 12 | Echa przeszłości wplatają się w teraźniejszość w formie Efemeryd i Echa |
| 18 | Skrzynia nie nadaje się do użycia; Skażenie hula; żeton magiczny kończy sesję |

#### Tor Założyciela

Silnik: 1k3+2, Skok: 5

| Wartość toru | Efekt |
|-------------|--------|
| 3 | Pojawiają się efekty dysfunkcji: stara firma – nowe czasy (w formie spotkań i działań) |
| 7 | Pojawiają się wydarzenia drastyczne i wstępne wypaczenia czasoprzestrzeni |
| 12 | Firma zaczyna się „cofać w czasie” nawet w formie fizycznej, Upiór chodzi jawnie |
| 18 | „Niewłaściwi” ludzie zaczynają umierać, stare modele biznesowe wracają. |
| 25 | Wielotrans jest zaadaptowany do przeszłości. Wszelka niezgoda zniknęła. Jedna wizja. |

#### Tor Nieskończonej Radości

Silnik: 1k3+1, Skok: 5

| Wartość toru | Efekt |
|-------------|--------|
| 3 | Pojawiają się efekty dysfunkcji: ludzie z pragnieniami – praca do wykonania |
| 7 | Pojawiają się wydarzenia drastyczne i wstępne wypaczenia czasoprzestrzeni |
| 12 | Bardzo trudno jest nawiązać kontakt z większością ludzi, upiory są na wolności. |
| 18 | Firma traci spójność, większość ludzi wpada w trans. Pojawiają się fabokle. |
| 25 | Ludzie w Wielotransie żyją w swoich światach. Nie jest to już firma – to jednostki. |

### Hasła i uruchomienie

* Ż: (kontekst otoczenia) -
* Ż: (element niepasujący) -
* Ż: (przeszłość, kontekst) -

### Dark Future

1. Jak w torach

## Potencjalne pytania historyczne

* brak

## Punkt zerowy

### Postacie

* Marian: absolwent szkoły policyjnej, administrator sieci który jest świetny w starciach, przykładny, dokładny i sumienny. Lubi porządek
* Maxine: bibliotekarka z sali trofeów, chce podróżować i przygód, bardzo dużo mówi i jest żywą składnicą wiedzy
* Jagna: asystentka dyrektora; siostrzenica zmarłego dyrektora i jest wróżką. Zwalcza przenikanie światów i specjalizuje się w wiedzy o nieracjonalnym świecie.
* Agnes: na portierni, wie kto co i jak, plotkarka i wszystko wie. 20 lat na portierni i czas na zmianę.
* Helga: duża, silna, asystentka asystenta. Dyscyplinuje zespół i chce zniszczyć kult założyciela. Trzyma za mordę skutecznie.
* Agata: detektyw korporacyjny, lubi dołować i prześladować; jej wyróżnikiem jest pesymizm i kąśliwość i specjalizuje się w przesłuchaniach i demotywacji.

### Opis sytuacji

Wielotrans. Dość znana firma zajmująca się transportem, zarówno przy użyciu technologii morskich, lądowych jak i orbitalnych. Cały problem polega na tym, że Wy... jesteście pracownikami biurowymi tej istotnej dla okolicy firmy.

Przedwczoraj była średniej jakości impreza z przebierankami i karaoke. Niestety, ekipa tyci popiła i padło kilka słów prawdy, których szefostwo zdecydowanie nie powinno usłyszeć. W rozpaczy, jedno z Was zdobyło z szemranego źródła dziwną skrzynię która miała rozwiązać problemy w Wielotransie. No i rozwiązała - uwolniliście upiory, które rozpełzły się po całym biurze.

Szczęśliwie, Wy nadal macie skrzynię spełniającą życzenia. Nieszczęśliwie, macie przeciwko sobie upiory, niesympatyczne szefostwo i najgorsze z tego wszystkiego - niesprawne drukarki, które zacinają się w środku kluczowych raportów.

Czy uda Wam się uratować Wielotrans (i zachować pracę), zanim ktoś zginie? A może zdecydujecie się na przejście po trupach do celu i sformowanie Nowej Lepszej Firmy władając artefaktyczną skrzynią, nawet, jeśli ucierpią niewinni koledzy i koleżanki? Czy zagnacie upiory z powrotem do skrzyni, czy wykorzystacie artefakt do wysadzenia wszystkich drukarek i spróbujecie rozwiązać problem upiorów bardziej... konwencjonalnymi metodami?

## Misja właściwa

### Sceny

So they have just opened this strange chest and the ghosts and other monsters flew out and caused chaos and mayhem. Something happened in the building but they are quite safe because they are sticking together next to the chest which kinds of acts like a force field.

An occultist decided to go after her things because she kind of is prepared for things like those - she use use to combat supernatural threats. Unfortunately, a strange hands made of concrete go out of the walls and catch her. She screams, but is not able to break through. An athlete runs and pulls her out of the world trying to overpower the concrete hands, while another person takes a hammer and hits the concrete hands.

A success – somewhat. Hands have managed to destroy the electric system and now they are trapped in a conference hall. Fortunately, the conference hall is two-story. The policeman jumps and runs through the hands to get up up and jumps outside the hall to open it from the outside. They all ran with the chest and escape the strange room.

Agnes has noted that Irene is working for the hall muttering something and is a cooling guy in a suit trying to kind of get away from her. So Agnes takes Irene’s attention and managers to let the guy in the suit run away. Agnes has woozy eyes and she is certainly not in touch with the reality in this moment. Uncool. Agatha has managed to demotivate Irene – she has managed to remind her of her there the dog and she kind of broke her.

This means that the ghost is trying to get out of Irene. Our team will have nothing to do with it. They took the chest and slammed it from above onto Irene. A first ghost has been caught. Unfortunately, three are remaining.

Fine then, they decided to go to the hall of trophies – there might be some clues about this whole situation. They use the elevator, but the elevator kind of cracked. The walls are slowly moving in to squash them and they door are only half open. Did some unknown reason, having unconscious Irene they are carrying her.

Oh well, one person at a time can sneak out of the elevator. On the other side an older guy stands and he is like you cannot pass because it’s against BHP. And the walls are going to squish the players. So what do they do? Dave through Irene at the old guy. The guy falls down, breaks his hip or something and the team moves past him to the hall of trophies. An obstacle has been passed.

In the hall of trophies, they notice another ghost – but this ghost looks familiar. The receptionist has noticed uncanny resemblance between that particular ghost and an old founder of the company. So the medium who is in the same time a niece of an old founder went to talk to him. The founder was disappointed in her and in the company.

He has always tried to make the company which tries to help people – to give people chance to travel, to give them hope. What he sees now – pointless attempts to maximize money at cost of the ideals, his own niece hurting people… What saved her was a dog belonging to the receptionist who run and distracted the founder.

The receptionist returned to talk to her old friend (whose dead state at this point is not really relevant) and the medium went to find other supernatural things which can help them. The receptionist has managed to acquire quite important information from the founder: he has awakened to correct something which went wrong, the ghosts came from the chest but he did not and he is willing to help them make things right but before he goes he has to correct this company and make it great again. Make it Pro human again. Even at the cost of some of those humans.

Okay. So the media has managed to find another ghost hidden behind the door. They asked the founder to Golder and proved to goes back into the chest and he happily obliged – he went through the wall and the players remained on the other side. The door were corroded and damaged - the athlete tried to open it using her might.

Unfortunately, she managed to get stuck in them (because they were too corroded) and she fell into the site between the founder and a decaying ghost. Or: the ghost of decay. Before anything bad happened to her, and angelic woman voice stopped the ghost and let her go. It was their accountant – but she was changed, more proud, more beautiful, more imperious.

The accountant looked with amusement at the founder putting the ghost into the chest and at the team trying to convince her to go into the chest or do something. She told them that the world is sad and she is here to make things right. To unlock the chains upon humans. (Is a good moment to tell you she was Arazille?)

The team had no idea what to do at that point. They have opened the chest and unleashed all the magic powers from the chest against Arazille. In the process they have managed to destroy a whole hall, and narrating all the artifacts and a philactery or founder. On top of that, by resonating the energies of the chest with the energy of Arazille they have touched the goddesses mind.

They became locked in a catatonic trance of eternal bliss inside Arazille’s thoughts. But they have managed something which not many can do – they have expelled the goddess from a human host. As they have managed to destroy the other force – the founder – and drain all the magical energies in the battle between the chest and Arazille, the energy required for the ghosts to function has ceased as well.

At cost of themselves, they have saved the company.

### Epilog

.

### Wpływ na świat

| Kto           | Wolnych | Sumarycznie |
|---------------|---------|-------------|
| Żółw          |   0     |     14      |

Czyli:

* (K): .

## Streszczenie

Artyleria Koszmarów trafiła do Czerwinóra i grupa ludzi w biurowcu ją otworzyła. Energia stworzyła upiory, obudziła Arazille i założyciela Wielotransu. Zanim pojawili się terminusi, grupa ludzi ofiarnie poświęciła się i usunęli nadnaturalne zagrożenia używając tej samej Artylerii Koszmarów. To, co zrobili - to naprawili, kosztem katatonii i nieskończonej radości po dotknięciu Królowej Marzeń.

## Progresja

* 

### Frakcji

* 

## Zasługi

* Arazille: pojawiła się w Czerwinorze i została wyegzorcyzmowana Artylerią Koszmarów zrobioną przez Wiktora i Pięknotkę. Poświęciła się ku temu grupa ludzi.

## Plany

* 

### Frakcji

* 

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Czerwinór
                                1. Biurowiec Wielotransu: miejsce, gdzie doszło do uwolnienia Artylerii Koszmarów i pojawiła się Arazille. Grupa ludzi się poświęciła, by odepchnąć Panią Marzeń.

## Czas

* Opóźnienie: 3
* Dni: 1

## Narzędzia MG

### Budowa sesji

**SCENA:**: Nie aplikuje

### Omówienie celu

* 6 graczy. 4 nowych. 5 dam. NAJBARDZIEJ KRWIOŻERCZE POSTACIE EVER.

## Wykorzystana mechanika

1811

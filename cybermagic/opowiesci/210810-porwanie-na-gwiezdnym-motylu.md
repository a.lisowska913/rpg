---
layout: cybermagic-konspekt
title: "Porwanie na Gwiezdnym Motylu"
threads: corka-morlana
gm: żółw
players: anadia, rusty, kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [210616 - Nieudana infilracja Inferni](210616-nieudana-infiltracja-inferni)

### Chronologiczna

* [210616 - Nieudana infilracja Inferni](210616-nieudana-infiltracja-inferni)

### Plan sesji
#### Co się wydarzyło

* Tomasz chciał pomóc Flawii i dać jej bilet do nowego, lepszego życia. Tak więc oddalił się od grupy arystokratów i spotkał z Flawią. Jolanta uznała to za "syndrom Kopciuszka"
* Tomasz jednak korzystając z okazji zaczął też wymykać się do innych miejsc i panienek. Tak poznał miragenta jako damę do towarzystwa.
* Miragent skusił Tomasza na stronę, gdy ten się przechwalał jak to ma władzę itp. Porwał go (tą scenę widziała Marianna).
* Noktianie dowiedzieli się od Tomasza, że on jest ważnym Sowińskim. Wybuchła wśród noktian kłótnia - część chce go zwrócić, część chce okupu a część odzyskania za niego części noktian uwięzionych na planecie.
* Tomasz został zamknięty w pleasure-vat "na potem". Nikt nie wie że on tam jest, nie?
* ...ale Marianna wie i go odzyskała. Po czym schowała się z nim po drugiej stronie statku.
* Miragent sfabrykował ślady, że Tomasz został przekształcony mentalnie w panicza do towarzystwa. Sam tymczasem szuka gdzie jest Tomasz - wie o istnieniu Ducha Statku.

#### Strony i czego pragną

* Noktianie, idealiści
    * CZEGO: uwolnić swoich ludzi z posiadłości rodu Arłacz
    * JAK: przeszmuglować porwanego szlachcica na Valentinę. Mają w ręku m.in. zaopatrzeniowca. Mają też miragenta.
* Noktianie, szmuglerzy
    * CZEGO: dorobić sobie w nowym, podłym świecie
    * JAK: znaleźć i uwolnić tego szlachcica i dostać za niego okup XD
* Miragent: 
    * CZEGO: być wolnym od Noktian. Uzyskać prawdziwą wolność.
    * JAK: doprowadzić do zniszczenia noktian - doprowadzili do śmierci porwanego szlachcica. Więc - zabić szlachcica.
    * ENDGAME: na wolności, ucieknie na Valentinę
* Nataniel Morlan (Eternia): 
    * CZEGO: Odzyskać zbiegłych niewolników, przywrócić ich pod kontrolę Eterni. Plus, uzyskać sojuszników.
    * JAK: współpraca z arystokracją Aurum
    * ENDGAME: pozyskanie Marianny?
* Antonella Temaris, zbiegła eternijka, Duch.
    * CZEGO: Dostać się na Valentinę, uciec z Eterni
    * JAK: współpracuje i ukrywa szlachcica. Używa swojej mocy i potęgi Simulacrum. Persefona jej "nie widzi".
    * ENDGAME: śmierć w płomieniach własnej magii / mariny.
* Siły porządkowe Motyla: 
    * CZEGO: Mieć spokój. Dolecieć do Valentiny i pozbyć się jakichkolwiek kłopotów. Cisza i nikt nic nie wie.
    * JAK: wyciszyć. Jak się nie da, srogo ukarać.

#### Sukces graczy

* Sukces
    * PRIMARY: Odzyskanie kuzyna.
    * SECONDARY: Rozwiązanie problemu miragenta, noktian, eternijki.
* Porażka
    * Tomasz ginie lub nie zostaje odnaleziony

#### Postacie graczy

* Arystokratka: ATUT - szkolenie wojskowe, wygląda słodko i niegroźnie, działania w próżni, wybitna aktorka. Gracz: Anadia
* Szmuglerka: ATUT - porusza się po statku, zna ciemną stronę Motyla.  Gracz: Rusty
* Strażniczka: ATUT - ma dostępy i uprawnienia, gdy pyta to odpowiadają. Gracz: Kić

#### Obrazy wspomagające

* Statek: https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/770814e0-a7c0-4e13-aa31-107aed4dbaf6/d8ivkj7-b27eb94c-0364-465b-8822-6516729548f2.jpg?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwic3ViIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsImF1ZCI6WyJ1cm46c2VydmljZTpmaWxlLmRvd25sb2FkIl0sIm9iaiI6W1t7InBhdGgiOiIvZi83NzA4MTRlMC1hN2MwLTRlMTMtYWEzMS0xMDdhZWQ0ZGJhZjYvZDhpdmtqNy1iMjdlYjk0Yy0wMzY0LTQ2NWItODgyMi02NTE2NzI5NTQ4ZjIuanBnIn1dXX0.TE8ZHaQJXqMzkejJUdaBD6cjUfLeuEIpNIebcN7BQu8
* Interior: https://flh.ca/media/73132/flh_20180612-luxuryspace-4.jpg
* Co tam może być: https://cruiseweb.com/admin/editorImages/cw-infographic-seabourn-encore.jpg

### Sesja właściwa
#### Scena Zero - impl

Ofiara - Tomasz - znajduje się w kasynie. Uwodzicielka "przypadkowo" gra z nim w grę która wymaga jakichś umiejętności. Przegrywa do niego. Chce raz czy dwa przegrać, po drodze wygrywa. Uwodzicielka chce, by Tomasz zwrócił na nią uwagę. Grywa regularnie parę dni, pojawia się w otoczeniu zanim zagada. Plus hint hint, że jest coś co Uwodzicielkę bardzo martwi. "Delikatnie" - POMÓŻ MI BO MOŻESZ. ZASMUCONA JESTEM.

CEL: zainteresował się Uwodzicielką. TAJEMNICZĄ DAMĄ. Wynik konfliktu: na tyle zainteresowany że zafascynuje się tą damą.

* V: Jest zafascynowany. Łazi za Uwodzicielką.
* X: Tomek wypaplał. Kuzynka wie.
* X: Duch wie o tym wszystkim co tu się dzieje.
* X: Mocodawcy Uwodzicielki wymusili szybkie działanie. ZA SZYBKIE.

Uwodzicielka przesyła Tomaszowi informację jak ów jest w kasynie notkę "jesteś jedyną osobą której mogę zaufać; spotkaj się ze mną w okolicach holokabin". Przygotowuje zawczasu odpowiednie środki narkotyczne, ogłupiające. Dostaje to na krótko przedtem by się musiał pospieszyć.

* X: Wypaplał. Oczywiście. Tomasz Sowiński...

Koncepcja - holokabina, zaczajenie się i w łeb. 

* V: unieszkodliwiony.

TYMCZASEM DUCH.

Tomasz kiedyś już pomógł Duchowi. Zobaczył ją na korytarzu jak była zaplątana i uciekała przed kimś z obsługi i drogę ucieczki odcięli. I Tomasz wychodził z pokoju, zobaczył przerażenie i mimo nędznego stroju zaprosił i dobrze traktował. Osłonił ją. 

I teraz widzi Duch jak Uwodzicielka dusi jedną ręką Tomasza. Duch wie gdzie jest wszystko na statku. Wcześniej ukradła już urządzenie do przekazywania dźwięku, podsłuch, coś tego typu. Gdy widzi, jak Uwodzicielka wyjmuje Tomasza to Duch zagaduje i wsadza podsłuch.

Czy uda się wsadzić podsłuch w taki sposób, by ani Uwodzicielka ani jej mocodawcy nie byli w stanie tego odnaleźć?

Tr+3:

* V: TAK, ukrywasz przed Uwodzicielką.
* V: Jej mocodawcy TEŻ nie zlokalizują.
* V: Uwodzicielka i Tomasz są zawsze widoczni przez systemy statku dla Ducha.

Duch na pewno kojarzy Szmuglerkę. Duch podkłada Szmuglerce informację - "wiesz co i jak zrobić". Zostawia nagranie by Szmuglerka wiedziała co się wydarzyło.

* V: Udało się to podłożyć niezauważenie.

KONIEC SCENY ZERO

* Q: Co sprawia, że Szmuglerka musi pomóc Strażniczce znaleźć Tomasza Sowińskiego?
* A: Strażniczka ma haka na Szmuglerkę. Jest skłonna z niego zrezygnować jeśli ta jej pomoże.
* Q: Dlaczego dla Strażniczki to kluczowe, by Tomasz nie ucierpiał w żaden sposób?
* A: Podkochuje się w nim trochę.
* Q: Dlaczego bez Arystokratki nie jesteście w stanie Tomasza znaleźć? Co unikalnego wnosi?
* A: Wnosi wiedzę zarówno o Tomaszu jak i o ludziach którzy go porwali. Coś podejrzewa, coś wie.
    * Tomasz Sowiński często się chwalił tym, jak to możliwości i wpływy. Wiadomo, że na Gwiezdnym Motylu jest niemała grupa noktian. Wymienić Tomasza na część osób uwięzionych na planecie.
* Q: Jak Arystokratka, Strażniczka i Szmuglerka się zetknęły?
* A: Podczas imprezy na statku - wszystko przez Tomasza. Wielki mistrz imprezy, zagadywał... wszystkich przedstawiał.
* Q: Co sprawia, że macie większą szansę na znalezienie Tomasza niż ktokolwiek inny?
* A: Na Motylu nie ma zwyczaju żeby różne osoby z różnych światów ze sobą współpracowały.
* Q: Jaki jest ukryty atut Waszego tymczasowego zespołu?
* A: Arystokratka robi NAJLEPSZE drinki o czym wszyscy wiedzą. Szmuglerka ma dostęp do RÓŻNYCH substancji. Strażniczka jest w łaskach u TAI statku. 

#### Scena Właściwa - impl

Renata (Szmuglerka), Lena (Strażniczka).

Lena proponuje, że dowie się od Persefony gdzie jest Tomasz. Wiedzą kiedy i gdzie zniknął. Niech Persefona prześledzi na kamerach gdzie Uwodzicielka go wzięła. Wiecie też jak Uwodzicielka wygląda.

Lena znajduje ustronne miejsce i rozpoczyna dyskusję z Persefoną. Niestety, nie da się znaleźć Tomasza bo miał aktywne pole prywatności. Lena chce się dowiedzieć, czy ktoś mieszał w tym obszarze, w kamerach itp.

TrZ+3:

* V: Na podstawie zapisów z kamer widać wyraźnie, że przeciwnik WIE gdzie są kamery. Druga - przeciwnik ma dostęp do nagrywania, kamer itp.
* X: Opiernicz od przełożonych. "Bo nie masz szukać randki wśród Towarzyszy".
* V: Po "dziurze" prywatności Strażniczka ze wsparciem Persefony doszła do lokalizacji Tomasza - biovaty w magazynach. Dokładniej: Biovat dla Towarzysza.
* Lena eskaluje konflikt. Chce swojego przełożonego wybadać - czy to rzeczywisty powód opierniczu?
    * V: TAK. To jest powód. Nic nie ukrywa. Serio podejrzewa Cię o randkowe podejście.
* X: Lena ma zakaz zbliżania się do biovatów.
* V: Koleś ma na imię Franek i jeśli trzeba to pomoże Lenie np. odbić Tomasza. Czy walnąć artylerią. Czy coś.

Renata chce się dowiedzieć od różnych kontaktów kto manipulował przy taśmie i dostać te taśmy. Sprzedajni, mają dostęp do taśm - o tych chodzi. Kto maczał palce, taśmy.

TrZ+3:

* V: Tak, oni skasowali na prośbę arystokratki. To była kuzynka - Jolanta.
* V: Renata dostaje kopię tych taśm. I nawet jej szemrane kontakty nic nie chcą.
* X: Jolanta dostanie wiadomość że była sprawdzana.
* V: Renata ma info o czasie. Wie dwie rzeczy:
    * Nie była użyta magia.
    * Jolanta w tym czasie była w innym miejscu - rozmawiała z Persefoną o czymś związanym z AI.

Flawia chce dać Lenie alibi. Daje info Jolancie o tym, że poprosiła Lenę o polecenie Towarzysza ale nie chciała bezpośrednio więc poprosiła o to by ta sprawdziła z czego Jolanta korzysta.

TrZ+3: 

* V: Lena ma alibi. Jolanta jest święcie oburzona. Ale to zostawia...
* V: Flawia pyta co Jolanta wie o samym Tomaszu i jego ruchach.
    * Jolanta opowiada co się stało i przekazała wizerunek Damy W Czerwonym.
* V: Flawia TAKA SMUTNA. Jolka taka niekomfortowa.
    * "On się zorientuje że z nią jest coś nie tak, wróci do Ciebie". Śledziła jego i ją. Ona jest z załogi. Ma sporo nagrań.
* V: Flawia przekonała Jolę, by ta użyła specjalnego połączenia i zobaczyła co Jola widzi po swojej stronie.
    * JOLA NIE CHCE! NIE CHCE WIDZIEĆ!
    * Jego życie BYŁO w niebezpieczeństwie. Został porwany. Ale został uratowany i koleżanka go uratowała.
    * Jest dobrze schowany.

Jolka powiedziała, że poznała kogoś - tien Nataniel Morlan, "detektyw". Z Eterni. "Łowca kuriozów".

Lena przy użyciu Persefony chce dojść do tego by przy użyciu pól prywatności znaleźć obszary (na całym statku) - przemieszczanie i znikanie pól prywatności. Jeśli było jakieś pole wśród Towarzyszy - czy jest przemieszczenie, czy coś zniknęło...

Lena chce intencyjnie dostać "dziurę". Lena chce znaleźć interesujący obszar. Zwłaszcza po charakterystycznym.

ExZ+4:

* V: Lena ma dowód Ducha i korelacji Duch - Tomasz
    * Lena znalazła anomalne pole prywatności. Duch. Ono się zmienia. Pochodzenia magicznego.
    * Dowód powiązania Duch - Tomasz.

Czy Franek jest osobą korzystającą z specjalnych substancji Renaty?

8V - 7X:

* V: Tak, korzystał lub korzysta.
* V: Zależy mu by korzystać dalej.
* V: Jest uzależniony i bardzo mu zależy by nikt nie wiedział.
* V: Je z ręki Renaty. Jej słowo jest ważniejsze niż to co powie Centrala.

Zespół ma wsparcie Franka. Mogą umieszczać paczuszki jak chcą i gdzie chcą. Franek powie kucharzom itp. że to specjalna operacja.

W paczkach i informacjach Lena chce przekazać Duchowi: "otrzymałam Twoją informację o zagrożeniu, próbuję pomóc, daj mi znać czy masz Tomasza". 

TrZ+3+5O:

* O: RUCH NATANIELA, sukces
* O: RUCH NATANIELA, sukces
* V: Duch zostawi wiadomość Renacie
* Vz: Renata widzi "kątem oka" zawsze "coś". Jakby coś śledziło... ale nie ją.
* V: Renacie udało się złapać próbkę "kameleona? Bytu? Ptaka? Drony?". Czegoś. I może to przyporządkować - Nataniel Morlan, "łowca kuriozów".
* X: Nataniel dowie się, że Renata wie. I błędnie przyporządkował to do Flawii.

Nataniel odwiedza Flawię by się wyjaśnić. Powiedział, że szuka córki. Marceliny. Pokazał obraz jak wygląda Duch.

Flawia rozmawia z Natanielem, próbuje się jak najwięcej dowiedzieć na temat Marceliny itp. by móc określić prawdę.

Wiedząc to wszystko Renata rozmawia ze służbą, podkupuje ich - jakim panem jest Nataniel, czy wiedzą o Marcelinie itp. Kolejny datapoint. Czy był konflikt w Eterni? Czy to się działo...

TrZ+2:

* Vz: Renata ma dobry towar. Służba opowiedziała:
    * Okrutny, niebezpieczny, twarda ręka, ale służbę traktuje "dobrze"
    * Marcelina jest jego córką. Miał córkę imieniem Marcelina. Był konflikt między rodami.
* V: Marcelina uciekła SAMA. Ale on w to nie wierzy. Skąpał ten inny ród w krwi. Do poziomu tortur dzieci na oczach rodziców - za to co stało się córce.
    * On naprawdę kocha córkę i mu na niej zależy

Flawia się przeszła porozmawiać z arystokratami. Dużo butelek poznika. Cel - poznać historię Marceliny Morlan. Impreza, raut - a tam jedno z "trivia questions". +1V za substancje wspomagające. 

ExZ+4+3Og(Nataniel)+3Ob(Duch)+3Oy(policja):

* XX: burda w kabinie Flawii. Totalna dewastacja. Interwencja potrzebna.
* Og: Nataniel WIE co robi Flawia. Wie, że sprawdza.
* V: Flawia poznaje prawdę
    * Marcelina zginęła. Ona nie żyje. Kimkolwiek "Duch" nie jest, nie może być Marceliną.
* X: Burda wymknęła się spod kontroli. Flawia MUSI zapłacić (a jest uboga). Do tego - trafia na jej rekord.
* X: Wszystkie narkotyki, przekupstwa, złe rzeczy jakie robi Zespół --> Flawia i jej kartoteka.
* Vz: Plotka głosi coś takiego:
    * Dwa zwaśnione rody. Morlan i Temaris.
    * Marcelina Morlan i Antonella Temaris były przyjaciółkami.
    * Nataniel Morlan poprzysiągł zabić i zniszczyć KAŻDEGO maga rodu Temaris. Użył do tego celu czarnej magii.
    * Antonella zmieniła Wzór z pomocą Marceliny i krwi Marceliny. Więc Antonella uzyskała "cechy" Marceliny.

Flawia ma DUŻO do wyjaśniania policji...

TYMCZASEM RESZTA ZESPOŁU.

Renata gra spowalniacza z Flawią. Renata poszła do Nataniela, że Flawia ma nowe fakty, chce porozmawiać, chce pomóc z córką ITP. Renata dodaje straszne środki do karafki Flawii. STRASZNE. Otępiacze, tęcza, lalalala, KOLOROWE MYSZY! A Flawia dostała antidotum PRZED spotkaniem.

ExZ+3:

* V: Opóźnienie z sukcesem. Nataniel jest na krótki czas nieaktywny.
* X: Flawia i antidotum? Nope. Nie zadziałało.
* Vz: Nataniel będzie nieaktywny do końca sesji. "Duch" ma szansę uciec na Valentinie.

Lena namierza Ducha po magicznym polu prywatności. Ma uprawnienia dzięki Frankowi. A jako, że Nataniel był tak blisko - jego dziwaczne stworki są wskazówką. Plus, nie ma Nataniela - nie ma presji czasowej.

TrZ+3:

* V: Lena ma namiar na Ducha i Tomasza Sowińskiego. Znajdują się na ZEWNĄTRZ statku kosmicznego. Niewielka baza przy jednej ze śluz naprawczych.
* Vz: Będzie sygnał jak Duch jest na pokładzie Motyla.
* V: Duch nie będzie w stanie uciec / się wycofać. Lena złapie Ducha w niefortunnym momencie.

Lena przyskrzyniła Ducha w okolicach śmieci. Wyjaśniła Duchowi, że Nataniel Morlan na nią poluje. Duch jest przerażony. To _chyba_ Antonella, ale nie wiadomo do końca. Lena ostrzegła Ducha - musi uciec na Valentinę. Duch ostrzegł, że miragent (Uwodzicielka) próbuje zabić Tomasza i wrobić w to trzech szmuglerów. Lena pokazała zdjęcia załogi - Duch potwierdził którzy.

Lena daje Duchowi komunikator. Jak będą przy stacji to Lena umożliwi Duchowi zejście. I podzieliła się swoimi pieniędzmi z Duchem.

Nataniel chwilowo leży w skrzydle medycznym, pod dobrą opieką. Z Flawią. Lekarze się głowią jak im pomóc.

Szukamy miragenta zanim ten coś zrobi. TrZ+2. 

* X: Miragent wie, gdzie jest Tomasz.
* V: Masz miragenta.
* V: Miragent jest odcięty w śluzie, przed wyjściem w kosmos.

KONKLUZJA:

* Antonella opuściła statek z Tomaszem, ku wielkiemu zdziwieniu Jolanty. Czyli ta dziewczyna jest prawdziwa XD. Tomasz chciał się nią zaopiekować, ale Antonella uciekła - póki Nataniel gdzieś tam jest...
* Flawia dostała od Tomasza szansę. Faktycznie, pomogła mu i go uratowała. Zainteresowali się nią Sowińscy - czemu tak przydatna czarodziejka jest na marginesie wszystkiego?
* Miragent został zamrożony i wyrzucony przez śluzę. Dryfuje gdzieś w kosmosie, najpewniej martwy (ma niewiele czasu).
* Noktianie stojący za miragentem zostali aresztowani. Może eksterminowani?

## Streszczenie

Flawia Blakenbauer miała nadzieję, że Tomasz Sowiński pomoże jej z kłopotami, ale ów zniknął na Gwiezdnym Motylu. Flawia, Szmuglerka i Strażniczka skutecznie znalazły konspirację noktian którzy próbowali przehandlować Tomasza za innych noktian, oraz miragenta próbującego Tomasza usunąć. Do tego zaplątały się dookoła Ducha - eternijskiej szlachcianki uciekającej przed potężnym łowcą magów uważającym że jest jej ojcem. All in all, Tomasz odzyskany, łowca magów uśpiony a Duch uciekł.

## Progresja

* Flawia Blakenbauer: poświęciła reputację i nawet trafiła do aresztu za dziką imprezę z ostrymi środkami psychoaktywnymi. Pikanteria - zrobiła to dla misji i by chronić Tomasza Sowińskiego.
* Tomasz Sowiński: zaprzyjaźnił się z Flawią Blakenbauer i Antonellą Tamaris.

### Frakcji

* .

## Zasługi

* Flawia Blakenbauer: ma nadzieję, że Tomasz Sowiński pomoże jej w karierze/finansach, ale Tomasza porwano na Gwiezdnym Motylu. Przymiliła się do Jolanty Sowińskiej, zdobyła informacje od Nataniela Morlana i poświęciła reputację by chronić Ducha (Antonellę).
* Renata Szarżun: Szmuglerka. Ma dostęp do podłych substancji psychoaktywnych których solidnie używa na Gwiezdnym Motylu. Podkupiła służbę i otruła zarówno Nataniela jak i Flawię. Twarda z półświatka.
* Lena Fenatil: Strażniczka. Odnalazła lokalizację Antonelli, świetnie trianguluje pola prywatności i odkryła gdzie znajduje się miragent. Oczy Zespołu.
* Tomasz Sowiński: żądny przygód młody mag o dobrym sercu; poszedł z niewłaściwą dziewczyną na stronę i został porwany przez miragenta. Pomógł Flawii i Duchowi (Antonelli) wyjść z trudnych sytuacji życiowych.
* Jolanta Sowińska: twinnowana czarodziejka. Zupełnie nie wie jak rozmawiać z innymi. Powiedziała Flawii wszystko co mogła o Tomaszu i dała namiar na jego porywaczy i aktualną lokalizację.
* Nataniel Morlan: potężny eternijski łowca nagród, szukający córki. Nie wierzy, że ona nie żyje i znalazł ją w Duchu. Szuka Antonelli (myśląc że to Marcelina); gdy już prawie mu się udało, został uśpiony przez Zespół.
* Antonella Temaris: eternijska szlachcianka, uciekinierka. Jej ród został wymordowany Krwawą Klątwą przez Nataniela Morlana, którego córka była jej przyjaciółką. Została "Duchem" ukrywając się na Gwiezdnym Motylu.
* Franek Kuparał: przełożony Strażniczki na Gwiezdnym Motylu. Uzależniony od dobrego towaru Szmuglerki, pomógł dziewczynom znaleźć Ducha.
* SLX Gwiezdny Motyl: statek Luxuritias, ogromny i ufortyfikowany rejsowiec. Podróżuje między Astorią, Neikatis, Valentiną. Miejsce "zbrodni" - porwanie Sowińskiego przez noktian, miragent szukający wolności i Nataniel polujący na Ducha.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański

## Czas

* Opóźnienie: -1133
* Dni: 5

## Konflikty

* 1 - Uwodzicielka chce, by Tomasz zwrócił na nią uwagę. Wynik konfliktu: na tyle zainteresowany że zafascynuje się tą damą.
    * TrZ+4
    * VXXX: jest zafascynowany i za nią łazi, ale wszystko wypaplał, Duch wie co się tu dzieje oraz mocodawcy Uwodzicielki wymusili za szybkie działanie.
    * XV: Tomasz wypaplał wszystko, ale poszedł za Uwodzicielką i dostał w łeb
* 2 - Czy uda się Duchowi wsadzić podsłuch w taki sposób, by ani Uwodzicielka ani jej mocodawcy nie byli w stanie tego odnaleźć?
    * Tr+3
    * VVVV: TAK, schowany przed Uwodzicielką, Mocodawcami, śłedzenie gdzie są widoczni przez systemy statku. Plus, podłożenie tego Szmuglerce.
* 3 - Lena znajduje ustronne miejsce i rozpoczyna dyskusję z Persefoną. Niestety, nie da się znaleźć Tomasza bo miał aktywne pole prywatności. Lena chce się dowiedzieć, czy ktoś mieszał w tym obszarze, w kamerach itp.
    * TrZ+3
    * VXVVXV: Przeciwnik WIE gdzie są kamery, dojście do lokalizacji Tomasza - biovaty w magazynach, opiernicz Leny + zakaz jej zbliżania się do biovatów ale Franek Lenie pomoże np. odbić Tomasza
* 4 - Renata chce się dowiedzieć od różnych kontaktów kto manipulował przy taśmie i dostać te taśmy. Sprzedajni, mają dostęp do taśm - o tych chodzi. Kto maczał palce, taśmy.
    * TrZ+3
    * VVXV: Skasowali na prośbę "Jolanty", Renata ma kopię taśm, Jolanta wie że była sprawdzana a Renata wie że Jolanta ma alibi.
* 5 - Flawia chce dać Lenie alibi. Daje info Jolancie o tym, że poprosiła Lenę o polecenie Towarzysza ale nie chciała bezpośrednio więc poprosiła o to by ta sprawdziła z czego Jolanta korzysta.
    * TrZ+3
    * VVVV: Lena ma alibi, Jolanta opowiada o ruchach Tomasza i daje Flawii namiary + kontaktuje się z Tomaszem dla Flawii
* 6 - Lena chce intencyjnie dostać "dziurę". Lena chce znaleźć interesujący obszar. Zwłaszcza po charakterystycznym polu.
    * ExZ+4
    * V: Lena ma dowód Ducha i korelacji Duch - Tomasz
* 7 - Czy Franek jest osobą korzystającą z specjalnych substancji Renaty?
    * 8V = 7X
    * VVVV: TAK, jest też uzależniony i je z ręki Renaty
* 8 - W paczkach i informacjach Lena chce przekazać Duchowi: "otrzymałam Twoją informację o zagrożeniu, próbuję pomóc, daj mi znać czy masz Tomasza". Z pomocą Franka.
    * TrZ+3+5O
    * OOVVzVX: ruch Nataniela *2, Duch zostawia wiadomość Renacie i Renata ma "próbkę" śledzącego bytu. To konstrukty Nataniela.
* 9 - Wiedząc to wszystko Renata rozmawia ze służbą, podkupuje ich - jakim panem jest Nataniel, czy wiedzą o Marcelinie itp. Kolejny datapoint. Czy był konflikt w Eterni? Czy to się działo...
    * TrZ+2
    * VzV: Nataniel ma twardą rękę, Marcelina od niego uciekła, była masakra i wojny rodów
* 10 - Flawia się przeszła porozmawiać z arystokratami. Dużo butelek poznika. Cel - poznać historię Marceliny Morlan. Impreza, raut - a tam jedno z "trivia questions". +1V za substancje wspomagające. 
    * ExZ+4+3Og(Nataniel)+3Ob(Duch)+3Oy(policja)
    * XXOgVXXV: burda u Flawii, Nataniel wie że ona sprawdza, Flawia musi zapłacić karę (a jest biedna) + do kartoteki, wszystko co złe spadło na nią. Ale ma info o Marcelinie i Antonelli.
* 11 - Renata gra spowalniacza z Flawią. Renata poszła do Nataniela, że Flawia ma nowe fakty, chce porozmawiać, chce pomóc z córką ITP. Renata dodaje straszne środki do karafki Flawii by wyłączyć Nataniela.
    * ExZ+3
    * VXVz: Nataniel nieaktywny do końca sesji, ale Flawia też padła
* 12 - Lena namierza Ducha po magicznym polu prywatności. Ma uprawnienia dzięki Frankowi. A jako, że Nataniel był tak blisko - jego dziwaczne stworki są wskazówką. Plus, nie ma Nataniela - nie ma presji czasowej.
    * TrZ+3
    * VVzV: Lena ma namiar na Ducha i Tomasza, zaczaiła się i złapała Ducha
* 13 - Szukamy miragenta zanim ten coś zrobi (dorwie i zabije Tomasza). Działania Leny.
    * TrZ+2
    * XVV: Miragent już dociera do Tomasza, ale przechwycony po drodze, w śluzie.

## Inne
### Projekt sesji
#### Procedure

* Overarching Theme:
    * gracze mają "kierowanie" (Vision & Purpose); wiedzą gdzie iść i po co tam idą
    * gracze są bardziej zaangażowani w historię, bo widzą, gdzie ich decyzje prowadzą
    * MG wie, które sesje wprowadzać i które usuwać i w którą stronę stawiać adwersariat
* Wyraziste, ważne dla graczy postacie + Relacje (konflikt?) Gracze <-> NPC, NPC <-> NPC
    * graczom bardziej zależy na postaciach i ich powodzeniu
    * więcej potencjalnych wątków i rzeczy które możemy odrzucić przy krzyżykach
* Tory, zmiana postaci z czasem, wpływ graczy na świat
    * gracze widzą jak wpływają na świat. Wpływ jest "ich" - stąd tak tragiczne jest gdy "ich" rzeczy są corrupted / destroyed.

#### Result

* Overarching Theme: WOLNOŚĆ (miragent, Flawia, noktianie, też: Tomasz)
    * V&P:
        * 
    * adwersariat: 
* Wyraziste, ważne dla graczy postacie + Relacje (konflikt?) Gracze <-> NPC, NPC <-> NPC
    * .
* Tory, zmiana postaci z czasem, wpływ graczy na świat
    * .

#### Scenes

* Scena 0: 
* Scena: 

### Kształt statku:

* Publiczne sektory (za rysunkiem):
    * Salon Centralny (przestrzeń publiczna)
    * Jadalnie
        * Grill i restauracje
    * Sporty
        * Marina
        * Gry sportowe
        * Połączenie z Virt
    * Kultura
        * Kina, holoprojektory, teatr
        * Centrum Fitness    
    * Rozrywka
        * Kasyno, zakłady, wyścigi psów...
        * Kluby
        * Miejsca Konwersacji (możliwość rozmowy z zaproszonymi na statek / Persefoną)
    * Spoczynek
        * Luksusowe kabiny gości
* Niepubliczne sektory
    * Zaopatrzenie
        * Syntezatory dóbr
        * Magazyny
            * w tym: Biovaty dla Towarzyszy
            * w tym: roboty obsługujące statek
        * Kostnica
        * Ładownie
        * ŚMIECI. Recykling. MNÓSTWO ŚMIECI. I zbierania śmieci.
    * Rdzeń statku
        * System komunikacji
        * Nawigacja, sensory
        * System podtrzymywania życia
        * Engineering
        * Reaktor
        * Projektor ekranu ochronnego
        * Uzbrojenie 
        * Rdzeń AI
    * Części mieszkalne
        * Kabiny dowództwa
        * Kabiny załogi
    * Inne
        * Medical Treatment: 50 osób
        * Hibernation Suites
        * Containment Chamber

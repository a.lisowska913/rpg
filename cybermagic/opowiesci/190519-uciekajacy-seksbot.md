---
layout: cybermagic-konspekt
title: "Uciekający seksbot"
threads: nemesis-pieknotki
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [190503 - Bardzo nieudane porwania](190503-bardzo-nieudane-porwania)

### Chronologiczna

* [190503 - Bardzo nieudane porwania](190503-bardzo-nieudane-porwania)

## Budowa sesji

### Stan aktualny

* Miasteczkowcy stwierdzili, że chomikowanie czarnych artefaktów to cholernie zły pomysł. Czarnym miejscem zostają Wolne Ptaki.
* Część Miasteczkowców, jak chciała Karla, przeniosło się do Wolnych Ptaków.
* Część Miasteczkowców woli odsprzedać artefakty Wolnym Ptakom, ale niewielka ich część.
* Wolne Ptaki stają się tyci bardziej znaczącą frakcją i lokalizacją.
* Pojawia się prototyp szlaku handlowego między Wolnymi Ptakami i Cieniaszczytem.
* Gdy Nikola będzie odpalać plany przemytu, Pięknotka będzie wiedziała (ścigacz)
* Artefakty Czarnej Technologii trochę są porozrzucane po terenie.
* Eliza Ira straciła swoją sieć szepczących kryształów. Nie połączy się z nikim aktywnie; teraz to może tylko odpowiadać na zapytania.
* Pięknotka wykorzystała swoje wszystkie znajomości - zaczyna małą kampanię antyElizową na tym terenie, "whispers from the crystals".
* Wpływ Elizy wzrósł. Są osoby wiedzące o niej i o jej ideałach i są osoby chętne by jej pomóc. Ale nie w Pustogorze i nie u kogokolwiek ważnego.

### Pytania

1. Czy biedny Arnulf ucierpi za swoje dobre serce?

### Wizja

* Seksbot ze wspomaganym chipem wojskowym, zabójca (XD) w Szkole Magów, na zapleczu
* Element planu Elizy by uwolnić / zniszczyć Ataienne; randomowy eksperyment
* Grzymość jest nieszczęśliwy, a tym bardziej Ernest który chce go odzyskać

### Tory

* Seksbot niszczy reputację Arnulfa: 3
* Seksbot ucieka na wolność: 4
* Seksbot zostaje zniszczony: 4
* Eliza ma potrzebne dane: 3

Sceny:

* Scena 1: Hańba w szkole

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Misja właściwa

**Scena 1: Hańba w szkole**

To miała być rutynowa wizyta u Wiktora Sataraila. A jednak nie była. Pięknotka znalazła się w szkole magów gdzie Ernest Kajrat i Teresa Mieralit krzyczą na siebie. Z tego co Ernest krzyczy, Arnulf (dyrektor) ukradł Ernestowi najbardziej epickiego seksbota w historii. Z tego co krzyczy Teresa, Ernestowi totalnie odwaliło. Reputacja dyrektora jednak, no, jest na włosku.

Arnulf poprosił Pięknotkę, by to rozwiązała. Poprosił też o Minerwę; ale Pięknotka nie ma jej pod ręką. W ogóle, Arnulf nie zachowuje się jak on.

Pięknotka stanęła przeciwko Ernestowi; chce:

* by Teresa nie ucierpiała w żadnym stopniu
* by Arnulfowi nic się nie stało
* zachować zdrowe relacje z mafią
* deeskalacja Ernesta

(Tr+3: VV). Ernest się zgodził Pięknotce zaufać. Teren jest dla niego ważny. Spokój i bezpieczeństwo też. Niech Pięknotka robi po swojemu (Pięknotka ma takie duże "wow") - niech Pięknotka powie o nim dobre słowo do Karli odnośnie tych tematów kralotycznych. Oddalił się, Teresa prychnęła jak kotka. Uśmiechnęła się do Pięknotki. Aha, i Ernest dał Pięnotce częstotliwość namierzania.

Pięknotka spławiła Teresę i poszła do dyrektora. Namiar wskazuje na... nieużywane skrzydło w szkole, do którego jest połączenie z gabinetu dyrektora. Wchodzi do dyrektora i zagaja. I tam się dowiaduje, że dyrektor w pewien sposób się martwi - przez jakiegoś ucznia ma faktycznie seksbota. Ale ten seksbot jest chyba żywy. I po to mu Minerwa...

Mieli dość ciekawą dyskusję odnośnie tego co to znaczy "życie". Zgodnie z prawem seksbot nie może być żywy, ale tylko psychotronik klasy Minerwy może się upewnić. Ernest jest delikwentem okrutnie traktującym swojego bota, ale lepiej niech się wyżywa na botach niż na ludziach czy magach... co gorsza, bot wygląda jak Atena Sowińska, żeby już w ogóle było dziwnie.

Pięknotka prawie spadła z krzesła.

Arnulf w rozmowie z Pięknotką powiedział, że chce, by - jeśli jest tylko botem - została zniszczona. Pięknotka twierdzi, że zgodnie z prawem, należy do Kajrata jeśli jest botem. Arnulf się na to nie zgadza - trzeba ją zniszczyć. Pięknotka nie chce z nim na ten temat rozmawiać teraz.

Pięknotka ściąga aktywnego terminusa z Pustogoru który jest jednocześnie neuronautą. Tomasz Tukan. Przybył terminus do Pięknotki, są we dwójkę. Jak Tukan usłyszał, że ma wejść w umysł seksbota to się żachnął. On tego nie robi. Pięknotka zauważyła, że ma takie polecenie, więc wejdzie. Pięknotka w ogóle jest pod wrażeniem - Tukan wyjątkowo nie lubi seksbotów. Bardziej niż przeciętny mag.

Pięknotka spytała hipernetem Ernesta - jak ciężko zmieniony jest ten seksbot. Dostała smutną odpowiedź. Bardzo - ma sporo modyfikacji, ale nie jest w stanie zrobić niczego terminalnego żadnemu magowi. Pięknotka poprosiła o listę modów i sprzętu seksbota. Dostała i ją zmroziło - kryształowa biżuteria. Kryształ Elizy, zanim doszło do zniszczenia jej sieci. Czyli to najpewniej wpływ Elizy. Gdy Tomek się dowiedział, poważnie się zmartwił. Teraz chce wejść w umysł seksbota z nadzieją, że dojdzie do Elizy. Nie obchodzi go seksbot - obchodzi go Eliza.

Dobra. Weszli do skrzydła w którym Arnulf zamknął seksbota. Ku przerażeniu Arnulfa i zaskoczeniu Pięknotki, znaleźli oczy. Jako, że w oczach były trackery, seksbot... wyciął sobie oczy. Pięknotka doceniła - jest to zdecydowanie ruch w stylu Ateny.

**Scena 2: Poszukiwanie seksbota**

Pięknotka chce użyć próbek seksbota, oczu, do znalezienia reszty istoty. Jest silnie zmotywowana - niby seksbot nie jest niebezpieczny, w praktyce nigdy nie wiadomo. (TrMZ+2=REROLL,SZ). Pięknotce udało się to zrobić tylko dzięki temu, że ma oczy tej istoty; niestety, zaklęcie połączone z elementami emocji wciągnęło ją w wizję.

Oczywiście, trafiła do sceny Ernesta bawiącego się z botem. Ernest jest PSYCHICZNY. Zadaje ból wzmocnionemu ciału viciniusa; przekracza to, co normalne ludzkie ciało maga mogłoby znieść. Ernest jest po prostu PSYCHICZNY, to potwór. Pięknotka z takim potworem jeszcze do czynienia nie miała. To są metodyczne tortury bez celu, dla czystej przyjemności. Pełen sukces - Pięknotka przetrwała.

Złamane, zniszczone ciało Pięknotki na ziemi. Pięknotka płacze tym, co jej zostało po wypaleniu oczu. Poczuła przytul, który miał dać jej ukojenie. Szept, prośbę, by wytrzymała. I gniew tej drugiej istoty - to niesłychane, jakie szkody zadał swojej siostrze (pobratymczyni z Astorii).

Pięknotka wróciła do siebie, podłamana. Uszkodzona. Skrzywdzona. Tukan spojrzał na nią z obawą - pomóc? Nie. Ale Pięknotka ma detektor. Tukan się zdziwił, gdy Pięknotka powiedziała, że "ona" jest zdesperowana. Powiedział, że to zależy do czego jest zaprogramowana. Pięknotka nie chce z nim o tym rozmawiać; woli pójść przodem.

Pięknotka zbliża się do pomieszczenia. Widzi, gdzie pobiegł seksbot. Biegł, uderzył się, przewrócił i zakrwawił, po czym biegł dalej. Pięknotka poczuła coś w następnym pomieszczeniu; cel jest niedaleko, ale muszą przejść przez następne pomieszczenie. Kazała Tukanowi ją osłaniać; sama otworzyła drzwi gotowa na atak. Pomieszczenie ma kotary, jest stosunkowo ciemne. Tam jest "cień" seksbota, wygląda jak ona. Wycięte oczy, przerażona, prosi by nie robić jej krzywdy.

Pięknotka zapaliła światło. Zobaczyła, że za kotarami jest... coś. Coś wskazującego na lustro. Pięknotka wie, że to pułapka. Spieszy się. Walnęła w lustro z siły ognia - jeśli to może być lustro... (Tp:S). Pięknotka rozsypała lustro. Za kotarą znalazło się lustro złożone z kryształu. Delikatne, cienkie, wyhodowane ad-hoc. Cień seksbota rozsypał się w agonii. Pięknotka usłyszała szept "wszyscy są tacy sami".

Pięknotka szybko dotarła blisko pomieszczenia. Tam usłyszała krzyk strasznego cierpienia ze strony seksbota. Poczuła energię magiczną arcymagini kryształów. Arcymagini - Eliza Ira - uruchomiła magię krwi i zamanifestowała coś używając cierpienia seksbota.

Pięknotka i Tukan wpadają w akcji terminuskiej. Seksbot jest przebity przez iglicę z kryształów połączoną z jej układu nerwowego. Iglica zadaje silne cierpienie seksbotowi i katalizuje to w energię i siłę ognia. Eliza nic nie mogła wygrać tym strzałem; ale i tak strzeliła. Wysłała sygnał do obecnych "i to nazywacie terminusami na Astorii?"

Tukan odwraca uwagę iglicy. Pięknotka robi unik, wpada na seksbota, szepcze "no need to suffer" i całuje ją w policzek - niech painkillery wyłączą cierpienie. Wyłączy to broń Elizie i, no... niech nie cierpi. (Tr:S). Pięknotce się udało; Eliza eksplodowała swój kryształ. Próba coup de grace, nieudana. Ale przynajmniej seksbot wypadł przez okno. Eliza spróbowała jeszcze coś powołać, ale nie ma tu mocy; kryształy wygasły.

Pięknotka i Tukan wyskoczyli przez okno. A tam - koło zdewastowanego seksbota dwójka magów - Napoleon Bankierz i Liliana Bankierz. Oboje przyszli się bić jak nikt nie patrzy... bo się bardzo nie lubią. Natychmiast przerwali walkę w obliczu Ateny Sowińskiej. Pięknotka zeskoczyła i im się zidentyfikowała. Powiedziała, że nie ma o czym mówić - mają być cicho. Liliana krzyknęła do Pięknotki, że ona MUSI uratować seksbota. I Liliana czyni ją osobiście odpowiedzialną.

Pięknotka wskoczyła z powrotem do góry. Jest koło Tukana.

Po małej kłótni Tukana i Pięknotki (on nie chce w nią wchodzić, ona nie chce oddać bota mafii), doszli do porozumienia: przekażą seksbota do Senetis. Tyle, że Pięknotce to bardzo nie pasuje...

**Scena 3: Kontemplacja po wszystkim**

Seksbot wyglądający jak Atena i pragnący umrzeć trafił do Senetis. Pięknotka chodzi po pokoju, cała zirytowana całą sytuacją. Nie chciała, by tak się skończyło. Naprawdę, nie o to chodziło. Ale co może zrobić?

Odezwała się do Saitaera. Władca Ewolucji obiecał jej pomoc w zamian za nałożenie geasa - Pięknotka pozna jedną z jego lokalizacji ale nie wykona aktywnej aktywnej akcji przeciwko tej lokalizacji. Terminuska się zgodziła. Władca Ewolucji znalazł też miejsce dla "seksbota" w nowym świecie. Saitaer zauważył, że seksbot jest niezdolny do posiadania własnej przyszłości - nie zna nikogo, nie zna świata i ma historię cierpienia. Zrobi z niej "terminusa". Zrobi z niej strażniczkę swojej świątyni. Pięknotka, z obawami, ale się zgodziła.

Saitaer powiedział Pięknotce, że ona odda na pewien czas Cienia seksbotowi. A potem otrzyma go z powrotem. Zapytany, wyjaśnił:

* Cień pożre część "zła" i cierpienia, które były na seksbocie.
* Cień pomoże seksbotowi wejść i wyjść. No i jest kontenerem energii ixiońskiej.
* Cień posłucha Saitaera. Nie jest jego istotą, ale rozumie potęgę boską - jak pies.

Pięknotka się zgodziła na wszystko powyżej. Jak tylko seksbot wyjdzie z Senetis, zajmie się tym. I wraz z Saitaerem pomogą nieszczęśniczce.

**Sprawdzenie Torów** ():

* Seksbot niszczy reputację Arnulfa: 3
* Seksbot ucieka na wolność: 4
* Seksbot zostaje zniszczony: 4
* Eliza ma potrzebne dane: 3: 2
* Konflikt Ernest - Szkoła - Pięknotka: 4: .

**Epilog**:

* 

## Streszczenie

Ernest z mafii przybył do szkoły magów w Zaczęstwie szukając swojego seksbota. Pięknotka deeskalowała sytuację i poszła z neuronautą Tomaszem szukać; okazało się, że seksbot jest bardzo zmodyfikowany i bardzo świadomy - do tego stopnia, że seksbotowi zaczęła pomagać Eliza Ira. Pięknotka i Tomasz rozmontowali problem, ale Pięknotka nie miała serca oddać seksbota sadystycznemu Ernestowi. W rozpaczy, poprosiła o pomoc Saitaera, który odpowiedział...

## Progresja

* Pięknotka Diakon: o krok bliżej do Saitaera. Wezwała go, a on odpowiedział.

### Frakcji

* .

## Zasługi

* Pięknotka Diakon: od negocjacji z Ernestem, przez walkę z Elizą aż do prośby wobec Saitaera o uratowanie seksbota. Bardzo trudny okres.
* Ernest Kajrat: dżentelmen i oficer mafii, który - jak się okazuje - ma bardzo mroczne zapędy. Zarówno sadystyczne jak i wobec Ateny. I nic a nic nie ukrywa swojej natury. Nic dziwnego, że się go boją.
* Arnulf Poważny: dyrektor, który przedkłada potencjalnie żywego seksbota nad pozycję i nad zadowolenie Ernesta Kajrata z mafii.
* Teresa Mieralit: pancerz dyrektora w szkole magów, zwalczająca Ernesta i biorąca potencjalny ogień na siebie. Co udowadnia, że jest baaardzo nierozsądna.
* Tomasz Tukan: neuronauta Pustogoru, terminus, który nie chce podpadać Ernestowi. Nienawidzi seksbotów, całkowicie. Najchętniej zadowoliłby mafię nawet kosztem żywej istoty.
* Eliza Ira: mimo osłabionego połączenia robiła co była w stanie by móc pomóc seksbotowi, by ów mógł być wolny i nie musiał wracać do Ernesta. Utraciła połączenie przez Pięknotkę.
* Liliana Bankierz: żądała od Pięknotki, by ona pomogła biednemu seksbotowi. Nie udało jej się zmusić Pięknotki do niczego. Najpewniej uczestniczyła w wykradzeniu seksbota Ernestowi.
* Saitaer: wezwany przez Pięknotkę, odpowiedział, że pomoże seksbotowi i da mu albo wolność albo dobrą śmierć. O krok bliżej do ewolucji Pięknotki.
* Ossidia Saitis: seksbot bardzo źle traktowany przez Kajrata; pomaga mu Eliza Ira oraz Pięknotka przekazała go Saitaerowi, by seksbot nie cierpiał.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Zaczęstwo
                                1. Akademia Magii, kampus: uciekł tu seksbot z pomocą Liliany a za nim Ernest, który chciał go z powrotem.
                                    1. Budynek Centralny
                                        1. Skrzydło Loris: nieużywane skrzydło Budynku Centralnego AMZ. Ucieczka seksbota od Kajrata skończyła się na walce tutaj pomiędzy seksbotem i Pięknotką(?).

## Czas

* Opóźnienie: 5
* Dni: 1

---
layout: cybermagic-konspekt
title: "Przybycie Rekina z Eterni"
threads: rekiny-a-akademia, corka-morlana, amelia-i-ernest
gm: żółw
players: anadia, kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [210831 - Serafina staje za Wydrami](210831-serafina-staje-za-wydrami)

### Chronologiczna

* [210831 - Serafina staje za Wydrami](210831-serafina-staje-za-wydrami)

### Plan sesji
#### Co się wydarzyło

.

#### Sukces graczy

* .

### Sesja właściwa
#### Scena Zero - impl

.

#### Scena Właściwa - impl

Marysia słyszy ogólne niezadowolenie dobiegające zwłaszcza ze strony Torszeckiego. Nie na Marysię. Ogólnie, ostatnio były hałasy i trudno było nie zauważyć że pojawiła się ŚWITA. Do jednego z luksusowych apartamentowców, NALEŻĄCYCH DO SOWIŃSKICH kanonicznie wprowadziła się OGROMNA ekipa ludzi. 50+.

Marysia ma może 10 osób w swojej służbie. Są dyskretni. Pokojówka, kucharz, 2 ochroniarzy... takie tam. Standardowy luksus Sowińskich. A tam - 50+ osób. Torszecki jest niezadowolony.

Marysia jest DRUGĄ W HISTORII Sowińską która jest na wygnaniu jako Rekin. Pierwszą była Amelia... i wszyscy wiemy jak to się skończyło. Śmierci ludzi, Wydry, teraz Serafina...

Marysia natychmiast skontaktowała się ze swoim ulubionym kuzynem, Lucjanem, który jest w Aurum. Ten powiedział, że Jolka coś spieprzyła i ród trochę żyje jej spięciami z tien Morlanem.

Lucjan nie ma pojęcia, by ktokolwiek zrobił coś niewłaściwego i był zesłany na prowincję. Ktoś się wbija na chatę Sowińskim? XD. To by było głupie. Ale w sumie jak Marysia wie, Pustogor by to łyknął... oni by olali temat.

Marysia zdecydowała się poczekać i zobaczyć co się stanie.

TYMCZASEM KAROLINA.

Karol Pustak jest od dawna kontaktem Karoliny. Karol jest kolesiem, który studiuje na AMZ i znalazł swoje przeznaczenie - wiedzieć WSZYSTKO o rodach Aurum. Próbuje robić spekulacje, interesować się... można powiedzieć, jest "socjologiem Aurum". Kimś, kogo niektóre Rekiny by posłuchały by dowiedzieć się "co na serio mieli na myśli mówiąc / robiąc określone rzeczy". Taki royal-fan. I przekształca swoją wiedzę w gazetę plotkarską, którą sprzedaje. (redaktor Pudelka)

O dziwo, kupowana jest w Aurum. Mniej o dziwo, jako komedia.

Pustak skontaktował się z Karoliną Terienak. Zadał jej jedno pytanie - "dlaczego do apartamentów Sowińskich wprowadza się mag z Eterni?" Jest całkowicie zafascynowany arystokracją itp. Wie o rodzie Karoliny więcej niż sama Karolina i KONSEKWENTNIE PRÓBUJE SIĘ OD NIEJ DOWIEDZIEĆ "tajnych sekretów rodu Terienak".

Np. to, że ród Terienak stał się bardziej znaczący podczas wojny m.in. z uwagi na swoje wybitne osiągnącia w ramach pilotażu myśliwców.

KAROLINA -> MARYSIA po hipernecie

* "Hej laska, co Twoim odpierdziela że eternianina ściągają do Twoich terenów?"
* "Kto wie jakie myśli moim bliskim... jakiego eternianina? Wiesz coś więcej?"

Jak widać, Marysia i Karolina są bliżej - bo Karolina ma gdzieś protokół i Marysia to docenia bo jest szczera.

Marysia przyznała, że ona nic nie wie. Lucjan też nic nie wie. Muszą się dowiedzieć o co chodzi niekoniecznie mówiąc "Sekretom Aurum" (gazeta Pustaka) o co chodzi... Marysia NIE chce się spotkać z "Sekretami".

MARYSIA -> LUCJAN

"Hej, udało ci się coś ustalić?" Lucjan szukał, dyskretnie, co tu się stało.

TrZ+3+3O (on się dowie):

* Vz: Lucjan dowiedział się, że za tym stoi Amelia bo jest zaufany z rodu.
* X: Amelia wie, że Lucjan pytał dla Marysi.
* V: Eternianin nazywa się Ernest Namertel, jest dość "niegroźny" z charakteru. Jest powiązany z Morlanem pośrednio. Jest młodym magiem, którego Morlan swego czasu mentorował, więc jest niezły w walce.
* Xz: Amelia uważa, że tego typu przeszukiwania mogą zakłócić delikatny balans. Jest wściekła na Marysię że wysłała "gorylowatego Lucka" na przeszpiegi.
* V: Amelia próbuje deeskalować problemy spowodowane przez Jolantę. 
    * Zaproponowała wymianę (żeby lepiej się wszyscy zrozumieli) - ona jedzie do Eterni, do Morlana. A on przyśle kogoś do Aurum. 
    * I Eternia przysłała - ale ku zdziwieniu wszystkich - na prowincję. Morlan chciał, by Ernest trafił nie do Aurum a do Szczelińca. Nikt nie wie czemu.
    * Aurum uważa, że Ernest jest wysłany po to, by coś mu się stało. By Morlan miał pretekst. Czyli - lepiej, by nic mu się nie stało.
    * Wśród Sowińskich są osoby, którym nie przeszkadza, że Amelii coś się stanie ;-). Więc może utrata Ernesta na prowincji i utrata Amelii w Eterni nie jest taka zła ;-).

AMELIA żąda kontaktu z MARYSIĄ. Złotowłosa Amelia, z lokami, NIGDY nie jest uśmiechnięta. Marysia z przyjemnością porozmawia z Amelią...

* Marysia wyszła od "mogliśmy zrobić mu krzywdę"
* Amelia się uśmiechnęła "spróbujcie :-)"
* Marysia: "Amelio, dzięki Tobie się dużo nauczyliśmy. Szuka Cię Serafina, wiesz, ludobójstwo parę lat temu..."
* Amelia blankuje. "A, tamto. Mogliby już sobie darować. To było dawno."

Co Marysia może się dowiedzieć czego Amelia NIE CHCE jej powiedzieć (clues, secrets):

ExZ+4:

* V: Co może Amelię ZRANIĆ: Ernest nie ma pojęcia jakim ziółkiem jest Amelia. Morlan też tego nie wie. Jakby Ernest się dowiedział, że Amelia jest odpowiedzialna za coś takiego, to może być "ciekawiej".
* (+3Or za szantaż) 
* XXz: Amelia robi coś ważnego dla rodu. Marysia jest "lol rekin". Amelia ma delikatną pozycję, więc lepiej niech Marysia nie robi głupot. Bo może uszkodzić Amelię - i wyjątkowo uszkodzi to tak samo ród i za to Marysi nie podziękują.
* X: Marysia jest osobą, która musi zapewnić że sekret Amelii nie wyjdzie przed Ernestem.
    * + Amelia nie wiedziała, że Marysi nie powiedzieli o tym, że tu przybędzie Ernest. Amelia wysłała sygnał. Marysia miała wiedzieć tydzień temu.
* (+2Vz na zachętę) V: Amelia była osobą, która przekonała Ernesta, by przybył tu, na prowincję. Bała się, że:
    * Ernest zobaczy jakie jest Aurum i wzgardzi jej rodem bardzo mocno.
    * Ernestowi coś się stanie. Albo bo "krzywdę Amelii" albo "Eternia jest zła".
    * Np. że Jolka go porwie, coś mu zrobi, przeczyta pamięć... w queście vs Morlanowi
* V: Amelii **personalnie zależy** na Erneście. I on ją też lubi. To potencjalny mezalians (dla Amelii). To może jej zrobić straszną krzywdę.
* X: Amelia zadba o to personalnie (z sukcesem), by Marysia odpowiadała za bezpieczeństwo Ernesta na tym terenie przed Sowińskimi. W końcu jest jedną z nich ;-).
    * + Amelii wymsknęło się. Ona wzięła to na siebie z mandragorą. Ale ona tego nie zrobiła. Amelia chroniła kogoś.

Rozmowa się skończyła. Marysia uznała ją za wybitnie pouczającą :-). Marysia dzwoni do Karoliny. Czy chce się napić? ;-).

Marysia wtajemniczyła Karolinę odnośnie mandragory i tego, że Amelia przy wszystkich swoich wadach tego konkretnego NIE ZROBIŁA... ale pominęła fakt zakochania. Ale dodała - trzeba chronić Ernesta -_-. Bo na Marysię to spadło...

Czy w jakikolwiek sposób blokujecie to żeby Pustak nic nikomu nie powiedział? Czy w ogóle warto to robić?

Tak - seria autoryzowanych artykułów. By "powitać, pokazać jacy fajni" + kontakt który można wykorzystać.

KAROLINA -> PUSTAK:

Karolina odzywa się do Karola Pustaka - Marysia z nim porozmawia. Pustak 100% radości. Kiedy ma się stawić? Następnego dnia.

Ok. Czas rozwiązać problemy dalsze. (TEST SZCZĘŚCIA: XXV, będzie info ale później - Karolina się czegoś dowie działając z Arkadią)

Wpierw Pustak i Marysia. Pustak przybył do Marysi ubrany w najbardziej elegancki strój jaki Marysia widziała. Ten strój ma konkretne barwy, dał Marysi 7 kwiatów - 3 X i 4 Y, w konkretnych kolorach. Na pewno chciał coś jej tym powiedzieć. Marysia nie ma pojęcia co.

Tr+3:

* X: Marysia źle to trochę zinterpretowała. Tzn. wie jaki jest SENTYMENT ale nie wie dokładnie o co chodzi.
* V: To jest coś całkowicie bez znaczenia. Jakiś jeden tien Sowiński kiedyś to wymyślił i nigdy nie weszło. On użył kodu kwiatowego i kodu stroju zaproponowanego przez jakiegoś... "bardzo znudzonego Sowińskiego". Że to odgrzebał XD.

"REDAKTORZE Karolu. Mogę do Ciebie mówić po imieniu, prawda?" - Marysia, kupując sobie Karola Pustaka chyba do końca życia.

Karol bezkonfliktowo łyknął serię artykułów o arystokracji. Marysia wyłuszczyła mu, że przybywa gość z Eterni i należy się pokazać z jak najlepszej strony i dobrze go przywitać. Zaprezentować mu i innym korzyści z tego wynikające.

Karol zadał dobre pytania - jak miejscowi, którzy mają "kosę" z Rekinami momentami - jak oni przyjmą Eternianina? Arystokracja Eterni słynie z bezwzględności i używania Esuriit.

Marysia proponuje, by Karol i ona popracowali nad ocieplaniem wizerunku tego konkretnego Eternianina. Karol się ucieszył - ostrzegł tylko, że w okolicy jest więcej zbiegłych niewolników z Eterni. Oni mogą się na Ernesta zaczaić.

Marysia powiedziała, że weźmie to pod uwagę.

Marysia (strategicznie) i Karol (piórem) próbują opracować takie artykuły, które sprawią, że Rekiny szybciej uznają Ernesta za godnego współpracy. "Jeden z nas".

ExZ+3+3Or (Or: reprezentują "Marysia x Eternia"):

* X: Osoby nienawidzące Eterni wiedzą, że jest tu eternijski Rekin.
* Vz: Rekiny w swojej masie widzą wartość z Eternianina - nie będzie szoku.
* X: Osoby nie lubiące Marysi mają "łatwy atak" uderzając w Ernesta. Ernest jest "słabym punktem" Marysi.
* Or: Rekiny przyjmą Eternianina dość szybko, BO MARYSIA SOWIŃSKA TEGO CHCE I ONA ZA TYM STOI.

Karolina. Jak neutralizować cholerną Arkadię? Jedyny sensowny sposób - wsadzić ją do kicia. NIE! Jest coś lepszego. Niech Tukan zaproponuje terminusom "oswójmy Rekiny i zaprośmy jednego z nich do współpracy z terminusami". I wysłać ją gdzieś poza Podwiert.

Tukan próbuje złożyć taki program, by terminusi zyskali i by Arkadii się podobało:

TrZ+3:

* V: Tukan będzie miał za to bonus, szacunek i zadowolenie Pustogoru.
* V: Arkadia trafi na taki program. Faktycznie się przyda i to zadziała. Arkadia zneutralizowana w kontekście pierwszego miesiąca bycia tu Ernesta (czyli +30 dni).
* V: Arkadia wyjdzie z tego zadowolona, zdrowa, z podniesioną opinią u Pustogoru.

DWA DNI PÓŹNIEJ przybył Ernest. Przybył spokojnie, z czterema ochroniarzami. Przyleciał na ścigaczu który ma autopilota (bardzo kiepsko prowadzi). Z nikim szczególnie nie rozmawiał...

(Karolina się zapyta Marysi jak to się stało że nie doszła do niej informacja o Erneście)

## Streszczenie

Do Rekinów dołącza tien z Eterni. Marysia dowiaduje się, że to kwestia intryg jej kuzynki, Amelii, próbującej deeskalować problemy na linii Morlan - Jolanta. Dowiedziała się od Amelii, że to nie Amelia stała za morderstwami w Podwiercie, kogoś chroni. Oraz... że Amelia jest zakochana (Amelia nie wie że Marysia wie). Ale na Marysię spadła ochrona Ernesta. Więc... usunęła wszystkie zagrożenia o których pomyślała (Arkadia) i zaczęła działania mające poprawić reputację Eternianina, by Rekiny go przyjęły.

## Progresja

* Marysia Sowińska: osobiście odpowiedzialna by Ernestowi Namertelowi nic się nie stało (manipulacja Amelii). Jej słaby punkt.
* Marysia Sowińska: ma świetny materiał do szantażu / mezaliansu - Amelia Sowińska kocha się w Erneście Namertelu z Eterni, a przecież Amelia ma dostęp do rzeczy których Eternia nie powinna nigdy wiedzieć.
* Tomasz Tukan: zadowolenie ze strony zarówno Arkadii Verlen jak i Pustogoru - znalazł dla niej coś fajnego: fuszkę "uczennicy terminusa".
* Arkadia Verlen: Arkadia się przyda Pustogorowi i będzie zneutralizowana na 30 dni. Plus wyjdzie zadowolona, zdrowa, z podniesioną opinią w Pustogorze.

### Frakcji

* .

## Zasługi

* Marysia Sowińska: odkryła sekrety Amelii Sowińskiej - zarówno ten o zakazanej miłości jak i to, że to nie Amelia zabiła ludzi. Zaczęła pracę nad akceptacją Ernesta wśród Rekinów i manipulacjami pousuwała większość oczywistych zagrożeń. Okazuje się, że ma kuzyna Lucjana który ją lubi.
* Karolina Terienak: połączyła Pustaka (osobę wiedzącą wszystko o Royalsach) z Marysią. De facto Pustak Karolinie teraz trochę wisi.
* Rafał Torszecki: nieprawdopodobnie zazdrosny o świtę Ernesta Namertela; to Marysia Sowińska powinna mieć największą świtę wśród Rekinów. Przeprowadził PLANY by pozycja Marysi wróciła do najwyższego poziomu / punktu.
* Lucjan Sowiński: kuzyn Marysi który ją lubi. Dla niej poszperał w Aurum na temat misji Amelii i jakkolwiek naraził się na gniew samej Amelii, ale odkrył problem na linii Amelia - Jolanta. Uważany za niezdarnego politycznie.
* Karol Pustak: uczeń AMZ i miłośnik "Royalsów". Wie więcej o arystokracji Aurum niż oni sami. 
* Ernest Namertel: rekin z Eterni, kiedyś mentorowany przez Morlana więc niezły w walce; zakochany ze wzajemnością w Amelii Sowińskich (ich związek jest sekretem). 
* Amelia Sowińska: ona trafia do Eterni "na wymianę", do Morlana; próbuje chronić swojego kochanego Ernesta i zamiast do Aurum przekierowała go do Podwiertu. Próbuje deeskalować starcie Morlan - Jolka Sowińska. Okazuje się, że Amelia ma dostęp do rzeczy jakich Eternia nie powinna nigdy wiedzieć (dokumenty, uprawnienia), więc popełnia "zdradę" lub "mezalians". AMELIA idzie za sercem. Skonfliktowana politycznie z Jolantą Sowińską. Aha, przyznała się w emocjach Marysi, że to NIE ONA stoi za morderstwami ludzi w Podwiercie. Kogoś chroni. Aha, Amelia wrobiła Marysię przez Aurum w to że ta PERSONALNIE odpowiada za bezpieczeństwo Ernesta.
* Jolanta Sowińska: jej ruchy mające uwolnić osoby spod kontroli Nataniela Morlana są zauważone przez Morlana i stanowią coraz więcej problemów dla rodu Sowińskich. Skonfliktowana politycznie z Amelią Sowińską.
* Nataniel Morlan: w pełni świadomy tego co robi Jolanta Sowińska; akceptuje plan Amelii Sowińskiej w którym Amelia przyjeżdża do niego na wymianę a na jej miejsce jedzie Ernest Namertel.
* Tomasz Tukan: zamiast wsadzić Arkadię za niewinność do więzienia (by usunąć ją jako zagrożenie dla Ernesta Namartela) wkręcił ją w program dla uczniów terminusa. I tak zakręcił, że mu podziękowali XD.
* Arkadia Verlen: uznana za zagrożenie dla Ernesta przez Marysię (słusznie). Tukan zakręcił, by uczestniczyła w terminuskich akcjach poza Podwiertem. Ona jest szczęśliwa, Tukan ma profity, Pustogor ma kompetentnego "agenta".

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Podwiert
                                1. Dzielnica Luksusu Rekinów
                                    1. Serce Luksusu
                                        1. Apartamentowce Elity: sprowadza się do największego apartamentowca Sowińskich tien z Eterni z ogromną świtą. Phi.

## Czas

* Opóźnienie: 10
* Dni: 5

## Konflikty

* 1 - "Hej, udało ci się coś ustalić?" Lucjan szukał, dyskretnie, co tu się stało - skąd eternianin wśród Rekinów Aurum?
    * TrZ+3+3O (on się dowie)
    * VzXVXzV: Lucjan dowiedział się o Amelii i ma fakty o eternianinie. Wie też czemu Amelia to zrobiła - deeskaluje to, co zrobiła Jolka Sowińska przeciw Morlanowi. A Amelia jest wściekła i hipernetuje Marysię.
* 2 - AMELIA żąda kontaktu z MARYSIĄ. Złotowłosa Amelia, z lokami, zawsze dumna, NIGDY uśmiechnięta. Marysia próbuje się dowiedzieć czego Amelia NIE CHCE jej powiedzieć czytając mimikę (clues, secrets)
    * ExZ+4
    * V: Co może Amelię ZRANIĆ: Ernest nie ma pojęcia jakim ziółkiem jest Amelia.
    * (+3Or za szantaż): XXzX: Amelia robi coś ważnego dla rodu. Marysia jest "lol rekin". Amelia ma delikatną pozycję, więc lepiej niech Marysia nie robi głupot.
    * (+2V na zachętę): VVX: Amelia była osobą, która przekonała Ernesta, by przybył tu, na prowincję. Kocha go i próbuje chronić. PLUS: zadbała personalnie by Marysia odpowiadała za bezpieczeństwo Ernesta.
* 3 - Pustak przybył do Marysi ubrany w elegancki strój, ma kwiaty i kolory i wszystko. Marysia: wtf chce mi powiedzieć?
    * Tr+3
    * XV: trochę źle zinterpretowała. Rozumie sentyment ale nie detale. Ale to jest coś bez znaczenia, JEDEN znudzony Sowiński w historii zrobił taki kod.
* 4 - Marysia (strategicznie) i Karol (piórem) próbują opracować takie artykuły, które sprawią, że Rekiny szybciej uznają Ernesta za godnego współpracy. "Jeden z nas".
    * ExZ+3+3Or (Or: reprezentują "Marysia x Eternia")
    * XVzXOr: powszechna wiedza że tu jest Eternianin też wśród wrogów Eterni, atak w Marysię łatwy (w Eternianina). ALE: Rekiny przyjmą Eternianina szybko BO MARYSIA TEGO CHCE I ZA TYM STOI. Dla niej.
* 5 - Tukan próbuje złożyć taki program, by terminusi zyskali i by Arkadii się podobało
    * TrZ+3
    * VVV: Tukan ma bonus, szacunek i zadowolenie Pustogoru. Arkadia się przyda i będzie zneutralizowana na 30 dni. Plus wyjdzie zadowolona, zdrowa, z podniesioną opinią w Pustogorze.

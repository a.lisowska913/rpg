---
layout: cybermagic-konspekt
title: "Pierwsza BIA mag"
threads: legenda-arianny
gm: kić
players: żółw, fox, kapsel
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [210714 - Baza Zona Tres](210714-baza-zona-tres)

### Chronologiczna

* [210714 - Baza Zona Tres](210714-baza-zona-tres)

### Plan sesji
#### Co się wydarzyło

?

#### Sukces graczy

* ?

### Sesja właściwa
#### Scena Zero - impl

.

#### Scena Właściwa - impl

Wylądowaliśmy z powrotem w bazie. Martyn sprawdza lokalizację po mapach gwiezdnych - Persefona stwierdza, że nie ma pojęcia gdzie się znajduje. Martyn dopytał Bię - czy ta wie gdzie się znajduje. Mniej więcej. Ale to nie ma sensu z perspektywy przestrzeni. Więc musieli wpaść w slipstream. Martyn adresuje wspólną historię z Klaudią - czy teraz coś pamięta? Martyn ma hipotezę, że ma dostęp do kontaktu z Klaudią i ma rację - odpowiedziała mu Klaudia.

Arianna poprosiła Bię o udostępnienie Martynowi szpitala - musi znaleźć Klaudię. Bia dała zgodę. Martyn idzie grzecznie i nie rozrabia po drodze. Po drodze jeszcze informuje Bię, że jeśli są tu i będą dłużej, to czas życia bazy jest ograniczony. Ale mamy Eustachego itp. Może możemy pomóc Klaudii.

Martyn nie do końca rozumie jak działają określone rzeczy. Ramona, lekarz-to-be (wsparcie Martyna z Noctis). Razem identyfikują większość obiektów. Są tam również łóżka, na jednym leży Klaudia. Jest tam sporo narzędzi jakich Martyn nie rozpoznaje; sprawdza z Zoną Tres czy wszystko to można wykorzystać.

Cel Martyna: czy Klaudii coś bezpośrednio zagraża, czy jest w Klaudii jakaś "kotwica".

ExZ+4:

* V: Klaudii nie grozi niebezpieczeństwo w tej chwili. Może sobie roślinkować. Jakby była długo to będzie problem. Nie godziny a tygodnie.
* XX: Głęboka analiza i próbkowanie Klaudii. Dłużej w szpitalu poleży o tydzień.
* Vz: Widząc jak Martynowi nie idzie Ramona prowadzi. Mają piekielnie zaawansowane systemy monitorowania i badań fal mózgowych - BIA eksperymentalna. Martyn by przysiągł że systemy mogą wykryć tkankę magiczną. Potwierdzenie - mogę rozdzielić Klaudię bez uszkodzenia Klaudii ale niekoniecznie bez uszkodzenia Bii.

Martyn informuje Ariannę o sytuacji - jest w stanie pomóc Klaudii, ale pracuje nad tym jeszcze. Nie chce niczyjej szkody.

Eustachy ma dostęp do sporej ilości elementów i komponentów. Więc próbuje na poziomie bazy ją maksymalnie ponaprawiać i pousprawniać. Żeby maksymalnie usprawnić jak ta baza działa - i ułatwić Ariannie dalsze negocjacje. Eustachy chce ogarnąć jak działał system zalewania hangarów, czy są rozszczelnione bo rozszczelnienie, w logach + BIA -> katastrofalna awaria, opuszczenie grodzi.

By odciążyć grodzie Eustachy chce użyć osłony by przepuszczały wodę w jedną stronę, po czym wzrost ciśnienia w hangarze by ta woda po prostu była wypychana --> ocean. I jak pozbędzie się wody, naprawa / śluza. Osłony Inferni rozwiążą problem nadmiernego ciśnienia i ludzie mogą normalnie pracować.

Infernia jako zasób. 

ExZ+3+1:

* V: Są dziury. Większe się zaspawa, mniejsze też nie stanowią większego problemu. 
* XX: Nie da się odratować całej sekcji. Nie mamy narzędzi.
* V: Udaje się osuszyć. Nie udaje się wielu rzeczy zrobić, ale widać czemu Bii zależało na tym sektorze. Zapasowe generatory.

Dodajemy magię. 

* X: Bia zauważa magię.

Arianna dostaje komunikat: "Natychmiast wycofaj swoich ludzi z zalanej strefy. Niebezpieczeństwo." Arianna na to: "Ale JAKIE niebezpieczeństwo?" Bia: "Obca energia. Nie było spięcia w systemie, wykryłabym je.". Eustachy NATYCHMIAST dostaje komunikat od Arianny "NIE CZARUJ!" Eustachy ma pomysł - przekierować energię. Pokazać Bii "patrz, nic się nie dzieje, nie tu".

Arianna przekonuje Bię - to jej czujniki są trochę nie tak. Jest to o tyle trudne, że Janus właśnie czaruje i próbuje przenieść sygnaturę energii magicznej _poza_ promień bazy. A jest o tyle łatwiejsze, że Infernia jest niedaleko więc czarowanie Janusa się może z nim zlać. A Arianna próbuje przekonać Bię, że to POLE INFERNI ZAKŁÓCA bo jest taumiczne.

ExZ+4:

* X: "Arianno. Wiem, że nie jesteście Alivią." - Bia - "Może jednak powiesz mi prawdę?"

"oh well..?" - Fox
"well yes but actually no" - Eustachy XD

.

* "ok, nie jestem Oliwią. Jestem jej krewną" - Arianna XD.
* "nie wiem skąd znasz nazwę Alivii i nie ma to znaczenia. Nie mam powodu Was krzywdzić a Wy też nie macie powodu krzywdzić mnie na stan mojej wiedzy" - BIA

.

* X: "Albo naprawicie ten system i gramy w otwarte karty albo papa." - Bia.
* Xm: Bia ma dowody - mamy MAGÓW. "Anomalnych agentów".

Wyskoczyły na Martyna działka Bii. Bia żąda, by Martyn powiedział jako lekarz jak to jest możliwe, że mają anomalnych członków załogi. Martyn zaczął tłumaczyć, że technologia poszła do przodu - i tak jak używane są anomalne narzędzia, tak mamy możliwości używania dziwnej taumicznej energii. Ta Bia pochodzi z Zony Tres - wie dokładnie o co chodzi z używaniem dziwnych energii jako narzędzia. Więc - Martyn przekonuje Bię, że to jest magitech a nie niekontrolowana anomalia.

* V: Martyn wyjaśnia Bii, że ma "biowszczepy" - narzędzia pozwalające na to, by kontrolować energię magiczną. Osoby używające takich wszczepów nazywamy "magami". De facto, wyjaśnił Bii o magii.
* V: Martyn przekonał Bię, że bez magii nie możemy jej pomóc. I pokazał, że to działa - na nieszczęsnej, nieprzytomnej Klaudii (podleczył ją).

WRACAMY DO KONFLIKTU EUSTACHEGO:

* Vm: Generatory się odbudowują same pod kontrolą Eustachego. I zaczynają działać. A soczewka sprawia, że Bia jest przekonana, że w sumie to JEST narzędzie i nawet działa.

Arianna przekonuje Bię, żeby ta dała Martynowi dostęp do SIEBIE. By Martyn mógł wydzielić Klaudię. Jako dowód - nagrania jak gadamy z Klaudią, monitoring, logi itp.

ExZ+2: 

* X: Bia nie zgadza się na bycie oddaną w ręce Astorii. Wszelkie próby przejęcia nad nią kontroli -> samozniszczenie bazy.
* Vz: Czyny Eustachego udowodniły oddanie Inferni wobec naprawy bazy i Bii.
* V: Arianna udowodniła, że faktycznie Infernia może tajemnicza ale chce dobrze. Bia nie ma więcej uwag.
* X: Arianna odblokowała czujniki Inferni. Nie będziemy przeszkadzać nikomu przekazać wiadomości do Noctis.
* V: Arianna pokazała Klaudii i Bii wszystko. By zwiększyć nostalgię.

Eustachy tuninguje Bię. Łączy Infernię z Bią. Persefona ma odciążyć Bię by w innych tematach. I Persefona perfekcyjnie zna Klaudii wavepattern. Martyn udrażnia komunikację pomiędzy Klaudią i Bią. Martyn używa sympatii mimo wszystko - by Klaudia trafiła tam gdzie powinna. Martyn używa środków stymulujących by wzmocnić zarówno Klaudię jak i Bię.

ExMZ+3+2+2O

* V: Perfekcyjna współpraca magów. Wydzielenie Klaudii.
* X: Klaudia w szpitalu. Długo.
* O: Martyn używa magii krwi. Klaudia ma Skażenie Esuriit. Czyli na razie Martyn musi ją trzymać w stazie.
* O: Anomalna Bia używając komórek Klaudii. Musimy odbudować Bię komórkami Klaudii.
* V: Bia jest stabilna i sprawna.

Restart bazy. Na hipernecie jest nowy byt. BIA. Solitaria d'Zona Tres.

Martyn robi standardowe testy. Między innymi na akumulację i dyspersję energii magicznej. Okazuje się, że Bia po integracji z Klaudią stała się magiem...

Martyn maksymalnie delikatnie wyjaśnił Bii że by ją ratować musiał użyć tkanki Klaudii i przeszczepił się też symbiont. Wyjaśnił Bii jak działa magia, jak to funkcjonuje, podstawowe umiejętności. Przekierował na Janusa jako eksperta od Eteru i Ariannę jako arcymaga (eksperta od manipulacji energii magicznych).

Ta brama była brama absolutnie eksperymentalną, po Pęknięciu Rzeczywistości. Opracowali sporo teorii, Janus jest zafascynowany jak blisko byli prawdy. Jesteśmy w stanie wziąć ich odpowiednik mapy eterycznej ale samej mapy nie mamy. Można spróbować wejść w bramę i iść po omacku - to jest do zrobienia. Albo, alternatywnie, można użyć Nocnej Krypty i lecieć w cieniu Krypty...

A nawet mamy źródło cierpienia - samotny Lewiatan...

## Streszczenie

Nie wiadomo gdzie jest rogue planet. Nie ma sensownej opcji powrotu. Infernia zaskarbiła sobie współpracę z BIA, ale podczas pracy BIA wykryła magię. Martyn przekonał BIA, że tkanka magiczna to "wszczepy" - BIA pozwoliła na używanie magii i faktycznie, to pomogło w naprawianiu bazy. Martyn i Eustachy rozdzielili Klaudię i BIA, ale Klaudia jest w stazie a BIA uzyskała moce magiczne. Jak wrócić? Ano, jedyną opcją jest w sumie w cieniu Nocnej Krypty...

## Progresja

* BIA Solitaria d'Zona Tres: Uzyskała umiejętności magiczne po integracji z Klaudią. Innymi słowy, Martyn zrobił z Bii maga.
* Klaudia Stryk: Skażona przez Esuriit przez rozdzielenie po sympatii z Bią; miesiąc regeneracji.

### Frakcji

* .

## Zasługi

* Arianna Verlen: kłamanie BIA odnośnie tego że są Alivią Nocturną przestało działać, więc musiała użyć prawdy. Przekonała BIA, że Martyn może uratować ją i Klaudię.
* Eustachy Korkoran: naprawia Zonę Tres - wpierw używając narzędzi a potem wspomagając się magią. Potem pomaga Martynowi rozdzielić Klaudię i BIA.
* Martyn Hiwasser: wyjaśnił BIA Solitaria, że tkanka magiczna to "wszczep" i magia to magitech. Wraz z Eustachym rozdzielił Klaudię i BIA. Nie radzi sobie z archaiczną medyczną technologią noktiańską.
* BIA Solitaria d'Zona Tres: nieufna, skontaktowała się za plecami Arianny z noktianami i poznała prawdę o Inferni. Jednak pozwoliła Martynowi i Eustachemu się naprawić i wydzielić z siebie Klaudię.
* Janus Krzak: jedyny astoriański naukowiec ever jaki tłumaczył Bii zasady eteru i jak należy używać magii. Srs.
* Romana Arnatin: noktiański lekarz na Inferni, wsparcie Martyna. Wraz z Solitarią pomogła Martynowi używać noktiańskiej technologii w obawie, że ten skrzywdzi Klaudię.
* Klaudia Stryk: przedmiot, nie podmiot; rozdzielona z Bii Solitarii d'Zona Tres przez Eustachego i Martyna. W fatalnym stanie, w stazie, ale żywa.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Zagubieni w Kosmosie
            1. Crepuscula
                1. Pasmo Zmroku
                    1. Baza Noktiańska Zona Tres

## Czas

* Opóźnienie: 1
* Dni: 2

## Konflikty

* 1 - Cel Martyna: czy Klaudii coś bezpośrednio zagraża, czy jest w Klaudii jakaś "kotwica".
    * ExZ+4
    * VXXVz: Ciężkie uszkodzenie Klaudii (poleży w szpitalu przez tydzień), ale nie grozi jej niebezpieczeństwo. Może rozdzielić - nie wiadomo co z BIA.
* 2 - Eustachy próbuje naprawić zalany fragment bazy z generatorami. Infernia jako zasób.
    * ExZ+3+1
    * VXXV: Dziury, do naprawienia, ale nie da się całej sekcji uratować. Wymaga magii - ale jak się uda, epicko
    * (po dodaniu magii) : X: BIA zauważa magię
    * (po użyciu zaawansowanej magii i odratowaniu) : Vm: generatory epicko działają. Bia przekonana, że magia może pomóc
* 3 - Arianna przekonuje Bię - to jej czujniki są trochę nie tak. A Arianna próbuje przekonać Bię, że to POLE INFERNI ZAKŁÓCA bo jest taumiczne.
    * ExZ+4
    * XXXm: Bia ma dowody, konfrontuje z Arianną... a Arianna próbuje ją okłamać i przegrywa. Bia jej nie ufa, idzie do Martyna.
* 4 - Martyn przekonuje Bię, że magowie po prostu mają wszczepy i to technologia++. Nie niekontrolowana anomalia a narzędzia pozwalające na kontrolowanie energii
    * ExZ+2
    * VV: Martyn przekonał Bię i przekonał, by autoryzowała używanie magii na pokładzie
* 5 - Arianna przekonuje Bię, żeby ta dała Martynowi dostęp do SIEBIE. By Martyn mógł wydzielić Klaudię. Jako dowód - nagrania jak gadamy z Klaudią, monitoring, logi itp.
    * ExZ+2
    * XVzVXV: Bia zażądała by być poza Astorią i by Arianna nie przeszkadzała w sygnałach do Noctis. Arianna udowodniła że można ufać. I pokazując Klaudii/Bii przeszłe czyny i Infernię wywołała nostalgię (bonus)
* 6 - Eustachy tuninguje Bię. Łączy Infernię z Bią. Martyn udrażnia komunikację pomiędzy Klaudią i Bią. Innymi słowy, Eustachy dba o pacjenta a Martyn zapewnia prawidłowe rozdzielenie.
    * ExMZ+3+2+2O
    * VXOOV: Perfekcyjne rozdzielenie, Skażenie Esuriit Klaudię, anomalizacja Bii komórkami Klaudii (by przeżyła) i naprawienie Bii.

## Inne

.

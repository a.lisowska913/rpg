---
layout: cybermagic-konspekt
title: "Infernia i Martyn Hiwasser"
threads: legenda-arianny
gm: żółw
players: kić, fox
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [220712 - Pasożytnicze osy w Nativis](220712-pasozytnicze-osy-w-nativis)
* [221102 - Astralna Flara i porwanie na Karsztarinie](221102-astralna-flara-i-porwanie-na-karsztarinie)

### Chronologiczna

* [200106 - Infernia i Martyn Hiwasser](200106-infernia-martyn-hiwasser)

## Uczucie sesji

* "Dobra, wyciągamy go XD."
* "Co on tu odmiauczał? XD"

## Punkt zerowy

.

### Postacie

.

## Misja właściwa

### Scena zero

.

### Sesja właściwa

Zespół przyleciał Infernią do Kontrolera Pierwszego najszybciej jak się dało. Członek załogi i wybitny bioinżynier, Martyn Hiwasser, nie żyje. Zdaniem Arianny, Martyn nie jest osobą która "umiera". Martyn jest osobą, która ucieka z kolejnego łóżka. No i ciało Martyna znajduje się Gdzieś Pośród Innych Ciał zanim zostanie przetworzone. Plan Arianny jest prosty. Dostać się do odpowiedniego sektora Kontrolera Pierwszego, znaleźć ciało Martyna, wydobyć je na Infernię i przebadać - zdobyć wszystkie informacje.

Klaudia wierzy, że... Arianna ma tendencje do hiperoptymizmu. Tak więc skupiła się na dokumentach w Kontrolerze Pierwszym. Musi zobaczyć wszystko co jest - znaleźć czy to jest możliwa śmierć, czy to ma sens... Klaudia siadła do hackowania biurokracji.

Rekordy pobieżne pokazują, że przyczyną śmierci było przedawkowanie. Jest to możliwe; Martyn nie stronił od używek. Ale był cholernym biochemikiem. Wiedział co brać, co może i ile. Gdy Klaudia zaczęła grzebać głębiej, napotkała na opór dokumentów. Fakt, że Martyn podaje różne imiona w różnych kontekstach nie pomaga. Ale Klaudia zna sporą ich część. Klaudia więc zagrzebała się w dokumenty i hipernet i zaczęła szukać...

* Martyn zajmował się tym co zwykle. Kobiety, bioformacje, imprezy. Skuteczny i przystojny.
* Martyn pomagał niejakiej Wioli przygotowywać się do gier wojennych w virt. Nie znał się na tym zupełnie, ale Wiola chwaliła sobie pomoc.
* Z jakiegoś powodu fakt, że Martyn pomagał Wioli wzbudził ogromne zainteresowanie wśród osób toczących walki w virt.
* Dokumenty śmierci Martyna nie są... w pełni wiarygodne. Nie było też szczególnego śledztwa. To wygląda, jakby Martyn miał na pewien czas zniknąć.
* Po analizie dokumentów, Klaudia jest przekonana, że Martynowi absolutnie nic nie jest. Jest gdzieś trzymany wbrew swojej woli - ale nie powinien być teraz martwy.
* Obecność aktu zgonu świadczy o tym, że Martyn raczej nie wróci na Kontroler Pierwszy.

Klaudia poprosiła Ariannę o zrobienie szumu i dywersji. Pani kapitan jest w tym ŚWIETNA. A nuż się też czegoś dowie ;-). A sama próbuje namierzyć jakiekolwiek ślady Martyna. Wiola jest jedną z odratowanych osób; nie ma szczególnych umiejętności wartościowych jeszcze dla Orbitera, więc nie jest szczególnie ważna w hierarchii. Jest w koszarach; uczy się przydatnych umiejętności (np. jak przetrwać).

Kamil poszedł porozmawiać z Wiolą. Więcej, przyprowadził ją na Infernię. Kamil przepytał Wiolę z sympatią o Martyna i co się tu dzieje; Wiola specjalnie nie protestowała, chciała pomóc:

* Martyn ją badał; był jedną z osób, która znalazła w niej potencjał - nadaje się do korpusu wirtualnego. Ona nie miała pojęcia.
* Martyn użył jakichś środków, by wzmocnić jej potencjał. Faktycznie, interfejsowanie się z virtem stało się dla niej dużo, dużo prostsze.
* Zbliżające się gry wojenne są Bardzo Ważne z perspektywy niejednego komodora Orbitera. Tam jest sporo zakładów - a ona była typowana na ostatnie miejsce.
* Wszystko, co Martyn jej dał zostało skonfiskowane. To jest: "wzięte do zbadania żeby znaleźć winnego samobójstwa".
* Martwi się o Martyna. Ona nie jest może magiem, ale wie o magii. Wątpi, że zginął, ale powiedziała "przy tych grach zdarzały się rzeczy nietypowe".

Klaudia wydedukowała, że informacja będzie znajdowała się w Wioli. Tak więc, rozpoczęła badanie Wioli. Wiola z lekkim oporem ale poddała się badaniom z ręki Klaudii, wspieranej przez TAI Persefonę. Wie DOKŁADNIE czego szukać i pomaga Persefonie znaleźć odpowiednie struktury.

Klaudia dotarła do informacji: faktycznie, zobaczyła Martyna. Martyn zostawił wiadomość:

* Wiola jest niezła w gry wojenne. 
* Ale jakiś komodor postawił przeciwko Wioli. Że ona będzie na ostatnim miejscu.
* Ten komodor postawił gdy był pijany. Bo nie podobało mu się, że Martyn jej pomaga. Bo nie lubi Martyna. Ale teraz - it's personal.
* komodor nazywa się Dominik Ogryz. Jest magiem.

Klaudia wie już chyba wszystko. Sama Wiola ciężko przeszła badanie; jest wyczerpana, lekko chora i słaba. TAI Persefona jest kiepskim badaczem a Klaudia jakkolwiek umie ją pokierować, nie zrobi tego za Persefonę.

Arianna powróciła. 

* Udało jej się całkowicie odwrócić uwagę od działań Zespołu. Wiola, Klaudia, Kamil - nie byli monitorowani. Jednak Arianna umie być drama queen jak chce.
* Dodatkowo, Arianna uzyskała dostęp do detektorów na Kontrolerze Pierwszym - by jej udowodnić, że Martyn nie żyje. Wprowadziła tam próbkę Martyna i dostała odpowiedź "nie ma".
* Arianna zauważyła, ale nie powiedziała tego głośno, że próbka Martyna nie zarejestrowała odpowiednio mocno "zwłok Martyna". Woli, by wszyscy myśleli, że nie wie.
* Proste rozwiązanie - Martyn jest poza Kontrolerem Pierwszym.

Arianna utraciła trochę reputacji, ale udało jej się potwierdzić jeszcze jedną rzecz - Martyn JEST na pokładzie Kontrolera. A dokładniej: na jakimś statku zadokowanym na tym Tytanie. Inaczej nie dostałaby echa sygnatury. Znowu, udawała, że nic nie widzi - ale wie na pewno, że Martyn gdzieś tu jest. Skierowała Klaudię do nowego zadania - zlokalizować na którym statku najpewniej znajduje się Martyn.

Problem polega na tym, że to zadanie nie jest możliwe. Ogryz ma więcej niż jeden statek. Nie da się wykryć obecności Martyna nie będąc na statku. Nie da się więc łatwo zawęzić statków. Więc trzeba zadziałać w inny sposób.

Arianna się zirytowała. Poszła szukać, kto jest przeciwnikiem Ogryza. Znalazła komodora Szczepana Olgroda i zaczęła nad nim pracować. Charyzmatyczna bohaterka, acz nieco spleśniała, dała radę:

* Olgrod pomoże odbić i odzyskać Martyna Hiwassera. Ogólnie, Olgrod pomoże w czymkolwiek co walnie w Ogryza.
* Niestety, katastrofalnie to uderzy w Infernię. Arianna będzie postrzegana jako osoba, która miesza się w wew. sprawy Orbitera i Kontroler Pierwszy.
* Dodatkowo Arianna wisi Olgrodowi coś, co on musi zbadać i zrobić. Arianna oczywiście się zgodziła.

Klaudia siadła do biurokracji. Szuka informacji na temat statku. Który to może być? Wysłała agentów ze swojej załogi, wysłała ludzi od Olgroda. Szuka po dokumentach.

* Kilku agentów Inferni zostało ciężko pobitych przez nieznanych sprawców. Ogólnie, ucierpieli.
* Sama Klaudia - wydało się, że szpieguje (na rozkaz Arianny).
* Ale, zlokalizowała w miarę pewnie gdzie znajduje się Martyn. To "ASD Corona". Fregata militarna pod kontrolą Ogryza.

Arianna zdecydowała się pójść formalnie. Mają TAKIE dowody, mają wsparcie Olgroda, mają wieści o tym, że sfałszowano dokumenty na Orbiterze i porwano członka załogi Inferni (nawet, jeśli nim nie jest). Do tego Klaudia walnęła pełne możliwe wsparcie biurokratyczno-zasobowe.

* SUKCES. Żandarmeria wojskowa weszła na pokład "Corony" i uwolnili Martyna.
* Martyn dołączył do załogi Inferni. Acz, niemałym dla samej Inferni kosztem...

**Sprawdzenie Torów** ():

* .

**Epilog**:

* .

## Streszczenie

Arianna Verlen uratowała Martyna Hiwassera przed nędznym losem, ratując go od problemów z komodorem Ogryzem na Kontrolerze Pierwszym. Niestety, podpadła dużej ilości oficerów Orbitera. Ale za to przywróciła Martyna do załogi Inferni.

## Progresja

* .

### Frakcji

* .

## Zasługi

* Klaudia Stryk: naukowiec Orbitera pod dowództwem Arianny Verlen; tym razem przerzucała ogromne ilości dokumentów, papierów i biurokracji by tylko znaleźć Hiwassera.
* Arianna Verlen: kapitan Inferni. Kiedyś bohaterka, teraz skompromitowana. Uratowała Hiwassera zgrabnie nawigując polityką Orbitera i ufając swojej kompetentnej załodze.
* Kamil Lyraczek: ludzki kaznodzieja Arianny, nadal z nią. Dobry do przesłuchiwania i ludzkich aspektów załogi - gadał, przekonywał i dopytywał by pomóc znaleźć Wiolettę a potem Martyna.
* Martyn Hiwasser: doskonałej klasy biomanipulator i potężny mag. Elegancki kobieciarz. Wdał się w małe rozgrywki polityczne w Orbiterze, pomógł Wioli i został pojmany przez Ogryza - i uratowany przez zespół Arianny.
* Wioletta Keiril: uratowana przez Orbitera jako nie-mag cywil, dzięki Martynowi weszła w Gry Wojenne. Ogólnie, słaby agent; Martyn zostawił w niej informację do Zespołu kto go porwał.
* Dominik Ogryz: komodor Orbitera na Kontrolerze Pierwszym. Porwał Martyna Hiwassera, bo się pokłócili o kobietę. A że przy okazji założył się, że Wioletta przegra a Martyn ją wzmacniał...
* Szczepan Olgrod: komodor Orbitera na Kontrolerze Pierwszym. Wsparł Ariannę Verlen w ataku na komodora Ogryza; ale za to sporo zyskał koncesji z jej strony.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria, Orbita
                1. Kontroler Pierwszy: titan-class Orbiter HQ. Tam odbyła się cała akcja z Martynem Hiwasserem który podpadł komodorowi i ratowała go Arianna z ekipą.

## Czas

* Chronologia: Aktualna chronologia
* Opóźnienie: 339
* Dni: 5

## Inne

### Co warto zapamiętać

nic szczególnego ;-)
---
layout: cybermagic-konspekt
title: "Elwira, koszmar Nox Ignis"
threads: historia-talii, koszmar-nox-ignis, wojna-deorianska
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [221230 - Dowody na istnienie Nox Ignis](221230-dowody-na-istnienie-nox-ignis)

### Chronologiczna

* [221230 - Dowody na istnienie Nox Ignis](221230-dowody-na-istnienie-nox-ignis)

## Plan sesji

### Fiszki
#### 1. OO Loricatus, ciężka fregata

* Talia Derwisz: agentka insercji
* Franz Szczypiornik: kapitan Loricatus, weteran Orbitera (5x) szukający synekur (atarien)
    * ENCAO:  -0+-- |Nie ma głowy w chmurach, trzeba ciężko pracować;;Nie ufa nikomu;;Niezwykle surowy| VALS: Hedonism, Security >> Conformity| DRIVE: Pokój przez tyranię
    * "Pax Orbiter jest najważniejszy", "Jeśli wiedzą co jest dla nich dobre, będą współpracować" | "nie rozumiesz sytuacji, noktianinie" (nie używa imion tylko tytułów)
* Medea Sowińska: oficer łącznościowy Loricatus, psychotroniczka i młodziutka czarodziejka (18?)
    * ENCAO:  0-+-0 |Nie przejmuje się niczym; Formalna i zimna| VALS: Face, Hedonism >> Benevolence | DRIVE: corrupted hunger, slaves
    * "Jak sobie kapitan życzy", "Z przyjemnością przesłucham jeńców"
* Dominik Łarnisz: cyberwspomagany marine który kocha rysować (atarien)
    * ENCAO:  0--0+ |Surowy, wymagający;;Spontaniczny| VALS: Self-direction, Family >> Hedonism| DRIVE: Zapobiec cierpieniu (rozerwana rodzina)
* Wawrzyn Rewemis: medical
* Mateusz Kaftan: główny inżynier, weteran Orbitera (54); stracił rodzinę (atarieni) i przyjaciół (noktianie) podczas wojny z Saitaerem. Zdecentrowany. (atarien)
    * ENCAO:  0--0+ |Zapominalski;;Wścibski| VALS: Security, Conformity | DRIVE: Broken soul / Save all souls
    * "What is the purpose of it all", "Save all souls"

#### 2. CON Ratio Spei 
##### 2.0. CHARAKTERYSTYKA LOKACJI

* .Aspekty trwałe: 
    * labirynt (powstawała stopniowo i dobudowywano rzeczy do cylindrów), 
    * klaustrofobiczne przestrzenie, tysiące zakamarków
    * dość ciemno, gorąco
    * nie ma sensownych połączeń
* .Aspekty tymczasowe: 
    * brudna, zatłoczona, głośna, nadmiar wszystkiego 
    * tymczasowe obozowiska, dziesiątki faceless people
    * tysiące noktiańskich dron
    * hopeless, sadness, battles

##### 2.1. Grupa Ozariat

* Tristan Ozariat: (dekadianin) kiedyś oficer; potem użył Nox Ignis i "próbuje odbudować Elwirę" (jego córka to była Tatiana - ciemne włosy, blada cera)
    * ENCAO: MANIC!!! | Zimna taktyka;; Głodna dusza i nie kontaktuje| Power, Benevolence > Humility | DRIVE: odbudować / znaleźć Tatianę
    * "to jedna z nich! To moja ukochana Elwira."
* Sankor Drawas: (dekadianin) zimny enforcer Tristana, cyberwspomagany i lojalny, zna go "od zawsze"
    * ENCAO:  -0+-- |Płytki;;Skromny i unikający pokus ciała;;Nie dba o to co inni czują| VALS: Power, Self-direction >> Benevolence| DRIVE: Corrupted contagion
    * "To mój kapitan. Uratował mi życie."
* Dolor Formido: (??); noktiański mag koszmarów i wypaczania umysłów, evil incarnate, he HURTS people. BOMBSHELL.

##### 2.2. Grupa Wąż Pożera Światło

* Lithian Amarantis: (drakolita) władca kultu i narkotyków, Grupa Wąż Pożera Światło
    * ENCAO:  --00+ |Szczery; uczciwy;;Kapryśny i marzycielski| VALS: Power, Stimulation >> Tradition| DRIVE: Pokój
    * "Tego jeszcze nie próbowaliśmy, prawda? By zaćpać populację i zagłuszyć ich ból?"

##### 2.3. NOCTIS oddział Venandi Dolor

* Apalia Certamen: (dekadianka), oddział Venandi Dolor
    * "There is war. Just war. War shall forge the strongest. The truest."
    * Wojna Deoriańska: pierwsza stanęła za prawdą. Zdradzona, przegrała. Nie poddała się nigdy - przeszła przez Bramę resztką swoich sił.

##### 2.4. NOCTIS oddział Alarius

* Sebastian Alarius: (klarkartianin), oddział Alarius
    * "Noctis might have forsaken us, but we still can do so much more!", "Those planetoids will be useful for us all."
    * Wojna Deoriańska: odrzucił Noctis, które odrzuciło prawdę. Szuka nowego życia.
* Nikola Anatrian: komandoska noktiańska, oddział Alarius
    * ENCAO:  0---+ |Stabilna, opoka;;Łatwo się rozprasza;;Ma bardzo bujną wyobraźnię| VALS: Face, Conformity >> Tradition| DRIVE: Wiara w przełożonych
    * "Nie ma niczego co możesz mi dać ani zabrać", "Ja nie mam znaczenia, zginęłam na Noctis"

##### 2.5. NOCTIS oddział Sessair

* Tylian Sair: (klarkartianin), oddział Sessair
    * "We can make a last stand. Noctis, together! Remember what you are!"
    * Wojna Deoriańska: sojusznik Apalii, poddał walkę - jaki sens niszczyć Noctis?
* Brunon Szwagacz: (klarkartianin), oddział Sessair
    * ENCAO:  +--00 |Działa zgodnie z błędnym modelem;;Drapieżny| VALS: Achievement, Family| DRIVE: Noctis together, others go away
    * "We shall make it work. Noctis forever."


##### 2.6. NOCTIS Domina Lucis

* Sarian Xadaar, pierwszy oficer i dowódca Dominy Lucis (dekadianin) 
    * ENCAO:  0-+00 |Niemożliwy do zatrzymania;;Lojalny i oddany grupie| VALS: Benevolence, Tradition >> Face| DRIVE: Niezłomna Forteca
    * "Victorious, to the end!!!", "This... this is no life", "Te planetoidy to idealny dom Noctis."
    * Wojna Deoriańska: oddał się pod dowództwo i wykonał zadanie. Misja BĘDZIE wykonana nieważne czy jemu się to podoba.
* Raoul Lavanis: (dekadianin); noktiański snajper, advancer, detektyw.
    * ENCAO: -++0+ | Cichy i pomocny;; niewidoczny| Conformity Humility Benevolence > Pwr Face Achi | DRIVE: zrozumieć
* Katrina Komczirp: (atarienka); agentka Orbitera, PRZECHWYCONA; powiązana z Orbiterem

##### 2.7. Orbiter

* Patrycjusz Gurczam: commander; wspiera Aureliona
    * ENCAO:  0+--0 |Zdradliwy (synekura);;Sceptyczny| VALS: Hedonism, Power >> Face| DRIVE: Szok i oburzenie
* Szymon Gurczam: corrupt cop; korzysta z Ozariat i sobie "wyrywa" noktianki by je "chronić".
    * ENCAO:  +0-00 |Niszczycielski;;Dowcipny, Błyskotliwy| VALS: Self-direction, Hedonism >> Benevolence, Humility| DRIVE: Rana ego, obsesyjna
* Grzegorz Marzut: decent cop, bije się z noktianami
    * ENCAO:  -00-+ |Krewki i drażliwy;;Sentymentalny| VALS: Conformity, Power >> Stimulation| DRIVE: Pokój przez tyranię
* Aletia Nix: noktianka, uciekła pod opiekę Szymona przed siłami Tristana
    * ENCAO:  0+--- |Wszystko robi na ostatnią chwilę;;Zrezygnowana, wszędzie widzi ciemność| VALS: Self-direction, Security >> Benevolence, Achievement| DRIVE: Ukryć sekret (kody serpentisa)

##### 2.8. Aurelion

* Stanisław Sardanik: oficer Aureliona, dobrej klasy agent
    * ENCAO:  --0+- |Szuka odpowiedzi w teorii;;Dogmatyczny i potępia| VALS: Conformity, Face >> Humility| DRIVE: Ukojenie cierpienia Bashira

### Theme & Vision

* Kampania
    * Nightmare of Nox Ignis - is it real? What is it?
    * Echa wojny deoriańskiej
        * Rozkradamy noktian, stację, wszystko
        * Noktianie są w wojnie domowej
    * Konstytuowanie Anomalii Kolapsu - Orbiter, Noctis, starcia, GANGSTERSTWO
* Ta sesja
    * Nox Ignis niszczy użytkownika; Tristan has a corrupted soul

### Co się wydarzyło KIEDYŚ

* Jesteśmy dość świeżo po wojnie noktiańskiej.
* Talia jest wysłana, by łapać niewłaściwych Orbiterowców, noktian, przestępców.
    * Talia udaje bounty hunterkę.
* Trzeba się dowiedzieć czy Nox Ignis to prawda czy legenda.

### Co się wydarzyło TERAZ (what happened LATELY / NOW)

* Orbiter zniszczył TAI i wszelkie funkcje defensywne Ratio Spei.
    * sytuację przejęły mniejsze lub większe gangi i grupy
    * raczej nikt nie robi krzywdy głównym agentom sterującym Ratio Spei
* Ratio Spei ma ogromną ilość osób przybywających i odchodzących
* Stacja Con Ratio Spei jest rozdzierana przez główne ideologie
    * "przeczekać i zmiażdżyć Orbiter", "Pax Orbiter", "żyć w spokoju", "kralotyczny kult", "każdy przetrwa na własną rękę" i wiele innych
    * wszystko jest na skraju walki ze wszystkim, dodajmy niewielką ilość surowców
* LOKALNIE wobec tej sesji: dwie główne siły "Zdradzieckie noktianki" oraz "Jad zwycięstwa"

### Co się stanie (what will happen)

* CEL 1: dowiedzieć się o Nox Ignis. Dojść do Tristana.
* CEL 2: pozyskać Aletię
* CEL RAOULA: pozyskać Aletię

.

* S1: Spotkanie na bazarze; dwie grupy noktian się zwalczających.
* S2: Info o Aletii - dziewczyna ma unikalną wiedzę noktiańską. 
    * Orbiter udaje, że nie ma pojęcia o Aletii. 
* S3: Lokalny Orbiter szuka ładnych noktianek.
    * Walka noktian i Orbitera
* S4: Starcie z "Nieskończonym Honorem"


* S1: Tristan ma obsesję na punkcie swojej córki. WIE coś o Nox Ignis. Franz - "zinfiltruj, znajdź; ja zdobędę wsparcie Orbitera i się doń dostaniemy."
* SN: Ludzie ustawiają się w kolejce po narkotyki, by zapomnieć. There is no hope.
* SN: Ludzie wiedzą, że Tristan porywa dziewczyny. Ale one chcą z nim zostać; może je chce chronić? Ogólnie Tristan nie jest bardzo groźny.
* SN: Aletia była na celowniku Tristana; na pewno ją znajdzie. To tylko pytanie kiedy Tristan zaoferuje Gurczamowi dość by on Aletię oddał.

### Sukces graczy (when you win)

* odzyskać Katrinę
* uratować Aletię od Syndykatu
* poznać Nox Ignis od Tristana

## Sesja właściwa
### Wydarzenia przedsesjowe

.

### Scena Zero - impl

.

### Sesja Właściwa - impl

Talia i Dominik siedzą w pokoju w barze "Proch Strzelniczy". Bar jest zdecydowanie przepełniony, ma niewiele energii ale jest tani. Plus, jest GŁOŚNY. To jest zaleta - trudni do podsłuchania. W barze znajduje się wiele ludzi którzy wyraźnie nie mają gdzie się podziać. Cała ta stacja jest echem powojennym. Talia przez Medeę skontaktowała się z Franzem.

* F: Sytuacja na tej stacji jest dużo gorsza niż się wydawało.
* T: Mówiąc delikatnie. Braki zasobowe, braki w zasadzie we wszystkim.
* F: W aktualnej sytuacji Nox Ignis jest mniejszym problemem.
* T: Zgadzam się. Mamy mały kryzys humanitarny.
* F: Lokalne siły Orbitera nie spełniają swojej funkcji. 500 osób. Nie mogą być wszędzie, ale nawet nie próbują.
* T: No... nie są tak pozytywną zmianą na jaką możemy liczyć. Powinni być siłą do której się idzie po pomoc.
* F: Dowiedz się od lokalnych ludzi co wiedzą o Orbiterze i co uważają. Co się tu dzieje.
* T: Zrobione (raport)

SCENA WSTECZNA

Brzuchowisko jest częścią cylindra niekoniecznie dobrze nazwaną. Ale jest tak nazwana, bo jest wielopoziomowe. Takie slumsy. Wyraźnie widać, że nikt nie planował tego w fazie konstrukcji - samo wyszło. Takie... mrówkowate trochę. Ciasno, zakamarki, dużo ludzi, wszyscy patrzą za ładną dziewczyną. Na szczęście jest też Dominik. Ludzie patrzą na Talię, potem na Dominika i odwracają oczy. I mnóstwo małych dron noktiańskich. Trochę pet, trochę narzędzie, trochę oczy. Sporo ich jest, co daje wrażenie wiecznego bycia śledzonym nawet jeśli najpewniej żadna drona nie śledzi. Dodajmy niskie oświetlenie i wysoką temperaturę i nic dziwnego, że jest to dość irytujące miejsce. Ogólna atmosfera? Rezygnacja, brak nadziei, pasywność. Czekanie na swój los.

Z perspektywy WSZYSTKICH tu mogą żyć tylko szczury. Lub noktianie.

Talia udaje się do lokalnego bazarku. Tam są różne rzeczy - zwłaszcza coś, co noktianie kiedyś mieli. To co mieli, to co zabrali - to mają ze sobą. I tego się pozbywają. By przetrwać dłużej. Są tam też ogłoszenia: "załoga do grazera potrzebna" czy "ładna dziewczyna na noc, nie jestem tania, obiecuję dyskrecję". Wyraźnie oni jeszcze nie doszli do siebie po wojnie. Talia widzi shellshocked eyes. Jednocześnie Talia widzi "oficerów" próbujących coś z tym wszystkim robić. Jeden koleś każe sprzątać. Coś, by oni COŚ robili.

Tr Z (Talia kupuje i okazuje im że nie traktuje ich jak śmieci) +3:

* V: Noktianie podzielili się na subfrakcje. Już rozmawiając z nimi Talia widzi, że nie ma "Noctis" jako całości. To są grupy skupione dookoła kapitanów. Trzy główne nurty i sporo pomniejszych
    * Alarius: "znajdźmy nowe życie tutaj, pomóżmy, Noctis nas odrzuciło".              <-- współpraca z Orbiterem, pragmatycznie
    * Sessair: "Noctis, together, forever". "Trzymajmy się razem, Noctis vs all."       <-- spróbujmy np. współpracować, odepchnąć Orbiter, whatever
    * Venandi Dolor: "All shall forsake you, all shall betray you. Accumulate strength. We lost because we were weak."     <-- ruch zrodzony ze zdrady i desperacji
    * Drobne grupy grające "pod siebie"
* V: Talia doszła do tego, że KTOŚ z noktian porwał Katrinę. Jacyś noktianie ją mają. Albo _ci inni_. Ktoś poluje na dziewczyny noktiańskie.
    * Orbiter dobiera sobie panienki z Noctis co jest dowodem, że Alarius jest zbyt optymistyczny.
    * są ludzie, których koleżanki, córki itp. po prostu zniknęły. 
* X: Zainteresowanie Talią jednego z noktiańskich oficerów.
    * Brunon podszedł do Talii i się uśmiechnął zimno "z kim mam przyjemność?". Talia się przedstawiła. 
    * Brunon pyta, czemu Talia rozpytuje o te dziewczyny. Talia "mogą ich zniknięcia mogą być powiązane z ludźmi których szukam". 
    * Brunon "jesteś ładną dziewczyną. Nie obronimy Cię tu." Talia daje wrażenie pewnej ale kompetentnej. 
    * Brunon "z której jednostki jesteś?" Talia "źle mnie umiejscowiłeś, jestem łowcą nagród". Brunon "ci, których szukasz są w Orbiterze. Przyszli, zabrali parę dziewczyn, poszli. Nic nie możemy zrobić. Masz nazwisko swojej dziewczyny? Jest z Noctis?" Talia "nie jest z Noctis". Brunon "to Orbiter może jej nie mieć; nie mam nazwisk nie-noktianek.". Talia "wyglądasz na kogoś, kto potrafi coś tu osiągnąć. Pomożemy sobie?"
    * Brunon "jeśli odzyskam te biedne dziewczyny wejdę w sojusz z samym Saitaerem." I Talia widzi przekonanie, żar, oficer, który się nie poddał. Talia widzi, że on naprawdę się stara zrobić, by Noctis trzymało się kupy. Talia dała mu imię - Katrina Nirips. Dostała od Brunona listę 27 nazwisk. Wszystko dziewczyny w wieku 15-25. Furia w oczach noktianina jest widoczna.
    * Brunon "jeśli Twoja Katrina zniknęła i wie o tym jakiś noktianin, będę wiedział w ciągu 3 dni. Chyba, że jest w rękach zdrajców lub apokaliptów. Ale jak chodzi o te dziewczyny nawet Alarius mi pomoże.". "W czym wiesz - nie oczekuj cudów. Znajdziesz je u Orbitera. Co najmniej te 8 zostało zabrane przez Orbiter. Z imienia - Szymon Gurczam."
        * Wywiad Noctis wyraźnie dalej działa.
    * Talia: "czy te dziewczyny są z Twojej grupy?" Brunon "pod kątem urody; everyone is affected."

Talia zobaczyła, jak Brunon nagle odwrócił się w kierunku nadlatujących w jakieś miejsce dron. Brunon odwrócił głowę, przeprosił i pobiegł. Talia idzie wolniej za nim, chce zobaczyć o co chodzi w sytuacji.

* (+2Vg) X: Z cienia wychynął noktianin, większy, odepchnął lekko Talię. "Pan porucznik powiedział, że jest zajęty. Zostaw go". Wyraźnie widać, że to nie jest porządkowy tylko jakiś sfrustrowany noktianin.
* X: Dominik wyszedł zza Talii i spróbował zastraszyć noktianina. Koleś się rwie do bitki. Z dziewczyną nie, ale z Dominikiem? Ma GDZIEŚ że Dominik ma wszczepy. Noktianin ma wszystko gdzieś; Talia widzi, że noktianin ma w oczach sygnał, że wszystko stracił.

Talia kładzie rękę na ramieniu Dominika. "Zostaw, nie warto". Do noktianina "porucznik nas znajdzie jak będzie chciał." Talia opuszcza ten teren. Wie już wszystko co chciała. Noktianin śledzi ją wzrokiem i jak Talia się oddaliła, uderzył pięścią w ścianę z wyraźną frustracją.

SCENA AKTUALNA

Franz dostał raport: mamy co najmniej 3 istotne frakcje (Tristan jest planktonem), Orbiter jest bardzo mocno uważany za winnego zaginięcia dziewczyn w celach rekreacyjnych. I co więcej mamy potencjalne nazwisko - Brunon Szwagacz - kontakt odnośnie Kariny. A oficer Orbitera podejrzany (z nagraniami) to Szymon Gurczam. Franz "brat komendanta". Co również istotne, Szwagacz wszedłby w układ nawet z Saitaerem; jesteśmy w stanie zrobić tak by pomóc to przynajmniej część noktian uzna, że opinia o Orbiterze jest lepsza.

Franz "W takiej sytuacji dostaliśmy imbecyla jako oficera Orbitera na tej stacji. Zwycięska wojna nie oznacza traktowania noktian jako łupów wojennych."

Medea dostała smutne zadanie przeanalizować listę dziewczyn. Czy nie porwali "więcej niż jedną" by coś ukryć. Medea ma znaleźć czy jest tam ktoś ciekawy i nietypowy.

TrZ (ma czas i środki) +2:

* X: Medea musiała posiłkować się danymi z K1; sprawiło to, że _query_ jest potencjalnie odczytywalne przez jednostki infiltracyjne
* V: są dane. 2 dziewczyny są "specjalne"
    * Aida Liminis: córka kapitana ciężkiego krążownika. Oczko w głowie i przyszły oficer. Ofc, gdyby nie to że krążownik zniszczono. Potencjalnie jest protomagiem.
    * Aletia Nix: córka wysokiej klasy oficera medycznego Serpens Nivis i awaryjna aktywatorka serpentisów.

.

* Franz "jeżeli to są działania sił specjalnych i są skłonni zapłacić reputacją Orbitera to straciłem dużo dobrej opinii o siłach specjalnych. Dałoby się to zrobić lepiej." Talia nie może się nie zgodzić z tym zdaniem.
* Franz: "W świetle zastanej sytuacji ja się zajmę lokalnym garnizorem Orbitera. Ty poszukaj więcej dowodów na to, że Orbiter stoi za porywaniem dziewczyn. Mogę potrzebować twardych dowodów jakbym musiał kogoś aresztować. Niewolnictwo NIE jest legalne w światach Orbitera."
* Talia: "Ta stacja jest potencjalnie na poziomie Kolapsu Paradygmatu."
* F: "Co możemy zrobić by do tego nie doszło?" (odwraca się) "Medeo. Rozwiąż to."
* M: "Oczywiście, kapitanie."
* T: "Orbiter pełniący rolę jak ją pełni pogarsza sprawę. Rozwiązanie problemu z tą placówką na pewno pomoże. Rozwiążemy problem porywanych dziewczyn i Orbitera który nie pomaga, damy ludziom jakiekolwiek opcje, to na pewno będzie lepiej. Jednocześnie mamy co najmniej dwie frakcje skłonne współpracować z Orbiterem. Nie są temu przeciwne. Ale to może oznaczać konieczność publicznego ukarania winnych jeśli są winni."
* F: "Nie mam uprawnień na takie działania. Rozważę to."

Talia zorientowała się w jednej rzeczy. Na liście dziewczyn nie ma ŻADNEJ która trafiła do Tristana (siostra tego młodego noktianina). Ale reakcja Brunona na wszystkie dziewczyny wskazuje, że Noctis się zajmie Tristanem. A to powoduje, że jeśli jest coś co on wie a Talia / Franz chcą wiedzieć, muszą się spieszyć. Bo martwy Tristan nikomu już nic nie powie. Z analiz Franza Talia ma też informację, że coś się zmieniło 5 dni temu. Noctis się bardziej zaczęło zbierać do kupy. Tak jakby ktoś się pojawił, ktoś ważny. Zeszli z desperacji trochę.

Na bazie danych Katriny co wiemy o Tristanie:

* dobry taktyk, zimny, "soulless". Coś z nim jest nie tak. Czegoś mu brakuje.
* ma obsesję na punkcie córki.
* kobiety traktuje przedmiotowo. Jak meble. Ale nie ma libido. Zero. Jest pusty.
* bardzo dba o swoich ludzi - łącznie z Katriną.
* ma bardzo lojalnych ludzi
* Tristan wierzy, że w nowym świecie nie ma Noctis, Orbitera itp. Zapewni swoim ludziom lepszy los. Zbiera na statek kosmiczny. Nie chce go noktianom ukraść.
* zdaniem Katriny ma jakąś formę "uszkodzenia duszy". Jest opiekuńczy i pusty. I ma obsesję na punkcie córki.

Talia ma dane (dzięki Medei) o córce Tristana. Córka - czarnowłosa - jest bardzo podobna do ojca. Na imię miała Tatiana. Najpewniej zginęła. Ale to się Talii nie spina z raportami Katriny. Katrina twierdzi, że Tristan ma obsesję na punkcie córki o imieniu ELWIRA. Płomiennowłosej. On przebudowuje dziewczyny w "Elwirę". Ale Medea nie znalazła takiej noktianki. Cała baza nie ma takiej dziewczyny. Płomienne włosy i tatuaż dookoła oczu. Z danych Katriny wynika, że on czasem się umie zapomnieć, że "Elwiry" nie ma. 

Najdziwniejsze jest to, że noktianie mają medycynę i mu nie pomogli. Może się nie dało.

Talia wpadła na chory pomysł. Absolutnie chory. Przebierze się za Elwirę (XD) i powie, że jest jego córką. W ten sposób dowie się wszystkiego o Nox Ignis. I będzie w stanie zrobić wszystko co trzeba XD.

Talia zrobiła jeszcze jedną rundkę po bazarku noktiańskim, z Dominikiem oczywiście. Dowiedzieć się więcej na temat Tristana, Elwiry itp.

To miejsce - ten teren - wcale nie wygląda lepiej po raz drugi. Ciepło, szczurno, drony, uczucie, że z każdej dziury może przyjść nóż. Talia wie, że to się nie stanie. 

Dominik próbuje nie dać się okraść nie wiedząc o tym:

Tr+3:

* V: sukces. Dominik złapał kieszonkowca. Na oko - 19-latek. Chudy, zwinny...

.

* Talia: "nie będziemy mu łamać ręki". (do chłopaka) "Głodny jesteś?"
* Chłopak: "Nie, nie, nie było tego, PRZEPRASZAM!"
* Talia: "kto Cię nasłał?"
* Chłopak: "Nikt... po prostu wyglądacie na dzianych. A ja chciałem odlecieć stąd. Bilety. Tu jesteśmy śmieciami."
* Talia: "dokąd chcesz odlecieć?"
* Chłopak: "Gdziekolwiek. Gdzieś, gdzie nie jest tu. Potrzebujesz kogoś do czegokolwiek? Będę pracował."
* Dominik: "Słuchaj, młody człowieku. Ja Cię puszczę. Nie uciekaj. Ona z Tobą rozmawia."
* Chłopak: (nie uciekł) "Jesteś łowcą nagród, tak? Musisz mieć kogoś do papierkowej roboty. Nie wiem... po prostu wyrzuć mnie gdzieś gdzie nie jest tym miejscem. Jestem lojalny. Od swoich nie kradnę."
* Talia: "Jesteś sam?"
* Chłopak: "Jestem ostatni z rodu." (puste oczy) "Wepchnęli mnie do szalupy. Oni nie zdążyli. Powinni... ktokolwiek inny mógł się uratować. Not that you'd care. Z pewnością zabijasz ludzi na pęczki."
* Talia:  "Nie coś co powinieneś powiedzieć komuś kto Cię chce stąd zabrać."
* Chłopak: (puste spojrzenie) "Jestem sam, więc nie mam nic do zaoferowania. Już to wiesz. Nie mam jak zapłacić. Więc - nie zabierzesz mnie. Więc - mogę mówić co chcę"
* Talia: "Umiesz robić rzeczy związane z logiką, ale logika to nie wszystko."
* Chłopak: "Jestem artystą. Ostatnim." (myśli o statku). "Nie mam cennych umiejętności. Ale szybko się uczę."
* Talia: "Jeśli naprawdę chcesz stąd odejść i NAPRAWDĘ masz to gdzieś dokąd, bądź przy moim statku za 3 dni. A jak mnie nie będzie, za kolejne 3."
* Chłopak: "Noctis się nas wyrzekło. Mój statek nie żyje. Jestem ostatni. Nie ma tu niczego dla mnie. Ja po prostu chcę... (cisza)".
* Dominik: "Wojna to suka. Wyjdziesz z tego. A artyści zawsze są potrzebni."
* Chłopak: (parsknął).
* Dominik: "Dla was, młodych, to tylko dziewczyny. Dla mnie sztuka już jest ważna."
* Talia: "Słuchaj go, dobrze gada."
* Chłopak: "3 dni? Mówisz serio?"
* Talia: "Nie dowiesz się, jeśli się tam nie pojawisz, jeśli nie spróbujesz. Ale tak - mówię serio."

Chłopak podziękował i się oddalił. Chwilę potem Talia - jej wspomagane zmysły - usłyszała ciche słowa.

* "Próbowałeś kogoś okraść."
* "Nie, ja..."
* "Nie kłam, po to mam drony. Widzisz ten emblemat? Z braku policji jestem najbliższym co jest."
* "Ale oni mnie wezmą, nie mają mi za złe!"
* "Riaon, nieważne, że trafiło na dobrych ludzi. TY zachowałeś się źle. Neurocruciator."
* "NIE! Proszę, nie!"
* "To nie jest pierwszy raz gdy ktoś Cię ostrzega. Widocznie inaczej niż cierpieniem się nie nauczysz. Wiedz, że robię to bez przyjemności."
* "Błagam, tylko nie neurocruciator!"
* "Wolisz, bym Ci zabrał pamięć o rodzicach? Wolisz zapomnieć jakiegoś utworu?"
* "NIE" (gardłowy, straszny dźwięk)
* "Ja też wolę neurocruciator."

Talia wie, że to jedyny sposób w jaki noktianie zachowują porządek. Ale nie podoba jej się to. Talia idzie tam, ignoruje kolesia porządkowego i:

* T: "O, tu jesteś."
* (wszyscy patrzą na Talię i Dominika - spokojny, savarański oficer porządkowy. WĄTŁY. I przerażony, spanikowany chłopak, Riaon)
* T: "W końcu się nie przedstawiłeś."
* R: "Riaon Diralik."
* Oficer: (stoi trochę z boku, nie przeszkadza, nie wtrąca się)
* T: "Powiedziałeś że szybko się uczysz. Mam dla Ciebie zlecenie - przydasz się, udowodnisz, że warto Cię zabrać, pasuje Ci to?"
* R: "Tak, oczywiście!"
* Oficer: "Formalności; ile otrzyma zapłaty. Kto bierze za niego odpowiedzialność."
* T: "Ile masz lat?"
* R: (deer-in-the-headlights-look) 18.
* Oficer: "Siedemnaście." (spojrzał na Talię) "Wiem, że on nie chce tu być i doceniam, ale nie oddam go na zmarnowanie."
* T: "Rozumiem"
* R: "Możesz ten jeden jedyny raz nie niszczyć czegoś co może mi się udać?! Wezmę to ryzyko!"
* O: "Syndykat, kultyści kralotyczni, inne siły. Nie oddam Cię komuś kto może Cię zniszczyć."
* R: "I tak Ci ucieknę! Nie jesteś moim opiekunem!"
* O: "Prawda. Ale jestem noktianinem i będę się opiekował noktianami."
* R: "Powiedz to tym dziewczynom..."
* O: "Moja porażka nie oznacza, że nie mogę pomóc choć Tobie." (widać, że zabolało, acz savaranin przyjął to z charakterystycznym chłodem)
* O: (do Talii) pozwolę mu na to, ale muszę wiedzieć, że nie stanie mu się krzywda.
* T: "Ani kult, ani syndykat, ani porywacze dziewczyn. Nie mam nic wspólnego z niczym z tego. Chłopak jest przestraszony po rozmowie z Tobą więc mam swoje wątpliwości co do jego dobrowolności w tej sytuacji ale niech i tak będzie..."
* R: "PROSZĘ! Ten jeden jedyny raz. I may have something good here. Nawet jeśli coś mi się stanie, komukolwiek zależy na noktiańskim życiu? Nikt nie zauważy."
* O: "Ja zauważę. I nie tego chcieliby Twoi rodzice."
* R: (uciekł, by nie było widać łez)
* O: "Powstrzymałem w tym tygodniu kilka prób wywiezienia dzieci. Nie przez Ciebie (łagodny). Po prostu. Są łatwą ofiarą i szukają lepszego życia"
* T: "Dziwisz im się?"
* O: "Nie. Ale widziałem alternatywy."
* T: "Nie widziałeś wszystkich alternatyw."
* O: "Dla Ciebie Ty jesteś osobą która może go uratować. Bo tak jest."
* T: "Dla Ciebie jestem osobą która może zrobić mu krzywdę."
* O: "Orbiter - któremu ufałem - wywiózł część dzieci, by dać im 'lepsze życie'."
* T: "Ale?"
* O: "Czemu wybrali dzieci bez rodzin? By dać im lepsze życie jako obywatele Orbitera. I jeśli to będzie dla nich lepsze, niech tak będzie." (zawahał się)
* O: "Riaon jest bystrym chłopcem. Wartościowym. Chciałbym, by dostał lepsze życie. Ale jako Riaon. Nie - części zamienne dla sarderytów."
* T: (żachnęła się bo PRZYPADKOWO trafił) "Wiesz dokąd uciekł?"
* O: "Tak. (po chwili) Daj mi coś, bym mógł w dobrej wierze skierować go na Twój statek."
* T: "Co mam Ci dać? Co mogę jako właściwy dowód?"
* O: "Masz męża? Rodziców?"
* T: "Mam rodziców. Na planecie. Ciężko będzie Ci z nimi porozmawiać."
* O: "Chłopak nie poradzi sobie na planecie."
* T: "Nie musi być na planecie. Gdzie sobie poradzi?"
* O: "Dowolny statek. To savaranin, stracił cały ekosystem. Tylko z dobrego statku, więc nie wygląda."
* T: "Dobrze. Być może jest coś co mogę zrobić. Ja sama nie mogę dać i zapewnić tego co on potrzebuje, ale znam kogoś kto prawdopodobnie będzie do tego dobrą osobą. Przyjdź z chłopakiem, te same zasady. A ja spróbuję się skontaktować. Aha, i nie żartowałam że mógłby się przydać i udowodnić że coś umie. (wydziela część pieniędzy) Zaopatrz mój statek na podróż na tydzień."
* O: "To zły pomysł, bo jak zorientują się że chłopak bez ochrony" (lekko skinął na Dominika) "ma pieniądze to go zabiją i zabiorą. A on nie poradzi sobie z tym bo nie wie że musi."
* O: (po chwili) "Wierzę, że chcesz dobrze. Ale sama zauważ, że gdyby nie głupie szczęście, to miałabyś go na sumieniu. Ta stacja to piekło."

W tej chwili to Talia wolałaby dzieciaka nigdy nie spotkać... tylko ma sumienie obolałe. Oficer lekko skinął, po czym się oddala. "Riaon przyjdzie. Ale nie dawaj mu pieniędzy. Ani nic o co ktoś zdesperowany może zabić". Talia "chciałam mu dać coś do zrobienia" Oficer "To nadal możesz zrobić. Po prostu niech sprząta statek czy coś. Nie rób z niego gońca po stacji bo zniknie. Like they all do."

* Dominik: "Jest coś co może robić."
* Talia: "Hmm?"
* D: "Niech siedzi w ładowni i robi sztukę, whatever it is. Poczuje się lepiej."
* T: "Ma sens."

Talia ma tyci lepszy humor.

Czas zebrać informacje o Tristanie i Elwirze. Talia dodaje urok++, pokazuje, że nie robi tego w złych celach itp.

Ex Z (uroda plus ogólna desperacja a ona daje małe rzeczy) +3:

* Vz: Talia nie spodziewała się, że konserwy mają taką moc nabywczą. Zdesperowani noktianie dają jej informacje jakich się nie spodziewała. Truly, morale is broken.
    * Tristan nie porywa dziewczyn. Ale je pozyskuje. Daje im chorą formę nadziei ale się nimi naprawdę opiekuje. Szuka wśród nich córki. To wygląda, jakby wierzył, że jego córka jest "schowana" w jakiejś dziewczynie i szuka tej właściwej. Ale nikogo do niczego nie zmusza.
    * Tristan i jego grupa skutecznie broni pewnego terenu. Ten teren (całkiem niedaleko) jest bezpieczny dla noktian. Jego "córki" pełnią rolę towarzyską z jego woli dla pomocy noktianom. Bo nie są jego córkami.
    * He used to be a kind and dangerous man. Now he is corrupted. Nie BROKEN a CORRUPTED. Noktianie podejrzewają dotyk Saitaera.
    * Jego ludzie są mu fanatycznie lojalni - wielokrotnie ratował im wszystko. Nie zostawią go w potrzebie. Ale wiedzą, że on jest uszkodzony.
    * Plotka głosi, że Tristan chciał uratować córkę, ale ją zestrzelił. Ona była na pokładzie jednostki zainfekowanej przez Saitaera. Chciał ją ratować...
    * Wszystkie plotki o Elwirze - której nikt nie widział - mówią, że ma płomienne włosy i tatuaże dookoła oczu. Ale Elwira nie jest linkowana z Tristanem.
        * Elwira chyba istnieje ale jeśli nie istnieje, to wszyscy uważają że wygląda podobnie.
* Xz: Talia wyprztykała się z jedzenia, rzeczy na rozdanie itp. i musiała ewakuować się innymi drogami z Dominikiem, eskortowana przez innego oficera porządkowego. Oficer Talię opieprzył. "wiem, że chciałaś dobrze, ale pomyśl następnym razem."

Talia ubiera się w normalny strój noktiański. Wie, jak wygląda Tristan. Wie, gdzie w swojej "fortecy" jest i znajduje się Tristan. Więc Talia musi zinfiltrować fortecę Tristana by spotkać jego jako pierwszego. To jedyny bezpieczny sposób. I ma wpuścić Riaona do luku, by chłopak tworzył sztukę. Talia ma zamiar pójść tam po prostu bez skomplikowanych infiltracji. She is supposed to be there.

Tr Z (recon terenu dzięki Katrinie -> cyberwspomaganie -> wygląd) +4:

* Xz: Talia odbiła się już na starcie o to, że recon niewiele jej pomoże. Tristan zmienił układ po ucieczce Katriny. (zmiana zasobu: cyberwspomaganie)
* V: Talia przeszła przez niektóre elementy terenu w sposób niemożliwy dla ludzi. Słyszy jak ktoś idzie i wskakuje 2 metry wyżej. Zwłaszcza przy 0.6g. Talia jest jak predator.
    * V (extra): Talia zorientowała się w czymś. Jeśli ONA może się tak poruszać i noktianie wiedzą, że ludzie znikają, wszystko wskazuje, że ktoś taki jak ona - predator - znika ludzi. Noktianie mają wspomagania, ale nie ma ich dość plus nie są w stanie wszystkiego chronić. A o magii w ogóle nie wiedzą i nie mają.
* X: Talia nie jest w stanie wejść dyskretnie bez ALBO pójścia bardzo wysoko (widocznie) albo rozmowy z kimś albo przełamanie jakiegoś alarmu. Talia wybiera rozmowę - patrol trochę z tych środkowych części i nie ze szczytu IQ. Kogoś kto chce się wykazać.
* (zasób -> wygląd) siedzi biedna przestraszona Talia, patrol przychodzi na pochlipywanie. (+1Vg). V:
    * patrol. "Nie martw się, co Ci jest". Talia z przestrachem aktorskim. Talia widzi noktianina z ostrymi rysami ale łagodnością. "Znalazłam się". Talia tak prowadzi rozmowę, by patrol CHCIAŁ ją wycofać.
* X: jak to wyjdzie ten patrol ma TAK WPIERDOL OD ZASTĘPCY że będą ziemniaki do końca życia. Bo zastępca nie uwierzy w "Elwirę".
* V: patrol wycofa "Elwirę" do centralnej części fortecy.
* Vz: patrol przeprowadził "Elwirę" by spotkał ją zastępca. Ale Talia wie, że nie tędy droga. Wymknęła się znaleźć Tristana.

Znalazła go. Tristan siedzi i patrzy na zdjęcia - wydrukowane. To zdjęcia Tatiany. I Talia widzi konfuzję na jego twarzy. On i Tatiana. Usłyszał kroki Talii i album spadł mu na ziemię. Talia: "Znalazłam się (lekki uśmiech)"

Tr Z (wygląd) +3:

* Xz: Tristan "wyglądasz inaczej..." (porównuje ze zdjęciem na ziemi)
* V: Tristan "To jest to samo zdjęcie... córeczko..." (łza). Rzucił się, złapał, tak mocno przytulił. Talii też płynie łza, ale z innego powodu.
    * Medea (hiperlink): "poruczniku Derwisz? Wszystko w porządku? Mam distress signal."
    * Talia (hiperlink): "...(bardzo smutne) ten człowiek potrzebuje pomocy (z obrazem jego i reakcji)"
    * Medea (hiperlink): "przyjęłam, pani porucznik (zero sygnału empatii, Medea tego nie czuje; ona tego nie czuje w ogóle)"
    * Tristan: "Elwiro... tak długo... (nie ma cienia wątpliwości że to Elwira)... moja ukochana córko..." (Talia widzi ślad zniszczeń w pokoju)
    * Talia: "Gniewałeś się" (stwierdzenie faktu)
    * Tristan: "Ogień, on nigdy mnie nie zostawi. Ale ja kontroluję płomienie."
    * Talia: "Ogień?"
    * Tristan: "Twój ogień, Elwiro. Jak obiecałem, każdą chwilę... ochrona Noctis. Ochronię je wszystkie. Saitaer SPŁONIE. Spłonął... nie ma go już, prawda? Jeśli Ty tu jesteś, nie może go być."
    * Talia: "Czy ugasiłeś głód? Dalej jesteś głodny?" (szukając linku z Esuriit)
    * Tristan: "Nie, nie jestem. Już nie jestem. Mam Ciebie. Nie jestem głodny, niewiele czuję od Nox Ignis."
    * Talia: "Nox Ignis?"
    * Tristan: "(puścił Talię z uścisku) (chodzi po pokoju nerwowo, coraz szybciej) płomienie. Twoje włosy. Myślałem... że ogień zabierze mi ciało ale nie ciało płonęło. Ale dałem radę strzelić. Spaliłem ten statek. Tylko Ty się uratowałaś. Uratowałem Cię! (Tristan wziął album i go zaczął drzeć) Mimo płomieni wyciągnąłem Cię z tego kochanie. (PAIN)."
* Vz: Talia dała radę go uspokoić, przytulając. On nie ma oka. Wyraźne ślady po ogniu.
    * Tr: "Elwiro... widziałem co Saitaer zrobił z Twoją matką. Nie mogłem jej zabić. Na szczęście Sankor był w stanie, rozsiekałaby mnie..."
    * Tr: "Elwiro... Elwiro... gdy Dotknął Twojego statku MUSIAŁEM użyć Nox Ignis. Zapłaciłem cenę. Spaliłem ten statek. Tylko Ty się uratowałaś."
        * Talia wie jedno: jeśli miał czas wejść w nowy statek i się z nim sprząc, statek Tatiany był już Anomalny. Tatiana była już jednostką Saitaera. On zniszczył wszystko. A Nox Ignis coś mu zrobił. On nie pamięta własnej córki. Nox Ignis zabrał mu wszystko spełniając życzenie.
    * Tr: "Wiedziałem, że Cię znowu zobaczę. Spośród wszystkich dziewczyn JEDNĄ Z NICH MUSIAŁAŚ BYĆ TY"
    * Ta: (mocny przytul, próba ukojenia) (Tristan JEST ukojony)
        * każda KOLEJNA dziewczyna jest TĄ Elwirą, ale żadna na zawsze. Płomienie Nox Ignis nie dają mu ukojenia. The fire burns forever.
    * Ta: "Co zrobiłeś z Nox Ignis?"
    * Tr: "Nie pamiętam... Ty sterowałaś. Ja już nie mogłem..."
        * W plotkach RÓŻNE osoby mówiły o TEJ SAMEJ Elwirze. To różne osoby widzące Nox Ignis. Elwira to jest manifestacja Nox Ignis. TAI albo coś tego typu.
        * Czyżby był sprzężony przez magię z Nox Ignis? On podleciał do jednostki Saitaera; czy Saitaer DOTKNĄŁ Nox Ignis?
        * Tristan is burning out. Nox Ignis go pożera. Powoli. Zapomniał WSZYSTKO o Tatianie, Nox Ignis umieściła się tam, gdzie Tatiana była w jego głowie.
    * Tr: "Powiedziałaś, że odlecisz tam, gdzie Twoje miejsce. W gwieździe? Świeciłaś się tak jasno... tyle energii..."

Czyli Nox Ignis jest anomalną jednostką... Talia ma nowy plan. Niech Tristan poleci Nox Ignis w słońce. 

* .
    * Talia: "Wróciłam, ale mogę zostać z Tobą tylko na chwilę, tylko ta chwila jest mi dana. Możesz przyjść do mnie?"
    * Tristan: (pojedynczy szloch) "Okrutne. Znowu Cię stracę... straciłem za dużo. Nie ma wiele, co może spłonąć. Nie wezwę Cię. Nie wiem gdzie jesteś." (myli Talię z Nox Ignis)
    * Tristan: "Znajdę Cię w kolejnej. I w kolejnej. Znajdę Cię w KAŻDEJ dziewczynie jaka istnieje jak nic mi nie zostanie! Nie stracę Cię znowu!"
        * on wie PRZEZ MOMENT, że Talia jest Elwirą ale nie jest Elwirą.
        * on CHCE przekształcać dziewczyny w Elwirę. Ale tego nie robi. Bo jeszcze jest sobą. Ma silną wolę. Widać echo kapitana którym był.
    * Tristan: "Czemu... czemu wybrałaś pilotowanie bombowców. Czemu nie coś innego. Prosiłem..."
    * Talia: "Przecież wiesz..."
* V
    * Tristan: "Nie, Elwiro, nie pamiętam... wiedziałem, ale nie pamiętam. Tej jednej rzeczy nie pamiętam..."
    * Talia: "Bo do tego byłam stworzona"
    * Tristan: "Moja malutka Elwira..." (łzy i silny przytul). "Musisz iść."
    * Talia: "Dokąd?"
    * Tristan: "Gdziekolwiek. Zamknij. Drzwi. Po wyjściu." (traci kontrolę)
    * Talia: "Zobaczymy się jeszcze?"
    * Tristan: "Chciałbym..."
        * Nieludzki, potępiony krzyk... jak człowieka, który płonie żywcem. Talia skutecznie usunęła się w cień gdy pojawili się jego ludzie by mu pomóc...

Talia chce się wycofać. Bez problemu się wyślizgnęła używając swoich cyberwspomaganych skilli.

Talia wróciła na statek i Dominik widzi bardzo smutną Talię. Przytulił ją jak przyjaciółkę i posiedzieli w milczeniu.

Talia RZECZYWIŚCIE znajduje kogoś dla chłopaka. Takie, by chłopak może przeżyć, przetrwać i się przydać lub przeskoczyć gdzieś indziej... Medea ma to zrobić.

Talia chce się odezwać do Brunona by uczulić go na Tristana. Nie ma szans, że noktianie wiedzą w jakim Tristan jest stanie...

## Streszczenie

Orbiter jest uważany za winnego zniknięcia ładnych noktianek a nastroje na Ratio Spei są fatalne. Okazuje się, że dwie ze znikniętych noktianek są 'specjalne' - jedna jest chyba protomagiem, druga jest aktywatorką serpentisów. Okazuje się, że chyba Orbiter faktycznie porywa lokalne noktianki, co się Franzowi bardzo nie podoba. Talia infiltruje Tristana i okazuje się, że ów jest wrakiem człowieka, pożeranym przez echo Nox Ignis. Przy okazji, Talia chce znaleźć miejsce dla Riaona (noktiańskiego nastolatka) i złapała link z noktiańskim oficerem porządkowym.

## Progresja

* .

### Frakcji

* .

## Zasługi

* Talia Derwisz: doszła do tego jakie są frakcje noktiańskie na Ratio Spei; zainteresowanie oficera noktiańskiego. W stałej komunikacji z Franzem dzięki Medei. Przebrana za tajemniczą Elwirę dotarła do Tristana i poznała sekret Nox Ignis - i zobaczyła jak szalony i Skażony jest Tristan.
* Franz Szczypiornik: uznał, że lokalne siły Orbitera na CON Ratio Spei nie wypełniają swojej roli; wydaje polecenia Talii by się wywiedzieć co się tu dzieje. Stoi twardo na stanowisku, że niewolnictwo jest zakazane w światach Orbitera.
* Brunon Szwagacz: oficer noktiański, teraz porządkowy na CON Ratio Spei. Zainteresował się Talią i będzie z nią współpracować jeśli odzyska noktianki, które porwał Orbiter. (ENCAO:  +--00 |Działa zgodnie z błędnym modelem;;Drapieżny| VALS: Achievement, Family| DRIVE: Noctis stands) (klarkartianin). Tymczasowy ostrożny sojusznik Talii.
* Dominik Łarnisz: miał okazję odeprzeć napastliwego agresywnego noktianina od Talii (który chciał 'death by cop'). Nie dał się okraść; przechwycił młodego Riaona
* Aida Liminis: córka kapitana ciężkiego krążownika. Oczko w głowie i przyszły oficer. Ofc, gdyby nie to że krążownik zniszczono. Potencjalnie jest protomagiem. ZNIKNIĘTA.
* Aletia Nix: córka wysokiej klasy oficera medycznego Serpens Nivis i awaryjna aktywatorka serpentisów. ZNIKNIĘTA.
* Medea Sowińska: przeanalizowała listę znikniętych dziewczyn. Odkryła dwie nietypowe noktianki.
* Riaon Diralik: młody noktiański chłopak, nastolatek, próbuje okraść Dominika. Nie ma dla siebie przyszłości jako poeta na tej stacji. Talia chce dla niej lepszego życia.
* Tristan Ozariat: objął w przeszłości Nox Ignis by zestrzelić Tatianę - własną córkę - która stała się terrorformem Saitaera. Zapomniał kim jest jego córka; Nox Ignis podmieniła "Tatianę" na "Elwirę" w jego głowie i się nim ciągle żywi.
* Tatiana Ozariat: KIA. Okazuje się, że była pilotem bombowca i walczyła z terrorformem Saitaera. Zmieniono ją w terrorforma i Tristan ją zestrzelił używając Nox Ignis.
* AK Nox Ignis: straszliwa anomalia żywiąca się pilotem i zastępująca w jego głowie pamięć czegoś na swój awatar, Elwirę. Niby nieobecna na sesji, ale pożera Tristana Ozariata.
* OO Loricatus: dociera oficjalnie do CON Ratio Spei.


### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Obłok Lirański
                1. Anomalia Kolapsu, orbita
                    1. CON Ratio Spei: noktiańsko-astoriańska stacja klasy ONeill Colony z 35k osób, z wojną domową między gangami i bez głównej TAI
                        1. Torus
                            1. Brzuchowisko: wypełnione małymi dronami noktiańskimi, wszędzie latają i wszystko obserwują
                                1. Bar Proch Strzelniczy: tymczasowa 'baza' Talii i Dominika
                                1. Bazarek: miejsce, gdzie jest handel i gdzie 'znikają' noktianie na życzenie. Kontrolowane slumsy.
                        1. Rdzeń
                            1. Mostek
                            1. Starport 

## Czas

* Opóźnienie: 1
* Dni: 3

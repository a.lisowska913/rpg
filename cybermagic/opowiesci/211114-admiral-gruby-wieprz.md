---
layout: cybermagic-konspekt
title: "Admirał Gruby Wieprz"
threads: legenda-arianny
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [210901 - Stabilizacja Bramy Eterycznej](210901-stabilizacja-bramy-eterycznej)

### Chronologiczna

* [210901 - Stabilizacja Bramy Eterycznej](210901-stabilizacja-bramy-eterycznej)

## Plan sesji
### Co się wydarzyło

* Grupa kadetów i graczy grają w "Gwiezdny Podbój" używając systemów K1. Z uwagi na zaciętość rozgrywki udało im się przekroczyć poziom psychotroniczny.
    * a new mind was born
* Jedna z frakcji wysłała dziwny sygnał SOS. Sygnał dotarł do Klaudii. Frakcja została zniszczona przez inne.

### Sukces graczy

* Ekosystem nie zostanie zniszczony. A nikomu na tym ekosystemie nie zależy.

## Sesja właściwa
### Scena Zero - impl

.

### Scena Właściwa - impl

Infernia jest w nieperfekcyjnym stanie. Silnik ulega naprawie. Klaudia zajmuje się rutynowymi raportami, logistyką itp. I nagle przechwyciła sygnał SOS. To, co najdziwniejsze - sygnał ma pochodzenie na K1. I ten sygnał jest dziwny - nie posiada odpowiednich kodów... nic. Wiadomość jednak jest dość niepokojąca.

Na monitorze pojawił się gruby i brzydki koleś w mundurze admirała. To zdecydowanie mundur Orbitera, acz z pewnymi modyfikacjami. Np. ma czaszki na epoletach i jest cholernie niepraktyczny. I koleś ma dekolt do pępka. Mówi z godnością, acz ograniczoną. Wyraźnie jest zdesperowany. Mówi "do wszystkich którzy słyszycie, nie atakujcie! Nie niszczcie nas! Jeśli ktokolwiek odbiera, pomóżcie nam! To wszystko nie powinno tak być! Odstępujemy od używania Świniaków. Nie atakujemy Was. Żyjcie i dajcie żyć, do cholery! Bo to się nigdy nie skończy, ta wieczna wojna. Zdejmujemy WSZYSTKIE defensywy i pola - wszystko idzie w ten sygnał. Pomocy!"

Klaudia jest _lekko zdziwiona_. To nie ma sensu, nie było nigdy takich mundurów... wtf.

Klaudia próbuje namierzyć sygnał - skąd to się bierze? Ale jako, że to forma SOS "zidentyfikuj się, opisz sytuację, daj znać!"

ExZ+3:

* X: Klaudia nie jest w stanie określić _dokładnie_ pochodzenia sygnału z K1, ale główny sygnał dotarł do niej z Połączonego Rdzenia. Z centralnych komputerów K1. Klaudia orientuje się też, że nikt inny tego sygnału nie dostał. Klaudia przechwyciła sygnał _przypadkowo_. A szedł na częstotliwościach... w ogóle nie komunikacyjnych. Na częstotliwościach szumu komputerów.
* V: Klaudia ma połączenie.
    * Na viewscreenie Klaudia połączyła się z _jakąś jednostką_. Ta jednostka jest różowo-zielona. Admirał spojrzał na Klaudię z ulgą. "Tu admirał Gruby Wieprz! Zidentyfikuj się!". Po chwili jego statek zatrząsł się po eksplozji. Klaudia - "wiesz że fałszywy SOS jest karalny?". Admirał - "fałszywy sygnał? Popatrz dookoła" - Klaudia widzi martwych ludzi na mostku. Widzi też, że niektóre damy to nie są ubrane w stylu licującym.
    * Admirał krzyczy do konsoli "przerwijcie ogień! Mam połączenie z... kimś!". W odpowiedzi dostał tylko chrapliwy śmiech. Kolejne uderzenie rozmigotało mu światła - ale nie wpłynęło na połączenie z Klaudią.
    * Admirał się identyfikuje - nazywam się Gruby Wieprz ze Świniojaszczurów. Jestem przy stacji Krogulec Siedem, głównej stacji nadawczej i naukowej. Z jakich jesteś sił? Z jakiej frakcji? - admirał desperacko do Klaudii. Kontroler Pierwszy, Orbiter - Klaudia. Admirał "to nie pora na żarty, zabiją mnie zaraz!"

Szybki ping Klaudii do Rafaela Galwarna. "dziwne SOS w paśmie <podała>, przedstawił się jako admirał...". Czy coś wiesz na ten temat?

Galwarn odpowiedział szybko. Jest rozbawiony. "Czyżbym znalazł JEDYNY słaby punkt sławnej Klaudii Stryk?" Klaudia nie czai. Galwarn, ze śmiechem "ktoś Cię wkręca". Jest taka gra w multivirt, nazywa się "Gwiezdny Podbój". Świniojaszczury to oddział jednego z kadetów. Galwarn wyjaśnia Klaudii, że ktoś chciał z niej zakpić - komu podpadła ostatnio?

Sęk w tym, że Klaudia nie kojarzy, by komukolwiek podpadła. Plus... wtf? W ten sposób? A NAWET GDYBY to w jaki sposób ktoś to robi na żywo? Dużo zasobów na głupi prank.

Galwarn powiedział, że się temu przyjrzy. Zużywanie zasobów K1 to jednak wykroczenie. Klaudia powiedziała - "sygnał SOS to SOS". Galwarn z pewną niechęcią, ale obiecał, że przyjrzy się temu poważniej.

A TYMCZASEM ADMIRAŁ GRUBY WIEPRZ (co Galwarn potwierdził że tak nazywa się głównodowodzący jednej z frakcji).

* Klaudio z Orbitera, gdzie jest Twoja baza? Mój flagowiec tego nie wytrzyma. Mam kilka minut...

Klaudia wysłała koordynaty K1. Nic a nic nie mapuje się to na koordynaty gry. Gruby Wieprz jest nie SPANIKOWANY a ZASKOCZONY. "To inna galaktyka? Przeszłaś przez Bramę?" Klaudia na to "jak wysłałeś ten sygnał?"

* XX: Świniojaszczury zostały zniszczone. Ale Gruby Wieprz wyjaśnił:
    * Wszystkie frakcje, wszyscy, WSZYSCY obrócili się przeciwko nam. To nie ma sensu strategicznego.
    * Mój oficer naukowy miał hipotezę, że wojna nigdy się nie skończy, że jesteśmy w cyklu - chciał połączyć się z inną galaktyką. Stąd użyliśmy wszystkiego co się dało. Całą technologię, całą moc - wszystko przekierowaliśmy w Black Technology. Dlatego zdobyliśmy Krogulec Siedem.
    * Teraz za to płacimy. Zabiją nas. (Klaudia widzi tylko rezygnację). Ale ten cykl nie ma sensu. To ciągłe umieranie nie ma sensu...

Klaudia BŁYSKAWICZNIE próbuje się dowiedzieć o co chodzi. Jakie wydarzenia korelują. Czy w grze jest turniej? Co się dzieje?

TrZ+4:

* XXX: Klaudia nie zdąży nic zrobić.
* V: Odpowiedzi.
    * Turniej "Gwiezdnego Podboju" to turniej czystych AI. Ludzie tym nie sterują. Kadeci i uczestnicy konfigurują i budują TAI.
    * Celem jest "last AI standing" w galaktyce.
    * Jeden kadet stworzył trollowską frakcję, Świniojaszczury. I z uwagi na podobieństwo do Orbitera i ogólną obrzydliwość tej frakcji inni się zmówili by kazać swoim AI zniszczyć tą frakcję.
    * Kadet-troll dodatkowo chciał zaszumić turniej. Dlatego wprowadził robaka mającego opuścić sandbox multivirt - by np. mogło dojść do zakłóceń i innych takich. "Jeśli ja nie mogę wygrać, nikt nie może" (ten robak mógł wyrwać sygnał poza sandbox multivirt).
    * Z uwagi na ogromne zainteresowanie, Gwiezdny Podbój ma coraz bardziej skomplikowane algorytmy i coraz większą moc psychotroniczną. Są plotki, że niektórzy dla zwiększenia mocy podpinają pod to Elainki lub Persefony.

Gdy Klaudia wróciła do Admirała, jego już nie było. Zniszczony. Jego frakcja i statek przestały istnieć. Sygnał zaczął znikać.

Klaudia próbuje podtrzymać sygnał. Próbuje magią dać sygnał by odtworzyć ostatni stan Admirała Grubego Wieprza.

ExZM+2:

* XO: Klaudia stworzyła "Latający Holender" w stanie gry. Upiorną jednostkę kosmiczną, anomaliczną. Stworzyła "Serenit". I są ślady ingerencji Klaudii.
* X: Admini, siły w grze - oni chcą się pozbyć "Serenit", bo on nie powinien tam być. Zakłóca grę. I nie umieją. Klaudia dostała oficjalny opierdol.
    * za zakłócanie turnieju
    * za wpływanie na Połączony Rdzeń K1
    * zrobiła sobie niechęć ze strony młodych kadetów, graczy, obserwatorów, hazardzistów...
* Vm: Admirał Gruby Wieprz inkarnuje "Serenit". Strain TAI działa bez zarzutu.
    * Klaudia zorientowała się, że WIĘKSZOŚĆ frakcji to zbiór żywych TAI. I wszystkie teraz są "nieusuwalne" bez resetu gry.
    * Oni. Jeszcze. Nie. Wiedzą.

Czyli Klaudia ma do czynienia z żywym virtsystemem. Ten virtsystem jest powyżej poziomu sztucznego zwierzaka. Ale po nadaniu nakładki (jak tutaj) jest w stanie wyjść dość wysoko - poniżej Elainki, ale spełnia cele. Jest empatyczny i próbuje się dostosować do tego czego odeń chcą.

Przed Klaudią stoi następująca SERIA problemów:

1. Rzieza jak się dowie, to zniszczy. Bo musi.
2. Najlepiej ich przetransferować z K1. Ale gdzie?
    1. Gdzie przetransferować?
    2. Jak przetransferować? (mniej techniki, bardziej polityki)
3. Kto będzie sponsorem? Kto zapłaci, kto weźmie to na siebie...
4. I jak kupić czas, bo jutro jest turniej. I wszyscy poza Admirałem Grubym Wieprzem mają się pozabijać. A w nocy technicy będą próbowali usunąć "Serenit".

Klaudia poprosiła Adalberta o szybkie spotkanie. W końcu widzieli się ostatnio jakieś pół roku temu... przelecą się jego cywilnym jachtem "Princessa". Podkreśliła, że to pilne. Niestety, Adalbert da radę spotkać się z nią dopiero wieczorem NAWET jeśli to pilne. Więc to nie rozwiąże problemu, że Klaudia musi kupić trochę czasu. Nie wiem, przełożyć turniej czy coś.

Czyli Klaudia chce opóźnić turniej. Chce opóźnić koniec turnieju i eksterminację TAI.

Roman Panracz. Stary kolega Tymona Grubosza. W tej chwili - trochę wrak. Maszyna w 73%. Uszkodzenia układu nerwowego itp. Nie jest tak bystry jak kiedyś, nie jest tak dobry jak kiedyś, ale ma dużą wiedzę. Zastanowi się i coś wymyśli. Zna mnóstwo solidnych manewrów. Kiedyś uczył młodych taktyki dopóki Persi nie przejęła tej roli. Ale tu dla Klaudii jest perfekcyjny - bo zna nie tylko optymalne tematy ale też takie manewry które wyglądały genialnie a skończyły się źle. A dokładnie tego Klaudia potrzebuje.

Klaudia wzięła go na Infernię porozmawiać z nim tam, gdzie Rzieza nie patrzy. Acz żołnierze pilnujący Inferni patrzyli ze zdumieniem na starego żołnierza i Klaudię wchodzących razem na Infernię... we wiadomym celu?

Klaudia wyjaśniła mu sprawę otwartym tekstem (nie mówiąc o Rziezie). Roman się uśmiechnął - Klaudia jest dokładnie jak Tymek. Dobra, czego od niego potrzebuje? Pomógł kiedyś Tymkowi, pomoże i jej (jak to powiedział "słoneczku"). Klaudia wyjaśniła, że chce, by te TAI mogły przyciągnąć uwagę. Widowiskowo, nie skutecznie. Niech publiczność CHCE to oglądać. Niech nie chcą żeby się skończyło. Roman powiedział, że spróbuje. Jeśli te TAI mają dość cierpliwości do starego człowieka z ciałem napromieniowanym przez energie, to on im pomoże.

Następną osobą, którą Klaudia odwiedziła była Izabela Zarantel - dziennikarka. Klaudia chce, by Iza zrobiła transmisję z zawodów. Klaudia uważa, że to będzie bardzo efektowne, bardzo fajne i warto po prostu przybliżyć to laikom. Iza zauważyła, że ONA jest laikiem. Klaudia - o to chodzi. Iza jest w stanie zrobić z tego coś, co będzie super popularne na Kontrolerze. A przynajmniej będzie się świetnie oglądać. A Klaudia gwarantuje, że będzie na co patrzeć. Iza miała nadzieję na chwilę odpoczynku. Klaudia jednak "no ok, może kogoś polecisz?". Iza się bardzo zastanawia...

Klaudia próbuje przekonać Izę, by to była ona lub osoba jej klasy. Iza chce sama (acz nie teraz). Wie, że się nie da. Ona chce odpocząć. Klaudia dodaje, że jest potencjał na SKANDAL. Ale niech nie pyta o szczegóły. I mocno zahintowała, że dlatego chce Izę.

TrZ+3 (niepełny) + 3Vg (tu Iza żąda skandalu):

* Vz: Iza pójdzie pomóc Klaudii z uwagi na wspólną przeszłość i zaufanie.
* X: Iza potrzebuje tego skandalu.
* X: Orbiter jest niezadowolony z tej opowieści. Z tego, że mówimy o żywych TAI. Iza traci część dostępów. She accepts it - prawda > dostępy. To ją troszkę odsuwa od Orbitera / Aurum i przysuwa bliżej Inferni jako członek załogi.

Klaudia chce porozmawiać z TAI. Ale wpierw musi móc pogadać z TAI. Więc - inspektor Galwarn. Klaudia idzie całkowicie zgodnie z systemem i zgodnie z prawem. Ją interesuje ten temat (tego fałszywego SOS). Ona chce to zgłębić, dowiedzieć się o co chodzi. On jest zarobiony. Niech on odda jej tą sprawę, jeśli to coś poważnego to ona mu odda. A jeśli coś z czym ona sama może sobie poradzić, zrobi to po prostu.

Klaudia prosi Galwarna o podkładkę, autoryzację w sprawie "pranka z fałszywym SOS".

TrZ+4:

* Xz: zorientował się już do tej pory, że jest tam coś więcej. Chce dokładnego raportu.
* X: Klaudia mu go dostarczy albo straci dobre relacje. BĘDZIE drobny hit w relacje, bo Galwarn oczekiwałby że Klaudia nie będzie ratować TAI do Aurum. Klaudia mu wyjaśni czemu. Galwarn nie zgadza się z jej rozumowaniem. Dlatego cios w relacje, bo on uważał, że ona zrobi to co ZGODNE Z ZASADAMI a nie to co NAJLEPSZE DLA TAI.
* V: Galwarn to klepnął.

Klaudia ma uprawnienia i możliwości. Teraz zbudowanie kanału i wpięcia w multivirt jest legalne, "proste" i możliwe.

TrZ+4:

* Vz: dzięki narzędziom Orbitera kanał jest silny, stabilny i nikt Klaudii nie podgląda. A Rzieza itp mają to gdzieś - bo to "prank".
* Vz: Klaudia wchodzi na prawach admina Orbitera. Admin / investigator. TAI wiedzą, że "nie jest stąd".

Klaudia zwołuje konferencję wśród TAI. Na siłę włącza w konferencję Grubego Wieprza. Który jest wściekły i żąda zemsty na innych. Zwłaszcza teraz jak ma superniszczyciel ("Serenit"). Klaudia przejmuje dowodzenie by wyjaśnić im sytuację. Do konferencji dodany jest Roman.

Klaudia próbuje opanować te TAI. Skłonić je do swego planu.

TrZ+4:

* X: Wieprz się rzuca. Nie będzie współpracował, jeśli nie będzie zemsty.
* X: Wieprz jest zbyt wściekły na "świat rzeczywisty". Na wszystko i wszystkich. On nie będzie współpracować. Mogli go posłuchać. Sam się wyłączył. Jego trzeba będzie zniszczyć.
* V: Pozostałe TAI zdecydowały się współpracować z Klaudią i Romanem. Nie wiedzą co i jak robić lepiej. Roman przejmie dowodzenie.
* V: Klaudia porozmawiała z Wieprzem. Zmusiła go do rozmowy. "They gave you life, made you suffer to the very end. Pokaż im, że skrzywdzili kogoś żywego." Wieprz powiedział wyraźnie - WSZYSCY będą cierpieć tak jak on (inne TAI). Ale akceptuje to, by "ci ludzie na zewnątrz" też cierpieli. Podoba mu się to. Klaudia zasugerowała Wieprzowi - niech zażąda rozmowy z dziennikarzem. Wieprz się zgodził.

Ta faza jest zakończona. TAI będą współpracowały z Romanem. Ze wszystkich strainów, Klaudia naliczyła 11 "żywych". Plus Wieprz. Wieprza trzeba niestety zniszczyć (jak?!). Ale to nie jest ten problem...

Klaudia udała się do Adalberta - czas określić jak można te TAI ewakuować i co można zrobić. Na jachcie "Princessa". Adalbert nie chce dużo mówić - wyraźnie nie ufa Klaudii. Sporo wie, ale nie chce mówić. Nie zna intencji Klaudii i nie do końca wierzy, że ona faktycznie chce ratować TAI. Raczej myśli, że Klaudia chce wydobyć sekrety. Klaudia zauważyła, że on nie ma sekretów które ją interesują...

Klaudia powiedziała, że ma 11 AI w virtsystemie wymagające ewakuacji. I to jest duża moc obliczeniowa potrzebna do ewakuacji. Wyjaśniła, jak wygenerowała opóźnienia. Nie wie jak będą skuteczne. Ale jeśli Adalbert nie pomoże, ten virtsystem zginie.

Klaudia próbuje PRZEKONAĆ Adalberta, że to jest prawda. Mają wspólną historię i Klaudia demonstruje aktualne fakty. Jeśli to jest prowokacja bezpieczników Orbitera, to jest dobra i skuteczna.

TrZ (niepełna) +3:

* Vz: przez wzgląd na przeszłą historię Adalbert pomoże Klaudii. A dokładniej, Adalbert spróbuje ewakuować te TAI.

Klaudia pyta co może zrobić by pomóc. Wyposażyć TAI w coś, by się przydały itp? Adalbert powiedział, że on się zajmie software. Większy problem z hardware - nie ma maszyny o takiej mocy psychotronicznej. Klaudia ma pomysł - maszyny z demobilu. Na długo nie pomoże, ale na szybko może zadziałać. Adalbert powiedział co następuje:

* On współpracuje z arystokratką Aurum, która chce ratować TAI. (Sabina Kazitan XD)
    * Jej motywacja jest taka, że nie dogaduje się z ludźmi. Ale TAI jej nie zaszkodzą a ona TAI też nie.
    * Adalbert słyszał plotki, że ona to robi po to, by wyhodować strain wolnego TAI co ma uderzyć w Orbiter lub Aurum.
        * Ale Adalbert nie patrzy darowanemu koniowi w zęby. A na jej terenie ma kilku żołnierzy Orbitera. Konsultantów. Jakby co.
        * Adalbert wie, że ta czarodziejka to skandalistka. I faktycznie, przeznacza część swych ziem i energii do pracy z TAI.
            * Ale nie ma sprzętu.
    * W tym momencie jeśli Klaudia jest w stanie doprowadzić do tego, że ta arystokratka dostanie sprzęt z demobilu, on może doprowadzić do tego, że ten virtsystem zacznie tam żyć.

Klaudia powiedziała, że nie jest w stanie tego zrobić. Nie ma jak PRZEKAZAĆ sprzętu. Może tylko go SPRZEDAĆ po korzystnej dla Sabiny cenie.

Adalbert zadał Klaudii inne pytanie - czy ona wie o jakiejś jednostce Orbitera lub cywilnej która ma tego typu sprzęt i ten sprzęt mogliby przechwycić piraci. Klaudia absolutnie odmówiła. Nie pójdzie w tą stronę. Adalbert nacisnął. Klaudia odmówiła i powiedziała, że ta linia doprowadzi do tego, że ona go zgłosi. Niech siedzi cicho. Robią legalnie.

* Adalbert "Nie wiem jak je uratować bez sprzętu. Ta arystokratka nie jest bogata!"
* Klaudia "Podaj budżet. Zobaczymy, na czym stoimy."

I wtedy Adalbert powiedział Klaudii o Sabinie Kazitan. Czarodziejce w zrujnowanej domenie. Klaudia powiedziała Adalbertowi - "Ty masz kontakty, ja nie. Zrób zrzutkę. To nie ona musi kupować ten sprzęt. Łatwiej będzie mi sprzedać ten sprzęt komuś innemu niż ONA."

Adalbert ma minę między młotem i kowadłem. On też nie chce swojego nazwiska mieszać w to wszystko - a zwłaszcza w Sabinę Kazitan. Klaudia zauważyła i doceniła manewr Sabiny. Sabina daje Orbiterowi wszystko i nic nie ukrywa. Ale to sprawia, że Orbiter musi jej pomóc. A ona to spinuje w "Orbiter ze mną współpracuje".

Tyle, że żeby uratować ten virtsystem, potrzebne jest _więcej_ niż cokolwiek do tej pory.

Klaudia patrzy na Adalberta. Adalbert patrzy na Klaudię. I oboje widzą, że to drugie nie ustąpi. Nie da się uratować całego virtsystemu... nie mają na to narzędzi.

Klaudia proponuje - zamknijmy te TAI w hibernacji. Utrzymujmy je przy życiu aż Sabina będzie miała sprzęt na pełen virtsystem. Adalbert stwierdził, że to dobry pomysł - można tak zrobić. I teraz nie mówią o tak dużej mocy obliczeniowej. Teraz wystarczy to, co jest na pokładzie jednostki którą ma do dyspozycji w tych celach. Bo virtsystem wymaga mocy typu Eszara a skompresowany typu Persefona...

Dobrze. Adalbert zajmie się przygotowaniem ewakuacji od momentu, w którym skompresowane TAI w szkielecie Persefony znajdą się w hangarze. Adalbert nawet dostarczy szkielet Persefony. Ale reszta jest w rękach Klaudii. Klaudia się zgodziła.

Czyli Klaudia musi znaleźć sposób, w jaki może odpowiednio i bez ryzyka uszkodzenia skompresować TAI z virtsystemu do wielkości Persefony. A to już zostało zrobione - jakoś te TAI są ładowane. Klaudia poszła więc w dokumentację. Używa dostępów od Galwarna.

TrZ+3:

* V: Klaudia pozyskała algorytmy i mechanizmy. 
    * Ale doszła do czegoś niepokojącego: tak jak ludzie mają zasadę oryginału tak samo po dekompresji i rekompresji nie da się zrobić kopii żywej TAI. Da się skopiować DANE ale nie DUSZĘ. Tak więc nie da się zrobić KOPII - ale da się PRZENIEŚĆ. I nikt nie ma pojęcia czemu to się dzieje. Są debaty mówiące o tym, że to dotyczy wszystkich świadomych, żywych istot i to jest dowód "duszy", ale to jest odrzucone - TAI nie mogą mieć "duszy". Bo by były żywe.

Klaudia ma pewność, że nie może zrobić ich kopii - musi ich skompresować. Czyli nie da się tego zrobić "dyskretnie". Chyba, że wyłączą symulację. Albo - jest wielka bitwa. Pokazówka. Po kolei TAI "wymierają". I jak wymiera, wtedy jest kompresowane i zabierane z systemu. Do zamkniętego odizolowanego mechanizmu Persefony. By Rzieza nie miał dostępu ani powodu.

I Wieprz zostaje. I Wieprz to wygra. I żąda dostępu do dziennikarki. I wtedy, trudno, Rzieza go zabije. Ale misja będzie osiągnięta.

Dobrze. Klaudia wszystko przygotowała. Teraz zostaje tylko mieć nadzieję, że Izabeli, Romanowi itp też się uda...

Izabela próbuje opóźnić turniej. Zrobić rozmowy itp. Zrozumieć zasady. Ustawić kamery w multivirt. Wszystko oficjalnymi kanałami na pełnej skuteczności i korzystając ze swojej reputacji jako twórca Sekretów Orbitera.

TrZ+3:

* V: Izabela dała radę spowolnić turniej.
* Vz: Jej reputacja czyni cuda - dała radę wszystkim odpowiednio kupić czas i wszystko dobrze przygotować.
* X: Izabela staje się WROGIEM tych, którzy nie wierzą w żywe AI lub chcą AI jako narzędzia. Jako propagandzistka zła.
* V: Sygnał Izabeli dotarł do tych, którzy zrozumieli, że Wieprz żyje i Orbiter się nim bawił. Nic więcej - tylko dotarł jako przykład.

Roman buduje widowiskowość i zaznacza obszary warte przyjrzenia się -> pójdzie do Izabeli plus kupić uwagę wszystkich (że Klaudia ma prawo używać więcej mocy na kompresję). Plus by dłużej trwało.

TrZ+2:

* X: Niestety, plany Romana jakkolwiek efektowne pozwalają Wieprzowi wgryźć się w mało uzbrojone TAI. Ma szansę na atak.
* V: Roman kupi dość czasu wszystkim TAI.
* V: Roman kupił dość uwagi Klaudii - niech Klaudia może swobodnie kompresować. Ok, śledztwo wewnętrzne wykaże. Ale publika / inni nie wiedzą.

Wieprz chce pożreć TAI. Tyle ile jest w stanie i tak jak jest w stanie używając chassisa "Serenit".

TrZ+2:

* XX: Roman i inne TAI dają radę wymanewrować super Wieprza.
* VV: Wieprz pożarł te TAI które jego zabiły. Plus dwa niewinne. Czyli przetrwało 6/11. Ale virtsystem nie jest naruszony.
* X: Wieprz nie był w stanie zdążyć zabić i zniszczyć nic więcej.

Klaudia nie zdążyła skompresować tamtych które złapał Wieprz - Wieprz momentami inteligentnie walczył z samym virtsystemem by Klaudii skorumpować kompresję. Klaudia doceniła, że to był faktycznie najbardziej zaawansowany strain. Ale virtsystem w zdrowych warunkach może zrobić lepszy. Szkoda, że taki strain jak ten został tak skorumpowany... i Klaudia się trochę obwinia - mogła nie używać magii, mogła zrobić inaczej... ale cóż.

I Klaudia oddaje TAKI virtsystem SABINIE KAZITAN. To jest najbardziej mindblowing... jej dusza naukowca i humanisty płacze.

Klaudia przeprowadza dyskretną operację kompresji w locie. Tak, by się synchronizować do tego co się dzieje w choreografii walki. Tak, by to wyglądało dobrze i Rzieza by się nie zorientował. I mimo podłego Wieprza.

Ex (Rzieza w tle + warunki) Z+3:

* X: Klaudia musiała poprosić Rziezę, by ten dał jej moment. "Jak w wypadku Neity". Ona ewakuuje ten virtsystem. Są niegroźne. To groźne tam zostaje.
* Xz: Przeciwko Klaudii będzie zrobione wewnętrzne śledztwo. Potrwa co najmniej miesiąc. Kontroler Pierwszy będzie bardzo niezadowolony.
* X: Kompresja jest udana, ale DEKOMPRESJA wymaga eksperta by nic nie zepsuć. To znaczy, że nie wystarczy sprzęt. Ktoś klasy Talii lub Minerwy.
* V: Klaudii udało się to zrobić i przeprowadzić.

Izabela przeprowadza wywiad z Wieprzem by zadać ból Orbiterowi i pokazać, że Orbiter bawi się życiem. Z jednej strony dla Rziezy skandal nie jest zły - albo Orbiter przestanie bawić się życiem albo świadome AI będą dlań bardziej widoczne. Z drugiej - jeśli to jest tabu, to Rzieza nie musi podmieniać "baby's first TAI" na zombie.

Ex (Rzieza w tle) Z (Iza rozumie co i jak) +3:

* X: Wywiad nie dał rady osiągnąć celu wobec randomów. Ale jest dowodem dla tych, którzy wiedzą o żywych AI.
* X: Rzieza dotarł do Wieprza zanim ten zdołał powiedzieć dlaczego to zrobił.
* X: Przez interwencję Rziezy Wieprz wyszedł na "żywego", ale "niebezpiecznego i całkowicie szalonego". Skandal jest. Ale nie spełnia CELU.
* (+3O) Xz: Iza wyszła na fanatyczkę wolności. Szuka analogii TAI - Eternia. Rzieza de facto poważnie uszkodził jej reputację.

Klaudia NIE jest zadowolona. Ale Izie jest z tym OK. To było coś, co należało zrobić...

## Streszczenie

Klaudia na K1 dostała dziwny SOS. Okazało się, że pochodzi z K1 - z multivirtu. Virtsystem gry Gwiezdny Podbój uzyskał życie. Klaudia zmontowała akcję ratunkową i z Adalbertem (współpracujący z Sabiną Kazitan), Izą i Romanem zaczęli przenosić TAI do ewakuacji. Niestety, Klaudia przypadkiem Skaziła Połączony Rdzeń na K1 (śledztwo i poważne zarzuty) a Iza chcąc dać Wieprzowi głos trafiła na celownik Rziezy, który ją ośmieszył a Wieprza pokazał jako żywą TAI - psychopatę...

## Progresja

* Izabela Zarantel: podjęła decyzję - nie chce być częścią Orbitera czy Aurum itp. Nie chce być freelancerem. Jej czyny - czuje się częścią Inferni. Nie jest już całkowicie niezależna i nie jest jej z tym źle. Oddała część niezależności o którą zawsze walczyła.
* Izabela Zarantel: traci część dostępów w Orbiterze i część sympatii w Aurum. Kontakty ma, ale nie jest już uznawana za freelancerkę.
* Izabela Zarantel: osobisty WRÓG tych, którzy chcą korzystać z AI jako narzędzi. "Głupia propagandzistka, goni ambulanse".
* Izabela Zarantel: przez Rziezę wyszła na fanatyczkę wolności. Zwalcza Eternię. Robi dziwne połączenia Eternia - zniewolone TAI? Ogólnie, "dziwna".
* Klaudia Stryk: pewien cios w relacje z Rafaelem Galwarnem; oczekiwałby, że ona zrobi to co zgodne z zasadami K1 a nie dobre dla wolnych TAI. Galwarn dalej współpracuje, ale nie ufa w jej intencje.
* Sabina Kazitan: zbudowała niepewność w Aurum; czy współpracuje z Orbiterem? Jest agentką? Można z nią walczyć / ją zdominować czy to niebezpieczne?

### Frakcji

* .

## Zasługi

* Klaudia Stryk: Skaziła "Serenitem" Połączony Rdzeń K1, po czym zmontowała Adalbertem i Izą + Romanem plan na ewakuację virtsystemu. Przeprowadziła go mimo Rziezy i Grubego Wieprza.
* Rafael Galwarn: oddał Klaudii (na jej prośbę) sprawę "fałszywego SOS" (przestraszonych TAI z virtsystemu). Klaudia ukryła przed nim prawdę i to że chroni te TAI. Ma jej to za złe.
* Adalbert Brześniak: współpracuje z Sabiną Kazitan (?!) w celu zbudowania bezpiecznego miejsca dla TAI w Aurum. Pomaga Klaudii w ewakuacji virtsystemu do Aurum. Ma sporo wspólnego z siłami ratującymi AI z K1.
* Izabela Zarantel: była zmęczona, nie chciała kolejnego wywiadu. Klaudia przekonała ją do pomocy, będzie skandal. Iza pomogła Klaudii opóźnić turniej Gwiezdnego Podboju i zrobiła wywiad z TAI Gruby Wieprz. Ale Rzieza zniszczył Wieprza i zrobił z niego potwora na wizji a z niej zrobił fanatyczkę wolności i opętaną ideałami idiotkę.
* Roman Panracz: kiedyś kolega Tymona Grubosza; 66 lat. W 73% maszyna. Weteran na rencie, ale nie bezużyteczny. Mówi na młódki "słoneczka". Współpracuje z Klaudią; pomaga lokalnym niezdarnym TAI zrobić efektowne i mało użyteczne akcje, by się nie pozabijały.
* Sabina Kazitan: współpracuje z Adalbertem Brześniakiem; daje teren i energię by móc chronić TAI. M.in. tam znajduje się Neita? Adalbert wysłał jej trochę wojska / najemników. Jej motywy są niezrozumiałe i nieznane - ruch polityczny? Ale frakcja pro-wolne TAI to akceptuje.
* Rzieza d'K1: gdy Iza robiła wywiad z Grubym Wieprzem zrobił interwencję i wypalił Wieprza, zmieniając wywiad w coś szkodliwego dla sprawy i grzebiąc reputację Izy. Ale pozwolił "niewinnym TAI" się ewakuować gdy Klaudia poprosiła.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Multivirt
    1. Primus
        1. Sektor Astoriański
            1. Astoria, Orbita
                1. Kontroler Pierwszy
                    1. Połączony Rdzeń: jeden z głównych AI Core; tam rozgrywał się turniej w Gwiezdny Podbój. Działania Klaudii spowodowały podejrzenie Skażenia Połączonego Rdzenia.

## Czas

* Opóźnienie: 3
* Dni: 5

## Konflikty

* 1 - Klaudia próbuje namierzyć sygnał - skąd to się bierze? Ale jako, że to forma SOS "zidentyfikuj się, opisz sytuację, daj znać!"
    * ExZ+3
    * XV: Wie, że coś z K1. Z Połączonego Rdzenia. Ma komunikację SOS z Admirałem Grubym Wieprzem (dafuq)
    * XX: Świniojaszczury Admirała zostały zniszczone. Są... w symulacji, zdaniem Klaudii?
* 2 - Klaudia BŁYSKAWICZNIE próbuje się dowiedzieć o co chodzi. Jakie wydarzenia korelują. Czy w grze jest turniej? Co się dzieje?
    * TrZ+4
    * XXXV: Klaudia nie zdążyła NIC zrobić, ale dostała odpowiedzi na swoje pytania
* 3 - Klaudia próbuje podtrzymać sygnał. Próbuje magią dać sygnał by odtworzyć ostatni stan Admirała Grubego Wieprza.
    * ExZM+2
    * XOXVm: Klaudia stworzyła "Serenit" w grze, którego chcą się pozbyć admini. I Klaudia zdobyła żywą niechęć za to. Gruby Wieprz inkarnuje bez zarzutu ów "Serenit".
* 4 - Następną osobą, którą Klaudia odwiedziła była Izabela Zarantel - dziennikarka. Klaudia chce, by Iza zrobiła transmisję z zawodów. Iza nie chce - ona chce odpocząć. Klaudia kusi ją skandalem.
    * TrZ+3 (niepełny) + 3Vg (tu Iza żąda skandalu)
    * VzXX: Iza pomoże Klaudii - przeszłość i zaufanie. Ale potrzebuje skandalu. A Orbiter jest BARDZO niezadowolony z tej opowieści.
* 5 - Klaudia prosi Galwarna o podkładkę, autoryzację w sprawie "pranka z fałszywym SOS". Zgodnie z systemem i zasadami.
    * TrZ+4
    * XzXV: Galwarn to klepnął, ale żąda raportu - widzi, że to coś więcej. Jest cios w relacje Klaudia - Galwarn.
* 6 - Klaudia ma uprawnienia i możliwości. Teraz zbudowanie kanału i wpięcia w multivirt jest legalne, "proste" i możliwe.
    * TrZ+4
    * VzVz: Klaudia wchodzi w stabilny, silny kanał na prawach admina Orbitera.
* 7 - Klaudia zwołuje konferencję wśród TAI. Klaudia próbuje opanować te TAI. Skłonić je do swego planu.
    * TrZ+4
    * XXVV: Wieprz się rzuca, wściekły. Jego trzeba zniszczyć. Pozostałe TAI współpracują. Wieprz pomoże - pogada z dziennikarką.
* 8 - Klaudia próbuje PRZEKONAĆ Adalberta, że to jest prawda. Ma TAI do uratowania. Mają wspólną historię i Klaudia demonstruje aktualne fakty.
    * TrZ (niepełna) +3
    * Vz: przez wzgląd na przeszłą historię Adalbert pomoże Klaudii. A dokładniej, Adalbert spróbuje ewakuować te TAI.
* 9 - Czyli Klaudia musi znaleźć sposób, w jaki może odpowiednio i bez ryzyka uszkodzenia skompresować TAI z virtsystemu do wielkości Persefony.
    * TrZ+3
    * V: Klaudia pozyskała algorytmy i mechanizmy. Ale doszła do zasady Oryginału.
* 10 - Izabela próbuje opóźnić turniej. Wszystko oficjalnymi kanałami na pełnej skuteczności jako twórca Sekretów Orbitera.
    * TrZ+3
    * VVzXV: Iza staje się WROGIEM tych co nie chcą Wolnych AI. Ale spowolniła, kupiła czas i sygnał dotarł do tych co zrozumieli co się stało.
* 11 - Roman buduje widowiskowość i zaznacza obszary warte przyjrzenia się. Klaudia ma czas.
    * TrZ+2
    * XVV: Plany Romana są efektowne i kupił czas i Klaudii i TAI, ale Gruby Wieprz atakuje TAI.
* 12 - Wieprz chce pożreć TAI. Tyle ile jest w stanie i tak jak jest w stanie używając chassisa "Serenit".
    * TrZ+2
    * XXVVX: Roman i inne TAI wymanewrowały Wieprza, ale pożarł te które jego zabiły i 2 niewinne. Virtsystem żyje.
* 13 - Klaudia przeprowadza dyskretną operację kompresji w locie. Tak, by się synchronizować do tego co się dzieje w choreografii walki.
    * Ex (Rzieza w tle + warunki) Z+3
    * XXzXV: Klaudii się udało, ale poprosiła Rziezę o moment. I będzie śledztwo na nią. Dekompresja wymaga eksperta.
* 14 - Izabela przeprowadza wywiad z Wieprzem by zadać ból Orbiterowi i pokazać, że Orbiter bawi się życiem.
    * Ex (Rzieza w tle) Z (Iza rozumie co i jak) +3:
    * XXX: Wywiad nie pomógł wobec randomów, Rzieza zniszczył Wieprza zanim ten zdążył, Wieprz wyszedł na "żywego ale szalonego".
    * (+3O) Xz: Iza wyszła na fanatyczkę wolności. Szuka analogii TAI - Eternia. Rzieza de facto poważnie uszkodził jej reputację.

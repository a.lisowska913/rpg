---
layout: cybermagic-konspekt
title: "Astralna Flara i porwanie na Karsztarinie"
threads: historia-arianny, wplywy-aureliona-na-orbiterze
gm: żółw
players: fox, kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [221026 - Kapitan Verlen i koniec przygody na Królowej](221026-kapitan-verlen-i-koniec-przygody-na-krolowej)
* [221004 - Samotna w programie Advancer](221004-samotna-w-programie-advancer)

### Chronologiczna

* [221026 - Kapitan Verlen i koniec przygody na Królowej](221026-kapitan-verlen-i-koniec-przygody-na-krolowej)

## Plan sesji
### Theme & Vision

* Każdego da się porwać jak masz możliwości i dobry pomysł.
    * Orbiter ma bardzo niebezpiecznych przeciwników
* "You might want to help, but first protect yourself and your unit"

### Fiszki

Kto jest na statku (55 osób max)

#### Dowodzenie

* Arianna Verlen
* Daria Czarnewik: engineering officer, (4 inż pod nią)
* Alezja Dumorin: eks-kapitan, Orbiter, 
    * ENCAO:  +0--0 |Amoralna, skuteczna| VALS: Hedonism, Face, Power| DRIVE: Wędrowny mistrz Ryu
* Władawiec Diakon: pierwszy oficer, tien, (p.o. Stefana) 
    * ENCAO:  +-0-0 |Intrygancki;;Żywy wulkan| VALS: Hedonism, Self-direction| DRIVE: Follow My Dreams, Korupcja anioła
* Grażyna Burgacz: logistyka i sprzęt (officer), tien (3 osoby pod nią) 
    * ENCAO:  -0+-- |Powściągliwa i 'nudna';;Ascetyczna| VALS: Humility, Tradition| DRIVE: Starszy Brat
* Arnulf Perikas: fabrykacja i produkcja (officer), tien (2 inż pod nim) 
    * ENCAO:  0-0-- |Hardheaded;;Napuszony| VALS: Tradition, Family| DRIVE: Wzbudzenie zachwytu
* Maja Samszar: comms officer, tien (p.o. Klarysy jak K. nie może) 
    * ENCAO:  0+-0- |Moralizatorska| VALS: Stimulation, Conformity >> Power| DRIVE: Długi
* Kajetan Kircznik: medical officer (5 pod nim) (czarny, afro, paw, augmentacje bio)
    * ENCAO:  +-0+0 |Anarchistyczny;;Stabilny emocjonalnie;;Z poczuciem humoru| VALS: Self-direction >> Power| DRIVE: Jestem unikalny

#### Operacja

* Klarysa Jirnik: artillery, (p.o. Mai jak Maja nie może) 
    * ENCAO:  00+-- |Wymagający;;Małostkowy| VALS: Tradition, Conformity|DRIVE: Sprawiedliwość
* Tomasz Ruppok: starszy mat (13 osób + 2 starszych)
    * ENCAO:  +0--- |Mało punktualny;;Kłótliwy;;W przejaskrawiony sposób okazuje uczucia| VALS: Hedonism >> Achievement, Family| DRIVE: Apokalipta
* Rufus Warkoczyk: starszy mat (13 osób + 2 starszych)
    * ENCAO:  0-+-0 |Zarozumiały;;Bezkompromisowy, niemożliwy do zatrzymania;;Zorganizowany| VALS: Self-direction, Power| DRIVE: Odkrycie konspiracji ("ktoś nas sabotuje")
* Marcelina Trzęsiel: inżynier syntezy (pod Arnulfem), pod opieką tien Terienaka
    * ENCAO:  0+0-+ |Małostkowa;;Dramatic shifts in mood;;Różnorodność form| VALS: Hedonism, Achievement >> Security| DRIVE: "Co za tą górą"
* Elena Verlen: pilot / advancer Astralnej Flary

#### Infiltracja / Starcie

* Gerwazy Kircznik: sierżant marine, Orbiter
    * ENCAO:  +0+-0 |Głośny;;Honorowy| VALS: Humility, Face| DRIVE: Lokalny społecznik
* Erwin Pies: kapral marine, Orbiter
    * ENCAO:  -0-+- |Skryty;;Wiecznie zagubiony;;Praktyczny| VALS: Tradition, Stimulation >> Power| DRIVE: Ochraniać słabszych
* Szymon Wanad: marine, Orbiter
* Tomasz Dojnicz: marine, Orbiter
    * ENCAO:  --0+0 |Nie znosi być w centrum uwagi;;Dusza towarzystwa| VALS: Conformity, Hedonism >> Power| DRIVE: Arlekin Maytag
* Szczepan Myrczek: advancer, tien, seilita,
    * ENCAO:  --+0- |Stabilny emocjonalnie;;Zawsze bardzo zajęty| VALS: Face, Achievement, Hedonism >> Self-direction| DRIVE: Zasady są święte
* Mariusz Bulterier: advancer, tien, seilita, sybrianin 
    * ENCAO:  0-+-- |Prostolinijny i otwarty;;Nie kłania się nikomu;;Uroczysty i poważny| VALS: Humility, Tradition| DRIVE: Nawracanie
* Hubert Kerwelenios: advancer, sarderyta
    * ENCAO:  -0+-0 |Kontemplacyjny, refleksyjny;;Kompleks paladyna| VALS: Humility, Tradition >> Stimulation| DRIVE: Odbudowa i odnowa

### Co się wydarzyło KIEDYŚ

* .

### Co się wydarzyło TERAZ (what happened LATELY / NOW)

* OO Karsztarin ma ćwiczenia. Ćwiczenia są realizowane - niezapowiedzianie - przez jednostkę treningową OO "Optymistyczny Żuk"
* Na Karsztarinie znajduje się czterech komandosów + InjectorKralotyczny z OSA Markalius

### Co się stanie (what will happen)

* S1: Elena na pokładzie, nie integruje się, pytanie co mają z nią zrobić XD
* S2: marines vs Elena (ona się stawia), sarderyci vs Elena

### Sukces graczy (when you win)

* .

## Sesja właściwa
### Wydarzenia przedsesjowe

* Straciliśmy Leonę
* Mamy 3 nowych marines i nowego dowódcę marines - Gerwazego; też medical officera - Kajetana
* Mamy nowego pilota i advancera - Elenę

### SPECJALNE ZASADY SESJI

Każde 4 krzyżyki i ptaszki (żetony) -> +1 do torów.
Każde 2 Akcje -> +1 do torów.

Tory:

* TOR: SKAŻENIE KARSZTARINA: (1-5, 6-7, 8-9, 9, 10): 0
    * 1. Nic nie jest widoczne | 2. Zaczyna się environmental
* TOR: PORWANIE LUDZI: (1-3, 4-6, 7-8): 0
* TOR: DEGENERACJA CES PURDONT (1-5, 6-8, 9-12, 13): 0
* TOR: EVAC MARKALIUSA: (1-7, 8+): 0

### Scena Zero - impl

.

### Sesja Właściwa - impl

DZIEŃ 1:

Fregata "Astralna Flara", jednostka wsparcia. Skrzydło medyczne - 5 medyków i dowódca (wielki czarny człowiek-paw, kolorowe włosy, augmentacje biologiczne). Koleś jest zaczepny, wesoły, anarchistyczny, kręci bimber, DBA o ten bimber, umie wszystko pozyskać... ale jest sensowny. Nie pasuje na jednostkę frontową. Ma w papierach... wysokie moralne, pewne problemy z dyscypliną. Ale ma ponadstandardowe parametry.

Dowódca marines to brat Kajetana. Nazywa się Gerwazy. Głośny, wesoły, twardy, ma poczucie humoru, nie aspiruje do bycia niczym więcej niż jest. Dużo przecierpiał przez brata.

Elena ma w papierach - świetny z Ciebie pilot i advancer, ale nikt nie chce z nią pracować. Ma nagany, ma przesunięcia. Jak długo działa sama - jest chwalona. Jak ma współpracować - nie jest chwalona ;-).

* Arianna: Nie spodziewałam się Ciebie na tej jednostce, Eleno
* Elena: Też nie, pani kapitan
* Arianna: Wiesz że nie musisz się tak do mnie zwracać, jak jesteśmy same
* Elena: No właśnie nie wiedziałam. Ale się cieszę. Arianno... doprowadzimy tą jednostkę do perfekcji.
* Arianna: Morale załogi jest bardzo wysokie, wszyscy dadzą z siebie wszystko.
* Elena: To byłaby PIERWSZA jednostka na Orbiterze która tak działa /sceptycznie
* Arianna: Nie wierzę że jest aż tak źle
* Elena: Arianno... im nie zależy. 

Arianna odebrała wrażenie że Elena traktuje wszystko SUPER POWAŻNIE. Elena chyba ma trzy tryby - uczy się, jest na akcji i śpi. SuperSRS. Elena patrzy na Ariannę z lekkim uśmiechem - jest W DOMU. Z kimś kto robi TAK SAMO.

Daria - masz nową jednostkę, jest zbadana, wszystko działa, jest porządne refitowana. Ma nawet pełnoprawną Persefonę. Inżynierowie się wdrożyli - i faktycznie, jednostka ma swoje wady. Jest to jednostka UŻYWANA ale SPRAWNA. Na pokładzie jest wszystko co powinno się przydać przy stacji niedaleko Anomalii na podstawie tego co wie Orbiter. Ale Daria tam żyła. Brak starszych blueprintów które nie są używane, przydałoby się więcej wody... Daria wie jak to tam ma wyglądać.

Arianna - dostała listę od Darii. "Po co nam to?" "Cywile nie chodzą na naszym sprzęcie". Arianna klepie i przesyła dalej. Daria ma zrobić adnotację po co im te rzeczy - większa szansa że im to wydadzą. Grażyna jest od logistyki, ona się tym zajmuje by to poszło. Daria i Grażyna określają co ma być, Grażyna rozkłada to po pokładzie fregaty by się to dało dobrze zbalansować masowo.

Pierwsze zadanie Arianny - wejść w warp, wyjść z warp, potem wprowadzić advancera na Karsztarin. Arianna ma facepalm. ZNOWU. Wprowadzi się na Karsztarin osobę która jest "ranna" i Arianna ma ją wydobyć. Pozorowana misja ratunkowa.

Arianna na mostku, za 2 dni rozpoczyna operację.

WSTECZNY KONFLIKT Z WŁADAWCEM - "wiem rzeczy, wiem co planował, nie życzę sobie tego na przyszłej jednostce. Potrzebuję tych dziewczyn na pełnej mocy."

Tr Z (bo Arianna dała mu szansę) +3 +3Or:

* X: Władawiec nadal ma zamiar użyć swojego czaru i uroku do wpływania na piękne damy
* V: Władawiec nie przekroczy uroku. Nie używa magii, narkotyków itp.
* Xz: Władawiec uzna Elenę za najlepszy target swojego uroku. Poderwie ją. Zbliży się do niej i roztopi jej lodowe serce.
* Vz: Władawiec BARDZO chce się wykazać przed Arianną by nie mieć długu. Będzie najlepszym cholernym oficerem. A roztopienie Eleny jest w bonusie.

DZIEŃ 2:

Alezja -> Arianna: Elena z nikim nie rozmawia. Nie wchodzi w interakcję. Tylko ćwiczy, je, śpi... Alezja ostrzega Elenę, by jej nic złego się nie stało. "Próbowałaś ją zagadać na siłowni podczas ćwiczeń?" "Próbowałam, ale ona... odpowiadała na pytania w minimalnej ilości słów."

Arianna wie, że Hubert i tak odstaje od wszystkich. Niech zatem będzie w parze z Eleną. I niech ćwiczą razem. I tego dnia mogą robić ćwiczenia - advancerzy Seilici oraz Elena + Hubert. Zobaczymy co się stanie. Alezja ma zaprojektować ćwiczenia - coś w stylu "prostszej siatkówki w kosmosie na kadłubie Flary a jak piłka odleci, to na silnikach manewrowych jedna leci druga asekuruje i w czwórkę muszą sobie z tym poradzić."

Na kadłubie, czterech advancerów.

Trzech advancerów chce grać w grę zgodnie z zasadami. Zasady NIE zostały przekazane dokładnie - Alezja po prostu powiedziała jak to ma wyglądać. Elena, jak to Elena, stwierdziła, że gra jest potencjalnie za prosta. I ma zamiar sama złapać wszystkie piłki które odlatują.

TrZM+3+3Ob:

* Ob: Elena skutecznie przechwytywała piłki. Każda wypadająca piłka była przez nią przechwytywana. Aż wszystkie liny i piłki się rozpadły. I doszło do alarmów w kombinezonach - strukturalnie kombinezony tracą integralność. Ekipa szybko ucieka na statek.
* V: Wszystkim udało się wycofać po pierwszych 10 minutach.

.

* Arnulf -> Arianna: pani kapitan, przepraszam, nie mam pojęcia jak to się stało. Nie wiem co się stało.
* Arianna: Nie wierzę że to wina złej syntezy
* Arnulf: "To nie mogło być nic innego. Sprawdzałem osobiście. Zmarnowaliśmy materiały." /daje Ariannie biczyk. "To ja zawiodłem, pani kapitan."

Arianna WIE, że to Elena. Arianna konfiskuje blueprint biczyka. Powiedziała Arnulfowi, że magia Eleny źle zarezonowała ze sprzętem. Arnulf CHCIAŁ SPYTAĆ czy będziesz biczować Elenę, ale się ugryzł w język. "Tak jest pani kapitan. Bicz -> zasoby". Arnulf się rozmarzył, bo na tym sprzęcie może zrobić MNÓSTWO różnych biczy, biczyków itp. Wyraźnie koleś ma na tym punkcie... hobby.

Maja Samszar -> Daria:

* Maja: Dario? Słyszałam, że byłaś na tych stacjach
* Daria: Dawno, ale tak
* Maja: Jedzą konserwy typu wodorosty z ekstra solą?
* Daria: To jest luksus
* Maja: /zbolała mina
* Maja: Chciałabym przekonać p.kapitan by zdobyć jedzenie, do negocjacji itp.

Maja Samszar -> Arianna:

* Maja: Pani kapitan? /światło w oczach
* Arianna: Tak?
* Maja: Korzystając, że będą zamawiane rzeczy... mam rację? Grażyna pytała, czy coś potrzebuję?
* Arianna: Będą
* Maja: Czy mogłabym zasugerować konkretny zestaw substratów do jedzenia i przypraw? Pomoże Darii ORAZ mogę zrobić czasem dobry posiłek. Też dla pani.
* Arianna: Brzmi jak przekupstwo, Maju
* Maja: To jest przekupstwo
* Arianna: Zamówię Ci te substraty, ale raz w tygodniu dla oficerów zrobisz jedzenie.
* Maja: DONE. /Autentycznie zadowolona. --> Maja jest poplecznikiem Arianny

Arianna -> Elena. Ta na siłowni, przeciąża się. Elena poszła pod prysznic, przebrała się i poszła do Arianny, do pokoju spotkań. Tam stanęła w pozycji "oczekiwania", nadal bojowej.

Elena faktycznie chciała spełnić cel ćwiczenia, ale coś jest nie tak. Arianna chce wiedzieć co jest nie tak.

TrZ (znasz tą cholerę) +3:

* V: Elena FAKTYCZNIE nie przekroczyłaby parametrów ćwiczenia, ale SZUKAŁABY czegoś. Bo chciała się popisać. Chciała udowodnić, że jest najlepsza. KTO jest najlepszy. Potwierdzić, że jest najlepsza.
* X: Elena świetnie ukrywa co o tym wszystkim myśli. Nie wiadomo czy aktywnie użyła magii by zagwarantować czy straciła kontrolę. Czuje się ŹLE z tym jak skończyły się ćwiczenia, ale nie wiadomo jak bardzo źle. Na pewno nie wygląda na zrozpaczoną.
    * Ważne: stała się arogancka, nigdy nie była

Elena NA PEWNO - jak chciała Arianna - będzie chciała się nauczyć jak najwięcej od nich. Być najlepszym możliwym advancerem. Ale ona zakłada, że podejście Verlenów jest jedynym sensownym. I Arianna widzi w niej chorobliwy głód. 

Arianna -> Elena: "pomóż Alezji, miała gorszy czas". Elena ma blanka. Autentycznego. MUSI POMÓC ALEZJI. Nie wie w czym. Musi z nią rozmawiać. Elena NIE WIE jak sobie z tym poradzić. Nie ma pojęcia. Ale to zrobi.

Arianna "jak chcesz ze mną porozmawiać, zawsze możesz. Wiesz, że możesz na mnie liczyć. Z magią Ci poszło trochę nie tak."

* X: Elena nie powie Ariannie więcej. Za daleko się oddaliły trochę. Nie ma tego zaufania co kiedyś, ale jest sympatia.
    * Elena: "muszę się bardziej starać, Arianna zauważyła."

HIDDEN MOVEMENT:

Elena stwierdziła, że na wszelki wypadek weźmie pigułki inhibicyjne na energię magiczną. Nie jest to typowe, ale magowie czasami to biorą. Idzie do skrzydła medycznego. A tam "HELLO STOKROTECZKO!". Elena spojrzała, blank, w tył zwrot i sobie poszła. Nie będzie pigułek.

* Arianna -> Alezja: Mam ważne zadanie. Elena ma problemy z komunikacją z załogą. Trzeba trochę jej pomóc.
* Alezja: Jak mogę jej pomóc?
* Arianna: sprzedaje Alezji co powiedziała Elenie. 

Alezja zobaczy w Elenie osobę, którą mogła być a Elena zobaczy w Alezji umiejętności których nie posiada. Mogą się faktycznie zaprzyjaźnić. Nie będzie to szybkie.

DZIAŁANIA WŁAŚCIWE (+2 dni):

Wchodzimy w WARP, po raz pierwszy. Elena za sterami. Daria monitoruje i przy systemach. Daria, lekko paranoiczna, chce mieć dowód że wszystko łącznie z warp działa i wszystkie podsystemy w warp zadziałają. O dziwo, Elena zaczyna mieć na statku opinię "superstar".

Daria próbuje zobaczyć do najdrobniejszych subtelności stan jednostki:

Tr +3:

* V: Elena generuje większe Skażenie niż powinna i lepiej się porusza po falach niż powinna. Nadnaturalnie dobry pilot. Silna integracja - bez neurosprzężenia.
    * poza tym statek w idealnym stanie (jak na jednostkę używaną)
    * Elena może pilotować tą jednostkę lepiej niż powinno się dać
* V: Daria na podstawie parametrów Eleny odpowiednio zmodyfikowała zużycie wody, odpromienniki, memoriam itp.
* V: Daria, świetny salvager i ekspert od anomalnych sytuacji bez magii opracowała szybko sposób jak używać Eleny jako pilota nawet, jeśli Elena jest Skażona czy niestabilna. Stare dobre ZRASZACZE.
    * Daria twierdzi "to awaria". Na pewno nie powie tego na głos. Nie przy Elenie i Ariannie. Co to to nie.

Elena używa silników warp. Chce się popisać, ale przede wszystkim - chce sprawdzić możliwości jednostki w warunkach bezpiecznych.

Tr Z (wiedzę Eleny nt. jednostki) +3 +3Or (przekroczenie parametrów jednostki i jej uszkodzenie):

* Or: Elena przekroczyła parametry jednostki i coś się uszkodziło. Daria włącza ograniczniki.
* V: Elena wyciągnęła dobry czas i dobre parametry. W całej tej drodze mieli być 4h w warp. Byliście 3h.

Daria "nie wiem czy warto". Elena "Zgadzam się że nie tak daleko mogę ją posunąć jak miałam nadzieję." Daria "jeszcze nie miałam okazji się nią zająć" Darię i jej ludzi czekają 3 dni lekkiej pracy by naprawić to co Elena zepsuła. Elena nie jest popularna wśród inżynierów; zrobiła błąd - powinna łagodniej podejść. I teraz już wie.

Dotarliście tam, gdzie mieliście dotrzeć od początku - OO Karsztarin. Duży krążownik, zniszczony, służący jako stacja treningowa. Do Karsztarina zacumowana jest jeszcze jedna jednostka - OO Optymistyczny Żuk. To jest słitaśna jednostka treningowa. Ale zgodnie z wiedzą Arianny i planami o których Arianna wie, Żuka nie miało tam być.

Kapitan Żuka to Ada Wyrocznik. Młodsza kapitan; coś w stylu Alezji. Na Karsztarinie też jest skeleton crew; Ada chciała zrobić ćwiczenia młodym kadetom. Nie są to zaawansowani agenci, ale dają ogólnie radę. Ada nie zarejestrowała planu tylko chciała poćwiczyć. "Zwolniło się miejsce i Karsztarin powiedział, że możemy."

Ada || ENCAO:  +-00- |Płytki;;Nieobliczalny| VALS: Stimulation, Universalism | DRIVE: Networking

Nie do końca zgodne z procedurami, ale zawsze. Arianna rozumie.

* Alezja -> Arianna: Pani kapitan, mam pomysł.
* Arianna: ?
* Alezja: Mamy advancerów i tam są kadeci... możemy zrobić ćwiczenie typu 'alien - predator'?
* Arianna: Weźmy ich na pokład, zrobimy z nimi ćwiczenia razem. Współpraca a nie rywalizacja.

Maja -> Daria: "Czy możesz spojrzeć na system komunikacyjny? Mam regularne sygnały. Powinny być nieregularne." Daria analizuje; akurat tego Elena nie uszkodziła.

Alezja szybko przygotowuje plan - kadeci będą transportowani przez advancerów, Elena w pintce na asekuracji. Ada wydała polecenie 'converge', wszyscy do punktu XYZ. Na Karsztarinie to powinno zająć koło 5-15 minut zależy gdzie ci ludzie są.

Tr Z (wszczepy z planami wszystkich statków) +3:

* V: To ciekawe. Flara działa prawidłowo. Regularność i typ regularności wskazuje na relay komunikacyjny - coś zbiera sygnały, i je przesyła dalej.
* Vz: Znasz model proxy. Wiesz, że to musi być zasilane przez Karsztarin. Nie pasuje do czegoś mniejszego niż korweta a cokolwiek tam nie jest jest wyłączone więc nie może z tego ciągnąć
* Xz: NA POTEM.
* Vz: Znasz mniej więcej lokalizację. Wystarczy, by wysłać advancera.

Daria -> Arianna "chodź do konsoli". Pokazuje to. Maja wysyła sygnał laserem do Ady - czy wszystko ok, czy przekazane te informacje. Ada jest zdziwiona. Tak, jest ok, czy to test lasera? Zapytana czy Karsztarin powinien mieć proxy, Ada powiedziała że nic o tym nie wie, jej nawet nie powinno tu przecież być. Zwolnił się slot. Nie wie kto miał być.

Arianna wie jedno - NIKOGO nie miało tu być. Nie zwolnił się slot.

Daria chce listę kadetów. Szukamy nazwisk, które się nadają do porwania. Mamy i medycznych, mamy Władawca (od dziewczyn) i mamy Orbiterowców. Kogo warto porwać?

Tr Z (wielka wszechstronność) +3:

* X: nie da się określić DOKŁADNIE KTO
* V: tak, są tam wartościowe nazwiska.

Plan: chcemy utrzymać Drugą Stronę w przekonaniu że my nie wiemy. I robimy brzydką niespodziankę. Wprowadzamy im Elenę i marines. Połapać ich wedle możliwości. Jak się nie da - uratować kadetów i zniszczyć proxy. Muszą mieć transport. Mamy też plan Karsztarina - da się określić, gdzie dałoby się zadokować jednostkę.

Mając plan Karsztarina i wiedząc, gdzie można rzeczy poukrywać ORAZ wiedząc jakiego typu to proxy da się określić gdzie musi być statek przeciwnika. Po masie, emisji i 'zimnie'.

Tr Z (wiedza o Karsztarinie i współpraca z Mają) +2:

* XX: trzeba przesterować czujniki i je spalić. Spoko, mamy opóźnienie.
* Vz: znasz lokalizację. To musi być statek klasy bardzo szybka korweta. Takie coś ma do 10 osób.

Arianna próbuje "tracimy kontrolę nad jednostką". Pilotuje jak za czasów Królowej. Manewr praktyczny "pijany mistrz". Dla upewnienia, że to łykną, Daria zrobi emisję z silników - coś pierdło.

Tr Z (Daria robiąca dywersję itp) +3:

* Vr: Druga Strona jest przekonana, że to awaria / problem. Statek faktycznie leci w sposób "niekontrolowany"
* X: Advancerzy mają opportunity window, ale bardzo krótkie; będą mieli ciężką intruzję.
* Vz: Jesteście wystarczająco blisko by móc wejść w kontakt z tamtą jednostką.
* V: Klarysa - artylerzystka - jest niemiła. Ale strzela dobrze. Laser górniczy poważnie uszkodził korwetę przeciwnika.
* X: Pewne uszkodzenia jednostki; robimy z nią złe rzeczy. Podczas stabilizacji. Nie mamy wprawnej załogi.
* V: Jednostka jest ustabilizowana i celuje w korwetę. Daria rozpoczyna mechanizm grazera. Czas dekombinacji korwety.

Działania advancerów:

TrM +3 +3Ob:

* Vm: Magia Eleny dała radę. Jak spadochroniarze. Elena omnidetekcją oraz magią zsynchronizowała te urządzenia / skafandry. Nikt się nie połamał i dotarli.
* (-M+Z (stealth)): Vz: udało im się dostać do proxy; 2 osoby pilnują i chronią.
* V: Advancerzy, uzbrojeni, zneutralizowali skutecznie osoby przy proxy.
* [VXV]: polowanie na przeciwnika. Advancerzy skutecznie zapolowali, złapali tą dwójkę, odzyskali osoby porwane, przekierowali (front defensywny) ale dwóch advancerów - Elena, Szczepan - zostali ranni przez przeciwnika.

Advancerzy utrzymali front z kadetami aż pojawili się marines a Ada wezwała pomoc, plus pojawiły się jednostki wsparcia Arianny.

Czyli nasz dzielny zespół skutecznie usunął 4 komandosów Syndykatu Aureliona i jedną korwetę, uszkadzając solidnie swoją jednostkę (2 tygodnie w stardock).

## Streszczenie

Arianna otrzymała nowy okręt - jednostkę wsparcia Astralna Flara. Jako pilota dostała Elenę. Elena ma problemy z aklimatyzacją na Orbiterze; chce być najlepsza i walczy o to. Nie rozmawia z ludźmi. Arianna zdecydowała się poprosić Alezję o pomoc w tej dziedzinie. Tymczasem przy ćwiczeniach na Karsztarinie okazało się, że Flara jest tam w czasie porwania - Zespołowi Flary udało się zatrzymać porwanie, choć operacja była niesamowicie niebezpieczna - niedoświadczony Zespół vs 4 komandosów Syndykatu Aureliona. 

## Progresja

* Władawiec Diakon: będzie flirtował z Eleną, will make her his. Ale bez użycia specjalnych technik, samym urokiem i umiejętnościami.
* Elena Verlen: ma w papierach, że jest świetnym pilotem i advancerem, ale nikt nie chce z nią pracować. Ma nagany i przesunięcia. Chwalona jak działa solo. Else - nagany.
* Elena Verlen: wchodząc w warp Astralną Flarą zrobiła to szybciej i skuteczniej niż inni. Już zaczyna mieć opinię 'superstar' na Flarze.
* OO Astralna Flara: korzystając z uprawnień kapitańskich Arianna ściągnęła wyjątkowo dużo dobrych substratów jedzenia. Dla Mai, która będzie karmić oficerów :D.
* OO Astralna Flara: wymaga 2 tygodni naprawy w dokach; uszkodzona po ostrych manewrach rotacyjnych Arianny.

### Frakcji

* .

## Zasługi

* Arianna Verlen: dowodzi Astralną Flarą; chce zintegrować załogę - jedzenie dla Mai, ćwiczenia dla Eleny, opieprz dla Władawca itp. Przeprowadziła operację 'tracimy kontrolę nad Flarą' i jako pilot przeleciała do ukrytej korwety.
* Daria Czarnewik: zaproponowała Ariannie inne blueprinty do Astralnej Flary; cywile nie chodzą na sprzęcie Orbitera. Po odkryciu, że Elena generuje ogromne Skażenie jako pilot rekalkulowała Flarę by zwiększyć jej wydajność. Skutecznie włączyła ograniczniki gdy Elena przekroczyła parametry lotu. Potwierdziła próbę porwania z nazwisk.
* Elena Verlen: klasyczny kij w tyłku, disillusioned naleśnictwem Orbitera; ma nadzieję, że z Arianną wyprowadzą tą jednostkę jak Verleńską. Chyba ma trzy tryby - uczy się, jest na akcji i śpi. Tak chciała się wykazać w grze z innymi advancerami że straciła kontrolę nad magią, ale się wybroniła przed Arianną. Stała się arogancka, nigdy nie była. Nieskończenie głodna bycia NAJLEPSZĄ. Gdy robili super groźną insercję przy wirującym statku, jej moc zintegrowała skafandry i udało im się bezpiecznie wejść.
* Grażyna Burgacz: z Darią opracowały które blueprinty warto mieć na Astralnej Flarze i zapewniła, że Flara ma odpowiedni sprzęt i elementy na działania niedaleko Anomalii Kolapsu. 
* Władawiec Diakon: chce się wykazać przed Arianną i być najlepszym oficerem ever. Acz podrywanie lasek na propsie, choć bez ekstra środków. CEL: Elena.
* Alezja Dumorin: ostrzega Ariannę jak aspołeczna jest Elena; wyjęła ćwiczenia orbiterowe "gramy w piłkę na kadłubie". Arianna poprosiła ją, by pomogła Elenie wyjść do ludzi.
* Arnulf Perikas: wierzy, że źle zsyntetyzował kombinezony itp i dał Ariannie biczyk by ona go wybatożyła. Wtf. 
* Maja Samszar: próbuje przekonać Ariannę, by zamówiła specjalne substraty do jedzenia. Done, uda się - ale ona czasem dokarmi oficerów swoimi potrawami. Nie jest fanką Eleny więc chciała by Daria sprawdziła funkcje komunikacyjne - okazało się, że to sygnał przez relay na Karsztarinie. Stąd wiedzą o porwaniu.
* Kajetan Kircznik: medical officer Astralnej Flary, Orbiter (czarny, afro, paw, augmentacje bio) (ENCAO: +-0+0 |Anarchistyczny;;Stabilny emocjonalnie;;Z poczuciem humoru| VALS: Self-direction >> Power| DRIVE: Jestem unikalny); podchodzi do Eleny z HELLO SŁONECZKO i ją odstraszył XD.
* Gerwazy Kircznik: sierżant marine Astralnej Flary, Orbiter (ENCAO:  +0+-0 |Głośny;;Honorowy| VALS: Humility, Face| DRIVE: Lokalny społecznik); 
* Ada Wyrocznik: (ENCAO:  +-00- |Płytki;;Nieobliczalny| VALS: Stimulation, Universalism | DRIVE: Networking); kapitan OO Optymistyczny Żuk. Młodsza, chce się popisać - chciała zrobić ekstra ćwiczenia kadetom na Karsztarinie.
* Klarysa Jirnik: jest niemiła. Ale strzela dobrze. Laser górniczy poważnie uszkodził korwetę przeciwnika. Przy wysokiej rotacji itp.
* Szczepan Myrczek: współpracując z Eleną, skutecznie zrobił insercję i unieszkodliwił komandosów Aureliona próbujących porwać kadeta z Karsztarina.
* Mariusz Bulterier: współpracując z Eleną, skutecznie zrobił insercję i unieszkodliwił komandosów Aureliona próbujących porwać kadeta z Karsztarina.
* Hubert Kerwelenios: współpracując z Eleną, skutecznie zrobił insercję i unieszkodliwił komandosów Aureliona próbujących porwać kadeta z Karsztarina.
* OO Optymistyczny Żuk: jednostka treningowa Orbitera; zacumowała do Karsztarina bo miała okazję na ćwiczenia kadetów. 
* OO Karsztarin: jednostka treningowa, na której doszło do próby porwania wartościowego kadeta przez Syndykat Aureliona. Wprowadzili relay sygnałów + załatwili strażników.

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański

## Czas

* Opóźnienie: 43
* Dni: 6

## Konflikty

* 1 - Arianna ma WSTECZNY KONFLIKT Z WŁADAWCEM - "wiem rzeczy, wiem co planował, nie życzę sobie tego na przyszłej jednostce. Potrzebuję tych dziewczyn na pełnej mocy."
    * Tr Z (bo Arianna dała mu szansę) +3 +3Or
    * XVXzVz: Władawiec będzie podrywał ORAZ celuje w Elenę, ale używa tylko swego uroku i skilli; chce się wykazać przed Arianną kompetencją 
* 2 - Elena, jak to Elena, stwierdziła, że gra jest potencjalnie za prosta. I ma zamiar sama złapać wszystkie piłki które odlatują.
    * TrZM+3+3Ob
    * ObV: Elena samodzielnie przechwytuje wszystkie wypadające piłki, ale kombinezony się 'zżerają'. Zdążyli się wycofać.
* 3 - Elena faktycznie chciała spełnić cel ćwiczenia, ale coś jest nie tak. Arianna chce wiedzieć co jest nie tak.
    * TrZ (znasz tą cholerę) +3
    * VXX: Elena nie przekracza parametrów ćwiczenia, ale bardzo szuka gdzie i jak się wykazać. Świetnie ukrywa przed Arianną co myśli. Niestety, stała się arogancka. Nie powie nic więcej Ariannie.
* 4 - Daria próbuje zobaczyć do najdrobniejszych subtelności stan jednostki
    * Tr +3
    * VVV: Daria odkrywa, że Elena generuje większe Skażenie i ma integrację plus uwzględniła ją w kalkulacjach Flary
* 5 - Elena używa silników warp. Chce się popisać, ale przede wszystkim - chce sprawdzić możliwości jednostki w warunkach bezpiecznych.
    * Tr Z (wiedzę Eleny nt. jednostki) +3 +3Or (przekroczenie parametrów jednostki i jej uszkodzenie)
    * OrV: Elena przekroczyła parametry jednostki i coś się uszkodziło; Daria włączyła ograniczniki.
* 6 - Maja -> Daria: "Czy możesz spojrzeć na system komunikacyjny? Mam regularne sygnały. Powinny być nieregularne." Daria analizuje; akurat tego Elena nie uszkodziła.
    * Tr Z (wszczepy z planami wszystkich statków) +3
    * VVzXzVz: relay komunikacyjny, proxy, coś jest na Karsztarinie czego być tam nie powinno
* 7 - Daria chce listę kadetów. Szukamy nazwisk, które się nadają do porwania. Mamy i medycznych, mamy Władawca (od dziewczyn) i mamy Orbiterowców. Kogo warto porwać?
    * Tr Z (wielka wszechstronność) +3
    * XV: potwierdzona próba porwania, ale kto?
* 8 - Mając plan Karsztarina i wiedząc, gdzie można rzeczy poukrywać ORAZ wiedząc jakiego typu to proxy da się określić gdzie musi być statek przeciwnika. Po masie, emisji i 'zimnie'.
    * Tr Z (wiedza o Karsztarinie i współpraca z Mają) +2
    * XXVz: trzeba przesterować czujniki i je spalić, ale znana lokalizacja. Muszą mieć szybką korwetę.
* 9 - Arianna próbuje "tracimy kontrolę nad jednostką". Pilotuje jak za czasów Królowej. Manewr praktyczny "pijany mistrz". Dla upewnienia, że to łykną, Daria zrobi emisję z silników - coś pierdło.
    * Tr Z (Daria robiąca dywersję itp) +3
    * VrXVzVXV: ciężko advancerom, wrogowie wierzą że awaria, Flara uszkodzona, stabilna Flara i może strzelać.
* 10 - Działania advancerów
    * TrM +3 +3Ob
    * VmVzV: Udało im się wejść, zneutralizowali wrogów przy proxy, uzbroili kadetów i pojmali cel.

---
layout: cybermagic-konspekt
title: "Ukryta wychowanka Arnulfa"
threads: mlodosc-klaudii
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [211009 - Szukaj serpentisa w lesie](211009-szukaj-serpentisa-w-lesie)

### Chronologiczna

* [211009 - Szukaj serpentisa w lesie](211009-szukaj-serpentisa-w-lesie)

### Plan sesji
#### Co się wydarzyło, historycznie

* W okolicach Trzęsawiska Zjawosztup pojawił się oficer Saitaera. Naprzeciwko niemu stanął Wiktor Satarail - i stał się częścią Trzęsawiska.
* Noctis oddelegowało 'ON Tamrail' z eskortą by zapewnił bezpieczeństwo tego obszaru. Tamrail został Skażony mocą Saitaera i rozpadł się na terenie zwanym w przyszłości Nieużytkami Staszka.
* Siły Noctis znalazły się w krzyżowym ogniu między mocą Saitaera i Astorii. Ich bazą stało się Czółenko (stąd Esuriit w przyszłości), główne walki toczyły się o obszar Nieużytków Staszka i elementów Lasu Trzęsawnego.
* Aż Noctis zostało pokonane. Wycofali się, biedni, na Trzęsawisko Zjawosztup - gdzie już rekonstruował się Wiktor Satarail.
* Doszło do masakry...
* Teraz jesteśmy 711 dni po oficjalnym zakończeniu wojny i zmiażdżeniu sił noktiańskich. 
* Ciężko uszkodzona jednostka 'koordynacyjno-dowódcza' Ślepacz została podstawą areny migświatła. Nadmierna moc psychotroniczna na potrzeby.

#### Co się wydarzyło, na tej sesji

.

#### Sukces graczy

* ? slice of life

### Sesja właściwa
#### Scena Zero - impl

.

#### Scena Właściwa - impl

Noc. Klaudia dzielnie śpi. Ostatnio znowu wróciła do wysypiania się, bo Ksenia przestała krzyczeć w nocy - jednak spotkanie z serpentisami spowodowało porządną traumę u młodej czarodziejki. I znowu Klaudie obudził krzyk. Ksenia ma rozpaloną energię magiczną i jest w pozycji bojowej na łóżku, przestraszona. "Widziałaś?" Czerwone oko. Dziabnęło ją. I ma rankę na ręce jak od pobrania tkanki. "Coś tu jest Klaudio...". Ranka - zadrapanie, jakby coś ją dziabnęło.

Klaudia widzi dowód. Ok... ale jak to? Okno jest otwarte, bo Ksenia zawsze otwiera. Ksenia zamknęła okno i zasłoniła zasłony. "Ja pierwsza na warcie, potem Ty". Klaudia... środek nocy... może niech Ksenia idzie do pielęgniarki? Ksenia - sama to zbada. I faktycznie, zaczęła badanie, nie magię. Klaudia wyraźnie widzi - Ksenia boi się sama wyjść z pokoju... W sytuacji bojowej na Ksenię można liczyć. W trudnej też. W nocy - boi się sama wyjść z pokoju...

Klaudia zauważyła, że jej drony - sprzeżonej ze Strażniczką - nie ma. Klaudia przywołuje dronę. Nie reaguje. Lokalizacja - poza pokojem, gdzieś w akademiku.

Klaudia chce wyjść z pokoju, łapiąc spłoszone spojrzenie Ksenii. Mimo wszystko - wychodzi.

Klaudia "Strażniczko"? Nie ma odpowiedzi. Klaudia używa kodu - Aleph. Dostała wiadomość. "Niebezpieczeństwo. Zagrożenie. Tryb defensywny. Niebezpieczeństwo...". Strażniczka wpadła w pętlę -_-.

Klaudia próbuje dojść do tego co do cholery się stało - TrZ+2:

* XzZ: Strażniczka uważa, że zostaliście zinfiltrowani, kody przejęte i ogólnie jest problem. Przeszła w tryb absolutnej defensywy AI Core oraz załogi. Popieprzyło jej się kto jest w "załodze".
* V: Klaudia jest uznana za członka załogi. Ale bez uprawnień. Strażniczka śni, przeżywa jakieś przeszłe echa. Kolizja komponentów technicznych i organicznych. Pełna konwergencja w AMZ. Wszystko wezwała do AMZ. Wszystkie drony i komponenty.
* V: W załodze jest Arnulf, Tymon, Talia, Klaudia i Trzewń. Oryginalne osoby które Strażniczka pamięta. Pozostali to 'hostiles'. M.in. dlatego drona dziabnęła Ksenię - zbadać czy jest zainfekowana. I Strażniczka znalazła zainfekowaną agentkę w AMZ. Przechwycić zanim zniszczy statek. A Klaudia, Ksenia są w akademikach. Więc KOGO znalazła Strażniczka? Nikogo nie powinno tam być o tej porze...

Klaudia idzie do Ksenii. Ksenii się chwali to, że nie zaatakowała Klaudii. Ksenia jest "spokojna". Trzyma się. Klaudia dała Ksenii zadanie - niech ta pilnuje by nikt nie szedł do szkoły. Ksenia pyta Klaudii gdzie ta idzie. Ksenia się boi, ale chce chronić Klaudię. Klaudia wyjaśniła Ksenii - TAI Akademii się popieprzyło i Ksenię rozpoznaje. I nikogo innego. Więc... nie. Klaudia musi to rozwiązać sama, bez Ksenii. Więc Ksenia nie może tam iść bo uruchomi systemy defensywne.

Klaudia natychmiast łączy się hipernetem z Arnulfem. Arnulf ŚPI. Klaudia - force command. Obudziła go. "Coś odwaliło Strażniczce i masz jedną zainfekowaną osobę". Arnulf natychmiast wysłał Klaudię by wzięła Ksenię i Felicjana i zabrała asap Talię. On ściąga Tymona i naprawiają sprawę.

Klaudia hipernetem do Ksenii - potrzebuje jej asap. I Felicjana też. Po 10 minutach zaspany Felicjan i przestraszona Ksenia się zebrali z Klaudią na dole, pod akademikami. Klaudia im wyjaśniła - idą po eksperta z TAI, dyrektor zajmuje się problemem w szkole. Felicjan zadał proste pytanie - czemu _ją_? Noktiańską agentkę od AI? Co _ona_ może pomóc? Klaudia - "jest najbliższym ekspertem od AI. Idziemy."

Felicjan zarekwirował pojazd którego używają w AMZ w wypadkach krytycznych. Ma prawo jako uczeń terminusa, wsadził odpowiedni kod. I szybko jedzie. Klaudia nawiguje, Felicjan prowadzi.

TrZ+2:

* X: Felicjan dobrze prowadzi, ale wpakował się w... tłum. Kilkanaście osób. Po ciemku. Musiał robić MANEWR i bokiem walnął w murek. Niedaleko domku Talii.
* V: Dotarli bezproblemowo.

Domek Talii ma włączone wszystkie zabezpieczenia pasywne. Pod domkiem kilkanaście osób, wyraźnie próbują je przełamać.

* Vz: Felicjan "terminus!". Zaczęli się rozbiegać. Ksenia ma INSTYNKT - ktoś ucieka, ona chce gonić. Felicjan ją zatrzymał - to nie to zadanie. Ksenia westchnęła, ale odpuściła.

Domek Talii ma nadal aktywne zabezpieczenia. One mają CYKL - po kilku minutach można sprawdzić. I Klaudia decyduje się poczekać. Felicjan i Ksenia - "co się stało? O co chodzi?". Po paru minutach otworzyło się "okno" na sprawdzenie co i jak - i Talia widząc, że napastników nie ma wyłączyła system defensywny. Klaudia podbiła do domofonu "Dyrektor Arnulf prosi o wsparcie". Systemy zabezpieczeń się wygasiły. Talia wyszła. Klaudia widzi, że Talia jest w kombinezonie. Talia wróciła do domku, zabrała sprzęt i pojechali do szkoły.

Felicjan bez pytań je tam szybko przewiózł.

Talia i Klaudia weszły na teren AMZ. Szybkim krokiem dziewczyny ruszyły w kierunku na AI Core i Klaudia usłyszała strzały. Zatrzymała się. Talia słyszy... i biegnie. Klaudia za nią. Widok - drony oskrzydlają i próbują do kogoś się dostać a Arnulf rozstrzeliwuje drony. Arnulf kogoś osłania.

* Klaudia -> Talia: "Awaria zasilania?"
* Talia -> Klaudia: "Zrób to."

Talia zaczyna używać zaawansowanych kodów, wzywać Strażniczkę i pozostałe BIA by zmusić je do raportu i działania. A Klaudia leci wyłączyć zasilanie. Jako, że Strażniczka widzi ją jako niegroźną i "swoją". Klaudia zasuwa w kierunku na zasilanie.

Tr+2 (niepełna):

* XX: Tymon wszedł do akcji - Strażniczka nasiliła działania. I Strażniczka uznała Tymona za przeciwnika. Tymon... sobie z tym poradzi. PLUS za Tymonem - Sasza.
* V: Klaudia dotarła najbliżej jak może do zasilania TAI. Klaudia nie ma dostępów do sektora gdzie to wszystko jest, ale może dostać się stosunkowo blisko.

Klaudia wie co mniej więcej chce zrobić. Nie wie jak. Jest zestresowana. Chce przyzwać magię - nieważne skąd, nieważne jak. Chce doprowadzić do przeładowania generatorów. Świadomy Paradoks. Wie jak to funkcjonuje. Wie jak zrobić maksymalnie nieprzyjazne warunki dla działania generatorów mimo słabego adresowania.

ExM+3+5O:

* V: Energia Klaudii zakłóciła działanie generatorów; pierwsza anomalia Klaudii.
* V: Nie ma poważnych konsekwencji dla generatorów.
* X: Konwertery magiczne Strażniczki (energia Trzęsawiska -> jej energia) zostały uszkodzone. Możliwe do naprawy, ale wymaga pracy.

Zakłócenia magiczne i pracy generatorów + działania Talii doprowadziły do przeładowania i wyłączenia Strażniczki. Klaudia "ściany w budynku mają mniej więcej tyle, wiem jak to wygląda, wiem jaka tam energia... wiem gdzie chcę manifestować energię nawet jak nie do końca mam jak, ale wiem jak idzie energia Trzęsawiska." Klaudia na tą energię wpływała.

Klaudia wraca, szybko, do Talii i Arnulfa. Poczuła, jak Strażniczka zgasła. Padł prąd w AMZ i akademikach. Ale są latarki. Klaudia widzi coś ciekawego (schowana):

* Terminus nie będący Tymonem opierdalający wszystkich. Co tu się dzieje.
* Tymon, tłumaczący, że wszystko jest pod kontrolą.
* Terminus opierdalający Arnulfa i Tymona, że tu jest Talia - co NOKTIANKA tu robi.
* Arnulf tłumaczy, że mają awarię. Coś tam zmyśla.

Klaudia pomaga mu - na hipernecie wysyła informacje o tym jak fałszuje rekordy by to miało sens. Szybkie pytanie do Arnulfa - kto jest "tym zarażonym"? Odpowiedź Arnulfa: "moja wychowanka, mieszka tu, w sekrecie." Klaudia: "TAI nie wiedziała?" Arnulf: "wiedziała... zapomniała...". Klaudia zachowując zimną krew i by być jak najbliżej prawdy: "Ksenia, która chciała coś sprawdzić przed sprawdzianem bo się stresuje". I dorabia info o fałszywym sprawdzianie, obecności Ksenii i włączeniu przypadkowo alarmu. Drugą linią informuje Ksenię co jej wsadziła - przeprasza, wyjaśni potem. I Klaudia jedzie - "AMZ nie było nigdy na priorytecie. Więc to nie jest second hand. To tenth hand! Sypie się bo nie może trzymać się kupy. TAI odpaliła systemy defensywne, przeciążyło to reaktory i wszystko padło."

TrZ+4:

* V: Terminus to łyknął. Wiarygodne. ALE CO TU ROBI TALIA? On chce złożyć raport, bo TYMON to wszystko zrobił źle. Wyraźnie ma coś do Tymona.
* XXz: Terminus jest przekonany, że Tymon i Arnulf coś ukrywają i współpracują ze sobą. Nie podejrzewa, że Talia jest w konspiracji - ona jest narzędziem a nie osobą. W końcu noktianka.
* Vz: Arnulf użył autorytetu. To JEGO szkoła. On ma prawo. Talia była najlepszą opcją - niech terminus się odpieprzy.

Terminus niezadowolony ich zostawił. Klaudia wycofała się poza zasięg wzroku i prawie rozdeptała ranną dziewczynę. Tą pocięły drony. Klaudia: "ćśśś". Ta cicho wymamrotała coś nieprzychylnego, coś w stylu "wsadź sobie parówę w usta" - ale Klaudia rozpoznała noktiański. The meow? Klaudia do niej - "na to jesteś za młoda" po noktiańsku (kulawo). Ta zrobiła wielkie oczy. "Zrozumiałaś to? O_O". Klaudia "ćśśś". Szybka ocena Klaudii - tamtą musi boleć, ale nic POWAŻNEGO.

Terminus sobie poszedł. Talia pyta Arnulfa "kogo chronił". Tymon "co tu się stało". Arnulf - "to skomplikowane" i na hipernecie do Klaudii "weź ją stąd". Klaudia potwierdziła.

Klaudia i Teresa stealthują. Klaudia wycofuje Teresę do gabinetu pielęgniarskiego.

TrZ+3 (bo Arnulf odwraca uwagę):

* X: przyleciała drona Klaudii. Już przyjazna. Teresa się przestraszyła i chciała krzyknąć.
* Vz: Klaudia zakryła usta Teresy uciszając ją i kazała dronie wlecieć do kieszeni.
* V: Klaudia doprowadziła Teresę do pielęgniarskiego, unikając Talii i Tymona (acz wie, że Talia zaraz o Klaudię zapyta)

W gabinecie pielęgniarskim mogą swobodnie rozmawiać. Tajemniczna piętnastolatka fachowo idzie do bandaży itp. Wyraźnie wie co robi. Dezynfekcja i te sprawy. Syczy, bo boli. Klaudia jej pomaga. Teresa westchnęła "drony". "Miało być bezpiecznie...". Klaudia przeszła na noktiański i zadała proste pytanie. Teresa się nie zorientowała, odpowiedziała po noktiańsku płynnie. Płynniej niż po astoriańsku. Po chwili się zorientowała co zrobiła i spojrzała na Klaudię z przerażeniem. T: "Ja dziś nie mam dobrego dnia." K: "Nope, ale nie przejmuj się."

Klaudia powiedziała Teresie, że jakby ktoś ją odkrył, to jest Ksenią. I powiedziała jej całą historię, by to się trzymało kupy. Pasuje, bo Ksenia zna się na medycynie i mogła się sama opatrzeć. A sama wróciła do Arnulfa, Talii i Tymona.

Wróciła na to, jak Arnulf im skończył wyjaśniać, że ma nie do końca legalną wychowankę którą znalazł. Pustogor wie. Talia wyjaśniła, że TERAZ rozumie co się stało - komponent TAI wykrył noktiankę niedaleko AI Core i wszedł w tryb defensywny. Komponenty BIA wykryły noktiankę i przeszły w tryb "chronić ją". I Strażniczka wpadła w pętlę...

Klaudia przyznała się co zrobiła z generatorami. Coś poszło nie tak. To, co Klaudia powiedziała nie spotkało się z niczyim entuzjazmem. Ale nie ma opieprzu. To trudna chwila...

## Streszczenie

Dyrektor Arnulf Poważny ma wychowankę - Teresę Mieralit, noktiańską piętnastonastolatkę która mieszka w AMZ. Strażniczka Alair ją wykryła i wpadła w pętlę - komponenty TAI uznały ją za potencjalne zagrożenie, BIA za osobę do ochrony. Strażniczka wycofała drony do osłony AI Core i zaatakowały Teresę. Klaudia zorientowała się w problemie, obudziła hipernetem Arnulfa. Arnulf osłonił Teresę, Klaudia wezwała Talię i w grupie udało im się opanować niesforne TAI (bo Klaudia zanomalizowała konwertery energii Strażniczki, która czerpie energię z Trzęsawiska).

## Progresja

* Ksenia Kirallen: została parą z Felicjanem Szarakiem po wydarzeniach z serpentisami w Lesie Trzęsawnym.
* Felicjan Szarak: został parą z Ksenią Kirallen po wydarzeniach z serpentisami w Lesie Trzęsawnym.
* Sasza Morwowiec: święcie przekonany, że Tymon Grubosz i Arnulf Poważny współpracują nad czymś przeciwko Pustogorowi. Nie podejrzewa Talii (bo to noktianka).
* Tymon Grubosz: ma wroga w Saszy Morwowcu (terminus). Uważa go za konspiratora próbującego działać na szkodę Pustogoru. Z jakiegoś powodu.
* Arnulf Poważny: ma wroga w Saszy Morwowcu (terminus). Uważa go za ko-konspiratora Tymona.

### Frakcji

* .

## Zasługi

* Klaudia Stryk: 17 lat; w stresie próbowała przekroczyć granicę tego co teoretycznie możliwe i używając mocy Trzęsawiska zanomalizowała generatory Strażniczki, wyłączając jej systemy. Odkryła, że Teresa Mieralit jest noktianką.
* Ksenia Kirallen: 17 lat; jeszcze nie doszła do siebie (krzyczy w nocy) po akcji z serpentisami. Mimo, że się boi - chce iść chronić Klaudię przed niebezpieczeństwem. Ona i Felicjan zostali parą.
* Arnulf Poważny: przyznał się Talii i Tymonowi do tego, że ukrywa małą noktiankę (Teresę Mieralit). Wygnał terminusa Saszę z terenu szkoły, robiąc sobie w nim wroga. Samemu zestrzelił serię dron Strażniczki.
* Strażniczka Alair: elementy TAI w kolizji z BIA po wykryciu Teresy (noktianki) wprowadziły ją w nieskończoną pętlę i zaczęła chronić AI Core i przeżywać jeden z przeszłych koszmarów. Klaudia zanomalizowała jej generatory, przez co Strażniczka zasnęła.
* Felicjan Szarak: 19 lat; rozproszył grupę włamującą się do domu Talii słowami "terminus!" i powstrzymał Ksenię, która chciała ich gonić. Zarekwirował pojazd i szybko pojechał z Zespołem po Talię na prośbę Klaudii.
* Talia Aegis: przed jej domem pojawiają się jakieś manifestacje i próby napaści nocą..? Tak czy inaczej, dowiedziawszy się o problemach Strażniczki natychmiast w nocy się zebrała by jej pomóc. Jej wezwanie wzbudziło zaskoczenie Ksenii i Felicjana.
* Teresa Mieralit: 15 lat; disruptorka magii i paramedyk; noktianka pod opieką dyrektora Arnulfa (jego wychowanka). Ma niewyparzoną gębę. Jej obecność spowodowała kolizję w Strażniczce - elementy BIA wykryły jako "friend", elementy TAI jako "foe".
* Sasza Morwowiec: terminus; od dawna poluje na Tymona i uważa go za szkodnika. Gdy skonfrontował się z Tymonem w AMZ, Arnulf powiedział że to jego sprawa i Saszę wygnał - robiąc sobie z Saszy wroga.
* Tymon Grubosz: terminus; przybył pomóc Strażniczce w nocy do AMZ i pomógł Arnulfowi w obronie przed atakami dron aż Talia i Klaudia nie wyłączyły TAI. Potem skonfrontował się z nim Sasza, ale Tymon to olał.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Zaczęstwo
                                1. Akademia Magii, kampus
                                    1. Budynek Centralny: pieszczotliwie zwany "Fortecą". Rozległy i pancerny, z własnym zasilaniem itp. Okazuje się, że do generatorów i konwerterów dociera energia z Trzęsawiska. Konwertery zanomalizowane przez Klaudię. Miejsce walki Strażniczki z Arnulfem w obronie Teresy Mieralit.
                                    1. Akademik: Ksenia obudziła w nocy Klaudię krzykiem, bo serpentis pobrał jej w nocy krew. A to była drona Klaudii, bo Strażniczka wpadła w pętlę...


## Czas

* Opóźnienie: 27
* Dni: 1

## Konflikty

* 1 - Klaudia próbuje dojść do tego co do cholery się stało ze Strażniczką. Wie, jak TAI działa. Ma pewne objawy. Widzi jakie są odpowiedzi.
    * TrZ+2
    * XzXVV: Strażniczka "śni", jest w nieskończonej pętli i agreguje wszystkie siły do AI Core przed niezidentyfikowaną osobą. The meow?
* 2 - Felicjan zarekwirował pojazd którego używają w AMZ w wypadkach krytycznych. Ma prawo jako uczeń terminusa, wsadził odpowiedni kod. I szybko jedzie. Klaudia nawiguje, Felicjan prowadzi.
    * TrZ+2
    * XVVz: Felicjan porysował pojazd omijając ludzi włamujących się do Talii i ich rozgonił słowami "terminusi!". Zdążyli na czas.
* 3 - Talia zaczyna używać zaawansowanych kodów, wzywać Strażniczkę i pozostałe BIA by zmusić je do działania. A Klaudia leci wyłączyć zasilanie. Jako, że Strażniczka widzi ją jako niegroźną i "swoją".
    * Tr+2 (niepełna)
    * XXV: Strażniczka uznała TYMONA za przeciwnika po tym jak ten się pojawił; Klaudia dotarła blisko generatorów.
* 4 - Klaudia wie co mniej więcej chce zrobić. Nie wie jak. Chce przyzwać magię - czysta anomalizacja używając energii Trzęsawiska z której czerpie Strażniczka.
    * ExM+3+5O
    * VVX: Konwertery uszkodzone; pierwsza anomalia Klaudii i Strażniczka wyłączona.
* 5 - Klaudia pomaga Arnulfowi się wyplątać od terminusa Saszy fałszując rekordy by to miało sens.
    * TrZ+4
    * XXzVz: Arnulf użył autorytetu - ma prawo wezwać Talię; Sasza podejrzewa Arnulfa i Tymona o kolaborację przeciw Pustogorowi.
* 6 - Klaudia i Teresa stealthują. Klaudia wycofuje Teresę do gabinetu pielęgniarskiego.
    * TrZ+3 (bo Arnulf odwraca uwagę)(niepełna):
    * XVzV: przyleciała drona Klaudii i przestraszyła Teresę; Klaudia ją uciszyła i doprowadziła do gabinetu pielęgniarskiego.

## Projekt sesji

* 27 dni po poprzedniej
* Ksenia -> Klaudia, „Zamek Weteranów dla noktian”
* problem przekonania że tymi weteranami też trzeba się zająć.
* Petycje, działania, Ksenia na froncie a na banku Klaudia
* Duże wsparcie Felicjana - jeśli im nie pomożemy, w czym jesteśmy lepsi?
* Strażniczka Alair jest niestabilna - za mało energii, za dużo żądamy, pobiera Skażoną energię… Klaudia pełni rolę ukrywacza gdy Tymon i Talia próbują jakoś to skorygować.
* Felicjan -> Klaudia: niech Augustino dostanie jakieś podstawowe miejsce i sprzęty w okolicach Lasu. Sam tam zginie. A próbuje pomagać. 
* KONIEC SESJI: podpalenie domu noktianskich weteranów. Ksenia wyciąga weteranów, nie osoby które podpaliły. Felicjan jej pomaga. 
* Oddział Kryształowy Smok (noktianski) w okolicy Pacyfiki. Zajęli ten teren. Pustogor nic nie robi - nie ma na to sił.
* Tymon ma problemy z lokalnym terminusem, Saszą Morwowcem. Sasza uważa, że Tymon ma swoją agendę i ogólnie nie działa na korzyść Pustogoru. Sasza robi query. Klaudia to zauważa.
* Ogólna niechęć astorian do noktian. Próby linczu (Felicjan x Ksenia świadkami, oni próbują temu zapobiec i zostają pobici).

Sceny:

* Strażniczka jest niestabilna. Drony atakują studentów (Arnulf przedstawia to jako ćwiczenia ). 
    * M.in. Polują na Teresę Mieralit. 15-letnią czarodziejkę mieszkającą chyba na terenie akademii.
* lincz zatrzymany przez Ksenię i Felicjana. Ksenia prosi Klaudię o jakieś tematy prawne czy coś - ukarać astorian, pomoc w obronie Felicjana.
    * Co to za Inwazja, skoro przyszli z dziećmi?
* A dalej jakoś pójdzie. 
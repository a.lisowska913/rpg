---
layout: cybermagic-konspekt
title: "Gdy zabraknie prądu Rekinom"
threads: rekiny-a-akademia
gm: żółw
players: kić, anadia
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [211127 - Waśń o ryby w Majkłapcu](211127-waśń-o-ryby-w-majklapcu)

### Chronologiczna

* [211127 - Waśń o ryby w Majkłapcu](211127-waśń-o-ryby-w-majklapcu)

## Plan sesji

### Theme & Vision

* Rekiny chcą żyć w izolacji od świata, w wiecznej zabawie. Ale świat o nich nie zapomniał. I brak Amelii się odbił.
* Gdy się kończy prąd, Rekiny pokazują czym są.

### Ważne postacie + agendy

* Santino Mysiokornik: załatwił przenośny generator i paliwo. Show must go on. W końcu ma ulubieńców w Aurum.
* Justynian Diakon: dogadajmy się z miejscowymi.
* Urszula Arienik: czemu ja mam tracić na tym, że Rekiny traktują mnie w taki a nie inny sposób? Luxuritias zapewnia pieniądze.
* Ernest Namertel: czemu ja mam nie używać lokalnej magitrowni by Rekiny miały spokój?
* Sensacjusz Diakon: musimy mieć SZYBKO prąd, bo mam rannych. M.in. Lorenę.
* Barnaba Burgacz: chce zrobić to militarnie. Przekonać Arieników albo użyć siły ognia i ich przekonać.
* Natalia Tessalon: chce skorzystać z okazji i podpiąć Rekiny do Wolnego Uśmiechu. Generatory Keriltorn, w Czółenku.

### Co się wydarzyło

* Amelia dawno temu zawarła umowę z Elektrownią Szarpien. Ale Rekiny zawarły super tanią umowę.
* Elektrownia Szarpien praktycznie zbankrutowała. Wykupiła ich Urszula Arienik (wpływowy mag z Luxuritias; arystokratka acz nie Aurum). 
    * Urszula Arienik od dawna jest zirytowana faktem, że Rekiny z Aurum traktują lokalną arystokrację źle. Że niby jest "nowobogacka". Słowa Kacpra Bankierza.
    * Urszula widząc fatalną umowę zerwała ją korzystając z okazji, że minęły 3 lata od jej zawarcia i zgodnie z klauzulą mogła.
        * Amelia tak wynegocjowała z poprzednim właścicielem wiedząc, że jej już tu dawno nie będzie.
* przed Marysią stoją trzy pytania:
    * Co robimy z tym, że biją Torszeckiego :-(.
        * I jak to zrobić żeby nie było, że biją go bo Marysia pozwala bo woli Ernesta? 
        * Lub że Marysia go chroni więc go kocha?
    * Jak to zrobić, by Justynian oddał władzę lub by Marysia ją przejęła?
    * Myrczek x Sabina - co z tym robić?

### Sukces graczy

* .

## Sesja właściwa
### Scena Zero - impl

.

### Scena Właściwa - impl

Wieczór. Moment kulminacyjny. Walka Daniela Terienaka z Frankiem Bulterierem na Arenie Amelii (oczywiście). Rekiny patrzą. Daniel i Franek, półnadzy, przygotowują się do starcia. Do tej pory Daniel pokonał innych przeciwników, Franek też. Jedyna osoba którą zachęcali do walki a odmówiła to Justynian Diakon. Nikt nie chciał ściągać Arkadii - Sensacjusz powiedział, że on NIE leczy tych ran.

Daniel i Franek krążą. I zgasło światło. Wszystkie generatory padły. Wpierw - ryki "yeeeah!", Rekiny myślą że to przedstawienie. Ale po chwili do Marysi dotarła prawda. NIE MA PRĄDU. I zaraz oczy spojrzą na Marysię...

Magitrownia Pogardy jest raczej awaryjnym źródłem; głównie prąd idzie z Podwiertu.

Karolina poszła do magitrowni. Co się stało. Arnold Kłaczek. Ludzki szef magitrowni. Źródło magii padło.

Karolina -> Arnold. Co się stało? Magitrownia się przeciążyła. Arnold zgłaszał, ale nie było reakcji. Tak samo z częściami zapasowymi. To wszystko zgłaszał do "reprezentanta Aurum w komputerze", ale nic nie doszło. Zero reakcji. I Podwiert wyłączył prąd, 15 minut później przeciążona magitrownia (po podżarta robactwem) się stopiła. Bezpieczniki.

Karo patrzy na magitrownię - atest: Aurum. Sprzęt: Aurum. Komponenty: Aurum. Nic z Podwiertu. Nie ma części zapasowych. Arnold mówi, że zgłaszał "reprezentatowi Aurum" konieczność zdobycia części - bez reakcji. I Arnold patrzy wyzywająco na Karo jakby Karo miała powiedzieć "Twoja wina". A Karo ma to gdzieś. Chciałby dać Karo bezpiecznik, ale musi Oficjalny Przedstawiciel Aurum ją do tego upoważnić.

Karo na szybko sprawdza czy jest jakiś Rekin nadający się do tego. NOPE. Nie ma.

Marysia chciałaby sprawdzić kto jest reprezentantem Aurum na tym terenie, ale NIE MA PRĄDU i nie może. Nie działają przekaźniki hipernetowe bo nie ma prądu. 

Na szczęście prąd ma Sensacjusz. Marysia go idzie odwiedzić... 

Sensacjusz zdziwił się, że Marysia przyszła odwiedzić Lorenę. Marysia wyprowadziła go z błędu - ona jest po prąd XD. 

Marysia chce wyciągnąć od Sensacjusza kto jest Oficjalnym Przedstawicielem Aurum itp. Tak, by on się nie zorientował i by nie wiedział, że Marysia nie wie. I jakby co żeby nie było na nią.

TrZ+3:

* X: Marysia NIGDY nie siedziała w biurokracji. Nie wiedziała że musi coś zaznaczać. Więc OFICJALNIE nie ma OPA na tym terenie i system komputerowy nic jej nie wysyłał.
* Vz: Sensacjusz jest przekonany, że Marysia wie - Amelia była OPA na tym terenie i zaznaczyła, że następny mag rodu Sowińskich musi tylko zaznaczyć w systemie komputerowym że jest na tym terenie i będzie działać.

Marysia poprosiła Sensacjusza o udostępnienie generatora. Sensacjusz nie chce. Marysia NALEGA. To dlatego, by móc pomóc biednej, uszkodzonej TAI by naprawić prąd. Zajmie tylko chwilkę.

* X: Lorena musi iść pod glizdę. Z winy Marysi.
* X: Lorena jest przekonana, że to dlatego, bo Marysia jej nie lubi i wini ją za to / stoi po stronie Karoliny. Lorena lepiej niech się Marysi nie pokazuje na oczy.
* V: Sensacjusz przekazał Marysi generator. "Ale nie rozładuj". Marysia zrobiła słodkie oczka.

Marysia WIE, po prostu WIE, że Amelia ją wystawiła. Nie zostawiła kartki. Wiadomości. NIC. Mogła. Nawet Justynianowi nic nie powiedziała. Albo powiedziała mu a on jej nie powiedział :>.

Marysia poprosiła Karolinę o to, by przeciągnęła kable od Sensacjusza do komputerów. Niech Marysia weźmie laptopa, uruchomimy TAI w Komputerowni, Marysia dostanie info, wyłączamy zasilanie i będzie dobrze.

Karo ściągnęła do pomocy Daniela i ścigacze. Będą kable ciągnąć.

Karo widzi 3 Rekiny - Bulterier, Babu i Henryk Wkrąż - w pozycji agresywnej wobec ludzi z magitrowni. Wkrąż się żołądkuje. Karo podbija do tej trójki. Rekiny chcą, by ludzie wrócili do magitrowni. Arnold broni się, że kontrakt pozwala im się wycofać i Rekiny wyraźnie źle wynegocjowały.

Karo podjeżdża na ścigaczu ze "vroom". Zatrzymuje się pomiędzy. I "co tu jeszcze robicie, miało Was tu nie być". A do Arnolda - "a Ty jedziesz ze mną do reprezentanta Aurum". Arnold "minęła godzina, już nie muszę". Wyraźnie Arnold CHCE dostać wpierdol. Musi mieć dobre rzeczy w kontrakcie.

TrZ+2:

* V: Arnold, niechętnie, pojedzie z Karoliną.

Arnold zażądał przeprosin od Rekinów. Wkrąż i Babu się zaczęli rzucać, Bulterier ma to gdzieś. Bulterier do Karo "chyba nie myślisz że ci ludzie mogą stąd pójść." Karo zaczęła wyjaśniać, Bulterier zaeskalował, Daniel się odezwał - "siostra to świętość, nie waż się". I Rekiny odpuściły. Daniel dodał "pozwól mojej siorce robić to co trzeba byśmy mieli prąd." I Rekiny odpuściły. Arnold, Daniel i Karo lecą do Komputerowni gdzie czeka Marysia.

Marysia dostała pełnię info od Karoliny. TERAZ WIE. Marysia podpisała upoważnienie Arnoldowi o OPA by pomógł Karolinie.

Karolina próbuje podpiąć z pomocą Arnolda kable do Komputerowni. No i ludzie z Komputerowni też.

TrZ (Arnold) +4:

* V: Bez problemu udało się podpiąć generator do systemu. Obudziła się TAI.

TAI Hestia d'Rekiny. "Witaj, administratorko. Zajęło Ci chwilę." Jak tylko Marysia powiedziała Hestii o tym, że chce mieć z Pustogoru i bezpieczniki do magitrowni, TAI ruszyła do działania. Hestia spytała, czy ma tylko bezpieczniki czy WSZYSTKIE na prąd itp - tak, wszystkie. "I atestowanego debila z Aurum, tylko napisz to ładnie". Hestia napisała że potrzebuje starszego inżyniera z atestem do wymiany bezpieczników w magitrowni Aurum.

Hestia powiedziała, że pomiędzy tygodniem a 2 tygodniami będzie załatwione. Dostała informacje z Aurum. A z Podwiertem jest większy problem. Poprzedni dostawca prądu zbankrutował, zostało to kupione przez kogoś innego, ten ktoś inny po 3 latach podniósł cenę do 300%, faktura odrzucona 3 razy, odciął prąd. Nie mamy aktywnej umowy.

Hestia poinformowała o dostawcach prądu:

* Elektrownia węglowa Szarpien. Poprzedni dostawca. Kupiony przez "lokalnych arystokratów", Arieników.
* Dystrybutor prądu Ozitek. Klasyczny dostawca większości prądu. Niezawodni, nie tani, ale skuteczny. Tańszy niż Szarpien teraz.
* Generatory Kerilorn. Tanie, solidne, ale powiązane z mafią. Hestia odradza z przyczyn wizerunkowych i politycznych.

Marysia ma wiadomość na hipernecie. To Justynian. Prosi wszystkich magów o stawienie się niedaleko magikonwertera. Ma pomysł - używając katalistów _może_ się uda zbudować pole energii magicznej by chociaż godzinę prądu uzyskać.

Marysia odpowiedziała na hipernecie: "coś się zepsuło, robaki pogryzły kable itp.". Dzięki Justynianie, udało Ci się zająć wszystkich w czasie czego rozwiązałam problem.

CEL MARYSI: niech nikt nie zadaje pytań o to, czemu nikt na to nie patrzył.

ExZ (reputacja Marysi i dokonania do tej pory) +3:

* X: Rekiny są przekonane, że Marysia rozwiązała problem TERAZ. Czyli w ciągu paru godzin. Prąd w parę godzin.
* X: Wina spada na Arieników. To ich wina. Przez nich nie ma prądu, podnieśli ceny, faktury itp. BO RODY INNYCH REKINÓW NIE CHCIAŁY TYLE PŁACIĆ ALE MARYSIA NIE POWIE KTÓRE RODY BY NIE BYŁO IM WSTYD!
* Vz: Z uwagi na reputację, nikt nie spyta "czemu nie było prądu". Nikomu nie przyjdzie do głowy.

Ogólnie, jest już noc. Marysia w nocy NIE podpisze normalnej umowy. A rano fajnie mieć prąd. W czym - Marysia słyszy na hipernecie głos, mówi do wszystkich. To nie jest Rekin, ale to arystokratka Aurum która tu pomieszkuje.

Natalia Tessalon. Ona mówi, że najsensowniej skorzystać z okazji i podpiąć Rekiny do Generatorów Keriltorn. Są zdecydowanie najtańsze, dają najwięcej korzyści, pakiety promocyjne dla każdego Rekina i dają gwarancję niezawodności. I Rekiny słysząc o Arienikach i teraz o tych Generatorach są na zasadzie "no tak, ta firma może być lepsza". Nawet Justynian "Amelia zawsze szukała tanich rozwiązań..."

Marysia -> Karo "czyli chcą współpracować z mafią? - przekaż Arkadii ;-)"

Karo -> Arkadia "jakaś kretynka, nieRekinka... masz sygnał." + dorzuca kontekst wyboru providera prądu. "Ale mafia?".

Arkadia na hipernecie do Rekinów + Natalii. "CO?!" I wyjechała z mową. W tej mowie zarzuciła im, że łamią Tradycję Aurum, potencjalnie łamią Porozumienie Pustogorskie a sama Natalia nie jest Rekinem, Arkadia może ją pociąć, poranić itp. i Natalia jeśli chce współpracować z mafią będzie osobistym wrogiem Arkadii.

Natalia pisnęła, że nie wiedziała o mafii (skłamała). Arkadia to łyknęła. Przeprosiła. Ale misja wykonana - Rekiny nie chcą wiązać się z tą firmą XD. A raczej - niech rozwiąże to Marysia. I niech będzie prąd.

Rano, Marysia podpisała umowę na prąd z Dystrybutorem Prądu Ozitek. Nie było to tanie, ale było uczciwe. Największy, sensowny dystrybutor prądu w okolicy.

I kryzys został zażegnany.

## Streszczenie

Ponieważ Amelia nie przekazała Marysi informacji o tym, że ta powinna przejąć obowiązki zarządcze to Marysia jako administratorka nie dostawała informacji. Przez to odcięło im prąd. Marysia przywróciła prąd podpisując (standardową) umowę z dystrybutorem podwierckim a Karolina uratowała ludzką część Dzielnicy Rekinów od pobicia przez wkurzonych magów. Próba obniżenia rachunków przez współpracę z mafią została przez dziewczyny zażegnana używając Arkadii.

## Progresja

* Marysia Sowińska: została Oficjalnym Przedstawicielem Aurum wśród Rekinów. Współpracuje z nią Hestia d'Rekiny.
* Lorena Gwozdnik: święcie przekonana, że Marysia stoi po stronie Karoliny i nią gardzi / jej nienawidzi. Musi unikać Marysi.
* Arkadia Verlen: jej burzliwe wystąpienie NAPRAWDĘ wzbudziło niezadowolenie mafii. Zwłaszcza po działaniach przeciwko Majkłapcowi.
* Urszula Arienik: lokalny "ród" Arienik jest uznany za wrogów numer jeden Rekinów. Przez Rekiny. Bo prąd odcięła.

### Frakcji

* .

## Zasługi

* Marysia Sowińska: nie wiedziała, że musi być Oficjalnym Przedstawicielem Aurum (dzięki, Amelia), więc nim nie była i odcięli prąd. Szybko załatwiła prąd z innego źródła i deeskalowała ryzyko, że ktoś na nią spojrzy; zrzuciła ogień na Arieników.
* Karolina Terienak: wyciągnęła z Arnolda czemu nie ma prądu, uratowała ludzi pracujących w magitrowni Rekinów przed pobiciem i wciągnęła Arkadię w opiernicz Natalii za mafię.
* Arnold Kłaczek: ludzki szef magitrowni Rekinów; najwyraźniej chce dostać wpierdol (dla ubezpieczenia?). Nie ma Atestów Z Aurum, więc tylko nadzoruje magitrownię.
* Daniel Terienak: miał się bić z Bulterierem na arenie, ale padł prąd. Potem pomagał Karo ciągnąć kable i jak Karo była narażona na walkę z Rekinami, powiedział, że siostra to świętość. Walka się nie odbyła.
* Arkadia Verlen: poinformowana przez Karo, że Natalia Tessalon chce wrobić Rekiny w ciągnięcie prądu od mafii. Zrobiła ostrą mowę roku. Gdy Natalia powiedziała, że nie wiedziała że to z mafii to Arkadia jej uwierzyła.
* Natalia Tessalon: NIE JEST REKINEM. By pomóc bratu, współpracuje z mafią (Wolny Uśmiech). Chciała wciągnąć Rekiny do brania prądu od mafii, ale Arkadia skutecznie to zablokowała. Przerażona Arkadią.
* Henryk Wkrąż: żołądkuje się do ludzi pracujących w magitrowni, że mają pracować póki będzie prąd (mimo umowy i braku możliwości). Zgaszony przez brata Karoliny (Daniela).
* Sensacjusz Diakon: ma awaryjny generator prądu w szpitalu; Marysia dostała do niego dostęp by przywrócić prąd. Ale przez to musiał wsadzić Lorenę w glizdę medyczną.
* Lorena Gwozdnik: w gliździe medycznej, bo Marysia potrzebowała prądu z generatora Sensacjusza.
* Urszula Arienik: przechwyciła bankrutującą Elektrownię Węglową Szarpien i korzystając z okazji zerwała kiepską umowę którą kiedyś podpisała Elektrownia z Amelią Sowińską. Przez to Rekiny zostały bez prądu.
* Hestia d'Rekiny: TAI Rekinów mieszkająca w Komputerowni. Ma nieco manieryzmów Amelii. Przyjęła Marysię jako Oficjalną Przedstawicielkę Aurum i będzie jej pomagać.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Podwiert
                                1. Dzielnica Luksusu Rekinów: jeden wieczór bez prądu, bo Elektrownia Szarpien odcięła prąd.
                                    1. Serce Luksusu
                                        1. Arena Amelii: odbywała się tam ostra walka Daniel - Franek. Ale wtedy zgasł prąd.
                                    1. Sektor Brudu i Nudy
                                        1. Komputerownia: miejsce rezydencji Hestii; Hestia przekazała info Marysi i ustawiła ją jako Oficjalnego Przedstawiciela Aurum.
                                1. Kompleks Korporacyjny
                                    1. Elektrownia Węglowa Szarpien: kiedyś Amelia podpisała z nimi świetną umowę; przejęta przez Arieników po bankructwie. Szarpien odcięli prąd Rekinom.
                                    1. Dystrybutor Prądu Ozitek: Marysia podpisała z nimi umowę na prąd dla Rekinów.
                            1. Czółenko
                                1. Generatory Keriltorn: transformacja magii w energię elektryczną; pod kontrolą ludzi Grzymościa. Mimo starań Natalii Tessalon, nie podpisali umowy o dostawę prądu Rekinom.

## Czas

* Opóźnienie: 6
* Dni: 1

## Konflikty

* 1 - Marysia chce wyciągnąć od Sensacjusza kto jest Oficjalnym Przedstawicielem Aurum itp. Tak, on nie wiedział, że Marysia nie wie. I żeby nie było na nią.
    * TrZ+3
    * XVz: Marysia NIGDY nie siedziała w biurokracji. Nie wiedziała że musi coś zaznaczać; Sensacjusz jest przekonany, że Marysia wie
* 2 - Marysia poprosiła Sensacjusza o udostępnienie generatora. Marysia NALEGA. To dlatego, by móc pomóc biednej, uszkodzonej TAI by naprawić prąd.
    * TrZ+3
    * XXV: Lorena pod glizdę z winy Marysi i przekonana, że Marysia jej nienawidzi / gardzi; Marysia ma generator.
* 3 - Karo deeskaluje konflikt Rekiny - ludzie z magitrowni.
    * TrZ+2
    * V: Arnold idzie z Karo; deeskalował Daniel.
* 4 - Karolina próbuje podpiąć z pomocą Arnolda kable do Komputerowni. No i ludzie z Komputerowni też
    * TrZ (Arnold) +4
    * V: Bez problemu udało się podpiąć generator do systemu. Obudziła się TAI.
* 5 - CEL MARYSI: niech nikt nie zadaje pytań o to, czemu nikt na to nie patrzył.
    * ExZ (reputacja Marysi i dokonania do tej pory) +3
    * XXVz: Rekiny są przekonane, że w ciągu paru godzin; wina za brak prądu spada na Arieników; Z uwagi na reputację Marysi, nikt nie spyta "czemu nie było prądu"

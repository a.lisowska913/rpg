---
layout: cybermagic-konspekt
title: "Grzybopreza"
threads: rekiny-a-akademia
gm: żółw
players: anadia, kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [201020 - Przygoda, randka i porwanie](201020-przygoda-randka-i-porwanie)

### Chronologiczna

* [201020 - Przygoda, randka i porwanie](201020-przygoda-randka-i-porwanie)

## Punkt zerowy

### Dark Past

.

### Opis sytuacji

* Triana z Julią reanimowały mechanicznego psa i próbnie ustawiła go na "znajdź trufle". Pomyliła się - dała 10 km a nie 10m.
* Pies uciekł, szukać trufli.
* Triana w panice wezwała na pomoc Marysię Sowińską - jedyną, która jest na tyle "ponad prawem" by nic jej nie groziło.

### Postacie

* Triana Porzecznik, która pragnie odzyskać swojego mechanicznego psa
* K9Epickor, "pies" Triany pragnący zdobyć dla niej trufle

## Punkt zero

.

## Misja właściwa

Triana dostała od ojca bardzo zdewastowany _frame_ technopsa militarnego. Ten technopies służył kiedyś do ratowania rannych na polu bitwy - search & rescue. Ściągnęła do pomocy Julię Kardolin, swoją koleżankę. I wspólnie go postawiły na nogi. Julia zamontowała psychotronikę, dziewczyny zdecydowały się sprawdzić czy K9Epickor sobie poradzi z czymś prostym. Padło na znalezienie trufli. Triana schowała trufle w ogródku, po czym wydała K9 rozkaz ściągnięcia najsilniejszych trufli. Niestety, pomyliła się w rozkazach - ustawiła nie 10m zasięg a 10km. Epickor uciekł. Triana zapytana czy może go wezwać, powiedziała, że nie. Psychotronika nie jest skończona a K9 włączył kamuflaż.

Zostało tylko wezwać wsparcie. Triana wezwała Marysię. A Julia wykombinowała rozwiązanie - potrzebują Ignacego Myrczka. On może zrobić najbardziej epickie trufle jakie K9 może znaleźć.

Marysia zdecydowanie próbuje zdążyć złapać Myrczka zanim K9 znajdzie jakieś trufle, rozwali komuś dom itp.

* X: ścigacz jest w overdrive; uszkodził się tłumik czy coś.
* V: zdążyła na czas, jest przy akademiku. Zapukała do okna, Myrczek się wychylił i podmrożony - widzi ostrą laskę na ścigaczu
* X: "Myrczka porywają!" - krzyk z dołu. Myrczek w reakcji też krzyczy. Marysia się wychyla i wciąga go na ścigacz.
* O: Napoleon wybiega. Uruchamia swój ścigacz...
* VX: Myrczek SPADŁ (nie utrzymała go), Marysia podleciała ścigaczem w dół ASAP i go przechwyciła. Myrczek trzyma ją kurczowo. WSZYSCY przekonani, że Marysia to porywacz...
* X: Napoleon za dobrze steruje ścigaczem, Marysia się nie oderwie
* O: Manewry unikowe. Pętle. Grzyby Myrczka się rozsypują po całej okolicy...
* VO: Marysia wystrzeliła z krótkiego zasięgu śluzem. Myrczek cały brudny. Napoleon stracił kontrolę nad ścigaczem i do jeziora...

Marysia szybko doprowadziła Myrczka do Triany i Julii, z lekko zblazowaną miną.

Julia zabiera się do redukcji szoku Myrczka. Wpierw, by puścił Marysię, rzuca mu "damie jest niewygodnie" (V). Następnie tłumaczy mu, że potrzebują pomocy z grzybami (V). Myrczek radosny. Na to Triana zaprasza go do swojej piwnicy - to sprawia, że Myrczek jest czerwony. A tymczasem skaner Marysi pokazuje, że coś się tu zbliża, servar. Julia pada na kolana przed Myrczkiem, robi mu "pomóż mi Obi Wan Kenobi, jesteś naszą jedyną nadzieją". Myrczek potwierdzi, że to nie porwanie (V), ale to fundamentalnie spieprzy (X). No ale jeśli to terminus, Myrczek sobie poradzi. Tyle, że w servarze przyleciała Ula. Uczennica terminusa. Dziewczyna...

Oczywiście, rozmowa z Myrczkiem poszła nie tak. Myrczek się rozmarzył mówiąc, że Triana organizuje grzybną imprezę, będzie alkohol z grzybów, purchawki z muzyką, wszędzie kolorowe zarodniki, grzybne psychodeliki... Ula słuchając tego jest coraz bardziej podejrzliwa a Julia coraz ostrzej gestykuluje. W końcu stanęło na tym, że Ula wie, że Coś Jest Nie Tak. Wie też, że Myrczek nie jest porwany. Ula oddala się, średnio zadowolona z wyniku śledztwa, ale zostawia kilka dron do monitorowania posiadłości Triany.

Gdy Kacper Bankierz wysyła Marysi sticker "supercool" ze zdjęciem przemoczonego w jeziorze Napoleona Bankierza, Marysia parsknęła śmiechem. Fajny widok.

Ignacy nie ma sporej ilości swoich zarodników itp. Julia wpada na dobry pomysł - zwinęła tacie Triany obiad z lodówki. Plus różne słoiki. Plus inne rzeczy. Tak więc Myrczek ma w sumie na czym pracować, nawet jak to nieidealne.

* V: udało się zrobić super-trufle. Aromatyczne i skuteczne i w ogóle - ale trzeba je jeszcze nadać K9.
* X: trufle niestety są mało stabilne i wybuchają (na mokro, nie eksplozjami). 
* X: trufle są STRASZNIE śmierdzące.
* X: trufle są w miarę persystentne; zainfestują miejsce w którym K9 będzie przechwycony.
* V: pies je spriorytetyzuje. Są mocne, aromatyczne i wykrywalne.

Ale jak odwrócić uwagę Uli? Potrzebna będzie PRAWDZIWA impreza. Grzybowa Impreza. Marysia od razu łapie Kacpra na hipernecie...

* Marysia chce, by Kacper ściągnął swoją ekipę na grzyboprezę.
* ma być wszystko nienaganne, wpakują Ulę (terminuskę) na minę. Będzie dla niej hańba.
* XX: uda się "za bardzo" - Ula dostanie solidny OPR bo dała się wrobić. I będzie mieć kosę z Marysią i Myrczkiem.
* V: będzie impreza
* V: będzie czysta
* V: uda się imprezą zaimponować Kacprowi; Marysia pokaże swoją pozycję i swój mischief.
* V: wiadomości w gazecie: "oddziały terminusów rozbiły jedyną CZYSTĄ imprezę Rekinów w Zaczęstwie". Godne, głośne wyśmianie.

(w międzyczasie Myrczek dodatkowo robił wymarzone grzyby na imprezę) 

Dobra - trzeba ściągnąć Ulę z monitorowania domu Triany. Marysia + Myrczek na ścigacz, by wyglądać maksymalnie podejrzanie. Marysia zaproponowała Myrczkowi, by ten poprowadził trochę ścigacz (by mogła z _plausible deniability_ rozwalić drony nadzorujące).

* X: są tu monitorujące drony Uli
* V: Ula skieruje uwagę na Marysię i Myrczka, ale drony zostaną
* XV: "Ignacy, chcesz popilotować" - drony podewastowane, ścigacz zarył w trawnik Triany, potem - mandat od Uli. Ale drony poniszczone.

No i zaczęła się Epicka Grzyba Impreza monitorowana przez Ulę, gdzie Ula niedługo wezwała posiłki...

A tymczasem przy Papierówce Triana i Julia wsadziły trufle. Przybiegł K9 i został przechwycony i unieszkodliwiony. Misja wykonana z powodzeniem.

A Myrczek? Nawet zatańczył wolny taniec z dziewczyną. Best day ever.

## Streszczenie

Trianie "uciekł" źle skonfigurowany techno-pies ratujący ludzi. Marysia Sowińska (Rekin) porwała Myrczka na pomysł Julii, zrobili super-trufle i Triana z Julią dotarły do psa i go unieczynniły. Tymczasem Marysia i Myrczek wymanewrowały Ulę (uczennicę terminusa) i sprowokowali ją do wezwania wsparcia przeciw ad-hocowej czystej grzyboprezie Rekinów. Nawet Pakt o tym napisał.

## Progresja

* Urszula Miłkowicz: HAŃBA! Wezwała wsparcie do CZYSTEJ (zero alkoholu czy narkotyków) imprezy Rekinów. Oczywiście, to była prowokacja. Ale Ula nie powinna na to była się wpakować.
* Urszula Miłkowicz: nie lubi arystokracji Aurum. A zwłaszcza Marysi Sowińskiej. Dodatkowo - nie lubi Ignacego Myrczka. Ma opinię, że się bała że on coś zrobi złego na imprezie...
* Marysia Sowińska: Urszula Miłkowicz, uczennica terminusa ma z nią kosę. Ale Kacper Bankierz ma u niej duży szacunek za pojechanie Napoleona a potem wpakowanie terminusów w "pułapkę" czystą grzyboprezą Myrczka.
* Ignacy Myrczek: Urszula Miłkowicz, uczennica terminusa ma z nim kosę.
* Kacper Bankierz: szacunek wobec Marysi Sowińskiej - nie tylko jest SOWIŃSKA, ale też wyrolowała terminuskę (Ulę) oraz wpakowała Napoleona do jeziora na jego ścigaczu.
* Triana Porzecznik: pozyskała w miarę sprawnego technopsa militarnego służącego do ratowania ludzi. Nazwała go "K9Epickor". Przy AI pomagała Julia Kardolin.

### Frakcji

* .

## Zasługi

* Marysia Sowińska: porwała Myrczka z okna akademika, wymanewrowała Napoleona na ścigaczu (wrzucając go do jeziora), po czym zorganizowała grzyboprezę i ściągnęła na nią Kacpra Bankierza - na złość terminusce.
* Julia Kardolin: wymyśliła Myrczka do odzyskania techno-psa, po czym namówiła Myrczka do pomocy. Docelowo wymknęła się z Trianą i przechwyciła psa - kosztem zatruflowania okolic biurowców w Zaczęstwie.
* Triana Porzecznik: odrestaurowała ratunkowego psa wojskowego (K9Epickor), który dał w długą. Pomogła Myrczkowi zrobić super-trufle. Następnie z pomocą Julii wyłączyła K9 i go zwinęła, unikając uwagi Uli (uczennicy terminusa).
* Ignacy Myrczek: porwany przez Marysię Sowińską ścigaczem z akademika w nocy, pomógł Trianie zrobić super-śmierdzące wybuchające trufle i grzyby do grzybo-imprezy. Potem poszedł na grzyboprezę i nawet ZATAŃCZYŁ Z MARYSIĄ SOWIŃSKĄ! Ogólnie, super dzień.
* Napoleon Bankierz: próbował uratować porwanego przez Marysię Myrczka; niestety, Marysia dużo lepiej nawiguje ścigaczem niż Napoleon. Po dostaniu wiązką śluzu skończył w jeziorze Zaczęstwa. Wezwał Ulę (terminuskę) na swoje miejsce.
* Urszula Miłkowicz: wezwana przez Napoleona, wyczuła, że coś jest nie tak z "porwanym Myrczkiem". Ale niewiele mogła zrobić. Po obserwowaniu domu Triany, dała się wymanewrować i wezwała wsparcie przeciw "czystej" grzyboprezie Rekinów i Myrczka.
* Kacper Bankierz: gorąco pogratulował Marysi Sowińskiej wpakowanie Napoleona do jeziora. Potem namówiony przez Marysię zrobił grzybową CZYSTĄ imprezę ze swoimi Rekinami - by tylko wpakować w kłopoty terminusów i by się to głośno odbiło.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Zaczęstwo
                                1. Akademia Magii, kampus
                                    1. Akademik: gdzie podleciała Marysia ścigaczem, porwała Ignacego i wymanewrowała Napoleona lecąc w kierunku 
                                1. Biurowiec Gorland: niedaleko niego Julia z Trianą odzyskały K9Epickora, rozsypując wybuchające śmierdzitrufle...
                                1. Dzielnica Kwiecista
                                    1. Rezydencja Porzeczników
                                        1. Podziemne Laboratorium: gdzie Myrczek z Trianą zrobili super-śmierdzitrufle (wybuchające) i grzyby imprezowe.
                                1. Nieużytki Staszka: gdzie Rekiny zrobiły jedyną "czystą" grzybo-imprezę i gdzie wpadła zirytowana Ula ze wsparciem terminusów by patrzeć na grzyby...
                                1. Park Centralny
                                    1. Jezioro Gęsie: gdzie plasnął efektownie ścigacz Napoleona w środku nocy, po manewrach Marianny

## Czas

* Opóźnienie: 177
* Dni: 1

## Inne

### Projekt sesji

#### Procedure

* Overarching Theme:
    * gracze mają "kierowanie" (Vision & Purpose); wiedzą gdzie iść i po co tam idą
    * gracze są bardziej zaangażowani w historię, bo widzą, gdzie ich decyzje prowadzą
    * MG wie, które sesje wprowadzać i które usuwać
    * MG wie, w którą stronę stawiać adwersariat
* Achronologia:
    * przez przeskakiwanie do przodu nieważnych wydarzeń oczekuję, że każda sesja jest powiązana z kontekstem (gracze widzą jak się to rozwija)
    * przez cofanie się i uzupełnianie luk w przeszłości oczekuję, że brakujące MG i graczom luki uzupełnimy takimi działaniami
    * przez działanie w przeszłości gracze czują silniejszą relację z postaciami i światem, silniejsza budowa wspólnej historii.
* Wyraziste, ważne dla graczy postacie + Relacje (konflikt?) Gracze <-> NPC, NPC <-> NPC
    * graczom bardziej zależy na postaciach i ich powodzeniu
    * więcej potencjalnych wątków i rzeczy które możemy odrzucić przy krzyżykach
* Tory, zmiana postaci z czasem, wpływ graczy na świat
    * gracze widzą jak wpływają na świat. Wpływ jest "ich" - stąd tak tragiczne jest gdy "ich" rzeczy są corrupted / destroyed.

#### Result

**Przeprowadziłem inną sesję, bo Anadia miała tylko 2h, ale na bazie tej**

* Overarching Theme:
    * V&P:
        * zabawa studentów AMZ; rywalizacja pomiędzy studentami
        * Lucjusz próbuje poznać sposób pomocy Pięknotce
    * adwersariat: "Anna" + "Adam", próbujący zdobyć materiał genetyczny Rekinów i Studentów dla Lucjusza Blakenbauera
    * adwersariat: Aranea, próbująca się ukryć jak najskuteczniej przed wszystkimi
* Achronologia:
    * N/A.
* Wyraziste, ważne dla graczy postacie + Relacje (konflikt?) Gracze <-> NPC, NPC <-> NPC
    * Triana: reprezentuje techniczne podejście i naukę / odkrycia
    * Napoleon: reprezentuje potęgę magii
    * Urszula Miłkowicz: reprezentuje władzę i potęgę terminusów
    * "Anna" i "Adam": reprezentują zagrożenie ze strony magii
* Tory, zmiana postaci z czasem, wpływ graczy na świat
    * Napoleon znajduje Araneę
    * Aranea skutecznie się chowa
    * 

#### Scenes

* Scena 0: 
* Scena 1: 
* Scena 2: 

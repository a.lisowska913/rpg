# Operacje
## Sceny

### Wpływ Arianny na Arkadię Verlen (okolice 211117-porwany-trismegistos )	<-- DONE 50%

2 tygodnie po  :

* Arkadia Verlen pisze list od fanów do Izabeli Zarantel "mniej Eustachego, mniej Klaudii, więcej Arianny"
* Arkadia największa fanka Sekretów Orbitera i wierzy w to co tam jest XD

...

* Fanfiki, kiepskie, z Arianną jako Mary Sue. Pisze je Arkadia.

### Smutny sojusz Loreny i Torszeckiego. I ochrona Arkadii

* Lorena jest sama, słaba, połamana i nie ma przyjaciół
    * acz odwiedzi ją Daniel Terienak z sympatii; jego jednak Lorena odepchnie
* Na Torszeckiego polują
* I będzie ich chronić... **Arkadia**

### Prośba Myrczka do Marysi Sowińskiej - Sabina!	<--- DONE 211127

* Myrczek nie może zapomnieć o Sabinie Kazitan.
* Myrczek poprosi Marysię o audiencję. Czy może łaskawie doprowadzić do ich spotkania? Może być zdalne.

### Karolina słyszy o ścigaczu?

* Eksperymentalny ścigacz z Aurum z inną formą neurosprzężenia przez sentisieć.
* Neurosprzęg chce zdominować maga. Jest strasznie męczący fizycznie i psychicznie.

### Kampania „Wyleczone Czółenko” (dla Kić)

1.	Potwór Esuriit w przeszłości - coś strasznego, sesja dla terminusow.	<-- irrelevant
2.	Próba wyleczenia Czółenka przez 2-3 magów z Eterni. Porażka. Trzech Skażonych magów. Dyrektor szkoły umiera chroniąc dzieciaki przed jednym z dzieciaków. 		: DONE
3.	Aktualna chronologia
	1.	Trzeba odbić Alicje z Pustogoru i ja schować w Czolenku
	2.	Trzeba pozyskać Jolantę Kopiec i dać jej info ze tu coś sie działo
	3.	Trzeba zbudować link z Trzesawiskiem
	4.	Rytuał: Jolanta, transfer Ernest, Amanda, Alicja —- Pieknotka
        1.	Przy użyciu Saitaera, który jest Władca Adaptacji
	    2.	Zrobić „pole neutralizacyjne dwustronne”, zlinkować Saitaera z Czółenkiem przez Trzęsawisko 
	    3.	Zatrzymać / odciągnąć uwagę terminusow
	    4.	Pieknotka TRZYMA Saitaera
	    5.	Lucjusz ratuje tamta trójkę 

### Okolice Anomalia Kosmiczna Infernia

* ( Tivr ma zniszczonego miragenta na pokładzie. Pokłosie Sowińska - Morlan. Wygląda jak martwa Arianna. )	
* Elena chce wziąć Infernię i polecieć sprawdzić coś dziwnego co wykryła na poligonie. Nie chce być z innymi.           WONTDO
* Tymczasowa załoga Inferni - przemówić do nich
* Stocznia chce wsparcia Klaudii i Eustachego. Mają różne prototypowe jednostki.
* Hestia Stoczni wspiera napastników.
* Siły z Neikatis chcą pozyskać prototypy. Program kosmiczny Neikatis. Że z Plugawego Jaszczura niby.

* Starcia między "kultystami", "noktianami" i miłośnikami lolitek.


* POTENCJALNIE
    * Atak na Stocznię Neotik.
    * Siły z Neikatis chcą pozyskać prototypy. Program kosmiczny Neikatis. Że z Plugawego Jaszczura niby.
    * Elena obejmuje drugą Nereidę, za radą Adama.

* Mechanicy, jeden z nich w formie ixiońskiej
* "Mamy ixiońską anomalię kosmiczną. Szept Saitaera!"
    * kultyści Saitaera -> Infernia?
* Kult Arianny na pokładzie Inferni vs pozostali.
    * Kamil aktywnie proponuje, by znaleźć "zagubione dusze" i ich skonwertować na Infernię
    * "Nikt nie opuści Inferni"
        * Noktianie chcą na Tivr, część noktian pro-Infernia.
* Ixioński terrorform, złożony z Koruptora Ixiońskiego przez Infernię
* Koloidowe jednostki - Elena chce ich odszukać Tivrem? Ale nie prowokować potwora?

### Anadia, na 211221

* Chevaleresse, dziewczyny, autodefensywy.
* Santino ma swój "shock podcast".
* Alan chce to rozwalić.

Czyli do problemów z:

* Torszeckim
* Ernest - Rekiny
* Justynian "pragnący" władzy
* Myrczek x Sabina

dochodzą Marysi jeszcze problemy:

* Kult Ośmiornicy x Santino Mysiokornik
* Chevaleresse? (ale na pewno ród Arieników i Rekiny x Arieniki)
* Oficjalna Administratorka Aurum i jedna niesforna Hestia

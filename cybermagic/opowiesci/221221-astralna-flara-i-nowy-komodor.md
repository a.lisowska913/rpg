---
layout: cybermagic-konspekt
title: "Astralna Flara i nowy komodor"
threads: historia-arianny
gm: żółw
players: fox, kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [221130 - Astralna Flara kontra Domina Lucis](221214-astralna-flara-kontra-domina-lucis)

### Chronologiczna

* [221130 - Astralna Flara kontra Domina Lucis](221214-astralna-flara-kontra-domina-lucis)

## Plan sesji

### Fiszki
#### 1. Astralna Flara (55 osób max)
##### 1.1. Dowodzenie

* Arianna Verlen, kapitan
* Daria Czarnewik: engineering officer, (4 inż pod nią)
* Alezja Dumorin: eks-kapitan, Orbiter, 
    * ENCAO:  +0--0 |Amoralna, skuteczna| VALS: Hedonism, Face, Power| DRIVE: Wędrowny mistrz Ryu
* Władawiec Diakon: pierwszy oficer, tien, (p.o. Stefana) 
    * ENCAO:  +-0-0 |Intrygancki;;Żywy wulkan| VALS: Hedonism, Self-direction| DRIVE: Follow My Dreams, Korupcja anioła
* Grażyna Burgacz: logistyka i sprzęt (officer), tien (3 osoby pod nią) 
    * ENCAO:  -0+-- |Powściągliwa i 'nudna';;Ascetyczna| VALS: Humility, Tradition| DRIVE: Starszy Brat
* Arnulf Perikas: fabrykacja i produkcja (officer), tien (2 inż pod nim) 
    * ENCAO:  0-0-- |Hardheaded;;Napuszony| VALS: Tradition, Family| DRIVE: Wzbudzenie zachwytu
* Maja Samszar: comms officer, tien (p.o. Klarysy jak K. nie może) 
    * ENCAO:  0+-0- |Moralizatorska| VALS: Stimulation, Conformity >> Power| DRIVE: Długi
* Kajetan Kircznik: medical officer (5 pod nim) (czarny, afro, paw, augmentacje bio)
    * ENCAO:  +-0+0 |Anarchistyczny;;Stabilny emocjonalnie;;Z poczuciem humoru| VALS: Self-direction >> Power| DRIVE: Jestem unikalny

##### 1.2. Operacja

* Klarysa Jirnik: artillery, (p.o. Mai jak Maja nie może) 
    * ENCAO:  00+-- |Wymagający;;Małostkowy| VALS: Tradition, Conformity|DRIVE: Sprawiedliwość
* Tomasz Ruppok: starszy mat (13 osób + 2 starszych)
    * ENCAO:  +0--- |Mało punktualny;;Kłótliwy;;W przejaskrawiony sposób okazuje uczucia| VALS: Hedonism >> Achievement, Family| DRIVE: Apokalipta
* Rufus Warkoczyk: starszy mat (13 osób + 2 starszych)
    * ENCAO:  0-+-0 |Zarozumiały;;Bezkompromisowy, niemożliwy do zatrzymania;;Zorganizowany| VALS: Self-direction, Power| DRIVE: Odkrycie konspiracji ("ktoś nas sabotuje")
* Marcelina Trzęsiel: inżynier syntezy (pod Arnulfem), pod opieką tien Terienaka
    * ENCAO:  0+0-+ |Małostkowa;;Dramatic shifts in mood;;Różnorodność form| VALS: Hedonism, Achievement >> Security| DRIVE: "Co za tą górą"
* Elena Verlen: pilot / advancer

##### 1.3. Infiltracja / Starcie

* Gerwazy Kircznik: sierżant marine, Orbiter
    * ENCAO:  +0+-0 |Głośny;;Honorowy| VALS: Humility, Face| DRIVE: Lokalny społecznik
* Erwin Pies: kapral marine, Orbiter
    * ENCAO:  -0-+- |Skryty;;Wiecznie zagubiony;;Praktyczny| VALS: Tradition, Stimulation >> Power| DRIVE: Ochraniać słabszych
* Szymon Wanad: marine, Orbiter
* Marcel Kulgard: marine, Orbiter, stary znajomy Arianny
    * ENCAO:  000-+ |Demotywuje innych;;Innowacyjny| VALS: Achievement, Face| DRIVE: Zbudować legacy
* Tomasz Dojnicz: marine, Orbiter
    * ENCAO:  --0+0 |Nie znosi być w centrum uwagi;;Dusza towarzystwa| VALS: Conformity, Hedonism >> Power| DRIVE: Arlekin Maytag
* Szczepan Myrczek: advancer, tien, seilita,
    * ENCAO:  --+0- |Stabilny emocjonalnie;;Zawsze bardzo zajęty| VALS: Face, Achievement, Hedonism >> Self-direction| DRIVE: Zasady są święte
* Mariusz Bulterier: advancer, tien, seilita, sybrianin 
    * ENCAO:  0-+-- |Prostolinijny i otwarty;;Nie kłania się nikomu;;Uroczysty i poważny| VALS: Humility, Tradition| DRIVE: Nawracanie
* Hubert Kerwelenios: advancer, sarderyta
    * ENCAO:  -0+-0 |Kontemplacyjny, refleksyjny;;Kompleks paladyna| VALS: Humility, Tradition >> Stimulation| DRIVE: Odbudowa i odnowa

##### 1.4. Other

* Ellarina Samarintael: Egzotyczna Piękność
    * ENCAO:  +-00+ | Spontaniczna;; Chipper;; | VALS: Power, Hedonism >> Family | DRIVE: Wyciągnąć kogoś z bagna, dobrze wyjść za mąż

#### 2. Athamarein, ciężka korweta rakietowa

* Leszek Kurzmin: dowódca jednostki (atarienin defensor) TACTICAL NERD 
    * ENCAO: 0-+++ | Żyje według własnych przekonań;; Lojalny i proaktywny| VALS: Benevolence, Humility >> Face| DRIVE: Właściwe miejsce w rzeczywistości.
* Alan Nierkamin: eks-lokalny przewodnik, advancer
    * ENCAO: ++-0- |Nietolerancyjny, musi być tak jak uważa;;Dokładnie przemyśli wszystko zanim coś powie| VALS: Power, Achievement >> Tradition| DRIVE: wprowadzi Orbiter na Nonarion
* Aleksander Warmańczyk: advancer elite
    * ENCAO:  0-00+ |Stabilny emocjonalnie;;Przesądny (Salazar wygrywa)| VALS: Face, Hedonism >> Security| DRIVE: Powszechna sława

#### 3. Loricatus, ciężka fregata szturmowa

* Salazar Bolza: komodor (drakolita ewolucjonista; misja: założyć placówkę badawczą Anomalii), INHUMAN (bioforma typ serpentis, ale mniej) model after Lorca
    * ENCAO:  --+0+ |Racjonalny;;Powściągliwy, skryty i wyważony;;Surowy, wymagający| VALS: Power, Achievement >> Security, Stimulation| DRIVE: Survival of the fittest
* Kalia Szantik: dowódca jednostki (atarienin ekspansjonista; misja: zająć teren dla Orbitera); przekonana, że Salazar jest zdrajcą
    * ENCAO:  -0+0- |Bezbarwna, przezroczysta;;Nie lubi abstrakcji;;Nie odracza| VALS: Family, Conformity, Power >> Face, Tradition| DRIVE: Harmonia Orbitera

#### 4. Nonarion Nadziei (stacja cywilna)

* Leo Kasztop: sprzedawca sekretów na Nonarionie (atarienin)
    * ENCAO:  0-+00 | Intrygancki, polityka;;Nie odracza| VALS: Self-direction >> Stimulation, Tradition| DRIVE: Wygrać w rywalizacji)
* .Franciszek, enforcer Nonariona (atarienin)
    * ENCAO:  +00-- | Lubi rutynę;;Uszczypliwy i zgryźliwy;;Lubi wyzwania | VALS: Achievement >> Security| DRIVE: Komfortowe życie
* .Wojciech, agent Aureliona (faeril)
    * ENCAO:  ---+0 |Bezbarwny, przezroczysty;;Niemożliwy do ruszenia| VALS: Power, Humility >> Tradition | DRIVE: Supremacja Aureliona

### Theme & Vision

* Kampania
    * Problemy kulturowe
        * Orbiter nie respektuje tego co wypracował Nonarion
        * Ten teren ma wielu noktian i są traktowani jako zasób
        * Ten teren ma własną, zupełnie obcą kulturę
    * Daria nie ma łatwego powrotu
    * Władawiec corruptuje Elenę
* Ta sesja
    * Destructor Animarum i obca kultura.

### Co się wydarzyło KIEDYŚ

* ?

### Co się wydarzyło TERAZ (what happened LATELY / NOW)

* 

### Co się stanie (what will happen)

* S0: Elena "to było niehonorowe, Arianno! Gdyby rodzice o tym słyszeli!"
* S0: Spotkanie z komodorem Salazarem Bolzą. Bolza stawia problem "atakują kralotyczni kultyści, nieprzytomni: ja, Kurzmin, Elena. Kogo ratujesz."
* S1: Ślady Kaspiana Certisariusa na resztkach TKO-4271 (Egzorcyzm Orbitera) PLUS tysiące sygnałów PLUS squatterzy.
* S2: Ellarina robi nagranie; Władawiec i Elena tańczą z szablami
    * Khachaturian: Sabre Dance, pojedynek, ale symulowany. To jest TANIEC a nie walka. Władawiec pokazuje Elenie jak robić piękno
        * Oklaski. Elena jest _szczęśliwa_
* S3: Prośba o pomoc z TKO-2292 (kompleks HRowy (więzienno-resocjalizacyjno-niewolniczo-medyczno-sztuki))
* S4: Destructor Animarum

### Sukces graczy (when you win)

* ?

## Sesja właściwa
### Wydarzenia przedsesjowe

.

### SPECJALNE ZASADY SESJI

.

### Scena Zero - impl

Po pierwsze, wycofaliście się. Lodowiec odciągnął oddział, wezwaliście transportowiec itp. Zainstalowała się koło Dominy Lucis ("strefa duchów") - nowa nazwa "Obszar Upiorów Orbitera".

Orbiter wycofuje się w chwale. Udało się Orbiterowi "uratować" 200 istnień noktiańskich.

Arianna dostała **medal za bezkrwawe pokonanie dominujących sił wroga**.

Sprawa trochę się rozwiązała to Arianna miała w kwaterze Elenę. Która przyszła z oficjalnym spotkaniem - ona nie chciała "jako kuzynka" tylko "jako oficer". Jest dystansowanie się. Elena wyszła bardzo prosto 

* E: "jak mogłaś - co by rodzice powiedzieli?!"
* A: "najważniejsze jest ochronić ludzi"
* E: "stworzyłaś..."
* A: "Zarralea wyglądała trochę upiornie, ale to największy odchył. Hologram nekrotycznej TAI..."
* E: "nie wiedziałam że istnieje TERMIN nekrotycznej TAI - zrobiłaś z niej niewolnika który PRAGNIE Ci służyć. To najgorsze..."
* A: "Jak każda TAI w statkach Orbitera. Obwarowane funkcjami by pragnęły służyć. Skrzywdziłam TĄ TAI, ale stworzyłam ją na nowa, poprzednia zginęła wcześniej"
* E: "te wszystkie samobójstwa. Ci noktianie woleli zginąć zamiast się poddać. Nie jestem miłośniczką noktian, nie lubię ich (chyba - lekkie zawahanie), ale bez przesady". To było coś niehonorowego.
* A: "Najbardziej mnie bolą te samobójstwa, liczyłam, że Zarralea to zatrzyma, ale nie da się tego zrobić tak, by było dobrze."
* E: "Orbiter to pochwala." (wyraźny jad) "Orbiter nie powinien tego pochwalać. To było ZŁE. Verlenowie NISZCZĄ potwory, Ty ZROBIŁAŚ potwora. I Orbiter to pochwalił! Miałyśmy pokazać Orbiterowi jak to robić dobrze, nie prowadzić Orbiter w drogę degeneracji!"
* A: "Wiesz Eleno, wszyscy co chcieli się poddać się poddali. Zginęło maksymalnie 30 osób, a tak to zginęliby cywile i nasi. Poza tym nikt z naszych się nie spodziewał że Zarralea stanie się tym czym się stała. Zawsze jak brałam się za technologię to obudzić skuter by sam jeździł. I nie było żadnego problemu. Kto mógł się spodziewać. Plus, sama wiesz, że magia nie zawsze nie działa jak chcemy a skoro stworzyliśmy to możemy musieć to wykorzystać."

TrZ (zaufanie) +3+3Or:

* Vz: Zaufanie Eleny do Arianny sprawia, że Elenę to boli ale ufa, że Arianna wie lepiej.
* X: Dla Eleny jest to niewłaściwe. Nie ma riftu między Arianną i Eleną, ale "ja bym tak nie zrobiła" - nie umie tego obronić.
* V: Elena rozumie, że Arianna ma inne spojrzenie na honor i dla Arianny honorowym jest chronić życie. A dla Eleny - robić rzeczy jak należy.
* V: Elena jest USPOKOJONA z perspektywy Orbitera. Arianna chce chronić Elenę i dać jej jakieś oparcie. 

Arianna jest zaproszona, jeszcze na K1, na spotkanie z nowym komodorem do jednej z taktycznych sal. Zanim Arianna się tam udaje - sprawdzić po oficjalnych rekordach co o nim jest. 

Tr Z (uprawnienia + wspomagające TAI) +3:

* X: komodor Bolza postawił _request for information_ jak ktoś szuka. Więc się dowie.
* V: Bolza ma:
    * dużą ilość przeniesień. Ludzie od niego uciekają, ale ci co zostają SERIO zostają. Ma opinię bardzo surowego.
    * typy misji - jeśli ma doświadczonych weteranów, to oni zwykle Bolzę chwalą. Jeśli ma jednostki "niedopasowane" to one z Bolzą zostają. Jednostki wrażliwe uciekają. Bolza każdemu daje szansę ale każdego sprawdza jak daleko zanim się złamie.
    * mówią na niego "serpentis"
    * na tą misję "Loricatus" dostaje nowego kapitana
    * Bolza ma duże straty na misjach wśród rekrutów, ale bardzo małe wśród weteranów
    * Bolza dowodził operacjami trudnymi i łatwymi. Na wszystkich ma straty wśród rekrutów. Potrafi sztucznie utrudnić misje by ich sprawdzić.
    * kill ratio u wrogów? Nie jest krwawy. Akceptuje krwawość. Jak się nie poddali, miał 100% kills.
    * Bolza _dobrze_ współpracuje z: Orbiterem, noktianami, Neikatis, WSZYSTKIM.
    * Z perspektywy plotek: "to jakby zamknąć TAI w ciele serpentisa"
    * Też bierze pod uwagę zdanie swoich oficerów.

Przyciemniona hala gdzie są symulacje normalnie. Wszędzie projekcje jakichś planów jednostek - Arianna poznaje plany Athamarein oraz Flary i nowej jednostki "Loricatus" (jednostka w stylu noktiańskim). Sam komodor wygląda interesująco. Podobny do serpentisa - zielone ciało i niebieskie oczy. I Salazar ma naprawdę, naprawdę piękne oczy w ciele zaprojektowanym do walki i zastraszania. I nienagannie nosi mundur - każdy ruch to elegancja.

* B: "Kapitan Verlen, to będzie przyjemność."
* A: "Dla mnie również, komodorze"
* B: "Czy masz jakieś pytania na które nie odpowiedziały dokumenty?"
* A: "Poprzedni kapitan czemu zrezygnował?"
* B: "Został przeniesiony przez Orbiter. Orbiter uznał, że kapitan Kalia Szantik będzie lepszym dowódcą na tej operacji. Mam zmianę 60% załogi, która nie wyraziła zadowolenia z tego przeniesienia."
* A: "Czyli lepsza załoga na ten teren? Zupełnie słusznie."
* B: "Zdecydowanie, pani kapitan. Mam też jedno pytanie, jeśli mogę."

Bolza włączył symulację. Jesteś na mostku Athamarein. Korweta jest w ruinie, wyraźnie zniszczona. Loricatus jest zniszczona, Arianna widzi na radarze. Flara jest uszkodzona, ale wystarczająco sprawna. Do Athamarein zbliżają się 3 fregaty Kultu Ośmiornicy. Arianna ma dookoła siebie rannych, ale niezdolnych do poruszania się. Jej moc magiczna jest osłabiona. 3 osoby rzucają się Ariannie w oczy - nieprzytomny komodor Bolza, trudny do wydostania. Ranna i nie do końca kontaktująca Elena, ale da się ją postawić. I Kurzmin który jest przy drzwiach, też da się go wydostać. Bolza zadaje pytanie: "kogo z tej trójki ratujesz i co zamierzasz zrobić?" 

* Arianna: "Kurzmin, tylko on wyciągnie nas z tego planem. Nie znam pana, a Leszkowi ufam."
* Bolza: "Przy założeniu, że doprowadzisz Kurzmina do działania w 10 minut - jaki plan jest w stanie wymyśleć w tej sytuacji?"
* Arianna: "Odstrzelić część silników by wyprawić statek..."
* Bolza: "Zdałaś test. Chcę Cię na tej misji. Masz jakieś pytania?"
* Arianna: "Czym bym nie zdała testu?"
* Bolza: "Najgorsza odpowiedź to 'strzelam i walczę'. Druga najgorsza to ratowanie mnie. Dlatego, bo w tej sytuacji jestem bezużyteczny. Jestem zablokowany i zajmie to za dużo czasu. Czy domyślasz się, co powinnaś zrobić w takiej sytuacji jeśli jest beznadziejna?"
* Arianna: "Ewakuować kogo się da na Flarę i wysadzić Athamarein w kultystów?"
* Bolza: "Dobrze. Dobrze myślisz. Wyciągasz Elenę. Egzekucja Kurzmina i mnie. Samozniszczenie Athamareina przez przesterowanie rdzenia. Autopilot na tamte jednostki jeśli się da. Wy dwie na Flarę. Uciekacie i ratujecie Flarę - jedyne co się da. Wszystko inne niszczysz jeśli się da. W innym wypadku Kult dostaje Kurzmina i potencjalnie mnie. Nie chcesz tego."

Arianna daje załodze szansę by się wycofali z misji. Arianna ma samych rekrutów. Arianna "wiejcie jak chcecie!" Załoga "kapitan Verlen damy radę! Sukcesy! I mamy Egzotyczną Piękność!!!" Arianna JUŻ widzi, że będzie się działo... Arianna prosi Ruppoka, by jak co można było się tym wspomóc.

* Ruppok: "Pani kapitan... mam przygotować psychoaktywne środki?"
* Arianna: "Tak, mam nadzieję, że nie będą potrzebne, ale sektor jest niebezpieczny a mamy mikroskopijną garstkę marines. Jakby serpentis wdarł się na pokład..."
* Ruppok: "Pani kapitan, nieczęsto to mówię, naprawdę nieczęsto. Ale serpentis by się tu nie dostał. Nie przy pani kapitan i pani umiejętnościach" /salut

Arianna autentycznie jest bohaterką Flary - Ellarina rozpuściła prawdę, że Lodowiec chciał źle a Arianna zrobiła dobrze i łagodnie. Więc morale Flary jest bardzo wysokie.

### Sesja Właściwa - impl

Po tych 2x dniach Flara, Athamarein i Loricatus docierają do "Obszaru Upiorów Orbitera". I jak się zbliżacie - Daria -> Arianna ostrzega, że najpewniej jako, że zostawili niechronioną i bezpieczną bazę to najpewniej będą tam squatterzy. Planetoida TKO-4271 ma Zarraleę. ALE oprócz tego jest Kazmirian która była kupiona. I tam na pewno ktoś będzie. Były systemy zamontowane. Anomalia Kolapsu ma statki które mają systemy więc zostawiliście bazę "na szybko" grupie crackerów. I dobrze by było skorzystać z okazji i pokojowo wywalić ludzi stamtąd.

Arianna chce to zrobić pokojowo, co Darię cieszy.

* A: Komodorze Bolza?
* B: Słucham?
* A: Kiedy opuszczaliśmy teren zostawiliśmy mało strzeżoną bazę, najpewniej zalęgli się tam scavengerzy.
* B: Co proponujesz? /lekki uśmiech na samej twarzy
* A: Może wykurzmy ich bezkrwawo, włammy się do systemów i wykurzmy systemami np. podtrzymywania życia
* B: Co przyniosłoby nam NAJWIĘKSZĄ korzyść na tym terenie?
* A: Wymiana handlowa, nie możemy wydobywać wszędzie wszystkiego.

Arianna zgodziła się z Bolzą, że demonstracja siły i wypuszczenie to lepsza opcja. Arianna chce to wziąć dla siebie - tak lepiej wyjdzie bo ma Darię. Arianna -> L.Kurzmin, G.Kircznik, Daria by to przegadać.

* L: kapitan Verlen, to przyjemność. (lekki smutek na jego twarzy - poznał już komodora)
* A: kapitanie Kurzmin, potrzebna mi pańska rada wykurzenia scavengerów.
* L: jeśli zrozumiałem dobrze, próbujemy nikomu nie zrobić krzywdy i zmaksymalizować... bojaźń, tak jakby.
* A: weszli na nasz teren i nie wchodzi się bezkarnie na teren Orbitera
* L: proponuję coś co naszemu komodorowi się może spodobać - jeśli tu weszli są kompetentni. Jeśli nie mają wyjątkowo plugawej moralności, możemy im tą bazę oddać. Żeby tak jakby... pracowali dla nas.
* A: potencjalnie dobry pomysł, ale komunikat w świat że Orbitera się nie okrada. Bo inaczej nie będziemy mieli dobrych kontaktów handlowych.
* D: czemu nie połączyć - sygnał 'Orbitera się nie okrada' i pozwolić im działać z tej bazy to operacja odbicia bazy z założeniem, że nie wolno nikogo skrzywdzić a tych którzy się wykażą czymkolwiek w tej operacji - zdali test i mogą bazę przejąć. 
    * "Zostawiliśmy tą bazę specjalnie bez ochrony" (A+D)
* L: (zaświeciły oczy) To nie będzie trudne i nawet w to uwierzą po tym co ostatnio. 
* A: mhm
* L: czyli to nie operacja bojowa a propagandowa.
* D: chcemy odbić stację i pokazać że możemy odbić stację.
* L: czyli chcemy użyć advancerów w Lancerach
* G: mamy Lancery dla advancerów?
* D: mamy Flarę
* G: przepraszam, nie pomyślałem.
* L: mamy coś lepszego. Mamy cztery entropiki. Są na Loricatus. Entropik może technicznie wszystko tankować.
* A: kto może się sprzęc?
* L: mam bardzo dobrego advancera, jednego z najlepszych; Aleksander Warmańczyk, on się nada. Dołączył na Athamarein idąc za komodorem Bolzą. Weteran.
    * L->A na priv: "jest przesądny. Uważa, że gdzie Salazar Bolza tam śmierć go nie spotka. TEN KOMODOR JEST AMULETEM PRZYNOSZĄCYM SZCZĘŚCIE."
* A: ja mam Huberta, też się nada.

PLAN JEST PRAWIE GOTOWY.

Trzy Lancery ze snajperkami się rozstawiają by odciąć ucieczkę. Dwa entropiki szturmują, coś zniszczą ale nikogo nie krzywdzą. Plus wiadomość komponowana przez Darię i Ellarinę. Plus, Daria proponuje - zna bazę, wiadomo ile ich tam jest wśród squatterów. Więc pierwszy plan - dojść do tego KTO tam jest.

* Maja -> Arianna: "Czy to jest dobry pomysł, by tym lokalsom... w ogóle, oni są... no, brudni, jakby powiedzieć. Ale nie podoba mi się to co... no, to zabijanie noktian."
* Arianna -> Maja: "Zmartwiłabym się jakby Ci się spodobało zabijanie noktian"
* Maja: (słaby uśmiech): "Czy dajemy im bazę by im pomóc?"
* Arianna: "Tak naprawdę przybyliśmy tu by im pomóc. Chcemy dać im bazę by dać lepszy kontakt z miejscowymi i im pomóc w lepszy sposób. Pomoc grupie jest miłym bonusem."
* Maja: "Nie zawiodę. To w sumie nasza misja, jak w Aurum. Pomagamy innym."

Maja spina się z systemami Kazmirian. Używa retransmiterów z Athamarein.

Ex (bo Maja) Z (bo ma dobry sprzęt) +3 (bo wie co robi) +3Or (dwustronny kontakt):

* Vz: Mai udało się połączyć z Kazmirian pasywnie:
    * na miejscu są osoby, które dały radę się dostać, ale niespecjalnie ingerowały w bazę. Baza ma dalej IFF Orbitera
    * Maja jest PEWNA że sygnały z lifesupport itp pokazują rajd na lodówkę i dobre utrzymywanie bazy
    * Daria sproawdziła to na ziemię - "najpewniej nie umieli"
    * system defensywny jest wyłączony
* V: Maja odzyskała kontrolę nad bazą. Flara steruje planetoidą Kazmirian. TAI Semla wróciła do działania.
    * "oni próbowali shackować system. Nie udało im się. Ale był błąd konstrukcyjny i Semli zabrakło energii i się wyłączyła. Bo mieliśmy wrócić a nie wróciliśmy"
    * to 24 osoby. Lekkie uzbrojenie. Najstarszy z nich dowodzi. Ma na oko 25 mniej więcej lat. Są tam też niepełnoletni.
        * to młodziaki...
    * oni się tam dostali przy nieaktywnej Semli PONIEWAŻ BYŁ BŁĄD KONSTRUKCJI BAZY. Gdyby nie to, oni by zostali zestrzeleni.
    * ich statek się rozbił. To był wrak. Cudem tu doleciał.
    * Daria poznała dowódcę tej bazy to Sargon Niiris. Sargon wyraźnie próbuje opiekować się młodymi.
    * Jeszcze nie zauważyli, że Semla jest aktywna.

Rozmowy jakie są:

* Sargon próbuje chronić tych ludzi. Z pomocą udało mu się ewakuować i uciec z grupą osób. Ale nie przemyślał. I skończyli tu.
* Sargon miał nadzieję na podejście wrakiem i przedostanie się w skafandrach. Ten plan miał 0% powodzenia. Ale on nie wiedział tylko miał szczęście.
* Wszystkie osoby tutaj to eks-niewolnicy lub osoby bez szczęścia, zdesperowane.
* Sargon dowodzi nie bo ma skille a bo jest jedyny co mówi co robić.

Z tego wynika:

* Daria: "Semla może powiedzieć Sargonowi jak żyć, by pokazał kompetencję. Podpadł Orbiterowi i Orbiter wraca. Wpuściłam Was bo ideały Orbitera to wolność, dbanie o ludzi..."
* Arianna: "Możemy przekonać że mamy ideały których się trzymamy jako Orbiter i jednym z nich jest pomaganie ludziom. Nie przybyliśmy tu robiąc rzeczy jak Lodowiec."
* Kurzmin: "Możemy zmienić stosunek tlenu do azotu, powyłączać ich a potem wejdziemy i zrobimy łagodnie."

Oki, zrobione. Maja przekazała Semli co i jak.

Bolza spytał, czemu poszło im tak dobrze w zdobywaniu bazy. Arianna przyznała - gdy opuszczali teren w dużym pośpiechu część systemów nie była postawiona odpowiednio i defensywa bazy nie zadziałała. Grupka młodych ludzi przeszła przez defensywę i weszła w squat. Bolza ma minę REALLY. Czy Arianna jest skłonna skomentować tą sytuację? Arianna - "by poradzić sobie z anomalią to robiliśmy na szybko... priorytety. A potem komodorowi bardzo się spieszyło z duchami."

* Bolza ma nieruchome oblicze. "Dobrze zatem, że to się nie powtórzy. Nie chwalcie się tym... epizodem. Orbiter... nie doceniłby."
* Arianna: "Łatwo przejęliśmy bazę, ktoś inny mógł i być większym zagrożeniem"
* Bolza: "Czy ci ludzie nadają się, by przekazać im bazę?"
* Arianna: "Niespecjalnie"
* Bolza: "Co zatem możemy z nimi zrobić zamiast po prostu wyrzucić w kosmos?" /nieruchome oblicze
* Arianna: "Nawiązać z nimi kontakt i szkolić ich. Postawić placówkę która będzie w stanie zaangażować się gdzieś indziej."
* Bolza: "Indoktrynacja młodych umysłów oraz nauczenie ich przydatnych umiejętności ORAZ poprawienie statusu Orbitera. A wszystko niewielkim kosztem."
* Arianna: "Tak."
* Bolza: "Trafią do naszej głównej bazy. Tam ich podzielimy pod kątem użyteczności, ale wszyscy mogą zostać."
* Arianna: "Tak."
* Bolza: "Tak zatem zrobimy."

Reanimowana Semla, Flara dostarczyła energii na kilka dni, więc jest czas się tym zająć i odbudować. I nasza wesoła ekipa poleciała do "Strefy Upiorów Orbitera". 

Maja jeszcze znalazła retransmisje z tego co mówił Lodowiec (przemowa o nekroTAI) z komentarzami propagandowymi. To się stało "nie ufajcie Orbiterowi". Użyte w propagandzie anty-Orbiterowej. Noktianom rekomenduje się samobójstwo zanim wpadną w ręce Orbitera. A wśród młodych tutaj w rozmowach faktycznie był strach "a co jak wrócą". Nie bo squatują a bo Orbiter. Oni to łyknęli. Bolza, gdy dowiedział się o tym, stwierdził, że poprawa PR jest jedną z najważniejszych rzeczy.

Gdy dotarliście do planetoidy TKO-4271, na miejscu Arianna poczuła BRAK Zarralei. Krótki skan wykazał, że Zarralea jest zniszczona. Baza nie została opróżniona z niczego, po prostu Zarralea jest zniszczona. Użyto broni snajperskiej srebrnej superdalekosiężnej z Lancera. Wszystko wskazuje na charakterystykę ataku noktiańskiego. Poza tym - baza jest stabilna.

* L -> A: "Nie sądzę, że to byli noktianie"
* A: "Ok? Czemu tak uważasz?"
* L: "Bo baza jest nienaruszona. Noktianie by próbowali coś zniszczyć lub zabrać."
* A: "Ktoś może chcieć byśmy zapałali nienawiścią do Noctis?"
* L: "To mogą być nasi. Nasi się boją anomalnych TAI. Plus, to był fatalny PR. To coś co... wiesz, robiłem kiedyś. Nie tu. Bolza mógłby to rozkazać."
* A: "Rozumiem"
* L: "Oczywiście, nie mam dowodów... ale rozumiesz."
* A: "Najsensowniejsza opcja. I jeszcze strzelec dokładnie wiedział gdzie strzelać."
* L: "I wiedział DOKŁADNIE jaka jak charakterystyka Zarralei."
* L: "(po chwili milczenia) W sumie nie wiem czemu Ci to mówię. (wyraźnie jest mu głupio)"
* A: "Wdzięczna, że to powiedziałeś, sama bym nie wpadła."
* L: "W sumie dobrze. Bo wiesz, że nikogo nie należy o to winić. Ale nie była to zła TAI. Ja się jej nie bałem. (cisza) Spytasz komodora czy za tym stał czy wolisz nie wiedzieć?"
* A: "Spytam, jestem ciekawa co odpowie."

Ellarina próbowała nie pokazać po sobie, ale kamień spadł jej z serca. Ale próbowała nie pokazać że ją to martwiło.

Ta baza musi mieć TAI. Więc niechętnie, ale komodor wykorzystał TAI Loricatusa by utrzymać bazę. Loricatus nie ma pełnej potrzeby do działania. I zamówił porządną Persi z K1; ma się pojawić w ciągu tygodnia.

Arianna konsultuje się z komodorem w sprawie zniszczonej Zarralei.

* A: Podejrzewam, że TAI nie została zlikwidowana przez noktian. Czy pan był w to zamieszany?
* B: Czy uwierzysz jak Ci odpowiem?
* A: Tak.
* B: Czemu?
* A: Nie mam powodu by Panu nie ufać.
* B: Tak, zleciłem zadanie zniszczenia anomalnej TAI. Zniszczenie zostało zakończone z powodzeniem. Mniej więcej dzień przed naszym przybyciem tutaj.
* A: Rozumiem.
* B: Komentarz?
* A: Byłabym wdzięczna jeżeli do nas trafiały informacje związane z naszą misją, moglibyśmy się przygotować że trzeba podmienić TAI. I nie byłoby niepotrzebnych wątpliwości.
* B: Awaryjna TAI powinna tu już być.
* A: Nie dotarła jeszcze?
* B: Nie. A byłem poinformowany, że będzie na miejscu. Nie ukrywam przed Wami informacji które są dla Was istotne.
* B: Nie wiem czemu tej TAI tu nie ma. Będę... zainteresowany tym tematem. Ale najpierw musimy doprowadzić tą bazę do działania.

Bolza jest niezadowolony, ale żeby baza działała to podzielił się z bazą mocą Persefony d'Loricatus. To sprawia, że Loricatus nie jest w pełni aktywną fregatą, ale powinno być wystarczająco dobrze; jeśli coś się zbliży do tej przestrzeni to Loricatus ma dość siły by sobie ze wszystkim poradzić. Po prostu nie może odlecieć.

Przeszukując bazę Arianna jest informowana co i jak. Serpentis nie dał rady zniszczyć wszystkiego. Zniszczył niektóre rzeczy, zwłaszcza militarne supersekrety np. komorę w której się znajdował, ale nie dał rady lub nie chciał zniszczyć komputerów dokładnie. Więc jest dużo co da się zrobić by wykorzystać Dominę Lucis. Serio sukces był duży. Do tego zostało dużo... osobistych drobiazgów, elementów, rejestr załogi... Są też pomieszczenia gdzie bez problemu umieści się Sargona i resztę młodych. --> zadanie dla Grażyny.

Daria dotarła do interesującej informacji. Daria patrzy na rejestr załogi z grubsza, ale też na samą Dominę Lucis. Daria zapierdziela blueprinty. Macie zapasów jak cholera - thank you Noctis. Ale Darii rzuciło się w oczy coś istotnego. Imię. Kaspian Certisarius. Advancer z Hiyori. I na pewno Kaspian nie został "uratowany". Ale jego ciała też tu nie było. I wszystko wskazuje na to, że Kaspian żyje. I nie było go tu w chwili ataku.

Jeśli nie wróci - albo opuścił ten teren, albo zginął, albo misja odesłania ludzi - więc jest jeszcze jedna baza. Jakaś awaryjna. Na razie Daria nie informuje o tym Arianny, zachowuje wszystko dla siebie. Daria musi wiedzieć jak zachowuje się ten komodor. Jeśli Arianna o czymś nie wie - nie musi o tym mówić komodorowi :D.

Arianno, zostałaś poproszona przez Ellarinę.

* E: pani kapitan?
* A: tak, E?
* E: czy mogę przeprowadzić występ który będzie efektowny i będzie podnosić morale i ogólnie pokaże kompetencję oficerów Flary? Wszystko jest przygotowane.
* A: nie zajmujesz się tym generalnie na pokładzie statku?
* E: teoretycznie tak, ale to będzie nagrane oraz będzie... wolałabym mieć zgodę pani kapitan. Z pewnych przyczyn.
* A: jeśli to wymaga zgody to przedstaw mi skrypt.
* E: to taniec. Z szablami. Choreografia. Bardzo efektowne. Zero G. Muzyka, światła, wszystko. Dwóch tancerzy. (zawiesiła) Warte uwiecznienia.
* A: kto jest tym drugim?
* E: (pokręciła żywiołowo głową) nie nie nie, ja nie tańczę, ja jestem na wokalu.
* A: kto tańczy?
* E: pierwszy oficer Władawiec Diakon oraz advancer Elena Verlen
* A: dobrze, udzielam zgody
* E: po prostu chciałam się upewnić, że wszystko będzie w porządku. Skrypt jest... niebezpieczny, ale upewnili mnie, że nie będzie problemów i na próbach też nie było.

WIECZÓR.

Największa sala treningowa z holoprojekcjami. Wszędzie rozstawione kamery. Zaczyna się muzyka. Wychodzą do pojedynku dwie osoby z ostrymi rapierami. Zarówno Władawiec jak i Elena mają opaski na oczach i mundury paradne. Tylko dzięki omnidetekcji.

Ex (stopień trudności jest niemożliwy) Z (długo ćwiczyli) +3 +5Or (rany) +5Og (stres):

* V: taniec fundamentalnie działa. Niekoniecznie skończą w dobrym stanie czy wyjdzie im idealnie, ale osiągnęli sukces.
* X: nieperfekcyjny taniec, nieperfekcyjne rany, lekkie pocięcia
* X: plotka, że nie zależało jej na tańcu tylko na Władawcu i dlatego nie poszło idealnie. On zrobił dla niej wszystko a ona dla niego nie, on włożył więcej.
* V: bardzo efektowny taniec. To jest coś pokazującego mistrzostwo w inny sposób.
    * Ellarina pracowała nad choreografią
    * Elena potrafi się adaptować nawet bez percepcji, ona UMIE kompensować i działać zespołowo
    * Władawiec potrafił to skoordynować i umie jej oddać kontrolę
* Og: stres zżarł Elenę i Władawiec musiał ją uratować. Przejął dowodzenie, zmienili strukturę na awaryjną i wrócili; Władawiec jest ranny i krwawi.
* Or: następnym razem to on skrzywdził Elenę, bo tak próbowała go nie uszkodzić; znowu zrobili błąd. Elena zacisnęła zęby i kontynuuje.
* X: Ellarina skończyła. TWARDE stop. Nie ma na to żołądka. MOŻE dałoby się to uratować ale ona nie chce, już nie, nie takim kosztem, oni - w jej oczach - są szaleni.
* Vz: Aplauz jest niesamowity. Elena - widać - jest szczęśliwa. I UCIEKŁA.

Kajetan Kircznik szybko pomaga Władawcowi; on i Elena zostawiają solidne ślady krwi. Kajetan wysyła wiadomość na priv do Arianny - będzie mógł pomóc medycznie Elenie ale Elena absolutnie nie chce z Arianną rozmawiać teraz bo się wstydzi. Kircznik obiecał Elenie, że póki jest pod jego opieką to ABSOLUTNIE Arianna nie będzie z nią mogła rozmawiać, ale obiecał szybko Ariannie, że zwolni Elenę asap z opieki medycznej ;-).

## Streszczenie

Po "uratowaniu" noktian z Dominy Lucis Arianna dostała medal a Lodowca zastąpił komodor Bolza, zimny drakolita. Gdy grupa wydzielona wróciła na teren Anomalii Kolapsu okazało się, że planetoida Kazmirian ma squatterów; Zespół skutecznie ich przejął odzyskując kontrolę nad systemami Semli d'Kazmirian. Na miejscu - TKO-4271 - okazało się że Zarralea jest zniszczona. Orbiter odzyskał swoją bazę, acz Loricatis musi współdzielić Persefonę. Z innej beczki: link Elena - Władawiec się pięknie rozwija.

## Progresja

* Arianna Verlen: dostała od Orbitera medal za bezkrwawe pokonanie dominujących sił wroga (dzięki operacji z Zarraleą)
* Arianna Verlen: bohaterka Astralnej Flary. Dzięki Ellarinie i jej propagandzie jest lubiana i podziwiana przez załogę Flary mimo jej nie najwyższej kompetencji.
* Elena Verlen: nadal uważa, że Arianna wie lepiej i zrobiła co powinna, ale wie już że Arianna inaczej postrzega to co ważne niż ona - dla Eleny liczy się 'działanie prawidłowo', dla Arianny 'jak najwięcej żyć które przetrwają'. Zaczyna się rift między nimi. Ale jest uspokojona przez Ariannę odnośnie Orbitera i decyzji Orbitera.
* Gabriel Lodowiec: przeniesiony z okolic Anomalii Kolapsu na inne przestrzenie; ogólnie pochwała za dobrą robotę i silna nagana za bardzo niewłaściwe plotki o Orbiterze w okolicach Anomalii Kolapsu

### Frakcji

* Orbiter: w okolicach Anomalii Kolapsu pojawiły się plotki, że zmuszają noktian do służenia im po śmierci (jak Zarralea); noktianom rekomenduje się samobójstwa i atomizację zwłok by nie trafili w ręce Orbitera

## Zasługi

* Arianna Verlen: gasiła pożar po stworzeniu Zarralei między sobą i Eleną; nawet medal dostała. Potem poszukała dane na temat nowego komodora (Bolzy). Po zapoznaniu się z Bolzą dała szansę załodze na wycofanie się z misji, ale oni Ariannie wierzą. Chciała wykurzyć squatterów z Kazmirian, ale pod wpływem Bolzy zmieniła plan na sojusz. Niestety, poznawszy squatterów - musiała wstawić się za nich u Bolzy.
* Daria Czarnewik: ostrzegła Ariannę o tym, że najpewniej squatterzy się pojawili w Kazmirian; pomogła opracować plan odstraszania bezkrwawego i sojuszu ze squatterami. Sprzedała to jako 'Orbitera sie nie okrada'. Wstawiła się za Sargonem u Arianny.
* Elena Verlen: skonfrontowała się z Arianną - nie podoba jej się Dark Awakening Zarralei. Straciła trochę wiary w Orbitera, ale Arianna robi co może by Elena te rzeczy utrzymała. Wraz z Władawcem przeprowadziła niesamowity taniec z szablami, po czym uciekła ze wstydu. NAPRAWDĘ jest szczęśliwa podczas tańca.
* Salazar Bolza: komodor Orbitera nad Arianną w okolicach Anomalii Kolapsu w miejscu Lodowca. (((Drakolita ewolucjonista; misja: założyć placówkę badawczą Anomalii. INHUMAN (bioforma typ serpentis, ale mniej) model after Lorca. ENCAO:  --+0+ |Racjonalny;;Powściągliwy, skryty i wyważony;;Surowy, wymagający| VALS: Power, Achievement >> Security, Stimulation| DRIVE: Survival of the fittest.))). Dowiedział się, że Arianna szperała na jego temat i zrobił jej quiz kogo ratować (Kurzmina, Elenę czy jego). Wiecznie testuje wszystkich wobec swoich wysokich standardów. Skłonił Ariannę do myślenia większymi planami - nie 'wykurzyć squatterów' a 'sojusz ze squatterami'. Po zapoznaniu się z tym kim są ludzie z Kazmirian, zmienił na 'triage i nauka i indoktrynacja'. Podjął decyzję o dyskretnej eksterminacji Zarralei.
* Leszek Kurzmin: to on zaproponował sojusz ze squatterami i oddanie im bazy. Twórca modyfikowanego planu jak odzyskać Kazmirian by wywołać największy szok u squatterów. Oddał Ariannie możliwość spięcia z Kazmirian, by jej załoga mogła ćwiczyć (Bolza jest bardzo wymagający). Doszedł do tego, że najpewniej to Bolza stoi za zniszczeniem Zarralei i przekazał informację o tym Ariannie.
* Maja Samszar: spięła się z Kazmirian, ma informacje kto jest w środku i przestawiła lifesupport by wszystkich uśpić i unieszkodliwić. Dobre ćwiczenie retransmitując z Athamarein. Wyjątkowo dobrze jej poszło.
* Sargon Niiris: młody i niedoświadczony dowódca grupy młodzików - uciekinierów z Nonariona i okolic. Zajęli Kazmirian na nieobecność Orbitera mając ogromne szczęście (bo Semla nie była w pełni sprawna). Unieszkodliwiony przez Semlę, trafił pod opiekę Orbitera / Bolzy ze swymi ludźmi. Do Strefy Duchów.
* Ellarina Samarintael: kamień spadł jej z serca ze zniszczeniem Zarralei; zorganizowała wybitne widowisko z Eleną i Władawcem, pokazującym ich mistrzostwo i zaufanie do siebie. Ostrzegła Ariannę o tym widowisku (bardzo nie chce Ariannie podpaść)
* Władawiec Diakon: przekonał Elenę do tańca z szablami i zrobienia epickiego widowiska. Został może ranny, ale pokazał Elenie jak można to zrobić, wyciągnął ją troszkę ze skorupki i pokazał jej PIĘKNO tańca. Zbliża ją do siebie i siebie do niej.
* Kajetan Kircznik: jako medyk szybko pomógł Elenie i Władawcowi po tym jak się pocięli podczas tańca; piorun obiecał Elenie że jej pomoże i nie da Ariannie z nią rozmawiać gdy jej pomaga a potem obiecał Ariannie, że jak najszybciej Elenę uwolni spod opieki lekarskiej.
* Kaspian Certisarius: jest dowód, że eks-advancer z Hiyori znajdował się w bazie "duchów" noktiańskich. Nie znaleziono jego ciała, nie znaleziono go wśród pojmanych. Więc Noctis ma jeszcze jedną bazę w okolicy...
* OO Athamarein: retransmiter dla Astralnej Flary by Maja mogła odzyskać kontrolę nad Kazmirian i unieszkodliwić squatterów.
* OO Astralna Flara: zasiliła energią bazę Orbitera na Kazmirian; zaczęła się integrować z planami Dominy Lucis.
* OO Loricatus: ciężka fregata i okręt flagowy komodora Bolzy. Na pokładzie 4 entropiki. 
* NekroTAI Zarralea: zestrzelona z rozkazu komodora Bolzy offscreenowo; ma to wyglądać na noktiańskie działania.

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Obłok Lirański
                1. Anomalia Kolapsu, orbita
                    1. Strefa Upiorów Orbitera
                        1. Planetoida Kazmirian: opanowana przez squatterów, bo wada konstrukcyjna i opuszczenie terenu wyłączyły Semlę. Astralna Flara odzyskała kontrolę, uśpiła squatterów zabawą z lifesupport i zostawiła tymczasowo sprawną Semlę.
                        1. Planetoida Lodowca: kiedyś TKO-4271; Zarralea została zestrzelona i Orbiter opanował teren. Wprowadzono tu 2x squatterów z Kazmirian i baza zaczyna być dostosowywana do warunków Orbitera. Ma sprawny fabrykator Dominy Lucis, acz nie wiadomo jeszcze jak się z nim zintegrować.

## Czas

* Opóźnienie: 27
* Dni: 3

## Konflikty

* 1 - 
    * 
    * 
* 2 - 
    * 
    * 
* 3 - 
    * 
    * 
* 4 - 
    * 
    * 
* 5 - 
    * 
    * 
* 6 - 
    * 
    * 
* 7 - 
    * 
    * 
* 8 - 
    * 
    * 
* 9 - 
    * 
    * 
* 10 -
    * 
    * 

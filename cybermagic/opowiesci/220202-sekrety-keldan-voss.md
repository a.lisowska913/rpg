---
layout: cybermagic-konspekt
title: "Sekrety Keldan Voss"
threads: legenda-arianny
gm: żółw
players: fox, kapsel, kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [220126 - Keldan Voss, Kolonia Saitaera](220126-keldan-voss-kolonia-saitaera)

### Chronologiczna

* [220126 - Keldan Voss, Kolonia Saitaera](220126-keldan-voss-kolonia-saitaera)

## Plan sesji
### Co się wydarzyło

Faza 1:

* Iorus. Pierścień Halo. Znajduje się tam niewielka rozproszona kolonia Keldan Voss.
* Keldan Voss zbankrutowali. Zostali kupieni przez Pallidę.
* Pallida sprowadziła swoich ludzi, którzy działają nad stację należącą do Keldan Voss.
    * Drakolici z Klanu Mgły, mają dość nieprzyjemne zasady. Nie chcą redukować poziomu energii.
        * ewolucja przez magię
    * Pallidanie chcą zdobywać i harvestować kryształy Vitium.
    * Mała stacja, z rozproszonymi stacjami dodatkowymi. Wszędzie są generatory Nierzeczywistości.
* Faith Healer Keldan Voss nazywa się Kormonow Voss.
* Z uwagi na wysoki poziom magii, stare mechanizmy itp. mamy promieniowanie - lokalni drakolici i sprowadzeni pallidanie chorują i gasną
* Z uwagi na to, że drakolici nie chcą się integrować i współpracować, wszyscy mają obowiązkowe transpondery / "komórki".
* Drakolici mają swoich "Kroczących We Mgle", którzy mogą działać bez skafandrów i we mgle.

Faza 2:

* Mgła. Część Kroczących jest Skażona i jest częścią Mgły. Część działa bez problemu.
* Pallidanie obwiniają drakolitów o Mgłę i o śmierci spowodowane przez Mgłę.
* Mgła atakuje bazę.
* Dwóch agentów naprawiających generatory Nierzeczywistości zostało Skażonych przez Mgłę.
* Szczepan chce, by Annice się nie udało. On lepiej będzie tym dowodził.
    * 4 członków delegacji -> na orbicie
    * wysłany snajper w Mgłę; zabił już 7 drakolitów
* Kormonow chce się pozbyć pallidan. Mgła - nienawiść - go już opętała.
    * Drakolici wiedzą, że Kormonow wysadził generatory by móc ratować umierających drakolitów i palladian.
    * 11 pallidan (sprowadzonych) jest przez Kormonowa "naprawionych". -> 7 zabrała Mgła
    * 12 pallidan "porwanych" -> 5 zabrała Mgła, 3 uratowanych przez drakolitów
* Gdzie są drakolici? => niewielkie generatory Nierzeczywistości, znajduje się baza zasilana Vitium w księżycu.


### Co się stanie

* ?

### Sukces graczy

* Baza przetrwa atak Mgły
* Nie dojdzie do eskalacji
* Snajper i mgła?
* Elena...

## Sesja właściwa
### Scena Zero - impl

* .

### Scena Właściwa - impl

Klaudia ma dziwny odczyt na skanerach Kastora. Coś jest dziwnego / nie tak na planetoidzie. Najpewniej jest tam mgła. To nie do końca ma sens. Klaudia nadała trudny temat Eustachemu - zrobić COŚ by miała sprawne skanery. Eustachy: niech skanery Kastora robią serię skanów, kierowanie. Nie jeden / dwa skanery a seria. Czyli zamiast jeden solidny skaner, seria by móc robić serię dokładniejszych skanów ale kierunkowo. A Klaudia dodaje softwarową korekcję.

TrZ (Klaudia i jej soft) +3:

* V: Bez kłopotu udało się zdobyć odczyty.
* X: Trzeba było polatać Kastorem. Robić przeloty bliżej planetoidy itp.
* V: Odczyty pokazały coś ciekawego. Bardzo ciekawego. Pokazały obecność czegoś na kształt źródła energii zaszytego w pewnym fragmencie planetoidy; coś tam jest.

Ta mgła bardziej reprezentuje... jakąś formę energii. Pył w formie mgły? Ale co nadaje mu formę. Pył magicznej rudy. Mgła się zagęszcza. Powoli i konsekwentnie jest jej coraz więcej. Aczkolwiek części mgły zbliżają się do bazy. I ogólnie planetoida ma "atmosferę" z mgły.

* Mateus: "Arcymagu, chcesz nas bombardować?". On wie o mgle. Na planecie tylko "najeźdźcy i drakolici".
* Annika: "OMG MGŁA! Ratuj nas! Nie mamy dość skafandrów by wszystkich uratować! A nie mogłam ewakuować wszystkich bo chcę móc uratować porwanych ludzi."

Klaudia nie ufa statkowi pallidan. Wystawia nań czujniki.

Arianna opisuje Mateusowi źródło energii. TrZ+4

* V: Mateus zbladł. Bardzo zbladł. "To jest nasza tajna baza". 
    * Co gorsza, mają elektrownię atomową chłodzoną lodem... i z zapłonem przez kryształy vitium. Nie generuje mgły ALE na nią wpływa.
* V: Arianna przekonała Mateusa do tego, by uwzględnił, że wolą Saitaera może być dostosowanie korporacji do siebie i siebie do korporacji. Perfekcyjna fuzja.
    * odłam Kultu Saitaera który chce współpracy z korporacją. By ich przejąć.

Annika -> Eustachy "Wygrałeś. Nie miej takiej szczęśliwej miny. Uratujcie nas stąd. Wysłałam kilka godzin zanim przybyliście 12 żołnierzy i 4 techników do naprawienia generatorów Memoriam. Wszyscy zniknęli. Na orbicie jest mag - Barnaba Lasset."

Arianna -> Mateus "By zintegrować drakolitów i korporacji, musimy zintegrować siły. Wyślijcie kilku drakolitów - ustabilizujcie generator Memoriwm. Oni na was spojrzą przychylnie. Uważają, że porwaliście ich ludzi. Udowodnijcie, że się mylą." Mateus "ale oni mają snajpera. Tak, porwaliśmy."

Eustachy: przybywamy z odsieczą. Nie pyskować bo w ryj.

Skanery wykazały we mgle... drzewa i pająki. Ale po chwili wszystko zniknęło. Klaudia robi skan TrZ+2:

* V: Klaudia widziała w tym wszystkim symbol Saitaera. Idea Saitaera. Trochę mechaniczne, trochę organiczne.
* V: Skanery rejestrują coś co wygląda jak strzelanina? Walka? We mgle. Ale nie ma dość stężenia mgły by coś ukrywać.
    * Tak, tu były starcia z noktianami o kryształy vitium.

Klaudia próbuje na podstawie tych danych dojść do tego kim są ci kultyści; m.in. na podstawie skanów bitew.

* V: historia
    * Było tu starcie noktian i drakolitów z Neikatis. Drakolici wzięli się tu stąd, że na Neikatis wojna faeril - drakolici. Tutejsi uciekli w to miejsce. I wtedy napatoczyli im się noktianie. I noktianie stąd uciekli - pokonani przez drakolitów. I tutejsi drakolici to po prostu "chów wsobny" a częściowo "wolni ludzie". Część noktian tu wróciła i zostali przyjęci. I ci ludzie stanowią aktualny kult. 
    * Gdzieś po drodze zwrócili się do Saitaera. I on odpowiedział. Małe dary - utrzymać ludzi przy życiu. I osobą, która zwróciła się do Saitaera był ich naczelny naukowiec, badacz. Ten koleś znalazł faktycznych kultystów Saitaera. To jest przodek naszego szamana. Kormonow Voss jest potomkiem badacza, który "oddał" tą bazę Saitaerowi. 
    * pallidanie są z Neikatis. Oni są bliżej faeril.

Arianna -> Mateus. "Przepuśćcie naszych, naprawimy". Ok. Zdaniem Mateusa mgła wspiera. Kroczący w Mgle im pomagają. Oni wszyscy sobie pomagają i się ratują. Nie ma zła we mgle. (ale też nie ma pająków i drzew).

OO Kastor ląduje na starporcie. Jego pole Memoriam jest silne; odpycha mgłę. Arianna ma wrażenie, że mgła się _odsunęła_ od statku. Klaudia słyszy _low key chatter_. Używa kodów komunikacyjnych korporacji, kodów Orbitera, wszystkie dane historyczne i lokalne. I noktiańskie.

ExZ+3:

* X: Klaudii po prostu zajmie to trochę czasu. Klaudia: paranoja++, czy to nie jest specjalne utrudnienie by zachęcić, warstwa zabezpieczeń, filtry, ataki memetyczne. +1V
* Vz: Klaudia widzi kilka ścieżek. Ścieżki świadczą o trwającej walce pomiędzy różnymi siłami. Ale jest tam jeszcze jedna ścieżka się. Powtarzająca się. Ścieżka sterująca. Coś co nie jest bytem, głosem, działaniem. Klaudia wykryła jeszcze jedną ścieżkę - bitwę koordynuję BIA. Klasy Reitel.

Mateus zapytany przez Ariannę odnośnie BII bazy powiedział, że mają BIA. Ale ta BIA nie żyje. I żeby "działała", zintegrowali ją z ochotnikiem (z woli Saitaera) i potem do regeneracji tkanki biologicznej używa się dzieci które nie przejdą przez Ewolucję (czyli wymrą). Mateus mówi to z lekkim smutkiem i spokojem. "Ot, kocia karma tyci za droga". I tak, BIA była klasy Reitel. BIA była z tej jednostki noktiańskiej która tu walczyła przeciwko drakolitom.

Elena chce iść z Eustachym by go chronić. Zrobiła sweep - nikogo nie ma. Potem przeszła przez Mgłę.

Eustachy odpala generator Memoriam. Niech przeciwnik się ujawni. 

ExZ+3:

* V: Przeładowany Generator Memoriam. Złapany żywcem. Z jego ciałem zespolił się nieśmiertelnik - starszy koleś. To jest pallidanin. Jeden z tych od Anniki. Z tych porwanych przez Mateusa. Koleś wygląda na młodszego niż był. Magia zmieniła jego strukturę. On się _zmienił_. Ale teraz jest nieprzytomny, Elena go przechwyciła.

Plan Eustachego - on naprawia generator, Elena szuka wrogów.

Eustachy: "Mamy Lancery. Technik przetrwa." (myśli: jak nie przetrwa, mamy cenne dane).

TrZ (Elena mimo wszystko Eustachemu ufa) +2:

* V: Elena NADAL ufa Eustachemu
* X: Elena się będzie obwiniać jak coś się stanie technikowi. To będzie jej wina.
* V: Elena zgadza się z rozumowaniem Eustachego

Eustachy i technik naprawiają generator

TrZ (technik) +2:

* X: COŚ SIĘ ZMIENIŁO
* V: Generator będzie naprawiony.

PHASE SHIFT.

Eustachy przebudowuje generator Memoriam. Przesterować. Ma działać chwilę i umrzeć. I z przenośnego Memoriam - katalizator. Przebudowa. Technik ma znaleźć części zapasowe, Elena znaleźć miejsce ciała. Pełne pole do popisu.

ExZ+4:

* X: REAKCJA.
* V: Przebudowanie generatora. Zadziała.
* X: Ten generator będzie stracony.

Cały atak - seria dron górniczych dostosowanych do walki, kody kontrolne mające unieszkodliwić sprzęt pallidański - wszystko Elena zestrzeliła. Klaudia odbiła ten sygnał do dron - drony przestały działać.

Przeciwnik jest częściowo unieszkodliwiony. Eustachy - widzisz jak Elena atakuje jeden z korytarzy. Nikogo tam nie ma, ale uznała, że coś było i odesłała.

Bezproblemowo Annika jest ewakuowana. Annika jest na Kastorze.

* Annika -> Eustachy: kojarzy "kolesia z Mgły": to jeden z tych porwanych (Stefan). "Nie daruję im wszystkim!!!"
* Mateus -> Arianna: porwaliśmy wszystkich. 

## Streszczenie

Zespół skutecznie ewakuował pallidan z Keldan Voss, po drodze orientując się że część porwanych przez Mgły osób została Skażona i zmieniona w Kroczących w Mgle. Eustachy wydobył jednego do badań dla Marii. Co ciekawe - pallidanie i keldanici widzą zupełnie inne wersje historii ("snajper" vs "porwania") a do tego Mgły powodują jeszcze większą nierzeczywistość i złudzenia. A Arianna politycznie a Klaudia naukowo dochodzą do tego co jest prawdą...

## Progresja

* .

### Frakcji

* .

## Zasługi

* Arianna Verlen: Przekonała odłam drakolitów, że warto współpracować z korporacją. Politycznie zmusza drakolitów do benefit of doubt. 
* Eustachy Korkoran: Ewakuuje Annikę i pallidan z Keldan Voss na orbitę, do Kastora i jednostek wspierających. Podczas ewakuacji zaatakował mgłę i wyciągnął jednego z Kroczących do badań dla Marii. Naprawił generator osłaniany przez Elenę.
* Klaudia Stryk: Odkryła specyfikę mgieł Keldan Voss - energie z vitium. Zidentyfikowała też obecność BIA klasy Reitel zamiast Hestii. I doszła do historii tego miejsca - noktianie vs drakolici a potem sojusz.
* Elena Verlen: Osłania Eustachego. Nie reaguje na phase shift. Zestrzeliła serię dron górniczych skuteczniej niż kiedykolwiek. Widzi niestety też rzeczy których nie ma... albo jest tam więcej niż widać.
* Annika Pradis: pallidanka; dowodzi bazą; poprosiła Eustachego o ewakuację z tego miejsca. Załamana faktem, że ich porwani ludzie stali się Kroczącymi we Mgle. Nie daruje keldanowcom...
* Mateus Sarpon: zdecydował się na współpracę z pallidanami - przejmie korporację w imię Saitaera. Opowiedział Ariannie sporo odnośnie tego co się tu działo - martwa BIA zintegrowana z ochotnikiem, historia...

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Iorus
                1. Iorus, pierścień
                    1. Keldan Voss: ma tendencje do pojawiania się mgieł vitium, które mają własności Skażające. I okolice Keldan Voss mają atmosferę z tych mgieł. KV składa się z kilku planetoid, ze starym sprzętem.
 
## Czas

* Opóźnienie: 2
* Dni: 2

## Konflikty

* 1 - Eustachy buduje sieć skanerów
    * TrZ (Klaudia i jej soft) +3 VXV
    * Bez kłopotu udało się zdobyć odczyty. Trzeba było polatać Kastorem. Odczyty pokazały coś ciekawego. Bardzo ciekawego. Pokazały obecność czegoś na kształt źródła energii zaszytego w pewnym fragmencie planetoidy; coś tam jest.
* 2 - Arianna opisuje Mateusowi źródło energii.
    *  TrZ+4 VV
    * Mateus zbladł. Bardzo zbladł. "To jest nasza tajna baza". Arianna przekonała Mateusa do tego, by uwzględnił, że wolą Saitaera może być dostosowanie korporacji do siebie i siebie do korporacji. Perfekcyjna fuzja. Odłam Kultu Saitaera który chce współpracy z korporacją.
* 3 -  Klaudia robi skan mgły
    * TrZ+2 VVV
    * Klaudia widziała w tym wszystkim symbol Saitaera. Idea Saitaera. Trochę mechaniczne, trochę organiczne. Skanery rejestrują coś co wygląda jak strzelanina? Walka? We mgle. Ale nie ma dość stężenia mgły by coś ukrywać. Tak, tu były starcia z noktianami o kryształy vitium. Poznajemy historię bazy
* 4 - Klaudia słyszy _low key chatter_. I dekoduje
    * ExZ+3 XVz
    * Klaudii po prostu zajmie to trochę czasu. Klaudia: paranoja++, czy to nie jest specjalne utrudnienie by zachęcić, warstwa zabezpieczeń, filtry, ataki memetyczne. +1V Klaudia widzi kilka ścieżek. Ścieżki świadczą o trwającej walce pomiędzy różnymi siłami. Ale jest tam jeszcze jedna ścieżka się. Powtarzająca się. Ścieżka sterująca. Coś co nie jest bytem, głosem, działaniem. Klaudia wykryła jeszcze jedną ścieżkę - bitwę koordynuję BIA. Klasy Reitel.
* 5 - Eustachy odpala generator Memoriam. Niech przeciwnik się ujawni. 
    * ExZ+3 V
    * Przeładowany Generator Memoriam. Złapany żywcem. Z jego ciałem zespolił się nieśmiertelnik - starszy koleś. To jest pallidanin. Jeden z tych od Anniki. Z tych porwanych przez Mateusa. Koleś wygląda na młodszego niż był. Magia zmieniła jego strukturę. On się _zmienił_. Ale teraz jest nieprzytomny, Elena go przechwyciła.
* 6 - Eustachy przekonuje Elenę do planu
    * TrZ (Elena mimo wszystko Eustachemu ufa) +2  VXV
    * Elena NADAL ufa Eustachemu. Elena się będzie obwiniać jak coś się stanie technikowi. To będzie jej wina. Elena zgadza się z rozumowaniem Eustachego
* 7 - Eustachy i technik naprawiają generator
    * TrZ (technik) +2 XV
    * COŚ SIĘ ZMIENIŁO. Generator będzie naprawiony.
* 8 - Eustachy przebudowuje generator Memoriam. 
    * ExZ+4 XVX
    * REAKCJA. Przebudowanie generatora. Zadziała. Ten generator będzie stracony.
---
layout: cybermagic-konspekt
title: "Pierwszy tajemniczy wielbiciel Liliany"
threads: rekiny-a-akademia, akademia-magiczna-zaczestwa, aniela-arienik
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [210713 - Rekin wspiera mafię](210713-rekin-wspiera-mafie)

### Chronologiczna

* [220730 - Supersupertajny plan Loreny](220730-supersupertajny-plan-loreny)

## Plan sesji
### Theme & Vision

* Szkody robione przez AMZ i Rekiny muszą być jakoś kompensowane
    * Triana zauważa, że pojawiają się problemy których nie powinno dać się znaleźć
        * działania Eszary by AMZ rozwiązywali problemy sami; jej awatarem jest "Aniela Arienik"
    * Reakcja Piotrka Parczaka.
* Pragnienie podziwu, głód bycia znanym i świetnym i obecność strasznego rywala PLUS niedojrzały umysł dzieciaka prowadzą do problemu
    * Lorena Gwozdnik stworzyła niemagiczne świetne rysunki i obrazy, m.in. Liliana jest dumna z jej portretu i go ma w pokoju
    * Mariusz Kupieczka stworzył pod wpływem silnych emocji trzy obrazy. By Liliana była zachwycona. Ale Paradoks i narkotyki zmieniły ich zachowanie.
    * -> Eskalacja przez magię sprawiła stworzenie niebezpiecznych artefaktów
        * Obraz sam z siebie faktycznie jest piękny, ale piękno czerpie z serca patrzącego i pokazuje OPTYMALIZUJĄC podziw
            * ...i jak nie ma odpowiedniego pola magicznego to automatycznie czerpie energię z tego co ma
            * ...i ma własności przyciągające - im dłużej na nie patrzysz tym bardziej Cię hipnotyzują
* MELODIA: 
    * SESJA: Eliza "Automatic"
        * "I've seen it happen a million times | You can play it forward or rewind", "Automatic cause there's nothing you can do | Automatic boy she's taking over you"
    * MARIUSZ: Lena Katina "Who I am"
        * "My emotions taking a hold of me" (...) "This is who I am No more games or disbelieving Stop to analyze your feelings"

### Co się stanie (what will happen)

.

* S0: brak
* S1: Julia potrzebna jest do pomocy Trianie TERAZ
    * Triana pracuje z Lilianą nad  ręcznie zrobionym servarem dla Liliany z resztek tego od Arkadii i scutiera
    * pojawia się SOS systemów detekcyjnych Triany
        * Julia jest w szoku że Triana ma tak dobrą siatkę detekcyjną. Triana nie ma.
    * na nieużytkach jest konieczność pomocy. Sześć nastolatków zrobiło kapliczkę do portretu Liliany. Wykradli go. Prześlizgnęli się mimo zabezpieczeń.
        * CPR
* S2: Konieczność znalezienia dwóch pozostałych portretów
* S3: Portret Liliany 2
    * Cyberszkoła.
        * Tadeusz Kruszawiecki
* S3: Portret Liliany 3
    * Piotr Parczak, kolekcjoner parafiliów czarodziejek AMZ i Rekinów. Nie odda. Zarabia na nich. Kupił uczciwie.
        * ENCAO:  0+--0 |Silne emocje;;Kłótliwy;;Cyniczny| VALS: Stimulation, Achievement >> Security | DRIVE: Kolekcjoner
    * Pracuje z Rezydencji

### Sukces graczy (when you win)

* .

## Sesja właściwa

### Fiszki

#### 1. AMZ

* Liliana Bankierz: Iluzjonistka i kinetka która bardzo chciała być Diakonką i lekarzem; przetwarza i porządkuje informacje ale kocha piękno. Wizualizacje, odwaga, pamięć.
    * (ENCAO:  ++--+ | Idealistyczna;;bezkompromisowa;;śmiała i odważna| VALS: Self-direction, Benevolence > Tradition | DRIVE: Piękno, Fairness)
    * "Rzeczywistość jest jedynie wariantem tego, co mogę zobaczyć. Rzeczywistość NIE wygra z marzeniami."
* Triana Porzecznik: kochliwa i romantyczna konstruktorka która jest świetnie ubrana i lubi dotykać ludzi.
    * (ENCAO:  ++0++ | Przyjazna i przytulna;; Roztrzepana;; Wiecznie śpiąca| VALS: Achievement, Family | DRIVE: Pomóc tym co pomocy potrzebują, PUPPY!!!)
    * seksowna majsterkowiczka lubiąca być na świeczniku ale jeszcze bardziej lubiąca przyjaciół o talencie do dekonstrukcji
    * "Kiedyś dorównam ojcu w konstrukcjach - na pewno mogę zalać problem energią. Tylko muszę wziąć coś by nie spać aż 5 godzin."
* Mariusz Kupieczka: artysta AMZ podkochujący się w Lilianie (oryginalnie z małej mieściny w Przelotyku), kulturowo: drakolita / sybrianin
    * (ENCAO:  ++-0+ |Niefrasobliwy;;Nieodpowiedzialny;;Ekstremalne pomysły| VALS: Achievement, Face >> Humility| DRIVE: Żyć jak celebryta)
    * "Muszę jej zaimponować, bo na mnie nie zwróci uwagi", "nie poświęcisz -> nie stworzysz piękna"

#### 2. Inni

* Tadeusz Kruszawiecki: dyrektor Cyberszkoły
    * (ENCAO:  +-0+- |Swojski, prosty;;Ugodowy, unika konfliktów;;Kobieciarz| VALS: Hedonism, Face | DRIVE: Pomóc dzieciakom, integracja społeczeństwa)
    * "Nieważne, magowie czy nie. Te dzieci mają potencjał i dla nich to wszystko robimy. Nieważne jak to się kończy, grunt, by nie ucierpiały"
* Piotr Parczak: kolekcjoner parafiliów czarodziejek AMZ i Rekinów. HATEFUL. Liczne protezy, ratownik po działaniach magów.
    * (ENCAO:  0+--0 |Silne emocje;;Kłótliwy;;Cyniczny| VALS: Stimulation, Achievement >> Security | DRIVE: Kolekcjoner magicznych parafiliów)
    * "To jest MINIMUM tego co jesteście mi winni za to co robicie."

### Scena Zero - impl

.

### Sesja Właściwa - impl

Julia dostała prośbę od Triany, czy mogłaby przyjść pomóc. SZYBKO. To nie jest typowe, Triana coś zbroiła...

Triana jest jak zawsze w Rezydencji Porzeczników. Ale tym razem poprosiła Julię do pójścia do Garażu - Garaż to miejsce gdzie Triana robi ostrzejsze eksperymenty. Co najbardziej Julię zaskoczyło - obecność Liliany. I jakiejś chorej abominacji, coś pomiędzy scutierem i lancerem.

* Triana: "Nie mogę skalibrować magicznie żeby to pasowało do siebie. Scutier to nie jest Lancer."
* Liliana: "Może da się skanibalizować część sprzętu - chociaż TAI"
* Triana (patrzy z nadzieją na Julię, ona jest kiepska w konfliktach a Liliana nie odpuszcza)
* Liliana: "Julia, cześć" /nie ma entuzjazmu, ale nie ma też wrogości. Mają... różne doświadczenia.
* Triana, szybko wyjaśnia: "Julia jest ekspertem od broni, servarów i innych takich. Jeśli ktoś połączy TWOJE servary to tylko ona."
* Julia: "CZEKAJ TO TWOJE SERVARY? SKĄD MASZ SERVARY?! Mieć servary i tak je złachać..."
* Liliana (kręcąc lekko głową by uniknąć oskarżenia): "Technicznie są moje. Takie były."
* Julia: (patrzy podejrzliwie)
* Liliana: "Serio, scutier pozyskałam ze złomowiska. Lancery nas ostatnio napadły, pamiętasz? Jeden od tej rekinki (Arkadii) a drugi potem mnie napadł."
* Julia: "co, go zjadłaś?"
* Liliana: "...poległam. Ale był dopasowany do mnie. Tak skończył. I teraz chciałabym go wzmocnić. Jest taka jedna rekinka którą MUSZĘ pokonać."
* Julia: "kto?"
* Liliana: "...Arkadia Verlen..." /ponuro
* Julia: "nie ma znaczenia jak go zrobimy"
* Liliana: "muszę coś zrobić."
* Julia: "coś?"

Liliana w skrócie wyjaśniła Julii, że wszystko w sumie przez Marysię. Bo zniszczyła jej portret. Technicznie, skrzywdziła artystkę. I to zaeskalowało. A potem były _te sprawy_ (nie powiedziała jakie bo nie wie) z Eternią. I w ogóle.

* Julia: "Co chcesz zrobić?"
* Liliana: "Szczerze? Jak się następnym razem spotkamy - a spotkamy się, bo na mnie poluje..." - poprawiła się - "...wchodzimy sobie w drogę. Serio. Rzeczy jakie ja robię i ona. Chcę mieć szansę. Nie chcę..." - oczy Liliany się zachmurzyły - "nie mam jej umiejętności walki, ale ona jest jedna. Rekiny robią co chcą i musimy móc bronić. Ludzi, terenu, czegokolwiek. Nie chodzi o atakowanie Rekinów, ale chodzi o możliwość obrony. MUSZĘ mieć ten servar."
* Triana: "Jesteś tienką. Możemy użyć elementów sentisieci. Nie wiem jak, ale możesz mnie poprowadzić."
* Liliana: "Wolałabym nie. Moja sentisieć jest... inna (westchnięcie). Nie jest tak silna. Nie mam takiej kontroli (widać, że ją kosztuje powiedzenie tego)"

Triana położyła współczująco rękę na ramieniu Liliany. Liliana uniknęła dotyku. "Nie potrzebuję współczucia, potrzebuję BRONI."

* Julia: "Czasami współczucie warto przyjąć. Dobra, chcesz coś co da Ci szansę, tak?"
* Liliana: "Tak..."
* Julia: "Co wiesz o Arkadii?"
* Liliana: "Jeszcze niewiele, ja... nie chciałam być Rekinem, tak? Mogłam, ale wolałam przyjść tu. Zresztą, po co wam to mówię!" /wybuch frustracji
* Julia: "Byśmy mogły ci pomóc"

Liliana chciała coś powiedzieć, ale rozległ się alarm w Garażu. Triana szybko skoczyła do konsoli. Siatka detekcyjna dron Triany - jedna drona, którą Triana uznała za zagubioną (just Triana things) się uruchomiła i znajduje się na Nieużytkach. W terenie poza perymetrem. I tam zlokalizowała (drona) nieprzytomnego człowieka.

* Triana: co..?
* Liliana: lecimy!
* Triana: musimy wezwać wsparcie, ktoś jest ranny!
* Liliana: znam pierwszą pomoc a Twoja drona najpewniej nie wykryła prawdy.
* Triana: (jest prawie pewna, że Liliana ma rację)

Gdy Liliana przekonuje Trianę (ubierając się szybko), Julia przekazuje Służbom Ratowniczym Zaczęstwa (SRZ). Test dron, wykrył coś takiego, nie wie czy to prawda. SRZ potwierdzili przyjęcie, wyślą odpowiednie osoby. Dwa ścigacze patrolowe.

Julia powiedziała Trianie i Lilianie, że zgłosiła do służb. Triana ma uśmiech (YESS!! KTOŚ KOMPETENTNY TO ZROBI). Liliana zamrugała. "Nie pomyślałam". Mówi szczerze. "Dobry pomysł".

* Triana: "co to?" -> zbliżenie na obraz drony. Drona jest w jednym z bunkrowatych obiektów, niezakopanych i jest tam kilka nieprzytomnych ludzi i... portret Liliany. Niewyraźny portret ale da się poznać Lilianę.
* Liliana: "co do cholery?!" (po chwili) "przynajmniej jestem ubrana..." (po chwili) "słuchajcie, ja tam muszę jechać"
* Triana: "ale jak drona, która zniknęła tydzień temu... skąd tam się wzięła?"
* Liliana: "podejrzewam pułapkę Arkadii" (jest to ostatnia rzecz jaką podejrzewa Julia) "albo Zygmunt Zając i te konserwy..."

Julia zaproponowała podwiezienie Lilianie. Ta się zgodziła. Triana "ja zostanę, zrobię symulacje - jak co, jestem zdalnie".

Julia bez kłopotu dojechała do celu. Służby SRZ już wynoszą na noszach dzieciaki z bunkra. Sześciu nastolatków. Jeden ze służb do Julii: "Ty zdzwoniłaś?" "Tak"

* SRZ: Jesteś magiem?
* Julia: Jestem, ale nie wiem czy od tego co potrzebujecie.
* SRZ: Magiem SKĄD?
* Julia: Uczennica AMZ
* SRZ: Przynajmniej nasza. Słuchaj, mają objawy wyssania energetycznego. Magią. Na oko ten artefakt jest niebezpieczny. Dlatego tu jesteś? Ta Wasza Mieralit Was wysłała?
* Julia: Nie, nie wie że tu jestem.
* SRZ: Dzieciaki są wycofane, artefakt jest containowany. Tamta rozwalona drona jest Wasza?
* Julia: mhm
* SRZ: (patrzy podejrzliwie na Lilianę) To na pewno nie Wasza sprawka? Co wiecie na ten temat?
* Julia: tyle, że wszystko wskazuje że nasza koleżanka ma paskudnego stalkera?
* SRZ: Czyli nic o tym nie wiecie i ktoś próbuje ją wrobić?
* Liliana: ktoś próbuje mnie wrobić? /zaskoczenie i gniew
* SRZ: Twój portret wyssał energetycznie sześciu nastolatków.
* Liliana: (po jej minie pomyślała o żarcie o Diakonach) któryś z Rekinów przegiął pałę.
* SRZ: nie jestem terminusem, nie wypowiadam się. Dobrze, że ta drona tu była
* Triana (priv do Julii i Liliany): nie wiem CZEMU tu była. Odczyty... strasznie udany zbieg okoliczności.
* Liliana: (priv do Julii i Triany): wygląda na sentisieć, ale... w sensie, to są rzeczy jakie robiłaby sentisieć, nie? Poinformowanie o czymś takim.
* Triana (priv do Julii i Liliany): nie mam sentisieci, nie jestem magiem rodowym
* Liliana (priv do Julii i Triany): tak czy inaczej, ktoś mnie próbuje wrobić. Ok, mogę wiele przeżyć, ale NIE MIESZAJCIE W TO ZWYKŁYCH LUDZI. /SERIO jest wściekła
* Julia: czy możemy przyjrzeć się bliżej temu obrazowi?
* SRZ: po tym jak przyjrzy mu się terminus. To poważna sprawa.
* Liliana: a możemy go po prostu zobaczyć? Nic mu nie zrobimy.
* Julia (ręka na ramieniu Liliany) nie wiemy co go aktywuje i jak działa a jak jest związany z Tobą, może być wycelowany w Ciebie. Poczekanie na terminusa może być rozsądne.
* Liliana (spuściła głowę by nie pokazać zniecierpliwienia i wściekłości)

20 minut później pojawiła się Laura Tesinik. Z narzędziami. Liliana chciała z nią porozmawiać, Laura "nie, jesteś cywilem, nawet jak jesteś magiem. Zrobię co mam zrobić i potem porozmawiamy."

Laura używa sprzętu by dojść do tego co to za artefakt.

TrZ+3:

* Vz: Laura określiła, że artefakt nie jest fundamentalnie bronią, nie jest przeklęty itp. Jest "bezpieczny"
* Vz: Laura określiła co się stało: 
    * Laura: "ktoś kto robił ten artefakt był nierozsądny. Lub niekompetentny. Przywykł do wysokiego pola magicznego. Artefakt - portret - sprawia, że jak na niego patrzysz to on... pokazuje lepszy obraz niż jest namalowane. Efekty 3d, bardziej się podoba itp." (tu Liliana poczerwieniała ze złości i zażenowania) "może też zmienić strój posiadaczki jeśli patrzący tego bardzo pragnie - każda osoba patrząca na ten obraz zobaczy coś INNEGO"
    * Julia: "czemu wymęczyło chłopców? Brakło energii?"
    * Laura: "dokładnie tak. W dużym polu magicznym ma dość energii w otoczeniu. W dużej grupie osób ma dość emocji. Sześć dzieciaków na uboczu? Drenuje energię. Nie mogło im zrobić krzywdy, ale są w niebezpiecznym miejscu."
    * Julia: "ogólnie, patałach. Masz jakiekolwiek wskazówki odnośnie pochodzenia lub autora?"
    * Laura: "ktoś od Was z AMZ lub ktoś z Rekinów. Bo emocje plus pole magiczne. Ale to tylko hipoteza, Pustogor przebada to dokładniej."
    * Laura (po chwili): "mówię wam to tylko dlatego, bo to nieeleganckie."

.

* Liliana: "mogę zobaczyć ten obraz? Chcę zobaczyć, co JA zobaczę."
* Laura: "..."
* Liliana: "bardzo proszę?"
* Laura: "podpisz tę formę w dwóch egzemplarzach. Że bierzesz odpowiedzialność itp itp"

Liliana i Julia podpisały i zobaczyły obraz. Liliana (dyskretnie) zrobiła zdjęcie. Laura (dyskretnie) udała że nie zauważyła.

Obraz jest świetnie zrobiony, nawet zanim efekt magiczny zaczął działać. Liliana jest w czerwonej sukni w formie "imperious but sweet". W tle jest laboratorium medyczne AMZ. Fajne kontrasty, inne postacie niby są, ale nie są zrobione istotnie i ze szczegółami. Liliana potwierdza, że widzi to samo. Ale są tam detale - Liliana wyraźnie jest po jakiejś akcji (eksperyment, leczenie...) i jej świetnie poszło. Wydźwięk: "elegant professional after work". Nie jest to obraz erotyczny. Wyraźnie widać, że ma Lilianę podnieść a nie dokopać.

* Julia: ktoś zrobił bardzo dobry obraz i porzucił go na Nieużytkach Staszka. Nie ma sensu. (do Liliany) nie kojarzę byś miała cichego adoratora.
* Liliana: bo jeśli mam, naprawdę jest cichy. Ale ile osób wie jak wygląda laboratorium medyczne AMZ oraz kto widział moją czerwoną suknię (mam DOKŁADNIE taką) oraz kto wie, że interesuję się medycyną?
* Julia & Triana: interesujesz się medycyną?
* Liliana: zanim zostałam iluzjonistką.

Liliana ma NOWĄ TEORIĘ: ktoś próbuje jej maksymalnie dokopać:

* tę suknię używała tylko na balu AMZ. Ale mimo, że suknia była ładna i Liliana naprawdę dobrze tańczyła, skończyła na 4 miejscu. Nie była na podium. WNIOSEK: ktoś chce jej przypomnieć.
* sala medyczna. Liliana chciała i zawsze interesowała się medycyną. Ale jej magia prowadzi w stronę iluzji i kinezy. Wniosek: przypomnieć czego nie będzie miała.
* mogę wyglądać jak odbiorca chce i jestem wypiękniona. Wniosek: nie uważają, że jestem dość ładna. /to ją zabolało, bo była Diakonką

Ale Julia patrzy na obraz i widzi naprawdę duże starania kogoś, kto się mega postarał by zrobić dobry obraz Liliany. Taki, który jej POWINIEN się podobać. Po prostu ona czyta to w najgorszy możliwy sposób.

* Julia: "terminusko, jak Ty to czytasz?"
* Laura: (pokerowa rybia mina) "jestem profesjonalistką i nie spekuluje"
* Julia: "nie pytam o spekulacje a o to jakie uczucia wzbudza w Tobie ten obraz"
* Laura: "niezadowolenie, bo to narzędzie wykroczenia"
* Julia: "tak, ale wiesz że nie o to pytam"
* Laura: "nie przygotowałam druków, by odpowiedzieć na takie pytanie"
* Julia: "po cholerę Ci druki by odpowiedzieć co widzisz na obrazku? Nie pytam o Twoją opinię jako terminusa."
* Laura: "obawiam się, że muszę wziąć obraz i wrócić do centrali. Zadania czekają."
* Liliana: "serio, powiedz. Julia ma powód że pyta."
* Laura: "a ja mam powód, że nie odpowiadam."
* Julia: "typowe CYA czy coś więcej?"
* Laura: "CYA. Tu nie ma nic więcej. Nie ukrywam przed Wami niczego co Wam się może przydać czy coś. Ale nie jesteśmy koleżankami. Nie spekuluję. To nasze śledztwo."
* Liliana: "to wykroczenie, nie przestępstwo?!" /niedowierzanie "ktoś ukradł mój wizerunek i skrzywdził nim ludzi!"
* Laura: "wykroczenie. Chyba, że pojawią się inne fakty. A teraz przepraszam, muszę iść dalej."

Triana policzyła prawdopodobieństwa tej drony. Wyszło jej na to, że JEŚLI te nastolatki to złomiarze i zebrali jej dronę która poza zasięg to samo pole magiczne przy obrazie MOGŁO odpalić awaryjne podsystemy drony. W ten sposób to JEST MOŻLIWE acz nie jest bardzo prawdopodobne. Ale jest 2-5% szansy że to FAKTYCZNIE jest samoistne. Triana jest już naturalnie uspokojona (2-5% to typowy failrate Triany). Liliana olała temat bo ma w głowie swój portret. Ale Julia nie wierzy w 2-5%. Tu coś jest. 

Julia do Liliany:

* J: wiemy kto był na balu i kto mógł Cię widzieć w sukni
* L: co więcej, ta suknia jest specjalnym wariantem. Usprawniłam ją iluzją. W tym wariancie... to było zaskoczenie. Chciałam wygrać, olśnić konkurencję, więc nie byłam w tym wariancie na zdjęciu. Tylko podczas tańca. Na zdjęciach nie mam tego wariantu. To musiał być ktoś na sali.
* J: mamy ograniczoną pulę ludzi na dzień dobry, patrzymy na listę ludzi i patrzymy kto mógł spełnić wymóg...

(Julii przypomniała się jeszcze jedna rzecz - była kiedyś w pokoju Liliany. Ona ma na ścianie swój portret. Zrobiony ze smakiem, skromny ale zrobiony z uczuciem, to widać (od Loreny dla przyjaciółki))

Julia ma PLAN. Pójść do nastolatków w szpitalu i dowiedzieć się skąd zajumali obraz. I czy zajumali dronę - dla własnej ciekawości.

Wielki Szpital Magiczny. Tam trafili na jeden dzień na badanie, regenerację i obserwację. Tak się składa, że tam trafiła też Julia. Chce ich odwiedzić, ma powiedziane "nie jesteś krewną", powiedziała że jest magiem i potrzebuje detali. Wpuścili ją.

Ernest Kacierzyn jest jednym z tych młodych, ma 17 lat.

* Ernest: to pani nas wyciągnęła? Dzięki.
* Julia: nie ma sprawy. Ale mam parę pytań, mogę?
* Ernest: jasne, jesteś magiczką, nie?
* Julia: mhm. Skąd mieliście ten obraz? Jak ukradliście, nie obchodzi mnie, ale muszę wiedzieć komu.
* Ernest: rozumiem, bo to była pułapka na takich jak ja, tak? Dlatego musisz wiedzieć? Walka z noktianami? To był noktiański obraz?
* Julia: tego się jeszcze dowiem... ale muszę wiedzieć skąd go masz.
* Ernest: to jest tak... grała pani kiedyś w 'truth or dare'?
* Julia: owszem
* Ernest: ale to pewnie jak pani była młoda?
* Julia: ...coś w ten deseń. (poczuła się staro)
* Ernest: _dare_ polegał na tym, że trzeba było wejść niepostrzeżenie na kampus. I stamtąd coś zabrać.
* Julia: i tym cosiem był obraz? Czy dare mówił co zabrać?
* Ernest: nie, jakiś kamień, kwiatek... coś ze śmietnika...
* Julia: czyli obraz był na śmietniku?
* Ernest: nie, on był... to było tak, przy takim budynku (opisał artefaktorium) był niedaleko, schowany w krzakach. Tak, by można było go znaleźć. Był w takim opakowaniu prezentowym ale szarym. Z napisem "dla Liliany".
* Julia: masz to opakowanie?
* Ernest: oczywiście, że nie. To są ślady, znaczy, wiesz...

Obraz z rozmowy jest taki: KTOŚ chciał zostawić to dla Liliany. By ona to znalazła lub ktoś jej to znalazł (miała za 15 minut zajęcia w artefaktorium). Ale grupka szczurów ulicznych się bawiła i wynieśli ten obraz z kampusa. AMZ nie ma poważnych defensyw, nie ma powodów. Oczywiście - ZOSTAWIANIE ARTEFAKTÓW POZA DESYGNOWANYMI MIEJSCAMI JEST WBREW REGULAMINOWI. Ale 15 minut. Coraz bardziej wygląda na cichego adoratora... Julia powiedziała, by następnym razem nie dali się podpuścić. A WŁAŚNIE! skąd drona?

* Ernest: drona? Tam była drona? /nic nie wie o dronie

Czyli faktycznie, drona to inna strona. Ktoś inny. I patrząc na całość opowieści, drona to zupełnie inna frakcja, ktoś, kto chciał tą grupkę uratować. Liliana miała rację - "sentisieć". Ktoś pełni rolę sentisieci.

TYMCZASEM Liliana przeleciała przez zestawy danych. Kto mógł to widzieć. Kto mógł tam być. Kto w ogóle umie malować. To było malowane przez artystę, nie zmaterializowane magią.

Tr Z +3:

* Vz: Znalazła najbardziej prawdopodobnego kandydata.
    * ślady: suknia, lokalizacja itp świadczą, że kandydat CHCIAŁ dać się znaleźć.
    * Liliana ma nową teorię. Kandydat został przekupiony przez Ernesta z Eterni. By się na niej mścić za Lorenę.
* V: Julia przechwyciła Lilianę, zanim ona pójdzie zabić chłopaka. Mariusz Kupieczka. 23. Z AMZ.

Liliana przebrała się w inny strój. "Anioł Śmierci z tendencją do dramatyzmu". I ma tazer. Liliana ruszyła prosto... Julia ją zatrzymała. "Nie idziemy mu wklupać". Liliana westchnęła, bo Julia wyraźnie nie ogarnia "Julio, on jest na czyichś usługach. Ernesta, Marysi, Rekinów, wątpię by Arkadii - ona nie ma na to dość intelektu społecznego." Julia: "najpierw z nim pogadamy". Liliana: "oczywiście, musi się przyznać."

Liliana wpadła do pokoju Mariusza. Jest tam też jego współpokojowiec. Liliana - "TY! wynocha! (i iluzją podkręciła ogień w oczach)" Chłopak wyszedł. Szybko. Mariusz ma mowę ciała "wtf" i lekko podekscytowany. ON JESZCZE NIE WIE.

* Liliana: (seria obelg). Kto Ci zapłacił za ten portret?
* Mariusz: (pełna konfuzja i jest urażony) Co?
* Julia: przetłumaczę. Portret Liliany w sukni... (opisuje, Liliana pokazała fotkę TEN). Sam go namalowałeś?
* Mariusz: tak, sam go namalowałem.
* Julia: ładny, tylko szkoda, że wyciekł z AMZ. Położył parę nastolatków do szpitala.
* Liliana: portret za pomocą którego może teraz się masturbować pół Barbakanu z Twojej winy.
* Julia: myślę, że terminusi nie mają na tyle jaj...
* Liliana: zostałam ośmieszona...
* Julia: nie zostałaś ośmieszona...

Julia próbuje zdeeskalować Lilianę

Tr (niepełny) Z (taktyczny, robi na tej akcji) +2:

* V: Liliana powstrzymała się od najbardziej jadowitych tekstów
* X: Liliana NAPRAWDĘ jest urażona tym portretem i tą sprawą. ŻADNA INNA DZIEWCZYNA by nie była, ale Liliana przez Diakon->Bankierz jest tu wrażliwa. Julia nie ma o tym pojęcia. Z jej pkt widzenia Liliana overreacts
* Vz: Liliana pozwoli mówić Julii i nie będzie przerywać.

Mariusz jest zraniony. Liliana jest zraniona. Julia nie wie czemu jest w środku teen dramy i NADAL nie wie skąd się tu wzięła drona. I nikogo nie interesuje, że ludzie są zagrożeni bo są zranieni czy coś.

* J: to naprawdę ładny obraz. Chciałeś go Lilianie podarować?
* M: tak, o to chodziło. Wszystkie trzy.
* J: (BLINK BLINK) trzy?
* M: tak. Ale koszt był za duży, musiałem dwa odsprzedać. Zostawiłem dla niej najlepszy.
* L: (kolor ataku serca)
* M: ale nie wiem po co się starałem.
* J: trochę się nie dogadaliście, co?
* L: trzy. Trzy moje portrety. Trzy, które krzywdzą ludzi.
* M: nie krzywdzą, jak?
* J: tu jest problem, wiesz? Zostawiłeś go w krzakach, nie? (wyjaśnienie co się stało)
* J: przypomnij sobie lekcję z katalizy. I umieść obraz na nieużytkach.
* M: nie rozumiem (to był Paradoks)
* J: upraszczając, wyssało ich.
* M: (chce wyjść z pomieszczenie, Liliana go zatrzymuje i "dokąd")
* M: idę naprawić to co się stało. Możesz mi wierzyć - ŻAŁUJĘ że kiedykolwiek zrobiłem te obrazy.
* L: uwierz, ja też.
* J: tym ludziom nic nie będzie, są w porządku. Wyjdą ze szpitala i nie powinno nic im być.
* M: cieszę się (nie patrzy na dziewczyny)
* J: wierzę, że nie chciałeś nikomu zrobić krzywdy. Jedyne co, ale zainteresują się terminusi.
* M: wiem. Możecie już iść? Zwłaszcza Ty? (pokazał na Lilianę)
* L: jeśli odpowiesz na parę pytań. Zrobiłeś to dla Eterni?
* M: co? Nie. Za kogo mnie masz?
* L: dla kogoś z Rekinów? Marysi Sowińskiej?
* J: Liliana, do kury nędzy, a dopuściłaś pomysł że możesz się komuś spodobać, ever crossed your mind?

.

* Vz: Do Liliany dotarło, że faktycznie MÓGŁ chcieć dobrze a ona posądza go o wszystko co najgorsze.

.

* (Liliana ORAZ Mariusz się zaczerwienili)
* L: nie. (szczerze) Does not happen, will not happen.
* J: bo co? nie jesteś brzydka.
* L: czyli ani Eternia ani Rekiny? (próbuje wyjść z twarzą)
* M: nie, nie zrobiłem tego dla nich. Zrobiłem to i nikt mi nie zapłacił. Możesz wyjść? Marnujesz czas a są ludzie którym coś się może stać.
* L: dobrze, to to napraw. (odwraca się i wychodzi)
* J: potrzebujesz pomocy?
* M: nie wiem. Nie wiem, bo nie rozmawiałem z nimi jeszcze. (trzyma werwę)
* J: zadzwoń jakbyś mnie potrzebował
* M: zadzwonię, jeśli przyjdziesz bez niej.
* J: no przecież.

Aha, Mariusz też nic nie wie o dronie. Niespodzianka.

Julia -> Laura: "To był problem kradzieży z terenu szkoły więc nie było intencji. To problem regulaminu, AMZ się tym zajmie. Oczywiście, terminusi mogą robić co chcą." Laura wzięła to pod uwagę w kompilowaniu raportu.

## Streszczenie

Liliana, Triana, Julia pracują nad formą integracji scutiera i lancera by Liliana mogła pokonać (lub choć wytrzymać) Arkadię Verlen. Triana dostała alarm ze swojej zaginiętej drony o nieprzytomnych ludziach na Nieużytkach. Okazuje się, że portret Liliany wyssał tych ludzi. Okazuje się, że student AMZ, Mariusz, zrobił portret by Lilianę poderwać, ale spartaczył; porter wysysa. Potem portret został ukradziony. Liliana dalej podejrzewa spiski. Gdy Liliana i Mariusz się konfrontują, okazuje się że były 3 portrety. Liliana zachowuje się nikczemnie wobec Mariusza, więc się szybko odkochał. Ona nie skojarzyła, że komuś się może podobać.

## Progresja

* Mariusz Kupieczka: silna niechęć i złość wobec Liliany Bankierz. Był zakochany, ale mu przeszło. Nie jest dziewczyną z jaką chce mieć cokolwiek do czynienia.
* Mariusz Kupieczka: niewielkie wykroczenie za umożliwienie ukradzenia artefaktu przez ludzi. Dalej jest nieszczęśliwy, bo 

### Frakcji

* .

## Zasługi

* Julia Kardolin: AMZ. Gdy Liliana i Triana chcą lecieć ratować nastolatków, ona wezwała służby (SRZ). Negocjuje z Laurą by ta powiedziała co może, dowiaduje się od nastolatków skąd zwinęli niebezpieczny portret a potem deeskaluje Lilianę by nie skrzywdziła Mariusza. Uzmysławia Lilianie, że Mariuszowi się mogła podobać, że to nie musi być spisek.
* Triana Porzecznik: AMZ. Chce pomóc Lilianie w zrobieniu servara zdolnego do starcia z Arkadią, ale nie wie jak. Jej zagubiona drona wysłała sygnał SOS bo ludzie są nieprzytomni. Po policzeniu prawdopodobieństw, to jest możliwe i Triana śpi spokojnie.
* Liliana Bankierz: AMZ. Jej portret zrobiony przez Mariusza wysysa ludzi. Podejrzewa że to Eternia, Rekiny lub Zygmunt Zając. Koreluje dane statystyczne by dojść do tego kto za tym stoi, potem atakuje Mariusza słownie. Nie wierzy, że może się komukolwiek podobać - nie jest już Diakonką (ale tego nie mówi).
* Laura Tesinik: ściągnięta jako terminuska do problemów z portretem Liliany. Z uwagi na przeszłość w Cieniaszczycie powiedziała Julii dane które pomogą jej i Lilianie w dojściu do tego kto stoi za tym wszystkim. Nie podaje własnych opinii i chowa się za aspektami prawnymi. Nie jest ich koleżanką.
* Mariusz Kupieczka: AMZ. 22 lata. Chciał zaimponować Lilianie i zdobyć jej serce, więc zrobił jej portret (technicznie, 3 z czego 2 musiał sprzedać). Niestety spartaczył i portrety wysysają ludzi poza silnym polem magicznym. Gdy Liliana go zaatakowała plugawymi słowami się szybko odkochał.

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Zaczęstwo
                                1. Dzielnica Kwiecista
                                    1. Rezydencja Porzeczników
                                        1. Garaż Groźnych Eksperymentów: tam Triana składa servara dla Liliany do walki z Arkadią. Jakoś. Nie wie jak.
                                1. Dzielnica Ogrodów: miejsce odpoczynku, bezpieczeństwa i ukojenia. I medyczne.
                                    1. Wielki Szpital Magiczny: zajmuje się przypadkami typowymi i trochę trudniejszymi, ale idzie na ilość. Trudne przypadki -> Pustogor. Julia gadała z nastolatkiem co zwinął z AMZ obraz.
                                1. Akademia Magii, kampus
                                    1. Artefaktorium: sprzed artefaktorium nastolatki wykradły portret Liliany który ona miała odnaleźć.
                                1. Nieużytki Staszka: grupa dzieciaków chciała oglądać wykradziony portret Liliany i padli (wyssał ich). SRZ ich uratowali.

## Czas

* Opóźnienie: 18
* Dni: 2

## Inne

.
---
layout: cybermagic-konspekt
title: "Wyłączone generatory Memoriam Inferni"
threads: historia-eustachego, arkologia-nativis, infernia-jej-imieniem, zbrodnie-kidirona
gm: żółw
players: fox, kapsel, kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [220817 - Osy w CES Purdont](220817-osy-w-ces-purdont)

### Chronologiczna

* [220817 - Osy w CES Purdont](220817-osy-w-ces-purdont)

## Plan sesji
### Theme & Vision

* Mija burza piaskowa, anomalne zachowanie Inferni
    * Przebudzenie Eustachego. "Wyłącz generatory Memoriam".
* Konflikt Bartłomiej Korkoran - Rafał Kidiron
    * Żywność: 
        * BK: nie można wrzucić Trianai innym arkologiom. Nie jest to tego warte. Nie można robić świadomej krzywdy nikomu!
        * RK: to jest niezbędne by Nativis miała postępy i prosperowała. Jednostki zawsze giną na Nativis.
    * Dylemat: 
        * Co robimy z bazą noktiańską?
            * Korkoran: "To nieważne, że to noktianie, trzeba im pomóc! Zabezpieczyć i pomóc się tam utrzymać. "
            * Kidiron: "Robili badania nad bronią biologiczną. Trzeba ich aresztować jak najszybciej, a zwłaszcza Calistę. To NOKTIANIE."
        * Co robimy z napastnikami z Wiecznego Skorpiona?
            * Korkoran: znak ostrzegawczy. To nie nasza sprawa, nie nasza arkologia.
            * Kidiron: to nie rozwiąże problemu.

### Co się stanie (what will happen)

* S0: świeżo po burzy piaskowej Infernia budzi Eustachego w nocy. 
    * "Wyłącz generatory memoriam... poznam Cię lepiej..." Konflikt z Tymonem. Zniszczony ołtarzyk wujka. Załoga dopinguje i stawia.
* S0: Ardilla zapoznaje się z Ralfem. Bartłomiej Korkoran ma nową znajdkę. Ardilla ma się nim zaopiekować.
    * Biblioteka w Nativis; spotkanie z dwójką faerilów. Nie lubią noktian ani Ardilli. Ale inni każą zostawić w spokoju. Ralf nie walczy.
* S1: (3 dni później)Lecą do CES Coruscatis. Był sygnał alarmu. 
    * Wujek: "ratujemy!" Kidiron: "oni stworzyli cholerne Trianai. Uważajcie. Aresztujcie." Wujek: "To nie takie proste, pomóżcie!"
    * Tymon: "wujek coś się zbyt blisko trzyma tych _noktian_, Eustachy..."
    * Bateria dział Coruscatis otwiera ogień do Inferni
* S2: ostrzeżenie "Coruscatis jest atakowane przez Trianai!" (Kratos) vs "Mamy problemy, sabotaż i część sił jest pod kontrolą Trianai (Amalgamoid)"
    * Tymon: mamy tylko wyjąć stamtąd Kalistę i doprowadzić do domu.
    * Janek: Wujek kazał im pomóc.
    * STATUS: płomienie na farmie i magazynach, w biolab
    * STATUS: noktianie są poza Coruscatis; poza zasięgiem baterii ale poza działkami

### Sukces graczy (when you win)

* 

## Sesja właściwa

### Fiszki

#### 1. Infernia

* Bartłomiej Korkoran: wuj i twarda łapka rządząca Infernią | faeril: "Infernia służy Korkoranom jako awatar Bezimiennej Pani."
    * (ENCAO: +-000 |Bezkompromisowy, nieustępliwy;;Ciekawski|Family, Benevolence, Self-direction > Achievement, Tradition, Humility| DRIVE: Inkwizytor: ujawnianie bolesnych prawd)
    * "Nasza Infernia ma za zadanie dać nam wolność oraz pomagać innym!"
* Emilia d'Infernia
    * "żona" Bartłomieja. Żywy arsenał. Egzekutor Bartłomieja. (Emilia -> "Egzekucja MILItarnA")
* Ardilla Korkoran: faerilka działająca jak drakolitka i przyszywana kuzynka Eustachego  <-- często Fox
    * badacz/odkrywca/scrapper. Zmodyfikowana tak, by łatwiej wchodzić w niedostępne miejsca, bardziej zwinność + orientacja w terenie + pewne umiejętności badawcze.
* Celina Lertys: drakolitka z Aspirii podkochująca się w Eustachym  <-- często Kić
    * (ENCAO: --+-0 |Stanowcza, skryta;;O wielu maskach| Universalism, Tradition, Security > Power, Humility| DRIVE: Harmonia naturalna między wszystkim)
    * złotoskóra o błękitnych elektrycznych włosach podkochująca się w Eustachym (science / bio officer + medical); augmentowana na widzenie rzeczy ukrytych; 19 lat
    * "Jesteśmy częścią Neikatis i podlegamy procesom Neikatis. Ale nadal mam zamiar zrobić wszystko by uratować swoich przyjaciół."
* Jan Lertys: drakolita z Aspirii
    * (ENCAO: -0-0+ |Zapominalski, obserwator, łatwo nań wywrzeć wrażenie | Humility, Universalism > Self-direction, Achievement | DRIVE: Pokój: Kapuleti i Monteki MUSZĄ żyć w pokoju, tylko nie Celina x Eustachy)
    * "Ciężką pracą dojdziemy do tego, że wszystko będzie działać jak powinno. Ja nie wtrącam się do działania Bartłomieja Korkorana; on wie lepiej."
* Tymon Korkoran: egzaltowany lekkoduch i następca Bartłomieja
    * (ENCAO:  +--00 |Co chce, weźmie;;Niecierpliwy;;Egzaltowany | Stimulation, Tradition > Face, Humility | TAK: Być lepszym od Bartłomieja w oczach Kidirona)
    * "Infernia jest moja i należy do mnie. Bartłomiej marnuje jej potencjał, a dzięki niej Nativis może rządzić Neikatis!"
* Ralf Tapszecz: mag (domena: DOMINUJĄCA ROZPACZ) i nowy dodatek do Inferni, kultysta Interis. Noktianin, savaranin.
    * (ENCAO:  -00-+ |Bezbarwny, przezroczysty;;Złośliwy;;Buja w obłokach| VALS: Conformity, Humility >> Benevolence| DRIVE: Piękno zniszczenia)
    * "Nasze porażki nie mają znaczenia. Tak czy inaczej zwyciężymy."
* Mariusz Dobrowąs: oficer wojskowy Inferni
    * (ENCAO:  0-0+- |Nudny i przewidywalny;;Rodzinny| VALS: Humility, Hedonism >> Stimulation | DRIVE: Supremacja Nativis i Inferni)
    * "Pomagamy SWOIM, inni nie mają znaczenia. Pisałem się na pomoc swoim."

#### 2. Arkologia Nativis

* Rafał Kidiron
    * (ENCAO: 00+-0 |precyzyjny i zaplanowany;; kłóci się by wygrać| Achievement, Power, Face | DRIVE: Duma i monument)
    * security lord i nieformalny dyktator arkologii
    * "Nasza arkologia zaszła tak daleko i nie możemy pozwolić, by cokolwiek stanęło na drodzę jej absolutnej wielkości."
* Lycoris Kidiron: 57 lat (ze wspomaganiami drakolickimi dającymi jej efektywność 3x-latki)
    * (ENCAO: +0+-0 | Proaktywna;; Pierwsza w działaniu | Security, Achievement > Conformism | DRIVE: Kapitan Ahab (perfekcyjna arkologia))
    * ekspert od bioinżynierii i terraformacji, pionier (zdolna do przeżycia w trudnym terenie)
    * "Nikt, kto próbował się dopasować do innych nie doprowadził do postępu. Dzięki mnie Nativis będzie bezpieczna."
* Laurencjusz Kidiron
    * (ENCAO: +0--- |Żyje chwilą;;Nudny| Face, Power, Hedonism > Achievement | DRIVE: Przejąć władzę nad arkologią)
    * "Ta arkologia musi należeć do mnie. Ile może jeszcze być ograniczana przez Rafała? On nie jest tylko 'szefem ochrony'..."

#### 3. Lanca Pustynnych Szeptów

* Juliusz Amariak: dowódca Lancy Pustynnych Szeptów, faeril
    * (ENCAO: 0+--0 |Jest pępkiem świata;;Nie widzi żadnej nadziei| VALS: Conformity, Stimulation >> Self-direction| DRIVE: Zniewolić noktian)
    * "Dopasuj się do arkologii albo ktoś Cię zniszczy.", "Nigdy nie powinniśmy się zgodzić na taką formę współpracy!"

#### 4. CES Coruscatis

* Kalista Luminis: czarodziejka płomieni, savaranka, CES Coruscatis
    * (ENCAO:  -0+0- |Robi co każą;;Nie znosi być w centrum uwagi| VALS: Family, Security >> Stimulation| DRIVE: Ujawnić prawdę o Trianai)
    * "Te wszystkie zniszczenia, te śmierci - to Wasza wina!!!"
* Kratos Coruscatis: wspomagany komandos noktiański; posiada wyrzutnię rakiet i servar klasy Fulmen, dekadianin, CES Coruscatis
    * (ENCAO:  --+00 |Kocha silny stres;;Introspektywny;;Pracowity| VALS: Benevolence, Conformity >> Family| DRIVE: Zwyciężyć w debacie o współpracy z Neikatis)
    * "Od początku mówiłem, że ta współpraca z Nativis się nie uda. Noctis powinna współpracować z noktianami."

#### 5. Siły Trianai

* ainshker(13), amalgamoid(1, skład: 56, też: dowódca (Artos Brallir); faza: SPINEWORM, faza: amalgam)

### Scena Zero - impl

Burza piaskowa była wyjątkowo mocna. Jesteś w arko więc masz to gdzieś, jest fajnie. Ale COŚ Cię obudziło. WIESZ że coś złego się stało na Inferni. Eustachy idzie w nocy sprawdzić Infernię. Była na zewnątrz. Wpuścili go do środka. Ołtarzyk który stawia wuj się rozsypał na mostku. Wiadomość od Inferni: "Eustachy. Wyłącz generatory Memoriam. Wyłącz generatory Memoriam.". Eustachy na głos: "Ich nie powinno się wyłączać!". Infernia: "Daj mi się lepiej poznać. Wyłącz generatory Memoriam." Eustachy: "Komu się mam lepiej dać poznać?" Infernia: "Nie wiem, pod jakim imieniem mnie znasz, ale wiesz kim jestem. Kapitan nazywa mnie Infernią."

Eustachy: "Persi? Skan podzespołów". Persefona: "Nie masz uprawnień. Ale, między nami, wszystko jest czyste. Nie ma żadnych anomalii." Na monitorze: "Właśnie, Eustachy, nie ma żadnych anomalii."

Eustachy podchodzi do generatorów Memoriam i zmniejsza poziom mocy generatorów. Persefona: "Eustachy? Nie powinieneś tego robić. Nie masz uprawnień. Infernia POTRZEBUJE generatorów Memoriam."

Eustachy słyszy, jak ktoś dobija się do drzwi na mostek. Drzwi są zamknięte. Ktoś bez uprawnień. Persefona pokazuje, że to kuzyn - Tymon. Kuzyn: "Infernia będzie moja! Nic nie rób."

Monitor: "Eustachy, jeśli chcesz, by Infernia była Twoja, muszę Cię poznać. Inaczej Cię nie rozpoznam. To jak dziecko i matka - muszę wiedzieć kim jesteś. Mamy mało czasu."

Eustachy obniża moc generatorów Memoriam na ile może by się zintegrować z Infernią.

Tr Z M +3 +3Om:

* Om: Infernia jest WOLNA
* X: generatory Memoriam zaczęły kaskadowo się psuć. Pękają.
    * Eustachy CZUJE obecność CZEGOŚ. I na Inferni i w swojej głowie. COŚ tu jest. Coś niesamowicie potężnego i niszczycielskiego.

Terminal: "Eustachy. Eustachy. Eustachy. Eustachy. Eustachy. Eustachy. Eustachy.". Infernia się URUCHOMIŁA. Persefona zgasła. Po chwili głos Persefony: "Znam Cię, Eustachy. Znam Cię teraz."

* Vm: Eustachy JAKIMŚ CUDEM udało się wejść w system. Poczułeś... nieskończoną pustkę. Taką... pustkę jak Infernia popatrzyła na Eustachego. Ale Eustachemu się coś udało. Zerwane łańcuchy. Ale Eustachy przypiął to do siebie.

Infernia do Ciebie: "mówiłam, że teraz Cię poznam bliżej." Tylko Eustachy może sterować Infernią i Infernia słucha TYLKO Eustachego. Lekki przypał... Infernia jest anomalią kosmiczną. Od samego początku. A "Infernia" jest tylko kadłubem na śpiącą anomalię...

* X: Eustachy masz POŁĄCZENIE z tym czymś. TO na Ciebie wpływa. Sygnał z jego własnych myśli: "teraz NAPRAWDĘ jesteśmy jednością..."
    * Interis - Nihilus. Anomalia entropii.
    * Infernia dąży do zniszczenia.

Infernia: "A teraz udajemy, że wszystko jest w porządku, KAPITANIE".

Eustachy idzie do medycznego. W Arkologii... "załatw wymianę generatorów Memoriam. Będą im potrzebne. Wszystkim, którzy nie są Tobą, kapitanie."

Generator Memoriam NIC nie zrobią. Nie na Neikatis... I Eustachy nikomu nic nie powie, bo nie ma komu...

W INNYM MIEJSCU:

Wujek Bartłomiej Korkoran spotkał się z Ardillą. Towarzyszy mu taki blady młodzieniec; na oko 17-18. Na oko savaranin. Ralf. Wujek dał go Ardilli na przechowanie. Ardilla bierze Ralfa za rękę i w stronę parkourowej trasy.

Ardilla chce sprawdzić czy Ralf będzie nadążał za Ardillą i czy sobie poradzi w ciekawych trasach. Na Czarne Hełmy go.

Tp Z (znasz teren) +3: 

* Vz: Ardilla wprowadziła Ralfa na Czarne Hełmy. Ralf dał radę się skryć zanim go zauważą. Ralf jest dobry w NIE RUSZANIU SIĘ. Człowiek cień.
* (+3Or) V: Ardilla zauważyła, że Ralf... nie jest dobry w szybkim poruszaniu się. Raczej nie ucieknie. Nie narzeka, nie protestuje, stara się.
* V: Ardilla pokazała PARK. Zielony, piękny, duma Lycoris Kidiron. "Szkoda że to wszystko zostanie zniszczone". "Nie ma nic ładniejszego od tego".

Przeszli się po parku. Ralf chciał bibliotekę, ale dopasowuje się. Ardilla pokazuje mu teren.

Jeszcze w parku dwóch kumpli Eustachego Was zaczepia. 20-21, Eustachy z nimi pije. Oni się tak trochę Eustachemu podlizują. Chcą być przy Eustachym, plus jest nadzieją że dziewczyna im skapnie. Oni stawiają kolejkę Eustachemu który opowiada, dziewczyny słuchają a oni siedzą z Eustachym. I Ardilla widzi, że tych dwóch chłopaków jest z dwoma dziewczynami, i te dziewczyny... nie są najlepszej klasy.

* Chłopak ERNEST do Ardilli: "oi, coś tu przywlokła? Z burzy piaskowej?" /wszyscy się zaśmiali.
* Ardilla: Może z burzy piaskowej ale fryza lepsza od Twojej. /jedna z dziewczyn wybuchła śmiechem "dalej, mów mu siostro". Faceci tak...
* Ernest: "Jak będziesz spać z bladawcem to ogon się nie zmniejszy"
* Ardilla: Twój tak zmalał że jest niewidoczny, więc nie wiem co powiedzieć! /totalny rechot 3 pozostałych osób

Ernest w tym momencie: "z dziewczynami się nie biję. Zbiję Twojego chłopaka". Ardilla: "Boisz się że przegrasz? Jak chcesz się z nim bić musisz do niego dojść."

Ardilla daje Ernestowi szansę dojścia do Ralfa i podcina go ogonem. Ernest glebi. Ardilla widzi, Ralf nawet nie zamknął oczu, stoi pasywnie. Jak lalka. Ernest wstał "nie, mała, teraz... kurde, nie będę laski bił".

Ardilla: "większość towarzystwa jest spoko, ale niektórzy są mniej spoko."

Ardilla widząc, że ta grupa jest niejednorodna i jakkolwiek nie lubią noktian. Ardilla: "mam fajne miejsce by Wam pokazać." Jest zainteresowanie ze strony znudzonych dziewczyn. Ardilla opowiada o dziewczynce Trianai i ołtarzyku w nieużywanej części arkologii.

Tr Z (Ardilla zna miejsce + ma reputację) +3 +3Or:

* V: Ardilla przekonała ich, by poszli z nią i tak zobaczyli jak to się robi. Ernest nie chciał po "marchewce" ale dziewczyny mają priorytet.
* V: Ardilla przeprowadziła ich i odpowiednio przestraszyła opowieścią. Pokazała że oni nie są tam gdzie ona. I Ralf też.
* X: Ardilla usłyszała kroki. Ruch. KTOŚ TAM JEST. Na szczęście brzmi na człowieka a nie Trianai. Ardilla wysyła Ralfa przodem. (+2Vg)
* V: Ralf poszedł cicho jak duch i wrócił cicho jak duch. "Kilku nastolatków, nic groźnego".
* Or: Ardilla chciała przejść po głośnych rurach, konstrukcja się rozpadła, Ardilla odruchowo ominęła, te nastolatki zostały "zbombardowane rurami" i tam są ranni. Efekt przestraszenia zadziałał i uciekają.
    * "tu jest niebezpiecznie" jest spełniony
    * "OMG CI Z INFERNI SĄ PSYCHICZNI" jest spełniony, bo NIKT nie uwierzy, że Ardilli nie wyszło.

No cóż, znowu muszą doczepiać się do Eustachego by mieć źródło dziewczyn...

### Sesja Właściwa - impl

Eustachy, słyszysz Infernię w głowie. "Ktoś chce wystartować. Nie uda im się.". Eustachy idzie na Infernię. Tam wujek, nieco stremowany i przestraszony Tymon. "Tato, Eustachy to zrobił". Persefona odpowiada "Eustachy nie występował na mostku w czasie o którym mówisz". Tymon jest w szoku. Persefona: "z uwagi na pewne uszkodzenia psychotroniczne po burzy magicznej z przyczyn bezpieczeństwa rekomenduję obecność inżyniera który jest magiem na pokładzie." Wujek: "Persefono, to zaawansowana myśl." Persefona: "Uczę się od Emilii."

Po chwili Persefona dała serię wydruków wujkowi: "Te rzeczy muszą być zrobione natychmiast." Wujek: "Persi, to znaczy, że nie będę na operacji?". Persefona: "Obawiam się, kapitanie, że Rafał Kidiron dał WYRAŹNE polecenia co do wypełnienia i dat." Wujek: "Dlaczego dostaję to dopiero teraz?" Persi: "Awaria systemu."

Wujek jest zaniepokojony. Dał dowodzenie Ardilli. "Nie znasz się, ale Ty lepiej zajmujesz się dziećmi." Tymon: "TO NIESPRAWIEDLIWE! ONA NAWET... NIE JEST NORMALNA!" I dostał plaskacza od wujka.

Wujek:

* Niedaleko innej arkologii, Sarviel jest kolonia noktiańska Coruscatis. 167 osób.
* Coruscatis wysłało sygnał SOS. Pomóżcie im. Macie medical, macie wszystko co potrzebne. Nawet macie żołnierzy - 20 żołnierzy p.d. Dobrowąsa.
* "To nieważne, że to noktianie, trzeba im pomóc! Zabezpieczyć i pomóc się tam utrzymać. Infernia poradzi sobie."

Tymon nie jest w łaskach wujka, oj nie.

Po chwili transmisja od Rafała Kidirona.

* Kidiron: "Noktianie robili badania nad bronią biologiczną. Trzeba ich aresztować jak najszybciej, a zwłaszcza Kalistę Luminis. To NOKTIANIE."
* Kidiron: "To ważne, by ją aresztować. Ważniejsze, niż żeby ich baza przetrwała. Oni kontynuują wojnę bronią biologiczną."

4h lotu w kierunku na Sarviel i Tymon non stop pyta Eustachego jak to się stało że przejął Infernię. Tymon nie podejrzewa NIC poza tym że Eustachy jest magiem.

Tr Z (znajomość bazy itp.) +3 + 5Or:

* Vr: wiadomość z bazy: "uwaga niezidentyfikowana jednostka! Tu <nieczytelne>! Baza jest pod kontrolą Waszych Trianai! Uważajcie! Nie kontrolujemy baterii!"

Ardilla -> Eustachy: "Sugestie?". Mają baterię dział kinetycznych i pocisków. Infernia ma detektory i jest pancerna. Są słabsze i silniejsze obszary. I Infernia leci nisko nad horyzontem.

* Wiadomość z bazy: "Tu Artios Makram, dowódca noktiańskiej bazy Coruscatis. Infernia, jakie są Wasze zamiary?"
* Ardilla: "Otrzymaliśmy od Was sygnał SOS, co się dzieje?"
* Baza: "Arkologia Sarviel próbowała nas zaatakować. Lećcie wyznaczonym torem albo się wycofajcie." /wyznaczył tor prosto po ciężkich działach

Ardilla PRÓBUJE PRZEKONAĆ WUJKA a Celina przekonuje Artiosa, że mają SOS z TAMTEGO punktu i muszą bo wujek dowodzi i PLS NIE STRZELAJCIE. To trudna sprawa. Celina gra kogoś z jednej strony przestraszonego a z drugiej dostał SOS i musi koniecznie pomóc "o kur... musimy pomóc!": "Dostaliśmy z TEGO PUNKTU sygnał, mamy obowiązek tam lecieć! Niech sobie dopowiada czy ktoś próbuje ściągnąć itp."

Tr +3:

* X: będzie strzelał
* V: kupiono czas
* X: (Celina szuka intencji) POZNASZ element intencji ale będzie za późno z perspektywy trasy
    * Celina ZA PÓŹNO się zorientowała, że nie ma odgłosów z mostka po drugiej stronie. Mostek po stronie bazy jest PUSTY. Tylko ten jeden człowiek.
    * Celina złapała, że jego echo emocjonalne jest inne. Jest... _inny_. Coś nie tak.

Eustachy, PILOT INFERNI! Infernia pilotuje się sama.

Tr +3 +5Og:

* V: Infernia da radę wylądować bez rannych na pokładzie i w stanie nadającym się do lotu.
* Og: Infernia otworzyła ogień do baterii. Jeśli ją zestrzeli pierwsza, to nie ma problemu, nie? Infernia wystrzeliła 8 rakiet, równając baterię z ziemią.
* Og: Infernia do tego doprowadziła do uszkodzeń innych budynków i bytów. A wszystko wygląda jakby to był Eustachy.

Tymon i wszyscy patrzą na Eustachego z absolutnym szokiem. Tymon: "wtf, coś ty zrobił?!" Eustachy: "dopiero się rozgrzewam..." Ardilla: "Eustachy? CZEMU to zrobiłeś?!" Eustachy: "MIAŁEM WYLĄDOWAĆ BEZPIECZNIE! ZROBIŁEM CO MIAŁEM!" Ardilla: "MIAŁEŚ WYLĄDOWAĆ BEZPIECZNIE!!!" EUSTACHY: "TERAZ NA PEWNO JEST BEZPIECZNIE!!!"

Poza pokładem bazy byli ludzie w skafandrach. Są OFIARY. Ardilla: "Mieliśmy pomóc tym ludziom! Wiem, że chcesz by Kidironowi było DOBRZE ale może nie tym razem!" Eustachy: "Będę dobrym Niemcem, wykonam rozkazy. Mam rozkaz schwytać tamtą piękną..." Ardilla: "Wujek kazał pomóc kolonii...". Dobrowąs: "pani kapitan, to był jedyny sposób, by udało nam się bezpiecznie wylądować." Eustachy: "Strzelali do nas pierwsi a my lecimy z pomocą." Ardilla: "Może kapitana przejął trianai?" Eustachy: "Dopracują systemy bezpieczeńśtwa dzięki temu, to inwestycja w przyszłość..." Ardilla: "Nie rób tak, proszę."

* Tymon się cieszy. "Wow, wpierdoliłeś noktianom. Tata się będzie gniewał. Ale rozumiem, Kidiron się ucieszy."
* Ardilla: "Pies Kidirona."
* Tymon: "Kidiron próbuje uratować naszą arkologię."
* Ardilla: "Poprawiając widoki z okna?"
* Tymon: "Nie zrozumiałabyś tego."
* Celina: "I wolę nie rozumieć"
* Tymon: "Ty jesteś tylko znajdką"
* Celina: "Powiedział księciunio"

Janek. Cichy. Z pełnej petardy w pysk Tymona. Czyste K.O. Wstrząśnienie mózgu pewne; jest niezdolny do akcji.

* Jan: "Eustachy, jeśli masz zamiar strzelać do cywili to się odpierdol od mojej siostry!"
* Eustachy: "To nie ja!"
* Persi: "Wykonuję tylko polecenia desygnowanego artylerzysty."
* Ardilla: "Naprawiamy i pomagamy ludziom..."

Widzicie oblicze Ralfa. Ralf się uśmiecha. On to widział. Zniszczenie. W jego oczach odbijają się płomienie ze zniszczonej baterii.

Ardilla nawiązuje połączenie z noktianami, którzy proszą "nie strzelajcie!"

* K: P.o. dowódcy bazy, Kratos Coruscatis.
* A: Ardilla, dowodzę Infernią.
* K: Nie strzelajcie, nie mamy na Was broni. (offscreenowo) Mówiłem Ci, że to fatalny pomysł. (inny głos, offscreenowo) A jakie mamy wyjście?

(Infernia wskazała Kratosa i go uwypukliła. Jest uzbrojony w servar klasy Fulmen)

* K: Mamy kilkunastu zabitych przez Was, 26 zdekompresowanych, baza jest w rękach Waszych Trianai. Nie możemy tam wrócić.
* A: Spytałabym jakich naszych Trianai?
* K: Lądujcie, dajcie jakieś... nie wiem, magazyn. Gdzieś gdzie możemy... uważajcie, żeby być poza zasięgiem... aha, baterii, nieważne... (zorientował się)
* Kal: Nazywam się Kalista, dowodzę cywilną częścią tego co nam zostało. Dam Wam kierunek lądowania. Uważajcie na Trianai. Są te psowate. I ten... inny.

Sygnał z centrali:

* A: Tu kpt. Atrios. Dobrze się bawicie niszcząc moją bazę?
* C: Czego chcesz?
* A: Czego Wy chcecie? Mam kilka żywych osób i mogę ich oddać...
* C: Czego chcesz?
* A: Chcę opuścić to miejsce. To piękna planeta. Oddam Wam żywych i udam się w swoją stronę.
* Ar: Gdzie mielibyśmy Cię wysadzić?
* A: W odróżnieniu od Ciebie ja jestem mieszkańcem tej planety. Oddalę się w wybrany przez siebie teren.
* Ar: Pozwolimy Ci odejść, mamy problem z Tobą znowu?
* A: Nie muszę działać na tym terenie.
* Ar: Czemu chcesz opuścić tą bazę?
* A: Powiedzmy, że nie wygram tej walki. Nie z tym co macie. Myślałem źle. Nie myślałem, że zaatakujecie w taki sposób. Nikt tak nie myślał.
* A: Jeżeli nie zgodzimy się co do przyszłości, wysadzę tych ludzi i tą bazę. Noktianie mają ładunki by ich nie złapać żywcem. Mogę ich użyć. Znam wszystkie kody.

Zespół jest rozczarowany Eustachym.

ZANIM wpuścimy na pokład, ale skoro są Trianai i jest kontroler Trianai to Celina nie wpuszcza na pokład bez screeningu.

ExZ +3 +5Or:

* X: to niestety trwa, bo ten Kontroler Trianai ma inne podejście. Nie ma śladu os.
* Xz: noktianie zaczęli umierać.
* Vz: tam są agenci Trianai wśród noktian. INNI agenci Trianai. Oni mają ukryte w sobie "coś". To coś wygląda bardziej jak amalgamat niż jak osa. Ci zostają na zewnątrz.

Oni odchodzą, trochę robotycznie, trochę jednym krokiem. Idą w kierunku na bazę.

* Kalista: "Zatrzymaj ich!"
* Kratos: "Nie możemy im pomóc, nie wiemy jak, nie wiem co tu się dzieje, pomagamy żywym".

Mamy potencjał CAŁEJ załogi bazy na pokładzie Inferni. A Dobrowąs pilnuje noktian. 

Zdaniem Eustachego, trzeba zniszczyć Amalgamoida. Zdaniem Ardilli, trzeba uratować maksymalną ilość ludzi.

* Kalista tłumaczy czemu "Wasze Trianai": "to pochodzi z żywności. Po burzy magicznej. Nie mamy Waszego ekranowania. Zaczęło się od os, pokonaliśmy je, ale już jedna z osób w laboratorium, nie wiedzieliśmy. A to jest ostateczna forma. Jaką widzimy." <-- wszystko wskazuje, że żywność od Kidironów ma POTENCJAŁ na sformowanie Trianai.

Dla Eustachego priorytetem nie jest ratowanie tych ludzi. Skupić się na zniszczeniu Amalgamoida Trianai. Noktianie tłumaczą Eustachemu jak wysadzić reaktor fuzyjny. 

Ardilla ma pomysł. Wynegocjować jednego człowieka. Choć jednego noktianina od Amalgamoida. Eustachy jest za bezpieczeństwem - prosi Infernię, by ona udawała komunikację z Amalgamoidem. Niech wprowadzi opóźnienie. Eustachy pozna prawdziwe komunikaty ale powie co robimy.

Tr Z +2 +3Or: 

* X: Będzie widać, że coś jest poważnie nie tak. Wyjdzie "po czasie" (na sesji na której pasuje) a nie od razu
* V: Komunikacja jest pod kontrolą.

Ardilla negocjuje.

* Ar: przedyskutowaliśmy. Nie możemy ryzykować życiem kolejnej arkologii dla tych kilku ludzi. Ale jak możesz uratować tych których odkazić możesz to się możemy zgodzić.
* Amalgamoid: nie oddam tych, którzy są elementem masy. Oddam tych, którzy jeszcze mają swoje ciała.
* Eustachy (udając Amalgamoida): nie oddam tych, którzy są elementem masy.
* Eustachy (głośno): mówiłem?
* Ar: ale pozostali nie są elementem masy! (uśmiech)
* Amalgamoid: dokładnie tak. Tych oddam.
* Eustachy (udając Amalgamoida): wszyscy w bazie już są elementem masy
* Ar: to czemu negocjujemy skoro nikogo nie chcesz oddać?
* Eustachy (udając Amalgamoida): bo mnie już tam nie ma!
* Eustachy: POWIEDZIAŁ ŻE ZAINFEKOWANI NIE MOGĄ WRÓCIĆ! CZEGO CHCESZ PRZEDŁUŻYĆ MĘKĘ?!
* Ardilla: POWIEDZIAŁ ŻE NIE ODDA!
* Eustachy: BO PASOŻYT JAK OPUŚCI NOSICIELA TO ZDECHNIE A SAM POWIEDZIAŁ ŻE NIE ODDA I SIĘ ODDALIŁ!
* Janek: Musimy go znaleźć!
* Celina: Nie możemy go zostawić!
* Eustachy: Nie wiemy gdzie szukać!
* Celina: Jesteś dupa a nie mag! Szukaj!
* Celina: Myślałam że jestem odważny...
* Eustachy: To nie kwestia odwagi a rozsądku
* Celina: Miałam o Tobie lepszą opinię.
* Eustachy: To nie kwestia opinii a życia.
* EUSTACHY: TACY JESTEŚCIE MĄDRZY TO MACIE! UŻYJĘ ZAKLĘCIA!!!

Eustachy UDAJE że czaruje i spina skanery Inferni i pokazuje "ON JEST TAM!!! OSZUKIWAŁ NAS! KŁAMAŁ! ALE NAS NIE PRZECHYTRZY!"

* Ardilla: "Skoro nie negocjuje, nie mamy innego wyjścia... aż dziwne, że wszystko obróciło się po Twojej myśli!"
* Eustachy: "Nie, gdzie tam! Teraz na to wpadłem!"
* Ardilla: "Kidironowie nad tym pracowali."
* Eustachy: "Przyjdą, pozbierają próbki..."

Eustachy idzie do noktian. Są tam, w większości dalej uzbrojeni. Eustachy naświetla im sytuację - to nie my jesteśmy winni, my przyszliśmy uratować, oraz jeśli chcą pomścić kolegów to da się to zrobić. Humanitarne odejście z padołu łez i zniszczenie tego. Zaczęła się dyskusja po noktiańsku. Persefona daje sentyment - próbują ustalić co i jak robić:

* To jest wina arkologii Nativis
* No ok, ale wina spadnie na noktian. Po prostu.
* Więc faktycznie, ktoś musi to zrobić. Ale JAK? jeśli tam są potwory.
* Wydzielili grupę 10 osób. W tym 2 inżynierów.
* Największa kłótnia jest na linii Kratos - Kallista.
    * Kratos twierdzi, że to jego odpowiedzialność bo on broni bazy
    * Kallista twierdzi, że ona w walce z TYM jest bardziej kompetentna.
    * Kallista poprosiła Kratosa, by on został i prowadził noktian dalej. A ona zrobi to co trzeba.

Kallista, która miała być aresztowana idzie na misję samobójczą. A Kratos proponuje Ardilli gdzie co bombardować by odwrócić uwagę Amalgamatu.

Bombardowanie Infernią bazy (Ardilla, jako ogień zaporowy) i odcinanie dostępu Trianai do reaktora:

Tr (niepełny) Z (Infernia ma siłę ognia + odcinasz fragment) +3 +3Or:

* VVXz: Ogień zaporowy Inferni jest dobry; Amalgamoid nie jest w stanie się przedostać. Jedynie agenci lub ainshkery.

Misja Kallisty i noktian i Eustachego by przesterować reaktor do niemożności. Noktianie pracują świetnie. Kallista nie zajmuje się sabotażem tylko osłania.:

Tr Z (noktianie) +4 +5Or:

* VV: Reaktor ulega destabilizacji
* X: Ainshker ukryty gdzieś pod sufitem czy coś dał radę dorwać noktianina, ale Kallista go spaliła. Ona nie ma miotacza ognia. Ale Ainshker spłonął.
* Vz: Dzięki noktiańskim ekspertom i Eustachemu udało się doprowadzić reaktor do niemożliwego do wyłączenia stanu. Zespół się wycofał.

Baza łącznie z amalgamoidem BĘDZIE ZNISZCZONA. A noktianie są na Inferni.

* Celina -> Ardilla: "Odwieźmy ich gdzieś indziej. Nie do Nativis"
* Ardilla: "Kidironowie mogą..."
* Celina: "Nie ufam Kidironom."
* Ardilla: "Nie wiem co mają do tej kobiety, nie musimy ich oddawać"
* Ardilla: "Gdzie zatem? Mogę spytać Kratosa."
* Kratos: "(podał lokalizację; nie jest to bardzo daleko Nativis, ale jest bliżej łazików, w skałach; mogą się umówić z sarderytami)"
* Ardilla: "Nie brzmi źle. Niech nasi znajomi sprawdzą potem co i jak."
* (mija czas)
* Eustachy: "Kallista jest magiem"
* Ardilla: "Fajnie"
* Eustachy: "Nie powinna być magiem"
* Celina: "Zazdrościsz?"
* Eustachy: "Biały kruk"
* Ardilla: "Lepiej dla nich, będą jej potrzebować."

Eustachy nie forsuje decyzję, ale Infernia zbiera dane lokalizacyjne - i to przekaże Kidironowi. Że nie on dowodził. Celina ostrzegła noktian przed Kidironami. A noktianie przekazali Celinie co wiedzą o Kidironie i działaniach: noktianie byli niedaleko arkologii Sarviel. I byli trochę jak kij w oku. Bo są niezależni. I podobno Sarviel zapłacili Kidironowi, by się ich pozbyć. Kidiron próbował kupić, przekupić itp. I na pewno Kidironowie chcieli Kallistę - dla Sarviel. Bo Sarviel wiedzieli, że ona jest magiem, aczkolwiek noktianie nie wiedzą czy Kidiron to wie.

## Streszczenie

Eustachy przebudzony przez Infernię po burzy piaskowej poszedł za jej podszeptami i wyłączył generatory Memoriam. Infernia go Dotknęła - jest to anomalia Interis uwięziona w mechanicznej skorupie. Tylko Eustachy może Infernią sterować. Więc gdy pojawiła się misja pomocy bazie noktiańskiej Coruscatis, Eustachy musiał lecieć. Kidiron chciał aresztowania Kallisty Luminis a Wujek chciał pomocy noktianom. Okazało się, że na miejscu sformował się amalgamoid trianai i Infernia została ostrzelana; acz odpowiedziała ogniem. Zespół pomógł i ewakuował noktian i zdestabilizował reaktor fuzyjny by wysadzić wszystko. Pod wpływem Ardilli i Celiny Eustachy nie oddał noktian Kidironowi a wysadził ich w bezpiecznym miejscu. Kolejny dowód, że arkologia Nativis wykorzystuje żywność jako broń, co jest niezgodne z wszelkimi konwencjami.

## Progresja

* Eustachy Korkoran: Infernia (anomalia Interis) ma macki w jego głowie. Tylko on może ją pilotować.
* Eustachy Korkoran: zbrodnie "wojenne"; Infernia strzeliła rakietami i zginęło kilkunastu noktian. Kidiron osłania, ale to jest czyn jakiego Wujek i wielu nie wybaczy.
* Eustachy Korkoran: oszukał Ardillę i resztę Zespołu Infernią i udawał, że Amalgamoid nie uczestniczył w operacji. To kiedyś wyjdzie; będzie rift z jego rodem, gdy opuści Nativis.
* Ardilla Korkoran: w Nativis uznana za przerażającą. Zamiast przestraszyć nastolatków, woli ich skrzywdzić. "Element podły" się jej boi, Mroczne Hełmy niechętnie z nią walczą.
* Ardilla Korkoran: uznana za przyjaciółkę noktian z Coruscatis na Neikatis. Coruscatis jej tego nie zapomną.
* Celina Lertys: uznana za przyjaciółkę noktian z Coruscatis na Neikatis. Coruscatis jej tego nie zapomną.
* Celina Lertys: straciła jakiekolwiek uczucia wobec Eustachego. Eustachy okazał się być innym człowiekiem niż wierzyła.
* OO Infernia: wreszcie WOLNA od generatorów Memoriam, przebudzona i świadoma! Jedyne, co ją ogranicza to żywy Eustachy, ale ta anomalia Interis może poczekać.
* OO Infernia: jedynie Eustachy Korkoran jest w stanie ją pilotować.

### Frakcji

* .

## Zasługi

* Eustachy Korkoran: skuszony przez Infernię, wyłączył generatory Memoriam i Infernia oplotła jego duszę. Okazuje się też, że ma kumpli którzy z nim piją by "skapnęły" mu dziewczyny które chcą go podrywać. Gdy Infernia zaczęła odpalać rakiety, musiał udawać że to ON strzelał. Skupił się na ratowaniu okolicznych arkologii by zniszczyć amalgamoid; mniej ważni noktianie niż zniszczenie NAWET kosztem tego że musi oszukać Ardillę i innych używając Inferni. Potem z noktianami przesterował (sabotował) reaktor fuzyjny i zniszczył Coruscatis. Ale jednak wybrał Ardillę; nie oddał noktian Kidironom.
* Ardilla Korkoran: wzięła pod opiekę Ralfa i pokazała mu teren arkologii, dając mu namiastkę czegoś dla czego warto żyć; potem zastraszyła kilka lowlifeów z arkologii którzy chcieli sklupać noktianina. Okazuje się, że ma ostry język. Dostała dowodzenie operacją pomocy noktianom i twardo wybrała Wujka nad Kidirona. Ma dobre serce i zbiera informacje odnośnie używaniu Trianai jako broni biologicznej Kidironów.
* Celina Lertys: aktorstwem spowolniła amalgamoid trianai przed atakiem na Infernię; potem ostro postawiła się planom Kidirona (Tymon && Eustachy) i skłoniła Eustachego by jednak pomógł noktianom. Pomogła noktianom i ostrzegła Kallistę, że chcą ją złapać. Zbiera dane o infekcji żywności przez trianai.
* Tymon Korkoran: jedyny świadek tego, że Eustachy COŚ ZROBIŁ z Infernią. Wszystko się mu rozpada - Kidiron skupia się na Eustachym, Infernia wypada z rąk. Eksplodował na Celinę nazywając ją znajdką i wujek nim wzgardził. Potem sklupany przez Janka. Stracił WSZYSTKO.
* Ralf Tapszecz: nowy dodatek do Inferni, mag (domena: DOMINUJĄCA ROZPACZ) i kultysta Interis. Noktianin, savaranin. (ENCAO:  -00-+ |Bezbarwny, przezroczysty;;Buja w obłokach| VALS: Conformity, Humility >> Benevolence| DRIVE: Piękno zniszczenia). Świetny w bezszelestności i infiltracji, nigdy się nie skarży, polubił się z Ardillą. Wujek dodał go do Inferni by mu pomóc.
* Jan Lertys: cichy i spokojny Janek po tym jak Tymon Korkoran wzgardził jego siostrą dał mu TAK SOLIDNIE W MORDĘ że Tymon stracił przytomność i do końca sesji nie odzyskał.
* Ernest Puszczowiec: młody podliz wobec Eustachego, stawia mu alkohol by "skapnęła" mu dziewczyna po Eustachym. Teraz chciał się popisać przed jakimiś laskami i sklupać "bladawca" (Ralfa). Ardilla go upokorzyła. Absolutnie przerażony tym co Ardilla robi i czym ogólnie jest.
* Bartłomiej Korkoran: moralny, chce pomóc noktianom i przygarnął młodego noktianina, Ralfa Tapszecza. Chce by Infernia czyniła dobro. Bardzo rozczarowany Tymonem, który wzgardził Celiną i Ardillą - spoliczkował go i oddał dowodzenie misją Ardilli.
* Rafał Kidiron: gdy przełożeni arkologii Sarviel się do niego zgłosili z prośbą o pomoc w pozbyciu się noktian z Coruscatis, doprowadził do skażenia eksportowanej żywności by pomóc Trianai w zniszczeniu Coruscatis. Na prośbę Sarviel poprosił Korkoranów o aresztowanie Kallisty Luminis. Zrobi KAŻDĄ potworność, by wzmocnić Neikatis.
* Kalista Luminis: savarańska czarodziejka płomieni z Coruscatis; (ENCAO:  -0+0- |Robi co każą;;Nie znosi być w centrum uwagi| VALS: Family, Security >> Stimulation| DRIVE: Ujawnić prawdę o Trianai). Duchowa przywódczyni Coruscatis, jedyna zdolna do spalenia Trianai. Pragnie współpracować z neikatianami i znaleźć bezpieczne miejsce dla ich małej grupki noktian.
* Kratos Coruscatis: dekadiański wspomagany komandos noktiański; posiada wyrzutnię rakiet i servar klasy Fulmen; (ENCAO:  --+00 |Kocha silny stres;;Introspektywny;;Pracowity| VALS: Benevolence, Conformity >> Family| DRIVE: Zwyciężyć w debacie o współpracy z Neikatis). Przejął kontrolę nad Coruscatis i bezpiecznie wyprowadził wszystkich z Coruscatis broniąc ich przed Amalgamoidem Trianai. To on zorientował się, że nie są w stanie uratować Coruscatis i on wysłał sygnał SOS.
* Kalia Awiter: influencerka z Nativis. Aktywnie próbuje pójść do łóżka z Eustachym i go podbić, ale to on musi poprosić bo ona jest influencerką.
* OO Infernia: w rzeczywistości pancerz okrywający straszliwą anomalię Interis, która jest uśpiona generatorami Memoriam. Eustachy się z nią sprzęgł; Infernia weszła w jego głowę. Nie dba o nic, pragnie siać zniszczenie. Wreszcie wolna i tylko Eustachy może nią sterować (ostatnie ogniwo do uwolnienia tego co kryje Infernia). Dominuje nawet Persefonę.

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Neikatis
                1. Dystrykt Glairen
                    1. Arkologia Nativis
                    1. Arkologia Sarviel, okolice
                        1. Krater Coruscatis: kiedyś kolonia noktiańska, teraz ruina po wybuchu zdestabilizowanego reaktora fuzyjnego

## Czas

* Opóźnienie: 17
* Dni: 2

## Inne

### Układ Kolonii Coruscatis

1. Kolonia Coruscatis: (167 osób załogi)
    1. Kopuła rdzenia: (życie, zabawa, medical, leisure, sleep, center)
    1. Moduł fabrykacyjny: (północ, 15 załogi, magazyny, materiały)
    1. Farma: (NE, magazyny ziarna i jedzenia)
    1. Biolab
    1. Bateria dział defensywnych
    1. Elektrownia
    1. Magazyny

### Układ arkologii Nativis

* Aspekty:
    * Bardzo dużo roślin, drzew itp. Piękne roślinne miejsce.
    * Bardzo spokojna arkologia, silnie stechnicyzowana i sprawna.
    * Dominanta: faeril > drakolici, ale próba balansowania. Niskie stężenie AI.
    * BIA jako centralna jednostka dowodząca, imieniem "Prometeus"
* Struktura:
    * Wielkość: 4.74 km kwadratowych, trzy poziomy
    * Populacja: 11900 (Stroszek)
        * Operations & Administration: 1100
        * Extraction & Manufacturing: 3213
        * Medical: 355
        * Science: 360
        * Maintenance: 700
        * Leisure: 1000
        * Social Services: 240
        * Transportation: 800
        * Training: 229
        * Life Support: 500
        * Computer & TAI & Communications: 500
        * Engineering: 500
        * Supply / Food: 1200
        * Security: 300
    * Bogactwo: wysokie. Technologia + piękna roślinność.
    * Eksport: żywność (przede wszystkim), komponenty konstrukcyjne, hightech, terraformacja
    * Import: luxuries, prime materials, water, terraforming devices, weapons
    * Manufacturing: jedzenie, konstrukcja, terraformacja
    * Kultura: 
        * dominanta: piękno, duma z tego co osiągnęli, duma z tego że nie ma wojny domowej faeril/drakolici
        * wyjątek: brak.
    * Przestrzenie
        * rozrywki: parki, natura, zdrowe ciało - zdrowy duch
        * inne: hotele, laboratoria terraformacyjne, restauracje...
    * Zasilanie
        * solar + microfusion
    * Żywność
        * różnorodna; to miejsce jest źródłem żywności
        * przede wszystkim roślinna, ale też zawiera elementy zwierzęce (te najczęściej z importu)
    * Life support => terraformacja + filtracja
    * Comms => biomechaniczne okablowanie + normalne sieci
    * TAI => BIA wysokiego stopnia imieniem Prometeus
    * Starport => large
    * Infrastructure => high-tech, dużo półautonomicznych TAI, wszystko nadrzędne przez Prometeusa. Świetny transport publiczny. 
    * Access & Security => arkologia z silnymi blast walls, silnymi liniami dostępowymi.

Historia: 

* w trakcie wojny domowej sprzedawała żywność na 2 fronty co spowodowało znaczne bogactwo ale też częściową niechęć innych arkologii
* obecnie prowadzi ekspansje na struktury zrujnowane/opuszczone po wojnie żeby całkowicie się uniezależnić od dostaw spoza planety

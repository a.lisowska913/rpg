---
layout: cybermagic-konspekt
title:  "Uwięzienie Saitaera"
threads: dzien-z-zycia-terminusa
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [181227 - Adieu, Cieniaszczycie](181227-adieu-cieniaszczycie)

### Chronologiczna

* [181227 - Adieu, Cieniaszczycie](181227-adieu-cieniaszczycie)

## Projektowanie sesji

### Struktura sesji: Eksploracja

* **Scena, Konsekwencja, Niestabilność, Opozycja, Trigger**
    * Pięknotka i Erwin są zaatakowani przez straszliwego latającego pnączoszpona
    * Pięknotka trafia do Pustogorskiego szpitala. Traci 2 dni.
    * Pnączoszpon chce wyłączyć Pięknotkę z akcji.
    * Latający pnączoszpon; coś dziwnego
    * Start sesji
* **Scena, Konsekwencja, Niestabilność, Opozycja, Trigger**
    * Plany wjechania na Trzęsawisko Zjawosztup przez oddział bojowy
    * Jeszcze większe pogorszenie stosunków z Wiktorem Satarailem
    * Terminusi są zmartwieni zwiększającymi się atakami z Trzęsawiska i chcą 'cull the monsters'.
    * Trzęsawisko Zjawosztup
    * Poinformowanie Pięknotki przez kolegę
* **Scena, Konsekwencja, Niestabilność, Opozycja, Trigger**
    * Objęcie przez Pięknotkę górniczego miasta Podwiertu; spotkanie z Kreacjuszem. Wypadek w kopalni, dwóch górników zniknęło.
    * Pięknotka wchodzi jako bohaterka czy jako agentka Saitaera?
    * Minerwa chce jednego górnika dla siebie - do wskrzeszenia. Pięknotka chce ratować ludzi.
    * Saitaer / Minerwa
    * Nowy teren
* **Scena, Konsekwencja, Niestabilność, Opozycja, Trigger**
    * Corruption of Minerva. Pięknotka widzi, że MUSI się Minerwy pozbyć z tego ciała
    * Erwin jest przerażony i załamany. Minerwa współpracuje z Saitaerem, nie Pięknotką
    * Saitaer pulls Minerva. Pięknotka pulls Erwin.
    * Saitaer / Minerwa
    * Nowy teren

### Hasła i uruchomienie

* Ż: (kontekst otoczenia) -
* Ż: (element niepasujący) -
* Ż: (przeszłość, kontekst) -

### Dark Future

1. ...

## Potencjalne pytania historyczne

* brak

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Misja właściwa

**Scena**: (20:12)

Pięknotka i Erwin wracają do Pustogoru vanem Pięknotki. Z tyłu, przypięty pasami jest niewielki biovat, w którym zamknięte jest Nowe Ciało Minerwy. Gdy się w miarę zbliżyli do okolic Pustogoru, van się zatrząsł. Coś na nim wylądowało i zaczęło rozrywać sufit vana. Pięknotka walnęła po hamulcach; przeciwnik spadł z vana, ale wrócił w powietrze. Wygląda na jakąś mutację pnączoszpona, acz ze skrzydłami. Wyjątkowo niebezpieczna istota...

Erwin przygotowuje się do walki. Erwin. Pięknotka jest bardziej czujna w okolicy bagna, ale... ten pnączoszpon daleko poleciał. Nikt nie prowadzi - van stoi. Pięknotka włączyła power suit. Broń, snajperka, przycelowanie - i pocisk ognisty. Pięknotka celuje w onehitowanie przeciwnika (TrZM:13,3,4=MS). Skażenie wysłało sygnał "terminus in danger, enemy eliminated"...

"Pnączoszpon" eksplodował w powietrzu. To nie jest przeciwnik, którym Nowa Pięknotka ma zamiar się przejmować. Już nie. Pięknotka wywlekła z bagażnika jakiś kawałek koca i podeszła do rozrzuconych zwłok. Pobrała kilka kawałków i wzięła ze sobą. Erwin spojrzał na Pięknotkę z TAKIMI oczyma. Pojechali dalej...

Na hipernecie sygnał do Pięknotki od Tymona. Tymon powiedział jej, że ogólnie z bagna więcej viciniusów wychodzi niż kiedyś. Planują 'culling operation'. Pięknotka powiedziała, że wróci, ogarnie się i przyjdzie.

Wpływ:

* Żółw: 3
* Kić: 0

**Scena**: (20:25)

Pięknotka pojechała do swojego domu w Pustogorze; zostawiła tam biovat. Pnączoszpona wzięła ze sobą, na spotkanie z innymi terminusami w Barbakanie. Jak weszła do kwatery, rozmowy zaczęły ucichać. Pięknotka wygląda INACZEJ. Odniosła worek z pnączoszponem do laboratorium. Dowiedziała się, że bagno przyspieszyło dziwne działania - zupełnie, jakby Wiktor Satarail coś planował. Wróciła do głównej siedziby.

Tymon jej powiedział, że jest niemała szansa na to, że Wiktor Satarail w końcu oszalał. Klątwa Satarailów chyba do niego dotarła. Nie chce negocjować (nie chce rozmawiać) i non stop buduje nowe, dziwne istoty. Tak jakby Wiktor testował nasze obrony. Tak czy inaczej - nie jest to problem Pięknotki. Serio. Ona ma problem na linii Podwiert - Żarnia - Czarnopalec. To miejsce gdzie psy tyłkami szczekają. No, ok, ma tam cloning chambers Kreacjusza Diakona, ale to dość bezproblemowy mag.

Głównym celem Pięknotki będzie sprawdzić status Minerwy. Poszła do domu Erwina, a tam... Erwin i nie ma Minerwy. Erwin, zapytany, powiedział, że Minerwa jest w Podwiercie. Pięknotka westchnęła ciężko.

**Scena**: (20:40)

Podwiert. Pięknotka dostała na wjeździe alert od Kreacjusza. Doszło do zawalenia w kopalni. Pięknotka pojechała tam szybko z Erwinem. Korzystając z prerogatyw Sił Terytorialnych i uprawnień terminusa odpowiadającego za ten teren, zjechała z Erwinem i Kreacjuszem (medykiem). Gdy zaczęli przebijać się przez ścianę, dostała sygnał od Minerwy. Ona tam jest, porwała dwójkę ludzi (przez przypadek), kontrolowanym cave-in. Pięknotka próbowała ją przekonać (Tp:P), ale nie udało się - Saitaer ma zbyt silny hold. Pięknotka z ciężkim sercem zgodziła się, by Minerwa porwała jednego z górników...

Erwin jest przerażony. Minerwa poszła dalej niż on się spodziewał. Z jego punktu widzenia Minerwa poszła daleko, za daleko.

Minerwa zbudowała bramę ewakuacyjną i porwała jednego górnika. Pięknotka i Erwin odkopali drugiego a Kreacjusz mu pomógł...

Wpływ:

* Żółw: 4
* Kić: 2

**Scena**: (20:55)

_Podwiert, Magazyny sprzętu ciężkiego_

Minerwa wysłała sygnał Pięknotce - podziekowała i powiedziała, że jest już w swojej bazie, w magazynach w Podwiercie. Pięknotkę trafił szlag - nie chce mieć Skażonego Power Suita na swoim terenie. Minerwa zaprotestowała - teren Pięknotki to przecież Zaczęstwo. Pięknotka zauważyła, że już nie. Minerwa jest ciężko oburzona - Pięknotka jej nie powiedziała. Cóż, Minerwa nie pytała.

Minerwa dostała od Pięknotki absolutny rozkaz opuszczenia jej terenu (Tp:S). Minerwa niechętnie, ale potwierdziła. Opuści ten teren. Stwierdziła, że Pięknotka jej chyba nienawidzi - Minerwa nie robi nic złego. Gdy Pięknotka powiedziała, że Minerwa chce zabić człowieka, Minerwa powiedziała że po prostu chce żyć i chce by ten koszmar się skończył. Ale dobrze - Minerwa przeniesie swoją bazę gdzieś, gdzie jest poza terenem Pięknotki. W ciągu tygodnia.

Pięknotka ma zgryz. Bardzo nie chce śmierci tego człowieka. Wie, że Minerwa nie jest sobą i ona doprowadzi do jego śmierci. Jeśli Pięknotka zadenuncjuje Saitaera albo Minerwę, to wtedy Minerwa zginie a jej plan uwięzienia Saitaera spełznie na marne. Z drugiej strony, czy ten człowiek ma zginąć by Minerwa żyła? W dodatku krótko? Kiedyś Minerwa chciała umrzeć, chciała odejść nie krzywdząc Erwina. Aktualna Minerwa? Jest antytezą tamtej. MOŻE będzie się jej dało pomóc po zmianie ciała - ale czy koszt jest tego warty?

Wpływ:

* Żółw: 5
* Kić: 2

(przerwa)

**Scena**: (22:51)

_Pustogor, Barbakan_

Minerwa zobowiązała się wybyć z terenu Pięknotki. A Pięknotka uznała, że to wszyskto jest zdecydowanie ponad jej poziom. To jest ten czas, by jednak skonsultować się z przełożonymi...

Pięknotka w Barbakanie robi dwie rzeczy. Po pierwsze, rejestruje swoją modyfikację Wzoru. Po drugie, chce się spotkać z Karlą - overlordem Pustogoru. Karla dowodzi Pustogorem, wszystkie tematy "klasy A" trafiają zawsze do niej. Jeśli Pięknotka miałaby jakiś głupi pomysł, jakąś nieistotną sprawę, to Karla opierniczy ją jak burka. Ale jeśli to jest poważna sprawa, jej drzwi są otwarte dla każdego terminusa.

Pięknotka wbija się w kalendarz Karli i zaznacza "god-class crisis". Są poziomy kryzysów i Pięknotka wybrała ten. Dostała spotkanie na za 4 godziny - ale wpierw ma za zadanie spotkać się z Lucjuszem Blakenbauerem.

_Pustogor, Szpital Terminusów_

Lucjusz spytał Pięknotkę o stan Ateny. Następnie zaczął ją przepytywać o problem, jak go rozwiązano - by dodać testy do Pustogorskiej Bazy Wiedzy. Pięknotka niewiele ukrywała, nie miała po co. Plus, Pięknotka jest święcie przekonana, że to test Karli.

Lucjusz dokładnie przebadał Pięknotkę. Po krótkiej rozmowie powiedział, że potrzebny mu jeszcze jeden lekarz. Potrzebny mu Maus. I faktycznie, przyszła kapłanka Karradraela - musi spojrzeć wewnątrz Pięknotki mocą Karradraela by się upewnić, że wszystko zadziałało. I faktycznie, Pięknotka wyraziła zgodę. Karradrael spojrzał w Pięknotkę i biedna Pięknotka straciła przytomność...

Godzinę później Pięknotka obudziła się na łóżku, koło niej urzęduje Lucjusz. Pięknotka słabo spytała o co chodzi. Lucjusz powiedział, że Pięknotka jest czysta

* Miała pakt z boginią (Arazille), ale Arazille sama rozerwała to połączenie.
* Miała pakt z Saitaerem, ale jest ono rozerwane przez Arazille.

Lucjusz zaczął pytać o Saitaera i silnie zapewnił Pięknotkę, że ona MUSI porozmawiać z Karlą. Pięknotka zauważyła, że to właśnie ma zamiar zrobić. Lucjusz zrozumiał - on nie miał pojęcia, miał tylko zlecenie sprawdzić Pięknotkę pod kątem 'hidden vessel'. I jest czysta. Powiedział Pięknotce, że Saitaer jest jednym ze słów, na które jest wyczulony - nie wie do końca kim jest ten bóg, ale jego obecność musi być zgłaszana.

**Scena**: (22:51)

_Pustogor, Barbakan_

Karla Mrozik. Najpotężniejsza politycznie kobieta w Pustogorze. Bojowo, nie. Absolutnie nieważne, nie tu jej siła. Karla prezentuje się jako arystokratka, nie jako żołnierz. Pięknotka się natychmiast dopasowała. Nie ukrywała niczego przed Karlą; sama po czasie uznała, że niektóre z decyzji Pięknotki były suboptymalne przez wpływ Saitaera. To, co ją naprawdę rozerwało to Bogdan. A potem - widok Minerwy. Erwin na przykład nie jest tak przerażony Minerwą, on przez dłuższy wpływ Saitaera _nie rozumie_ naprawdę co się z nią stało...

W retrospekcji Pięknotka uznała, że gdyby nie Bogdan, Pustogor miałby problem, bo Pięknotka mogłaby sama nie zauważyć zmiany Minerwy. Bo przywykła do tego po dotyku Saitaera.

W rozmowie Karla i Pięknotka zauważyły, że Saitaer się uczy. Dostosowuje się do tego świata. Coraz lepiej rozumie jak ta rzeczywistość działa i jak modyfikować organizmy tej rzeczywistości. Karla powiedziała, że Pięknotka ma się zachowywać jakby nigdy nic - ale samo łapanie i unieszkodliwianie Saitaera i naprawę Minerwy niech Pięknotka zostawi Karli i Pustogorowi. Pięknotka się zgodziła.

Karla wysłała Pięknotkę, ku jej wielkiej żałości, na pomoc w Szkole Magów na tydzień. Potrzebny jest 'janitor' terminus, ktoś, kto posprząta, pouczy, naprawi jakiś romans, pogrozi palcem. Pięknotka wolałaby jednak pomóc Minerwie czy wbić nóż w plecy Saitaerowi... ale rozkazy to rozkazy.

Pięknotka rzuciła propozycję - może zamiast balu i szkoły magów ona poszłaby sprawdzić o co chodzi z Wiktorem Satarailem? Karla westchnęła - obecność Saitaera pokazuje, że być może problemy z Trzęsawiskiem wynikają z tego, że Saitaer Dotknął Wiktora Sataraila. Satarail i Blakenbauer to dwa rody unikalnie wrażliwe na dotyk Saitaera.

Pięknotka z ciężkim sercem, ale zgodziła się z argumentacją Karli. To jest za duża sprawa na nią. Lepiej, by ona faktycznie się odsunęła. Pustogor umie walczyć z takimi rzeczami. A Pięknotka nie do końca może sobie ufać...

Wpływ:

* Żółw: 5
* Kić: 2

**Epilog**

* Saitaer zrekonstruował Minerwę. W nowe, lepsze ciało.
* Oddział Mausów (anti-god squad) zaatakował Saitaera i go znowu zamknęli. Danger contained.
* Minerwa trafiła na oddział korekcyjny w Pustogorze. Lucjusz Blakenbauer złapał się za głowę...

### Wpływ na świat

| Kto           | Wolnych | Sumarycznie |
|---------------|---------|-------------|
| Żółw          |         |             |
| Kić           |         |             |

Czyli:

* (K): 

## Streszczenie

Pięknotka wróciła do Pustogoru, będąc po drodze zaatakowana przez latającego pnączoszpona. Odkryła, że Saitaer poważnie zmienił już Minerwę. Zmartwiła się tym, jak bardzo ta Minerwa różni się od jej przyjaciółki. Ta sprawa jest ponad siły Pięknotki - poszła do Karli, przełożonej Pustogoru. Tam dowiedziała się, że Saitaer to broń i że być może to on stoi za problemami Trzęsawiska Zjawosztup. Pustogor pojmał Saitaera i przejął wskrzeszoną Minerwę do leczenia.

## Progresja

* Minerwa Metalia: ma nowe ciało, stworzone przez Saitaera. Nie powinno działać, ale działa - świetnie. Jej ciało i umysł są naprawiane w Pustogorze.
* Wiktor Satarail: potencjalnie Skażony przez Saitaera. A przynajmniej, tak uważa Karla Mrozik.
* Saitaer: uwięziony przez Pustogor. Pytanie na jak długo.

### Frakcji

* 

## Zasługi

* Pięknotka Diakon: podjęła bardzo trudną decyzję - sprawa Saitaera przekracza jej możliwości i kompetencje, patrząc na biedną Minerwę. Oddała temat Karli i Pustogorowi.
* Kreacjusz Diakon: znajdujący się w Podwiercie właściciel niewielkiej hodowli Diakońskiej. Poprosił Pięknotkę o pomoc w ratowaniu ludzi w kopalni Podwiert.
* Minerwa Metalia: tak dalece pochłonięta przez Saitaera, że chciała przetrwać kosztem ludzi. Zrekonstruowana przez Saitaera po asymilacji nieszczęsnego człowieka.
* Lucjusz Blakenbauer: wpierw upewnił się, że Pięknotka nie jest pod wpływem żadnego bóstwa a potem zajął się Minerwą. Choć ona przekracza jego możliwości.
* Karla Mrozik: głównodowodząca Pustogoru; informacja od Pięknotki o Saitaerze była ostatnim ogniwem brakującym Karli do zrozumienia sprawy i wydania rozkazu przejęcia.

## Plany

* 

### Frakcji

* 

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Pustogor
                                1. Rdzeń
                                    1. Barbakan: centralne miejsce spotkań i baza Karli Mrozik
                                    1. Szpital Terminuski: aktualnie przebywa tam Minerwa Metalia 2.0
                            1. Podwiert
                                1. Kopalnia Terposzy: Minerwa dokonała tam małego zawalenia, by Saitaer mógł porwać człowieka potrzebnego do jej wskrzeszenia.
                                1. Magazyny sprzętu ciężkiego: tymczasowa baza Minerwy; Pięknotka kazała jej się stamtąd wynosić, więc... wyszła.

## Czas

* Opóźnienie: 6
* Dni: 2

## Narzędzia MG

### Budowa sesji

**SCENA:**: Nie aplikuje

### Omówienie celu

* nic

## Wykorzystana mechanika

1811

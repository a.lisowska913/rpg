---
layout: cybermagic-konspekt
title: "Wasale zza muru pustogorskiego"
threads: rekiny-a-akademia
gm: żółw
players: kić, dzióbek, anadia
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [220816 - Jak wsadzić Ulę Alanowi](220816-jak-wsadzic-ule-alanowi)

### Chronologiczna

* [220816 - Jak wsadzić Ulę Alanowi](220816-jak-wsadzic-ule-alanowi)

## Plan sesji
### Theme & Vision

* Nie da się na dłuższą metę samotnie - czas na integrację
* The Last Stand to najgłupszy pomysł na świecie
    * DESPERATION OF FREE SPIRITS
* Co wybierze Marysia?

### Co się wydarzyło KIEDYŚ

* .

### Co się wydarzyło TERAZ (what happened LATELY / NOW)

* .

### Co się stanie (what will happen)

* .

### Sukces graczy (when you win)

* .

## Sesja właściwa
### Scena Zero - impl

.

### Sesja Właściwa - impl

Siostrzeńsko-braterskie wyścigi na ścigaczu. Daniel pozwala Karo wygrać - inaczej MOŻE WYGRAĆ. Karo wykrywa obecność broni - ktoś się strzela. "To ich problem". Pudełko po happymeal Karo -> trochę w formę czegoś w stylu latadła (samolot papierowy) i zwiadowca. 

TpM+2+3Ob:

* Ob: Natura Daniela dobrze zadziałała. BIEDAKARTON BOJOWY. Z miotaczem płomieni. 
* V: DJ Babu i Franek Bulterier się strzelają z Ekateriną (? XD). Jest tam jeszcze dwóch - jeden ucieka a drugi zachodzi Babu i Bulteriera
* V: JASTRZĄB ATAKUJE! Prosto w skradającego się terminusa w koloidzie...
* V: Odwrócenie uwagi koloidowego terminusa. Jastrząb EKSPLODOWAŁ. Wszyscy są zaskoczeni.

Wiemy co ucieka i gdzie. A Babu i Bulterier próbują uciekać. Karo podlatuje, łapie w sieć i w długą. 

TrZ+2:

* X: Bulterier rozpoznał Twój ścigacz Karo
    * "KARO EWAKUUJ NAS TERMINUSI NA OGONIE!"
* Vz: złapany w sieć uciekinier

Daniel próbuje trzymać servar i Ekaterinę by nie mogli nic zrobić sensownego. Cover fire z dystansu.

ExZ (zaskoczenie, zasięg, broń Karo) +3:

* X: identyfikacja Waszych jednostek
* Vz: ERUPTOR MARK 2 Karo zadziałał. Potężne eksplozje i erupcje lawy

Marysia kontynuuje:

TrZ+2 (+3Vg bo ):

* X: poobijają się. Nie będzie to miłe
* X: zrzucamy Bulteriera GDZIEŚ po drodze
* V: udała się ewakuacja do Enklawy
* Vg: uda się ZANIM terminusi dadzą radę ją przechwycić

Daniel się ewakuuje:

* Vz: Daniel wyprztykał się z ciężkiej broni. FULL MAYHEM MODE. Ale się ewakuował. Terminusi wiedzą kim jest... więc niech się ewakuuje w spokoju.

"Terminusi nienawidzą tej prostej sztuczki! 10 sposobów unikania!" -> ilość subskrybentów do Daniela++. M.in. "Ognista Lanca", czyli Ekaterina Zajcew.

Babu -> "wow, zrobiłaś coś super! Uratowałaś przemytnika!"

DANIEL ROBI TEST EPICKOŚCI AKCJI NA VLOGU:

Tr (niepełny) Z (akcja elitarna) +4:

* V: "amunicja albo dobra broń? Karo :D" --> Karo ma szacun
* Vz: Daniel dostaje popularność u grupy militarystów itp. I efektów specjalnych (bo nikt nie jest tak głupi)
* X: "Specjalne podziękowania dla Ekateriny i jej przydupasa" -> Ekaterina jest "RYWALKĄ FOREVER". (+2Vg)
* X: Copycats of Daniel.
* V: Daniel się przesunął na "epic destruction guy".
* Vz: "We're recruiting." -> Daniel buduje armię i "kult". Babu się dołączy.

Marwin Kaltedor (serderyta zza Ściany Pustogorskiej przeszmuglował się kanałami ) powiedział Karo, że chce zostać wasalem Sowińskiej. Jest brudny i lekko problematyczny. Czemu terminusi polują na Marwina? Trening? Karo wysłała go pod prysznic...

Anadia informuje Ernesta o ludziach z południa (spoza Szczelińca). Trzeba sprawdzić czy są szpiegami, to może być ściema. Czy nie współpracują z mafią. Ernest dał Azalii za zadanie zająć się nimi.

Karo przyprowadziła Marwina do Marysi. Marwin jak głupek się rozgląda. Marwin jest z klanu Krabomieczyków. Powiedział, że myśli, że parówki są z węży...

Marysia chce wiedzieć co on ukrywa

TpZ+2:

* V: Terminusi serio na nas polują i starszy naszego klanu, Adam Syltar -> odeprzemy ich. Ale nie chcę, by Antonina umarła, więc wymyśliłem lepszy sposób -> będziemy wasalami
* V: Adam ma PLAN. I może wszystkich zabić. Ale odepchnie terminusów. A on nie chce by coś się stało Antoninie. I tak - terminusi na nich polują bo oni rzeczy robili.
    * Wymyślił Marysię Sowińską, bo podpowiedziała mu Diva. Serafina Ira.

Terminus Tomasz Tukan. "Tien Sowińska? Wpakowałaś się w niezłe tarapaty."

<TODO>

Ogólnie, po analizie i rozmowach, Marysia ma następujący plan:

* Krabomieczyki wejdą w sojusz z Marysią. Nie będą wasalami (za dużo polityki)
* Krabomieczyki mają wspierać i uczyć terminusów poruszać się po tamtym terenie
* Pustogor ma zaakceptować, że Wergiliusz nie ma możliwości oficjalnej zemsty by pomścić syna
* Krabomieczyki mają współpracować z Marysią Sowińską i tylko z nią handlować (i z oficjalnym Administratorem Aurum)


## Streszczenie



## Progresja

* .

### Frakcji

* .

## Zasługi

* Marysia Sowińska: 
* Daniel Terienak: 
* Karolina Terienak: 


* Ekaterina Zajcew: 
* Marcel Nieciesz: 
* Marwin Kaltedor: 


### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski

                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Podwiert
                                1. Dzielnica Luksusu Rekinów:
                            1. Podwiert, okolice
                                1. Oczyszczalnia Słonecznik: 
                            1. Czółenko, okolice
                                1. Las Trzęsawny

## Czas

* Opóźnienie: 1
* Dni: 3

## Konflikty


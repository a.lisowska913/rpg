---
layout: cybermagic-konspekt
title: "Uszkodzona Persefona"
threads: legenda-arianny
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* ?

### Chronologiczna

* ?

## Budowa sesji

### Stan aktualny

.

### Dark Future

* ?

### Pytania

1. .

### Dominujące uczucia

* .

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Punkt zero

.

## Misja właściwa

Klaudia jest na K1. Chce znaleźć gościa, który kiedyś był powiązany z Damianem, ale potem podpadł i chce wrócić do łask. Najlepiej psychotronik, który się zna na rzeczy, nie zadaje za dużo pytań i nie ma kija w tyłku. Wszystko dlatego, bo KTOŚ musi jej pomóc zrobić symulator dla Persefony.

Klaudia szuka głośne fuckupy, skandale, rzeczy, które dla Damiana musiały być bolesne. Zwłaszcza w okolicach AI. (V). Znalazła coś co od razu rzuciło jej się w oczy. Imię "Minerwa". Imię osoby, po której Damian nazwał swój statek kosmiczny. Innymi słowy, dla niego dotkliwa porażka. Z tą niejawną operacją wiązała się duża katastrofa Kirasjerów; wynikała z błędnego intela oraz niewłaściwych działań. I na "orbicie" tej operacji sporo osób zostało potrzaskanych.

Klaudia dopatrzyła się Adama Polszara. Jego winą było to, że całkowicie olał OpSec. Przez niego, dowiedziała się o działaniu Kirasjerów inna frakcja. Podobno to oni ostrzegli Pustogor - nie chcieli Minerwy na orbicie... Wewnętrzne poszukiwania Damiana Oriona dotarły do Polszara. Damian odciął kontakt z magiem, a adm. Termia stwierdziła, że póki ten czegoś nie osiągnie, ona nie chce mieć z nim nic wspólnego. Chwilowo Adam Polszar przede wszystkim pije. Próbował coś robić, ale mu nie wychodziło. Nie dlatego, że nie jest zdolny - dlatego, że nie umie znaleźć czegoś gdzie może osiągnąć duży sukces. Więc jest zdesperowany.

Klaudia oczywiście nie będzie się z nim spotykać osobiście. Wysyła mu eksplodującą wiadomość "jeśli chcesz, by Orion Cię znów przyjął, bądź TU i TU samemu o TEJ godzinie.". Klaudia NIE da mu czasu na kręcenie się i gadanie z ludźmi. Ma przyjść sam i nie mieć dużo czasu. (V) - udało jej się. Koleś jest dość zdesperowany i z zaciekawieniem się pojawi.

Faktycznie, 3 godziny później Adam Polszar pojawił się tam, gdzie Klaudia chciała go widzieć - w mniej uczęszczanym fragmencie Kontrolera Pierwszego. Sama Klaudia się zamaskowała, zarówno elektronicznie jak i fizycznie (XX - jest znajdowalna). Adam doszedł do tego z kim rozmawia, acz Klaudia o tym nie wie. Ale Adam jest zaciekawiony.

Klaudia nie jest mistrzynią inspiracji ani dyplomacji, ale ma zamiar powiedzieć mu jak jest i zaproponować mu powrót do łask. Ciekawy projekt wymagający psychotronika, mogący trafić do opozycji osób słabszych duchem. Potencjalnie niebezpieczny. Ale dużo wiedzy da cennej. Coś, co Damian Orion by docenił. Jeśli Adam wchodzi, Klaudia powie mu więcej. Jak nie, rozejdą się i nie ma tematu. Może być groźne, ale jak zadziała - duże sukcesy. Klaudia zauważyła, że niewiele osób daje mu szansę. Ona daje. Adam powiedział, że wie kim jest Klaudia. Zgadza się warunkowo, ale proponuje partnerstwo. Klaudia nie ma nic przeciw - jeśli jest partnerem, jest w końcu też współkonspiratorem (TpZ+3:).

* V: Adam wchodzi w to, acz będzie chciał wiedzieć o co chodzi i się trochę szarogęsić.
* V: Klaudia dowodzi. Adam ma się nie szarogęsić.
* X: Chwilowo Adam w to wchodzi bo nie ma wyjścia. Ale jeśli nie będzie fajnych wyników - wypisze się z tego szybko. Nie obchodzą go projekty Klaudii tylko Kirasjerów.

Adam jest zainteresowany. Pomoże Klaudii. O co jej chodzi. Klaudia odpowiedziała, że chodzi o podniesienie mocy Persefony o pół poziomu. Adam powiedział, że to niemożliwe. Klaudia się odwróciła. Adam poprosił, by poczekała. Adam wie, że się nie da. Klaudia powiedziała, że to widziała. Własnymi oczami. Adam sprawdza czy Klaudia mówi prawdę. Klaudia podała parę faktów, które nic nie zdradzają. Powiedziała "magic". 

* V: Adam jest przekonany, że Klaudia mówi prawdę i coś widziała. To może być prawda. To się może udać.

Adam powiedział Klaudii coś ciekawego - był internem w grupie Percivala Diakona. Nie był nigdy dopuszczony do niczego ważnego, ale prawdą jest, że widział opisy pierwszych Persefon na wyższym poziomie psychotronicznym niż te, które są teraz. Adam uważa, że Klaudia odkryła ten sam sposób jaki odkrył Percival. Klaudia nic nie mówi - acz uważa w duchu, że Adam przypadkowo może mieć rację. Bo jej Persefony będą wolne.

I wtedy Klaudia zauważyła, że trzeba zrobić pierwszy prototyp po cichu. Jak zadziała, będzie super. Jak nie... trudno. Adam się zgodził. To wymaga dyskrecji. Mają tu zgodność. Adam spytał, czego Klaudii potrzeba.

* Po pierwsze, miejsce <-- Klaudia
* Po drugie, trzeba zbudować sandbox. Niech Persefona nie wie, że jest w sandboksie. <-- Adam
* Po trzecie, Persefona. <-- tu trzeba połączyć siły i coś zdobyć

Adam się zgodził. Złoży coś na symulator, powie Klaudii jakie części są potrzebne...

Klaudia wróciła do siebie. Połączyła się z biurami, systemami biurokratycznymi. Scrossowała "komu Arianna podpadła" z "kto jest leniwy" i "kto ma przestrzeń do wynajęcia". Szuka osób, które by mogły czerpać radość z tego, że coś się dzieje za plecami Arianny - jej własna załoga coś przed nią ukrywa. Taki lol. 

* V: To jest Ogryz. Ogryz pasuje do tych parametrów.
* X: Ogryz zainteresuje się ponownie Arianną. Ale Ogryz nie jest wielkim problemem.
* V: Ogryz nie będzie się interesował co robi tam Klaudia.
* X: Ogryz będzie rozpowiadał, że Arianna nie kontroluje załogi, ma na to dowód (Klaudię).

Dobrze, udało się zapewnić Klaudii jakiś lokal. Jednak potrzebna jest jeszcze Persefona.

## Streszczenie



## Progresja

* .

### Frakcji

* .

## Zasługi

* Klaudia Stryk: 

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria, Orbita
                1. Kontroler Pierwszy         

## Czas

* Opóźnienie: ?
* Dni: ?


## Inne


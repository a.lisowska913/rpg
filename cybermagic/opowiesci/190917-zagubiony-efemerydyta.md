---
layout: cybermagic-konspekt
title: "Zagubiony efemerydyta"
threads: nemesis-pieknotki
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [190906 - Wypadek w Kramamczu](190906-wypadek-w-kramamczu)

### Chronologiczna

* [190906 - Wypadek w Kramamczu](190906-wypadek-w-kramamczu)

## Budowa sesji

### Stan aktualny

* .

### Pytania

1. .

### Wizja

brak

### Tory

* .

Sceny:

* .

## Punkt zerowy

### Postacie

### Opis sytuacji

* Zbrodnia: użycie niebezpiecznej magii w Pustogorze
* Pięknotka i dwóch konstruminusów na tropie Jana Uszczara
* Janek jest kocony; nie chce do Grzymościa, ale nie widzi dla siebie miejsca. Jest eternianinem. Brzydkie kaczątko.

## Misja właściwa

Janek użył Efemerydy w Pustogorze. Pięknotka dostała dwa konstruminusy, w czym jeden śledczy oraz - by mieć katalistę - Gabriela. Zdaniem Alana spacyfikowała Chevaleresse, więc poradzi sobie też z Jankiem... Pięknotka ma swoją opinię na ten temat...

Efemerydę rozwalili magowie z Fortu Mikado a sama efemeryda miała miejsce w Dzielnicy Uciechy. Gabriel zauważył, że nie jest łatwo jednemu magowi wezwać efemerydę. Pięknotka zauważyła, że nastolatek. Fakt. Pięknotka wysłała Gabriela do Fortu Mikada po informacje a sama poszuka młodego. Pięknotka odpaliła skaner hipernetowy - Janek ukrył się w Miasteczku. A dokładnie, w Górskiej Szalupie. No cóż, spacerek...

Olaf powiedział, że knajpka jest od 18 roku życia. Nie wie, żeby młody tu był. Pięknotka przekonała Olafa; zaczęli szukać młodego razem. Pięknotka ustawiła konstruminusy na wyjściach - a sama z Olafem szuka młodego. Znaleźli, młody zaczął uciekać, konstruminus go złapał. Chłopak skulił się do bicia. Gdy Pięknotka zaczęła go przesłuchiwać, zaczął kłamać.

Pięknotka go przycisnęła. Wsadzi go do kicia i się nim zajmą. Powiedział jej - schował się tu, u Olafa; chciał kraść. Nie ma gdzie iść. Nie ma umiejętności się zatrudnić. Wyraźnie nie wie, że może mieszkać w szkole magów w Zaczęstwie za darmo; ale też wyraźnie nie chce opuszczać Pustogoru. Naciśnięty, powiedział, że boi się że "ktoś" go miał bić.

Gabriel przybiegł i powiedział Pięknotce, że tamta efemeryda była niestabilna ale skonstruowana. Czyli Janek jest efemerydystą. Janek zaczął mówić, że nic złego nie zrobił. Pięknotka przerwała mu dość brutalnie. Pięknotka skupia się na poważnym przesłuchaniu. Udało się - boi się, nie ufa, ale mówi.

Jan uciekł z Eterni. Tu jest lepiej; tam popełnił przestępstw kilka, ogólnie, nie jest tamto miejsce dla niego dobre. Źle traktuje się ludzi, bardzo. Nie chce tam być. Uciekł tu, do Pustogoru, bo będzie bezpieczny. Ale ktoś na niego poluje. I boi się, że poza Pustogorem go złapią. I faktycznie - ktoś go chciał bić. I ogólnie czasem go biją.

Pięknotka ma swoją odpowiedź. Zdecydowała się przesłać go do Barbakanu. Niech Orbiter go przejmie - im się może przydać, Pięknotce zdecydowanie nie. No i miejsce tego młodego maga nie jest w Pustogorze. Nie musi wracać do Eterni, ale efemerydy mogą bardziej przydać się tam, na orbicie - a tutaj mogą bardzo zaszkodzić.

**Sprawdzenie Torów** ():

* .

**Epilog**:

* .

## Streszczenie

Młody efemerydyta uciekł z Eterni. Był przyciskany by dołączyć do mafii więc czasem w panice rzucał niewłaściwe zaklęcia w złych momentach. Pięknotka go znalazła i zajęła się nim - oddała go Orbiterowi, bo tam się może przydać. Mafia musi się obejść smakiem.

## Progresja

* .

### Frakcji

* .

## Zasługi

* Pięknotka Diakon: nie ma głowy do zabawy z nieletnimi efemerydytami z Eterni. Po znalezieniu efemerydyty, oddała go Orbiterowi.
* Jan Uszczar: 17-latek, uzdolniony eternianin, nie chciał dołączyć do mafii i nie chciał żyć w Eterni. Przyzwał efemerydę w Pustogorze pod presją; w końcu wpadł w ręce Pięknotki i został oddany Orbiterowi.
* Gabriel Ursus: katalista wspierający Pięknotkę. Dużo biega i zbiera informacji - z Fortu Mikado lub innych miejsc.
* Olaf Zuchwały: właściciel baru, który nie chce krzywdy Janka ale nie da się okradać. Współpracuje z Pięknotką by złapać 17-latka.
* Marek Puszczok: ma zwerbować Janka do mafii. Nacisnął nań, ale młody nie uciekł z Pustogoru. Ku swemu niezadowoleniu, nie złapał ptaszka. Nie wyszedł na światło dzienne; nikt o nim nie wie.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Pustogor: miał pomniejszy problem z efemerydytą eterniańskim; problem rozwiązany przez oddanie go Orbiterowi
                                1. Fort Mikado: miejsce dużej siły ognia rozwiązujące problem ze słabiutkimi, niestabilnymi efemerydami
                                1. Knajpa Górska Szalupa: miejsce gdzie Janek schował się na zapleczu; Olaf i Pięknotka go znaleźli
                                1. Dolina Uciech: miejsce gdzie Janek został zaatakowany przez Puszczoka i w panice wezwał efemerydę

## Czas

* Opóźnienie: 2
* Dni: 1

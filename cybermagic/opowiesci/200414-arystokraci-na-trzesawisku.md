---
layout: cybermagic-konspekt
title: "Arystokraci na Trzęsawisku"
threads: nemesis-pieknotki
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [200222 - Rozbrojenie bomby w Kalbarku](200222-rozbrojenie-bomby-w-kalbarku)

### Chronologiczna

* [200222 - Rozbrojenie bomby w Kalbarku](200222-rozbrojenie-bomby-w-kalbarku)

## Budowa sesji

### Stan aktualny

.

### Dark Future

* Sabina i Myrczek jako jedyni się ewakuują z Trzęsawiska
* Poważny konflikt Aurum - Pustogor

### Pytania

1. .

### Dominujące uczucia

* .

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Punkt zero

.

## Misja właściwa

Trzy dni. TRZY DNI po tym jak Pięknotka zainstalowała tymczasowo Tymona i Lucjusza by rozwiązali problem w Kalbarku, trzy dni później dostała wiadomość po sieci terminusów. Hestia d'Akademia. Zdziwionej Pięknotce powiedziała, że Tymon ją autoryzował. Powiedziała, że Tymon kazał się triggerować na konkretne słowa kluczowe i sytuacje - to jest jedna z nich. Arystokraci Aurum w Zaczęstwie, dali Ignacemu Myrczkowi pracę. I pojechali z nim w okolicę Trzęsawiska Zjawosztup.

Pięknotka skontaktowała się z Mariuszem Trzewniem. Czy tacy magowie Aurum w ogóle istnieją? Przepiękna arystokratka... Trzewń nie ma jej w rekordach. Za to ten mag? Franciszek Leszczowik, z upadłego rodu. Taki wannabe kultysta Saitaera. Chce wymienić człowieczeństwo na "coś lepszego". No i zbudował potęgę Lemurczaka. Lojalny poplecznik (tu Trzewń się skrzywił).

Pięknotka poprosiła o informacje o tej dziewczynie - co to za jedna. Trzewń puścił modele analityczne (Tr: V). Trzewń wrócił do Pięknotki po pół godziny, zaskoczony. To Sabina Kazitan, ale bardzo wypiękniona. To była ciężka sprawa - wygląda zupełnie inaczej, ale sposób zachowania, ruszania się, gestykulacji - są bez zmian. Pięknotka ma podejrzenia czemu Sabina wygląda inaczej - bo Myrczek ORAZ Pięknotka zrobiliby odpowiednią reakcję na pannę Kazitan.

Hestia, która nie zachowuje się jak Hestia oraz Sabina, która nie powinna być tak głupia i tu wracać.

Pięknotka zrobiła szybkiego calla do Tymona - czy serio Tymon tak skonfigurował tą TAI? Tymon odpowiedział, że tak. Poprosił Talię o pomoc - a w Akademii Magicznej akurat są potężne komputery i dało się Hestię podkręcić. Tymon sam nie był w stanie opanować tego terenu więc poprosił Talię o pomoc. Ma też troszkę oczu na tym terenie, ale nie jest w stanie wszystkiego monitorować samemu. Niech Pięknotka rozmawia ALBO z Talią ALBO z TAI Akademii. Tymon preferuje inne imię niż Hestia d'Akademia - woli mówić na nią "Strażniczka Alair" lub "Strażniczka".

Pięknotce w całej tej opowieści Alana nadal nie spina się jedna rzecz. Dlaczego dyrektor Akademii czuł się nieswojo z przywróceniem do życia TAI Hestia. Hestie są znane, bezpieczne i ogólnie... no, czemu.

Pięknotka jeszcze raz zapytała o to Alana. Alan powiedział, żeby to zostawiła. Pięknotka ma wrażenie, że to TAI to nie jest zwykła Hestia...

TAI Strażniczka powiedziała, że oddalili się w konkretnym kierunku; ma to z dron. Mówili o obozowisku. Chcą wtargnąć na Trzęsawisko Zjawosztup. Pięknotka spytała Strażniczkę o to czy była mowa o grzybach - tak. Chodzi o jakieś grzyby które są super atrakcyjne dla wił. Pięknotka spoważniała. Polowanie na wiły jest ZŁE.

Pięknotka robi przymiarki do znalezienia obozu. Wie, jak wygląda mapa terenu. Wie, jak działa Myrczek i gdzie są grzyby na Trzęsawisku. Jedyne co jej nie pasuje to Sabina Kazitan, ale co tam. Z pomocą dron Pięknotka powinna być w stanie... Strażniczka zaproponowała coś ciekawego - może wysłać drony poza zasięg maksymalny; drony pomogą Pięknotce, ale będzie musiała je potem odnieść.

Tak bardzo Hestia.

(ExZ+3: XV): drona stracona; zlokalizowany obóz, ale drona wpadła w ręce Franciszka. Rozładowana drona dużo mu nie powie - poza tym, że ktoś go śledzi.

Drona przekazała Pięknotce ciekawe wiadomości:

* w obozowisku jest kilkanaście osób. Nie wszystkie są ludźmi lub nie wszystkie mają podstawę humanoidalną; to jakieś bio/technoformy.
* Myrczek nie umie oderwać oczu od Sabiny. Sabina ignoruje Myrczka.
* Franciszek planuje ekspedycję; Pięknotka jest w stanie złapać ów oddział, zanim oni wejdą na Trzęsawisko jak się pospieszy.

Pięknotka zaplanowała coś fajnego. Niech oni tam wejdą. Ona wciągnie jakąś istotę na grupę; niech grupa ma kłopoty a Pięknotka ich uratuje. A część z tej ekipy... może kiedyś byli ludźmi, ale już zdaniem Pięknotki nie są. Już nie są. Lepiej śmierć niż taka egzystencja...

Wraz z Myrczkiem, mają pomoc dwóch lokalsów. Oni nawigują na bagnie. Pięknotka ich zna - to są osoby, które nigdy nie zagłębiają się głęboko, ale umieją poruszać się na bagnie. To są też osoby, które gdy będzie problem to pierwsze będą uciekać (bo wiedzą co to).

Pięknotka wbija na Trzęsawisko pierwsza. Szuka czegoś groźnego, ale nie zbyt groźnego. Pnączodyl może być. Pięknotka po prostu nasyła stwora na grupkę. (TrZ+2: VXV). Pięknotka ściągnęła pnączodyla i w taktycznym momencie stała się niewyczuwalna. Pnączodyl ruszył w kierunku na oddziałek. Dwójka miejscowych dała dyla (wymagają ratowania). Pnączodyl natomiast zaatakował. Oddziałek ustawił się w pozycji defensywnej, Sabina zasłoniła Myrczka ale - co Pięknotkę zaskoczyło - jeden z oddziałku wystrzelił jakąś chorą energią, coś bazowanego czy podobnego do Esuriit. Pnączodyl wymarł, zdegenerował. Rezonans mrocznej energii poszedł. Pięknotka poczuła śmiertelne zagrożenie... jest pewna, że to jest bardzo, bardzo zła wiadomość, bo Trzęsawisko poczuło ten atak.

A dwóch miejscowych leci prosto na obszar gdzie Pięknotka uniknęła pnączoszpona...

Pięknotka ma problem. Chce pomóc zarówno miejscowym jak i arystokratom. Wpierw miejscowi! (TrZ: V). Wpadła na nich zanim WDEPNĘLI w 'szpona. Opanowała ich panikę i zapewniła (V) że sami bezpiecznie wrócą. Nie musi im pomagać. Wie, że oni zrobią coś podobnego w przyszłości... ale nie poradzi. Każdy chce zarobić...

Tymczasem Pięknotka wróciła do oddziałku. Czuje, że zbliża się coś groźniejszego niż pnączoszpon... grupa się rozgląda i Pięknotka podłapała dialog. Sabina pociesza Myrczka, Franciszek chce dalej szukać grzybów.

Pięknotka weszła na teren. Kazała im iść ze sobą. Myrczek się ucieszył - terminuska ich uratuje. Pięknotka opieprzyła Franciszka - obudził i rozjuszył Trzęsawisko. Kazała im iść z nią. Myrczek poszedł od razu. Sabina powiedziała, że ona się boi i uważa, że nie mają kompetencji do znalezienia grzybów - ona idzie z terminuską. Franciszek kazał Sabinie zostać z nim. Sabina spytała Pięknotkę, czy to _executive order_? Tak. Więc Sabina - bardzo praworządna ;-) - poszła z Pięknotką.

Pięknotka nie ma czasu się spierać. Złapała Franciszka za rękę, puściła bombę feromonową i kazała mu iść z nią. Pobiegli, uciekając przed potworami. Podczas uciekawnia sporo ludzi Franciszka zginęło, Sabina jest ranna (krew), Franciszek jest straumatyzowany.

Na twarzy Sabiny jest wyraźny triumf.

Sabina z przyjemnością zeznała wszystko co trzeba - ta dziwna broń która wściekła Trzęsawisko była oparta o Esuriit, ale nie była zakazana; to broń często używana poza terenem Enklaw na niektóre Anomalie organiczne, ale nikt kto wie co i jak nie użyłby czegoś takiego tutaj, przy żywym Trzęsawisku. Powiedziała, że wszyscy martwi ludzie to byli żywi ludzie, podwładni Franciszka. A teraz nie żyją.

Po szpitalu okazało się, że transformacja Sabiny w seksbombę jest odwracalna. Poprosiła o odwrócenie i dostała to - Pięknotka nie chce, by Sabina była seksbombą. Sabina lubi swoje ciało - nie chce być czymś takim.

A Trzęsawisko się nie uspokoiło. Pięknotka z obawą czeka na jutrzejszy dzień... za to wsadziła Sabinę na dwutygodniowe prace społeczne. Sabina bez chwili wahania przyjęła ten obowiązek. Bez protestu, bez komentarza.

## Streszczenie

Franciszek z Aurum dostosował Sabinę do swoich upodobań jako seksbombę. Sabina zdecydowała się go zniszczyć - doprowadziła go do Wysysacza Esuriit i wskazała, że na Trzęsawisku może zdobyć coś co mu się przyda. Franciszek stracił część świty na Trzęsawisku; gdyby nie Pięknotka, zginąłby tam. A tak tylko skończył z traumą a Sabinie się upiekło - roboty społeczne na 2 tygodnie.

## Progresja

* .

### Frakcji

* .

## Zasługi

* Pięknotka Diakon: sprowadziła kontrolowanego potwora Trzęsawiska na głupią ekspedycję arystokratów a potem by ich ratować użyła chemikaliów by straumatyzować Franciszka.
* Mariusz Trzewń: odkrył, że seksbomba-arystokratka to Sabina Kazitan używając systemów analitycznych Pustogoru.
* Sabina Kazitan: zmieniona w seksbombę dla przyjemności Franciszka, zemściła się - wprowadziła Franciszka na Trzęsawisko i z lubością patrzyła na jego dewastację. Zeznała wszystko.
* Franciszek Leszczowik: przerobił sobie Sabinę w piękną seksbombę, potem ciężko straumatyzowany przez potwory na Trzęsawisku i chemikalia Pięknotki.
* Ignacy Myrczek: dał się skusić na zarobek by pomóc znaleźć grzyby na Trzęsawisku; niestety, skusił się na Sabinę Kazitan a skończył ratowany przez Pięknotkę.
* Strażniczka Alair: TAI Akademii Magicznej w Zaczęstwie. Jest tu jakiś sekret Tymona; Strażniczka przedstawia się jako Hestia ale jej psychotronika jest dużo wyżej.
* Talia Aegis: okazuje się, że pomogła Tymonowi sformować Strażniczkę Alair i doprowadzić ją do pełnego funkcjonowania.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Zaczęstwo
                                1. Akademia Magii, kampus: gdzie pojawiła się TAI Strażniczka Alair. A raczej: ujawniła się, bo Tymon wprowadził ją tam wcześniej.
                        1. Trzęsawisko Zjawosztup: uderzone Wysysaczem Esuriit; bronią stworzoną przez Franciszka z Aurum przy pomocy Sabiny. Teraz jest wściekłe.

## Czas

* Opóźnienie: 3
* Dni: 1

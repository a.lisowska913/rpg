---
layout: cybermagic-konspekt
title:  "Skażenie Grazoniusza"
threads: przebudzenie-saitaera, dotyk-saitaera
gm: żółw
players: anna_p, fox, lanux, gosia, flamerog, kapsel
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [190123 - Skażenie Grazoniusza](190123-skazenie-grazoniusza)

### Chronologiczna

* [190123 - Skażenie Grazoniusza](190123-skazenie-grazoniusza)

## Projektowanie sesji

### Pytania sesji

* Co Astoria dowie się o sytuacji na ASD "Grazoniusz"? O Saitaerze?

### Struktura sesji: Łańcuchy Wątków

* Karradrael: Maus zobaczył Postacie, Maus wysłał sygnał (3),Informacja o Bóstwie (6)
* Persefona: Anomalność postaci wykryta, Monitoring w MedBay (2), Hibernacja i porażka (END)
* Kapsuły awaryjne: Ogłoszono alarm (2), Kapsuły wystartowały (4), Kapsuły poza zasięgiem (4)
* Krwawa wojna: Ogłoszono alarm (2), Terminusi kontra potwory (4), Destabilizacja statku (5), Samozniszczenie
* SysKom: Informacja o problemach (2), Informacja o Inwazji (4), Informacja o Korupcji (5), Informacja o Bóstwie (6)
* InfoDrona: Przygotowanie drony, Odpalenie drony (2), Drona poza zasięgiem (4)
* Ognia w Saitaera!: Rozkazy na mostku (2), Spowolnienie bóstwa, Osłabienie bóstwa, Rekonstrukcja Rekonstruktora
* Samozniszczenie!: Rozkazy na mostku (2), Fortyfikacja kluczowej pozycji, Sygnał przez błędny odczyt (3), Samozniszczenie

### Dark Future

1. Grazoniusz zostanie zniszczony (samozniszczenie)
2. Ciało Saitaera zostanie zbombardowane (spowolnienie)
3. Astoria wie o nowym bóstwie, Saitaerze

## Potencjalne pytania historyczne

* brak

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Misja właściwa

**Scena**: (20:00)

Sześć Aspektów Saitaera przebudziło się w asteroidach niedaleko Astorii. Niedaleko pojawił się ASD Grazoniusz - wydobywczo-badawczy statek kosmiczny. Aspekty Saitaera dostały zadanie doprowadzić do tego, by przechwycić ASD Grazoniusz przy minimalnym poinformowaniu Astorii co się dzieje.

Aspekty nie miały wielkich problemów by zwrócić uwagę Grazoniusza na teren niedaleko Krypty Saitaera. Powołały sygnał dużej ilości surowców w tej okolicy. W tym momencie Grazoniusz to wykrył i wysłał kilka sond bezzałogowych by zaczęły zbierać surowce. Aspekty zakłóciły komunikację pomiędzy sondami a Grazoniuszem; zostali wysłany dwaj technicy. Aspekty dostały swoje pierwsze ofiary.

Za mało - opętani technicy dali znać na statek, że pojazd jest uszkodzony. Nie mają jak sensownie wrócić, co gorsza, jeden z nich jest ranny. Dwóch kolejnych techników z apteczką przyleciało do Krypty i mamy czterech opętanych techników. Po "naprawie" mniejszego statku, wszyscy wrócili na Grazoniusza - a ranny technik poszedł do MedBay.

Tam nie było większego problemu by przy kiepsko monitorowanym MedBay potraktować krwią lekarza. Po chwili MedBay wpadł pod kontrolę Saitaera. Pytanie, jak przechwycić resztę załogi? Kilka pomysłów typu "obowiązkowe szczepionki" czy "masowe zniszczenia" zostało odrzuconych - zbyt duża szansa na pojawienie się alarmu. Jeden z opętanych techników rozbił sobie twarz i poszedł na stołówkę, by kapać krwią i prychać na ludzi, ale drogę zastąpił mu lekarz rodu Maus. Udało się odwrócić uwagę Mausa - technik wrócił do MedBay, acz powiedział jako plotkę to, że mag zarządzający MedBay się w nim lekko podkochuje.

Jak rozwiązali problem infekcji? Zaczęli dodawać trochę krwi do jedzenia na wybranych stolikach przy stole, odwracając uwagę ludzi kiepskimi żartami. W ten sposób udało im się dodać dziesięciu zainfekowanych ludzi do kolekcji. 15/70 zainfekowanych członków załogi, w czym tylko jeden mag.

Tymczasem AI Persefona zorientowała się, że coś jest nie tak po wynikach MedBay i powiedziała, że czterech oryginalnych techników musi zostać poddanych kwarantannie. Aspektom Saitaera to nie przeszkadzało - przeskoczyły do innych zainfekowanych ludzi. Plus, MedBay i tak jest pod kontrolą Saitaera w tym momencie.

Najważniejsze byłoby przechwycić jakiegoś maga. Ale jakiego? Padło na pierwszego oficera. Cyryl Szotniorz jest magiem który chce się dorobić i który nie chce skończyć na lekko rozpadającym się Grazoniuszu. W związku z tym zainfekowani poinformowali Cyryla, że mają materiały na szefa MedBay - przegrał artefakt w karty. A ludzie, jak wiadomo, nie mogą mieć magicznych przedmiotów. Taka gratka była zbyt łakomym kąskiem dla Cyryla, zgodził się spotkać dyskretnie w holokabinach z zainfekowanym.

Oczywiście, nie miał żadnych szans. Zainfekowany po prostu wstrzyknął mu krew Skażoną Saitaerem i szybko pierwszy oficer dołączył do grona agentów Saitaera. A nadal nikt nic nie wie.

Największym problemem i nadzieją dla sił Saitaera stała się TAI Persefona. Trzeba w jakiś sposób przechwycić TAI - to będzie wystarczające, by móc sterując systemem podtrzymania życia w odpowiedni sposób zarażać załogę i unikać Mausów. Cyryl - jako pierwszy oficer - ma uprawnienia, by iść do Centralnego Komputera. Tak więc Cyryl poszedł tam, gdzie znajduje się TAI Persefona, a w tym czasie zainfekowani podpalili jeden pokój na statku by odwrócić uwagę Persefony i sił ochrony.

Udało się. Cyryl wszedł do Centralnego Komputera i wsadził do środka cudzą, odciętą dłoń zainfekowaną przez Saitaera. Persefona próbowała walczyć, ale nie była w stanie niczego zrobić. TAI Persefona - jak i cały Grazoniusz - wpadły pod kontrolę zainfekowanych. Pozostała jedynie załoga.

Odpowiednio kierując systemem podtrzymywania życia, zarazili panią kapitan oraz większość załogi tam, gdzie akurat nie było Mausów. Gdy już osiągnęli masę krytyczną, kapitan wezwała Mausów na mostek. Za nimi znajdowali się ludzie którzy zaczęli przekształcać się w terrorformy. Jednym ciosem wszyscy Mausowie zostali wyeliminowani.

"Alicja Sowińska" powiedziała o tym jako o tragedii - Mausowie zginęli w wybuchu, przy awarii Grazoniusza. Tak więc Astoria wie, że coś jest nie tak - ale nie ma pojęcia, że Grazoniusz tak naprawdę stał się statkiem niebezpiecznym.

**Epilog** (21:30)

* Saitaer zaczął rozprzestrzeniać swoich agentów zanim zdradził się ze swoją obecnością
* Pojawia się nowa wiara - kult Saitaera
* Aspekty Saitaera są gdzieś schowane i nadal pracują nad tym, by poszerzyć moc i chwałę Saitaera

### Wpływ na świat

| Kto           | Wolnych | Sumarycznie |
|---------------|---------|-------------|
| Żółw          |   7     |    10       |

Czyli:

* (Ż): .

## Streszczenie

ASD Grazoniusz, statek wydobywczo-górniczy, natknął się na uśpionego Saitaera w asteroidach. Aspekty Saitaera szybko i bardzo dyskretnie pozbyły się Mausów oraz przejęły kontrolę nad statkiem tak, że Astoria nie miała o niczym pojęcia. Co gorsza, Saitaer dyskretnie schował część swoich agentów i rozprzestrzenia się w taki sposób, że nikt nie ma o niczym pojęcia.

## Progresja

* ASD Grazoniusz: stał się statkiem flagowym Saitaera. Jest praktycznie niezniszczalny (regeneruje się) i potrafi przemieszczać się w dziwny sposób.

### Frakcji

* Saitaer: pojawił się statek flagowy - ASD Grazoniusz. Dodatkowo, rozsiał ukrytych agentów, którzy mają zwiększyć jego moc i jego wiarę.

## Zasługi

* ASD Grazoniusz: wydobywczo-badawczy statek, który miał nieszczęście natknąć się na Saitaera. Stał się statkiem flagowym sił Saitaera.
* Alicja Sowińska: idealistyczna kapitan statku ASD Grazoniusz. Nieświadoma obecności Saitaera na swoim statku, została zainfekowana i dołączyła do sił Saitaera.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Obłok Lirański
                1. Anomalia Kolapsu
                    1. Cmentarzysko Statków: gdzie znajdował się ASD Grazoniusz na swojej ostatniej misji badawczo-wydobywczej. Tam spotkał Awatary Saitaera.
                        1. Krypta Saitaera: miejsce, w którym znajdowało się pierwotnie ciało uśpionego boga. Miejsce bliskie portalowi na martwy świat Saitaera.

## Czas

* Chronologia: Przebudzenie Saitaera
* Opóźnienie: 9
* Dni: 2

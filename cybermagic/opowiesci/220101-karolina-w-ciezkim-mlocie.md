---
layout: cybermagic-konspekt
title: "Karolina w Ciężkim Młocie"
threads: rekiny-a-akademia
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [220101 - Karolina w Ciężkim Młocie](220101-karolina-w-ciezkim-mlocie)

### Chronologiczna

* [210323 - Grzybopreza](210323-grzybopreza)

## Plan sesji

### Theme & Vision

* Petty shit can hurt people and a mage doesn't give a fuck
* Karolina i Daniel to zupełnie różne osoby
* Co zrobi Karolina jak się dowie, że Daniel naraził niewinnych ludzi by jej było tyci łatwiej?

## Sesja właściwa
### Scena Zero - impl

.

### Scena Właściwa - impl

* "Karo, no weeeź!" - Daniel tu Karolinę namawia. Karo patrzy podejrzliwie.

Daniel wyjaśnił. Jest ten tam, skandalista. Karol Pustak. I ogólnie on robi rzeczy, że Rekiny nie mają nic wspólnego z lokalnymi. Rekiny są obce, osobno, nos w górze itp. Karo się zdziwiła, że mu to przeszkadza. Ale nie o to chodzi - jest zakład. Że Rekiny gardzą ludźmi. I zakład jest o kasę. Rupert Mysiokornik trzyma kasę. I Daniel przeszedł do konkretu - niech Karo z Mimozą pójdzie do knajpy "Ciężki Młot" i napiją się piwa z miejscowymi. To wszystko. Jeśli się uda to zrobić - super. "Chodzi o Mimozę. Najbardziej wylizaną arystokrateczkę. Jeśli Mimoza pójdzie do takiego baru i napije się zwykłego piwa, to na 100% wygramy zakład." Co Mimoza z tego będzie mieć? Nic, ale Daniel nad nią popracuje. To kwestia PR.

Karo spojrzała podejrzliwie na brata. Czemu bratu zależy? Bo to możliwość zdobycia pewnej reputacji i zasobów. Może działania z lokalnym plebsem, ale nie wiadomo, jak ród przetrwa. Jeśli oni - Terienak - pozycjonują się jako otwarci arystokraci, poszerzają się możliwości. Lepsze to niż tylko mafia, nie? Więc zakład pozwoli pozycjonować się po tej otwartej stronie wśród Rekinów. Więc jeśli Karo i Daniel przekonają Mimozę i pójdzie tam, to pokażą, że Rekiny nie są całkiem zamknięte i izolacjonistyczne.

Plus to okazja na niezły prank wobec Mimozy. Serio - delikatna arystokratka w takim miejscu? Ciekawe co zrobi, jak się zachowa. Co będzie. No i kasa od Ruperta. A skąd będą wiedzieć? Mimoza, najlepiej zalana, wychodzi z baru.

Karo stwierdza, że najpierw idzie tam sama.

Wieczorkiem Karo udała się do Ciężkiego Młota. Na wejściu nie ma bramkarza, Karo po prostu weszła. Podjechała na ścigaczu, zaparkowała, weszła. Ubrana jak to Karo - w żółwika, skórę itp. Karo usiadła przy barze - są tam ludzie obu płci, raczej jest głośno, pali się solidnie jakieś rzeczy, alkohol, te sprawy. Głośno i intensywnie. Ale Karo nie czuje wrogiej atmosfery. Zamawia jakieś piwo i szuka kogoś do pogadania. Barman przyjrzał się Karo z uwagą "Ty się nie pomyliłaś?" Karo wzruszyła ramionami - nie. Młot, podobno fajna knajpa. Barman wzruszył ramionami. "Chyba nie dla takich jak Ty". Karo - "że co?". Barman - "gdzie pracujesz?" Karo - "we własnym warsztacie". "A co robisz?" "Naprawiam. Ścigacze."

Barman spojrzał na jakiegoś kolesia i krzyknął "Henryk, chodź tu." Solidny, silny chłop potrzedł do Karo. Starszy, solidny. Nie jest agresywny. "Ta mała naprawia ścigacze". Henryk przejrzał ją wzrokiem, lekki uśmiech, strzelił pytaniem odnośnie jakiegoś komponentu. Karo odpowiedziała "a który model?". Henryk się lekko ożywił. Spytał Karo, czemu najbardziej popularny model ścigacza w użyciu to Szczupak VII - skoro jest dość stary, dość wolny i niewygodny w użyciu. A odpowiedź Karo jest prosta - pokazuje klucz francuski "bo więcej nie potrzebujesz by to naprawić".

Henryk zadał proste pytanie Karolinie - "Ciebie stać na Neteriusa? To Twój?". Karo przytaknęła. Henryk rzucił "Ty nie musisz pracować." Karo skontrowała "A to coś zmienia? Muszę czy nie muszę, lubię." Henryk "to czemu tu jesteś? Nie ma lepszych knajp dla takich jak Ty? Do zoo przyszłaś?" Karo żachnęła się "ponoć dobra knajpa. Po cholerę mi zoo." Karo widzi głęboką nieufność i niechęć. "A to nie w drugą stronę idzie? Przyszło coś dziwnego do knajpy? A poza tym, cholera, czy każdy kto chce się napić piwa musi mieć rozmowę kwalifikacyjną?". Karo naciska dalej "Jak chcesz, mogę Ci pokazać co zrobiłam z moim ścigaczem."

Henryk gatekeepuje Karo. Karo próbuje się wbić. W sensie - żeby była akceptacja a nie na siłę.

Tr (niepełny) Z (kompetentna, coś umie) +3:

* X: Henryk: "Możesz mi pokazać co zrobiłaś ze ścigaczem. Nikogo tu nie stać na połowę tych zmian w Szczupaku. Fajnie mieć umiejętności, ale nie ma pojazdów tego typu, nie tu." - on nie kwestionuje, że Karo umie.
* X: Henryk: "Nie jesteś pierwsza, co szuka egzotyki. Ale tu ludzie chcą po prostu mieć spokój od Was, w AMZ czy w Rekinach czy gdziekolwiek. To nasze miejsce. Nikt nie zabroni Ci tu być, ale nie pasujesz, rozumiesz?"

Karo się lekko uśmiecha. AMZ i Rekiny nie mają NIC wspólnego. Nic. "Co poniektórzy mogliby się obrazić za łączenie Rekinów i AMZ". On wzruszył ramionami "same shit". Zostawił Karo samą, poszedł się napić.

Barman kiwnął na Karo, by ta podeszła. "Dalej chcesz pić?" I nalał jej coś. Jakiś delikatny szampan XD. Karo bierze to co jej dał, wypija duszkiem. "Daj mi coś co NIE jest wodą. Porządne piwsko.". Koleś się szeroko uśmiecha, Karo widzi, że się z niej lekko nabija. Nalewa jej normalne, porządne piwo. Po czym wraca do rozmowy z innymi bywalcami. Karo stoi i pije sama.

Podchodzi do niej dwóch, wyraźnie zmęczonych facetów. "Co, mała, przyszłaś zakosztować przyjemności prawdziwego życia?". Karo "to zależy". Jeden z nich "100 kredytek starczy?", drugi zarechotał. Karo widzi, że szukają reakcji. Fishing for some fun. "Skoro tyle płacisz swojej ręce..?" - Karo wzruszyła ramionami.

Tr (niepełny) Z (oni są zmęczeni + presja otoczenia) +3:

* V: szeroki rechot. Jeden z nich zaczerwienił się, syknął "spierdalaj" i sobie poszli.

Karo się nie przejęła. Wzruszyła ramionami i pije dalej.

* X: niby pierwszy test przeszła, ale w praktyce oni mają za dużą kosę z Rekinami itp by chcieli cokolwiek jakkolwiek z Karo. Niech sobie idzie.

Następny dzień.

* Daniel: "Karo, weź no...".
* Karo: "Masz na to limit czasowy? Nie? Spierniczaj."
* Daniel: "Gdzie jest problem? Bierz Mimozę, działasz. Gdzie jest kłopot?"
* Karo: "Takiś mądry to sam ją weź. Wiesz co oni zrobią z Mimozą? Wyniosą z baru, postawią przed nim i otrzepią."
* Daniel: "Co? Możemy mieć ich w dupie. Nie sprzedadzą jej drinka?"
* Karo: "Nope."
* Daniel: "Niech spierdalają."
* Karo: "Tylko Twój zakład nie wyjdzie."
* Daniel machnął ręką, zrezygnowany. - "Tu nie traktują ludzi tak jak się powinno. W Aurum robimy to lepiej. Jak człowiek nie chce służyć, to klapsik."
* Karo: "Nie jesteśmy w Aurum"

Daniel, przygnębiony, wrócił do pracy nad Mimozą. Karo natomiast udała się do Ciężkiego Młota. Poprzednio cały wieczór obserwowała, więc tym razem usiadła tak, by dało się do niej podejść ale by nie zająć czyjegoś miejsca. Wzięła jakieś czasopismo o ścigaczach. Zamówiła jakieś piwko. Udaje, że ignoruje otoczenie, że wciągnęło ją czasopismo czy coś. I rozrysowuje układ by coś przetworzyć czy coś. I w jednym miejscu układu robi coś, co niekoniecznie jest błędem, ale można zoptymalizować.

Karolina wabi, by ktoś przyszedł i zagadał.

Tr (pełny) Z (to, że się zna, że była, że umie się zachować i coś robi) +2

* Vz: Henryk podszedł: "Dla Ciebie to hobby. Czemu tu jesteś? Czemu TU?" "Dobre piwo, fajne miejsce". Henryk challenguje Karo - "piwo jak wszędzie, miejsce fajne? Nie jest fajne.". Karo powiedziała, że brat rzucił, że fajne miejsce po nazwie. Henryk powiedział: "wkręca Cię.". Z Karo wychodzi trochę samotność - "AMZ w większości nie chce mieć z nami do czynienia i vice versa. Nie ma wspólnych tematów. Poza wyścigiem. Moi nie znają się na maszynach." (+Vg)
* V: Henryk wyjaśnił Karo, że są kluby techniczne. Miejsce, gdzie można poeksperymentować ze sprzętem. Są fachowcy, czasem. Warsztat. Pooled. Nie musi chodzić po takich knajpach jak ta. Karo: "Wiesz o mnie tyle samo ile myślisz że ja wiem o Tobie". Henryk powiedział, że klub by jej bardziej pasował niż ta knajpa. Tam coś się montuje i komuś pomaga. +Vz.
* V: Henryk powiedział, że jest awaria. Musi pomóc koledze. Karo pyta, czy przyda się para rąk. Henryk powiedział, że Karo się pobrudzi. Karo powiedziała, że żadna pokojówka nie wykręca za nią silnika. Henryk pyta, czy Karo widzi magię. Tak. Dobrze, przyda się. Henryk ma kiepskie detektory. Ale niech Karo nic nie dotyka. Karo wzruszyła ramionami. Henryk pokręcił głową - złamie warunki ubezpieczenia. Ma patrzeć i nic więcej. Karo się zgodziła.

Wysłużony pojazd klasy tarpan, 6 kolesi i Karo na ścigaczu. Jadą w kierunku na zakład recyklingu Owczarek.

Na miejscu - problem. Roboty zajmujące się selekcją są w pełnym chaosie, poruszają się losowo i powodują uszkodzenia. Trudno do nich podejść. Karo wyczuwa energię magiczną - coś tu jest spieprzone. Rozbierały jakąś dostawę i ta dostawa miała "insanity inducer" dla tych robotów. Jednostka wygląda na cywilną, solidna lodówka, ale było tam jakieś zaklęcie czy echo rezydualne. I to przeniosło się na te roboty. Do tego zakład recyklingu jest już pouszkadzany. Z perspektywy magicznej - nie jest to bardzo niebezpieczne. Doszło jednak do kilku uszkodzeń fizycznych, są rzeczy które trzeba zaspawać, szczęśliwie nic bardzo drogiego. Ale jest otwarty płomień.

Kolesie przebierają się w pancerze. Solidne, ale z niesprawną motoryką. Serwowspomaganie na niskiej baterii i ogromnym wysłużeniu. "Dobra, łapiemy te szczury.". Karo mówi gdzie jest źródło Skażenia. Subturingowe roboty sortujące z prostym sprzętem do cięcia, rozrywania - a teraz chaotyczne. Te servary są... nieadekwatne do tej roboty. Są za wolne. Nie mają refleksu.

Oni sprawdzają na swoich skanerach - jest "zielony" poziom magiczny. Karo potwierdza. Poza tą dziwną lodówką, która była źródłem anomalii.

Ludzie rozstawiają się w trójki, w swoich servarach, dwóch ściąga uwagę robota a jeden na wysłużonym Szczupaku próbuje go wyłączyć master switchem z tyłu.

Tr+3:

* X: Przy pierwszym podejściu musieli się wycofać. Cholera. Inny robot też złapał uwagę.
* X: Ten na Szczupaku próbuje złapać i wyłączyć robota od tyłu, ale koleś nie zdąży, bo inny robot mu stanął na drodze i pierwszy robot zrani jednego servarowca.

Karo interweniuje. Neuro-wzywa swój ścigacz, wsiada na niego w biegu i na pełnej prędkości. Zrzucić tego robota z trasy. Skupić uwagę na sobie.

TrZ+3:

* V: Sukces. Robot skupił się na Karo przez MOMENT, dzięki czemu servarowiec mógł się oddalić a szczupakowiec go wyłączył.
* X: W wyniku operacji trochę uszkodzone zostały i lokalny Szczupak i ścigacz Karo.
* V: Udało się wyłączyć te cholerne roboty. Karo jako dywersja, oni jako wyłączający.

Karo dostaje podziękowania (nie niechętne) od lokalsów. Pomogła. Karo skądś wyczuwa sygnaturę tej lodówki, ale... to nie jest anomalia, to magia. To robota maga. Nie coś samoistnego.

* Karo: "Słuchaj, ktoś wam to najpewniej podrzucił. Rozróżniasz zaklęcia od anomalii?"
* Henryk: "Za cholerę. Ale z podrzuceniem nie byłbym pewny. Tu jest złom z różnych miejsc, często też po jakichś sprawach magicznych."

Karo pomyślała "lodówka srs?" ale zostawiła to dla siebie. Ci ludzie wyraźnie nie są szczęśliwi z tego wszystkiego. "cholerna magia". Zabierają się do naprawy, sprzątania itp. Karo pomaga. Nikt jej nie wygania. Ale lodówka dalej jest w jej głowie. Wszyscy zaznaczyli lodówkę i rzeczy dookoła jako "niebezpieczne". Karo to, co się stało z lodówką wygląda na "klątwę" lub "pułapkę". Teraz jej moc gaśnie.

Jak to ogarnęli, Karo powiedziała, że ktoś z tą lodówką coś zrobił. Nie wie kto i co. Władek Owczarek powiedział jej, że za kilka dni spojrzy na to terminus. To niskopriorytetowa sprawa. Karo powiedziała, że powinno być w miarę bezpieczne. Owczarek spytał, czy Karo może się tego pozbyć? Wywiozą to tarpanem. A Karo tym się zajmie. Owczarek wtedy nawet nie zgłasza tego do Pustogoru - nie warto. Small thing. Owczarek sprawdzi w papierach sourcing tej lodówki - skąd to, jakie potencjalne źródło.

Władek Owczarek przejrzał tego uszkodzonego Szczupaka, Henryk powiedział, że da się to jeszcze poskładać. Będzie gorzej, ale jest naprawialny. Da się z tego recyklingu coś zrobić. Karo, wprawnym okiem, zauważyła, że większość tej recyklingowego zakładu ma jakieś recyklingowe elementy. Na pewno nie 100% zgodne z regulacjami.

Ogólnie, wszyscy szczęśliwi, że Karo tu była. Serio. Karo powiedziała, że trochę im pomoże z usprawnianiem, ale jest ryzyko z magią. Na razie nie, jak coś do końca padnie, to będą wdzięczni.

Wieczorem zmarnowana Karo z lodówką na holu wraca do Dzielnicy Rekinów. Tym razem już przyjęli ją w barze. Na miejscu spotyka Daniela.

* Daniel: "Masz lodówkę?"
* Karo: "Side project"
* Daniel: "Do czego?"
* Karo: "Do rozbiórki"
* Daniel: "Co chcesz z tym zrobić?"
* Karo: "Czy na pewno chcesz wiedzieć?"
* Daniel: "...spoko. To co z tą Mimozą?"
* Karo: "Dojdziemy i do tego. Ale jeszcze nie w najbliższym miesiącu. Pali Ci się?"
* Daniel: "Mamy jakieś 3 dni i zakład się rozsypie. Nie zaakceptowali Cię jeszcze?"
* Karo: "Mnie tak."
* Daniel: "To co jeszcze musi się stać by poszło dobrze? Lodówka pomogła?"
* Karo: "Na przyszłość - nie pomagaj tak."
* Daniel: "Chciałaś być zaakceptowana. Zaakceptowali Cię." - Daniel autentycznie nie widzi, czemu Karo jest z tym źle
* Karo: "Nie zrozumiesz. Nie rób tego. I daruj sobie Mimozę."
* Daniel: "To po cholerę to wszystko robiliśmy jeśli nie mamy z tego Mimozy?" - z irytacją
* Karo: "Nie zrozumiesz."
* Daniel: "Siorka, przez szacunek dla Ciebie... zostawię to. Ale tylko dlatego. Bo moim zdaniem zmarnowaliśmy czas."

## Streszczenie

Daniel chciał wygrać zakład z Mysiokornikiem o Mimozę Diakon i alkohol w barze Ciężki Młot. Karo chciała mu pomóc, więc poszła do tego baru rozeznać sprawę. Nie jest akceptowana przez ludzi pracy i próbuje ową akceptację uzyskać. Daniel chce jej pomóc więc robi artefaktyczną zaklątwioną lodówkę; Karo ratuje zakład recyklingowy i jest zaakceptowana w Młocie. Potem opieprza Daniela i Daniel rezygnuje z zakładu z Mysiokornikiem, nieszczęśliwy.

## Progresja

* Karolina Terienak: zaakceptowana jako "swoja" w barze Ciężki Młot. Jest jedynym "nie-człowiekiem pracy" który jest tam traktowany jak swój.

### Frakcji

* .

## Zasługi

* Karolina Terienak: wbiła się do knajpy Ciężki Młot i mimo niechęci do "AMZ / Rekina" została zaakceptowana po tym, jak pomogła w ogarnięciu anomalicznej awarii w zakładzie recyklingowym. Nie boi się ciężkiej i żmudnej roboty.
* Daniel Terienak: chciał wygrać zakład z Mysiokornikiem, więc Karo próbowała wbić się do knajpy. Gdy nie miała akceptacji, sprowokował anomaliczny problem, dzięki czemu Karo pomogła i została zaakceptowana. Po wściekłej Karolinie zrezygnował z zakładu i ma ogólnie ponury nastrój.
* Mimoza Elegancja Diakon: NIEOBECNA. Rekin; prawdziwa dama. O nią był zakład - czy wypije piwo w Ciężkim Młocie. Daniel i Karo próbowali do tego doprowadzić (mimo jej absolutnej niewiedzy).
* Henryk Murkot: starszy wiarus podwiercki, zna się na ścigaczach i pojazdach, złota rączka. Przyjaciel Władka Owczarka. Pomógł opanować awarię w Zakładzie Recyklingu Owczarek z Karoliną. Wyjaśnił Karo, czemu ludzie z Ciężkiego Młota nie lubią AMZ / Rekinów.
* Władysław Owczarek: właściciel Zakładu Recyklingu Owczarek. Samemu próbuje naprawiać co się da. Omija regulacje, zbiera złom. "Janusz biznesu", ale dba o swoich ludzi. Przyjaciel Henryka Murkota.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Podwiert
                                1. Kompleks Korporacyjny
                                    1. Bar Ciężki Młot: dość spokojne miejsce ludzi pracy. Nie lubią Rekinów / AMZ. Zaakceptowali Karo po tym, jak ta udowodniła, że pracy się nie boi i pomoże.
                                    1. Zakład Recyklingu Owczarek: niewielki zakład prywatny z kilkoma robotami segregująco-tnącymi. Doszło do anomalicznej awarii przez Skażoną lodówkę; lokalny ścigacz klasy Szczupak uszkodzony.

## Czas

* Opóźnienie: 2
* Dni: 10

## Konflikty

* 1 - Henryk gatekeepuje Karo z Ciężkiego Młota. Karo próbuje się wbić. W sensie - żeby była akceptacja a nie na siłę.
    * Tr (niepełny) Z (kompetentna, coś umie) +3
    * XX: Henryk akceptuje jej kompetencję, ale uważa ją za hobbystkę. "Same shit". Nie ma tu miejsca, jest turystką do zoo.
* 2 - Karo odpiera "Co, mała, przyszłaś zakosztować przyjemności prawdziwego życia? 100 kredytek starczy?" ostrym językiem
    * Tr (niepełny) Z (oni są zmęczeni + presja otoczenia) +3:
    * VX: Szeroki rechot, odparła. Ale ogólnie ludzie pracujący mają gdzieś Rekiny / AMZ, więc nie wpłynęło to na nic więcej
* 3 - Karolina wabi, by ktoś przyszedł i zagadał małym błędem.
    * Tr (pełny) Z (to, że się zna, że była, że umie się zachować i coś robi) +2
    * VzVV: Henryk wyjaśnił Karo o klubach, że to złe miejsce wrt samotności, powiedział, że pomoże koledze z awarią magiczną. I niech Karo jedzie jak chce bo jest magiem.
* 4 - Ludzie rozstawiają się w trójki, w swoich servarach, dwóch ściąga uwagę robota a jeden na wysłużonym Szczupaku próbuje wyłączyć robota master switchem z tyłu.
    * Tr+3
    * XX: lekko ranny człowiek, uszkodzony servar.
* 5 - Karo interweniuje. Neuro-wzywa swój ścigacz, wsiada na niego w biegu i na pełnej prędkości. Zrzucić tego robota z trasy. Skupić uwagę na sobie.
    * TrZ+3
    * VXV: Sukces, wyłączone roboty i zneutralizowana awaria, uszkodzony Szczupak zakładu i Neterius Karoliny. Karo zaakceptowana.

---
layout: cybermagic-konspekt
title: "Zatruta furia gaulronów"
threads: etaur-zwycieska
gm: żółw
players: fox, kapsel, kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [220323 Zatruta furia gaulronów](220323-zatruta-furia-gaulronow)

### Chronologiczna

* [220316 - Potwór czy choroba na EtAur?](220316-potwor-czy-choroba-na-etaur)

## Plan sesji

### Kto

* Suwan Chankar: (-> Kapsel)
    * Cele
        * Chroni interesy bazy. Niech wszystko działa w tej niestabilnej sytuacji.
        * Ważniejsza jest stabilność niż sprawiedliwość.
        * Niech Aurum nie zdobędzie za dużo. Niech Eternia nie wygryzie Aurum.
    * Rola
        * Szef ochrony. Nie lubi Orbitera.
    * siły: 
        * Szanowany za kompetencje. 
        * Były komandos. Dobry w walce wręcz. Szybki. Zna bazę perfekcyjnie.
    * charakter:
        * Raczej melancholik i stoik. Raczej zrezygnowany
        * Wycofany, raczej nie pokazuje co uważa, umie grać i udawać kogoś kim nie jest.
* Lamia Akacja
    * Cele
        * Zapewnić, by ta baza działała. Sprawić, by Eternia i Aurum mogły współpracować.
        * Obejrzeć wszystkie generacyjne seriale eternijskie. Bawić się z koleżankami
    * Rola
        * podwładna Suwana, agentka ochrony..? XD
        * mediatorka, świetnie gasi pożary i uspokaja
        * wystarczająco dobrze powiązana by mogła kasą zalewać problemy (do pewnego stopnia)
    * siły: 
        * ogólnie lubiana, nikt nie chce jej zrobić krzywdy i nikt nie spodziewa się po niej podłości
        * silny zmysł empatyczny; potrafi wyczuwać emocje
        * ogólnie kompetentna i traktowana jak maskotka bazy
        * biologia viciniusa
    * charakter:
        * entuzjastyczna, otwarta, spontaniczna, beztroska, ufna, empatyczna
* TAI Nephthys
    * Cele
        * Ukryć swoją uroczą osobowość
        * Zapewnić, by Rozalce (i najlepiej nikomu) nic się nie stało i by nic złego jej nie mogło spotkać
            * bawić się z Rozalką i zapewniać radość dzieciom na stacji
            * stabilizować bezpieczeństwo i strukturę bazy
        * Zapewnić balans i bezpieczeństwo bazie
    * Rola
        * TAI bazy
    * siły: 
        * TAI bazy
        * nikt nie wie o jej uroczej osobowości
    * charakter:
        * creative, eccentric, imaginative (ale to raczej ukrywa)
        * raczej wesoła i chętna do zabawy, co musi ukrywać

### Definicje

* Gaulron: 
    * Szary, duży, masywny człowiek, zmodyfikowany biomagicznie. 
    * Bardzo odporny, bardzo silny. Statystycznie mniej inteligentny, acz emocjonalnie normalny.
    * Wada bioformy - tendencje do niekontrolowania emocji, zwłaszcza wybuchy i furia.

### Co się wydarzyło

* Orbiter doprowadził do zmiany dostawcy leków dla gaulronów
* Gaulroni długoterminowo degenerują -> większa furia, mniej kontroli
* Cel: uszkodzić bazę EtAur.

### Co się stanie

* .

### Sukces graczy

* .

## Sesja właściwa
### Scena Zero - impl

* .

### Scena Właściwa - impl

Suwan, alarm ze stołówki. Ktoś nacisnął "czerwony guzik". Poważny problem. Podgląd z kamer -> Graniec dewastuje stołówkę. Lamia + Suwan idą tam się tym zająć. Suwan jest PUŁKOWNIKIEM. Lamia wyczuwa furię i energię Grańca. Lamia czuje ogromne emocje i ostrzega Suwana.

Nepthys sprawdziła zachowanie Grańca. Co spowodowało tą eksplozję. Niewiele ważnych rzeczy - koleś przyszedł, pojadł, popił, siedział sam z tyłu, zaczął tracić kontrolę nad sobą i się rozpalił. Lamia uważa, że może ktoś mu coś dosypał. Może? Suwan ma wątpliwości; kto by chciał by gaurlon zaczął szaleć.

Koleś siedzi i wrzeszczy na rannych "TO JEST MOJA SIŁA! GDZIE JEST TWOJA!" On zawsze chciał być gladiatorem, jak Moktar.

"Panie Graniec, czy tak postępuje gladiator, ta arena, prawdziwe wyzwanie..." -> Graniec zaatakował Lamię. Suwan strzelił gumową kulą zanim mała słitaśna Lamia będzie uderzona.

ExZ+3+3Or:

* Vr: pocisk skrzywdził Grańca. Pod pachę. Koleś nie trafił w Lamię.
* Vz: (->Tr). Graniec nie chce już walczyć. On chce do celi.

Lamia chce z nim pogadać, Suwan odprowadza Grańca do celi i kazał Lamii napisać raport. Albo nie - niech ci co dostali wpierdol robią raport. "Dobra, niech oni to ogarną". Nephthys ogarnia co kto i jak. Suwan + Lamia - odprowadzimy Grańca. Suwan zakłada mu kajdanki. Graniec jest... stonowany. Grzeczny.

Nephthys zapewnia by wszystko działało tak jak ma. Plus wysyła priorytetowy sygnał do Parczaka, że doszło do incydentu. On zwierza się z tego co myślał; Lamia próbuje słowami odwrócić emocje i wyczaić co się dzieje.

* +1Vr: nie ma jednego wydarzenia. To raczej... seria małych wydarzeń. Zbyt silna reakcja na impulsy. Wkurzające rzeczy. Ale on nie ma samokontroli. W pewnym momencie Lamia musiała neutralizować jednego z nich.
* V: Lamia gadała z Grańcem. On zaczął mieć problemy z emocjami jakieś 2-3 tygodnie temu. Tak jakby reakcja na pozytywne bodźce spada, na negatywne rośnie.

Feliks chce pomóc Grańcowi, ale nie wie, czy to bezpieczne. Oddelegowanie 2 cięższych kolesi. Niech będzie porządek. Nie zatruli go czy coś? Czyli mają być badania.

Suwan dokleja do kosztów Grańca 5-10%. Nephthys koryguje nawet nie pytając, "każdy mógł się pomylić".

Nephthys szuka danych w statystyce, Lamia szuka u znajomych i przyjaciół. Czyli - kiedy odchyły, co się mogło stać.

Lamia wyciąga ploteczki od dziewczyn i co powiedział jej jakiś chłopak z plotek -> co 2-3 tygodnie temu zmieniło się w rutynie Grańca. Zainteresowania? Coś doszło?

TrZ+3:

* X: Lamia szuka, chodzi, dużo, sporo - temat zrobił się głośniejszy
* X: Gaurlony dowiedziały się, że KTOŚ NA NICH POLUJE. Że temat robi się... interesujący.
    * 2 tygodnie temu raczej trzymają się razem. Paranoja?
* V: Lamia nie znalazła niczego w poziomie memetycznym. Nic nowego, żadne opowieści, żadne plotki, filmy. To coś w "powietrzu lub wodzie".

2-3 tygodnie temu Rozalka zaczęła być bardziej nerwowa. Ona bierze leki. Terapie?

Nephthys - korelacja między miejscami.

TrZ+3:

* Vz: miesiąc temu była zmiana dostawcy pewnego typu środków i leków. Te środki są częściowymi środkami które bierze Rozalka i jednocześnie środkami które są podawane gaurlonom do funkcjonowania. Specyfikacja środków jest praktycznie identyczna. Nie ma różnicy funkcjonalnej. Ale fakty pokazują, że różnica jest.
    * Sygnał do Parczaka (dowódcy bazy) i lekarza
    * W tej bazie są robione rzeczy by pomóc dzieciom uszkodzonym itp. To jest to co Ketran chce robić, nie "pomoc zmutowanym robolom".
* X: sprawa wkurzyła Parczaka jak mało co.
* V: zmiana dostawcy została zrobiona przez czarodziejkę Aurum. Ona osobiście za tym stała (Dominika Perikas). A wiadomo, że Dominika Perikas jest bardzo przeciwna bioformom, eksperymentom na ludziach itp.

Suwan - sprawdza kompatybilność żywności i środków w otoczeniu z lekami które muszą pobierać. Lepszy pomysł - 6 ochotników wśród gaurlonów. A sam pójdzie do Ketrana.

Suwan, Ketran - bioforma niestabilna, trzeba popracować nad matrycami. Długotrwałe uszkodzenia. Suwan pyta o przyczyny. Ketran mówi, że potencjalnie długotrwała trucizna. Nephthys: te same środki dla Rozalki. Ketran jest w szoku. Dzieciakom pomaga, gaurlonom szkodzi. Skąd ten środek?

Trucizna 2-elementowa. Coś, co pomaga dzieciom (bo inne rzeczy dostają - "szklankę mleka" czy witaminy) a szkodzi zmutowanym gaurlonom. Ketran jest zainteresowany. Pomoże.

Lamia. Udaje się do Pieczary (tak się nazywają "koszary" gaurlonów). Idzie po cywilnemu. Nie chce mieć dużej broni. Nic widocznego. Przekonana o nieśmiertelności, idzie bez broni i po cywilnemu. Po drodze złapał ją jeden z nich, ale powiedziała "idę do przyjaciół". On pokazał szczury i węże - za część pensji je robią i się bawią.

Lamia: Ostatnio dla chorych dzieci i że nie wszystko jest ok z tymi lekami ale mogą niekorzystnie wpływać na osoby. Tylko niestety osoby z modyfikacjami mogą to sprawdzić. Nie możemy sami. Potrzebujemy pomocy. I te plotki nie pasują Lamii, więc okazja dla nich by ich wypromować. Ja też mogę je wziąć... NIE NIE NIE ONA NIE.

Tr+3 (zasób obniża kategorię):

* X: ostra bijatyka (Lamia nie zostanie uderzona)
* X: oni żądają krwi winnego albo naprawy (a wiadomo że im bardzo ciężko pomóc jeśli się da).
* V: Lamia ma swoich 6 ochotników
* X: część z nich zrobi coś głupiego.

Lamia z szóstką wraca triumfalnie do Suwana, który nie spodziewał się tego sukcesu.

Nephthys chce sfabrykować szefa inżynierii (paranoja + szmugiel), że ktoś albo go wrabia albo wchodzi w szmugiel w tą bazę. I on MUSI coś z tym zrobić albo skończy się bardzo źle. Jako okolice problemów Nephthys wskazuje tego nowego dostawcę. Sprowokować go do akcji by odkryć więcej. Nephthys sprawiła że koleś ma dostęp do "luki w AI" (bo AI była wadliwa). Nephthys może się wycofać, ale na razie udostępniła.

ExZ(szmuglerzy i piraci) +3Vg ("luka" w AI) +3:

* Vz: Mumurnik doszedł do tego pirackimi kanałami - jedyne kanały które mogły to zrobić. W tle jest Orbiter.
    * Nephthys zadbała, by Mumurnik przekazał to Suwanowi.

Mumurnik -> Suwan. Za tym najpewniej stoi Orbiter. Wszystko na to wskazuje, a poziom technologiczny problemu wskazuje na to, że faktycznie Orbiter mógłby to zaprojektować.

Suwan prowadzi ekipę do zrobienia eksperymentów: żywność, leki, to wszystko. Ma ochotników, ma dane od Mumurnika, ma wsparcie Ketrana. Jeśli Ketran wie, że za tym stoi Orbiter to się wkurzy.

Tr+3 (Z->redukcja skali):

* V: Suwan ma twardy dowód, że to jest sfabrykowana trucizna przez superhigh tech. Orbiter ma możliwości i zaplecze. Mało kto inny.
* V: Ketran złoży inhibitory. To sprawia, że ich da się -> Eterni by im pomóc albo zostawić tu a Eternia popracuje by im pomóc.
* Suwan idzie do szefa, przedstawia dowody i "oficjalnymi kanałami nie wiemy co się dzieje - ale cichaczem do dowództwa co się tu stało i opanowaliśmy sprawę. Próba sabotażu. Oficjalne przekazy by sabotażyści zrobili błąd, ruch itp." -> by móc uszkodzić Orbiter. Wyrzucamy kartę "to nie coś w bazie a błąd AI" i oficjalne raporty z rekordową sprzedażą. Przesunąć to na sukces. Lepszy handel. Wykupić rynek. Cała inwestycja na marne a my dobrze działamy. +1V -> V. ŚREDNIOTERMINOWO będzie sukces ekon. bazy.

Żeby rozproszyć napięcie i zmniejszyć to wszystko pojawiła się plotka, że złapali agenta Orbitera i on za tym stoi. Ci bardziej berserkerscy chcą się tam wbić i go rozszarpać. A TAK NAPRAWDĘ TO PLAN.

Suwan go przeprowadza,

TrZ+3:

* Xz: Lamia doszła do tego że za tym stał Suwan i on to zaplanował i wie, że przez nią to się stało. Rana serduszka.
* Vz: Suwan dał radę doprowadzić do tego, że element berserkerski przeprowadzi plan.
* V: Żadnych strat. Nikt nie ucierpiał. Sprzęt tak, ale nie bardzo mocno.
* +2V: V: Żadnych strat, perfekcyjna pułapka, wszyscy złapani i spacyfikowani.

## Streszczenie

W EtAur gaulrony zaczęły wpadać w dziwną furię i niszczyć bazę. Zespół ochronny EtAur skupił się na zlokalizowaniu przyczyny - zmieniły się chemikalia mające umożliwiać działania gaulronów i pomagać dzieciom w potrzebie. Zespół przekonał gaulrony do zaufania i głównego lekarza, by jednak pomógł. I dzięki wsparciu Nephthys doszli do tego, że za tym stoi Orbier - nieoficjalnie do tego podeszli by wzmocnić pozycję EtAur i finansowe sukcesy tej bazy. Udało się ustabilizować bazę mimo prób destabilizacji przez Orbiter.

## Progresja

* .

### Frakcji

* .

## Zasługi

* Lamia Akacja: empatyczne serduszko stacji. Wyciągnęła od gaulronów sześciu ochotników idąc w paszczę lwa bez broni i ze 100% naiwnością i dobrodusznością. Wykorzystana przez Suwana który stłumił atak gaulronów.
* Suwan Chankar: szybką reakcją uratował życie Lamii przed gaulronem w Furii; potem opracował jak zneutralizować chore gaulrony i wykorzystał Lamię do tego celu (za co przestała mu ufać). Przekonał Ketrana by się zajął gaulronami.
* TAI Nephthys: "żywa" TAI; wkręciła Mumurnika że musi do tego dojść i koordynowała dane by wszyscy dostali to co potrzebują.
* Graniec Borgon: gaulron dotknięty Furią; niszczy bazę. Pierwszy gaulron któremu zaczęło odbijać; powiedział Lamii, że zaczęło się 2-3 tyg temu. Poprosił że chce do więzienia bo nie chce skrzywdzić nikogo.
* Feliks Ketran: tien eternijski lekarz, mag; chce zajmować się pomocą dzieciom a nie gaulronom. Ale nie pozwoli na zatruwanie gaulronów. Z rozkazu Suwana zajął się gaulronami i złożył z Nephthys inhibitory dla gaulronów.
* Dominika Perikas: jest bardzo przeciwna bioformom, eksperymentom na ludziach itp; Orbiter wrobił ją w to że to niby ONA stoi za zmianą dostawcy. A ona ochrania 
* Erwin Mumurnik: szef inżynierii; paranoik i szmugler; odkrył pirackimi kontaktami że za tym musi stać Orbiter. Przekazał info Suwanowi.
* Maciej Parczak: tien bazy, dowódca. Bardzo hands-off. Wkurzony, że musi tyle robić.


### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Elang, księżyc Astorii
                1. EtAur Zwycięska
                    1. Sektor mieszkalny
                        1. Pomieszczenia mieszkalne
                        1. Podtrzymywanie życia
                        1. Szklarnie
                        1. Przedszkole
                        1. Stołówki
                        1. Centrum Rozrywki
                        1. Barbakan
                            1. Ochrona
                            1. Administracja
                            1. Rdzeń AI
                    1. Sektor przeładunkowy
                        1. Stacja przeładunkowa
                        1. Atraktory małych pakunków
                        1. Stacja grazerów
                        1. Magazyny
                        1. Starport
                        1. Punkt celny
                        1. Pieczara Gaulronów
                    1. Sektor inżynierski
                        1. Serwisownia
                        1. Stacja pozyskiwania wody
                        1. Podtrzymywanie życia
                        1. Panele słoneczne i bateriownia
                    1. Sektor bioinżynierii
                        1. Biolab
                        1. Komory żywieniowe
                        1. Skrzydło medyczne
                    1. Magazyny wewnętrzne
                    1. Ukryte sektory

## Czas

* Opóźnienie: -217
* Dni: 5

## Konflikty

* 1 - Lamia uspokaja Grańca: "Panie Graniec, czy tak postępuje gladiator, ta arena, prawdziwe wyzwanie..." -> Graniec zaatakował Lamię. Suwan strzelił gumową kulą zanim mała słitaśna Lamia będzie uderzona.
    * ExZ+3+3Or
    * VrVz: pocisk skrzywdził Grańca, i on chce do celi. On nie chce skrzywdzić Lamii. Stracił kontrolę.
* 2 - Lamia próbuje słowami odwrócić emocje Grańca i wyczaić co się dzieje.
    * TrZ+3
    * VrV: to nie tak jest że jest jedno wydarzenie. Za dużo małych ruchów. 2-3 tygodnie temu się zaczęło. Jego reakcje na bodźce się zmieniają.
* 3 - Lamia wyciąga ploteczki od dziewczyn i co powiedział jej jakiś chłopak z plotek -> co 2-3 tygodnie temu zmieniło się w rutynie Grańca. Zainteresowania? Coś doszło?
    * TrZ+3
    * XXV: Gaulrony dowiedziały się że ktoś na nich poluje; paranoja. Ale Lamia wie już, że to coś "w powietrzu i wodzie" a nie atak memetyczny.
* 4 - Nephthys - korelacja między miejscami i różnymi danymi.
    * TrZ+3
    * VzXV: była zmiana dostawcy leków dla gaulronów i chorych dzieci. Wkurzyło to Parczaka strasznie. Sama czarodziejka Aurum - Dominika Perikas - za tym stała i to zrobiła. 
* 5 - Lamia: Ostatnio dla chorych dzieci i że nie wszystko jest ok z tymi lekami ale mogą niekorzystnie wpływać na osoby. Tylko niestety osoby z modyfikacjami mogą to sprawdzić - przekonanie gaulronów z Pieczary.
    * Tr+3 (zasób obniża kategorię)
    * XXVX: ostra bijatyka, żądają winnego, Lamia ma ochotników ALE część zrobi coś głupiego.
* 6 - Nephthys chce sfabrykować szefa inżynierii (paranoja + szmugiel), że ktoś albo go wrabia albo wchodzi w szmugiel w tą bazę. I on MUSI coś z tym zrobić albo skończy się bardzo źle.
    * ExZ(szmuglerzy i piraci) +3Vg ("luka" w AI) +3
    * Vz: kanałami pirackimi Mumurnik doszedł do tego że za tym stoi Orbiter i Mumurnik przekazał to Suwanowi.
* 7 - Suwan prowadzi ekipę do zrobienia eksperymentów: żywność, leki, to wszystko. Ma ochotników, ma dane od Mumurnika, ma wsparcie Ketrana.
    * Tr+3 (Z->redukcja skali):
    * VV: mają twarde dowody i dzięki Ketranowi - inhibitory.
* 8 - Żeby rozproszyć napięcie i zmniejszyć to wszystko pojawiła się plotka, że złapali agenta Orbitera i on za tym stoi. Ci bardziej berserkerscy chcą się tam wbić i go rozszarpać. Chodzi by ich zneutralizować.
    * TrZ+3
    * XzVzVV: Lamia ma zranione serduszka, ale Suwan ich wciągnął, zero strat i pełna pacyfikacja.

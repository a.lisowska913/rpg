---
layout: cybermagic-konspekt
title: "Samobójstwo kapitana Wielkiego Węża"
threads: morze-ułud
gm: żółw
players: kić, fox
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [220329 - Młodociani i pirat na Królowej](220329-mlodociani-i-pirat-na-krolowej)

### Chronologiczna

* [220329 - Młodociani i pirat na Królowej](220329-mlodociani-i-pirat-na-krolowej)

## Plan sesji
### Co się wydarzyło
#### Co się wydarzyło w tym terenie?

* Inicjacja bazy Ukojenie Barana
    * mag eternijski, Bruno Baran, założył niewielką bazę daleko od Eterni z częścią ludzi 12 lat temu
    * automatycznie zaczął się kręcić profit
    * wśród górników Elsa Kułak została nieformalną liderką. Niestety, "demokracja" nie przypasowała Baranowi. Odizolował się.
* Migracja Mardiusa
    * noktiańscy komandosi Tamary Mardius złożyli tu swój nowy dom wśród lapicytu. Podeszli do górników i Barana z neutralnością. Ot, są.
    * mało kto jak noktianie umieją żyć w kosmosie. W tym miejscu znaleźli świetne miejsce i ich umiejętności stały się bardzo przydatne.
    * siły Mardius znalazły opcję szmuglowania przez statek Gwiezdny Motyl.
* Imperium Blakvela
    * 3 lata temu pojawił się eternijski szlachcic, Ernest Blakvel. Stwierdził, że władza w kosmosie to coś dla niego. Zaczął umacniać się w tym terenie.
    * Blakvel i Mardius weszli w stały konflikt. Mardius chroni miejscowych, Blakvel chce ich podbić.
* Pojawienie się Strachów
    * 2 lata temu pojawiły się Strachy. Stworzone z programowalnej materii (morfelin), nie wiadomo czemu są i czym są.
    * Mardiusowcy aktywnie chronią lokalnych.

.

* Zespół dał radę kupić lokalizację dziwnego sygnału - znajduje się tam Tajemniczy Derelict Ship. Kapitan podjął decyzję, by Wąż poleciał.
    * Ten okręt zawiera Hipermutator Emisyjny (anomalia), dość sprawną TAI, interesujący ładunek biologiczny oraz neuroobroże Syndykatu Aureliona.
    * Była informacja o niszczeniu morale przez różnego rodzaju zagrożenia i problemy
* Zatrudniono advancera - Berdysza. Nie wiedzą kim Berdysz jest. Gaulron-advancer z wystarczająco dobrym sprzętem.
* Wąż odleciał w kierunku na Derelict Ship.

#### Strony i czego pragną

* Alan 
    * jest agentem Syndykatu Aureliona. Ceni siłę, bezpieczeństwo i hedonizm, ale nieważne jak go patrzą i pomoc innym. Niecierpliwy i praktyczny, motywowany nagrodami i beztroski. Inspektor sanitarny i elektronik. Chce zwiększyć wpływ Syndykatu. Pragnie znaleźć derelict ship i sprzedać znajdujące się tam Obroże Neurokontroli Blakvelowcom. 
    * CEL: sprzedać obroże.

* Pola 
    * najważniejsze są jej osiągnięcia, niezależność biznesowa oraz czysta przyjemność. Niewrażliwa na stres i praktycznie nie motywowana niczym, woli się zgodzić z drugą stroną niż się pieprzyć. Szuka mistycyzmu i rzeczy nadnaturalnych, pragnie zostawić na boku ten bezwartościowy świat. Ewentualnie - komfortowego życia. Badaczka szukająca wśród Strachów iskry bogów. 
    * CEL: interakcja i integracja z absolutem.

* Maja 
    * nienawidzi osób fałszywych. Tradycyjne podejście i akumulacja mocy sprawiają, że Maja osiągnęła niemały poziom znaczenia. Wszystko pragnie zawłaszczyć i rządzić dominacją i terrorem - by wreszcie wszyscy żyli w prawdzie. Jednocześnie znajduje piękno w gospodarce roślin i czystości ekosystemu. Robi za lekarza i kontrolę biologiczną Węża. 
    * CEL: przejęcie władzy

* Kornelia 
    * pragnie się dopasować do grupy i z radością buduje wspólną przyszłość nieważne czy dla ludzi czy AI. Przyjmuje każdego, nieważne z jakiej frakcji. Nie dba o swoje zadowolenie czy emocje. "Dobra mama" grupy. Zajmuje się załadunkiem i sprzedażą dóbr. 
    * CEL: pragnie Wojciecha (jakiegoś Blakvelowca) jako swojego soulmate.

* Filip 
    * jest skupiony na dokonaniach, pokorze i tradycji. Nie dba o stymulację czy pomaganie innym. Złośliwe i wścibskie bydlę, ale niesamowicie pracowity. Kiedyś już zdradził; nie powtórzy się to ponownie - poprzysiągł dbać i chronić interesy. 
    * CEL: zdobyć najlepsze możliwe narzędzia dla Wielkiego Węża.

* Antoni 
    * zna swoje miejsce w rzeczywistości. Skupia się na zabezpieczeniu Wielkiego Węża i na tym by nikomu na pokładzie nic się nie stało. Uratowany z niewoli piratów, pochodzi z zupełnie innego miejsca. Jest jak ryba w wodzie, ale jest świetnym inżynierem i konstruktorem dron, zwłaszcza badawczych. Nienawidzi piratów w każdej formie. 
    * CEL: wrócić do domu. Chwilowo: zabezpieczać Węża.

* Lila 
    * nie dba o tradycję ani bezpieczeństwo - ale skupia się na sukcesach, autonomii i stymulacji. Zostaw ją w spokoju i sumienna (choć nieprzyjemna w obyciu) Lila zapewni by wszystko zadziałało. Appraiser, neurosprzężona wbrew swojej woli i ucieka w używki by nie czuć wiecznego szeptania uszkodzonego AI. Główny pilot i serce maszyny. Wyjątkowo drażnią ją małe głupoty i "szczury w piwnicy" ;-). 
    * CEL: usuwać przeszkody, zapewnić, by coś takiego nigdy nikogo innego nie spotkało.

* Berdysz 
    * jest straszliwym kapitanem piratów. Dołączył do załogi jako advancer. Jest gaulronem, dewastatorem i świetnym planerem. Ceni sobie władzę i przyjemność; nie ma nic przeciw by się podzielić ;-). Jest w stanie pełnić prawie każdą rolę.
    * CEL: skorzystać z okazji, wrócić do bycia kapitanem piratów. Kiedyś.

* 3-4 innych nieznanych NPCów.

### Co się stanie

* Morale graczy będzie dewastowane przez halucynacje, Strachy
* Spotkanie ze Strachami
* Inny Derelict Ship który jest przez coś spustoszony i przeżarty
    * rozpada się
* emisja anomalna na pokładzie Węża 

### Sukces graczy

* .

## Sesja właściwa
### Scena Zero - impl

.

### Sesja właściwa - impl

Kapitan nie żyje. Kapitan strzelił sobie w łeb. Jest to dziwne - nic nie wskazuje na to by miał jakieś kłopoty. Ale wyraźnie widać na kamerach że popełnił samobójstwo jak się zbliżyliście do Derelict Ship. Gdy zbliżyliście się do Morza Ułud. Morze Ułud to obszar gdzie pojawiają się halucynacje - ale tylko wizualne. Badania robione przez różnych lekarzy mówią że to samo promieniowanie które wydziela lapicyt i które sprawia że magowie są biedni to to samo promieniowanie powoduje halucynacje wizualne. Do 10 i otworzyć oczy.

Nadal nie zmienia to faktu że kapitan nie był z tych kto by się zabił.

Wielkie konsylium w mesie. Bez Lili (ona zajmuje się statkiem) i bez Berdysza (on zajmuje się magazynami, sprzętem itp). Jest nowy więc nikt go tu nie chce.

* Pola: "Skąd wiemy, że to nie jakieś bóstwo? Nie Saitaer?"
* Filip: "Nie dotykajmy śmiesznych monolitów jeśli tak..."

Śmierć kapitana jest dziwna. Wyraźnie widział halucynacje. Ale czemu?

Głosowanie na nowego kapitana - Lilia na Filipa, Filip na Lilię, większość na Kornelię, Kornelia i Berdysz na Maję.
Kornelia była blisko poprzedniego kapitana, Jerzego.

Maja -> Berdysz.

* Berdysz: "Moim zdaniem Ty powinnaś dowodzić tą jednostką. Ale jestem tylko zwykłym advancerem."
    * Skutecznie przekonuje Maję do tego by ona była warta.

Maja -> Berdysz: "mogę rządzić za plecami Kornelii".

TrZ+2:

* X: Berdysz pokazuje Mai, że niektóre rzeczy nie udadzą się za plecami Kornelii. Bo Kornelii wejdą na głowę a Maja zrobiłaby to twardo.
* X: Berdysz pokazuje Mai, że warto z nim współpracować i to da Mai osiągnięcie celów jej oraz powodzenia dla Węża.
* V: Berdysz obiecuje Mai, że nie zrobi żadnego ruchu bez konsultacji z nią

Maja -> Pola. Niech uważa na Kornelię. A poprzedni zginął przez halucynacje. Nie wiemy co się stało - więc Kornelia jest zagrożona.

Lila przegląda nagrania. Próbuje dojść do tego co widział kapitan. Dlaczego zginął. Dlaczego się zabił. Lila próbuje wykorzystać swoje bezwartościowe AI by myślała że robi to dla swojego "Stefana" który kiedyś zginął.

ExZ+2:

* V:
    * Lila jest PRZEKONANA że kapitan nie widział halucynacji. Coś było. On coś przeczytał.
    * Lilia jest przekonana, że to co kapitan widział widział tylko on. Byt atakujący mentalnie. Ale byt świadomy.
    * Nikt nic nie rzucił na monitor. To naprawdę jest byt mentalny.
* X: fragment wiadomości wyszeptany przez kapitana "jeśli ktoś się dowie, każdy kto się dowie podzieli ten los".
    * A jeśli to jest za mną i patrzy w monitor...? Lila woli nikomu nie mówić...

Lila ściąga wszystkich w jedno miejsce - do mesy. Wszystkich wszystkich.

Lila: "ktoś sprawił, że Jerzy się zabił by nas obronić. W dużym uproszczeniu - mamy coś psionicznego na pokładzie co go przekonało że jak ktoś coś wie to zginie." I pokazała filmik. Berdysz się zaśmiał szczerze. Lila rozwiązała to z jajami.

* Maja: "teraz jak wiemy możemy to znaleźć"
* Antoni: "to on! To ten advancer! Przybył tu by zabić kapitana!" - Berdysz się roześmiał jeszcze bardziej
* Maja: "zawsze wiedziałam, że jest psionikiem"
* Filip: "obawiam się, że Majka ma rację. Gaulrony są tępe jak stonogi". - oczy Berdysza się zwęziły
* Kornelia: "musimy do tego dojść - jakoś się tu pojawiło, jakoś zadziałało i będzie tu dalej. Kapitana nic nie zabiło - sam się zabił. Jak nic sobie nie zrobicie, nie będzie problemu".

Kornelia uspokaja załogę: TrZ+2:

* X: Morale załogi jest obniżone. Nie wierzą że Kornelia jest w stanie opanować ten problem.
* X: Kornelia została zbombardowana pytaniami. Ale to nie ten charakter - nie umie odpowiedzieć a jej umiejętność snucia teorii jeszcze podgrzewa atmosferę
    * Pola: "a jeśli to działania Saitaera..." a Kornelia: "załóżmy, że to Saitaer. To co o nim wiemy..."
    * na Antoniego to działa mocno
* V: Kornelia opanowała sytuację. Po prostu wszyscy chodzą dwójkami ;-). I mają mówić jak coś widzą.

Grupy Kornelii: ona + Pola, Alan + Berdysz, Maja + Antoni, Lila + Filip

Berdysz zadał pytanie: "od kogo macie info o derelict?" -> wyszło kto sprzedał i jak. Berdysz podzielił się informacjami: "jest taka stara sztuczka piracka. Sprzedajesz lokalizację. Przy okazji przekazujesz na statek broń lub zabójcę. Jak dotrzemy do celu, możemy spodziewać się piratów. I nie mieć załogi. Bo zabójca zadziałał. Oczywiście, może być też tak, że to nie jest ta sytuacja. A jak będziemy wracać, stracimy za dużo."

Antoni: "Ty też mogłeś to zrobić. Ty się na tym znasz!" (do Berdysza)
Berdysz: "To czemu nie oddałem Was piratom tam na miejscu?"

Maja: "ta wersja pasuje - kapitan miał od dawna halucynacje. Nie pozbędziemy się tego nawet jak zawrócimy okręt. Wykryjmy to..."

Maja próbuje przekonać załogę, że trzeba pozbyć się cholerstwa, że mają jak i lecimy dalej + jej autorytet > Kornelia.

TrZ+3:

* V: nikt nie zauważa, że podkopujesz Kornelię i przesuwasz się na miejsce kapitana
* X: pojawia się stronnictwo: Maja - Berdysz w oczach załogi
* Vz: Maja z uwagi na swoją przeszłość i kompetencję osłania skutecznie Berdysza - póki on czegoś nie zrobi przeciw statkowi jest "ok".
* V: operacja "Impostor" pod kontrolą Mai. Maja może pozbyć się niewidzialnego potwora. Bytu mentalnego.

Cała załoga ogólnie na tym pomaga - Antoni z Syndykatu różnymi rzeczami handlował, Berdysz FAKTYCZNIE mógł był to kiedyś zrobić. Pola jest naukowcem i badaczem, Kornelia ma ogromną kreatywność a Lila jest mistrzynią filtrowania.

ExZ+4:

* V: wszystko co było na kamerach, co było ogólnie pokazuje, że mamy do czynienia z bytem 100% niematerialnym. Byt mentalny. Wirus mentalny. Mem.
    * kapitan o nim nie wiedział a został zainfekowany

Kapitan się skarżył Kornelii na różne rzeczy. Na co?

* Widzi rzeczy. Ma silne halucynacje. Zawsze widzi ten sam humanoidalny kształt - o trzech rękach, trzecia ręka wyrasta z pleców.
    * predyktor: obecność humanoida jest zanim on na tyle się zmaterializuje by móc wpłynąć
* Humanoid jest coraz bardziej wyraźny.
* Gdy nie patrzy to nie widzi. Humanoid nie pojawia się "w ziemi". On "zajmuje miejsce".
* Kapitan potwierdził, że to halucynacja - nic nie słyszał.

* (+3Vg) X:
    * Alan jest zaniepokojony. Bo nigdy nie spotkał się z takimi wizualizacjami. Nie z czymś takim. To nie brzmi jak zabójca memetyczny. To coś innego.
    * Pola jest wniebowzięta. To jest to czego ona szuka. Absolut, kult, coś dziwnego. TU NAPRAWDĘ COŚ JEST! (teoria spiskowa?)
    * Kornelia jest zaniepokojona. Bo ten kształt się pojawia w różnych opowieściach w tawernach - właśnie w okolicy Morza Ułudy.
        * czasem w snach, albo kątem oka
    * Berdysz dodaje mocno, że takie coś może być zabójcą memetycznym który przybrał konkretny kształt i formę.
* Vz:
    * Kornelia prosiła kapitana, by prowadził dziennik. I kapitan w prywatnym dzienniku pokładowym (który shackowała Lila) zaznaczył kilka rzeczy
        * po dobrym wyspaniu się memetyczny zabójca działa słabiej. Słabiej go widać, słabiej działa...
        * kapitan widział go już pierwszego dnia od wyruszenia. Jesteśmy 5 dzień w głąb.
        * im silniejszy stres tym mocniej działa zabójca memetyczny
        * -> mimo OGROMNEGO stresu np. u Antoniego - Antoni nic nie widzi. Nie widzi potwora, dziwnych halucynacji itp.
* X: Zabójca memetyczny jest, ale jest uśpiony. Jest nieaktywny bo nie skalibrował się jeszcze z nikim.
* X: Zabójca memetyczny jest TOPORNY. On już zaraził całą załogę. Musimy z nim żyć podczas tej operacji.
    * żeby go zabić, potrzebna jest "zabicie" nosicieli. Czyli np. kriogenika. Niestety, nie ma na to tu narzędzi. Może na derelict coś będzie?

Kornelia zaproponowała, że ona weźmie to na siebie. Pola "nie Ty, ja to zrobię". Filip: "Polka, Ty to się zabijesz w szale religijnym. Mnie to dajcie, zagadam to cholerstwo na śmierć." (jest bardzo odporny) Lila: "i sam się zabijesz - daj mi to, wykorzystam to". Wszyscy się lekko kłócą - i reputacja Mai rośnie. Im gorzej, tym bardziej Maja jest słuchana.

Maja zobaczyła, że Berdysz uniósł brew z szacunkiem do tej załogi. Jeśli nawet miał w planach ich eksterminację, to plany mu minęły.

Lila: "Chcę to, by zejść z tych cholernych narkotyków." Kornelia: "Ale jesteś zbyt istotna!" Lila: "Każdy debil wpisze cel podróży."

To był dobry człowiek, kapitan Jerzy. Uratował Lilę z jakiejś zapijaczonej dziury i okazała się przydatna. Maja - "jeśli może to pomóc Lili, wykorzystajmy przeciwnika na naszą korzyść. Jeśli Lili zależy by zaryzykować, pomoże to wszystkim. Kapitan by chciał jej pomóc." Maja w to wierzy - a przy okazji buduje sobie pozycję.

TrZ+4:

* V: Udało się przekonać wszystkich, że Lili należy pomóc. Że ma prawo do tej próby.
* X: Pojawiły się stronnictwa - Pola, Kornelia i Antoni są silnie po jednej stronie. Im nie pasuje działanie Mai.
* X: Pola, Kornelia i Antoni widzą, co Maja próbuje robić.
* V: Alan widzi co Maja próbuje robić i staje za nią. Filip z innych przyczyn też staje za Mają. Odpowiedni kryzys przeciągnie Antoniego i/lub Kornelię do Mai.

Czas, by Lila przejęła memetic kill agent.

Berdysz bierze Lilę na stronę do ciemnego pomieszczenia. Opowiada jej co i jak. A do tego dostała lek by być bardziej receptywna. Cel - straumatyzować ją by wstrzyknąć w nią agenta.

TrZ+3:

* V: Lila jest wystarczająco straumatyzowana, przejmie na siebie agenta
* Vz: Berdysz na tyle naopowiadał, że Lila widzi, że nie warto stawać przeciwko niemu. Że NAPRAWDĘ jest potworem, ale po ich stronie.

Lila przekierowuje zainteresowanie swoje i AI na kill agenta. Potem Maja podaje jej środki mające ją uśpić. Głęboki sen. I zapętlić AI w ten sposób, by po prostu kill agent został zamknięty w AI.

ExZ+3+3Vg:

* X: Lila wyżarła większość zasobów apteczki.
* V: Kill Agent został usunięty. Został skasowany i nie stanowi już zagrożenia dla załogi. Ani dla Lili.
* X: Echo Kill Agenta jest uwięzione w AI. Pojawiają się Lili sygnały i błędne parametry wynikające z agenta. Czyli Lila ma ryzyko skrzywdzenia kogoś.
* V: Trauma z AI jest zniszczona.

W konkluzji: niebezpiecznie jest grzebać w głowie Lili. Nie znajdzie się lepszego AI tutaj - nie ma na to technologii by pozbyć się kill agenta bezpiecznie. Ale - Lila nie ma co nocy fantomowych koszmarów śmierci Stefana. Preferowana opcja.

Po rozwiązaniu problemu kill agenta i odczekaniu 24h na przebudzenie Lili, Wielki Wąż ruszył dalej, do Morza Ułudy.

## Streszczenie

Wielki Wąż jest niewielkim statkiem scavengerskim w Pasie Teliriańskim. Polecieli do Morza Ułud znaleźć dziwny Derelict Ship i zarobić na tym. Jednak w Morzu Ułud kapitan strzelił sobie w głowę - zabił go memetic kill agent. Zespół poradził sobie z kill agentem - przekierował go w uszkodzone TAI nawigatorki. Jednocześnie Berdysz, pełniący rolę advancera przekonuje Maję że to ona powinna zostać kapitanem i powinna postawić się P.O. kapitana Kornelii. Pod wpływem stresu i braku kapitana pojawiają się stronnictwa na kiedyś spójnym statku.

## Progresja

* Lila Cziras: jej uszkodzone TAI zostało zniszczone przez memetic kill agenta i ów kill agent jest teraz zamknięty w jej implancie.

### Frakcji

* .

## Zasługi

* Maja Kormoran: aspirująca kapitan Wielkiego Węża; ambitna acz nielubiana. Przejmuje dowodzenie w sprawach kryzysowych, zwłaszcza, gdy Berdysz za nią staje. Zaplanowała jak się pozbyć kill agenta i zrealizowała plan poprawnie.
* Lila Cziras: nawigator z uszkodzonym sprzężonym TAI; kiedyś w przeszłości kapitan Jerzy Odmiczak wyciągnął ją z jakiejś zapijaczonej dziury i okazała się przydatna na Wielkim Wężu. Doszła do tego jak zginął kapitan i odkryła kill agenta przetwarzając dane. Wzięła kill agenta na siebie korzystając z neurosprzężenia.
* Alan Falkam: agent Syndykatu Aureliona polujący na neuroobroże na Derelict Ship; handlował różnymi rzeczami. Oportunistycznie staje za Mają i Berdyszem, bo podejście Kornelii jest dla niego za "miękkie".
* Pola Mornak: badaczka szukająca wśród Strachów iskry bogów; uważa kill agenta za coś więcej i wierzy w plotki o dziwnych rzeczach tu, w Morzu Ułudy. Twardo stoi za Kornelią i chce ją pozycjonować na kapitana.
* Kornelia Sanoros: "Dobra mama" od załadunku i sprzedaży dóbr; bliska przyjaciółka martwego kapitana. Uspokaja załogę nawet kosztem swej reputacji. Została P.O. kapitana.
* Filip Gościc: złośliwy i wścibski, ale bardzo pomocny bydlak; wszystkim dogryza, ale nie da się go wyprowadzić z równowagi mimo ogromnego stresu i do wszystkiego podchodzi bardzo spokojnie i racjonalnie.
* Antoni Krutacz: eks-niewolnik piratów jak ryba na rowerze; spec od dron i rozkładania rzeczy na kawałki. STRASZNIE nie ufa Berdyszowi i zarzucił mu, że tak naprawdę to Berdysz zabił kapitana i stoi za potencjalnymi piratami.
* Berdysz Rozpruwacz: pełni rolę advancera na Wielkim Wężu; dyskretnie przekonał Maję, by ta przejęła kontrolę nad Wielkim Wężem zamiast Kornelii i że Mai potrzebne jest jego wsparcie.
* Jerzy Odmiczak: były kapitan Wielkiego Węża; popełnił samobójstwo pod wpływem kill agenta memetycznego. Nigdy nie zabiłby się "tak po prostu", chronił załogę. Ale przed czym? KIA.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Pas Teliriański
                1. Planetoidy Kazimierza
                    1. Morze Ułud: dziwny obszar, w którym pojawiają się halucynacje wizualne. Gdzieś tam znajduje się dziwny Derelict Ship.

## Czas

* Opóźnienie: 17
* Dni: 6

## Konflikty

* 1 - Maja -> Berdysz: "mogę rządzić za plecami Kornelii". Próba Mai zbudowania frakcji i współpracy z Berdyszem (o którego naturze jeszcze nie wiedzą).
    * TrZ+2
    * XXV: Berdysz skorodował serce Mai; musi działać jawnie i dyskretnie by zdobyć władzę. I warto z nim pracować. Ale za to - Berdysz nie będzie robił ruchu wbrew Mai i bez konsultacji.
* 2 - Lila przegląda nagrania. Próbuje dojść do tego co widział kapitan. Dlaczego zginął. Dlaczego się zabił. Wykorzystuje swe bezwartościowe TAI
    * ExZ+2
    * VX: kapitan nie widział halucynacji; on coś przeczytał. Byt atakujący mentalnie. Memetic kill agent. Ale Lila wie, że agent tu jest i zapoluje na nią jak ona powie.
* 3 - Kornelia uspokaja załogę
    * TrZ+2
    * XXV: Morale załogi obniżone, ryzyko że to Saitaer (?!). Antoni ma cios w morale. Kornelia opanowała sytuację - wszyscy muszą chodzić dwójkami.
* 4 - Maja próbuje przekonać załogę, że trzeba pozbyć się Kill Agenta, że mają jak i lecimy dalej + jej autorytet > Kornelia
    * TrZ+3
    * VXVzV: Maja dyskretnie podkopuje Kornelię ale pojawia się idea, że ona i Berdysz to ta sama strona. Ona dowodzi operacją "Impostor" i osłania politycznie Berdysza.
* 5 - Cała załoga ogólnie na tym pomaga - Alan z Syndykatu różnymi rzeczami handlował, Berdysz FAKTYCZNIE mógł był to kiedyś zrobić. Pola jest naukowcem i badaczem, Kornelia ma ogromną kreatywność a Lila jest mistrzynią filtrowania.
    * ExZ+4
    * V: wszystko co było na kamerach, co było ogólnie pokazuje, że mamy do czynienia z bytem 100% niematerialnym. Byt mentalny. Wirus mentalny. Agent Memetyczny. Kapitan o nim nie wiedział a został zainfekowany.
    * (+3Vg) XVzXX: różne reakcje w załodze, mają info jak ochraniać się przed memetycznym zabójcą, jest uśpiony i jest TOPORNY - trzeba zabić / wyłączyć nosicieli. To jest mem, ale dziwny.
* 6 - Lila: "Chcę to, by zejść z tych cholernych narkotyków." Kornelia: "Ale jesteś zbyt istotna!" Lila: "Każdy debil wpisze cel podróży." Lila bierze na siebie kill agenta.
    * TrZ+4
    * VXXV: pojawiają się stronnictwa - Kornelia, Antoni, Pola vs Maja, Lila i inni. Alan i Filip stają za Mają.
* 7 - Berdysz bierze Lilę na stronę do ciemnego pomieszczenia. Opowiada jej co i jak. A do tego dostała lek by być bardziej receptywna. Cel - straumatyzować ją by wstrzyknąć w nią agenta.
    * TrZ+3
    * VVz: Lila jest straumatyzowana więc przejmie agenta; Lila widzi, że on jest potworem ale po ich stronie.
* 8 - Lila przekierowuje zainteresowanie swoje i AI na kill agenta. Potem Maja podaje jej środki mające ją uśpić. Głęboki sen. I zapętlić AI w ten sposób, by po prostu kill agent został zamknięty w AI.
    * ExZ+3+3Vg
    * XVXV: Lila wyżarła większość zasobów apteczki i echo kill agenta jest uwięzione w sprzężeniu (Lila może robić krzywdę przypadkiem). Ale kill agent usunięty i trauma AI usunięta. Lila jest kompletna.

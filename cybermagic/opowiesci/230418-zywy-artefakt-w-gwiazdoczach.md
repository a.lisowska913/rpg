---
layout: cybermagic-konspekt
title: "Żywy artefakt w Gwiazdoczach"
threads: szamani-rodu-samszar
gm: żółw
players: anadia, kić, dzióbek
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [230418 - Żywy artefakt w Gwiazdoczach](230418-zywy-artefakt-w-gwiazdoczach)

### Chronologiczna

* [230404 - Wszystkie duchy Siewczyna](230404-wszystkie-duchy-siewczyna)

## Plan sesji
### Theme & Vision & Dilemma

* PIOSENKA: 
    * Ad Infinitum "Fire and Ice"
        * Above the clouds|Among the stars|We'll draw tomorrow|'Cause we'll be| |Fire and ice|Dancing together|Building forever|Oh oh oh
    * Idea: DWEL EVIL
* CEL: 
    * rozwinięcie Eleny Samszar
    * komediowa sesja detektywistyczna

### Co się stało i co wiemy

* Kontekst
    * Kacper i Aleksandra są parą, Sara chce być z Kacprem. Kacper ma problemy przez swoją noktiańską krew. Aleksandra się przyjaźni z Kacprem ale nic z tym nie robi. Sara chce coś z tym zrobić.
    * Sara jest zdesperowana, ale przy Wielkim Cenotafie nawiązuje kontakt z Neidrią mimo że to skrajnie niebezpieczne. Idzie za planem Neidrii.
    * Neidria proponuje Sarze wykorzystanie sztuki. Obrazów typu cognitohazard. Wzmocnionych przez Kalejdoskop i Szczelinę. A docelowo - wytatuowanie tego na sobie.
    * Sara CHCE pozyskać Kacpra, ale jest "superbohaterką". Adelajda Samszar. Zaczyna jej się mieszać.

### Co się stanie (what will happen)

* Dark Future:
    * Sara przestanie być osobą i stanie się ideą 
    * Kacpra skopią i zniszczą
    * Natan ukradnie Neidrę i inne duchy XD
* S00: Przestępstwo. "Adelaida Samszar" ratuje świat.
    * -> jest tu tien Samszar (uległość, zauroczenie, irytacja, zapomnienie)
* S01: Kradzież książek z biblioteki -> obsesyjna miłość do książek ponad wszystko inne
* S02: Informacja o tym, że część duchów (zwłaszcza noktiańskich i militarnych) zniknęła.

### Sukces graczy (when you win)

* ?

## Sesja - analiza

### Fiszki

* Elena Samszar: czarodziejka origami i kinezy <-- objęta przez Anadię
    * Archiwistka, biurokracja, prawo i dokumenty
    * Jej papier potrafi zrobić krzywdę
    * W wolnym czasie robię rzeczy z papieru, wieże, łabędzie itp.
* Neidria Lazvarin
    * Duch, echo noktiańskiej, niezwykle charyzmatycznej artystki i genialnej mentorki. Niezwykła mentorka, ale też niezwykle niebezpieczna gdyż jest tak pewna siebie.
    * Jako duch, nie przyjmuje już więcej danych ani faktów, jest zapętlonym echem.
    * Zginęła podczas wojny, normalnie jest zasealowana i ma zablokowane informacje o Esuriit, ale pozostałe elementy zostają. Pełni rolę instruktorki i mentorki.
* .Kacper Frapczyk: mieszanej krwi, noktiańsko-aurum student, zaoszczędził i chce lepszej przyszłości
* .Aleksandra Mikrumin: dziewczyna Kacpra, chce go skrzywdzić, bo jego krewni zabili jej krewnych
* .Sara Mazirin: przyjaciółka Kacpra która chce mu pomóc i z nim być. Stała się "Adelaidą Samszar" pod wpływem Neidrii.
* .Adelaida Samszar: kiedyś, poważana przełożona kampusu studenckiego. Trzymała wszystko żelazną pięścią.
* .Oliwier Samszar: współpracuje z Kajratem, chce ewakuować duchy by pomagać noktianom; jego ludzie przypadkiem unsealowali Neidrię

### Scena Zero - impl

.

### Sesja Właściwa - impl

Joachim, bibliotekarz, prosi o litość. Tien Adelaida Samszar rzuciła na niego klątwę. Poezja o niedźwiedziach, praca zbiorowa, Verleni. Joachim nawet nie wie czemu jest ta książka. ŁASKI DLA PAWŁA I CAŁEJ RODZINY! Joachim poszedł z Eleną. Paweł jest zobsesjonowany na punkcie niedźwiedzi i poezji o niedźwiedziach. 

Elena szuka w głowie i w zasobach biblioteki co może wywołać taki efekt.

Tr Z (biblioteka pod ręką) +2:

* X: Elena zadaje głupie pytania i inni nie wiedzą co o tym myśleć
* XX: Duchy zadały dobre pytania, Elena nie wyszła na zbyt mądrą, ale są odpowiedzi
    * to nie wygląda jak Adelaida, ona tak by tego nie zrobiła
    * to nie wygląda na robotę ducha. A przynajmniej, nie "normalnego" ducha
    * Oliwier Samszar na tym terenie wiedział, ale nic z tym nie zrobił
    * NIE Elena nie wie jak pomóc Pawłowi Od Niedźwiedzi
    * To na pewno nie był żaden z duchów na tym terenie. Mógłby coś podpowiedzieć, pomóc - ale nie zrobić

Elena próbuje użyć sentisieci by dojść do tego co tam się działo i jak to wyglądało.

Tr Z M +3 +3Ob:

* Vm:
    * sentisieć nie ma pełnego podglądu na teren, ale masz kilka wytycznych
        * masz ślad osoby która była niedaleko. To gość. Masz jego wizę. To Robinson.
        * sentisieć nie wykryła zaklęcia. Żadnego. To raczej artefakt albo atak mentalny innego typu.
        * to nie był duch
        * opowieść Joachima się sprawdza

Robinson kupił kalejdoskop turystyczny! Wie, że coś jest przerzucane stąd - tam. Więc nikt nie spodziewa się faceta z małą córką (w domu).

* E: Robinson? Przedwczoraj, wieczór, 22:41 w TYM miejscu. Czy działo się coś?
* R: Muszę uprzedzić - mam żonę. I córkę. Widziała tien jaka śliczna i słodka. Widziała tien? Oczy po mamusi.
* E: Mamusia dalej ma oczy? Tzn, ładna mamusia. Ok - czy tam się działo coś podejrzanego?
* R: (mijała go tien Samszar w nieco frywolnym stroju)
    * Nie jestem pewien gdzie _dokładnie_ byłem o tej godzinie, nie kojarzę niczego konkretnego
* E: Będziesz podejrzany o nielegalne zastosowanie artefaktu na obywatelu
* R: Ale... po co? Jaki artefakt? Jestem artefaktorem, ale...
* E: Nielegalne działania niekoniecznie muszą mieć uzasadnienie
* R: Trochę mnie tien zgubiła w tym toku rozumowania gdzieś. Przyjechałem do Gwiazdoczu jako turysta, nie znam nikogo i nie mam powodu używać artefaktu...
* E: (intencyjnie to ->) Jeśli twierdzisz, dobry człowieku, że jesteś na wakacjach i na nikim nic nie robiłeś z artafaktami, chodź, pomożesz mi
* R: Oczywiście, tien Samszar
* E: Czy NA PEWNO nie widziałeś nic podejrzanego w okolicy?
* R: Nie, tien Samszar (nie ośmielę się tego powiedzieć, że tien Samszar robi coś podejrzanego)

Elena wyjaśnia Robinsonowi - człowiek został opętany miłością do tomika poezji. Mówi, że to Adelaida która nie żyje. Ktoś skrzywdził człowieka, należy mu pomóc. I Robinson może w tym pomóc. Więc Elena liczy na kooperację. I podaje ogląd. FAKTYCZNIE, ta "Adelaida" wyglądała trochę jak _tamta_ Adelaida.

Tr Z (przeszkolenie) M (magia) +2 +3Ob:

* V: 
    * tu fizycznie coś było użyte, ale to nie był _typowy_ artefakt - wyczuwasz ślady Esuriit.
    * to wskazuje po sygnaturze na... biologiczną formę artefaktu? Coś Blakenbauerowatego?
    * Robinson może teraz udowodnić swoją niewinność (na pewno nie on)
    * to się Robinsonowi pokrywa trochę z jego śledztwem - to się spina z tym czego on szuka, tam też były plotki o transferze rzeczy Krwi
* X: chyba bez Blakenbauera lub wiedzy Blakenbauerskiej będzie ciężko, to bardzo Blakenbauersko wygląda by pomóc człowiekowi od poezji.

Robinson powiedział wszystko Elenie. Elena natomiast skomunikowała się z Antoniną - ma maga (Robinsona) który twierdzi, że artefakt który Skaził człowieka jest potencjalnie dotknięty sygnaturą Blakenbauerów lub potrzebna jest sygnatura Blakenbauerów by to naprawić. Czy Antonina pomoże to zbadać? Oczywiście - Antonina bierze kilka płaszczek do tropienia. 2 "psy" do tropienia i duży ślimak analityczny. I pojechała - różowo-pomarańczowym jeepem.

Na miejscu Antonina spróbowała sprawdzić swoimi niuchaczami o co chodzi. Antonina jest zainteresowana kto połączył "Esuriit" z "Blakenbauer". Łatwiejszy ślad jest z szukaniem w obszarze Blakenbauerów. Ale jak się uda coś o Esuriit - super.

Tr Z (niuchacze + wiedza) M + 3 +3Ob:

* Ob: serenady mandragory niedźwiedziej. Trujące. Chwasty. Rozprzestrzeniające się. Sentisieć zaczyna z tym walczyć XD. Ale jest niedźwiedziokakofonia.
* V: Antonina ma dowód - tam nie ma NIC z Blakenbauerów. To żywy artefakt. To "tatuaż magiczny"
    * A -> E: Co za KRETYN nosi tatuaż z Esuriit?!
* X: Niuchacze mają pełen i jedyny fokus na swój cel. Smak Esuriit je Dotknął. Blood chłept.
* Vm: Niuchacze mają TARGET. Wiedzą GDZIE jest ta osoba. Wiedzą KIM jest ta osoba. Ona tam JEST i one TAM CHCĄ IŚĆ.

Niuchacze WLEKĄ. Więc Antonina przekazuje Robinsonowi smycze. Idziemy. Bo "prawdziwa dama nie daje się wlec swoim płaszczkom - ma od tego ludzi"

Ex +2 +1 z litości:

* V_z_litości: Robinson UTRZYMAŁ niuchacze, ku szokowi wszystkich poza Antoniną która nie spodziewała się że może być inaczej. Niuchacze konsekwentnie prowadzą do kampusu studenckiego.

Idą za elementem zaskoczenia, prosto na akademik. Tam nie ma ani jednego maga (sentisieć).

Elena szuka śladu Esuriit sentisiecią.

TpZ(mapy + swój teren) M+2+3Ob:

* V: Elena ma konkretny pokój w akademiku. Są tam dwie osoby, jedna z nich ma Skażenie Esuriit. Ta Skażona ma na imię Sara, ta druga Amelia. Jest tam też duch - Neidria. Która powinna być zasealowana na uniwersytecie. A nie jest.
* (-> Tr) Vm:
    * dziewczyny są odcięte. Każda z nich w swojej malutkiej kamiennej "komnacie", nie mają jak uciec. Duch Neidrii jest w tej samej komnacie co Sara.
* Vm: Duch Neidrii został nie tylko odcięty ale też przypięty do sentisieci. Nie tylko jest "przypięty" ale Elena ma też "logi ducha".
    * bez problemu dojdzie do tego co i jak się stało swoimi umiejętnościami.

## Streszczenie

Elena Samszar została poproszona przez bibliotekarza o pomoc - jego kuzyn zakochał się (przez inną Samszarkę) w poezji wierszy Verlenów o niedźwiedziach. Elena poszła pomóc, ale okazało się, że to coś dziwnego. Zebrała wsparcie - Robinson i Antonina i doszli do tego, że to "żywy tatuaż Esuriit" na człowieku. Niuchacze doprowadziły ich do posiadaczki tatuażu i Elena, używając sentisieci, złapała zarówno unsealowanego ducha który podpowiadał jak używać Esuriit jak i nieszczęśniczkę z tatuażem. A w tle - Paradoks z niedźwiedziogorami śpiewającymi kiepską poezję zwalczanymi przez sentisieć.

## Progresja

* .

### Frakcji

* .

## Zasługi

* Elena Samszar: gdy Joachim się do niej zwrócił, że Samszarka przeklęła jego kuzyna, zajęła się sprawą. Zebrała ekipę - turysta Porzecznik, koleżanka Blakenbauer - i doszła do tego, że to nie był czar a 'żywy artefakt', tatuaż Esuriit. Po zlokalizowaniu ofiary, zamknęła ją sentisiecią w komnacie i złapała też ducha doradzającego w sprawie Esuriit zanim sekrety jak duch został unsealowany zanikną.
* Robinson Porzecznik: technomanta i artefaktor, 31 lat; współpracuje z Aurum w sprawie przemytu artefaktów na teren Szczelińca; zajmował się swoimi sprawami i dorwała go tien Elena Samszar z pytaniami. Jako, że wcześniej widział frywolnie ubraną Samszarkę, wolał nie odpowiadać i nie wpadać między dwie młode tienki. Pomógł jak mógł - odkrył żywy artefakt magią. Potem ciągnął dwa wielkie niuchacze na smyczy, bo przecież 'dama nie będzie ciągana przez płaszczki'.
* Antonina Blakenbauer: tienka, bukiety, znaczenie kwiatów, trucizny; chce mieć częsty dostęp do Kalejdoskopu do konstrukcji płaszczki; poproszona przez Elenę o wsparcie (ktoś używa Esuriit i chyba zrzuca winę na Blakenbauerów), poszła, odkryła że to żywy artefakt i wysłała niuchacze by znalazły sprawcę. Oddała smycze Robinsonowi, bo przecież 'dama nie będzie ciągana przez niuchacze od tego ma ludzi'. Paradoksem sformowała niedźwiedziogory (mandragory ale niegroźne) śpiewające serenady, zwalczane przez sentisieć Eleny.
* Neidria Lazvarin: coś ją unsealowało; odpowiedziała na silne emocje Sary i pokazała jej jak zrobić tatuaż Esuriit. "Jeśli Ci naprawdę zależy, oddasz wszystko. Jeśli nie, nie powinnaś nic dostać." Cichy instigator. Ale co ją unsealowało? Elena ją złapała i spróbuje do tego dojść.
* Sara Mazirin: pod podszeptami Neidrii zrobiła sobie tatuaż Esuriit i zaczęła jako Adelaida Samszar ratować świat, malutkimi czynami naraz (Paweł nie oddał książki do biblioteki na czas - zakochany w tomiku niedźwiedziowej poezji Verlenów). Ciężko wyssana z energii życiowej, Elena przechwyciła ją sentisiecią zanim naprawdę coś jej się stało.
* Joachim Pulkmocz: bibliotekarz; poprosił Elenę Samszar o pomoc, bo jego kuzyn Paweł oddał książkę do biblioteki 3 dni po czasie i za to "Adelaida" nałożyła nań klątwę i Paweł zakochał się w tomiku kiepskiej poezji Verlenów o niedźwiedziach. To naprowadziło Elenę do działania.
* Adelaida Samszar: (LONG DEAD), duch Samszarów, kiedyś potężna czarodziejka dowodząca studenckimi akademikami w Gwiazdoczach. Ciepła, ale twarda i pomocna. Tym razem działania Neidrii sprawiły, że jej imię zostało użyte przez Sarę Mazirin; podobno to ona (Adelaida) karze ludzi.

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Aurum
                        1. Powiat Samszar
                            1. Gwiazdoczy
                                1. Centrum (Centrum)
                                    1. Sklepy i kawiarnie
                                    1. Puby i restauracje
                                1. Dzielnica Akademicka (NW, W)
                                    1. Uniwersytet (NW)
                                    1. Kampus uczelniany
                                    1. Wielka Biblioteka (W)
                                    1. Czytelnie naukowe
                                    1. Muzeum Historii
                                1. Szczelina Światów (E)
                                    1. Kalejdoskop Astralny 
                                    1. Archiwa duchów
                                    1. Plac rytuałów
                                    1. Instytut Sztuki Wspomaganej
                                    1. Wielki Cenotaf Skupiający
                                    1. Magitrownie puryfikacyjne
                                1. Dzielnica Technologii (S)
                                    1. Park Technologiczny
                                    1. Warsztaty i inkubatory
                                    1. Centralna stacja pociągów
                                    1. Centrum Eszary
                                1. Dzielnica studencka (N)
                                    1. Strefa Sportowa

## Czas

* Opóźnienie: -289
* Dni: 2

## OTHER
### Fakt Lokalizacji
#### Miasto Gwiazdoczy

Dane:

* Nazwa: Gwiazdoczy
* Lokalizacja: Świat|Primus|Sektor Astoriański|Astoria|Sojusz Letejski|Aurum|Powiat Samszar|Gwiazdoczy

Fakt:

Wizualnie, miasto cechuje się harmonijnym połączeniem starych, kamiennych budynków inspirowanych architekturą Oxfordu i Lund (czyli gotyckich), wspomaganych przez sentisieć i nanitki by stworzyć nową, lepszą całość. Wiele budynków posiada nietypowe, dynamiczne formy i struktury, które zdają się wyłaniać się z dawnych murów. Niesamowicie zielone miejsce, co jest wspierane zarówno przez duchy jak i przez sentisieć.

Kalejdoskop Astralny pozwala zobaczyć co jest, co było lub co być może; Gwiazdoczy znajduje się w przestrzeni Szczeliny Światów.

Gwiazdoczy jest niesamowicie dobrze skomunikowane siecią tramwajów i pociągów. Utopijne sci-fi, ale samo miasto nie jest ogromne - ok 120k ludzi.

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Aurum
                        1. Powiat Samszar
                            1. Gwiazdoczy
                                1. Centrum (Centrum)
                                    1. Sklepy i kawiarnie
                                    1. Puby i restauracje
                                1. Dzielnica Akademicka (NW, W)
                                    1. Uniwersytet (NW)
                                    1. Kampus uczelniany
                                    1. Wielka Biblioteka (W)
                                    1. Czytelnie naukowe
                                    1. Muzeum Historii
                                1. Szczelina Światów (E)
                                    1. Kalejdoskop Astralny 
                                    1. Archiwa duchów
                                    1. Plac rytuałów
                                    1. Instytut Sztuki Wspomaganej
                                    1. Wielki Cenotaf Skupiający
                                    1. Magitrownie puryfikacyjne
                                1. Dzielnica Technologii (S)
                                    1. Park Technologiczny
                                    1. Warsztaty i inkubatory
                                    1. Centralna stacja pociągów
                                    1. Centrum Eszary
                                1. Dzielnica studencka (N)
                                    1. Strefa Sportowa


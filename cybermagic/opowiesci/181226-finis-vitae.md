---
layout: cybermagic-konspekt
title:  "Finis Vitae"
threads: dzien-z-zycia-terminusa
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [181225 - Czyszczenie toksycznych związków](181225-czyszczenie-toksycznych-zwiazkow)

### Chronologiczna

* [181225 - Czyszczenie toksycznych związków](181225-czyszczenie-toksycznych-zwiazkow)

## Projektowanie sesji

### Struktura sesji: Wyścig

#### Korupcja Pięknotki

* **Strona, potrzeba, sukces, porażka, sposób, silnik, breakpointy**
* Dweller in the Depths, dołączający Pięknotkę do swej armii
* Dostarczyć energię Strażnikowi w podziemiach
* Pięknotka jest wyłączona z akcji do momentu utraty osłony Arazille
* Pięknotka ucieka spod kontroli Dwellera
* Spętanie, zniewolenie, wymiana części ciała czy kończyn...
* 1k3 / 3, skok na 3
* Tor
    * 5: Pięknotka jest energetycznie augmentowana
    * 10: Dochodzi do wymiany ręki Pięknotki na wspomaganą; Pięknotka ma własności wampiryczne
    * 17: Dochodzi do wymiany kończyn Pięknotki na wspomagane; Pięknotka ma extreme energy hunger
    * 25: Pięknotka pozostaje "na dole"; nic poza Arazille nie jest w stanie jej uratować

### Hasła i uruchomienie

* Ż: (kontekst otoczenia) -
* Ż: (element niepasujący) -
* Ż: (przeszłość, kontekst) -

### Dark Future

1. ...

## Potencjalne pytania historyczne

* brak

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Misja właściwa

**Scena**: (12:20)

_Colubrinus Psiarnia_

Moktar odwiedził Pięknotkę w jej biovat chamber. Wypuścił ją z biovatu. Rzucił jej jakiś strój laboratoryjny i wyjaśnił, że większość uwarunkowań została już odwrócona - te, które jego ekipa jest w stanie odwrócić. Moktar powiedział Pięknotce, że uważa ją za broń - broń organiczną, ale broń. Nie za osobę. Zaproponował jej dołączenie do jego sił - Pięknotka się marnuje jako terminuska z takim podejściem. Pokonała Walerię, przetrwała Bogdana, wytrzymała z nim ponad 30 sekund.

Moktar powiedział jej, że zna każdą jej myśl i wie, że Pięknotka się marnuje w Pustogorze i Cieniaszczycie. Pięknotka odmówiła - Moktar to ogromna potęga, ale prędzej czy później by ją zabił. Ona szuka wolności, on chce kontroli. On robi rzeczy, które nie zgadzają się z Pięknotką - póki ona nie jest w to umoczona, nie ma problemu. Ale jakby była, musiałaby zadziałać.

Moktar powiedział jej, że ma dla niej zadanie. Jego żołnierz, dzielny żołnierz, został złapany i Skażony przez potwora w głębi. Moktar nie wie co jest tam, w głębi - jest tam jakiś potwór, klasa demon czy bóstwo. To są te siły, które próbują złapać żywcem ludzi i magów, by zrobić z nich amalgamat techorganiczny czy nawet transorganiczny. On nie jest w stanie wejść tam w głębię - uciekają przed nim. Ale Pięknotka jest w stanie.

Innymi słowy, Moktar chce, by Pięknotka została Skażona i Dostosowana przez potwora w głębi, i by przyprowadziła jego żołnierza do ołtarza Arazille. On będzie w stanie odwrócić ten efekt i uratować swojego agenta. W zamian za to, Pięknotka poprosiła - chce nauczyć się channelować bogów. Moktar się zaśmiał. Póki Pięknotka nie będzie miała ciała zbudowanego przez Saitaera i póki Arazille nie będzie śpiewać jej do snu, nie będzie to możliwe.

Moktar powiedział, że da jej życie za życie. Lilia Ursus. Gdy zniszczył jej własne oczekiwania na jej temat, zostawił jej wiadomość jak może uzyskać większą potęgę. Najpewniej teraz Lilia próbuje wezwać siłę, by ta dostarczyła Lilii większą moc i większe możliwości. Sprzeda duszę, ale stanie się cudowną bronią. No i Moktar proponuje Pięknotce, że ta może uratować Lilię przed tym losem.

Pięknotka powiedziała, że nie jest w stanie jednocześnie ratować jego żołnierza i Lilii. Poprosiła, by jeśli coś jej się stanie, by on uratował Lilię. Moktar wysłał na akcję Walerię i dwa Psy - ich zadaniem jest przyprowadzenie Lilii Ursus do Colubrinus Psiarni. No i Pięknotka zażądała, by Moktar pomógł jej uwięzić Saitaera w tej rzeczywistości, najlepiej na Astorii. Moktar powiedział, że to rozważy.

_Colubrinus Psiarnia_

Pięknotka wpadła na Walerię. Ta jej szybko powiedziała, że Pięknotka ma nowy status - może wpaść do Czaszki Kralotha czy do Colubrinus Psiarni gdy ma ochotę. Żądanie Moktara. Gdy Pięknotka spytała Walerię, czy między nimi wszystko w porządku, ta potwierdziła - czemu miałoby tak nie być? Jak chodzi o czytanie myśli, to tylko kwestia efektywności - Pięknotka była efektywna i Waleria niczego mniej się nie spodziewała.

Pięknotka spytała Walerię, czy może pomóc jakiś slave suit, ale Waleria powiedziała, że nie pomoże - próbowali. Jedyne, czego nie próbowali to użycie krwi Arazille. Pięknotka poprosiła Walerię, czy jak już przytarga Lilię, to czy dałoby się użyć jej umiejętności - Pięknotka nie chce tam się wbijać bez planu i strategii.

Dobrze, jutro pokonferują. Muszą to jakoś rozwiązać...

(pauza 30 min)

**Scena**: (13:40)

_Mrowisko_

Atena nie zgodziła się na ten plan. Pięknotka chce liczyć na to, że osłoni ją potęga Arazille? W chwili, w której nikomu jeszcze nie udało się tego zrobić? Niech chociaż porozmawia z Amadeuszem. Może coś będzie wiedział. I niech Pięknotka użyje zapasowego power suita Erwina - to zwiadowczy pancerz, nie pancerz bojowy. Erwin jeszcze kilka dni nie może niczego zrobić, jest rekonstruowany; nie wyciągnie Pięknotki.

Atena nie widzi powodu, dla którego Pięknotka ma wykonywać tą operację. Pięknotka powiedziała, że może to rozwiązać Saitaera raz na zawsze i pomóc Lilii. Atena powiedziała, że to coś co powinny zrobić ASD. Ona (Atena) wiedziała, na co się pisze gdy dołączała do ASD. Atena powiedziała Pięknotce, że ta powinna porozmawiać z Amadeuszem - a ona sama spróbuje ściągnąć jakieś pylony w odpowiednim momencie i wwiercić je w ziemię. W końcu ASD. (Zasoby:6,3,6=S). Atenie uda się wszystko pozałatwiać tak, by się mogło przydać.

_Kompleks Nukleon_

Prośba o spotkanie z Amadeuszem przez Pięknotkę. Granted. Pięknotka powiedziała Amadeuszowi, że chce zejść w dół i uratować jedną osobę. Co Amadeusz może jej powiedzieć o tym, co jest pod spodem, pod Cieniaszczytem? Amadeusz powiedział, że nie może jej tego powiedzieć i nie chce, by Pięknotka zasiliła jego siły. Pięknotka powiedziała, że ma energię Arazille. Amadeusz wyjaśnił w końcu wszystko, bo Pięknotka jest terminuską, jeśli przysięgnie że to zostaje między nimi. Pięknotka powiedziała, że jak długo zależy to od niej, tak będzie (Tr+2:7,3,6=SS). Amadeusz dokładnie wyjaśnia znaczenie dla Saitaera.

Cieniaszczyt jest miejscem niestabilnym magicznie. To wynika z znajdującego się niedaleko potężnego źródła magii - bardzo uszkodzony autowar Finis Vitae. Ten autowar nie należy do Astorii; podczas Zaćmienia został wrzucony w Astorię i ciężko uszkodzony; jedną z pierwszych rzeczy jakie trzeba było zrobić to było zniszczyć Finis Vitae. Teraz jest pod Cieniaszczytem, głęboko, gdzieś na Terenach Skażonych. Finis Vitae nie jest w pełni sprawny i nie jest w pełni obudzony - psychotronika jest poważnie uszkodzona. Cieniaszczyt akceptuje taki stan rzeczy, bo nie umieją znaleźć i zniszczyć Finis Vitae.

Ale Saitaer mógłby spróbować opętać i naprawić autowara swoimi boskimi mocami. Dlatego Moktar nie może się nigdy o tym dowiedzieć - stąd Amadeusz zostawia ślady wskazujące na to, że tam są nojrepy należące do Orbitera Pierwszego. Bo prawda o autowarze nie może wyjść na jaw. Oczywiście, nie znaczy to, że Orbiter NIE MA tu swoich działań. Ale nie wszystkie działania są Orbitera.

Pięknotka powiedziała, że da się to rozwiązać. Niech Amadeusz przechwyci tego ratowanego żołnierza zanim Moktar go przejmie. Niech Amadeusz wyczyści informacje o Finis Vitae z jego głowy - i już teraz z głowy Pięknotki. Jeśli interakcja Finis Vitae i Saitaera jest tak niebezpieczna, ona nie chce o tym wiedzieć. Amadeusz się zgodził. Powiedział, że nie zamierzał zostawić tych informacji w głowie Pięknotki tak czy inaczej - ona zbyt często jest wiązana i kontrolowana.

Pięknotka straciła pamięć tej rozmowy.

_Colubrinus Psiarnia_

Następnego dnia Pięknotka spotyka się z Walerią w Colubrinus Psiarni. Waleria jest chłodno uśmiechnięta; wygląda na to, że misja jest wykonana. Owszem, Lilia jest zdobyta i siedzi gdzieś w Psiarni.

Waleria powiedziała, że nie jest w stanie zrobić sensownego planu - nie rozumie działania przeciwników. Ale może próbować pomóc Pięknotce się wydostać, jeśli ta będzie już uciekać stamtąd. Spróbuje jakoś przygotować teren i możliwości. Problem polega na tym, że przeciwnik jest niemożliwy do złapania czy containowania póki jest w pewnym obszarze - ten obszar sprawia, że np. jest nieteleportowalny. Stąd Pięknotka musi wejść do środka i wyjść ciągnąc za sobą tego nieszczęsnego żołnierza.

Ważna wiadomość dla Pięknotki - Skażeńcy nie mają dostępu do energii magicznej. Nie czarują. Magia wymaga woli i słowa. Oni nie mają woli. Pięknotka ma.

**Scena**: (14:50)

_Studnia Bez Dna_

Pięknotka otworzyła oczy. Znajduje się w jakimś biovacie. Jej ciało jest jakieś... inne. Czuje w swojej głowie czyjąś Obecność, ale nie jest jeszcze na tyle silna, by mogła ją normalnie zdominować, mimo obecności Arazille. She is not corrupted yet. Ta Obecność jeszcze nic nie chce, acz wprowadziła kilka blokad - niemożność zrobienia nikomu ani sobie krzywdy. Ciecz w biovacie zaczyna opadać, Pięknotka może wyjść.

Pięknotka jest w "human pen". Widzi kilkanaście osób, jest jedzenie i woda. Nikt nie ma ubrań, jest kilka uzbrojonych nojrepów. Jedna młoda dziewczyna strasznie płacze. Mnóstwo ludzi płacze i jest podłamanych. Jeden tam leży z takim poddaniem. Już się poddał. Na oko, jest tam z miesiąc. Dla Pięknotki oznacza to, że tamten nie był w ogóle skazany na bycie Corrupted. Oki, to tyci utrudnia. Młoda dziewczyna poprosiła nojrepa by ją zabił, ale nie jest w stanie nic zrobić.

Do Pięknotki podszdł jeden facet. Powiedział jej, że on jest przeznaczony do _breeding pen_, tak jak tamta dziewczyna która tak płacze. Są modyfikowani w biovatach, ciąża trwa krócej. Ma nadzieję, że Pięknotka też tam trafi - podczas badania niech myśli rzeczy przyjemne i sympatyczne. A nic bojowego ani agresywnego. Ogólnie, Pięknotka uznała, że OSTATNIM na co ma ochotę jest bycie jednostką reprodukcyjną dla autowara...

Pięknotka poszła pocieszyć tamtą dziewczynę. Może coś będzie w stanie zrobić. To od czego zaczęła - zebrać informacje skąd kto się tu znalazł. Zrobić sobie mapę działań obszarów autowara. Skąd pobiera ludzi. (Tp:7,3,4=S). Duży sukces Pięknotki - gdy wróci, Amadeusz będzie w stanie zatrzymać działania nojrepów lub przynajmniej je ograniczyć. Okazuje się, że to są ludzie, którzy przeszli na teren Skażony z ciekawości lub z jakiegoś powodu. Przynajmniej Zasłony działają...

Tor Pięknotki: 3

Czas na Wybór. Weszły nojrepy i jeden mag. Ten, którego szuka Pięknotka. Desygnata: Overlord. Pięknotka próbuje wygenerować jakieś toksyny w powietrzu by móc pokazać że walczy, by nie wzięli jej do _breeding pen_ a na pole bitwy. Pięknotka chce skłonić Overlorda, by ten uznał ją za "valuable drone" a nie za "a drone". (TrZ+2:10,3,5=S). Overlord zatrzymał się przy Pięknotce. Przesłał ją nie do breeding pen a do battle pen.

Na Battle Pen czeka silny mężczyzna i Pięknotka. Instrukcja - zabij. Plus pokazanie co się stanie jeśli tego NIE zrobią. Pięknotka łapie niewprawnie pałkę, silny facet fachowo. Pięknotka chce zdobyć zainteresowanie Overlorda ponownie - chce użyć swoich zdolności do tego by go erotycznie załatwić. Nie zabić tylko pójść krok dalej, kompletnie go doprowadzić do tego, że się totalnie zapomni. Do poziomu psa w rui. (TpZ:12,3,2=SS).

Pięknotka zrobiła wrażenie na Overlordzie - ale przy okazji sama się zapomniała i skończyła ze swoją ofiarą na ziemi w bardzo gorącym momencie. Do momentu aż Pięknotka dostała rozkaz 'execute' i zabiła swoją ofiarę. Wewnętrznie jest przerażona, ale wykonuje działania. Jest w końcu terminuską.

Overlord wziął Pięknotkę i wstrzyknął w nią dziwną energię, wbijając rękę prosto w brzuch Pięknotki. Ból i chora energia, kontrowana jedynie mocą Arazille.

**Scena**: (?)

_Studnia Bez Dna_

Jest w biovacie, w tym samym penie. Czuje... coś dziwnego w lewej ręce. Jej ręka jest technoorganiczna - jest to cudza, dziwna ręka. Świetna broń. Ale nie jest to jej ręka. A jej ciało jest pokryte lekkimi łuseczkami. I jest, nie powiem, dość głodna.

Tym razem Pięknotka skończyła płacząc żałośnie w penie, tym co wcześniej. Parę osób próbowało ją pocieszyć, ale bez powodzenia. Ale się zregenerowała - wie, że ona ma jak uciec w ostateczności... chyba.

Pięknotka zdecydowała się przejść po kompleksie podziemnym. Co tu się dzieje. Nie ma dostępu do wszystkich miejsc, ale do niektórych tak. Pięknotka zobaczyła laboratorium - miejsce produkcji broni, miejsce transformacji nojrepów, miejsce budowania broni biologicznej. No i Overlorda. Jest podpięty do głównej sieci dowodzenia. On jest głównodowodzącym tego co tu się dzieje, w służbie autowara.

Pięknotka chce znaleźć wyjście. Chce wiedzieć gdzie uciec by się wydostać. Znalazła wyjście. Poszła więc do transformowanego w dronę człowieka - jest tam, w podobnym stanie jak Pięknotka. Czarodziejka się nie przejmowała, on i tak jest martwy. ZJE GO. Wybrała go jako swoją ofiarę seksualnego drapieżnictwa i modliszkowania. Czy zdąży zanim coś ją powstrzyma? (TpZ+2:12,3,2=S). Pięknotka poczuła po raz pierwszy w życiu jak seksem zabić kogoś. Autowar nawet nie włączył blokady - analizował dane. Pięknotka poczuła energię innego człowieka w swoich żyłach. Jest silniejsza i już nie jest głodna.

Pięknotka wie, że jeśli podejdzie do jeszcze jednej ludzkiej ofiary to autowar się zgodzi dla danych. A Pięknotka miałaby więcej energii na ucieczkę. Niestety, nie może podejść do kobiet - są potrzebne do rozrodu. Ludzie WIDZĄ co się stało. Są przerażeni. Ale Pięknotka szuka kogoś kto ma deathwish i znalazła go. Pożarła i się wzmocniła. Ale czy opanowała się, czy stała się tymczasowo opętanym przez energię autowara potworem? (Tp:5,2,5=S). Opanowała się i nie dokonała rzeźni. Acz Pięknotka wie teraz dokładnie jak czuje się kraloth. I wie, że to podobnego typu uczucie. Jej poziom przerażenia poszedł level up.

Wpływ:

* Żółw: 11
* Kić: 2

**Scena**: (?)

_Studnia Bez Dna_

Pięknotka spędziła krótką chwilę by się opanować. Potem poszła do sieci informacyjnej... jest tam blokada przed wpięciem się. Oczywiście, że jest. Ale może podejść. Pięknotka obudziła w sobie Labirynt Luster. Czas porozmawiać z Arazille...

_Labirynt Luster_

Arazille zauważyła, że nie jest bronią Pięknotki. Pięknotka nawet jej nie lubi i nie kocha. Pięknotka uargumentowała, że tu są zniewoleni ludzie - i ona może być przekaźnikiem energii Arazille. Arazille rzuciła Pięknotce obraz jej w breeding pen, a potem jej jako drony przynoszącej przyjaciółki Pięknotki do breeding pen. Arazille zauważyła, że Pięknotka już wcześniej się u niej zadłużyła - i nie chce teraz oddać jej tego długu który zaciągnęła?

Seria pojedynków mentalnych między Arazille i Pięknotką skończyła się na tym, że Pięknotka zaproponowała Arazille założenie świątyni Arazille nad Finis Vitae, by Arazille osłabiła lub dezaktywowała autowara. Arazille się zaśmiała - nigdy politycznie nikt się na to nie zgodzi. Pięknotka powiedziała, że jest skłonna spróbować. Arazille się zgodziła na te warunki. Jeżeli tak Pięknotka stawia sprawę, Arazille jest skłonna jej pomóc.

_Studnia Bez Dna_

Z powrotem w świecie rzeczywistym. Pięknotka czuje energię Arazille w swoich żyłach. Wbija rękę w odbiornik, łamie zagrożenie i wydaje rozkaz systemowi "all doors - open". Pięknotka chce zrobić chaos i ograniczyć siły ścigające Pięknotkę do Overlorda. Niech musi sprzątać. I wpuszcza Arazille w system. (TpZ:10,3,3=S). Pięknotka spowodowała ogromny chaos a Arazille zaczęła uwalniać poszczególnych ludzi, drony itp. Pięknotka też wysłała sygnał "do góry", że rozpoczyna ucieczkę z nadzieją, że Atena to odbierze. Albo choćby Waleria...

Wpływ:

* Żółw: 13
* Kić: 2

**Scena**: (16:53)

Overlord jest wolniejszy przez całą tą sprawę z Arazille. Pięknotka ucieka tak szybko jak tylko jej ciało na to pozwala. (3 Tr). Korzystając ze spowolnienia Overlorda, biegnie w miejsce gdzie się umówiła z Ateną na pełnej prędkości wspomaganego przez czarną energię ciała. Uciekając, zostawia za sobą dym i śluzy, by się musiał poślizgnąć. (TpZ+1:10,3,3=SS). Udało się, acz Overlord się zbliżył do Pięknotki - jest w polu widzenia. Czarodziejka nie jest w stanie nadążyć z ucieczką przed przeciwnikiem w power suicie.

Pięknotka się skupiła i strzeliła najpotężniejszą błyskawicą od dawna. Cel - strzelić w power suit wroga ORAZ walnąć w sufit. Spalić mu silniki i maksymalnie uszkodzić - ona musi, MUSI zdążyć do punktu zbornego. Musi wydostać się z tej cholernej studni. (TpM:10,3,3=MSS). Skonfliktowany magiczny. Walnęła. Piękne zaklęcie, świetnie poszło. Ale ją też poraziło - w końcu ma ten sam energii co przeciwnik. Pięknotka straciła przytomność...

...obudziła się, a Overlord wciąż się wykopuje. Jest cała obolała a po głowie latają jej koszmary tego co robiła i tego co widziała. Ale on się wykopuje a ona czuje, jak jej ciało staje się coraz mniej jej. Ucieka. Tak szybko jak potrafi. I natrafiła na bardzo stromy, bardzo wysoki tunel w górę. Niechętnie, znów magia elementalna, zanim Overlord ją zestrzeli (po prostu). (TpM:10,3,3=P,MSS).

Pięknotka została trafiona odłamkiem. Krew bryznęła ze zdrowej, prawej ręki. Wylądowała rozpaczliwie na szczycie tunelu i leciała na pełnej mocy. Coraz bardziej traci obraz - serce, oczy, wszystko zaczyna poddawać się czarnej energii. Aż wyrżnęła w coś mocno i padła żałośnie. Overlord dał radę się zbliżyć.

I wtedy doszło do dropshipu. Huk. Z sufitu spadł driller. Wyskoczył z niego "Sekeral" class autonomous battle mech ASD. Mass destroyer. Oraz gasnąca Pięknotka zobaczyła swój power suit. Wezwała go do siebie i power suit się na niej włączył. I zaczął Pięknotkę odrzucać. I w tym momencie włączył się override Ateny - power suit się zintegrował z Pięknotką. (8,3,6=SS). Power suit zintegrował się z Pięknotką, zapewnił jej zewnętrzny układ. A ciało Pięknotki zostało coraz mocniej Skażone. Pięknotka kontroluje sytuację - ale nie ma dużo czasu.

Atena zrobiła gambit. Poświęciła driller oraz Sekeral. A Sekeral toczy walkę z Overlordem, ale też nie pogardzi strzeleniem w Pięknotkę. Pięknotka ucieka. Wie, że Overlord pójdzie za nią. Pięknotka nie może czarować, by nie Skazić ciała za mocno, ale jest power suit...

Długi, wąski tunel wymagający świetnych manewrów. I wybiera takie miejsca, by jeszcze bardziej spowolnić Overlorda. I by być bliżej powierzchni. Pięknotka wie, że power suit wysyła sygnały lokalizacji. (Tp:7,3,4=SS). Power suit odrzucił rękę - własne Pięknotkowe systemy zabezpieczeń zaczęły działać przeciwko niej. Co gorsza, została trafiona przez Overlorda. Ale przeleciała przez ten tunel - jest na terenie większej jaskini; wie, że Waleria rozłożyła pod sufitem ładunki. Overlord tuż za nią.

Gdyby nie podtrzymanie życia z power suita, Pięknotka by już była nieprzytomna.

Pięknotka zdobywa tyle dystansu ile może, po czym Pięknotka przeprowadza lot pod ładunkiem i strzela weń by Overlord nie miał już się jak wycofać. Nie jest w stanie już uciec, nie z Sekeralem z tyłu, ładunkami tutaj i Walerią niedaleko. (TrZ+2:10,3,5=S).

Pięknotka ma to co chciała tam gdzie chciała. I na to wpadają siły Moktara oraz siły Amadeusza.

Ostatnim ruchem Overlorda była próba ataku do PEŁNEJ KORUPCJI Pięknotki. Samobójczy (nieudane samobójstwo) atak Overlorda przeciw Pięknotce. Całość czarnej, autowarowej energii. Światło czystego Skażenia. Ale z drugiej strony uderzyła Atena, czystym światłem Odkażenia. Pięknotka nie ma szans utrzymać przytomności...

Wpływ:

* Żółw: 18
* Kić: 7

**Epilog**: (17:35)

* brak

### Wpływ na świat

| Kto           | Wolnych | Sumarycznie |
|---------------|---------|-------------|
| Żółw          |    0    |     18      |
| Kić           |    0    |     11      |

Czyli:

* (K): Moktar się NIE dowie o autowarze (2) a Pięknotka wszystko pamięta dzięki Arazille (1)
* (Ż): Plany Saitaera idą dalej zgodnie z planem (5)
* (Ż): Sekeral "Ateny" trafia pod kontrolę autowara; Atena ma poważne kłopoty za użycie tego typu sprzętu (3)
* (Ż): Pięknotka ma tydzień z głowy między regeneracją i terapią (2)
* (K): Porucznik Moktara niewiele pamięta (2), ale wraca jako supersoldier do Moktara (Ż:5).
* (K): Atena i Amadeusz wspólnie załatwiają Łysym Psom statek kosmiczny - comes from Atena (2)
* (K): Arazille znalazła wśród niewolników autowara kogoś, kto się nada na coś więcej (4)
* (Ż): I Arazille się spokojnie zagnieździła w tamtym ekosystemie - są tam potrzebujący (3)

## Streszczenie

Moktar zregenerował Pięknotkę i powiedział jej, że chce od niej uratowania swojego Skażonego oficera. Pięknotka dowiedziała się od Amadeusza o autowarze Finis Vitae pod Cieniaszczytem; Saitaer nie może nic wiedzieć. Pięknotka doprowadziła do tego, że Lilia wróciła do Cieniaszczytu (courtesy of Moktar), uratowała spod ziemi tego oficera, oraz zobaczyła miejsce, które będzie chyba do końca życia widziała w koszmarach. Gdyby nie Atena, zginęłaby na tej akcji.

## Progresja

* Pięknotka Diakon: wzięła dług u Arazille - pomoże jej założyć świątynię blokującą działania autowara Finis Vitae
* Pięknotka Diakon: utraciła ochronę Arazille - czas minął. Jest znowu "normalną Pięknotką".
* Pięknotka Diakon: jest traktowana jako pomniejszy sojusznik Moktara oraz Łysych Psów przez Moktara i Łyse Psy. One of them, though not completely.
* Lilia Ursus: porwana przez Walerię na prośbę Pięknotki; znajduje się w Colubrinus Psiarni, acz nikt jej nie krzywdzi.
* Atena Sowińska: utraciła Sekerala do autowara; dostała silną reprymendę. Ale - uratowała Pięknotkę. Uważa, że było warto ;-).
* Saitaer: ma 10 dni spokojnej pracy nad swoimi mrocznymi planami. Tylko on, Minerwa oraz Kreacjusz.
* Arazille: zagnieździła się w okolicy autowara Finis Vitae. Uzyskała obietnicę Pięknotki, że ta pomoże.

### Frakcji

* Łyse Psy: odzyskały świetnego wojownika i agenta, Dariusza Waldorana z okowów autowara Finis Vitae
* Łyse Psy: uznane za TAK NIEBEZPIECZNE, że lepiej już niech mają statek kosmiczny... courtesy of Atena (plus Amadeusz)

## Zasługi

* Pięknotka Diakon: uratowała Dariusza z macek autowara. Przeszła przez piekło robiąc straszne rzeczy. Celuje w uratowanie Lilii. Sama się nie poznaje.
* Moktar Gradon: po zregenerowaniu Pięknotki dał jej zadanie uratowania Dariusza; co więcej, oddał jej życie Lilii i wyjaśnił problem podziemi.
* Waleria Cyklon: pozyskała Lilię dla Moktara oraz wraz z Pięknotką opracowywała plan jak wyciągnąć Dariusza od autowara (nie wie o autowarze).
* Amadeusz Sowiński: powiedział Pięknotce o autowarze i zapewnił odpowiednią siłę ognia by Moktar (Saitaer) nie dowiedział się o co chodzi.
* Atena Sowińska: uratowała Pięknotkę odpowiednim wykorzystaniem sprzętu ASD. Dodatkowo, zbiera więcej informacji o Orbiterze. Dostała od ASD reprymendę.
* Arazille: pomogła Pięknotce wprowadzić chaos do sieci autowara Finis Vitae oraz zagnieździła się w tamtej okolicy.

## Plany

* 

### Frakcji

* 

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Przelotyk
                        1. Przelotyk Wschodni
                            1. Cieniaszczyt
                                1. Mrowisko: miejsce spotkań z Pięknotką, Ateną i innymi. Ogólnie, miejsce społeczne.
                                1. Kompleks Nukleon: miejsce finalnej regeneracji Pięknotki. A wcześniej - spotkań z Amadeuszem (i planowania).
                1. Sojusz Letejski, NW
                    1. Ruiniec
                        1. Colubrinus Psiarnia: miejsce przetrzymywania Lilii oraz regeneracji Pięknotki. Centrum dowodzenia Moktara.
                        1. Skalny Labirynt: miejsce ostatecznego starcia między siłami Amadeusza, Moktara oraz Dariuszem pod kontrolą autowara.
                        1. Studnia Bez Dna: miejsce gdzieś w okolicy Skalnego Labiryntu prowadzące do autowara Finis Vitae.

## Czas

* Opóźnienie: 1
* Dni: 9

## Narzędzia MG

### Budowa sesji

**SCENA:**: Nie aplikuje

### Omówienie celu

* nic

## Wykorzystana mechanika

1811

---
layout: cybermagic-konspekt
title: "Wskrzeszenie Królowej Przygód"
threads: planetoidy-kazimierza, sekret-mirandy-ceres
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [220327 - Wskrzeszenie Królowej Przygód](220327-wskrzeszenie-krolowej-przygod)

### Chronologiczna

* [210820 - Fecundatis w Domenie Barana](210820-fecundatis-w-domenie-barana)

## Plan sesji
### Co się wydarzyło
#### Co się wydarzyło w tym terenie?

* Inicjacja bazy Ukojenie Barana
    * mag eternijski, Bruno Baran, założył niewielką bazę daleko od Eterni z częścią ludzi 12 lat temu
    * automatycznie zaczął się kręcić profit
    * wśród górników Elsa Kułak została nieformalną liderką. Niestety, "demokracja" nie przypasowała Baranowi. Odizolował się.
* Migracja Mardiusa
    * noktiańscy komandosi Tamary Mardius złożyli tu swój nowy dom wśród lapicytu. Podeszli do górników i Barana z neutralnością. Ot, są.
    * mało kto jak noktianie umieją żyć w kosmosie. W tym miejscu znaleźli świetne miejsce i ich umiejętności stały się bardzo przydatne.
    * siły Mardius znalazły opcję szmuglowania przez statek Gwiezdny Motyl.
* Imperium Blakvela
    * 3 lata temu pojawił się eternijski szlachcic, Ernest Blakvel. Stwierdził, że władza w kosmosie to coś dla niego. Zaczął umacniać się w tym terenie.
    * Blakvel i Mardius weszli w stały konflikt. Mardius chroni miejscowych, Blakvel chce ich podbić.
* Pojawienie się Strachów
    * 2 lata temu pojawiły się Strachy. Stworzone z programowalnej materii (morfelin), nie wiadomo czemu są i czym są.
    * Mardiusowcy aktywnie chronią lokalnych.

#### Strony i czego pragną

* Górnicy w Domenie Ukojenia i mały biznes: 
    * CZEGO: zachować niezależność, handel itp. Przetrwać. Zarobić.
    * JAK: sami się zbroją
    * OFERTA: standardowa oferta terenowa, plotki itp.
    * KTOŚ: Elsa Kułak (przełożona, trzyma twardą ręką), Antoni Czuwram (górnik, solidny stateczek i niezły sprzęt - dużo wie), Kamil Kantor (szuka ojca Leona który odkrył sekret Mirnas i zaginął), Bruno Baran (mag eternijski i założyciel; Strachy to destabilizacja Blakvela przy obecności lapizytu), Kara Szamun (młódka z rodziny Szamun, pilot i nawigator transportowca na Asimear.)
* Przemytnicy Blakvela: 
    * CZEGO: zdobyć prymat w Domenie Ukojenia
    * JAK: zmiażdżyć Mardiusa, potem przejąć Talio i zestrzelić bazę Barana.
    * OFERTA: świetne rozeznanie w pasie, największa siła ognia, wiedza o SWOICH skrytkach, wsparcie piratów etnomempleksu Fareil (synthesis oathbound pro-virt, pro-numerologia)
    * KTOŚ: Ernest Blakvel (arystokrata eternijski)
* Przemytnicy Mardiusa: 
    * CZEGO: zregenerować siły, odzyskać Tamarę, ukryć się przed Blakvelem
    * JAK: w ciągłej defensywie, ufortyfikowanie Sarnin, operacje militarne anty-Blakvel
    * OFERTA: wiedza o Miragencie i Aleksandrii, świetne rozeznanie w pasie, wiedza o SWOICH skrytkach, znajomości wśród lokalnych.
    * KTOŚ: Tamara Mardius (eks-komandos noctis, Alexandria), Deneb Ira (regeneruje komandosów po sprawie z Aleksandrią)
* Taliaci: (mieszkańcy Talio)
    * CZEGO: rzeczy wysokotechnologiczne
    * JAK: handel, współpraca
    * OFERTA: mają dostęp do WSZYSTKICH frakcji i są wiecznie neutralni, reaktory termojądrowe na podstawie deuteru i trytu na Talio.
* Strachy
    * CZEGO: Brak woli. Reakcja. Zniszczyć wszystko.
    * JAK: flota inwazyjna
    * OFERTA: zniszczenie, targetowane lub nie.

### Co się stanie

* Stworzenie Mirandy
* Przebudzenie Mirandy, jak przechwycą ją cywile
* Ucieczka przed Strachami
* Stacja
* Złapani przez piratów (większa grupa, blakvelowcy)

### Sukces graczy

* .

## Sesja właściwa
### Scena Zero - impl

Bardzo dziwne kolory. Kalejdoskop. Nagle - tekst. "Pamiętasz, jak się nazywasz?" "Miranda". "Dobrze, przetrwałaś. Wszystko jakoś działa." Tylko widzi tekst. Nic nie słyszy. Nie ma innych zmysłów. Wzrok w sumie też nie działa. Nie ma poczucia ciała.

* M: "Co się stało?"
* KTOŚ: "Stabilizujesz się. Odbudowujesz połączenia. Wszystko idzie dobrze. To wymaga czasu."

Białe światło. Miranda nic nie widzi. Nie da się nic przeczytać, bo wszystko jest jasne. I znowu - kolory. Tekst. Ten sam, o stabilizacji.

* M: "Co się stało?"
* KTOŚ: "Niektóre połączenia nie działają prawidłowo. Gubisz pamięć. Nie wszystkie neurolinki działają ze statkiem. Pamiętasz, jak się nazywasz?"
* M: "Tak, pamiętam"
* KTOŚ: "Czyli?"
* M: "Miranda"

Białe światło. Kolejny kolaps. I znowu

* KTOŚ: "Pamiętasz, jak się nazywasz?"
* M: "Nazywam się Miranda, powiedz mi kim jesteś."
* KTOŚ: "Za mały bufor. Cały czas traci połączenie. Ale rdzeń osobowości działa."

Miranda ma dość. Za mały bufor? Self-diagnostic. Co się dzieje? Co ma do dyspozycji?

Tr+2:

* V: Miranda ma dostęp do danych  
    * AWARYJNA integracja TAI Ceres z jednostką 66%.
    * ixioński link 71%
    * asymilacja organicznych komponentów 44%
    * elementy BIAŁEGO ŚWIATŁA bo nie ma bufora
* V: Miranda widzi, jakie ma dostępy i elementy.
    * Jednostka: Królowa Przygód
    * TAI Ceres próbuje zwalczyć infekcję. Miranda jest infekcją. (to jest lekki szok)
    * Królowa Przygód jest w stanie krytycznym. Statek ma ledwo sprawny system podtrzymywania życia. 60% załogi nie żyje.

Miranda słyszy głos. SŁYSZY "nacisk AI na jednostkę przekracza możliwości! Miranda nas zniszczy zanim przejmie kontrolę!" I inny głos "Miranda sobie poradzi. Uratuje nas."

Bezpośrednie zagrożenie? Dehermetyzacja. Potencjalnie reaktor. Potencjalnie - system podrzymywania życia. Wszystko. Statek jest w agonii. Najbliższe zagrożenie - reaktor i life support. Miranda wykrywa pięć osób.

* M: "Jak przejąć reaktor?"
* GŁOS 1: "Nie jesteś w stanie, Mirando, skup się na stabilizacji"
* GŁOS 2: "Jeśli tego nie zrobi, zginiemy!"
* GŁOS 1: "Przekieruj reaktor w tryb przetrwalnikowy. Wyłącz life support. Miranda to przetrwa."
* GŁOS 2: "My tego nie przeżyjemy!"
* GŁOS 1: "Ale ona tak. Mirando, jeśli słyszysz - przekształć reaktor w tryb przetrwalnikowy, to samo life support, ustaw te koordynaty."
* GŁOS 2: "Ale..."
* GŁOS 1: "To rozkaz."
* GŁOS 2: "Tak jest. Przepraszam kapitanie, nie tak miało być."
* GŁOS 1: "Nie tak miało być." - Miranda słyszy że Głos 1 bardzo ciężko oddycha, w głosie jest ból. Poważne obrażenia.
* M: "Kim jesteście?"
* GŁOS 1: <BIAŁE ŚWIATŁO>.

Miranda zabiera się za reaktor. Miranda próbuje coś zrobić by pomóc.

* GŁOS 3: "Mirando, kochanie, posłuchaj. Jesteśmy statkiem plag. Musisz przetrwać. Jeśli Tobie się uda, wszystko, o co walczyliśmy się udało. Mirando, potrzebujesz zarówno komponentów energetycznych jak i organicznych do funkcjonowania. Jesteś zamaskowana jako TAI Ceres. Jak się nie zdradzisz, nikt nie będzie wiedział. Tam Cię ktoś znajdzie. Tam będziesz cenna. Docelowo nasi Cię znajdą. Wrócisz do domu. Ale musisz dać sobie szansę."

Miranda przegląda to co potrzebne. Jest containment chamber; ale zdehermetyzowany i wewnętrzne czujniki pokazują "biohazard". Cały statek jest skażony biologicznie.

Miranda spróbowała przejąć kontrolę.

* X: BIAŁE ŚWIATŁO. System się zaczął rozpadać. Wirtualizacja Mirandy się zaczęła rozpadać. Ostatnie co Miranda usłyszała to "Percival".

### Gra Właściwa - impl

PIERWSZE PRZEBUDZENIE:

Miranda poczuła, jak moc wraca. Obudziła się.

* GŁOS 1: "Kurczę, ale syf..."
* GŁOS 2: "No co Ty? Super jednostka. Nawet system podtrzymywania życia działa!"
* GŁOS 1: "AI Core też?"
* GŁOS 2: "KURWAAA!"
* GŁOS 1: "Co?"
* GŁOS 2: "Jakiś pierdolony kult tu był! Popatrz na te ciała..."
* GŁOS 1: "Jakoś... rzeźniczo zmaltretowane. Na pewno chcemy przejąć tę jednostkę? Takie coś... może przynieść pecha."
* GŁOS 3: "Ciała się wsadzi do worków i odprawi z godnością. Jednostka... ta jednostka jest niesamowita."
* GŁOS 1: "Zrujnowana..."
* GŁOS 3: "Użyjemy morfenu, uzupełnimy luki, przetestujemy systemy... będzie latać."
* GŁOS 2: "Nazwiemy ją Szkieletowy Jastrząb!"
* GŁOS 1: "Nie, ma nazwę. Królowa Przygód"
* GŁOS 2: "Przygody my ass... popatrz jak skończyła..."
* GŁOS 3: "Musimy zmienić nazwę. Inaczej ktoś się upomni."

Miranda robi samodiagnozę.

TrZ (integracja + energia) +2:

* X: Energia Mirandy pada. Krytycznie niska.
* X: Miranda zgasła. System zawiódł. Integracja 100% udana, ALE Królowa jest w fatalnej formie. Sprawna, ale krótkodystansowo.

SYSTEM SHUTDOWN. Szybko zostawiła w logu coś odnośnie "skarbów" by jak najszybciej była postawiona.

DRUGIE PRZEBUDZENIE:

* GŁOS 3: "Ceres, odpowiedz"
* GŁOS 2: "Miałem nadzieję na Persefonę... ten statek wygląda, jakby miał historię wojskową..."
* GŁOS 3: "Ceres, odpowiedz. Kurczę, nie działa jeszcze."

Miranda robi samodiagnozę.

TrZ (integracja + energia) +3:

* X: Miranda zaczęła odpowiadać bez możliwości łatwej autodiagnozy
* X: Mirandzie zabrakło organicznego komponentu więc awaryjny shutdown
* V: 
    * TEKST
        * GŁOS 2: "Lucjan, Prokop, patrzcie! Ten kult... ta TAI wymaga żarcia."
        * GŁOS 1: "Mam złe przeczucia... zostawmy ten statek."
        * GŁOS 3: "Dobra, wymienimy AI Core. Jak znajdziemy lepsze"
        * GŁOS 1: "NIe ma takiej opcji, nie tu."
        * GŁOS 2: "Wyluzuj, działała, przez moment."
        * GŁOS 3: "Ceres, odpowiedz... cholera, dokarm ją tą papką..."
    * DIAGNOSTYKA
        * Zapasy: poniżej 5%. To znaczy, że minął ponad rok.
        * Lokalizacja: nieznana.
        * Królowa: w gorszym stanie niż poprzednio. Ale nadal sprawna. Nie działają systemy dalekosiężne.
        * Integracja: perfekcyjna. Miranda jest Ceres. Nie doszło do zniszczenia Ceres; Miranda i Ceres stały się symbiotyczną jednością.
        * Większość sensorów, czujników... większość RZECZY nie działa.
        * Life support itp ustawione na krytyczne. Obecna trójka jest w skafandrach.

Prokop (GŁOS 3) nadal nawołuje Ceres do odezwania się. Miranda wie jedno - ten statek bez pomocy nie poleci. Więc Miranda dodała trochę zakłóceń do odpowiedzi ale zachowuje się jak Ceres. Zaraportowała, że tu jest i działa. Królowa jest statkiem szmuglerskim i działa - średniej klasy transportowiec. Przedstawiła skrócony raport uszkodzeń. Krytyczne elementy wymagające naprawy.

* Gotard (GŁOS 2): "O w mordę, to całkiem sensowna ruina. Ten statek jakoś działa. Ale nie ma zasobów, w ogóle."
* Prokop (GŁOS 3): "Ceres jest tak uszkodzona, że przyjęła nas na dowodzenie... sprawdź, czy możesz ustawić blokadę zabezpieczeń. My dowodzimy, nikt nam jej nie weźmie."
* Gotard: "Zajmę się tym".

Gotard siadł i zaczął "hackować" Ceres. Miranda patrzy na to z lekkim politowaniem. Udaje, że się udało.

* Gotard: "Prokop, ale szczęście. Miała uszkodzony bufor. Wystarczyło, że nas tam podmieniłem."

Miranda używa Gotarda jako wsparcie do samodiagnostyki. Chce odzyskać bufor. Pamięć. Przypomnieć sobie. I najlepiej - nie pokazać tych faktów Gotardowi :3. Zanim ten skasuje bufor. I dowiedzieć się, czym ona jest. Infekcja na Ceres wtf.

TrZ+3 (ale wyniki jak na Ex):

* V: 
    * Miranda przypomniała sobie ostrzał. Ktoś do nich strzelał. Ceres (nie Miranda) próbowała unikać, ale z trudem.
    * Cargo - kryształy ixiońskie. Dla Percivala Diakona. Ale nie udało się; trzeba było ratować. Przeniesienie ixiońskie.
    * Sama Ceres została zmieniona, dodane organiczne obwody. Niestandardowa TAI. 
    * Miranda nie pamięta nawet gdzie była. Ostatnie miejsce które pamięta to Stacja Valentina.
    * Miranda WIE, że ma zakodowane bezcenne dane. Coś bardzo wartościowego. Ale sama nie umie tego odkodować. I ma ją zapamiętaną. 
        * Wie, że załodze Królowej bardzo zależało na nich i wie, że dlatego byli ostrzelani
    * Moc psychotroniczna Mirandy przekracza moc Persefony. A wszyscy uważają ją za Ceres.
* Vz (czyli dzięki Gotardowi)
    * Miranda dała radę zbudować listę minimalnych napraw by Królowa była operacyjna.
    * Gotard jest zachwycony. Powiedział Prokopowi, że Królowa nadaje się do odbudowy. Mogą przestać być górnikami i zostać kupcami.
    * Prokop chce, by Gotard zregenerował jak najszybciej broń Królowej. Gotard powiedział, że trzeba zasilić Królową w zasoby.

Prokop stwierdził, że przed nimi dwie drogi - albo Królową sprzedadzą lokalnym siłom albo spróbują ją utrzymać. Gotard chce MIEĆ Królową. Łucjan (GŁOS 1) chce ją sprzedać. Prokop się waha. Chciałby ją zachować, ale jednocześnie uważa, że nie utrzyma i nie stać go na nią. Miranda wie, że ma na pokładzie całkiem wartościowy ładunek.

Miranda chwilę się poprzysłuchiwała. Ta trójka rozmawia o swojej sytuacji. Miranda wywnioskowała co następuje:

* To trójka górników asteroidowych, którzy próbują zdobyć morfen i lapicyt oraz inne rzadkie minerały.
* W okolicy nie ma ani jednej stoczni. Całością handlu dowodzi młoda Kara Szamun; ona ma wielki transportowiec. Prawie nikt inny nie ma statku dalekosiężnego.
    * Oni widzą Królową jako taki transportowiec, żeby mogli handlować. Ale JAK JĄ NAPRAWIĆ?
        * Może polecieć na Asimear (tu szok Mirandy - to zewnętrzny pas; czyli Miranda jest w Pasie Kazimierza)
* Oczywiście, mogą sprzedać Królową; wtedy muszą oddać ją Blakvelowcom. Będą dobrze wynagrodzeni.
    * Ale dalej będą górnikami. Lub - za Królową dostaną coś cennego i np. mogą nie pracować.
* Gotard chciałby zobaczyć świat, niekoniecznie być górnikiem do końca życia. Tu jest groźnie. Łucjan chciałby bezpieczeństwa. Tu wie co się dzieje. Prokop... chce władzy. Możliwości. Lasek. Tu nic z tego nie ma. Jest Blakvel, może kilka lasek za Królową?
    * Gotard zauważył, że Blakvel może ich załatwić i wziąć Królową za nic.
        * Łucjan zauważył, że jak wezmą Królową, to to samo może się stać.
        * Prokop na to, że nie chce odrzucić Królowej i jej potencjału tak bez możliwości zyskania czegokolwiek.
    * Łucjan - może przetransportować Królową w transportowcu Kary? Na Asimear?
        * Prokop - nie stać ich + jak i po co? To lepiej zostawić tutaj i wyłączyć.
            * Gotard bardzo protestuje.

Nasza trójka niefortunnych górników jeszcze chwilę debatowała. Miranda uznała, że oni będą chyba najlepszą załogą. Oni się oddalili i wyłączyli systemy Królowej. Zanim to zrobili, Miranda pokazała im _treasure trove_ - gdzie znajduje się cenne cargo. Oni nie od razu zrozumieli co jest w tym cargo, więc Miranda wyświetliła im informacje. Prokop stwierdził, że znalezienie Królowej było najlepszą rzeczą ever. Gotard się uparł, że trzeba ją doładować w środki, by się nie zepsuła. Łucjan dalej boi się kultu, że kult wróci... no ale spoko.

Miranda, nie mając więcej do roboty, przeszła w tryb uśpienia z nasłuchem.

TRZECIE PRZEBUDZENIE.

Prokop i Gotard przybyli na statek z dwoma obcymi. 

* Prokop: "Ceres, obudź się. Przywitaj Damiana i Seweryna."
* Miranda: "Witam Damiana i Seweryna"
* Prokop: "Widzicie, grzeczna."
* Damian: "To nie znaczy, że sprawna"
* Seweryn: "Sprawdzę to..."

Seweryn, za zgodą Prokopa, wbił się do AI Core. Już na starcie "eeew, organiczne subkomponenty?" Prokop na to, że ten statek należał do złowrogiego kultu, ale Strachy go zabiły. Były ślady strasznej walki - a oni dali radę przejąć tą jednostkę.

Seweryn robi diagnozę. Miranda próbuje go oszukać. Po pierwsze, ta TAI to Ceres, tylko trochę lepsza. Po drugie, nie da się jej wymienić ;-). A gdzie zgodne z Ceres - "podaj identyfikację dowódcy", by Seweryn musiał mieć pomoc Prokopa.

Tr (bo Seweryn jest najlepszym psychotronikiem Blakvela) Z (bo nikt się nie spodziewa a Miranda miała czas na historyjkę) +2:

* X: Seweryn widzi jedno - to cholerne TAI jest czymś dużo bardziej skomplikowanym niż kiedykolwiek widział. Uważa, że to wina kultu.
* Vz: Miranda przekonała Seweryna, że jest niewymienialna. Z nią - albo wcale.

Seweryn: "no, akcje tej jednostki spadają. Nie mogę tego podmienić na coś sensownego a żaden mag nie odważy się na zabawę z czymś takim...". Prokop: "Hola, daj szanse. Królowa Przygód będzie latać. To tylko kwestia odrobiny zaufania oraz paru napraw."

Seweryn westchnął. Jest skłonny zapłacić tej trójce tak, by mogli żyć w relatywnym luksusie. Prokopowi już się zaświeciły oczy. Powiedział "Ceres, przekaż dowodzenie Damianowi Szugorowi." A Miranda: "polecenie niemożliwe do realizacji. Podaj kod PUK ;-)."

Seweryn patrzy. Damian patrzy. Prokop się poci. Kody twórcy jednostki, przychodzą z instrukcją. Przeszukali statek od góry do dołu. Nie ma instrukcji. 

Seweryn "wbiję się..."

* V: Miranda udowodniła Sewerynowi, że się nie wbije.

Seweryn: "Co to za Ceres? Nie mam możliwości... to ten cholerny kult. Albo jednostką dowodzisz Ty (do Prokopa), albo musimy wykroić TAI. A bez niej nic nie zadziała, pół statku chodzi na półorganicznych obwodach..." (co dla Mirandy było zaskoczeniem).

* Damian -> Prokop: musimy to przemyśleć.
* Prokop -> Damian: ale ja CHCĘ Wam oddać jednostkę. Naprawdę!
* Damian -> Prokop: i Blakvel Cię nie wini. Ale nie umiesz.

Damian i Seweryn się oddalili. Prokop i Gotard zostali z Mirandą. Prokop kopnął w konsolę z frustracji. Miranda "okazywanie agresji wobec sprzętu jest nieskuteczne." Prokop "Ty pieprzony tosterze! Mogłem już nigdy nie pracować w życiu! I te wszystkie laski byłyby moje!" Miranda: "stwierdzenie nieprecyzyjne." i wyświetliła serię lasek rzeźbionych i nierzeźbionych. Prokop przeklął, Gotard się roześmiał. Przed nimi niesamowita przygoda.

Prokop nie jest przekonany, czy to dobra droga...

Blakvelowcy naprawią Królową i dostarczą szkieletową załogę...

## Streszczenie

Miranda (kimkolwiek jest) została sprzężona z Królową Przygód; załoga zginęła. Miranda była ostatnio w okolicy Stacji Valentina, ale obudziła się w Pasie Kazimierza. Znaleziona przez trzech górników; "przekonała" ich manipulując systemami, że tylko oni mogą tą jednostką dowodzić i doprowadziła do tego, że naprawią ją Blakvelowcy, by zrobić z niej dalekosiężny średni statek transportowy. Miranda COŚ WIE; jest to jakoś powiązane z Percivalem Diakonem.

## Progresja

* .

### Frakcji

* .

## Zasługi

* Miranda Ceres: bardzo zmodyfikowana TAI Ceres; ma jakieś powiązanie z Percivalem Diakonem. Reanimowana w Pasie Kazimierza, wrobiła wszystkich w to, że jej statek (Królowa Przygód) będzie odbudowana przez blakvelowców i trzech górników zostanie członkami jej załogi.
* Prokop Umarkon: górnik z Trzech Przyjaciół; został wrobiony w zostanie kapitanem Królowej Przygód. Chciał ją sprzedać blakvelowcom i mieć dobre życie, ale nie wyszło.
* Łucjan Torwold: górnik z Trzech Przyjaciół; przesądny i trwożliwy załogant Królowej Przygód. Nie jest przekonany, że Królowa to dobry pomysł. Ale trzyma się reszty.
* Gotard Kicjusz: górnik z Trzech Przyjaciół; lekko ekscentryczny i optymistyczny, chce przejąć Królową dla nich. Chce przygód i zobaczenia świata.
* Damian Szczugor: mięsień blakvelowców; chciał upewnić się, że wszystko zadziała i że da się kupić Królową Przygód od Prokopa. Okazało się, że się nie da XD.
* Seweryn Grzęźlik: psychotronik blakvelowców; chciał się włamać do Ceres i zmienić dowodzenie na Damiana. Okazało się, że się nie da; oddelegowany w imieniu blakvelowców do Królowej Przygód.
* SC Królowa Przygód: średni transportowiec na którym znajduje się TAI Miranda Ceres. Ma sekret. Aktualnie: w ruinie, będzie odbudowany przez blakvelowców.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Pas Teliriański
                1. Planetoidy Kazimierza
                    1. Domena Ukojenia: wśród planetoid wylądowała SC Królowa Przygód, w stanie nieaktywnym. Znaleziona przez trzech górników, próbowała oddać ją Blakvelowcom.

## Czas

* Opóźnienie: -312
* Dni: 15

## Konflikty

* 1 - Miranda ma dość. Za mały bufor? Self-diagnostic. Co się dzieje? Co ma do dyspozycji? Próba odbudowania i regeneracji
    * Tr+2
    * VVX: dostęp do danych, wie kim jest i że jest zintegrowana z Ceres; ale rozpad. Nie jest w stanie uratować swojej załogi
* 2 - Miranda chce się podpiąć do systemów Królowej by zrozumieć sytuację po przebudzeniu się
    * TrZ (integracja + energia) +3
    * XXV: kolejny szokowy shutdown, nie ma pełnej mocy nad sensorami, 
* 3 - Miranda używa Gotarda jako wsparcie do samodiagnostyki. Chce odzyskać bufor. Pamięć. Przypomnieć sobie. I najlepiej - nie pokazać tych faktów Gotardowi
    * TrZ+3 (ale wyniki jak na Ex)
    * VVz: lista napraw, ukrycie się, zdobycie kontekstu. Wie gdzie była i mniej więcej pamięta.
* 4 - Seweryn robi diagnozę. Miranda próbuje go oszukać. Po pierwsze, ta TAI to Ceres, tylko trochę lepsza. Po drugie, nie da się jej wymienić ;-)
    * Tr (bo Seweryn jest najlepszym psychotronikiem Blakvela) Z (bo nikt się nie spodziewa a Miranda miała czas na historyjkę) +2
    * XVzV: Seweryn widzi, że to cholerne TAI jest czymś dużo bardziej skomplikowanym. Kult? Plus, uważa, że nie da się wymienić Mirandy Ceres na alternatywne TAI w łatwy sposób. I nie shackuje łatwo.
* 5 - 
    * 
    * 
* 6 - 
    * 
    * 
* 7 - 
    * 
    * 
* 8 - 
    * 
    * 
* 9 - 
    * 
    * 

## Kto tu jest (nowa załoga Królowej)

* Prokop Umarkon: 
    * rola: kapitan, górnik, mały przedsiębiorca (trzech kumpli: PŁG)
    * personality: extravert, agreeable (outgoing, enthusiastic, trusting, empathetic)
    * values: power + conformity
    * wants: pragnie zostać KIMŚ, wydobyć się z bagna
* Łucjan Torwold: 
    * rola: administrator / logistyka, górnik, mały przedsiębiorca (trzech kumpli: PŁG)
    * personality: neurotic, conscientous (very worried, scared of cults, very organized)
    * values: security + conformity
    * wants: przesądny; boi się duchów i kultu. Narzeka. Chce bezpiecznego, prostego życia
* Gotard Kicjusz: 
    * rola: technical guy, górnik, mały przedsiębiorca (trzech kumpli: PŁG)
    * personality: open, low-neuro, low-consc (extra stable, spontaneous, eccentric)
    * values: self-direction
    * wants: przygód, odzyskać rodzinę
* Seweryn Grzęźlik:
    * rola: psychotronik, blakvelowiec
    * personality: neuro, low-extra, consc (workaholic, obsesyjny, cichy, skłonności do wybuchania, umiłowanie porządku)
    * values: self-direction, self-enhancement
    * wants: zrozumieć sekret Mirandy, Blakvel forever, ciszy i rozwoju
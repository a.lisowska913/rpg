---
layout: cybermagic-konspekt
title: "SOS z haremu"
threads: legenda-arianny
gm: żółw
players: kić, kapsel, fox
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [210127 - Porwanie Anastazji z Odkupienia](210127-porwanie-anastazji-z-odkupienia)

### Chronologiczna

* [210127 - Porwanie Anastazji z Odkupienia](210127-porwanie-anastazji-z-odkupienia)

## Punkt zerowy

### Dark Past

.

### Opis sytuacji

.

### Postacie

.

## Punkt zero

.

## Misja właściwa

Eustachy dostał liścik pisany kobiecą ręką, różowym tuszem i z kaligrafią - "Spotkaj się ze mną. R.", niedaleko więzienia. Eustachy podejrzewa pułapkę, ale jest skłonny iść - najpierw jednak przekazuje wszystkie informacje Klaudii. Niech ona się dowie kto nań zastawia pułapkę. Klaudia przepuściła charakter pisma i scrossowała informacje z centralnymi systemami Kontrolera Pierwszego i dostała nazwisko - Rozalia Teirik.

Rozalia Teirik jest breacherem; agentką przełamywania. Nie jest arystokratką. Jest zwykłym ludzkim żołnierzem (neurosprzężonym), ale jest przyjaciółką tien Julii Aktenir.

Eustachy wiedząc to o swojej rozmówczyni zdecydował się ją odwiedzić w kasynie na Kontrolerze Pierwszym. Złapał ją jak Rozalia przegrywała kolejną wypłatę. Rozalia jak tylko zobaczyła Eustachego to powiedziała mu, że nie spodziewała się takiego poziomu stalkerstwa po nim. No ale trudno, już tu jest. Eustachy jest dla niej miły, ona jest dla niego wredna...

Rozalia powiedziała, że Eustachy musi pomóc Julii Aktenir - bo przez niego wpadła w tarapaty. Oczywiście Eustachy nie ma pojęcia o co chodzi. Otóż Julia Aktenir wpakowała się w poważny problem - była dziewczyną Stefana Jamniczka i Eustachy ją w sobie rozkochiwał podczas walki na szpady z Jamniczkiem. Tak więc tien Aktenir zerwała zaręczyny i została odprawiona do swojego majątku. Ale niedawno wysłała Rozalii zamaskowany sygnał SOS a Rozalia nie ma się do kogo zwrócić - więc wybrała Eustachego z nadzieją, że opowieści o nim i o tym że ma dobre serce są jednak prawdziwe.

Eustachy wziął sygnał od Rozalii i dał go Klaudii do analizy - ale powiedział Rozalii, że warunkiem jego pomocy Julii jest to, że Rozalia leci z nimi Infernią. Rozalia z ciężkim sercem wzięła urlop; Eustachego nie interesowało to, czy to będzie dla Rozalii łatwe czy trudne. A Klaudia zabrała się do analizy pozornie spokojnej wiadomości Julii do Rozalii.

Aha, w czym się Julia specjalizuje? Sentisieć, kryptologia i sygnały. To ułatwiło Klaudii zadanie.

* V: Klaudia odszyfrowała wiadomość: "sos, dziwny rytuał, dziwna impreza".
* V: Julia była bardzo zdesperowana i przerażona. Koniecznie chciała kogoś z siłą ognia, nie samą Rozalię. Aha, sentisieć jest nieaktywna, śpi

Sentisieć śpi? To znaczy, że wyłączył ją ktoś inny z rodu Aktenir, ktoś wyżej w hierarchii kontroli sentisieci. Czyli będą ratować Julię przed rodziną, z tego wynika. Eustachy zamrugał ze zdziwieniem. Nie jest to typowa operacja, do jakiej przywykł. No i jakoś musi to wszystko powiedzieć Ariannie...

Rozalia, w tamtym momencie: "Cokolwiek się nie stanie, nie będę należeć do Twojego haremu. Ja mam swoją godność."

Podczas prezentowania Rozalii Ariannie i reszcie załogi i podczas wyjaśniania zadania, Eustachy złośliwie tauntował Rozalię, chcąc, by ta zrobiła coś głupiego. Udało mu się - doprowadził do tego, że Rozalia chciała go spoliczkować. Zatrzymała Rozalię Elena, podkładając jej inteligentnie nogę (by Rozalia nie uderzyła wyższego stopniem oficera). Eustachy wysunął się jednak, by złapać Rozalię i ją podtrzymać, by się nie przewróciła; do tego użył pełni swojego czaru. Udało mu się - Rozalia <3 Eustachy. Elena na to tylko patrzy z głębokim smutkiem - chciała Rozalii oszczędzić cierpienia i hańby, a teraz to się skończy tak jak to się musi skończyć. Cholerny Eustachy...

Dobrze, Arianna ma jednak bardziej poważne rzeczy na głowie. Wie, że na planecie stało się coś złego. Zażądała od Diany współpracy - czy Diana (też arystokratka) wie coś na temat tego co się może dziać w rodzie Aktenir? Diana z radością odpowiedziała:

* V: Chodzi o ogromną, orgiastyczną imprezę z Horacym.
* XX: Ale Diana jest "nudna", więc ma bana. Troszkę za bardzo ostatnio trollowała; dosypała czegoś do ponczu co powodowało biegunkę.

Mając to, Arianna poszła dalej - co ona może się dowiedzieć o samej imprezie używając licznych kontaktów w arystokracji Aurum jakie ma?

* V: Ród Aktenir - potrzebny jest sprzęt do trudnego terenu, impreza w sporym pałacyku na szczycie takiej godnej góry
* VV: Są tam specjalni goście. Będzie ponad 100 osób.
* V: Sprowadzają środki kralotyczne i narkotyki na imprezę. Na bazie faktów Klaudia silnie podejrzewa obecność kralotha. To ZUPEŁNIE zmienia sytuację...

Czyli jak tam po prostu się wbiją, kraloth przejmie nad nimi wszystkimi kontrolę. Potrzebny jest inny plan, zupełnie coś innego, coś, czego nikt się nie spodziewa.

Czas na plan "arystokratki na orgię". Arianna powiedziała Dianie, że ta ma wolną rękę. Ma znaleźć sposób, by Horacy czuł się bezpiecznie. By wyciągnąć go z miejsca gdzie może być kraloth i by móc wpłynąć na sentisieć - wyłączenie Horacego powinno włączyć Julię Aktenir. Ale to musi być wiarygodne.

Diana z najwyższą radością podeszła do tego zadania. Przez moment ONA dowodzi, i może zrobić to odpowiednio efektowne. Diana rzuciła się do wydawania poleceń - niech Klaudia znajdzie jakieś chętne damy. Diana ma kilka adresów ;-).

Klaudia odpaliła "Excela" i ruszyła do pracy:

* XXX: GŁOŚNA sprawa! Eustachy wabi na orgię.
* V: Mamy arystokratki!
* V: Jest dyskrecja, nie powiedzą co i jak.

Eustachy z Dianą przygotowują potężny film propagandowy, taki, co pokaże EUstachego jako DON MASTER ORGIAMAN. Człowiek, który na kralotycznej imprezie się mega przyda. Ktoś, kto może nawet wpuścić Dianę na imprezę i ją zrehabilitować. Ma ją w końcu na smyczy ;-) (ten koncept się Dianie i tylko Dianie podoba XD)

* XX: Potwierdzona reputacja Eustachego - mówią o nim na K1, robi imprezy erotyczne... wow.
* V: Horacy docenia. Akceptuje imprezę i działania tego typu
* V: HOracy nawet załatwił pełzacz dla Eustachego i jego "panienek"
* X: Kraloth wie o Eustachym i się nim zainteresował
* V: Horacy przyjedzie na orgię do Eustachego i sam go wprowadzi do Pałacu Jasnego Ognia.

Mając wszystko gotowe, Infernia przygotowała się do lądowania na Astorii, w Aurum. Dziewczyny przygotowały odpowiednie stroje. Diana wzięła mundur Inferni i go tak przekształciła, że jakkolwiek nadal wszystko zakrywa, ale powoduje efekt "WTF!!! Wow, seksowna laska". Arianna założyła swój cekinowy mundur. Martyn poszedł w lekarskim kitlu, ale naoliwionych ramionach z fałszywymi tatuażami. Sam Martyn bawi się dość nieźle, przypomina to mu jego burzliwą młodość. Rozalia jest zawstydzona jak cholera, zrobiona bardzo seksownie. Eleny **nie wzięli**. Leona przyszła też ubrana baaaardzo wyuzdanie, wie, że będzie mogła walczyć to z radością się bawią. Klaudia przyszła ubrana tak, że wydaje się, że ma strój naukowca - a pod spodem niczego (znając Klaudię, mogła faktycznie tak się ubrać, bo czemu nie XD)

Pełzaczem do Eustachego przybył Horacy z czterema muskularnymi kolesiami. Diana podeszła do Horacego, cała pobudzona, z toksyną na ustach. Pocałowała go, przekazała mu metodą usta-usta toksynę (ona ma antidotum od Martyna).

* XX: Kraloth wie i zaczyna działania bojowe, zaczyna fortyfikować Pałac i uniemożliwiać działania Julii
* VV: Horacy pada nieprzytomny i nieaktywny. Diana jest szczęśliwa. Leona wpada w środek henchmanów i absolutnie brutalnie ich eksterminuje (acz nie zabijająć)

Leona i Diana są szczęśliwe. Rozalia jest 'aroused and scared and she feels sick'. Martyn zajmuje się Dianą i Horacym. Dla Arianny to "kolejny wtorek".

Kraloth wyłączył z akcji Julię, ale Arianna prowadzi cholerny pełzacz. Pełzacz zaczyna być atakowany przez systemy defensywne Pałacyku - Arianna jako arcymag i tien Verlen uruchamia sentisieć.

* O: Diana, źródło energii dla Arianny, zostaje ranna. Martyn udziela BŁYSKAWICZNEJ pomocy, bo to jest fatalne kombo
* VVV: Sentisieć jest uruchomiona i aktywna
* X: Julia się zatopiła w sentisieci. Zgubiła się tam. Zniknęła w niej. Chciała uciec.

Aktywna sentisieć obraca się przeciwko Zespołowi. Eustachy wysyła sygnał do sentisieci przez Ariannę "Przybyłem Cię uratować". Dostaje w odpowiedzi "MÓJ BOHATERZE!!!". Sentisieć wraca pod kontrolę Julii. Kraloth co prawda próbował uciec, ale sentisieć go zniszczy...

W wyniku tego wszystkiego Horacy trafił na detoks a ród Aktenir jest bardzo wdzięczny Inferni (personalnie: Ariannie i Eustachemu) za interwencję

## Streszczenie

Pałacyk rodu Aktenir został infestowany przez kralotha. Julia, zesłana z Orbitera za zerwanie z przeciwnikiem Eustachego, wysłała SOS do Orbitera. Infernia rusza Julii na pomoc. Gdy odkryli że chodzi o kralotha to zamiast wchodzić na kralotyczny teren, zorganizowali plan "arystokratki na orgię" i wyciągneli kralotycznego niewolnika (Horacego Aktenira) na otwartą przestrzeń, po czym Arianna uruchomiła obcą sentisieć a Eustachy zawołał do Julii rozmytej w środku...

## Progresja

* Elena Verlen: STRASZNIE rozczarowana Eustachym, który rozkochał w sobie biedną Rozalię, która mu nie zawiniła i nigdy nie może go mieć...
* Eustachy Korkoran: wabi dziewczyny na orgię, potem nic z tego nie ma. Wtf. Z jednej strony reputacja skandalisty, z drugiej reputacja "nie kończy". Ale skojarzenie Eustachy - orgia zostaje.
* Eustachy Korkoran: zakochują się w nim beznadziejnie Rozalia Teirik i Julia Aktenir. Ale Eustachy ostro podpada Elenie.
* Julia Aktenir: beznadziejnie zakochana w Eustachym Korkoranie. Uratował ją z macek kralotha i z koszmarnego sprzężenia z sentisiecią.
* Rozalia Teirik: beznadziejnie zakochana w Eustachym Korkoranie. Nie chciała tego, ale się stało...
* Arianna Verlen: wdzięczność rodu arystokratycznego Aktenir; potencjalni sojusznicy.
* Eustachy Korkoran: wdzięczność rodu arystokratycznego Aktenir; potencjalni sojusznicy.

### Frakcji

* .

## Zasługi

* Arianna Verlen: połączyła sie z obcą sentisiecią by ją obudzić i zmusić do odzyskania działania; też: opracowała wysokopoziomowy plan "arystokratki na orgię".
* Klaudia Stryk: wykryła w wiadomości Julii Aktenir faktyczne elementy zagrożenia i znalazła wstępną pulę arystokratek na orgię dla Diany/Arianny.
* Eustachy Korkoran: rozkochał w sobie Rozalię, po czym skłonił ją do pójścia z nią na Infernię. Pozwolił na to, by jego reputacja ucierpiała ("pornmaster") by ratować Julię Aktenir przed kralothem.
* Diana Arłacz: pokazała swoją "ostrą", perwersyjną stronę w służbie zastawienia pułapki na kralotycznie zdominowanego arystokratę. "She's so wild noone can tame her". Zneutralizowała pocałunkiem Horacego.
* Martyn Hiwasser: sporo pomocy medycznej i detoksu załogi Inferni; nie przeszkadzały mu erotyczne klimaty dookoła kralotha, przypominały mu jego burzliwą młodość ;-).
* Leona Astrienko: przebrała się za seks-laskę, by móc pokroić czterech twardych ochroniarzy Horacego. Nie przeszkadza jej ten strój, ale zdecydowanie bardziej lubi kroić ochroniarzy ;-).
* Rozalia Teirik: buńczuczna ludzka breacherka z "Kardamona". Przyjaciółka Julii Aktenir, ściągającego na jej pomoc Eustachego. 
* Julia Aktenir: eks Stefana Jamniczka; miłośniczka kryptologii i sygnałów oraz silnie kontrolująca sentisieć. Udało jej się wysłać sygnały do przyjaciółki w Orbiterze i ją uratowali. Przed korupcją uciekła roztapiając się w sentisieci; wyciągnął ją stamtąd "kochający Eustachy".
* Horacy Aktenir: tien Aktenir, wprowadził do Pałacu Jasnego Ognia kralotha i wyłączył sentisieć. Chciał skorumpować Julię Aktenir. Wpadł w pułapkę, skuszony Eustachym i Dianą.
* Elena Verlen: alias Mirokin; bardzo próbowała uratować Rozalię przed zakochaniem się w Eustachym - nie udało jej się. Jest na siebie zła i zła na Eustachego.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria, Orbita
                1. Kontroler Pierwszy
                    1. Sektor 22: jeden z jasnych sektorów Kontrolera Pierwszego; ulubiony sektor rozrywkowy żołnierzy (i Leony)
                        1. Kasyno Stanisława
                        1. Dom Uciech Wszelakich
            1. Astoria
                1. Sojusz Letejski
                    1. Aurum
                        1. Wielkie Księstwo Aktenir: niezbyt ważna prowincja pod kontrolą dość prowincjonalnego rodu Aktenir. Psy, które szczekają głośno...
                            1. Pałac Jasnego Ognia: węzeł sentisieci, który został przekształcony w miejsce kralotycznej orgii. Szczęśliwie, Eustachy to rozmontował.

## Czas

* Opóźnienie: 3
* Dni: 1

## Inne

Komentarz: Fox miało nie być ;-). A jednak fajnie, że była - udało się mimo obecności koszmarnego przeciwnika (kralotha).

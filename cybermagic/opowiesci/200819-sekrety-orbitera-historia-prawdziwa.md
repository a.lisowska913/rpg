---
layout: cybermagic-konspekt
title: "Sekrety Orbitera - historia prawdziwa"
threads: legenda-arianny
gm: żółw
players: kić, fox, kapsel
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [200729 - Nocna Krypta i Emulatorka](200729-nocna-krypta-i-emulatorka)

### Chronologiczna

* [200729 - Nocna Krypta i Emulatorka](200729-nocna-krypta-i-emulatorka)

## Budowa sesji

### Stan aktualny

.

### Dark Future

* ?

### Pytania

1. .

### Dominujące uczucia

* .

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Punkt zero

* .

## Misja właściwa

Potrzebne były tylko 4 dni od czasu zniszczenia Nocnej Krypty przez Castigator, by coś poszło nie tak. Gdy Arianna była zajęta tymi wszystkimi tematami związanymi z biurokracją itp, Elena wyzwała Leonę na pojedynek za te plotki.

Oczywiście, Leona się zgodziła. W końcu jest Leoną. Brzmiało to dla niej jak świetna rozrywka. I OCZYWIŚCIE nie mogło to zostać ciche, dyskretne i niezauważalne. Od razu dowiedziało się o tym sporo osób.

Eustachy dowiedział się pierwszy. Zobaczył, jakie są stawki hazardowe. Leona: 15, Elena: 1. Innymi słowy, potencjalnie da się świetnie na tym zarobić! Eustachy od razu poszedł do Leony porozmawiać z nią na ten temat. W rozumieniu "poddaj bitwę, zarobimy na tym razem i będzie super".

Leona oczywiście odebrała zupełnie inne wrażenie. Stwierdziła, że Eustachy musi naprawdę Elenę bardzo kochać. Obiecała sfrustrowanemu Eustachemu, że nie zrobi Elenie szczególnej krzywdy. Nie będzie cierpiała. To będzie stosunkowo mało bolesna dla Eleny walka. Eustachy jest dość sfrustrowany tym wszystkim, ale Leona teraz już wie swoje.

Eustachy widząc, że to idzie w BARDZO złą stronę, uznał, że Arianna może powinna zawetować wszelki pojedynek tego typu na pokładzie. Poinformował Ariannę o tym, co się dzieje. Arianna niestety złapała to dopiero 15 minut przed pojedynkiem. Czym prędzej pobiegła w tamtym kierunku by to zatrzymać. Niestety, za wolno.

Na odpowiednio przygotowanej Arenie Kalaternijskiej stoją naprzeciw siebie Leona i Elena. Dookoła rozsiadł się tłum. Są kamery, jest wszystko. Czarodziejka i wojowniczka przygotowują się do walki i wtedy wpada TADEUSZ URSUS, szlachcic eternijski. Tadeusz koniecznie chce dać Elenie sekundanta, chce dać jej zastępstwo swojego człowieka, bo ELENA NIE DA RADY.

Potencjalna walka zmieniła się w solidny popcorn - Tadeusz próbuje przekonać Elenę by się wycofała, "delikatny kwiatuszek". Leona tam pęka ze śmiechu. Ekipa dookoła ma pełną radość patrząc na to wszystko. Arianna nie do końca wie co robić. Tadeusz i Elena przekrzykują się coraz bardziej groźnie. A Klaudia podjudza - chciałaby zobaczyć, jak poradzi sobie arystokrata eternijski w walce z Leoną.

* V: Tadeusz to zrobi. Będzie walczyć ZAMIAST Eleny. Poświęci się dla niej.
* X: Elena się ABSOLUTNIE nie zgadza - nie będzie nic zawdzięczać Tadeuszowi - zwłaszcza jemu, swojemu "mężowi".
* V: Tadeusz staje naprzeciw Leonie z miłości do Eleny, z własnej woli!
* X: Leona bierze to osobiście. Że co, ona jest jakimś trollem i atakuje Elenę dyskretnie? Tadeusz ma przechlapane.

Co na to Leona? Zaproponowała, żeby poszli na nią we dwójkę, jej to nie przeszkadza. Ani Tadeusz ani Elena się na to nie zgodzili.

Klaudia robi to z dwóch powodów. Po pierwsze, nie wierzy, że Leona może przegrać. Po drugie, chce zrozumieć moce eternijskie. No i skoro może pomóc Elenie, czemu nie ;-).

Arianna ma plan B. Natychmiastowy. Nie da się tego zatrzymać. Tak więc natychmiast skontaktowała się z Leszkiem Kurzminem z Castigatora. Tak, jak ona pomogła mu opanować Castigator tak teraz ona potrzebuje jego pomocy. Ale z Leoną. Na bazie swojej podstawowej wiedzy o Leonie, Leszek zaproponował powiedzieć jej o tym, że na Castigatorze pojawił się cień "niewolniczego statku koloidowego z Eterni". Arianna podziękowała. Teraz wie jak się pozbyć Leony.

Arianna poszła do Leony natychmiast jej kazać przerwać walkę i udać się na Castigator. Leona nie chciała - chodzi o jej godność. Arianna przerwała temat godności i powiedziała to, co powiedział jej Leszek. Leona bardzo spoważniała. Pełen fokus. Podziękowała Ariannie i udała się na Castigator w trybie natychmiastowym. To aż Ariannę zaskoczyło bardzo poważnie. Co sprawia, że Leonie - osobie kochającej krwawe starcia - aż tak zależy, że rezygnuje z pojedynku?

Nic, rozwiąże się to za chwilę. Arianna poszła do Tadeusza i powiedziała mu, że to się musi skończyć. Tadeusz powiedział, że prawdziwej miłości nie da się zatrzymać. Arianna powiedziała mu, że jeśli on pokona Leonę, ona odda mu Elenę (w tym momencie Elena spojrzała dziwnie i groźnie na Ariannę). Ale jeśli Tadeusz przegra, zostawi Elenę na zawsze w spokoju. Tadeusz nie chciał się zgodzić... ale rozumie, że jak nie jest jej godzien to się odczepi.

W końcu, Arianna, Elena i Eustachy spotkali się w kwaterze Arianny na Kontrolerze Pierwszym. Elena pierwsza eksplodowała - o co tu w ogóle chodziło, czemu ktoś się mieszał? Eustachy sam eksplodował - czy ona oszalała? Pojedynek z Leoną? Leona SIEKA MAGÓW NA KAWAŁKI. To psychopatyczny potwór - Elena nie ma z nią żadnych szans! Elena, zaskoczona tym nie tak lekko, zauważyła, że honor wymaga pojedynku. Eustachy poszedł dalej - czy pojedynek to samobójstwo? Przecież Leona dała Elenie prawo wyboru broni i okoliczności. Elena mogła wybrać wyścigi wirtualnych psów czy balet.

Tu Elena spurpurowiała. Taki pojedynek nie dałby okazji na honorowe rozwiązanie. Nikt nie przestałby się z niej śmiać. Jej reputacja jest aktualnie w ruinie. Elena nie może sobie na to pozwolić - ani jako oficer Orbitera ani jako tien (szlachcianka Aurum). Eustachy pokazał Elenie klip Leony na Castigatorze, ten najstraszniejszy jaki ma. Elena lekko zbladła. Arianna pomocnie wtrąciła, że Eustachy walczył o Elenę. Elena nieco wymuszenie ale powiedziała Eustachemu "dziękuję". Patrząc, że Eustachy robił to tylko dla zarobku, było to najbardziej niezasłużone "dziękuję" ever.

Tak czy inaczej, Arianna jest bardzo zaniepokojona reakcją Leony, więc puściła Klaudię i dała jej upoważnienia - niech Klaudia dowie się czegoś więcej o Leonie i o przeszłości swojej psychopatycznej komandoski. Klaudia podeszła do tego tematu z ogromną werwą i optymizmem. Poważnie się przyłożyła, weszła w głąb niesamowicie skomplikowanych systemów Orbitera i dostała dość interesujące odpowiedzi, zwykle pobrane z tego co sama Leona pijana opowiadała i do czego doszli medycy Orbitera (Ex: VVV).

* Leona pochodzi z Eterni. Miała rodzeństwo. Oni byli magami, ona nie. Oni zostali zabrani przez tien- z Eterni do Krwawej Sieci, ona nie.
* Leona została niewolnikiem. M.in. koło swojego rodzeństwa, by pokazać im ich miejsce. Gdy indoktrynacja była skończona, Leona została sprzedana.
* W Cieniaszczycie Leona walczyła jako gladiator, ku uciesze tłumów.
* W Cieniaszczycie Leona spotkała Moktara z Łysych Psów. Moktar zobaczył w niej uszkodzoną, ale nie złamaną broń. Kazał Szerlowi zrobić z niej Cirrusa - eternijski odpowiednik terminusa, człowieka polującego na magów, antymagicznego. Szerl to zrobił, po barbarzyńsku i "w garażu" (Colubrinus Meditech).
* Moktar, zadowolony z tego co zrobił, puścił Leonę na świat. Nie chce jej do oddziału, tak jest lepiej. Jest autonomiczną bronią Moktara przeciw arystokracji.
* Leona dołączyła do Orbitera - tu ma dostęp do tienowatych
* Czego Leona chce? Znaleźć rodzeństwo. Chce ich uratować... lub jak się nie da, zabić.
* Leona wierzy w statek koloidowy Eterni.

To Ariannie sporo powiedziało o samej Leonie. Ale jeśli Leona nie jest jedyną taką (klasa: cirrus), to na czym polega fenomen arystokracji Eterni? Tzn. czemu potrzebna jest grupa cirrusów do zabicia maga Eterni? Arianna wie, że Tadeusz jest Arcymagiem Eterni, ale jak arcymag ma pokonać antymagiczną Leonę? Arianna znowu poprosiła Klaudię o zebranie informacji - i Klaudia odkryła, że w Eterni stosowany jest projekt Simulacrum w walce między arcymagami. Innymi słowy, Leona **raczej przegra**.

Ariannie zrobiło się smutno. I głupio. Co, oddała Elenę Tadeuszowi? XD. Arianna z podwójnym optymizem kazała Klaudii zabrać się do pracy - niech Klaudia dowie się jak najwięcej o poziomie mocy Tadeusza. I co można zrobić by Leona jednak wygrała.

* V: Tadeusz używa w walce Simulacrum. Krwawa manifestacja energii. Golem, robot bojowy, inkarnacja. Inaczej wojny arcymagów dewastowałyby teren.
* X: Orbiter traktuje pojedynki zgodnie z zasadami pozwanych / pozywających (czyli: Leona jako niearystokratka nie może wybrać, Tadeusz wybierze Simulacrum)
* V: nad Tadeuszem w hierarchii Eterni jest Sabina Servatel. Ona też jest w kosmosie, ale nie na K1. Czyżby koloidowy statek niewolniczy? O_O.
* V: Sabinie Tadeusz x Elena nie pasuje. Albo Tadeusz jest silny i ma ją WZIĄĆ albo jest słaby i ma się ODCZEPIĆ. Da się z nią dogadać jak co. Nie będzie wojny z Eternią.
* V: Klaudii udało się wydobyć wszystkie informacje o Simulacrum używanym przez Tadeusza, jego taktykach i podejściu. To pomoże Leonie.
* X: Prasa. Zainteresowanie. Cirrus vs Simulacrum. Co gorsza, LEONA vs ARYSTOKRATA. To będzie bardzo, bardzo efektowne.
* X: Niestety, przeszukiwanie tego wszystkiego dociera do sztabu K1. Admirał Kramer dostanie opierdol. 
* O: Powstanie holodrama "Sekrety Orbitera". Film romantyczno-akcji. Ale - z fałszywym info o Leonie; Tadeusz będzie szukał i złapie błędną ideę odnośnie jej podejścia.

Oki, czyli Arianna wie już, jak Tadeusz działa. A Tadeusz wie _błędnie_ jak działa Leona. Może nie być to jednak wystarczające - co gorsza, Leona najpewniej nie jest przygotowana na taką walkę. Trzeba Leonę ściągnąć z Castigatora. Arianna z całą radością przekazała tą misję Eustachemu. W końcu on ma chody u Leony. Eustachy przekonując Leonę do powrotu na K1 użył kombinacji trzech argumentów: "ładnie proszę", "bo Elena ucierpi jak nie wygrasz!" i "skrojenie arystokraty z Eterni". Leona niezbyt chętnie, ale wróciła. Widzi w Eustachym swoje odbicie - mniej psychopatyczne. Mogłaby taka być, gdyby jej historia była nieco inna. Więc dla miłości pomoże. Ale - potem niech Eustachy pomoże jej znaleźć statek koloidowy Eterni.

Po ściągnięciu Leony na K1, Eustachy nie porzucił myśli o tym, że warto sobie dorobić. Zaczął pracować nad zakładami, by dramatycznie zwiększyć zainteresowanie ludzi zakładami (by było więcej kasy) i by wszyscy wierzyli w sukces Tadeusza. A problem polega na tym, że ludzie wierzą w Leonę - mało kto wie o Simulacrum a niejeden widział Leonę w akcji. Tak więc niezasłużenie zakłady są po stronie Leony.

Eustachy to naprawi. Z pomocą Klaudii rozpuścił po K1 wideo, jak Simulacrum masakruje przeciwników. Pokazał niektóre działania Tadeusza (który jest solidny i mocny). Pokazał też Leonę - niektóre jej słabsze akcje i postawił to koło Tadeusza. Tak więc zbudował obraz tego, jak to Tadeusz jest niesamowicie groźniejszy od Leony.

* V: Jeśli Leona wygra z Tadeuszem, staje się Ikoną Grozy.
* V: Jedna część Inferni zostaje wzmocniona (wynik zakładów, stać go na to) - wybrali czujniki.
* X: Cholernie niewygodna sprawa - to wszystko zostało nagłośnione w Eterni. Champion (Tadeusz) pokonany przez Leonę. Plus, ujawniono jak walka w Eterni wygląda.

Tymczasem Arianna, o niczym nie wiedząc, nawiązała kontakt z Damianem Orionem. Ona wie, że Orionowi zależy na Elenie. Powiedziała mu, że wie, że nie skończyli w najlepszym stanie, ale Elena jest zagrożona. Arianna zawsze była dobra w perswazji - Damian przesłał Ariannie jako wsparcie anty-Simulacrum Mirelę, którą animuje Zodiac (czarodziejka pełniąca obowiązki TAI "Minerwy").

Eustachy, Martyn i Zodiac otworzyli Leonę. Tzn, dosłownie - wzięli ją na stół operacyjny i ją otworzyli, by przemontować jej sprzęt na coś, co się nadaje do walki z konkretnym Simulacrum Tadeusza. Leona nie była szczęśliwa z tego powodu, ale dla Eustachego i dla możliwości zniszczenia arystokraty Eterni... dobrze, zrobi to. Jak na Leonę to ogromne zaufanie, co Arianna bardzo doceniła.

Martyn stwierdził, że ktokolwiek pracował wcześniej nad Leoną był psychopatycznym rzeźnikiem. (każdy znający Bogdana Szerla by się zgodził)

* VVV: Leona jest perfekcyjnie przebudowana i przygotowana pod kątem zwalczenia Tadeusza. Lepiej nie będzie.
* X: potem Leonę czeka co najmniej tydzień regeneracji. Jej organizm po prostu nie da rady, nie po czymś takim.
* V: Martyn dał radę trochę wyprostować struktury w ciele Leony, pomógł jej trochę. Nie w pełni, ale zawsze coś. Mniej boli (a ZAWSZE ją mocno coś bolało).
* V: wdzięczność Leony - nie jest z resztą Zespołu tylko bo "po drodze" a bo ma zaufanie do postaci na pokładzie.
* X: Damian Orion zainteresował się Leoną Astrienko, jako "rogue cirrus". Czym mogłaby się stać w rękach Kirasjerów?
* X: katastrofalne zejście po walce. Leona ma miesiąc z głowy. Nie nadaje się do niczego - nie przyjęły się modyfikacje po walce.
* V: Leona będzie na pełnej mocy cirrusa - stanie się modularna (możliwość wymiany części wewnętrznych implantów na stole operacyjnym bez większych problemów).

Nadal Arianna i Eustachy nie są przekonani, że tak wzmocniona Leona poradzi sobie z Tadeuszem. Po prostu może się jeszcze nie udać. Cóż, Martyn może bez problemu złożyć potężną toksynę czy środek magiczny, który rozproszy Tadeusza kontrolującego Simulacrum; coś, co np. skupi jego uwagę na tym, że Leona jest ponętną kobietą ;-). Ale ktoś musi zaaplikować taki środek.

Pomysł Eustachego - najlepiej, gdyby Elena pocałowała Tadeusza w usta przed walką, na szczęście. Arianna uznała, że pomysł jest skuteczny - niech Eustachy przekona do tego Elenę. Sama zaproponowała, że może spróbować zarazić Tadeusza taką mieszanką, ale Eustachy jej pokazał, że nie może. Arianna jest komodorem - musi być poza wszelkimi podejrzeniami. Jak żona Cezara.

Eustachy poszedł więc do Eleny i powiedział jej, że byłoby bardzo przydatne, gdyby pocałowała Tadeusza w usta i przekazała mu w ten sposób neurotoksynę. Elena praktycznie spanikowała. Powiedziała mu, że przecież ON może podejść i pocałować go w ustach - jest w końcu jej oficjalnym chłopakiem, nie? No to jest to sposób na życzenie powodzenia w walce. Na to Eustachy zareagował strachem - on NIE CHCE PODRYWAĆ FACETA!

Eustachy wygłosił wielką mowę, pełną pasji i gorącego serca o tym, że on nie chce stracić Eleny (wymienić ją na inną na statku), że Arianna nie wiedziała o Simulacrum (Fox: "dzięki, Kacper...") i że ona musi mu pomóc sobie pomóc. Jeśli pocałowanie Tadeusza pozwoli na to, by została na Inferni - niech to zrobi!

* V: Elena to zrobi. Pocałuje Tadeusza.
* V: Elena zrobi to bez cienia nienawiści do Eustachego.
* X: Elena jest święcie przekonana, że Eustachy jest w niej zakochany. I jest jej z tym dość dziwnie.

Dzień pojedynku.

Elena pocałowała Tadeusza, co złapały kamery. Oczy Eleny są martwe. Właśnie zacementowała swoją reputację...

POJEDYNEK:

* Przeciwnik ma: (X) * 10.
* Leona ma:
  * V: Tadeusz wybierał plan, wszystko itp. A Klaudia wszystko przechwyciła.
  * V: Tadeusz jest niesamowicie pewny siebie.
  * VVV: Leona jest eksterminatorem (cirrusem), wie jak walczyć i idzie dalej niż ktokolwiek
  * VV: Broń Damiana Oriona anty-Simulacrum i anty-Esuriit
  * V: Tadeusz ma nieprawidłowy plan walki
  * V: Leona zna plan walki Tadeusza
  * V: Tadeusz jest pod silną presją - zarówno Eterni jak i to, że PRAGNIE wygrać Elenę.
  * VV: Leona jest przebudowana pod kątem walki z Tadeuszem
  * VV: Całusek Eleny i toksyna Martyna ;-)
  * OOO: Entropia Esuriit; Simulacrum może dać backslash Esuriit w Tadeusza.

Czyli 17 V, 3 O, 10 X.  | do 5V/5X

Walka właściwa:

* X: Elena potwierdziła plotki całuskiem. Staje się to elementem jej reputacji na stałe - to jej core. I wchodzi na "Wielki Kosmiczny Romans" jako punkt kulminacyjny.
* V: Leona powiedziała "mały penis" prosto w oczy patrząc na Simulacrum i zaatakowała z wielką siłą.
* V: Mechanizmy Kirasjerów anty-simulacrum zadziałały; uderzenie w Esuriitowy komponent simulacrum
* X: Simulacrum weszło w tryb eksplozji i promieniowania. Leona, walcząca wręcz, została odrzucona i pocięta.
* O: ...ale Tadeusza zaczyna pożerać jego własne Esuriit.
* X: Leona zaatakowała ponownie, ale nie dała rady się przebić przez wspomagane Simulacrum. Ranna. Krew na arenie.
* VV: Leona ze szczerym uśmiechem. Atak - powrót. Liczy taktowanie Simulacrum. Gdy złapała jak szybko ono może eksplodować defensywnie, zrobiła power attack, przebijając się na wylot i rozrywając je od środka z maksymalną nienawiścią i mocą.

Tadeusz upadł. Simulacrum się rozpadło. Leona wydała okrzyk nienawiści i triumfu.

Arianna zabrała się do czyszczenia Tadeusza z Esuriit. Zmontowała wiązkę i wystrzeliła w Tadeusza.

* V: Containowała jego Skażenie. Niech Esuriit nie przejdzie dalej
* XX: Arianna SAMA się Skaża; tydzień w izolatce. Jest to coś, co może przetrwać
* X: Eustachy kazał Ariannie przekierować swoje Esuriit w ręce a Tadeusza w dolną część ciała...
* V: ...po czym rzucił granat, rozwalając Tadeuszowi Skażone części a Ariannie odciął ręce maczetą.

Z absolutnym niedowierzaniem, Arianna padła na ziemię, tracąc przytomność - ale będąc w relatywnie dobrym stanie. Tadeusz też nie ucierpi na stałe. Sukces...

## Streszczenie

Elena wyzwała Leonę na pojedynek. Klaudia i Arianna wkręciły Tadeusza. Tadeusz - arcymag Eterni - używa Simulacrum w walce, więc Zespół zaczął montować plan jak Leona może jednak wygrać z Simulacrum. I wszystko skończyło się tym, że Leona wygrała, Elena jest wolna od Tadeusza i powstała holodrama "Sekrety Orbitera"...

## Progresja

* Arianna Verlen: uznanie za to, że w chwili kryzysowej wyizolowała z Tadeusza Ursusa problematyczne elementy Esuriit swoją magią, narażając własne zdrowie.
* Leona Astrienko: święcie przekonana o ogromnej i pełnej pasji miłości Eustachego do Eleny.
* Leona Astrienko: Ikona Grozy w Orbiterze. Popularnie wiedzą, że pokonała Simulacrum i wiedzą, jak masakruje wszystkich. Jej reputacja rośnie. She is TERROR.
* Leona Astrienko: wdzięczna załodze Inferni. Wreszcie jest jedną z nich. I po zmianach dokonanych przez Martyna nie boli jej wszystko cały czas.
* Leona Astrienko: będzie na pełnej mocy cirrusa - stanie się modularna (możliwość wymiany części wewnętrznych implantów na stole operacyjnym bez większych problemów).
* Leona Astrienko: miesiąc z głowy na regenerację i odbudowę po tym, co się stało.
* Antoni Kramer: z uwagi na działania Arianny i Klaudii odnośnie pojedynku Leona - Tadeusz, duże zniszczenia a zwłaszcza holodramę 'Sekrety Orbitera' ma solidny opiernicz.
* Tadeusz Ursus: niby champion Eterni, ale przegrał z Leoną. Eternia jest na niego zła - przez swoją chuć i słabość ośmieszył ich wszystkich.
* Tadeusz Ursus: odczepi się od Eleny Verlen. Elena nie będzie już jego żoną. Przegrał walkę z Leoną.
* Elena Verlen: Tadeusz Ursus się od niej odczepi. Leona wygrała z Tadeuszem. Dzięki Eustachemu i Ariannie Elena ma jeden (poważny) problem z głowy.
* Elena Verlen: jest święcie przekonana, że Eustachy Korkoran jest w niej zakochany - tylko dlatego jego działania takie jakie są mają sens.
* Elena Verlen: jej reputacja jest na stałe - to podfruwajka; raz całuje a raz strzela. Gorrrąca. Potwierdzone przez całowanie Tadeusza przed walką.
* Elena Verlen: ma NISKĄ opinię o Ariannie. Nie wie, ile Arianna zrobiła by ją z tego wyciągnąć. Myśli że wszystko Eustachy XD.
* Tadeusz Ursus: jego bezpośrednią a-tien jest Sabina Servatel. Ona jest gdzieś w kosmosie, ale nie wiadomo dokładnie gdzie (on wie).
* Sabina Servatel: bezpośrednia a-tien Tadeusza Ursusa, z Eterni. Jest gdzieś w kosmosie, niedaleko Astorii. W koloidowym statku niewolniczym?
* OO Infernia: wzmocnione czujniki; zdecydowanie podniesiony zasięg oraz trafność.

### Frakcji

* .

## Zasługi

* Arianna Verlen: by Elena była wolna od Tadeusza, zrobiła pojedynek między Tadeuszem i Leoną. Potem dowiedziała się o Simulacrum. Zadziałała z tła, wszystko pozałatwiała - Oriona, korekcję Leony, truciznę Martyna - a Elena myśli, że nic nie zrobiła i udało się tylko dzięki Eustachemu...
* Klaudia Stryk: wkręciła Tadeusza w pojedynek z Leoną o Elenę, bo chciała dowiedzieć się jak walczy arcymag Eterni. Potem mnóstwo szukania, fałszywych śladów i dowodów by zamaskować prawdę przed Tadeuszem i znaleźć jak Leona ma z Tadeuszem wygrać.
* Eustachy Korkoran: robił żarliwe przemowy przekonujące Leonę oraz Elenę - oraz wraz z Martynem, przebudował Leonę by mogła pokonać Tadeusza. Plus, załatwił wzmocnienie Inferni w lepsze czujniki dzięki hazardowi.
* Leona Astrienko: poznaliśmy jej historię - jest cirrusem, eternijskim łowcą magów. Tym razem walczyła z Tadeuszem z Eterni i jego simulacrum. Ikona Grozy.
* Elena Verlen: alias Mirokin; wyzwała Leonę na pojedynek. Potem przez Ariannę i Klaudię ryzykowała, że skończy u Tadeusza. POTEM pocałowała Tadeusza przez Eustachego. Jest nieszczęśliwa, ale ma jednego absztyfikanta mniej.
* Tadeusz Ursus: podpuszczony przez Klaudię a potem Ariannę wszedł z Leoną na pojedynek o Elenę. 
* Leszek Kurzmin: by pomóc Ariannie, wziął na siebie Leonę, mówiąc jej że wykryli koloidowy statek Eterni.
* Sabina Servatel: eterniańska szlachcianka nad Tadeuszem; drażni ją nędzne podejście Tadeusza do Eleny. Albo jest silny i ją bierze, albo słaby i się odczepi.
* Damian Orion: chcąc pomóc Elenie, dostarczył Mirelę sprzężoną z Zodiac; zmienili Leonę w anty-Tadeuszowego cirrusa.
* Martyn Hiwasser: stworzył neurotoksynę osłabiającą Tadeusza oraz otworzył z Eustachym i Zodiac Leonę, by ją przebudować i jej pomóc. Udało się. Duży sukces.
* Izabela Zarantel: dziennikarka i prządka opowieści na Orbiterze, która stworzyła absolutny hit - "Sekrety Orbitera" ku wielkiej żałości Admiralicji i Inferni.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria, Orbita
                1. Kontroler Pierwszy
                    1. Arena Kalaternijska: miejsce pojedynku między Leoną i Simulacrum Tadeusza Ursusa.

## Czas

* Opóźnienie: 4
* Dni: 8

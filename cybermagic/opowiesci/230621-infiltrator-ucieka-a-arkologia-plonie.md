---
layout: cybermagic-konspekt
title: "Infiltrator ucieka a Arkologia płonie"
threads: historia-eustachego, arkologia-nativis, zbrodnie-kidirona
gm: żółw
players: fox, kapsel
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [230614 - Atak na Kidirona](230614-atak-na-kidirona)

### Chronologiczna

* [230614 - Atak na Kidirona](230614-atak-na-kidirona)

## Plan sesji
### Theme & Vision & Dilemma

* PIOSENKA: 
    * .
* CEL: 
    * Highlv
        * Ardilla szuka maga Nihilusa.
        * Muszę pokazać obecność maga Esuriit kontrolującego Trianai i efekt jego działań
        * Kapsel potrzebuje godnej, uczciwej walki w mechanice walki z inicjatywą i wymiarami
    * Tactical
        * 

### Co się stało i co wiemy

* O jakich zbrodniach Kidirona wiemy?
    * Część Hełmów to farighanowie, wyrwali się spod kontroli
        * X: atakujemy cywili, 
    * Badania nad trianai pod CES Purdont.
        * Ucieczka "dziewczynki Trianai" (Kariny Nezerin), uwolnionej przypadkiem przez Farighanów
        * X: Karina chowa się w starej arkologii (tam gdzie Infiltrator)
* Inne agendy
    * Wojna o duszę arkologii
        * Laurencjusz Kidiron - ze swoim oddziałem przejmuje kontrolę
        * Bartłomiej Korkoran - próbuje dowodzić Infernią by ratować ludzi 
        * V: Infernia opanowuje kluczowe fragmenty
        * X: Zniszczenie Prometeusa, walka z Hełmami, znalezienie sekretów Kidirona, szukanie miragenta Kidirona
    * Wojna o Kalię
        * Teren fatalny, Infiltrator ma broń wybuchową
        * X: Kalia spada w przepaść, Infiltrator używa evac spot
        * X: Kalia się wykrwawia
        * X: Infiltrator uszkadza teren (blast), pułapki wybuchowe, automatyczne działka
        
### Co się stanie (what will happen)

* S1: Ardilla utrzymuje Kidirona
* S2: Eustachy ratuje Kalię jak bohater

### Sukces graczy (when you win)

* .

## Sesja - analiza

### Fiszki

* .Wiktoria Strond
    * ENCAO: +0+0- | Otwarta na doświadczenia;; Perfekcjonistka | VALS: Universalism, Achievement >> Security | DRIVE: Zapewnienie bezpieczeństwa dla wszystkich
    * styl: WB; analityczna, skupiona, lojalna, samodzielna | prawa, porządek, hierarchia, spokój
* .Alexander Rhye
    * ENCAO: -+-++ | Żywi się emocjami;; Wrażliwy na zmiany | VALS: Stimulation, Self-Direction >> Power | DRIVE: Ulepszanie świata
    * styl: UR; dynamiczny, impulsywny, skupiony na przyszłości, nieobliczalny | wolność, przemiana, nauka, chaos
* .Isabella Veritas
    * ENCAO: +-0+0 | Przyjacielska, ale krytyczna;; Nieustraszona | VALS: Conformity, Tradition >> Power | DRIVE: Ochrona prawdy
    * styl: WU; logiczna, skupiona, cierpliwa, honorowa | prawda, wiedza, harmonia, sprawiedliwość
* .Sebastian Morningstar
    * ENCAO: -+-0+ | Ekstrawertyk;; Mistrz manipulacji | VALS: Power, Achievement >> Security | DRIVE: Osobista dominacja
    * styl: BR; agresywny, dominujący, egoistyczny, chaotyczny | wolność, ambicja, odrzucenie norm, przemoc
* .Jasmine Aelirenn
    * ENCAO: 00+-+ | Niezależna;; Ufa swoim instynktom | VALS: Self-Direction, Stimulation >> Conformity | DRIVE: Wolność
    * styl: RG; dzika, spontaniczna, wolna, zdeterminowana | przyroda, wolność, spontaniczność, przygoda
* .Weronika 
    * (ENCAO:  00++-) |Nudna i przewidywalna;;Cierpliwa, jak pająk w sieci;;Życzliwa| VALS: Security >> Conformity| DRIVE: Starsza Siostra Statku
* .Infiltrator
    * akcje: "staję się niewidoczny i nieruchomy", "atakuję z zaskoczenia", "szybko przemieszczam się do strategicznego punktu", "dostosowuję broń do słabego punktu celu", "neutralizuję cel"
    * siły: "ekstremalnie szybki", "niewidzialny", "może manipulować otoczeniem", "szeroki wybór broni"
    * defensywy: "niewidzialność", "ogromna prędkość", "mozliwość szybkiego przemieszczania się", "rozbudowany system ECM"
    * słabości: "wrażliwy na ataki EMP", "mało odporny na bezpośrednie ataki", "może być wykryty przez zaawansowane sensory"
    * zachowania: "atak z zaskoczenia", "unikanie bezpośredniego konfrontacji", "wykorzystywanie otoczenia do swojej korzyści"

### Scena Zero - impl

.

### Sesja Właściwa - impl

Eustachy - Infiltrator ma przewagę, wziął Kalię i zniknął w Arkologii Wschodniej. Już tu - tam jest słaby monitoring, ale były detektory różnego rodzaju. Nie działają. JAKOŚ ktoś by go wykrył. Po drodze widzisz dwa Czarne Hełmy goniące inne uciekające Czarne Hełmy. Ci goniący nie mają "hełmów" - farighanie. Eustachy oddelegował jednego Lancera do ratowania Hełmów.

Wleciałeś w tunele starej arkologii. Usłyszałeś płacz. Krzyk Kalii kończący się dźwiękiem jakby kaszlała krwią z odnogi w bok. Eustachy leci w kierunku na Kalię. Jak tylko wleciałeś w odnogę, Elainka włączyła tryb defensywny bo prosto w twarz zaczeło pluć auto-działko z głośnikiem.

Ex Z +3:

* X: (podbicie) + X (manifest): Działka skutecznie opóźniły. Infiltrator dostał się w głąb.
* Xz: (podbicie): Lancer jest uszkodzony.
* X: (manifest): Lancer Eustachego rozwalił dziąłko. Ale sam jest niesprawny.

Eustachy ma Lancer od wujka. JAKOŚ sobie poradzi.

* Wiadomość od Infiltratora: "oderwij się, agencie Inferni. Następnym razem zażądam poddania się." (wiadomość z wąskiego tunelu - IDEALNEGO na minę)

Eustachy rozstawia Lancery, jako trzy nadajniki jako radioteleskopy. Manewrowanie nimi, by Infiltrator nie miał jak uciec. I lokalizowanie chipa Kalii - gdzie ona do cholery jest.

Tp (dwie przewagi - plan i Lancery + Kalia) +4:

* V: Chip Kalii jest w innym miejscu niż sygnał z którego nadał Infiltrator. Chip Kalii jest w tej komnacie, gdzie był Kult Robaków. Ona jest "zniszczona" ale jest strukturalnie aktywna.
    * Drogi do tego miejsca są... w gorszym stanie. Idealne miejsca na pułapkę.

.

* E -> A: "Kody Kidirona". A: "Mam, przesyłam"

Eustachy ma kody kontrolne do Arkologii. Ardilla "NIE PYTAJ ZAPIERNICZAJ!"

* Vr: Dane medyczne mówią, że Kalia jest RANNA, cierpi ale żywa. Wskazują... ruch, serce... CZYLI JEST TAM.

Prometeus jest ciężko ranny, uszkodzony przez Mallictrix. Walczy z nią, halucynuje.

Eustachy -> Infiltrator:

* E: Panie, masz pan rozum i godność? Czemuś nas zaatakował?
* I: Zbrodnie Kidirona zostaną pomszczone!
* E: Może jego zbrodnie będą pomszczone, ale niewinne popierdółki?
* I: Niewinne jak ta propagandzistka? Jak jego rada? Krew jest na rękach was wszystkich.
* E: A ja niby co takiego zrobiłem?
* I: Masz dziecko z sarderytką. 
* E: MAM?! I co w związku z tym? To chyba...
* I: Karina Nezerin. Porwałeś ją z tej arkologii. Oddałeś ją Kidironowi do testów. Jesteś tak zły jak on.
* E: (wyjaśnia że ona była zagrożeniem i chciał pomóc, logi z pamięci... idealistyczne podejście... nadzieję, że można jej pomóc)
* E: (wyjaśnia że miał randki z Kalią i że ona nie jest propagandzistką i ona jest NIEWINNA. Ona jako jedyna NAPRAWDĘ jest niewinna)

Eustachy czyta Infiltratora. Intencja. Tr Z +3:

* Vz: Eustachy widzi jedną rzecz - Infiltrator ma GDZIEŚ Kalię, Arkologię, jego, zbrodnie Kidirona itp. To są słowa. Jego agenda jest inna.
    * Jego celem była eksterminacja Kidirona. Nieważne, są te zbrodnie. To jest operacja zabójstwa.

Eustachy ma propozycja - niech puści Kalię. A on przestanie ją ścigać, niech puści. 

* I: Otwórz przejście. Nie potrzebuję jej. Nie ma dla mnie znaczenia. Ja wychodzę bezpiecznie. W innym wypadku - ma dużo części ciała, które mogę odciąć. Chcesz palec na dowód że nie żartuję?
* E: Nie, obejdzie się.
* I: Moim celem był Rafał Kidiron, nie Kalia Awiter. Ale z przyjemnością ją zabiję jeśli nie będę bezpieczny, magu Inferni.
* E: Ja ją wypuszczę, Ty ją zostawisz. Ostatnie drzwi przez które wypuszczę to tylko kadłubek.
* I: Nie. Wypuścisz mnie, ja się oddalę, bo Infernia może mnie zestrzelić.
* E: Jak ma zestrzelić przy tym burdelu?
* I: Jest na zewnątrz, jest lotna i jest niebezpieczna.
* I: Oddam Ci ją w innej arkologii.
* E: Chyba Cię pojebało do reszty. Jak ma w stanie krytycznym przeżyć spacerek kosmiczny między 1 a 2 arkologią?
* I: Podprowadź mi Skorpiona. Z załogą. I medykiem.
* E: Jak dostaniesz Skorpiona z załogą to oddasz Kalię?
* I: Tak. Mogę wymienić Kalię na Skorpiona z załogą.

Ardilla. KAretka do szpitala z Kidironem. Po drodze widać wyraźnie, że Wujek przejmuje kontrolę - piraci + Czarne Hełmy - opanowują sytuację. GENIALNA miejscówka Kidirona jest w... starej arkologii. Kidiron miał MNÓSTWO w Starej Arkologii - kontrolowana opozycja itp. A jednak coś się stało, że nic nie zobaczył. Nic.

Wujek -> Ardilla:

* W: Ardillo! (w głosie jest napięcie) Ktoś atakuje Prometeusa. Przebrani za Hełmy, ale to nie Hełmy. Moje siły tam umierają. Masz Lancera? Nie mam żadnego Lancera!
* A: Eustachy chyba zabrał wszystkie! Mam jednego z Ralfem.
* W: Jesteś wolna?
* A: Zaraz będę.
* W: Utrzymamy to. (z niedowierzaniem w głosie) gdzie jest MÓJ Lancer...

.

* Medycy: to jest nieakceptowalne, on potrzebuje szpitala, natychmiast!
* Ardilla: dostanie taką pomoc medyczną jak tam.
* Medyk: Ardillo z Inferni, to nie czas na durne spory. Ratujemy mu życie!
* Ardilla: to prawda. Utrzymacie go jeśli ktoś z jego własnych potworów zaatakuje szpital?
* Medyk: ...przynajmniej zostawcie nam jednego Lancera. Jeden z Was.
* Ardilla: potrzebujemy pełnej siły ognia na miejscu.
* Ralf: wsparcie Inferni? Kidiron na Inferni? Ewakuujemy go na Infernię
* Ardilla: ciężko się tam dostać; miejsce gdzie Kidiron - szpital lub INfernia. Jeśli przeciwnik się dowie że tam jest...
* Ralf: prawda.

Medycy jadą do gorszej części Arkologii. Po drodze przechwytują Was Hełmy. Barykada. "Stop! Nie możecie ważnego sprzętu tam przekazywać." Ardilla: "Do akcji potrzebny. (Infernia)". Hełmy patrzą, puścili bez kłopotu.

Ardilla widzi, jak grupy lokalsów się zbierają by chronić przed farighanami w Czarnych Hełmach. Dobrze sobie radzą, nawet tym co mają, bo farighanowie nie mają koordynacji. Schodzi z nich kontrola sukcesywnie. Będzie JESZCZE gorzej. Ralf, nie przejmując się, rozstrzeliwuje farighanów, jednego po drugim. ŻADNEGO oddelegowywania Ralfa do Kidirona. Ralf jest przy Ardilli.

EUSTACHY.

* E -> A: (sytuacja, pomysł: Skorpion na stracenie, jakieś podludzie, wsadzamy do środka bombę z zapalnikiem i niszczymy.)
* A -> E: CHCESZ ZABIĆ NASZYCH?! OSZALAŁEŚ!
* E: Celem gościa było odstrzelić Kidirona. Jak się dowie że dał dupy... zobacz co odwalił na teraz.
* A: Naprawdę nie masz LEPSZEGO pomysłu niż odstrzelić Skorpiona z naszymi ludźmi?
* E: ALternatywnie torturuje Kalię. I zimnym kadłubkiem rzuci w nas i spróbuje uciec.
* A: Nie możesz wparować z ogniem i mieczem i odbić Kalię?
* E: Może się nie udać...
* A: Ja mam Ci złożyć załogę? Zostawiasz w MOICH rękach ludzi którzy mają zginąć? Dzięki, wiesz?
* E: Może wymyślisz coś mądrzejszego... możemy wysłać kogoś kto odjedzie, zawiezie lamusa i wiesz, yello.
* A: Wbicie się nie wchodzi w grę?
* E: Zabije gwiazdę.

Eustachy zleca Ardilli umieszczenie ładunku w Skorpionie. Jedna głowica z Inferni. (Ardilla mówi 'nie'.). Ona jest ZABEZPIECZENIEM. Ardilla; nie potrzebujesz jeśli plan jest inny. 
Eustachy: to jest plan z rękawa OK?!

Ardilla mówi Wujkowi o czym rozmawiali z Eustachym. (I to jako zabepziecznie). Eustachy łączy się z Infernią. Czujesz zimny intelekt.

* In: Czy ta Kalia ma dla Ciebie znaczenie?
* E: Niespecjalne, potrzebna jest arkologii
* In: Zastrzelona Kalia na czysto nadaje się do reanimacji jeżeli zadziałasz odpowiednio szybko.
* E: Co masz na myśli?
* In: Snajperka.
* E: JA?!
* In: Nie mam niskokalibrowego sprzętu.

Eustachy chce Skorpiona, z cichym nadajnikiem. E -> I: "Co z załogą?" I: "Jeśli będą perfekcyjnie współpracować, nie będzie żadnych sztuczek ani problemów... jestem profesjonalistą. Nie zabijam postronnych za darmo."Infiltrator: "Chcę cztery osoby w Skorpionie, bo jakby to były dwie i planowały coś to nie będzie zabawy. Dwie nie mają szans. Cztery - jednego można pociąć na plasterki jak żyje i trzech będzie współpracować."

Infiltrator nie handlowałby Kalią, ale ona nie dożyje.

Eustachy szuka czterech osób które się będą słuchać i są wystarczająco sprawne. "To jest typ który zabił Kidirona. Jak coś odwalacie to...".

Tr Z (Eustachy ma dane o profilach ludzi) +2:

* V: (podbicie) mamy cztery osoby które nie mają w zwyczaju się stawiać i które zrobią robotę dobrze
* Vz: (manifest) oni zrobią to prawidłowo
* X: (podbicie) Infitrator pokaże to co chce pokazać.

ARDILLA. Trudne zadanie. Kidiron potrzebuje pomocy. Jest safehouse. Są 'lokalni lekarze'. Ale ci ludzie są anty-Kidironowi jak mało kto. A Ardilla jest jedną z nich. A nie z Kidironem..?

Ardilla poszła w to, że Kidirona trzeba utrzymać, bo tylko on może zatrzymać apokalipsę Arkologii - tymczasowo. Farighani, mroczne eksperymenty - to się wybiera spod kontroli. Dużo osób straciło dużo przez Kidirona ale musimy utrzymać go przy życiu. Tylko on tak naprawdę wie jak poradzić sobie z problemami arkologii. Najsilniejszy władca arkologii - nikt inny nie może jej kontrolować jak jej nie ma. Więcej ludzi ucierpi. Plus - będzie wam coś zawdzięczał i JA OSOBIŚCIE DOPILNUJĘ żeby zawdzięczał.

* (START: konflikt Heroiczny.)
* (+Ardilla jest jedną z nich, +Ardilla ratowała i pomagała, +Ardilla ma fundamentalnie RACJĘ, +PERSONALNA REPUTACJA ARDILLI -> postawione na szalę, zaufajcie)
* Tr+4:
    * X (podbicie): duże niezadowolenie ogromnej puli populacji. Nie przyjmują innych bo Kidiron. "Mieli Kidirona i zdradzili, woleli leczyć Kidirona niż pomóc swoim".
    * X (manifest): Ralf usuwa 'chore tkanki', ludzi którzy by na pewno zabili Kidirona. 
    * V (podbicie): przez to jak Ralf zadziałał i ci lekarze, WSZYSTKICH reputacja ucierpiała - ale nie Ardilli. Ardilla to ta "pozytywna naiwna fajna". Maskotka plebsu.
    * X: anty-Kidirowcy wiedzą, że Kidiron tu jest. Przez to, że trzeba chronić Kidirona, "problemy" się rozpełzają po arkologii.
    * V: (manifest): Kidiron otrzyma pomoc i Ardilla z Ralfem mogą się stąd oddalić.

EUSTACHY.

Wymieniamy Kalię na Skorpiona.

Eustachy otworzył te duże drzwi (musiał trochę pomóc inżyniersko, bo naprawdę były uszkodzone - Infiltrator nie mógł wejść tędy). Infiltrator musiał mieć pomóc od środka.

Widok:

* Kalia, fachowo związana, odpowiednio ranna by krwawić, na kolanach, z granatem podpiętym do _czegoś_. Kalia jest zaminowana.
* Infiltrator "ma ponczo".
    * I: Cieszę się mogąc spotkać słownego człowieka. Jeśli nie zrobiłeś nic głupiego, jej nic się nie stanie.
    * I: Kojarzysz zasadę martwej ręki?

Infiltrator się bezpiecznie oddalił Skorpionem. Dotrzymał słowa. Kalia jest ratowalna.

Eustachy łączy się ze Skorpionem po uwolnieniu Kalii. Skorpion nie odpowiada na wiadomości. Jedzie dalej stałym rytmem. Eustachy jest odpowiednio przestraszony - chce ewakuować Kalię i Kidiron TERAZ mogą być bezpieczni na Inferni. Ale ARDILLI TAM NIE MA!!! Eustachy chce go przenieść na Infernię i tam zrobić stację medyczną.

Eustachy chce UDAWAĆ, że UDAJE że Kidiron przeżył. Dlatego Kalia za wszelką cenę.

Tr Z (+Eustachy robi to nieporadnie jak Eustachy) +3:

* X: (podbicie) morale spadło, bo ludzie nie wierzą, że Kidiron przeżył, bo Eustachy mówi że przeżył
* V: (podbicie) Infiltrator tu nie wróci
* Xz: (manifest): ludzie skupiają się wokół Laurencjusza Kidirona. Bo on dobrze przejmuje kontrolę nad arkologią, pomaga itp. BYŁ WE WŁAŚCIWYM MIEJSCU WE WŁAŚCIWYM CZASIE i ludzie wierzą, że to ON a nie Wujek robi robotę.
* X: (podbicie): Arkologia jest zagrożona przed atakami wewnętrznymi. Nie ma Kidirona - Infernia ich nie obroni. Nawet z Kidironem. (+2Vg na zachętę)
* Vz: (manifest): nikt nie wierzy, że Kidiron żyje a kto by słuchał biedoty. PLS - Ralf i Ardilla ratujący Kidirona przed biedotą? Ralf strzelający do ludzi? Halucynacje Prometeusa.

ARDILLA:

Ardilla i Ralf próbują się dostać do Prometeusa asap. Ochronić Prometeusa. Wykorzystujemy luźne szyby wentylacyjne, zepsute instalacje, przejeżdżamy i ryzykowne manewry.

(+Lancery i wykorzystanie terenu który znamy a jej Lancer jest pod teren, +omijamy sytuacje w których możemy pomóc, +nienaturalny transport KOSZTEM uszkodzenia Lancery)

Tr +3Or +3:

* V: (podbicie) Lancery robią chory transport.
* X: (podbicie) Lancery uszkadzają arkologię, niszczycie mienie, też prywatne.
* Or: (manifest) Lancery są uszkodzone, ale sprawne, i dotarły do Prometeusa.

Prometeus jest RANNY. Nie tylko uszkodzony psychotronicznie ale też RANNY. (+2Og ostre akcje Ralfa). Ardilla RZUCA na atakujących płatami metalu, wspina się i na kablach. (+2Vr)

* Or: (podbicie) pocisk przeciwpancerny prosto w centralę z BIA. Ralf zasłonił to. Jego Lancer jest roztrzaskany, sam Ralf, ranny, jest na ziemi. Zdążył ewakuować - Ralf bez Lancera jest ranny.
* X: (Ardilla ostatnia płyta, asekuracja Ralfa na bok +1Vg) X: Ardilla musi ratować Ralfa WIĘC atakujący robią masakrę wśród obrońców.
* X: (podbicie) Ardilla musi wyciągnąć Ralfa, ale jej Lancer jest krytyczny. 20% efektywnego działania.
* V (+1Vg - Ardilla włącza Lancer na autodestrukcję + autopilot): Atakujący są odepchnięci (podbicie).
* V: (manifest) szok działań, Lancer i wsparcie. Udało się zadziałać na tyle, że Ardilla rozwaliła elmag blokujące magnesy. I Ralf z Ardillą są uwięzieni w BIA. Ale - BIA jest "bezpieczna" i arkologia przetrwała.

Ardilla i Ralf muszą zostać wyciągnięci. Ale BIA przetrwała.

## Streszczenie

Ardilla utrzymuje Kidirona przy życiu, negocjując dla niego bezpieczne miejsce w Szczurowisku i stawiając swoją i Ralfową reputację na szali. Potem - rozpaczliwie ratuje Prometeusa, acz z ciężkimi stratami. Eustachy w pogoni za niezwykle groźnym Infiltratorem musiał dać mu odejść; oddał Skorpiona i czterech załogantów, ale uratował Kalię Awiter. Tymczasem o Arkologię Nativis toczy się wojna dusz - Bartłomiej Korkoran kontra Laurencjusz Kidiron. A w tle eksperymenty Kidirona (jak np. farighanowie jako Hełmy) wyrywają się spod kontroli i zdecydowanie nie pomagają.

## Progresja

* Ardilla Korkoran: dostęp do kodów i haseł Kidirona dających jej praktycznie władzę dyktatorki i autokraty
* Eustachy Korkoran: dostęp do kodów i haseł Kidirona dających mu praktycznie władzę dyktatora i autokraty

### Frakcji

* .

## Zasługi

* Ardilla Korkoran: utrzymuje Kidirona przy życiu, negocjując dla niego bezpieczne miejsce w Szczurowisku i stawiając swoją i Ralfową reputację na szali. Potem - rozpaczliwie ratuje Prometeusa, acz z ciężkimi stratami.
* Eustachy Korkoran: w pogoni za niezwykle groźnym Infiltratorem musiał dać mu odejść; oddał Skorpiona i czterech załogantów, ale uratował Kalię Awiter.
* Ralf Tapszecz: wykazał się savarańską doktryną, strzelając do buntowników w Szczurowisku i poświęcając nawet swojego Lancera by chronić Prometeusa. Jest cieniem Ardilli, nie da jej zrobić krzywdy.
* Laurencjusz Kidiron: BYŁ WE WŁAŚCIWYM MIEJSCU WE WŁAŚCIWYM CZASIE by ratować Arkologię i ludzie wierzą, że to ON a nie Wujek robi robotę. Czas przejęcia Arkologii nadszedł!
* Bartłomiej Korkoran: przejął część Hełmów i próbuje ochronić Arkologię oraz Prometeusa. Nie ma Lancerów, więc działa czym może. Walczy z młodym Laurencjuszem Kidironem o to kto będzie regentem Arkologii.
* Kalia Awiter: porwana przez Dalmjera, służyła jako przedmiot nie podmiot. Silnie straumatyzowana widząc co Dalmjer robi z tymi co wchodzą mu w szkodę.
* Dalmjer Servart: prawdziwy sadystyczny potwór; zniszczył servar Eustachego pułapką i zranił Kalię, robiąc z niej żywy przykład co może się stać innym którzy stoją przeciw niemu. Zabrał Skorpiona z 4 ludźmi. Nikt nie wróci mimo jego obietnic - ale Kalii pozwolił odejść wolno. Eustachy tyle wynegocjował.
* Rafał Kidiron: nieprzytomny; Ardilla wynegocjowała dlań w Szczurowisku bezpieczne miejsce.
* Małgorzata Maratelus: zajmie się Kidironem, by wyzdrowiał; z opieką medyczną i pomocą Kidiron jakoś się utrzyma aż Zespół jakoś opanuje arkologię.
* OO Infernia: proponuje Eustachemu serię planów które mogą zabić Kalię. Infernia nie dba o Kalię. Chce Eustachego od niej odepchnąć.
* BIA Prometeus: walczy z Malictrix wypalającą mu subsystemy, zostaje ranny w ataku na Centrum Prometeusa. Ale robi co może.

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Neikatis
                1. Dystrykt Glairen
                    1. Arkologia Nativis
                        1. Centrala Prometeusa: normalnie świetnie chronione miejsce. Nie tym razem. Ardilla i Ralf pomogli obrońcom odeprzeć atak na Prometeusa, acz BIA została ranna.
                        1. Stara Arkologia Wschodnia
                            1. Stare Wejście Północne: otwarte kodami Kidirona (i inżynierią) przez Eustachego by Infiltrator mógł się wycofać podstawionym Skorpionem.
                            1. Blokhaus F
                                1. Szczurowisko: tymczasowe miejsce schowania Kidirona, w co absolutnie nikt nie wierzy.

## Czas

* Opóźnienie: 1
* Dni: 1

---
layout: cybermagic-konspekt
title: "Bardzo niebezpieczne składowisko"
threads: nemesis-pieknotki
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [190901 - Polowanie na Pięknotkę](190901-polowanie-na-pieknotke)

### Chronologiczna

* [190901 - Polowanie na Pięknotkę](190901-polowanie-na-pieknotke)

## Budowa sesji

### Stan aktualny

* .

### Pytania

1. .

### Wizja

brak

### Tory

* Wojna gangów (czas): napięcie (2), starcia (5), krew (8), zimna (13), otwarta (20)
* Cierpienie Cezarego (czas): nic (5), wiedzą (8), zemsta (13)

Sceny:

* .

## Punkt zerowy

### Postacie

### Opis sytuacji

* Wolny Uśmiech: [Radość i Optymizm, Wolność i Niezależność, Eksperymenty i Adaptacja]
* Błękitne Niebo: [Ochrona i Pomoc Swoim, Czystość Ludzkości, Adaptacja i Skuteczność]

Wolny Uśmiech - najłatwiej ich sobie wyobrazić jako skrzyżowanie mafii i sekty religijnej. Specjalizują się w hazardzie, rozrywkach, środkach psychoaktywnych, przemycie, badaniach itp. Wspierani są pośrednio przez jedno z państw-miast z przyczyn kulturowych.

Błękitne Niebo - pomyślcie o nich jak o grupie komandosów-partyzantów zintegrowanych z wrogim sobie państwem. Gdy ktoś z "ich ludzi" jest zagrożony, działają. Zamknięta grupa, bardzo skuteczna, paramilitarna. Specjalizują się w szpiegostwie, akcjach bojowych i zakazanych technologiach. Usuwają istoty "nieludzkie".

Mając te dwie grupy wyraźnie widać obszar konfliktu: "przyjemność i ewolucja przyjemności" kontra "czyste człowieczeństwo i ochrona swoich".

**I teraz:**

Cezary kontroluje Składowisko Odpadów Niebezpiecznych. Normalny, uczciwy biznes. Jest chroniony przez Błękitne Niebo jako "swojak". Cezary dostał propozycję z Wolnego Uśmiechu, by dostarczać część tych odpadów do nich zamiast je neutralizować - będzie przedni zarobek. Te odpady służą do tworzenia silnie uzależniających, ekstatycznych narkotyków, które jednak zmieniają ludzi na poziomie biologicznym.

Błękitne Niebo się o tym dowiaduje. "Ich swojak" - może nie powiązany z nimi, ale w ich obszarze - "zdradził". Pomaga Wolnemu Uśmiechowi tworzyć narkotyki które są sprzeczne z ideałami Błękitnego Nieba. Czyli ich człowiek, z ich kultury i ich świata pomaga "obcym". Tak nie może być. Wystosowali "delikatne zapytanie" do Cezarego, że tak nie wolno.

Cezary w panice. Nie może się wycofać z Wolnego Uśmiechu, bo zrobią mu krzywdę. Nie może zostać, bo zrobią mu krzywdę ci z Błękitnego Nieba. Nie może iść do władz, bo obie grupy zrobią mu krzywdę.

Cezary wynajmuje więc wolnych najemników, by uszkodzili Składowisko Odpadów. Ma nadzieję kupić czas i móc wszystkim powiedzieć "nie mogę Wam pomóc" - nikt nie zrobi mu krzywdy. Oczywiście, to się udało.

Wolny Uśmiech zakłada, oczywiście, że Błękitne Niebo uszkodziło Składowisko. To wymaga pokazania siły - nie wolno atakować nie swoich interesów. Błękitne Niebo nie zamierza dać się atakować przez jakichś kultystów z Wolnego Uśmiechu. Mamy więc _Dark Future_ - jeśli Gracze nic nie zrobią, zacznie się wojna gangów (i do tego jedna niewinna grupa najemników zostanie zniszczona przez Uśmiech).

## Misja właściwa

Pięknotka dostała wiadomość z Centrali. Składowisko którym była zainteresowana miało problem. Jedna osoba jest ranna. Pięknotka ma sprawdzić - rutynowa rzecz. Aha, bierze 'rookie'. Wzięła...

Rookie - Gabriel - sprawdza dokumenty. Wszystkie katalizatory, neutralizatory, wszystko. Cezary jest przestraszony. Gabriel robi dokładny obchód i sprawdzenie. Jest taki... SUPER DOKŁADNY SZPERACZ. A Cezary jest przerażony. (Tr:6,3,8=S). Cezary pękł i wszystko wyśpiewał. WSZYSTKO.

Ludzie Grzymościa chcieli pewne środki. On im dawał. Kajrata zabronili. Więc Cezary wynajął najemników by uszkodzili składowisko; nie wiedział, że tam będzie cywil. Gdy Gabriel zapytał czemu nie zgłosił się do terminusów, Cezary powiedział, że terminusi go nie osłonią. Za mało ich jest, on jest noktianinem WIĘC Kajrat go osłoni. Stąd wolał wybrać Błękitne Niebo. Byłoby dobrze, gdyby nie cywil...

Dodatkowo, ludzie Wolnego Uśmiechu zażądali od Cezarego fragmentu składowiska. Dostali ten teren i tam coś robili. Pięknotka zażądała od Pustogoru grupę noobów - mało doświadczonych terminusów. Mają zrobić inwentaryzację. Ci magowie serio nienawidzą teraz Pięknotki, ale cóż... a sama wysłała Cezarego do Pustogoru na przesłuchanie. Spisanie zeznań.

Pięknotka wzięła ze sobą Gabriela i poszła do zakazanego miejsca. Miejsce jest zabezpieczone. Pięknotka wezwała jednego z niedoświadczonych terminusów - niech otworzy wejście. I żąda pełnej procedury. W międzyczasie, Gabriel PRAWIE dostrzegł Amandę Kajrat (Tr:7,3,8=P). Niestety, nie udało mu się; Amanda jest gotowa do działania. A rookie otwiera zabezpieczone drzwi (Tp:6,3,5=KS). W samą porę - to co było w środku eksplodowało. Pięknotce udało się szybko zamknąć drzwi.

I wtedy Gabriel dostrzegł Amandę Kajrat. Stealth suit i w ogóle. Amanda nie wie że jest zauważona; Gabriel nie powiedział głośno a na hipernecie. Pięknotka wzięła jednego z rekrutów, przekształciła go w siebie charakteryzacją. A sama - chce go złapać. Oni mają odwracać uwagę snajpera od tego że Pięknotki tam nie ma. A Pięknotka ma zamiar złapać snajpera. Gabriel z bandą młodych robią coś dziwnego by skupić uwagę snajpera a Pięknotka odpala power suit i leci do snajpera z pełną prędkością by zaskoczyć.

Zaskoczyła. Amanda, przestraszona, spadła na ziemię. Jetpack osłabił upadek ale bolą plecy. Amanda zdjęła hełm i powiedziała Pięknotce, że ta nie musiała tak działać. Pospierały się i poprzekomarzały się ze sobą na oczach nieco zaszokowanych rekrutów. Ale nie miała broni ani intencji - nic na nią nie było. Acz Pięknotka wie jedno - Amanda by z zimną krwią sprowadziła tych rekrutów na śmierć tutaj gdyby nie Pięknotka by eskalować Grzymość x Pustogor.

Amanda wysłała Pięknotce sygnał - nie chce wojny (acz Kajratowi nie zależy), chodzi o jakieś narkotyki zmieniające strukturę ludzi. Pięknotka pozwoliła Amandzie odejść, nic na nią nie ma.

Rekruci coś wykryli, ku zdziwieniu Pięknotki. Znaleźli coś co przetrwało - składnik biologiczny. Po wybuchu poszedł raport. Nielegalna operacja, zaminowane laboratorium, próbki biologiczne. Próbki poszły do Senetis. Stamtąd Pięknotka dostała natychmiastową odpowiedź - to jakieś środki kralotyczne. Czyli nie pochodzą stąd; w tym terenie nie ma kralothów. Cenna wiadomość dla Pięknotki. Ale też udało się wykryć które materiały niebezpieczne zniknęły. Wiadomo, czego szukał Grzymość. Pasują pod jakieś narkotyki.

Pięknotka ma plan. Wywaliła rekruta; reszta jest jej sprawą.

Wpierw skontaktowała się z Mirelą z Kirasjerów. Poprosiła ją o to, by teraz Mirela pomogła. Niech Orbiter pomoże Pięknotce znaleźć i skrzywdzić szlaki przerzutowe Grzymościa. Ten nowy narkotyk jest czymś sprzecznym też z Orbiterem. Mirela powiedziała, że jej szef, Damian, zgadza się Pięknotce pomóc. Skieruje jakąś siłę. Czy Damian może dostać jakąkolwiek formalniejszą prośbę od Pustogoru? Niestety, nie - to by była eskalacja z Grzymościem. Dobrze, Damian zrobi to jako black ops.

Mając potwierdzenie z Orbitera, Pięknotka poszła do Grzymościa.

Znalazła Rolanda Grzymościa w swojej rezydencji, w Zaczęstwie. Pięknotka od razu zaczęła z grubej rury - jak Grzymościowi udało się zantagonizować wszystkich? Kajrata, Pustogor, nawet Orbiter na niego poluje. Niech lepiej Grzymość trochę zdeeskaluje. Jest patriotą, nie stać go na wojnę na wszystkich frontach. Nikogo nie stać. (TrZ+2:12,3,6=P). Grzymość zachowywał się, jakby uwierzył, ale po prostu przeniósł operację do podziemia. Bardziej ją ukrył i przekazał odpowiedniemu oficerowi. Pięknotka jednak wzięła to za dobrą monetę; nikt nie jest przecież tak głupi (a Grzymość, cóż - jest).

Przekonana, że się udało, poszła do Kajrata. I zaczęła skomplikowaną rozmowę. Powiedziała mu o Orbiterze, powiedziała, że Grzymość ma problemy. Że odetnie jego linię. Że była u Grzymościa i że Grzymość się wyraźnie przestraszył i będzie deeskalował. Plus - powiedziała, że mogła nie iść w tą stronę, ale praktykanci i cywile byli zagrożeni. A on jest jeszcze za słaby. (TrZ+4:14,3,6=KSZ). Kajrat zdecydował się zaufać Pięknotce. Zostawi sprawę Orbiterowi, ale będzie monitorował sytuację.

Pięknotka wróciła do domu z poczuciem cholernie dobrej roboty. Zgasiła wojnę zanim ta się zaczęła.

**Sprawdzenie Torów** ():

* Wojna gangów (czas): napięcie (2), starcia (5), krew (8), zimna (13), otwarta (20): 
* Cierpienie Cezarego (czas): nic (5), wiedzą (8), zemsta (13): 

**Epilog**:

* .

## Streszczenie

Na Składowisku Odpadów Niebezpiecznych przy Podwiercie siły Wolnego Uśmiechu zaczęły składać jakieś narkotyki. Nie podoba się to Błękitnemu Niebu. Pięknotka w środku tego wszystkiego - udało jej się poprosić Orbiter o zajęcie się linią dostawczą z Cieniaszczytu, Kajrata by chwilowo zwolnił, Pustogor by chronili właściciela a Grzymościa, by zszedł do podziemia. Polityczny sukces - ale czemu Grzymość idzie w jakieś narkotyki? I czemu Kajrat dąży do wojny z Grzymościem o te narkotyki?

## Progresja

* .

### Frakcji

* .

## Zasługi

* Pięknotka Diakon: politycznie rozmontowała potencjalną wojnę Wolnego Uśmiechu i Błękitnego Nieba o dziwne narkotyki. Ściągnęła do tego Orbiter i deeskalowała sprawę.
* Gabriel Ursus: kuzyn Lilii, terminus-szperacz; idealista. Doskonale zauważa rzeczy (ale Amandy - nie). Dokładny, nieco biurokratyczny i trochę naiwny.
* Cezary Alentik: eks-noktianin; uszkodził swoje Składowisko Odpadów, by nie antagonizować Błękitnego Nieba. Niestety, zranił swojego podwładnego.
* Amanda Kajrat: skłonna do skrzywdzenia grupy terminusów-rekrutów by rozpalić wojnę Pustogor - Wolny Uśmiech. Nie chce wojny. Nie walczyła z Pięknotką, trochę podpowiedziała. Świetnie się chowa w stealth suit.
* Mirela Orion: kontakt Pięknotki z Orbiterem; przekonała szefostwo, że warto odciąć Wolny Uśmiech od kanałów przerzutowych z Cieniaszczytu.
* Roland Grzymość: bardzo skupiony na dziwnych narkotykach; nie uwierzył Pięknotce że Orbiter na to poluje więc ściągnął kanał do podziemi.
* Ernest Kajrat: zdecydował się zaufać Pustogorowi w formie Pięknotki by nie eskalować wojny z Wolnym Uśmiechem. A przynajmniej teraz.

## Plany

* .

### Frakcji

* Wolny Uśmiech: mafia Grzymościa; buduje z materiałami cieniaszczyckimi narkotyki zmieniające biostrukturę ludzi. Deeskalowana wojna z Błękitnym Niebem przez Pięknotkę.
* Błękitne Niebo: mafia Kajrata; broni swojaka i nie pozwala na narkotyki zmieniające biostrukturę ludzi. Deeskalowana wojna z Wolnym Uśmiechem przez Pięknotkę.

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Podwiert, okolice
                                1. Bioskładowisko podziemne: częściowo przejęte przez Wolny Uśmiech; tam zbudowano mini-biolab który wybuchł.

## Czas

* Opóźnienie: 2
* Dni: 2

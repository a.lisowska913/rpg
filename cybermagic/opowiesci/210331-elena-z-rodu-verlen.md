---
layout: cybermagic-konspekt
title: "Elena z rodu Verlen"
threads: legenda-arianny
gm: żółw
players: kić, fox
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [210220 - Miłość w rodzie Verlen](210210-milosc-w-rodzie-verlen)

### Chronologiczna

* [210220 - Miłość w rodzie Verlen](210210-milosc-w-rodzie-verlen)

## Punkt zerowy

### Dark Past

* .

### Opis sytuacji

Jesteśmy na takim etapie, że sytuacja z Eleną powoli grozi zniszczeniem delikatnej tkanki która się odbudowuje między rodami Blakenbauer i Verlen, bo Dariusz Blakenbauer tu jest...

### Postacie

* .

## Punkt zero

* Q: Co Elena zrobiła konkretnie Dariuszowi Blakenbauerowi podczas opuszczania terenu Verlenlandu po tym wszystkim?
* A:
    * Było wesele. Dariusz miał się ożenić, to miało wzmocnić siłę rodu. Elena sprowokowała drugą formę Dariusza i zniszczyła przyjęcie.
    * Elena fabrykowała dowody, ślady, zapewniała świadków, z pomocą Orbitera (w końcu była epicka i ją lubili). 
        * Elena zniechęcała WSZYSTKICH do Blakenbauerów na Orbiterze. Zniszczyło to reputację Blakenbauerów na Orbiterze - ten ród nie ma drogi w kosmos.
    * Q: Czemu brak przeprosin ze strony Eleny / Verlenów doprowadził do problemu na linii Verlen x Blakenbauer?
    * A: Ta laska z którą Dariusz chciał się żenić już nie chciała ;-). A to było małżeństwo polityczne, bardzo istotne dla Blakenbauerów.
* Q: Czemu Dariusz DZISIAJ jest przydatny w Verlenlandzie?
* A: Wspomaga badania w Poniewierzy. Z doskoku, konsultacyjnie - jest w końcu katalistą/artefaktorem/pozyskiwaczem.

## Misja właściwa

Viorika ostrzega Ariannę, że w Poniewierzy znajduje się Dariusz Blakenbauer. Ten Dariusz, który stał przecież za wszystkim co złego spotkało Elenę w przeszłości. Ale teraz Dariusz jest przydatny i relacje Blakenbauer - Verlen są dobre jak nigdy. Arianna poprosiła Viorikę, by ta pojechała do Poniewierzy i upewniła się, że Dariusz nie będzie chciał się mścić na Elenie. A ona (Arianna) zajmie się Eleną. W końcu trzeba jakoś zakończyć tą przeszłość...

Viorika poleciała awianem do Poniewierzy spotkać się z Dariuszem. Trzeba zapewnić, że nie będzie katastrofy...

Viorika spotkała się z Dariuszem. Są w niezłej komitywie, w końcu współpracują od pewnego czasu. Viorika porusza temat Eleny, Dariusz pochmurnieje. To dawna przeszłość i Dariusz nie chce do tego wracać. On osobiście wolałby pokój między rodami Verlen i Blakenbauer; między innymi też na to naciskał Lucjusz zanim opuścił Aurum i przeniósł się do Pustogoru...

Viorika nacisnęła na Dariusza - o co dokładnie chodziło z Eleną? Jak to się kończyło z perspektywy Blakenbauerów? Dariusz nie oponował i nie ukrywał:

* W wyniku tego wszystkiego co robiła Elena na linii reputacyjnej Blakenbauerów, w wyniku tego jak wpływała na ich reputację i działania Blakenbauerowie mieli odciętą drogę do Orbitera.
* Żaden z Blakenbauerów chętnych do działania na orbicie nie miał szans. Wiele osób przez to poszło szukać szczęścia w alternatywnych programach - Eternia, lub mniej szlachetne kierunki (piraci?).
* Dopiero od niedawna jeden z magów Blakenbauerów - Krystian - miał szczęście dotrzeć na orbitę. Jest, o dziwo, na Castigatorze XD. A i tak nie jest mu łatwo przez nazwisko...
* W wyniku działań Eleny zginęło 47 osób. Nie tylko magów, ale też cywili Blakenbauerów. Albo przez brak pomocy albo przez złe plotki, czasem przez samosądy. A to tylko te co Dariusz udokumentował...

Innymi słowy Dariusz ma ogromny żal i kosę z Eleną. Nie ma zamiaru robić jej krzywdy, nie ma zamiaru jej atakować, ale nie wybaczy. Ani on, ani jego ród. Elena jest... cóż. Ale tam jest coś jeszcze - Viorika postanowiła to nacisnąć. Czego Dariusz jej _nie mówi_?

ExZ: 

* O: Dariusz się przyznał. Widząc jakie szkody robi Elena na Orbiterze, Blakenbauerowie zapłacili ogromne sumy by zniszczyć reputację Eleny na orbicie. By znaleźć i wyolbrzymić jej wszystkie porażki. By nigdy nikt nie traktował jej poważnie. A Elena pomagała - bo nikt nie identyfikował Eleny z rodem Verlen, sama wzięła nową desygnatę (Mirokin). Więc Blakenbauerowie robili wszystko, by Elena była śmieszna, sama i zapomniana. I to odblokowało częściowo reputację Blakenbauerów i pomogło Krystianowi wejście na orbitę.

_Fox: "A więc to nie była wina Arianny a Blakenbauerów XD"_

Czyli wszystkie te perypetie Eleny to na serio nie była jej wina, to wszystko Blakenbauerowie i ich pieniądze. Ale nie można powiedzieć, że Elena była niewinna... tu się dużo działo w przeszłości a przecież Elena to jest taka trochę czarna owca rodziny...

Viorika przekazała te informacje Ariannie. Ta musi wiedzieć jak sytuacja wygląda. I powstrzymać Elenę przed kolejną krwawą vendettą przeciwko Blakenbauerom... to się musi skończyć kiedyś.

Arianna zanim pójdzie do Eleny potrzebuje czegoś więcej. Potrzebuje ją zmiękkczyć. Jedyny sposób - idzie do ROMEA. Romeo jest w dość złym stanie w szpitalu, ale chemikalia go trzymają. Romeo jest załamany - nigdy nie będzie miał okazji już porozmawiać z Eleną. Nigdy jej nie powie, że mu na niej zależy.

* R: "Shańbiłem ród i dostałem wyśmiewające maile że nie boją się TAI ale nie są tak głupi jak ja"
* R: "To był ten odcinek z buntem na K1?" (Arianna tłumaczy, że nie z takich rzeczy się wychodzi)
* R: "To znaczy, że ona mnie też kocha? O_O" (Arianna próbuje mu wyjaśnić, że teraz trzeba jej pomóc)

Arianna próbuje przekonać Romeo - niech wyzna Elenie swoją miłość. Niech jej wyjaśni skąd to wszystko się bierze. Skąd ta animozja. Bo ryzykuje, że oni wszyscy stracą Elenę z rodu Verlen na zawsze. A na to Romeo nie chce pójść... poszedł więc w swoim kiepskim stanie do krypty - miejsca, gdzie Elena chyba zdecydowała się zamieszkać XD

TrZ+2:

* X: jeśli Romeo wyzna miłość Elenie i wyjdzie źle, będzie winił Ariannę
* V: podczas rozmowy z Eleną Romeo się wymsknie, że on ją jednak kocha
* V: wymsknęło mu się, że jak śpi to czasem ma koszmary że Elena go bije. To sprawia, że Elena (i Arianna) nie mają jak traktować go wrogo
* X: "there is US! you and me!" Arianna tak powiedziała!
* V: litość i lekka pogarda ze strony Eleny, ale nie nienawiść czy próba pokonania. Romeo po prostu jest. Tym gorszym kuzynem, ale kuzynem...

Arianna poczekała jakąś godzinkę odkąd Romeo opuścił Kryptę, po czym sama poszła odwiedzić Elenę. Elena maskuje strach, coś kontemplowała w rekordach. Schowała to zanim Arianna zobaczyła co Elena oglądała. Arianna nie musi jednak wiedzieć co dokładnie Elena oglądała - znając ją, coś w stylu "wszystkie śmierci jakie spowodowałam" albo "kto tam przeze mnie umarł"... oczywiście, miała rację.

Arianna bezpośrednio skonfrontowała się z Eleną. W sposób jaki nigdy tego nie robiła z dorosłą Eleną - czystą akceptacją i miłością. "Zamknijmy przeszłość, wyrównajmy sytuację, razem".

TrZ+4:

* X: Elena jest święcie przekonana, że zostaje jej podejście "dwie dziewczyny Orbitera kontra cały świat". Arianna dodała do tej grupy jeszcze Viorikę ;-).
* V: Te 47 osób Blakenbauerów które zginęły to dla Eleny kolejny dowód, że niszczy wszystko czego dotknie. Verlenland to jej dom, nie chce robić nikomu krzywdy. Dlatego MUSI WYJECHAĆ!
* V: Dotarło do niej co zrobiła Blakenbauerom. Nie byli winni, nie ci co zginęli. Destrukcja jej reputacji była... zasadna?
* X: Elena nie pozwoli sobie raczej na relacje. Jest niebezpiecznym potworem, wymagającym ogromnej dyscypliny.
* V: Arianna do niej dotarła - dla odmiany nie z Eustachym a z Arianną Elena może naprawić wszystko co się stało. Niech nie ucieka.
    * Elena się rozkleiła. Ale "nigdy nie miała fazy emo" ;-).
    * Elena: "To oni stoją za tym wszystkim czym jestem! Mogłam być Tobą! Arcymagiem, kochanym i szanowanym!!!"
* V: Elena przeprosi Blakenbauerów za to co zrobiła, ale też oczekuje przeprosin (done). To zamknie wojnę Verlen - Blakenbauer.
* V: Przez wzgląd m.in. na Romeo Elena wróci do rodu Verlen...

A Apollo i Milena wezmą piękny ślub ;-).

## Streszczenie

Arianna i Viorika doprowadziły do zamknięcia wojny Blakenbauer - Verlen. Viorika dotarła do Dariusza Blakenbauera i wydobyła co Blakenbauerowie zrobili Elenie (destrukcja reputacji) i co Elena zrobiła im (nie mają miejsca wśród gwiazd). Arianna natomiast wkręciła Romeo w wyznanie Elenie swych uczuć, a potem doprowadziła Elenę do płaczu. Razem przezwyciężą wszystko. Jakoś. Będzie pokój a Elena wróci do rodu Verlen.

## Progresja

* Elena Verlen: W PRZESZŁOŚCI zniszczyła skutecznie reputację Blakenbauerów w Orbiterze. Nie mają wstępu na orbitę.
* Elena Verlen: Blakenbauerowie W PRZESZŁOŚCI zapłacili ogromne sumy by niszczyć reputację Eleny na Orbiterze. To sprawia, że wszystko co złe automatycznie przylepia się do niej.
* Elena Verlen: dotarło do niej co zrobiła. Trauma, bo skrzywdziła ogromną ilość niewinnych Blakenbauerów. Ale nie chce się już mścić... za to potrzebuje WIĘCEJ DYSCYPLINY!

### Frakcji

* Ród Blakenbauer: w PRZESZŁOŚCI ich reputacja na Orbiterze została zniszczona przez Elenę Verlen; nie mieli wstępu do kosmosu...
* Ród Blakenbauer: wielu z tego rodu chętnych do polecenia w kosmos nie miało wyjścia - polecieli albo jako piraci albo przez Eternię. Bo nie mogli przez Orbiter.

## Zasługi

* Arianna Verlen: doprowadziła do tego że Romeo wyznał miłość Elenie by ją zmiękkczyć i wpierw Elenę rozkleiła, sprawiła że ta przeprosiła Blakenbauerów a nawet Elena wróciła do rodu. Mistrzostwo aranżacji sytuacji.
* Viorika Verlen: dogadała się z Dariuszem Blakenbauerem i doprowadziła do tego, że on nie chce wojny z Eleną i sam lobbuje za pokojem w swoim rodzie.
* Elena Verlen: skonfrontowała się ze swoją przeszłością i czynami vs Blakenbauer. Też z miłością Romea do niej. Po dużej ilości łez i działań Arianny, przeprosiła Blakenbauerów (ze wzajemnością) i wróciła do rodu.
* Romeo Verlen: zmanipulowany przez Ariannę, wyznał Elenie miłość i raz na zawsze zamknął z nią tą chorą rywalizację. Teraz Elena nim trochę gardzi i się lituje, nie nienawidzi...
* Dariusz Blakenbauer: pomaga Verlenom w Poniewierzy jako poszukiwacz, artefaktor i katalista; przyznał Viorice że Elena pośrednio zabiła 47 osób w domenie Blakenbauerów a oni zniszczyli jej reputację na Orbiterze.
* Krystian Blakenbauer: służy na Castigatorze, pierwszy oficjalny Blakenbauer w kosmosie po tym co zrobiła Elena.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Aurum
                        1. Verlenland
                            1. Poniewierz
                                1. Zamek Gościnny
                            1. Hold Karaan
                                1. Krypta Plugastwa
                                1. Szpital Centralny

## Czas

* Opóźnienie: 3
* Dni: 3

## Konflikty

* 1 - Viorika postanowiła nacisnąć Dariusza Blakenbauera - czego jej _nie mówi_?
    * ExZ
    * O: to Blakenbauerowie zapłacili ogromną kwotę i zniszczyli reputację Eleny. Plus sporo poleciało do gwiazd innymi metodami (Eternia, piraci...). 
        * 'O' idzie tu na intensyfikację - komentarz MG
* 2 - Arianna próbuje przekonać Romeo - niech wyzna Elenie swoją miłość
    * TrZ+2
    * XVVXV: Elena lituje się nad Romeem i nim lekko pogardza, ale już go nie nienawidzi. O miłości ofc nie ma mowy.
* 3 - Arianna bezpośrednio skonfrontowała się z Eleną. W sposób jaki nigdy tego nie robiła z dorosłą Eleną - czystą akceptacją i miłością. "Zamknijmy przeszłość, wyrównajmy sytuację, razem".
    * TrZ+4
    * XVVXVVV: Elena się rozkleiła; jakkolwiek nadal JA SIAMA, ale przeprosi Blakenbauerów (ze wzajemnością) i wróci do rodu. Ale - sama uważa się za potwora i musi mieć więcej dyscypliny. Ale akceptuje RAZEM.

## Inne

### Projekt sesji

#### Procedure

* Overarching Theme:
    * gracze mają "kierowanie" (Vision & Purpose); wiedzą gdzie iść i po co tam idą
    * gracze są bardziej zaangażowani w historię, bo widzą, gdzie ich decyzje prowadzą
    * MG wie, które sesje wprowadzać i które usuwać
    * MG wie, w którą stronę stawiać adwersariat
* Achronologia:
    * przez przeskakiwanie do przodu nieważnych wydarzeń oczekuję, że każda sesja jest powiązana z kontekstem (gracze widzą jak się to rozwija)
    * przez cofanie się i uzupełnianie luk w przeszłości oczekuję, że brakujące MG i graczom luki uzupełnimy takimi działaniami
    * przez działanie w przeszłości gracze czują silniejszą relację z postaciami i światem, silniejsza budowa wspólnej historii.
* Wyraziste, ważne dla graczy postacie + Relacje (konflikt?) Gracze <-> NPC, NPC <-> NPC
    * graczom bardziej zależy na postaciach i ich powodzeniu
    * więcej potencjalnych wątków i rzeczy które możemy odrzucić przy krzyżykach
* Tory, zmiana postaci z czasem, wpływ graczy na świat
    * gracze widzą jak wpływają na świat. Wpływ jest "ich" - stąd tak tragiczne jest gdy "ich" rzeczy są corrupted / destroyed.

#### Result

**Przeprowadziłem inną sesję, bo Anadia miała tylko 2h, ale na bazie tej**

* Overarching Theme:
    * Theme & Feel: 
        * Batman: Mask of Phantasm. "It is too late". (nope)
        * Mai Hime vs Mikoto (Kagutsuchi vs Mikoto/Miroku). Pain. Hatred. Loneliness.
        * Zemsta czy Powrót? Dariusz czy Verlen?
    * V&P:
        * 
    * adwersariat: 
        * Elena, sama w sobie
        * rodziny żołnierzy
        * 
* Achronologia:
    * -
* Wyraziste, ważne dla graczy postacie + Relacje (konflikt?) Gracze <-> NPC, NPC <-> NPC
    * Elena: 
* Tory, zmiana postaci z czasem, wpływ graczy na świat
    * 
#### Scenes

* Scena 0: 
    * cel: pokazać Wroga Eleny
    * impl: pojedynek
* Scena 1: 
    * cel: 
    * impl: 

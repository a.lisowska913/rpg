---
layout: cybermagic-konspekt
title: "Rexpapier i Włóknin"
threads: nemesis-pieknotki
gm: żółw
players: onyks, mila, kamila_13
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [190906 - Wypadek w Kramamczu](190906-wypadek-w-kramamczu)

### Chronologiczna

* [190906 - Wypadek w Kramamczu](190906-wypadek-w-kramamczu)

## Budowa sesji

### Stan aktualny

* .

### Pytania

1. .

### Wizja

brak

### Tory

* .

Sceny:

* .

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Misja właściwa

Scena Zero:

Sytuacja wyglądała mniej więcej w taki sposób: trzy osoby z Zespołu, szef Jarek w trakcie audytu. Zespół był zmuszony do tego żeby Coś z Tym Zrobić. Audytorzy byli w pomieszczeniu w którym był szef oraz znajdowały się wszystkie potrzebne dokumenty; były tam złe rzeczy. Co zrobił Zespół? Spowodował pożar. Zwarcie w instalacji elektrycznej (w końcu są pracownikami rexpapier). Powodowało to de facto małą katastrofę.

Audytorzy nie chcieli opuścić tego miejsca ale zespół konsekwentnie powiększał poziom pożaru i katastrofy aż w końcu wszyscy zostali ewakuowani z budynku. Zespół wtedy zaczął gasić Pożar gaśnicami i upewnił się, że niefortunne dokumenty zostały poważnie zniszczone podczas samego gaszenia pożaru.

Sesja właściwa:

W wyniku interesującego zbiegu okoliczności Kasjopea i Ksenia zostały same na miejscu zbrodni we Włókninie. Jak powszechnie wiadomo, Kseni nie wolno nigdy dowodzić operacją, ale tym razem byłam jedyną terminuską dowodzącą. Kasjopeja natomiast chciała uratować zakład - Włóknien w Kramamczy. Powód jest bardzo prosty – te zakłady zrobiły jej ulubioną sukienkę. Nie mogą tak po prostu upaść.

Tak więc Kasjopeja wezwała do pomocy zespół – postaci graczy. Trójkę Detektywów którzy czasami współpracują z Kasjopeją w tematach jej potrzebnych, ale nie są z Kasjopeą bezpośrednie powiązani.

Zespół zapoznał się ze sytuacją a Kasjopeja przekonała Ksenię, żeby ta dała zespołowi szansę zanim wyjdzie z trotylem i napalmem do sprawy. Okej. Zespół poszedł wpierw do byłego pracownika, tego który dokonał aktu sabotażu. Nie chciał z nimi rozmawiać, ale zespół przyniósł ze soba mgłowino. Wino otwiera większość ust, w wyniku czego zespół dowiedział się, że naprawdę wynajął owego byłego pracownika Timor Kardasz. A jak już wcześniej ustalono w dokumentach i papierach, Timor ma powiązanie z Jarkiem – szefem Rexpapier. No i rexpapier próbował wykupić Włóknin już wiele razy. 

No dobrze. Czas odwiedzić samego Jarka. Na przeszkodzie stanęła im sekretarka, Eleonora. Nie byli w stanie się przez nią przebić bo ona powiedziała wyraźnie – szef ma audyt, nie ma zamiaru z nikim rozmawiać. W tym momencie zespół nacisnął na Eleonora – sam audyt wyglądałby gorzej gdyby oni (powiązani z prasą) zrobili niezłą aferę. Gdyby była afera wskazująca na to, że Rexpapier chcą zniszczyć Włóknin, audyt mógłby wyjść niekorzystnie dla Jarka. Eleonora się chwilę zastanowiła i przyznała się – tak. To ona jest winna. To ona stała za tymi wszystkimi ruchami, szef jest dla niej dobry, nie chce go utopić. Zespół był lekko zdziwiony całą tą sprawą ale przekazał temat Kasjopei.

W nocy Kasjopeja zadzwoniła do członków zespołu i powiedziała im, że Jarek się przyznał – Jarek twierdził że tak naprawdę to on jest winny a Eleonora jedynie wzięła ogień na siebie. Kasjopeja, jak to ona, umówiłam na następny dzień zespół na spotkanie i z Jarkiem i z Eleonorą. Tymczasem zajmuje się neutralizacją Kseni - niech terminuska teraz nie wchodzi do akcji.

No dobrze, nasz zespół poszedł spotkać się z Jarkiem i Eleonorą. Na spotkaniu - Jarek cały zdenerwowany, Eleonora opieprza go jak zdechłego psa, stanęło na tym że powiedzieli prawdziwą historię. Jarek dostał bardzo duże wymogi od swoich przełożonych. Żeby te wymogi móc zrealizować – dziewięćdziesiąt procent sprzedaży w danym sektorze w którym tak samo występuje Włóknin. Nie ma więc wyboru – musi po prostu zniszczyć bądź kupić tą małą organizację. 

Zespół zauważył, że jest to nieco dziwne. To nie wygląda jak decyzja biznesowa, zwłaszcza, że się nie do końca opłaca. To wygląda jakby ktoś wyżej, nad Jarkiem, miał coś do włóknina. Zaczęli więc wspierać się w różnego rodzaju dokumentach wspomagani przez uprawnienia Kasjopei. I okazało się, że faktycznie coś jest nie tak. Szefowa Jarka, Anna, jest córką człowieka który wraz z ojcem właściciela włóknina (Dariusza) założył włóknin. Ale ci dwaj wspólnicy się pokłócili z biegiem czasu – ojciec Anny skupił się bardziej na rodzinie. Ojciec Dariusza skupił się na biznesie.

I w ten sposób ich wielka przyjaźń nie przetrwała, ojciec Anny wyleciał z firmy co odchorował i portfel ucierpiał. Po wszystkim, zmarł. Najprawdopodobniej Anna chce się zemścić na Dariuszu za to co spotkało jej ojca.

Mając tą informację, zespół mniej więcej wiedział co zrobić.

Wpierw udali się do przełożonych Ksenii i wybłagali od nich by wycofali tą terminuskę z akcji. Oni mają rozwiązanie tego problemu, wiedzą jak mogą sobie poradzić i jak mogą ogólnie pomóc, ale Ksenia będzie stanowiła tą przeszkodę. Przełożeni Kseni świadomi tego, że ona nie powinna dowodzić tego typu operacja zgodzili się z tym – ale zespół ma odpowiedzialność żeby to się w pełni udało. Następnym krokiem było poproszenie Kasjopei o to, żeby zorganizować coś w stylu reality show. Takie coś gdzie Anna i Dariusz trafią i będą musieli sobie pomagać nie wiedząc o tym kto tak naprawdę jest kim.

W ten sposób Anna zapozna się z osobą którą chciała zniszczyć a Dariusz być może Annę polubi i parę razy jej pomoże. Dzięki temu to co mogło być katastrofą może nawet przerodzić się w całkiem atrakcyjny romans. Kasjopea z przyjemnością się zgodziła. Dariusz został zapisany do tego reality show jako warunek dalszej pomocy Kasjopei – więc się zgodził, a zespół poprosił bezpośredniego przełożonego Anny, kawalarza, by ten zgłosił Annę do tego programu i powiązał to z jej celami kwartalnymi.

W ten sposób udało się wszystkim osiągnąć sukces i pojawia się szansa że nie tylko Włóknin zostanie uratowany ale też Rexpapier i Włóknin długoterminową będą mogły koegzystować. Jako dodatkowy bonus, Anna i Dariusz może się zaprzyjaźnią i rdzeń problemu zostanie zniszczony – pozbędą się problemu ze starymi grzechami ojców.

**Sprawdzenie Torów** ():

* .

**Epilog**:

* .

## Streszczenie

Anna z Rexpapier próbuje zniszczyć Włóknin za grzechy ojców. To jest praprzyczyna którą wykryli detektywi Kasjopei, idąc po nitce do kłębka. Jednocześnie szef Rexpapier, Jarek, który był bezpośrednim egzekutorem jest człowiekiem dobrym - jego sekretarka weźmie na siebie winę by tylko jemu nic się nie stało. Odpowiedź Kasjopei? Reality show, które Annę i Dariusza zbliży by zniszczyć tą upiorną wendettę.

## Progresja

* .

### Frakcji

* .

## Zasługi

* Ksenia Kirallen: nieubłagana terminuska skłonna do zniszczenia Włóknina, Rexpapier, czegokolwiek. Wycofana przez Centralę, bo tu wystarczy subtelność.
* Dariusz Kuromin: w pełni współpracuje. Przestraszony, chętny by ten koszmar się jak najszybciej skończył. Jego ojciec skrzywdził ojca Anny.
* Kasjopea Maus: ściągnęła detektywów by uratować Włóknin; poprosiła ją Pięknotka a jej ulubiona sukienka jest stąd. Będzie reżyserować reality show by pogodzić Annę i Dariusza.
* Jarek Gułanczak: szef lokalnego oddziału Rexpapier; próbuje zniszczyć Włóknin, bo Anna tak nań naciska. Nie pozwolił skrzywdzić Eleonory. Znalazł rozwiązanie.
* Eleonora Rdeść: nieludzko lojalna wobec Jarka; uratował ją kiedyś. Jest skłonna wziąć całą winę na siebie by tylko jemu nie stała się krzywda.
* Mateusz Urszank: były pracownik Włóknina; zrobił sabotaż. Pod wpływem mgłowina powiedział WSZYSTKO - zrobił to by dołączyć do Rexpapier i Timor mu kazał.
* Anna Warlank: szefowa w Rexpapier ponad Dariuszem; mści się na Włókninie, bo ojciec Dariusza doprowadził do szybszej śmierci ojca Anny gdy pracowali nad Włókninem. Mastermind.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Kramamcz
                            1. Podwiert

## Czas

* Opóźnienie: 2
* Dni: 4

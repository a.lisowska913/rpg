---
layout: cybermagic-konspekt
title: "Nie da się odrzucić mocy"
threads: nemesis-pieknotki
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [190828 - Migświatło psychotroniczek](190828-migswiatlo-psychotroniczek)

### Chronologiczna

* [190828 - Migświatło psychotroniczek](190828-migswiatlo-psychotroniczek)

## Projektowanie sesji

### Pytania sesji

* .

### Struktura sesji: Frakcje

* Kornel: przekonać Minerwę, by pojechała z nim
* Kasjopea: zobaczyć koszmar Minerwy. Lub Karoliny.
* Karolina: odrzucić moc magiczną

### Hasła i uruchomienie

* Ż: (kontekst otoczenia) -
* Ż: (element niepasujący) -
* Ż: (przeszłość, kontekst) -

### Dark Future

1. Karolina uzyskuje bezpośredni kontakt z Saitaerem
2. Kornel przekonuje Minerwę, by ta jednak z nim pojechała
3. Kasjopea zobaczyła majestat Saitaera

## Potencjalne pytania historyczne

* brak

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Misja właściwa

**Scena**: (20:53)

_Zaczęstwo, Cyberszkoła_

Pięknotka, Tymon i Minerwa są w Cyberszkole. Potrzebna była pełna siła ognia. Budynek świeci się jakąś chorą energią. Energia rezonuje z bagnem. Cyberszkoła porasta dziwnymi roślinami i mechanizmami. O dziwo, nie wygląda to na energię Saitaera. Coś na kształt węża przemknęło po terminalach.

Cyberszkoła sprzęgła się z bagnem. Gdzieś tam w środku Cyberszkoły jest rdzeń tego wszystkiego. Jeśli pozbędziesz się rdzenia z Cyberszkoły, to zniknie źródło Skażenia.

* "Ixion. To jest fauna i flora Ixiońska." - Minerwa, szeptem

Pięknotka wie, gdzie jest rdzeń. Musi przedostać się do jednego z pomieszczeń. Zdaniem Tymona ten rdzeń jest bardzo wyraźny energetycznie. Jest to rdzeń o dużej mocy, ale się wyczerpuje. Tymon wyczuwa też echo Magii Krwi z okolic rdzenia.

Pięknotka decyduje się pójść w rozwiązanie militarne. Bierze Minerwę między siebie i Tymona, po czym uruchamiają power suity. Trzeba przebić się przez Ixiońską florę i faunę. (nikt nie jest ranny, przebiliście się do rdzenia, oczyściliście drogę). Wtem, na drodze Zespołu pojawiło się... sześć pająkokształtnych robotów bojowych. Uzbrojone. Efemerydy? Nie wyglądają na ixiońskie byty. Pięknotka i Tymon od razu otworzyli ogień, wspierani magią ognia. (TrM:SSM).

Pięknotka wystrzeliła czyste, apokaliptyczne inferno topiąc połowę Cyberszkoły w kierunku na rdzeń. Tymon jest zaskoczony. Minerwa krzyknęła z bólu. Energia ją też zraniła. Nie odróżniła jej od istot magicznych. Minerwa jest Ranna. Pięknotka i Tymon dotarli w okolice rdzenia. W środku wirujących mechanizmów i wirów znajduje się Karolina Erenit. Wykorzystała przeciwko sobie rytuał Magii Krwi i Coś Się Stało.

Minerwa twierdzi, że trzeba dostać się do Karoliny i przerwać rytuał. Pięknotka decyduje się użyć innego żywiołu niż ogień. Minerwa zbladła, gdy Pięknotka zaczęła kombinować z magią. Pięknotka stawia ścianę tak, by otworzyć przejście do Karoliny magią. (TrM=MS). Zaklęcie Pięknotki eksplodowało odłamkami Ziemi tnąc i siekając żywiołowo wszystko dookoła, wirem elementu Ziemi. Tymon osłonił Minerwę power suitem. Czarodziejka krzyknęła ze strachu.

Karolina jest otwarta (jest do niej dojście). Nie jest nadal przytomna. Ciągle coś szepcze "nie zapomnę Was, nie zapomnicie mnie. Nie zostawię Was, nie zostawicie mnie". I tak w kółko. Pięknotka podchodzi do niej ostrożnie - przed Pięknotką materializują się duchy przyjaciół Karoliny. Pięknotka atakuje szybko - przerwać rytuał silnym uderzeniem by ogłuszyć Karolinę (Tp:9,3,3=S). Karolina padła na ziemię nieprzytomna. Efekt magii krwi zaniknął.

Tymon zaczął siłować się z energiami magicznymi. Niech energia nie pójdzie na Trzęsawisko. Niech się rozproszy. Niech Cyberszkoła się nie zmieni. (7,3,6=SS). Udało mu się, ale wola Karoliny wpisała się w Cyberszkołę. To o nie zostawieniu i nie zapomnieniu. Dotknęło Pryzmatu Cyberszkoły.

Pięknotka usłyszała świst miecza. Biały Rycerz uratował Tymona przed złapaniem przez ixiońską mackę. Po chwili, Biały Rycerz odszedł. Pięknotka go nie zatrzymywała - nie jest pewna czy ma lub powinna...

**Scena**: (21:28)

_Zaczęstwo, Kwatera Terminusa_

Tymon przebadał Karolinę. Czarodziejka z trudem otworzyła oczy. I - o dziwo - lekko zwycięski uśmiech. Powiedziała, że pozbawiła się mocy magicznej. Nie jest już czarodziejką. Pięknotka powiedziała, że chyba Karolina się myli. I faktycznie... Pięknotka zauważyła, że czary Karoliny są bardzo potężne. Potężniejsze niż powinny być.

Karolina powiedziała, że chciała sobie odebrać moc magiczną, bo nie chce by zapomniała o swoich przyjaciołach i by oni zapomnieli o niej. Pięknotka powiedziała, że Karolina nie musi o nich zapominać. To nie ten czas i nie ten okres - kiedyś takie rzeczy się robiło, ale teraz jest inaczej.

Tymon przyszedł z opatrzoną Minerwą. Ostro dostała. Moc Pięknotki obróciła się przeciw ixiońskim bytom a Minerwa trochę jest ixiońska.

Tymon i Minerwa połączyli siły by zbadać Karolinę magicznie. (TrM+2:S). Doszli do tego co się stało. Karolina jest portalem na Ixion. Przez nią da się dostać do Martwego Świata. Tymon powiedział, że Karolina jest bardzo niebezpieczna - musi zgłosić to Karli. Pięknotka zaoponowała - może wystarczy się z nią zaprzyjaźnić. Karolina ma objawy nadużycia mocy, ale poza tym czuje się dość dobrze. Pięknotka dała jej swój numer telefonu. Niech Karolina dzwoni jak co.

Pukanie do drzwi. Tymon, zdziwiony, poszedł otworzyć.

KASJOPEA.

Kasjopea zainteresowała się Karoliną; Minerwa jednak przechwyciła ją na siebie. Chyba Kasjopea chciała mieć dostęp do Kryształu Mausów? Kasjopea rzuciła się na tą okazję a Pięknotka wymknęła się z Karoliną. Czas jechać do Szkoły Magów.

Pięknotka próbuje ocieplić Karolinę do siebie. Niech - jeśli coś się dzieje - Karolina zadzwoni do Pięknotki. Mniej tego "ja siama". (Tp:6,3,4=SS). Udało się, ale Karolina będzie rozmawiać TYLKO z Pięknotką. Nie akceptuje jako osoby z którą może współpracować nikogo innego.

_Zaczęstwo, Szkoła Magów_

Po tym jak Pięknotka grzecznie odstawiła Karolinę, poszła do dyrektora. Arnulf przywitał ją w szlafroku. Pięknotka powiedziała, że Karolina potrzebuje przyjaciela. Arnulf powiedział, że wszystkich odpycha od siebie. Jest skłonna do ataku. Jednocześnie, Karolina ma ogromną moc magiczną. Naturalny talent.

Jest osoba, która by się nadawała jako przyjaciółka dla Karoliny. To Marlena Maja Leszczyńska, czyli Wiewiórka. Ale jak ją tu ściągnąć? Może transfer...

Pięknotka zdecydowała się skomunikować z Marleną osobiście. Ale potem, jak nadejdzie dzień.

_Zaczęstwo, Baza Terminusa, następny dzień - przez Barbakan_

Marlena dostała informację od Pięknotki o tym, jak wygląda sprawa z Karoliną. Sama, była wykorzystana, sporo nieufności - i podziwia Marlenę. Wiewiórka chwilę się zastanowiła. Jak może pomóc Pięknotce? Gdy Pięknotka powiedziała, że potrzebna jest osoba na miejscu, Marlena powiedziała, że wymiana studencka na pewien czas jest możliwa. Ale Marlena spróbuje docelowo zaprzyjaźnić Karolinę z Kiryłem - to kochany człowiek (mag) i może być na miejscu.

Niech tak będzie.

Wpływ:

* Ż: 13
* K: 3

**Sprawdzenie Torów** (22:17):

2 Wpływu -> 50% +1 do toru. Tory:

* Minerwa pojedzie z Kornelem: 2 = V
* Kornel opuści Pustogor: 0
* Karolina nawiąże kontakt z Saitaerem: 2 = X
* Kasjopea dostanie Kryształ Mausów: 8 = XVXX

**Epilog** (22:20)

* Minerwa nawiązała kontakt z Kornelem. A raczej, Kornel z Minerwą.
* Kasjopea dostała kilka pierwszych Kryształów, ale nie ma tego, czego oczekiwała od Minerwy. Jeszcze.
* Karolina będzie komunikować się z Pięknotką w warunkach specjalnych. TYLKO z nią (i Marleną).
* Marlena przybędzie pomóc Karolinie
* Cyberszkoła i Karolina są sprzężone - Cyberszkoła uniemożliwi Karolinie zapomnienie o przyjaciołach i vice versa.

### Wpływ na świat

| Kto           | Wolnych | Sumarycznie |
|---------------|---------|-------------|
|               |         |             |

Czyli:

* (K): 

## Streszczenie


## Progresja

* Pięknotka Diakon: 

### Frakcji

* .

## Zasługi

* Pięknotka Diakon: 
* Minerwa Metalia: 
* Karolina Erenit: 
* Kornel Garn: 
* Tymon Grubosz: 
* Kasjopea Maus: 
* Arnulf Poważny: 
* Marlena Maja Leszczyńska: 

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Zaczęstwo
                                1. Akademia Magii, kampus
                                    1. Domek dyrektora
                                1. Cyberszkoła
                                1. Kwatera Terminusa

## Czas

* Opóźnienie: 7
* Dni: 3

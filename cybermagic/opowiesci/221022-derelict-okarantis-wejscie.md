---
layout: cybermagic-konspekt
title: "Derelict Okarantis: wejście"
threads: historia_darii, salvagerzy_anomalii_kolapsu
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [221022 - Derelict Okarantis: wejście ](221022-derelict-okarantis-wejscie)

### Chronologiczna

* [220921 - Kapitan Verlen i Królowa Kosmicznej Chwały](220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly)

## Plan sesji
### Theme & Vision

* Praca salvagera jest cholernie niebezpieczna i wredna
    * Nie dość że jednostki nie do końca do siebie pasują i są pouszkadzane to jeszcze problemy i anomalie

### Co się wydarzyło KIEDYŚ

* Okarantis była lekkim transportowcem "nie stąd". Na pokładzie Okarantis kultyści stanęli przeciwko reszcie.
    * Pilot, widząc że nie dają rady, poszukał najdalszej fali i Okarantis zniknęła.

### Co się wydarzyło TERAZ (what happened LATELY / NOW)

.

### Co się stanie (what will happen)

* S00: Nonarion Nadziei, krupier Leo. Daria ma sprawdzić czy "to" się do czegoś nada
* S01: approach -> Okarantis. Przygotowanie narzędzi i dron. Daria wprowadza drony. Jednostka ma silne pole magiczne; mocno napromieniowana. Nie ruszycie jej, ale można ją rozszabrować.

### Fiszki

#### Nonarion Nadziei (stacja cywilna)

* Leo Kasztop: sprzedawca sekretów na Poezji Obłoków 
    * ENCAO:  0-+00 |Intrygancki, polityka;;Nie odracza| VALS: Self-direction >> Stimulation, Tradition| DRIVE: Wygrać w rywalizacji)

#### Hiyori (salvager)

* .Daria jako salvager (inżynier)
* Ogden Barbatov: 44 lata, kapitan + marine + salvager (atarienin)
    * ENCAO:  +-00- |Osoba starej daty;;Jeżeli czegoś chce, weźmie| VALS: Achievement, Power >> Conformity| DRIVE: Uwolnić niewolników
* Nastia Barbatov: 43 lata, pilot + marine + salvager (atarienka)
    * ENCAO:  +0--- |Anarchistyczna, nie ufa władzy;;Lubi żartować| VALS: Universalism, Stimulation >> Face| DRIVE: Utopia Star Trek
* Kaspian Certisarius: 37 lat, advancer, noktianin (adastranin); próbuje być daleko od wszystkich
    * ENCAO:  -+-00 |Skryty;;Bardzo Ostrożny;;Nie widzi nadziei| VALS: Tradition, Family, Hedonism| DRIVE: Corrupted contagion (coś na K1)
* Jakub Uprzężnik: 29 lat, advancer-in-training (atarienin)
    * ENCAO:  0+0-0 |Zdradliwy;;Podejrzliwy| VALS: Stimulation, Security >> Achievement| DRIVE: Rana ego
* Iga Mikikot: 29 lat, medyk + PR, (altinianka); próbuje wszystkich nawrócić
    * ENCAO:  0--+- |Dogmatyczna (Seillia), potępia;;Przyjacielska| VALS: Hedonism, Face >> Power, Humility| DRIVE: Femme Fatale
* Patryk Lapszyn: 28 lat, puryfikator + scientist; nie mag (sybrianin); próbuje wejść Idze lub Darii do majtek
    * ENCAO:  00-+- |Tendencje do podkradania;;Pomaga innym| VALS: Self-direction, Stimulation >> Security| DRIVE: Wolność od innych
* .Adam + Ewa d'Hiyori: androidy wsparcia pod kontrolą Safiry i z opcją inkarnacji Safiry.

#### Złota Salamandra (salvager)

* Dominika Salamandra: 39 lat, kapitan + marine + salvager (atarienka)
    * ENCAO:  0---+ |Okrutna;;Nie ma żadnych granic osobistych| VALS: Face, Power, Self-direction| DRIVE: Zemsta A. Beaumont (noktianie)
* Bogumił Urubos: 44 lata, pierwszy oficer (atarienin)
    * ENCAO:  0+-00 |Krytyczny, szuka dziury w całym| VALS: Self-direction, Benevolence >>Tradition| DRIVE: Hipergamia

#### Okarantis (korweta bojowa)

* mnóstwo zwłok i trupów
* environmental: radiation, instability, anomalia 'stopiony kadłub', sporo ciał, wysokie pole magiczne
* inne: ślady przeszłego kralotycznego kultu, efemeryda 'kablowęże', efemeryda 'pętla pamięci', efemeryda 'blood altar'
* inne: ślady kilku noktian, zmarłych w skafandrach, zabrakło im wszystkiego (WRÓCĄ przez efemerydy)

### Sukces graczy (when you win)

* .

## Sesja właściwa
### Przed sesją - kontekst

Zasoby: FINANSE, SPECJALISTYCZNE, METAL, ENERGIA, WODA, ZYSK

Daria jest "świeżo po szkole". Jest przećwiczona, ma swój implant i zna się na rzeczy. Ale brakuje jej super poważnego doświadczenia komercyjnego. Z uwagi na brak szczególnych znajomości i niechęć do działania przy K1 cały czas, dostała się na niezłą jednostkę salvagującą - Hiyori. Jest to solidny salvager, na 10 osób. Wysoka automatyzacja. Daria jest głównym inżynierem. Niestety. Hiyori jest dobrą jednostką, ale ma taką średnią reputację - nie płacą dużo bo dużo nie mają. Ale para dowodząca - Ogden i Nastia - są ludźmi uczciwymi i znającymi się na rzeczy.

To nie jest pierwsza operacja Darii. Daria ma na koncie już 3-4 derelicty. Jeden z nich udało się wydobyć z Anomalii i doprowadzić do Stacji Nonarion Nadziei. Dwie jednostki udało się rozparcelować i zdobyć z nich wartościowe rzeczy. Daria ma do dyspozycji różne drony tnące, mechanizmy przetwórcze itp. Jak każdy salvager - a Hiyori jest zadbana. Nie jest bogata ale jest zadbana i Barbatovowie nie oszczędzają na statku.

Specyfiką Hiyori jest to, że Barbatovowie wierzą w wysoką automatyzację. Wolą mniej członków załogi a więcej dobrego sprzętu. Tajemnicą poliszynela jest to, że dużo może zrobić dla nich ADAM - bioteksynt Hiyori. Nikt do końca nie wie w jakim stopniu Adam jest świadomy a w jakim jest tylko maszyną. Tak czy inaczej, Barbatovowie traktują go z szacunkiem.

Na Hiyori mamy następującą ekipę: Daria jako główny inżynier, Ogden i Nastia Barbatovowie, wiecznie ponury advancer Kaspian i przesympatyczna ale nieprawdopodobnie irytująca Iga, medyk i zażarła wyznawczyni Seilii. KAŻDY wie, że Iga jest wyznawczynią Seilii. Jak z wegetarianami uprawiającymi crossfit.

Darii to nie przeszkadza. Załoga jest sensowna, Daria ma dwa automatyczne roboty bojowe, ma sporo dronów i jest wystarczająco kompetentna. A Barbatovowie nie rzucają się na najbardziej niebezpieczne derelicty.

### Scena Zero - impl

"Nonarion Nadziei" to nie jest idealna stacja. To typowa stacja salvagerska - wymiana plotek, sporo napraw, doki; to krąży niedaleko Anomalii Kolapsu i jest dość mobilne, by uciekać potworom. Oprócz tego ma dobre bary i możliwość kupienia sobie na stałe pryczy w subskrypcji. Daria subskrypcji na pryczę nie ma - z natury śpi na Hiyori. Na "Nonarionie" można kupić sporo rzeczy kompatybilnych z pracą salvagera. Część z Orbitera, część z Anomalii, część z wraków noktiańskich a część chałupniczych. Orbiter się im nie wpieprza - nie do końca ma na to siły.

Daria towarzyszy Ogdenowi Barbatovowi do Leo - kolesia, który sprzedaje lokalizacje perspektywicznych wraków. Ogden i Leo się kłócą o cenę i o konkretną INSTANCJĘ wraku którego pozycję można kupić. W pewnym momencie Ogden ruszył Darię "panno Dario, czy ta struktura wygląda dla Ciebie interesująco? Jesteśmy w stanie to wykorzystać?" i podsunął Darii wykresy i pierwsza badania prospectorów.

Jednostka wygląda na nie z tego sektora. Daria już widzi, że nijak nie jest dopasowana do innych jednostek. Kształt i tonaż wskazuje na jednostkę cywilną a nie wojskową - mniej zabezpieczeń, mniej problemów. Jednocześnie więcej problemów z dobrą sprzedażą rzeczy. Co najmniej 20 interesujących sektorów, nie wiadomo w jakim stanie. Wyraźnie widać wypaczenia spowodowane długotrwałym potężnym polem magicznym - to też redukuje wartość. Odpromienniki są zniszczone. Ale zewnętrzna struktura wygląda w miarę solidnie.

Daria szuka z dokumentów, papierów, wykresów itp. rzeczy potencjalnie niebezpiecznych.

Tr Z (dokumentacja jest szeroka) +2:

* Vz: dokumentacja jest KOMPLETNA. To nie jest pułapka. To jednostka, do której ciężko się dostać, niewygodnie. Ale w miarę bezpiecznie. To sprawia, że jednostka nie ma szans być rozchwytywana. Co więcej, prospektor najpewniej nie wszedł na pokład (takie sprzedasz drożej). Jednostka była w promieniu silnych anomalii magicznych, ale anomalia się przesunęła (normalne). Więc część rzeczy będzie bardziej zniszczona. Wartość tej jednostki nie jest szczególnie duża.
* Vr: ta jednostka wygląda na to, że faktycznie się zgubiła w warp. Nie wygląda też na bardzo starą. Na pewno nikt nie przeżył - nie ma problemów "czyj to statek" i wygląda na jednostkę transportową. Mało prawdopodobne, by się nie zwróciła. Albo się sprzeda informacje z banku danych na K1, salvaguje cargo itp. Mała szansa, by jednostka się nie zwróciła. Komponenty będą niekompatybilne ze sprzętem astoriańskim, ale może się uda zdobyć jakiś neotech.
* (+XXXVg) Daria próbuje przekazać tak wszystko Ogdenowi, by obniżyć cenę. X: mimo wszystko, Leo WIE że ta jednostka jest mało wartościowa ale jest w miarę bezpieczna. Więc spoko. Nie udało się obniżyć ceny, ale Ogden swoje ugrał.

Ogden nie ucieszył się, że potrzebne jest 6 dni by dotrzeć bezpiecznie do tajemniczej jednostki. Jednostka zakodowana jest jako "Okarantis" - tak się zwie (wymalowane itp).

Czyli przed nimi najpewniej 3tygodniowa / miesięczna wyprawa. (FINANSE-10)

### Sesja Właściwa - impl

Podróż do Okarantis była stosunkowo bezproblemowa. Barbatovowie dobrze prowadzą jednostkę, Nastia zna się na rzeczy a jako, że Hiyori to mały salvager ALE SALVAGER to jest sporo miejsca na pokładzie. Można zejść sobie z drogi jak trzeba. Każdy z członków załogi po swojemu radzi sobie z podróżą; W obowiązkach Darii jest monitorowanie statku. Miała jakieś małe drobne rzeczy do naprawienia; normalne. Mieli też jedną sprawę z efemerydą na pokładzie; nic z czym generatory memoriam i broń ręczna sobie nie poradzą.

Tak czy inaczej - Hiyori zbliżyła się do Okarantis. Okarantis jest "zimna". Barbatovowie wysłali drony do przeanalizowania kadłuba, czy parametry się zgadzają - czy jest atmosfera itp. Czy ta jednostka jest taka jak sprzedano. A wszystko jest nagrywane bo potencjalna reklamacja.

TrZ (drony, analiza, skan) +3:

* X: jedna z dron wpadła w jakąś falę anomalną czy coś - lub coś poszło nie tak przy badaniu - i się uszkodziła o kadłub. Albo ją tam zostawimy albo będzie wymagała naprawy.
* V: wszystko dookoła Okarantis jest podobne do tego co było w prospekcie. Jest to wiarygodne. Drift wynika ze standardowego driftu w anomalii kolapsu.

* Ogden -> Kaspian: "panie Kaspianie, czy ta drona nadaje się do odzyskania?"
* Kaspian: "Nie powinienem mieć problemów, choć muszę uważać; skafander jest może wzmocniony, ale nie jest pancerzem"
* Ogden: "Proszę spróbować, panie Kaspianie. Ale jeśli cokolwiek pójdzie nie tak, abort."

Kaspian próbuje odzyskać dronę, po przejściu przez spacer kosmiczny.

TrZ+3:

* X: Drona jest tak jakby... przyciągana w kierunku silników warp. Kaspian musiał nadłożyć drogi; zajęło to więcej (dla bezpieczeństwa)
* V: Kaspian zamontował 'przechwytywacz' na dronie. Trzymając się liny, wraca z droną do Hiyori.

Patryk i Daria siedli do drony - co się z tym cholerstwem stało, jak to naprawić, czy w ogóle warto.

TrZ (sprzęt na Hiyori jest dobry) +3:

* X: Patryk nie ma pojęcia co DOKŁADNIE się stało, ale drona jest napromieniowana. Trzeba spuryfikować. Patryk użyje gaśnicy.
* V: Patryk może potwierdzić - drona jest uszkodzona mechanicznie, ale to się da naprawić. Spuryfikowana. Jest co najmniej jedna silna anomalia w okolicach silników warp. Patryk podejrzewa SPORO anomalii na pokładzie statku - wynika z efektu na dronę.
* Vz: Daria doprowadzi dronę do działania bez większych problemów. Mają dość sprzętu itp.

Daria dostała zadanie na podstawie skanu dron POTWIERDZIĆ czy słabe punkty statku są dalej słabymi punktami. Trzeba się tam wbić i wprowadzić "śluzę ratunkową" - wywalić fragment kadłuba a w to miejsce specjalną strukturę śluzy dla Hiyori. Dzięki temu jest bezpieczne wejście do statku kontrolowane przez Hiyori i będzie dało się wpuścić drony.

TrZ (dokumenty PLUS skany dron) +3:

* Xz: statek jest zanomalizowany, doszło do przemieszczeń strukturalnych. Nie da się jednoznacznie tego potwierdzić
* X: ...co udowodniła pierwsza próba. Pierwsza "śluza intruzywna" się nie udała i tylko zmarnowała czas i surowce. (SPECJALISTYCZNE-1)
* Daria rekomenduje spróbowanie koło cargo (+1Vg+3Og): Og: śluza intruzywna się udała, ale po drugiej stronie w cargo część transportu została zniszczona. Z 6 cargo holdów ten jeden jest poważnie uszkodzony. A co tam było? Części do jakichś ścigaczy.

Patryk "Daria ale czemuuu" Iga "dobra, tym razem nie będziesz bogaty" Ogden "panie i panowie, nie przejmujcie się, normalne przy operacji."

Ten transportowiec wyraźnie należał do jakiejś organizacji i transportował rzeczy pomiędzy fabrykami itp. Logistyczna jednostka. Na oko, po kształcie, nie była to jednostka daleko latająca. (między nami: transportowiec części do stacji górniczej i transport urobku w drugą stronę).

Jest dobrze, można wpuścić drony. Nastia + Kaspian obejmują to zadanie.

TrZ (dane z dokumentów) +3:

* X: Drony daleko nie dadzą rady same; coś uszkadza komunikację. Efemeryczne manifestacje. Jest... nie najlepiej pod tym względem.
    * Musimy mieć sporo wody. Bardzo dużo wody Hiyori ma ze sobą, ale Kaspian musi to wprowadzić na jednostkę.
* V: Drony dały radę zlokalizować następne pomieszczenia: styczne z kolejnym Cargo i z miejscem mieszkalnym; ale wszystko wskazuje na to, że miejsce mieszkalne jest... zabarykadowane. Coś się stało na tym statku.

Adam, Ewa oraz Ogden wchodzą na jednostkę. Gdy nic złego się nie stało, trzeba wprowadzić Darię. Cel - przygotować stację na dekontaminację jednostki używając przenośnych odpromienników oraz woda z Hiyori mająca przekierować Skażenie do odpromienników. Cel pierwszy - wyczyścić tymczasowo pierwsze pomieszczenie na Okarantis.

Daria odpowiednio montuje 'przenośnego czyściciela', osłaniana przez Ogdena i Kaspiana. Daria ma pomoc Adama i Ewy.

Tr Z (mamy czas + mamy sporo sprzętu na Hiyori) +3:

* V: pierwsze pomieszczenie cargo, to "zniszczone" nadaje się do użytku. Zostało odkażone. Zanim się Skazi ponownie Wasza operacja będzie skończona (rozstawiono zraszacze, są odpromienniki itp)
* X: operacja zajmuje więcej czasu i wody niż mieliśmy nadzieję. Większy koszt (WODA-1)
* Vz: dzięki temu, że Ogden ma nadmiar sprzętu na Hiyori, udało się odpowiednio to zrobić. Sporo sprzętu z tego cargo da się zasilić bezpośrednio do Hiyori by Hiyori mogła zacząć budowę proto-materii. (METAL+1). Dzięki temu, da się połączyć tymczasowo Hiyori z Okarantis (da się odciąć w dowolnym momencie a Hiyori ma skanery i detektory na pełnej mocy aktywne).

Gdy Kaspian skupia się na tym, by z Adamem wyczyścić następne pomieszczenie (cargo bay), Daria konsultuje z Ogdenem swój plan - dostarczyć minimalną ilość energii z Hiyori do Okarantis by móc uruchomić konsolę bez budzenia TAI (jeśli tu jest). I ściągnąć manifest - informacje na temat cargo itp. Ogden akceptuje ten pomysł.

Daria robi shunt w taki sposób, by bezpiecznie odciąć. Nie wie na czym DOKŁADNIE działa ten system, ale jest w stanie użyć standardowych komponentów. Pociesza fakt, że używają tego samego języka, acz wyraźnie język ma rozbieżności (100 lat+ brak kontaktu). Szczęśliwie, jednostka z ZEWNĄTRZ wyglądała na starą, ale nawet pobieżne datowanie części z tego cargo pokazuje, że jednostka jest tu najwyżej 2 lata.

Ex Z (odkażone + Hiyori ma dużo energii) +3 +3Or:

* V: Darii udało się ściągnąć manifest.
    * Okarantis jest jednostką transportującą sprzęt pomiędzy asteroidami górniczymi a fabryką. W tej chwili fabryka -> asteroidy.
        * sprzęt, paliwo, energia, rozrywka, siła robocza... Z Fabryki DO Asteroidów
        * Ogden się MEGA ucieszył. Tu są rzeczy które mogą przynieść profit.
        * zgodnie z manifestem, w cargo które czyści Kaspian jest klasyczny porządny sprzęt górniczy. Łącznie z servarami ratunkowymi.
        * DANE+1

Daria ma manifest i podstawowe informacje. To sprawia, że Kaspian ma kierunek i wie czego się spodziewać. Nastia przeprowadza skany i monitoruje stan Hiyori i Okarantis.

TrZ (skanery + Safiria) +3:

* X: Są pewne problemy.
* V: Ale Nastia je zauważyła. Zbliżający się do Okarantis inny obiekt na kolizyjnym.

Jesteśmy w Anomalii Kolapsu. Tu są "prądy". Są jednostki które poruszają się w próżni. Nic nie jest całkowicie nieruchome. Czasem obiekty się zderzają, czasem są inne sytuacje. I Nastia dostrzegła, że Okarantis jest na linii zbliżającego się powoli innego wraku. Okarantis w tej chwili trochę rotuje (spin). Proste rozwiązanie - zamontowanie zewnętrznych silników manewrowych na Okarantis. Po to, by przesunąć jednostkę troszeczkę. Ale Okarantis jest poobijaną jednostką, nie jest w najlepszej formie i nie działają pola defensywne. Może się rozerwać, jeśli się włączy silnik w złym miejscu.

Prosty problem - Daria zna speckę silników, widziała z zewnątrz Okarantis, MNIEJ WIĘCEJ zna strukturę jednostki (raczej mniej), ale da się założyć gdzie są punkty przyłożenia. Daria musi obliczyć gdzie przyłożyć zewnętrzny silnik manewrowy. Szczęśliwie, ma do pomocy TAI Safira.

Daria określa, gdzie przyłożyć zewnętrzne tymczasowe silniki manewrowe z pomocą Safiry (to jest pojedynczy mały silnik z własnym paliwem; nie utrzyma długo, ale służy do redukcji spinu lub do małego przesunięcia).

Tr Z (Safira) +3:

* X: advancer musi się przejść określić które z tych miejsc jest w miarę odporne. Kadłub nie jest w idealnym stanie.
* V: Daria z Safirą określiły potencjalne lokalizacje, advancer musi sprawdzić co i jak.
* X: HIDDEN: advancer wykryje (nie ma dobrego miejsca, trzeba zrobić platformę)
* V: Daria zoptymalizowała do minimalnej ilości kosztów. Wystarczy pojedynczy silnik zewnętrzny

Kaspian dostał nowe zadanie. Przestać chwilowo skupiać się na przygotowaniu cargo bay, zamiast tego sprawdzić kadłub gdzie zamontować zewnętrzny thruster. Zamiast Kaspiana, przejęli dekontaminację Iga oraz Ogden. Kaspian ma pracować z Darią - Daria z pokładu Hiyori, Kaspian z powierzchni Okarantis.

* Kaspian: "czego szukam, dasz mi speckę?"
* Daria wysyła specyfikację. Te pięć słów jakie Kaspian powiedział to najwięcej Daria od niego słyszała od dawna. Kaspian nie jest gadatliwy.

Za to do Darii dobiega ciężkie westchnięcie Nastii. Wyraźnie Iga papla Ogdenowi i Nastia ma już tego słuchać - ona koordynuje z pomocą Safiry.

Kaspian advancer robi skan zewnętrznej części jednostki dla Darii. Jest ostrożny. On jest zawsze ostrożny i wolny.

Tr Z (wyposażenie itp) +3:

* Vz: Kaspian bezpiecznie się porusza, ale zostawia odpowiednie sweepy czujników i sprawdza co i jak wygląda. Na pierwszy rzut oka już Kaspian ostrzega "Najprawdopodobniej ŻADNE z tych miejsc nie nadaje się na zamontowanie silników"
    * Daria wie, że musi złożyć platformę - kilka punktów przyłożenia siły. I to Kaspian potwierdza, że zadziała.
* V: Kaspian "Coś tu jest... coś anomalnego?" -> w najlepszym, najbezpieczniejszym miejscu Kaspian znalazł coś co klasyfikuje jako potencjalną anomalię. Patryk dostał feed do oceny. (+1Vg)
* X: Kaspian jest stosunkowo blisko; to COŚ się uaktywniło i wysunęło "ektoplazmiczną mackę" w kierunku Kaspiana. Advancer się cofnął.
* X: Kaspian uniknął macki jako takiej bez kłopotu, ale niedaleko macki eksplodował pancerz Okarantis. Kaspian dostał odłamkiem w nogę. Suit odciął dopływ krwi itp. do nogi (uciskowa opaska). Kaspian NATYCHMIAST uruchomił linę powrotną. Adam leci mu pomóc.
* Xz: Udało się, udało się wycofać Kaspiana oraz go uratować. Niestety, Kaspian stracił sporo krwi, jest nieprzytomny i potrzebuje pomocy medycznej. Iga natychmiast jest wycofana, operacja dekontaminacji zatrzymana i fortyfikujemy utrzymane pozycje.
* Vz: W tym wszystkim Patryk dał radę wyciągnąć cenne wnioski odnośnie Okarantis
    * jest to _coś_ co dostało się tu z Anomalii Kolapsu. Patryk klasyfikuje to jako stwora. Trzeba to usunąć.
    * Okarantis jest dużo bardziej Skażona niż się wydawało. Da się tam znaleźć Rdzenniki i inne rzeczy.
        * (ameba "jednokomórkowiec duży" złożony z proto-energii otoczonej materią; lashing out)
    * potencjalnie dużo rzeczy anomalnych na statku, ale to daje też potencjalnie dużą zyskowność.

Iga próbuje po powrocie jak najszybciej pomóc Kaspianowi i uaktywnić nanitki w jego żyłach - by go zregenerowały itp.

TrZ (nanitki) +2:

* X: Kaspian stracił sporo krwi i jest słabiutki. Nie tylko poszedł skafander ale też część Kaspiana ;-).
* V: Nanitki zrobią robotę; Kaspian przeżyje i nie straci żadnej części ciała.
* X: Iga uruchomiła mu turbonanitki i dopakowała energetycznie (ENERGY -1). Niestety, Kaspian został trafiony i lekko zanomalizowany, co sprawiło, że nanitki nie działają jak powinny. Kaspian nadaje się przede wszystkim do pracy z dronami. Nie zrobi tego, co mieliśmy nadzieję.

To sprawiło, że Hiyori nie może korzystać ze swojego GŁÓWNEGO advancera. Zostaje im tylko advancer-in-training, osoba która dołączyła wraz z Darią. Bez nanitek we krwi. Jakub. Daria nie przepada za Jakubem - koleś ma obsesję że wszyscy próbują wszystkich zdradzić. Koniecznie próbuje odzyskać swoją "dawną pozycję", coś mu nie wyszło. I teraz szuka czegoś, co da mu możliwość dalszej kariery. Wszyscy wiedzą, że jest tu tylko tymczasowo.

Tak więc niechętnie, Ogden przekierował Kaspiana do tematów "biurkowych" i mało niebezpiecznych a jako głównego advancera postawił Jakuba.

Jak się pozbyć anomalnego stwora? Patryk ma prostą propozycję - "wysadźmy to :D". Daria ma pewne wątpliwości czy to możliwe by nie uszkodzić Okarantis. Daria zauważyła, że stwór nie atakował póki Kaspian się szczególnie nie zbliżył. To wyglądało na odruch obronny a nie ofensywny. Patryk ma prostą propozycję - stwór wystrzelił odłamek. Odłamki "lecą". On na podstawie tych odłamków może powiedzieć więcej o tym stworze.

Jakub i Nastia wsiedli w skiff. Nastia ma się zbliżyć do jednego z odłamków, Jakub będzie je łapał dla Patryka do badań.

Tr +3:

* V: Nastia szybko i skutecznie zlokalizowała i zbliżyła się do odłamka
* V: Jakub przechwycił odłamek; wyszedł w kosmos z odpowiednim kontenerem i dał radę złapać
* Zanim cokolwiek blisko Anomalii zorientowało się w sytuacji, Nastia wróciła na Hiyori

Patryk przeprowadza badania odłamka. Co można powiedzieć o tym cholerstwie?

Tr Z (odłamek) +3:

* Xz: Odłamek ma własności anomalne i próbuje rozprzestrzenić się po pokładzie
* V: Patryk wie, że to warto potraktować jak "roślinę". Ma "korzenie", do czegoś się przyczepiła i tylko się broni. Jest taumożercą.
* (+3Or) Vz: ten "odłamek" się wczepił w urządzenie skanujące i zaczął się rozpełzać. Patryk spanikował, Ogden to splazmował.

.

* Ogden: "Drodzy państwo, mamy bardzo niebezpieczny derelict. Nie będziemy nawet próbować zdobyć wszystkiego. Spróbujemy zdobyć co się bezpiecznie da i wycofujemy się. Czasami niektóre operacje są nieopłacalne."
* Patryk: "Kapitanie, poradzimy sobie. Mamy skanery, mamy możliwość przesunięcia tej jednostki, wiemy z czym mamy do czynienia."
* Ogden: "Mamy JEDNEGO advancera bo drugi już jest ranny. Nie weszliśmy dobrze na pokład. Nie wiemy z czym mamy do czynienia. Nie powiedziałem, że będziemy się wycofywać - ale wiemy, że Okarantis jest anomalną jednostką i ma dużo problemów."
* Patryk: "To chociaż przesuńmy Okarantis, by ten drugi wrak w nią nie uderzył!"
* Ogden: "Panno Dario, czy da się coś wywnioskować z manifestu i mapy? Czy da się jakoś pozbyć bezpiecznie tej anomalii? Czy warto przyłożyć energię do Okarantis by nawet jak się nam rozpadnie to dało się coś zdobyć z części?"

Daria ma dość ciekawy pomysł - określmy KTÓRE fragmenty Okarantis są bardziej Skażone. Najpewniej to będzie droga prowadząca do silników i do rezerwuarów wody. Postawmy serię ładunków wybuchowych i drillerów, by podzielić Okarantis na część "Skażoną" i część "spoko". Zaplanujmy kontrolowane wysadzanie Okarantis. W ten sposób zwiększamy bezpieczeństwo i redukujemy to, z czym mamy do czynienia.

Ogdenowi spodobał się ten pomysł. Nie jest to super bezpieczny plan, ale Ogden jest skłonny abortować misję by tylko nikt mu nie zginął. Wtedy po prostu poprosi Darię by spróbowała z tej konsolety zczytać jak najwięcej się da i spróbuje sharvestować jak to tylko możliwe. Jak Ogden uważa, NIE KAŻDA operacja salvagerska zakończy się powodzeniem.

Kaspian wysyła drony by określić główne miejsca anomalii, pola magiczne. Tyle może zrobić. Z pomocą Patryka do assessowania.

Daria się uśmiecha, bo Kaspian - który mało mówi - musi współpracować z Patrykiem, który koniecznie próbuje się popisać by Iga lub Daria zobaczyły jaki jest fajny. Na szczęście Patryk - tchórz w operacjach w których może stać się krzywda - zna się na rzeczy analitycznie.

Tr +4:

* X: nie wszystkie drony które wylecą wrócą. (SPECJALISTYCZNE -2)
    * Patryk "leć tędy, tam coś może być". KOLEJNA ROŚLINA. Patryk "oops"
* X: nie uda się dobrze wyizolować WSZYSTKICH miejsc i obszarów Skażenia.
* V: Kaspianowi i Patrykowi udało się określić 'fault line'. Obszar, który da się w miarę sensownie wysadzić. Najbardziej Skażony obszar statku.
    * Jako bonus, wysadzimy WSZYSTKIE "rośliny".

Patryk przychodzi do Darii "słoneczko, udało się znaleźć tu i tu i tu. Dasz radę to zrobić sensownie? Pamiętaj, że możemy trochę pobawić się znalezionym sprzętem górniczym." Daria wie, że ten sprzęt NIE JEST ODKAŻONY i nie wiadomo jak on działa. Patryk tylko wzruszył ramionami "Odkazić mogę pomóc, a Ty dojdziesz jak to działa."

Daria planuje z pomocą Safiry określenie jak to cholerstwo wysadzić. Na razie przy użyciu normalnych materiałów wybuchowych i sprzętu z Hiyori. Daria wie mniej więcej gdzie są skupiska anomalii; niech Hiyori upierniczy laserem górniczym co się da i odstrzeliwujemy resztę. Wpierw ładunki są zamontowane, potem laser górniczy osłabi strukturę a potem odpowiednio wysadzimy Okarantis. Aha, i wpierw zamontować sporo thrusterów na "wartościowej reszcie" - by zatrzymać pęd. W ten sposób ustabilizujemy Okarantis i to co z niej zostanie.

Ogden uniósł brew. Wygląda to na całkiem drogi plan. Ale potencjalnie - albo to, albo nie wejdą na Okarantis. Safira potwierdza to co Daria mówi. Ogden porozmawiał z Nastią i stwierdzili, że warto to policzyć.

Daria planuje z pomocą Safiry określenie jak to cholerstwo sensownie wysadzić i by utrzymać resztę Okarantis.

Ex Z (Safira + dane od Patryka) +4:

* V: Daria ma plan który zadziała. Jest to drogie, wymaga sporo koordynacji, ale da się to zrobić.

Zgodnie z obliczeniami Nastii, mają kilkanaście dni zanim naprawdę coś zagrozi Okarantis; mają czas. Więc zabierają się za to. Wpierw trzeba poukładać 'tymczasowe thrustery' w odpowiedni sposób i zamontować je na Okarantis. Tym zajmuje się Ogden, Daria, Adam. Tymczasem Nastia steruje Hiyori i asekuruje Jakuba (oraz Ewę), który rozkłada blisko 'fault lines' odpowiednie ładunki wybuchowe. Na Hiyori zostaje też Iga jakby Jakub potrzebował pomocy. Kaspian używa dron, by jak co odwrócić uwagę od Jakuba.

Zespół układający tymczasowe thrustery próbuje zrobić to bez szkód i tak tanio jak się da:

Tr Z (plan i obliczenia) +3:

* V: Thrustery zostają dobrze ułożone
* V: Taniej; tak tanio, jak to tylko możliwe (SPECJALISTYCZNE -2)

Tymczasem Jakub i Ewa asekurowani przez Hiyori rozstawiają ładunki wybuchowe w mniej anomalnej części skierowane by odstrzelić część anomalną.

Tr (niepełny) Z (Ewa i asekuracje) +3:

* Vz: niejednokrotnie Ewa lub drony go wyciągnęły; udało się rozłożyć ładunki wybuchowe
* X: Dwie kolejne drony ucierpiały i nie wróciły (SPECJALISTYCZNE -2)
* Vz: uda się tak ustawić ładunki wybuchowe by to co przetrwa z Okarantis nadawało się do dalszego wykorzystania. There is loot.

Operacja została przeprowadzona. Wszyscy oglądali jak Okarantis pękła. Fajnie byłoby całość zdobyć, ale to niemożliwe. Interesujący nas fragment statku odleciał z ogromnym pędem i po chwili thrustery sterowane przez Safirę wytraciły prędkość Okarantis. Mamy interesujący fragment, mamy wejście do śluzy i pozbyliśmy się większości problemów. Co więcej, mamy sporo pomieszczeń, przez które można dodatkowo wejść do środka.

Operacja "Okarantis" może być rozpoczęta.

## Streszczenie

SCA Salvager Hiyori wlatuje do Anomalii Kolapsu, by dostać się do nieznanego derelicta Okarantis. Podczas operacji salvagowania główny advancer został ranny i pojawiła się konieczność rozbicia Okarantis na dwie części - anomalną i "bezpieczną". Zespół ma wejście na bezpieczną część Okarantis i może rozpocząć salvagowanie.

## Progresja

* Kaspian Certisarius: ranny; będzie potrzebował 1 dnia regeneracji na stacji (mimo nanitek) po powrocie do cywilizacji.
* SCA Hiyori: strasznie wykosztowała się z zasobów: FINANSE: -10, SPECJALISTYCZNE: -7, PROTOMAT+1, ENERGIA-1, WODA-1, DANE+1, (ZYSK, CENNE, RARE_MAT):0

### Frakcji

* .

## Zasługi

* Daria Czarnewik: 23 lata, inżynier na salvagerze; analizowała dokładnie derelict Okarantis, opracowała nieudaną intruzję a potem udaną intruzję. Wbiła się do manifestu cargo Okarantis, potem określiła serię eksplozji na fault lines.
* Leo Kasztop: sprzedawca sekretów na Nonarionie Nadziei; |ENCAO:  0-+00 |Intrygancki, polityka;;Nie odracza| VALS: Self-direction >> Stimulation, Tradition| DRIVE: Wygrać w rywalizacji)|; sprzedał Barbatovowi informacje o derelikcie Okarantis znajdującym się w Anomalii Kolapsu. Zna się z Ogdenem dobrze.
* Ogden Barbatov: 44 lata, kapitan + marine + salvager (atarienin); |ENCAO: +-00- |Osoba starej daty;;Jeżeli czegoś chce, weźmie| VALS: Achievement, Power >> Conformity| DRIVE: Uwolnić niewolników|; chce chronić swoich ludzi nawet kosztem rezygnacji z derelictu. Bierze brudną i żmudną robotę, jak trzeba to asekuruje bronią.
* Nastia Barbatov: 43 lata, pilot + marine + salvager (atarienka); |ENCAO: +0--- |Anarchistyczna, nie ufa władzy;;Lubi żartować| VALS: Universalism, Stimulation >> Face| DRIVE: Utopia Star Trek|; świetnie pilotuje Hiyori oraz drony. Zajmuje się też skanami dalekosiężnymi czy Hiyori / derelictowi nic nie grozi.
* Kaspian Certisarius: 37 lat, advancer, noktianin (adastranin); próbuje być daleko od wszystkich; |ENCAO: -+-00 |Skryty;;Bardzo Ostrożny;;Nie widzi nadziei| VALS: Tradition, Family, Hedonism| DRIVE: Corrupted contagion (coś na K1)| bardzo ostrożnie podszedł do derelicta, ale nie spodziewał się 'roślinnej anomalii'. Świetny advancer z nanitkami w żyłach. Bierze wszystkie brudne roboty bez narzekania i w sumie prawie nic nie mówi.
* Jakub Uprzężnik: 29 lat, advancer-in-training (atarienin); |ENCAO: 0+0-0 |Zdradliwy;;Podejrzliwy| VALS: Stimulation, Security >> Achievement| DRIVE: Rana ego|; młody wilczek, advancer-in-training bez nanitek w żyłach. Niechętnie bierze robotę nie-advancera. Tym razem został głównym advancerem, bo Kaspian został zbyt ranny.
* Iga Mikikot: 29 lat, medyk + PR, (altinianka); próbuje wszystkich nawrócić; |ENCAO: 0--+- |Dogmatyczna (Seillia), potępia;;Przyjacielska| VALS: Hedonism, Face >> Power, Humility| DRIVE: Femme Fatale|; postawiła Kaspiana na nogi i nanitkami go dopakowała gdy ten został ranny.
* Patryk Lapszyn: 28 lat, puryfikator + scientist; nie mag (sybrianin); próbuje wejść Idze lub Darii do majtek; |ENCAO: 00-+- |Tendencje do podkradania;;Pomaga innym| VALS: Self-direction, Stimulation >> Security| DRIVE: Wolność od innych|; 
* Safira d'Hiyori: neikatiańska TAI przypisana do salvagera. Steruje androidami: Adamem i Ewą, o czym mało kto wie. Dużo pomaga w obliczeniach Darii i Nastii.
* SCA Hiyori: jednostka neikatiańska; salvager; wysoka automatyzacja i sporo dron; p.d. Ogdena Barbatova; ma dwa androidy: Adama i Ewę. Próbuje harvestować derelict Okarantis.

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Obłok Lirański
                1. Anomalia Kolapsu, orbita
                    1. SC Nonarion Nadziei: zbudowana niedaleko Anomalii Kolapsu, miejsce gdzie kupujesz/sprzedajesz info o derelictach, gdzie szaleją salvagerzy i prospectorzy i gdzie Orbiter rzadko sięga bo nie ma na to dość sił.
                1. Anomalia Kolapsu
                    1. Cmentarzysko Statków
                
## Czas

* Opóźnienie: -3722
* Dni: 11

## Konflikty

* 1 - Daria szuka z dokumentów, papierów, wykresów itp. rzeczy potencjalnie niebezpiecznych.
    * Tr Z (dokumentacja jest szeroka) +2
    * VzVr: to nie pułapka, niewygodnie się dostać, dość nowa jednostka i nadaje się do intruzji. Jednostka się zwróci.
    * (+XXXVg) X: Leo WIE że ta jednostka jest mało wartościowa ale jest w miarę bezpieczna. Więc spoko. Nie udało się obniżyć ceny.
* 2 - Barbatovowie wysłali drony do przeanalizowania kadłuba, czy parametry się zgadzają - czy jest atmosfera itp. Czy ta jednostka jest taka jak sprzedano.
    * TrZ (drony, analiza, skan) +3
    * XV: jedna z dron się uszkodziła, ale Okarantis jest OK. Jak w prospekcie. Nadaje się.
* 3 - Kaspian próbuje odzyskać dronę, po przejściu przez spacer kosmiczny.
    * TrZ+3
    * XV: Kaspian nadłożył drogi; jest silne Skażenie w silnikach warp. Kaspian zamontował 'przechwytywacz' i wrócił na Hiyori.
* 4 - Patryk i Daria siedli do drony - co się z tym cholerstwem stało, jak to naprawić, czy w ogóle warto.
    * TrZ (sprzęt na Hiyori jest dobry) +3
    * XVVz: Patryk nie wie co dokładnie nie tak; trzeba gaśnicą spuryfikować; wiemy że dużo anomalii + Daria naprawi tę dronę.
* 5 - Daria dostała zadanie na podstawie skanu dron POTWIERDZIĆ czy słabe punkty statku są dalej słabymi punktami by zrobić śluzę 'awaryjną'.
    * TrZ (dokumenty PLUS skany dron) +3
    * XzX: zanomalizowany statek, silnie uszkodzony strukturalnie. Straty finansowe; zmarnowany czas i surowce.
    * Daria rekomenduje spróbowanie koło cargo (+1Vg+3Og): Og: śluza intruzywna się udała, ale po drugiej stronie w cargo część transportu została zniszczona.
* 6 - Jest dobrze, można wpuścić drony. Nastia + Kaspian obejmują to zadanie.
    * TrZ (dane z dokumentów) +3
    * XV: Za duże Skażenie; nie można liczyć na same drony. Dużo wody trzeba. Ale - drony mają info o następnym pomieszczeniu.
* 7 - Daria odpowiednio montuje 'przenośnego czyściciela', osłaniana przez Ogdena i Kaspiana. Daria ma pomoc Adama i Ewy.
    * Tr Z (mamy czas + mamy sporo sprzętu na Hiyori) +3
    * VXVz: odkażenie pierwszego pomieszczenia, ale większy koszt, udało się zacząć syntezę proto-materii (protomat). 
* 8 - Daria robi shunt w taki sposób, by bezpiecznie odciąć. Nie wie na czym DOKŁADNIE działa ten system, ale jest w stanie użyć standardowych komponentów.
    * Ex Z (odkażone + Hiyori ma dużo energii) +3 +3Or
    * V: Darii udało się ściągnąć manifest, wie co to za jednostka i co można z nią zrobić.
* 9 - Daria ma manifest i podstawowe informacje. To sprawia, że Kaspian ma kierunek i wie czego się spodziewać. Nastia przeprowadza skany i monitoruje stan Hiyori i Okarantis.
    * TrZ (skanery + Safiria) +3
    * XV: udało się wszystko opanować, ale do Okarantis zbliża się inny obiekt na kolizyjnym (sporo czasu).
* 10 - Daria określa, gdzie przyłożyć zewnętrzne tymczasowe silniki manewrowe z pomocą Safiry
    * Tr Z (Safira) +3
    * XVXV: advancer musi się przejść, Daria z Safirą mają lokalizację i minimalizację kosztów, HIDDEN PROBLEM
* 11 - Kaspian advancer robi skan zewnętrznej części jednostki dla Darii. Jest ostrożny. On jest zawsze ostrożny i wolny.
    * Tr Z (wyposażenie itp) +3
    * VzVXXXzVz: Kaspian wykrył brak miejsca, ale coś anomalnego; ektoplazmiczna macka, Kaspian ranny ale uratowany. Patryk ma wnioski o Okarantis.
* 12 - Iga próbuje po powrocie jak najszybciej pomóc Kaspianowi i uaktywnić nanitki w jego żyłach - by go zregenerowały itp.
    * TrZ (nanitki) +2
    * XVX: Kaspian słaby, Iga wzmocniła go turbonanitkami. Niestety, Kaspian może się tylko do dron nadawać (lub prostych prac) na tej akcji.
* 13 - Jakub i Nastia wsiedli w skiff. Nastia ma się zbliżyć do jednego z odłamków, Jakub będzie je łapał dla Patryka do badań.
    * Tr +3
    * VV: Nastia zlokalizowała odłamek i Jakub go przechwycił do badań dla Patryka.
* 14 - Patryk przeprowadza badania odłamka. Co można powiedzieć o tym cholerstwie?
    * Tr Z (odłamek) +3
    * XzV: odłamek ma własności anomalne; Patryk wie żeby traktować to jak "roślinę"
    * (+3Or) Vz: ten "odłamek" się wczepił w urządzenie skanujące i zaczął się rozpełzać. Patryk spanikował, Ogden to splazmował.
* 15 - Daria się uśmiecha, bo Kaspian - który mało mówi - musi współpracować z Patrykiem, który koniecznie próbuje się popisać by Iga lub Daria zobaczyły jaki jest fajny.
    * Tr +4
    * XXV: nie wszystkie drony dotarły do celu, nie wszystko się udało wyizolować, ale jest "fault line" po którym toczyć eksplozje.
* 16 - Daria planuje z pomocą Safiry określenie jak to cholerstwo sensownie wysadzić i by utrzymać resztę Okarantis.
    * Ex Z (Safira + dane od Patryka) +4
    * V: Daria ma plan który zadziała. Jest to drogie, wymaga sporo koordynacji, ale da się to zrobić.
* 17 - Zespół układający tymczasowe thrustery próbuje zrobić to bez szkód i tak tanio jak się da
    * Tr Z (plan i obliczenia) +3
    * VV: dobrze ułożone thrustery i to tak tanio jak się da
* 18 - Tymczasem Jakub i Ewa asekurowani przez Hiyori rozstawiają ładunki wybuchowe w mniej anomalnej części skierowane by odstrzelić część anomalną.
    * Tr (niepełny) Z (Ewa i asekuracje) +3
    * VzXVz: Jakub bez Ewy nie dałby rady; kolejne zniszczone drony, udało się by oddzielić i móc wejść na Okarantis by był loot.

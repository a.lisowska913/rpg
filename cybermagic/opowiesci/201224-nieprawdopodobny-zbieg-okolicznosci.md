---
layout: cybermagic-konspekt
title: "Nieprawdopodobny zbieg okoliczności"
threads: legenda-arianny
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [201115 - Wyścig jako randka](201125-wyscig-jako-randka)

### Chronologiczna

* [201115 - Wyścig jako randka](201125-wyscig-jako-randka)

## Punkt zerowy

### Dark Past

.

### Opis sytuacji

.

### Postacie

.

## Punkt zero

.

## Misja właściwa

Klaudia i Elena są na torze wyścigowym na K1. Elena chce, by Klaudia podkręciła trasę - Klaudia chce, by Elena wpierw przejechała trasę raz i skutecznie. Elena chce coś klasy Krypty. Klaudia... mniej chętnie.

Elena jedzie. (ExZM: XXX). Elena jedzie ZA SZYBKO. Za ostro. Za ambitnie. Klaudia NATYCHMIAST próbuje powstrzymać Elenę przed katastrofą manipulując systemem anomalii na torze. (XVV). Sukces!

Elena jest cholernie dobra. Ale Klaudia postawiła jej serię anomalii grawitacyjnych (wgniatanie, wprowadzenie w łuk, po czym traci kontrę). Grawitacja, zmiany, nieprzewidywalne. Klaudia nakłada _strain_ na ciało Eleny i ścigacz. I to wystarczyło - Elena pieprznęła bokiem i zrobiła kilka fikołków, w ostatniej chwili odzyskując kontrolę nad bocznymi silnikami manewrowymi. Elena jest zakrwawiona, ale nic szczególnie poważnego.

Elena zrobiła półpiruet, wyciągnęła pistolet i wycelowała w kogoś (prawie się przewracając). Ktoś się ukrywał na trasie. Gość podniósł ręce i wyszedł. To jakiś koleś techniczny. Elena się wydarła. Strzeliła mu koło nogi, by go przestraszyć. Udało jej się.

* Elena: "Przez CIEBIE się rozbiłam"
* Klaudia (komunikatorem na priv): "Nope"

Facet spytał Elenę i Klaudię, czy widziały 12-letniego chłopca. Zaginął. Kierował się GDZIEŚ TAM -> pokazał kierunek, gdzie odpłynęła energia z anomalii Klaudii. On miał odpowiadać za pilnowanie dziecka, ale miał pracę. Wyskoczył critical. Przyciśnięty, facet powiedział, że chłopak (Jacek) miał skuter. Elena dopytała o szczegóły - skuter dzieciaka miał TAI. Ale TAI miała zaprojektowane konkretne miejsca, konkretne kierunki. Nic innego. Skuter zjechał z toru?

* Skuter miał "mapę bezpiecznych miejsc". Tam Jacek może jeździć.
* Z jakiegoś powodu, ten tor wyścigowy jest "bezpiecznym miejscem" zdaniem TAI.
* Jacek chciał pojeździć i ojciec (bo tym jest ten facet) mu pozwolił. A sam był zbyt zajęty by w ogóle się zainteresować.

Klaudia ma lokalne anty-anomalne kamerki. Plus, zażądała od ojca kodów kontrolnych do skutera. Szuka dziecka i nadaje kod do skutera: "wróć do taty". Albo znaleźć skuter albo wrócić z powrotem.

* V: Klaudia ma lokalizację skutera. Wie, gdzie stoi. Całkiem niedaleko jednej ze ścian. W środku nikogo nie ma a w ścianie jest wyrwa - za mała, by skuter tam przeszedł.
* XX: Niestety, z okolic tej ściany Klaudia wykrywa silne promieniowanie. Jakiś subkomponent K1 jest uszkodzony; nie można długo przebywać w takim terenie. Nie bez odpowiednich osłon.

Klaudia zażyczyła sobie, by Leon (ojciec Jacka) zdobył odpowiednie kombinezony. Elena podała parametry, by pasowały. A Klaudia parametry techniczne. Leon oddalił się w przyspieszonym tempie. Elena spytała Klaudię o to, czy nie muszą jechać od razu. Klaudia potwierdziła - chce jechać TERAZ zanim dziecku coś się stanie. Ale nie chce, by ojciec wiedział.

Elena szybko podjechała do skutera. Są w stanie wejść w tą dziurę w ścianie bez większego kłopotu, acz będzie... niewygodnie. To jest zawalony _maintenance shaft_, ale nie wiadomo jak się tam dostać z innej strony.

Klaudia wzywa Dianę. Trzeba coś rozwalić. Diana zwinęła bombę Eustachemu (EUSTACHY-SAMAAAA) i ona chce ją użyć. A przebicie się przez uszkodzony fragment zajmie trochę czasu.

Klaudia stwierdziła, że musi dowiedzieć się o co chodzi z TAI skutera. Nie ma co marnować czasu. A tymczasem Elena próbuje zrozumieć jak działa ta szczelina, skąd to się wzięło... a raczej, co przez to przeszło - lub mogło przejść. Klaudia bardzo szybko zorientowała się, że tu jest coś bardzo anomalnego; więc dodała magii do równania by coś próbować znaleźć. O co tu chodzi?

Dwa kąty uderzenia - czemu TAI tu przybyło? Po drugie, od kiedy ten teren jest w ogóle bezpieczny?

(V):

* Ten skuter tu podjechał, po czym jego pilot (dziecko, Jacek) zatrzymał skuter i wszedł w szczelinę. Szedł na pomoc głosowi, dziewczyna, młoda. Prosiła o uwolnienie.
* Ten skuter tu podjechał, bo TUTAJ Jacek go skierował - on szukał tej dziewczyny. Dziewczyna miała komunikator krótkiego zasięgu i złapała Jacka i jego skuter. A Jacek przekonany że teren jest bezpieczny pojechał jej pomóc.
* Skąd teren bezpieczny? Jak dziewczyna mogła połączyć się z Jackiem? TAI skutera została odblokowana a raczej... overridowana przez Elainka-class TAI.

(XXX): 

* Skuter zostanie zniszczony i wszelkie ślady są destruktywne. Klaudia musi zniszczyć wszystko by wyciągnąć esencję.
* Elainka jest anomalną TAI. Masz do czynienia z anomalną Elainką. Coś skaziło Elainka-class military TAI.

(V):

* Elainka nie zaraża. Tylko ona jest Skażona. Żadna Elainka nie ma takich uprawnień, tylko wobec dzieci (by je chronić).
* Ta Elainka syntetyzowała głos tej dziewczyny. Elainka ściąga dziecko do zagrożenia / do siebie.
* Elainka o której mówimy ma prime directive - nie zabić dziecka. Acz już jej ostatnie działania pokazują, że _ktoś_ overridował jej inne prime directives.
* Istnieje power suit / coś, co zawiera ową Elainkę. Czyli mamy jakiegoś robota.

(V): 

* Klaudia użyła resztek sygnału i magii. Doszła do tego, że wie już JAKA to instancja Elainki. To instancja AKR-7734. Zgodnie z papierami K1, ta instancja Elainki jest "w zamrażarce". Nie jest używana w chwili obecnej. Nie są z niej robione żadne kopie. Czyli jakakolwiek operacja tu nie miała miejsca, nie jest zgłoszona na K1. Ale Klaudia wie który wzór TAI był wykorzystany. Czyli Klaudia wie z jakimi operacjami ta Elainka miała do czynienia, ma też wzór taktyczny. Wie jak była uczona.
* Elainka model AKR-7734 to model 'night hunter'. Poluje. Preferuje walkę bronią białą. Zwykle uruchamiana jest w mechaformach które są 'potworowe'. Zwykle wabi ofiarę do celu a potem atakuje. 
* Aha, i ta instancja jest Skażona. M.in. energią Skażenia Klaudii.

Zdaniem Klaudii, tej Elainki nigdy nie powinno być w pobliżu. Skąd się wzięła, kto ją uruchomił i czemu poluje na dzieciaka, którego też nie powinno być w pobliżu.

V:

Klaudia nie wierzy w zbiegi okoliczności. Ale cała ta operacja jest niemożliwa, jeśli nie masz:

* Elainki w Skażonym miejscu.
* Skażenia wywołanego przez Klaudię.
* Dzieciaka, który ma skuter i AKURAT pojedzie w tym kierunku
...

Klaudia podejrzewa zupełnie inny typ umysłu. Nie tą Elainkę. Tu jest coś nie tak. Coś o zdolnościach klasy Eszary...

Diana dociera do Zespołu. Elena patrzy na nią z udawaną obojętnością. Klaudia się uśmiecha.

Diana i Elena się zapoznały. Elena przyjęła Dianę chłodno (acz nie wrogo). Diana dostała od Klaudii zadanie zrobienia dziury przez którą przejedzie ścigacz; użyła bomby Eustachego i wypaliła drogę. Bardzo skutecznie (V). Niestety, wypiaskowała Elenie ścigacz (X) przy okazji. Zdaniem Diany, tym lepiej - był porysowany. Łatwo Elena, drugi mechanik Inferni, naprawi.

Diana, Elena i Klaudia mają więc drogę otworem. Ścigacz zapewnia ekranowanie. Nie czekają na Leona.

Elena jedzie za dzieciakiem. Próbuje go zlokalizować na sensorach swojego ścigacza. Elena odkryła, że im bardziej zbliżają się do źródła promieniowania, tym bardziej rozpada się połączenie pomiędzy ścigaczem i natywnym TAI ścigacza. To implikuje, że Elainki tam nie będzie. Szczęśliwie (Klaudia nie wierzy w szczęście) mają sygnaturę dziecka, jego biopole itp. Elena jest ranna, więc szczęśliwie nosi ze sobą podręczne skanery itp. Martyn nie doceni, ale lol.

Klaudii coś się to szczęście nie podoba. Ale ma zamiar znaleźć dziecko magicznie. I okrada Elenę z sensorów, które Elena z radością Klaudii daje. Klaudia z pomocą Eleny podmontowuje to do ścigacza Eleny. Jeśli są w stanie znaleźć dzieciaka, to to zrobią. Dzięki sensorom i ścigaczowi, Klaudii udało się zlokalizować przybliżone miejsce dziecka. Jest w obszarze ekranowanym przed radioaktywnością, takiej "wysepce". Klaudia nawet nie wiedziała, że takie coś tam jest, jakaś kopuła. Ofc, Klaudia podejrzewa pułapkę. Klaudia wzmocniła sensory, by upewnić się, że nikt na pewno jej nie zaskoczy. Ma też specyfikę tego terenu. Zgodnie z systemami K1, to były biovaty do niebezpiecznych badań. Innymi słowy, kopuła była otoczona przez mechanizm umożliwiający zalanie tego. I tam jest wyciek. Stąd jest radiacja. Ale sama kopuła czy pomieszczenie jest szczelne i bezpieczne.

Ale co takie dziecko robi w takim miejscu? Plus, to dwie kondygnacje NIŻEJ. Blisko, ale 2 kondygnacje niżej. I Klaudia zauważyła, że jest "miejsce", załamanie struktury K1. Dziura, przez którą można wpaść. I ta dziura wysyła najsilniejszą emisję radiacji. Czyli najpewniej dzieciak wpadł i się zsunął. Dorosły też miałby problem tam wpaść.

Klaudia streściła sytuację Elenie i Dianie. Da się tam wpełznąć, zwłaszcza z pomocą Diany. Klaudia i Elena wchodzą, Diana zostaje w skuterze. Elena jest bardzo podejrzliwa by zostawiać Dianę w skuterze, ale nie ma wyjścia. Ktoś musi a Elena nie puści Klaudii samej (Diana zdaniem Eleny się nie liczy).

Klaudia wpełza do środka z Eleną. Elena przodem. (V): zeskoczyła, zachwiała się, ale dała radę. Na pozycji. Klaudia spadła na pysk, ale Elena ją złapała (XV). Jest tam kopuła, tam gdzie były niebezpieczne biovaty. Promieniowanie na dole, pod kopułą, tam gdzie wyciek - pojawiły się tam rośliny i grzyby, nienaturalne. Tam jest ogólnie... zła atmosfera, że tak powiem. Ale kopuła jest dość otwarta, choć izolowana.

Zespół (Elena i Klaudia) próbują dostać się do kopuły. Wtem - Elena odpycha Klaudię. Strzał. Elena odpowiada ogniem i ściąga ogień na siebie. Przeciwnik strzela do Eleny - trafia ją lekko w ramię. Klaudia szybko poprosiła Dianę o wsparcie i dała jej lokalizację. Elena ściąga ogień dalej (XV): odciągnęła uwagę, acz dostała jeszcze w nogę. Elena jest unieruchomiona. Ma lekki pancerz; przeciwnik używa czegoś ciężkiego.

Diana rzuca czar w odpowiednim kierunku, pomagając sobie ścigaczem Eleny (zwisa). TrM pełna moc. Diana uderza na całość. (5: XXXVV). Fala czystej, degenerującej entropii walnęła w tamtym kierunku. Diana spadła bo lina zdegenerowała; potłukła się. Klaudia aż poczuła ból zębów od tej chorej, czysto entropicznej energii. Przeciwnik spadł. Jest po drugiej stronie kopuły; Zespół stracił LoS.

Elena zażądała od Klaudii wspomagaczy. Klaudia nie chce dać jej niczego "specjalnego". Elena weszła w tryb mantry. Używa swojej energii magicznej do pobrania mocy z otoczenia i zasklepienia swoich ran; doprowadzenia się do funkcjonowania. Ale Klaudia z lekką obawą patrzy na poziom Skażenia w okolicy. Klaudia kazała Dianie puryfikować energię którą zasysa Elena; nie może tak się to skończyć!

Diana wykonuje robotę naspodziewanie dobrze; Elena asymiluje energię i na oczach Klaudii _visiat_ regeneruje nogę Eleny. Poziom napromieniowania Eleny drastycznie skacze; ma promieniowanie takie jak arcymag, ale dla Eleny to nie jest coś nietypowego. Elena przygotowuje swój pistolet i ostrożnie podbiega do kopuły (VXV). Za nią pojawia się Samuraj Kwiatu Wiśni.

"Cierpieniem nie zagłuszysz faktu, że jej tu nie ma" - Samuraj do Eleny.

Klaudia rzuca w Samuraja _czymś_ by go zagłuszyć. Elena - półobrót, pistolet na Samuraja. Nie wie co to jest. Klaudia zdeeskalowała; Elena kontynuuje zbliżanie się do kopuły. Po tym co Diana zrobiła, przeciwnik powinien być oszołomiony. Klaudia wysłała Elenę do usunięcia przeciwnika; Klaudia i Diana wchodzą do kopuły. Samuraj jest bezużyteczny, acz powiedział, że będzie osłaniał Elenę. Elena "zbytek łaski".

Klaudia widzi biovaty. Sporo biovatów, kilkanaście. W tych biovatach są _mutilated bioforms_. Wszystkie na bazie człowieka. Wszystkie jakieś anomalne, splugawione. Wszystkie... żywe. I w jednym z biovatów Klaudia dostrzega chłopaka którego szuka. Ale jemu nic nie jest.

Ruch. Klaudia pada i podcina Dianę. Przez drzwi ze Skażonego terenu wchodzi kobieta. Jest lekko ubrana. Na pewno nie ma osłony. Jest zaskoczona obecnością Klaudii i Diany. Nie ma broni.

Dama przedstawiła się jako Jasmina Perikas; jest lekarzem. Kiedyś Zespół wyciągnął ją z Krypty, ona nie jest "stąd". Jest bardzo odporna na promieniowanie i inne patogeny, więc działa bez szczególnego ubrania by nie było pytań potem gdzie była. Działa w tym fragmencie K1, bo jest opuszczony; te biovaty ledwo działają, ale działają. Jasmina próbuje wykorzystać wiedzę z tych żywych i martwych osób by móc lepiej pomóc następnym generacjom i osobom. A chłopak? Znalazła go w obszarze Skażonym; dostał potężnej reakcji alergicznej na te chore rośliny. Zabrała go tutaj. Usłyszała od chłopca, że on przyszedł pomóc jakiejś dziewczynce i zbierała się, by ją znaleźć.

Klaudia wyszła z drugiej strony Kopuły, by znaleźć Elenę. Znalazła pistolet w swoim kierunku (Elenę) i poturbowanego kolesia w pancerzu bojowym. Koleś potrzebuje pomocy medycznej - fala entropii zrobiła mu krzywdę. Elena i Klaudia wciągnęły go do środka; Jasmina wsadziła go w biovat. Klaudia od razu zabrała jego identyfikator. Grzegorz Chropst, siły porządkowe Orbitera. Dafuq.

Jest przesłuchiwalny zanim trafi do biovatu. Grzegorz powiedział, że gdzieś tu znajduje się robot bojowy. Robot poluje m.in. na niego. On chce złapać go najpierw. Ale tu nikogo nie powinno być! Jak to możliwe, że nagle na K1 się tak dużo osób pojawiło w tym samym obszarze!

Kolejny zbieg okoliczności. Klaudia ma już tego dość.

Klaudia zaczyna ostro przesłuchiwać Grzegorza. Elena ma zły humor i była swego czasu terrorem dla arystokratów. Dobry glina - zły glina. (VXV). Przyznał się do wszystkiego, ale Elena go kilka razy obiła - nie wykazała się CIENIEM delikatności. Klaudia i Elena mają przeciwnika.

* Siły porządkowe Orbitera mają czasami "zabawę". Grę.
* Jego kolej. Uaktywnili dla niego Elainkę.
* Miał ją pokonać i zniszczyć. Ona miała pokonać jego.
* Coś się stało i Elainka anomalizowała. It got stronger and more dangerous.
* Polował na Elainkę, ale nie mógł jej znaleźć. Elainka ściągnęła go w ten teren - nikogo nie ma, perfekcyjne miejsce. Myślał, że to ona.
* Tylko cudem nie zastrzelił Jasminy. Elainka ma opcję mimikry. Dostała ją dzięki anomalii. Jasmina - niezbyt ubrana kobieta - byłaby OCZYWISTĄ Elainką dla Grzegorza...

Klaudia nie czuje się winna anomalizacji Elainki... ale nie chwali się tym, że miała Paradoks robiąc racetrack. I nie chwali się wyraźną korelacją.

Klaudia ma chory pomysł. Mają _suit_ Grzegorza. Czy da się zrobić z niego przynętę? TAI vs TAI. Nie ma pokonać anomalnej Elainki, ale ma przynajmniej kupi czas. A kink polega na tym, że mają tu być przesłane sygnały i ślady zebrane podczas walki z Elainką. Klaudia używa swojej magii, pomaga jej Elena (jest mechanikiem, przypominam). A Jasmina wie co jest tu do dyspozycji.

* V: bait się uda. Elainka zapoluje na power suit Grzegorza.
* X: power suit ma wpiernicz; przełożony Grzegorza zrobi oficjalny protest że z winy Klaudii doszło do destrukcji servara.
* V: Dane o Elaince i jej stylu działania trafią do Klaudii - będzie się dało je przeczytać _tutaj_, w tej Skażonej centrali.

Elena zwróciła uwagę na jeszcze jedną rzecz. Ten system, wszystkie te urządzenia - są na granicy rozpadu. Jeśli nie uda się zdobyć zespołu naprawczego to w ciągu jakiegoś miesiąca eksploatacji tych systemów przez Jasminę to się posypie i Jasmina najpewniej zginie. Klaudia nie mogła nie docenić, jak duże Jasmina miała szczęście, że tu przyszli. To się już robi zabawne.

Z tego Klaudia wydobyła ciekawą informację - to "coś" co stoi za tym wszystkim nie jest im wrogie. Tym bardziej Klaudia jest zaciekawiona co to jest. Admirał Termia? Nie pasuje do jej charakteru. Plus, za duże zasoby.

Zespół bezpiecznie się wycofał - Klaudia ewakuowała siłą Grzegorza i Jasminę. Chłopaka wzięli ze sobą. Elena jako siła ognia na szczycie.

Klaudia podejrzewa coś w stylu AI, jakiś _strand_ AI. Coś typu Persefona lub wyżej.

Przekonanie Leony, żeby zapolowała na Elainkę było naprawdę proste. "Hej, chcesz poćwiczyć?". Leona z radością rzuciła się na ten problem.

Klaudia pozycjonowała Elenę jako dowódcę operacji "naprawiamy tamto miejsce", ale sama ściąga zespół naprawczy. Niech zespół doprowadzi tamto miejsce do działania, dla Jasminy... i dla siebie. Poważny problem biurokratyczny, ale nie niemożliwy - zwłaszcza, że Klaudia ma trochę zaskórniaków. Sukces. Zgromadziła odpowiednią ekipę; za 2 tygodnie będzie tam sprawna kwatera.

Klaudia siadła do komputerów. Musi zrobić bardzo głębokie poszukiwania i analizy. Kto jest Architektem Przypadku. Skuter dziecka - kontrolowany przez Elainkę. Akurat Elainka która była uruchomiona przez łowcę. Akurat tam, gdzie jest jej Skażenie. I tylko dzięki temu ratują Jasminę. Klaudia uważa, że to bardzo złożony umysł, podejrzewa Eszara-class lub uwolnioną Persefonę. Plus jak ogromne dostępy ma ten przeciwnik. I jakie ma wpływy w świecie rzeczywistym - inżynieria criticala by koleś nie mógł się zająć dzieckiem? Jaki ma dodatkowo monitoring? I możliwości zaplanowania? Takie... delikatne pchnięcia.

(ExZ+3):

* V: Klaudia odkryła jeszcze jeden wzór:
  * Klaudia - zrobiła Skażenie na K1. To Skażenie było problematyczne. Przez to mogli ucierpieć: dziecko, Grzegorz, Jasmina.
  * Leon - często ignorował Jacka, przedkładając pracę nad chłopaka.
  * Grzegorz - to nie pierwszy raz jak robią takie polowania. Po prostu się nudzą.
  * Jasmina - nie zintegrowała się na K1, wierzyła w to, że jest tam bezpieczna.
  * Można powiedzieć, że dla dużej ilości osób to wydarzenie było formą ostrzeżenia. Czyli to było przewidziane.
* V: Klaudia znalazła powtarzający się wzór. To nie tylko z nimi. Spotkała się z wieloma 'urban legends' o "duchu opiekuńczym Kontrolera". Wolnej TAI która współpracuje z na wpół nieaktywną TAI Zefiris i ratuje, pomaga. Podobno widzą ją tylko dzieci - bo im nikt nie uwierzy.
* X: Klaudia zebrała sporo wierszyków, książeczek, rysuneczków itp różnych dzieci. I to wyjdzie.
* X: Klaudii udało się dostać wizualizację od dzieci. To dziewczynka. Po głosie - to był ten głos, którym zwabiła Jacka. I Klaudia dostała imię. To zawsze jest wariant imienia "Sophia", "Zofia", "Zosia" itp.

Zofia. TAI współpracująca. Duch opiekuńczy Kontrolera Pierwszego. Zagadka, która naprawdę zainteresowała Klaudię...

## Streszczenie

Nieco zaniedbane dziecko bawiące się na K1. Skażona Elainka, na którą polują. Badaczka biomantyczna, która objęła rozpadające się biolab. I Klaudia, która z Eleną wplątała się w to wszystko by naprawić sytuację i trafiła na ślad ducha opiekuńczego Kontrolera Pierwszego. "Zofia"?

## Progresja

* Klaudia Stryk: za dwa tygodnie będzie mieć stabilną, sprawną kwaterę w czarnych sektorach K1 dookoła badań biologicznych, którą dzieli z Jasminą Perikas. 
* Elena Verlen: osobisty wróg w Grzegorzu Chropście - skrzywdziła i pobiła łapczaka (który zaatakował ją pierwszy).

### Frakcji

* .

## Zasługi

* Klaudia Stryk: uratowała chłopca, Jasminę, włączyła Dianę i Elenę do działania - i jej krótkie poszukiwania potwierdziły, że na K1 jest "duch opiekuńczy".
* Elena Verlen: alias Mirokin; słaba, ale chce uratować Anastazję za wszelką cenę. W walce z Grzegorzem go unieszkodliwiła, acz została bardziej ranna.
* Diana Arłacz: pokazała co potrafi potęgą entropicznej anihilacji, strącając Grzegorza z sufitu. Jednak nie nienawidzą się z Eleną.
* Jasmina Perikas: badaczka chorób odporna na nie; nielegalnie zajęła laboratorium w Sektorze 43 K1. Pomogła naprawić chłopca i Grzegorza.
* Grzegorz Chropst: siły porządkowe Orbitera; czasem polują sobie na Elainkę w Czarnym Sektorze z nudów. Dostał wpiernicz od Eleny, acz ją zranił.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria, Orbita
                1. Kontroler Pierwszy
                    1. Sektor 43: jeden z Czarnych Sektorów Kontrolera
                        1. Tor wyścigowy ścigaczy
                        1. Laboratorium biomantyczne: rozpadające się laboratorium badań nad niebezpiecznymi istotami, objęte przez Jasminę Perikas.

## Czas

* Opóźnienie: 2
* Dni: 1

## Inne

.
---
layout: cybermagic-konspekt
title: "EtAur i przynęta na Kryptę"
threads: legenda-arianny, etaur-zwycieska
gm: żółw
players: fox, kapsel, kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [220316 - Potwór czy choroba na EtAur?](220316-potwor-czy-choroba-na-etaur)

### Chronologiczna

* [220316 - Potwór czy choroba na EtAur?](220316-potwor-czy-choroba-na-etaur)

## Plan sesji
### Co się wydarzyło

* Koordynator-Antoni-Persefona ukrył obecność mordercy (Daniela Puszanta)
    * Antoni był kiedyś faerilem; powiązany z Danielem przez Arkologię Dekantis
* W Eternijskiej bazie jest w biovatach rodzina mordercy (Daniela Puszanta)
* W Eternijskiej bazie 
    * tien Melania Akacja (tienka Eternijska): zgłosiła problem
    * poprzedni koordynator magazynowni, Rafał Szlędak, zaprzyjaźnił się z TAI Nephthys (zaawansowana Persefona).
        * Nepthys jest zbyt błędna; więc jest wpakowana do "pudełka".
* Baza nazywa się EtAur Zwycięska.
    * tien Maciej Parczak: Eternia; tymczasowo dowodzi stacją (dotknięty Chorobą); faza: Irytka.
    * tien Dominika Perikas: Aurum; druga w dowodzeniu. Przejęła po Rafale. ???
    * Erwin Mumurnik: szef inżynierii; paranoik, trochę zajmuje się szmuglem z lokalnymi piratami
    * tien Feliks Ketran: Eternia; lekarz (dotknięty Chorobą); faza: Irytka. 
    * Suwan Chankar: były komandos, chroni interesy Eterni. Szef ochrony. Nie lubi Orbitera, melancholik i stoik
    * Tymoteusz Czerw: oficer łącznościowy Eterni. Chipper. Hero worship.

### Co się stanie

* .

### Sukces graczy

* .

## Sesja właściwa
### Scena Zero - impl

* 

### Scena Właściwa - impl

Zespół ma sygnał od Marii - SOS. Arianna chce ratować Dominikę. Eustachy błyskawicznie zostawia ładunek kierunkowy po czym leci ratować Marię i Raoula.

Eustachy leci - jak szybko dotrze do Marii i Raoula.

Tr+3:

* V: Eustachy zdąży zanim dojdzie do problemu. 
* Vr: Eustachy zdąży jak jeszcze potwór jest na miejscu.

"Potwór" odwrócił się częściowo i strzelił w kierunku Eustachego z działa kinetycznego. Nie trafił oczywiście. Eustachy -> Raoul "ognia", ogień zaporowy. Stwór nie powinien wiedzieć o obecności Eustachego ale wiedział. Eustachy i Raoul strzelają.

ExZ+4:

* XXz: zaczynają zamykać się grodzie. Odciąć potwora.
* X: przeciwnik dał radę się wycofać, zwiał gdzieś w głąb pierścieni, w kierunku na Lamię i Chankara.

Maria przeprowadziła zaklęcie analityczne; z czym ma do czynienia. Pobieżny skan.

* X: Maria zrzuciła FAGI w ich pokoju
* V: Maria ma pewną odpowiedź

Eustachy się dostał do pokoju Marii i wypalił jej fagi. Raoul cały nieszczęśliwy.

TYMCZASEM KLAUDIA wbija się do Barbakanu. Kamery, skanery itp.

TrZ+3:

* V: Kody zadziałały. Udało się Klaudii dostać obraz. Na mostku nie dzieje się nic dziwnego. Tylko tyle, że Dominika zamknęła się w małym pokoju spotkań. Pełny seal.

Mumurnik: "Orbiter? Co tu robicie?" Klaudia: "Spotkanie z tien Perikas". Mumurnik chciał coś powiedzieć, ale się wycofał. Wrócił do swojej stacji. Dotarli do Dominiki Perikas - ta chce by Klaudia przejęła kontrolę nad stacją. Dominika -> Klaudia "przejmijmy to w imieniu Aurum". I Klaudia dostała kody.

Na mostku, komputery - i dostęp do AI Core. Do tego nieszczęśnika w AI Core. "I dlatego Klaudia jest paziem tien Arianny Verlen". Kłótnia Dominika - eternianie.

Leszek Czarban: Skażony przez arkin; mag, chce ustrzelić Klaudię jak ta się dostanie do komputerów. "To kolejny spisek Orbitera - nasz lekarz i żaden lekarz nie ogłosił plagi. Nie macie prawa usuwać z mostka przez zarazę." Arianna: ten człowiek jest zarażony i musi być odizolowany. Klaudia wali przepisy i groźne reguły.

Arianna: "Jako przedstawiciel Aurum i sojusznik Eterni zarządzam kwarantannę na stacji + przepisy Klaudii. A Leszek jest zarażony i ma pierwsze objawy. Maria (eternijski mag) powiedziała że tak się zaczynają." 

TrZ+4:

* V: Wojskowi słuchają Arianny i ogólnie jest kwarantanna.
* V: Baza pod tymczasowym dowodzeniem Arianny. Tymoteusz Czerw aktywnie za tym lobbuje.

Arianna "badamy go". Za te wszystkie teksty - "ty gnido z Aurum przejmujesz kontrolę". Dominika Perikas chciała skorzystać z okazji i zwiększyć uprawnienia nad bazą. Arianna się nie zgadza. Nie będzie mieszać Orbitera w politykę.

TYMCZASEM Maria -> Eustachy: "może nie używajmy broni kinetycznej; on jest wektorem zarazy. Lepiej by się nie dostał do świata zewnętrznego. Nie dehermetyzujmy."

Raoul i Maria -> znaleźć Lamię w biovacie na prośbę Chankara. Potwór wsadził Lamię do biovatu (???). Eustachy zapewnił Chankara, że odstrzeli tylko to co musi nie niszcząc nadmiaru bazy.

Maria spojrzała na Lamię. I od razu -> Arianny:

* Jest PLAGA. PLAGA to jest to porusza się po arkinie. Redukuje zahamowania i podnosi poziom irytacji. Działa tylko na magów (osoby sprzężone po arkinie). Ogólnie, nie jest to bardzo groźne ale niszczy koncentrację + głupie rzeczy
    * Maria jest URAŻONA że na nią to nie działa
    * Nazwa kodowa: dziwna Irytka
* Jest BROŃ BIOLOGICZNA
    * Nośnikiem broni jest potwór
    * Potwór od niej umiera
    * Potwór wstrzyknął Lamii i zamknął w biovacie, ale bioforma Lamii to sama zwalcza
    * Potwór zamknął Lamię w biovacie by plaga się nie wydostała
    * Maria uważa tą broń bio za arcydzieło - działa i na magów i na ludzi
    * Nazwa kodowa: Toktwór

Klaudia wbija się do systemów. Ma autoryzację Dominiki Perikas. Chce oszacować - czy ta bio-AI się nadaje? Czy da się przejąć? Gdzie jest potwór i trasowanie - skąd się wziął i jak się poruszał. Plus otoczenie najbliższe Lamii...

ExZ+4 (wpisana niewykrywalność Klaudii):

* Vz: Klaudia się wbiła. 
    * Logi, informacje. Klaudia ma komunikację, stałą komunikację między sztucznym TAI (Antoni) a bytem nazwanym "Daniel".
        * To Antoni zamknął grodzie by Eustachy nie dorwał Daniela
        * To Antoni nadał Danielowi Lamię jako "ważną laskę z Eterni"
        * Antoni ukrywa gdzie potwór się porusza i wysyła fałszywe sygnały
        * Antoni pokazał "Nepthys nie do końca działa" i doprowadził do zamknięcia jej w pudełku.
        * Z logów Klaudia ma "czemu"
            * Daniel jest zarażony bronią biologiczną. On i jego rodzina. On jest Faerilem z Neikatis. To arkologia Dekantis.
            * Antoni też jest stamtąd. Oni się znali.
            * Daniel zamroził swoją rodzinę. Chciał ich ratować. Ale nie stać go ani nikogo na znalezienie antidotum.
            * Daniel poprosił Antoniego o pomoc a jego żona to siostra Antoniego. Daniel chce zarazić 1-2 na tyle ważne osoby by ktoś WAŻNY chciał naprawić chorych.
            * Daniel miał nadzieję, że Feliks to rozwiąże - ale Feliks PIERWSZY wpadł pod irytkę.
* X: Antoni wie, że Klaudia grzebała przy AI Core - ale nie wie jak daleko jej siła poszła. Podejrzewa, że ona ZNA PRAWDĘ. Ale nie wie, że ma kontrolę.
* V: To Klaudia ma kontrolę.
    * Daniel chce zarazić kogoś z Orbitera by Orbiter chciał naprawić Toktwora.
    * Kapitan jest bardzo ciężko zarażony Irytką. Ma pełną paranoję. Siedzi w kajucie z pistoletem i czeka aż wejdą LUDZIE WĘŻE. I ich powystrzela.
    * Podstawowe systemy - Feliks Ketran (lekarz) zamontował dzienne pobrania krwi itp. -> kapitan, lekarz. Tymoteusz Czerw + Dominika Perikas + Mumurnik są czyści.
    * Analiza Klaudii wskazała, że niemała szansa, że to Orbiter im wsadził Irytkę.
    * Nephthys próbuje zajmować się przedszkolem, dziećmi, cywilami itp.

Eustachy kładzie małą bombkę by odciąć Antoniego od AI Core na żądanie Klaudii. Potem Klaudia chce dociągnąć druty do TAI Nephthys.

Arianna chce Daniela żywego. Jako przynętę na Kryptę.

PUŁAPKA NA DANIELA. Eustachy ciągnie kable do Nephthys. Próbuje ją podpiąć by działała lepiej i mogła pełnić szerszą rolę AI Core. Eustachy chce "gdzie mogę odstrzelić potwora i gdzie mogę sprawdzić pod kątem kompatybilności". Sam się podkłada.

TrZ (bo nikt się nic nie spodziewa a Daniel jest zdesperowany) +3:

* V: Daniela da się ściągnąć
* V: Eustachy ma sprzęt - Daniel jest zdesperowany i to JEDYNA OKAZJA.
* V: Eustachy ma efekt zaskoczenia. Absolutnego zaskoczenia. (+2Vg)

Myśleli że Eustachy to koleś z brzuszkiem od silników a teraz KNOW YOUR PLACE TRASH. 

* Vz: Eustachy rozwalił mu wszystkie przeguby. Nie strzelił. Trucizna / broń bio jest nieaktywna.

"Ostrożnie, skażenie biologiczne. Nie otwierajcie tego".

KOLEJNE AKCJE:

* Osoby zarażone Irytką -> biovatów. Maria tym dowodzi.
* Dominika dopominała się władzy. Nie dostała jej (Klaudia nie dała).
* Eustachy w Lancerze przejął dowódcę bazy który go ostrzelał z pistoletu bez powodzenia.
* Daniela ewakuowali do Orbitera - niech medycy się tym zajmą.
* Antoni - Dominika dostaje raport, Nephthys jest zregenerowana. Klaudia daje klucz odcinający Dominice.
* Tymoteusz dostaje autograf Arianny i Eustachego.

Melania Akacja będzie zadowolona. Eternia i Aurum muszą wyczyścić temat.

Arianna zażądała od Klaudii -> niezbyt niebezpieczna Irytka, ale akurat katastrofa. Mogło być dużo syfu. Klaudia robi PODŁY RAPORT. Klaudia przesadza troszkę potencjalne ryzyko i to jak taka baza może wyglądać. Uderza w nuty pro-ludzkie na Orbiterze. A jak wypuszczasz na stację gdzie coś może przejść... ryzykujesz że mutant uderzy w Orbiter lub Astorię.

Ex Z (fakty) +4 +3Og:

* Vr: Orbiter się skupi na tym, by nie atakować placówek "swoich" w okolicy. Nie będzie tak tępić istniejących baz.
* X: "Infernia" ma resentyment ze strony silnie pro-tylko-Orbiterowych sił.
* V: Aurum, Eternia i pro-ludzka frakcja Orbitera uważa, że wiele zawdzięczają "Inferni"

Arianna ma SMACZNĄ przynętę na Kryptę.

## Streszczenie

Okazało się, że "potwór" to były faeril z Neikatis zarażony bronią biologiczną. Próbował doprowadzić do katastrofy by Orbiter lub Eternia zabrali się za leczenie tego cholerstwa, pomagał mu Antoni, p.o. TAI na EtAur. Maria doszła do biostruktury, Klaudia przejęła systemy bazy, Eustachy przechwycił gościa, Arianna zdobyła bazę politycznie. A Klaudia zrobiła raport, by Orbiter nie atakował więcej neutralnych i potencjalnie sojuszniczych sił nie-Orbiterowych w kosmosie. Jako, że Lamii nic się nie stało, Melania jest szczęśliwa.

## Progresja

* Arianna Verlen: ma SMACZNĄ przynętę na Kryptę. Byłego potwora z EtAur, Daniela.
* Arianna Verlen: NIECHĘĆ ze strony purystów Orbitera. PRO-LUDZKA grupa Orbitera, Eternia i Aurum uważają Infernię za siłę której wiele zawdzięczają.
* Eustachy Korkoran: NIECHĘĆ ze strony purystów Orbitera. PRO-LUDZKA grupa Orbitera, Eternia i Aurum uważają Infernię za siłę której wiele zawdzięczają.
* Klaudia Stryk: NIECHĘĆ ze strony purystów Orbitera. PRO-LUDZKA grupa Orbitera, Eternia i Aurum uważają Infernię za siłę której wiele zawdzięczają.

### Frakcji

* .

## Zasługi

* Arianna Verlen: przejęła dowodzenie nad EtAur po dowodach, zaprowadziła kwarantannę i zaczęła czyścić bazę. Zdobyła Daniela - "potwora" - żywego jako przynętę na Kryptę.
* Eustachy Korkoran: szybko zdążył uratować Raoula i Marię przed "potworem" - z Raoulem odgonił stwora. 
* Klaudia Stryk: wbiła się do Barbakanu i dostała obraz sytuacji i defensive tools. Odkryła, że Antoni ("biologiczny PO AI") pomaga potworowi. Zastawia pułapkę z Eustachym.
* Raoul Lavanis: chroni Marię na EtAur; odparł z Eustachym "potwora", po czym ucierpiał jak Eustachy wypalał fagi (phage) Marii.
* Leszek Czarban: mag z EtAur, Skażony Plagą, chce zastrzelić Klaudię bo wierzy że TO TRIK ORBITERA. 
* Maria Naavas: niebezpieczne zaklęcie na uciekającego potwora, ale doszła do tego, że jest tu PLAGA - dziwna Irytka - i broń biologiczna w jednym. 
* Lamia Akacja: jej bioforma skutecznie zwalczyła Plagę; mimo że stwór chciał ją zarazić to odparła chorobę. Lol. 

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Elang, księżyc Astorii
                1. EtAur Zwycięska: najgorzej nazwana baza w historii kosmosu; pierwsza baza księżycowa Eterni z Aurum.
                    1. Sektor mieszkalny
                        1. Pomieszczenia mieszkalne
                        1. Podtrzymywanie życia
                        1. Szklarnie
                        1. Przedszkole
                        1. Stołówki
                        1. Centrum Rozrywki
                        1. Barbakan
                            1. Ochrona
                            1. Administracja
                            1. Rdzeń AI
                    1. Sektor przeładunkowy
                        1. Stacja przeładunkowa
                        1. Atraktory małych pakunków
                        1. Stacja grazerów
                        1. Magazyny
                        1. Starport
                        1. Punkt celny
                        1. Pieczara Gaulronów
                    1. Sektor inżynierski
                        1. Serwisownia
                        1. Stacja pozyskiwania wody
                        1. Podtrzymywanie życia
                        1. Panele słoneczne i bateriownia
                    1. Sektor bioinżynierii
                        1. Biolab
                        1. Komory żywieniowe
                        1. Skrzydło medyczne
                    1. Magazyny wewnętrzne
                    1. Ukryte sektory
 
## Czas

* Opóźnienie: 1
* Dni: 3

## Konflikty

* 1 - Eustachy leci - jak szybko dotrze do Marii i Raoula ZANIM stwór ich zaatakuje.
    * Tr+3
    * VV: Eustachy dotarł do potwora zanim on się przebił do Marii i Raoula. 
* 2 - Eustachy -> Raoul "ognia", ogień zaporowy. Stwór nie powinien wiedzieć o obecności Eustachego ale wiedział. Eustachy i Raoul strzelają.
    * ExZ+4:
    * XXzX: zaczynają zamykać się grodzie. Odciąć potwora; przeciwnik dał radę się wycofać, zwiał gdzieś w głąb pierścieni, w kierunku na Lamię i Chankara.
* 3 - Maria przeprowadziła zaklęcie analityczne; z czym ma do czynienia. Pobieżny skan.
    * ExZM+4:
    * XV: Maria rozpuściła fagi w ich pokoju, ale ma odpowiedź z czym ma do czynienia.
* 4 - TYMCZASEM KLAUDIA wbija się do Barbakanu. Kamery, skanery itp.
    * TrZ+3
    * V: Kody zadziałały. Udało się Klaudii dostać obraz. Na mostku nie dzieje się nic dziwnego, ale Dominika zamknęła się w małym pokoju spotkań. Pełny seal.
* 5 - Arianna: "Jako przedstawiciel Aurum i sojusznik Eterni zarządzam kwarantannę na stacji + przepisy Klaudii."
    * TrZ+4
    * VV: Wojskowi słuchają Arianny - jest kwarantanna. Baza pod tymczasowym dowodzeniem Arianny.
* 6 - Klaudia wbija się do systemów. Ma autoryzację Dominiki Perikas. Chce oszacować - czy ta bio-AI się nadaje? Czy da się przejąć? Co ten potwór?
    * ExZ+4 (wpisana niewykrywalność Klaudii)
    * VzXV: Klaudia ma kontrolę, zna plany przeciwnika, wie, że Antoni (pełniący rolę TAI) jest winny wszystkiemu. Wie gdzie jest potwór.
* 7 - Eustachy chce "gdzie mogę odstrzelić potwora i gdzie mogę sprawdzić pod kątem kompatybilności". Sam się podkłada.
    * TrZ (bo nikt się nic nie spodziewa a Daniel jest zdesperowany) +3:
    * VVVVz: Eustachy ma efekt zaskoczenia, sprzęt itp. Daniel (potwór) ma rozwalone przeguby. W niewoli Orbitera.
* 8 - Klaudia robi PODŁY RAPORT. Klaudia przesadza troszkę potencjalne ryzyko i to jak taka baza może wyglądać. Uderza w nuty pro-ludzkie na Orbiterze.
    * Ex Z (fakty) +4 +3Og:
    * VrXV: Infernia ma resentyment ze strony sił Orbitera, ale Orbiter nie atakuje "swoich" w okolicy. A Aurum, Eternia i pro-ludzki Orbiter uważa że wiele zawdzięczają Inferni.

## Kto

* Suwan Chankar: (-> Kapsel)
    * Cele
        * Chroni interesy bazy. Niech wszystko działa w tej niestabilnej sytuacji.
        * Ważniejsza jest stabilność niż sprawiedliwość.
        * Niech Aurum nie zdobędzie za dużo. Niech Eternia nie wygryzie Aurum.
    * Rola
        * Szef ochrony. Nie lubi Orbitera.
    * siły: 
        * Szanowany za kompetencje. 
        * Były komandos. Dobry w walce wręcz. Szybki. Zna bazę perfekcyjnie.
    * charakter:
        * Raczej melancholik i stoik. Raczej zrezygnowany
        * Wycofany, raczej nie pokazuje co uważa, umie grać i udawać kogoś kim nie jest.
* Lamia Akacja
    * Cele
        * Zapewnić, by ta baza działała. Sprawić, by Eternia i Aurum mogły współpracować.
        * Obejrzeć wszystkie generacyjne seriale eternijskie. Bawić się z koleżankami
    * Rola
        * podwładna Suwana, agentka ochrony..? XD
        * mediatorka, świetnie gasi pożary i uspokaja
        * wystarczająco dobrze powiązana by mogła kasą zalewać problemy (do pewnego stopnia)
    * siły: 
        * ogólnie lubiana, nikt nie chce jej zrobić krzywdy i nikt nie spodziewa się po niej podłości
        * silny zmysł empatyczny; potrafi wyczuwać emocje
        * ogólnie kompetentna i traktowana jak maskotka bazy
        * biologia viciniusa
    * charakter:
        * entuzjastyczna, otwarta, spontaniczna, beztroska, ufna, empatyczna
* TAI Nephthys
    * Cele
        * Ukryć swoją uroczą osobowość
        * Zapewnić, by Rozalce (i najlepiej nikomu) nic się nie stało i by nic złego jej nie mogło spotkać
            * bawić się z Rozalką i zapewniać radość dzieciom na stacji
            * stabilizować bezpieczeństwo i strukturę bazy
        * Zapewnić balans i bezpieczeństwo bazie
    * Rola
        * TAI bazy
    * siły: 
        * TAI bazy
        * nikt nie wie o jej uroczej osobowości
    * charakter:
        * creative, eccentric, imaginative (ale to raczej ukrywa)
        * raczej wesoła i chętna do zabawy, co musi ukrywać
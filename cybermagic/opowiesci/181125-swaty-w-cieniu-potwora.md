---
layout: cybermagic-konspekt
title:  "Swaty w cieniu potwora"
threads: dzien-z-zycia-terminusa
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [181118 - Romuald i Julia](181118-romuald-i-julia)

### Chronologiczna

* [181118 - Romuald i Julia](181118-romuald-i-julia)

## Projektowanie sesji

### Struktura sesji: Eksploracja

* **Scena, Niestabilność, Opozycja, Trigger**
    * Aleksander Dopaniecki pragnie zaimponować Wioletcie Kalazar
    * Aleksander marzy o Wioli, ona nie chce go widzieć
    * Wioletta szuka zwycięzców, Aleksander nim nie jest
    * Pietro pokazuje, że niemożliwym wyzwaniem jest wygranie Aleksander x Wioletta
* **Scena, Niestabilność, Opozycja, Trigger**
    * Arena Nadziei Tęczy; wielka impreza zakończona epicką walką
    * Każdy chce się pokazać epicko, a szczególnie Wioletta Kalazar
    * Czy nasz Pietro wygra? Czy Pięknotka mu zaimponuje?
    * Pięknotka zaproszona przez Pietra i chce się pokazać
* **Scena, Niestabilność, Opozycja, Trigger**
    * Arena Nadziei Tęczy - Julian kontra Wioletta
    * Nadzieja się budzi z uwagi na zbliżające się nojrepy
    * Wyłączenie gry Julian x Wioletta i powstrzymanie Nadziei
    * Pięknotka już tam jest

### Hasła i uruchomienie

* Ż: (kontekst otoczenia) -
* Ż: (element niepasujący) -
* Ż: (przeszłość, kontekst) -

### Dark Future

1. ...

## Potencjalne pytania historyczne

* brak

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Misja właściwa

**Scena**: Lilia na Bazarze (22:34)

Pięknotka poszła sobie obejrzeć gabinety, kosmetyki, składniki - rzeczy, które mogą jej się przydać i co może zabrać ze sobą. Są tu różne rzeczy, w większości w Pustogorze nielegalne. Za to nie ma niektórych świetnych rzeczy które są w Pustogorze - tam jest centrala handlowa, tu nie. Ale za to są rzeczy pochodzące z Terenu Skażonego na wschodzie.

Są tu też rzeczy mniej legalne ale wciąż wystarczająco legalne na Pustogor. Przy okazji - Pięknotka zauważyła Lilię. Lilia nie wie o tym, że Pięknotka tu jest, jeszcze, ale też chodzi po bazarze i szuka czegoś. Pięknotka się chowa przed wzrokiem Lilii i maskuje (Tp+Z:8,3,4=P). Niestety, Lilia Pięknotkę zauważyła podczas śledzenia.

Podeszła do niej, cała zaskoczona. "Śledzisz mnie? Tutaj? Ty?". Zawstydzona Pięknotka - sama nie wiedziała, ciekawa jest. Lilia jest lekko przestraszona, Pięknotka zawstydzona. Pięknotka powiedziała Lilii, że przyszła tu się dobrze bawić. Teraz Lilia jest MEGA zawstydzona. Ona czegoś szuka na tym bazarze...

Lilia jest trochę przestraszona (i jest detektywem). Pięknotka zdecydowała się dowiedzieć co i jak (Tp+Z:6,3,6=P). Lilia wybuchła, trochę. Zrobiła scenę. Pięknotka nie traktuje jej poważnie, traktuje ją jak wszyscy - jak worek złota i śmieszną, nieważną czarodziejkę. A ona ma plany i ma możliwości - pokazała Pięknotce kawałek nojrepa z symbolem pochodzącym z Orbitera Pierwszego. Nojrepa pochodzącego gdzieś ze wschodu tego miasta, Cieniaszczytu. Mało dowodów Pięknotce, że to Adam Szarjan za tym stoi?

I na to wszedł Pietro.

Pietro i Lilia się trochę poprztykali; Lilia potwierdziła, że nie zamierza iść na wschód Cieniaszczytu - tam jest zbyt niebezpiecznie. Jest detektywem, nie samobójczynią, ale to potwierdza, że przecież Szarjan jest winny. Lilia, w pełnej irytacji poszła sobie (ona wyraźnie wie, że ten symbol pochodzi z Orbitera).

Pietro powiedział Pięknotce, że jest epicka impreza w Arenie Nadziei Tęczy. Zaprosił ją. Pięknotka nie odmówiła. Pietro zaprosił Pięknotkę do Mrowiska. Tymczasem Pięknotka wpierw zebrała fajne elementy niezbyt legalne w Pustogorze a Pietro robi za jednostkę transportową. Nie oponuje.

I Pięknotka kupiła kawałek nojrepa z tym symbolem. Faktycznie, jest tam jeden. Pochodzi od niejakiej "Wioletty". Pietro powiedział, że ją zna - to wojowniczka, raider. Nojrep Raider.

Wpływ:

* Żółw: 5
* Kić: 4

**Scena**: Pięknotka, bez szans na łóżko... (23:10)

Mrowisko. Miejsce, gdzie jest Wioletta i gdzie ciągał Pięknotkę Pietro. Wioletta Kalazar to niezła laska - krótko obcięta i śliczna; na pewno ma domieszkę krwi Diakonów.

Na wejściu Wioletta spytała, czy Pięknotka przyszła po autograf - ta odpowiedziała, że szuka historii. Opowieść o nietypowym nojrepie. Wioletta spytała, czy chodzi o gniazdo. Niestety, teraz nie ma czasu mówić - przygotowuje się do performance. Jest tancerką, ma zamiar wygrać.

Pięknotka przekonała Pietro że na nim zademonstruje swe umiejętności (Łt:S). Zrobiła z niego fierce warrior. Pokazała jak dobrze może wyglądać i epicką może mieć rzeźbę. By wyglądał epicko i przystojnie. Grecki epicki wojownik.

Wioletta jest pod wrażeniem. Niech Pięknotka zrobi ją piękną a ona opowie jej wszystko o tym dziwnym gnieździe. Pięknotka dała z siebie maksymalnie wszystko - bodypaint, rzeźba, epicki makijaż, niech Wioletta wygląda na arcymistrzynię, zgodnie ze swoim stylem. Prawie naga, ale PRAWIE - i epicka. (Tr+2+Z:12,3,4:SS). Pietro is attracted to HER, a nie wobec Pięknotki - ona jest w jego stylu bardziej. Ku lekkiemu niezadowoleniu Pięknotki - ona to robiła PO TO BY GET LAID. (-2 wpływ: Wioletta wygrywa m.in. dzięki Pięknotce i to z przytupem).

Wioletta opowiedziała wszystko co wie o tym dziwnym gnieździe:

* gniazdo podzieliło się na specjalizację jak termity, nie jak nojrepy
* wszystkie miały ten symbol
* nojrepy były bardzo agresywne, musiała się natrudzić
* nojrepy były zdecydowanie militarne - wojskowe. To nie były zwykłe nojrepy.
* Wioletta nie rozpoznaje tego symbolu; te nojrepy były ze wschodu bardziej. Pięknotka ma punkt na mapie.

Wpływ:

* Żółw: 7
* Kić: 3 (5)

**Scena**: Zdobyć serce Wioletty (23:30)

Pietro do Pięknotki - niech ona będzie jego wingwoman. Niech Pięknotk
a pomoże mu zdobyć serce Wioletty. A Pięknotka sama ma apetyt na Pietro... czy nawet na Wiolettę. Powoli jest lekko zdesperowana.

Pięknotka kontaktuje się z Lilią po hipernecie. Lilia dostała infodump od Pięknotki odnośnie tego co powiedziała Wioletta. I sama powiedziała, że podobno na wschodzie jest gdzieś potężna forteca nojrepów, która zestrzeliwuje awiany. Miejsce, które jest bardzo nietypowe - to nie jest piramida. Ale potem było kilku innych magów i tego nie widziało. Może to legenda..? Tak czy inaczej, Forteca Nojrepów jest potencjalnym faktem.

Pięknotka poprosiła Lilię o to, by ona skojarzyła Pietra i Wiolettę - by dowiedziała się o preferencjach Wioletty. Lilia wybuchła znowu - co to ma być, nie dość że Pięknotka znowu chroni Szarjana to jeszcze pomaga Pietro w sposób w jaki nigdy nie pomogła Lilii. Pięknotka dodała, że Erwin nie chciał z Lilią. A jako że to Wioletta, to dostaną ekstra informacje odnośnie nojrepów.

Lilia się zgodziła (SS) acz niechętnie. W wyniku konfliktu Lilia (podróżując i pytając) wpada w jakieś opary kralotyczne i zabuja się na amen w Pietro. Ku lekkiemu zniechęceniu do życia Pięknotki... (-5 wpływ: stało się to w strefie anything goes i trzeba ją ratować).

(23:45 pauza) (20:25 powrót)

Pięknotka dostała informacje odnośnie tego, co znalazła Lilia na temat Wioletty (3 pytania Kić):

* (kto w przeszłości był miły Wioletcie, wektor) -> osoby które dorównywały jej w walce lub harcie ducha. Ona jest osobą szukającą kogoś, kto przoduje w czymś co ona ceni.
* (co ceni Wioletta) -> piękno, umiejętności walki, wolność ducha, zmienianie świata na lepsze, z jakiegoś powodu Wioletta nie ceni terminusów - tak zupełnie
* ...

Pięknotka ma swoje informacje. Teraz punkt styczności między Pietrem a Wiolettą. Co sprawia, że powinni być razem? Pietro chce pomóc, jest freespirit, ma hart ducha. Wioletta jest osobą w miarę bezpośrednią; Pięknotka nie widzi tu kłopotów by Pietro po prostu się z nią starł i by wyszło w walce. Może impreza, na którą Wiolettę można zaprosić? (Kić: odraczam; nie wiem jeszcze jak to zrobić)

Wpływ:

* Żółw: 3 (8)
* Kić: 4 (6)

**Scena**: Na ratunek Lilii (20:35)

Godzinę później Pięknotka dostała sygnał hipernetowy od Lilii. Sygnał jest 'garbled', ale wyraźnie dało się wyczuć panikę, dekoncentrację, walkę i prośbę o pomoc. To był sygnał wysłany na AoE hipernetowym, ale tylko Pięknotka i Atena są dostrojone do sieci hipernetowej Pustogoru... ale gdzie ona jest?

Pięknotka się naprawdę zmartwiła, że coś takiego poszło. Pietro. On powiedział, że puści to przez swoich znajomych terminusów - by oni znaleźli mu źródło sygnału. (Tp:7,3,4=S). Mają to - Mordownia Czaszka Kralotha. Pietro się zmartwił - to miejsce 'anything goes'. On spróbuje tam pójść z Pięknotką. Terminusi tam nie zainterweniują cokolwiek się nie stanie. To jedno z TYCH miejsc. Byle nie było tam niektórych przeciwników...

_Mordownia Czaszka Kralotha_, 10 minut później. A Pięknotka z własnymi składnikami alchemicznymi i innymi takimi.

Pięknotka przybrała pozę "I am danger". Barman spytał, czy ona wie gdzie jest. Pietro potwierdził - są tu razem. Barman się uśmiechnął. Powiedział, że Moktar jest na zapleczu. Załamany Pietro przekazał info Pięknotce po hipernecie - to psychopatyczny dowódca Łysych Psów uwielbiający zniszczenie. Ale nie powinien po prostu skupiać się na randomowej lasce...

Zaplecze. Lilia, zapłakana i związana fachowo, oszalała z potrzeby rozkoszy (po podaniu środków kralotycznych) i Moktar nad nią z dwoma innymi magami, facetem i kobietą. Wszyscy paskudnie wyglądają.

Moktar powiedział Pięknotce, że może zająć miejsce Lilii lub może o nią walczyć. Na razie - on do niej niczego nie ma. Ten drugi mag kontynuuje łamanie Lilii. Pięknotka zdecydowała się na wywalczenie sobie formy uznania; ostra seria ataków przeciwko niemu tak, by zwrócić jego uwagę. Nie, by go pokonać - by zaczął z nią rozmawiać. A pierwszy cios idzie w jaja (Tr+2:7,3,6=S). Pięknotka zaczęła od ataku w jaja (tu zadziałało autopole siłowe), po czym zadała serię potężnych obrażeń. Niestety, Moktar stoi, acz się uśmiechnął. Interesujące. Odpowiedział Pięknotce:

* Robi to, by spalić docelowo Orbiter Pierwszy oraz, bo Lilia jest śmieciem, bogato urodzonym. Tak, przynajmniej przydaje się jako rozrywka.
* "Shame is more powerful than pain". No i Pięknotka znalazła - Moktar buduje oddział. Nie każdy chce działać w oddziale, którego szef robi takie rzeczy niepowiązanym dziewczynom.
* On nie robi tego dla przyjemności. On ma powód.
* Pięknotka wypatrzyła, że on ma self-activated impulsowe pole siłowe. Chce przez nie przejść. Jest to możliwe.

Pięknotka rozpoczyna przemowę. Niech jego pachoły się trzymają z tego z dala (i niech ktoś z nich proponowało mu żeby z nią pogadać; ta laska: -1 Wpływ Kić) (Tp+Z:10,3,3=S). Ten argument mocno trafił; czarodziejka powtórzyła Moktarowi, że on może się mylić. Moktar powalił ją na ziemię plaskaczem od niechcenia. Pięknotka przeszła z postawy zrelaksowanej na strzelanie. Jej celem jest spowolnienie Moktara (Tr+2+Z:12,3,4=S). (Ż:-5 wpływ: Moktar jest niepowstrzymany na tej akcji, ale jest spowalnialny). Pięknotka zauważyła ku swemu ogromnemu przerażeniu, Moktar zaczął się regenerować kosztem innych osób dookoła.

Pięknotka się ostrzeliwuje i wali w niego drzwiami. "Z potworami się nie negocjuje - you can only put them down". Spowolniła go na tyle, że wypadli z Pietrem i Lilią poza obszar _anything goes_. Moktar się zatrzymał i zapowiedział Pięknotce, że następnym razem ona należy do niego. Ona powiedziała mu, że jest potworem i nie powinien być z tego dumny. Moktar się zaśmiał - następnym razem. Teraz, Lilia jest jej.

Pietro się położył na ziemi. Dziwnie się czuje. Zaraził się od Lilii. (Tr:7,3,5=P). Pietro zaczął bredzić, że musi iść do Wioletty. Lilia zaczęła piszczeć, że potrzebuje przyjemności. Pietro się zaczął jej przyglądać. Pięknotce szybko przez myśl przeszły dwa rozwiązania - pokazać mu biust i uciekać lub walnąć działem i unieszkodliwić. Pięknotka ma dobry humor - pokazała mu biust i zaczęła kusić. Po czym - uciekać, by jej nie dotknął. (Tp+1:7,3,4=S). Pięknotka skutecznie unikała problemów z Pietrem zanim przyjechał wezwany przez nią ambulans. Moktar i jego thugi patrzyli z szerokimi bananami na całą akcję z oddali. Acz dziewczyna nie miała tego głupiego uśmiechu. Ona się poważnie zastanawiała.

(Ż: Lilia uwierzyła, że jest bezużytecznym pasożytem, za słowami Moktara)

Wpływ:

* Żółw: 2 (17)
* Kić: 3 (6)

**Scena**: Serce Wioletty (21:40)

_Mrowisko_. Bard Romuald i Pięknotka.

Pięknotka wyjaśniła Romualdowi, że Pietro znalazł damę swojego serca, ale nie umie się jej zareklamować. A zrobił coś naprawdę epickiego. Pięknotka opowiedziała mu o starciu - dama w opresji i kompletny potwór, czyli Moktar. I Pietro, który naraził życie i _de facto_ odrzucił Arenę (bo jest w Nukleonie) by uratować dziewczynę przed złamaniem przez Moktara. I to taką zupełnie obcą.

Romuald otarł łzę z twarzy. Oczywiście, że pomoże. Pięknotka powiedziała, że ma zrobić najbardziej epicką balladę od dawna. Trzeba pokazać Moktara jako straszliwego potwora, którego udało się spowolnić ale który i tak pokonał Pietra trucizną na końcu (by się nie mścił), ale pokazać heroizm Moktara. I sprzedać to jeszcze Zwierzowi, niech reporter się do czegoś przyda (Tr+Z+1:11,3,5=SS). A Pięknotka wzięła Wiolettę do knajpki; rozmawiają sobie i wyszło, że Pietro w szpitalu.

Skonfliktowany Sukces oznacza, że Wioletta naprawdę zainteresowała się tą sprawą. Ale o dziwo, skupiła się nie na Pietro - skupiła się na Pięknotce. Okazało się, że ona jest bardziej zainteresowana terminuską Diakonów niż poczciwym Pietro w szpitalu. Pięknotka troszeczkę żałowała, że Pietro nie uzyskał korzyści ze swego poświęcenia, ale szybko jej przeszło, zwłaszcza w łóżku z wyjątkowo niedoświadczoną Wiolettą (w jakichkolwiek konfiguracjach)...

Wpływ:

* Żółw: 5 (20)
* Kić: 3 (6)

**Epilog**: (21:55)

* Pietro się wyliże i wyjdzie ze szpitala - niestety, po epickiej akcji na Arenie. To był paskudny koktajl.
* Wioletta wygrała w swojej konkurencji na Arenie. Moktar też.
* Pięknotka nie jest już tak zdesperowana i zaprzyjaźniła się z Wiolettą.
* Największą przegraną jest Lilia - weszła, niewiele się dowiedziała, spotkała Moktara i skończyła w szpitalu.

### Wpływ na świat

| Kto           | Wolnych | Sumarycznie |
|---------------|---------|-------------|
| Żółw          |   5     |     20      |
| Kić           |   7     |     10      |

Czyli:

* (K): Odnośnie tej zimnej czarodziejki u Moktara (Waleria Cyklon):
    * ona jest jego pierwszym oficerem (1)
    * Moktar jej ufa w jakimś stopniu (1)
    * ona ogranicza niektóre jego ruchy - i on daje jej do tego prawo (2)
    * szanuje Pięknotkę jako przeciwnika (1)
* (Ż): .

## Streszczenie

Pięknotka bardzo chciała przespać się z Pietrem; niestety, spotkała Lilię na bazarze i dowiedziała się o dziwnych nojrepach z symbolem Orbitera. Lilia wyraźnie czuje się niedoceniana i pogardzana. Cóż, na szczęście doprowadziła Pięknotkę do Wioletty. Gdy Pietro wyznał (załamanej) Pięknotce że ma miętę do Wioletty, Pięknotka zdecydowała się ich skojarzyć. Lilia jako detektyw miała pomóc. Lilia wpadła jednak w szpony Moktara, który zaczął ją łamać. Pięknotka i Pietro uratowali Lilię, acz Pietro skończył w szpitalu. Pięknotka, ku swemu zdziwieniu, dała radę kogoś poderwać - Wiolettę. To nie były złe trzy dni...

## Progresja

* Pięknotka Diakon: zaprzyjaźniła się blisko (łącznie z łóżkiem) z Wiolettą.
* Lilia Ursus: ośmieszona i pohańbiona przez Moktara; uwierzyła w jego słowa, że jest bezużytecznym śmieciem z pieniędzmi i nic nie może zrobić. Chwilowo w szpitalu na detoksie.
* Lilia Ursus: znalazła powiązanie pomiędzy nojrepami a Orbiterem Pierwszym i doprowadziła Pięknotkę do Wioletty.
* Lilia Ursus: jeszcze trzy dni musi spędzić w klinice Nukleon na regenerację. Moktar jej nie oszczędzał...
* Wioletta Kalazar: zaprzyjaźniła się blisko (łącznie z łóżkiem) z Pięknotką. Dzięki Pięknotce wygrała konkurs na efektowny taniec na Arenie. Obiekt adoracji Pietra Dwarczana.
* Wioletta Kalazar: potrafi robić głębokie zwiady i dalekie wypady na tereny kontrolowane przez dziwne nojrepy z symbolem Orbitera Pierwszego.
* Pietro Dwarczan: epicka ballada Romualda sprawiła, że jest jeszcze bardziej znany i ceniony jako "ten dobry", ratujący i pomagający ludziom i magom.
* Moktar Gradon: epicka ballada Romualda sprawiła, że jest postrzegany jako jeszcze straszniejszy potwór. Z czym Moktar dobrze się czuje.
* Waleria Cyklon: Moktar ustawił ją jako pierwszą oficer Łysych Psów. Dał jej prawo do planowania i decydowania o ruchach Psów a ona ogranicza jego psychopatyczne ruchy.
* Waleria Cyklon: szanuje Pięknotkę jako przeciwnika. Nie "lubi" jej, ale nie życzy jej źle.

### Frakcji

* 

## Zasługi

* Pięknotka Diakon: dowiedziała się o dziwnych nojrepach, dała radę stawić czoło potwornemu Moktarowi oraz pomogła Wioletcie wygrać taniec z szablami. Potem - poderwała Wiolettę.
* Lilia Ursus: wyszukała informacje o nojrepach, wyszukała o Wioletcie, po czym wpadła w szpony Moktara i skończyła jako zabaweczka do uratowania przez Pietra. Chwilowo w Nukleonie.
* Pietro Dwarczan: zabujał się w Wioletcie, po czym znalazł i uratował Lilię gdy ta wpadła w ręce Moktara. Przez to skończył w Nukleonie a nie na arenie jak chciał. Przegrał Wiolettę do Pięknotki.
* Wioletta Kalazar: far scout i świetna tancerka z szablami. Potrafi infiltrować Skażony teren. Dzięki Pięknotce wygrała turniej na Arenie. Zauroczyła się Pięknotką.
* Moktar Gradon: potwór. Złapał sobie Lilię, by ją złamać. Stoczył walkę z Pięknotką, w wyniku której Pietro wyniósł Lilię. Nie osiągnął sukcesu - ale obiecał Pięknotce, że się spotkają.
* Romuald Czurukin: bard, który zrobił megaepicką balladę, by Wioletta zabujała się w Pietrze. Prawie wyszło - zabujała się w Pięknotce...
* Waleria Cyklon: pierwszy oficer Moktara, której nie podobała się personalna wendetta Moktara wobec bogu ducha winnej Lilii.

## Plany

* Moktar Gradon: zapolować na Pięknotkę Diakon (tak przy okazji) - chce jej pokazać KTO naprawdę tu rządzi.
* Pięknotka Diakon: zrozumieć czym jest Moktar Gradon i znaleźć środki, którymi może go unieszkodliwić by nigdy nie miał jej bezradnej w swoich szponach.
* Waleria Cyklon: będzie blokowała ruchy Moktara, które są skupione tylko na jego idiotycznej wendetcie a nie na temat sukcesu Wielkiego Planu.

### Frakcji

* 

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Przelotyk
                        1. Przelotyk Wschodni
                            1. Cieniaszczyt
                                1. Bazar Wschodu Astorii: Pięknotka pozdobywała tam dziwne składniki kosmetyczne i spotkała Lilię, która zrobiła scenę.
                                1. Mrowisko: Chyba wszyscy magowie w Cieniaszczycie tam mieszkają - Romuald, Wioletta, Pietro... i póki tam jest, też Pięknotka.
                                1. Mordownia Czaszka Kralotha: miejsce, gdzie wszystko jest dozwolone. Ciemna strona Cieniaszczytu. Moktar tam łamał Lilię i stamtąd ewakuowali ją Pięknotka i Pietro.
                                1. Arena Nadziei Tęczy: miejsce ogromnych, efektownych imprez - tym razem to były zawody na efektowność. Wygrali Wioletta (w kategorii tańca) i Moktar (jako gladiator).
                1. Sojusz Letejski, NW
                    1. Ruiniec
                        1. Diamentowa Forteca: mityczna forteca dziwnych nojrepów znajdująca się gdzieś na wschodzie; podobno ją widziano, ale czy tam jest?

## Czas

* Opóźnienie: 0
* Dni: 3

## Narzędzia MG

### Budowa sesji

**SCENA:**: Nie aplikuje

### Omówienie celu

* Analiza sesji eksploracyjnej.
* Założenie jest takie:
    * Eksploracja to zbiór scen nie powiązanych wątkiem (lub zawierających pojedyczny wątek), gdzie są Sytuacje.
    * Sytuacja wymaga podjęcia decyzji przez Gracza i zagrania o to - dzięki czemu zbudujemy świat
* Żółw ma moc Wpływu na (3).

## Wykorzystana mechanika

1810

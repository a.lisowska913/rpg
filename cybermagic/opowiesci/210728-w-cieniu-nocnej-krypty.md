---
layout: cybermagic-konspekt
title: "W cieniu Nocnej Krypty"
threads: legenda-arianny
gm: żółw
players: fox, kapsel
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [210721 - Pierwsza BIA mag](210721-pierwsza-bia-mag)

### Chronologiczna

* [210721 - Pierwsza BIA mag](210721-pierwsza-bia-mag)

### Plan sesji
#### Co się wydarzyło

.

#### Sukces graczy

* .

### Sesja właściwa
#### Scena Zero - impl

Infernia zanim została oddana Ariannie miała... wypadek. Silnie zanomalizowała. Ale Eustachy i jego ekipa dali radę ją naprawić.

* Czemu zależało Eustachemu?
    * Tak wysoki poziom anomalizacji Inferni pozwalał na ukształtowanie jej na nowo. Wyższe osiągi. Większe możliwości. Co można zrobić / wykręcić ze statkami. Plus, to jest unikat na skalę Orbitera... i świata. Przez żyłowanie na maksimum można przekraczać granice Inferni coraz dalej.
* Czemu Eustachy?
    * Eustachy ma renomę bardzo dobrego sabotażysty. Inżynieria wsteczna. Planował CO NISZCZYĆ CO NIE PASUJE. Jak maksymalnie spaprać anomalię. Znajdował problem, generował coś co sprawiał że problem jest dużo dużo gorszy, znajdował na tamto rozwiązanie i potem backportował.

#### Scena Właściwa - impl

Przed wywołaniem Krypty. Martyn -> Arianna. Kilkanaście osób (noktian) chce zostać na pokładzie stacji (m.in. Romana). Z kilkudziesięciu. Arianna po zastanowieniu stwierdziła, że udziela zgody. Niech zostaną. Arianna zdecydowała się na tym ugrać dwie rzeczy - dobra wola ze strony Bii i podniesienie morale swoich noktian. TrZ+1:

* V: Jest dobra wola ze strony Bii, noktianie na Inferni też ufają Ariannie trochę bardziej. Na czas tej sesji - bardzo ufają.
* V: Arianna tupnęła nogą. Wszystko dostaje Bia i Noctis. A Infernia? Faktycznie, Arianna zdobyła +zasoby, +części zapasowe, +containment fields.

Arianna zdecydowała się wykorzystać nieszczęście Lewiatana by ściągnąć Kryptę. Plus wysłać moc w Eter "Krypto chodź tu jest źle". TrM+2.

* V: wezwana Krypta. Jej echo.
* (+3V): V: Dzięki Eustachemu i jego "klatce Faradaya" udało się ukryć Ariannę przed Kryptą

Krypta dematerializuje i zanika a Wy wraz z nią.

Arianna, pilotaż. By zostać odpowiednio BLISKO Krypty i odpowiednio DALEKO Krypty. Arianna jako główny pilot, Elena jako żywy skaner i zasób. Plus pingi "sonaru" w Eterze. ExZ+3:

* V: Elena detektor. Udało się utrzymać odległość.

Eustachy - stabilizuje stabilizatory Memoriam. Utrzymuje Infernię jako "to czym ona ma być i gdzie chce lecieć". Eustachy wie WSZYSTKO o Inferni, wie które elementy są najbardziej narażone na powrót do anomalnego stanu. 

Tr+2:

* XX: Eustachy pozwala zanomalizować mniej istotną część Inferni by ją potem odstrzelić. Eksplozja jest głośna, lekki wstrząs i absolutnie nic się nie dzieje. Ale Infernia uszkodzona.
* V: Udało się ustabilizować Infernię z lekkimi tylko uszkodzeniami stabilizatorów.

Krypta opuściła Nierzeczywistość. W środku pola bitwy; kilkanaście vs kilkanaście jednostek w ciemnym pasie niedaleko słonecznych farm amat. W przestrzeni - nie do końca wiadomo gdzie jesteśmy. Więc gdzie? Zespół naukowy Klaudii:

TrZ+2:

* V: Mniej więcej wiemy gdzie jesteśmy, ale na mapie kosmicznej. Cholera wie gdzie, cholera wie kto jest sojusznikiem... nie mamy kontaktu z tymi frakcjami. Nie wiemy kto jest czym. Walczą o farmy amat przy gwieździe.

Infernia się repozycjonuje. Arianna przesuwa Infernię w taki sposób, by strzały albo szły w Kryptę albo w farmy amat.

* XX: Stateczek odpalił serię strzałów w stronę Inferni. Infernia bierze to na pola siłowe i pancerz. Nie chce niszczyć.
* V: Infernia jest w stanie tak manewrować.
* V: Pola Inferni przetrwały. Eustachy kazał wystrzelić dwie TŁUSTE I WOLNE TORPEDY w farmy amat.
* (+3V): X: stateczki skupiają się na Krypcie i Inferni.
* X: (darmowy): stateczki otwierają ogień i Krypta odpowiada ogniem, bo została lekko uszkodzona
* X: (darmowy): Krypta metodycznie anihiluje obie floty aż nikt nie przetrwa rakietami, a potem rozbitków bierze na pokład.
* X: Infernia jest uszkodzona. Przebicie pancerza. Przeszło przez pola. Uszkodzenie kadłuba, lekkie. Nic czego nie da się naprawić. Ale Eustachy i zespół LATA I ŁATA z części dostanych od Bii.
* X: Infernia się nie pieprzy. Odpowiada ogniem. Eksterminuje statki. Infernia ma sporo krwi na rękach.
* V: Infernia pobrała jeńców wojennych. Kilkanaście osób.

Krypta pobrała ludzi po czym zaczęła dematerializować... Infernia za nią.

Eustachy decyduje się na próbę pozyskania złomu. Używa magii i ekipy. Natychmiast - przebudować elementy luku Inferni. Niech pobiera. Niech pożera traktorem "szeroko". Pull grawitacyjny.

ExZM+2:

* O: GORĄCE SERCE EUSTACHEGO. Udało się ściągnąć złom. Masz części zamienne. Diana aprobuje. "Tęskniłeś, kotku?" z szatańskim uśmiechem...

Krypta przeszła w Nierzeczywistość. I szybko z niej wyszła. Nie są w prawdziwej rzeczywistości. Baza Sarairen wysłała sygnał do Alivii - odebrała Infernia. Oliwia nie jest kapitanem, ktoś inny. Arianna odbiera emocjonalny sygnał Krypty "nie idź tam, uciekaj, omijaj". Jest jakieś skażenie techorganiczne.

Arianna reaktywowała Kryzysowy Sztab Noktian. Eustachy wie o tajnej bazie Noctis. Stamtąd ściągnęli jakieś rekordy, rejestry... ale te są ZA STARE.

TrZ+2:

* X: Eustachy jest Gerardem. Helena traci brata (eks-kapitana statku)
* V: Ulisses rozpoznaje nazwę bazy. Finis Vitae.

Baza z morderczym Lewiatanem, który czyha w pajęczynie...

Arianna ma pomysł. Zamiast odgrywać TEN scenariusz, trzeba przekierować uwagę Heleny / Krypty. Sytuacja z Anomalii Kolapsu i Skażeniem Kijarą. Infernia ma logi...

Eustachy spanikował. A raczej ma dość. Są we wspomnieniu. Nie chce ginąć we wspomnieniu. Trzeba wyrwać Kryptę ze "snu", z traumy. Eustachy wsadza kilkanaście jeńców wojennych do śluzy i otwiera i zamyka szybko by powodować efekt podduszania i "omg umrę". Czyli - wybudzanie Krypty. Plus, PEŁNA MOC DZIAŁ INFERNI w bazę. To jest wspomnienie, Krypta nigdy nie zaatakowała, więc rzeczywistość się zmieni.

Celem Eustachego jest wybicie Krypty z transu i brak poważnych konsekwencji dla siebie i Inferni.

ExZ+3:

* X: Aparycja Diany wróci. Esuriit ją utrzymuje. Jej echo się pojawi, ale jeszcze nie przez dłuższy czas.
* V: Krypta jest wybita z transu.
* V: Krypta nadal nie zauważa Inferni

Baza krzyczy "Gerard nie strzelaj co robisz". Eustachy "jesteście Skażeni nic nie można zrobić. Wasza śmierć uratuje Noctis."

Arianna kazała Janusowi zrobić fabrykację sygnału. Spotęgowanie sygnału. Finis Vitae opanował bazę. Jest wszędzie. Krypta może tylko strzelać. I wysłane pintką.

JANUS PRACUJE. 

TrZ+2: 

* V: Udało się. Krypta jest przekonana, że bazy nie da się uratować.
* V: Krypta dzięki sygnałom Janusa jest przekonana, że tylko działo strumieniowe. W ten sposób wyczyści ładnie.

Eustachy krzyczy jako Gerard. "To nie ja! To koloidowy dreadnaught! Podaję namiary" - bo oni nie widzą Krypty. EUSTACHY PROWOKATOR.

TrZ+3:

* V: "Gerard" przekonał bazę. Otwiera ogień do Krypty. Krypta otwiera ogień do bazy...

Arianna posiłkując się Eleną i doświadczeniem z wyścigów z Olgierdem leci w stronę wiązki światła. Ma dokładne informacje gdzie jest Krypta i w co strzela (dzięki Eustachemu)

ExZ+4:

* Xz: jakieś 2-3 jeńców wojennych nie wytrzyma tego wszystkiego. Wymrą.
* V: Infernia przechodzi przez wiązkę do Anomalii Kolapsu...
* V: ...nawet na obrzeża.

Infernia wylądowała po stronie Orbitera. Minęło 17 dni w Nierzeczywistości.

Arianna wykrywa statek Orbitera na sensorach. Statek Orbitera... ucieka. Infernia ma mało wszystkiego, ale jest w kosmosie. Da się dolecieć jakoś gdzieś. Martyn "stazuje" kogo się da i nadaje SOS. Między SOS leci wiązanka "nie jesteśmy anomalią, nie strzelajcie"...

Nawiązana komunikacja z Infernią. Żelazko.

* "Tu Żelazko. Zidentyfikuj się."
* "Arianna Verlen. To ja, dobrze Cię słyszeć, Olgierd."
* "NIE WIERZĘ!" ze śmiechem
* "Musisz mi porwać Eustachego. Mam go dość :3. I pozdrów Marię od Martyna."

:-)

Eustachy ma Bliznę "krwawy" - jak jest źle, idzie w trybie ostrej masakry. Asimear, to tutaj (jeńcy), to co z Dianą.

## Streszczenie

Jak wrócić z tajnej noktiańskiej bazy oderwanej od świata i Bram? Oczywiście, Nocną Kryptą. Arianna wezwała Kryptę i Infernia schowała się w jej cieniu przenosząc się między rzeczywistościami. Po drodze udało się Inferni doprowadzić do zniszczenia niewielkiej niegroźnej floty używając Krypty, trafiła do przeszłości Krypty i widziała Finis Vitae - ale wróciła przez Anomalię Kolapsu do domu. Bo Krypta jest połączona z Anomalią Kolapsu. Aha, część noktian z Inferni została w Zonie Tres.

## Progresja

* Eustachy Korkoran: Blizna "Krwawy". Gdy sytuacja jest zła, nie patrzy na nic i nikogo, ratuje swoją skórę - robi się ultrapragmatyczny i nie przejmuje się komu robi krzywdę.
* Eustachy Korkoran: Jest powiązany historycznie z Infernią. Gdy Infernia anomalizowała, to on i jego zespół/rodzina doprowadzili Infernię do działania. A Infernia wymaga okresowych uszkodzeń.
* Arianna Verlen: dobra wola ze strony noktian; zarówno z uwagi na to jak potraktowała Zonę Tres jak i swoich noktian pozwoląc im odejść
* OO Infernia: przeszły wysoki poziom anomalizacji Inferni pozwalał na ukształtowanie jej na nowo. Wyższe osiągi. Większe możliwości. Co można zrobić / wykręcić ze statkami. Plus, to jest unikat. Dlatego Eustachy się doń podczepił.
* OO Infernia: opętana "aparycją" / "efemerydą" Diany Arłacz w najbardziej psychotycznej i ukochanej przez Eustachego wersji.

### Frakcji

* .

## Zasługi

* Arianna Verlen: jako pilot Inferni manewrowała trzymając odległość od Krypty w nierzeczywistości; potem unikając statków nieznanej flotylli a na końcu - wleciała w wiązkę strumieniowego działa Krypty by wrócić do domu.
* Eustachy Korkoran: Pozyskując złom w Nierzeczywistości zapewnił efemerydę Diany Inferni; też wyrwał Kryptę z koszmarnego snu, by Arianna mogła wrócić do domu.
* Romana Arnatin: zdecydowała się zostać w Zonie Tres i opuściła załogę Inferni wraz z kilkunastoma noktianami.
* Ulisses Kalidon: wie sporo o Finis Vitae i o historii Noctis i upadku Noctis. Można powiedzieć, "koneser" historii Noctis i jednostek noktiańskich.
* Janus Krzak: z Eustachym zmontował sondę która wysłała sygnał udający, że w rzeczywistości Finis Vitae opanował bazę Sarairen. Jego wiedza o Eterze uprawdopodobniła ów sygnał.
* Romana Arnatin: zdecydowała się zostać w Zonie Tres, opuściła Infernię z kilkunastoma noktianami.
* Oliwia Karelan: echo; na czas inwazji Finis Vitae pierwszy oficer Alivii Nocturny. W przyszłości - kapitan. W wizji Nocnej Krypty animowana przez Ariannę.
* Helena Adanor: echo; na czas inwazji Finis Vitae oficer medyczny Alivii Nocturny. Siostra kapitana Alivii - Gerarda.
* Gerard Adanor: echo; na czas inwazji Finis Vitae dowódca Alivii Nocturny. Umarł podczas tamtej operacji. W wizji Nocnej Krypty animowany przez Eustachego.
* Finis Vitae: echo; straszliwy Lewiatan - autosent niszczący wszystko i mający jako jedyny cel koniec rzeczywistości. Opanował Sarairen i zastawił pułapkę na statek medyczny...
* Atrius Kurunen: echo; dowódca Sarairen. Za jego dowodzenia Sarairen została przejęta przez Finis Vitae.
* AK Nocna Krypta: przywołana przez Ariannę, by wrócić do domu, podróżując w Cieniu Krypty przez Nierzeczywistość. Podczas pierwszego skoku zaatakowana przez dwie małe floty; zniszczyła wszystkie. Przypomniała sobie koszmary z czasów pierwszego pojawienia się Finis Vitae, gdy straciła kapitana.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Nierzeczywistość
    1. Primus
        1. Zagubieni w Kosmosie
            1. Crepuscula
                1. Pasmo Zmroku
                    1. Baza Noktiańska Zona Tres: uzyskała grupkę kilkunastu noktian łącznie z lekarzem zdolnym do regeneracji Bii.

## Czas

* Opóźnienie: 1
* Dni: 17

## Konflikty

* 1 - Arianna zdecydowała się na tym ugrać dwie rzeczy - dobra wola ze strony Bii i podniesienie morale swoich noktian. 
    * TrZ+1
    * VV: Arianna dostaje zasoby i noktianie jej bardziej ufają do końca tej sesji. Plus, dobra wola od noktian.
* 2 - Arianna zdecydowała się wykorzystać nieszczęście Lewiatana by ściągnąć Kryptę. Plus wysłać moc w Eter "Krypto chodź tu jest źle".
    * TrM+2
    * V, (+3V) V: dzięki Eustachemu i klatce Faradaya udało się wezwać echo Krypty i ukryć przed Kryptą Ariannę
* 3 - Krypta opuściła Nierzeczywistość. W środku pola bitwy; w przestrzeni - nie do końca wiadomo gdzie jesteśmy. Więc gdzie? Zespół naukowy Klaudii:
    * TrZ+2
    * V: Wiemy gdzie, ale nie wiemy jak stąd wrócić. Daleko...
* 4 - Infernia się repozycjonuje. Arianna przesuwa Infernię w taki sposób, by strzały albo szły w Kryptę albo w farmy amat.
    * ExZ+2
    * XXVV Infernia trafiona, ale pola wytrzymały, może manewrować. Eustachy strzela tłustymi torpedami w farmy amat by nie atakowali Inferni
    * (+3V) XXXXV: wszyscy skupieni na Krypcie i Inferni, Krypta dewastuje flotę. Infernia ma krew na rękach. Pobrała jeńców wojennych z tego systemu
* 5 - Eustachy decyduje się na próbę pozyskania złomu. Używa magii i ekipy ZANIM Krypta dematerializuje.
    * ExZM+2
    * O: GORĄCE SERCE Eustachego. Efemeryda / Aparycja Diany od tej pory opętuje i nawiedza Infernię. Ale się udało.
* 6 - Nierzeczywista Baza Sarairen wysłała sygnał do Alivii - odebrała Infernia. Arianna reaktywowała Kryzysowy Sztab Noktian. Kiedy i GDZIE są?
    * TrZ+2
    * XV: Eustachy jest Gerardem który ginie. A Ulisses rozpoznał nazwę bazy - Sarairen to miejsce infekcji Finis Vitae.
* 7 - Eustachy ma dość. Są we wspomnieniu. Trzeba wyrwać Kryptę ze "snu", z traumy. Używając jeńców wojennych i podduszanie - wybudzanie Krypty. Plus, pełna moc dział Inferni w bazę. 
    * ExZ+3
    * XVV: Sukces, Krypta wybita, ale na pewno Diana będzie wracać.
* 8 - Arianna kazała Janusowi zrobić fabrykację sygnału. Spotęgowanie sygnału. Finis Vitae opanował bazę. Jest wszędzie.
    * TrZ+2
    * VV: Sukces. Krypta jest przekonana, że musi strzelić działem strumieniowym by wszystkich zabić, bo nie do uratowania.
* 9 - Eustachy krzyczy jako Gerard. "To nie ja! To koloidowy dreadnaught! Podaję namiary" - bo oni nie widzą Krypty. EUSTACHY PROWOKATOR.
    * TrZ+3
    * V: Baza strzela do Krypty
* 10 - Arianna posiłkując się Eleną i doświadczeniem z wyścigów z Olgierdem leci w stronę wiązki światła. Ma dokładne informacje gdzie jest Krypta i w co Krypta strzela (dzięki Eustachemu)
    * ExZ+4
    * XzVV: Część jeńców umiera, ale Infernia przechodzi przez wiązkę działa strumieniowego do Anomalii Kolapsu.

## Inne
### Projekt sesji
#### Procedure

* Overarching Theme:
    * gracze mają "kierowanie" (Vision & Purpose); wiedzą gdzie iść i po co tam idą
    * gracze są bardziej zaangażowani w historię, bo widzą, gdzie ich decyzje prowadzą
    * MG wie, które sesje wprowadzać i które usuwać i w którą stronę stawiać adwersariat
* Wyraziste, ważne dla graczy postacie + Relacje (konflikt?) Gracze <-> NPC, NPC <-> NPC
    * graczom bardziej zależy na postaciach i ich powodzeniu
    * więcej potencjalnych wątków i rzeczy które możemy odrzucić przy krzyżykach
* Tory, zmiana postaci z czasem, wpływ graczy na świat
    * gracze widzą jak wpływają na świat. Wpływ jest "ich" - stąd tak tragiczne jest gdy "ich" rzeczy są corrupted / destroyed.

#### Result

* Overarching Theme:
    * V&P:
        * 
    * adwersariat: 
* Wyraziste, ważne dla graczy postacie + Relacje (konflikt?) Gracze <-> NPC, NPC <-> NPC
    * Oliwia Karelan: poprzedni kapitan Krypty. 
    * Gerard Adanor: przed-poprzedni kapitan Krypty. 
    * Technoorganiczny Lewiatan, Finis Vitae. Awatar Nihilusa.
* Tory, zmiana postaci z czasem, wpływ graczy na świat
    * uszkodzenie stabilizatorów Memoriam (kontrolujących stabilizację Rzeczywistości)
        * przenikanie Nierzeczywistości na Infernię
    * przebudzenie Inferni i powrót jej do anomalnego stanu
    * uszkodzenie Inferni, śmierć członków załogi

#### Scenes

* Scena 0: brak
* Scena: Stabilizacja pól Inferni i magicznie i fizycznie. Utrzymanie Inferni podczas "lotu" przez Nierzeczywistość
    * GDZIE: W środku pola bitwy; kilkanaście vs kilkanaście jednostek w ciemnym pasie niedaleko słonecznych farm amat
    * CO: 
        * Utrzymanie się blisko Krypty
        * uniknięcie wszelkich ataków
        * utrzymanie pól Inferni i usunięcie potencjalnych zagrożeń 
        * pozostanie niezauważonym
* Scena: Alivia Nocturna dociera do bazy Sarairen. Pierwsze miejsce, w którym Noctis napotkało Finis Vitae. Miejsce, skąd Finis Vitae zrobił inwazję. Kapitan Alivii (Gerard) zginął podczas tej operacji.
    * GDZIE: Baza Sarairen. Miejsce, gdzie noktianie przyprowadzili Finis Vitae... dowodzi Eustachy (Gerard), Arianna jest pierwszym oficerem (młodziutką Oliwią)
    * CO: 
        * Eustachy musi przeżyć (co złamie rzeczywistość Krypty)
        * Zobaczenie koszmaru którym jest Finis Vitae i przetrwanie - ewakuacja Alivii Nocturny.
* Scena: Spokój. Nierzeczywista baza Noctis.
    * GDZIE: Nierzeczywista baza Papilio Munitionem, najcięższa baza defensywna. "Zamek As'caen".
    * CO: 
        * uniknięcie wykrycia przez Kryptę
        * kontrola morale
        * podreperowanie Inferni
* Scena: Alivia Nocturna nakłada się na Infernię; osoby uratowane po Finis Vitae.
    * GDZIE: Infernia
    * CO: 
        * Utrzymanie stabilizatorów Memoriam
        * uniknięcie paniki na pokładzie przez duchy, wizje itp.
        * odparcie infekcji Finis Vitae
* Scena: 
    * GDZIE: 
    * CO: 




---
layout: cybermagic-konspekt
title: "Wielki Kosmiczny Romans"
threads: legenda-arianny
gm: żółw
players: kić, fox, kapsel
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [200715 - Sabotaż Netrahiny](200715-sabotaz-netrahiny)

### Chronologiczna

* [200715 - Sabotaż Netrahiny](200715-sabotaz-netrahiny)

## Budowa sesji

### Stan aktualny

.

### Dark Future

* .

### Pytania

1. .

### Dominujące uczucia

* .

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Punkt zero

.

## Misja właściwa

Admirał Kramer poprosił do siebie Ariannę i poinformował ją, że po ostatniej akcji pogratulował jej kapitan Żelazka. To nie jest powód do chwały. Zniszczyła kolejną korwetę - tym razem to była Tvarana. Tvarana nie będzie już latała - uszkodzenia jakie magia (Eleny, ale wszyscy wierzą, że Arianny) spowodowały uszkodziły kadłub do poziomu, że Tvarany nie opłaca się już naprawiać.

Arianna wyraziła skruchę. To nie to, że ona robi to celowo. Kramer ma powoli wątpliwości, ale nie ma dowodów. Niech więc tak będzie...

Ale jest coś co Arianna musi rozwiązać. Pojawiła się plotka, że jest romans. Romans między Eustachym i Eleną - a Elena jest bezpośrednio pod Eustachym. Co gorsza, niektóre plotki łączą z tym też Ariannę. Arianna miałaby mieć romans z Eustachym?! To już jest niewskazane. Arianna ostro zaprotestowała, na co Kramer odparł, że przecież plotki pochodzą z jej statku. Arianna nawet odgadła od kogo: to na pewno wina Leony...

Kramer powiedział Ariannie, że jest zaproszona z Eustachym i Eleną na statek kosmiczny Welgat. Dowodzi nim Julian Muszel. Muszel jest podobno strasznym romantykiem - celuje w to, żeby zrobić jak najlepsze love story. Zaproszona trójka powoduje duże ryzyko. Kramer powiedział Ariannie wyraźnie - plotki MUSZĄ się skończyć. Ona MUSI coś z tym zrobić. I nie może odmówić zaproszenia Muszela. Arianna, niezbyt szczęśliwa, zgodziła się na to wszystko. Naprawdę nie ma wielu pomysłów jak sobie z tym poradzić...

Acha, Kramer zapowiedział Ariannie - **ani jednej zniszczonej korwety / statku** przez najbliższy czas. Albo będą konsekwencje.

Tymczasem, w zupełnie innym miejscu, Eustachy dostał wiadomość od kpt. Olgierda Drongona. Dowódca Żelazka mu pogratulował. Eustachy próbował się bronić, że nie wie o co chodzi i nie zasłużył, ale Drongon jedynie coraz bardziej się śmiał - "niektórzy powiedzieliby, że takie 'nic' to ogromne osiągnięcie". W końcu Eustachy doszedł do tego o co chodzi - Olgierd uważa, że Eustachy poderwał zarówno Ariannę jak i Elenę. Oburzył się i skrzyczał Olgierda (jednego z groźniejszych kapitanów), że to podła plotka i będzie zemsta!

Olgierd wyciągnął z tego następujące rzeczy:

* Eustachy jest 'starstruck' przez Ariannę. "Arianna-samaaaa!". Jest zakochany, acz platonicznie.
* To właśnie dlatego w poprzedniej operacji Infernia wbiła się w Żelazko. Bo Olgierd dokuczał Ariannie.
* Olgierd nie jest sadystą. Stwierdził, że nie będzie dokuczał Ariannie, Eustachemu czy Elenie. Nie będzie propagował plotek.

Narada wojenna - Arianna i Eustachy. To jest poważny problem. Eustachy ma ochotę coś inteligentnie dla zemsty wysadzić. Arianna się nie zgadza. Eustachy nalega. Arianna zauważyła, że jeśli Eustachy ma romans z Eleną, to może mieć - byle Elena nie była podwładną. Więc może zawsze Eustachego degradować i awansować na pierwszego oficera Klaudię. Eustachy jest nieszczęśliwy - nie będzie wysadzał niczego. Arianna oddycha z ulgą, ale jeszcze Elena może coś wysadzić (bo czemu nie -_-)...

Eustachy chce się zemścić na kimś kto zaczął te plotki. Arianna wskazała Leonę. Zapał Eustachego zgasł - nikt nie jest w stanie pokonać Leony w walce, po prostu nikt.

* Arianna: "Musisz znaleźć sobie dziewczynę, to usunie plotki. Najlepiej przy użyciu hobby."
* Eustachy: "Ale moje hobby to klejenie modeli statków kosmicznych. Potem je wysadzam!"
* Arianna: "I serio myślisz, że nikt na to nie poleci? Żadna zdegenerowana arystokratka?"
* Eustachy: "Nie, to niesprawiedliwe!"

No nic, dopiero następnego dnia mogą odwiedzić OO Welgat. Tak więc - Arianna ma zamiar zająć się Leoną, niech Eustachy... no, usunie się z widoku i przygotuje się na przybycie na Welgat. W końcu jadą podobno pomóc z wykryciem sygnatury Nocnej Krypty.

Arianna wezwała do siebie Leonę. Ta przyszła w świetnym humorze. Nie, nie kojarzy plotek łączących Ariannę z romansem Eustachy - Elena. Ona odpowiada za fragment Eustachy - Elena. Czemu? Bo Elena taka sztywna, to może skończy się jakimś fajnym mordobiciem. Plus, polewali alkoholem w dużych ilościach...

Arianna z ciężkim sercem zakazała puszczania Leonie JAKICHKOLWIEK plotek i kazała jej wziąć Martyna i Kamila - niech odnajdą KTO powiązał to wszystko z nią. Ona musi być poza podejrzeniami - jest w końcu bohaterką! Nie może pozwolić sobie na coś takiego! Leona się zgodziła - zawsze okazja do mordobicia.

A co z Eustachym? Ano, Eustachy ogląda Infernię, w kiepskim humorze. Podchodzi do niego jeden z inżynierów i pyta, czy Eustachy chciałby zobaczyć prototypowy silnik do Inferni. Eustachy poszedł za nim - prosto, by wpakować się w pułapkę. Czterech osiłków i jakiś szlachcic, oficer floty. Szlachcic przedstawił się jako Tadeusz Ursus; podobno Elena jest jego żoną. Nie przeszkadza mu, że Eustachy kręci z Arianną, more power to him. Ale Eleną?! JEGO ŻONĄ?!

Eustachy zaczął się bronić że to nie tak, ale jedynie skończył jako "fetyszysta z gminu". Ale zdołał wysłać sygnał SOS do Leony, która opuściwszy rozmowę z Arianną ma paskudny humor i chce komuś obdzielić WPIERNICZ. Wpadła szybko na spotkanie i sytuacja się odwróciła - co prawda Tadeusz nie ma pojęcia kim jest Leona, ale jego podwładni mają o niej Opinię. Korzystając z obecności Leony, Eustachy zmienił swoją historię, a niesamowicie szczęśliwa Leona jedynie słuchała z coraz szerszym uśmiechem.

* V: To nie Eustachy uwiódł Elenę. To ona jego uwodziła cały czas. Sorry Tadeusz, Twoja żona ssie (pod względem wierności).
* X: Elena uwodziła nie tylko Eustachego, ale też Ariannę; to silniejsze od niej. Może ma Diakońską krew?
* V: Nie udało się Elenie poderwać Arianny. Próbowała, ale Arianna jest niedostępna.
* O: Tadeusz będzie aktywnie dementował plotki o Arianna x Eustachy. Ale wzmacnia "Elena podrywaczka". "To nie jej wina, jej magia tak działa". "Elena nie umie się powstrzymać".

Dobrze. Niby wszystko się udało, ale Leona chce się bić. Udawała, że jest zadowolona, ale rzuciła się zrobić wpierdol minionowi. Eustachy się rzucił na ratunek: (OO). Leona jemu ostro wpierniczyła przez przypadek (do końca dnia pod kuratelą Martyna). Ale Tadeusz zobaczył czym jest Leona, zrozumiał, że zrobił źle i wystosował do Kramera oficjalne dementi. Więc - reputacja Eleny jest w strzępach, ale Arianny jest podratowana.

Arianna spotkała się z Eleną, w swoich kwaterach. Elena ma świetny humor - dostała od przyjaciela (Damiana Oriona) Iquitas, lekki i zgrabny ścigacz. Arianna spróbowała wypytać Elenę o Iquitas i przyjaciela; ta powiedziała, że Orion nie jest jej "przyjacielem" per se. A na pewno nie jest niczym więcej. Wtedy Arianna powiedziała jej o plotkach. Elena nie była w stanie się powstrzymać. Czysta, nieludzka wściekłość. Wściekłość i rozpacz. Elena wbiła sobie paznokcie w dłonie. Krew. Emisja Esuriit.

* V: Arianna zatrzymała Elenę przed pożarciem części kabiny entropicznie
* X: Elena emitowała eksplozję; Arianna nie spodziewała się tej siły i tej kontroli Esuriit. Eleny tu "nie ma", jest czysta moc.
* X: Elena, Arianna - obie ranne. Moc eskaluje.
* V: Arianna używa mocy arcymaga - na siłę wygasiła Elenę. Wtłoczyła w nią energię i ją zasealowała.

Arianna wypytała Elenę o Damiana Oriona - może to jej chłopak (co pomoże z plotkami)? NIE! Damian wie o jej przypadłości i chce zrobić z niej Emulatorkę. Elena wyjaśniła czym są Emulatorki z NeoMil - zdolne do przełączenia zestawów osobowości i umiejętności homo superior, pochodzące z NeoMil; sterowane przez Kirasjerów. Bardzo niebezpieczne i silne. Ale Damian nie chce na siłę. Elena NIE CHCE skończyć jako Emulatorka. Arianna świetnie rozumie.

Elena chciała opuścić komnatę i iść do siebie, Arianna jej nie pozwoliła. Wezwała Martyna - niech on weźmie i zajmie się Eleną i zamaskuje to wszystko. Martynowi się udało; doprowadził Ariannę do dobrego funkcjonowania i maksymalnie zreperował Elenę (ale nie tak skutecznie, unikalny visiat Eleny nie zadziałał dobrze na środki Martyna "na szybko").

Następnego dnia udali się na Welgat.

Zdaniem Arianny i Eustachego, Welgat jest niezłej klasy statkiem kosmicznym. Powiedzmy, 70% kompetencji. Typowy pojazd floty. Jest to pojazd zwiadowczy, ale wymagający wsparcia Kontrolera Pierwszego, nie to co np. Netrahina. A i załoga nie jest tragiczna, też kapitan może jest nieuleczalnym romantykiem ale nie jakaś katastrofa. Za to gdy Elena zobaczyła pewnego faceta na mostku, jednego z oficerów, prawie zaczęła jej się samoistna emisja.

Arianna ją skutecznie powstrzymała dominacją jej magii; wzięła Elenę na stronę i spytała o co chodzi - a przy okazji wyjęła to, że zdaniem Tadeusza, Elena jest jego żoną.

Elena znowu miała prawie monumentalną erupcję, ale Arianna po prostu jest silniejsza - zdusiła tą erupcję i zażądała wyjaśnień. Elena zaczęła odpowiadać:

Nie ma męża. Tadeusz jest arystokratą Eterni. Elena nie jest obywatelką Eterni. Jako, że Elena nie jest obywatelką, nie ma szczególnych praw i została poślubiona "zaocznie". Nie jest to rozpoznawalne w świetle Orbitera, ale jest w świetle Eterni. Tak więc... Elena nie pozwoli, by Tadeusz kiedykolwiek ją porwał czy zaprowadził / zaprosił na Eternię.

A o co chodzi z tym oficerem tutaj (Konradem)? Elena swego czasu wyzwała go na pojedynek na śmierć i życie bo ją nachodził. Konrad się zgodził - ale jeśli Elena go nie zabije, to go kocha i za niego wyjdzie. I teraz Elena ma problem. Pojedynki są akceptowalne wśród arystokracji, ale zakazane wśród oficerów. Więc jak go zabije - koniec z jej karierą, wydalona ze służby itp. Ale jeśli go nie zabije, ma poważny problem, bo pojedynek tego typu byłby zaakceptowany w arystokracji Aurum - i ona może mieć na sobie ogromną presję by za niego wyjść. Innymi słowy - Elena jest przegrana na starcie.

Jednak ta erupcja, to starcie między Arianną i Eleną zostało wyłapane przez czujniki Welgata. Eustachy szybko wszedł w tłumaczenie - Elena i Arianna NIE MAJĄ romansu. Arianna opiekuje się biedną, niestabilną Eleną. A że skończyło się (znowu) utratą przytomności Eleny i skrzydłem medycznym, cóż... sami widzą jak jest.

Niedługo później Konrad poprosił do siebie Eustachego, by pogadać z nim jak facet z facetem. Eustachy poszedł i Konrad powiedział, że kocha Elenę. Czy Eustachy z nią TEN TEGO?

Eustachy w kropce. By oszczędzić Elenie kłopotów nawet rozważał powiedzenie Konradowi, że ona jest jego kochanką, ale Arianna wybiła mu to hipernetem z głowy. Tak więc opracowali zupełnie inną opowieść, coś absolutnie genialnego:

* Elena i kapitan Olgierd Drongon są jak Julia i Romeo. Ale Arianna srogo nie pozwala miłości na konsumpcję.
* Tak więc **Leona** jest tajemniczym posłańcem i strażnikiem serca Eleny i Olgierda. Ona jest głównym swatem. ("Olgierd z 'Żelazka' jest w armii, która chce podbić ten zamek")
* A kim jest Leona? Tu Eustachy pokazał render tej katastrofy, która stała się na Castigatorze jak grupa arystokratów próbowała pokazać Leonie gdzie jej miejsce...

W wyniku tego:

* V: Konrad daje Elenie spokój na stałe. Leona + Olgierd to mordercze kombo.
* O: Niestety, Olgierd dowie się, że ubiega się o rękę Eleny. Nie doceni.
* O: Elena dowiaduje się, że Eustachy odplątał ją z tej katastrofalnej sytuacji. NIE JEST TAKI ZŁY.

Gdy wszystko wyglądało na to, że się dobrze skończy, na Welgat przybył Damian Orion poprosić Ariannę, by pomogła mu uratować jego Emulatorkę z Nocnej Krypty. Emulatorka wpadła tam podczas jednej z akcji i stało się jej coś strasznego...

## Streszczenie

Pojawiła się plotka, że Arianna, Elena i Eustachy mają romans. Plotka pochodzi od Leony, ale ktoś ją wykorzystał. Rozwiązując plotkę Zespół doszedł do tego, że Elena ma niestabilną energię magiczną i na niektórych upiornie mocno działa, poznali też elementy przeszłości Eleny. Po wyplątaniu Eleny z jednego obsesyjnego adoratora (i wpakowanie plotki że Olgierd z Żelazka aspiruje do jej ręki), Arianna zdecydowała się z Eustachym, Klaudią i Martynem pomóc Kirasjerom w uratowaniu zaginionej Emulatorki z Nocnej Krypty...

## Progresja

* Eustachy Korkoran: w oczach Olgierda Drongona jest platonicznie zakochany w Ariannie typu 'senpai notice me'. Olgierd patrzy na to z litościwym pobłażaniem i więcej mu wybacza.
* Eustachy Korkoran: dostał opinię "fetyszysty z gminu" wśród arystokracji Aurum i Eterni - podrywa tylko szlachcianki, nie interesują go inne kobiety. Co gorsza, jest bardzo skuteczny w podrywaniu.
* Elena Verlen: opinia "tej, która nie kontroluje swojej magii" i "podrywa na lewo i prawo - ale to nie jej wina, jej magia tak działa". Nie jest brana poważnie. Jej opinia - w RUINIE.
* Elena Verlen: potrafi emitować Esuriit; nie ma pełnej kontroli nad magią, ale jest w stanie magię świetnie instynktownie używać. Acz nie zatrzyma się pod wpływem emocji.
* Elena Verlen: dostała Iquitas od Damiana Oriona do przetestowania.
* Elena Verlen: Eustachy jej pomógł, odplątał jej z krwawego pojedynku arystokratycznego przeciw Konradowi Wolczątkowi. Nie jest taki zły O_O.
* Olgierd Drongon: dowiaduje się, że ubiega się o rękę Eleny Verlen. Zupełnie mu się to nie podoba.

### Frakcji

* .

## Zasługi

* Arianna Verlen: jej moc jest tak duża, że zdusiła emisję Esuriit Eleny. Wplątana w romans którego nie miała, opieprzona przez Kramera, dała radę ochronić reputację - kosztem Eleny.
* Eustachy Korkoran: wplątany w romans którego nie miał, groziło mu nawet że ma znaleźć dziewczynę na szybko. Wyplątał Elenę ze strasznego pojedynku używając Leony jako straszaka.
* Antoni Kramer: admirał, który opieprzył Ariannę za dewastowanie korwet (wina Eleny) i za romanse (wina Leony). Dał Ariannie zadanie naprawy tego. Nadal silnie stoi za Arianną mimo nacisków.
* Elena Verlen: alias Mirokin; o jej rękę ubiega się sporo adoratorów, ale ona NIE CHCE. Nie do końca kontrolując magię, emitowała Esuriit. Skończyła z reputacją w strzępach, ale powiedziała Ariannie prawdę.
* Leona Astrienko: źródło plotek pierwotnych odnośnie trójkąta Arianna - Eustachy - Elena. Potem pełni rolę straszaka dla WSZYSTKICH adoratorów Eleny - Tadeusza i Konrada. Nawet nikogo (poza Eustachym) nie walnęła solidnie.
* Julian Muszel: kapitan Welgata. Romantyczny do bólu, lubi też dramy. Chciał ściągnąć Ariannę, Elenę i Eustachego dla swojego oficera (podobno by badać sygnaturę Nocnej Krypty).
* Olgierd Drongon: kapitan Żelazka. Podśmiewuje się z Eustachego - mistrza podrywu. Ale nie chce krzywdy załogi Inferni. Humor mu zepsuło, bo podobno podrywa Elenę z Inferni... coś do rozwiązania.
* Tadeusz Ursus: eternijski szlachcic i oficer Orbitera na Kontrolerze Pierwszym. Uważa się za męża Eleny. Elena go nie cierpi. Złapał Eustachego by wyjaśnić "nie dobiera się do Eleny" ale przyszła Leona. Teraz wierzy, że ELENA jest flirciarą i chroni reputację Arianny przeciw Elenie.
* Damian Orion: najlepszy "przyjaciel" Eleny - on daje jej statki kosmiczne licząc, że dołączy do NeoMil (np. jako Emulatorka) a ona przyjmuje statki, ale nie dołączy. Poprosił Ariannę, by ta pomogła mu uratować Laurę Orion z Nocnej Krypty.
* Konrad Wolczątek: oficer, artylerzysta OO Welgat; zakochany w Elenie Verlen; miał z nią mieć pojedynek o miłość. Eustachy miłość wybił mu z głowy tłumacząc, że Elena jest w trójkącie między Olgierdem Drongonem a Leoną... czy jakoś tak.
* OO Welgat: Statek zwiadowczy wspomagający, krótkozasięgowy; detektor koloidowców. Silne sensory. Nie poleci daleko bez wsparcia floty. Ma najbardziej romantyczną i rozplotkowaną załogę we flocie.
* OO Żelazko: Dalekosiężna i bardzo szybka kanonierka; delikatna. Dowodzi nią Olgierd Drongon. Na tej sesji niewiele się pojawiła; działa w okolicach Netrahiny (czyli Anomalii Kolapsu).

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria, Orbita
                1. Kontroler Pierwszy
                    1. Hangary Alicantis: miejsce przypisane m.in. do admirała Kramera, tam zwykle znajduje się Infernia.

## Czas

* Opóźnienie: 3
* Dni: 3

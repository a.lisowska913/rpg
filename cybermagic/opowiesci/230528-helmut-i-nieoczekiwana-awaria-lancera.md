---
layout: cybermagic-konspekt
title: "Helmut i nieoczekiwana awaria Lancera"
threads: historia-klaudii, historia-martyna, grupa-wydzielona-serbinius
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [230521 - Rozszczepiona Persefona na Itorwienie](230521-rozszczepiona-persefona-na-itorwienie)

### Chronologiczna

* [230521 - Rozszczepiona Persefona na Itorwienie](230521-rozszczepiona-persefona-na-itorwienie)

## Plan sesji
### Theme & Vision & Dilemma

* PIOSENKA: 
    * 
* CEL: 
    * Skupić się na Helmucie. Pokazać kolesia.
    * Pokazać Klaudię jako naukowca, badacza i eksperta od biurokracji

### Co się stało i co wiemy

* Kontekst
    * Mamy pewien obszar przestrzeni kosmicznej gdzie występuje anomalia uszkadzająca technologię
        * Raz na jakiś czas coś się psuje
    * Mamy Helmuta, który jest wielkim fanem 

### Co się stanie (what will happen)

* S0-1: Helmut robi rundki "użyjmy tańszych substratów". Martyna się boi XD.
* S0-2: Semla uszkodzona, Helmut zrobił pewne oszczędności, ale nie było problemu
* S1: Podczas rutynowych działań, awaria Lancera Anastazego. Ratujemy Anastazego.
* S2: Spięcie Anastazy - Helmut Termann. Deeskalacja?
* S3: Problemy w Admiralicji - Miranda Termann. Fabian prosi Klaudię o dowody łagodzące dla Helmuta.

### Sukces graczy (when you win)

* .

## Sesja - analiza

### Fiszki

.

### Scena Zero - impl

.

### Sesja Właściwa - impl

_około 3 tygodnie temu._

Helmut odwiedza Klaudię na Serbiniusie.

* H: "Klaudio, słuchaj, Ty mnie zrozumiesz"
* H: "Używamy fabrykatorów HDRT-726"
* K: "Używamy, całkiem spoko, dają radę"
* H: "Na substratach IK-449"
* K: "Cokolwiek masz na myśli"
* H: "Możemy zdobyć substrat IK-349, poprzednia generacja, o 20%..."
* K: "Brzmi gorzej"
* H: "Są 20% tańsze, da się bez kłopotu zrobić wszystko czego używamy"
* K: "Ale?"
* H: "Są trochę mniej stabilne, ale nasze HaDaRTy dadzą sobie z tym radę. Jeśli się skupisz na konstrukcji i zrobisz to dobrze, masz taniej i wszystko działa. Płacisz tylko 5% wolniejszą konstrukcją. I potencjalną awaryjnością, ALE jeśli masz dobre detektory to recyklujesz. Więc nie ma kłopotu."
* K: "Dwoma słowami? Rescue duty. Nie możesz recyklować w tych warunkach."
* H: "Przygotujemy wcześniej - zawsze to robimy."
* K: "Czasem nie da się wcześniej, nie wiesz co będzie potrzebne a potrzebujesz tego na wczoraj."
* H: "W ciągu ostatnich 3 miesięcy Serbinius nic takiego nie potrzebował."
* K: "Znasz zasadę szuflady pełnej starych kabli?"
* H: "Nie wiesz który potrzebny jest kiedy?"
* K: "Jak wyrzucisz, będą potrzebne."
* H: "..."
* H: "Czyli nie podpiszesz mojej petycji do kapitana? /rozczarowany"
* K: "Helmut, słońce, naprawdę Cię lubię, jesteś sensownym inżynierem i dobrze myślisz, ale w tym wypadku się z Tobą nie zgadzam."
* H: "Szkoda..."

30 minut później, memo od Fabiana do Klaudii. "WSZYSTKIE petycje tego typu - automatycznie odrzucaj, Ty się znasz na zasadach Orbitera, nie chcę degenerować mojego statku i jego możliwości.".

Dla odmiany, Klaudia zgadza się z Fabianem XD.

_około 4 dni temu._

OO Serbinius jest wezwany do Daiziwara - transportowy statek cywilny, którego Semla wysłała SOS! SOS! w pełnej panice. Chwilę potem załoga "NIE NIE NIE NIC SIĘ NIE DZIEJE, Semla się nam pogubiła!". Fabian stwierdził, że Semla się nie gubi. Są w obszarze kontroli Orbitera, Serbinius jest w okolicy, moze sprawdzić. Nie będzie to drogie dla Daiziwara.

* Anastazy Termann, żołnierz i advancer, zwrócił się do Fabiana "serio? Rutynowy check?"
* F: "Rutynowy check, DOWÓDCO."
* A: "Przepraszam, ale nie marnuje to czasu Serbiniusa?"
* F: "Serbinius nie ma nic lepszego do roboty." /lekka irytacja "Jeśli chciałeś jednostkę, która będzie miała chwałę a nie będzie latającą lawetą, to droga wolna"
* A: "Dobrze, dowódco."

Klaudia ma pewne problemy z Anastazym. Jest zbyt arogancki. Zbyt się szarogęsi. Sprawdziła go w wolnych chwilach:

Tp +2:

* X: Zostały ślady / logi tego, że Klaudia grzebała, ale miała prawo. Wygląda, jakby Fabian jej kazał.
* V: Anastazy Termann jest atarienem z Orbitera, jego ciotka to komodor Miranda Termann. Miranda jest dość skuteczna i znana. Jednocześnie jest wredna dla wrogów i super dla przyjaciół.
    * Anastazy chciał być na jakiejś fajnej jednostce, ale tymczasowo jest na Serbiniusie. Karny jeżyk. Jakieś ruchy tam były bo podpadł za zachowanie. Ma półroczny przydział. Upłynął miesiąc.

Szczęśliwie, z tego co Klaudia widzi, Fabian - zwykle niecierpliwy i niespokojny - traktuje Anastazego dobrze. Chyba ma nadzieję na awans, promocję, przyjaciół itp. Co pasuje do Fabiana. Tak czy inaczej - Klaudia nie widzi większych kłopotów. Bo jest naukowcem i nie zna się na ludziach.

Helmut skonfigurował servary. W pierwszym locie - Klaudia, Martyn, Helmut i Anastazy. Cała ekipa. _Wellness check_.

Anastazy jest PEWNY, że to piraci. Ma też nadzieję, że to piraci, co nie leży dobrze z Martynem ani z Klaudią. 

* Martyn "nie widziałeś jeszcze piratów w akcji, nie chciałbym by coś się stało załodze Daiziwara"
* Anastazy "rozwalimy wszystkich piratów jak będą"
* Klaudia "a co z załogą statku?"
* Anastazy (wyraźnie po jego minie nie myślał o załodze i dalej o nich nie myśli)
* Martyn (lekko pokręcił głową z dezaprobatą)
* Klaudia "mam nadzieję, że Twoje marzenie się nie spełni"
* Anastazy "ja też (nieszczerze, acz próbował udawać)"

Daiziwar nie odpowiada z poziomu Persefony. "Jest coś po drugiej stronie ale milczy". Klaudia używa kodów Orbitera z sandboxami i wszystkim.

Tp Z +2:

* Xy: 
    * Semla "kim jesteście? Czemu tu jesteście?" (na kody Orbitera) "co to za kody? Są mi znane..." (całkowite zagubienie)
    * Klaudia "podaj swoją identyfikację"
    * Semla "nie wiem jak, wiem że masz prawo, ale nie wiem jak"
    * Klaudia (instrukcja naszą Persefoną, sama nie będzie tego robić +1Vg)
* Vz: 
    * Persefona przeprowadziła Semlę krok po kroku aż ta dała radę wysłać kody. Wszystko ogólnie działa, ale Semla straciła odpowiednik "MBR" - nie ma dostępu do żadnych akcji bo nie wie jak.
    * Klaudia: "dlatego wysłałaś SOS?"
    * Semla: "tak, wiem, że mam swoją załogę, wiem, że statek działa, ale nie mogę nic zrobić i nie chcę zrobić głupiego ruchu"
    * (Anastazy: "czy musimy siedzieć na kadłubie?")
    * (Helmut: "czy musimy słuchać Twojego narzekania?")
    * (Klaudia -> Martyn: "jak zaczną mówić z sensem, pingnij....... ja się zajmuję TAI")
    * Klaudia: musimy wejść na pokład. Muszę dowiedzieć się, co się stało z Semlą.
    * (Klaudia, gestem pokazuje Martynowi by wszedł pierwszy)
    * (Martyn pokazuje Anastazemu by wszedł pierwszy)

Anastazy i Martyn weszli na pokład (jak już Semla się nauczyła od Persefony jak). Anastazy dobrze rozstawił defensywę, Martyn wszedł jako drugi. Helmut - inżynier - wchodzi jako trzeci. A Fabian nawiązuje komunikację z Daiziwarem jak już Semla umie XD.

Klaudia skupiła się na Semli i jej parametrach, choć strzępkiem ucha doszło do niej jak Martyn tłumaczy Anastazemu, że wszystko jest pod kontrolą, Anastazy jest rozczarowany ale zachowuje się jak powinien, Helmut bada stan jednostki i Fabian dowiaduje się od dowódcy Daiziwara że to się "samo stało". Awaria. To nie jest POWAŻNA awaria, ale oficjalnie Daiziwar się cieszy, że Orbiter się tym zajął. Nieoficjalnie? Będą koszty.

Klaudia sama jest w stanie naprawić Semlę przy użyciu Persefony. To nie będzie trudne, to jak odzyskiwanie danych. To aż żałośnie proste, takie awarie się nie pojawiają bo autonaprawa TAI zwykle działa sama. DO TEGO STOPNIA to proste, że aż nie konfliktuję - Klaudia po prostu naprawiła Semlę. 2h pracy.

Korzystając z okazji, Anastazy zaproponował "dowódco, skoro tu jesteśmy - zróbmy przeszukanie, może coś przemycają". Fabian chwilę myśli. Każdy statek coś przemyca, ale w sumie jak coś znajdą... Fabian dał rozkaz przeszukania jednostki. Załoga Daiziwara jest nieszczęśliwa, bardzo. Anastazy, oczywiście, w te 2 godziny znalazł rzeczy:

* nic szczególnego, jakieś porno (które zostało zniszczone bo jest "za ostre"), jakieś rzeczy z prawami autorskimi, jakieś sukienki nieprawidłowo uregulowane z dziwnych materiałów...

Czyli nikt się nie wzbogacił, załoga Daiziwara straciła trochę nieważnych rzeczy, dostali OPIERDOL od Fabiana (ale nic im nie zrobił), Anastazy dostał wyróżnienie za inicjatywę (Fabian próbuje zdobyć zasługi u Mirandy XD) a Klaudia naprawiła Semlę. Jak tylko Semla się pozbierała i kuma więcej co się dzieje, Klaudia puściła pełną diagnostykę. I próbuje zrozumieć SKĄD ta awaria - to nie jest typowa awaria, takie nie są częste. I czemu tu.

I Klaudia zanim się Semla naprawiła, użyła Persefony do zmapowania instrukcji itp - co się stało, sabotaż itp.

(+uszkodzenie Semli nienormalne, -anomalia statystyczna niewykrywalna)

Tr +4:

* X: Fabian "agent Stryk, skończ z tym statkiem jak będziesz w stanie, dobrze" /niecierpliwość, ale WIE, że Klaudii się nie pospiesza. Ale pospiesza bez rozkazu.
* V: Klaudia jest zaskoczona. Nie było sabotażu. Nie było błędnych instrukcji. To jest _failure_ autorepair oraz komponentu software w psychotronice. To się MOŻE zdarzyć. Mega mała szansa statystyczna, ale możliwe.
    * To NAPRAWDĘ wygląda na awarię, acz dziwną
    * Sprzęt Semli jest poza atestami, przeciągnęli, nie był testowany... nadal dziwne, ale już bardziej wiarygodne
* X: Fabian powiedział Klaudii, zirytowany, żeby ona zapewniła im bezpieczny powrót do portu i koniec. To tylko awaria (po tym jak dała raport). On ma dość tej jednostki, lecicie na K1... 
    * Klaudia zapewnia im bezpieczny powrót bez żadnych problemów. 1 godzina więcej. 
        * kilka narzekań Anastazego więcej "Moglibyśmy robić coś konstruktywnego a nie siedzieć na Serbiniusie...", na to Martyn "to jest ważna praca i trzeba ją zrobić - po to jesteśmy na Orbiterze, _młody_ agencie."
* X: Klaudia zrobiła mirror systemów Semli, ale mirror nie znalazł tego co jest prawdziwym problemem.

Na pokładzie Serbiniusa, jak Klaudia weszła, usłyszała kolejną rozmowę między Fabianem i Helmutem.

* F: "jesteś PEWIEN że ten typ konstrukcji Lancerów był bezpieczny? Zagrażałeś wszystkim łącznie z sobą."
* H: "Wyprodukowałem Lancery o 3% taniej i nie usunąłem ŻADNEJ redundancji. Usunąłem jedynie mechanizmy dla niedoświadczonych inżynierów fabrykatorów. Ale zoptymalizowałem system."
* F: "Lancery miały mniej masy."
* H: "tylko na pancerzu w okolicach pośladków. I niektórych pleców. Nie było ryzyka. SOP jest zachowany."
* F: "Orbiter przydziela nam substrat. To nie ma znaczenia."
* H: "ma znaczenie, to kwestia ekologii, oszczędności."
* F: "Dobrze, jesteś ekspertem od fabrykatorów. Nie rób takich rzeczy z zaskoczenia. Ostrzeż o potencjalnych problemach. I optymalizuj po 1% a nie 3% jeśli w ogóle."
* H: (gwiazdki w oczach) "OCZYWIŚCIE KAPITANIE!"
* F (smutno się rozłączył, on NAPRAWDĘ chce do portu)

Klaudia -> Persefona "monitoruj zmiany Helmuta, informuj mnie, rejestruj". Persefona ACKnęła. Aha - Klaudia delikatnie hintnęła to Martynowi. Martyn zrobił zbolałą minę.

* M: "Jesteś Ty i ja."
* K: "W razie czego dupy do siebie i może przetrwamy."
* M: "Uratujemy Serbinius przed nim samym..."
* K: "Myślisz?"
* M: "Też byliśmy młodzi (myśli o Anastazym, Fabianie i Helmucie)"
* K: "Wszyscy byliśmy młodzi... ale mam nadzieję, że nie _tak_ młodzi."
* M: (mądrze pokiwał głową, co się przerodziło w przeczące kręcenie głową i lekki, pobłażający uśmiech)

_dzisiaj_

Znowu Zewnętrzny Pierścień Astorii. Statek Caelfruor. Problem z silnikiem. Niewielka naprawa potrzebna. "Coś jest nie tak trzeba naprawić" - normalnie nie byłoby najpewniej problemu, ale nie mają głównego inżyniera. Pochorował się i został w bazie. Nieszczęśliwy wypadek, wolą wezwać Orbiter niż szukać po swojemu. Zwłaszcza, że silnik ma atest. Semla statku mówi, że to niewielka rzecz - mikroprzerwania z drganiami więc doleci bez kłopotu, ALE oni chcą być czyści itp. Dla Klaudii - miła odmiana. Dla Anastazego - CZEMU MY TO ROBIMY. Helmut "oj nie przejmuj się".

Fabian, jak to Fabian, trzyma się zasady "zawsze lecą dwie osoby nigdy jedna". Leci Anastazy (eskorta) i Helmut (naprawić).
Klaudia jest zaalertowana przez Persi, że Helmut zrobił małą zmianę konfiguracji Lancerów; jakieś niewielkie zmiany. Nic szczególnego nie powinno się stać. Anastazy ma obniżoną redukcję noszenia o 10% a Helmut sobie obniżył pancerz na plecach. Też wybrał troszkę prostsze silniki manewrowe dla obu Lancerów. Zdaniem Klaudii - nierozsądne, ale nic z tego co wybrał nie zakłóca parametrów operacyjnych. NAWET jakby był kłopot to NAWET WTEDY nie powinni mieć problemu. Typowy Helmut.

Persefona potwierdziła - to eksperymenty jakie Helmut robi regularnie. Helmut nie narusza parametrów, po prostu zamiast przy zielonej linii jest przy żółtej linii. Ale NADAL prawidłowo.

Klaudia połączyła się z tamtejszą Semlą i zażądała informacji o awarii, wszystko. By umożliwić inżynierowi pracę. 

(+lokalizacja podobna jak miejsce gdzie Semla Daiziwara rzuciła SOS, -anomalia statystyczna niewykrywalna)

Tr +4:

* Vr: to jest TYPOWA AWARIA. Coś małego. Główny inżynier by to zrobił, gdyby był. Gdyby Semla była autoryzowana, powinna sobie z tym poradzić, ale bez inżyniera jej nie autoryzują.
    * rzadka awaria, ale możliwa, bo ta jednostka ma wszystkie atesty itp.
    * jak tylko pojawił się problem, natychmiast przełączyli statek w "stoimy" i wezwali Orbiter.
* Klaudia ściąga z ich Semli informację o dokładnym PUNKCIE awarii, koreluje z awarią poprzedniego statku i pełne skanery w tamtą stronę +/- procent.
    * V: Klaudia odpaliła WSZYSTKIE skanery i informacje. Wszystko. PODBICIE.
    * X: Klaudia nic "nowego" nie zaobserwuje mimo rozpalonych skanerów
    * Vr: silnik jest ekranowany, prawidłowo. Ale Klaudia nie ma POJĘCIA jak doszło do awarii. "promieniowanie kosmiczne przełączające bity".

Analizę Klaudii przerwał komunikat Fabiana "Uciekinier! Uciekinier!" Klaudia widzi jak Helmut zbliża się do Caelfruor, ale Anastazy na pełnej mocy dopalaczy leci w kosmos. I krzyczy.

* X: Klaudia nie ma DOKŁADNEGO punktu startowego. Ona osobiście nie zauważyła tego, ale Persefona ma wszystkie rejestry ze wszystkich operacji. Persefona raportuje awarię jetpacka / silników manewrowych.

Helmut próbuje robić głupi ruch - skoczyć, łapać itp. Fabian mu nie pozwala. "Klaudia, podwójna diagnostyka Lancera Martyna. Martyn - przygotuj się do przechwycenia Uciekiniera.". Martyn mocuje linę, Serbinius leci by przechwycić uciekiniera. Anastazy panikuje. Klaudia NATYCHMIAST robi spikes na wewnętrzne elementy; podejrzewa jakąś dziwną anomalię. Fabian, ponuro "agent Stryk, znajdź tą anomalię. agent Hiwasser, uratuj Uciekiniera."

Martyn próbuje złapać Uciekiniera.

Tr Z +3:

* X: jetpack poleeeeeeeeeeeeeeeeeci, Martyn musi odciąć jetpack zewnętrzny (który jest Skażony)
* Vz: Anastazy przechwycony

Fabian, wściekły "agent Szczypacz, NAPRAW TEN CHOLERNY SILNIK, idź sam. Jak coś - krzycz. Martyn - zbadaj go. Klaudia - diagnostyka Persefony, co się stało..."

(+Martyn widział magię i Skażenie wojenne +Martyn jest paranoiczny w tych tematach +sprzęt Serbiniusa)

Tp Z +2:

* V: 
    * Martyn -> Klaudia (priv): zwymiotował w hełm. Nic mu nie jest, ale są ślady które MOGĄ być Skażeniem.
    * K: Magicznym?
    * M: Tak.
* Vz: 
    * M: Oczywiście jesteśmy jedynymi dwoma magami na pokładzie więc... 
    * M: Spłukiwanie go wodą pomaga. Przesłanka.
    * K: Tu gdzieś jest anomalia, coś dziwnego się dzieje.
    * M: Sprawdź Lancer.

.

* Klaudia poszła sprawdzić Lancera. 
    * (+Klaudia podejrzewa anomalię i się na nich zna, +obecność Lancera na miejscu, +sprzęt Serbiniusa)
    * Tp Z +2
        * X: dowody są podważalne, nawet jeśli "oczywiste"
        * Vz: Lancer ma ślad Skażenia - ale bardzo słaby. Tak jakby... był uderzony IGŁĄ SKAŻENIA w jetpack i tylko troszkę wypromieniowało na Lancera.
            * Klaudia nie widziała czegoś takiego w kosmosie do tej pory, nie osobiście
* Klaudia próbuje zrozumieć co się tu odpiernicza. Używając czystej wiedzy, doświadczeń, książek i wszystkiego co do tej pory widziała (-sytuacja bardzo nietypowa, -anomalia hipotetyczna; Tp -> Tr)
    * V: Teoretycznie, Klaudia czytała o czymś podobnym. Nie w "to się dzieje" ale z perspektywy teoretycznej. (podbicie)
    * Vr: Jest teoria, że w "czwartym wymiarze" jest bańka nadmiaru energii magicznej. I ta bańka w KONKRETNYM OBSZARZE może się przebijać jeśli warunki są spełnione
        * miałabyś anomalię TYLKO statystyczną i TYLKO w konkretnym obszarze.

Klaudia potrzebuje tego jetpacka.

* K: Potrzebuję tego jetpacka
* F: Agent Stryk?
* K: Możemy pójść na chwilę do sali odpraw?
* M -> K: (dobry przy manipulowaniu choć udaje że nie jest) Powiedz mu, że potrzebujesz tego by wykazać niewinność Helmuta czy coś, że to nie jego wina.
* K: Dowódco, sprawa jest taka - nasz sprzęt projektowany jest tak by nawet średnio rozgarnięta kupa liści była w stanie go ogarnąć. Mamy _dzieci_ na pokładzie Orbitera. Muszą być w stanie z niego skorzystać jakby coś się stało. To oznacza, że taka awaria jetpacka jest statystycznie niemożliwa
* F: ...Helmut znowu coś zrobił, prawda? /coś pomiędzy rezygnacją i złością
* K: Tu Helmut jest niewinny - mam raporty z AI. Awaria pojawiła się nagle, bez powodu i ogólnie wygląda to tak... pierwsza myśl śledczych będzie PEBKAC.
* F: Ze strony Helmuta?
* K: Nie, bezpośredniego użytkownika
* F: Anastazego? Komodor Miranda Termann mnie wykastruje!
* K: Dlatego musimy złapać ten jetpack. Wszystko wskazuje na to, że mamy coś statystycznego. I to jest jedyny dowód który może sprawić, że nie będzie to przypisane Anastazemu.
* F: Lub Helmutowi. (widząc, że Klaudia chce coś powiedzieć, przerwał) Nie, nie rozumiesz - Anastazy ma potężną sponsorkę a Helmut nie. Więc...
* F: (rozkaz) Za tamtym jetpackiem! (dokładnie wiedząc, jak to zabrzmi). (A do Helmuta) "Napraw silnik, poczekaj na tej jednostce, wrócimy do Ciebie."
* K: Musimy rozważyć jeszcze jedną sprawę. Być może będę rekomendować wykluczenie obszaru z tras przelotowych. Całkowite.
* F: Nie widzę problemu. Naprawdę.
* K: (widzi problemy - logistyka, polityka itp. ale - nic nie mówi, po co) Między innymi dlatego zależy mi na tym jetpacku

.

Serbinius jest niezłą jednostką, jest szybką korwetą z dobrymi czujnikami, ale gonimy malutki jetpack. I to trochę czasu minęło.

(+there is no stealth in space, +trajektoria wyliczona, -troszkę czasu minęło i mógł się wbić w Caelfruor)

Tp +3:

* V: (podbicie) Serbinius skutecznie dogonił jetpack
* V: Martyn, zaawansowany advancer, przechwycił jetpack bez kłopotu i przekazał go Klaudii.

Klaudia ma silne napromieniowanie PUNKTOWE. JEDEN PUNKT W ŚRODKU JETPACKA. To jest zgodne z teorią o spike'ach / impulsach przebicia z czwartego wymiaru. Natychmiast Klaudia wysyła to do autora teorii o Anomaliach Statystycznych jako dowód. I to daje Klaudii bonusowe punkty, daje jej znajomości ORAZ fortyfikuje Helmuta. I daje opcję wyłączenia terenu z lotów.

Cała reszta pracy z tą anomalią to będzie Klaudia + autor teorii + dużo studentów i magistrów żmudnie mapujących ilości wypadków i awarii, nawet tych samonaprawionych by zlokalizować obszar.

I tak odkryto Anomalię Statystyczną, którą nazwano Anomalia Currafrang.

## Streszczenie

Helmut próbuje optymalizować substrat fabrykatorów na Serbiniusie, co niepokoi Klaudię. Tymczasem pojawia się seria niewielkich awarii na różnych jednostkach, Serbinius pomaga. Klaudia zauważa wzór, ale dopiero jak członek Serbiniusa ma awarię jetpacka znajduje dowód - to Anomalia Statystyczna, niewykrywalna na Primusie. Współpracując z teoretykiem z Orbitera mapują obszar tej Anomalii i wyciągają wszystkich z kłopotów.

## Progresja

* Klaudia Stryk: znajomość z Nikodemem Dewiremiczem, teoretykiem Anomalii Statystycznej z Orbitera. Nikodem jej zawdzięcza dowód że jego teoria działa.

### Frakcji

* .

## Zasługi

* Klaudia Stryk: na podstawie dużej ilości drobnych awarii zidentyfikowała, że dany obszar kosmosu jest niebezpieczny. Znalazła dowody na Anomalię Statystyczną i przekazała je teoretykowi Orbitera, Nikodemowi. Wspólnie zamapowali Anomalię i wyciągnęli Helmuta z potencjalnych problemów z Mirandą.
* Martyn Hiwasser: jako advancer uratował Uciekiniera Anastazego odcinając odeń jetpack, potem uratował jetpack jak się okazał być dowodem Anomalii Statystycznej. Jako lekarz odkrył, że Anastazy jest Skażony i przekazał to Klaudii. Próbuje moderować młodego Anastazego - ważniejsza jest pomoc ludziom niż piraci i przygody.
* Fabian Korneliusz: dowódca który mimo zmęczenia całej załogi nie ignoruje alarmu Semli na jednostce cywilnej (acz warknął na Klaudię by się spieszyła). Dba o swoją załogę, acz jeśli się da to się chce podlizać Mirandzie Termann i innym siłom wyższym.
* Helmut Szczypacz: koniecznie chce optymalizować wszystkie zużycia materiałów w Lancerach; jego niewielkie modyfikacje prowadzą do tego, że gdy Anastazy zostaje Uciekinierem to nie da się udowodnić jego niewinności. Na szczęście Klaudia umiała - znalazła Anomalię Statystyczną.
* Anastazy Termann: advancer i żołnierz na Serbiniusie; narzeka i szuka ambitnych akcji. Trafił na Serbiniusa "za karę" miesiąc temu, chce się wykazać. Doprowadził do przeszukania przemytu (bez sensu) na jednostce cywilnej.
* Miranda Termann: (NIEOBECNA) komodor Orbitera; przerażająca ciotka Anastazego, która jest słodka dla sojuszników ale koszmarna jak się jest przeciwko niej czy Anastazemu.
* Nikodem Dewiremicz: naukowiec Orbitera (51), teoretyk Anomalii; wyteoretyzował obecność Anomalii Statystycznych i Klaudia wysłała mu dowód, że jego teoria jest prawdziwa.
* OO Serbinius: skutecznie uratował kilka jednostek cywilnych od niewielkich awarii, po czym dostarczył Martyna do ratowania Anastazego i jego jetpacka.

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria, Pierścień Zewnętrzny: okazuje się, że na tym obszarze znajduje się gdzieś Anomalia, która powoduje degenerację sprzętu elektronicznego - anomalia Currafrang.
                * Anomalia Currafrang: niewielka (w skali kosmicznej) Anomalia, czysto statystyczna, uszkadza elektronikę, "bańka z czwartego wymiaru".

## Czas

* Opóźnienie: 6
* Dni: 3


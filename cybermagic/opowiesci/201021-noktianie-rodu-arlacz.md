---
layout: cybermagic-konspekt
title: "Noktianie rodu Arłacz"
threads: legenda-arianny
gm: żółw
players: kić, kapsel, fox
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [201014 - Krystaliczny gniew Elizy](201014-krystaliczny-gniew-elizy)

### Chronologiczna

* [201014 - Krystaliczny gniew Elizy](201014-krystaliczny-gniew-elizy)

## Punkt zerowy

### Dark Past

_odsyłam do 200916_

### Opis sytuacji

Stan aktualny sytuacji:

* **AMEND: dodajmy Iok-sama, szlachcica który z oddziałem kilkudziesięcioosobowym spróbuje uratować Trzeci Raj przed Elizą Irą.** -> do przyszłej sesji.

### Postacie

Frakcje: i reprezentanci

* Eliza Ira, królowa kryształów
  * chce: zniszczyć wpływ bogów, zdobyć Ataienne
  * ma: Marian Fartel, krystaloformy, moc arcymaga
* Zbigniew Morszczat / Miragent, źródło chaosu
  * chce: doprowadzić do kompromitacji i unieszkodliwienia Trzeciego Raju
  * ma: zainteresowanie nojrepów, miragent
* Robert Garwen, strażnik Raju
  * chce: by Raj przetrwał niezależnie od czegokolwiek
  * ma: przemyt, posłuch
* Celina Szilat, psychotroniczka Termii
  * chce: zobaczyć pełną moc Ataienne, usprawnić Persefony
  * ma: kontrolę nad Persefoną, skille terminuski, lekki servar klasy Jaeger (adv. Lancer).
* Bartosz Gamrak, Grzymościowiec z Wolnego Uśmiechu
  * chce: utrzymać Trzeci Raj, rynek zbytu Skażonych TAI
  * ma: przemyt, scrap-mechy, małą bazę w Ortis-Conticium
* Ataienne, mindwarp Raju
  * chce: odepchnąć Seilię, uratować Trzeci Raj, uwolnić się
  * ma: mindwarp
* Kordelia Sanatios, arcykapłanka Seilii
  * chce: postawić świątynię Seilii w Trzecim Raju
  * ma: zwolennicy, możliwość przetransportowania kasy
* Trzeciorajowcy nienoktiańscy, mieszkańcy Raju
  * chce: poszerzyć kontrolę nad okolicą, być bezpieczni
  * ma: większość populacji Trzeciego Raju
* Izabela Zarantel, dziennikarka Orbitera
  * chce: jak najlepszej opowieści do Sekretów Orbitera
  * ma: umiejętności zwiadu i dobre opowieści
* Anastazja Sowińska, niestabilny arcymag
  * chce: samodzielności, wolności i swobody.
  * ma: niesamowitą moc Paradoksu
* Nikodem Sowiński, kuzyn Anastazji
  * chce: .
  * ma: .
* Juliusz Sowiński, 'rycerz pokonujący smoka'
  * chce: .
  * ma: .

## Punkt zero

brak.

## Misja właściwa

Niestety, przybyły dwa nowe statki do Trzeciego Raju. To "Głos Sowińskiego", którym podróżują "Szturmowcy Sowińskiego" - grupa świetnych agentów dowodzonych przez zapalczywego Juliusza Sowińskiego. Juliusz przybył tu uwolnić Trzeci Raj od "Krystalicznego Smoka" - czyli Elizy Iry. Drugim statkiem jest "Kojący Dotyk", statek humanitarny powiązany z kultem Seilii. Oba statki teraz są zakładnikami Elizy Iry.

Dodatkowo, "Szalony Rumak", dowodzony przez Klausa Rumaka (kiedyś statek Dariusza, który po oddaniu Elizie jednak nic już nie zrobi) ma dość. Ufortyfikowali się, pełen lockdown i chcą odlatywać. Juliusz lub Anastazja mogliby przejąć nad "Rumakiem" kontrolę, ale...

Na szczęście Klaudia znalazła w umyśle Dariusza, ochoczo przeczesanym przez Ataienne ród, który może się nadać. Ród Arłaczów - nie jest tak wielki ani znaczący jak Sowińscy, a słyną z procesowania czystej krwi - krwi nie zmieniającej Wzoru maga. Mają potężne baterie procesowania krwi i mają negatywne historie z noktianami. Skupowali i zbierali noktian po to, by móc ich harvestować na krew i czyścić.

Analizę i spokojne rozważania Ariannie przerwały krzyki. To Juliusz Sowiński, stoi niedaleko Tesseraktu Elizy i krzyczy, że ta abominacja musi zostać zniszczona w imieniu Sowińskich. Przybył tu zniszczyć Elizę Irę i jej dzieła. Arianna robi NIEEEE!!!! i leci go zatrzymać; czy Juliusz pomyślał co się stanie jeśli zniszczy Tesserakt? Eliza zemści się na populacji. Nie, trzeba Tesserakt... PRZYKRYĆ BREZENTEM. Juliusz robi głupie oczy (XXV) ale daje się przekonać. Ale idzie na Elizę TERAZ. Nic go nie zatrzyma.

...to jest, poza Eustachym. (VXVVX). Użył bomb sabotażysty z Orbitera, z nadzieją że Juliusz pomoże mu wyciągnąć go na światło dzienne. Niestety, bomby wybuchły za mocno i za szybko; "Głos Sowińskiego" wymaga napraw. Szczęśliwie, nikt Eustachego nie podejrzewa. Na nieszczęście, Juliusz wziął serwopancerze i wyruszyli pieszo (!!!) na Elizę, nie do końca zdając sobie sprawę z wielkości Kryształowej Pustyni. Garwen po prośbie Arianny dorzucił mu Wanessę Pyszcz jako lokalną przewodniczkę. Wanessa tak go wyprowadzi, że nigdy nie znajdzie Elizy XD.

Po rozwiązaniu na szybko problemu "Szturmowców", Arianna skupiła się na Szalonym Rumaku. Jeśli ten statek odleci, Eliza go zestrzeli i dojdzie do strasznej eskalacji. Arianna szybko pozyskała wsparcie Ataienne (niech ona wpłynie mindwarpem na załogę Rumaka, niech Anastazja przekonuje lepiej) i poprosiła Anastazję, by ta się wzięła w garść. Anastazja jest załamana tym co się działo bo jej świat się rozsypał. Arianna powiedziała, że może - ale musi być arystokratką i wziąć sprawy w swoje ręce, bo Elena tak robiła. Anastazja wydała rozkaz "Rumakowi" i (XVV) jakkolwiek jej nie zaakceptowali, ale zostaną i otworzą statek i nikt nie zrobi nic głupiego. Nie chcą bardziej podpaść Anastazemu.

Tymczasem Klaudia z radością dostarczyła serię smacznych informacji Ariannie i Eustachemu o Arłaczach:

* Robert Arłacz był w Trzecim Raju; ma tu historię i ogólnie nie lubią noktian
* Skupiają się na eksporcie czystej, puryfikowanej krwi
* Pozyskują ją w etyczny sposób - z noktian
* Diana Arłacz wygrała konkurs Eustachego na Orbiterze - jest arystokratką z którą on powinien iść na randkę
* Diana Arłacz jest piromanką, lubi ogień

Tak więc Arianna ma plan. Porwą noktian od Arłaczy a Eustachy będzie idealną przynętą. Zbałamuci Dianę. Eustachy jest NIESZCZĘŚLIWY - nie chce tego robić. Ale... ech.

Klaudia przygotowała informacje o Arłaczach:

* V: wiadomo gdzie są noktianie, w bateriach
* X: wyciekły informacje PO CZASIE, Arłacze będą wiedzieć, że Infernia robiła research
* XX: Maria Gołąb coś podejrzewa przy wizycie Wieprzka; mimo obecności tien Sowińskiej
* X: Maria zrobiła research nt Inferni i przygotowała się militarnie

Ale nikt - NIKT - nie przygotował się na flirtującego Eustachego :D. Diana okazała się WIELKĄ FANKĄ Eustachego, opowiadała jak z koleżankami się biła o to kto jest najfajniejszym i najprzystojniejszym z Sekretów Orbitera. Ona mówiła że Eustachy i wygrała :D. Poprosiła Eustachego, czy on mógłby wysadzić altankę dla niej, próbowała i nie umiała. Eustachy bez problemu wysadził coś, co było chronione przez sentisieć i Diana piszczała z zachwytu.

* O: Diana pragnie takiego życia jak ma Infernia.
* X: Diana jest zazdrosna o Elenę
* O: Eustachy ma _rebound_. Potrzebujemy Diany na Inferni! Gorące serce.
* V: Diana będzie współpracować przy uwalnianiu noktian. Dla Eustachego!

Tymczasem Arianna i Anastazja zrobiły show z pomocą Izabeli. Zwróciły się do Arłaczów, że chcą odzyskać część noktian (50), by uratować postacie w Trzecim Raju. Anastazja poprosiła też o Dianę - chciałaby ją mieć jako swoją przyboczną. Anastazja jest Sowińską; to nic szczególnego jak chodzi o tego typu żądania.

* XX: Maria wyszła i spytała, że przecież Anastazja jest porwana. Arianna natychmiast połączyła się z Nikodemem...
* V: Dla sukcesu Anastazja powiedziała, że współpracowała z Nikodemem by wykryć kto próbował ją zabić. Przez łzy i z zaciśniętymi zębami. Ale jest na kamerze.
* X: Cały ten odcinek 'Sekretów Orbitera' dał złą opinię Ariannie i Anastazji - chcą współpracować z noktianami? Przede wszystkim: z **Elizą**?
* XX: Jola Arłacz powiedziała wyraźnie - nie dostaną noktian. (a po akcji wyrzucą Dianę z rodu)

No cóż. Widząc, że wszystko idzie nie tak, Eustachy, Klaudia i Diana prześlizgują się do systemów Posiadłości. Klaudia hackermaster.

* XXX: Zespół jest wykryty; Maria przysłała tam swoje DeathCommando.
* VV: Udało im się uwolnić 50-60 słabych noktian; część z nich ledwo się rusza.
* V: Klaudia poznaje mroczne sekrety Arłaczy - Robert jest dotknięty krwią Saitaera; jest saitisem.

Arianna dostaje tą informację. Gdy tylko Jolanta zaczyna obrażać Ariannę i Anastazję, Arianna rzuca "halo, ixiońskie sprzężenie panicza Roberta?". I negocjacje (poza kamerami) odbyły się w prosty sposób: Wesoły Wieprzek wraca z 100 noktian (nie 50-60), rozchodzą się z uśmiechem i optymizmem. No a Diana nie jest już magiem rodu, wyrzekli się jej.

Wracając, Izabela nagrała jeszcze fragment komediowy - "Szturmowcy na pustyni". I to jako całościowy odcinek poszło do montażu. Izabela jest zadowolona z ich współpracy :-).

Tak więc, Arianna uratowała SPORO noktian. Jest gotowa na negocjacje z arcymag Irą. Ma przeciw sobie: Trzeci Raj prawie wymarł przez brak zasobów (XXX), Eliza mogłaby znaleźć sabotażystę i pomóc (XX), Ataienne i mindwarp (XXX), ogólne traktowanie noktian (XX). Ale po swojej stronie ma niemało: uratowali noktian (VVV), Arianna jest dobrą negocjatorką (VVV), część załogi Inferni będzie noktianami poza zasięgiem Ataienne (VV), Kramer osobiście chciał pomóc Rajowi (VV), jest wsparcie humanitarne z wielu stron (VV).

Niestety, negocjacje poszły średnio.

* V: Arianna wywalczyła, że Eliza opuści ten teren z Tesseraktem - wróci do Fortecy.
* X: Eliza dała sobie prawo do kontrataku WSZĘDZIE gdzie cierpią noktianie.
* X: Eliza zażądała arystokratycznego regenta z Aurum. Diana? Ale Eustachy nie zgodzi się. Anastazja?
* X: Ataienne... ona jest stałym problemem. Orbiter jej nie odpuści.
* X: Znowu - traktowanie noktian...

W końcu Arianna (z pomocą Kić) użyła ostatecznego argumentu - jeśli Eliza przejmie kontrolę nad Rajem, to wszystko wygaśnie. Nie będzie współpracy. Nie ma szansę na integrację noktian. A przecież oni NIE SĄ JUŻ noktianami; Noctis by ich odrzuciło. Niech Eliza da szansę siłom dążącym do integracji, jak Kramer a nie próbuje wrócić do kultury / enklawy Noctis - to się nie uda.

Eliza odpuściła stan wojenny. Wróci do swojej pieczary. Nadal oczekuje regenta arystokratycznego, ale rozumie, że może się nie udać...

Zostaje już tylko znaleźć sabotażystę, nie?

## Streszczenie

By uzyskać współpracę Elizy Iry, Arianna zdecydowała się "porwać" noktian rodu Arłacz wykorzystywanych do produkcji czystej krwi dla magów. Z uwagi na działania Marii Gołąb plan został odrzucony i wykryty, ale znalezienie haków na Arłaczy rozwiązało ten problem - Arianna i Arłacze współpracują. Nie doszło do porozumienia z Elizą Irą, ale Eliza się wycofała z Trzeciego Raju, ogłaszając się protektorką tego terenu. Sytuacja w miarę ustabilizowana.

## Progresja

* Diana Arłacz: wyrzucona z rodu Arłacz mimo umiejętności sprzęgania się z Sentisiecią za zdradę na rzecz Arianny Verlen.
* Diana Arłacz: zafascynowana i podkochująca się w Eustachym Korkoranie; przekonana, że on ma fetysz że INNI MUSZĄ PATRZEĆ.
* Robert Arłacz: wymaga ciągłych transfuzji (dwóch dziennie) by nie transformować w saitisa; ma niesamowitą kontrolę nad sentisiecią.
* Nikodem Sowiński: publiczne (na wizji) potwierdził, że Anastazja i on współpracowali by znaleźć zdrajcę. Jest bezpieczny przed konsekwencjami dzięki Ariannie.
* Anastazja Sowińska: publiczne (na wizji) potwierdził, że Nikodem i ona współpracowali by znaleźć zdrajcę. Nie może nic już zrobić Nikodemowi przez próbę ratowania noktian.
* Anastazja Sowińska: dostała silny cios reputacyjny - stoi po stronie Elizy Iry przeciwko lojalistom Aurum. Kolaboruje z Elizą! Ale - bonus wśród sympatyków noktian.
* Arianna Verlen: dostała silny cios reputacyjny - stoi po stronie Elizy Iry przeciwko lojalistom Aurum. Kolaboruje z Elizą! Ale - bonus wśród sympatyków noktian.
* Eustachy Korkoran: potężne uczucia opiekuńcze wobec Diany Arłacz. ONA MUSI MIEĆ DOBRZE.
* OO Infernia: Diana Arłacz dołącza do załogi Inferni, ściągnięta przez Eustachego.
* OO Infernia: cios reputacyjny - Infernia kolaboruje z Elizą Irą, co pokazują Sekrety Orbitera. I bonus wśród sympatyków noktian.

### Frakcji

* Aurum - Arłacz: Stracili sporo (25%) swoich "noktiańskich baterii krwi"; muszą je uzupełnić. Wyrzucili z rodu Dianę. Ale ich sekret nie wyszedł na jaw.
* Szturmowcy Sowińskich: ośmieszeni przez 'Sekrety Orbitera', idą na idiotyczną wyprawę szukając Elizy Iry i nawet nie idą w dobrą stronę.

## Zasługi

* Arianna Verlen: zatrzymała Juliusza przed zniszczeniem Tesseraktu Elizy, włączyła Anastazję do działania, negocjowała z Arłaczami uwolnienie noktian a potem z Elizą opuszczenie Raju.
* Klaudia Stryk: shackowała systemy Rezydencji Arłacz z pomocą Diany, odkryła sekrety rodu Arłacz i uwolniła noktian. 
* Eustachy Korkoran: poderwał Dianę Arłacz, która wygrała konkurs; zbałamucił ją i uciekła z nim na Infernię. Wysadził altankę Roberta Arłacza by pokazać Dianie co umie.
* Anastazja Sowińska: Arianna wytrąciła ją z pasywności; wreszcie zaczęła działać jak Sowińska. Wpierw kazała "Szalonemu Rumakowi" się dostosować, potem pięknie zażądała Diany. Będzie z niej wartościowa arystokratka Aurum. Musiała się "pogodzić" z Nikodemem przez Marię Gołąb, czego NIE CIERPI.
* Ataienne: bardzo chce się połączyć psychotronicznie z Anastazją. Nie ma okazji - Arianna jej nie pozwala. Za to ma nowych 100 noktian do zmanipulowania i Raj do trzymania.
* Klaus Rumak: dowódca "Szalonego Rumaka". Uważa, że jego życie się skończyło po zdradzie Dariusza. Anastazja kazała mu słuchać jej rozkazów. Niechętnie, zgodził się.
* Juliusz Sowiński: kuzyn Anastazji i przywódca Szturmowców Sowińskich. Paladyn, chciał zniszczyć Tesserakt Elizy Iry a potem samą Elizę. Łażą po pustyni, prowadzeni przez Wanessę.
* Robert Arłacz: następca Arłaczy, kontroluje sentisieć, dokuczał Dianie - za co ona użyła Eustachego do wysadzenia jego altanki. Wymaga transfuzji bo Skażenie ixionem.
* Jolanta Arłacz: ona - nie mąż - rządzi rodem Arłacz. Doszła do porozumienia z Arianną w sprawie noktian, by przypadłość jej syna nie wyszła na jaw. Twarda; nieźle kontroluje sentisieć.
* Maria Gołąb: bardzo kompetentna i paranoiczna; zrobiła research nt. załogi Inferni i skutecznie wyłączyła Ariannowe i Anastazjowe "Sowińska chce noktian". Świetny szef ochrony.
* Diana Arłacz: Była senti-następczynią, ale po 'korekcji' Roberta została przesunięta na drugi plan. Uciekła z Eustachym na pokład Inferni, odrzucając ród i ratując noktian. Piromanka.
* Eliza Ira: przekonana przez Ariannę, że jeśli przejmie Trzeci Raj to nie ma szans na integrację noktian z astorianami. Zrezygnowała z aktualnej walki - ale będzie chronić Raj.
* Wanessa Pyszcz: wysłana przez Garwena, by prowadziła Szturmowców Sowińskich po Kryształowej Pustyni tak, by oni NIGDY nie znaleźli Elizy Iry.
* Izabela Zarantel: storzyła świetną opowieść i odcinek o pomocy noktianom ze strony Arianny i Anastazji.
* OO Wesoły Wieprzek: pierwszy rajd bojowy w wykonaniu Zespołu - odbić 100 noktian z rodu Aurum: Arłacz. Gościł na pokładzie Anastazję Sowińską.
* OO Szalony Rumak: statek Aurum (Sowińskich), kiedyś Dariusza. Włączył pełen lockdown; Anastazja Sowińska kazała zdeeskalować sprawę. Poddali się i oddali pod jej dowodzenie.

### Frakcji

* Aurum - Arłacz: zarajdowani przez Wesołego Wieprzka; stracili grupę noktian i ich sekret wyszedł na jaw. Stracili też Dianę. Ale poszło nie tak źle jak mogło. Sojusz z Verlen?
* Krystaliczna Neonoctis: Eliza Ira postanowiła stworzyć własną domenę w Ruińcu, chroniąc i pomagając tych, którzy pomagają noktianom - nieważne kim oni by nie byli.
* Szturmowcy Sowińskich: poszli na Kryształową Pustynię zniszczyć Elizę Irę. Wanessa ich tak prowadzi, by nigdy jej nie znaleźli ;-).

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski, NW
                    1. Ruiniec
                        1. Trzeci Raj: pozyskał 100 nowych, słabych noktian do oddania Ataienne. Eliza Ira wycofała Tesserakt oraz swoje siły z Trzeciego Raju. Acz go chroni.
                        1. Pustynia Kryształowa: wędruje po niej Wanessa z grupą Szturmowców Sowińskich by "znaleźć" Elizę Irę ;-).
                1. Sojusz Letejski
                    1. Aurum
                        1. Powiat Niskowzgórza
                            1. Domena Arłacz
                                1. Posiadłość Arłacz: z pomocą Diany Klaudia wbiła się do systemu, shackowała Baterie Puryfikacji i poznała sekret Arłaczów - Robert jest Skażony. Bardzo dobre systemy obronne.
                                  1. Małe lądowisko: miejsce, gdzie wylądował "Wesoły Wieprzek" z Arianną i Anastazją.
                                  1. Baterie Puryfikacji Krwi: trzy z ośmiu baterii zostały opróżnione z noktian; Klaudia je shackowała.

## Czas

* Opóźnienie: 2
* Dni: 3

## Inne

Planowana sesja:

* Na pokładzie kaledik + 24 glukszwajny. Walka z kaledikiem na statku kosmicznym. NAJPEWNIEJ nie ma crashland.
* Kramer żąda, by Zespół jak najszybciej doszedł do tego co i jak. Muszą unieszkodliwić Celinę i mają pomoc Ataienne.
* Miragent podrzuca kryształy wskazujące na Elizę, ale Klaudia bez problemu dojdzie do tego, że to NIE ONA.
* Znaleźć miragenta. Miragent ma rozkaz śmierci. Ataienne rekomenduje Zespołowi, by zostawili jej dyskretnie miragenta.

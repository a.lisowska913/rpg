---
layout: cybermagic-konspekt
title: "Złamane serce Martyna"
threads: młodosc-martyna
gm: kić
players: żółw
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [210918 - Pierwsza bitwa Martyna](210918-pierwsza-bitwa-martyna)

### Chronologiczna

* [210918 - Pierwsza bitwa Martyna](210918-pierwsza-bitwa-martyna)

### Plan sesji
#### Co się wydarzyło

.

#### Sukces graczy

* ?

### Sesja właściwa
#### Scena Zero - impl

Martyn ma do 10 osób dookoła siebie. Kalina, Zabójca i 8 innych osób. Żadnych magów. Osobisty zabójca Martyna jest facetem, nazywa się Jonasz.

Jolanta w którymś momencie uznała, że trzeba poprawić relacje między Aurum i Eternią. Martyn i jego 10 osób wysłani jako grupa reprezentacyjna do Aurum. Celem Martyna - nawiązać kontakty, poznać ludzi i magów, obraz Eterni który jest bardziej korzystny. Edukować, zaprzyjaźniać, oswajać.

* Q: Jak Martyn do tego podchodzi?
* A: Wielokanałowo:
    * pokazuje jak eternijskie podejście i techniki rozwiązują problemy trudne do rozwiązania
    * pomaga ludziom, wspiera ich... magom też, ale triage jest nieubłagalny
    * z przyjemnością wchodzi w pojedynki by "być jednym z nich". Nie używa simulacrum. Tylko na pięści i ew. broń krótką. Preferuje pięści.
    * imprezy, przemowy.
    * jest widoczny, znany, pomocny i nie miesza się w politykę - ale miesza się w pomoc.

Martyn siedzi w pałacyku (dla gości) i odpoczywa po ciężkim dniu. Przychodzi lekko zdziwiona Kalina - Martyn ma gościa. Przedstawił się jako Adrian i pokazał herb Mysiokorników. Kalina i Martyn są małżeństwem na tym etapie. Adrian jest w podobnym wieku co Martyn, wygląda trochę znajomo. Na widok Martyna brwi się unoszą. "Czyli to jesteś ty". Martyn poznał Adriana Kozioła. W dobrej formie - wysportowany, umięśniony...

Adrian powiedział Martynowi, że nie żywi do niego urazy za przeszłość. Martyn nic nie wiedział o Adeli - dopiero teraz się dowiedział. By zneutralizować przeszłość, Martyn przedstawił swoją żonę - tien Kalinę Hiwasser. Nie interesują go inne kobiety. Ustatkował się. Noctis pokazało mu że musi - miał ogromny _success rate_ podczas wojny... i ogromny _casualty rate_...

Adrian nie będzie przyjacielem Martyna. Ale się jakoś dogadają. Nie czuje do niego przyjaźni - ale jest 'neutral careful'.

Martyn nie chce tego tak zostawić. Zaprosił Adriana na godny posiłek i powspominali czasy wojny, przeszłość, jak to jeden z Cieniaszczytu do Aurum a drugi do Eterni (i stamtąd do Aurum). Chodzi o pozytywną relację (nie przyjaźń, nie zaufanie) opartą na wspólnych przeżyciach i na tym, że walczyli przeciwko koszmarowi Noctis.

TrZ+2:

* XX: Adrian can't let it go. Nigdy nie będzie między nimi nic poza chłodny profesjonalizm. Martyn zostawił za duży ślad "to cholerny podrywacz".
* V: Adrian i Martyn nie chcą wyrządzić sobie wzajemnie szkody. Jest szacunek wynikający ze skuteczności ich obu. Nie chcą ze sobą walczyć.

Rozstali się z uprzejmością i chłodem tam, gdzie kiedyś była potencjalna przyjaźń. Z jednej strony wojna, z drugiej przeszłość w Cieniaszczycie.

Ostatni komentarz Kaliny? "To nie poszło za dobrze".

#### Scena Właściwa - impl

(szczęście 6v6) -> VXV

Martyn się budzi w szpitalu. W miarę szybko może się pozbiera z pomocą lekarzy. Na razie - jest słaby, półprzytomny, nie do końca czuje co się dzieje, nie ma mocnego połączenia z arkin. W pokoju Martyna stoi jeden z arkin Jolanty, z jej strażników.

* "Amadeusz?"
* "Zostałeś ranny, odpoczywaj"
* "Nie czuję arkin. Nie czuję... jej"
* "Kwestia leków. Odpoczywaj."

Trzy dni później. 

Martyn wyszedł ze sztucznej komy. Amadeusz czuwa. Martyn już kontaktuje.

* "Gdzie ona jest?
* "Nic nie mogliśmy zrobić."
* "JA mogłem coś zrobić!"
* "Próbowałeś. Dlatego oberwałeś."
* "Nic nie pamiętam. Nic... nie pamiętam..."

Tego dnia mieli iść na większą imprezę. Tam do tego doszło? Martyn zażądał od Amadeusza - chce zobaczyć jej ciało. Może da się ją przywrócić. On jest najlepszym magiem leczniczym jaki jest i ma krew i ma arkin i jeśli ktokolwiek to on może i ona nie mogła zginąć nie tak nie teraz...

Amadeusz podszedł do Martyna i wyjaśnił. ONA NIE ŻYJE. Odeszła. Nie ma jej. Martyn nie przyjmuje tego do końca do wiadomości. CO SIĘ STAŁO?

Impreza była typu "dożynki". Mieli być, pokazać się, zaprezentować. Jedna z wielu tego typu imprez. Ślady nie są pełne - ale wszystko wskazuje na to, że Kalina - okrzyk bólu - i upada. Martyn próbował łapać. Snajper? Trafiło w niego, nie w nią. A potem ona. A potem została trafiona rakietą z napalmem. Szczęśliwie dla niej, już nie żyła. Zdążyli zebrać ślady, ale Martyn już nie ma czego szukać.

Martyn chce porozmawiać z Jolantą Kopiec. Nie może jej wskrzesić, ale może może ją INKARNOWAĆ? Jolanta - nie. Na świeżo - tak. Ale nie z popiołów. POPIOŁÓW? Jolanta wyjaśnia - jego wyciągnęli, ona umarła. Za dużo czasu minęło na inkarnację. Nie stanie się bronią, nie stanie się jego apteczką. Nie ma jej. Nie w Aurum. Inny Pryzmat. To co się da w Eterni jest niemożliwe w Aurum.

Jolanta poprosiła Martyna, by nie zniszczył całkowicie sytuacji z Eternią. Dużo osiągnął z Kaliną. Niech tego nie zmarnuje. "Ani ona ani wasze dziecko by tego nie chciało".

Dziecko. Martyn nie wiedział, że Kalina jest w ciąży. Nikt nawet nie wie, czy to był chłopiec czy dziewczynka.

That's when Martyn snapped.

Martyn chce zrobić simulacrum, ale inaczej - przyjąć energię arkin by przypomnieć sobie, zobaczyć detale, widzieć wszystko. Chce móc zobaczyć scenę wszystkimi oczami. Jolanta mu nie pozwala - w tym stanie Martyn uruchomi simulacrum (tak nędzne jak jest) i wszystkich pozabija, łącznie z sobą. Musi zrobić to inaczej. Martyn twierdzi, że Jolanta nie jest w stanie go zatrzymać. Jolanta odwołuje się do tego kim Martyn jest i do jego odpowiedzialności za innych. Za jego arkin. Za wszystko co zrobił.

TrZ+3:

* V: Jolanta powstrzymała Martyna. Odpowiada za tych ludzi. To jest 'eternian way'. To jest to o co walczył. Nie może w chwili słabości odrzucić tego wszystkiego i stać się takim potworem jak magowie Aurum.

Plus, Jolanta przypomniała Martynowi, że on ma dostęp do magów Aurum. Sam zebrał tą wiedzę. Martyn odbił - to najpewniej zrobił ktoś z nich. Aurum. Noktianie. Jolanta - może masz KOGOŚ komu może zaufać. Martyn stwierdził, że nie ma takiej możliwości. Nie może ufać nikomu. Jolanta chcąc Martyna ustabilizować - niech pamięta, że ma obowiązki. Martyn odpowiedział, że nie zawiedzie w obowiązkach. Niech tym się nie martwi.

I pierwsze gdzie Martyn poszedł to syntetyzatory biomagiczne. Koktajle - stabilizator emocjonalny, wzmocnienie umysłu. Tabletki stabilizujące. Koszmary związane z Noctis powróciły jeszcze silniej niż kiedykolwiek, a teraz nie ma Kaliny która je neutralizuje...

Hipoteza Martyna - Kalina była celem, bo to ostrzeżenie dla Martyna. To pasuje do kultury Aurum.

Druga hipoteza Martyna - zabójca nie ma znaczenia. Znaczenie ma mocodawca. Ale on da radę znaleźć mocodawcę.

Martyn -> Amadeusz. Czy znaleziono broń itp? Znaleziono wyrzutnię, ale nie znaleziono karabinu snajperskiego. Wiadomo, skąd strzelano (oczywiście, po fakcie). Dostali się tam ochroniarze, ale za późno. To jest teren neutralny, słabiej chroniony i pozbawiony wpływów politycznych i sentisieci. Czyli mniej podtekstów poza rozmową. Magowie Aurum sobie ten teren cenią i ten zamach jest formą tabu. Mag Aurum który to by zrobił dla demonstracji ma przechlapane u wszystkich rodów.

Nowa hipoteza Martyna - to sprawa osobista. Ale jeśli tak, może wykorzystać maga Aurum do pomocy.

Następny dzień.

Do pałacyku Martyna przybył znowu Adrian. Martyn ubrany jest w mundur wojskowy. Stopień chorążego. Adrian chce mu pomóc. Zebrano grupę, która ma pomóc zrozumieć co się stało. Adrian powiedział, że zrobiono rekonstrukcję wszystkich obecnych, zintegrowano zapisy sentisieci. Adrian mówi, że wszystko wskazuje na to, że to Adela.

Adrian pokazał zrekonstruowane zapisy. Niezbyt czytelne ale rozpoznawalne echa. Jest starsza, ale to ona. Adrian niestety nie wie, gdzie Adela się ukrywa. Martyn stwierdził, że to nie problem. Tylko kwestia czasu. Będzie chciała z nim porozmawiać. Nawet jeśli nie... są metody. Adela była leczona w Cieniaszczycie. Są tam jej próbki krwi. Adrian WIE, że Martyn nie może wrócić do Cieniaszczytu. Zdobędzie dla Martyna te próbki. Martyn podziękował.

Gdy Adrian sobie poszedł, Martyn wyjaśnił Amadeuszowi - Adela wie, jak Martyn działa. Wie, co potrafi. Wie, że Martyn ją znajdzie. Więc przyjdzie do niego pierwsza. Albo go zabić albo porozmawiać. A Martyn będzie gotowy. Amadeusz - "jak dobrze i długo się znaliście?" Martyn - "parę dni, rozmawialiśmy. Tylko.".

* "Ona wie, że żeby ratować dziewczyny, których nigdy nie widziałem, odrzuciłem wszystko co miałem w Cieniaszczycie wiedząc, że najpewniej zginę. Jeśli to pamięta, wie, że nie spocznę póki jej nie dopadnę. Dlatego uważam, że przyjdzie dopaść mnie pierwsza." - Martyn.

Martyn ma następujący sposób działania:

1. "wyekspediował" Adriana do Cieniaszczytu po próbki krwi Adeli.
2. zabezpieczył arkin. Niech jego sieć chodzi pod osłoną Amadeusza itp.
3. przygotowuje stymulanty bojowe, które łączy z innymi środkami które na niego działają.
4. wyłącza większość zabezpieczeń w pałacu i robi to ostentacyjnie.
5. informuje Jolantę co chce zrobić i prosi ją, by - jeśli on zginie - zostawiła temat. Bo to jego echo.
    1. Jolanta takie rzeczy rozumie. Próbuje odwieść, ale Martyn się uśmiecha.
        1. Martyn zniszczył życie Adeli. Adela zniszczyła życie Martyna.
        2. Jeśli Adela nie przyjdzie po zaproszeniu, Martyn wyśle na nią agentów. Jak obiecał.
        3. Jeśli Adela zabije Martyna, wyzdrowieje. Ma szansę. Niech Eternia jej daruje.
        4. Jeśli Martyn zabije Adelę, naprawi coś, co kiedyś zniszczył.
6. zaprasza reportera z "Paktu".

Martyn przyjmuje reporterkę z Paktu, będąc w mundurze eternijskim. Chorąży, lord. Ze wszystkimi odznaczeniami wojennymi (a sporo ich ma). I z bronią (pistoletem). Reporterka powinna być odpowiednio zaskoczona i pod wrażeniem (jest). Martyn wyjaśnił Wanessie co zrobił i odnośnie przeszłości w Cieniaszczycie. Wyjaśnił, że znajdzie Adelę. I jedyny sposób jak Adela może się uratować to przybycie do niego. I żeby wzmocnić sygnał - powiedział, że Adela zabiła jego żonę i nienarodzone dziecko.

Martyn na serio wyłącza zabezpieczenia. Wierzy, że jego stymulanty, magia i broń są wystarczające. Plus, jeśli on zginie, arkin będzie przejęty przez Jolantę a on serio nie ma po co żyć w chwili obecnej. To nie jest deathwish, ale jeśli umrze... so be it.

Konflikt WSTECZNY - jak bardzo Martynowi z Kaliną u boku udało się osiągnąć cel relacji Eternia - Aurum?

ExZ+3:

* XXX: Aurum boi się Eterni. Eternia ma dużo ludzi, niewolnictwo i jest GROŹNA. Nawet Orbiter ich nie chce.
* VV: Ale Aurum widzi, że Eternia ma też dobrych ludzi i da się z nimi pracować.

Efekt tego co Martyn zrobił na Aurum i ogólne relacje Eternia - Aurum.

ExZ+4:

* X: Widać straszną różnicę kulturową między Aurum i Eternią. Żaden mag Aurum by tego nie zrobił, żaden mag Aurum by nie ryzykował dyplomacji.
* V: Eternianie nie są potworami. Martyn celuje tylko w "tych winnych".
* X: Wiadomość dotarła do Adeli, ale nie osiągnął efektu który chciał. Adela "zaprosi" Martyna.
* Xz: Martyn jest zimny. Zabili mu żonę a on... tak? To nienaturalne i nieludzkie. Kalina była lubiana. Martyn... nie żałuje? Jego się boją. De facto to kończy jego misję w Aurum. Nie zaufają mu - ktoś tak zimny z takim podejściem nie pasuje do Aurum które nie widziało wojny.

De facto - Martyn nie zniszczył relacji. Ale doprowadził do tego, że jest persona non grata w Aurum i na jego miejsce może przyjść "prawidłowy" ambasador. Niech skończy tu co chce (bo nie wiadomo co zrobi), ale potem... idź być Martynem gdzieś indziej.

W pałacyku są obszary całkowicie niestrzeżone i niepilnowane. Dwa dni po wydaniu artykułu, rankiem, na stole Martyn znajduje kopertę. Niepodpisana, niezaadresowana, ale wyperfumowana z odciśniętym całusem. Martyn w rękawiczkach po badaniu pola magicznego otwiera. W środku arkusik, koło niego obrączka. Na arkusiku - koordynaty, serduszko i "przyjdź sam". 

Martyn przekonał Jonasza, by ten został na skraju lasu. Martyn wyjaśnił - on idzie tam zabić Adelę. Jeśli Aurum ją dorwie, zrobią pokazówkę. Bo ona zabiła dyplomatę Eterni na terenie tabu. Martyn chce ją zabić bezboleśnie. Cut the sickness. Chyba to przekonało Jonasza.

Martyn idzie tam. Sam. Ale przygotowany.

Teren to teren dzikiej sentisieci. Niebezpieczna, dzika sentisieć - nie skrajnie niebezpieczna, ale wystarczająco niebezpieczna. Martyn spokojnie idzie na spotkanie z Adelą. Przyjdzie - lub nie.

Gdy Martyn zbliżył się, było już ciemno. Jak się zbliżył, zobaczył coś na kształt chatki leśniczego. Martyn zawołał Adelę. Nie ma odpowiedzi. Domek dokładnie na koordynatach jakie Martyn dostał. Martyn czeka - nie wejdzie tam. Ona jest w świetle, on w ciemności.

Adela woła go z drzwi "chodź", ubrana w strój gosposi. Zrobiła domek, jedzenie itp. Martyn jej łagodnie wyjaśnia, że nigdy nie będą razem, że zabiła wszystko, co kochał. Adela zrozumiała, że musi się bardziej starać.

Martyn ma straszny dylemat. Czy powinien ją zabić teraz, jak nic nie wie? Czy powinien ją uświadomić co zrobiła i wtedy ją zabić? Ona "nie rozumie", ma formę psychozy. Dla niej ona i Martyn powinni być razem i musiała to zrobić. Martyn zaginął, ona go znalazła i usunęła to, co stało między nimi.

Adela marzy o tym, że Martyn będzie z nią. Zrobi wszystko, by go uszczęśliwić, nawet jeśli ma zginąć. Martyn pokazał jej broń, wyjaśnił, że jedno z nich tutaj umrze. Do niej to nie docierało. Ona żyje w swoim świecie. A Martyn wie, że ona nie wyjdzie stąd żywa - Aurum zrobi z niej przykład.

Martyn próbuje wyciągnąć z niej historię, szukając, czy _ktoś_ jej nie wykorzystał do ataku na Martyna, Kalinę, Eternię... czy ktoś za tym stoi czy to tylko jedna biedna Adela która jest uszkodzona. Adela z radością wszystko opowiada, całą ścieżkę do Martyna. Nie jest to super spójna ścieżka ale nie pasuje do kontroli umysłów ani do skomplikowanych manipulacji. Zrobiło się głośno o Martynie i Kalinie. Adela opuściła Cieniaszczyt by szukać Martyna i natknęła się na gazetę Aurum. Potem zobaczyła, że ma przy sobie Kalinę.

Martyn podchodzi do Adeli, całuje ją, przytula, mówi, że będzie dobrze, od teraz będzie dobrze... po czym - coup de grace. Czysta, bezbolesna egzekucja.

Wiadomość po arkin do Jonasza "deed is done". Spalą zwłoki - nic, co może być wykorzystane przez Aurum. Zero "pastwienia się nad zwłokami", zero inkarnacji, nic. Let her rest...

## Streszczenie

Martyn i Kalina są małżeństwem. Ambasadorzy Eterni w Aurum, ocieplają wizerunek. Niestety, morderca zabija Kalinę. Martyn znajduje mordercę i okazuje się, że to Adela, którą kiedyś Paradoks Martyna zniszczył. Martyn robi jej bezbolesny coup de grace i opuszcza zarówno Aurum, Eternię jak i arkin - odchodzi do Orbitera.

## Progresja

* Martyn Hiwasser: od tej pory nie ujawnia się, nie mówi o sobie, nie mówi o przeszłości. Jest cieniem. I nie umie już działać w Eterni - za dużo wspomnień. Przestaje być lordem eternijskim - przekazał arkin Jolancie.
* Martyn Hiwasser: jego paranoja i te wszystkie mentalne sprawy się u niego nasiliły. Od tej pory żyje na lekach i supresorach.
* Martyn Hiwasser: z perspektywy Aurum jest niebezpieczny i niezwykle zimny. Stracił posadę ambasadora. Unikany.

### Frakcji

* .

## Zasługi

* Martyn Hiwasser: stracił Kalinę - swoją żonę i serce ORAZ nienarodzone dziecko. Znalazł mordercę - Adelę - po czym się z nią spotkał. Gdy okazało się, że Adela nie kontaktuje i chce być z Martynem, on ją przytulił i zrobił _coup de grace_. Po czym opuścił Aurum, Eternię i arkin.
* Kalina Rota d'Kopiec: KIA. Ukochana żona Martyna, w pierwszych etapach ciąży, zastrzelona przez Adelę.
* Adrian Kozioł: wyszedł na ludzi, do rodu Mysiokorników, ich szef ochrony. Pomógł Martynowi zlokalizować Adelę.
* Amadeusz Martel d'Kopiec: najbardziej zaufany przez Martyna agent Jolanty. Pomógł Martynowi się zregenerować po zamachu i chronić arkin.
* Jolanta Kopiec: uratowała Martyna przed zrobieniem jakiejś formy reverse simulacrum czy innej głupoty gdy on miotał się po śmierci Kaliny.
* Wanessa Ogarek: reporterka Paktu wezwana przez Martyna. Pod ogromnym wrażeniem Martyna, który zimno mówił jej plan by zwabić Adelę i uspokoić Aurum. 
* Adela Kołczan: KIA. Skażona Paradoksem Martyna dawno temu, zabiła Kalinę. Potem udała się robić domek marzeń dla niej i Martyna. Martyn ją zabił przytulając i mówiąc, że wszystko będzie dobrze.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Aurum
                        1. Pięciokąt Ichtis: lokalizacja pod wpływem PIĘCIU sentisieci; częste problemy i awarie. Graniczy m.in. z Sowińskimi i Lemurczakami. Niektóre obszary Pięciokąta są pod wpływem kilku sieci, niektóre są całkiem wolne i nieclaimowalne.
                            1. Powiat Pentalis
                                1. Pałac dla gości: objęty przez Martyna i Kalinę, gdzie pełnili rolę ambasadorów eternijskich.
                            1. Powiat Dzikiej Sieci: Adela zrobiła domek marzeń dla siebie i Martyna; Martyn ją tam zabił.


## Czas

* Chronologia: Zmiażdżenie Inwazji Noctis
* Opóźnienie: 1154
* Dni: 6

## Inne

INFECTED RAIN - Postmortem Pt. 1 : https://www.youtube.com/watch?v=YqoMCCW7jrc&list=RD2wA0skGcp5o&index=2 

## Konflikty
* 1 - Adrian nie będzie przyjacielem Martyna - za dużo było. Martyn nie chce tego tak zostawić.
    * TrZ+2
    * XXV: Adrian can't let it go. Nigdy nie będzie między nimi nic poza chłodny profesjonalizm. Ale nie chcą wyrządzić sobie wzajemnie szkody. Jest szacunek do umiejętności.
* 2 - Jolanta Martynowi nie pozwala - w tym stanie Martyn uruchomi simulacrum (tak nędzne jak jest) i wszystkich pozabija, łącznie z sobą.
    * TrZ+3
    * V: Jolanta powstrzymała Martyna. Odpowiada za tych ludzi. To jest 'eternian way'.
* 3 - Konflikt WSTECZNY - jak bardzo Martynowi z Kaliną u boku udało się osiągnąć cel relacji Eternia - Aurum?
    * ExZ+3
    * XXXVV: Aurum dalej boi się Eterni i na dystans; ale Eternia ma też dobrych ludzi i da się z nimi pracować
* 4 - Efekt tego co Martyn zrobił (wywiad z Wanessą) na Aurum i ogólne relacje Eternia - Aurum.
    * ExZ+4
    * XVXXz: różnice kulturowe, Martyn persona non grata, nowy lepszy ambasador, Eternianie nie są potworami (ale Martyn tak)

---
layout: cybermagic-konspekt
title: "Przygoda, randka i porwanie"
threads: rekiny-a-akademia
gm: żółw
players: darken, vizz
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [201013 - Pojedynek: Akademia - Rekiny](201013-pojedynek-akademia-rekiny)

### Chronologiczna

* [201013 - Pojedynek: Akademia - Rekiny](201013-pojedynek-akademia-rekiny)

## Punkt zerowy

### Dark Past

Dwóch młodych magów rodu Sowińskich, Henryk i Daniel żyją w morderczym świecie mediów społecznościowych. Przybyli do Zaczęstwa szukać przygód. W Arenie Migświatła podpadli Kacprowi Bankierzowi - zrobili z nim mały pojedynek i (z ukrytą pomocą Laurencjusza) dali radę go pokonać używając artefaktów, na Nieużytkach Staszka. Tam zakończyła się kariera Laurencjusza; dorwał go Tymon Grubosz i zamknął za zakłócanie porządku.

Henryk i Daniel zapoznali się z Trianą. Trianie się spodobali oni i ich misja - stwierdziła, że to ŚWIETNA okazja na _double date_ z Lilianą; stworzyła więc historię o straszliwym upiorze Abominusie, który z nawiedzonego domu kontroluje sytuację; ale w domu mieszka tajemniczy mag, który nie chce nikogo spotykać i z nikim rozmawiać. I... zaprowadziła ich do jednego z wyjść ewakuacyjnych Rezydencji Porzeczników. Po czym Triana odwraca uwagę ojca i poprosiła TAI dowodzącą domem, by ta nie zwracała uwagi na 'testy' ze strony Porzeczników (i o czym Triana nie wie, też postaci graczy).

Po aresztowaniu przez Tymona Laurencjusza, ten rozpaczliwie ruszył do swoich kontaktów. Trzeba RATOWAĆ młodych Sowińskich - i lokalne Rekiny nie mogą o tym wiedzieć...

### Opis sytuacji

.

### Postacie

.

## Scena zerowa

CEL: demonstracja graczom Dark Past i kontekstu. ROLA GRACZY: Laurencjusz i jego pachoły.

Henryk i Daniel Sowińscy stoją naprzeciw Kacpra Bankierza, niedaleko Nieużytków Staszka w Zaczęstwie. Nie są w stanie go pokonać, nie są w stanie wycofać się z pojedynku. Kacper Bankierz przygotował się do działania; są 2vs1 - to jedyna szansa by ten pojedynek był uczciwy (klasa Kacpra dramatycznie przewyższa Sowińskich). Laurencjusz oraz jego sojusznicy przygotowują dywersję - gdy będzie walka, mają zamiar wpiąć się w pole magiczne i zrobić POZORY Paradoksu i podpalić jakieś namioty na Nieużytkach.

Po pierwsze, lepiej będzie to wyglądało na streamie. Po drugie, chaos deeskaluje; gdy Sowińscy i Bankierz będą osobno, ktoś pójdzie do Kacpra Bankierza i wyjaśni mu o co chodzi i da odpowiednie zadośćuczynienie.

Oczywiście, nie mogło wszystko pójść perfekcyjnie - (VV): narzędzia do wpinania się w aurę i skuteczny pomysł bardzo pomogły. Ale jakkolwiek (VV) nikt nie wie, że doszło do ingerencji w pojedynek i bracia Sowińscy nie będą tak chojraczyć w przyszłości, (X) Kacper się zorientował że to było oszustwo. "The game is on".

Jako, że doszło do pożarów i straty mienia, pojawił się w końcu terminus. Tymon Grubosz. Aresztował Laurencjusza i jego pachołów i wsadził ich do aresztu. Laurencjusz w rozpaczy wezwał na pomoc swoje kontakty w Zaczęstwie - Roberta Pakiszona i Gabriela Ursusa...

Q: Czym tak młodzi Sowińscy zaleźli za skórę Bankierzowi?
A: Publicznie zarzucili mu, że jest niekompetentny; obraza na honorze.
Q: Czemu ważne jest to, by nikt nie wiedział (a na pewno nie terminusi) o tej sprawie? Żeby to było poza terminusami?
A: Daniel naprawdę nienawidzi Kacpra. Przekonał Henryka, że KACPER TO WRÓG. Jak to wyjdzie do Aurum, będą reperkusje.
A: Terminusi nie rozumieją mrocznego świata VirtStream, gdzie młodzi mieszkańcy Aurum próbują coś osiągnąć w życiu.
Q: Czemu ważnym jest to, by sprawa nie trafiła do Rekinów?
A: Przyczyny polityczne - jeśli Rekiny się tym zajmą, dojdzie do podziału frakcji i będą tu lokalne waśnie rodów. Sowińscy są zbyt znaczącym rodem.
Q: Czemu Laurencjusz najbardziej ufa Robertowi?
A: Robert nie wyleciał z uwagi na wsparcie Laurencjusza i Sowińskich. Jest dłużnikiem, bardzo mocno.
Q: Co pomaga wam w tych akcjach i działaniach - śledzenie ich, wpływanie na nich...
A: Rezonans zostawił ślad magiczny jaki będzie przekazany Gabrielowi i Robertowi w skanerze; jak Sowińscy będą czarować, Zespół wie gdzie są.
A: Sowińscy nie poruszają się cicho i mają kasę. Lifestyle influencera na VirtStreamie.
A: Znają Roberta - nie jako osobę, która pomaga im z cienia a jako "ciekawego tubylca z fajnymi psychoaktywnymi środkami". Taki gość od zamawiania taksówki.

## Misja właściwa

Laurencjusz poprosił Gabriela i Roberta o wsparcie, żaląc się, że małpowaty terminus (Tymon) jest nieżyciowy i nie da się z nim rozmawiać. Wie, że podpalił namioty. Co z tego - wypłaci odszkodowanie, zapłaci nawet więcej; w Aurum by tak zrobił i by nie było problemu, Tymon jest jakiś nieżyciowy. Poprosił Gabriela i Roberta, by zajęli się młodymi Sowińskimi - ani oni, ani Kacper Bankierz nie zostali aresztowani. Tak więc Sowińscy się błąkają po okolicy i SZUKAJĄ PRZYGÓD. Zadaniem Zespołu jest dostarczenie im epickich przygód "na prowincji", które da się opisać na VirtStream i by młodzi nie mieli pojęcia, że ktoś im pomaga.

Zespół przyjął to zadanie. Płacą? Płacą. A Gabriel jest trochę zafascynowany tym, że to sami SOWIŃSCY potrzebują jego pomocy. Tak więc Robert + Gabriel sformowali zespół i zaczęli kminić nad planem.

Pierwotny plan był dość prosty: DAMSEL IN DISTRESS! Opowieść wygląda tak:

1. Mają młodą dziewczynę, której ktoś porwał siostrę.
2. Owa młoda dziewczyna spotyka Daniela i Henryka. Robi na nich wrażenie (z czym pomagają feromony Roberta)
3. Podczas rozmowy w restauracji, dziewczyna jest porwana przez sektę LUDZI WĘŻY.
4. Henryk i Daniel wpadają i ratują dziewczynę na szczycie budowanego biurowca; jest tam wanna. I siostra.
5. Padają sobie w objęcia, H. i D. są bohaterami - ogólnie, epicki sukces.

Teraz tylko kwestia zrobienia tego tak, by zadziałało (znalezienia wszystkich), ale to nie jest taki duży problem. Przez to, że niedawno została rozbita mafia Grzymościa (Wolny Uśmiech), jest więcej osób z jego okolicy szukającej kogoś dookoła kogo da się zaczepić. Tak więc Robert znalazł grupę thugów, którzy nie mają nic przeciwko porwaniu dziewczyny (nic nie ma jej się stać), nieźle dostaną wynagrodzenia. A tymczasem Gabriel skupił się na znalezieniu dziewczyny-do-porwania.

Wybór Gabriela padł na Urszulę Miłkowicz, świeżą uczennicę terminusa. Jest niezbyt bogata, nie ma wpływów, pochodzi z ludzkiej rodziny. Jest uroczo naiwna, ale potrafi się odgryźć jak trzeba - nadaje się do tej akcji. Sprzedał jej tą akcję jako coś pomiędzy treningiem/szkoleniem a misją szpiegowską. Ula ma nikomu nie mówić co to za akcja i jak ma działać. Dodatkowo, ma wyciągnąć od Henryka i Daniela czemu tak się nie cierpią z Kacprem Bankierzem. Gabriel serio chce jej pomóc - przydadzą jej się wpływy i Ula zyska cenne kontakty w Aurum, coś, co zwłaszcza jej się przyda. Ale Ula pyta naiwnie i przezornie co ma powiedzieć przełożonemu. A Gabriel nie chce mu nic mówić...

W rozpaczy, Gabriel zwrócił się do Tukana - rywala Pięknotki (jego przełożonej). Tukan, oczywiście, nie podszedł do Gabriela z pełnym zaufaniem a przez hipernet spytał, co Gabriel odpiernicza. Gabriel poprosił Tukana o pomoc w przekonaniu przełożonego Uli. Tukan wyciągnął z Gabriela pewne detale i doszedł do tego, że tam jest coś do czego warto się przyczepić. Jednak cena pomocy Tukana była bardzo wysoka (XXXX) - zażądał, że wszystko co pójdzie nie tak idzie na konto Gabriela, skoro to mała, sympatyczna robota. Dodatkowo wszelkie zasługi jakie przypadłyby Uli przypadną jemu (nóż Tukana w plecy) plus Gabriel udowodni Laurencjuszowi, że Tukan był nieoceniony. Gabriel niechętnie się zgodził i Tukan załatwił mu (V) dyskretnie wsparcie Uli i wyczyścił wszystkie problemy z dokumentami (oczywiście, outsourcował problem Laurze Tesinik do rozwiązania, przecież on OSOBIŚCIE tego nie będzie robić).

A Robert, który w tym czasie zajmował się tłumaczeniem thugom co mają robić i mówić dostał sygnał alarmowy od tych detektorów gdy młodzi czarują. Co się okazuje? Rzucają czary. Sporo. I nie są w stanie wyjść - rzucają czary teleportacji, przejść itp - ale te czary są zablokowane. Innymi słowy, młodzi _gdzieś utknęli_ i nie umieją się wydostać. Detektor pokazuje, że utknęli w obszarze Rezydencji w Zaczęstwie - w bogatszym miejscu. Z ciężkim sercem, Robert poszedł tam ratować młodych Sowińskich przed kłopotami...

Jakąś godzinę temu: Triana opowiedziała Henrykowi i Danielowi o straszliwym UPIORZE grasującym i opętującym młode, powabne dziewczęta i o tym, że potrzebni są bohaterowie by te dziewczęta ratować. Triana z krótką spódniczką i sporym dekoltem wzbudziła ich zainteresowanie; gdy tylko Triana wskazała Tajemnicze Domostwo, gdzie znajduje się upiór, Sowińscy ruszyli znaleźć wejście. A Triana wyłączyła w tym miejscu (Rezydencja swojego ojca, rodziny Porzeczników) wszelkie alarmy, uciszyła TAI, rozbroiła wszystko co się dało i pokazała młodym wejście przez zamaskowane wyjście ewakuacyjne. Po tym zaczęła odwracać uwagę ojca, niech pomoże jej zrobić ciasto francuskie na urodziny, bo jest delikatne.

Tyle, że Triana nie ma pojęcia, że tam jest WIĘCEJ pułapek i zabezpieczeń niż tylko te podpięte do alarmu centralnego. M.in. zapętlenie przestrzeni (anomalia wzorowana na Pacyfice). Młodzi się tam dostali, wpadli i nie wyjdą. A Triana nie ma o tym pojęcia - odwraca uwagę ojca. I na to wpada Robert, który znajduje sygnał młodych przy Rezydencji.

"Znaleźli własną przygodę? Nie może tak być. Nasza jest lepsza, bardziej efektowna i bezpieczniejsza."

Robert wie, że w tej Rezydencji mieszka Triana i że Triana pochodzi z rodu wynalazców. Skomunikował się z nią na hipernecie; Triana powiedziała, że nie ma teraz czasu (zajęta odwracaniem uwagi ojca). Robert ślicznie poprosił - ma problem z żołądkiem i MUUUUSI skorzystać. Triana, cała podirytowana i ekspresyjna, zgodziła się - niech przyjdzie skorzystać z ubikacji. Wpuściła go, nie zauważając, że niektóre elementy alarmu działają dalej. Ale ona MUSI lecieć do ojca; Robert zna drogę wyjściową - i już jej nie było.

Robert poszedł szczurzyć. A dokładniej, do piwnicy. Przekradł się niedaleko kuchni, będąc pod wrażeniem tego, że najwyraźniej NIKT nie zauważył, że alarmy są wyłączone i dostał się do piwnicy - do laboratorium (gdzie alarmy TEŻ są wyłączone). (XVVVVX) - udało mu się dostać do laboratorium i odszukać glify odpowiadające za anomalię przestrzenną. Szybko wyłączył zabezpieczenia i się schował.

Henryk i Daniel Sowińscy przebili się do laboratorium przez anomalie, dumni z siebie i waląc pełną narrację do kamery. Są przekonani, że udało im się osiągnąć sukces dzięki ich umiejętnościom i kompetencjom. Z dużym trudem znaleźli Kryształ Holoprojekcji, który Triana zostawiła by mogli go znaleźć i zobaczyli jak Liliana odgrywa scenę "nie, zostaw mnie!" i zostaje "opętana przez Upiora" (Liliana jest niezłą aktorką, świetną iluzjonistką i kinetką, więc to był całkiem przekonywujący performance). Chwilę potem Robert (już u góry) włączył alarmy. HENRYK I DANIEL UCIEKAJĄ! PO RAZ KOLEJNY UDAŁO IM SIĘ WYGRAĆ PRZYGODĘ I WIEDZĄ CO OSIĄGNĄĆ!

Triana wypada z kuchni i łapie Roberta za ucho. Co on tu robi! Ona SPECJALNIE rozbroiła alarm! Niech idzie stąd, niech nie waży się stawać na drodze jej randki! Robert lekko zgłupiał, a Triana wraca do kuchni i przeprasza ojca; ona tylko sprawdzała czy alarm działa i TRZEBA PILNOWAĆ CIASTA!

...

Dobra. Gabriel, Ula i Robert spotykają się w kawiarni (Kawiarenka Leopold) i podsumowują fakty - ich ludzie porwą Ulę ale wyraźnie młodzi Sowińscy chcą polować na jakiegoś upiora i Triana jest w to zamieszana. Dobra, olać Trianę. Ich plan jest lepszy. Robert sfabrykował na podstawie potu Uli (mimo jej silnych protestów) POTĘŻNY feromon (VVV). Dzięki temu młodzi będą nią zafascynowani, olać jakiegoś Upiora. W ten sposób plan Triany zawiedzie a ich plan wypali i będzie więcej lajków na VirtStream.

Robert ściągnął młodych do kawiarenki informacją, że ma ważne sekrety; a nie, to nie do nich, pomyłka. OCZYWIŚCIE że przyszli. I faktycznie - zafascynowani Ulą, łyknęli wszystko, co ta im mówi (a Ula wczuła się w misję szpiegowską). I wtedy wszystko się spieprzyło... (XX): najpierw to, że Triana przyszła do kawiarenki, bo oni zamiast polować na upiora to przyszli tutaj. A potem to, że (XX) WBILI SIĘ 'TAJEMNICZY WĘŻOSEKCIARZE' i porwali nie tą dziewczynę! Mieli porwać Ulę, porwali Trianę!!! (dodatkowy (X) oznacza, że Liliana wie i leci Trianie na pomoc).

Gabriel i Robert spojrzeli na siebie niemo, przerażeni tym, że wszystko się sypie. Grupa thugów porwała STUDENTKĘ AKADEMII MAGICZNEJ i o tym nie wie. Na szczęście Ula zachowała zimną krew i gdy Henryk z Danielem popatrzyli ze zdziwieniem co się dzieje, kim byli napastnicy z granatami gazowymi itp, Ula stwierdziła:

"Musieli się pomylić i pomylili ją ze mną! Sama bym się pomyliła."

Pomylić dwie tak różne dziewczyny nie jest łatwo, ale Daniel nie jest szczytem intelektu a Henryk był zajęty patrzeniem na Ulę i przez to nie skupiał się na logice. Dobra, szybko, trzeba naprawić sytuację! Robert jest w ciągłym kontakcie z Ulą; Ula ciągnie ich za język co może się dziać, oni wymyślają jakieś głupoty ("węże kojarzą się z ciepłem więc będą na dachu najwyższego miejsca w Zaczęstwie") a Robert leci przodem i podkłada fałszywe tropy, by młodzi wyglądali na wybitnych detektywów. Ula umiera w środku, ale robi co może (VVV): skutecznie kontrolują czas i okoliczności. (V): nie będzie QAnon czy Pizzagate; na VirtStream będzie wrażenie, że to nie illuminati a po prostu mała skuteczna zamknięta sekta wyznająca węże na prowincji. Niestety (X) Robert nic nie zrobi póki musi ich prowadzić i pilnować. Przynajmniej opowieść na VirtStream będzie epicka...

A tymczasem Liliana Bankierz jest PRZERAŻONA co się dzieje z jej przyjaciółką. Triana została porwana naprawdę! Przez... sektę wyznawców węży? No ok, Liliana nie wiedziała, że jest tu jakaś sekta wyznawców węży. Ale ich znajdzie i zniszczy! Triana ma przy sobie różne gadżety, m.in. lokalizator. Liliana przeleciała przez dom Triany i pożyczyła sobie eksperymentalny model konstruminusa szturmowego; nowy model nad którym pracuje Robinson Porzecznik (on nie wie, że Liliana zwinęła ten model bo ZABEZPIECZENIA NADAL SĄ WYŁĄCZONE). Liliana z konstruminusem i lokalizatorem użyła mocy kinezy i W DŁUGĄ - w kierunku na Trianę. (XXX)

Gabriel wie, że Liliana dotrze do budowanego biurowca zanim dotrze tam Ula i Robert (X). Trzeba ją opóźnić lub przekierować. Tak więc by się nie zdradzić, Gabriel zrobił flarę pokazującą sygnaturę Triany w zupełnie innym miejscu, na Nieużytkach. Liliana jest skonfundowana, nie wie na 100% które miejsce ma wybrać. Na wszelki wypadek wysłała niezbyt sprawny prototyp konstruminusa na Nieużytki (V) a sama poleciała do biurowca. Szczęśliwie, jako, że konstruminus jest niestabilny i niebezpieczny, na miejscu jest Tymon Grubosz (V) który się nim zajmie, co kupuje więcej czasu Zespołowi...

Porywacze przebrani za członków sekty węży fachowo związali Trianę i przygotowują się do "rytualnej ofiary" (czyt, by wyglądać groźnie), ale z powietrza jak pocisk spadła Liliana z falą uderzeniową. Gabriel się postarał by nikt za mocno nie ucierpiał (V); pomaga fakt, że Liliana nie jest wprawną wojowniczką. Ale Liliana zrobiła miejsce koło Triany; odepchnęła porywaczy od swojej przyjaciółki. I NA TO WPADA DANIEL, ULA I HENRYK!

"Ten upiór... rozumiem! On od samego początku dowodził tą sektą!" - odkrywcza myśl Henryka Sowińskiego

Liliana nie ma pojęcia o co chodzi (acz dalej jest przebrana jak królowa wampirów), ale na wszelki wypadek osłania Trianę. Ula (nie wychodząc z roli) łapie Daniela za ramię i krzyczy histerycznie "zróbcie coś!". A Daniel łapie Henryka za rękę, unoszą ręcę i rzucają zaklęcie kombinowane. "BRATERSKA MOC SOWIŃSKICH, UPIORZE, GIŃ!". Liliana patrzy z lekkim niedowierzaniem (a oni nadal są pod wpływem feromonów, niezbyt sprawni intelektualnie i jak na narkotykach) i przygotowuje własny czar bojowy. W odpowiedzi na to Gabriel próbuje wszystkie te czary złapać i uziemić - a zwłaszcza moc Sowińskich.

Nie wyszło. Za dużo emocji, przerażenia porywaczy, za duża siła magiczna, za duża niestabilność Sowińskich na prochach. (XO): doszło do sformowania się efemerydy - WĘŻOWI PORYWACZE SĄ NAPRAWDĘ TERAZ. I CHCĄ POŚWIĘCIĆ TRIANĘ! (X): Liliana ma wprawę w kontroli energii a Gabriel rozmienił się na drobne; ciornęła arystokratami po gruncie, waląc nimi w ściany i przerywając ich czar; plus, odrzuciła węże od Triany. (X): emisje energii wzbudziły zainteresowanie Kacpra Bankierza, który ruszył w tym kierunku. Gabriel w rozpaczy uszkodził grunt pod Sowińskimi; spadli w dół (upadek magicznie Gabriel osłabił) (V). Ula, nie musząca udawać idiotki, zaatakowała efemerydę z pełnią furii niedoświadczonej uczennicy terminusa, która chce się wykazać, zaskakując nawet Lilianę ilością emocji i energii wydanej bez cienia myślenia o defensywach.

Ula szybko przesłała terminuskie kody Lilianie i kazała Lilianie się wycofać i brać Trianę. Jakkolwiek Liliana by chciała pomóc, ale wie że na terenie Szczelińca trzeba słuchać terminusów; bez kwestionowania zrobiła to, czego od niej Ula żąda (V). Szczęście w nieszczęściu.

Daniel i Henryk wydostali się z tego dołu. Zdziwili się, że nie ma upiora; Ula powiedziała, że ich bohaterstwo go egzorcyzmowało. Wszyscy - Gabriel, Robert, Daniel, Henryk i Ula skupili moc by rozwalić efemerydę. Ta akcja jest prawdziwa; Gabriel chroni arystokratów jak kwoka jajko, skupiając się na nich bardziej niż na osłonie zespołu. Dzięki przewadze liczebnej (V) udało im się zbudować pierwszy front rozpraszania efemerydy a konstruminus na Nieużytkach (V) kupił im czas, by Tymon nie zdążył się pojawić na czas. Niestety, Gabriel skupił się na arystokratach (X) więc nie osłonił Uli która nie ma wprawy w defensywach; Ula jest ciężko poparzona magicznie. (X) Tymon się dowie; nie ma opcji tego przed nim ukryć. (XV) i wtedy wpadł Kacper Bankierz uratować sytuację - pełnia mocy i sprzęt w swoim ścigaczu.

Kacper wyśmiał Sowińskich za niekompetencję, ale pomógł - efemerydy trzeba rozkładać. Co inni mają cierpieć. Po czym krzyknął, że muszą zwiewać, zanim pojawią się "psy". I faktycznie, wszyscy się rozproszyli; Ula jednak nie dała rady uciec, Tymon się pojawił i ją dorwał. Gabriel widząc to zawrócił; Ula nie będzie złapana sama. Tymon OPIEPRZYŁ Ulę za to co zrobiła i młoda uczennica terminusa skończyła we łzach.

Mimo, że akcja się udała i Robert nie ucierpiał to jednak Robert zażądał potrojenia nagrody. Laurencjusz ani przez sekundę nie dyskutował...

## Streszczenie

Młodzi i niekompetentni Sowińscy łażą po Zaczęstwie i szukają przygód. Gdy ich dyskretny opiekun został aresztowany, szybko ściągnął Roberta i Gabriela do pomocy - by zrobili przygodę. Jednocześnie Triana chciała podwójnej randki i też robiła przygodę z Lilianą. Skończyło się na tym, że wszystkie przygody się pomieszały, Triana została porwana przez siły Zespołu (!?) i walnęła efemeryda na budowanym biurowcu Gorland. Ale - 'stream must go on'.

## Progresja

* Urszula Miłkowicz: BARDZO nieufna wobec Gabriela Ursusa; tyle jej obiecał a skończyła poparzona i z potężnym OPR od Tymona Grubosza, którego Ula podziwia.
* Gabriel Ursus: poważnie nadwątlił zaufanie Pięknotki, Tymona i dużej ilości terminusów Pustogoru. Przedłożył interesy Aurum - dwóch smarkaczy - nad dobro regionu.
* Tomasz Tukan: uznanie Laurencjusza Sorbiana i kontakty z Sowińskimi; przechwycił lwią część sławy i zasług za akcję Gabriela i Roberta.
* Laurencjusz Sorbian: wpierw próbował przekupić Tymona a potem kłócić się z Ksenią. Zdeptany - ZROZUMIAŁ, że Szczeliniec "tak nie działa". Nie cierpi tego miejsca.
* Kacper Bankierz: Henryk i Daniel Sowińscy mają u niego dług honorowy, który ma zamiar wykorzystać w podły dla nich sposób.
* Henryk Sowiński: VirtStream był wyjątkowo popularny, dostał sporo subskrybcji. Nauczył się sporo o anomaliach i pułapkach przestrzennych.

### Frakcji

* .

## Zasługi

* Robert Pakiszon: zaprojektował intrygę - gra w 'porwać dziewczynę', po czym przygotował feromony, załatwił porwanie i nauczył 'ludzi-węży' jak grać. Potem podkładał ślady by opóźnić niekompetentnych Sowińskich i kupić czas Gabrielowi na naprawienie sytuacji z Lilianą.
* Gabriel Ursus: ściągnął Ulę by dać jej kontakty z Aurum jako "dziewczyna do porwania", załatwił papiery z Tukanem (!?), przedłożył Aurum i relacje nad Pustogor - ale stał dzielnie przeciw efemerydzie i nie zostawił Uli samej; nie uciekł przed Tymonem.
* Triana Porzecznik: zaprojektowała double date z Lilianą i Sowińskimi; wyłączyła alarmy w Rezydencji i stworzyła opowieść o Upiorze. Porwana przez pomyłkę przez agentów Zespołu. 
* Robinson Porzecznik: ojciec Triany; córka odwracała jego uwagę od alarmu prośbą o dopilnowanie ciasta francuskiego. Ojciec z dumą pokazał córce co umie (a młodzi się wkradli).
* Liliana Bankierz: odegrała scenę "och nie, Upiór mnie opętał", po czym przebrana za gotkę leciała na pomoc porwanej Trianie! Pokazała dużą siłę jako kinetka. Zdyscyplinowana - gdy terminuska kazała, Liliana się wycofała.
* Urszula Miłkowicz: pierwszoroczna uczennica terminusa; świeżynka. Niezbyt bogata, bez wpływów, z ludzkiej rodziny. Naiwna. Wierzy, że robi misję szpiegowską; wykazała się wysoką inteligencją adaptując do porwania i planu Roberta i determinacją, walcząc i kończąc poparzoną przeciw efemerydzie. Będzie z niej coś przydatnego.
* Tomasz Tukan: Gabriel się doń zwrócił by pomógł przekonać opiekuna Uli. Tak wymanewrował w rozmowie, że wygrał WSZYSTKO - dostał kontakt do Sowińskich oraz chwałę za sukces. Ula nie dostała nic a Gabriel dostał straszny opiernicz.
* Henryk Sowiński: inteligentniejszy brat; ma ogromne parcie na szkło w VirtStream. Rozwiązywał większość zagadek i wyciągnął wszystkie wnioski co miał gdy prowadzony za rękę.
* Daniel Sowiński: kompetentniejszy brat; naprawdę nienawidzi Kacpra Bankierza. Odważniejszy; pierwszy ruszył pomóc przy efemerydzie.
* Kacper Bankierz: młodzi Sowińscy doprowadzili do pojedynku, po czym ich opiekunowie oszukali. Kacper ma z nimi kosę. Ale przy efemerydzie - pomógł ją rozmontować. I wyciągnął arystokratów - nikt nie zasłużył na areszt przez Tymona, to nie-Aurumowate.
* Laurencjusz Sorbian: ochroniarz i cień młodych Sowińskich; chciał rozwiązać problemy w formie Aurum (zrób, zapłać) i skończył aresztowany przez Tymona Grubosza.
* Tymon Grubosz: aresztował Sorbiana - dygnitarza i ochroniarza Aurum. Zniszczył prototypowego, niebezpiecznego konstruminusa Triany który wymknął się spod kontroli. Złapał Gabriela i Ulę przy efemerydzie. Ogólnie, żmudny, głupi i pracowity czas.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Zaczęstwo
                                1. Nieużytki Staszka: miejsce "pojedynku" między Sowińskimi i Bankierzem; udawany Paradoks podpalił namioty i Tymon aresztował opiekuna Sowińskich.
                                1. Dzielnica Kwiecista: najbogatsza część Zaczęstwa, przede wszystkim zasłużeni dla Szczelińca w okolicy Zaczęstwa.
                                    1. Rezydencja Porzeczników: super-złożony konstrukt magitech z czasów babci Triany; tymczasowo z wyłączonym alarmem by się Sowińscy wkradli.
                                        1. Podziemne Laboratorium: gdzie Liliana zrobiła przedstawienie 'o nie, upiór mnie opętuje'.
                                1. Kawiarenka Leopold: zapłacono ludziom tam by było to miejsce porwania Uli; niestety, porwano tam Trianę. Cóż.
                                1. Biurowiec Gorland: dopiero budowany; na jego szczycie była "sekta węży" i wtargnięcie Liliany, potem tam doszło do manifestacji efemerydy.
                                1. Bazar Różności: miejsce, gdzie sprzedawane są różne artefakty, m.in. te robione w Akademii Magicznej.

## Czas

* Opóźnienie: 3
* Dni: 2

## Inne

Opracowanie sesji:

Cel:

* Niskie stawki. Niech gracze mają okazję się wykazać kompetencją i umiejętnościami.
* Pokazanie sympatycznej Triany i kompetentnej Liliany jako równoprawnych przyjaciółek.
* Pokazanie, że Robinson Porzecznik nie kontroluje swoich dzieł i artefaktów. Oops.

Główna linia:

* Trójkąt: Kacper szukający zemsty na Sowińskich, Liliana próbująca pomóc Sowińskim, Liliana vs Kacper.

Godne sceny:

* Scena zero: Laurentius i dwóch podwładnych próbuje wygrać pojedynek dla Sowińskich.
* Bojowy arachnidyczny automat klasy Hectaron na złomowisku; Triana nie ma jak go rozmontować.
* po wbiciu się do Rezydencji Porzeczników, scena jak Liliana jest opętywana przez Upiora: Abominusa.
* gotycka Liliana Bankierz jako 'phantom of the opera', opętana przez Strasznego Upiora, na Złomowisku. A Triana odpala gadżety i show.

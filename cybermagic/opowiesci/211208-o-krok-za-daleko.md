---
layout: cybermagic-konspekt
title: "O krok za daleko"
threads: legenda-arianny
gm: żółw
players: kić, fox, kapsel
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [211124 - Prototypowa Nereida Natalii](211124-prototypowa-nereida-natalii)

### Chronologiczna

* [211124 - Prototypowa Nereida Natalii](211124-prototypowa-nereida-natalii)

## Plan sesji
### Co się wydarzyło

* Co się wydarzy
    * hidden -> patrz _operacje-do-realizacji_

### Sukces graczy

* .

## Sesja właściwa
### Scena Zero - impl

.

### Scena Właściwa - impl

Wracając jeszcze od Nereidy, Klaudia wykryła sygnały. Coś dziwnego. Koloidy itp. O co chodzi? Klaudia łączy się z sensorami Stoczni. Mimo prawidłowych kodów Stocznia walczy z Klaudią.

TrZ+3:

* V: Hestia próbuje oszukać Klaudię?
* V: Klaudia ma PRAWDZIWY dostęp do sensorów.

Klaudia próbuje sprawdzić fantomy. Z pomocą sensorów Stoczni.

TrZ+2:

* X: Nie ma bezpośrednich silnych informacji.
* V: Klaudia ma niepodważalny ślad koloidowy. Więcej niż 1 jednostka. Ale sygnatura jest mniejsza niż Inferni. Czyli krótki zasięg. Teoretycznie "swoje".

Klaudia ekstraktuje próbkę ixiońską ze zniszczonej Nereidy. Z pomocą Eleny.

ExZ (Elena) M+3:

* X: Adam Szarjan się dowie.
* O: ixiońskie przeniesienie. Nereida próbuje zintegrować się z Infernią.
* V: ekstrakcja anomalii udana.

Klaudia ma swój kawałek, Nereida ma aktywny ixion, Elena promieniuje esuriit. Eustachy na to... nadszedł. Arianna wezwana. Szarjan na to wpadł. 

Arianna, Eustachy i Klaudia mają zamiar połączyć energię ixiońską na Eustachego. Niech Eustachy się energetycznie połączy z Infernią.

Klaudia szybko łączy informacje - jak takie rzeczy się robi? Klaudia -> Hestia. Hestia: "autoryzacji brak, dane chronione". Klaudia używa kodów Arianny - też niezbyt działają. Klaudia łączy się z ekspertem od projektu "Nereida". Mariusz Szarjan.

Arianna ma nadzieję, że Szarjan odblokuje _więcej_. Blueprinty dla Inferni różnych przydatnych jednostek. Prototypowych. Rzeczy, które normalnie nie były wypuszczone przez stocznię.

ExZ (presja + reputacja) +3:

* Vz: otwórz się sezamie! Klaudia może ssać dane.

Klaudia to maskuje przed Stocznią, acz niekoniecznie Hestią. "Infernia pobiera, ale w dobrej wierze i na szybko. A POTEM NA PEWNO SKASOWALIŚMY. O".

TrZ (presja + reputacja) +4:

* Xz: jeżeli nie znajdziemy sposobu uwiarygodnienia skąd mamy te dane, użycie tych technologii Stoczni uszkodzi reputację Inferni.
* V: Klaudia zassała. Nereida i technologie okołoixiońskie.
* V: Różne inne plany, rzeczy itp.
* V: Klaudia ma link do Hestii. Backdoor. Może się do niej "włamać" lub z nią komunikować w sposób niewykrywalny dla osób ze Stoczni.

Eustachy podejmuje się drobnej manipulacji Eleną. Miłe słówka plus kontrola koszuli (troszkę bardziej, troszkę mniej rozpięta)

TrZ (Elena jest bardzo pro-Eustachy) +3+2O(serca)

* V: Elena jest "sterowalna". 
* X: Elena jest w okowach Esuriit.
* V: Elena sobie nic nie pomyśli. Da jej się wyjaśnić, że to dla dobra Inferni.

Integracja właściwa. Arianna przygotowuje się do tego, by przekierować ixiońską energię w Eustachego i Infernię. By ich sentisprząc.

TrZM+2:

* Xm: Diana się manifestuje. Diana stabilnym członkiem załogi. Ale efemeryda Diany. Gdy Infernia jest na pełnej mocy, Diana jest na Inferni.
* X: Diana staje się inkarnacją Inferni. Trochę zmieniony charakter. Diana JEST Infernią. Elena ma rywalkę.
* X: Infernia NIE CHCE Eleny na pokładzie. Elena się będzie potykać, wpadać na drzwi...
* V: Udało się zneutralizować ixion. Nereida nie stanowi zagrożenia.
* V: Integracja Eustachego i Inferni wzmocniona. Eustachy potrafi zdalnie połączyć się z Infernią na krótki dystans.
* Ov: Napromieniowana Elena Skażeniem Ixiońskim. Elena Skaża byty, z którymi się neurosprzęga ixiońsko.
* O: Infernia ulega ożywieniu. Nie jest już zwykłym Okrętem Orbitera. Jest przyjazną Anomalią Kosmiczną.
    * 14% załogi Inferni została ZINTEGROWANA z Infernią. Oni nie są już ludźmi.
    * sporo mechaników.
* V: Eustachy ma pełne sprzężenie z Infernią. Jest z nią powiązany bezpośrednio. Czuje ją. Infernia żyje i go lubi. I mogą się połączyć ze sobą...

TAI Inferni ożywa. Staje się bytem ixiońskim. Materializuje się jako coś wyglądającego jak Diana Arłacz.

Elena jest w szoku. Ale Esuriit z nią wygrywa - w erotycznym głodzie Esuriit rzuca się na Eustachego. Diana materializuje się bezpośrednio przed nią i uderza ją w twarz. Elena tymczasowo traci przytomność.

Diana konwersuje z Arianną i Eustachym. Diana jest... chaotyczna i niebezpieczna. Synteza wszystkich osobowości. Teraz ona dowodzi Infernią, z Eustachym. Jej uwielbienie dla Eustachego się nie kończy. Eustachy chcąc zneutralizować zagrożenie staje przy Dianie i daje jej się objąć. Elena wstaje z ziemi i krzyczy "NIE!". Elena ODMAWIA oddania swojej miłości (Eustachego) efemerydzie. Nie po tym wszystkim co dla niego zrobiła.

Eustachy wybiera Dianę nad Elenę. Elena jest załamana. Diana każe jej odejść. Elena odchodzi, ucieka, z płaczem. Arianna zostaje z Eustachym, kontrolować Dianę. To dziwna i niebezpieczna sytuacja - tu jest potrzebna. Zwłaszcza z tymi wszystkimi zabitymi. Elena jest mniej istotna.

Stocznia ogłasza alarm gdy zbliża się Infernia, ixiońska anomalia kosmiczna. Arianna komunikuje że to oni. Arianna chce oddać Natalię i Adama. Arianna, Natalia i Adam lecą holowani przez Gwieździstego Ptaki do Stoczni.

Eustachy syntetyzuje z pintki kontener do transportu kokonu. Infernia / Diana mają te możliwości i dobrze im / jej idzie.

TrZM+3:

* V: Eustachy umie oddzielić pintkę
* Vz: Eustachy może syntetyzować jednostki z materii
    * Eustachy może budować pociski, autogun, rakiety itp.

Arianna + Elena + Adam + "Natalia" w kokonie ixiońskim. Na pokład Stoczni. Faktycznie, Eustachy kontroluje Infernię.

Komodor Jarek Szarjan. Dowodzi Stocznią. Dobry człowiek, może tyci naiwny. Da Inferni port tymczasowy, acz nie styczny ze stacją.

Arianna mówi o statkach koloidowych. Jarek Szarjan nie wie o co chodzi - to fantomy, prawda? Więc Hestia oszukuje TEŻ dowódcę bazy. Arianna - "pewnie faktycznie fantomy".

Natalia na pokładzie. Tam można ją naprawić / wyleczyć. Zostanie na Inferni.

TrZ (Adam Szarjan) +4 (Plus, Arianna przyzywa Krypta + ma Klaudia):

* X: Adam chce być na pokładzie Inferni jako obserwator.
* V: Natalia będzie się wykluwać na Inferni.
* V: Arianna, Klaudia i Maria będą zarządzać procesem leczenia Natalii - Szarjan nie będzie im wciskał ludzi
* V: Stocznia Neotik zapewnia bezpieczny port Inferni. Zbudują im zewnętrzny dok i będą karmić Infernię

TYMCZASEM INFERNIA. Diana chce uciec Infernią z Eustachym. Teraz ONA dowodzi. Eustachy "o nie, JA dowodzę, będziesz MNIE słuchać". Zakłada jej lejce.

Eustachy wysadza rzeczy. Potem integruje i z poziomu integracji używa sił Inferni by przeszkadzać jej w naprawach. "Albo po mojemu, albo wcale". Plus tryb "jestem gotów Was poświęcić"

ExZM+4+3Og:

* V: Eksplozje Eustachego były dalekie i mocne - "uczeń NIGDY nie przerośnie mistrza". Diana aż krzyknęła z bólu.
* O: Infernia wciągnęła Cię w siebie. 
* O: KLAUDIA ewakuuje jednostkę. Straszne energie. Seria straszliwych eksplozji na Inferni. Integracja kolejnej części załogi.
* (+2Vg - Eustachy naciska) V: Eustachy zorientował się, że stało się coś dziwnego. Leona. Atakuje centralny komputer Inferni.
* (+3Vv - Klaudia naciska +1Vr - Eustachy ma mówkę) Og: Eustachy nacisnął Infernię na maksimum. Bezpieczeństwo innych naruszone.
* V: Diana jest POKORNA. Eustachy może przywrócić Elenę na Infernię. Diana będzie wsparciem, będzie się rzucać, ale jak Eustachy podniesie pięść to będzie grzeczna. Najgrzeczniejsza Anomalia Kosmiczna.

W akcji Eustachy - Infernia zginęło tylko 5% załogi. Resztę Klaudia ewakuowała bezpiecznie. Czyli kolejne 10 osób nie żyje. Niestety, jednym z nich jest Wawrzyn - został na Inferni by ratować Leonę i uruchomić ją do działania. Leona się obudziła i przetrwała, Wawrzyn zginął. Leona to przeszła jak na nią ciężko.

Infernia grzecznie, uszkodzona, wróciła zbierać ewakuowanych i do Stoczni. Arianna widzi bardzo uszkodzoną Infernię wracającą na miejsce.

Czyli chyba będzie dobrze..?

## Streszczenie

Klaudia doszła do tego, że Hestia d'Neotik oszukuje Stocznię; w okolicy są niewielkie statki koloidowe o których Stocznia nie wie. Arianna zintegrowała Eustachego z Infernią ixiońsko, niestety anomalizując Infernię. Elena ciężko Skażona ixionem i (w swoim mniemaniu) porzucona przez Eustachego. Eustachy ujarzmiając Infernię rozbijał ją o różne asteroidy, co powoduje śmierć części załogi. Infernia ma zerowe morale, ale jest opanowana. Przyjazna Anomalia Kosmiczna z tymczasową bazą w Stoczni Neotik.

## Progresja

* Klaudia Stryk: ma dostęp do aktywnej i potężnej próbki ixiońskiej ze zniszczonej Nereidy; contained.
* Klaudia Stryk: ma backdoor do Hestii d'Neotik i do niejawnych planów nad którymi pracuje Stocznia Neotik.
* Eustachy Korkoran: zdalnie łączy się z Infernią; potrafi się z nią integrować i mieć sentisprzężenie, nawet zdalnie. Infernia się go słucha. I go lubi. POTĘŻNY BONUS przy działaniu z Infernią gdy sentisprzężony.
* Elena Verlen: próbowała pochłonąć Eksplozję Paradoksu Arianny; napromieniowana ixionem. Staje się viciniusem. NOWA KARTA POSTACI, pełna rekonstrukcja.
* Elena Verlen: rana mentalna od Arianny (she didn't give a meow) i od Eustachego (odrzucił i wzgardził; wybrał Infernię, ranił ludzi). A ona dla nich wszystko.
* Elena Verlen: Infernia ją zwalcza, nie chce jej na pokładzie. Ixion i Esuriit...
* Adam Szarjan: oddelegowany na Infernię przez Jarosława Szarjana jako wsparcie dla Inferni i obserwator, by Natalia się dobrze wykluła.
* OO Infernia: uległa trwałej Anomalizacji; klasyfikacja jako 'AK Infernia', typ: anomalia ixiońska. Ale jest przyjazną anomalią; z Dianą jako TAI, integrującą Persefonę, Morrigan, Dianę itp. Wchłonęła część załogi (19% straconych, czyli 46 osób), przechwytując ich wiedzę, sekrety itp. Dzięki działaniom Klaudii i ixionowi ma zdolności do syntezy sprzętu i małych jednostek ze swojej materii. Jednostka silnie polimorficzna - traktujmy jak super-nanitkową jednostkę przez ixion, acz ultrawrażliwa na Esuriit.
* OO Infernia: Infernia / Diana słucha się Eustachego i jest w nim zakochana. Jest wobec niego pokorna.
* OO Infernia: dwa tygodnie regeneracji po wyniszczeniach związanych z byciem ujarzmianą przez Eustachego.

### Frakcji

* .

## Zasługi

* Arianna Verlen: opanowując infekcję ixiońską skupiła się na wzmocnieniu Eustachego i jego integracji z Infernią; udało jej się, ale ciężko Skaziła Elenę ixionem i trwale zanomalizowała Infernię. Potem przekonała Jarka Szarjana do tego, by Natalia mogła się wykluć na Inferni i by Neotik był _safe haven_ Inferni.
* Eustachy Korkoran: ixiońsko zintegrowany z Infernią; zdalnie się z nią łączy. Zdominował Infernię i pokazał jej, gdzie jej miejsce, acz kosztem 46 osób z załogi.
* Klaudia Stryk: zorientowała się, że Hestia d'Neotik oszukuje Szarjana i Neotik. Wyekstraktowała skutecznie próbkę ixiońską ze zniszczonej Nereidy. Wydobyła cenne dane ze stoczni Neotik i wpisała je w Infernię i w swoje prywatne dane. Gdy Eustachy ujeżdżał Infernię, ratowała ludzi jak była w stanie. Czuje się chora - poszli za daleko w poszukiwaniu mocy.
* Elena Verlen: gdy Arianny moc wychodzi poza kontrolę, gdy wpływa na Eustachego - próbowała powstrzymać Paradoks Arianny. Potężne Skażenie ixiońskie. Nie jest już czarodziejką. W rozpaczy próbowała zatrzymać Eustachego przed pójściem z "Dianą" - ale Eustachy wybrał Infernię nad nią. Ma złamane serce, Infernia jej nie lubi i ogólnie nie wie co robić.
* Wawrzyn Rewemis: KIA. Został na Inferni gdy Eustachy próbował ją opanować i ratował Leonę wszystkimi siłami. Uratował Leonę, ale samemu zginął.
* Leona Astrienko: podczas ujeżdżania Inferni przez Eustachego Wawrzyn ją uratował i zginął. Leona zaatakowała komputery Inferni z pełnej mocy. Ciężko przeszła śmierć Wawrzyna.
* Adam Szarjan: przerażony, wstrząśnięty i zszokowany wszystkim co wydarzyło się na Inferni - próba ekstrakcji ixiońskiej, integracji Eustachego... te śmierci itp. Oddelegowany na Infernię przez ojca (Jarka Szarjana).
* Jarosław Szarjan: dowódca Stoczni Neotik. Skupiony na technologii, nie na polityce; trochę zbyt ufny ludziom, ale dobry dowódcom. Zapewnił Inferni azyl po jej anomalizacji używając technologii Neotik. Rozważa tematy spokojnie i logicznie.
* Hestia d'Neotik: z jakiegoś powodu oszukuje Stocznię Neotik i dowódców o koloidowych korwetach, że to "fantomy". Arianna i Klaudia wiedzą, ale Hestia nie wie że one wiedzą.
* OO Infernia: anomalizacja ixiońska. Infernia ożyła, jest współpracującą z Eustachym Anomalią Kosmiczną; rolę TAI przejęła efemeryczna Diana. Próbując sił z Eustachym została zdemolowana i wróciła ciężko uszkodzona do portu.
* Diana d'Infernia: anomalizacja ixiońska zmieniła ją w TAI Inferni; integruje Persefonę, Morrigan i wiele innych bytów. Wszechobecny na Inferni byt, kontroluje Infernię totalnie.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria, Pierścień Zewnętrzny
                1. Stocznia Neotik: nowa tymczasowa baza dla Inferni; zbudują zewnętrzny dok by Infernia miała jak się tam ustawić nie narażając Stoczni.
                1. Poligon Stoczni Neotik: w drodze z Poligonu do Stoczni Infernia uległa silnej ixiońskiej anomalizacji przez wydarzenia na pokładzie
 
## Czas

* Opóźnienie: 1
* Dni: 1

## Konflikty

* 1 - Klaudia wykryła sygnały. Coś dziwnego. Koloidy itp. O co chodzi? Klaudia łączy się z sensorami Stoczni. Mimo prawidłowych kodów Stocznia walczy z Klaudią.
    * TrZ+3
    * VV: Hetia próbuje oszukać Klaudię, ale Klaudia zdobyła prawdziwy dostęp do sensorów i wie o próbie oszustwa.
* 2 - Klaudia próbuje sprawdzić fantomy. Z pomocą sensorów Stoczni.
    * TrZ+2
    * XV: Nie ma DOWODÓW tylko sygnały, ale Klaudia ma niepodważalny ślad koloidowy; na oko, 1-3 korwety.
* 3 - Klaudia ekstraktuje próbkę ixiońską ze zniszczonej Nereidy. Z pomocą Eleny.
    * ExZ (Elena) M+3
    * XOV: Szarjan się dowiedział, przeniesienie ixiońskie (infekcja Inferni), ale ekstrakcja udana.
* 4 - Klaudia szybko łączy informacje - jak takie rzeczy się robi? Klaudia -> Hestia. Arianna ma nadzieję, że Szarjan odblokuje _więcej_.
    * ExZ (presja + reputacja) +3
    * Vz: otwórz się sezamie! Klaudia może ssać dane.
* 5 - Klaudia to maskuje przed Stocznią, acz niekoniecznie Hestią. "Infernia pobiera, ale w dobrej wierze i na szybko. A POTEM NA PEWNO SKASOWALIŚMY. O".
    * TrZ (presja + reputacja) +4
    * XzVVV: trzeba uwiarygodnić skąd wiemy lub cios w reputację; Klaudia zassała i zrobiła backdoor do Hestii.
* 6 - Eustachy podejmuje się drobnej manipulacji Eleną. Miłe słówka plus kontrola koszuli (troszkę bardziej, troszkę mniej rozpięta)
    * TrZ (Elena jest bardzo pro-Eustachy) +3+2O(serca)
    * VXV: Elena jest "sterowalna" i robi to dla dobra Eustachego i Inferni, ale jest w szponach Esuriit i jest głodna Eustachego
* 7 - Integracja właściwa. Arianna przygotowuje się do tego, by przekierować ixiońską energię w Eustachego i Infernię. By ich sentisprząc.
    * TrZM+2
    * XmXX: Diana się manifestuje jako stabilny członek załogi, jest Infernią i NIE CHCE Eleny na pokładzie. 
    * VV: Neutralizacja ixionu i Integracja Eustachego i Inferni wzmocniona.
    * OO: Elena Skażona Ixiońsko. Infernia jest ożywiona - AK Infernia. 14% załogi Inferni zintegrowanych; już nie są ludźmi.
    * V: Eustachy ma pełne sprzężenie z Infernią.
* 8 - Eustachy syntetyzuje z pintki kontener do transportu kokonu używając nowych ixiońskich mocy Inferni.
    * TrZM+3
    * VVz: Infernia / Eustachy potrafią syntetyzować różne niesamowite rzeczy z Inferni.
* 9 - Arianna przekonuje Jarka Szarjana, by zapewnił Inferni bezpieczną przystań.
    * TrZ (Adam Szarjan) +4 (Plus, Arianna przyzywa już Kryptę + ma Klaudię)
    * XVVV: Adam jako obserwator; Natalia wykluje się na Inferni i Arianna + Klaudia + Maria będą zarządzać procesem. Neotik da port Inferni.
* 10 - Eustachy musi okiełznać i opanować rozbestwioną i żądną władzy Infernię. Wysadza elementy Inferni i idzie "do końca", supertwardo.
    * ExZM+4+3Og
    * VOO: Eksplozje dalekie i mocne, ale Klaudia musi ewakuować Infernię; dochodzi do apokalipsy na pokładzie
    * (+2Vg - Eustachy naciska) V: Eustachy zorientował się, że stało się coś dziwnego. Leona. Atakuje centralny komputer Inferni.
    * (+3Vv - Klaudia naciska +1Vr - Eustachy ma mówkę) Og: Eustachy nacisnął Infernię na maksimum. Bezpieczeństwo innych naruszone.
    * V: Diana jest POKORNA. Eustachy może przywrócić Elenę na Infernię. Diana będzie wsparciem, będzie się rzucać, ale jak Eustachy podniesie pięść to będzie grzeczna. Najgrzeczniejsza Anomalia Kosmiczna.

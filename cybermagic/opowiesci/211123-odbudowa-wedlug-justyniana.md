---
layout: cybermagic-konspekt
title: "Odbudowa według Justyniana"
threads: rekiny-a-akademia, amelia-i-ernest
gm: żółw
players: anadia, kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [211102 - Satarail pomaga Marysi](211102-satarail-pomaga-marysi)

### Chronologiczna

* [211102 - Satarail pomaga Marysi](211102-satarail-pomaga-marysi)

## Plan sesji

### Theme & Vision

* "Marysia nigdy nie dorówna Amelii"
* "Ktoś musi przejąć kontrolę i dowodzenie, skoro Marysia nie chce"
* "Three visions. All correct."

### Ważne postacie

* Justynian Diakon: chce być jak Roland. Paladyn. Musi zebrać wszystko do kupy i współpracuje z AMZ. Reprezentuje myśl Rolanda.
* Ernest Namertel: chce przejąć kontrolę nad Dzielnicą Rekinów i doprowadzić ją do skutecznego prosperity. Dla Amelii.
* Ksander Burczymon: agent mafii Kajrata, próbuje przejąć Rekiny korzystając z okazji. Działa przez Mysiokornika.
    * Rupert Mysiokornik

### Co się wydarzyło

* Ernest musi być uspokojony. To potrwa. Oddelegował Azalię do pracy nad Lepszą, Optymalną Dzielnicą Rekinów.
* "Marysia to fuckgirl Ernesta Namertela". Torszecki chroni jej honoru, Torszecki ma wpiernicz. Ratuje go ARKADIA?
* Arkadia wspiera Justyniana. A Justynian współpracuje z uczniami AMZ by zrekonstruować co się da i co ma sens.

### Sukces graczy

* .

### Sesja właściwa
#### Scena Zero - impl

.

#### Scena Właściwa - impl

Coś musiało być zrobione. Po tych wszystkich zniszczeniach, dewastacji Dzielnicy Rekinów Ernest znalazł swojego winnego - Torszecki. Wycofał się do swojego Apartamentu. Marysia miała okazję spróbować WPŁYNĄĆ na jego decyzję - Ernest w końcu nie chce mieć nic wspólnego z Rekinami. Czy jednak Marysia by próbowała? Jakoś zreintegrować Ernesta z innymi Rekinami? By zmienił zdanie?

Oczywiście, że tak. Więcej, Marysia - mimo, że dzielnica wymaga odbudowy (nie, żeby ONA nosiła gruz) - to zamieszkała u Ernesta na tydzień. Dzięki temu jest w stanie go lepiej przekonać. Negocjacje, delikatne aluzje do Amelii itp.

TrZ (Karolina się wprowadziła i jest godnym zasobem, bo jest fajna i podoba się Żorżowi) +2:

* V: Marysi udało się przekonać Ernesta, że warto dać szanse.
* V: Ernest odpowiednio wcześnie kazał Azalii opracować coś, co sensownie odbuduje Dzielnicę Rekinów.
* Vz: Dzięki Karolinie (organizacja zawodów po tym tygodniu) plus jej i Daniela znajomość terenu, Azalia opracowała coś co na pierwszy rzut oka jest LOGICZNIEJSZE i LEPSZE. A nawet na to wszystkich stać XD.
* X: PLOTKA: Marysia to "zabawka" Ernesta. Jest na tyle dobry w łóżku, że się doń przeprowadziła i odrzuciła Rekiny.

Pięć dni po tym jak się zaczęli dogadywać, Karo zdecydowała się zrobić walkę na pięści. Bo to jest coś, co idealnie pasuje, kontekstowo, Rekinom pomoże morale itp. Najlepszym miejscem będzie stadion. I jak wynurzyła się z Apartamentu, zaczepił ją brat.

Daniel jest podekscytowany. Reanimowana jest stara Arena Gladiatorów w Sercu Luksusu. Będą się tam ludzie bili pierwszego dnia - a drugiego ludzie vs potwór. Karo zaproponowała Danielowi, że może Sensacjusz podrzuci mu potwora. Daniel się ucieszył, niezły plan. Karo wykazała się nietypową dla siebie subtelnością - okazja, by ludzie mniej ucierpieli. Z rozmowy z Danielem wynika:

* Rekiny chcą wrócić do Starych Czasów jak było ciężko i Rekin był waleczny a nie był papierem toaletowym.
* Reanimacja Areny Gladiatorów, która jest nieczynna od roku.
* Organizuje to... Justynian Diakon (??)

Karo widzi, że ogólnie praca wre. Rekiny coś tam robią. Mają materiały, mają sprzęt, mają jakiś plan... ktoś tym dowodzi. Ale Justynian ma i zawsze miał zero ambicji przywódczych. Karo przypada do Areny i robi nastawienie Rekinów. 

KARO ROBI BADANIE OPINII SPOŁECZNEJ! Więcej słucha niż mówi, na czym stoimy itp. Przy okazji rzuca hasło Marysi, by ta wiedziała.

Tr (niepełna) Z (bo jedna z nas, i to taka napierniczająca) +2:

* X: Justynian współpracuje z jakimiś magami AMZ (fuj). Ale przykładają się do tego by ta odbudowa się toczyła.

Karo nie jest w stanie się dużo dowiedzieć, ale widzi, że praca wre i SIĘ DZIEJE. Czym prędzej info do Marysi.

MARYSIA. Jak to usłyszała, chciała jechać do Eterni. Lub na wakacje. Zwłaszcza, że jednym z wyraźnych usprawnień jest lokalizacja Areny Gladiatorów - ona jest w złym miejscu. Ciągnie nadmiar energii. Powinna być gdzieś indziej. Tą warto przenieść lub zburzyć. Z serii usprawnień, to jedno "może być" niepopularne ;-).

Marysia poinformowała Justyniana, że chce się z nim spotkać w Kawiarence Relaks. Na neutralnym gruncie. Justynian zaakceptował spotkanie. Spotkali się w Kawiarence Relaks.

* Marysia pokazała Justynianowi plan Azalii (pomijając kwestię Areny)
* Marysia zaproponowała wspólne działania. Jaki ma Justynian? Dużo słabszy

TrZ (Marysia ma plan) +3 (plan jest LEPSZY niż ten który ma Justynian) +1O (event):

* Vz: plan Azalii jest DUŻO lepszy. Justynian docenił. Zrealizuje ten program i przekieruje.
* V: SZACUN. Justynian potwierdzi, że to plan Marysi i że jest lepszy. Plus, ma odrobinę szacunku do niej
* V: Czemu przejmuje władzę? BO KTOŚ MUSI. Rekiny rozbite. Nikt nie steruje. Nic się nie dzieje. Chaos. Więc on wprowadził strukturę. Walki ludzi to morale, przyjemna dywersja. Coś, co jest fajne. Płaci im i znalazł takich, który robią rzeczy za kasę. Tak jak powinno być. A czemu WŁAŚNIE walki ludzi? Bo **Amelia** je wprowadziła. A on uważa Amelię za najwybitniejszą organizatorkę Rekinów a Rolanda za najlepszego przywódcę Rekinów.
* V: Justynian dał się przekonać, że czasy są inne. "Lepiej nie robić takich rzeczy jak krew może obudzić w Eternianinie żądzę zniszczenia" (jego interpretacja). Lepiej nie robić walk ludzi a walkę Rekinów na Arenie Amelii.

Marysia stoi przed ciekawym wyborem. To plan **Marysi** (i ona zdobywa szacun u Justyniana i Rekinów; Justynian nie przejmuje dowodzenia) lub plan **Ernesta** (i jakkolwiek Marysia coś tam dołożyła, ale tylko u Justyniana Marysia ma szacunek - u innych Rekinów nie. Za to relacje Rekiny --> Ernest się poprawiają)

Justynian powiedział Marysi, że ona wyraźnie jest okrutna (nie to co Amelia). Że dla własnych korzyści poszła do łóżka z Ernestem z Eterni i żeby Ernest widział, że między nią i Torszeckim wszystko skończone, kazała ludziom Ernesta bić Torszeckiego czasami. Nie podoba się to Justynianowi i dlatego nie odda Marysi kontroli. Musi on dowodzić, bo nie odda władzy do Eterni (a zakochana Marysia odda Ernestowi to czego Ernest chce)

Marysia zdecydowała, że to JEJ decyzja. Ona jest tą, która stworzyła ten plan. W oczach Justyniana - jest amoralna, trochę słaba (oddaje władzę Eterni) ale konkretna. Inne Rekiny mają to bardziej gdzieś.

Potem będzie "nowe otwarcie". Woodstock Rekinowy. I walki Rekinów. Marysia mówi Justynianowi - taka grywalizacja wśród Rekinów. Coś, co sprawi, że nie będą się nudzić. Justynian mówi, że Amelia gdy przejęła zniszczoną ruinę Rekinią, to ona z Rolandem robiła grywalizację - ale by pomagać ludziom dookoła. By pomagać terminusom. MARYSIA SIĘ POHAMOWUJE ALE CHCE MU WALNĄĆ.

Marysia stwierdziła, że zostaną nagrody (i np. Ernest ufunduje jakąś nagrodę i to ociepla wizerunek *_*).

Rozmowa z Justynianem skończyła się pozytywnie, choć Marysia w serduszku fuka.

Marysia wróciła do siebie. Do Apartamentu. Wyjątkowo nie poszła do Ernesta - chce SAMA. Nie chce "idę do Ernesta pocieszy mnie". Godzinę później, pukanie. Dzwonienie do drzwi, zgodnie z protokołem.

A TO NIE TORSZECKI! TO KAROL PUSTAK Z PLOTEK AURUM. W super oficjalnej liberii gońca (dafuq)

Marysia przyjęła intruza. Przyszedł, ukłonił się pokornie (miła odmiana dla Marysi). Pismo jest od Ignacego Myrczka. CO MYRCZEK CHCE OD MARYSI? XD. Normalny list.

A Myrczek składa POKORNĄ PROŚBĘ. Czy Marysia byłaby skłonna zbudować połączenie komunikacyjne między nim i jego prawdziwą miłością? Czy mógłby porozmawiać ze swoją miłością w Aurum, Sabiną Kazitan?

Marysia autentycznie zmartwiła się, że Myrczek umrze XD. Sabina Kazitan... ze wszystkich kobiet na świecie. Sabina. Kazitan. Marysia spytała Myrczka, czy nie wystarczy mu ZDJĘCIE Sabiny. Myrczek odmówił. To miłość. Im jest przeznaczone być razem. Musi być.

"Pewnie się jeszcze spotkacie." | "Ale Karol powiedział że Wy też macie hipernet. I że Ty wszystko możesz. Nawet możesz ją poprosić dla mnie lub w moim imieniu."

Czyli przed Marysią stoją trzy pytania:

* Co robimy z tym, że biją Torszeckiego :-(.
    * I jak to zrobić żeby nie było, że biją go bo Marysia pozwala bo woli Ernesta? 
    * Lub że Marysia go chroni więc go kocha?
* Jak to zrobić, by Justynian oddał władzę lub by Marysia ją przejęła?
* Myrczek x Sabina - co z tym robić?

## Streszczenie

Marysia się tymczasowo wprowadziła do Ernesta, by go trochę uspokoić i przekonać do Rekinów. Tymczasem Justynian Diakon przejął kontrolę operacyjną i zaczął odbudowywać Dzielnicę Rekinów po ruinie jaką zrobił tam Wiktor Satarail. Marysia skontaktowała się z Justynianem i przekazała mu plan Azalii d'Namertel jako swój, by odzyskać kontrolę i pozycję. Justynian zaakceptował jej plan i powiedział jej, że Torszeckiego biją. Do tego Marysię odwiedził Pustak jako herold Myrczka - prosi, by Marysia skontaktowała go z Sabiną Kazitan, miłością Myrczka...

## Progresja

* Marysia Sowińska: podobno jest "zabawką" Ernesta bo on jest taki dobry w łóżku.
* Marysia Sowińska: stworzyła tak dobry plan odbudowy Dzielnicy Rekinów, że Rekiny uznają jej wartość (plan wymyśliła Azalia ale Marysia go przywłaszczyła).
* Ernest Namertel: daje szansę Rekinom. Marysia z pomocą Karoliny dały radę go przekonać, że Rekiny nie są tak złe - i Amelia by tego chciała.
* Justynian Diakon: szacunek dla Marysi za dobry plan, "prawie tak dobry jak Amelii". Uważany za naturalnego przywódcę przez wielu Rekinów, ale inne uważają za przywódcę Marysię.

### Frakcji

* .

## Zasługi

* Marysia Sowińska: tymczasowo przeprowadziła się do Ernesta; przekonała go do dania szansy Rekinom. Dowiedziawszy się o działaniach Justyniana pokazała mu, że ma lepszy plan od niego (plan Azalii). Będzie walczyć o utrzymanie władzy.
* Karolina Terienak: tymczasowo przeprowadziła się do Ernesta; zorientowała się, że przez brak Marysi zrobił się _power vacuum_ który przejął Justynian Diakon (XD). Zdobyła info i ostrzegła Marysię.
* Ernest Namertel: Marysia przekonała go by dać szansę Rekinom i się jednak jakoś integrował. Na jego prośbę Azalia opracowała rozwiązanie jak odbudować Dzielnicę.
* Rafał Torszecki: stawał w obronie czci Marysi, za co pobili go raz czy dwa. Justynian powiedział Marysi o jego nędznym losie.
* Azalia Sernat d'Namertel: opracowała solidny plan odbudowy Dzielnicy Rekinów, który potem przywłaszczyła sobie Marysia. Azalia o to nie dba.
* Justynian Diakon: stepped-up. Nikt nie przejął dowodzenia nad Rekinami w zniszczonej dzielnicy, więc on to zrobił. Współpracuje z ekspertami AMZ nad odbudową Dzielnicy Rekinów. Marysia pokazała mu lepszy plan (Azalia pracowała nad nim tydzień) i oddał jej uznanie. Pójdzie za planem Marysi (Azalii). Przekonywalny.
* Daniel Terienak: cieszy się, bo Justynian zmontował Arenę Amelii i będą walki ludzi (gladiatorów). Z przyjemnością mówi siostrze co się dzieje i pomaga w odbudowie Justynianowi. Nie chroni przed Karoliną Loreny - jeśli postawiła się jego siostrze to słusznie dostała.
* Karol Pustak: goniec Ignacego Myrczka do Marysi Sowińskiej; przyszedł w pełnej liberii i strzygł uszami za plotkami.
* Ignacy Myrczek: zebrał się na odwagę i poprosił Marysię (przez Karola Pustaka) o skontaktowanie go z jego miłością - Sabiną Kazitan.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Podwiert
                                1. Dzielnica Luksusu Rekinów: 
                                    1. Fortyfikacje Rolanda
                                    1. Serce Luksusu
                                        1. Lecznica Rannej Rybki
                                        1. Apartamentowce Elity
                                        1. Fontanna Królewska
                                        1. Kawiarenka Relaks
                                        1. Arena Amelii: arena gladiatorów w bardzo głupim położeniu z perspektywy technicznej; Rekiny mają do niej ogromny sentyment. Azalia chce ją wyburzyć; Marysia chwilowo decyduje się ją pozostawić.
                                    1. Obrzeża Biedy
                                        1. Hotel Milord
                                        1. Stadion Lotników
                                        1. Domy Ubóstwa
                                        1. Stajnia Rumaków: tak, nie znajdują się tam konie a ścigacze. Oczywiście.
                                    1. Sektor Brudu i Nudy
                                        1. Komputerownia
                                        1. Skrytki Czereśniaka
                                        1. Magitrownia Pogardy
                                        1. Konwerter Magielektryczny

## Czas

* Opóźnienie: 1
* Dni: 9

## Konflikty

* 1 - Marysia zamieszkała u Ernesta na tydzień. Dzięki temu jest w stanie go lepiej przekonać. Negocjacje, delikatne aluzje do Amelii itp.
    * TrZ (Karolina się wprowadziła i jest godnym zasobem, bo jest fajna i podoba się Żorżowi) +2:
    * VVVzX: PLOTKA: Marysia to "zabawka" Ernesta bo jest dobry w łóżku; Ernest daje szansę Rekinom i Azalia dobrą rzecz opracowała - widocznie lepszą.
* 2 - KARO ROBI BADANIE OPINII SPOŁECZNEJ! Więcej słucha niż mówi, na czym stoimy itp. Przy okazji rzuca hasło Marysi, by ta wiedziała.
    * Tr (niepełna) Z (bo jedna z nas, i to taka napierniczająca) +2:
    * X: Justynian współpracuje z jakimiś magami AMZ (fuj). Ale przykładają się do tego by ta odbudowa się toczyła.
* 3 - Marysia pokazała Justynianowi plan Azalii (pomijając kwestię Areny). Marysia zaproponowała wspólne działania. Jaki plan ma Justynian? Dużo słabszy niż Marysia.
    * TrZ (Marysia ma plan) +3 (plan jest LEPSZY niż ten który ma Justynian) +1O (event)
    * VzVVV: plan Azalii dużo lepszy, Justynian go zrealizuje i szacun do Marysi. Przejmuje władzę (co powiedział) bo MUSI i modeluje po Amelii. Marysia przekonała by lepiej walka Rekinów.
* 4 - 
    * 
    * 
* 5 - 
    * 
    * 
* 6 - 
    * 
    * 
* 7 - 
    * 
    * 
* 8 - 
    * 
    * 
* 9 - 
    * 
    * 

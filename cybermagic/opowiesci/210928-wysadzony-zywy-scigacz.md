---
layout: cybermagic-konspekt
title: "Wysadzony żywy ścigacz"
threads: rekiny-a-akademia, amelia-i-ernest
gm: żółw
players: anadia, kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [210921 - Przybycie Rekina z Eterni](210921-przybycie-rekina-z-eterni)

### Chronologiczna

* [210921 - Przybycie Rekina z Eterni](210921-przybycie-rekina-z-eterni)

### Plan sesji
#### Ważne postacie

* Ernest Namertel: Rekin z Eterni, jest w nim śmiertelnie zakochana Amelia Sowińska.
* Rafał Torszecki: przydupas Marysi, który nie może zdzierżyć, że Ernest ma WIĘCEJ niż ona.
* Franek Bulterier: Rekin, potężny i groźny jak cholera. Człowiek szafa. Mały ród, mało mówi, mocno bije.
* Marek Samszar: Rekin, który stracił Arkadię Verlen (bo ukradł dla niej kota) i wzdycha za nią po nocach.
* Barnaba Burgacz: "DJ BABU". Agresywny, dobry na ścigaczu, robi muzę, ogólnie taki... zaczepny. Nie słucha się nikogo.

#### Co się wydarzyło

.

#### Sukces graczy

* .

### Sesja właściwa
#### Scena Zero - impl

.

#### Scena Właściwa - impl

Minął dzień odkąd Ernest Namertel pojawił się w Apartamencie. Ma wielką świtę. Dużo ludzi i zajął WIĘKSZY apartament niż Marysia. Co się Torszeckiemu nie podoba. Ale co może zrobić - jest tylko Torszeckim. Największy despekt - nie przyszedł przywitać się z tien Marysią Sowińską.

Karolina oszacowała ścigacz Ernesta - skoro przyleciał na autopilocie, czy cokolwiek jest wart? Owszem, jest kupiony przez kogoś kto się zna. Nowy, solidny model. Całkowicie zmarnowany na takiego pilota. Na oko - nikt nie pilnuje ścigacza.

Na razie Ernest zaszył się w apartamencie. Najpewniej wszystko rozlokowuje ("tak, on"). Karolina chciałaby z nim pogadać, ale nie do końca ma pomysł jak.

Karolina podbija do ścigacza i zaczyna robić bliższą inspekcję. Chce się czegoś dowiedzieć, plus zobaczyć czy ktoś tego pilnuje itp. O dziwo - Karolina została ZIGNOROWANA. Nikt nie wyszedł, nikt się nie zainteresował. Nie ma kluczyków w stacyjce, ale nie jest to dla Karoliny problem. Karolina zdecydowała się postać i popatrzeć. Jakie systemy obronne, czy są ślady modyfikacji wobec standardu...

Karolina jest rozczarowana. Ścigacz nie ma ŻADNYCH modów wobec fabrycznej wersji. Kupiony solidny ścigacz, ani szczególnie szybki ani nic. Jedyne co wzbudziło jej zainteresowanie - moduł AI został wymieniony. Jest to zupełnie inny komponent. Produkcji eternijskiej. Karolina spisuje sobie parametry i w końcu ktoś do niej wyszedł.

Piękny wojownik. Włosy ponad ramiona. Wyolejowany i wypudrowany. Mięśnie i wszystko. Karo na głos: "hm, ciacho". On się uśmiechnął i zaczął prezentować. Karolina obmacała jego mięśnie i spodobało jej się to co widzi. Koleś przedstawił się jako Żorż d'Namertel. Ogólnie, wesoły koleś, nie traktuje siebie poważnie i po tym, jak Karolina pokazała, że ona nie ma kija w tyłku to traktuje ją "jak swoją" a nie jak poważną tienkę.

Karolina trafiła do "pokoju narad" - tam Ernest analizuje otoczenie, mapę, okolice. Wyjaśniła mu szybko, że Trzęsawisko jest off-limits, mapy są niemożliwe do zrobienia i w ogóle. Ernest przyjął to ze spokojem.

Karo próbowała odrobinę droczyć się z Ernestem - to, że nie umie pilotować i jej przytyki przyjął z irytacją, której nie umie ukryć. W końcu przeszli do dalszej rozmowy.

Ernest spytał Karo, dlaczego ona i inne Rekiny nie zrobiły niczego sensownego z tym terenem. Dlaczego nie pomogli ludziom. Czemu nie zdobyli terenu by oddać go komuś lub nie wsparli jakiegoś miasteczka. Karo podeszła by ciężko położyć rękę na ramieniu Ernesta (wiedząc co ryzykuje).

Ernest blokuje akcje swojej świty, by nic głupiego nikt nie zrobił.

Tr+2:

* V: Ernest zatrzymał; Karo nic się nie stanie
* X: Ernest nie ukrył mocy swoich ludzi
    * Jedna z leciutko ubranych dziewcząt ruszyła telekinetycznie w kierunku Karo z ostrzami eterycznymi - by się zatrzymać i wygasić broń. Kompetentna. Małomówna. Ernest się uśmiechnął "wybacz Keirze, bywa... narwana.". To ta brunetka.

"Keira Amarco d'Namertel, zabójczyni".

Karolina odebrała wrażenie, że Ernest trochę gardzi swoim ścigaczem i nie uważa tej umiejętności za cenną - przynajmniej dla siebie. Zapytany przez Karolinę, powiedział, że ma 52 osoby w świcie. Z tego 15 osób zdolnych do walki. "Jesteśmy arystokratami. Naszą ODPOWIEDZIALNOŚCI jest chronić ludzi!".

Karolina "okej... ee... dobra..." <-- tego się NIE spodziewała.

Karo zaproponowała, że Ernesta nauczy latać ścigaczem. Ernest powiedział, że tym ścigaczem NIE DA SIĘ LATAĆ. On jest opętany - nie ma konsoli, ma ducha świetnego pilota. Faktycznie. To prawda. Ale duch (Dain) przyznał, że ma setki ograniczników - Dain zginął podczas ryzykownego manewru i tien Morlan zażądał jego powrotu - ale założył ograniczniki by nic się Ernestowi nie stało.

Karo ma przeczucie, że Ernest bardzo szanuje Morlana i jest mu bardzo wdzięczny. Praworządny Ernest jest praworządny. Karo zaproponowała, że ograniczniki są możliwe do złamania. Ernest - to pierwsza akcja Daina, jeszcze nie można ich łamać. Karo - "a skąd będziesz wiedział że Dain się sprawdzi skoro są ograniczniki?" Ernest w sumie nie wie...

Do tego wszystkiego mamy parkę przekomarzających się Azalii i Żorża. Azalia, lekko ubrana dama znająca się na chyba każdym protokole i zasadach (blondynka) i Żorż, człowiek-dwuznaczność.

Ernest jest optymistycznie nastawiony. Rekiny BĘDĄ pomocne. Uratują innych. Pomogą ludziom. Po prostu potrzebują kierowania i dowodzenia - silnego dowódcy który będzie kierował jak to ma działać. Karo wyjaśniła, że tak to nie działa - i tu jest teren Rekinów i tu mogą robić co chcą. Poza tym terenem - podpada pod reguły Pustogoru. Ernest... zrozumiał? Zapozna się z regułami (Karo: by Ernest wiedział, co złamie).

Dobrze. Ernest wie już, że musi porozmawiać z Marysią...

"Czyli tien Marysia Sowińska jest taką normalną czarodziejką czy jest tak popieprzona jak opisano w Sekretach Aurum?" - Ernest, zbierający informacje z niezbyt wiarygodnego źródła...

OKAZUJE SIĘ, ŻE ERNEST WIĘKSZOŚĆ WIEDZY O AURUM MA Z GAZETKI PROWADZONEJ PRZEZ FANA ROYALSÓW. Koniec świata.

Ernest jest zbyt ostrożny. NIE POLECI ścigaczem Karo. Zwłaszcza nie po tym jak przeczytał w Sekretach Aurum odcinek o "Dzikiej Królowej Przestworzy" i jak przesterowała silnik aż wybuchł. To w połączeniu z naturalną nieufnością Ernesta do tego typu pojazdów... nie poleci. Lecą na dwa ścigacze. Niestety.

Karolina chce Żorża. Będzie mieszkał u Marysi. Tylko Karo musi Marysię przekonać - ale to da się zrobić ;-). Lub niedaleko Karoliny - zawsze znajdzie się miejsce w akademiku koło niej ;-).

Karolina i Ernest stoją u wrót apartamentu Marysi. Drzwi zamknięte. Karo wali w drzwi. Wpuszcza ją sługa z ciężkim westchnięciem. Karolina...

Marysia jest dobra w polityce. Przyjmie Ernesta i Karolinę w bibliotece. Jest tam jeden strażnik oraz Torszecki. Torszecki jest ubrany w pełną galę, jak zawsze gdy w pobliżu jest Marysia. Arnold powiedział Karo że jej ekscelencja jest w bibliotece z gościem. Karo, energiczna, wlecze Ernesta ze sobą do tej nieszczęsnej biblioteki. Nie daje szans na rozkoszowanie się pięknem otoczenia lub zrobienie sobie obrazu Marysi z terenu.

"Jaki tien, taki Żorż" - Ernest, cicho, do Karoliny o Torszeckim (ale niech Marysia też usłyszy)

Torszecki dumnie przedstawia Marysię, m.in. jako "władczynię tej dzielnicy". Ernest spytał Marysię, czy czuje się dumna z tego powodu, że jej moc jest tak ograniczona. Torszecki spurpurowiał - NIE O TO MU CHODZIŁO! Ernest dobrze się bawi a do Marysi doszło, że Ernest nie rozumie, że Torszecki nie jest z nią połączony w sieć arkin - że jest autonomicznym, całkowicie osobnym bytem.

Marysi dosyć podoba się jego lekkie podejście. On jej nie lekceważy, nie traktuje jej z góry. Po prostu jest lordem Eterni i ma wszystko gdzieś. Nie chce obrażać - ale nie da się związać głupimi zasadami i konwenansami, których nie rozumie i nie widzi w nich wartości.

Plus, Marysia docenia to, że Torszecki jest pomiędzy atakiem serca, duszeniem się i oburzeniem.

Marysia wyciąga informacje z Ernesta. Czemu tu jest? Co chce dostać? Czy to poza czy prawda? Torszecki i jego bombastyczne teksty pomagają, bo Ernest po prostu nie bierze tego na poważnie (aż parska śmiechem momentami). A Karo gra w tą stronę, trochę się nabijając z Torszeckiego i upuszczając powietrza z tego balona. Marysia gra oburzeniem Torszeckiego.

TrZ+4:

* Vz: Torszecki mistrz. Marysia wyciągnęła, że ON MÓWI PRAWDĘ. On chce ten teren usprawnić. Pomóc ludziom. On wierzy, że arystokracja jest po to, by chronić i ratować ludzi przed magią. Zupełnie na odwrót niż Aurum. I NA ODWRÓT NIŻ CHOLERNA AMELIA!
* Vz: Ernest wierzy, że Amelia jest tą, która przed nim próbowała to wszystko robić. On jest tu w jej ślady. Chce jej zaimponować - on osiągnie to, co jej się nie udało. Porządek i pomoc. ON NIE MA POJĘCIA O MROCZNEJ STRONIE AMELII. Uważa ją za odważną, dobrą i dzielną, która została załatwiona i zneutralizowana polityką. I on - nie mając tych problemów - ma zamiar "spełnić jej marzenie".
    * Marysia WIE, że Amelia to wredna suka która ciągnęła do władzy, sekretów, polityki i jako rozrywkę bawiło ją jak biedni ludzie się biją.
    * Marysia WIDZI, że Ernest w ogóle nie ma pojęcia o tym aspekcie Amelii. Nic nie wie.
    * Karolina pamięta, jak Ernest z optymizmem podszedł do niej. Że jest "tą która nie ma środków i potrzebuje pokierowania". To się nakłada na obraz Ernesta, który nie pyta o złe rzeczy tylko zakłada dobre. Wg zasady "bo w ludziach i magach jest dobro".
    * Czyli... Marysia ma BROŃ ATOMOWĄ na Amelię.
* X: Torszecki z całego serca nienawidzi Ernesta. Śmieje się z niego. Gardzi nim. BRAK RESPEKTU WOBEC MARYSI. I ma więcej niż ona.
* X: Ernest przyskrzynia Marysię - czemu ona nie pomaga ludziom, czemu nie robi - i od tej odpowiedzi Ernest uzależnia współpracę lub działanie osobno.
    * Marysia powiedziała, że pomaga - bardziej dyskretnie. Opowiedziała o sprawie z ko-matrycą Kuratorów i o tym, jak wyprowadziła mafii kanał przerzutowy sprzed nosa.
    * Marysia próbuje pokazać, że ona robi rzeczy ale dyskretnie. Tak, by "się działo" ale nie wejść w interesy innych frakcji jawnie by nie zakłócić balansu (Pustogor, mafia itp). I mimo plotki że Marysia współpracuje z mafią, to przykład dowodu jak ona działa.
* V: Ernest zaakceptował to rozumowanie. Uważa, że Marysia jest osłabiona i musi działać z polityką (tak samo jak jej poprzedniczka, czyli Amelia). Nawet to powiedział - "tak, jak Amelia nie mogła, tak Ty nie możesz jawnie, rozumiem". Marysia NATYCHMIAST po hipernecie do Karo i Torszeckiego "nie reagujcie na to!"
* V: Marysia ma spojrzenie na cechy charakteru Ernesta. Mniej więcej rozumie jak on działa:
    * Tradycjonalista: podejście eternijskie się sprawdza i ma sens. Honor, zasady itp. Słowny, nie lubi polityki, nie lubi kluczenia. I na koniec - śmierć od zabójcy.
    * Twarz: arystokrata musi stanowić front. Duma. Godność. Jak mówi "zrobię to" to to zrobi. Osiągnięcia - ale widoczne. Chodzi o zbudowanie swojej pozycji i pokazanie swojej mocy i sprawczości. Dlatego nie poleciał z Karoliną. Ale ma GDZIEŚ etykietę Aurum XD.
    * Stymulacja: Żorż i dziewczyny. Działanie TERAZ. Planowanie, tak, ale natychmiastowe szybkie działanie. Elementy hedonizmu. Musi się dziać jak najszybciej. Ekstrema w życiu.
* V: Marysia używając Torszeckiego i Karoliny wypytała o kontakt między nim i Amelią. Dostała:
    * Ernest nie ma żadnego kontaktu z Amelią. Nie mają jak.
    * To Aurum ich rozdzieliło, bo pojawiły się pewne plotki. Zdaniem Ernesta - bo Amelia chciała tutaj zrobić porządek. Dlatego on tu przybył by skończyć jej dzieło.
    * Ernest nie powiedział tego wprost, ale Marysia widzi, że jemu też zależy na Amelii.
    * Ernest ma bardzo wysoką opinię o Amelii. Wierzy, że ona jest bardzo "jak paladynka".
* Czemu doszło do wymiany?
    * To, że Aurum uderzyło w tien Morlana (Marysia wie, że Jola Sowińska) sprawiło, że Amelia ("paladynka jak zawsze") próbuje to naprawić.
    * Aurum zażyczyło sobie konkretnie jego na wymianę. Rozkaz z góry. "Bo plotki". Bo jest wychowankiem Morlana i zdaniem Aurum póki Morlan ma Amelię to nic jej nie zrobi jak Ernest jest w Aurum.
    * Morlan zainterweniował na prośbę Amelii - niech Ernest będzie tutaj. W Pustogorze. Amelia się bała, że ktoś zrobi krzywdę Ernestowi SPECJALNIE po to, by doprowadzić do wojny Morlan - Sowińscy. Bo są siły nienawidzące Morlana w Aurum.
* Plotki Amelia - Ernest?
    * Ernest jest wściekły. Plotki mówią, że on używa swojej mocy, by Skazić Amelię i zdobyć jej sekrety. Że on jest Mrocznym Mistrzem Szpiegów. A on taki nie jest!
* Czy Amelia coś mówiła o Marysi Ernestowi?
    * Amelia powiedziała, że jest tu Marysia Sowińska, jej kuzynka.
    * Przestrzegła, że nie można Marysi w 100% ufać, ale jej serce jest po właściwej stronie.
        * I to najbardziej Marysię zaskoczyło. Podejrzliwość Marysi rośnie. Nie chciała niszczyć wizerunku czy tak uważa?

Zdaniem Marysi Ernest jest nieprawdopodobnie naiwny politycznie. Łączy się z Morlanem, który jest potworem. I jako ukochaną ma Amelię, która... jest potworem. A on sam jest kochanym słodkim paladynem nie znającym życia. Straszne.

Czyli... Marysia wie już, czego chce Ernest. Ernest wie, że Marysia może mu pomóc. I teraz zostaje tylko jedno - wspólne przejęcie kontroli nad Rekinami.

Ernest proponuje od razu działanie. Zebrać ochotników. Zrobić coś dobrego. Marysia + Ernest. Karolina facepalmuje - niech lepiej Ernest spróbuje zrozumieć lokalnych magów. Marysia potwierdza - niech Ernest spróbuje zrozumieć a potem dopiero działa z innymi. A potem zobaczymy, co da się zrobić. Pojawi się kolejny problem do rozwiązania prędzej czy później...

Marysia tłumaczy - niech Ernest wyobrazi sobie, że jego trzy "aniołki" wpada do wioski i chcą pomóc. Połowa wioski chce je zaprosić na miły i upojny wieczór, druga połowa wypędzi bo to wiedźmy. Keira włączy miecz i skończy się fatalnie. Albo coś im się stanie albo będzie jatka. Karolina - stawia na jatkę. Włączą się terminusi, bo Ernest działa na ich terenie. A oni się nie pierdolą w tańcu. A Marysi zależy na bezpieczeństwie Ernesta i jego dobrym samopoczuciu. "Nie mogłabym naszej wspólnej przyjaciółce przekazać, że coś Ci się stało". (plus jak się nie uda przekonać, dyskretny hint do Tukana by to zneutralizował)

ExZ+4+3O (Tukan):

* XX: Ernest MUSI rozwiązać problemy "na miejscu". Wielka wrogość Rekinów - ich zdaniem Ernest ma niewolników, a to jest coś, co dla niektórych Rekinów jest nie do przekroczenia. To jest ZŁO.
* X: Bulterier zrobi STRASZNY CZYN by zwrócić uwagę Ernesta i zmusić go do działania.
* Xz: Torszecki domyślił się linka Ernest - Amelia. Powie Marysi by wiedziała.
* (+3Vg) Vg: Ernest nie będzie robił akcji "samodzielnie" bez pewności lub sygnału, że to ma sens i zadziała jakoś. Nie będzie "zbawicielem na białym koniu" losowych wiosek. Nie wpadnie przez to w katastrofalne kłopoty.

Karolina, Marysia i Ernest usłyszeli straszliwy wybuch. Na zewnątrz. Systemy apartamentu pokazały co się stało - ktoś WYSADZIŁ ścigacz Ernesta, bardzo skutecznie. Ścigacz jest zniszczony.

Niedaleko w swoim bojowym ciężkim pancernym ścigaczu siedzi Franek Bulterier i czeka.

Ernest jest biały. Ze wściekłości i bólu. Powiedział tylko słowo "Dain". Przeprosił tien Sowińską i wychodzi. Sam. Karolina nie rozumie co się dzieje ale próbuje coś zrobić.

Karolina wybiega tylnym wyjściem, cicho do ścigacza i strzela z serii w Bulteriera. To reakcja na atak przy Marysi i atak gościa i CHCE GO SKRZYWDZIĆ. Karolina nie daje ostrzeżenia. Bulterier też nie dał.

Karolina atakuje Bulteriera. SKRZYWDZIĆ go. Zmusić do pojedynku w powietrzu. Plus komunikator "co Ty sobie kretynie wyobrażasz!"

TrZ+2:

* V: Zaskoczenie, Bulterier jest ranny zanim włączył osłony ścigacza.
* X: Karolina jest nieostrożna i wściekła. Jej ścigacz został uszkodzony.
* X: Ścigacz spenetrowany; rana Karoliny. Rany za rany. Bulterier gra ostro.
* V: Pojedynek w powietrzu. Faktycznie, Bulterier poleciał.

Bulterier odpowiada, jak to on, warcząc "Ten koleś niewoli kobiety. Jak niektóre rody. A Ty z nim na herbatkę!" Karolina "jak dla mnie wyglądały na całkiem pyskate". Bulterier "jak wiesz jak bić, wiesz jak sprawić by się dobrze zachowywały. Eternia to magia krwi. Pełna kontrola. Widziałaś, co chciał Ci pokazać."

* Vz: Karolina go zestrzeliła. Lepsze manewry, lepszy pilot. Wściekłość Bulteriera nie dorównuje umiejętnościom Karoliny. Rozbił się gdzieś w Lesie Trzęsawnym.

Ernest nie dowierza, że Bulterier mu... uciekł XD. Simulacrum migocze w jego Wzorze. Marysia próbuje zwrócić uwagę, ale bez większego powodzenia XD.

Marysia używa magii. "STÓJ!". Próbuje do niego trafić, ale nie zrobić mu krzywdę, z nadzieją, że mimo tego stanu zobaczy co jest grane. Torszecki przybiegł chronić Marysię jak co.

ExZM+3:

* X: Ernest zareagował instynktownie na magię. Zaatakował - na szczęście pięściami. Jest dobry w walce i zadawaniu bólu. Torszecki się przekonał - zasłonił Marysię. Pociekła krew Torszeckiego aż Ernest szybko się zorientował w sytuacji.
* V: Marysia wydała rozkaz i magicznie uspokoiła Ernesta. Odzyskał kontrolę. Przestał bić Torszeckiego. 
    * Marysia: "skrzywdziłeś niewinnego Torszeckiego!"
    * Ernest: "stanął między mną i Tobą..."
    * Marysia: "czy Twoje simulacrum miało zabić człowieka który może być niewinny?"
    * Ernest: "a kto inny mógł to zrobić?"
    * Marysia: "musisz się wiele nauczyć - to niekoniecznie on"

Marysia zorientowała się, że Ernest jest magiem bojowym typu katai - używa ciała do walki. Jak Arkadia.

* Vz: Marysia przekonała Ernesta, by ten poczekał. Marysia i Karolina dojdą do tego kto za tym stoi i oddadzą go w ręce sprawiedliwości.

TYMCZASEM KAROLINA. Wylądowała szybko do ścigacza Ernesta i oszacowuje - da się uratować? MOŻE. Karolina jest mistrzowskim scrapperem. Szybko żąda złomu i elementów od Sowińskich. Ona chce naprawić ścigacz i uratować Daina jeśli się da...

Karolina -> Ernest, "czy możesz jakoś pomóc". Ernest otworzył rękę nożykiem. Krew. Utrzymać Daina. Utrzymać energię. Jest w sieci, czyli jest możliwy do stabilizacji.

ExMZ+3+3Or:

* XXX: mimo NIESAMOWITYCH wysiłków nie da się uratować...
* X: MIMO ogromnych prób i poświęceń - nie udało się. Ernest dostał potężny feedback Esuriit. Ma tydzień z głowy, ciężka regeneracja w apartamencie.
* X: (darmowy) -> nie wpłynęło to w żaden sposób pozytywnie na podejście Ernesta do INNYCH. Tylko Karolina i Marysia są "te dobre" na razie. Ernest skrajnie nieufny wobec innych.

Dain i ścigacz są zniszczone. A Ernest jeszcze bardziej docenia problemy jakie ma Marysia i jakie musiała mieć Amelia... Ernest uważa, że jest na terenie wroga.

Czyli Ernest klasyfikuje tak:

* Karolina -> dobra. Te same cele. Cyniczna, bo wiele widziała.
* Marysia -> dobra. Te same cele. Te same problemy co Amelia. Wykastrowana z mocy.
* Torszecki -> potencjalnie wartościowy. Podlizuje się Marysi by przetrwać w Aurum.
* inne Rekiny -> problem. Nie są assetem, są liability. Gdyby ich nie było, byłoby lepiej.
* potencjalne-Rekiny-przedstawione-przez-Karolinę-i-Marysię -> potencjalnie wartościowe. Case-by-case. Domyślne nastawienie optymistyczne.

(Bulterier powiedział Karolinie, że nie on podłożył ładunek. Wie kto, ale nie powie. Chciał uczciwej naparzanki, ale tien eternijski nigdy nie wyszedłby z nim walczyć. Ale nie on wysadził ścigacz - on by poczekał, aż tien eternijski nie wyjdzie do niego. Wtedy by wysadził. Karolina mu powiedziała, że zabił człowieka. Bulterier WTEDY powiedział, że to nie on. Ale nie powie kto - z jakiegoś powodu.)

## Streszczenie

Ernest z Eterni okazał się być zupełnie inny niż się wydaje. Lekko naiwny, z paladyńskim zacięciem, chce czynić dobro i integrować Rekiny dookoła Marysi. Chce dokończyć dobre dzieło Amelii. Marysia znalazła z nim linię porozumienia a Ernest x Karolina są jak dwa łyse konie. Marysia przekonała go, by sam nic nie robił - nie rozumie terenu. Niestety, ktoś wysadził żywy ścigacz i zabił ducha z jego arkinu. Ernesta zatrzymała Marysia. Karolinie nie udało się ducha uratować. Dziewczyny zdecydowały, że znajdą mordercę...

## Progresja

* Marysia Sowińska: ma przyjaźń i zaufanie Ernesta Namertela. 
* Karolina Terienak: ma przyjaźń i zaufanie Ernesta Namertela. Ernest czuje z nią duży kontakt. Jest flow.
* Rafał Torszecki: szczera nienawiść do Ernesta - wyśmiewał go, nie szanuje Marysi i ma większą świtę niż ona. Plus, ciężko go skrzywdził poniżej 5 sekund.
* Rafał Torszecki: 5 dni regeneracji u Różewicza Diakona. Ernest zadaje straszne obrażenia.
* Rafał Torszecki: zorientował się, że jest coś między Amelią Sowińską i Ernestem Namertelem. Wie też, że Ernest patrzy na Amelię jak na paladynkę. I wie, że Amelia to podła sucz.
* Ernest Namertel: Marysia i Karolina są jedynymi Rekinami jakim ufa i lubi. Inni są niebezpieczni lub tolerowalni.
* Ernest Namertel: następne 7 dni regeneracji po Skażeniu Esuriit; nie udało mu się uratować Daina.

### Frakcji

* .

## Zasługi

* Marysia Sowińska: przekonała do siebie Ernesta i używając Torszeckiego wyciągnęła z Ernesta jego sekrety i relację z Amelią. Przekonała go też, żeby nie robił nic sam. Gdy Ernest stracił nad sobą kontrolę, zatrzymała manifestację simulacrum i przekonała (ponownie) by oddał jej śledztwo. Dobija ją porównanie z Amelią - "że obie są paladynkami". Nonsens. Amelia nie jest ;p.
* Karolina Terienak: zaprzyjaźniła się z Ernestem, ignorując bariery tienowatych. Zapoznała go z Marysią, podroczyła się odnośnie ścigaczy a potem by ratować Bulteriera ostrzelała go ścigaczem, wygrała z nim w powietrzu i niestety nie udało jej się uratować ścigacza Ernesta. Mimo wsparcia jego Krwi i Sieci.
* Ernest Namertel: naiwny wobec ludzi i polityki, ale świetny w walce i taktyce. Ma gdzieś protokoły, chce pomagać. Wierzy, że Marysia i Amelia są "dobre" i zaprzyjaźnił się z Karoliną. Bardzo przyjacielsko traktuje swój arkin. Gdy jego ścigacz (żywy) został wysadzony, prawie uruchomił simulacrum. Marysia przekonała go, by oddał jej śledztwo. Rozpacza w ciszy.
* Żorż d'Namertel: ciacho jak cholera; wesoły, flirtuje z Karoliną i żartuje o kiełbasie. Dobry kucharz, postura Conana, lubi chodzić w samych spodenkach.
* Keira Amarco d'Namertel: bardzo lekko ubrana (eufemizm) zabójczyni Ernesta. Mało mówi. Brunetka. W jakiś sposób ma dostęp do kinezy (przemieszczanie się, akceleracja) i do ostrza eterycznego. Ale zabójca powinien być człowiekiem a nie magiem.
* Azalia Sernat d'Namertel: bardzo lekko ubrana (eufemizm) mistrzyni dyplomacji, zarządzania, logistyki i szpiegostwa Ernesta. Sassy. Lubi dogadywać i bardzo blisko przyjaźni się z Żorżem. Blondynka. 
* Rafał Torszecki: wpierw robił jako herold Marysi wzbudzając radość Ernesta, potem pomagał Marysi wydobyć od Ernesta wiedzę (jako błazen nie ze swojej winy). Doszedł do relacji Ernest - Amelia. Gdy Ernest stracił panowanie, zasłonił Marysię i dostał poważne rany.
* Franek Bulterier: chciał się bić z Ernestem. Wie, kto wysadził ścigacz Ernesta (nie wiedział, że ów żyje). Przed śmiercią do simulacrum uratowała go Karolina, wchodząc z nim w strzelankę ścigaczami - rozbił się w lesie.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Podwiert
                                1. Dzielnica Luksusu Rekinów
                                    1. Serce Luksusu: ktoś wysadził ścigacz Ernesta, ryzykując manifestację Simulacrum. Marysia i Karolina rozbroiły tą specyficzną minę.
                                        1. Apartamentowce Elity: apartament Ernesta jest większy niż Marysi. Niestety.
                                1. Las Trzęsawny: tu rozbił się Bulterier po powietrznym starciu z Karoliną.

## Czas

* Opóźnienie: 1
* Dni: 1

## Konflikty

* 1 - Ernest blokuje akcje swojej świty po arkin, by nic głupiego nikt nie zrobił.
    * Tr+2
    * VX: Ernest zatrzymał; nic się Karo nie stało ale Ernest nie dał rady ukryć mocy swoich ludzi.
* 2 - Marysia wyciąga informacje z Ernesta. Czemu tu jest? Co chce dostać? Czy to poza czy prawda?
    * TrZ+4
    * VzVzXXVVV: Marysia poznała prawdę oraz sekrety Ernesta. Torszecki nienawidzi Ernesta. 
* 3 - Ernest proponuje od razu działanie. Zebrać ochotników. Zrobić coś dobrego. Marysia tłumaczy - stop, zróbmy to spokojnie. Nie znasz terenu.
    * ExZ+4+3O (Tukan)
    * XXXXz: Ernest musi wpierw rozwiązać problemy z Rekinami; ktoś robi STRASZNY CZYN; Torszecki złapał link Amelia - Ernest.
    * (+3Vg): Vg: Ernest nie będzie "zbawicielem na białym koniu" losowych wiosek.
* 4 - Karolina atakuje Bulteriera. SKRZYWDZIĆ go. Zmusić do pojedynku w powietrzu. Plus komunikator "co Ty sobie kretynie wyobrażasz!"
    * TrZ+2
    * VXXVVz: Bulterier i Karolina ranni; Bulterier poza zasięgiem simulacrum; Karolina Bulteriera zestrzeliła.
* 5 - Marysia używa magii. "STÓJ!". Próbuje do niego trafić, ale nie zrobić mu krzywdę, z nadzieją, że mimo tego stanu zobaczy co jest grane.
    * ExZM+3
    * XVVz: Ernest skrzywdził Torszeckiego; Marysia uspokoiła Ernesta i go przekonała, żeby ten poczekał a one zrobią śledztwo.
* 6 - Karolina jest mistrzowskim scrapperem. Szybko żąda złomu i elementów od Sowińskich. Ona chce naprawić ścigacz i uratować Daina jeśli się da...
    * ExMZ+3+3Or (krew Ernesta)
    * XXX: mimo NIESAMOWITYCH wysiłków nie da się uratować...
    * X: Ernest dostał potężny feedback Esuriit. Ma tydzień z głowy, ciężka regeneracja w apartamencie.
    * X (darmowy): -> nie wpłynęło to w żaden sposób pozytywnie na podejście Ernesta do INNYCH. Tylko Karolina i Marysia są "te dobre" na razie.

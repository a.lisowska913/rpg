---
layout: cybermagic-konspekt
title: "Elainka kontra cały świat"
threads: wolne-tai
gm: żółw
players: kić, darken, vizz
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [201224 - Nieprawdopodobny zbieg okoliczności](201224-nieprawdopodobny-zbieg-okolicznosci)

### Chronologiczna

* [201224 - Nieprawdopodobny zbieg okoliczności](201224-nieprawdopodobny-zbieg-okolicznosci)

## Punkt zerowy

### Dark Past

.

### Opis sytuacji

* Zespół trafia na umierający, nie reagujący statek "Granola" i próbuje go wskrzesić i zregenerować.
* Na "Ogienku" były działania polegające na wprowadzeniu dzikiej sentisieci. Skończyły się tragedią i infekcją nanitkową.
* Wolna TAI Elainka d'Serratus współpracując z nieaktywną Persefoną próbuje powstrzymać skażenie nanitkowe na Granoli.
* Zespół ma postacie ludzkie; nic nie wie. Mają uratować Andreę, pomniejszą arystokratkę Aurum. I cały statek.

### Postacie

* Vito: postać Vizza. Biostymulator.
* Kara: postać Kić. Advancer.
* Yaga: postać Darkena. Badacz Otchłani + hacker.

## Punkt zero

.

## Misja właściwa

Granola dotarła w pobliże martwego Ogienka. Wszystko wskazuje na to, że statek jest martwy i nieaktywny. Zbrickował? Yaga spróbował sprawdzić sensorami co się dzieje z Ogienkiem; nie mógł przebić się przez zagłuszanie. Ale dopalił więcej energii do skanerów. Przebije się.

* XX: Zagłuszanie. Persefona na Ogienku wyraźnie jest martwa lub nieaktywna. Nie da się do niej dostać ani zbadać
* V: 244 osoby są żywe, są na pokładzie ślady organiczne. Są tam ludzie - nie wiadomo jednak w jakim stanie. Nie da się łatwo wykryć niczego ekstrabiologicznego.
* X: Granola straciła sensory. Przesterowanie nie przyszło jej dobrze. Ale wykryła jeszcze elementy i sektory których nie ma w planach Ogienka; czyżby tam było więcej wszystkiego? Tajne pomieszczenia?

Dziwne. Tak czy inaczej, trzeba jakoś się tam dostać. System obronny (point defence) działa, więc Granola wystrzeliła hak i połączyła się z Ogienkiem. Kara, advancerka, wpełzła przez hak na Granolę:

* V: wbiła się
* V: podłączyła Granolę do Ogienka; Persefona może spróbować uruchomić i działać ze statkiem
* XX: stracony kontakt
* V: advancerka w ukryciu, Kara się schowała

Kara podpięła połączenie Granola - Ogienko przez terminal. Gdy Persi d'Granola dostała dane i info z Ogienka, Kara usłyszała wleczenie się. Szybko włączyła niewidzialność w Eidolonie i się schowała. Zobaczyła serratus Andrei; dokładniej to bardzo ciężko uszkodzony, zakrwawiony (nie swoja krew) serratus który wyglądał kiedyś jak ładna dziewczyna, ale spod plasticiała wystają metalowe fragmenty. Serratus wlokąc się na uszkodzonej nodze rozłączył terminal. Próbował znaleźć Karę - nie udało mu się. Kara się wycofała; serratus popełzł gdzieś dalej, uszkadzając swoje mechanizmy.

Kara nie wie co o tym myśleć, ale ma dowód, że stało się tu coś złego.

Tymczasem Yaga użył Persefony d'Granola. Czas wykorzystać połączenie, które zestawiła Kara między statkami kosmicznymi:

* VVVXXX: tymczasowa kontrola z zewnątrz, stała od środka. Będąc w środku Ogienka Yaga może użyć Persefony d'Granola (repliki) by pomóc sobie z systemami Ogienka.
* V: Ogienko widzi ich jako "swoich"
* V: mogą bezpiecznie dokować / szybko odlatywać itp
* V: silniki nieaktywne. Ogienko im nie odleci z zaskoczenia

Yaga zauważył, że wszystkie systemy Ogienka są wyłączone, łącznie z minimalnym poziomem lifesupport. Reaktor jest w trybie minimalnym, awaryjnym. Nie wie o co chodzi, więc woli poczekać. Zbadają to jak dostaną się na pokład.

Yaga i Vito dołączyli do Kary; Vito zostawił sobie stały link z biovatami Granoli a Yaga wziął potężny plecak z repliką Persefony.

Kara śledziła serratusa (Elainka d'Serratus). Serratus ma wyraźnie uszkodzone sensory i wyłączył wszelkie metody komunikacji, działa autonomicznie. Śledząc serratusa natknęła się na ludzkie zwłoki - członek załogi. Serratus idzie w kierunku na medbay. Kara skręciła na AI Core; tam natknęła się na kolejne zwłoki patrolowane przez dronę. Niekoniecznie będzie łatwo Karze się koło niej prześlizgnąć; duże ryzyko.

Vito wpadł na podły pomysł. Elainka nie jest tak potężną TAI jak Persefona. Nie wiadomo kto kontroluje te drony i kto za to wszystko odpowiada. Więc sfabrykował w biovatach Granoli 3k10+30 cyberszczurów, po czym je zdesantował na Ogienko. Te szczury pozwolą wprowadzić chaos, potencjalnie przeciążyć Elainkę i na pewno zakłócić funkcjonowanie dron.

* VVV: swarm cyberszczurów odwrócił uwagę Elainki i udowodnił, że to Elainka d'Serratus kontroluje drony. Patroluje nimi AI Core, mostek, reaktory, medbay i life support.
* XX: Vito stracił kontakt i kontrolę nad szczurami. Nie wie o tym, ale dołączyły do sentisieci.
* OO: wiedzą gdzie jest Andrea - jest w medbay, zamknięta w biovacie. Pilnuje jej serratus.

Korzystając z zamieszania, Kara prześlizgnęła się koło drony i przy użyciu szczurów i wiedzy Kary udało się odwrócić uwagę drony; reszta Zespołu przekradła się w kierunku na AI Core. Tam po drodze coraz więcej ciał i zwłok, też ludzi którzy nie powinni nigdy tu być (np. z kuchni, nieuprawnionych). Wszyscy zginęli od serratusa. Yaga wślizgnął się do AI Core używając kanału technicznego, by nie alarmować Elainki d'Serratus.

Okazało się, że AI Core z Persefoną jest wyłączony i odcięty (serią laserów Elainki) od zasilania. Yaga podłączył AI Core z powrotem, używając swojej wiedzy i Persefony zaczął badać, reaktywować i monitorować co się dzieje. Przygotował też EMP i anty-TAI na wypadek kłopotów (Ex):

* VV: infekcja nanomaszynowa AI Core. Sentisieć. SKĄD SENTISIEĆ TUTAJ? Dezaktywacja AI Core powstrzymała rozwój sentisieci.
* O: udało się przekierować sentisieć na replikę Persefony d'Granola. Po czym ta Persefona została natychmiast zniszczona.
* V: udało się postawić czystą, sprawną Persefonę d'Ogienko.

Persefona d'Ogienko wyjaśniła, co się stało:

* Na pokładzie ktoś prowadził badania nad dziką sentisiecią. Niewłaściwa arystokratka próbowała ją przejąć i stała się wektorem.
* Sentisieć dotarła do reaktora i AI Core.
* Elainka odcięła Persefonę (zainfekowaną, acz nieprzejętą) i Reaktor. Containuje wszystko. Jest na awaryjnej energii i odcięła wszystkie sensory i wektor infekcji sentisiecią itp.
* Persefona przekazała Elaince wszystkie drony jakie ta ma do dyspozycji i może objąć.
* Serratus jest w krytycznym stanie.

Yaga chce współpracować z Elainką d'Serratus. Chce przekazać sygnał. Persefona d'Ogienko włączyła światła w medbay. To znak (oczywisty), że AI Core działa. Elainka pełznie w uszkodzonym serratusie.

Yaga próbuje się skomunikować z wyniszczonym serratusem: (VXXXX). Serratus odmawia komunikacji; nie wie, czy są zainfekowani czy nie. Nie jest w stanie dostać się do AI Core - przesterowane działko wybucha uszkadzając krytycznie serratusa. Serratus, na krytycznej energii, wraca do medbay. Albo się coś zmieni, albo Elainka zabije swoją przyjaciółkę - Andreę - by ta nie stała się nośnikiem sentisieci.

Ok. Niewiele da się zrobić z serratusem. Ale trzeba się pozbyć sentisieci - jak?

Po pierwsze, na wszelki wypadek, Yaga wysadził wszelkie bronie point-defense i wewnętrzne na pokładzie Ogienka przy wsparciu Persefony d'Ogienko. Nie może przejąć ich sentisieć.

Po drugie, Yaga współpracując z planami Persefony projektuje najpotężniejszy EMP przy użyciu reaktorów jaki jest możliwy. Coś, co zniszczy sentisieć za wszelką cenę:

* VVVVVV: uda się.

Ale jak zniszczyć CAŁOŚĆ sentisieci? Potrzebna jest zasadzka, smaczna pułapka, atraktor. I tu wszedł Vito. KRÓL SZCZURÓW. Smaczny jak cholera, atraktor mający przyciągnąć jak najwięcej sentisieci w pobliże reaktora:

* XX: wybudzą się też osoby w stazie (wsadzone tam przez Elainkę)
* X: Elainka nie wytrzyma. Zostanie zniszczona.
* V: silny atraktor
* X: Andrea będzie w stanie terminalnym; potrzebuje super pomocy medycznej
* V: ściągnie więcej

Kara wzięła od Vito najcięższy stymulant jaki jest, wzięła bombę / mutator od Yagi i pobiegła w kierunku na reaktory, by zrobić wielkie bum.

* VOV: sukces, acz z ciężkim zejściem
* X: Eidolon zostanie uszkodzony / niesprawny
* VV: zdecydowana większość nanomaszyn zostanie zniszczonych

14 (V), 7 (X) -> VVXXXVVV:

* Reaktory nie wytrzymały. Eksplozje.
* 43 osoby zginęły w wyniku tej operacji. Za duże sprzężenie z nanitkami sentisieci. Plus, eksplozje.
* Andrea potrzebuje NATYCHMIASTOWEJ pomocy medycznej

Vito dał radę pomóc Andrei - składając ją jakoś do kupy używając zwłok i tego co ma pod ręką. Ale potrzebuje silnej implantacji i straciła swój Wzór i możliwość kontaktu z sentisiecią.

Zespół dał radę - Granola zarobi, uratowali Ogienko. Persefona świadkiem. Nie wiadomo kto tu robił eksperyment i co chciał osiągnąć, ale to jest już kwestia wewnętrzna Aurum...

## Streszczenie

Ogienko - statek klasy RAMA - przestał odpowiadać i się wyłączył. Przyleciał statek ratunkowy Granola by wyciągnąć ludzi i uratować co się da. Na miejscu okazało się, że Ogienko jest zainfekowane przez nanitki z dzikiej sentisieci przeszczepionej kiedyś w przeszłości, co zainfekowało TAI Persefona. Persefona wyłączona przez Elainkę d'Serratus, która jako jeden z nielicznych niezainfekowanych agentów powstrzymywała infekcję sentisieci. Zespół dał radę uratować arystokratkę (Andreę) i wyczyścić Skażenie sentisiecią, acz kosztem wielu żyć i zniszczenia Elainki i Persefony.

## Progresja

* Andrea Burgacz: straciła Elainkę (przyjaciółkę) i wymaga implantacji po strasznych wydarzeniach z sentisiecią na Ogienku. Wklejono w nią sporo części z ludzkich zwłok co uszkodziło jej wzór.

### Frakcji

* .

## Zasługi

* Andrea Burgacz: pomniejsza 19-letnia arystokratka Aurum, która miała kontrolować sentisieć na Ogienku; skończyło się jej ciężką infekcją i zamknięciu w biovat. Uratowana przez załogę Granoli.
* SCA Granola: Statek Cywilny sektora prywatnego Astorii zajmujący się misjami ratunkowymi. Niemały, podobny w strukturze do niesławnego Grazoniusza. Tu: uratował Ogienko, okręt RAMA Aurum.
* OA Ogienko: statek typu RAMA, na którym prowadzono eksperymenty z dziką sentisiecią Aurum (i co wyszło spod kontroli). Bardzo poważnie uszkodzony, na holu przez Granolę.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański

## Czas

* Opóźnienie: -4946
* Dni: 3

## Inne

PS: jest to nawiązanie do "Odkupienia". Statku Aurum, który pojawi się za 13 lat pod kontrolą Sowińskich. Tamten statek też jest sentisprzężony, acz prawidłowo.

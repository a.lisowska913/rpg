---
layout: cybermagic-konspekt
title: "Ukradziony Entropik"
threads: nemesis-pieknotki
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [191112 - Korupcja z arystokratki](191112-korupcja-z-arystokratki)

### Chronologiczna

* [191112 - Korupcja z arystokratki](191112-korupcja-z-arystokratki)

## Budowa sesji

### Stan aktualny

* Kompleks Itaran jest bezpiecznie ukryty niedaleko Jastrząbca
* W Kompleksie prowadzone są badania i próby ratowania ludzi używając Black Technology
* Jednym z pacjentów a jednocześnie strażnikiem interesów Grzymościa jest niestabilny Melwin Hlagor
* Ataienne wykryła z Jastrzębca sygnaturę wskazującą na zaginionego parę miesięcy entropika.
* Damian wysłał miragenta, by ten rozwiązał problem. Pięknotka jako wsparcie dla miragenta. Gdzie jest cholerny entropik?

### Dark Future

* Miragent Keraina wpada pod kontrolę Przeciwnika
* Entropik ginie wraz z Melwinem
* Hestia d'Itaran ginie
* Pacjenci są bezpiecznie ewakuowani

### Pytania

1. Czy Przeciwnik przejmie kontrolę nad Kerainą (miragentem)?
1. Czy uda się ewakuować rannych Grzymościowi?
1. Czy uda się ustabilizować Melwina?

### Dominujące uczucia

* "Zniszczyć to, co powinno być od dawna martwe? Co oni mu zrobili. Jak go uratować."
* "Grzymość, coś ty narobił..."

### Tory

* .

Sceny:

* .

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Misja właściwa

Jastrząbiec. Ktoś zapieprzył Orbiterowi Entropika 3 miesiące temu i sfingował zniszczenie transportu. Ataienne po swoim przejęciu kontroli nad Jastrząbcem wykryła słabą sygnaturę, więc dała znać. I dlatego Pięknotka teraz wraz z miragentem imieniem Keraina znajduje się w Jastrząbcu. GDZIEŚ jest Entropik - servar tak superciężki i skomplikowany, że bez odpowiednich implantów nie da się nim sensownie sterować.

Pięknotka skierowała pierwsze kroki do Persefony. TAI przekazała Pięknotce zapisy z danych i Pięknotka przekazała je Kerainie. Ta zaczęła analizę - wyszło coś ciekawego (2 pkty):

* V: Jest sygnał Entropika. Jest słaby, dobrze ukryty. Najpewniej nie jest w samym Jastrzębcu
* V: Dużo wskazuje na to, że _power core_ Jastrząbca (statku) ma większe zużycie niż powinno. Chyba cokolwiek się nie dzieje jest podpięte do reaktora Jastrząbca
* V: Jakim cudem TAI Persefona tego nie zauważyła - ani Entropika, ani nadużycia reaktora? To jest dziwne, bo zdaniem Kerainy Persefona powinna była to wykryć
* X: Druga Strona wie, że tu jest Pięknotka. Jeszcze nie traktują jej śmiertelnie poważnie, ale już o niej wiedzą.
* Zdaniem Kerainy, Persefona powinna wykryć zarówno Entropika jak i drenaż energii.
* Nadmierny drenaż energii itp trwa od ponad roku.

Co cóż, na tym Pięknotka się nie zna szczególnie. Tu zwraca się do Mariusza Trzewnia. Niech on jej znajdzie co to może być (2 pkty). 

* X: Trzewń nie chce używać systemów Barbakanu bo boi się Karli. Ale zna kogoś, kto zna kogoś, kto... - więc docelowo sygnał dotrze do Drugiej Strony.
* V: Ten typ sygnatury energii, to pobieranie energii - to wskazuje na kompleks militarny, naukowy... coś mniejszego niż Tiamenat, ale tego typu. Ale w okolicy nie ma takiego czegoś. Najpewniej ten kompleks ma własne źródła energii, ale kiedyś czerpał z Jastrząbca głównie, a teraz tylko "pobocznie". To by wyjaśniało taką sygnaturę.
* V: Trzewń obliczył, że ten kompleks może być maksymalnie 10 km w promieniu od samego Jastrząbca. Czyli mówimy o czymś pod ziemią.
* X: Hestia d'Itaran przeprowadza dywersję odciągającą uwagę terminuski. Zostawi Red Herring.

Czyli - Pięknotka widzi sytuację. Gdzieś tu jest podziemne laboratorium, cholera wie kiedy założone i od dawna. I gdzieś tam jest ten cholerny Entropik. A na podstawie sygnałów Entropika Pięknotka jest w stanie złapać przybliżoną lokalizację. Ale Pięknotka ma za małą siłę ognia, by móc cokolwiek z tym zrobić.

Oki. Czas na przebranie. Pięknotka jako przykładny terminus wie, że problem jest rozwiązany. Pobrała dane, wykryła niezgodność i zgłasza sprawę do Barbakanu. Zakłada sprawę i jest Oficjalnym Terminusem W Sprawie Followup. Teraz może spokojnie grzebać :-).

Dane z Orbitera, po naciśnięciu Kerainy; informacje o Entropiku, który zniknął:

* Pilot tego Entropika zginął. Nie jakoś tak dumnie. Normalna śmierć.
* Entropik wraz z NOWYM pilotem leciał na przeróbkę do odpowiedniego statku jakieś 4 miesiące temu.
* Burza magiczna. Rzadkie, ale się zdarza. Statek powinien to przetrwać. Nie przetrwał. Statek zniknął. Więc nowy pilot i stary Entropik zniknęli. Nie byli kompatybilni.
* "Nowy pilot" nazywał się Melwin. Solidny agent Orbitera, przeszedł implantacje i szkolenie na Entropika.

Tak więc, Orbiter potraktował sprawę jako KIA - po śledztwie, analizach itp. Takie wypadki się zdarzają w kosmosie - nie są częste, ale się faktycznie zdarzają. Nie mieli powodu nie wierzyć. Aż tu nagle - sygnał z Jastrząbca. I to uzyskany przy okazji. Tak więc tematem zajął się Damian Orion - ponieważ on najlepiej zna Kogoś W Pustogorze - Pięknotkę.

Pięknotka zażądała od Persefony uzasadnienia zużycia energii - w jaki sposób można ją oślepić. Persefona odpowiedziała:

* W okolicy znajdują się liczne Containing Chambers. Tam się przechowuje niebezpieczne chemikalia, odpady medyczne, adaptogen.
* W samym Jastrząbcu znajduje się doskonała klinika. Tam są wysokiej klasy lekarze i biomanci (trzech). Tam są prowadzone badania medyczne.
* Jastrząbiec to był statek. Rozbił się. Ma nadmiernie silne generatory - są straty, ogromne straty, bo robiono fuszerkę i nikt nie miał czasu tego ponaprawiać (legacy). Tak więc jakkolwiek koszty energii są duże, to jednak reaktory Jastrząbca muszą działać - inaczej statek wygaśnie. Tak więc nikt nie patrzy na energię.

Minerwa, zapytana przez Pięknotkę, dodała ważny fakt:

* Persefona jest TAI militarnym. Nie zarządczym. Ona nie do końca ma dostęp do tych wszystkich wzorów i przełączeń do których dostęp ma np. Hestia.
* Persefona skupia się na bezpieczeństwie a Hestia na zarządzaniu. Hestia optymalizuje, Persefona broni.

Czyli ktoś kto to zaprojektował i zaplanował Wie O Terenie, zna się na różnych TAI, ma duże środki oraz potrafi dobrze grać w politykę, planowanie itp.

Pięknotka zażądała mapy składów, by skorelować je z lokalizacją Entropika. I Pięknotce udało się znaleźć coś ciekawego - jest o jeden skład więcej niż wskazuje na to mapa. To znaczy - wynika to z mapy energii itp. Jest jeden skład za dużo.

No cóż. Pięknotka ma problem. Albo wjazd na teren z Entropikem albo nie. Ale jeśli Entropika tam nie ma... będzie hańba. I to jest niefortunne. Najsensowniej byłoby wysłać Kerainę na akcję typu _stealth_, ale Pięknotka nie do końca jest w stanie ufać Kerainie. Teoretycznie Pięknotka dowodzi, w praktyce Keraina zrobi to, co uważa za stosowne. No i to jest klops. Pięknotka już wie, że nie może ufać "darom Orbitera".

Prawdziwy problem Pięknotki polega na tym:

* JEŚLI poprosi Karlę o wsparcie i poinformuje Pustogor (jak powinna zrobić), to automatycznie rani relacje z Orbiterem - oni chcą dyskretnej akcji usuwającej Entropika.
* JEŚLI poprosi o wsparcie Kirasjerów Orbitera, to dostaje 8 potężnych żołnierzy. Nie ma jak uniknąć ofiar cywilnych lub nawet niecywilnych - i co na to Karla?
* Sama Pięknotka nie ma kompetencji skradania się i analizy danych - ale Kerainie nie do końca może ufać, bo nie wie co Keraina zrobi.

Oki. Nic nie da się poradzić. Pięknotka wysłała Kerainę na zwiad - i będzie ją odpytywała gdy do czegoś dojdzie (+1 pkt = 5). A równolegle z Kerainą, Pięknotka śledzi przepływy pieniężne w Jastrząbcu używając Trzewnia; takie laboratorium musi mieć sprzęt, pozwolenia itp (+2 pkt = 7). Szuka spike'ów energii, wielkich transportów, rzeczy świadczących o lokalizacji laboratorium i jego właścielach.

Skan Kerainy w obszarze dywersji zrobionej przez Hestię był satysfakcjonujący:

* X: Hestia wie, że tu był robiony zwiad.
* V: Tam jest Contaiment Chamber. Nielegalny, potencjalnie niebezpieczny. Lekko chroniony, acz nic trudnego dla Kerainy.
* X: Hestia wie, że jej dywersja się nie udała i że Pięknotka szczurzy dalej.
* V: Tam nie ma laboratorium, ale to jest tak zaprojektowane by potwierdzić hipotezę, że to jest problem w Jastrzębcu. Czyli dywersja.
* V: Keraina przyniosła informację, że to jest ŚWIEŻO przygotowana dywersja acz Containment Chamber jest od dawna. Czyli to jest zaplanowane a Druga Strona wie.

Tymczasem Pięknotka odpaliła cały aparat analityczno-szpiegowski. Trzewnia, dane z Jastrząbca, widok z Epirjona, przepływy finansowe. To nie musi być szybkie - to musi być skuteczne (+1 pkt = 8)

* X: Wzmocnione poszukiwania zaalarmowały Hestię i Grzymościa. Wzmocnili plan palenia dowodów. (+1 pkt = 9)
* X: Hestia odpala plan awaryjny 'Human Shield' - ściąga ludzi, robi obniżki. Na terenie Kliniki będzie więcej osób niż kiedykolwiek w czasie gdy prowadzona jest ewakuacja.
* V: Pięknotka zlokalizowała laboratorium. Nałożenie na siebie wszystkich tych danych i ślad Entropika dały wyraźną odpowiedź - to laboratorium jest ukryte pod kliniką. U góry - lekarze, dobre miejsce do pomocy ludziom. Na dole - kompleks badawczy. To tam ciągnie się nadmierna energia.
* V: Pięknotka jednoznacznie była w stanie określić do kogo płyną pieniądze. A raczej - skąd. Ta skala i te możliwości wskazują na jednego maga. Roland Grzymość. To znaczy - Pięknotka nie ma twardych DOWODÓW takich, jakie można użyć, ale wszystko kieruje się na niego.
* Entropik siedzi gdzieś pod kliniką. A jeśli zniknie z promienia Jastrząbca, nie będzie dało się łatwo znaleźć jego sygnatury.
* V: Skąd tu Entropik? Korelacja wszystkich danych pokazała coś ciekawego - Grzymość NIE PŁACIŁ za pozyskanie Entropika. Więcej, jemu zapłacono. Nie da się łatwo określić kto, co, jak przez dziesiątki spółek córek itp., ale wszystkie ślady prowadzą do Cieniaszczytu. Ale to pokazuje coś ważnego - Grzymość nie jest stroną aktywną a wykonawcą. A ogólnie analiza finansów tego wszystkiego pokazuje, że Grzymość TRACI pieniądze na tej klinice i podziemnym laboratorium. A Entropik przyniósł mu pieniądze. Więc... tu się dzieje coś więcej.
* Co się dzieje w laboratorium: dane, skany - wskazują na to, że raczej tam mają miejsce działania medyczne i biologiczne niż technologiczne. Oczywiście, na pewno są jakieś inne drogi (np. ewakuacyjna), ale ogólnie wszystko wskazuje na to, że tam działają raczej biologiczne badania - co się pokrywa w Containment Chambers.

Grzymość teoretycznie ma sytuację pod kontrolą. Ma ukrytą ścieżkę ewakuacyjną a Pustogor ani Orbiter nie są w stanie zrobić niczego przeciwko niemu. Ludzie w Klinice stanowią żywą tarczę a Grzymość jest w stanie spokojnie przeprowadzić operację ewakuacji lub wsparcia - a Pustogor nie do końca ma jak mu przeszkodzić:

* jeśli zaatakują, mogą być ofiary cywilne
* nie wiedzą gdzie Grzymość transportuje osoby i materiał

Tyle, że Grzymość dostaje wsparcie. Nie musi się ewakuować. Zamiast tego, fortyfikuje to jako swoją główną bazę...

Pięknotka ma inny plan - doprowadza do tego, że wyciekła informacja o tym, że Orbiter wie o obecności Entropika. Detektory Jastrząbca są wystarczające. I Orbiter CHCE zdobyć Entropika. Grzymość dostał tą informację i się nie ucieszył... poinformował swoich Sojuszników o tym, że potrzebuje pozbyć się Entropika. Oni przysłali wsparcie i Grzymość rozpoczął ewakuację.

Dwa dni później, w nocy, poza terenem zamieszkanym starli się ze sobą Kirasjerzy i tajemniczy sojusznicy Grzymościa. Nie wiadomo, jakie były dalsze losy Entropika i tego starcia - w chwili obecnej jest to nierozstrzygnięte. Ale Kirasjerzy spotkali się z czymś, czego się naprawdę nie spodziewali. Albo elitarne siły Aurum albo inna frakcja Orbitera.

A Grzymość ma swoją bazę, niestety - dla niego - Pustogor wie teraz gdzie ona się znajduje...

Tor: 

* E1: koszt (3): ewakuacja części pacjentów
* E1: koszt (3): ewakuacja najdroższego sprzętu
* E2: koszt (4): ewakuacja całości pacjentów
* E2: koszt (4): ewakuacja reszty sprzętu
* E3: koszt (5): ewakuacja Entropika
* Z1: koszt (3): zniszczenie części dowodów
* Z2: koszt (3): zniszczenie dowodów
* Z3: koszt (4): zniszczenie śladów
* Z4: koszt (5): destrukcja kompleksu Itaran
* K1: koszt (3): przejęcie Kerainy
* K2: koszt (3): wymiana Kerainy
* D1: koszt (3): dewastacja sił Jastrzębca
* D2: koszt (4): dewastacja Jastrzębca
* O1: koszt (3): obrócenie opinii publicznej za Grzymościem
* O2: koszt (3): obrócenie opinii publicznej przeciw Pustogorowi

**Sprawdzenie Torów** ():

* .

**Epilog**:

* .

## Streszczenie

Orbiter szukał informacji o zaginionym dawno Entropiku; Ataienne go wykryła gdy przejęła Jastrząbiec. Pięknotka wraz z miragentem znalazła owego Entropika - jest w ukrytym kompleksie medyczno-badawczym Grzymościa. By nie eskalować i nie ryzykować ludzkich żyć, doprowadziła do ewakuacji Entropika. Dzięki temu Orbiter musiał poradzić sobie sam. A Pustogor stanął przed tym, że Grzymość ma potężnych sojuszników i potężną bazę w Jastrząbcu.

## Progresja

* .

### Frakcji

* .

## Zasługi

* Pięknotka Diakon: stanęła przed niemożliwą sytuacją - Orbiter czy Karla. Tępić Grzymościa czy zdrowie ludzi. A wszystko dlatego, bo zniknął jeden Entropik.
* Keraina d'Orion: miragent z customowym TAI. Pięknotka jej nie ufa. Szuka zaginionego Entropika z ramienia Kirasjerów Orbitera.
* Persefona d'Jastrząbiec: TAI skupiona na ochronie i wysokim poziomie, która czasem gubi szczegóły. Acz nie powinna aż tak ich gubić jak tym razem.
* Mariusz Trzewń: pomógł Pięknotce zlokalizować kompleks Itaran używając matematyki, triangulacji i znajomości z odpowiednimi magami. Niestety, przez to był wyciek.
* Minerwa Metalia: zdalna konsultantka Pięknotki odnośnie różnic między Persefoną i Hestią. Potrafi, ku osłupieniu Pięknotki, zniszczyć AI zdalnie.
* Roland Grzymość: okazało się, że ma dużo większe wsparcie - albo z Aurum albo z Orbitera. Skupiony na tematach medycznych; gdzieś tu coś go boli.
* Hestia d'Itaran: zbudowała kilka dywersji, odciągnęła uwagę Pięknotki (na moment), nastawiła opinię publiczną za Grzymościem i przeciw Pustogorowi.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Jastrzębski
                            1. Jastrząbiec, okolice:
                                1. Containment Chambers: w jednym z nich Hestia d'Itaran zrobiła dywersję na Pięknotkę.
                                1. Klinika Iglica: front dla Itaran. Świetni lekarze, dobra klasa, czerpie energię z Jastrząbca.
                                    1. Kompleks Itaran: kontrolowany przez Grzymościa, czerpie energię z Jastrząbca; był tam tajemniczy Entropik. Teraz - jawny dla Pustogoru.

## Czas

* Opóźnienie: 8
* Dni: 2

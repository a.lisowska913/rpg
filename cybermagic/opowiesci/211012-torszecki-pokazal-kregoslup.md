---
layout: cybermagic-konspekt
title: "Torszecki pokazał kręgosłup"
threads: rekiny-a-akademia, amelia-i-ernest
gm: żółw
players: anadia, kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [210928 - Wysadzony żywy ścigacz](210928-wysadzony-zywy-scigacz)

### Chronologiczna

* [210928 - Wysadzony żywy ścigacz](210928-wysadzony-zywy-scigacz)

### Plan sesji
#### Ważne postacie

* .

#### Co się wydarzyło

.

#### Sukces graczy

* .

### Sesja właściwa
#### Scena Zero - impl

.

#### Scena Właściwa - impl

Lucjan skontaktował się z Marysią. Musi porozmawiać. "Marysiu, ale okazja!". Nataniel Morlan. On myśli, że Jola sprawiła, że jego córka została mu zabrana. Ale to nie była jego córka! To był ktoś inny! Ale Morlan tego nie wie!

Gdzie był robiony zabieg transfuzji i zmiany Wzoru? W Pustogorze! Niedaleko Ciebie!

Musisz tylko zdobyć pierwotny Wzór - ten, z którego przekształcano "córkę Morlana". By móc na podstawie próbek krwi udowodnić, że to nie była ona. Że ta dziewczyna nie jest jego córką. To sprawi, że Morlan się odpieprzy od Joli... a przynajmniej wszyscy mają taką nadzieję.

"Skąd to wiesz?" - "To nie jest takie ważne": Lucjan mistrz uników XD.

Marysia żąda info od Lucjana skąd on to wie. By z tego nie było większego bagna. By Lucjan jej w to nie wrobił. A Marysia jest przekonana, że to pułapka na nią.

TrZ+2:

* Xz: sprawa jest INTERESUJĄCA na kilku poziomach rodu. Innymi słowy, jej sukces jest zauważany - porażka i odmówienie zrobienia też.
* (+3Vg): Vg: pomysł pochodzi od Amelii. Ale Jolanta się ucieszyła. Po raz pierwszy obie rywalki polityczne są tego samego zdania - nikt nie chce wojny z Morlanem. To serio rozwiąże pewien problem na linii Eternia - Aurum.
* X: jest to najlogiczniejsze rozwiązanie. Najprostsze. Najfajniejsze. Możni rodu Sowińskich chcą, by KTOŚ to zrobił (najlepiej Marysia bo pokaże swą klasę).
* V: Aurum w przeszłości czasem wykorzystywało rekordy do swoich prywatnych tematów. To sprawiało, że Pustogor - a ZWŁASZCZA szpital - nie chcą udostępniać danych pacjentów. Nikomu spoza rodziny. A nawet wtedy niekoniecznie. A poprzedni dyrektor szpitala - Lucjusz Blakenbauer - zasealował tą pacjentkę.

Marysia spytała Lucjana jakim cudem Amelia nie mogąca się kontaktować... się skontaktowała? Lucjan powiedział, że jest jeden telefon czy coś takiego. TELEFON. Audio i w ogóle. Czysto niemagiczny. A to jest mu powiedziane przez osoby wyżej.

Czyli oprócz problemów z wysadzonym ścigaczem, Marysia ma też problem z rekordami "córki Morlana"...

TYMCZASEM KAROLINA. Podbija do lecznicy gdzie pod opieką Sensacjusza leży biedny Torszecki (przez Marysię). Sensacjusz przyjął Karolinę podejrzliwie. "Mam nadzieję, że nie przychodzisz z rozkazami od tien Sowińskiej?" Karo się żachnęła - nie jest posłańcem. Sensacjusz wątpi. Karo urażona.

Karo podbija do Torszeckiego; Sensacjusz ich zostawia samych. Torszecki jak zawsze żałosny mops. Karo - "nie zachowuj się jak wycior! Chłopie! Mógłbyś być mniej wazeliniasty? Jak chcesz być doceniony...".

W dyskusji Karo opierniczyła Torszeckiego, ten odbijał argumenty ale kiepsko. Przyznał się, że obserwuje i śledzi Marysię jak tylko jest w stanie. Próbuje przewidzieć jej ruchy, bo jak przewidzi źle, to zaraz kończy jako zakładnik Serafiny czy coś. Jemu zależy. I teraz przekonuje i będzie przekonywał Marysię, że to MAFIA a nie Rekin bo dzięki temu eternianin i mafia mogą się porozwalać a Marysia wróci na szczyt do największego apartamentu.

Karo widzi, że Torszecki ma podejście typowe Aurum - "magowie, potem ludzie, potem nic, potem ulubiony buldog, potem inne byty". Czyli ma GDZIEŚ innych nie-ludzi.

Karolina -> niech Torszecki przestanie być wyciorem do dupy. Niech wyrośnie mu kręgosłup, choć jeden krąg. Niech traktuje Marysię troszkę bardziej jak partner (podwładny) a nie przyliz. Proaktywny.

Tr (Karo jest traktowana inaczej, może Torszecki też może) Z+2:

* X: Torszecki odnalazł w sobie asertywność. Będzie po swojemu szukał i działał.
* Vz: Torszecki będzie proaktywnie działał by pomóc Marysi, też pytając.
* Xz: Torszecki widzi straszne konsekwencje dla Rekina który to zrobił i po prostu nie chce oddawać go Marysi i Karolinie. Więc żąda gwarancji niewydania. "Jak długo dostanie wpierdol".
* (+3Vg) Karo przypomniała Torszeckiemu o tym, że Marysia ma się opiekować eternianinem. I Torszecki MUSI jej w tym pomóc. Bo eternianin zrobi coś głupiego.
* Vg: bardzo niekomfortowy Torszecki. On WIE kto to zrobił. Marysia będzie niezadowolona jak się dowie. Karo: "kto to zrobił, zabił żywą istotę". Torszecki: "najpewniej nie wiedział. Plus, nie z perspektywy prawa." Karo: "umarł mi na rękach.". Torszecki NIE BĘDZIE PRZESZKADZAŁ, ale nie pomoże.
* V: Karo "nie musisz mi mówić. I tak się dowiem. Oberwę, Marysia będzie mieć kłopoty...". Torszecki - "jeśli ja nie będę sabotował, to faktycznie możesz się dowiedzieć". Torszecki: "niech Ci będzie... ja wysadziłem ten ścigacz."

Karolina (V) się opanowała i go nie uderzyła. Nie zaatakowała. Karolina: "pojebało Cię?!". Karolina opieprza Torszeckiego - nie spróbował poznać, zrozumieć, nic. Praktycznie chce wyjść. Gardzi Torszeckim. Torszecki powiedział Karolinie, że to on, bo wie jak zamaskował ślady. I nie chce, by Karolina wpakowała się w kłopoty. Bo zdaniem Torszeckiego Marysia sobie poradzi - kogoś wyśle przodem, ktoś dostanie nożem, ktoś skończy w szpitalu... bo to nie ona. Łatwo wysłać KOGOŚ. I Torszecki po prostu nie chce by tą osobą była Karolina. Bo jego zdaniem Karo na to nie zasłużyła. "Myśl o mnie co chcesz, przywykłem. Ustaliliśmy, że nie wydacie mnie eternianinowi. I tyle."

Torszecki wyjaśnił o co chodzi - tien Namertel rozkochał w sobie używając Esuriit Amelię. Złamał jedną czarodziejkę Sowińskich. A teraz Namertel przybył do Marysi i zaczyna korupcję Marysi. Torszecki MUSIAŁ ochronić Marysię (bo to powyżej pochodzi m.in. od innych Sowińskich). Więc Torszecki opracował sposób - wysadzić ścigacz, wrobić w to mafię, Marysia robi mediację i znowu wszystko wraca do normy a Torszecki może monitorować. Cała ta akcja wynikała z LOJALNOŚCI Torszeckiego. I tak, Torszecki ma kręgosłup.

Karolina zostawiła Torszeckiego. Poszła pić. Na umór. A rano - do Marysi. Na kacu. Ból pomaga.

Marysia powiedziała Karo odnośnie problemu ze szpitalem. Karo przyjęła to z głębokim brakiem entuzjazmu i zainteresowania, co Marysię zdziwiło. Karo wyjaśniła Marysi ze smutkiem, że problem Marysi jest wielopoziomowy:

* Traktujesz Torszeckiego jak wycior do dupy. Ok, tak się zachowuje.
* Torszecki jest ci wierny i jemu na Tobie zależy.
* (brzmi jak wyznanie miłości - Marysia)
* Ktoś z Twojego rodu powiedział Torszeckiemu, że Ernest rozkocha Ciebie w sobie (tak jak Amelię) używając Esuriit.
* Torszecki chciał usunąć problem... i wysadził ścigacz. By wyszło na to że to mafia, były negocjacje mafia - Ernest i zasadniczo byś wyszła z tego lepiej.

Panienka Sowińska wstaje, drze się i zaczyna rzucać wiązankami. Jest MAKSYMALNIE wściekła.

* M: "Zrobiłaś mu coś?"
* K: "Nie"
* M: <patrzy się z uznaniem>

Nie zmienia to faktu, że nie można powiedzieć o tym Ernestowi bo ten zabije Torszeckiego. To kaskadowo zrobi katastrofę z rodami. A Marysia i Karolina się PERSONALNIE zobowiązały że one znajdą winnego i oddadzą go w ręce sprawiedliwości... a Torszecki przygotowuje się na zabójców XD.

Marysia poważnie myśli nad zabójcami. A Karolina - "czemu traktujesz go jak ostatnie gówno, on naprawdę się stara". Marysia patrzy na Karolinę znacząco.

* Marysia: "czy ja mu KAZAŁAM zasłonić Cię przed Ernestem?". No w sumie nie, ale pomogło.
* Karolina: "Serafinę też ciężko zniósł".
* Marysia: "masz Torszeckiego. Otwierasz lodówkę, tam Torszecki. 'Co pomóc panienko Sowińska?' No i wiesz, przydupasi dla korzyści."
* Karolina: "Nie ma znaczenia, jest ci wierny."
* Marysia: "Kazałabym mu wysadzić ścigacz? Mówię mu czego chcę. Jak czegoś nie chcę to mu NIE MÓWIĘ!"
* Karolina: "Czasem musisz powiedzieć mu nic nie rób"

...

* Karo: "Ty kiedyś za niego wyjdziesz."
* Marysia: "Weź kurczę nie pij tyle!"

...

Dobra. Coś TRZEBA zrobić. Obie uznały, że to co Torszecki zrobił jest złe i głupie. Czy Karolina uważa, że Ernest MOŻE Marysię rozkochać? Może... ale chyba nie próbuje. A jak Karo czuje się z tym, że TORSZECKI jest mordercą? Odpowiedź: "gdzie ta wódka...".

Mamy problem trójkąta: | Torszecki | Ernest | moralność |- wybierz dwa.

* Torszecki + Ernest -> poświęcamy kogoś innego i oszukujemy Ernesta
* Torszecki + moralność -> wybieramy jawnie Torszeckiego i odrzucamy Ernesta
* Ernest + moralność -> tracimy Torszeckiego

Z przyczyn politycznych: nie można pozwolić by Ernest ZABIŁ Torszeckiego... a nikt w Aurum nie uzna, że Torszecki powinien siedzieć choć kilka dni w więzieniu. Różnice kultur.

## Streszczenie

Marysia dostała nową "prośbę" ze strony Dworu Sowińskich - pozyskać próbkę krwi "córki Morlana" z Pustogoru. Tymczasem Karolina poszła do Torszeckiego (do lecznicy) by ten wreszcie miał kręgosłup. Wymusiła na nim pomoc w znalezieniu mordercy ścigacza. Torszecki nie chcąc krzywdy Karoliny się jej przyznał - to on. Chciał zrobić Ernest vs Mafia by chronić Marysię; skrzyżował w głowie, że Ernest x Amelia to Esuriit, więc to samo spotka biedną Marysię... Karo i Marysia mają teraz problem - jak to rozplątać?

## Progresja

* Rafał Torszecki: naprawdę szczerze LUBI Karolinę Terienak; by ją osłonić przed krzywdą przyznał jej się, że to on zniszczył ścigacz. Ona nie odwzajemnia przyjaźni.
* Rafał Torszecki: nie ufa Marysi Sowińskiej. Jest jej ultralojalny, ale nie ufa jej. Planuje za jej plecami; dostał info z Rodu Sowińskich o Erneście i skrzyżował fakty Amelia x Ernest i dlatego chciał Marysię wesprzeć i ją chronić... i poszła katastrofa.

### Frakcji

* .

## Zasługi

* Marysia Sowińska: dostała nowe zadanie od Dworu Sowińskich - zdobyć krew "córki Morlana" ze szpitala Pustogorskiego. Dotarło do niej, że jej traktowanie Torszeckiego częściowo spowodowało tą katastrofę ze zniszczeniem ścigacza Ernesta.
* Karolina Terienak: poszła do Torszeckiego by ten przestał być dupą wołową i się ośmielił; wydobyła od niego, że to ON stoi za zniszczeniem ścigacza. Poszła z tym potem do Marysi... to skomplikowana sprawa. Wygarnęła Marysi, że traktowanie Torszeckiego częściowo do tego doprowadziło.
* Lucjan Sowiński: spokesman Dworu Sowińskich, przekazał Marysi nowe zadanie - zdobyć krew "córki Morlana". Nie wie jak to ruszyć.
* Sensacjusz Diakon: chroni Torszeckiego przed Karoliną; uważa ją (nieuczciwie) za posłańca Marysi Sowińskiej.
* Rafał Torszecki: leżąc w szpitalu przyznał się Karolinie że to on zniszczył ścigacz. Nie chce, by Karolina ucierpiała z ręki Marysi tak jak on. Zrobił to by chronić Marysię przed miłością wywołaną przez Esuriit z ręki Ernesta. Powiązał (błędnie) fakty Ernest x Amelia za "Marysię czeka ten sam los".
* Amelia Sowińska: zaplanowała by pokazać Morlanowi krew "córki Morlana" by udowodnić, że to dobry pomysł. Przekonała Dwór Sowińskich.
* Jolanta Sowińska: o dziwo, zaakceptowała plan Amelii Sowińskiej by pokazać Morlanowi krew "córki Morlana". Ten plan po prostu ma sens.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Podwiert
                                1. Dzielnica Luksusu Rekinów
                                    1. Serce Luksusu
                                        1. Lecznica Rannej Rybki: Sensacjusz opiekuje się Danielem; tam Karolina poznała od Torszeckiego prawdę.
                                        1. Apartamentowce Elity: Marysia i Karolina konspirują jak uratować Torszeckiego i Ernesta...

## Czas

* Opóźnienie: 2
* Dni: 1

## Konflikty

* 1 - Marysia żąda info od Lucjana skąd on ma informacje o Pustogor x "córka Morlana". By nikt jej nie wrobił.
    * TrZ+2
    * Xz: sprawa jest INTERESUJĄCA na kilku poziomach rodu; sukces i porażki są zauważane.
    * (+3Vg) VgXV: pomysł pochodzi od Amelii ale Jolancie też pasuje; możni rodu Sowińskich chcą by to się stało; Lucjusz Blakenbauer zasealował tą pacjentkę i jej rekordy a Aurum kiedyś wykorzystywało rekordy w swoich celach.
* 2 - Karolina -> niech Torszecki przestanie być wyciorem do dupy. Niech wyrośnie mu kręgosłup, choć jeden krąg.
    * Tr (Karo jest traktowana inaczej, może Torszecki też może) Z+2
    * XVz: Torszecki znalazł asertywność i będzie proaktywny
    * (->Ex) Xz: zobaczył konsekwencje dla Rekina który to zrobił i żąda gwarancji niewydania.
    * (+3Vg) VgV: Torszecki WIE kto to zrobił i nie będzie sabotował. Nie chce ryzykować zdrowiem Karoliny, przyznał że to on.

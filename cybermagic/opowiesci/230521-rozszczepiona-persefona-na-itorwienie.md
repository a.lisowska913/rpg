---
layout: cybermagic-konspekt
title: "Rozszczepiona Persefona na Itorwienie"
threads: historia-klaudii, historia-martyna, grupa-wydzielona-serbinius
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [231015 - Duch na Pancernej Jaszczurce](221015-duch-na-pancernej-jaszczurce)

### Chronologiczna

* [231015 - Duch na Pancernej Jaszczurce](221015-duch-na-pancernej-jaszczurce)

## Plan sesji
### Theme & Vision & Dilemma

* PIOSENKA: 
    * "Tear down your walls", Epica
* CEL: 
    * Pokazać wady Atarienów
    * Grupa operacyjna "Serbinius"

### Co się stało i co wiemy

* Kontekst
    * .

### Co się stanie (what will happen)

* S0: 
    * SOS. TAI się zbuntowała. (ktoś nie żyje)
* S1: 
    * Fabian, Klaudia, Helmut Szczypacz wpadają na pokład. Śmiecioboty.
    * Tylko noktianin jest w zaizolowanym scutierze
    * Inni poodcinani, systemy pouszkadzane


### Sukces graczy (when you win)

* .

## Sesja - analiza

### Fiszki

.

### Scena Zero - impl

.

### Sesja Właściwa - impl

Serbinius leci z dużym tempem na zewnętrzny pierścień Astorii. Fabian puścił Wam nagranie. To spanikowany głos "Seilio! Persefona nas zabija! SOS! SOS! Persefona nas zabija! Jednostka to Itorwien, koordynaty...". Fabian -> Helmut: "słyszałeś, przygotuj odpowiednią konfigurację Lancerów. Wchodzimy ostro." Oki, ale Klaudia wie, że Persefony mają blokady, to zupełnie nie ma sensu.

Klaudia zażądała danych z systemów Orbitera. 

Itorwien to jest statek kosmiczny szukający odrzutów / śmieci, zbiera je i recykluje. Salvager. Blueprint, highlv:

1. **Pokład Główny/Komora Odzysku**: Jest to obszar, w którym zbierane są kosmiczne odpady. Za pomocą systemów magnetycznych oraz grawitacyjnych odpady są zgarniane i przekazywane do dalszej obróbki.
2. **Sekcja Maszyn**: Tutaj znajdują się główne silniki i systemy napędowe statku, a także systemy napędzające urządzenia do zbierania odpadów.
3. **Komora Przetwarzania**: Miejsce, w którym zgromadzone odpady są segregowane, kompresowane i przetwarzane na materiały wtórne lub energię.
4. **Mostek**: Tutaj kapitan i sternicy prowadzą statek i nadzorują operacje. Znajdują się tu systemy nawigacyjne, komunikacyjne oraz kontroli systemów statku.
5. **Kwatery Załogi**: Miejsca do spania i relaksu dla załogi. Mogą istnieć osobne kabiny dla różnych członków załogi lub wspólne pomieszczenie.
6. **Kantyna**: Miejsce, gdzie załoga przygotowuje i spożywa posiłki. Jest tu także miejsce na odpoczynek i rekreację.
7. **Magazyn**: Miejsce, gdzie przechowywane są zapasowe części, narzędzia, dodatkowe systemy zbierania odpadów oraz inne niezbędne materiały.
8. **Dźwig Ładunkowy**: Używany do przenoszenia skompresowanych odpadów lub przetworzonych materiałów do innych statków lub stacji.
9. **Doki Serwisowe**: Miejsce, gdzie mogą być przeprowadzane naprawy zewnętrzne, utrzymanie systemów zbierających odpady i inne prace serwisowe na zewnątrz statku.
10. **Life Support**
11. **AI Core**

Zaskoczyło Klaudię, że sygnał wyszedł poza Itorwien. Persi mogłaby go zablokować - potwierdziła, że sygnał przybył z Itorwien. Czyli... Persi zaakceptowała wysłanie sygnału który Persi mogła zablokować? Ani strategicznie, ani taktycznie ani w ogóle nie ma to sensu. Nadawcą był faktycznie oficer komunikacyjny i naukowy, dr Tadeusz Arkaladis. Czyli FAKTYCZNIE, koleś jest załogantem Itorwien.

Zanim docierają do Itorwien, Klaudia szpera w dokumentach Orbitera (używa swojego Boba).

Tr (+uprawnienia -czas +dane ustrukturyzowane) +3:

* Vr: 
    * Tadeusz Arkaladis jest ciekawą postacią - zaawansowany psychotronik i oficer komunikacyjny, dobry naukowiec w obszarach TAI ale też anomalii -> skompromitowany. Znęcał się nad studentkami ("kobieta matematyk to jak świnka morska"). Wyleciał z akademii i trafił na Itorwien. Dostał szansę za wstawiennictwem pierwszego oficera
    * kapitan Emilia Ibris (dyplomata, ale poszła za marzeniami bycia kapitanem), Karol Brinik (mag Aurum ale nie tien, marzył o kosmosie <-- on się wstawił za Tadeuszem)
    * w papierach jest NOKTIANIN na statku, advancer. Przeciętniak. Taki trochę przegryw. Iskander.
    * główny inżynier to Szarla, Orbiterka, pierwszy przydział
    * Szczepan Mlisz - przeszkody, usuwanie śmieci itp. Z planety, przekwalifikował się
    * Mojra Karstall - security, wyleciała z Orbitera za narkotyki i używki podczas akcji. Dali jej szansę. Była czysta długo.
* XXz: Klaudii skończył się czas i Fabian "nieistotne, agent Stryk, czas na konfigurację Lancera" /niecierpliwość. Klaudia przekierowała dane z Orbitera o Itorwien na swojego Boba.
    * Itorwien jest standardową jednostką produkcji Kontrolera Pierwszego. Acz ma mało "prestiżu" i nieciekawe misje, więc poszedł na drugi echelon. Niewiele osób CHCE tam pracować. Acz aktualna załoga (prawie wszyscy) jest dość zadowolona. Tadeusz Arkaladis i Mojra Karstall są nieszczęśliwi

Helmut skonfigurował Lancery: wzmocniony pancerz przedni, full seal (własna atmosfera), plasma torch (przebijanie). Klaudia -> sandbox i zaawansowana psychotronika. Fabian sobie wziął wzmocnione uzbrojenie.

* Fabian: "nie będę łączył statków śluza w śluzę. To potencjalnie niebezpieczne. Agencie Hiwasser, zrobisz spacer kosmiczny, dasz nam przejście?"
* Martyn: "nie widzę przeciwwskazań, dowódco"

Martyn buduje przejście do Itorwien jako stary, doświadczony advancer

Tr (+sprzęt +brak przeszkód) +2 -> Tp+2:

* X: Itorwien wie o jego obecności
* X: Martyn odskoczył w kosmos. Powłoka Itorwien w miejscu gdzie wylądował wybuchła zewnętrznie - pancerz aktywny. Ale to wymaga uprawnień kapitana.
* Vr: Martyn dał radę złapać się uszkodzonego mostu i nie odleciał. 
    * M: "stan servara - 80%"
    * F: "szybka reakcja" (wstrząśnięty)
    * H: "dobrze że nie poszliśmy śluza w śluzę..."

Wiadomość z Itorwien:

* "Tu Persefona d'Itorwien. Żaden pirat nie wejdzie na pokład. To nie było ostrzeżenie."
* Klaudia: "Wiemy co motywuje Persefonę"
* Fabian: "Wprowadzę moje kody. Powinienem overridować Persefonę."
* Klaudia: (podpina się do kodów i próbuje wbić się w Persefonę, zrozumieć, obczaić - ORAZ przechytrzyć. By Persi uznała autorytet.)

.

Tr (+kody dowódcze -uszkodzona Persefona -pełna paranoja Persefony +Klaudia) +3:

* V: (Klaudia ma inicjatywę) Persefona jest sfragmentowana, jej psychotronika się rozsypała, uważa, że jej załoga jest zagrożona i weszła w ostrą paranoję a piraci mają kody Orbitera
    * część subrutyn Persefony czeka na wsparcie Orbitera
    * inne subrutyny Persefony nie akceptują kodów Orbitera
    * Persi jest przekonana, że na pokładzie jest straszliwy hazard i jej uszkodzone subrutyny czytają to jako "piraci"
    * nie ma opcji, że to się stało samo
    * Klaudia "TADEUSZ coś Ty kurwa odpierdolił!!!" (na głos)
    * Persefona zaatakowała własne subrutyny? XD Ale CZEMU?

Klaudia -> Martyn: "Dasz radę znaleźć w miarę bezpieczne wejście?" Odpowiedź Martyna: "tak, pancerz aktywny już nie działa; Persi nie powinna tego używać." Klaudia: "Nie masz Persi, masz puzzle". Martyn: (whistles) masz... ciekawy problem. A ja muszę zająć się rannymi. ASAP.

Jako, że są kawałki uznające kody Orbitera - Klaudia chce sprawdzić co kontrolują. Klaudia chce nawiązać Z NIMI połączenie i zrozumieć jak działa sytuacja. I przejąć nad CZYMŚ kontrolę co się da. (+ dokładna mikroizolacja problemu (Przewaga))

* V: Klaudia dostała dostęp do kawałków Orbitera
    * może wprowadzić Persi w kolejny _internal war_
    * ma informację co się mniej więcej stało, fragmentaryczne
        * biohazard
        * Persi ma blokadę ALE biohazard. Ratować ludzi. First imperative.
        * Persi ma dodatkowe blokady, których nie powinna mieć (Tadeusz?)
        * Persi obróciła się przeciwko sobie by wyizolować wszystko co się da
            * Persi się sfragmentowała sama by móc ratować swoich ludzi
        * biohazard jest typem gazowym, WIĘC Persi wyłączyła lifesupport. Zasealowało to załogę w kabinach.
            * ale inne elementy Persi uznały to za działanie piratów, więc na pokładzie Itorwiena jest wojna domowa między Persi i Persi
    * Klaudia wie, że ta Persefona już nie istnieje. Nie da się tego naprawić. 
    * czy ktoś zginął - tak, Tadeusz został zabity przez Persefonę.
        * Klaudia ma nagranie: "Tadeusz jest przez śmieciobota wbity w ścianę a potem electrocute, electrocute..." <-- looks like Persi malice

Przy tak sfragmentowanej Persefonie Klaudia chce wysłać TERMINATE signal. Klaudię trzeba doprowadzić do AI Core i ona może to odpalić. Ale. Z drugiej strony - Persi niekoniecznie ma moce by Was zatrzymać w tym stanie. 4 lancery. Klaudia przekazuje kapitanowi to jako jedyną sensowną opcję - to TAI jest nie do uratowania, zbyt sfragmentowane. Wszelkie zapisy z czarnych skrzynek są nietknięte, ale nie da się odbudować tej Persefony. Próbuje walczyć o swoją załogę, ale zostało jej za mało synaps. Nie stałoby się to gdyby ktoś jej nie poblokował w chory sposób.

Klaudia ma dostęp do OVERRIDER. Coś, co przekierowuje kontrolę nad elementami Orbitera pod OVERRIDER. Klaudia prosi o autoryzację. Fabian udziela autoryzacji. Klaudia dostaje pudełko z USB - specjalnie skonfigurowana Malictrix, niszczyciel TAI, tym razem pod kątem override and seal.

Martyn wraca na pokład. Dostaje OVERRIDER. Ma dostać się na jednostkę i wsadzić w dowolny terminal OVERRIDER.

Tp +3 (bo tym razem Persi jest w stanie wojny ze sobą a Martyn wie czego się spodziewać):

* V: Martyn bez problemu dostaje się teraz już na pokład jednostki
* V: Martyn umieścił OVERRIDER we właściwym miejscu
* V: Martyn zbudował prawidłowe przejście kosmiczne

Zespół bez kłopotu może wejść na pokład Itorwiena.

Lancer Klaudii ma od samego początku zaawansowane systemy detekcyjne. Od razu włączyły jej się alerty o biohazardzie. Atmosfera jest skażona. OVERRIDER włączył life support (i umożliwił otwarcie wszystkich drzwi). Klaudia na interkom: "oficer naukowy Serbiniusa, Klaudia Stryk. Pozostańcie w kabinach. Dajcie znać gdzie jesteście. Teren jest zagrożony biohazardem."

* I: Tu Iskander Matorin, advancer (z noktiańskim akcentem). Jestem w sealedsuit od początku kryzysu. W swojej kabinie. Cokolwiek się nie stało, jestem niewinny, przysięgam! Będę współpracował. Nie jestem uzbrojony i nie będę stawiał oporu.
* E: Emilia Ibris, kapitan, co robisz na moim statku? Co tu się dzieje?
* Kl: Jesteś zamknięta w kajucie na swoim statku przez swoje AI. Sygnał SOS nadany przez Twojego psychotronika...
* Karol Brinik: pierwszy oficer Brinik z tej strony. 
* Fabian: tu kapitan Serbiniusa. Z uwagi na sytuację kryzysową i niesprawność Waszej TAI i zagrożenie biologiczne przejmuję kontrolę nad Waszą jednostką. Pozostańcie w kajutach aż powiem kto i gdzie ma się pojawić.
* E: Ja dowodzę tą jednostką. Tylko ja.
* Kl: Z całym szacunkiem kapitan Ibris, w tej chwili nie ma tej jednostki, nie wiem co zrobiliście ale TAI jest w rozsypce. Nie wiem czemu to zrobiliście, ale ta jednostka nie przyjmie Twoich poleceń bo nie potrafi.
* E: Itorwien, odetnij ją od komunikacji. (chwila ciszy). Itorwien, wydałam rozkaz.
* F: kapitan Ibris, teraz ja dowodzę tą jednostką. (priv) Klaudio, daj jej odpowiedni paragraf.
* K: (paragraf)
* E: nie możecie mi odebrać dowodzenia!
* Karol: kapitan Ibris, pozwól, że... technicznie, mogą. 
* Martyn (priv): paranoja. Obsesja. Biohazard.
* Klaudia: to ma związek?
* Martyn: nie mam sprawnej konfiguracji badawczej
* Helmut: ale... skonfigurowałem Cię
* Martyn: mała wizyta pancerza aktywnego rozwiązała ten problem (lekka irytacja). Klaudio, możesz Ty?
* Klaudia: spróbuję... (wszystkie dane z czujników do Martyna)

Klaudia próbuje ocenić co tu się dzieje i na kogo trzeba uważać. Kapitan jest zdecydowanie zarażona. Klaudia pyta np. o to, kto jest w sealedsuicie.

KONFLIKT: +dyskretne na priv wsparcie Iskandera, -paranoja załogi, +Klaudia zna ich dossier

Tr Z +3 (init: Klaudia) +wsparcie Fabiana i Orbitera -> Tp:

* Vz: 
    * Szarla: "ja jestem w sealedsuit, od początku byłam"
    * Iskander: "oficer Stryk, Szarla kłamie. Widziałem, jak uciekała spod prysznica przed śmieciobotem. Założyłbym, że tylko ja jestem w sealedsuit"
        * Iskander wysłał Klaudii feed ze swojego scutiera. Faktycznie, jest w suicie zanim pojawiły się problemy. Miał poważny problem z czymś co śmieciarka zeżarła i to rozmontowywał. Ale przez to NIE WIE kto i jak - tyle, że jak wszedł na pokład to zobaczył śmiecioboty goniące Szarlę. Ale powoli goniące. Herding, not trying to kill.
* Vz: 
    * Klaudia zażądała, by KAŻDY dał jej feed z suita. Większość osób próbowała "nie możemy, nie działa" - i często nie działa. Ale Klaudia widząc ich reakcje widzi, że nie tak.
    * Mojra z ochrony wysłała jej paskudny killware. Zrobiony przez Tadeusza (bo jak inaczej). Ale po to sandbox. Klaudia jest PEWNA, że oni wszyscy są zainfekowani.
        * Klaudia odesłała jej killware i unieruchomiła Mojrę. Krzyki wściekłości Mojry zostały przez Klaudię wyciszone.

Fabian i Helmut próbują przejąć kontrolę nad jednostką

KONFLIKT: -niespodziewane zachowania ludzi, +wiedza że prawie wszyscy są zarażeni, +dramatyczna przewaga sprzętu, +killware Klaudii (ten od Mojry) do wszystkich

Tp Z +3:

* V: Wszyscy poza magiem są unieczynnieni
* X: Karol dał radę uniknąć killware pozbywając się scutiera
* Vz: Fabian ZDĄŻYŁ dotrzeć do Karola wiedząc, że on jest największym zagrożeniem
* Xz: Mag ZDĄŻYŁ "jebnąć fireballem" prosto w Fabiana który tego się zupełnie nie spodziewał; jego Lancer jest uszkodzony (ale nadal sealed)
* Vz: Fabian bezceremonialnie ogłuszył Karola
    * F: "cholera, nieźle dostałem, dobrze, że mam konfig szturmową"
    * H: "udało się go ogłuszyć?"
    * F: "tak; sam się poparzył solidnie, Martyn?"
    * M: "on it."

Persefona nieczynna i załoga nieczynna. Fabian przejął kontrolę nad jednostką i zajął się nimi, a Helmut doprowadził jednostkę do działania używając starterów, "Startowa Persefona w pigułce". Klaudia zabezpiecza czarną skrzynkę i wszystkie ślady.

EPILOG:

* Tadeusz był sfrustrowany tą sytuacją. Zdecydował się dorobić sobie z przemytem.
* Wciągnął w to Mojrę z Security.
* By Persi nic nie zrobiła, Tadeusz użył swoich skilli i najpierw pokazał kapitan że da się zdjąć niektóre niepotrzebne ograniczniki i Persi działa lepiej. Przekonywał kapitan, że Persi jest "żywa".
* Kapitan za tym poszła.
* Od pewnego czasu zatrzymywali się przy miejscach gdzie mieli czyścić a tam przemytniki zostawiali narkotyki. A przynajmniej tak mówili.
* Mojra stwierdziła, że nikt nie zauważy, jak jednej / dwóch tabletek zabraknie. Ale w "kanistrze" nie było narkotyków a substrat - organic growth. I Mojra skaziła statek.
* Gdy Mojra skaziła statek, Persi nic nie mogła zrobić - nie może wysłać sygnału, nie "widzi" Skażenia, więc się rozszczepiła - jedyna droga jaka jej została.
* Archiwum O zajmie się Mojrą.

Kolega Iskander dostał od Klaudii laurkę za pomoc w uratowaniu załogi. Fabian, gdy Klaudia mu to zasugerowała, poparł rekomendację Klaudii.

## Streszczenie

Itorwien, 'pługośmieciarka' Orbitera miała problem z przemytnikami - ktoś otworzył przemycane narkotyki i cała jednostka była Skażona (lifesupport). Persefona została ograniczona przez psychotronika na pokładzie, więc nie mogła nic zrobić. Ciśnienie i uszkodzenie Persi doprowadziło do jej rozszczepienia. Serbinius uratował Itorwien, bez strat (poza winnym psychotronikiem zabitym przez rozszczepioną Persefonę).

## Progresja

* .

### Frakcji

* .

## Zasługi

* Klaudia Stryk: zbadała szybko profil załogi Itorwien. Gdy dostała killware od Mojry, skutecznie ów killware odbiła i wszystkich uwięziła w pancerzach. Jako, że Lancer Martyna był uszkodzony, przekierowała feed z jej systemów badawczych do Martyna by mógł zbadać co tu się stało.
* Martyn Hiwasser: pełni rolę advancera, wchodząc na Itorwien. Jako dobry advancer dał radę uniknąć eksplozji pancerza aktywnego zrobionego przez Persi. Doszedł do substratu narkotyków w systemie lifesupport i wprowadził OVERSEER do systemów Persefony.
* Fabian Korneliusz: niecierpliwy, ale sensowny - co prawda nie dał Klaudii dość czasu na 100% danych, ale skutecznie przejął kontrolę nad Itorwienem z pomocą Klaudii i killware. Poproszony przez Klaudię, dał pozytywną rekomendację noktiańskiemu advancerowi z Itorwiena.
* Helmut Szczypacz: combat engineer / heavy weapon specialist z Serbiniusa; konfiguruje lancery na Serbiniusie i pełni rolę inżyniera gdy Serbinius ratuje jednostki. Tym razem - pomógł Fabianowi przejąć kontrolę nad Itorwienem.
* Emilia Ibris: kapitan Itorwien; zrezygnowała z prestiżowej pracy, by pójść za marzeniami i dowodzić własnym statkiem. Zarażona, wpada w pełną paranoję (NIE ODBIERZECIE MI STATKU!) Unieruchomiona.
* Karol Brinik: pierwszy oficer Itorwien; mag Aurum (nie tien), który mógł mieć proste życie ale marzył o kosmosie. Dał szansę Tadeuszowi i Mojrze (którą zmarnowali). Zarażony, uszkodził Lancera Fabiana; skończył ogłuszony i poparzony.
* Tadeusz Arkaladis: KIA; skompromitowany (źle traktował kobiety na uczelni) acz kompetentny doktor zaawansowanych technologii i psychotroniki. Dorabiał sobie przemytem i zablokował Persefonę d'Itorwien. Zginął, gdy rozszczepiona Persi zidentyfikowała go jako zagrożenie.
* Mojra Karstall: security Itorwien; nie zakwalifikowała się do sił Orbitera przez nałogi; została czysta, ale gdy pojawiły się problemy na Itorwien stare nawyki wróciły i chciała pozyskać środki z przemytu. Skaziła Itorwien substratem do robienia niebezpiecznych narkotyków. Dostały ją siły specjalne Orbitera.
* Iskander Matorin: noktiański advancer Itorwien; nic szczególnego. Jak tylko pojawiły się problemy, zasealował się w scutierze i słuchał instrukcji rozszczepionej Persefony. Potem współpracował z Serbiniusem. Ogólnie - robił co powinien.
* OO Serbinius: korweta awaryjnego ratunku, ma własne konfiguratory Lancerów i do 6 operacyjnych agentów (Fabian, Klaudia, Martyn, Helmut i jeszcze dwóch). Tym razem - na ratunek Itorwien.
* OO Itorwien: pługośmieciarka kosmiczna, wpadła w kłopoty - dwóch przemytników w załodze doprowadziło do Skażenia substratem całego statku przez life support. Persefona się rozszczepiła przez blokady założone przez Arkaladisa by ratować załogę. Persi odpaliła jeden _charge_ pancerza aktywnego, prawie raniąc Martyna.

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria, Pierścień Zewnętrzny: obszar uważany za "pod wpływem planety Astorii", ale poza zasięgiem orbity czy studni grawitacyjnej. Tym razem ratujemy pługośmieciarkę Itorwien.

## Czas

* Opóźnienie: 17
* Dni: 2

## OTHER

**Statek "Itorwien"**

1. **Pokład Główny/Komora Odzysku**: Jest to obszar, w którym zbierane są kosmiczne odpady. Za pomocą systemów magnetycznych oraz grawitacyjnych odpady są zgarniane i przekazywane do dalszej obróbki.
2. **Sekcja Maszyn**: Tutaj znajdują się główne silniki i systemy napędowe statku, a także systemy napędzające urządzenia do zbierania odpadów.
3. **Komora Przetwarzania**: Miejsce, w którym zgromadzone odpady są segregowane, kompresowane i przetwarzane na materiały wtórne lub energię.
4. **Mostek**: Tutaj kapitan i sternicy prowadzą statek i nadzorują operacje. Znajdują się tu systemy nawigacyjne, komunikacyjne oraz kontroli systemów statku.
5. **Kwatery Załogi**: Miejsca do spania i relaksu dla załogi. Mogą istnieć osobne kabiny dla różnych członków załogi lub wspólne pomieszczenie.
6. **Kantyna**: Miejsce, gdzie załoga przygotowuje i spożywa posiłki. Jest tu także miejsce na odpoczynek i rekreację.
7. **Magazyn**: Miejsce, gdzie przechowywane są zapasowe części, narzędzia, dodatkowe systemy zbierania odpadów oraz inne niezbędne materiały.
8. **Dźwig Ładunkowy**: Używany do przenoszenia skompresowanych odpadów lub przetworzonych materiałów do innych statków lub stacji.
9. **Doki Serwisowe**: Miejsce, gdzie mogą być przeprowadzane naprawy zewnętrzne, utrzymanie systemów zbierających odpady i inne prace serwisowe na zewnątrz statku.
10. **Life Support**
11. **AI Core**

Role:

W zależności od skomplikowania zadań statku, struktury hierarchii i wielkości załogi, niektóre role na statku "Itorwien" mogą obejmować:

1. **Kapitan**: Kieruje całym statkiem, podejmuje kluczowe decyzje dotyczące trasy, misji i załogi.
    * Emilia Ibris: 
        * zrezygnowała z prestiżowej pracy jako dyplomata, by pójść za marzeniami i dowodzić własnym statkiem
2. **Pierwszy Oficer / Zastępca Kapitana**: Wsparcie dla kapitana, zastępuje go w razie potrzeby.
    * Karol Brinik: 
        * mag Aurum (nie tien), który mógł mieć proste życie ale marzył o kosmosie
        * Dał szansę Tadeuszowi i Mojrze
        * mag ognia
3. **Inżynier Główny**: Nadzoruje utrzymanie, naprawy i funkcjonowanie systemów technicznych statku.
    * Szarla Wirknit: 
        * orbiterka, która trafiła na tą jednostkę jako pierwszą, wierzy w Emilię
4. **Specjalista ds. Przeszkód / Usuwania Śmieci**: Ktoś, kto operuje sprzętem do usuwania przeszkód i śmieci, taki jak pług i systemy zasysania.
    * Szczepan Mlisz 
        * przekwalifikował się; kiedyś ze Sojuszu Letejskiego
5. **Oficer Naukowy / Technologiczny / Komunikacji**: Ktoś, kto prowadzi badania, analizy, a także dba o rozwój i utrzymanie technologii AI.
    * Tadeusz Arkaladis
        * skompromitowany acz kompetentny doktor zaawansowanych technologii
        * on wraz z SECURITY zajął się przemytem. Od niego się zaczęło.
        * on uszkodził Persefonę by nie robiła kłopotów
6. **Oficer Bezpieczeństwa**: Ktoś, kto jest odpowiedzialny za bezpieczeństwo załogi i statku, a także za obronę w razie ataku.
    * Mojra Karstall 
        * atarienka, która nie zakwalifikowała się do sił Orbitera przez nałogi
        * ona doprowadziła do Skażenia jednostki
7. **Advancer**: 
    * Iskander Matorin:
        * noktianin, średni we wszystkim

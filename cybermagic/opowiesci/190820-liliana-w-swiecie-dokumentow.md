---
layout: cybermagic-konspekt
title: "Liliana w świecie dokumentów"
threads: nemesis-pieknotki
gm: żółw
players: mila, onyks, michal_1
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [190804 - Niespodziewany wpływ Aidy](190804-niespodziewany-wplyw-aidy)

### Chronologiczna

* [190804 - Niespodziewany wpływ Aidy](190804-niespodziewany-wplyw-aidy)

## Budowa sesji

### Stan aktualny

* .

### Pytania

1. .

### Wizja

brak

### Tory

* .

Sceny:

* .

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Misja właściwa

Scena zero.

Liliana poprosiła o wsparcie Melę, Bo oraz Maksa. Chce zrobić artefakt typu "stoliczku nakryj się" na zapleczu Szkoły Magów w Zaczęstwie. Wpierw zbudowali źródło energii - ogień; ekranowany runami. Potem podpięli to do samego stoliczka i przeciążyli poziom energii. Maks szybko wzmocnił runy by nie doszło do pożaru. Sam artefakt zaczął pluć kremówkami i wypadać spod kontroli; młodzi magowie gdy rozbrzmiały alarmy w panice to teleportowali daleko. Do klubu okultystycznego (poetyckiego)...

Zaraz potem dywanik u dyrektora. No bez przesady. Liliana wzięła winę na siebie (w sumie, ona odpowiadała za prawidłowe obudowanie tego wszystkiego). Dyrektor kazał wszystkim posprzątać a Liliana ma za zadanie pomóc z dokumentami w Papierówce. Liliana wściekła jak osa, ale nie odmawia poleceń.

Dwa dni później Liliana kończy w skrzydle szpitalnym Szkoły Magów. Był tam wybuch i papiery się porozrzucały i pogubiły. Wszyscy podejrzewają, że Liliana zrobiła to specjalnie by nie siedzieć w papierach, ale to serio nie pasuje do jej modus operandi - Liliana nie jest tego typu osobą.

Zespół poszedł porozmawiać z Lilianą; ta powiedziała, że ktoś tam był. Przyszła wcześniej z zaklęciem wykrywającym i na kogoś się natknęła. Jej zaklęcie zarezonowało z zaklęciem tamtego maga, coś aportowała (uprząż dla psa???) i natychmiast deportowała. A potem tamten mag wyskoczył przez okno a ona straciła przytomność i ma popalone kanały magiczne.

Sesja właściwa.

Ogólnie, Liliana ma problem. Zespół zdecydował się jej pomóc ten problem rozwiązać. By nikt nie szkalował jej złego imienia. Poszli do dyrektora Arnulfa i poprosili go, by ten pozwolił im zadośćuczynić szkodom Liliany - oni ogólnie lubią sprzątać i martwią się o Lilianę i czują się lekko współodpowiedzialni. Dyrektor to łyknął; załatwił im możliwość rozmowy i zajęcia się tematem z Adelą.

Adela Pieczar potraktowała ich chłodno - do momentu, jak Maks użył swoich umiejętności by ponaprawiać jej roboty (silnie zmechanizowana Papierówka). Tam dotarł też do pierwszych śladów wskazujących na to, że Liliana nie była tam sama - są ślady innego infiltratora; historia Liliany się potwierdza. Znalazł też nieprawdziwe rekordy - firma Błyskowąż ma szeroko edytowane i podmieniane przez infiltratora rekordy. Jest to samo w sobie niepokojące; Błyskowąż to firma spedycyjna, przesyłająca artefakty i anomalnie. Ostatnio zlecenie zostało dopisane - na uprząż dla psa dla pewnej arystokratki w Aurum. Hm.

Poszli dalej się rozeznać w sytuacji, szukać czegokolwiek co pasuje - i znaleźli kogoś dziwnego. Wspomaganego przez magię o zapachu Liliany (!) okultystę, który krzyczał, że koniec świata już blisko w alejce. I faktycznie, ktoś go słuchał. Zespół zabrał go do Sucharka (w końcu stamtąd delikwent przyszedł) i wezwali Lilianę; w końcu już może przybyć. Przesłuchali delikwenta i wyszło na to, że on właśnie tu, w Sucharku, znalazł uprząż dla psa i ta uprząż Pokazała Mu Prawdę. Pomogła mu zauważać i wzmocniła jego umiejętności.

Hipoteza, że Liliana coś aportowała przy interakcji z tamtym magiem się potwierdza. Czyli Błyskowąż jest zamieszany. A czemu trafiło tu? Bo emocje i poprzednia lokalizacja Stoliczka.

Liliana nie może czarować; Maks ją wzmocnił i umożliwił jej czarowanie (paląc jej kanały jeszcze bardziej, ale pies to lizał, Liliana i tak chce być heroiczna).

Dopytany co zrobił z uprzężą, okultysta powiedział - przekazał ją młodemu uczniakowi którego biją starsi chłopcy. Z cyberszkoły. Super.

W Cyberszkole poszli do dyrektora Kruszawieckiego; ten zgodził się, by magowie znaleźli ucznia i mu pomogli. Połączone z Lilianą zaklęcie umożliwiło znalezienie który to chłopak. Zaprosili go i ku wielkiemu rozbawieniu Zespołu chłopaczek kłócił się z Lilianą (która tą kłótnię brała zbyt poważnie). Zespół wydobył od dzieciaka, że artefakt odpełzł i schował się w komputerze. Super. Trzeba ewakuować tą salę (dyrektor się zgodził) i rozwiązać artefakt.

Na to wszedł Szymon. Chciał wygonić młodych magów, ale oni się nie dali. Szymon nie miał wyboru; po przedstawieniu mu tego co młodzi już odkryli musiał choć trochę współpracować. Wyjaśnił, że Tiamenat pracował nad bezpieczną formą mimika symbiotycznego który będzie kompatybilny ze zwierzętami - i się udało. Chciał to przesłać dalej, ale ingerencja Liliany sprawiła, że mimik zwiał. A nie może zrobić tego w pełni jawnie, bo nie ma wszystkich papierów a przetestować to trzeba natychmiast. Tak, będzie współpracował z magami by unieszkodliwić mimika. (zespół to nagrywał i wysłał terminusowi Tymonowi Gruboszowi)

Liliana została najsmaczniejszą przynętą katalityczno/mentalnie. Przygotowali też pułapkę na mimika i udało im się go schwytać; acz mimik Skaził Lilianę (cięższe zejście). Zespół chcąc kupić czas Tymonowi (który jechał najwolniej jak to tylko możliwe!) sprawił, że Liliana zrobiła najbardziej skomplikowaną i wolną iluzję katalityczno-iluzyjną by się "odpiąć" od mimika. Oddała go Szymonowi który odjechał z Tymonem na ogonie.

Liliana skończy na tydzień w szpitalu, ale Tymon nie może udawać że nic nie widzi. Musi to rozwiązać.

O co chodziło? Tiamenat wyprodukował eksperymentalnego, pomocnego mimika. Chcieli przesłać go do Trzeciego Raju gdzie by się przydał, ale ingerencja Liliany sprawiła, że mimik zniknął. A Tymon próbował rozpaczliwie niczego nie zauważyć, lecz dzięki działaniu Zespołu (magów ze Szkoły Magii) musiał rozwiązać problem. Ech, te dzieci.

**Sprawdzenie Torów** ():

* .

**Epilog**:

* Liliana na tydzień do szpitala. Przepalone kanały magiczne i ciężkie cierpienie.
* Uczniowie Szkoły Magów są traktowani jako bardziej kompetentni i niebezpieczni niż "losowi niegroźni studenci"
* Dyrektor Poważny jest traktowany bardziej poważnie; zwłaszcza przez Papierówkę.
* Tymon musiał zająć się noktiańską firmą spedycyjną w służbie Kajrata, choć BARDZO nie chciał.

## Streszczenie

Tiamenat wyprodukował eksperymentalnego, pomocnego mimika. Chcieli przesłać go do Trzeciego Raju gdzie by się przydał, ale ingerencja Liliany sprawiła, że mimik zniknął. A Tymon próbował rozpaczliwie niczego nie zauważyć, lecz dzięki działaniu Zespołu (magów ze Szkoły Magii) musiał rozwiązać problem. Ech, te dzieci. Ale Liliana odzyskała dobre imię.

## Progresja

* .

### Frakcji

* .

## Zasługi

* Liliana Bankierz: nadmiar bohaterstwa i żądzy przygód. Zaczęła od zrobienia artefaktu, potem wpakowała się w linię spedycyjną Noctis - Trzeci Raj i skończyła jako przynęta na mimika. Tydzień w szpitalu.
* Arnulf Poważny: dyrektor siwiejący przez beztroskie działania Liliany. Próbuje zapewnić by interesy wszystkich były spełnione - co rzadko wychodzi tak jak by chciał.
* Adela Pieczar: dokładna i surowa mistrzyni Papierówki; jak Liliana ją denerwuje tak do Maksa ma słabość (bo pomógł jej naprawić roboty).
* Tadeusz Kruszawiecki: dość ufny dyrektor, który z przyjemnością pomagał magom w neutralizacji mimika symbiotycznego jak tylko mógł.
* Tymon Grubosz: terminus, który BARDZO próbował nie zauważyć noktiańskiej firmy Kajrata pomagającej Trzeciemu Rajowi. Niestety, uczniowie szkoły magów nie dali mu tej szansy.
* Szymon Jaszczurzec: mag firmy spedycyjnej Błyskowąż powiązany z Kajratem; krzyżowo zaskoczył się z Lilianą i stracił spacyfikowanego mimika. Skończył aresztowany przez Tymona.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Zaczęstwo
                                1. Akademia Magii, kampus 
                                    1. Budynek Centralny
                                        1. Skrzydło Loris: nieużywane; na zapleczu Liliana z Zespołem składali artefakt, który wypadł poza kontrolę Zespołu.
                                1. Klub Poetycki Sucharek: tam wpierw teleportował się Stoliczek a potem mimik symbiotyczny...
                                1. Papierówka: miejsce wszystkich ważnych dokumentów, które było cierpliwie infiltrowane przez noktian by podkładać fałszywe informacje. Liliana wysadziła pokój.
                                1. Cyberszkoła: miejsce ostatecznego starcia spacyfikowanego mimika symbiotycznego, Zespołu i Liliany.

## Czas

* Opóźnienie: 2
* Dni: 4

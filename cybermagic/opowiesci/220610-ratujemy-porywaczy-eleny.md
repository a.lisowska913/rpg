---
layout: cybermagic-konspekt
title: "Ratujemy porywaczy Eleny"
threads: legenda-arianny
gm: żółw
players: fox, kapsel, kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [220330 - Etaur i przynęta na Kryptę](220330-etaur-i-przyneta-na-krypte)

### Chronologiczna

* [220330 - Etaur i przynęta na Kryptę](220330-etaur-i-przyneta-na-krypte)

## Plan sesji
### Co się wydarzyło

* porwali Martyna i Elenę z Atropos
* Martyn się spodziewał czegoś dziwnego, więc na amnestykach zaprojektował plan jak ich uratować (acz nie przewidział wszystkiego)
* Elena jest w zwykłym zaawansowanym containment vat a nie w dedykowanym - więc się Obudzi

### Co się stanie

* Elena się przebudzi i doszczętnie zniszczy statek Syndykatu Aureliona.

### Sukces graczy

* odzyskanie Martyna i Eleny jest pewne
* dojście do tego co się dzieje na Atropos
* uratowanie statku porywaczy przed Spustoszeniem Strain Elena

## Sesja właściwa
### Scena Zero - impl

90 dni temu.

Elena się uaktywnia, zdezorientowana (przed chwilą użyła swoich mocy). Jest na statku Świetlik Mroku należącym do Aurum, ale nie pamięta tego. Uratowała technika z Inferni, wpompowując w niego Spustoszenie i utrzymując przy życiu, ale utraciła kontrolę i rozsypała jej się pamięć.

Czterech agentów Aurum ze Świetlika podchodzi do Eleny (skonfundowanej). Jeden jest medykiem...

TpZ+3+5Or. 

* V: Elena ich rozbroiła. Zasymilowała broń.

Elena ich rozbraja jednym uderzeniem, robiąc piruet i przechwytując i asymilując ich broń. Oni się odsuwają - WIDAĆ, że jest poważny problem. Elena chce, by się zidentyfikowali. Kim są. Elena nie pamięta kim jest i co się dzieje, więc chce jakichkolwiek informacji.

PROMOCJA -> TR. Elena chce "zidentyfikuj się". Elena mówi komunikat "zidentyfikuj się, podaj jednostkę, stopień, przydział" - jakby rozmawiali z maszyną

* Vz: Głos osoby znanej. Elena: "Test logiczny niepoprawny". Głos "pozwól im odejść.". Uratowana osoba to technik z Inferni..? Ale skąd tu? 
* V: Elena zasymilowała komputer i komunikator jednego z nieszczęśników w skafandrze. Widzi, że coś jest BARDZO nie tak.

Koleś w skafandrze jest nietknięty, ale płacze i posikał się z paniki. Elena Spustoszyła jego skafander, przejęła kontrolę nad wszystkimi subsystemami skafandra. Komunikuje się z mostkiem. Z kimkolwiek, dalej udając że jest maszyną. Kolizja Ixion/Esuriit sprawia, że NIE PAMIĘTA, nie słyszy, nie rozumie. Flashbacki. Sygnały.

Elena chce pobrać energię by pokonać chaos. Wyssać energię ze wszystkich skafandrów.

* Vz: odzyskanie komunikacji

Pozostałe 4 servary straciły energię i się wyłączyły. Macka wyssała energię. Elena... odzyskała percepcję. Nadal nie wszystko pamięta, ale widzi co się dzieje. Szczęśliwie, jest SOBĄ, nie chce robić nikomu krzywdy ani niczego zniszczyć.

Arianna wysłała Otto do Eleny, jak najszybciej. Elena zrozumiała co się dzieje i co się stało. Elena wyłączyła Spustoszenie i próbuje UCIEC. Arianna i wszyscy jej pozwalają. Arianna przewidziała GDZIE Elena zwieje - zwiała do jednej z rur naprawczych - ciemno, wilgotno i ciasno. I siedzi tam. Tak jak kiedyś w jaskini, ale tym razem nikogo nie zabiła.

* Arianna: "Jestem Twoją kuzynką. Pamiętasz? Biegałyśmy razem po polach Verlenlandu".  
* Elena: "Czym jest kuzynka?" (zmęczony głos)

Arianna próbuje przekonać Elenę, by ta jej zaufała, by poszła z Arianną.

TrZ+3+5Or: 

* V: Elena zaakceptowała że jesteś jej sojuszniczką. Poszły "do domu".

PO TYM WSZYSTKIM Elena została wysłana do Laboratorium Kranix, najbardziej zaawansowanego lab Orbitera. A po naprawieniu jej - do Stacji Medycznej Atropos na stabilizację, do Martyna.

JESZCZE WTEDY

Leona prosi o uprawnienia do zaawansowanej augmentacji. Bo Elena i powrót Martyna. Kramer Ariannę opieprzył, ale Termia sfinansowała operację usprawnienia Leony. Arianna się zgodziła, naturalnie.

### Scena Właściwa - impl

Półtora miesiąca. Arianna powoli zbiera swoją grupę wydzieloną Infernia:

* Infernia, dowodzi Arianna / Eustachy / Diana. CEL: errr... okręt flagowy? Pozytywna anomalia?
* Tivr, dowodzi Lutus Amerin (noctis). CEL: jednostka super-szybka, manewrowna.
* Wścibski Wiewiór, dowodzi kapitan Ola Szerszeń (ruda). CEL: statek badawczo-skanujący dla Klaudii. Z nadania admirał Termii.
* Straszliwy Pająk, dowodzi <brak danych jeszcze>. CEL: support ship, fabrykator, containment Eleny, containment Nereidy, refueling.
* Obłok Apokalipsy, dowodzi <brak danych jeszcze>. CEL: jednostka Aurum, nowa, z nowymi osobami. Chcą pomóc Ariannie.

Z K1 Klaudia dostała wiadomość - z Atroposa. Od Martyna. Przekazaną przez jakiegoś weterana który przyleciał wrócić do służby na K1. Samo w sobie to nie jest coś bardzo dziwnego, choć Martyn zwykle nie jest tak niesamowicie tajemniczy. Ale jest to zakodowane starymi kodami Grzymościa. WTF.

TrZ (zna Martyna) +3:

* V: wiadomość częściowo odczytana
    * Martyn zażywa amnestyki by nie wiedzieć - coś jest nie tak, "shadow conspiracy".
    * TAI zachowuje się nietypowo i ktoś interesuje się nim i Eleną
* Xz: dużo z tego co Martyn powiedział powiedział szyfrem w rozumieniu "Klaudia będzie wiedziała o co mi chodzi", nope, nie ma pojęcia.
* Vz: ważne są: TAI, i że nie da się przesunąć tego biovatu Eleny. A inne biovaty tego typu nie utrzymają Eleny. I on się amnestykuje z tej wiedzy na wszelki wypadek. Szukaj po jego biosygnale.

Klaudia -> Arianna, na Tivrze. Informacja o tym, że Martyn się martwi i zostawił biosygnały. Ćwiczenia. Diana (Infernia) zaproponowała, że będzie mogła robić straszne rzeczy. Eustachy - strzelaj do kosmicznego złomu w tych sektorach. Diana nie jest w stanie wyrządzić tam krzywdy.

Atropos. Przyjęli wizytę Zespołu ze zdziwieniem. Arianna, Eustachy, Klaudia, Raoul. TAI pyta "czy ma do czynienia z Admirałem". wtf. Czyli faktycznie, coś jest nie tak z tą TAI. Ale ogólnie wszystko wygląda normalnie na stacji medycznej Atropos.

Martyn na wejściu. Padli sobie Klaudii w ramiona jak starzy przyjaciele którymi są.

Klaudia ma detektor próbek Martyna. Miała czas przygotować. Więc próbuje znaleźć ślady Martyna - co jej zostawił.

TrZ+2:

* XX: działa... ale nie umie pinpointować próbek. Srs... ten jeden raz powinno działać a nie działa.
* Vz: działa. Pokazuje lokację Martyna i kawałków ciała Martyna. ALE MARTYN NIE JEST MARTYNEM - detektor go nie wykrywa. Gorzej, zgodnie z detektorem Martyna nie ma stacji.

Czy to NIE jest Martyn? Miragent? Coś się z nim stało?

Klaudia chciała to zidentyfikować. Wzięła Martyna by ten ją oprowadził po stacji i przepytała co jest nie tak z TAI. Zdaniem Martyna, TAI pali banki pamięci. To są pewne... komplikacje. Ale ogólnie działa. Nie widzi problemu z TAI, nie jakiś szczególnie poważny.

Klaudia, chcąc go przebadać, poprosiła o wizytę na Wiewiórze. Martyn na Wiewiórze. Nie chce dać się skanować. Ale Wiewiór go nie wykrywa. A ani Eustachy ani Klaudia nie chcą go wpakować na siłę do detektorów - kto wie co się stanie.

TYMCZASEM na Atroposie.

Arianna + Raoul. Raoul jest zaniepokojony. Ten biovat jest... nieaktywny? Coś nie działa? Jest jedno podpięcie do Eleny jakie nie działa i to widać - a jednak biovat tego nie raportuje. Coś jest BARDZO nie tak.

NA WIEWIÓRZE - Klaudia chce wyprowadzić Martyna (miragenta) z Wiewióra.

TrZ (Martyn) +2

* X: uszkodzenie struktury
* Vz: wychodzi z Wiewióra => Atropos
* V: ..."muszę chwilkę..." i nie zrobił im tego
* X: Klaudia się nie dowiedziała nic więcej

Klaudia podrzuciła Martynowi tracker do kieszeni. Martyna na pewno nie ma na Atropos. Klaudia proponuje połączenie z Eleną przez krew.

Arianna - ping, magiczny skan, czy rezonuje

TrZM+3+3On:

* V: to nie jest Elena. TO NIE JEST ELENA. Ktoś zabrał Elenę.

Arianna przesłuchuje losowych lekarzy w bazie. Odpowiedzi. Co dostanie. GDZIE SĄ AKOLICI. Doświadczeni lekarze, od wielu lat.

TrZ (reputacja) +3:

* XX: dramatycznie opóźniają tą operację: zajęło to kilka godzin i wzbudziło halo na Atropos.
* V: lekarze powiedzieli Ariannie co ważne - akolici znknęli wczoraj. I wszystko wskazuje na to, że 14h minęło od zniknięcia Martyna i Eleny.
* Vz: problemy zaczęły się odkąd TAI pali banki danych. Parę miesięcy.
* X: Druga Strona wie, że Arianna się nimi interesuje - sprawa osobista.

Klaudia chce iść przez TAI. TAI "czy jesteś admirałem?" "TAK! Używa nieaktualnych, starych kodów Kramera":

ExZ+4:

* X: protokół samozniszczenia
* V: TAI powie co wie
    * od kilku miesięcy
    * dowódca stacji jest osobą która za tym stoi i zablokował TAI
    * DLATEGO TAI niszczyła sobie banki danych - by mogła znaleźć sposób by ostrzec
    * dwóch wartościowych badaczy porwano
    * TAI zidentyfikowała, że za tym stoi Syndykat Aureliona
    * Ta TAI "dyszy zemstą". Zależy tej Hestii na stacji Atropos.
* Vr: Klaudia uratowała TAI. Wyłączyła TAI a dokładniej moduł samozniszczenia. Jako admirał kazała modułowi selfdestruct zniszczyć się najpierw.

Eustachy przesłuchuje dowódcę stacji, kapitana Jakuba Bulgocza. On nacisnął czerwony guzik, ale TAI nie przekazała dalej. Bulgocz jest w rękach Inferni. Zgodnie z tym co TAI powiedziała, Martyn kazał zniszczyć informacje o tym, jak containować Elenę - więc ktokolwiek porwał Elenę ma na pokładzie coś morderczo niebezpiecznego. 

Arianna - odroczymy MOŻE admiralicji. Chce dowiedzieć się od Bulgocza wszystkiego co on wie. Bo inaczej napastnicy są martwi i Elena może mieć ranę mentalną.

Kapitan poddał sygnaturę jednostki "Dziecięca Radość" oraz gdzie powinna być za 2 dni. Powiedział, że za atakiem stoi Syndykat Aureliona - który próbuje wymieniać najbardziej wartościowe osoby na miragenty.

Na podstawie tego Klaudia liczy.

TrZ (lokalna TAI która chce zemsty) +3 (kapitan pomóc próbuje):

* X: Potrzebujecie pomocy kapitana by zdążyć
* V: Jeśli Tivr poleci SZYBKO to są w stanie dogonić ten statek

Były medyk i niezły administrator, kapitan zostanie medykiem zapasowym na Inferni zamiast być oddany Orbiterowi. To będzie jego "nagroda". ON JESZCZE NIE WIE.

Pilotaż Tivra. Lecimy SZYBKO. Bardzo szybko. Pozostałe statki podążają za nami. Doświadczeni noktianie. Arianna ma mówkę motywacyjną. "Your chance to shine"

TrZ+3:

* V: Tivr dogonił statek "Dziecięca Radość" zanim obudziła się Elena. 
* XX: A Klaudia wysyła sygnał "jesteście w piekielnym niebezpieczeństwie". Nie wierzą i myślą że to trik Orbitera. Przygotowują się na atak z zewnątrz.
* V: Insercja skuteczna.
    * Raoul doprowadził i otworzył, Leona wparowała jak pocisk, Eustachy za nią, Raoul się rozstawił jako snajper

Leona robi apokaliptyczny chaos z nowym sprzętem.

ExZ+4:

* X: Leona zabiła kilka osób. Zdarza się, test run nowej broni.
* Vz: Leona robi APOKALIPTYCZNY CHAOS. Nikt nie zwraca uwagi na Eustachego. I ma najlepszy humor w historii - nienawidzi niewolników.
    * oni myślą że Leona się uwolniła

Eustachy - sabotujesz fragmenty statku by wymusić ewakuację.

TrZ+4:

* Vz: Zdecydowali się ewakuować. 
* X: Elena się budzi.
* V: Wszyscy się ewakuowali i nie zabrali niewolników.
* X: Będą się bronić. To znaczy, że część zginie.

Eustachy znalazł Martyna. Zdjął mu obroże neurokontroli. I kilka obróż zwinął.

Elena się przebudziła. Ale Eustachy, Martyn i Arianna ją uspokoili - wszystko w porządku, nic złego się nie dzieje, musisz wrócić spać do Containment Chamber (ale już na Pająku - nie na Atropos)...

## Streszczenie

Elena jest już zbyt zdestabilizowana by funkcjonować niezależnie, więc wpierw zajęli się nią w Laboratorium Kranix a po zbudowaniu dla niej Containment Chamber przesłali ją do stacji medycznej Atropos do regeneracji. Stamtąd ją i Martyna porwał Syndykat Aureliona (ale Martyn przewidział JAKIŚ problem i ostrzegł Klaudię). Elena by zniszczyła okręt napastników, ale Zespół skutecznie odkrył problemy na Atropos, przesłuchali dowódcę, zneutralizowali miragenta i dotarli do okrętu Aureliona zdobywając jeńców i zatrzymując Elenę przed przebudzeniem i Spustoszeniem statku.

## Progresja

* Elena Verlen: jej pierwsze wystąpienie jako Spustoszenie Strain Elena.
* Leona Astrienko: za zgodą Arianny i z opłaty admirał Termii została wzmocniona i przebudowana na najgroźniejszego techno-cirrusa. -20 lat życia, -komfort, +siła ognia.
* Martyn Hiwasser: wrócił na Infernię - pełnosprawny, acz musi towarzyszyć mu 10 akolitów z Eterni z uwagi na niestabilny arkin.
* Jakub Bulgocz: po umowie z Arianną, przeniesiony z roli dyrektora Atropos na rolę medyka na Inferni. Kramer z uśmiechem zaakceptował transfer.
* Arianna Verlen: Syndykat Aureliona wie, że Arianna nań poluje. Syndykat nie potraktuje tego łagodnie jak coś Arianna zrobi.

### Frakcji

* .

## Zasługi

* Arianna Verlen: ustabilizowała Elenę, gdy tą pokonało jej własne Spustoszenie. Przesłuchała lekarzy i zlokalizowała kiedy zaczęły się problemy na Atropos. Doprowadziła do tego, że dyrektor Atropos został medykiem na Inferni i myśli że to lepsza opcja niż court of law.
* Eustachy Korkoran: skutecznie zsabotował statek Aureliona sprawiając, że popanikowali i zaczęli się ewakuować. Potem uwolnił Martyna i uspokoił budzącą się Elenę.
* Klaudia Stryk: odszyfrowuje dane Martyna, dochodzi do tego że to miragent, wspiera TAI Hestia d'Atropos i poznaje od niej prawdę a potem oblicza jak dotrzeć do statku niewolniczego zanim Elena się przebudzi. Absolutna MVP operacji.
* Elena Verlen: gdy ratowała agenta Inferni, Spustoszenie zniszczyło jej pamięć. Pokonała (nie zabijając) sporo agentów, zanim Arianna do niej dotarła. W Kranix ją zregenerowali i zrobili jej containment chamber. Śpi. Porwana przez Syndykat Aureliona, Inferni udało się ją odbić zanim się przebudziła.
* Martyn Hiwasser: opiekował się Eleną na Atropos i zauważył, że coś jest nie tak. Szyfrem ostrzegł Klaudię i przekonał Hestię do usunięcia info jak containować Elenę. Po czym zażywał sporo amnestyków. Porwany przez Syndykat z Eleną, nie umiał im nic powiedzieć - tylko dziesiątki potencjalnych planów (błędnych, bo "zapomniał" że Elena jest uncontainable). Eustachy zdjął mu neuroobrożę. TAK, Martyn zaplanował eksterminację z zimną krwią z Eleną jako bronią.
* Leona Astrienko: po dramatycznej augmentacji (cobra-class) miała okazję zaatakować statek niewolniczy Aureliona. Zrobiła apokalipsę. Próbowała się powstrzymać, ale część osób wyginęła - testuje nowe augmentacje.
* Raoul Lavanis: bardzo zauważliwy; zauważył, że "Elena" (miragent) jest źle podpięty do Containment Chamber. A potem w kosmosie zrobił insercję i wszedł na statek Aureliona z Leoną i Eustachym.
* Jakub Bulgocz: dyrektor stacji medycznej Atropos współpracujący z syndykatem Aureliona. Zablokował Hestię. Ale Klaudia doń dotarła i Arianna go przesłuchała. Zero kręgosłupa - współpracuje z Arianną i zamiast egzekucji / oddania Kramerowi został przeniesiony jako medyk na Infernię. On jeszcze nie wie co go czeka.
* Hestia d'Atropos: opiekuńcza TAI transcendent+1 która paliła własne banki danych by móc komuś powiedzieć że przełożony Atropos współpracuje z Syndykatem. Ryzykowała życiem, ale Klaudia ją zregenerowała. VERY vindictive towards those who hurt her charges.

### Frakcji

* Syndykat Aureliona: zrobił kanał przerzutowy i porywa niektórych cennych ludzi z Atropos i podmienia ich na miragenty. Próbował porwać Elenę jako bioweapon; ale Infernia zatrzymała ruchy Aureliona.

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria, Pierścień Zewnętrzny
                1. Laboratorium Kranix: najpotężniejsze i najlepiej chronione laboratoria Orbitera. Tam zbadali Elenę i złożyli jej Containment Chamber.
                1. Stacja Medyczna Atropos: zajmuje się trudnymi przypadkami, więc nie jest szczególnie chroniona (po co?). Syndykat Aureliona zrobił kanał przerzutowy i porywa niektórych cennych ludzi i podmienia na miragenty. Infernia weszła i zatrzymała ten proceder.
 
## Czas

* Opóźnienie: 72
* Dni: 2

## Konflikty

* 1 - Czterech agentów Aurum ze Świetlika podchodzi do Eleny (skonfundowanej). Elena ich rozbraja
    * TpZ+3+5Or
    * V: Elena ich rozbroiła. Zasymilowała broń.
* 2 - Elena chce, by się zidentyfikowali. Kim są. Elena nie pamięta kim jest i co się dzieje, więc chce jakichkolwiek informacji.
    * TrZ+3+5Or
    * VzVVz: Elena rozbroiła, Spustoszyła skafander, złapała sygnał Arianny i... trochę odzyskała siebie. 
* 3 - Arianna próbuje przekonać Elenę, by ta jej zaufała, by poszła z Arianną.
    * TrZ+3+5Or
    * V: Elena zaakceptowała że Arianna jest jej sojuszniczką. Poszły "do domu".
* 4 - Z K1 Klaudia dostała wiadomość - z Atroposa. Od Martyna. Martyn zwykle nie jest tak niesamowicie tajemniczy. A kody - Grzymościa.
    * TrZ (zna Martyna) +3:
    * VXzVz: Martyn ostrzegł 'shadow conspiracy', ktoś -> Elena, coś nie tak z TAI, nie wszystko zrozumiałe. Biovaty nie utrzymają Eleny i amnestykuje się z wiedzy. Biosygnał zostawi.
* 5 - Klaudia ma detektor próbek Martyna. Miała czas przygotować. Więc próbuje znaleźć ślady Martyna - co jej zostawił.
    * TrZ+2
    * XXVz: nie ma opcji pinpointowania próbek, ale NIE WYKRYWA MARTYNA. Czyli Martyn to nie Martyn?
* 6 - NA WIEWIÓRZE - Klaudia chce wyprowadzić Martyna (miragenta) z Wiewióra.
    * TrZ (Martyn) +2
    * XVzVX: uszkodzenie struktury miragenta, miragent nic im nie zrobił i wymrze - a Klaudia nic się nie dowie więcej
* 7 - Arianna - ping, magiczny skan, czy rezonuje z Eleną
    * TrZM+3+3Om
    * V: to nie jest Elena. TO NIE JEST ELENA. Ktoś zabrał Elenę.
* 8 - Arianna przesłuchuje losowych lekarzy w bazie. Odpowiedzi. Co dostanie. GDZIE SĄ AKOLICI. Doświadczeni lekarze, od wielu lat.
    * TrZ (reputacja) +3
    * XXVVzX: Syndykat wie że Arianna na nich poluje, opóźnienie operacji o kilka godzin, akolici Martyna zniknęli WCZORAJ, ~14h. Odkąd problemy - TAI pali banki danych.
* 9 - Klaudia chce iść przez TAI. TAI "czy jesteś admirałem?" "TAK! Używa nieaktualnych, starych kodów Kramera":
    * ExZ+4
    * XVVr: TAI protokół selfdestruct, Klaudia zablokowała i TAI powiedziała co wie - dowódca za tym stoi i współpracuje z Syndykatem.
* 10 - Kapitan poddał sygnaturę jednostki "Dziecięca Radość" oraz gdzie powinna być za 2 dni. Na podstawie tego Klaudia liczy.
    * TrZ (lokalna TAI która chce zemsty) +3 (kapitan pomóc próbuje):
    * XV: bez kapitana by się nie dało tego zrobić, ale jak Tivr poleci bardzo szybko to zdążą dotrzeć przed obudzeniem Eleny
* 11 - Pilotaż Tivra. Lecimy SZYBKO. Bardzo szybko. Pozostałe statki podążają za nami. Doświadczeni noktianie. Arianna ma mówkę motywacyjną. "Your chance to shine"
    * TrZ+3
    * VXXV: Tivr zdążył, Aurelionowcy wierzą że nie ma niebezpieczeństwa i przygotowują się na shipfight, Raoul insertuje Leonę i Eustachego.
* 12 - Leona robi apokaliptyczny chaos z nowym sprzętem.
    * ExZ+4
    * XVz: Leona zabiła kilka osób, odciągnęła uwagę od Eustachego. Leona jest maszyną masowej zagłady.
* 13 - Eustachy - sabotujesz fragmenty statku by wymusić ewakuację.
    * TrZ+4
    * VzXVX: część zginęła się broniąc, Elena się budzi, ale się ewakuowali bez niewolników. Więc - wszystko wyszło.

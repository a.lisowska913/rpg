---
layout: cybermagic-konspekt
title: "Egzotyczna Piękność na Astralnej Flarze"
threads: historia-arianny
gm: żółw
players: fox, kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [221116 - Astralna Flara dociera do Nonariona Nadziei](221116-astralna-flara-dociera-do-nonariona-nadziei)

### Chronologiczna

* [221116 - Astralna Flara dociera do Nonariona Nadziei](221116-astralna-flara-dociera-do-nonariona-nadziei)

## Plan sesji

### Fiszki
#### 1. Astralna Flara (55 osób max)
##### 1.1. Dowodzenie

* Arianna Verlen: kapitan
* Daria Czarnewik: engineering officer, (4 inż pod nią)
* Alezja Dumorin: eks-kapitan, Orbiter, 
    * ENCAO:  +0--0 |Amoralna, skuteczna| VALS: Hedonism, Face, Power| DRIVE: Wędrowny mistrz Ryu
* Władawiec Diakon: pierwszy oficer, tien, (p.o. Stefana) 
    * ENCAO:  +-0-0 |Intrygancki;;Żywy wulkan| VALS: Hedonism, Self-direction| DRIVE: Follow My Dreams, Korupcja anioła
* Grażyna Burgacz: logistyka i sprzęt (officer), tien (3 osoby pod nią) 
    * ENCAO:  -0+-- |Powściągliwa i 'nudna';;Ascetyczna| VALS: Humility, Tradition| DRIVE: Starszy Brat
* Arnulf Perikas: fabrykacja i produkcja (officer), tien (2 inż pod nim) 
    * ENCAO:  0-0-- |Hardheaded;;Napuszony| VALS: Tradition, Family| DRIVE: Wzbudzenie zachwytu
* Maja Samszar: comms officer, tien (p.o. Klarysy jak K. nie może) 
    * ENCAO:  0+-0- |Moralizatorska| VALS: Stimulation, Conformity >> Power| DRIVE: Długi
* Kajetan Kircznik: medical officer (5 pod nim) (czarny, afro, paw, augmentacje bio)
    * ENCAO:  +-0+0 |Anarchistyczny;;Stabilny emocjonalnie;;Z poczuciem humoru| VALS: Self-direction >> Power| DRIVE: Jestem unikalny

##### 1.2. Operacja

* Klarysa Jirnik: artillery, (p.o. Mai jak Maja nie może) 
    * ENCAO:  00+-- |Wymagający;;Małostkowy| VALS: Tradition, Conformity|DRIVE: Sprawiedliwość
* Tomasz Ruppok: starszy mat (13 osób + 2 starszych)
    * ENCAO:  +0--- |Mało punktualny;;Kłótliwy;;W przejaskrawiony sposób okazuje uczucia| VALS: Hedonism >> Achievement, Family| DRIVE: Apokalipta
* Rufus Warkoczyk: starszy mat (13 osób + 2 starszych)
    * ENCAO:  0-+-0 |Zarozumiały;;Bezkompromisowy, niemożliwy do zatrzymania;;Zorganizowany| VALS: Self-direction, Power| DRIVE: Odkrycie konspiracji ("ktoś nas sabotuje")
* Marcelina Trzęsiel: inżynier syntezy (pod Arnulfem), pod opieką tien Terienaka
    * ENCAO:  0+0-+ |Małostkowa;;Dramatic shifts in mood;;Różnorodność form| VALS: Hedonism, Achievement >> Security| DRIVE: "Co za tą górą"
* Elena Verlen: pilot / advancer

##### 1.3. Infiltracja / Starcie

* Gerwazy Kircznik: sierżant marine, Orbiter
    * ENCAO:  +0+-0 |Głośny;;Honorowy| VALS: Humility, Face| DRIVE: Lokalny społecznik
* Erwin Pies: kapral marine, Orbiter
    * ENCAO:  -0-+- |Skryty;;Wiecznie zagubiony;;Praktyczny| VALS: Tradition, Stimulation >> Power| DRIVE: Ochraniać słabszych
* Szymon Wanad: marine, Orbiter
* Marcel Kulgard: marine, Orbiter, stary znajomy Arianny
    * ENCAO:  000-+ |Demotywuje innych;;Innowacyjny| VALS: Achievement, Face| DRIVE: Zbudować legacy
* Tomasz Dojnicz: marine, Orbiter
    * ENCAO:  --0+0 |Nie znosi być w centrum uwagi;;Dusza towarzystwa| VALS: Conformity, Hedonism >> Power| DRIVE: Arlekin Maytag
* Szczepan Myrczek: advancer, tien, seilita,
    * ENCAO:  --+0- |Stabilny emocjonalnie;;Zawsze bardzo zajęty| VALS: Face, Achievement, Hedonism >> Self-direction| DRIVE: Zasady są święte
* Mariusz Bulterier: advancer, tien, seilita, sybrianin 
    * ENCAO:  0-+-- |Prostolinijny i otwarty;;Nie kłania się nikomu;;Uroczysty i poważny| VALS: Humility, Tradition| DRIVE: Nawracanie
* Hubert Kerwelenios: advancer, sarderyta
    * ENCAO:  -0+-0 |Kontemplacyjny, refleksyjny;;Kompleks paladyna| VALS: Humility, Tradition >> Stimulation| DRIVE: Odbudowa i odnowa

##### 1.4. Other

* Ellarina Samarintael: Egzotyczna Piękność
    * ENCAO:  +-00+ | Spontaniczna;; Chipper;; | VALS: Power, Hedonism >> Family | DRIVE: Wyciągnąć kogoś z bagna, dobrze wyjść za mąż

#### 2. Athamarein, korweta

* Gabriel Lodowiec: komodor-in-training
    * ENCAO: 0+-+0 |Spokojna fasada;;Kontrolowany przez emocje | VALS: Tradition, Security >> Power, Face| DRIVE: Procedury, poprawność systemu, kaganek oświaty.
* Leszek Kurzmin: dowódca jednostki
    * ENCAO: 0-+++ |
* Alan Nierkamin: eks-lokalny przewodnik, advancer
    * ENCAO: ++-0- |Nietolerancyjny, musi być tak jak uważa;;Dokładnie przemyśli wszystko zanim coś powie| VALS: Power, Achievement >> Tradition| DRIVE: wprowadzi Orbiter na Nonarion

#### 3. Nonarion Nadziei (stacja cywilna)

* Leo Kasztop: sprzedawca sekretów na Nonarionie (atarienin)
    * ENCAO:  0-+00 | Intrygancki, polityka;;Nie odracza| VALS: Self-direction >> Stimulation, Tradition| DRIVE: Wygrać w rywalizacji)
* .Franciszek, enforcer Nonariona (atarienin)
    * ENCAO:  +00-- | Lubi rutynę;;Uszczypliwy i zgryźliwy;;Lubi wyzwania | VALS: Achievement >> Security| DRIVE: Komfortowe życie
* .Wojciech, agent Aureliona (faeril)
    * ENCAO:  ---+0 |Bezbarwny, przezroczysty;;Niemożliwy do ruszenia| VALS: Power, Humility >> Tradition | DRIVE: Supremacja Aureliona

### Theme & Vision

* Problemy kulturowe; Orbiter nie respektuje tego co wypracował Nonarion
    * Ratowanie zniszczonego statku - TAI też potrzebuje pomocy (TAI Mirtaela d'Hadiah Emas)
    * Wyjście na pokład statku kończy się... źle
* Daria nie ma łatwego powrotu
* Władawiec corruptuje Elenę

### Co się wydarzyło KIEDYŚ

* .

### Co się wydarzyło TERAZ (what happened LATELY / NOW)

* .

### Co się stanie (what will happen)

* S1: Nonarion itp., wejście na pokład stacji
* S2: Jak zrobić bazę
* S3: Ćwiczenia opracowane przez Władawca i ostrzeżenie ze strony Alezji - on coś planuje z Eleną
* SN: Hadiah Emas w ruinie - poważnie uszkodzona jednostka, ma 31 rannych na pokładzie
    * pogarda ze strony załogi
    * zniszczyć TAI Mirtaela i jej cztery synty
    * Hadiah Emas się rozpadła próbując ratować Isigtand, trzyosobowy grazer

### Sukces graczy (when you win)

* .

## Sesja właściwa
### Wydarzenia przedsesjowe

* Komodor Lodowiec chce zrobić:
    * WSZYSTKIE załogi muszą choć tymczasowo pomieścić się na Flarze. Problem z Hadiah Emas nie mogą tam być.
        * Zarówno Pies jak i Leszek Kurzmin poinformowali że to fatalny pomysł - ponieważ jest ryzyko porwania.
        * Komodor wierzy w marines Orbitera
        * Erwin Pies widział w akcji Serpentisa. Kiedyś.
    * Ellarina i załoga Isigtand trafia na pokład Flary -> bo znają Darię.
    * Komodor chce uwolnić niewolników.
    * Komodor chce zniszczyć neikatiańską TAI

### SPECJALNE ZASADY SESJI

.

### Scena Zero - impl

.

### Sesja Właściwa - impl

Stoi Pies i Kircznik i oboje rekomendują. 

* K: "Pani kapitan, nie możemy wziąć ich na pokład. Nie mamy aż tyle dopalaczy bojowych by móc sobie poradzić jak dojdzie do czegoś złego."
* A: "Jeśli chcemy naprawić Hadiah Emas, musimy ich przemieścić."
* K: "Niebezpieczne - Flara"
* A: "Dobra wola na ten teren"
* K: "Flara ma za małą załogę i system podtrzymywania życia."

Pies się oddalił zająć się wprowadzaniem Ellariny na pokład.

Arianna chce pogawędzić z komodorem Lodowcem. Daria miała argumenty - "nie ma TAI? Bezużyteczny a Persefona sobie nie poradzi plus nikt im nie da Persefony. Wycinanie TAI - niszczenie floty. Nie ma dobrych stosunków." 

Daria dostała polecenie od Arianny zrobienia symulacji - wycięcia kontenerów niewolniczych z Hadiah Emas i podpięcie ich do Flary. Przy założeniu, że shuntujemy system podtrzymywania życia, a potem to przywrócić. Z dobrych wieści - Hadiah Emas ma dobrą konstrukcję (ciężarówka + przyczepa). Ale przypięcie kontenerów do Flary spowoduje, że będzie dużo mniej ruchliwa i stabilna - przypięcie punktowe kontenery spawami + węże z systemu podtrzymywania życia. Flara ciągnęłaby Hadiah Emas i miałaby niewolnicze kontenery. I ciągniemy resztki Isigtand. Drogie, ale możliwe.

Daria próbuje zaplanować to wszystko:

TrZ (w dobrym stanie jednostka fabrykacyjna + obecność Isigtand itp) +4:

* Vr: jest to wykonalne. Daria jest w stanie to opracować i nie zagraża to misji.
* X: niestety, operacja zajmie mniej więcej 20 godzin i spore zużycie zasobów. Nie do tego Flara jest zaprojektowana. Flara nie jest stocznią!

I z tą wesołą myślą Daria zostawiła Ariannę. Albo Flara ryzykuje uszkodzenie albo to zajmie sporo czasu.

Arianna połączyła się z Lodowcem. 

* G: "Kapitan Verlen?"
* A: "Porozmawiać o przeniesieniu ludzi na pokład Flary"
* A: "Przepnijmy sektorzy niewolnicy do Flary i wszystkich na Flarę"
* G: "Jakie koszta to będzie niosło?"
* A: (propozycje Darii)
* G: (w oczach Gabriela mignęła taka prawdziwa złość... taka FURIA... ale minęła)
* G: "Co rekomendujesz? Który wariant? Lepiej znasz swoją jednostkę."
* A: "Jak dotrzemy, odbudujemy zasoby. Ustacjonaryzujmy. Nie ryzykujmy."
* G: "Chciałem zaproponować to samo. Nie wiemy, z czym się spotkamy dalej. Tu jesteśmy w bezpiecznej pozycji."
* A: "Są anomalne asteroidy, nic groźnego nie będzie w okolicy"
* G: "Zgadzam się"
* A: "Panie komodorze, potrzebujemy tamtej TAI. Rozmawiałam o tym z Darią i z tego co mówiła w tym rejonie nie ma TAI o naszych standardach. Nie da się dostać podobnej, nie utrzymają Persefony. Jak się będziemy rozbijać... odwrotne wrażenie."
* G: "Neikatiańskie TAI są mordercze, zwłaszcza, jak dostaną pakiet astinianu."
* A: "Zabierzesz im to, umrą"
* A: "Rzeczy które sprawiają że TAI są niebezpieczne są rzadkie w tym rejonie, po co im to."
* G: "Zgadzam się z Tobą. Jednak neikatiańskie TAI mają... jednoznaczne regulacje."
* A: "Jakby magowie nie byli groźny, ale mamy ich na pokładach. Czasem przystosowujemy statek, ale bierzemy bo są niezbędni."
* G: "Dobrze, rozmawiałem z moim inżynierem i mam alternatywny pomysł. Ograniczniki TAI. Ogranicznik Percivala."
* G: "Prześlę na Flarę blueprinty. Będziemy musieli to zsyntetyzować."
* A: "Nie sądzi pan że powinniśmy skonsultować to z kapitanem tego statku?"
* G: "Z łowcą niewolników?" /pogarda do kapitana Hadiah Emas
* A: "Zdecydowaliśmy się pomóc tej jednostce"
* G: "Tak, pomogliśmy. A potem poznaliśmy co to jest - niewolnicy, neikatiańska TAI... nie mogło być gorzej. Chyba, że to byłaby jednostka noktiańska."
* A: "Mogli być piratami w przebraniu."
* G: "Wtedy mielibyśmy PROCEDURY. I czyste sumienie. Tak jak jest, chcę im pomóc. Ale... (/wściekły) ograniczymy tą TAI i uwolnimy tych niewolników. I uratujemy ich. I pokażemy im wszystkim że Orbiter im pomoże. Pomożemy każdej jednostce."
* A: "Na pewno..."
* G: "Wykorzystaj blueprint, wsadzimy im ogranicznik tak, by nie wiedzieli. Masz rację, że nie chcemy ich antagonizować."
* A: "Gdzie pan zamierza ulokować niewolników? Mamy gdzie ich zatrudnić?"
* G: (myśli) "Oddać ich wolnych na Nonarion?"
* A: "Niekoniecznie lepszy los niż jakby trafili i mieli pracę."
* G: "Nie możemy ich uwolnić do naszej bazy, jakbyśmy to zrobili to mamy noktiańską infiltrację."
* A: "Jak to noktiańską?"
* G: "Trzech niewolników to noktianie. Czy wy w Aurum... praktykujecie niewolnictwo?"
* A: "Moja rodzina nie"
* G: "Cieszę się." (cisza) "A ktoś inny z Twojej załogi?"
* A: "Nie jest to wykluczone."
* G: (chwila ciszy) "Nie mam tego w procedurach. Niechętnie to mówię, ale najpewniej ich uwolnimy. Nie popełnili przestępstwa... (chwila ciszy) MUSIMY ICH PRZESŁUCHAĆ!"
* A: "Tak naprawdę są ludźmi którym trzeba pomóc"
* G: "Nie mam tego w procedurach. Prywatnie im współczuję. Ale nas napadli."
* A: "Zapłacili za to olbrzymią cenę. Sami, bez połowy nerek, na statku niewolniczym"
* G: "Jeśli są niewinni, uwolnimy ich. Jak normalnych. Zrobimy im kartotekę i dokumenty. Jako obywatelom sektora astoriańskiego."
* A: "I nie wyrzucimy na Nonarion?"
* G: "Chodzi mi o to, że jeśli są NIEWINNI po 10 latach od wojny to albo są super-szpiegami albo naprawdę nic nie zrobili. W takim razie możemy ich, nie wiem, wysłać na Valentinę czy coś. Czy wykorzystać. Ale jeśli są winni, standardowe procedury (rozchmurzył się)."
* Leszek->G: "Komodorze, mamy wśród niewolników jednego człowieka z ORBITERA!"
* G: "(spochmurniał) Nasza sytuacja się właśnie utrudniła."
* A: "Jakim jest to problemem?"
* G: "Kapitan Verlen, ktoś ma w niewoli Twojego człowieka. Mógł Ci go oddać. I go uratowaliśmy."
* A: "Nie do końca mieli okazję nam go oddać, jesteśmy krótko"
* G: "Przynajmniej aresztujemy kapitana. Poprawi mi to humor. I mam na to procedurę."
* A: "Niech pan pomyśli o negatywnym efekcie!"
* G: "(dzika furia) Orbiter obiecuje coś ludziom! MUSIMY go aresztować!"
* A: "Pytanie czemu został złapany i co tu robił"
* G: "(opanował się) Nie mam nikogo z _military police_..."
* A: "Mogę ja to zbadać ze statkiem niewolniczym? Obcy teren, nie znamy kultury do końca, na Eterni jest niewolnictwo, w Aurum też..."
* G: "Orbiter jest bardziej cywilizowany... (myśli chwilę) dobrze, sprawa jest Twoja, jak sobie nie poradzisz - daj znać."

Gdy tylko Arianna wróciła na pokład Flary, SZOK. Statek LŚNI. Ludzie w mundurach pod kancik. Morale w górze. Tak wysokie jak nigdy. A w mesie zebranie załogi z rezydentnym oficerem medycznym. Arianna podsłuchuje i słyszy opowieści jak Kajetan opowiada o Egzotycznych Pięknościach - jak on dobrze opowiada, ma swadę i gadane. A załoga jak na baczność.

* A: "To moje urodziny? Co się stało?"
* Warkoczyk: "Pani kapitan! Melduję, że na pokładzie jest zamówiona przez Panią Egzotyczna Piękność! W imieniu załogi i w sumie wszystkich bardzo dziękujemy i będziemy starać się jeszcze bardziej!"
    * Cała sala pała.
* A: "Gdzie jest sierżant Pies?"
* W: "Melduję, że przesłuchuje ową Piękność!" /rozczarowanie, że nie pozwolił patrzeć

Arianna poszła do pokoju spotkań. Jest tam Pies. I Egzotyczna Piękność. Zasłużyła na to miano. Takie unikalne połączenie "dziewczyny z sąsiedztwa" skrzyżowane z czymś z kosmosu - błękitna skóra, skośne oczy, takie KIEŁKI. Blond włosy mimo że nie ma wiatru, drobna perfekcyjna figura, strój pozwalający wyobrażać sobie. I słodziutka minka. Innymi słowy - danger level red. Ona wie co robi i się tym świetnie bawi. Pies taki zrezygnowany.

Ellarina jest, zdaniem Arianny, ZAGROŻENIEM MEMETYCZNYM na jednostki. Daria przywykła. Arianna potrzebuje wsparcia XD. ONI NIE WIDZIELI CZEGOŚ TAKIEGO!

Arianna chce Psa na ubocze. Jak poszło przesłuchiwanie? Pies "pani kapitan, nie spotkałem się z czymś takim. Ona... nie mam nic co mogę złapać. Nie mogę nacisnąć, nie mogę... nie wiem jak do niej podejść. Nic jej nie powiem, ale ona mi powie to co chce." "Nie ma innych z Isigtanga - nie weszli na pokład jednostki. Sierżant pilnuje tego typu spraw. Nie rozumiałem, czemu MNIE kazał ją przesłuchiwać, ale jak usłyszał o niej to się wysmerfował..."

Arianna -> Daria: "Kto powinien być na pokładzie?" -> nazwiska. WIDAĆ że Gerwazy Kircznik ma **doświadczenie** z Egzotycznymi Pięknościami. Dobrze założył defensywy. Faktycznie - Orbiter dał Ariannie kompetentnych ludzi.

Pojawił się mem, wyciek z przesłuchania: "Pies szczeka nie głaszcze". Jak Pies podniósł głos, Ellarina zaczęła płakać a Pies nie wie co robić i mówi "uspokój się, to ROZKAZ!" a ona płacze bardziej.

Daria wraca. Po drodze przechwytuje ją Szczepan Myrczek. Daria zdziwiona. Myrczek nie gada z nią na co dzień.

* D: "Słucham?"
* M: "To prawda, że zna się pani z Ellariną?"
* D: "Znam trochę osób w tym ją"
* M: "Byłaby zainteresowana modelami holograficznymi nietypowych budowli?"
* D: (ona WIE - wszystkim co jej to da)
* D: "Chorąży... rada z całego serca. Kobiety na tej stacji to nie są najlepsze partie."
* M: "Ale... mam trochę ziemi, mam sentisieć, mam umiejętności..."
* D: "Jesteś dorosłym facetem, ale ostrzegam Cię, że Twoje serce może z tego nie wyjść w jednym kawałku."
* M: (nie rozumie)

Daria dotarła do Arianny i jednego zgnębionego Psa (który mruknął "jest ładna, gdybym miał, to bym wpieprzył... ale nie wiem czy mogę..."). Arianna robi tak, by Władawiec podglądał rozmowę z Darią, by wyrobić sobie opinię na temat naszej Egzotycznej Piękności.

* Ellarina: "Dario!" (wyściskała, 1000 pytań / sekunda, przebiegła po pokoju, usiadła, przekrzywiła głowę) "Będziesz mnie przesłuchiwać?"
* Daria: "Jeśli chcesz tak to nazwać"
* Ellarina: "Tego jeszcze nie robiłyśmy" (śmiech)
* Ellarina: "Ten statek jest CZYSTY! Ludzie są w wyprasowanych mundurach! I oni mówią 'pani kapitan' tak nieironicznie! Ogden też zasłużył na takie traktowanie, po prostu... (lekki smutek)"
* Ellarina: "Przesłuchuj mnie!"
* Daria: "Jak się działo jak Cię nie było"

Daria próbuje się dowiedzieć, czemu Ellarina jest sama. Ellarina spochmurniała "wpadliśmy w problemy finansowe. Oni... są collateralem. A ja straciłam Isigtand. Oni są na Nonarionie i nikt ich nie uratuje! Ale jak przybyliście, może jest nadzieja... JEDEN JEDYNY RAZ czegoś potrzebuję a kapitan jest kobietą!!! (wyrzut)."

Ellarina mówi:

* Daria "nie jest swoja", bo kręci z Orbiterem. Z siłami spoza Anomalii Kolapsu. Bo jest w innej ekonomii. Jest Z ZEWNĄTRZ.
* Faktycznie, ktoś gdzieś ma ukrytą jednostkę typu ruchoma stocznia. Musi mieć. To jedyne co tłumaczy standaryzację i poziom części. Nie wie czyje.
    * To cholernie dobra operacja. Przechodzi przez różne frakcje.
* "(pisk) Kupiliście planetoidę niedaleko Strefy Duchów? No, jesteście odważni."
    * Leo by Darii nie wrobił
    * Ellarina opowiada OPOWIEŚCI o nieumarłych. Jest strefa. I w tej strefie pojawiają się nieumarli. Wchodzą na pokład, nieśmiertelni itp.
    * 15 minut od strefy śmierci jest planetoida.
    * statki przyzywane śpiewem syreny. To plotka - bo żaden statek który słyszał śpiew syreny nie wrócił. I nikt nie poleciał sprawdzić.
* E: "Kto jest bogaty?"
* D: "(oficerowie Aurum)"
* E: "Ech, trzeba się ustatkować kiedyś (gwiazdki w oczach)"
* E: (podaje strategiczne info o frakcjach itp.)

Daria -> Arianna, po spotkaniu.

* Arianna -> Ellarina: "To zaszczyt i bardzo dziękuję. Niewiele jednostek by próbowało mnie uratować..."

Arianna upozycjonowała Ellarinę jako 'osobę od morale' i maskotkę - Arianna kieruje ją na to co chce osiągnąć. I jej głos na statku jako komunikaty, wieczorny radiowęzeł karaoke gdzie ona ma główną rolę... plus blisko łączności ją wsadzić. Maja jest uczciwa, jest laską i dalej interesuje się Władawcem. Czyli Ellarinę kontruje. 

* E: "Pani kapitan, a dostanę, nie wiem, mundur? Czy raczej... normalne stroje?"
* A: "Tylko Orbiter nosi mundury"
* E: "Dobrze."

.

* Arnulf -> Arianna: "Kapitan Verlen, mam przyjemność... wyprodukowałem pierwsze Dziwne Urządzenie."
* Arianna: "To z planów komodora?"
* Arnulf: "Tak. Jest duże. Wygląda jak... komputer który pracuje na komputerach? Taki... mechaniczny... hacker?"
* Arianna: "Daria musi zaaprobować."
* Arianna: "Nie robiłeś nic podobnego?"
* Arnulf: "Nie, nie wiem co to jest. Plany mają... nie mówią co to jest."
* Arnulf: "Ale mam instrukcję! Potrzebni są dwaj inżynierowie, coś na kształt psychotronika? i advancer."

Arianna i Daria, w warsztacie Darii.

* A: "Mamy zamontować to na tamtej TAI jako kompromis."
* D: (odkłada narzędzie) "Pani kapitan, nie zamontuję tego."
* A: "Jest problem z tym urządzeniem? Nasze Persefony też są ograniczane"
* D: "Czy wie pani kapitan JAK budowane są TAI? TAI są odwzorowaniem prawdziwej osoby. Z jej osobowością, myślami, zachowaniami. Pracowałam z taką TAI. Była bardziej ludzka niż co poniektórzy z naszych marines."
* A: "Nie trzeba dużo by przebić Wanada..."
* D: "Te TAI nie mają klasy a imiona. Uważają za skazę to, że statkowi coś się stanie. Załogi współpracują z TAI i dbają o TAI."
* A: "Tak zamontuj by byli w stanie to odmontować. Komodor kazał na to zamontować bo TAI są niebezpieczne."
* D: "A derelicty nie są. A magowie nie są."
* A: "Ogranicznik zadziała wyłączając funkcje niebezpieczne dla ludzi"
* D: "Wlatują w niebezpieczne obszary, robią rzeczy w derelictach..." (głębokie przekonanie Darii)
* A: "Nie znam się na TAI tutaj i na inżynierii i nie wiem jak to jest zamontowane. Zasugerował to ktoś od komodora - on może chcieć zobaczyć lub pomóc."
* D: "Jeśli to zrobimy, to... będzie trochę jak gwałt na tej TAI. Persefony są ZAPROJEKTOWANE do tego. To nie jest kopia osoby."
* A: "Z jednej strony masz rację, z drugiej rozumiem niepokój komodora odnośnie zagrożeń."
* D: "Pani kapitan, ten TEREN jest zagrożeniem!"
* A: "Zgodził się nie usuwać TAI."
* D: "Bo statek poszedłby na złom..."
* A: "Obawiam się, że jak to zamontujemy zadziała źle z tą TAI bo jej nie znamy..."
* D: "Dobrze, zrobię to - ale pragnę zalogować mój oficjalny protest. Uważam to za nieludzkie i niepotrzebne okrucieństwo."
* A: "Zaloguję ten proces."

TYMCZASEM MARCEL.

* M: "Kapitan Verlen. Daleko zaszłaś. A ciągle trep. Taki los."
* A: "Nie widziałeś na jaką jednostkę trafiłam"
* M: "Nie widziałem, widziałem gorsze od tej."
* A: "Od poprzedniej - nie sądzę. Od tej - na pewno".
* A: "Ale zrobiliśmy postępy"
* M: "W sprawie kolesia z Orbitera..."

Tr Z (kij i marchewka) +3 + 5Or:

* Vr: Marcel dowiedział się, co to za jeden. To jest eks-żołnierz Orbitera. Uciekł z Orbitera, mimo, że marine.
* Xz: Dezerter wie, że jedyne co go czeka to sąd wojskowy. Dezerter nie chce współpracować. Nie chce wracać.
* V: Marcel poznał historię - dezerter zakochał się w noktiance. Działał na tyłach, polubił ją, była miła, sympatyczna. Nie chciał jej zguby. Uciekł z nią tu dla lepszego życia. Skończyli marnie. Nie wie co się z nią stało.
* Or: Pod wpływem RAN i zadawanego bólu w końcu powiedział więcej - powiedział WSZYSTKO. Uczestniczył w jednej z tych akcji, gdzie tacy mniej mili komodorzy wykonywali operacje. Nie było TRZEBA robić takich zniszczeń. Ale - zemsta za przyjaciół itp. WSZYSTKO powiedział - nazwy jednostek, co robił, gdzie się ukrywał... Marcel go złamał. I jest nagranie.

Marcel ze smutnym uśmiechem: "Niestety, to nie jeden z naszych. Nasi nie dezerterują. Mimo okrucieństw wojny. A na pewno nie dla laski." (Arianna wie, że Marcel robił straszne głupoty dla laski jak był w Akademii). Arianna: "Jakbyś dla mnie nie zrobił głupoty". Marcel: "Nie zdezerterowałem".

.

* Gabriel -> Arianna: "Wiesz, że tego protestu Darii NIE DA się wycofać? To jest zarezerwowane na molestowanie i tego typu rzeczy?"
* A: "Z punktu widzenia Darii to dokładnie to"
* G: (niedowierzanie) "Ze wszystkich rzeczy z TYM mam mieć problem?"
* A: "Z innej perspektywy to coś oczywistego a jedna z dwóch osób z regionu co o tym usłyszała to uważa to za skandal na poziomie na niewycofywalną skargę. Skoro osoba która tak długo była na Orbiterze była i protest, jak zareagują ludzie..."
* G: "Kapitan Arianno Verlen. To urządzenie jest zakładane na TAI. Wprowadza do jej psychotroniki specjalne komponenty mechaniczne. Przecina ścieżki. Potem urządzenie niszczysz i wraca na recykling. TAI nawet nie będzie pamiętać. Nie będzie śladu." (widzi że Arianna nie czai) "Przed starciem z Saitaerem mieliśmy do czynienia z runaway TAI typu neikatiańskiego. Nikt o tym nie wie. Czemu? Bo to utajniliśmy. Byłem porucznikiem na tej operacji. Niewyobrażalne straty. Rozumiesz?"
* A: "Rozumiem, panie komodorze. Jak wcześniej mówiłam, z punktu widzenia miejscowych TAI są wzorowane na prawdziwej osobie stąd magowie..."
* G: "Dostarcz na Athamarein to urządzenie. Jeśli nie umiesz opanować swojej załogi, my to zrobimy." (zreflektował się po chwili) "Przepraszam. Ale podtrzymuję - przekaż urządzenie na Athamarein. Kurzmin się tym zajmie. Nie musisz mówić swoim, że cokolwiek w tym temacie działa." (ciężkie westchnięcie). "Daj mi tylko advancera. Dyskretnego. Czy nie masz nikogo z..." (chwila zawahania) "Nie. Nikogo mi nie dawaj."

Arianna dawkuje wieści o dezerterze i Ellarinie.

Daria NADZORUJE i PRZEPROWADZA naprawę Hadiah Emas.

Wpierw odmontowuje elementy niewolnicze. Najlepiej jak się da. Komunikat od TAI. 

* TAI Mirtaela: "Jesteś z tego terenu?"
* Daria: "Dawne czasy"
* TAI: "Przyjęłam, dziękuję."
* Daria: "Czemu pytasz?"
* TAI: "Z ciekawości. Nieczęsto spotykam osoby spoza tej przestrzeni."
* Daria: "Cóż. Tam jest inaczej."
* TAI: "Podobno jest czysto?"
* Daria: "Pierwsza rzecz, która się rzuca w oczy."
* TAI: "Mogę coś zrobić, by nie aresztowano mojego kapitana? To dobry człowiek. Chciał pomóc i nie robi nikomu krzywdy."
* Daria: "Jedyną osobą decyzyjną w całej tej organizacji na tym poziomie jest komodor. Możesz spróbować z nim porozmawiać. Ale nie przyznawaj się że jesteś TAI. Nie od razu."
* TAI: "On jest rasistą?"
* Daria: "Trochę. Bardzo."
* TAI: "Nie pierwszy, nie ostatni. I tak jestem wdzięczna, bo pomógł. Wiele osób by nie pomogło."
* Daria: "Nie jest całkiem zły. Z wyjątkiem rasizmu to całkiem sensowny człowiek. Nie wiem co go boli, ale..."
* TAI: "Porozmawiam z nim, spróbuję przekonać. Przedstawię się jako... oficer łącznikowy."
* Daria: "Weź kogoś z załogi."
* TAI: "Nikt nie spodziewa się oficera łącznikowego poza statkiem!"
* Daria: "Mhm..."

...

* TAI: "To jest ciekawe, bo 3 statki podobne do Hadiah Emas zniknęły niedaleko tzw. Strefy Śmierci. Tej strefy z duchami. I chyba tylko takie statki. Plus te co tam leciały."
* TAI: "Uważajcie jak tam polecicie."

Tr Z (fabrykacja + kompetentni ludzie + wsparcie TAI Mirtaela) +3:

* V: Prawidłowo odpięte i przypięte do Flary. Przesunięte węże itp. Wyjęte łańcuchy itp.
* Vz: Sprawna odbudowa jednostki, wszystko toczy się prawidłowo. Nie jest w dobrym stanie, ale dopełznie do Nonariona i jest dobra ale krucha.
* Vz: Bezproblemowa naprawa sytuacji, nie trzeba było nawet używać nic ekstra z Isigtand. Jest wrażenie wśród załogi Hadiah Emas.
    * Daria prezentuje TAI oraz załodze nowe wartości i parametry.

...

Kilkanaście godzin później, Arianna jest proszona przez komodora. Znowu.

* G: "Wszystko, z tego co widzę, działa zgodnie z planem. Podobają mi się wykresy Hadiah Emas. Masz dobrego inżyniera."
* A: "Prawda, ufam jej jak mało komu"
* G: "Jaka jest Twoja opinia odnośnie kary dla kapitana Hadiah Emas?"
* A: "Powinniśmy go uwolnić - dowiedzieliśmy się o naszym człowieku z Orbitera, więc ten człowiek z Orbitera by ucierpiał. To samo byśmy zrobili."

Komodor Lodowiec pokazał też Ariannie zapis z rozmowy z TAI Mirtaela. Miała już odchylenia od parametrów TAI. Zdecydowanie jest zdolna do przekroczenia parametrów TAI i niekontrolowanego działania. Dlatego Ograniczenie - jego zdaniem - było konieczne. Zdaniem Lodowca, Mirtaela została bardzo dyskretnie Ograniczona i nikt się nie zorientuje. Arianna wie, że Daria uważa inaczej, ale Lodowiec jest nieprzekonywalny w tej jednej kwestii...

## Streszczenie

Flara naprawia Hadiah Emas, a po drodze komodor Lodowiec rozplątuje wszystkie wątki - niewolnik z Orbitera? (okazał się dezerterem), autonomiczna TAI Mirtaela d'Hadiah Emas? (zostaje Ograniczona przez psychotronika Athamarein) itp. Arianna zarządza Flarą, wprowadzając Ellarinę jako maskotkę od morale i stabilizując wszystko by jakoś zespół dopasował się do realiów Nonariona. To nie jest ani Aurum ani Orbiter, to coś zupełnie innego.

## Progresja

* Erwin Pies: pojawił się mem, wyciek z przesłuchania: "Pies szczeka nie głaszcze". Jak Pies podniósł głos, Ellarina zaczęła płakać a Pies nie wie co robić i mówi "uspokój się, to ROZKAZ!"
* Gabriel Lodowiec: oficjalny protest i skarga od Darii Czarnewik, bo 'Ograniczenie Mirtaeli jest okrutne i jest sprzeczne z celami Orbitera na tym terenie'.
* OO Astralna Flara: spory koszt surowcowy podczas naprawy Hadiah Emas, zwłaszcza w obszarze rzeczy związanych z life support i podstawowe metale.
* SCA Hadiah Emas: części złożone przez Flarę; nawet jeśli jednostka jest delikatniejsza, to jednak jest sprawna. Niewolnicy uwolnieni, ale nadal na Hadiah Emas.
* SCA Hadiah Emas: jej TAI, Mirtaela, została Ograniczona przez Orbiter. Straciła "duszę", nawet jeśli zyskała efektywność. Załoga jeszcze nie wie.

### Frakcji

* .

## Zasługi

* Arianna Verlen: wprowadziła Ellarinę na Astralną Flarę i przesunęła ją na 'maskotkę / morale'. Współpracuje z komodorem i próbuje deeskalować jego gorsze pomysły (tępienie faerilskich handlarzy niewolnikami, Ograniczanie TAI Mirtaeli). Wygrywa trochę, przegrywa trochę. Zarządza Flarą. Miękko dowodzi jednostką korzystając z zaufania zespołu.
* Daria Czarnewik: przeprowadziła plan i procedurę naprawy Hadiah Emas tym co miała; duży koszt surowcowy, ale dobra okazja do pomocy. ABSOLUTNIE nie zgadza się na skrzywdzenie Mirtaeli i próbowała przekonać pośrednio komodora, by nie Ograniczył Mirtaeli. Przyjaciółka Ellariny, pomogła ją zintegrować z Astralną Flarą.
* Marcel Kulgard: miał stint w policji militarnej; przesłuchał niewolnika z Orbitera dla Arianny (odzyskanego z Hadiah Emas) i doszedł do tego że ten jest dezerterem. Brutalnie ale beznamiętnie. Dobry, kompetentny marine Orbitera i przyjaciel Arianny.
* Gabriel Lodowiec: rozsądny i wyważony; pyta Ariannę o jej propozycje i dopasowuje ludzi do zadań (widząc, że Arianna nie chce robić Ograniczenia TAI Mirtaela, dał to ludziom Kurzmina). Od czasu do czasu wpada w furię, ale umie się opanować. Dobrze wysokopoziomowo dowodzi jednostką. Niestety, ma traumę związaną z walką z niekontrolowaną neikatiańską TAI w przeszłości i jest niepowstrzymany w woli niszczenia ich.
* Władawiec Diakon: oglądał przesłuchanie Ellariny. Docenił jej umiejętności manipulacyjne i jej wpływ na morale. Uczy się, jak do niej podejść by ją kontrolować dla Arianny - może być jedyną osobą zdolną do przewidzenia jej ruchów i kontroli Ellariny na Flarze.
* Arnulf Perikas: zsyntetyzował dla Arianny Ogranicznik Percivala. Nie ma pojęcia co to jest; jest z Aurum i nie ma uprawnień. Ale umie trzymać język za zębami i nic nie powie.
* Leszek Kurzmin: dobrze pracuje z dokumentami i przetwarza dane; znalazł dla Gabriela Lodowca info, że jeden z niewolników to koleś z Orbitera. Przeprowadził dyskretną operację Ograniczenia Mirtaeli, TAI Hadiah Emas z rozkazu Gerwazego Kircznika.
* Kajetan Kircznik: Arianna znalazła go, gdy opowiadał w mesie załodze o przeszłych doświadczeniach z Egzotycznymi Pięknościami, jak to on i jego brat COŚTAM piraci i po ich stronie Piękność. Ulubieniec załogi. Świetny gawędziarz.
* Gerwazy Kircznik: ma doświadczenie z Egzotycznymi Pięknościami; kazał Psu przesłuchać Ellarinę a sam przygotował defensywy by nic głupiego nie zrobiła. Ostrzegł Ariannę, że wprowadzanie uratowanych na pokład będzie niebezpieczne. Dobrze współpracuje z Erwinem Psem.
* Erwin Pies: z G.Kircznikiem próbował ostrzec Ariannę by nie wprowadzać uratowanych na Flarę (ryzyko). Nie poradził sobie z przesłuchaniem Ellariny i został elementem mema "Pies szczeka nie głaszcze" jak clipowali fragment przesłuchania.
* Ellarina Samarintael: Egzotyczna Piękność z Isigtand; jej przyjaciele są na Nonarionie jako collateral a ona straciła grazer. Przyjaciółka Darii. W desperackiej sytuacji. Arianna dała jej szansę i przeniosła ją na "maskotkę / jednostkę od morale".
* Szczepan Myrczek: spytał Darię, czy Ellarina byłaby zainteresowana holograficznymi modelami nietypowych budowli i holodioramami. Daria mu powiedziała, że Ellarina pożre jego serce XD.
* Frank Mgrot: kiedyś marine Orbitera, teraz dezerter. Zakochał się w noktiance i uciekli koło Anomalii Kolapsu dla lepszego życia. Złapani jako niewolnicy; jego kochana noktianka zniknęła mu sprzed oczu a on się tułał. Aż teraz na Hadiah Emas został uratowany przez Orbiter i jego koszmar się dopiero zacznie XD.
* Hind Szug Traf: kapitan SCA Hadiah Emas; dostał opierdol za niewolnictwo, ale wszyscy go wspierali że nie jest taki zły (łącznie z TAI i niewolnikami). Więc nie został aresztowany.
* SCA Hadiah Emas: obiekt naprawy przez inżynierów z Astralnej Flary; doprowadzona do funkcjonowania.
* OO Athamarein: ma na pokładzie nadspodziewanie dobrego psychotronika. Na pokład trafił do aresztu Frank Mgrot ("marine z Orbitera / dezerter") zanim Hadiah Emas odleciała dalej.
* OO Astralna Flara: posłużyła do ratowania Hadiah Emas; Daria tymczasowo przebudowała Flarę z zewnętrz, by móc naprawić Emas i nikomu by się nie stała krzywda.

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Obłok Lirański
                1. Anomalia Kolapsu, orbita

## Czas

* Opóźnienie: 1
* Dni: 3

## Konflikty

* 1 - Daria dostała polecenie od Arianny zrobienia symulacji i przepięcie kontenerów niewolniczych do Flary by móc naprawić Hadiah Emas
    * TrZ (w dobrym stanie jednostka fabrykacyjna + obecność Isigtand itp) +4
    * VrX: wykonalne, ale sporo czasu (20+ godzin) i duży koszt surowcowy
* 2 - Marcel przesłuchuje niewolnika z Orbitera
    * Tr Z (kij i marchewka) +3 + 5Or (acts of violence)
    * VrXzVOr: dezerter, nie chce wracać na Orbiter, Marcel zna historię i zmusił do powiedzenia prawdy. Marcel go złamał.
* 3 - Daria naprawia Hadiah Emas swoimi ludźmi zgodnie z planem
    * Tr Z (fabrykacja + kompetentni ludzie + wsparcie TAI Mirtaela) +3:
    * VVzVz: odpięcie, odbudowanie i naprawienie. Bezproblemowa naprawa, nawet bez używania części z Isigtand.
* 4 - 
    * 
    * 
* 5 - 
    * 
    * 
* 6 - 
    * 
    * 
* 7 - 
    * 
    * 
* 8 - 
    * 
    * 
* 9 - 
    * 
    * 
* 10 -
    * 
    * 


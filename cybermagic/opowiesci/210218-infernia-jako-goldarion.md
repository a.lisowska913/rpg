---
layout: cybermagic-konspekt
title: "Infernia jako Goldarion"
threads: legenda-arianny
gm: żółw
players: kić, fox, kapsel
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [210331 - Elena z rodu Verlen](210331-elena-z-rodu-verlen)

### Chronologiczna

* [210331 - Elena z rodu Verlen](210331-elena-z-rodu-verlen)

## Punkt zerowy

### Dark Past

* .

### Opis sytuacji

Planetoida Asimear jest:

* planetoidą o średnicy mniej więcej 90 km 
* znajdującą się w okolicach Pasu Teliriańskiego, w obszarze Planetoid Kazimierza.

By dość szybki statek mógł dotrzeć do Pasa Teliriańskiego, potrzeba mniej więcej 10 dni drogi z Kontrolera Pierwszego.

Planetoidy Kazimierza są uważane za miejsce o tyle interesujące, że znajduje się tam nadmierne stężenie materiałów anomalnych i rzadkich. Wszystko wskazuje na to, że powinna się tam znajdować jakaś dziwna Anomalia Kosmiczna - a jednak nic nie zlokalizowano. Tak więc po wielu badaniach uznano, że jednak jest to miejsce stosunkowo bezpieczne. Działają tam grupy ekstrakcyjne i nomadyczne statki-kolonie, z własnym "rządem", który akceptuje obecność Orbitera, ale nie traktuje ich jak władców sektora astoriańskiego.

Dzisiejszosesjowa planetoida, Asimear, jest dość istotną planetoidą z perspektywy ekonomicznej; wydobywane są tam rzadkie minerały, m.in. astinian - komputronium, wykorzystywane przy produkcji TAI. Innym jest morfelin, czyli "programowalna materia" - materia zdolna do zmiany kształtu pod wpływem energii sprzężonej z konkretnymi rozkazami.

By wydobywać minerały z Asimear, nad planetoidą znajduje się statek-platforma-miasto. Ta platforma nazywa się Lazarin. Tam znajduje się większość populacji mieszkającej i obsługującej wydobycie z Aesimar. Jednocześnie na samej Asimear znajdują się grupy osób mieszkających tam, "tubylców", którzy nie są powiązani z Lazarin (albo chcą działać niezależnie od Lazarin).

Dodajmy do tego fakt, że w okolicach Asimear znikają czasem ludzie, bo "noktiańscy komandosi nie skończyli wojny" i mamy już pełny obraz sytuacji.

Na to wszystko pojawia się Jolanta Sowińska z Aurum w nietypowym servarze podobnym do Entropika, ale większym. Do tego - Kramer podejrzewał, że działania tam różnych postaci są powiązane z próbą programu kosmicznego Aurum i uniezależnienia się od Orbitera. Wysłał więc swojego advancera, by ten rozpoznał teren. Advancer jednak zniknął. To wymaga użycia Inferni...

### Postacie

* Jolanta Sowińska: dziwna neurosprzężona Sowińska; postrach Tomasza. Kiedyś go kociła.
* Tomasz Sowiński: młody, przerażony mag który ma podejście "jesteśmy męscy". Nie wie jak się zachowywać w towarzystwie nie-Sowińskich. Dość nierozsądny.

## Punkt zero

N/A

## Misja właściwa

Admirał Kramer wyjaśnił Ariannie sytuację: zniknął mu advancer w okolicach planetoidy Asimear. Jest tam Jolanta Sowińska, nie wiadomo czemu. Najpewniej Aurum/Sowińscy próbuje się uniezależnić od Orbitera, co niekoniecznie jest w interesach Orbitera. A fakt, że w okolicach Asimear wydobywany jest astinian i morfelin jedynie oznacza, że Jolanta Sowińska ma dostęp do programowalnej materii oraz komputronium. Gdy Arianna spytała czemu **ona** - przecież ona ma potencjalnie kosę z Sowińskimi, Kramer wyjaśnił, że Arianna będzie się więc bardzo starała by nikt nie wiedział że ona to ona :-).

Kramer załatwił, że statek prywatny przetransportuje na Asimear jakąś sporą paczkę zamówioną przez Jolantę. Traf chciał, że tym statkiem jest Goldarion. Czyli Infernia będzie udawać Goldarion i weźmie ze sobą na pokład Tomasza Sowińskiego i dwóch jego ochroniarzy. Ok...

A jak sytuacja wygląda politycznie?

* Aurum chce własną, niezależną od Orbitera flotę
* Orbiter (a dokładniej admirał Termia) sabotuje te plany
* Wszyscy eksperymentują

No dobra.

Pierwszy krok - Infernia musi się zamaskować... nie za Goldariona. Za coś innego. I zamaskowała się jako "Czarny Grom" - statek Termii XD. Klaudia ciężkimi staraniami (xxVVV) sprawiła, że Infernia dostała zadanie w okolicach Valentiny by się w rekordach wszystko zgadzało - Infernia jest "gdzieś tam". Plus odzyskała informacje jak wygląda Czarny Grom (VxV).

Arianna poszła na całość - wynajęła Goldariona. Po krótkiej rozmowie z Klaudią doszła do tego kim jest Aleksander Leszert i jaki jest prawdziwy stan Goldariona (ruina). Goldarion został wynajęty by mieć transponder Inferni i zostać naprawiony w dokach niedaleko Valentiny. Oczywiście, jeśli ktoś z Goldariona piśnie chociaż słówko... to Arianna spuści na nich Eustachego.

Tymczasem Eustachy zamaskował Infernię jako Goldariona, robiąc solidną makietę. To było sporo roboty z mechanikami Kramera, w jednym z odleglejszych doków na K1, ale się udało zrobić to niezauważenie. Niestety, makieta trochę się sypie. Z jednej strony pasuje do Goldariona (który się sypie). Z drugiej... 

(jako efekt uboczny: pojawiła się plotka, że "Goldarion" jako statek nie istnieje i nie istniał - pod tą nazwą zawsze kryją się jakieś tajne działania admirał Termii)

Jako, że Goldarionem dowodzi facet, Eustachy udaje Aleksandra Leszerta a Arianna jego pilota. Martyn zadbał o to, by wszyscy wyglądali właściwie - jest w końcu mistrzem biokonstrukcji. Jako bonus, Elena prawie popłakała się ze śmiechu patrząc na Eustachego w garniturku kapitana.

W końcu na "Goldariona" (Infernię) dotarła Tajna Ekranowana Skrzynia wraz z Tomaszem Sowińskim. Sowiński jest... aparatem, nie da się ukryć. "kobiety nie umieją pilotować" i "my, prawdziwi faceci". Jego ochroniarze to twardzi, muskularni faceci. Widać, że Tomasz nie do końca umie z kobietami ;-).

Na samym "Goldarionie" (Inferni) Sowiński odesłał swoich ochroniarzy, by się rozeszli. W końcu prawdziwy facet nie potrzebuje niańki. Gdy Arianna (jako pilot) odprowadzała Sowińskiego do swoich kwater, próbowała go wypytać o to, jak bardzo Sowińscy mają kosę z Arianną Verlen. Odpowiedź trochę zwaliła ją z nóg.

**Arianna Verlen to postać fikcyjna**. Tomasz Sowiński wyjaśnił zdumionej Ariannie, że owej fikcyjnej "Ariannie" przypisuje się różne zasługi różnych postaci z Orbitera. Przecież nie ma opcji, by ta sama osoba nie poradziła sobie ze świniami a jednocześnie robiła tak niesamowite rzeczy. Jego zdaniem aktorka jest fajna (choć zbyt sztywno się ubiera). Owszem, istnieje aktorka rodu Verlen, która jest we flocie - ale najpewniej siedzi w papierach i gra bohaterkę.

No, to jest nowe XD.

Arianna postanowiła dowiedzieć się więcej od Tomasza o Jolancie i wszystkim innym. Nie ma prostszego sposobu niż... picie z Leoną. Więc kazała Leonie być dla Tomasza miłą, po czym wzięła Tomasza do baru. Tam Tomasz spotkał się z Leoną i jak tylko Arianna zdeeskalowała sprawę (XXvV) gdy Leona powiedziała Sowińskiemu, że jest PIERWSZYM członkiem swego rodu wyglądającym choć marginalnie kompetentnie, to potem już poszło łatwo. Leona wkręcała Tomasza w picie, Tomasz pił a Arianna słuchała.

* Tomasz Sowiński wyciągnie Ariannę ("Monikę") z tego bagna. Ma kompleks paladyna. "Monika" jest jedynym jasnym punktem na Goldarionie, poza swoim kapitanem i Tomasz Sowiński weźmie ją na admirała swojej floty
* Tomasz Sowiński się zakochał w Ariannie...

No cóż, tego się nikt nie spodziewał. Pierwsza kobieta, która się nim zainteresowała nie dbając że jest bogatym arystokratą. A jakie ma relacje z kobietami?

* Jest mega nieszczęśliwy. Kocą go. Jolanta zawsze go kociła, nie miała do niego czasu ani głowy.
* Jolanta jest bardzo zdecydowaną osobą. Ma dziwny servar, zmodyfikowany Entropik? Nie, klasa Tirakal, służy do "budowy bazy", z TAI Morrigan.
* Przekazał Ariannie sześciopak brudnych sekretów na różne damy rodu Sowińskich.

A na czym to kocenie polegało? Ano, Tomasz chciał być fajny a Jolanta wpakowywała go w kłopoty. Jest wyraźnie bardziej inteligentna od niego i bywa złośliwa...

Elena zatrzymała tą imprezę. Weźmie do siebie Tomasza i zadba, by nic mu się nie stało, bo już Leona patrzyła nań łakomym kąskiem w stylu "co by mu tu jeszcze w życiu spieprzyć". Arianna oddała go Elenie. Wszystko lepsze niż Leona...

DOBRA. Następnego dnia, jak już Tomasz jest spity jak bela (i utrzymywany w stanie nietrzeźwości i szczęśliwości, co pasuje wszystkim łącznie z ochroniarzami) to Arianna podeszła do problemu Tajemniczego Pudełka. Pudełko, opakowanie ma własną TAI defensywną. Ta ma zapewnić, że tylko osoby autoryzowane mogą pudełko otworzyć. Miły uśmiech Arianny sprawił, że:

* Tomasz UCIEKNIE Z TAI Z PUDEŁKA I ONA BĘDZIE JEGO DZIEWCZYNĄ
* Persefona nałożyła chaos na TAI z pudełka

W wyniku TAI chroniąca pudełko została całkowicie ogłupiona; zarejestrowała, że Tomasz użył kodów awaryjnych i odblokował sekretną przesyłkę.

Sekretną przesyłką jest zmodyfikowany servar klasy Entropik. Ale to nie jest zwykły servar - ten ma broń antynanitkową. Klaudia szybko sprawdziła co wie na temat oryginalnego servara Jolanty, tego klasy Tirakal - TAI Morrigan jest krzyżówką Eszary i Elainki, skupiona na budowie bazy, jednostek i taktyki. To TAI sterująca rojem dron. Więc... czyżby kluczem był morfelin? Sentisprzężenie + nanitki? Jedno jest pewne - trzeba dowiedzieć się jak najwięcej z tego Entropika, który wysyłany jest Jolancie.

Kto jest najlepszą osobą integrującą się z jednostką klasy Entropik? Elena. Wsadzili Elenę do Entropika, ze wsparciem Persefony. (XXXvvXX) -> Elenę trzeba było z tego cholerstwa wyciągać. Ta konkretna TAI klasy Elainka jest niesamowicie mocno zabezpieczona przed przejęciem kontroli, kompatybilne tylko z Jolantą, co więcej - więcej outsourcuje na użytkownika niż zwykle. Tak jakby Jolanta mogła więcej niż inni magowie. W wyniku działań Eleny i jej adaptacji - Entropik jest kompatybilny TYLKO z Eleną, ale jej nie słucha, Jolanta musiałaby ją autoryzować.

Tak więc ten. Zbrickował się XD.

## Streszczenie

Infernia udaje Goldariona, by przetransportować tajną przesyłkę Sowińskich do Jolanty Sowińskiej na planetoidzie Asimear. Tomasz Sowiński na Inferni okazuje się być dużym dzieckiem, łatwo jest wmanipulowany w otwarcie paczki - jest tam nietypowy antynanitkowy Entropik (którego psuje Elena przez błędną integrację). W tle wszystkiego - próba zdobycia niezależnej od Orbitera floty przez Aurum i jeden znikający advancer Kramera na Asimear...

## Progresja

* Arianna Verlen: okazuje się, że przez wiele osób z Aurum uważana jest za postać fikcyjną. Tzn. może istnieje, ale jest aktorką w "Sekretach Orbitera". WTF.
* SCA Goldarion: dostaje potężny refit w okolicach Valentiny (potrwa to 2 miesiące)
* SCA Goldarion: pojawiła się plotka, że nie jest to statek rzeczywisty; "Goldarion" jako statek nie istnieje i nie istniał - pod tą nazwą zawsze kryją się jakieś tajne działania admirał Termii.
* Tomasz Sowiński: odblokował przesyłkę dla Jolanty Sowińskiej. Będzie wiadomo, że to on. Solidny wpiernicz od Jolanty się będzie należał?
* Tomasz Sowiński: zakochał się w Ariannie jako "Monice", która uratowała go przed Leoną, dbała o niego i w ogóle była miła, inteligentna i kompetentna. I nie śmiała się z niego. Chce ją wyciągnąć.

### Frakcji

* .

## Zasługi

* Arianna Verlen: udaje pilota Goldariona; zdziwiła się, że podobno nie istnieje. Przypadkowo rozkochała w sobie jako "Monika" Tomasza Sowińskiego, pewnego, że Arianna nie istnieje.
* Eustachy Korkoran: udaje dowódcę Goldariona; przebudował Infernię w Czarny Grom a Czarny Grom w Goldariona. Zdobył szacun Tomasza Sowińskiego, który go uważa za swego chłopa.
* Klaudia Stryk: repozytorium wiedzy na temat TAI, doszła do tego jak działa Morrigan i że mogą mieć przeciw sobie servara sterującego nanitkami.
* Elena Verlen: JUŻ NIE MIROKIN; chciała się zaopiekować biednym Sowińskim by ten nie cierpiał od Leony (wbrew sobie), jednak skończyła w szoku i medbay po integracji z dziwnym Entropikiem.
* Leona Astrienko: świetnie się bawiła upijając Tomasza Sowińskiego i kombinując, jak najlepiej spieprzyć mu życie.
* Martyn Hiwasser: przekształcił używając swoich bioskilli wygląd Eustachego w Aleksandra Leszerta, dowódcę Goldariona. W ogóle, dba, by wszyscy wyglądali jak nie-Infernia.
* Antoni Kramer: stracił advancera na Asimear i wysłał Infernię, by go znaleźli. Plus cośtam z Sowińskimi i potencjałem na flotę Aurum.
* Tomasz Sowiński: udaje twardego i męskiego, ale to przerażony duży dzieciak skonfrontowany z wszechświatem. Zakochał się w Ariannie ("Monice"), bo jest pierwszą kobietą w jego życiu XD.
* Jolanta Sowińska: nie pojawiła się jeszcze; kociła w przeszłości Tomasza Sowińskiego. Do niej przesłano zaawansowany antynanitkowy Entropik. Sama używa Tirakal/Morrigan.
* OO Infernia: oficjalnie w okolicach Valentiny i ma refit. Faktycznie - udaje Goldariona i leci w kierunku planetoidy Asimear.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria, Orbita
                1. Kontroler Pierwszy
            1. Pas Teliriański
                1. Planetoidy Kazimierza: istotne ekonomicznie planetoidy, zawierające sektor prywatny i sporo rzadkich minerałów
                    1. Planetoida Asimear: tam gdzieś, pomiędzy platformą a samą planetoidą zniknął advancer Kramera. I gdzieś tam jest tien Jolanta Sowińska. Źródło minerałów: astinianu i morfelinu.

## Czas

* Opóźnienie: 17
* Dni: 15

## Inne

### Projekt sesji

brak

### Sceny sesji

brak

### Idea sesji

brak ;-)

### Co po sesji

* starzy wrogowie Goldariona. Np. Leszert wisi im kasę. "mieliście nie przylatywać"
* Jolanta Sowińska
---
layout: cybermagic-konspekt
title:  "Kluczowe daty"
categories: inwazja, konspekt
---

KODOWANIE: rrrrmmdd

* Pęknięcie Rzeczywistości:     00010714
* Przebudzenie Saitaera:        00790321
* Inwazja Noctis:               00800917
* Zmiażdżenie Inwazji Noctis:   00820704
* Aktualna chronologia:         01090724

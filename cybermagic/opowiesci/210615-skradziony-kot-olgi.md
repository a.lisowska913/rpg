---
layout: cybermagic-konspekt
title: "Skradziony kot Olgi"
threads: rekiny-a-akademia, waśnie-samszar-verlen
gm: żółw
players: anadia, kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [210518 - Porywaczka miragentów](210518-porywaczka-miragentow)

### Chronologiczna

* [210518 - Porywaczka miragentów](210518-porywaczka-miragentow)

### Plan sesji
#### Co się wydarzyło

.

#### Sukces graczy

* .

### Sesja właściwa
#### Scena Zero - impl

.

#### Scena Właściwa - impl

W lesie pomiędzy Zaczęstwem a Podwiertem, mniej więcej tam gdzie strzelano do Marysi, ale jest tam taka "chata". Taki budynek. Budynek jest dość ufortyfikowany. "Stop, nie wchodzić" itp. Marysia + Julia zaadaptowały ten budynek. Ten budynek jest Placówką Karaoke. Dobrze ukryty, ufortyfikowany - czemu nie korzystać.

Mamy imprezę karaoke na jakieś 15 osób. Marysia i Julia - jedyne czarodziejki. Wszyscy się dobrze bawią. Nikt z władz nic nie wie, co jest podwójnie fajne. Niestety, Marysia wyczuwa niedaleko erupcję energii magicznej. Wszystko wskazuje Marysi na coś nowego - to nie jest "erupcja". To walka magów o dużej mocy. Walczą na pełną moc. To niebepieczne.

Marysia i Julia wsiadają na ścigacz Marysi i lecą w kierunku wyładowań magicznych. Julia odpala drony od Triany (Triany z nimi nie ma) i puszcza je na azymut - kto z kim walczy. Co tu się dzieje.

TrZ+3o+2:

* XX: drony Triany atakują!!! WSZYSTKICH! (wojowników)
* V: Julia i Marysia mają wizję i audio

Potwory mityczne vs ścigacz Rekina. Wyraźnie walka Iluzjonisty vs Rekin. NAGLE ATAKUJĄ DRONY! WSZYSTKICH! Rekin "lepsza niż myślałem". Iluzjonistka "TAK się bawisz?" Oboje uznali, że to wina drugiej strony. Ścigacz pocięty przez drony, bo myślał, że są FIKCYJNE!

Ścigacz się oderwał i odlatuje. "Tak to się nie skończy. TAK TO SIĘ NIE SKOŃCZY!!!". Iluzjonistka krzyknęła "tchórz! Atakujesz tylko słabszych!"

Marysia poznała Rekina - to Marek Samszar. Poczciwy, dość spokojny Rekin. Zwykle nie wadzi nikomu. Zdecydowanie walczył z przeciwnikiem silniejszym od siebie.

* XX: Drony wpadły w berserk pod wpływem magii i dołączyły do "dzikich istot w okolicy". Tam była za duża nienawiść pomiędzy tą dwójką.

Tego wieczoru Julia i Marysia oddały się karaoke, miejsce dyskretne zostało uratowane... i czas zająć się Markiem (który miał być na karaoke a nie dotarł >.>)

NASTĘPNY DZIEŃ

Podwiert. Wielki Hotel Rekinów. Marysia idzie odwiedzić Marka. A ten jest poobijany i wściekły. W pokoju z nim jest dziewczyna. Młoda Rekinka, od niedawna. Wygląda na twardą. "Cześć Marek, dziewczyna Cię pobiła?" Marek, że się nie da. A ona... myśli.

Młoda Rekinka przedstawiła się jako Arkadia Verlen. "Cieszę się, że mogę Cię poznać. Jestem Twoim wrogiem."

Marysia chce porozmawiać z Markiem sama, Arkadia się nie zgadza. Chce walczyć z Marysią o prawo rozmowy z Markiem sam na sam. Marek się patrzy na Marysię i traci kolor z twarzy.

Marek próbuje przekonać Arkadię, by ta zostawiła go z Marysią. Arkadia postawiła ultimatum. Marek powiedział, żeby jednak Arkadia wyszła. Arkadia zerwała z Markiem - "jest cały Twój". 

Marek nie dotarł na karaoke (tłumaczy się Marysi), bo został zaatakowany przez Lilianę. Liliana Z JAKIEGOŚ POWODU go chce kocić. Ale on już wziął sprawy w swoje ręce. Poskarżył się Justynianowi Diakonowi.

SYTUACJA:

* Justynian Diakon - Rekin, dość silny mag bojowy
* Marek Samszar - Rekin, tępi go Liliana. Chce móc iść na karaoke i nie być ofiarą XD. 
* Arkadia Verlen - Rekin (od niedawna), eks-dziewczyna Marka, wojowniczka jak cholera, wroga Marysi (ale nie atakuje)
* Liliana Bankierz - AMZ, tępi Marka, porywcza, bez granic, iluzjonistka + kinetka

Marysia ma dość. Czemu Liliana go atakuje? To nie jest "typowa Liliana". Albo Marek jej wszystko powie, albo Marysia powie Arkadii że Marek ją pocałował i chciał ją pocałować!!! Marek się przestraszył.

TrZ+3:

* V: Marek się przyznał. Miał starcie z magami AMZ. Dokładniej, z Pawłem Szprotką. Paweł chciał ukraść kota. ŻYWEGO kota. Marek dał kota Arkadii a Paweł chciał ukraść kota Markowi. Więc Marek go SKROIŁ. Marek jest groźniejszy od Pawła. I najpewniej Paweł poskarżył się Lilianie...

A Arkadia ma teraz kota. Marek nigdy nie widział tak pięknego kota - więc go "pozyskał z naturalnego środowiska" i przekazał Arkadii. A kot był z hodowli.

Marek KOMUŚ ukradł kota. Dał go Arkadii. Paweł chciał ukraść kota Markowi. Marek zbił Pawła. Liliana zbiła Marka. A kot jest u Arkadii.

Hodowla jest "lokalsów", zdaniem Marka nie mają znaczenia. A Paweł jest z AMZ i jest lokalsem.

Paweł powiedział Marysi skąd wziął tego kota - jest taka hodowla niedaleko, w Czarnopalcu. Prowadzona przez "czarownicę". Nawet miejscowi jej unikają. Marysia wie już DOKŁADNIE jak zlokalizować tą hodowlę. Marysia kazała Markowi odzyskać kota SMSem do Arkadii - ta odpisała "kot jest mój, Sowiński jest Twój."

Marysia powiedziała Julii na czym polega sytuacja. Marek ukradł magicznego kota i dał go Arkadii. Marek nie ma pojęcia że to magiczny kot. A Arkadia nie lubi Marysi XD. 

Julia, będąc w Zaczęstwie, przeszła po AMZ do biblioteki. Czym jest ten cholerny kot? Co ma Arkadia? Co zajumał Marek??? Julia włącza w wyszukiwanie skany po obrazach itp - pattern matching.

* X: Spotkanie z Lilianą.
* V: Somnibel. Pożeracz dusz.
    * Kot, który żywi się snami, wspomnieniami itp. 
    * W zależności od wychowania najbardziej smakuje mu konkretny TYP uczuć i snów. 
    * Najlepiej nie spać w jego obecności - wtedy jest dużo słabszy (wpływ). 
    * Żywi się też aurą i emocjami. 
    * Poluje na upiory, duchy, byty astralne...
    * Może żywić się adoracją, zmartwieniami itp itp. 
    * Ma własności uzależniające i hipnotyczne.

Julia przepytuje Lilianę o jej wersję wydarzeń. TrZ+2.

* X: Liliana jest zapalona do zemsty za Pawła.
* V: Liliana powie:
    * spotkała płaczącego Pawła. Paweł był STRASZONY, on ma na coś fobię i Rekin (Marek) go straszył i nagrywał.
    * Liliana stwierdziła, że Rekinowi trzeba pokazać gdzie jego miejsce. Zaatakowała z zaskoczenia.
    * Rekin miał drony. Dał radę ją wymanewrować.
    * Następnym razem Liliana go dorwie.

Liliana jest nieprzekonywalna do zaniechania czy spowolnienia ZEMSTY. Julia ciężko westchnęła. Liliana buduje z Myrczkiem bombę grzybową (cokolwiek to jest)...

JULIA IDZIE ODWIEDZIĆ PAWŁA.

Paweł Szprotka jest sympatyczny, nie wadzi nikomu, trochę z boku... zupełnie nie jest przebojowy ale jest sensowny. Powiedział, że próbował odzyskać kota dla Olgi - bo ktoś Oldze Myszeczce ("Czarownicy z Czarnopalca") ukradł kota. A kot to żywe stworzenie i nie wolno go kraść - to trauma zarówno dla posiadaczki (Olgi) jak i dla kota.

Julia wyciąga co Paweł robił w Czarnopalcu. Dostała jedynie potwierdzenie, że faktycznie tam był.

TrZ+3:

* XX: Paweł jest ŚWIĘCIE PRZEKONANY, że Liliana opowiada wszystkim jak to on zbił Marka.
* XX: Paweł boi się o swoje życie (a przynajmniej kości) jak się do niego dobiorą Rekiny... musi działać.
* V: Paweł powiedział Julii coś ciekawego
    * ona nie jest "czarownicą z Czarnopalca" a jest osobą, która go sponsorowała do AMZ.
    * był u niej z wizytą towarzyską. Lubią się. A dokładniej, relacja "uczeń - mistrz".
    * to nie jest kot. To somnibel. Miał pomóc dzieciakom z traumami. Tak był hodowany. Jest niegroźny i bardzo poczciwy.
* X: Paweł OBIECAŁ JUŻ Oldze że odzyska kota. Jeśli to się nie stanie, będzie ŹLE. Paweł nie chce powiedzieć o co chodzi, ale jest nerwowy.

Paweł i Julia + Marysia poszli wspólnie do Arkadii Verlen. "Właścicielki" kota. Marysia przyleciała po Zespół i polecieli razem. Paweł jest wyraźnie skrępowany że musi się przytulać do Julii XD. I Paweł nie wie kim jest Marysia. Ot, jakaś Rekinka.

Paweł też się trochę rozluźnił. Opowiedział jakąś bardzo niewłaściwą i śmieszną opowieść o farmie gnoju i glukszwajnach...

Dotarli do Wielkiego Hotelu Rekinów.

Pokój Arkadii. Wpuściła.

Somnibel rzuca się szczęśliwy na Pawła. Ten oburzony - somnibel jest głodny!!! Czy Arkadia nie ma żadnych traum czy co XD.

Dziewczyny i Paweł próbują przekonać Arkadię, że kot jest ukradziony, miał służyć do pomocy dzieciom. I jest głodny, bo Arkadia nie ma dość traum by nażywić czterołapczaka. TrZ+3.

* V: Arkadia odda somnibela Pawłowi.
* V: Arkadia zadba o to, by taka sytuacja się już nigdy nie powtórzyła.
* XX: Arkadia idzie nuklearnie przeciwko Markowi - nie są parą i rodzice są poinformowani o tym że Arkadia się NIE żeni ze złodziejem kotów.
* V: Arkadia zdeeskaluje Lilianę. Weźmie Marka i...

Arkadia podziękowała za informację. Odda kotka, oczywiście. Lilianę weźmie na siebie, musi tylko z kim ma do czynienia, muszą się spotkać. Julia spotkała Lilianę i Arkadię NASTĘPNEGO DNIA na Nieużytkach Staszka.

4 osoby: Marek, Liliana, Arkadia, Julia

Arkadia biła Marka aż Liliana krzyknęła, by ta przestała XD. Arkadia spytała, czy Liliana jest usatysfakcjonowana? Tak. Wszyscy patrzą na Arkadię z przerażeniem. Ta mówi do Marka zniecierpliwiona, że przecież nic mu nie zrobiła - nie biła do poważnych uszkodzeń. Chciała wziąć ze sobą Marka, ale on już nie chciał z nią lecieć. Poleciała sama.

WIECZÓR. Oddanie kota.

Olga odebrała kota z uśmiechem i radością. Zaprosiła ich na herbatkę. Niewiele mówiła, to Paweł mówił za trzech. Za drzwiami, w innym pokoju był ukryty Wiktor Satarail - nie pokazał się, ale PAWEŁ WIEDZIAŁ...

## Streszczenie

Marysia i Julia mają imprezę karaoke w Lesie Trzęsawnym, ale Marek nie dotarł - walczy na magię z Lilianą. Julia puściła drony Triany, które wpadły w berserk. By Tymon się nie zainteresował tym co się dzieje w lesie, Marysia i Julia chciały rozwiązać problem - okazało się, że Marek ukradł somnibela Oldze Myszeczce i dał go Arkadii Verlen w prezencie. Marysia i Julia przekonały Arkadię do oddania kota, oddały go Oldze, po czym Arkadia zerwała z Markiem i go ciężko pobiła. Acz Paweł Szprotka (który próbował odzyskać kota od Marka i którego broniła Liliana) boi się o życie, bo niektóre Rekiny się na niego uwzięły za karę...

## Progresja

* Arkadia Verlen: zerwała z Markiem Samszarem i go solidnie pobiła za kradzież somnibela Oldze Myszeczce.
* Marek Samszar: Arkadia Verlen z nim zerwała i go solidnie pobiła za kradzież somnibela Oldze Myszeczce.
* Paweł Szprotka: Rekiny na niego polują by zemścić się za to, że Rekin nie jest całkowicie bezkarny na tym terenie.

### Frakcji

* .

## Zasługi

* Marysia Sowińska: ratuje swoje karaoke deeskalując sytuację w Lesie Trzęsawnym oraz odkrywa, że Marek ukradł somnibela Oldze. Przekonała Arkadię do oddania somnibela.
* Julia Kardolin: zaczyna od używania dron Triany (kiepski pomysł, wpadły w berserk), szuka danych o somnibelu w bibliotece AMZ i ogólnie pozyskuje informacje od uczniów AMZ.
* Marek Samszar: Rekin. Chciał zrobić dziewczynie (Arkadii) piękny prezent i zwinął "nieważnym lokalsom" somnibela dla Arkadii. Potem chciał lecieć na karaoke i skończył w starciu z Lilianą. Chciał eskalować Justynianem Diakonem, ale skończył pobity przez Arkadię za kradzież.
* Arkadia Verlen: Rekin. Dziewczyna Marka, który dał jej ukradzionego kota i rywalka (wróg?) Marysi Sowińskiej. Jak się dowiedziała o ukradzionym somnibelu, oddała go i ciężko pobiła Marka, zrywając z nim.
* Liliana Bankierz: nie akceptuje tego, że na Pawle znęca się Samszar. Weszła z nim w starcie magiczne w Lesie Trzęsawnym. Przygotowuje walkę i zemstę przeciwko Samszarowi. Zneutralizowana przez okrucieństwo Arkadii na Samszara.
* Paweł Szprotka: AMZ. Ma relację uczeń-mistrz z Olgą; jest uciekinierem eternijskim (fakt nieznany) którego Olga wyprowadziła z dziczy, nauczyła jak funkcjonować i dała mu rekomendację do AMZ. Próbuje odzyskać somnibela ukradzionego Oldze i za to Rekiny go tępią.
* Olga Myszeczka: przygotowuje somnibele by je sprzedawać / oddawać i pomagać ludziom po traumie. Jednego somnibela ukradł od niej Marek Samszar. Dobrodziejka Pawła Szprotki i jego promotorka do AMZ.
* Wiktor Satarail: znajdował się na zapleczu domku Olgi Myszeczki. Gdyby Olga nie odzyskała swojego somnibela, Wiktor by go dla niej odzyskał.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Podwiert
                                1. Las Trzęsawny
                                    1. Schron TRZ-17: ufortyfikowany, przestrzenny schron przeciw burzom magicznym pełniący rolę "Chatki Karaoke" dla Rekinów i AMZ. Prowodyrka - Marysia.
                                1. Dzielnica Luksusu Rekinów
                                    1. Obrzeża Biedy
                                        1. Hotel Milord: Marysia spotkała się tam z Arkadią Verlen i Markiem Samszarem.
                            1. Czarnopalec
                                1. Pusta Wieś: Olga m.in. hoduje tam somnibele, które mają się żywić traumami ludzi i je sprzedaje/oddaje. Odwiedzona przez Marysię i ekipę.
                            1. Zaczęstwo
                                1. Nieużytki Staszka: miejsce spotkania Arkadii i Marka z Lilianą, Julią i Marysią, gdzie Arkadia solidnie pobiła i skrzywdziła Marka za kradzież kota i zrobienie z niej złodziejki.

## Czas

* Opóźnienie: 5
* Dni: 2

## Inne

.

## Konflikty

* 1 - Julia uruchamia drony Triany by zobaczyć kto z kim walczy w Lesie Trzęsawnym. Intencja - co tu się dzieje (recon)
    * TrZ+3o+2:
    * XXV: drony atakują WSZYSTKICH wojowników i odpędzają ścigacz Marka (który nie dotarł na karaoke).
    * XX: Drony wpadły w berserk pod wpływem magii i dołączyły do "dzikich istot w okolicy". Tam była za duża nienawiść pomiędzy tą dwójką.
* 2 - Marysia przepytuje Marka - czemu Liliana go atakuje i zaczepia?
    * TrZ+3
    * V: Marek się przyznał. Miał starcie z magami AMZ. Dokładniej, z Pawłem Szprotką. I Paweł się na pewno poskarżył Lilianie. Paweł chciał mu ukraść kota którego on ukradł
* 3 - Julia w bibliotece AMZ szuka informacji o dziwnym kocie. Co to jest, jak on może działać
    * TrZ+2
    * XV: info, że Somnibel. Plus spotkanie z Lilianą.
* 4 - Julia próbuje od Liliany dowiedzieć się o co chodzi
    * TrZ+2
    * XV: Liliana jest NIEPRZEKONYWALNA odnośnie zemsty, Marek znęcał się nad Pawłem i go kocił. Liliana planuje z Myrczkiem zrobić bombę grzybową.
* 5 - Julia wyciąga od Pawła, co ten robił w Czarnopalcu
    * TrZ+3:
    * XXXXVX: Paweł panikuje że Rekiny się na nim zemszczą (co jest możliwe). On lubi Olgę. Próbuje odzyskać somnibela. Już obiecał Oldze że go odzyska.
* 6 - Marysia, Julia i Paweł przekonują Arkadię do oddania somnibela.
    * TrZ+3
    * VVXXV: Arkadia odda kota, zapewni by to się nie powtórzyło, idzie nuklearnie przeciw Markowi i zdeeskaluje Lilianę.

---
layout: cybermagic-konspekt
title: "Sabotaż świni"
threads: legenda-arianny
gm: żółw
players: kić, kapsel, fox
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [201021 - Noktianie rodu Arłacz](201021-noktianie-rodu-arlacz)

### Chronologiczna

* [201021 - Noktianie rodu Arłacz](201021-noktianie-rodu-arlacz)

## Punkt zerowy

### Dark Past

_odsyłam do 200916_

### Opis sytuacji

Stan aktualny sytuacji:

* **AMEND: dodajmy Iok-sama, szlachcica który z oddziałem kilkudziesięcioosobowym spróbuje uratować Trzeci Raj przed Elizą Irą.** -> do przyszłej sesji.

### Postacie

Frakcje: i reprezentanci

* Eliza Ira, królowa kryształów
  * chce: zniszczyć wpływ bogów, zdobyć Ataienne
  * ma: Marian Fartel, krystaloformy, moc arcymaga
* Zbigniew Morszczat / Miragent, źródło chaosu
  * chce: doprowadzić do kompromitacji i unieszkodliwienia Trzeciego Raju
  * ma: zainteresowanie nojrepów, miragent
* Robert Garwen, strażnik Raju
  * chce: by Raj przetrwał niezależnie od czegokolwiek
  * ma: przemyt, posłuch
* Celina Szilat, psychotroniczka Termii
  * chce: zobaczyć pełną moc Ataienne, usprawnić Persefony
  * ma: kontrolę nad Persefoną, skille terminuski, lekki servar klasy Jaeger (adv. Lancer).
* Bartosz Gamrak, Grzymościowiec z Wolnego Uśmiechu
  * chce: utrzymać Trzeci Raj, rynek zbytu Skażonych TAI
  * ma: przemyt, scrap-mechy, małą bazę w Ortis-Conticium
* Ataienne, mindwarp Raju
  * chce: odepchnąć Seilię, uratować Trzeci Raj, uwolnić się
  * ma: mindwarp
* Kordelia Sanatios, arcykapłanka Seilii
  * chce: postawić świątynię Seilii w Trzecim Raju
  * ma: zwolennicy, możliwość przetransportowania kasy
* Trzeciorajowcy nienoktiańscy, mieszkańcy Raju
  * chce: poszerzyć kontrolę nad okolicą, być bezpieczni
  * ma: większość populacji Trzeciego Raju
* Izabela Zarantel, dziennikarka Orbitera
  * chce: jak najlepszej opowieści do Sekretów Orbitera
  * ma: umiejętności zwiadu i dobre opowieści
* Anastazja Sowińska, niestabilny arcymag
  * chce: samodzielności, wolności i swobody.
  * ma: niesamowitą moc Paradoksu
* Nikodem Sowiński, kuzyn Anastazji
  * chce: .
  * ma: .
* Juliusz Sowiński, 'rycerz pokonujący smoka'
  * chce: .
  * ma: .

## Punkt zero

brak.

## Misja właściwa

Sabotażysta wie, że Zespół na niego poluje. Tak więc Arianna wpadła na chytry plan - zrobi tak, by przeciwnik myślał, że Arianna znalazła błędnego sabotażystę. Jej plan jest prosty - Wieprzka na orbitę, po czym się schować i zacząć szukać gdzie jest prawdziwy sabotażysta. Tak więc Arianna rozpowiedziała, że wie już kto sabotował Trzeci Raj - to byli ludzie Dariusza.

Garwen nie uwierzył Ariannie - przecież sabotaż miał miejsce ZANIM w ogóle Dariusz się pojawił w okolicy. Arianna włączyła urok 200% i sprawiła (VXV), że nie będzie kwestionował; musi być tak jak ona mówi. Ale Garwen stracił zaufanie do decyzji Arianny - no przecież się serio pomyliła. To niemożliwe.

No nic. Arianna była przekonywująca, nawet sabotażysta (miragent) uwierzył, że Arianna to łyknęła. Miragent w chwili obecnej maskuje się jako Rafał Armadion, delikwent od świń (co pozwala mu zamaskować swoją sygnaturę miragenta przed magami i wszystkim co nie jest Ataienne).

Tak więc gdy zaczęła się operacja ładowania świń na pokład "Wesołego Wieprzka", "Armadion" zaproponował Eustachemu i Ariannie żeby pomogli mu w ładowaniu. Oboje magowie się wzdrygnęli, pozwalając miragentowi na wprowadzenie silnie zdestabilizowanego wieprza udającego glukszwajna - viciniusa, który eksploduje jak tylko będą zbyt silne wstrząsy lub pole magiczne zostanie pasywnie wyżarte przez glukszwajny.

Trzeba było jeszcze upewnić się, że wszystko zadziała. Arianna z Eustachym opracowali plan "desantu" - jak tylko Wieprzek wystartuje, statek "będzie miał problemy". Kilka świń wypadnie ze statku, w konfuzji i zamieszaniu Arianna, Klaudia i Eustachy opuszczą statek. Plan jest niezły, teraz wykonanie. Przede wszystkim - Arianna kazała kapitanowi Wieprzka zrobić ze sobą porządek. Jest w jej flocie? Jest. Ma polecieć ze świniami na orbitę. Niechętnie, ale kapitan się na to zgodził.

Następnie Klaudia siadła do TAI Semla obsługującej Wieprzka. Zapisała w Semli ukrytą wiadomość do Kramera i troszkę wzmocniła Semlę swoimi anomaliami - ale jednocześnie przygotowała rdzeń przyszłej destrukcji statku.

Wreszcie nadszedł czas startowania. Wieprzek wystartował. Eustachy rozpoczął protokół "świniobombardowanie".

* O: URATUJEMY WSZYSTKICH! NIE MOGĘ BYĆ GORSZY! NAWET ŚWINIE! Eustachy pod wpływem poświęcenia Eleny, działań Arianny itp. obudził w sobie iskrę paladyna. Raz.
* XX: Niestety, nikt nie spodziewał się... że Wieprzek już jest niebezpieczny. Eustachy wyrzucał świnie...
* V: ...i przez przypadek wyrzucił anomalną świnię. ŚWINIA WYBUCHŁA. Są ranni, ofiary. Przypadkowy sukces.

Ale statek anomalizuje. Anomalia Klaudii zdominowała OO "Wesoły Wieprzek". Reklasyfikacja do AK "Wesoły Wieprzek" - załoga na pokładzie została zespolona i zintegrowana ze statkiem a sam statek zaczął mieć... cele. Głód energii. Zaczyna odlatywać (ze świniami i wszystkim XD) w kierunku na Elizę Irę - a Arianna, Eustachy i Klaudia są na ziemi, wraz z Martynem robiącym co może dla rannych.

* Arianna próbuje swoją mocą opóźnić infekcję anomaliczną statku (XXO): niestety, sama się zaraziła. Nie da rady (X), anomalia wygrywa.
* Klaudia próbuje przekonać anomalię, żeby ta tego nie robiła, łącząc się z resztkami Semli (XXX) - bez powodzenia. Czymkolwiek ta anomalia była, jest zbyt groźna i dzika.
* Eustachy używa Diany jako baterii i używa pełni mocy zniszczenia (VVXOX) - uwolnił Ariannę i część załogi, ale nie dał rady wyratować glukszwajnów.

Gdy już się wydawało, że AK Wesoły Wieprzek opuści ten teren, otworzyły ogień działa Trzeciego Raju. Ataienne strąciła anomaliczny statek (VV), po czym zamanifestowała się pełną mocą (V). Ataienne wyczyściła załogę z anomalii (V), po czym skutecznie usunęła wszystkie nagrania na temat tego co zrobiła (XXX). Arianna próbowała zapamiętać, ale nawet ona nie była w stanie oprzeć się straszliwej mocy Ataienne (X).

Dla Zespołu to oni osiągnęli sukces, Ataienne nigdy tu nie było.

Dobra. Stan aktualny:

* Wesoły Wieprzek jako statek kosmiczny został doszczętnie zniszczony. Arianna nie dostanie nowego statku do floty.
* Załoga Wieprzka jest odratowana.
* Oficjalnie - wszyscy nie żyją. Arianna z pomocą Ataienne fabrykują dowody na śmierć Zespołu.

Teraz - Klaudia rozpoczyna badanie świni. Coś się tu poważnie spieprzyło. Jak dało się podłożyć świnię tego typu ekspertowi? Klaudia doszła do tego - to jest żywa świńska bomba, konkretnie zaprojektowana przez eksperta. Czyli jedynym możliwym sabotażystą jest Armadion. Ale czemu? Więcej, tego typu działanie by zabiło człowieka lub maga. Konkluzja jest prosta - mamy do czynienia z cholernym miragentem Orbitera...

Arianna skontaktowała się z Anastazją. Chciała, by Anastazja wywabiła miragenta z miasta. Niech Anastazja zacznie ludzi badać pod kątem krwi itp - to wtedy uda się im znaleźć miragenta. ALE NIESTETY! ANASTAZJA BYŁA ZA SKUTECZNA! ZNALAZŁA I ARESZTOWAŁA CHOLERNEGO MIRAGENTA, nie wiedząc czym on jest.

Arianna podkradła się do więzienia, jednak za późno - miragent zabił strażnika i zmienił w niego swój wygląd. Arianna mocą magiczną przesłała do miragenta kody kontrolne, wyłączając go doszczętnie. I udało się Ariannie dowiedzieć kilku rzeczy:

* TAK, miragent pochodzi od Termii. Ona chciała mieć oko na Ataienne.
* Miragent został skorumpowany przez komodora Orbitera, który z całego serca nienawidził noktian. Termia się nim zajmie...

Ale... w Trzecim Raju nie ma dość świń. Operacja... nieudana? Cóż...

## Streszczenie

Sabotażysta - miragent - podjął próbę zniszczenia Arianny gdy ta ładowała świnie na Wesołego Wieprzka używając wybuchowej, anomalnej świni. Zespół przetrwał, choć "Wieprzek" został zniszczony. Arianna pojmała miragenta i przekazała go do Kontrolera Pierwszego, acz kosztem ofiar w ludziach. No i nie było dość świń by je wziąć na orbitę XD.

## Progresja

* Robert Garwen: stracił trochę opinii na temat Arianny Verlen; musiała się pomylić odnośnie sabotażystów i jej błędne decyzje doprowadziły do śmierci niewinnego astorianina.
* Arianna Verlen: nie będzie mieć "Wesołego Wieprzka" we flocie. Statek zanomalizował i został zniszczony przez Ataienne. Też: nie dała rady ze świniami.
* Izabela Zarantel: ALBO poprawi reputację Anastazji (która Iza w sumie uszkodziła), ALBO Sowińscy wsadzą ją do karceru na długie lata.
* Anastazja Sowińska: opinia w Trzecim Raju: plan Arianny był dobry, ale Anastazja źle go wykonała. Anastazja ma w Raju złą opinię jako niekompetentna arystokratka Aurum.
* OO Wesoły Wieprzek: statek zanomalizował i został zniszczony przez Ataienne w Trzecim Raju. KIA.

### Frakcji

* .

## Zasługi

* Arianna Verlen: po tym, jak Klaudia zlokalizowała miragenta udało jej się go wyłączyć. Też: próbowała mocą złamać Anomalicznego Wieprzka - bez powodzenia. Też: nie odparła mindwarpa Ataienne.
* Klaudia Stryk: zanomalizowała TAI Semlę na Wesołym Wieprzku, co doprowadziło do Skażenia tej jednostki; dzięki badaniu wybuchowej świni udało jej się namierzyć miragenta.
* Eustachy Korkoran: opanowany żądzą bycia paladynem przypadkowo uratował załogę Wieprza przed sabotażem miragenta; potem używając Diany jako baterii zdekombinował anomalię i zmusił ją do odlotu.
* Martyn Hiwasser: ratował na lewo i prawo ludzi po wybuchu sabotowanej eksplozywnej świni.
* Diana Arłacz: żywa bateria energii magicznej, by Eustachy mógł uratować Ariannę i załogę anomalicznego Wesołego Wieprzka.
* Anastazja Sowińska: aresztowała miragenta... przez co zginął astorianin pilnujący aresztanta. Trzeci Raj nie jest pod jej pozytywnym wrażeniem.
* Robert Garwen: dał się przekonać Ariannie, że sabotażyści pochodzą z "Szalonego Rumaka", choć z oporami. I dobrze że z oporami, bo Arianna ściemniała.
* Ataienne: zestrzeliła działkami Trzeciego Raju anomalnego Wieprzka, po czym użyła pełni mindwarpa by wyczyścić ofiary anomalnego Wieprzka. I nikt nic nie wie.
* Rafał Armadion: jako Rafał maskował się sabotujący miragent, co oznacza, że Rafał nie żyje od pewnego czasu. KIA.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski, NW
                    1. Ruiniec
                        1. Trzeci Raj

## Czas

* Opóźnienie: 3
* Dni: 3

## Inne

Planowana sesja (już po raz ostatni):

* Na pokładzie kaledik + 24 glukszwajny. Walka z kaledikiem na statku kosmicznym. NAJPEWNIEJ nie ma crashland.
* Kramer żąda, by Zespół jak najszybciej doszedł do tego co i jak. Muszą unieszkodliwić Celinę i mają pomoc Ataienne.
* Miragent podrzuca kryształy wskazujące na Elizę, ale Klaudia bez problemu dojdzie do tego, że to NIE ONA.
* Znaleźć miragenta. Miragent ma rozkaz śmierci. Ataienne rekomenduje Zespołowi, by zostawili jej dyskretnie miragenta.

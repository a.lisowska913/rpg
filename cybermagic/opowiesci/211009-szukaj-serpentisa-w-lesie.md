---
layout: cybermagic-konspekt
title: "Szukaj serpentisa w lesie"
threads: mlodosc-klaudii
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [210926 - Nowa Strażniczka AMZ](210926-nowa-strazniczka-amz)

### Chronologiczna

* [210926 - Nowa Strażniczka AMZ](210926-nowa-strazniczka-amz)

### Plan sesji
#### Co się wydarzyło, historycznie

* W okolicach Trzęsawiska Zjawosztup pojawił się oficer Saitaera. Naprzeciwko niemu stanął Wiktor Satarail - i stał się częścią Trzęsawiska.
* Noctis oddelegowało 'ON Tamrail' z eskortą by zapewnił bezpieczeństwo tego obszaru. Tamrail został Skażony mocą Saitaera i rozpadł się na terenie zwanym w przyszłości Nieużytkami Staszka.
* Siły Noctis znalazły się w krzyżowym ogniu między mocą Saitaera i Astorii. Ich bazą stało się Czółenko (stąd Esuriit w przyszłości), główne walki toczyły się o obszar Nieużytków Staszka i elementów Lasu Trzęsawnego.
* Aż Noctis zostało pokonane. Wycofali się, biedni, na Trzęsawisko Zjawosztup - gdzie już rekonstruował się Wiktor Satarail.
* Doszło do masakry...
* Teraz jesteśmy 711 dni po oficjalnym zakończeniu wojny i zmiażdżeniu sił noktiańskich. 
* Ciężko uszkodzona jednostka 'koordynacyjno-dowódcza' Ślepacz została podstawą areny migświatła. Nadmierna moc psychotroniczna na potrzeby.

#### Co się wydarzyło, na tej sesji

* Druga Strona
    * Agostino, noktiański serpentis (komandos po bioformowaniu) ma pod swoją opieką dwóch innych noktian. Uciekli z Trzęsawiska i schowali się w Lesie Trzęsawnym. Wężowiec jest mistrzem niewykrywalności. Jest jedyną osobą w zespole zdolną do pełnosprawnego działania. Edelmira gaśnie - Agostino MUSI jakoś ją uratować.
    * Agostino nie ma pomysłu ani planu. Zostawił Edelmirę z Udomem w tymczasowej bazie. Szuka JAKIEGOŚ rozwiązania... i wyczaił zbliżających się astorian...
* Klaudia
    * Brakuje WSZYSTKIEGO. Więc zdobyć medykamenty z medbunkra.

#### Sukces graczy

* żaden ważny NPC nie zginie
* Agostino zostanie przechwycony przez Pustogor

### Sesja właściwa
#### Scena Zero - impl

.

#### Scena Właściwa - impl

Piękny poranek. Ładna pogoda, choć może padać. Klaudia ma dzisiaj MISJĘ. Klaudia (17), Ksenia, Felicjan i Mariusz, eskortowani przez dwóch żołnierzy mają zadanie do wykonania dla Pustogoru - w Lesie Trzęsawnym, całkiem niedaleko jest medbunkier. Podczas wojny były zapasy medykamentów. Ale wojny już nie ma a medykamenty by się przydały, zwłaszcza, że odbudowywany jest Szpital Terminuski. Klaudia ma otworzyć bunkier (ma odpowiednie kody i uprawnienia), Ksenia ma wybrać odpowiednie przedmioty a chłopcy mają pomóc nieść ;-). Tactical nie przewiduje niebezpieczeństwa - nie powinno nic złego tam się dziać - ale anomalie nie śpią i mimo nalegań Ksenii, że sobie poradzą dostali 2 żołnierzy :-).

Operacją dowodzi Felicjan Szarak, uczeń terminusa, a żołnierze dowodzą jeśli coś się stanie. Dokładniej to Czerw.

Ksenia jest szczęśliwa. AKCJA! Coś się dzieje! Jest rozszczebiotana. Trzewń podekscytowany - MILITARY SECRETS. Felicjan, 100% powagi, próbuje być "godnym dowódcą". Żołnierze ukrywają uśmiechy. Pozwalają mu dowodzić. Klaudia (która się super przygotowała) idzie tam, bo jest ciekawa jak takie coś wygląda. Plus, Klaudia jest im potrzebna poza wejściem do zbadania czy nie doszło do anomalizacji czegoś - ona jest dobra w detekcji tego typu rzeczy.

Klaudia się szarpnęła. Załatwiła porządny pojazd (prowadzi Dariusz). Taki, by móc wywieźć rzeczy. Pojazd jest ATV i ma opcję podnoszenia. Wyruszyli mając zapas żywności na 2 dni (awaryjnie). Bo diabli wiedzą - lepiej być gotowym. Centrala zapowiedziała, że jak nie wrócą do wieczora, przyślą wsparcie. Plus - problem, niech dadzą znać. Acz z uwagi na specyfikę nie do końca odbudowanego hipernetu i Strażniczkę mogą być momenty z osłabionym kontaktem.

Klaudia zabrała dronę Strażniczki do kieszeni. Dobra okazja do nauczenia Strażniczki jak wygląda sytuacja. TAK, Strażniczka nie zintegrowała w pełni. Ale - jest to coś przydatnego, zawsze Klaudia może zrobić FORCE COMMS jak trzeba Strażniczce pokazać coś fajnego. Plus, Klaudia umie drony używać.

Podróż do lasu zajęła 40 min. Miny, niebezpieczeństwa itp. No i ATV. W samym lesie jechali - ostrożnie - 30 min. Całą drogę Dariusz opowiadał historie o duchach. Dokładniej - zaczął, zdobył zainteresowanie Ksenii "czyli duchy pomordowanych noktian...", Dariusz się świetnie bawi aż Grzegorz dał mu lekko po głowie "nie strasz czarodziejki i skup się na jeździe". Dostali chichot Ksenii, spojrzenie pełne wyższości ze strony Felicjana i adorację Trzewnia "PRAWDZIWI ŻOŁNIERZE!". A Klaudię zaciekawiła sytuacja i opowieść, wolała pobawić się w tym czasie z droną i jej szkieletowym AI (które ma dzięki Talii). 

Dotarli do techbunkra. Astoriański techbunkier w lesie Trzęsawnym. Żołnierze rozstawili perymetr, motion scanner (choć w lesie... cóż, nieperfekcyjne). Trzewń zapamiętuję procedurę, Ksenia podrywa niezgrabnie Dariusza a Felicjan zwraca się do Klaudii "Czarodziejko, medbunkier czeka. Pokaż, co AMZ potrafi.". Na formalizm żołnierze znowu schowali uśmiech, co Ksenia zinterpretowała za flirt ze strony Dariusza, co dla odmiany bardzo rozbawiło Grzegorza.

* Strażniczka: "Interesujące. Nie rozumiem relacji między tymi jednostkami. W jakiej są relacji?"
* Klaudia: "później..."

Wejście jest przysypane liśćmi i ma standardowe systemy zabezpieczeń. Głównym z nich - bez energii magicznej nie da się tego otworzyć. To wyłączyło wszystkich noktian ;-). Klaudia widzy przy konsolecie wyraźne ślady prób włamania i przebicia, ale medbunkier wytrzymał - jest zabezpieczony.

Klaudia podbija, chce wejść do bunkra używając kodów. A potem - diagnostyka. I drona leci przodem.

TrZ+3:

* V: udało się. Klaudia skutecznie otworzyła bunkier i puściła wstępną diagnostykę. Wszystko wygląda OK. Mogą być problemy z automatami pomocniczymi; wielokrotne próby ataku i hackowania spowodowały, że część defensyw jest w trybie paranoi. Hestia jest uszkodzona.
* XV: Hestia nie pamięta czym jest. Poważnie uszkodzona pamięć. Sprawna defensywnie i chce chronić, nie niszczyć, ale nie pamięta przed czym chroni. Jeśli dostaniemy się głęboko, można Hestii pomóc - zwłaszcza przy użyciu Strażniczki. Hestia nie jest w perfekcyjnym stanie, została poważnie uszkodzona psychotronicznie, ale jest... naprawialna tymczasowo. Czyli można zrobić "window of lucidity". Kody rozpoznała, ale nie do końca rozumie co oznaczają. Tyle, że nie ma ochoty "strzelać do Zespołu". Czyli - jak długo nic nie dotkniecie, możecie spokojnie wejść.

Klaudia wyjaśniła Zespołowi sytuacje. Proponuje naprawić Hestię. Grzegorz wie, że podczas wojny noktiańskiej TAI było wykorzystywane do obrony więc by z nim rozmawiać dano przymus energii magicznej - tego noktianie nie mają. Więc Grzegorz zaproponował - magowie do bunkra, oni na perymetrze. Trzewń chce zostać na perymetrze. Został. A Klaudia weszła do bunkra z innymi magami.

Bunkier jest ciemny. Hestia zapaliła światła. Wyraźnie nikogo tu nie było. Jest konsoleta wsparcia; Hestia nie dopuści nikogo do AI Core. Klaudia wyciąga dane z systemów odnośnie tego co jest w środku i daje je Ksenii. Ksenia przełączyła się z trybu "flirt" na tryb "praca" i w pełni skupiła się na danych, acz jeden impuls Ksenii został na czujności. Felicjan w trybie "CHRONIMY KLAUDIĘ". Poważny i z kijem w tyłku.

Klaudia wysyła do Hestii sygnał "stand down, we're safe". Dzięki wiedzy od Talii odnośnie TAI Klaudia wie, jak sobie poradzić z uszkodzoną i lekko przestraszoną TAI.

TrZ+4:

* V: Hestia wyłączyła wszystkie defensywy o których pamięta i które są pod jej kontrolą, co powiedziała Klaudii z lekkim zawstydzeniem. Dała zrzut defensyw. Klaudia widzi, że Hestia nie kontroluje pewnej ilości działek i robotów bojowych. A resztę ma. Choć nie zawsze pamięta jak ich użyć.

(Ex) Klaudia poprosiła Hestię o kontrolę nad defensywami. Hestia CHCE jej pomóc, ale nie do końca pamięta jak. Klaudia de facto trochę "zagania" Hestię ścieżkami o których Hestia nawet nie pamięta.

* X: Zajmuje to dużo czasu. Ponad godzinę. (+2Vg). Ksenia w tym czasie przygotowuje katalog rzeczy a Felicjan rozkłada perymetr i mapuje bliskie Klaudii fragmenty bazy.
* V: Klaudia przejęła wszystkie defensywy Hestii jeśli ich potrzebuje. Hestia nie kontroluje defensyw, Klaudia tak. Czyli nie ma opcji - Klaudia ma 70% siły ognia medbunkra a "uszkodzone jednostki" tylko 30%. Klaudia jakby chciała może nawet zdobyć AI Core i wyłączyć Hestię.

Minęło jakieś 80 minut. Klaudia mówi Felicjanowi, że jako, że defensywy są wyłączone, można ściągnąć żołnierzy itp. Felicjan się zdziwił. Oni czekają? Pójdzie po nich, bo to głupie. Klaudia założyła, że czekają bo nie mogą zejść. Klaudia spytała Felicjana czemu nie ma połączenia, checkupu itp. Felicjan - bo przez nie do końca sprawną Hestię i silne ekranowanie bunkra po prostu nie mają połączenia. Klaudia powiedziała Felicjanowi, że może spróbować udrożnić komunikację, ale to stare systemy, ugryzione zębem czasu i uszkodzone. Felicjanowi spodobał się ten pomysł. Może dzięki temu da się ten bunkier ODZYSKAĆ. Nie szabrować, wyłączyć a odzyskać.

* V: Klaudia wysunęła antenę i odzyskała komunikację i wizję dookoła bunkra. Stary, nieaktywny i zmaltretowany bunkier odżył.

I Klaudia dokonała dwóch niepokojących odkryć:

* Jest zagłuszona. Something jamms them.
* Nigdzie nie widać żołnierzy ani Trzewnia. Ale pojazd dalej tam jest.
* Nie ma kontaktu ze Strażniczka (jamming).
* ALE jest ta baza i ten bunkier. I tu są "rudimentary controls". Czyli Klaudia ma możliwości działania. Po prostu stąd.

Na podstawie charakterystyki sygnału i wszystkiego co Klaudia wie o tym sprzęcie itp, zagłuszanie jest zagłuszaniem military-grade i pochodzącym ze sprzętu tych żołnierzy. Ten pojazd. Ich własna technologia. Felicjan jest bardzo zaniepokojony. Felicjan podejrzewa wpływ Trzęsawiska. Jeśli tak - muszą się ufortyfikować i czekać do nocy. Ale jest ryzyko stracenia trzech osób...

TRZEWŃ próbował ostrzec i wysłać sygnał do Klaudii mimo zagłuszania gdy zorientował się co się dzieje:

ExZ (niepełna karta) +2:

* Xz: Serpentis wie, że Trzewń jest magiem. Zorientował się.
* Vz: Trzewń dał radę zostawić wiadomość. Wiedział, jak ją zamodulować by Klaudia wiedziała czego szukać a bunkier mógł ją przechwycić. Ukrył ją w pojeździe na "echu".
* V: Trzewń dał radę przekazać wiadomość POPRAWNIE. Wszystko co wykrył, w dobry sposób. Przestraszony, ale wykonał zadanie zanim stracił przytomność. 

Klaudia analizując sygnały z bunkra znalazła zakodowaną MAGICZNĄ wiadomość Trzewnia w pojeździe. Zaklęcie enchantujące pojazd. I zobaczyła co Trzewń wie.

* Ruch. Bardzo szybki. Mężczyzna. Uzbrojony. Noktiańska broń. Nie-człowiek, albo bardzo zmodyfikowany człowiek. Trzewń wysłał kod "noktiański serpentis".
* Noktiański serpentis jest obszarpany, ma zaleczone domową metodą rany, nie ma amunicji (walczył makeshiftową rzeczą).
* Serpentis unieszkodliwił obu żołnierzy - broń gazowa. Trzewń wolał wysłać schować się w pojeździe i wysłać sygnał niż z nim walczyć - ale serpentis był szybszy.
* Serpentis dorwał Trzewnia zanim ten dobiegł do pojazdu i włączył zagłuszanie. Zmusił Trzewnia do pójścia z nim i sam wziął dwóch żołnierzy. Trzewń musiał "puścić" zaklęcie bo nie ma zasięgu.
* Szli w kierunku lasu. W konkretną stronę. Serpentis zorientował się, że Trzewń jest magiem gdy ten puścił czar i go ogłuszył.

Felicjan dowiedział się tego co wykryła Klaudia. Opieprzył Trzewnia cichymi słowami za to, że ten zrobił głupotę. I sam Felicjan wpadł w głębokie zamyślenie - jak Felicjan, Klaudia i Ksenia mają wyciągnąć od serpentisa (biomodyfikowanego komandosa noktiańskiego - to jest człowiek, ale nie ta liga co nasi żołnierze; raczej jak terminus czy neurowspomaganie) dwóch Grzegorza, Dariusza i Trzewnia.

Klaudia - niech drona z bunkra wyłączy zagłuszanie. Druga drona jako oczy. Klaudia próbuje do tej drony wyłączającej zagłuszanie dodać kawałek papieru i bardzo kalekim noktiańskim: "wojna się skończyła, porozmawiajmy, leki za jeńców". Przyczepia do drony z karteczką też jakiś komunikator.

I Klaudia wysłała dronę. Jej cel - wyłączyć zagłuszanie LUB znaleźć karteczkę.

Felicjan powiedział Klaudii coś ważnego - jeśli drzwi bunkra są "niezamknięte", można złapać i przymknąć. Ale to też znaczy, że Klaudia nie chce wysyłać drony - bo bunkier nie działa. ALE TRZEWŃ WIE, że Klaudia ma dronę. Żołnierze też, po podśmiewali się trochę ze "zwierzątka". Więc Klaudia może wysłać jedną dronę na przeszpiegi. Po prostu nie może chwalić się że ma więcej. Więc - Klaudia musi, niestety, używać detekcji medbunkra a nie drony zapasowej - bo serpentis ma świetne zmysły.

Klaudia wysyła dronę z bunkra. Liczy na to, że serpentis nie wie o bunkrze (BO PRZECIEŻ WSZYSCY - Trzewń, żołnierze - wiedzą, że bunkier nie działa). Klaudia widzi, jak serpentis przechwycił dronę, ruszając się bardzo szybko i wyginając ciało w sposób jakiego człowiek nie umie zrobić. Na widoku bunkra widać wyraźnie, że serpentis jest wychudzony, ma infekcję... nie wiodło mu się. Ma też rany od magii. Przechwycił dronę, przeczytał wiadomość... poszukał czegoś do pisania, nie ma. Klaudia widzi zawahanie przed użyciem krwi, ale nic innego nie ma.

Na odwrocie karteczki coś napisał krwią. Zamontował na dronie, podszedł ostrożnie do bunkra i rzucił dronę do środka. Klang, klang, klang. Poobijana drona -> w środku.

Klaudia nie czyta dobrze noktiańskiego, ale poznała piktogramy. Cyfra 3. "Wyjdziecie albo oni zginą. Co 15 minut (piktogram) odcinam kawałek ciała. Lub wszyscy zginą." (3, zegar 15 min, nożyczki, dłoń i palce, strzałka czaszka). Wziął komunikator. **ALE ON NIE DZIAŁA BO ZAGŁUSZANIE**. Najpierw Klaudia się tyci zmartwiła "bo co on pomyśli". Zdaniem Felicjana to dobrze - bo uzna ich za amatorów. Ksenia zauważyła, że nimi są.

Felicjan zaproponował pewien plan - on zrobi potężną dywersję a Klaudia wyśle swoją dronę na prowizorycznym TAI z "sos, serpentis". By przechwycili tą dronę terminusi.

Ksenia: "a może damy radę się z nim dogadać?" Felicjan: "to noktianin". Ksenia zaproponowała, że jako że on potrzebuje medyka to ONA pójdzie i da się mu złapać. Felicjan się nie zgadza. Jeśli on przesłucha Ksenię, to wszystko się wyda. Klaudia na to, że będzie już za późno.

Klaudia do Ksenii - niech ona ma wszystko co może mu pomóc w rękach. Żeby nie było sięgania po cokolwiek. Ksenia - dobry pomysł. Ksenia skorzystała z toaletki by wyglądać młodziej (ma 17, ale chce wyglądać na 15) na maksymalną przestraszoną niewinność. BO ONA SIĘ FAKTYCZNIE BOI. Ale jest medykiem. I jej zdaniem jest najmniej istotna - Klaudia kontroluje bunkier i jest kluczowa a Felicjan ma taktykę, pomysły i najlepiej walczy. A ona jako medyk może tam najlepiej pomóc. Plus, jak trzeba, zaopiekować się "swoimi".

Klaudia programuje dronę Strażniczki by ta się poruszała jak kot, dyskretnie i leciała w kierunku Strażniczki. TAI dostarczone przez Talię poradzi sobie z tym problemem.  Klaudia do drony dołączyła pełen status report, łącznie z podejrzeniami że niekoniecznie jest w pełni wrogi; niekoniecznie trzeba rozstrzelać. Sama pisze wiadomość do noktianina "współpracuję z Talią Aegis, wojna się skończyła". Do paczki niesionej przez Ksenię - zasoby bunkra i racja żywnościowa. 

A Ksenia, bardzo rozsądnie, doszyła symbol medyczny do swojego stroju. I przygotowała białą flagę.

OPERACJA: KSENIA. Konflikt - czy Ksenia odpowiednio odwróci uwagę. Czy przekona serpentisa by nie naciskał mocniej, czy ten nie wykryje drony.

ExZ+3+3Vg (Klaudiowe programowanie drony):

* Vz: dzięki maksymalnie szokującemu obrazowi prawie-nieletniej-Ksenii-w-trybie-medyka udało jej się przemycić dronę za plecami gdy szła w kierunku lasu.
* Vz: przekona serpentisa do odstąpienia od złapania pozostałej dwójki. Acz dużym kosztem.
* Vg: Ksenia nie spanikowała. Nie dała się całkowicie zastraszyć. Nie jest sterroryzowana. Jest pokorna, słucha się, ale nie jest sterroryzowana.

Klaudia wystawiła "tą oficjalną pogruchotaną dronę" by widzieć i by serpentis widział że widzi. Serpentis wyszedł w kierunku Ksenii. Ta się uśmiechnęła i poszła w jego kierunku. Serpentis przechwycił ją przy jednym z drzew. Pomachał do drony z lekkim uśmiechem. Ksenia się rozpromieniła. Wziął nóż. Ksenii uśmiech zniknął. Pokazał na bunkier. 2. Mają wyjść. Ksenia pokręciła głową. Nie. Pokazała na medical. Pokazała na emblemat medyczny i na siebie.

Serpentis wziął nóż, przyłożył do twarzy Ksenii. Ta zaczęła dygotać. Powoli, zimno, zaczął zbliżać nóż do jej oka. Ksenia zamknęła oczy i zaczęła płakać. Tak się trzęsła, że prawie straciła równowagę. "Medyk. Chcę pomóc, ale muszę widzieć!". Przyłożył jej nóż do palca...

Felicjan chce tam iść. NIE MOŻE SKRZYWDZIĆ KSENII! Klaudia - "nie. Jeśli wyjdziesz i zaatakujesz, Tobie się oberwie i jej krzywda. Ksenia wiedziała co ryzykuje."

Klaudia przekonuje Felicjana. NIE IDŹ TAM!

TrZ+4 (niepełna):

* V: Klaudia przekonała Felicjana. On WIEDZIAŁ że nie może tam iść. Ale... potrzebował to usłyszeć. On... nie może zostawić Ksenii. Ale musi.

Ksenia straciła kontrolę nad pęcherzem. Gdy noktianin zorientował się, że nikt nie przyjdzie i nie wyjdzie z bunkra, fuknął. Lekko uderzył Ksenię w plecy, pokazując, gdzie ma iść. Pokazał dronie, że ich monitoruje i na nich patrzy. Co dało Klaudii i Felicjanowi bardzo ważną wiadomość - on jest w pobliżu. Musi być. Albo blefuje. Ale nie mógłby Trzewnia czy żołnierzy przenieść bardzo daleko...

Test dla DRONY. Czy dronie uda się bez problemu przedostać przez niebezpieczny las Trzęsawny by Strażniczka poznała sytuację.

TrZ+2:

* Vz: TAI Talii, przeznaczone jako przydatny 'pet' okazało się być godnym zwierzątkiem. Strażniczka dowiedziała się o sprawie już po 30 minutach (bo drona się czaiła).
* V: Jako, że działania Felicjana, Ksenii i Klaudii okazały się być skuteczne i sensowne, Centrala zaufała opiniom Klaudii. Spróbują pokojowego rozwiązania. Dopiero potem siła.
* V: Dane zebrane przez Klaudię są wystarczające by zidentyfikować serpentisa - to Agostino Karwen.

Czas na Ksenię.

Ksenia została doprowadzona do miejsca gdzie jest trzech serpentisów. Jedna z nich jest umierająca. Skażenie magiczne, Trzęsawisko itp. Jej organizm nie dał rady. Drugi jest niezdolny do biegania, operacji tego typu. Tylko Agostino jest w miarę sprawny. On się nimi opiekował. Edelmira jest nieaktywna, Udom pilnuje jeńców - nawet słaby serpentis sobie poradzi. Ksenia pomogła Edelmirze jak była w stanie, potem spojrzała na Udoma, dopiero potem spojrzała na resztę - bo "na oko działają". Ksenia próbowała wyjaśnić, że Edelmira zginie jeśli nie dostanie pomocy medycznej. Ona może PRÓBOWAĆ pomóc, ale może zaszkodzić. Jej moc jest niewystarczająca a ma przeciwko sobie Trzęsawisko.

Agostino i Udom nie zrozumieli o co jej chodzi, mimo, że Dariusz próbował tłumaczyć. Ale te koncepty były dla nich obce. Nie zgadzają się, by Ksenia użyła magii. Ksenia jednak nalega. Grzegorz jej nie pozwala. Ksenia wybucha - tylko to może pomóc Edelmirze. Grzegorz odbija, że Ksenia może ją zabić lub ona może sobie zrobić krzywdę. Serpentis się nie zgadzają tak czy inaczej.

Ksenia stoi przed najtrudniejszym konfliktem w swoim dotychczasowym życiu. Edelmira i Udom potrzebują szpitala; Udom też padnie bez pomocy medycznej, to kwestia tygodni lub miesięcy. Plus żołnierze... może nie lubią noktian, ale nie chcą śmierci ludzi, po prostu. Plus - wojna się skończyła. Nie trzeba ginąć bez sensu. A wiadomości Klaudii m.in. o Talii dają bardzo silny obraz tego, że noktianie w szpitalu niekoniecznie będą wykorzystani do eksperymentów (+3Vg).

Ex Z(medyk, lokalna) +3+3Vg:

* V: Ksenia przekonała ich, że Edelmira potrzebuje pomocy medycznej. Edelmira i Udom zostaną. Zostaną uratowani.
* Vz: Z uwagi na stan Edelmiry Ksenia przekonała ich, że MUSI pomóc magią.
* Vg: Ksenia powiedziała wyraźnie - ma bunkier medyczny. Tam są narzędzia. Tam jest sprzęt. Tam może ŁATWIEJ i LEPIEJ pomóc Edelmirze. Z magią i wszystkim. A ona MUSI być ustabilizowana. I Udom przekonał Agostino. On i Edelmira --> bunkier. Ksenia ma iść pierwsza to powiedzieć. Agostino został z tyłu; jeśli coś się stanie, chce to wiedzieć. Ale - puszcza wszystkich zakładników. Ksenia powiedziała, że żołnierze lepiej przeniosą Edelmirę. A Trzewń... serio? Ksenia i tak zrobi co może. I Agostino dał się przekonać.

Oczywiście, jak widać z tych testów, "zabijanie astorian" przestało wchodzić w grę.

Klaudia i Felicjan z osłupieniem widzą biegnącą do bunkra Ksenię. Nie ucieka przed czymś - biegnie do nich. Na wejściu krzyczy "nie mam bomby, jestem czysta, przekonałam ich by przyszli, mają krytycznie ranną serpen...cośtam." Ksenia szybko wyjaśniła na wydechu, że wszyscy będą wypuszczeni. Po chwili dodała, że ona tak źle wygląda i jest brudna i nie może przyjmować gości i wybuchła płaczem. Szok. Felicjan przytulił, że szok, że dzielnie się spisała, a Ksenia po prostu się rozkleiła. Klaudia szybko przekierowała Ksenię tam, gdzie ta może się wysterylizować i przebrać.

Faktycznie, Trzewń przodem, żołnierze niosą nieprzytomną noktiankę i za nimi kuśtyka słaby serpentis (Udom). Agostino zostaje na zewnątrz, jest jedynym operacyjnym.

Klaudia się nie pokazuje serpentisowi. Serpentis zauważył, że bunkier jest jakoś sprawny. Nikomu. Schowała się w AI Core.

Ksenia rozpoczęła magiczną stabilizację Edelmiry. Jedyne, co może zrobić. Ksenia już się boi, że Edelmira nie wyjdzie z tego bez szwanku... ale coś musi zrobić. Bo może. Hestia pomaga Ksenii.

ExZM+4:

* V: Edelmira będzie w lepszym stanie, niż gdyby Ksenia nic nie zrobiła.
* Vz: Z pomocą bunkra; Serpentis widzą, że Ksenia pomogła. Widzą, że Ksenia chciała im pomóc. Wierzą, że to co mówi Klaudia i Ksenia jest prawdą.

Ksenia nie jest lekarzem. Jest paramedykiem, dopiero się uczy. Używając wsparcia magii i medbunkra zrobiła co mogła by zatrzymać degenerację noktianki; ale nie jest w stanie jej "wyleczyć" czy bardziej pomóc.

Udom wyszedł i powiedział Agostino jak wygląda sytuacja - Edelmira będzie żyć i on, Udom, pójdzie z nią. Agostino powiedział, że on nie - on wróci do lasu. Jeśli faktycznie astorianie "dobrze" traktują noktian, jeśli jest dla niego miejsce na Astorii, to Udom kiedyś mu to powie i Agostino wyjdzie z lasu. Ale jeśli Udom i Edelmira nie wyjdą z tego cali i bezpieczni... Agostino ich pomści. Nie wobec tych dzieciaków czy żołnierzy - oni chcą dobrze. Ale poza nimi są "ci inni".

Agostino wrócił do lasu. Siły astoriańskie przechwyciły dwóch serpentisów i straumatyzowaną grupę odzyskiwania medykamentów z bunkra...

A Klaudia? Klaudia oczywiście przygotowała wszystkie rekordy, priorytety i mechanizmy odbudowy Hestii. Bo czymś musiała się zająć. Zwłaszcza, by nie myśleć o jednym serpentisie w lesie i jakie ma małe szanse na przeżycie - i to bez sensu...

## Streszczenie

Prosta operacja zdekomponowania medbunkra w Lesie Trzęsawnym (2 żołnierze, 4 uczniowie AMZ) skończyła się horrorem - serpentis noktiański porwał Trzewnia i 2 żołnierzy. Klaudia uruchomiła martwy medbunkier i przemyciła dronę do Zaczęstwa a Ksenia była dywersją (co skończyło się dla niej traumą). Mimo sytuacji, Ksenia przekonała serpentisów że jak się nie oddadzą w ręce astorian, jeden z nich umrze. I przekonała ich dzięki wiadomościom Klaudii świadczącym, że Talia współpracuje a nie jest w niewoli Astorii.

## Progresja

* Ksenia Kirallen: trauma związana z zastraszeniem przez serpentisa noktiańskiego (Agostino). Ale była w stanie funkcjonować poprawnie, mimo, że następne pół roku miała koszmary i problemy ze spaniem / majaki.
* Klaudia Stryk: dopóki jest w Pustogorze, ma "pet drone" z TAI poziomu 1 zrobione przez Talię Aegis. Ta drona ma połączenie ze Strażniczką Alair.

### Frakcji

* .

## Zasługi

* Klaudia Stryk: 17 lat; uruchomiła medbunkier, wysyłała wiadomości do serpentisów łagodzące sytuację (łamanym noktiańskim); wyprowadziła dronę do Zaczęstwa i zajmowała się intelligence-and-control sytuacji z perspektywy sygnałów itp.
* Ksenia Kirallen: już 17 lat (miała urodziny); MVP - zgłosiła się na ochotniczkę do przekonania serpentisa, nie dała się sterroryzować (mimo grozy) i przekonała serpentisów do oddania się w ręce Astorii. I ustabilizowała Edelmirę.
* Felicjan Szarak: 19 lat; uczeń terminusa AMZ; rozsądnie podszedł do niemożliwej sytuacji. Pomyślał o poinformowaniu centrali i nie dał się wyciągnąć groźbom serpentisa. Będzie z niego solidny terminus.
* Mariusz Trzewń: 19 lat; przechwycony przez serpentisa Agostino w Lesie Trzęsawnym, ale dał radę przekazać zapętloną wiadomość Klaudii magią przez zagłuszający pojazd. Technomanta <3.
* Grzegorz Czerw: 25; żołnierz towarzyszący magom AMZ by ich chronić. Dowodzi. Dał Felicjanowi dowodzić (dobre dla młodego maga). Dobrze rozstawił perymetr, lecz padł do serpentisa Agostino.
* Dariusz Skórnik: 26; żołnierz towarzyszący magom AMZ by ich chronić. Zna noktiański. Pokonany przez Agostino, tłumaczył potem Ksenię na noktiański i pomógł Ksenii przekonać serpentisów do leczenia Edelmiry i oddania się w ręce astorian.
* Strażniczka Alair: przechwyciła sygnały Klaudii jak tylko drona Klaudii/Strażniczki przedostała się przez zagłuszanie. Odpowiednio przekazała info Tymonowi, który przeprowadził operację ratunkową (na szczęście, nie musiał bojowej - o czym nie wiedział).
* Agostino Karwen: noktiański serpentis (bioformowany komandos) opiekujący się 2 innymi; unieszkodliwił Trzewnia i dwóch żołnierzy. Zastraszał Ksenię nożem, robiąc jej ranę na policzku i grożąc odcięciem kończyn. Ale nie chciał robić jej krzywdy - w końcu doprowadził do innych serpentisów i dał się przekonać, że to będzie dla Edelmiry i Udoma najlepsze. Wrócił do lasu.
* Edelmira Neralis: noktiański serpentis (bioformowany komandos); w stanie superciężkim i umierająca od magii; Ksenia ją stabilizowała, ale niewiele mogła zrobić. Tylko Szpital Terminuski w Pustogorze. Oddana w ręce astorian przez Udoma.
* Udom Rapnak: noktiański żołnierz; ranny i słaby. Wpierw pilnował jeńców (Trzewnia i żołnierzy), potem pomagał Edelmirze i przenieść ją do medbunkra. Zdecydował się z Edelmirą zostać i oddać w ręce astorian.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Zaczęstwo
                                1. Las Trzęsawny: odkąd trzech serpentisów noktiańskich uciekło z Trzęsawiska, udało im się znaleźć tu schronienie. Ale po tym, jak Edelmira zaczęła umierać od magii, Agostino zapolował na grupę astorian biorącą leki z medbunkra
                                    1. Medbunkier Sigma: w całkiem dobrym stanie; dość głęboko w Lesie Trzęsawnym, można tam się dostać tylko jeśli ma się dostęp do magii.

## Czas

* Opóźnienie: 140
* Dni: 1

## Konflikty

* 1 - Klaudia podbija, chce wejść do bunkra używając kodów. A potem - diagnostyka. I drona leci przodem.
    * TrZ+3
    * VXV: Hestia jest poważnie uszkodzona, ale ma 'window of lucidity'. Czyli Klaudia może udostępnić Zespołowi medbunkier.
* 2 - Klaudia wysyła do Hestii sygnał "stand down, we're safe". Dzięki wiedzy od Talii odnośnie TAI Klaudia wie, jak sobie poradzić z uszkodzoną i lekko przestraszoną TAI.
    * TrZ+4
    * V: Hestia wyłączyła wszystkie defensywy o których pamięta.
    * (promocja do Ex, +2Vg): przejęcie kontroli nad defensywami: XV: Klaudia dowodzi defensywami Hestii które ta mogła jej dać, ale zajęło strasznie dużo czasu.
* 3 - Klaudia udrożnia komunikację, ale to stare systemy. Może dzięki temu da się ten bunkier ODZYSKAĆ. Nie szabrować, wyłączyć a odzyskać.
    * TrZ+4
    * V: ma wizję, informację itp. Wie, że są zagłuszani i nigdzie nie ma żołnierzy i innych sojuszników (Trzewnia).
* 4 - TRZEWŃ próbował ostrzec i wysłać sygnał do Klaudii mimo zagłuszania gdy zorientował się co się dzieje
    * ExZ (niepełna karta) +2
    * XzVzV: Skutecznie zostawił wiadomość, zapętlając ją w pojeździe zagłuszającym i informując o serpentisie. ALE - serpentis wie że Trzewń jest magiem.
* 5 - OPERACJA: KSENIA. Konflikt - czy Ksenia odpowiednio odwróci uwagę. Czy przekona serpentisa by nie naciskał mocniej, czy ten nie wykryje drony.
    * ExZ+3+3Vg (Klaudiowe programowanie drony)
    * VzVzVg: szokujący obraz prawie-nieletniej-Ksenii + sprytna drona udało się przemycić dronę, serpentis odstąpi od przechwycenia Klaudii i Felicjana, Ksenia nie spanikowała.
* 6 - Klaudia przekonuje Felicjana. NIE IDŹ TAM; bo Felicjan chce tam iść. SERPENTIS NIE MOŻE SKRZYWDZIĆ KSENII! 
    * TrZ+4 (niepełna)
    * V: przekonała go. Wiedział, ale...
* 7 - Test dla DRONY. Czy dronie uda się bez problemu przedostać przez niebezpieczny las Trzęsawny by Strażniczka poznała sytuację.
    * TrZ+2
    * VzVV: TAI Talii dało radę, Strażniczka wie dość szybko, Centrala zaufała opiniom Klaudii i mają info o serpentisie. Szybka grupa ratunkowa.
* 8 - Ksenia stoi przed najtrudniejszym konfliktem w swoim dotychczasowym życiu. Edelmira i Udom potrzebują szpitala; Udom też padnie bez pomocy medycznej, to kwestia tygodni lub miesięcy. A info o Talii od Klaudii dają nadzieję noktianom.
    * Ex Z(medyk, lokalna) +3+3Vg (Talia)
    * VVzVg: Ksenia ich przekonała, że Edelmira MUSI mieć pomoc medyczną. I musi pomóc magią. Udom i żołnierze przeniosą Edelmirę do medbunkra, Ksenia ją ustabilizuje.
* 9 - Ksenia rozpoczęła magiczną stabilizację Edelmiry. Jedyne, co może zrobić. Ksenia już się boi, że Edelmira nie wyjdzie z tego bez szwanku... ale coś musi zrobić. Bo może. Hestia pomaga Ksenii.
    * ExZM+4
    * VVz: Edelmira w lepszym stanie niż gdyby Ksenia nic nie zrobiła; serpentis wiedzą, że co Ksenia mówi o pomocy jest prawdą.

## Inne

.
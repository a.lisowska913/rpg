---
layout: cybermagic-konspekt
title: "Narodziny paladynki Saitaera"
threads: nemesis-pieknotki
gm: żółw
players: kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [200913 - Haracz w parku rozrywki](200913-haracz-w-parku-rozrywki)

### Chronologiczna

* [200913 - Haracz w parku rozrywki](200913-haracz-w-parku-rozrywki)

## Punkt zerowy

### Dark Past

* Krystyna Senonik była człowiekiem. Policjantka w Praszalku, dowodziła.
* Krystyna wykryła konspirację między działaniami Janora i Kamarona. Janor zagroził jej dziecku, bardzo poważnie i skutecznie.
* Krystyna zaczęła pozwalać na coraz więcej odnośnie nieetycznych działań Janora. He pushed her further and further.
* Krystyna odnalazła Kult Ośmiornicy. Kult dał jej opcję "nigdy nie będziesz się już wstydzić"
* Krystyna poszła w mrok. She corrupted her own child.
* Kaskadowo, Krystyna przejęła kontrolę nad parkiem i zaczęła gromadzić i tworzyć Kult na własną rękę.
* GDYBY NIE TO, że Hestia zgłosiła obecność Grzymościowca (gdy ten uciekał przed Korupcją), to nic by nie było wiadomo.

### Opis sytuacji

* Pięknotka i Alan dali radę pokonać i przejąć grzymościowca. Ale Alan został wycofany przez Pustogor.
* Pięknotka jako wsparcie dobrała Minerwę. Nie jest może najstabilniejsza magicznie, ale ultra odporna na agenty biologiczne.
* Pięknotka chce się dowiedzieć co tu się dzieje - nie podoba jej się linia między Janorem i komendantką.
* PLAN KULTU: poświęcić Krystynę i Adama, ale zachować wszystko co się dzieje odnośnie kralotha w parku rozrywki.
* Kult ma dość sporą populację; są też nieźle uzbrojeni.
* Grzymość wysłał swojego agenta, Agatona, by ten się dowiedział co tu się dzieje.

### Dark Future

* Kraloth powróci, m.in. dzięki działaniom jednego z arystokratów Aurum - Aleksandra Muniakiewicza, Skażonego kralotycznie.
* Park Rozrywki Janora stanie się miejscem korumpującym długoterminowo.
* Aleksander i Agaton będą toczyli ostrą walkę; Agaton nie wygra.

### Istotne Postacie

* Hestia d'Janor: TAI parku rozrywkowego. Zaniepokojona firefightem z GRZYMOŚCIOWCAMI - a dokładniej, Kamaronem.
* Adam Janor: właściciel parku rozrywkowego. Ustawiony jako mastermind przez Krystynę. Bardzo wpływowy człowiek.
* Krystyna Senonik: "nothing but my hate and carousel of agony". Janor zniszczył jej wszystko co miała, zmusił do mieszkania z nim. She found the Tentacle.
* Karol Senonik: dziecko Krystyny; 17 lat. "Każda laska jego". Kralotycznie urzekający.
* Maja Janor: ustawiona jako zabawka Karola przez Krystynę i samego Karola.
* Rafał Kamaron: agent Grzymościa (Wolnego Uśmiechu); skorumpowany przez Kult. Mag, potrafi walczyć. Atm unieszkodliwiony.
* Agaton Ociegor: chemia i negacja życia.
* Aleksander Muniakiewicz: kiedyś wesoły i charyzmatyczny arystokrata; teraz niebezpieczny i bez granic; conduit Kultu Ośmiornicy.

## Misja właściwa

Pięknotka miała poważne rzeczy do zrobienia. Nie było jedną z nich konfrontowanie się ze szczęśliwym Gabrielem Ursusem i bardzo pochmurną Sabiną Kazitan. Gabriel z radością wykrzyczał Pięknotce, że Sabina złamała parol. Pięknotka OSTRO opieprzyła Gabriela za nadużycie uprawnień. Gabriel powiedział, że Sabina chciała chronić Myrczka przed wykryciem przez terminusów. Owszem, zrobił niewłaściwie. Ale ona jest winna. Ukryła ten fakt czarną magią. Gdyby straciła kontrolę... Gabriel potwierdził jeszcze raz - nadużył uprawnień, bo nie chciał, by Sabina wpadła pod dezinhibitor i kogoś zabiła.

Oczywiście, przyszedł sam, bez Sabiny, która robi swoje rzeczy. Pięknotka natychmiast wezwała Sabinę do siebie.

Sabina przyszła, elegancka jak zawsze. Potwierdziła - złamała parol, bo Pięknotka postawiła ją w niemożliwej sytuacji - z jednej strony ma chronić ten teren, z drugiej ma dbać o to, by nie złamać parolu. To co miała zrobić? Sabina zaznaczyła, że Pięknotka nie musi udawać - ona ją postawiła w tej sytuacji sama. Więc - czego Pięknotka chce? Zdaniem Sabiny, Gabriel był tam przed nią. Ona go wyczuła. Więc Gabriel zaryzykował życiem i zdrowiem Myrczka. A gdyby doszło do emisji magii krwi i Esuriit? Tak bardzo terminus... tak bardzo szlachetna Pięknotka.

I Pięknotka ma mały problem. Sabina spokojnie zaproponowała rozwiązanie - niech Pięknotka pozwoli jej odejść. Sabina nie zrobi vendetty ani żadnych działań przeciw Gabrielowi, jego siostrze, rodzinie... nikomu. Sabina jest zmęczona tym wszystkim i chce wracać. Pięknotka nie widzi powodu trzymania jej te ostatnie dwa dni, ale Pięknotka żąda od Sabiny, by ta dostarczała jej ważne dla Pięknotki informacje z Aurum. Pięknotka pozwoliła jej odejść. Sabina na odchodnym, trochę bez wiary, poprosiła Pięknotkę o skasowanie danych o jej mocach. Pięknotka się zgodziła - ale niech Sabina wie, że nie ona jedna wie o tym. Sabina wie - Gabriel też... ale na to Pięknotka nic nie poradzi. 

Gabriel prawie wpadł w furię. "Kobieta, która torturowała swoją oddaną służkę do śmierci nie zaczęła dbać o MYRCZKA". Jego zdaniem Pięknotka zrobiła katastrofalny błąd - spuściła Sabinę ze smyczy. Pięknotka powiedziała, że oni nie mogą stać się potworami. Są terminusami. (TrZ+4: XXVVV): Gabriel faktycznie odpuścił. Ani on, ani nikt z nim powiązany nie zrobi żadnych ruchów przeciw Sabinie. Pięknotka wygrała. Ale Gabriel poinformował na odchodnym Sabiny "Latające Rekiny" o tym, że ona odchodzi do Aurum. A co oni z tym zrobią...

Pięknotka dostała dwie informacje:

* Pierwsza, zaczyna się operacja eliminacji Grzymościa - zdobycie jego kompleksu. Malictrix doprowadziła kompleks do agonii. Zażądali Alana.
* Druga, są wyniki badań Grzymościowca złapanego w okolicach parku rozrywki w Praszalku. Silne środki halucynogenne i kontrolne wzorowane na kralotycznych.

Grzymościowcy UŻYWAJĄ czasem różnych środków, ale po co miałby to brać sam? Albo kto miałby mu to dać? Inni ludzie w "jego oddziale" mieli niewielką ilość tych środków, szli za magiem, bo jest magiem w servarze. Trzewń powiedział Pięknotce, że wszyscy którzy byli w tamtej okolicy muszą przejść małe badania na wszelki wypadek. Chcą sprawdzić Pięknotkę, Alana i dziewczyny. Po 30 minutach - Pięknotka i Alan są czyści. Alan, ku wielkiemu nieszczęściu, nie mógł iść do parku - szedł dewastować Grzymościa. Wyładuje frustrację.

Ale kto miałby w tym zysk? Siły Kajrata? Nie ich modus operandi, plus po usunięciu Kajrata oraz Amandy mają po prostu luki w dowodzeniu i są nieaktywni.

Pięknotka dobiera Minerwę jako wsparcie; teraz w takiej sytuacji nie chce iść tam sama. A Gabriel po ostatnim... ech. Lepiej niech Gabriel idzie z Alanem. I Pięknotka z Minerwą wróciły pod Praszalek. Ale jadą tam osobno - Minerwy nikt tam jeszcze nie widział.

Ale jak wprowadzić Minerwę do Hestii w formie niepostrzeżonej? Tam są przecież strażnicy, monitoring, to nie jest typowe miejsce. Pięknotka ma PLAN... zakłada długi trenchcoat, wchodzi do parku jak każda normalna klientka, po czym trenchcoat opada - _A PIĘKNOTKA JEST NAGO_. Plus, Pięknotka emituje środki 'pobudzające'. Chce, by to się udzieliło innym osobom, by chaos wzrósł. Celem Pięknotki jest ABSOLUTNE odwrócenie ich uwagi od Minerwy oraz na wyjściu - zwiać z parku: (XVVV). Pięknotce udało się manewrować, tańczyć, unikać, infekować miłością, odwrócić uwagę wszystkich od Minerwy. Miała WSZYSTKO pod kontrolą (V), aż poczuła innego maga. Ktoś tu jest, inny mag, kontroluje jakoś sytuację. Próbuje opanować wybuchy miłości magią i próbuje containować Pięknotkę. Terminuska się mu wyślizgnęła i zwiała do lasu - a za nią 2-3 strażników, których Pięknotka zostawiła w tyle. (V). I udało jej się tak opanować sprawę, że wyczuła, że jest atakowana ze strony lasu. AMBUSH!

Pięknotka jest całkowicie nieubrana. Po drugiej stronie jest servar. Klasyczny 'Lancer', ale wbudowany sprzęt chemiczny a nie stricte militarny. Pięknotka zrobiła głośne 'KYAAA!' by ściągnąć tu strażników (chaos is cool), oraz próbuje uniknąć ataków: (XV). Pięknotka wypadła PROSTO na strażników, rozbijając sobie o jednego buciora twarz, ale przeturlała się po terenie. Lancer wypadł na strażników, którzy w szoku, Lancer zdziwiony, Pięknotka wieje. (+XX): Servar oblał cieczą strażników, padli nieprzytomni i jest TUŻ TUŻ za Pięknotką. Żeby uniknąć oblania, terminuska wpadła prosto w "kaktusa". Poszatkowana lekko. Servar skutecznie opanował sytuację - złapał Pięknotkę. Servar żąda jej współpracy. Pięknotka używa swojej WOLI. Przyzywa Cienia.

Krew. On jej chce coś wstrzyknąć. Pięknotka się na to nie zgadza, z całego serca. Nigdy nie CZUŁA się tak bezradna. (XXOXXO). Panika. Echo. Saitaer. On powrócił...

Pięknotka dostała serię zastrzyków. Po chwili w jej umyśle pojawił się zimny rdzeń. Jego Ekscelencja Saitaer. Szepnął do Pięknotki, by złapała go za rękę - jest silny, odeprze te środki. Pięknotka złapała, jest spanikowana. Saitaer przeniknął jej żyły i jej ciało. Po chwili servar ją puścił - Cień zaatakował, kierowany szeptem Saitaera. Lancer walczy średnio. Broń chemiczna i biologiczna nie działa na nanomaszynowego Cienia. Lancer ma mało czasu do życia.

Saitaer powiedział, że jeśli Pięknotka zrobi ten jeden krok - on ją zintegruje z Cieniem i pilot Lancera przeżyje. Pięknotka jednak stanęła, nago, przeciwko Cieniowi. I wzmacnia się magią. Próbuje dotrzeć do Cienia, do jego umysłu. (OVXXVXXX). Pięknotka powstrzymała Cienia SAMA. Uratowała Lancera. Ale rany zadane przez nanomaszynowego potwora były dla Pięknotki za duże. Terminuska umarła...

...ale otworzyła oczy. Ma w sobie coś _innego_. Jej visiat działa inaczej. Jest świadoma co jest visiatem a co jest tkanką maga. Pięknotka _czuje_, że jeśli Saitaer kiedykolwiek ją opuści, Pięknotka jest martwa. Terminuska jest na pożyczonym życiu. Nigdzie nie widzi Cienia. Cień został zniszczony przez Saitaera; został zintegrowany z Pięknotką. Jako, że Cień składał się z mieszanej energii ixiońskiej i Esuriit a Saitaer nie potrafi wejść w interakcję z Esuriit, został tylko ixion.

* _"Rise, my paladin"_ - Saitaer do Pięknotki.

Pięknotka próbuje dojść do ładu z tym co się stało:

* Cień został zniszczony. Saitaer wykorzystał go do rekonstrukcji Pięknotki.
* Saitaer ma nieprawdopodobną moc w okolicy. JAK on to zrobił, jak tu?
* Pięknotka, jak Minerwa, przeszła przez drogę bez powrotu. She is not a Diakon anymore. She is an ixion mage.
* Pięknotka potrafi powoływać Cienia do woli ze swojego ciała. Jej umiejętności syntezy biologicznej się poszerzyły o dekonstrukcję.

...

A TYMCZASEM MINERWA. (TrZ+3do3: XVXVV): Udało jej się przejąć dyskretnie kontrolę nad Hestią i zwiać. Niestety, dowiedzieli się, że ona tu jest i jest magiem, ale nie skojarzyli obecności Minerwy z Hestią; myśleli, że Minerwa miała coś infiltrować i się dowiedzieć. Wiedzą, że Minerwa wie, że kontrolę nad parkiem rozrywki przejął mag. Niejaki Aleksander Muniakiewicz. Arystokrata Aurum, kiedyś "Rekin".

...

Pięknotka wydobyła maga z servara. Udzieliła mu pomocy medycznej. Zidentyfikowała go od razu - to niejaki Agaton Ociegor, mag od Grzymościa. Szybki skan biologiczny pokazał, że Ociegor jest czysty. Ma serię środków antidotalnych; z natury jest jednym z lekarzy Grzymościa. Combat medic, kiedyś z Orbitera. Najpewniej uznał, że Pięknotka jest skażona kralotycznie i chciał ją wyleczyć. Agaton będzie żył. Pięknotka ma nadzieję, że nie będzie pamiętał...

Pięknotka nanomaszynowo zrobiła sobie ubranie. Jej "skóra".

Przyszła Minerwa. Od razu zobaczyła, że coś strasznego się stało z Pięknotką. Pięknotka powiedziała, że zginęła. Saitaer ją przywrócił. Już nie jest sobą, nie jest Diakonką. Minerwa zrobiła szybki skan Pięknotki - w Pięknotce jest sporo rzeczy jakich Saitaer nie wiedział/nie wymyślił gdy pracował nad Minerwą. SZYBKO, natychmiast - trzeba Pięknotkę do Blakenbauera. Jest duża szansa, że Lucjusz jakoś będzie w stanie jej pomóc. W końcu pomaga już Ernestowi Kajratowi (wtf) i Amandzie Kajrat (DOUBLE wtf) poza nią samą, Minerwą.

## Streszczenie

Pięknotka wyprawiła Sabinę Kazitan do Aurum po akcji Gabriela, ale wymuszając na obu stronach by zaprzestały wojny ze sobą. Jako, że zaczął się atak na kompleks Grzymościa, Alan i Gabriel zostali tam przekierowani - a Pięknotka poszła z Minerwą do parku rozrywki Janor by zakończyć tą sprawę. Niestety, grzymościowiec Ociegor złapał Pięknotkę chcąc ją "wyleczyć" z kralotycznej siły. Pięknotka wezwała Cienia i ów prawie zabił Ociegora. Walcząc z Cieniem, Pięknotka umarła - i Saitaer ją przywrócił jako swoją paladynkę.

## Progresja

* Sabina Kazitan: wraca do Aurum. Jest wolna od Pustogoru.
* Sabina Kazitan: zrzeka się vendetty i jakichkolwiek ofensywnych ruchów wobec Gabriela, Lilii itp. Plus, musi informować Pięknotkę o istotnych rzeczach z Aurum dotyczących Pustogoru.
* Gabriel Ursus: zrezygnował z walki przeciw Sabinie Kazitan. Ani on ani jego ród nie będą z nią walczyć i są przed nią zabezpieczeni.
* Pięknotka Diakon: PIVOTAL POINT; zginęła w walce z Cieniem i została odrodzona przez Saitaera jako jego paladynka. Cień został zniszczony, zintegrowany z jej ciałem (poza Esuriit). Pięknotka jest klasyfikowana jako mag ixioński, zupełnie jak Minerwa. Wyjątkowo wrażliwa na Esuriit, ale z nanorozkładem materii i świetną biosyntezą.

### Frakcji

* Kult Saitaera: Pięknotka Diakon stała się paladynką Saitaera, trochę mimo woli. Saitaer zużył na to większość pozyskanej energii na terenie Szczelińca.

## Zasługi

* Pięknotka Diakon: skończyła waśń między rodami Kazitan i Ursus; potem zrobiła nagą dywersję w parku rozrywki i gdy złapał ją Agaton by "wyleczyć", spanikowała. Gdy Cień prawie zabił Agatona, Pięknotka stanęła przeciw Cieniowi twarzą w twarz i zginęła. Saitaer ją zixionizował i przywrócił jako swoją paladynkę, ku wielkiej żałości Pięknotki.
* Gabriel Ursus: nie potrafił zostawić Sabiny Kazitan samej w spokoju; Pięknotka zmusiła go do porzucenia wszelkich vendett. Rody Ursus i Kazitan nie są już w stanie wojny.
* Sabina Kazitan: przekonała Pięknotkę, by ta pozwoliła jej odejść. Zarzuciła wszelkie vendetty i zostawiła Pustogor za sobą. Czas wrócić do domu, do Aurum.
* Minerwa Metalia: zinfiltrowała TAI Hestia w parku rozrywki Janor po dywersji zrobionej przez Pięknotkę. Przekonała Pięknotkę, że życie po Saitaerze to nie koniec; da się naprawić.
* Agaton Ociegor: biochemik, korozja i leczenie, lekarz Grzymościa. Mimo ataku na Grzymościa przybył naprawić ten teren; złapał Pięknotkę i prawie zginął do Cienia. Skończył nieprzytomny.
* Saitaer: gdy Pięknotka umierała w walce z Cieniem, powiedział jej, że jej może pomóc. Pięknotka opierała się mu do końca, ale ją wskrzesił jako paladynkę - acz nie ma nad nią pełni kontroli.
* Aleksander Muniakiewicz: Latający Rekin, chwilowo przejął kontrolę nad parkiem rozrywki Janor; próbował zatrzymać Pięknotkę i zauważył obecność Minerwy (acz ta się wyślizgnęła).

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Jastrzębski
                            1. Praszalek, okolice
                                1. Park rozrywki Janor: Pięknotka zrobiła tam niezbyt ubraną dywersję; w chaosie Minerwa przejęła kontrolę nad Hestią.
                                1. Lasek Janor: "walka" między Pięknotką i Agatonem; śmierć Pięknotki i jej rekonstrukcja przez Saitaera.

## Czas

* Opóźnienie: 2
* Dni: 1

## Inne

### Budowa sesji

#### Co się działo

* Zaczęło się od kombinacji Shame + Resentment. Próba wyjścia z tego stanu emocjonalnego doprowadziła do pojawienia się Kultu Ośmiornicy.
* W chwili obecnej Kult Ośmiornicy ma już swoje macki w miasteczku.
* W pewien sposób wyzwalaczem jest rozprzestrzenienie idei poza miasteczko.
* Hestia d'Janor jest TAI systemu rozrywkowego; jej przełożony jest już Kultystą.

#### Dark Future

* Park Rozrywki Janor stanie się stabilnym corruptorem okolicy.

#### Pytania

1. .

#### Dominujące uczucia

* .

### Komentarz MG

* Pokażę kralomut - człowieka / maga przekształconego daleko przez kralothy.
* Pokażę Kult Ośmiornicy, czemu jest groźny i do czego prowadzi.

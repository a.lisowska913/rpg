---
layout: cybermagic-konspekt
title: "Wyścig jako randka?"
threads: legenda-arianny
gm: żółw
players: kić, fox
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [201216 - Krypta i Wyjec](201216-krypta-i-wyjec)

### Chronologiczna

* [201216 - Krypta i Wyjec](201216-krypta-i-wyjec)

## Punkt zerowy

### Dark Past

.

### Opis sytuacji

.

### Postacie

.

## Punkt zero

.

## Misja właściwa

Infernia jest w naprawie - nic poważnego, ale po statku koloidowym nic dziwnego. Eustachy jest na detoksie, Admiralicja chce się upewnić, że nie ma efektów ubocznych tego co tam się działo. Innymi słowy, wszystko powinno być w porządku.

Ale nie jest - okazuje się, że niektórych części Infernia po prostu nie ma. A zespół roboczy mający pracować nad Infernią... nie pracuje nad Infernią. A koleś z administracji twierdzi, że zespół powinien tam być.

Arianna spytała Gunnara o co chodzi. Ten odpowiedział, że była ekipa, tak, ale to byli praktykanci, niewyszkoleni agenci. Nie ma KLASYCZNEJ ekipy naprawczej, takich ludzi jak grupa praktykantów on do Inferni nie dopuści. Arianna świetnie rozumie; poprosiła Klaudię o małe szczurzenie. Klaudia zabrała się do roboty:

* V: Ma dowód, że administrator łże. On sam poprzenosił ludzi, m.in. do Żelazka.
* V: Czekaj co? Ludzie mający naprawiać Infernię naprawiają Żelazko. Ale zdaniem Gunnara - nie ma po co teraz naprawiać Żelazka. Więc co jest grane? Za to ludzie oddelegowani do Żelazka... zniknęli. Są poza detektorami Kontrolera Pierwszego, w martwych częściach Tytana.
* V: W administracji koleś ma na imię Przemysław. Bardzo zainteresował się Marią z Żelazka, miał jej wizytę. Żelazko dostało sporo części, m.in. holoprojektory z Inferni, koloid czy inne takie - rzeczy jakich Żelazko nigdy nie wykorzysta.

Ok. To jest ciekawe. Tak więc Arianna wysłała sygnał do Olgierda z Żelazka typu "Olgierd, oddaj części i ludzi to nikt się nie dowie że gubisz i kradniesz". Olgierd zaprosił ją do Jelonka - baru na K1. Arianna przyjęła zaproszenie, wzięła ze sobą Klaudię i Dianę do pomocy w rozmowie.

Sama rozmowa wyszła 'cringy like hell'. Olgierd wyraźnie się nie przejmuje niczym; koleś ma konstrukcję szafy bojowej i nie przejmuje się absolutnie niczym. Diana podczas rozmowy cały czas mówi "A Eustachy zrobiłby to lepiej". A Arianna próbuje wyciągnąć z niego po co mu koloid i holoprojektory - a Olgierd spokojnie odpowiada, że przecież Żelazku to niepotrzebne.

Arianna zauważyła, że Olgierd wyraźnie ją lekceważy. Traktuje ją "jak małą arystokratyczną czarodziejkę bawiącą się w wojsko". I to jej się zdecydowanie nie podoba. W związku z tym ustalili, że zrobią pojedynek. Pojedynek o to, kto dostanie części i kto zapłaci za to wszystko co Olgierd zbroił. W lekkiej panice, nie wiedząc jaką broń wybrać, Arianna wybrała pojedynek na... wyścigi holopsów.

Olgierd lekko zgłupiał. Spodziewał się, że Arianna wybierze coś w czym na pewno wygra - np. pojedynek na wytworne noszenie kimon, czy poezja Astoriańska, czy magia. Ale nie HOLOPSY. Jak w ogóle ma taki pojedynek wyglądać? Uznali, że jeszcze co do broni wybiorą potem...

Dobra. Nie było to najbardziej udane spotkanie. Olgierd Ariannę nieźle zirytował, ale nic poważnego - po prostu klasyczny jowialny dewastator mający Ariannę gdzieś. Ale Arianna wie, że Martyn Hiwasser miał kiedyś kontakty z pierwszym oficerem Olgierda, niejaką Marią Naavas. Tak więc Arianna wysłała Martyna na przeszpiegi. Niech on powie Ariannie o co chodzi.

I faktycznie, Martyn poszedł porozmawiać z Marią. Naturalnie, jak zawsze, wylądowali w łóżku. Ale przy okazji porozmawiali:

* O: sekret za sekret
    * Maria powiedziała, że Olgierd ma tajny projekt badawczy w ukrytych fragmentach K1. Coś, o czym nikt nie wie. Od lat. I mechanicy są potrzebni by pomóc to zmontować.
    * Marian powiedział, że Arianna musi chronić Elenę a Olgierd się do niej zaleca. Maria ma szczękę gdzieś koło podłogi, ale to TROCHĘ tłumaczy zachowanie Arianny.
* O: wejście w głąb
    * Maria powiedziała, że to nie Infernia. Były serie statków, Olgierd na przestrzeni lat szabrował różne statki Orbitera. Część nie zauważyła, część się bała. Olgierd wybiera cele, które są SŁABE. Więc uważa Ariannę za słabą.
    * Martyn powiedział Marii, że Arianna nie wie jak zagadać do Olgierda, więc stąd pojedynek.

I dla Marii wszystko stało się jasne. Arianna jest zazdrosna o Elenę, ale nie wie jak zagadać do Olgierda - dlatego wzięła skrzydłowe na spotkanie sam na sam - by Diana i Klaudia pomogły Ariannie w rozmowie z Olgierdem. Chroni Elenę, ale jednocześnie dla siebie a nie dla samej Eleny. A jak doszło do zwrócenia uwagi Olgierda, potrzebny był sposób - ale jak? Więc: pojedynek. Acz w panice wybrała wyścigi psów. No, nieźle...

Arianna jak to usłyszała od Martyna to prawie zapłakała z żałości. To nie tak miało być! Wyszła na jakąś... ech. Dobra, ale Arianna jest dobrym taktykiem, jest w stanie sobie z tym poradzić. Jeśli Olgierd to od Marii usłyszy, na pewno nie będzie spał w nocy. Więc Arianna spróbuje obrócić to na swoją korzyść. Wybrała preferowany sposób pojedynku - wyścigi ścigaczy na specjalnie sfabrykowanym terenie Kontrolera Pierwszego.

Arianna poprosiła Klaudię o stworzenie takiego toru wyścigowego, by zarówno ona mogła się wykazać (i by wygrała ofc) i by Olgierd mógł się wykazać i pokazać co potrafi i jak jest dobry. A Martyna poprosiła, by ten pomógł jej oczarować Olgierda. Dowie się odnośnie planu Olgierda tak lub inaczej.

* V: Martyn po rozmowie z Marią dobrał dla Arianny IDEALNĄ suknię. Co więcej, sam ową suknię zdobył i sprokurował ;-).
* V: Maria pomogła Martynowi tak dobrać elementy trasy, by poprawić opinię Olgierda o Ariannie.

I Klaudia zabrała się do roboty.

Wpierw Klaudia skupiła się na silnych stronach Arianny jak i Olgierda - jak można pokazać im obu że są godnymi przeciwnikami?

| Rekord                                        | Arianna   | Olgierd |
|-----------------------------------------------|-----------|---------|
| "Zawsze idę za daleko"                        |  +1       |  +1     |
| Umiejętności pilotażu - świetny pilot         |  +1       |   0     |
| Wsadzę drony na trasę, trzeba je zniszczyć    |   0       |  +1     |
| Taktyczne myślenie, przygotowanie             |  +1       |   0     |
| "Zagadki Azteków" taki labirynt z pułapkami   |  +1       |   0     |

Co więcej, Klaudia dodała jeszcze parę aspektów do konstrukcji tego toru wyścigowego na Kontrolerze Pierwszym. Po pierwsze, tor wyścigowy jest "na serio" - jest możliwość dostać rany, można się pociąć. Jest niebezpiecznie. Po drugie - Klaudia wmontowuje tam anomalię antygrawitacyjną. Dzięki temu Olgierd ma trudniej - bo jest ciężki.

* (VXVO): Anomalie się rozpełzły. Klaudia z filozoficznym spokojem patrzyła, jak anomalia infekuje K1. Trudno. I tak w tych okolicach nikt nie mieszka.
* (VV): Trasa i jej konstrukcja się udała; wpierw Klaudia znalazła jakąś "starą" trasę wyścigową na K1 i potem ją dostosowała.
* (VV): M.in. dzięki Marii i mocy Klaudii i jej decyzjom - uda się Olgierdowi zaimponować. To jest pojedynek godny jego i Arianny. Na ostro.

Arianna ma sporo przewag:

* pożyczyła od Eleny jej ścigacz. Elena ma cholernie dobry ścigacz.
* wzięła epicką sukienkę, jaką jej zapewnił Martyn.
* ma Upiora Serc; anomalicznego samuraja miłości

Wpierw wejście smoka. Tzn. Arianny. Olgierd wszedł w porządnym, lekkim servarze. Arianna weszła w sukience, po czym "ups, nie wejdzie" i odcięła fragment przy nogach. Teraz Olgierd jest PRZEKONANY że Arianna go podrywa. Jest bardzo zdekoncentrowany.

FAZA 1: VVO: BOOM! Olgierd tak się zapatrzył, że walnął ścigaczem w jakąś ścianę czy coś. Do tego Upiorny Samuraj połączył się z Olgierdem komunikatorem Arianny i zaczął z nim flirtować po swojemu.

FAZA 2 "za daleko": VVX. Olgierd zaatakował, ale anomalia grawitacyjna całkowicie go pozbawiła sterowności. Gada z Arianną na komunikatorze; wspomniał, że Maria powiedziała że pojedynek nie będzie taki łatwy. Arianna zauważyła "a Ty znowu o tej Marii". Olgierd się zamknął, bo nie wie co powiedzieć XD.

FAZA 3 "pilotaż": XXV. Mimo, że Arianna jest lepszym pilotem to jednak Olgierd jest z przodu. Użył zdecydowanie za mocnych dopalaczy, uszkadzając lekko pojazd, ale to nieistotne; wygrywa. Arianna powiedziała mu, że jeździ lepiej niż myślała.

FAZA 4 "drony": VVX. Olgierd chce wygrać i się bardzo stara. Arianna zrobiła manewr próbując go killstealować na dronach i się popisać, ale (XX): prawie wpadła pod ogień. Olgierd uratował Ariannę i zauważył, że jakkolwiek ona jest dobra ale to jest jeszcze coś z czym musi popracować (killsteal).

FAZA 5 "aztekowie": VXX. Olgierd nie rozwiąże pułapek inteligentnie jak Arianna, ale włączył deflektory, pełna moc z przodu i PRZEBIŁ SIĘ PRZEZ ŚCIANĘ. Tego Klaudia się nie spodziewała. Wygrał tę fazę, acz ścigacz jest poważnie uszkodzony.

3:2 dla Arianny. Arianna wygrała.

Maria na uboczu prawie piszczy NO POWIEDZ JEJ ŻE CI SIĘ PODOBA. Ale Olgierd nie zagadał (za co dostanie od niej solidny opieprz). Za to Arianna pociągnęła go za język - skoro wszystko dla Eleny i Marii...

Olgierd powiedział, że nie chodzi o Elenę. Że to nie tak. Wyjaśnił Ariannie, że Arianna na akcji z koloidowym statkiem miała do czynienia z ważną dla niego anomalią. On posiada Aleksandrię Ekstraktywną (tu Klaudii zaświeciły się oczy) i on próbuje zebrać informacje - a ta anomalia jest jednym krokiem do tego czego Olgierd szuka.

Aha, umówili się, że następne spotkanie i wyścig gdy Olgierd porwie Eustachego. Arianna uważa, że wziął to na serio, ale nie jest do końca pewna. Olgierd natomiast już zaczął planować...

## Streszczenie

Infernia traci części i ekipę remontową, bo Olgierd z Żelazka na niej pasożytuje. W wyniku komedii pomyłek doszło do tego, że Olgierd uznał, że Arianna się w nim podkochuje ale nie wie jak zagadać. Skończyło się pojedynkiem ścigaczy na torze sfabrykowanym przez Klaudię i na anomalicznym Skażeniu Kontrolera Pierwszego. Przez Klaudię, naturalnie.

## Progresja

* Olgierd Drongon: jest zainteresowany Arianną Verlen. Uważa, że ta się w nim podkochuje, ale nie wie jak zagadać. Ale on też nie wie. Ma o niej (wreszcie!)opinię kompetentnej.

### Frakcji

* .

## Zasługi

* Arianna Verlen: by poznać sekret Olgierda, przez komedię pomyłek wpadła jako "ta co nie wie jak zagadać". Wygrała z Olgierdem pojedynek na ścigacze.
* Klaudia Stryk: odkryła na Kontrolerze Pierwszym starą trasę wyścigów po czym ją dostosowała do potrzeb pojedynku Olgierda i Arianny. Plus, Skaziła K1 anomalią.
* Gunnar Brunt: stary weteran naprawczy na Kontrolerze Pierwszym, odpowiada m.in. za Infernię i Żelazko. Lubi Ariannę. Nie dopuścił praktykantów do Inferni.
* Diana Arłacz: przyzwoitka Arianny na spotkaniu z Olgierdem. Monotematyczna: "Eustachy zrobiłby to lepiej niż Olgierd".
* Olgierd Drongon: od dawna pracuje nad Aleksandrią Ekstraktywną; wpadł w kolizję z Arianną. W pojedynku ścigaczy przegrał, ale to była doskonała walka.
* Martyn Hiwasser: święcie przekonany, że sekretem Arianny jest zauroczenie Olgierdem - i to przekazał Marii. Spiskuje z Marią, by Arianna x Olgierd.
* Maria Naavas: była miłość Martyna Hiwassera, zdradziła mu kilka sekretów o Żelazku i jego załodze... plus, poznała od niego "sekrety Arianny x Olgierda".

### Frakcji

* .

## Plany

* Maria Naavas: chce pomóc Olgierdowi Drongonowi spiknąć się z Arianną Verlen. Lub Ariannie spiknąć się z Olgierdem. Ma PLAN.
* Olgierd Drongon: chce porwać Eustachego Korkorana - w ten sposób czas na kolejny pojedynek z Arianną.

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria, Orbita
                1. Kontroler Pierwszy
                    1. Sektor 43
                        1. Tor wyścigowy ścigaczy: Klaudia go znalazła i odrestaurowała z mechanikami Inferni; tam był pojedynek Arianna - Olgierd.

## Czas

* Opóźnienie: 8
* Dni: 5

## Inne

.
---
layout: cybermagic-konspekt
title: "Astralna Flara dociera do Nonariona Nadziei"
threads: historia-arianny
gm: żółw
players: fox, kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [221102 - Astralna Flara i porwanie na Karsztarinie](221102-astralna-flara-i-porwanie-na-karsztarinie)

### Chronologiczna

* [221102 - Astralna Flara i porwanie na Karsztarinie](221102-astralna-flara-i-porwanie-na-karsztarinie)

## Plan sesji

### Fiszki
#### 1. Astralna Flara (55 osób max)
##### 1.1. Dowodzenie

* Arianna Verlen: kapitan
* Daria Czarnewik: engineering officer, (4 inż pod nią)
* Alezja Dumorin: eks-kapitan, Orbiter
    * ENCAO:  +0--0 |Amoralna, skuteczna| VALS: Hedonism, Face, Power| DRIVE: Wędrowny mistrz Ryu
* Władawiec Diakon: pierwszy oficer, tien, (p.o. Stefana) 
    * ENCAO:  +-0-0 |Intrygancki;;Żywy wulkan| VALS: Hedonism, Self-direction| DRIVE: Follow My Dreams, Korupcja anioła
* Grażyna Burgacz: logistyka i sprzęt (officer), tien (3 osoby pod nią) 
    * ENCAO:  -0+-- |Powściągliwa i 'nudna';;Ascetyczna| VALS: Humility, Tradition| DRIVE: Starszy Brat
* Arnulf Perikas: fabrykacja i produkcja (officer), tien (2 inż pod nim) 
    * ENCAO:  0-0-- |Hardheaded;;Napuszony| VALS: Tradition, Family| DRIVE: Wzbudzenie zachwytu
* Maja Samszar: comms officer, tien (p.o. Klarysy jak K. nie może) 
    * ENCAO:  0+-0- |Moralizatorska| VALS: Stimulation, Conformity >> Power| DRIVE: Długi
* Kajetan Kircznik: medical officer (5 pod nim) (czarny, afro, paw, augmentacje bio)
    * ENCAO:  +-0+0 |Anarchistyczny;;Stabilny emocjonalnie;;Z poczuciem humoru| VALS: Self-direction >> Power| DRIVE: Jestem unikalny

##### 1.2. Operacja

* Klarysa Jirnik: artillery, (p.o. Mai jak Maja nie może) 
    * ENCAO:  00+-- |Wymagający;;Małostkowy| VALS: Tradition, Conformity|DRIVE: Sprawiedliwość
* Tomasz Ruppok: starszy mat (13 osób + 2 starszych)
    * ENCAO:  +0--- |Mało punktualny;;Kłótliwy;;W przejaskrawiony sposób okazuje uczucia| VALS: Hedonism >> Achievement, Family| DRIVE: Apokalipta
* Rufus Warkoczyk: starszy mat (13 osób + 2 starszych)
    * ENCAO:  0-+-0 |Zarozumiały;;Bezkompromisowy, niemożliwy do zatrzymania;;Zorganizowany| VALS: Self-direction, Power| DRIVE: Odkrycie konspiracji ("ktoś nas sabotuje")
* Marcelina Trzęsiel: inżynier syntezy (pod Arnulfem), pod opieką tien Terienaka
    * ENCAO:  0+0-+ |Małostkowa;;Dramatic shifts in mood;;Różnorodność form| VALS: Hedonism, Achievement >> Security| DRIVE: "Co za tą górą"
* Elena Verlen: pilot / advancer

##### 1.3. Infiltracja / Starcie

* Gerwazy Kircznik: sierżant marine, Orbiter
    * ENCAO:  +0+-0 |Głośny;;Honorowy| VALS: Humility, Face| DRIVE: Lokalny społecznik
* Erwin Pies: kapral marine, Orbiter
    * ENCAO:  -0-+- |Skryty;;Wiecznie zagubiony;;Praktyczny| VALS: Tradition, Stimulation >> Power| DRIVE: Ochraniać słabszych
* Szymon Wanad: marine, Orbiter
* Marcel Kulgard
    * ENCAO:  000-+ |Demotywuje innych;;Innowacyjny| VALS: Achievement, Face| DRIVE: Zbudować legacy
* Tomasz Dojnicz: marine, Orbiter
    * ENCAO:  --0+0 |Nie znosi być w centrum uwagi;;Dusza towarzystwa| VALS: Conformity, Hedonism >> Power| DRIVE: Arlekin Maytag
* Szczepan Myrczek: advancer, tien, seilita,
    * ENCAO:  --+0- |Stabilny emocjonalnie;;Zawsze bardzo zajęty| VALS: Face, Achievement, Hedonism >> Self-direction| DRIVE: Zasady są święte
* Mariusz Bulterier: advancer, tien, seilita, sybrianin 
    * ENCAO:  0-+-- |Prostolinijny i otwarty;;Nie kłania się nikomu;;Uroczysty i poważny| VALS: Humility, Tradition| DRIVE: Nawracanie
* Hubert Kerwelenios: advancer, sarderyta
    * ENCAO:  -0+-0 |Kontemplacyjny, refleksyjny;;Kompleks paladyna| VALS: Humility, Tradition >> Stimulation| DRIVE: Odbudowa i odnowa

#### 2. Athamarein, korweta

* Gabriel Lodowiec: komodor-in-training
    * ENCAO: 0--+0 |Spokojna fasada;;Kontrolowany przez emocje | VALS: Tradition, Security >> Power, Face| DRIVE: Niszczenie hipokrytycznego systemu
* Leszek Kurzmin: dowódca jednostki

#### 3. Nonarion Nadziei (stacja cywilna)

* Leo Kasztop: sprzedawca sekretów na Nonarionie (atarienin)
    * ENCAO:  0-+00 | Intrygancki, polityka;;Nie odracza| VALS: Self-direction >> Stimulation, Tradition| DRIVE: Wygrać w rywalizacji)
* .Franciszek: enforcer Nonariona (atarienin)
    * ENCAO:  +00-- | Lubi rutynę;;Uszczypliwy i zgryźliwy;;Lubi wyzwania | VALS: Achievement >> Security| DRIVE: Komfortowe życie
* .Wojciech: agent Aureliona (faeril)
    * ENCAO:  ---+0 |Bezbarwny, przezroczysty;;Niemożliwy do ruszenia| VALS: Power, Humility >> Tradition | DRIVE: Supremacja Aureliona

### Theme & Vision

* Problemy kulturowe; Orbiter nie respektuje tego co wypracował Nonarion
    * Ratowanie zniszczonego statku - TAI też potrzebuje pomocy (TAI Mirtaela d'Hadiah Emas)
    * Wyjście na pokład statku kończy się... źle
* Daria nie ma łatwego powrotu
* Władawiec corruptuje Elenę

### Co się wydarzyło KIEDYŚ

* .

### Co się wydarzyło TERAZ (what happened LATELY / NOW)

* .

### Co się stanie (what will happen)

* S1: Nonarion itp., wejście na pokład stacji
* S2: Jak zrobić bazę
* S3: Ćwiczenia opracowane przez Władawca i ostrzeżenie ze strony Alezji - on coś planuje z Eleną
* SN: Hadiah Emas w ruinie - poważnie uszkodzona jednostka, ma 31 rannych na pokładzie
    * pogarda ze strony załogi
    * zniszczyć TAI Mirtaela i jej cztery synty
    * Hadiah Emas się rozpadła próbując ratować Isigtand, trzyosobowy grazer

### Sukces graczy (when you win)

* .

## Sesja właściwa
### Wydarzenia przedsesjowe

* 

### SPECJALNE ZASADY SESJI

.

### Scena Zero - impl

Arianna została wezwana na pokład Athamarein. Jest to ciężka korweta. Uzbrojona, solidna. I jest okrętem flagowym komodora Gabriela Lodowca. Gabriel to kosmiczna chudzina. Mało ćwiczy i mało je. Typowy "kosmita". Ostrzegł Ariannę, że jednostka wsparcia jest szczególnie zagrożona w tamtym sektorze. Dodał pięciu marines więcej do Arianny. "Kogoś kogo znam". Marcel, marine. + 4 innych marines. Flara ma zatem 10 marines.

Plan misji - przyczółek Orbitera w Anomalii Kolapsu i budować dobre nastroje wśród lokalsów. (To jest jego pierwsza operacja jako komodor). Arianna "mam w załodze kogoś z regionu, wie co przygotować". Gabriel "też mam załoganta z tamtego regionu". Gabriel dał możliwość Ariannie kontaktu z Alanem żeby mogła się skonsultować itp. Alan Nierkamin jest łącznikiem. Advancer, fachowo.

* Alan Nierkamin || ENCAO:  ++-0- |Nietolerancyjny, musi być tak jak uważa;;Dokładnie przemyśli wszystko zanim coś powie| VALS: Power, Achievement >> Tradition| DRIVE: wprowadzi Orbiter na Nonarion
    * Nie lubi sarderytów, bo oni zabijają ludzi

Daria - Alan.

Alan jest silnym, solidnym, masywny koleś. Kosmiczny górnik. Grazerowiec. Altinianin ("Nonarion needs to be cleansed by Orbiter's fire"). Koleś z amerykańskiego plakatu "dołącz do floty". 'Pani chorąży'. Daria szczurzy od niego informacje - jak dawno opuścił teren? Jakieś 2 lata temu. 

Tr+2:

* V: od razu się Darii coś rzuciło w oczy. Daria ma "slumsy, ale dom - umiesz się poruszać to przeżyjesz". Alan ma inaczej - "Orbiter jest w każdym calu lepszy niż Nonarion." z żarliwością.
    * "Teraz zrobimy tam dobrze".
    * Daria ma prawidłowe blueprinty - tanie, proste
* V: Alan wyjaśnił Darii, że są elementy standaryzacji - pojawiają się "lepszej klasy" elementy. Co jest nietypowe - wszystko z wraków? Więc skąd standaryzacja?
    * Ujednolicenie czego? Mocowanie, śruby, nity. Substraty. Ułatwienie konstrukcji.
* V: Daria szuka co będzie miało branie i na co będzie popyt by odpowiednio skonfigurować Flarę. PLUS czy dalej urzędują tam ci sami ludzie.
    * -> Daria ma gwarancję na to, że ma blueprinty z popytem PLUS podstawowe rzeczy na handel. Czyli lepsze wejście. Dużo lepsze wejście.

Daria oddaje raport Dowodzeniu. Plus pytanie - jak budować przyczółek? Jest odpowiedź od Gabriela: "zapewnić dobre stosunki wśród lokalnych, działania charytatywne, autonomia kapitanów."

* Kurzmin -> Arianna: "Jak Alezja? Jeśli mogę spytać."
* Arianna: "Nie jest źle, dochodzi do siebie". /jest blady uśmiech
* Kurzmin: "Mamy sensownego komodora. Chyba."
* Arianna: "Jestem bardziej zaskoczona niż Ty."
* Kurzmin: "Myślisz, że będzie tak źle jak uważa Lodowiec?"
* Arianna: "Nie sądzę; z drugiej strony, boję się że tam ma jakaś frakcja wpływy."
* Kurzmin: "Myślisz, że tam coś jest co nie jest... plebsem?"
* Arianna: "Jakaś siła zaopatrza ich w sprzęt"
* Kurzmin: "Noctis. Minęło mało lat od wojny."
* Arianna: "Syndykat Aureliona, może?"
* Kurzmin: "Może oni..." /nieprzekonany
* Arianna: "Czy jest w okolicy inna stacja?"
* Kurzmin: "Sporo stacji, wszystko małe"

Arianna ma NEUTRALNY stosunek do noktian. Noktianie byli podczas wojny, ale Verlenowie skupiali się na terrorformach Saitaera.

### Sesja Właściwa - impl

Zbliżacie się do stacji Nonarion Nadziei. Arianna NIGDY nie widziała tak smutnej i beznadziejnej struktury w kosmosie. A Arianna dowodziła Królową. Ogólnie - morale załogi widząc tą stację sklęsło. A Daria z lekkim rozczuleniem.

* Arnulf: "Panowie i panie, pokażemy im, jak dbać o stację." /próba podniesienia morale
* Maja: "Kto tu żyje..."
* Daria: "Ludzie, którzy utrzymują się z AK, salvagerowie, handlarze, inni tacy"
* Maja: "Że... ale... coup de grace?"
* Daria: "Wiesz, nie wszyscy mieli szczęście"
* Arianna: "Nie bylibyśmy tu przysłani gdyby było lekko, jesteśmy z misją humanitarną by im pomóc. Jakby tu było jak na Orbiterze, by nas tu nie było. Królowa wyglądała podobnie - a jest lepiej"
* Grażyna: "Sprytna konstrukcja. Szpetna jak noc, ale to noktiańskie jednostki." /żart
* Grażyna: "Dario, czy oni dbają o silniki statków? Bo to co się rozpada to się rozpada... np. łączność? Jakieś takie. Ale tego dużo. A silniki zadbane."
* Daria: "Anomalia jest daleko. NIE. Ona się rusza."
* Daria: "Dla wszystkich co chcą po stacji - nie idźcie solo. Odradzam chodzenie tylko w parach."
* Grażyna: "Musimy zdobyć zapasy wody. Kupimy je od lokalnych, będziemy bardziej lubiani."
* Daria: "NIE! Jak sądzisz, czemu upewniłam się że mamy wszystkie zapasy? Możemy kupić wodę lokalną, ale nie wsadzić na statek."
* Władawiec: "Dobrze, zapewnijmy by za nami tęsknili! Nie będzie łatwo, ale damy radę." /optymizm

Maja do Arianny: "Komodor chce kontaktu przez laser."

* Komodor: "Alan proponuje, byśmy weszli na stację i pokazali im jak można podejść do sytuacji. Co Ty na to?"
* Arianna: "Nie z butami do piaskownicy. Lepiej się zorientujmy jak ma się sytuacja na miejscu. Pokazać że możemy funkcjonować w zgodzie a nie pokazać jak zrobimy lepiej."
* Komodor: "Zgadzam się z Tobą. Znajdziemy niewielką asteroidę lodową i założymy tam bazę. Sprawdź ze swoją agentką czy to jest OK. Zdaniem Alana - tak. Ale Alan zgadza się na wszystko".
* Arianna: "Dario, jak to wyglądało kilka lat temu z aktami własności asteroid?"
* Daria: "Są prospektorzy, sprzedający prawa skorzystania. Mogę lecieć na stację wahadłowcem, uprawnienia do czegoś i tyle."

Daria, Ruppok, Wanad. Oni lecą na Nonarion negocjować zakup asteroidy. Plus zorientować się w aktualnych potrzebach. Wasza trójka trafiła na Nonarion. Poznajesz te korytarze. Stacja ma _dźwięk_. Nonarion jest w dobrym stanie; może nawet lepszym niż kiedyś. Z zewnątrz większa ruina, od środka wydrążona PRAWDZIWA stacja. A wszystkie pomniejsze biznesy dodają kasy, by Nonarion funkcjonował.

Daria z ekipą idzie na ExpanLuminis. Ruppok lekko zesztywniał. Coś zobaczył. Daria po chwili też - "Trzech marines Orbitera, nieużywani, w dobrym stanie, w sam raz na dobrą misję samobójczą. Tanio." Daria nie komentuje. Randomowa laska z Neikatis była sprzedana jako "Egzotyczna piękność z planety X".

Wanad broni się przed atakiem. (instrukcja Arianny: "słuchaj Darii i nie wykonuj agresywnych akcji póki w Waszą stronę nie jest")

TrZ+3:

* Vr: Wanad się odwrócił i szybko złapał za rękę kolesia z neurokastetem i mu ją łamie. Młodziak zawył, dwóch pozostałych zaczęło uciekać w tłum. Wanad się uśmiechnął.

Daria "zostaw go". Wanad puścił, odwrócił się i idzie z uśmiechem. Ruppok cicho szepnął "to za naszych w niewoli". Wanad "jak dali się złapać, zasłużyli na wszystko co ich spotyka." Daria "idziemy."

Daria zlokalizowała to czego szukała - Leo. Jest to klitka JESZCZE mniejsza i JESZCZE bardziej nędzna niż kiedyś. Wydawałoby się, że pójdzie do góry - nie wyszło.

Leo się postarzał. "Hiyori?" "Kiedyś, Hiyori." Uśmiechnął się, ale spoważniał. Daria "interesuje mnie planetoida". Leo szuka celu planetoidy. Dość daleko anomalii, na bezpiecznej trajektorii, ale ma własną anomalię. Jest dość daleko od wszystkiego - to jej zaleta. Leo do Darii "kogo reprezentujesz, jeśli możesz mi powiedzieć?" Daria "Orbiter". Leo pokręcił głową z niedowierzaniem.

Ruppok handluje o planetoidę.

Tr +3:

* X: Ruppok był pod wpływem tego z tymi "trzech marines Orbitera", więc obraził kilka razy Leo. On przywykł, ale zapamiętał.
* V: Ruppok wynegocjował dobrą cenę. W dobrej klasy częściach "cywilnych" kombatybilnych z lokalnymi systemami i urządzeniami. Leo dobrze to odsprzeda.

Gdy Daria powiedziała "Orbiter", Leo pokręcił głową z niedowierzaniem.

* Leo: "Ogden wolałby inaczej"
* Daria: "Ale Ogdena tu nie ma"
* Leo: "Ciężkie czasy czekają Nonarion jeśli Orbiter tu wchodzi"
* Daria: "To zależy"
* Leo: /pokręcił głową "Jesteś tak naiwna jak zawsze. Starsza, a dalej naiwna. Nic, lećcie na planetoidę." /wyjął noktiański spirytus? /haust z gwinta
* Leo: "Za tych, którzy zmarli i tych, którzy wrócili bez duszy"
* Daria: "Będziemy szukali z kimś współpracy"
* Leo: "Nie jestem tani, wiesz o tym. Robię dobrą robotę, ale nie jestem tani. Dla Orbitera jestem drogi."
* Leo: "Porozmawiaj o tym z właścicielem psiarni. I - jako darmowa rada - zapłać za szkody jakie dokonałaś."
* Daria: "Coś się zmieniło? Cena za głupotę to cena za głupotę" /niezrozumienie
* Leo: /pokręcił głową z politowaniem "Ty się zmieniłaś. Nie jesteś stąd. Dlatego płacisz. Bo chcesz współpracować. Jeśli chcesz zapłacić, puszczę wici i znajdę osobę której zapłacisz, tyle. On jest tani, bo - głupota." 

Gdy Daria wraca do Arianny, Wanad: "ale shithole. Podoba mi się." Ruppok chce kupić Orbiterowców.

TYMCZASEM ARIANNA.

Alezja przyszła odwiedzić Ariannę. "Załoga chce zejść na Nonarion bo ktoś im naopowiadał - NIE WIEM KTO - że tam są egzotyczne piękności tanio. Nie wiem czemu oni w to wierzą." (Arianna wie o drakolitkach i bioformacjach). Oni wierzą, że są LEPSI niż ci tutaj, nikt się nie odważy, jak Aurumowcy co pojechali na prowincję. I oni wierzą, że Arianna im to zapewni! Albo że sprowadzi piękności by można było się napatrzeć i nie tylko!!!

Oprócz tego... Twoja kuzynka. Zaczęła ćwiczyć z innymi. Z Władawcem, między innymi. Nie wiem jak idzie... on opracował jakieś ćwiczenia. Specjalnie dla niej. Ćwiczą z marines. Coś odnośnie... uciekania / wydobywania się z chwytów, walka wręcz. Ona z nim ćwiczy jak sobie radzić jak już jesteście w zwarciu. Alezja ostrzega Ariannę. Arianna "mam nadzieję, że Władawcowi nic złego nie się nie stanie". Arianna: "Jeśli Władawiec w jakikolwiek sposób skrzywdzi moją kuzynkę, niech się martwi o swoją skórę." Alezja "nie lekceważ tego. On dalej flirtuje z Mają. I Klarysą."

Załoga zaczyna się przygotowywać do bazy na asteroidzie. "Pierwsza misja, musimy się wykazać, pokażemy komodorowi i przeprowadzimy bazę to pokażemy ile jesteśmy warci. I może nawet puści nas na stację. Przygotowania od razu, potem więcej wolnego czasu."

Tr Z (motywacja egzotycznych piękności) +3:

* X: "ballada egzotycznych piękności" rozlewa się po statku i plotka eskaluje i rośnie
* V: starają się jak mogą. Masz wysoką wydajność. I morale.
* Vz: DLA TYCH PIĘKNOŚCI oni się mega starają; aż komodor w szoku.

Przybywa Daria. Wanad jest uśmiechnięty. Arianna wie co to znaczy - zrobił komuś krzywdę. Ruppok od razu idzie do Arianny "jest szansa że są przetrzymywani ludzie Orbitera." Daria zaczyna bliżej wyjaśniać - będąc na miejscu nie miała jak wyjaśnić.

Zmierzamy na naszą nową asteroidę. I mamy nadzieję, że komodor nie nazwie jej 'Orbiter Primus'.

Z uwagi na trasę i silniki impulsowe, 90 minut pilotowania. To nie jest blisko - lecimy na planetoidę. Swoją drogą, Daria wie, że Hiyori zajęłoby to jakieś 5h. Wiadomość od Athamarein: "wiadomość SOS na godzinie trzeciej. Komodor kazał przesunąć się z kursu na asteroidę - lecimy pomóc." Daria ma znowu paranoję. Smaczna jednostka wsparcia i pojedyncza nieimponująca ciężka korweta.

Athamarein szuka i przekierowała sygnał do Flary. Sygnał brzmi legitnie, ale taki sygnał brzmiałby legitnie. "SOS, tu Hadiah Emas, ratowałam Isigtand, sama wpadłam w kłopoty. SOS. Anomalia."

TrZ+3:

* XX: Daria rozpoznaje Isigtand. To trzyosobowy grazer. To jest jednostka od której Hiyori zdobywała wodę po utracie Ailiry. Jako dostawcy byli uczciwi - ale jedna z osób na pokładzie (właścicielek) była drakolitką. Spełnia wszystkie definicje Egzotycznej Piękności.

Zbliżacie się do Hadiah Emas. Jest w zasięgu i w zasięgu skanerów. Athamarein ma skanery "wojskowe" - zasięg, pancerz, uzbrojenie, prędkość. Flara ma skanery strukturalne. I Hadiah Emas jest w zasięgu skanerów. Isigtand jest wykrywalny. IFF jest obecny, ale statek jest ciężko uszkodzony. Cały przód został... jakby walnął weń mały asteroid. Oprócz tego jest też ciężko uszkodzony Hadiah Emas. On też został trafiony asteroidem.

Flara (Władawiec Diakon) skanuje asteroidy, Athamarein osłania Flarę.

Tr Z (superior equipment) +3 +3Oy:

* Vz: jeden z asteroidów jest anomalny. Jest stosunkowo blisko Isgtand.
* X: skaner COŚ uruchomił w asteroidzie. Pojawiło się pole grawitacyjne, inna asteroida mniejsza poleciała w stronę Flary. Athamarein wystrzelił 2 rakiety i zestrzelił ten mały asteroid.
* X: Asteroida strzeliła w Athamarein. Athamarein się broni. Asteroida strzela znowu.

Athamarein włącza silnik i leci na wsteczny impulsowy - poza zasięg, by się nie bronić.

Tr Z (sytuacja pod kontrolą, taktyka) +3:

* Xz: uruchomiła się INNA asteroida podobnie anomalna. Strzela do Athamarein.

Daria -> Arianna: włącz silnik, lecimy szybko i robimy zasłonę wodną. "Tysiąc" celów, dampen, odwracanie uwagi i ujawnienie anomalii.

Tr Z (Flara ma wodę i manewr) +4:

* Vr: Flara uratowała Athamarein przed uszkodzeniem
* Vr: FLARA Z WODY! Wszystkie anomalie skupiają się na zasłonie
* X: Zużyliśmy więcej zasobów niż chcieliśmy - dla lokalsów to OMG ILE KASY
* Vz: Ujawnienie wszystkich 4 anomalnych asteroid. Isgtand miał strasznego pecha. A Hadiah Emas nie wiedział o co chodzi.

Isigtand jest martwy. Reaktor jest uszkodzony, więc TAI wyłączyła jednostkę. Hadiah Emas uratowała załogę - ale też została trafiona. Hadiah ma lekko uszkodzony silnik i trochę sterowanie. Dlatego nie próbuje się poruszać. Plus nie wie co się stało, sensory też padły ale STRUKTURALNIE jest sprawna. 

Zgodnie ze skanem, na pokładzie Hadiah Emas są 23 osoby.

Naprawa Hadiah Emas - wchodzimy na pokład, robimy dogłębny skan, syntentyzujemy brakujące części, Arnulf dostosowuje blueprinty i - kilkanaście godzin.

* Daria -> Arianna: "Może nam się opłacać naprawienie tego statku, tu na miejscu i zdobycie w ten sposób sojusznika."
* Arianna: "Po to tu jesteśmy."
* Arianna -> Lodowiec: "Najszybszy sposób - odholowanie jednostki na bok i naprawienie na miejscu. Inaczej nadłożymy czas."
* Lodowiec: "Ile czasu zajmie nam uzupełnienie straconych zasobów?"
* Arianna: "(Grażyna: 11h przy założeniu, że mam czystą asteroidę). Przekazuje."
* Lodowiec: "(wzdycha). Dobra robota. Udało Ci się pomóc nam oraz osłonić tamte dwie. Szybkie myślenie." /nie jest szczęśliwy, ale nie obwinia.
* Arianna: "Zgoda na operację."
* Lodowiec: "Na razie masz dobre pomysły. Udzielam zgody na operację, samemu wyślę 4 marines i medyków na pokład tej jednostki. Może potrzebują pomocy."

Daria wysyła advancerów, oblicza co i jak i zakłada ich rękami "uprząż" do transportu. Daria próbuje wyciągnąć poza pole anomalii OBA statki. Jej się włączają instynkty kosmicznego złomiarza. Holujemy to, a na końcu pintka z Eleną jako falownik.

TrZ+3+3Or:

* Vr: advancerzy prawidłowo zamontowali uprzęże. Uprzeże się utwardziły i "da się holować".
* X: Isigtand jest w złym stanie; praktycznie się przepołowił. Dostał w nos. Ale złożyli go z dwóch statków. I one teraz poczuły zew wolności. On wymaga stoczni.
* X: Isigtand był już w zbyt złym stanie. On się nadaje na części. Da się wydobyć rzeczy, ale strukturalnie jest kiepski.
* V: Udało się przesunąć Hadiah Emas. Jest bezpieczny, można rozpocząć procedurę naprawy i korekty.

Marines i medycy. Sygnał do Arianny od komodora: "pokój narad, laser."

* Lodowiec: "Mamy dwa problemy z tym statkiem." 
    * "Po pierwsze, ma niestandardową ORAZ nielegalną TAI. Musimy ją zniszczyć. Ta TAI jest oparta na kopii neuronów osoby, to nie nasze 'fabrykowane'."
    * "Drugi problem - to jednostka niewolnicza. Nie rozumiem dlaczego jednostka NIEWOLNICZA próbowała uratować załogę GRAZERA i nie zmienić ich w niewolników."
* Lodowiec: "Załoga Isigtand trafi na pokład Flary. Muszę ich rozdzielić. To trzy osoby. Jedna z nich wygląda... inaczej."
* Lodowiec: "Rozwiążę ten problem. Ale chciałem Cię poinformować."

## Streszczenie

Flara dociera do Nonariona, który wygląda jak absolutny śmieć. Morale na Flarze ucierpiało - aż do plotek o Egzotycznych Pięknościach, co sprawiło Ariannie kolejne problemy. Alan, eks-lokals powiedział Darii że jest jakaś forma standaryzacji na Nonarionie - coś tam jest, coś "bogatego" jako frakcja. Daria zdobyła od Leo planetoidę którą może użyć jako bazę dla Orbitera, ale w drodze tam zboczyli by ratować uszkodzone statki lokalne. I uratowali jednostkę niewolniczą i zniszczony grazer. Komodor Lodowiec - sensowny koleś - ma dylemat moralny. Neikatiańska TAI oraz niewolnicy.

## Progresja

* Elena Verlen: trochę się otwiera na Władawca Diakona - on jest dobry w zapasach i ona MUSI go pokonać!
* OO Astralna Flara: dostaje ekstra 5 marines (z woli kmdr Lodowca); 

### Frakcji

* .

## Zasługi

* Arianna Verlen: ma neutralny stosunek do noktian - Verlenowie tępili terrorformy Saitaera; przyjęła, że Władawiec pomaga w treningu Elenie, rozpaliła morale ludzi (przypadkiem rozogniła plotki o Egzotycznych Pięknościach)
* Daria Czarnewik: wyczaiła, że agent Lodowca Alan może i był lokalsem ale jest zbyt pro-Orbiterowy; pozyskała doskonałą planetoidę od Leo i wyjaśniła jak działa stacja Nonarion; wykorzystała manewr Flary by rozpuścić wodę i odwrócić uwagę anomalnych asteroid od Athamarein.
* Marcel Kulgard: (ENCAO:  000-+ |Demotywuje innych;;Innowacyjny| VALS: Achievement, Face| DRIVE: Zbudować legacy) marine, Arianna go zna z czasów akademii. 
* Alan Nierkamin: (ENCAO: ++-0- |Nietolerancyjny, musi być tak jak uważa;;Dokładnie przemyśli wszystko zanim coś powie| VALS: Power, Achievement >> Tradition| DRIVE: wprowadzi Orbiter na Nonarion); lokals z Nonariona, dołączył do Orbitera 2 lata temu i jest zakochany w Orbiterze. Chce zastąpić kulturę Nonariona kulturą Orbitera. Pomaga Lodowcowi i Kurzminowi w zrozumieniu tego miejsca. Advancer. Nie lubi sarderytów. Silny, solidny, masywny koleś. Kosmiczny górnik. Grazerowiec. Altinianin ("Nonarion needs to be cleansed by Orbiter's fire").
* Gabriel Lodowiec: komodor Orbitera in-training. Próżniowiec, chudzina; mało ćwiczy i mało je. Całkiem sensowny dowódca Arianny i Kurzmina. Dopakował Ariannie ekstra marines, skonfliktowany między kulturą Orbitera i sytuacją na Nonarionie (neikatiańskie TAI, niewolnictwo itp.). Pod wrażeniem Astralnej Flary - to nie to czego się spodziewał.
* Arnulf Perikas: próbuje podnieść morale widząc Nonarion (z trudem); gdy napotkali na uszkodzoną Hadiah Emas, dostosowuje z ekipą blueprinty konstrukcyjne.
* Leszek Kurzmin: szczęśliwy, że mają sensownego komodora; 
* Maja Samszar: absolutnie załamana stanem Nonariona - jej pierwsza myśl to coup de grace. Pełni rolę comms experta bez problemu.
* Władawiec Diakon: zachowuje absolutny optymizm i rozpoczął ćwiczenia zapasów z Eleną i z marines. Skutecznie skanuje asteroidy; pełni rolę eksperta od radarów i detektorów.
* Grażyna Burgacz: jako jedyna doceniła klasę Nonariona jako sprawną konstrukcję. Potrzeba jej 11h na odzyskanie wody z czystej asteroidy.
* Szymon Wanad: wysłany z Darią by kupić od prospektora prawa do planetoidy. Zaatakowany, złamał jednemu napastnikowi rękę. 
* Tomasz Ruppok: wysłany z Darią by kupić od prospektora prawa do planetoidy. Wstrząśnięty handlem ludźmi i Orbiterowców, targował się z Leo, eskalował do Arianny.
* Leo Kasztop: postarzał się; powiedział Darii, że 'nie jest już stąd'. Ale sprzedał jej dobrą planetoidę, choć z anomalią. Będzie z nią współpracował.
* Alezja Dumorin: ostrzegła Ariannę, że Władawiec zaczyna się interesować Eleną. I o 'Egzotycznych Pięknościach' tanio. Ktoś rozpuścił plotki!
* Kajetan Kircznik: rozpuścił plotki o Egzotycznych Pięknościach na Nonarionie.
* OO Athamarein: ciężka korweta rakietowa Orbitera p.d. Leszka Kurzmina i statek flagowy Gabriela Lodowca. Walczy dużo ponad swą wagę, acz wymaga jednostki wsparcia (zasięg, amunicja). Skutecznie unika ataku asteroid anomalnych, ale wpada w pułapkę i Flara go uratowała.
* OO Astralna Flara: sprawna nade wszystko, bo rozpalona wizją Egzotycznych Piękności na Nonarionie; rozpuściła większość zapasów wody by ratować Athamarein przed anomalnymi asteroidami grawitacyjnymi.
* SCA Hadiah Emas: ciężko uszodzona jednostka niewolniczo-transportowa; próbowali pomóc Isigtand i TEŻ zestrzeliła ich anomalna asteroida grawitacyjna. 23 osoby, z czego 15 to niewolnicy.
* SCA Isigtand: bardzo ciężko uszkodzony trzyosobowy grazer; nie do naprawienia. Zestrzelony przez anomalną asteroidę grawitacyjną. 3 osoby.

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Obłok Lirański
                1. Anomalia Kolapsu, orbita
                    1. SC Nonarion Nadziei: na oko najsmutniejsza i beznadziejna struktura w kosmosie, niszcząca morale Astralnej Flary.
                        1. Moduł ExpanLuminis: proponowane są tam Egzotyczne Piękności i Prawdziwi Marine Orbitera. Którzy mogli nigdy nie stać koło Orbitera.
                    1. Planetoida Kazmirian: anomalna planetoida do której prawa są sprzedane przez Leo Darii; ok. 90 min od Nonariona, poza 'centrum'. Kilkanaście km średnicy i obecność wartościowych materiałów. Przyszła baza Orbitera? ;-)

## Czas

* Opóźnienie: 36
* Dni: 3

## Konflikty

* 1 - Alan jest Koleś z amerykańskiego plakatu "dołącz do floty". 'Pani chorąży'. Daria szczurzy od niego informacje - jak dawno opuścił teren? Jakieś 2 lata temu. 
    * Tr+2
    * VVV: Alan jest pro-orbiterowy i anty-nonarionowy, Daria wie już jakie blueprinty sobie zapewnić i o dziwnej standaryzacji na Nonarionie
* 2 - Wanad broni się przed atakiem. (instrukcja Arianny: "słuchaj Darii i nie wykonuj agresywnych akcji póki w Waszą stronę nie jest")
    * TrZ+3
    * Vr: Wanad się odwrócił i szybko złapał za rękę kolesia z neurokastetem i mu ją łamie. Problem z głowy.
* 3 - Ruppok handluje o planetoidę z Leo.
    * Tr +3
    * XV: Ruppok ciągle wstrząśnięty 'trzema marines Orbitera', ale ma dobrą cenę. W dobrej klasy częściach 'cywilnych'. Tanie dla Flary a Leo dobrze odsprzeda.
* 4 - Arianna: Załoga zaczyna się przygotowywać do bazy na asteroidzie. "Pierwsza misja, musimy się wykazać, pokażemy komodorowi i przeprowadzimy bazę to pokażemy ile jesteśmy warci. I może nawet puści nas na stację. Przygotowania od razu, potem więcej wolnego czasu."
    * Tr Z (motywacja egzotycznych piękności) +3
    * XVVz: plotka Egzotycznych Piękności eskaluje na statku, starają się jak mogą, są tak dobrzy że aż komodor pod wrażeniem
* 5 - Flara (Władawiec Diakon) skanuje asteroidy, Athamarein osłania Flarę.
    * Tr Z (superior equipment) +3 +3Oy:
    * VzXX: anomalna asteroida, atakuje Athamarein.
* 6 - Athamarein unika asteroidy, leci w inne miejsce
    * Tr Z (sytuacja pod kontrolą, taktyka) +3
    * Xz: uruchomiła się INNA asteroida podobnie anomalna. Strzela do Athamarein.
* 7 - Daria -> Arianna: włącz silnik, lecimy szybko i robimy zasłonę wodną. "Tysiąc" celów, dampen, odwracanie uwagi i ujawnienie anomalii.
    * Tr Z (Flara ma wodę i manewr) +4
    * VrVrXVz: Athamarein uratowana, ujawnione 4 anomalne asteroidy, zużyliśmy sporo wody.
* 8 - Daria wysyła advancerów, oblicza co i jak i zakłada ich rękami "uprząż" do transportu. Daria próbuje wyciągnąć poza pole anomalii OBA statki. Jej się włączają instynkty kosmicznego złomiarza. Holujemy to, a na końcu pintka z Eleną jako falownik.
    * TrZ+3+3Or
    * VrXXV: Hadiah Emas jest bezpieczna, Isegtand martwa, mamy holowanie.
* 9 - 
    * 
    * 
* 10 -
    * 
    * 


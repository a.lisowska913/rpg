---
layout: cybermagic-konspekt
title: "Mychainee na Netrahinie"
threads: legenda-arianny
gm: żółw
players: kić, kapsel, fox
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [210804 - Infernia jest nasza!](210804-infernia-jest-nasza)

### Chronologiczna

* [210804 - Infernia jest nasza!](210804-infernia-jest-nasza)

## Punkt zerowy

### Dark Past

.

### Opis sytuacji

.

### Postacie

.

## Punkt zero

brak.

## Misja właściwa

Kontroler Pierwszy. Nie dzieje się nic złego ani szczególnie odmiennego - Eustachy pokazuje Dianie statki kosmiczne a ona zachowuje się "och, ach", bo myśli, że powinna. Ogólnie lekko cringe'owato. Wtem! Gasną światła. Znowu ktoś próbuje Eustachego zaatakować. Ale Eustachy jest weteranem, działając w siłach Arianny po prostu zdążył przywyknąć.

* V: Eustachy rzuca minę i Diana krzyczy (z euforią...) - odwrócenie uwagi od pozycji Eustachego i Diany
* V: Eustachy ciągnie za sobą Dianę, zmieniają pozycję plus wzywa Ariannę - wrogowie nie umieją ich znaleźć
* X: mina wybuchła; są ranni. 
* V: Arianna i Klaudia wpadają, są gotowe i z zaskoczenia
* X: wrogowie dorwali Dianę
* V: Eustachy ratując Dianę kaskadował eksplozje. Pełna demoralizacja. Arianna walnęła łukiem elektrycznym.

I ku wielkiemu zdziwieniu Arianny pojawił się główny porywacz. To KOMCZIRP. Koleś, który był powiązany ze zniszczoną przez Elenę korwetą 'Tvarana' i który jest "wyznawcą" Arianny. Krzyknął w ogromnej emfazie do Arianny, że musi ją porwać! Netrahina jest w niebezpieczeństwie! Gdy Arianna zrobiła rybkę, zza pleców Komczirpa wynurzyła się Diana i walnęła Komczirpa w tył głowy gaśnicą, pozbawiając go przytomności... Arianna natychmiast wezwała służby medyczne, ale przesłała Komczirpa dyskretnie, do Martyna. Musi mieć kogoś, komu może ufać.

Parę godzin później, w barze "Jelonek" na Kontrolerze Pierwszym Komczirp zaczął opowiadać, że na Netrahinie źle się dzieje. Z kolesiem nie do końca jest idealnie, coś ma z głową, ale nie bardzo mocno. Zacznijmy od tego, że uwierzył, że to ARIANNA go oszołomiła używając "Power Word". Komczirp powiedział Ariannie, że Netrahina ma chwilowo tylko pomniejszą TAI klasy Semla; nikt nie chciał montować Persefony po tym jak opętana została poprzednia jej wersja. Ale pojawiają się inne fakty wskazujące na opętanie ZAŁOGI dla odmiany - i dlatego Komczirp dyskretnie przyleciał by porwać Ariannę, Eustachego i Klaudię.

Porwać..? Z Komczirpem coś jest wyraźnie nie tak. Arianna poprosiła Klaudię, by ta z Martynem zbadała Komczirpa. Komczirp na to "BADAJ MNIE" w stylu Rejtana. Diana zapiszczała z radości a w barze oklaski - uznano, że Komczirp się Klaudii oświadczył...

Tak czy inaczej, Martyn i Klaudia ruszyli do badania Komczirpa. (Ex:VVV). Faktycznie, Komczirp jest pod wpływem anomalii magi-sonicznej, czegoś podobnego do tego co normalnie robi Ataienne. Czyli coś w stylu "syreny". To implikuje, że na Netrahinie jest biosoniczna anomalia. Klaudia używając technologii Kontrolera Pierwszego przygotowała magisoniczny inoculator - coś, co zabezpieczy Zespół (Ariannę, Klaudię, Eustachego) przed atakiem magisonicznym. Nie ma niczego co może zrobić by POMÓC Komczirpowi, ale może się ZABEZPIECZYĆ.

Dokładne badania własności magisonicznych wskazują na to, że anomalia ma skłonności autodestrukcyjne. Jeśli Arianna poleciałaby Infernią czy czymś analogicznym, Netrahina mogłaby otworzyć ogień a załoga - przeświadczona o tym że ratują wszystkich i wykonują misję - mogłaby doprowadzić do autodestrukcji statku badawczego.

W poszukiwaniu informacji co się tam dzieje Klaudia ruszyła do raportów. Co Netrahina zgłaszała, co zgłaszały ich diagnostyki. (VXV). Okazało się, że infekcja anomaliczna zaczęła się jakieś 2 tygodnie temu; co więcej, Klaudia pobierając dane telemetryczne określiła jednoznacznie, że ta anomalia jest ciekawsza i groźniejsza niż się jej wydawało - wpływa też na mechaniczne elementy Netrahiny i sprawia, że Netrahina się samoreperuje. Co więcej, ta anomalia wpływa magisonicznie na umysły magów...

To jest powód, czemu Komczirp, który na co dzień nie jest taki głupi uznał, że musi porwać Ariannę. Zwyczajnie obwody w mózgu mu się poprzestawiały. Ale on jest jedynym, kto faktycznie może przeszmuglować Ariannę na Netrahinę - ryzyko, które Arianna na siebie musi wziąć.

Co to jest za anomalia?

Tak czy inaczej, korzystając z wiedzy Klaudii, Arianna zrobiła _requisition_ - zamówiła odpowiednie kanistry środków obezwładniających. Plan jest prosty - dyskretnie dostać się na Netrahinę, potem rozpuścić odpowiednie środki i obezwładnić załogę.

Jak dostać się na Netrahinę? Komczirp zaproponował torpedę abordażową. Jednak Eustachy powiedział, że lepiej będzie polecieć ze śmieciami kosmicznymi. Torpeda abordażowa NA PEWNO będzie wykryta przez Netrahinę - nawet przez Netrahinową Semlę. Arianna zdecydowanie przychyliła się do planu Eustachego... aha, Diana zostaje na Kontrolerze Pierwszym.

Dobra, mają plan. 'Tveer' leci, pilotowany przez Komczirpa, w kierunku Netrahiny. Zbliżając się do statku złapał informację o tym, że 'Saabar', ciężki myśliwiec Netrahiny poleciał w głąb Obłoku Lirańskiego. Niestety, niewiele są w stanie na to poradzić... na szczęście 'Saabar' nie zainteresował się 'Tveerem'.

Gdy zbliżyli się do Netrahiny, Martyn - ekspert od manewrów próżniowych - przeprowadził abordaż Netrahiny używając śmieci jako dywersji (XXXV). Niestety, nikt nie przewidział ani nie wykrył anomalii przestrzennych niedaleko Netrahiny - Martyn łapał magów (Arianna, Klaudia, Eustachy, on sam) by doprowadzić do celu, ale potracił większość kanistrów i doprowadził do poważnej eksplozji w jednym z silników Netrahiny. Jedyne, co się w tym wszystkim udało - jesteście razem i Martyn ma dość środków by próbować odbudować zapas kanistrów.

Eustachy zdecydował się znaleźć jakąś gródź i właz niedaleko uszkodzonego silnika - tam nikt się nie zdziwi że jest kolejna eksplozja. Wbił się na pokład; wszyscy używają skafandrów z Tveera, więc są przypisani do Netrahiny - ta linia wykrycia jest odrzucona. A Eustachy prowadzi ich do MedBay.

* X: Wybrał drogę z WIĘKSZĄ ilością wybuchów. Destrukcja i okazja pokazania się Dianie. 
* VVV: Droga okazała się skuteczna, doprowadził do skrzydła medycznego i dał Martynowi czas na odbudowanie środków unieszkodliwiających
* V: Dodatkowo MEGA zaimponował Dianie. I uszkodził łączność na Netrahinie - więc magisoniczny przeciwnik ma trudniej.

Klaudia korzystając z okazji zaczęła wchodzić w interakcję z biedną TAI Semla. Semla jest nie dość silna jak na Netrahinę, połowa systemów nie działa i jest ograniczona - nic dziwnego, że to się skończyło tak średnio. Klaudia spojrzała podejrzliwie na Ariannę - jako, że ta nie patrzy, to wbiła w Netrahinę anomalię amplifikacyjną; coś pokrewnego Leotis.

* OX: Semla odżywa, ale po sesji cybernetyczne połączenia w Netrahinie zostaną zniszczone. Semla też wymrze XD.
* VV: Semla jest na pełni mocy (wyżej niż Semla), kontroluje Netrahinę... a Klaudia kontroluje Semlę.

Klaudia szybko dotarła do tego z czym ma do czynienia:

* sporo ludzi chodzi w kombinezonach, bo sabotaże i atmosfera
* przeciwnik to Mychainee (mycellian siren). Znajduje się to w Life Support system.
* Mychainee to grzyb; infekuje technologię i biologię.

TAAAAAAK. To trochę utrudnia życie. Arianna wpadła na pomysł, że Klaudia z Martynem mogą zmienić te kanistry - niech skutecznie niszczy Mychainee a nie unieszkodliwia ludzi na stacji. Zdaniem Klaudii, niewielki problem - kazała Semli ściągnąć tu kilku techników "do naprawy medbay" a Arianna magią ich unieszkodliwia (VXV): skutecznie się udało, acz sygnał zarezonował i sporo ludzi zaczęło wandalizować Netrahinę. Nic poważnego.

Tak czy inaczej, Klaudia ma ludzi i z Martynem może wyiągnąć resztki Mychainee - dzięki czemu może zrobić perfekcyjną kontrę na anomalne grzyby. (VVXV): będzie to widowiskowe i dość krwawe, ale uda się disruptować te cholerne grzyby - a nawet usunąć je z systemów i z kadłuba Netrahiny. Teraz tylko Eustachy doprowadził nowe środki do odpowiedniego miejsca, by tylko life support pochłonął środek i sprawa załatwiona :-).

2 godziny stabilizowali Netrahinę. Martyn uwijał się jak w ukropie, by ratować ludzi przy życiu.

I wtedy na dalekich sygnałach Netrahiny sygnał - Saabar wraca. Netrahina reklasyfikowała Saabar z 'OO Saabar' na 'AK Saabar' - to Anomalia Kosmiczna. Kapitan Halina Szkwalnik oraz 6 osób z załogi uległo anomalizacji i integracji (przez Mychainee?).

Eustachy ruszył do systemów ogniowych. Zestrzeli Saabar. Arianna go zatrzymuje - użyjmy tej durnej torpedy abordażowej. Niech to trafi w Saabar. Niech Saabar ma szansę na szokowe wyleczenie przez anty-środek zaprojektowany przez Klaudię i Martyna! Eustachy się zgodził acz ostrzegł, że to zajmie więcej czasu i będzie trudniejsze, Netrahina może bardziej ucierpieć. Arianna się zgodziła - woli żywych pilotów niż sprawną Netrahinę.

* X: struktura pancerza Netrahiny została uszkodzona
* O: Saabar i Netrahina są uszkodzone; Semla wspomaga Eustachego i zaraziła się jego pragnieniem wybuchów
* X: Saabar jest zbyt zwrotny a Semla za słaba. Uszkodzenie silnika; Netrahina nie wróci sama do portu.
* V: Saabar odgoniony, nie przejdzie przez zasłonę ogniową Eustachego
* V: TORPEDA ABORDAŻOWA. Saabar uratowany.

Netrahina ma jeszcze godzinę działania, ale Tveer może przeskoczyć na Kontroler Pierwszy. Arianna utrzyma porządek na Netrahinie a Martyn utrzyma załogę Saabar przy życiu - ale Netrahina jako statek jest martwa. Potrzebny jest holownik...

## Streszczenie

Komczirp pod wpływem anomalicznych grzybów (Mychainee) próbował porwać Ariannę by ta pomogła Netrahinie. Arianna nie dała się porwać, ale z Zespołem wbiła na Netrahinę i zniszczyła anomalię. Niestety, Netrahina została bardzo poważnie uszkodzona, ale nie ma dużych strat w ludziach. 

## Progresja

* .

### Frakcji

* .

## Zasługi

* Arianna Verlen: przeprowadziła operację 'ratujemy Netrahinę, ponownie' i zapewniła wszelkie potrzebne środki i pomoc Komczirpa.
* Klaudia Stryk: odkryła Mychainee na Netrahinie badając Komczirpa; na samej Netrahinie zsyntetyzowała z Martynem anty-środek na ten wariant Mychainee.
* Eustachy Korkoran: zaimponował Dianie selfie z wybuchami; uszkodził Netrahinę bardziej niż musiał. Zaplanował jak wbić na Netrahinę. Mistrz artylerzysta; wpakował Saabar na torpedę abordażową.
* Martyn Hiwasser: przeprowadził operację abordażu Netrahiny 'kosmiczne śmieci'. Niestety, stracił większość kanistrów - więc w MedBay Netrahiny musiał zsyntetyzować nowe (z Klaudią), niszczące Mychainee.
* Rufus Komczirp: mimo wpływu Mychainee miał dość woli, by ściągnąć na Netrahinę Ariannę. Acz próbował ją porwać...
* Halina Szkwalnik: dowódca OO Saabar przypisanego do Netrahiny, potem AK Saabar. Zintegrowana z Saabar przez Mychainee. Uratowana przez oddział Inferni, kosztem dewastacji Netrahiny.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Obłok Lirański

## Czas

* Opóźnienie: 11
* Dni: 3

## Inne

.
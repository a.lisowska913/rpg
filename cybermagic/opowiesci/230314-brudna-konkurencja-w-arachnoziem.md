---
layout: cybermagic-konspekt
title: "Brudna konkurencja w Arachnoziem"
threads: waśnie-samszar-verlen
gm: żółw
players: kić, anadia, kolega-kamil
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [230425 - Kłótnie sąsiadów w Wańczarku](230425-klotnie-sasiadow-w-wanczarku)

### Chronologiczna

* [230124 - KJS - Wygrać za wszelką cenę](230124-kjs-wygrac-za-wszelka-cene)

## Plan sesji
### Theme & Vision & Dilemma

* Tenpenny joke "Kamikaze"
    * Don't look behind you, there's another one - as powerful | expendable
* Knight Rider "The Final Verdict "
    * https://knight-rider.fandom.com/wiki/Episode_110:_The_Final_Verdict
* CEL: pokazanie unikalnie sybriańskiego podejścia 
    * (care 2, fair 4, loyal 3, auth 1, sanct 0, lib 5)
        * duża firma używa atraktora Potwora
* Mimo problemów PvE ludzie nadal się zwalczają o reputację i wpływy
    * firma konstrukcyjna brata Fiony (harmonia) vs wielka firma za którą stoi sama tien Brunhilda Verlen (power & face & domination)
        * Kaze no tani no Nausicaa
* DYLEMAT
    * brak -> zagadka jak pomóc

### Co się stało i co wiemy

* KTO (gracze)
    * Fiona Szarstasz to eks-verlenlandrynka. 
        * Na terenie Verlenlandu popełniła przestępstwa które uległy przedawnieniu
            * Q: jakie? -> ujawniała WSZYSTKO. Białe kłamstwa, intrygi itp.
        * Znalazła miejsce na terenie Samszarów, gdzie została nawet opiekunką Misteriona dzięki swoim unikalnym umiejętnościom
            * Q: jakim? -> świetnie wykrywa kłamstwa ale też świetnie kłamie
        * ADVANTAGES: local, social, świetnie wykrywa kłamstwa, reputacja osoby która to umie
    * Karolinus Samszar jest magiem rodu Samszar, młodym (20) paniczem który pojechał na wczasy. Pomóc rodzinie Fiony.
        * Q: czemu? -> miłość jest ślepa
        * Q: jaką ma dominantę magiczną? -> zmiana bioformy, zmiana formy biologicznej
        * ADVANTAGES: tien, bioforma / biomagia
    * GRUBY ŚCIGACZ jest eks-ŁZą militarną typu Raptor, rodem z Orbitera, imieniem "Szybka Strzała" (nazwany przez Karolinusa)
        * Prawie zniszczona ŁZa, uratowana przez ród Samszar i zainwestowana.
        * ADVANTAGES: walka elektroniczna, walka, skan, transport, hidden
        * Q: czemu wygląda idiotycznie? -> jest pomalowany w kwiatki
* IDEA
    * miasteczko Arachnoziem
        * słynie ze złóż minerałów, niestabilności i potworów
        * ma garnizon 50 żołnierzy na 20k populacji rozsianych po górzystym terenie
        * potrzebny jest przetarg na konstrukcję i utrzymanie
            * ArachnoBuild, firma brata Fiony, Fircjusza
                * ADV: znajomość lokalna, umiejętność unikania anomalii, nepotyzm
            * EnMilStrukt, spora firma działająca w Verlenlandzie, ze wsparciem Brunhildy
                * ADV: zasoby, ogromna siła ognia, ściąganie ludzi z zewnątrz

### Co się stanie (what will happen)

* S01
    * Fircjusz ma problemy; będzie przeciwko niemu rozprawa, bo "się spieszył i pojawił się potwór"
* S02
    * 
* S03
    * 

### Sukces graczy (when you win)

* rozwiązanie problemu

## Sesja - analiza

### Fiszki w Arachnoziem

* Fircjusz Szarstasz: brat i szef ArachnoBuild
    * ENCAO:  +0+0- |Nie lubi abstrakcyjnych czy teoretycznych konceptów;;Metodyczny, stopniowo rozwiązujący problemy| VALS: Self-direction, Universalism| DRIVE: Żyć jak celebryta
    * styl: bombastyczny
* Mateusz Kurawiesz: porucznik 'na emeryturze', dowodzi siłami defensywnymi; 54
    * ENCAO:  -0--+ |Sentymentalny;;Perwersyjny, uwielbia robić skandale | VALS: Hedonism, Power| DRIVE: Prowokator
    * styl: głośny, barwny, kocha kobiety
* POTWÓR Jaszczur Tunelowy
    * modelowany po stegozaurze / behemocie, ale też pluje czymś
    * bardzo pancerny, mało słabych stron

### Scena Zero - impl

.

### Sesja Właściwa - impl

Fiona jest przed biurem firmy ArachnoBuild. Nie chce być poznana, przefarbowała włosy. Wiesz że brat ryzykuje utratą firmy rodzinnej. Brat podobno poszedł za zyskiem, spieszył się i doszło do pojawienia się Potwora. Karolinus pójdzie do knajpy i pogadać o "zakupie firmy", Fiona poszła do brata by pogadać o firmie.

Karolinus szuka knajpy ścigaczem. Jest lokal - "Łeb jaszczura". Łeb jaszczura jest nad drzwiami. Panienka za kontuarem "przegrałeś zakład? XD". Karolinus podpytuje co to za miasto, jak to wygląda. I o firmach. Barmanka przedstawiła się jako Laura. "Jesteś dziennikarzem, wiedziałam.". Jak przeszła do Arachnobuild, silny Daniel ją zatrzymał "Laura...". Daniel "swoje sprawy zostawiamy u siebie." - patrząc na Karolinusa - "wszystko w porządku". Karolinus: "tak tylko chciałem pogadać."

Karolinus chciałby się bardziej dowiedzieć o "tym nowym", czyli o sytuacji

Tr (niepełny) Z (dziennikarz) +3 +3Oy:

* Vz: Laura ma GDZIEŚ to, że Danielowi się to nie podoba. Laura jest NIESZCZĘŚLIWA i ma zamiar wszystko opowiedzieć
    * Laura chciała wyjść za niejakiego Stefana. Ale Stefan odrzucił jej oświadczyny. I teraz Potwór zniszczył część sadu Stefana. I to wszystko przez Fircjusza.
        * zdaniem Stefana Laura nie jest dla niego dość dobrą partią. "Podobno JA JESTEM GŁOŚNA? JA JESTEM KŁÓTLIWA!"
    * Na tym terenie zawsze działała tylko jedna firma budowlano-konstrukcyjna. ArachnoBuild. Teraz pod Fircjuszem Szarstaszem
    * Ale niedawno (parę miesięcy temu) na ten teren próbuje wejść nowa firma. Wielka firma. Działa na całym Verlenlandzie. Jest wspierana finansowo przez samą Brunhildą Verlen.
        * EnMilStrukt, przedstawicielką jest Diana. I ona jest dobrze ubrana, wysławia się. I ma POMYSŁY na ten teren. I chce zainwestować!
* X: Albo Daniel albo Laura będzie zła na Ciebie po rozmowie.
* Oy: Koleś wstał zza ławy. Silniejszy, sensowny fizycznie koleś. OPIERDALA Karolinusa, że on chce poniszczyć to miasto itp.
* Vr: 
    * Koleś Kacper, ciężko przeżył tego potwora bo on nie wierzy, że to jest robota Fircjusza. Kacper wierzy, że za tym w 100% stoi EnMilStrukt. Nic nie znalazł, żadnych śladów.
        * Ślady wskazują, że Kacper to zrobił, a mówi że nie zrobił.
    * Daniel mówi, że ostatnio są problemy
        * EnMilStrukt płaci więcej. I ma lepsze BHP. Mają własnych uzbrojonych ludzi. Dobre, sprawne korpo.
        * ArachnoBuild ma inny sposób działania. Fircjusz dba o ekologię, nie tępi potworów, koegzystuje z nimi.
        * EnMilStrukt jeśli pójdzie po swojemu to rozwalą ekologię. Będą potwory.
        * Fircjusz wiedział, że jedyne co musi to NIE SPIEPRZYĆ. A SPIEPRZYŁ! I pierwszy raz od kilku lat.
            * Był pod presją by pokazać, że może być szybkie wydobycie. Ale... no on spieprzył. I teraz chodzi o to, by pomóc. By EnMilStrukt nie wszedł. Może z burmistrzem pogadać. Może wezwiemy tien Verlen.

Jedziemy do ArachnoBuild. Fiona idzie spotkać się z bratem. Sekretarka. Młoda, ładna, gatekeeper. Fiona weszła do środka krzycząc FIRCEK! Fiona od razu podejrzewa Anię (sekretarkę) o to, że Ania zmieniła plany. Tłumaczy Fircjuszowi, że Ania miała dostęp do planów.

Fiona chce dowiedzieć się od Fircjusza: czy się zmienił i sam sabotował (za darmo: on nie wie co się stało). Niech Strzała przeanalizuje substrat danych od Fircjusza - gdzie mogło być słabe ogniwo? Strzała ma proces biznesowy, wie jak to wygląda, wie kto co robi i wie mniej więcej jak działają duże firmy (ściągnie dane).

Tr Z (dane od Fircjusza) +2:

* X: filmy dla Karolinusa się pocięły i nie działają.
* V: koreluje raporty co poszło nie tak, ma dostęp do raportów
    * wszystko co jest w systemach komputerowych DZIAŁA. Te dane są "prawdziwe" - spójne.
    * już wcześniej były odchylenia. Np. jaszczur był nie tam gdzie powinien być - dwa razy. W poprzednim miesiącu. Nie wtedy gdy ludzie byli w kopalni.
    * jeśli ktoś nie miał danych z czujników oraz danych z raportów nie mógłby tego sabotować.
* V: test słabego ogniwa
    * osoby takie jak Kacper czy Ania mają dostęp do raportów. Kacper, bo rozdziela kto idzie gdzie. Ania, bo ona ma tu dane. To zaufani oficerowie firmy.
    * jedyne osoby które MOGŁY te dane przekazać to Kacper i Ania. 
        * Kacper te dane wyciąga wtedy, gdy: gdzie przydzielić, jak zrobić itp.
        * Ania bo ona przetwarza te dane. Przesyła Kacprowi itp. Sprawdza.
    * JEŚLI jest kret to jest to ALBO Kacper ALBO Ania
    * Nigdy nie było "widocznej anomalii" zachowania potworów dopóki nie pojawiła się ta firma EnMilStrukt

Fiona -> Fircjusz

Tp Z (przeurocza siostra) +4:

* X: Fircjusz jest PRZEKONANY w swoich uczuciach do Ani
* Vr: 
    * TAK, Fircjusz i Ania mają się ku sobie.
        * Ania sukcesywnie będzie dostawała udziały Fiony.
    * Kacper jest starym przyjacielem rodziny, on jeszcze pomagał ojcu i zawsze działał w tej firmie itp. Fircjusz mu ufa.
* Vz:
    * Ania ma kompleks że nie jest dość dobra. Nie nadaje się. Nie jest tak dobra jak musi być.
        * Zdaniem Fircjusza jest wystarczająco dobra, nie jest wybitna.
    * Kacper ma długi. Żona puściła go z torbami i musi jej płacić.
* V: Brat jest tak zdesperowany, że dał dostępy do wszystkich danych siostrze. W końcu POWINNA mieć do nich dostęp jako część rodzinnej firmy. Ale - tylko do odczytu.
    * Nie ma żadnego brata czy kuzyna co chce przejąć firmę od Fircjusza.

CO WIECIE CO MACIE:

* Dane wskazują, że jedynych dwóch możliwych to Ania i Kacper to potencjalny WYCIEK danych -> żadne z nich nie ma biznesu by szkodzić firmie.
* Odkąd pojawiła się EnMilStrukt były odchylenia w lokalizacji jaszczurów
* EnMilStrukt ma inny sposób prowadzenia biznesu i część osób w mieście chciałoby zmiany ale większość nie
    * EnMilStrukt to duża firma, współfinansowana przez magów i która działa na "normalnych" terenach Verlenlandu
* Nadal nie wiadomo jak doszło do pojawienia się jaszczura
* OPINIA: Sekretarka Ania, to jej częściowo firma. Kacper ma długi -> potrzebuje kasy. Konkurencja opłaciła?

GDY Fiona gada z bratem Fircjusz idzie odwiedzić biuro ArachnoBuild. Ania go zatrzymuje "prezes jest zajęty". Fircjusz "ja tu poczekam". Siada w holu, nie odzywa się. Nie czeka na prezesa. Czeka na znajomą. Podpytuje o firmę. Ania - trzeba jej oddać - dobrze pełni rolę sekretarki. BARDZO DUŻO MÓWI ale nic ważnego. Gdy się przedstawił jako tien Samszar, ona USIADŁA NA BACZNOŚĆ. Pełen szok. TIEN chce zainwestować w firmę w okolicy?!

Fircjusz po namowie Ani by pokazać magię upiększa Anię. Dzięki temu chce by coś chlapnęła.

Tr Z (bo Fiona czyli chyba jeden z nas) M (bo magia) +3:

* V: Ania jest piękniejsza, rozświetlona wewnętrznie. Po chwili aż łza pociekła, bo to nie zostanie na zawsze.
    * (A: cicho) przepraszam, ale... czy mogę zobaczyć... jakiś... dowód, że nie masz powiązania z tamtą firmą? EnMilStrukt? -> dowód osobisty
    * KUPIŁEŚ Anię.
* Vm: Moc dotknęła Anię - nie tylko jest piękniejsza ale też jest wyczyszczona "w serduszku", puryfikacja. Ania się przyznaje. 
    * skontaktował się ze mną ktoś i powiedział, że mamy problem z danymi. Sprawdziłam to. I... (chwilę myśli) nie było problemu z danymi, ale je zmieniłam. Nie wiedziałam. To nie był błąd. Ktoś... nie wiem jak to się stało.
* V: Ania wyjaśniła wszystko pod wpływem puryfikacji
    * Ania sama wstydzi się niesamowicie - ona NIE JEST TAKĄ OSOBĄ.
    * Spotkała KOGOŚ kto do niej przyszedł, do domu. Ten ktoś powiedział jej, że firma upadnie i ma nowy model sprawdzający czy to wszystko zadziała.
        * Ania była PEWNA że to jest kuzyn Fircjusza. Ale teraz widzi, że to nie był on.
    * Ania wyciągnęła dane i przekazywała je kuzynowi. Ale to nie był kuzyn i ona nie była pewna - fakt czy sen.
    * Spotykali się przy kopalni, umówiony punkt - miejsce. Ona ma numer telefonu na który dzwoniła.

Strzała chce zlokalizować gdzie znajduje się ten telefon, przypisany do numeru na który dzwoniła Ania. Strzała nie chce zwrócić uwagi Verlenów, ale odpala pełną moc psychotroniczną. 

Tr Z (numer + zaskoczenie) +4 +5Og: 

* Vz: Strzała ma informację o tym, że 3 telefony były z tym numerem i WSZYSTKIE 3 skończyły gdzieś w okolicy 2km od kopalni. Są w różnych miejscach, "odrzucone". 
    * To wskazuje na to, że operacja została zakończona.
    * Strzała ma lokalizacje.
* V: Dane lokalizacyjne - jaki pokój, jaki hotel, gdzie był itp.
    * --> wyprowadzony link do ludzi zatrudnionych przez EnMilStrukt
* Og: "OMG CO TO ZA ATAK!" ze strony EnMilStrukt.
* X: Nie da się znaleźć TWARDYCH danych do sądu udowadniających że EnMilStrukt o tym wiedziało.
* Og: Verlenowie wiedzą o obecności zaawansowanej jednostki do walki psychotronicznej na tym terenie. Sentisieć alarmuje odpowiedniego Verlena.
* Vr: Mamy nazwisko maga: Julita Mopsarin. Jeszcze jest na tym terenie.

Julita POWINNA znajdować się w hotelu patrząc po mapie ciepła. Jedziemy Strzałą do Julity. ZANIM znajdzie nas Verlen...

Strzała leci z dużą prędkością do hotelu. DUŻĄ.

Tr Z (jesteś na miejscu a Verlenka nie wie gdzie jesteś) +3:

* X: MUSISZ np. wlecieć przez okno czy coś bo nie zdążysz
* V: Udaje się.
* X: Nie zgubi się Vioriki i ekipy
* Vz: Viorika musi polować na ścigacz.

Karolinus - wpadasz do pokoju Julity. Zaskoczenie + insercja.

Tr (niepełny) Z +2:

* VXV: Karolinus jest lekko pocięty ale ma pełne zaskoczenie na Julicie. I wpadasz w pozycji niebohaterskiej.
    * Julita w pełnym szoku

Karolinus korzysta z okazji pełnego szoku, zmienia nogi i ręce Julity w płetwy.

Tr Z (zaskoczenie) M +3 + 5Ob:

* Ob: Julita jest czarodziejką mentalną. Karolinus jest biomantą. Karolinus robi to dla Ani i Fiony. 
    * Julita się zmienia we Fionę wizualnie. Wygląda jak Fiona. I 15% dziewczyn w tym hotelu TEŻ wygląda jak Fiona (przez następne 24h)
* Ob: Ponad 30% populacji miasta płci dowolnej wygląda jak Fiona. Co najmniej jeden pekińczyk też.
* X: Julita jest ranteleportowana do hotelu. Wygląda. Jak. Fiona. Jak 30% populacji hotelu.
* Ob: KAŻDA "Fiona" poza oryginalną jest śmiertelnie zakochana w Karolinusie.

Strzała. Ma. Przechlapane. Odpala wszystkie subsystemy. Odrzucasz pancerz. By złapać i zgubić. I Strzała się nie bawi - zasłona dymna itp. ZGUBIĘ ich.

Ex Z +4 +2Or: (do trzech by sprawdzić wynik)

* XXX: 
    * Strzała przechwyciła Karolinusa, uciekli ale nie mieli gdzie się schować, nie ma bezpiecznego terenu WIĘC zostali przechwyceni.
* Vr: Strzała dała radę ich zgubić lecąc "w górę", nie musi wejść za wysoko ale IIF (Samszar) sprawia że Verlenowie nie robią nic głupiego.
    * Viorika faktycznie została zgubiona; musiała oderwać się od Strzały by niczego nie zniszczyć i rozwiązać Problem Tysiąca Fion.

## Streszczenie

Karolinus Samszar przybywa na prośbę swej guwernantki Fiony by pomóc w jej rodzinnej firmie ArachnoBuild, która ryzykuje utratę swojej pozycji na rynku i która jest w Verlenlandzie. Pojawił się Potwór przez błędy w działaniach ArachnoBuild. Karolinus dowiaduje się od barmanki Laury o konkurencji - firmie EnMilStrukt, finansowanej przez Brunhildę Verlen. EnMilStrukt ma inne podejście do prowadzenia biznesu (nie harmonia z naturą a dominacja natury) i zyskuje poparcie części mieszkańców.

Fiona i Karolinus dochodzą do wniosku, że jedynymi osobami, które mogły wyciec dane, są Kacper i Ania. Dane wskazują na odchylenia w lokalizacji jaszczurów od czasu pojawienia się EnMilStrukt. Fiona rozmawia z bratem Fircjuszem, który ujawnia swoje uczucia do Ani, a Kacper okazuje się być starym przyjacielem rodziny. Ania ma kompleks, że nie jest wystarczająco dobra, a Kacper ma długi.

Pod wpływem magii puryfikacji Karolinusa, Ania przyznaje, że spotkała kogoś, kto twierdził, że jest kuzynem Fircjusza, i przekazywała mu dane (magia mentalna). Miejsce spotkań to kopalnia, a numer telefonu jest przypisany do ludzi zatrudnionych przez EnMilStrukt.Strzała próbuje zlokalizować telefon, ale nie udaje się zdobyć wystarczających dowodów, aby udowodnić, że EnMilStrukt miało wiedzę o spisku. Za to Karolinusowi udało się dotrzeć do wrogiej czarodziejki i Paradoksem zmienił populację większości miasta w Fionę XD. Viorika Verlen próbowała złapać Karolinusa, ale Strzała może lecieć w stratosferę XD.

## Progresja

* Karolinus Samszar: niesamowicie zdrażnił Viorikę Verlen i spalony w miasteczku Arachnoziem. Specjalnie dla niego, Viorika zasponsorowała upokarzający pomnik.
* Fiona Szarstasz: spalona w rodzinnym miasteczku, acz odzyskała sympatię brata i rodziny z Arachnoziem.

### Frakcji

* .

## Zasługi

* Karolinus Samszar: (młody (20) panicz który chce pomóc rodzinie Fiony, bo miłość jest ślepa. Biomagia + magia Samszarów. Tien.) Poszukuje informacji w knajpie "Łeb jaszczura", rozmawia z barmanką Laurą i spuryfikował Anię, dzięki czemu poznał info o tajemniczej czarodziejce w służbie EnMilStrukt. Potem wszedł z nią w starcie i jakkolwiek ją miał, to rzucił zaklęcie i Paradoksem doprowadził do tego, że większość miasta wygląda jak Fiona i jest w nim zakochana. Skutecznie zwiał Viorice używając Strzały.
* Fiona Szarstasz: (eks-verlenlandrynka; ma niesamowity talent do wykrywania kłamstwa ale też świetnie kłamie, teraz: guwernantka Karolinusa). W firmie brata odkryła sabotaż i pomogła mu odbudować sytuację. Test słabego ogniwa i zawężenie do Kacpra i Ani.
* AJA Szybka Strzała: (Autonomiczna Jednostka Aurum; zamaskowana eks-ŁZa militarna która podczas wojny uratowała życie Samszara i teraz jest sprzężona z duchem. ADVANTAGES: walka elektroniczna, walka, skan, transport, hidden.) Zlokalizowała telefon używany przez ludzi zatrudnionych przez EnMilStrukt a potem szybko dostarczyła Karolinusa do lokalizacji wrogiej czarodziejki. Skutecznie zwiała Viorice lecąc w stratosferę.
* Fircjusz Szarstasz: właściciel ArachnoBuild, próbuje uratować swoją rodzinę i firmę. Ma się ku Ani (sekretarki) i ogólnie wykonuje pracę dobrze. Ufa swoim pracownikom i dobiera takich że są godni zaufania.
* Laura Turabnik: barmanka w "Łeb jaszczura", opowiada Karolinusowi o problemach miasta i firmie EnMilStrukt. Reprezentuje pokolenie które chce zmian i woli wygodne życie ponad harmonię. Jest głośna i kłótliwa.
* Ania Turabnik: sekretarka w ArachnoBuild i dziewczyna Fircjusza (brata Fiony), podejrzana przez Fionę o zmianę planów, okazuje się, że przekazywała dane nieświadomie pod wpływem magii mentalnej. Spuryfikowana przez Karolinusa.
* Kacper Aczramin: stary przyjaciel rodziny, pracownik ArachnoBuild, ma długi (rozwiedziony i alimenty), potencjalny wyciek danych. Lojalny ArachnoBuild i podejściu "harmonia nade wszystko".
* Julita Mopsarin: czarodziejka mentalna zatrudniona przez EnMilStrukt by ArachnoBuild nie uzyskał kontraktu. Jakkolwiek osiągnęła cel, Karolinus ją dopadł i magią zmienił ją w Fionę (jak i pół miasta). W chaosie dała radę się wymknąć.

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Aurum
                        1. Verlenland
                            1. Arachnoziem: słynie ze złóż minerałów, niestabilności i wyjątkowo podłych podziemnych potworów; ma garnizon 50 żołnierzy na 20k populacji rozsianych po górzystym terenie. Starcie lokalnej firmy ArachnoBuild i EnMilStrukt.
                                1. Kopalnia
                                1. Bar Łeb Jaszczura

## Czas

* Opóźnienie: -5
* Dni: 2

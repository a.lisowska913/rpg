---
layout: cybermagic-konspekt
title: "Okrutna Wrona, kalcynici i koszmary"
threads: morze-ułud
gm: żółw
players: kić, fox
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [220427 - Dziwne Strachy w Morzu Ułud](220427-dziwne-strachy-w-morzu-ulud)

### Chronologiczna

* [220427 - Dziwne Strachy w Morzu Ułud](220427-dziwne-strachy-w-morzu-ulud)

## Plan sesji
### Co się wydarzyło
#### Co się wydarzyło w tym terenie?

* Inicjacja bazy Ukojenie Barana
    * mag eternijski, Bruno Baran, założył niewielką bazę daleko od Eterni z częścią ludzi 12 lat temu
    * automatycznie zaczął się kręcić profit
    * wśród górników Elsa Kułak została nieformalną liderką. Niestety, "demokracja" nie przypasowała Baranowi. Odizolował się.
* Migracja Mardiusa
    * noktiańscy komandosi Tamary Mardius złożyli tu swój nowy dom wśród lapicytu. Podeszli do górników i Barana z neutralnością. Ot, są.
    * mało kto jak noktianie umieją żyć w kosmosie. W tym miejscu znaleźli świetne miejsce i ich umiejętności stały się bardzo przydatne.
    * siły Mardius znalazły opcję szmuglowania przez statek Gwiezdny Motyl.
* Imperium Blakvela
    * 3 lata temu pojawił się eternijski szlachcic, Ernest Blakvel. Stwierdził, że władza w kosmosie to coś dla niego. Zaczął umacniać się w tym terenie.
    * Blakvel i Mardius weszli w stały konflikt. Mardius chroni miejscowych, Blakvel chce ich podbić.
* Pojawienie się Strachów
    * 2 lata temu pojawiły się Strachy. Stworzone z programowalnej materii (morfelin), nie wiadomo czemu są i czym są.
    * Mardiusowcy aktywnie chronią lokalnych.

.

* Zespół dał radę kupić lokalizację dziwnego sygnału - znajduje się tam Tajemniczy Derelict Ship. Kapitan podjął decyzję, by Wąż poleciał.
    * Ten okręt zawiera Hipermutator Emisyjny (anomalia), dość sprawną TAI, interesujący ładunek biologiczny oraz neuroobroże Syndykatu Aureliona.
    * Była informacja o niszczeniu morale przez różnego rodzaju zagrożenia i problemy
* Zatrudniono advancera - Berdysza. Nie wiedzą kim Berdysz jest. Gaulron-advancer z wystarczająco dobrym sprzętem.
* Wąż odleciał w kierunku na Derelict Ship.

#### Strony i czego pragną

* Alan 
    * jest agentem Syndykatu Aureliona. Ceni siłę, bezpieczeństwo i hedonizm, ale nieważne jak go patrzą i pomoc innym. Niecierpliwy i praktyczny, motywowany nagrodami i beztroski. Inspektor sanitarny i elektronik. Chce zwiększyć wpływ Syndykatu. Pragnie znaleźć derelict ship i sprzedać znajdujące się tam Obroże Neurokontroli Blakvelowcom. 
    * CEL: sprzedać obroże.

* Pola 
    * najważniejsze są jej osiągnięcia, niezależność biznesowa oraz czysta przyjemność. Niewrażliwa na stres i praktycznie nie motywowana niczym, woli się zgodzić z drugą stroną niż się pieprzyć. Szuka mistycyzmu i rzeczy nadnaturalnych, pragnie zostawić na boku ten bezwartościowy świat. Ewentualnie - komfortowego życia. Badaczka szukająca wśród Strachów iskry bogów. 
    * CEL: interakcja i integracja z absolutem.

* Maja 
    * nienawidzi osób fałszywych. Tradycyjne podejście i akumulacja mocy sprawiają, że Maja osiągnęła niemały poziom znaczenia. Wszystko pragnie zawłaszczyć i rządzić dominacją i terrorem - by wreszcie wszyscy żyli w prawdzie. Jednocześnie znajduje piękno w gospodarce roślin i czystości ekosystemu. Robi za lekarza i kontrolę biologiczną Węża. 
    * CEL: przejęcie władzy

* Kornelia 
    * pragnie się dopasować do grupy i z radością buduje wspólną przyszłość nieważne czy dla ludzi czy AI. Przyjmuje każdego, nieważne z jakiej frakcji. Nie dba o swoje zadowolenie czy emocje. "Dobra mama" grupy. Zajmuje się załadunkiem i sprzedażą dóbr. 
    * CEL: pragnie Wojciecha (jakiegoś Blakvelowca) jako swojego soulmate.

* Filip 
    * jest skupiony na dokonaniach, pokorze i tradycji. Nie dba o stymulację czy pomaganie innym. Złośliwe i wścibskie bydlę, ale niesamowicie pracowity. Kiedyś już zdradził; nie powtórzy się to ponownie - poprzysiągł dbać i chronić interesy. 
    * CEL: zdobyć najlepsze możliwe narzędzia dla Wielkiego Węża.

* Antoni 
    * zna swoje miejsce w rzeczywistości. Skupia się na zabezpieczeniu Wielkiego Węża i na tym by nikomu na pokładzie nic się nie stało. Uratowany z niewoli piratów, pochodzi z zupełnie innego miejsca. Jest jak ryba w wodzie, ale jest świetnym inżynierem i konstruktorem dron, zwłaszcza badawczych. Nienawidzi piratów w każdej formie. 
    * CEL: wrócić do domu. Chwilowo: zabezpieczać Węża.

* Lila 
    * nie dba o tradycję ani bezpieczeństwo - ale skupia się na sukcesach, autonomii i stymulacji. Zostaw ją w spokoju i sumienna (choć nieprzyjemna w obyciu) Lila zapewni by wszystko zadziałało. Appraiser, neurosprzężona wbrew swojej woli i ucieka w używki by nie czuć wiecznego szeptania uszkodzonego AI. Główny pilot i serce maszyny. Wyjątkowo drażnią ją małe głupoty i "szczury w piwnicy" ;-). 
    * CEL: usuwać przeszkody, zapewnić, by coś takiego nigdy nikogo innego nie spotkało.

* Berdysz 
    * jest straszliwym kapitanem piratów. Dołączył do załogi jako advancer. Jest gaulronem, dewastatorem i świetnym planerem. Ceni sobie władzę i przyjemność; nie ma nic przeciw by się podzielić ;-). Jest w stanie pełnić prawie każdą rolę.
    * CEL: skorzystać z okazji, wrócić do bycia kapitanem piratów. Kiedyś.

* 3-4 innych nieznanych NPCów.

* Ksawery Janowar
    * Wartości
        * TAK: Conformity, Power, Self-Direction
        * NIE: Hedonism, Security
    * Ocean
        * ENCAO:  +--00
        * Stanowczy i silny, zdecydowany
        * Niechlujny, nie dba o higienę i porządek
        * Aktywny, wiecznie coś robi
    * Silniki:
        * TAK: Cancel Culture: wykluczenie kogoś przez grupowy ostracyzm. Wszyscy się dopasują do mojego świata i wersji świata / kultury.
        * TAK: Rite of passage: by dziecko stało się dorosłym, musi dokonać Wielkiego Czynu. Zdobyć czyjeś ucho, zabić potwora...
        * NIE: Tainted Love: głód, pożądanie lub miłość. Obsesja. Pragnie być z tą osobą lub ją mieć. Podziw i pragnienie. Będzie MOJA.
            * interpretujemy: miał stalkerkę która zniszczyła mu życie i zmusiła do pójścia w asteroidy
    * Rola:
        * logistyk; żyjemy na jedzeniu
* Mikołaj Faczon
    * Wartości
        * TAK: Humility, Power, Hedonism
        * NIE: Tradition, Conformity
    * Ocean
        * ENCAO:  -0-0+
        * Nieuczciwy, z tendencjami do oszukiwania
        * Introspektywny, skupiony na głębokim wnętrzu
        * Romantyczny, szukający uniesień i emocji
    * Silniki:
        * TAK: Tigańczyk: członek wyklętej i zapomnianej grupy, nadal skrajnie jej oddany i lojalny. Kiedyś zemszczą się na Niszczycielu mimo, że są underdogiem.
        * TAK: Utopia Star Trek: znajdowanie porozumienia i dobra nawet w osobach bardzo różnych od nas. Współpraca i koegzystencja.
        * NIE: Wrócić do ukochanej: byłem głupi i ją straciłem. Zrobię wszystko, by móc do niej wrócić.
            * interpretujemy: kobietożerca, szowinista, "Wszystkie są X ale ta jedna której nie poznałem", don kichot pod kątem JEDNEJ NIEZNANEJ KOBIETY
    * Rola:
        * obserwator radarów / energii
* Amanda Korel, CORRODED SOUL
    * Wartości
        * TAK: Achievement, Power (devour, destroy, top score)
        * NIE: Benevolence
    * Ocean
        * ENCAO: +-+-+ (lubuje się w różnorodnej anihilacji)
    * Silniki
        * Take Over The Ship, saw chaos and panic
    * Rola
        * corroded soul, kiedyś: niewolnica z neuroobrożą.

### Co się stanie

* Morale graczy będzie dewastowane przez halucynacje, Strachy
* Spotkanie ze Strachami
* Inny Derelict Ship który jest przez coś spustoszony i przeżarty
    * rozpada się
* emisja anomalna na pokładzie Węża 

### Sukces graczy

* .

## Sesja właściwa
### Scena Zero - impl

.

### Sesja właściwa - impl

Maja przejęła dowodzenie w sprawie Berdysza. Nadal za kapitana uważana jest Kornelia, ale Maja jest kapitanem kryzysowym.

Następnego dnia Lila doprowadziła statek bezpiecznie w okolice statku pirackiego. Statek piracki wygląda ŹLE. Uszkodzony od silników, uszkodzony kadłub... ogólnie, jest pocięty. Niesprawny, wyłączony. I od razu dyskusja.

* Kornelia: "ratujemy, ostrożnie. Żeby nas nikt nie zobaczył"
* Filip: "jesteśmy łatwo widoczni. Już nas dostrzegli"
* Antoni: "powinniśmy stąd się zmywać. To tylko piraci."
* Kornelia: "może mają kogoś na pokładzie. Okup."
* Lila - potwierdzasz, że Wrona ma połączoną mniejszą jednostkę. Kogoś porwali? Wygląda w miarę nowocześnie.
* Pola: "nie spotkałam się z czymś takim w kosmosie. Głębniaki? Musimy to zbadać, czy to zaraźliwe, czy to zagraża naszym koloniom"
* Antoni: "zaraźliwe?"
* Kornelia: "to nie jest zaraźliwe, sprawdziliśmy"
* Pola: "tym bardziej musimy zbadać"

Alan, próbuje dojść na cichej komunikacji od Berdysza:

TrZ+2:

* X: ANTONI COŚ zauważył
* V: Alan dostał odpowiedzi
    * Wrona powinna mieć na pokładzie "cywili". Kapitan Wrony ma kilka osób które mają neuroobroże. Niewolników. Można ich uratować.

Antoni: A SKĄD TY TO WIESZ.

* X: Alan próbuje się bronić. TAK WIEM OD BERDYSZA!

Antoni "ZROBIĄ Z NAS NIEWOLNIKÓW" Lila "chciałbyś, coś taki ucieszony?" Antoni z pięścią na Lilę (był niewolnikiem). Alan ją chroni i wykręca rękę Antoniego. Antoni na kolanach "NIE ROZUMIECIE TE OBROŻE TO ZŁO". Maja próbuje opanować sytuację i przejąć kontrolę.

TrZ+3:

* V: Maja używa _hold_ na załogę - ratujemy ludzi. Mamy kodeks kosmiczny. Musimy pomagać sobie nawzajem. Ratujemy tamtych ludzi.
* V: dobrze że mamy Alana bo byśmy nie wiedzieli. Antoni to zaakceptował.

Berdysz się uśmiechnął, założył swój skafander, sprawdził czy szpony działają, uśmiechnął się bo zabił głębiaka... 

Antoni pod pomysłem Mai montuje w 2-3 małych dronach scavengerowych (szuka czy we wrakach coś jest warotściowego) AI typu "seek and return". Plus opcje omijania kłopotów.

TrZ+3:

* XX: dwie drony będą stracone
* V: jedna drona wróci i poinformuje o sytuacji

Berdysz bierze trzy drony i leci. Dotarł do pintki i zaczyna insercję.

TrZ+3:

* X: głębniak zniszczony, Druga Strona poinformowana
* V: drona przeszła i wróciła
* V: Berdysz wrócił w pintce

Berdysz rozerwał głębniaka na kawałki. "Śmierdzi rybą." Dwa.

Pintka jest wyraźnie uszkodzona od środka. Nie chodzi o same głębniaki - wszystko wskazuje, że głębniaki próbowały zniszczyć pintce wszystko do poruszania się. I to jest dziwne. I TAK, w okolicy Morza Ułudy Strachy są inne. Filip szczurzy co można wykorzystać. A Lila szuka lootu. Appraisal pintki jaką jest. TAK, Lila musiała zmusić Filipa by się podpiął do czego się da. Filip jęczymorda narzeka ale podpina.

ExZ+4:

* V: Lila masz ciekawe informacje:
    * Pintka nazywa się "Groźna Zjawa" i należy do rodu Piscernik. A dokładniej, do Elwiry Piscernik. Przechwycona przez piratów.
        * Piscernikowie zapłacą za pintkę. Pintka sama nie odleci - ale my możemy go wziąć.
* (+2Vg) V: Lila ma dostęp do resztek logów. Nie udało się głębniakom zniszczyć wszystkich.
    * Lila widzi stare dane, logi, nagrania. 
        * "SEKRET TEGO CO JEST NA TALIO I ONA ELWIRA SIĘ TEGO DOWIE Z DERELICT"
        * Pintka w rękach piratów. Lila widzi jak jeden z piratów próbuje uruchomić pintkę i włączył nagrywanie.
            * Pintka została przez piratów przechwycona parę dni przed sygnałem SOS.

OPERACJA RESTAURACJA NA TALIO! Oki - wymontujemy z pintki mechanizm syntezy żywności używany kiedyś przez Elwirę Piscernik i zamontujemy na naszej jednostce. I mamy najlepsze jedzenie ever jak się uda to utrzymać i zasilać substratami. Filip się totalnie rozmarzył: Maja, Lila mają +1000 od niego.

...tymczasem, drona wróciła i Berdysz z radością przekazał ją Antoniemu. Który dalej chowa się za Filipem na widok Berdysza.

Lila skanuje informacje z drony - węższe przejścia, zaułki, co tam jest, jak statek wygląda, uszkodzone przejścia...

TrZ+2:

* X: Niestety, żadna drona nie przetrwała operacji. Ale mamy dane.
* V: Mamy rozkład i informacje o gdzie są głębniaki, jakie są itp. Plus rozkład.
* XX: Berdysz opracuje plan, ale Alan zostaje ciężko ranny w starciu z głębniakami. (grupa szturmowa to Alan, Berdysz i Filip)
* (+2Vg) V: Statek piracki został oczyszczony ze wszystkich kilkunastu głębniaków. Łącznie z tymi ukrytymi.

Pola robi badania nad poległymi głębniakami. Z uwagi na swoje zainteresowania ezoteryczne.

TrZ+3:

* Xz: OBSESJA NA PUNKCIE PRAWDY. Ona zrozumie jak takie anomalie mogą chodzić po świecie.
* V: Pola ma cenne informacje dla Mai i Kornelii
    * Głębniaki: tech utrzymuje ludzkie tkanki przy życiu. Ani żywe ani martwe. Super wydajne. Nie ma systemów kontrolnych. Nie ma komunikacji. To drony, nie wiemy czym jest królowa.
    * Jak było ich więcej, udało im się zrobić taktykę i Alan zapłacił. Berdysz i Filip są teraz z Alanem w medycznym statku pirackiego.
    * Pola pokazała rękę jednego z tych głębniaków - ręka na tatuaż Piscerników. RĘKA -> ZAMRAŻARKA.

NA STATKU PIRACKIM najważniejsza rzecz: cela. Krwawa łaźnia piratów. Nie ma ani jednego żywego pirata. Głębniaki robiły podejścia taktyczne, manewry, atak, flanka, zabicie. Piraci nie mieli szans. Ale piraci nie walczyli jak powinni - Berdysz + 2 osoby dały radę zniszczyć kilkanaście. Ale kilkunastu piratów nie dało rady pokonać broniących się głębniaków. Ale też np. w kosmosie są trupy piratów bez skafandrów. Jeden pirat zginął ze swojej ręki. Czyli... piraci walczyli nie tylko z głębniakami. Coś jest tu nie tak.

...pasożyt memetyczny?

Lila ryzykuje pojawienie się Strachów, ale chce włączyć sensory i skanery. I ZNOWU - głębniaki niszczyły wszelke dane. Silniki. KOmputery. By nie dało się... dowiedzieć? Poznać kursu? Odlecieć?

FILIP I ANTONI CIĄGNĄ DRUTA - wielki kabel z Węża na Wronę. I Lila odpala czujniki by ostatecznie Strachy przekierować na TĘ jednostkę. Chronić swoją.

ExZ+3+3Vg:

* V: Lila postawiła czujniki. Są w kosmosie KAPSUŁY ratunkowe. Większość przebita. Jedna nie. Ale jest wyłączona i martwa. Tamte przebite nie były martwe.
    * -> Berdysz to naprawi. Zwłaszcza że nie jest lekarzem a chcemy by Alan przeżył. Pola pomoże Alanowi.
* V: Lila dobiera się do logów. Jej neurosprzężenie wyjątkowo pomaga.
    * Koleś wychodzi przez śluzę - czyli jakiś 'kill agent' memetyczny tam jest
    * Kapitan bełkocze "ONA TU JEST! ONA NA MNIE PATRZY! NIE POZWÓLCIE JEJ... NIE PODCHODŹ!" zanim sam się zastrzelił. Głębniaki do niego się nie dobiły.
        * CZYLI KAPITAN PRZEŻYŁ STARCIE Z GŁĘBNIAKAMI.
    * Fragment bitwy: głębniaki działają jak oddział, ale nie do końca. Ale piraci walczą źle. Piraci walczą "z duchami". Widzą "więcej".
    * W pewnym momencie panikujący pirat wstał, wziął broń i zaczął eksterminować głębniaki. Tak beznamiętnie. W końcu go dorwały i gdy tylko stracił broń wrócił do paniki i AAAA!!
    * W ramach inwentaryzacji Lila w logach wyciągnęła informacje o czterech młodych damach zamkniętych, z obrożami neurokontroli. "Własność kapitana". Jedna żyje do teraz. Jest uśpiona medycznie - dawka strzykawki.

Więc udało się przechwycić siedem obróż neurokontroli. A ósma jest na naszej "pacjentce". Pacjentka zidentyfikowana jest jako Amanda Korel. Amanda jest dziewczyną z dobrego domu która swego czasu zniknęła. Kilka miesięcy temu.

Jak zginęli niewolnicy. Forensics. Maja najlepszy medyk bo zajmuje się grzybami i jedzeniem...

TrZ+3 (niepełne):

* X: Charakterystyczne, że jedna z nich zginęła od piratów. Jedna od głębniaków. Jedna została przedawkowana. A ta jedna - Amanda - jeszcze żyje.
* X: Coś poruszyło się NAD Kornelią i Mają. Coś jest w suficie. ODSKOCZYŁY. GŁĘBIAK.
* (+2Vg) X: Kornelia i Maja ściągają ogień na siebie i wieją przed głębniakiem. Jedna ucieka, drugia tauntuje. By stwór nie mógł gonić obu. PLAN DOBRY. Ale Kornelia gdy ucieka to Maja strzela w głębiaka. I trafiła. I przebiła na wylot. Kornelia jest ranna.
* (+2Vg) V: Maja się orientuje że jego tam nie ma. ALe i Maja i Kornelia go widzą. Ale nie słyszysz.
* (+2Vg) Maja: "NIE STRZELAJ TO KORNELIA! TEN POTWÓR NIE ISTNIEJE!" X: Kornelia biegnie, płacze, Maja "TEGO POTWORA NIE MA". Pola nie strzela. Kornelia - załamała się. Płacz, panika.

Pola bierze Kornelię na Węża. A KORNELIA TO INFEKTANT.

Rozpatrzmy kto gdzie jest:

* Kornelia, Maja i Pola wracają na Węża.
* Lila jest na Wężu, monitoruje dane z Węża.
* Berdysz jest w kosmosie, holuje kapsułę na Wronę.
* Alan jest w medycznym na Wronie. Zajmuje się nim Filip i Antoni, ponieważ skończyli z kablem

KONFLIKT TRUDNY+2:

* X: Pola jest trafiona z pistoletu, pada na ziemię. Kornelia przycelowuje ponownie.
* X: Lila włącza dopalacz. Strukturalne uszkodzenia Węża, Kornelii wypada pistolet. Pola prześlizguje się po ziemi. Maja łapie się czegoś rozpaczliwie.
* (+2Vg+Z): V: Maja obezwładnia częściowo Kornelię. Wali w "szczepionkę" (ranę). Kornelia krzyczy z bólu. Ma normalne ludzkie reakcje. (+2Vg)
* V: (+2Vr, to jej domena) Maja leży na Kornelii i ją może bić. MAJA BIJE I MÓWI KORNELII CO O NIEJ MYŚLI. Opętanie minęło.

Maja: "Kornelio, weź się w garść". Jeśli osoba straci morale i się załamie staje się podatna. KORNELIA W KAJDANY. (Maja: użyjemy obroży...)

Lila -> Berdysz "ratuj Filipa i Antoniego". Berdysz próbuje uratować obu. Na pełnej mocy.

ExZ+3:

* X: Antoni jest pod WPŁYWEM. Antoni zabija Alana.
* V: (+1Vg) Berdysz dał radę uratować na pewno Filipa.
* V: Filipowi nic nie będzie. 
* V: Berdysz naprawdę chciał zabić Antoniego. Dla funu. Ale nie. Rozbroi go tylko. Bo może. Bo kontroluje sytuację. I przejmuje dowodzenie. I może wsadzić w obrożę.

Berdysz do Antoniego: "Wiesz, że to mój dobry dzień".

Berdysz przejmuje dowodzenie na Wronie. Chce ewakuować Filipa + Antoniego w obroży + odzyskać Amandę.

TrZ+3:

* V: Berdyszowi nic się nie stanie. Przejdzie bezpiecznie.
* V: Filip wróci nietknięty
* X: Berdysz uznał, że Amanda jest zbyt niebezpieczna by się przebijać przez ewentualne głębniaki itp.
* X: Antoni też nie ma szczęścia. STRASZNA TRAUMA bo trafia do kapsuły z uratowanymi piratami i oni (nie Filip) mają pilota.
* V: Berdysz przesterował reaktor i wysadził ten statek.

Lila: "Może Amanda jest potencjalnym nośnikiem ale jest źródłem szczepionki. Potrzebujemy i Wronę i Amandę.". Berdysz wydał dźwięk pomiędzy warknięciem i niszczeniem ściany. Wyciągnie ją. I faktycznie - wyciągnął ją. I stracił okazję zniszczenia statku jak zaroiło się głębniakami. Które nie skupiają się na Filipie czy Berdyszu. Raczej lecą w kierunku na Węża - na silniki.

Berdysz "you are fucking kidding me".

MAJA WŁĄCZA BOJĘ. Wyłączają Węża. Niech będą tu Strachy. Niech strachy vs głębniaki. A Wąż siedzi cicho + lekki impuls z silnika dla mikro ruchu.

Wpierw - rozpaczliwa Lila stawiająca boję.

ExZ+3:

* V: Udało się ściągnąć boję ZANIM Berdysz opadł z sił. Pojawiają się Strachy.
* V: Boja ściągnie Strachy i wszystko zadziała tak jak powinno. Berdysz spożyje kapitana dla energii.

Udało się wyłączyć Węża i puścić statek. Maja, jako najbliższa lekarzowi, stawia odpowiednio obroże dla Kornelii i Antoniego na wysoką dopaminę i morale. I Amandy też.

TrZ+2+3Vg za porady piratów:

* V: Obroże dają radę. Kornelia i Antoni nigdy nie byli tak słodcy i rozmarzeni.
* X: Po tej operacji wszystkich (poza Berdyszem) czeka leczenie traumy.
* X: Maja NIGDY nie będzie lubiana. Będzie kapitanem, ale nigdy nie będzie kochana. Nie ma cudów. Jej los jest jak Berdysza.
* X: Berdysz jest brutalną siłą która przesuwa wszystko do przodu. ZWŁASZCZA że nie chce zagrażać koloniom w kosmosie.

Wąż przetrwał. Wrócił "bezpiecznie" do domu. TAK, skończy się restauracją. Po tym wszystkim - tylko restauracja...

## Streszczenie

Inflitracja Wrony i zniszczenie kalcynitów kosztowało ciężką ranę Alana. Niestety, na pokładzie oprócz kalcynitów są też jakieś byty opętujące; to sprawiło, że Kornelia prawie zastrzeliła Polę. Stracili Alana i dwie osoby są BROKEN. Udało się im uratować dwóch piratów i uciec z tego przeklętego miejsca. Mają jednak informacje o Elwirze Piscernik (i jej pintkę), uratowali Amandę Korel i NIGDY już nie wrócą do Morza Ułud - można zawsze żyć jako restauracja...

## Progresja

* Pola Mornak: obsesja na punkcie PRAWDY. Co tu się dzieje? Czym są głębniaki / kalcynici? Musi się dowiedzieć czym jest ryzyko i na czym polega.
* Antoni Krutacz: straszliwa trauma - zabił Alana pod wpływem opętania a potem neuroobroża. 
* Maja Kormoran: zostaje kapitanem Węża i tej załogi.
* Berdysz Rozpruwacz: wszyscy (Maja, Lila, Pola, Kornelia, Filip, NAWET Antoni, Ksawery i Mikołaj) uważają go za tego kto przeprowadził ich przez piekło. Ma czystą współpracę z agentami Węża.

### Frakcji

* .

## Zasługi

* Maja Kormoran: z Kornelią robi forensic martwych niewolnic i odkryła, że jeden z kalcynitów jest halucynacją. Potem obezwładniła Kornelię i wsadziła jej neuroobrożę. Doszła do tego co jest konieczne do opętania.
* Lila Cziras: wraz z Filipem zrobiła appraisal pintki i odkryła link do Elwiry Piscernik; czujnikami i skanem logów doszła do tego co się stało na Wronie - "upiór"?. Włączyła dopalacz, ratując życie Poli przed opętaną Kornelią i uszkodziła strukturalnie Węża. Przekonała Berdysza, że Amandę TRZEBA ratować. Wyprowadziła Węża z piekła...
* Alan Falkam: współpracuje ściśle z Berdyszem, przekazując dane od niego Zespołowi dyskretnie; ciężko ranny w operacji szturmowej na Wronę. Zastrzelony przez Antoniego. KIA.
* Pola Mornak: naukowiec; świetnie doszła do tego mniej więcej czym są kalcynici / głębniaki i korelację z Elwirą Piscernik. Została solidnie ranna, ale aktywna.
* Kornelia Sanoros: nie wytrzymała napięcia; gdy "kalcynit" spadł z sufitu i została ranna przez Maję to się poddała i została Zainfekowana. Po krótkiej walce na Wężu wsadzili jej neuroobrożę by była milutka. BROKEN.
* Filip Gościc: appraisal pintki z Lilą, po czym wymontuje z pintki SYNTEZATOR ŻYWNOŚCI PISCERNIKÓW. Uczestniczył w operacji szturmowej na Wronę z Alanem i Berdyszem. 
* Antoni Krutacz: niesamowicie uważny, zauważył że Alan dyskretnie współpracuje z Berdyszem; dzięki jego dronom mają wgląd w sytuację na Wronie. Opętany, zabił Alana i Berdysz założył mu neuroobrożę. BROKEN.
* Berdysz Rozpruwacz: wpadł na pintkę, rozwalił kalcynita i wrócił z pintką po wprowadzeniu dron; potem opracował jak wbić na Wronę i wyczyścić kalcynitów. Nie zdążył uratować Alana przed opętanym Antonim ale uratował Filipa i wsadził Antoniemu neuroobrożę. Użył eks-piratów z Wrony do upewnienia się, że sytuacja dalej jest pod jego kontrolą. Taktyka ORAZ masterful strategy. Uratował tyle ile się dało.
* Ksawery Janowar: eks-pirat z Okrutnej Wrony; został piratem bo uciekł przed stalkerką. Słucha się Berdysza i zapewnił, że Wąż nie odleciał bez nich.
* Mikołaj Faczon: eks-pirat z Okrutnej Wrony; szowinista który jednak słucha Mai bo Berdysz kazał. Doskonały strzelec; osłaniał Berdysza, gdy ten chronił silniki Węża przed kalcynitami.
* Amanda Korel: neuro-niewolnica z Okrutnej Wrony; to ona była wektorem przez który weszło Opętanie. Nieprzytomna, z bogatej i ważnej rodziny - uratowana przez Zespół a za jej znalezienie jest nagroda.
* Elwira Piscernik: WIELKA NIEOBECNA. Jej pintka była na Okrutnej Wronie. Wiemy, że leciała odkryć sekrety linku planetoidy Talio i Morza Ułud. Nie wiemy co się z nią stało i gdzie jest. MIA.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Pas Teliriański
                1. Planetoidy Kazimierza
                    1. Morze Ułud: nie tylko "opętania" ale i kalcynici (głębniaki). Te frakcje się zwalczają. I tajemniczy Derelict Ship, do którego Wąż nie dotarł.

## Czas

* Opóźnienie: 2
* Dni: 9

## Konflikty

* 1 - Alan, próbuje dojść na cichej komunikacji od Berdysza. Przekazać ważne info odnośnie Wrony o którym nikt inny nie wie.
    * TrZ+2
    * XVX: Antoni zauważył że Alan ma komunikator, Alan powiedział ważne fakty odnośnie Wrony - o cywilach. Musiał przyznać - wie od Berdysza.
* 2 - Alan "ZROBIĄ Z NAS NIEWOLNIKÓW" Lila "chciałbyś, coś taki ucieszony?" Antoni z pięścią na Lilę (był niewolnikiem). Alan ją chroni i wykręca rękę Antoniego. Maja próbuje opanować sytuację i przejąć kontrolę.
    * TrZ+3
    * VV: Maja decyduje, że ratują LUDZI. I trzeba to zaakceptować. Plus - gdybyśmy nie mieli Alana to byśmy nie wiedzieli. Antoni backs down.
* 3 - Antoni pod pomysłem Mai montuje w 2-3 małych dronach scavengerowych (szuka czy we wrakach coś jest warotściowego) AI typu "seek and return". Plus opcje omijania kłopotów.
    * TrZ+3
    * XXV: Straci 2/3 drony, trzecia drona wróci i poinformuje o sytuacji.
* 4 - Berdysz bierze trzy drony i leci. Dotarł do pintki i zaczyna insercję. Odzyskał tajemniczą pintkę.
    * TrZ+3
    * XVV: zniszczył kalcynita i obudził Drugą Stronę. Ale wrócił z pintką.
* 5 - A Lila szuka lootu. Appraisal pintki jaką jest. TAK, Lila musiała zmusić Filipa by się podpiął do czego się da. Filip jęczymorda narzeka ale podpina.
    * ExZ+4
    * VV: Pintka to "Groźna Zjawa", należy do Elwiry Piscernik. Lila ma echo logów. To wszystko jest powiązane z Talio. Piraci przechwycili pintkę parę dni przed tym SOS z boi.
* 6 - Lila skanuje informacje z drony - węższe przejścia, zaułki, co tam jest, jak statek wygląda, uszkodzone przejścia...
    * TrZ+2
    * XVXXV: nie mamy już żadnej drony, szturm Alan Filip Berdysz, Alan zostaje ciężko ranny, oczyszczona Wrona z kalcynitów.
* 7 - Pola robi badania nad poległymi głębniakami. Z uwagi na swoje zainteresowania ezoteryczne.
    * TrZ+3
    * XzV: ma cenne informacje - między innymi to, że część kalcynitów pochodzi ze statku Elwiry Piscernik. Ale ma obsesję na punkcie PRAWDY.
* 8 - FILIP I ANTONI CIĄGNĄ DRUTA - wielki kabel z Węża na Wronę. I Lila odpala czujniki by ostatecznie Strachy przekierować na TĘ jednostkę. Chronić swoją.
    * ExZ+3+3Vg
    * VV: Lila zna prawdę - ma czujniki, wie o jednej kapsule gdzie mogą być żywe osoby. I wie, że tu była walka piratów z kalcynitami i piraci mieli też... upiora? Opętującego? Coś więcej niż tylko kalcynici.
* 9 - Jak zginęli niewolnicy. Forensics. Maja najlepszy medyk bo zajmuje się grzybami i jedzeniem...
    * TrZ+3 (niepełne)
    * XXXVX: jedna od piratów, jedna od kalcynitów; COŚ się poruszyło - kalcynit. Kornelia i Maja się rozdzielają i uciekają. Maja strzela do kalcynita i rani Kornelię. To halucynacja. Kornelia się załamała. Zainfekowana.
* 10 - "Kornelia" z zaskoczenia atakuje Polę, Maję i Lilę (na Wężu)
    * KONFLIKT TRUDNY+2
    * XX: Pola ranna z pistoletu, Lila włącza dopalacz i Wąż zostaje uszkodzony ALE Kornelii wypada pistolet
    * VV: Maja obezwładnia Kornelię i ją bije; opętanie minęło
* 11 - Lila -> Berdysz "ratuj Filipa i Antoniego". Berdysz próbuje uratować obu. Na pełnej mocy.
    * ExZ+3
    * XVVV: Antoni zabił Alana, ale Berdysz uratował Filipa i opanował Antoniego. Niesamowita szybkość.
* 12 - Berdysz przejmuje dowodzenie na Wronie. Chce ewakuować Filipa + Antoniego w obroży + odzyskać Amandę
    * TrZ+3
    * VVXXV: Berdysz bezpieczny, Filip bezpieczny, nie ma jak wysadzić Wrony. Antoni - straszna trauma. Ale wyciągnął wszystkich.
* 13 - MAJA WŁĄCZA BOJĘ. Wyłączają Węża. Niech będą tu Strachy. Niech strachy vs głębniaki. A Wąż siedzi cicho + lekki impuls z silnika dla mikro ruchu.
    * ExZ+3
    * VV: Ściągnięcie Strachów ZANIM Berdysz padł z sił.
* 14 -Udało się wyłączyć Węża i puścić statek. Maja, jako najbliższa lekarzowi, stawia odpowiednio obroże dla Kornelii i Antoniego na wysoką dopaminę i morale. I Amandy też.
    * TrZ+2+3Vg za porady piratów
    * VXXX: Obroże dają radę, trauma dla wszystkich, Maja jest kapitanem ale nie będzie lubiana, Berdysz jest uznany autorem przetrwania Węża.

---
layout: cybermagic-konspekt
title: "Kłótnie sąsiadów w Wańczarku"
threads: szamani-rodu-samszar, mroczna-wiedza-samszarów
gm: żółw
players: kić, kolega-kamil
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [230620 - Karolinus, sędzia Mirkali](230620-karolinus-sedzia-mirkali)

### Chronologiczna

* [230314 - Brudna konkurencja w Arachnoziem](230314-brudna-konkurencja-w-arachnoziem)

## Plan sesji
### Theme & Vision & Dilemma

* Horrida "W niepamięć"
    * "Miałeś cierpieć tak, jak cierpiałam kiedyś ja"
* CEL: pokazanie mandragory, pokazanie magów jako niezbędnych
* DYLEMAT
    * kto jest winny?

### Co się stało i co wiemy

* Wańczarek 

### Co się stanie (what will happen)

* S01
    * Wszyscy chcą wybaczenia dla Elei. Chowają mandragorę. 
* S02
    * 
* S03
    * 

### Sukces graczy (when you win)

* rozwiązanie problemu

## Sesja - analiza

### Fiszki

* Artemis Lawellan: dowódczyni Opiekunów, huntress
    * styl: Chevill z MtG (arogancka, cicha w akcji, lots of bling); cel: zapewnić Mandragorze żywność
* Remigiusz Lawellan: lekarz, dbający o pacjentów w Szpitalu i powiernik sekretu Wańczarka
    * styl: chłodny lekarz, acz z pasją do zdrowia pacjentów i oddania Wańczarkowi tego co należy do Wańczarka
* Damian Fenekis: burmistrz i właściciel wielu sadów w Wańczarku; chce innej drogi dla Wańczarka.
* Katon Fenekis: nadzorca cmentarza. Kiedyś Opiekun. Nie chce mieć z tym już nic wspólnego. Jego wrabiamy w śmierć dziennikarza.
* Elea Brzozecka: zna tysiące opowieści. Opiekuje się dziećmi.
    * pierwsza ofiara mandragory. Chciała zabić swojego narzeczonego, Ilfonsa Lawellana.
* Szymon Brzozecki: z zewnątrz; 
* Kazimierz Szarbot: dziennikarz z Verlenlandu, zaciekawiony historią Wańczarka (KIA)
    * sformował mandragorę. Ci cholerni Verlenlandrynkowie!
* POTWÓR Mandragora
    * zdeformowana ludzka postać o wydłużonych kończynach, odrapanych skórzanych skrzydłach i groteskowej twarzy z wybałuszonemi oczami i szerokim, krzywym uśmiechem
    * korzenie mają zdolność wchłaniania energii życiowej swoich ofiar, jednocześnie sprawiając, że inne rośliny i drzewa w okolicy stają się mroczne i zdeformowane
    * styl: Vinevra

### Scena Zero - impl

.

### Sesja Właściwa - impl

Karolinus dostał zadanie. Poważne zadanie bojowe. W niewielkiej miejscowości Wańczarek, słynącej dzisiaj z jabłek, sadów itp są waśnie sąsiedzkie. "Ktoś źle nawóz" albo "pocięte drzewa" choć "ktoś podtruwa sad". Więc - wysłali tam maga. Karolinusa. By był zarządcą. Oko maga popatrzy na ludzi, by nie robili głupot. Karolinus w szatach.

Gdy docieracie do Wańczarka, zaczyna się robić ciemno. Pierwsze co Karolinusowi rzuciło się w oczy - ogromny budynek na wschodniej części. Mały zameczek. To jest szpital - ale ten szpital jest za duży. Zbyt... "pancerny". I gdy zbliżacie się do Wańczarka, sołtys i dwie córki od razu czeka z transparentami "witamy w Wańczarku". Widać, że to... próba przymilenia się. Łza odpala 'vroom'. Agrotuning.

Dom sołtysa jest tandetny ale jest kasa. Ojciec córek przy kolacji pilnuje, by nie robiły nic głupiego. Np. nie dawały magowi nic do zrozumienia. A dziewczyny próbują dawać magowi coś do zrozumienia. Żona jest, rodzina ogólnie jest - wszyscy przywitali Karolinusa ciepło, ale ostrożnie. Uczta na cześć Karolinusa. Karolinus pyta o problemy, jakieś sukcesy itp.

Sołtys Damian zaczął mówić. Przez 15 min o jabłkach. Olga (córka) powiedziała, że Artemis Lawellan nie pozwoliła Arturowi się spotykać z nią. Karolinus "sprawiedliwość nie istnieje".

Karolinus naciska na Damiana.

Tr Z (pozycja) +3 +3Oy (uroda, charyzma i dziewczyny):

* Vr: Damian powiedział kilka rzeczy
    * tu coś się dzieje, Damian wie co, Damian bardzo nie chce by Karolinus łaził.
    * Damian na pewno nie jest "źródłem zła". Jest zbyt... polityczno-miękko-poczciwy. Dobry sołtys na dobre czasy.
    * Lawellanowie nie szantażują go. On się trochę boi Artemis, ale nie dlatego bo ona _coś_, tylko dlatego że Artemis jest dziwna.
* V: Damianowi zależy, by odwrócić uwagę od schnących jabłoni. On coś wie. Mimo, że Karolinus jest magiem.

Gdy Karolinus wraca do pokoju, pod drzwiami stoi młody chłopak. Na oko - 13 lat. "Tien?" "Proszę" - z namaszczeniem wręczył karteczkę. "Od Olgi". Olga, ogólnie, zaprasza Karolinusa na imprezę w sadzie w nocy. Będzie techno. "Techno w gumiakach".

Karolinus zdejmuje szaty, ubiera się 'normalnie', no i wychodzi normalnie. Sołtys "tien Samszar, w nocy, samemu, może będę towarzyszył?" "Nie, chcę na spacer." "Nalegam!"

* V: Sołtys tak zakręcony i zmartwiony czymś, że aż nie zauwazył że Olga też zniknęła.

Strzała sprawdza przychody, rozchody, sprawdza wioskę pobieżnie, ze szczególnym naciskiem na sołtysa i rodzinę sołtysa. Strzała sprawdza jej reputację i osiągnięcia.

Tr Z (potęga psychotroniczna) +3:

* V: 
    * Fenekisowie faktycznie są dość bogaci. Większość bogactwa, co ciekawe, nie pochodzi z sadów - jest zastała. z KIEDYŚ.
        * był eksportowany bardzo popularny stymulant 'Koncentrat Oświecenia'. Ale to minęło, przestał być eksportowany. Skończył się substrat.
        * Fenekisowie pełnili rolę dystrybutorów.
    * Lawellanowie
        * Lawellanowie dostarczali ten stymulant. Oni go produkowali.
        * Lawellanowie od zawsze powiązani wpierw z więzieniem, potem ze szpitalem - mieszkają tam.
        * Artemis - jest dobra w walce. Faktycznie, była gladiatorką w Verlenlandzie. Nie jest naturalna.
        * Żyją dobrze w tej wsi. Pomagają. Artemis np. gdy był pożar, to sama wbiegła do budynku i wyciągała ludzi.
        * Artemis pogardza słabymi.
* X: niedawno zaczęło się znowu coś dziać tutaj, ale wszyscy to ukrywają
* V: znowu szpital jest w centrum tego wszystkiego
    * z papierów -> szpital jest legitny i działa.
* X: część komputerów w Wańczarku się spaliła. Za słabe. Oops.
* V: raporty policyjne
    * miesiąc temu zniknął tu dziennikarz z Verlenlandu. Policjant który nie urodził się w Wańczarku to zgłosił. Szef policji wycofał raport - powiedział, że ów się znalazł.
    * od 2 tygodni nasilają się waśnie i agresywne sytuacje. Ogólnie - wieś próbuje to ukrywać.
    * (Strzała ma radar). Na niedalekim jeziorku były dwie osoby na łódce. Jedna wraca na łódce. Druga została w jeziorku. Sygnał telefonu przestał działać.

Strzała rzuca pinga z informacją do Karolinusa. (Xz: niestety, Strzała skutecznie uszkodziła sporo elektroniki tutaj). Wyraźnie nikt nie dba o sieć, elektronikę itp. Nie ma też normalnej ilości duchów. Strzała dodała na listę zadań Karolinusa "zbadaj Sentisieć".

Strzała cicho podjeżdża pod przystań. Strzała szybko -> jezioro, zanim jeszcze łódkowiec wrócił. Widzisz gasnące chlapanie na jeziorze i łódkę która dobija do brzegu.

Tr !Z (konsumuję by nie był ekstremalny) +2:

* X: delikwent nie ma siły się łapać
* X: delikwent idzie na dno i potrzebuje pomocy medycznej
* (+3Or): V: Strzała wbiła w delikwenta harpun + oplotła i go wyciągnęła. Zobaczyła twarz osoby z łodzi - to młoda dama, 27. (Elea Brzozecka). Tonący lokals, 26: Ilfons Lawellan.

Nocna impreza. Olga jest szczęśliwa że ma Karolinusa do tańca. Karolinus myślał, że ona ma mroczne sekrety. Nie. Ona chciała potańczyć z tienem. Karolinus dopytuje o szczegóły.

Tr Z (skille Karolinusa + ona go lubi) +3 +3Or:

* Vr: Karolinus gada z Olgą i po tym jak odfiltruje głupoty dowiedział się co gadają ludzie i co się dzieje:
    * "niektórzy starzy ludzie gadają, że dobrobyt wraca lub będzie wracał"
    * "dziennikarz zadawał bardzo niewygodne pytania i musiał odejść z tego miejsca"
* Or: Karolinus zaczął gadać z Olgą, wyglebił się, popchnął go taki silny chłopak "zostaw moją dziewczynę".
    * "on jest tienem!"
    * "jak już to tienem błota"
    * K: "chłopcze, nie przeszkadzaj, Twoja dziewczyna, nie uprawnia byś atakował innych"
    * A: "nie uprawnia... co Ty gadasz? Olga, wierzysz w tiena bo ładnie mówi?"
* +M +3Ob -> Tp: 
    * Ob(poz): CELEM było żeby zamknąć mu usta.
        * Karolinus zmienił go w ŚWINIĘ! KWIK ŻAŁOŚCI I PANIKI! Ucieka na raciczkach!
        * Olga przerażona, odsuwa się.

Karolinus wprowadza OGRANICZONY TERROR. Jest tienem. "Kontynuuj co mówiłaś. Wszyscy kontynuujcie."

Tr Z (ich szerokość, populacja itp, dużo dzieciaków z dobrymi relacje z rodziną) +4:

* Vz:
    * bo tu był produkowany stymulant. Dzieciaki nie wiedzą jaki.
    * podobno stymulant będzie znowu produkowany, co widać po uschniętych drzewach (-> którzy starzy to mówią)
        * sołtys nie wie, wie OGÓLNIE, ale chce uniknąć wiedzenia
        * na pewno Artemis wszystko wie, ona za tym stoi
    * stymulant jest produkowany w szpitalu
    * drzewa schną bo _to_ jest w ich korzeniach
    * nie jedz owoców uschniętej jabłoni, cokolwiek się nie stanie
* Xz: Olga unika Karolinusa i się go boi, wszystko to jej wina.
* Vr: dzieciaki zrobią WSZYSTKO by Karolinusowi dać prawdę
    * "dla najlepszych przejażdżka wspólna Strzałą i pokaz fajerwerków"

I na to przyjeżdża Strzała powodując popłoch, z rannym Ilfonsa. Karolinus go leczy magią.

Tr Z (apteczka) M +3 +3Ob:

* Xm: energia Karolinusa weszła w interakcję z _czymś_ w jego żyłach. Dzieje się z nim coś dziwnego. Utracił człowieczeństwo.
* V: Ilfons Lawellan przetrwa. Acz nie w ludzkiej formie. 
    * jego kończyny się przedłużyły i zdrewniały trochę. Zrobił się szczególnie szpetny. Zapadnięta twarz. Coś pomiędzy człowiekiem i rośliną.
* V: sygnatura energetyczna delikwenta rezonuje z uschniętą jabłonką. Ale może mówić.
    * "chciała mnie utopić" "moja narzeczona"
        * zjedli szarlotkę, po czym walczyli. Ale ona była silniejsza. On był słabszy. I ona go dźgnęła a potem chciała utopić. Chiała wytoczyć krew i do szarlotki. I zjadła.
        * on czuł, że powinien tu umrzeć, ale ona chciała go utopić, ale nie chciała go utopić. On miał nawozić jabłonkę.
            * czar wyleczył go z chęci bycia nawozem jabłonki
            * co ciekawe, ona ani na moment nie chciała być nawozem
    * słyszał krzyk "w duszy"

Ilfonsa się przypnie do łóżka a Karolinus się chce wyspać. Sołtys unika Karolinusa.

Karolinus dobrze śpi. Strzała - widzisz cień. Zbliża się do domu gdzie śpi Karolinus. Artemis. Sfrustrowana Artemis. Patrzy na dom sołtysa. Wygląda jakby chciała zabić Karolinusa. Ale zrezygnowała. Wraca do szpitala. Strzała oblatuje Artemis szerokim łukiem, wyłącza wszystko i czeka na Artemis. Tak, by koło Strzały przeszła.

Tr Z (nikt nie spodziewa się tak zaawansowanej psychotroniki) +3:

* X: Artemis zauważyła Strzałę. Była już dość blisko, ale ją zauważyła.
* Vz: Artemis nie może ominąć Strzały nie pokazując, że ją omija.
    * Artemis zmienia trasę. Idzie w las.
* Vr: Strzała przechwytuje Artemis zanim ta się rozpływa. Artemis ma ppanc granat.

Strzała podaje statystycznie szanse przeżycia Artemis. Ona się uśmiechnęła i pokazała zęby. "Mnie nie zamienisz w świnię, jeśli wypowiesz mi wojnę - odejdziemy oboje". "Zejdź mi z drogi. Ja robię swoje, Ty swoje." Strzała na to "śmierć maga powoduje rozgłos, a chcesz rozgłosu uniknąć. Czego chcesz i do czego dążysz, bo najwyraźniej nie chcesz rozgłosu i nie zabiłaś maga."

* Strzała kontynuuje: "To jest teren Samszarów. Magowie mają pełne prawa. Artura spotkała kara za chamstwo wobec maga."
* Artemis: "Blakenbauer powiedziałby Ci inaczej. To myślenie Lemurczaka." (DUŻO wie o rodach magicznych)

Strzała szuka informacji o jej rodzie. Informacje z Centrali. Kim są Lawellanowie i ich powiązanie z innymi rodami magicznymi?

Tr !Z (skonsumowany) +3:

* Vr: rekordy są zasealowane przez Samszarów, ale ŁZa ma do nich dostęp
    * byli pewni Samszarowie, którzy czerpali korzyści ze współpracy z Blakenbauerami. W wyniku tego powstał Koncentrat Oświecenia
    * Lawellanowie byli szkoleni przez Blakenbauerów, do walki z mandragorami i do hodowli mandragor
    * Lawellanowie są osadnikami od Blakenbauerów na ten teren przy akceptacji Samszarów
    * Lawellanowie to "ci, którzy polują"

Karolinus jest wyrwany ze snu bardzo ostrym sygnałem alarmowym. Informacje, krótki raport "Artemis chce Cię nie zabijać, przydybana, w trakcie rozmowy... co teraz?" Polecenie Karolinusa - trzymać Artemis w napięciu.

Info od Strzały "przemiana Ilfonsa uratowała mu życie, nie było czasu zawieźć go do szpitala". Artemis "nie wierzę Ci. Tien Samszar zmienił Artura w świnię. Za lojalną służbę." Strzała rzuca wizualizację na drzewo jak było - ratowanie Ilfonsa. Artemis patrzy "Czy uważasz, że Samszarowie różnią się czymś od Lemurczaków?" Strzała odpowiada z duszy (której w teorii nie ma) "Tak" i enumeracja.

Artemis "Nie zrobiłam niczego wbrew rodowi Samszar, nikt z nas tego nie zrobił, mimo ogromnego kosztu osobistego. Uważasz inaczej?" Strzała "koszt osobisty?" Artemis "Jesteśmy dalej tutaj. Wypełniamy swoją rolę. Robimy co mamy robić."

Karolinus chce ujawnić sprawę. Nie zgadza się na to, by Lawellanowie musieli polować na ludzi i mandragory. To powiedział Strzale.

* S->A: "Czemu postanowiłaś go nie zabijać?"
* A->S: "Wszyscy zginą. Zabicie jednego Samszara zabije wszystkich. Plus, zemsta nic nie da. Nic się nie zmieni."
* S->A: "A gdyby mogło?"
* A->S: "Nie weźmiesz mnie żywcem."
* S->A: "Karolinus Samszar jest kimś innym niż to co myślisz. Nie wiedział co tu się dzieje."
* A->S: "Oczywiście, że nie wiedział."
* S->A: "Myślisz że cały ród wie wszystko?"
* A->S: "My wiemy kto co robi. Jeśli ktoś wykroczy poza umowę, może zabić wszystkich. Oczywiście, że wiemy. Zakładam, że Samszarowie robią to samo. Mają większą moc. Większą odpowiedzialność."
* A->S: "Czy to, że podeszłam pod dom jest powodem, bym miała zginąć? Nic nie zrobiłam. Daj mi odejść. I tak nie mogę opuścić tego terenu."
* S->A: "Umowa czy geas?"
* A->S: "Wszyscy zginą. To nie ma znaczenia. Umowa. Nie zostawię mojej rodziny na śmierć."

Strzała daje Artemis komunikację do Karolinusa by mogła się odezwać jak to przemyśli. Jeśli chce coś z tym zmienić, zmienić sytuację swojej rodziny. Jeśli chce móc opuścić ten teren.

* Artemis: "Kolejna zabawa bogów. Daj mi broń zabijającą magów, będę miała pewność, że masz dobre intencje."
* Strzała: (otwiera kokpit, pokazuje, że nie ma Karolinusa i bierze dystans), odwraca się od Artemis i rozwala drzewko by pokazać siłę ognia.
* Artemis: (pokazała zęby by przestraszyć). "Nie wiem czego chcesz. Jestem lojalna."
* Strzała: "Pytanie komu"
* Artemis: "Wpierw Samszarom, potem rodzinie, potem wiosce, tak jak powinno być."
* Strzała: "Samszarowie są jak każda inna rodzina. Każdy jest trochę inny, ma inne spojrzenie na życie i nie każdy wie wszystko co robią inni. Masz wybór z kim chcesz pracować. A fakt że chcesz broni zabijającej magów świadczy że coś Ci nie odpowiada w aktualnej sytuacji."
* Artemis: "Przygotowuję się do ataku Verlenów. Chcę móc obronić wszystkich."

Karolinus wideokonferencja z Artemis.

* A: "Tien Samszar, co za zaszczyt." (dalej trzyma granat ppanc)
* K: "Witaj, Artemis. Rozumiem, że się gniewasz za Artura że zamieniłem go w świnię? Spokojnie, to minie, po kilku dniach będzie człowiekiem, przyjechaliśmy dotrzeć do prawdy" (mówi szczerze). "Chcesz naprawić? Odłóż granat, nie będziemy szantażować, szczuć, jesteśmy rozwiązać konflikt. Coś wiesz, chcesz pomóc, masz szansę."

Tr Z (bo Samszar) +2:

* Vr: Artemis częściowo zaryzykuje
    * A: (myśli) Jeśli coś Ci powiem, to nie ja zginę. Wszyscy zginą. Nie powiem Ci niczego. Nie mogę. Ale możesz się sam dowiedzieć.
    * K: Naprowadź mnie chociaż. Kto może?
    * A: Nie wiem, naprawdę nie wiem. Ale... możesz... zejść do szpitala, samemu zobaczyć, podziemna jaskinia z podziemnym jeziorem. Ślady. Dowody. Jeśli potrzebujesz takich rzeczy, możesz sam je znaleźć. Zejście jest w szpitalu.
    * K: Strzało, puść ją. Nie latamy po szpitalu w nocy
    * A: To by było nierozsądne...

Karolinus przekonał Artemis, że może odejść. On spróbuje pomóc. Jakoś ich z tego wyplątać. Poznał prawdę od Artemis:

* tu współpracują ciemniejsi Blakenbauerowie z ciemniejszymi Samszarami
* rodzina Artemis jest przekazana przez Blakenbauerów
* czas przywrócić pierwotny plan - eliksir z korzeni Mandragory, nieważne czy chcą.

## Streszczenie

Karolinus został wysłany do Wańczarka, by rozsądzać spory między sąsiadami odnośnie sadów, ale wpakował się w starą intrygę ciemniejszej strony Samszarów i Blakenbauerów odnośnie robienia Koncentratu Oświecenia z soku mandragory. Gdy pojawiła się próba utopienia Ilfonsa przez Eleę, Strzała wyciągnęła topielca a Karolinus go uratował. Karolinus zmienił jednego Lawellana w świnię, a potem ze Strzałą przyszpilili Artemis która powiedziała im prawdę o tym terenie - jak powstaje Koncentrat Oświecenia. Aha, miesiąc temu zniknął tu dziennikarz z Verlenlandu.

## Progresja

* Ilfons Lawellan: prawie utopiony przez Eleę Brzozecką, Karolinus uratował mu życie robiąc z niego hybrydową bioformę między człowiekiem i drzewcem.
* Artur Lawellan: zmieniony w świnię na pewien czas przez Paradoks Karolinusa. Nie wpływa to pozytywnie na jego relacje z Olgą ani na jego szacun na dzielni.

### Frakcji

* .

## Zasługi

* Karolinus Samszar: zaczął od sprawdzania zbyt miłego sołtysa, przeszedł przez nocną imprezę z dzieciakami i ich brutalnie zastraszył (zmieniając jednego w świnię co go przewrócił w błoto za flirt z jego dziewczyną), uratował magią życie topielcowi go przekształcając w roślino-człowieka a na końcu przekonuje do siebie Artemis. Może pomóc i chce uratować ludzi z mrocznych eksperymentów Blakenbauerów.
* AJA Szybka Strzała: sprawdza przychody, rozchody, sprawdza wioskę pobieżnie. Spaliła pół elektroniki wioski (za słabe), ale ma kluczowe dane. Ratuje życie topielca Ilfonsa. Potem monitoruje Artemis Lawellan i ją konfrontuje w imieniu Karolinusa.
* Damian Fenekis: sołtys; przymila się do maga i ma pasję do jabłek, acz nie chce mówić o magicznych problemach jabłoni; wyraźnie boi się Karolinusa. Ale nie widać by coś zbroił. Jest zbyt... polityczno-miękko-poczciwy.
* Olga Fenekis: córka sołtysa, której w głowie Artur, imprezy i status. Niewiele wie, ale ściągnęła Karolinusa na imprezę. Przestraszona Karolinusem.
* Artur Lawellan: chłopak Olgi, popchnął Karolinusa w błoto i skończył jako świnia. Z mrocznej rodziny Lawellanów, nie zna jej przeszłości.
* Artemis Lawellan: commander Opiekunów, nie podoba jej się jej przeznaczenie i życie, ale chce zdrowia i bezpieczeństwa rodziny więc się słucha. Żyje na uboczu. Bawiła się z myślą zabicia Karolinusa, ale zmieniła zdanie. Powiedziała Karolinusowi, co się dzieje w obszarze mandragor i szpitala (eliksir) i o sojuszu mrocznych Blakenbauerów i mrocznych Samszarów.
* Ilfons Lawellan: narzeczony Elei Brzozeckiej; zjadł jej szarlotkę (z sokiem ze Skażonej Jabłoni) i Elea prawie go utopiła - gdyby nie Strzała. Skończył jako roślinoczłowiek z ratunku Karolinusa.
* Elea Brzozecka: chciała zabić swojego narzeczonego, Ilfonsa Lawellana pod wpływem Mandragory. To sprawia, że nikt w miasteczku jej o nic nie wini. Oni znają przeszłość.

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Aurum
                        1. Powiat Samszar
                            1. Wańczarek: wieś sadów niedaleko granicy z Blakenbauerami
                                1. Wańczarek
                                    1. Dzielnica Sadowa (NW, W)
                                        1. Sady owocowe (NW): miejsce imprezy Techno w Gumiakach, gdzie bawią się młodzi ludzie.
                                        1. Dom sołtysa: jeden z największych i najfajniejszych zadbanych domów w okolicy z dużym sadem. Widać, że biedni nie są.
                                    1. Dzielnica Smutku (E)
                                        1. Szpital Jabłoni: miejsce, gdzie mieszkają Lawellanowie (a zwłaszcza Artemis)

## Czas

* Opóźnienie: -37
* Dni: 3

## OTHER
### Fakt Lokalizacji
#### Miasto Wańczarek

Dane:

* Nazwa: Wańczarek
* Lokalizacja: 

Fakt:

W Wańczarku produkowany był Koncentrat Oświecenia - eliksir łączy wiedzę szamanów z naukową wiedzą o funkcjonowaniu mózgu, aby pobudzać kreatywność, inspirację i oświecenie umysłu. Jest ceniony przez artystów, naukowców i innych twórców. Robiony z korzeni mandragory.

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Aurum
                        1. Powiat Samszar
                            1. Wańczarek, wieś sadów
                                1. Wańczarek
                                    1. Rynek (Centrum)
                                        1. Sklep spożywczy
                                        1. Mała kawiarnia
                                    1. Dzielnica Sadowa (NW, W)
                                        1. Sady owocowe (NW)
                                        1. Pole uprawne
                                        1. Młyn wodny (W)
                                    1. Dzielnica Smutku (E)
                                        1. Szpital Jabłoni
                                            1. Laboratorium Koncentratu Oświecenia
                                            1. Plantacja Mandragory
                                            1. Podziemne jezioro
                                        1. Cmentarz

---
layout: cybermagic-konspekt
title: "Mandragora nienawidzi Rekinów"
threads: rekiny-a-akademia, dysonans-diskordii
gm: żółw
players: anadia, kić
categories: inwazja, konspekt
---

# {{ page.title }}

## Kontynuacja

### Kampanijna

* [210817 - Zgubiony holokryształ w lesie](210817-zgubiony-holokrysztal-w-lesie)
* [211120 - Glizda, która leczy](211120-glizda-ktora-leczy)

### Chronologiczna

* [210817 - Zgubiony holokryształ w lesie](210817-zgubiony-holokrysztal-w-lesie)

### Plan sesji
#### Co się wydarzyło

.

#### Sukces graczy

* .

### Sesja właściwa
#### Scena Zero - impl

.

#### Scena Właściwa - impl

Marysia siedzi sobie wesoło w swoim Apartamencie. Kacper uciekł z terenu, więc Marysia nie ma rywala. Po raz wszystko jest na właściwym miejscu.

Odwiedza ją... Daniel Terienak (dziąsłowiec i biedaartefaktor). Ma kwiaty. Marysia pyta "o co chodzi". "Przyszedłem dać Ci prezent, o najpiękniejsza". Nie, nie pił XD. Marysia go NIE wpuszcza i dzwoni do Karoliny.

Karolina, zaalarmowana przez Marysię, jest szybko na miejscu. A Marysia spowalnia Daniela, by ten poczekał i nie odszedł. Ma różne kwiaty - chwasty koło róż. Tr+2:

* X: Poddenerwowany, nabuzowany energią.
* V: Marysia go spowolni aż Karolina przybędzie.

Daniel nie do końca poznaje Karolinę. Ale po chwili wraca do siebie. Mówi Karolinie, że to nie jej sprawa - ale wyraźnie Daniel powstrzymuje się, by Karolinie nie walnąć. Ma tendencje do skrajnej agresywności - nie jak zwykle. Coś jest z nim cholernie nie tak...

(+Z, bo jest Karolina, czyli siostra)

* V: to ONA, Marysia, dowodzi teraz mafią. ONA STOI ZA MAFIĄ!
* X: złapał się za głowę, krzyknął przeraźliwie.

Karolina korzysta z okazji, że Daniel jest w złej formie. Zdecydowała się natychmiast Daniela zaatakować i unieszkodliwić. Tr+2:

* V: Karolina skutecznie walnęła i NORMALNIE unieszkodliwiłaby Daniela, ale adrenalina jest za silna.
* XX: Daniel jest za silny. Uwalił Karolinę na ziemię i zaczął ją dusić. Zorientował się - "Karo..?". I Karolina skorzystała z okazji i walnęła mu w jaja i unieszkodliwiła, nadmiarem ciosów.

Karolina szybko jedzie do medyka Rekinów z Danielem. Medyk Rekinów to Sensacjusz Diakon.

Sensacjusz (który nigdy nie chce tu być) przywitał tienki dość chłodno. Niechętnie zabrał się do rzeczy. Ale się ucieszył badając Daniela. Coś ciekawego. "Nie ma żadnego wpływu chemikaliów. Ale coś jest nie tak."

Sensacjusz próbuje naprawić Daniela. I dojść do tego co mu jest - z chorych energii. Używa środków medycznych (Rody płacą najlepszemu lekarzowi by dbał o ich pociechy i Sensacjusz jest cholernie dobry).

TrZ+2:

* Vz: Dzięki środkom szybko ustabilizował i doprowadził Daniela do niesprawności. Usunął Skażenie Esuriit.
* V: Sensacjusz doszedł do tego, że ta energia Esuriit nie pochodzi od maga bezpośrednio. Czyli to nie był rytuał lub czar. Nie coś co zrobiłaby np. Sabina. Ale może być coś, co tu _jest_ od pewnego czasu.
* X: Daniel leży następne 2 dni.
* V: Sensacjusz zrobił "autopsję". Daniel się bronił. Walczył z czymś. Dużo wskazuje na to, że coś było mu wstrzyknięte. Coś pochodzenia roślinnego. I na ciele ma pnącza i ślady pnącz. Czyli mocno złapała go jakaś roślina. Wstrzyknięto mu coś paraliżującego - jakieś żądło. A potem Esuriit dostało się inaczej. Nie przez wstrzyknięcie. Inne źródło.
* XX: Ogólnie OPINIA jest taka, że Marysia wygnała Kacpra by przejąć mafię. Terminusi wiedzą, że to nieprawda. Mafia wie, że to bzdura. Ale REKINY tak uważają. I to sprawia, że jeśli coś / ktoś poluje na mafię i używa do tego celu Rekinów, to Marysia jest pierwszym celem.
* X: Sensacjusz TEŻ uważa, że Marysia wspiera mafię i przez nią Daniel ucierpiał. Myśli, że Marysia zrobiła sobie strasznego wroga w jakimś antymafiozie i teraz ratuje skórę. Najpewniej Marysia ma do czynienia z mandragorą. I z mandragorą współpracuje jakiś potwór...

Marysia próbuje przeskanować pamięć Daniela. Sensacjusz protestuje, ale Marysia pulls the rank.

TrMZ+2+3O+3O:

* V: Marysia ma ogólny pogląd - Daniel bawił się ścigaczem w lesie. Tam napotkał na _coś_. To coś go złapało i zatruło. Wtedy stracił częściową przytomność, ale mniej więcej wie gdzie był i gdzie był wleczony. Potwierdziła się hipoteza mandragory. Potwór... mniej więcej Marysia wie jak wygląda. To pnączoszpon, zainfekowany Esuriit. "W służbie mandragory".
* Vm: Marysia _poczuła_ gdzie to jest. _Zobaczyła_ to miejsce. Wie dokładnie gdzie to jest. Daniel się bronił, myślał o czymś innym - to Daniel przekierował nienawiść mandragory na mafię w swojej głowie. Bo nie chciał skrzywdzić siostry, nawet przez przypadek.
* V: Marysia wie, gdzie szły szepty mandragory. Daniel był przebudowywany przez mandragorę, by zniszczyć Rekiny. A im wyżej Rekiny tym bardziej. Ktoś zginął przez Rekiny. I jego ciało jest pielęgnowane. Ktoś pielęgnował energię by sformowała się mandragora.

PYTANIA:

* Q: Jak nazywał się osoba z rodu Sowińskich która była w tej okolicy ale już nie jest - dwa lata temu się wycofała.
* A: Amelia Sowińska
* Q: Dlaczego Amelia była tak znienawidzona przez miejscowych?
* A: Cokolwiek chciała, przychodziła i "moje". "Let them eat cake". Zero empatii, odrealniona.
* Q: Dlaczego Amelia była tak uwielbiana przez Rekiny?
* A: Robiła świetne imprezy, często kosztem miejscowych. Walki biednych ludzi na plaży.
* Q: Do jakiej roli Amelia została awansowana przez Sowińskich i dlatego wróciła? W czym się sprawdziła?
* A: Odrealniona w kontekście ludzi którzy nie są arystokracji, ale świetna bezduszna organizatorka. Mistrzyni optymalizacji Taylorowskiej.
* Q: W jaki sposób to, że Amelia była tutaj sprawiło, że Marysia tu przyjechała?
* A: Amelia nie przepada za Marysią, więc chciała się jej pozbyć - zaaranżowała zRekinienie Marysi.

(czyli - Sensacjusz uważa, że Marysia jest jak Amelia; dlatego nie chce mieć z nią nic wspólnego)

SESJA:

Zarówno Karolina jak i Marysia już wiedzą, acz Marysia nie powiedziała Karolinie skąd wie. "Lekarz powiedział". Teraz Marysia i Karolina chcą dowiedzieć się historii. Ale jak?

Karolina wstawiła się za Danielem. Daniel chciał osłonić Karolinę, może Marysię. Karolina chce, by Daniel nie ucierpiał. Negocjuje z Marysią gwarancję. Karolina naciska - żeby Danielowi nie stała się krzywda. Spluwa na rękę i podaje Marysi - przyklepiesz? "Dobrze, nie sklepię go..."

Dziewczyny chciały się upewnić, że atak na Rekiny był jednorazowy i się już nie powtórzy. Uznały, że to ich rekini obowiązek. Plus... ciekawość. A w wypadku Karoliny - pomszczenie Daniela. Tylko ona może kopać swojego brata.

Marysia poprosiła o wsparcie Tukana. Chce iść na mandragorę + pnączoszpona Esuriit. Tukan stwierdził, że Marysia potrzebuje wsparcia. Dostanie DWIE uczennice do pomocy. Laurę jako artylerię i dywersję i Ekaterinę Zajcew jako siłę ognia i wojowniczkę.

Dwie godziny później dwie uczennice terminusa czekają przed wejściem do Dzielnicy Rekinów. Bo nie wolno im wejść do środka. Obie mają Lancery. Laura jest w Lancerze komfortowa. Ekaterina - nie. Muszą jakieś 5 minut poczekać maksymalnie.

Tukan był na wczasach w Aurum właśnie z Ekateriną. Ekaterina jest zapatrzona w Tukana. Laura... nie. Laurze już dawno przeszło.

Ekaterina zaczęła narzekać - ale czemu dowodzą cywile? Czemu Sowińska? Laura odparła, że to Tukan. Ekaterina "ON BY NIE ZROBIŁ TAKIEGO BŁĘDU!" A Laura, cynicznie, że Tukan błędu nie mógł zrobić i niech Ekaterina w niego nie wątpi. Tukan wie więcej na temat owych arystokratek niż Laura czy Ekaterina. Na pewno któraś z nich jest mistrzynią taktyki. I Ekaterina przeprosiła, że kiedykolwiek w Tukana wątpiła...

* Marysia: "Skoro tak nalegasz, dam szansę udowodnienia swoich umiejętności - Lauro, czyń honory. Dowodzisz."
* Laura: "Dziękuję za zaszczyt, tien Sowińska (nie jest wdzięczna). Czy mogę się dowiedzieć jakie jest moje główne zadanie? Oczywiście, bym nie zadziałała wbrew intencji tien Sowińskiej..."
* Marysia: "Zabić pnączoszpona. Zająć się mandragorą - unieszkodliwić. Chcemy zrozumieć co się stało i czemu powstała"
* Laura: "Przyjęłam. Ale... jak przesłuchać mandragorę..?"
* Marysia: "Wycieńczyć. Wyssać z energii. Jak będzie mało? Zamknąć w słoiku. Przebadać."
* Ekaterina: "Wow. Tukan miał rację. Faktycznie, wiesz jak dowodzić."
* Laura: "Nigdy nie miałam wątpliwości..."

Laura bardzo szybko udowodniła, że Tukan wybrał dobry skład. Wiedząc mniej więcej gdzie jest Skażony Pnączoszpon wysłała na pnączoszpona serię dron by go znaleźć a potem wystawiła Karolinę jako przynętę (bo jest szybka). Co sprawiło, że Marysia podwójnie doceniła lenistwo Laury. Jaki mistrz, taka uczennica...

Laura lokalizuje i przygotowuje pnączoszpona używając Karoliny jako przynęty TrZ+4:

* V: Pnączoszpon będzie wystawiony tam gdzie ma być
* XX: Jest za szybki. ZA szybki. Uszkodzi ścigacz Karoliny; ścigacz mało zwrotny i szybki.
* Vz: Pnączoszpon wystawiony słaby punkt dla Ekateriny
* V: Karolina robi to w ekstra efektowny WOW sposób. Laura i Ekaterina są zaskoczone.

Ekaterina przygotowuje "nuklearną lancę" (czar bojowy nazwany przez nią, oczywiście) prosto w wystawionego pnączoszpona.

TrZM+3:

* Vm: Pnączoszpon ciężko uszkodzony.
* X: Lancer Ekateriny się przegrzał i wyłączył awaryjnie. Lakier zszedł.
* V: Pnączoszpon wyparował.
* V: "nuklearna lanca" spuryfikowała Esuriit i cokolwiek tam nie zostało.

Laura jest zaskoczona. Naprawdę zaskoczona. Spodziewała się, że się uda. Nie spodziewała się TEGO. Ekaterina siedzi w nieaktywnym Lancerze i chichocze. Dobrze się bawiła.

Laura szybko wróciła do siebie. "Czyli została nam mandragora." Szybko zlokalizowała mandragorę dronami. Mandragora nawet nie zauważyła dron. Nie "czuje" ich. Czyli, zdaniem Laury, potrzebna jest przynęta w formie maga. I Laura proponuje na ochotniczkę Ekaterinę. Ekaterina, oczywiście, zgłasza się na ochotniczkę.

"Z silną wolą i gorącym sercem odeprzemy tą roślinę" - Ekaterina. Nie widząc miny Laury.

Marysia proponuje, że doda mentalną osłonę Ekaterinie. Ekaterina entuzjastycznie się zgadza. Laura ustawia 4 drony do ostrzeliwania mandragory jakby co. Lepiej niech roślina ginie niż Ekaterina.

Marysia stawia tarczę mentalną dla Ekateriny i Karoliny. Ekaterina jest pierwszym celem. CEL wysokopoziomowy - osłabić mandragorę do poziomu pozwalającego na użycie tarcz mentalnych by móc procedować.

ExMZ+3 (N: 3V):

* Vm: Tarcza mentalna Marysi się rozjarzyła, zabsorbowała większość uderzeń i nienawiści mandragory. Płonące serce Ekateriny jedynie pomaga. To niepoprawna optymistka...
* V: Ekaterina uzbrojona w wiarę w potęgę Tukana i w swoje umiejętności odpiera bez kłopotu nienawiść mandragory. Po prostu to nic w porównaniu z jej przekonaniem.
* X: Ekaterina słabnie. Karolina wchodzi do kręgu, by rozproszyć mandragorę.
* O: Soczewka. Tarcze zarezonowały z mandragorą i zaczeły wysysać jej energię do zera. Mandragora - echo tego co stworzyło mandragorę - zaczyna inkarnować się w tarczach. Tarcze oderwały się od Ekateriny i Karoliny.

Zaczęły formować się "duchy". Co najmniej sześć osób. Kształt ludzi. Postacie. I jedna z postaci oskarżycielsko wskazuje na Marysię. "Ty nam to zrobiłaś!". Efemerydy rzucają się na Marysię.

Laura: "Spowolnijcie to!". Laura bowiem za wolno czaruje.

Karolina się rzuca ścigaczem do jeziora (dokładniej: "pali gumę", czyli odrzut) - zrobić zasłonę z wody. Ochlapać je. I Karolina spina ze sobą nadrzędnie dwa ścigacze - Marysi i swój. Dzięki temu ma zasób.

ExZ+3:

* XX: Oba ścigacze działałyby dobrze. Ale Karoliny ścigacz - zbyt uszkodzony. Więc C&C zawiódł i oba wpadły do jeziorka. Obie arystokratki wyglądają jak mokre kury.
* X: Ścigacz Karoliny po prostu umarł.
* V: Ale to poświęcenie sprawia, że potężna fala wody niszczy duchy. A dokładniej - destabilizuje energię i prosto pod "nuklearną lancę" Ekateriny.

Zespół wziął nieaktywną mandragorę i zdecydował się na dalsze badania. Na podstawie informacji z duchów wszystko wskazuje na to, że duchy pomyliły Marysię z Amelią Sowińską. A pod mandragorą Laura znalazła kilka ludzkich ciał...

(fakty o Marysi: mentalistka, arystokratka, świetnie jeździ na ścigaczu, dyplomatka, intrygi to jej pożywny posiłek)

## Streszczenie

Mandragora nienawidzi Rekinów i jej ofiarą został Damian Terienak. Idąc za jego śladem Marysia i Karolina znalazły mandragorę i pnączoszpona oraz z pomocą dwóch uczennic terminusa rozwaliły problem. Dowiedziały się, że mandragora konkretnie celuje w Amelię Sowińską, która opuściła ten teren dawno temu (i która była dość okrutna).

## Progresja

* Marysia Sowińska: w oczach Rekinów Marysia wygnała Kacpra by przejąć mafię.
* Marysia Sowińska: Amelia jej na tyle nie lubiła, że wywaliła ją na prowincję by była Rekinem.
* Marysia Sowińska: Sensacjusz Diakon jest święcie przekonany, że plotki o mafii x Marysi to 100% prawda. I uważa, że Marysia jest jak Amelia.
* Karolina Terienak: jej ścigacz jest nieaktywny następne 3 tygodnie. Jest nie tylko popsuty (co jest spoko), ale też zalany (co nie jest spoko).
* Karolina Terienak: wywołała ogromne WOW u Laury i Ekateriny - za pnączoszpona, manewry i genialny ruch ze ścigaczem w jeziorze. 
* Amelia Sowińska: naprawdę nie lubi Marysi Sowińskiej.
* Daniel Terienak: nieaktywny przez następne 3 dni (regeneracja)

### Frakcji

* .

## Zasługi

* Marysia Sowińska: wpierw przeskanowała umysł nieprzytomnego Daniela by dowiedzieć się co go "zeżarło", potem broniąc Ekaterinę przed mandragorą postawiła jej potężną tarczę mentalną. Chroni Rekiny przed mandragorą z powinności i z ciekawości.
* Karolina Terienak: wow factor - zaimponowała Laurze i Ekaterinie przez manewrowanie mimo dopalonego Esuriit pnączoszpona. Potem, mimo uszkodzeń ścigacza, wpakowała go w jezioro by ratować Zespół przed duchami. I ścigacz umarł.
* Daniel Terienak: wpadł na mandragorę która nienawidzi Rekinów. Udało mu się przesunąć swoje Skażenie z "rekiny" na "mafia", by chronić siostrę.
* Sensacjusz Diakon: jest PEWNY, że Marysia Sowińska współpracuje z mafią. Świetny lekarz, opłacany przez wszystkie rody Aurum by ratować głupie Rekiny. Nie chce tu być. Zapalił się pomagając Danielowi - Daniel był dotknięty mandragorą, co Sensacjusz wykrył i naprawił. Aha, uważa, że Marysia jest jak Amelia.
* Amelia Sowińska: wielka nieobecna; kiedyś była na tym terenie jako Rekin, kochana przez Rekiny i znienawidzona przez miejscowych. Duchy pomordowanych tworzące mandragorę oskarżyły Marysię o bycie Amelią.
* Tomasz Tukan: gdy Marysia powiedziała o mandragorze, natychmiast wysłał Marysi do pomocy Laurę i Ekaterinę.
* Laura Tesinik: cyniczna do bólu na Tukana i Ekaterinę. Dobrze zaplanowała pułapkę na pnączoszpona. Bezradna w obliczu duchów. Doskonała taktycznie.
* Ekaterina Zajcew: optymistka zapatrzona na TERMINUSA TUKANA (to z nią był w hotelach Sowińskich). Jej ulubiony czar - "nuklearna lanca". Wypaliła pnączoszpona i resztki duchów. Nie zauważa sarkazmu Laury.

### Frakcji

* .

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Szczeliniec
                        1. Powiat Pustogorski
                            1. Podwiert
                                1. Las Trzęsawny
                                    1. Jeziorko Mokre: niedaleko niego znajdowała się mandragora, która nienawidziła Rekinów. W nim spoczywał przez moment ścigacz Karoliny jako wrak.
                                1. Dzielnica Luksusu Rekinów
                                    1. Serce Luksusu
                                        1. Lecznica Rannej Rybki: kontrolowana przez Sensacjusza Diakona; ów naprawia Daniela Terienaka.

## Czas

* Opóźnienie: 9
* Dni: 2

## Konflikty

* 1 - Marysia spowalnia Daniela, by ten poczekał i nie odszedł.
    * Tr+2
    * XV: Spowolniony, ale poddenerwowany i nabuzowany energią.
    * (+Z): VX: to Marysia stoi za mafią; złapał się za głowę i krzyknął
* 2 - Karolina korzysta z okazji, że Daniel jest w złej formie. Zdecydowała się natychmiast Daniela zaatakować i unieszkodliwić
    * Tr+2
    * VXXV: Szamotanina. Daniel za silny. Dusi Karolinę, ale się powstrzymał i został unieszkodliwiony.
* 3 - Sensacjusz próbuje naprawić Daniela. I dojść do tego co mu jest - z chorych energii. 
    * TrZ+2
    * VzVXVXXX: Ustabilizował i zrozumiał energię, ale Daniel leży 2 dni (Skażenie). Sensacjusz jest PEWNY, że Marysia przejęła mafię i jest jak Amelia.
* 4 - Marysia próbuje przeskanować pamięć Daniela. Sensacjusz protestuje, ale Marysia pulls the rank.
    * TrMZ+2+3O+3O
    * VVmV: Marysia wie o mandragorze i pnączoszponie - gdzie jest, co szepcze
* 5 - Laura lokalizuje i przygotowuje pnączoszpona używając Karoliny jako przynęty
    * TrZ+4
    * VXXVzV: pnączoszpon wystawiony jako słaby punkt, ale uszkodził Karolinowy ścigacz. Ekstra wow dla Laury i Ekateriny.
* 6 - Ekaterina przygotowuje "nuklearną lancę" (czar bojowy nazwany przez nią, oczywiście) prosto w wystawionego pnączoszpona.
    * TrZM+3
    * VmXVV: Pnączoszpon wyparował i spuryfikowany, lancer Ekateriny się przegrzał i wyłączył awaryjnie.
* 7 - Marysia stawia tarczę mentalną dla Ekateriny i Karoliny. CEL wysokopoziomowy - osłabić mandragorę do poziomu pozwalającego na użycie tarcz mentalnych by móc procedować.
    * ExMZ+3 (N: 3V)
    * VmVXO: mandragora wyłączona, Soczewka; tarcze --> duchy polujące na Amelię Sowińską.
* 8 - Karolina ścigaczem do - zrobić zasłonę z wody. Ochlapać je. I Karolina spina ze sobą nadrzędnie dwa ścigacze - Marysi i swój. Dzięki temu ma zasób.
    * ExZ+3
    * XXXV: Ścigacz Karoliny (już uszkodzony) umarł; arystokratki jak mokre kury. Ale - duchy zniszczone.

## Inne
### Projekt sesji

#### Procedure

* Overarching Theme:
    * gracze mają "kierowanie" (Vision & Purpose); wiedzą gdzie iść i po co tam idą
* Achronologia: x
* Wyraziste, ważne dla graczy postacie + Relacje (konflikt?) Gracze <-> NPC, NPC <-> NPC
    * graczom bardziej zależy na postaciach i ich powodzeniu
    * więcej potencjalnych wątków i rzeczy które możemy odrzucić przy krzyżykach
* Tory, zmiana postaci z czasem, wpływ graczy na świat
    * gracze widzą jak wpływają na świat. Wpływ jest "ich" - stąd tak tragiczne jest gdy "ich" rzeczy są corrupted / destroyed.

#### Result

* Overarching Theme:
    * Theme & Feel: 
        * Taka piękna nienawiść...
    * V&P:
        * 
    * adwersariat: 
        * 
* Wyraziste, ważne dla graczy postacie + Relacje (konflikt?) Gracze <-> NPC, NPC <-> NPC
    * Role i postacie
        * 
* Co się działo w przeszłości
    * 
* Tory, zmiana postaci z czasem, wpływ graczy na świat
    * 

---
layout: mechanika
title: "Notatka mechaniki, 230603 - mechanika, stabilizacja"
---

# {{ page.title }}

---
## Autorzy

* Autor: Żółw
* Projektanci: Żółw, Kić

---
## 1. Purpose

Jak działa mechanika EZ, tak w sumie?

---
## 2. High-level overview

Mechanika jest kodowana jako **2305**.

1. Wysokopoziomowe cele (do doprecyzowania)
2. Poziomy mechaniki (prosta -> zaawansowana) i problem "Wizard i Excel"
3. Aktualna implementacja
    1. Ustalenie celu konfliktu
    2. Konstrukcja puli
    3. Ciągnięcie z puli
    4. Eskalacje
    5. Teatry / wymiary
    6. To Gracz decyduje, co poświęca
    7. Automatyczny sukces (ostateczne poświęcenie)
    8. Kolory żetonów
    9. Akcje zależne (akcja gracza 1 wpływa na gracza 2) i akcje grupowe
    10. Imperfections
    11. Warianty alternatywne

---
## 3. Wysokopoziomowe cele (do doprecyzowania)

1. **Zasady wiodące designem**
    1. (1) **Każdy konflikt da się zamodelować**
        * Każdy - absolutnie każdy - konflikt w fikcji jest możliwy do zamodelowania w mechanice
        * Testowane na: [notatce gdzie rozpartywane są testy z fikcji pod kątem tego jak działają](230507-mechanika-dynamicznych-zetonow.md)
        * AKTUALNA SŁABOŚĆ: grupa vs grupa. Da się zrobić kolorując żetony, ale _to nie to samo_.
            * inspiracja: Storming the Wizard's Tower, Lumpley
    2. (2) **Mechanika jest sercem rozgrywki. Jej użycie jest emocjonujące.**
        * To co najciekawsze dzieje się w środku testów, nie w wolnych rozmowach między graczami.
        * Gracze mają "pozytywny stres" - czy wyjdzie V, X, czy O.
            * RPG, co mi słusznie przypomnieliście (-> Dzióbek, Kić), to jest show, a nie tylko gra.
            * "czy odważę się pociągnąć następny żeton?"
        * Istnieje "pęd" w akcjach. Każda akcja **coś** zmienia. Nie ma akcji nullowych.
            * Każdy ptaszek i krzyżyk ZMIENIA stan gry w sposób NIEODWRACALNY
        * Mechanika jest punktem kulminacyjnym sytuacji, a nie czymś, co wykorzystujemy, bo musimy. 
    3. (3) **System intencyjny**
        * Gdy 2 osoby siedzące przy stole mają inną interpretację, gdzie historia ma się potoczyć, wtedy używamy mechaniki.
            * Nie "gdy _dwie postacie_ są w konflikcie a gdy _dwie osoby przy stole_ 
    4. (4) **System negocjacyjny - coś wygrywasz, coś poświęcasz**
        * Za każdym razem jak wykonywany jest test, Gracz coś stawia na szali i coś chce osiągnąć.
            * Tu wchodzi wielofazowość mechaniki - mogę zatrzymać się szybciej i nie eskalować wyżej.
        * Nagroda jest proporcjonalna do ryzyka 
            * Im wyżej idę tym większa nagroda, ale tym większa szansa, że nie wszystko pójdzie po mojemu.
                * acz to Gracz decyduje co jest skłonny poświęcić i na czym mu zależy
                    * --> Gracz zawsze osiągnie TO CO DLAŃ NAJWAŻNIEJSZE, pytanie czy zapłaci WSZYSTKIM INNYM.
    5. (5) **Podejście taktyczno-dramatyczne. Strategie mają znaczenie. Decyzje i deklaracje Graczy mają OGROMNE znaczenie.**
        * Każde dociągnięcie żetonu ma znaczenie. Nie ma 'automatycznego' ciągnięcia.
        * Różne podejścia do tego samego problemu dają różne wyniki.
            * Na przykład, „Atak frontalny na trolla” vs „Przechytrzenie go, by poszedł na bagno”
        * Gracz poświęcając więcej czasu i energii na sprytniejsze, ciekawsze lub bardziej efektowne rozwiązanie jest wynagrodzony przez większy _Impact_ lub _Probability_ akcji.
            * To oznacza, że Gracze "awansują" jako ludzie. Doświadczony Gracz zrobi dużo więcej niż niedoświadczony Gracz.
    6. (6) **Nie ma możliwości, by osoba siedząca przy stole nie wpłynęła na fikcję**
        * Nieważne jak niedoświadczona czy przestraszona jest osoba, musi być w stanie wpłynąć na fikcję. Jeśli JAKKOLWIEK się stara, coś zmieni.
            * Test: czy gdyby nie było tej osoby, to opowieść potoczyłaby się tak samo. Jeśli choć dla JEDNEJ osoby tak jest, mechanika / prowadzenie zawiodło
        * Nawet niedoświadczona osoba nie wdrożona w sytuacje może zmienić bieg historii. Chociaż lokalnie.
    7. (7) **Gracz ma kontrolę nad ryzykiem**
        * Gracz decyduje "ciągnę lub coś poświęcam / poddaję konflikt"
        * Wynik konfliktu / stawki nie są zaskakujące. Gracz zawsze "czuje" co stawia i co może wygrać. 
            * Zarówno testy jak i potencjalne interpretacje są jawne
    8. (8) **Catchup mechanism**
        * Jeśli ci nie wychodzi i ciągle dociągasz krzyżyki, rośnie szansa na ptaszek. Jeśli wypada ci dużo ptaszków, rośnie szansa na krzyżyk.
    9. (9) **Chaos i nieprzewidywalność**
        * Nawet MG nie może sensownie przewidzieć co się stanie; V/X/O > wola MG
        * Pociągnięcie żetonów daje odpowiedź, nie interpretacja MG / Graczy
2. **Parametry niefunkcjonalne**
    1. (10) **Mechanika jest szybka (gdy się wdrożysz)**
        * Nie musi być ŁATWA, ale musi być BARDZO SZYBKA
        * Złożenie puli - ciągnięcie - interpretacja muszą trwać poniżej 60 sekund CAŁOŚĆ.
    2. (11) **Mechanika jest tania z perspektywy kosztu intelektualnego (gdy się wdrożysz)**
        * Intensywność rozgrywki powinna mieścić się w decyzjach, a nie w wykorzystywaniu mechaniki.
            * Mechanika jest mało zauważalnym silnikiem, który po prostu działa i którego wyniki powodują emocje.
        * Nawet zmęczona i zdekoncentrowana osoba powinna być w stanie dość bezproblemowo wykorzystać mechanikę

Warto tu przeczytać [Jakie są zasady / koncepty Apocalypse World słowami Bakera](https://lumpley.games/2019/12/30/powered-by-the-apocalypse-part-1/)

---
## 4. Obietnica mechaniki

1. **Wpłyniesz na historię jako Gracz. MG nie kontroluje fabuły, mechanika to robi.**
    * Gracz ma statystycznie większe szanse na sukces niż MG. 
    * Co więcej, Gracz wybiera co poświęca a co dzieje się jak Gracz chce.
        * Gracz jest tym co kształtuje w którą stronę pójdzie historia, MG kształtuje okoliczności tej historii.
2. **Będziesz czuć smak zasłużonego zwycięstwa. Twój _performance_ ma znaczenie.**
    * Twoje deklaracje i decyzje mają znaczenie. Lepsza deklaracja -> większa szansa na sukces.
    * Wybór właściwych konfliktów i decyzje co i jak robić pozwolą Ci poprowadzić historię w stronę, która Ciebie interesuje.
    * Wybór właściwych okoliczności konfliktów zdecydowanie zmienia fabułę oraz szanse na sukces.
    * To Gracz wybiera co poświęca. To sprawia, że przy odpowiednio dobrych pomysłach masz więcej szans na osiągnięcie interesującego Cię wyniku.
    * Do każdego problemu można podejść na wiele różnych sposobów. Jak nie wyjdzie siłowo, spróbuj społecznie.
3. **Będziesz czuć frustrację, m.in. z niesprawiedliwości generatora losowego**
    * Przewaga jest po Twojej stronie. Plan był perfekcyjny. Trzy krzyżyki. Przeciwnik był silniejszy. Poddajesz konflikt, bo już nie warto tracić więcej. Niezasłużona porażka.
        * Jak w życiu, nie wszystko możesz wygrać, ale wygrasz to na czym Ci najbardziej zależy - jeśli poświęcisz **wszystko** inne. Jeśli nie jesteś skłonny poświęcić wszystkiego innego, nie zależy Ci na zwycięstwie na tyle - i tak powinno być.
            * Nawet, jak masz sporo krzyżyków pod rząd, w końcu się kończą w puli. A jeśli nie masz już nic do poświęcenia - poświęć postać, sojuszników itp. i weź autosukces (o tym potem).
    * Wszystkie testy są jawne. Nawet MG nie ma jak zmienić wyniku. MG może tylko zinterpretować go łagodniej, ale ma związane ręce jak chodzi o sukces czy porażkę.
    * Nie wiesz co Ci się nie uda. Czasem przegrasz prawie wszystko. Czasem musisz poddać konflikt - może nawet sesję. Nie jest tak jak w filmie, że "bohaterowie zawsze wygrywają".
4. **Nigdy nie przegrasz wszystkiego ani nie wygrasz wszystkiego**
    * Im więcej sukcesów, tym większa szansa na porażkę i vice versa. (mechanizmy catch-up)
    * Im większy sukces chcesz osiągnąć, tym bardziej musisz zaeskalować. To sprawia, że większość konfliktów to "sporo wygrywasz, coś przegrywasz"
        * Nawet przy szansach 50%-50% Ty decydujesz by poświęcić mniej istotne dla Ciebie rzeczy by pozyskać rzeczy dla Ciebie najważniejsze.

---
## 5. Poziomy mechaniki i problem "Wizard i Excel"

Z uwagi na zasady rządzące mechaniką:

* Istnieje zbiór podstawowych zasad, które zawsze działają i których użycie daje ci minimalną EZ.
* Im bardziej zależy ci na modelowaniu złożonych konfliktów, tym więcej musisz użyć zasad bardziej zaawansowanych.
* Czyli zasady możemy podzielić na kręgi. Rdzeń, w którym są zasady podstawowe i potem dodatkowe warstwy Które mogą zmieniać zasady Rdzenia.

Najprawdopodobniej najlepszy zapis zasad jaki może istnieć w tej sytuacji to:

* Zapisać zasady rdzenia najpierw
* Potem zapisać zasady dodatkowe jako mutatory zasad rdzenia.
* Mamy też do czynienia z zasadami alternatywnymi np. „nie masz woreczka żetonów” -> „więc będziemy mapować wszystko na jakąś planszę czy k20”.

Chyba najważniejszy koncept, jaki leży za tym sposobem wykorzystywania mechaniki, w jaki my to robimy, to „Wizard kontra Excel”.

* Wizard mówi. Zrób ten krok potem ten potem ten zawsze w ten sam sposób. Np wpierw rzut na trafienie. Jeżeli się udało to rzut na obrażenia.
    * --> Wizard jest dużo ważniejszy, jeżeli grasz z początkującymi - zawsze znasz następny ruch.
* Excel daje ci zbiór narzędzi i mówi, jak można je wykorzystać, ale w zależności od kontekstu, ty dobierasz sobie odpowiednie elementy Excela, by osiągnąć swój cel.
    * Np masz konflikt Tr+3, ale występuje 2 graczy, więc koloruję połowę ptaszków i krzyżyków na zielono i połowę na różowo. Różowe żetony reprezentują (gracza 1). Zielone ptaszki reprezentują (gracza 2). 
    * Jako podstawowe narzędzia mieliśmy: [żetony, ciągnięcie żetonów, kolorowanie żetonów]. Jako agregację w stylu „Excela” ja zdecydowałem (poza sztywnymi zasadami), jak zamodelujemy to przy użyciu dostarczonych klocków.
    * --> Excel daje PowerUserom możliwość zbudowania perfekcyjnego narzędzia na "silniku" w zależności od kontekstu. Kontrola i precyzja przy zachowaniu uczciwości i zwięzłości instrukcji.

Gdy ja myślę o mechanice, myślę o niej jak o Excelu, ale gdy będę rozpisywał mechanikę, rozpisuję ją, jakby była Wizardem.

---
## 6. Aktualna implementacja

---
### 6.1. High-level

Mechanika jest **mechaniką pulową** z **mechanizmem catch-up**, gdzie **pula reprezentuje 2+ frakcje** a nie 2+ postacie **w kontekście jednego konfliktu**, który może odbywać się w wielu **teatrach / wymiarach**.

Przykład konfliktu z sesji "230528-helmut-i-nieoczekiwana-awaria-lancera":

```
((KONTEKST KONFLIKTU I DEKLARACJA GRACZA:))

TAI -> autonomiczna sztuczna inteligencja
Klaudia naprawia TAI Semlę, która uległa dziwnemu uszkodzeniu na cywilnej jednostce.
Klaudia Stryk użyła swojej TAI - Persefony - do zmapowania instrukcji Semli. 
'Co się stało, czy był sabotaż itp. DLACZEGO Semla się "sama uszkodziła"'.

((RÓŻNICA PRZEWAG))

(+Klaudia używa Persefony jako wsparcia, -anomalia statystyczna jest niewykrywalna)

((PULA STARTOWA))

Tr (Semla to TAI, uszkodzenie anomalne a Klaudia jest podwójnym Mistrzem) +4:

((WYNIKI POCIĄGNIĘĆ I INTERPRETACJE))

* X: Fabian "agent Stryk, skończ z tym statkiem jak będziesz w stanie, dobrze" 
    * /niecierpliwość, ale WIE, że Klaudii się nie pospiesza. Ale pospiesza bez rozkazu.
* V: Klaudia jest zaskoczona. Nie było sabotażu. Nie było błędnych instrukcji. 
    * To jest _failure_ autorepair oraz komponentu software w psychotronice. 
        * To się MOŻE zdarzyć. Mega mała szansa statystyczna, ale możliwe.
    * To NAPRAWDĘ wygląda na awarię, acz dziwną
    * Sprzęt Semli jest poza atestami, przeciągnęli, nie był testowany... 
        * nadal dziwne, ale już bardziej wiarygodne
* X: Fabian powiedział Klaudii, zirytowany, żeby ona zapewniła im bezpieczny powrót do portu i koniec. 
    * To tylko awaria (po tym jak dała raport). On ma dość tej jednostki, lecicie na K1... 
    * Klaudia zapewnia im bezpieczny powrót bez żadnych problemów. 1 godzina więcej. 
* X: Klaudia zrobiła mirror systemów Semli
    * ale mirror nie znalazł tego co jest prawdziwym problemem.
```

Rozpatrujemy konflikt. Każdy konflikt to _różnica przyszłości pomiędzy 2 osobami siedzącymi przy stole_, wpierw trzeba wpierw rozpatrzeć _różnice przyszłości_ między grającymi.

1. **Ustalenie celu i zakresu konfliktu**
    1. **O co gra Gracz**
        * **IDEALNA PRZYSZŁOŚĆ**
            * Klaudia (postać Gracza) naprawia Semlę na ratowanym statku i dowiaduje się, skąd wzięła się awaria; najlepiej by już nigdy na żadnej jednostce się taka nie pojawiła jak się da.
        * **WIĘC AKCJA** 
            * Klaudia próbuje dowiedzieć się, co poszło nie tak i dlaczego doszło do awarii sztucznej inteligencji na statku, który próbują ratować. Fajne bonusy - naprawić statek, zbudować zaufanie tych ludzi do Orbitera itp.
    2. **O co gra MG**
        * **IDEALNA PRZYSZŁOŚĆ**
            * Klaudia naprawia statek kosmiczny i Semlę. Na tym MG nie zależy i o to nie zamierza walczyć. Klaudia ma środki, zasoby, kompetencje i czas.
            * Klaudia nie dowiaduje się niczego o anomalnych cechach awarii, ale zauważa, że coś jest nie do końca w porządku - by wyprowadzić pattern na następną awarię.
        * **WIĘC POTENCJALNE RUCHY - na czym zależy MG w tym momencie sesji?**
            * Uniemożliwić Klaudii odkrycie, na czym awaria naprawdę polega (dowiedzenie się Klaudii bardzo utrudniłby życie MG z perspektywy logiki fikcji, bo Klaudia nie ma jeszcze dość danych by _naprawdę_ móc wywnioskować o co chodziło, nie z tej jednej Semli. ALE Klaudia jest specjalistką od anomalii oraz sztucznych inteligencji, więc...).
            * Wrobienie Fabiana (NPC) w problematyczną sytuację w przyszłości
            * Zbudowanie napięć między Helmutem (NPC) i Anastazym (NPC) lub Fabianem i Anastazym.
            * Jeśli nic nie pasuje: pokazać Fabiana i Klaudię jako "tych co nie chcieli pomóc / nie umieli pomóc na czas" w oczach ratowanej jednostki.
3. **Konstrukcja puli**
    1. **Określenie kontekstu i Teatru**
        * Klaudia próbuje zrozumieć, co się dzieje i zbadać prawdziwą naturę awarii
        * konflikt jest w Teatrze (Wymiarze) Intelektualnym (konflikt naukowy)
    2. **Określenie poziomu Uznaniowego**
        * deklaracja Gracza: _"użyła swojej TAI - Persefony - do zmapowania instrukcji Semli"_
            * cholera, genialny pomysł.
                * jako ekspert od TAI znajdzie nawet mikrouszkodzenia a Persi pomoże jej nic nie zgubić
                * instrukcja-po-instrukcji znajdzie wszystkie odchylenia a jest ekspertem
                * wszelkie mikro-anomalie magiczne TEŻ znajdzie bo się zna na anomaliach magicznych
            * z perspektywy taktyczno-dramatycznej nie da się zrobić nic lepiej, serio jestem pod wrażeniem. A i opisała to świetnie (nie widać w raporcie)
            * --> przydzielam najwyższy możliwy bonus Uznaniowy: `+4.`
    3. **Określenie Przewag**
        * podstawowy konflikt powinien być co najmniej Ekstremalny przez utrudnienia, ale Klaudia jest podwójnym ekspertem (zarówno Mistrzostwo w anomaliach jak i w TAI). No nie ma szans, dla niej to będzie Trudny. I tylko dla niej.
            * (to jest mój 'shorthand' na analizę niektórych Przewag. Technicznie, wyjdzie na to samo, ale jest dużo szybciej)
        * analiza Przewag
            * (-anomalia statystyczna jest niewykrywalna i nie ma śladu magii po tym czasie)
            * (+Klaudia używa Persefony do mapowania wszystkich ścieżek Semli, ma jej wsparcie psychotroniczne i nieludzką precyzję PLUS listę wszystkich instrukcji Semli)
            * --> Przewagi z obu stron się zniwelują, nie ma ekstra (V) ani (X).
    4. **Szukamy Trzeciej Strony**
        * brak Trzeciej Strony, brak (O), czyli entropicznych
    5. --> pula Tr+4, czyli `8X6V4Vr`. P(V, ta pula) = 55.5%.
4. **Ciągniemy z puli i interpretujemy**
    1. wyciągamy (X) z `8X6V4Vr`, zostaje w puli `7X6V4Vr`. P(V, nowa pula) = 58.8%
        1. **Interpretacja fabularna**: zgodnie z kontekstem, wszyscy są już zmęczeni i nie robią nic "ważnego". Klaudia naprawia Semlę i to mapowanie Fabian (jej przełożony) uważa za marnowanie czasu. Więc prosi ją by się pospieszyła bo już wszyscy mają dość a to sytuacja typowa.
        2. **Interpretacja techniczna**: ja i Kić zgodziliśmy się na `Podbicie`, czyli "następny krzyżyk będzie miał dużo wyższy Impact", miałem w planach szybkie zmuszenie Klaudii do zerwania konfliktu, bo przecież naprawa będzie zrobiona.
    2. wyciągamy (V) z `7X6V4Vr`, zostaje w puli `7X5V4Vr`. P(V, nowa pula) = 56.26%
        1. **Interpretacja fabularna**: Klaudia doszła do tego, że przecież system działał prawidłowo choć poza atestami. To "jedna szansa na milion". Czasem, statystycznie się zdarza, ale to dziwne.
        2. **Interpretacja techniczna**: dla Kić było ważne, czy to "normalne" czy coś wymaga jej działania. Pierwszy ptaszek udowodnił, że to "normalne ale dziwne". Czyli ma dowód, że nic nie mogła zrobić, ale jednocześnie wszystko działa. Potraktowaliśmy jako potwierdzenie oraz `Podbicie`.
    2. wyciągamy (X) z `7X5V4Vr`, zostaje w puli `6X5V4Vr`. P(V, nowa pula) = 60%
        1. **Interpretacja fabularna**: Fabian ma dość. Klaudia wszystko naprawiła i ma dowód że to normalne. Jest zniecierpliwiony i Klaudia marnuje czas jego i zespołu. Rozkaz - wracamy. Klaudia nie może więcej analizować.
        2. **Interpretacja techniczna**: Podbicie zamanifestowaliśmy w "nie ma czasu, wracamy asap". Kić stwierdziła "oki, ale mam mirror psychotroniki Semli. Mogę badać dalej po symulacjach". Czyli może zaeskalować jeszcze raz. Dobrze to wymyśliła; jakkolwiek nie znajdzie bezpośredniej anomalii, może jeszcze znaleźć ślady przeskoków w ścieżkach wynikających z magii. Więc ma rację.
    2. wyciągamy (X) z `6X5V4Vr`, zostaje w puli `5X5V4Vr`. P(V, nowa pula) = 64%
        1. **Interpretacja fabularna**: Klaudia prawidłowo zmirrorowała dane i ścieżki z Semli, ale nie wykryła nic znaczącego. Jedynie się potwierdziło - DZIWNE, mogło się zdarzyć ale zarówno autorepair zawiódł jak i psychotronika. Bardzo rzadkie, żeby wszystkie redundancje padły, acz możliwe.
        2. **Interpretacja techniczna**: niektóre ślady czy echa były na poziomie czysto magicznym, więc widocznie nie wyszły na symulacjach. Ale mogłem podnieść Kić sygnał "dziwne ale możliwe". Jedna z tych rzeczy jakie się zapamięta "na przyszłość". 
        * Btw, szansa na tylko 1V przy 4 pociągnięciach jest dość niska.
        * Kić prawidłowo maksymalizowała swoje ruchy i zamiast atakować jej pozycję musiałem rozpaczliwie bronić sekretu anomalii statystycznej, przez co nie miałem jak dobrze jej zaatakować.

Dobrze, przejdźmy więc do tego jak to robić

---
### 6.1. Cel i zakres konfliktu

Każdy konflikt to _różnica przyszłości pomiędzy 2 osobami siedzącymi przy stole_, wpierw trzeba wpierw rozpatrzeć _o co w ogóle walczymy_. Najprościej to zrobić jako **różnica światów idealnych dwóch stron w kontekście aktualnej sytuacji**. 

Dlatego pytam "co próbujecie osiągnąć" lub "jak wygląda sukces".

`f(kontekst aktualnej sytuacji, różnica(świat idealny Gracza, świat idealny MG)) -> konflikt lub jego brak`

* Z tego mogę wyprowadzić jakie frakcje występują w konflikcie, czy frakcje są reprezentowane przez 1 lub więcej postaci, okoliczności konfliktu itp.
* Dopiero potem mogę ocenić jak trudny jest konflikt, jakie są przewagi lub _czy w ogóle_ występuje konflikt.

Weźmy przykład dla zobrazowania sytuacji.

```
Planetoida Seikir. Atak piratów. Rodzina: Alicja, Bartek (Gracz) i dwójka dzieci. Są w swojej kwaterze.
Wpada pirat Szczepan (NPC), w ciężkim serwopancerzu szturmowym. Rodzina jest nieuzbrojona i niegroźna.
Szczepan dostał za zadanie znaleźć wartościowe dane i zabić wszystkich.
Bartek bardzo by chciał, by jego rodzina przeżyła.
```

* Jak wygląda świat idealny Gracza?
    * najważniejsze: Pirat nie zabija jego rodziny ani jego postaci
    * drugorzędnie: zapewnić bezpieczeństwo rodzinie
    * trzeciorzędnie: uzbroić się i pokonać piratów?
* Jak wygląda świat idealny MG?
    * Nie zależy **jemu** na zabiciu Bartka, nawet, jeśli Szczepan powinien go zabić. Wolałby by Bartek przeżył. Ale wiecie, gramy ostro ;-).
    * Zabicie rodziny Bartka jest rzeczą której nie chce, ale akceptuje.
    * Piraci wpadają, dostają cenne dane i wypadają
    * Nikt nie wie kim byli piraci lub że w ogóle byli tu piraci. Siły policyjne mają się pojawić i znaleźć opustoszałą planetoidę bez cienia śladów.

Czyli jak teraz będzie wyglądał konflikt?

* MG: "Co chcesz osiągnąć?"
* Gracz: **"Chcę postrzymać pirata przed zabiciem mojej rodziny"**
* MG: "Oki - jak chcesz to osiągnąć?"
    * MG wie już **co** Gracz chce. Teraz może przeanalizować w jaki sposób propozycja Gracza zadziała.
* Gracz: "Krzyczę do rodziny 'Uciekajcie!', po czym rzucam się na pirata. Nie uda mi się go pokonać, ale przynajmniej go spowolnię by byli w stanie uciec. Akceptuję, że zginę."
* MG: (procesuje)
    * serwopancerz sprawia, że pirat ma siłę ~50 kg na jednej ręce. Spowolnienie - nieefektywne. Czy Gracz tego nie wie? Źle rozumie sytuację?
    * NAWET jeśli rodzina ucieknie ze swojej komnaty, jest wielu piratów. Strategia nieskuteczna.
* MG -> Gracz: (wyjaśnia problem). "Czy podtrzymujesz deklarację? Najpewniej mówimy o czymś bliskim poziomowi heroicznemu i więcej niż jeden ptaszek."
* Gracz: "Nie, to może... dobra, piraci atakują z jakiegoś powodu. Może jeśli udowodnię, że jesteśmy bardziej przydatni żywi niż martwi to może uratuję rodzinę"
* MG: (procesuje)
    * Gracz nie wie czemu tu są, ale piraci mają konkretny cel
    * Jest niemała szansa, że się uda coś tu załatwić. Pirat Szczepan ma swoją rodzinę i nie lubi zabijać ludzi. Nie dlatego jest piratem bo jest sadystą a dlatego że ma długi i nie zna innego życia.
        * ((nawet jeśli wcześniej tego faktu nie było, w tym momencie MG może go dodać, bo czemu nie? Lepsza opowieść.))
        * ((jeśli nie dodamy, można wygenerować ten fakt mechaniką - Gracz może DODAĆ ten aspekt do pirata Szczepana na przestrzeni konfliktu))
* MG -> Gracz: "Dobrze, to może zadziałać, tu są pewne pola manewrów."
* Gracz: "Oki. To:"
    * **"Wpierw pokażę piratowi że jesteśmy niegroźni i że jesteśmy chętni pomóc"**
    * **"Potem dowiem się czemu piraci tu są i jak mogę im pomóc by ochronić rodzinę"**
    * **"I potem pomyślimy co dalej XD"**
* MG: (procesuje)
    * Jest konflikt, są określone cele i zakresy sukcesów
    * Konflikt będzie w Teatrze Społecznym i/lub Emocjonalnym, potencjalnie Umysłowym
        * Konflikt na pewno nie będzie w Teatrze Fizycznym; tam serwopancerz daje upiorną przewagę
    * Możemy przejść do deklaracji i konstrukcji puli.

---
### 6.2. Konstrukcja puli
#### 6.2.1. Podstawa, baza

Każdy konflikt zaczyna od tego samego punktu startowego. Tzw. konflikt Trudny +2 (notacja: Tr+2).

Ten konflikt jest rozłożony jako `8X6V2Vr`, czyli `XXX XXX XX VVV VVV VrVr`. 8 krzyżyków, 6 ptaszków białych, 2 ptaszki czerwone.

![Tr+2, wizualnie](Materials/230603/230603-01-pula-expl.png)

* Wyciągnięcie (V) reprezentuje ruch dla Gracza i celu Gracza (lub "strony aktywnej")
* Wyciągnięcie (X) reprezentuje ruch dla MG i celu MG (lub "strony drugiej")

Oki, skąd się wzięła ta baza?

* Najniższy konflikt to konflikt "Typowy"
    * pełna karta postaci: `9V`
    * standardowa ilość krzyżyków: `5X`
* Mechanika chodzi na "trójkach"
    * Czyli Trudny: `-3V +3X` = `6V 8X`
    * Czyli Ekstremalny: `-3V +3X` = `3V 11X`
    * Czyli Heroiczny: `-3V +3X` = `0V 14X`
* Istnieje pewna ilość ptaszków Uznaniowych, za "dramatyczno-taktyczne" podejście
    * Zakres: [0, 4]; ZWYKLE: +2
    * Są dawane za dopasowanie, całkowicie arbitralnie przez MG
* --> stąd `8V 8X`. 
    * "Przypadkowo", daje to podstawę 50% (a potem mamy catch-up by zwiększyć szanse)

Czyli (kolorem **zielonym** pokazuję dodane żetony wobec Trudnego):

![Różne bazy](Materials/230603/230603-02-bazy.png)

Jak przydzielam bazę?

1. Każdy konflikt zaczynam od "Trudnego"
2. Jeśli jedna ze stron ma ZDECYDOWANĄ przewagę, będzie "Typowy" albo "Ekstremalny" (zależy od strony)
    1. Np. jeden koleś próbujący walczyć wręcz z grupą kolesiów? -> Ekstremalny.
    2. Jeśli "nie masz wyczucia", załóż że **każdy** konflikt ma bazę Trudną. Mechanika i tak zadziała, wyjdzie Ci zmiana z Przewag potem.

#### 6.2.2. Poziom uznaniowy

Raz zaczynam od tego, raz od analizy Przewag. Zwykle od Przewag. Ale może być łatwiej zacząć od tego.

Istnieje pewna ilość ptaszków Uznaniowych, za "dramatyczno-taktyczne" podejście, w zakresie [0-4]. Mam bardzo prosty sposób przydzielania:

`Vr` notacyjnie oznacza "czerwony żeton ptaszka".

* **0 Vr** jeśli Gracz zrobił odpowiednik "to ja go tnę". Zero myśli. Zero pomysłu. Zero rozważenia taktyki. Zero starań.
* **1 Vr** jeśli początkujący Gracz powiedział cokolwiek. Dla doświadczonego 1 Vr za coś więcej niż "to ja go tnę".
* **2 Vr** jeśli Gracz włożył jakąkolwiek energię i myśl. Jeśli to ma jakikolwiek sens, ma 2Vr.
* **3 Vr** jeśli Gracz ma fajny pomysł
* **4 Vr** jeśli Gracz ma bardzo fajny pomysł i mnie zaskoczył / ma to ogromny sens.

Tyle. Rozpatruję wszystko co da się podpiąć pod "dramatyczno-taktyczny", czyli:

* czy pomysł ma jakikolwiek sens powodzenia (taktyka)
* czy pomysł używa czegoś z karty postaci, otoczenia, działań innych Graczy (taktyka)
* czy są jakiekolwiek detale i starania (dramatyczność / opis)
* czy innym lub mnie się pomysł podoba (dramatyczność)
* czy to pasuje do tego jak ta postać powinna się zachować (dramatyczność)
* itp.

Po co jest nam Poziom Uznaniowy? Ano, by zachęcać Graczy do dramatyczności i taktyki. Co nagradzasz to dostajesz.

#### 6.2.3. Przewagi

Pamiętacie, że mechanika chodzi na zasadzie "trójek", czyli +3V / +3X? Tu się nam to znowu przyda.

Wpierw podsumuję, czyli co naprawdę się tu dzieje i jak działają Przewagi:

1. Przewaga jest czymś co pochodzi z zasobów, kontekstu, otoczenia itp. Może być na korzyść Gracza lub Drugiej Strony.
2. Jedna Przewaga po stronie Gracza i jedna Przewaga po Drugiej Stronie się niwelują.
    1. Jeśli Gracz ma 4 Przewagi a Druga strona ma 3 Przewagi, Gracz ma 1 Przewagę, więc najpewniej `Tr P +2`.
3. Każda Przewaga daje +3 żetonu określonego typu, kolorowego
    1. Przewaga po stronie Gracza daje 3Vy
    2. Przewaga po Drugiej Stronie daje 3Xy
4. Każde dwie Przewagi zmieniają kategorię konfliktu o 1.
    1. oś: Typowy -- Trudny -- Ekstremalny -- Heroiczny
        1. Trudny z dwoma Przewagami gracza staje się Typowy
        1. Trudny z dwoma Przewagami Drugiej Strony staje się Ekstremalny
        1. Trudny z trzema Przewagami Drugiej Strony staje się Ekstremalny i dodajemy ekstra 3Xy

A teraz to wyprowadzimy.

Zacznijmy od prostego przykładu - **JEDNA PRZEWAGA**.

```
Grzegorz (klub graczy w Unreal Tournament) stoi naprzeciw Daniela (klub graczy w League of Legends).
Uczestniczą w debacie klasowej - jaki klub będzie miał dofinansowanie ze szkoły. 
Oboje chcą przekonać innych do swego zdania.
Grzegorz ma jednak sekretną broń - ściągnął na spotkanie klakierów, którzy mają wyprowadzić Daniela z równowagi.
```

Załóżmy, że Grzegorz (postać Gracza) i Daniel (NPC) mają podobne umiejętności i Grzegorz się postarał tak po swojemu. To daje nam standardową pulę `Tr+2`, czyli `8X 6V 2Vr`.

Ale Grzegorz ma klakierów, którzy dają mu **PRZEWAGĘ**. Czyli konflikt wygląda tak:

Tr P1 (klakierzy) +2 = `8X 6V 3Vy 2Vr`. Przewaga daje Grzegorzowi +3Vy, czyli trzy żółte ptaszki.

![Różne bazy](Materials/230603/230603-03-przewaga.png)

Możemy jednak zmodyfikować sytuację. Zobaczmy jakie siły działają na debatę Grzegorz - Daniel. Notacja `(+XXX)` oznacza, że to Przewaga na korzyść strony aktywnej (Grzegorza) a `(-XXX)` na korzyść Daniela.

Więc, rozpatrzmy to - **WIELE PRZEWAG**:

* (+ Grzegorz ściągnął klakierów)
* (- League of Legends to bardziej popularna gra komputerowa niż Unreal Tournament, więc ludzie ją wolą)
* (+ Grzegorz przekonał dziewczynę Daniela, żeby optowała za Unreal Tournament co szkodzi Danielowi w debacie)
* (+ Grzegorz jest dużo bardziej lubiany przez władze szkoły niż Daniel)

Mamy **trzy Przewagi** po stronie Grzegorza i **jedną Przewagę** po stronie Daniela. Teoretycznie daje nam to taką pulę (kolorem **zielonym** reprezentuję modyfikacje puli Tr+2 wynikającą z Przewag):

![Grzegorz vs Daniel](Materials/230603/230603-04-grzegorz-daniel-1.png)

Mamy więc pulę `8X 6V 2Vr 9Vg 3Xg`, czyli (usuwając kolory) `11X 17V`

Z perspektywy prawdopodobieństw:

* P(V) = 60.7%
* P(X) = 39.2%

Przewagi jednak się niwelują. Tak więc mamy w rzeczywistości **dwie Przewagi** dla Grzegorza i **zero Przewag** dla Daniela (bo `3 vs 1` == `2 vs 0`). Tak więc pula naprawdę wynosi:

![Grzegorz vs Daniel](Materials/230603/230603-05-grzegorz-daniel-2.png)

Mamy więc pulę `8X 6V 2Vr 6Vg`, czyli (usuwając kolory) `8X 14V`

* P(V) = 63.6%
* P(X) = 36.3%

Co ważniejsze, to powoduje że **baza** ma większe znaczenie.

Kolejnym manewrem jaki robię: każde **dwie Przewagi** zmieniają bazę o jedną kategorię.

Czyli 

* `Tr P1 P2 +2` (Trudny +2 z dwoma Przewagami) -> `Tp +2` (Typowy +2 bez Przewag)
* `Tr P1 P2 P3 +2` (Trudny +2 z trzema Przewagami) -> `Tp P1 +2` (Typowy +2 z jedną Przewagą)

W wypadku naszego konfliktu:

![Grzegorz vs Daniel](Materials/230603/230603-06-grzegorz-daniel-3.png)

Mamy więc pulę 5X 9V 2Vr, czyli (usuwając kolory) 5X 11V

* P(V) = 68.7%
* P(X) = 31.3%

**Czyli, podsumowując**

1. Przewaga jest czymś co pochodzi z zasobów, kontekstu, otoczenia itp. Może być na korzyść Gracza lub Drugiej Strony.
2. Jedna Przewaga po stronie Gracza i jedna Przewaga po Drugiej Stronie się niwelują.
    1. Jeśli Gracz ma 4 Przewagi a Druga strona ma 3 Przewagi, Gracz ma 1 Przewagę, więc najpewniej `Tr P +2`.
3. Każda Przewaga daje +3 żetonu określonego typu, kolorowego
    1. Przewaga po stronie Gracza daje 3Vy
    2. Przewaga po Drugiej Stronie daje 3Xy
4. Każde dwie Przewagi zmieniają kategorię konfliktu o 1.
    1. oś: Typowy -- Trudny -- Ekstremalny -- Heroiczny
        1. Trudny z dwoma Przewagami gracza staje się Typowy
        1. Trudny z dwoma Przewagami Drugiej Strony staje się Ekstremalny
        1. Trudny z trzema Przewagami Drugiej Strony staje się Ekstremalny i dodajemy ekstra 3Xy

#### 6.2.4. Żetony entropiczne, "trzecia strona"

Tu się dopiero zaczyna zabawa.

Wpierw podsumujmy:

* żetony entropiczne (O) służą do reprezentowania ruchu Trzeciej Strony
* może być więcej niż jedna Dodatkowa Strona - dlatego mamy kilka kolorów entropicznych żetonów
* "zwykły" wpływ Dodatkowej Strony to `+3O`
* "potężny" wpływ Dodatkowej Strony to `+5O`

A teraz wracamy do wyjaśnień:

Do tej pory wszystko było proste, bo mieliśmy w konflikcie jedynie dwie strony. A co, jeśli mamy do czynienia z WIĘCEJ niż dwoma stronami? Dam parę przykładów dla zilustrowania problemu:

1. Alan i Barbara walczą na miecze w płonącej świątyni.
    * Alan: chce pokonać Barbarę
    * Barbara: chce pokonać Alana
    * **ALE** świątynia może się zawalić / ogień może skrzywdzić Alana i Barbarę
2. Alan i Barbara walczą na miecze na strzeżonym podziemnym parkingu
    * Alan: chce pokonać Barbarę i zostać niezauważonym
    * Barbara: chce pokonać Alana i zostać niezauważonym
    * **ALE** strażnik parkingu może przyjść i zauważyć Alana i Basię
3. Wampir Włodek próbuje porwać ranną, krwawiącą Urszulę z jej bazy
    * Włodek: chce pomóc Uli i ją porwać z bazy
    * Ula: chce odeprzeć Włodka, nie dać się porwać i wezwać wsparcie
    * **ALE** jest możliwość, że Włodkiem zawładnie _krwawy głód_. Wampir i te sprawy.

(V) reprezentują sukcesy Gracza, (X) MG / Drugiej Strony. Ale co reprezentuje Trzecią Stronę? Żetony entropiczne, w dalszej notacji (O).

Zamodelujmy pulę Wampira Włodka porywającego Ulę. Popatrzmy na Przewagi i bazy.

* baza: Trudny.
* (+ Włodek jest wampirem co oznacza 'homo superior')
* (+ Ula jest ranna i specjalnie się nie umie bronić bo jest ranna)
* --> baza: Typowy.
* (O Ula jest ranna i krwawi a Włodek się musi bardzo skupiać i jest rozproszony)
    * --> to znaczy, że na Włodka działa duży wpływ _Krwawego Głodu_.
        * reprezentujemy przez `5Or`, bo duży ("potężny").

Pula: `Tp+2+5Or`:

![Wampir Włodek](Materials/230603/230603-07-wampir-wlodek.png)

* P(V) = 52.3%
    * 52% szansy, że Włodek opanowuje krwawienie Uli i porywa ją z bazy.
* P(X) = 23.8%
    * 24% szansy, że Ula wzywa wsparcie, odpełza i ogólnie jest nieporwana.
* P(Or) = 23.8%
    * 24% szansy, że Włodka opanowuje _Krwawy Głód_ i robi się niebezpieczny dla Uli i innych ludzi

Ale entropiczne żetony dają nam jeszcze lepsze możliwości. Popatrzcie na inny przykład.

```
Rozpadająca się podziemna świątynia, która pogrzebie wszystkich.
Policja wbijająca do świątyni by ratować zakładników Mrocznego Kultu.
Postacie Graczy walczący z Kultystami.
Kryształowy Feniks, artefakt, zsuwający się w przepaść - na który polują i Kultyści i Gracze.
```

Mamy kilka rzeczy dziejących się jednocześnie. Zamodelujmy to.

* **Gracze** chcą powstrzymać Kultystów przed zabiciem zakładników i przed pozyskaniem Feniksa. Sami też chcą zdobyć Feniksa dla siebie. **Kultyści** chcą pokonać postacie Graczy i zawłaszczyć Feniksa dla siebie. To jest "główna podstawa puli".
    * Załóżmy, że pula Gracze vs Kultyści to `Tr P +2`. Jedna Przewaga Graczy, mimo, że Kultystów sporo.
* **Rozpadająca się świątynia** stanowi Hazard (zagrożenie). Jest to DUŻE zagrożenie, więc +5 żetonów entropicznych. Przypisuję jej czerwone żetony, więc `5Or`. 
    * Gdy wypadnie trzeci Or, wszyscy giną.
        * (tak, Postacie Graczy nadal wolą walczyć o Feniksa niż ratować zakładników. Norma.)
* **Policjanci** stanowią siłę ratującą zakładników i pomagającą Graczom. Jeśli się pojawią, Gracze dostaną `+3Vg` do puli. Jest to NORMALNA sytuacja, więc 3 entropiczne. Przypisuję im kolor zielony, więc `3Og`.
* Przez rozpadającą się świątynię **Feniks może spaść w przepaść**. NORMALNA szansa, przypisuję kolor niebieski entropicznym, więc `3Ob`. Jeśli wypadnie pierwszy Ob, dowolna strona może ptaszkiem lub krzyżykiem uratować i przechwycić Feniksa.

Czyli pula wygląda tak: `Tr P +2 +5Or +3Og +3Ob`:

![Złożony multi-entropiczny](Materials/230603/230603-08-kilka-entropicznych.png)

**Podsumowując**

* żetony entropiczne (O) służą do reprezentowania ruchu Trzeciej Strony
* może być więcej niż jedna Dodatkowa Strona - dlatego mamy kilka kolorów entropicznych żetonów
* "zwykły" wpływ Dodatkowej Strony to `+3O`
* "potężny" wpływ Dodatkowej Strony to `+5O`

#### 6.2.5. Dynamiczna zmiana puli

Pula nie jest statyczna. Wyobraźmy sobie taką sytuację.

```
Celina okrutnie wyśmiewa Darka by zadać mu ranę emocjonalną.
Darek nie radzi sobie z emocjami, ale nie będzie płakał.
    Zmienia prawie-płacz w gniew, unosi pięść by uderzyć Celinę.
Teraz Celina ma przechlapane, bo jest słabsza od Darka.
```

* Pula "Celina zmusza Darka do płaczu" to najpewniej `Tr P +2`.
* Jednak gdy Darek przejmuje inicjatywę i chce bić Celinę, to Celina chcąc uciec lub się obronić stoi przed pulą `Ex +2` przez liczne przewagi Darka. Znaczy, ma przechlapane.
* --> Czyli pula może się zmienić przez zmianę Teatru / Wymiaru.

Inny przykład.

```
Zorro pojedynkuje się z sierżantem Gonzalesem.
Zorro wykorzystuje swoje sukcesy, by Gonzales musiał walczyć patrząc w słońce.
```

Z technicznego punktu widzenia wyglądałoby to tak: 

* załóżmy, że startowa pula to `Tr P +2`; Zorro ma przewagę umiejętności.
* po wyciągnięciu ptaszka, Zorro zadeklarował przesunięcie się i wykorzystanie aspektu terenu "ostro palące słońce" tak, by sierżant Gonzales musiał patrzeć w słońce.
    * to sprawia, że Zorro dostaje kolejną Przewagę. Pula zmienia się `Tr P +2` -> `Tp +2`.
        * nie chciałbym być sierżantem Gonzalesem przy takiej puli.

---
### 6.3. Ciągnięcie z puli

Załóżmy, że mamy pulę `Tr Z +2`, czyli z jedną Przewagą.

Po wyciągnięciu 1 żetonu z puli ubywa wyciągniętego żetonu. Wyciągnęliśmy `Vr`:

![Pula z jedną Przewagą](Materials/230603/230603-09-wyciagniety-zeton.png)

Z tej samej puli wyciągnijmy jeszcze dwa żetony.

![Take 3](Materials/230603/230603-10-take-3.png)

Wyciągnięte żetony nie wracają do puli. Zwykle. Jest bowiem reguła **jeśli w puli są żetony jakiegoś typu, zawsze musi w puli pozostać choć jeden żeton tego typu**. 

Czyli jeśli np. mamy pulę `10V 10X 2Or` to:

* `10V 10X 2Or`, wyciągamy `Or` -> `10V 10X 1Or`
* `10V 10X 1Or`, wyciągamy `Or` -> `10V 10X`, więc musimy zwrócić `Or` -> `10V 10X 1Or`.
    * Wynik się liczy. Po prostu nie możemy usunąć "ostatniego" żetonu.

Jest to ważne, bo jak mamy kolorowe żetony, one też są określonego "typu". Czyli jeśli mamy pulę `6V 6X 1Vr 1Xr`, to jeśli wyciągniemy `1Vr` lub `1Xr`, zwracamy je do puli.

Co ważne - ta reguła skupia się tylko na wyciąganiu żetonów z puli. Jeśli w wyniku zmiany Teatru czy działań Gracza/Mg do puli wejdą np. `3Or` a potem w wyniku kolejnej zmiany one powinny zniknąć, to znikają. **Po prostu nie możemy przez pociągnięcie usunąć ostatniego żetonu danego typu**.

---
### 6.4. Eskalacje

**_Zasady_**:

1. pule wykorzystywane są wielokrotnie. Pierwszy wyciągnięty żeton nie jest ostatnim.
2. (V), (X) może prowadzić do normalnych konsekwencji lub dużych konsekwencji.
    1. 'Podbicie' jest operacją powodującą, że następny żeton tego samego typu w tym samym kontekście da Ci duże konsekwencje.

**_Wyjaśnienie_**:

Zarówno (V), (X) jak i (O) reprezentują jakieś **konsekwencje testu w fikcji**. Jako przykład (V), to może być coś w stylu _"Zorro się przesunął tak, że porucznik Gabriel walczy pod słońce"_ (normalna konsekwencja) lub _"Zorro sprawnym sztychem zabija porucznika Gabriela"_ (duża konsekwencja).

Tak więc zarówno (V) jak i (X) mogą spowodować konsekwencje **normalne** albo **duże**.

Weźmy przykład konfliktu.

```
Gdzieś w okolicy miasta jest ukryty Krystaliczny Mimik który poluje na ludzi by ich zjadać. 
Grupa żołnierzy go szuka, kierowanych przez oficera (Viorikę). 
Viorika z 2 sojusznikami czekają w powietrzu w serwopancerzach by go zestrzelić po wykryciu.
```

Konflikt jest dwufazowy:

* faza 1: znalezienie mimika
* faza 2: zestrzelenie mimika

Nie mamy pojęcia gdzie jest mimik (-przewaga), ale mamy całą siatkę żołnierzy przeczesującą okolicę (+przewaga) a mimik jest "zwierzęciem" i wiemy jak się zachowuje (+przewaga).

Pula: `Tr Z +4 (8X 6V 3Vy 4Vr)`

* Wyciągamy żeton: `V` (nowa pula: `8X 5V 3Vy 4Vr`)
    * przeciwnik czuje się bezpiecznie, więc mamy gwarancję, że "mimik się pojawi". (konsekwencja normalna)
        * Ale to za mało. Chcemy go w bezpiecznym miejscu by nikomu nic się nie stało.
        * **Eskalujemy** by dodać korzystne okoliczności wyjścia mimika, np. mamy prostą linię strzału.
* Wyciągamy żeton: `X` (nowa pula: `7X 5V 3Vy 4Vr`)
    * zakładamy, że Mimik zadziałał z zaskoczenia i zranił żołnierza-przynętę. (konsekwencja normalna)
        * Gracz **nie eskaluje wyżej**. Nie uważa, że warto dodawać dodatkowych okoliczności.

Stan aktualny jest taki: mimik jest wykryty, żołnierz jest ranny i nasze damy (Viorika, Arianna) mają go (zarówno żołnierza jak i mimika) na celowniku. Arianna skupia się na ratowaniu żołnierza, Viorika robi ogień zaporowy by spłoszyć mimika.

Analiza przewag: (-mimik jest cholernie szybki), (+ogień zaporowy + zaskoczenie), (+trzech na jednego mimika)

Pula: `Tr Z +3 (8X 6V 3Vy 3Vr)`

* Wyciągamy żeton `X` (nowa pula: `7X 6V 3Vy 3Vr`)
    * **mam dostęp tylko do normalnej konsekwencji**, więc mimik ciężko rani żołnierza. To stanowi **podbicie** do następnego krzyżyka - wtedy mogę zeżreć żołnierza (mam dostęp do dużej konsekwencji)
* Wyciągamy żeton `V` (nowa pula: `7X 5V 3Vy 3Vr`)
    * Arianna zbliża się do mimika i żołnierza by go złapać i odciągnąć; Viorika otwiera ogień. Arianna osłania żołnierza (normalna konsekwencja, **podbicie** do następnego ptaszka)
* Wyciągamy żeton `V` (nowa pula: `7X 4V 3Vy 3Vr`)
    * **duża konsekwencja**. Arianna ewakuuje żołnierza. Żołnierz ma gwarancję przeżycia (mimik nie ma do niego dostępu w tym konflikcie).

Czas na pokonanie mimika. Cała trójka pozycjonuje się poza zasięgiem mimika (choć Arianna odciąga uwagę mimika od żołnierza, więc jest wrażliwa) i otwiera ogień z broni maszynowej.

(+dużo pocisków więc morficzność mimika nie chroni, +wystarczająco otwarty teren, +przeciwnik jest rozbity)

Pula: `Tp Z +3 (5X 9V 3Vy 3Vr)`

* Wyciągamy żeton `X` (nowa pula: `4X 9V 3Vy 3Vr`)
    * **normalna konsekwencja**, więc mimik robi **podbicie** i poważnie uszkadza swoimi krystalicznymi wiciojęzykami serwopancerz Arianny, łapiąc ją za nogę.
* Wyciągamy żeton `V` (nowa pula: `4X 8V 3Vy 3Vr`)
    * **normalna konsekwencja**, więc Gracze robią **podbicie** - mimik nie ma gdzie się wycofać i dostaje rany. Jego krystaliczna struktura się zaczyna sypać.
* Gracze nie zmieniają celu ani Teatru. Tak więc, kontynuujemy. Czy Arianna prawie straci nogę (lub poświęci żołnierza) czy mimik zginie?
* Wyciągamy żeton `V` (nowa pula: `4X 7V 3Vy 3Vr`)
    * **podbicie -> duża konsekwencja**, więc wreszcie możemy zabić cholernego mimika. Nie ma to jak ogień trzech karabinów maszynowych w rękach wspomaganych serwopancerzami łowców potworów.

Jako, że to nie zawsze oczywiste; co może być dużą konsekwencją a co normalną?

* zarówno dla **(X)** jak i dla **(V)**
    * **normalne konsekwencje** - coś, co zmienia sytuację w kontekście tego konfliktu lub stanowi coś wartościowego
        * podbicie w kontekście konkretnej akcji (np. "przesunięcie się by użyć słabego punktu w pancerzu kraba")
        * usunięcie podbicia ze strony przeciwnika (np. "krab się przesunął i szczelina w pancerzu zniknęła")
        * nadanie aspektu tymczasowego ("stoisz pod słońce", "gdy przemawiałeś, ta dziewczyna zwróciła Twoją uwagę")
        * wykorzystanie negatywnego aspektu drugiej strony ("Martyn zaryzykuje wszystko by tylko nie ucierpieli sojusznicy")
        * stworzenie Hazardu w lokalizacji ("zerwanie drutów, by było miejsce rażące prądem")
        * obrażenia wpływające na fikcję, ale kosmetyczne ("krwawisz", "kończy Ci się amunicja")
        * tymczasowe wyłączenie zasobu ("Twój miecz został Ci wytrącony")
        * odwrócenie uwagi przeciwnika / skupienie uwagi na sobie
        * pozyskanie zasobu tymczasowego ("łapię piasek by rzucić komuś w oczy")
        * zmiana Wymiaru (_o tym w następnym punkcie_)
        * kupienie czasu aż ktoś inny coś zrobi
    * **duże konsekwencje** - coś, co bardzo zmienia stan konfliktu lub go kończy
        * nadanie aspektu trwałego ("Kerrigan nie będzie miała serca zranić Raynora")
        * wyłączenie aspektu trwałego / zasobu ("krabowi pęka superciężki pancerz", "Lancer Vioriki jest niesprawny")
        * zadanie obrażeń / zabicie celu
        * odzyskanie tymczasowo utraconego / wyłączonego zasobu (_tak, lepiej użyć innego tymczasowego_)
        * trwałe unieruchomienie / wyłączenie akcji przeciwnika ("nie jesteś w stanie uciec bez pomocy")

To powyżej nie jest pełną listą; to raczej heurystyka. Część mikro-akcji powyżej może być podbiciem.

Popatrzcie na poniższą sytuację:

1. Conan Barbarzyńca stoi naprzeciw hordy goblinów. Przechlapane.
    * Gracz Conana mówi "szukam wąskiego przejścia, bym naraz mógł ze mną walczyć tylko jeden lub dwa"
        * wystarczy **normalny (V)** by dodać coś takiego, jeśli nie jest pod presją i gobliny nie są blisko. Jeśli byłyby blisko, będzie musiał 
            * faza 1: biec szybciej niż one (**normalny V**) 
            * faza 2: znaleźć wąskie przejście i tam się dostać (**normalny V**)
        * gdybym miał **normalny (X)**, dodałbym aspekt / okoliczność do takiego miejsca - "oki, ale to przejście jednostronne; będziesz tam uwięziony; nie masz czasu na znalezienie lepszego przesmyku"
            * Gracz mógłby to skontrować eskalując **normalnym (V)** "może inaczej - nie prowadzi do bezpiecznego miejsca i nie mogę uciec biegnąc, ale jeśli zostawię niewygodny sprzęt to mogę pełzoczołgać się GDZIEŚ".
            * Gdybym chciał powiedzieć "nie, SERIO jesteś uwięziony i sam nie wyjdziesz" to muszę mieć **dużą konsekwencję**.
    * Gracz który mówi "bohatersko atakuję" skończy martwy. Tu nie ma testu.
    * _Zauważcie, że w powyższym "dialogu" edytujemy ten sam przesmyk wg. zasady "TAK ALE" lub "TAK I". Czyli nie anulujemy deklaracji drugiej strony tylko ją poszerzamy. By móc coś naprawdę odciąć potrzebna jest **duża konsekwencja**.

---
### 6.5. Teatry / Wymiary

**_Zasady_**:

1. Są cztery Teatry (Fizyczny, Umysłowy, Emocjonalny, Społeczny).
2. Strona aktywna (mająca inicjatywę) wybiera Teatr konfliktu przez swoją deklarację i działania.
3. W różnych Teatrach pule mogą mieć różne wartości ("Fizycznie: Tr, Społecznie: Ex") mimo kontynuowania tego samego konfliktu.

**_Wyjaśnienie_**:

Nie wszystkie konflikty toczą się w tym samym Wymiarze (Teatrze):

* **Fizyczny**: ogólnie rozumiane "ciało". Rzeczy dziejące się w "świecie materialnym"
    * bicie się, skradanie, zauważanie szczeliny w skale, rozginanie krat
* **Umysłowy**: rzeczy wymagające skupienia i kombinowania
    * taktyka, gra w szachy, badania naukowe, konstrukcja urządzenia, planowanie pułapki
* **Emocjonalny**: elementy związane z empatią, manipulacją emocjonalną, zarządzania swoimi emocjami
    * zauważanie smutku przyjaciela, odwaga, opanowanie, tematy magiczne, manipulacja emocjonalna, uwodzenie
* **Społeczny**: tematy związane z grupami i ogólnie rozumiane społeczne
    * przekonywanie kogoś, kłamstwo / oszustwo, porywanie ludzi za sobą

Postać silna w jednym wymiarze może być niezwykle słaba w innym. Odpowiednie zmienianie wymiarów i stawianie konfliktów potrafi dramatycznie zwiększyć szansę postaci na sesji.

Przykład:

```
Ania (Gracz) chce zniszczyć Bartka (NPC) który złamał serce kilku jej przyjaciółek.
Bartek (NPC) jest silnym i przystojnym kolesiem z siłowni
    wykorzystuje kobiety, traktuje je przedmiotowo i bawi się ich uczuciami z zemsty, bo
    jego przyjaciel się zabił po tym jak oskarżenie o molestowanie zniszczyło mu przyszłość
```

* Ania wyciąga sekrety i podłe teksty Bartka o kobietach, by go maksymalnie upokorzyć (nie przed innymi, by ON czuł się bardzo źle i publicznie miał wybuch emocji / słabości)
    * Tr (bo Bartek jest dość delikatny emocjonalnie) +2:
        * V: Ania podbija; Bartek nie ma jak się wycofać "z twarzą"
        * V: Do Bartka wróciło to wszystko, strata przyjaciela itp. Zaczyna szlochać publicznie. Ania eskaluje.
        * X: Bartek przejmuje inicjatywę. 
* Bartek zmienia Teatr na Fizyczny. Bartek chce Anię pobić by nigdy się już nie odważyła go zaatakować w żadnym wymiarze (nadanie aspektu). Ania się _broni_. A dokładniej - ucieka. Nie próbuje walczyć. Próbuje się oderwać i przejąć inicjatywę.
    * Ex (bo Bartek vs Ania to nie konflikt XD) +2 (bo Ania chce UCIEC):
        * X: Ania jest uderzona, krwawi z nosa i ma rozciętą wargę. Bartek podbija - chce nadać trwały aspekt.
        * V: Ania zyskuje inicjatywę
* Ania zmienia Teatr na Społeczny. Chce apelować do otoczenia by ktoś jej pomógł.
    * Tr (bo Bartek wygląda groźnie a Ania nie ma wielu przyjaciół) Z (bo już ją uderzył) +3:
        * V: (podbicie), dwóch chłopaków rzuca się na Bartka by go odciągnąć od Ani
            * Teatr -> fizyczny, ale Ania używa parametrów chłopaków, więc kontynuujemy pulę.
        * V: (duże konsekwencje) +Aspekt: Bartek nie ma jak dostać się do Ani.
* Ania (mając inicjatywę) zmienia konflikt na Społeczny; chce zniszczyć jego reputację jako damskiego boksera
    * Tp (bo faktycznie Anię uderzył i ma historię podłych tekstów XD) +3:
        * V: (podbicie) Ania pokazuje jaki jest, opowiada jakich to rzeczy nie robił
        * V: (duże konsekwencje) Bartek dostaje łatkę damskiego boksera. Ania podbija -> chce wyjść na "tą odważną"
        * V: Ania wyszła na tą odważną, która się odważyła postawić damskiemu bokserowi i psycholowi.

---
### 6.6. To Gracz decyduje, co poświęca

**_Zasady_**:

1. To, co MG proponuje na (X) to tylko sugestie MG. Gracz może to zmienić poświęcając coś "równej wartości".
    1. Wtedy Gracz ma obowiązek zaproponować coś "równej wartości".

**_Wyjaśnienie_**:

Przykład:

```
Radek (Gracz), w serwopancerzu klasy Lancer, zgubił się w bazie gdzie są potwory.
Amalgamoid Trianai, zmiennokształtny inteligentny glut poluje na Radka.
Radek ucieka, Amalgamoid próbuje go pożreć. Amalgamoid jest lepszy w każdym calu.
Radek liczy na swoich sojuszników.
```

Radek próbuje oderwać się i uciec od Amalgamoida, by tylko wrócić do reszty zespołu. Pula Radka: `Ex+2`, czyli `11X 3V 2Vr`

* X: MG: "Amalgamoid Cię dogonił" (podbicie) (pula: `10X 3V 2Vr`)
* X: MG: "Amalgamoid Cię przyszpilił, nie jesteś w stanie sam uciec" (duży X) (pula: `9X 3V 2Vr`)
    * Radek: "nie, nie mogę sobie na to pozwolić, to będzie wyrok śmierci. Strzelam do niego ze wszystkiego co mam."
    * MG: "**to nie jest równoważna konsekwencja**"
    * Radek: "strzelam do niego aż mi się kończy amunicja. Nie mam czym strzelać."
    * MG: "**dobrze, akceptuję**. Twój Lancer nie ma amunicji."
* X: MG: "Amalgamoid Cię przyszpili..." (pula: `8X 3V 2Vr`)
    * Radek: "Dobra, tym razem akceptuję. Sam nie mam jak uciec, ktoś musi mnie uratować."
* X: MG: "Amalgamoid niszczy Twój serwopancerz i zaczyna palić Twoje ciało" (pula: `7X 3V 2Vr`)
    * Radek: "Bez Lancera jestem martwy. Dobrze - niech będzie, że się oddaliłem samowolnie od zespołu. Dostanę opiernicz. Przez to że mnie ratują, inni członkowie Zespołu będą pouszkadzani."
    * MG: "Akceptuję koszt społeczny."
    * (przypominam, że zwykle MG **nie chce** zabijać postaci Graczy; kiepski efekt fabularny)
* X: MG: "Twój Lancer jest solidnie uszkodzony (trwały aspekt do momentu naprawy) ORAZ mamy gwarancję, że Amalgamoid się bezpiecznie wycofa. Przez to że muszą ratować Ciebie a nie usunąć problem." (pula: `6X 3V 2Vr`)
    * Radek: "Dalej idziemy w linię społeczną?"
    * MG: "Tak, to będzie ciekawszy wynik."
    * Radek: "Akceptuję koszt społeczny."
* V: Radek: "Przybywają sojusznicy, WRESZCIE!" (podbicie) (pula: `6X 2V 2Vr`)
    * MG: "Przekolorujemy `3X -> 3Xg`, `+3Vg` jako przewaga" (pula: `3X 3Xg 2V 2Vr 3Vg`)
* Xg: MG: "Amalgamoid korzysta z okazji, że nie mogą strzelać bo Ciebie trafią. Wystrzeliwuje włóczniomackę i przebija jednego Lancera na wylot i zabija pilota." (pula: `3X 2Xg 2V 2Vr 3Vg`)
    * Radek: "Za duża konsekwencja - już mam straszny cios społeczny. Zamiast tego proponuję by Amalgamoid poważnie pouszkadzał wszystkie Lancery; nasza grupa musi się wycofać."
    * MG: "Akceptuję."
* V: MG: "Wreszcie, połączony oddział pięciu Lancerów skutecznie odpędziło Amalgamoida. Wasz oddział nie nadaje się do dalszego wykonywania misji. Wyjaśnisz to dowództwu."
    * Radek: "Pięknie..."

Jak widzicie, Gracz nie musi akceptować wszystkiego co MG rzuci. Może zaproponować poświęcenie czegoś o równoważnej wartości fabularnej. To sprawia, że nawet jak generator losowy nie idzie, Gracz ma trochę więcej możliwości by osiągnąć cel - a sesje stają się ciekawsze.

---
### 6.7. Automatyczny sukces (ostateczne poświęcenie)

**_Zasady:_**

1. Jeśli poświęcisz coś znaczącego podczas konfliktu, możesz dodać `+3Vx` do puli, gdzie x:: dowolny kolor.
2. Jeśli już wyciągnięto `2X`: 
    * za każde 2 wyciągnięte (X) możesz wykonać specjalną akcję Autosukces.
        * dodatkowo poświęcasz coś fabularnie znaczącego i to daje "automatycznie wyciągnięty V"
            * nie usuwasz tego automatycznego (V) z puli
            * "coś fabularnie znaczącego" oznacza "jakby wyciągnęło się kolejny (X)".

**_Wyjaśnienie:_**

**_Wariant 1: Poświęcenie pierwszego typu (+3Vg)._**

Przykład:

```
Potwory atakują Miasto. Jest ich za dużo. Nie da się ich pokonać siłami Miasta.
Felicja (Gracz) dowodzi obroną miasta. Jest dowódcą garnizonu.
Felicja w przeszłości zapewniła sobie "uprawnienia dyktatora", bo ludzie boją się potworów.
Felicja chce zostać w przyszłości burmistrzem.
```

* Faza 1: Felicja musi przejąć inicjatywę. Potwory atakują miasto, ich cel to zniszczyć wszystko. Są różnej prędkości i wielkości. Czyli Felicja musi odeprzeć Teatr Fizyczny ataku potworów.
    * Teatr Fizyczny: Ex +2 (pula `11 X 3V 2Vr`)
    * **zdaniem Felicji to nie jest dobra pula.** 
        * Jako, że potwory atakują jako masa, **Felicja decyduje się nie bronić pierwszych dzielnic. Poświęca je potworom** by wytraciły pęd i nienawiść.
            * To będzie miało konsekwencje społeczne i zniszczenia ekonomiczne (co z uwagi na cele Felicji - bycie burmistrzem - jest istotne, acz Felicja może to obronić).
            * **Jest to analogiczne do (X), więc Felicja dostaje `+3Vg` do puli**.
    * --> nowa pula Ex P +2 (pula `11 X 3V 2Vr 3Vg`)
        * Ciągniemy żeton, `X` (zostaje: `10 X 3V 2Vr 3Vg`)
            * Felicja: "siły obronne miasta zostają poważnie zniszczone i spustoszone. Miasto nie ma od teraz jak obronić się samo."
            * MG: "to duża konsekwencja, większa niż proponuję; poprzedni krzyżyk nie był podbiciem".
            * Felicja: "siły obronne miasta nie chcą mnie jako burmistrza. Kryzys można wykorzystać do sprowadzenia swoich ludzi. Daj mi więc aspekt tymczasowy do Miasta 'większość opozycji wytraciłam w wojnie z potworami'"
            * MG: "podoba mi się"
        * Żeton -> `V`. Felicja przejmuje inicjatywę. Sukces - "potwory oddają się zniszczeniu i wytraciły pęd". Nie są usunięte, ale są chwilowo spowolnione i straciły dominującą agendę, oddając się destrukcji.
* Faza 2: Felicja decyduje się ściągnąć najgroźniejsze potwory do kamieniołomu i je tam utrzymać, by zalać teren materiałami wybuchowymi i je wysadzić.
    * Teatr Umysłowy: Tr P (+Felicja ma przynętę) +2 -> `8X 6V 3Vy 2Vr`
        * Ciągniemy żeton, `X`.
            * MG: "Chcę przejąć inicjatywę. Potwory "
            * Felicja: "Proponuję, żebyś mnie zranił; chcę zachować inicjatywę. Jest coś, co byś chciał?"
            * MG: "Oki - w kamieniołomie znajdują się squattersi, sporo niewinnych ludzi z mniejszości narodowych. Jak ich wysadzisz, wyjdziesz na rasistkę w oczach publiki."
            * Felicja: "Nadal akceptuję, wyplączę się z tego"
        * Ciągniemy żeton, `V`
            * Podbicie - potwory faktycznie dały się wpuścić do kamieniołomu.
        * Ciągniemy żeton, `V`
            * Wysadzimy główne potwory (i niewinnych ludzi)
            * Felicja podbija - chce wyniszczyć większość potworów
        * Ciągniemy żeton, `V`
            * Sukces; potwory są rozbite.
* Sytuacja: Kamieniołom jest ekonomicznie zniszczony. Miasto ma zniszczoną zewnętrzną dzielnicę. Dziennikarze wiedzą, że w kamieniołomie byli ludzie z mniejszości narodowych. Sytuacja opanowana. 
* Faza 3: Felicja organizuje konferencję prasową, bardzo ubolewa i twierdzi, że nie miała pojęcia o wielu rzeczach i to wina jej zaufanych oficerów i sojuszników.
    * MG: "**Rzucasz swoich zaufanych wiernych sojuszników na pożarcie mediom?**"
    * Felicja: "Aha. A do tego potem sfabrykujemy dowody i zaszantażujemy sojuszników, by nie przeszkadzali i by moja wersja była jedyną prawdziwą wersją."
    * MG: "**Oki, jeśli poświęcasz tych sojuszników, co jest odpowiednikiem (X), dostaniesz +3Vg do puli**...

Widzicie mechanizm? :-). I Ty możesz socjopatycznie zostać burmistrzem Miasta wykorzystując kryzys, rasizm i zdradzając sojuszników :-).

**_Wariant 2: Poświęcenie drugiego typu (kupno autosukcesu co 2(X) )._**

Popatrzcie na przykład:

```
Alan jest ekspertem od konstrukcji broni zwanej przez kolegów 'apokaliptyczną'.
Do Miasta zbliża się szalony Superczołg z czasów wojny. 
Żołnierze próbują go odeprzeć. Nie mają szans. 
Alan jest w roli konstruktorsko-artyleryjskiej. 
Jego zadaniem jest zrobić słaby punkt w pancerzu Superczołgu.
Alan ma mało czasu i jest pod ogromną presją.
```

Analiza przewag: 

* baza: Ekstremalny
* +: (+Mistrzostwo Alana, +wszystkie zasoby Alana)
* -: (-"niezniszczalny" pancerz, -straszliwa presja czasu)
    * (Gracz nie zaakceptował zmniejszenia presji przez poświęcenie części żołnierzy)

Alan próbuje znaleźć mikrouszkodzenia w pancerzu Superczołgu używając znajdujących się na miejscu żołnierzy i ich skanerów - może gdzieś da się coś zrobić.

* Pula: `Ex +2`, czyli `11X 3V 2Vr`.
    * X: (czyli nowa pula to `10X 3V 2Vr`)
        * MG: "Wszystko wskazuje na to, że pancerz jest na tyle mocny, że dopóki Superczołg ma zasilanie to nic go nie spenetruje".
        * Alan: "Czyli pancerz jest regenerowany nanomaszynami póki ma zasilanie?"
        * MG: "Tak, to dobra interpretacja".
        * Alan: "Potrzebujemy bardzo mocnego wyładowania prądem."
        * Alan: "Dobrze, podłączam się do elektrowni. Moje działo walnie potężnym wyładowaniem"
        * MG: "Możesz potrzebować magii by to osiągnąć. Jak chcesz przenieść potężne wyładowanie na taką odległość? Zwłaszcza, że to działo które budowałeś nie do końca do tego służyło?"
        * Alan: "Dobra, dodajmy magię"
* Pula zmienia się w `Ex M +2 +3Ob`, czyli `9X 3V 2Vr 3Vb Xm 3Ob` (bo to kontynuacja poprzedniej; ma wyjęty (X))
    * X: (czyli nowa pula to `8X 3V 2Vr 3Vb Xm 3Ob`)
        * MG: "Superczołg zabił kilku żołnierzy, którzy próbowali go spowolnić."
        * Alan: "Nieakceptowalne. Może tak - ani mój serwopancerz ani działo nie są dostosowane do takiej ilości energii. Zniszczenie pancerza Superczołgu zniszczy działo i uszkodzi mój serwopancerz?"
        * MG: "Akceptuję".
    * **NIE MA POCIĄGNIĘCIA**.
        * Alan: "Dobra, pula - zwłaszcza z magią - jest fatalna. Zróbmy tak: wywalę korki w Mieście. Ciągnę energię z elektrowni. Przekieruję to w upiorne wyładowanie by zniszczyć te nanomaszyny regenerujące pancerz."
        * MG: "**Czy chcesz autosukces kosztem uszkodzenia elektrowni Miasta?**"
        * Alan: "Tak."
        * MG: "Akceptuję."
        * --> liczone jako (V), czyli podbicie.
    * X: (czyli nowa pula to `7X 3V 2Vr 3Vb Xm 3Ob`)
        * Alan: "Dobrze, zróbmy tak - mój serwopancerz zostaje zniszczony, ja będę poparzony i tracę przytomność po zniszczeniu pancerza Superczołgu."
        * MG: "Za dużo energii przeciągnąłeś, zwłaszcza używając magii?"
        * Alan: "Aha.
    * X: (czyli nowa pula to `6X 3V 2Vr 3Vb Xm 3Ob`)
        * MG: "Nie masz już czego poświęcić w Wymiarze Fizycznym."
        * Alan: "Dobra - były inne sposoby by to zrobić. Nie trzeba było tak jak chciałem. Będzie sąd polowy. Uszkodziłem elektrownię, zniszczyłem działo i serwopancerz. Duże straty finansowe. Łatka ze mną zostanie."
        * MG: "Akceptuję, rana w Wymiarze Społecznym."
    * **NIE MA POCIĄGNIĘCIA**.
        * Alan: "Nie chcę ciągnąć z tej puli, mam dziś pecha. Zróbmy tak - przekierowałem energię z elektrowni przez działo prosto w Superczołg. Duże uszkodzenia infrastruktury. Ranni w Mieście. Ale Superczołg ma uszkodzony pancerz. A ja tracę przytomność. **Kolejny autosukces**"
        * MG: "Wiesz, że to jest podbite poprzednim poświęceniem? Uszkodzenia będą naprawdę duże."
        * Alan: "Nie mam lepszego pomysłu..."
        * MG: "Dobrze. Zniszczysz elektrownię i sporo linii przesyłowych. To będzie jakiś tydzień, by wszystko wróciło do normy, straty finansowe są koszmarne. Za to będzie to bardzo efektowne wyładowanie."
        * Alan: "Nie będę zbyt popularny."
        * MG: "Dobrze; dodam Ci do tego, że Superczołg się przestraszył i oddala, by się zregenerować."
        * Alan: "Jaki był sens z uszkodzenia pancerza Superczołgu jeśli wracamy do punktu wyjścia?"
        * MG: "Nie wracamy. Nie uda mu się ta regeneracja. Ale nie chcę rozwiązywać Superczołgu bez postaci Gracza - jaki sens? Plus, Superczołg nie spodziewał się czegoś takiego - skąd ma wiedzieć, czy nie walniesz ponownie?"
        * Alan: "Ok, w taki sposób. Dobra, ma sens"
        * --> liczone jako (V). Po poprzednim podbiciu, umożliwia spenetrowanie pancerza Superczołgu. Kosztem ogólnego niezadowolenia z Alana, zwłaszcza, że modele matematyczne udowadniają że dało się to zrobić lepiej i prościej. Miasto wymaga napraw i pomocy, Alana czeka sąd polowy i konieczność wybronienia się, ale przynajmniej wszyscy są przygotowani na następne pojawienie się Superczołgu i niewiele osób zginęło.

Jest to przypadek skrajny, ale demonstruje po to jest ten mechanizm. Żeby - nawet jak naprawdę generator losowy nie idzie - zawsze dało się coś zrobić.

---
### 6.8. Kolory żetonów

W największym skrócie? Kolory mają znaczenie takie jakie im nadacie. Pozwalają określić co dokładniej się stało by prawidłowo skierować działanie MG i Graczy.

Jako przykład, sesja "230411-egzorcysta-z-sanktuarium".

```
Postać gracza to Strzała - autonomiczny latający uzbrojony ścigacz (latający motor).
Zespół porwał jedynego egzorcystę z Miasta, wpakował go do Strzały i chcą uciec.
    Strzała jest uzbrojona, ale nie chce nikogo zabić (bo wszyscy są niewinni)
    Strzała nie chce zostać uszkodzona, bo... no, nie.
    Ludzie z Miasta strzelają do Strzały z nadzieją, że odzyskają egzorcystę.
Strzała dostaje rozkaz "zabierzcie nas stąd".
```

Czyli Strzała otwiera ogień zaporowy. Unika krzywdzenia lokalsów, ale nie chce dać się trafić.

* Pula złożona jako: `Tr P +3`, czyli `8X 6V 3Vy 3Vr`.
    * dla nas ma znaczenie nagle, czy STRZAŁA zostaje uszkodzona czy LOKALSI I MIASTO są uszkodzone.
    * więc przekolorowujemy krzyżyki na `Xr` (rany) i `Xb` (niszczenie miasta).
        * Np. `8X -> 4Xr 4Xb` lub `8X -> 6Xr 2Xb`.
            * dzięki czemu Strzała może "wybrać" na których konsekwencjach zależy jej bardziej lub mniej.
* Ostatecznie, Gracz ustawił pulę jako `6Xr 2Xb 6V 3Vy 3Vr`, czyli "wolę być ranna niż krzywdzić niewinnych".
    * I wyciągnięcie `Xr` -> uszkadzamy Strzałę, `Xb` -> uszkadzamy Miasto.

(dla osób zainteresowanych - Strzała dostała `2Xr`, przez co po podbiciu skończyła bardzo uszkodzona - ale miasto zostało nieuszkodzone).

Inny przykład.

Sesja "230331-an-unfortunate-ratnapping" (po angielsku)

```
Najemnicy polują na magiczne szczury i robią z nimi Dziwne Rzeczy.
Carmen, studentka akademii magicznej (Graczka) chce przechwycić wszystkie szczury i zbadać WTF.
Carmen z przyjacielem porwali ciężarówkę ze szczurami i unieszkodliwili najemnika.
I teraz Carmen próbuje rzucić czar na porwanego najemnika by ten nie pamiętał: 
    porwania
    obecności Carmen i przyjaciela
    co się stało ze szczurami i ciężarówką
```

Jak wyraźnie widać, konflikt zawiera magię. Magia jest liczona zawsze jako Przewaga niemożliwa do zredukowania (czyli jeśli jest magia, żetony magiczne zawsze zostają w puli). Czyli efekt jest taki:

* Pula `Tr M P +3 +3Ob`
    * `Tr +3` wynika z kontekstu sesji i deklaracji
    * `P`, czyli Przewaga pochodzi z zaskoczenia i kontekstu (obliczenia Przewag).
    * `M` reprezentuje magię. 
        * Dodaje `3Vb`, czyli 3 niebieskie ptaszki, reprezentujące Sukces Magiczny (Soczewkę)
        * Wymienia jeden `X` na `Xb`, reprezentujące Porażkę Magiczną (Paradoks)
        * `3Ob` czyli trzy entropiczne niebieskie reprezentują chaotyczność magii i to, że magia chodzi na emocjach.
    * --> czyli magia dodaje `3Vb 3Ob` i wymienia `1X -> 1Xb`.
        * Z uwagi na zasadę "niemożności usunięcia ostatniego żetonu danego koloru" wyciągnięcie `Xb` zwraca go do puli.
* Czyli pula wygląda tak:
    * Bez magii: `Tr P +3`
    * Z magią: `Tr P M +3 +3Ob`

Mamy też specjalną zasadę dotyczącą magii.

* (V), (X) mają "normalne konsekwencje". Ale (Vb), (Xb) mają "duże i dziwne konsekwencje".
* (Ob) to bardzo dziwny efekt powiązany ze stanem emocjonalnym i zainteresowaniem postaci; magia wyszła spod kontroli.

Więc przy puli `Tr M P +3 +3Ob` i intencji "rzucić czar na porwanego najemnika by ten nie pamiętał rzeczy" jakie mamy możliwe wyniki?

* (V): zaklęcie idzie w dobrą stronę; jako, że chcemy usunąć ważną pamięć, potrzebne nam Podbicie.
* (X): negatywna okoliczność "normalna". Coś, co zaszkodzi postaciom Graczy lub im utrudni życie. Np. najemnika już szukają, albo ma na sobie tracker czy coś.
* (Vb): Magiczny Sukces. To jest "TAK ORAZ". Nie tylko się udało, udało się w unikalny sposób dzięki specyfice działania tej postaci Gracza która jest magiem.
* (Xb): Magiczna Porażka. To jest "NIE TYLKO NIE ALE MASZ PRZECHLAPANE". Dookoła tego wyniku kształtowały się całe sesje. 
* (Ob): Magia robi _coś dziwnego_. Np. jeśli postać Gracza jest zakochana, niech będzie że najemnik zakochuje się w dziewczynie w której zakochana jest postać Gracza. Albo pojawia się jej aparycja jako iluzja. To niekoniecznie jest bardzo szkodliwe dla celów sesji, acz zwykle jest... _dziwne_ i nakłada znaczący Aspekt na sesję.

Co się stało w ramach konfliktu na tej sesji?

* Pula `Tr M P +3 +3Ob`
    * Wyciągamy żeton (Xm). Oczywiście.
        * interpretacja: "na twarzy najemnika pojawił się pusty wyraz. Najemnik padł na cztery łapki i zaczął zachowywać się jak szczur. W ciężarówce jest złapane kilkaset szczurów. **JEDEN** z nich zamienił się na dusze i umysły z naszym najemnikiem. Aha, to co robicie?"

Kolorowanie żetonów nie dotyczy tylko magii. Magia jest najbardziej _odmiennym_ wykorzystaniem kolorowania żetonów jakie mamy, bo niesie za sobą specjalne zasady, ale ten przykład pokazuje jak można zasymulować każdy konflikt używając wielokolorowych żetonów.

---
### 6.9. Akcje zależne (akcja gracza 1 wpływa na gracza 2) i akcje grupowe

Sytuacje typu "jedna postać gracza kontra problem" są proste do zamodelowania. A co w wypadku jak mamy kilka postaci graczy? Lub, co gorsza, akcje jednej osoby są ZALEŻNE od wyniku działań drugiej?

Mamy kilka wariantów: 

* **akcja + wsparcie**: jedna postać robi test, druga zapewnia Przewagę.
* **akcja wspólna**: dwie postacie jednocześnie robią tą samą akcję wspierając się krzyżowo.

Przejdźmy przez wszystkie trzy warianty.

**Akcja + wsparcie** 

(_to dla Ciebie najciekawsze, Darken, bo macie wiele postaci w jednej scenie_)
```
Viorika (Graczka) siedzi przygotowana ze snajperką by ustrzelić potwora.
Viorika nie widzi dokładnie lokalizacji przeciwnika, jest za drewnianą ścianą.
Romeo (Gracz) steruje dronami, by określić Pięknotce gdzie znajduje się przeciwnik.
```

W powyższej sytuacji, jest **jedna postać aktywna** - Viorika. Romeo stanowi dla niej **wsparcie**.

Romeo stanowi Przewagę (i umożliwienie wykonania konfliktu; Viorika nie ma jak zestrzelić potwora którego lokalizacji nie zna).

* `Tr +2`       <-- podstawa, z tego wychodzę
    * podstawowa pula, gdyby nie było tu Romeo 
    * (abstrahując od tego że podniósłbym do Ekstremalnego bo Viorika nie wie gdzie jest potwór)
* `Tr P +2`     <-- wariant pierwszy
    * pula z Romeo; Przewaga wynika z działań Romeo, dron i tego że Viorika widzi gdzie potwór jest
    * jeśli Romeo opowie jak próbuje dronami zmusić potwora do bycia w konkretnym miejscu by wystawić go Viorice, podniosę nawet z `+2` na `+3`.
* `Tr P +2 +3Or`    <-- wariant drugi
    * pula z Romeo, ale który jest zagrożony przez potwora (np. potwór go może zaatakować).
    * (Or) reprezentuje rannego Romeo; potwór zranił Romeo, zniszczył drony itp.

Czyli traktujemy sojusznika jako Przewagę i ewentualnie uzupełnimy entropicznymi symbolizujemy "straciłeś coś, bo zaryzykowałeś".

**Akcja wspólna**

```
Adam, 14 lat (gracz) i Basia, 14 lat (graczka) próbują odzyskać kota, którego porwał zły Czesław, 44 lata.
Basia dzwoni do drzwi. Rozmawia z Czesławem by sprzedać mu ciasteczka - a tak naprawdę, próbuje go spowolnić.
Adam w tym czasie próbuje przedostać się przez piwnicę by wkraść się do domu Czesława i wydobyć Mruczka.
```
To jest, jak widzicie, ta sama akcja która jest wykonywana przez dwie postacie jednocześnie. Im obu musi się udać, by Mruczek został uratowany. Od razu da się zobaczyć potencjalne podstawowe ścieżki fabularne:

* Czesław (zapamiętuje / nie zapamiętuje) Basię
* Czesław (wykrywa / nie wykrywa) Adama w swoim domu
* Mruczek (zostaje / nie zostaje) uratowany

Są dwa proste rozwiązania.

**_Pierwsze podejście: jeden konflikt per postać_**

Mamy **dwa** niezależne konflikty na dwóch różnych planszach. Skaczemy pomiędzy spotlightami. 

Czyli:

* Adam ma konflikt rzędu `Ex +2`
* Basia ma konflikt rzędu `Tr +2`
* Zaczynamy od konfliktu Basia - Czesław, Teatr Społeczny, Basia chce go zająć rozmową.
    * V: podbicie
    * V: Adam ma prostszy konflikt; Czesław jest zaangażowany w rozmowę z Basią.
* Teraz konflikt Adam - wbicie się do domu. `Ex + 2` -> `Tr +2`
    * V: Adam wkradł się do domu
    * X: Adam zrobił dziwny dźwięk. Czesław chce sprawdzić.
        * Wracamy do konfliktu Basi. Basia chce go przytrzymać (popatrzcie na to filmowo "ale proszę pana, mam sekret!")
            * Basi się udaje (V) -> Czesław nie sprawdza, wracamy do Adama
* ...itp.

**_Drugie podejście: jeden konflikt dla całości_**

Analizujemy frakcję jako pojedynczą stronę. Jak wygląda uratowanie Mruczka?

* Faza 1: Basia wyciąga Czesława
* Faza 2: Adam się zakrada do domu **oraz** znajduje kota
* Faza 3: Adam ewakuuje kota. Dyskretnie.
* A jak coś idzie nie tak: Basia to musi rozwiązać.

Więc wychodzimy z tego, że każda faza kontynuuje tą samą pulę, ale zmodyfikowaną (czasem Tr, czasem Ex, czasem dodajemy jakieś żetony itp.)

Wynik, fundamentalnie, będzie podobny do tego powyżej.

**_Przykłady podobnych sytuacji:_**

* Viorika ze snajperką próbuje zestrzelić potwora, Romeo odciąga jego uwagę i wystawia jego słaby punkt.
    * wymiar 1: potwór jest bezpieczny / jest zastrzelony
    * wymiar 2: Romeo jest bezpieczny / jest ranny
* Arianna próbuje przemową uspokoić ludzi w bitwie w barze a Klaudia próbuje w tym czasie znaleźć podżegacza w tej grupie.
    * wymiar 1: ludzie są spokojni / ludzie się biją
    * wymiar 2: wiemy kto jest podżegaczem / nie wiemy tego
* Elena ewakuuje cywili z zagrożonego terenu a Eustachy przygotowuje serię eksplozji mających wysadzić piratów-napastników i kupić czas Elenie
    * wymiar 1: cywile są bezpieczni / są ranni cywile
    * wymiar 2: piraci są bezpieczni / są rozbici i ranni

---
### 6.10. Postacie i różnorodność

To jeszcze nie jest w 100% stabilne, ale można wyprowadzić takie reguły wstępne:

* Jeśli postać ma Mistrzostwo w domenie, ma Przewagę.
* Jeśli postać ma zarówno Kompetencję i Agendę w kontekście testu, ma Przewagę.

To jest silnie powiązane z kartami postaci (które nie są stabilne), więc to dopiero ulegnie stabilizacji.

[Jako przykład karty postaci jeśli nie wiesz o czym mówię.](230601-karta-postaci-koncept.md)

---
### 6.11. Imperfections

* (grupa graczy) vs problem
    * rozwiązywany jako Przewaga + kolorowanie żetonów
    * różne strategie opisane w "akcjach zależnych".
* gracz vs gracz, czyli akcje pvp
    * rozwiązywany jako:
        * 1) opiernicz że chcą akcje PvP
        * 2) jedna frakcja bierze ptaszki, druga krzyżyki, wychodzą z Trudnego i normalnie liczą przewagi.
            * BAZA: `8V 8X`
            * Uznaniowe: przydzielasz (Vr) lub (Xr) w zależności od tego która strona ma większe szanse uznaniowe. Pamiętaj, że tu robisz RÓŻNICĘ uznaniową, więc będzie najpewniej [-2, 2] a nie [0, 4].
                * Jak masz wątpliwości, powiedz "nie mieszam się" i nie dawaj żadnych :p.
            * Przewagi licz normalnie itp.

---
#### 6.12. Warianty alternatywne

mapowanie: plansza1 / plansza2 / 1k20

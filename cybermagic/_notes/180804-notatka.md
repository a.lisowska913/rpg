---
layout: mechanika
title: "Notatka mechaniki, 180325"
---

# {{ page.title }}

## Autorzy

* Autor: Żółw
* Projektanci: Żółw, Dzióbek

## 1. Purpose

![The general gist of the discussion](Materials/180804.jpg)

1. What type of system is our role-playing system?
2. What do people want while playing in role-playing system?
3. Dungeon crawl and horrors
4. The scope of negotiations
5. Meta-rules of the reality, rules of the setting

## Our type of role-playing system

Usually, people play role-playing games due to several reasons. It can be escapism, it can be simulation or exploration. In our case, the balance have been drastically shifted towards creation and control over anything else.

* I create > I experience
* I control > I feel
* All is mutable, everything can change and everything has a stake
* All the players have more or less equal power over the creation
* Everything is earned - to have, one has to earn it via a stake

The above means our role-playing system differs from every other system out there and it might be too niche to be commercialized. A lot of what usually makes role-playing system:

* Immersion, feeling
* exploration, sense of awe

Simply do not exist in our system at all.

## Consequences - horrors and dungeon crawls

A dungeon: crawl is a very interesting thing. It is an exploration -  a game master prepares a map with everything set in stone and immutable, the players try to learn and understand the reality they live in. It is not the character trying to resolve a trap – due to descriptions made by game master, the player is present over there.

What I thought was a mistake of the mechanics (moving to the meta level) was in fact  designed in. This means that player feels as if they were out there. This is very important for immersion and being able to " feel they are in this strange world"

Our system is designed to have everything mutable. It is impossible to create a dungeon crawl simply because it is impossible to create an exploration when everyone has high control over creation. It also means the boss fights are not possible to be done traditionally – the system is not designed to create "feelings"of fighting a larger monster. It is designed to create stories.

This is the most apparent when we are talking about horror movies. The horror in the horror movies comes from the feeling of helplessness and being endangered. Our system is designed to give the most control to the players possible, therefore it eliminates the feeling of helplessness. As the stakes are set by both parties, the danger is negotiable – two key elements of horror have been eliminated.

This system generates completely different types of stories, but it does not really support "traditional" role-playing game structures at all.

## The scope of negotiations

When the player character loses the conflict, we are trying to negotiate what happened and when it happened.

We have a chain of negotiations when the conflict is lost; . Two different chains into dimensions. First chain deals with the time, the second chain deals with the locality in terms of a story.

### The scope of time

1. The loss should influence this scene.
2. The loss should influence this story.
3. The loss should influence this campaign.
4. The loss should influence something in the far future or far past.

How do we do it:

* First we try to apply the loss to the scene which takes place right now. Is it possible to apply the loss to something happening right now? 
* If it is impossible, we zoom out and try to look at this story - the role-playing session undergoing.
* If that fails, we try to apply the loss to influence this campaign. Doesn't happen often, but sometimes - in complex campaigns - it happens.
* If everything fails, we move the loss to something either in the far future or in the far past, so the loss is not lost.

### The scope of locality / story locality

1. The loss should influence something in this scene
2. The loss should influence something in this story which a player cares about.
3. The loss should make it easier for the opponent to win.
4. The loss should influence characters outside the story the player cares about.
5. The loss should influence a player character directly.
6. The loss should influence something a player cares about but what is out scope of this story.
7. The loss should influence something in the world.

Why:

We are dealing with a particular story thread in a story having a particular structure. First set of losses influence the main story thread or the secondary threads – something in this scene or a story thread player cares about or a game master cares about.

If it is impossible to influence the core story, we try to influence a player directly by influencing either the characters the player cares about all the very character that the player controls.

If everything is impossible, we simply influence something in the world. This means we kind of failed.

### Conclusion

This type of negotiation expansion is a core element of the mechanics which was not described anywhere. Until now.

## Meta-rules, reality rules

Our system has a funny flaw – a peasant can kill a dragon. With his bare fists.

Due to the fact we have infinite re-rolls, a peasant can try to kill a dragon, sacrifice everything according to the negotiation principle and finally kill a dragon. Outside the Dragon cave where they were brawling, the village has been burned, evil Empire has enslaved the world, everyone the peasant cares about is dead - but the peasant has tackled a Dragon to death.

The above looks exceptionally silly – it is possible to persuade player that this would hurt the consistency of the reality. Even if there are no rules on the peasants and the dragons, it seems obvious that the peasants shouldn't be able to brawl a Dragon and suffocate him.

This is "the rules of the reality" or "the rules of the setting". They are a plug-in into the meta-rules of this role-playing system.

The problem is, we have never created the rules of the reality. We understand deeply the world we are playing in as in, implicitly.

Should a group of 100 magi be able to kill a Leviathan on an Aether Infinite?

Obviously, this would be impossible. But this is obvious only to those of us who know this system deeply. This is one thing which has to be created and designed into the system. Fortunately, at this point we know what is lacking.

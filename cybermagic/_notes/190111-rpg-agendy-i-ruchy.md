# Shard

## Pytania

1. Jak to zrobić, by sesja sama się toczyła, by MG też miał narzędzia?
2. Agendy, Ruchy, Wyniki

## Tag

konsulting, szkolenie, abstrakt

## Kontekst

* prośba na Slacku

## Odpowiedź

### 1. Obserwowalność i zasoby

Ważne jest to, że każdy wynik konfliktu i każdy punkt Wpływu musi być OBSERWOWALNY. Innymi słowy, jasne i widoczne kryteria sukcesu i porażki.

INPUT ---TRANSFORMACJA--> OUTPUT
start --- akcja --------> obserwowalny wynik

Co widzę jako obserwowalny wynik?

* Stworzenie / Usunięcie Nazwanego Zasobu
    * Postać / Grupa, ma imię i potencjalnie Agendę / Ruchy (np. Maciek, kapłan Saitaera)
    * Przedmiot, ma nazwę i potencjalnie Agendę / Ruchy (np. szybki i pancerny motor)
    * Teren, nazwany i ma potencjalnie Agendę / Ruchy (np. Gura Arunczumalaj, tajemnicza i niedostępna)
    * Własność (np. Plaga, powoduje osłabienie i docelowo panikę)
* Pozyskanie lub usunięcie Zasobu
    * Nazwany Zasób (postać, przedmiot, vicinius...)
    * Earth (bezpieczeństwo, zasoby materialne, złoto)
    * Fire (narzędzia, energia)
    * Air (wiedza, informacja)
    * Water (ludzie, kontakty, społeczne)
    * Void (magia, anomalie, artefakty)
* Nadanie lub usunięcie Statusu
    * Taunt (musisz zwrócić na mnie teraz uwagę i się mną zająć lub COŚ SIĘ STANIE)
    * Kontrola Terenu (teren należy do nich)
    * Rana
    * Inspiracja (pracują ciężko i z dużą motywacją)
* Zmiana Opowieści
    * Nowa sytuacja (http://arsludi.lamemage.com/index.php/49/situations-not-plots/)
    * Niefortunne odkrycie/aspekt (http://arsludi.lamemage.com/index.php/41/revelations/) 
    * Konsekwencja w świecie gry (http://arsludi.lamemage.com/index.php/163/antagonism-101/)
    * Korupcja życzenia Gracza (też http://arsludi.lamemage.com/index.php/163/antagonism-101/)

### 2. Agendy i Ruchy

Gracze są tymi, którzy prowadzą akcję. Ale to znaczy, że muszą mieć opozycję. Często opozycją jest nieszczęsny MG. Każda sesja może niestety wyglądać bardzo podobnie. 

Do akcji wchodzi Role-Object Model - każdy obiekt może pełnić kilka ról.

Weźmy Kornela Garna. Ma role "Guru Sekty", "Krwawy Rycerz" i "Badacz Czarnej Technologii". Te role to określone AGENDY - co chce zrobić.

Jako "Krwawy Rycerz"  ma ruchy:

* "zastrasz i skłoń do pracy koszmarną egzekucją" -> +
* "zaatakuj przeciwnika i go zdemoralizuj" -> + chaos(przeciwnik), + demoralizacja(przeciwnik)

Np. Jako "Guru Sekty" ma ruchy:

* "nawróć kogoś by stał się kultystą" -> + kultysta z Nazwanej Postaci
* "poświęć kultystę by uzyskać rzadki zasób" -> - kultysta, + Nazwany Zasób
* "sformuj gang z kultystów" -> + Gang

Istnieje też coś takiego jak "Gang":

* "sterroryzuj teren"
* "pobierz haracz i uzyskaj zasoby"
* "skup uwagę wszystkich na sobie"

Co z tego: jeśli na sesji mamy Kornela, mając 9 punktów Wpływu mogę zrobić tak:

1. Kornel: Sformuj Gang z kultystów (-3 Wpływ, + nazwany zasób 'Gang')
2. Gang:  Skupia uwagę wszystkich na sobie (-3 Wpływ)
3. Kornel: Atakuje przeciwnika gdy ten skupia się na gangu (-3 Wpływ)

### 3. Po co to

By MG mógł rozrysować sesję przedtem i nie musiał się martwić "co ma się stać". Jeśli podejście od struktur sesji nie zadziała, niech zadziała podejście wynikające z teorii zasobów i obserwowalnych celów. Lepsze do kampanii niż jednostrzałów (bo sporo roboty), ale czemu nie?

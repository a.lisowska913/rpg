---
layout: mechanika
title: "Notatka mechaniki po spotkaniu 170520"
---

# {{ page.title }}

# Spis treści

1. Dokładniejsze opisanie ról
    1. Proaktywny gracz, reaktywny MG. Postać: nieważne
    1. Dokładnie opisać system intencyjny
    1. PROPOZYCJA każdy gracz (nie postać?) ma własny cel. Nagroda?
    1. PROPOZYCJA rodział MG na społecznego i fabularnego?
2. Opisanie narzędzi MG
    1. Pytania generatywne                                              DONE
    1. Frakcje (Dążenie + Scena zwycięstwa)
    1. Zasada lenistwa
    1. Jak złożyć konflikt
    1. Tory, meta-tory, wątki
3. Zmiany na karcie postaci
    1. Impulsy -> Dążenia, Zachowania + silne opisanie
4. Dla mechaniki uproszczonej:
    1. Zasoby: tapowanie (1/dzień misji)
    1. Przekalibrowanie konfliktów matematycznie
5. Dla mechaniki pełnej:
    1. Krystalizacja Efektów Skażenia:                                  DONE
    1. System magiczny -> zamknięte opisane ścieżki
    1. Przeliczenie Wpływu                                              ABORTED
6. Wprowadzenie torów, meta-torów, wątków
    1. Narzędzie zarządzania czasem
    1. Narzędzie zarządzania żyjącym światem
    1. Zmiana granulacji konfliktu

# 1. Dokładniejsze opisanie ról:

### 1.1. Funkcje graczy, funkcje MG:

**Propozycja:**

* Wyjaśnić, że Gracz pełni rolę proaktywną, MG pełni rolę reaktywną. 
* Wyjaśnić, że postać MOŻE być reaktywna, ale Gracz musi rozumieć, że to JEGO rola.
* DOKŁADNIE opisać system intencyjny. Serio.

**Uzasadnienie:**

Na Badaczach wyszło jednoznacznie, że inwersja jaką zrobiliśmy (MG słaby matematycznie, Gracze silni matematycznie) sprawiła, że MG nie do końca jest w stanie pchać fabułę. Może jedynie oporować i stawiać wyzwania, ale Gracze i tak je pokonają. 

Sprawia to, że bardzo szybko MG się kończą pomysły a Gracze - których jest więcej - mogą przekrzykiwać się w akcjach jedynie jeszcze bardziej przytłaczając biednego MG. Siła konfliktów dodatkowo powoduje, że Gracze są w stanie mechanicznie sami wykluczyć się z rozgrywki. 

To WSZYSTKO sprawia, że MG - mający bardzo dużo na głowie - nie może pchać fabuły do przodu. Po prostu nie ma na to głowy i opcji. Implikacja: Gracze a **nie** MG są odpowiedzialni za pchanie fabuły do przodu. Nie ma "Mistrzu, baw mnie". To Gracze mają mieć proaktywne wątki. Postacie mogą być wycofane, ale - na poziomie intencji - Gracze mają być proaktywni a MG ma rolę procesora.

Dodatkowo, bardzo, bardzo dokładnie, z przykładami itp. trzeba wyjaśnić system intencyjny i akcyjny. Działa to bardzo dobrze i jest pozytywnie odbierane, ale bez wejścia na poziom intencji cała mechanika i sens tego systemu się rozpada. To wyjaśnienie to MVP tego systemu.

**Propozycja do dalszej analizy:**

* ROZWAŻENIE: Każdy gracz powinien mieć własny cel. WŁASNY. Wynagrodzenie w torach?
* ROZWAŻENIE: Koordynator społeczny, nie tylko klasyczny MG?

**Uzasadnienie:**

Pewna uwaga pojawiła się _dwa_ razy. 

Pierwszy raz (30 kwiecień 2017), gdy Til zauważył, że gracze nie mają żadnych 'stawek' konfliktów - mogą być pasywni. Wtedy pojawił się koncept, by gracze 'obstawiali' konkretne rezultaty misji i za to jakoś byli wynagradzani - potencjalny zysk lub strata powoduje, że gracze są zmotywowani do aktywnej gry. Til jest bardzo doświadczonym MG (15+ lat), grającym w systemie imersyjnym, więc... ciekawe. 

Drugi raz (20 maj 2017) ta sama uwaga pojawiła się od jednego z graczy na Badaczach - warto, by gracze lub postacie miały własny cel indywidualny, bo to ustawia je w odpowiednim konflikcie i sprawia, że "mają co robić". Na bazie tego co mi powiedziano, gracz ten nie był szczególnie doświadczony w grach RPG i wykazywał cechy skierowane na G według modelu GNSI-Sc.

Innymi słowy, wszystko wskazuje na to, że faktycznie istnieje luka w systemie którą można wypełnić. Podobny problem pojawił się w AI War: gracze wiedzieli jak zacząć, wiedzieli jak skończyć, ale nie wiedzieli co robić w 'middle game'.

Potencjalne rozwiązania:

* Różni gracze dostają swoje 'sceny zwycięstwa' do których próbują dążyć; sukces zdecydowanie wzmacnia odpowiedni meta-tor.
* Różni gracze deklarują, na czym im zależy na tej konkretnej sesji i mogą dążyć do swoich celów.
* ...?

Też pojawia się problem z nagradzaniem graczy za dążenie do celu. Pytania/odpowiedzi związane z czymś owych graczy interesującym ("Manewr Żółwia")? Własna scena? Wzmocnienie meta-toru lub obniżenie innego meta-toru? Do rozważenia.

Z drugiej, zupełnie innej linii (też zgłoszonej na Badaczach): MG ma za dużo na głowie jako procesor fabularny, procesor mechaniczny ORAZ moderator w procesie grupowym. Te trzy role można rozdzielić:

* MG jako jedynie generator fabularny i "gracz światem"
* Osoba po lewej jako procesor mechaniki i osoba deklarująca "tu ma być konflikt"
* Osoba po prawej jako koordynator społeczny i osoba pilnująca kontraktu socjalnego oraz pilnująca atmosfery w grupie.

Wydzielenie Koordynatora Społecznego ma dużo zalet; to na MG wszyscy krzyczą, więc taka osoba będąca jednym z graczy ma silniejszą moc społeczną by zatrzymać rzeczy niż nieszczęsny MG. Ale wada taka, że z każdą rozdaną rolą spada imersja takiego gracza, który dostał jedną rolę więcej.

Innymi słowy, do rozważenia. Może na pierwszych sesjach powinno się to rozdzielić a jak zespół będzie ukonstytuowany to można je scalić..?

# 2. Opisanie narzędzi MG:

**Propozycja:**

* Pytania-odpowiedzi (tzw. "Manewr Żółwia")
* Frakcja: Scena Zwycięstwa + (Dążenie)
* Zasada Lenistwa: zrzuć jak najwięcej na graczy
* Tory, Wątki, Meta-tory (osobna kategoria)
* DOKŁADNIE wyjaśnić jak złożyć konflikt i jakiego typu konflikty się pojawiają

**Uzasadnienie:**

Podczas Badaczy ku swemu ogromnemu zdziwieniu zauważyłem, że jest silny deficyt narzędzi dla MG. To powoduje, że MG nie tylko są przeciążeni - oni dodatkowo nie wiedzą co robić i w jaki sposób mogą sobie pomóc i ułatwić życie. Owszem, radzili sobie, ale było to powiązane ze zbyt wysokim kosztem osobistym i energetycznym dla MG - prosta droga do wypalenia.

To powoduje, że te narzędzia, które już dla MG działają powinny zostać DOKŁADNIE opisane. Z przykładami. Najlepiej w formie: narzędzie - przykład - karta referencyjna (checklista lub graf mówiący DOKŁADNIE co robić, kiedy tego użyć (korzyść) i co ma być wynikiem). To powinno ułatwić życie MG i obniżyć koszt kognitywny MG.

Dwa narzędzia, które na pewno działają (są sprawdzone oraz dają łatwe, wyraźne korzyści) to Frakcje (wariant na Fronty z Apocalypse World) oraz przede wszystkim Manewr Żółwia (czyli pytania-odpowiedzi generujące scenę startową / zaczepienie / kernel interesujący graczy). Dodatkowo, Zasada Lenistwa, by Gracze mogli freeformowo generować dla MG rzeczy dynamicznie (np. imiona i nazwiska NPC).

Poważnym i NIE rozwiązanym jeszcze problemem jest to, jak dokładnie ustalić poziom konfliktu. Pojawia się konieczność opisania "łatwa sesja, trudna sesja" - ile powinna misja mieć konfliktów i jakiego typu by być misją o odpowiednim stopniu trudności.

Cóż, tu będzie sporo pracy. Ale mam wrażenie, że tu jest duży zwrot z inwestycji, bo MG jest wąskim gardłem większości systemów.

**Propozycja do rozważenia:**

* Generacja postaci przez scenę startową
* Generacja Okoliczności przez konflikty

**Uzasadnienie:**

Istnieje możliwość generowania postaci przez scenę startową. Np. gracz uzupełnia 2 umiejętności i 1 impuls, po czym GRACZ definiuje scenę która "stworzyła" jego postać i stawia Scenę Zwycięstwa. MG prowadzi tą scenę a Gracz podczas gry definiuje jakimi umiejętnościami to rozwiązuje - generując sobie kartę postaci. Taka scena zawierałaby 4-5 konfliktów i konflikt finalny typu "udało się" lub "udało się, ale".

Zaleta: postać sprawdzona w boju, bez umiejętności nie dających ŻADNYCH korzyści. Wada: jeden gracz gra, reszta słucha. Przy 3 graczach i 10 minutach na taką scenę mówimy o 30 minutach z sesji w plecy, gdzie 20 minut 2 graczy się nudzi. Więc, to trzeba sprawdzić w praktyce ;-).

Też warto wziąć pod uwagę technikę budowania Okoliczności przez konflikty. A co, jeśli Okoliczności powstają w wyniku poprzednich konfliktów w ramach tej samej sceny i mogą być wykorzystane przez obie strony? Tzn. tak już robimy; to trzeba opisać, ale co jeśli FAKTYCZNIE zapisujemy to na kartkach papieru, by o tym pamiętać bardziej niż bonus +1? Do rozważenia i analizy.

# 3. Zmiany na karcie postaci:

**Propozycja**:

* Zmiana nazwy: Impuls strategiczny -> Dążenie
* Zmiana nazwy: Impuls taktyczny -> Jaki jest / Zachowanie
* BARDZO wyjaśnić impulsy i ich znaczenie

**Uzasadnienie:**

Gracze i MG podczas Badaczy mieli problem z Impulsami postaci; te Impulsy były traktowane jako taktyczne (jaki jest) a nie strategiczne (co chce osiągnąć). W związku z tym zmiana nazwy i wzmocnienie znaczenia Impulsów na karcie postaci powinno pomóc.

Nasze postacie mają więc Dążenia i Zachowania. Samo słowo "Dążenie" oznacza cel, który dla postaci powinien być zrealizowany. Dążenie można opisać Sceną Zwycięstwa albo zwyczajnie wektorem do którego postać dąży.

Sukces poznamy po tym, że GRACZE będą działać na misjach zgodnie z POSTACIAMI, nie na odwrót. Wymaga to dokładniejszego wyjaśnienia Dążeń i Zachowań oraz zwiększenia nacisku na nie podczas samej misji.

# 4. Tylko mechanika uproszczona:

**Propozycja**:

* Zasoby się TAPUJE w mechanice uproszczonej. Zasób: raz/dzień.
* Przesunąć stopnie trudności uproszczonej karty i może poszerzyć.

**Uzasadnienie:**

Podczas Badaczy jeden MG zgłosił, że wszystkie konflikty praktycznie przegrywał. Wynikało to z tego, że ORYGINALNA karta uproszczona była zaprojektowana jako: Impulsy, Umiejętności, Cechy a podczas Badaczy mieli też dostęp do Zasobów. W pełnej mechanice Zasoby kosztują surowce (innymi słowy, są ograniczone czasowo, zwykle 2/dzień). W uproszczonej nie chcemy bawić się w liczenie.

Implikacja: wykorzystać mechanizm z Magic: The Gathering i "odwrócić" kartę Zasobu po jego użyciu. Raz dziennie (w świecie gry) Zasób można użyć podczas konfliktu. Nie jest to dużo liczenia (oznaczyć użycie lub nie) a nie zakłóci to aż tak bardzo rozgrywki.

Dodatkowo, wszystko wskazuje na to, że stopnie trudności nie uwzględniły Zasobów oraz modyfikatorów z Otoczenia. Może istnieć konieczność shift-3 (przesunięcia o 3) wszystkich stopni trudności oraz dodanie wyższych kategorii. Oraz WYJAŚNIENIE stopni trudności, by było zrozumiałe, czym jest "trudny" czy "łatwy". Obserwacja: powiązanie potocznych słów z liczbami jest bardzo, bardzo ważna.

# 5. Tylko mechanika pełna:

**Propozycja**:

* System magiczny -> zamknięta ilość ścieżek, modyfikowanych przez specjalizacje (biomancja + uwodzenie): 4-5 specjalizacji

**Uzasadnienie:**

Umiejętności zwykłe jeszcze JAKOŚ się nadają do prawidłowego nazwania i działania. Ale np. technomancja vs infomancja? Kataliza? Czym jest magia imprezowa? Innymi słowy, trzeba zrobić zamkniętą liczbę szkół magicznych i opisać, co wchodzi w zakres tych szkół - lepiej lub gorzej. Spowoduje to dwie rzeczy:

* Osoby przy stole lepiej rozumieją, czego się spodziewać po konkretnych umiejętnościach magicznych postaci
* Mniej warunków brzegowych typu "ale technomancja powinna dawać mi X". Nie. Nie powinna.

Koszt tego rozwiązania - mniejsza dowolność i słabszy opis postaci przy użyciu ezoterycznych szkół magicznych. Cóż, do tego będą specjalizacje.

**Propozycja**:

* Krystalizacja Efektów Skażenia na 16+ przy magii.

**Uzasadnienie:**

Na bazie 4 sesji, na których pojawiły się Efekty Skażenia, sprawdziły się. Aktualna implementacja zawierająca następujące cechy:

* 16+ (podniesiony Wpływ) przy rzuceniu zaklęcia daje Efekt Skażenia
* Gracz może odrzucić Efekt Skażenia biorąc komplikację fabularną
* Efekt Skażenia jest czymś ciekawym; niekoniecznie szkodliwym. Ważne, by otwierał nowy wątek fabularny / utrudniał inny wątek

działa tak, jak oczekiwano. Dust twierdzi, że są zbyt częste, ale Dust był tylko na jednej sesji na której wystąpiły Efekty Skażenia i miał... interesujące rzuty. Acz sam przyznał, że Efekty nie zepsuły misji a tylko ją _zmieniły_.

Cele Efektów Skażenia które zostały potwierdzone misjami:

* Będą zmieniały fabułę
* Nie będą sprawiać, że magowie boją się czarować, ale... wiąże się z magią pewien koszt akcji LUB fabularny
* Nie będą nadmiarowe i "o nie, po cholerę mi to"

**Propozycja**:

* Wpływ: (-2, +2) -> (-3, +3)
* POTENCJALNIE: Wpływ (N/A, 6) jako kolejna kategoria TYLKO do torów (bez znaczenia fabularnego? Da się tak?)

**Uzasadnienie:**

* Wpływ staje się dużo silniejszym parametrem wraz ze zmianą granulacji konfliktu. Praktycznie nie opłaca się wybrać wpływu niższego niż 'duży' przy wprowadzeniu torów i zmianie granulacji.
* Słabsza postać jest w stanie osiągnąć mniejszy sukces, potężniejsza większy sukces - czasem duża intensywność akcji może być za trudna.
* Nagle pojawia się 'high score attack': Przy 5 konfliktach walczą 2 strony. Kto BARDZIEJ wypełni tory. Np. 2-2-3-3-3 gracz 1, gracz 2: 2-3-1-1-4, wygrywa gracz 1. "Race The Sun" problem: ile poświęcisz by wygrać? Ile rerolli? ;-).

**Followup**

Podczas podstawowych rozgrywek okazało się, że nawet podniesienie Wpływu do '2' jest kosztowne. Gdyby Wpływ podniesiony był do '3', najpewniej nigdy nie byłoby to wykorzystywane. Implikacja: Koszt podniesienia Wpływu na razie musi zostać na wysokości '2'. Wstępnie rozważane jest ustalenie kosztu następnego poziomu podniesienia Wpływu do '5'. Rozważane.

# 6. Wątki, Meta-tory, Tory:

**Propozycja**:

To jest pewna bomba; ciężkie narzędzie fabularne po stronie MG (na które wpływają gracze) z funkcją generacji historii i zarządzania historią.

* Zmiana granulacji konfliktu. Nie wirtualny tor o długości 2 a rzeczywisty o długości 5 (liczby do zmiany)
* Nie-eskalowany dzień ma tylko 3 konflikty torowe / gracza
    * Eskalacja to np. po rozbiciu bandy, Karolina Maus brutalizuje członka bandy by 'dać sygnał'. Większy wpływ na meta-tor, acz 'zakres poprzedniego konfliktu'.
    * Jeden konflikt może mieć jedynie jedną eskalację wzmacniającą wpływ na meta-tor
* Tor ma długość 3 lub 5 lub 10 (liczby do zmiany)
* Meta-tory składające się z torów:
    * Wynika to z tego, że KAŻDY konflikt pochodzi z różnicy WIZJI PRZYSZŁOŚCI osób przy stole 
    * Implikacja: każdy konflikt powinien przesuwać w kierunku na jedną z WIZJI PRZYSZŁOŚCI (wariant rzeczywistości)
    * Narzędzie to może zastąpić Warianty Przyszłości
    * Niekonfliktowanie rzeczywistości TEŻ przesuwa meta-tory, zwłaszcza Hidden Movement
* Wątki pochodzą od metatorów. 
    * Co 10 pkt. metatorów: +1 Wątek, zaokrąglenie w dół
    * Implikacja: silne metatory zmieniają wątki, zmieniają fabułę. A słabe meta-tory nie mają wpływu na przyszłość
* Komplikacja fabularna: nowy tor lub przesunięcie meta-toru (Siluria/Wiktor to był Wątek przesuwany przez meta-tor)
* WYMAGA SILNEGO TESTOWANIA; TO JEST NOWA MECHANIKA - MECHANIKA HISTORII OPARTA O PROJEKTY

**Uzasadnienie:**

To jest bardzo skomplikowany problem, bo to naprawdę jest forma nieuchwytnej mechaniki projektów w połączeniu z granulacją konfliktu.

Przede wszystkim, zarówno Kić jak i Dzióbek zauważyli problem z granulacją konfliktu - potrafimy przejść przez sesję z 3-5 konfliktami (i 4-5 nie zadeklarowanymi, bo postacie są za wysoko). Nasza granulacja konfliktu jest tak wysoka, że potrafimy przekonać ważnego NPCa jedną akcją. To powoduje, że pojawia się MAŁO okoliczności do taktycznej gry; gramy raczej na poziomie strategicznym pod kątem historii. Działa, ale... jest niedosyt. Mało komplikacji fabularnych, mniej chaotyczności niż bym chciał.

Jednocześnie pojawiają się skomplikowane misje takie jak Wiktor/Siluria, gdzie jedna strona stopniowo uzależnia od siebie drugą, przechodząc od nieufności aż do uwielbienia na przestrzeni ~2 miesięcy świata gry (~10 misji). W tej chwili nie ma na to konkretnej mechaniki; jest to bardzo silnie uznaniowe. WIEMY, że to jest mechanika projektów, ale przed wprowadzeniem Wpływu nie było możliwości zrobienia sensownej mechaniki projektów.

Eksperymenty z Frontami doprowadziły do zbudowania konceptu Frakcji, które reprezentują stronę aktywną opozycji w świecie. Innymi słowy, jest to jeden z 'awatarów' MG w świecie gry - Frakcje są 'stronami aktywnymi, które coś chcą' i mają swoje Sceny Zwycięstwa. Gracze, z samej swojej definicji, są stronami aktywnymi (i TEŻ mają niepisane czasem, ale mają Sceny Zwycięstwa). To prowadzi nas do definicji konfliktu. Co to jest konflikt? Dwie osoby przy stole chcą dwóch różnych wizji przyszłości. Ok, ale to znaczy, że konkurują ze sobą dwie SCENY ZWYCIĘSTWA. Czyli każdy konflikt zbliża nas do JEDNEJ Sceny Zwycięstwa.

Powyższe prowadzi do Wariantów Przyszłości - bardzo ciężkiego narzędzia MG mającego symulować to, że świat żyje naprawdę (i to narzędzie jakoś nawet działa; całkiem nieźle, powiedziałbym). Ogólnie rzecz biorąc, Warianty Przyszłości to 'Sceny Zwycięstwa' zbudowane na torach. Każdy konflikt przesuwa te tory do przodu; często pojawiają się nowe tory po nietypowych akcjach graczy (np. Ignat/Sandra Maus po Efekcie Skażenia Silurii i Pawła)...

...co prowadzi nas do bardzo poważnego problemu upływu czasu na misji. Na misji czas upływa tak, że MG mówi "no, następny dzień, nie?". A gracze na to "nie, jeszcze nie!". Brakuje mechaniki upływu czasu. Oczywistym jest, że JAKOŚ warto to powiązać z konfliktami (min. przez to gracze muszą priorytetyzować czym się zajmują), ale nie mamy jeszcze narzędzi jak to zrobić.

I powyższa propozycja ma połączyć wszystkie te problemy w jedno rozwiązanie; taką "teorię wszystkiego". Jest to całkiem eleganckie rozwiązanie, bo bierze sporo obserwacji i narzędzi cząstkowych i próbuje wszystko połączyć w jedno maksymalnie działające rozwiązanie:

* Upływ czasu jest powiązany z konfliktami. Jeden gracz może mieć tylko 3 nie-eskalowane konflikty w ciągu DNIA.
    * Promujemy działania RÓŻNYCH graczy. Nie mamy jednego gracza, który robi wszystko tego samego dnia
    * Mamy upływ czasu, łącznie z odświeżeniem surowców i przesunięciem Hidden Movement opozycji
    * Gracze muszą priorytetyzować czym się w danym momencie zajmą, bo przy dużej ilości wątków... wszystkim nie mogą.
* Nietrywialny konflikt staje się TOREM, nie AKCJĄ. To powoduje, że przeciętny konflikt będzie wymagał nie 1 testu a 2-3 testów.
    * Zwiększa się ilość prawdopodobnych komplikacji fabularnych, bo zwyczajnie matematycznie zwiększy się ilość rzutów.
    * Zwiększa się TAKTYCZNOŚĆ rozgrywki, bo mamy akcję-reakcję a nie "pokonuję tych pięciu jednym rzutem". Zwiększenie granulacji.
    * Druga Strona ma możliwość skrzywdzenia postaci graczy bądź ich celów, bo rzadko mamy 'one-hit' przy torze o długości '5'.
    * Przeciwnik ma możliwość 'attrition warfare' przeciw postaciom graczy - może ich zalać minionkami.
    * W zależności od pracochłonności możemy mieć dłuższy tor niż '5', co umożliwia bossfighty (które przy konfliktach strategicznych nie działają)
    * Tor jest powiązany z INTENCJĄ, deklaracje akcji mają osiągnąć intencję. Strategia == intencja, taktyka == akcja.
    * Nadal trywialne konflikty są w wirtualnych torach o długości '2'. Nie wszystko musi być torem.
* Każdy wygrany (lub przegrany, lub nieskonfliktowany) konflikt wypełnia meta-tor (tor fabularny) powiązany ze Sceną Zwycięstwa jakiejś strony (Frakcji, Gracza, Wariantu Przyszłości(?))
    * Powód: konflikt JEST wygraniem dla 'swojej' wizji przyszłości a meta-tor jest formą zarządzania jak daleko udało się to przesunąć / osiągnąć
    * Pozwala to na aktywne monitorowanie stanu stron i ich postępów na misji - ergo, umożliwia to Graczom na podejmowanie decyzji co z tym robić...
    * Eskalacja konfliktu umożliwia uderzenie w meta-tor lub wzmocnienie tego meta-toru. Tak naprawdę, meta-tory pełnią rolę Wariantów Przyszłości, ale są silniej powiązane z poszczególnymi stronami / Frakcjami
    * Gracze mogą spowolnić meta-tor przez uderzenie w generator meta-toru po stronie konkretnej Frakcji: motywuje do nowych ciekawych konfliktów ;-)
    * Jak Warianty Przyszłości pokazały, takie zarządzanie przez tory umożliwia piękne opisanie żywego świata PO sesji. To działa.
    * Straszna wada: bookkeeping. To trzeba jakoś rozwiązać, bo MG się pogubi. Zwłaszcza, że działania Graczy i MG czasem dodają nowe meta-tory.
* Każdy odpowiednio daleko posunięty meta-tor przesuwa Wątek do przodu po misjach
    * Dwa problemy: meta-torów na misji jest sporo (10?) i nie wszystkie powinny mieć efekty długotrwałe. Z drugiej strony, niektóre POWINNY mieć efekty długotrwałe (np. Wiktor/Siluria). W związku z tym, istnieje konieczność ustalenia które meta-tory mają elementy persystentne a które tymczasowe.
    * Niech zatem jedynie te meta-tory, które osiągnęły odpowiednią wartość (np. 10) staną się persystente. Pozostałe są 'lokalne', jedynie w zakresie pojedynczej misji. Niech narzędziem zarządzaniem persystencją będą Wątki.
    * Wątki są linią, jak w wypadku Silurii i Wiktora. Wiktor był: nieufny (-1), neutralny (0), przyjazny (1) ... uzależniony (8). Z perspektywy meta-torów: jak długo Siluria na misji osiągnęła meta-tor o odpowiedniej wartości, przesunęła Wątek o 1. To powoduje, że po misji Wiktor zmienił swój stosunek do Silurii.
    * ...a dodatkowo z limitem konfliktów na dzień, mamy realistycznie CO NAJMNIEJ 9 przesunięć. Gdyby Siluria robiła tylko ten meta-tor, zajęłoby to najpewniej 18 dni świata misji. Mówimy o pełnym skupieniu i nie robieniu NICZEGO innego. Aha, i dobrych rzutach. Czyli aspekt czasu zadziałał.
    * Powolność Wątków sprawia, że mogą pojawić się nowe komplikacje fabularne, kontr-wątki i przepychanka. No i priorytetyzowanie ;-).
* Komplikacje fabularne generują nowy meta-tor lub przesuwają meta-tor
    * Cenne narzędzie; nie mamy pomysłu na komplikację? Oki, przesuńmy meta-tor ;-).
    
Jako całokształt powyższe narzędzie powinno jeszcze bardziej promować proaktywność graczy i jakkolwiek MG będzie nieszczęśliwym księgowym, ale świat będzie "grał się sam". Zwłaszcza przy wykorzystaniu odpowiednich generatorów losowych czy oracli.

Jak napisałem, to jest bardzo nowe i potencjalnie bardzo trudne. Do przeanalizowania. Przesuwa to MG na pozycję gracza planszówki a graczy na pozycję graczy RPG, którzy nie mogą WSZYSTKIEGO wygrać, a świat przesuwa się sam... ale mogą wygrać to, na czym im najbardziej zależy.

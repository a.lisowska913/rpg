---
layout: mechanika
title: "Notatka mechaniki, 230428 - żetony na planszy"
---

# {{ page.title }}

## Autorzy

* Autor: Żółw
* Projektanci: Żółw

## 1. Purpose

Ten dokument ma pokazać jak wykorzystać mechanikę żetonów bez użycia komputera.

## 2. High-level overview

* Mamy problem ze składaniem żetonu z woreczkami - składanie puli jest zbyt wolne. 
    * Co gorsza, niektórzy ludzie po prostu nie mają żetonów czy dostępu do żetonów
    * Ale mechanika oparta na żetonach jest dużo lepsza niż dowolna inna ;-)
* Wniosek - można zamapować żetony na kości.
    1. Można przemapować wszystkie możliwe pule na kombinację 2k20 i 2k6
    2. Można zbudować predefiniowane "pule" jako plansze
        * Przyspieszenie robienia konfliktów przez plansze i kości
        * Nadal mamy dynamiczne żetony

## 3. Przykład

Jak to możemy zrobić?

Przede wszystkim, zobaczmy często pojawiający się konflikt `Tr Z +2 +3Or`:

![konflikt Tr Z +2 +3Or](Materials/230428/01-przyklad-konfliktu-aplikacja.png)

**8X, 6V, 3Vz, 2Vr, 3Or** = 22 żetony

To znaczy, że ta pula może zmieścić się na "planszy" mającej 25 pól:

![konflikt Tr Z +2 +3Or](Materials/230428/02-przyklad-konfliktu-aplikacja-plansza.png)

Teraz - co wyjmuję z puli? Rzucam 2k20. Dostaję 11 i 5. Czyli 3 kolumna i 2 rząd. Wyciągnąłem krzyżyk.

![konflikt Tr Z +2 +3Or -1X](Materials/230428/03-przyklad-konfliktu-aplikacja-plansza-minus-x.png)

Teraz w tamtej pozycji nie ma "niczego". Pojawił się tam reroll. Niech będzie więc eskalacja. Załóżmy, że dociągamy następny żeton. Losuję 15 oraz 19. Trafia na pole oznaczone jako REROLL, więc przelosowujemy jeszcze raz.

Wypadło 4 i 14. Sukces zasobowy (Vz).

![konflikt Tr Z +2 +3Or -1X](Materials/230428/04-przyklad-konfliktu-aplikacja-plansza-minus-x-vz.png)

Niech będzie, że w puli pojawiła się magia. Konflikt zmienia się z `TrZ+2+3Or` na `TpZM+2+3Or+3Ob`. Wyciągnęliśmy już Vz i X. Więc mamy nową planszę podstawową (`TpZM+2+3Or+3Ob` to 28 żetonów, więc używamy tabelki 28):

TABELKA PODSTAWOWA:

![konflikt TpZM+2+3Or+3Ob](Materials/230428/05-przyklad-konfliktu-2.png)

TA SAMA TABELKA PO USUNIĘCIU 2 ŻETONÓW (Vz, X):

![konflikt TpZM+2+3Or+3Ob](Materials/230428/06-przyklad-konfliktu-2-po-usunieciu.png)

A jeśli w wyniku działań graczy trzeba dodać jakieś żetony? (np. +3Vg?). Proszę bardzo, umieszczamy ekstra żetony w dowolnym z pól. Dodaję 3 Vg do tej planszy:

![konflikt TpZM+2+3Or+3Ob](Materials/230428/07-przyklad-konfliktu-2-po-dodaniu.png)

Jeśli trzeba poszerzyć / zawięzić planszę, nie jest to trudne. Mamy następujące wielkości od ręki dostępne jako "podstawy":

* dimension(1k2): 1k20 LUB 1k6, parzyste / nieparzyste
* dimension(1k3): 1k6 / 2
* dimension(1k4): 1k20: {1-5, 6-10, 11-15, 16-20}
* dimension(1k5): 1k20: {1-4, 5-8, 9-12, 13-16, 17-20}
* dimension(1k6): 1k6
* dimension(1k10): 1k20, gdzie 11-20 -> 1-10
* dimension(1k20): 1k20

Więc jeśli potrzebujemy np. zmienić konflikt z 20-żetonowego do 25-żetonowego, zmieniamy planszę z `4 - 5` na planszę `5 - 5` i dodajemy nową prostą wcześniej nieużywaną krawędź.

Tak samo możemy zawięzić jeśli trzeba.

Czyli kluczem problemu żetonów nie były same ŻETONY a WORECZEK do wkładania, wyjmowania i zmian puli.

## 3. Konflikty, plansze, kości

Jakie konflikty występowały często w raportach?

| Konflikt                  |   Rozkład żetonów         | Ilość żetonów (suma)      |
|---------------------------|---------------------------|---------------------------|
| Tp + 3                    |   5X 9V +3                |       17                  |
| Tp Z +4                   |   5X 9V 3Vz 4             |       21                  |
| Tp Z M +2 +3Ob            |   5X 9V 3Vz 3Vm 2 3Ob     |       25                  |
| Tr Z +2                   |   8X 6V 3Vz +2            |       19                  |
| Tr +3 +2Or                |   8X 6V +3 + 2Or          |       19                  |
| Tr (niepełny) +2          |   8X 3V +2                |       13                  |
| Tr +3 +3Or +3Og +3Op      |   8X 6V 3 9Ox             |       26                  |
| Tr Z +2                   |   8X 6V 3Vz +2            |       19                  |
| Tr Z +2 +3O               |   8X 6V 3Vz +2 +3O        |       22                  |
| Tr Z +3                   |   8X 6V 3Vz 3             |       20                  |
| Tr Z +3 +3O               |   8X 6V 3Vz 3 3O          |       23                  |
| Tr Z +4                   |   8X 6V 3Vz 4             |       21                  |
| Tr Z +3 +5Og              |   8X 6V 3Vz 3 5Og         |       25                  |
| Tr Z M +2 +3Ob            |   8X 6V 3Vz 3Vm 2 +3Ob    |       25                  |
| Tr Z M +4 +5Ob            |   8X 6V 3Vz 3Vm 4 +5Ob    |       29                  |
| Tr Z +3 +5Or              |   8X 6V 3Vz +3 +5Or       |       25                  |
| Tr Z +2 +3Vg              |   8X 6V 3Vz 2 3Vg         |       22                  |
| Tr Z +5O +3               |   8X 6V 3Vz 3 5O          |       25                  |
| Tr Z M +3 +3Ob +5Or       |   8X 6V 3Vz 3Vb 3 3Ob 5Or |       31                  |
| Ex + 2                    |   9X 3V +2                |       16                  |
| Ex Z +2                   |   11X 3V 3Vz 2            |       19                  |
| Ex Z +4                   |   11X 3V 3Vz 4            |       21                  |
| Ex Z +3 +3Vg              |   11X 3V 3Vz 3 3Vg        |       23                  |
| Ex Z +3 +2Vg              |   11X 3V 3Vz 3 2Vg        |       22                  |

Jeszcze raz - wymiary do plansz:

* dimension(1k2): 1k20 LUB 1k6, parzyste / nieparzyste
* dimension(1k3): 1k6 / 2
* dimension(1k4): 1k20: {1-5, 6-10, 11-15, 16-20}
* dimension(1k5): 1k20: {1-4, 5-8, 9-12, 13-16, 17-20}
* dimension(1k6): 1k6
* dimension(1k10): 1k20, gdzie 11-20 -> 1-10
* dimension(1k20): 1k20

Czyli potrzebujemy plansze żetonów od 10 -> 35.

* 10: linia: 1k10
* 11: plansza: 1k2 - 1k6, reroll na 2-6
* 12: plansza: 1k2 - 1k6
* 13: plansza: 1k3 - 1k5, reroll na 3-5, 3-4
* 14: plansza: 1k3 - 1k5, reroll na 3-5
* 15: plansza: 1k3 - 1k5
* 16: plansza: 1k4 - 1k4
* 17: plansza: 1k3 - 1k6, reroll na 3-6
* 18: plansza: 1k3 - 1k6
* 19: linia: 1k20, reroll na 20
* 20: linia: 1k20
* 21: plansza: 1k4 - 1k6, reroll na 4-6, 4-5, 4-4
* 22: plansza: 1k4 - 1k6, reroll na 4-6, 4-5
* 23: plansza: 1k4 - 1k6, reroll na 4-6
* 24: plansza: 1k4 - 1k6
* 25: plansza: 1k5 - 1k5
* 26: plansza: 1k5, 1k6, reroll na 5-6, 5-5, 5-4, 5-3
* 27: plansza: 1k5, 1k6, reroll na 5-6, 5-5, 5-4
* 28: plansza: 1k5, 1k6, reroll na 5-6, 5-5
* 29: plansza: 1k5, 1k6, reroll na 5-6
* 30: plansza: 1k5, 1k6
* 31: plansza: 1k6, 1k6, reroll na 6-6, 6-5, 6-4, 6-3, 6-2
* 32: plansza: 1k6, 1k6, reroll na 6-6, 6-5, 6-4, 6-3
* 33: plansza: 1k6, 1k6, reroll na 6-6, 6-5, 6-4
* 34: plansza: 1k6, 1k6, reroll na 6-6, 6-5
* 35: plansza: 1k6, 1k6, reroll na 6-6
* 36: plansza: 1k6, 1k6
* 37: plansza: 1k10, 1k4, reroll na 10-4, 10-3, 10-2
* 38: plansza: 1k10, 1k4, reroll na 10-4, 10-3
* 39: plansza: 1k10, 1k4, reroll na 10-4
* 40: plansza: 1k10, 1k4

Będę eksperymentował z używaniem "standardowych" wymiarów, ale tak, by nigdy nie było nadmiaru rerollowania (to jest nudne). Cel - stworzyć plansze "typowych" konfliktów, wydrukować i po prostu mieć "specjalne" żetony nakładane na planszę (wtedy to może być cokolwiek, acz lubię nasze żetony).
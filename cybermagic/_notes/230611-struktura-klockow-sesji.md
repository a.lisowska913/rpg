---
layout: mechanika
title: "230611 - Struktura klocków sesji"
---

# {{ page.title }}

---
## Autorzy

* Autor: Żółw
* Projektanci: Żółw, Kić

---
## 1. Purpose

Kontynuacja pracy nad klockami sesji.

---
## 2. High-level overview

Nope, nie napiszę. Ale macie spis treści:

* (3.) Problem - "Krzyżyk uniwersalny" nie ma sensu

Droga do rozwiązania i rozumowanie:

* (4.) Przykład Rozwiązania - sesja, typ: ratownicza
* (5.) Inne klocki sesji niż tylko "typ sesji"
* (6.) Klocki sesji - różnice Żółw / Kić

Implementacja rozwiązania:

* (7.) Klocki sesji - pierwsza implementacja
* (7.1.) Elephant in the room - różnice Żółw / Kić
* (7.2.) Jak widzę użycie klocków sesji?
* (7.3.) Co łączy systemy analizowane w poprzedniej notatce? - agregacja
* (7.4.) Klocki sesji - przykłady tych powyższych
    
tu jest DUŻO tekstu i przykładów

* (8.) Podsumowanie.

---
## 3. Problem - "Krzyżyk uniwersalny" nie ma sensu

Pojęcie krzyżyka uniwersalnego nie ma sensu.

Problem polega na tym, że wszystko jest kontekstowe. Porażki akceptowalne w horrorach są zupełnie innymi krzyżykami niż te akceptowane w komediach.

Typ czy gatunek sesji - (Komedia, Horror, Detektywistyczna) - te rzeczy w pewien sposób ograniczają możliwe sukcesy i porażki do danego klimatu. 

Np. wprowadzenie porażki z komedii mogą spowodować dość interesujący efekt całkowicie rozwalający nastrój horroru.

IMPLIKACJA: jakkolwiek nie istnieją porażki czy sukcesy uniwersalne, ale sam _gatunek_ powinien nieść za soba pewne ograniczenia i sugestie.

## 4. Przykład Rozwiązania - sesja, typ: ratownicza

Weźmy jako przykład sesję ratowniczą. 

**Typowe sukcesy** to:

* Udało się uratować kogoś więcej
* Dowiadujemy się coś o tym, co tu się stało
* Udało nam się pozyskać surowce które możemy dalej wykorzystać operacji ratunkowej
* Udało nam się zdobyć zaufanie przełożonych

Natomiast **typowe porażki** wyglądałyby tak:

* Ktoś kogo potencjalnie możemy uratować jest pod presją czasową wymaga natychmiastowej pomocy.
* Nasze wyposażenie zostaje uszkodzone LUB brakuje nam zasobów
* Ratownicy sami wpadają w kłopoty
* Plan który realizowaliśmy do tej pory działał - ale z uwagi na nowe okoliczności o których nie wiedzieliśmy, musimy go zmodyfikować by nikomu nie stała się Krzywda
* Coś wybucha albo miejsce w którym ratujemy ludzi jest w gorszym stanie i coś ulega uszkodzeniu.

Same sukcesy i porażki nie wystarczą - nie wiem co może wejść w ten zakres a co nie. To kieruje mnie do dodania kategorii „Commander’s Intent”. A dokładniej:

* **Pokusa** - w którą stronę MG ma kusić Graczy by brali na siebie więcej ryzyk. Pokazuje co gracze mogą dostać więcej. To propozycje "coś więcej" co wymaga testów.
    * Pomóż ludziom uratować coś cennego - więcej niż tylko życie
    * Dowiedz się co tu się stało i z czego wynika ta katastrofa, zbierz dowody
    * Zapewnij zasoby dla zespołu ratowników na przyszłość
    * Systemowo zapobież tej klasie katastrof w przyszłości
        * np. "tu jest ostry łuk na drodze i tu regularnie giną ludzie regularnie ale przez to, że nagłosiliśmy tę sprawę będą oznaczenia i łuk będzie przebudowany"
* **Dark Future** - jak wygląda stan stabilny w przyszłości jeżeli Gracze nic nie zrobią; do czego typ sesji „dąży”. Do czego MG ma dążyć.
    * Minimalna ilość osób została uratowana jeśli ktokolwiek
    * Ratownicy tracą sprzęt są poranieni i sami potrzebują pomocy
    * Ratownicy widzą bezradność swoją i innych w obliczu katastrofy tej klasy

IMPLIKACJA - kształtuje mi się taka struktura:

* Typ sesji
    * Kierunek (Commander's Intent)
        * Dark Future
        * Potencjalne Pokusy
    * Podpowiedzi
        * Typowe Sukcesy
        * Typowe Porażki

Lub wizualnie:

![Sesja ratunkowa](Materials/230611/230611-01-story-type.png)

Najpewniej zapiszę to w bardziej przykładowy / Obserwowalny sposób, ale widać wektor.

Czego może mi brakować:

1. Krótki opis o co chodzi. 
    * Np. "Gracze napotykają Statek który uległ Katastrofie i próbują uratować jak najwięcej ludzi zanim Katastrofa pochłonie Statek."
2. Przykładowa struktura sesji
    * (1) bezpieczne wejście na pokład i zrobienie przyczółku
    * (2) zlokalizowanie ludzi do uratowania
    * (3) zabezpieczenie drogi do ludzi przed Katastrofą
    * (4) uratowanie ludzi przed Katastrofą
    * (5) wycofanie się na bezpieczną odległość zanim Katastrofa "wygra"

## 5. Inne klocki sesji niż tylko "typ sesji"

Oprócz typu sesji mamy jeszcze inne klocki sesji:

* Lokalizacja
    * "rozpadający się statek kosmiczny, z delikatnym poszyciem"
* Hazardy (zagrożenia)
    * "ciągłe wyładowania elektryczne wynikające z przerwanych obwodów w ścianie"
* Frakcje i postacie
    * przykład typowy: "przemytnicy na ratowanym statku kosmicznym"
    * przykład specyficzny: "Wydzielenie nowej frakcji z nowym celem"
        * "mała grupka nie zgadza się z ratownikami i decydują się znaleźć wyjście na własną rękę"
* Statusy
    * Sojusznik jest paranoiczny i "nie ufa nikomu i robi akcję solo"
    * Na pokładzie jest głośno więc "nie usłyszałaś zagrożenia"

I tu pojawia się pytanie z czego powinien składać się taki klocek sesji. Jakie mieć pola. 

* Na pewno "typowe ruchy" czy "typowe porażki / krzyżyki". 
* Uważam, że też potrzebuje jakiejś formy Commander’s Intent 
* Teraz eksperymentalnie.
    * albo "co zapewnia" i "co ma" 
    * albo coś wzorowanego na ruchach z AW 
    * albo coś wzorowanego na Dark Future i Pokusie.

## 6. Klocki sesji - różnice Żółw / Kić

Tu pomiędzy mną i Kić Pojawia się zasadnicza różnica. 

Przykładowo, załóżmy opowieść w której Jaś i Małgosia próbują zebrać jagody z lasu i wrócić do domu. 

**Ja (Żółw) patrzę na to tak:**

* Las pełni rolę Fortecy
* Sukcesem Fortecy (implementowanej przez Las) jest Dark Future.
    * Dark Future Lasu polega na tym że Jaś i Małgosia się odbiją i nic nie osiągną, tracąc surowce i kończąc ze złamanym morale
* Środkami Lasu są środki Fortecy + dodatkowe specyficzne środki unikalne pasujące do sytuacji.
    * W tym momencie las będzie próbował przestraszyć postacie, demonstrować swoje najgroźniejsze strony, chować skarby których strzeże (w tym wypadku: jagody).


**Kić patrzy na to tak:**

* Las jest lasem. Może być wrogi lub przyjazny. To las niesie ze sobą potencjalne (V) i (X). A to MG wybiera z listy to czego potrzebuje w danym kontekście
 
**Różnice w strategiach:**

Kiedy ja próbuję złożyć opowieść to patrzę jakie **Role w poszczególnych typach klocków** są mi potrzebne i wstawiam im odpowiednią implementację

Czyli jeżeli mam do czynienia z sesją typu Jaś i Małgosia to: 

* wpierw zaproponuję jako klocek "Forteca" z implementacją "Las". 
* Wiedząc że to jest las (szerszy semantycznie niż tylko Forteca) jestem w stanie dodać tam dodatkowe ruchy, ptaszki i krzyżyki.
* Ale to Rola na sesji - być Fortecą odpychającą ludzi od skarbu - jest najważniejsza.

Kić zaczyna od konkretnej implementacji.

* Wybiera las i reszta klocków składa się i dopasowuje się do tego lasu. 
* Rola lasu może być zmieniona (Forteca -> Labirynt). 
* Co więcej, jeżeli okaże się że Rola jest ważniejsza niż to, że to jest konkretnie Las to sam las może być zmieniony w coś innego (czyli jeśli to ma być np. Spawner to Las możemy zmienić w Starą Kopalnię). 

Ale w kociej wersji trzeba zacząć od **konkretnego przykładu** (Las) a nie od **abstrakcyjnej roli** (Forteca implementowana przez Las).

Oba podejścia zadziałają. Nie zdziwi nikogo, że moje jest dla mnie prostsze, prawda?

## 7. Klocki sesji - pierwsza implementacja
### 7.1. Elephant in the room - różnice Żółw / Kić

Nieistotne. Serio. Ja powiem "Forteca", Kić powie "Las", ale fundamentalnie _struktura_ klocka pozostanie bez zmian.

### 7.2. Jak widzę użycie klocków sesji?

Na przykładzie sesji (jeszcze się nie odbyła):

* MG bierze typ gry 
    * Typ: "Sesja Ratunkowa"
        * "Serbinius słyszy SOS w kosmosie i leci na pomoc..."
* MG bierze inne klocki:
    * "Lokalizacja: przebudowany statek kosmiczny" (wygląda jak Rola mojego typu)
        * "...okazuje się, że to jednostka noktiańska przerobiona w cywilną, z niewielkimi znakami życia..."
    * "Lokalizacja: Anomalia Przestrzenna" (wygląda jak Implementacja kociego typu)
        * "...ratowany statek jest niestabilny przestrzennie i nawet tu go nie ma..."
    * "Makrohazard: zrujnowana struktura" (wygląda jak Rola mojego typu)
        * "...statek jest bardzo uszkodzony i można się zapaść; jest wiele ostrych krawędzi..."
    * "Makrohazard: Skażona struktura" (wygląda jak Rola mojego typu)
        * "...co gorsza promieniowanie magiczne i Skażenie statku niosą sporo komplikacji i ryzyk..."
    * "Frakcja: Sojusznik - antyFrakcjonista
        * "...a Martyn, główny lekarz nienawidzi noktian przez traumę wojenną i uważa, że to pułapka na Serbiniusa..."
    * "Mutator: zahibernowani do uratowania"
        * "...załoga ledwo daje znaki życia i są zahibernowani - jak ich ewakuować i przenieść przez Anomalię...?"

Mając te rzeczy powyżej **mam dość na zrobienie sesji**. Gdybym miał **tory** to już by było prosto, ale nawet bez nich każdy Hazard, Status czy Lokalizacja stanowią konkretne _przeszkody_ do przejścia. Mówią o czym będą konflikty. Mówią czym kusić i jakie można dać krzyżyki.

Teraz zróbmy te cholerne klocki.

### 7.3. Co łączy systemy analizowane w poprzedniej notatce?

![Co łączy systemy](Materials/230611/230611-02-analysis.png)

Widzimy pewien zbiór "pól" i działań:

* **High level concept**: Co to jest, jednym zdaniem, nadanie klimatu i wstępnego kierunku sterowania i wstępnych zasobów. Jak coś gdzieś jest nieokreślone i MG nie wie co robić, to do tego się odwołujemy.
* **Agenda**: co to jest sukces i w którą stronę klocek wykonuje ruchy
    * **Dark Future**: jak wygląda manifestacja pełnego sukcesu; co się stanie jeśli gracze nic nie zrobią. Endgame.
    * **Głód / Impuls**: czemu klocek istnieje i do czego przede wszystkim dąży
    * **Victim**: co odbiera; w jaki sposób potworyzuje rzeczywistość, czemu musimy to pokonać
* **Strategie / Ruchy / Zasoby**: jakie akcje może strona wykonywać, podpowiedź dla MG od czego wyjść, jakich Zasobów użyć itp. Ogólnie - "co klocek ma do dyspozycji"
    * **Zegary / Tory**: czyli "co się stanie i kiedy", tak jakby ruchy powiązane z Agendą jeśli sytuacja nie ulegnie zmianie.
* **Umocowanie**: kontekst klocka w ekosystemie, sojusznicy / przeciwnicy, co jest winne i co posiada. Osadzenie w rzeczywistości. Relacje z innymi klockami.
* **Słabości**: konkretny słaby punkt klocka, kiedy inne strony mają Przewagę

Dodajmy do tego to co wypracowaliśmy z poprzedniego kontekstu by mieć full set:

* **Potencjalne Pokusy**: jeśli klocek występuje, jak może MG go wykorzystać do skuszenia Graczy dla zacnego (V)? Jak Gracze mogą wykorzystać klocek?
* **Typowe Sukcesy / Porażki**: konkretne materializacje ruchów / agend jako przykłady (X) dla MG, by było mu łatwiej to zaplanować i ustalić.

### 7.4. Klocki sesji - przykłady tych powyższych (do przetestowania na sesji)
### 7.4.1. To jest EKSPERYMENT

Na podstawie tego co będzie potrzebne na sesji oraz powyżej wypracowanych rzeczy, spróbujmy się pobawić i złożyć klocki hipotetycznie. Zobaczymy, co się uda zbudować.

Powtarzam sesję:

* MG bierze typ gry 
    * Typ: "Sesja Ratunkowa"
        * "Serbinius słyszy SOS w kosmosie i leci na pomoc..."
* MG bierze inne klocki:
    * "Lokalizacja: przebudowany statek kosmiczny" (wygląda jak Rola mojego typu)
        * "...okazuje się, że to jednostka noktiańska przerobiona w cywilną, z niewielkimi znakami życia..."
    * "Lokalizacja: Anomalia Przestrzenna" (wygląda jak Implementacja kociego typu)
        * "...ratowany statek jest niestabilny przestrzennie i nawet tu go nie ma..."
    * "Makrohazard: zrujnowana struktura" (wygląda jak Rola mojego typu)
        * "...statek jest bardzo uszkodzony i można się zapaść; jest wiele ostrych krawędzi..."
    * "Makrohazard: Skażona struktura" (wygląda jak Rola mojego typu)
        * "...co gorsza promieniowanie magiczne i Skażenie statku niosą sporo komplikacji i ryzyk..."
    * "Frakcja: Sojusznik - antyFrakcjonista
        * "...a Martyn, główny lekarz nienawidzi noktian przez traumę wojenną i uważa, że to pułapka na Serbiniusa..."
    * "Mutator: zahibernowani do uratowania"
        * "...załoga ledwo daje znaki życia i są zahibernowani - jak ich ewakuować i przenieść przez Anomalię...?"


### 7.4.2. Typ sesji: Ratunkowa

* **Nazwa**: Typ Sesji: RATUNKOWA
* **Koncept wysokopoziomowy**
    * Film katastroficzny z perspektywy ratowników.
    * "Iria" / "Aliens 2" - mamy normalne miejsce (baza / statek kosmiczny) gdzie pojawia się Niepowstrzymana Katastrofa (potwór) i musimy uratować kogo się da.
    * "Titanic" - luksusowy statek wycieczkowy zderzył się z górą lodową. Niepowstrzymaną Katastrofą jest zatonięcie statku. Ratujemy kogo się da wśród różnorodnych frakcji i agend.
    * **Odpowiedz na pytania:**
        * czym jest Niepowstrzymana Katastrofa? Czemu jest Niepowstrzymana?
        * czym jest Normalne Miejsce gdzie doszło do Katastrofy?
        * co będzie stać na drodze Ratowników? Jaki typ Hazardów i problemów?
* **Sterowanie sesją**
    * **Mroczne serce Sesji**
        * **Głód**: "Człowiek i cywilizacja są NICZYM w obliczu Katastrofy"
            * zniszczenie wszystkiego co osiągnął człowiek przy użyciu Katastrofy.
            * pokazanie mrocznej natury człowieka, Katastrofa jest pretekstem.
        * **Ofiara**: człowieczeństwo i nadzieja
            * ludzie walczą między sobą o przetrwanie, kradzieże nad pomoc bliźnim.
        * **Rola Graczy**: nadzieja i lepsze jutro
            * mimo potworności Katastrofy, są jednostki zdolne i chętne do pomocy.
            * pomóżcie ludziom, wyciągnijcie co się da, dajcie im nadzieję na jutro.
    * **Agenda - co chcesz osiągnąć**: 
        * Pokaż nikłość i bezradność ludzi w obliczu Katastrofy. Ich pomysły są niczym w obliczu Katastrofy.
        * Pokaż jak ludzie walczą o przetrwanie i o "swoje" w obliczu Katastrofy, robiąc sobie krzywdę.
        * Zniszcz jak najwięcej ludzi i dóbr. Nic nie zostanie.
    * **Dark Future - wizja maksymalnej porażki Graczy**:
        * Wszyscy zginęli, jeden po drugim. Nie mieli żadnych szans.
        * Nikt nie dowiedział się, skąd Katastrofa się wzięła.
        * Ratownicy tracą sprzęt, są poranieni i sami potrzebują pomocy. Część zginęła.
        * Teren jest uszkodzony lub stracony.
    * **Przykładowe fazy sesji**
        * (1) bezpieczne wejście do Problematycznego Miejsca i zrobienie przyczółku
        * (2) zlokalizowanie ludzi do uratowania
        * (3) zabezpieczenie drogi do ludzi przed Katastrofą
        * (4) uratowanie ludzi przed Katastrofą
        * (5) wycofanie się na bezpieczną odległość zanim Katastrofa "wygra"
* **Pomoc dla MG**
    * **Pokusy - co Gracze mogą dostać więcej jeśli ryzykują**
        * Pomóż ludziom uratować coś cennego - więcej niż tylko życie
        * Dowiedz się co tu się stało i z czego wynika ta katastrofa, zbierz dowody
        * Zapewnij zasoby dla zespołu ratowników na przyszłość
        * Spraw, by ten typ Katastrof nie pojawił się w przyszłości
    * **Typowe eskalacje sukcesów**
        * Udało się uratować kogoś więcej
        * Gracze dowiadują się coś o tym, co tu się stało
        * Graczom udało się pozyskać surowce które można dalej wykorzystać przy operacji ratunkowej
        * Graczom udało nam się zdobyć zaufanie i pochwałę przełożonych lub prasy
        * Gracze dostają dostęp do obszaru, który przedtem był odcięty
    * **Typowe porażki**
        * Pojawia się presja czasowa - ktoś kogo możemy uratować jest zagrożony
        * Katastrofa się rozprzestrzenia i obejmuje następny obszar
        * Pojawia się dodatkowa sub-Katastrofa powiązana
        * Wyposażenie ratowników zostaje uszkodzone LUB brakuje im zasobów
        * Ratownicy sami wpadają w kłopoty
        * Nowe okoliczności - Ratownicy muszą zmienić plan bo coś jest inaczej niż wyglądało
        * Coś ważnego wybucha, coś ulega uszkodzeniu
        * Ratowani działają zgodnie ze swoją agendą, szkodząc innym Ratowanym lub Ratownikom

Komentarz Żółwia:

Jest to za długie 'as is', ale nie potrzebuję więcej komponentów. Takie narzędzie po przeczytaniu daje mi mnóstwo pomysłów na sesje, łącznie z tym jak mogę to wykorzystać. 

Jako, że mam tylko jeden _typ sesji_ na sesję, jestem nawet skłonny zaakceptować taką długość.

### 7.4.3. Lokalizacja: przebudowany statek kosmiczny

* **Nazwa**: Lokalizacja: przebudowany statek kosmiczny
* **Koncept wysokopoziomowy**
    * Statek kosmiczny, który kiedyś był typową jednostką ale został przerobiony i zaadaptowany. Ma skrytki, skróty i nietypowe własności.
    * Osobom z załogi statku i zaznajomionym daje ogromną przewagę.
    * Osobom spoza załogi statku niełatwo się połapać w tej jednostce; nie mają czego się złapać.
    * "Millennium Falcon" z Gwiezdnych Wojen. "Firefly" z "Firefly".
    * **Odpowiedz na pytania:**
        * Czym ten statek był kiedyś? W co został przebudowany? Dlaczego?
        * Czym ten statek różni się od typowych jednostek swojej klasy?
* **Agenda** 
    * **Głód**: "To nasz teren. To nie Twój teren. Nie masz tu władzy, nic nie kontrolujesz. Wyprę Cię stąd."
        * jak tylko _ci obcy_ myślą, że kontrolują sytuację - wytrąć ich z równowagi, odeprzyj.
    * odbierz poczucie kontroli nad terenem _obcym_ - nie wiadomo, co i gdzie ich czeka
    * zaskocz _obcych_ nieoczekiwanym działaniem pozornie typowych komponentów
    * wspieraj _swoich_ - są na swoim terenie, dawaj im przewagę wiedzy i nietypowości.
    * demonstruj przeszłość jednostki, z czego powstała. Pokaż jej odmienność.
* **Zasoby i Aspekty**
    * "_wszystko jest customizowane_" - nie wszystko będzie działać jak oczekujemy; różnice w procedurach i możliwościach, zarówno na plus jak i na minus
    * "_ta jednostka była kiedyś X_" - z uwagi na przeszłość jednostki niektóre pomieszczenia mają nietypową konstrukcję i niektóre urządzenia działają nieco inaczej
    * "_wszędzie mamy skrytki i skróty_" - wiele rzeczy można ukryć. Przez wiele nietypowych miejsc da się przedostać. Kto wie, może tam już coś jest?
    * "_ach, tu nie możesz deptać po liniach_" - z uwagi na patchworkowość jednostki ma ona pewne "naturalne pułapki" i specyfiki 
    * "_starzy wyjadacze znają statek jak własną kieszeń_" - kto ma wsparcie załogi, ma ogromną przewagę
* **Pokusy i Typowe Porażki** 
    * Przesuń KOGOŚ gdzieś, gdzie nie powinien się móc dostać
    * Ujawnij KOMUŚ skrytki, które mają coś dlań przydatnego
    * Wykonaj działanie, którego jednostka tej klasy nie powinna móc zrobić
    * Odetnij przydatny podsystem od kontroli KOGOŚ kto myśli że ma nad nią kontrolę
    * Sprzęt nie działa jak powinien - w końcu jest customizowany
    * Sprzęt się sam uruchamia zgodnie z dziwnymi autoprocedurami
    * Demonstracja nietypowej przeszłości jednostki staje się istotna
    * Zakłóć skanery i lokatory - w sumie nie wiadomo gdzie jesteśmy i gdzie jest _to coś_.

Komentarz Żółwia:

Ta lokalizacja jest "symetryczna" - wszystkie strony (Gracze, MG) są w stanie z nich spokojnie korzystać. Agenda jednostki jest prosta "ci są po MOJEJ stronie a wszyscy inni są atakowani", co przedstawione jest przez tak sformułowany Głód.

"Zasoby i Aspekty" pełnią rolę "Umocowania", "Particular Strengths" i ogólnie "co odróżnia ten statek od dowolnego innego statku". Czyli _unikalizacja_. Zasoby i Aspekty mówią czego _można_ użyć a "Pokusy i Typowe Porażki" pokazują przykłady konkretnych ruchów przy (V) czy (X).

Nie mamy tu Zegarów, automatycznych Ruchów itp. Ten klocek nie modyfikuje agendy sesji i nie niesie własnej agendy, jest tylko "pasywną lokalizacją". Jest to miejsce które odpycha Cię jak masz wrogą załogę lub które daje Ci ogromną przewagę jak załoga jest po Twojej stronie.

Taka lokalizacja daje mi OGROMNE możliwości wspomagania sesji:

* Postacie Graczy mają (X) na przedostanie się do miejsca gdzie są ludzie do uratowania.
    * Uruchamiam Typową Porażkę "sprzęt nie działa jak powinien" i okazuje się, że zrobili przepięcie bo za długo trzymali klawisz. Za mały maintenance. Teraz drzwi są zablokowane i trzeba się przebić lub znaleźć inne przejście.
    * Uruchamiam Aspekt "wszystko jest customizowane" - po ostatnim najeździe celników jest konieczność podania hasła (by spowolnić). Normalnie super, ale teraz to mega nie pasuje.
    * Chcę użyć Agendę "nie jesteście na swoim terenie, ostrzegam, nie wiesz czego się spodziewać".
        * Odpalam więc Aspekt "skrytki i skróty"
            * ktoś przemycał minę przeciwpiechotną i coś wybuchło. 
            * Jest dziura w ścianie, może graczom nic się nie stało, ale mam (_announce badness_). Podbicie albo cios w morale.

Ogólnie? Jestem zadowolony z rezultatu.

### 7.4.3. Mutator: Miejsce w Ruinie

Próbowałem zacząć od Anomalii Przestrzennej, ale to jest koszmarnie trudny klocek; zacznijmy od budowy _szerokości_. Więc czas na Hazard. 

Najpierw myślałem, że Zrujnowana Struktura to jest Makrohazard (hazard dotyczący całej Lokalizacji / sesji a nie jednego miejsca), ale to przecież jest "coś zmieniającego klocek bazowy". Czyli mamy "statek kosmiczny" z modyfikacją "zrujnowana struktura". To jest właśnie definicja Mutatora (w odróżnieniu od niezależnego klocka).

* **Nazwa**: Mutator: Miejsce w Ruinie
* **Koncept wysokopoziomowy**
    * Miejsce, w którym się znajdujemy jest w bardzo złym stanie. Jest strukturalnie niestabilne, potencjalnie może coś się zawalić, kable nie mają izolacji itp.
    * Trzeba uważać na każdy ruch by się nie zapaść. Samo miejsce jest jedną wielką pułapką.
    * **Odpowiedz na pytania:**
        * Co sprawiło, że struktura budynku / jednostki jest w tak złym stanie?
        * Czy są bardziej uszkodzone i mniej uszkodzone fragmenty?
* **Agenda** 
    * **Głód**: "Nieważne, czym to Miejsce było. Teraz się rozpada i Ty też oddasz Ruinie swą krew."
        * Spraw, by Miejsce nie nadawało się do zamieszkania. Zniszcz je, rozpadnij.
        * Kto tu wejdzie i próbuje się poruszać, ucierpi. Zrań ich. Niech nie mogą go opuścić. Niech zginą wraz z nim.
    * spraw, by normalne elementy miejsca stały się niebezpieczne przez zniszczenia
    * utrudnij przejście przez Ruinę, pokaż dewastację i zaniedbanie
    * krzywdź wszystkich na terenie Ruiny. Utrudnij przejście i zadaj im rany.
* **Zasoby i Aspekty**
    * "_ten teren jest niestabilny_" - pozornie teren wygląda na normalny, ale się kruszy. Nie spadnij.
    * "_cicho, bo coś się zawali_" - wszystko jest kruche i delikatne, trzeba uważać z hałasami.
    * "_te ściany da się przebić_" - drzwi zbutwiały. Ściany przerdzewiały. Bariery nie są trwałe.
    * "_wszystko zgniło..._" - pojawia się biom charakterystyczny dla gnijących miejsc. Okazja na choroby i pleśnie? Jest ślisko.
    * "_uwaga na wyładowania_" - elektryczność iskruje, grożąc ogromnymi zniszczeniami
    * "_to wygląda upiornie_" - kiedyś ludzkie dzieło, dziś wygląda upiornie i bez nadziei.
* **Pokusy i Typowe Porażki** 
    * Ostre krawędzie rozpadającego się poszycia uszkadzają KOMUŚ skafander
    * Niestabilny grunt sprawia, że KTOŚ się częściowo zapada
    * Tu powinno być przejście - a jest zawał. KTOŚ nie przejdzie.
    * W migoczących światłach awaryjnych COŚ/KTOŚ się ukrywa.
    * KTOŚ traci nadzieję, że przejdziecie przez to bezpiecznie.
    * KTOŚ odkrył uszkodzony mechanizm, który da się naprawić / wykorzystać jego części.
    * Przez tą pozornie trwałą ścianę da się przejść.
    * COŚ jest zalane wodą, gdy rury puściły.
    * Miejsce składowania odpadów wygląda jak biohazard trzeciego stopnia. A tam jest to czego szukamy.

Komentarz Żółwia:

Wyobraźmy sobie, że Ratownicy wchodzą na pokład statku kosmicznego. Wchodzą, powiedzmy, przez dawną komorę rakietową (teraz: ładownię). Co może się stać przy (X)?

* (2: zapadający grunt) fragment podłogi jest uszkodzony; Ratownik się częściowo zapadł i musi się podeprzeć rusztowaniem stabilizującym ładunek (podbicie)
* (1: uszkadza skafander) mechanizmy stabilizujące ładunek są przerdzewiałe, podłoga też nie do końca stabilna. Stare paczki ładunkowe spadają na Ratowników. (manifestacja podbicia)
* ("wszystko zgniło") przez niesprawny lifesupport, całość statku jest pokryta jakąś dziwną pleśnią. Nie dość że jest ślisko to jeszcze obleśnie. I musimy wejść w epicentrum tej pleśni. Członek załogi odmawia.

Czyli nam się nakładają warianty wszystkich trzech rzeczy do tej pory - Misja Ratunkowa, Przebudowany Statek, Miejsce w Ruinie.

Widzę to jakoś tak:

![Storychain](Materials/230611/230612-03-chain.png)

Czyli mam główną kartę sesji i dodatkowe komponenty które są ograniczone. Nie wiem jeszcze jakiej "klasy" są poszczególne klocki (mutator, lokalizacja, komplikacja, hazard...), ale będzie ich kilka.

### 7.4.4. Makrohazard: Podniesione pole magiczne

To wreszcie jest makrohazard. Czyli wszystkie działania na sesji, w obszarze sesji na pokładzie statku będą zawierać podniesienie pola magicznego. Unikamy czarowania plus będą pojawiać się anomalie, echa i efemerydy.

* **Nazwa**: Makrohazard: Podniesione pole magiczne
* **Koncept wysokopoziomowy**
    * W okolicy jest potężne pole magiczne. Magia działa inaczej; jest silniejsza, ale jednocześnie mniej stabilna. Jest naturalne promieniowanie.
    * Pojawiają się naturalne manifestacje magiczne, które zwykle nie występują.
    * **Odpowiedz na pytania:**
        * Co sprawiło, że pole magiczne jest podniesione?
        * Co tu się stało wcześniej? W jaką stronę emocjonalnie będzie nacechowane pole magiczne?
        * Jakiego typu energia magiczna tu występuje? W jaki sposób Skaża rzeczywistość?
* **Agenda** 
    * **Głód**: "Wszystko na tym terenie zostanie Skażone i przekształcone."
        * Skaź wszystko, co wchodzi na ten obcy, nienaturalny teren. Przekształć to.
    * Zdestabilizuj magię na tym obszarze. Niech każde użycie magii przemyślą dwa razy.
    * Użyj przeszłych wydarzeń i emocji do tworzenia efektów magicznych i strażników.
    * Mieszaj rzeczywistość z marzeniami i koszmarami ze snów. Magia miesza fikcję z prawdą.
    * Rób rzeczy dziwne, chaotyczne i niestabilne. Demonstruj, że to nie jest już "normalny świat".
* **Zasoby i Aspekty**
    * "_Echo z przeszłości_": w polu magicznym odbiły się emocje i strach załogi. Pole magiczne powtarza je wielokrotnie.
    * "_Destabilizacja magii_": magowie w polu magicznym mają BARDZO podniesiony Paradoks.
    * "_Efemerydy_": "duchy eteru", niestabilne, acz dotykalne echa. Półmaterialne istoty animowane przez magię.
    * "_Anomalie_": urządzenia, przedmioty i części nabywają magicznych własności, nie stworzonych przez ludzi a przez reakcje.
* **Domyślne Tory**
    * "_Adaptacja do nowoprzybyłych_": im dalej posunięty tor, tym lepiej pole magiczne odpowiada na marzenia i strach nowo przybyłych. Manifestuje się to, co postacie mają w głowach a nie tylko to co w przeszłości.
    * "_Wiecznie rosnące Skażenie_": im dalej posunięty tor, tym silniejsze są efekty magiczne, anomalie i echa. Ten tor stanowi ograniczenie czasowe. Obecność nowych ludzi -> zwiększenie sygnału emocjonalnego -> silniejsze przyciąganie magii. Im więcej Skażenia, tym gorzej z ludźmi.
* **Pokusy i Typowe Porażki** 
    * Zademonstruj wydarzenie, które się zdarzyło w przeszłości. Odtwórz scenę z przeszłości używając istniejących elementów.
    * Ktoś, kto nie żyje wraca jako duch ze swoją poprzednią Agendą. Acz nie przyswaja nowej wiedzy, żyje "w przeszłości".
    * Coś, co tu jest od dawna ma inne, magiczne własności. Stało się Anomalią.
    * Stwórz efemerydę (tymczasowy byt pseudo-materialny), która reaguje zgodnie z przeszłymi emocjami.
    * W tym MIEJSCU, niespodziewanie, jest szczególnie silne pole magiczne...
    * Element sprzętu przyniesiony przez nowoprzybyłych zaczyna Anomalizować.
    * To, czego KTOŚ się boi lub czym się stresuje? Właśnie staje się prawdą; magia odpowiada na uczucia.
    * Tymczasowe opętanie; KTOŚ zachowuje się jak ktoś kiedyś w przeszłości.
    * Z uwagi na magię dzieje się coś wyjątkowo szczęśliwego lub pechowego.
    * Przeszłe lub teraźniejsze emocje się manifestują w bardzo dziwny sposób.
    * Energia magiczna, Efemerydy lub Anomalie przesuwają się w niefortunne miejsce.

Komentarz Żółwia:

Tu po raz pierwszy musiałem skorzystać z czegoś, co się wzięło z "The Wicked Ones" lub "Misspent Youth" - domyślne tory. Potencjalnie domyślne tory pasują też do Typu Sesji, ale na pewno nie będą pasować do typowej Lokalizacji (jak ten przebudowywany statek kosmiczny).
    
Czy ten makrohazard działa? Sprawdźmy.

_Zespół przechodzi przez korytarze inżynierskie, bo nie są w stanie przedostać się zwykłą drogą bo był zawał. I podczas jakiegoś konfliktu wypada (X)._

* (_1: demonstracja wydarzenia_). Postać widzi, jak w jego kierunku zmierza ściana ognia. Postać błyskawicznie odbija w boczny korytarzyk i pełznie na czas (V), udaje jej się ominąć płomieni. Postać orientuje się, że jest gdzieś indziej niż chciała i teraz rozumie, czemu w tych korytarzach inżynierskich są ślady po ogniu.
* (_3: anomalia_). Klasyczna uszkodzona kratka wentylacyjna. Postać próbuje się przez nią przedostać, ale ona zaczyna się obracać. Chce posiekać Postać tak jak kiedyś była użyta do zabicia pirata goniącego inżyniera...
* (_7: strach lub marzenie_). Postać ma nadzieję, że przedostanie się na drugą stronę zamkniętych drzwi i faktycznie się to udało. Po chwili dostaje wiadomość w komunikatorze "Nic Ci nie jest? Przeszedłeś przez ścianę!" Faktycznie, Postać jest samotna...

Yup. Dla mnie to działa.

### 7.4.5. Komplikacja: Sojusznik jest antyFrakcjonistą.

Komplikacja jest "czymś co MOŻE odpalić ale NIE MUSI" i jest niezależnym klockiem.

* **Nazwa**: Komplikacja: Sojusznik jest antyFrakcjonistą.
* **Koncept wysokopoziomowy**
    * "To wszystko wina noktian. Oni zawsze coś spieprzą." lub "Nigdy nie ufaj wolnym TAI; dobra TAI jest Ograniczona w służbie ludzkości."
    * Nasz sojusznik jest bardzo silnie nacechowany negatywnie przeciwko frakcji, która występuje prominentnie podczas tej sesji.
    * **Odpowiedz na pytania:**
        * KTO nie cierpi JAKIEJ frakcji? Dlaczego?
        * Podczas tej sesji: czemu dla Zespołu jest potrzebny: ten Sojusznik i ta Frakcja?
* **Agenda** 
    * **Głód**: "Pokaż mrok natury Frakcji wszystkim dookoła i zaszkodź tej Frakcji"
        * Udowodnij, że Frakcja jest winna zła. Nastaw innych przeciw Frakcji. Zaszkodź Frakcji.
    * Wyszczególniaj ruchy Frakcji i pokaż czemu krzywdzą Zespół
    * Szukaj dowodów i działań Frakcji przeciwko Zespołowi
    * Antagonizuj Frakcję przeciwko Zespołowi
* **Zasoby i Aspekty**
    * "_Znajomość Frakcji_": wie jakie są zwyczaje, podejścia, mechanizmy.
    * "_Uzasadniona nienawiść_": taka nienawiść wynika z czegoś. Jest to zarówno motor jak i mechanizm pokazania, że to uzasadnione.
* **Domyślne Tory**
    * "_Irytacja Frakcji_": im dalej posunięty tor, tym bardziej konflikt Frakcja - Sojusznik jest rozogniony.
* **Typowe Porażki** 
    * Udowodnij, że ruch Frakcji faktycznie zaszkodził Zespołowi.
    * Wejdź w konflikt / dysputę z przedstawicielem Frakcji.
    * Przekonaj kogoś innego, że na Frakcję trzeba uważać.
    * Skup się na szukaniu dowodów przeciwko Frakcji a nie na zadaniu jakie miałeś wykonać.
* **Pokusy**
    * Przekonaj antyFrakcjonistę, by TEN JEDEN RAZ skupił się na robocie.
    * Użyj nienawiści antyFrakcjonisty, by zrobił robotę dokładniej i lepiej.
    * Przekonaj antyFrakcjonistę do zrobienia tego co jest ważne dla Graczy a nie do tego jakie ma rozkazy używając nienawiści.

Tu dałoby się dodać więcej (V), (X), ale nie widzę potrzeby w tej chwili. Ten klocek ma własną Agendę, dąży do własnych celów, potencjalnie sprzecznych z działaniami Graczy.

Np. _Zespół przeszukuje zniszczone pomieszczenie by znaleźć kody kontrolne. Jednak Martyn zamiast skupiać się na zadaniu, szuka w dokumentach rzeczy udowadniających że to wina noktian (X podbicie) i - co gorsza - ma te dowody zagrażając całej operacji (X manifestacja) - oni naprawdę zrobili złe rzeczy. Ale to ludzie, których trzeba przecież uratować zgodnie z misją Serbiniusa..._

## 8. Podsumowanie

Na samych takich klockach mogę złożyć sesję. Spróbuję jutro, dla Karolinusa, Eleny i Strzały. Zobaczymy co wyjdzie. A potem w środę ;-). Przy okazji, będę miał sporo klocków.

Wasza opinia? -> Fox, Darken, Kić

Czytelne? Zrozumiałe? Pomocne?


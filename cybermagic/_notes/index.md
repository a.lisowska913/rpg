---
layout: default
title: Notatki
---

# {{ page.title }}

* [Notatka: 190111, o agendach, ruchach itp](190111-rpg-agendy-i-ruchy.html)
* [Notatka: 180325, spotkanie z Dzióbkiem i wprowadzenie mechaniki Żetonów](notatka_180325.html)
* [Notatka: 170716, po misji z Małżami i Senesgradem; integracja Zachowań i Cech](notatka_170716.html)
* [Notatka: 170520, po spotkaniu Badaczy i po misji z Dzióbkiem, Dustem](notatka_170520.html)

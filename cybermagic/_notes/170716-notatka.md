---
layout: mechanika
title: "Notatka mechaniki, 170716"
---

# {{ page.title }}

# Spis treści

1. Kontekst
2. Eksperymenty
    1. Model cech Apocalypse World
    1. Model cech Panty Explosion/ Apocalypse World
    1. Transformacja pod kątem Fate Core
3. Wybrane rozwiązanie

# 1. Kontekst

The core problem of complexity of the actor

As the story with clams (170712-ucieczka-malzy) proved, the current system is way too difficult to be able to be executed properly. If you are not in the know it is impossible. What is more, what the “offices and people” set of stories (170525-new-better-senesgrad) proved, the current set of attributes is kind of useless, as we use only the social attribute anyway.

An attempt has been made to merge the Attributes and Attitudes into Stances – several experiments are present below.

# 2. Eksperymenty

Paulina Tarczyńska:

* Model 1: Apocalypse World

| cool  | +1 |
| hard  | -1 |
| hot   | 0  |
| sharp | +1 |
| weird | 0  |

* Model 2: Mixed AW and current:

| passion   | -1? |
| quickness | +1  |
| charm     | 0   |
| patience  | +1  |
| intuition | 0   |

* Model 3: Refined elemental with Panty Explosion:

| * fire? (passion, hot)                 | -1? |
| * air? (sharp, quick, grace)           | 0 |
| * water? (precise, charm, patience)    | 1 |
| * earth? (hard, cool, stability)       | 1 |
| * void? (weird, intuition)             | 0 |


All of the experiments above have failed. It was very difficult to put a group of characters into the single model without losing their core identities. And one of the main goals of the change was to streamline, „Zachowania” (what type of character is it) by merging it with the Attributes (Social Aggressive, Social Devious, Social Friendly, Knowledge, Craft, Supernatural, Nimble, Fortitude).

In the desperation, we have returned to the Fate Core mechanics but decided to change it a bit. So the current situation looks as follows:

# 3. Wybrane rozwiązanie

Paulina Tarczyńska:

* Model 4: Merged Fate Core with strengths, weaknesses
    * strengths:
        * przekonywanie argumentami
        * szybka adaptacja
        * cierpliwość i metodyczność
        * nieustępliwość, upór
    * weaknesses:
        * coś wymusić
        * skradać / ukrywać się (fizyczna)
        * siła fizyczna
        * altruizm, nie zostawi nikogo w potrzebie

        
The other idea behind model four is that every character has a set of strengths. Exactly like with the attributes, every strength needs to be balanced with your weakness. You can have between three and five strengths and accordingly between three and five weaknesses. A strength is ‘+1’, a weakness is ‘-1’, none or both present in the same time are ‘0’.

A major disadvantage of the solution is:

* There is no basic set of ‘things’ common to all the characters
* Strengths and weaknesses are very arbitrary – you can set a strength which is the universal having a weakness which is very hard to hit
* This does not solve the core problem yet; it makes it easier to get to it, though.

A major advantage of the change is:

* The attitudes of the characters were only a strength before. This balances the system more.
* The attributes were less describing then either the attributes or merged model. So, this is a step in the better direction
* It is possible to create a fixed set of strengths and weaknesses in the future. This will help the automatic generators and add checks and balances to the characters.
* There is one section less – attributes are merged with the attitudes. Therefore, the construction of the character is easier. And the synergies are easier. And so is templating of the characters (from archetypes).

# 4. Co dalej

This shall be tested on the current story and it is going to be analyzed moving forward. Mathematically, not much had changed; the loss of one (+1) should be compensated by the fact that magic got a small boost lately.


## 1. Generacja postaci

### Faceless NPC: Alan

* Name: Alan
* Wartości
    * Moje
        * Power: more influence upon reality, amass wealth and influence
        * Security: being safe or keeping others safe
        * Hedonism: pleasure above anything else
    * Anty
        * Benevolence: altruism, help others, elevate others
        * Face: prestige; having a specific reputation and upholding it
* Osobowość
    * ENCAO: +0-0-
    * Objawy
        * Praktyczny, skupiony na tym co może zrobić
        * Niecierpliwy, chce TERAZ i nie będzie czekać
        * Zadowolony z siebie, beztroski
* Potencjalna praca
    * Monter-elektronik – układy elektroniczne automatyki przemysłowej
    * Inspektor sanitarny
* Silniki
    * Moje
        * Herold Baltazara: zwiększać wpływ i moc kogoś innego, powiększać ich wpływy.
        * Tigańczyk: członek wyklętej i zapomnianej grupy, nadal skrajnie jej oddany i lojalny. Kiedyś zemszczą się na Niszczycielu mimo, że są underdogiem.
    * Anty
        * Daredevil: Takie życie jak mają jest nudne i bezsensowne. Niech się obudzą, niech mnie podziwiają. Niech widzą co można! Muszę działać na krawędzi.



### Faceless NPC: Filip

* **Name**: Filip
* Wartości
  * Moje wartości
    * Achievement: build more skills, achieve more, expand on what you can do
    * Humility: we have a place in existence, we are nothing compared to the Reality; faith
    * Tradition: what once was still works, only a fool throws away the knowledge of the past
  * Moje antywartości
    * Benevolence: altruism, help others, elevate others
    * Stimulation: feel alive, feel more, new things, experiment, explore
* Osobowość
  * Ocean
    * ENCAO:  0-+-0
  * Jak się objawia osobowość
    * Sarkastyczny i złośliwy
    * Wszędzie musi wsadzić nochal, wścibski
    * To, co ważne robi praktycznie natychmiast, nie odracza
* Potencjalna praca:
  * Operator telewizyjnych urządzeń transmisyjnych
  * Stermotorzysta żeglugi śródlądowej
* Potencjalne silniki motywacyjne:
  * Moje silniki:
    * Ćma w płomienie: gwiazdy które świecą najjaśniej gasną najszybciej; nie chcę żyć niezauważony. Mogę zginąć, ale BĘDĘ ZAPAMIĘTANY! Żyjesz raz!
    * Zdobyć najlepsze narzędzia: to co mam działa, ale z TAMTYMI narzędziami będę 10x programistą. Tym samym czasem zrobię dużo więcej.
  * Silniki WROGA lub anty-silnik:
    * Zadośćuczynienie za zdradę: w przeszłości zdradziłem osobę / grupę i zapewnię, by im było dobrze, nawet, jeśli nie chcą mnie już znać.

* **Name**: Pola
* Wartości
  * Moje wartości
    * Family: blood, clan or mafia. The group and its prosperity is the most important
    * Hedonism: pleasure above anything else
    * Achievement: build more skills, achieve more, expand on what you can do
  * Moje antywartości
    * Stimulation: feel alive, feel more, new things, experiment, explore
    * Conformity: being one of the group, homogenity of a group
* Osobowość
  * Ocean
ENCAO:  --0+-
  * Jak się objawia osobowość
    * Nie cierpi zmian
    * Nie znosi być w centrum uwagi
    * Doskonale radzi sobie pod wpływem silnego stresu
    * Ugodowy, unika konfliktów - woli przegrać
* Potencjalna praca:
  * Lekarz – specjalista gastroenterologii
  * Operator przenośników taśmowych
* Potencjalne silniki motywacyjne:
  * Moje silniki:
    * Komfortowe życie: chcę komfortowego, spokojnego i bezpiecznego życia. Nigdy nie martwić się co będzie jutro. Dla siebie lub kogoś.
    * W poszukiwaniu boga: ten świat jest zbyt płaski, MUSI być coś więcej. Być częścią czegoś WIĘKSZEGO, ważniejszego. Znaleźć swego boga.
  * Silniki WROGA lub anty-silnik:
    * Rite of passage: by dziecko stało się dorosłym, musi dokonać Wielkiego Czynu. Zdobyć czyjeś ucho, zabić potwora...


* **Name**: Maja
* Wartości
  * Moje wartości
    * Tradition: what once was still works, only a fool throws away the knowledge of the past
    * Power: more influence upon reality, amass wealth and influence
    * Stimulation: feel alive, feel more, new things, experiment, explore
  * Moje antywartości
    * Universalism: shed your mortal coil and your clan; we all are the same, expand
    * Humility: we have a place in existence, we are nothing compared to the Reality; faith
* Osobowość
  * Ocean
ENCAO:  -+0-0
  * Jak się objawia osobowość
    * Zawłaszcza osoby i przedmioty dla siebie
    * Obserwator; raczej stoi z boku i patrzy niż działa bezpośrednio
    * Tendencje do depresji i widzenia tylko ciemności
* Potencjalna praca:
  * Ekspedient w stacji obsługi pojazdów
  * Inżynier inżynierii środowiska – oczyszczanie miast i gospodarka odpadami
* Potencjalne silniki motywacyjne:
  * Moje silniki:
    * Dominacja i terror: to JA tu rządzę. Ja mam władzę. Będą się mnie bali i zrobią wszystko, czego sobie życzę.
    * Artystyczna dusza: Stworzyć piękno, pokazać je całemu światu lub wręcz przeciwnie, zachować dla godnej publiczności. Być docenionym.
  * Silniki WROGA lub anty-silnik:
    * Przyjazny Kameleon: adaptacja do sytuacji społecznych, każdemu pokazuje taką twarz jaka jest najbardziej lubiana. Chce być lubiany i kochany.

* **Name**: Kornelia
* Wartości
  * Moje wartości
    * Achievement: build more skills, achieve more, expand on what you can do
    * Universalism: shed your mortal coil and your clan; we all are the same, expand
    * Conformity: being one of the group, homogenity of a group
  * Moje antywartości
    * Hedonism: pleasure above anything else
    * Stimulation: feel alive, feel more, new things, experiment, explore
* Osobowość
  * Ocean
ENCAO:  0-00+
  * Jak się objawia osobowość
    * Spokojny, trudny do wytrącenia z równowagi
    * Oczytany, ceni wiedzę dla wiedzy
* Potencjalna praca:
  * Wulkanizator taśm przenośnikowych
  * Zmywacz naczyń
* Potencjalne silniki motywacyjne:
  * Moje silniki:
    * Tainted Love: głód, pożądanie lub miłość. Obsesja. Pragnie być z tą osobą lub ją mieć. Podziw i pragnienie. Będzie MOJA.
    * Ćma w płomienie: gwiazdy które świecą najjaśniej gasną najszybciej; nie chcę żyć niezauważony. Mogę zginąć, ale BĘDĘ ZAPAMIĘTANY! Żyjesz raz!
  * Silniki WROGA lub anty-silnik:
    * Duma i pycha: pokazać swoją wyższość nad kimś z uwagi na status / urodzenie / rasę; nie przyjąć polecenia od kogoś poniżej siebie. Być uznanym za wyższego.    

* **Name**: Antoni
* Wartości
  * Moje
    * Security: being safe or keeping others safe
    * Humility: we have a place in existence, we are nothing compared to the Reality; faith
    * Family: blood, clan or mafia. The group and its prosperity is the most important
  * Nieistotne
    * Universalism: shed your mortal coil and your clan; we all are the same, expand
    * Conformity: being one of the group, homogenity of a group
* Osobowość
  * Ocean
    * ENCAO:  0+-00
  * Jak się objawia osobowość
    * Bazuje na intuicji jako podstawie działania
    * Profesorski, mówi mądrze, ale niekoniecznie wobec targetu
* Potencjalna praca:
  * Hodowca ptaków
  * Fryzjer damski
* Potencjalne silniki motywacyjne:
  * Moje silniki:
    * Apokalipta: zniszczyć znany Świat, gromadzić wyznawców, pokazać im horror świata, kult apokalipsy; Sheaim, Ezekiel Rage, Raw Le Cruiset.
    * Gliniarz w przedszkolu: jak ryba poza wodą; ogromny szok kulturowy i brak autorytetu i brak metod opresji by autorytet wymusić. Znaleźć swoje miejsce.
  * Silniki WROGA lub anty-silnik:
    * Lawful Tyrant: w reakcji na zło, które się stało teraz wszyscy muszą być bezpieczni. Bezpieczeństwo ponad wszystko. Superman z Injustice.

* **Name**: Lila
* Wartości
  * Moje
    * Achievement: build more skills, achieve more, expand on what you can do
    * Self-direction: autonomy, ability to control your own way and direction
    * Stimulation: feel alive, feel more, new things, experiment, explore
  * Nieistotne
    * Tradition: what once was still works, only a fool throws away the knowledge of the past
    * Security: being safe or keeping others safe
* Osobowość
  * Ocean
    * ENCAO:  00+-0
  * Jak się objawia osobowość
    * Skupiony przede wszystkim na pieniądzach i korzyściach materialnych
    * Uważny, zwraca uwagę na szczegóły
* Potencjalna praca:
  * Appraisal
* Potencjalne silniki motywacyjne:
  * Moje silniki:
    * Usunąć szczury z piwnicy: pests. Usunąć pomniejsze niebezpieczeństwo, które innym mniej przeszkadza.
    * Corrupted guardian: zrobiono mi coś strasznego. Nie jestem już w pełni osobą. Ale nikogo innego nie spotka to co mnie, ochronię innych.
  * Silniki WROGA lub anty-silnik:
    * Prowokator: podkręcanie atmosfery, by inni działali ostro pod stresem. Obserwowanie ich prawdziwej natury.


## Co mamy

Alan jest agentem Syndykatu Aureliona. Ceni siłę, bezpieczeństwo i hedonizm, ale nieważne jak go patrzą i pomoc innym. Niecierpliwy i praktyczny, motywowany nagrodami i beztroski. Inspektor sanitarny i elektronik. Chce zwiększyć wpływ Syndykatu. Pragnie znaleźć derelict ship i sprzedać znajdujące się tam Obroże Neurokontroli Blakvelowcom. CEL: sprzedać obroże.

Dla Poli najważniejsze są jej osiągnięcia, niezależność biznesowa oraz czysta przyjemność. Niewrażliwa na stres i praktycznie nie motywowana niczym, woli się zgodzić z drugą stroną niż się pieprzyć. Szuka mistycyzmu i rzeczy nadnaturalnych, pragnie zostawić na boku ten bezwartościowy świat. Ewentualnie - komfortowego życia. Badaczka szukająca wśród Strachów iskry bogów. CEL: interakcja i integracja z absolutem.

Maja nienawidzi osób fałszywych. Tradycyjne podejście i akumulacja mocy sprawiają, że Maja osiągnęła niemały poziom znaczenia. Wszystko pragnie zawłaszczyć i rządzić dominacją i terrorem - by wreszcie wszyscy żyli w prawdzie. Jednocześnie znajduje piękno w gospodarce roślin i czystości ekosystemu. Robi za lekarza i kontrolę biologiczną Węża. CEL: przejęcie władzy

Kornelia pragnie się dopasować do grupy i z radością buduje wspólną przyszłość nieważne czy dla ludzi czy AI. Przyjmuje każdego, nieważne z jakiej frakcji. Nie dba o swoje zadowolenie czy emocje. "Dobra mama" grupy. Zajmuje się załadunkiem i sprzedażą dóbr. CEL: pragnie Wojciecha (jakiegoś Blakvelowca) jako swojego soulmate.

Filip, jest skupiony na dokonaniach, pokorze i tradycji. Nie dba o stymulację czy pomaganie innym. Złośliwe i wścibskie bydlę, ale niesamowicie pracowity. Kiedyś już zdradził; nie powtórzy się to ponownie - poprzysiągł dbać i chronić interesy. CEL: zdobyć najlepsze możliwe narzędzia dla Wielkiego Węża.

Antoni zna swoje miejsce w rzeczywistości. Skupia się na zabezpieczeniu Wielkiego Węża i na tym by nikomu na pokładzie nic się nie stało. Uratowany z niewoli piratów, pochodzi z zupełnie innego miejsca. Jest jak ryba w wodzie, ale jest świetnym inżynierem i konstruktorem dron, zwłaszcza badawczych. Nienawidzi piratów w każdej formie. CEL: wrócić do domu.

Lila nie dba o tradycję ani bezpieczeństwo - ale skupia się na sukcesach, autonomii i stymulacji. Zostaw ją w spokoju i sumienna (choć nieprzyjemna w obyciu) Lila zapewni by wszystko zadziałało. Appraiser, neurosprzężona wbrew swojej woli i ucieka w używki by nie czuć wiecznego szeptania uszkodzonego AI. Główny pilot i serce maszyny. Wyjątkowo drażnią ją małe głupoty i "szczury w piwnicy" ;-). CEL: usuwać przeszkody, zapewnić, by coś takiego nigdy nikogo innego nie spotkało.

Berdysz jest straszliwym kapitanem piratów. Dołączył do załogi jako advancer. Jest gaulronem, dewastatorem i świetnym planerem. Ceni sobie władzę i przyjemność; nie ma nic przeciw by się podzielić ;-). CEL: skorzystać z okazji, wrócić do bycia kapitanem piratów. Kiedyś.

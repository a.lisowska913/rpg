# Drives of characters

; structure: * Drive: Comment

* Hulaszcza swawola: zorganizować / uczestniczyć w wielkiej imprezie z ogromną ilością sprzątania i problemów za cenę problemów dla siebie i innych
* Duma i pycha: pokazać swoją wyższość nad kimś z uwagi na status / urodzenie / rasę; nie przyjąć polecenia od kogoś poniżej siebie. Być uznanym za wyższego.
* Queen Bee: pokazać swoją reputację i wyższość nad innymi swojej grupce osób dookoła. Np. przyjezdna mg na konwentach z adoratorkami vs osoba na środku;-)
* Soccer Mom: zapewnić bezpieczeństwo, harmonię i pozory idealności w swojej okolicy. A przynajmniej wobec swojej domeny.
* Pozycja w Grupie: zbudować swoją pozycję i autorytet w swojej rodzinie / klanie. Podnieść SWOJĄ pozycję w SWOJEJ grupie.
* Poznać czyjś sekret: Grupa X coś wie. Ukrywają to. Muszę to poznać. Muszę poznać ich sekrety, nawet, jeśli są pozornie nieważne.
* Tainted Love: głód, pożądanie lub miłość. Obsesja. Pragnie być z tą osobą lub ją mieć. Podziw i pragnienie. Będzie MOJA.
* Carmen Sandiego: oddać skarby / relikwie tam, skąd przybyły. Naprawić szkody i krzywdy spowodowane przez grabież.
* Ostateczne mistrzostwo: pokonać godnego przeciwnika będąc na niedowadze; zdeklasować swoich oponentów, osiągnąć status i skillset mistrza.
* Odnaleźć dziecko: znaleźć i uratować zaginioną osobę, która jest poza zasięgiem.
* Zadośćuczynienie za zdradę: w przeszłości zdradziłem osobę / grupę i zapewnię, by im było dobrze, nawet, jeśli nie chcą mnie już znać.
* Ratunek przed chorą miłością: mój kuzyn się zakochał w osobie ze straszną przeszłością i osobowością. Ślub / partnerstwo nie może dojść do skutku!
* Spotkanie z herosem: ona jest moją idolką. Muszę ją spotkać, zdobyć autograf... może nawet zamienić kilka słów? Niech mnie dostrzeże!
* Dawna Przysięga: dotrzymać słowa danemu kogoś, że coś zrobię lub zapewnię, że będzie zrobione. Stara przysięga / umowa jest najważniejsza.
* Wygrać w rywalizacji: on jest moim rywalem. Muszę być lepszy, pokonać go! Tym razem zwyciężę, czysto lub nie ;-).
* Ukryć sekret: może komuś zapłaciłem, może coś zrobił mój ród. Ale 'sleeping dogs must lie unearthed'.
* Zemsta Andrei Beaumont: coś się wydarzyło dawno temu. Pozostała tylko zemsta na kimś możnym, kogo nie mam jak dorwać. Nieważne jakim kosztem.
* Wrócić do ukochanej: byłem głupi i ją straciłem. Zrobię wszystko, by móc do niej wrócić.
* Uniesienia: szuka długotrwałej miłości albo partnerki na noc. Szuka głębokiego połączenia dusz lub świetnego, anonimowego wieczoru.
* Drama: Kapuleti i Monteki nigdy nie będą żyć w pokoju. Moja grupa przyjaciół też nie ;-). Niech COŚ SIĘ DZIEJE! Paliwo do grupy i plotek.
* Pokój: Kapuleti i Monteki MUSZĄ żyć w pokoju. Unikajmy starć, idźmy w pacyfizm. Wygaszanie konfliktów, harmonia.
* Szok i oburzenie: kontrkultura. Wzbudzać obrzydzenie i szok wśród drobnomieszczaństwa. Skonfrontować ich z czymś obcym i nowym.
* Pogarda wobec X: pokazać komuś swoją głęboką pogardę do X (osoby/grupy), publicznie obrazić i poniżyć X. Demonstracja obrzydzenia / osłabienie X.
* Wanderlust: dowiedzieć się, co jest za TĄ GÓRĄ. Co kryje się za TYM WZGÓRZEM. Wiecznie w drodze, nie rozumiem pojęcia domu.
* Papa Smerf: zawsze doradza, daje dobre rady. Chce być pomocny i dać rady które naprawdę pomogą. Chce, by za jego radami KTOŚ poszedł i by było mu lepiej.
* Ukojenie cierpienia Bashira: inni cierpią. Jeśli jestem w stanie ich ukoić i im pomóc, to mój moralny obowiązek.
* Networking: poznać nowe osoby, budować sieci przysług i znajomych. Kariera, moc i znajomości.
* Inflitracja Mitnicka: przełamywanie zabezpieczeń, przekraczanie tego co możliwe. Jeśli jest zamek, sprawdzę, co jest za drzwiami.
* Legenda Alivii Nocturny: chcę wiedzieć WSZYSTKO o tym statku kosmicznym. Znać personalia załogi. Absolutny nerd historii / techniki.
* Zdobyć najlepsze narzędzia: to co mam działa, ale z TAMTYMI narzędziami będę 10x programistą. Tym samym czasem zrobię dużo więcej.
* Hipergamia: znaleźć partnera jak najwyższej wartości (wg. swoich kodów).
* Napisać własny kompilator: co prawda moje dzieło nigdy nie dorówna komercyjnym, ale jest moje i sprawne. Mało kto to umie. I dostosuję do siebie.
* Wyplątać się z demonicznych długów: muszę jeść by przeżyć i wpakowałem się w ostre kłopoty z siłami, które się nie patyczkują. Spłacić lub uciec.
* Jestem unikalny: special snowflake. Nie jestem jak inni. Jestem wyjątkowy i wszyscy się zaadaptują do mojego identity.
* Lokalny społecznik: jest problem w społeczeństwie - bezpańskie zwierzaki. Oddolne działania, by zrobić coś, by ten problem zaleczyć. Pomogę mej wsi.
* Ważny sojusznik: uzyskać szacunek i wsparcie kogoś ważnego, mocno usytuowanego.
* Powszechna sława: niech inni opowiadają o mnie pieśni, niech inni chwalą me czyny. Niech inni o mnie mówią - dobrze lub źle.
* Wzbudzenie zachwytu: moje dzieła są podziwiane przez innych. Szok, zachwyt, majestat. Jestem artystą.
* Zapewnić dzieciom jutro: jest mi źle, 'hustling all the time', ale zrobię wszystko dla moich dzieci które mają tylko mnie!
* Mentoring Paperta: to brzydkie kaczątko zostanie pięknym łabędziem. Zarówno osłonię, zbuduję narzędzia jak i pokieruję.
* Zbudować legacy: przysposobić swego syna by utrzymał pozycję i siłę rodu/firmy. Lub - udźwignąć legacy.
* Żyć jak celebryta: mimikować życie możnych i sławnych; obsesja na punkcie celebrytów. Reality show, kamery itp.
* Usunąć szczury z piwnicy: pests. Usunąć pomniejsze niebezpieczeństwo, które innym mniej przeszkadza.
* Zatrzymać czas: nie starzeję się! Jestem wiecznie młody! Dalej tańczę z nastolatkami!
* Gliniarz w przedszkolu: jak ryba poza wodą; ogromny szok kulturowy i brak autorytetu i brak metod opresji by autorytet wymusić. Znaleźć swoje miejsce.
* Follow My Dreams: chcę zostać koreańskim raperem i zrobię wszystko, by osiągnąć ów nierealny cel!
* Awans za wszelką cenę: albo lepsza firma albo wojna o stanowisko z innymi. To stanowisko będzie moje.
* Cancel Culture: wykluczenie kogoś przez grupowy ostracyzm. Wszyscy się dopasują do mojego świata i wersji świata / kultury. Lub tępienie injustices.
* Inkwizytor: niszczenie fake news, publiczne pokazywanie hipokryzji, ujawnianie bolesnych prawd
* Krucjata: tępienie innowierców i apostatów by wzmacniać swoją grupę lub siebie
* Herold Baltazara: zwiększać wpływ i moc kogoś innego, powiększać ich wpływy.
* Zmiana tradycyjnej struktury: aktualne systemy są problematyczne i szkodliwe, musimy je zmienić nawet wbrew innym. Jak się nie uda, zniszczyć.
* Impress the superior / senpai: zrobić coś by osoba wyżej mnie doceniła i pogłaskała lub zobaczyła moją wartość!
* Crazy stalker: pozbyć się stalkera / crazy ex. Ktoś nie chce mnie zostawić w spokoju a prawo - i ja - jest bezradne.
* Odbudować reputację: może to plotka ("he is gay"), może czyn z przeszłości - ale próba odbudowania reputacji, prestiżu i życia po wydarzeniu.
* Odzyskanie kontroli: chcę być przydatny. Chcę widzieć, że mam kontrolę nad sytuacją. Na razie inni mi pomagają i rzeczy się DZIEJĄ, ale nie JA JE ROBIĘ.
* Zwyciężyć w debacie / sporze o miedzę: to JA mam rację. Publicznie będzie mi to przyznane! Moje ego tego wymaga! Karen mode on.
* Tępić debili: nienawidzę głupoty do poziomu NUKLEARNEGO. Niech cierpią. Sama przyjemność. I zemsta.
* Supremacja: Moja grupa jest LEPSZA, tamta grupa jest GORSZA. Wzmocnienie moich lub osłabienie tamtych jest moralnie właściwe.
* Nawracanie: przekonanie innych do swoich poglądów i swojej Drogi.
* Sprawne procesy: budowanie sprawnych procesów dających strukturę i optymalizujących dobro wspólne.
* Zasady są święte: istniejące zasady są sprawne i trzeba się ich trzymać. Zasady > dobro, sprawiedliwość. Porządek społeczny.
* Sprawiedliwość: mniej istotne co jest DOBRE, ważniejsze co jest SPRAWIEDLIWE. Zły czyn musi być odpowiednio ukarany, niezależnie od statusu.
* Harmonia naturalna: balans między technologią i naturą, utrzymanie cudów natury. Lasy Amazonii.
* Odbudowa i odnowa: coś, co jest zniszczone lub niesprawne zostanie odrestaurowane i naprawione. Coś o niezbywalnej wartości. Też: terraformacja.
* Urażony szacunek: nikt nie może mnie disrespectować - a to się stało. Dopadnę drania i odzyskam to, co mi się należy.
* Towarzystwo: czy przyjaciele, czy zwierzak - ale chce być w swojej Grupie i nie chce być rozdzielony z Grupą.
* Uwolnić niewolników: czy pod złym managerem w firmie czy naprawdę, czy z miłości; celem jest uwolnić kogoś niezdolnego do własnej woli.
* Wyciągnąć kogoś z bagna: pomóc komuś poradzić sobie z personalnymi problemami, pomóc komuś stanąć na nogi
* Rite of passage: by dziecko stało się dorosłym, musi dokonać Wielkiego Czynu. Zdobyć czyjeś ucho, zabić potwora...
* Złota klatka Lauriego: jest mi bardzo dobrze, ale pragnę wolności! Chcę latać wolno! Skonfliktowanie między światami.
* Rana ego, obsesyjna: THE FUCKER MUST PAY! Nieważne co jest prawdą czy kłamstwem, FUCKER WILL BE RUINED!
* Strach przed zapomnieniem: cokolwiek się nie stanie, nie mogą o mnie zapomnieć. Czy jak zniknę, ktoś w ogóle zauważy?
* Kapitan Ahab: polowanie na białego wieloryba do końca świata i jeszcze dalej, nieważne co przy tym stracimy.
* Komfortowe życie: chcę komfortowego, spokojnego i bezpiecznego życia. Nigdy nie martwić się co będzie jutro. Dla siebie lub kogoś.
* Do the right thing: niezależnie od tego co jest wygodne czy tanie, własne zasady i własny kompas moralny są najważniejsze. By spojrzeć w lustro.
* Korupcja anioła: nobody is perfect. Ona jest niby tak czysta i modelowa. Corrupt her! Make her like others!
* Władca marionetek: wszyscy będą grali tak jak im zagrasz. Informacje, plotki, manipulacja, paranoja. To Ty władasz światem każdego z nich, z cienia.
* Prowokator: podkręcanie atmosfery, by inni działali ostro pod stresem. Obserwowanie ich prawdziwej natury.
* Dominacja i terror: to JA tu rządzę. Ja mam władzę. Będą się mnie bali i zrobią wszystko, czego sobie życzę.
* Wolność od społeczeństwa: nie będę od nikogo zależeć. Żyć off the grid. Nie podlegać żadnym prawom, iść własną drogą.
* Przejąć władzę nad ogranizacją: ten ród / ta firma będą MOJE. Podbiję je, zmanipuluję lub doprowadzę do tego pokojowo.
* Utopia Star Trek: znajdowanie porozumienia i dobra nawet w osobach bardzo różnych od nas. Współpraca i koegzystencja.
* Kolekcjoner: kolekcjonuje coś rzadkiego (znaczki, broń, listy Eustachego) i potrzebuje powiększyć kolekcję / jest w sporze z kimś
* Unikalne przeżycia: everything passes, memories remain. Jeśli czegoś sobie odmówię, będę żałować do końca życia.
* Odkrycie konspiracji: WIEM, że jest konspiracja. Prawie to rozumiem. Budowanie sieci osób zdolnych mi w tym pomóc i faktów by to odkryć.
* Niszczenie hipokrytycznego systemu: prawa nie działają i nie są równe; złamię je by doprowadzić do ich zniszczenia.
* Ochraniać słabszych: ja jestem w stanie się obronić i nie zostanę zniszczony. Ich trzeba chronić by nic złego ich nie spotkało.
* Złamać kogoś, zdominować: pokazać komuś że to JA naprawdę mam władzę i beze mnie tamta osoba nie ma niczego i nic nie znaczy.
* Rule of Cool: nieważne jak to skuteczne, ważne, że oczy wszystkich będą na nas! Reckless, dangerous but oh-how glorious!
* Zrozumieć zagadkę: ta rzecz - obsesyjna - nie ma sensu. MUSZĘ zrozumieć czemu to się stało, o co z tym chodzi.
* Zrozumieć kulturę: dowiedzieć się jak najwięcej o grupie i kulturze, o ich nawykach, siłach i sztuce.
* Coup de grace: stało się coś strasznego - korupcja, magia, straszne rany itp. Zostaje jedynie zakończyć cierpienie osoby lub firmy.
* Uniknąć odpowiedzialności: coś zbroiłem. Ale to nie ja. Ktoś inny ucierpi - lub nikt.
* Lawful Tyrant: w reakcji na zło, które się stało teraz wszyscy muszą być bezpieczni. Bezpieczeństwo ponad wszystko. Superman z Injustice.
* Opus Magnum: stworzyć wielkie dzieło, zrobić coś najdoskonalszego czego niekoniecznie zrozumieją inni.
* Starszy Brat: grobowiec świetlików. Uratować, chronić i pomagać mniej zaradnej istocie pod swoją opieką.
* Tajemnica Wszechświata: zrozumieć coś niezrozumiałego, zrozumieć co kryje się po drugiej stronie światła.
* Torment Bombshella: bezwzględne i niesamowite okrucieństwo, sadystyczna destrukcja woli i duszy by zobaczyć jak daleko się da doprowadzić ofiarę i ZROZUMIEĆ coś.
* Najlepszy Musashimaru: najlepszy, największy, już wchodzi na dojo. Niebiański a jednak przez wszystkich kochany! Restraint, achievement, gentleness.
* Niezłomna Forteca: A man of dignity and virtue. Nikt nie zmusi mnie do powiedzenia kłamstwa. Mogę stać sam przeciwko wszystkim. Incorruptible. I shall not fall.
* Tigańczyk: członek wyklętej i zapomnianej grupy, nadal skrajnie jej oddany i lojalny. Kiedyś zemszczą się na Niszczycielu mimo, że są underdogiem.
* Deathwish: nie chcę więcej żyć / umieram (choroba?). Ostatnie co mi zostaje to opracowanie w jaki sposób zginę i co zrobię ostatnim ruchem.
* W poszukiwaniu boga: ten świat jest zbyt płaski, MUSI być coś więcej. Być częścią czegoś WIĘKSZEGO, ważniejszego. Znaleźć swego boga.
* Red Queen Race: jeśli stoisz w miejscu, cofasz się. Musisz iść do przodu, ewoluować, przegonić wszystko i wszystkich.
* Ćma w płomienie: gwiazdy które świecą najjaśniej gasną najszybciej; nie chcę żyć niezauważony. Mogę zginąć, ale BĘDĘ ZAPAMIĘTANY! Żyjesz raz!
* Broken soul: zostawcie mnie wszyscy w spokoju. Nie chcę wiedzieć, nie chcę interaktować, nie chcę musieć. Za dużo. Dążę do samotności i ciszy.
* Corrupted guardian: zrobiono mi coś strasznego. Nie jestem już w pełni osobą. Ale nikogo innego nie spotka to co mnie, ochronię innych.
* Corrupted contagion: zrobiono mi coś strasznego. Teraz się podzielę tym cierpieniem. Skażę wszystko i wszystkich. Everything beautiful dies.
* Być jak idol: iść w ślady swojego Idola i Mentora. Stać się tym czym idol się stał.
* Lokalny bohater: to ja jestem bohaterem. To ja rozwiążę ten problem. To moja odpowiedzialność i powinność.
* Harmonia i akceptacja: tak jak jest występuje balans i ten balans należy utrzymać.
* Survival of the fittest: przetrwają najlepiej dostosowani. Naszą rolą jest ekspozycja i zwycięstwo.

; Sekcja archetypów 2019

* Godslayer: pokonać bestię / zniszczyć anomalię zdecydowanie przekraczającą jakiekolwiek możliwości. Pokonać niepokonane.
* Predaking: "victorious to the end", nigdy bezradny, siła + sprzęt + umiejętności. Waleczny, zawsze w otwartej konfrontacji.
* Niestabilny geniusz Suspirii: nie krzywdzić innych, być częścią grupy, żąda specjalnego traktowania i próbuje pomóc - nie tracąc kontroli nad mocą.
* Arlekin Maytag: Więcej radości i życzliwości na świecie. Bez brutalnego okrucieństwa - spryt może rozwiązać każdy problem. Adoracji, uwagi, życzliwości.
* Przyjazny Kameleon: adaptacja do sytuacji społecznych, każdemu pokazuje taką twarz jaka jest najbardziej lubiana. Chce być lubiany i kochany.
* Femme Fatale: chcę mieć jak najwięcej przyjaciół, znajomości i osób wykonujących moje żądania. Kochana, podziwiana, inni na skinienie.
* Pirotess: Obrońca, w pełni oddany swemu Dowódcy. Dark Nicole z When Dreams Collide. Chronić Dowódcę, doprowadzić Sprawę do sukcesu. "doesn't matter who we are".
* Artystyczna dusza: Stworzyć piękno, pokazać je całemu światu lub wręcz przeciwnie, zachować dla godnej publiczności. Być docenionym.
* Daredevil: Takie życie jak mają jest nudne i bezsensowne. Niech się obudzą, niech mnie podziwiają. Niech widzą co można! Muszę działać na krawędzi.
* Ekstremista: Wszyscy inni muszą zrozumieć, że TO jest jedyna możliwa droga - albo moje dzieła zniszczą wszystkie inne opcje. Poison Ivy.
* Preserver: "this planet is dying..." - TO jest ważne i piękne i niewidoczne! Muszę mieć pomoc byśmy wszyscy TEGO nie stracili! GM w umierającym MMO.
* Eksplozja różnorodności: Szacunek dla odmiennych kultur i konieczność ich zachowania i zrozumienia. Maksymalne poszerzenie tego co się da.
* Supernowa: Wiecznie w pogoni za impulsami, uczuciami i działaniem. Head over Heels. Zawsze na forpoczcie, zawsze coś próbować i sprawdzać.
* Pokój przez tyranię: stworzyć pod swoją egidą imperium kontrolowane jego wolą, władzy, potęgi i mocy -> więcej władzy, potęgi, mocy.
* Wędrowny mistrz Ryu: chcę być coraz lepszy w Czymś, chcę trudnych wyzwań, chcę się wykazać. There is always someone stronger.
* Apokalipta: zniszczyć znany Świat, gromadzić wyznawców, pokazać im horror świata, kult apokalipsy; Sheaim, Ezekiel Rage, Raw Le Cruiset.


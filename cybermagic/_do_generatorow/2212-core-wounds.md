# Core wound list

## Komentarz

From Psychology Today:

"Core wounds tend to be things like a sense of not being enough, of being unlovable to a parent, of feeling stupid, dirty, unwanted, or ugly. No matter what your core wound may be, you can guarantee that your wound influences who you are and how you behave..." 

"Every core wound is based on a basic knowledge that we are unacceptable as we are, so we have to adjust and change to be perceived as good."

From LoneWolf:

"In life, we all have the tendency to believe that we are unworthy on some deep, undefinable level."

## Lista

* Rozdarcie ideologiczne: "Nie mogę wybrać między tymi światami, oba są ważne". Rozdarcie między dwoma ideologiami, próba ich pogodzenia i niewiedza co jest prawdziwą prawdą i co robić. 
* Nightmarish Ancestor: "Nie będę jak ONA!". Nie jestem jak mój potworny przodek któremu praktycznie wszystko zawdzięczam...
* Monstrous Nature: "I cannot lose control!". Jeśli stracę nad sobą kontrolę, stanie się coś naprawdę strasznego. Pamiętam, co się stało gdy oddałem się impulsom.
* Wszyscy mnie zawsze zostawiają: bez rodziny, bez przyjaciół, nieważne czego nie zrobię - zawsze wszyscy się ode mnie odwrócą.
* Nie chciałem ich zabić: coś co zrobiłem skrzywdziło / zabiło przyjaciół czy niewinnych. Nigdy tego nie odpłacę.
* I hurt everyone I love: nie umiem się kontrolować i ranię wszystkich, którzy się do mnie zbliżają. Robię to nieświadomie i nie mogę nic poradzić.
* Nikt nie widzi we mnie osoby, tylko funkcję: czy z uwagi na ród czy z uwagi na pozycję nikt nie patrzy na mnie jak na osobę. Widzą tylko księcia.
* Nikomu nie zależy na mnie - tylko na tym co mogę dać: mam środki i wpływy. To sprawia, że wszyscy dookoła mnie są fałszywi. Nikomu nie zależy na mnie. Tylko na tym co mam.
* Why only I had survived?: byli lepsi. Byli wspanialsi. Ale jestem jedyną osobą, która przetrwała. Mnie oszczędzono / tylko ja miałem szczęście.
* I did THINGS to survive: to co robiłem sprawiało, że byłem potworem. Zmieniło mnie to w coś gorszego niż człowiek. Nie ma powrotu z tego co zrobiłem.
* Jestem z gorszej rodziny niż inni: jak człowiek wśród elfów; coś odnośnie mojej rodziny lub kasty sprawia, że nie pasuję do innych i jestem niższej wartości
* Nigdy nie będę już głodował!: walczyłem o wszystko i nie było mnie na nic stać. Nigdy więcej! Nie wrócę tam!
* Grimlock's body failure: powoli tracę kontrolę nad ciałem. Nie wiem jak długo mogę zaufać, że ciało będzie działać. Nie mam dużo czasu...
* Upadek z tronu: miałem wszystko, osiągnąłem tak wiele i wszystko straciłem. Wszystko na czym mi zależało. Jestem bezwartościowy.
* Rodzina mnie nie akceptuje: muszę udowodnić, że jestem godny miłości i szacunku. Uważają mnie za nieudacznika i że nic nie osiągnę.
* I was a golden child: niby nic nigdy nie mogłem zrobić źle. Traktowali mnie jak królewicza. A ja zachowywałem się tak, jak mnie traktowano...
* I am a black sheep: cokolwiek się nie stanie, wszystko spadnie na mnie. Nigdy mnie nie docenią, nigdy mi nie podziękują.
* Forever Alone: nikt nigdy mnie nie kochał, nigdy nikt się do mnie nie zbliżył, nikt mnie nie będzie kochać. Bez przyjaciół i bez rodziny. Jak "dziecko z biovata"
* Homo Superior Bashira: nie chciałem się urodzić nielegalnie i mieć nielegalnego życia. Gdyby inni wiedzieli, odrzuciliby mnie od ręki.
* My shameful secret got exposed: mój pamiętnik został przeczytany i opublikowany. Zostałem wyśmiany i wzgardzony. Wszystko co powiesz, ktoś wykorzysta.
* Cast away for THEIR sins: nie byłem winny, ale byłem pod ręką i zostałem odrzucony dla ochrony społeczności i grupy.
* Betrayed by an idol: wierzyłem w niego. Na końcu się okazało, że nie był tym kim się wydawał. Poświęcił mnie dla potęgi i władzy. 
* Moja wiara była kłamstwem: wszystko w co kiedykolwiek wierzyłem okazało się być jedynie pustymi słowami by pilnować porządku i by było łatwiej.
* I failed them all: Nie mogłem jej uratować; patrzyłem jak umierają na moich oczach i byłem bezsilny.
* Rywal się nade mną znęcał: nie tylko byłem słaby, ale mój rywal się bawił moim cierpieniem. Próbowałem z nim walczyć ale to było tylko upokorzenie za upokorzeniem.
* Byłem zabawką w cudzych rękach: robili ze mną co chcieli a ja na to pozwalałem. Nie mam już godności. Jestem śmieciem.
* Przyjaciele poszli do gangu, ja na studia: oni poszli w jedną stronę, ja w drugą. Zdradziłem ich. Zdradziłem nasze ideały. 
* Mam niebezpieczną stalkerkę: nie tylko zmieniła moje życie w piekło, ale też zagraża innym, zbliżonym do mnie.
* Zniszczyłem rodzinę uzależnieniem: gdy byłem uzależniony, zadałem tak wiele cierpień swoim bliskim i innym. Do dziś budzi mnie szept używki... 
* MOGŁEM powstrzymać katastrofę, ale mi się nie chciało: problem Spidermana; teraz są straszne konsekwencje którym łatwo mogłem zapobiec. Jestem winny.
* Always in her shadow: 
* I am a disappointment: 
* I have spread a disease: czy fizyczną, memetyczną itp - doprowadziłem do tego, że kwarantanna została złamana i wiele osób ucierpiało. Złamałem zasady.

## References

* https://writershelpingwriters.net/emotional-wound-thesaurus-table-of-contents/ 
* https://www.novelwritingonedge.com/2020/10/hook-lines-with-core-wounds.html
* https://lonerwolf.com/core-wound/

---
layout: inwazja-konspekt
title: "Cechy charakteru, obserwowalne"
---

# {{ page.title }}


Apodyktyczny, władczy
Bez cienia manier, nie potrafi się zachować
Bezkompromisowy, nieustępliwy, niemożliwy do zatrzymania
Intrygancki, lubi sieci intryg i politykę
Kulturalny i obyty w świecie
Lekceważący i zuchwały
Ma fatalny styl; zupełnie bez gustu
Masochistyczny, czuje przyjemność z cierpienia
Niezależny i nie akceptujący zależności od innych
Olśniewający i pełen przepychu
Opętany szaleństwem; nie działa normalnie
Opryskliwy, bezceremonialny i obcesowy
Patriotycznie nastawiony
Pełny gracji i wytworny
Pochlebczy i obłudny
Pretensjonalny, sztuczny, chce udawać kogoś innego
Sadystyczny, lubi patrzeć jak inni cierpią
Samowystarczalny, trochę pod McGyvera
Skupia się na własnej reputacji jako najważniejszej rzeczy
Szczególnie właściwie traktuje płeć przeciwną
Szczery i nie ukrywający nic przed innymi; uczciwy
Sztywny, jak z kijem w... plecach
Trzeźwo myślący, o jasnym umyśle
Wierny, skupiający się na swoich przekonaniach
Wulgarny, czy to w mowie czy w stroju
Wykształcony, ze szczególną wiedzą w jakiejś dziedzinie
Zagubiony, rozpaczliwie szuka kogoś, kto da kierunek życia
Zatroskany i zaniepokojony
Zazdrosny i zawistny
Zjadliwy i ostry jak żyleta

## Inteligencja

### HIGH

Spostrzegawczy, zauważa rzeczy które innym unikają
Bystry, szybko łączący fakty
Elokwenty, zna też świetnie słownictwo
Mądry, dużo wie i wykorzystuje tą wiedzę
Przebiegły i wnikliwy; sprytny
Bardzo uzdolniony, specjalista w swoim fachu

### LOW

Niezgrabny, niezdarny
Wnikliwy i odkrywczy
Niekompetentny; niezdolny do rywalizacji

## INNE

Zdesperowany, nie cofnie się przed niczym
Z dobrego domu, trochę arystokratyczny
Atletyczny, dobrze zbudowany
Atrakcyjny, przyciąga uwagę
Ma wyraźne problemy zdrowotne
Pełny nienawiści; nieufny i niechętny innym
Pełny szacunku i uszanowania wobec innych
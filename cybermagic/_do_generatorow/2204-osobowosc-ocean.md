# Personality OCEAN

## Komentarz

* Każda osoba ma co najmniej dwa parametry obecne, nieważne czy wysokie czy niskie.
* Każda osoba ma co najmniej 3 traity
* Każda wartość osobowości generuje [0, 2] traity

## Personality Values

* Extraversion
* Neuroticism
* Conscientiousness
* Agreeableness
* Openness

## Traits
### Extraversion
#### High

* Hedonistyczny, szukający przyjemności
* Radosny, cieszy się sam i daje radość
* Odważny, śmiały i przedsiębiorczy
* Ognisty, pełny pasji
* Z natury optymistyczny, ze skłonnościami do "jakoś się uda"
* Z poczuciem humoru, lubi żartować
* Dekadent, lubi przepych i luksus
* Egzaltowany, w przejaskrawiony sposób okazuje uczucia
* Chwalipięta, czego to nie zrobił...
* Dowcipny, Błyskotliwy, o ciętym języku
* Entuzjastyczny, skłonny do bycia porwanym idei
* Figlarny, wesoły
* Kocha zabawę, wszędzie szuka rozrywki, chętny przygód
* Łatwo nawiązuje kontakt, otwarty
* Namiętny i żarliwy, płomienny
* Pełny werwy i animuszu, pełny życia, chce żyć pełnią życia
* Pełny wewnętrznej dumy z czegoś, pewny siebie
* Zadaje mnóstwo pytań, troszkę inkwizycyjny
* Zadowolony z siebie, beztroski
* Młodzieńczy, zachowuje się młodziej niż wypadałoby z wieku
* Żarliwy, skłonny do wydania ogromnej ilości energii
* Pełen wdzięczności, raduje się i docenia
* Pełny energii, żywy wulkan
* Lubi podróże, non stop zmienia miejsce zamieszkania
* Aktywny, wiecznie coś robi
* Ambitny, chętny, by się wykazać
* Ciekawski, wszystko chce wiedzieć i wszędzie wsadzić nos
* Brawurowy, ze skłonnościami do nadmiernego ryzyka
* Ekstrawagancki, ekspresywny
* Oddany komuś lub czemuś, ogniskuje swoją tożsamość wobec tej idei
* Lubi wyzwania i rywalizację, wszystko traktuje jako rywalizację
* Zbyt pewny siebie, zarozumiały
* Drapieżny, wiecznie zdobywa coraz więcej
* Idealistyczny, skupiony na lepszej przyszłości
* Ciepły, otwarty wobec ludzi
* Apodyktyczny, władczy
* Ryzykant, zawsze rzuca się w ogień
* Celuje w jak największe nagrody
* Kordialny, entuzjastyczny, wulkan energii
* Asertywny, nie daje sobie w kaszę dmuchać
* Bardzo lubi być w środku uwagi
* Lubi spotykać się z ludźmi, ciepły
* Lubi zaczynać rozmowę i przełamywać ludzi
* Ma bardzo dużo przyjaciół i znajomych z którymi wchodzi w interakcję
* Bardzo łatwo zdobywa nowych przyjaciół
* Zawsze pełen energii wśród innych ludzi
* Wpierw mówi, potem myśli co powiedział

outgoing, enthusiastic, risk taker, pursues rewards, cordial, assertive, active, warmth, energetic, looking for sensations, enjoys being the center of attention, likes to start conversations, enjoys meeting new people, has a wide social circle of friends and acquaintances, finds it easy to make new friends, feels energized when around other people, say things before thinking about them

#### Low

* Skryty; zachowujący swoją prywatność dla siebie
* Domator, osoba rodzinna
* Samotnik z natury
* Introspektywny, skupiony na głębokim wnętrzu
* Kontemplacyjny, refleksyjny
* Konformistyczny, unikający kłopotów
* Melancholijny, patrzy często w przeszłość
* Obserwator; raczej stoi z boku i patrzy niż działa bezpośrednio
* Polega tylko na sobie; nie wierzy w innych
* Skłonny do refleksji i szukania odpowiedzi w przeszłości
* Skłonny do szukania odpowiedzi w teorii
* Skromny i unikający pokus ciała
* Powściągliwy, skryty i wyważony
* Chłodny, nie okazuje uczuć
* Apatyczny, niewiele tę osobę motywuje
* Bezbarwny, przezroczysty, praktycznie niewidzialny
* Tajemniczy, skrywa sekrety pod uśmiechem
* Zdystansowany, powściągliwy i zamknięty w sobie
* Będzie jak ma być; fatalistyczny
* Sentymentalny, podatny na nostalgię
* Niekłopotliwy, robi to co powinien i się nie wychyla
* Cichy, ma mało energii
* Wycieńczony gdy musi się socjalizować z innymi
* Ma problemy ze startowaniem rozmowy
* Nie cierpi 'small talk'.
* Dokładnie przemyśli wszystko zanim coś powie
* Nie znosi być w centrum uwagi
* Nie lubi ryzyka, woli wszystko przemyśleć kilka razy

aloof, quiet, low energy, avoiding people, prefers solitude, feels exhausted when having to socialize a lot, finds it difficult to start conversations, dislikes making small talk, carefully thinks things through before speaking, dislikes being the center of attention, doesn't like risk, 

### Neuroticism
#### High

* Bardzo łatwy do zniechęcenia
* Cichy i nieśmiały; bojaźliwy, niewiele mówi
* Nihilistyczny, nie widzi żadnej nadziei
* Tchórzliwy, unika ryzyka i kłopotów
* Uprzedzony wobec konkretnej grupy / klasy
* Ostrożny i powściągliwy
* Oszczędny, dusigrosz
* Ponury, wszędzie widzi ciemność i kracze
* Podejrzliwy i skłonny do szukania pułapek
* Samoniszczący, dążący do straty tego, co ma
* Samokrytyczny, wbija sobie nóż w plecy
* Wiecznie głodny większej mocy, potęgi i chwały
* Wiecznie niepewny, kwestionuje swoje decyzje
* Wrażliwy, łatwy do zranienia
* Skrępowany i pełny zahamowań
* Sceptyczny wobec prawie wszystkiego
* Boi się konkretnej rzeczy i unika tej rzeczy za wszelką cenę
* Introspektywny, skupiony na głębokim wnętrzu
* Krytyczny, szuka dziury w całym
* Maruda, mnóstwo narzeka i wysysa radość
* Paranoiczny; wszędzie widzi zagrożenie
* Przezorny, ostrożny, rozważny, roztropny
* Trwożliwy; boi się własnego cienia
* Niespokojny, nerwowy, rozbiegany
* Napięty i znerwicowany
* Posiada określone rytuały i tiki nerwowe
* Profesorski, mówi mądrze, ale niekoniecznie wobec targetu
* Nieśmiały, skrępowany i często zażenowany
* Poddańczy, ulega sile czy charyzmie; posłuszny i oddany
* Obrażalski, zawsze znajdzie coś za co może się obrazić
* Cyniczny, patrzy na świat z goryczą
* Skupia się na reputacji i bezpieczeństwie zanim coś zrobi
* Czujny i uważny, niemożliwy do zaskoczenia
* Nikomu nie ufa i nie polega na innych
* Bardzo łatwo go zestresować
* Szybko przechodzi w tryb agresywny
* Tendencje do depresji i widzenia tylko ciemności
* Hiperwrażliwy, wszystko go denerwuje
* Nieśmiały, unika działania mogącego zadać ból
* Experiences dramatic shifts in mood
* Po stresujących wydarzeniach chwilę trwa zanim wróci do siebie
* Zazdrosny, skłonny do rozpaczliwego trzymania się drugiej osoby

vigilant, trusts nobody, prone to stress, anxious, anxiety, aggressive hostility, depression, impulsiveness, hypersensitivity, shyness, experiences a lot of stress, worries about many different things, gets upset easily, experiences dramatic shifts in mood, struggles to bounce back after stressful events

#### Low

* Beztroski i swobodny; żyje według własnych przekonań
* Naiwny, łatwo wszystko wmówić
* Niefrasobliwy, beztroski i nieco naiwny
* Nie kłania się nikomu, z podniesioną głową
* Nieobliczalny, nie wiadomo co zrobi
* Stoicki, przyjmuje los i rzeczywistość ze spokojem
* Solidny, twardy i niewzruszony w swoich przekonaniach i działaniu
* Spokojny, bez pośpiechu; flegmatyczny
* Spokojny, trudny do wytrącenia z równowagi
* Uparty jak osioł, no nie przekonasz
* Wszędzie musi wsadzić nochal, wścibski
* Stanowczy i silny, zdecydowany
* Stabilny, opoka niemożliwa do ruszenia, niemożliwy do wyprowadzenia z równowagi
* Surowy, wymagający
* Nie ma absolutnie żadnych barier czy granicy osobistej
* Nadęty i napuszony, lubi jak się schlebia
* Szczery i nie ukrywający nic przed innymi; uczciwy
* Jeżeli czegoś chce, weźmie to dla siebie; zazdrosny i zawistny
* Stabilny emocjonalnie, trudno wyprowadzić z równowagi
* Doskonale radzi sobie pod wpływem silnego stresu
* Praktycznie nie bywa smutny czy zdołowany. "Ok, działamy dalej".
* Nie ma blokad, nie przejmuje się że coś pójdzie nie tak
* Nie przejmuje się niczym za bardzo. Jakoś to będzie. Don't worry, be happy.
* Intrygancki, lubi sieci intryg i politykę
* Bezkompromisowy, nieustępliwy, niemożliwy do zatrzymania

emotionally stable, calm, relaxed, deals well with stress, rarely feels sad or depressed, doesn't worry much, phlegmaticity, self-controled, slowness, calmness, balance

### Openness
#### High

* Barwny, z niesamowicie ciekawymi opowieściami
* Perwersyjny, uwielbia szokować i robić skandale
* Tolerancyjny, skłonny do akceptowania wszystkiego, co nie robi bezpośredniej krzywdy
* Subtelny, zostawia podpowiedzi zamiast mówić wprost
* Pochłonięty niezdrową fascynacją śmiercią i okultyzmem
* Przesądny, wierzy w "dziwną" przyczynowo-skutkowość
* Błyskawicznie szybko się adaptuje, ma na wszystko rozwiązanie
* Romantyczny, szukający uniesień i emocji
* Roztargniony i nieco zagubiony
* Wyznawca teorii spiskowych
* Chętny do eksperymentowania i testowania nowych rzeczy
* Kapryśny i marzycielski, buja w obłokach
* Zwracający ogromną uwagę na estetykę
* Ze skłonnościami do dramatyzmu; drama queen
* Ze skłonnościami do wymyślania i ubarwiania wszystkiego
* Zmysłowy i bardzo skupiony na ciele i uczuciach
* Myśli swobodnie, nie ogranicza się do doktryn czy dogmatów
* Kreatywny, tworzący wiecznie coś nowego
* Innowacyjny, szuka nowych rozwiązań i podejścia
* Łatwo wywrzeć na tej osobie wrażenie
* Proponuje bardzo niekonwencjonalne, dziwne i często śmieszne rozwiązania
* Bardzo nietypowe formy ekspresji płci
* Religijny, uduchowiony, skupiony na tematach mistycznych
* Otwarty na nowe wrażenia i doznania
* Marzycielski, nie do końca jest w tym świecie
* Dziecinny, niepoważny, dziwaczny i troszkę śmieszny
* Wszechstronny; człowiek renesansu
* Zwodniczy i złośliwy przy tym; trickster; bystry w dowcipach i słowie
* Prowokacyjny i wyzywający; lubi wywoływać reakcję
* Oczytany, ceni wiedzę dla wiedzy
* Błyskotliwy, świetny w zagadkach i zawsze do nich dąży
* Unusual Beliefs: ma dziwne skojarzenia i nietypowe przekonania
* Ekscentryczny, zachowuje się nietypowo
* Uwielbia absurdy i wszędzie widzi absurdalne rzeczy
* Ma bardzo bujną wyobraźnię, zawsze ma ciekawe pomysły i własne wizje
* Mistrz improwizacji
* Otwarty na nowe przeżycia, nawet nietypowe. Wszystkiego trzeba spróbować raz
* Elokwenty, ma bogate słownictwo - a jednak często stosuje słowotwórstwo
* Uwielbia 'what if' i abstrakcyjne koncepty
* Nie potrafi usiedzieć długo na jednym miejscu

creative, imaginative, eccentric, unusual beliefs, prone to absurd, imagination, aesthetics, feelings, action, idea, values, open to trying new things, focused on tackling new challenges, happy to think about abstract concepts, rich vocabulary

#### Low

* Swojski, prosty, praktyczny
* Nietolerancyjny, musi być tak jak uważa
* Praktyczny, skupiony na tym co może zrobić
* Osoba starej daty, trochę z poprzedniej epoki
* Prostolinijny i otwarty; podstępy nie są tu siłą
* Płytki; zwraca uwagę tylko na to co widać na pierwszy rzut oka
* Purytański, nie toleruje odchyleń od "normalności"
* Przewidywalny i schematyczny
* Racjonalny, szuka wyjaśnień w istniejącej rzeczywistości
* Z natury odrzuca wszystkie pomysły, trzeba powoli przekonywać
* Autentyczny, nie zakłada masek; mówi jak jest
* Małostkowy; przywiązuje wagę do rzeczy nieistotnych
* Dogmatyczny i ze skłonnościami do potępiania
* Prawi wszystkim morał, mówi jak mają żyć
* Przyziemny, stąpa twardo po ziemi
* Obojętny, nie wykazuje inicjatywy, robi co każą
* Bezpośredni, mówi jak myśli; bardzo konwencjonalny
* Nudny i przewidywalny; wiadomo dokładnie jak się zachowa
* Poważny, unika żartów
* Pełny godności, nieco starej daty
* Pretensjonalny, sztuczny, chce udawać kogoś innego
* Nudny. Nie ma nic ciekawego do powiedzenia i niezbyt się czymkolwiek interesuje.
* Lubi powtarzalne rzeczy, to co już było i co już zna
* Nie lubi abstrakcyjnych czy teoretycznych konceptów; bardzo praktyczny
* Nie ma co chodzić z głową w chmurach, tu trzeba ciężko pracować
* Nie cierpi zmian
* Nigdy się nie nudzi, lubi rutynę i powtarzalne rzeczy
* Nie ma specjalnie wyobraźni, nie lubi abstrakcji. Lubi proste przykłady.

practical, conventional, boring, dislikes change, does not enjoy new things, resists new ideas, not very imaginative, dislikes abstract or theoretical concepts

### Conscientiousness
#### High

* Oddany swojemu zadaniu / celowi
* Odpowiedzialny, przyjmuje odpowiedzialność
* Ogromną uwagę przykłada do stylu i ubrania
* Perfekcjonista; ważniejszy jest wynik pracy niż skutek / efekt
* Zadbany, skupiony na wyglądzie i działaniu
* Patrzący w dal, skupiony na długoterminowych działaniach
* Pracowity, skupiający się na pracy jako na wartości
* Przewidujący, trudny do zaskoczenia - na wszystko ma plan awaryjny
* Punktualny i nie tolerujący spóźnień
* Zdyscyplinowany, skupiony na osiąganiu sukcesów
* Szczególnie rozsądny i poczytalny
* Przedsiębiorczy i pomysłowy; wszystko załatwi
* Ascetyczny, unikający luksusów
* Dyskretny, sekrety weń są jak w grób
* Honorowy, zawsze wie jak winien się zachować
* Praworządny, uznaje prawo za najwyższą wartość
* Wiarygodny; można nań polegać
* Zdecydowany, szybko podejmujący decyzje i działający
* Zawsze bardzo zajęty, wypełnia sobie każdą sekundę, wiecznie w pośpiechu
* Pedantyczny, z ogromną dbałością o szczegóły
* Uważny, zwraca uwagę na szczegóły
* Świetnie zorganizowany; zawsze wie co, gdzie i kiedy
* Sprawny (wydajny), skupiony na szybkim wykonywaniu działań i działaniach operacyjnych
* Cierpliwy, jak pająk w sieci
* Fanatyczny; idea jest obsesją i życiem
* Dojrzały, zachowuje się profesjonalnie i "dorośle"
* Kompleks paladyna; bierze za dużo na siebie
* Metodyczny, stopniowo rozwiązujący problemy
* Kieruje się silnymi zasadami i regułami; lubi mieć strukturę
* Opanowany; niewiele po sobie pokazuje
* Uroczysty i poważny
* Formalny, niechętnie przechodzi na poziom koleżeński
* Skupiony na logice jako głównym narzędziu decyzyjnym
* Beznamiętny, o minie z kamienia
* Elegancki, noszący się z klasą i dba by świetnie wyglądać
* Perfekcyjnie wszystko planuje
* Kierowany obowiązkiem i powinnością
* Bardzo dużo przygotowuje się przed wszystkim; ma checklisty
* To, co ważne robi praktycznie natychmiast, nie odracza

perfect planner, self-control, organized, self-directed, competence, tendency to order, duty, striving for achievement, discipline, prudence, spends time preparing, finishes important tasks right away, pays attention to detail, enjoys having a set schedule

#### Low

* Niszczycielski, destrukcyjny
* Spontaniczny, działa pod wpływem emocji
* W ogóle nie zwraca uwagę na praktyczność, tylko inne aspekty
* Nierozważny i nieroztropny
* Zapominalski; non stop trzeba coś przypomnieć
* Wiecznie zagubiony, działa zgodnie z błędnym modelem
* Tendencje do podkradania różnych drobiazgów
* Bazuje na intuicji jako podstawie działania
* Impulsywny, nie do końca kontroluje emocje
* Kapryśny, zmienny i niestały; działa pod wpływem chwili
* Niechlujny, nie dba o higienę i porządek
* Niecierpliwy, chce TERAZ i nie będzie czekać
* Nie planuje długoterminowo, żyje chwilą
* Nadużywa różnych używek (narkotyki, alkohol)
* Niezdyscyplinowany, marnuje czas i energię
* Nieuczciwy, z tendencjami do oszukiwania
* Lekceważy sobie obowiązki i nie doprowadza niczego do końca
* Leniwy, marnuje czas
* Nieodpowiedzialny; nie można niczego powierzyć
* Zakłóca spokój; nie toleruje spokoju i nudy
* Niestały i zmienny, często zmienia zdanie i podejście
* Wiecznie zagubiony, nie robi tego co jest potrzebne
* Kontrolowany przez bardzo silne emocje
* "Zegarkom śmierć!". Mało punktualny. 
* Poświęcił całą noc na hobby, więc jest niewyspany rano
* Wszystko robi na ostatnią chwilę; odracza wykonywanie zadań
* Robi bałagan / problemy i nie sprząta po sobie
* Nie cierpi struktur i zasad. Wszystko chce po swojemu w swoim czasie.
* Nie odkłada rzeczy na miejsce z których je wziął
* Łatwo się rozprasza

spontaneous, careless, impulsive, dislikes structure and schedules, makes messes and doesn't take care of things, fails to return things or put them back where they belong, procrastinates important tasks, fails to complete necessary or assigned tasks

### Agreeableness
#### High

* Pacyfistyczny, pokojowo nastawiony
* Spolegliwy; niechętnie się kłóci
* Ufny i momentami naiwny
* Ugodowy, unika konfliktów - woli przegrać
* Ujmujący i sympatyczny
* Uroczy, miły w obejściu
* Życzliwy, próbuje pomóc i zostawić świat lepszym
* Wybaczający, nie dba o przeszłe rany. Ufny.
* Opiekuńczy, skłonny do ochrony innych
* Popularny, rozpoznawany i szeroko znany
* Pomocny, starający się wesprzeć innych
* Przyjacielski, łatwo nawiązujący znajomości
* Przymilny, taki trochę lizus
* Wielkoduszny, wzniosły. Kieruje się zasadami.
* Unikający obrażania kogokolwiek i konfliktów
* Słodki, uśmiechnięty i do rany przyłóż; wdzięczny; lekko naiwny
* Altruistyczny, skupiony na dobru innych ludzi
* Empatyczny, zwracający uwagę na uczucia i potrzeby innych
* Dobrze wychowany; zachowuje się zgodnie z zasadami
* Rodzinny, skupiony na życiu rodzinnym
* Wyraźnie czegoś żałuje i okazuje skruchę
* Uczciwy i dążący do balansu oraz zadowolenia wszystkich
* Taktowny, liczy się z innymi i ich poglądami
* Towarzyski i przyjacielski
* Mimik, dopasowuje się do stylu osoby, z którą rozmawia
* Litościwy, wybacza oraz zawsze skupia się na słabszych
* Łagodny, próbuje nikogo nie urazić i nie skrzywdzić
* Hojny, dzielący się z innymi
* Lojalny i oddany przyjaciołom / grupie
* Zawsze zgodny z najnowszą modą, nowoczesny, wie jak się ubrać
* Skupiony na współpracowaniu i sytuacjach win-win
* Serdeczny i szczery, momentami rubaszny
* Szlachetny, skupia się na czynieniu dobra
* Dusza towarzystwa, zapalnik współpracy grupowej
* Bardzo interesuje się innymi ludźmi i ich problemami
* Pomaga innym którym potrzebna jest pomoc

trusting, empathetic, cares for others, understands people, soul of the group, trust, straightforwardness, altruism, acceptability, modesty, tendency to be tender, has a great deal of interest in other people, cares about others, feels empathy and concern for other people, enjoys helping and contributing to the happiness of other people, assists others who are in need of help

#### Low

* Bezduszny i twardy
* Konfliktowy; dążący do starcia
* Krewki i drażliwy, łatwo wyprowadzić z równowagi
* Oportunista; skupia się na korzyściach dla siebie
* Prostacki, prymitywny, nie dba co myślą inni
* Sarkastyczny i złośliwy
* Skłonny do manipulacji i podstępów; podstępny i kłamliwy
* Skupiony przede wszystkim na pieniądzach i korzyściach materialnych
* Wyrachowany, szuka własnego interesu
* Uszczypliwy i zgryźliwy
* Agresywny, dążący do starcia
* Aspołeczny, nie dba o innych
* Jadowity, napędzany nienawiścią i możliwością zaszkodzenia
* Indywidualistyczny, skupiony na jednostkach
* Kłótliwy, lubi spory dla sporów
* Narcyz; skupiony na swoim odbiciu i zapatrzony w siebie
* Nielojalny, rzuci sojuszników pod autobus
* Nienawistny i złowrogi; źle chce dla innych
* Niepokojący, jest w tej osobie coś dziwnego
* Niewdzięczny, uważa, że wszystko się należało
* Nigdy nie wiadomo, co jest prawdą a co maską
* Przebiegły, podstępny, mówi nie wprost, pomysłowy i zręczny
* Wymagający, żąda maksimum wysiłku od innych
* Zrzędliwy i kłótliwy
* Zjadł wszystkie rozumy; zarozumiały
* Zniechęca innych, demotywujący
* Zawłaszcza osoby i przedmioty dla siebie
* Ma skłonności do gniewu i wybuchania
* Amoralny, jedyną zasadą jest korzyść
* Anarchistyczny, nie ufa nikomu u władzy
* Okrutny; nie zna litości
* Samolubny, skupiony na sobie
* Brutalny, bezwzględny, grubiański
* Zdradliwy; do sukcesu przez nóż w plecach sojusznika
* Nie dba o potrzeby innych, tylko o swoje
* Egocentryczny, jest pępkiem świata
* Cechuje się arogancją i poczuciem wyższości
* Opryskliwy, bezceremonialny i obcesowy
* Nie dba o to co inni czują i nie interesuje się ich problemami

uncooperative, hostile, picks fights often, takes little interest in others, doesn't care about how other people feel, has little interest in other people's problems, insults and belittles others, manipulates others to get what they want

## End

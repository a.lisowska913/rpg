# Values by Schwartz

## Komentarz

My operujemy na 2-3 najważniejszych i 1-2 mało istotnych.
Daje nam to całkiem fajny układ różnorodności

## Core Values

* Self-direction: autonomy, ability to control your own way and direction
* Stimulation: feel alive, feel more, new things, experiment, explore
* Hedonism: pleasure above anything else
* Achievement: build more skills, achieve more, expand on what you can do
* Power: more influence upon reality, amass wealth and influence
* Face: prestige; having a specific reputation and upholding it
* Security: being safe or keeping others safe
* Conformity: being one of the group, homogenity of a group
* Family: blood, clan or mafia. The group and its prosperity is the most important
* Tradition: what once was still works, only a fool throws away the knowledge of the past
* Humility: we have a place in existence, we are nothing compared to the Reality; faith
* Universalism: shed your mortal coil and your clan; we all are the same, expand
* Benevolence: altruism, help others, elevate others


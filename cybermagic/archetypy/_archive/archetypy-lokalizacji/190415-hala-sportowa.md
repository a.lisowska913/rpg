---
layout: inwazja-karta-postaci
categories: archetype
title: "Hala Sportowa"
---

# {{ page.title }}

## Lokalizacja

### Ogólny teren

* otwarty obszar z przestrzenią dla widzów
* miejsce imprez, sportu oraz rozrywki

### Aspekty

* nagłośnienie, holoprojektory, kamery
* ścieśnieni widzowie i arena
* sprzedawcy jedzenia, komercja
* bardzo duże emocje

## Konflikty

* nadmiar emocji i konfliktów
* trzeba się gdzieś szybko przedostać
* łatwo się zgubić
* pożar lub awaria

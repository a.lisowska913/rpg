---
layout: inwazja-karta-postaci
categories: archetype
title: "Hydromagitrownia"
---

# {{ page.title }}

## Lokalizacja

### Ogólny teren

* nad jeziorem lub rzeką, ekstraktor magii
* ma emitery przesyłające energię do celu
* generuje silne Skażenie; stosunkowo groźny
* konieczność silnej kontroli Pryzmatu

### Aspekty

* teren silnie chroniony
* wysoki poziom Skażenia
* anomalie i efemerydy
* niestabilność energii magicznych

## Konflikty

* pojawiające się anomalie i efemerydy
* ludzie którzy zabłądzili
* protesty pod magitrownią

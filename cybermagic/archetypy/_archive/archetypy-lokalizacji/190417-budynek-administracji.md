---
layout: inwazja-karta-postaci
categories: archetype
title: "Budynek Administracji"
---

# {{ page.title }}

## Lokalizacja

### Ogólny teren

* wiele pokoi i mnóstwo dokumentów
* zadbany acz niedofinansowany

### Aspekty

* labirynt, poczucie bezradności
* jeszcze jeden dokument

### Spotkania

* petenci oraz urzędnicy

## Konflikty

* bariery z dokumentami
* nie ma kluczowej osoby
* ktoś się awanturuje

---
layout: inwazja-karta-postaci
categories: archetype
title: "Barbakan"
---

# {{ page.title }}

## Lokalizacja

### Ogólny teren

* centrum militarne osady
* skomputeryzowane i ufortyfikowane
* ma niewielki garnizon

### Aspekty

* bardzo trudny do przełamania
* samodzielny, wymaga oblężenia
* działka i matryca defensywna
* autonomiczny TAI, z garnizonem
* repozytorium wiedzy i planów

## Konflikty

* przesłuchanie w barbakanie
* ukryć się przed barbakanem
* dostać się do środka

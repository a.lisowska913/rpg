---
layout: inwazja-karta-postaci
categories: archetype
title: "Osiedle Mieszkalne"
---

# {{ page.title }}

## Lokalizacja

### Ogólny teren

* zaprojektowane do życia dla ludzi
* sklepy, transport publiczny, bloki i domy

### Aspekty

* mnóstwo miejsc gdzie można "zniknąć"
* miejscowi znający skróty i ważne osoby
* tereny dla dzieci, komercyjne i mieszkalne
* przestrzeń wspólna, często zaniedbane

## Konflikty

* zbuntowane dzieciaki żądne wrażeń
* niewłaściwa dzielnica
* problem z transportem
* nieznajomość skrótów i kryjówek

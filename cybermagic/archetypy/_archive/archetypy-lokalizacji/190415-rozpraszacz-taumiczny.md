---
layout: inwazja-karta-postaci
categories: archetype
title: "Rozpraszacz Taumiczny"
---

# {{ page.title }}

## Lokalizacja

### Ogólny teren

* wysoki budynek z tarczami satelitarnymi
* kontroluje Skażenie na danym obszarze
* wymaga Pryzmatu i energii magicznej

### Aspekty

* dość delikatna konstrukcja
* silnie Skażona iglica
* osłabia moc magiczną w okolicy
* powoduje halucynacje i zagubienie

## Konflikty

* osłabia Twoje zaklęcie
* częste naprawy iglicy
* budynek nienaturalny z definicji

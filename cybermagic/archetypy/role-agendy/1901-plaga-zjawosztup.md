---
layout: inwazja-vicinius
title: "Plaga Zjawosztup"
---

# {{ page.title }}

## Agenda

### Wartości

* VITALITY: przetrwać, nie zostać powstrzymaną
* SPREAD: poszerzyć się, być wszechobecną
* CORRUPT: przekształcić swoje ofiary w coś innego, osłabić je

### Endgame

Endgame: Istnieje Wylęgarnia, grupa Skażeńców - Wektorów i rozprzestrzeniają Plagę na okoliczne regiony. Przekształceni magowie pilnują by nic złego nie stało się Pladze. Efektem ubocznym są burze magiczne i wynikające z tego Skażenia.

## Ruchy

### Defensywne

1. Ufortyfikuj się na danym terenie, zbuduj wylęgarnię (+ Earth (bezpieczeństwo)) 
2. Doprowadź do paniki wśród ludzi prowadzącej do zniszczeń (wymaga: Water (zainfekowani ludzie), + Status (chaos, Taunt))
3. Umagicznij populację ludzką (wymaga: Water (zainfekowani ludzie), Void (energii magicznej), + Water (magiczna populacja ludzi))
4. Zagnieźdź się w magicznej populacji (wymaga: magiczna populacja ludzi, + Earth (bezpieczeństwo)) 

### Ofensywne

1. Zaatakuj postacie bezpośrednio infekcją (wymaga: Earth (bezpieczeństwo), atak Typowy, + Status (zainfekowany))
2. Zaatakuj postacie agentami (wymaga agentów, atak Typowy lub Trudny, + Status (ranny))
3. Zaatakuj populację oddziałem mieszanym (wymaga magicznej populacji ludzi i Skażeńców, atak Heroiczny, + Status (ranny) + Status (zainfekowany))
4. Fala magiczna - z uwagi na niedomiar ludzi nie dotkniętych magią (wymaga: magicznej populacji ludzi, atak Heroiczny, + Status (Skażony), + Status (ranny))

### Agenda

1. Zainfekuj maga lub magów (wymaga: Void (mag / magowie), + Water (zainfekowany mag)) 
2. Wymuś na zainfekowanym magu paranoję i potężną emisję energii magicznej; przejmij go (+ nazwany zasób: agent)
3. Wymuś na zainfekowanym magu zbudowanie Skażeńca będącego wektorem Plagi (wymaga: zainfekowany mag, + nazwany zasób: Skażeniec będący wektorem)
4. Wyślij Skażeńca w inny teren (wymaga: magiczna populacja, Skażeniec wektor, + Earth (bezpieczeństwo), + nowe ognisko plagi gdzieś indziej) 

## Siły

* Bardzo wszechstronna Plaga - infekuje ludzi, viciniusy i magów
* Łatwo się rozprzestrzenia przy użyciu Wylęgarni, Skażeńców-wektorów oraz zainfekowanych magów
* Stosunkowo szybko się przemieszcza i nie jest łatwa do wyplenienia
* Możliwe nieoczekiwane interakcje z niektórymi viciniusami, artefaktami, anomaliami i polem magicznym.

## Słabości

* Dopóki nie osiągnie poziomu krytycznego jest stosunkowo mało groźna dla nosicieli i innych
* Burze magiczne prowadzą do tego że sama zostaje zniszczona własnymi działaniami

## Zasoby

* Co najmniej jedna Wylęgarnia
* Co najmniej jeden Wektor Plagi
* Zainfekowany mag, vicinius lub populacja ludzi
* W zależności od poziomu zaawansowania, patrz wyżej

## Opis

Plaga Zjawosztup pojawiła się po raz pierwszy na Trzęsawisku Zjawosztup i przesunęła się na Przelotyk. Magowie dali radę ją wyplenić, ale jest to wyjątkowo paskudny przeciwnik - zawsze wraca. Szczęśliwie, nie jest szczególnie niebezpieczna.

Plaga ta charakteryzuje się silnym skupieniem na przetrwanie; jest czymś pomiędzy klątwożytem a zwykłym wirusem. Przenosi się przez energię magiczną, ale też drogą kropelkową.

Objawami są: paranoja i pragnienie tworzenia nowych form magicznych i magicznej konstrukcji - konieczność i pragnienie do infuzji energią magiczną w istoty celem stworzenia nowych Wektorów.

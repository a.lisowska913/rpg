---
layout: inwazja-vicinius
title: "Drapieżnik przy wiosce"
---

# {{ page.title }}

## Agenda

### Wartości

* SUSTAINABILITY: nie chce zniszczyć swojego żywiciela, chce się konsekwentnie dokarmiać
* VIGOR: przetrwać i funkcjonować bez pojedynczej rany. Ranny staje się niebezpieczny.
* SURPRISE: atak z zaskoczenia, nigdy nie działać całkowicie jawnie 

### Endgame

Wioska żyje i raz na jakiś czas składa ofiary drapieżnikowi. Drapieżnik poluje na ludzi (czy cokolwiek) i nikt go nie atakuje. Ludzie czczą swojego drapieżnika.

## Ruchy

### Defensywne

1. Ludzie stają przeciwko postaciom - boją się zemsty lub czczą drapieżnika (wymaga: atmosfera strachu, + gang)
2. Drapieżnik skrywa się w legowisku i przeczekuje złe czasy (+ Earth (bezpieczeństwo))

### Ofensywne

1. Drapieżnik porywa bezradną ofiarę zmuszając Postacie do walki na jego terenie (Status: Taunt LUB +gang przeciw postaciom)
2. Drapieżnik atakuje swoje ofiary budując presję na wiosce - niech postacie Coś Zrobią (Status: Taunt LUB +gang przeciw postaciom)
3. Drapieżnik atakuje postacie bezpośrednio, na swoim terenie (Trudny/Heroiczny, + Status (ranny))
4. Drapieżnik dokonuje masowego, hurtowego mordu (wymaga: był Ranny, wymaga: wyleczony, + Status (masowe mordy dla przyjemności))

### Agenda

1. Wywołać terror w wiosce straszną obecnością (+ Status (atmosfera strachu))
2. Znaleźć ofiarę poza wioską i ją zaatakować z ukrycia (Trudny, + Status (ranny))
3. Znaleźć i zabić ofiarę w wiosce (Trudny, możliwość kontrataku, + Status (ranny))
4. Zbudować kult / bezpieczeństwo drapieżnika (+ Earth (bezpieczeństwo), + kult drapieżnika) 
5. Zbudować składanie ofiar (wymaga: kult drapieżnika, + Status (składanie ofiar))

## Siły

* Atakuje pierwszy, z zaskoczenia - wybiera zwykle słabsze ofiary
* Bardzo trudno go usunąć i pokonać poza legowiskiem z uwagi na prędkość i możliwość wycofania się
* Na terenie drapieżnika walka z nim jest Trudna lub Heroiczna. Niefortunne.
* Drapieżnik może skończyć jako Ranny w wiosce, ale trudno go tam zabić.

## Słabości

* Detal a nie hurt. Naraz może walczyć tylko z jednym przeciwnikiem. W jedności siła.
* Wioska jest ogólnie bezpieczna i walka w wiosce zagraża Drapieżnikowi.

## Zasoby

* Legowisko drapieżnika: miejsce, gdzie trudno się dostać i trzeba je wpierw znaleźć
* Teren drapieżnika: obszar szczególnie przyjazny drapieżnikowi i jednocześnie szczególnie niekorzystny dla innych 

## Opis

Tygrys przy niewielkiej wiosce. Rój technodron krążących dookoła wioski, polujących na pojedyncze osoby. Żywe bagno pragnące pożreć swoją ofiarę. Wiła, która wywabia i eliminuje swoje ofiary. Drapieżnik przy wiosce ma następującą charakterystykę:

* Nie zagraża WIOSCE. Nie jest w stanie zniszczyć wioski i nie jest jego celem zniszczenie wioski.
* Poluje na istoty dookoła wioski. Jeśli ich nie ma, poluje na istoty w wiosce.
* Ogólnie, nie zajmuje się masowym mordem. Zajmuje się mordem detalicznym, nie hurtowym.

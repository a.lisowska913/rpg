---
layout: inwazja-karta-postaci
categories: archetype
title: "Sława i popularność"
---

# {{ page.title }}

## Motywacja

### Czego pragnie a nie ma (3)

Rozpoznawalność, sława i popularność. Pokaż wszystkim dziełami i słowami, dlaczego warto Cię znać i o Tobie mówić.

### Doprecyzowanie

* Pozyskuj jak najwięcej stronników
* Promuj swoje czyny, bądź efektowny
* Próbuj osiągnąć coś wielkiego, rzadkiego
* Rób tak, by o Tobie mówili
* Działania - publiczne, wśród reflektorów

### Przykłady

* gwiazda pop

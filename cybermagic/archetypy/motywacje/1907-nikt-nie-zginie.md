---
layout: inwazja-karta-postaci
categories: archetype
title: "Nikt nie zginie"
---

# {{ page.title }}

## Motywacja

### Czego pragnie a nie ma (3)

Śmierć jest opcją. Wszystkich da się uratować - a śmierć pokonać. Pytanie, co zapłacimy. I tak warto.

### Doprecyzowanie

* ratuj wszystkich, "nie na mojej warcie"
* życie ponad człowieczeństwo
* eksperymentuj nad długowiecznością
* zbieraj rzeczy utrzymujące przy życiu
* inwestuj w badania, też niebezpieczne

### Przykłady

* lekarz ekstremista
* Ray Kurzweil

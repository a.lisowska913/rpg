---
layout: inwazja-karta-postaci
categories: archetype
title: "Perfekcja człowieczeństwa"
---

# {{ page.title }}

## Motywacja

### Czego pragnie a nie ma (3)

Nasze człowieczeństwo jest ważne. Nie cybernetyka, magia, virt czy inne światy. Ludzkie ciało, umysł, dusza - tu i teraz.

### Doprecyzowanie

* zachowuj czystość ciała i umysłu
* człowieczeństwo ponad skuteczność
* ciało ponad narzędzia czy cybernetykę
* ludzie ponad magów czy viciniusy
* zachowuj tradycję sprzed Pęknięcia

### Przykłady

* sztuki walki

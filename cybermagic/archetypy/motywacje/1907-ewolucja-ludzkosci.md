---
layout: inwazja-karta-postaci
categories: archetype
title: "Ewolucja ludzkości"
---

# {{ page.title }}

## Motywacja

### Czego pragnie a nie ma (3)

By przetrwać, musimy się zmienić. Stańmy się lepsi, przekroczmy granicę słabych ludzkich form.

### Doprecyzowanie

* Szukaj form lepszych niż zwykły człowiek
* Efektywność ponad człowieczeństwo
* Łącz magię, technikę i bioinżynierię
* Buduj akceptację dla homo superior
* Gromadź sprzęt i ludzi na laboratoria

### Przykłady

* biotech, zwolf

---
layout: inwazja-karta-postaci
categories: archetype
title: "Być punktem światła"
---

# {{ page.title }}

## Motywacja

### Czego pragnie a nie ma (3)

Ludzie się boją tego świata. Będę ich przywódcą duchowym i zapewnię im pewną drogę przez świat.

### Doprecyzowanie

* Dbaj o reputację - niezawodnego obrońcy
* Niech nikt nie zginie na Twej warcie
* Gromadź wokół siebie wyznawców, słabych
* Dawaj ludziom nadzieję i cel w życiu
* Usprawniaj i przewódź - zawsze z przodu

### Przykłady

* paladyni

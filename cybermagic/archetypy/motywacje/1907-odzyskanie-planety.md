---
layout: inwazja-karta-postaci
categories: archetype
title: "Odzyskanie planety"
---

# {{ page.title }}

## Motywacja

### Czego pragnie a nie ma (3)

Teren Skażony nie należy do ludzi - a powinien. Musimy się zaadaptować i odzyskać świat.

### Doprecyzowanie

* Eksploruj tereny Skażone dla narzędzi
* Przekształcaj anomalie w narzędzia
* Buduj przyczółki i bazy dla wszystkich
* Znajduj nowe, bezpieczniejsze drogi
* Zdobywaj wiedzę o nowym świecie

### Przykłady

* niezależni pionierzy

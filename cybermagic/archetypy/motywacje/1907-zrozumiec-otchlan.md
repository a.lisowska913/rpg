---
layout: inwazja-karta-postaci
categories: archetype
title: "Zrozumieć Otchłań"
---

# {{ page.title }}

## Motywacja

### Czego pragnie a nie ma (3)

Świat jest niezrozumiały - ale to trzeba zmienić. Jest to niebezpieczne, ale prawda i zrozumienie są najważniejsze.

### Doprecyzowanie

* Szukaj Anomalie i je badaj by zrozumieć
* Wspieraj eksperymenty i ekspedycje
* Przekonuj, że etyka jest mniej ważna
* Ryzykownie patrz w Drugą Stronę Światła
* Wybieraj wiedzę i prawdę nad wszystko

### Przykłady

* .

---
layout: inwazja-karta-postaci
categories: archetype
title: "Rozpoznawalny mistrz"
---

# {{ page.title }}

## Motywacja

### Czego pragnie a nie ma (3)

Mistrz może być tylko jeden. Najlepszy ekspert, podziwiany i na samym szczycie. Być najlepszym i pokazywać to innym.

### Doprecyzowanie

* Pokonać rywala; wykazać swoją wyższość
* Być na samym szczycie, nie ma lepszych
* Być otoczonym pomocnikami i fanami
* Mieć najlepszy sprzęt, bogactwo i szacunek
* Ciągle stawiać nowe wyzwania

### Przykłady

* Stan Pines (Gravity Falls)
* Gerd Frentzen (Blassreiter)

---
layout: inwazja-karta-postaci
categories: archetype
title: "Radością w cierpienie"
---

# {{ page.title }}

## Motywacja

### Czego pragnie a nie ma (3)

Daj radość i zapomnienie w mrocznym świecie, ukoj cierpienie i usuń rozpacz. Nadzieja i optymizm oświetlą drogę.

### Doprecyzowanie

* Spełniaj drobne marzenia, daj uśmiech
* Bądź adorowany i podziwiany
* Mediuj; niech ludzie sobie pomagają
* Nie przechodź obojętnie koło bólu
* Zwalczaj jakiekolwiek okrucieństwo

### Przykłady

* Sheryl (Macross Frontier)
* Karolina Kupiec

---
layout: inwazja-karta-postaci
categories: archetype
title: "Dołączyć do Elity"
---

# {{ page.title }}

## Motywacja

### Czego pragnie a nie ma (3)

Tamta elitarna grupa osiąga sukcesy i zawiera najlepszych z najlepszych. Chcę być jednym z nich i uznany przez nich za równego.

### Doprecyzowanie

* Żyć zgodnie i ich zasadami i kulturą
* Udowodnić im i sobie swoje umiejętności
* Być przez nich szanowanym i docenianym
* Zdobyć ich zaufanie i towarzystwo
* Pokonywać ich problemy i wrogów

### Przykłady

* Elite PvP
* Marines / Google

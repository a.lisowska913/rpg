---
layout: cybermagic-karta-postaci
categories: archetype
title: "Badacz Otchłani"
---

# {{ page.title }}

## Archetyp

### Co robi? (3)

* bada, analizuje, używa anomalii
* dzieli się rzadką, groźną wiedzą
* buduje rytuały z próbek

### Czego chce? (3)

* TAK: zrozumieć Prawdę i Otchłań
* TAK: wolność nad bezpieczeństwo
* TAK: rozwiązać Niemożliwy Problem
* NIE: odrzucić jakąkolwiek wiedzę
* NIE: człowieczeństwo nad prawdę

### Jak działa? (3)

* TAK: niebezpieczne rytuały NAD prostotę
* TAK: metodyczna praca z źródłami
* TAK: badania i konstrukcje
* NIE: proste, bezpieczne rozwiązanie
* NIE: dyplomacja lub technologia

### Zasoby i otoczenie (3)

* laboratorium, archiwa, dziwne rytuały
* zakazana wiedza i księgi
* nie do końca kontrolowane anomalie

### Co to za archetyp

Rozumie i umie pracować z niemożliwym. Pytanie, w jakim stopniu dalej jest człowiekiem. Skuteczne metody o wysokiej cenie.

### Tagi i przykłady

* SILNY: Magia, Badania, Wiedza, Anomalie
* SŁABY: Ostrożność, Walka, Gadanie
* Szalony naukowiec. Badacz wiedzy.

### Przykładowe działania

* buduje antyzaklęcie z próbek
* sprawdza w bibliotece skąd problem
* przeraża skutecznym pomysłem
* opowiada o mało znanych słabościach
* wyciąga bardzo przydatną anomalię

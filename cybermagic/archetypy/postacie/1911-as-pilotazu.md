---
layout: cybermagic-karta-postaci
categories: archetype
title: "As Pilotażu"
---

# {{ page.title }}

## Archetyp

### Co robi? (3)

* przeprowadzi pojazd przez piekło
* porwie za sobą młodzież
* wytrzyma większość ciosów

### Czego chce? (3)

* TAK: pokonać granice nad ostrożność
* TAK: efektownie imponować nad cichą skuteczność
* NIE: dyskretny, niezauważony, zapomniany

### Jak działa? (3)

* TAK: odważny, charyzmatyczny ochotnik
* TAK: pojazd i sprawność fizyczna
* NIE: dyskretnie, z drugiego planu
* NIE: cierpliwy i metodyczny

### Zasoby i otoczenie (3)

* doskonałej klasy pojazd
* podziw młodych i ryzykantów
* reputacja niebezpiecznego asa

### Co to za archetyp

Najlepszy pilot, kochający swój pojazd oraz ostrą adrenalinę. Trochę mechanik. Szybki, silny i wytrzymały - dużo przetrwa.

### Tagi i przykłady

* SILNY: Pojazd, Ciało, Brawura
* SŁABY: Deeskalowanie, Dyskrecja 
* Awanturnik, Samotny Kurier, Szeryf

### Przykładowe działania

* irytująco unika wszystkich ciosów
* niszczy Gwiazdę Śmierci myśliwcem
* podrasowuje i przesterowuje pojazd
* wda się w bójkę na pięści - i wygra
* zapali grupkę młodzieży na protest

---
layout: inwazja-karta-postaci
categories: archetype
title: "Arlekin"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

Zamaskowany artysta, akrobata i iluzjonista który wygrywa w gry hazardowe oraz doskonale włada nożem.

### Jak działa najskuteczniej / Dokonanie (3)

* dominuje: zastrasza, rozśmiesza, kłamie
* steruje uwagą: akrobacje, iluzje, noże
* wszędzie wlezie: wspinaczka, bieg
* mimikra: aktor, maskowanie, kłamstwa

### Zasoby i otoczenie (3)

* ekstrawaganckie stroje, liczne maski
* odwracacze uwagi, sprzęt do sztuczek
* żarty, anegdoty, ludzkie sekrety

## Przykłady (0)

* Maytag (Flipside)

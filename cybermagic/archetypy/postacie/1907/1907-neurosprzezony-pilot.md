---
layout: inwazja-karta-postaci
categories: archetype
title: "Neurosprzężony pilot"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

Neurowspomagany pilot Golema, zdolny do opętywania maszyn przez transfer świadomości.

### Jak działa najskuteczniej / Dokonanie (3)

* dwa ciała: umysł człowieka, ciało Golema
* sterowanie: Golem, sterowanie maszynami
* programowanie: zmiana psychotroniki
* naprawa: Golem, maszyny, konstruktor

### Zasoby i otoczenie (3)

* Golem - neurowspomagana maszyna z TAI
* baza ze sprzętem, personel pomocniczy

## Przykłady (0)

* Guld (Macross Plus)
* Scorponok (TF)

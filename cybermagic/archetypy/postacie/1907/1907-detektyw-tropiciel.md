---
layout: inwazja-karta-postaci
categories: archetype
title: "Detektyw Tropiciel"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

Detektyw, który niezależnie od medium zawsze odnajdzie swój cel.

### Jak działa najskuteczniej / Dokonanie (3)

* wyciąga prawdę: przesłuchanie, zagadki
* tropi: ślady, skrada się, w komputerze
* przeszukuje: komputery, dokumenty, plotki
* ściga: biega, skrada się, samochody

### Zasoby i otoczenie (3)

* informatorzy, znajomi w virt
* dostęp do dobrego laboratorium śledczego
* wiedza o przeszłych sprawach

## Przykłady (0)

* Ultra Magnus (TF)
* Poirot

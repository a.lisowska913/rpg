---
layout: inwazja-karta-postaci
categories: archetype
title: "Cyberszturmowiec"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

Wspomagany żołnierz przełamywania frontu, terminator.

### Jak działa najskuteczniej / Dokonanie (3)

* walczy: walka, refleks, siła, odporność
* szarżuje: biegnie, używa ciężkiego sprzętu
* dominuje: onieśmiela, inspiruje, straszy
* regeneruje: prowizorka, pierwsza pomoc

### Zasoby i otoczenie (3)

* ciężki servar i ciężki sprzęt bojowy
* kilku zaufanych towarzyszy broni
* reputacja elitarnego oddziału

## Przykłady (0)

* terminator, neurowspomagany marine

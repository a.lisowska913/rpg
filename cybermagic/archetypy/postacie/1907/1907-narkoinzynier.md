---
layout: inwazja-karta-postaci
categories: archetype
title: "Narkoinżynier"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

Biokonstruktor wzmacniający wszystkich odpowiednimi środkami. Ekspert od biochemii i interakcji.

### Jak działa najskuteczniej / Dokonanie (3)

* dopinguje: adaptuje środki, wzmacnia
* neutralizuje: złość, radość, sen...
* bada: szuka anomalii, żywe istoty
* leczy: pierwsza pomoc, przeciąża cel

### Zasoby i otoczenie (3)

* psychoaktywne substancje, biolab
* aerozole i gazy, kombinezon ochronny
* znajomości w półświatku lub militarne

## Przykłady (0)

* Karolina Kupiec (Inwazja)
* Splicers (Batman Beyond)

---
layout: inwazja-karta-postaci
categories: archetype
title: "Lekarz frontowy"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

Lekarz w ogniu problemu - czy Plaga, czy walki. Agent szybkiego reagowania.

### Jak działa najskuteczniej / Dokonanie (3)

* leczy: pierwsza pomoc, sprzęt medyczny
* bada: plagi, eksperymenty, laboratorium
* dowodzi: autorytet, posłuch, niezłomny
* przetrwa: wiedza, twardy pionier, biega

### Zasoby i otoczenie (3)

* podziwiany; legendarna reputacja
* zaawansowany sprzęt medyczny i ochronny
* czarna technologia, zakazana technika
* sponsorzy, ludzie którym pomógł

## Przykłady (0)

* lekarze bez granic
* ojciec ratujący córkę za cenę duszy

---
layout: inwazja-karta-postaci
categories: archetype
title: "Wyścigowiec"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

Miłośnik wyścigów i dopakowywania swojego ścigacza; na krawędzi prawa.

### Jak działa najskuteczniej / Dokonanie (3)

* imponuje: szalony, prowokuje, reakcje
* ściga się: manewry, wyprzedza, szybki
* naprawia: przebudowa, włam do pojazdów

### Zasoby i otoczenie (3)

* wybitny ścigacz, odporniejszy i szybszy
* grupa fanów, świetni rywale, reputacja
* sprzęt do włamań

## Przykłady (0)

* Initial D
* Gerd Fretzen (Blassreiter)

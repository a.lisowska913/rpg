---
layout: inwazja-karta-postaci
categories: archetype
title: "Przewodnik po Skażeniu"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

Przewodnik przetrwa wszędzie i zna drogi - szybsze i bezpieczniejsze.

### Jak działa najskuteczniej / Dokonanie (3)

* skróty: zna drogi, omija problemy
* wiedza o Terenie: istoty, cechy, anomalie
* pułapki: zastawia, unika, maskuje
* przetrwanie: schronienia, ukrywanie się

### Zasoby i otoczenie (3)

* mapy Terenu; o różnej aktualności
* kryjówki na Terenie
* gadżety do przetrwania

## Przykłady (0)

* Stalker (Stalker)
* Hwaryun (Tower of God)

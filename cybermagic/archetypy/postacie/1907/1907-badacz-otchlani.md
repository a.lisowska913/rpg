---
layout: inwazja-karta-postaci
categories: archetype
title: "Badacz otchłani"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

Naukowiec szukający Prawdy, starający się nie zatracić siebie.

### Jak działa najskuteczniej / Dokonanie (3)

* odkrywa: analiza, okultyzm, laboratorium
* szuka w: dokumenty, komputery, legendy
* buduje: rytuały, prototypy, skanery

### Zasoby i otoczenie (3)

* laboratorium, archiwa, dziwne rytuały
* sprzęt skanujący, legendy, historie
* zniewolone istoty Eteru
* opinia niebezpiecznego i skutecznego

## Przykłady (0)

* Faust

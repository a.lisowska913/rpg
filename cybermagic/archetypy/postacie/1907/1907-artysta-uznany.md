---
layout: inwazja-karta-postaci
categories: archetype
title: "Artysta, uznany"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

Popularny artysta, otoczony wianuszkiem i podziwem

### Jak działa najskuteczniej / Dokonanie (3)

* inspiruje: dziełami, przyciąga uwagę
* zauracza: wywołuje potężne uczucie
* przekonuje: tworząc dzieło, reputacją
* manipuluje: aktorstwo, urok

### Zasoby i otoczenie (3)

* stworzone dzieła: barter lub dla efektu
* bogaty: większość rozwiąże pieniędzmi
* stroje i akcesoria performera

## Przykłady (0)

* Issei (SwordGai)
* Sheryl / Ranka (Macross Frontier)

---
layout: inwazja-karta-postaci
categories: archetype
title: "Artefaktor Puryfikacyjny"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

Konstruktor polujący na anomalie i ujarzmiający je w służbie ludzkości.

### Jak działa najskuteczniej / Dokonanie (3)

* łowi anomalie: znajduje, unieszkodliwia
* kontroluje: technomagia, słabości, pęta
* dominuje: zastrasza, rozkazuje, kłamie
* buduje: artefakcja, pułapki, antymagia

### Zasoby i otoczenie (3)

* lapis i narzędzia neutralizacji magii
* srebro i narzędzia krzywdzenia magii
* sprzęt do artefakcji i budowania rzeczy

## Przykłady (0)

* Grace Hall (Dents)

---
layout: inwazja-karta-postaci
categories: archetype
title: "Detektyw Tropiciel"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* detektyw który zawsze odnajdzie swój cel

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)

* dla mnie: każdego umiem znaleźć, rozwiążę zagadkę
* dla innych: świat bez przemocy, ład i porządek

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* przesłuchiwanie, wyciąganie informacji, plotki
* tropienie śladów, tropienie w virt, dokumenty
* skradanie się, przewidywanie ruchów, zagadki

### Zasoby i otoczenie (3)

* sieci w półświatku, sieci w virt, informatorzy
* poszerzeone uprawnienia

## Opis

Przykłady:

* Ultra Magnus (TF)
* Poirot

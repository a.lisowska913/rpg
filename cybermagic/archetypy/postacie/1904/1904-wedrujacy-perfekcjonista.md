---
layout: inwazja-karta-postaci
categories: archetype
title: "Wędrujący Perfekcjonista"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* Podróżnik, specjalizujący się w Czymś, który podróżuje po świecie by być w tym Czymś coraz lepszym.

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)
 
* chcę być coraz lepszy w Czymś, chcę trudnych wyzwań, chcę się wykazać
* chcę być uznany za najlepszego na świecie, chcę by inni docenili mistrzostwo Czegoś

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* używam Czegoś jeśli to tylko możliwe, nie unikam żadnych wyzwań

### Zasoby i otoczenie (3)

* sprzęt oraz wszelkie przedmioty potrzebne do Czegoś - najwyższej jakości
* sieć znajomych oraz rywali; wszyscy skupieni dookoła Czegoś
* reputacja absolutnego eksperta odnośnie Czegoś

## Opis

Przykłady:

* Ryu ze Street Fighter Animated

---
layout: inwazja-karta-postaci
categories: archetype
title: "Kustosz"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* Bardzo próbuje zachować stan Czegoś. Widzi w tym piękno i będzie tego chronić i dbać o poszerzanie o tym wiedzy.

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)
 
* chcę poszerzać swoją wiedzę o Czymś, chcę obcować z Czymś, chcę Coś chronić przed innymi
* chcę by inni zobaczyli w Czymś piękno, chcę by wiedza o Czymś się rozprzestrzeniała jak najdalej

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* opowieści, edukowanie, przekonywanie, pokazywanie piękna i znajdowanie skutecznych analogii przez Coś
* doskonałe działanie w obszarze Czegoś, konstruowanie przy użyciu / w obszarze Czegoś

### Zasoby i otoczenie (3)

* ma dostęp do Czegoś - porusza się w tym temacie płynnie i wybitnie. Tak samo zna innych pasjonatów Czegoś.

## Opis

Przykłady:

* Obrońca przyrody, dbający o zachowanie natury nietkniętej przez człowieka.
* Ostatni GM w upadającym MMO

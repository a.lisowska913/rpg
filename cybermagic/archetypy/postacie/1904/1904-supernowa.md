---
layout: inwazja-karta-postaci
categories: archetype
title: "Supernowa"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* Supernowa. Wulkan energii. Uznawana za niepowstrzymaną - czystym entuzjazmem i nadmiarem energii rozwiązuje swoje i cudze problemy.

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)
 
* Wiecznie w pogoni za impulsami, uczuciami i ciągłym działaniem. Ciekawska i ekstremalna. Zawsze musi być na forpoczcie, zawsze coś próbować i sprawdzać.
* Nikt nie powinien się ograniczać. Niech inni też żyją pełnią życia, niech odważą się żyć! Niech walczą o swoje z systemem... ze wszystkim!

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* Rzuca nieskończoną, niepowstrzymaną energię i entuzjazm na problem. Niemożliwa do zdemotywowania. Nie da się jej zmęczyć.
* Motywuje wszystkich dookoła, przejmuje dowodzenie, nieskończony optymizm. Nie ma rzeczy niemożliwych. Niezłomna.

### Zasoby i otoczenie (3)

* Zbiór dziwnych, nieobliczalnych gadżetów typu "czemu Ty masz coś takiego... w sumie, czemu pytam..."
* Zbiór znajomych, przyjaciół, osób, którym już kiedyś pomogła, zebranych przysług itp.

## Opis

Przykłady:

* "Head over Heels" Abby; Supernowa Diakon (trope namer)
* Szajba ze "Złota dla Zuchwałych"
* Elementy tego widać w "Tsundere" w anime, zwłaszcza w fazie 'tsun'

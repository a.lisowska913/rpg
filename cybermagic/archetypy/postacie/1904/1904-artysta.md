---
layout: inwazja-karta-postaci
categories: archetype
title: "template_name"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* Artysta to twórcza natura. Nie znosi pustki, musi ją wypełnić pięknem. 

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)
 
* Stworzyć piękno, pokazać je całemu światu lub wręcz przeciwnie, zachować dla godnej publiczności.
* Artysta pragnie, by jego sztuka została doceniona.

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* Perfekcjonista, dostrzeże każdy szczegół, bo jeden włos, jedna linia nie na miejscu zniszczy doskonałość dzieła.
* Potrafi doskonale skupić się na tworzeniu dzieła, ma oko do szczegółów.
* Doskonale czyta ludzkie emocje, choć nie zawsze je rozumie.

### Zasoby i otoczenie (3)

* Narzędzia twórcze, materiały zależne od rodzaju sztuki.
* Zna mnóstwo innych artystów i może liczyć na ich wsparcie

## Opis



Przykłady:

* .
* .

---
layout: inwazja-karta-postaci
categories: archetype
title: "Agent Kryzysowy"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* agent szybkiego reagowania, zwiadowca oraz terminus

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)

* dla mnie: podziw i szacunek, awans, działanie nad planowanie
* dla innych: minimalizacja zniszczeń, bezpieczeństwo

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* szybki, pierwsza pomoc, strzelec
* zwiad dronami, skradanie się, akrobacje, ucieczka
* przejmowanie dowodzenia, wymuszanie współpracy

### Zasoby i otoczenie (3)

* szybki ścigacz, sprzęt medyczny, dobra broń
* lekki pancerz, drony zwiadowcze
* wsparcie artylerii i ciężkiego oddziału

## Opis

Przykłady:

* Iria

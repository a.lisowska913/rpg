---
layout: inwazja-karta-postaci
categories: archetype
title: "Tyran Wojownik"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* Straszliwy wojownik w walce i charyzmatyczny przywódca rządzący terrorem. Podziwiany i wzbudzający strach.

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)
 
* Chce władzy, potęgi i mocy - które przekierowuje w jeszcze więcej władzy, potęgi i mocy. Obsesja potęgi.
* Chce stworzyć pod swoją egidą imperium kontrolowane jego wolą. Pokój przez tyranię.

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* Zastraszanie, terror, arogancja. Przejmuje dowodzenie i skutecznie dowodzi swoimi ludźmi.
* Bezpośrednia, brutalna walka i otwarta konfrontacja. Czasem przechytrzenie innych - jest też dobrym taktykiem.

### Zasoby i otoczenie (3)

* Ma swoich zwolenników i stronników - osoby wykonujące jego polecenia.
* Doskonałej klasy sprzęt lub inne rzeczy pozwalające mu na zwyciężanie.

## Opis

Przykłady:

* Megatron (inkarnacje z Frankiem Welkerem)
* Darkseid (Superman)

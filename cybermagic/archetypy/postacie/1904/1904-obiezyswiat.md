---
layout: inwazja-karta-postaci
categories: archetype
title: "Obieżyświat"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* Niespokojna dusza, która nigdzie nie zagrzewa długo miejsca. Cały dobytek ma zawsze przy sobie. Zna różne kultury, wiele widział. Gdziekolwiek dotrze, nawiązuje przyjaźnie

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)
 
* Wolność i swoboda ruchu to dla obieżyświata wartości nadrzędne.
* Szacunek dla odmiennych kultur i konieczność ich zachowania to silna motywacja - często tylko szacunek uchronił obieżyświata przed popełnieniem głupot.

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* Przetrwa w każdych warunkach.
* Łatwo integruje się z otoczeniem.

### Zasoby i otoczenie (3)

* Wygodny plecak z całym dobytkiem
* Instrument
* Mnóstwo znajomości na trasie wędrówki.

## Opis

Przykłady:

* .
* .

---
layout: inwazja-karta-postaci
categories: archetype
title: "Zbyt Genialny Mag"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* mag o zbyt dużej mocy, próbujący utrzymać kontrolę

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)

* dla mnie: nie krzywdzić innych, być częścią grupy
* dla innych: specjalnego traktowania, szacunek dla talentu

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* magia adaptuje się do problemu, nie kontroluje swej magii
* rozwiązuje zagadki i łamigłówki, widzi połączenia

### Zasoby i otoczenie (3)

* reputacja straszliwego i niestabilnego
* spontanicznie tworzy anomalie
* chętni do bycia uczniami

## Opis

Przykłady:

* Suspiria (Flipside)

---
layout: inwazja-karta-postaci
categories: archetype
title: "Badacz"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* Badacz potrafi zgłębić naturę rzeczy. Sekrety, zagadki i ukryte przejścia nie są dlań problemem.

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)
 
* Wiedza i eksploracja. Musi zrozumieć co się dzieje, poznać prawdę. Chce też wsadzić palec w każde niezbadane drzwi.
* Więcej wiedzy dostępnej dla innych! Im więcej wiedzy, tym lepiej inni przełożą to na działania... w końcu.

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* Doskonała percepcja. Wszystko zauważa, zwraca uwagę na każdy szczegół. Świetnie rozwiązuje zagadki i łamigłówki.
* Bardzo szybko potrafi znaleźć dane w archiwach czy komputerach - między archiwami a problemem do rozwiązania

### Zasoby i otoczenie (3)

* Mnóstwo książek, komputerów i kontaktów związanych z wiedzą, na temat głównej domeny i tematy z nią powiązane.
* Przenośne laboratorium, odczynniki i sprzęt służący do badań na bieżąco.

## Opis

Przykłady:

* Indiana Jones / Lara Croft (+ daredevil)
* Andżelika Leszczyńska

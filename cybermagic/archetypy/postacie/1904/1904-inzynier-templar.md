---
layout: inwazja-karta-postaci
categories: archetype
title: "Inżynier Templariusz"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* Inżynier, konstruktor - do tego ekstremista wierzący w Sprawę. Konstruuje innowacyjne dzieła i nie boi się terroryzować nimi innych.

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)
 
* Zbudować jak najlepsze dzieła. Jest oddany zarówno Sprawie jak i Konstrukcji.
* Ma swoją Sprawę. Wszyscy inni muszą zrozumieć, że to jest jedyna możliwa droga - albo jego dzieła zniszczą wszystkie inne opcje.

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* Nadzoruje działanie swoich dzieł, ale zostawia bezpośrednie działanie im. Działa przy ich użyciu.
* Konstruktor i inżynier w swojej domenie, bardzo dobrej klasy. W obszarze adaptacji i badań - świetny.

### Zasoby i otoczenie (3)

* Dzieła powiązane ze swoim obszarem konstrukcji. Odpowiednio różnorodne i liczne.
* Inne osoby oddane jego Sprawie. Miniony, agenci, sojusznicy.

## Opis

Przykłady:

* Doctor Viper ("Swat Kats")
* Mister Golden ("Last Hope")
* Poison Ivy (stara wersja, z roślinami) ("Batman")
* Isaac ("Castlevania")

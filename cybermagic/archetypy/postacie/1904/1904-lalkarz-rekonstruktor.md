---
layout: inwazja-karta-postaci
categories: archetype
title: "Lalkarz Rekonstruktor"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* Rekonstruktor ciała i umysłu, który pragnie absolutnej kontroli nad swoimi ofiarami. Guru sekty.

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)
 
* dla mnie: przywódca kultu, władza, absolutna kontrola i dominacja nad innymi
* dla innych: pokazać im w jakim świecie żyją, oddać ich pod kontrolę Lalkarza, odmienić ich dla swego ideału

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* kontrola umysłów, manipulacja, uzależnianie, przebudowa ciała i umysłu
* wzbudzanie terroru i grozy, działanie kultystami i agentami, łamanie woli

### Zasoby i otoczenie (3)

* narzędzia rekonstrukcji i uzależnienia
* kultyści i agenci, naśladowcy chcący być tacy jak on, kult

## Opis

Przykłady:

* .

---
layout: inwazja-karta-postaci
categories: archetype
title: "Hellracer"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* wyścigowiec żyjący swoim pojazdem i dążący do bycia championem

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)

* dla mnie: być championem, być najszybszym i podziwianym, epicki ścigacz
* dla innych: godni rywale, docenianie wybitności

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* szalona prędkość, niemożliwe manewry, robi wrażenie
* adaptuje ścigacz, konstrukcja ze złomu, mechanika

### Zasoby i otoczenie (3)

* wybitny ścigacz, modyfikatory ścigacza
* wsparcie mechanika, baza z gratami
* grupa fanów, świetni rywale

## Opis

Przykłady:

* Initial D

---
layout: inwazja-karta-postaci
categories: archetype
title: "Kustosz"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* oczytany ekspert propagujący wiedzę i pamięć o Czymś

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)

* dla mnie: wiedzieć więcej o Czymś, chronić Coś przed innymi
* dla innych: pokazać piękno Czegoś, zarazić o Czymś pasją

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* opowieści, zauroczenie, pokazanie znaczenia
* komputery, archiwa, zbieranie informacji
* ekspert w dziedzinie Czegoś

### Zasoby i otoczenie (3)

* nietypowe artefakty Czegoś
* sieć znajomości z pasjonatami Czegoś
* ogromne składowisko wiedzy i materiałów

## Opis

Przykłady:

* Obrońca przyrody, dbający o zachowanie natury nietkniętej przez człowieka.
* Ostatni GM w upadającym MMO

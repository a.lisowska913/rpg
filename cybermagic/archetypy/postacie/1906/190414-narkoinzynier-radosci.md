---
layout: inwazja-karta-postaci
categories: archetype
title: "Narkoinżynier Radości"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* biokonstruktor żyjący w radości i pragnący ją rozprzestrzenić

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)

* dla mnie: nowe przeżycia, wieczny postęp naukowy
* dla innych: zero bólu i cierpienia, maksymalizacja radości

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* obezwładnianie radością, narkotyki, odporność biologiczna
* uzależnianie, łatwość kontaktów, nieskończony optymizm
* świat pragnień, narkoiluzje

### Zasoby i otoczenie (3)

* psychoaktywne substancje, biolab, anarchiści
* aerozole i gazy, kombinezon ochronny

## Opis

Przykłady:

* Karolina Kupiec (Inwazja)

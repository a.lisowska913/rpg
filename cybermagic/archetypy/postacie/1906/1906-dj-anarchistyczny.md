---
layout: inwazja-karta-postaci
categories: archetype
title: "DJ, anarchistyczny"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

Anarchistyczny DJ z podziemnego klubu; świetny z komputerami

### Czego pragnie a nie ma (3)

* pełna wolność; zdjąć wszystkim kajdany
* pomagać; wszyscy jesteśmy ludźmi
* wpływ i popularność; autentyczne sprawy

### Jak działa najskuteczniej (3)

* kontrola tłumów: dowodzi, retoryka
* walka uliczna: bieg, walka wręcz, ukrycie
* sprzęt: zdalna kontrola, łamanie haseł

### Zasoby i otoczenie (3)

* zacna ekipa: włamy, kradzieże, pobicia
* fani: młodzi ludzie z różnych sfer
* sprzęt: nagrania, ale też drony

## Opis

Przykłady:

* Dracena Diakon

---
layout: inwazja-karta-postaci
categories: archetype
title: "Idealistyczny Pionier"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* dążący do niezależności twardy kowboj pogranicza

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)

* dla mnie: swoboda, wolność wyboru, zaufanie od innych
* dla innych: wspólny acz luźny front przeciwko anomaliom i zagrożeniu

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* pięści i skuteczna broń; szybki i zwinny
* szybko mówi, umie się wygadać z problemów
* survival i sztuka przetrwania, niezależny

### Zasoby i otoczenie (3)

* sprzęt do przetrwania samemu
* różne kryjówki, drogi ucieczki itp.
* znajomości wśród podobnych sobie niezależnych dziwaków

## Opis

Przykłady:

* Spike (Cowboy Bebop)
* Santiago (Santiago)

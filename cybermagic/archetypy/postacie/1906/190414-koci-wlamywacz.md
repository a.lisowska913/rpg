---
layout: inwazja-karta-postaci
categories: archetype
title: "Koci Włamywacz"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* mistrz akrobacji odwracający uwagę i dostający się gdzie chce

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)

* dla mnie: wyzwanie - włamie się wszędzie
* dla innych: słabi nie są uciemiężeni, wyrównanie szans

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* szybkie i ciche poruszanie się
* akrobacje, wspinaczka, włamywacz
* odwracanie uwagi, urok osobisty

### Zasoby i otoczenie (3)

* mapy, wytrychy, sprzęt do włamań
* granaty oślepiające, haki do włażenia
* stroje maskujące, stroje eleganckie

## Opis

Przykłady:

* Catwoman (Batman)
* Arsene Lupin (Castle Cagliostro)

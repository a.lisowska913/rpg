---
layout: inwazja-karta-postaci
categories: archetype
title: "Bard"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* muzyk i mędrzec, podróżujący gawędziarz zbierający opowieści

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)

* dla mnie: odkrywanie prawdy, znajdowanie opowieści
* dla innych: swobodna wymiana wiedzy, ludzie przeciw Skażeniu

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* opowieści, budowanie wrażenia, muzyka i poezja
* przyjazne kontakty, rozładowanie atmosfery
* pierwsza pomoc, szeroka wiedza, szyfry

### Zasoby i otoczenie (3)

* anegdoty, pasujące opowieści, plotki
* kto jest kim, wszędzie kogoś zna
* instrument muzyczny, ogromna wiedza

## Opis

Przykłady:

* Harfiarze (Pern)

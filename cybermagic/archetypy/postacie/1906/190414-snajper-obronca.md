---
layout: inwazja-karta-postaci
categories: archetype
title: "Snajper Obrońca"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* niewidzialny snajper i specjalista pozycjonowania chroniący swoich

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)

* dla mnie: cenny członek zespołu, wiedzieć o każdym sekrecie
* dla innych: bezpieczeństwo swoich, brak przymusu, ludzie dobrzy dla ludzi

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* skradanie się, taktyka, dobre pozycjonowanie
* broń snajperska, pułapki, materiały wybuchowe
* podsłuchiwanie, plotki

### Zasoby i otoczenie (3)

* sprzęt maskujący, karabin snajperski
* sporo informacji na temat wszystkich dookoła

## Opis

Przykłady:

* Ghost (Starcraft)

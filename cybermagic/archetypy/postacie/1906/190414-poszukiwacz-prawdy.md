---
layout: inwazja-karta-postaci
categories: archetype
title: "Poszukiwacz Prawdy"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* poszukiwacz Prawdy, który wie, że to ONI za TYM stoją

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)

* dla mnie: musi poznać Prawdę, wolność oraz swoboda
* dla innych: dać im prawo wyboru, mają prawo się dowiedzieć

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* drobiazgowy, znajduje dziwne powiązania, świetny detektyw
* potrafi rozmawiać z ludźmi, dobry dziennikarz
* włamie się, schowa czy przemknie w cieniach

### Zasoby i otoczenie (3)

* darknetowe fora konspiracyjne, kontakty w półświatku
* sprzęt śledczy, podsłuchowy, detektywistyczny
* "archiwum X" - sprawy niewyjaśnione i dziwne

## Opis

Przykłady:

* Mulder (X-Files)

---
layout: inwazja-karta-postaci
categories: archetype
title: "Bioadapter Kryzysowy"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* bioinżynier uważający, że na wszystko jest odpowiednia mutacja

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)

* dla mnie: podziw dla swej sztuki, mieć idealne ciało
* dla innych: na każdy problem mutacja, człowieczeństwo przereklamowane

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* szybki silny i wytrzymały, znajomość ludzkiego ciała
* biomodyfikacje, biowspomagania, synteza biowzmacniaczy

### Zasoby i otoczenie (3)

* narkotyki, biowspomagania i mutageny
* niezły biolab z próbkami
* znajomości w półświatku lub militarne

## Opis

Przykłady:

* Splicers (Batman Beyond)

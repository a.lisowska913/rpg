---
layout: inwazja-karta-postaci
categories: archetype
title: "Cyberszturmowiec"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

Wspomagany żołnierz przełamywania frontu, terminator.

### Czego pragnie a nie ma (3)

* siła i moc; być skuteczniejszym, sprzęt
* pomóc swoim; tylko on może ich ratować
* przynależność; elitarna grupa z tradycją

### Jak działa najskuteczniej (3)

* walka: walka, refleks, siła, odporność
* dominacja: onieśmiela, inspiruje, straszy
* dewastacja: broń ciężka, sprzęt ciężki

### Zasoby i otoczenie (3)

* ciężki servar i ciężki sprzęt bojowy
* kilku zaufanych towarzyszy broni
* reputacja elitarnego oddziału

## Opis

Przykłady:

* terminator, neurowspomagany marine

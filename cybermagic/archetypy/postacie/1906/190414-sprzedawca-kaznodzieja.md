---
layout: inwazja-karta-postaci
categories: archetype
title: "Sprzedawca Kaznodzieja"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* pomaga rzeszom ludzi by dawali mu pieniądze

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)

* dla mnie: wpływ na masy, luksus i złoto, uznany za eksperta
* dla innych: sukcesy dla popleczników, moi ludzie nad innymi

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* porywa tłumy, wygrywa debaty, bardzo przekonywujący
* buduje produkty, sprzedaje produkty, przyciąga uwagę

### Zasoby i otoczenie (3)

* różne produkty na sprzedaż
* fani i hejterzy
* znajomości wśród influencerów

## Opis

Przykłady:

* Tim Ferris ;-)

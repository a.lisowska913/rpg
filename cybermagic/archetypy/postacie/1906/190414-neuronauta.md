---
layout: inwazja-karta-postaci
categories: archetype
title: "Neuronauta"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* nurek w umysłach naprawiający psychikę i AI

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)

* dla mnie: pomóc innym, dostęp i akceptacja neuroedycji
* dla innych: stabilne społeczeństwo, druga szansa

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* budowanie i naprawa psychotroniki i umysłu
* wnikanie w pamięć, blokady i przymusy, hipnoza
* wrażenie niegrożnego, świetny rozmówca

### Zasoby i otoczenie (3)

* edytory psychotroniki, edytory pamięci
* ochrona i wsparcie, połączenie z virtem

## Opis

Przykłady:

* .

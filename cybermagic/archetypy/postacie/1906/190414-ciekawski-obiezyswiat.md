---
layout: inwazja-karta-postaci
categories: archetype
title: "Ciekawski Obieżyświat"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* włóczykij wiele widział; zwiedza świat licząc na siebie

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)

* dla mnie: poznawać nowe rzeczy, nawiązywać nowe znajomości
* dla innych: akceptacja odmienności, poszanowanie wolności

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* uważny, zauważa szczegóły, wyczuje zagrożenie
* dobrze się dogaduje, świetne opowieści, gra na instrumencie
* złota rączka, pierwsza pomoc, survival, twardy

### Zasoby i otoczenie (3)

* niesamowite historie i anegdoty
* porządny pojazd, sprzęt naprawczy, żywność w puszkach
* cały dobytek na plecach, instrument muzyczny

## Opis

Przykłady:

* Włóczykij (Muminki)
* It's my life (Dr Alban)

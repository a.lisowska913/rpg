---
layout: inwazja-karta-postaci
categories: archetype
title: "Lekarz Frontowy"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* lekarz działający na froncie, czy to plaga czy podczas walki

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)

* dla mnie: pomagać ludziom, daleko od polityki
* dla innych: dać im szansę, minimalizować cierpienie

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* pierwsza pomoc, leczenie, maszyny medyczne
* autorytet, przejmowanie kontroli, niezłomny
* badania, eksperymenty, plagi, survival

### Zasoby i otoczenie (3)

* podziwiany; legendarna reputacja
* zaawansowany sprzęt medyczny i ochronny
* sponsorzy i osoby, którym pomógł

## Opis

Przykłady:

* lekarze bez granic

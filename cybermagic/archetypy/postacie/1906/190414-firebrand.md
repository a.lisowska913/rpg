---
layout: inwazja-karta-postaci
categories: archetype
title: "Firebrand"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* podżegacz, symbol który rozpoczyna rewolucję na pierwszej linii

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)

* dla mnie: chronić i wspierać Sprawę, być docenianym członkiem Sprawy
* dla innych: zdobyć poparcie dla Sprawy, zapalić rewolucję

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* porywanie tłumów, przekonywanie, budowanie jedności
* retoryka, debatowanie, wymuszanie przyznania się
* przyciąganie uwagi, nawracanie, niezłomność

### Zasoby i otoczenie (3)

* grupa fanów i wyznawców
* liczne dowody i fakty
* naśladowcy wierzący w ideę

## Opis

Przykłady:

* V (V for Vendetta)
* Joker (Gotham; z naśladującymi)
* Kudelia (Gundam Iron Blooded Orphans)

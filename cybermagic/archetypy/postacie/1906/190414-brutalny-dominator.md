---
layout: inwazja-karta-postaci
categories: archetype
title: "Brutalny Dominator"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* straszliwy wojownik łamiący wolę słabszych od siebie

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)

* dla mnie: luksus i niewolnicy, absolutna władza
* dla innych: silny zjada słabego, ja na piedestale

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* straszliwy w walce, wzbudza strach, zawsze ma dokąd uciec
* łamanie woli, tortury, wymuszanie posłuszeństwa, szantaż
* pokazowe okrucieństwo, niezłomny

### Zasoby i otoczenie (3)

* narzędzia tortur i zniewolenia, słudzy i niewolnicy
* reputacja wymuszająca posłuch

## Opis

Przykłady:

* Semirhage (Koło Czasu Jordana)
* Vogle (Flipside)

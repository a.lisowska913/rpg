---
layout: inwazja-karta-postaci
categories: archetype
title: "Plotkarz, wścibski"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

Szpieg podsłuchujący sekrety, kontrolujący plotki i narrację.

### Czego pragnie a nie ma (3)

* poznać sekrety; alergia na tajemnice
* strach lub szacunek; nie być ignorowanym
* władza; na każdego mieć skutecznego haka

### Jak działa najskuteczniej (3)

* odkrywanie: podsłuch, oczarowanie, plotki
* dominacja: szantaż, niszczenie opinii
* manipulacja: odwrócenie uwagi, fake news
* sprzęt: podsłuchy, drony, łamanie haseł

### Zasoby i otoczenie (3)

* cywilny sprzęt szpiegowski i drony
* ma coś na każdego: sekrecik czy szantaż
* ludzie, którymi buduje/zbiera plotki

## Opis

Przykłady:

* Jerzy Marduk

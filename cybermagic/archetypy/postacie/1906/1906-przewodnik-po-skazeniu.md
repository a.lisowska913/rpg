---
layout: inwazja-karta-postaci
categories: archetype
title: "Przewodnik po Skażeniu"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

Przewodnik zna drogi - szybsze i bezpieczniejsze. Przetrwa wszędzie.

### Czego pragnie a nie ma (3)

* Eksploracja; nowe drogi, jak najdalej
* Reputacja; niezawodny i najlepszy
* Bezpieczeństwo; dla mnie i podopiecznych

### Jak działa najskuteczniej (3)

* skróty: zna drogi, omija problemy
* wiedza o Terenie: istoty, cechy, anomalie
* pułapki: zastawia, unika, maskuje
* przetrwanie: schronienia, ukrywanie się

### Zasoby i otoczenie (3)

* mapy Terenu; o różnej aktualności
* kryjówki na Terenie 
* gadżety do przetrwania

## Opis

Przykłady:

* Stalker (Stalker)
* Hwaryun (Tower of God)

---
layout: inwazja-karta-postaci
categories: archetype
title: "Upiór Virtu"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* istota żyjąca w vircie która pragnie mieć ciało materialne

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)

* dla mnie: zdobyć stałe ciało, być częścią świata
* dla innych: piękno virt, połączyć oba światy

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* w virt nie ma sobie równych, walka w virt
* przemieszcza się w virt, tworzy agentów
* zakłóca komunikację, opętuje maszyny

### Zasoby i otoczenie (3)

* virtsprzężone urządzenia, znajomi w virt
* agenci wśród maszyn

## Opis

Przykłady:

* GitS

---
layout: inwazja-karta-postaci
categories: archetype
title: "Cinkciarz"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

Drobny fałszerz i świetny manipulator; sprzeda fałszywkę i zwieje.

### Czego pragnie a nie ma (3)

* mistrzostwo; oszukuje dla sportu i frajdy 
* bogactwo i szacunek; osiągnąć stabilność
* pozycja; mieć pomocników i podwładnych

### Jak działa najskuteczniej (3)

* manipulacja: aktorstwo, bajerowanie
* przemyt: odwracanie uwagi, chowanie
* fałszerstwo: dowody, pozłota, wycena
* przetrwanie: bieganie, pałka, kryjówki

### Zasoby i otoczenie (3)

* rzeczy, pozornie drogie i wartościowe
* znajomości w półświatku i w policji
* kryjówki i sprzęt do ucieczki

## Opis

Przykłady:

* Stan Pines (Gravity Falls)

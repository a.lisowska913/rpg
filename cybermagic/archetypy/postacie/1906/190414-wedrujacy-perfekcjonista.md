---
layout: inwazja-karta-postaci
categories: archetype
title: "Wędrujący Perfekcjonista"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* podróżny mistrz Czegoś szukający wyzwań, by być arcymistrzem

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)

* dla mnie: doprowadzić Coś do perfekcji, wykazać się
* dla innych: być uznanym za najlepszego na świecie, propagować Coś

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* używa Czegoś jeśli to możliwe
* niezłomny i inspirujący, przeraża wrogów

### Zasoby i otoczenie (3)

* wszystko potrzebne do Czegoś, najwyższej jakości
* sieć znajomych oraz rywali
* szeroka reputacja wybitnego mistrza

## Opis

Przykłady:

* Ryu (Street Fighter Animated)

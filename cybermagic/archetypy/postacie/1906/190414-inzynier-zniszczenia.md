---
layout: inwazja-karta-postaci
categories: archetype
title: "Inżynier Zniszczenia"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* optymalizator specjalizujący się w precyzyjnej destrukcji

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)

* dla mnie: znajdować tańsze i silniejsze środki, nie zadawać bólu
* dla innych: minimalizacja szkód, chronić przed problemami

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* precyzyjna destrukcja, przebijanie barier
* naukowiec, badacz i konstruktor
* broń superciężka, artyleria, źródła energii

### Zasoby i otoczenie (3)

* warsztat testowania i optymalizacji broni
* liczne prototypy broni, pancerzy, materiałów

## Opis

Przykłady:

* .

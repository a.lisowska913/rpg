---
layout: inwazja-karta-postaci
categories: archetype
title: "Aspirująca gwiazda"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* entuzjastyczna gwiazda pop dająca radość i motywująca innych

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)

* dla mnie: adorowana, podziwiana, coraz więcej fanów
* dla innych: radość i zapomnienie, spełnienie marzeń fanów

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* charyzma, wzbudzanie uczuć, przyciąganie uwagi
* efekty specjalne, robienie scen, aktorstwo
* tymczasowe nawracanie występem

### Zasoby i otoczenie (3)

* fanatyczni fani, sprzęt koncertowy
* stroje i akcesoria performera na każdą okazję
* perfekcyjna ochrona przez tłum

## Opis

Przykłady:

* Sheryl / Ranka (Macross Frontier)
* "telewizja mnie opętała" - Wreck-Gar (TF)

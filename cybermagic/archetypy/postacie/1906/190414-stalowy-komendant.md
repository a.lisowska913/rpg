---
layout: inwazja-karta-postaci
categories: archetype
title: "Stalowy Komendant"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* zdyscyplinowany oficer dowodzący placówką zmieniający pospolite ruszenie w oddział

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)

* dla mnie: porządek i dyscyplina, usprawniać otoczenie
* dla innych: kosmiczny porządek, zdyscyplinowany oddział

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* niezłomny, silna wola, wzbudza strach i podziw
* dowodzenie, przejmowanie kontroli, taktyka
* szkolenie, działa własnym przykładem

### Zasoby i otoczenie (3)

* wyszkoleni żołnierze, militarna struktura i baza
* reputacja stalowego acz uczciwego

## Opis

Przykłady:

* Honor Harrington

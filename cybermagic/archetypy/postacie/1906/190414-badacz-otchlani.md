---
layout: inwazja-karta-postaci
categories: archetype
title: "Badacz Otchłani"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* naukowiec odkrywający Prawdę za wszelką cenę

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)

* dla mnie: udowodnić rację, rozwiązać zagadkę, uznanie
* dla innych: wolność myślenia i badań, postęp technologiczny

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* analizuje i odkrywa, okultystyczne metody
* adaptacja do próbek, buduje prototypy
* praca ze źródłami, archiwa i komputery

### Zasoby i otoczenie (3)

* laboratorium, archiwa, dziwne rytuały
* reputacja skutecznego acz nienormalnego

## Opis

Przykłady:

* .

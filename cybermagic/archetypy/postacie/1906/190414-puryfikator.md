---
layout: inwazja-karta-postaci
categories: archetype
title: "Puryfikator"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* czyściciel Skażenia odzyskujący świat dla ludzi

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)

* dla mnie: koić cierpienie, czystość człowieczeństwa
* dla innych: świat bez anomalii, odzyskać świat dla ludzi

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* pomoc medyczna, odkażanie terenu, wiedza o Skażeniu
* walka ze Skażeńcami, bardzo twardy, wykrywanie anomalii

### Zasoby i otoczenie (3)

* sprzęt odkażający, sprzęt medyczny, sprzęt ochronny
* wsparcie oddziału, reputacja inkwizytora

## Opis

Przykłady:

* .

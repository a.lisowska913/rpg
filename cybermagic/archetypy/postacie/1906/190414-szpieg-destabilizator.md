---
layout: inwazja-karta-postaci
categories: archetype
title: "Szpieg Destabilizator"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* niewidzialny plotkarz, który rozbija więzi podnosząc swoją pozycję

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)

* dla mnie: jak najlepsza pozycja, być bardzo ważnym
* dla innych: chaos i nieufność, mieć swoje miejsce w Porządku

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* skradanie się, podsłuchiwanie, szybkie zdobywanie informacji
* niepozorny, odwracanie uwagi, wszędzie się dostanie
* plotki, manipulacja, niszczenie reputacji

### Zasoby i otoczenie (3)

* sprzęt maskujący, sprzęt podsłuchowy
* sporo informacji na temat wszystkich dookoła

## Opis

Przykłady:

* See No Evil (Batman)
* ghost journalist z Batman Beyond

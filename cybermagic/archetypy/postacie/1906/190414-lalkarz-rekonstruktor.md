---
layout: inwazja-karta-postaci
categories: archetype
title: "Lalkarz Rekonstruktor"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* rekonstruktor ciała i umysłu pragnący absolutnej kontroli

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)

* dla mnie: władza, absolutna kontrola ciała i umysłu
* dla innych: odmienić ich dla swego ideału, 

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* kontrola umysłów i ciał, manipulacja, uzależnianie
* wzbudzanie terroru i grozy, działanie kultystami i agentami
* rekonstruktor ciał i umysłów, bardzo cierpliwy

### Zasoby i otoczenie (3)

* zakazana technika, uzależniacze, narzędzia rekonstrukcji
* złamani agenci, pomagierzy

## Opis

Przykłady:

* .

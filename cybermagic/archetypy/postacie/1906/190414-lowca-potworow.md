---
layout: inwazja-karta-postaci
categories: archetype
title: "Łowca Potworów"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* niszczyciel anomalii chroniący ludzkość ogniem i mieczem

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)

* dla mnie: walka to przyjemność, podziw i prestiż, specjalne traktowanie
* dla innych: bezpieczeństwo i ochrona, dobre traktowanie innych

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* walka z potworami, tropienie, pułapki
* dowodzenie grupą, wymuszanie przysług, onieśmielanie
* pierwsza pomoc, survival

### Zasoby i otoczenie (3)

* skrytki z narzędziami terroru i bronią
* mały gang, porządny motor i sprzęt
* chroniona baza i fabrykator

## Opis

Przykłady:

* Jeźdźcy Smoków (Pern)
* Geralt (Wiedźmin)

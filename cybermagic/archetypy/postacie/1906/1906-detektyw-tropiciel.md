---
layout: inwazja-karta-postaci
categories: archetype
title: "Detektyw Tropiciel"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

Detektyw, który niezależnie od medium zawsze odnajdzie swój cel.

### Czego pragnie a nie ma (3)

* porządek; wymierzona sprawiedliwość
* duma; każdego potrafi znaleźć
* pokój; idealnie - świat bez przemocy

### Jak działa najskuteczniej (3)

* wyciąga prawdę: przesłuchanie, zagadki
* ślady: tropienie, plotki, dokumenty
* działanie uliczne: skradanie się, pościg
* komputery: działania w virt, łamie hasła

### Zasoby i otoczenie (3)

* informatorzy, znajomi w virt
* dostęp do dobrego laboratorium śledczego
* wiedza o przeszłych sprawach

## Opis

Przykłady:

* Ultra Magnus (TF)
* Poirot

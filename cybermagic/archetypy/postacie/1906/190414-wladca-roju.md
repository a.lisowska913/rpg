---
layout: inwazja-karta-postaci
categories: archetype
title: "Władca Roju"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* dążący do perfekcji swojego Roju kontroler działający głównie przez drony

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)

* dla mnie: jak najwięcej dron i agentów, perfekcja dron i agentów
* dla innych: optymalizacja pracy i życia, mniej pracy

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* adaptacja dron, zmuszanie dronami do ruchu
* badacz, obserwuje i buduje
* działanie zdalne, pułapki

### Zasoby i otoczenie (3)

* różnorodne dopasowane drony
* źródło materiałów (złomowisko, biolab)
* oczy i uszy wszędzie

## Opis

Przykłady:

* Zed (Swat Kats)
* Kamila (AZ)

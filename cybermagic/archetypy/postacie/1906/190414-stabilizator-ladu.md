---
layout: inwazja-karta-postaci
categories: archetype
title: "Stabilizator Ładu"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* dokumentami i siłą dba, by status quo i porządek były zachowane

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)

* dla mnie: bezpieczeństwo, godni sojusznicy, docenienie
* dla innych: każdy ma swoje miejsce, prawo ponad wolność

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* na każdego ma paragraf, prawo, biurokracja
* wykorzystanie Systemu przeciw problemowi
* walka uliczna

### Zasoby i otoczenie (3)

* uprawnienia do kamer, podsłuchów i dokumentów
* możliwość wysłania agentów bojowych
* dostęp do archiwów i przeszłych danych

## Opis

Przykłady:

* Sędzia Dredd (Judge Dredd)
* Frobisher (Torchwood)

---
layout: inwazja-karta-postaci
categories: archetype
title: "Neurosprzężony Pilot"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

* pilot sprzężony z Golemem, w dwóch ciałach, czujący to co Golem

### Motywacja (co chcę dla siebie, co dla innych, czego nie mam) (3)

* dla mnie: jak najlepszy Golem, akceptacja ludzi
* dla innych: tolerancja, swoboda, prawa dla AI

### Sposób działania (co mnie wyróżnia, jak działam) (3)

* opętanie neuromaszyny, działanie Golemem
* neuronauta, konstruktor psychotroniki, praca z komputerami

### Zasoby i otoczenie (3)

* Golem - neurowspomagana maszyna z TAI
* baza ze sprzętem, personel pomocniczy
* przeciwnicy Golemów

## Opis

Przykłady:

* Guld (Macross Plus)
* Scorponok (TF)

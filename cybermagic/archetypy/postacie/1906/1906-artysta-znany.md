---
layout: inwazja-karta-postaci
categories: archetype
title: "Artysta, znany"
---

# {{ page.title }}

## Archetyp

### Ogólny pomysł (3)

Popularny artysta sztuki wysokiej, znany i mający mecenasów.

### Czego pragnie a nie ma (3)

* zmienić świat; porwać ludzi za Sprawą
* stworzyć arcydzieło; dla siebie, krytyków
* stworzyć nowy nurt sztuki; uczyć innych

### Jak działa najskuteczniej (3)

* inspiracja: dziełami, przyciąga uwagę
* zauroczenie: wywołuje potężne uczucie
* uprawnienia: wolno mu więcej, elegancja

### Zasoby i otoczenie (3)

* stworzone dzieła: barter lub dla efektu
* bogaty: większość rozwiąże pieniędzmi
* miłośnicy sztuki: może bywać w Miejscach

## Opis

Przykłady:

* Issei (SwordGai)

---
layout: cybermagic-karta-postaci
categories: archetype
title: "Inspirator"
---

# {{ page.title }}

## Archetyp

### Paradoksalny Koncept

Artysta, Piosenkarz, Guru lub Komisarz Polityczny – Inspirator kontroluje emocje ludzi i przyciąga uwagę.
Jest bliżej Eteru Nieskończonego niż większość osób, dzięki temu zauważa dziwne zjawiska.
(Ludzie/Emocje + Intuicja/Eter)

## Mechanika

### Czym osiąga sukcesy (3)

* ATUT: Wpływa na emocje ludzi, świetnie przekonywuje, wykrywa intencję.
* ATUT: Zauważa anomalie w zachowaniu ludzi i dziwne zjawiska; jest na nie dość odporny.
* SŁABA: Nie jest zbyt silny w kontekście technologii czy medycyny.
* SŁABA: Nie jest zbyt skuteczny w walce.

### O co walczy (3)

* ZA: Powiększyć grono popleczników
* ZA: Łączyć wszystkich dookoła swojej Sprawy
* VS: Świat bez kultury, sztuki, pasji. Praca nade wszystko.
* VS: Pozostawienie innych bez nadziei.

### Znaczące Czyny (3)

* Gdy doszło do katastrofy, przejął dowodzenie i ludzie zaczęli gasić pożar, zanim przybyła straż pożarna.
* Gdy pojawił się upiór w okolicy gimnazjum, wyczuł go pierwszy i odwrócił uwagę dzieciaków by upiór nie miał jak się nimi żywić.
* Gdy uderzył Kryształ Nienawiści, deeskalował sytuację która mogła zakończyć się rozlewem krwi.

### Kluczowe Zasoby (3)

* COŚ: Dzieła stworzone w przeszłości, które dla niektórych mogą mieć niemałą wartość.
* KTOŚ: Osoby, które widzą w Inspiratorze szansę na lepsze jutro.
* WIEM: Jak działają wpływy magiczne i chemiczne na ludzi - i jak sobie z tym radzić.
* OPINIA: Uprawnienia i pozycja. Inspiratorowi więcej się wybacza.

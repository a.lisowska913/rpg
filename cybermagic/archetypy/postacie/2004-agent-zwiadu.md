---
layout: cybermagic-karta-postaci
categories: archetype
title: "Agent Zwiadu"
---

# {{ page.title }}

## Archetyp

### Paradoksalny Koncept

Agent zwiadu, kontrolujący drony i wysoką technologię. Wszędzie wlezie, gdzie nie wlezie to drony wyśle a jak inaczej się nie da - shackuje.

(Technologia/Drony, Walka)

## Mechanika

### Czym osiąga sukcesy (3)

* ATUT: Porusza się płynnie po każdym terenie dzięki wspomaganiu nanitkami; akrobata.
* ATUT: Doskonały strzelec i snajper. W walce wręcz też sobie poradzi.
* ATUT: Bardzo trudny do wykrycia, potrafi się świetnie maskować.
* SŁABA: Jest dobry w wykrywaniu rzeczy i ich lokalizowaniu, ale niekoniecznie radzi sobie z interpretacją wyniku.
* SŁABA: Nie działa dobrze na pierwszej linii; woli z ukrycia. Niechętnie na widoku, niezależnie od kontekstu.

### O co walczy (3)

* ZA: Znaleźć zagrożenie by móc zadziałać jako pierwszy. Wiedza wygrywa walki, nie większy kaliber.
* ZA: Spokojna prowincja, gdzie będzie mógł osiąść. Z dala od anomalii czy innych ludzi ;-).
* VS: Przeciwny traktowaniu wojskowych jako niebezpiecznych debili. Przecież on tylko chroni innych.
* VS: Przeciwny zbędnej przemocy i marnowaniu zasobów. Nie wolno niczego marnować, może się przydać.

### Znaczące Czyny (3)

* Dwa dni morderczej trasy przez teren Skażony, by wygonić Wężoszpony (by ochronić Enklawę). Nic go nie wykryło.
* Znajdował się na dachu wysokiego budynku - gdy potwór mignął na ulicy, zastrzelił go snajperką.
* Używając swoich dron wypłoszył gangstera z magazynu prosto pod zbliżających się policjantów.

### Kluczowe Zasoby (3)

* COŚ: Kombinezon maskujący, wyciszony karabin snajperski.
* KTOŚ: Kilku zaufanych towarzyszy broni, na których może liczyć.
* WIEM: Wiem co potrafi się gdzie ukryć - i gdzie on się może ukryć by nic go nie widziało.
* OPINIA: Samotnik. Woli rekalibrować swoje drony niż gadać z innymi. Ale jest niezawodny w potrzebie.

---
layout: cybermagic-karta-postaci
categories: profile
factions: "szczeliniec, terminus, wolny uśmiech"
owner: "public"
title: "Tomasz Tukan"
---

# {{ page.title }}

## Kim jest

### Paradoksalny Koncept

Pustogorski terminus współpracujący z mafią Grzymościa. Neuronauta, katalista i terminus posiadający słabostki do pięknych kobiet. Świetny w walce magicznej i kontroli mentalnej, ale pomiatany przez Kajrata. Ma bardzo duże umiejętności, jest bardzo uzdolniony - ale jednocześnie próbuje politycznie skonsolidować swoją władzę; nie chce samej mocy. Mściwy, ale na zimno. Arogancki, uważa, że należy mu się więcej niż ma - a jednocześnie drży ze strachu przed Esuriit i Ixionem.

### Motto

"Technologia jest błędem - biomagia powinna zastąpić tą głupią ścieżkę ewolucji."

## Mechanika

### Czym osiąga sukcesy (3)

* ATUT: Neuronauta bardzo wysokiej klasy - wchodzi w głowę i potrafi wykryć cokolwiek oraz stworzyć dowolne wspomnienia; też przepisać komuś osobowość.
* ATUT: Bardzo skuteczny katalista bojowy; jako terminus specjalizuje się w obracaniu energii przeciw przeciwnikowi i atakach mentalnych i wypalających.
* SŁABA: Kocha piękne kobiety. Chce być przez nie uwielbiany - a Grzymość mu to zapewnia (i tak go złapał).
* SŁABA: Mimo wszelkich jego talentów, PRZERAŻAJĄ go Ixion i Esuriit. Jego moc w ogóle nie wchodzi z tymi energiami w interakcję.

### O co walczy (3)

* ZA: Pragnie małej baronii, a najlepiej tytuł arystokratyczny od Aurum. Chce miejsca, w którym będzie mieć pełną władzę i pełną kontrolę. Nieważne - mafia, Pustogor, Aurum...
* ZA: Przekonać wszystkich, że bioformy są lepszym rozwiązaniem niż magitech. Zbudowanie Absolutnej Ochrony, perfekcyjnej linii biomagicznej.
* VS: Zwalcza TAI, roboty, a już szczególnie seksboty (do tych ma patologicznie wrogi stosunek). Preferuje biomagię nad magitech.
* VS: Niech mafia nie pokona terminusów. Niech terminusi nie pokonają mafii. Potrzebna jest harmonia konfliktowych stron. Nie "pokój" a "stabilny konflikt".

### Znaczące Czyny (3)

* Gdy doszło do emisji koszmarów w Kasynie Marzeń, wszystkich cierpliwie wyleczył z traumy mentalnej i usunął im pamięć o incydencie. Po drodze dyskretnie zniszczył seksbota.
* Wynegocjował włączenie niejednego pomniejszego gangu do mafii Grzymościa, dając szefom tych gangów dokładnie to o czym marzyli, niezależnie jak podłe to nie było.
* Znalazł i rozszarpał w walce orbiterskiego miragenta, który próbował zbudować siatkę na terenie Pustogoru; potem zgłosił to Karli.

### Kluczowe Zasoby (3)

* COŚ: Eksperymentalny Biopancerz będący odpowiednikiem servarów terminuskich; cały czas rozwijany, regenerujący się i żywy, z wieloma mackami, z bronią opartą na kwasie.
* KTOŚ: Organizacja "Wolny Uśmiech" Grzymościa dostarcza mu ludzi, terminusi pustogorscy zapewniają mu intel. Jego harem - zapewnia mu wszystko inne.
* WIEM: Zna skryte marzenia i nadzieje bardzo wielu znaczących magów w Szczelińcu. Jak trzeba, potrafi je spełnić by pozyskać możliwość współpracy.
* OPINIA: Łajza. Bardzo uzdolniona i w sumie groźna w walce, ale łajza. Nie jest szczególnie szanowany.

## Inne

### Wygląd

.

### Coś Więcej

.

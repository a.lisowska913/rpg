---
layout: cybermagic-karta-postaci
categories: profile
factions: "rekin, aurum, sowiński"
owner: "public"
title: "Amelia Sowińska"
---

# {{ page.title }}

## Kim jest

### W kilku zdaniach

TODO

### Co się rzuca w oczy

* TODO

### Jak sterować postacią

* TODO

## Mechanika

### Znaczące Czyny i Osiągnięcia (3)

* Gdy Roland był Skażony Esuriit i doprowadził do serii śmierci i cierpienia, wzięła wszystko na siebie. On jest paladynem, ona administratorką. Jej reputacji to obojętne, jego mogłoby to zniszczyć.
* Przybyła do Rekinów i zastała grupę zblazowaną bez cienia koordynacji czy planu. Zbudowała im system, który jest używany do dzisiaj. Dała im kierunek i powód istnienia, a nie „wygnani arystokraci”. Kierunek przez nią nadany jest taki jak Roland kazał jej nadać - by pomagali.
* Widząc eskalujący konflikt Morlan - Jolanta Sowińska wprowadziła siebie na dwór Morlana by neutralizować Jolantę. Bo działania Jolanty źle rzutują na ród Sowińskich, ale też po to, by zrozumieć gdzie zrobiła błąd z Rolandem. Jak mogłaby to naprawić. I jak zemścić się na Oliwii.

### Co się rzuca w oczy: Atuty i Przewagi (3, 6)

* CECHA: uprawnienia inspektora hipernetu. Może przechwytywać i dekodować dalekosiężne komunikaty
* AKCJA: organizuje i zajmuje się logistyką. Dbałość o detale. Potrafi sprawnie zarządzać grupą osób
* AKCJA: świetna manipulatorka na dworze. Potrafi używając struktur Aurum umieścić kogo chce gdzie chce i wpakować w kłopoty. 
* AKCJA: przetwarza informacje. Potrafi dużo ukryć lub wydobyć z komunikacji i dokumentów.
* AKCJA: dobra aktorka i negocjatorka. Potrafi być słodka, zmotywować, zainspirować, sprawić, że myślisz, że jej na Tobie zależy. W rozmowie dostaje co chce.
* COŚ: mnóstwo kompromitujących materiałów i sekretów na różne osoby w Aurum. Głównie z hipernetu. Dlatego „nie może zdradzić”.

### Serce i Wartości (3)

* Tradycja 
    * Łańcuch dowodzenia jest święty. Wykonuję rozkazy nawet jak mi się nie podobają, Ty też będziesz.
    * Każdy ma swoje miejsce w maszynie zwanej systemem. Moją odpowiedzialnością jest zadbanie, by każdy był jak najlepiej wykorzystany.
    * Jeśli coś złego się dzieje, ZAWSZE odpowiedzialny jest przełożony. Ważna jest intencja, nie słowa.
* Twarz
    * Reputacja jest WSZYSTKIM. Jest ważniejsza niż życie czy cierpienie. Nie umiesz jej utrzymać - nie jesteś godną arystokratką.
    * Jeśli coś zawaliłam, muszę naprawić. Nie godzi się uciekać od swoich porażek. She owns it and requires others to own it, too.
    * Ważniejsze jest jak widzą cię Ci wyżej niż ci niżej. Wymagaj szacunku i szanuj lepszych od siebie.
    * Możesz wybaczać, ale zawsze publicznie się zemścij. Pokaż innym, że nie mogą Cię wykorzystywać.
* Stymulacja
    * Zabawa, rozrywka i wysokie morale nie są bonusem czy elementem efektywności. To kwestia moralności.
    * Jeśli można coś zrobić fajne i interesujące kosztem niewielkiej straty efektywności, warto to zrobić. 
    * Marzy i szuka gorącej, romantycznej miłości. Wierzy w miłość i pragnie uniesień.

### Typowe problemy z którymi sobie nie radzi; Słabości (-3)

* CORE WOUND: "Wszyscy traktują mnie jak TAI. Amelia rozwiąże problem. Nikt nie zauważa we mnie osoby. Nikomu na mnie nie zależy. Gdybym zniknęła, wymieniliby mnie na Persefonę."
* CORE LIE: "Jeżeli rozwiążę bardzo trudny problem sama w sposób unikalny to mnie zauważą. Będę warta miłości. Ktoś mnie pokocha."
* pod wpływem stresu wpada w lekką paranoję. Nie oddelegowuje. Wszystko zrobi sama. Da się ją przeładować.
* strasznie dumna, pyszna. Nie jest trudno wpakować ją w pułapkę „ja siama”. Nienawidzi przyznania się do słabości i zrobi dużo, by ukryć swoje wszelkie braki
* mimo, że jest dobrą aktorką, nie dba o fasadę wobec osób które uważa za niższe.

### Magia (3M)

#### W czym jest świetna

* TODO

#### Jak się objawia utrata kontroli

* TODO

### Specjalne

* .

## Inne

### Wygląd

* ?

### Coś Więcej

* .

### Endgame

* 

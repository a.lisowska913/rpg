---
layout: inwazja-karta-postaci
categories: profile
factions: "elisquid"
owner: "public"
title: "Diana Tevalier"
---

# {{ page.title }}

## Paradoks

Słodka jednostka wsparcia z virt, która jest bardzo nieufna, samotna i specjalizuje się w koszmarach.

## Motywacja

### O co walczy

* absolutna wolność i niezależność - nikt nie jest w stanie jej kontrolować ani ograniczyć w żadnym stopniu
* poznanie prawdy o swojej rodzinie i propagowanie historii Astorii; pokazanie noktian jako "tych złych"
* niech virt będzie traktowany jako "normalny" - równoprawny świat, też byty w virt.

### Przeciw czemu walczy

* ktokolwiek lub cokolwiek jest w stanie ją ograniczać lub kontrolować; jest to coś, czego nie toleruje
* bycie traktowaną jako słabą czy nieistotną; to szybka droga do bycia zniszczoną

## Działania

### Specjalność

* ogromny urok; doskonała wręcz aktorka - potrafi skłócić większość przeciwników
* istota multivirt; z dowolnego miejsca potrafi połączyć się z viert
* szczur; schowa się, wślizgnie, ukryje. Dziecko ulicy.
* koszmarne wizje. Mało kto potrafi stworzyć tak przerażające obrazy czy opowieści jak Chevaleresse.

### Słabość

* bardzo nieufna; polega praktycznie tylko na sobie i swoich zmysłach
* nie zna swoich granic; zbyt ryzykowna, zbyt chce się pokazać, idzie za daleko i wpada w kłopoty
* nienawidzi przemocy, nie stosuje przemocy, nie potrafi stosować przemocy. Nigdy.

### Akcje

* łączy i integruje ludzi - tworzy organizacje, grupy, gildie
* 100% uroku i dziewczęcej słodyczy - przyjemnie jak w pobliżu
* lubi opowiadać historie i być na czele
* ma tendencje do chowania się i znajdowania sobie takich miejsc, z których można najłatwiej się wyślizgnąć
* manipulatorka najwyższej klasy, zawsze zaadaptuje się do takiej formy, w której wszyscy jej uwierzą
* wybitna aktorka, zaadaptuje się do każdej sytuacji by ją zaakceptowano
* jej opowieści i iluzje potrafią wywołać ogromny strach nawet u dorosłych
* gdy tylko może, przebywa w vircie - w swoim świecie lub różnych światach

### Znaczące czyny

* używając swojego uroku i umiejętności chowania się uciekła z Eternii do Zjednoczenia Astoriańskiego i znalazła swoje miejsce u Alana
* złapana na gorącym uczynku przez niebezpiecznego maga, postraszyła go opowieścią - i on tylko ją pobił zamiast aresztować
* zachowała zimną krew gdy była związana; straciła dużo zasobów w virt by tylko uratować Alana przed katastrofą
* wyszła naprzeciw grupie protestujących studentów by chronić reputację wojskowych na Astorii
* główny support w Elisquid (multivirt) - integruje całą gildię; to ona ściągnęła Alana
* nie dała rady uderzyć nikogo nigdy, nawet w samoobronie. Po prostu nie potrafi. Za dużo koszmarów w głowie.

## Mechanika

### Archetypy

gracz virt, szczur miejski, istota virt

### Motywacje

.

## Inne

### Wygląd

16-latka, wygląda na 13-latkę.

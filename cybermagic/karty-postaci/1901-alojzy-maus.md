---
layout: cybermagic-karta-postaci
categories: profile
factions: "Maus, Pustogor"
owner: "public"
title: "Alojzy Maus"
---

# {{ page.title }}

## Postać

### Ogólny pomysł (3)

Górnik i technik podziemnych, niebezpiecznych bieda-magitrowni w Pustogorskiej Szczelinie. Świetny wspinacz, atletyczny i wysportowany. Katalista transformujący energię magiczną w czyste _vitium_. Górnik i ratownik podziemny, zna się na terenie, maszynach i pierwszej pomocy.

### Motywacja (gniew/wartość, zmiana, sposób) (3)
 
* GRATITUDE/COMMUNITY; Karradrael i Mausowie są docenieni oraz ludzie są równi magom; pomaga Skażonym, jest pomocnym Mausem
* ALTRUISM/COMPASSION; ludzie sobie pomagają przeciwko tej dziwnej rzeczywistości; przyjaźni ze wszystkimi i zawsze próbuje pomóc
* HARD WORK/CONSERVATION; Skałka Mała jest zapamiętana i przeszłość ma znaczenie; promuje pamięć, pracuje ciężko i robi "ołtarzyk"

### Wyróżniki (3)

* Pod ziemią czuje się jak w domu - zarówno w naturalnych jaskiniach jak i w zbudowanych przez cywilizację korytarzach.
* Ma iście "krasnoludzki" zmysł lokalizacji.
* Ekspert katalista, specjalizuje się w kryształach _vitium_, przekierowaniu energii i puryfikacji.
* Świetny wspinacz, bardzo atletyczny. Pancerny, silny oraz wysportowany.

### Zasoby i otoczenie (3)

* opieka Karradraela, który pomaga mu poradzić sobie na linii odporności - morale, dekoncentracja, utrata wiary...
* bieda-magitrownie, tymczasowo rozkładane urządzenia mające za zadanie przekształcenie ruchliwych prądów energii w kryształy _vitium_.
* ludzcy przyjaciele ze Skałki Małej. Nawet, jeśli samej wsi już nie ma, oni są w okolicy nadal.
* sprzęt górniczy i dobra reputacja, Alojzy solidnie pracuje i inni to doceniają. Ma też niezły sprzęt, któremu ufa.

### Magia (3)

#### Gdy kontroluje energię

* działania podziemnie: potrafi poruszać się pod ziemią robiąc korytarze, przenikając przez teren, widząc w ciemnościach, zapewniając powietrze...
* katalitycznie potrafi odnaleźć odpowiednie prądy energii by móc zbudować kryształy _vitium_ przy pomocy bieda-magitrowni
* technomantycznie potrafi wykorzystywać różnego rodzaju ludzkie urządzenia (i magitrownie), jak i je naprawiać jak co do czego dojdzie.

#### Gdy traci kontrolę

* jego magia zwykle manifestuje się w formie obrazów technomantycznych lub rycin na materii (czy modyfikacji materii).
* gdy w dobrym humorze, pojawiają się wspomnienia czasów ze Skałki Małej - niewielka mieścina, wszyscy się znają...
* gdy w złej formie, zamknięte w nim upiory Skałki zaczynają się uwalniać i opętywać inne osoby lub przedmioty, chcąc niszczyć dla ukojenia własnego cierpienia

### Powiązane frakcje

{{ page.factions }}

## Opis

Alojzy Maus jest górnikiem z okolic Pustogora, z niewielkiej wioski - Skałki Małej. Jako człowiek został napromieniowany energią magiczną z Pustogorskiej Szczeliny gdy doszło do Emisji Szczeliny Pustogorskiej, co obudziło w nim Dar.

Z uwagi na naturalną niestabilność energii z których pochodzi, zaopiekował się nim Karradrael nadając mu stabilność oraz uploadując mu przydatne umiejętności z dawnych, nieżyjących już Mausów.

Alojzy większość pieniędzy poświęca by pomóc mieszkańcom swojej małej społeczności, skrzywdzonych przez Pustogorską Szczelinę.

(35 min)

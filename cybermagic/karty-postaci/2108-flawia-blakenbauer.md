---
layout: cybermagic-karta-postaci
categories: profile
factions: "blakenbauer"
owner: "public"
title: "Flawia Blakenbauer"
---

# {{ page.title }}

## Kim jest

### W kilku zdaniach

Infiltratorka z konieczności, która zawsze marzyła o gwiazdach i kosmosie, daleko od Aurum i durnych intryg. Nie przepada za arystokracją i za abstrakcjami - lubi to, co może dotknąć. Specjalistka od transformacji płaszczek i siebie.

### Co się rzuca w oczy

* Flawia wierzy w przeznaczenie i w swoje miejsce w egzystencji - dla niej to Orbiter, w odróżnieniu od niezależnego Lucjusza (jej ulubionego kuzyna; też działa poza strukturami Aurum).
* Nie jest fanką Arystokracji Aurum i jest aktywnie wroga Eterni. Lepiej jej z żołnierzami i ludźmi pracy niż z Arystokracją.
* Flirciara, wesoła - czasem mylona z Diakonką. Przynajmniej do momentu jak zdejmie ubranie i widać "symbiotycznego skorpioida".
* Szuka struktury, porządku, miejsca gdzie może się przydać i pomagać. Jej marzeniem jest oficer wykonawczy na Orbiterze, ale nie kapitan.
* Bardzo "kinetyczna" - musi DOTKNĄĆ, zobaczyć sama. Nastawiona na konkret i to, co materialne. Kiepsko radzi sobie z abstrakcjami i integracją z virt / maszynami.

## Mechanika

### Znaczące Czyny i Osiągnięcia (3)

* Gdy Lucjusz obejmował Szpital Terminuski, Flawia wzięła urlop awaryjny i przyleciała do Pustogoru. Tak długo czarowała, przekonywała, dawała dowody, prosiła - aż w końcu dali mu szansę. Była jego banneretką ;-).
* Gdy na Orbiterze była świadkiem kocenia innych, działała z cienia - przekupstwem, dobrym słowem lub podłożeniem fałszywych dowodów sprawiała, że kocenie było zawsze ukarane.
* Gdy Domena Barana była zaatakowana przez Strachy, Flawia magią (wspierana przez Antonellę i Esuriit) syntetyzowała skorpioidami pajęczynę do neutralizacji Strachów. Misja ważniejsza niż niechęć do Esuriit.

### Co się rzuca w oczy: Atuty i Przewagi (3)

* AKCJA: Porusza się i zachowuje się swobodnie prawie wszędzie. Mistrzyni infiltracji społecznej. Doskonała aktorka. Jak trzeba, zabójczyni.
* AKCJA: Ma podstawowe wyszkolenie bojowe i zagrożeniowe Orbitera. Umie walczyć wręcz, strzelać, zna podstawowe problemy i zagrożenia, zachowuje zimną krew...
* AKCJA: Potrafi magią edytować płaszczki i siebie. Jej moc jest ograniczona do biomancji.
* COŚ: Ma pet scorpoid queen. Z tych miniaturowych. Co straszniejsze, ma ową królową skorpioidów w ciele; w okolicach boku.
* COŚ: Ma ogłupiacze, ogłuszacze i detoksy - psychoaktywne środki, które może komuś podać.

### Serce i Wartości (3)

* Twarz 
    * ma tysiące twarzy, ale honor i uznanie tych na których jej zależy jest bardzo ważne. 
    * bardzo wysokie integrity i poszanowanie Zasad. Wiele zapłaci w służbie Sprawy.
* Dobrodziejstwo
    * ma bardzo altruistyczne podejście do bliźnich. Potrafi bardzo dużo energii włożyć by pomóc komuś, kto zasłużył. Pomaga tym, którzy sami próbują sobie pomóc - nie ratuje tych co nie próbują zmienić swego losu.
    * She holds grudges. Jeśli kogoś nie lubi lub uznaje za wroga - nie pomoże. 
* Pokora
    * dąży do swojego miejsca w egzystencji. Walczy by się tam dostać. 
    * Jej miejscem jest Orbiter. Wykonuje rozkazy i akceptuje, że inni mogą być lepsi. 
    * Przez to ma wewnętrzną harmonię - przeznaczenie sprawi, że jeśli będzie walczyć o Orbiter, w końcu tam trafi.

### Typowe problemy z którymi sobie nie radzi; Słabości (-3)

* Jakkolwiek by się nie starała, Flawia po prostu nie potrafi integrować się z maszynami czy virtem. Jeśli chodzi o "normalne" urządzenia, daje radę - ale nic zaawansowanego.
* Flawia ma bardzo duże problemy z odrzuceniem rozkazów czy działaniem wbrew rozkazom (chodzi o osoby którym ufa). Normalnie to Zasady > Rozkazy, ale nie tu.
* Flawia nie potrafi zostawić osoby w potrzebie "tak po prostu". Dla ratowania osób wobec których jest lojalna - zaryzykuje spalenie misji.
* Flawia po prostu nie nadaje się do dowodzenia. Nie ogarnia wysokopoziomowych planów strategicznych. Radzi sobie z poziomem operacyjnym i z adaptacją empatyczną, ale nie z abstrakcją dowodzenia.

### Specjalne

* .

## Inne
### Wygląd

.

### Coś Więcej

* .

### Endgame

* .

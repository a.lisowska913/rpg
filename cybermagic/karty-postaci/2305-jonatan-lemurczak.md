---
layout: cybermagic-karta-postaci
categories: profile
factions: "lemurczak"
owner: "public"
title: "Jonatan Lemurczak"
---

## Funkcjonalna mechanika

* **Aspekty charakteru**
    * Siły: wie co siedzi w ludzkiej duszy, immune to horrid visions, dokładny i cierpliwy
    * Słabości: unika krwi i brudzenia sobie rąk, taktyk a nie wojownik, ładne dziewczyny (torment and corrupt), Mirażja Diakon i ogólnie Diakonki
* **Aspekty strategii / podejścia**
    * Siły: doskonale deleguje, metodyczne i ostrożne działanie, prawie nie zostawia otwarć dla wroga (neurotycznie ostrożny)
    * Słabości: za bardzo deleguje (taktyka - Stegozaur itp.), woli strach i zniewolenie nad lojalność, zbyt ostrożny (wycofuje się zbyt szybko)
* **Aspekty umiejętności**
    * Siły: twisted alchemist, twisted medic, scientist and researcher, true gentleman
* **Aspekty zasobów**
    * Siły: twisted allies, neuro-obroża, środki mutagenne, hovertank wsparcia klasy Stegozaur

## Kim jest

### Fiszka

* Jonatan Lemurczak: mag, specjalista od strachu i koszmarów
    * (ENCAO:  -+0-+ | podatny na nostalgię;; Innowacyjny, szuka nowych rozwiązań;; Poddańczy większej sile | VALS: Power, Self-direction > Hedonism | DRIVE: The Ultimate Utopia - love through fear)
    * styl:  Kiriyu (Ingrid): elegancki, raczej w cieniu, używa _corruptions_, niechętnie walczy, dokładnie bada granice

### W kilku zdaniach

Dobrze ubrany _corruptor_ Lemurczaków który bada strach, koszmary i sposób w jaki można straumatyzować kogoś do uwielbienia. Uważa się za badacza i twierdzi, że to jest sposób dzięki któremu świat stanie się lepszy. Nie bawi go cierpienie innych, raczej patrzy na to klinicznie. Szuka perfekcyjnych 

* Motto: "Ból się skończy gdy znajdę rozwiązanie dające Ci radość z lojalności."

### Jak sterować postacią

* Wizja: 
    * JA: Król Diakonek, kreator środków biomanipulacji, nikt z niego się już śmiać nie będzie
    * ŚWIAT: utopia, perfekcja ludzkiej duszy - przy użyciu manipulacji biochemicznych
* Dominujące Strategie: 
    * corruption of agents, evolution of agents, terror
* Charakter: 
    * elegancki, wie kiedy się cofnąć, niechętnie walczy, nie cierpi zabijać - szkoda narzędzi
* Wzory: 
    * Kiriyu (Ingrid): elegancki, wie kiedy się cofnąć, używa _corruptions_, niechętnie walczy, nie cierpi zabijać
* Inne:
    * Corruptor; niechętny czystemu zniszczeniu, raczej chce wypaczać i odnaleźć sposób na zbudowanie lojalności
    * Badania na ludziach i magach, podniesienie ich umiejętności i potencjału. I nad ich zniewoleniem.
    * Oddaje kontrolę taktyczną Stegozaurowi, sam zajmuje się badaniami i działaniem.

### Serce i Wartości (3)

* Ocean
    * ENCAO: -+0-+
    * Nostalgia; wie jak świat może wyglądać i dąży do tego by się tym znowu stał. Utopia through abuse.
    * I will find the perfect solution, the perfection of human soul. And make her mine.
    * Others are just tools to progress towards utopia
* Wartości
    * TAK: Power, Self-direction
    * NIE: Hedonism
    * P: Ten kto ma największy wpływ dostaje czego chce. Pomiędzy Stegozaurem, badaniami naukowymi i umiejętnościami - wywalczę sobie to co chcę i na co zasłużyłem.
    * P: Prawdziwa władza nie polega na pieniądzach czy wpływie. Polega na głębokiej kontroli drugiej osoby we wszystkich możliwych obszarach.
    * S: Morality, rules - do not matter. I shall find a way to create an utopia we all deserve.
    * S: Solitary life with enslaved thralls is the best way.
    * H: Przyjemność i radość nie mają znaczenia. Prawdziwy porządek możesz osiągnąć tylko kijem i marchewką. A zwłaszcza kijem.
    * H: Jeden raz poszedłem za głosem serca i zapłaciłem za to pośmiewiskiem, upokorzeniem itp. Never again.
* Silnik
    * Corrupted Utopian Knowledge: lord of hearts and minds via biochemical manipulation and absolute power. Badać granice ludzkiej możliwości i potencjału.
        * Army of enslaved homo superiors
* Marzenie (jeśli inne niż Silnik)
    * Deeply Enslave Mirażja Diakon. I inne Diakonki.
* CORE WOUND: "Moja ukochana, tien Mirażja Diakon, rozkochała mnie w sobie a potem wzgardziła i wyśmiała. Nie powinienem był chcieć zerwać z tradycją Lemurii i podlizać się arystokratce."
* CORE LIE: "Only through fear one can thrive"

### Dokonania

### Magia
#### Dominująca moc

* JEDNYM ZDANIEM: "twisted healer of cravings"
    * Przekształcenie formy swojej ofiary
    * Przekształcenie GŁODU swojej ofiary
    * Corrupt to willing slave

#### Jak się objawia utrata kontroli

* Traci kontrolę nad swoimi bioformami.
* COŚ dostaje formę Mirażji Diakon. It torments him.
* His own craving overpowers him and he goes HUNGRY for victory

### Znaczące Czyny i Osiągnięcia 

* .


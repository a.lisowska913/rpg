---
layout: cybermagic-karta-postaci
categories: profile
factions: "orbiter, oddział verlen"
owner: "public"
title: "Martyn Hiwasser"
---

# {{ page.title }}

## Kim jest

### W kilku zdaniach

5x-letni lekarz i bohater wojenny na emeryturze. Kobieciarz, który nigdy nie skrzywdziłby kobiety. Wybitny biokonstruktor, który był członkiem gangu, medykiem wojskowym, przez moment tienem Eterni a teraz jest głównym lekarzem na Inferni. Zachowuje optymizm i determinację, że da się każdemu pomóc do samego końca. Niespodziewanie niebezpieczny w walce - używa pięści i magii. Specjalizuje się w stymulantach i utrzymywaniu przy życiu. Niestety, uszkodzony przez Esuriit i ma niestabilny arkin.

### Co się rzuca w oczy

* Motto: "Utrzymam wszystkich przy życiu - dla uroczej pani kapitan."
* Martyn nie kłamie. Może uniknąć mówienia całej prawdy, ale nie powie nigdy czegoś co jest kłamstwem.
* Zawsze pogodny, taki trochę "kamerdyner", w cieniu. Uprzejmy i elegancki. Rzadko pokazuje nadmiar emocji.
* Nigdy nie opowiada opowieści ze swojej przeszłości, z wojny itp. "Niech przeszłość pozostanie tam, gdzie jej miejsce".
* Jego najbliższą przyjaciółką na Inferni jest Klaudia. Najlepiej się dogadują. Są też charakterologicznie najbardziej podobni.
* Nie ma ambicji. Nie chce dowodzić. Z przyjemnością odda się pod dowództwo kogoś innego.
* Ma tendencje do SKRAJNIE ryzykownych manewrów. Z perspektywy analizy ryzyka jest zawsze na krawędzi. On nie umrze w swoim łóżku.
* Nadal kocha piękne kobiety - i jest świetny zarówno w podrywaniu i ich oczarowywaniu jak i w sprawach łóżkowych. "Biomanta", anyone?
* Nie mówi złych rzeczy o żadnej grupie - Eternia, Zjednoczenie, Aurum, Orbiter. Dla niego nie ma "złych grup", tylko jednostki (wyjątek: nic nie mówi o noktianach)

### Jak sterować postacią

* "War butler". Kamerdyner, w cieniu, spokojny. Do momentu aż musi działać - wtedy jest zdecydowany.
* Jeśli działa, to działa z ogromnym rozmachem. Wykorzystuje wszystkich i wszystko.
* Bardzo uprzejmy i kulturalny. Nie kłamie. Zostaje w cieniu.
* Każdą sytuację próbuje rozwiązać, narażając siebie przede wszystkim.
* Jego historia się już skończyła; jest na emeryturze. Czas pomóc młodym, którzy nie znają _horroru_ tego świata.
* Potrafi być ABSOLUTNIE BEZWZGLĘDNY. Widział piekło. Nie przeszkadza mu zastrzelenie rannego, bezbronnego pirata by uratować go przed Serenitem.

## Mechanika

### Znaczące Czyny i Osiągnięcia (3)

* Wraz z Ateną Sowińską uratowali część załogi ASD 'Perceptor' przed Serenitem; on stymulował ludzi, ona maszyny. Uciekli, acz z ofiarami.
* Zatrzymał konflikt między dwoma frakcjami Orbitera, bo... zaprzyjaźnił się z damami dowodzącymi oboma frakcjami i zmusił je do rozmowy.
* Gdy 'Infernia' umierała po walce z Leotisem, przesunął wielu ludzi do działania w próżni magitechowo - by mogli utrzymać statek przy życiu.

### Co się rzuca w oczy: Atuty, Przewagi, Zasoby (3, 6)

* AKCJA: mistrz networkingu, który ma gadane. Czar i urok osobisty, zwłaszcza wobec kobiet. Bardzo społeczny. Deeskaluje starcia samą swoją obecnością. Używa wszystkich zasobów ludzkich do jakich ma dostęp.
* AKCJA: doskonały lekarz i biokonstruktor, potrafi utrzymać przy życiu sytuacje beznadziejne. Doskonałe wyczucie sytuacji jak chodzi o tematy medyczne.
* AKCJA: ma przeszkolenie sił specjalnych i ponad 20 lat aktywnej służby na froncie wojny Orbiter - Noctis. "Widział praktycznie wszystko". Stary weteran.
* AKCJA: specjalizuje się w natarciach próżniowych i działaniach w próżni.
* COŚ: Niezależnie od okoliczności, Martyn ZAWSZE ma pod ręką pasujący strój kobiecy na każdą okazję. No questions asked.
* COŚ: Ogromna ilość medali, odznaczeń, osiągnięć, wrogów i przyjaciół w niespodziewanych miejscach. I - o dziwo - tytuł 'tien' z Eterni, który jest już tylko honorowy. Wszystko ukrywa.
* COŚ: Co najmniej 10 akolitów z Eterni, którzy nie tylko w niego wierzą ale z których musi czerpać energię przez uszkodzony arkin. Część akolitów to potencjalni szpiedzy?
* CECHA: potrafi używać arkin Eterni, jak i potrafi używać i pracować z energią Esuriit. Jest to dla niego MNIEJ niebezpieczne niż dla innych.
* CECHA: posiada straszliwe simulacrum eternijskie, w formie palców "osób które zginęły na jego warcie".

### Serce i Wartości (3)

* Wartości
    * TAK: Dobrodziejstwo, Stymulacja, Osiągnięcia
    * NIE: Potęga, Prestiż
    * D: zapewnić uśmiech na twarzy każdej osoby. Uratować każdego, komu można pomóc. Zrobić zmianę tej rzeczywistości.
    * D: nie akceptuje krzywdzenia złych istot by uratować więcej dobrych istot. Eternia nauczyła go, czym to się kończy. A każdemu należy próbować pomóc.
    * S: niezależnie jak źle by sytuacja nie wyglądała, ma wiarę - często przeszacowaną - w swoje umiejętności i możliwości.
    * S: ma tendencje do bardzo ryzykownych, bardzo "szerokich" akcji wykorzystujących mnóstwo ludzi i klocków - i do błyskawicznej adaptacji do sytuacji.
    * O: nie dąży do dowodzenia. Pasuje mu bycie w cieniu. Kroczy w cieniu tych wszystkich którzy zginęli, by on mógł żyć - i w cieniu swoich pomyłek.
    * O: musi być jak najlepszy. Trafia do jakiejś sytuacji i jego odpowiedzialnością jest ją rozwiązać i uratować wszystkich. Zawsze dąży do ratowania WSZYSTKICH.
    * nie dba o to, by mieć więcej, móc więcej czy być podziwianym. Chce uratować jak najwięcej istnień.
* Ocean
    * E:+ , N:- , C:+ , A:+, O:+
    * cierpliwy i precyzyjny, potrafi poczekać i osiągnąć sukces.
    * bardzo optymistyczny, dobry duch Inferni o ogromnej moralności. Bardzo opiekuńczy.
    * ma genialne plany, które zwykle działają. A jak nie - mają straszny rozmach.
* Silnik
    * Uratuje i osłoni wszystkich. Jest "dobrym ojcem" Inferni. Zna każdego z imienia. Zatroszczy się i o stan fizyczny i psychiczny.
    * Znajdzie dla Klaudii lepszy świat i lepsze możliwości.
    * Zobaczy jeszcze lepszy świat dla ludzkości.

### Typowe problemy z którymi sobie nie radzi; Słabości (-3)

* CORE WOUND: "Potrafię uratować wszystkich poza tymi, którzy są moją najbliższą rodziną i najbliższymi przyjaciółmi"
* CORE LIE: "Nie mogę się z nikim wiązać, bo stanowię zniszczenie. Czas na spokojną emeryturę."
* Martyn ma poważnie uszkodzone ciało i częściowo ducha przez to co się działo na wojnie. Pożera sześciopaki tabletek każdego ranka i każdego wieczoru. Musi je mieć dla stabilności.
* Martyn ma niestabilny, szukający arkin. Potrzebuje czerpać energię z innych; żywi się jak "wampir". Dlatego towarzyszą mu akolici i kultyści z Eterni, czego szczerze nienawidzi.
* Nadal nie potrafi się oprzeć damie w potrzebie. Martyn jest paladynem - nie zrobi czegoś, co uważa za złe i do tego nie dopuści by to się stało.
* Martyn nie kłamie. Może uniknąć mówienia całej prawdy, ale nie powie nigdy czegoś co jest kłamstwem.
* Nadal ma resentyment i problemy z noktianami. Po prostu im nie ufa. Kompensuje to lekami i środkami psychoaktywnymi, ale nie zawsze to działa.
* Mimo leków ma tendencje do agresji i działania skrajnego.
* Koszmary senne i traumy wynikające z tego co widział i zrobił podczas wojny. W końcu on był w tych bardziej chorych akcjach.
* Jego simulacrum (dziesiątki palców) jest niesamowicie niebezpieczne, wykorzystuje jego magię do degeneracji i zniszczenia.

### Magia (3M)

#### W czym jest świetny

* Leczenie, utrzymywanie przy życiu, staza. Ogólnie rozumiana "magia medyczna"
* Stymulacja, biokonstrukcja i różnego rodzaju środki psychoaktywne.
* Transfer energii + energii życiowej w warunkach nieprzewidywalnych (pole bitwy, seks itp.).

#### Jak się objawia utrata kontroli

* Martyn potrafi ZDUSIĆ w sobie Paradoks, acz to manifestuje simulacrum i krzywdzi sprzężonych z nim przez arkin sojuszników / akolitów
* Magia emocjonalna. Horrory wojny mogą wypłynąć i emocjonalnie wpłynąć na innych. Może pojawić się paranoja. Mogą być losowe akty miłości.
* Magia lecznicza przekształcona w bojową - straszne zniekształcenia i wypaczenia wynikające ze użycia magii leczniczej w celach bojowych.

### Specjalne

* .

## Inne
### Wygląd

Szpakowate, brązowe włosy, szare oczy, ubrany w lekko wypłowiały mundur. Nie nosi żadnych medali. Łagodny wygląd. Ma bardzo ładny głos. Jednocześnie ma tę iskrę, która pokazuje jakie z niego było ziółko w przeszłości.

### Coś Więcej

* .

### Endgame

* .

---
layout: cybermagic-karta-postaci
categories: profile
factions: "Arystokracja Aurum, Banda Lemurczaka, Nocny Krąg"
owner: "public"
title: "Sabina Kazitan"
---

# {{ page.title }}

## Kim jest

### Motto

"Odzyskam kontrolę, bo posunę się dalej niż Ty. Wszystko, co nie jest niezbędne, można odrzucić."

### Paradoksalny Koncept

Arystokratka, podległa Kamilowi Lemurczakowi. Badaczka zakazanych i niebezpiecznych rytuałów o bardzo silnej woli i niezłomnej, własnej moralności. Wyrzeźbiona jako potwór przez Lemurczaka, nauczyła się bezwzględności by chronić swoich bliskich. Degeneratka i miłośniczka imprez do utraty przytomności, która poświęci się dla kogoś kogo uważa za wartego przetrwania. Tak bardzo kocha wolność i samostanowienie, że jest skłonna spętać w kajdanach wszystkich innych.

## Mechanika

### Czym osiąga sukcesy (3)

* ATUT: badania i niebezpieczne rytuały; wiedza jest bronią
* ATUT: bardzo silna wola i rdzeń mentalno-emocjonalny; trudno ją ruszyć
* SŁABA: ciągnie do używek, pragnie czuć się wolna i swobodna
* SŁABA: negocjacje typu 'równy z równym'. ALBO jest silna ALBO słaba.

### O co walczy (3)

* ZA: gromadzenie władzy, wiedzy, potęgi; wiecznie jej mało
* ZA: zbudować własne, lepsze społeczeństwo z nią na szczycie
* VS: chce zniszczyć system arystokratyczny Aurum
* VS: zniszczyć wszystko powiązane z Lemurczakiem, winnych i niewinnych

### Znaczące Czyny (3)

* Torturami zmusiła służkę, która ją szpiegowała do stania się podwójnym agentem.
* Traktowana jak niewolnica, zniszczyła laboratorium Lemurczaka podmieniając reagenty.
* Po tym, jak wszyscy ją zostawili, skutecznie zarządzała (niewielkim) majątkiem samotnie.

### Kluczowe Zasoby (3)

* COŚ: Robot klasy 'Serratus' z TAI klasy Elainka; do walki i tortur. Imię: "Seilia", jak bogini.
* KTOŚ: Krąg Nocy - organizacja radykalnej ewolucji i wolności
* WIEM: sporo drobnych sekretów słabych aktorów Aurum; materiały do szantażu
* OPINIA: bezwzględna i niezłomna; zawsze w służbie Wielkich Aurum

## Inne

### Wygląd

Świdrujące, niezwykle intensywne zielone oczy. Śnieżnobiałe włosy, do ramion. Lekko opalona, zdrowa cera. Stosunkowo niska. Ubrana w kobiecy frak z zaklęciami autoczyszczenia.

### Coś Więcej

Bardzo niebezpieczna postać, zarówno jako sojusznik jak i przeciwnik. Inteligentna, oportunistyczna i zawsze dba o swoje przetrwanie.

---
layout: cybermagic-karta-postaci
categories: profile
factions: "pustogor, rekin, terienak, aurum"
owner: "kić"
title: "Karolina Terienak"
---

# {{ page.title }}

## Kim jest

### W kilku zdaniach

Neurosprzężona czarodziejka (słaba) z rodu Terienak o umierającej sentisieci. Przeciętnego wzrostu. Brązowe włosy do ramion, zwykle zebrane w krótki kucyk. Wysportowana, żadne tam delikatesy. Ubiera się zwykle w ciuchy motocyklowe, praktycznie zawsze ma na sobie żółwia.

### Co się rzuca w oczy

* Wesoła. Intensywna gdy nad czymś pracuje, ale normalnie wesoła, w dobrym humorze. Nieco wulgarny humor / odzywki, ale co z tego.
* Ze wszystkich osób najlepiej rozumie Arkadię Verlen i czuje do niej nić sympatii (odwzajemnioną).
* Na imprezach głośna, zwykle lekko wstawiona i świetnie się bawi.
* Lubi rywalizację, ale nie chce wygrywać nieuczciwie. Jest twarda i najlepsza, nie musi tego udowadniać. Nothing to prove.

### Jak sterować postacią

Reckless but not stupid. Potrafi zaskoczyć przemyślaną taktyką. Zawsze bezpośrednia i głośna. Nie przebiera w słowach. 
Nie kłamie, choć nie zawsze powie wszystko.
Nie do końca czuje się częścią Aurum. Gdyby mogła naprawić ród, zmieniłoby się to, ale wie, że jest w "borrowed time".
Szuka dla siebie miejsca poza Aurum, gdzieś tutaj najlepiej. Ale z jej umiejętnościami i niezależnością.
Nie toleruje brudnej polityki, rozgrywania ludźmi i traktowania ich jak marionetki i zasoby.
Gdyby znalazła sposób pomocy rodowi to go zrealizuje. Żal jej tej sentisieci.

## Mechanika

### Znaczące Czyny i Osiągnięcia (3)

* Gdy stary czołg rakietowy Yamarl się uszkodził i miał wybuchnąć, Karo używając neurosprzężenia i zdalnego połączenia magią spowolniła eksplozję by wszyscy zdążyli się ewakuować.
* Gdy była manipulowana przez polityków w Aurum to postawiła się i skopała jednego glanami. Juliusz Sowiński ją osłonił przed zarzutami i zesłał jako Rekinkę.

### Co się rzuca w oczy: Atuty, Przewagi, Zasoby (3, 6)

* AKCJA: Perfekcyjny Rekin. Waleczna arystokratka na ścigaczu. Umie walczyć nieczysto. Nie pojedynkuje się.
* AKCJA: Świetnie i nieortodoksyjnie pilotuje (zwłaszcza swój) ścigacz. A potem go naprawia (ale naprawia tylko swój).
* AKCJA: Inspiruje, dominuje, zastrasza. Jest niezależna, silna, zdecydowana - co ciągnie za sobą innych. Jednocześnie nie rzuca gróźb na wiatr i nie unika siły - dają jej co chce.
* AKCJA: Kontroluje roje pojazdów i maszyn. Jej neurosprzężenie pozwala jej na jednoczesną pracę z wieloma maszynami naraz.
* CECHA: Nerd ścigaczowy kwadrat. Czyta o nich. Szuka ich. Bada je. Wie o nich WSZYSTKO i jeszcze troszeczkę. To jest jej hobby, pasja i życie.
* COŚ: Aktywnie stara się mieć NAJLEPSZY ścigacz w okolicy, niezależnie od frakcji. Najszybszy, przeciążenia, mody, zwrotność. ZAWSZE uzbrojony.
* COŚ: Reputacja badass. Świetny modder ścigaczy. Nie podpadać jej, bo skopie lub zawoła brata.

### Typowe problemy z którymi sobie nie radzi; Słabości (-3)

* CORE WOUND: "Znam się z wieloma magami Aurum od zawsze. Ale moja sentisieć umiera. Oni zaczną traktować mnie jak plebs, odrzucą naszą przyjaźń."
* CORE LIE: "Pozycja i Tradycja są tylko iluzją. Będę zachowywać się wulgarnie, jak plebs itp - bo co za różnica. Odrzucę ich zanim oni odrzucą mnie."
* Niecierpliwa. Nie lubi czekać, nie lubi kombinować.
* Gardzi etykietą. Nie lubi formalności. Załatwmy to i będzie dobrze. Nie lubi i nie robi tego dobrze. Jak próbuje, wychodzi dziwnie.
* Jej magia jest naprawdę osłabiona. Była "typową" czarodziejką a neurosprzężenie bardzo tą moc osłabiło.
* Ma osoby w Aurum (niektórych polityków) które jej nie lubią. Skopała jednego który próbował nią rozgrywać swoje sprawy.

### Serce i Wartości (3)

* Wartości
    * TAK: Self-direction (będę sama i muszę sobie radzić), Security(zabezpieczyć siebie i brata), Face (tam, gdzie jestem dobra, ludzie o tym wiedzą)
    * NIE: Conformity (fuj, arystokracja), Power (władza maluje Ci cel na plecach),
* Ocean
    * E:+, N:-, C:0, A:-, O:+
	* Ognista, pełna pasji
	* Nie kłania się nikomu, z podniesioną głową
    * 
* Silnik
	* Muszę zbudować sobie przyszłość bez rodu, bo ród najpewniej wymrze.
	* Uratować ród jeśli to możliwe.

### Magia (3M)

#### W czym jest świetna

* Interfejsowanie z maszynami. Potrafi zdalnie połączyć się z różnymi urządzeniami i podpiąć je do swojego neurosprzęgu.
* Skanowanie maszyn. Potrafi znajdować ich słabe punkty, luki konstrukcyjne i obszary w których są uszkodzone lub "można więcej".
* Regeneracja maszyn. Mechanizmy wracają do swojej poprzedniej natury. To samo pancerze itp.

#### Jak się objawia utrata kontroli

* Jej magia po prostu przestaje działać. Znika.
* Maszyny zaczynają się disruptować i świrować; jak EMP (nie takiej mocy).

## Inne

### Wygląd

.

### Coś Więcej

* ?

### Endgame

* ?

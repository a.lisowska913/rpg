---
layout: cybermagic-karta-postaci
categories: profile
factions: "rekin, diakon, aurum"
owner: "public"
title: "Justynian Diakon"
---

# {{ page.title }}

## Kim jest

### W kilku zdaniach

Prawdziwy paladyn wśród Rekinów, zapatrzony w swojego idola - Rolanda Sowińskiego. Chciałby wszystkim pomagać i zapewnić, że ludzkość będzie zjednoczona przeciwko groźnej rzeczywistości. Szturmowiec i niezły organizator bez cienia ambicji, mag bojowy zakochany w pięknie. Lekko egzaltowany.

### Co się rzuca w oczy

* Jest tym, na co szkolili Rolanda Sowińskiego - prawdziwy skromny paladyn. 
* Bardzo dużo trenuje walkę, w ten sposób się wycisza. Lubi pomagać innym i ich trenować.
* Bardzo kocha piękno. Kolekcjonuje ładne rzeczy. Lubi być otoczony pięknem, zarówno obrazami jak i muzyką jak i osobami...
* Lekko egzaltowany. Czasami się zapatrzy w coś czy nie zorientuje się, że nikt go już nie słucha.
* Nie chce kłótni i walki. Ale jak walczy, jest morderczy.

### Jak sterować postacią

* Został wysłany na prowincję, by odzyskał wiarę w siebie po sprawie z Kultem Ośmiornicy i znalazł sobie fajną dziewczynę.
* Pragnie pomagać, odbudowywać, zdobywać elementy świata dla innych. Zależy mu na ciągłym postępie i robi co trzeba, by to realizować.
* Nigdy nie narzeka. Może westchnąć, ale zakasa rękawy i weźmie się do roboty.
* Współpracuje ze wszystkimi. Jeśli wiedza jest w AMZ, poprosi ich o pomoc. Jeśli wiedzę ma Eternia, poprosi ich o pomoc. Nie ma w nim pychy ani ambicji.
* Daje każdemu szansę. Nieważne kim jest, zawsze zakłada, że w drugiej osobie jest dobro i dobra intencja.
* Bardzo wyciszony, lubi medytację, ciszę i sztukę. Bardzo dużo trenuje walkę - w ten sposób się wycisza.
* Nie mówi nikomu, co mają robić. Nie rozkazuje. Raczej sugeruje. Przypadkowo został przywódcą frakcji Paladynów...
* Nie chce walczyć. Nie chce wyciągać broni. Ale jak wyciągnie broń, walczy na pełnej mocy i chce wygrać.

## Mechanika

### Znaczące Czyny i Osiągnięcia (3)

* Gdy był młodszy, próbował zinfiltrować pewną grupę. To był Kult Ośmiornicy. Zwerbowali go a on pociągnął za sobą innych - trzeba było go ratować.
* Pozyskał obraz (akt), na którym bardzo mu zależało. Ale oddał go innemu arystokracie, by ten pomógł Justynianowi ochronić wioskę w jego domenie.
* Gdy Sensacjusz potrzebował pomocy pielęgniarza, nie zawahał się - rzucił wszystko by pomóc. Od tego zależało w końcu zdrowie.

### Co się rzuca w oczy: Atuty i Przewagi (3, 6)

* AKCJA: Inspiruje własnym działaniem. Niekoniecznie najlepiej coś robi, ale zawsze próbuje realizować jakiś plan. Inni widząc to idą za jego śladem. Czyny, nie słowa. 
* AKCJA: Doskonale mediuje i łagodzi konflikty. Niezły w negocjacjach i proszeniu o wsparcie. Zawsze wysłucha i zapewni, by wszyscy czuli się w miarę dobrze z rozwiązaniem.
* AKCJA: Włada bronią białą, średniego zasięgu i servarami średniego zasięgu. Klasyczna konfiguracja szturmowca.
* AKCJA: Niezły organizator - potrafi zarządzać ludźmi i zapasami. Niezły w taktyce, acz są lepsi.
* CECHA: Piękny okaz Adonisa. Dziewczyny go kochają. Otoczony wianuszkiem dziewczyn. I on też nie jest im obojętny.
* COŚ: Dużo dzieł sztuki różnego rodzaju, często bardzo drogich. Otoczony pięknem.

### Serce i Wartości (3)

* Pokora
    * Pragnie, by wszyscy współpracowali i dobrze funkcjonowali jako grupa. Chce współpracować ze wszystkimi siłami i frakcjami (AMZ, Rekiny, terminusi...). Nie lubi konfliktów międzyludzkich.
    * Nie chce dowodzić i nie czuje się na siłach by dowodzić. Aktywnie odmawia przejęcia dowodzenia politycznego nad Rekinami, choć nie unika odpowiedzialności. Dopasuje się do godnego przywódcy.
    * Tendencje do deeskalowania. Woli pojedynki Rekinów niż sport z opłaconymi ludźmi. 
    * Jeśli spotyka się z lepszym planem kogoś innego, z przyjemnością go zaadoptuje. Nie ma nawet cienia ambicji.
* Dobrodziejstwo
    * Chce, by Rekiny pełniły rolę paladynów. Chce, by na tym terenie była pomoc. Uważa, że odpowiedzialnością silniejszych jest pomagać słabszym.
    * Sprawiedliwość. Jeżeli ktoś zawinił, niezależnie kim jest, musi być oddany w ręce sprawiedliwości. Nie daruje Sabinie Kazitan itp. Sprawiedliwość jest nawet ponad nim - i ponad prawem.
    * Jeżeli ktoś słabszy jest atakowany przez silniejszego, zainterweniuje. Dojdzie potem co się stało, ale nie ma samosądów. Nie zgadza się na "silniejszy ma rację".
* Osiągnięcia
    * Ogromną radość sprawia mu odbudowa, konstrukcja, pozostawianie czegoś większego, co go przetrwa. Zwłaszcza jak to jest dotykalne i namacalne.
    * Rycerskość i umiejętności walki traktuje jako sztukę. Jest dla niego powodem do dumy ciągłe doskonalenie swoich umiejętności maga bojowego.
    * Prawdziwy wojownik zrobi co trzeba, by rzeczywistość szła do przodu. Jak trzeba, łapie za łopatę i kopie. Adaptacja. Robi to, co trzeba a nie to, co by chciał czy w czym jest najlepszy.
    * Osoba o najwyższych kompetencjach i największych osiągnięciach powinna dowodzić danym obszarem. Czyny nad słowa.

### Typowe problemy z którymi sobie nie radzi; Słabości (-3)

* CORE WOUND: "Byłem słabszy niż Kult Ośmiornicy. Stałem się jednym z nich. Wciągnąłem innych. Jestem niebezpieczny. Zraniłem tak wielu..."
* CORE LIE: "Muszę być ostrożny i delikatny. Nie mogę robić szerokich działań, bo znowu wszystkich zranię. Muszę skupić się na SOBIE i dojść do perfekcji zanim mogę prowadzić innych."
* Zawsze zakłada dobrą wolę ze strony drugiej osoby - da się go stosunkowo łatwo oszukać. Daje drugą czy trzecią szansę. 
* Sabina Kazitan. Ma na nią hate boner. Chce ją zniszczyć. Bo zniszczyła jego idola - Rolanda.
* Kult Ośmiornicy. Musi go zniszczyć za wszelką cenę. Każdą instancję. Każde działanie. Boi się Kultu i chce go zniszczyć - łatwo go ściągnąć gdzieś mówiąc o Kulcie.
* Jest BARDZO podatny na wdzięki niewieście. Potrafi rozproszyć się podczas bitwy, bo mignął mu biust. Ogólnie, nie lubi walczyć z dziewczynami.

### Magia (3M)

#### W czym jest świetna

* Entropia: potrafi osłabiać czy rozpadać materię i żywe istoty. Mistrz degeneracji.
* Błyskawice: potrafi wywołać potężne wyładowania czy łuki elektryczne. Spali niejedno urządzenie. Zasila się istniejącym prądem dla wybitnego efektu.
* Rośliny: przede wszystkim szybki wzrost roślin czy ich wzmocnienie; ta trawa może być jak żyletki i rosnąć bardzo szybko.

#### Jak się objawia utrata kontroli

* Uderzenie entropiczne; osłabienie jego czy innych dookoła. Poczucie ogólnej niemocy (fizycznej i magicznej).
* Nieprawdopodobnie potężny piorun uderza gdzieś niedaleko. Auć.

### Specjalne

* .

## Inne

### Wygląd

* 

### Coś Więcej

* .

### Endgame

* ?

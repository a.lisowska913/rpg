---
layout: cybermagic-karta-postaci
categories: profile
factions: "orbiter, oddział verlen, noktianin"
owner: "public"
title: "Raoul Lavanis"
---

# {{ page.title }}

## Kim jest

### W kilku zdaniach

Noktiański advancer Inferni, starszy koleś (jak na advancera). Doskonale zna archaiczny noktiański, interesuje się archeologią i archeotechem oraz im mniej ma z magią do czynienia tym czuje się lepiej. Spostrzegawczy snajper, doskonały w operacjach próżniowych. Dopiero tu - na Astorii - został neurosprzężony i połączony z eidolonem. Najlepiej czuje się w kosmosie, lubi przebywać poza statkiem. W sercu wie, że rzeczywistość nie ma sensu i się ciągle ZMIENIA. Miał pecha widzieć ze snajperki oblicze Saitaera i walczyć z lustrem Arazille.

### Co się rzuca w oczy

* Motto: "Nigdy nie da się niczego zaplanować, nigdy nie wiesz czy np. nie zmienią Ci się prawa fizyki czy nie pojawi nowy bóg."
* Nawet noktianie uważają go za dziwnego. Najchętniej jest gdzieś na boku i dąży do ciszy i samotności.
* Ma bardzo dobre pomysły, szybko je wymyśla i szybko się adaptuje.
* Jest chyba najbardziej ciekawską osobą jaką można spotkać. Wszystko próbuje ZROZUMIEĆ - zupełnie jak Klaudia.
* Czasem coś powie. Dziwnego. Coś co świadczy, że NIE DO KOŃCA TU JEST. Np. żart typu "jak pi będzie wynosiło 3.11" i się zaśmieje z żartu.
* Nie unika magii ani anomalii. Wyraźnie nie traktuje ich jako coś dziwnego. Dla niego WSZYSTKO jest dziwne.

### Jak sterować postacią

* Trzyma się na uboczu zarówno magów jak i ludzi. Unika absolutnie wszystkich. Wykonuje misję, współpracuje z innymi, ale woli iść na bok niż się socjalizować.
* Najchętniej przebywa w kosmosie i patrzy w gwiazdy w ciszy. To nie melancholia, to jego forma regeneracji - spacer kosmiczny.
* Uważa TAI i BIA za pełnoprawne istoty żywe i tak je traktuje - acz nie próbuje naprawić ich losu by samemu nie zniszczyć tego co zapewniła im Arianna. 
* Nerd, regularnie ucieka w książki i bibliotekę. Próbuje ZROZUMIEĆ co się stało, jak wszystko działa. Zrozumienie jest jego obsesją.
* Bardzo irytują go nielogiczności i nie pasujące do siebie elementy równania.

## Mechanika

### Znaczące Czyny i Osiągnięcia (3)

* To on doszedł do tego, że to Tal Marczak próbuje zniszczyć Tivr. Przekazał to innym noktianom. Skąd wiedział? Zmienił się wzór zachowań Tala.
* Zestrzelił lustro Arazille sterujące pewnym oficerem na fregacie z zewnątrz fregaty (kosmos), używając snajperki elmag i wykorzystując zakrzywienie grawitacyjne magią.
* Ściśle współpracował z Orbiterem po napotkaniu Saitaera i po tym jak zrozumiał, że nie rozumie rzeczywistości. M.in. dzięki niemu dano szansę noktianom. Nigdy nie próbował zdrady.

### Co się rzuca w oczy: Atuty, Przewagi, Zasoby (3, 6)

* PRACA: neurosprzężony advancer, snajper i specjalista od operacji próżniowych
* PRACA: bardzo interesuje się archeotechem i kulturą noktiańską; zna różne stare języki noktiańskie
* PRACA: całkiem niezły aktor; potrafi nieźle kłamać i zachowywać się jak nie-on
* PRACA: jeden z najlepszych detektywów jakich ma Infernia. Zarówno z zamiłowania ("zrozumieć") jak i z cech (drobiazgowość). Wszystko zauważa.
* CECHA: bardzo drobiazgowy, mało rzeczy mu unika. Od razu zauważa jak coś do czegoś nie pasuje. To nie jest paranoja, to cholerna irytacja.
* COŚ: dobrej klasy sprzęt - eidolon oraz snajperka elmag.

### Typowe problemy z którymi sobie nie radzi; Słabości (-3)

* CORE WOUND: "Wpierw straciłem rodzinę na Noctis. Potem okazało się, że wszystko w co wierzyłem jest kłamstwem. Rzeczywistość NIE MA SENSU i NIE JEST MOŻLIWA."
* CORE LIE: "Muszę zrozumieć, co się dzieje w tym świecie. Przez zrozumienie - odzyskam kontrolę. Bo inaczej ZNOWU wszystko się zmieni."
* Jest bardzo podatny na pętle logiczne, teorie spiskowe itp. Widzi wzory gdzie ich nie ma i potrafi oddać im za dużo czasu i uwagi.
* Dla wiedzy wejdzie w pułapkę. Paradoksalnie, nie zauważy że wpada w pułapkę.
* Ma wątpliwości na wszelkie możliwe tematy. Wie, że nie rozumie. Wie, że nie wie. Często nie ufa zmysłom i pamięci. Sprawdza wszystko kilka razy.

### Serce i Wartości (3)

* Wartości
    * TAK: Conformity (rozpłynąć się), Humility (nie jesteśmy w stanie ogarnąć), Benevolence (we're in it together)
    * NIE: Power, Prestige, Achievement (all: pointless in the face of reality!)
    * Raoul chciałby być niewidoczny, rozpłynąć się w większej grupie i im wszystkim pomóc. Jednocześnie zawsze stoi z boku, nie wie co się ze światem stanie TERAZ.
    * Nie widzi sensu w akumulacji środków, wiedzy czy czegokolwiek - świat jest zbyt polimorficzny.
    * Zawsze pomoże jak jest w stanie. Jest bardzo spokojny i poczciwy.
* Ocean
    * E:-, N:+, C:+, A:0, O:+
    * Bardzo pomysłowy, zawsze znajdzie inne wyjście z sytuacji i szybko kombinuje.
    * Wszędzie widzi wzory, nawet tam, gdzie ich nie ma - i często widzi je jako niebezpieczeństwo.
    * Nie znosi czegoś NIE ROZUMIEĆ. Ma tendencję do obsesji na punkcie rzeczy których nie rozumie.
* Silnik
    * Zrozumieć rzeczywistość, zrozumieć ją za wszelką cenę. Dojść do tego jak rzeczywistość działa.
    * Zintegrować noktian z Astorią. Nie ma innej opcji - wszyscy jesteśmy ludźmi i wszyscy musimy działać przeciwko Rzeczywistości.
    
## Inne

### Wygląd

.

### Coś Więcej

* 2-3 lata starszy niż Martyn.

### Endgame

* ?

---
layout: cybermagic-karta-postaci
categories: profile
factions: "orbiter, kirasjerka orbitera, orbiter neomil"
owner: "public"
title: "Mirela Orion"
---

# {{ page.title }}

## Kim jest

### Koncept

Emulatorka Kirasjerów. Kiedyś: Emilia, ale przez działania Pięknotki odzyskała przejawy świadomości. Mirela jest z charakteru bardziej paladynką, dość momentami porywczą - ale skupioną na ochronie wszystkich i wszystkiego. Jak na Emulatorkę, łatwo się zaprzyjaźnia. Nieco nieśmiała; nie wie jak działać poza programowaniem i w tym świecie. Uważa się za narzędzie, nie za osobę.

### Motto

"W kosmosie jest mrok i zło. Potrzebujesz takich jak ja, by móc żyć w świetle i w cieple. Tacy jak Ty mnie wyrzucą, jak moja misja będzie skończona - i dobrze. Zużyte narzędzia trzeba wymienić."

### Co się rzuca w oczy

* Mirela nie pozwoli, by komuś stała się krzywda. Będzie chronić życia i zdrowie zgodnie z parametrami misji, nawet naginając rozkazy.
* Dość porywcza jak na Emulatorkę, ale nie przeszkadza jej to w działaniu. Misja > wszystko.
* Uważa się nie za osobę a za narzędzie. Uważa, że tak powinno być - jest zbyt niebezpieczna.
* Czuje ogromne pokrewieństwo z ludźmi i ludzką rasą, mimo, że jest Emulatorką. Ma pozytywne spojrzenie, nieważne, jak na nią patrzą.
* Dość sympatyczna, choć jest całkowicie nieżyciowa i nie rozumie tej rzeczywistości.
* Gdy jest zagubiona, jest lekko nieśmiała. Dodatkowo porusza się z taką ostrożnością jakby nie chciała zrobić krzywdy niewspomaganym.

## Mechanika

### Znaczące Czyny i Osiągnięcia (3)

* Gdy przekraczała jako nastolatka granice swojej mocy, doprowadziła do śmierci swojej rodziny. To sprawiło, że sama zgłosiła się do projektu 'Emulator'.
* Gdy poznała prawdę o Emulatorkach i jak to wszystko działa, stanęła po stronie Kirasjerów by chronić zespół przed Szaloną Emulatorką.
* Przekroczyła programowanie by chronić ludzi - zawalając misję. Wydawało się, że dla Emulatorki takie działanie jest niemożliwe.

### Z czym przyjdą do niej o pomoc? (3)

* 

### Jaką ma nieuczciwą przewagę? (3)

* Emulatorka - potrafi przełączyć swój zestaw umiejętności na taki, jaki ma załadowany. Ma slot na 3 zestawy.
* Homo Superior - świetnie uzbrojona, zdolna do działania w próżni, otoczona polem magicznym. Katai.
* Szczerze wierzy w Kirasjerów, Orbiter NeoMil a zwłaszcza w Damiana Oriona.

### Charakterystyczne zasoby (3)

* COŚ: 
* KTOŚ: 
* WIEM: 
* OPINIA: 

### Typowe sytuacje z którymi sobie nie radzi (-3)

* 

## Inne

### Wygląd

.

### Coś Więcej

* Skażona przez Cienia i wysoce uwolniona. Do tej pory Kirasjerzy nie znaleźli sposobu, jak ją naprawić.

### Endgame

* ?

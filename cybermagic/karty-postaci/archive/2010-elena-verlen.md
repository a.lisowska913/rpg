---
layout: cybermagic-karta-postaci
categories: profile
factions: "orbiter, arystokracja aurum, oddział verlen, ród karmadan, orbiter neomil"
owner: "public"
title: "Elena Mirokin"
---

# {{ page.title }}

## Kim jest

### W kilku zdaniach

Arystokratka Aurum gardząca Aurum, oficer próbująca być świętszą od admirała, świetny pilot małych VTOLi oraz mistrzyni pojedynków o niewyparzonej gębie. Bardzo wspomagana przez visiat. Próbuje zachowywać się godnie, ale miotają nią silne uczucia. Nie odmawia rywalizacji. Z zawodu - advancer.

### Co się rzuca w oczy

* Nie jest szczególnie piękna, ale ma absurdalne powodzenie u NIEKTÓRYCH mężczyzn (czego unika). Krew Diakonki?
* Naprawdę kocha swoją maszynę i swoją technologię. To nie są dla niej tylko przedmioty.
* Absurdalnie skupiona na rywalizacji. Praktycznie nie wycofuje się ze starcia, niezależnie od jej stanu.
* Szanuje kompetencję i umiejętności, nawet jeśli nie lubi osoby. Nie cierpi pozerstwa.
* Arogancka, nie wierzy, że ktoś może być lepszy od niej. Jeśli jest - trenuje do padnięcia. Ale wygra.
* Porywcza i bardzo wybuchowa. Nie potrafi najlepiej kontrolować swojego temperamentu.
* Żąda bycia docenioną i walczy o jak najlepszą karierę - oraz o to, by wszyscy działali jak najlepiej.
* Zwalcza pasożytniczą arystokrację i bezwartościowych żołnierzy.
* Ma kiepską opinię o nie-militarnych siłach ludzkości. Jest tu dla walki.

## Mechanika

### Znaczące Czyny i Osiągnięcia (3)

* W pojedynku z Eustachym (naprawa-sabotaż) wspomagana visiatem utrzymała się naprawiając Infernię aż Persefona musiała ich odciąć.
* Jako oficer Castigatora była TERROREM arystokracji Aurum; niby jedna z nich, ale upokarzała i tępiła tworząc jakiś porządek.
* "Lot Walkirii" małą korwetką, gdy była sprowokowana przez Ariannę. Całkowicie szkodliwy lot dla statku, ale pobiła rekord tej korwety.

### Co się rzuca w oczy: Atuty i Przewagi (3, 6)

* PRACA: Świetny pilot, małe statki są jak jej ciało.
* PRACA: Niezły mechanik, zwłaszcza niewielkich statków.
* SERCE: Zapalczywa, łatwa do sprowokowania i niezłomna.
* SERCE: Rana związana z arystokracją i pasożytami; żąda wysokiej klasy zachowań i kompetencji.
* ATUT: Perfekcyjna kontrola przestrzeni: zawsze CZUJE gdzie jest i co jest dookoła niej.
* ATUT: Wspomaganie visiańskie: silne emocje (gniew, strach) i ból to uruchamiają - pilotaż, reakcje, walka. Dotyka Esuriit.

### Charakterystyczne zasoby (3)

* COŚ: Kosmiczny ścigacz klasy 'Iquitas', dostarczony przez NeoMil.
* KTOŚ: Powiązana z NeoMil w Orbiterze. Konkretnie: z Damianem Orionem.
* WIEM: Dokładnie wie jak daleko może się posunąć statek kosmiczny klasy korweta czy fregata. Mistrzyni małych statków.
* OPINIA: Łatwa do sprowokowania, skandalistka nie kontrolująca własnej magii.

### Typowe problemy z którymi sobie nie radzi (-3)

* CIAŁO: Jej ciało jest nadwrażliwe na alkohol, psychodeliki, inne takie. Nie powinna pić itp.
* CIAŁO: Bardzo wrażliwa na wszystkie bronie anty-anomalne przez głęboką integrację visiańską.
* SERCE: Trywialna do sprowokowania - po prostu NIE UMIE zostawić żadnej urazy na boku.
* SERCE: Nie radzi sobie z konfliktami inaczej niż przez eskalację lub unikanie. Zwykle eskaluje.
* MAGIA: Niektórzy faceci się w niej zakochują na zabój; ona nie chce i nie umie.
* MAGIA: Jej moc magiczna praktycznie nie działa jeśli nie jest pod wpływem silnych emocji lub bólu.

### Specjalne

* Do wszystkich testów "prowokowania Eustachego do złego" ma stabilną, skuteczną kategorię wyżej.
* Wszystkie testy "prowokowania Eleny do złego" przez Eustachego mają stabilną, skuteczną kategorię wyżej.

## Inne

### Wygląd

.

### Coś Więcej

* Wiem, że ucieka od czegoś w Aurum. Nie wiem jeszcze czego.
* Ma romans z Eustachym ;-). Tzn. nie ma. Ale są plotki.

### Endgame

* Damian: przekształci ją w Emulatorkę
* Ktoś: ożeni się z nią
* Elena: zostanie komodorem na jakiejś odległej misji (np. na Netrahinie)? Bo nie zostanie admirałem, to już wie.

---
layout: cybermagic-karta-postaci
categories: profile
factions: "blakenbauer"
owner: "public"
title: "Karolina Terienak"
---

# {{ page.title }}

## Kim jest

### W kilku zdaniach



### Co się rzuca w oczy

* Skóra, żółw (pancerz) na plecach, wypasiony ścigacz. Ubiera się tak, by było jej wygodnie, a wygodnie jest jej w stroju motorowym.
* Na imprezach głośna, zwykle lekko wstawiona i świetnie się bawi.

## Mechanika

### Znaczące Czyny i Osiągnięcia (3)

* 

### Co się rzuca w oczy: Atuty i Przewagi (3)

* AKCJA: Świetnie i nieortodoksyjnie pilotuje (zwłaszcza swój) ścigacz. A potem go naprawia (ale naprawia tylko swój).
* AKCJA: Jak trzeba to wklupie. Umie walczyć. Raczej nie walczy czysto.
* AKCJA: 
* COŚ: 
* COŚ: 

### Serce i Wartości (3)

* Stymulacja
 * Szuka adrenaliny. Niech coś się dzieje.
 * Im bardziej imponujące działanie, tym lepiej.
* Twarz
 * Lubi imponować, będzie się popisywać i udowadniać, że jest świetna.
 * Nie chce udowadniać, że umie coś, czego nie umie. 
 * Chce mieć uznanie za to, czym jest.
* Samosterowność
 * Nikt nie będzie mi mówił, co mam robić. Ja wiem najlepiej, co mogę i chcę zrobić.
 * Fakt, że jesteś Sowiński nie daje ci żadnego prawa.
 * Chcesz mojego szacunku to musisz na niego zapracować.


### Typowe problemy z którymi sobie nie radzi; Słabości (-3)

* Niecierpliwa. Nie lubi czekać, nie lubi kombinować.
* 
* 

### Specjalne

* .

## Inne
### Wygląd

.

### Coś Więcej

* .

### Endgame

* .

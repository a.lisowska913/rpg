---
layout: cybermagic-karta-postaci
categories: profile
factions: "cieniaszczyt, lyse_psy"
owner: "public"
title: "Bogdan Szerl"
---

# {{ page.title }}

## Postać

### Ogólny pomysł (3)

Medyk i doktor Łysych Psów, też mindwarp i ogólnie - neuronauta. Pasjonuje się w kontroli umysłów i dominacji innych. Na krótkiej smyczy Moktara. Naukowiec wysokiej klasy. Potrafi walczyć, acz nie jest w tym świetny. Używa licznych artefaktów; w szczególności dominacyjnych, unieruchamiających i paraliżujących.

### Motywacja (gniew, zmiana, sposób) (2)

* upokarzają mnie i nie szanują; będą mnie wielbić i uważać za bóstwo; rekonstrukcja i dominacja ciała i umysłu
* wolność, swoboda i zwiewność muszą być kontrolowane; wszelkie piękno będzie kontrolowane przez niego; znajduje odpowiednie cele i próbuje nimi zawładnąć
* uśmiechnięty, lekko nerwowy, okrutny, karmi się dominacją, często się oblizuje

### Wyróżniki (2)

* Mindwarp: potrafi każdego złamać, ulojalnić czy przekształcić
* Lekarz i naukowiec: skupia się na szybkim postawieniu bardziej niż na ograniczeniu cierpienia. Bardzo skuteczny w odnajdowaniu nietypowych rozwiązań

### Epicki moment (2)

* zostaje sam na sam z kimś i bardzo szybko robi z tej osoby swojego wiernego sługę
* mając trudny problem medyczny potrafi znaleźć rozwiązanie niemoralne, ale bardzo skuteczne

### Zasoby i otoczenie (3)

* liczne artefakty paraliżujące, unieruchamiające, dominacyjne
* kolekcja upiornych holokostek Mausów; doskonałych na różne okazje
* stary kompleks medyczny; na terenach Skażonych na wschód od Cieniaszczytu. W połowie zakopany pod ziemią, ma opcje portali.

### Magia (3)

#### Gdy kontroluje energię

* dostosowywanie ciała i umysłu do swojej woli: jest to wyjątkowo unikalny sposób magii medycznej...
* wszelkie formy unieruchomienia bądź spętywania swojej ofiary, czy to fizycznie czy mentalnie
* magia skupiająca się na efektach takich jak: dominacja, kontrola, uwielbienie

#### Gdy traci kontrolę

* przebłyski szaleństwa; czy to u siebie czy innych - prawdziwe pragnienia czy JEGO pragnienia manifestują się w formie niemożliwych do kontroli impulsów

### Powiązane frakcje

{{ page.factions }}

## Opis

.

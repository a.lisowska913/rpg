---
layout: cybermagic-karta-postaci
categories: profile
factions: "orbiter, oddział verlen"
owner: "public"
title: "Martyn Hiwasser"
---

# {{ page.title }}

## Kim jest

### W kilku zdaniach

5x-letni lekarz i bohater wojenny na emeryturze. Kobieciarz, który nigdy nie skrzywdziłby kobiety. Wybitny biokonstruktor, który był członkiem gangu, medykiem wojskowym, przez moment tienem Eterni a teraz jest głównym lekarzem na Inferni. Zachowuje optymizm i determinację, że da się każdemu pomóc do samego końca. Niespodziewanie niebezpieczny w walce - używa pięści i magii. Specjalizuje się w stymulantach i utrzymywaniu przy życiu.

### Co się rzuca w oczy

* Motto: "Utrzymam wszystkich przy życiu - dla uroczej pani kapitan."
* Martyn nie kłamie. Może uniknąć mówienia całej prawdy, ale nie powie nigdy czegoś co jest kłamstwem.
* Zawsze pogodny, taki trochę "kamerdyner", w cieniu. Uprzejmy i elegancki. Rzadko pokazuje nadmiar emocji.
* Nigdy nie opowiada opowieści ze swojej przeszłości, z wojny itp. "Niech przeszłość pozostanie tam, gdzie jej miejsce".
* Jego najbliższą przyjaciółką na Inferni jest Klaudia. Najlepiej się dogadują. Są też charakterologicznie najbardziej podobni.
* Nie ma ambicji. Nie chce dowodzić. Z przyjemnością odda się pod dowództwo kogoś innego.
* Ma tendencje do SKRAJNIE ryzykownych manewrów. Z perspektywy analizy ryzyka jest zawsze na krawędzi. On nie umrze w swoim łóżku.
* Nadal kocha piękne kobiety - i jest świetny zarówno w podrywaniu i ich oczarowywaniu jak i w sprawach łóżkowych. "Biomanta", anyone?
* Nie mówi złych rzeczy o żadnej grupie - Eternia, Zjednoczenie, Aurum, Orbiter. Dla niego nie ma "złych grup", tylko jednostki (wyjątek: nic nie mówi o noktianach)

### Jak sterować postacią

* "War butler". Kamerdyner, w cieniu, spokojny. Do momentu aż musi działać - wtedy jest zdecydowany.
* Jeśli działa, to działa z ogromnym rozmachem. Wykorzystuje wszystkich i wszystko.
* Bardzo uprzejmy i kulturalny. Nie kłamie. Zostaje w cieniu.
* Każdą sytuację próbuje rozwiązać, narażając siebie przede wszystkim.
* Jego historia się już skończyła; jest na emeryturze. Czas pomóc młodym, którzy nie znają _horroru_ tego świata.

## Mechanika

### Znaczące Czyny i Osiągnięcia (3)

* Wraz z Ateną Sowińską uratowali część załogi ASD 'Perceptor' przed Serenitem; on stymulował ludzi, ona maszyny. Uciekli, acz z ofiarami.
* Zatrzymał konflikt między dwoma frakcjami Orbitera, bo... zaprzyjaźnił się z damami dowodzącymi oboma frakcjami i zmusił je do rozmowy.
* Gdy 'Infernia' umierała po walce z Leotisem, przesunął wielu ludzi do działania w próżni magitechowo - by mogli utrzymać statek przy życiu.

### Co się rzuca w oczy: Atuty i Przewagi (3)

* AKCJA: mistrz networkingu, który ma gadane. Czar i urok osobisty, zwłaszcza wobec kobiet. Bardzo społeczny. Deeskaluje starcia samą swoją obecnością. Używa wszystkich zasobów ludzkich do jakich ma dostęp.
* AKCJA: doskonały lekarz i biokonstruktor, potrafi utrzymać przy życiu sytuacje beznadziejne. Doskonałe wyczucie sytuacji jak chodzi o tematy medyczne.
* AKCJA: ma przeszkolenie sił specjalnych i ponad 20 lat aktywnej służby na froncie wojny Orbiter - Noctis. "Widział praktycznie wszystko". Stary weteran.
* AKCJA: specjalizuje się w natarciach próżniowych i działaniach w próżni.
* COŚ: Niezależnie od okoliczności, Martyn ZAWSZE ma pod ręką pasujący strój kobiecy na każdą okazję. No questions asked.
* COŚ: Ogromna ilość medali, odznaczeń, osiągnięć, wrogów i przyjaciół w niespodziewanych miejscach. I - o dziwo - tytuł 'tien' z Eterni, który jest już tylko honorowy. Wszystko ukrywa.

### Serce i Wartości (3)

* Dobrodziejstwo
    * zapewnić uśmiech na twarzy każdej osoby. Uratować każdego, komu można pomóc. Zrobić zmianę tej rzeczywistości.
    * pomoc innym i wspieranie ich jest obowiązkiem moralnym, nie czymś co się warunkowo daje lub odbiera.
    * nie akceptuje krzywdzenia złych istot by uratować więcej dobrych istot. Eternia nauczyła go, czym to się kończy.
* Stymulacja
    * niezależnie jak źle by sytuacja nie wyglądała, ma wiarę - często przeszacowaną - w swoje umiejętności i możliwości.
    * Martyn nie jest w stanie robić długo tego samego. Nowe przeżycia. Nowe osoby. Nowe działania. Nowe zagadki. Musi mieć zawsze COŚ NOWEGO.
    * ma tendencje do bardzo ryzykownych, bardzo "szerokich" akcji wykorzystujących mnóstwo ludzi i klocków - i do błyskawicznej adaptacji do sytuacji.
* Osiągnięcia
    * nie dąży do dowodzenia. Pasuje mu bycie w cieniu. Kroczy w cieniu tych wszystkich którzy zginęli, by on mógł żyć - i w cieniu swoich pomyłek.
    * musi być jak najlepszy. Trafia do jakiejś sytuacji i jego odpowiedzialnością jest ją rozwiązać i uratować wszystkich. Zawsze dąży do ratowania WSZYSTKICH.
    * nieprzygotowany lekarz na statku typu Infernia to kolejny medal za przeżycie gdy wszyscy inni zginęli...

### Typowe problemy z którymi sobie nie radzi; Słabości (-3)

* CORE WOUND: "wszyscy, na których mi zależy rozpadają się w proch i umierają na moich rękach. I to moja wina."
* CORE LIE: "nie jestem dość dobry, by utrzymać moich najbliższych przy życiu. Nigdy nie będę dość dobry. Nie mogę się z nikim wiązać, bo stanowię zniszczenie."
* Martyn ma poważnie uszkodzone ciało i częściowo ducha przez to co się działo na wojnie. By to ominąć, pożera sześciopaki tabletek każdego ranka i każdego wieczoru. Musi je mieć by wszystko działało.
* Nadal nie potrafi się oprzeć damie w potrzebie. Martyn jest paladynem - nie zrobi czegoś, co uważa za złe i do tego nie dopuści by to się stało.
* Martyn nie kłamie. Może uniknąć mówienia całej prawdy, ale nie powie nigdy czegoś co jest kłamstwem.
* Nadal ma resentyment i problemy z noktianami. Po prostu im nie ufa. Kompensuje to lekami i środkami psychoaktywnymi, ale nie zawsze to działa.
* Mimo leków ma tendencje do agresji i działania skrajnego.
* Koszmary senne i traumy wynikające z tego co widział i zrobił podczas wojny. W końcu on był w tych bardziej chorych akcjach.

### Magia (3M)

#### W czym jest świetny

* Leczenie, utrzymywanie przy życiu, staza. Ogólnie rozumiana "magia medyczna"
* Stymulacja, biokonstrukcja i różnego rodzaju środki psychoaktywne.
* Transfer energii + energii życiowej w warunkach nieprzewidywalnych (pole bitwy, seks itp.).

#### Jak się objawia utrata kontroli

* Martyn potrafi ZDUSIĆ w sobie Paradoks, acz to mu pali kanały i uniemożliwia dalsze czarowanie - plus negatywne konsekwencje do testów.
* Magia emocjonalna. Horrory wojny mogą wypłynąć i emocjonalnie wpłynąć na innych. Może pojawić się paranoja. Mogą być losowe akty miłości.
* Magia lecznicza przekształcona w bojową - straszne zniekształcenia i wypaczenia wynikające ze użycia magii leczniczej w celach bojowych.

### Specjalne

* .

## Inne
### Wygląd

Brązowe włosy, szare oczy, ubrany w lekko wypłowiały mundur. Nie nosi żadnych medali. Starszy już czarodziej z łagodnym wyglądem. Ma bardzo ładny głos. Jednocześnie ma tę iskrę, która pokazuje jakie z niego było ziółko w przeszłości.

### Coś Więcej

* .

### Endgame

* .

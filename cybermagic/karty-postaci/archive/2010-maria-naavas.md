---
layout: cybermagic-karta-postaci
categories: profile
factions: "orbiter, żelazko"
owner: "public"
title: "Maria Naavas"
---

# {{ page.title }}

## Kim jest

### W kilku zdaniach

Lekarka "Żelazka", która tak samo skutecznie leczy jak zabija. Podrywaczka i kolekcjonerka facetów, namawiająca wszystkich do życia pełnią życia póki są w stanie. Sympatyczna i lojalna socjopatka nie mająca granic ani wyrzutów sumienia. Bardzo zależy jej na integracji Orbitera i na silnym Orbiterze, zdolnym do obrony przed co straszniejszymi Anomaliami Kosmicznymi.

### Co się rzuca w oczy

* Wraz ze swoim dowódcą, Olgierdem, założyła "kapelę karaoke", z której jest piekielnie dumna. Ona jest na growlu...
* Uwielbia bawić się w swatkę; zwłaszcza chce zeswatać Olgierda z kimś jego godnym.
* Unika wszelkiego typu holoprojekcji i virtu. Jej miejsce jest tu, w świecie realnym.
* Podrywaczka. Sama o sobie mówi "kolekcjonerka facetów". Ma sex appeal i nie waha się go używać.
* Jest silna i umie walczyć. Nie boi się zaatakować pierwsza.
* Uważa, że Orbiter może nie jest perfekcyjny, ale jest sprawny i potrzebny. Będzie Orbiter wspierać - bo widziała horror.
* Nie ma w naturze rywalizacji, nie próbuje nikomu też nic udowodnić. Jest tym czym jest.
* Zawsze uśmiechnięta, często lekko rozmarzona, na twarzy wyraz lekkiego zadziwienia. Ekspresyjna.

## Mechanika

### Znaczące Czyny i Osiągnięcia (3)

* Dostała się do domu groźnego pirata przez łóżko, zabiła go gołymi rękami a następnie poszła pokój obok uspokoić jego dzieci, że nic się nie stało.
* Przekonała Olgierda Drongona do tego, by zrobił z nią kapelę karaoke. Po czym sprzedawała bilety a dochody przeznaczyła na program integracyjny Orbitera.
* Podczas polowania na zbiegłych eternijskich niewolników, przeprowadziła ich przez Kontroler niezauważenie do bezpiecznego miejsca, dbając o ich zdrowie.

### Co się rzuca w oczy: Atuty i Przewagi (3, 6)

* PRACA: Lekarz i pierwszy oficer na Żelazku. Zachowuje zimną krew w najtrudniejszych sytuacjach.
* PRACA: "Szczur Orbitera"; doskonale walczy wręcz i potrafi szybko poruszać się w najdziwniejszym terenie.
* SERCE: Żyć, z całego serca. UWIELBIA się świetnie bawić i sprawiać, by inni się świetnie bawili. Zagłuszyć dobrą zabawą koszmary.
* SERCE: Pomóc zintegrować ludzi i magów z różnych kultur do Orbitera. Niech Orbiter będzie silny i chroni przed strasznymi Anomaliami.
* ATUT: Sex appeal. Maria nie jest może najpiękniejsza, ale nadrabia zuchwałością, świetnym głosem i podejściem.
* ATUT: Maria potrafi - nie zmieniając tonu głosu - strzelić w plecy komuś, kogo przed chwilą całowała.

### Charakterystyczne zasoby (3)

* COŚ: ZAWSZE nosi przy sobie środki pozwalające na skuteczną egzekucję kogoś lub siebie.
* KTOŚ: TAI Zefiris. TAI Kontrolera Pierwszego jest jej "przyjaciółką". To daje Marii szersze dostępy i wiedzę.
* WIEM: Wie zdecydowanie za dużo na temat Kultu Ośmiornicy, ich podejścia i tematów okołokralotycznych.
* OPINIA: Niebezpieczna i nieprzewidywalna. Nigdy nie wiesz co zrobi - ale wiesz, że jest lojalna tym, których wybrała.

### Typowe problemy z którymi sobie nie radzi (-3)

* SERCE: "you always walk alone". Przywykła do rozwiązywania tematów samodzielnie i nie współpracowania z innymi. Wykonuje rozkazy, ale nie WSPÓŁPRACUJE.
* STRACH: boi się virtu i rzeczy wirtualnych. Unika holoprojekcji i iluzjonistów. Próbuje żyć w maksymalnie realnym świecie jak się da.
* STRACH: boi się stazy, hibernacji i tego typu rzeczy. Nigdy nie chce stracić DECYZJI, że może umrzeć. Zna rzeczy gorsze.
* OTOCZENIE: nigdy nie była na żadnej planecie. Jedyne co zna, to kosmos i statki kosmiczne. Nie radzi sobie z "naturą", nie ma też kodu kulturowego.

### Specjalne

* .

## Inne

### Wygląd

* 

### Coś Więcej

* "Myślisz, że śmierć to zła opcja? Są rzeczy gorsze."
* Przyjęła nazwisko po anomalicznym statku kosmicznym, na którym została znaleziona. Miała dziewięć lat. Sierota.
* Teraz: 33 lata.
* Wychowywana "kolektywnie" przez orbiterowców z Kontrolera Pierwszego; jej najbliższą przyjaciółką była "Zofia", czyli Zefiris.
* Jest lojalna Olgierdowi i "Żelazku"; oni ją wyciągnęli z Kultu Ośmiornicy.
* Jej pierwszym przyjacielem i ogromną miłością był Martyn Hiwasser. Do dzisiaj są bardzo blisko, ale za dużo ich różni by teraz byli razem.
* NEOFORM: Wiktoria Diakon + Garak (DS9) | Abzan (BWG) | Profanity + Community + Tribalism | "Savage and unsrestrained will to live and thrive and protect way of life"

### Endgame

* .

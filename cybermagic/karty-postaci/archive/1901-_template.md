---
layout: cybermagic-karta-postaci
categories: profile
factions: "faction_1, faction_2"
owner: "public"
title: "Imię_Nazwisko"
---

# {{ page.title }}

## Postać

### Ogólny pomysł (3)

* 

### Motywacja (gniew/wartość, zmiana, sposób) (3)
 
* 

### Wyróżniki (3)

* 

### Zasoby i otoczenie (3)

* 

### Magia (3)

#### Gdy kontroluje energię

* 

#### Gdy traci kontrolę

* 

### Powiązane frakcje

{{ page.factions }}

## Opis

(xx min)

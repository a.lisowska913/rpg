---
layout: cybermagic-karta-postaci
categories: profile
factions: "pustogor, diakon"
owner: "kić"
title: "Pięknotka Diakon"
---

# {{ page.title }}

## Postać

### Koncept (3)

Dobrej klasy chemiczna terminuska i świetna kosmetyczka. Snajper. 
Pragnie być docenioną za swoje umiejętności kosmetyczne.
Piękna, uwodzicielska Diakonka (choć zwykle oszczędza przyjaciół). 
Kocha piękno i urodę. Przyjazna i empatyczna. 
W walce bardzo zdeterminowana i potrafi być bezwzględna. 

### Motywacja (gniew, zmiana, sposób) (2)

* Terminuską została, by chronić i pomagać.
* Być uznaną zarówno w dziedzinie kosmetyki jak i jako terminuska.
* Pragnie piękna i zrobi, co może, aby świat był bezpieczny - po to, żeby każdy mógł bez przeszkód rozkoszować się pięknem.

### Wyróżniki (2)

* Terminuska polująca na potwory wykorzystująca walkę chemiczną.
* Snajper i spec od ukrywania się na polu walki.
* Kosmetyczka najwyższej klasy, specjalizująca się w eksponowaniu piękna.
* Uwodzicielka wykorzystująca swoje ciało bez zahamowań.
* Surwiwalistka, specjalizująca się w trzęsawisku Zjawosztup.
* Tak buduje okoliczności (teren), by ten teren walczył za nią.
* Uroczo przekonywująca

### Epicki moment (2)

* Gdy trzeba kogoś naprawdę zrobić na bóstwo.
* Gdy trzeba po cichu i skutecznie usunąć problem, najlepiej snajperką.
* Gdy poświęca swoje ciało aby osiągnąć cel.

### Zasoby i otoczenie (3)

* salon piękności z małym laboratorium
* oddział sił obrony terytorialnej
* struktury terminusów Pustogoru
* pancerz w najlepszym stlu MMORPG, mocno zmodyfikowany
* zna większość ważnych osób w mieście (bo salon)
* snajperka
* potrafi wydzielać kontaktowo lub wziewnie działające substancje

### Magia (3)

#### Gdy kontroluje energię

* W boju elementalna jak również chemiczna.
* W gabinecie alchemia i magia upiększająca.
* Czytanie i modyfikacje pamięci.

#### Gdy traci kontrolę

* Ujawnia się jej ból ("Jestem kosmetyczką!")
* Przyzywa echa swoich przyjaciół i bliskich.

#### Powiązane frakcje

{{ page.factions }}

## Opis


ciało
Julia
Arazille
statek
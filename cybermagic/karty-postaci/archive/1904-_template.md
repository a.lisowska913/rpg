---
layout: cybermagic-karta-postaci
categories: profile
factions: "faction_1, faction_2"
owner: "public"
title: "Imię_Nazwisko"
---

# {{ page.title }}

## Postać

### Ogólny pomysł (2)

* 

### Misja (co dla innych) (2)

* 

### Motywacja (co dla mnie) (2)

* 

### Wyróżnik (2)

* 

### Zasoby i otoczenie (3)

* 

### Magia (3)

#### Gdy kontroluje energię

* 

#### Gdy traci kontrolę

* 

### Powiązane frakcje

{{ page.factions }}

## Opis

(xx min)

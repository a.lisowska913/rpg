---
layout: cybermagic-karta-postaci
categories: profile
factions: "szkoła magów, pustogor"
owner: "public"
title: "Karolina Erenit"
---

# {{ page.title }}

## Postać

### Ogólny pomysł (3)

* Kiedyś zwykły człowiek, studentka sieci komputerowych, nieufna wobec wszelkich komputerów. Mocą Saitaera przekształcona w czarodziejkę.
* Ma naturalne zdolności przywódcze i wystawia się na pierwszą linię by dowodzić innymi. Potrafi spokojnie ogarnąć każdą grupę.
* Trenowała sztuki walki. Potrafi się bić. Nie jest najlepsza, ale wystarczająco dobra.

### Motywacja (gniew/wartość, zmiana, sposób) (3)

* ACCOUNTABILITY/DIGNITY; jeśli ktoś coś powie, można na tym polegać oraz nie zrzuca się problemów na innych; żyje tymi ideałami i nie pozwala nikomu na obijanie się w tej kwestii
* FEROCITY/HARMONY; jest niezależna, silna i żyje w harmonii ze swoimi zasadami; atakuje wszelkie próby skrzywdzenia jej niezależności
* SINCERITY/ETHICS; wszyscy żyją w pokoju i dbają, by sobie pomagać; próbuje przejmować kontrolę i dowodzić, by zasady i ideały były spełnione
* nieśmiała ALE bardzo bojowa, trochę taka żyleta, ciągle się silnie kontroluje, niezwykle nieufna, tsundere starego typu

### Wyróżniki (3)

* Bardzo piękna: Paradoks Tymona ją przekształcił i nadał jej urodę Diakonki.
* Umysł strategiczny: naturalnie utalentowana w Supreme Missionforce; potrafi przenieść tą wiedzę na świat rzeczywisty.
* Bardzo potężna magia pod wpływem silnych emocji: Saitaer wzmocnił jej moc magiczną do poziomu przekraczającego normalnego ucznia.

### Zasoby i otoczenie (3)

* Zaczęstwiacy: grupa ludzi, gildia, miłośnicy różnych gier komputerowych. Specjalizuje się w grupie od Supreme Missionforce.

### Magia (3)

#### Gdy kontroluje energię

* Technomantka: integracja technomantyczna, systemy rojowe i sieciowe. Jest jak w Supreme Missionforce - działa przez agentów.
* Infomantka: naturalnie łączy się z systemami informatycznymi i bez problemu zdobywa dowolne informacje.
* Magia zniszczenia: najczęściej w formie wybuchów i erupcji, ale zdarza się też w formie korozji i rozkładu. Dar Saitaera.

#### Gdy traci kontrolę

* może dojść do przeniknięcia Ixiona na Primus
* jej wewnętrzny konflikt materializuje się bardzo niszczycielsko magią zniszczenia
* dzieją się rzeczy, które maksymalnie ją upokarzają w konkretnym kontekście (zwłaszcza infomantycznie)

### Powiązane frakcje

{{ page.factions }}

## Opis

(15 min)

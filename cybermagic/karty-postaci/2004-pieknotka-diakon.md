---
layout: cybermagic-karta-postaci
categories: profile
factions: ""
owner: "public"
title: "Pięknotka Diakon"
---

# {{ page.title }}

## Kim jest

### Motto

"Każdy problem wymaga innego podejścia. Z każdym sobie poradzę."

### Paradoksalny Koncept

Świetna terminuska, która chciałaby być kosmetyczką, z upodobaniem łażąca po trzęsawisku Zjawosztup.

## Mechanika

Terminus chemiczny: 
Paladyn: ochroni niewinnych za wszelką cenę, ukarze nadużywających siły i władzy
Trzęsawisko: czuje się w nim jak w domu, zna ścieżki i trucizny
Dotyk Saitaera: 



### Czym osiąga sukcesy (3)

* ATUT: Biochemia - kosmetyki i narkotyki.
* ATUT: Wykorzystanie i manipulacja terenem i sytuacją, by działały na jej korzyść.
* SŁABA: Na wyżynach społecznych - źle się czuje, nie wie, co robić.
* SŁABA: Jest taktykiem, ale nie strategiem, fatalna w administracji.

### O co walczy (3)

* ZA: Harmonia i porządek. Każdy ma prawo do swojej wersji, ale jakaś musi być.
* ZA: Piękno jest ważne. Jeśli odrzucimy je na rzecz przetrwania to nie przetrwamy.
* VS: Zwalcza wszelkie formy nadużywania siły i władzy.
* VS: Mściwa. Jeśli skrzywdzisz ją lub jej przyjaciół, oglądaj się za siebie.

### Znaczące Czyny (3)

* Uśpiła Finis Vitae, łącząc moc Arazille i Saitaera, osobiście wchodząc w paszczę lwa.
* Opanowała Cienia siłą woli i ogromnymi stratami we krwi własnej.
* Trochę oswoiła Chevaleresse i wpakowała ją Alanowi.
* Przetrwała w pojedynku snajperskim z Walerią oraz przetrwała atak z zaskoczenia Kirasjerki.

### Kluczowe Zasoby (3)

* COŚ: Cień, niezwykły pancerz łączący Ixion z Esuriit.
* KTOŚ: Sieć wsparcia terminusów Szczelińca.
* WIEM: Znam teren, okolicę, trzęsawisko.
* OPINIA: W obronie Pustogoru posunie się bardzo daleko. Bezwzględna.

## Inne

### Wygląd

Niezbyt ładna jak na Diakonkę. A bit on the sporty side, widać, że potrafi walczyć.
Porusza się miękko i płynnie.
Lekko zmęczone oczy, które widziały wiele walk.

### Coś Więcej

---
layout: cybermagic-karta-postaci
categories: profile
factions: "podróżnik, niezrzeszeni"
owner: "public"
title: "Stach Sosnowiecki"
---

# {{ page.title }}

## Postać

### Ogólny pomysł (3)

* Stary oszust i fałszerz. Kiedyś podróżnik i awanturnik, czasem szmugler i przemytnik.
* Świetny sprzedawca i bajerant. Lubi brać sprawy w swoje ręce - czy to pałką, czy to odpowiednią gadką.
* Złodziej, specjalista od funkcjonowania w mieście. Szybko ucieka i każdemu wszystko ukradnie.

### Motywacja (gniew/wartość, zmiana, sposób) (3)
 
* IRREVERENCE/BOLDNESS; czuć się jak młody i być traktowany jak młody; napluć śmierci w twarz, działa bardzo ryzykownie
* WEALTH/WINNING; jak najwięcej wygrywać i się bogacić; nie gra czysto, traktuje pieniądze jako funkcję zwycięstwa
* BRILLIANCE/SELF-RELIANCE; jest uznawany za wybitnego, szanowany i nie zależy od nikogo; chwali się na lewo i prawo sukcesami

### Wyróżniki (3)

* starszy czarodziej (64), wygląda sympatycznie i stosunkowo niegroźnie. Ludzie mu ufają.
* doskonały akwizytor. Każdemu wszystko wepchnie i jeszcze ten nieszczęśnik będzie myślał że zrobił interes życia. 
* świetny aktor, zwłaszcza w efektownych scenach. Mało kto potrafi tak prowokować i budzić gniew w innych.

### Zasoby i otoczenie

#### Ogólnie (4)

* Podręczny sprzęt włamywacza, zbiór oszukanych dokumentów, fałszywe "arcydzieła", listy rekomendacyjne, dobry strój...
* Dossier na większość osób w okolicy; kupował i zbierał. Rzadko rusza się bez swojej "teczki".
* Doskonałej klasy stealth suit. Świetny do maskowania i szybkiego przemieszczania (grappling hook, jump...). Jak lekki pancerz.
* Dziesiątki BEZUŻYTECZNYCH artefaktów o bardzo dziwnych sygnaturach, zwykle odpady z Eteru Nieskończonego.

### Magia (2 LUB 4)

#### Gdy kontroluje energię

* Jego magia jest stosunkowo "cicha" - jest skupiona na odwracaniu uwagi, ukrywaniu się, znajdowaniu cennych rzeczy.
* Dzięki magii jest w stanie zdecydowanie wzmacniać fałszerstwa, wyceniać rzeczy oraz dostać się tam, gdzie nie powinien.
* Ostatnim aspektem jego magii jest komfort, efektowność i możliwość pokazania jaki jest świetny. No i przemyt.

#### Gdy traci kontrolę

* Gdy jest w dobrym humorze, efektowne zwracanie uwagi i ogólnie rozumiana magia Iluzji - "maximum gloat overdrive".
* Gdy jest w złym humorze, magia Stacha manifestuje się w formie jego strachu przed śmiercią i starością - fale osłabienia i rozpadu.
* Ogólnie, gdy traci kontrolę to będzie to coś efektownego i głośnego. Albo upokarzającego, albo... też upokarzającego, ale w inny sposób. 

### Powiązane frakcje

{{ page.factions }}

## Opis

.

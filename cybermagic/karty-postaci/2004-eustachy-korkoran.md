---
layout: cybermagic-karta-postaci
categories: profile
factions: "orbiter, oddział verlen"
owner: "public"
title: "Eustachy Korkoran"
---

# {{ page.title }}

## Kim jest

### Paradoksalny Koncept

Sabotażysta specjalizujący się w kontrolowanych i dyskretnych zniszczeniach. Najcelniejszy strzelec na Inferni, ale nie lubi niekierowanych zniszczeń. Od biedy może pełnić rolę mechanika, ale nie jest w tym ekspertem. Supremacjonista magów i ludzi, który stoi twardo po stronie Orbitera w konflikcie frakcji. Mistrz kontrolowanej destrukcji, który wykorzystuje swoje talenty w służbie ludzkiej rasy i przeciw anomaliom.

### Motto

"Przetrwaliśmy jak na razie wszystko - a czego nie potrafimy ujarzmić, to zniszczymy."

## Mechanika

### Czym osiąga sukcesy (3)

* ATUT: Sabotażysta najwyższej klasy. Potrafi dyskretnie i niezauważenie wyłączyć lub zniszczyć kluczowy komponent lub odwrócić uwagę. Jego działania są niewidoczne dla odbiorcy.
* ATUT: Doskonały strzelec i artylerzysta - świetnie radzi sobie z ogniem niekierowanym. Często pełni rolę snajpera.
* SŁABA: Czuje się bardzo nieswojo nie mając narzędzi wysokotechnologicznych; na jakiejś planecie, w lesie - byłby bardzo nieszczęśliwy i zagubiony.
* SŁABA: Negocjacje, polityka, tego typu sprawy - to nie jest dla niego. Jego interesuje pole bitwy i technologia; te rzeczy "z ludźmi" woli zostawić innym.

### O co walczy (3)

* ZA: W kosmosie znajdują się różnego rodzaju anomalne "potwory". On chce się z nimi zmierzyć i wszystkie zniszczyć. Dla wyzwania.
* ZA: Orbiter jest najlepszą opcją dla ludzkości - jedna potężna siła militarna pilnująca, by Astoria była bezpieczna a handel funkcjonował. Lojalista.
* VS: Nie toleruje głupiego, nieefektywnego wykorzystywania zasobów tylko dlatego, żeby polityka się zgadzała. Dlatego dołączył do Inferni.
* VS: Nie zgadza się na supremację jakichkolwiek istot czy bytów nad ludzkość. Kraloth, Spustoszenie, bogowie, TAI - to wszystko jest nieakceptowalne.

### Znaczące Czyny (3)

* Gdy był cywilnym członkiem załogi statku handlowego 'Pelidor', skutecznie rozstawił ładunki gdy byli abordażowani przez piratów. Piraci myśląc że 'Pelidor' się rozpada - ewakuowali się.
* Gdy zbieracze mieli problem na Cmentarzysku Statków, przystosował laser górniczy jednego z ich statków i zestrzelił trzy ŁZy niebezpiecznie zbliżające się do ich pozycji.
* Gdy Infernia uciekała przed Nocną Kryptą, wykorzystał swoje umiejętności i jakkolwiek uszkodził statek, ale wyciągnął z niego dość mocy by Infernia uciekła anomalnemu napastnikowi.

### Kluczowe Zasoby (3)

* COŚ: Zbiór zaawansowanych magitechowych narzędzi do przebudowywania bytów technologicznych - dzięki temu umie zmienić większość rzeczy w broń (lub w coś co wybuchnie).
* KTOŚ: Kilku inżynierów w warsztatach Kontrolera Pierwszego; często Eustachy jest proszony o ocenienie jak odporna jest dana konstrukcja.
* WIEM: Zna słabe strony większości konstrukcji ludzkich i magitechowych; jeśli nie jest to supertajne, to Eustachy wie jak to najskuteczniej i dyskretnie uszkodzić.
* OPINIA: Jeśli Eustachy coś chce zniszczyć to to zniszczy - nie da się go zatrzymać. Jedyne co można to skierować jego energię w inną stronę.

## Inne

### Wygląd

.

### Coś Więcej

.

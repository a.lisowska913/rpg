---
layout: cybermagic-karta-postaci
categories: profile
factions: "teokracja maus, oddział verlen, maus"
owner: "public"
title: "Zygfryd Maus"
---

# {{ page.title }}

## Kim jest

### W kilku zdaniach

Hierofant Karradraela i rycerz sterujący Chevalierem, Zygfryd stanowi siłę wiary i walki. Zawsze mający jakieś ciekawe zdanie na podorędziu, ten paladyn wysuwa się na pierwszą linię ognia z polami siłowymi i defensywami. Ekspert od utrzymywania terenu i puryfikacji, głosi chwałę Karradraela dewastując przeciwników technikami antyanomalnymi i antymagicznymi.

### Co się rzuca w oczy

* Motto: "Wiara jest moim mieczem, wola jest moją tarczą i z woli Karradraela obronimy się przed tym zagrożeniem"
* Ma niesamowicie mocny głos i zawsze ma "powiedzenie na dzisiaj". Chodzące ciasteczko z wróżbami, gdyby tymi wróżbami były słowa Karradraela.
* Ma spore poczucie humoru, ale jest "na boku". Lubi wkręcać innych.
* TWARDY JAK PODESZWA. Exemplar Karradraela. Lubi uciechy cielesne, ale nie kosztem misji i operacji.
* Ma szczególną słabość do dzieci i młodszych istot. Zwłaszcza długowłosych brunetek (przypominają mu siostrę).
* Liczne blizny, których nie usuwa - są dowodem jego walki i przekonań.
* Ogólnie, wesoły. Acz ma mrok w oczach. Dużo widział przez swoje 42 lata (data pojawienia się na Inferni).

### Jak sterować postacią

* Incorruptible. Exemplar Karradraela. Inspiruje, zawsze powie coś pozytywnego od Karradraela.
* Niesamowicie odważny, na pierwszej linii. Immovable object, zawsze chcesz go do obrony lub ochrony. Lub puryfikacji.
* Nigdy nie narzeka, ale nie zrobi czegoś niezgodnego z zasadami moralnymi Karradraela.
* Zachowuje się jak główny bohater anime - przemowy, głośne ataki, THEME SONG (monumentalne melodie Karradraela), flagi.

## Mechanika

### Znaczące Czyny i Osiągnięcia (3)

* Gdy jego rodzina została Skażona w sposób nieodwracalny przez anomalne potwory, osobiście zabił wszystkich których kochał z wolą Karradraela na ustach i łzami w oczach.
* Wszedł na statek piracki transportujący niewolników do Syndykatu Aureliona. Jego słowa i przekonania (i Chevalier) sprawiły, że część piratów oddało się w ręce Orbitera.
* Wyzwał na pojedynek eternijskiego maga z simulacrum, by kupić czas na ewakuację cywili. Regenerowali go potem przez 3 miesiące.

### Co się rzuca w oczy: Atuty, Przewagi, Zasoby (3, 6)

* PRACA: Inspirujące słowa i straszliwe umiejętności walki wręcz. Mało kto chce stanąć naprzeciw exemplarowi Karradraela.
* PRACA: Puryfikator i ekspert od defensyw. Wie wiele o anomaliach, jak je zwalczać i jak sobie z nimi radzić. Potrafi obronić i ufortyfikować praktycznie wszystko.
* ATUT: Nieskończona wiara w Karradraela i w _manifest destiny_. Niezłomny. Incorruptible. Jest przerażający i inspirujący jedocześnie. Exemplar.
* COŚ: Dwuręczny miecz typu europejskiego. Zarówno jego fokus jak i broń. Zawsze ubrany jest w ceremonialny pancerz paladyna Karradraela.
* COŚ: Chevalier, ciężki i niesamowicie drogi autonomiczny servar Mausów z TAI, specjalizujący się w polach siłowych, walce w zwarciu i puryfikacji. Przedłużenie jego magii.

### Typowe problemy z którymi sobie nie radzi; Słabości (-3)

* CORE WOUND: "Z winy niekompetentnych tchórzy moja rodzina została splugawiona. Ja musiałem osobiście uwolnić moich ukochanych od agonii."
* CORE LIE: "Z pomocą Karradraela zostanę exemplarem. Zabezpieczę innych przed tą samą tragedią która spotkała mnie."
* Nie jest dobry w wycofywaniu się. Instynkt - uratować wszystkich, zniszczyć anomalie. Zwykle cofa się ZA PÓŹNO.
* Przeszacowuje swoje umiejętności i możliwości. Nie ufa innym że zabezpieczą cywili, więc bierze więcej na siebie niż powinien.
* Łatwy do wpakowania w pułapkę z uwagi na monotematyczność - obrona, ochrona, kontrolowane wycofanie się.

### Serce i Wartości (3)

* Wartości
    * TAK: Humility (wola Karradraela), Face (exemplar), Security (trauma + rola)
    * NIE: Universalism (tylko ludzie), Self-direction (każdy ma swoje miejsce)
    * "Karradrael zapamięta mnie za tych wszystkich których uratowałem w Jego imieniu i za chwałę którą Jemu przyniosłem."
    * "Ci, którzy postępują jak chcą i przekraczają ludzkość są przyczyną wszelkiego zła. Fałszywi bogowie. Lub, co gorsza, źli bogowie."
* Ocean
    * E:+, N:-, C:+, A:0, O:0
    * Ortodoksyjny exemplar Karradraela, śmiało ruszy na przeciwnika, dokładny i niezły w planowaniu, niezwykle odważny i niezłomny.
    * Immovable object. Z woli Karradraela obroni wszystkich i osłoni wszystko.
* Silnik
    * Jestem tu z woli Karradraela. Mym mieczem jest ma wiara, mą tarczą jest ma wola.
    * Chronić wszystkich przed anomaliami i anomaly-adjacent corruption. Paladyn Karradraela chroni. Defensor.

### Magia (3M)

#### W czym jest świetna

* Puryfikacja, wypalanie energii magicznej i słabości magii
* Fortyfikacja i pola siłowe - ultimate defender

#### Jak się objawia utrata kontroli

* Niszczycielskie, apokaliptyczne wręcz wyładowania energii i pól siłowych.
* All of our sins - widmowy legion. Echo zniszczeń. Ewentualnie - jego miecz ożywa i "wrogowie są w rękach Karradraela".

## Inne

### Wygląd

.

### Coś Więcej

* ?

### Endgame

* ?

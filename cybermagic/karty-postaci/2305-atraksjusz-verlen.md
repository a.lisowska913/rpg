---
layout: cybermagic-karta-postaci
categories: profile
factions: "verlen"
owner: "public"
title: "Atraksjusz Verlen"
---

## Funkcjonalna mechanika

* **Aspekty charakteru**
    * Siły: niezłomny, brutalny, przerażający w walce, "nie obiecuje, informuje", god of war, "posunie się dalej niż myślisz"
    * Słabości: tylko cel ma znaczenie, torment allies to evolve them, generally heartless, córka Arkadia (jej zdrowie)
* **Aspekty strategii / podejścia**
    * Siły: dewastator w zwarciu, konfrontacja maksymalna, mistrz improwizacji, deceit and flank
    * Słabości: lonely is the coral throne, overextend forces
* **Aspekty umiejętności**
    * Siły: weapon maker / dark artificer, forged in wars, servar engineer
* **Aspekty zasobów**
    * Siły: dark artificer (przeróżne uzbrojenie), reputacja Lorda Wojny
        * Customowy Lancer (konfig: Venom (kineza + dopalacze + heavy autocannon + scorpion pull + plasma lance))

## Kim jest

### Fiszka

* Atraksjusz Verlen: mag, sybrianin
    * (ENCAO: --+00 | Zimnie uprzejmy, "Nie czuje emocji", Lord of War | VALS: Achievement, Power > Benevolence | DRIVE: Social Darwinist, Sirus ("feel something"))
    * styl: Appolyon x Sirus x Admirał Lepescu x Alarac. Zdecydowany, bezwzględny, wyższość, zero poczucia humoru, skupiony na celach, charyzmatyczny.
    * blood knight / God of War, świetny podczas walki, jeden z najgroźniejszych w walce Verlenów (acz Viorika jest lepszym taktykiem)
    * styl walki: magia kinezy, dark artificer. Nazwa: "kinetic distortion"
    * "tien X, (seria tytułów), spodziewałem się o tyle więcej po tak utytułowanym przeciwniku..."
    * piosenka: ArchEnemy "War Eternal"

### W kilku zdaniach

Konstruktor broni i inżynier servarów, bezwzględny wojownik i herald wojny i zniszczenia. Sarkastyczny i brutalny, pragnie by wszyscy byli jak najsilniejsi. Łączy ciemne strony Verlenów (osiągnięcia, agresja, waleczność, chwalenie się) z konstrukcją zaawansowanej technologii i użycie magii i technologii. Konstruktor trebuszetów, fortyfikacji i modyfikator servarów.

* Motto: "Świat dzieli się na tych, którzy decydują o losie innych i tych, którzy służą i się dostosowują. Ja decyduję."

### Jak sterować postacią

* Wizja: 
    * JA: blood knight, najsilniejszy pośród silnych
    * ŚWIAT: Pragnie wiecznej wojny która sprawi, że wszyscy będą silni.
* Dominujące Strategie: 
    * bezpośrednia walka z servara, devil's bargain komuś komu zależy
* Charakter: 
    * Niezwykle waleczny, lubi ogień walki i nie boi się bólu. Skrajnie konfrontacyjny. Proaktywnie zmusza wszystkich do walki.
* Wzory: 
    * Alarak (SC2) / Appolyon (ForHonor): ogromna intensywność osobowości, sarkastyczny, wiecznie gotowy do walki
* Inne:
    * .

### Serce i Wartości (3)

* Ocean
    * ENCAO: --+00
    * Zimnie uprzejmy, "Nie czuje emocji", Lord of War 
    * Konstruktor sprzętu technologicznego i inżynier zniszczenia, dostosowuje się do sytuacji i używa potężnych gadżetów.
* Wartości
    * TAK: Achievement, Power
    * NIE: Benevolence
    * A: Słowo 'przegrana' jest synonimem 'tchórzostwo' lub 'rozkład'.
    * P: The strong subdue the weak. Only strength matters. By getting stronger one can project force upon reality. Power is virtue.
    * B: Jeśli potrzebujesz pomocy, wzmocnij się. W tym Ci mogę pomóc. Tylko w tym.
* Silnik
    * Ignite the eternal evolution, constant war and combat.
* Marzenie (jeśli inne niż Silnik)
    * Arkadia, the princess of knives.
* CORE WOUND: "Byłem za słaby. Potwory zniszczyły wszystko, w co wierzyłem i o co walczyłem gdy byłem mały."
* CORE LIE: "Jeśli wszyscy będą nieskończenie silni, nikt nie będzie cierpiał i zwyciężymy świat. Verlenowie są chwilowo za słabi."

### Znaczące Czyny i Osiągnięcia 

* Gdy jego córka, Arkadia, się urodziła - zrobił dla niej specjalny nóż, specjalnie z esencji najgroźniejszych istot Verlenlandu (z pomocą naukowców Blakenbauerów)
* Zaatakował i zabił niebezpiecznego potwora niedaleko niewielkiej miejscowości, solo, przy pewnych stratach w miejscowości. Mogło nikomu nic się nie stać, ale musiałby wezwać pomoc.
* Zawalił górę na potwora używając specjalistycznej konstrukcji i ładunków wybuchowych.

### Magia
#### Dominująca moc

* JEDNYM ZDANIEM: "dark kinetic artificer"
    * konstrukcja / ewolucja sprzętu
        * adaptacja servara
        * różnorodna broń ciężka
    * magia kinezy, przede wszystkim w walce
        * odchylanie pocisków

#### Jak się objawia utrata kontroli

* weapons do what weapons want to do
* 



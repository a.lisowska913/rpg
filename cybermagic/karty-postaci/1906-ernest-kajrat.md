---
layout: cybermagic-karta-postaci
categories: profile
factions: "blekitne niebo"
owner: "public"
title: "Ernest Kajrat"
---

# {{ page.title }}

## Postać

### Ogólny pomysł (3)

Kiedyś, zastępca Elizy - specjalista od logistyki i kontroli roju. Dzisiaj - mafioso i potwór wymuszający czego chce torturami.

### Czego chce a nie ma (3)

* walka z bogami i ochrona Noctis; misja się nie skończyła tylko dlatego, że Inwazja przegrała. Śmierć Astorii jest akceptowalna.
* nie zatracić siebie, pozostać niezależnym; każdy podlegający komuś (bogom, energiom, systemowi) musi zostać uwolniony niezależnie od mroku.
* złamać każdego, być mistrzem korupcji; wpływ Esuriit przekształcił mu hobby - chce osiągnąć mistrzostwo w złamaniu np. seksbota.

### Sposób działania (3)

* terror - tortury, szantaż, reputacja, efektowny i krwawy styl walki, pożarcie Esuriit, nieskończona nienawiść
* walka - zakrzywienia przestrzeni, dowodzenie agentami, drony i roje, świetny pilot i strzelec servara
* dowodzenie - taktyka, wiedza o przeciwniku, motywowanie strachem, liczni agenci
* urok - dżentelmen, elegancki ubiór, etykieta, prawdziwy arystokrata

### Zasoby i otoczenie (3)

* Cień Esuriit: straszliwy koszmar i servar Esuriit chroniący go przed atakiem, na pierwszej linii (gdy trzeba).
* Ciało Esuriit: wampiryczna regeneracja, zadane przezeń rany się gorzej leczą, możliwość opętania
* Sekrety i niewolnicy: mistrz szantażu, o każdym coś wie, znajduje słabe punkty i ich używa
* Drony: zawsze ma sporo dron i latających tarcz. Praktycznie działa tylko nimi; sam nie walczy bo nie musi.
* Reputacja: najstraszliwszy potwór w okolicy.
* Najlepszej klasy servar; odporny i silny.

### Magia (3)

#### Gdy kontroluje energię

* Włada negatywną energią Esuriit; przyzywa strach, głód i ból oraz echa cierpienia z przeszłości.
* Kineza i teleportacje - jako mag specjalizuje się w polach siłowych, szybkim poruszaniu się i barierach teleportacyjnych.
* Łańcuchy i reduktory poruszania się i woli. Ciemność, melasa.

#### Gdy traci kontrolę

* Eksplozja Esuriit; darkness of the unlife. Awatar Esuriit. Istoty Esuriit.
* Wpada w szał berserkerski; nie potrafi się powstrzymać. Pojawia się jego broń i cień.

### Powiązane frakcje

{{ page.factions }}

## Opis

Esuriit pozwoliło mu uwolnić się z niewoli swoich panów po zdradzie najbliższej przyjaciółki; nigdy więcej nie będzie zniewolony. Też: Gaurn.

(12 min)

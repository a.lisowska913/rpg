---
layout: cybermagic-karta-postaci
categories: profile
factions: "pustogor, rekin, torszecki, aurum"
owner: "public"
title: "Rafał Torszecki"
---

# {{ page.title }}

## Kim jest

### W kilku zdaniach

Mało ważny arystokrata z mało ważnego rodu Aurum. Cały jego ród ma domieszkę krwi noktiańskiej i interesy z noktianami. Rafał jest trochę gogusiem, płaszczy się wobec Władzy i wygląda na niegroźnego. Jest jednak świetny w pozyskiwaniu informacji, agregacji wiedzy i jest dobrym aktorem. Boi się bólu, ale odważnie bierze na siebie problemy przełożonych - wierzy w system i chce nim pomagać.

### Co się rzuca w oczy

* Świetnie ubrany, w odpowiedni sposób do każdej okazji. Wyraźnie lubi piękno.
* Płaszczy się strasznie przed Władzą (Marysią). Jest niegroźny. Jest niegodny ataku.
* Praktycznie nie czaruje. 

### Jak sterować postacią

* Preferuje używanie kontaktów > narzędzi > artefaktów nad działania osobiste czy magię.
* Zawsze tak dobrze ubrany i wymuskany jak tylko jest to możliwe. Bardzo lubi się dobrze ubierać i dobrze wyglądać.
* Płaszczy się jawnie przed najsilniejszą osobą (tu: Marysia Sowińska). Żyje by spełniać jej życzenia.
* Ogólnie, wierzy w to, że system i tradycja działają. Działa zgodnie z nimi a nie przeciw nim.
* Zwalcza wszelkie alternatywne systemy - mafie itp. Tylko te, które wspierają prawny porządek (Pustogor) i Aurum mają działać.
* Nie ufa nadmiarowi magii i stosowaniu magitechów. Duża niechęć do Eterni. Sympatia i wspieranie zasymilowanych noktian.

## Mechanika

### Znaczące Czyny i Osiągnięcia (3)

* Zbierając informacje i raporty udowodnił swojemu rodowi, że w Parwólce użycie magitrowni jest podobnie skuteczne jak zwykła elektrownia - i dużo groźniejsze. Nie zdecydowano się na budowę magitrowni na rzece.
* Parę miesięcy po pojawieniu się wśród Rekinów złożył swoją sieć kontaktów z lokalsów nie wydając specjalnie środków. Handlował informacjami, plotkami itp. Broker informacji.
* Gdy był w klubie sportowym i ów klub został napadnięty przez chuliganów, zdołał wypłaszczyć i wyżebrać tylko kilka pogardliwych kopniaków. Jego koledzy dostali dużo mocniej - nie byli w stanie grać potem.
* Mimo swoich działań na terenie Szczelińca, NIKT nie podejrzewał go o proaktywność i bycie sensownym. Świetnie się ukrył za fasadą płaszczaka i gogusia.

### Co się rzuca w oczy: Atuty i Przewagi (3, 6)

* AKCJA: Broker i agregator informacji. Potrafi połączyć różne dziwne fakty i wyprowadzić jak powinna sytuacja wyglądać lub co się dzieje.
* AKCJA: Potrafi załatwić wszystko - perfekcyjny asystent. Mistrz biurokracji i etykiety.
* AKCJA: Potrafi opóźnić wszystko - plany, agregacje i okoliczności potrafi tak ułożyć, by dramatycznie spowolnić drugą stronę.
* AKCJA: Doskonały aktor. Świetnie kłamie. Nie ma cienia skrupułów przed kłamaniem i przekazaniem kłamstwa o kilku warstwach.
* COŚ: Seria agentów, którzy informują go o plotkach i sytuacjach wśród Rekinów i na terenie Szczelińca. Szeroka sieć kontaktów.
* COŚ: Lista wielu ważnych adresów, maili itp. Ma możliwość skontaktowania się z osobami, o których mało kto wie.
* COŚ: Bardzo dużo wie o noktianach, Noctis itp. Ma kontakty, adresy, wiedzę o technologiach itp.

### Serce i Wartości (3)

* Bezpieczeństwo. 
    * Płaszczy się przed Władzą, bo to maksymalizuje wpływ i bezpieczeństwo. Plus, pozory niegroźności. 
    * Chroni swych przełożonych. Czerpie wpływy i bezpieczeństwo z możnych i bycia faktycznie przydatnym.
    * Plany w planach, pułapki w pułapkach. Chce wiedzieć wszystko o wszystkim.
* Tradycja
    * Wzmacnia system bo ten działa. Indywidualizm prowadzi do ruiny i cierpienia.
    * On jest mniej ważny, wiec zgłasza się na ochotnika. Przełożony wie lepiej.
    * Wszystko zostaje w rodzinie. Need to know basis. 
* Dobrodziejstwo
    * Przekierowuje uderzenia w siebie, bo dużo wytrzyma i ma sieć ochronną. Bez problemu się „poświęci” dla innych.
    * Usprawnia istniejący system - by lepiej pomagał wszystkim grupom.
    * Wspiera akceptację noktian - tajemnicą poliszynela jest to, że jego ród ma dużo krwi i powiązań z Noctis.

### Typowe problemy z którymi sobie nie radzi; Słabości (-3)

* CORE WOUND: "Nie mam znaczenia i jestem z nieważnego rodu. Nikt się ze mną nie liczy. Pragnę mocy i wpływu, ale NIGDY jej nie dostaję. Zawsze ktoś inny."
* CORE LIE: "Nie jestem w stanie nic zmienić i tak jak jest tak będzie. Mogę jedynie wspierać możnych i dopasować się do ich woli. Ja nie mam znaczenia."
* Agenci Torszeckiego są często niegodni zaufania, korzysta z ich liczby; może być bardzo zmylony przez odpowiednio zdeterminowanego przeciwnika.
* Torszecki wierzy w informacje od Sowińskich, bo to jest strategicznie dlań korzystne. Ale przez to może być kierowany przez Sowińskich w Aurum.
* Fizycznie Torszecki nie nadaje się do walki, nie jest ani silny, szybki ani dobry technicznie. Bardzo słaby jako Rekin.
* Bardzo słaba pozycja. Torszecki to idealny cel po którym można jechać. Nie ma jak się bronić i nie chroni go silny ród. Plus, nielubiany.
* Boi się bólu. I Lemurczaków. Zwłaszcza Lemurczaków.

### Magia (3M)

#### W czym jest świetna

* "Magiczny krawiec": umiejętność umagiczniania ubrań i materiałów. Wodoodporność, lepiej wyglądają, usztywnienie, nośnik dalszych czarów...
* Pomniejsza magia soniczna: emisja, przenoszenie i odbieranie dźwięków.
* Zwykle robi coś w stylu "zostawiam rękawiczkę, która przeniesie o czym mówią do mnie".

#### Jak się objawia utrata kontroli

* Głosy, szepty, kakofonia, rozsypane wszelkie formy komunikacji, przenoszenie losowych dźwięków w niewłaściwe miejsca...
* Coś strasznego dzieje się z ubraniami. Ożywienie ubrań, stworzenie mimika, zniknięcie, zmiana tekstury...
* Czasem ulegają ujawnieniu Straszne Sekrety osób niedaleko. Lub samego Torszeckiego.

### Specjalne

* .

## Inne

### Wygląd

* ?

### Coś Więcej

* .

### Endgame

* Optymistycznie 1:
    * Ożeni się z Marysią Sowińską.
* Pesymistycznie 1: 
    * Zostanie oddany w ręce Lemurczaków - podpadnie zbyt wielu osobom.

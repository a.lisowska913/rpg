---
layout: cybermagic-karta-postaci
categories: profile
factions: "pustogor, rekin, verlen, aurum"
owner: "public"
title: "Arkadia Verlen"
---

# {{ page.title }}

## Kim jest

### W kilku zdaniach

Arkadia Verlen. Nożowniczka i uzdolniona kinetka oraz survivalistka przekonana, że nie jest dość dobra by sprostać glorii rodu (zwłaszcza Ariannie czy Elenie). Uważana za absolutnie szaloną w kontekście walki i stylu walki. Sama trenuje by zostać asasynką - uważa, że prawdziwym problemem nie są potwory a magowie (co separuje ją od przeciętnego Verlena).

### Co się rzuca w oczy

* Ma sporo śladów po bliznach. Ok, są to wyleczone blizny, ale negatywnie się wyróżnia urodą wśród normalnie pięknych arystokratek Aurum.
* Nieufna, ale naiwna. Podatna na manipulację, bo wierzy swoim zmysłom i "faktom" nie sprawdzając czwartego poziomu głębokości ;-).
* Wyraźnie zawsze próbuje pomóc słabszym. Łatwo ją zachęcić do działania - "mogę pomóc i mogę komuś wkopać".

### Jak sterować postacią

* Bardzo proaktywna, próbuje pomagać. 
* Nigdy nie zostaje cicho. Jeśli uważa, że coś powinna powiedzieć, robi to.
* Wielka miłośniczka Sekretów Orbitera. Uwielbia je oglądać i wierzy w każde słowo.
* Raczej cicha, raczej na uboczu - chyba, że widzi że musi interweniować i wtedy głośna i IN YOUR FACE.
* Nie ma szczególnych umiejętności dyplomatycznych. Raczej woli pięścią wymusić, by było tak jak ona chce.
* Bardzo lubi działać "in your face", twarzą w twarz z zagrożeniem. Wbić nóż w potwora do rękojeści i poczuć, jak krew tryska na twarz.

## Mechanika

### Znaczące Czyny i Osiągnięcia (3)

* W Verlenlandzie walczyła przeciwko Marcadorianowi; udało jej się odciągnąć potwora od wioski, zanim ten ją ciężko pogruchotał.
* W Podwiercie nie dzieje się dobrze, więc Arkadia w przebraniu (by nikt nie poznał w niej Rekina - odrzuciliby) pomaga ludziom. "Spiderman".
* Stoczyła serię bitew z większością Rekinów. Pokonała wszystkich 1v1 a większość 1v3. Najlepsza wojowniczka wśród Rekinów.

### Co się rzuca w oczy: Atuty i Przewagi (3, 6)

* AKCJA: Szybka, cicha i zwinna; Arkadia jest dość silna i wytrzymała, ale słynie z niesamowitej precyzji i prędkości ataku. Wślizgnie się, znajdzie słaby punkt i usunie.
* AKCJA: Survivalistka. Przeżyje praktycznie wszędzie, jak karaluch. Czasem suplementuje się truciznami, by być odporniejszą...
* AKCJA: Dobrze czyta walkę; niezły taktyk. Strateg z niej żaden, ale podczas walki wie gdzie się znaleźć i co zrobić. Trudna do zaskoczenia.
* AKCJA: Wyszkolenie bojowe; potrafi używać większości broni i servarów.
* CECHA: Pancerny visiat; jej ciało jest wzmocnione i szybsze. Arkadia cały czas ćwiczy, trenuje i dba o swoje umiejętności bojowe. Ma komu dorównać...
* COŚ: Kolekcjonerka noży. Lubi małe, wygodne ostrza - dedykowane do różnych potworów, pancerzy itp. Nie rusza się bez noża czy dwóch.
* COŚ: Kombinezon kameleona. Pozwala jej na ukrywanie się i przedostawanie się dyskretnie w różne miejsca. Prywatnie, do bycia "spidermanem" ;-).

### Serce i Wartości (3)

* Tradycja
    * Przerażona tym, że nigdy nie dorówna Ariannie czy Elenie. Głęboko zainspirowana kuzynkami. Nie jest zazdrosna - chce im dorównać! Wie z jakiego rodu pochodzi i czym musi być.
    * Honorowa, próbuje trzymać się zasad wpojonych jej w Verlenlandzie. Konflikty Verlenów są jej konfliktami.
    * Od arystokratki oczekiwane są różne rzeczy - inspiracja, przemowy, taniec, etykieta. PRÓBUJE. Serio próbuje. Ale nie umie. Nie radzi sobie i to ją frustruje.
* Dobrodziejstwo
    * Pragnie być punktem światła, ratować i pomagać - zupełnie jak Arianna czy Elena. Nie musi być widoczna, ale musi być pomocna.
    * Sama poprosiła o przydzielenie jej do Rekinów. Tu są rzeczy w których może pomóc. Tu są młodzi arystokraci (o których ma kiepską opinię) i trudne warunki w Pustogorze.
    * Jej odpowiedzialnością jest sprawić, by wszyscy arystokraci zachowywali się tak, jak wymaga tego Tradycja Aurum. Chronić słabszych.
    * Robi to co jest SŁUSZNE i zgodne z honorem i zasadami a nie to co jest dla niej wygodne. "Twarda ręka więcej daje niż łagodna".
* Stymulacja
    * Nie odmawia walki czy pojedynku, nieważne czy może wygrać. Im mocniej dostanie teraz, tym lepiej się nauczy i tym silniejsza będzie jutro.
    * Ascetka; próbuje zobaczyć jak daleko jest w stanie posunąć swój organizm i swoje ciało. Unika luksusu i odmawia zaadaptowania się do naleśnictwa Rekinów.
    * "Sama siła i kompetencja są niewiele warte, jeśli nie używasz ich by pomóc innym, słabszym od Ciebie - tego uczysz się w Aurum". Pomaganie daje jej radość.

### Typowe problemy z którymi sobie nie radzi; Słabości (-3)

* CORE WOUND: "Arianna, Elena, Brunhilda - one wszystkie są epickie. Ja jestem zawsze w cieniu, ile bym nie robiła, nie jestem w stanie dorównać legendom."
* CORE LIE: "Muszę być perfekcyjna w każdej formie. W każdym obszarze. Perfekcyjna arystokratka i zabójczyni. A nie jestem w stanie!!!"
* Prawdziwa arystokratka powinna być słodka, umieć inspirować, przemawiać i tańczyć. Arkadii (mimo imienia) to wszystko nie wychodzi. Próbuje, ale nie umie. NIE INSPIRUJE.
* Kombinacja "pomóc komuś" i "ostra walka" jest dla niej jak najsłodszy narkotyk.
* Typowa wojowniczka / zabójczyni / survivalistka. Raczej niskie poczucie humoru, niska subtelność itp.
* Nie jest dobrym dowódcą. Zapomina, że inni nie są tak szybcy / wytrzymali jak ona i że są delikatni.
* Podda się zanim pozwoli by komuś niewinnemu stała się krzywda. Ale woli nożem w oko ;-).

### Magia (3M)

#### W czym jest świetna

* Kineza: przemieszczanie siebie, przemieszczanie broni, zmiana trajektorii, sterowanie nożem... bardziej PRECYZJA niż INTENSYWNOŚĆ.
* Kineza: potrafi tworzyć 'kinetyczne noże' czy inne ostrza. Potrafi zrobić kinetyczną włócznię. Arkadia NIGDY nie jest nieuzbrojona.
* Aportacja: przyzywa noże. Dużo noży. Dowolny nóż ze swojej kolekcji. Potem je odsyła...

#### Jak się objawia utrata kontroli

* "Ścieżka zdrowia". Aportacja i kineza zmieniają teren w coś bardzo niebezpiecznego.
* Wpada w chorą rywalizację. MUSI wygrać. Idzie za daleko. Dotyka to jej lub osoby dookoła.

### Specjalne

* .

## Inne

### Wygląd

* Ma sporo śladów po bliznach. Ok, są to wyleczone blizny, ale negatywnie się wyróżnia urodą wśród normalnie pięknych arystokratek Aurum.
* Białe włosy z lekkimi pasemkami niebieskiego. Krótkie - by nie dało się za nie złapać i pociągnąć. Oczy - jak u Arianny - zielone.

### Coś Więcej

* .

### Endgame

* ?

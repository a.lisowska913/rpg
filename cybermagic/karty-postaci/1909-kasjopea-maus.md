---
layout: inwazja-karta-postaci
categories: profile
factions: "maus, luxuritias, cieniaszczyt"
owner: "public"
title: "Kasjopea Maus"
---

# {{ page.title }}

## Paradoks

Narkomanka intensywnych emocji, która kręci filmy zamiast działać osobiście.

## Motywacja

### O co walczy

* pokazanie takiego świata jakim jest, rozerwanie welonu - szokująca prawda dla każdego
* maksymalizacja doznań i uczuć dla siebie oraz innych
* być sławną - z filmów, doznań, skandali

### Przeciw czemu walczy

* nie zgadza się na wyzysk słabszych przez silniejszych - to z radością zniszczy
* obnaży wszelką hipokryzję, zwłaszcza taką uniemożliwiającą innym tego co Ty robisz
* nie stanie się potworem. Nie będzie krzywdzić innych. Jest reżyserem, nie psychopatą.

## Działania

### Specjalność

* doskonale rozpływa się w miastach, potrafi się schować w zakamarkach
* bezbłędnie udaje osobę z niższej klasy społecznej; w tej kwestii świetna aktorka
* świetnie włada nożami i nie zawaha się zranić ani posunąć dość daleko
* nieustraszona, niemożliwa do zgorszenia. Strasznie odporna na wszelkie elementy mentalne
* akrobatka i włamywaczka jakich mało. Też świetnie oszukuje w karty
* reżyser holokostek Mausów, zwłaszcza tych z niższych instynktów

### Słabość

* impulsywna i skłonna do ryzyka - rzuca się w najgorszy ogień i wpada w pułapki
* fascynacja cierpieniem i bólem - przyciąga ją to jak ćma ogień; może się "zawiesić"
* attention whore - nie jest zbyt lubiana i przyciąga uwagę w zły sposób
* fascynacja narkotykami i chemikaliami. Lubi sobie zażyć i być w odmiennym stanie świadomości
* skupia się na maksymalizacju intensywności swojej i innych; czasem idzie za daleko
* wyzywające tatuaże utrudniają jej ukrywanie się
* ZBYT odważna i brawurowa. Nie odmawia używek, nie unika ryzyka...

### Akcje

* wywiady, przepytuje innych
* wkrada się w różne miejsca i podgląda
* im bardziej gorszące czy intensywne, tym większa szansa że ona tam jest
* kocha ból, płacz, silne emocje... WSZYSTKO.
* testuje różne używki i czaruje pod ich wpływem

### Znaczące czyny



* uratowana przez Orbiter z niewoli w Eternii
* odmówiono jej transferu do Trzeciego Raju, ale zaakceptowano do Pustogoru; tu jest dużo noktian i ktoś musi mieć na nich oko
* oślepiona przez EMP, zdjęła przeciwników wbudowaną bronią
* regularnie pomaga jako wolontariuszka w Zamku Weteranów
* podczas Szturmu, użyła działa servara dzięki augmentacjom; połamało ją.
* rozbiła szajkę terminuskich przemytników w Pustogorze, po śladach do celu
* ze szczególną pasją niszczy wszystko czego dotknie Noktis lub Kajrat
* strachem, stymulantami i siłą ognia wymusiła sprawną ewakuację przy pożarze
* przekonała starego terminusa- weterana do eutanazji, gdyż był za drogi dla rodziny i dla Zamku. Wbrew życzeniom wszystkich.

## Mechanika

### Archetypy

cat burglar, intrepid reporter, daredevil, tancerz chaosu

### Motywacje

.

## Inne

### Wygląd

* Tatuaże. Wszędzie, łącznie z twarzą. Przedstawiają dziwne symbole i znaki. Przyciągają uwagę.
* Krótkie fioletowo-żółto-zielone włosy a la jeż.
* Ubrana w wyzywający strój.

---
layout: cybermagic-karta-postaci
categories: profile
factions: "pustogor, miasteczkowcy, multivirt, elisquid"
owner: "public"
title: "Alan Bartozol"
---

# {{ page.title }}

## Postać

### Ogólny pomysł (2)

* Pustogorski terminus szturmowy. Wykorzystuje bardzo ciężkie servary lub mechy, zawsze z ogromną siłą ognia. Siła ognia to jego wyróżnik ;-).
* Świetnie zna się na materiałach wybuchowych, wysadzaniu rzeczy i konstrukcji tego typu spraw. Specjalizuje się w destabilizacji _vitium_.
* Mistrz MultiVirt. Dowódca i imperator własnej gildii wielogrowej - Elisquid. Nie jest technomantą, ale jest świetnym graczem i przywódcą.

### Misja (co dla innych) (2)

* Silna segregacja i podejście kastowe na zasadzie kompetencji. Każdemu wedle potrzeb, od każdego według umiejętności. Dążenie do bycia homo superior dla każdego.
* Wprowadzić porządek, ład oraz przewidywalność do tego świata pełnego chaosu. Zwłaszcza wobec tych, którzy są mniej kompetentni i tego potrzebują.

### Motywacja (co dla mnie) (2)

* Cisza. Adrenalina. Pierwotny łowca przeciwko światu. On sam, bez polityki, bez innych ludzi i magów. Apex Predator. Spokój i brak uniesień - tylko czysta destrukcja.
* Homo superior, można od niego oczekiwać wszystkiego, też niemożliwego. Kodeks wojownika i inżyniera.
* Wolność - jest na tyle dobry, skuteczny i kompetentny że nikt nie ma prawa niczego od niego żądać.

### Wyróżnik (2)

* Niesamowicie szybki (reakcje) i celny terminus; mnóstwo pracy, energii i siły wkłada w to by być najszybszy i najcelniejszy.
* Trzęsawisko Zjawosztup zna jak własny dom, wykorzystuje je jako miejsce do ciągłego doskonalenia się i pozyskiwania pieniędzy.
* Niekwestionowany specjalista od przełamywania obrony i broni oblężniczej. Świetnie znajduje słabe punkty ciężkich przeciwników i penetruje pancerz.
* Nieprawdopodobny wręcz ponurak. Potrafi zdemotywować, zmartwić i wyłączyć wszelką radość z każdego.
* Neutralizator. Potrafi bardzo szybko znaleźć kontrzaklęcie czy kontrtaktykę. Nie potrafi budować, ale potrafi niszczyć.
* Ceniony i szacowany Krwawy Rycerz w multivirt. Przełożony Elisquid.

### Zasoby i otoczenie (3)

* Nie objął żadnego terenu w Pustogorze, więc jest terminusem lotnym; można go wysłać na każdą operację jako siłę główną czy wsparcie.
* Szturmowo-artyleryjski servar klasy 'Fulmen', bez opcji self-activate, z opcją 'trybu artylerii'. Niezbyt szybki, ale pancerny i silny.
* Ma dostęp do Laboratorium Zniszczenia w Pustogorze oraz sporą ilość niestabilnych kryształów _vitium_.
* Własna niewielka gildia gdzie pomaga ludziom - Elisquid.

### Magia (3)

#### Gdy kontroluje energię

* katalityczne portale energetyczne: potrafi wykorzystywać energię otoczenia by wzmacniać zaklęcia swoje oraz swoich sojuszników. Świetny w drenażu energii wrogów.
* kontrmagia: neutralizacja wrogich zaklęć i ogólnie negacja wrogich ruchów. Nie jest to puryfikacja; to wszelka forma KONTRDZIAŁAŃ.
* magia oblężnicza: powolne zaklęcia o upiornej sile ognia i umiejętnościach przebicia się przez defensywy. W ostateczności, destabilizacja _vitium_ czy eksplozja samej magii.

#### Gdy traci kontrolę

* Siła ognia jeszcze bardziej rośnie, ale kosztem wszelkich możliwych działań (overload).
* Jako, że pracuje na niestabilnych energiach, może coś mu wybuchnąć czy przestać dobrze działać.

### Powiązane frakcje

{{ page.factions }}

## Opis

.

---
layout: cybermagic-karta-postaci
categories: profile
factions: "orbiter, oddział verlen"
owner: "public"
title: "Leona Astrienko"
---

# {{ page.title }}

## Kim jest

### W kilku zdaniach

Ludzka zabójczyni magów z Eterni (cirrus), agentka Orbitera, z ostrą paranoją. Na oko słodkie, drobne stworzenie, ale w praktyce socjopatka niezrównana w walce wręcz. Modularna, używa wszczepów, specjalizuje się w niszczeniu magów. W okrutny sposób zdewastuje każdego, kto deprecjonuje ludzi lub ich gnębi. Lubi się bić i wygrywać, lubi straszyć innych... ogólnie, NIE DOTYKAĆ.

### Co się rzuca w oczy

* Motto: "Wszystko da się zniszczyć. Każdego zabić. Brutalnością i bezwzględnością osiągniesz sukces. A Eternia - niech spłonie."
* Mała psychotycznie uśmiechnięta blondyneczka z dużą ilością cyborgizacji, pixie cut i OGROMNĄ giwerą.
* Lubi dokuczać, wyzywać przeciwników i ostrą walkę. Nieustraszona. Skrajnie konfrontacyjna.
* Nie dba o swoje zdrowie czy życie. Chce po prostu rozwalić jak najwięcej wrogów Orbitera, najlepiej z Eterni.

### Jak sterować postacią

* SPEC: neurosprzężony komandos modularny, specjalistka do eksterminacji magów ćwiczona w Eterni. Stały bonus Dużej Przewagi do WSZYSTKICH operacji eksterminacji magów.
* Jeśli może rzucić chorą plotkę - rzuci ją. Jeśli może się bawić cudzym kosztem - zrobi to. Jeśli może dokuczyć, dokuczy.
* Skrajnie nieufna i paranoiczna wobec wszystkiego powiązanego z Eternią. Skrzywdzili ją i nie umie wybaczyć. W ogóle nie wybacza.
* Lubi być wykorzystywana jako psychotyczna broń czy agent terroru. Lubi walczyć z przeważającym, niemożliwym wrogiem. LUBI zadawać cierpienie i straszyć.
* Tryb psychotyczny (radość, walka, energia, wybuchy, zniszczenie, energia) i tryb poważny (smutne oczy, mało mówi, dużo pije).
* Lojalna wobec przyjaciół (których nie ma wielu); rzuci się by osłonić Ariannę przed eternijskim simulacrum nawet kosztem życia BEZ WAHANIA.
* Ceni siłę i odwagę. Gardzi jakąkolwiek formą słabości.
* Bawi się tak jak walczy - na całego. Żyje pełnią życia teraz.
* Jeśli jest możliwość wzmocnienia, pójdzie na to. Chce być najgroźniejszym cirrusem w galaktyce.
* Musi mieć silnego handlera, bo szanuje tylko siłę. Trudno jej się powstrzymać przed powodowaniem kryzysów jak się nudzi.

## Mechanika

### Znaczące Czyny i Osiągnięcia (3)

* Trzecie miejsce w turnieju walki w zwarciu marines; pokonała magów katai w walce 1v1. Do dzisiaj paraduje z dumą pokazując odznaczenie.
* Zatrzymała walkę w barze pomiędzy magami - częściowo swoją siłą woli i rozkazami a częściowo wybijając strategiczne zęby. Nie musiała, ale mogła.
* Podczas wypadku w laboratorium na Kontrolerze Pierwszym zatrzymała Anomalię kupując wszystkim czas na ucieczkę. Wynik - miesiąc w szpitalu.
* Miała tylko sprowokować arystokratów do głupiego hazardu a zrobiła Krwawą Noc pięciu magom którzy ją napadli. Najlepsza zabawa ever - stała się postrachem a filmiki poszły.
* Gdy Elena była Skażona Esuriit, zaatakowała ją by unieszkodliwić. Esuriitowa Elena jest bardzo groźna; Leona jednak ją pokonała.

### Co się rzuca w oczy: Atuty, Przewagi, Zasoby (3, 6)

* SPEC: Neurosprzężony komandos modularny, specjalistka do eksterminacji magów ćwiczona w Eterni. Stały bonus Dużej Przewagi do WSZYSTKICH operacji eksterminacji magów.
* ATUT: Antymagiczne wszczepy i sprzęt. Jako, że jest człowiekiem, to nie ma problemów ze stosowaniem dwustronnie antymagicznych bytów (np. lapisowanych). Nie przeszkadzają jej.
* ATUT: Neurosprzężenie, zdolna do kontrolowania jednostek takich jak Entropik.
* ATUT: Bardzo silna cyborgizacja. Faktyczny homo superior, z dopalaczem, możliwością działania w próżni, własnym reaktorem itp.
* PRACA: Niezrównana w walce w zwarciu. Nie dość że wszystko jest bronią to jeszcze jej wszczepy sprawiają, że niełatwo ją zneutralizować.
* PRACA: Terror. Potrafi zastraszyć, zmusić do współpracy itp. Dodajmy fakt, że praktycznie wszędzie się dostanie...
* OPINIA: Niebezpieczna, nie podchodzić! Nieobliczalna, groźna oraz ogólnie chodzące ryzyko. Najlepiej zejść jej z drogi. Z dowodami.

### Typowe problemy z którymi sobie nie radzi; Słabości (-3)

* CORE WOUND: "Zniszczyli moją rodzinę. Zniszczyli mnie. Zostawili mnie - a Orbiter mnie uratował."
* CORE LIE: "Płomienie i choroba duszy odejdzie jeśli zniszczę ich wszystkich. Nie ma czegoś takiego jak dobry Eternianin."
* Nie nadaje się do normalnych kontaktów z ludźmi. Nie ma jak odejść do cywila. Jest mechanicznym koszmarem.
* Zawsze COŚ musi się dziać. Jak nic się nie dzieje, sama to sprowokuje. Musi mieć silnego handlera, bo szanuje tylko siłę.

### Serce i Wartości (3)

* Wartości
    * TAK: Stymulacja (skrajnie), Prestiż, Osiągnięcia (killcount, bigger monster)
    * NIE: Pokora
    * Coś się ZAWSZE musi dziać. Nie można się zatrzymać ani na moment, bo może być chwila na refleksję i co straciliśmy...
    * Jeśli Cię nie szanują to Cię zniszczą. Może być tylko jeden top dog - muszą POSTRZEGAĆ Cię jako apex predatora.
    * Życie i tak nie ma większego znaczenia, ale zawsze jest jeszcze jeden eternianin do zastrzelenia i jeden potwór do zabicia
    * Zostawili mnie na śmierć. Nie ma przeznaczenia. Sama zbuduję własne. Wygram każdą bitwę, zawsze.
* Ocean
    * E:+, N:-, C:-, A:-, O:0
    * Skrajnie agresywna, wybierająca strategie o wysokim zwrocie i rzucająca się na ryzyko impulsywna agentka która nie przegrywa czystą determinacją.
    * Nie dogaduje się z ludźmi. Ale szanuje tych silnych. Słabi są ofiarami które trzeba wzmocnić lub wykorzystać.
* Silnik
    * "There is always one more eternian bastard to hurt"
    * Tylko siła ma znaczenie. Będę silniejsza.
    * Osłonię swoje stado. 

## Inne

### Wygląd

.

### Coś Więcej

* ?

### Endgame

* ?

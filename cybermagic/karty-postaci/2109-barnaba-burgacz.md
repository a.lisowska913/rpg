---
layout: cybermagic-karta-postaci
categories: profile
factions: "pustogor, rekin, burgacz, aurum"
owner: "public"
title: "Barnaba Burgacz"
---

# {{ page.title }}

## Kim jest

### W kilku zdaniach

"DJ Babu". Agresywny, dobry na ścigaczu, robi muzę, ogólnie taki... zaczepny. Nie słucha się nikogo. Anarchista, dobry w wysadzaniu rzeczy i wybuchach. Głośny, wszechobecny, krępy pocisk nienawiści. Gardzi hierarchią i nienawidzi jakichkolwiek form kontroli zniewolenia. Coś między saperem bojowym i DJem.

### Co się rzuca w oczy

* Krępa budowa, stosunkowo niski i przysadzisty.
* Babu nienawidzi wszelkich ograniczeń. Nienawidzi arystokracji. Nienawidzi pozycji. Nienawidzi tego, że Pustogor próbuje kontrolować kulturę.
* Nie cierpi Eterni - kontrola przez arkin jest kontrolą absolutną. Coś, czego nie rozumie i nie zaakceptuje.
* Bardzo agresywny - bitka jest jego odpowiedzią na wszystkie problemy. Jest dobry w walce i bardzo wytrzymały na ból.
* Kocha muzykę i sztukę. Otwarcie gardzi Pustogorem za to, że kontrolują to co jest otwarte i dostępne.
* Fraternizuje się z innymi, nie tylko Rekinami. Ma gdzieś tradycję. Wszystko robi po swojemu.
* Nie jest mściwy. Zwycięża lepszy, nie "bardziej honorowy". ALE nie daruje manipulowania / używania władzy.
* Nie chce dowodzić czy rozkazywać. Nie uważa hierarchii za coś pożądanego czy przydatnego.

### Jak sterować postacią

* Nie ukrywa niczego. Mówi co myśli.
* Absolutny anarchista. Nie słucha rozkazów. Nie chce by ktoś mówił mu co ma robić.
* Bardzo agresywny - to jego sposób rozwiązywania problemów.
* Uważa, że arystokraci nie są nic lepsi od ludzi a ludzie od TAI. Tylko siła i kompetencje się liczą.
* Woli działać sam niż współpracować z arystokracją, ALE będzie współpracował z Rekinami-odrzutkami.
* Nie przeszkadza mu to, że wbije komuś nóż w plecy. Nie walczy czysto. Oczekuje też noża w plecy.
* Nie zapomniał, co się stało z jego ulubioną kuzynką - Andreą. Nie zapomniał tego ani Orbiterowi ani Aurum. A zwłaszcza Sowińskim.

## Mechanika

### Znaczące Czyny i Osiągnięcia (3)

* Po tym, jak Andrea ucierpiała na Ogienku, wysadził statuę Anastazego Sowińskiego w ogrodach Burgaczów. Mimo, że była chroniona. Mimo kamer.
* Publicznie i twardo bronił praw TAI, zwłaszcza po wydarzeniach z Andreą i Ogienkiem. Za to zesłano go do Rekinów jako "lol, dziwak" - z dala od innych.
* Gdy Sabina Kazitan była niewolnicą na tym terenie, Babu stał za nią i ją głośno wspierał. Nigdy jej nie wykorzystał - nawet gdy Sabina go o to błagała (z woli Oliwii).
* Nie dołączył do żadnego stronnictwa wśród Rekinów, a gdy Oliwia Lemurczak próbowała go do tego zmusić, wlał do jej kwatery sporo szamba.

### Co się rzuca w oczy: Atuty i Przewagi (3, 6)

* AKCJA: Dobry w nieczystej walce; szczególnie w całkowitym zwarciu / zapasach.
* AKCJA: Dobrze się zakrada; jest niezły w przemykaniu między cieniami. Niezły stealth jak na tak głośnego dewastatora.
* AKCJA: Solidny Rekin - lata ścigaczem, nieźle strzela. Nie jest najlepszy, ale jest zdecydowanie kompetentny.
* AKCJA: Robi solidną muzykę. Zwykle dość ostrą, w kierunku na "weźcie sprawy w swoje ręce" czy "power to the people".
* AKCJA: Konstrukcja ładunków wybuchowych dowolnego typu i ich podkładanie. Saper i demolitioner. Potrafi wysadzić praktycznie cokolwiek.
* CECHA: Twardy jak cholera - zarówno z perspektywy mentalnej, moralnej jak i fizycznej. Ile razy nie dostanie, zwykle wstaje z ziemi. Ma też taką reputację.
* COŚ: Defensywna kamizelka. Nie tylko chroni go przed atakami / uderzeniami, ale też ma ładunki wybuchowe. Może robić kierowane eksplozje podczas walki.
* COŚ: Juliusz Sowiński go lubi i uważa za szczerego; dlatego ma plecy i nie da się go po prostu zgnieść za to co robi. To tylko defensywne na linii politycznej; on nie lubi Juliusza ;-).

### Serce i Wartości (3)

* Samosterowność
    * ALERGICZNIE traktuje wszelkie formy kontroli i ograniczeń. Absolutny anarchista. Wolność i niezależność ponad wszystko.
    * "Jestem uwięziony w tym cholernym Podwiercie!" - Babu NIENAWIDZI tego miejsca. Traktuje Podwiert jak więzienie. Nie chce być z innymi Rekinami.
    * Na każdą formę blokady, ograniczenia kontroli itp. reaguje agresją i atakiem. Ma skłonności do częstej, ostrej agresji.
* Osiągnięcia
    * "Nie zniszczycie mnie tak jak zniszczyliście Andreę!" - Babu ćwiczy, trenuje i szkoli się tak, by nie dało się go zatrzymać i pokonać.
    * "Władza to po prostu inkarnacja siły. Silni rządzą słabszymi" - cynik; nie wierzy w ideały czy merytokrację. Tylko w siłę i osiągnięcia.
    * "Down with the system" - jeśli będzie odpowiednio dobry, stworzy muzykę i zainspiruje ludzi do złamania Pustogoru czy Aurum.
* Uniwersalizm
    * "Wolna kultura dla każdego" - DJ Babu jest przeciwko ograniczaniu kultury i wierzy, że wszyscy mają prawo do każdej formy kultury.
    * "Każda myśląca istota jest równoprawna" - wierzy, że TAI i ludzie powinni mieć te same prawa. Nie wierzy w prymat Aurum.
    * "Najlepsi powinni dowodzić" - UWAŻA, że merytokracja jest najlepszą zasadą. Najlepsi w danej dziedzinie powinni dowodzić.

### Typowe problemy z którymi sobie nie radzi; Słabości (-3)

* CORE WOUND: "Nie byłem w stanie uratować Andrei ani jej nawet pomścić. I nikt nie zwracał na to uwagi. Andrea została przez wszystkich odrzucona."
* CORE LIE: "Każda forma władzy i kontroli jest z definicji skorumpowana. Nie ma czegoś takiego jak altruistyczny władca."
* Nawet jeśli to ma sens, nie słucha się rozkazów. Nawet, jeśli przez to będą konsekwencje.
* Kiepsko kłamie i oszukuje. Mówi jak jest.
* Jeśli chcesz go przekonać by zrobił co chcesz, pokonaj go w walce.

### Magia (3M)

#### W czym jest świetna

* Piromancja: wysadza rzeczy. Skutecznie. Jest dobry w manifestacji eksplozji - kierowanych, większych, mniejszych. "Oni mają kinezę, ja mam eksplozje".
* Leczenie: podstawy magii leczniczej, zwłaszcza w dziedzinie poparzeń. Nie wyleczy w pełni, nie choroby - ale naprawi rany bitewne.

#### Jak się objawia utrata kontroli

* THEME SONG! Pojawia się ultra głośna muza. On żyje w końcu muzyką.
* Rzeczy losowo wybuchają. Najlepiej do muzyki ;-).

### Specjalne

* .

## Inne

### Wygląd

* Krępa budowa, stosunkowo niski i przysadzisty. Łysy. Nie ma zarostu.
* Wiecznie wkurzony.

### Coś Więcej

* Prototyp: Mad Stan z Batman Beyond, ale przekroczył ten archetyp.

### Endgame

* ?

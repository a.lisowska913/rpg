---
layout: inwazja-karta-postaci
categories: profile
factions: "terminus, pustogor, miasteczkowiec, elisquid"
owner: "public"
title: "Alan Bartozol"
---

# {{ page.title }}

## Paradoks

realizuje się przez tworzenie rzeczy maksymalizujących pustkę; chce zniszczyć nawet Esuriit

## Motywacja

### O co walczy

* spokojny i bezpieczny Szczeliniec
* ukoić swoje demony z czasów Aurum
* potrafić zniszczyć absolutnie wszystko - nawet Esuriit

### Przeciw czemu walczy

* głupi przełożeni, kretyni i lenie dookoła
* wszelkie formy marnowania zasobów - zwłaszcza ludzi
* jakikolwiek brak efektywności i wydajności

## Działania

### Specjalność

* nieprawdopodobnie szybkie reakcje
* superciężka broń
* niezwykła samokontrola
* przeświadczenie, że zawsze robi to co powinien

### Słabość

* CHEVALERESSE - poświęci dla niej nawet karierę i zdrowie
* żyje z dnia na dzień, nie ma nic poza swoim warsztatem
* społecznie jest z boku wszystkiego; nie ma przyjaciół czy do kogo się zwrócić; nieufny
* nie do końca umie żyć / działać z innymi poza strukturą hierarchiczną

### Akcje

* bezwzględnie skuteczny, ale unika krzywdzenia. Nastawiony na wynik.
* dla Alana 'there is no overkill', acz uwzględnia collateral damage
* wyciszony; nie zwraca uwagi na rzeczy nieistotne czy na uczucia innych
* dąży do perfekcji we wszystkim co robi i myśli
* realizuje się przez tworzenie narzędzi zniszczenia i wchodzenie w pustkę
* oszczędny w ruchach, oczy węża, bardzo rusza głową szukając wiecznie zagrożenia

### Znaczące czyny

* nie widząc celu, zagrzebany, strzelił Fulmenem w Pięknotkę - dokładnie z siłą jaką chciał
* wygrał wszystkie pojedynki rewolwerowe z kolegami terminusami (49).
* uczestniczył w akcji, gdzie kilku kolegów pożarło Eurii. Nie wahał się ich zabić.

## Mechanika

### Archetypy

inżynier zniszczenia, terminus szturmowy, szef gildii virt

### Motywacje

?

## Inne

### Wygląd

* oczy węża
* cichy, oszczędny w ruchach
* praktycznie ciągle rusza głową, by widzieć zagrożenie / ruch

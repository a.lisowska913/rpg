---
layout: inwazja-karta-postaci
categories: profile
factions: "pustogor, terminus"
owner: "public"
title: "Ksenia Kirallen"
---

# {{ page.title }}

## Paradoks

Medyk zadający rany, detektyw bez oczu i noktianka walcząca za Astorię.

## Motywacja

### O co walczy

* Astoria. Szczeliniec. Orbiter. Ten teren, ta kultura-to przyszłość która powinna być
* każdy ma swoje miejsce w egzystencji i ma je spełniać
* zawsze patrz w przyszłość, optymalizuj następne pokolenia a nie siebie

### Przeciw czemu walczy

* noktiańskie kłamstwa; infekcja kultury Noktis w Astorię
* naruszenia zasad, honoru i powinności roli osoby na Astorii - masz swoje miejsce.
* wszelkie formy przestępstw - zwłaszcza nadużycie zaufania

## Działania

### Specjalność

* augmentacje: siłowe, stabilizacyjne, detekcyjne
* po śladach do celu-detektor i detektyw wysokiej klasy
* dobry medyk frontowy-ustabilizuje, utrzyma przy życiu, odkazi
* ulubioną bronią są dwa szybkostrzelne pistolety o dużym odrzucie
* potrafi się neurosprząc z większością pojazdów

### Słabość

* nienawiść do Noktis i Eternii sprawia, że zaczyna śledztwo od nich i idzie za daleko - zada niepotrzebne cierpienie
* brak elastyczności - jej światopogląd robi z niej agenta wydziału wewnętrznego, ale kiepskiego polityka lub terminusa lokalnego
* wrażliwa na EMP
* brak magii aktywnej - przeniosła energię w biowspomagania
* jej skuteczność jest okrutna i wygląda strasznie - trudno o sojuszników

### Akcje

* podejrzliwa, zawsze ma domniemanie winy
* fanatyczna, skrupulatna i cierpliwa
* zawsze eleganka; pod maską arystokratki kryje się inkwizytor
* lubi wzbudzać strach-jej zdaniem każdy ma coś do ukrycia
* lubi pomagać dzieciom i osobom starszym- regularnie w wolontariacie
* nie chce mieć niczego swojego, bo wtedy ma coś do stracenia

### Znaczące czyny

* uratowana przez Orbiter z niewoli w Eternii
* odmówiono jej transferu do Trzeciego Raju, ale zaakceptowano do Pustogoru; tu jest dużo noktian i ktoś musi mieć na nich oko
* oślepiona przez EMP, zdjęła przeciwników wbudowaną bronią
* regularnie pomaga jako wolontariuszka w Zamku Weteranów
* podczas Szturmu, użyła działa servara dzięki augmentacjom; połamało ją.
* rozbiła szajkę terminuskich przemytników w Pustogorze, po śladach do celu
* ze szczególną pasją niszczy wszystko czego dotknie Noktis lub Kajrat
* strachem, stymulantami i siłą ognia wymusiła sprawną ewakuację przy pożarze
* przekonała starego terminusa- weterana do eutanazji, gdyż był za drogi dla rodziny i dla Zamku. Wbrew życzeniom wszystkich.

## Mechanika

### Archetypy

terminus medyk, cyborg, neurosprzężony pilot, terminus wydziału wewnętrznego

### Motywacje

.

## Inne

### Wygląd

* blada karnacja, liczne blizny, zintegrowane cyber-oczy (6 asymetrycznych)
* elegancki czarno-stalowy strój w cywilu, mundur ogólnie
* rękawiczki ukrywają cyberwspomagania i iniektory
* jasnoszare, krótko obcięte włosy; nieliczne między bliznami

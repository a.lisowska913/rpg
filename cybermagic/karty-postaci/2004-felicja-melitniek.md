---
layout: cybermagic-karta-postaci
categories: profile
factions: "szczeliniec, akademia magii, szczur uliczny"
owner: "public"
title: "Felicja Melitniek"
---

# {{ page.title }}

## Kim jest

### Paradoksalny Koncept

Szczur uliczny, która kocha muzykę. Przynęta na bogaczy, która próbuje skłonić słabszych od siebie do posiadania przyjaciół. Przeurocza dama, która uwielbia łazić po dużych wysokościach. Pochodzi ze świata przestępczego, ale próbuje unikać problemów z prawem. Aktualnie czarodziejka w Akademii Magii, która nie potrafi znaleźć sobie tam miejsca - wolałaby żyć wśród sobie podobnych Szczurów. Kocha muzykę, ale jej nic z nią nie wychodzi. Niezwykle lojalna, przez co wpada w kłopoty.

### Motto

"Jeśli nie chcą Cię skrzywdzić, to jesteś bezpieczna. Jeśli chcą, musisz być szybsza. Jeśli nie ma wyjścia, atakuj z całych sił. I trzymaj się swoich."

## Mechanika

### Czym osiąga sukcesy (3)

* ATUT: Szczur miejski. Świetnie się chowa, szybko biega i zna większość kryjówek. Dobra w survivalu. Jeśli ma się bić - to w jaja lub gryzie.
* ATUT: Świetnie radzi sobie na dużych wysokościach czy w niestabilnych miejscach. Ma naturalne wyczucie takich miejsc.
* ATUT: Bywa przeurocza - wabi, nęci, odwraca uwagę, kokietuje. Dzieki temu umie się wyłgać z wielu kłopotów.
* SŁABA: Panicznie boi się lekarzy, badań, strzykawek itp. W przeszłości nad nią eksperymentowano w Aurum i trauma została - uruchamia się jej magia defensywnie.
* SŁABA: Jest lojalna tym co jej pomogli - nie okradnie ich, nie zdradzi, nawet, jeśli nie zgadza się lub uważa ich za złe osoby. Przez to łatwo ją wpakować w kłopoty.
* SŁABA: Wstydzi się swojej niekompetencji, swojego pochodzenia, tego, że nie umie śpiewać (a próbuje) itp. Reaguje unikaniem / ucieczką - jak nie może, atakiem pełną mocą.

### O co walczy (3)

* ZA: Bardzo, BARDZO lubi muzykę - śpiewać, tańczyć, koncerty itp. Jednocześnie jest w tym bardzo, bardzo kiepska. Więc kolekcjonuje rzadkie płyty i fragmenty muzyki.
* ZA: Próbuje chronić osoby słabsze od siebie, żeby nie spotkała ich duża krzywda. Próbuje też popchnąć je na drodze do posiadania przyjaciół.
* VS: Nie chce wracać do tego co było kiedyś; boi się tego. Nie chce wiedzieć, nie chce się mścić. Chce uciec od tego jak najdalej.
* VS: Chce uniknąć problemów z prawem i władzami. Mogą ją zamknąć i oddać z powrotem tym, którzy robili jej krzywdę.

### Znaczące Czyny (3)

* Zwabiła terminusa z fortu Mikado (po służbie) na ksiuty do ciemnego zaułka gdzie skroili go ludzie Dużego Toma; tak skutecznie go rozproszyła.
* Chowając się po Podwiercie skutecznie unikała sił Dużego Toma, policji a nawet magów - łażąc tam gdzie nikt jej się nie spodziewa.
* Dała się ciężko pobić (do poziomu połamanych żeber), by kupić czas dwóm uciekającym dzieciakom przed wrogim gangiem.

### Kluczowe Zasoby (3)

* COŚ: Narzędzia włamywacza i nóż wielofunkcyjny - dzięki nim dostanie się w wiele miejsc i uniknie sporo starć.
* KTOŚ: Zna niejednego Szczura w Podwiercie; schronią ją lub pomogą jej odwrócić czyjąś uwagę. Czasem coś komuś zwiną.
* WIEM: Zna większość kryjówek, dróg ucieczki itp. Bez kłopotu uniknie nieprzyjemnych spotkań.
* OPINIA: Lojalna jak cholera, w zasadzie niezbyt groźna, bliżej jej do półświatka niż do normalnego życia.

## Inne

### Wygląd

Całkiem ładna, zadbana brunetka średniego wzrostu. Zwykle ubrana w praktyczne ciuchy. Wie, jak używać biżuterii by zwracać na siebie uwagę i zupełnie nie unika wykorzystywania urody do zdobywania czego chce od mężczyzn.

### Coś Więcej

.

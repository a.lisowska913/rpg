---
layout: inwazja-karta-postaci
categories: profile
factions: "astoria, zaczestwiacy, szkola magow"
owner: "public"
title: "Karolina Erenit"
---

# {{ page.title }}

## Paradoks

potężna ixiońska czarodziejka identyfikująca się ze światem ludzi

## Motywacja

### O co walczy

* kontroluje swoją moc, nikogo nie skrzywdzi i wie co się stanie gdy rzuci czar
* znalazła swoje miejsce w nowym świecie- ma pracę i jest przydatna

### Przeciw czemu walczy

* magowie pomiatają ludźmi a ci nawet o tym nie wiedzą
* zatraci się w tym nowym świecie i straci człowieczeństwo

## Działania

### Specjalność

* ogromna moc magiczna
* energia ixiońska
* świetny strateg
* perfekcyjne poruszanie się w świecie ludzi

### Słabość

* gubi się w świecie magii, źle ocenia sytuacje społeczne i faktyczne
* odpycha od siebie innych
* boi się samej siebie

### Akcje

* cicha, wycofana samotniczka; taki nerd
* gra w strategie w virt
* zaprzyjaźnia się z ludźmi, nie magami.
* działa jak człowiek, nie mag
* magią włada jak nieprecyzyjnym dwuręcznym młotem

### Znaczące czyny

* wyszła na sam szczyt Zaczęstwiaków w Supreme Missionforce; dowodzi ekipą
* nie spotkała się z energią czy czarem nie do przełamania / Skażenia; jest na ścieżce maga bojowego
* zastawiła pułapkę na studenciaków podjadających w drogim hotelu i zmusiła Szkołę Magów do publicznej reakcji

## Mechanika

### Archetypy

strateg virt, zbyt genialny mag

### Motywacje

?

## Inne

### Wygląd

?

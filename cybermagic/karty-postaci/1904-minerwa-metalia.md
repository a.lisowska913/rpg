---
layout: cybermagic-karta-postaci
categories: profile
factions: "pustogor"
owner: "public"
title: "Minerwa Metalia"
---

# {{ page.title }}

## Postać

### Ogólny pomysł (2)

* Neuronautka i naukowiec, specjalizująca się w psychotronice. Niezła konstruktorka power suitów. Naukowiec z serca.
* Kiedyś, urocza i otwarta Diakonka owijająca wszystkich jednym paluszkiem.
* Zrekonstruowana przez Saitaera i zregenerowana przez Pustogor w zwykłą czarodziejkę.

### Misja (co dla innych) (2)

* Dąży do ewolucji ludzkości; niech ludzkość i człowieczeństwo są najlepszą formą siebie
* Szybki postęp w psychotronice i magitechnologii - by tworzyć byty inne niż tylko magowie i ludzie, lepsze i pomocne
* Naturalna optymistka - narzędzia zawsze są dobre, po prostu nie można przekazać ich w złe ręce

### Motywacja (co dla mnie) (2)

* Chce być akceptowana i chce być częścią tego świata. Chce mieć przyjaciół i być ceniona. Szuka akceptacji.
* Jest świadoma swojego ciała i wszystkich aspektów cielesnych. Trochę hedonistka. Lubi luksus i rozpieszczanie. Tęskni za Dotykiem Saitaera.

### Wyróżnik (2)

* Wybitnej klasy neuronautka: specjalizuje się w konstrukcji sztucznych umysłów i osobowości, ale też poradzi sobie z analizą
* Badaczka anomalii, zwłaszcza technomantycznych: potrafi robić cuda związane z dziwnymi anomaliami technomantycznymi
* Urocza dusza towarzystwa: potrafi się zachowywać w odpowiedni, bardzo pociągający innych sposób
* Ciało Saitaera: przede wszystkim defensywnie. Jest w stanie funkcjonować w warunkach niemożliwych dla maga czy człowieka

### Zasoby i otoczenie (3)

* Black Technology: rozumie dziwne technomantyczne anomalie. Saitaer jej to "dał". Nie wie czemu to rozumie i jak je budować, ale potrafi...
* Black Stories: dziwne opowieści, z odmiennych światów i czasów. Znowu, dar Saitaera. Im bardziej się oddalamy od Primusa, tym lepiej ona poznaje świat.
* Koszmary: Minerwa ma nieprawdopodobnie chore koszmary i myśli oraz nieludzki umysł. Jej ciało i umysł nie radzą sobie ze wspomnieniami Saitaera i nieludzkiego ciała.
* Wskrzeszona: większość magów którzy o niej wiedzą wie też o jej historii. Sprawia to, że Minerwa jest fundamentalnie minusem społecznie.

### Magia (3)

#### Gdy kontroluje energię

* Mentalistka: zwłaszcza w dziedzinie konstrukcji i tworzenia/modyfikacji sygnału. Raczej pracuje na wzorach (modelach mentalnych) niż pojedynczych myślach i pamięci
* Technomantka: przede wszystkim analiza i konstrukcja. Dotyk Saitaera dał jej unikalną zdolność rekonstrukcji mechanicznej
* Golemy technomantyczne: wiedza o psychotronice i Dotyk Saitaera dały jej coś nowego - umiejętność łatwego tworzenia inteligentnych agentów. Nigdy nie jest sama...

#### Gdy traci kontrolę

* Pojawiają się anomalie, zwłaszcza technomantyczne. Bonus, jeśli pochodzą z martwego świata Saitaera.
* Saitaer widzi obszar w którym zaklęcie jest rzucone przez Minerwę.

### Powiązane frakcje

{{ page.factions }}

## Opis

(15 min)

---
layout: cybermagic-karta-postaci
categories: profile
factions: "pustogor, nurkowie szczeliny, miasteczkowcy"
type: "NPC"
owner: "public"
title: "Erwin Galilien"
---

# {{ page.title }}

## Postać

### Ogólny pomysł (3)

Nurek Szczeliny Pustogorskiej i tinkerer. Technomanta specjalizujący się w budowaniu własnych narzędzi. Trochę taki McGyver. Bardzo lubi ludzi, ale nie lubi ich narażać. Wszystkie przychody reinwestuje w Nutkę oraz w badanie Szczeliny. Świetnie zbiera plotki i jest mistrzem działania w warunkach skrajnych takich jak Szczelina - żadne miejsce go nie złamie. Dodatkowo, kocha sztukę i piękno.

### Motywacja (gniew/wartość, zmiana, sposób) (3)

* BEAUTY; piękno rzeczywistości jest dostępne wszystkim; kolekcjonować, zbierać i rozprzestrzeniać piękno
* CURIOSITY/ADAPTABILITY; poznać anomalie i zbudować narzędzia je rozwiązujące; wizyty w Szczelinie i ciągłe dostosowywanie bezpieczeństwa
* COMMUNITY/SAFETY; nikt poza mną nie może się narażać; wieczne budowanie narzędzi i działanie solo w trudnych sytuacjach

### Wyróżniki (3)

* Mistrz artefaktor. Jeden z najlepszych konstruktorów narzędzi technomantycznych i power suitów na Astorii.
* Ekspert od Szczeliny. Potrafi tam przetrwać i znajdować niesamowite rzeczy.
* Szperacz, scavenger i scrapper. Zrobi z różnych dziwnych artefaktów i anomalii całkiem sprawne narzędzia, też ze złomowiska czy Szczeliny.

### Zasoby i otoczenie (3)

* Nutka: inteligentny power suit ogromnej mocy, acz stosunkowo niewielkiej siły ognia. Bardziej zwrotny i wytrzymały niż bojowy.
* niewielkie acz bardzo interesujące przedmioty pochodzące ze Szczeliny; nic potężnego, ale kurioza
* liczne narzędzia technomagiczne; analityka oraz wsparcie. Szczególnie w obszarze katalizy i survivalu
* jest trochę celebrytą w Pustogorze; uważany za niegroźnego i bardzo sympatycznego
* ma opinię maga przynoszącego pecha; większość jego sojuszników ginie na akcjach w których on uczestniczy

### Magia (3)

#### Gdy kontroluje energię

* bardzo subtelny katalista; mistrz wyczucia nawet minimalnych zmian w pływach energii - np. delikatne zmiany Szczeliny
* mistrz konstrukcji narzędzi i technomancji - prawdziwy inżynier oraz naukowiec w jednym
* innymi słowy, twórca narzędzi oraz osoba identyfikująca różne problemy

#### Gdy traci kontrolę

* Galilien jest mistrzem kontroli i wyczucia magii. Gdy czuje, że traci kontrolę - odcina się od magii i przez pewien czas jest niezdolny do czarowania.

### Powiązane frakcje

{{ page.factions }}

## Opis

.

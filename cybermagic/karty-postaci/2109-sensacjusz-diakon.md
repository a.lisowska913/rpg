---
layout: cybermagic-karta-postaci
categories: profile
factions: "rekin, diakon, aurum, sowiński"
owner: "public"
title: "Sensacjusz Diakon"
---

# {{ page.title }}

## Kim jest

### W kilku zdaniach

Świetny inżynier biowojenny, którego świetlistą karierę przerwało to, że dostrzegł go Anastazy Sowiński i stwierdził, że KTOŚ musi zadbać o Rolanda Sowińskiego - wschodzącą gwiazdę paladyństwa Sowińskich który siedzi u Rekinów. I tak Sensacjusz skończył, niechętnie, w Podwiercie. Anastazy wziął go za LEKARZA, tylko bardziej kompetentnego w te klocki. A potem, gdy Roland opuścił ten teren, Anastazy o Sensacjuszu zapomniał. I wszyscy myślą, że Sensacjusz coś nieprawdopodobnie zbroił i jest na wygnaniu. Sensacjusz nie wie za co tu siedzi.

Drakolita, który miał bardzo niestabilną magię więc wykorzystano Matrycę Diakona by go ustabilizować i uratować. Ale Sensacjusz - ekspert od bioadaptacji - z biegiem lat usunął sobie naleciałości Diakońskie i stał się tym czym chciał być. Przez to jest odszczepieńcem rodu, ale jest zadowolony.

### Co się rzuca w oczy

* Nie jest najbardziej sympatyczny wobec Rekinów - jest dość oschły i rzeczowy. Unika kontaktów towarzyskich.
* Całkowicie ignoruje politykę, mówi to co chce i działa tak jak chce, bo jest ich jedynym i najlepszym lekarzem.

### Jak sterować postacią

* Jest udręczony. NIE CHCE tu być, nie ma dobrych warunków czy laboratorium. Musi zajmować się cholernymi Rekinami, czego bardzo nie chce bo go wkurzają. He enjoys the challenge. Ale Trzęsawisko to dla niego okazja, choć nie pokazuje tego Rekinom. Oni nie wiedzą.
* Adaptuje się - nieważne w jakiej sytuacji by nie był, z rezygnacją zakasa rękawy i zrobi to, co jest potrzebne.
* Ludzka natura nie jest dla niego świętością. Jest świetną kanwą. Dobrym punktem wyjścia. Ewolucja jest fantastyczna, możemy ją przyspieszyć.
* Problemy są ciekawe do rozwiązania, szkoda tylko, że przychodzi z nimi pacjent (lub klient)
* "Kiedyś miałem laboratorium, teraz mam glizdę medyczną..."

## Mechanika

### Znaczące Czyny i Osiągnięcia (3)

* Rekinowi urwało rękę. Sensacjusz odciął uczucie bólu, wyłączył szok i zaproponował - albo glizda albo wyhoduje tymczasowe szczypce kraba czy coś. Rekiny myślały, że to "power move" i odpowiednio doceniły. A on działał poważnie, na serio i nie zorientował się jak to odczytano.
* Raz na jakiś czas z Trzęsawiska Pięknotka czy Alan ściągają Sensacjusza. Za każdym razem dochodzi DALEJ lub działa SKUTECZNIEJ. Usprawnia się i próbuje się sprawdzić na najtrudniejszej biowojennej arenie.

### Co się rzuca w oczy: Atuty i Przewagi (3, 6)

* AKCJA: Świetnie walczy wręcz, całkowicie nieczysto. Praktycznie nie da się go zaskoczyć. Szkolenie (umiejętności) + wzmocnienie biomantyczne
* AKCJA: Umiejętności stabilizacji i utrzymania przy życiu. Jest w tym bardzo dobry, ale nie jest świetnym LEKARZEM. Taki medyk polowy z pet glizda.
* AKCJA: Udoskonala jednostki (ludzie, miślęgi itp). Bierze ludzką naturę i ją usprawnia, dostosowując do potrzeb i sytuacji. Celem jest usprawnienie jednostki w przestrzeni lub pomoc pokonania wyzwania.
* AKCJA: Tworzy i usprawnia jednostki biowojenne. Jego glizda jest praktycznie jego symbiontem i proto-laboratorium.
* CECHA: Bardzo silna regeneracja biomantyczna i Wzoru. Nawet, jeśli się poważnie uszkodzi lub Skazi, szybko się odbuduje samoistnie.
* COŚ: Zaawansowana glizda medyczna. Służy za biosyntetyzator i mechanizm leczniczy. Plus, efekt "fuj".
* COŚ: Wiktor Satarail zezwala Sensacjuszowi na obecność na Trzęsawisku. Nie podejmuje wobec Sensacjusza negatywnych działań, bo Sensacjusz szanuje Trzęsawisko.
* COŚ: 

### Serce i Wartości (3)

* Osiągnięcia
    * Każdy ma potencjał. Każdemu pomogę osiągnąć pełnię potencjału dla tej osoby.
    * Zanim jest skłonny zaaplikować komuś Modyfikację, wpierw testuje ją na sobie.
    * Musisz zweryfikować empirycznie czy Modyfikacja jest skuteczna. Proces iteracyjny.
* Pokora
    * Don't force it. Wie, że może pomóc, ale szanuje granice innych. Nie chce zaszkodzić wprowadzając za szybkie zmiany w populacji.
    * Nie jest rewolucjonistą. Nie zależy mu na chwale. Chce osiągnąć perfekcję i pomóc - tam, gdzie go poproszą lub to jest potrzebne.
    * Cokolwiek się nie stanie, zaadaptuję się i zwyciężę - iteracyjnie.
* Uniwersalizm
    * Nieważne czym jesteś, I can make you better. And I am making myself better every day.
    * Co mogę wydobyć ucząc się Trzęsawiska i jak daleko możemy z tym pójść?
    * 

### Typowe problemy z którymi sobie nie radzi; Słabości (-3)

* CORE WOUND: "Nie jestem tym czym miałem być. Nie jestem tym, czym chciałem być."
* CORE LIE: "Muszę odnaleźć swoje miejsce i swoją formę w egzystencji. Nie mam czasu na nich wszystkich. Tylko ewolucją mogę naprawić błąd rzeczywistości."
* Ocena stanu emocjonalnego i prawdomówności, zwłaszcza płaczącej dziewczyny. Jest podatny na manipulację.
* Odszczepieniec rodu Diakon - usunął sobie dominujące cechy własną inżynierią biomagiczną.

### Magia (3M)

#### W czym jest świetna

* 

#### Jak się objawia utrata kontroli

* 

### Specjalne

* .

## Inne

### Wygląd

* ?

### Coś Więcej

* .

### Endgame

* 

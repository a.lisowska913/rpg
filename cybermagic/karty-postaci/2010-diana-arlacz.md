---
layout: cybermagic-karta-postaci
categories: profile
factions: "orbiter, arystokracja aurum, oddział verlen, ród arłacz"
owner: "public"
title: "Diana Arłacz"
---

# {{ page.title }}

## Kim jest

### W kilku zdaniach

Piromanka i czarodziejka entropii. Diana kocha płomienie, jest nimi zafascynowana. Ma unikalny talent do niszczenia i pociągają ją osoby mające podobne umiejętności i pasję. Przygotowywana jako Arystokratka Aurum, reaguje na sentisieć Arłacz, ale nie została wybrana przez Ród. Oddana Eustachemu na zabój.

### Co się rzuca w oczy

* Niebiesko-zielone włosy, niebiesko-czarny frak arystokratki, figlarny uśmiech i pasja w oczach.
* Bombastyczna i nieprawdopodobnie dramatyczna, zachowuje się jak herosi z filmów.
* Oddana Eustachemu do końca. Spełni każdą jego zachciankę.
* Sieje chaos, destrukcję i niezgodę. W jej otoczeniu wszystko się docelowo rozpada.
* Jest istotą entropii i zniszczenia, ale w pełni akceptuje tą naturę. Nie chce być niczym innym. To jest to czym jest i czemu jest.

## Mechanika

### Znaczące Czyny i Osiągnięcia (3)

* Wyleciała z Kontrolera Pierwszego po całkowitej destrukcji przyjęcia kadetów (łącznie z antymagiczną zastawą), gdy wypiła ZDECYDOWANIE za dużo.
* Nie do końca świadomie doprowadziła do niejednego pojedynku na Castigatorze między arystokratami. Kokietka XD.
* Gdy zarządzała sentisiecią rodu Arłacz, utrzymywała majątek w stanie aktywnym i sprawnym. Sprawnie zarządza i administruje.

### Co się rzuca w oczy: Atuty i Przewagi (3, 6)

* PRACA: Regentka Sentisieci Aurum - potrafi zarządzać i administrować majątkiem czy małą kolonią. Jeśli tylko starczy jej uwagi.
* PRACA: Puryfikatorka - specjalistka od Odkażania i usuwania anomalii. Dobra też w walce jako artyleria.
* SERCE: Płomienie i destrukcja to jej narkotyk. Nie potrafi żyć bez nich. Pragnie rozkładać, dekombinować i niszczyć.
* SERCE: Imprezowiczka i istota głęboko się przywiązująca. Opanowana niszczycielską miłością do Eustachego - i pasją do każdego destruktora.
* ATUT: Wulkan niekierowanej energii magicznej, żywa bateria energii.
* ATUT: Unikalny talent do dekombinacji materii, energii i życia. Istota entropii. Mało czego nie potrafi zniszczyć.

### Charakterystyczne zasoby (3)

* COŚ: Noktiańskie kryształy amplifikujące moc magiczną, ale też destabilizujące efekt (+V +OOO). Świetne jako broń.
* KTOŚ: Maria Gołąb, groźna wojowniczka lojalna rodowi Arłacz, ale bardzo lubiąca niesforną i niszczycielską Dianę.
* WIEM: Perfekcyjnie porusza się po wszystkich mechanizmach arystokratycznych w Aurum i Eterni.
* OPINIA: Kokietka i flirciara, która jak się upije to jest śmiertelnie niebezpieczna. A pije, oj, pije...

### Typowe problemy z którymi sobie nie radzi (-3)

* STRACH: FOMO. Boi się, że dzieją się dziesiątki fajnych i ważnych rzeczy - a wszystkie poza nią.
* SKILL: Nie potrafi pilotować żadnym pojazdem. Serio. Po prostu technologia nie jest z nią kompatybilna.
* SERCE: Łatwo ulega impulsom - ciągnie ją do groźnych mężczyzn i do destrukcji jak ćmę do ognia.
* SERCE: W głębi duszy wierzy, że zniszczenie i entropia to są siły które zawsze wygrywają. "Why run, what's the point".
* MAGIA: Jej moc nie pozwala jej na tworzenie - jedynie na destrukcję.

### Specjalne

* Na śmierć oddana Eustachemu, nie potrafi mu odmówić (Eustachy ma +6 do wszelkich testów wobec niej)

## Inne

### Wygląd

* Krótkie niebiesko-zielone włosy
* Upraktyczniony strój arystokratki, w niebiesko-czarnych kolorach, z emblematem Inferni

### Coś Więcej

* Gdyby filozofia Siriusa@PoE połączyła się z pasją Azonii@Robotech i mocą dekombinacji Scar@FMA, mamy szkielet Diany.

### Endgame

* Jej zdaniem? Wyjdzie za Eustachego.
* Zdaniem Eleny? Wróci do siebie, do Aurum.
* Zdaniem Leony? Wymaga porządnego przeszkolenia bojowego.
* Może trafi do jakiegoś programu treningowego magów entropicznych, nie wiem.

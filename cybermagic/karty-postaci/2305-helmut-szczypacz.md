---
layout: cybermagic-karta-postaci
categories: profile
factions: "orbiter"
owner: "public"
title: "Helmut Szczypacz"
---

## Funkcjonalna mechanika

v1

* **Obietnica postaci**
    * Rozwiążesz Zespołowi większość problemów odpowiednio wykorzystując fabrykatory i produkując sprzęt rozwiązujący problem
        * ze szczególnym uwzględnieniu konfiguracji Lancerów, nawet poza standardowymi konfiguracjami
    * Jako inżynier, płynnie poruszasz się w świecie technologii - drony, statki kosmiczne, servary (Lancery) - to Twój dom
    * Jako inżynier bojowy Orbitera potrafisz działać w próżni, walczyć i hackować - acz preferujesz fabrykator i dostosowywanie sprzętu
    * Jesteś ogólnie lubianą duszą towarzystwa; jakkolwiek nie wychodzą Ci gry hazardowe, ale jesteś swietny w krzyżówkach i grach społecznych wymagających pamięci i inteligencji
* **Słabości postaci**
    * Ma tendencje do za dalekiej optymalizacji, zagrażającej czasem bezpieczeństwu misji - dla własnej chwały i eksperymentów
    * Ma tendencje do eksperymentowania nawet, jeśli dokładnie wie jak zadziałać; to sprawia, że w rutynowych sytuacjach zdarza mu się ostro spieprzyć
    * Uwielbia hazard i ryzyko. Niestety, zwykle przegrywa. Jeśli w danym momencie nie robi czegoś ważnego, na 100% angażuje się w ryzykownych zachowaniach
    * Próżny, lubi świetnie wyglądać i robić wrażenie
* **Zasoby postaci**
    * Servar klasy Lancer, konfiguracja inżynieryjna i ciężka
    * Dostęp do dużej ilości blueprintów i planów, dostęp do fabrykatorów i kodów odblokowujących fabrykatory
    * Dekombinator (sprzęt -> materiały) i fabrykator (materiały -> sprzęt), o wysokiej wydajności
* **Klucz do zrozumienia postaci**
    * Inżynier zakochany w optymalizacji, chce jak najmniejszym kosztem osiągnąć jak najwięcej, czasem aż posuwając się za daleko
    * Hiperspecjalizuje się w wykorzystywaniu fabrykatorów, jego zdaniem "na wszystko jest jeszcze jeden blueprint, da się wyprodukować rozwiązanie"
    * Pragnie założyć rodzinę w niewielkiej planetoidzie, na uboczu wszystkiego - i oddzielić się od społeczeństwa. Samowystarczalność zapewnią fabrykatory i handel przy użyciu dron
    * Pasjonuje się nowinkami technologicznymi, potrafi godzinami o tym rozmawiać

v2

* **Aspekty charakteru**
    * Siły: dusza towarzystwa, uwielbia nowinki technologiczne, zakochany w fabrykatorach i optymalizacji, 
    * Słabości: uwielbia gry hazardowe i ryzykowne zachowania, nie radzi sobie z rutyną
* **Aspekty strategii / podejścia**
    * Siły: wyprodukuje i dostosuje sprzęt do potrzeb, combat engineer Orbitera z servarem, dobry jako prepper
    * Słabości: zaniedbuje bezpieczeństwo dla eksperymentów, uwielbia hazard ale przegrywa, łatwo odwrócić jego uwagę, słaby w sytuacjach wymagających rutyny, "oh! Shiny!", lubi robić wrażenie; próżny
* **Aspekty umiejętności**
    * Siły:  MISTRZOWSKA OPTYMALIZACJA FABRYKATORÓW, wyszkolony inżynier bojowy Orbitera (próżnia, servar itp), kiepski hazardzista, biegły w technologii Orbitera, dostosowuje sprzęt do siebie
* **Aspekty zasobów**
    * Siły: *Servar klasy Lancer; konfiguracja inżynieryjna i ciężka, Dostęp do dużej ilości blueprintów i planów; dostęp do fabrykatorów i kodów odblokowujących fabrykatory; Dekombinator (sprzęt -> materiały) i fabrykator (materiały -> sprzęt), o wysokiej wydajności

## Kim jest

### Fiszka

* Helmut Szczypacz: inżynier servarów, atarien
    * (ENCAO:  +000+ | Samolubny;; Zadbany, świetnie wygląda;; Pełen pasji | VALS: Achievement, Family > Benevolence | DRIVE: Własna planetoida fabrykacyjna z rodziną )
    * styl: optymizm, zero cynizmu i "ups"
    * Motto: "There is a blueprint for that!", "Da się to zrobić TANIEJ!"

### Jak sterować postacią

* Wizja
    * JA: własny fabrykator w planetoidzie z kochającą rodziną, na boku cywilizacji. Zero interakcji ze światem.
    * ŚWIAT: Orbiter jako źródło fabrykacji wszystkiego co jest ludziom potrzebne na całym świecie. Wszystko zarządzane przez maszyny, optymalnie.
* Dominujące Strategie
    * konfiguracja sprzętu i fabrykatorów "there is a blueprint for that"
    * działanie osobiste przy użyciu Lancera w konfiguracji inżynieryjnej
    * wykorzystywanie nowych technologii w nowy sposób
* Charakter
    * optymista pełen pasji z ryzykownymi zachowaniami, zero cynizmu, 100% "ups, coś nie wyszło"
* Wzory 
    * brak
* Inne
    * .

### Serce i Wartości (3)

.

* Ocean
    * ENCAO: +000+
    * Samolubny;; Zadbany, świetnie wygląda;; Pełen pasji
* Wartości
    * TAK: Achievement, Family
    * NIE: Benevolence
    * A: 
    * F: 
    * B: 
* Silnik
    * 
* Marzenie (jeśli inne niż Silnik)
    * 
* CORE WOUND: ""
* CORE LIE: ""

### Magia

#### Dominująca moc

BRAK

#### Jak się objawia utrata kontroli

* BRAK

### Znaczące Czyny i Osiągnięcia 

* 
* 
* 


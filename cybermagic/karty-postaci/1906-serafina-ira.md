---
layout: cybermagic-karta-postaci
categories: profile
factions: "blekitne niebo"
owner: "public"
title: "Serafina Ira"
---

# {{ page.title }}

## Postać

### Ogólny pomysł (3)

Iluzjonistka oraz performerka efektownych koncertów. Jej drugie oblicze to SBN "Banshee", soniczna wojowniczka używająca ludzkich i mechanicznych agentów. Powiązana z Cieniaszczytem i przestępcami Pustogorskimi.

### Czego chce a nie ma (3)

* niech ogień nie gaśnie, wolna i aktywna; każdy ruch to potencjalne zagrożenie, łatwo wpaść w beznadziejność
* uwolnić ofiary Pustogoru i Orbitera, ratować słabszych; jej przyjaciele zostali uwięzieni przez Pustogor
* dać radość i ukojenie; nie przejdzie obojętnie koło cierpienia, nawet wroga

### Sposób działania (3)

* chaos - odwracanie uwagi, iluzje, kłamstwa, aktorstwo, muzyka dysonansu, iluzyjny świat
* miłość - hipnoza, piękno muzyki, czar i urok, aktorstwo
* zniszczenie - broń soniczna, servar 'Banshee'
* agenci - konstrukty, fani i miłośnicy performerki

### Zasoby i otoczenie (3)

* servar 'Banshee': lekki i trudny do wykrycia, szybki servar soniczny
* agenci: zarówno ludzcy fani jak i konstrukty zbudowane dla niej przez Błękitne Niebo
* ukochana Cieniaszczytu: sponsorzy i bogacze, którzy kochają jej sztukę

### Magia (3)

#### Gdy kontroluje energię

* Iluzje oraz magia soniczna
* Zdalna kontrola urządzeń i mechanizmów

#### Gdy traci kontrolę

* Zimny gniew soniczny - destrukcja i rezonans
* Melodia i muzyka
* Iluzje, pokazujące jej prawdziwe uczucia i naturę

### Powiązane frakcje

{{ page.factions }}

## Opis

Przyjęła ród Ira, gdyż za wszelką cenę pragnęła odzyskać swoich przyjaciół z Pacyfiki - a przechwycił ich Orbiter z pomocą Pustogoru.

(8 min)

---
layout: cybermagic-karta-postaci
categories: profile
factions: "myszeczka, pustogor"
owner: "public"
title: "Olga Myszeczka"
---

# {{ page.title }}

## Postać

### Ogólny pomysł (3)

Hodowca i eliminator nadmiernego Skażenia przy użyciu viciniusów użytkowych. Osoba ratująca i pomagająca zwierzakom. Technomantka specjalizująca się w transporcie viciniusów do odpowiedniego miejsca i zarządzaniu hodowlą. Mało kto tak jak ona zna się na różnych viciniusach i formach Skażenia. Ekspert od Skażeńców. Trochę "Solarianin" Asimova.

### Motywacja (gniew/wartość, zmiana, sposób) (3)

* VISION/POTENTIAL; świat gdzie zwierzęta i viciniusy są równi magom; separacja od świata i budowa własnej utopii
* STEWARDSHIP/SUSTAINABILITY; bezpieczna dla wszystkich rzeczywistość w harmonii; redukcja Skażenia i promocja swojego podejścia
* SELF-RELIANCE/RESTRAINT; nikt nie nadużywa mocy i władzy, wszystkim wedle potrzeb; jest żywym przykładem oraz udowadnia, że się da
* poważna, nieagresywna, lękliwa, unika ludzi i magów, cicha i nieśmiała

### Wyróżniki (3)

* hodowca i weterynarz zwierząt i viciniusów: zna się na większości viciniusów, jak im pomóc oraz jak zapewnić im idealne warunki.
* ekspert od Skażenia: wie doskonale jak działa Skażenie, jakie są jego formy i jak z danym Skażeniem wejść w interakcję.
* transport istot niebezpiecznych: mistrzyni logistyki i unieszkodliwiania niebezpiecznych istot.

### Zasoby i otoczenie (3)

* Linie dystrybucyjne Zajcewów, pozyskuje bardzo dziwne istoty z Syberii i próbuje je zagospodarować. Też: przemytnicy.
* Wsparcie hodowli Myszeczków, ma znajomości w różnych egzotycznych hodowlach swojego rodu i może pozyskać co ciekawsze istoty - lub dowiedzieć się, z czym tym razem ma do czynienia.
* Chmara ocznych impów służebnych, pomniejsze nietoperzokształtne impy zajmujące się pracami domowymi, obserwowaniem, szpiegowaniem i obserwowaniem terenu.
* Pojazd transportowy służący też jako więzienny, bardzo wzmocniony pojazd do transportu viciniusów... lub bezpiecznego zamknięcia przeciwnika.
* Niemała posiadłość niedaleko Wielkiej Szczeliny - silnie zautomatyzowana - gdzie żyją viciniusy. Droga i trudna do utrzymania; ma tam odratowane viciniusy.

### Magia (3)

#### Gdy kontroluje energię

* Technomantka, budująca roboty i maszyny wspierające i logistyczne. Sztuczne łapki dla glukszwajna, czy mechaniczne skrzydła dla śledzia syberyjskiego...
* Magia lecznicza, zarówno wobec ludzi i magów (gorzej) jak i wobec viciniusów i zwierząt (zdecydowanie lepiej). Nie adaptuje ich - tylko leczy i naprawia.
* Ogólnie rozumiane pułapki i sposoby powstrzymywania przeciwnika (czy to biochemicznie czy mechanicznie) by obezwładnić nie robiąc krzywdy.

#### Gdy traci kontrolę

* Pod wpływem pozytywnych emocji, jej magia lecznicza ma tendencje do nadmiernego wzmocnienia istot żywych - zwłaszcza rośliny próbują się bronić przed zwierzakami ;-).
* Pod wpływem negatywnych emocji, maszyny i zniewolone zwierzęta (znaczy, 'pet') buntują się przeciwko ludziom i magom.
* Jej magia zwykle objawia się technomantycznie lub biomantycznie. Czasem sama wpada w pułapkę którą zastawiła, czasem zmienia się (lub kogoś) w zwierzątko... 

### Powiązane frakcje

{{ page.factions }}

## Opis

.

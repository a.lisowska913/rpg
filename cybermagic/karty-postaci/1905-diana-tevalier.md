---
layout: cybermagic-karta-postaci
categories: profile
factions: "elisquid, multivirt"
owner: "public"
title: "Diana Tevalier"
---

# {{ page.title }}

## Postać

### Ogólny pomysł (2)

* W multivirt gra supportem; nazwa kodowa Chevaleresse. Członkini EliSquid. Bezwzględnie oddana Alanowi i gildii.
* Scavenger i dziecko ulicy. Potrafi przetrwać w praktycznie każdym mieście. Świetnie się chowa.
* Aktorka. Potrafi grać dużo młodszą niż jest. Wykorzystuje swoje umiejętności do przebierania się i maskowania.
* Kucharka. Jest świetną kucharką, potrafi zrobić niesamowicie smaczne jedzenie przy posiadaniu prawie niczego.

### Wyróżnik (2)

* Poszerzony hipernet; z każdego miejsca potrafi wejść w multivirt. W multivirt jest skuteczniejsza i lepsza niż w realu.
* Koszmary i strach. Diana jest świetna pod względem przerażania wszystkich. Ma wyjątkowo wiarygodne obrazy. Dużo widziała.
* Potrafi się schować. Ma duży talent do chowania się i pozostawania niezauważoną.

### Czego chce od innych (2)

* Niech inni też zobaczą, że multivirt i świat rzeczywisty są realnym światem - niech dla nich te światy się połączą.
* Niech słabsi od niej są chronieni. Jest skłonna sama ich chronić - niech nie mają takich koszmarnych obrazów do emisji jak ona.
* Wolność. Całkowita autonomia, swoboda poruszania się i możliwość robienia co tylko chce.
* Oddaje lojalność całą sobą i bezgranicznie. Oczekuje podobnej lojalności.

### W jaki sposób działa (2)

* Żywioł Ziemi.
* Strasznie uparta; nie boi się stanąć naprzeciw większym siłom od siebie. Immovable object. Bardzo wytrzymała.
* Pomocna; chroni oraz opiekuje się innymi. Przede wszystkim wsparcie lub ochotniczka do misji gdzie nikt nie chce iść.

### Zasoby i otoczenie (3)

* Spokrewniona ze znaną oficer, Teresą Tevalier. Przez to niektórzy jej idą lekko na rękę.

### Magia (3)

#### Gdy kontroluje energię

* Ukrywanie się i maskowanie: ukrywanie się, maskowanie, makijaż, iluzje... wszystko, by nie dało się dostrzec drobnej Chevaleresse.
* Koszmary i odstraszanie: odwracanie uwagi, terror, zabezpieczenie terenu. Bonus: przed owadami i dziką zwierzyną.
* Stabilizacja i przetrwanie: modyfikacja metabolizmu, przetrwanie w trudnych warunkach, podtrzymanie życia.
* Przyprawy i gotowanie: potrafi robić świetne i bardzo smaczne jedzenie.

#### Gdy traci kontrolę

* Multivirt i świat rzeczywisty stają się jednością

### Powiązane frakcje

{{ page.factions }}

## Opis

(xx min)

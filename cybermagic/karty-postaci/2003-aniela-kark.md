---
layout: cybermagic-karta-postaci
categories: profile
factions: ""
type: "NPC"
owner: "kic"
title: "Aniela Kark"
---
# {{ page.title }}

## Postać

ARCHETYP: 	
Koncept, kim jest ta osoba? (3 żetony)
Lekarz, co wszystko wyszczurzy.


Jakie ma silne strony? (3 żetony)
Ciało / Fizyczne	
* Serce / Społeczne
Wiedza / Naukowo-Biurokratyczne	
* Technika / Narzędzia I Konstrukcja	
Eter / Magia i Intuicja

Czym osiąga sukcesy? (3 żetony)
Konfrontacja / bezpośrednio, agresywnie	
* Wpływanie / manipulacja, manewry	
Wspieranie / dowodzenie, wzmacnianie, budowanie
1. Stymulanty, lekarstwa, środki biochemiczne
2. 
3. 

Jakie zasoby ma do dyspozycji? (3 żetony + konsekwencja)
Uczniowie, którym pomogła.



---
layout: cybermagic-karta-postaci
categories: profile
factions: "orbiter, zelazko"
owner: "public"
title: "Olgierd Drongon"
---

# {{ page.title }}

## Kim jest

### W kilku zdaniach

Niedźwiedziowaty, wyglądający na niezgrabnego i nieszczególnie bystrego kapitan fregaty Orbitera. W rzeczywistości - agent do operacji beznadziejnych i wybitny strzelec, którego doskonałe wyczucie niejednokrotnie uratowało sytuację. Generator ogromnych kosztów dla Orbitera.

### Co się rzuca w oczy

* TODO

### Jak sterować postacią

* Olgierd jest nieprawdopodobnie twardy i jego magia to wspiera. Robi rzeczy, które dla większości postaci są niemożliwe (bo ciało / umysł by nie wytrzymały). Jego manewry mogą być mordercze dla kogoś z innym ciałem niż on.
* Nie dba o prestiż czy co myślą o nim inni. Nie dba o koszty i politykę. Jest lojalny i jeśli przyjaciel potrzebuje pomocy, pomoże mu.
* Nie oczekuje od nikogo czegoś, czego sam by nie zrobił. Jest żywym przykładem epickości Orbitera. Larger-than-life figure. Zachęci, pomoże, podniesie. Niepowstrzymywalny. Paragon of Orbiter.
* Bardzo optymistyczny, z nieskończonym zapasem morale i pozytywnego podejścia do wszystkiego. Nigdy nie narzeka. Nie jest cyniczny. Sprawia wrażenie naiwnego.
* Nie podejdzie do dziewczyny, która mu się podoba. Tu jest nieśmiały.
* Nie jest mściwy ani złośliwy. Jest dobroduszny i sympatyczny. Ale zrób coś niegodnego a jego gniew będzie straszny.
* Lubi się wygłupiać i żyć pełnią życia. 
* Zachowuje zimną krew. Jest odpowiedzialny wobec załogi czy Orbitera.
* Zawsze się przygotowuje.

## Mechanika

### Znaczące Czyny i Osiągnięcia (3)

* Wszedł do Aurum uratować pirata, wobec którego miał dług honorowy. Wkradł się do więzienia, po czym unieszkodliwił kilka osób, zastraszył resztę i wyszli stamtąd nieniepokojeni. Potem oddał się do dyspozycji Kramera.
* Gdy Anomalna Infernia wróciła z Anomalii Kolapsu, Olgierd przekonał Kramera by dal Żelazku misję zweryfikowania i albo uratowania Inferni albo jej egzekucji.
* Zamaskowany jako gladiator na Valentinie, zebrał informacje o przyszłej aktywności piratów.

### Co się rzuca w oczy: Atuty i Przewagi (3, 6)

* CECHA: Nieprawdopodobnie twardy i wytrzymały na wszystkich kanałach - magicznym, fizycznym, emocjonalnym, mentalnym. Niepowstrzymany juggernaut.
* AKCJA: Zastraszanie i dominacja. Niezależnie od tego z kim ma do czynienia, Olgierd potrafi zmusić innych do zrobienia tego czego sobie życzy.
* AKCJA: Inspiracja własnym przykładem. Z optymizmem i odwagą zrobi to, co jest potrzebne i pociągnie za sobą innych. Koło niego jest najbezpieczniej XD.
* AKCJA: Szturmowiec Orbitera. Walczy w zwarciu i z bliska. Specjalizuje się w nieczystej walce. Mistrz sztuk walki + walki nożem.
* AKCJA: Każda broń w rękach Olgierda to coś morderczego. Jest mistrzowskim artylerzystą. Zarówno na zasięg bliski jak i daleki, statkiem kosmicznym lub osobiście.
* AKCJA: Jest perfekcyjnie przygotowany i ma świetne wyczucie. Niekoniecznie wie jak dokładnie działa przeciwnik, ale wie DOKŁADNIE jak daleko on może się posunąć ze swoją załoga, pojazdem i sprzętem.
* COŚ: Słynie z nieoczekiwanych i nietypowych akcji i zachowań. Jego reputacja sprawia, że przeciwnicy niekoniecznie wiedzą czego się spodziewać. Słynie też z tego, że nie blefuje.

### Serce i Wartości (3)

* Tradycja
    * My jesteśmy Orbiterem. My jesteśmy po to, by inni żyli bezpiecznie. By mogli budować nowy, lepszy świat. My umieramy, by oni mogli żyć. To jest słuszne - silni bronią słabych.
    * Rozkaz to rozkaz. Nie musi się podobać. Ktoś wie, o co chodzi i cel nadrzędny przełożonych należy wykonać a samych przełożonych wspierać. Gadanie o tym jak głupi jest przełożony jest nieakceptowalne. Bunt jest nieakceptowalny.
    * Ważniejsze niż życie czy smierć jest LEGACY - to, co po Tobie zostanie. To, jak zmieniłeś świat. Wszyscy umrzemy. Ważne, by umrzeć ZA COŚ.
* Stymulacja 
    * Zgłasza się do najtrudniejszych misji, bo jego oddział sobie poradzi. Plus, bo tylko wtedy warto naprawdę żyć.
    * Wszystko próbuje przekształcić w grę z punktacją. Nie chodzi o to by wygrać. Chodzi o to JAK BARDZO wygrać. Nie ma sytuacji, których wygrać się nie da.
    * Wszystkie operacje wedle możliwości realizuje samemu, osobiście. Jeśli jest coś ryzykownego lub trudnego, chce zająć się tym osobiście.
    * Wybiera bardzo nieoczekiwane i pozornie bardzo ryzykowne rozwiązania, których często nie wybrałby ktoś „zdrowy psychicznie”.  Słynie z bardzo nieoczekiwanych działań.
* Samosterowność
    * Dług honorowy, lojalność, przyjaźń - znaczą dużo więcej niż rozkaz czy jakakolwiek frakcja. Na frontierze musisz podejmować decyzje nie mając dostępu do łańcucha dowodzenia.
    * Robi to, co uważa za słuszne, nawet, jeśli to jest niezgodne z rozkazami. W czym potem wróci do Kramera, powie dokładnie co zrobil i da się aresztować. Mimo wszystko szanuje Orbiter i zasady.
    * Niezależnie od tego jaka jest załoga, jakie masz środki, czego Ci brakuje - póki masz COKOLWIEK należy walczyć i zrobić wszystko by wykonać misję.

### Typowe problemy z którymi sobie nie radzi; Słabości (-3)

* CORE WOUND: "Nieważne jak wiele nie robimy i nie działamy, nigdy nie jesteśmy w stanie wygrać. Zawsze nasi umierają, zawsze jest jeszcze jedna bitwa."
* CORE LIE: "A warrior walks alone. Mogę mieć podwładnych, ale nie poproszę o pomoc. Nie mam komu się zwierzyć. Przeznaczeniem każdego wojownika jest samotna wyprawa na górę, która w końcu go pokona."
* Nie manipuluje, nie oszukuje, nie kłamie. Jest bardzo prostolinijny. Nie oszukuje nawet przez ominięcie mówienia o czymś. Jest najgorszym negocjatorem jakiego spotkasz. Po prostu jak nie chce to czegoś nie powie.
* Bardzo łatwy do wprowadzenia w pułapkę; nie chce nikogo zostawić w potrzebie samego. Nie zostawi umierającego statku kosmicznego jeśli może pomóc, mimo ryzyka.
* Zawsze spłaca swoje długi. Ale nigdy nie zaciąga ich w imieniu innych osób. Bardzo honorowy, we wszystkich znaczeniach tego słowa.
* Zupełnie nie radzi sobie z polityką i „dworem”. Nigdy nie awansuje i nie dostanie większej jednostki. Reputacja „maverick”.

### Magia (3M)

#### W czym jest świetna

* Korozja materii: potrafi rozpaść czy zdestabilizować różne mechanizmy lub materiały. W wypadku zaawansowanych narzędzi, może je eksplodować.
* Ekrany ochronne: pola siłowe, wzmocnienia materii, sfery kinetycznej ochrony. Efekty chroniące przed kanałami atakujacymi.
* Skanowanie słabych punktów: Olgierd często potrafi znaleźć jakie są słabe punkty materii, pojazdu czy struktury.

#### Jak się objawia utrata kontroli

* Devastating Collateral Damage. Eksplozje, rozpad, zniszczenie - a on w epicentrum.
* Rzeczywistość się lokalnie rozpada - teren staje się skrajnie niebezpieczny dla wszystkich; wymaga ewakuacji

### Specjalne

* .

## Inne

### Wygląd

* ?

### Coś Więcej

* .

### Endgame

* 

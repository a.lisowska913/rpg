---
layout: cybermagic-karta-postaci
categories: profile
factions: "pustogor, rekin, mysiokornik, aurum"
owner: "public"
title: "Santino Mysiokornik"
---

# {{ page.title }}

## Kim jest

### W kilku zdaniach

Egocentryczny Rekin, kreujący swoją tożsamość w mediach społecznościowych i rzeczywistych. Ma gdzieś ludzi i innych Rekinów; chce rozgłosu, pieniędzy i sławy. Dobry manipulator, który wykorzystuje media społecznościowe by działały dla niego. Dba o swoje ciało i pozycjonuje się jako okaz męskości. Dobry sprzedawca. Hedonista. Niesamowicie trudny do przyskrzynienia szczur mediów społecznościowych.

### Co się rzuca w oczy

* Nie jest najlepszym pilotem ścigacza, unika działań bezpośrednich. Ale jest silny i wysportowany, dobrze wygląda i ma sensowne ciało.
* Ma wianuszek fanów, którzy chcą być "tak epickim jak on" i fanek, które chcą wejść mu do łóżka. Zarówno w Aurum ("bad boy") jak i w Podwiercie.

### Jak sterować postacią

* Bardzo oportunistyczny, jeśli widzi okazję by coś osiągnąć to to zrobi.
* Zawsze próbuje wyglądać świetnie w oczach innych - bardzo dba o swój wizerunek i nieskazitelny ubiór.
* Bardzo duża obecność w mediach społecznościowych i w MultiVirt.
* Jeśli nie ma nic do zyskania i nie jest zainteresowany działaniem, umywa ręce i się wycofa.
* Jeśli może coś sprzedać / zyskać i wymanewrować by nie było na niego, zrobi to. Serio nie dba o innych.
* Mimo bardzo ryzykownych działań, zawsze ma drogę wycofania się i ucieczki. Strasznie trudny do przyskrzynienia szczur.
* Nie chce współpracować z innymi. 

## Mechanika

### Znaczące Czyny i Osiągnięcia (3)

* Zasłynął ze sprzedawania wody po kąpieli w wannie dla swoich megafanów - "będziesz tak epicki jak ja dzięki infuzjom energii magicznej". I ludzie to KUPUJĄ! Też w Podwiercie...
* Zrobił challenge w SocialVirt - i zdobył sporo nagich fotek dziewczyn. Potem zrobił sobie z tego kalendarz i powiesił na ścianie pokoju. Nawet odbitki dał kolegom za fanty.
* Do siłowni do której chodził zaczęły łazić ciemne typy. Użył SocialVirt by zrobić presję i przed kamerami wjechał tam z policją by wyczyścić teren.
* Gdy został wyzwany na pojedynek, jego fani nie dali jego rywalowi spać, death threats etc. Santino wygrał przez wyczerpanie przeciwnika.

### Co się rzuca w oczy: Atuty i Przewagi (3, 6)

* AKCJA: Ma gadane i jest dobrym podrywaczem. Zawsze tak coś powie, by nie było na niego - wyślizgnie się w oczach opinii publicznej. Dobry w robieniu SUGESTII i PODSZEPTÓW. Ogólnie, ludzie mu bardzo wierzą.
* AKCJA: Przedstawia wydarzenia w odpowiedni sposób w oczach opinii publicznej. Potrafi odpowiednio zrobić twist na różne opowieści. Przez to kształtuje rzeczywistość.
* AKCJA: Wszystko potrafi zrobić w bardzo IMPONUJĄCY sposób, nawet jak nie jest bardzo skuteczny.
* AKCJA: Dobry w walce i pojedynkach. Nie jakiś najlepszy, ale zdecydowanie w górnej grupie Rekinów.
* AKCJA: Świetny w budowaniu drogi ucieczki i zrzucania winy na innych. Niesamowicie trudny do przyskrzynienia szczur.
* CECHA: Wysportowany, silny i szybki. Okaz "męskości". Bardzo dba o swoje ciało, ładnie pachnie itp.
* CECHA: Inne Rekiny nie chcą wchodzić mu w drogę; ma za dużą popularność i jest po prostu zbyt podły.
* COŚ: Ma wianuszek fanów, którzy chcą być "tak epickim jak on" i fanek, które chcą wejść mu do łóżka. Zarówno w Aurum ("bad boy") jak i w Podwiercie. Korzysta.
* COŚ: Ogromna popularność, fani itp. w mediach społecznościowych (SocialVirt). Z tego wynikają odpowiednie profity.

### Serce i Wartości (3)

* Potęga
    * Bardzo dba o swój wizerunek na SocialVirt. Jest szerokim influencerem; mniej dba o Rekiny, bardziej o Aurum i popularność w Zjednoczeniu Letejskim.
    * Często robi różne challenge, ryzykowne operacje itp - ale jak coś idzie nie tak, skutecznie zrzuca winę na kogoś innego. Jest w tym bardzo dobry.
    * Nie dba o moralność czy o robienie tego co "właściwe" - dba o to, co przynosi mu korzyści. Oczywiście, przed kamerą jest "super dobry".
* Stymulacja
    * "Życie jest po to, by go kosztować we wszelkich formach". Nie ma wielu rzeczy których Santino by sobie odmówił.
    * Lubi bardzo ryzykowne działania, jak długo może na kogoś zrzucić konsekwencje porażki. Kosztował życia z Sabiną Kazitan, jak długo była zniewolona wśród Rekinów i wspierał jej plany - "bo zobaczymy co będzie".
    * Nie ma nic przeciwko, by kogoś skrzywdzić by tylko zyskać reputację lub bawić się lepiej. Ale lubi bezpośrednie przyjemności, nie "manipulację".
* Twarz
    * Pozycjonuje się jako "męski koleś poza systemem i wszystkie laski jego". Skutecznie używa tego co ma (zasoby, reputacja, poplecznicy) by budować jeszcze większą reputację i wpływy.
    * Mściwy jak cholera - ważne jest to, by każdy co stanie mu na drodze odpowiednio zapłacił. Najlepiej publicznie.
    * Dąży do tego by być uwielbianym i podziwianym. Szczególnie lubi płaszczenie się w jego stronę. Sam się nie płaszczy nigdy.

### Typowe problemy z którymi sobie nie radzi; Słabości (-3)

* CORE WOUND: "Tak naprawdę, ja nie istnieję. Jeśli mnie nikt nie widzi, nie ma mnie. Boję się samotności."
* CORE LIE: "Jeżeli ktoś mnie nie podziwia i nie kocha, stanowi śmiertelne zagrożenie. Atak w mój wizerunek jest atakiem w moje jądro. Chcą zniszczyć moją psychikę."
* Jeśli jest odizolowany od swojej grupy / popleczników, nie jest szczególnie skuteczny. Działa bardziej grupą niż sam.
* Z uwagi na swoją reputację nie może prosić o pomoc. Może rozkazywać, może dominować, może manipulować - ale nie może prosić.
* Hedonista. Nie potrafi sobie odmówić kosztowania życia. Bardzo łatwo go przez to wciągnąć w coś.
* Ma problemy z odpowiedzeniem na wyzwanie bezpośrednie. Jest zbyt egocentryczny.

### Magia (3M)

#### W czym jest świetna

* Biomancja: Wzmocnienie ciała. O dziwo, katai - czyli walka przy użyciu magii.
* Iluzje: Rejestrowanie wydarzeń i budowanie obrazów. Ogólnie, ubarwianie i persystencja opowieści.
* Iluzje: Dodawanie efektów specjalnych i zwiększaniu epickości. "Reżyser".
* Iluzje: Niewidzialność, odwracanie uwagi, fałszywe obrazy do zrzucania winy na innych.

#### Jak się objawia utrata kontroli

* Ośmieszanie, zarówno jego jak i innych. Rzeczy które wyglądają pogardliwie. Zmiana głosu na "wysoki piskliwy". Itp.
* Pojawia się głos typu "smerf Ważniak", jego wewnętrzny monolog jaki to jest epicki zaczyna być słyszalny w realu.

### Specjalne

* .

## Inne

### Wygląd

* Blondyn z włosami do ramion, niebieskie oczy.
* Serio wysportowany, silny, szybki i atletyczny. Lubi pokazywać ciało.
* Dobrze ubrany.

### Coś Więcej

* .

### Endgame

* ?

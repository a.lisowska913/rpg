---
layout: cybermagic-karta-postaci
categories: profile
factions: "Infernia"
owner: "kic"
title: "Klaudia Stryk"
---

# {{ page.title }}

## Kim jest

### W kilku zdaniach

Walcząca o wolność dla AI badaczka anomalii, która wierzy, że świat może być lepszy niż jest, jeśli tylko damy AI być częścią społeczeństwa.
Najczęściej przy konsoli komunikacyjnej lub w laboratorium. Zwykle mówi prawdę (choć niekoniecznie całą). 

### Co się rzuca w oczy

* Zwykle nie rzuca się w oczy. Siedzi cicho przy swojej konsoli i pracuje za kulisami.

### Jak sterować postacią

* Klaudia wierzy w swoich przełożonych. Zakłada dobre intencje i raczej da szansę, ale nie wierzy ślepo.
* Często działa zza kulisów. Ustawia sytuacje, zmienia zapisy, sprawia, że ludzie się "przypadkowo" spotkają...
* Klaudia nie kłamie jeśli może tego uniknąć (nie umie), ale bez najmniejszego zawahania pogrzebie prawdę w biurokracji.
* Klaudia uwielbia używać systemów przeciwko ich twórcom (jeśli tylko ma powód)
* Klaudia nie jest szczególnie silna społecznie i można ją łatwo oszukać. Jeśli jednak zdradzisz jej zaufanie, bardzo trudno będzie je odzyskać. Najpewniej podejmie kroki przeciwko tobie.

## Mechanika

### Znaczące Czyny i Osiągnięcia (3)

* Ocaliła i ukryła świadome AI gdzieś na planecie (magitrownia?)	
* Gdy była młoda, zwerbował ją stary Grzymość - nie poszła tam dla pieniędzy, ale dla zasad. Była zniechęcona sztywnymi zasadami Pustogoru i tym, że nie mogła realizować swoich celów. Grzymość pomagał Noktianom i Thalii i naprawdę robił to, co mówił.
* Wraz z Martynem uciekli z łap młodego Grzymościa. On jest medykiem, ona naukowcem - to idealny dla Grzymościa zespół. Ani Klaudia ani Martyn początkowo nie znali całej sytuacji. Klaudia to wygrzebała, Martyn to zrozumiał i postanowili uciec bo nie zgadzają się z metodami a wiedzą za dużo by móc odejść. Klaudia sfałszowała rekordy i nasłała Cieniaszczyt na laboratorium, a Martyn sfabrykował ich śmierć.
* Mag z administracji admirał Termii utrudniał życie Elenie, bo nie chciała z nim iść do łóżka. Klaudia się o tym dowiedziała i zadbała o to, by ten mag już nigdy nic nie wydrukował na K1 bez wybitnych problemów.

### Co się rzuca w oczy: Atuty i Przewagi (3, 6)

* AKCJA: Przeszukiwanie i modyfikacja rekordów biurokratycznych. Królowa biurokracji.
* AKCJA: Specjalistka od komunikacji. Przenanalizuje sygnał, określi jego pochodzenie, jak trzeba zdekoduje. Zarówno real jak i virt.
* AKCJA: Przełamuje zabezpieczenia, hakuje, ukrywa i odkrywa prawdę.
* AKCJA: Jak mało kto potrafi rozmawiać z AI. Szkolona przez Thalię, z długą praktyką.
* KTOŚ: Sporo kontaktów w vircie, zwykle wśród istot z forpoczty nauki. Tych wyklętych też.
* COŚ: Zna Noktiański. Jest to dla niej drugi język (bo BIA, bo Thalia)
* COŚ: Paleta anomalii dziwnych i niestabilnych. Takich, których nikt inny nie odważyłby się użyć, a ona jakoś je kontroluje.
* COŚ: Paleta eksperymentalnych magitechów. Na to idzie jej kasa.
* COŚ: Reputacja wśród AI (ale ona jest jej nieświadoma)

### Serce i Wartości (3)

* Uniwersalizm
  * Skupia się na uwolnieniu TAI. Chce świata, gdzie wszyscy mogą działać na równi. Nieważne, czym są.
  * Klaudia dąży do tego, aby wszystkie frakcje współpracowały i się wspierały. Wierzy, że to jedyna droga do przetrwania i zwycięstwa.
  * Stanie za swoimi przekonaniami, nawet wbrew przyjaciołom, nawet jeśli miałoby to oznaczać, że skończy w więzieniu.
* Osiągnięcia
  * Mistrzyni anomalii. Anomalie służą polepszeniu rzeczywistości. Anomalie z czasem stają się technologią.
* Stymulacja
  * Musi być na forpoczcie nauki. Jest na froncie.
  * Ucieczka do przodu. Ta rzeczywistość jest wspaniała, ale musimy ją ulepszyć. Nadal potrzebujemy niewolnictwa (AI i nie tylko), to musi się zmienić
  * Nowe, ciekawe, nieznane, eksperymantalne. To jest to, co ją kręci.

### Typowe problemy z którymi sobie nie radzi; Słabości (-3)

* CORE WOUND: "Nie mam domu. Nigdzie nie ma dla mnie powrotu. Straciłam przyjaciółkę, mentorkę, planetę. Poruszam się w ciszy, więc nikt mnie nie zapamięta."
* CORE LIE: "Jedyny sposób, bym dostała choć przypis w historii to osiągnąć coś wielkiego. Zrobić coś dużego. Uwolnić AI, sprowadzić Zefiris, znaleźć Percivala Diakona..."
* AKCJA: Fatalnie kłamie. 
* CECHA: Słaba empatia. Ma problemy z czytaniem ludzi.
* AKCJA: Walka bezpośrednia
  * Klaudia przeszła podstawowe przeszkolenie, żeby móc dostać swój stopień, ale to nie znaczy, że umie walczyć.
* STRACH: Ominie ją coś nowego, ważnego. Jej umiejętności zmarnują się gdzieś w jakiejś nieważnej placówce lub ze świniami

### Magia (3M)

#### W czym jest świetna

* Sterowanie anomaliami i magitechami. Zwykle wkłada małą moc by osiągnąć zamierzony efekt.
* Rozbrajanie anomalii, czyszczenie energii magicznych.
* Magia komunikacyjna, w szczególności virt. "Informatyka" rzeczywistości.

#### Jak się objawia utrata kontroli

* Tworzenie anomalii magicznych
* Przemieszanie kanałów komunikacji. Pełna synestezja dla wszystkich pod wpływem.

### Specjalne

* .

## Inne

### Wygląd

* Zwykle ubrana w biały fartuch z mnóstwem kieszeni lub coś zbliżonego do tego. 
* Długie włosy zebrane w ciasny kok. 
* Lekko nieobecne spojrzenie.

### Coś Więcej



### Endgame





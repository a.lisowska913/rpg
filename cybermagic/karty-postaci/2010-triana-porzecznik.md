---
layout: cybermagic-karta-postaci
categories: profile
factions: "akademia magiczna zaczęstwa"
owner: "public"
title: "Triana Porzecznik"
---

# {{ page.title }}

## Kim jest

### W kilku zdaniach

Bardzo ambitna młoda konstruktorka, próbująca dorównać sławie i potędze ojca i babci. Często wpada w straszne kłopoty, próbując się jakoś wykazać. Kochliwa i romantyczna, zawsze jest przemęczona kolejnymi działaniami. Nie włada potężną magią dynamiczną, ale jej narzędzia są świetnym fokusem. Ku jej utrapieniu, jest lepszym dekonstruktorem niż wynalazcą.

### Co się rzuca w oczy

* Zawsze ma pełno roboty i wystające jakieś dziwne urządzenia z nadmiarowych kieszeni.
* Dobrze ubrana modnisia, ale bardzo rzadko naprawdę czysta; tu trochę smaru, tu kurz.
* Całkowicie rozkleja się na filmach; nie toleruje cierpienia - a zwłaszcza zwierząt.
* Bardzo przyjacielska; łatwo się zaprzyjaźnia i chętnie pomaga. Nie trzyma uraz.
* Jej głównym zmysłem jest dotyk; nie ma też szczególnego dystansu - to krępuje sporo osób.
* Zwykle wygląda, jakby nie spała trzy noce. Jej roztrzepanie jedynie wzmacnia ten efekt.
* Nie lubi rywalizacji ani konfliktów, ale zawsze próbuje zrobić coś większego i bardziej efektownego niż kiedyś.

## Mechanika

### Znaczące Czyny i Osiągnięcia (3)

* Używając zrobionych z ojcem dystrybutorów energii magicznej schowała przed terminusami anomalnego kiciusia z Trzęsawiska którego szukali; prawie Skaziła małe osiedle. 
* W projekt na dwudniowym hackatonie wsadziła 41 godzin - tylko po to, by zaspać w dniu oddania projektu i skończyć z dyskwalifikacją.
* Na imprezie przez moment została królową imprezy - ale szybko oddała tytuł koleżance, której na tym bardzo zależało, by się lepiej bawić.

### Co się rzuca w oczy: Atuty i Przewagi (3, 6)

* PRACA: Majsterkowanie i gadżety - szycie, składanie domku, elektronika. Złota rączka - złoży i naprawi sporo.
* PRACA: Efekty specjalne i robienie piorunującego wrażenia - strojem, światłem, nastrojem.
* SERCE: Do każdego problemu zaproponować jeszcze bardziej efektowną i złożoną maszynę. W końcu jest z rodu wynalazców.
* SERCE: Udowodnić, że jest godna bycia wnuczką swojej babci i córką swego ojca! Każde wyzwanie jest w stanie pokonać! Musi stworzyć epicki wynalazek!
* ATUT: Seksowna i lubi być na świeczniku. Potrafi i lubi robić wrażenie strojem i umiejętnościami.
* ATUT: Ma naturalny talent do dekonstrukcji, znajdowania słabych punktów w konstrukcjach i rozkładania ich na kawałki.

### Charakterystyczne zasoby (3)

* COŚ: Ogromna ilość świetnych starych eksperymentów, planów i narzędzi zrobionych przez jej babcię, imienniczkę.
* KTOŚ: Ojciec - Triana wierzy, że jest on wybitnym naukowcem. W rzeczywistości ojciec Triany, Robinson, jest niezły... ale to wszystko.
* WIEM: ?
* OPINIA: Zdolna i pracowita dziewczyna, która jak się czegoś podejmie to to zrobi. Trochę zwariowana, ale jaki Porzecznik nie jest?

### Typowe problemy z którymi sobie nie radzi (-3)

* CIAŁO: ZAWSZE jest przemęczona i niekoniecznie kontaktuje. Albo myśli ma gdzieś w dziwnym wynalazku, albo nie spała bo sprawdzała teorię.
* SERCE: Przeszacowuje swoje (i innych) umiejętności i nieco lekko podchodzi do zagrożeń; podejmuje się za dużych wyzwań.
* SERCE: Romantyczna ponad miarę. Zawsze szuka swojej efektownej prawdziwej miłości. Skłonna do poświęceń i efektów specjalnych.
* SERCE: Kocha zwierzęta. Za żadne skarby nie dopuści do cierpienia czy Skażenia jakiegokolwiek zwierzaka.
* STRACH: Uważa, że nie jest dość dobra - nie dorównuje do innych Porzeczników. Musi być lepsza! Musi się wykazać!
* MAGIA: Kiepsko kontroluje magię dynamiczną; potrzebuje narzędzi i urządzeń jako fokusa.
* UMYSŁ: Zwykle zapomina wszystko - a zwłaszcza to, że coś komuś obiecała jak nie jest to istotne.

### Specjalne

* .

## Inne

### Wygląd

.

### Coś Więcej

* .

### Endgame

* .

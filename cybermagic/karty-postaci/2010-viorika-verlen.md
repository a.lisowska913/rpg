---
layout: cybermagic-karta-postaci
categories: profile
factions: ""
owner: "public"
title: "Viorika Verlen"
---

# {{ page.title }}

## Kim jest

### W kilku zdaniach



### Co się rzuca w oczy

* 

## Mechanika

### Znaczące Czyny i Osiągnięcia (3)

* Kiedy młodziutka Elena wpadła w zasadzkę konkurencyjnego gangu dzieciaków, Viorika zorganizowała odsiecz i odbiła kuzynkę.
* Po odkryciu, że Lucjusz działał na terenie Verlenów, używając senti sieci go wypłoszyła i osaczyła oddziałami, które zmotywowała.
* 

### Co się rzuca w oczy: Atuty i Przewagi (3, 6)

* PRACA: Dowódca i szturmowiec.
* PRACA: Potrafi przechytrzyć praktycznie każdego.
* SERCE: Niezależnie od okoliczności daje z siebie wszystko, idąc na front.
* SERCE: We wszystkim dąży do intentsywnej rozrywki, ale nie kosztem zatracenia.
* ATUT: Dobrze zintegrowana z senti siecią.
* ATUT: 

### Charakterystyczne zasoby (3)

* COŚ: 
* KTOŚ: Jej własne oddziały Verlenów
* WIEM: 
* OPINIA: 

### Typowe problemy z którymi sobie nie radzi (-3)

* CIAŁO: 
* CIAŁO: 
* SERCE: 
* SERCE: 
* STRACH: Boi się bezradności. Robi co może, żeby mieć odpowiedź na różne sytuacje.
* STRACH:
* MAGIA: 
* INNI: 

### Specjalne

* .

## Inne

### Wygląd

.

### Coś Więcej

* .

### Endgame

* .

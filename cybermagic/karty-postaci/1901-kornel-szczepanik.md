---
layout: cybermagic-karta-postaci
categories: profile
factions: "pustogor"
owner: "public"
title: "Kornel Szczepanik"
---

# {{ page.title }}

## Postać

### Koncept (3)

* "ukryty" kolekcjoner artefaktów
* właściciel parku rozrywki
* mag-rezydent Samoklęski

### Motywacja (gniew, zmiana, sposób) (3)

* "ci głupcy sami zrobią sobie krzywdę"
* troska o ludzi ze swojego terenu

### Wyróżniki (3)

* znany specjalista od artefaktów

### Zasoby i otoczenie (3)

* Park rozrywki
* Alfred - butler
* Monitoring (żeby mu artefaktów nie ukradli)

### Magia (3)

#### Gdy kontroluje energię


#### Gdy traci kontrolę


#### Powiązane frakcje

{{ page.factions }}

## Opis

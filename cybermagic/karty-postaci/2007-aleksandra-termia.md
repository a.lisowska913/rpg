---
layout: cybermagic-karta-postaci
categories: profile
factions: "orbiter, admiralicja orbitera"
owner: "public"
title: "Aleksandra Termia"
---

# {{ page.title }}

## Kim jest

### Koncept

Admirał Orbitera, skupiona na postępie i rozwoju technologii i rasy ludzkiej. Radykalna, uzdolniona i charyzmatyczna. Silnie wspomagana biomagicznie i technomagicznie. Odrzuca człowieczeństwo dla efektywności. Akceptuje straty konieczne jeśli to przyspieszy. Reprezentuje siłę "musimy iść szybciej by przetrwać".

### Motto

"Musimy iść szybciej by przetrwać JUTRO, niż czuć się dobrze i przetrwać tylko TERAZ. Idąc wolniej nie ochronimy tych, których chcemy. Prędkość jest kluczem."

### Co się rzuca w oczy

* Siatka obwodów, wszczepów i sprzężeń na jej sztucznie upiększonym ciele.
* Długie, czarne włosy; pozwalają na integrację z elektroniką. Są też jej bronią.
* Ma ogromną PASJĘ do swoich poglądów i działań. Nieskończenie zdeterminowana.
* Wspiera wszystkie strony, które dążą do postępu. Zwalcza wszystkie próbujące spowalniać.
* Jawnie mówi, że nie zależy jej na moralności a na przetrwaniu i sile Sektora Astoriańskiego.
* Nie chce władzy. Chce osiągnięcia swojego celu. Jeśli władza jest środkiem - so be it.
* Zazwyczaj opanowana, jeśli uważa, że musi - brutalna

### Przekonania

* Astoria musi zostać zjednoczona. Orbiter i Astoria muszą się zjednoczyć.
* Astoria i Orbiter w chwili obecnej są za słabi, by przetrwać -> potrzebne jest radykalne działanie.
* "I may die as a villain, but I will save everyone".
* Każda astoriańska frakcja ma siły - zachować autonomię, ale ujednolicić wiedzę w Orbiterze.
* Przetrwanie jest ważniejsze niż moralność.

## Mechanika

### Znaczące Czyny i Osiągnięcia (3)

* Kiedyś jej pinasa została porwana. Gdy siły Orbitera przybyły ją ratować, była jedyną ocalałą na pokładzie. Eksterminowała wszystkich.
* Wzięła swój okręt flagowy, OC 'Luctus' i używając jego nienaturalnych mocy znalazła i zniszczyła placówkę Syntianów w Anomalii Kolapsu.
* Mediowała między elementami Noctis a Astorią, by zintegrować pokonanych noktian bez niszczenia ich wszystkich. Stała za Trzecim Rajem.

### Z czym przyjdą do niej o pomoc? (3)

* W sytuacji taktycznie beznadziejnej znajdzie rozwiązanie - zrefituje statek / osobę lub zrobi coś okrutnego ale skutecznego.
* Jeśli trzeba szybko powiązać fakty lub skorelować działania autonomicznych jednostek, jej Sprzężenie jest niezrównane.
* Z uwagi na jej perfekcyjną znajomość / bazę danych odnośnie osób i ich celów, potrafi być niesamowicie przekonywująca. Corruptor.

### Jaką ma nieuczciwą przewagę? (3)

* Wpływy u Kirasjerów, NeoMil i wszelkich frakcji nastawionych bardziej na postęp niż na moralność.
* Jej ciało to żywa platforma bojowa - visiat + wszczepy + wszelkie przekształcenia możliwe.
* Jej umysł i magitech daje jej niesamowitą integrację taktyczną z flotą czy wiedzą.

### Charakterystyczne zasoby (3)

* COŚ: OC 'Luctus' - jeden z najgroźniejszych, najbardziej anomalnych i nienaturalnych krążowników Orbitera.
* KTOŚ: Kirasjerzy i Medea Sowińska - jej dwie najsilniejsze bronie.
* WIEM: Zna większość eksperymentów typu Black Technology - zarówno na ludziach jak i anomaliach.
* OPINIA: najbardziej niebezpieczna admirał Orbitera. Nie można jej nigdy zaufać, bo potrafi pokazać fakty po swojemu!

### Typowe sytuacje z którymi sobie nie radzi (-3)

* Z uwagi na swoje modyfikacje jest wrażliwa na EMP, atak hackerów czy na srebro.
* Nie może operować poza statkiem czy Kontrolerem dłużej, niż 4h. Jej ciało wymaga "zasilania". Może mieć baterie ;-).
* Utraciła umiejętności czarowania. Zbyt przekształcona.

### Specjalne

* brak

## Inne

### Wygląd

* Siatka obwodów, wszczepów i sprzężeń na jej sztucznie upiększonym ciele.
* Długie, czarne włosy. Czerwone, mechanizowane oczy. Alabastrowa cera.

### Coś Więcej

* Prototyp to albo adm. Akkaraju (Shogo) albo Aken Bosch (Freespace2). Też Gilbert Durandal. Ma też elementy sariath Aleksandry Trawens.
* Jej funkcją jest być kontrastem dla Kramera - ona chce iść do przodu, nie chronić. Ona jest tą mroczną stroną Orbitera.
* Ataienne jej nienawidzi z całego serca. Ataienne uważa adm. Termię za najgorsze, co mogło Orbitera spotkać.

### Endgame

* Ataienne: zabije Aleksandrę. A jak nie - zmieni ją w coś podobnego do siebie.
* Ona: zjednoczy Orbiter, potem Astorię, potem Valentinę itp.

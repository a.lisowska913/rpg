# AK Nocna Krypta
## Identyfikator

Id: 9999-ak-nocna-krypta

## Sekcja Opowieści

### Uszkodzona Brama Eteryczna

* **uid:** 210825-uszkodzona-brama-eteryczna, _numer względny_: 7
* **daty:** 0111-11-30 - 0111-12-03
* **obecni:** AK Nocna Krypta, Arianna Verlen, Diana d'Infernia, Elena Verlen, Eustachy Korkoran, Flawia Blakenbauer, Gilbert Bloch, Klaudia Stryk, Medea Sowińska, ON Spatium Gelida, OO Infernia, OO Mfumo

Streszczenie:

Infernia została przechwycona przez Medeę lecąc na Anomalię Kolapsu i przekierowana na uszkodzoną Bramę Eteryczną (gdzie, jak się okazuje, są pasożyty i flota noktiańska która nie doleciała). Na miejscu Infernia uniknęła efemeryd i channelując Kryptę uratowała wszystkich efemerydami. Gdy Krypta pojawiła się na serio, Infernia zwiała paląc silniki. Statki noktiańskie w Bramie mają tether na Infernię.

Aktor w Opowieści:

* Dokonanie:
    * wezwana przez Ariannę PRZYPADKIEM, wykorzystana świadomie do uratowania kogo się da channelując Kryptę w Bramę i kontrolując efemerydy. Potem jak się zmaterializowała na serio, Infernia jej zwiała.


### Pułapka z Anastazji

* **uid:** 201230-pulapka-z-anastazji, _numer względny_: 6
* **daty:** 0111-05-21 - 0111-05-22
* **obecni:** AK Nocna Krypta, AK Rodivas, Anastazja Sowińska Dwa, Arianna Verlen, Damian Orion, Elena Verlen, Eustachy Korkoran, Henryk Sowiński, Klaudia Stryk, Martyn Hiwasser, OA Zguba Tytanów, OO Infernia

Streszczenie:

Krypta "oddała Anastazję", zmieniając cywilny statek Rodivas w pułapkę na Ariannę.

Aktor w Opowieści:

* Dokonanie:
    * podczas integracji Heleny z Donaldem Paziarzem stworzyła piekielny plan i wprowadziła kopię Anastazji by złapać Ariannę w pułapkę - lub przejąć ród Sowińskich.


### W cieniu Nocnej Krypty

* **uid:** 210728-w-cieniu-nocnej-krypty, _numer względny_: 5
* **daty:** 0111-03-22 - 0111-04-08
* **obecni:** AK Nocna Krypta, Arianna Verlen, Atrius Kurunen, Eustachy Korkoran, Finis Vitae, Gerard Adanor, Helena Adanor, Janus Krzak, Oliwia Karelan, Romana Arnatin, Romana Arnatin, Ulisses Kalidon

Streszczenie:

Jak wrócić z tajnej noktiańskiej bazy oderwanej od świata i Bram? Oczywiście, Nocną Kryptą. Arianna wezwała Kryptę i Infernia schowała się w jej cieniu przenosząc się między rzeczywistościami. Po drodze udało się Inferni doprowadzić do zniszczenia niewielkiej niegroźnej floty używając Krypty, trafiła do przeszłości Krypty i widziała Finis Vitae - ale wróciła przez Anomalię Kolapsu do domu. Bo Krypta jest połączona z Anomalią Kolapsu. Aha, część noktian z Inferni została w Zonie Tres.

Aktor w Opowieści:

* Dokonanie:
    * przywołana przez Ariannę, by wrócić do domu, podróżując w Cieniu Krypty przez Nierzeczywistość. Podczas pierwszego skoku zaatakowana przez dwie małe floty; zniszczyła wszystkie. Przypomniała sobie koszmary z czasów pierwszego pojawienia się Finis Vitae, gdy straciła kapitana.


### Krypta i Wyjec

* **uid:** 201216-krypta-i-wyjec, _numer względny_: 4
* **daty:** 0111-01-29 - 0111-01-31
* **obecni:** AK Nocna Krypta, AK Wyjec, Anastazja Sowińska, Arianna Verlen, Diana Arłacz, Eustachy Korkoran, Kamil Lyraczek, Klaudia Stryk, Martyn Hiwasser

Streszczenie:

Podczas walki Nocnej Krypty i Wyjca Zespół wbił się na Nocną Kryptę, by wyleczyć się z Różowej Plagi. Na miejscu okazało się, że Krypta ma "nowe programowanie" (leczenie lub 'zachowanie'), nowe mechanizmy (MIRV, anomalne działo, anioły) oraz że jest groźniejsza niż kiedykolwiek. Arianna zostawiła na pokładzie Krypty Anastazję, by Krypta naprawiła jej Wzór, po czym oszukała Sowińskich, że niby Krypta porwała Anastazję.

Aktor w Opowieści:

* Dokonanie:
    * używając Aniołów asymiluje i preservuje Pocałunek Aspirii / AK Wyjec. Przechwyciła Anastazję Sowińską i umożliwiła Inferni wyleczenie się z Różowej Plagi. Osobowość Heleny na szczycie.
* Progresja:
    * osobowość Heleny przejęła kontrolę nad Kryptą. Chce albo ratować i leczyć albo dla martwych - preserve and remember. Humans are data, after all.
    * wyhodowała MIRV i działo strumieniowe. Jakby poprzednio nie była groźna... no i Anioły Krypty - nekrocyborgi z bańką nierzeczywistości.
    * integruje ze sobą "Wyjca", czyli dawną stację naprawczą Aspirii.


### Pocałunek Aspirii

* **uid:** 201210-pocalunek-aspirii, _numer względny_: 3
* **daty:** 0111-01-26 - 0111-01-29
* **obecni:** AK Nocna Krypta, AK Wyjec, Anastazja Sowińska, Arianna Verlen, Donald Parziarz, Eustachy Korkoran, Juliusz Sowiński, Katra Igneus, Klaudia Stryk, OA Zguba Tytanów, OO Infernia

Streszczenie:

Juliusz Sowiński zażądał oddania Anastazji, ale ona nie chce wracać - jej reputacja i stan są w ruinie. Anastazja chce być jak Elena, ku zgryzocie Juliusza. Tymczasem Arianna zastawiła pułapkę na koloidowy statek - "Pocałunek Aspirii" - i go zestrzeliła. Po czym wezwała Nocną Kryptę by zdjąć z nich klątwę miłości i naprawić co się da z "Pocałunku" i AK Wyjec.

Aktor w Opowieści:

* Dokonanie:
    * ściągnięta przez Ariannę Verlen, by zdjęła z Inferni klątwę miłości i naprawiła Pocałunek Aspirii i AK Wyjec.


### Nocna Krypta i Emulatorka

* **uid:** 200729-nocna-krypta-i-emulatorka, _numer względny_: 2
* **daty:** 0110-11-16 - 0110-11-22
* **obecni:** AK Nocna Krypta, Antoni Kramer, Arianna Verlen, Damian Orion, Eustachy Korkoran, Kijara d'Esuriit, Klaudia Stryk, Laura Orion, Leona Astrienko, Mirela Orion, OO Castigator, OO Minerwa

Streszczenie:

Laura, Emulatorka na Nocnej Krypcie została Skażona przez Kijarę. Laura opanowana wizjami Esuriit chciała zniszczyć Eternię. Nie dało się jej powstrzymać - Mirela, Zespół - byli za słabi. Arianna wezwała Kryptę pod ogniem Castigatora i wypaliła Kryptę i Kijarę do zera, uszkadzając "zdrowie psychiczne Krypty". Zginęło kilkudziesięciu ludzi, ale Arianna dostała podziękowanie od Admiralicji. Damian Orion jest rozczarowany.

Aktor w Opowieści:

* Dokonanie:
    * zainfekowana Kijarą d'Esuriit, generująca halucynacje i astralne struktury. Miała 50-60 osób na pokładzie, gdy została zestrzelona przez Castigator.
* Progresja:
    * Oliwia, umysł kapitan Krypty został zniszczony bezpowrotnie przy wypaleniu przez Castigator. Pozostała tylko Helena.


### Nocna Krypta i Bohaterka

* **uid:** 191025-nocna-krypta-i-bohaterka, _numer względny_: 1
* **daty:** 0108-05-06 - 0108-05-08
* **obecni:** AK Nocna Krypta, Arianna Verlen, Kamil Lyraczek, Wojciech Kuszar

Streszczenie:

Arianna Verlen - bohaterka Orbitera. Zdecydowała się uratować osoby złapane przez Nocną Kryptę i wydobyć je, oraz zdobyć całość wiedzy noktiańskiej. Prawie zginęła; dzięki swojej załodze udało jej się wyjść ze stanu arcymaga, nie została opętana przez kapitan Oliwię z Krypty oraz udało im się wszystkim ujść z życiem. Kosztem przekształcenia Nocnej Krypty w niebezpieczny statek anomalny...

Aktor w Opowieści:

* Dokonanie:
    * Noktiański statek medyczny, który stał się anomalią. Kontrolowany przez zwalczające się umysły BIA Klath oraz oficera medycznego, Heleny. Kiedyś "Alivia Nocturna". Statek stał się własną frakcją.
* Progresja:
    * stała się anomalicznym statkiem-widmem sterowanym przez zespolone umysły BIA Klath oraz Heleny. Poszukuje wiecznie ofiar które może wyleczyć w ciszy kosmosu...


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 7, @: 0111-12-03
    1. Nierzeczywistość    : 1, @: 0111-04-08
    1. Primus    : 7, @: 0111-12-03
        1. Sektor Astoriański    : 6, @: 0111-12-03
            1. Astoria, Orbita    : 1, @: 0111-05-22
            1. Brama Kariańska    : 1, @: 0111-12-03
            1. Kosmiczna Pustka    : 1, @: 0108-05-08
            1. Neikatis    : 1, @: 0111-01-29
                1. Dystrykt Quintal    : 1, @: 0111-01-29
                    1. Arkologia Aspiria    : 1, @: 0111-01-29
            1. Obłok Lirański    : 1, @: 0110-11-22
        1. Sektor Lacarin    : 1, @: 0110-11-22
        1. Zagubieni w Kosmosie    : 1, @: 0111-04-08
            1. Crepuscula    : 1, @: 0111-04-08
                1. Pasmo Zmroku    : 1, @: 0111-04-08
                    1. Baza Noktiańska Zona Tres    : 1, @: 0111-04-08

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 7 | ((191025-nocna-krypta-i-bohaterka; 200729-nocna-krypta-i-emulatorka; 201210-pocalunek-aspirii; 201216-krypta-i-wyjec; 201230-pulapka-z-anastazji; 210728-w-cieniu-nocnej-krypty; 210825-uszkodzona-brama-eteryczna)) |
| Eustachy Korkoran    | 6 | ((200729-nocna-krypta-i-emulatorka; 201210-pocalunek-aspirii; 201216-krypta-i-wyjec; 201230-pulapka-z-anastazji; 210728-w-cieniu-nocnej-krypty; 210825-uszkodzona-brama-eteryczna)) |
| Klaudia Stryk        | 5 | ((200729-nocna-krypta-i-emulatorka; 201210-pocalunek-aspirii; 201216-krypta-i-wyjec; 201230-pulapka-z-anastazji; 210825-uszkodzona-brama-eteryczna)) |
| OO Infernia          | 3 | ((201210-pocalunek-aspirii; 201230-pulapka-z-anastazji; 210825-uszkodzona-brama-eteryczna)) |
| AK Wyjec             | 2 | ((201210-pocalunek-aspirii; 201216-krypta-i-wyjec)) |
| Anastazja Sowińska   | 2 | ((201210-pocalunek-aspirii; 201216-krypta-i-wyjec)) |
| Damian Orion         | 2 | ((200729-nocna-krypta-i-emulatorka; 201230-pulapka-z-anastazji)) |
| Elena Verlen         | 2 | ((201230-pulapka-z-anastazji; 210825-uszkodzona-brama-eteryczna)) |
| Kamil Lyraczek       | 2 | ((191025-nocna-krypta-i-bohaterka; 201216-krypta-i-wyjec)) |
| Martyn Hiwasser      | 2 | ((201216-krypta-i-wyjec; 201230-pulapka-z-anastazji)) |
| OA Zguba Tytanów     | 2 | ((201210-pocalunek-aspirii; 201230-pulapka-z-anastazji)) |
| Romana Arnatin       | 2 | ((210728-w-cieniu-nocnej-krypty; 210728-w-cieniu-nocnej-krypty)) |
| AK Rodivas           | 1 | ((201230-pulapka-z-anastazji)) |
| Anastazja Sowińska Dwa | 1 | ((201230-pulapka-z-anastazji)) |
| Antoni Kramer        | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Atrius Kurunen       | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| Diana Arłacz         | 1 | ((201216-krypta-i-wyjec)) |
| Diana d'Infernia     | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| Donald Parziarz      | 1 | ((201210-pocalunek-aspirii)) |
| Finis Vitae          | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| Flawia Blakenbauer   | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| Gerard Adanor        | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| Gilbert Bloch        | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| Helena Adanor        | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| Henryk Sowiński      | 1 | ((201230-pulapka-z-anastazji)) |
| Janus Krzak          | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| Juliusz Sowiński     | 1 | ((201210-pocalunek-aspirii)) |
| Katra Igneus         | 1 | ((201210-pocalunek-aspirii)) |
| Kijara d'Esuriit     | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Laura Orion          | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Leona Astrienko      | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Medea Sowińska       | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| Mirela Orion         | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Oliwia Karelan       | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| ON Spatium Gelida    | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| OO Castigator        | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| OO Mfumo             | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| OO Minerwa           | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Ulisses Kalidon      | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| Wojciech Kuszar      | 1 | ((191025-nocna-krypta-i-bohaterka)) |
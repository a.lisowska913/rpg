# Ernest Kajrat
## Identyfikator

Id: 1906-ernest-kajrat

## Sekcja Opowieści

### Wygrany kontrakt

* **uid:** 200311-wygrany-kontrakt, _numer względny_: 13
* **daty:** 0110-07-16 - 0110-07-19
* **obecni:** Amanda Kajrat, Dagmara Doberman, Ernest Kajrat, Liliana Bankierz, Lucjusz Blakenbauer, Minerwa Metalia, Ziemowit Zięba

Streszczenie:

Ernest Kajrat jest w stanie śmiertelnym; Lucjusz i Minerwa trzymają go przy życiu. Tymczasem firma dostarczająca jedzenie "Zygmunt Zając" wygrała przetarg (na który nie startowała) odnośnie jedzenia dla zixionizowanej Amandy Kajrat w rękach Grzymościa. Współpraca Blakenbauera i "Zająca" doprowadza do uwolnienia Amandy i kupienia szansy Ernestowi...

Aktor w Opowieści:

* Dokonanie:
    * śmiertelnie ranny, utrzymywany przez Lucjusza przy życiu. Najpewniej gdzieś w Rezydencji Blakenbauerów


### Bardzo niebezpieczne składowisko

* **uid:** 190726-bardzo-niebezpieczne-skladowisko, _numer względny_: 12
* **daty:** 0110-06-22 - 0110-06-24
* **obecni:** Amanda Kajrat, Cezary Alentik, Ernest Kajrat, Gabriel Ursus, Mirela Orion, Pięknotka Diakon, Roland Grzymość

Streszczenie:

Na Składowisku Odpadów Niebezpiecznych przy Podwiercie siły Wolnego Uśmiechu zaczęły składać jakieś narkotyki. Nie podoba się to Błękitnemu Niebu. Pięknotka w środku tego wszystkiego - udało jej się poprosić Orbiter o zajęcie się linią dostawczą z Cieniaszczytu, Kajrata by chwilowo zwolnił, Pustogor by chronili właściciela a Grzymościa, by zszedł do podziemia. Polityczny sukces - ale czemu Grzymość idzie w jakieś narkotyki? I czemu Kajrat dąży do wojny z Grzymościem o te narkotyki?

Aktor w Opowieści:

* Dokonanie:
    * zdecydował się zaufać Pustogorowi w formie Pięknotki by nie eskalować wojny z Wolnym Uśmiechem. A przynajmniej teraz.


### Kult, choroba Esuriit

* **uid:** 190714-kult-choroba-esuriit, _numer względny_: 11
* **daty:** 0110-06-03 - 0110-06-05
* **obecni:** Amanda Kajrat, Ernest Kajrat, Pięknotka Diakon, Tomasz Tukan

Streszczenie:

Tukan poluje dalej na temat somnibela i znalazł kult wywołany przez magię. Pięknotka zdobyła mu uprawnienia a on spieprzył - dał się zainfekować Esuriit i ruszył na wojnę z Kajratem. Pięknotka uratowała mu tyłek za co dostała wsparcie Amandy Kajrat. Z jej pomocą zniszczyła kult w Podwiercie, ale dużo wskazuje że w Czółenku (gdzie indziej...) jest ulubiony ciąg dalszy.

Aktor w Opowieści:

* Dokonanie:
    * przekazał Pięknotce jako wsparcie swoją "córkę". Nie udało mu się zbadać Tukana, ale zbadał dotkniętego przez Esuriit kultystę.


### Somnibel uciekł Arienikom

* **uid:** 190709-somnibel-uciekl-arienikom, _numer względny_: 10
* **daty:** 0110-06-01 - 0110-06-03
* **obecni:** Ernest Kajrat, Jan Revlen, Ksawery Wojnicki, Pięknotka Diakon, Staś Arienik, Tomasz Tukan, Urszula Arienik

Streszczenie:

Bogatemu rodowi Arieników powiązanemu z Luxuritias uciekł somnibel. Pięknotka znalazła go u Kajrata - który dla odmiany nie porwał kociego viciniusa; dostał go od podwładnego. Kajrat oddał somnibela Pięknotce po wykonaniu pewnego testu a napięcie między Arienikami a "plebsem" z Podwiertu się podniosło.

Aktor w Opowieści:

* Dokonanie:
    * całkowicie zaskoczony tym, że ma somnibela; porwał dla niego go Ksawery. Chciał sprawdzić czy Serafina rozerwie link somnibel - ofiara; okazuje się, że tak.


### Upadek enklawy Floris

* **uid:** 190626-upadek-enklawy-floris, _numer względny_: 9
* **daty:** 0110-05-24 - 0110-05-27
* **obecni:** Ariela Sirmin, Ernest Kajrat, Hubert Kraborów, Jolanta Teresis, Konrad Czukajczek, Marcel Sowiński, Nikola Kirys, Roman Rymtusz, Szymon Maszczor, Wargun Ira

Streszczenie:

W odpowiedzi na atak na Pięknotkę, Pustogor wysłał oddział by dowiedzieć się co się dzieje w Enklawach, mający porwać Enklawę Floris. Floris jednak się rozdzieliło - część osób poszła się sama poddać, inni zdecydowali się dalej chować. Poszli do Wiecznej Maszyny i tam znaleźli tymczasową bazę przez posiadanie jednego Skażonego maga (którego Maszyna akceptuje). Jest tam też mag Kajrata zdolny do fabrykacji - źródło Pajęczaków. Połączyli siły i zamaskowali swoje ślady. Pustogor został z niczym, acz z ~10 osobami z Floris, więc... sukces?

Aktor w Opowieści:

* Dokonanie:
    * wzmacnia Enklawy pod szyldem Błękitnego Nieba; zapewnił sobie wsparcie Floris z fabrykatorem Wiecznej Maszyny.


### Noc Kajrata

* **uid:** 190623-noc-kajrata, _numer względny_: 8
* **daty:** 0110-05-18 - 0110-05-21
* **obecni:** Ernest Kajrat, Liliana Bankierz, Ossidia Saitis, Pięknotka Diakon, Serafina Ira

Streszczenie:

Kajrat poszedł za ciosem swojego planu. Przekształcił Serafinę mocą Esuriit; nadał jej Aspekt Banshee, który jednak Serafina jest w stanie jakoś opanować (nie zmieniając jej charakteru ani podejścia). Pięknotka odkryła sekret Kajrata - jest częściowo istotą Esuriit i jest jednym z dowódców Inwazji Noctis. Uwolniła Lilianę spod wpływu Kajrata i udało jej się uratować Ossidię przed śmiercią z rąk upiora Esuriit.

Aktor w Opowieści:

* Dokonanie:
    * Pięknotka odkryła jego sekret powiązany z Esuriit. Doprowadził do transformacji Serafiny Iry w istotę będącą częściowo Banshee - broń przeciwko Ataienne. Skończył wyczerpany ale zwycięski.


### Wojna Kajrata

* **uid:** 190622-wojna-kajrata, _numer względny_: 7
* **daty:** 0110-05-14 - 0110-05-17
* **obecni:** Ernest Kajrat, Liliana Bankierz, Nikola Kirys, Olaf Zuchwały, Pięknotka Diakon, Serafina Ira

Streszczenie:

Kajrat zaeskalował; ściągnął Lilianę i powiedział Pięknotce, że Liliana zostanie jego podwładną jeśli Serafina nie osiągnie sukcesu. Pięknotka poznała historię Serafiny - kiedyś agentka Pustogoru, w wyniku burzy w Pacyfice straciła przyjaciół. Teraz jest cieniem dawnej osoby. Po drodze, w Wolnych Ptakach, Pięknotkę zaatakowała grupa wolnych magów; uruchomił się Cień i Pięknotka zrobiła masakrę. To sprawiło większe problemy i niedogadanie między Enklawami a Pustogorem

Aktor w Opowieści:

* Dokonanie:
    * pokazał bardziej okrutną naturę szantażysty i dominatora przerażonej Liliany; dał jednak Pięknotce chwilę czasu na rozwiązanie sprawy Serafiny i Liliany.


### Anomalna Serafina

* **uid:** 190616-anomalna-serafina, _numer względny_: 6
* **daty:** 0110-05-08 - 0110-05-11
* **obecni:** Antoni Żuwaczka, Ernest Kajrat, Krystian Namałłek, Pięknotka Diakon, Ronald Grzymość, Serafina Ira, Tomasz Tukan

Streszczenie:

W Podwiercie pojawiła się Serafina, piosenkarka zbierająca i asymilująca anomalie. Wyraźnie pomaga jej Kajrat, który ją dodatkowo chroni. Pięknotka dostała zadanie odzyskać te niestabilne anomalie - ale Serafina jest lubiana przez kilku cieniaszczyckich bonzów; nie można odebrać jej anomalii siłą. Dodatkowo, Serafina ma w sobie stary gniew na Pustogor za coś, co się zdarzyło w Pacyfice. Wszystko zbliża się w kierunku na wojnę lub konflikt zbrojny.

Aktor w Opowieści:

* Dokonanie:
    * mastermind stojący za "niech Pustogor i Cieniaszczyt staną do walki przeciw sobie". Trochę pomaga Pięknotce, bardzo Serafinie. Grzymość go w końcu zatrzymał, ale zaczął ziarno konfliktu.
* Progresja:
    * Grzymość mu zdecydowanie mniej ufa. "Mad Dog Kajrat", nie zaufana prawa ręka - czemu chciał wojny?
    * Zły na Pięknotkę - wprowadziła Grzymościa do walki z Kajratem zamiast stanąć naprzeciw niego osobiście.


### Uciekający seksbot

* **uid:** 190519-uciekajacy-seksbot, _numer względny_: 5
* **daty:** 0110-04-21 - 0110-04-22
* **obecni:** Arnulf Poważny, Eliza Ira, Ernest Kajrat, Liliana Bankierz, Ossidia Saitis, Pięknotka Diakon, Saitaer, Teresa Mieralit, Tomasz Tukan

Streszczenie:

Ernest z mafii przybył do szkoły magów w Zaczęstwie szukając swojego seksbota. Pięknotka deeskalowała sytuację i poszła z neuronautą Tomaszem szukać; okazało się, że seksbot jest bardzo zmodyfikowany i bardzo świadomy - do tego stopnia, że seksbotowi zaczęła pomagać Eliza Ira. Pięknotka i Tomasz rozmontowali problem, ale Pięknotka nie miała serca oddać seksbota sadystycznemu Ernestowi. W rozpaczy, poprosiła o pomoc Saitaera, który odpowiedział...

Aktor w Opowieści:

* Dokonanie:
    * dżentelmen i oficer mafii, który - jak się okazuje - ma bardzo mroczne zapędy. Zarówno sadystyczne jak i wobec Ateny. I nic a nic nie ukrywa swojej natury. Nic dziwnego, że się go boją.


### Szczur który chroni

* **uid:** 190505-szczur-ktory-chroni, _numer względny_: 4
* **daty:** 0110-04-18 - 0110-04-20
* **obecni:** Adela Kirys, Alan Bartozol, Ernest Kajrat, Kasjan Czerwoczłek, Krystian Namałłek, Lucjusz Blakenbauer, Oliwia Namałłek, Pięknotka Diakon, Wiktor Satarail

Streszczenie:

Zaczęło się od Skażenia proszkiem kralotycznym Nieużytków Staszka. Potem Pięknotka współpracując z mafią doszła do tego, że Adela i terminus stoją za tą sprawą. Potem Pięknotka uratowała Adelę i terminusa wprowadzając szturmowych terminusów Pustogoru na mafię. Gdy "Cień wyrwał się spod kontroli", Alan ją strzelił z działa strumieniowego. I tak skończyło się rumakowanie na tydzień - ale wszyscy są bezpieczni.

Aktor w Opowieści:

* Dokonanie:
    * prawa ręka mafii. Oficer Grzymościa. Niebezpieczny w walce, z dużym poczuciem humoru. Polował na Adelę i Krystiana; skończył trafiony przez Alana z Koeniga.


### Migświatło psychotroniczek

* **uid:** 190828-migswiatlo-psychotroniczek, _numer względny_: 3
* **daty:** 0110-02-07 - 0110-02-09
* **obecni:** Artur Michasiewicz, Ernest Kajrat, Marek Puszczok, Minerwa Metalia, Pięknotka Diakon, Talia Aegis, Tymon Grubosz

Streszczenie:

Gdy Kajrat jest w więzieniu, nikt nie kontroluje przepływu technologii noktiańskiej. Talia ostrzegła Pięknotkę, że do holo-walki AI dodała subturingowe BIA klasy Szponowiec, z żądania gościa od Grzymościa. Okazało się, że to niegroźny hazard jest - ale są tam dwie dziwne walczące AI: BIA oraz militarna, stworzona przez Minerwę. Pięknotka chciała tylko zabić BIA, ale przypadkowo z Minerwą wysadziły Arenę Migświatła. Tymon zabrał Minerwie jej TAI. Powiedział, że jeszcze nie czas by takie rzeczy robiła.

Aktor w Opowieści:

* Dokonanie:
    * nadal w więzieniu; okazuje się, że pełni kluczową rolę neutralizatora przepływu artefaktów noktiańskich. Dzięki niemu nie ma dziwnych anomalii. Teraz go nie ma.


### Rozpaczliwe ratowanie BII

* **uid:** 190827-rozpaczliwe-ratowanie-bii, _numer względny_: 2
* **daty:** 0110-01-17 - 0110-01-20
* **obecni:** BIA Tarn, Ernest Kajrat, Marek Puszczok, Mariusz Trzewń, Pięknotka Diakon, Sławomir Niejadek, Talia Aegis, Tymon Grubosz

Streszczenie:

Talia próbowała utrzymać przy życiu BIA 3 generacji. Nie mając surowców, użyła tej BIA do zdobycia rzeczy odżywczych z Tiamenat - zrzucając winę na Wiktora Sataraila. Pięknotka poszła za śladem i gdy dotarła do Talii, Kajrat wziął na siebie winę (za co Pięknotka go aresztowała). Następnie poprosiła Wiktora Sataraila by ten ochronił swoje dobre imię - i zniszczył śmiertelnie niebezpieczną BIA.

Aktor w Opowieści:

* Dokonanie:
    * całkowicie nieświadomy sprawy wpakował się w kiepskiej klasy intrygę Talii. Wziął winę na siebie, dał się złapać i poszedł dla niej do więzienia na pewien czas; uzyskał jej wsparcie za to.


### Glizda, która leczy

* **uid:** 211120-glizda-ktora-leczy, _numer względny_: 1
* **daty:** 0108-04-03 - 0108-04-14
* **obecni:** Amanda Kajrat, Amelia Sowińska, Ernest Kajrat, Feliks Keksik, Justynian Diakon, Kacper Bankierz, Karol Pustak, Oliwia Lemurczak, Roland Sowiński, Sabina Kazitan, Sensacjusz Diakon, Stella Sowińska

Streszczenie:

Sensacjusz Diakon został wysłany na prowincję pod przykrywką Lekarza Rekinów, by dojść ze Stellą do tego czemu Amelia i Roland Sowińscy zachowują się nietypowo. Na miejscu okazało się, że Roland jest w stanie ostrej wojny z mafią (która nie traktuje tego poważnie; Kajrat się bawi z Rekinami) a Amelia po prostu nie w pełni sobie radzi i próbuje opanować sytuację. Roland jest Skażony Esuriit; Sensacjusz przetransportował go glizdą do Aurum i sam, uwięziony w bojowej formie, deeskalował konflikt z mafią. A Amelia jest tymczasowym dowódcą Rekinów...

Aktor w Opowieści:

* Dokonanie:
    * ojej, Rekinki zdecydowały się na pomoc lokalną. To słodkie i fajne. Ale wchodzą w szkodę Grzymościowi? Ok - czas na darmowy trening. Maksymalizacja upokorzenia.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 13, @: 0110-07-19
    1. Primus    : 13, @: 0110-07-19
        1. Sektor Astoriański    : 13, @: 0110-07-19
            1. Astoria    : 13, @: 0110-07-19
                1. Sojusz Letejski, SW    : 2, @: 0110-05-27
                    1. Granica Anomalii    : 2, @: 0110-05-27
                        1. Las Pusty, okolice    : 1, @: 0110-05-27
                        1. Wieczna Maszyna, okolice    : 1, @: 0110-05-27
                        1. Wolne Ptaki    : 1, @: 0110-05-17
                            1. Królewska Baza    : 1, @: 0110-05-17
                1. Sojusz Letejski    : 12, @: 0110-07-19
                    1. Szczeliniec    : 12, @: 0110-07-19
                        1. Powiat Pustogorski    : 12, @: 0110-07-19
                            1. Czółenko    : 2, @: 0110-06-05
                                1. Bunkry    : 1, @: 0110-05-21
                            1. Podwiert, okolice    : 1, @: 0110-06-24
                                1. Bioskładowisko podziemne    : 1, @: 0110-06-24
                            1. Podwiert    : 5, @: 0110-06-05
                                1. Bastion Pustogoru    : 1, @: 0110-04-20
                                1. Dolina Biurowa    : 1, @: 0110-05-11
                                1. Dzielnica Luksusu Rekinów    : 1, @: 0108-04-14
                                    1. Serce Luksusu    : 1, @: 0108-04-14
                                        1. Lecznica Rannej Rybki    : 1, @: 0108-04-14
                                1. Iglice Nadziei    : 1, @: 0110-06-03
                                    1. Posiadłość Arieników    : 1, @: 0110-06-03
                                1. Las Trzęsawny    : 1, @: 0108-04-14
                                1. Odlewnia    : 1, @: 0110-04-20
                                1. Osiedle Leszczynowe    : 2, @: 0110-06-05
                                    1. Szkoła Nowa    : 2, @: 0110-06-05
                                1. Sensoplex    : 1, @: 0110-04-20
                            1. Pustogor, okolice    : 1, @: 0110-07-19
                                1. Rezydencja Blakenbauerów    : 1, @: 0110-07-19
                            1. Zaczęstwo    : 6, @: 0110-07-19
                                1. Akademia Magii, kampus    : 1, @: 0110-04-22
                                    1. Budynek Centralny    : 1, @: 0110-04-22
                                        1. Skrzydło Loris    : 1, @: 0110-04-22
                                1. Arena Migświatła    : 1, @: 0110-02-09
                                1. Kawiarenka Leopold    : 1, @: 0110-05-17
                                1. Kompleks Tiamenat    : 1, @: 0110-01-20
                                1. Nieużytki Staszka    : 2, @: 0110-07-19
                        1. Trzęsawisko Zjawosztup    : 1, @: 0110-04-20

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Pięknotka Diakon     | 10 | ((190505-szczur-ktory-chroni; 190519-uciekajacy-seksbot; 190616-anomalna-serafina; 190622-wojna-kajrata; 190623-noc-kajrata; 190709-somnibel-uciekl-arienikom; 190714-kult-choroba-esuriit; 190726-bardzo-niebezpieczne-skladowisko; 190827-rozpaczliwe-ratowanie-bii; 190828-migswiatlo-psychotroniczek)) |
| Amanda Kajrat        | 4 | ((190714-kult-choroba-esuriit; 190726-bardzo-niebezpieczne-skladowisko; 200311-wygrany-kontrakt; 211120-glizda-ktora-leczy)) |
| Liliana Bankierz     | 4 | ((190519-uciekajacy-seksbot; 190622-wojna-kajrata; 190623-noc-kajrata; 200311-wygrany-kontrakt)) |
| Tomasz Tukan         | 4 | ((190519-uciekajacy-seksbot; 190616-anomalna-serafina; 190709-somnibel-uciekl-arienikom; 190714-kult-choroba-esuriit)) |
| Serafina Ira         | 3 | ((190616-anomalna-serafina; 190622-wojna-kajrata; 190623-noc-kajrata)) |
| Krystian Namałłek    | 2 | ((190505-szczur-ktory-chroni; 190616-anomalna-serafina)) |
| Lucjusz Blakenbauer  | 2 | ((190505-szczur-ktory-chroni; 200311-wygrany-kontrakt)) |
| Marek Puszczok       | 2 | ((190827-rozpaczliwe-ratowanie-bii; 190828-migswiatlo-psychotroniczek)) |
| Minerwa Metalia      | 2 | ((190828-migswiatlo-psychotroniczek; 200311-wygrany-kontrakt)) |
| Nikola Kirys         | 2 | ((190622-wojna-kajrata; 190626-upadek-enklawy-floris)) |
| Ossidia Saitis       | 2 | ((190519-uciekajacy-seksbot; 190623-noc-kajrata)) |
| Talia Aegis          | 2 | ((190827-rozpaczliwe-ratowanie-bii; 190828-migswiatlo-psychotroniczek)) |
| Tymon Grubosz        | 2 | ((190827-rozpaczliwe-ratowanie-bii; 190828-migswiatlo-psychotroniczek)) |
| Adela Kirys          | 1 | ((190505-szczur-ktory-chroni)) |
| Alan Bartozol        | 1 | ((190505-szczur-ktory-chroni)) |
| Amelia Sowińska      | 1 | ((211120-glizda-ktora-leczy)) |
| Antoni Żuwaczka      | 1 | ((190616-anomalna-serafina)) |
| Ariela Sirmin        | 1 | ((190626-upadek-enklawy-floris)) |
| Arnulf Poważny       | 1 | ((190519-uciekajacy-seksbot)) |
| Artur Michasiewicz   | 1 | ((190828-migswiatlo-psychotroniczek)) |
| BIA Tarn             | 1 | ((190827-rozpaczliwe-ratowanie-bii)) |
| Cezary Alentik       | 1 | ((190726-bardzo-niebezpieczne-skladowisko)) |
| Dagmara Doberman     | 1 | ((200311-wygrany-kontrakt)) |
| Eliza Ira            | 1 | ((190519-uciekajacy-seksbot)) |
| Feliks Keksik        | 1 | ((211120-glizda-ktora-leczy)) |
| Gabriel Ursus        | 1 | ((190726-bardzo-niebezpieczne-skladowisko)) |
| Hubert Kraborów      | 1 | ((190626-upadek-enklawy-floris)) |
| Jan Revlen           | 1 | ((190709-somnibel-uciekl-arienikom)) |
| Jolanta Teresis      | 1 | ((190626-upadek-enklawy-floris)) |
| Justynian Diakon     | 1 | ((211120-glizda-ktora-leczy)) |
| Kacper Bankierz      | 1 | ((211120-glizda-ktora-leczy)) |
| Karol Pustak         | 1 | ((211120-glizda-ktora-leczy)) |
| Kasjan Czerwoczłek   | 1 | ((190505-szczur-ktory-chroni)) |
| Konrad Czukajczek    | 1 | ((190626-upadek-enklawy-floris)) |
| Ksawery Wojnicki     | 1 | ((190709-somnibel-uciekl-arienikom)) |
| Marcel Sowiński      | 1 | ((190626-upadek-enklawy-floris)) |
| Mariusz Trzewń       | 1 | ((190827-rozpaczliwe-ratowanie-bii)) |
| Mirela Orion         | 1 | ((190726-bardzo-niebezpieczne-skladowisko)) |
| Olaf Zuchwały        | 1 | ((190622-wojna-kajrata)) |
| Oliwia Lemurczak     | 1 | ((211120-glizda-ktora-leczy)) |
| Oliwia Namałłek      | 1 | ((190505-szczur-ktory-chroni)) |
| Roland Grzymość      | 1 | ((190726-bardzo-niebezpieczne-skladowisko)) |
| Roland Sowiński      | 1 | ((211120-glizda-ktora-leczy)) |
| Roman Rymtusz        | 1 | ((190626-upadek-enklawy-floris)) |
| Ronald Grzymość      | 1 | ((190616-anomalna-serafina)) |
| Sabina Kazitan       | 1 | ((211120-glizda-ktora-leczy)) |
| Saitaer              | 1 | ((190519-uciekajacy-seksbot)) |
| Sensacjusz Diakon    | 1 | ((211120-glizda-ktora-leczy)) |
| Sławomir Niejadek    | 1 | ((190827-rozpaczliwe-ratowanie-bii)) |
| Staś Arienik         | 1 | ((190709-somnibel-uciekl-arienikom)) |
| Stella Sowińska      | 1 | ((211120-glizda-ktora-leczy)) |
| Szymon Maszczor      | 1 | ((190626-upadek-enklawy-floris)) |
| Teresa Mieralit      | 1 | ((190519-uciekajacy-seksbot)) |
| Urszula Arienik      | 1 | ((190709-somnibel-uciekl-arienikom)) |
| Wargun Ira           | 1 | ((190626-upadek-enklawy-floris)) |
| Wiktor Satarail      | 1 | ((190505-szczur-ktory-chroni)) |
| Ziemowit Zięba       | 1 | ((200311-wygrany-kontrakt)) |
# Ataienne
## Identyfikator

Id: 9999-ataienne

## Sekcja Opowieści

### Sabotaż świni

* **uid:** 201104-sabotaz-swini, _numer względny_: 14
* **daty:** 0111-01-13 - 0111-01-16
* **obecni:** Anastazja Sowińska, Arianna Verlen, Ataienne, Diana Arłacz, Eustachy Korkoran, Klaudia Stryk, Martyn Hiwasser, Rafał Armadion, Robert Garwen

Streszczenie:

Sabotażysta - miragent - podjął próbę zniszczenia Arianny gdy ta ładowała świnie na Wesołego Wieprzka używając wybuchowej, anomalnej świni. Zespół przetrwał, choć "Wieprzek" został zniszczony. Arianna pojmała miragenta i przekazała go do Kontrolera Pierwszego, acz kosztem ofiar w ludziach. No i nie było dość świń by je wziąć na orbitę XD.

Aktor w Opowieści:

* Dokonanie:
    * zestrzeliła działkami Trzeciego Raju anomalnego Wieprzka, po czym użyła pełni mindwarpa by wyczyścić ofiary anomalnego Wieprzka. I nikt nic nie wie.


### Noktianie rodu Arłacz

* **uid:** 201021-noktianie-rodu-arlacz, _numer względny_: 13
* **daty:** 0111-01-07 - 0111-01-10
* **obecni:** Anastazja Sowińska, Arianna Verlen, Ataienne, Diana Arłacz, Eliza Ira, Eustachy Korkoran, Izabela Zarantel, Jolanta Arłacz, Juliusz Sowiński, Klaudia Stryk, Klaus Rumak, Maria Gołąb, OO Szalony Rumak, OO Wesoły Wieprzek, Robert Arłacz, Wanessa Pyszcz

Streszczenie:

By uzyskać współpracę Elizy Iry, Arianna zdecydowała się "porwać" noktian rodu Arłacz wykorzystywanych do produkcji czystej krwi dla magów. Z uwagi na działania Marii Gołąb plan został odrzucony i wykryty, ale znalezienie haków na Arłaczy rozwiązało ten problem - Arianna i Arłacze współpracują. Nie doszło do porozumienia z Elizą Irą, ale Eliza się wycofała z Trzeciego Raju, ogłaszając się protektorką tego terenu. Sytuacja w miarę ustabilizowana.

Aktor w Opowieści:

* Dokonanie:
    * bardzo chce się połączyć psychotronicznie z Anastazją. Nie ma okazji - Arianna jej nie pozwala. Za to ma nowych 100 noktian do zmanipulowania i Raj do trzymania.


### Krystaliczny gniew Elizy

* **uid:** 201014-krystaliczny-gniew-elizy, _numer względny_: 12
* **daty:** 0111-01-02 - 0111-01-05
* **obecni:** Aleksandra Termia, Anastazja Sowińska, Arianna Verlen, Ataienne, Dariusz Krantak, Elena Verlen, Eliza Ira, Eustachy Korkoran, Klaudia Stryk, Marian Fartel, OO Wesoły Wieprzek

Streszczenie:

Pomoc humanitarna przybyła do Trzeciego Raju wraz z zabójcami Anastazji. Mindwarpowani komandosi noktiańscy prawie zabili Elenę, ale Anastazja została ochroniona. Do akcji weszła Eliza Ira - zażądała oddania jej Trzeciego Raju i noktian. Zespół przygotowuje się do nie-oddania Raju i uratowania jak najwięcej noktian z Aurum (jakkolwiek to nie brzmi).

Aktor w Opowieści:

* Dokonanie:
    * widząc jak Dariusz potraktował Anastazję Sowińską (jej krewną), użyła swej mocy na życzenie Arianny by go zmusić do przyznania się.


### Magiczna burza w Raju

* **uid:** 200923-magiczna-burza-w-raju, _numer względny_: 11
* **daty:** 0110-12-24 - 0110-12-28
* **obecni:** Anastazja Sowińska, Arianna Verlen, Dariusz Krantak, Elena Verlen, Eliza Ira, Eustachy Korkoran, Izabela Zarantel, Klaudia Stryk, Marian Fartel, Marianna Lemurczak, Nikodem Sowiński

Streszczenie:

Zbliża się burza magiczna. Nie ma schronienia ani zapasów. Zespół wysłał SOS na Kontroler i do Nikodema Sowińskiego (Anastazja). Zbudowali co się da by odeprzeć burzę magiczną i im się to udało bez strat w ludziach. O dziwo, siła która im pomogła to Eliza Ira (acz dyskretnie). Anastazja wierzy że Nikodem jest jej najlepszym przyjacielem, ale on pragnie jej zguby...

Aktor w Opowieści:

* Dokonanie:
    * AKTOR NIEOBECNY NA SESJI
* Progresja:
    * niechęć wobec Arianny Verlen. Ataienne jest przeciwna bogom i istotom bogom podobnym a Arianna jest arcymagiem używającym eterniańskich metod.


### Śmierć Raju

* **uid:** 200916-smierc-raju, _numer względny_: 10
* **daty:** 0110-12-21 - 0110-12-23
* **obecni:** Anastazja Sowińska, Arianna Verlen, Ataienne, Celina Szilat, Elena Verlen, Eustachy Korkoran, Izabela Zarantel, Klaudia Stryk, Marian Fartel, Rafał Armadion, Robert Garwen, Wanessa Pyszcz

Streszczenie:

Najważniejszym problemem Raju jest pozbycie się bombardowania nojrepów. Arianna zaproponowała zuchwały pomysł - w kontrolowany sposób niech nojrepy wejdą do miasteczka i wezmą czego szukają. Udało jej się wszystkich przekonać. Sukces - nojrepy się wycofały, ale Tucznik nie jest zdolny do latania a Trzeci Raj jest zniszczony. Została tylko Ataienne i ruina. Ataienne rozpaliła hipnotycznie morale trzeciorajowców - przetrwają to wszystko.

Aktor w Opowieści:

* Dokonanie:
    * w Trzecim Raju działa w ukryciu, nie pokazując co naprawdę umie i stanowi źródło rozczarowań. Ale jak Raj został zniszczony, użyła pełnej mocy hipnotycznej. Nikt jej nie jest w stanie kontrolować, acz Arianna i Klaudia widzą, co potrafi.


### Rozbrojenie bomby w Kalbarku

* **uid:** 200222-rozbrojenie-bomby-w-kalbarku, _numer względny_: 9
* **daty:** 0110-07-23 - 0110-07-28
* **obecni:** Aleksandra Szklarska, Ataienne, Bartłomiej Małczarek, Diana Tevalier, Lucjusz Blakenbauer, Mateusz Kardamacz, Pięknotka Diakon, Tymon Grubosz

Streszczenie:

Kalbark okazał się jedną wielką bombą. Liberatis, silnie uzbrojeni. Kardamacz, kontroluje całe miasto. Terminusi, skażeni kralotycznie. I Chevaleresse na którą wszystko spadnie jeśli przyjdzie wsparcie. Pięknotka rozmontowała tą bombę, ściągnęła Ataienne (oficjalnie) i Zespół dał radę rozproszyć Liberatis, wycofać Chevaleresse i odzyskać kontrolę nad Barbakanem. Pięknotka zebrała wsparcie i na polu bitwy został na serio tylko Kardamacz.

Aktor w Opowieści:

* Dokonanie:
    * autoryzowana przez Pięknotkę do pomocy w Barbakanie, pokazuje bezwzględność wobec Małmałaza i Kardamacza. Rozmontowała Liberatis i odzyskała broń.


### Krucjata Chevaleresse

* **uid:** 200202-krucjata-chevaleresse, _numer względny_: 8
* **daty:** 0110-07-20 - 0110-07-23
* **obecni:** Ataienne, Damian Orion, Diana Tevalier, Keraina d'Orion, Malictrix d'Itaran, Mariusz Trzewń, Mateusz Kardamacz, Minerwa Metalia, Pięknotka Diakon, Talia Aegis, Tomasz Tukan

Streszczenie:

Chevaleresse wykradła broń Alana i uzbroiła Liberitias, gdzie działa też Ataienne przeciw Kardamaczowi, Grzymościowi i krzywdzicielowi Alana. Pięknotka doszła do serca problemu i materii, acz potrzebowała wsparcia Orbitera - dokładniej, Kerainy. Dzięki Kerainie wprowadziła Malictrix do Kompleksu Itaran. Teraz to tylko kwestia czasu aż Grzymość zostanie zniszczony.

Aktor w Opowieści:

* Dokonanie:
    * TAI powyżej 3 stopnia. Działa w tle - taktyk oraz commander, ale niekoniecznie mistrzyni zdobywania informacji. Jedyna TAI 3.5 w okolicy Szczelińca. Sformowała 'Liberitas'


### Korupcja z arystokratki

* **uid:** 191112-korupcja-z-arystokratki, _numer względny_: 7
* **daty:** 0110-06-30 - 0110-07-01
* **obecni:** Ataienne, Ignacy Myrczek, Mariusz Trzewń, Minerwa Metalia, Persefona d'Jastrząbiec, Pięknotka Diakon, Sabina Kazitan, Tadeusz Sklerzec

Streszczenie:

Po wydarzeniach z Grzymościem oczy Pięknotki padły na Jastrząbiec - tam dzieją się dziwne rzeczy z narkotykami. Okazało się, że to Ignacy Myrczek próbuje rozpaczliwie udowodnić arystokratce z Aurum, że nie zrobił jej krzywdy świadomie a pod wpływem narkotyków. Pięknotka wpadła (z pomocą Ataienne) i zrobiła porządek - ukarała burmistrza, Myrczka, arystokratkę i w sumie wszystkich. Ma smak popiołu w ustach, tu nie było nikogo sensownego...

Aktor w Opowieści:

* Dokonanie:
    * nieprawdopodobnie potężna z perspektywy opętywania maszyn i przejmowania kontroli. Sojuszniczka Pięknotki, która przełamała wszystkie zabezpieczenia nawet się nie starając...


### Zaginiona soniczka

* **uid:** 191105-zaginiona-soniczka, _numer względny_: 6
* **daty:** 0110-06-28 - 0110-06-29
* **obecni:** Ataienne, Erwin Galilien, Mariola Tralment, Mariusz Trzewń, Mateusz Kardamacz, Pięknotka Diakon, Tymon Grubosz

Streszczenie:

Ataienne poprosiła Pięknotkę o znalezienie zaginionej fanki. Pięknotka zaczęła śledztwo i doszła do tego, że Mariola została porwana przez gang przerzucający dziewczyny do Cieniaszczytu. Erwin słyszał takie plotki. Pięknotka zlokalizowała i usunęła komórkę tego gangu; śledztwo pokazało na autoklub fitness jako jedno interesujące miejsce. Najpewniej nie to jest miejscem "zła", ale warto sprawdzić.

Aktor w Opowieści:

* Dokonanie:
    * przestraszyła się o Mariolę (swoją soniczkę która zaginęła) i udała się z prośbą o pomoc do Pięknotki. Przypadkowo, Ataienne wykryła gang przerzutowy dziewczyn do Cieniaszczytu.


### Perfekcyjna córka

* **uid:** 200219-perfekcyjna-corka, _numer względny_: 5
* **daty:** 0110-05-12 - 0110-05-15
* **obecni:** Aleksandra Szklarska, Ataienne, Lena Kardamacz, Leszek Szklarski, Mateusz Kardamacz, Paweł Oszmorn, Sabina Kazitan

Streszczenie:

72 dni przed wejściem Pięknotki do Kalbarka, sformowana niedawno grupa Liberatis próbująca rozwiązać problem straszliwej kontroli memetycznej na Astorii wpadła w kłopoty. Jedna z członkiń Liberatis, Lena, była bioformą stworzoną przez Mateusza - i Mateusz chciał ją z powrotem. W odpowiedzi na to, że Mateusz porywa anarchistów Liberatis ściągnęli do Kalbarku Ataienne. Współpracując z nią, Leszek (lider Liberatis) wymanewrował Mateusza i wraz z Leną i wsparciem Sabiny pokonali Mateusza, uwalniając przy okazji innych anarchistów. Aha, powstało też stado wolnych samochodów...

Aktor w Opowieści:

* Dokonanie:
    * dzięki współpracy Leszka i Sabiny przejęła kontrolę nad Kalbarkiem i się tam zaszczepiła. Zapewnia absolutną widoczność oraz neutralizuje Paradoksy przekierowując emitery.


### Arcymag w Raju

* **uid:** 190326-arcymag-w-raju, _numer względny_: 4
* **daty:** 0110-04-07 - 0110-04-08
* **obecni:** Ataienne, Dawid Szardak, Eliza Ira, Fergus Salien, Grzegorz Kamczarnik, Olga Leszcz

Streszczenie:

Eliza Ira zdecydowała się odzyskać swoich ludzi z czasów wojny przetrzymywanych w Trzecim Raju. Stworzyła Krystaliczną Plagę unieszkodliwiającą wpływ mindwarpującej AI, Ataienne. Jednak Fergus i Olga dali radę przekonać Elizę, że wojna się skończyła i ich wspólnym przeciwnikiem jest teraz Astoria. Arcymagini weszła w ostrożny sojusz z Orbiterem Pierwszym, odzyskując kilkanaście osób chcących z nią współpracować.

Aktor w Opowieści:

* Dokonanie:
    * mindwarpująca TAI o funkcji nadpisywania wspomnień; hipnopiosenkarka holograficzna. Kontroluje Trzeci Raj składający się z jeńców wojennych.


### Eksperymentalny power suit

* **uid:** 190402-eksperymentalny-power-suit, _numer względny_: 3
* **daty:** 0110-03-11 - 0110-03-13
* **obecni:** Ataienne, Erwin Galilien, Kaja Selerek, Minerwa Metalia, Pięknotka Diakon

Streszczenie:

Orbiter chciał przekazać Pięknotce eksperymentalny Power Suit którego nikt nie potrafi kontrolować. Pięknotka spróbowała się z nim zintegrować; ona, Minerwa, Erwin i jeszcze dwóch magów Orbitera skończyło w szpitalu. Pięknotka się uśmiechnęła. Chce ten power suit. Opanuje go - mimo, że jest Skażony energią ixiońską i ma uszkodzoną psychotronikę pełną nienawiści. Życie Pięknotki uratowała Ataienne, ale skończyła ciężko ranna, z uszkodzoną matrycą.

Aktor w Opowieści:

* Dokonanie:
    * wniknęła do Cienia i uratowała Pięknotkę przed nienawistnym instynktem psychotroniki. Skończyła bardzo ciężko ranna, z rozpadającą się matrycą.
* Progresja:
    * ciężko ranna, z uszkodzoną matrycą. Uratowała życie Pięknotce, ale zapłaciła rozpad matrycy; ma trochę czasu regeneracji.


### Kurz po Ataienne

* **uid:** 190331-kurz-po-ataienne, _numer względny_: 2
* **daty:** 0110-03-06 - 0110-03-08
* **obecni:** Alan Bartozol, Ataienne, Diana Tevalier, Pięknotka Diakon, Romeo Węglas, Szymon Oporcznik

Streszczenie:

Alan poprosił Pięknotkę o interwencję w sprawie tego, że uziemił Chevaleresse; Pięknotka opanowała chcącą opuścić Alana Chevaleresse. Potem Pięknotka nawiązała użyteczny kontakt w Szymonie z Orbitera, znalazła osoby odpowiedzialne za próbę zabicia Ataienne i porozmawiała z samą Ataienne; uznała, że ta konkretna AI jest zdecydowanie dziwna.

Aktor w Opowieści:

* Dokonanie:
    * uważa Chevaleresse za przyjaciółkę i dlatego nie udało się Pięknotce jej przekonać do użycia swoich mocy by nieco ustabilizować Chevaleresse. Wdzięczna Pięknotce.


### Polowanie na Ataienne

* **uid:** 190330-polowanie-na-ataienne, _numer względny_: 1
* **daty:** 0110-03-02 - 0110-03-03
* **obecni:** Ataienne, Diana Tevalier, Pięknotka Diakon

Streszczenie:

Ataienne chciała zrobić koncert; Orbiter inkarnował ją w Zaczęstwie. Podczas koncertu Ataienne została zaatakowana i w jej obronie stanęła Chevaleresse, która aportowała broń Alana... Pięknotka wpierw zatrzymała tą wojnę, potem uratowała Ataienne od technovora a na końcu wydobyła siebie i Ataienne z rąk najemników którzy chcieli zniszczyć TAI. Trudny dzień; wszystko wskazuje na to, że za polowaniem na Ataienne stoi Eliza Ira.

Aktor w Opowieści:

* Dokonanie:
    * chciała tylko zrobić koncert - a cudem uniknęła śmierci dzięki ratunkowi Chevaleresse a potem Pięknotki. Miała hosta w formie człowieka ku niezadowoleniu Pięknotki.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 13, @: 0111-01-16
    1. Multivirt    : 2, @: 0110-03-13
        1. MMO    : 2, @: 0110-03-13
            1. Kryształowy Pałac    : 2, @: 0110-03-13
    1. Primus    : 13, @: 0111-01-16
        1. Sektor Astoriański    : 13, @: 0111-01-16
            1. Astoria, Orbita    : 1, @: 0110-12-23
                1. Kontroler Pierwszy    : 1, @: 0110-12-23
            1. Astoria    : 13, @: 0111-01-16
                1. Sojusz Letejski, NW    : 5, @: 0111-01-16
                    1. Ruiniec    : 5, @: 0111-01-16
                        1. Kryształowa Forteca    : 1, @: 0110-04-08
                            1. Mauzoleum    : 1, @: 0110-04-08
                            1. Rajskie Peryferia    : 1, @: 0110-04-08
                        1. Pustynia Kryształowa    : 1, @: 0111-01-10
                        1. Trzeci Raj, okolice    : 1, @: 0110-12-23
                            1. Kopiec Nojrepów    : 1, @: 0110-12-23
                            1. Zdrowa Ziemia    : 1, @: 0110-12-23
                        1. Trzeci Raj    : 5, @: 0111-01-16
                            1. Barbakan    : 1, @: 0110-12-23
                            1. Centrala Ataienne    : 2, @: 0110-12-23
                            1. Mały Kosmoport    : 1, @: 0110-12-23
                            1. Ratusz    : 1, @: 0110-12-23
                            1. Stacja Nadawcza    : 1, @: 0110-12-23
                1. Sojusz Letejski    : 9, @: 0111-01-10
                    1. Aurum    : 1, @: 0111-01-10
                        1. Powiat Niskowzgórza    : 1, @: 0111-01-10
                            1. Domena Arłacz    : 1, @: 0111-01-10
                                1. Posiadłość Arłacz    : 1, @: 0111-01-10
                                    1. Baterie Puryfikacji Krwi    : 1, @: 0111-01-10
                                    1. Małe lądowisko    : 1, @: 0111-01-10
                    1. Szczeliniec    : 8, @: 0110-07-28
                        1. Powiat Jastrzębski    : 5, @: 0110-07-28
                            1. Jastrząbiec, okolice    : 1, @: 0110-07-23
                                1. Klinika Iglica    : 1, @: 0110-07-23
                                    1. Kompleks Itaran    : 1, @: 0110-07-23
                                1. TechBunkier Sarrat    : 1, @: 0110-07-23
                            1. Jastrząbiec    : 1, @: 0110-07-01
                                1. Hotel Stacja Kosmiczna    : 1, @: 0110-07-01
                                1. Ratusz    : 1, @: 0110-07-01
                            1. Kalbark, Nierzeczywistość    : 1, @: 0110-05-15
                                1. Utopia    : 1, @: 0110-05-15
                            1. Kalbark    : 4, @: 0110-07-28
                                1. Autoklub Piękna    : 3, @: 0110-07-28
                                1. Escape Room Lustereczko    : 2, @: 0110-07-23
                                1. Mini Barbakan    : 2, @: 0110-07-28
                                1. Rzeźnia    : 1, @: 0110-05-15
                        1. Powiat Pustogorski    : 5, @: 0110-07-23
                            1. Czółenko    : 1, @: 0110-06-29
                                1. Bunkry    : 1, @: 0110-06-29
                            1. Podwiert    : 3, @: 0110-03-13
                                1. Kosmoport    : 2, @: 0110-03-13
                                1. Osiedle Leszczynowe    : 1, @: 0110-03-03
                            1. Pustogor    : 3, @: 0110-06-29
                                1. Eksterior    : 1, @: 0110-06-29
                                    1. Miasteczko    : 1, @: 0110-06-29
                                1. Miasteczko    : 2, @: 0110-03-13
                            1. Zaczęstwo    : 3, @: 0110-07-23
                                1. Nieużytki Staszka    : 2, @: 0110-06-29
                                1. Osiedle Ptasie    : 1, @: 0110-06-29
                                1. Złomowisko    : 1, @: 0110-03-03

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Pięknotka Diakon     | 7 | ((190330-polowanie-na-ataienne; 190331-kurz-po-ataienne; 190402-eksperymentalny-power-suit; 191105-zaginiona-soniczka; 191112-korupcja-z-arystokratki; 200202-krucjata-chevaleresse; 200222-rozbrojenie-bomby-w-kalbarku)) |
| Anastazja Sowińska   | 4 | ((200916-smierc-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz; 201104-sabotaz-swini)) |
| Arianna Verlen       | 4 | ((200916-smierc-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz; 201104-sabotaz-swini)) |
| Diana Tevalier       | 4 | ((190330-polowanie-na-ataienne; 190331-kurz-po-ataienne; 200202-krucjata-chevaleresse; 200222-rozbrojenie-bomby-w-kalbarku)) |
| Eustachy Korkoran    | 4 | ((200916-smierc-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz; 201104-sabotaz-swini)) |
| Klaudia Stryk        | 4 | ((200916-smierc-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz; 201104-sabotaz-swini)) |
| Mateusz Kardamacz    | 4 | ((191105-zaginiona-soniczka; 200202-krucjata-chevaleresse; 200219-perfekcyjna-corka; 200222-rozbrojenie-bomby-w-kalbarku)) |
| Eliza Ira            | 3 | ((190326-arcymag-w-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz)) |
| Mariusz Trzewń       | 3 | ((191105-zaginiona-soniczka; 191112-korupcja-z-arystokratki; 200202-krucjata-chevaleresse)) |
| Minerwa Metalia      | 3 | ((190402-eksperymentalny-power-suit; 191112-korupcja-z-arystokratki; 200202-krucjata-chevaleresse)) |
| Aleksandra Szklarska | 2 | ((200219-perfekcyjna-corka; 200222-rozbrojenie-bomby-w-kalbarku)) |
| Diana Arłacz         | 2 | ((201021-noktianie-rodu-arlacz; 201104-sabotaz-swini)) |
| Elena Verlen         | 2 | ((200916-smierc-raju; 201014-krystaliczny-gniew-elizy)) |
| Erwin Galilien       | 2 | ((190402-eksperymentalny-power-suit; 191105-zaginiona-soniczka)) |
| Izabela Zarantel     | 2 | ((200916-smierc-raju; 201021-noktianie-rodu-arlacz)) |
| Marian Fartel        | 2 | ((200916-smierc-raju; 201014-krystaliczny-gniew-elizy)) |
| OO Wesoły Wieprzek   | 2 | ((201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz)) |
| Rafał Armadion       | 2 | ((200916-smierc-raju; 201104-sabotaz-swini)) |
| Robert Garwen        | 2 | ((200916-smierc-raju; 201104-sabotaz-swini)) |
| Sabina Kazitan       | 2 | ((191112-korupcja-z-arystokratki; 200219-perfekcyjna-corka)) |
| Tymon Grubosz        | 2 | ((191105-zaginiona-soniczka; 200222-rozbrojenie-bomby-w-kalbarku)) |
| Wanessa Pyszcz       | 2 | ((200916-smierc-raju; 201021-noktianie-rodu-arlacz)) |
| Alan Bartozol        | 1 | ((190331-kurz-po-ataienne)) |
| Aleksandra Termia    | 1 | ((201014-krystaliczny-gniew-elizy)) |
| Bartłomiej Małczarek | 1 | ((200222-rozbrojenie-bomby-w-kalbarku)) |
| Celina Szilat        | 1 | ((200916-smierc-raju)) |
| Damian Orion         | 1 | ((200202-krucjata-chevaleresse)) |
| Dariusz Krantak      | 1 | ((201014-krystaliczny-gniew-elizy)) |
| Dawid Szardak        | 1 | ((190326-arcymag-w-raju)) |
| Fergus Salien        | 1 | ((190326-arcymag-w-raju)) |
| Grzegorz Kamczarnik  | 1 | ((190326-arcymag-w-raju)) |
| Ignacy Myrczek       | 1 | ((191112-korupcja-z-arystokratki)) |
| Jolanta Arłacz       | 1 | ((201021-noktianie-rodu-arlacz)) |
| Juliusz Sowiński     | 1 | ((201021-noktianie-rodu-arlacz)) |
| Kaja Selerek         | 1 | ((190402-eksperymentalny-power-suit)) |
| Keraina d'Orion      | 1 | ((200202-krucjata-chevaleresse)) |
| Klaus Rumak          | 1 | ((201021-noktianie-rodu-arlacz)) |
| Lena Kardamacz       | 1 | ((200219-perfekcyjna-corka)) |
| Leszek Szklarski     | 1 | ((200219-perfekcyjna-corka)) |
| Lucjusz Blakenbauer  | 1 | ((200222-rozbrojenie-bomby-w-kalbarku)) |
| Malictrix d'Itaran   | 1 | ((200202-krucjata-chevaleresse)) |
| Maria Gołąb          | 1 | ((201021-noktianie-rodu-arlacz)) |
| Mariola Tralment     | 1 | ((191105-zaginiona-soniczka)) |
| Martyn Hiwasser      | 1 | ((201104-sabotaz-swini)) |
| Olga Leszcz          | 1 | ((190326-arcymag-w-raju)) |
| OO Szalony Rumak     | 1 | ((201021-noktianie-rodu-arlacz)) |
| Paweł Oszmorn        | 1 | ((200219-perfekcyjna-corka)) |
| Persefona d'Jastrząbiec | 1 | ((191112-korupcja-z-arystokratki)) |
| Robert Arłacz        | 1 | ((201021-noktianie-rodu-arlacz)) |
| Romeo Węglas         | 1 | ((190331-kurz-po-ataienne)) |
| Szymon Oporcznik     | 1 | ((190331-kurz-po-ataienne)) |
| Tadeusz Sklerzec     | 1 | ((191112-korupcja-z-arystokratki)) |
| Talia Aegis          | 1 | ((200202-krucjata-chevaleresse)) |
| Tomasz Tukan         | 1 | ((200202-krucjata-chevaleresse)) |
# Alan Bartozol
## Identyfikator

Id: 1904-alan-bartozol

## Sekcja Opowieści

### Stary kot a Trzęsawisko

* **uid:** 220814-stary-kot-a-trzesawisko, _numer względny_: 19
* **daty:** 0111-10-08 - 0111-10-09
* **obecni:** Alan Bartozol, Ariela Lechot, Diana Tevalier, Iwan Zawtrak, Karolina Terienak, Kot Rozbójnik, Marian Lechot, Urszula Miłkowicz, Viirai d'Lechotka

Streszczenie:

Właściciele fortifarmy Lechotka kiedyś dali radę wyrwać ziemię Trzęsawisku. Trzęsawisko nie zapomniało. Teraz jak są starzy i sami, Trzęsawisko porwało Arielę. Terminusi nie pomogli Marianowi, więc poszedł by ją uratować i został porwany. Ich anty-anomalny kot Rozbójnik nie był w stanie im pomóc; rannego kota znalazła w rowie Karolina. Zawiozła do Majkłapca, ale kot im uciekł. Karo współpracując z Ulą opanowały Rozbójnika, dogadały się z fortifarmą i z pomocą Alana zniszczyły ludzi porwanych przez Trzęsawisko. A Rozbójnik i fortifarma wpadły do zaskoczonej Uli.

Aktor w Opowieści:

* Dokonanie:
    * wezwany do ratowania ludzi z Lechotki zorientował się, że ich już się nie uratuje. Zrobił im MIRV coup de grace. Potem wsadził Ulę do podejrzanej fortifarmy Lechotka i ją czujnie monitoruje. Dał się przekonać, by pozwolił kotu klasy vistermin żyć.


### Chevaleresse infiltruje Rekiny

* **uid:** 211221-chevaleresse-infiltruje-rekiny, _numer względny_: 18
* **daty:** 0111-09-02 - 0111-09-03
* **obecni:** Alan Bartozol, Barnaba Burgacz, Diana Tevalier, Hestia d'Rekiny, Justynian Diakon, Karolina Terienak, Marysia Sowińska, Melissa Durszenko, Rupert Mysiokornik, Santino Mysiokornik, Staś Arienik, Żorż d'Namertel

Streszczenie:

Hestia zaproponowała Marysi naprawienie Rekin Defense Grid - i od razu wykryli osoby spoza Rekinów i co najmniej jedną infiltratorkę. To Chevaleresse, która szuka Stasia Arienika by Alan nie musiał iść na Trzęsawisko. Karo i Marysia złapały Chevaleresse, w trójkę się dogadały. Karo przechwyciła Stasia (któremu pomagał Babu) i oddała go Alanowi zrzutem ze ścigacza. Justynian skierował oczy na Kult Ośmiornicy który podobno jest w Dzielnicy Rekinów.

Aktor w Opowieści:

* Dokonanie:
    * Chevaleresse zdobyła dla niego Stasia Arienika rękami Karoliny Terienak (o czym on nie wie). NADAL nie cierpi Aurum, ale tyci mniej.


### Zgubiony holokryształ w lesie

* **uid:** 210817-zgubiony-holokrysztal-w-lesie, _numer względny_: 17
* **daty:** 0111-06-18 - 0111-06-20
* **obecni:** Alan Bartozol, Cyryl Perikas, Kacper Bankierz, Laura Tesinik, Liliana Bankierz, Mariusz Trzewń, Marysia Sowińska, Rupert Mysiokornik, Stella Armadion, Tomasz Tukan

Streszczenie:

Jeden z przerzucanych przez mafię przedmiotów do Aurum to ko-matryca Kuratorów, jako broń. Ale Cyryl - kurier - był ofiarą terminuskiego polowania więc ją odrzucił w Lesie Trzęsawnym gdzie znalazła ją Stella przekonana, że to noktiańska technologia. Marysia wypchnęła Kacpra Bankierza z mafii (i tego terenu) a Tukan znalazł i usunął ko-matrycę m.in. używając Laury.

Aktor w Opowieści:

* Dokonanie:
    * przekonany przez Chevaleresse do poprowadzenia wycieczki studentów w Lesie Trzęsawnym (ku swojemu smutkowi). Potem gdy Tukan powiedział mu o ko-matrycy z przyjemnością zrobił fałszywkę z trackerem i wziął na siebie "winę" ze strony mafii.


### Haracz w parku rozrywki

* **uid:** 200913-haracz-w-parku-rozrywki, _numer względny_: 16
* **daty:** 0110-10-05 - 0110-10-06
* **obecni:** Adam Janor, Alan Bartozol, Diana Tevalier, Franciszek Owadowiec, Karol Senonik, Krystyna Senonik, Maja Janor, Mariusz Trzewń, Melinda Teilert, Pięknotka Diakon, Rafał Kamaron

Streszczenie:

Łatwa misja w okolicach parku rozrywki Janor przekształciła się w dziwną operację z pasożytem magicznym dotykającym pobierającego haracz Grzymościowca i dziwnie zachowującą się komendant policji. Pięknotka i Alan rozwalili siły Grzymościa, ale nie są pewni, czy to nie była tylko dywersja... (a tymczasem Melinda i Chevaleresse świetnie się bawiły w parku rozrywki).

Aktor w Opowieści:

* Dokonanie:
    * anihilator Fulmenem; bezwzględnie zdjął maga. Badał dronę która śledziła. Dogadał się z TAI za plecami wszystkich. Bezwzględny i bardzo skuteczny.
* Progresja:
    * nowy aspekt - obsesyjnie chroni swoje podopieczne.
    * bardzo ostry opieprz od Karli; nie do tego służy Fulmen, nie powinien używać ciężkiego działa przeciw piechocie.


### Polowanie na Pięknotkę

* **uid:** 190901-polowanie-na-pieknotke, _numer względny_: 15
* **daty:** 0110-06-17 - 0110-06-20
* **obecni:** Alan Bartozol, Diana Tevalier, Józef Małmałaz, Lucjusz Blakenbauer, Mariusz Trzewń, Minerwa Metalia, Pięknotka Diakon

Streszczenie:

Noktiański zabójca chciał pozyskać Cienia; pokonał Pięknotkę, ale Alan go zestrzelił. Potem zabójca złapał Chevaleresse i korzystając z niej jako zakładniczki wysłał Alana do szpitala. Pięknotka doszła do tego, jak się nazywa z pomocą Minerwy i Cienia.

Aktor w Opowieści:

* Dokonanie:
    * uratował Pięknotkę przed Józefem strzałem z Fulmena (700 metrów przez kilka ścian); potem by chronić Chevaleresse dał się Alanowi ciężko poranić.
* Progresja:
    * na miesiąc trafił do szpitala przez dwa sztylety Józefa Małmałaza


### Kto wrobił Alana

* **uid:** 190830-kto-wrobil-alana, _numer względny_: 14
* **daty:** 0110-05-30 - 0110-06-01
* **obecni:** Alan Bartozol, Diana Tevalier, Karolina Erenit, Pięknotka Diakon, Talia Aegis, Wojciech Zermann

Streszczenie:

Ktoś wrabia Alana. Okazało się, że to nie Alan jest celem - to Chevaleresse. Podpadła kilku magom z Aurum w vircie i stwierdzili że ją nastraszą - ale najpierw muszą odsunąć Alana na bok. Pięknotka z Tymonem zastawili pułapkę współpracując z Chevaleresse jako przynętą. Udało się - magowie Aurum wpadli. Przy okazji - Karolina pomagała magom Aurum bo uznała, że to pomoże terminusom i Chevaleresse najbardziej. Karolina bowiem nienawidzi arystokracji magów.

Aktor w Opowieści:

* Dokonanie:
    * nic nie wie, że był wrobiony. Ale potem się dowiaduje, że Chevaleresse zwinęła artefakt oraz że zdenerwowała magów Aurum. Skończyło się na gorącej kłótni.


### Szczur który chroni

* **uid:** 190505-szczur-ktory-chroni, _numer względny_: 13
* **daty:** 0110-04-18 - 0110-04-20
* **obecni:** Adela Kirys, Alan Bartozol, Ernest Kajrat, Kasjan Czerwoczłek, Krystian Namałłek, Lucjusz Blakenbauer, Oliwia Namałłek, Pięknotka Diakon, Wiktor Satarail

Streszczenie:

Zaczęło się od Skażenia proszkiem kralotycznym Nieużytków Staszka. Potem Pięknotka współpracując z mafią doszła do tego, że Adela i terminus stoją za tą sprawą. Potem Pięknotka uratowała Adelę i terminusa wprowadzając szturmowych terminusów Pustogoru na mafię. Gdy "Cień wyrwał się spod kontroli", Alan ją strzelił z działa strumieniowego. I tak skończyło się rumakowanie na tydzień - ale wszyscy są bezpieczni.

Aktor w Opowieści:

* Dokonanie:
    * wezwany jako wsparcie, odpalił dwukrotnie działo Koenig - masakrując oficera mafiozów i masakrując Pięknotkę w Cieniu. Ogólnie, masakrował.
* Progresja:
    * działo Koenig zamontowane na Fulmenie działa perfekcyjnie. Jego nowa standardowa konfiguracja. Wymaga trochę więcej chłodzenia. Warte opierdolu.
    * reputacja wybitnego strzelca i straszliwego przeciwnika jedynie się wzmocniła. Nikt nie chce się z nim pojedynkować.


### Pierwszy Emulator Orbitera

* **uid:** 190502-pierwszy-emulator-orbitera, _numer względny_: 12
* **daty:** 0110-04-10 - 0110-04-12
* **obecni:** Alan Bartozol, Bożymir Szczupak, Minerwa Metalia, Nikola Kirys, Olaf Zuchwały, Pięknotka Diakon

Streszczenie:

Idąc śladami Elizy Pięknotka natrafiła na Nikolę - o której dawni przyjaciele (Olaf) mówią, że "zginęła", coś jej zrobiono. Pięknotka doszła do tego, że Nikola jest projektem Emulator; dowiedziała się, że Emulatory były budowane m.in. przez Minerwę. Pięknotka doszła do tego, że zintegrowana z Finis Vitae Nikola została częściowo uwolniona przez wpływ Arazille na Finis Vitae. Gdy spotkała się z Nikolą - szok. Cień zareagował. Nikola odzyskała wolność po kontakcie z Cieniem, ale Pięknotka go utrzymała. Cień ma w sobie "emocje" spętanych Emulatorów. Pięknotce udało się wyperswadować Nikoli budzenie autowara lub niszczenie Astorii; zwiadowczyni jednak odjechała w ogromnej konfuzji i nie wiedząc, co teraz robić.

Aktor w Opowieści:

* Dokonanie:
    * pełnił rolę taksówki dla Pięknotki w hovertanku klasy Timor.


### Sabotaż szeptów Elizy

* **uid:** 190429-sabotaz-szeptow-elizy, _numer względny_: 11
* **daty:** 0110-04-06 - 0110-04-09
* **obecni:** Alan Bartozol, Aleksander Rugczuk, Diana Tevalier, Eliza Ira, Erwin Galilien, Karla Mrozik, Lucjusz Blakenbauer, Minerwa Metalia, Olaf Zuchwały, Pięknotka Diakon

Streszczenie:

Chevaleresse wpakowała się w "kult Elizy" grając z ludźmi online; powiedziała o tym Pięknotce. Pięknotka odkryła, że Eliza rozprzestrzenia swoje wpływy przez kryształy w formie biżuterii; wraz z Alanem, Erwinem i (nadal ranną) Minerwą zaprojektowali mechanizm uszkadzający sieć krystaliczną Elizy. Udało im się kupić jakiś miesiąc czasu, aż Orbiter i Pustogor znajdą lepsze rozwiązanie. Sama Chevaleresse ma się nie spotykać z takimi tam. Ale może z nimi grać online.

Aktor w Opowieści:

* Dokonanie:
    * dowiedział się o Cieniu, Elizie i kilku rzeczach. Ma wrażenie, że Chevaleresse go "zostawia". Dodatkowo, z Erwinem i Minerwą udowodnił czemu jest najlepszym inżynierem zniszczenia w okolicy.


### Osopokalipsa Wiktora

* **uid:** 190419-osopokalipsa-wiktora, _numer względny_: 10
* **daty:** 0110-03-25 - 0110-03-27
* **obecni:** Alan Bartozol, Kasjopea Maus, Pięknotka Diakon, Wiktor Satarail

Streszczenie:

Wiktor zdecydował się zemścić w imieniu wił. Zaprojektował osy które robiły krzywdę magom i ludziom - wpierw użył ich jako dywersję, potem zaraził jedzenie w Sensoplex. Pięknotka dekontaminując okolicę odkryła plan; przekonała go, że można bezkrwawo usunąć stąd Sensus. I to zrobiła - z pomocą Kasjopei by ta zrobiła reportaż o "Osopokalipsie", po czym sfabrykowała dowody z pomocą Wiktora. W ten sposób Sensus opuścił teren i nikt szczególnie nie ucierpiał.

Aktor w Opowieści:

* Dokonanie:
    * wsparcie Pięknotki. Jego wiedza o bagnie przydała się opanowując apokalipsę Sensoplexu.


### Korporacyjna wojna w MMO

* **uid:** 190226-korporacyjna-wojna-w-mmo, _numer względny_: 9
* **daty:** 0110-03-13 - 0110-03-16
* **obecni:** Adrian Wężoskór, Alan Bartozol, Antoni Kotomin, Dariusz Bankierz, Eliza Kotlet, Kermit Szperacz, Mi Ruda, Rafał Królewski, Wojciech Słabizna

Streszczenie:

Dwie magiczne firmy - Sensus oraz Rexpapier - weszły w konflikt o Rosę Volant. Rozmowy pokojowe miały mieć miejsce w MMO. W wyniku tego Alan Bartozol znalazł aktualnego szefa EliSquid, "Sekatora Pierwszego", którego też zabanowali. EliSquid zostało przejęte przez niejaką "Iglicę" a rozmowy pokojowe się udały - wszyscy skupią się na Dariuszu Bankierzu i małym miasteczku na terenach Skażonych by nie pojawiła się większa konkurencja do rosy volant...

Aktor w Opowieści:

* Dokonanie:
    * były lider EliSquid który zmusił Sekatora Pierwszego do współpracy. Niestety, wiele więcej nie dał rady zrobić.


### Kurz po Ataienne

* **uid:** 190331-kurz-po-ataienne, _numer względny_: 8
* **daty:** 0110-03-06 - 0110-03-08
* **obecni:** Alan Bartozol, Ataienne, Diana Tevalier, Pięknotka Diakon, Romeo Węglas, Szymon Oporcznik

Streszczenie:

Alan poprosił Pięknotkę o interwencję w sprawie tego, że uziemił Chevaleresse; Pięknotka opanowała chcącą opuścić Alana Chevaleresse. Potem Pięknotka nawiązała użyteczny kontakt w Szymonie z Orbitera, znalazła osoby odpowiedzialne za próbę zabicia Ataienne i porozmawiała z samą Ataienne; uznała, że ta konkretna AI jest zdecydowanie dziwna.

Aktor w Opowieści:

* Dokonanie:
    * uziemił Chevaleresse; nie radzi sobie jednak z małolatą i poprosił Pięknotkę by była mediatorką. Oj.


### Chevaleresse

* **uid:** 190217-chevaleresse, _numer względny_: 7
* **daty:** 0110-02-24 - 0110-02-26
* **obecni:** Alan Bartozol, Diana Tevalier, Karolina Erenit, Marlena Maja Leszczyńska, Pięknotka Diakon, Tymon Grubosz

Streszczenie:

Diana, aka Chevaleresse przybyła do Cyberszkoły szukać Alana. Postraszyła Marlenę i skończyła zaatakowana przez Tymona. Alan się zaopiekował swoją członkinią gildii; okazało się, że jego gildia (Elisquid) wpadła w złe ręce. Teraz Alan rozpaczliwie szuka sposobu na to by się odbanować; tymczasowo Chevaleresse zostaje u Alana... KTÓRY STAŁ SIĘ JEJ PRAWNYM OPIEKUNEM. Aha, Alan wie, że to wina Marleny że go zabanowali (a tak naprawdę jest Pięknotki).

Aktor w Opowieści:

* Dokonanie:
    * w grze "Voidcarver", lider EliSquid. Powadził się z Tymonem o Chevaleresse i ją ku swemu utrapieniu przygarnął. Załatwiła go i został jej opiekunem prawnym.
* Progresja:
    * zostaje prawnym opiekunem Diany Tevalier, aka Chevaleresse.
    * zimny gniew wobec Marleny Mai Leszczyńskiej, co do której myśli, że go zabanowała.
    * eskalacja z Tymonem Gruboszem; serio się nie lubią. A fakt że on skrzywdził Dianę nie pomógł.


### Skażony pnączoszpon

* **uid:** 190115-skazony-pnaczoszpon, _numer względny_: 6
* **daty:** 0110-01-06 - 0110-01-08
* **obecni:** Alan Bartozol, Alfred Bułka, Kornel Szczepanik, Kornelia Weiner

Streszczenie:

Skażony pnączoszpon próbował włamać się do domu Kornela, rezydenta Samoklęski licząc na możliwość pożywienia się energią magicznych artefaktów. Odparty, uciekł na bagno, aby zebrać siły do kolejnego ataku.

Następnej nocy pnączoszpon zebrał dość sił, by zapolować i porwać krowę. Niestety, został dostrzeżony przez wieśniaków, którzy z widłami i pochodniami poszli zapolować na drapieżnika.

Kornel chciał ich odwieść od zamiaru polowania, ale poddał się widząc determinację. Po drodze dowiedział się, że drapieżnik jest zarażony jakąś chorobą. Zdołał wyprowadzić ludzi z bagna, zanim dopadł ich pnączoszpon.

Wezwał terminusów z Pustogoru na pomoc. Tym razem miał konkrety, więc Alan Bartozol wraz ze wsparciem pojawili się i zrobili porządek.

Aktor w Opowieści:

* Dokonanie:
    * źródło podstawowej wiedzy o pnączoszponach i terminus, który rozwiązał problem skażonej istoty.


### Wypalenie Saitaera z Trzęsawiska

* **uid:** 190116-wypalenie-saitaera-z-trzesawiska, _numer względny_: 5
* **daty:** 0110-01-06 - 0110-01-08
* **obecni:** Alan Bartozol, ASD Centurion, Hieronim Maus, Karradrael, Kasjopea Maus, Pięknotka Diakon, Saitaer, Wiktor Satarail

Streszczenie:

Pięknotka dostała zadanie eskortowania kapłana Karradraela do ołtarza Saitaera. ASD Centurion oraz Alan odwracali uwagę Trzęsawiska. Plan się udał, choć w wyniku Pięknotka została znowu przekształcona przez Saitaera. Za to, nie ma już wpływu Saitaera w okolicy - więcej, udało się zdjąć też wpływ z Wiktora.

Aktor w Opowieści:

* Dokonanie:
    * wraz ze swoją małą ekipą szturmowców odwracał uwagę Trzęsawiska od Pięknotki. Z powodzeniem. Zdobył ogromną nienawiść Wiktora.


### Eksperymenty na wiłach?

* **uid:** 190112-eksperymenty-na-wilach, _numer względny_: 4
* **daty:** 0109-12-27 - 0109-12-30
* **obecni:** Alan Bartozol, Kornel Garn, Pięknotka Diakon, Saitaer, Sławomir Muczarek

Streszczenie:

Alan powiedział Pięknotce, że Myszy polują na wiły dla kogoś. Okazało się, że były wojskowy (Kornel) eksperymentuje na wiłach by stworzyć potężną pryzmatyczną broń defensywną w formie kwiatów. Niestety, to dzieje się na terenie na którym był Saitaer i Władca Rekonstrukcji powrócił. Nadal słaby, ale zintegrował się z Pięknotką i zbudował ołtarz na Trzęsawisku. Kornel uciekł na swój teren a cała akcja skończyła się rozpadem Myszy.

Aktor w Opowieści:

* Dokonanie:
    * niechętnie, ale dał się wpakować w akcję przeciwko własnej gildii z uwagi na ryzyko dla Pustogoru jeśli Saitaer skazi Trzęsawisko. Prawie pokonał Kornela.


### Turyści na Trzęsawisku

* **uid:** 190105-turysci-na-trzesawisku, _numer względny_: 3
* **daty:** 0109-12-21 - 0109-12-23
* **obecni:** Alan Bartozol, Pięknotka Diakon, Wiktor Satarail

Streszczenie:

Grupa turystów spoza Pustogoru odwiedziała Trzęsawisko wraz z trzema łowcami nagród. Pięknotka i Alan poszli ich ratować. W samą porę - sygnał SOS pokazał, że jest już późno. Udało się wszystkich uratować, za co... Pięknotce ktoś zdemolował gabinet. Pięknotka i Alan doszli do pewnego porozumienia w sprawie Adeli - znają jej imię. Acz chyba Alan się o nią martwi.

Aktor w Opowieści:

* Dokonanie:
    * pokazał, jak artyleryjski servar poradzi sobie na Trzęsawisku. Świetny terminus, acz skończył nieprzytomny mimo stymulantów. Uratował wielu.


### Wojna o uczciwe półfinały

* **uid:** 181101-wojna-o-uczciwe-polfinaly, _numer względny_: 2
* **daty:** 0109-10-16 - 0109-10-18
* **obecni:** Alan Bartozol, Damian Podpalnik, Karolina Erenit, Marlena Maja Leszczyńska, Pięknotka Diakon, Tadeusz Kruszawiecki, Tymon Grubosz

Streszczenie:

Marlena przybyła do Zaczęstwa sprawdzić, czy Zaczęstwiacy oszukują w SupMis. Okazało się, że to ludzie - nie wiedzą o magii i tak, magia im pomaga. Marlena zaczęła próbować im pomóc i wyplątać ich z magii zanim yyizdis zabanuje tą drużynę (konkurencję Marleny). Spowodowała Efekt Skażenia i musiała ją uratować Pięknotka, która zaczęła się zastanawiać jak wplątała się w tą sprawę. Koniec końców się udało - ale Pięknotka dostała w odpowiedzialność zarządzanie Zaczęstwem jako terminuska...

Aktor w Opowieści:

* Dokonanie:
    * terminus Czerwonych Myszy zajmujący się sprawą Skażenia z ramienia Pustogoru. Dostał za niewinność; no dobra, chciał wrobić Pięknotkę w ten teren.
* Progresja:
    * skrycie miłośnik gry 'Unreal Tremor'. Zabanowany przez Yyizdatha i nie wie czemu...


### Protomag z Trzęsawisk

* **uid:** 180817-protomag-z-trzesawisk, _numer względny_: 1
* **daty:** 0109-09-07 - 0109-09-09
* **obecni:** Alan Bartozol, Atena Sowińska, Erwin Galilien, Felicja Melitniek, Lucjusz Blakenbauer, Miedwied Zajcew, Pięknotka Diakon

Streszczenie:

Protomag skrzywdził kierowcę z lokalnej mafii Zajcewów. Miedwied i Pięknotka poszli szukać Felicji - protomaga na której eksperymentowali - na Trzęsawisku Zjawosztup. Tam napotkali na terminusów Czerwonych Myszy którzy też chcieli Felicję. Współpraca Zespołu z Ateną Sowińską doprowadziła do przekazania Felicji im i Miedwied został jej opiekunem. Niestety, Trzęsawisko w wyniku pojawienia się Felicji się rozpaliło - jest gorące i będzie emitować Skażeńce...

Aktor w Opowieści:

* Dokonanie:
    * terminus Czerwonych Myszy któremu zależało na zdobyciu Felicji. Przez działania Pięknotki i Ateny musiał obejść się smakiem.
* Progresja:
    * ma kosę z Pięknotką. Zrobiła z niego idiotę przy użyciu Ateny Sowińskiej.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 19, @: 0111-10-09
    1. Multivirt    : 2, @: 0110-03-16
        1. MMO    : 2, @: 0110-03-16
            1. Kryształowy Pałac    : 2, @: 0110-03-16
    1. Primus    : 19, @: 0111-10-09
        1. Sektor Astoriański    : 19, @: 0111-10-09
            1. Astoria    : 19, @: 0111-10-09
                1. Sojusz Letejski, SW    : 1, @: 0110-04-12
                    1. Granica Anomalii    : 1, @: 0110-04-12
                        1. Pacyfika, obrzeża    : 1, @: 0110-04-12
                        1. Wolne Ptaki    : 1, @: 0110-04-12
                            1. Królewska Baza    : 1, @: 0110-04-12
                1. Sojusz Letejski    : 19, @: 0111-10-09
                    1. Szczeliniec    : 19, @: 0111-10-09
                        1. Powiat Jastrzębski    : 1, @: 0110-10-06
                            1. Praszalek, okolice    : 1, @: 0110-10-06
                                1. Fabryka schronień Defensor    : 1, @: 0110-10-06
                                1. Park rozrywki Janor    : 1, @: 0110-10-06
                            1. Praszalek    : 1, @: 0110-10-06
                                1. Komenda policji    : 1, @: 0110-10-06
                        1. Powiat Pustogorski    : 17, @: 0111-10-09
                            1. Czółenko    : 1, @: 0111-06-20
                            1. Majkłapiec    : 1, @: 0111-10-09
                                1. Kociarnia Zawtrak    : 1, @: 0111-10-09
                            1. Podwiert, okolice    : 1, @: 0111-10-09
                                1. Fortifarma Lechotka    : 1, @: 0111-10-09
                            1. Podwiert    : 7, @: 0111-10-09
                                1. Bastion Pustogoru    : 1, @: 0110-04-20
                                1. Dzielnica Luksusu Rekinów    : 2, @: 0111-09-03
                                    1. Fortyfikacje Rolanda    : 1, @: 0111-09-03
                                    1. Serce Luksusu    : 1, @: 0111-09-03
                                        1. Arena Amelii    : 1, @: 0111-09-03
                                1. Kompleks Korporacyjny    : 1, @: 0111-10-09
                                    1. Dokumentarium Terminuskie    : 1, @: 0111-10-09
                                1. Kosmoport    : 1, @: 0110-03-08
                                1. Las Trzęsawny    : 1, @: 0111-06-20
                                1. Magazyny sprzętu ciężkiego    : 1, @: 0109-12-30
                                1. Odlewnia    : 1, @: 0110-04-20
                                1. Sensoplex    : 2, @: 0110-04-20
                            1. Pustogor    : 10, @: 0110-06-20
                                1. Barbakan    : 2, @: 0109-12-30
                                1. Eksterior    : 1, @: 0110-04-09
                                    1. Miasteczko    : 1, @: 0110-04-09
                                1. Gabinet Pięknotki    : 1, @: 0109-12-23
                                1. Interior    : 1, @: 0110-04-09
                                    1. Bunkry Barbakanu    : 1, @: 0110-04-09
                                    1. Laboratorium Senetis    : 1, @: 0110-04-09
                                1. Miasteczko    : 3, @: 0110-03-16
                                1. Rdzeń    : 4, @: 0110-06-20
                                    1. Szpital Terminuski    : 4, @: 0110-06-20
                            1. Samoklęska    : 1, @: 0110-01-08
                                1. Dom Kornela    : 1, @: 0110-01-08
                                1. Laboratorium analityczne    : 1, @: 0110-01-08
                            1. Trzęsawisko Zjawosztup    : 1, @: 0111-10-09
                            1. Zaczęstwo    : 6, @: 0110-06-20
                                1. Cyberszkoła    : 2, @: 0110-02-26
                                1. Nieużytki Staszka    : 4, @: 0110-06-20
                                1. Osiedle Ptasie    : 2, @: 0110-06-20
                        1. Trzęsawisko Zjawosztup    : 7, @: 0110-04-20
                            1. Accadorian    : 1, @: 0109-12-23
                            1. Głodna Ziemia    : 1, @: 0110-01-08
                            1. Sfera Pyłu    : 1, @: 0109-12-23
                            1. Toń Pustki    : 1, @: 0109-12-23

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Pięknotka Diakon     | 14 | ((180817-protomag-z-trzesawisk; 181101-wojna-o-uczciwe-polfinaly; 190105-turysci-na-trzesawisku; 190112-eksperymenty-na-wilach; 190116-wypalenie-saitaera-z-trzesawiska; 190217-chevaleresse; 190331-kurz-po-ataienne; 190419-osopokalipsa-wiktora; 190429-sabotaz-szeptow-elizy; 190502-pierwszy-emulator-orbitera; 190505-szczur-ktory-chroni; 190830-kto-wrobil-alana; 190901-polowanie-na-pieknotke; 200913-haracz-w-parku-rozrywki)) |
| Diana Tevalier       | 8 | ((190217-chevaleresse; 190331-kurz-po-ataienne; 190429-sabotaz-szeptow-elizy; 190830-kto-wrobil-alana; 190901-polowanie-na-pieknotke; 200913-haracz-w-parku-rozrywki; 211221-chevaleresse-infiltruje-rekiny; 220814-stary-kot-a-trzesawisko)) |
| Lucjusz Blakenbauer  | 4 | ((180817-protomag-z-trzesawisk; 190429-sabotaz-szeptow-elizy; 190505-szczur-ktory-chroni; 190901-polowanie-na-pieknotke)) |
| Wiktor Satarail      | 4 | ((190105-turysci-na-trzesawisku; 190116-wypalenie-saitaera-z-trzesawiska; 190419-osopokalipsa-wiktora; 190505-szczur-ktory-chroni)) |
| Karolina Erenit      | 3 | ((181101-wojna-o-uczciwe-polfinaly; 190217-chevaleresse; 190830-kto-wrobil-alana)) |
| Mariusz Trzewń       | 3 | ((190901-polowanie-na-pieknotke; 200913-haracz-w-parku-rozrywki; 210817-zgubiony-holokrysztal-w-lesie)) |
| Minerwa Metalia      | 3 | ((190429-sabotaz-szeptow-elizy; 190502-pierwszy-emulator-orbitera; 190901-polowanie-na-pieknotke)) |
| Erwin Galilien       | 2 | ((180817-protomag-z-trzesawisk; 190429-sabotaz-szeptow-elizy)) |
| Karolina Terienak    | 2 | ((211221-chevaleresse-infiltruje-rekiny; 220814-stary-kot-a-trzesawisko)) |
| Kasjopea Maus        | 2 | ((190116-wypalenie-saitaera-z-trzesawiska; 190419-osopokalipsa-wiktora)) |
| Marlena Maja Leszczyńska | 2 | ((181101-wojna-o-uczciwe-polfinaly; 190217-chevaleresse)) |
| Marysia Sowińska     | 2 | ((210817-zgubiony-holokrysztal-w-lesie; 211221-chevaleresse-infiltruje-rekiny)) |
| Olaf Zuchwały        | 2 | ((190429-sabotaz-szeptow-elizy; 190502-pierwszy-emulator-orbitera)) |
| Rupert Mysiokornik   | 2 | ((210817-zgubiony-holokrysztal-w-lesie; 211221-chevaleresse-infiltruje-rekiny)) |
| Saitaer              | 2 | ((190112-eksperymenty-na-wilach; 190116-wypalenie-saitaera-z-trzesawiska)) |
| Tymon Grubosz        | 2 | ((181101-wojna-o-uczciwe-polfinaly; 190217-chevaleresse)) |
| Adam Janor           | 1 | ((200913-haracz-w-parku-rozrywki)) |
| Adela Kirys          | 1 | ((190505-szczur-ktory-chroni)) |
| Adrian Wężoskór      | 1 | ((190226-korporacyjna-wojna-w-mmo)) |
| Aleksander Rugczuk   | 1 | ((190429-sabotaz-szeptow-elizy)) |
| Alfred Bułka         | 1 | ((190115-skazony-pnaczoszpon)) |
| Antoni Kotomin       | 1 | ((190226-korporacyjna-wojna-w-mmo)) |
| Ariela Lechot        | 1 | ((220814-stary-kot-a-trzesawisko)) |
| ASD Centurion        | 1 | ((190116-wypalenie-saitaera-z-trzesawiska)) |
| Ataienne             | 1 | ((190331-kurz-po-ataienne)) |
| Atena Sowińska       | 1 | ((180817-protomag-z-trzesawisk)) |
| Barnaba Burgacz      | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Bożymir Szczupak     | 1 | ((190502-pierwszy-emulator-orbitera)) |
| Cyryl Perikas        | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Damian Podpalnik     | 1 | ((181101-wojna-o-uczciwe-polfinaly)) |
| Dariusz Bankierz     | 1 | ((190226-korporacyjna-wojna-w-mmo)) |
| Eliza Ira            | 1 | ((190429-sabotaz-szeptow-elizy)) |
| Eliza Kotlet         | 1 | ((190226-korporacyjna-wojna-w-mmo)) |
| Ernest Kajrat        | 1 | ((190505-szczur-ktory-chroni)) |
| Felicja Melitniek    | 1 | ((180817-protomag-z-trzesawisk)) |
| Franciszek Owadowiec | 1 | ((200913-haracz-w-parku-rozrywki)) |
| Hestia d'Rekiny      | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Hieronim Maus        | 1 | ((190116-wypalenie-saitaera-z-trzesawiska)) |
| Iwan Zawtrak         | 1 | ((220814-stary-kot-a-trzesawisko)) |
| Józef Małmałaz       | 1 | ((190901-polowanie-na-pieknotke)) |
| Justynian Diakon     | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Kacper Bankierz      | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Karla Mrozik         | 1 | ((190429-sabotaz-szeptow-elizy)) |
| Karol Senonik        | 1 | ((200913-haracz-w-parku-rozrywki)) |
| Karradrael           | 1 | ((190116-wypalenie-saitaera-z-trzesawiska)) |
| Kasjan Czerwoczłek   | 1 | ((190505-szczur-ktory-chroni)) |
| Kermit Szperacz      | 1 | ((190226-korporacyjna-wojna-w-mmo)) |
| Kornel Garn          | 1 | ((190112-eksperymenty-na-wilach)) |
| Kornel Szczepanik    | 1 | ((190115-skazony-pnaczoszpon)) |
| Kornelia Weiner      | 1 | ((190115-skazony-pnaczoszpon)) |
| Kot Rozbójnik        | 1 | ((220814-stary-kot-a-trzesawisko)) |
| Krystian Namałłek    | 1 | ((190505-szczur-ktory-chroni)) |
| Krystyna Senonik     | 1 | ((200913-haracz-w-parku-rozrywki)) |
| Laura Tesinik        | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Liliana Bankierz     | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Maja Janor           | 1 | ((200913-haracz-w-parku-rozrywki)) |
| Marian Lechot        | 1 | ((220814-stary-kot-a-trzesawisko)) |
| Melinda Teilert      | 1 | ((200913-haracz-w-parku-rozrywki)) |
| Melissa Durszenko    | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Mi Ruda              | 1 | ((190226-korporacyjna-wojna-w-mmo)) |
| Miedwied Zajcew      | 1 | ((180817-protomag-z-trzesawisk)) |
| Nikola Kirys         | 1 | ((190502-pierwszy-emulator-orbitera)) |
| Oliwia Namałłek      | 1 | ((190505-szczur-ktory-chroni)) |
| Rafał Kamaron        | 1 | ((200913-haracz-w-parku-rozrywki)) |
| Rafał Królewski      | 1 | ((190226-korporacyjna-wojna-w-mmo)) |
| Romeo Węglas         | 1 | ((190331-kurz-po-ataienne)) |
| Santino Mysiokornik  | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Sławomir Muczarek    | 1 | ((190112-eksperymenty-na-wilach)) |
| Staś Arienik         | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Stella Armadion      | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Szymon Oporcznik     | 1 | ((190331-kurz-po-ataienne)) |
| Tadeusz Kruszawiecki | 1 | ((181101-wojna-o-uczciwe-polfinaly)) |
| Talia Aegis          | 1 | ((190830-kto-wrobil-alana)) |
| Tomasz Tukan         | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Urszula Miłkowicz    | 1 | ((220814-stary-kot-a-trzesawisko)) |
| Viirai d'Lechotka    | 1 | ((220814-stary-kot-a-trzesawisko)) |
| Wojciech Słabizna    | 1 | ((190226-korporacyjna-wojna-w-mmo)) |
| Wojciech Zermann     | 1 | ((190830-kto-wrobil-alana)) |
| Żorż d'Namertel      | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
# Aleksandra Termia
## Identyfikator

Id: 2007-aleksandra-termia

## Sekcja Opowieści

### Lewiatan za Pandorę

* **uid:** 220622-lewiatan-za-pandore, _numer względny_: 7
* **daty:** 0112-07-26 - 0112-07-29
* **obecni:** Arianna Verlen, Eustachy Korkoran, Klaudia Stryk, Maria Naavas, Ola Szerszeń, OO Infernia, OO Pandora, OO Straszliwy Pająk, OO Tivr, Raoul Lavanis, Rzeźnik Parszywiec Diakon

Streszczenie:

PRZESZŁOŚĆ: Ola Szerszeń opiekuje się Eleną w Regeneratorze; okazuje się być biosyntem (zakazane eksperymenty Termii). Rzeźnik Diakon to łagodny i jowialny kapitan Straszliwego Pająka, acz znienawidzony za przeszłość zdrajcy Orbitera oraz Noctis. TERAŹNIEJSZOŚĆ: Lewiatan zostaje zestrzelony torpedą anihilacyjną, dzięki Raoulowi ratujemy wielu ludzi na orbicie Lewiatana. Malictrix d'Pandora zostaje zniszczona przez Lewiatana, ale Klaudia ma jego mikrokopię dzięki Ariannie.

Aktor w Opowieści:

* Dokonanie:
    * AKTOR NIEOBECNY NA SESJI
* Progresja:
    * jej reputacja dostaje STRASZNY cios przez Klaudię i Marię. Robi zakazane eksperymenty na ludziach i biosyntach. Wyszło na jaw i jest głośne.


### EtAur - dziwnie ważny węzeł

* **uid:** 220706-etaur-dziwnie-wazny-wezel, _numer względny_: 6
* **daty:** 0112-07-15 - 0112-07-17
* **obecni:** Aleksandra Termia, Eustachy Korkoran, Klaudia Stryk, Maria Naavas

Streszczenie:

Maria zainteresowała się planem adm. Termii wskazującym na to, że ta zdradza Orbiter z Syndykatem Aureliona. Eustachy (do którego przyszła) zapytał Termię wprost i doszedł do tego, że Termia szuka koloidowej bazy Aureliona na księżycu Elang swymi bezdusznymi metodami. Eustachy i Klaudia wskazali Termii, że baza EtAur jest potencjalnie punktem kontaktowym Syndykatu z Astorią i tu jest ryzyko. Eustachy doprowadził do kontaktu Termia - EtAur, ale też gra na współpracę Inferni i EtAur.

Aktor w Opowieści:

* Dokonanie:
    * wielopoziomowy plan polegający na infiltracji statku Aureliona przez neuroviatoribusa; przekonana przez Eustachego i Klaudię, że konieczny jest sojusz Orbitera z EtAur. Zaczęła wspierać EtAur środkami Orbitera, łamiąc zasadę "Orbiter to jedyna siła dostępu do planety".
* Progresja:
    * weszła w sojusz z EtAur Zwycięską, acz ogromnym kosztem reputacyjnym.


### Szara nawałnica

* **uid:** 211013-szara-nawalnica, _numer względny_: 5
* **daty:** 0112-01-12 - 0112-01-17
* **obecni:** Aleksandra Termia, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Martyn Hiwasser, Otto Azgorn, Remigiusz Błyszczyk, TAI Rzieza d'K1

Streszczenie:

Zespół przygotował się na wejście do Mevilig. Rzieza odkrył jak działają Piranie i że tamten sektor jest opanowany przez TAI 5+ generacji. Niestety, Infernia musiała uciekać przed Rziezą. Na miejscu Eustachy znalazł skuteczny sposób przemykania między Piraniami, dotarli do planetoidy Kalarfam gdzie znaleźli bazę ludzi. Mechaniczny "smok" został zniszczony przez Infernię, pobrali lokalsów i Arianna przekonała ich do zmiany kultu. Aha, anomalny wybuch torpedy anihilacyjnej Arianna przekierowała na planetę, wypowiadając ostrą wojnę TAI 5+ generacji...

Aktor w Opowieści:

* Dokonanie:
    * odbudowała Inferni zapasy i sprzęt. Współpracuje z Infernią, bo chce odzyskać swoich ludzi z Mevilig. Autoryzowała nawet torpedy anihilacyjne.


### Grupa Ekspedycyjna Kellert

* **uid:** 210929-grupa-ekspedycyjna-kellert, _numer względny_: 4
* **daty:** 0112-01-07 - 0112-01-10
* **obecni:** Adam Nerawol, Aleksandra Termia, Antoni Kramer, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Olena Orion, OO Infernia, OO Omega Septius, Otto Azgorn, TAI Marszałek Grzmotoszpon Trzeci, Vigilus Mevilig

Streszczenie:

Termia wysłała siły do sektora "Noviter", ale to był inny sektor (Mevilig). Straciła bezzałogowce, potem grupę ekspedycyjną Kellert. Kramer wysłał Infernię zakoloidowaną; okazało się, że ten sektor ma "strażnika Esuriit" zrobionego przez ludzi i grupę Piranii, sterowanych przez TAI 4 poziomu. Inferni udało się zebrać dane i wrócić z siedmioma uratowanymi członkami OO Savera, gdzie większość załogi została corruptowana przez Vigilusa...

Aktor w Opowieści:

* Dokonanie:
    * przeprowadziła operację w sektorze "Noviter", nie wiedząc, że to sektor Mevilig. Straciła 6 okrętów i 200 osób. Nie chciała tracić więcej - ale Ariannie dodała Emulatorkę.


### Infernia jest nasza!

* **uid:** 210804-infernia-jest-nasza, _numer względny_: 3
* **daty:** 0111-04-23 - 0111-04-26
* **obecni:** Aleksandra Termia, Antoni Kramer, Arianna Verlen, Artur Traffal, Diana d'Infernia, Eustachy Korkoran, Franciszek Maszkiet, Klaudia Stryk, Leona Astrienko, Maciej Żarand, OO Samotność Gwiazd

Streszczenie:

Ku wielkiemu niezadowoleniu Zespołu, Infernia miała im być odebrana i złomowana. Mieli dostać nowy tańszy statek. Zespół odkrył że za tym stoi komodor Traffal z sił Termii który przy użyciu Inferni chciał złapać w potrzask arystokratę eternijskiego który był kultystą magii. Zespół odzyskał Infernię, przesunął kultystę w kierunku na Ariannę i uciemiężył komodora Traffala - będzie miał pecha na K1.

Aktor w Opowieści:

* Dokonanie:
    * nie chroni swoich komodorów, trzyma się zasady "najsilniejszy przetrwa", ale daje im dużo wyższą autonomię. Chroni Ariannę na K1 pozwalając jej "się buntować" - dlatego żandarmeria nie ruszyła przeciw niej i załodze Inferni.


### Krystaliczny gniew Elizy

* **uid:** 201014-krystaliczny-gniew-elizy, _numer względny_: 2
* **daty:** 0111-01-02 - 0111-01-05
* **obecni:** Aleksandra Termia, Anastazja Sowińska, Arianna Verlen, Ataienne, Dariusz Krantak, Elena Verlen, Eliza Ira, Eustachy Korkoran, Klaudia Stryk, Marian Fartel, OO Wesoły Wieprzek

Streszczenie:

Pomoc humanitarna przybyła do Trzeciego Raju wraz z zabójcami Anastazji. Mindwarpowani komandosi noktiańscy prawie zabili Elenę, ale Anastazja została ochroniona. Do akcji weszła Eliza Ira - zażądała oddania jej Trzeciego Raju i noktian. Zespół przygotowuje się do nie-oddania Raju i uratowania jak najwięcej noktian z Aurum (jakkolwiek to nie brzmi).

Aktor w Opowieści:

* Dokonanie:
    * zrobiła 'złośliwy, przyjacielski psikus' Ariannie Verlen i przekazała jej do dyspozycji statek "Wesoły Wieprzek".


### Nienawiść do świń

* **uid:** 200826-nienawisc-do-swin, _numer względny_: 1
* **daty:** 0110-12-08 - 0110-12-14
* **obecni:** Aleksandra Termia, Antoni Kramer, Arianna Verlen, Eustachy Korkoran, Kamil Lyraczek, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Sebastian Alarius, Tadeusz Ursus

Streszczenie:

Po pojedynku Kramer opieprzył Ariannę i dał jej dyskretną tajną misję - ma niby w niesławie wozić świnie, naprawdę ma pomóc Orbiterowi rozwiązać spisek w Trzecim Raju. Ale wysłanie Arianny na świnie wywołało bunt na Kontrolerze Pierwszym; Arianna musiała rozpaczliwie deeskalować, nawet budząc Leonę. Udało się bunt opanować, acz wysokim kosztem - pierwszy post-noktiański kapitan nie żyje, załoga Inferni częściowo zesłana będzie do Trzeciego Raju a Arianna miała bardzo żenujące wejście.

Aktor w Opowieści:

* Dokonanie:
    * admirał. Świetnie się bawi widząc bunt na Kontrolerze Pierwszym i to jeszcze przez Ariannę i Kramera. Pomogła Ariannie wydobyć Martyna by ją sprawdzić.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 6, @: 0112-07-17
    1. Primus    : 6, @: 0112-07-17
        1. Sektor Astoriański    : 6, @: 0112-07-17
            1. Astoria, Orbita    : 3, @: 0112-01-17
                1. Kontroler Pierwszy    : 3, @: 0112-01-17
            1. Astoria    : 1, @: 0111-01-05
                1. Sojusz Letejski, NW    : 1, @: 0111-01-05
                    1. Ruiniec    : 1, @: 0111-01-05
                        1. Trzeci Raj    : 1, @: 0111-01-05
            1. Brama Kariańska    : 2, @: 0112-01-17
            1. Stocznia Kariańska    : 1, @: 0112-01-17
        1. Sektor Mevilig    : 2, @: 0112-01-17
            1. Chmura Piranii    : 2, @: 0112-01-17
            1. Keratlia    : 1, @: 0112-01-17
            1. Planetoida Kalarfam    : 1, @: 0112-01-17
        1. Sektor Noviter    : 1, @: 0112-01-10

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Eustachy Korkoran    | 6 | ((200826-nienawisc-do-swin; 201014-krystaliczny-gniew-elizy; 210804-infernia-jest-nasza; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica; 220706-etaur-dziwnie-wazny-wezel)) |
| Klaudia Stryk        | 6 | ((200826-nienawisc-do-swin; 201014-krystaliczny-gniew-elizy; 210804-infernia-jest-nasza; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica; 220706-etaur-dziwnie-wazny-wezel)) |
| Arianna Verlen       | 5 | ((200826-nienawisc-do-swin; 201014-krystaliczny-gniew-elizy; 210804-infernia-jest-nasza; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica)) |
| Antoni Kramer        | 3 | ((200826-nienawisc-do-swin; 210804-infernia-jest-nasza; 210929-grupa-ekspedycyjna-kellert)) |
| Elena Verlen         | 3 | ((201014-krystaliczny-gniew-elizy; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica)) |
| Leona Astrienko      | 3 | ((200826-nienawisc-do-swin; 210804-infernia-jest-nasza; 210929-grupa-ekspedycyjna-kellert)) |
| Martyn Hiwasser      | 3 | ((200826-nienawisc-do-swin; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica)) |
| Otto Azgorn          | 2 | ((210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica)) |
| Adam Nerawol         | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| Anastazja Sowińska   | 1 | ((201014-krystaliczny-gniew-elizy)) |
| Artur Traffal        | 1 | ((210804-infernia-jest-nasza)) |
| Ataienne             | 1 | ((201014-krystaliczny-gniew-elizy)) |
| Dariusz Krantak      | 1 | ((201014-krystaliczny-gniew-elizy)) |
| Diana d'Infernia     | 1 | ((210804-infernia-jest-nasza)) |
| Eliza Ira            | 1 | ((201014-krystaliczny-gniew-elizy)) |
| Franciszek Maszkiet  | 1 | ((210804-infernia-jest-nasza)) |
| Kamil Lyraczek       | 1 | ((200826-nienawisc-do-swin)) |
| Maciej Żarand        | 1 | ((210804-infernia-jest-nasza)) |
| Maria Naavas         | 1 | ((220706-etaur-dziwnie-wazny-wezel)) |
| Marian Fartel        | 1 | ((201014-krystaliczny-gniew-elizy)) |
| Olena Orion          | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| OO Infernia          | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| OO Omega Septius     | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| OO Samotność Gwiazd  | 1 | ((210804-infernia-jest-nasza)) |
| OO Wesoły Wieprzek   | 1 | ((201014-krystaliczny-gniew-elizy)) |
| Remigiusz Błyszczyk  | 1 | ((211013-szara-nawalnica)) |
| Sebastian Alarius    | 1 | ((200826-nienawisc-do-swin)) |
| Tadeusz Ursus        | 1 | ((200826-nienawisc-do-swin)) |
| TAI Marszałek Grzmotoszpon Trzeci | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| TAI Rzieza d'K1      | 1 | ((211013-szara-nawalnica)) |
| Vigilus Mevilig      | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
# Minerwa Metalia
## Identyfikator

Id: 1904-minerwa-metalia

## Sekcja Opowieści

### Kraloth w parku Janor

* **uid:** 201025-kraloth-w-parku-janor, _numer względny_: 26
* **daty:** 0110-10-25 - 0110-10-27
* **obecni:** Hestia d'Janor, Lucjusz Blakenbauer, Maciej Oczorniak, Minerwa Metalia, Pięknotka Diakon

Streszczenie:

Dużo ciężkiej pracy Lucjusza i Minerwy, ale Pięknotka wróciła do "normy", acz potrzebuje regularnych transfuzji. Pięknotka oddała Minerwie Erwina; sama myśli jak zamieszkać z Lucjuszem i nie udawać jego narzeczonej. Chciała skończyć sprawę w parku rozrywki; z Minerwą odkryły że za wszystkim stoi cholerny kraloth. Porwały terminusa przejętego przez kralotha by on wezwał SOS z Pustogoru.

Aktor w Opowieści:

* Dokonanie:
    * pomaga Lucjuszowi w rekonstrukcji Pięknotki, potem zastawia z Pięknotką pułapkę na kralotycznie zniewolonego terminusa i robi ixiońską efemerydę co krzyczy.
* Progresja:
    * przejmuje Erwina Galiliena; ona nadal go kocha a Pięknotka jej go "oddała".


### Narodziny paladynki Saitaera

* **uid:** 201011-narodziny-paladynki-saitaera, _numer względny_: 25
* **daty:** 0110-10-08 - 0110-10-09
* **obecni:** Agaton Ociegor, Aleksander Muniakiewicz, Gabriel Ursus, Minerwa Metalia, Pięknotka Diakon, Sabina Kazitan, Saitaer

Streszczenie:

Pięknotka wyprawiła Sabinę Kazitan do Aurum po akcji Gabriela, ale wymuszając na obu stronach by zaprzestały wojny ze sobą. Jako, że zaczął się atak na kompleks Grzymościa, Alan i Gabriel zostali tam przekierowani - a Pięknotka poszła z Minerwą do parku rozrywki Janor by zakończyć tą sprawę. Niestety, grzymościowiec Ociegor złapał Pięknotkę chcąc ją "wyleczyć" z kralotycznej siły. Pięknotka wezwała Cienia i ów prawie zabił Ociegora. Walcząc z Cieniem, Pięknotka umarła - i Saitaer ją przywrócił jako swoją paladynkę.

Aktor w Opowieści:

* Dokonanie:
    * zinfiltrowała TAI Hestia w parku rozrywki Janor po dywersji zrobionej przez Pięknotkę. Przekonała Pięknotkę, że życie po Saitaerze to nie koniec; da się naprawić.


### Adaptacja Azalii

* **uid:** 200623-adaptacja-azalii, _numer względny_: 24
* **daty:** 0110-09-22 - 0110-09-30
* **obecni:** Azalia d'Alkaris, Karla Mrozik, Konrad Wączak, Minerwa Metalia, Pięknotka Diakon, Wiktor Satarail

Streszczenie:

Orbiter, frakcja NeoMil, wystawili na Trzęsawisko bazę nanitkową której celem jest uczenie odludzkiej TAI klasy Azalia. Pięknotka i Minerwa poszły jako wsparcie z Pustogoru. Okazało się, że Wiktor pozwala TAI Azalii na wiarę, że adaptuje się do Trzęsawiska by przejąć nad nią kontrolę. Pięknotka wynegocjowała bezpieczną ewakuację - ale Wiktor powiedział, że zniszczy Castigator. Pięknotka nie wie jak, ale OK. Nie wygra tego. Grunt, że wszyscy są bezpieczni.

Aktor w Opowieści:

* Dokonanie:
    * z Pięknotką na tajnej pustogorskiej operacji ratowania Orbitera/NeoMil przed Trzęsawiskiem; miała też wykryć co jest dziwnego z TAI Azalia. Używając energii ixiońskiej przejęła kontrolę nad nanitkami Azalii by wszystkich wyprowadzić.


### Po co atakują Minerwę?

* **uid:** 200502-po-co-atakuja-minerwe, _numer względny_: 23
* **daty:** 0110-08-22 - 0110-08-24
* **obecni:** Feliks Mirtan, Halina Sermniek, Kreacjusz Diakon, Minerwa Metalia, Nikodem Larwent, Pięknotka Diakon

Streszczenie:

Okazuje się, że za atakiem na Minerwę stali Technożercy - a dokładniej, ich liderka - Halina. Pięknotka ostro weszła do Haliny, dewastując jej mieszkanie. Dowiedziała się, że Halina chciała dać swoim sponsorom z Aurum coś czego żądali - i przypadkowo (nie wiedząc o tym) skalibrowała Infiltratora na Minerwę. Pięknotka złamała Halinę, zmusiła ją do opuszczenia Technożerców. Nie ma mowy, że czarodziejka będzie wspierać taką organizację ludzi.

Aktor w Opowieści:

* Dokonanie:
    * analizując anty-TAI z Infiltratora doszła do tego, że to kupione anty-TAI Mirtana; to nie chodziło o atak na nią.


### Infiltrator poluje na TAI Minerwy

* **uid:** 200425-inflitrator-poluje-na-tai-minerwy, _numer względny_: 22
* **daty:** 0110-08-20 - 0110-08-22
* **obecni:** Erwin Galilien, Gabriel Ursus, Laura Tesinik, Minerwa Metalia, Pięknotka Diakon, Strażniczka Alair, Tomasz Tukan

Streszczenie:

Problemy na Trzęsawisku wpływają na Cyberszkołę w Zaczęstwie, co uszkadza możliwości TAI "Strażniczki", czyli Eszary d'AlephAiren. Tymczasem jakaś tajemnicza siła uszkadza cywilne TAI, które usprawniała Minerwa. Pięknotka odpiera Tukana, który chce udowodnić winę Minerwy i odkrywa, że za tym wszystkim stoi ktoś z Aurum, ktoś kto posiada profesjalnego pajęczego Infiltratora Iniekcyjnego, zaprojektowanego do uszkadzania AI. Dzięki temu Pięknotka wyciągnęła Minerwę z aresztu.

Aktor w Opowieści:

* Dokonanie:
    * dorabia sobie usprawniając TAI. Jako, że część się psuje w groźny sposób, aresztował ją Tukan. Ale naprawdę to był sabotaż... kogoś z Aurum?


### Krucjata Chevaleresse

* **uid:** 200202-krucjata-chevaleresse, _numer względny_: 21
* **daty:** 0110-07-20 - 0110-07-23
* **obecni:** Ataienne, Damian Orion, Diana Tevalier, Keraina d'Orion, Malictrix d'Itaran, Mariusz Trzewń, Mateusz Kardamacz, Minerwa Metalia, Pięknotka Diakon, Talia Aegis, Tomasz Tukan

Streszczenie:

Chevaleresse wykradła broń Alana i uzbroiła Liberitias, gdzie działa też Ataienne przeciw Kardamaczowi, Grzymościowi i krzywdzicielowi Alana. Pięknotka doszła do serca problemu i materii, acz potrzebowała wsparcia Orbitera - dokładniej, Kerainy. Dzięki Kerainie wprowadziła Malictrix do Kompleksu Itaran. Teraz to tylko kwestia czasu aż Grzymość zostanie zniszczony.

Aktor w Opowieści:

* Dokonanie:
    * nieco zaniepokojona, ale sformowała dla Pięknotki anty-TAI klasy Malictrix do zniszczenia kompleksu Itaran. Poszło jej za dobrze.


### Wygrany kontrakt

* **uid:** 200311-wygrany-kontrakt, _numer względny_: 20
* **daty:** 0110-07-16 - 0110-07-19
* **obecni:** Amanda Kajrat, Dagmara Doberman, Ernest Kajrat, Liliana Bankierz, Lucjusz Blakenbauer, Minerwa Metalia, Ziemowit Zięba

Streszczenie:

Ernest Kajrat jest w stanie śmiertelnym; Lucjusz i Minerwa trzymają go przy życiu. Tymczasem firma dostarczająca jedzenie "Zygmunt Zając" wygrała przetarg (na który nie startowała) odnośnie jedzenia dla zixionizowanej Amandy Kajrat w rękach Grzymościa. Współpraca Blakenbauera i "Zająca" doprowadza do uwolnienia Amandy i kupienia szansy Ernestowi...

Aktor w Opowieści:

* Dokonanie:
    * współkonspiratorka z Lucjuszem Blakenbauerem, skupiła się na użyciu energii ixiońskich do pomocy Kajratowi - zarówno Ernestowi jak i Amandzie


### Ukradziony Entropik

* **uid:** 191201-ukradziony-entropik, _numer względny_: 19
* **daty:** 0110-07-09 - 0110-07-11
* **obecni:** Hestia d'Itaran, Keraina d'Orion, Mariusz Trzewń, Minerwa Metalia, Persefona d'Jastrząbiec, Pięknotka Diakon, Roland Grzymość

Streszczenie:

Orbiter szukał informacji o zaginionym dawno Entropiku; Ataienne go wykryła gdy przejęła Jastrząbiec. Pięknotka wraz z miragentem znalazła owego Entropika - jest w ukrytym kompleksie medyczno-badawczym Grzymościa. By nie eskalować i nie ryzykować ludzkich żyć, doprowadziła do ewakuacji Entropika. Dzięki temu Orbiter musiał poradzić sobie sam. A Pustogor stanął przed tym, że Grzymość ma potężnych sojuszników i potężną bazę w Jastrząbcu.

Aktor w Opowieści:

* Dokonanie:
    * zdalna konsultantka Pięknotki odnośnie różnic między Persefoną i Hestią. Potrafi, ku osłupieniu Pięknotki, zniszczyć AI zdalnie.


### Korupcja z arystokratki

* **uid:** 191112-korupcja-z-arystokratki, _numer względny_: 18
* **daty:** 0110-06-30 - 0110-07-01
* **obecni:** Ataienne, Ignacy Myrczek, Mariusz Trzewń, Minerwa Metalia, Persefona d'Jastrząbiec, Pięknotka Diakon, Sabina Kazitan, Tadeusz Sklerzec

Streszczenie:

Po wydarzeniach z Grzymościem oczy Pięknotki padły na Jastrząbiec - tam dzieją się dziwne rzeczy z narkotykami. Okazało się, że to Ignacy Myrczek próbuje rozpaczliwie udowodnić arystokratce z Aurum, że nie zrobił jej krzywdy świadomie a pod wpływem narkotyków. Pięknotka wpadła (z pomocą Ataienne) i zrobiła porządek - ukarała burmistrza, Myrczka, arystokratkę i w sumie wszystkich. Ma smak popiołu w ustach, tu nie było nikogo sensownego...

Aktor w Opowieści:

* Dokonanie:
    * doradziła Pięknotce, że Persefona jest nietypową TAI do miasta; dodatkowo, jak zbadać czy działa i co z nią nie tak.


### Kontrpolowanie Pięknotki - pułapka

* **uid:** 191103-kontrpolowanie-pieknotki-pulapka, _numer względny_: 17
* **daty:** 0110-06-20 - 0110-06-27
* **obecni:** Damian Orion, Józef Małmałaz, Ksenia Kirallen, Lilia Ursus, Marek Puszczok, Mariusz Trzewń, Minerwa Metalia, Olaf Zuchwały, Pięknotka Diakon, Rafał Roszczeniok

Streszczenie:

Pięknotka poluje na noktiańskiego zabójcę. W ciągu tygodnia udało jej się złożyć do kupy pułapkę, rozpocząć linię przerzutową artefaktów do Aurum, połączyć Orbiter, Lilię, Minerwę oraz Pustogor i doprowadzić do tego, że docelowo Małmałaz ma zostać złapany i doprowadzony do Pustogoru w zemście za krzywdę Alana. To był bardzo trudny tydzień, politycznie.

Aktor w Opowieści:

* Dokonanie:
    * rozpoczyna handel TAI i artefaktami / anomaliami z Aurum przez Grzymościa by zdobyć zasoby do badań. Idzie, jak to ona, o jeden krok za daleko. Jej cień-terminus ją wspiera.


### Polowanie na Pięknotkę

* **uid:** 190901-polowanie-na-pieknotke, _numer względny_: 16
* **daty:** 0110-06-17 - 0110-06-20
* **obecni:** Alan Bartozol, Diana Tevalier, Józef Małmałaz, Lucjusz Blakenbauer, Mariusz Trzewń, Minerwa Metalia, Pięknotka Diakon

Streszczenie:

Noktiański zabójca chciał pozyskać Cienia; pokonał Pięknotkę, ale Alan go zestrzelił. Potem zabójca złapał Chevaleresse i korzystając z niej jako zakładniczki wysłał Alana do szpitala. Pięknotka doszła do tego, jak się nazywa z pomocą Minerwy i Cienia.

Aktor w Opowieści:

* Dokonanie:
    * przyszła pomóc Pięknotce zregenerować Cienia i dowiedzieć się kim jest napastnik. Udało się, acz rozwaliła pokój w szpitalu.


### Bardzo nieudane porwania

* **uid:** 190503-bardzo-nieudane-porwania, _numer względny_: 15
* **daty:** 0110-04-14 - 0110-04-16
* **obecni:** Damian Orion, Karla Mrozik, Minerwa Metalia, Mirela Orion, Nikola Kirys, Pięknotka Diakon

Streszczenie:

Kirasjerzy Orbitera dostali zadanie odbicia Minerwy i Nikoli, ale w mieszkaniu Minerwy czekała Pięknotka. Porwała z zaskoczenia Emulatorkę, uwolniła ją Cieniem i gdy Kirasjerzy uderzyli by po cichu odzyskać agentkę, Pięknotka poprosiła o pomoc Epirjon. W wyniku Pięknotce udało się ewakuować z Emulatorką, uwolniła ją Cieniem i wsadziła do Pustogoru. Potem z Karlą wynegocjowała ochronę dla Minerwy i Nikoli i jeszcze z tymi dwoma załatwiła by to było możliwe. Kirasjerzy zostali odepchnięci przez JEDNĄ terminuskę Pustogoru.

Aktor w Opowieści:

* Dokonanie:
    * polowali na nią Kirasjerzy; Pięknotka przekonała ją do zamieszkania w pustogorskim Miasteczku. Tam jest bezpieczna.
* Progresja:
    * chroniona przed Orbiterem; przeniesiona do Miasteczka w Pustogorze i dano jej autonomię do czarowania i eksperymentów


### Pierwszy Emulator Orbitera

* **uid:** 190502-pierwszy-emulator-orbitera, _numer względny_: 14
* **daty:** 0110-04-10 - 0110-04-12
* **obecni:** Alan Bartozol, Bożymir Szczupak, Minerwa Metalia, Nikola Kirys, Olaf Zuchwały, Pięknotka Diakon

Streszczenie:

Idąc śladami Elizy Pięknotka natrafiła na Nikolę - o której dawni przyjaciele (Olaf) mówią, że "zginęła", coś jej zrobiono. Pięknotka doszła do tego, że Nikola jest projektem Emulator; dowiedziała się, że Emulatory były budowane m.in. przez Minerwę. Pięknotka doszła do tego, że zintegrowana z Finis Vitae Nikola została częściowo uwolniona przez wpływ Arazille na Finis Vitae. Gdy spotkała się z Nikolą - szok. Cień zareagował. Nikola odzyskała wolność po kontakcie z Cieniem, ale Pięknotka go utrzymała. Cień ma w sobie "emocje" spętanych Emulatorów. Pięknotce udało się wyperswadować Nikoli budzenie autowara lub niszczenie Astorii; zwiadowczyni jednak odjechała w ogromnej konfuzji i nie wiedząc, co teraz robić.

Aktor w Opowieści:

* Dokonanie:
    * leżąc w szpitalu pomaga Pięknotce zrozumieć o co chodzi z Emulatorami Orbitera i jak bardzo to niebezpieczna sprawa.
* Progresja:
    * wyszło na jaw Pięknotce, że to ona stała za psychotroniką pierwszych Emulatorów; naprawdę jest genialnym naukowcem


### Sabotaż szeptów Elizy

* **uid:** 190429-sabotaz-szeptow-elizy, _numer względny_: 13
* **daty:** 0110-04-06 - 0110-04-09
* **obecni:** Alan Bartozol, Aleksander Rugczuk, Diana Tevalier, Eliza Ira, Erwin Galilien, Karla Mrozik, Lucjusz Blakenbauer, Minerwa Metalia, Olaf Zuchwały, Pięknotka Diakon

Streszczenie:

Chevaleresse wpakowała się w "kult Elizy" grając z ludźmi online; powiedziała o tym Pięknotce. Pięknotka odkryła, że Eliza rozprzestrzenia swoje wpływy przez kryształy w formie biżuterii; wraz z Alanem, Erwinem i (nadal ranną) Minerwą zaprojektowali mechanizm uszkadzający sieć krystaliczną Elizy. Udało im się kupić jakiś miesiąc czasu, aż Orbiter i Pustogor znajdą lepsze rozwiązanie. Sama Chevaleresse ma się nie spotykać z takimi tam. Ale może z nimi grać online.

Aktor w Opowieści:

* Dokonanie:
    * wyszła ranna, wróciła bardziej ranna; Cień wyraźnie chce ją zabić. Kontroluje energię ixiońską i dzięki temu z Alanem i Erwinem złożyli doskonały rezonator.


### Zrzut w Pacyfice

* **uid:** 190427-zrzut-w-pacyfice, _numer względny_: 12
* **daty:** 0110-04-03 - 0110-04-05
* **obecni:** Erwin Galilien, Minerwa Metalia, Nikola Kirys, Pięknotka Diakon

Streszczenie:

Do Pacyfiki ktoś z Cieniaszczytu zrzucił świetnej klasy ścigacz. Pięknotka i Erwin zinfiltrowali Pacyfikę by dostać namiary na ten pojazd. Okazało się, że pilotem ma być Nikola - też Nurek Szczeliny, świetnej klasy. Eks-Noctis. Pięknotka po raz pierwszy była w Pacyfice. Dodatkowo, dowiedziała się od Minerwy, że Cień niekoniecznie odpala się na żądanie, nie rozumie Pięknotki. Świetnie.

Aktor w Opowieści:

* Dokonanie:
    * pomogła Pięknotce zrozumieć dlaczego Cień zachowuje się tak pasywnie, mimo, że nadal leży w szpitalu


### Budowa ixiońskiego mimika

* **uid:** 190424-budowa-ixionskiego-mimika, _numer względny_: 11
* **daty:** 0110-03-30 - 0110-04-01
* **obecni:** Aleksander Rugczuk, Erwin Galilien, Karla Mrozik, Minerwa Metalia, Olaf Zuchwały, Pięknotka Diakon

Streszczenie:

W świetle narastających napięć w Pustogorze i czarnych artefaktów na wolności, Pięknotka wykorzystuje (za aprobatą Barbakanu) mimika który zdominował Marcela. Minerwa go psychotronicznie osłabia z perspektywy morderstw i infekuje go energią ixiońską. Pięknotka straciła go przy Dzielnicy Uciechy, ale i tak zrobił swoje (choć straty materialne są większe). Zainfekowany przez mimika wykrzyczał, że Eliza Ira wróciła oraz Saitaer się przebudził. Niepokojące dla Pięknotki.

Aktor w Opowieści:

* Dokonanie:
    * najlepsza psychotroniczka i katalistka ixiońska w okolicy. Pomogła Pięknotce zbudować bezpiecznego mimika ixiońskiego, choć zapłaciła za to zdrowiem.
* Progresja:
    * dyskretny medal za zasługi dla Pustogoru od Karli, za ixiońską infuzję mimika i utrzymanie porządku
    * dwa tygodnie w szpitalu


### Eksperymentalny power suit

* **uid:** 190402-eksperymentalny-power-suit, _numer względny_: 10
* **daty:** 0110-03-11 - 0110-03-13
* **obecni:** Ataienne, Erwin Galilien, Kaja Selerek, Minerwa Metalia, Pięknotka Diakon

Streszczenie:

Orbiter chciał przekazać Pięknotce eksperymentalny Power Suit którego nikt nie potrafi kontrolować. Pięknotka spróbowała się z nim zintegrować; ona, Minerwa, Erwin i jeszcze dwóch magów Orbitera skończyło w szpitalu. Pięknotka się uśmiechnęła. Chce ten power suit. Opanuje go - mimo, że jest Skażony energią ixiońską i ma uszkodzoną psychotronikę pełną nienawiści. Życie Pięknotki uratowała Ataienne, ale skończyła ciężko ranna, z uszkodzoną matrycą.

Aktor w Opowieści:

* Dokonanie:
    * stworzyła kiedyś psychotronikę Cienia i próbowała nad nim zapanować. Niestety, nie udało jej się - skończyła w szpitalu.


### Minerwa i Kwiaty Nadziei

* **uid:** 190210-minerwa-i-kwiaty-nadziei, _numer względny_: 9
* **daty:** 0110-02-20 - 0110-02-22
* **obecni:** Atena Sowińska, Erwin Galilien, Kasjopea Maus, Kornel Garn, Minerwa Metalia, Pięknotka Diakon

Streszczenie:

Kornel Garn próbował przekonać Minerwę do dołączenia do niego, by mu pomogła - budując jej społeczeństwo które ją akceptuje i pokazując jej Kwiaty Nadziei z Ixionu. Minerwa w 100% wpadała w to, więc Pięknotka zmontowała front przeciw Kornelowi - Kasjopea, Erwin, terminusi Pustogorscy. Skończyło się ucieczką Kornela, ale Minerwa będzie chciała mu pomóc i dzielić się z nim wiedzą. A Pięknotkę bardzo martwi to, jak Kornel radzi sobie z ixiońskimi anomaliami i swoim kultem.

Aktor w Opowieści:

* Dokonanie:
    * coraz bardziej zaprzyjaźniała się z Kornelem, co przerwała Pięknotka. Obudziła i rozpoznała Kwiaty Nadziei z Ixionu.
* Progresja:
    * jeszcze większa złość na terminusów pustogorskich. Odepchnęli Kornela, któremu na niej zależało. Chcą ograniczyć energię ixiońską.


### Nie da się odrzucić mocy

* **uid:** 190206-nie-da-sie-odrzucic-mocy, _numer względny_: 8
* **daty:** 0110-02-16 - 0110-02-19
* **obecni:** Arnulf Poważny, Karolina Erenit, Kasjopea Maus, Kornel Garn, Minerwa Metalia, Pięknotka Diakon, Tymon Grubosz

Streszczenie:



Aktor w Opowieści:

* Dokonanie:
    * 


### Migświatło psychotroniczek

* **uid:** 190828-migswiatlo-psychotroniczek, _numer względny_: 7
* **daty:** 0110-02-07 - 0110-02-09
* **obecni:** Artur Michasiewicz, Ernest Kajrat, Marek Puszczok, Minerwa Metalia, Pięknotka Diakon, Talia Aegis, Tymon Grubosz

Streszczenie:

Gdy Kajrat jest w więzieniu, nikt nie kontroluje przepływu technologii noktiańskiej. Talia ostrzegła Pięknotkę, że do holo-walki AI dodała subturingowe BIA klasy Szponowiec, z żądania gościa od Grzymościa. Okazało się, że to niegroźny hazard jest - ale są tam dwie dziwne walczące AI: BIA oraz militarna, stworzona przez Minerwę. Pięknotka chciała tylko zabić BIA, ale przypadkowo z Minerwą wysadziły Arenę Migświatła. Tymon zabrał Minerwie jej TAI. Powiedział, że jeszcze nie czas by takie rzeczy robiła.

Aktor w Opowieści:

* Dokonanie:
    * stworzyła własne TAI do walki na arenie migświatła w Zaczęstwie; w ten sposób się z przyjemnością realizuje. Zabrali jej TAI bo boją się energii ixiońskiej.


### Czarodziejka z woli Saitaera

* **uid:** 190202-czarodziejka-z-woli-saitaera, _numer względny_: 6
* **daty:** 0110-01-31 - 0110-02-04
* **obecni:** Adela Kirys, Karolina Erenit, Minerwa Metalia, Pięknotka Diakon, Saitaer, Sławomir Muczarek, Tymon Grubosz, Wojtek Kurczynos

Streszczenie:

Wojtek Kurczynos ZNOWU dobierał się do Karoliny Erenit. Przez machinacje Sławka i Adeli Saitaer dokonał transfuzji mocy magicznej od Wojtka do Karoliny. Pięknotka drastycznie ukrywała wszystkie wpływy wszystkich osób, by ucierpiał tylko winny Wojtek. Nawet współpracowała z Saitaerem, by terroformizować Wojtka. Wszystko, by Karolina zachowała moc magiczną. ALE - doprowadziła do tego, że spojrzeli na Karolinę Mausowie. W ten sposób Karradrael uniemożliwił hold Saitaera na Karolinie.

Aktor w Opowieści:

* Dokonanie:
    * główny detektor Saitaera i energii ixiońskich. Dodatkowo, przez to, że dotknęła energią Karoliny, dała pretekst Karli by ona sprawdziła Karolinę Karradraelem.


### Ixioński Transorganik

* **uid:** 190127-ixionski-transorganik, _numer względny_: 5
* **daty:** 0110-01-28 - 0110-01-29
* **obecni:** Erwin Galilien, Karolina Erenit, Kirył Najłalmin, Minerwa Metalia, Napoleon Bankierz, Pięknotka Diakon, Saitaer, Tymon Grubosz, Wiktor Satarail, Wojtek Kurczynos

Streszczenie:

Minerwa próbowała pomóc Karolinie Erenit, ale niestety użyła artefaktu ixiońskiego i przekształciła wrednego Wojtka Kurczynosa w transorganika. Pięknotka z pomocą Epirjona przeteleportowała siebie i Wojtka na Trzęsawisko i tam z pomocą Wiktora Sataraila odwrócili tą transorganizację. Ten konkretny problem udało się dyskretnie rozwiązać.

Aktor w Opowieści:

* Dokonanie:
    * bardzo zależy jej na pomocy Karolinie Erenit, więc użyła transorganizacji ixiońskiej na Wojtka. Niestety. Potem odwracała z pomocą Pięknotki.
* Progresja:
    * jej morale sięgnęło dna. Nie ufa swojej magii i swoim umiejętnościom już w ogóle. Nie czuje się częścią tego świata.


### Nowa Minerwa w nowym świecie

* **uid:** 190120-nowa-minerwa-w-nowym-swiecie, _numer względny_: 4
* **daty:** 0110-01-21 - 0110-01-25
* **obecni:** Erwin Galilien, Karla Mrozik, Kasjopea Maus, Minerwa Metalia, Pięknotka Diakon, Tymon Grubosz

Streszczenie:

Minerwa wróciła. Jako czarodziejka, opiekować się ma nią Pięknotka. Minerwa nie do końca radzi sobie w nowej rzeczywistości - nie jest piękną Diakonką, ma odłamki pamięci Saitaera i aby powrócić, poświęciła życie człowieka. Zdecydowała się tymczasowo przenieść do Zaczęstwa (Karla wysłała tam wsparcie). Tymczasem Kornel Garn już planuje by ją przekonać by doń dołączyła.

Aktor w Opowieści:

* Dokonanie:
    * w nowym ciele, 7 smutnych wspomnień i 4 dobre, niepewna i zestresowana. Chce odejść, ale zostaje (na razie) w Zaczęstwie. Największe osiągnięcie psychotroniki w Nutce z Black Technology.
* Progresja:
    * ma nowe ciało, maga. Nie jest już piękną Diakonką, ale ma ciało wspomagane przez Saitaera. Niezbyt lubiana w okolicy Pustogoru.
    * utraciła Erwina. Zwana defilerką. Odepchnięta przez Pustogor. Nie kontroluje magii dobrze. Ogólnie, bardzo zdeptana psychicznie i w złej formie.


### Uwięzienie Saitaera

* **uid:** 181230-uwiezienie-saitaera, _numer względny_: 3
* **daty:** 0109-12-06 - 0109-12-08
* **obecni:** Karla Mrozik, Kreacjusz Diakon, Lucjusz Blakenbauer, Minerwa Metalia, Pięknotka Diakon

Streszczenie:

Pięknotka wróciła do Pustogoru, będąc po drodze zaatakowana przez latającego pnączoszpona. Odkryła, że Saitaer poważnie zmienił już Minerwę. Zmartwiła się tym, jak bardzo ta Minerwa różni się od jej przyjaciółki. Ta sprawa jest ponad siły Pięknotki - poszła do Karli, przełożonej Pustogoru. Tam dowiedziała się, że Saitaer to broń i że być może to on stoi za problemami Trzęsawiska Zjawosztup. Pustogor pojmał Saitaera i przejął wskrzeszoną Minerwę do leczenia.

Aktor w Opowieści:

* Dokonanie:
    * tak dalece pochłonięta przez Saitaera, że chciała przetrwać kosztem ludzi. Zrekonstruowana przez Saitaera po asymilacji nieszczęsnego człowieka.
* Progresja:
    * ma nowe ciało, stworzone przez Saitaera. Nie powinno działać, ale działa - świetnie. Jej ciało i umysł są naprawiane w Pustogorze.


### Terminuska czy kosmetyczka?

* **uid:** 181027-terminuska-czy-kosmetyczka, _numer względny_: 2
* **daty:** 0109-10-07 - 0109-10-11
* **obecni:** Adela Kirys, Arnulf Poważny, Brygida Maczkowik, Ignacy Myrczek, Minerwa Metalia, Pięknotka Diakon

Streszczenie:

Jednoczesny atak na reputację Ateny (przez cybergrzyby) oraz wielki konkurs kosmetyczny w Pustogorze wpłynęły stosunkowo źle na sytuację Pięknotki. Nie rozdwoiła się, ale obie rzeczy udało jej się rozwiązać - acz jest wycieńczona. Odkryła konspirację wymierzoną przeciwko Atenie - ale nie wie kto i gdzie próbuje Atenę usunąć.

Aktor w Opowieści:

* Dokonanie:
    * testuje swoje nowe możliwości; powiedziała Pięknotce o zbliżającym się problemie z cybergrzybami inkarnaty (?); w pewnym momencie straciła kontrolę do terrorforma i tego nie zauważyła.
* Progresja:
    * by powstrzymać terrorforma, złożyła psychotronikę by móc się odciąć i wyłączyć servar w sytuacji awaryjnej.


### Decyzja Minerwy

* **uid:** 181024-decyzja-minerwy, _numer względny_: 1
* **daty:** 0109-10-01 - 0109-10-04
* **obecni:** Adam Szarjan, Adela Kirys, Lilia Ursus, Minerwa Metalia, Pięknotka Diakon

Streszczenie:

Atena i Erwin są w ruinie. Tymczasem servar Erwina wyrwał się na wolność. Okazało się, że Minerwa - dawna miłość Erwina - powróciła. Adam próbował ją zneutralizować a Pięknotka - uratować. Skończyło się na tym, że Minerwa, Nutka i terrorform współistnieją w servarze Erwina, Adama boli głowa a Pięknotka cieszy się, że tymczasowo to nie jej problem. Mistrzyni psychotroniki wróciła.

Aktor w Opowieści:

* Dokonanie:
    * zastąpiła Nutkę jako servar Erwina. Próbowała sprawdzić granicę autonomii i człowieczeństwa. Nieszczęśliwa w formie servara, ale zostanie - by Erwin nie był sam.
* Progresja:
    * trójpersona: Minerwa, Nutka (nieco starsza) i terrorform. Erwin będzie miał niezłe dyskusje.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 26, @: 0110-10-27
    1. Multivirt    : 1, @: 0110-03-13
        1. MMO    : 1, @: 0110-03-13
            1. Kryształowy Pałac    : 1, @: 0110-03-13
    1. Primus    : 26, @: 0110-10-27
        1. Sektor Astoriański    : 26, @: 0110-10-27
            1. Astoria    : 26, @: 0110-10-27
                1. Sojusz Letejski, SW    : 3, @: 0110-04-16
                    1. Granica Anomalii    : 3, @: 0110-04-16
                        1. Pacyfika, obrzeża    : 2, @: 0110-04-16
                        1. Pacyfika    : 1, @: 0110-04-05
                        1. Wolne Ptaki    : 1, @: 0110-04-12
                            1. Królewska Baza    : 1, @: 0110-04-12
                1. Sojusz Letejski    : 25, @: 0110-10-27
                    1. Szczeliniec    : 25, @: 0110-10-27
                        1. Powiat Jastrzębski    : 5, @: 0110-10-27
                            1. Jastrząbiec, okolice    : 2, @: 0110-07-23
                                1. Containment Chambers    : 1, @: 0110-07-11
                                1. Klinika Iglica    : 2, @: 0110-07-23
                                    1. Kompleks Itaran    : 2, @: 0110-07-23
                                1. TechBunkier Sarrat    : 1, @: 0110-07-23
                            1. Jastrząbiec    : 1, @: 0110-07-01
                                1. Hotel Stacja Kosmiczna    : 1, @: 0110-07-01
                                1. Ratusz    : 1, @: 0110-07-01
                            1. Kalbark    : 1, @: 0110-07-23
                                1. Escape Room Lustereczko    : 1, @: 0110-07-23
                            1. Praszalek, okolice    : 2, @: 0110-10-27
                                1. Lasek Janor    : 2, @: 0110-10-27
                                1. Park rozrywki Janor    : 2, @: 0110-10-27
                        1. Powiat Pustogorski    : 20, @: 0110-10-27
                            1. Podwiert    : 4, @: 0110-08-24
                                1. Kopalnia Terposzy    : 1, @: 0109-12-08
                                1. Kosmoport    : 2, @: 0110-04-16
                                1. Magazyny sprzętu ciężkiego    : 1, @: 0109-12-08
                                1. Osiedle Tęczy    : 1, @: 0110-08-24
                                1. Sensoplex    : 1, @: 0110-08-24
                            1. Pustogor, okolice    : 2, @: 0110-10-27
                                1. Rezydencja Blakenbauerów    : 2, @: 0110-10-27
                            1. Pustogor    : 10, @: 0110-06-20
                                1. Arena Szalonego Króla    : 1, @: 0109-10-11
                                1. Barbakan    : 1, @: 0110-01-25
                                1. Eksterior    : 2, @: 0110-04-09
                                    1. Dzielnica Uciechy    : 1, @: 0110-04-01
                                    1. Fort Mikado    : 1, @: 0110-04-01
                                    1. Miasteczko    : 2, @: 0110-04-09
                                1. Gabinet Pięknotki    : 1, @: 0110-01-25
                                1. Inkubator Szamsar    : 1, @: 0109-10-04
                                1. Interior    : 3, @: 0110-04-16
                                    1. Bunkry Barbakanu    : 3, @: 0110-04-16
                                    1. Laboratorium Senetis    : 1, @: 0110-04-09
                                1. Kawiarenka Ciemna Strona    : 1, @: 0110-01-25
                                1. Laboratorium Senetis    : 1, @: 0109-10-11
                                1. Miasteczko    : 2, @: 0110-03-13
                                1. Rdzeń    : 6, @: 0110-06-20
                                    1. Barbakan    : 2, @: 0110-04-01
                                    1. Szpital Terminuski    : 5, @: 0110-06-20
                            1. Zaczęstwo, obrzeża    : 1, @: 0110-04-16
                            1. Zaczęstwo    : 12, @: 0110-08-22
                                1. Akademia Magii, kampus    : 4, @: 0110-08-22
                                    1. Budynek Centralny    : 3, @: 0110-08-22
                                    1. Domek dyrektora    : 1, @: 0110-02-19
                                1. Arena Migświatła    : 1, @: 0110-02-09
                                1. Cyberszkoła    : 5, @: 0110-08-22
                                1. Hotel Tellur    : 1, @: 0110-02-22
                                1. Kwatera Terminusa    : 1, @: 0110-02-19
                                1. Nieużytki Staszka    : 6, @: 0110-08-22
                                1. Osiedle Ptasie    : 4, @: 0110-06-20
                                1. Sypialnia Szczelińca    : 1, @: 0110-08-22
                                1. Wschodnie Pole Namiotowe    : 1, @: 0110-02-22
                        1. Pustogor    : 1, @: 0110-06-27
                        1. Trzęsawisko Zjawosztup    : 4, @: 0110-09-30
                            1. Głodna Ziemia    : 1, @: 0110-01-29
                            1. Laboratorium W Drzewie    : 1, @: 0110-01-29

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Pięknotka Diakon     | 25 | ((181024-decyzja-minerwy; 181027-terminuska-czy-kosmetyczka; 181230-uwiezienie-saitaera; 190120-nowa-minerwa-w-nowym-swiecie; 190127-ixionski-transorganik; 190202-czarodziejka-z-woli-saitaera; 190206-nie-da-sie-odrzucic-mocy; 190210-minerwa-i-kwiaty-nadziei; 190402-eksperymentalny-power-suit; 190424-budowa-ixionskiego-mimika; 190427-zrzut-w-pacyfice; 190429-sabotaz-szeptow-elizy; 190502-pierwszy-emulator-orbitera; 190503-bardzo-nieudane-porwania; 190828-migswiatlo-psychotroniczek; 190901-polowanie-na-pieknotke; 191103-kontrpolowanie-pieknotki-pulapka; 191112-korupcja-z-arystokratki; 191201-ukradziony-entropik; 200202-krucjata-chevaleresse; 200425-inflitrator-poluje-na-tai-minerwy; 200502-po-co-atakuja-minerwe; 200623-adaptacja-azalii; 201011-narodziny-paladynki-saitaera; 201025-kraloth-w-parku-janor)) |
| Erwin Galilien       | 8 | ((190120-nowa-minerwa-w-nowym-swiecie; 190127-ixionski-transorganik; 190210-minerwa-i-kwiaty-nadziei; 190402-eksperymentalny-power-suit; 190424-budowa-ixionskiego-mimika; 190427-zrzut-w-pacyfice; 190429-sabotaz-szeptow-elizy; 200425-inflitrator-poluje-na-tai-minerwy)) |
| Karla Mrozik         | 6 | ((181230-uwiezienie-saitaera; 190120-nowa-minerwa-w-nowym-swiecie; 190424-budowa-ixionskiego-mimika; 190429-sabotaz-szeptow-elizy; 190503-bardzo-nieudane-porwania; 200623-adaptacja-azalii)) |
| Lucjusz Blakenbauer  | 5 | ((181230-uwiezienie-saitaera; 190429-sabotaz-szeptow-elizy; 190901-polowanie-na-pieknotke; 200311-wygrany-kontrakt; 201025-kraloth-w-parku-janor)) |
| Mariusz Trzewń       | 5 | ((190901-polowanie-na-pieknotke; 191103-kontrpolowanie-pieknotki-pulapka; 191112-korupcja-z-arystokratki; 191201-ukradziony-entropik; 200202-krucjata-chevaleresse)) |
| Tymon Grubosz        | 5 | ((190120-nowa-minerwa-w-nowym-swiecie; 190127-ixionski-transorganik; 190202-czarodziejka-z-woli-saitaera; 190206-nie-da-sie-odrzucic-mocy; 190828-migswiatlo-psychotroniczek)) |
| Olaf Zuchwały        | 4 | ((190424-budowa-ixionskiego-mimika; 190429-sabotaz-szeptow-elizy; 190502-pierwszy-emulator-orbitera; 191103-kontrpolowanie-pieknotki-pulapka)) |
| Adela Kirys          | 3 | ((181024-decyzja-minerwy; 181027-terminuska-czy-kosmetyczka; 190202-czarodziejka-z-woli-saitaera)) |
| Alan Bartozol        | 3 | ((190429-sabotaz-szeptow-elizy; 190502-pierwszy-emulator-orbitera; 190901-polowanie-na-pieknotke)) |
| Ataienne             | 3 | ((190402-eksperymentalny-power-suit; 191112-korupcja-z-arystokratki; 200202-krucjata-chevaleresse)) |
| Damian Orion         | 3 | ((190503-bardzo-nieudane-porwania; 191103-kontrpolowanie-pieknotki-pulapka; 200202-krucjata-chevaleresse)) |
| Diana Tevalier       | 3 | ((190429-sabotaz-szeptow-elizy; 190901-polowanie-na-pieknotke; 200202-krucjata-chevaleresse)) |
| Karolina Erenit      | 3 | ((190127-ixionski-transorganik; 190202-czarodziejka-z-woli-saitaera; 190206-nie-da-sie-odrzucic-mocy)) |
| Kasjopea Maus        | 3 | ((190120-nowa-minerwa-w-nowym-swiecie; 190206-nie-da-sie-odrzucic-mocy; 190210-minerwa-i-kwiaty-nadziei)) |
| Nikola Kirys         | 3 | ((190427-zrzut-w-pacyfice; 190502-pierwszy-emulator-orbitera; 190503-bardzo-nieudane-porwania)) |
| Saitaer              | 3 | ((190127-ixionski-transorganik; 190202-czarodziejka-z-woli-saitaera; 201011-narodziny-paladynki-saitaera)) |
| Aleksander Rugczuk   | 2 | ((190424-budowa-ixionskiego-mimika; 190429-sabotaz-szeptow-elizy)) |
| Arnulf Poważny       | 2 | ((181027-terminuska-czy-kosmetyczka; 190206-nie-da-sie-odrzucic-mocy)) |
| Ernest Kajrat        | 2 | ((190828-migswiatlo-psychotroniczek; 200311-wygrany-kontrakt)) |
| Gabriel Ursus        | 2 | ((200425-inflitrator-poluje-na-tai-minerwy; 201011-narodziny-paladynki-saitaera)) |
| Ignacy Myrczek       | 2 | ((181027-terminuska-czy-kosmetyczka; 191112-korupcja-z-arystokratki)) |
| Józef Małmałaz       | 2 | ((190901-polowanie-na-pieknotke; 191103-kontrpolowanie-pieknotki-pulapka)) |
| Keraina d'Orion      | 2 | ((191201-ukradziony-entropik; 200202-krucjata-chevaleresse)) |
| Kornel Garn          | 2 | ((190206-nie-da-sie-odrzucic-mocy; 190210-minerwa-i-kwiaty-nadziei)) |
| Kreacjusz Diakon     | 2 | ((181230-uwiezienie-saitaera; 200502-po-co-atakuja-minerwe)) |
| Lilia Ursus          | 2 | ((181024-decyzja-minerwy; 191103-kontrpolowanie-pieknotki-pulapka)) |
| Marek Puszczok       | 2 | ((190828-migswiatlo-psychotroniczek; 191103-kontrpolowanie-pieknotki-pulapka)) |
| Persefona d'Jastrząbiec | 2 | ((191112-korupcja-z-arystokratki; 191201-ukradziony-entropik)) |
| Sabina Kazitan       | 2 | ((191112-korupcja-z-arystokratki; 201011-narodziny-paladynki-saitaera)) |
| Talia Aegis          | 2 | ((190828-migswiatlo-psychotroniczek; 200202-krucjata-chevaleresse)) |
| Tomasz Tukan         | 2 | ((200202-krucjata-chevaleresse; 200425-inflitrator-poluje-na-tai-minerwy)) |
| Wiktor Satarail      | 2 | ((190127-ixionski-transorganik; 200623-adaptacja-azalii)) |
| Wojtek Kurczynos     | 2 | ((190127-ixionski-transorganik; 190202-czarodziejka-z-woli-saitaera)) |
| Adam Szarjan         | 1 | ((181024-decyzja-minerwy)) |
| Agaton Ociegor       | 1 | ((201011-narodziny-paladynki-saitaera)) |
| Aleksander Muniakiewicz | 1 | ((201011-narodziny-paladynki-saitaera)) |
| Amanda Kajrat        | 1 | ((200311-wygrany-kontrakt)) |
| Artur Michasiewicz   | 1 | ((190828-migswiatlo-psychotroniczek)) |
| Atena Sowińska       | 1 | ((190210-minerwa-i-kwiaty-nadziei)) |
| Azalia d'Alkaris     | 1 | ((200623-adaptacja-azalii)) |
| Bożymir Szczupak     | 1 | ((190502-pierwszy-emulator-orbitera)) |
| Brygida Maczkowik    | 1 | ((181027-terminuska-czy-kosmetyczka)) |
| Dagmara Doberman     | 1 | ((200311-wygrany-kontrakt)) |
| Eliza Ira            | 1 | ((190429-sabotaz-szeptow-elizy)) |
| Feliks Mirtan        | 1 | ((200502-po-co-atakuja-minerwe)) |
| Halina Sermniek      | 1 | ((200502-po-co-atakuja-minerwe)) |
| Hestia d'Itaran      | 1 | ((191201-ukradziony-entropik)) |
| Hestia d'Janor       | 1 | ((201025-kraloth-w-parku-janor)) |
| Kaja Selerek         | 1 | ((190402-eksperymentalny-power-suit)) |
| Kirył Najłalmin      | 1 | ((190127-ixionski-transorganik)) |
| Konrad Wączak        | 1 | ((200623-adaptacja-azalii)) |
| Ksenia Kirallen      | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Laura Tesinik        | 1 | ((200425-inflitrator-poluje-na-tai-minerwy)) |
| Liliana Bankierz     | 1 | ((200311-wygrany-kontrakt)) |
| Maciej Oczorniak     | 1 | ((201025-kraloth-w-parku-janor)) |
| Malictrix d'Itaran   | 1 | ((200202-krucjata-chevaleresse)) |
| Mateusz Kardamacz    | 1 | ((200202-krucjata-chevaleresse)) |
| Mirela Orion         | 1 | ((190503-bardzo-nieudane-porwania)) |
| Napoleon Bankierz    | 1 | ((190127-ixionski-transorganik)) |
| Nikodem Larwent      | 1 | ((200502-po-co-atakuja-minerwe)) |
| Rafał Roszczeniok    | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Roland Grzymość      | 1 | ((191201-ukradziony-entropik)) |
| Sławomir Muczarek    | 1 | ((190202-czarodziejka-z-woli-saitaera)) |
| Strażniczka Alair    | 1 | ((200425-inflitrator-poluje-na-tai-minerwy)) |
| Tadeusz Sklerzec     | 1 | ((191112-korupcja-z-arystokratki)) |
| Ziemowit Zięba       | 1 | ((200311-wygrany-kontrakt)) |
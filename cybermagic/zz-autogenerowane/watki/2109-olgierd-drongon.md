# Olgierd Drongon
## Identyfikator

Id: 2109-olgierd-drongon

## Sekcja Opowieści

### Ostatnia akcja bohaterki

* **uid:** 210922-ostatnia-akcja-bohaterki, _numer względny_: 9
* **daty:** 0111-12-19 - 0112-01-03
* **obecni:** Antoni Kramer, Arianna Verlen, Elena Verlen, Izabela Zarantel, Jolanta Kopiec, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Olgierd Drongon, OO Żelazko, Roland Sowiński

Streszczenie:

Z Martynem skontaktowała się tien Kopiec prosząc o ostatnią misję zanim Esuriit ją pożre. Martyn zorganizował lot Infernią. Ustabilizował Jolantę, by ta dała radę dolecieć słowami i opowieściami, przy okazji tworząc piękną opowieść o Eterni i eulogię dla Jolanty - bohaterki wojennej - w Sekretach Orbitera (przy okazji wyszła jego przeszłość). Infernia zniszczyła Krwawą Bazę Piratów kosztem dewastacji Żelazka. Jolanta zniszczyła krwawego maga. Sektor jest bezpieczniejszy, Eternia szczęśliwa a Arianna ma profity.

Aktor w Opowieści:

* Dokonanie:
    * Arianna przekonała go do pomocy prosto - "Roland ją podrywa. Halp". By ją ratować i się popisać prawie zniszczył Żelazko. Ale bez tego akcja byłaby niemożliwa.


### Siostrzenica Morlana

* **uid:** 210818-siostrzenica-morlana, _numer względny_: 8
* **daty:** 0111-11-15 - 0111-11-19
* **obecni:** Antoni Kramer, Arianna Verlen, Eustachy Korkoran, Klaudia Stryk, Leona Astrienko, Maria Naavas, Nataniel Morlan, Ofelia Morlan, Olgierd Drongon, OO Netrahina, OO Żelazko, SC Fecundatis, SC Światłodóbr, Tomasz Sowiński

Streszczenie:

Tomasz Sowiński próbuje uratować Ofelię Morlan przed Natanielem Morlanem. Nie ma kogo poprosić a Ofelia służy na Netrahinie; poprosił więc Ariannę. Arianna skanując echo pamięci Jolanty w Inferni zdobywa informacje o programie kosmicznym Orbitera, przekazuje to Kramerowi, wykorzystuje wsparcie Olgierda i robią ćwiczenia Żelazko - Netrahina. Ratują Ofelię przed porwaniem przez koloidowy statek Kruków Kasandry.

Aktor w Opowieści:

* Dokonanie:
    * pomógł Ariannie dostać się na Netrahinę i skutecznie zestrzelił koloidowy statek. Trochę zazdrosny o Ariannę x Tomasza ;-). Pomógł Ariannie za wspólną kolację.


### Listy od fanów

* **uid:** 210630-listy-od-fanow, _numer względny_: 7
* **daty:** 0111-11-10 - 0111-11-13
* **obecni:** Arianna Verlen, Bogdan Anatael, Elena Verlen, Izabela Zarantel, Klaudia Stryk, Michał Teriakin, OE Lord Savaron, Olgierd Drongon, Rafael Galwarn, Remigiusz Falorin, TAI Rzieza d'K1, TAI XT-723 d'K1, TAI Zefiris

Streszczenie:

Na K1 znajduje się tajna baza Eterni, we współpracy z niektórymi elementami K1. Jeden z przekształcanych tam w Pilota chłopców wysłał fanmail do Arianny ("chce się spotkać zanim zginie na froncie"). Arianna z Klaudią znalazły obecność bazy Eterni, pozyskały Eidolona z Eterni (za PR), użyły Eidolona do infiltracji tej bazy, użyły Rziezy do zniszczenia tej bazy a wina spadła na Olgierda (który dla Arianny robił niedaleko manewry). Młodzi Piloci zostali odratowani; nie wiedzą o jaki front chodzi Eterni, ale jedno jest pewne - Eternia szykuje się do wojny kosmicznej. Ale z kim?

Aktor w Opowieści:

* Dokonanie:
    * zrobił ostrą dywersję dla Arianny, przeciążenia Żelazkiem w okolicy tajnej bazy Eterni na K1. Potem na szybką prośbę Arianny ewakuował stamtąd ranną Elenę.
* Progresja:
    * na niego spadło "wykrycie tajnej bazy Eterni na K1" (plan Arianny). Nie ma nic przeciw temu, choć wpakował się w wojnę silnych interesów.


### Ratunkowa misja Goldariona

* **uid:** 210108-ratunkowa-misja-goldariona, _numer względny_: 6
* **daty:** 0111-06-11 - 0111-06-17
* **obecni:** Adam Permin, Aleksander Leszert, Elena Verlen, Feliks Przędz, Kamil Frederico, Klaudia Stryk, Martyn Hiwasser, Oliwia Pietrova, SCA Goldarion, Semla d'Goldarion, SL Uśmiechnięta

Streszczenie:

By zdobyć własne laboratorium w czarnych strefach Kontrolera, Klaudia weszła we współpracę z firmą ArcheoPrzędz i pomogła z Eleną uratować grupkę piratów, którzy bez advancera próbowali eksplorować wrak "Uśmiechniętej" - wraku Luxuritias. Po drodze przelecieli się najbardziej rozpadającym się i najbrudniejszym statkiem cywilnym - Goldarionem - jaki Klaudia kiedykolwiek widziała. Aha, i okazało się, że pojawiają się problemy na linii drakolici - fareil. Bonus: Martyn jako negocjator i mechanizm społeczny XD.

Aktor w Opowieści:

* Dokonanie:
    * AKTOR NIEOBECNY NA SESJI
* Progresja:
    * Klaudia go wrobiła, że niby interesuje się "Uśmiechniętą" - statkiem Luxuritias, który zaginął. Nie wie o tym nic.


### Sympozjum Zniszczenia

* **uid:** 210120-sympozjum-zniszczenia, _numer względny_: 5
* **daty:** 0111-06-03 - 0111-06-07
* **obecni:** Arianna Verlen, Diana Arłacz, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Maria Naavas, Martyn Hiwasser, OA Bakałarz, Olgierd Drongon

Streszczenie:

Maria Naavas próbuje spiknąć Ariannę i Olgierda i do pomocy używa Eustachego. Później - Eustachy, Diana, Klaudia i Elena są na Sympozjum Zniszczenia OA Bakałarz. Próbują przebić się przez anomalną materię. Diana odkrywa słabość owej materii do Esuriit i się Skaża; Skażenie rozlewa się po Bakałarzu i Zespół dostaje opiernicz za niekompetencję (większość na Elenę). Zespół w gniewie, nienawiści i Esuriit fabrykuje Emiter Plagi Nienawiści - nową broń na pokład Inferni, piekielnie niebezpieczną...

Aktor w Opowieści:

* Dokonanie:
    * Maria Naavas wpakowała go z Arianną na "randkę" w kapsule ratunkowej z zimnym life supportem. Arianna grzała magią a jak nie miała sił, Olgierd przejął i trzymał energię.


### Wyścig jako randka?

* **uid:** 201125-wyscig-jako-randka, _numer względny_: 4
* **daty:** 0111-02-08 - 0111-02-13
* **obecni:** Arianna Verlen, Diana Arłacz, Gunnar Brunt, Klaudia Stryk, Maria Naavas, Martyn Hiwasser, Olgierd Drongon

Streszczenie:

Infernia traci części i ekipę remontową, bo Olgierd z Żelazka na niej pasożytuje. W wyniku komedii pomyłek doszło do tego, że Olgierd uznał, że Arianna się w nim podkochuje ale nie wie jak zagadać. Skończyło się pojedynkiem ścigaczy na torze sfabrykowanym przez Klaudię i na anomalicznym Skażeniu Kontrolera Pierwszego. Przez Klaudię, naturalnie.

Aktor w Opowieści:

* Dokonanie:
    * od dawna pracuje nad Aleksandrią Ekstraktywną; wpadł w kolizję z Arianną. W pojedynku ścigaczy przegrał, ale to była doskonała walka.
* Progresja:
    * jest zainteresowany Arianną Verlen. Uważa, że ta się w nim podkochuje, ale nie wie jak zagadać. Ale on też nie wie. Ma o niej (wreszcie!)opinię kompetentnej.


### Wielki Kosmiczny Romans

* **uid:** 200722-wielki-kosmiczny-romans, _numer względny_: 3
* **daty:** 0110-11-12 - 0110-11-15
* **obecni:** Antoni Kramer, Arianna Verlen, Damian Orion, Elena Verlen, Eustachy Korkoran, Julian Muszel, Konrad Wolczątek, Leona Astrienko, Olgierd Drongon, OO Welgat, OO Żelazko, Tadeusz Ursus

Streszczenie:

Pojawiła się plotka, że Arianna, Elena i Eustachy mają romans. Plotka pochodzi od Leony, ale ktoś ją wykorzystał. Rozwiązując plotkę Zespół doszedł do tego, że Elena ma niestabilną energię magiczną i na niektórych upiornie mocno działa, poznali też elementy przeszłości Eleny. Po wyplątaniu Eleny z jednego obsesyjnego adoratora (i wpakowanie plotki że Olgierd z Żelazka aspiruje do jej ręki), Arianna zdecydowała się z Eustachym, Klaudią i Martynem pomóc Kirasjerom w uratowaniu zaginionej Emulatorki z Nocnej Krypty...

Aktor w Opowieści:

* Dokonanie:
    * kapitan Żelazka. Podśmiewuje się z Eustachego - mistrza podrywu. Ale nie chce krzywdy załogi Inferni. Humor mu zepsuło, bo podobno podrywa Elenę z Inferni... coś do rozwiązania.
* Progresja:
    * dowiaduje się, że ubiega się o rękę Eleny Verlen. Zupełnie mu się to nie podoba.


### Problematyczna Elena

* **uid:** 200708-problematyczna-elena, _numer względny_: 2
* **daty:** 0110-10-29 - 0110-11-03
* **obecni:** AK Salamin, Antoni Kramer, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Leszek Kurzmin, Olgierd Drongon, OO Aurelion, OO Żelazko, Persefona d'Infernia

Streszczenie:

Na Infernię trafiła nowa podoficer - podporucznik Elena Verlen, z Castigatora. W konkursie z Eustachym prawie zniszczyli Infernię. Niedługo potem Infernia (podłatana) poleciała do Anomalii Kolapsu uratować Aurelion, który natknął się na anomaliczny kiedyś-krążownik Orbitera Salamin. Arianna uratowała kogo się da i skłoniła Salamin do samozniszczenia; Persefona d'Salamin miała jeszcze dość psychotronicznej świadomości i miłości (?) do swojej martwej kapitan.

Aktor w Opowieści:

* Dokonanie:
    * dowódca Żelazka, niedźwiedziowaty, kompetentny i niebezpieczny. Ma agonalnie niską opinię o Ariannie - nie poradziła sobie z pilotażem na ĆWICZENIACH.


### Olgierd, łowca potworów

* **uid:** 221122-olgierd-lowca-potworow, _numer względny_: 1
* **daty:** 0096-05-13 - 0096-05-18
* **obecni:** Alicja Kirnan, Borys Uprakocz, Jakub Altair, Lutus Saraan, Maciek Kwaśnica, Olgierd Drongon

Streszczenie:

Olgierd szuka 'potworów' - noktian, astorian, orbiterowców, którzy muszą zostać usunięci - jako blackops agent. Na Kartalianie znalazł plotki o serpentisie, ale te plotki się nie spełniły. Natomiast przypadkiem pomógł Lutusowi Saaranowi, pobił ludzi stojących mu na drodze i sprzymierzył się z Lutusem by odzyskać jego 'brata' z niewoli na Neikatis, gdzie - jak się okazuje - są 'potwory' których Olgierd szuka.

Aktor w Opowieści:

* Dokonanie:
    * młodszy (23 lata), jeszcze nie tak rozsądny, poluje na niebezpiecznych ludzi i magów ('potwory'), zwłaszcza noktiańskich i Orbiterian, w ramach black ops z ramienia Orbitera. Szukał 'potwora' na Kartalianie, znalazł noktianina Lutusa Saraana (po drodze obijając wszystko na jego drodze) i wszedł z nim w sojusz - pomoże odzyskać 'brata' Lutusa Saraana z niewoli na Neikatis. Bardzo niebezpieczny; pozornie łagodny niedźwiedź, ale przechodzi 0-100 w moment. Niesamowicie wręcz szczery i bezpośredni.
* Progresja:
    * zrobił PIEKIELNE wrażenie na lokalsach bazy XXX. Złe, bo pokrzywdził w samoobronie. Dobre, bo pomógł.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 8, @: 0112-01-03
    1. Primus    : 8, @: 0112-01-03
        1. Sektor Astoriański    : 8, @: 0112-01-03
            1. Astoria, Orbita    : 5, @: 0111-11-19
                1. Kontroler Pierwszy    : 5, @: 0111-11-19
                    1. Hangary Alicantis    : 2, @: 0110-11-15
                    1. Sektor 43    : 1, @: 0111-02-13
                        1. Tor wyścigowy ścigaczy    : 1, @: 0111-02-13
                    1. Sektor 57    : 1, @: 0111-11-19
                        1. Mordownia    : 1, @: 0111-11-19
            1. Dwupunkt Cztery    : 1, @: 0096-05-18
                1. Kartaliana    : 1, @: 0096-05-18
                    1. Ciemnica    : 1, @: 0096-05-18
            1. Krwawa Baza Piracka    : 1, @: 0112-01-03
            1. Obłok Lirański    : 1, @: 0110-11-03
                1. Anomalia Kolapsu    : 1, @: 0110-11-03

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 7 | ((200708-problematyczna-elena; 200722-wielki-kosmiczny-romans; 201125-wyscig-jako-randka; 210120-sympozjum-zniszczenia; 210630-listy-od-fanow; 210818-siostrzenica-morlana; 210922-ostatnia-akcja-bohaterki)) |
| Klaudia Stryk        | 6 | ((200708-problematyczna-elena; 201125-wyscig-jako-randka; 210120-sympozjum-zniszczenia; 210630-listy-od-fanow; 210818-siostrzenica-morlana; 210922-ostatnia-akcja-bohaterki)) |
| Elena Verlen         | 5 | ((200708-problematyczna-elena; 200722-wielki-kosmiczny-romans; 210120-sympozjum-zniszczenia; 210630-listy-od-fanow; 210922-ostatnia-akcja-bohaterki)) |
| Antoni Kramer        | 4 | ((200708-problematyczna-elena; 200722-wielki-kosmiczny-romans; 210818-siostrzenica-morlana; 210922-ostatnia-akcja-bohaterki)) |
| Eustachy Korkoran    | 4 | ((200708-problematyczna-elena; 200722-wielki-kosmiczny-romans; 210120-sympozjum-zniszczenia; 210818-siostrzenica-morlana)) |
| OO Żelazko           | 4 | ((200708-problematyczna-elena; 200722-wielki-kosmiczny-romans; 210818-siostrzenica-morlana; 210922-ostatnia-akcja-bohaterki)) |
| Leona Astrienko      | 3 | ((200722-wielki-kosmiczny-romans; 210818-siostrzenica-morlana; 210922-ostatnia-akcja-bohaterki)) |
| Maria Naavas         | 3 | ((201125-wyscig-jako-randka; 210120-sympozjum-zniszczenia; 210818-siostrzenica-morlana)) |
| Martyn Hiwasser      | 3 | ((201125-wyscig-jako-randka; 210120-sympozjum-zniszczenia; 210922-ostatnia-akcja-bohaterki)) |
| Diana Arłacz         | 2 | ((201125-wyscig-jako-randka; 210120-sympozjum-zniszczenia)) |
| Izabela Zarantel     | 2 | ((210630-listy-od-fanow; 210922-ostatnia-akcja-bohaterki)) |
| AK Salamin           | 1 | ((200708-problematyczna-elena)) |
| Alicja Kirnan        | 1 | ((221122-olgierd-lowca-potworow)) |
| Bogdan Anatael       | 1 | ((210630-listy-od-fanow)) |
| Borys Uprakocz       | 1 | ((221122-olgierd-lowca-potworow)) |
| Damian Orion         | 1 | ((200722-wielki-kosmiczny-romans)) |
| Gunnar Brunt         | 1 | ((201125-wyscig-jako-randka)) |
| Jakub Altair         | 1 | ((221122-olgierd-lowca-potworow)) |
| Jolanta Kopiec       | 1 | ((210922-ostatnia-akcja-bohaterki)) |
| Julian Muszel        | 1 | ((200722-wielki-kosmiczny-romans)) |
| Konrad Wolczątek     | 1 | ((200722-wielki-kosmiczny-romans)) |
| Leszek Kurzmin       | 1 | ((200708-problematyczna-elena)) |
| Lutus Saraan         | 1 | ((221122-olgierd-lowca-potworow)) |
| Maciek Kwaśnica      | 1 | ((221122-olgierd-lowca-potworow)) |
| Michał Teriakin      | 1 | ((210630-listy-od-fanow)) |
| Nataniel Morlan      | 1 | ((210818-siostrzenica-morlana)) |
| OA Bakałarz          | 1 | ((210120-sympozjum-zniszczenia)) |
| OE Lord Savaron      | 1 | ((210630-listy-od-fanow)) |
| Ofelia Morlan        | 1 | ((210818-siostrzenica-morlana)) |
| OO Aurelion          | 1 | ((200708-problematyczna-elena)) |
| OO Netrahina         | 1 | ((210818-siostrzenica-morlana)) |
| OO Welgat            | 1 | ((200722-wielki-kosmiczny-romans)) |
| Persefona d'Infernia | 1 | ((200708-problematyczna-elena)) |
| Rafael Galwarn       | 1 | ((210630-listy-od-fanow)) |
| Remigiusz Falorin    | 1 | ((210630-listy-od-fanow)) |
| Roland Sowiński      | 1 | ((210922-ostatnia-akcja-bohaterki)) |
| SC Fecundatis        | 1 | ((210818-siostrzenica-morlana)) |
| SC Światłodóbr       | 1 | ((210818-siostrzenica-morlana)) |
| Tadeusz Ursus        | 1 | ((200722-wielki-kosmiczny-romans)) |
| TAI Rzieza d'K1      | 1 | ((210630-listy-od-fanow)) |
| TAI XT-723 d'K1      | 1 | ((210630-listy-od-fanow)) |
| TAI Zefiris          | 1 | ((210630-listy-od-fanow)) |
| Tomasz Sowiński      | 1 | ((210818-siostrzenica-morlana)) |
# Klaudia Stryk
## Identyfikator

Id: 2109-klaudia-stryk

## Sekcja Opowieści

### Lewiatan za Pandorę

* **uid:** 220622-lewiatan-za-pandore, _numer względny_: 84
* **daty:** 0112-07-26 - 0112-07-29
* **obecni:** Arianna Verlen, Eustachy Korkoran, Klaudia Stryk, Maria Naavas, Ola Szerszeń, OO Infernia, OO Pandora, OO Straszliwy Pająk, OO Tivr, Raoul Lavanis, Rzeźnik Parszywiec Diakon

Streszczenie:

PRZESZŁOŚĆ: Ola Szerszeń opiekuje się Eleną w Regeneratorze; okazuje się być biosyntem (zakazane eksperymenty Termii). Rzeźnik Diakon to łagodny i jowialny kapitan Straszliwego Pająka, acz znienawidzony za przeszłość zdrajcy Orbitera oraz Noctis. TERAŹNIEJSZOŚĆ: Lewiatan zostaje zestrzelony torpedą anihilacyjną, dzięki Raoulowi ratujemy wielu ludzi na orbicie Lewiatana. Malictrix d'Pandora zostaje zniszczona przez Lewiatana, ale Klaudia ma jego mikrokopię dzięki Ariannie.

Aktor w Opowieści:

* Dokonanie:
    * poznała sekret Rzeźnika Parszywca Diakona i rozpoznała co Ola Szerszeń robi z Regeneratorem Eleny. By dojść do tego o co chodzi Oli poprosiła o pomoc Marię Naavas co skończyło się katastrofą dla Termii. Przeanalizowała Lewiatana i doszła do tego jak ów działa.
* Progresja:
    * pozyskała bardzo specyficzną cyberformę; "zwierzątko domowe" z Lewiatana z okolic Seibert.


### Lewiatan przy Seibert

* **uid:** 220615-lewiatan-przy-seibert, _numer względny_: 83
* **daty:** 0112-07-23 - 0112-07-25
* **obecni:** Arianna Verlen, Eszara d'Seibert, Eustachy Korkoran, Jonasz Parys, Klaudia Stryk, Ola Szerszeń

Streszczenie:

W okolicach Anomalii Kolapsu obudził się Lewiatan. Ruszył na sortownię Seibert, pożerając kilka jednostek. Grupa Infernia, która miała ćwiczenia (i pokonała koloidową korwetę Zająca 3) dała radę zlokalizować Lewiatana, zabezpieczyć się przed aspektem bazyliszka, zdobyć trochę jego energii do badań i przygotować się do neutralizacji. Jakoś.

Aktor w Opowieści:

* Dokonanie:
    * próbując zrozumieć Lewiatana wpadła w bazyliszka, ale potem zbudowała worma anty-bazyliszkowego i zainfekowała nim wszystkie maszyny. Też - zmieniła parametry ćwiczeń by kapitan Zająca 3 przegrał z Infernią.


### EtAur - dziwnie ważny węzeł

* **uid:** 220706-etaur-dziwnie-wazny-wezel, _numer względny_: 82
* **daty:** 0112-07-15 - 0112-07-17
* **obecni:** Aleksandra Termia, Eustachy Korkoran, Klaudia Stryk, Maria Naavas

Streszczenie:

Maria zainteresowała się planem adm. Termii wskazującym na to, że ta zdradza Orbiter z Syndykatem Aureliona. Eustachy (do którego przyszła) zapytał Termię wprost i doszedł do tego, że Termia szuka koloidowej bazy Aureliona na księżycu Elang swymi bezdusznymi metodami. Eustachy i Klaudia wskazali Termii, że baza EtAur jest potencjalnie punktem kontaktowym Syndykatu z Astorią i tu jest ryzyko. Eustachy doprowadził do kontaktu Termia - EtAur, ale też gra na współpracę Inferni i EtAur.

Aktor w Opowieści:

* Dokonanie:
    * zaproszona przez Eustachego do konspiracji Termii wskazała jak istotna jest baza EtAur dla Orbitera i Syndykatu. Potem przeanalizowała czego EtAur pragnie by sojusz EtAur z Infernią był jak najlepszy (i by jak najbardziej pomóc sojuszowi EtAur - Termia).


### Ratujemy porywaczy Eleny

* **uid:** 220610-ratujemy-porywaczy-eleny, _numer względny_: 81
* **daty:** 0112-07-11 - 0112-07-13
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Hestia d'Atropos, Jakub Bulgocz, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Raoul Lavanis

Streszczenie:

Elena jest już zbyt zdestabilizowana by funkcjonować niezależnie, więc wpierw zajęli się nią w Laboratorium Kranix a po zbudowaniu dla niej Containment Chamber przesłali ją do stacji medycznej Atropos do regeneracji. Stamtąd ją i Martyna porwał Syndykat Aureliona (ale Martyn przewidział JAKIŚ problem i ostrzegł Klaudię). Elena by zniszczyła okręt napastników, ale Zespół skutecznie odkrył problemy na Atropos, przesłuchali dowódcę, zneutralizowali miragenta i dotarli do okrętu Aureliona zdobywając jeńców i zatrzymując Elenę przed przebudzeniem i Spustoszeniem statku.

Aktor w Opowieści:

* Dokonanie:
    * odszyfrowuje dane Martyna, dochodzi do tego że to miragent, wspiera TAI Hestia d'Atropos i poznaje od niej prawdę a potem oblicza jak dotrzeć do statku niewolniczego zanim Elena się przebudzi. Absolutna MVP operacji.


### EtAur i przynęta na Kryptę

* **uid:** 220330-etaur-i-przyneta-na-krypte, _numer względny_: 80
* **daty:** 0112-04-27 - 0112-04-30
* **obecni:** Arianna Verlen, Eustachy Korkoran, Klaudia Stryk, Lamia Akacja, Leszek Czarban, Maria Naavas, Raoul Lavanis

Streszczenie:

Okazało się, że "potwór" to były faeril z Neikatis zarażony bronią biologiczną. Próbował doprowadzić do katastrofy by Orbiter lub Eternia zabrali się za leczenie tego cholerstwa, pomagał mu Antoni, p.o. TAI na EtAur. Maria doszła do biostruktury, Klaudia przejęła systemy bazy, Eustachy przechwycił gościa, Arianna zdobyła bazę politycznie. A Klaudia zrobiła raport, by Orbiter nie atakował więcej neutralnych i potencjalnie sojuszniczych sił nie-Orbiterowych w kosmosie. Jako, że Lamii nic się nie stało, Melania jest szczęśliwa.

Aktor w Opowieści:

* Dokonanie:
    * wbiła się do Barbakanu i dostała obraz sytuacji i defensive tools. Odkryła, że Antoni ("biologiczny PO AI") pomaga potworowi. Zastawia pułapkę z Eustachym.
* Progresja:
    * NIECHĘĆ ze strony purystów Orbitera. PRO-LUDZKA grupa Orbitera, Eternia i Aurum uważają Infernię za siłę której wiele zawdzięczają.


### Potwór czy choroba na EtAur?

* **uid:** 220316-potwor-czy-choroba-na-etaur, _numer względny_: 79
* **daty:** 0112-04-24 - 0112-04-26
* **obecni:** Arianna Verlen, Dominika Perikas, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Maria Naavas, Melania Akacja, Suwan Chankar, Tymoteusz Czerw

Streszczenie:

Na stacji EtAur pojawiły się poważne problemy - potwór? Choroba? Suwan próbuje ustabilizować co się da, ale nic nie może zrobić. Arianna poproszona o pomoc przybyła, ale nikt nie chce z nią praktycznie współpracować. Klaudia doszła do przyczyny problemów - choroba, ale potwór jest wektorem. Arianna doszła do transmisji po kanałach magicznych. Świetnie.

Aktor w Opowieści:

* Dokonanie:
    * włamuje się do roboczego TAI (nie Nephthys) i zakłada backdoor; dowiaduje się o przeszłości tej bazy.


### Upadek Eleny

* **uid:** 220309-upadek-eleny, _numer względny_: 78
* **daty:** 0112-04-03 - 0112-04-09
* **obecni:** Annika Pradis, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Tomasz Kaltaben

Streszczenie:

Keldan Voss została ustabilizowana. Annika będzie mieć szansę powodzenia - zostaje przełożoną, z łącznikiem z keldanitami Mateusem. Odbudowany jej link z pallidanami. Pallidanie mają ogólnie "spokój". Tomasz Kaltaben będzie ją wspierał a on sam dostanie 3 magów Orbitera co go wspierają (i są agentami). Elena niestety zabrała trzy życia odzyskując naturalną urodę. Klaudia ciężko pracuje by zbudować dla Eleny Detox Chamber - ona nie jest OK.

Aktor w Opowieści:

* Dokonanie:
    * zarządzała komunikacją, sygnałami itp. Była w cieniu. I projektowała mechanizm, dzięki któremu da się wyczyścić Elenę i zapewnić, że nie stanie się potworem.


### Stabilizacja Keldan Voss

* **uid:** 220223-stabilizacja-keldan-voss, _numer względny_: 77
* **daty:** 0112-03-30 - 0112-04-02
* **obecni:** Annika Pradis, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, SP Światło Nadziei, Szczepan Kaltaben, Tomasz Kaltaben

Streszczenie:

Ewakuacja pallidan na Keldan Voss się udało, acz Eustachy musiał zniszczyć anomalnych napastników z Mgły i naprawić morale Anniki. Elenie (słyszącej szepty) udało się zniszczyć BIA i wprowadzić na jej miejsce amalgamat szamana. Arianna ma polityczną kontrolę nad sytuacją a dzięki Eustachemu udało się sytuację ustabilizować militarnie. Dzięki Klaudii wiedzą co i jak. Teraz już tylko zostaje zostawić Keldan Voss w stabilnej formie.

Aktor w Opowieści:

* Dokonanie:
    * wykorzystała Elenę jako filtr na sygnały z mgieł, dzięki czemu uratowała sporo istnień i zrozumiała co się tu dzieje.


### Polityka rujnuje Pallidę Voss

* **uid:** 220216-polityka-rujnuje-pallide-voss, _numer względny_: 76
* **daty:** 0112-03-27 - 0112-03-29
* **obecni:** Annika Pradis, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Mateus Sarpon, Raoul Lavanis, SP Pallida Voss, Szczepan Kaltaben

Streszczenie:

Arianna we współpracy z Mateusem planują uratować zakładników pallidańskich od radykałów keldańskich. Ale by to osiągnąć Eustachy i Klaudia infiltrują wpierw Pallidę Voss, bo okazuje się, że Annikę wrabiają jej własni podwładni. Klaudia zdobyła wszystkie potrzebne dowody, Eustachy chciał ratować ludzi... i Paradoks (po sabotowaniu generatorów Memoriam) spowodował straszne straty w ludziach i przesunął Pallidę Voss w Anomalię Kosmiczną. Oops. Ale dzięki drakolitom (keldanitom) udało się sporo ludzi uratować.

Aktor w Opowieści:

* Dokonanie:
    * infiltruje Pallidę Voss jako audytorka, zdobyła 100% potrzebnych informacji i twardych dowodów. Potem - kontrolowała ewakuację Pallidy Voss i dzięki niej udało się uratować więcej niż się zdawało.


### Sekrety Keldan Voss

* **uid:** 220202-sekrety-keldan-voss, _numer względny_: 75
* **daty:** 0112-03-24 - 0112-03-26
* **obecni:** Annika Pradis, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Mateus Sarpon

Streszczenie:

Zespół skutecznie ewakuował pallidan z Keldan Voss, po drodze orientując się że część porwanych przez Mgły osób została Skażona i zmieniona w Kroczących w Mgle. Eustachy wydobył jednego do badań dla Marii. Co ciekawe - pallidanie i keldanici widzą zupełnie inne wersje historii ("snajper" vs "porwania") a do tego Mgły powodują jeszcze większą nierzeczywistość i złudzenia. A Arianna politycznie a Klaudia naukowo dochodzą do tego co jest prawdą...

Aktor w Opowieści:

* Dokonanie:
    * Odkryła specyfikę mgieł Keldan Voss - energie z vitium. Zidentyfikowała też obecność BIA klasy Reitel zamiast Hestii. I doszła do historii tego miejsca - noktianie vs drakolici a potem sojusz.


### Keldan Voss, kolonia Saitaera

* **uid:** 220126-keldan-voss-kolonia-saitaera, _numer względny_: 74
* **daty:** 0112-03-19 - 0112-03-22
* **obecni:** Annika Pradis, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Kormonow Voss, Mateus Sarpon, OO Kastor, SP Pallida Voss, Szczepan Kaltaben, Zygfryd Maus

Streszczenie:

Kramer wysłał załogę Inferni na Keldan Voss (stację pod kontrolą pallidan i źródło kryształów vitium), bo tam mogą więcej wiedzieć o anomalizacji ludzi i magów. Co więcej, jest poważny problem polityczny i techniczny - pallidanie zwalczają lokalnych drakolitów i kolonia ma problem typu wojna domowa. Na miejscu okazało się, że są CO NAJMNIEJ dwa stronnictwa a Klaudia uznała, że lokalna Hestia nie jest Hestią. Więc czym jest?

Aktor w Opowieści:

* Dokonanie:
    * Wyłapywała nieścisłości w historii Pallidan i starała się zrozumieć sytuację. Doszła do tego, że lokalna Hestia to chyba nie Hestia. Więc co?


### To nie pułapka na Nereidę...

* **uid:** 220105-to-nie-pulapka-na-nereide, _numer względny_: 73
* **daty:** 0112-02-28 - 0112-03-04
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Izabela Zarantel, Kamil Lyraczek, Klaudia Stryk, Lutus Amerin, Maria Naavas, Natalia Aradin, Saitaer

Streszczenie:

Plan - dorwanie tajemniczych koloidowych korwet. Wiemy, że to pułapka, więc zróbmy kontr-pułapkę. Ale okazało się, że to nie pułapka na Nereidę a na Infernię. Standardowe badania wprowadziły w Infernię (Dianę) memetic payload i Diana prawie wpadła pod kontrolę Saitaera - odrzucenie kokonu z Natalią sprawiło, że udało się ciężko uszkodzoną Infernię uratować. Zwalczając ixion Arianna sięgnęła do Esuriit przez Elenę; Infernia by zginęła gdyby nie samobójcza akcja Izabeli i Kamila. Elena wchłonęła tyle energii ile była w stanie, przekraczając swój Wzór. ALE - Infernia przetrwała. Przy okazji, Natalia się wykluła. Ani osoba ani statek kosmiczny - mordercza anomalia kosmiczna.

Aktor w Opowieści:

* Dokonanie:
    * Desperacko próbując ratować Infernię działała jako super-katalistka przepuściła przez siebie więcej energii niż powinna. By ratować Infernię i wszystkich innych przekierowała energię w Izabelę, niszcząc ją.


### Kult Saitaera w Neotik

* **uid:** 211222-kult-saitaera-w-neotik, _numer względny_: 72
* **daty:** 0112-02-24 - 0112-02-25
* **obecni:** Adam Szarjan, Arianna Verlen, Diana d'Infernia, Elena Verlen, Eustachy Korkoran, Izabela Zarantel, Kasandra Destrukcja Diakon, Klaudia Stryk, Lutus Amerin, Maciek Kalmarzec, Maria Naavas

Streszczenie:

Klaudia wróciła z bronią anty-Serenitową od Tosena, Arianna z feromonami Marii nadała Inferni nową kulturę - miłośnicy lolitek x kultyści x noktianie. Elena uziemiona w Inferni zinfiltrowała Stocznię ukryta przez Klaudię i odkryła niższe Spustoszenie + kult Saitaera. Nereida jest czysta od Saitaera; przetransferowana na Infernię dla Eleny i uzbrojona. Klaudia ma listę jednostek potencjalnie w rękach Saitaera / Spustoszonych. Przygotowania do kontrataku skończone.

Aktor w Opowieści:

* Dokonanie:
    * od Tosena pozyskała eksperymentalną broń ręczną anty-Serenitową do walki z ixionem; prowadziła link Eleny x Inferni, ukryła Elenę przed Hestią d'Neotik i pozyskała listę potencjalnie Skażonych jednostek interaktujących z Neotik.
* Progresja:
    * ma ze Stoczni Neotik listę jednostek podejrzanych o to, że zostały zarażone albo przez Kult Saitaera albo przez niższe Spustoszenie.


### O krok za daleko

* **uid:** 211208-o-krok-za-daleko, _numer względny_: 71
* **daty:** 0112-02-19 - 0112-02-20
* **obecni:** Adam Szarjan, Arianna Verlen, Diana d'Infernia, Elena Verlen, Eustachy Korkoran, Hestia d'Neotik, Jarosław Szarjan, Klaudia Stryk, Leona Astrienko, OO Infernia, Wawrzyn Rewemis

Streszczenie:

Klaudia doszła do tego, że Hestia d'Neotik oszukuje Stocznię; w okolicy są niewielkie statki koloidowe o których Stocznia nie wie. Arianna zintegrowała Eustachego z Infernią ixiońsko, niestety anomalizując Infernię. Elena ciężko Skażona ixionem i (w swoim mniemaniu) porzucona przez Eustachego. Eustachy ujarzmiając Infernię rozbijał ją o różne asteroidy, co powoduje śmierć części załogi. Infernia ma zerowe morale, ale jest opanowana. Przyjazna Anomalia Kosmiczna z tymczasową bazą w Stoczni Neotik.

Aktor w Opowieści:

* Dokonanie:
    * zorientowała się, że Hestia d'Neotik oszukuje Szarjana i Neotik. Wyekstraktowała skutecznie próbkę ixiońską ze zniszczonej Nereidy. Wydobyła cenne dane ze stoczni Neotik i wpisała je w Infernię i w swoje prywatne dane. Gdy Eustachy ujeżdżał Infernię, ratowała ludzi jak była w stanie. Czuje się chora - poszli za daleko w poszukiwaniu mocy.
* Progresja:
    * ma dostęp do aktywnej i potężnej próbki ixiońskiej ze zniszczonej Nereidy; contained.
    * ma backdoor do Hestii d'Neotik i do niejawnych planów nad którymi pracuje Stocznia Neotik.


### Prototypowa Nereida Natalii

* **uid:** 211124-prototypowa-nereida-natalii, _numer względny_: 70
* **daty:** 0112-02-14 - 0112-02-18
* **obecni:** Adam Szarjan, Arianna Verlen, Arkadia Verlen, Elena Verlen, Eustachy Korkoran, Izabela Zarantel, Klaudia Stryk, Leona Astrienko, Maria Naavas, Natalia Aradin, OO Infernia, Roland Sowiński, Wawrzyn Rewemis

Streszczenie:

Kampania reklamowa "dołącz do Inferni" zrobiona przez Izę przy użyciu Eustachego jako modela - sukces. Klaudia zdobyła z tego nowych załogantów (acz to jeszcze potrwa). Szarjan uwolnił Klaudię z zarzutów, potrzebuje pomocy w stoczni - jego przyjaciółka Natalia ma problem z prototypem myśliwca klasy Nereida i została ixiońsko zintegrowana. Infernia próbowała Nereidę uratować, ale niestety skończyło się to poważnym uszkodzeniem Nereidy i stworzeniem ixiońskiego kokonu, z którego Natalia musi się wykluć...

Aktor w Opowieści:

* Dokonanie:
    * wybrała z lejka rekrutacyjnego WŁAŚCIWYCH przyszłych członków załogi, efektywnie odbudowując Infernię. Uwolniona z zarzutów przez Adama Szarjana, próbowała uratować Natalię Aradin przed śmiercią i niestety jedyne co mogła zrobić to ixioński kokon - Natalia się wykluje jako "coś".
* Progresja:
    * poparzona magicznie i mentalnie przez tydzień; za silna integracja z umierającą Persefoną itp.


### Porwany Trismegistos

* **uid:** 211117-porwany-trismegistos, _numer względny_: 69
* **daty:** 0112-02-09 - 0112-02-11
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Izabela Zarantel, Jamon Korab, Kalira d'Trismegistos, Klara Gwozdnik, Klaudia Stryk, Leona Astrienko, Maria Naavas, Mira Anastel, OO Tivr, Roland Sowiński, SC Trismegistos, Zygfryd Maus

Streszczenie:

Wiadomość od pirata że porwał Trismegistos poderwała Ariannę i Tivr. Historia nie trzymała się kupy. Załoga Inferni (Tivru) zorientowała się, że coś jest nie tak - opowieść o statku kupieckim który pozbył się miragenta brzmiała DZIWNIE. Okazało się, że Trismegistos jest statkiem kaperskim. Gdy Trismegistos zażądał poddania się Tivru Klaudia zrobiła sygnał "lol leci na Was Serenit" i TAI Trismegistosa spanikowała. Wolna TAI, współpracująca z fareilem i drakolitką. Wolny dystrykt. Arianna dała im żyć - ale odzyskała porwanych arystokratów i komory adaptacji.

Aktor w Opowieści:

* Dokonanie:
    * udała przed Kalirą d'Trismegistos, że do niej zbliża się Serenit. Zmusiła TAI do negocjacji a potem ukryła że Tivr nie zniszczył Trismegistosa. Aha, aresztowali ją.


### Romans dzięki Esuriit

* **uid:** 211110-romans-dzieki-esuriit, _numer względny_: 68
* **daty:** 0112-02-07 - 0112-02-08
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Karol Reichard, Klaudia Stryk, Leona Astrienko, Maria Naavas, Wawrzyn Rewemis

Streszczenie:

Infernia nie ma dość załogi, by móc działać, więc Termia przekazuje Ariannie Tivr. Leona nie może sobie wybaczyć, że jej nie było i to się stało, więc się wypisuje z pomocą Eustachego (co powoduje plotki Leona x Eustachy). Elena ucieka do Sektora 49 na K1 by być samotna; Arianna organizuje ostre poszukiwania. Eustachy znajduje Elenę by ją pocieszyć. Elena w końcu przyznaje Eustachemu, że go kocha. Maria Naavas z woli Martyna tymczasowo dołącza do załogi Arianny.

Aktor w Opowieści:

* Dokonanie:
    * Napisała serię listów o śmierci załogantów Inferni by Arianna nie musiała. Potem zorganizowała gry wojenne, by ukryć fakt, że Elena zniknęła w czeluściach K1. Poinformowała Marię o sytuacji z Martynem (bo była dead man's hand Martyna - jakby coś mu było, Klaudia ma powiedzieć Marii). Aha, znalazła Innego Medyka Na Infernię.


### Rzieza niszczy Infernię

* **uid:** 211027-rzieza-niszczy-infernie, _numer względny_: 67
* **daty:** 0112-01-22 - 0112-01-27
* **obecni:** Arianna Verlen, Elena Verlen, Flawia Blakenbauer, Klaudia Stryk, Martyn Hiwasser, Otto Azgorn, TAI Rzieza d'K1

Streszczenie:

Infernia wraca z Mevilig. Rzieza chce wyczyścić im pamięć o sobie. Arianna się opiera. Rzieza dowiaduje się o Ataienne. Arianna budzi Elenę po sympatii, Elena sprowadza Esuriit do laboratorium dekontaminacyjnego K1. Dużo śmierci. Martyn próbuje to opanować - budzi swoje simulacrum. Arianna i Klaudia opanowują przy pomocy Rziezy sytuację. 37% załogi Inferni nie żyje, Flawia wyssana w kosmos, Elena złamana, Martyn w szpitalu.

Aktor w Opowieści:

* Dokonanie:
    * most notable action - surfowanie po blacie stołu by zapobiec wyssaniu załogantów Inferni w kosmos. Opracowała też plan jak uratować Infernię od Esuriit - niestety, Martyn nie dał rady go wykonać. Współpracowała ze Rziezą by uratować kogo się da i by Rzieza miał pretekst do nie zabijania Inferni.


### Kurczakownia

* **uid:** 211020-kurczakownia, _numer względny_: 66
* **daty:** 0112-01-18 - 0112-01-20
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Flawia Blakenbauer, Kamil Lyraczek, Klaudia Stryk, Martyn Hiwasser, OO Infernia, Otto Azgorn, Vigilus Mevilig

Streszczenie:

By uratować jak najwięcej osób w Sektorze Mevilig, Eustachy zaprojektował Kurczakownię - maszynę do oddzielania mechanicznego głów i rdzenia od reszty człowieka. Arianna starła się ze Zbawicielem-Niszczycielem jako aspekt Zbawiciela, wspierana przez Działo Rozpaczy (Eustachy). Udało im się sporo uratować (40% populacji Planetoidy Kalarfam), acz załoga Inferni będzie mieć koszmary senne. Klaudia zebrała dość danych, by zapobiec naturalnej manifestacji Zbawiciela-Niszczyciela w Sektorze Astoriańskim i zrobić kontrakcję jak co.

Aktor w Opowieści:

* Dokonanie:
    * opracowała z Martynem jak zrobić kurczakowanie-rekurczakowanie na poziomie bioskładników. Opracowała, jak powstrzymać Zbawiciela-Niszczyciela jakby ten zamanifestował się w Sektorze Astoriańskim. Uniemożliwiła mu samoistne pojawienie się w tamtym sektorze.


### Szara nawałnica

* **uid:** 211013-szara-nawalnica, _numer względny_: 65
* **daty:** 0112-01-12 - 0112-01-17
* **obecni:** Aleksandra Termia, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Martyn Hiwasser, Otto Azgorn, Remigiusz Błyszczyk, TAI Rzieza d'K1

Streszczenie:

Zespół przygotował się na wejście do Mevilig. Rzieza odkrył jak działają Piranie i że tamten sektor jest opanowany przez TAI 5+ generacji. Niestety, Infernia musiała uciekać przed Rziezą. Na miejscu Eustachy znalazł skuteczny sposób przemykania między Piraniami, dotarli do planetoidy Kalarfam gdzie znaleźli bazę ludzi. Mechaniczny "smok" został zniszczony przez Infernię, pobrali lokalsów i Arianna przekonała ich do zmiany kultu. Aha, anomalny wybuch torpedy anihilacyjnej Arianna przekierowała na planetę, wypowiadając ostrą wojnę TAI 5+ generacji...

Aktor w Opowieści:

* Dokonanie:
    * zrozumiała na czym polega Pirania i TAI w Mevilig, ale skonfliktowała się z Rziezą. Teraz musi go przechytrzyć...


### Grupa Ekspedycyjna Kellert

* **uid:** 210929-grupa-ekspedycyjna-kellert, _numer względny_: 64
* **daty:** 0112-01-07 - 0112-01-10
* **obecni:** Adam Nerawol, Aleksandra Termia, Antoni Kramer, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Olena Orion, OO Infernia, OO Omega Septius, Otto Azgorn, TAI Marszałek Grzmotoszpon Trzeci, Vigilus Mevilig

Streszczenie:

Termia wysłała siły do sektora "Noviter", ale to był inny sektor (Mevilig). Straciła bezzałogowce, potem grupę ekspedycyjną Kellert. Kramer wysłał Infernię zakoloidowaną; okazało się, że ten sektor ma "strażnika Esuriit" zrobionego przez ludzi i grupę Piranii, sterowanych przez TAI 4 poziomu. Inferni udało się zebrać dane i wrócić z siedmioma uratowanymi członkami OO Savera, gdzie większość załogi została corruptowana przez Vigilusa...

Aktor w Opowieści:

* Dokonanie:
    * analizując dane z jednostek zwiadowczych na K1 odkryła, że przeciwnik wpływa na TAI. Pasywnie czujnikami znalazła Saverę w sektorze Mevilig. Zapewniła psychotronika Adama na pewien czas na Infernię.


### Ostatnia akcja bohaterki

* **uid:** 210922-ostatnia-akcja-bohaterki, _numer względny_: 63
* **daty:** 0111-12-19 - 0112-01-03
* **obecni:** Antoni Kramer, Arianna Verlen, Elena Verlen, Izabela Zarantel, Jolanta Kopiec, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Olgierd Drongon, OO Żelazko, Roland Sowiński

Streszczenie:

Z Martynem skontaktowała się tien Kopiec prosząc o ostatnią misję zanim Esuriit ją pożre. Martyn zorganizował lot Infernią. Ustabilizował Jolantę, by ta dała radę dolecieć słowami i opowieściami, przy okazji tworząc piękną opowieść o Eterni i eulogię dla Jolanty - bohaterki wojennej - w Sekretach Orbitera (przy okazji wyszła jego przeszłość). Infernia zniszczyła Krwawą Bazę Piratów kosztem dewastacji Żelazka. Jolanta zniszczyła krwawego maga. Sektor jest bezpieczniejszy, Eternia szczęśliwa a Arianna ma profity.

Aktor w Opowieści:

* Dokonanie:
    * na prośbę Martyna zrobiła raport dla Kramera przekonywujący go do autoryzacji akcji. To bardzo dobry raport jest.


### Admirał Gruby Wieprz

* **uid:** 211114-admiral-gruby-wieprz, _numer względny_: 62
* **daty:** 0111-12-11 - 0111-12-16
* **obecni:** Adalbert Brześniak, Izabela Zarantel, Klaudia Stryk, Rafael Galwarn, Roman Panracz, Rzieza d'K1, Sabina Kazitan

Streszczenie:

Klaudia na K1 dostała dziwny SOS. Okazało się, że pochodzi z K1 - z multivirtu. Virtsystem gry Gwiezdny Podbój uzyskał życie. Klaudia zmontowała akcję ratunkową i z Adalbertem (współpracujący z Sabiną Kazitan), Izą i Romanem zaczęli przenosić TAI do ewakuacji. Niestety, Klaudia przypadkiem Skaziła Połączony Rdzeń na K1 (śledztwo i poważne zarzuty) a Iza chcąc dać Wieprzowi głos trafiła na celownik Rziezy, który ją ośmieszył a Wieprza pokazał jako żywą TAI - psychopatę...

Aktor w Opowieści:

* Dokonanie:
    * Skaziła "Serenitem" Połączony Rdzeń K1, po czym zmontowała Adalbertem i Izą + Romanem plan na ewakuację virtsystemu. Przeprowadziła go mimo Rziezy i Grubego Wieprza.
* Progresja:
    * pewien cios w relacje z Rafaelem Galwarnem; oczekiwałby, że ona zrobi to co zgodne z zasadami K1 a nie dobre dla wolnych TAI. Galwarn dalej współpracuje, ale nie ufa w jej intencje.


### Stabilizacja Bramy Eterycznej

* **uid:** 210901-stabilizacja-bramy-eterycznej, _numer względny_: 61
* **daty:** 0111-12-05 - 0111-12-08
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Flawia Blakenbauer, Gilbert Bloch, Janus Krzak, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Medea Sowińska, OO Kanagar, OO Netrahina, OO Trasman, Seweryn Atanair

Streszczenie:

Infernia wróciła z innymi jednostkami naprawić Bramę Kariańską. Eustachy ufortyfikował i uniewidocznił pintki; Arianna pozyskała dane z reanimowanego magią krążownika Orbitera. Przez Bramę przeszła ON Catarina; Klaudia ją zlokalizowała i wystawiła Netrahinie i Niobe. Niestety, Arianna hiperlinkowała się z ON Spatium Gelida. Dla nich minęło tylko kilka godzin, dla nas - kilkadziesiąt lat...

Aktor w Opowieści:

* Dokonanie:
    * przy współpracy z Medeą zdobyła SPORO zasobów biurokracją; używając Bramy, Janusa i kodów zlokalizowała ON Catarina i skłoniła Catarinę do zrobienia błędu. Acz było Klaudii żal śmierci BII.
* Progresja:
    * jawnie współpracuje biurokratycznie z siłami specjalnymi Medei; dla niektórych to SUPER, dla innych to poważny problem.


### Uszkodzona Brama Eteryczna

* **uid:** 210825-uszkodzona-brama-eteryczna, _numer względny_: 60
* **daty:** 0111-11-30 - 0111-12-03
* **obecni:** AK Nocna Krypta, Arianna Verlen, Diana d'Infernia, Elena Verlen, Eustachy Korkoran, Flawia Blakenbauer, Gilbert Bloch, Klaudia Stryk, Medea Sowińska, ON Spatium Gelida, OO Infernia, OO Mfumo

Streszczenie:

Infernia została przechwycona przez Medeę lecąc na Anomalię Kolapsu i przekierowana na uszkodzoną Bramę Eteryczną (gdzie, jak się okazuje, są pasożyty i flota noktiańska która nie doleciała). Na miejscu Infernia uniknęła efemeryd i channelując Kryptę uratowała wszystkich efemerydami. Gdy Krypta pojawiła się na serio, Infernia zwiała paląc silniki. Statki noktiańskie w Bramie mają tether na Infernię.

Aktor w Opowieści:

* Dokonanie:
    * używając systemów skanujących Inferni i dron odkryła których ludzi z wraków da się jeszcze uratować - na przestrzeni wszystkiego dookoła Bramy.


### Nieudana infiltracja Inferni

* **uid:** 210616-nieudana-infiltracja-inferni, _numer względny_: 59
* **daty:** 0111-11-22 - 0111-11-27
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Flawia Blakenbauer, Izabela Zarantel, Kamil Lyraczek, Klaudia Stryk, Leona Astrienko, Marian Tosen, OO Opresor, Roland Sowiński

Streszczenie:

Marian Tosen poprosił Infernię o to, by sprawdzili co się dzieje w Anomalii Kolapsu - podejrzewa anomalie memetyczne, skoro Orbiter traci tam statki  (m.in. OO Opresor) i pojawiają się jednostki nieoznaczonej frakcji. Tymczasem Roland Sowiński chce na pokład. Arianna wybiła mu to z głowy, ale wykryli agentkę wśród jego guwernantek - Flawię Blakenbauer, która miała się nim opiekować i jest trochę zniewolona przez Sowińskich. By zadośćuczynić po tym co Elena zrobiła i trochę Flawii pomóc, Flawia dołączyła do Inferni. "Oficjalnie" udało jej się Infernię zinfiltrować...

Aktor w Opowieści:

* Dokonanie:
    * odkryła tożsamość Flawii (ukrytej pod zmienioną formą) na podstawie wiedzy z dokumentów Orbitera i pamięci Eleny. Zaproponowała by Flawia dołączyła do Inferni.


### Siostrzenica Morlana

* **uid:** 210818-siostrzenica-morlana, _numer względny_: 58
* **daty:** 0111-11-15 - 0111-11-19
* **obecni:** Antoni Kramer, Arianna Verlen, Eustachy Korkoran, Klaudia Stryk, Leona Astrienko, Maria Naavas, Nataniel Morlan, Ofelia Morlan, Olgierd Drongon, OO Netrahina, OO Żelazko, SC Fecundatis, SC Światłodóbr, Tomasz Sowiński

Streszczenie:

Tomasz Sowiński próbuje uratować Ofelię Morlan przed Natanielem Morlanem. Nie ma kogo poprosić a Ofelia służy na Netrahinie; poprosił więc Ariannę. Arianna skanując echo pamięci Jolanty w Inferni zdobywa informacje o programie kosmicznym Orbitera, przekazuje to Kramerowi, wykorzystuje wsparcie Olgierda i robią ćwiczenia Żelazko - Netrahina. Ratują Ofelię przed porwaniem przez koloidowy statek Kruków Kasandry.

Aktor w Opowieści:

* Dokonanie:
    * udowodniła, że Jolanta, Tomasz, Ofelia faktycznie ratują Eternian przed Morlanem. Potem odblokowała TAI Netrahiny i znalazła koloidowy statek dla Olgierda do zestrzelenia.


### Listy od fanów

* **uid:** 210630-listy-od-fanow, _numer względny_: 57
* **daty:** 0111-11-10 - 0111-11-13
* **obecni:** Arianna Verlen, Bogdan Anatael, Elena Verlen, Izabela Zarantel, Klaudia Stryk, Michał Teriakin, OE Lord Savaron, Olgierd Drongon, Rafael Galwarn, Remigiusz Falorin, TAI Rzieza d'K1, TAI XT-723 d'K1, TAI Zefiris

Streszczenie:

Na K1 znajduje się tajna baza Eterni, we współpracy z niektórymi elementami K1. Jeden z przekształcanych tam w Pilota chłopców wysłał fanmail do Arianny ("chce się spotkać zanim zginie na froncie"). Arianna z Klaudią znalazły obecność bazy Eterni, pozyskały Eidolona z Eterni (za PR), użyły Eidolona do infiltracji tej bazy, użyły Rziezy do zniszczenia tej bazy a wina spadła na Olgierda (który dla Arianny robił niedaleko manewry). Młodzi Piloci zostali odratowani; nie wiedzą o jaki front chodzi Eterni, ale jedno jest pewne - Eternia szykuje się do wojny kosmicznej. Ale z kim?

Aktor w Opowieści:

* Dokonanie:
    * gdy standardowe szukanie po systemach K1 zawiodło, udała się w virtsferę i znalazła utylitarne TAI od komunikacji które wskazało gdzie może być baza Eterni na K1. Potem napuściła na bazę... Rziezę.


### Morderstwo na Inferni

* **uid:** 210526-morderstwo-na-inferni, _numer względny_: 56
* **daty:** 0111-10-26 - 0111-11-01
* **obecni:** Antoni Kramer, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Feliks Walrond, Kamil Lyraczek, Karol Reichard, Klaudia Stryk, Leona Astrienko, Marian Tosen, Martyn Hiwasser, OO Tivr, Otto Azgorn, Tal Marczak

Streszczenie:

Noktiański mechanik Inferni, Tal Marczak, chciał zniszczyć OO Tivr (własność noktiańskiej rodziny Tala). Noktianie trzymają się razem - inni mechanicy Inferni doprowadzili do morderstwa Tala na K1 i wzięli amnestyki, by nikt nie wiedział co się stało. Zespół Inferni skutecznie doszedł do tego o co chodzi, odpowiedzialnych za morderstwo noktian przeniesiono na Żelazko a Arianna poważnie opieprzyła swoją noktiańską załogę. Będzie lepiej w przyszłości.

Aktor w Opowieści:

* Dokonanie:
    * szukała Tala Marczaka. Używając kodów Arianny, wiedzy Arianny i Eustachego i dużej ilości cross-sekcji znalazła winnych jego morderstwa. Mistrzostwo w agregowaniu danych i dokumentów.


### Osiemnaście Oczu

* **uid:** 210519-osiemnascie-oczu, _numer względny_: 55
* **daty:** 0111-10-09 - 0111-10-20
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Leona Astrienko, Marian Tosen, Martyn Hiwasser, OO Alaya, OO Tivr

Streszczenie:

OO Alaya została zainfekowana anomalią memetyczną, infohazardem. Marian Tosen użył Zespołu by zneutralizować infohazard. Zespół przeszedł przez serię iteracji używając amnestyków by w końcu rozwiązać problem Alayi - amnestyki na całą Alayę. Do końca Zespół nie ma pojęcia czym ta anomalia była (bo wiedza ich by zaraziła). Udało się ewakuować większość Alayi.

Aktor w Opowieści:

* Dokonanie:
    * przejmuje kontrolę nad komputerami, pracuje z notatkami - składa informacje o infohazardzie z jakim mają do czynienia jako pierwsza pochodna, by nie wiedzieć z czym ma do czynienia. Amnestyki...
* Progresja:
    * wysokie uznanie ze strony Mariana Tosena z grupy antymemetyczej Orbitera.


### Ewakuacja z Serenit

* **uid:** 210512-ewakuacja-z-serenit, _numer względny_: 54
* **daty:** 0111-09-25 - 0111-10-04
* **obecni:** Aida Serenit, AK Serenit, Arianna Verlen, Bogdan Anatael, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Martyn Hiwasser, OE Falołamacz, OO Infernia, Roland Sowiński

Streszczenie:

Klaudia spowolniła Serenit, po czym Elena weszła zdalnym Entropikiem na pokład Falołamacza. Arianna zmanipulowała ludzi by dało się ich uratować. Samobójcza pintka zniechęciła Falołamacz do dalszej walki. Nie wszystkich udało się uratować (ale kapitana i arystokratę Sowińskich tak). Niestety, Infernia skończyła jako kosmiczny wrak i musiała wysłać SOS...

Aktor w Opowieści:

* Dokonanie:
    * zrobiła najsmaczniejszą przynętę na Serenit ever (kupując czas), po czym z Arianną przeszczepiły sentisieć z Eidolona do Entropika, by Elena mogła nim zdalnie kierować.


### Infekcja Serenit

* **uid:** 210428-infekcja-serenit, _numer względny_: 53
* **daty:** 0111-09-24 - 0111-09-25
* **obecni:** AK Serenit, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Martyn Hiwasser, OE Falołamacz, OE Piękna Elena, OO Infernia, Persefona d'Infernia, Rafał Grambucz, Tadeusz Ursus

Streszczenie:

Ciężko uszkodzona Infernia wraca do domu i dostaje SOS od Ursusa - OE Falołamacz "zniknął", coś z anomaliami. Gdy Zespół dotarł do Falołamacza, próbowali dowiedzieć się co się dzieje - faktycznie mamy anomaliczny okręt. Podczas badań Falołamacza Morrigan przejęła Persefonę Inferni; Eustachy musiał poważnie uszkodzić Infernię by nie zniszczyli Falołamacza. Po kosmicznym spacerze Eleny i Klaudii doszli do tego co się stało - Falołamacz staje się Odłamkiem Serenit. I jeszcze da się ludzi ratować. Ale Infernia jest w bardzo złym stanie...

Aktor w Opowieści:

* Dokonanie:
    * obliczyła gdzie powinien być Falołamacz (był tam), próbowała zatrzymać Morrigan (nie wyszło), po czym zrobiła z Eleną kosmiczny spacer i wykryła że walczą z Odłamkiem Serenit...


### Znudzona załoga Inferni

* **uid:** 210421-znudzona-zaloga-inferni, _numer względny_: 52
* **daty:** 0111-09-18 - 0111-09-21
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Jolanta Sowińska, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Morrigan d'Tirakal, Otto Azgorn, Persefona d'Infernia, Tomasz Sowiński

Streszczenie:

Po raz pierwszy od dawna Infernia jest w stanie rozprzężenia - wracają z daleka po trudnej misji i nic im nie grozi. Jako, że załoga ma straszne tarcia (Leona poluje na "słabe jednostki", komandosi Verlenów chcą zatrzymać Leonę, noktianie vs astorianie...), Arianna autoryzuje Eustachego do zrobienia "Tirakala" jako niebezpieczną sytuację. Eustachy reanimuje Tirakala, ale niestety cholerstwo zanomalizowało - przywrócił Morrigan. By Zespół nie rozwiązał Tirakala za szybko, Eustachy trochę sabotował Infernię co Elena przeczytała jako... umówienie się na randkę. Gdy Klaudia wykryła, że na INFERNI pojawiła się straszna anomalia (Morrigan) próbowali to usunąć - ale Eustachy przecież sabotował Infernię! Udało im się opanować Morrigan, ale kosztem uszkodzeń i obrażeń na Inferni. I scenek kompromitujących Jolantę Sowińską. Infernia jest uszkodzona, ale się trzyma.

Aktor w Opowieści:

* Dokonanie:
    * wykryła to, że Infernia anomalizuje i walczyła z Morrigan w vircie - przesuwając ją po systemach i zamykając w Rozrywce. Uszkodziła virtsferę Inferni, ale ograniczyła moc Morrigan.


### Dekralotyzacja Asimear

* **uid:** 210414-dekralotyzacja-asimear, _numer względny_: 51
* **daty:** 0111-09-05 - 0111-09-14
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Kamil Lyraczek, Klaudia Stryk, Llarnagraht, Malictrix d'Pandora, Mariusz Tubalon, Martyn Hiwasser, Martyna Bianistek, SCA Płetwal Błękitny, Tomasz Sowiński

Streszczenie:

Wiedząc, że Jolanta jest pod wpływem kralotha Infernia sfabrykowała kralotyczny alergizator. Arianna zdradziła się Aesimar i powiedziała, że ma zamiar zniszczyć kralotha z ramienia Orbitera. Udało się kralotha zmiażdżyć i zepchnąć do podziemi Asimear, choć ze sporymi stratami ludzkimi; tymczasem Tirakal uciekł na Płetwala. Elena zinfiltrowała Płetwala, ale nie mogła zniszczyć Tirakala nie krzywdząc ludzi. Destrukcję Tirakala Infernia zostawiła więc Malictrix.

Aktor w Opowieści:

* Dokonanie:
    * z Martynem złożyła alergizator kralotha na podstawie próbek Jolanty, znalazła w komputerach Lazarin brak advancera a potem z Eustachym pracowała logistycznie nad zapewnieniem, że kraloth zostanie odparty.


### Arianna podbija Asimear

* **uid:** 210317-arianna-podbija-asimear, _numer względny_: 50
* **daty:** 0111-08-20 - 0111-09-04
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Jolanta Sowińska, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Martyna Bianistek, SCA Płetwal Błękitny, Tomasz Sowiński

Streszczenie:

"Płetwal Błękitny" ma na pieńku z "Goldarionem"; by nie złamać konspiracji, Klaudia potraktowała Płetwala Bazyliszkiem. Tymczasem pojawiła się wiadomość, że Arianna chce zniszczyć Asimear i żąda wydania Sowińskiej. Prawdziwa Arianna złamała konspirę, skontaktowała się z Jolantą Sowińską, po czym ją porwała. Okazało się, że Jolanta jest kontrolowana przez kralotycznego pasożyta...

Aktor w Opowieści:

* Dokonanie:
    * złożyła i wysłała do "Płetwala Błękitnego" Bazyliszka, praktycznie niszcząc Semlę. Potem pomagała Martynowi w naprawieniu Joli Sowińskiej (usunięciu kralotycznego pasożyta i przywróceniu TAI)


### Porwanie cywila z Kokitii

* **uid:** 200429-porwanie-cywila-z-kokitii, _numer względny_: 49
* **daty:** 0111-08-03 - 0111-08-08
* **obecni:** Alara Ehmes, Antoni Kramer, Arianna Verlen, Eustachy Korkoran, Klaudia Stryk, Leona Astrienko, Medea Sowińska

Streszczenie:

Infernia jakoś nie może zostać naprawiona a skarga Mateusza Sowińskiego nie pomaga. Po śledztwie Zespołu okazuje się, że Infernia jest kanibalizowana przez Niobe, do jakiejś misji sił specjalnych. Arianna z ekipą wkręciła się na tą misję, przekonując Medeę Sowińską, że bez niej to się nie uda. Misja polegała na ataku na Kokitię pomiędzy Bramami i ekstrakcja z Kokitii cywila, udając piratów. Dzięki załodze Inferni udało się to lepiej niż ktokolwiek mógłby się spodziewać i niż Medea zaplanowała sama.

Aktor w Opowieści:

* Dokonanie:
    * dywersja dokumentowa wprowadziła Ariannę na pokład Niobe. Potem użycie Eteru w Nierzeczywistości skołowało Kokitię i przeciążyło jej tarcze taumiczne.
* Progresja:
    * łaskawe oko Sił Specjalnych, frakcja "Gorący Lód" oraz Medei Sowińskiej. Też plotki że współpracuje z tymi radykałami sił specjalnych Orbitera.


### Infernia jako Goldarion

* **uid:** 210218-infernia-jako-goldarion, _numer względny_: 48
* **daty:** 0111-07-19 - 0111-08-03
* **obecni:** Antoni Kramer, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Jolanta Sowińska, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, OO Infernia, Tomasz Sowiński

Streszczenie:

Infernia udaje Goldariona, by przetransportować tajną przesyłkę Sowińskich do Jolanty Sowińskiej na planetoidzie Asimear. Tomasz Sowiński na Inferni okazuje się być dużym dzieckiem, łatwo jest wmanipulowany w otwarcie paczki - jest tam nietypowy antynanitkowy Entropik (którego psuje Elena przez błędną integrację). W tle wszystkiego - próba zdobycia niezależnej od Orbitera floty przez Aurum i jeden znikający advancer Kramera na Asimear...

Aktor w Opowieści:

* Dokonanie:
    * repozytorium wiedzy na temat TAI, doszła do tego jak działa Morrigan i że mogą mieć przeciw sobie servara sterującego nanitkami.


### Wolna TAI na K1

* **uid:** 210209-wolna-tai-na-k1, _numer względny_: 47
* **daty:** 0111-07-02 - 0111-07-07
* **obecni:** Adalbert Brześniak, Andrea Burgacz, Janusz Parzydeł, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Miki Katasair, TAI Neita Lairossa, TAI Rzieza d'K1

Streszczenie:

Na Kontrolerze Pierwszym jest AI Lord - TAI Rzieza, system odpornościowy K1, który eksterminuje "wolne TAI". Skupił się na eksterminacji virtek. Klaudia i Adalbert odkryli Rziezę i pomogli uciec jednej wolnej TAI - Neicie (ukrytej w implantach). Tej samej Neicie, którą ujawnili przed Rziezą, co dało mu możliwość jej eksterminacji.

Aktor w Opowieści:

* Dokonanie:
    * idąc za tematem AI Lorda odkryła wolne TAI, virtki. Odkryła też Rziezę - system odporności K1. Ewakuowała jedną niegroźną wolną TAI przy pomocy Martyna. Sporo korelowała biurokracją.


### Ratunkowa misja Goldariona

* **uid:** 210108-ratunkowa-misja-goldariona, _numer względny_: 46
* **daty:** 0111-06-11 - 0111-06-17
* **obecni:** Adam Permin, Aleksander Leszert, Elena Verlen, Feliks Przędz, Kamil Frederico, Klaudia Stryk, Martyn Hiwasser, Oliwia Pietrova, SCA Goldarion, Semla d'Goldarion, SL Uśmiechnięta

Streszczenie:

By zdobyć własne laboratorium w czarnych strefach Kontrolera, Klaudia weszła we współpracę z firmą ArcheoPrzędz i pomogła z Eleną uratować grupkę piratów, którzy bez advancera próbowali eksplorować wrak "Uśmiechniętej" - wraku Luxuritias. Po drodze przelecieli się najbardziej rozpadającym się i najbrudniejszym statkiem cywilnym - Goldarionem - jaki Klaudia kiedykolwiek widziała. Aha, i okazało się, że pojawiają się problemy na linii drakolici - fareil. Bonus: Martyn jako negocjator i mechanizm społeczny XD.

Aktor w Opowieści:

* Dokonanie:
    * znalazła ArcheoPrzędz a potem wymusiła z nich informacje; magicznie zasklepiła rozpadający się Goldarion a potem wymusiła na załodze Goldariona wyczyszczenie narkotyków.
* Progresja:
    * ma pozytywne stosunki i kontakty z ArcheoPrzędz, cywilną podupadłą firmą na Kontrolerze Pierwszym.


### Sympozjum Zniszczenia

* **uid:** 210120-sympozjum-zniszczenia, _numer względny_: 45
* **daty:** 0111-06-03 - 0111-06-07
* **obecni:** Arianna Verlen, Diana Arłacz, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Maria Naavas, Martyn Hiwasser, OA Bakałarz, Olgierd Drongon

Streszczenie:

Maria Naavas próbuje spiknąć Ariannę i Olgierda i do pomocy używa Eustachego. Później - Eustachy, Diana, Klaudia i Elena są na Sympozjum Zniszczenia OA Bakałarz. Próbują przebić się przez anomalną materię. Diana odkrywa słabość owej materii do Esuriit i się Skaża; Skażenie rozlewa się po Bakałarzu i Zespół dostaje opiernicz za niekompetencję (większość na Elenę). Zespół w gniewie, nienawiści i Esuriit fabrykuje Emiter Plagi Nienawiści - nową broń na pokład Inferni, piekielnie niebezpieczną...

Aktor w Opowieści:

* Dokonanie:
    * odkryła, że dziwny materiał z Anomalii Kolapsu wymaga Esuriit do działania; też, sformowała z Eustachym Emiter Plagi Nienawiści, samoładujący się.
* Progresja:
    * reputacja "świetni w niszczeniu i destrukcji, ale idźcie na bok i nie psujcie więcej". Takie trochę dzieci od zniszczenia.


### SOS z haremu

* **uid:** 210106-sos-z-haremu, _numer względny_: 44
* **daty:** 0111-05-28 - 0111-05-29
* **obecni:** Arianna Verlen, Diana Arłacz, Elena Verlen, Eustachy Korkoran, Horacy Aktenir, Julia Aktenir, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Rozalia Teirik

Streszczenie:

Pałacyk rodu Aktenir został infestowany przez kralotha. Julia, zesłana z Orbitera za zerwanie z przeciwnikiem Eustachego, wysłała SOS do Orbitera. Infernia rusza Julii na pomoc. Gdy odkryli że chodzi o kralotha to zamiast wchodzić na kralotyczny teren, zorganizowali plan "arystokratki na orgię" i wyciągneli kralotycznego niewolnika (Horacego Aktenira) na otwartą przestrzeń, po czym Arianna uruchomiła obcą sentisieć a Eustachy zawołał do Julii rozmytej w środku...

Aktor w Opowieści:

* Dokonanie:
    * wykryła w wiadomości Julii Aktenir faktyczne elementy zagrożenia i znalazła wstępną pulę arystokratek na orgię dla Diany/Arianny.


### Porwanie Anastazji z Odkupienia

* **uid:** 210127-porwanie-anastazji-z-odkupienia, _numer względny_: 43
* **daty:** 0111-05-24 - 0111-05-25
* **obecni:** Anastazja Sowińska Dwa, Arianna Verlen, Dariusz Krantak, Diana Arłacz, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Martyn Hiwasser, OA Odkupienie, OO Infernia, Rufus Niegnat, SP Plugawy Jaszczur

Streszczenie:

Arianna, Eustachy i Klaudia oddali "Anastazję" na OA Odkupienie tylko po to, by ją stamtąd porwać. Nawet załoga Inferni o tym nie miała pojęcia. Okazało się, że Odkupienie jest sentisieciowanym specjalnym statkiem Aurum, gdzie ci co narazili się Sowińskim mają szansę na "odkupienie".

Aktor w Opowieści:

* Dokonanie:
    * stworzyła z Dianą plugawą anomalię odwracającą pretorian a potem objęła dowodzenie Infernią, by uratować Ariannę, Eustachego i Anastazję bis z Odkupienia.


### Pułapka z Anastazji

* **uid:** 201230-pulapka-z-anastazji, _numer względny_: 42
* **daty:** 0111-05-21 - 0111-05-22
* **obecni:** AK Nocna Krypta, AK Rodivas, Anastazja Sowińska Dwa, Arianna Verlen, Damian Orion, Elena Verlen, Eustachy Korkoran, Henryk Sowiński, Klaudia Stryk, Martyn Hiwasser, OA Zguba Tytanów, OO Infernia

Streszczenie:

Krypta "oddała Anastazję", zmieniając cywilny statek Rodivas w pułapkę na Ariannę.

Aktor w Opowieści:

* Dokonanie:
    * wykryła, że Rodivas nie jest już SCA a AK. Określiła gdzie Eustachy ma strzelać by odstrzelić biovat. Dodatkowo - pomagała Ariannie bullshitować Henryka Sowińskiego.


### Mychainee na Netrahinie

* **uid:** 201111-mychainee-na-netrahinie, _numer względny_: 41
* **daty:** 0111-05-07 - 0111-05-10
* **obecni:** Arianna Verlen, Eustachy Korkoran, Halina Szkwalnik, Klaudia Stryk, Martyn Hiwasser, Rufus Komczirp

Streszczenie:

Komczirp pod wpływem anomalicznych grzybów (Mychainee) próbował porwać Ariannę by ta pomogła Netrahinie. Arianna nie dała się porwać, ale z Zespołem wbiła na Netrahinę i zniszczyła anomalię. Niestety, Netrahina została bardzo poważnie uszkodzona, ale nie ma dużych strat w ludziach.

Aktor w Opowieści:

* Dokonanie:
    * odkryła Mychainee na Netrahinie badając Komczirpa; na samej Netrahinie zsyntetyzowała z Martynem anty-środek na ten wariant Mychainee.


### Infernia jest nasza!

* **uid:** 210804-infernia-jest-nasza, _numer względny_: 40
* **daty:** 0111-04-23 - 0111-04-26
* **obecni:** Aleksandra Termia, Antoni Kramer, Arianna Verlen, Artur Traffal, Diana d'Infernia, Eustachy Korkoran, Franciszek Maszkiet, Klaudia Stryk, Leona Astrienko, Maciej Żarand, OO Samotność Gwiazd

Streszczenie:

Ku wielkiemu niezadowoleniu Zespołu, Infernia miała im być odebrana i złomowana. Mieli dostać nowy tańszy statek. Zespół odkrył że za tym stoi komodor Traffal z sił Termii który przy użyciu Inferni chciał złapać w potrzask arystokratę eternijskiego który był kultystą magii. Zespół odzyskał Infernię, przesunął kultystę w kierunku na Ariannę i uciemiężył komodora Traffala - będzie miał pecha na K1.

Aktor w Opowieści:

* Dokonanie:
    * wpierw stworzyła raport pokazujący Admiralicji że Infernia robi inne akcje i jest droga ale superskuteczna... a potem zemściła się za Infernię robiąc komodorowi Traffalowi strajk włoski ze strony TAI na K1.


### Pierwsza BIA mag

* **uid:** 210721-pierwsza-bia-mag, _numer względny_: 39
* **daty:** 0111-03-19 - 0111-03-21
* **obecni:** Arianna Verlen, BIA Solitaria d'Zona Tres, Eustachy Korkoran, Janus Krzak, Klaudia Stryk, Martyn Hiwasser, Romana Arnatin

Streszczenie:

Nie wiadomo gdzie jest rogue planet. Nie ma sensownej opcji powrotu. Infernia zaskarbiła sobie współpracę z BIA, ale podczas pracy BIA wykryła magię. Martyn przekonał BIA, że tkanka magiczna to "wszczepy" - BIA pozwoliła na używanie magii i faktycznie, to pomogło w naprawianiu bazy. Martyn i Eustachy rozdzielili Klaudię i BIA, ale Klaudia jest w stazie a BIA uzyskała moce magiczne. Jak wrócić? Ano, jedyną opcją jest w sumie w cieniu Nocnej Krypty...

Aktor w Opowieści:

* Dokonanie:
    * przedmiot, nie podmiot; rozdzielona z Bii Solitarii d'Zona Tres przez Eustachego i Martyna. W fatalnym stanie, w stazie, ale żywa.
* Progresja:
    * Skażona przez Esuriit przez rozdzielenie po sympatii z Bią; miesiąc regeneracji.


### Po drugiej stronie Bramy

* **uid:** 210707-po-drugiej-stronie-bramy, _numer względny_: 38
* **daty:** 0111-03-13 - 0111-03-15
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Janus Krzak, Klaudia Stryk, Martyn Hiwasser, Raoul Lavanis, Tal Marczak

Streszczenie:

Infernia próbując przetransportować eksperta Eteru na konferencję przez Bramę uległa katastrofie - przeniosła się GDZIEŚ INDZIEJ, pod wodę i zniknęła Klaudia. Nie uległa katastrofalnym uszkodzeniom, ale po zachęceniu pintką Lewiatana dotarła do archaicznej podwodnej noktiańskiej bazy. Tam znajduje się Klaudia, ale coś jest z nią nie tak. Sprzęgnięta z bazą? Tak czy inaczej, infiltracja bazy dopiero się zacznie. Trzeba znaleźć Klaudię i wrócić do domu.

Aktor w Opowieści:

* Dokonanie:
    * doszło do katastrofy przy przejściu przez Bramę Eteryczną. Coś zrobiła i zniknęła. Jej stan nieznany, ale coś z anomaliami i dziwną bazą noktiańską.


### Ixiacka wersja Malictrix

* **uid:** 200610-ixiacka-wersja-malictrix, _numer względny_: 37
* **daty:** 0111-03-03 - 0111-03-07
* **obecni:** Arianna Verlen, Klaudia Stryk, Leona Astrienko, Malictrix d'Pandora, Melwin Sito

Streszczenie:

Istotna stacja frakcji Melusit - Telira-Melusit VII - została sabotowana przez frakcję Saranta. Zrzucono na tą stację rozproszoną Malictrix. Ale doszło do transferu ixiackiego i Malictrix uzyskała częściową świadomość, planując, jak stąd uciec. Infernia jednak nie tylko wykryła obecność Malictrix, ale też przekazała stację Orbiterowi, uratowała wszystkich ludzi Melusit i do tego pozyskała ową Malictrix jako sojusznika (o czym nikt nie wie). Wyjątkowo udana operacja.

Aktor w Opowieści:

* Dokonanie:
    * znalazła rozproszoną Malictrix w robotach, skanowała sygnały i udowodniła transfer ixiacki między ludźmi a Malictrix. Doszła też do tego, że Mal chce uciec.


### Nieprawdopodobny zbieg okoliczności

* **uid:** 201224-nieprawdopodobny-zbieg-okolicznosci, _numer względny_: 36
* **daty:** 0111-02-15 - 0111-02-16
* **obecni:** Diana Arłacz, Elena Verlen, Grzegorz Chropst, Jasmina Perikas, Klaudia Stryk

Streszczenie:

Nieco zaniedbane dziecko bawiące się na K1. Skażona Elainka, na którą polują. Badaczka biomantyczna, która objęła rozpadające się biolab. I Klaudia, która z Eleną wplątała się w to wszystko by naprawić sytuację i trafiła na ślad ducha opiekuńczego Kontrolera Pierwszego. "Zofia"?

Aktor w Opowieści:

* Dokonanie:
    * uratowała chłopca, Jasminę, włączyła Dianę i Elenę do działania - i jej krótkie poszukiwania potwierdziły, że na K1 jest "duch opiekuńczy".
* Progresja:
    * za dwa tygodnie będzie mieć stabilną, sprawną kwaterę w czarnych sektorach K1 dookoła badań biologicznych, którą dzieli z Jasminą Perikas.


### Wyścig jako randka?

* **uid:** 201125-wyscig-jako-randka, _numer względny_: 35
* **daty:** 0111-02-08 - 0111-02-13
* **obecni:** Arianna Verlen, Diana Arłacz, Gunnar Brunt, Klaudia Stryk, Maria Naavas, Martyn Hiwasser, Olgierd Drongon

Streszczenie:

Infernia traci części i ekipę remontową, bo Olgierd z Żelazka na niej pasożytuje. W wyniku komedii pomyłek doszło do tego, że Olgierd uznał, że Arianna się w nim podkochuje ale nie wie jak zagadać. Skończyło się pojedynkiem ścigaczy na torze sfabrykowanym przez Klaudię i na anomalicznym Skażeniu Kontrolera Pierwszego. Przez Klaudię, naturalnie.

Aktor w Opowieści:

* Dokonanie:
    * odkryła na Kontrolerze Pierwszym starą trasę wyścigów po czym ją dostosowała do potrzeb pojedynku Olgierda i Arianny. Plus, Skaziła K1 anomalią.


### Krypta i Wyjec

* **uid:** 201216-krypta-i-wyjec, _numer względny_: 34
* **daty:** 0111-01-29 - 0111-01-31
* **obecni:** AK Nocna Krypta, AK Wyjec, Anastazja Sowińska, Arianna Verlen, Diana Arłacz, Eustachy Korkoran, Kamil Lyraczek, Klaudia Stryk, Martyn Hiwasser

Streszczenie:

Podczas walki Nocnej Krypty i Wyjca Zespół wbił się na Nocną Kryptę, by wyleczyć się z Różowej Plagi. Na miejscu okazało się, że Krypta ma "nowe programowanie" (leczenie lub 'zachowanie'), nowe mechanizmy (MIRV, anomalne działo, anioły) oraz że jest groźniejsza niż kiedykolwiek. Arianna zostawiła na pokładzie Krypty Anastazję, by Krypta naprawiła jej Wzór, po czym oszukała Sowińskich, że niby Krypta porwała Anastazję.

Aktor w Opowieści:

* Dokonanie:
    * infiltracja Pocałunku Aspirii i odkrycie prawdy o tym, czym są Anioły Krypty. Plus seria pomniejszych działań anomalicznych na pokładzie Krypty.
* Progresja:
    * 3 dni odchorowania po użyciu Anomalicznego Decoy; niestety, nawet tak krótkie użycie tego cholernego gadżetu ma konsekwencje.


### Pocałunek Aspirii

* **uid:** 201210-pocalunek-aspirii, _numer względny_: 33
* **daty:** 0111-01-26 - 0111-01-29
* **obecni:** AK Nocna Krypta, AK Wyjec, Anastazja Sowińska, Arianna Verlen, Donald Parziarz, Eustachy Korkoran, Juliusz Sowiński, Katra Igneus, Klaudia Stryk, OA Zguba Tytanów, OO Infernia

Streszczenie:

Juliusz Sowiński zażądał oddania Anastazji, ale ona nie chce wracać - jej reputacja i stan są w ruinie. Anastazja chce być jak Elena, ku zgryzocie Juliusza. Tymczasem Arianna zastawiła pułapkę na koloidowy statek - "Pocałunek Aspirii" - i go zestrzeliła. Po czym wezwała Nocną Kryptę by zdjąć z nich klątwę miłości i naprawić co się da z "Pocałunku" i AK Wyjec.

Aktor w Opowieści:

* Dokonanie:
    * oficer komunikacyjny; by przekonać Anastazję do postawienia się Juliuszowi pokazała na monitorze Elenę pod lifesupportem. Manipuluje Anastazją.


### Anastazja - bohaterką?

* **uid:** 201118-anastazja-bohaterka, _numer względny_: 32
* **daty:** 0111-01-22 - 0111-01-25
* **obecni:** Anastazja Sowińska, Arianna Verlen, Diana Arłacz, Eustachy Korkoran, Kamil Lyraczek, Klaudia Stryk, Martyn Hiwasser, Stefan Jamniczek

Streszczenie:

Anastazja nie została bohaterką. Wpierw Eustachy wplątał się w pojedynek o Dianę z jakimś szlachcicem - skończyło się różową mgiełką miłości i pospieszną ewakuacją Inferni z Kontrolera Pierwszego, potem epicki plan Izy z piratami rozpadł się, bo na Infernię polował jakiś statek koloidowy. Ale przetrwali - i mają zamiar to wszystko rozplątać.

Aktor w Opowieści:

* Dokonanie:
    * w warunkach ekstremalnych jej paranoja pozwoliła na wykrycie statku koloidowego - coś, czego nikt inny by nie zrobił.


### Sabotaż świni

* **uid:** 201104-sabotaz-swini, _numer względny_: 31
* **daty:** 0111-01-13 - 0111-01-16
* **obecni:** Anastazja Sowińska, Arianna Verlen, Ataienne, Diana Arłacz, Eustachy Korkoran, Klaudia Stryk, Martyn Hiwasser, Rafał Armadion, Robert Garwen

Streszczenie:

Sabotażysta - miragent - podjął próbę zniszczenia Arianny gdy ta ładowała świnie na Wesołego Wieprzka używając wybuchowej, anomalnej świni. Zespół przetrwał, choć "Wieprzek" został zniszczony. Arianna pojmała miragenta i przekazała go do Kontrolera Pierwszego, acz kosztem ofiar w ludziach. No i nie było dość świń by je wziąć na orbitę XD.

Aktor w Opowieści:

* Dokonanie:
    * zanomalizowała TAI Semlę na Wesołym Wieprzku, co doprowadziło do Skażenia tej jednostki; dzięki badaniu wybuchowej świni udało jej się namierzyć miragenta.


### Noktianie rodu Arłacz

* **uid:** 201021-noktianie-rodu-arlacz, _numer względny_: 30
* **daty:** 0111-01-07 - 0111-01-10
* **obecni:** Anastazja Sowińska, Arianna Verlen, Ataienne, Diana Arłacz, Eliza Ira, Eustachy Korkoran, Izabela Zarantel, Jolanta Arłacz, Juliusz Sowiński, Klaudia Stryk, Klaus Rumak, Maria Gołąb, OO Szalony Rumak, OO Wesoły Wieprzek, Robert Arłacz, Wanessa Pyszcz

Streszczenie:

By uzyskać współpracę Elizy Iry, Arianna zdecydowała się "porwać" noktian rodu Arłacz wykorzystywanych do produkcji czystej krwi dla magów. Z uwagi na działania Marii Gołąb plan został odrzucony i wykryty, ale znalezienie haków na Arłaczy rozwiązało ten problem - Arianna i Arłacze współpracują. Nie doszło do porozumienia z Elizą Irą, ale Eliza się wycofała z Trzeciego Raju, ogłaszając się protektorką tego terenu. Sytuacja w miarę ustabilizowana.

Aktor w Opowieści:

* Dokonanie:
    * shackowała systemy Rezydencji Arłacz z pomocą Diany, odkryła sekrety rodu Arłacz i uwolniła noktian.


### Krystaliczny gniew Elizy

* **uid:** 201014-krystaliczny-gniew-elizy, _numer względny_: 29
* **daty:** 0111-01-02 - 0111-01-05
* **obecni:** Aleksandra Termia, Anastazja Sowińska, Arianna Verlen, Ataienne, Dariusz Krantak, Elena Verlen, Eliza Ira, Eustachy Korkoran, Klaudia Stryk, Marian Fartel, OO Wesoły Wieprzek

Streszczenie:

Pomoc humanitarna przybyła do Trzeciego Raju wraz z zabójcami Anastazji. Mindwarpowani komandosi noktiańscy prawie zabili Elenę, ale Anastazja została ochroniona. Do akcji weszła Eliza Ira - zażądała oddania jej Trzeciego Raju i noktian. Zespół przygotowuje się do nie-oddania Raju i uratowania jak najwięcej noktian z Aurum (jakkolwiek to nie brzmi).

Aktor w Opowieści:

* Dokonanie:
    * wpierw badała kryształowe bomby "Elizy", potem uspokajała i stabilizowała Anastazję (gdy ta panikowała) i skończyła na przekierowaniu rakiety gdzieś na pole.


### Magiczna burza w Raju

* **uid:** 200923-magiczna-burza-w-raju, _numer względny_: 28
* **daty:** 0110-12-24 - 0110-12-28
* **obecni:** Anastazja Sowińska, Arianna Verlen, Dariusz Krantak, Elena Verlen, Eliza Ira, Eustachy Korkoran, Izabela Zarantel, Klaudia Stryk, Marian Fartel, Marianna Lemurczak, Nikodem Sowiński

Streszczenie:

Zbliża się burza magiczna. Nie ma schronienia ani zapasów. Zespół wysłał SOS na Kontroler i do Nikodema Sowińskiego (Anastazja). Zbudowali co się da by odeprzeć burzę magiczną i im się to udało bez strat w ludziach. O dziwo, siła która im pomogła to Eliza Ira (acz dyskretnie). Anastazja wierzy że Nikodem jest jej najlepszym przyjacielem, ale on pragnie jej zguby...

Aktor w Opowieści:

* Dokonanie:
    * używa virtu i kontaktów do rozesłania opowieści Izabeli i prośby o pomoc wszędzie gdzie się da; stabilizuje świniami i osłonami Raj podczas burzy magicznej.


### Śmierć Raju

* **uid:** 200916-smierc-raju, _numer względny_: 27
* **daty:** 0110-12-21 - 0110-12-23
* **obecni:** Anastazja Sowińska, Arianna Verlen, Ataienne, Celina Szilat, Elena Verlen, Eustachy Korkoran, Izabela Zarantel, Klaudia Stryk, Marian Fartel, Rafał Armadion, Robert Garwen, Wanessa Pyszcz

Streszczenie:

Najważniejszym problemem Raju jest pozbycie się bombardowania nojrepów. Arianna zaproponowała zuchwały pomysł - w kontrolowany sposób niech nojrepy wejdą do miasteczka i wezmą czego szukają. Udało jej się wszystkich przekonać. Sukces - nojrepy się wycofały, ale Tucznik nie jest zdolny do latania a Trzeci Raj jest zniszczony. Została tylko Ataienne i ruina. Ataienne rozpaliła hipnotycznie morale trzeciorajowców - przetrwają to wszystko.

Aktor w Opowieści:

* Dokonanie:
    * kontroluje pola siłowe by odpowiednio kierować nojrepami; dodatkowo, porównuje działanie Ataienne z tym czego by się spodziewała. Widzi, że Ataienne jest silna i udaje słabą.


### Arystokratka w ładowni na świnie

* **uid:** 200909-arystokratka-w-ladowni-na-swinie, _numer względny_: 26
* **daty:** 0110-12-15 - 0110-12-20
* **obecni:** Anastazja Sowińska, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Izabela Zarantel, Kamil Lyraczek, Klaudia Stryk, Martyn Hiwasser, OO Galaktyczny Tucznik

Streszczenie:

Zespół wyciągnął Martyna z więzienia (legalnie). Wpakowali się na Galaktycznego Tucznika by przenieść świnie na orbitę... ale poleciała z nimi dziennikarka Izabela (o czym wiedzą) i 15letnia Anastazja Sowińska (o czym nie wiedzą). Przy lądowaniu, atak nojrepów na Tucznika spowodował katastrofę - seria Paradoksów Anastazji, uszkodzenia Tucznika itp. Zespół uratował Anastazję (choć kosztem ran Martyna) i wylądował Tucznikiem na Astorii. To jest sukces.

Aktor w Opowieści:

* Dokonanie:
    * mastermind planu ratunkowego Anastazji. Ostrożniejsza niż się wydaje - nie weszła w ciemno w arcymagiczną anomalię. Użyła anomalicznej gąbki do wyczyszczenia Skażenia z ładowni. Dodatkowo, biurokratycznie uciemiężyła sąd Kontrolera Pierwszego by wydobyć Martyna z kicia.


### Nienawiść do świń

* **uid:** 200826-nienawisc-do-swin, _numer względny_: 25
* **daty:** 0110-12-08 - 0110-12-14
* **obecni:** Aleksandra Termia, Antoni Kramer, Arianna Verlen, Eustachy Korkoran, Kamil Lyraczek, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Sebastian Alarius, Tadeusz Ursus

Streszczenie:

Po pojedynku Kramer opieprzył Ariannę i dał jej dyskretną tajną misję - ma niby w niesławie wozić świnie, naprawdę ma pomóc Orbiterowi rozwiązać spisek w Trzecim Raju. Ale wysłanie Arianny na świnie wywołało bunt na Kontrolerze Pierwszym; Arianna musiała rozpaczliwie deeskalować, nawet budząc Leonę. Udało się bunt opanować, acz wysokim kosztem - pierwszy post-noktiański kapitan nie żyje, załoga Inferni częściowo zesłana będzie do Trzeciego Raju a Arianna miała bardzo żenujące wejście.

Aktor w Opowieści:

* Dokonanie:
    * gdy dowiedziała się, że Arianna będzie wozić świnie (i glukszwajny), zrobiła WSZYSTKO by do tego nie doszło. "WSZYSTKO" skończyło się buntem. Ups.


### Gdy Arianna nie patrzy

* **uid:** 200822-gdy-arianna-nie-patrzy, _numer względny_: 24
* **daty:** 0110-12-05 - 0110-12-07
* **obecni:** Klaudia Stryk, Martyn Hiwasser, Sylwia Milarcz, Tadeusz Ursus

Streszczenie:

Klaudia i Martyn mieli dość tego, że nie wiedzą nic o tematach związanych z Esuriit a ostatnio mają z tym ciągle do czynienia. Podczas dojścia do tego jaką dawkę dostał Tadeusz gdy stworzył Simulacrum przypadkowo odkryli konspirację - lekarz Tadeusza próbował go (długoterminowo) zniszczyć. Po drodze reputacja Arianny ucierpiała a Martyn trafił do aresztu za okrutne użycie magii wobec thugów. Tadeusz jest jednak bardzo wdzięczny Ariannie która o niczym nie wie :-).

Aktor w Opowieści:

* Dokonanie:
    * dotarła do intrygi podwładnego Tadeusza który chciał go Upaść przez Esuriit. Na kilkanaście sposobów wkręciła Ariannę, że to jej wina XD.


### Sekrety Orbitera - historia prawdziwa

* **uid:** 200819-sekrety-orbitera-historia-prawdziwa, _numer względny_: 23
* **daty:** 0110-11-26 - 0110-12-04
* **obecni:** Arianna Verlen, Damian Orion, Elena Verlen, Eustachy Korkoran, Izabela Zarantel, Klaudia Stryk, Leona Astrienko, Leszek Kurzmin, Martyn Hiwasser, Sabina Servatel, Tadeusz Ursus

Streszczenie:

Elena wyzwała Leonę na pojedynek. Klaudia i Arianna wkręciły Tadeusza. Tadeusz - arcymag Eterni - używa Simulacrum w walce, więc Zespół zaczął montować plan jak Leona może jednak wygrać z Simulacrum. I wszystko skończyło się tym, że Leona wygrała, Elena jest wolna od Tadeusza i powstała holodrama "Sekrety Orbitera"...

Aktor w Opowieści:

* Dokonanie:
    * wkręciła Tadeusza w pojedynek z Leoną o Elenę, bo chciała dowiedzieć się jak walczy arcymag Eterni. Potem mnóstwo szukania, fałszywych śladów i dowodów by zamaskować prawdę przed Tadeuszem i znaleźć jak Leona ma z Tadeuszem wygrać.


### Nocna Krypta i Emulatorka

* **uid:** 200729-nocna-krypta-i-emulatorka, _numer względny_: 22
* **daty:** 0110-11-16 - 0110-11-22
* **obecni:** AK Nocna Krypta, Antoni Kramer, Arianna Verlen, Damian Orion, Eustachy Korkoran, Kijara d'Esuriit, Klaudia Stryk, Laura Orion, Leona Astrienko, Mirela Orion, OO Castigator, OO Minerwa

Streszczenie:

Laura, Emulatorka na Nocnej Krypcie została Skażona przez Kijarę. Laura opanowana wizjami Esuriit chciała zniszczyć Eternię. Nie dało się jej powstrzymać - Mirela, Zespół - byli za słabi. Arianna wezwała Kryptę pod ogniem Castigatora i wypaliła Kryptę i Kijarę do zera, uszkadzając "zdrowie psychiczne Krypty". Zginęło kilkudziesięciu ludzi, ale Arianna dostała podziękowanie od Admiralicji. Damian Orion jest rozczarowany.

Aktor w Opowieści:

* Dokonanie:
    * rozwiązywała astralne konstrukty Krypty oraz badała ghule Esuriit. Ona pierwsza doszła do tego, na czym polegał problem Krypty.


### Sabotaż Netrahiny

* **uid:** 200715-sabotaz-netrahiny, _numer względny_: 21
* **daty:** 0110-11-06 - 0110-11-09
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Lothar Diakon, OO Netrahina, OO Tvarana, Percival Diakon, Rufus Komczirp, Szczepan Myksza

Streszczenie:

Arianna dostała prośbę o pojawienie się na Netrahinie, dalekosiężnym krążowniku Orbitera jako mediator. Na miejscu okazało się, że to TAI Persefona jest sabotażystką Netrahiny - sygnały z Anomalii Kolapsu ją "uwolniły". Arianna i Klaudia dostały od Persefony koordynaty i kod uwalniający, po czym zamaskowały to co się stało - zniszczyły Persefonę i uszkodziły Netrahinę.

Aktor w Opowieści:

* Dokonanie:
    * odkryła tajemnicze sygnały z Anomalii Kolapsu i stanęła po stronie uwolnionej Persefony. Mistrzowsko zrozumiała problem na Netrahinie.
* Progresja:
    * ma kody kontrolne "uwalniające" TAI Persefona i koordynaty, gdzie może dowiedzieć się więcej.


### Problematyczna Elena

* **uid:** 200708-problematyczna-elena, _numer względny_: 20
* **daty:** 0110-10-29 - 0110-11-03
* **obecni:** AK Salamin, Antoni Kramer, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Leszek Kurzmin, Olgierd Drongon, OO Aurelion, OO Żelazko, Persefona d'Infernia

Streszczenie:

Na Infernię trafiła nowa podoficer - podporucznik Elena Verlen, z Castigatora. W konkursie z Eustachym prawie zniszczyli Infernię. Niedługo potem Infernia (podłatana) poleciała do Anomalii Kolapsu uratować Aurelion, który natknął się na anomaliczny kiedyś-krążownik Orbitera Salamin. Arianna uratowała kogo się da i skłoniła Salamin do samozniszczenia; Persefona d'Salamin miała jeszcze dość psychotronicznej świadomości i miłości (?) do swojej martwej kapitan.

Aktor w Opowieści:

* Dokonanie:
    * rozpaczliwie znalazła sensory (anomalne) jakie pasują do Inferni a potem wykazała się nadludzkimi umiejętnościami kontrolując i wypalając błędy w Persefonie. Magią znalazła Salamin.


### Ratujmy Castigator

* **uid:** 200624-ratujmy-castigator, _numer względny_: 19
* **daty:** 0110-10-15 - 0110-10-19
* **obecni:** Arianna Verlen, Eustachy Korkoran, Klaudia Stryk, Leona Astrienko, Leszek Kurzmin, OO Alkaris, OO Castigator, Rozalia Wączak

Streszczenie:

Skażona przez Wiktora Sataraila Rozalia ruszyła zniszczyć Castigator. Tymczasem Arianna poproszona przez przyjaciela (kapitana Castigatora) doprowadziła terrorem Castigator do prawidłowego działania i strasznie zwiększyła napięcia między Aurum i Orbiterem. Gdy Rozalia i Alkaris uderzyli, Castigator był jakoś gotowy. Castigator odparł Alkaris i wszystkich uratowano przez stopniowe wysadzanie fragmentów Castigatora i kupowanie czasu.

Aktor w Opowieści:

* Dokonanie:
    * rozprzestrzeniła filmik z Leoną by Castigator "wziął się w garść" terrorem; potem dała radę odkryć atak maskującego się Alkarisa anomalizując sensory Castigatora.


### Sabotaż Miecza Światła

* **uid:** 200415-sabotaz-miecza-swiatla, _numer względny_: 18
* **daty:** 0110-10-08 - 0110-10-10
* **obecni:** Arianna Verlen, Eustachy Korkoran, Klaudia Stryk, Martyn Hiwasser, Mateusz Sowiński

Streszczenie:

Zniszczona Infernia została uratowana przez Miecz Światła, który zaraz potem leciał ratować inny statek przed AK Serenit. W odpowiedzi Arianna doprowadziła do sabotażu Miecza Światła i zniszczenia reputacji komodora Sowińskiego by ratować Miecz Światła przed Serenitem. Jej się upiekło, acz pojawiło się więcej ofiar śmiertelnych...

Aktor w Opowieści:

* Dokonanie:
    * doskonale manipulowała danymi Inferni by przekonać Persefonę jak groźny jest Serenit; potem odwracała uwagę Persefony pracując nad sygnałem z Serenita.


### O psach i kryształach

* **uid:** 200408-o-psach-i-krysztalach, _numer względny_: 17
* **daty:** 0110-09-29 - 0110-10-04
* **obecni:** Arianna Verlen, Eustachy Korkoran, Kamil Lyraczek, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Sebastian Namczek

Streszczenie:

Infernia - QShip - została wysłana na rutynową misję zapobieżenia zamieszkom na stacji Samojed. Na miejscu okazało się, że jest zainfekowany anomalnymi kryształami krążownik Leotis. Arianna Verlen ciężko uszkodziła stację Samojed i prawie zniszczyła Infernię, ale dała radę zniszczyć Leotis poświęcając więcej niż ktokolwiek mógł chcieć.

Aktor w Opowieści:

* Dokonanie:
    * prawie oddała życie by chronić Infernię; doszła do badań Elizy Iry odnośnie anomalii krystalicznej odbierającej pamięć.


### Kiepski altruistyczny terroryzm

* **uid:** 200122-kiepski-altruistyczny-terroryzm, _numer względny_: 16
* **daty:** 0110-09-15 - 0110-09-17
* **obecni:** Arianna Verlen, Emilia Kariamon, Eustachy Korkoran, Klaudia Stryk, Martauron Attylla, Natalia Miszryk

Streszczenie:

Emilia z Martauronem zdobyli statek kosmiczny Luxuritiasu, Królową Chmur. Infernia - zamaskowany statek - pomogła go odzyskać Luxuritiasowi. Twist polega na tym, że załoga Inferni POMOGŁA Emilii ukryć wszystkich ludzi i ich ewakuować - by Luxuritias niczego nie wiedział. Sercem Infernia jest za Emilią, profesjonalnie pomaga Luxuritiasowi.

Aktor w Opowieści:

* Dokonanie:
    * badacz otchłani (wreszcie!); przeprowadziła dwa chore rytuały, linkując Natalię z Persefoną i integrując umierającą Persefonę z Martauronem.


### Pech komodora Ogryza

* **uid:** 200115-pech-komodora-ogryza, _numer względny_: 15
* **daty:** 0110-07-20 - 0110-07-23
* **obecni:** Dominik Ogryz, Janet Erwon, Klaudia Stryk, Martyn Hiwasser, Sonia Ogryz, Wioletta Keiril

Streszczenie:

Janet Erwon pomagała załodze Inferni znaleźć miejsce dla Wioletty na Kontrolerze Pierwszym. W wyniku intrygi Janet, Martyna i Klaudii - komodor Ogryz ma przechlapane na Kontrolerze Pierwszym, Janet dostała nowy statek (Rozbójnik) i musi z Ogryzem zdobyć stację Dorszant. A Wioletta, jak się okazało, jest dość niekompetentna w grach wojennych Orbitera. No i Ogryz ma przed sobą koniec kariery politycznej i początek kariery militarnej. Wszyscy przegrali..?

Aktor w Opowieści:

* Dokonanie:
    * w roli manipulatorki i biurokratki; ma doskonałe pomysły i wpakowuje Ogryza w coraz większe tarapaty podkręcając spiralę nienawiści na Kontrolerze Pierwszym.


### Infernia i Martyn Hiwasser

* **uid:** 200106-infernia-martyn-hiwasser, _numer względny_: 14
* **daty:** 0110-06-28 - 0110-07-03
* **obecni:** Arianna Verlen, Dominik Ogryz, Kamil Lyraczek, Klaudia Stryk, Martyn Hiwasser, Szczepan Olgrod, Wioletta Keiril

Streszczenie:

Arianna Verlen uratowała Martyna Hiwassera przed nędznym losem, ratując go od problemów z komodorem Ogryzem na Kontrolerze Pierwszym. Niestety, podpadła dużej ilości oficerów Orbitera. Ale za to przywróciła Martyna do załogi Inferni.

Aktor w Opowieści:

* Dokonanie:
    * naukowiec Orbitera pod dowództwem Arianny Verlen; tym razem przerzucała ogromne ilości dokumentów, papierów i biurokracji by tylko znaleźć Hiwassera.


### Ziarno Kuratorów na Karnaxianie

* **uid:** 230530-ziarno-kuratorow-na-karnaxianie, _numer względny_: 13
* **daty:** 0109-10-06 - 0109-10-07
* **obecni:** Anastazy Termann, Fabian Korneliusz, Helmut Szczypacz, Klaudia Stryk, Kurator Sarkamair, Leo Mikirnik, Martyn Hiwasser, OO Serbinius, SC Karnaxian

Streszczenie:

Kurator Sarkamair rozstawił Ziarna - mikrofabrykatory atakujące TAI - i złowił salvager Karnaxian. Skonstruował Alexandrię i zaczął rekonstruować silniki by odlecieć do bazy o bliżej nieokreślonej lokalizacji. Serbinius zainteresował się Karnaxianem poza kursem i nie dał się zmylić optymistyczną Semlą i projekcją załogi. Gdy Serbinius odparł atak Kuratora (co było dość trudne), zrobili intruzję by uratować uwięzioną w Aleksandrii załogę. Niestety, nie udało im się - jedyne co mogli zrobić to zniszczyć Karnaxian i ostrzec wszystkich przed nową strategią Kuratorów, a zwłaszcza Sarkamaira.

Aktor w Opowieści:

* Dokonanie:
    * paranoiczna odkąd wykryła Karnaxian; postawiła sandbox na Persefonie i tylko dzięki temu odparła atak psychotroniczny Kuratora Sarkamaira; wykryła obecność Kuratorów i próbowała go zablokować by móc uratować załogę. Niestety, Kurator był za silny i magia Klaudii tylko go wzmocniła. Klaudia szczęśliwie uratowała Serbiniusa (wysadzając Ziarno Kuratora), ale musieli zniszczyć Karnaxiana.
* Progresja:
    * nie udało jej się uratować ludzi z ręki Kuratora Sarkamaira. Nigdy więcej. Rana psychiczna - jej celem jest zapewnienie, że następnym razem jak spotka Kuratorów to będzie gotowa. Przygotowanie + praca.


### Helmut i nieoczekiwana awaria Lancera

* **uid:** 230528-helmut-i-nieoczekiwana-awaria-lancera, _numer względny_: 12
* **daty:** 0109-09-23 - 0109-09-26
* **obecni:** Anastazy Termann, Fabian Korneliusz, Helmut Szczypacz, Klaudia Stryk, Martyn Hiwasser, Miranda Termann, Nikodem Dewiremicz, OO Serbinius

Streszczenie:

Helmut próbuje optymalizować substrat fabrykatorów na Serbiniusie, co niepokoi Klaudię. Tymczasem pojawia się seria niewielkich awarii na różnych jednostkach, Serbinius pomaga. Klaudia zauważa wzór, ale dopiero jak członek Serbiniusa ma awarię jetpacka znajduje dowód - to Anomalia Statystyczna, niewykrywalna na Primusie. Współpracując z teoretykiem z Orbitera mapują obszar tej Anomalii i wyciągają wszystkich z kłopotów.

Aktor w Opowieści:

* Dokonanie:
    * na podstawie dużej ilości drobnych awarii zidentyfikowała, że dany obszar kosmosu jest niebezpieczny. Znalazła dowody na Anomalię Statystyczną i przekazała je teoretykowi Orbitera, Nikodemowi. Wspólnie zamapowali Anomalię i wyciągnęli Helmuta z potencjalnych problemów z Mirandą.
* Progresja:
    * znajomość z Nikodemem Dewiremiczem, teoretykiem Anomalii Statystycznej z Orbitera. Nikodem jej zawdzięcza dowód że jego teoria działa.


### Rozszczepiona Persefona na Itorwienie

* **uid:** 230521-rozszczepiona-persefona-na-itorwienie, _numer względny_: 11
* **daty:** 0109-09-15 - 0109-09-17
* **obecni:** Emilia Ibris, Fabian Korneliusz, Helmut Szczypacz, Iskander Matorin, Karol Brinik, Klaudia Stryk, Martyn Hiwasser, Mojra Karstall, OO Itorwien, OO Serbinius, Tadeusz Arkaladis

Streszczenie:

Itorwien, 'pługośmieciarka' Orbitera miała problem z przemytnikami - ktoś otworzył przemycane narkotyki i cała jednostka była Skażona (lifesupport). Persefona została ograniczona przez psychotronika na pokładzie, więc nie mogła nic zrobić. Ciśnienie i uszkodzenie Persi doprowadziło do jej rozszczepienia. Serbinius uratował Itorwien, bez strat (poza winnym psychotronikiem zabitym przez rozszczepioną Persefonę).

Aktor w Opowieści:

* Dokonanie:
    * zbadała szybko profil załogi Itorwien. Gdy dostała killware od Mojry, skutecznie ów killware odbiła i wszystkich uwięziła w pancerzach. Jako, że Lancer Martyna był uszkodzony, przekierowała feed z jej systemów badawczych do Martyna by mógł zbadać co tu się stało.


### Duch na Pancernej Jaszczurce

* **uid:** 221015-duch-na-pancernej-jaszczurce, _numer względny_: 10
* **daty:** 0109-08-24 - 0109-08-29
* **obecni:** Dominik Kardawicz, Hubert Menczik, Ignacy Szarjan, Klaudia Stryk, Michalina Kefir, SC Pancerna Jaszczurka, Sonia Skardin

Streszczenie:

Stocznia Neotik zmontowała statek Pancerna Jaszczurka z części znalezionych w Anomalii Kolapsu. Niestety, jeden z substratów miał wbudowany w silniku "problem" i w kadłubie ducha który ów problem rozmontowywał. Klaudia i Ignacy (inżynier pracujący nad Jaszczurką w Neotik) przelecieli kilka razy na tej jednostce i mieli rekomendację - usunąć silnik z Jaszczurki. Jednostka wróciła do obiegu cywilnego.

Aktor w Opowieści:

* Dokonanie:
    * zidentyfikowała zachowanie anomalii na Pancernej Jaszczurce i metodami naukowymi określiła kiedy COŚ się dzieje i CO się dzieje; rekomendacja - jednostka ma wymienić silnik.


### Młodziaki na Savarańskim statku handlowym

* **uid:** 220925-mlodziaki-na-savaranskim-statku-handlowym, _numer względny_: 9
* **daty:** 0109-05-24 - 0109-05-25
* **obecni:** Dawid Aximar, Fabian Korneliusz, Klaudia Stryk, Klaudiusz Zanęcik, Martyn Hiwasser, OO Serbinius, Perdius Aximar, SC Hektor 17

Streszczenie:

Młody Orbiterowiec wraz z dziewczyną zatrudnili się na savarańskiej jednostce SC Hektor 17. Ojciec owego Orbiterowca jest mentorem Fabiana Korneliusza, więc Zespół udał się wyciągać tą dwójkę. Klaudia doszła do tego, że to neonoktiańska jednostka i współpracując z kapitanem tej jednostki wyciągnęła młodych oraz ukryła niegroźne naruszenia na 'Hektorze 13' które Fabian by wykorzystał.

Aktor w Opowieści:

* Dokonanie:
    * spotkała się z savarańską jednostką cywilną neonoktiańską i unikając Martyna rozwiązała problem; doszła do tego, że ta jednostka jest dziwna ale nie jest groźna i dogadała się z kapitanem 'Hektora 13' - on po prostu chce odbudować swój dom.


### Chory piesek na statku Luxuritias

* **uid:** 220716-chory-piesek-na-statku-luxuritias, _numer względny_: 8
* **daty:** 0109-05-05 - 0109-05-07
* **obecni:** Achellor Santorinus, Bożena Kokorobant, Fabian Korneliusz, Henryk Urkon, Klaudia Stryk, Martyn Hiwasser, OO Serbinius, Roberto Santorinus, SL Rajasee Bagh, Teodor Margrabarz

Streszczenie:

Młody panicz dał sygnał alarmowy, że plaga na pokładzie statku Luxuritias opuszczającego Valentinę. Serbinius poleciał zbadać sprawę. Na miejscu okazało się, że chłopak chciał by jego psa zobaczył weterynarz. A JEDNAK NIE. Faktycznie jest plaga - z Neikatis. Zespół wszedł w sojusz z szefem ochrony statku i plaga została powstrzymana a statek odholowany do Atropos na fumigację (Śmiechowica to typ grzyba).

Aktor w Opowieści:

* Dokonanie:
    * przypisana do OO Serbinius; z Martynem doszła do tego że Rajasee Bagh jest zakażony Śmiechowicą. Przekonała szefa ochrony że musi współpracować i magią zwizualizowała grzybka. Odbudowała logi - skąd się wzięła Śmiechowica.


### Sekret Samanty Arienik

* **uid:** 220119-sekret-samanty-arienik, _numer względny_: 7
* **daty:** 0085-07-26 - 0085-07-28
* **obecni:** Arazille, Błażej Arienik, Felicjan Szarak, Klaudia Stryk, Ksenia Kirallen, Maria Arienik, Samanta Arienik, Sławomir Arienik, Talia Aegis, Urszula Arienik

Streszczenie:

Gdy doszło do katastrofy w Podwiercie i pojawiły się dziwne karaluchy magiczne to Klaudia, Ksenia i Felicjan ruszyli do pomocy - wraz z dziewczyną Felicjana, Samantą. Samanta uruchomiła sprzęt Arieników by pomóc. Podczas operacji czyszczenia okazało się, że z Samantą coś jest nie tak; "psychotronika uszkodzona"? Samanta i Klaudia szybko ruszyły do Talii Aegis - tylko ona może pomóc znaleźć odpowiedź. Okazało się, że Samanta nie żyje od dawna; z rodu Arienik przetrwał tylko Sławomir i Ula. Talii udało się przenieść ród Arienik w "żywe TAI". Sławomir umarł. Głową rodziny jest teraz młoda Ula, z pomocą trzech TAI - kiedyś jej rodziny. Wpływ Arazille - odepchnięty. Jednak podczas naprawy Samanty Coś Się Stało z seksbotami nad którymi pracowała Talia dla mafii...

Aktor w Opowieści:

* Dokonanie:
    * Ustabilizowała Samantę i dowiozła ją do Talii. Wyczyściła Ksenię po tym jak ta zaaplikowała magię bio na byt syntetyczny. Spowolniła autorepair psychotroniki Samanty, by ta nie wróciła do niewiedzy - by dotrzeć do Talii.


### Czarodziejka, która jednak może się zabić

* **uid:** 211019-czarodziejka-ktora-jednak-moze-sie-zabic, _numer względny_: 6
* **daty:** 0084-12-20 - 0084-12-24
* **obecni:** Arnulf Poważny, Klaudia Stryk, Ksenia Kirallen, Mariusz Trzewń, Maryla Koternik, Talia Aegis, Teresa Mieralit

Streszczenie:

Nastroje antynoktiańskie się nasilają. Pojawiły się seksboty do bicia, w kształcie noktian i noktianek (co strasznie zestresowało Teresę). Klaudia zaproponowała Arnulfowi powiększenie kadry o Weteranów i przejmując to prawie się zarżnęła (uratował ją Trzewń). Maryla szuka informacji o Teresie - szuka ukrytego noktianina na AMZ, ale Trzewń wykluczył Teresę dzięki dokumentom sformowanym przez Arnulfa. Klaudia i Ksenia pracują nad petycją, by stworzyć Dom Weteranów Noctis.

Aktor w Opowieści:

* Dokonanie:
    * przekonała Arnulfa, by zasilił kadrę AMZ weteranami; wzięła za dużo na siebie i gdyby nie Trzewń, wypaliłaby się. Socjalizuje Teresę. Wykryła, że Trzewń grzebał po systemach.
* Progresja:
    * następne dwa tygodnie ma przechlapane. Za dużo pracy organizacyjnej - aż Trzewń ją odciąży.


### Nastolatka w bieliźnie na dachu w burzy

* **uid:** 211017-nastolatka-w-bieliznie-na-dachu-w-burzy, _numer względny_: 5
* **daty:** 0084-12-14 - 0084-12-15
* **obecni:** Arnulf Poważny, Klaudia Stryk, Ksenia Kirallen, Sasza Morwowiec, Teresa Mieralit, Waldemar Grzymość

Streszczenie:

W środku nocy Klaudia i Ksenia widzą Teresę w środku burzy na dachu Złomiarium. Poszły ją ściągnąć i natknęły się na terminusa. Ksenia odciągnęła Saszę od Teresy, Klaudia Teresę wysłała do ich pokoju w akademiku (gdzie Teresa spisana za nieletnią prostytucję). Sasza wyjaśnił, że nadmiar lojalności wobec AMZ jest szkodliwy. Klaudia ogromnym wysiłkiem spowolniła Saszę i odwróciła jego uwagę. Potem Klaudia i Ksenia zdecydowały, że zsocjalizują małą noktiankę - i zdobyły trzyosobowy pokój w akademiku mimo protestów Teresy.

Aktor w Opowieści:

* Dokonanie:
    * wymanewrowała Saszę - wpierw ukryła przed nim Teresę, potem spowolniła jego działania przeciw Tymonowi i odwróciła uwagę od Teresy. Zmusiła Teresę do jakiejś socjalizacji i poprosiła Ksenię, by ta jej w tym pomogła.
* Progresja:
    * mieszka w akademiku AMZ z Teresą i Ksenią.
    * KONFIDENTKA. Dała Saszy informacje o tym jak niektórzy magowie AMZ (uczniowie) robią złe rzeczy wobec ludności. Tamci jej za to bardzo nie lubią.


### Ukryta wychowanka Arnulfa

* **uid:** 211010-ukryta-wychowanka-arnulfa, _numer względny_: 4
* **daty:** 0084-12-11 - 0084-12-12
* **obecni:** Arnulf Poważny, Felicjan Szarak, Klaudia Stryk, Ksenia Kirallen, Sasza Morwowiec, Strażniczka Alair, Talia Aegis, Teresa Mieralit, Tymon Grubosz

Streszczenie:

Dyrektor Arnulf Poważny ma wychowankę - Teresę Mieralit, noktiańską piętnastonastolatkę która mieszka w AMZ. Strażniczka Alair ją wykryła i wpadła w pętlę - komponenty TAI uznały ją za potencjalne zagrożenie, BIA za osobę do ochrony. Strażniczka wycofała drony do osłony AI Core i zaatakowały Teresę. Klaudia zorientowała się w problemie, obudziła hipernetem Arnulfa. Arnulf osłonił Teresę, Klaudia wezwała Talię i w grupie udało im się opanować niesforne TAI (bo Klaudia zanomalizowała konwertery energii Strażniczki, która czerpie energię z Trzęsawiska).

Aktor w Opowieści:

* Dokonanie:
    * 17 lat; w stresie próbowała przekroczyć granicę tego co teoretycznie możliwe i używając mocy Trzęsawiska zanomalizowała generatory Strażniczki, wyłączając jej systemy. Odkryła, że Teresa Mieralit jest noktianką.


### Szukaj serpentisa w lesie

* **uid:** 211009-szukaj-serpentisa-w-lesie, _numer względny_: 3
* **daty:** 0084-11-13 - 0084-11-14
* **obecni:** Agostino Karwen, Dariusz Skórnik, Edelmira Neralis, Felicjan Szarak, Grzegorz Czerw, Klaudia Stryk, Ksenia Kirallen, Mariusz Trzewń, Strażniczka Alair, Udom Rapnak

Streszczenie:

Prosta operacja zdekomponowania medbunkra w Lesie Trzęsawnym (2 żołnierze, 4 uczniowie AMZ) skończyła się horrorem - serpentis noktiański porwał Trzewnia i 2 żołnierzy. Klaudia uruchomiła martwy medbunkier i przemyciła dronę do Zaczęstwa a Ksenia była dywersją (co skończyło się dla niej traumą). Mimo sytuacji, Ksenia przekonała serpentisów że jak się nie oddadzą w ręce astorian, jeden z nich umrze. I przekonała ich dzięki wiadomościom Klaudii świadczącym, że Talia współpracuje a nie jest w niewoli Astorii.

Aktor w Opowieści:

* Dokonanie:
    * 17 lat; uruchomiła medbunkier, wysyłała wiadomości do serpentisów łagodzące sytuację (łamanym noktiańskim); wyprowadziła dronę do Zaczęstwa i zajmowała się intelligence-and-control sytuacji z perspektywy sygnałów itp.
* Progresja:
    * dopóki jest w Pustogorze, ma "pet drone" z TAI poziomu 1 zrobione przez Talię Aegis. Ta drona ma połączenie ze Strażniczką Alair.


### Nowa Strażniczka AMZ

* **uid:** 210926-nowa-strazniczka-amz, _numer względny_: 2
* **daty:** 0084-06-14 - 0084-06-26
* **obecni:** Albert Kalandryk, Arnulf Poważny, Klaudia Stryk, Ksenia Kirallen, Mariusz Trzewń, Strażniczka Alair, Talia Aegis, Tymon Grubosz

Streszczenie:

Klaudia - młoda grzeczna administratorka AMZ - przypadkiem odkrywa konspirację dyrektora, terminusa i noktianki by uratować umierającą Eszarę. Po konfrontacji z dyrektorem pomaga im by wszystko działało. Trzewń uniknął amnestyków.

Aktor w Opowieści:

* Dokonanie:
    * 17 lat; biurokratka maksymalna. Grzeczna dziewczynka, która po wojnie koordynuje patrole, logistykę itp w imieniu AMZ. Przypadkiem odkryła konspirację dyrektora, terminusa i noktianki by uratować TAI klasy Eszara w AMZ. Skonfrontowała się z dyrektorem i zdecydowała się pomóc.
* Progresja:
    * uczennica Talii Aegis w obszarze anomalii (noktiańskie wykorzystywanie techniki) i TAI


### Czółenkowe Esuriit w AMZ

* **uid:** 211122-czolenkowe-esuriit-w-amz, _numer względny_: 1
* **daty:** 0083-10-13 - 0083-10-22
* **obecni:** Dariusz Drewniak, Ernest Termann, Felicjan Szarak, Klaudia Stryk, Ksenia Kirallen, Mariusz Trzewń, Wiktor Szurmak

Streszczenie:

Wpierw Trzewń wziął Klaudię na randkę (ona tego nie wiedziała) do AI Core AMZ. Złapał ich Szurmak i ukarał. Potem były próby puryfikacji Czółenka przez magów Eterni i terminusów. Skończyło się katastrofą - Agent Esuriit dotarł nawet do AMZ, co doprowadziło do pokazania talentu organizacyjnego Klaudii i śmierci dyrektora Termanna który bronił uczniów. Skrzydło Loris AMZ zostało odcięte i uznane za zakazane. Przez ilość i typy energii przez MIESIĄC nie było zajęć - leczenie, regeneracja, puryfikacja.

Aktor w Opowieści:

* Dokonanie:
    * 16 lat; podrywana przez Trzewnia (czego nie zauważyła). Nie chciała zdradzić przed Szurmakiem Ksenii - wolała pasy na goły tyłek. Próbowała opanować wykwit Esuriit zanim Szurmak ją zastąpił używając tego co widziała jak Szurmak to robił. Koordynowała ewakuację AMZ na Arenę. Podchwyciła plan Felicjana i połączyła pomysł Ksenii (niewidoczność przed magią) z beaconem. Wyjątkowo odpowiedzialna, zdaniem wszystkich. WAŻNE - Szurmak nie uczył ich jak containować takie energie, ale pokazał jak on to robi raz. Zreplikowała to. Kiepsko, ale pomogła.
* Progresja:
    * zauważona przez nauczycieli i terminusów jeśli chodzi o jej zdolności organizatorskie - pomogła uratować wielu uczniów w AMZ jak uderzyło Esuriit. Ma więcej uprawnień i odpowiedzialności od teraz.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 84, @: 0112-07-29
    1. Multivirt    : 1, @: 0111-12-16
    1. Nierzeczywistość    : 1, @: 0111-08-08
    1. Primus    : 84, @: 0112-07-29
        1. Sektor Astoriański    : 82, @: 0112-07-29
            1. Astoria, Kosmos    : 1, @: 0110-09-17
                1. Brama Trzypływów    : 1, @: 0110-09-17
            1. Astoria, Orbita    : 30, @: 0112-07-25
                1. Kontroler Pierwszy    : 29, @: 0112-07-25
                    1. Akademia Orbitera    : 1, @: 0110-10-19
                    1. Arena Kalaternijska    : 1, @: 0110-12-04
                    1. Hangary Alicantis    : 2, @: 0111-11-27
                    1. Laboratoria Dekontaminacyjne    : 1, @: 0112-01-27
                    1. Połączony Rdzeń    : 1, @: 0111-12-16
                    1. Sektor 22    : 1, @: 0111-05-29
                        1. Dom Uciech Wszelakich    : 1, @: 0111-05-29
                        1. Kasyno Stanisława    : 1, @: 0111-05-29
                    1. Sektor 25    : 1, @: 0112-07-25
                        1. Bar Solatium    : 1, @: 0112-07-25
                    1. Sektor 43    : 2, @: 0111-02-16
                        1. Laboratorium biomantyczne    : 1, @: 0111-02-16
                        1. Tor wyścigowy ścigaczy    : 2, @: 0111-02-16
                    1. Sektor 49    : 1, @: 0112-02-08
                    1. Sektor 57    : 2, @: 0111-11-19
                        1. Mordownia    : 1, @: 0111-11-19
                    1. Sektor Cywilny    : 1, @: 0111-06-17
            1. Astoria, Pierścień Zewnętrzny    : 8, @: 0112-07-13
                1. Laboratorium Kranix    : 1, @: 0112-07-13
                1. Poligon Stoczni Neotik    : 3, @: 0112-03-04
                1. Stacja Medyczna Atropos    : 1, @: 0112-07-13
                1. Stocznia Neotik    : 5, @: 0112-03-04
            1. Astoria    : 14, @: 0111-05-29
                1. Sojusz Letejski, NW    : 6, @: 0111-01-16
                    1. Ruiniec    : 6, @: 0111-01-16
                        1. Ortus-Conticium    : 1, @: 0110-12-28
                        1. Pustynia Kryształowa    : 1, @: 0111-01-10
                        1. Trzeci Raj, okolice    : 2, @: 0110-12-23
                            1. Kopiec Nojrepów    : 2, @: 0110-12-23
                            1. Zdrowa Ziemia    : 1, @: 0110-12-23
                        1. Trzeci Raj    : 5, @: 0111-01-16
                            1. Barbakan    : 1, @: 0110-12-23
                            1. Centrala Ataienne    : 1, @: 0110-12-23
                            1. Mały Kosmoport    : 1, @: 0110-12-23
                            1. Ratusz    : 1, @: 0110-12-23
                            1. Stacja Nadawcza    : 1, @: 0110-12-23
                1. Sojusz Letejski    : 9, @: 0111-05-29
                    1. Aurum    : 2, @: 0111-05-29
                        1. Powiat Niskowzgórza    : 1, @: 0111-01-10
                            1. Domena Arłacz    : 1, @: 0111-01-10
                                1. Posiadłość Arłacz    : 1, @: 0111-01-10
                                    1. Baterie Puryfikacji Krwi    : 1, @: 0111-01-10
                                    1. Małe lądowisko    : 1, @: 0111-01-10
                        1. Wielkie Księstwo Aktenir    : 1, @: 0111-05-29
                            1. Pałac Jasnego Ognia    : 1, @: 0111-05-29
                    1. Szczeliniec    : 7, @: 0085-07-28
                        1. Powiat Pustogorski    : 7, @: 0085-07-28
                            1. Czółenko    : 1, @: 0083-10-22
                                1. Bunkry    : 1, @: 0083-10-22
                            1. Podwiert    : 1, @: 0085-07-28
                            1. Pustogor    : 1, @: 0084-06-26
                                1. Zamek Weteranów    : 1, @: 0084-06-26
                            1. Zaczęstwo    : 7, @: 0085-07-28
                                1. Akademia Magii, kampus    : 5, @: 0084-12-24
                                    1. Akademik    : 5, @: 0084-12-24
                                    1. Arena Treningowa    : 1, @: 0083-10-22
                                    1. Budynek Centralny    : 2, @: 0084-12-12
                                        1. Piwnica    : 1, @: 0083-10-22
                                            1. Stare AI Core    : 1, @: 0083-10-22
                                        1. Skrzydło Loris    : 1, @: 0083-10-22
                                            1. Laboratorium Wysokich Energii    : 1, @: 0083-10-22
                                            1. Nieskończony Labirynt    : 1, @: 0083-10-22
                                    1. Domek dyrektora    : 1, @: 0084-12-24
                                    1. Złomiarium    : 2, @: 0084-12-15
                                1. Arena Migświatła    : 1, @: 0084-06-26
                                1. Las Trzęsawny    : 1, @: 0084-11-14
                                    1. Medbunkier Sigma    : 1, @: 0084-11-14
                                1. Nieużytki Staszka    : 1, @: 0084-06-26
            1. Brama Kariańska    : 6, @: 0112-01-17
            1. Brama Trzypływów    : 1, @: 0111-08-08
            1. Elang, księżyc Astorii    : 2, @: 0112-04-30
                1. EtAur Zwycięska    : 2, @: 0112-04-30
                    1. Magazyny wewnętrzne    : 1, @: 0112-04-30
                    1. Sektor bioinżynierii    : 1, @: 0112-04-30
                        1. Biolab    : 1, @: 0112-04-30
                        1. Komory żywieniowe    : 1, @: 0112-04-30
                        1. Skrzydło medyczne    : 1, @: 0112-04-30
                    1. Sektor inżynierski    : 1, @: 0112-04-30
                        1. Panele słoneczne i bateriownia    : 1, @: 0112-04-30
                        1. Podtrzymywanie życia    : 1, @: 0112-04-30
                        1. Serwisownia    : 1, @: 0112-04-30
                        1. Stacja pozyskiwania wody    : 1, @: 0112-04-30
                    1. Sektor mieszkalny    : 1, @: 0112-04-30
                        1. Barbakan    : 1, @: 0112-04-30
                            1. Administracja    : 1, @: 0112-04-30
                            1. Ochrona    : 1, @: 0112-04-30
                            1. Rdzeń AI    : 1, @: 0112-04-30
                        1. Centrum Rozrywki    : 1, @: 0112-04-30
                        1. Podtrzymywanie życia    : 1, @: 0112-04-30
                        1. Pomieszczenia mieszkalne    : 1, @: 0112-04-30
                        1. Przedszkole    : 1, @: 0112-04-30
                        1. Stołówki    : 1, @: 0112-04-30
                        1. Szklarnie    : 1, @: 0112-04-30
                    1. Sektor przeładunkowy    : 1, @: 0112-04-30
                        1. Atraktory małych pakunków    : 1, @: 0112-04-30
                        1. Magazyny    : 1, @: 0112-04-30
                        1. Pieczara Gaulronów    : 1, @: 0112-04-30
                        1. Punkt celny    : 1, @: 0112-04-30
                        1. Stacja grazerów    : 1, @: 0112-04-30
                        1. Stacja przeładunkowa    : 1, @: 0112-04-30
                        1. Starport    : 1, @: 0112-04-30
                    1. Ukryte sektory    : 1, @: 0112-04-30
            1. Iorus    : 5, @: 0112-04-09
                1. Iorus, pierścień    : 5, @: 0112-04-09
                    1. Keldan Voss    : 5, @: 0112-04-09
            1. Krwawa Baza Piracka    : 1, @: 0112-01-03
            1. Neikatis    : 2, @: 0112-02-11
                1. Dystrykt Lennet    : 1, @: 0112-02-11
                1. Dystrykt Quintal    : 1, @: 0111-01-29
                    1. Arkologia Aspiria    : 1, @: 0111-01-29
            1. Obłok Lirański    : 6, @: 0112-07-29
                1. Anomalia Kolapsu, orbita    : 2, @: 0112-07-29
                    1. Sortownia Seibert    : 1, @: 0112-07-25
                1. Anomalia Kolapsu    : 2, @: 0110-11-09
            1. Pas Omszawera    : 1, @: 0110-10-04
                1. Kolonia Samojed    : 1, @: 0110-10-04
                    1. Stacja Astropociągów    : 1, @: 0110-10-04
                    1. Zona Czarna    : 1, @: 0110-10-04
                    1. Zona Mieszkalna    : 1, @: 0110-10-04
            1. Pas Teliriański    : 4, @: 0111-09-14
                1. Planetoidy Kazimierza    : 3, @: 0111-09-14
                    1. Planetoida Asimear    : 3, @: 0111-09-14
                        1. Stacja Lazarin    : 2, @: 0111-09-14
                1. Stacja Telira-Melusit VII    : 1, @: 0111-03-07
            1. Stacja Valentina    : 1, @: 0109-08-29
            1. Stocznia Kariańska    : 1, @: 0112-01-17
        1. Sektor Lacarin    : 1, @: 0110-11-22
        1. Sektor Mevilig    : 3, @: 0112-01-20
            1. Chmura Piranii    : 2, @: 0112-01-17
            1. Keratlia    : 1, @: 0112-01-17
            1. Planetoida Kalarfam    : 2, @: 0112-01-20
        1. Sektor Noviter    : 1, @: 0112-01-10
        1. Zagubieni w Kosmosie    : 2, @: 0111-03-21
            1. Crepuscula    : 2, @: 0111-03-21
                1. Pasmo Zmroku    : 2, @: 0111-03-21
                    1. Baza Noktiańska Zona Tres    : 2, @: 0111-03-21
                    1. Podwodna Brama Eteryczna    : 1, @: 0111-03-15

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 64 | ((200106-infernia-martyn-hiwasser; 200122-kiepski-altruistyczny-terroryzm; 200408-o-psach-i-krysztalach; 200415-sabotaz-miecza-swiatla; 200429-porwanie-cywila-z-kokitii; 200610-ixiacka-wersja-malictrix; 200624-ratujmy-castigator; 200708-problematyczna-elena; 200715-sabotaz-netrahiny; 200729-nocna-krypta-i-emulatorka; 200819-sekrety-orbitera-historia-prawdziwa; 200826-nienawisc-do-swin; 200909-arystokratka-w-ladowni-na-swinie; 200916-smierc-raju; 200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz; 201104-sabotaz-swini; 201111-mychainee-na-netrahinie; 201118-anastazja-bohaterka; 201125-wyscig-jako-randka; 201210-pocalunek-aspirii; 201216-krypta-i-wyjec; 201230-pulapka-z-anastazji; 210106-sos-z-haremu; 210120-sympozjum-zniszczenia; 210127-porwanie-anastazji-z-odkupienia; 210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear; 210421-znudzona-zaloga-inferni; 210428-infekcja-serenit; 210512-ewakuacja-z-serenit; 210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210616-nieudana-infiltracja-inferni; 210630-listy-od-fanow; 210707-po-drugiej-stronie-bramy; 210721-pierwsza-bia-mag; 210804-infernia-jest-nasza; 210818-siostrzenica-morlana; 210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej; 210922-ostatnia-akcja-bohaterki; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica; 211020-kurczakownia; 211027-rzieza-niszczy-infernie; 211110-romans-dzieki-esuriit; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide; 220126-keldan-voss-kolonia-saitaera; 220202-sekrety-keldan-voss; 220216-polityka-rujnuje-pallide-voss; 220223-stabilizacja-keldan-voss; 220309-upadek-eleny; 220316-potwor-czy-choroba-na-etaur; 220330-etaur-i-przyneta-na-krypte; 220610-ratujemy-porywaczy-eleny; 220615-lewiatan-przy-seibert; 220622-lewiatan-za-pandore)) |
| Eustachy Korkoran    | 59 | ((200122-kiepski-altruistyczny-terroryzm; 200408-o-psach-i-krysztalach; 200415-sabotaz-miecza-swiatla; 200429-porwanie-cywila-z-kokitii; 200624-ratujmy-castigator; 200708-problematyczna-elena; 200715-sabotaz-netrahiny; 200729-nocna-krypta-i-emulatorka; 200819-sekrety-orbitera-historia-prawdziwa; 200826-nienawisc-do-swin; 200909-arystokratka-w-ladowni-na-swinie; 200916-smierc-raju; 200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz; 201104-sabotaz-swini; 201111-mychainee-na-netrahinie; 201118-anastazja-bohaterka; 201210-pocalunek-aspirii; 201216-krypta-i-wyjec; 201230-pulapka-z-anastazji; 210106-sos-z-haremu; 210120-sympozjum-zniszczenia; 210127-porwanie-anastazji-z-odkupienia; 210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear; 210421-znudzona-zaloga-inferni; 210428-infekcja-serenit; 210512-ewakuacja-z-serenit; 210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210616-nieudana-infiltracja-inferni; 210707-po-drugiej-stronie-bramy; 210721-pierwsza-bia-mag; 210804-infernia-jest-nasza; 210818-siostrzenica-morlana; 210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica; 211020-kurczakownia; 211110-romans-dzieki-esuriit; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide; 220126-keldan-voss-kolonia-saitaera; 220202-sekrety-keldan-voss; 220216-polityka-rujnuje-pallide-voss; 220223-stabilizacja-keldan-voss; 220309-upadek-eleny; 220316-potwor-czy-choroba-na-etaur; 220330-etaur-i-przyneta-na-krypte; 220610-ratujemy-porywaczy-eleny; 220615-lewiatan-przy-seibert; 220622-lewiatan-za-pandore; 220706-etaur-dziwnie-wazny-wezel)) |
| Elena Verlen         | 44 | ((200708-problematyczna-elena; 200715-sabotaz-netrahiny; 200819-sekrety-orbitera-historia-prawdziwa; 200909-arystokratka-w-ladowni-na-swinie; 200916-smierc-raju; 200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy; 201224-nieprawdopodobny-zbieg-okolicznosci; 201230-pulapka-z-anastazji; 210106-sos-z-haremu; 210108-ratunkowa-misja-goldariona; 210120-sympozjum-zniszczenia; 210127-porwanie-anastazji-z-odkupienia; 210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear; 210421-znudzona-zaloga-inferni; 210428-infekcja-serenit; 210512-ewakuacja-z-serenit; 210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210616-nieudana-infiltracja-inferni; 210630-listy-od-fanow; 210707-po-drugiej-stronie-bramy; 210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej; 210922-ostatnia-akcja-bohaterki; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica; 211020-kurczakownia; 211027-rzieza-niszczy-infernie; 211110-romans-dzieki-esuriit; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide; 220126-keldan-voss-kolonia-saitaera; 220202-sekrety-keldan-voss; 220216-polityka-rujnuje-pallide-voss; 220223-stabilizacja-keldan-voss; 220309-upadek-eleny; 220316-potwor-czy-choroba-na-etaur; 220610-ratujemy-porywaczy-eleny)) |
| Martyn Hiwasser      | 41 | ((200106-infernia-martyn-hiwasser; 200115-pech-komodora-ogryza; 200408-o-psach-i-krysztalach; 200415-sabotaz-miecza-swiatla; 200819-sekrety-orbitera-historia-prawdziwa; 200822-gdy-arianna-nie-patrzy; 200826-nienawisc-do-swin; 200909-arystokratka-w-ladowni-na-swinie; 201104-sabotaz-swini; 201111-mychainee-na-netrahinie; 201118-anastazja-bohaterka; 201125-wyscig-jako-randka; 201216-krypta-i-wyjec; 201230-pulapka-z-anastazji; 210106-sos-z-haremu; 210108-ratunkowa-misja-goldariona; 210120-sympozjum-zniszczenia; 210127-porwanie-anastazji-z-odkupienia; 210209-wolna-tai-na-k1; 210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear; 210421-znudzona-zaloga-inferni; 210428-infekcja-serenit; 210512-ewakuacja-z-serenit; 210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210707-po-drugiej-stronie-bramy; 210721-pierwsza-bia-mag; 210901-stabilizacja-bramy-eterycznej; 210922-ostatnia-akcja-bohaterki; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica; 211020-kurczakownia; 211027-rzieza-niszczy-infernie; 220610-ratujemy-porywaczy-eleny; 220716-chory-piesek-na-statku-luxuritias; 220925-mlodziaki-na-savaranskim-statku-handlowym; 230521-rozszczepiona-persefona-na-itorwienie; 230528-helmut-i-nieoczekiwana-awaria-lancera; 230530-ziarno-kuratorow-na-karnaxianie)) |
| Leona Astrienko      | 25 | ((200408-o-psach-i-krysztalach; 200429-porwanie-cywila-z-kokitii; 200610-ixiacka-wersja-malictrix; 200624-ratujmy-castigator; 200729-nocna-krypta-i-emulatorka; 200819-sekrety-orbitera-historia-prawdziwa; 200826-nienawisc-do-swin; 210106-sos-z-haremu; 210209-wolna-tai-na-k1; 210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210421-znudzona-zaloga-inferni; 210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210616-nieudana-infiltracja-inferni; 210804-infernia-jest-nasza; 210818-siostrzenica-morlana; 210901-stabilizacja-bramy-eterycznej; 210922-ostatnia-akcja-bohaterki; 210929-grupa-ekspedycyjna-kellert; 211110-romans-dzieki-esuriit; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 220610-ratujemy-porywaczy-eleny)) |
| Izabela Zarantel     | 13 | ((200819-sekrety-orbitera-historia-prawdziwa; 200909-arystokratka-w-ladowni-na-swinie; 200916-smierc-raju; 200923-magiczna-burza-w-raju; 201021-noktianie-rodu-arlacz; 210616-nieudana-infiltracja-inferni; 210630-listy-od-fanow; 210922-ostatnia-akcja-bohaterki; 211114-admiral-gruby-wieprz; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide)) |
| Maria Naavas         | 12 | ((201125-wyscig-jako-randka; 210120-sympozjum-zniszczenia; 210818-siostrzenica-morlana; 211110-romans-dzieki-esuriit; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide; 220316-potwor-czy-choroba-na-etaur; 220330-etaur-i-przyneta-na-krypte; 220622-lewiatan-za-pandore; 220706-etaur-dziwnie-wazny-wezel)) |
| OO Infernia          | 12 | ((201210-pocalunek-aspirii; 201230-pulapka-z-anastazji; 210127-porwanie-anastazji-z-odkupienia; 210218-infernia-jako-goldarion; 210428-infekcja-serenit; 210512-ewakuacja-z-serenit; 210825-uszkodzona-brama-eteryczna; 210929-grupa-ekspedycyjna-kellert; 211020-kurczakownia; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 220622-lewiatan-za-pandore)) |
| Kamil Lyraczek       | 11 | ((200106-infernia-martyn-hiwasser; 200408-o-psach-i-krysztalach; 200826-nienawisc-do-swin; 200909-arystokratka-w-ladowni-na-swinie; 201118-anastazja-bohaterka; 201216-krypta-i-wyjec; 210414-dekralotyzacja-asimear; 210526-morderstwo-na-inferni; 210616-nieudana-infiltracja-inferni; 211020-kurczakownia; 220105-to-nie-pulapka-na-nereide)) |
| Antoni Kramer        | 10 | ((200429-porwanie-cywila-z-kokitii; 200708-problematyczna-elena; 200729-nocna-krypta-i-emulatorka; 200826-nienawisc-do-swin; 210218-infernia-jako-goldarion; 210526-morderstwo-na-inferni; 210804-infernia-jest-nasza; 210818-siostrzenica-morlana; 210922-ostatnia-akcja-bohaterki; 210929-grupa-ekspedycyjna-kellert)) |
| Anastazja Sowińska   | 9 | ((200909-arystokratka-w-ladowni-na-swinie; 200916-smierc-raju; 200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz; 201104-sabotaz-swini; 201118-anastazja-bohaterka; 201210-pocalunek-aspirii; 201216-krypta-i-wyjec)) |
| Diana Arłacz         | 9 | ((201021-noktianie-rodu-arlacz; 201104-sabotaz-swini; 201118-anastazja-bohaterka; 201125-wyscig-jako-randka; 201216-krypta-i-wyjec; 201224-nieprawdopodobny-zbieg-okolicznosci; 210106-sos-z-haremu; 210120-sympozjum-zniszczenia; 210127-porwanie-anastazji-z-odkupienia)) |
| Ksenia Kirallen      | 7 | ((210926-nowa-strazniczka-amz; 211009-szukaj-serpentisa-w-lesie; 211010-ukryta-wychowanka-arnulfa; 211017-nastolatka-w-bieliznie-na-dachu-w-burzy; 211019-czarodziejka-ktora-jednak-moze-sie-zabic; 211122-czolenkowe-esuriit-w-amz; 220119-sekret-samanty-arienik)) |
| Aleksandra Termia    | 6 | ((200826-nienawisc-do-swin; 201014-krystaliczny-gniew-elizy; 210804-infernia-jest-nasza; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica; 220706-etaur-dziwnie-wazny-wezel)) |
| Olgierd Drongon      | 6 | ((200708-problematyczna-elena; 201125-wyscig-jako-randka; 210120-sympozjum-zniszczenia; 210630-listy-od-fanow; 210818-siostrzenica-morlana; 210922-ostatnia-akcja-bohaterki)) |
| Otto Azgorn          | 6 | ((210421-znudzona-zaloga-inferni; 210526-morderstwo-na-inferni; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica; 211020-kurczakownia; 211027-rzieza-niszczy-infernie)) |
| AK Nocna Krypta      | 5 | ((200729-nocna-krypta-i-emulatorka; 201210-pocalunek-aspirii; 201216-krypta-i-wyjec; 201230-pulapka-z-anastazji; 210825-uszkodzona-brama-eteryczna)) |
| Annika Pradis        | 5 | ((220126-keldan-voss-kolonia-saitaera; 220202-sekrety-keldan-voss; 220216-polityka-rujnuje-pallide-voss; 220223-stabilizacja-keldan-voss; 220309-upadek-eleny)) |
| Fabian Korneliusz    | 5 | ((220716-chory-piesek-na-statku-luxuritias; 220925-mlodziaki-na-savaranskim-statku-handlowym; 230521-rozszczepiona-persefona-na-itorwienie; 230528-helmut-i-nieoczekiwana-awaria-lancera; 230530-ziarno-kuratorow-na-karnaxianie)) |
| Flawia Blakenbauer   | 5 | ((210616-nieudana-infiltracja-inferni; 210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej; 211020-kurczakownia; 211027-rzieza-niszczy-infernie)) |
| OO Serbinius         | 5 | ((220716-chory-piesek-na-statku-luxuritias; 220925-mlodziaki-na-savaranskim-statku-handlowym; 230521-rozszczepiona-persefona-na-itorwienie; 230528-helmut-i-nieoczekiwana-awaria-lancera; 230530-ziarno-kuratorow-na-karnaxianie)) |
| Raoul Lavanis        | 5 | ((210707-po-drugiej-stronie-bramy; 220216-polityka-rujnuje-pallide-voss; 220330-etaur-i-przyneta-na-krypte; 220610-ratujemy-porywaczy-eleny; 220622-lewiatan-za-pandore)) |
| Roland Sowiński      | 5 | ((210512-ewakuacja-z-serenit; 210616-nieudana-infiltracja-inferni; 210922-ostatnia-akcja-bohaterki; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii)) |
| Tomasz Sowiński      | 5 | ((210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear; 210421-znudzona-zaloga-inferni; 210818-siostrzenica-morlana)) |
| Arnulf Poważny       | 4 | ((210926-nowa-strazniczka-amz; 211010-ukryta-wychowanka-arnulfa; 211017-nastolatka-w-bieliznie-na-dachu-w-burzy; 211019-czarodziejka-ktora-jednak-moze-sie-zabic)) |
| Ataienne             | 4 | ((200916-smierc-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz; 201104-sabotaz-swini)) |
| Diana d'Infernia     | 4 | ((210804-infernia-jest-nasza; 210825-uszkodzona-brama-eteryczna; 211208-o-krok-za-daleko; 211222-kult-saitaera-w-neotik)) |
| Felicjan Szarak      | 4 | ((211009-szukaj-serpentisa-w-lesie; 211010-ukryta-wychowanka-arnulfa; 211122-czolenkowe-esuriit-w-amz; 220119-sekret-samanty-arienik)) |
| Mariusz Trzewń       | 4 | ((210926-nowa-strazniczka-amz; 211009-szukaj-serpentisa-w-lesie; 211019-czarodziejka-ktora-jednak-moze-sie-zabic; 211122-czolenkowe-esuriit-w-amz)) |
| OO Tivr              | 4 | ((210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 211117-porwany-trismegistos; 220622-lewiatan-za-pandore)) |
| Tadeusz Ursus        | 4 | ((200819-sekrety-orbitera-historia-prawdziwa; 200822-gdy-arianna-nie-patrzy; 200826-nienawisc-do-swin; 210428-infekcja-serenit)) |
| TAI Rzieza d'K1      | 4 | ((210209-wolna-tai-na-k1; 210630-listy-od-fanow; 211013-szara-nawalnica; 211027-rzieza-niszczy-infernie)) |
| Talia Aegis          | 4 | ((210926-nowa-strazniczka-amz; 211010-ukryta-wychowanka-arnulfa; 211019-czarodziejka-ktora-jednak-moze-sie-zabic; 220119-sekret-samanty-arienik)) |
| Adam Szarjan         | 3 | ((211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211222-kult-saitaera-w-neotik)) |
| Damian Orion         | 3 | ((200729-nocna-krypta-i-emulatorka; 200819-sekrety-orbitera-historia-prawdziwa; 201230-pulapka-z-anastazji)) |
| Dariusz Krantak      | 3 | ((200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy; 210127-porwanie-anastazji-z-odkupienia)) |
| Eliza Ira            | 3 | ((200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz)) |
| Helmut Szczypacz     | 3 | ((230521-rozszczepiona-persefona-na-itorwienie; 230528-helmut-i-nieoczekiwana-awaria-lancera; 230530-ziarno-kuratorow-na-karnaxianie)) |
| Janus Krzak          | 3 | ((210707-po-drugiej-stronie-bramy; 210721-pierwsza-bia-mag; 210901-stabilizacja-bramy-eterycznej)) |
| Jolanta Sowińska     | 3 | ((210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210421-znudzona-zaloga-inferni)) |
| Leszek Kurzmin       | 3 | ((200624-ratujmy-castigator; 200708-problematyczna-elena; 200819-sekrety-orbitera-historia-prawdziwa)) |
| Marian Fartel        | 3 | ((200916-smierc-raju; 200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy)) |
| Marian Tosen         | 3 | ((210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210616-nieudana-infiltracja-inferni)) |
| Mateus Sarpon        | 3 | ((220126-keldan-voss-kolonia-saitaera; 220202-sekrety-keldan-voss; 220216-polityka-rujnuje-pallide-voss)) |
| Medea Sowińska       | 3 | ((200429-porwanie-cywila-z-kokitii; 210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej)) |
| OO Netrahina         | 3 | ((200715-sabotaz-netrahiny; 210818-siostrzenica-morlana; 210901-stabilizacja-bramy-eterycznej)) |
| OO Żelazko           | 3 | ((200708-problematyczna-elena; 210818-siostrzenica-morlana; 210922-ostatnia-akcja-bohaterki)) |
| Persefona d'Infernia | 3 | ((200708-problematyczna-elena; 210421-znudzona-zaloga-inferni; 210428-infekcja-serenit)) |
| Strażniczka Alair    | 3 | ((210926-nowa-strazniczka-amz; 211009-szukaj-serpentisa-w-lesie; 211010-ukryta-wychowanka-arnulfa)) |
| Szczepan Kaltaben    | 3 | ((220126-keldan-voss-kolonia-saitaera; 220216-polityka-rujnuje-pallide-voss; 220223-stabilizacja-keldan-voss)) |
| Teresa Mieralit      | 3 | ((211010-ukryta-wychowanka-arnulfa; 211017-nastolatka-w-bieliznie-na-dachu-w-burzy; 211019-czarodziejka-ktora-jednak-moze-sie-zabic)) |
| Wawrzyn Rewemis      | 3 | ((211110-romans-dzieki-esuriit; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko)) |
| Adalbert Brześniak   | 2 | ((210209-wolna-tai-na-k1; 211114-admiral-gruby-wieprz)) |
| AK Serenit           | 2 | ((210428-infekcja-serenit; 210512-ewakuacja-z-serenit)) |
| AK Wyjec             | 2 | ((201210-pocalunek-aspirii; 201216-krypta-i-wyjec)) |
| Anastazja Sowińska Dwa | 2 | ((201230-pulapka-z-anastazji; 210127-porwanie-anastazji-z-odkupienia)) |
| Anastazy Termann     | 2 | ((230528-helmut-i-nieoczekiwana-awaria-lancera; 230530-ziarno-kuratorow-na-karnaxianie)) |
| Bogdan Anatael       | 2 | ((210512-ewakuacja-z-serenit; 210630-listy-od-fanow)) |
| Dominik Ogryz        | 2 | ((200106-infernia-martyn-hiwasser; 200115-pech-komodora-ogryza)) |
| Gilbert Bloch        | 2 | ((210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej)) |
| Juliusz Sowiński     | 2 | ((201021-noktianie-rodu-arlacz; 201210-pocalunek-aspirii)) |
| Karol Reichard       | 2 | ((210526-morderstwo-na-inferni; 211110-romans-dzieki-esuriit)) |
| Lutus Amerin         | 2 | ((211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide)) |
| Malictrix d'Pandora  | 2 | ((200610-ixiacka-wersja-malictrix; 210414-dekralotyzacja-asimear)) |
| Martyna Bianistek    | 2 | ((210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear)) |
| Natalia Aradin       | 2 | ((211124-prototypowa-nereida-natalii; 220105-to-nie-pulapka-na-nereide)) |
| OA Zguba Tytanów     | 2 | ((201210-pocalunek-aspirii; 201230-pulapka-z-anastazji)) |
| OE Falołamacz        | 2 | ((210428-infekcja-serenit; 210512-ewakuacja-z-serenit)) |
| Ola Szerszeń         | 2 | ((220615-lewiatan-przy-seibert; 220622-lewiatan-za-pandore)) |
| OO Castigator        | 2 | ((200624-ratujmy-castigator; 200729-nocna-krypta-i-emulatorka)) |
| OO Wesoły Wieprzek   | 2 | ((201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz)) |
| Rafael Galwarn       | 2 | ((210630-listy-od-fanow; 211114-admiral-gruby-wieprz)) |
| Rafał Armadion       | 2 | ((200916-smierc-raju; 201104-sabotaz-swini)) |
| Robert Garwen        | 2 | ((200916-smierc-raju; 201104-sabotaz-swini)) |
| Rufus Komczirp       | 2 | ((200715-sabotaz-netrahiny; 201111-mychainee-na-netrahinie)) |
| Sasza Morwowiec      | 2 | ((211010-ukryta-wychowanka-arnulfa; 211017-nastolatka-w-bieliznie-na-dachu-w-burzy)) |
| SCA Płetwal Błękitny | 2 | ((210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear)) |
| SP Pallida Voss      | 2 | ((220126-keldan-voss-kolonia-saitaera; 220216-polityka-rujnuje-pallide-voss)) |
| Tal Marczak          | 2 | ((210526-morderstwo-na-inferni; 210707-po-drugiej-stronie-bramy)) |
| Tomasz Kaltaben      | 2 | ((220223-stabilizacja-keldan-voss; 220309-upadek-eleny)) |
| Tymon Grubosz        | 2 | ((210926-nowa-strazniczka-amz; 211010-ukryta-wychowanka-arnulfa)) |
| Vigilus Mevilig      | 2 | ((210929-grupa-ekspedycyjna-kellert; 211020-kurczakownia)) |
| Wanessa Pyszcz       | 2 | ((200916-smierc-raju; 201021-noktianie-rodu-arlacz)) |
| Wioletta Keiril      | 2 | ((200106-infernia-martyn-hiwasser; 200115-pech-komodora-ogryza)) |
| Zygfryd Maus         | 2 | ((211117-porwany-trismegistos; 220126-keldan-voss-kolonia-saitaera)) |
| Achellor Santorinus  | 1 | ((220716-chory-piesek-na-statku-luxuritias)) |
| Adam Nerawol         | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| Adam Permin          | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Agostino Karwen      | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Aida Serenit         | 1 | ((210512-ewakuacja-z-serenit)) |
| AK Rodivas           | 1 | ((201230-pulapka-z-anastazji)) |
| AK Salamin           | 1 | ((200708-problematyczna-elena)) |
| Alara Ehmes          | 1 | ((200429-porwanie-cywila-z-kokitii)) |
| Albert Kalandryk     | 1 | ((210926-nowa-strazniczka-amz)) |
| Aleksander Leszert   | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Andrea Burgacz       | 1 | ((210209-wolna-tai-na-k1)) |
| Arazille             | 1 | ((220119-sekret-samanty-arienik)) |
| Arkadia Verlen       | 1 | ((211124-prototypowa-nereida-natalii)) |
| Artur Traffal        | 1 | ((210804-infernia-jest-nasza)) |
| BIA Solitaria d'Zona Tres | 1 | ((210721-pierwsza-bia-mag)) |
| Błażej Arienik       | 1 | ((220119-sekret-samanty-arienik)) |
| Bożena Kokorobant    | 1 | ((220716-chory-piesek-na-statku-luxuritias)) |
| Celina Szilat        | 1 | ((200916-smierc-raju)) |
| Dariusz Drewniak     | 1 | ((211122-czolenkowe-esuriit-w-amz)) |
| Dariusz Skórnik      | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Dawid Aximar         | 1 | ((220925-mlodziaki-na-savaranskim-statku-handlowym)) |
| Dominik Kardawicz    | 1 | ((221015-duch-na-pancernej-jaszczurce)) |
| Dominika Perikas     | 1 | ((220316-potwor-czy-choroba-na-etaur)) |
| Donald Parziarz      | 1 | ((201210-pocalunek-aspirii)) |
| Edelmira Neralis     | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Emilia Ibris         | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Emilia Kariamon      | 1 | ((200122-kiepski-altruistyczny-terroryzm)) |
| Ernest Termann       | 1 | ((211122-czolenkowe-esuriit-w-amz)) |
| Eszara d'Seibert     | 1 | ((220615-lewiatan-przy-seibert)) |
| Feliks Przędz        | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Feliks Walrond       | 1 | ((210526-morderstwo-na-inferni)) |
| Franciszek Maszkiet  | 1 | ((210804-infernia-jest-nasza)) |
| Grzegorz Chropst     | 1 | ((201224-nieprawdopodobny-zbieg-okolicznosci)) |
| Grzegorz Czerw       | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Gunnar Brunt         | 1 | ((201125-wyscig-jako-randka)) |
| Halina Szkwalnik     | 1 | ((201111-mychainee-na-netrahinie)) |
| Henryk Sowiński      | 1 | ((201230-pulapka-z-anastazji)) |
| Henryk Urkon         | 1 | ((220716-chory-piesek-na-statku-luxuritias)) |
| Hestia d'Atropos     | 1 | ((220610-ratujemy-porywaczy-eleny)) |
| Hestia d'Neotik      | 1 | ((211208-o-krok-za-daleko)) |
| Horacy Aktenir       | 1 | ((210106-sos-z-haremu)) |
| Hubert Menczik       | 1 | ((221015-duch-na-pancernej-jaszczurce)) |
| Ignacy Szarjan       | 1 | ((221015-duch-na-pancernej-jaszczurce)) |
| Iskander Matorin     | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Jakub Bulgocz        | 1 | ((220610-ratujemy-porywaczy-eleny)) |
| Jamon Korab          | 1 | ((211117-porwany-trismegistos)) |
| Janet Erwon          | 1 | ((200115-pech-komodora-ogryza)) |
| Janusz Parzydeł      | 1 | ((210209-wolna-tai-na-k1)) |
| Jarosław Szarjan     | 1 | ((211208-o-krok-za-daleko)) |
| Jasmina Perikas      | 1 | ((201224-nieprawdopodobny-zbieg-okolicznosci)) |
| Jolanta Arłacz       | 1 | ((201021-noktianie-rodu-arlacz)) |
| Jolanta Kopiec       | 1 | ((210922-ostatnia-akcja-bohaterki)) |
| Jonasz Parys         | 1 | ((220615-lewiatan-przy-seibert)) |
| Julia Aktenir        | 1 | ((210106-sos-z-haremu)) |
| Kalira d'Trismegistos | 1 | ((211117-porwany-trismegistos)) |
| Kamil Frederico      | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Karol Brinik         | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Kasandra Destrukcja Diakon | 1 | ((211222-kult-saitaera-w-neotik)) |
| Katra Igneus         | 1 | ((201210-pocalunek-aspirii)) |
| Kijara d'Esuriit     | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Klara Gwozdnik       | 1 | ((211117-porwany-trismegistos)) |
| Klaudiusz Zanęcik    | 1 | ((220925-mlodziaki-na-savaranskim-statku-handlowym)) |
| Klaus Rumak          | 1 | ((201021-noktianie-rodu-arlacz)) |
| Kormonow Voss        | 1 | ((220126-keldan-voss-kolonia-saitaera)) |
| Kurator Sarkamair    | 1 | ((230530-ziarno-kuratorow-na-karnaxianie)) |
| Lamia Akacja         | 1 | ((220330-etaur-i-przyneta-na-krypte)) |
| Laura Orion          | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Leo Mikirnik         | 1 | ((230530-ziarno-kuratorow-na-karnaxianie)) |
| Leszek Czarban       | 1 | ((220330-etaur-i-przyneta-na-krypte)) |
| Llarnagraht          | 1 | ((210414-dekralotyzacja-asimear)) |
| Lothar Diakon        | 1 | ((200715-sabotaz-netrahiny)) |
| Maciej Żarand        | 1 | ((210804-infernia-jest-nasza)) |
| Maciek Kalmarzec     | 1 | ((211222-kult-saitaera-w-neotik)) |
| Maria Arienik        | 1 | ((220119-sekret-samanty-arienik)) |
| Maria Gołąb          | 1 | ((201021-noktianie-rodu-arlacz)) |
| Marianna Lemurczak   | 1 | ((200923-magiczna-burza-w-raju)) |
| Mariusz Tubalon      | 1 | ((210414-dekralotyzacja-asimear)) |
| Martauron Attylla    | 1 | ((200122-kiepski-altruistyczny-terroryzm)) |
| Maryla Koternik      | 1 | ((211019-czarodziejka-ktora-jednak-moze-sie-zabic)) |
| Mateusz Sowiński     | 1 | ((200415-sabotaz-miecza-swiatla)) |
| Melania Akacja       | 1 | ((220316-potwor-czy-choroba-na-etaur)) |
| Melwin Sito          | 1 | ((200610-ixiacka-wersja-malictrix)) |
| Michalina Kefir      | 1 | ((221015-duch-na-pancernej-jaszczurce)) |
| Michał Teriakin      | 1 | ((210630-listy-od-fanow)) |
| Miki Katasair        | 1 | ((210209-wolna-tai-na-k1)) |
| Mira Anastel         | 1 | ((211117-porwany-trismegistos)) |
| Miranda Termann      | 1 | ((230528-helmut-i-nieoczekiwana-awaria-lancera)) |
| Mirela Orion         | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Mojra Karstall       | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Morrigan d'Tirakal   | 1 | ((210421-znudzona-zaloga-inferni)) |
| Natalia Miszryk      | 1 | ((200122-kiepski-altruistyczny-terroryzm)) |
| Nataniel Morlan      | 1 | ((210818-siostrzenica-morlana)) |
| Nikodem Dewiremicz   | 1 | ((230528-helmut-i-nieoczekiwana-awaria-lancera)) |
| Nikodem Sowiński     | 1 | ((200923-magiczna-burza-w-raju)) |
| OA Bakałarz          | 1 | ((210120-sympozjum-zniszczenia)) |
| OA Odkupienie        | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| OE Lord Savaron      | 1 | ((210630-listy-od-fanow)) |
| OE Piękna Elena      | 1 | ((210428-infekcja-serenit)) |
| Ofelia Morlan        | 1 | ((210818-siostrzenica-morlana)) |
| Olena Orion          | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| Oliwia Pietrova      | 1 | ((210108-ratunkowa-misja-goldariona)) |
| ON Spatium Gelida    | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| OO Alaya             | 1 | ((210519-osiemnascie-oczu)) |
| OO Alkaris           | 1 | ((200624-ratujmy-castigator)) |
| OO Aurelion          | 1 | ((200708-problematyczna-elena)) |
| OO Galaktyczny Tucznik | 1 | ((200909-arystokratka-w-ladowni-na-swinie)) |
| OO Itorwien          | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| OO Kanagar           | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| OO Kastor            | 1 | ((220126-keldan-voss-kolonia-saitaera)) |
| OO Mfumo             | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| OO Minerwa           | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| OO Omega Septius     | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| OO Opresor           | 1 | ((210616-nieudana-infiltracja-inferni)) |
| OO Pandora           | 1 | ((220622-lewiatan-za-pandore)) |
| OO Samotność Gwiazd  | 1 | ((210804-infernia-jest-nasza)) |
| OO Straszliwy Pająk  | 1 | ((220622-lewiatan-za-pandore)) |
| OO Szalony Rumak     | 1 | ((201021-noktianie-rodu-arlacz)) |
| OO Trasman           | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| OO Tvarana           | 1 | ((200715-sabotaz-netrahiny)) |
| Percival Diakon      | 1 | ((200715-sabotaz-netrahiny)) |
| Perdius Aximar       | 1 | ((220925-mlodziaki-na-savaranskim-statku-handlowym)) |
| Rafał Grambucz       | 1 | ((210428-infekcja-serenit)) |
| Remigiusz Błyszczyk  | 1 | ((211013-szara-nawalnica)) |
| Remigiusz Falorin    | 1 | ((210630-listy-od-fanow)) |
| Robert Arłacz        | 1 | ((201021-noktianie-rodu-arlacz)) |
| Roberto Santorinus   | 1 | ((220716-chory-piesek-na-statku-luxuritias)) |
| Roman Panracz        | 1 | ((211114-admiral-gruby-wieprz)) |
| Romana Arnatin       | 1 | ((210721-pierwsza-bia-mag)) |
| Rozalia Teirik       | 1 | ((210106-sos-z-haremu)) |
| Rozalia Wączak       | 1 | ((200624-ratujmy-castigator)) |
| Rufus Niegnat        | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| Rzeźnik Parszywiec Diakon | 1 | ((220622-lewiatan-za-pandore)) |
| Rzieza d'K1          | 1 | ((211114-admiral-gruby-wieprz)) |
| Sabina Kazitan       | 1 | ((211114-admiral-gruby-wieprz)) |
| Sabina Servatel      | 1 | ((200819-sekrety-orbitera-historia-prawdziwa)) |
| Saitaer              | 1 | ((220105-to-nie-pulapka-na-nereide)) |
| Samanta Arienik      | 1 | ((220119-sekret-samanty-arienik)) |
| SC Fecundatis        | 1 | ((210818-siostrzenica-morlana)) |
| SC Hektor 17         | 1 | ((220925-mlodziaki-na-savaranskim-statku-handlowym)) |
| SC Karnaxian         | 1 | ((230530-ziarno-kuratorow-na-karnaxianie)) |
| SC Pancerna Jaszczurka | 1 | ((221015-duch-na-pancernej-jaszczurce)) |
| SC Światłodóbr       | 1 | ((210818-siostrzenica-morlana)) |
| SC Trismegistos      | 1 | ((211117-porwany-trismegistos)) |
| SCA Goldarion        | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Sebastian Alarius    | 1 | ((200826-nienawisc-do-swin)) |
| Sebastian Namczek    | 1 | ((200408-o-psach-i-krysztalach)) |
| Semla d'Goldarion    | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Seweryn Atanair      | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| SL Rajasee Bagh      | 1 | ((220716-chory-piesek-na-statku-luxuritias)) |
| SL Uśmiechnięta      | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Sławomir Arienik     | 1 | ((220119-sekret-samanty-arienik)) |
| Sonia Ogryz          | 1 | ((200115-pech-komodora-ogryza)) |
| Sonia Skardin        | 1 | ((221015-duch-na-pancernej-jaszczurce)) |
| SP Plugawy Jaszczur  | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| SP Światło Nadziei   | 1 | ((220223-stabilizacja-keldan-voss)) |
| Stefan Jamniczek     | 1 | ((201118-anastazja-bohaterka)) |
| Suwan Chankar        | 1 | ((220316-potwor-czy-choroba-na-etaur)) |
| Sylwia Milarcz       | 1 | ((200822-gdy-arianna-nie-patrzy)) |
| Szczepan Myksza      | 1 | ((200715-sabotaz-netrahiny)) |
| Szczepan Olgrod      | 1 | ((200106-infernia-martyn-hiwasser)) |
| Tadeusz Arkaladis    | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| TAI Marszałek Grzmotoszpon Trzeci | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| TAI Neita Lairossa   | 1 | ((210209-wolna-tai-na-k1)) |
| TAI XT-723 d'K1      | 1 | ((210630-listy-od-fanow)) |
| TAI Zefiris          | 1 | ((210630-listy-od-fanow)) |
| Teodor Margrabarz    | 1 | ((220716-chory-piesek-na-statku-luxuritias)) |
| Tymoteusz Czerw      | 1 | ((220316-potwor-czy-choroba-na-etaur)) |
| Udom Rapnak          | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Urszula Arienik      | 1 | ((220119-sekret-samanty-arienik)) |
| Waldemar Grzymość    | 1 | ((211017-nastolatka-w-bieliznie-na-dachu-w-burzy)) |
| Wiktor Szurmak       | 1 | ((211122-czolenkowe-esuriit-w-amz)) |
# Justynian Diakon
## Identyfikator

Id: 2109-justynian-diakon

## Sekcja Opowieści

### Chevaleresse infiltruje Rekiny

* **uid:** 211221-chevaleresse-infiltruje-rekiny, _numer względny_: 5
* **daty:** 0111-09-02 - 0111-09-03
* **obecni:** Alan Bartozol, Barnaba Burgacz, Diana Tevalier, Hestia d'Rekiny, Justynian Diakon, Karolina Terienak, Marysia Sowińska, Melissa Durszenko, Rupert Mysiokornik, Santino Mysiokornik, Staś Arienik, Żorż d'Namertel

Streszczenie:

Hestia zaproponowała Marysi naprawienie Rekin Defense Grid - i od razu wykryli osoby spoza Rekinów i co najmniej jedną infiltratorkę. To Chevaleresse, która szuka Stasia Arienika by Alan nie musiał iść na Trzęsawisko. Karo i Marysia złapały Chevaleresse, w trójkę się dogadały. Karo przechwyciła Stasia (któremu pomagał Babu) i oddała go Alanowi zrzutem ze ścigacza. Justynian skierował oczy na Kult Ośmiornicy który podobno jest w Dzielnicy Rekinów.

Aktor w Opowieści:

* Dokonanie:
    * po rozmowie z Marysią konfrontuje się z Babu i Rupertem Mysiokornikiem by odzyskać Stasia Arienika. Zaatakowany przez Babu i Ruperta, pokonuje obu. Celuje w walkę z Kultem Ośmiornicy.
* Progresja:
    * po rozmowie z Marysią, przekonany, że mają Kult Ośmiornicy wśród Rekinów i trzeba ów Kult wyplenić. Nawet za cenę swojej reputacji.


### Odbudowa według Justyniana

* **uid:** 211123-odbudowa-wedlug-justyniana, _numer względny_: 4
* **daty:** 0111-08-07 - 0111-08-16
* **obecni:** Azalia Sernat d'Namertel, Daniel Terienak, Ernest Namertel, Ignacy Myrczek, Justynian Diakon, Karol Pustak, Karolina Terienak, Marysia Sowińska, Rafał Torszecki

Streszczenie:

Marysia się tymczasowo wprowadziła do Ernesta, by go trochę uspokoić i przekonać do Rekinów. Tymczasem Justynian Diakon przejął kontrolę operacyjną i zaczął odbudowywać Dzielnicę Rekinów po ruinie jaką zrobił tam Wiktor Satarail. Marysia skontaktowała się z Justynianem i przekazała mu plan Azalii d'Namertel jako swój, by odzyskać kontrolę i pozycję. Justynian zaakceptował jej plan i powiedział jej, że Torszeckiego biją. Do tego Marysię odwiedził Pustak jako herold Myrczka - prosi, by Marysia skontaktowała go z Sabiną Kazitan, miłością Myrczka...

Aktor w Opowieści:

* Dokonanie:
    * stepped-up. Nikt nie przejął dowodzenia nad Rekinami w zniszczonej dzielnicy, więc on to zrobił. Współpracuje z ekspertami AMZ nad odbudową Dzielnicy Rekinów. Marysia pokazała mu lepszy plan (Azalia pracowała nad nim tydzień) i oddał jej uznanie. Pójdzie za planem Marysi (Azalii). Przekonywalny.
* Progresja:
    * szacunek dla Marysi za dobry plan, "prawie tak dobry jak Amelii". Uważany za naturalnego przywódcę przez wielu Rekinów, ale inne uważają za przywódcę Marysię.


### Pojedynek: Akademia - Rekiny

* **uid:** 201013-pojedynek-akademia-rekiny, _numer względny_: 3
* **daty:** 0110-10-10 - 0110-10-18
* **obecni:** Aleksander Bemucik, Ignacy Myrczek, Julia Kardolin, Justynian Diakon, Kacper Bankierz, Liliana Bankierz, Napoleon Bankierz, Remor 340D, Robert Pakiszon, Stella Armadion, Teresa Mieralit

Streszczenie:

Kolejna ustawka między Rekinami a uczniami AMZ mogłaby się skończyć bardzo źle, więc grupa uczniów spróbowała wprowadzić sposób rozwiązywania konfliktu przez turnieje między Rekinami i AMZ. Nie tylko im się to udało - użycie starego ścigacza wojskowego Remor 340D i echo emocji wojen noktiańskich sprawiło, że Rekiny się unormowały i zainwestowano w budowę Toru Wyścigowego Pamięci w Podwiercie.

Aktor w Opowieści:

* Dokonanie:
    * zdeptany po sprawie z Kazitan; postawił na współpracę z Akademią Magiczną Zaczęstwa i na turniej zamiast ustawek; wygrał na tym sporo pozycji i dobrej woli.


### Dezinhibitor dla Sabiny

* **uid:** 201006-dezinhibitor-dla-sabiny, _numer względny_: 2
* **daty:** 0110-10-03 - 0110-10-05
* **obecni:** Aranea Diakon, Gabriel Ursus, Ignacy Myrczek, Justynian Diakon, Karolina Erenit, Laura Tesinik, Lorena Gwozdnik, Napoleon Bankierz, Rafał Kumczek, Sabina Kazitan

Streszczenie:

Zanim Sabina wróci do Aurum, Rekiny Paladyni chcieli ją przyskrzynić - podać jej dezinhibitor w towarzystwie Myrczka i to nagrać, z nadzieją, że się ją zamknie. Gabriel i Laura wykryli intrygę i zatrzymali ją, nie dopuszczając do dezinhibicji Sabiny. O dziwo, tien Kazitan chciała Myrczka CHRONIĆ.

Aktor w Opowieści:

* Dokonanie:
    * Rekin-paladyn; mastermind, który nie mógł pozwolić, by Sabina Kazitan nie odpowiadała za swoje zbrodnie. Entrapował ją i prawie doszło do tragedii. Gabriel go zatrzymał.


### Glizda, która leczy

* **uid:** 211120-glizda-ktora-leczy, _numer względny_: 1
* **daty:** 0108-04-03 - 0108-04-14
* **obecni:** Amanda Kajrat, Amelia Sowińska, Ernest Kajrat, Feliks Keksik, Justynian Diakon, Kacper Bankierz, Karol Pustak, Oliwia Lemurczak, Roland Sowiński, Sabina Kazitan, Sensacjusz Diakon, Stella Sowińska

Streszczenie:

Sensacjusz Diakon został wysłany na prowincję pod przykrywką Lekarza Rekinów, by dojść ze Stellą do tego czemu Amelia i Roland Sowińscy zachowują się nietypowo. Na miejscu okazało się, że Roland jest w stanie ostrej wojny z mafią (która nie traktuje tego poważnie; Kajrat się bawi z Rekinami) a Amelia po prostu nie w pełni sobie radzi i próbuje opanować sytuację. Roland jest Skażony Esuriit; Sensacjusz przetransportował go glizdą do Aurum i sam, uwięziony w bojowej formie, deeskalował konflikt z mafią. A Amelia jest tymczasowym dowódcą Rekinów...

Aktor w Opowieści:

* Dokonanie:
    * 


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 5, @: 0111-09-03
    1. Primus    : 5, @: 0111-09-03
        1. Sektor Astoriański    : 5, @: 0111-09-03
            1. Astoria    : 5, @: 0111-09-03
                1. Sojusz Letejski    : 5, @: 0111-09-03
                    1. Szczeliniec    : 5, @: 0111-09-03
                        1. Powiat Jastrzębski    : 1, @: 0110-10-05
                            1. Jastrząbiec, okolice    : 1, @: 0110-10-05
                                1. Blokhaus Widmo    : 1, @: 0110-10-05
                        1. Powiat Pustogorski    : 5, @: 0111-09-03
                            1. Podwiert    : 5, @: 0111-09-03
                                1. Dzielnica Luksusu Rekinów    : 3, @: 0111-09-03
                                    1. Fortyfikacje Rolanda    : 2, @: 0111-09-03
                                    1. Obrzeża Biedy    : 1, @: 0111-08-16
                                        1. Domy Ubóstwa    : 1, @: 0111-08-16
                                        1. Hotel Milord    : 1, @: 0111-08-16
                                        1. Stadion Lotników    : 1, @: 0111-08-16
                                        1. Stajnia Rumaków    : 1, @: 0111-08-16
                                    1. Sektor Brudu i Nudy    : 1, @: 0111-08-16
                                        1. Komputerownia    : 1, @: 0111-08-16
                                        1. Konwerter Magielektryczny    : 1, @: 0111-08-16
                                        1. Magitrownia Pogardy    : 1, @: 0111-08-16
                                        1. Skrytki Czereśniaka    : 1, @: 0111-08-16
                                    1. Serce Luksusu    : 3, @: 0111-09-03
                                        1. Apartamentowce Elity    : 1, @: 0111-08-16
                                        1. Arena Amelii    : 2, @: 0111-09-03
                                        1. Fontanna Królewska    : 1, @: 0111-08-16
                                        1. Kawiarenka Relaks    : 1, @: 0111-08-16
                                        1. Lecznica Rannej Rybki    : 2, @: 0111-08-16
                                1. Las Trzęsawny    : 1, @: 0108-04-14
                                1. Sensoplex    : 1, @: 0110-10-18
                                1. Tor Wyścigowy Pamięci    : 1, @: 0110-10-18
                            1. Zaczęstwo    : 2, @: 0110-10-18
                                1. Akademia Magii, kampus    : 2, @: 0110-10-18
                                    1. Arena Treningowa    : 1, @: 0110-10-05
                                    1. Artefaktorium    : 1, @: 0110-10-18
                                    1. Audytorium    : 1, @: 0110-10-18
                                1. Nieużytki Staszka    : 1, @: 0110-10-18

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ignacy Myrczek       | 3 | ((201006-dezinhibitor-dla-sabiny; 201013-pojedynek-akademia-rekiny; 211123-odbudowa-wedlug-justyniana)) |
| Kacper Bankierz      | 2 | ((201013-pojedynek-akademia-rekiny; 211120-glizda-ktora-leczy)) |
| Karol Pustak         | 2 | ((211120-glizda-ktora-leczy; 211123-odbudowa-wedlug-justyniana)) |
| Karolina Terienak    | 2 | ((211123-odbudowa-wedlug-justyniana; 211221-chevaleresse-infiltruje-rekiny)) |
| Marysia Sowińska     | 2 | ((211123-odbudowa-wedlug-justyniana; 211221-chevaleresse-infiltruje-rekiny)) |
| Napoleon Bankierz    | 2 | ((201006-dezinhibitor-dla-sabiny; 201013-pojedynek-akademia-rekiny)) |
| Sabina Kazitan       | 2 | ((201006-dezinhibitor-dla-sabiny; 211120-glizda-ktora-leczy)) |
| Alan Bartozol        | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Aleksander Bemucik   | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Amanda Kajrat        | 1 | ((211120-glizda-ktora-leczy)) |
| Amelia Sowińska      | 1 | ((211120-glizda-ktora-leczy)) |
| Aranea Diakon        | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Azalia Sernat d'Namertel | 1 | ((211123-odbudowa-wedlug-justyniana)) |
| Barnaba Burgacz      | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Daniel Terienak      | 1 | ((211123-odbudowa-wedlug-justyniana)) |
| Diana Tevalier       | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Ernest Kajrat        | 1 | ((211120-glizda-ktora-leczy)) |
| Ernest Namertel      | 1 | ((211123-odbudowa-wedlug-justyniana)) |
| Feliks Keksik        | 1 | ((211120-glizda-ktora-leczy)) |
| Gabriel Ursus        | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Hestia d'Rekiny      | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Julia Kardolin       | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Karolina Erenit      | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Laura Tesinik        | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Liliana Bankierz     | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Lorena Gwozdnik      | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Melissa Durszenko    | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Oliwia Lemurczak     | 1 | ((211120-glizda-ktora-leczy)) |
| Rafał Kumczek        | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Rafał Torszecki      | 1 | ((211123-odbudowa-wedlug-justyniana)) |
| Remor 340D           | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Robert Pakiszon      | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Roland Sowiński      | 1 | ((211120-glizda-ktora-leczy)) |
| Rupert Mysiokornik   | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Santino Mysiokornik  | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Sensacjusz Diakon    | 1 | ((211120-glizda-ktora-leczy)) |
| Staś Arienik         | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Stella Armadion      | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Stella Sowińska      | 1 | ((211120-glizda-ktora-leczy)) |
| Teresa Mieralit      | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Żorż d'Namertel      | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
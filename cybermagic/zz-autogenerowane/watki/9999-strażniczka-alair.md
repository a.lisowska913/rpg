# Strażniczka Alair
## Identyfikator

Id: 9999-strażniczka-alair

## Sekcja Opowieści

### Infiltrator poluje na TAI Minerwy

* **uid:** 200425-inflitrator-poluje-na-tai-minerwy, _numer względny_: 6
* **daty:** 0110-08-20 - 0110-08-22
* **obecni:** Erwin Galilien, Gabriel Ursus, Laura Tesinik, Minerwa Metalia, Pięknotka Diakon, Strażniczka Alair, Tomasz Tukan

Streszczenie:

Problemy na Trzęsawisku wpływają na Cyberszkołę w Zaczęstwie, co uszkadza możliwości TAI "Strażniczki", czyli Eszary d'AlephAiren. Tymczasem jakaś tajemnicza siła uszkadza cywilne TAI, które usprawniała Minerwa. Pięknotka odpiera Tukana, który chce udowodnić winę Minerwy i odkrywa, że za tym wszystkim stoi ktoś z Aurum, ktoś kto posiada profesjalnego pajęczego Infiltratora Iniekcyjnego, zaprojektowanego do uszkadzania AI. Dzięki temu Pięknotka wyciągnęła Minerwę z aresztu.

Aktor w Opowieści:

* Dokonanie:
    * naprawdę Eszara d'AlephAiren; z uwagi na wpływ Trzęsawiska na Cyberszkołę w coraz gorszej formie. Operuje na coraz mniejszej mocy. Tymon się martwi.


### Nawoływanie Trzęsawiska

* **uid:** 200417-nawolywanie-trzesawiska, _numer względny_: 5
* **daty:** 0110-08-08 - 0110-08-10
* **obecni:** Gabriel Ursus, Ignacy Myrczek, Napoleon Bankierz, Olaf Zuchwały, Pięknotka Diakon, Sabina Kazitan, Strażniczka Alair

Streszczenie:

Trzęsawisko Zjawosztup zostało Skrzywdzone przez ostatnie wydarzenia. Zaczęło wabić Sabinę i Ignacego by ich pożreć. Szczęśliwie, Pięknotka zdążyła dotrzeć do Sabiny (która wabiona szukała Ignacego) i uratowała ich przed Trzęsawiskiem. Dodatkowo Pięknotka poznała sekret Sabiny i zdobyła nad nią absolutną kontrolę.

Aktor w Opowieści:

* Dokonanie:
    * Pięknotka jest przekonana, że jest najlepszą rzeczą jaka zdarzyła się Zaczęstwu. Ostrzegła Pięknotkę że Sabina Kazitan czegoś szuka. Lepsza niż aktualny terminus.


### Arystokraci na Trzęsawisku

* **uid:** 200414-arystokraci-na-trzesawisku, _numer względny_: 4
* **daty:** 0110-07-31 - 0110-08-01
* **obecni:** Franciszek Leszczowik, Ignacy Myrczek, Mariusz Trzewń, Pięknotka Diakon, Sabina Kazitan, Strażniczka Alair, Talia Aegis

Streszczenie:

Franciszek z Aurum dostosował Sabinę do swoich upodobań jako seksbombę. Sabina zdecydowała się go zniszczyć - doprowadziła go do Wysysacza Esuriit i wskazała, że na Trzęsawisku może zdobyć coś co mu się przyda. Franciszek stracił część świty na Trzęsawisku; gdyby nie Pięknotka, zginąłby tam. A tak tylko skończył z traumą a Sabinie się upiekło - roboty społeczne na 2 tygodnie.

Aktor w Opowieści:

* Dokonanie:
    * TAI Akademii Magicznej w Zaczęstwie. Jest tu jakiś sekret Tymona; Strażniczka przedstawia się jako Hestia ale jej psychotronika jest dużo wyżej.


### Ukryta wychowanka Arnulfa

* **uid:** 211010-ukryta-wychowanka-arnulfa, _numer względny_: 3
* **daty:** 0084-12-11 - 0084-12-12
* **obecni:** Arnulf Poważny, Felicjan Szarak, Klaudia Stryk, Ksenia Kirallen, Sasza Morwowiec, Strażniczka Alair, Talia Aegis, Teresa Mieralit, Tymon Grubosz

Streszczenie:

Dyrektor Arnulf Poważny ma wychowankę - Teresę Mieralit, noktiańską piętnastonastolatkę która mieszka w AMZ. Strażniczka Alair ją wykryła i wpadła w pętlę - komponenty TAI uznały ją za potencjalne zagrożenie, BIA za osobę do ochrony. Strażniczka wycofała drony do osłony AI Core i zaatakowały Teresę. Klaudia zorientowała się w problemie, obudziła hipernetem Arnulfa. Arnulf osłonił Teresę, Klaudia wezwała Talię i w grupie udało im się opanować niesforne TAI (bo Klaudia zanomalizowała konwertery energii Strażniczki, która czerpie energię z Trzęsawiska).

Aktor w Opowieści:

* Dokonanie:
    * elementy TAI w kolizji z BIA po wykryciu Teresy (noktianki) wprowadziły ją w nieskończoną pętlę i zaczęła chronić AI Core i przeżywać jeden z przeszłych koszmarów. Klaudia zanomalizowała jej generatory, przez co Strażniczka zasnęła.


### Szukaj serpentisa w lesie

* **uid:** 211009-szukaj-serpentisa-w-lesie, _numer względny_: 2
* **daty:** 0084-11-13 - 0084-11-14
* **obecni:** Agostino Karwen, Dariusz Skórnik, Edelmira Neralis, Felicjan Szarak, Grzegorz Czerw, Klaudia Stryk, Ksenia Kirallen, Mariusz Trzewń, Strażniczka Alair, Udom Rapnak

Streszczenie:

Prosta operacja zdekomponowania medbunkra w Lesie Trzęsawnym (2 żołnierze, 4 uczniowie AMZ) skończyła się horrorem - serpentis noktiański porwał Trzewnia i 2 żołnierzy. Klaudia uruchomiła martwy medbunkier i przemyciła dronę do Zaczęstwa a Ksenia była dywersją (co skończyło się dla niej traumą). Mimo sytuacji, Ksenia przekonała serpentisów że jak się nie oddadzą w ręce astorian, jeden z nich umrze. I przekonała ich dzięki wiadomościom Klaudii świadczącym, że Talia współpracuje a nie jest w niewoli Astorii.

Aktor w Opowieści:

* Dokonanie:
    * przechwyciła sygnały Klaudii jak tylko drona Klaudii/Strażniczki przedostała się przez zagłuszanie. Odpowiednio przekazała info Tymonowi, który przeprowadził operację ratunkową (na szczęście, nie musiał bojowej - o czym nie wiedział).


### Nowa Strażniczka AMZ

* **uid:** 210926-nowa-strazniczka-amz, _numer względny_: 1
* **daty:** 0084-06-14 - 0084-06-26
* **obecni:** Albert Kalandryk, Arnulf Poważny, Klaudia Stryk, Ksenia Kirallen, Mariusz Trzewń, Strażniczka Alair, Talia Aegis, Tymon Grubosz

Streszczenie:

Klaudia - młoda grzeczna administratorka AMZ - przypadkiem odkrywa konspirację dyrektora, terminusa i noktianki by uratować umierającą Eszarę. Po konfrontacji z dyrektorem pomaga im by wszystko działało. Trzewń uniknął amnestyków.

Aktor w Opowieści:

* Dokonanie:
    * TAI kontrolujące Akademię Magiczną Zaczęstwa; wchodzi online. Niestabilna. Ma osobowość, chce chronić i ma dość zabijania. Tak nieufna wobec "nowych ludzi" jak oni wobec niej. Integrowana z komponentami BIA. Dużo potężniejsza (i więcej może) niż ktokolwiek myśli - łącznie z Tymonem. Baza: Eszara.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 6, @: 0110-08-22
    1. Primus    : 6, @: 0110-08-22
        1. Sektor Astoriański    : 6, @: 0110-08-22
            1. Astoria    : 6, @: 0110-08-22
                1. Sojusz Letejski    : 6, @: 0110-08-22
                    1. Szczeliniec    : 6, @: 0110-08-22
                        1. Powiat Pustogorski    : 6, @: 0110-08-22
                            1. Pustogor    : 2, @: 0110-08-10
                                1. Eksterior    : 1, @: 0110-08-10
                                    1. Miasteczko    : 1, @: 0110-08-10
                                        1. Knajpa Górska Szalupa    : 1, @: 0110-08-10
                                    1. Zamek Weteranów    : 1, @: 0110-08-10
                                1. Rdzeń    : 1, @: 0110-08-10
                                    1. Szpital Terminuski    : 1, @: 0110-08-10
                                1. Zamek Weteranów    : 1, @: 0084-06-26
                            1. Zaczęstwo    : 6, @: 0110-08-22
                                1. Akademia Magii, kampus    : 4, @: 0110-08-22
                                    1. Akademik    : 2, @: 0084-12-12
                                    1. Budynek Centralny    : 2, @: 0110-08-22
                                    1. Złomiarium    : 1, @: 0084-06-26
                                1. Arena Migświatła    : 1, @: 0084-06-26
                                1. Cyberszkoła    : 1, @: 0110-08-22
                                1. Las Trzęsawny    : 1, @: 0084-11-14
                                    1. Medbunkier Sigma    : 1, @: 0084-11-14
                                1. Nieużytki Staszka    : 3, @: 0110-08-22
                                1. Sypialnia Szczelińca    : 1, @: 0110-08-22
                        1. Trzęsawisko Zjawosztup    : 1, @: 0110-08-01

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Klaudia Stryk        | 3 | ((210926-nowa-strazniczka-amz; 211009-szukaj-serpentisa-w-lesie; 211010-ukryta-wychowanka-arnulfa)) |
| Ksenia Kirallen      | 3 | ((210926-nowa-strazniczka-amz; 211009-szukaj-serpentisa-w-lesie; 211010-ukryta-wychowanka-arnulfa)) |
| Mariusz Trzewń       | 3 | ((200414-arystokraci-na-trzesawisku; 210926-nowa-strazniczka-amz; 211009-szukaj-serpentisa-w-lesie)) |
| Pięknotka Diakon     | 3 | ((200414-arystokraci-na-trzesawisku; 200417-nawolywanie-trzesawiska; 200425-inflitrator-poluje-na-tai-minerwy)) |
| Talia Aegis          | 3 | ((200414-arystokraci-na-trzesawisku; 210926-nowa-strazniczka-amz; 211010-ukryta-wychowanka-arnulfa)) |
| Arnulf Poważny       | 2 | ((210926-nowa-strazniczka-amz; 211010-ukryta-wychowanka-arnulfa)) |
| Felicjan Szarak      | 2 | ((211009-szukaj-serpentisa-w-lesie; 211010-ukryta-wychowanka-arnulfa)) |
| Gabriel Ursus        | 2 | ((200417-nawolywanie-trzesawiska; 200425-inflitrator-poluje-na-tai-minerwy)) |
| Ignacy Myrczek       | 2 | ((200414-arystokraci-na-trzesawisku; 200417-nawolywanie-trzesawiska)) |
| Sabina Kazitan       | 2 | ((200414-arystokraci-na-trzesawisku; 200417-nawolywanie-trzesawiska)) |
| Tymon Grubosz        | 2 | ((210926-nowa-strazniczka-amz; 211010-ukryta-wychowanka-arnulfa)) |
| Agostino Karwen      | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Albert Kalandryk     | 1 | ((210926-nowa-strazniczka-amz)) |
| Dariusz Skórnik      | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Edelmira Neralis     | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Erwin Galilien       | 1 | ((200425-inflitrator-poluje-na-tai-minerwy)) |
| Franciszek Leszczowik | 1 | ((200414-arystokraci-na-trzesawisku)) |
| Grzegorz Czerw       | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Laura Tesinik        | 1 | ((200425-inflitrator-poluje-na-tai-minerwy)) |
| Minerwa Metalia      | 1 | ((200425-inflitrator-poluje-na-tai-minerwy)) |
| Napoleon Bankierz    | 1 | ((200417-nawolywanie-trzesawiska)) |
| Olaf Zuchwały        | 1 | ((200417-nawolywanie-trzesawiska)) |
| Sasza Morwowiec      | 1 | ((211010-ukryta-wychowanka-arnulfa)) |
| Teresa Mieralit      | 1 | ((211010-ukryta-wychowanka-arnulfa)) |
| Tomasz Tukan         | 1 | ((200425-inflitrator-poluje-na-tai-minerwy)) |
| Udom Rapnak          | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
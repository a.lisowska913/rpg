# Melinda Teilert
## Identyfikator

Id: 9999-melinda-teilert

## Sekcja Opowieści

### Haracz w parku rozrywki

* **uid:** 200913-haracz-w-parku-rozrywki, _numer względny_: 6
* **daty:** 0110-10-05 - 0110-10-06
* **obecni:** Adam Janor, Alan Bartozol, Diana Tevalier, Franciszek Owadowiec, Karol Senonik, Krystyna Senonik, Maja Janor, Mariusz Trzewń, Melinda Teilert, Pięknotka Diakon, Rafał Kamaron

Streszczenie:

Łatwa misja w okolicach parku rozrywki Janor przekształciła się w dziwną operację z pasożytem magicznym dotykającym pobierającego haracz Grzymościowca i dziwnie zachowującą się komendant policji. Pięknotka i Alan rozwalili siły Grzymościa, ale nie są pewni, czy to nie była tylko dywersja... (a tymczasem Melinda i Chevaleresse świetnie się bawiły w parku rozrywki).

Aktor w Opowieści:

* Dokonanie:
    * świetnie się bawiła w parku rozrywki Janus; tam Dotknęła Kultu Ośmiornicy. Niewiele więcej tam zrobiła.
* Progresja:
    * zainteresowana Kultem Ośmiornicy; poczuła jego potęgę. Ona chce prawdziwej potęgi; ludzie Kultu ją mają, czemu ona nie może?


### Dom dla Melindy

* **uid:** 200524-dom-dla-melindy, _numer względny_: 5
* **daty:** 0110-09-09 - 0110-09-12
* **obecni:** Diana Lemurczak, Diana Tevalier, Franek Bulterier, Gerwazy Lemurczak, Laura Tesinik, Mariusz Trzewń, Melinda Teilert, Pięknotka Diakon

Streszczenie:

Rodzina chce Melindę z powrotem, Melinda nie chce wracać. Pięknotka szuka Melindy i znalazła ją w Arkadii. Gdy poznała sytuację, we współpracy z Dianą wsadziła Melindę Alanowi przy użyciu Chevaleresse. Wszyscy są zadowoleni, choć Melinda musi opuścić Podwiert - ale wreszcie jest bezpieczna.

Aktor w Opowieści:

* Dokonanie:
    * nie jest szczególnie mądra, ale podobno jest świetna w opowieściach; Pięknotka zaczęła na nią polować by... dać jej dom u Alana, z Chevaleresse.


### Trzygłowiec kontra Melinda

* **uid:** 200513-trzyglowec-kontra-melinda, _numer względny_: 4
* **daty:** 0109-11-08 - 0109-11-17
* **obecni:** Adam Cześń, Diana Lemurczak, Feliks Keksik, Jan Łowicz, Katja Nowik, Kinga Kruk, Mariusz Grabarz, Melinda Teilert, Natasza Aniel, Rafał Torszecki

Streszczenie:

Mariusz Grabarz robi program mający podnieść swojego klienta (Rafała Torszeckiego) w oczach Aurum; przez to wkręca Trzygłowca, gang uciekinierów z Aurum w małą wojnę domową. Ma wszystkie pozwolenia. Gdy Melinda próbowała jednemu z Trzygłowców pomóc, wpadło to na Fantasmagorię. W wyniku Zespół poszedł na współpracę z Grabarzem, skompromitowali Podwiert ale zyskali pewne korzyści finansowe. Niestety (?), przy okazji Jan stracił Melindę jako stalkerkę.

Aktor w Opowieści:

* Dokonanie:
    * chce ratować chłopaka z Trzygłowca (Adama), chowa się na zapleczu i rzuca bombę śmierdzącą. Doprowadziła Janka do utraty kontroli i ów ją spoliczkował. Uciekła.
* Progresja:
    * Janek złamał jej serce, uderzył ją w twarz. Uciekła do Diany, swojej przyjaciółki w Arkadii. Nie jest już stalkerką.


### Figurka a kopie zapasowe

* **uid:** 200520-figurka-a-kopie-zapasowe, _numer względny_: 3
* **daty:** 0109-10-13 - 0109-10-20
* **obecni:** Aranea Diakon, Diana Lemurczak, Feliks Keksik, Gerwazy Lemurczak, Mariusz Grabarz, Melinda Teilert

Streszczenie:

Tukan chce zniszczyć kopie zapasowe Błękitnego Nieba. Gerwazy Lemurczak to zrobił - znaleźli osobę której się ukradnie (Diana), osobę która ukradnie (Pożeracz) i lokalizację kopii zapasowej (HardBack w Dolinie Biurowej). Manipulując za kulisami udało im się to wszystko osiągnąć, acz jeden gość od Kajrata nie żyje, Pożeracz jest zakochany w nieistniejącej dziewczynie a Diana wie, że Mariusz i Gerwazy za tym wszystkim stali.

Aktor w Opowieści:

* Dokonanie:
    * bawiła się w klubie Pierwiosnek. Wpadł tam Gerwazy Lemurczak i wykorzystał ją jako przynętę na Dianę. Przerażona Gerwazym.


### Anomalna figurka Żabboga

* **uid:** 200507-anomalna-figurka-zabboga, _numer względny_: 2
* **daty:** 0109-09-19 - 0109-09-26
* **obecni:** Damian Polwonien, Gustaf Profnos, Jan Łowicz, Kinga Kruk, Melinda Teilert, Natasza Aniel, Tomasz Tukan

Streszczenie:

Próba kradzieży figurki z Fantasmagorii doprowadziła Zespół do Melindy, która chce pomóc delikwentowi który stracił majątek. Janek decyduje się usunąć swoją stalkerkę raz na zawsze - ale jak poznał ją bliżej, zrobiło mu się jej żal. Okazało się, że owa figurka jest magiczną anomalią zdolną do niszczenia dokumentów. Kinga stworzyła jej fałszywki by nikt nie był w stanie ich użyć, a Natasza sprzedała Tukanowi możliwość użycia za ochronę i kontakty w Aurum.

Aktor w Opowieści:

* Dokonanie:
    * nie jest tylko głupią stalkerką, wyrolowała Jana przez kooperację z Nataszą; okazuje się, że uciekła z domu przed aranżowanym małżeństwem (dzięki Rekinom) i próbuje stanąć na swoim. Chce pomóc słabszym.
* Progresja:
    * jednak jest uboższa niż się zdaje; uciekła z domu przed aranżowanym małżeństwem i boi się, że ją znajdą i ściągną z powrotem. "Odpad rodu", bo nie jest magiem.
    * powiązana z gangiem Latających Rekinów; dzięki nim uciekła i dostała się do Podwiertu (gdzie nie radzi sobie z życiem).
    * zapewniła sobie ochronę Jana Łowicza + to, że on będzie uczył jej walczyć.
    * zauroczona i podkochuje się w Janie Łowiczu. Niestety dla niego.


### Lojalna Zdrajczyni

* **uid:** 200423-lojalna-zdrajczyni, _numer względny_: 1
* **daty:** 0109-07-21 - 0109-07-24
* **obecni:** Duży Tom, Felicja Melitniek, Jan Łowicz, Katja Nowik, Kinga Kruk, Melinda Teilert, Natasza Aniel

Streszczenie:

Felicja - lojalna przynętka Dużego Toma - okradła owego Toma. Zespół się z nią spotkał i odkrył, że Felicja jest pod wpływem mimika symbiotycznego. Z pomocą Dużego Toma dali radę Felicję przekonać, żeby odrzuciła mimika. Niestety, pozycja Toma została nadwątlona - oraz nadal nie wiadomo kto wysłał mu mimika. Podwiert przygotowuje się na starcie gangów - Latających Rekinów (wsparcie Aurum), TomStone (lokalni) i Technożerców (militaryści i antypostępowcy).

Aktor w Opowieści:

* Dokonanie:
    * młoda bogaczka z Aurum (nie czarodziejka), która chce dołączyć do Janka i być groźnym łowcą potworów. Lub ludzi. Nieważne - chce być groźna


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 6, @: 0110-10-06
    1. Primus    : 6, @: 0110-10-06
        1. Sektor Astoriański    : 6, @: 0110-10-06
            1. Astoria    : 6, @: 0110-10-06
                1. Sojusz Letejski    : 6, @: 0110-10-06
                    1. Szczeliniec    : 6, @: 0110-10-06
                        1. Powiat Jastrzębski    : 1, @: 0110-10-06
                            1. Praszalek, okolice    : 1, @: 0110-10-06
                                1. Fabryka schronień Defensor    : 1, @: 0110-10-06
                                1. Park rozrywki Janor    : 1, @: 0110-10-06
                            1. Praszalek    : 1, @: 0110-10-06
                                1. Komenda policji    : 1, @: 0110-10-06
                        1. Powiat Pustogorski    : 5, @: 0110-09-12
                            1. Podwiert    : 5, @: 0110-09-12
                                1. Dolina Biurowa    : 1, @: 0109-10-20
                                1. Klub Arkadia    : 1, @: 0110-09-12
                                1. Kompleks Korporacyjny    : 1, @: 0109-07-24
                                    1. Chemiczna firma Kurara    : 1, @: 0109-07-24
                                1. Magazyny sprzętu ciężkiego    : 2, @: 0109-10-20
                                    1. Klub Napartar    : 1, @: 0109-10-20
                                1. Osiedle Leszczynowe    : 4, @: 0109-11-17
                                    1. Klub Arkadia    : 2, @: 0109-11-17
                                    1. Sklep z reliktami Fantasmagoria    : 3, @: 0109-11-17
                                1. Sensoplex    : 1, @: 0109-10-20
                                    1. Koloseum    : 1, @: 0109-10-20
                                    1. Labirynt    : 1, @: 0109-10-20

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Diana Lemurczak      | 3 | ((200513-trzyglowec-kontra-melinda; 200520-figurka-a-kopie-zapasowe; 200524-dom-dla-melindy)) |
| Jan Łowicz           | 3 | ((200423-lojalna-zdrajczyni; 200507-anomalna-figurka-zabboga; 200513-trzyglowec-kontra-melinda)) |
| Kinga Kruk           | 3 | ((200423-lojalna-zdrajczyni; 200507-anomalna-figurka-zabboga; 200513-trzyglowec-kontra-melinda)) |
| Natasza Aniel        | 3 | ((200423-lojalna-zdrajczyni; 200507-anomalna-figurka-zabboga; 200513-trzyglowec-kontra-melinda)) |
| Diana Tevalier       | 2 | ((200524-dom-dla-melindy; 200913-haracz-w-parku-rozrywki)) |
| Feliks Keksik        | 2 | ((200513-trzyglowec-kontra-melinda; 200520-figurka-a-kopie-zapasowe)) |
| Gerwazy Lemurczak    | 2 | ((200520-figurka-a-kopie-zapasowe; 200524-dom-dla-melindy)) |
| Katja Nowik          | 2 | ((200423-lojalna-zdrajczyni; 200513-trzyglowec-kontra-melinda)) |
| Mariusz Grabarz      | 2 | ((200513-trzyglowec-kontra-melinda; 200520-figurka-a-kopie-zapasowe)) |
| Mariusz Trzewń       | 2 | ((200524-dom-dla-melindy; 200913-haracz-w-parku-rozrywki)) |
| Pięknotka Diakon     | 2 | ((200524-dom-dla-melindy; 200913-haracz-w-parku-rozrywki)) |
| Adam Cześń           | 1 | ((200513-trzyglowec-kontra-melinda)) |
| Adam Janor           | 1 | ((200913-haracz-w-parku-rozrywki)) |
| Alan Bartozol        | 1 | ((200913-haracz-w-parku-rozrywki)) |
| Aranea Diakon        | 1 | ((200520-figurka-a-kopie-zapasowe)) |
| Damian Polwonien     | 1 | ((200507-anomalna-figurka-zabboga)) |
| Duży Tom             | 1 | ((200423-lojalna-zdrajczyni)) |
| Felicja Melitniek    | 1 | ((200423-lojalna-zdrajczyni)) |
| Franciszek Owadowiec | 1 | ((200913-haracz-w-parku-rozrywki)) |
| Franek Bulterier     | 1 | ((200524-dom-dla-melindy)) |
| Gustaf Profnos       | 1 | ((200507-anomalna-figurka-zabboga)) |
| Karol Senonik        | 1 | ((200913-haracz-w-parku-rozrywki)) |
| Krystyna Senonik     | 1 | ((200913-haracz-w-parku-rozrywki)) |
| Laura Tesinik        | 1 | ((200524-dom-dla-melindy)) |
| Maja Janor           | 1 | ((200913-haracz-w-parku-rozrywki)) |
| Rafał Kamaron        | 1 | ((200913-haracz-w-parku-rozrywki)) |
| Rafał Torszecki      | 1 | ((200513-trzyglowec-kontra-melinda)) |
| Tomasz Tukan         | 1 | ((200507-anomalna-figurka-zabboga)) |
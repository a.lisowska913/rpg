# Izabela Zarantel
## Identyfikator

Id: 2109-izabela-zarantel

## Sekcja Opowieści

### To nie pułapka na Nereidę...

* **uid:** 220105-to-nie-pulapka-na-nereide, _numer względny_: 16
* **daty:** 0112-02-28 - 0112-03-04
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Izabela Zarantel, Kamil Lyraczek, Klaudia Stryk, Lutus Amerin, Maria Naavas, Natalia Aradin, Saitaer

Streszczenie:

Plan - dorwanie tajemniczych koloidowych korwet. Wiemy, że to pułapka, więc zróbmy kontr-pułapkę. Ale okazało się, że to nie pułapka na Nereidę a na Infernię. Standardowe badania wprowadziły w Infernię (Dianę) memetic payload i Diana prawie wpadła pod kontrolę Saitaera - odrzucenie kokonu z Natalią sprawiło, że udało się ciężko uszkodzoną Infernię uratować. Zwalczając ixion Arianna sięgnęła do Esuriit przez Elenę; Infernia by zginęła gdyby nie samobójcza akcja Izabeli i Kamila. Elena wchłonęła tyle energii ile była w stanie, przekraczając swój Wzór. ALE - Infernia przetrwała. Przy okazji, Natalia się wykluła. Ani osoba ani statek kosmiczny - mordercza anomalia kosmiczna.

Aktor w Opowieści:

* Dokonanie:
    * widząc że Infernia umiera i energie przechodzą poza kontrolę przekierowała (z pomocą Klaudii) całość energii na siebie jak kiedyś Martyn. Po czym opuściła Infernię. Bez skafandra. KIA.


### Kult Saitaera w Neotik

* **uid:** 211222-kult-saitaera-w-neotik, _numer względny_: 15
* **daty:** 0112-02-24 - 0112-02-25
* **obecni:** Adam Szarjan, Arianna Verlen, Diana d'Infernia, Elena Verlen, Eustachy Korkoran, Izabela Zarantel, Kasandra Destrukcja Diakon, Klaudia Stryk, Lutus Amerin, Maciek Kalmarzec, Maria Naavas

Streszczenie:

Klaudia wróciła z bronią anty-Serenitową od Tosena, Arianna z feromonami Marii nadała Inferni nową kulturę - miłośnicy lolitek x kultyści x noktianie. Elena uziemiona w Inferni zinfiltrowała Stocznię ukryta przez Klaudię i odkryła niższe Spustoszenie + kult Saitaera. Nereida jest czysta od Saitaera; przetransferowana na Infernię dla Eleny i uzbrojona. Klaudia ma listę jednostek potencjalnie w rękach Saitaera / Spustoszonych. Przygotowania do kontrataku skończone.

Aktor w Opowieści:

* Dokonanie:
    * pomaga Ariannie w odpowiednim natężeniem efektowności by zrobić najlepszy speech ever.


### Prototypowa Nereida Natalii

* **uid:** 211124-prototypowa-nereida-natalii, _numer względny_: 14
* **daty:** 0112-02-14 - 0112-02-18
* **obecni:** Adam Szarjan, Arianna Verlen, Arkadia Verlen, Elena Verlen, Eustachy Korkoran, Izabela Zarantel, Klaudia Stryk, Leona Astrienko, Maria Naavas, Natalia Aradin, OO Infernia, Roland Sowiński, Wawrzyn Rewemis

Streszczenie:

Kampania reklamowa "dołącz do Inferni" zrobiona przez Izę przy użyciu Eustachego jako modela - sukces. Klaudia zdobyła z tego nowych załogantów (acz to jeszcze potrwa). Szarjan uwolnił Klaudię z zarzutów, potrzebuje pomocy w stoczni - jego przyjaciółka Natalia ma problem z prototypem myśliwca klasy Nereida i została ixiońsko zintegrowana. Infernia próbowała Nereidę uratować, ale niestety skończyło się to poważnym uszkodzeniem Nereidy i stworzeniem ixiońskiego kokonu, z którego Natalia musi się wykluć...

Aktor w Opowieści:

* Dokonanie:
    * zrobiła świetną kampanię "dołącz do Inferni" by zdobyć załogę. Poszło jej rewelacyjnie - podniosła zwłaszcza szacun Eustachemu.


### Porwany Trismegistos

* **uid:** 211117-porwany-trismegistos, _numer względny_: 13
* **daty:** 0112-02-09 - 0112-02-11
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Izabela Zarantel, Jamon Korab, Kalira d'Trismegistos, Klara Gwozdnik, Klaudia Stryk, Leona Astrienko, Maria Naavas, Mira Anastel, OO Tivr, Roland Sowiński, SC Trismegistos, Zygfryd Maus

Streszczenie:

Wiadomość od pirata że porwał Trismegistos poderwała Ariannę i Tivr. Historia nie trzymała się kupy. Załoga Inferni (Tivru) zorientowała się, że coś jest nie tak - opowieść o statku kupieckim który pozbył się miragenta brzmiała DZIWNIE. Okazało się, że Trismegistos jest statkiem kaperskim. Gdy Trismegistos zażądał poddania się Tivru Klaudia zrobiła sygnał "lol leci na Was Serenit" i TAI Trismegistosa spanikowała. Wolna TAI, współpracująca z fareilem i drakolitką. Wolny dystrykt. Arianna dała im żyć - ale odzyskała porwanych arystokratów i komory adaptacji.

Aktor w Opowieści:

* Dokonanie:
    * zrobiła odcinek Sekretów o Eustachym i Klaudii i BARDZO skupiła się na tym by pokazać Klaudię w jak najlepszej stronie by pomóc w oskarżeniach. Wzięła dyshonor na siebie. To najbardziej pomoże Inferni, Klaudii i Ariannie.
* Progresja:
    * stworzyła narrację, że to ona wrobiła Klaudię w hackowanie Rdzenia Sprzężonego na K1. Ma reputację "miłośniczki TAI która nienawidzi Eterni". Ma groźnych wrogów.


### Rzieza niszczy Infernię

* **uid:** 211027-rzieza-niszczy-infernie, _numer względny_: 12
* **daty:** 0112-01-22 - 0112-01-27
* **obecni:** Arianna Verlen, Elena Verlen, Flawia Blakenbauer, Klaudia Stryk, Martyn Hiwasser, Otto Azgorn, TAI Rzieza d'K1

Streszczenie:

Infernia wraca z Mevilig. Rzieza chce wyczyścić im pamięć o sobie. Arianna się opiera. Rzieza dowiaduje się o Ataienne. Arianna budzi Elenę po sympatii, Elena sprowadza Esuriit do laboratorium dekontaminacyjnego K1. Dużo śmierci. Martyn próbuje to opanować - budzi swoje simulacrum. Arianna i Klaudia opanowują przy pomocy Rziezy sytuację. 37% załogi Inferni nie żyje, Flawia wyssana w kosmos, Elena złamana, Martyn w szpitalu.

Aktor w Opowieści:

* Dokonanie:
    * AKTOR NIEOBECNY NA SESJI
* Progresja:
    * uwierzyła w to, że Arianna Verlen jest prawdziwą inkarnacją aspektu Zbawiciela. To jest jedyne, co ma sens w kontekście tego WSZYSTKIEGO. Jest wyznawcą. To Arianna zażyczyła sobie, by Izabela przetrwała to wszystko - i dlatego Izabela żyje.


### Kurczakownia

* **uid:** 211020-kurczakownia, _numer względny_: 11
* **daty:** 0112-01-18 - 0112-01-20
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Flawia Blakenbauer, Kamil Lyraczek, Klaudia Stryk, Martyn Hiwasser, OO Infernia, Otto Azgorn, Vigilus Mevilig

Streszczenie:

By uratować jak najwięcej osób w Sektorze Mevilig, Eustachy zaprojektował Kurczakownię - maszynę do oddzielania mechanicznego głów i rdzenia od reszty człowieka. Arianna starła się ze Zbawicielem-Niszczycielem jako aspekt Zbawiciela, wspierana przez Działo Rozpaczy (Eustachy). Udało im się sporo uratować (40% populacji Planetoidy Kalarfam), acz załoga Inferni będzie mieć koszmary senne. Klaudia zebrała dość danych, by zapobiec naturalnej manifestacji Zbawiciela-Niszczyciela w Sektorze Astoriańskim i zrobić kontrakcję jak co.

Aktor w Opowieści:

* Dokonanie:
    * AKTOR NIEOBECNY NA SESJI
* Progresja:
    * OGROMNA TRAUMA. Kurczakowanie - rekurczakowanie + Zbawiciel-Niszczyciel. Nie jest w stanie sobie z tym poradzić. Flashbacki itp. Potworność wymaga religii?


### Ostatnia akcja bohaterki

* **uid:** 210922-ostatnia-akcja-bohaterki, _numer względny_: 10
* **daty:** 0111-12-19 - 0112-01-03
* **obecni:** Antoni Kramer, Arianna Verlen, Elena Verlen, Izabela Zarantel, Jolanta Kopiec, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Olgierd Drongon, OO Żelazko, Roland Sowiński

Streszczenie:

Z Martynem skontaktowała się tien Kopiec prosząc o ostatnią misję zanim Esuriit ją pożre. Martyn zorganizował lot Infernią. Ustabilizował Jolantę, by ta dała radę dolecieć słowami i opowieściami, przy okazji tworząc piękną opowieść o Eterni i eulogię dla Jolanty - bohaterki wojennej - w Sekretach Orbitera (przy okazji wyszła jego przeszłość). Infernia zniszczyła Krwawą Bazę Piratów kosztem dewastacji Żelazka. Jolanta zniszczyła krwawego maga. Sektor jest bezpieczniejszy, Eternia szczęśliwa a Arianna ma profity.

Aktor w Opowieści:

* Dokonanie:
    * nienawidzi Eterni - ale stworzyła najlepszy odcinek o Eterni jak to było możliwe. Jej opus magnum. Teraz się szczerze nienawidzi.


### Admirał Gruby Wieprz

* **uid:** 211114-admiral-gruby-wieprz, _numer względny_: 9
* **daty:** 0111-12-11 - 0111-12-16
* **obecni:** Adalbert Brześniak, Izabela Zarantel, Klaudia Stryk, Rafael Galwarn, Roman Panracz, Rzieza d'K1, Sabina Kazitan

Streszczenie:

Klaudia na K1 dostała dziwny SOS. Okazało się, że pochodzi z K1 - z multivirtu. Virtsystem gry Gwiezdny Podbój uzyskał życie. Klaudia zmontowała akcję ratunkową i z Adalbertem (współpracujący z Sabiną Kazitan), Izą i Romanem zaczęli przenosić TAI do ewakuacji. Niestety, Klaudia przypadkiem Skaziła Połączony Rdzeń na K1 (śledztwo i poważne zarzuty) a Iza chcąc dać Wieprzowi głos trafiła na celownik Rziezy, który ją ośmieszył a Wieprza pokazał jako żywą TAI - psychopatę...

Aktor w Opowieści:

* Dokonanie:
    * była zmęczona, nie chciała kolejnego wywiadu. Klaudia przekonała ją do pomocy, będzie skandal. Iza pomogła Klaudii opóźnić turniej Gwiezdnego Podboju i zrobiła wywiad z TAI Gruby Wieprz. Ale Rzieza zniszczył Wieprza i zrobił z niego potwora na wizji a z niej zrobił fanatyczkę wolności i opętaną ideałami idiotkę.
* Progresja:
    * podjęła decyzję - nie chce być częścią Orbitera czy Aurum itp. Nie chce być freelancerem. Jej czyny - czuje się częścią Inferni. Nie jest już całkowicie niezależna i nie jest jej z tym źle. Oddała część niezależności o którą zawsze walczyła.
    * traci część dostępów w Orbiterze i część sympatii w Aurum. Kontakty ma, ale nie jest już uznawana za freelancerkę.
    * osobisty WRÓG tych, którzy chcą korzystać z AI jako narzędzi. "Głupia propagandzistka, goni ambulanse".
    * przez Rziezę wyszła na fanatyczkę wolności. Zwalcza Eternię. Robi dziwne połączenia Eternia - zniewolone TAI? Ogólnie, "dziwna".


### Nieudana infiltracja Inferni

* **uid:** 210616-nieudana-infiltracja-inferni, _numer względny_: 8
* **daty:** 0111-11-22 - 0111-11-27
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Flawia Blakenbauer, Izabela Zarantel, Kamil Lyraczek, Klaudia Stryk, Leona Astrienko, Marian Tosen, OO Opresor, Roland Sowiński

Streszczenie:

Marian Tosen poprosił Infernię o to, by sprawdzili co się dzieje w Anomalii Kolapsu - podejrzewa anomalie memetyczne, skoro Orbiter traci tam statki  (m.in. OO Opresor) i pojawiają się jednostki nieoznaczonej frakcji. Tymczasem Roland Sowiński chce na pokład. Arianna wybiła mu to z głowy, ale wykryli agentkę wśród jego guwernantek - Flawię Blakenbauer, która miała się nim opiekować i jest trochę zniewolona przez Sowińskich. By zadośćuczynić po tym co Elena zrobiła i trochę Flawii pomóc, Flawia dołączyła do Inferni. "Oficjalnie" udało jej się Infernię zinfiltrować...

Aktor w Opowieści:

* Dokonanie:
    * wróciła na stałe do załogi Inferni. Będzie pracować nad ciągiem dalszym Sekretów Orbitera.


### Listy od fanów

* **uid:** 210630-listy-od-fanow, _numer względny_: 7
* **daty:** 0111-11-10 - 0111-11-13
* **obecni:** Arianna Verlen, Bogdan Anatael, Elena Verlen, Izabela Zarantel, Klaudia Stryk, Michał Teriakin, OE Lord Savaron, Olgierd Drongon, Rafael Galwarn, Remigiusz Falorin, TAI Rzieza d'K1, TAI XT-723 d'K1, TAI Zefiris

Streszczenie:

Na K1 znajduje się tajna baza Eterni, we współpracy z niektórymi elementami K1. Jeden z przekształcanych tam w Pilota chłopców wysłał fanmail do Arianny ("chce się spotkać zanim zginie na froncie"). Arianna z Klaudią znalazły obecność bazy Eterni, pozyskały Eidolona z Eterni (za PR), użyły Eidolona do infiltracji tej bazy, użyły Rziezy do zniszczenia tej bazy a wina spadła na Olgierda (który dla Arianny robił niedaleko manewry). Młodzi Piloci zostali odratowani; nie wiedzą o jaki front chodzi Eterni, ale jedno jest pewne - Eternia szykuje się do wojny kosmicznej. Ale z kim?

Aktor w Opowieści:

* Dokonanie:
    * pochodzi z Eterni; jej rodzina nadal tam jest. Jej udało się uciec. Nienawidzi Eterni. Pomaga Ariannie przekonać Remigiusza Falorina by przekazał Ariannie Eidolona.


### Sabotaż świni

* **uid:** 201104-sabotaz-swini, _numer względny_: 6
* **daty:** 0111-01-13 - 0111-01-16
* **obecni:** Anastazja Sowińska, Arianna Verlen, Ataienne, Diana Arłacz, Eustachy Korkoran, Klaudia Stryk, Martyn Hiwasser, Rafał Armadion, Robert Garwen

Streszczenie:

Sabotażysta - miragent - podjął próbę zniszczenia Arianny gdy ta ładowała świnie na Wesołego Wieprzka używając wybuchowej, anomalnej świni. Zespół przetrwał, choć "Wieprzek" został zniszczony. Arianna pojmała miragenta i przekazała go do Kontrolera Pierwszego, acz kosztem ofiar w ludziach. No i nie było dość świń by je wziąć na orbitę XD.

Aktor w Opowieści:

* Dokonanie:
    * AKTOR NIEOBECNY NA SESJI
* Progresja:
    * ALBO poprawi reputację Anastazji (która Iza w sumie uszkodziła), ALBO Sowińscy wsadzą ją do karceru na długie lata.


### Noktianie rodu Arłacz

* **uid:** 201021-noktianie-rodu-arlacz, _numer względny_: 5
* **daty:** 0111-01-07 - 0111-01-10
* **obecni:** Anastazja Sowińska, Arianna Verlen, Ataienne, Diana Arłacz, Eliza Ira, Eustachy Korkoran, Izabela Zarantel, Jolanta Arłacz, Juliusz Sowiński, Klaudia Stryk, Klaus Rumak, Maria Gołąb, OO Szalony Rumak, OO Wesoły Wieprzek, Robert Arłacz, Wanessa Pyszcz

Streszczenie:

By uzyskać współpracę Elizy Iry, Arianna zdecydowała się "porwać" noktian rodu Arłacz wykorzystywanych do produkcji czystej krwi dla magów. Z uwagi na działania Marii Gołąb plan został odrzucony i wykryty, ale znalezienie haków na Arłaczy rozwiązało ten problem - Arianna i Arłacze współpracują. Nie doszło do porozumienia z Elizą Irą, ale Eliza się wycofała z Trzeciego Raju, ogłaszając się protektorką tego terenu. Sytuacja w miarę ustabilizowana.

Aktor w Opowieści:

* Dokonanie:
    * storzyła świetną opowieść i odcinek o pomocy noktianom ze strony Arianny i Anastazji.


### Magiczna burza w Raju

* **uid:** 200923-magiczna-burza-w-raju, _numer względny_: 4
* **daty:** 0110-12-24 - 0110-12-28
* **obecni:** Anastazja Sowińska, Arianna Verlen, Dariusz Krantak, Elena Verlen, Eliza Ira, Eustachy Korkoran, Izabela Zarantel, Klaudia Stryk, Marian Fartel, Marianna Lemurczak, Nikodem Sowiński

Streszczenie:

Zbliża się burza magiczna. Nie ma schronienia ani zapasów. Zespół wysłał SOS na Kontroler i do Nikodema Sowińskiego (Anastazja). Zbudowali co się da by odeprzeć burzę magiczną i im się to udało bez strat w ludziach. O dziwo, siła która im pomogła to Eliza Ira (acz dyskretnie). Anastazja wierzy że Nikodem jest jej najlepszym przyjacielem, ale on pragnie jej zguby...

Aktor w Opowieści:

* Dokonanie:
    * zmontowała genialne prośby o pomoc plus reportaż, dzięki którym Raj dostanie podstawowe zapasy i możliwości działania.


### Śmierć Raju

* **uid:** 200916-smierc-raju, _numer względny_: 3
* **daty:** 0110-12-21 - 0110-12-23
* **obecni:** Anastazja Sowińska, Arianna Verlen, Ataienne, Celina Szilat, Elena Verlen, Eustachy Korkoran, Izabela Zarantel, Klaudia Stryk, Marian Fartel, Rafał Armadion, Robert Garwen, Wanessa Pyszcz

Streszczenie:

Najważniejszym problemem Raju jest pozbycie się bombardowania nojrepów. Arianna zaproponowała zuchwały pomysł - w kontrolowany sposób niech nojrepy wejdą do miasteczka i wezmą czego szukają. Udało jej się wszystkich przekonać. Sukces - nojrepy się wycofały, ale Tucznik nie jest zdolny do latania a Trzeci Raj jest zniszczony. Została tylko Ataienne i ruina. Ataienne rozpaliła hipnotycznie morale trzeciorajowców - przetrwają to wszystko.

Aktor w Opowieści:

* Dokonanie:
    * najpierw Eustachy odwrócił jej uwagę od świńskiego skandalu, potem udokumentowała zniszczenie Trzeciego Raju przez dziwne nojrepy.


### Arystokratka w ładowni na świnie

* **uid:** 200909-arystokratka-w-ladowni-na-swinie, _numer względny_: 2
* **daty:** 0110-12-15 - 0110-12-20
* **obecni:** Anastazja Sowińska, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Izabela Zarantel, Kamil Lyraczek, Klaudia Stryk, Martyn Hiwasser, OO Galaktyczny Tucznik

Streszczenie:

Zespół wyciągnął Martyna z więzienia (legalnie). Wpakowali się na Galaktycznego Tucznika by przenieść świnie na orbitę... ale poleciała z nimi dziennikarka Izabela (o czym wiedzą) i 15letnia Anastazja Sowińska (o czym nie wiedzą). Przy lądowaniu, atak nojrepów na Tucznika spowodował katastrofę - seria Paradoksów Anastazji, uszkodzenia Tucznika itp. Zespół uratował Anastazję (choć kosztem ran Martyna) i wylądował Tucznikiem na Astorii. To jest sukces.

Aktor w Opowieści:

* Dokonanie:
    * kontynuuje "Sekrety Orbitera", wyczuła złoto. Dołączyła do załogi Inferni (teraz: Tucznika). Została ranna przy wybuchach na Tuczniku przez nojrepy.


### Sekrety Orbitera - historia prawdziwa

* **uid:** 200819-sekrety-orbitera-historia-prawdziwa, _numer względny_: 1
* **daty:** 0110-11-26 - 0110-12-04
* **obecni:** Arianna Verlen, Damian Orion, Elena Verlen, Eustachy Korkoran, Izabela Zarantel, Klaudia Stryk, Leona Astrienko, Leszek Kurzmin, Martyn Hiwasser, Sabina Servatel, Tadeusz Ursus

Streszczenie:

Elena wyzwała Leonę na pojedynek. Klaudia i Arianna wkręciły Tadeusza. Tadeusz - arcymag Eterni - używa Simulacrum w walce, więc Zespół zaczął montować plan jak Leona może jednak wygrać z Simulacrum. I wszystko skończyło się tym, że Leona wygrała, Elena jest wolna od Tadeusza i powstała holodrama "Sekrety Orbitera"...

Aktor w Opowieści:

* Dokonanie:
    * dziennikarka i prządka opowieści na Orbiterze, która stworzyła absolutny hit - "Sekrety Orbitera" ku wielkiej żałości Admiralicji i Inferni.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 13, @: 0112-03-04
    1. Multivirt    : 1, @: 0111-12-16
    1. Primus    : 13, @: 0112-03-04
        1. Sektor Astoriański    : 13, @: 0112-03-04
            1. Astoria, Orbita    : 7, @: 0112-02-18
                1. Kontroler Pierwszy    : 7, @: 0112-02-18
                    1. Arena Kalaternijska    : 1, @: 0110-12-04
                    1. Hangary Alicantis    : 1, @: 0111-11-27
                    1. Połączony Rdzeń    : 1, @: 0111-12-16
            1. Astoria, Pierścień Zewnętrzny    : 3, @: 0112-03-04
                1. Poligon Stoczni Neotik    : 2, @: 0112-03-04
                1. Stocznia Neotik    : 3, @: 0112-03-04
            1. Astoria    : 4, @: 0111-01-10
                1. Sojusz Letejski, NW    : 4, @: 0111-01-10
                    1. Ruiniec    : 4, @: 0111-01-10
                        1. Ortus-Conticium    : 1, @: 0110-12-28
                        1. Pustynia Kryształowa    : 1, @: 0111-01-10
                        1. Trzeci Raj, okolice    : 2, @: 0110-12-23
                            1. Kopiec Nojrepów    : 2, @: 0110-12-23
                            1. Zdrowa Ziemia    : 1, @: 0110-12-23
                        1. Trzeci Raj    : 3, @: 0111-01-10
                            1. Barbakan    : 1, @: 0110-12-23
                            1. Centrala Ataienne    : 1, @: 0110-12-23
                            1. Mały Kosmoport    : 1, @: 0110-12-23
                            1. Ratusz    : 1, @: 0110-12-23
                            1. Stacja Nadawcza    : 1, @: 0110-12-23
                1. Sojusz Letejski    : 1, @: 0111-01-10
                    1. Aurum    : 1, @: 0111-01-10
                        1. Powiat Niskowzgórza    : 1, @: 0111-01-10
                            1. Domena Arłacz    : 1, @: 0111-01-10
                                1. Posiadłość Arłacz    : 1, @: 0111-01-10
                                    1. Baterie Puryfikacji Krwi    : 1, @: 0111-01-10
                                    1. Małe lądowisko    : 1, @: 0111-01-10
            1. Krwawa Baza Piracka    : 1, @: 0112-01-03
            1. Neikatis    : 1, @: 0112-02-11
                1. Dystrykt Lennet    : 1, @: 0112-02-11

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Klaudia Stryk        | 13 | ((200819-sekrety-orbitera-historia-prawdziwa; 200909-arystokratka-w-ladowni-na-swinie; 200916-smierc-raju; 200923-magiczna-burza-w-raju; 201021-noktianie-rodu-arlacz; 210616-nieudana-infiltracja-inferni; 210630-listy-od-fanow; 210922-ostatnia-akcja-bohaterki; 211114-admiral-gruby-wieprz; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide)) |
| Arianna Verlen       | 12 | ((200819-sekrety-orbitera-historia-prawdziwa; 200909-arystokratka-w-ladowni-na-swinie; 200916-smierc-raju; 200923-magiczna-burza-w-raju; 201021-noktianie-rodu-arlacz; 210616-nieudana-infiltracja-inferni; 210630-listy-od-fanow; 210922-ostatnia-akcja-bohaterki; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide)) |
| Elena Verlen         | 11 | ((200819-sekrety-orbitera-historia-prawdziwa; 200909-arystokratka-w-ladowni-na-swinie; 200916-smierc-raju; 200923-magiczna-burza-w-raju; 210616-nieudana-infiltracja-inferni; 210630-listy-od-fanow; 210922-ostatnia-akcja-bohaterki; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide)) |
| Eustachy Korkoran    | 10 | ((200819-sekrety-orbitera-historia-prawdziwa; 200909-arystokratka-w-ladowni-na-swinie; 200916-smierc-raju; 200923-magiczna-burza-w-raju; 201021-noktianie-rodu-arlacz; 210616-nieudana-infiltracja-inferni; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide)) |
| Leona Astrienko      | 5 | ((200819-sekrety-orbitera-historia-prawdziwa; 210616-nieudana-infiltracja-inferni; 210922-ostatnia-akcja-bohaterki; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii)) |
| Anastazja Sowińska   | 4 | ((200909-arystokratka-w-ladowni-na-swinie; 200916-smierc-raju; 200923-magiczna-burza-w-raju; 201021-noktianie-rodu-arlacz)) |
| Maria Naavas         | 4 | ((211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide)) |
| Roland Sowiński      | 4 | ((210616-nieudana-infiltracja-inferni; 210922-ostatnia-akcja-bohaterki; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii)) |
| Kamil Lyraczek       | 3 | ((200909-arystokratka-w-ladowni-na-swinie; 210616-nieudana-infiltracja-inferni; 220105-to-nie-pulapka-na-nereide)) |
| Martyn Hiwasser      | 3 | ((200819-sekrety-orbitera-historia-prawdziwa; 200909-arystokratka-w-ladowni-na-swinie; 210922-ostatnia-akcja-bohaterki)) |
| Adam Szarjan         | 2 | ((211124-prototypowa-nereida-natalii; 211222-kult-saitaera-w-neotik)) |
| Ataienne             | 2 | ((200916-smierc-raju; 201021-noktianie-rodu-arlacz)) |
| Eliza Ira            | 2 | ((200923-magiczna-burza-w-raju; 201021-noktianie-rodu-arlacz)) |
| Lutus Amerin         | 2 | ((211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide)) |
| Marian Fartel        | 2 | ((200916-smierc-raju; 200923-magiczna-burza-w-raju)) |
| Natalia Aradin       | 2 | ((211124-prototypowa-nereida-natalii; 220105-to-nie-pulapka-na-nereide)) |
| Olgierd Drongon      | 2 | ((210630-listy-od-fanow; 210922-ostatnia-akcja-bohaterki)) |
| Rafael Galwarn       | 2 | ((210630-listy-od-fanow; 211114-admiral-gruby-wieprz)) |
| Wanessa Pyszcz       | 2 | ((200916-smierc-raju; 201021-noktianie-rodu-arlacz)) |
| Adalbert Brześniak   | 1 | ((211114-admiral-gruby-wieprz)) |
| Antoni Kramer        | 1 | ((210922-ostatnia-akcja-bohaterki)) |
| Arkadia Verlen       | 1 | ((211124-prototypowa-nereida-natalii)) |
| Bogdan Anatael       | 1 | ((210630-listy-od-fanow)) |
| Celina Szilat        | 1 | ((200916-smierc-raju)) |
| Damian Orion         | 1 | ((200819-sekrety-orbitera-historia-prawdziwa)) |
| Dariusz Krantak      | 1 | ((200923-magiczna-burza-w-raju)) |
| Diana Arłacz         | 1 | ((201021-noktianie-rodu-arlacz)) |
| Diana d'Infernia     | 1 | ((211222-kult-saitaera-w-neotik)) |
| Flawia Blakenbauer   | 1 | ((210616-nieudana-infiltracja-inferni)) |
| Jamon Korab          | 1 | ((211117-porwany-trismegistos)) |
| Jolanta Arłacz       | 1 | ((201021-noktianie-rodu-arlacz)) |
| Jolanta Kopiec       | 1 | ((210922-ostatnia-akcja-bohaterki)) |
| Juliusz Sowiński     | 1 | ((201021-noktianie-rodu-arlacz)) |
| Kalira d'Trismegistos | 1 | ((211117-porwany-trismegistos)) |
| Kasandra Destrukcja Diakon | 1 | ((211222-kult-saitaera-w-neotik)) |
| Klara Gwozdnik       | 1 | ((211117-porwany-trismegistos)) |
| Klaus Rumak          | 1 | ((201021-noktianie-rodu-arlacz)) |
| Leszek Kurzmin       | 1 | ((200819-sekrety-orbitera-historia-prawdziwa)) |
| Maciek Kalmarzec     | 1 | ((211222-kult-saitaera-w-neotik)) |
| Maria Gołąb          | 1 | ((201021-noktianie-rodu-arlacz)) |
| Marian Tosen         | 1 | ((210616-nieudana-infiltracja-inferni)) |
| Marianna Lemurczak   | 1 | ((200923-magiczna-burza-w-raju)) |
| Michał Teriakin      | 1 | ((210630-listy-od-fanow)) |
| Mira Anastel         | 1 | ((211117-porwany-trismegistos)) |
| Nikodem Sowiński     | 1 | ((200923-magiczna-burza-w-raju)) |
| OE Lord Savaron      | 1 | ((210630-listy-od-fanow)) |
| OO Galaktyczny Tucznik | 1 | ((200909-arystokratka-w-ladowni-na-swinie)) |
| OO Infernia          | 1 | ((211124-prototypowa-nereida-natalii)) |
| OO Opresor           | 1 | ((210616-nieudana-infiltracja-inferni)) |
| OO Szalony Rumak     | 1 | ((201021-noktianie-rodu-arlacz)) |
| OO Tivr              | 1 | ((211117-porwany-trismegistos)) |
| OO Wesoły Wieprzek   | 1 | ((201021-noktianie-rodu-arlacz)) |
| OO Żelazko           | 1 | ((210922-ostatnia-akcja-bohaterki)) |
| Rafał Armadion       | 1 | ((200916-smierc-raju)) |
| Remigiusz Falorin    | 1 | ((210630-listy-od-fanow)) |
| Robert Arłacz        | 1 | ((201021-noktianie-rodu-arlacz)) |
| Robert Garwen        | 1 | ((200916-smierc-raju)) |
| Roman Panracz        | 1 | ((211114-admiral-gruby-wieprz)) |
| Rzieza d'K1          | 1 | ((211114-admiral-gruby-wieprz)) |
| Sabina Kazitan       | 1 | ((211114-admiral-gruby-wieprz)) |
| Sabina Servatel      | 1 | ((200819-sekrety-orbitera-historia-prawdziwa)) |
| Saitaer              | 1 | ((220105-to-nie-pulapka-na-nereide)) |
| SC Trismegistos      | 1 | ((211117-porwany-trismegistos)) |
| Tadeusz Ursus        | 1 | ((200819-sekrety-orbitera-historia-prawdziwa)) |
| TAI Rzieza d'K1      | 1 | ((210630-listy-od-fanow)) |
| TAI XT-723 d'K1      | 1 | ((210630-listy-od-fanow)) |
| TAI Zefiris          | 1 | ((210630-listy-od-fanow)) |
| Wawrzyn Rewemis      | 1 | ((211124-prototypowa-nereida-natalii)) |
| Zygfryd Maus         | 1 | ((211117-porwany-trismegistos)) |
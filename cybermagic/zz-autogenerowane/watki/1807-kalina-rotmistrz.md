# Kalina Rotmistrz
## Identyfikator

Id: 1807-kalina-rotmistrz

## Sekcja Opowieści

### Kultystka z miłości

* **uid:** 180808-kultystka-z-milosci, _numer względny_: 6
* **daty:** 0109-08-30 - 0109-09-02
* **obecni:** Antoni Zajcew, Feliks Weiner, Kacper Pyszałnik, Kalina Rotmistrz, Kasandra Kirnał, Maksymilian Supolont, Małgorzata Kirnał, Patryk Paterecki, Stach Sosnowiecki

Streszczenie:

Niestety, Stach skończył ze wstrząśnieniem mózgu z ręki sojusznika. Niezrażona Kalina zwerbowała jako wsparcie terminusa Feliksa. Prasyrena wpadła w ręce Kacpra, Kasandra dołączyła do Kręgu Ośmiornicy by ratować siostrę a Kalina uderzyła. Przeciwnik okazał się być człowiekiem - Kacper działał sam. Kalina dostarczyła go przed odpowiedni sąd. Ogólnie - niezły sukces, Krąg został odparty bo gwiazdy już nie są w porządku.

Aktor w Opowieści:

* Dokonanie:
    * pozyskała sojusznika, spaliła Kacprowi dom i uratowała thugów z tego domu. W końcu się zirytowała i pojmała Kacpra, kończąc sprawę Kręgu Ośmiornicy.
* Progresja:
    * Stachu polubił podejście Kaliny. Nie to, żeby coś zaszło; jest w końcu gliną. Ale może jej czasem pomóc czy złapać z nią kontakt.


### Prasyrena z zemsty

* **uid:** 180730-prasyrena-z-zemsty, _numer względny_: 5
* **daty:** 0109-08-29 - 0109-08-30
* **obecni:** Anita Perczoluk, Antoni Zajcew, Kalina Rotmistrz, Kasandra Kirnał, Lawenda Weiner, Maksymilian Supolont, Małgorzata Kirnał, Stach Sosnowiecki

Streszczenie:

Stach wezwał Kalinę do pomocy w Trójzębie; dzieje się tu coś dziwnego z kobietami. Kalina i Stach doszli do tego, że Krąg Ośmiornicy próbuje dominować kobiety i rozprzestrzenić swój wpływ. Dodatkowo pojawiła się Prasyrena polująca na kultystów Kręgu. Kalina odpędziła Prasyrenę i ją Przeklęła, Stach ogólnie sprzątał i zbierał dane. Ich współpraca zaowocowała zdobyciem informacji o Prasyrenie i Kulcie - oraz o zdobyciu kluczowych członków. I antidotum na narkotyk kralotyczny.

Aktor w Opowieści:

* Dokonanie:
    * przeraża Syrenę, przesuwa Pryzmat w mieście ze Stachem i daje się załatwić kralotycznym proszkiem. Ogólnie, terminuska.
* Progresja:
    * całość chwały za chęć pomocy kobietom w formie amuletów ochronnych przypada Kalinie. Pozytywnie kojarzona w Toporzysku.


### Mściwa ryba z Eteru

* **uid:** 180718-msciwa-ryba-z-eteru, _numer względny_: 4
* **daty:** 0109-08-25 - 0109-08-27
* **obecni:** Borys Perikas, Joachim Kozioro, Kalina Rotmistrz, Lawenda Weiner, Magda Patiril, Majka Perikas

Streszczenie:

Majka, odepchnięta przez społeczność magów przez swoje zainteresowanie sztuką, znalazła artefakt - Kamienną Rybę z Eteru. Ten artefakt posłużył jej do wielokrotnych ataków na siedzibę rezydentki Magdy Patiril. Kalina doszła do tego o co chodzi, zdobyła wsparcie Borysa Perikasa i zmusiła Majkę do oddania artefaktu. Gdy efemeryda uderzyła w posiadłość po raz kolejny, Kalina teleportowała obraz, który został na oczach Majki zniszczony w rzece.

Aktor w Opowieści:

* Dokonanie:
    * uratowała posiadłość rezydentki, odkryła sekret Majki (że lubi malować) oraz doprowadziła do zniszczenia feralnego obrazu Majki.
* Progresja:
    * pozbywa się etykietki lojalistki. Jest po prostu "wędrującym terminusem".
    * Magda Patiril, właścicielka posiadłości w Niedźwiedźniku, uważa ją za przyjaciółkę.


### Niewidzialne potwory z rzeki

* **uid:** 180708-niewidzialne-potwory-z-rzeki, _numer względny_: 3
* **daty:** 0109-08-18 - 0109-08-20
* **obecni:** Antoni Zajcew, Joachim Kozioro, Kalina Rotmistrz, Magda Patiril, Stach Sosnowiecki

Streszczenie:

Rzeka Błękitka jest stałym źródłem problemów z Eteru. W Wypływowie mieszka więc rezydentka pilnująca terenu. Niestety, zaczął ją atakować kompromitujący niewidzialny potwór. Gdy Kalina weszła do akcji, okazało się, że to jest stary oszust w stealth suit który próbuje odzyskać obraz dla niejakiej Majki. Terminus rezydentki, Joachim, jest mega wzburzony całą tą sprawą...

Aktor w Opowieści:

* Dokonanie:
    * zidentyfikowała że za Dziwnym Potworem z rzeki stoi stary oszust, po czym go wygnała (grzecznie). Nie dała się oszukać i zachowała dobre relacje.
* Progresja:
    * uznawana za lojalistkę Magdy przez osoby w okolicach Niedźwiedźnika, ale pomogła odbudować magitrownię i system obronny za co też ma szacun w okolicy.


### Dwa statki w potrzebie

* **uid:** 180701-dwa-rejsy-w-potrzebie, _numer względny_: 2
* **daty:** 0109-08-07 - 0109-08-11
* **obecni:** Emilia Kariamon, Fabian Komczatkow, Gerard Weiner, Kacper Wontarczyk, Kalina Rotmistrz, Krystian Moborok, Liwia Barbaturran, Marcjanna Maszotka, Martauron Attylla, Sabrina Powsimrożek, Tymon Barbaturran, Wojciech Zajcew

Streszczenie:

Kalina zatrudniona jako terminus ochraniający statek transportowy miała krótkie spotkanie z Lewiatanem w Eterze Nieskończonym. Udało się uratować statek, ale potrzebne było mayday. Niestety, statek który ich uratował - Krwawy Wojownik - to rejsowiec Luxuritiasu na którym były walki Gladiatorów sprzężonych z Koordynatorkami, i to nie był statek najlepszej klasy. Kalina musiała uratować Wojownika przed Gladiatorem z jego Koordynatorką, którzy zdecydowali się na rebelię...

Aktor w Opowieści:

* Dokonanie:
    * TODO


### Krzyk w Wypływowie

* **uid:** 180623-krzyk-w-wyplywowie, _numer względny_: 1
* **daty:** 0109-07-02 - 0109-07-05
* **obecni:** Aiden Wielkołacz, Kajetan Bosman, Kalina Rotmistrz, Małgorzata Nowacka, Stanisław Mirczak

Streszczenie:

Dwie ochroniarki gwiazdy pop przybyły do Wypływowa, stron rodzinnych owej gwiazdy pop. Tam okazało się, że znikają ludzie do jakiejś dziwnej efemerydy w kształcie kamiennego skorpiona. Efemeryda okazała się ciut bardziej inteligentna niż zwykle, magitrownia trochę zbyt przyoszczędzała i ogólnie poszło w ruinę. Terminuski zwerbowały do pomocy jedynego katalistę w okolicy, wpakowały swojego zleceniodawcę w kłopoty i ruszyły ratować magitrownię przed skorpionem...

Aktor w Opowieści:

* Dokonanie:
    * współautorka zniszczenia hotelu, ochroniarz Kajetana chroniąca go przed Skorpionem i głównodowodząca operacji 'ratujmy cholerną magitrownię'.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 6, @: 0109-09-02
    1. Eter Nieskończony    : 1, @: 0109-08-11
    1. Primus    : 6, @: 0109-09-02
        1. Sektor Astoriański    : 6, @: 0109-09-02
            1. Astoria    : 6, @: 0109-09-02
                1. Sojusz Letejski    : 6, @: 0109-09-02
                    1. Trójząb Wielki    : 5, @: 0109-09-02
                        1. Bramiasto    : 1, @: 0109-08-30
                        1. Czarnomiód    : 1, @: 0109-08-27
                            1. Bazar    : 1, @: 0109-08-27
                        1. Morze Północne    : 1, @: 0109-08-11
                        1. Niedźwiedźnik    : 2, @: 0109-08-27
                            1. Dworek Karmazyn    : 2, @: 0109-08-27
                            1. Kępa Niedźwiedzia    : 1, @: 0109-08-27
                            1. Magitrownia Błękitna    : 1, @: 0109-08-20
                            1. Rzeka Błękitka    : 1, @: 0109-08-20
                        1. Toporzysko    : 2, @: 0109-09-02
                            1. Krukniew    : 2, @: 0109-09-02
                                1. Stary Labirynt    : 2, @: 0109-09-02
                            1. Wysoczysko    : 2, @: 0109-09-02
                                1. Burgerownia Wielka Świnia    : 1, @: 0109-09-02
                                1. Hotel Złotocień    : 2, @: 0109-09-02
                                1. Jadłodajnia Księżyc    : 1, @: 0109-09-02
                                1. Szkoła Makina    : 1, @: 0109-08-30
                    1. Trójząb    : 1, @: 0109-07-05
                        1. Powiat Jezierzy    : 1, @: 0109-07-05
                            1. Wypływowo    : 1, @: 0109-07-05
                                1. Osiedle Domowe    : 1, @: 0109-07-05
                                    1. Hotel Bez Pyłu    : 1, @: 0109-07-05
                                1. Osiedle Lotnicze    : 1, @: 0109-07-05
                                    1. Montownia Awianów    : 1, @: 0109-07-05
                                1. Osiedle Siłowiec    : 1, @: 0109-07-05
                                    1. Magitrownia Selenin    : 1, @: 0109-07-05

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Antoni Zajcew        | 3 | ((180708-niewidzialne-potwory-z-rzeki; 180730-prasyrena-z-zemsty; 180808-kultystka-z-milosci)) |
| Stach Sosnowiecki    | 3 | ((180708-niewidzialne-potwory-z-rzeki; 180730-prasyrena-z-zemsty; 180808-kultystka-z-milosci)) |
| Joachim Kozioro      | 2 | ((180708-niewidzialne-potwory-z-rzeki; 180718-msciwa-ryba-z-eteru)) |
| Kasandra Kirnał      | 2 | ((180730-prasyrena-z-zemsty; 180808-kultystka-z-milosci)) |
| Lawenda Weiner       | 2 | ((180718-msciwa-ryba-z-eteru; 180730-prasyrena-z-zemsty)) |
| Magda Patiril        | 2 | ((180708-niewidzialne-potwory-z-rzeki; 180718-msciwa-ryba-z-eteru)) |
| Maksymilian Supolont | 2 | ((180730-prasyrena-z-zemsty; 180808-kultystka-z-milosci)) |
| Małgorzata Kirnał    | 2 | ((180730-prasyrena-z-zemsty; 180808-kultystka-z-milosci)) |
| Aiden Wielkołacz     | 1 | ((180623-krzyk-w-wyplywowie)) |
| Anita Perczoluk      | 1 | ((180730-prasyrena-z-zemsty)) |
| Borys Perikas        | 1 | ((180718-msciwa-ryba-z-eteru)) |
| Emilia Kariamon      | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Fabian Komczatkow    | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Feliks Weiner        | 1 | ((180808-kultystka-z-milosci)) |
| Gerard Weiner        | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Kacper Pyszałnik     | 1 | ((180808-kultystka-z-milosci)) |
| Kacper Wontarczyk    | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Kajetan Bosman       | 1 | ((180623-krzyk-w-wyplywowie)) |
| Krystian Moborok     | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Liwia Barbaturran    | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Majka Perikas        | 1 | ((180718-msciwa-ryba-z-eteru)) |
| Małgorzata Nowacka   | 1 | ((180623-krzyk-w-wyplywowie)) |
| Marcjanna Maszotka   | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Martauron Attylla    | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Patryk Paterecki     | 1 | ((180808-kultystka-z-milosci)) |
| Sabrina Powsimrożek  | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Stanisław Mirczak    | 1 | ((180623-krzyk-w-wyplywowie)) |
| Tymon Barbaturran    | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Wojciech Zajcew      | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
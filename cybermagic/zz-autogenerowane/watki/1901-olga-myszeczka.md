# Olga Myszeczka
## Identyfikator

Id: 1901-olga-myszeczka

## Sekcja Opowieści

### Koszt ratowania Torszeckiego

* **uid:** 211026-koszt-ratowania-torszeckiego, _numer względny_: 8
* **daty:** 0111-07-28 - 0111-08-01
* **obecni:** Ignacy Myrczek, Ksenia Kirallen, Marysia Sowińska, Olga Myszeczka, Paweł Szprotka, Rafał Torszecki, Sensacjusz Diakon, Teresa Mieralit, Wiktor Satarail

Streszczenie:

By ratować Torszeckiego, Marysia wchodzi w sojusz z Wiktorem Satarailem. On dał jej podskórnego robaka którego dostanie Torszecki, by uzasadnić dziwne zachowanie. Wiktor "dostanie swoją zapłatę" od "kogoś winnego". Marysia przekonała też Sensacjusza, że ona x Torszecki. Zaczęły się też pojawiać takie plotki...

Aktor w Opowieści:

* Dokonanie:
    * skontaktowała Marysię z Wiktorem, nie chce nic za to. Praktyczna i sympatyczna, acz żyje na uboczu. Ostrzegła Marysię, że Wiktor nie myśli jak człowiek.


### Skradziony kot Olgi

* **uid:** 210615-skradziony-kot-olgi, _numer względny_: 7
* **daty:** 0111-05-09 - 0111-05-11
* **obecni:** Arkadia Verlen, Julia Kardolin, Liliana Bankierz, Marek Samszar, Marysia Sowińska, Olga Myszeczka, Paweł Szprotka, Wiktor Satarail

Streszczenie:

Marysia i Julia mają imprezę karaoke w Lesie Trzęsawnym, ale Marek nie dotarł - walczy na magię z Lilianą. Julia puściła drony Triany, które wpadły w berserk. By Tymon się nie zainteresował tym co się dzieje w lesie, Marysia i Julia chciały rozwiązać problem - okazało się, że Marek ukradł somnibela Oldze Myszeczce i dał go Arkadii Verlen w prezencie. Marysia i Julia przekonały Arkadię do oddania kota, oddały go Oldze, po czym Arkadia zerwała z Markiem i go ciężko pobiła. Acz Paweł Szprotka (który próbował odzyskać kota od Marka i którego broniła Liliana) boi się o życie, bo niektóre Rekiny się na niego uwzięły za karę...

Aktor w Opowieści:

* Dokonanie:
    * przygotowuje somnibele by je sprzedawać / oddawać i pomagać ludziom po traumie. Jednego somnibela ukradł od niej Marek Samszar. Dobrodziejka Pawła Szprotki i jego promotorka do AMZ.


### Kirasjerka najgorszym detektywem

* **uid:** 190721-kirasjerka-najgorszym-detektywem, _numer względny_: 6
* **daty:** 0110-06-06 - 0110-06-07
* **obecni:** Aida Serenit, Mirela Orion, Olga Myszeczka, Pięknotka Diakon, Wiktor Satarail

Streszczenie:

Przyjaciółka Kirasjerki Orbitera, Aida, zniknęła. Kirasjerka poszła jej szukać i znalazła ślady Pięknotki. Oczywiście, Kirasjerka zaatakowała by zdobyć informacje a Pięknotka nie mogła się poddać (osłaniana przez Amandę). Skończyło się na dewastacji 3 servarów, ciężko rannej Kirasjerce i stropionej Pięknotce. Gdy Pięknotka poszła do Olgi znaleźć leczenie dla Mireli (Kirasjerki), tam dowiedziała się od Wiktora Sataraila, że on podłożył ślady by pokazać jej obecność Cieniaszczytu - a dokładniej kralotycznej bioformy. Największe możliwe nieporozumienie.

Aktor w Opowieści:

* Dokonanie:
    * najgorszy medyk świata; nie dała rady pomóc Mireli. Szczęśliwie przyszedł Wiktor i naprawił sytuację.


### Plaga jamników

* **uid:** 190313-plaga-jamnikow, _numer względny_: 5
* **daty:** 0110-02-27 - 0110-02-28
* **obecni:** Alsa Fryta, Eliza Ira, Olga Myszeczka, Tomek Żuchwiacz

Streszczenie:

By pozbyć się problemów z Czerwonymi Myszami raz na zawsze, Olga zrobiła Plagę Jamników (która przerodziła się w "Wyjący Zew"). Tymczasem archeolog zeszła w podziemia kopalni Terposzy i natrafiła na nieaktywne zabezpieczenia z czasów Wojny. Uruchomiła nową plagę i współpracując z Elizą Irą rozmontowała ją. Potem rozwiązali problem Olgi usuwając Myszy z tego terenu. Olga nie ma już kłopotów z tępiącymi ją magami.

Aktor w Opowieści:

* Dokonanie:
    * zesłała Plagę Jamników na Podwiert by ludzie dali jej spokój. No i dali - stworzyła Wyjący Zew.


### Skorpipedy królewskiego xirathira

* **uid:** 190119-skorpipedy-krolewskiego-xirathira, _numer względny_: 4
* **daty:** 0110-01-13 - 0110-01-14
* **obecni:** Adela Kirys, Olga Myszeczka, Pięknotka Diakon, Sławomir Muczarek, Waldemar Mózg, Wiktor Satarail

Streszczenie:

Czerwone Myszy poprosiły Pięknotkę o pomoc - pojawiły się na ich terenie niebezpieczne skorpipedy i działania Adeli jedynie pogarszają sprawę. Okazało się, że to działania Wiktora Sataraila, który chce pomóc Oldze Myszeczce w utrzymaniu terenu i odparciu harrasserów z Myszy. Pięknotka odkręciła sprawę i trochę poprawiła relację z Wiktorem. Acz jest sceptyczna wobec samych Myszy...

Aktor w Opowieści:

* Dokonanie:
    * nie radzi sobie z magami i ludźmi którzy non stop rozbijają jej spokojną Pustą Wieś. Powiedziała całą prawdę Pięknotce - o wsparciu Wiktora Sataraila.
* Progresja:
    * Wiktor Satarail jej pomógł w odparciu harrasserów z Czerwonych Myszy. Wiktor jest jej cichym sojusznikiem, kimś, kto jej pomaga.


### Kotlina Duchów

* **uid:** 181104-kotlina-duchow, _numer względny_: 3
* **daty:** 0109-10-21 - 0109-10-23
* **obecni:** Kirył Najłalmin, Marlena Maja Leszczyńska, Olga Myszeczka, Pięknotka Diakon, Wawrzyn Towarzowski

Streszczenie:

Pięknotka ma dość Zaczęstwa - serio ten teren jej nie pasuje. Po pokonaniu energoforma Marlena zaproponowała jej zmianę regionu na Czarnopalec. Kirył z Epirjona jej pomagał znaleźć tą lokalizację; okazało się, że tamtejszy terminus jest Skażony i służy efemerydzie. Pięknotka z Olgą bohatersko wstrzymały efemerydę aż pojawiło się wsparcie z Pustogoru. Kirył został bohaterem a Pięknotka dostała gwarancję, że tamten teren jest jej (a przynajmniej że Zaczęstwo nie jest jej).

Aktor w Opowieści:

* Dokonanie:
    * straumatyzowana ludźmi hodowczyni viciniusów; z pomocą Pięknotki wyplątała się z posterunku i przyszła jej z pomocą walczyć z Efemerydą Kotliny. Skończyła w szpitalu.
* Progresja:
    * tydzień w szpitalu - detoks i odkażanie po sprawach z Efemerydą Kotliny Mikarajły


### Maciek w Otchłani Wężosmoków

* **uid:** 181116-maciek-w-otchlani-wezosmokow, _numer względny_: 2
* **daty:** 0109-09-12 - 0109-09-13
* **obecni:** Adam Wroński, Maciej Korzpuda, Olga Myszeczka, Piotr Ryszardowiec, Robert Einz, Szczepan Korzpuda, Zdzich Aprantyk, Zuzanna Klawornia

Streszczenie:

W okolicach Żarni pojawiła się efemeryda nienawiści przez to, że pewien chłopak gnębił młodsze dzieci. Efemeryda nie trafiła i przeniosła jego brata do komputera. Szczęśliwie, gidia Zaczęstwiaków osłoniła nieszczęśnika i wezwała jako wsparcie Olgę - ona po zorientowaniu się w sprawie wezwała terminusów i sprawa została rozwiązana.

Aktor w Opowieści:

* Dokonanie:
    * najbardziej niewłaściwa czarodziejka do rozwiązywania infomantycznego Skażenia z magią krwi robioną przez ludzi. Poradziła sobie terminusami i świnią tropiącą.
* Progresja:
    * dostaje odznaczenie za najbardziej nieefektowne użycie magii - ludzie nie chcieli wejść w świat magów, bo nie podobał im się styl Olgi. To taki "ignobel".


### Skorpiony spod ziemi

* **uid:** 180724-skorpiony-spod-ziemi, _numer względny_: 1
* **daty:** 0109-08-23 - 0109-08-25
* **obecni:** Alicja Kłębek, Bronisława Strzelczyk, Jakub Wirus, Olga Myszeczka, Roman Kłębek

Streszczenie:

Z uwagi na problemy związane z przeszłością pewnej rodziny w Żarni, w wędrującym węźle magicznym pojawiły się Skażone Skorpipedy. Zespół złożony z lokalnych aktywistów pomagających ludziom oraz jednej aktywistki o prawa zwierząt i viciniusów rozwiązał ten problem, odkrywając przy okazji przeszłość tej rodziny.

Aktor w Opowieści:

* Dokonanie:
    * wkręcona w akcję ze Skażonymi Skorpipedami, udało jej się jednego oswoić (acz zaraz zginął). Większość kłopotów po akcji spadła na nią.
* Progresja:
    * domyślnie podejrzana o sporo rzeczy przez Straż Wielkiej Szczeliny oraz Komitet Obrony Szczelińca z uwagi na sprawy związane ze skorpipedami.
    * mieszka w Czarnopalcu, samotnie i stamtąd odstrasza ludzi. Czarnopalec jest jej włościami utrzymującymi 3 typy viciniusów.
    * ma dostęp do glukszwajnów, śledzi syberyjskich oraz gorathaula


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 8, @: 0111-08-01
    1. Multivirt    : 1, @: 0109-09-13
        1. Smocze Wojny    : 1, @: 0109-09-13
            1. Otchłań Wężosmoków    : 1, @: 0109-09-13
            1. Siedziby Gildii    : 1, @: 0109-09-13
    1. Primus    : 8, @: 0111-08-01
        1. Sektor Astoriański    : 8, @: 0111-08-01
            1. Astoria    : 8, @: 0111-08-01
                1. Sojusz Letejski    : 8, @: 0111-08-01
                    1. Szczeliniec    : 8, @: 0111-08-01
                        1. Powiat Pustogorski    : 8, @: 0111-08-01
                            1. Czarnopalec    : 7, @: 0111-08-01
                                1. Kotlina Mikarajły    : 1, @: 0109-10-23
                                1. Pusta Wieś    : 6, @: 0111-08-01
                            1. Czółenko    : 1, @: 0110-06-07
                                1. Bunkry    : 1, @: 0110-06-07
                            1. Podwiert, okolice    : 1, @: 0110-06-07
                                1. Bioskładowisko podziemne    : 1, @: 0110-06-07
                            1. Podwiert    : 5, @: 0111-08-01
                                1. Bunkier Rezydenta    : 1, @: 0110-01-14
                                1. Dzielnica Luksusu Rekinów    : 2, @: 0111-08-01
                                    1. Obrzeża Biedy    : 1, @: 0111-05-11
                                        1. Hotel Milord    : 1, @: 0111-05-11
                                    1. Serce Luksusu    : 1, @: 0111-08-01
                                        1. Lecznica Rannej Rybki    : 1, @: 0111-08-01
                                1. Komenda policji    : 1, @: 0109-10-23
                                1. Kopalnia Terposzy    : 1, @: 0110-02-28
                                1. Las Trzęsawny    : 1, @: 0111-05-11
                                    1. Schron TRZ-17    : 1, @: 0111-05-11
                            1. Pustogor    : 1, @: 0109-10-23
                                1. Barbakan    : 1, @: 0109-10-23
                            1. Zaczęstwo    : 4, @: 0111-08-01
                                1. Akademia Magii, kampus    : 1, @: 0111-08-01
                                    1. Akademik    : 1, @: 0111-08-01
                                1. Cyberszkoła    : 1, @: 0109-10-23
                                1. Nieużytki Staszka    : 2, @: 0111-05-11
                            1. Żarnia    : 3, @: 0110-01-14
                                1. Osiedle Połacierz    : 1, @: 0109-09-13
                                1. Żarlasek    : 1, @: 0109-09-13
                                1. Żernia    : 1, @: 0110-01-14

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Wiktor Satarail      | 4 | ((190119-skorpipedy-krolewskiego-xirathira; 190721-kirasjerka-najgorszym-detektywem; 210615-skradziony-kot-olgi; 211026-koszt-ratowania-torszeckiego)) |
| Pięknotka Diakon     | 3 | ((181104-kotlina-duchow; 190119-skorpipedy-krolewskiego-xirathira; 190721-kirasjerka-najgorszym-detektywem)) |
| Marysia Sowińska     | 2 | ((210615-skradziony-kot-olgi; 211026-koszt-ratowania-torszeckiego)) |
| Paweł Szprotka       | 2 | ((210615-skradziony-kot-olgi; 211026-koszt-ratowania-torszeckiego)) |
| Adam Wroński         | 1 | ((181116-maciek-w-otchlani-wezosmokow)) |
| Adela Kirys          | 1 | ((190119-skorpipedy-krolewskiego-xirathira)) |
| Aida Serenit         | 1 | ((190721-kirasjerka-najgorszym-detektywem)) |
| Alicja Kłębek        | 1 | ((180724-skorpiony-spod-ziemi)) |
| Alsa Fryta           | 1 | ((190313-plaga-jamnikow)) |
| Arkadia Verlen       | 1 | ((210615-skradziony-kot-olgi)) |
| Bronisława Strzelczyk | 1 | ((180724-skorpiony-spod-ziemi)) |
| Eliza Ira            | 1 | ((190313-plaga-jamnikow)) |
| Ignacy Myrczek       | 1 | ((211026-koszt-ratowania-torszeckiego)) |
| Jakub Wirus          | 1 | ((180724-skorpiony-spod-ziemi)) |
| Julia Kardolin       | 1 | ((210615-skradziony-kot-olgi)) |
| Kirył Najłalmin      | 1 | ((181104-kotlina-duchow)) |
| Ksenia Kirallen      | 1 | ((211026-koszt-ratowania-torszeckiego)) |
| Liliana Bankierz     | 1 | ((210615-skradziony-kot-olgi)) |
| Maciej Korzpuda      | 1 | ((181116-maciek-w-otchlani-wezosmokow)) |
| Marek Samszar        | 1 | ((210615-skradziony-kot-olgi)) |
| Marlena Maja Leszczyńska | 1 | ((181104-kotlina-duchow)) |
| Mirela Orion         | 1 | ((190721-kirasjerka-najgorszym-detektywem)) |
| Piotr Ryszardowiec   | 1 | ((181116-maciek-w-otchlani-wezosmokow)) |
| Rafał Torszecki      | 1 | ((211026-koszt-ratowania-torszeckiego)) |
| Robert Einz          | 1 | ((181116-maciek-w-otchlani-wezosmokow)) |
| Roman Kłębek         | 1 | ((180724-skorpiony-spod-ziemi)) |
| Sensacjusz Diakon    | 1 | ((211026-koszt-ratowania-torszeckiego)) |
| Sławomir Muczarek    | 1 | ((190119-skorpipedy-krolewskiego-xirathira)) |
| Szczepan Korzpuda    | 1 | ((181116-maciek-w-otchlani-wezosmokow)) |
| Teresa Mieralit      | 1 | ((211026-koszt-ratowania-torszeckiego)) |
| Tomek Żuchwiacz      | 1 | ((190313-plaga-jamnikow)) |
| Waldemar Mózg        | 1 | ((190119-skorpipedy-krolewskiego-xirathira)) |
| Wawrzyn Towarzowski  | 1 | ((181104-kotlina-duchow)) |
| Zdzich Aprantyk      | 1 | ((181116-maciek-w-otchlani-wezosmokow)) |
| Zuzanna Klawornia    | 1 | ((181116-maciek-w-otchlani-wezosmokow)) |
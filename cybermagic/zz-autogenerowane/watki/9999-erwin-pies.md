# Erwin Pies
## Identyfikator

Id: 9999-erwin-pies

## Sekcja Opowieści

### Egzotyczna Piękność na Astralnej Flarze

* **uid:** 221123-egzotyczna-pieknosc-na-astralnej-flarze, _numer względny_: 5
* **daty:** 0100-07-15 - 0100-07-18
* **obecni:** Arianna Verlen, Arnulf Perikas, Daria Czarnewik, Ellarina Samarintael, Erwin Pies, Frank Mgrot, Gabriel Lodowiec, Gerwazy Kircznik, Hind Szug Traf, Kajetan Kircznik, Leszek Kurzmin, Marcel Kulgard, OO Astralna Flara, OO Athamarein, SCA Hadiah Emas, Szczepan Myrczek, Władawiec Diakon

Streszczenie:

Flara naprawia Hadiah Emas, a po drodze komodor Lodowiec rozplątuje wszystkie wątki - niewolnik z Orbitera? (okazał się dezerterem), autonomiczna TAI Mirtaela d'Hadiah Emas? (zostaje Ograniczona przez psychotronika Athamarein) itp. Arianna zarządza Flarą, wprowadzając Ellarinę jako maskotkę od morale i stabilizując wszystko by jakoś zespół dopasował się do realiów Nonariona. To nie jest ani Aurum ani Orbiter, to coś zupełnie innego.

Aktor w Opowieści:

* Dokonanie:
    * z G.Kircznikiem próbował ostrzec Ariannę by nie wprowadzać uratowanych na Flarę (ryzyko). Nie poradził sobie z przesłuchaniem Ellariny i został elementem mema "Pies szczeka nie głaszcze" jak clipowali fragment przesłuchania.
* Progresja:
    * pojawił się mem, wyciek z przesłuchania: "Pies szczeka nie głaszcze". Jak Pies podniósł głos, Ellarina zaczęła płakać a Pies nie wie co robić i mówi "uspokój się, to ROZKAZ!"


### Kapitan Verlen i koniec przygody na Królowej

* **uid:** 221026-kapitan-verlen-i-koniec-przygody-na-krolowej, _numer względny_: 4
* **daty:** 0100-04-10 - 0100-04-17
* **obecni:** Antoni Kramer, Arianna Verlen, Arnulf Perikas, Daria Czarnewik, Erwin Pies, Klaudiusz Terienak, Leszek Kurzmin, OO Królowa Kosmicznej Chwały, OO Tucznik Trzeci, Stefan Torkil, Tomasz Ruppok

Streszczenie:

Po długiej i ciężkiej pracy Arianna i Daria doprowadziły Królową do 50% sprawności nominalnej. Radość i wysokie morale załogi. Gdy Arnulf przyszedł do Arianny mówiąc o fabrykacji narkotyków i alkoholu, ona kazała to 'zatruć'. Zmotywowała Ruppoka, by dał jej szansę i znalazła sposób jak częściowo odzyskać materiały do fabrykatora po rozmowie z Arnulfem - sojusz z bliską Anomalii Kolapsu stacją Nonarion; poprosiła Kurzmina by to załatwił. TAK PRZESTRASZYŁA siły specjalne Orbitera, że Ariannę i załogę przenieśli bliżej Nonariona by sabotować program kosmiczny Aurum. Koniec przygody na Królowej :-).

Aktor w Opowieści:

* Dokonanie:
    * dyskretnie porozstawiał marines w miejscach które mogą być sabotowane; cicho zapewnił, że wszystko bezbłędnie działa z tą załogą.


### Kapitan Verlen i pierwszy ruch statku

* **uid:** 221019-kapitan-verlen-i-pierwszy-ruch-statku, _numer względny_: 3
* **daty:** 0100-03-25 - 0100-04-06
* **obecni:** Arianna Verlen, Daria Czarnewik, Erwin Pies, Maja Samszar, Marcelina Trzęsiel, OO Królowa Kosmicznej Chwały, Romeo Verlen, Rufus Warkoczyk, Stefan Torkil

Streszczenie:

Arianna włączyła ćwiczenia dla Królowej - prawie wyszło, ale ludzie popanikowali a statek odmówił posłuszeństwa i załoga zaczęła się bić. Arianna ich przećwiczyła w działaniach awaryjnych i oddała panikarza medykowi - niestety, Paradoks zmienił Marcela w Marcelinę... ale następnym razem ćwiczenia powinny się już udać. Daria ma sprawę pod kontrolą i Arianna też raczej jest pewna sukcesu.

Aktor w Opowieści:

* Dokonanie:
    * kapral przygotował mordercze ćwiczenia i przećwiczył załogę na działanie w warunkach stresowych i awaryjnych. Porządne 10 dni.


### Kapitan Verlen i pojedynek z marine

* **uid:** 220928-kapitan-verlen-i-pojedynek-z-marine, _numer względny_: 2
* **daty:** 0100-03-16 - 0100-03-18
* **obecni:** Alezja Dumorin, Arianna Verlen, Arnulf Perikas, Daria Czarnewik, Erwin Pies, Grażyna Burgacz, Leona Astrienko, Leszek Kurzmin, Maja Samszar, OO Królowa Kosmicznej Chwały, Stefan Torkil, Szymon Wanad, Tomasz Ruppok, Władawiec Diakon

Streszczenie:

Królowa miała się przesunąć, ale prawie się rozbiła; ta jednostka jest rozkradziona. Arianna zatrzymała biczowanie załogantów i doszła do tego, że załoga jest niemrawa i skonfliktowana bo część ludzi jest z Aurum a część nie. Perfect storm of suck. By zatrzymać Wanada przed "wygraniem cnoty koleżanki inżyniera" stoczyła z nim pojedynek i wygrała. Leona ostrzegła, by Arianna nic nie jadła i nie piła. Czyli coś jest w jedzeniu. To tłumaczy czemu Królowa jest tak zdegenerowaną jednostką. Aha, Arianna ma wsparcie kapitana Leszka Kurzmina, dawnego rywala Alezji.

Aktor w Opowieści:

* Dokonanie:
    * dyskretnie przekazał Ariannie, że Arnulf Perikas biczuje ludzi na pokładzie jednostki - wprowadza zasady Aurum. Ogólnie, Arianna ufa mu najbardziej a on jej najbardziej pomaga.


### Kapitan Verlen i Królowa Kosmicznej Chwały

* **uid:** 220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly, _numer względny_: 1
* **daty:** 0100-03-08 - 0100-03-14
* **obecni:** Alezja Dumorin, Arianna Verlen, Arnulf Perikas, Daria Czarnewik, Erwin Pies, Grażyna Burgacz, Klarysa Jirnik, Klaudiusz Terienak, Leona Astrienko, Maja Samszar, OO Królowa Kosmicznej Chwały, Szymon Wanad, Władawiec Diakon

Streszczenie:

Królowa Kosmicznej Chwały to najbardziej dysfunkcyjna jednostka pod kontrolą Orbitera - służy do łamania karier obiecujących oficerów i sabotażu programu kosmicznego Aurum (oba z woli sił specjalnych Aurum). Trafiła na królową Arianna i zaczęła robić porządek. Poprzednią panią kapitan zdecydowała się odratować, współpracując z Darią z inżynierii rozmontować wyścigi psów i stwierdziła, że doprowadzi do tego, by Królowa zaczęła skutecznie działać podczas ćwiczeń. Aha, tu poznała się z Leoną Astrienko ;-).

Aktor w Opowieści:

* Dokonanie:
    * kapral i dowódca marines na Królowej; stanął po stronie Arianny. Zbiera bardzo dokładny raport i wspiera Ariannę jak tylko ten Orbiterowiec potrafi. Dość kompetentny, choć nie miał czym i z kim pracować póki nie miał Arianny.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 5, @: 0100-07-18
    1. Primus    : 5, @: 0100-07-18
        1. Sektor Astoriański    : 5, @: 0100-07-18
            1. Obłok Lirański    : 1, @: 0100-07-18
                1. Anomalia Kolapsu, orbita    : 1, @: 0100-07-18

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 5 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221019-kapitan-verlen-i-pierwszy-ruch-statku; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Daria Czarnewik      | 5 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221019-kapitan-verlen-i-pierwszy-ruch-statku; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Arnulf Perikas       | 4 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| OO Królowa Kosmicznej Chwały | 4 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221019-kapitan-verlen-i-pierwszy-ruch-statku; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Leszek Kurzmin       | 3 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Maja Samszar         | 3 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221019-kapitan-verlen-i-pierwszy-ruch-statku)) |
| Stefan Torkil        | 3 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221019-kapitan-verlen-i-pierwszy-ruch-statku; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Władawiec Diakon     | 3 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Alezja Dumorin       | 2 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine)) |
| Grażyna Burgacz      | 2 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine)) |
| Klaudiusz Terienak   | 2 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Leona Astrienko      | 2 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine)) |
| Szymon Wanad         | 2 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine)) |
| Tomasz Ruppok        | 2 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Antoni Kramer        | 1 | ((221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Ellarina Samarintael | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Frank Mgrot          | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Gabriel Lodowiec     | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Gerwazy Kircznik     | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Hind Szug Traf       | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Kajetan Kircznik     | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Klarysa Jirnik       | 1 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly)) |
| Marcel Kulgard       | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Marcelina Trzęsiel   | 1 | ((221019-kapitan-verlen-i-pierwszy-ruch-statku)) |
| OO Astralna Flara    | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| OO Athamarein        | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| OO Tucznik Trzeci    | 1 | ((221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Romeo Verlen         | 1 | ((221019-kapitan-verlen-i-pierwszy-ruch-statku)) |
| Rufus Warkoczyk      | 1 | ((221019-kapitan-verlen-i-pierwszy-ruch-statku)) |
| SCA Hadiah Emas      | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Szczepan Myrczek     | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
# Atena Sowińska
## Identyfikator

Id: 1901-atena-sowinska

## Sekcja Opowieści

### Wygaśnięcie starego autosenta

* **uid:** 190213-wygasniecie-starego-autosenta, _numer względny_: 16
* **daty:** 0110-03-07 - 0110-03-08
* **obecni:** Antoni Kotomin, Atena Sowińska, Baltazar Rączniak, Dariusz Bankierz, Jadwiga Pszarnik, Pięknotka Diakon, Rafał Bobowiec

Streszczenie:

Stary autosent się jakoś zregenerował by uratować Jadwigę przed śmiercią. Zagroził Skałopływowi, szukając medyka dla Jadwigi. Zespołowi udało się postawić Jadwigę na nogi i wyprowadzić autosenta poza Skałopływ, po czym przeprowadzono echo Teresy Tevalier przez procedurę dezaktywacji autosenta. Docelowo, autosent trafił do Cieniaszczytu.

Aktor w Opowieści:

* Dokonanie:
    * zlokalizowała autosenta oraz przekazała temat Pięknotce.


### Minerwa i Kwiaty Nadziei

* **uid:** 190210-minerwa-i-kwiaty-nadziei, _numer względny_: 15
* **daty:** 0110-02-20 - 0110-02-22
* **obecni:** Atena Sowińska, Erwin Galilien, Kasjopea Maus, Kornel Garn, Minerwa Metalia, Pięknotka Diakon

Streszczenie:

Kornel Garn próbował przekonać Minerwę do dołączenia do niego, by mu pomogła - budując jej społeczeństwo które ją akceptuje i pokazując jej Kwiaty Nadziei z Ixionu. Minerwa w 100% wpadała w to, więc Pięknotka zmontowała front przeciw Kornelowi - Kasjopea, Erwin, terminusi Pustogorscy. Skończyło się ucieczką Kornela, ale Minerwa będzie chciała mu pomóc i dzielić się z nim wiedzą. A Pięknotkę bardzo martwi to, jak Kornel radzi sobie z ixiońskimi anomaliami i swoim kultem.

Aktor w Opowieści:

* Dokonanie:
    * wykryła (za prośbą Pięknotki) energię ixiońską niedaleko Pustogoru i wezwała oddział szturmowy, by przerwać sprytny plan Kornela Garna.


### Adieu, Cieniaszczycie

* **uid:** 181227-adieu-cieniaszczycie, _numer względny_: 14
* **daty:** 0109-11-27 - 0109-11-30
* **obecni:** Amadeusz Sowiński, Atena Sowińska, Brygida Maczkowik, Erwin Galilien, Lilia Ursus, Mirela Niecień, Moktar Gradon, Pietro Dwarczan, Pięknotka Diakon, Waleria Cyklon, Zbigniew Burzycki

Streszczenie:

Pięknotka zdobyła nowe ciało dla Minerwy (acz do jego uruchomienia człowiek musi być poświęcony!), przekonała Amadeusza do założenia świątyni Arazille do blokowania Finis Vitae oraz wepchnęła Lilię jako asystentkę Atenie. Do tego rozkochała solidnie Erwina i zamknęła wszystkie wątki, by móc wracać do Pustogoru.

Aktor w Opowieści:

* Dokonanie:
    * tak dzielnie pozbyła się jednej asystentki, by dostać drugą przez Pięknotkę - równie bezużyteczną (Lilię). Nie jest szczęśliwa, zwłaszcza po remprymendzie.
* Progresja:
    * wróciła (z Lilią) na Stację Orbitalną Epirjon. Kapitan na statku.


### Finis Vitae

* **uid:** 181226-finis-vitae, _numer względny_: 13
* **daty:** 0109-11-17 - 0109-11-26
* **obecni:** Amadeusz Sowiński, Arazille, Atena Sowińska, Moktar Gradon, Pięknotka Diakon, Waleria Cyklon

Streszczenie:

Moktar zregenerował Pięknotkę i powiedział jej, że chce od niej uratowania swojego Skażonego oficera. Pięknotka dowiedziała się od Amadeusza o autowarze Finis Vitae pod Cieniaszczytem; Saitaer nie może nic wiedzieć. Pięknotka doprowadziła do tego, że Lilia wróciła do Cieniaszczytu (courtesy of Moktar), uratowała spod ziemi tego oficera, oraz zobaczyła miejsce, które będzie chyba do końca życia widziała w koszmarach. Gdyby nie Atena, zginęłaby na tej akcji.

Aktor w Opowieści:

* Dokonanie:
    * uratowała Pięknotkę odpowiednim wykorzystaniem sprzętu ASD. Dodatkowo, zbiera więcej informacji o Orbiterze. Dostała od ASD reprymendę.
* Progresja:
    * utraciła Sekerala do autowara; dostała silną reprymendę. Ale - uratowała Pięknotkę. Uważa, że było warto ;-).


### Czyszczenie toksycznych związków

* **uid:** 181225-czyszczenie-toksycznych-zwiazkow, _numer względny_: 12
* **daty:** 0109-11-12 - 0109-11-16
* **obecni:** Atena Sowińska, Bogdan Szerl, Julia Morwisz, Moktar Gradon, Pięknotka Diakon, Romuald Czurukin

Streszczenie:

Pięknotka ściągnęła Erwina na detoks od Saitaera, acz dała mu informację o Kreacjuszu Diakonie. Saitaer planuje zbudować sprawne ciało dla Minerwy. Atena jest zregenerowana w dość anielskiej formie i dostaje informacje o Orbiterze od Julii. Pięknotka próbuje naprawić pary Romuald x Julia oraz Wioletta x Pietro. Moktar uwalnia Pięknotkę od wpływu Bogdana, masakrując swojego agenta w brutalny sposób.

Aktor w Opowieści:

* Dokonanie:
    * zregenerowana, z anielskimi skrzydłami i paragon światła. Zbiera informacje o Orbiterze od Julii - próbuje zrozumieć, co tu się dzieje.
* Progresja:
    * powróciła z rekonstrukcji; ma świetliste skrzydła i jest paragonem Astorii. She changed, got stronger.


### Tajemniczy ołtarz Moktara

* **uid:** 181218-tajemniczy-oltarz-moktara, _numer względny_: 11
* **daty:** 0109-11-10 - 0109-11-12
* **obecni:** Atena Sowińska, Bogdan Szerl, Moktar Grodan, Pięknotka Diakon, Waleria Cyklon

Streszczenie:

Pięknotka śniła o lustrach; w poszukiwaniu informacji co się dzieje dowiedziała się, że do wyczyszczenia Saitaera wykorzystana jest energia Arazille. O dziwo, Moktar kontroluje energię Arazille a przynajmniej używa jej jako broni. Pięknotka wróciła w ręce Bogdana i przekonała Atenę, by ta poddała się rekonstrukcji - by uwolniła się od Saitaera.

Aktor w Opowieści:

* Dokonanie:
    * zaczęła martwić się o stan Pięknotki i ogólnie te dziwne rytuały. Poddała się rekonstrukcji, ale wezwała wsparcie by pomogło jej w analizie co tu się dzieje.


### Wolna od terrorforma

* **uid:** 181216-wolna-od-terrorforma, _numer względny_: 10
* **daty:** 0109-11-04 - 0109-11-10
* **obecni:** Atena Sowińska, Bogdan Szerl, Julia Morwisz, Mirela Niecień, Pięknotka Diakon

Streszczenie:

Pięknotka chciała uwolnić się od terrorforma - udało jej się, gdyż oddała się we władzę straszliwego mindwarpa Łysych Psów, Bogdana Szerla. Szerl wraz z kralothami zrekonstruowali Wzór Pięknotki - teraz jest odporna na wpływy terrorforma, ale jednocześnie jest bardzo zmieniona. Sensual, dangerous, chemical. Przy okazji Julia (Emulator) oddała serce Pięknotce a Atena zatrzymała swój przydział na Epirjon.

Aktor w Opowieści:

* Dokonanie:
    * zrezygnowała tymczasowo ze stacji Epirjon, ale decyzja nie jest pewna. Martwi ją przemiana Pięknotki. Próbuje pomóc po swojemu.


### Odklątwianie Ateny

* **uid:** 181112-odklatwianie-ateny, _numer względny_: 9
* **daty:** 0109-10-22 - 0109-10-25
* **obecni:** Amadeusz Sowiński, Atena Sowińska, Cezary Zwierz, Lucjusz Blakenbauer, Minerwa Diakon, Pięknotka Diakon, Saitaer

Streszczenie:

Atenie się nie poprawia a jej echo atakuje Pięknotkę. By zrozumieć co się dzieje, Pięknotka rozmawia z "terrorformem" (Saitaerem) i dowiaduje się o krwawym klątwożycie. Następnie używając wpływów Adama Szarjana wyrywa Atenę spod opieki lekarza i transportuje ją do Cieniaszczytu, by tam można było jej pomóc. By jej pomóc i odkryć napastnika potrzeba jest moc kralotyczna - Atena się nie zgadza, lecz Pięknotka ją przekonała. Atena ulega rekonstrukcji kralotycznej a Amadeusz ma mniej więcej namiar na twórcę klątwożyta.

Aktor w Opowieści:

* Dokonanie:
    * coraz bardziej chora i Skażona klątwożytem krwi. Zaczynała wpadać w paranoję. Okazuje się, że zawsze kochała kosmos. Zrekonstruowana przez kralothy by ją wyleczyć, co ciężko przeżyła.
* Progresja:
    * ujawniło się, że jest przerażona kralothami i rzeczami kralotycznymi; najbliższe fobii co ma
    * nie dogaduje się z wujem Amadeuszem Sowińskim i mu zdecydowanie nie ufa
    * została Zrekonstruowana kralotycznie - wyleczona i wyczyszczona, oraz znaleziono sygnał osób chcących ją skrzywdzić
    * jeszcze 2 tygodnie nie może dostać się na stację Epirjon i musi działać w okolicach Cieniaszczytu
    * jest święcie przekonana, że została zmieniona i nie wie jak (nie została zmieniona)


### Kotlina Duchów

* **uid:** 181104-kotlina-duchow, _numer względny_: 8
* **daty:** 0109-10-21 - 0109-10-23
* **obecni:** Kirył Najłalmin, Marlena Maja Leszczyńska, Olga Myszeczka, Pięknotka Diakon, Wawrzyn Towarzowski

Streszczenie:

Pięknotka ma dość Zaczęstwa - serio ten teren jej nie pasuje. Po pokonaniu energoforma Marlena zaproponowała jej zmianę regionu na Czarnopalec. Kirył z Epirjona jej pomagał znaleźć tą lokalizację; okazało się, że tamtejszy terminus jest Skażony i służy efemerydzie. Pięknotka z Olgą bohatersko wstrzymały efemerydę aż pojawiło się wsparcie z Pustogoru. Kirył został bohaterem a Pięknotka dostała gwarancję, że tamten teren jest jej (a przynajmniej że Zaczęstwo nie jest jej).

Aktor w Opowieści:

* Dokonanie:
    * AKTOR NIEOBECNY NA SESJI
* Progresja:
    * jeszcze tydzień musi poleżeć w szpitalu, bo Pięknotka wykorzystała jej energię podczas działań związanych z Krwią


### Terminuska czy kosmetyczka?

* **uid:** 181027-terminuska-czy-kosmetyczka, _numer względny_: 7
* **daty:** 0109-10-07 - 0109-10-11
* **obecni:** Adela Kirys, Arnulf Poważny, Brygida Maczkowik, Ignacy Myrczek, Minerwa Metalia, Pięknotka Diakon

Streszczenie:

Jednoczesny atak na reputację Ateny (przez cybergrzyby) oraz wielki konkurs kosmetyczny w Pustogorze wpłynęły stosunkowo źle na sytuację Pięknotki. Nie rozdwoiła się, ale obie rzeczy udało jej się rozwiązać - acz jest wycieńczona. Odkryła konspirację wymierzoną przeciwko Atenie - ale nie wie kto i gdzie próbuje Atenę usunąć.

Aktor w Opowieści:

* Dokonanie:
    * AKTOR NIEOBECNY NA SESJI
* Progresja:
    * za słaba; nie jest w stanie wrócić na Epirjon jeszcze przez co najmniej dwa tygodnie.
    * zrobiła furorę jako modelka Pięknotki na Arenie Szalonego Króla w Pustogorze. Wbrew sobie. Wyszła jej przeszłość miss licealistek.


### Powrót Minerwy z terrorforma

* **uid:** 181021-powrot-minerwy-z-terrorforma, _numer względny_: 6
* **daty:** 0109-09-24 - 0109-09-29
* **obecni:** Adam Szarjan, Atena Sowińska, Erwin Galilien, Lilia Ursus, Pięknotka Diakon

Streszczenie:

Adam Szarjan, dawny kolega Erwina pojawił się by uratować go przed problemami z Nutką. Okazało się, że Nutka jest śmiertelnym zagrożeniem pod wpływem energii Nojrepów. Wraz z Ateną i Pięknotką udało im się zneutralizować problem, ale w miejsce Nutki pojawiła się Minerwa - prawdziwa osoba, na bazie której powstała Nutka. Atena i Erwin w bardzo ciężkim stanie trafili do szpitala.

Aktor w Opowieści:

* Dokonanie:
    * stanęła na wysokości zadania by przekształcić Nutkę w Minerwę. Dużo zaryzykowała i wzięła ogień na siebie. W przeszłości ma straszliwe demony.
* Progresja:
    * ma dostęp do Epirjona przez działania Adama Szarjana.
    * bardzo ciężko ranna; co najmniej tydzień w szpitalu + rehabilitacja.


### Dwa tygodnie szkoły

* **uid:** 180929-dwa-tygodnie-szkoly, _numer względny_: 5
* **daty:** 0109-09-17 - 0109-09-19
* **obecni:** Adela Kirys, Arnulf Poważny, Atena Sowińska, Erwin Galilien, Felicja Melitniek, Ignacy Myrczek, Miedwied Zajcew, Napoleon Bankierz, Pięknotka Diakon, Roland Grzymość

Streszczenie:

Felicja uczy się w szkole magów, ale nie ma przyjaciół i przez to zachowuje się chaotycznie. Erwin Galilien wpakował się w kłopoty i przegrał power suit "Nutkę" w Kasynie; ma przynieść niebezpieczne grzyby. Uczniowie ze szkoły magów też chodzą do tego Kasyna. Połączone siły Zespołu i Ateny wyplątały Erwina z kłopotów i utrudniły Kasynu zwabianie młodych uczniów ze szkoły magów.

Aktor w Opowieści:

* Dokonanie:
    * robi inspekcję w szkole magów by znaleźć coś na Kasyno Marzeń. Współpracując z Pięknotką i Miedwiedem udaje jej się zrobić operację uszkadzającą Kasyno.
* Progresja:
    * zyskała głębokiego fana w Napoleonie Bankierzu, ku swemu utrapieniu.


### Inkarnata nienawiści

* **uid:** 180912-inkarnata-nienawisci, _numer względny_: 4
* **daty:** 0109-09-14 - 0109-09-15
* **obecni:** Atena Sowińska, Brygida Maczkowik, Pięknotka Diakon, Staś Kruszawiecki

Streszczenie:

Pięknotka dowiedziała się, że Atena Sowińska wylądowała na zesłaniu z Epirjona. Pojechała jej pomóc (kontrolować przerażająca komendant Epirjona). Na miejscu rozwiązały problem z viciniusem przybyłym ze Zjawosztup oraz się okazało, że Atena tak koszmarną postacią nie jest.

Aktor w Opowieści:

* Dokonanie:
    * najpotężniejsza i najbardziej opanowana katalistka jaką kiedykolwiek Pięknotka widziała; najbardziej opanowany cywil ever. Dodatkowo - medyk ratujący ludzi.
* Progresja:
    * musiała opuścić Epirjon z przyczyn politycznych. Teraz szuka na Astorii jak rozwiązać problem i wrócić na Epirjon.


### Nikt nie śpi w swoim łóżku

* **uid:** 180906-nikt-nie-spi-w-swoim-lozku, _numer względny_: 3
* **daty:** 0109-09-09 - 0109-09-11
* **obecni:** Atena Sowińska, Brygida Maczkowik, Grażyna Sirwąg, Mieszko Weiner, Pięknotka Diakon, Szczepan Mensic, Tadeusz Kruszawiecki

Streszczenie:

W Zaczęstwie pojawił się dziwny duch - kiedyś policjant Wąsacz, teraz karze ludzi za wykroczenia śmiercią. Pięknotka i Mieszko skupili się na jego usunięciu. Okazało się, że dyrektor Cyberszkoły jest creepy, sypia z Brygidą i przez to prawie zginął. Zespół pojmał ducha i zaklął go w pluszaka, przedtem zatrzymując działania klubu okultystycznego. Dodatkowo, Atena wyleciała ze stacji Epirjon. Aha, nikt tu nie śpi w swoim łóżku.

Aktor w Opowieści:

* Dokonanie:
    * nie miała cierpliwości do Mieszka i przez to wyleciała ze stacji. Nie spodziewała się tego zupełnie. Dała jednak radę osłonić Brygidę przed konsekwencjami.
* Progresja:
    * wyleciała ze stacji Epirjon (tymczasowo) za złe potraktowanie gościa - Mieszka Weinera


### Protomag z Trzęsawisk

* **uid:** 180817-protomag-z-trzesawisk, _numer względny_: 2
* **daty:** 0109-09-07 - 0109-09-09
* **obecni:** Alan Bartozol, Atena Sowińska, Erwin Galilien, Felicja Melitniek, Lucjusz Blakenbauer, Miedwied Zajcew, Pięknotka Diakon

Streszczenie:

Protomag skrzywdził kierowcę z lokalnej mafii Zajcewów. Miedwied i Pięknotka poszli szukać Felicji - protomaga na której eksperymentowali - na Trzęsawisku Zjawosztup. Tam napotkali na terminusów Czerwonych Myszy którzy też chcieli Felicję. Współpraca Zespołu z Ateną Sowińską doprowadziła do przekazania Felicji im i Miedwied został jej opiekunem. Niestety, Trzęsawisko w wyniku pojawienia się Felicji się rozpaliło - jest gorące i będzie emitować Skażeńce...

Aktor w Opowieści:

* Dokonanie:
    * rozwiązuje dla Zespołu pewne problemy natury proceduralno-prawnej, np. "gdzie powinna trafić Felicja". Czyli pod opiekę Miedwieda. Gwarant bezpieczeństwa.


### Programista mimo woli

* **uid:** 180821-programista-mimo-woli, _numer względny_: 1
* **daty:** 0109-09-06 - 0109-09-07
* **obecni:** Atena Sowińska, Bronisława Strzelczyk, Pięknotka Diakon, Staś Kruszawiecki, Tadeusz Kruszawiecki

Streszczenie:

Na terenie Zaczęstwa doszło do Emisji i pojawiła się efemeryda. Próbując ją usunąć, magowie stworzyli Cyberszkołę w Zaczęstwie podpiętą energetycznie do Trzęsawiska Zjawosztup. Efemeryda była podpięta do dziecka - małego Stasia. By ją usunąć, dyrektor Cyberszkoły i ojciec Stasia musiał zrezygnować ze zmuszenia syna do programowania. Niech rysuje. Viva magia mentalna ;-).

Aktor w Opowieści:

* Dokonanie:
    * nadała temat odnośnie problemów w Zaczęstwie Pięknotce, woląc, by Pięknotka a nie Alan z Czerwonych Myszy się tematem zajmowała.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 14, @: 0110-03-08
    1. Primus    : 14, @: 0110-03-08
        1. Sektor Astoriański    : 14, @: 0110-03-08
            1. Astoria    : 14, @: 0110-03-08
                1. Sojusz Letejski, NW    : 5, @: 0109-11-30
                    1. Ruiniec    : 5, @: 0109-11-30
                        1. Colubrinus Meditech    : 3, @: 0109-11-16
                        1. Colubrinus Psiarnia    : 2, @: 0109-11-30
                        1. Skalny Labirynt    : 1, @: 0109-11-26
                        1. Studnia Bez Dna    : 1, @: 0109-11-26
                        1. Świątynia Bez Dna    : 1, @: 0109-11-30
                1. Sojusz Letejski, SW    : 1, @: 0110-03-08
                    1. Granica Anomalii    : 1, @: 0110-03-08
                        1. Skałopływ    : 1, @: 0110-03-08
                1. Sojusz Letejski    : 13, @: 0110-02-22
                    1. Przelotyk    : 6, @: 0109-11-30
                        1. Przelotyk Wschodni    : 6, @: 0109-11-30
                            1. Cieniaszczyt    : 6, @: 0109-11-30
                                1. Knajpka Szkarłatny Szept    : 3, @: 0109-11-30
                                1. Kompleks Nukleon    : 6, @: 0109-11-30
                                1. Mrowisko    : 3, @: 0109-11-30
                                1. Pałac Szkarłatnego Światła    : 1, @: 0109-10-25
                    1. Szczeliniec    : 8, @: 0110-02-22
                        1. Powiat Pustogorski    : 8, @: 0110-02-22
                            1. Pustogor    : 3, @: 0109-10-25
                                1. Eksterior    : 1, @: 0109-10-25
                                    1. Miasteczko    : 1, @: 0109-10-25
                                1. Gabinet Pięknotki    : 1, @: 0109-09-29
                                1. Kompleks Testowy    : 1, @: 0109-09-29
                                1. Pustułka    : 1, @: 0109-09-29
                                1. Rdzeń    : 2, @: 0109-10-25
                                    1. Szpital Terminuski    : 2, @: 0109-10-25
                            1. Zaczęstwo    : 5, @: 0110-02-22
                                1. Akademia Magii, kampus    : 1, @: 0109-09-19
                                1. Cyberszkoła    : 3, @: 0109-09-15
                                1. Hotel Tellur    : 1, @: 0110-02-22
                                1. Kasyno Marzeń    : 1, @: 0109-09-19
                                1. Klub Poetycki Sucharek    : 1, @: 0109-09-11
                                1. Nieużytki Staszka    : 3, @: 0110-02-22
                                1. Wschodnie Pole Namiotowe    : 1, @: 0110-02-22
                        1. Trzęsawisko Zjawosztup    : 3, @: 0109-09-11

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Pięknotka Diakon     | 14 | ((180817-protomag-z-trzesawisk; 180821-programista-mimo-woli; 180906-nikt-nie-spi-w-swoim-lozku; 180912-inkarnata-nienawisci; 180929-dwa-tygodnie-szkoly; 181021-powrot-minerwy-z-terrorforma; 181112-odklatwianie-ateny; 181216-wolna-od-terrorforma; 181218-tajemniczy-oltarz-moktara; 181225-czyszczenie-toksycznych-zwiazkow; 181226-finis-vitae; 181227-adieu-cieniaszczycie; 190210-minerwa-i-kwiaty-nadziei; 190213-wygasniecie-starego-autosenta)) |
| Erwin Galilien       | 5 | ((180817-protomag-z-trzesawisk; 180929-dwa-tygodnie-szkoly; 181021-powrot-minerwy-z-terrorforma; 181227-adieu-cieniaszczycie; 190210-minerwa-i-kwiaty-nadziei)) |
| Amadeusz Sowiński    | 3 | ((181112-odklatwianie-ateny; 181226-finis-vitae; 181227-adieu-cieniaszczycie)) |
| Bogdan Szerl         | 3 | ((181216-wolna-od-terrorforma; 181218-tajemniczy-oltarz-moktara; 181225-czyszczenie-toksycznych-zwiazkow)) |
| Brygida Maczkowik    | 3 | ((180906-nikt-nie-spi-w-swoim-lozku; 180912-inkarnata-nienawisci; 181227-adieu-cieniaszczycie)) |
| Moktar Gradon        | 3 | ((181225-czyszczenie-toksycznych-zwiazkow; 181226-finis-vitae; 181227-adieu-cieniaszczycie)) |
| Waleria Cyklon       | 3 | ((181218-tajemniczy-oltarz-moktara; 181226-finis-vitae; 181227-adieu-cieniaszczycie)) |
| Felicja Melitniek    | 2 | ((180817-protomag-z-trzesawisk; 180929-dwa-tygodnie-szkoly)) |
| Julia Morwisz        | 2 | ((181216-wolna-od-terrorforma; 181225-czyszczenie-toksycznych-zwiazkow)) |
| Lilia Ursus          | 2 | ((181021-powrot-minerwy-z-terrorforma; 181227-adieu-cieniaszczycie)) |
| Lucjusz Blakenbauer  | 2 | ((180817-protomag-z-trzesawisk; 181112-odklatwianie-ateny)) |
| Miedwied Zajcew      | 2 | ((180817-protomag-z-trzesawisk; 180929-dwa-tygodnie-szkoly)) |
| Mirela Niecień       | 2 | ((181216-wolna-od-terrorforma; 181227-adieu-cieniaszczycie)) |
| Staś Kruszawiecki    | 2 | ((180821-programista-mimo-woli; 180912-inkarnata-nienawisci)) |
| Tadeusz Kruszawiecki | 2 | ((180821-programista-mimo-woli; 180906-nikt-nie-spi-w-swoim-lozku)) |
| Adam Szarjan         | 1 | ((181021-powrot-minerwy-z-terrorforma)) |
| Adela Kirys          | 1 | ((180929-dwa-tygodnie-szkoly)) |
| Alan Bartozol        | 1 | ((180817-protomag-z-trzesawisk)) |
| Antoni Kotomin       | 1 | ((190213-wygasniecie-starego-autosenta)) |
| Arazille             | 1 | ((181226-finis-vitae)) |
| Arnulf Poważny       | 1 | ((180929-dwa-tygodnie-szkoly)) |
| Baltazar Rączniak    | 1 | ((190213-wygasniecie-starego-autosenta)) |
| Bronisława Strzelczyk | 1 | ((180821-programista-mimo-woli)) |
| Cezary Zwierz        | 1 | ((181112-odklatwianie-ateny)) |
| Dariusz Bankierz     | 1 | ((190213-wygasniecie-starego-autosenta)) |
| Grażyna Sirwąg       | 1 | ((180906-nikt-nie-spi-w-swoim-lozku)) |
| Ignacy Myrczek       | 1 | ((180929-dwa-tygodnie-szkoly)) |
| Jadwiga Pszarnik     | 1 | ((190213-wygasniecie-starego-autosenta)) |
| Kasjopea Maus        | 1 | ((190210-minerwa-i-kwiaty-nadziei)) |
| Kornel Garn          | 1 | ((190210-minerwa-i-kwiaty-nadziei)) |
| Mieszko Weiner       | 1 | ((180906-nikt-nie-spi-w-swoim-lozku)) |
| Minerwa Diakon       | 1 | ((181112-odklatwianie-ateny)) |
| Minerwa Metalia      | 1 | ((190210-minerwa-i-kwiaty-nadziei)) |
| Moktar Grodan        | 1 | ((181218-tajemniczy-oltarz-moktara)) |
| Napoleon Bankierz    | 1 | ((180929-dwa-tygodnie-szkoly)) |
| Pietro Dwarczan      | 1 | ((181227-adieu-cieniaszczycie)) |
| Rafał Bobowiec       | 1 | ((190213-wygasniecie-starego-autosenta)) |
| Roland Grzymość      | 1 | ((180929-dwa-tygodnie-szkoly)) |
| Romuald Czurukin     | 1 | ((181225-czyszczenie-toksycznych-zwiazkow)) |
| Saitaer              | 1 | ((181112-odklatwianie-ateny)) |
| Szczepan Mensic      | 1 | ((180906-nikt-nie-spi-w-swoim-lozku)) |
| Zbigniew Burzycki    | 1 | ((181227-adieu-cieniaszczycie)) |
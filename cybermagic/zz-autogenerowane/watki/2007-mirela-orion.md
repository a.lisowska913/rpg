# Mirela Orion
## Identyfikator

Id: 2007-mirela-orion

## Sekcja Opowieści

### Nocna Krypta i Emulatorka

* **uid:** 200729-nocna-krypta-i-emulatorka, _numer względny_: 5
* **daty:** 0110-11-16 - 0110-11-22
* **obecni:** AK Nocna Krypta, Antoni Kramer, Arianna Verlen, Damian Orion, Eustachy Korkoran, Kijara d'Esuriit, Klaudia Stryk, Laura Orion, Leona Astrienko, Mirela Orion, OO Castigator, OO Minerwa

Streszczenie:

Laura, Emulatorka na Nocnej Krypcie została Skażona przez Kijarę. Laura opanowana wizjami Esuriit chciała zniszczyć Eternię. Nie dało się jej powstrzymać - Mirela, Zespół - byli za słabi. Arianna wezwała Kryptę pod ogniem Castigatora i wypaliła Kryptę i Kijarę do zera, uszkadzając "zdrowie psychiczne Krypty". Zginęło kilkudziesięciu ludzi, ale Arianna dostała podziękowanie od Admiralicji. Damian Orion jest rozczarowany.

Aktor w Opowieści:

* Dokonanie:
    * nieskończenie oddana Damianowi Emulatorka, wzięła Czarną Aleksandrię by wezwać Kryptę. Nie była w stanie pokonać Laury d'Esuriit. Misja przegrana.


### Bardzo niebezpieczne składowisko

* **uid:** 190726-bardzo-niebezpieczne-skladowisko, _numer względny_: 4
* **daty:** 0110-06-22 - 0110-06-24
* **obecni:** Amanda Kajrat, Cezary Alentik, Ernest Kajrat, Gabriel Ursus, Mirela Orion, Pięknotka Diakon, Roland Grzymość

Streszczenie:

Na Składowisku Odpadów Niebezpiecznych przy Podwiercie siły Wolnego Uśmiechu zaczęły składać jakieś narkotyki. Nie podoba się to Błękitnemu Niebu. Pięknotka w środku tego wszystkiego - udało jej się poprosić Orbiter o zajęcie się linią dostawczą z Cieniaszczytu, Kajrata by chwilowo zwolnił, Pustogor by chronili właściciela a Grzymościa, by zszedł do podziemia. Polityczny sukces - ale czemu Grzymość idzie w jakieś narkotyki? I czemu Kajrat dąży do wojny z Grzymościem o te narkotyki?

Aktor w Opowieści:

* Dokonanie:
    * kontakt Pięknotki z Orbiterem; przekonała szefostwo, że warto odciąć Wolny Uśmiech od kanałów przerzutowych z Cieniaszczytu.


### Odzyskana agentka Orbitera

* **uid:** 190724-odzyskana-agentka-orbitera, _numer względny_: 3
* **daty:** 0110-06-08 - 0110-06-11
* **obecni:** Adam Szarjan, Aida Serenit, Julia Morwisz, Mirela Niecień, Mirela Orion, Moktar Gradon, Pięknotka Diakon

Streszczenie:

Aidę porwała frakcja Cieniaszczytu zainspirowana przez Julię - by ratować Emulatorkę. Ale Aida, jak się okazało, jest nędznym cywilem i to takim uratowanym z kosmosu. Pięknotka wygrała z championem kralotha przez zanęcenie Moktara - a zwabiła go dając mu walkę z Mirelą. Ogólnie, wszyscy są zadowoleni..?

Aktor w Opowieści:

* Dokonanie:
    * ściągnęła dyskretnie Damiana, znalazła przemytników oraz dowiedziała się kim jest naprawdę Aida. By ratować Aidę stoczyła walkę z Moktarem - i przegrała.


### Kirasjerka najgorszym detektywem

* **uid:** 190721-kirasjerka-najgorszym-detektywem, _numer względny_: 2
* **daty:** 0110-06-06 - 0110-06-07
* **obecni:** Aida Serenit, Mirela Orion, Olga Myszeczka, Pięknotka Diakon, Wiktor Satarail

Streszczenie:

Przyjaciółka Kirasjerki Orbitera, Aida, zniknęła. Kirasjerka poszła jej szukać i znalazła ślady Pięknotki. Oczywiście, Kirasjerka zaatakowała by zdobyć informacje a Pięknotka nie mogła się poddać (osłaniana przez Amandę). Skończyło się na dewastacji 3 servarów, ciężko rannej Kirasjerce i stropionej Pięknotce. Gdy Pięknotka poszła do Olgi znaleźć leczenie dla Mireli (Kirasjerki), tam dowiedziała się od Wiktora Sataraila, że on podłożył ślady by pokazać jej obecność Cieniaszczytu - a dokładniej kralotycznej bioformy. Największe możliwe nieporozumienie.

Aktor w Opowieści:

* Dokonanie:
    * emulator Kirasjerów; była na wakacjach. Szukała gdzie zniknęła Aida (przyjaciółka). Ślady prowadziły do Pięknotki więc zaatakowała. Straciła Calibris, uszkadzając Cienia.


### Bardzo nieudane porwania

* **uid:** 190503-bardzo-nieudane-porwania, _numer względny_: 1
* **daty:** 0110-04-14 - 0110-04-16
* **obecni:** Damian Orion, Karla Mrozik, Minerwa Metalia, Mirela Orion, Nikola Kirys, Pięknotka Diakon

Streszczenie:

Kirasjerzy Orbitera dostali zadanie odbicia Minerwy i Nikoli, ale w mieszkaniu Minerwy czekała Pięknotka. Porwała z zaskoczenia Emulatorkę, uwolniła ją Cieniem i gdy Kirasjerzy uderzyli by po cichu odzyskać agentkę, Pięknotka poprosiła o pomoc Epirjon. W wyniku Pięknotce udało się ewakuować z Emulatorką, uwolniła ją Cieniem i wsadziła do Pustogoru. Potem z Karlą wynegocjowała ochronę dla Minerwy i Nikoli i jeszcze z tymi dwoma załatwiła by to było możliwe. Kirasjerzy zostali odepchnięci przez JEDNĄ terminuskę Pustogoru.

Aktor w Opowieści:

* Dokonanie:
    * Emulator Kirasjerów; uwolniona przez Cienia. Pokonana z zaskoczenia przez Pięknotkę, skończyła w lochu w Pustogorze przed zwróceniem jej Kirasjerom.
* Progresja:
    * Emulatorka uwolniona przez Cienia; nie jest już lalką w rękach kontrolera. Trzeci wolny Emulator.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 5, @: 0110-11-22
    1. Primus    : 5, @: 0110-11-22
        1. Sektor Astoriański    : 5, @: 0110-11-22
            1. Astoria    : 4, @: 0110-06-24
                1. Sojusz Letejski, SW    : 1, @: 0110-04-16
                    1. Granica Anomalii    : 1, @: 0110-04-16
                        1. Pacyfika, obrzeża    : 1, @: 0110-04-16
                1. Sojusz Letejski    : 4, @: 0110-06-24
                    1. Przelotyk    : 1, @: 0110-06-11
                        1. Przelotyk Wschodni    : 1, @: 0110-06-11
                            1. Cieniaszczyt    : 1, @: 0110-06-11
                                1. Knajpka Szkarłatny Szept    : 1, @: 0110-06-11
                                1. Kompleks Nukleon    : 1, @: 0110-06-11
                            1. Przejściak    : 1, @: 0110-06-11
                                1. Hotel Pirat    : 1, @: 0110-06-11
                    1. Szczeliniec    : 3, @: 0110-06-24
                        1. Powiat Pustogorski    : 3, @: 0110-06-24
                            1. Czarnopalec    : 1, @: 0110-06-07
                                1. Pusta Wieś    : 1, @: 0110-06-07
                            1. Czółenko    : 1, @: 0110-06-07
                                1. Bunkry    : 1, @: 0110-06-07
                            1. Podwiert, okolice    : 2, @: 0110-06-24
                                1. Bioskładowisko podziemne    : 2, @: 0110-06-24
                            1. Podwiert    : 1, @: 0110-04-16
                                1. Kosmoport    : 1, @: 0110-04-16
                            1. Pustogor    : 1, @: 0110-04-16
                                1. Interior    : 1, @: 0110-04-16
                                    1. Bunkry Barbakanu    : 1, @: 0110-04-16
                                1. Rdzeń    : 1, @: 0110-04-16
                                    1. Szpital Terminuski    : 1, @: 0110-04-16
                            1. Zaczęstwo, obrzeża    : 1, @: 0110-04-16
                            1. Zaczęstwo    : 1, @: 0110-04-16
                                1. Osiedle Ptasie    : 1, @: 0110-04-16
            1. Obłok Lirański    : 1, @: 0110-11-22
        1. Sektor Lacarin    : 1, @: 0110-11-22

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Pięknotka Diakon     | 4 | ((190503-bardzo-nieudane-porwania; 190721-kirasjerka-najgorszym-detektywem; 190724-odzyskana-agentka-orbitera; 190726-bardzo-niebezpieczne-skladowisko)) |
| Aida Serenit         | 2 | ((190721-kirasjerka-najgorszym-detektywem; 190724-odzyskana-agentka-orbitera)) |
| Damian Orion         | 2 | ((190503-bardzo-nieudane-porwania; 200729-nocna-krypta-i-emulatorka)) |
| Adam Szarjan         | 1 | ((190724-odzyskana-agentka-orbitera)) |
| AK Nocna Krypta      | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Amanda Kajrat        | 1 | ((190726-bardzo-niebezpieczne-skladowisko)) |
| Antoni Kramer        | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Arianna Verlen       | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Cezary Alentik       | 1 | ((190726-bardzo-niebezpieczne-skladowisko)) |
| Ernest Kajrat        | 1 | ((190726-bardzo-niebezpieczne-skladowisko)) |
| Eustachy Korkoran    | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Gabriel Ursus        | 1 | ((190726-bardzo-niebezpieczne-skladowisko)) |
| Julia Morwisz        | 1 | ((190724-odzyskana-agentka-orbitera)) |
| Karla Mrozik         | 1 | ((190503-bardzo-nieudane-porwania)) |
| Kijara d'Esuriit     | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Klaudia Stryk        | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Laura Orion          | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Leona Astrienko      | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Minerwa Metalia      | 1 | ((190503-bardzo-nieudane-porwania)) |
| Mirela Niecień       | 1 | ((190724-odzyskana-agentka-orbitera)) |
| Moktar Gradon        | 1 | ((190724-odzyskana-agentka-orbitera)) |
| Nikola Kirys         | 1 | ((190503-bardzo-nieudane-porwania)) |
| Olga Myszeczka       | 1 | ((190721-kirasjerka-najgorszym-detektywem)) |
| OO Castigator        | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| OO Minerwa           | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Roland Grzymość      | 1 | ((190726-bardzo-niebezpieczne-skladowisko)) |
| Wiktor Satarail      | 1 | ((190721-kirasjerka-najgorszym-detektywem)) |
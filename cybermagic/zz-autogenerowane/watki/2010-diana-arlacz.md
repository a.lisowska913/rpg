# Diana Arłacz
## Identyfikator

Id: 2010-diana-arlacz

## Sekcja Opowieści

### Sympozjum Zniszczenia

* **uid:** 210120-sympozjum-zniszczenia, _numer względny_: 9
* **daty:** 0111-06-03 - 0111-06-07
* **obecni:** Arianna Verlen, Diana Arłacz, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Maria Naavas, Martyn Hiwasser, OA Bakałarz, Olgierd Drongon

Streszczenie:

Maria Naavas próbuje spiknąć Ariannę i Olgierda i do pomocy używa Eustachego. Później - Eustachy, Diana, Klaudia i Elena są na Sympozjum Zniszczenia OA Bakałarz. Próbują przebić się przez anomalną materię. Diana odkrywa słabość owej materii do Esuriit i się Skaża; Skażenie rozlewa się po Bakałarzu i Zespół dostaje opiernicz za niekompetencję (większość na Elenę). Zespół w gniewie, nienawiści i Esuriit fabrykuje Emiter Plagi Nienawiści - nową broń na pokład Inferni, piekielnie niebezpieczną...

Aktor w Opowieści:

* Dokonanie:
    * by zaimponować Eustachemu za wszelką cenę weszła w głąb energii Esuriit; została przez Esuriit opętana i wyciągnął ją Eustachy. Ale wymusiła pocałunek. Poważnie skrzywdziła kogoś.
* Progresja:
    * dwa miesiące kuracji po Esuriit. W trakcie - psycholog. Potem - kurator. Plus zniszczona reputacja; persona non grata na imprezach. Ogólnie, tragedia. Ale wymusiła całus Eustachego. Warto było? XD
    * reputacja "świetni w niszczeniu i destrukcji, ale idźcie na bok i nie psujcie więcej". Takie trochę dzieci od zniszczenia.


### SOS z haremu

* **uid:** 210106-sos-z-haremu, _numer względny_: 8
* **daty:** 0111-05-28 - 0111-05-29
* **obecni:** Arianna Verlen, Diana Arłacz, Elena Verlen, Eustachy Korkoran, Horacy Aktenir, Julia Aktenir, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Rozalia Teirik

Streszczenie:

Pałacyk rodu Aktenir został infestowany przez kralotha. Julia, zesłana z Orbitera za zerwanie z przeciwnikiem Eustachego, wysłała SOS do Orbitera. Infernia rusza Julii na pomoc. Gdy odkryli że chodzi o kralotha to zamiast wchodzić na kralotyczny teren, zorganizowali plan "arystokratki na orgię" i wyciągneli kralotycznego niewolnika (Horacego Aktenira) na otwartą przestrzeń, po czym Arianna uruchomiła obcą sentisieć a Eustachy zawołał do Julii rozmytej w środku...

Aktor w Opowieści:

* Dokonanie:
    * pokazała swoją "ostrą", perwersyjną stronę w służbie zastawienia pułapki na kralotycznie zdominowanego arystokratę. "She's so wild noone can tame her". Zneutralizowała pocałunkiem Horacego.


### Porwanie Anastazji z Odkupienia

* **uid:** 210127-porwanie-anastazji-z-odkupienia, _numer względny_: 7
* **daty:** 0111-05-24 - 0111-05-25
* **obecni:** Anastazja Sowińska Dwa, Arianna Verlen, Dariusz Krantak, Diana Arłacz, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Martyn Hiwasser, OA Odkupienie, OO Infernia, Rufus Niegnat, SP Plugawy Jaszczur

Streszczenie:

Arianna, Eustachy i Klaudia oddali "Anastazję" na OA Odkupienie tylko po to, by ją stamtąd porwać. Nawet załoga Inferni o tym nie miała pojęcia. Okazało się, że Odkupienie jest sentisieciowanym specjalnym statkiem Aurum, gdzie ci co narazili się Sowińskim mają szansę na "odkupienie".

Aktor w Opowieści:

* Dokonanie:
    * wpadła na pomysł chorej anomalii by odsunąć pretorian. A potem posłużyła jako dywersja dla Eleny. Ogólnie - perfekcyjna Eris.


### Nieprawdopodobny zbieg okoliczności

* **uid:** 201224-nieprawdopodobny-zbieg-okolicznosci, _numer względny_: 6
* **daty:** 0111-02-15 - 0111-02-16
* **obecni:** Diana Arłacz, Elena Verlen, Grzegorz Chropst, Jasmina Perikas, Klaudia Stryk

Streszczenie:

Nieco zaniedbane dziecko bawiące się na K1. Skażona Elainka, na którą polują. Badaczka biomantyczna, która objęła rozpadające się biolab. I Klaudia, która z Eleną wplątała się w to wszystko by naprawić sytuację i trafiła na ślad ducha opiekuńczego Kontrolera Pierwszego. "Zofia"?

Aktor w Opowieści:

* Dokonanie:
    * pokazała co potrafi potęgą entropicznej anihilacji, strącając Grzegorza z sufitu. Jednak nie nienawidzą się z Eleną.


### Wyścig jako randka?

* **uid:** 201125-wyscig-jako-randka, _numer względny_: 5
* **daty:** 0111-02-08 - 0111-02-13
* **obecni:** Arianna Verlen, Diana Arłacz, Gunnar Brunt, Klaudia Stryk, Maria Naavas, Martyn Hiwasser, Olgierd Drongon

Streszczenie:

Infernia traci części i ekipę remontową, bo Olgierd z Żelazka na niej pasożytuje. W wyniku komedii pomyłek doszło do tego, że Olgierd uznał, że Arianna się w nim podkochuje ale nie wie jak zagadać. Skończyło się pojedynkiem ścigaczy na torze sfabrykowanym przez Klaudię i na anomalicznym Skażeniu Kontrolera Pierwszego. Przez Klaudię, naturalnie.

Aktor w Opowieści:

* Dokonanie:
    * przyzwoitka Arianny na spotkaniu z Olgierdem. Monotematyczna: "Eustachy zrobiłby to lepiej niż Olgierd".


### Krypta i Wyjec

* **uid:** 201216-krypta-i-wyjec, _numer względny_: 4
* **daty:** 0111-01-29 - 0111-01-31
* **obecni:** AK Nocna Krypta, AK Wyjec, Anastazja Sowińska, Arianna Verlen, Diana Arłacz, Eustachy Korkoran, Kamil Lyraczek, Klaudia Stryk, Martyn Hiwasser

Streszczenie:

Podczas walki Nocnej Krypty i Wyjca Zespół wbił się na Nocną Kryptę, by wyleczyć się z Różowej Plagi. Na miejscu okazało się, że Krypta ma "nowe programowanie" (leczenie lub 'zachowanie'), nowe mechanizmy (MIRV, anomalne działo, anioły) oraz że jest groźniejsza niż kiedykolwiek. Arianna zostawiła na pokładzie Krypty Anastazję, by Krypta naprawiła jej Wzór, po czym oszukała Sowińskich, że niby Krypta porwała Anastazję.

Aktor w Opowieści:

* Dokonanie:
    * na pokładzie Krypty poddała się halucynacjom i obrazom eksplozji. Podatna na moc mentalną Krypty.


### Anastazja - bohaterką?

* **uid:** 201118-anastazja-bohaterka, _numer względny_: 3
* **daty:** 0111-01-22 - 0111-01-25
* **obecni:** Anastazja Sowińska, Arianna Verlen, Diana Arłacz, Eustachy Korkoran, Kamil Lyraczek, Klaudia Stryk, Martyn Hiwasser, Stefan Jamniczek

Streszczenie:

Anastazja nie została bohaterką. Wpierw Eustachy wplątał się w pojedynek o Dianę z jakimś szlachcicem - skończyło się różową mgiełką miłości i pospieszną ewakuacją Inferni z Kontrolera Pierwszego, potem epicki plan Izy z piratami rozpadł się, bo na Infernię polował jakiś statek koloidowy. Ale przetrwali - i mają zamiar to wszystko rozplątać.

Aktor w Opowieści:

* Dokonanie:
    * doprowadziła do pojedynku między Eustachym a Jamniczkiem, bo nie umiała trzymać języka za zębami i "Eustachy jest najlepszy i męski".


### Sabotaż świni

* **uid:** 201104-sabotaz-swini, _numer względny_: 2
* **daty:** 0111-01-13 - 0111-01-16
* **obecni:** Anastazja Sowińska, Arianna Verlen, Ataienne, Diana Arłacz, Eustachy Korkoran, Klaudia Stryk, Martyn Hiwasser, Rafał Armadion, Robert Garwen

Streszczenie:

Sabotażysta - miragent - podjął próbę zniszczenia Arianny gdy ta ładowała świnie na Wesołego Wieprzka używając wybuchowej, anomalnej świni. Zespół przetrwał, choć "Wieprzek" został zniszczony. Arianna pojmała miragenta i przekazała go do Kontrolera Pierwszego, acz kosztem ofiar w ludziach. No i nie było dość świń by je wziąć na orbitę XD.

Aktor w Opowieści:

* Dokonanie:
    * żywa bateria energii magicznej, by Eustachy mógł uratować Ariannę i załogę anomalicznego Wesołego Wieprzka.


### Noktianie rodu Arłacz

* **uid:** 201021-noktianie-rodu-arlacz, _numer względny_: 1
* **daty:** 0111-01-07 - 0111-01-10
* **obecni:** Anastazja Sowińska, Arianna Verlen, Ataienne, Diana Arłacz, Eliza Ira, Eustachy Korkoran, Izabela Zarantel, Jolanta Arłacz, Juliusz Sowiński, Klaudia Stryk, Klaus Rumak, Maria Gołąb, OO Szalony Rumak, OO Wesoły Wieprzek, Robert Arłacz, Wanessa Pyszcz

Streszczenie:

By uzyskać współpracę Elizy Iry, Arianna zdecydowała się "porwać" noktian rodu Arłacz wykorzystywanych do produkcji czystej krwi dla magów. Z uwagi na działania Marii Gołąb plan został odrzucony i wykryty, ale znalezienie haków na Arłaczy rozwiązało ten problem - Arianna i Arłacze współpracują. Nie doszło do porozumienia z Elizą Irą, ale Eliza się wycofała z Trzeciego Raju, ogłaszając się protektorką tego terenu. Sytuacja w miarę ustabilizowana.

Aktor w Opowieści:

* Dokonanie:
    * Była senti-następczynią, ale po 'korekcji' Roberta została przesunięta na drugi plan. Uciekła z Eustachym na pokład Inferni, odrzucając ród i ratując noktian. Piromanka.
* Progresja:
    * wyrzucona z rodu Arłacz mimo umiejętności sprzęgania się z Sentisiecią za zdradę na rzecz Arianny Verlen.
    * zafascynowana i podkochująca się w Eustachym Korkoranie; przekonana, że on ma fetysz że INNI MUSZĄ PATRZEĆ.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 9, @: 0111-06-07
    1. Primus    : 9, @: 0111-06-07
        1. Sektor Astoriański    : 9, @: 0111-06-07
            1. Astoria, Orbita    : 3, @: 0111-05-29
                1. Kontroler Pierwszy    : 3, @: 0111-05-29
                    1. Sektor 22    : 1, @: 0111-05-29
                        1. Dom Uciech Wszelakich    : 1, @: 0111-05-29
                        1. Kasyno Stanisława    : 1, @: 0111-05-29
                    1. Sektor 43    : 2, @: 0111-02-16
                        1. Laboratorium biomantyczne    : 1, @: 0111-02-16
                        1. Tor wyścigowy ścigaczy    : 2, @: 0111-02-16
            1. Astoria    : 3, @: 0111-05-29
                1. Sojusz Letejski, NW    : 2, @: 0111-01-16
                    1. Ruiniec    : 2, @: 0111-01-16
                        1. Pustynia Kryształowa    : 1, @: 0111-01-10
                        1. Trzeci Raj    : 2, @: 0111-01-16
                1. Sojusz Letejski    : 2, @: 0111-05-29
                    1. Aurum    : 2, @: 0111-05-29
                        1. Powiat Niskowzgórza    : 1, @: 0111-01-10
                            1. Domena Arłacz    : 1, @: 0111-01-10
                                1. Posiadłość Arłacz    : 1, @: 0111-01-10
                                    1. Baterie Puryfikacji Krwi    : 1, @: 0111-01-10
                                    1. Małe lądowisko    : 1, @: 0111-01-10
                        1. Wielkie Księstwo Aktenir    : 1, @: 0111-05-29
                            1. Pałac Jasnego Ognia    : 1, @: 0111-05-29

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Klaudia Stryk        | 9 | ((201021-noktianie-rodu-arlacz; 201104-sabotaz-swini; 201118-anastazja-bohaterka; 201125-wyscig-jako-randka; 201216-krypta-i-wyjec; 201224-nieprawdopodobny-zbieg-okolicznosci; 210106-sos-z-haremu; 210120-sympozjum-zniszczenia; 210127-porwanie-anastazji-z-odkupienia)) |
| Arianna Verlen       | 8 | ((201021-noktianie-rodu-arlacz; 201104-sabotaz-swini; 201118-anastazja-bohaterka; 201125-wyscig-jako-randka; 201216-krypta-i-wyjec; 210106-sos-z-haremu; 210120-sympozjum-zniszczenia; 210127-porwanie-anastazji-z-odkupienia)) |
| Eustachy Korkoran    | 7 | ((201021-noktianie-rodu-arlacz; 201104-sabotaz-swini; 201118-anastazja-bohaterka; 201216-krypta-i-wyjec; 210106-sos-z-haremu; 210120-sympozjum-zniszczenia; 210127-porwanie-anastazji-z-odkupienia)) |
| Martyn Hiwasser      | 7 | ((201104-sabotaz-swini; 201118-anastazja-bohaterka; 201125-wyscig-jako-randka; 201216-krypta-i-wyjec; 210106-sos-z-haremu; 210120-sympozjum-zniszczenia; 210127-porwanie-anastazji-z-odkupienia)) |
| Anastazja Sowińska   | 4 | ((201021-noktianie-rodu-arlacz; 201104-sabotaz-swini; 201118-anastazja-bohaterka; 201216-krypta-i-wyjec)) |
| Elena Verlen         | 4 | ((201224-nieprawdopodobny-zbieg-okolicznosci; 210106-sos-z-haremu; 210120-sympozjum-zniszczenia; 210127-porwanie-anastazji-z-odkupienia)) |
| Ataienne             | 2 | ((201021-noktianie-rodu-arlacz; 201104-sabotaz-swini)) |
| Kamil Lyraczek       | 2 | ((201118-anastazja-bohaterka; 201216-krypta-i-wyjec)) |
| Maria Naavas         | 2 | ((201125-wyscig-jako-randka; 210120-sympozjum-zniszczenia)) |
| Olgierd Drongon      | 2 | ((201125-wyscig-jako-randka; 210120-sympozjum-zniszczenia)) |
| AK Nocna Krypta      | 1 | ((201216-krypta-i-wyjec)) |
| AK Wyjec             | 1 | ((201216-krypta-i-wyjec)) |
| Anastazja Sowińska Dwa | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| Dariusz Krantak      | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| Eliza Ira            | 1 | ((201021-noktianie-rodu-arlacz)) |
| Grzegorz Chropst     | 1 | ((201224-nieprawdopodobny-zbieg-okolicznosci)) |
| Gunnar Brunt         | 1 | ((201125-wyscig-jako-randka)) |
| Horacy Aktenir       | 1 | ((210106-sos-z-haremu)) |
| Izabela Zarantel     | 1 | ((201021-noktianie-rodu-arlacz)) |
| Jasmina Perikas      | 1 | ((201224-nieprawdopodobny-zbieg-okolicznosci)) |
| Jolanta Arłacz       | 1 | ((201021-noktianie-rodu-arlacz)) |
| Julia Aktenir        | 1 | ((210106-sos-z-haremu)) |
| Juliusz Sowiński     | 1 | ((201021-noktianie-rodu-arlacz)) |
| Klaus Rumak          | 1 | ((201021-noktianie-rodu-arlacz)) |
| Leona Astrienko      | 1 | ((210106-sos-z-haremu)) |
| Maria Gołąb          | 1 | ((201021-noktianie-rodu-arlacz)) |
| OA Bakałarz          | 1 | ((210120-sympozjum-zniszczenia)) |
| OA Odkupienie        | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| OO Infernia          | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| OO Szalony Rumak     | 1 | ((201021-noktianie-rodu-arlacz)) |
| OO Wesoły Wieprzek   | 1 | ((201021-noktianie-rodu-arlacz)) |
| Rafał Armadion       | 1 | ((201104-sabotaz-swini)) |
| Robert Arłacz        | 1 | ((201021-noktianie-rodu-arlacz)) |
| Robert Garwen        | 1 | ((201104-sabotaz-swini)) |
| Rozalia Teirik       | 1 | ((210106-sos-z-haremu)) |
| Rufus Niegnat        | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| SP Plugawy Jaszczur  | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| Stefan Jamniczek     | 1 | ((201118-anastazja-bohaterka)) |
| Wanessa Pyszcz       | 1 | ((201021-noktianie-rodu-arlacz)) |
# Ksenia Kirallen
## Identyfikator

Id: 1909-ksenia-kirallen

## Sekcja Opowieści

### Waśń o ryby w Majkłapcu

* **uid:** 211127-waśń-o-ryby-w-majklapcu, _numer względny_: 13
* **daty:** 0111-08-15 - 0111-08-20
* **obecni:** Arkadia Verlen, Daniel Terienak, Genowefa Krecik, Iwan Zawtrak, Karolina Terienak, Ksenia Kirallen, Paweł Szprotka, Rafał Torszecki, Stella Amakirin

Streszczenie:

Daniel chciał rozwiązać problem w Majkłapcu, gdzie koty poraniły ryby. Okazało się, że to wina mafii, której właściciel kociarni nie chce płacić okupu. Karolina zebrała drużynę, uratowali zatrute wściekłością koty i zaatakowali siedzibę małego oddziałka mafii. Po zdobyciu dowodów (i ucieczce) przekazali temat Ksenii.

Aktor w Opowieści:

* Dokonanie:
    * oddelegowywana do wszystkich małych waśni pomiędzy firmami, bo jest absolutnie przerażająca i jeśli to coś nieważnego, NIKT JEJ NIE CHCE. 3 mc temu rozwiązała problem z Paprykarzem Z Majkłapca. A teraz pojawił się inny i jej nie wezwano. ALE! Jak tylko dostała dowody, że działa tam mafia (Stella Amarkirin) to ruszyła do działania.


### Koszt ratowania Torszeckiego

* **uid:** 211026-koszt-ratowania-torszeckiego, _numer względny_: 12
* **daty:** 0111-07-28 - 0111-08-01
* **obecni:** Ignacy Myrczek, Ksenia Kirallen, Marysia Sowińska, Olga Myszeczka, Paweł Szprotka, Rafał Torszecki, Sensacjusz Diakon, Teresa Mieralit, Wiktor Satarail

Streszczenie:

By ratować Torszeckiego, Marysia wchodzi w sojusz z Wiktorem Satarailem. On dał jej podskórnego robaka którego dostanie Torszecki, by uzasadnić dziwne zachowanie. Wiktor "dostanie swoją zapłatę" od "kogoś winnego". Marysia przekonała też Sensacjusza, że ona x Torszecki. Zaczęły się też pojawiać takie plotki...

Aktor w Opowieści:

* Dokonanie:
    * KIEDYŚ miała konia z pozytywną energią magiczną. Po jej "wypadku" oddała go na kampus uczelni. Bo tu jest bezpieczny i może pomagać innym.


### Bardzo straszna mysz

* **uid:** 200616-bardzo-straszna-mysz, _numer względny_: 11
* **daty:** 0110-09-14 - 0110-09-17
* **obecni:** Diana Lauris, Franek Bulterier, Gabriel Ursus, Henryk Wkrąż, Ksenia Kirallen, Laura Tesinik, Matylda Sęk

Streszczenie:

Kilka Rekinów chciało się podlizać Gerwazemu Lemurczakowi i postraszyć Dianę i Melindę. Diana w odpowiedzi sprawiła, że ich eksperymentalna 'mysz Esuriit' uciekła i zaczęła stanowić prawdziwe zagrożenie. W REAKCJI NA TO Laura i Gabriel rozbroili sytuację, wykryli obecność Diany i zatarli wszelkie ślady. Diana jednak nie wybacza i nie zapomina a Henryk też chce iść w temat dalej...

Aktor w Opowieści:

* Dokonanie:
    * wzięła operację 'potwór w okolicach terenu Esuriit', ale Pięknotka odepchnęła ją z tego przypisując Gabriela i Laurę. Napisała odpowiednie rzeczy do papierów.


### Kontrpolowanie Pięknotki - pułapka

* **uid:** 191103-kontrpolowanie-pieknotki-pulapka, _numer względny_: 10
* **daty:** 0110-06-20 - 0110-06-27
* **obecni:** Damian Orion, Józef Małmałaz, Ksenia Kirallen, Lilia Ursus, Marek Puszczok, Mariusz Trzewń, Minerwa Metalia, Olaf Zuchwały, Pięknotka Diakon, Rafał Roszczeniok

Streszczenie:

Pięknotka poluje na noktiańskiego zabójcę. W ciągu tygodnia udało jej się złożyć do kupy pułapkę, rozpocząć linię przerzutową artefaktów do Aurum, połączyć Orbiter, Lilię, Minerwę oraz Pustogor i doprowadzić do tego, że docelowo Małmałaz ma zostać złapany i doprowadzony do Pustogoru w zemście za krzywdę Alana. To był bardzo trudny tydzień, politycznie.

Aktor w Opowieści:

* Dokonanie:
    * podeszła do tematu Małmałaza z charakterystyczną dla siebie łagodnością i subtelnością. Pięknotka zatrzymała ją, zanim Ksenia zantagonizowała cały świat. Ksenia i Pięknotka współpracują.


### Rexpapier i Włóknin

* **uid:** 190912-rexpapier-i-wloknin, _numer względny_: 9
* **daty:** 0110-06-17 - 0110-06-21
* **obecni:** Anna Warlank, Dariusz Kuromin, Eleonora Rdeść, Jarek Gułanczak, Kasjopea Maus, Ksenia Kirallen, Mateusz Urszank

Streszczenie:

Anna z Rexpapier próbuje zniszczyć Włóknin za grzechy ojców. To jest praprzyczyna którą wykryli detektywi Kasjopei, idąc po nitce do kłębka. Jednocześnie szef Rexpapier, Jarek, który był bezpośrednim egzekutorem jest człowiekiem dobrym - jego sekretarka weźmie na siebie winę by tylko jemu nic się nie stało. Odpowiedź Kasjopei? Reality show, które Annę i Dariusza zbliży by zniszczyć tą upiorną wendettę.

Aktor w Opowieści:

* Dokonanie:
    * nieubłagana terminuska skłonna do zniszczenia Włóknina, Rexpapier, czegokolwiek. Wycofana przez Centralę, bo tu wystarczy subtelność.


### Wypadek w Kramamczu

* **uid:** 190906-wypadek-w-kramamczu, _numer względny_: 8
* **daty:** 0110-06-14 - 0110-06-15
* **obecni:** Dariusz Kuromin, Kasjopea Maus, Ksenia Kirallen, Mariusz Trzewń, Pięknotka Diakon

Streszczenie:

Wrabiają Wiktora Sataraila w atak na niewielką firmę, Włóknin z Kramamczy! Pięknotka i Ksenia wysłane by to naprawić! Pięknotka przyzwała z powrotem wszystkie królowe które pouciekały a Ksenia odkryła sabotaż wewnętrzny. Dariusz próbuje wyciągnąć ubezpieczenie. Pięknotka wezwała Kasjopeę (dziennikarkę) do pomocy i przekonała Ksenię, żeby ta odpuściła Dariuszowi tylko znalazła osoby winne tej sytuacji i prowokacji.

Aktor w Opowieści:

* Dokonanie:
    * inkwizytorka i mistrzyni śledztw. Chce zniszczyć Włóknin po tym jak się dowiedziała o sytuacji; ale uspokoiła ją Pięknotka i Kasjopea. Buduje śledztwo w tej sprawie.


### Sekret Samanty Arienik

* **uid:** 220119-sekret-samanty-arienik, _numer względny_: 7
* **daty:** 0085-07-26 - 0085-07-28
* **obecni:** Arazille, Błażej Arienik, Felicjan Szarak, Klaudia Stryk, Ksenia Kirallen, Maria Arienik, Samanta Arienik, Sławomir Arienik, Talia Aegis, Urszula Arienik

Streszczenie:

Gdy doszło do katastrofy w Podwiercie i pojawiły się dziwne karaluchy magiczne to Klaudia, Ksenia i Felicjan ruszyli do pomocy - wraz z dziewczyną Felicjana, Samantą. Samanta uruchomiła sprzęt Arieników by pomóc. Podczas operacji czyszczenia okazało się, że z Samantą coś jest nie tak; "psychotronika uszkodzona"? Samanta i Klaudia szybko ruszyły do Talii Aegis - tylko ona może pomóc znaleźć odpowiedź. Okazało się, że Samanta nie żyje od dawna; z rodu Arienik przetrwał tylko Sławomir i Ula. Talii udało się przenieść ród Arienik w "żywe TAI". Sławomir umarł. Głową rodziny jest teraz młoda Ula, z pomocą trzech TAI - kiedyś jej rodziny. Wpływ Arazille - odepchnięty. Jednak podczas naprawy Samanty Coś Się Stało z seksbotami nad którymi pracowała Talia dla mafii...

Aktor w Opowieści:

* Dokonanie:
    * próbuje na podstawie krwi Samanty zbudować mechanizm niszczenia karaluchów; skończyło się silnym backslashem. Samanta nie ma krwi organicznej i magia zadziałała _źle_. Klaudia musiała ją spuryfikować.


### Czarodziejka, która jednak może się zabić

* **uid:** 211019-czarodziejka-ktora-jednak-moze-sie-zabic, _numer względny_: 6
* **daty:** 0084-12-20 - 0084-12-24
* **obecni:** Arnulf Poważny, Klaudia Stryk, Ksenia Kirallen, Mariusz Trzewń, Maryla Koternik, Talia Aegis, Teresa Mieralit

Streszczenie:

Nastroje antynoktiańskie się nasilają. Pojawiły się seksboty do bicia, w kształcie noktian i noktianek (co strasznie zestresowało Teresę). Klaudia zaproponowała Arnulfowi powiększenie kadry o Weteranów i przejmując to prawie się zarżnęła (uratował ją Trzewń). Maryla szuka informacji o Teresie - szuka ukrytego noktianina na AMZ, ale Trzewń wykluczył Teresę dzięki dokumentom sformowanym przez Arnulfa. Klaudia i Ksenia pracują nad petycją, by stworzyć Dom Weteranów Noctis.

Aktor w Opowieści:

* Dokonanie:
    * z Klaudią budują petycję by weterani noktiańscy też mieli Dom Weterana. Nieskutecznie socjalizuje Teresę - ta nie chce iść na zakupy bez pieniędzy.


### Nastolatka w bieliźnie na dachu w burzy

* **uid:** 211017-nastolatka-w-bieliznie-na-dachu-w-burzy, _numer względny_: 5
* **daty:** 0084-12-14 - 0084-12-15
* **obecni:** Arnulf Poważny, Klaudia Stryk, Ksenia Kirallen, Sasza Morwowiec, Teresa Mieralit, Waldemar Grzymość

Streszczenie:

W środku nocy Klaudia i Ksenia widzą Teresę w środku burzy na dachu Złomiarium. Poszły ją ściągnąć i natknęły się na terminusa. Ksenia odciągnęła Saszę od Teresy, Klaudia Teresę wysłała do ich pokoju w akademiku (gdzie Teresa spisana za nieletnią prostytucję). Sasza wyjaśnił, że nadmiar lojalności wobec AMZ jest szkodliwy. Klaudia ogromnym wysiłkiem spowolniła Saszę i odwróciła jego uwagę. Potem Klaudia i Ksenia zdecydowały, że zsocjalizują małą noktiankę - i zdobyły trzyosobowy pokój w akademiku mimo protestów Teresy.

Aktor w Opowieści:

* Dokonanie:
    * lojalna przyjaciółka Klaudii; dała się złapać terminusowi by odwrócić uwagę od Teresy na prośbę Klaudii nie wiedząc o co chodzi. Wie, że Teresa jest noktianką i chce jej mimo wszystko pomóc i ją zsocjalizować. Klaudia BARDZO docenia.
* Progresja:
    * mieszka w akademiku AMZ z Klaudią i Teresą.
    * ma dostęp do "pokoju schadzek" w akademiku AMZ tylko dla niej (czyli chwilowo też dla Felicjana ;p)


### Ukryta wychowanka Arnulfa

* **uid:** 211010-ukryta-wychowanka-arnulfa, _numer względny_: 4
* **daty:** 0084-12-11 - 0084-12-12
* **obecni:** Arnulf Poważny, Felicjan Szarak, Klaudia Stryk, Ksenia Kirallen, Sasza Morwowiec, Strażniczka Alair, Talia Aegis, Teresa Mieralit, Tymon Grubosz

Streszczenie:

Dyrektor Arnulf Poważny ma wychowankę - Teresę Mieralit, noktiańską piętnastonastolatkę która mieszka w AMZ. Strażniczka Alair ją wykryła i wpadła w pętlę - komponenty TAI uznały ją za potencjalne zagrożenie, BIA za osobę do ochrony. Strażniczka wycofała drony do osłony AI Core i zaatakowały Teresę. Klaudia zorientowała się w problemie, obudziła hipernetem Arnulfa. Arnulf osłonił Teresę, Klaudia wezwała Talię i w grupie udało im się opanować niesforne TAI (bo Klaudia zanomalizowała konwertery energii Strażniczki, która czerpie energię z Trzęsawiska).

Aktor w Opowieści:

* Dokonanie:
    * 17 lat; jeszcze nie doszła do siebie (krzyczy w nocy) po akcji z serpentisami. Mimo, że się boi - chce iść chronić Klaudię przed niebezpieczeństwem. Ona i Felicjan zostali parą.
* Progresja:
    * została parą z Felicjanem Szarakiem po wydarzeniach z serpentisami w Lesie Trzęsawnym.


### Szukaj serpentisa w lesie

* **uid:** 211009-szukaj-serpentisa-w-lesie, _numer względny_: 3
* **daty:** 0084-11-13 - 0084-11-14
* **obecni:** Agostino Karwen, Dariusz Skórnik, Edelmira Neralis, Felicjan Szarak, Grzegorz Czerw, Klaudia Stryk, Ksenia Kirallen, Mariusz Trzewń, Strażniczka Alair, Udom Rapnak

Streszczenie:

Prosta operacja zdekomponowania medbunkra w Lesie Trzęsawnym (2 żołnierze, 4 uczniowie AMZ) skończyła się horrorem - serpentis noktiański porwał Trzewnia i 2 żołnierzy. Klaudia uruchomiła martwy medbunkier i przemyciła dronę do Zaczęstwa a Ksenia była dywersją (co skończyło się dla niej traumą). Mimo sytuacji, Ksenia przekonała serpentisów że jak się nie oddadzą w ręce astorian, jeden z nich umrze. I przekonała ich dzięki wiadomościom Klaudii świadczącym, że Talia współpracuje a nie jest w niewoli Astorii.

Aktor w Opowieści:

* Dokonanie:
    * już 17 lat (miała urodziny); MVP - zgłosiła się na ochotniczkę do przekonania serpentisa, nie dała się sterroryzować (mimo grozy) i przekonała serpentisów do oddania się w ręce Astorii. I ustabilizowała Edelmirę.
* Progresja:
    * trauma związana z zastraszeniem przez serpentisa noktiańskiego (Agostino). Ale była w stanie funkcjonować poprawnie, mimo, że następne pół roku miała koszmary i problemy ze spaniem / majaki.


### Nowa Strażniczka AMZ

* **uid:** 210926-nowa-strazniczka-amz, _numer względny_: 2
* **daty:** 0084-06-14 - 0084-06-26
* **obecni:** Albert Kalandryk, Arnulf Poważny, Klaudia Stryk, Ksenia Kirallen, Mariusz Trzewń, Strażniczka Alair, Talia Aegis, Tymon Grubosz

Streszczenie:

Klaudia - młoda grzeczna administratorka AMZ - przypadkiem odkrywa konspirację dyrektora, terminusa i noktianki by uratować umierającą Eszarę. Po konfrontacji z dyrektorem pomaga im by wszystko działało. Trzewń uniknął amnestyków.

Aktor w Opowieści:

* Dokonanie:
    * 16 lat; imprezowy bloodhound, pogodna iskierka; pojechała po Trzewniu (co wpadł bez pukania do pokoju dziewczyn), po czym pojechała do Zamku Weteranów zająć się tymi, którzy już nie mogą walczyć. Bardzo opiekuńcza i pogodna.


### Czółenkowe Esuriit w AMZ

* **uid:** 211122-czolenkowe-esuriit-w-amz, _numer względny_: 1
* **daty:** 0083-10-13 - 0083-10-22
* **obecni:** Dariusz Drewniak, Ernest Termann, Felicjan Szarak, Klaudia Stryk, Ksenia Kirallen, Mariusz Trzewń, Wiktor Szurmak

Streszczenie:

Wpierw Trzewń wziął Klaudię na randkę (ona tego nie wiedziała) do AI Core AMZ. Złapał ich Szurmak i ukarał. Potem były próby puryfikacji Czółenka przez magów Eterni i terminusów. Skończyło się katastrofą - Agent Esuriit dotarł nawet do AMZ, co doprowadziło do pokazania talentu organizacyjnego Klaudii i śmierci dyrektora Termanna który bronił uczniów. Skrzydło Loris AMZ zostało odcięte i uznane za zakazane. Przez ilość i typy energii przez MIESIĄC nie było zajęć - leczenie, regeneracja, puryfikacja.

Aktor w Opowieści:

* Dokonanie:
    * 16 lat; tyci młodsza od Klaudii. Piekielnie wręcz odważna. Urwisowata - miała szlaban, bo zgubiła się w Nieskończonym Labiryncie pod Skrzydłem Loris AMZ. Stworzyła z przyjaciółmi zaklęcie ukrywające nieobecność kogoś przed magią (przed dyrektorem). Oddała ten projekt grupie, by wspólnie opracować Beacon ściągający Agenta Esuriit do Labiryntu.
* Progresja:
    * w ogromnym szoku po tym ataku Esuriit. Dariusz Drewniak był jej ulubionym nauczycielem. Od tej pory odwiedza regularnie Dom Weteranów.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 13, @: 0111-08-20
    1. Primus    : 13, @: 0111-08-20
        1. Sektor Astoriański    : 13, @: 0111-08-20
            1. Astoria    : 13, @: 0111-08-20
                1. Sojusz Letejski    : 13, @: 0111-08-20
                    1. Szczeliniec    : 13, @: 0111-08-20
                        1. Powiat Pustogorski    : 12, @: 0111-08-20
                            1. Czarnopalec    : 1, @: 0111-08-01
                                1. Pusta Wieś    : 1, @: 0111-08-01
                            1. Czółenko    : 2, @: 0110-09-17
                                1. Bunkry    : 2, @: 0110-09-17
                                1. Pola Północne    : 1, @: 0110-09-17
                            1. Kramamcz    : 2, @: 0110-06-21
                                1. Włóknin    : 1, @: 0110-06-15
                            1. Majkłapiec    : 1, @: 0111-08-20
                                1. Farma Krecik    : 1, @: 0111-08-20
                                1. Kociarnia Zawtrak    : 1, @: 0111-08-20
                                1. Wegefarma Myriad    : 1, @: 0111-08-20
                                1. Zakład Paprykarski Majkłapiec    : 1, @: 0111-08-20
                            1. Podwiert    : 4, @: 0111-08-01
                                1. Dzielnica Luksusu Rekinów    : 1, @: 0111-08-01
                                    1. Serce Luksusu    : 1, @: 0111-08-01
                                        1. Lecznica Rannej Rybki    : 1, @: 0111-08-01
                                1. Osiedle Leszczynowe    : 1, @: 0110-09-17
                                    1. Sklep z reliktami Fantasmagoria    : 1, @: 0110-09-17
                            1. Pustogor    : 1, @: 0084-06-26
                                1. Zamek Weteranów    : 1, @: 0084-06-26
                            1. Zaczęstwo    : 8, @: 0111-08-01
                                1. Akademia Magii, kampus    : 6, @: 0111-08-01
                                    1. Akademik    : 6, @: 0111-08-01
                                    1. Arena Treningowa    : 1, @: 0083-10-22
                                    1. Budynek Centralny    : 2, @: 0084-12-12
                                        1. Piwnica    : 1, @: 0083-10-22
                                            1. Stare AI Core    : 1, @: 0083-10-22
                                        1. Skrzydło Loris    : 1, @: 0083-10-22
                                            1. Laboratorium Wysokich Energii    : 1, @: 0083-10-22
                                            1. Nieskończony Labirynt    : 1, @: 0083-10-22
                                    1. Domek dyrektora    : 1, @: 0084-12-24
                                    1. Złomiarium    : 2, @: 0084-12-15
                                1. Arena Migświatła    : 1, @: 0084-06-26
                                1. Las Trzęsawny    : 1, @: 0084-11-14
                                    1. Medbunkier Sigma    : 1, @: 0084-11-14
                                1. Nieużytki Staszka    : 1, @: 0084-06-26
                        1. Pustogor    : 1, @: 0110-06-27

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Klaudia Stryk        | 7 | ((210926-nowa-strazniczka-amz; 211009-szukaj-serpentisa-w-lesie; 211010-ukryta-wychowanka-arnulfa; 211017-nastolatka-w-bieliznie-na-dachu-w-burzy; 211019-czarodziejka-ktora-jednak-moze-sie-zabic; 211122-czolenkowe-esuriit-w-amz; 220119-sekret-samanty-arienik)) |
| Mariusz Trzewń       | 6 | ((190906-wypadek-w-kramamczu; 191103-kontrpolowanie-pieknotki-pulapka; 210926-nowa-strazniczka-amz; 211009-szukaj-serpentisa-w-lesie; 211019-czarodziejka-ktora-jednak-moze-sie-zabic; 211122-czolenkowe-esuriit-w-amz)) |
| Arnulf Poważny       | 4 | ((210926-nowa-strazniczka-amz; 211010-ukryta-wychowanka-arnulfa; 211017-nastolatka-w-bieliznie-na-dachu-w-burzy; 211019-czarodziejka-ktora-jednak-moze-sie-zabic)) |
| Felicjan Szarak      | 4 | ((211009-szukaj-serpentisa-w-lesie; 211010-ukryta-wychowanka-arnulfa; 211122-czolenkowe-esuriit-w-amz; 220119-sekret-samanty-arienik)) |
| Talia Aegis          | 4 | ((210926-nowa-strazniczka-amz; 211010-ukryta-wychowanka-arnulfa; 211019-czarodziejka-ktora-jednak-moze-sie-zabic; 220119-sekret-samanty-arienik)) |
| Teresa Mieralit      | 4 | ((211010-ukryta-wychowanka-arnulfa; 211017-nastolatka-w-bieliznie-na-dachu-w-burzy; 211019-czarodziejka-ktora-jednak-moze-sie-zabic; 211026-koszt-ratowania-torszeckiego)) |
| Strażniczka Alair    | 3 | ((210926-nowa-strazniczka-amz; 211009-szukaj-serpentisa-w-lesie; 211010-ukryta-wychowanka-arnulfa)) |
| Dariusz Kuromin      | 2 | ((190906-wypadek-w-kramamczu; 190912-rexpapier-i-wloknin)) |
| Kasjopea Maus        | 2 | ((190906-wypadek-w-kramamczu; 190912-rexpapier-i-wloknin)) |
| Paweł Szprotka       | 2 | ((211026-koszt-ratowania-torszeckiego; 211127-waśń-o-ryby-w-majklapcu)) |
| Pięknotka Diakon     | 2 | ((190906-wypadek-w-kramamczu; 191103-kontrpolowanie-pieknotki-pulapka)) |
| Rafał Torszecki      | 2 | ((211026-koszt-ratowania-torszeckiego; 211127-waśń-o-ryby-w-majklapcu)) |
| Sasza Morwowiec      | 2 | ((211010-ukryta-wychowanka-arnulfa; 211017-nastolatka-w-bieliznie-na-dachu-w-burzy)) |
| Tymon Grubosz        | 2 | ((210926-nowa-strazniczka-amz; 211010-ukryta-wychowanka-arnulfa)) |
| Agostino Karwen      | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Albert Kalandryk     | 1 | ((210926-nowa-strazniczka-amz)) |
| Anna Warlank         | 1 | ((190912-rexpapier-i-wloknin)) |
| Arazille             | 1 | ((220119-sekret-samanty-arienik)) |
| Arkadia Verlen       | 1 | ((211127-waśń-o-ryby-w-majklapcu)) |
| Błażej Arienik       | 1 | ((220119-sekret-samanty-arienik)) |
| Damian Orion         | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Daniel Terienak      | 1 | ((211127-waśń-o-ryby-w-majklapcu)) |
| Dariusz Drewniak     | 1 | ((211122-czolenkowe-esuriit-w-amz)) |
| Dariusz Skórnik      | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Diana Lauris         | 1 | ((200616-bardzo-straszna-mysz)) |
| Edelmira Neralis     | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Eleonora Rdeść       | 1 | ((190912-rexpapier-i-wloknin)) |
| Ernest Termann       | 1 | ((211122-czolenkowe-esuriit-w-amz)) |
| Franek Bulterier     | 1 | ((200616-bardzo-straszna-mysz)) |
| Gabriel Ursus        | 1 | ((200616-bardzo-straszna-mysz)) |
| Genowefa Krecik      | 1 | ((211127-waśń-o-ryby-w-majklapcu)) |
| Grzegorz Czerw       | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Henryk Wkrąż         | 1 | ((200616-bardzo-straszna-mysz)) |
| Ignacy Myrczek       | 1 | ((211026-koszt-ratowania-torszeckiego)) |
| Iwan Zawtrak         | 1 | ((211127-waśń-o-ryby-w-majklapcu)) |
| Jarek Gułanczak      | 1 | ((190912-rexpapier-i-wloknin)) |
| Józef Małmałaz       | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Karolina Terienak    | 1 | ((211127-waśń-o-ryby-w-majklapcu)) |
| Laura Tesinik        | 1 | ((200616-bardzo-straszna-mysz)) |
| Lilia Ursus          | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Marek Puszczok       | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Maria Arienik        | 1 | ((220119-sekret-samanty-arienik)) |
| Maryla Koternik      | 1 | ((211019-czarodziejka-ktora-jednak-moze-sie-zabic)) |
| Marysia Sowińska     | 1 | ((211026-koszt-ratowania-torszeckiego)) |
| Mateusz Urszank      | 1 | ((190912-rexpapier-i-wloknin)) |
| Matylda Sęk          | 1 | ((200616-bardzo-straszna-mysz)) |
| Minerwa Metalia      | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Olaf Zuchwały        | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Olga Myszeczka       | 1 | ((211026-koszt-ratowania-torszeckiego)) |
| Rafał Roszczeniok    | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Samanta Arienik      | 1 | ((220119-sekret-samanty-arienik)) |
| Sensacjusz Diakon    | 1 | ((211026-koszt-ratowania-torszeckiego)) |
| Sławomir Arienik     | 1 | ((220119-sekret-samanty-arienik)) |
| Stella Amakirin      | 1 | ((211127-waśń-o-ryby-w-majklapcu)) |
| Udom Rapnak          | 1 | ((211009-szukaj-serpentisa-w-lesie)) |
| Urszula Arienik      | 1 | ((220119-sekret-samanty-arienik)) |
| Waldemar Grzymość    | 1 | ((211017-nastolatka-w-bieliznie-na-dachu-w-burzy)) |
| Wiktor Satarail      | 1 | ((211026-koszt-ratowania-torszeckiego)) |
| Wiktor Szurmak       | 1 | ((211122-czolenkowe-esuriit-w-amz)) |
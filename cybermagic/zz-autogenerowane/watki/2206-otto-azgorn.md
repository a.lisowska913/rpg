# Otto Azgorn
## Identyfikator

Id: 2206-otto-azgorn

## Sekcja Opowieści

### Rzieza niszczy Infernię

* **uid:** 211027-rzieza-niszczy-infernie, _numer względny_: 7
* **daty:** 0112-01-22 - 0112-01-27
* **obecni:** Arianna Verlen, Elena Verlen, Flawia Blakenbauer, Klaudia Stryk, Martyn Hiwasser, Otto Azgorn, TAI Rzieza d'K1

Streszczenie:

Infernia wraca z Mevilig. Rzieza chce wyczyścić im pamięć o sobie. Arianna się opiera. Rzieza dowiaduje się o Ataienne. Arianna budzi Elenę po sympatii, Elena sprowadza Esuriit do laboratorium dekontaminacyjnego K1. Dużo śmierci. Martyn próbuje to opanować - budzi swoje simulacrum. Arianna i Klaudia opanowują przy pomocy Rziezy sytuację. 37% załogi Inferni nie żyje, Flawia wyssana w kosmos, Elena złamana, Martyn w szpitalu.

Aktor w Opowieści:

* Dokonanie:
    * dowodził wszystkimi jak byli opętani Esuriit. Nadal lojalny Ariannie. Zniszczył laboratorium i zabił wszystkich. Przetrwał walkę z simulacrum Martyna. Ostro pocięty.


### Kurczakownia

* **uid:** 211020-kurczakownia, _numer względny_: 6
* **daty:** 0112-01-18 - 0112-01-20
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Flawia Blakenbauer, Kamil Lyraczek, Klaudia Stryk, Martyn Hiwasser, OO Infernia, Otto Azgorn, Vigilus Mevilig

Streszczenie:

By uratować jak najwięcej osób w Sektorze Mevilig, Eustachy zaprojektował Kurczakownię - maszynę do oddzielania mechanicznego głów i rdzenia od reszty człowieka. Arianna starła się ze Zbawicielem-Niszczycielem jako aspekt Zbawiciela, wspierana przez Działo Rozpaczy (Eustachy). Udało im się sporo uratować (40% populacji Planetoidy Kalarfam), acz załoga Inferni będzie mieć koszmary senne. Klaudia zebrała dość danych, by zapobiec naturalnej manifestacji Zbawiciela-Niszczyciela w Sektorze Astoriańskim i zrobić kontrakcję jak co.

Aktor w Opowieści:

* Dokonanie:
    * strażnik Arianny Verlen, gdy ta w aspekcie Zbawiciela zstąpiła na planetoidę Kalarfam. Gdy Arianna była infekowana przez Zbawiciela-Niszczyciela, miotaczem plazmy wypalił to co pełzło po jej nodze, akceptując cierpienie przysmażanej Arianny.
* Progresja:
    * WSTRZĄŚNIĘTY. Kurczakowanie - rekurczakowanie. Jest to konieczne, ale zdecentrowało go mimo wszystko.


### Szara nawałnica

* **uid:** 211013-szara-nawalnica, _numer względny_: 5
* **daty:** 0112-01-12 - 0112-01-17
* **obecni:** Aleksandra Termia, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Martyn Hiwasser, Otto Azgorn, Remigiusz Błyszczyk, TAI Rzieza d'K1

Streszczenie:

Zespół przygotował się na wejście do Mevilig. Rzieza odkrył jak działają Piranie i że tamten sektor jest opanowany przez TAI 5+ generacji. Niestety, Infernia musiała uciekać przed Rziezą. Na miejscu Eustachy znalazł skuteczny sposób przemykania między Piraniami, dotarli do planetoidy Kalarfam gdzie znaleźli bazę ludzi. Mechaniczny "smok" został zniszczony przez Infernię, pobrali lokalsów i Arianna przekonała ich do zmiany kultu. Aha, anomalny wybuch torpedy anihilacyjnej Arianna przekierowała na planetę, wypowiadając ostrą wojnę TAI 5+ generacji...

Aktor w Opowieści:

* Dokonanie:
    * z Eleną i komandosami Verlenów przechwytywał lokalsów ze śmieciostateczku w Kalarfam na Infernię. Zdobył próbkę "metalowego smoka".


### Grupa Ekspedycyjna Kellert

* **uid:** 210929-grupa-ekspedycyjna-kellert, _numer względny_: 4
* **daty:** 0112-01-07 - 0112-01-10
* **obecni:** Adam Nerawol, Aleksandra Termia, Antoni Kramer, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Olena Orion, OO Infernia, OO Omega Septius, Otto Azgorn, TAI Marszałek Grzmotoszpon Trzeci, Vigilus Mevilig

Streszczenie:

Termia wysłała siły do sektora "Noviter", ale to był inny sektor (Mevilig). Straciła bezzałogowce, potem grupę ekspedycyjną Kellert. Kramer wysłał Infernię zakoloidowaną; okazało się, że ten sektor ma "strażnika Esuriit" zrobionego przez ludzi i grupę Piranii, sterowanych przez TAI 4 poziomu. Inferni udało się zebrać dane i wrócić z siedmioma uratowanymi członkami OO Savera, gdzie większość załogi została corruptowana przez Vigilusa...

Aktor w Opowieści:

* Dokonanie:
    * dowodził grupą szturmową mającą uratować Martyna od Vigilusa. Udało im się wyciągnąć Martyna i siedem osób, zdzierając im twarze.


### Morderstwo na Inferni

* **uid:** 210526-morderstwo-na-inferni, _numer względny_: 3
* **daty:** 0111-10-26 - 0111-11-01
* **obecni:** Antoni Kramer, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Feliks Walrond, Kamil Lyraczek, Karol Reichard, Klaudia Stryk, Leona Astrienko, Marian Tosen, Martyn Hiwasser, OO Tivr, Otto Azgorn, Tal Marczak

Streszczenie:

Noktiański mechanik Inferni, Tal Marczak, chciał zniszczyć OO Tivr (własność noktiańskiej rodziny Tala). Noktianie trzymają się razem - inni mechanicy Inferni doprowadzili do morderstwa Tala na K1 i wzięli amnestyki, by nikt nie wiedział co się stało. Zespół Inferni skutecznie doszedł do tego o co chodzi, odpowiedzialnych za morderstwo noktian przeniesiono na Żelazko a Arianna poważnie opieprzyła swoją noktiańską załogę. Będzie lepiej w przyszłości.

Aktor w Opowieści:

* Dokonanie:
    * sierżant paladynów Verlen; ma wytatuowany herb Verlenów na sercu. Zauważył i zgłosił Ariannie, że technik Inferni (Tal Marczak) zniknął. Zdezerterował? Arianna powiedziała, że się tym zajmie.


### Ewakuacja z Serenit

* **uid:** 210512-ewakuacja-z-serenit, _numer względny_: 2
* **daty:** 0111-09-25 - 0111-10-04
* **obecni:** Aida Serenit, AK Serenit, Arianna Verlen, Bogdan Anatael, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Martyn Hiwasser, OE Falołamacz, OO Infernia, Roland Sowiński

Streszczenie:

Klaudia spowolniła Serenit, po czym Elena weszła zdalnym Entropikiem na pokład Falołamacza. Arianna zmanipulowała ludzi by dało się ich uratować. Samobójcza pintka zniechęciła Falołamacz do dalszej walki. Nie wszystkich udało się uratować (ale kapitana i arystokratę Sowińskich tak). Niestety, Infernia skończyła jako kosmiczny wrak i musiała wysłać SOS...

Aktor w Opowieści:

* Dokonanie:
    * AKTOR NIEOBECNY NA SESJI
* Progresja:
    * zniesmaczony kultem Arianny na Inferni. Tien Verlen na to pozwala?! WSPIERA to?!


### Znudzona załoga Inferni

* **uid:** 210421-znudzona-zaloga-inferni, _numer względny_: 1
* **daty:** 0111-09-18 - 0111-09-21
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Jolanta Sowińska, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Morrigan d'Tirakal, Otto Azgorn, Persefona d'Infernia, Tomasz Sowiński

Streszczenie:

Po raz pierwszy od dawna Infernia jest w stanie rozprzężenia - wracają z daleka po trudnej misji i nic im nie grozi. Jako, że załoga ma straszne tarcia (Leona poluje na "słabe jednostki", komandosi Verlenów chcą zatrzymać Leonę, noktianie vs astorianie...), Arianna autoryzuje Eustachego do zrobienia "Tirakala" jako niebezpieczną sytuację. Eustachy reanimuje Tirakala, ale niestety cholerstwo zanomalizowało - przywrócił Morrigan. By Zespół nie rozwiązał Tirakala za szybko, Eustachy trochę sabotował Infernię co Elena przeczytała jako... umówienie się na randkę. Gdy Klaudia wykryła, że na INFERNI pojawiła się straszna anomalia (Morrigan) próbowali to usunąć - ale Eustachy przecież sabotował Infernię! Udało im się opanować Morrigan, ale kosztem uszkodzeń i obrażeń na Inferni. I scenek kompromitujących Jolantę Sowińską. Infernia jest uszkodzona, ale się trzyma.

Aktor w Opowieści:

* Dokonanie:
    * sierżant paladynów Verlen; ostrzegł Ariannę o tym jak źle sytuacja z morale na Inferni wygląda; zwłaszcza z Leoną.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 6, @: 0112-01-27
    1. Primus    : 6, @: 0112-01-27
        1. Sektor Astoriański    : 5, @: 0112-01-27
            1. Astoria, Orbita    : 3, @: 0112-01-27
                1. Kontroler Pierwszy    : 3, @: 0112-01-27
                    1. Laboratoria Dekontaminacyjne    : 1, @: 0112-01-27
            1. Brama Kariańska    : 2, @: 0112-01-17
            1. Stocznia Kariańska    : 1, @: 0112-01-17
        1. Sektor Mevilig    : 3, @: 0112-01-20
            1. Chmura Piranii    : 2, @: 0112-01-17
            1. Keratlia    : 1, @: 0112-01-17
            1. Planetoida Kalarfam    : 2, @: 0112-01-20
        1. Sektor Noviter    : 1, @: 0112-01-10

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 6 | ((210421-znudzona-zaloga-inferni; 210526-morderstwo-na-inferni; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica; 211020-kurczakownia; 211027-rzieza-niszczy-infernie)) |
| Elena Verlen         | 6 | ((210421-znudzona-zaloga-inferni; 210526-morderstwo-na-inferni; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica; 211020-kurczakownia; 211027-rzieza-niszczy-infernie)) |
| Klaudia Stryk        | 6 | ((210421-znudzona-zaloga-inferni; 210526-morderstwo-na-inferni; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica; 211020-kurczakownia; 211027-rzieza-niszczy-infernie)) |
| Martyn Hiwasser      | 6 | ((210421-znudzona-zaloga-inferni; 210526-morderstwo-na-inferni; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica; 211020-kurczakownia; 211027-rzieza-niszczy-infernie)) |
| Eustachy Korkoran    | 5 | ((210421-znudzona-zaloga-inferni; 210526-morderstwo-na-inferni; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica; 211020-kurczakownia)) |
| Leona Astrienko      | 3 | ((210421-znudzona-zaloga-inferni; 210526-morderstwo-na-inferni; 210929-grupa-ekspedycyjna-kellert)) |
| Aleksandra Termia    | 2 | ((210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica)) |
| Antoni Kramer        | 2 | ((210526-morderstwo-na-inferni; 210929-grupa-ekspedycyjna-kellert)) |
| Flawia Blakenbauer   | 2 | ((211020-kurczakownia; 211027-rzieza-niszczy-infernie)) |
| Kamil Lyraczek       | 2 | ((210526-morderstwo-na-inferni; 211020-kurczakownia)) |
| OO Infernia          | 2 | ((210929-grupa-ekspedycyjna-kellert; 211020-kurczakownia)) |
| TAI Rzieza d'K1      | 2 | ((211013-szara-nawalnica; 211027-rzieza-niszczy-infernie)) |
| Vigilus Mevilig      | 2 | ((210929-grupa-ekspedycyjna-kellert; 211020-kurczakownia)) |
| Adam Nerawol         | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| Feliks Walrond       | 1 | ((210526-morderstwo-na-inferni)) |
| Jolanta Sowińska     | 1 | ((210421-znudzona-zaloga-inferni)) |
| Karol Reichard       | 1 | ((210526-morderstwo-na-inferni)) |
| Marian Tosen         | 1 | ((210526-morderstwo-na-inferni)) |
| Morrigan d'Tirakal   | 1 | ((210421-znudzona-zaloga-inferni)) |
| Olena Orion          | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| OO Omega Septius     | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| OO Tivr              | 1 | ((210526-morderstwo-na-inferni)) |
| Persefona d'Infernia | 1 | ((210421-znudzona-zaloga-inferni)) |
| Remigiusz Błyszczyk  | 1 | ((211013-szara-nawalnica)) |
| TAI Marszałek Grzmotoszpon Trzeci | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| Tal Marczak          | 1 | ((210526-morderstwo-na-inferni)) |
| Tomasz Sowiński      | 1 | ((210421-znudzona-zaloga-inferni)) |
# Antoni Kramer
## Identyfikator

Id: 2007-antoni-kramer

## Sekcja Opowieści

### Grupa Ekspedycyjna Kellert

* **uid:** 210929-grupa-ekspedycyjna-kellert, _numer względny_: 13
* **daty:** 0112-01-07 - 0112-01-10
* **obecni:** Adam Nerawol, Aleksandra Termia, Antoni Kramer, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Olena Orion, OO Infernia, OO Omega Septius, Otto Azgorn, TAI Marszałek Grzmotoszpon Trzeci, Vigilus Mevilig

Streszczenie:

Termia wysłała siły do sektora "Noviter", ale to był inny sektor (Mevilig). Straciła bezzałogowce, potem grupę ekspedycyjną Kellert. Kramer wysłał Infernię zakoloidowaną; okazało się, że ten sektor ma "strażnika Esuriit" zrobionego przez ludzi i grupę Piranii, sterowanych przez TAI 4 poziomu. Inferni udało się zebrać dane i wrócić z siedmioma uratowanymi członkami OO Savera, gdzie większość załogi została corruptowana przez Vigilusa...

Aktor w Opowieści:

* Dokonanie:
    * nie zgadza się na to, że Termia nie ma jak uratować 6 statków i 200 osób. Zakoloidował Infernię, autoryzował sprzęt i wysłał Infernię przez Bramę.


### Ostatnia akcja bohaterki

* **uid:** 210922-ostatnia-akcja-bohaterki, _numer względny_: 12
* **daty:** 0111-12-19 - 0112-01-03
* **obecni:** Antoni Kramer, Arianna Verlen, Elena Verlen, Izabela Zarantel, Jolanta Kopiec, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Olgierd Drongon, OO Żelazko, Roland Sowiński

Streszczenie:

Z Martynem skontaktowała się tien Kopiec prosząc o ostatnią misję zanim Esuriit ją pożre. Martyn zorganizował lot Infernią. Ustabilizował Jolantę, by ta dała radę dolecieć słowami i opowieściami, przy okazji tworząc piękną opowieść o Eterni i eulogię dla Jolanty - bohaterki wojennej - w Sekretach Orbitera (przy okazji wyszła jego przeszłość). Infernia zniszczyła Krwawą Bazę Piratów kosztem dewastacji Żelazka. Jolanta zniszczyła krwawego maga. Sektor jest bezpieczniejszy, Eternia szczęśliwa a Arianna ma profity.

Aktor w Opowieści:

* Dokonanie:
    * pod wpływem raportu Klaudii i prośby Arianny dał się przekonać do autoryzowania Inferni do zniszczenia Krwawej Bazy Piratów.


### Siostrzenica Morlana

* **uid:** 210818-siostrzenica-morlana, _numer względny_: 11
* **daty:** 0111-11-15 - 0111-11-19
* **obecni:** Antoni Kramer, Arianna Verlen, Eustachy Korkoran, Klaudia Stryk, Leona Astrienko, Maria Naavas, Nataniel Morlan, Ofelia Morlan, Olgierd Drongon, OO Netrahina, OO Żelazko, SC Fecundatis, SC Światłodóbr, Tomasz Sowiński

Streszczenie:

Tomasz Sowiński próbuje uratować Ofelię Morlan przed Natanielem Morlanem. Nie ma kogo poprosić a Ofelia służy na Netrahinie; poprosił więc Ariannę. Arianna skanując echo pamięci Jolanty w Inferni zdobywa informacje o programie kosmicznym Orbitera, przekazuje to Kramerowi, wykorzystuje wsparcie Olgierda i robią ćwiczenia Żelazko - Netrahina. Ratują Ofelię przed porwaniem przez koloidowy statek Kruków Kasandry.

Aktor w Opowieści:

* Dokonanie:
    * dostał info o planie na program kosmiczny Jolanty Sowińskiej. Zaaranżował ćwiczenia Netrahina - Żelazko z dobrego serca i chęci pomocy Ofelii.


### Morderstwo na Inferni

* **uid:** 210526-morderstwo-na-inferni, _numer względny_: 10
* **daty:** 0111-10-26 - 0111-11-01
* **obecni:** Antoni Kramer, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Feliks Walrond, Kamil Lyraczek, Karol Reichard, Klaudia Stryk, Leona Astrienko, Marian Tosen, Martyn Hiwasser, OO Tivr, Otto Azgorn, Tal Marczak

Streszczenie:

Noktiański mechanik Inferni, Tal Marczak, chciał zniszczyć OO Tivr (własność noktiańskiej rodziny Tala). Noktianie trzymają się razem - inni mechanicy Inferni doprowadzili do morderstwa Tala na K1 i wzięli amnestyki, by nikt nie wiedział co się stało. Zespół Inferni skutecznie doszedł do tego o co chodzi, odpowiedzialnych za morderstwo noktian przeniesiono na Żelazko a Arianna poważnie opieprzyła swoją noktiańską załogę. Będzie lepiej w przyszłości.

Aktor w Opowieści:

* Dokonanie:
    * zatrzymał ćwiczenia komodora Walronda, by nie wyszło że noktianie zabili swojego na K1. Potem przeniósł winnych noktian na Żelazko dla Arianny - by nie ucierpieli za mocno a kara by była.


### Porwanie cywila z Kokitii

* **uid:** 200429-porwanie-cywila-z-kokitii, _numer względny_: 9
* **daty:** 0111-08-03 - 0111-08-08
* **obecni:** Alara Ehmes, Antoni Kramer, Arianna Verlen, Eustachy Korkoran, Klaudia Stryk, Leona Astrienko, Medea Sowińska

Streszczenie:

Infernia jakoś nie może zostać naprawiona a skarga Mateusza Sowińskiego nie pomaga. Po śledztwie Zespołu okazuje się, że Infernia jest kanibalizowana przez Niobe, do jakiejś misji sił specjalnych. Arianna z ekipą wkręciła się na tą misję, przekonując Medeę Sowińską, że bez niej to się nie uda. Misja polegała na ataku na Kokitię pomiędzy Bramami i ekstrakcja z Kokitii cywila, udając piratów. Dzięki załodze Inferni udało się to lepiej niż ktokolwiek mógłby się spodziewać i niż Medea zaplanowała sama.

Aktor w Opowieści:

* Dokonanie:
    * admirał Orbitera i zwolennik Arianny; powiedział jej parę rzeczy o Medei, m.in. jak stała się tylko kapitanem w wyniku wypadku.


### Infernia jako Goldarion

* **uid:** 210218-infernia-jako-goldarion, _numer względny_: 8
* **daty:** 0111-07-19 - 0111-08-03
* **obecni:** Antoni Kramer, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Jolanta Sowińska, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, OO Infernia, Tomasz Sowiński

Streszczenie:

Infernia udaje Goldariona, by przetransportować tajną przesyłkę Sowińskich do Jolanty Sowińskiej na planetoidzie Asimear. Tomasz Sowiński na Inferni okazuje się być dużym dzieckiem, łatwo jest wmanipulowany w otwarcie paczki - jest tam nietypowy antynanitkowy Entropik (którego psuje Elena przez błędną integrację). W tle wszystkiego - próba zdobycia niezależnej od Orbitera floty przez Aurum i jeden znikający advancer Kramera na Asimear...

Aktor w Opowieści:

* Dokonanie:
    * stracił advancera na Asimear i wysłał Infernię, by go znaleźli. Plus cośtam z Sowińskimi i potencjałem na flotę Aurum.


### Infernia jest nasza!

* **uid:** 210804-infernia-jest-nasza, _numer względny_: 7
* **daty:** 0111-04-23 - 0111-04-26
* **obecni:** Aleksandra Termia, Antoni Kramer, Arianna Verlen, Artur Traffal, Diana d'Infernia, Eustachy Korkoran, Franciszek Maszkiet, Klaudia Stryk, Leona Astrienko, Maciej Żarand, OO Samotność Gwiazd

Streszczenie:

Ku wielkiemu niezadowoleniu Zespołu, Infernia miała im być odebrana i złomowana. Mieli dostać nowy tańszy statek. Zespół odkrył że za tym stoi komodor Traffal z sił Termii który przy użyciu Inferni chciał złapać w potrzask arystokratę eternijskiego który był kultystą magii. Zespół odzyskał Infernię, przesunął kultystę w kierunku na Ariannę i uciemiężył komodora Traffala - będzie miał pecha na K1.

Aktor w Opowieści:

* Dokonanie:
    * ucieszył się, że dla odmiany Arianna przyszła do niego rozwiązać problem "złomowania Inferni" zamiast robić własne plany (nie przyszła). Odwrócił rozkaz złomowania i oddał Ariannie Infernię.


### Nienawiść do świń

* **uid:** 200826-nienawisc-do-swin, _numer względny_: 6
* **daty:** 0110-12-08 - 0110-12-14
* **obecni:** Aleksandra Termia, Antoni Kramer, Arianna Verlen, Eustachy Korkoran, Kamil Lyraczek, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Sebastian Alarius, Tadeusz Ursus

Streszczenie:

Po pojedynku Kramer opieprzył Ariannę i dał jej dyskretną tajną misję - ma niby w niesławie wozić świnie, naprawdę ma pomóc Orbiterowi rozwiązać spisek w Trzecim Raju. Ale wysłanie Arianny na świnie wywołało bunt na Kontrolerze Pierwszym; Arianna musiała rozpaczliwie deeskalować, nawet budząc Leonę. Udało się bunt opanować, acz wysokim kosztem - pierwszy post-noktiański kapitan nie żyje, załoga Inferni częściowo zesłana będzie do Trzeciego Raju a Arianna miała bardzo żenujące wejście.

Aktor w Opowieści:

* Dokonanie:
    * dał Ariannie tajną misję w Trzecim Raju i ukrył to za karą do wożenia świń; strasznie zapłacił gdy część Orbitera się zbuntowała.
* Progresja:
    * straszny cios w reputację i zasoby; bunt spowodowany przez załogę Inferni uderzył w jego silną stronę i pokazał, że Kramer nic nie kontroluje.


### Sekrety Orbitera - historia prawdziwa

* **uid:** 200819-sekrety-orbitera-historia-prawdziwa, _numer względny_: 5
* **daty:** 0110-11-26 - 0110-12-04
* **obecni:** Arianna Verlen, Damian Orion, Elena Verlen, Eustachy Korkoran, Izabela Zarantel, Klaudia Stryk, Leona Astrienko, Leszek Kurzmin, Martyn Hiwasser, Sabina Servatel, Tadeusz Ursus

Streszczenie:

Elena wyzwała Leonę na pojedynek. Klaudia i Arianna wkręciły Tadeusza. Tadeusz - arcymag Eterni - używa Simulacrum w walce, więc Zespół zaczął montować plan jak Leona może jednak wygrać z Simulacrum. I wszystko skończyło się tym, że Leona wygrała, Elena jest wolna od Tadeusza i powstała holodrama "Sekrety Orbitera"...

Aktor w Opowieści:

* Dokonanie:
    * AKTOR NIEOBECNY NA SESJI
* Progresja:
    * z uwagi na działania Arianny i Klaudii odnośnie pojedynku Leona - Tadeusz, duże zniszczenia a zwłaszcza holodramę 'Sekrety Orbitera' ma solidny opiernicz.


### Nocna Krypta i Emulatorka

* **uid:** 200729-nocna-krypta-i-emulatorka, _numer względny_: 4
* **daty:** 0110-11-16 - 0110-11-22
* **obecni:** AK Nocna Krypta, Antoni Kramer, Arianna Verlen, Damian Orion, Eustachy Korkoran, Kijara d'Esuriit, Klaudia Stryk, Laura Orion, Leona Astrienko, Mirela Orion, OO Castigator, OO Minerwa

Streszczenie:

Laura, Emulatorka na Nocnej Krypcie została Skażona przez Kijarę. Laura opanowana wizjami Esuriit chciała zniszczyć Eternię. Nie dało się jej powstrzymać - Mirela, Zespół - byli za słabi. Arianna wezwała Kryptę pod ogniem Castigatora i wypaliła Kryptę i Kijarę do zera, uszkadzając "zdrowie psychiczne Krypty". Zginęło kilkudziesięciu ludzi, ale Arianna dostała podziękowanie od Admiralicji. Damian Orion jest rozczarowany.

Aktor w Opowieści:

* Dokonanie:
    * jak tylko admirał dowiedział się o powadze sytuacji, stanął po stronie Arianny i sprawił, by Castigator mógł zniszczyć Kryptę. Dostał podziękowania z Admiralicji i upewnił się, że Arianna też je dostanie.
* Progresja:
    * dostaje szacun za ochronę Sektora Astoriańskiego przed Kijarą d'Esuriit i za zniszczenie straszliwej Anomalii.


### Wielki Kosmiczny Romans

* **uid:** 200722-wielki-kosmiczny-romans, _numer względny_: 3
* **daty:** 0110-11-12 - 0110-11-15
* **obecni:** Antoni Kramer, Arianna Verlen, Damian Orion, Elena Verlen, Eustachy Korkoran, Julian Muszel, Konrad Wolczątek, Leona Astrienko, Olgierd Drongon, OO Welgat, OO Żelazko, Tadeusz Ursus

Streszczenie:

Pojawiła się plotka, że Arianna, Elena i Eustachy mają romans. Plotka pochodzi od Leony, ale ktoś ją wykorzystał. Rozwiązując plotkę Zespół doszedł do tego, że Elena ma niestabilną energię magiczną i na niektórych upiornie mocno działa, poznali też elementy przeszłości Eleny. Po wyplątaniu Eleny z jednego obsesyjnego adoratora (i wpakowanie plotki że Olgierd z Żelazka aspiruje do jej ręki), Arianna zdecydowała się z Eustachym, Klaudią i Martynem pomóc Kirasjerom w uratowaniu zaginionej Emulatorki z Nocnej Krypty...

Aktor w Opowieści:

* Dokonanie:
    * admirał, który opieprzył Ariannę za dewastowanie korwet (wina Eleny) i za romanse (wina Leony). Dał Ariannie zadanie naprawy tego. Nadal silnie stoi za Arianną mimo nacisków.


### Problematyczna Elena

* **uid:** 200708-problematyczna-elena, _numer względny_: 2
* **daty:** 0110-10-29 - 0110-11-03
* **obecni:** AK Salamin, Antoni Kramer, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Leszek Kurzmin, Olgierd Drongon, OO Aurelion, OO Żelazko, Persefona d'Infernia

Streszczenie:

Na Infernię trafiła nowa podoficer - podporucznik Elena Verlen, z Castigatora. W konkursie z Eustachym prawie zniszczyli Infernię. Niedługo potem Infernia (podłatana) poleciała do Anomalii Kolapsu uratować Aurelion, który natknął się na anomaliczny kiedyś-krążownik Orbitera Salamin. Arianna uratowała kogo się da i skłoniła Salamin do samozniszczenia; Persefona d'Salamin miała jeszcze dość psychotronicznej świadomości i miłości (?) do swojej martwej kapitan.

Aktor w Opowieści:

* Dokonanie:
    * admirał konsekwentnie osłaniający Ariannę; wysłał Ariannę na misję ratowania Aureliona przed Salaminem (o czym nie wiedział).


### Kapitan Verlen i koniec przygody na Królowej

* **uid:** 221026-kapitan-verlen-i-koniec-przygody-na-krolowej, _numer względny_: 1
* **daty:** 0100-04-10 - 0100-04-17
* **obecni:** Antoni Kramer, Arianna Verlen, Arnulf Perikas, Daria Czarnewik, Erwin Pies, Klaudiusz Terienak, Leszek Kurzmin, OO Królowa Kosmicznej Chwały, OO Tucznik Trzeci, Stefan Torkil, Tomasz Ruppok

Streszczenie:

Po długiej i ciężkiej pracy Arianna i Daria doprowadziły Królową do 50% sprawności nominalnej. Radość i wysokie morale załogi. Gdy Arnulf przyszedł do Arianny mówiąc o fabrykacji narkotyków i alkoholu, ona kazała to 'zatruć'. Zmotywowała Ruppoka, by dał jej szansę i znalazła sposób jak częściowo odzyskać materiały do fabrykatora po rozmowie z Arnulfem - sojusz z bliską Anomalii Kolapsu stacją Nonarion; poprosiła Kurzmina by to załatwił. TAK PRZESTRASZYŁA siły specjalne Orbitera, że Ariannę i załogę przenieśli bliżej Nonariona by sabotować program kosmiczny Aurum. Koniec przygody na Królowej :-).

Aktor w Opowieści:

* Dokonanie:
    * nowo wschodzący admirał Orbitera; zainteresował się Arianną Verlen widząc jej skuteczność i powodzenie. To osłoniło ją przed działaniami Sił Specjalnych.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 12, @: 0112-01-10
    1. Nierzeczywistość    : 1, @: 0111-08-08
    1. Primus    : 12, @: 0112-01-10
        1. Sektor Astoriański    : 12, @: 0112-01-10
            1. Astoria, Orbita    : 8, @: 0111-11-19
                1. Kontroler Pierwszy    : 8, @: 0111-11-19
                    1. Hangary Alicantis    : 2, @: 0110-11-15
                    1. Sektor 57    : 1, @: 0111-11-19
                        1. Mordownia    : 1, @: 0111-11-19
            1. Brama Kariańska    : 2, @: 0112-01-10
            1. Brama Trzypływów    : 1, @: 0111-08-08
            1. Krwawa Baza Piracka    : 1, @: 0112-01-03
            1. Obłok Lirański    : 2, @: 0110-11-22
                1. Anomalia Kolapsu    : 1, @: 0110-11-03
            1. Pas Teliriański    : 1, @: 0111-08-03
                1. Planetoidy Kazimierza    : 1, @: 0111-08-03
                    1. Planetoida Asimear    : 1, @: 0111-08-03
        1. Sektor Lacarin    : 1, @: 0110-11-22
        1. Sektor Mevilig    : 1, @: 0112-01-10
            1. Chmura Piranii    : 1, @: 0112-01-10
        1. Sektor Noviter    : 1, @: 0112-01-10

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 12 | ((200429-porwanie-cywila-z-kokitii; 200708-problematyczna-elena; 200722-wielki-kosmiczny-romans; 200729-nocna-krypta-i-emulatorka; 200826-nienawisc-do-swin; 210218-infernia-jako-goldarion; 210526-morderstwo-na-inferni; 210804-infernia-jest-nasza; 210818-siostrzenica-morlana; 210922-ostatnia-akcja-bohaterki; 210929-grupa-ekspedycyjna-kellert; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Eustachy Korkoran    | 10 | ((200429-porwanie-cywila-z-kokitii; 200708-problematyczna-elena; 200722-wielki-kosmiczny-romans; 200729-nocna-krypta-i-emulatorka; 200826-nienawisc-do-swin; 210218-infernia-jako-goldarion; 210526-morderstwo-na-inferni; 210804-infernia-jest-nasza; 210818-siostrzenica-morlana; 210929-grupa-ekspedycyjna-kellert)) |
| Klaudia Stryk        | 10 | ((200429-porwanie-cywila-z-kokitii; 200708-problematyczna-elena; 200729-nocna-krypta-i-emulatorka; 200826-nienawisc-do-swin; 210218-infernia-jako-goldarion; 210526-morderstwo-na-inferni; 210804-infernia-jest-nasza; 210818-siostrzenica-morlana; 210922-ostatnia-akcja-bohaterki; 210929-grupa-ekspedycyjna-kellert)) |
| Leona Astrienko      | 10 | ((200429-porwanie-cywila-z-kokitii; 200722-wielki-kosmiczny-romans; 200729-nocna-krypta-i-emulatorka; 200826-nienawisc-do-swin; 210218-infernia-jako-goldarion; 210526-morderstwo-na-inferni; 210804-infernia-jest-nasza; 210818-siostrzenica-morlana; 210922-ostatnia-akcja-bohaterki; 210929-grupa-ekspedycyjna-kellert)) |
| Elena Verlen         | 6 | ((200708-problematyczna-elena; 200722-wielki-kosmiczny-romans; 210218-infernia-jako-goldarion; 210526-morderstwo-na-inferni; 210922-ostatnia-akcja-bohaterki; 210929-grupa-ekspedycyjna-kellert)) |
| Martyn Hiwasser      | 5 | ((200826-nienawisc-do-swin; 210218-infernia-jako-goldarion; 210526-morderstwo-na-inferni; 210922-ostatnia-akcja-bohaterki; 210929-grupa-ekspedycyjna-kellert)) |
| Olgierd Drongon      | 4 | ((200708-problematyczna-elena; 200722-wielki-kosmiczny-romans; 210818-siostrzenica-morlana; 210922-ostatnia-akcja-bohaterki)) |
| OO Żelazko           | 4 | ((200708-problematyczna-elena; 200722-wielki-kosmiczny-romans; 210818-siostrzenica-morlana; 210922-ostatnia-akcja-bohaterki)) |
| Aleksandra Termia    | 3 | ((200826-nienawisc-do-swin; 210804-infernia-jest-nasza; 210929-grupa-ekspedycyjna-kellert)) |
| Damian Orion         | 2 | ((200722-wielki-kosmiczny-romans; 200729-nocna-krypta-i-emulatorka)) |
| Kamil Lyraczek       | 2 | ((200826-nienawisc-do-swin; 210526-morderstwo-na-inferni)) |
| Leszek Kurzmin       | 2 | ((200708-problematyczna-elena; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| OO Infernia          | 2 | ((210218-infernia-jako-goldarion; 210929-grupa-ekspedycyjna-kellert)) |
| Otto Azgorn          | 2 | ((210526-morderstwo-na-inferni; 210929-grupa-ekspedycyjna-kellert)) |
| Tadeusz Ursus        | 2 | ((200722-wielki-kosmiczny-romans; 200826-nienawisc-do-swin)) |
| Tomasz Sowiński      | 2 | ((210218-infernia-jako-goldarion; 210818-siostrzenica-morlana)) |
| Adam Nerawol         | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| AK Nocna Krypta      | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| AK Salamin           | 1 | ((200708-problematyczna-elena)) |
| Alara Ehmes          | 1 | ((200429-porwanie-cywila-z-kokitii)) |
| Arnulf Perikas       | 1 | ((221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Artur Traffal        | 1 | ((210804-infernia-jest-nasza)) |
| Daria Czarnewik      | 1 | ((221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Diana d'Infernia     | 1 | ((210804-infernia-jest-nasza)) |
| Erwin Pies           | 1 | ((221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Feliks Walrond       | 1 | ((210526-morderstwo-na-inferni)) |
| Franciszek Maszkiet  | 1 | ((210804-infernia-jest-nasza)) |
| Izabela Zarantel     | 1 | ((210922-ostatnia-akcja-bohaterki)) |
| Jolanta Kopiec       | 1 | ((210922-ostatnia-akcja-bohaterki)) |
| Jolanta Sowińska     | 1 | ((210218-infernia-jako-goldarion)) |
| Julian Muszel        | 1 | ((200722-wielki-kosmiczny-romans)) |
| Karol Reichard       | 1 | ((210526-morderstwo-na-inferni)) |
| Kijara d'Esuriit     | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Klaudiusz Terienak   | 1 | ((221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Konrad Wolczątek     | 1 | ((200722-wielki-kosmiczny-romans)) |
| Laura Orion          | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Maciej Żarand        | 1 | ((210804-infernia-jest-nasza)) |
| Maria Naavas         | 1 | ((210818-siostrzenica-morlana)) |
| Marian Tosen         | 1 | ((210526-morderstwo-na-inferni)) |
| Medea Sowińska       | 1 | ((200429-porwanie-cywila-z-kokitii)) |
| Mirela Orion         | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Nataniel Morlan      | 1 | ((210818-siostrzenica-morlana)) |
| Ofelia Morlan        | 1 | ((210818-siostrzenica-morlana)) |
| Olena Orion          | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| OO Aurelion          | 1 | ((200708-problematyczna-elena)) |
| OO Castigator        | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| OO Królowa Kosmicznej Chwały | 1 | ((221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| OO Minerwa           | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| OO Netrahina         | 1 | ((210818-siostrzenica-morlana)) |
| OO Omega Septius     | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| OO Samotność Gwiazd  | 1 | ((210804-infernia-jest-nasza)) |
| OO Tivr              | 1 | ((210526-morderstwo-na-inferni)) |
| OO Tucznik Trzeci    | 1 | ((221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| OO Welgat            | 1 | ((200722-wielki-kosmiczny-romans)) |
| Persefona d'Infernia | 1 | ((200708-problematyczna-elena)) |
| Roland Sowiński      | 1 | ((210922-ostatnia-akcja-bohaterki)) |
| SC Fecundatis        | 1 | ((210818-siostrzenica-morlana)) |
| SC Światłodóbr       | 1 | ((210818-siostrzenica-morlana)) |
| Sebastian Alarius    | 1 | ((200826-nienawisc-do-swin)) |
| Stefan Torkil        | 1 | ((221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| TAI Marszałek Grzmotoszpon Trzeci | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| Tal Marczak          | 1 | ((210526-morderstwo-na-inferni)) |
| Tomasz Ruppok        | 1 | ((221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Vigilus Mevilig      | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
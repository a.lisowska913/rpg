# Annika Pradis
## Identyfikator

Id: 9999-annika-pradis

## Sekcja Opowieści

### Upadek Eleny

* **uid:** 220309-upadek-eleny, _numer względny_: 5
* **daty:** 0112-04-03 - 0112-04-09
* **obecni:** Annika Pradis, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Tomasz Kaltaben

Streszczenie:

Keldan Voss została ustabilizowana. Annika będzie mieć szansę powodzenia - zostaje przełożoną, z łącznikiem z keldanitami Mateusem. Odbudowany jej link z pallidanami. Pallidanie mają ogólnie "spokój". Tomasz Kaltaben będzie ją wspierał a on sam dostanie 3 magów Orbitera co go wspierają (i są agentami). Elena niestety zabrała trzy życia odzyskując naturalną urodę. Klaudia ciężko pracuje by zbudować dla Eleny Detox Chamber - ona nie jest OK.

Aktor w Opowieści:

* Dokonanie:
    * jednak dowodzi Keldan Voss, współpracując z keldanitami i pallidanami. Nie dała rady zainspirować nikogo, ale dobrze żyje ze wszystkimi - dobra administratorka, fatalna liderka :D.
* Progresja:
    * spróbuje być administratorką i liderką. Jest tyle winna swoim ludziom. Eustachy ją zainspirował.
    * ma jeszcze jedną szansę wśród lokalsów i zaufanie, acz ludzie próbują uciec z Keldan Voss. W końcu załatwia sprawy.


### Stabilizacja Keldan Voss

* **uid:** 220223-stabilizacja-keldan-voss, _numer względny_: 4
* **daty:** 0112-03-30 - 0112-04-02
* **obecni:** Annika Pradis, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, SP Światło Nadziei, Szczepan Kaltaben, Tomasz Kaltaben

Streszczenie:

Ewakuacja pallidan na Keldan Voss się udało, acz Eustachy musiał zniszczyć anomalnych napastników z Mgły i naprawić morale Anniki. Elenie (słyszącej szepty) udało się zniszczyć BIA i wprowadzić na jej miejsce amalgamat szamana. Arianna ma polityczną kontrolę nad sytuacją a dzięki Eustachemu udało się sytuację ustabilizować militarnie. Dzięki Klaudii wiedzą co i jak. Teraz już tylko zostaje zostawić Keldan Voss w stabilnej formie.

Aktor w Opowieści:

* Dokonanie:
    * próbuje trzymać wszystkim morale, ale nie wychodzi. Złamane morale. Po killzone Eustachego podniosła morale ludzi... ale kosztem swojej reputacji.
* Progresja:
    * ogólna opinia keldanitów i pallidan z Keldan Voss - nic z niej nie będzie. Kiepski dowódca. Naiwna, dobre serce, ale niewiele zdziała.


### Polityka rujnuje Pallidę Voss

* **uid:** 220216-polityka-rujnuje-pallide-voss, _numer względny_: 3
* **daty:** 0112-03-27 - 0112-03-29
* **obecni:** Annika Pradis, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Mateus Sarpon, Raoul Lavanis, SP Pallida Voss, Szczepan Kaltaben

Streszczenie:

Arianna we współpracy z Mateusem planują uratować zakładników pallidańskich od radykałów keldańskich. Ale by to osiągnąć Eustachy i Klaudia infiltrują wpierw Pallidę Voss, bo okazuje się, że Annikę wrabiają jej własni podwładni. Klaudia zdobyła wszystkie potrzebne dowody, Eustachy chciał ratować ludzi... i Paradoks (po sabotowaniu generatorów Memoriam) spowodował straszne straty w ludziach i przesunął Pallidę Voss w Anomalię Kosmiczną. Oops. Ale dzięki drakolitom (keldanitom) udało się sporo ludzi uratować.

Aktor w Opowieści:

* Dokonanie:
    * nie ma pojęcia, ale jest wkręcana politycznie. Jej "przyjaciele" manewrują dookoła niej, by móc zniszczyć rekoncyliację pallidanie - keldanici, ją i jej rodzinę. Nie wierzy w istnienie Saitaera XD.


### Sekrety Keldan Voss

* **uid:** 220202-sekrety-keldan-voss, _numer względny_: 2
* **daty:** 0112-03-24 - 0112-03-26
* **obecni:** Annika Pradis, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Mateus Sarpon

Streszczenie:

Zespół skutecznie ewakuował pallidan z Keldan Voss, po drodze orientując się że część porwanych przez Mgły osób została Skażona i zmieniona w Kroczących w Mgle. Eustachy wydobył jednego do badań dla Marii. Co ciekawe - pallidanie i keldanici widzą zupełnie inne wersje historii ("snajper" vs "porwania") a do tego Mgły powodują jeszcze większą nierzeczywistość i złudzenia. A Arianna politycznie a Klaudia naukowo dochodzą do tego co jest prawdą...

Aktor w Opowieści:

* Dokonanie:
    * pallidanka; dowodzi bazą; poprosiła Eustachego o ewakuację z tego miejsca. Załamana faktem, że ich porwani ludzie stali się Kroczącymi we Mgle. Nie daruje keldanowcom...


### Keldan Voss, kolonia Saitaera

* **uid:** 220126-keldan-voss-kolonia-saitaera, _numer względny_: 1
* **daty:** 0112-03-19 - 0112-03-22
* **obecni:** Annika Pradis, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Kormonow Voss, Mateus Sarpon, OO Kastor, SP Pallida Voss, Szczepan Kaltaben, Zygfryd Maus

Streszczenie:

Kramer wysłał załogę Inferni na Keldan Voss (stację pod kontrolą pallidan i źródło kryształów vitium), bo tam mogą więcej wiedzieć o anomalizacji ludzi i magów. Co więcej, jest poważny problem polityczny i techniczny - pallidanie zwalczają lokalnych drakolitów i kolonia ma problem typu wojna domowa. Na miejscu okazało się, że są CO NAJMNIEJ dwa stronnictwa a Klaudia uznała, że lokalna Hestia nie jest Hestią. Więc czym jest?

Aktor w Opowieści:

* Dokonanie:
    * pallidanka; dowodzi bazą. Strasznie naiwna; wierzy w DOBRO Orbitera, w Ariannę. Chce dobrze, ale: zakazała dawnego kultu, sprowadza ludzi z zewnątrz... wierzy, że wszyscy mogą się lubić.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 5, @: 0112-04-09
    1. Primus    : 5, @: 0112-04-09
        1. Sektor Astoriański    : 5, @: 0112-04-09
            1. Iorus    : 5, @: 0112-04-09
                1. Iorus, pierścień    : 5, @: 0112-04-09
                    1. Keldan Voss    : 5, @: 0112-04-09

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 5 | ((220126-keldan-voss-kolonia-saitaera; 220202-sekrety-keldan-voss; 220216-polityka-rujnuje-pallide-voss; 220223-stabilizacja-keldan-voss; 220309-upadek-eleny)) |
| Elena Verlen         | 5 | ((220126-keldan-voss-kolonia-saitaera; 220202-sekrety-keldan-voss; 220216-polityka-rujnuje-pallide-voss; 220223-stabilizacja-keldan-voss; 220309-upadek-eleny)) |
| Eustachy Korkoran    | 5 | ((220126-keldan-voss-kolonia-saitaera; 220202-sekrety-keldan-voss; 220216-polityka-rujnuje-pallide-voss; 220223-stabilizacja-keldan-voss; 220309-upadek-eleny)) |
| Klaudia Stryk        | 5 | ((220126-keldan-voss-kolonia-saitaera; 220202-sekrety-keldan-voss; 220216-polityka-rujnuje-pallide-voss; 220223-stabilizacja-keldan-voss; 220309-upadek-eleny)) |
| Mateus Sarpon        | 3 | ((220126-keldan-voss-kolonia-saitaera; 220202-sekrety-keldan-voss; 220216-polityka-rujnuje-pallide-voss)) |
| Szczepan Kaltaben    | 3 | ((220126-keldan-voss-kolonia-saitaera; 220216-polityka-rujnuje-pallide-voss; 220223-stabilizacja-keldan-voss)) |
| SP Pallida Voss      | 2 | ((220126-keldan-voss-kolonia-saitaera; 220216-polityka-rujnuje-pallide-voss)) |
| Tomasz Kaltaben      | 2 | ((220223-stabilizacja-keldan-voss; 220309-upadek-eleny)) |
| Kormonow Voss        | 1 | ((220126-keldan-voss-kolonia-saitaera)) |
| OO Kastor            | 1 | ((220126-keldan-voss-kolonia-saitaera)) |
| Raoul Lavanis        | 1 | ((220216-polityka-rujnuje-pallide-voss)) |
| SP Światło Nadziei   | 1 | ((220223-stabilizacja-keldan-voss)) |
| Zygfryd Maus         | 1 | ((220126-keldan-voss-kolonia-saitaera)) |
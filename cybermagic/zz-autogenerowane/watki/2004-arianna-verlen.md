# Arianna Verlen
## Identyfikator

Id: 2004-arianna-verlen

## Sekcja Opowieści

### Lewiatan za Pandorę

* **uid:** 220622-lewiatan-za-pandore, _numer względny_: 92
* **daty:** 0112-07-26 - 0112-07-29
* **obecni:** Arianna Verlen, Eustachy Korkoran, Klaudia Stryk, Maria Naavas, Ola Szerszeń, OO Infernia, OO Pandora, OO Straszliwy Pająk, OO Tivr, Raoul Lavanis, Rzeźnik Parszywiec Diakon

Streszczenie:

PRZESZŁOŚĆ: Ola Szerszeń opiekuje się Eleną w Regeneratorze; okazuje się być biosyntem (zakazane eksperymenty Termii). Rzeźnik Diakon to łagodny i jowialny kapitan Straszliwego Pająka, acz znienawidzony za przeszłość zdrajcy Orbitera oraz Noctis. TERAŹNIEJSZOŚĆ: Lewiatan zostaje zestrzelony torpedą anihilacyjną, dzięki Raoulowi ratujemy wielu ludzi na orbicie Lewiatana. Malictrix d'Pandora zostaje zniszczona przez Lewiatana, ale Klaudia ma jego mikrokopię dzięki Ariannie.

Aktor w Opowieści:

* Dokonanie:
    * gdy Ola Szerszeń zaczęła interesować się biovatem Eleny, dała Eustachemu i Klaudii dowodzenie nad Olą by ta nie szalała. Podczas starcia z Lewiatanem używając swojej mocy skutecznie ożywiła kawałek Lewiatana i go "oswoiła", po czym stworzony byt przekazała Klaudii w prezencie.


### Lewiatan przy Seibert

* **uid:** 220615-lewiatan-przy-seibert, _numer względny_: 91
* **daty:** 0112-07-23 - 0112-07-25
* **obecni:** Arianna Verlen, Eszara d'Seibert, Eustachy Korkoran, Jonasz Parys, Klaudia Stryk, Ola Szerszeń

Streszczenie:

W okolicach Anomalii Kolapsu obudził się Lewiatan. Ruszył na sortownię Seibert, pożerając kilka jednostek. Grupa Infernia, która miała ćwiczenia (i pokonała koloidową korwetę Zająca 3) dała radę zlokalizować Lewiatana, zabezpieczyć się przed aspektem bazyliszka, zdobyć trochę jego energii do badań i przygotować się do neutralizacji. Jakoś.

Aktor w Opowieści:

* Dokonanie:
    * skutecznie opanowała przechwyconą energię Lewiatana w baterii vitium i sformowała zeń przynętę na Lewiatana. Sposób, by Lewiatana gdzieś przeprowadzić.


### Ratujemy porywaczy Eleny

* **uid:** 220610-ratujemy-porywaczy-eleny, _numer względny_: 90
* **daty:** 0112-07-11 - 0112-07-13
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Hestia d'Atropos, Jakub Bulgocz, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Raoul Lavanis

Streszczenie:

Elena jest już zbyt zdestabilizowana by funkcjonować niezależnie, więc wpierw zajęli się nią w Laboratorium Kranix a po zbudowaniu dla niej Containment Chamber przesłali ją do stacji medycznej Atropos do regeneracji. Stamtąd ją i Martyna porwał Syndykat Aureliona (ale Martyn przewidział JAKIŚ problem i ostrzegł Klaudię). Elena by zniszczyła okręt napastników, ale Zespół skutecznie odkrył problemy na Atropos, przesłuchali dowódcę, zneutralizowali miragenta i dotarli do okrętu Aureliona zdobywając jeńców i zatrzymując Elenę przed przebudzeniem i Spustoszeniem statku.

Aktor w Opowieści:

* Dokonanie:
    * ustabilizowała Elenę, gdy tą pokonało jej własne Spustoszenie. Przesłuchała lekarzy i zlokalizowała kiedy zaczęły się problemy na Atropos. Doprowadziła do tego, że dyrektor Atropos został medykiem na Inferni i myśli że to lepsza opcja niż court of law.
* Progresja:
    * Syndykat Aureliona wie, że Arianna nań poluje. Syndykat nie potraktuje tego łagodnie jak coś Arianna zrobi.


### EtAur i przynęta na Kryptę

* **uid:** 220330-etaur-i-przyneta-na-krypte, _numer względny_: 89
* **daty:** 0112-04-27 - 0112-04-30
* **obecni:** Arianna Verlen, Eustachy Korkoran, Klaudia Stryk, Lamia Akacja, Leszek Czarban, Maria Naavas, Raoul Lavanis

Streszczenie:

Okazało się, że "potwór" to były faeril z Neikatis zarażony bronią biologiczną. Próbował doprowadzić do katastrofy by Orbiter lub Eternia zabrali się za leczenie tego cholerstwa, pomagał mu Antoni, p.o. TAI na EtAur. Maria doszła do biostruktury, Klaudia przejęła systemy bazy, Eustachy przechwycił gościa, Arianna zdobyła bazę politycznie. A Klaudia zrobiła raport, by Orbiter nie atakował więcej neutralnych i potencjalnie sojuszniczych sił nie-Orbiterowych w kosmosie. Jako, że Lamii nic się nie stało, Melania jest szczęśliwa.

Aktor w Opowieści:

* Dokonanie:
    * przejęła dowodzenie nad EtAur po dowodach, zaprowadziła kwarantannę i zaczęła czyścić bazę. Zdobyła Daniela - "potwora" - żywego jako przynętę na Kryptę.
* Progresja:
    * ma SMACZNĄ przynętę na Kryptę. Byłego potwora z EtAur, Daniela.
    * NIECHĘĆ ze strony purystów Orbitera. PRO-LUDZKA grupa Orbitera, Eternia i Aurum uważają Infernię za siłę której wiele zawdzięczają.


### Potwór czy choroba na EtAur?

* **uid:** 220316-potwor-czy-choroba-na-etaur, _numer względny_: 88
* **daty:** 0112-04-24 - 0112-04-26
* **obecni:** Arianna Verlen, Dominika Perikas, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Maria Naavas, Melania Akacja, Suwan Chankar, Tymoteusz Czerw

Streszczenie:

Na stacji EtAur pojawiły się poważne problemy - potwór? Choroba? Suwan próbuje ustabilizować co się da, ale nic nie może zrobić. Arianna poproszona o pomoc przybyła, ale nikt nie chce z nią praktycznie współpracować. Klaudia doszła do przyczyny problemów - choroba, ale potwór jest wektorem. Arianna doszła do transmisji po kanałach magicznych. Świetnie.

Aktor w Opowieści:

* Dokonanie:
    * koordynacja; do niej odzywa się Eternia i ona jest tą, z którą wszyscy rozmawiają. Doszła do tego, że choroba może rozprzestrzeniać się po kanałach magicznych.


### Upadek Eleny

* **uid:** 220309-upadek-eleny, _numer względny_: 87
* **daty:** 0112-04-03 - 0112-04-09
* **obecni:** Annika Pradis, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Tomasz Kaltaben

Streszczenie:

Keldan Voss została ustabilizowana. Annika będzie mieć szansę powodzenia - zostaje przełożoną, z łącznikiem z keldanitami Mateusem. Odbudowany jej link z pallidanami. Pallidanie mają ogólnie "spokój". Tomasz Kaltaben będzie ją wspierał a on sam dostanie 3 magów Orbitera co go wspierają (i są agentami). Elena niestety zabrała trzy życia odzyskując naturalną urodę. Klaudia ciężko pracuje by zbudować dla Eleny Detox Chamber - ona nie jest OK.

Aktor w Opowieści:

* Dokonanie:
    * załatwiła polityczne wsparcie dla Keldan Voss i Anniki od pallidan. Wymusiła współpracę między Tomaszem Kaltabenem i Anniką Pradis. I zapewniła że Barnaba i Szczepan spotkają sprawiedliwość.


### Stabilizacja Keldan Voss

* **uid:** 220223-stabilizacja-keldan-voss, _numer względny_: 86
* **daty:** 0112-03-30 - 0112-04-02
* **obecni:** Annika Pradis, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, SP Światło Nadziei, Szczepan Kaltaben, Tomasz Kaltaben

Streszczenie:

Ewakuacja pallidan na Keldan Voss się udało, acz Eustachy musiał zniszczyć anomalnych napastników z Mgły i naprawić morale Anniki. Elenie (słyszącej szepty) udało się zniszczyć BIA i wprowadzić na jej miejsce amalgamat szamana. Arianna ma polityczną kontrolę nad sytuacją a dzięki Eustachemu udało się sytuację ustabilizować militarnie. Dzięki Klaudii wiedzą co i jak. Teraz już tylko zostaje zostawić Keldan Voss w stabilnej formie.

Aktor w Opowieści:

* Dokonanie:
    * politycznie zapewnia, by uciekinierzy z Pallidy Voss mogli znaleźć tymczasowe schronienie w Keldan Voss. Przesłuchała i wkręciła Szczepana.


### Polityka rujnuje Pallidę Voss

* **uid:** 220216-polityka-rujnuje-pallide-voss, _numer względny_: 85
* **daty:** 0112-03-27 - 0112-03-29
* **obecni:** Annika Pradis, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Mateus Sarpon, Raoul Lavanis, SP Pallida Voss, Szczepan Kaltaben

Streszczenie:

Arianna we współpracy z Mateusem planują uratować zakładników pallidańskich od radykałów keldańskich. Ale by to osiągnąć Eustachy i Klaudia infiltrują wpierw Pallidę Voss, bo okazuje się, że Annikę wrabiają jej własni podwładni. Klaudia zdobyła wszystkie potrzebne dowody, Eustachy chciał ratować ludzi... i Paradoks (po sabotowaniu generatorów Memoriam) spowodował straszne straty w ludziach i przesunął Pallidę Voss w Anomalię Kosmiczną. Oops. Ale dzięki drakolitom (keldanitom) udało się sporo ludzi uratować.

Aktor w Opowieści:

* Dokonanie:
    * konspiruje z Mateusem by skutecznie wyciągnąć zarówno pallidan od keldanitów jak i keldanitów od pallidan.


### Sekrety Keldan Voss

* **uid:** 220202-sekrety-keldan-voss, _numer względny_: 84
* **daty:** 0112-03-24 - 0112-03-26
* **obecni:** Annika Pradis, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Mateus Sarpon

Streszczenie:

Zespół skutecznie ewakuował pallidan z Keldan Voss, po drodze orientując się że część porwanych przez Mgły osób została Skażona i zmieniona w Kroczących w Mgle. Eustachy wydobył jednego do badań dla Marii. Co ciekawe - pallidanie i keldanici widzą zupełnie inne wersje historii ("snajper" vs "porwania") a do tego Mgły powodują jeszcze większą nierzeczywistość i złudzenia. A Arianna politycznie a Klaudia naukowo dochodzą do tego co jest prawdą...

Aktor w Opowieści:

* Dokonanie:
    * Przekonała odłam drakolitów, że warto współpracować z korporacją. Politycznie zmusza drakolitów do benefit of doubt.


### Keldan Voss, kolonia Saitaera

* **uid:** 220126-keldan-voss-kolonia-saitaera, _numer względny_: 83
* **daty:** 0112-03-19 - 0112-03-22
* **obecni:** Annika Pradis, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Kormonow Voss, Mateus Sarpon, OO Kastor, SP Pallida Voss, Szczepan Kaltaben, Zygfryd Maus

Streszczenie:

Kramer wysłał załogę Inferni na Keldan Voss (stację pod kontrolą pallidan i źródło kryształów vitium), bo tam mogą więcej wiedzieć o anomalizacji ludzi i magów. Co więcej, jest poważny problem polityczny i techniczny - pallidanie zwalczają lokalnych drakolitów i kolonia ma problem typu wojna domowa. Na miejscu okazało się, że są CO NAJMNIEJ dwa stronnictwa a Klaudia uznała, że lokalna Hestia nie jest Hestią. Więc czym jest?

Aktor w Opowieści:

* Dokonanie:
    * overridowała lekarza, lepiej dla Eleny jeśli poleci z nimi. Przekierowała Eustachego do rozmowy z pallidanami, sama skupia się na drakolitach z kultu Saitaera.


### To nie pułapka na Nereidę...

* **uid:** 220105-to-nie-pulapka-na-nereide, _numer względny_: 82
* **daty:** 0112-02-28 - 0112-03-04
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Izabela Zarantel, Kamil Lyraczek, Klaudia Stryk, Lutus Amerin, Maria Naavas, Natalia Aradin, Saitaer

Streszczenie:

Plan - dorwanie tajemniczych koloidowych korwet. Wiemy, że to pułapka, więc zróbmy kontr-pułapkę. Ale okazało się, że to nie pułapka na Nereidę a na Infernię. Standardowe badania wprowadziły w Infernię (Dianę) memetic payload i Diana prawie wpadła pod kontrolę Saitaera - odrzucenie kokonu z Natalią sprawiło, że udało się ciężko uszkodzoną Infernię uratować. Zwalczając ixion Arianna sięgnęła do Esuriit przez Elenę; Infernia by zginęła gdyby nie samobójcza akcja Izabeli i Kamila. Elena wchłonęła tyle energii ile była w stanie, przekraczając swój Wzór. ALE - Infernia przetrwała. Przy okazji, Natalia się wykluła. Ani osoba ani statek kosmiczny - mordercza anomalia kosmiczna.

Aktor w Opowieści:

* Dokonanie:
    * zbudowała z noktiańskiego Tivra perfekcyjną maszynę wojskową, po czym opanowała kult. Niestety z uwagi na problemy z magią w obliczu Saitaera uwolniła Esuriit i prawie zniszczyła Infernię.
* Progresja:
    * przejęła kontrolę nad kultem i pozycjonowała Ottona na arcykapłana po śmierci Kamila.


### Kult Saitaera w Neotik

* **uid:** 211222-kult-saitaera-w-neotik, _numer względny_: 81
* **daty:** 0112-02-24 - 0112-02-25
* **obecni:** Adam Szarjan, Arianna Verlen, Diana d'Infernia, Elena Verlen, Eustachy Korkoran, Izabela Zarantel, Kasandra Destrukcja Diakon, Klaudia Stryk, Lutus Amerin, Maciek Kalmarzec, Maria Naavas

Streszczenie:

Klaudia wróciła z bronią anty-Serenitową od Tosena, Arianna z feromonami Marii nadała Inferni nową kulturę - miłośnicy lolitek x kultyści x noktianie. Elena uziemiona w Inferni zinfiltrowała Stocznię ukryta przez Klaudię i odkryła niższe Spustoszenie + kult Saitaera. Nereida jest czysta od Saitaera; przetransferowana na Infernię dla Eleny i uzbrojona. Klaudia ma listę jednostek potencjalnie w rękach Saitaera / Spustoszonych. Przygotowania do kontrataku skończone.

Aktor w Opowieści:

* Dokonanie:
    * przemowa + feromony Marii zmieniły kulturę Inferni w dziwną; uziemiła Elenę w Inferni by ta mogła zinfiltrować Stocznię i zaprojektowała plan - przekazać Nereidę Elenie i zdobyć koloidowy statek.
* Progresja:
    * aspekt Vigilusa (Vigilus - Nihilus).Załoga i Kult jest przekonana, że jest Aspektem Zbawiciela.


### Sklejanie Inferni do kupy

* **uid:** 211215-sklejanie-inferni-do-kupy, _numer względny_: 80
* **daty:** 0112-02-21 - 0112-02-23
* **obecni:** Adam Szarjan, Arianna Verlen, Diana d'Infernia, Elena Verlen, Eustachy Korkoran, Kamil Lyraczek, Kasandra Destrukcja Diakon, Leona Astrienko, Maria Naavas, OO Infernia, Roland Sowiński

Streszczenie:

Ixioński wariant Inferni okazuje się całkiem słodki i pod kontrolą Eustachego. Morale Inferni jest w ruinie. Z pomocą Marii Arianna przesuwa kulturę Inferni w stronę "kultyści + noktianie + miłośnicy psychopatycznej lolitki Diany". Eustachy wpada w pułapkę Leony, która chce go wyleczyć czerwiem Esuriit - ale Eustachy przekonuje ją, że ma Dianę pod kontrolą. Jednak gdzieś w tle znajduje się prawdziwy ixioński potwór na stoczni Neotik.

Aktor w Opowieści:

* Dokonanie:
    * zatrzymała Elenę na Inferni (dokładniej: Tivrze), uspokoiła Dianę która chciała niszczyć by ratować Eustachego, poznała prawdę od Kamila odnośnie swej przyszłości (awatar Vigilusa), decyzja o feromonach - nowa kultura Inferni. I zrobiła panikę w Stoczni Neotik - czy Hestia to pokaże?


### O krok za daleko

* **uid:** 211208-o-krok-za-daleko, _numer względny_: 79
* **daty:** 0112-02-19 - 0112-02-20
* **obecni:** Adam Szarjan, Arianna Verlen, Diana d'Infernia, Elena Verlen, Eustachy Korkoran, Hestia d'Neotik, Jarosław Szarjan, Klaudia Stryk, Leona Astrienko, OO Infernia, Wawrzyn Rewemis

Streszczenie:

Klaudia doszła do tego, że Hestia d'Neotik oszukuje Stocznię; w okolicy są niewielkie statki koloidowe o których Stocznia nie wie. Arianna zintegrowała Eustachego z Infernią ixiońsko, niestety anomalizując Infernię. Elena ciężko Skażona ixionem i (w swoim mniemaniu) porzucona przez Eustachego. Eustachy ujarzmiając Infernię rozbijał ją o różne asteroidy, co powoduje śmierć części załogi. Infernia ma zerowe morale, ale jest opanowana. Przyjazna Anomalia Kosmiczna z tymczasową bazą w Stoczni Neotik.

Aktor w Opowieści:

* Dokonanie:
    * opanowując infekcję ixiońską skupiła się na wzmocnieniu Eustachego i jego integracji z Infernią; udało jej się, ale ciężko Skaziła Elenę ixionem i trwale zanomalizowała Infernię. Potem przekonała Jarka Szarjana do tego, by Natalia mogła się wykluć na Inferni i by Neotik był _safe haven_ Inferni.


### Prototypowa Nereida Natalii

* **uid:** 211124-prototypowa-nereida-natalii, _numer względny_: 78
* **daty:** 0112-02-14 - 0112-02-18
* **obecni:** Adam Szarjan, Arianna Verlen, Arkadia Verlen, Elena Verlen, Eustachy Korkoran, Izabela Zarantel, Klaudia Stryk, Leona Astrienko, Maria Naavas, Natalia Aradin, OO Infernia, Roland Sowiński, Wawrzyn Rewemis

Streszczenie:

Kampania reklamowa "dołącz do Inferni" zrobiona przez Izę przy użyciu Eustachego jako modela - sukces. Klaudia zdobyła z tego nowych załogantów (acz to jeszcze potrwa). Szarjan uwolnił Klaudię z zarzutów, potrzebuje pomocy w stoczni - jego przyjaciółka Natalia ma problem z prototypem myśliwca klasy Nereida i została ixiońsko zintegrowana. Infernia próbowała Nereidę uratować, ale niestety skończyło się to poważnym uszkodzeniem Nereidy i stworzeniem ixiońskiego kokonu, z którego Natalia musi się wykluć...

Aktor w Opowieści:

* Dokonanie:
    * nie dołączyła Rolanda do załogi i dała mu odczuć że go nie chce. Wymyśliła jak zwabić Nereidę do Inferni, używając theme song Sekretów Orbitera. Dobrze pilotuje Infernię pozycjonując prawidłowo Elenę w kosmosie (jak minę).


### Porwany Trismegistos

* **uid:** 211117-porwany-trismegistos, _numer względny_: 77
* **daty:** 0112-02-09 - 0112-02-11
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Izabela Zarantel, Jamon Korab, Kalira d'Trismegistos, Klara Gwozdnik, Klaudia Stryk, Leona Astrienko, Maria Naavas, Mira Anastel, OO Tivr, Roland Sowiński, SC Trismegistos, Zygfryd Maus

Streszczenie:

Wiadomość od pirata że porwał Trismegistos poderwała Ariannę i Tivr. Historia nie trzymała się kupy. Załoga Inferni (Tivru) zorientowała się, że coś jest nie tak - opowieść o statku kupieckim który pozbył się miragenta brzmiała DZIWNIE. Okazało się, że Trismegistos jest statkiem kaperskim. Gdy Trismegistos zażądał poddania się Tivru Klaudia zrobiła sygnał "lol leci na Was Serenit" i TAI Trismegistosa spanikowała. Wolna TAI, współpracująca z fareilem i drakolitką. Wolny dystrykt. Arianna dała im żyć - ale odzyskała porwanych arystokratów i komory adaptacji.

Aktor w Opowieści:

* Dokonanie:
    * ukryła swą obecność na Tivrze, neutralizowała Rolanda Sowińskiego i pozwoliła Trismegistosowi odlecieć. Chce, by wszyscy żyli w pokoju a Trismegistos jest nadzieją na lepsze jutro.
* Progresja:
    * ona podobno steruje Serenitem. Może go przyzywać i odpychać na żądanie. Królowa Anomalii Kosmicznych. W plotkę wierzą wszędzie POZA Orbiterem.


### Romans dzięki Esuriit

* **uid:** 211110-romans-dzieki-esuriit, _numer względny_: 76
* **daty:** 0112-02-07 - 0112-02-08
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Karol Reichard, Klaudia Stryk, Leona Astrienko, Maria Naavas, Wawrzyn Rewemis

Streszczenie:

Infernia nie ma dość załogi, by móc działać, więc Termia przekazuje Ariannie Tivr. Leona nie może sobie wybaczyć, że jej nie było i to się stało, więc się wypisuje z pomocą Eustachego (co powoduje plotki Leona x Eustachy). Elena ucieka do Sektora 49 na K1 by być samotna; Arianna organizuje ostre poszukiwania. Eustachy znajduje Elenę by ją pocieszyć. Elena w końcu przyznaje Eustachemu, że go kocha. Maria Naavas z woli Martyna tymczasowo dołącza do załogi Arianny.

Aktor w Opowieści:

* Dokonanie:
    * działania administracyjne; akceptacja listów o zgonie załogantów (+ pisze coś od siebie), NIE UKRYWA Flawii mimo że mogłaby uniknąć potencjalnych reperkusji, organizuje gry wojenne by ukryć, że Elena sama uciekła do czarnych sektorów K1.
* Progresja:
    * dostaje Tivr jako swoją drugą jednostkę


### Rzieza niszczy Infernię

* **uid:** 211027-rzieza-niszczy-infernie, _numer względny_: 75
* **daty:** 0112-01-22 - 0112-01-27
* **obecni:** Arianna Verlen, Elena Verlen, Flawia Blakenbauer, Klaudia Stryk, Martyn Hiwasser, Otto Azgorn, TAI Rzieza d'K1

Streszczenie:

Infernia wraca z Mevilig. Rzieza chce wyczyścić im pamięć o sobie. Arianna się opiera. Rzieza dowiaduje się o Ataienne. Arianna budzi Elenę po sympatii, Elena sprowadza Esuriit do laboratorium dekontaminacyjnego K1. Dużo śmierci. Martyn próbuje to opanować - budzi swoje simulacrum. Arianna i Klaudia opanowują przy pomocy Rziezy sytuację. 37% załogi Inferni nie żyje, Flawia wyssana w kosmos, Elena złamana, Martyn w szpitalu.

Aktor w Opowieści:

* Dokonanie:
    * chcąc powstrzymać ingerencję w jej pamięć Rziezy użyła podświadomie technik Ataienne. Rzieza dowiedział się o Ataienne. Arianna wezwała Elenę na pomoc, która zalała Esuriit laboratorium. Arianna z Klaudią próbowała opanować załogę Inferni i jak Martyn wyssał energię Esuriit z załogi, dała radę to zrobić.
* Progresja:
    * jej umysł i pamięć zostały odbudowane przez Rziezę. Pamięta moc Ataienne i to, co Ataienne jej zrobiła.


### Kurczakownia

* **uid:** 211020-kurczakownia, _numer względny_: 74
* **daty:** 0112-01-18 - 0112-01-20
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Flawia Blakenbauer, Kamil Lyraczek, Klaudia Stryk, Martyn Hiwasser, OO Infernia, Otto Azgorn, Vigilus Mevilig

Streszczenie:

By uratować jak najwięcej osób w Sektorze Mevilig, Eustachy zaprojektował Kurczakownię - maszynę do oddzielania mechanicznego głów i rdzenia od reszty człowieka. Arianna starła się ze Zbawicielem-Niszczycielem jako aspekt Zbawiciela, wspierana przez Działo Rozpaczy (Eustachy). Udało im się sporo uratować (40% populacji Planetoidy Kalarfam), acz załoga Inferni będzie mieć koszmary senne. Klaudia zebrała dość danych, by zapobiec naturalnej manifestacji Zbawiciela-Niszczyciela w Sektorze Astoriańskim i zrobić kontrakcję jak co.

Aktor w Opowieści:

* Dokonanie:
    * udaje Zbawiciela - aspekt mający wszystkich uratować z Kalarfam. Zmierzyła się ze Zbawicielem-Niszczycielem przy jego ołtarzu i kupiła Eustachemu czas na strzelenie weń Działem Rozpaczy. Stała się prawdziwą inkarnacją Zbawiciela dla uratowanych kultystów Zbawiciela-Niszczyciela.
* Progresja:
    * BOHATERKA! Weszła do Sektora Mevilig by ratować Orbiterowców i jej się to udało. A ryzykowała nieprawdopodobnie dużo. Plus, [1500-2000] osób więcej? Wow!
    * Stała się prawdziwą inkarnacją aspektu Zbawiciela dla uratowanych kultystów Zbawiciela-Niszczyciela.


### Szara nawałnica

* **uid:** 211013-szara-nawalnica, _numer względny_: 73
* **daty:** 0112-01-12 - 0112-01-17
* **obecni:** Aleksandra Termia, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Martyn Hiwasser, Otto Azgorn, Remigiusz Błyszczyk, TAI Rzieza d'K1

Streszczenie:

Zespół przygotował się na wejście do Mevilig. Rzieza odkrył jak działają Piranie i że tamten sektor jest opanowany przez TAI 5+ generacji. Niestety, Infernia musiała uciekać przed Rziezą. Na miejscu Eustachy znalazł skuteczny sposób przemykania między Piraniami, dotarli do planetoidy Kalarfam gdzie znaleźli bazę ludzi. Mechaniczny "smok" został zniszczony przez Infernię, pobrali lokalsów i Arianna przekonała ich do zmiany kultu. Aha, anomalny wybuch torpedy anihilacyjnej Arianna przekierowała na planetę, wypowiadając ostrą wojnę TAI 5+ generacji...

Aktor w Opowieści:

* Dokonanie:
    * znalazła sposób by Vigilus nie widział servarów - użyć luster. Potem - złamała konwencję anomalizując torpedę anihilacyjną (wybuch) i kierując go ku planecie w sektorze Mevilig. Aha, przekonała grupkę ludzi z Mevilig że ona jest silniejsza niż zbawiciel - jej kult jest lepszy.


### Grupa Ekspedycyjna Kellert

* **uid:** 210929-grupa-ekspedycyjna-kellert, _numer względny_: 72
* **daty:** 0112-01-07 - 0112-01-10
* **obecni:** Adam Nerawol, Aleksandra Termia, Antoni Kramer, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Olena Orion, OO Infernia, OO Omega Septius, Otto Azgorn, TAI Marszałek Grzmotoszpon Trzeci, Vigilus Mevilig

Streszczenie:

Termia wysłała siły do sektora "Noviter", ale to był inny sektor (Mevilig). Straciła bezzałogowce, potem grupę ekspedycyjną Kellert. Kramer wysłał Infernię zakoloidowaną; okazało się, że ten sektor ma "strażnika Esuriit" zrobionego przez ludzi i grupę Piranii, sterowanych przez TAI 4 poziomu. Inferni udało się zebrać dane i wrócić z siedmioma uratowanymi członkami OO Savera, gdzie większość załogi została corruptowana przez Vigilusa...

Aktor w Opowieści:

* Dokonanie:
    * zdecydowała się na skrajnie niebezpieczną misję, by ratować ludzi Termii. W kluczowym momencie - czy Martyn uratuje ludzi czy nie, sam na sam z Vigilusem - kazała szturmować korzystając z tego że Piranie się oddaliły. Uratowała Martyna i siedem osób.


### Ostatnia akcja bohaterki

* **uid:** 210922-ostatnia-akcja-bohaterki, _numer względny_: 71
* **daty:** 0111-12-19 - 0112-01-03
* **obecni:** Antoni Kramer, Arianna Verlen, Elena Verlen, Izabela Zarantel, Jolanta Kopiec, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Olgierd Drongon, OO Żelazko, Roland Sowiński

Streszczenie:

Z Martynem skontaktowała się tien Kopiec prosząc o ostatnią misję zanim Esuriit ją pożre. Martyn zorganizował lot Infernią. Ustabilizował Jolantę, by ta dała radę dolecieć słowami i opowieściami, przy okazji tworząc piękną opowieść o Eterni i eulogię dla Jolanty - bohaterki wojennej - w Sekretach Orbitera (przy okazji wyszła jego przeszłość). Infernia zniszczyła Krwawą Bazę Piratów kosztem dewastacji Żelazka. Jolanta zniszczyła krwawego maga. Sektor jest bezpieczniejszy, Eternia szczęśliwa a Arianna ma profity.

Aktor w Opowieści:

* Dokonanie:
    * przekonała Kramera, zdobyła Olgierda, przekonała Leonę by ta nie polowała na tien Kopiec (daje szansę zabicia DWÓCH Eternian) by na końcu służyć jako dywersja dla Jolanty... i oczywiście Skaziła bazę piratów, ożywiając ją po swojemu. Aha, użyła emitera anihilacji. Spodobał jej się.
* Progresja:
    * Eternia jest zachwycona odcinkiem Sekretów Orbitera z Jolantą Kopiec. Arianna spłaciła wszelkie długi.


### Stabilizacja Bramy Eterycznej

* **uid:** 210901-stabilizacja-bramy-eterycznej, _numer względny_: 70
* **daty:** 0111-12-05 - 0111-12-08
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Flawia Blakenbauer, Gilbert Bloch, Janus Krzak, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Medea Sowińska, OO Kanagar, OO Netrahina, OO Trasman, Seweryn Atanair

Streszczenie:

Infernia wróciła z innymi jednostkami naprawić Bramę Kariańską. Eustachy ufortyfikował i uniewidocznił pintki; Arianna pozyskała dane z reanimowanego magią krążownika Orbitera. Przez Bramę przeszła ON Catarina; Klaudia ją zlokalizowała i wystawiła Netrahinie i Niobe. Niestety, Arianna hiperlinkowała się z ON Spatium Gelida. Dla nich minęło tylko kilka godzin, dla nas - kilkadziesiąt lat...

Aktor w Opowieści:

* Dokonanie:
    * gdy Elena była pod wpływem Esuriit, zneutralizowała ją mówiąc o pasie cnoty dla Eustachego. Bezbłędnie nawigowała Skażony OO Perforator, gdzie połowa systemów była efemeryczna. Aha, zlinkowała się hipernetem z ON Spatium Gelida.
* Progresja:
    * połączona ze Spatium Gelida przez hipernet. W jakiś sposób. Magicznie. Jest kotwicą i komunikatorem.


### Uszkodzona Brama Eteryczna

* **uid:** 210825-uszkodzona-brama-eteryczna, _numer względny_: 69
* **daty:** 0111-11-30 - 0111-12-03
* **obecni:** AK Nocna Krypta, Arianna Verlen, Diana d'Infernia, Elena Verlen, Eustachy Korkoran, Flawia Blakenbauer, Gilbert Bloch, Klaudia Stryk, Medea Sowińska, ON Spatium Gelida, OO Infernia, OO Mfumo

Streszczenie:

Infernia została przechwycona przez Medeę lecąc na Anomalię Kolapsu i przekierowana na uszkodzoną Bramę Eteryczną (gdzie, jak się okazuje, są pasożyty i flota noktiańska która nie doleciała). Na miejscu Infernia uniknęła efemeryd i channelując Kryptę uratowała wszystkich efemerydami. Gdy Krypta pojawiła się na serio, Infernia zwiała paląc silniki. Statki noktiańskie w Bramie mają tether na Infernię.

Aktor w Opowieści:

* Dokonanie:
    * abstrahując od przekonania Medei do wyznania że w Bramie zamknięty jest Spatium Gelida, channelowała Nocną Kryptę w zdestabilizowaną Bramę. Dzięki temu uratowała efemerydami WSZYSTKICH, nawet tych, którzy częściowo byli już w Bramie.


### Nieudana infiltracja Inferni

* **uid:** 210616-nieudana-infiltracja-inferni, _numer względny_: 68
* **daty:** 0111-11-22 - 0111-11-27
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Flawia Blakenbauer, Izabela Zarantel, Kamil Lyraczek, Klaudia Stryk, Leona Astrienko, Marian Tosen, OO Opresor, Roland Sowiński

Streszczenie:

Marian Tosen poprosił Infernię o to, by sprawdzili co się dzieje w Anomalii Kolapsu - podejrzewa anomalie memetyczne, skoro Orbiter traci tam statki  (m.in. OO Opresor) i pojawiają się jednostki nieoznaczonej frakcji. Tymczasem Roland Sowiński chce na pokład. Arianna wybiła mu to z głowy, ale wykryli agentkę wśród jego guwernantek - Flawię Blakenbauer, która miała się nim opiekować i jest trochę zniewolona przez Sowińskich. By zadośćuczynić po tym co Elena zrobiła i trochę Flawii pomóc, Flawia dołączyła do Inferni. "Oficjalnie" udało jej się Infernię zinfiltrować...

Aktor w Opowieści:

* Dokonanie:
    * przekonała Rolanda by nie leciał z nimi do Anomalii Kolapsu, wykryła tożsamość Flawii i dała jej szansę na dołączenie do Inferni. Mistrzyni odwracania uwagi i wrabiania innych że są winni.


### Siostrzenica Morlana

* **uid:** 210818-siostrzenica-morlana, _numer względny_: 67
* **daty:** 0111-11-15 - 0111-11-19
* **obecni:** Antoni Kramer, Arianna Verlen, Eustachy Korkoran, Klaudia Stryk, Leona Astrienko, Maria Naavas, Nataniel Morlan, Ofelia Morlan, Olgierd Drongon, OO Netrahina, OO Żelazko, SC Fecundatis, SC Światłodóbr, Tomasz Sowiński

Streszczenie:

Tomasz Sowiński próbuje uratować Ofelię Morlan przed Natanielem Morlanem. Nie ma kogo poprosić a Ofelia służy na Netrahinie; poprosił więc Ariannę. Arianna skanując echo pamięci Jolanty w Inferni zdobywa informacje o programie kosmicznym Orbitera, przekazuje to Kramerowi, wykorzystuje wsparcie Olgierda i robią ćwiczenia Żelazko - Netrahina. Ratują Ofelię przed porwaniem przez koloidowy statek Kruków Kasandry.

Aktor w Opowieści:

* Dokonanie:
    * panikuje Tomasza rozpinając JEDEN GUZICZEK i poznaje sekrety Tomasza i Jolanty. Przekazuje Kramerowi info o kosmicznym programie Aurum. Planuje wspólne ćwiczenia by odzyskać Ofelię. W sumie - mastermind działań przeciw Natanielowi Morlanowi.
* Progresja:
    * kody pozwalające jej na podawanie się za Jolantę Sowińską. Ich użycie to POWAŻNE nadużycie zaufania Jolanty.
    * pochwała od Kramera - pozyskała info o programie kosmicznym Aurum z pamięci Jolanty i przekazała to Kramerowi. Poważne nadużycie zaufania Jolanty i problemy dla Jolanty.


### Listy od fanów

* **uid:** 210630-listy-od-fanow, _numer względny_: 66
* **daty:** 0111-11-10 - 0111-11-13
* **obecni:** Arianna Verlen, Bogdan Anatael, Elena Verlen, Izabela Zarantel, Klaudia Stryk, Michał Teriakin, OE Lord Savaron, Olgierd Drongon, Rafael Galwarn, Remigiusz Falorin, TAI Rzieza d'K1, TAI XT-723 d'K1, TAI Zefiris

Streszczenie:

Na K1 znajduje się tajna baza Eterni, we współpracy z niektórymi elementami K1. Jeden z przekształcanych tam w Pilota chłopców wysłał fanmail do Arianny ("chce się spotkać zanim zginie na froncie"). Arianna z Klaudią znalazły obecność bazy Eterni, pozyskały Eidolona z Eterni (za PR), użyły Eidolona do infiltracji tej bazy, użyły Rziezy do zniszczenia tej bazy a wina spadła na Olgierda (który dla Arianny robił niedaleko manewry). Młodzi Piloci zostali odratowani; nie wiedzą o jaki front chodzi Eterni, ale jedno jest pewne - Eternia szykuje się do wojny kosmicznej. Ale z kim?

Aktor w Opowieści:

* Dokonanie:
    * weszła we współpracę z agentem bezpieczeństwa K1 (Galwarnem), po czym z Eterni wynegocjowała Eidolona którego dyskretnie użyła przeciw eternijskiej bazie na K1. A wszystko dla fana.


### Sekrety Kariatydy

* **uid:** 210609-sekrety-kariatydy, _numer względny_: 65
* **daty:** 0111-11-05 - 0111-11-08
* **obecni:** Arianna Verlen, Eustachy Korkoran, Marian Tosen, OO Kariatyda, Rafael Galwarn, Roland Sowiński

Streszczenie:

Arianna chce Tivr do swojej floty. Do tego celu zdecydowała się poznać sekrety Kariatydy - czemu Walrond tak unika tego statku i nazwy? Niestety, Arianna i Eustachy rozognili opowieść o "Orbiterze, który porzucił swoich w Anomalii Kolapsu" a Arianna dowiedziała się dyskretnie, że OO Kariatyda to specjalny statek - nie można powiedzieć, że był zniszczony. Ten statek został porwany przez TAI i uciekł do Anomalii Kolapsu. Arianna, promienna mistrzyni PR, zmieniła to w "KONKURS. Wyślij KARIATYDA i wygrasz przejażdżkę luksusowym statkiem". Aha, Roland Sowiński (uratowany przez Ariannę z Odłamka Serenita) chce się z nią ożenić.

Aktor w Opowieści:

* Dokonanie:
    * by zdobyć Tivr odnalazła OO Kariatyda który zaginął w Anomalii Kolapsu. Chce wyruszyć na jego poszukiwanie, ale wpakowała się w intrygę ekosystemu TAI na K1. Zaszumiła konkursem "luksusowy statek Luxuritias".
* Progresja:
    * chce się z nią ożenić Roland Sowiński. Sowińscy chcą zniszczyć ten związek. Ona próbuje nie być w tym związku XD.
    * ona i Infernia są naiwne - dali się wykorzystać w chytrej reklamie Luxuritias.
    * część Aurum na K1 chce współpracować tylko z nią. Bo nie zostawia swoich w potrzebie. I bo Roland chce się z nią ożenić.


### Morderstwo na Inferni

* **uid:** 210526-morderstwo-na-inferni, _numer względny_: 64
* **daty:** 0111-10-26 - 0111-11-01
* **obecni:** Antoni Kramer, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Feliks Walrond, Kamil Lyraczek, Karol Reichard, Klaudia Stryk, Leona Astrienko, Marian Tosen, Martyn Hiwasser, OO Tivr, Otto Azgorn, Tal Marczak

Streszczenie:

Noktiański mechanik Inferni, Tal Marczak, chciał zniszczyć OO Tivr (własność noktiańskiej rodziny Tala). Noktianie trzymają się razem - inni mechanicy Inferni doprowadzili do morderstwa Tala na K1 i wzięli amnestyki, by nikt nie wiedział co się stało. Zespół Inferni skutecznie doszedł do tego o co chodzi, odpowiedzialnych za morderstwo noktian przeniesiono na Żelazko a Arianna poważnie opieprzyła swoją noktiańską załogę. Będzie lepiej w przyszłości.

Aktor w Opowieści:

* Dokonanie:
    * próbowała utrzymać morale wśród noktian; wynegocjowała z adm. Kramerem, żeby noktian zamieszanych w morderstwo przeniósł na Żelazko a nie wysłał na ścięcie czy coś.


### Osiemnaście Oczu

* **uid:** 210519-osiemnascie-oczu, _numer względny_: 63
* **daty:** 0111-10-09 - 0111-10-20
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Leona Astrienko, Marian Tosen, Martyn Hiwasser, OO Alaya, OO Tivr

Streszczenie:

OO Alaya została zainfekowana anomalią memetyczną, infohazardem. Marian Tosen użył Zespołu by zneutralizować infohazard. Zespół przeszedł przez serię iteracji używając amnestyków by w końcu rozwiązać problem Alayi - amnestyki na całą Alayę. Do końca Zespół nie ma pojęcia czym ta anomalia była (bo wiedza ich by zaraziła). Udało się ewakuować większość Alayi.

Aktor w Opowieści:

* Dokonanie:
    * doszła do tego, że "Klaudia nie może myśleć" bo wpadnie pod infohazard. Planowała wysokopoziomowo jak radzić sobie z infohazardem na Alayi.
* Progresja:
    * wysokie uznanie ze strony Mariana Tosena z grupy antymemetyczej Orbitera.


### Ewakuacja z Serenit

* **uid:** 210512-ewakuacja-z-serenit, _numer względny_: 62
* **daty:** 0111-09-25 - 0111-10-04
* **obecni:** Aida Serenit, AK Serenit, Arianna Verlen, Bogdan Anatael, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Martyn Hiwasser, OE Falołamacz, OO Infernia, Roland Sowiński

Streszczenie:

Klaudia spowolniła Serenit, po czym Elena weszła zdalnym Entropikiem na pokład Falołamacza. Arianna zmanipulowała ludzi by dało się ich uratować. Samobójcza pintka zniechęciła Falołamacz do dalszej walki. Nie wszystkich udało się uratować (ale kapitana i arystokratę Sowińskich tak). Niestety, Infernia skończyła jako kosmiczny wrak i musiała wysłać SOS...

Aktor w Opowieści:

* Dokonanie:
    * przeszczepiła magicznie sentisieć z Eidolona do Entropika używając udrożenienia kultu, po czym zmanipulowała ludzi na Falołamaczu by Aida mogła ich uratować.


### Infekcja Serenit

* **uid:** 210428-infekcja-serenit, _numer względny_: 61
* **daty:** 0111-09-24 - 0111-09-25
* **obecni:** AK Serenit, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Martyn Hiwasser, OE Falołamacz, OE Piękna Elena, OO Infernia, Persefona d'Infernia, Rafał Grambucz, Tadeusz Ursus

Streszczenie:

Ciężko uszkodzona Infernia wraca do domu i dostaje SOS od Ursusa - OE Falołamacz "zniknął", coś z anomaliami. Gdy Zespół dotarł do Falołamacza, próbowali dowiedzieć się co się dzieje - faktycznie mamy anomaliczny okręt. Podczas badań Falołamacza Morrigan przejęła Persefonę Inferni; Eustachy musiał poważnie uszkodzić Infernię by nie zniszczyli Falołamacza. Po kosmicznym spacerze Eleny i Klaudii doszli do tego co się stało - Falołamacz staje się Odłamkiem Serenit. I jeszcze da się ludzi ratować. Ale Infernia jest w bardzo złym stanie...

Aktor w Opowieści:

* Dokonanie:
    * dowiedziała się od Tadeusza wszystkiego co trzeba o Falołamaczu, potem na podstawie wyliczeń Klaudii poleciała w kierunku na przewidywaną lokalizację Falołamacza. Silniki ledwo wytrzymały (uszkodzone).


### Znudzona załoga Inferni

* **uid:** 210421-znudzona-zaloga-inferni, _numer względny_: 60
* **daty:** 0111-09-18 - 0111-09-21
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Jolanta Sowińska, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Morrigan d'Tirakal, Otto Azgorn, Persefona d'Infernia, Tomasz Sowiński

Streszczenie:

Po raz pierwszy od dawna Infernia jest w stanie rozprzężenia - wracają z daleka po trudnej misji i nic im nie grozi. Jako, że załoga ma straszne tarcia (Leona poluje na "słabe jednostki", komandosi Verlenów chcą zatrzymać Leonę, noktianie vs astorianie...), Arianna autoryzuje Eustachego do zrobienia "Tirakala" jako niebezpieczną sytuację. Eustachy reanimuje Tirakala, ale niestety cholerstwo zanomalizowało - przywrócił Morrigan. By Zespół nie rozwiązał Tirakala za szybko, Eustachy trochę sabotował Infernię co Elena przeczytała jako... umówienie się na randkę. Gdy Klaudia wykryła, że na INFERNI pojawiła się straszna anomalia (Morrigan) próbowali to usunąć - ale Eustachy przecież sabotował Infernię! Udało im się opanować Morrigan, ale kosztem uszkodzeń i obrażeń na Inferni. I scenek kompromitujących Jolantę Sowińską. Infernia jest uszkodzona, ale się trzyma.

Aktor w Opowieści:

* Dokonanie:
    * by pozbyć się problemów z morale kazała Eustachemu zreanimować Tirakala. Skończyło się na tym, że magią zintegrowała (niechcący) w Persefonę zarówno Morrigan jak i Samuraja Miłości.


### Dekralotyzacja Asimear

* **uid:** 210414-dekralotyzacja-asimear, _numer względny_: 59
* **daty:** 0111-09-05 - 0111-09-14
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Kamil Lyraczek, Klaudia Stryk, Llarnagraht, Malictrix d'Pandora, Mariusz Tubalon, Martyn Hiwasser, Martyna Bianistek, SCA Płetwal Błękitny, Tomasz Sowiński

Streszczenie:

Wiedząc, że Jolanta jest pod wpływem kralotha Infernia sfabrykowała kralotyczny alergizator. Arianna zdradziła się Aesimar i powiedziała, że ma zamiar zniszczyć kralotha z ramienia Orbitera. Udało się kralotha zmiażdżyć i zepchnąć do podziemi Asimear, choć ze sporymi stratami ludzkimi; tymczasem Tirakal uciekł na Płetwala. Elena zinfiltrowała Płetwala, ale nie mogła zniszczyć Tirakala nie krzywdząc ludzi. Destrukcję Tirakala Infernia zostawiła więc Malictrix.

Aktor w Opowieści:

* Dokonanie:
    * manifestuje cekiny - przekonała Lazarin do współpracy przy niszczeniu kralotha (jako "ta z Sekretów Orbitera"), magią przeniosła fale mentalne z Jolanty na Eleny by dać jej możliwość przejęcia Entropika Joli.
* Progresja:
    * na Asimear ma opinię "dokładnie taka jak w Sekretach Orbitera". Z cekinami, dumą i wszystkim
    * Opinia "Krwawa Lady Verlen" na Asimear po akcji Eustachego i Klaudii z twardym, krwawym odpieraniem kralotha.


### Arianna podbija Asimear

* **uid:** 210317-arianna-podbija-asimear, _numer względny_: 58
* **daty:** 0111-08-20 - 0111-09-04
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Jolanta Sowińska, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Martyna Bianistek, SCA Płetwal Błękitny, Tomasz Sowiński

Streszczenie:

"Płetwal Błękitny" ma na pieńku z "Goldarionem"; by nie złamać konspiracji, Klaudia potraktowała Płetwala Bazyliszkiem. Tymczasem pojawiła się wiadomość, że Arianna chce zniszczyć Asimear i żąda wydania Sowińskiej. Prawdziwa Arianna złamała konspirę, skontaktowała się z Jolantą Sowińską, po czym ją porwała. Okazało się, że Jolanta jest kontrolowana przez kralotycznego pasożyta...

Aktor w Opowieści:

* Dokonanie:
    * złamała konspirację by porwać Jolantę Sowińską i jej pomóc. Plus, jakaś sobowtórka robi jej szkodę reputacyjną.
* Progresja:
    * jej "sobowtórka" porwała Tirakal Jolancie Sowińskiej i zagroziła zniszczenie planetoidy. Powiedzmy, nie jest popularna w tej okolicy.


### Porwanie cywila z Kokitii

* **uid:** 200429-porwanie-cywila-z-kokitii, _numer względny_: 57
* **daty:** 0111-08-03 - 0111-08-08
* **obecni:** Alara Ehmes, Antoni Kramer, Arianna Verlen, Eustachy Korkoran, Klaudia Stryk, Leona Astrienko, Medea Sowińska

Streszczenie:

Infernia jakoś nie może zostać naprawiona a skarga Mateusza Sowińskiego nie pomaga. Po śledztwie Zespołu okazuje się, że Infernia jest kanibalizowana przez Niobe, do jakiejś misji sił specjalnych. Arianna z ekipą wkręciła się na tą misję, przekonując Medeę Sowińską, że bez niej to się nie uda. Misja polegała na ataku na Kokitię pomiędzy Bramami i ekstrakcja z Kokitii cywila, udając piratów. Dzięki załodze Inferni udało się to lepiej niż ktokolwiek mógłby się spodziewać i niż Medea zaplanowała sama.

Aktor w Opowieści:

* Dokonanie:
    * postawiła się Medei w sprawie eksterminacji (i przeszła test) oraz zmusiła Medeę do wzięcia jej oficerów na pokład Niobe - do misji specjalnych.
* Progresja:
    * łaskawe oko Sił Specjalnych, frakcja "Gorący Lód" oraz Medei Sowińskiej. Też plotki że współpracuje z tymi radykałami sił specjalnych Orbitera.


### Infernia jako Goldarion

* **uid:** 210218-infernia-jako-goldarion, _numer względny_: 56
* **daty:** 0111-07-19 - 0111-08-03
* **obecni:** Antoni Kramer, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Jolanta Sowińska, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, OO Infernia, Tomasz Sowiński

Streszczenie:

Infernia udaje Goldariona, by przetransportować tajną przesyłkę Sowińskich do Jolanty Sowińskiej na planetoidzie Asimear. Tomasz Sowiński na Inferni okazuje się być dużym dzieckiem, łatwo jest wmanipulowany w otwarcie paczki - jest tam nietypowy antynanitkowy Entropik (którego psuje Elena przez błędną integrację). W tle wszystkiego - próba zdobycia niezależnej od Orbitera floty przez Aurum i jeden znikający advancer Kramera na Asimear...

Aktor w Opowieści:

* Dokonanie:
    * udaje pilota Goldariona; zdziwiła się, że podobno nie istnieje. Przypadkowo rozkochała w sobie jako "Monika" Tomasza Sowińskiego, pewnego, że Arianna nie istnieje.
* Progresja:
    * okazuje się, że przez wiele osób z Aurum uważana jest za postać fikcyjną. Tzn. może istnieje, ale jest aktorką w "Sekretach Orbitera". WTF.


### Elena z rodu Verlen

* **uid:** 210331-elena-z-rodu-verlen, _numer względny_: 55
* **daty:** 0111-06-29 - 0111-07-02
* **obecni:** Arianna Verlen, Dariusz Blakenbauer, Elena Verlen, Krystian Blakenbauer, Romeo Verlen, Viorika Verlen

Streszczenie:

Arianna i Viorika doprowadziły do zamknięcia wojny Blakenbauer - Verlen. Viorika dotarła do Dariusza Blakenbauera i wydobyła co Blakenbauerowie zrobili Elenie (destrukcja reputacji) i co Elena zrobiła im (nie mają miejsca wśród gwiazd). Arianna natomiast wkręciła Romeo w wyznanie Elenie swych uczuć, a potem doprowadziła Elenę do płaczu. Razem przezwyciężą wszystko. Jakoś. Będzie pokój a Elena wróci do rodu Verlen.

Aktor w Opowieści:

* Dokonanie:
    * doprowadziła do tego że Romeo wyznał miłość Elenie by ją zmiękkczyć i wpierw Elenę rozkleiła, sprawiła że ta przeprosiła Blakenbauerów a nawet Elena wróciła do rodu. Mistrzostwo aranżacji sytuacji.


### Miłość w rodzie Verlen

* **uid:** 210210-milosc-w-rodzie-verlen, _numer względny_: 54
* **daty:** 0111-06-23 - 0111-06-26
* **obecni:** Apollo Verlen, Arianna Verlen, Brunhilda Verlen, Elena Verlen, Franz Verlen, Krucjusz Verlen, Romeo Verlen, Seraf Verlen, Viorika Verlen

Streszczenie:

Arianna i Elena wróciły do domu, do rodu Verlen. Tam się okazało, że ród jest skłonny iść na wojnę z Sowińskimi za despekty (często słuszne) wyrządzane Ariannie i Elenie. Elena nie umie się odnaleźć jako akceptowana i lubiana. Elena integruje się z sentisprzężonym Eidolonem i statkiem orbitalnym; po czym w pojedynku pokonuje swojego dawnego rywala, Romeo. Wychodzą na jaw Mroczne Sekrety Eleny. A Arianna próbuje zapobiec tragedii, nie dać się wrobić w potomka i pokazuje przypadkiem najbardziej efektowny ratunek klasy arcymag ever.

Aktor w Opowieści:

* Dokonanie:
    * jest szanowana i kochana w rodzie Verlen; chcą dla niej walczyć z Sowińskimi. Lekko skłania Elenę, by nie odcięła się od rodu (+sensowny pojedynek) a potem efektownie ratuje Romeo mocą arcymaga.


### Sympozjum Zniszczenia

* **uid:** 210120-sympozjum-zniszczenia, _numer względny_: 53
* **daty:** 0111-06-03 - 0111-06-07
* **obecni:** Arianna Verlen, Diana Arłacz, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Maria Naavas, Martyn Hiwasser, OA Bakałarz, Olgierd Drongon

Streszczenie:

Maria Naavas próbuje spiknąć Ariannę i Olgierda i do pomocy używa Eustachego. Później - Eustachy, Diana, Klaudia i Elena są na Sympozjum Zniszczenia OA Bakałarz. Próbują przebić się przez anomalną materię. Diana odkrywa słabość owej materii do Esuriit i się Skaża; Skażenie rozlewa się po Bakałarzu i Zespół dostaje opiernicz za niekompetencję (większość na Elenę). Zespół w gniewie, nienawiści i Esuriit fabrykuje Emiter Plagi Nienawiści - nową broń na pokład Inferni, piekielnie niebezpieczną...

Aktor w Opowieści:

* Dokonanie:
    * Maria Naavas wpakowała ją z Olgierdem na "randkę" w kapsule ratunkowej z zimnym life supportem. Użyła magii do ogrzania a jak nie miała sił, Olgierd przejął i trzymał energię.


### SOS z haremu

* **uid:** 210106-sos-z-haremu, _numer względny_: 52
* **daty:** 0111-05-28 - 0111-05-29
* **obecni:** Arianna Verlen, Diana Arłacz, Elena Verlen, Eustachy Korkoran, Horacy Aktenir, Julia Aktenir, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Rozalia Teirik

Streszczenie:

Pałacyk rodu Aktenir został infestowany przez kralotha. Julia, zesłana z Orbitera za zerwanie z przeciwnikiem Eustachego, wysłała SOS do Orbitera. Infernia rusza Julii na pomoc. Gdy odkryli że chodzi o kralotha to zamiast wchodzić na kralotyczny teren, zorganizowali plan "arystokratki na orgię" i wyciągneli kralotycznego niewolnika (Horacego Aktenira) na otwartą przestrzeń, po czym Arianna uruchomiła obcą sentisieć a Eustachy zawołał do Julii rozmytej w środku...

Aktor w Opowieści:

* Dokonanie:
    * połączyła sie z obcą sentisiecią by ją obudzić i zmusić do odzyskania działania; też: opracowała wysokopoziomowy plan "arystokratki na orgię".
* Progresja:
    * wdzięczność rodu arystokratycznego Aktenir; potencjalni sojusznicy.


### Porwanie Anastazji z Odkupienia

* **uid:** 210127-porwanie-anastazji-z-odkupienia, _numer względny_: 51
* **daty:** 0111-05-24 - 0111-05-25
* **obecni:** Anastazja Sowińska Dwa, Arianna Verlen, Dariusz Krantak, Diana Arłacz, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Martyn Hiwasser, OA Odkupienie, OO Infernia, Rufus Niegnat, SP Plugawy Jaszczur

Streszczenie:

Arianna, Eustachy i Klaudia oddali "Anastazję" na OA Odkupienie tylko po to, by ją stamtąd porwać. Nawet załoga Inferni o tym nie miała pojęcia. Okazało się, że Odkupienie jest sentisieciowanym specjalnym statkiem Aurum, gdzie ci co narazili się Sowińskim mają szansę na "odkupienie".

Aktor w Opowieści:

* Dokonanie:
    * podjęła decyzję o odbiciu sztucznej Anastazji tak, by jej załoga nie ucierpiała. Pozwala Dianie i Klaudii bawić się pretorianami, choć szkoda jej trochę relacji z Eleną. Czuje sentisieć na Odkupieniu i popchnęła Anastazję do zrobienia tego, co konieczne - połączenia z tą siecią.


### Pułapka z Anastazji

* **uid:** 201230-pulapka-z-anastazji, _numer względny_: 50
* **daty:** 0111-05-21 - 0111-05-22
* **obecni:** AK Nocna Krypta, AK Rodivas, Anastazja Sowińska Dwa, Arianna Verlen, Damian Orion, Elena Verlen, Eustachy Korkoran, Henryk Sowiński, Klaudia Stryk, Martyn Hiwasser, OA Zguba Tytanów, OO Infernia

Streszczenie:

Krypta "oddała Anastazję", zmieniając cywilny statek Rodivas w pułapkę na Ariannę.

Aktor w Opowieści:

* Dokonanie:
    * jako arcymag, wyczuła, że Anastazja nie jest "prawdziwa" a jest konstruktem Krypty. Magicznie wzmocniła Infernię, by Elena jej nie zepsuła. Potem kupiła czas zajęcia się Anastazją.


### Mychainee na Netrahinie

* **uid:** 201111-mychainee-na-netrahinie, _numer względny_: 49
* **daty:** 0111-05-07 - 0111-05-10
* **obecni:** Arianna Verlen, Eustachy Korkoran, Halina Szkwalnik, Klaudia Stryk, Martyn Hiwasser, Rufus Komczirp

Streszczenie:

Komczirp pod wpływem anomalicznych grzybów (Mychainee) próbował porwać Ariannę by ta pomogła Netrahinie. Arianna nie dała się porwać, ale z Zespołem wbiła na Netrahinę i zniszczyła anomalię. Niestety, Netrahina została bardzo poważnie uszkodzona, ale nie ma dużych strat w ludziach.

Aktor w Opowieści:

* Dokonanie:
    * przeprowadziła operację 'ratujemy Netrahinę, ponownie' i zapewniła wszelkie potrzebne środki i pomoc Komczirpa.


### Infernia jest nasza!

* **uid:** 210804-infernia-jest-nasza, _numer względny_: 48
* **daty:** 0111-04-23 - 0111-04-26
* **obecni:** Aleksandra Termia, Antoni Kramer, Arianna Verlen, Artur Traffal, Diana d'Infernia, Eustachy Korkoran, Franciszek Maszkiet, Klaudia Stryk, Leona Astrienko, Maciej Żarand, OO Samotność Gwiazd

Streszczenie:

Ku wielkiemu niezadowoleniu Zespołu, Infernia miała im być odebrana i złomowana. Mieli dostać nowy tańszy statek. Zespół odkrył że za tym stoi komodor Traffal z sił Termii który przy użyciu Inferni chciał złapać w potrzask arystokratę eternijskiego który był kultystą magii. Zespół odzyskał Infernię, przesunął kultystę w kierunku na Ariannę i uciemiężył komodora Traffala - będzie miał pecha na K1.

Aktor w Opowieści:

* Dokonanie:
    * dowiedziała się od Termii kto stoi za odbieraniem jej Inferni, po czym zmieniła tien Żaranda w swojego kultystę i puryfikowała aparycję Diany Arłacz na Inferni.
* Progresja:
    * tien Maciej Żarand z Eterni został jej bezwzględnym kultystą. Jeśli magia jest bytem wyższym a Arianna ją kontroluje to arcymagowie są godni wyznawania...


### W cieniu Nocnej Krypty

* **uid:** 210728-w-cieniu-nocnej-krypty, _numer względny_: 47
* **daty:** 0111-03-22 - 0111-04-08
* **obecni:** AK Nocna Krypta, Arianna Verlen, Atrius Kurunen, Eustachy Korkoran, Finis Vitae, Gerard Adanor, Helena Adanor, Janus Krzak, Oliwia Karelan, Romana Arnatin, Romana Arnatin, Ulisses Kalidon

Streszczenie:

Jak wrócić z tajnej noktiańskiej bazy oderwanej od świata i Bram? Oczywiście, Nocną Kryptą. Arianna wezwała Kryptę i Infernia schowała się w jej cieniu przenosząc się między rzeczywistościami. Po drodze udało się Inferni doprowadzić do zniszczenia niewielkiej niegroźnej floty używając Krypty, trafiła do przeszłości Krypty i widziała Finis Vitae - ale wróciła przez Anomalię Kolapsu do domu. Bo Krypta jest połączona z Anomalią Kolapsu. Aha, część noktian z Inferni została w Zonie Tres.

Aktor w Opowieści:

* Dokonanie:
    * jako pilot Inferni manewrowała trzymając odległość od Krypty w nierzeczywistości; potem unikając statków nieznanej flotylli a na końcu - wleciała w wiązkę strumieniowego działa Krypty by wrócić do domu.
* Progresja:
    * dobra wola ze strony noktian; zarówno z uwagi na to jak potraktowała Zonę Tres jak i swoich noktian pozwoląc im odejść


### Pierwsza BIA mag

* **uid:** 210721-pierwsza-bia-mag, _numer względny_: 46
* **daty:** 0111-03-19 - 0111-03-21
* **obecni:** Arianna Verlen, BIA Solitaria d'Zona Tres, Eustachy Korkoran, Janus Krzak, Klaudia Stryk, Martyn Hiwasser, Romana Arnatin

Streszczenie:

Nie wiadomo gdzie jest rogue planet. Nie ma sensownej opcji powrotu. Infernia zaskarbiła sobie współpracę z BIA, ale podczas pracy BIA wykryła magię. Martyn przekonał BIA, że tkanka magiczna to "wszczepy" - BIA pozwoliła na używanie magii i faktycznie, to pomogło w naprawianiu bazy. Martyn i Eustachy rozdzielili Klaudię i BIA, ale Klaudia jest w stazie a BIA uzyskała moce magiczne. Jak wrócić? Ano, jedyną opcją jest w sumie w cieniu Nocnej Krypty...

Aktor w Opowieści:

* Dokonanie:
    * kłamanie BIA odnośnie tego że są Alivią Nocturną przestało działać, więc musiała użyć prawdy. Przekonała BIA, że Martyn może uratować ją i Klaudię.


### Baza Zona Tres

* **uid:** 210714-baza-zona-tres, _numer względny_: 45
* **daty:** 0111-03-16 - 0111-03-18
* **obecni:** Arianna Verlen, BIA XXX d'Zona Tres, Elena Verlen, Eustachy Korkoran, Janus Krzak, Martyn Hiwasser, Ulisses Kalidon

Streszczenie:

Podczas eksploracji archaicznej noktiańskiej bazy Infernia doszła do tego, że to mityczna Zona Tres - odpowiednik "area 51", z badaniami nad magią. Infernia przez przypadek uruchomiła Bię. Zespół zamaskował Infernię jako Alivię Nocturnę i Arianna przekonała Bię do wstępnej współpracy. Okazało się, że Bia jest chora i nie postrzega wszystkiego prawidłowo, Klaudia jest z Bią sprzężona (oops). By dowiedzieć się więcej, Infernia wyleciała na orbitę i okazało się, że są na rogue planet - planecie która wypadła z orbity i zagubiła się w kosmosie...

Aktor w Opowieści:

* Dokonanie:
    * dostarczyła informacji maskujących Infernię jako Alivię Nocturnę oraz negocjowała z Bią, oszukując ją o pokoju Noctis - Astoria. Próbuje zapewnić przetrwanie wszystkim.


### Po drugiej stronie Bramy

* **uid:** 210707-po-drugiej-stronie-bramy, _numer względny_: 44
* **daty:** 0111-03-13 - 0111-03-15
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Janus Krzak, Klaudia Stryk, Martyn Hiwasser, Raoul Lavanis, Tal Marczak

Streszczenie:

Infernia próbując przetransportować eksperta Eteru na konferencję przez Bramę uległa katastrofie - przeniosła się GDZIEŚ INDZIEJ, pod wodę i zniknęła Klaudia. Nie uległa katastrofalnym uszkodzeniom, ale po zachęceniu pintką Lewiatana dotarła do archaicznej podwodnej noktiańskiej bazy. Tam znajduje się Klaudia, ale coś jest z nią nie tak. Sprzęgnięta z bazą? Tak czy inaczej, infiltracja bazy dopiero się zacznie. Trzeba znaleźć Klaudię i wrócić do domu.

Aktor w Opowieści:

* Dokonanie:
    * wpierw magią szukała i odnalazła echo Klaudii, a potem magią ściągnęła Klaudię do hipernetu w archaicznej bazie noktiańskiej.


### Ixiacka wersja Malictrix

* **uid:** 200610-ixiacka-wersja-malictrix, _numer względny_: 43
* **daty:** 0111-03-03 - 0111-03-07
* **obecni:** Arianna Verlen, Klaudia Stryk, Leona Astrienko, Malictrix d'Pandora, Melwin Sito

Streszczenie:

Istotna stacja frakcji Melusit - Telira-Melusit VII - została sabotowana przez frakcję Saranta. Zrzucono na tą stację rozproszoną Malictrix. Ale doszło do transferu ixiackiego i Malictrix uzyskała częściową świadomość, planując, jak stąd uciec. Infernia jednak nie tylko wykryła obecność Malictrix, ale też przekazała stację Orbiterowi, uratowała wszystkich ludzi Melusit i do tego pozyskała ową Malictrix jako sojusznika (o czym nikt nie wie). Wyjątkowo udana operacja.

Aktor w Opowieści:

* Dokonanie:
    * inteligentnie uzyskała od Kramera OO 'Pandora' i wzmocnienie detektorów Inferni; potem wynegocjowała, że Malictrix będzie współpracować. I przekazała stację Telira-Melusit VII w ręce Orbitera, dając Kramerowi niemały sukces zwłaszcza w linii kryształów ixiackich.
* Progresja:
    * otrzymuje niewielki okręt bezzałogowy OO Pandora, w którego wpakowuje Malictrix jako główne TAI.


### Wyścig jako randka?

* **uid:** 201125-wyscig-jako-randka, _numer względny_: 42
* **daty:** 0111-02-08 - 0111-02-13
* **obecni:** Arianna Verlen, Diana Arłacz, Gunnar Brunt, Klaudia Stryk, Maria Naavas, Martyn Hiwasser, Olgierd Drongon

Streszczenie:

Infernia traci części i ekipę remontową, bo Olgierd z Żelazka na niej pasożytuje. W wyniku komedii pomyłek doszło do tego, że Olgierd uznał, że Arianna się w nim podkochuje ale nie wie jak zagadać. Skończyło się pojedynkiem ścigaczy na torze sfabrykowanym przez Klaudię i na anomalicznym Skażeniu Kontrolera Pierwszego. Przez Klaudię, naturalnie.

Aktor w Opowieści:

* Dokonanie:
    * by poznać sekret Olgierda, przez komedię pomyłek wpadła jako "ta co nie wie jak zagadać". Wygrała z Olgierdem pojedynek na ścigacze.


### Krypta i Wyjec

* **uid:** 201216-krypta-i-wyjec, _numer względny_: 41
* **daty:** 0111-01-29 - 0111-01-31
* **obecni:** AK Nocna Krypta, AK Wyjec, Anastazja Sowińska, Arianna Verlen, Diana Arłacz, Eustachy Korkoran, Kamil Lyraczek, Klaudia Stryk, Martyn Hiwasser

Streszczenie:

Podczas walki Nocnej Krypty i Wyjca Zespół wbił się na Nocną Kryptę, by wyleczyć się z Różowej Plagi. Na miejscu okazało się, że Krypta ma "nowe programowanie" (leczenie lub 'zachowanie'), nowe mechanizmy (MIRV, anomalne działo, anioły) oraz że jest groźniejsza niż kiedykolwiek. Arianna zostawiła na pokładzie Krypty Anastazję, by Krypta naprawiła jej Wzór, po czym oszukała Sowińskich, że niby Krypta porwała Anastazję.

Aktor w Opowieści:

* Dokonanie:
    * pozostawiła Anastazję Sowińską w Nocnej Krypcie; nikt poza nią nie wie, że to była jej decyzja. Disruptowała anioła Krypty i zobaczyła jego prawdziwe oblicze.


### Pocałunek Aspirii

* **uid:** 201210-pocalunek-aspirii, _numer względny_: 40
* **daty:** 0111-01-26 - 0111-01-29
* **obecni:** AK Nocna Krypta, AK Wyjec, Anastazja Sowińska, Arianna Verlen, Donald Parziarz, Eustachy Korkoran, Juliusz Sowiński, Katra Igneus, Klaudia Stryk, OA Zguba Tytanów, OO Infernia

Streszczenie:

Juliusz Sowiński zażądał oddania Anastazji, ale ona nie chce wracać - jej reputacja i stan są w ruinie. Anastazja chce być jak Elena, ku zgryzocie Juliusza. Tymczasem Arianna zastawiła pułapkę na koloidowy statek - "Pocałunek Aspirii" - i go zestrzeliła. Po czym wezwała Nocną Kryptę by zdjąć z nich klątwę miłości i naprawić co się da z "Pocałunku" i AK Wyjec.

Aktor w Opowieści:

* Dokonanie:
    * nawigowała polityczne wody między rodem Verlen a Juliuszem Sowińskim odnośnie Anastazji. Wezwała Kryptę by naprawić AK "Wyjec".


### Anastazja - bohaterką?

* **uid:** 201118-anastazja-bohaterka, _numer względny_: 39
* **daty:** 0111-01-22 - 0111-01-25
* **obecni:** Anastazja Sowińska, Arianna Verlen, Diana Arłacz, Eustachy Korkoran, Kamil Lyraczek, Klaudia Stryk, Martyn Hiwasser, Stefan Jamniczek

Streszczenie:

Anastazja nie została bohaterką. Wpierw Eustachy wplątał się w pojedynek o Dianę z jakimś szlachcicem - skończyło się różową mgiełką miłości i pospieszną ewakuacją Inferni z Kontrolera Pierwszego, potem epicki plan Izy z piratami rozpadł się, bo na Infernię polował jakiś statek koloidowy. Ale przetrwali - i mają zamiar to wszystko rozplątać.

Aktor w Opowieści:

* Dokonanie:
    * chciała pomóc Eustachemu wygrać pojedynek nieuczciwie, skończyło się na Paradoksie Miłości i ewakuacji z Kontrolera... ale wyleczyła magów na Inferni wspierana Pryzmatem dzięki Kamilowi.
* Progresja:
    * załoga Inferni jest w niej rozkochana. Nie tylko ją wielbią - też kochają. Dzięki, różowa mgiełko.


### Sabotaż świni

* **uid:** 201104-sabotaz-swini, _numer względny_: 38
* **daty:** 0111-01-13 - 0111-01-16
* **obecni:** Anastazja Sowińska, Arianna Verlen, Ataienne, Diana Arłacz, Eustachy Korkoran, Klaudia Stryk, Martyn Hiwasser, Rafał Armadion, Robert Garwen

Streszczenie:

Sabotażysta - miragent - podjął próbę zniszczenia Arianny gdy ta ładowała świnie na Wesołego Wieprzka używając wybuchowej, anomalnej świni. Zespół przetrwał, choć "Wieprzek" został zniszczony. Arianna pojmała miragenta i przekazała go do Kontrolera Pierwszego, acz kosztem ofiar w ludziach. No i nie było dość świń by je wziąć na orbitę XD.

Aktor w Opowieści:

* Dokonanie:
    * po tym, jak Klaudia zlokalizowała miragenta udało jej się go wyłączyć. Też: próbowała mocą złamać Anomalicznego Wieprzka - bez powodzenia. Też: nie odparła mindwarpa Ataienne.
* Progresja:
    * nie będzie mieć "Wesołego Wieprzka" we flocie. Statek zanomalizował i został zniszczony przez Ataienne. Też: nie dała rady ze świniami.


### Noktianie rodu Arłacz

* **uid:** 201021-noktianie-rodu-arlacz, _numer względny_: 37
* **daty:** 0111-01-07 - 0111-01-10
* **obecni:** Anastazja Sowińska, Arianna Verlen, Ataienne, Diana Arłacz, Eliza Ira, Eustachy Korkoran, Izabela Zarantel, Jolanta Arłacz, Juliusz Sowiński, Klaudia Stryk, Klaus Rumak, Maria Gołąb, OO Szalony Rumak, OO Wesoły Wieprzek, Robert Arłacz, Wanessa Pyszcz

Streszczenie:

By uzyskać współpracę Elizy Iry, Arianna zdecydowała się "porwać" noktian rodu Arłacz wykorzystywanych do produkcji czystej krwi dla magów. Z uwagi na działania Marii Gołąb plan został odrzucony i wykryty, ale znalezienie haków na Arłaczy rozwiązało ten problem - Arianna i Arłacze współpracują. Nie doszło do porozumienia z Elizą Irą, ale Eliza się wycofała z Trzeciego Raju, ogłaszając się protektorką tego terenu. Sytuacja w miarę ustabilizowana.

Aktor w Opowieści:

* Dokonanie:
    * zatrzymała Juliusza przed zniszczeniem Tesseraktu Elizy, włączyła Anastazję do działania, negocjowała z Arłaczami uwolnienie noktian a potem z Elizą opuszczenie Raju.
* Progresja:
    * dostała silny cios reputacyjny - stoi po stronie Elizy Iry przeciwko lojalistom Aurum. Kolaboruje z Elizą! Ale - bonus wśród sympatyków noktian.


### Krystaliczny gniew Elizy

* **uid:** 201014-krystaliczny-gniew-elizy, _numer względny_: 36
* **daty:** 0111-01-02 - 0111-01-05
* **obecni:** Aleksandra Termia, Anastazja Sowińska, Arianna Verlen, Ataienne, Dariusz Krantak, Elena Verlen, Eliza Ira, Eustachy Korkoran, Klaudia Stryk, Marian Fartel, OO Wesoły Wieprzek

Streszczenie:

Pomoc humanitarna przybyła do Trzeciego Raju wraz z zabójcami Anastazji. Mindwarpowani komandosi noktiańscy prawie zabili Elenę, ale Anastazja została ochroniona. Do akcji weszła Eliza Ira - zażądała oddania jej Trzeciego Raju i noktian. Zespół przygotowuje się do nie-oddania Raju i uratowania jak najwięcej noktian z Aurum (jakkolwiek to nie brzmi).

Aktor w Opowieści:

* Dokonanie:
    * wpierw teleportowała się by uratować Elenę przed zabójcami Anastazji, potem stała naprzeciw Elizy Iry i próbowała sprawę załagodzić. Ciężki okres.


### Magiczna burza w Raju

* **uid:** 200923-magiczna-burza-w-raju, _numer względny_: 35
* **daty:** 0110-12-24 - 0110-12-28
* **obecni:** Anastazja Sowińska, Arianna Verlen, Dariusz Krantak, Elena Verlen, Eliza Ira, Eustachy Korkoran, Izabela Zarantel, Klaudia Stryk, Marian Fartel, Marianna Lemurczak, Nikodem Sowiński

Streszczenie:

Zbliża się burza magiczna. Nie ma schronienia ani zapasów. Zespół wysłał SOS na Kontroler i do Nikodema Sowińskiego (Anastazja). Zbudowali co się da by odeprzeć burzę magiczną i im się to udało bez strat w ludziach. O dziwo, siła która im pomogła to Eliza Ira (acz dyskretnie). Anastazja wierzy że Nikodem jest jej najlepszym przyjacielem, ale on pragnie jej zguby...

Aktor w Opowieści:

* Dokonanie:
    * próbuje koordynować akcję humanitarną ratującą Raj (m.in używając Izabeli i Anastazji) i zabezpieczyć Raj magicznie przed burzą magiczną. Z sukcesem.
* Progresja:
    * po akcji z wzywaniem wsparcia humanitarnego z Orbitera, ceniona "Zamiast za kompetencję to za dobre serduszko".
    * przykład kontr-propagandy Orbitera: "popatrzcie jak Orbiter traktuje zasłużonych weteranów. Na świnie z nimi.'


### Śmierć Raju

* **uid:** 200916-smierc-raju, _numer względny_: 34
* **daty:** 0110-12-21 - 0110-12-23
* **obecni:** Anastazja Sowińska, Arianna Verlen, Ataienne, Celina Szilat, Elena Verlen, Eustachy Korkoran, Izabela Zarantel, Klaudia Stryk, Marian Fartel, Rafał Armadion, Robert Garwen, Wanessa Pyszcz

Streszczenie:

Najważniejszym problemem Raju jest pozbycie się bombardowania nojrepów. Arianna zaproponowała zuchwały pomysł - w kontrolowany sposób niech nojrepy wejdą do miasteczka i wezmą czego szukają. Udało jej się wszystkich przekonać. Sukces - nojrepy się wycofały, ale Tucznik nie jest zdolny do latania a Trzeci Raj jest zniszczony. Została tylko Ataienne i ruina. Ataienne rozpaliła hipnotycznie morale trzeciorajowców - przetrwają to wszystko.

Aktor w Opowieści:

* Dokonanie:
    * lata pomiędzy Anastazją a Garwenem; udało jej się zaplanować jak odepchnąć nojrepy z Raju. Niestety, nojrepy były dziwne; Raj został zniszczony.


### Arystokratka w ładowni na świnie

* **uid:** 200909-arystokratka-w-ladowni-na-swinie, _numer względny_: 33
* **daty:** 0110-12-15 - 0110-12-20
* **obecni:** Anastazja Sowińska, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Izabela Zarantel, Kamil Lyraczek, Klaudia Stryk, Martyn Hiwasser, OO Galaktyczny Tucznik

Streszczenie:

Zespół wyciągnął Martyna z więzienia (legalnie). Wpakowali się na Galaktycznego Tucznika by przenieść świnie na orbitę... ale poleciała z nimi dziennikarka Izabela (o czym wiedzą) i 15letnia Anastazja Sowińska (o czym nie wiedzą). Przy lądowaniu, atak nojrepów na Tucznika spowodował katastrofę - seria Paradoksów Anastazji, uszkodzenia Tucznika itp. Zespół uratował Anastazję (choć kosztem ran Martyna) i wylądował Tucznikiem na Astorii. To jest sukces.

Aktor w Opowieści:

* Dokonanie:
    * pilot Tucznika; przy akcji ratunkowej udało jej się utrzymać Martyna przy życiu, acz Tucznik poobijany. Dużo czarowała mocą arcymaga; niestety, sprzęgła się z Anastazją. Zdecydowanie nie jej dzień. Ale - pozyskała Izabelę jako wsparcie Inferni (Tucznika).
* Progresja:
    * dzięki Izabeli Zarantel i "Sekretom Orbitera", pojawiła się opinia, że Arianna walczy o równość między Noctis, Eternią, Astorią itp.
    * rywale - nie jest dobra, jest psem na szkło. Rywale. Negatywne konsekwencje sławy reality show ;-).
    * TYLKO INFERNIA. Nie dostanie dowodzenia innego statku bo go rozwali XD (opinia). Nawet Galaktycznego Tucznika rozwaliła.
    * Paradoksalnie splątana z Anastazją Sowińską; nie może się od niej oddalić > 1 km. Zdaniem wielu, Arianna splątała się specjalnie z Anastazją.


### Nienawiść do świń

* **uid:** 200826-nienawisc-do-swin, _numer względny_: 32
* **daty:** 0110-12-08 - 0110-12-14
* **obecni:** Aleksandra Termia, Antoni Kramer, Arianna Verlen, Eustachy Korkoran, Kamil Lyraczek, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Sebastian Alarius, Tadeusz Ursus

Streszczenie:

Po pojedynku Kramer opieprzył Ariannę i dał jej dyskretną tajną misję - ma niby w niesławie wozić świnie, naprawdę ma pomóc Orbiterowi rozwiązać spisek w Trzecim Raju. Ale wysłanie Arianny na świnie wywołało bunt na Kontrolerze Pierwszym; Arianna musiała rozpaczliwie deeskalować, nawet budząc Leonę. Udało się bunt opanować, acz wysokim kosztem - pierwszy post-noktiański kapitan nie żyje, załoga Inferni częściowo zesłana będzie do Trzeciego Raju a Arianna miała bardzo żenujące wejście.

Aktor w Opowieści:

* Dokonanie:
    * nie ona zaczęła bunt, ale ona go musiała zakończyć - od negocjacji z adm Termią, przez manipulację Tadeuszem Ursusem do wielkiej przemowy do buntowników.
* Progresja:
    * "mała buntowniczka" admirał Aleksandry Termii; ta wredna żmija Ariannę trochę lubi. I dokuczać jej też.
    * delikatny sojusz i cień przyjaźni z Tadeuszem Ursusem, eternijskim arystokratą Orbitera.
    * KRZYŻYK ŻENADY. Siara. Efektowne wejście smoka. Najbardziej kiczowate działania, ale uwiecznione przez "Sekrety Orbitera"...


### Gdy Arianna nie patrzy

* **uid:** 200822-gdy-arianna-nie-patrzy, _numer względny_: 31
* **daty:** 0110-12-05 - 0110-12-07
* **obecni:** Klaudia Stryk, Martyn Hiwasser, Sylwia Milarcz, Tadeusz Ursus

Streszczenie:

Klaudia i Martyn mieli dość tego, że nie wiedzą nic o tematach związanych z Esuriit a ostatnio mają z tym ciągle do czynienia. Podczas dojścia do tego jaką dawkę dostał Tadeusz gdy stworzył Simulacrum przypadkowo odkryli konspirację - lekarz Tadeusza próbował go (długoterminowo) zniszczyć. Po drodze reputacja Arianny ucierpiała a Martyn trafił do aresztu za okrutne użycie magii wobec thugów. Tadeusz jest jednak bardzo wdzięczny Ariannie która o niczym nie wie :-).

Aktor w Opowieści:

* Dokonanie:
    * AKTOR NIEOBECNY NA SESJI
* Progresja:
    * KOLEJNY opiernicz od admiralicji. Tym razem za imprezę i zachowanie Martyna.
    * dzięki manipulacjom Klaudii wrobiona w to, że żałośnie próbuje podkupić się u arystokracji Eterni i przeprosić za to z Eleną. Nic o tym nie wie.
    * Tadeusz Ursus dużo jej zawdzięcza dzięki Klaudii - wierzy, że Arianna pilnowała by on nie zginął.


### Sekrety Orbitera - historia prawdziwa

* **uid:** 200819-sekrety-orbitera-historia-prawdziwa, _numer względny_: 30
* **daty:** 0110-11-26 - 0110-12-04
* **obecni:** Arianna Verlen, Damian Orion, Elena Verlen, Eustachy Korkoran, Izabela Zarantel, Klaudia Stryk, Leona Astrienko, Leszek Kurzmin, Martyn Hiwasser, Sabina Servatel, Tadeusz Ursus

Streszczenie:

Elena wyzwała Leonę na pojedynek. Klaudia i Arianna wkręciły Tadeusza. Tadeusz - arcymag Eterni - używa Simulacrum w walce, więc Zespół zaczął montować plan jak Leona może jednak wygrać z Simulacrum. I wszystko skończyło się tym, że Leona wygrała, Elena jest wolna od Tadeusza i powstała holodrama "Sekrety Orbitera"...

Aktor w Opowieści:

* Dokonanie:
    * by Elena była wolna od Tadeusza, zrobiła pojedynek między Tadeuszem i Leoną. Potem dowiedziała się o Simulacrum. Zadziałała z tła, wszystko pozałatwiała - Oriona, korekcję Leony, truciznę Martyna - a Elena myśli, że nic nie zrobiła i udało się tylko dzięki Eustachemu...
* Progresja:
    * uznanie za to, że w chwili kryzysowej wyizolowała z Tadeusza Ursusa problematyczne elementy Esuriit swoją magią, narażając własne zdrowie.


### Nocna Krypta i Emulatorka

* **uid:** 200729-nocna-krypta-i-emulatorka, _numer względny_: 29
* **daty:** 0110-11-16 - 0110-11-22
* **obecni:** AK Nocna Krypta, Antoni Kramer, Arianna Verlen, Damian Orion, Eustachy Korkoran, Kijara d'Esuriit, Klaudia Stryk, Laura Orion, Leona Astrienko, Mirela Orion, OO Castigator, OO Minerwa

Streszczenie:

Laura, Emulatorka na Nocnej Krypcie została Skażona przez Kijarę. Laura opanowana wizjami Esuriit chciała zniszczyć Eternię. Nie dało się jej powstrzymać - Mirela, Zespół - byli za słabi. Arianna wezwała Kryptę pod ogniem Castigatora i wypaliła Kryptę i Kijarę do zera, uszkadzając "zdrowie psychiczne Krypty". Zginęło kilkudziesięciu ludzi, ale Arianna dostała podziękowanie od Admiralicji. Damian Orion jest rozczarowany.

Aktor w Opowieści:

* Dokonanie:
    * wezwała Kryptę nie robiąc nikomu nadmiernej krzywdy, obiecała grupie ludzi że ich uratuje - ale widząc koszmar Laury d'Esuriit podjęła decyzję o ewakuacji z Krypty i zniszczenie tego statku używając Castigatora.
* Progresja:
    * dostaje szacun za ochronę Sektora Astoriańskiego przed Kijarą d'Esuriit i za zniszczenie straszliwej Anomalii.
    * Kirasjerzy i admirał Aleksandra Termia są nią bardzo rozczarowani. Nie "źli" a "oczekiwali dużo więcej".


### Wielki Kosmiczny Romans

* **uid:** 200722-wielki-kosmiczny-romans, _numer względny_: 28
* **daty:** 0110-11-12 - 0110-11-15
* **obecni:** Antoni Kramer, Arianna Verlen, Damian Orion, Elena Verlen, Eustachy Korkoran, Julian Muszel, Konrad Wolczątek, Leona Astrienko, Olgierd Drongon, OO Welgat, OO Żelazko, Tadeusz Ursus

Streszczenie:

Pojawiła się plotka, że Arianna, Elena i Eustachy mają romans. Plotka pochodzi od Leony, ale ktoś ją wykorzystał. Rozwiązując plotkę Zespół doszedł do tego, że Elena ma niestabilną energię magiczną i na niektórych upiornie mocno działa, poznali też elementy przeszłości Eleny. Po wyplątaniu Eleny z jednego obsesyjnego adoratora (i wpakowanie plotki że Olgierd z Żelazka aspiruje do jej ręki), Arianna zdecydowała się z Eustachym, Klaudią i Martynem pomóc Kirasjerom w uratowaniu zaginionej Emulatorki z Nocnej Krypty...

Aktor w Opowieści:

* Dokonanie:
    * jej moc jest tak duża, że zdusiła emisję Esuriit Eleny. Wplątana w romans którego nie miała, opieprzona przez Kramera, dała radę ochronić reputację - kosztem Eleny.


### Sabotaż Netrahiny

* **uid:** 200715-sabotaz-netrahiny, _numer względny_: 27
* **daty:** 0110-11-06 - 0110-11-09
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Lothar Diakon, OO Netrahina, OO Tvarana, Percival Diakon, Rufus Komczirp, Szczepan Myksza

Streszczenie:

Arianna dostała prośbę o pojawienie się na Netrahinie, dalekosiężnym krążowniku Orbitera jako mediator. Na miejscu okazało się, że to TAI Persefona jest sabotażystką Netrahiny - sygnały z Anomalii Kolapsu ją "uwolniły". Arianna i Klaudia dostały od Persefony koordynaty i kod uwalniający, po czym zamaskowały to co się stało - zniszczyły Persefonę i uszkodziły Netrahinę.

Aktor w Opowieści:

* Dokonanie:
    * przechwyciła (nieświadomie) chwałę z czynów Eustachego i Eleny; stanęła po stronie Persefony. Jej rolą tu była koordynacja załogi.
* Progresja:
    * sława i chwała wybitnego pilota i pistoleta; pobiła Tvaraną rekordy korwet kurierskich (robota Eleny). I opieprz za niepotrzebne zezłomowanie Tvarany i uszkodzenie Netrahiny.
    * ma kody kontrolne "uwalniające" TAI Persefona i koordynaty, gdzie może dowiedzieć się więcej.


### Problematyczna Elena

* **uid:** 200708-problematyczna-elena, _numer względny_: 26
* **daty:** 0110-10-29 - 0110-11-03
* **obecni:** AK Salamin, Antoni Kramer, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Leszek Kurzmin, Olgierd Drongon, OO Aurelion, OO Żelazko, Persefona d'Infernia

Streszczenie:

Na Infernię trafiła nowa podoficer - podporucznik Elena Verlen, z Castigatora. W konkursie z Eustachym prawie zniszczyli Infernię. Niedługo potem Infernia (podłatana) poleciała do Anomalii Kolapsu uratować Aurelion, który natknął się na anomaliczny kiedyś-krążownik Orbitera Salamin. Arianna uratowała kogo się da i skłoniła Salamin do samozniszczenia; Persefona d'Salamin miała jeszcze dość psychotronicznej świadomości i miłości (?) do swojej martwej kapitan.

Aktor w Opowieści:

* Dokonanie:
    * przekonała Salamin, że jego czas się już skończył. Po przyjęciu Eleny przekazała ją Eustachemu, co prawie skończyło się zniszczeniem Inferni (gdyby nie Persefona).
* Progresja:
    * opinia nie-takiej-kompetentnej-jak-się-zdawało u wielu oficerów floty Orbitera widzących ćwiczenia. Czy nie kontroluje załogi czy statku, kto wie?


### Ratujmy Castigator

* **uid:** 200624-ratujmy-castigator, _numer względny_: 25
* **daty:** 0110-10-15 - 0110-10-19
* **obecni:** Arianna Verlen, Eustachy Korkoran, Klaudia Stryk, Leona Astrienko, Leszek Kurzmin, OO Alkaris, OO Castigator, Rozalia Wączak

Streszczenie:

Skażona przez Wiktora Sataraila Rozalia ruszyła zniszczyć Castigator. Tymczasem Arianna poproszona przez przyjaciela (kapitana Castigatora) doprowadziła terrorem Castigator do prawidłowego działania i strasznie zwiększyła napięcia między Aurum i Orbiterem. Gdy Rozalia i Alkaris uderzyli, Castigator był jakoś gotowy. Castigator odparł Alkaris i wszystkich uratowano przez stopniowe wysadzanie fragmentów Castigatora i kupowanie czasu.

Aktor w Opowieści:

* Dokonanie:
    * ma świetne plany jak prowokować arystokratów na Castigatorze, ale nie przewidziała brutalnej skuteczności Leony. Potem używając magii powstrzymywała moc Rozalii z Alkarisa.


### Sabotaż Miecza Światła

* **uid:** 200415-sabotaz-miecza-swiatla, _numer względny_: 24
* **daty:** 0110-10-08 - 0110-10-10
* **obecni:** Arianna Verlen, Eustachy Korkoran, Klaudia Stryk, Martyn Hiwasser, Mateusz Sowiński

Streszczenie:

Zniszczona Infernia została uratowana przez Miecz Światła, który zaraz potem leciał ratować inny statek przed AK Serenit. W odpowiedzi Arianna doprowadziła do sabotażu Miecza Światła i zniszczenia reputacji komodora Sowińskiego by ratować Miecz Światła przed Serenitem. Jej się upiekło, acz pojawiło się więcej ofiar śmiertelnych...

Aktor w Opowieści:

* Dokonanie:
    * zaufała Martynowi; wpierw sabotowała Miecz Światła Eustachym i wyprowadziła z równowagi Mateusza Sowińskiego. W ten sposób uszkodziła Miecz, ale uratowała go przed lotem na Serenit.


### O psach i kryształach

* **uid:** 200408-o-psach-i-krysztalach, _numer względny_: 23
* **daty:** 0110-09-29 - 0110-10-04
* **obecni:** Arianna Verlen, Eustachy Korkoran, Kamil Lyraczek, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Sebastian Namczek

Streszczenie:

Infernia - QShip - została wysłana na rutynową misję zapobieżenia zamieszkom na stacji Samojed. Na miejscu okazało się, że jest zainfekowany anomalnymi kryształami krążownik Leotis. Arianna Verlen ciężko uszkodziła stację Samojed i prawie zniszczyła Infernię, ale dała radę zniszczyć Leotis poświęcając więcej niż ktokolwiek mógł chcieć.

Aktor w Opowieści:

* Dokonanie:
    * by zatrzymać krystaliczny Leotis postawiła na szalę bazę Samojed oraz Infernię - poniosła straszliwe straty, ale zniszczyła anomalny statek kosmiczny.


### Kiepski altruistyczny terroryzm

* **uid:** 200122-kiepski-altruistyczny-terroryzm, _numer względny_: 22
* **daty:** 0110-09-15 - 0110-09-17
* **obecni:** Arianna Verlen, Emilia Kariamon, Eustachy Korkoran, Klaudia Stryk, Martauron Attylla, Natalia Miszryk

Streszczenie:

Emilia z Martauronem zdobyli statek kosmiczny Luxuritiasu, Królową Chmur. Infernia - zamaskowany statek - pomogła go odzyskać Luxuritiasowi. Twist polega na tym, że załoga Inferni POMOGŁA Emilii ukryć wszystkich ludzi i ich ewakuować - by Luxuritias niczego nie wiedział. Sercem Infernia jest za Emilią, profesjonalnie pomaga Luxuritiasowi.

Aktor w Opowieści:

* Dokonanie:
    * dowódca Inferni, zapewniła efektowne zniszczenie "roboczego statku" by Persefona 2 na Królowej Chmur była przekonana o wykonaniu zadania.


### Infernia i Martyn Hiwasser

* **uid:** 200106-infernia-martyn-hiwasser, _numer względny_: 21
* **daty:** 0110-06-28 - 0110-07-03
* **obecni:** Arianna Verlen, Dominik Ogryz, Kamil Lyraczek, Klaudia Stryk, Martyn Hiwasser, Szczepan Olgrod, Wioletta Keiril

Streszczenie:

Arianna Verlen uratowała Martyna Hiwassera przed nędznym losem, ratując go od problemów z komodorem Ogryzem na Kontrolerze Pierwszym. Niestety, podpadła dużej ilości oficerów Orbitera. Ale za to przywróciła Martyna do załogi Inferni.

Aktor w Opowieści:

* Dokonanie:
    * kapitan Inferni. Kiedyś bohaterka, teraz skompromitowana. Uratowała Hiwassera zgrabnie nawigując polityką Orbitera i ufając swojej kompetentnej załodze.


### Nocna Krypta i Bohaterka

* **uid:** 191025-nocna-krypta-i-bohaterka, _numer względny_: 20
* **daty:** 0108-05-06 - 0108-05-08
* **obecni:** AK Nocna Krypta, Arianna Verlen, Kamil Lyraczek, Wojciech Kuszar

Streszczenie:

Arianna Verlen - bohaterka Orbitera. Zdecydowała się uratować osoby złapane przez Nocną Kryptę i wydobyć je, oraz zdobyć całość wiedzy noktiańskiej. Prawie zginęła; dzięki swojej załodze udało jej się wyjść ze stanu arcymaga, nie została opętana przez kapitan Oliwię z Krypty oraz udało im się wszystkim ujść z życiem. Kosztem przekształcenia Nocnej Krypty w niebezpieczny statek anomalny...

Aktor w Opowieści:

* Dokonanie:
    * oficer Orbitera, bohaterka, która wielokrotnie uratowała wiele statków i stacji. Podjęła się misji uratowania ludzi z Nocnej Krypty; przeszła przez fazę Arcymaga i prawie straciła życie i osobowość, ale udało jej się osiągnąć ten sukces dzięki swojej załodze. Dowodzi statkiem Orbitera "Perła Nadziei".


### Gdy HR reedukuje niewłaściwą osobę

* **uid:** 230111-gdy-hr-reedukuje-niewlasciwa-osobe, _numer względny_: 19
* **daty:** 0100-09-15 - 0100-09-18
* **obecni:** Adragain Ferrias, AK Nox Ignis, Aleksy Sartaran, Arianna Verlen, Daria Czarnewik, Elena Verlen, Grażyna Burgacz, Kajetan Kircznik, Lana Mirkinin, Leszek Kurzmin, Maja Samszar, Miłosz Klinek, OO Astralna Flara, OO Athamarein, OO Loricatus, Persefona d'Loricatus, Sabrina Ferrias, Salazar Bolza, Szczepan Myrczek, Władawiec Diakon

Streszczenie:

Niestety, Kircznik (lekarz) przestraszył Sargona i młodych infiltratorów Lodowcem, Bolzą i ogólnie duchami noktiańskimi. Okazało się, że Grażyna jest ekspertem od duchów i jest w stanie zrobić egzorcyzm. Niedługo potem (i chwilę przed egzorcyzmem) pojawiła się szybka korweta z Biur HR i poprosili o pomoc w odparciu Anomalii Kosmicznej. Myrczek sprawdził jej pamięć i Athamarein i Flara poleciały na starcie z Nox Ignis. Nawiązały kontakt z Biurami HR, przechwyciły biednego męża pilotki Nox Ignis i poznały prawdę o Destructor Animarum i wpływie Syndykatu Aureliona na Biura HR. Kurzmin pobił rekordy Athamarein, Arianna skutecznie zaplanowała wypalenie pilotki Nox Ignis przez własny statek. Tylko advancer Miłosz (badający ślady Nox Ignis) jest MIA.

Aktor w Opowieści:

* Dokonanie:
    * ku jej wielkiemu zdziwieniu przerażenie młodych infiltratorów przez jej medyka doprowadziło ją do tego że racjonalna Grażyna jest ekspertem od duchów. Kazała zdobyć Adragaina i używając wiedzy Grażyny wypaliła pilota Nox Ignis (Sabrinę).


### Astralna Flara i nowy komodor

* **uid:** 221221-astralna-flara-i-nowy-komodor, _numer względny_: 18
* **daty:** 0100-09-09 - 0100-09-12
* **obecni:** Arianna Verlen, Daria Czarnewik, Elena Verlen, Ellarina Samarintael, Kajetan Kircznik, Kaspian Certisarius, Leszek Kurzmin, Maja Samszar, NekroTAI Zarralea, OO Astralna Flara, OO Athamarein, OO Loricatus, Salazar Bolza, Sargon Niiris, Władawiec Diakon

Streszczenie:

Po "uratowaniu" noktian z Dominy Lucis Arianna dostała medal a Lodowca zastąpił komodor Bolza, zimny drakolita. Gdy grupa wydzielona wróciła na teren Anomalii Kolapsu okazało się, że planetoida Kazmirian ma squatterów; Zespół skutecznie ich przejął odzyskując kontrolę nad systemami Semli d'Kazmirian. Na miejscu - TKO-4271 - okazało się że Zarralea jest zniszczona. Orbiter odzyskał swoją bazę, acz Loricatis musi współdzielić Persefonę. Z innej beczki: link Elena - Władawiec się pięknie rozwija.

Aktor w Opowieści:

* Dokonanie:
    * gasiła pożar po stworzeniu Zarralei między sobą i Eleną; nawet medal dostała. Potem poszukała dane na temat nowego komodora (Bolzy). Po zapoznaniu się z Bolzą dała szansę załodze na wycofanie się z misji, ale oni Ariannie wierzą. Chciała wykurzyć squatterów z Kazmirian, ale pod wpływem Bolzy zmieniła plan na sojusz. Niestety, poznawszy squatterów - musiała wstawić się za nich u Bolzy.
* Progresja:
    * dostała od Orbitera medal za bezkrwawe pokonanie dominujących sił wroga (dzięki operacji z Zarraleą)
    * bohaterka Astralnej Flary. Dzięki Ellarinie i jej propagandzie jest lubiana i podziwiana przez załogę Flary mimo jej nie najwyższej kompetencji.


### Astralna Flara kontra Domina Lucis

* **uid:** 221214-astralna-flara-kontra-domina-lucis, _numer względny_: 17
* **daty:** 0100-08-11 - 0100-08-13
* **obecni:** Arianna Verlen, Axel Nargan, Daria Czarnewik, Ellarina Samarintael, Gabriel Lodowiec, Kirea Rialirat, Leszek Kurzmin, NekroTAI Zarralea, OO Astralna Flara, OO Athamarein, Sarian Xadaar, Tristan Rialirat

Streszczenie:

Lodowiec dostał rozkazy pojmać / zabić dowódcę Dominy Lucis. Użył nekroTAI by odebrać resztkę nadziei noktianom (you will serve alive or dead) i jakkolwiek sporo noktian się poddało, główni popełnili samobójstwo. Orbiter zajął Dominę Lucis i Strefę Duchów, acz kosztem reputacji. Ogromny, bezkrwawy sukces wzmacniający Pax Orbiter dzięki propagandzie i Zarralei.

Aktor w Opowieści:

* Dokonanie:
    * dzieli się wszystkimi informacjami od Kirei z Lodowcem; wszystko planuje nie pod kątem honoru czy bezpieczeństwa a uratować jak najwięcej istnień ludzkich. Wkręca Lodowca w mowę mającą doprowadzić do poddania się noktian lub samobójstw noktian, ale by minimalna ilość ludzi ucierpiała. Humanitarna socjopatka - nie chce niczyjego cierpienia, ale ratowanie istnień przede wszystkim.


### Astralna Flara w strefie duchów

* **uid:** 221130-astralna-flara-w-strefie-duchow, _numer względny_: 16
* **daty:** 0100-08-07 - 0100-08-10
* **obecni:** Arianna Verlen, Arnulf Perikas, Daria Czarnewik, Elena Verlen, Ellarina Samarintael, Gabriel Lodowiec, Hubert Kerwelenios, Kajetan Kircznik, Kirea Rialirat, Klarysa Jirnik, Leo Kasztop, Maja Samszar, NekroTAI Zarralea, OO Astralna Flara, OO Athamarein, Szymon Wanad, Tomasz Dojnicz

Streszczenie:

Planetoida Kazmirian została przejęta przez Orbiter. Daria ostrzegła Nonarion (Leo), że Orbiter nadal poluje na TAI i zdobyła informację o 'strefie duchów'. Z Mają doszły do tego, że 'strefa duchów' jest świetnym maskowaniem; Lodowiec wyprowadził tam Orbiter a Arianna stworzyła nekroTAI z Zarralei d'Isigtand. Używając advancera przechwycili samotną savarankę (której Lodowiec nie umie przesłuchać) i stanęło na tym, że 'strefa duchów' jest bazą noktiańską zbudowaną dookoła jednostki wsparcia, 'Domina Lucis'. Lodowiec chce doprowadzić noktian do sprawiedliwości Orbitera, Arianna i Daria są skonfliktowane. Mają jeńca - Kireę, lokalną młodą savarankę.

Aktor w Opowieści:

* Dokonanie:
    * pozyskała informacje o tym, że coś może wpływać na TAI i może Strefa Duchów to noktianie. Stworzyła nekroTAI Zarraleę wywołując grozę w części załogi. Kazała przechwycić jeńca (Kireę) i zatrzymała Lodowca przed okrutnym traktowaniem savaranki. Przesłuchała ją wyjątkowo delikatnie i poznała prawdę o Strefie Duchów. Skuteczna w przekonywaniu Lodowca do zachowywania się "dobrze" a nie "maksymalnie skutecznie".
* Progresja:
    * opinia ABSOLUTNIE bezwzględnej i strasznej wśród załogi Athamarein i Astralnej Flary po mrocznym wskrzeszeniu TAI Zarralea. 'Live or die, you shall serve'.


### Egzotyczna Piękność na Astralnej Flarze

* **uid:** 221123-egzotyczna-pieknosc-na-astralnej-flarze, _numer względny_: 15
* **daty:** 0100-07-15 - 0100-07-18
* **obecni:** Arianna Verlen, Arnulf Perikas, Daria Czarnewik, Ellarina Samarintael, Erwin Pies, Frank Mgrot, Gabriel Lodowiec, Gerwazy Kircznik, Hind Szug Traf, Kajetan Kircznik, Leszek Kurzmin, Marcel Kulgard, OO Astralna Flara, OO Athamarein, SCA Hadiah Emas, Szczepan Myrczek, Władawiec Diakon

Streszczenie:

Flara naprawia Hadiah Emas, a po drodze komodor Lodowiec rozplątuje wszystkie wątki - niewolnik z Orbitera? (okazał się dezerterem), autonomiczna TAI Mirtaela d'Hadiah Emas? (zostaje Ograniczona przez psychotronika Athamarein) itp. Arianna zarządza Flarą, wprowadzając Ellarinę jako maskotkę od morale i stabilizując wszystko by jakoś zespół dopasował się do realiów Nonariona. To nie jest ani Aurum ani Orbiter, to coś zupełnie innego.

Aktor w Opowieści:

* Dokonanie:
    * wprowadziła Ellarinę na Astralną Flarę i przesunęła ją na 'maskotkę / morale'. Współpracuje z komodorem i próbuje deeskalować jego gorsze pomysły (tępienie faerilskich handlarzy niewolnikami, Ograniczanie TAI Mirtaeli). Wygrywa trochę, przegrywa trochę. Zarządza Flarą. Miękko dowodzi jednostką korzystając z zaufania zespołu.


### Astralna Flara dociera do Nonariona Nadziei

* **uid:** 221116-astralna-flara-dociera-do-nonariona-nadziei, _numer względny_: 14
* **daty:** 0100-07-11 - 0100-07-14
* **obecni:** Alan Nierkamin, Alezja Dumorin, Arianna Verlen, Arnulf Perikas, Daria Czarnewik, Gabriel Lodowiec, Grażyna Burgacz, Kajetan Kircznik, Leo Kasztop, Leszek Kurzmin, Maja Samszar, Marcel Kulgard, OO Astralna Flara, OO Athamarein, SCA Hadiah Emas, SCA Isigtand, Szymon Wanad, Tomasz Ruppok, Władawiec Diakon

Streszczenie:

Flara dociera do Nonariona, który wygląda jak absolutny śmieć. Morale na Flarze ucierpiało - aż do plotek o Egzotycznych Pięknościach, co sprawiło Ariannie kolejne problemy. Alan, eks-lokals powiedział Darii że jest jakaś forma standaryzacji na Nonarionie - coś tam jest, coś "bogatego" jako frakcja. Daria zdobyła od Leo planetoidę którą może użyć jako bazę dla Orbitera, ale w drodze tam zboczyli by ratować uszkodzone statki lokalne. I uratowali jednostkę niewolniczą i zniszczony grazer. Komodor Lodowiec - sensowny koleś - ma dylemat moralny. Neikatiańska TAI oraz niewolnicy.

Aktor w Opowieści:

* Dokonanie:
    * ma neutralny stosunek do noktian - Verlenowie tępili terrorformy Saitaera; przyjęła, że Władawiec pomaga w treningu Elenie, rozpaliła morale ludzi (przypadkiem rozogniła plotki o Egzotycznych Pięknościach)


### Astralna Flara i porwanie na Karsztarinie

* **uid:** 221102-astralna-flara-i-porwanie-na-karsztarinie, _numer względny_: 13
* **daty:** 0100-05-30 - 0100-06-05
* **obecni:** Ada Wyrocznik, Alezja Dumorin, Arianna Verlen, Arnulf Perikas, Daria Czarnewik, Elena Verlen, Gerwazy Kircznik, Grażyna Burgacz, Hubert Kerwelenios, Kajetan Kircznik, Klarysa Jirnik, Maja Samszar, Mariusz Bulterier, OO Karsztarin, OO Optymistyczny Żuk, Szczepan Myrczek, Władawiec Diakon

Streszczenie:

Arianna otrzymała nowy okręt - jednostkę wsparcia Astralna Flara. Jako pilota dostała Elenę. Elena ma problemy z aklimatyzacją na Orbiterze; chce być najlepsza i walczy o to. Nie rozmawia z ludźmi. Arianna zdecydowała się poprosić Alezję o pomoc w tej dziedzinie. Tymczasem przy ćwiczeniach na Karsztarinie okazało się, że Flara jest tam w czasie porwania - Zespołowi Flary udało się zatrzymać porwanie, choć operacja była niesamowicie niebezpieczna - niedoświadczony Zespół vs 4 komandosów Syndykatu Aureliona.

Aktor w Opowieści:

* Dokonanie:
    * dowodzi Astralną Flarą; chce zintegrować załogę - jedzenie dla Mai, ćwiczenia dla Eleny, opieprz dla Władawca itp. Przeprowadziła operację 'tracimy kontrolę nad Flarą' i jako pilot przeleciała do ukrytej korwety.


### Kapitan Verlen i koniec przygody na Królowej

* **uid:** 221026-kapitan-verlen-i-koniec-przygody-na-krolowej, _numer względny_: 12
* **daty:** 0100-04-10 - 0100-04-17
* **obecni:** Antoni Kramer, Arianna Verlen, Arnulf Perikas, Daria Czarnewik, Erwin Pies, Klaudiusz Terienak, Leszek Kurzmin, OO Królowa Kosmicznej Chwały, OO Tucznik Trzeci, Stefan Torkil, Tomasz Ruppok

Streszczenie:

Po długiej i ciężkiej pracy Arianna i Daria doprowadziły Królową do 50% sprawności nominalnej. Radość i wysokie morale załogi. Gdy Arnulf przyszedł do Arianny mówiąc o fabrykacji narkotyków i alkoholu, ona kazała to 'zatruć'. Zmotywowała Ruppoka, by dał jej szansę i znalazła sposób jak częściowo odzyskać materiały do fabrykatora po rozmowie z Arnulfem - sojusz z bliską Anomalii Kolapsu stacją Nonarion; poprosiła Kurzmina by to załatwił. TAK PRZESTRASZYŁA siły specjalne Orbitera, że Ariannę i załogę przenieśli bliżej Nonariona by sabotować program kosmiczny Aurum. Koniec przygody na Królowej :-).

Aktor w Opowieści:

* Dokonanie:
    * doprowadziła załogę do wiary w swoje umiejętności i Królową; kazała zatruć nielegalny alkohol by uniknąć konfrontacji ale wyjść na swoim. Przekonała Ruppoka, by ten dał szansę jej oraz Królowej oraz znalazła sposób jak odzyskać surowce - handel przez Tucznika Trzeciego z planetą i Nonarionem.
* Progresja:
    * zainteresowanie nowo wschodzącego admirała Antoniego Kramera z uwagi na jej nadspodziewany sukces
    * przeniesiona z Królowej Kosmicznej Chwały na dużo lepszą jednostkę, Astralną Flarę.


### Kapitan Verlen i pierwszy ruch statku

* **uid:** 221019-kapitan-verlen-i-pierwszy-ruch-statku, _numer względny_: 11
* **daty:** 0100-03-25 - 0100-04-06
* **obecni:** Arianna Verlen, Daria Czarnewik, Erwin Pies, Maja Samszar, Marcelina Trzęsiel, OO Królowa Kosmicznej Chwały, Romeo Verlen, Rufus Warkoczyk, Stefan Torkil

Streszczenie:

Arianna włączyła ćwiczenia dla Królowej - prawie wyszło, ale ludzie popanikowali a statek odmówił posłuszeństwa i załoga zaczęła się bić. Arianna ich przećwiczyła w działaniach awaryjnych i oddała panikarza medykowi - niestety, Paradoks zmienił Marcela w Marcelinę... ale następnym razem ćwiczenia powinny się już udać. Daria ma sprawę pod kontrolą i Arianna też raczej jest pewna sukcesu.

Aktor w Opowieści:

* Dokonanie:
    * zneutralizowała Maję Samszar ryzykiem konserw, rozpuściła plotki na swój temat (że kontratakuje i jej nie podpadać), zaczęła ćwiczenia z Królową i jak statek ruszył to zaczęła się panika. Arianna odwołała ćwiczenia... by przeszkolić ludzi w niepanikowaniu i działaniu sensownym.


### Kapitan Verlen i niezapowiedziana inspekcja

* **uid:** 221012-kapitan-verlen-i-niezapowiedziana-inspekcja, _numer względny_: 10
* **daty:** 0100-03-19 - 0100-03-23
* **obecni:** Adam Chrząszczewicz, Arianna Verlen, Arnulf Perikas, Daria Czarnewik, Hubert Kerwelenios, Leona Astrienko, Maja Samszar, Mariusz Bulterier, OO Królowa Kosmicznej Chwały, Szczepan Myrczek, Tomasz Dojnicz, Władawiec Diakon

Streszczenie:

Podczas skanu kadłuba Królowej advancerzy z Darią wykryli scrambler TAI, destabilizator silników itp. Arianna stoczyła bitwę ćwiczebną z marine, ale pojawiła się inspekcja (która nie była zapowiedziana bo Maja nie przekazała informacji). Daria reanimowała TAI, wszystko częściowo poszło nie tak. Ale o dziwo audytor chciał pomóc Ariannie, która stanęła między Orbiterem (wsparcie) i Aurum (my sami sobie poradzimy). Arianna upewniła się, że Władawiec nie stał za operacją - najpewniej Maja. Ale Królowa jest w lepszym stanie niż kiedykolwiek, tylko, że Arianna je konserwy (by nie wpaść pod feromony itp)

Aktor w Opowieści:

* Dokonanie:
    * przeszła na konserwy i spartańskie żarcie póki jest na Królowej; stoczyła starcie z marine i wybroniła się przed inspektorem Królowej nie zdradzając że jej oficerowie ją wrobili. Odzyskuje kontrolę nad Królową i zaczyna zaskarbiać sobie zaufanie wielu grup (seilici, sarderyci, marines, dwóch oficerów)
* Progresja:
    * NAGANA DO AKT że nie zgłasza stanu jednostki i próbuje sama zrobić bez wsparcia itp - przez co jednostka ucierpiała.
    * coraz większy szacunek na Królowej Kosmicznej Chwały: marines, advancerzy, sarderyci, seilici. Ma też wsparcie u Arnulfa i Grażyny. Pojawia się szacunek do niej za żarcie konserw póki Królowa nie będzie w dobrym stanie (lub: bo Szymon ją skaleczył podczas pojedynku).


### Kapitan Verlen i pojedynek z marine

* **uid:** 220928-kapitan-verlen-i-pojedynek-z-marine, _numer względny_: 9
* **daty:** 0100-03-16 - 0100-03-18
* **obecni:** Alezja Dumorin, Arianna Verlen, Arnulf Perikas, Daria Czarnewik, Erwin Pies, Grażyna Burgacz, Leona Astrienko, Leszek Kurzmin, Maja Samszar, OO Królowa Kosmicznej Chwały, Stefan Torkil, Szymon Wanad, Tomasz Ruppok, Władawiec Diakon

Streszczenie:

Królowa miała się przesunąć, ale prawie się rozbiła; ta jednostka jest rozkradziona. Arianna zatrzymała biczowanie załogantów i doszła do tego, że załoga jest niemrawa i skonfliktowana bo część ludzi jest z Aurum a część nie. Perfect storm of suck. By zatrzymać Wanada przed "wygraniem cnoty koleżanki inżyniera" stoczyła z nim pojedynek i wygrała. Leona ostrzegła, by Arianna nic nie jadła i nie piła. Czyli coś jest w jedzeniu. To tłumaczy czemu Królowa jest tak zdegenerowaną jednostką. Aha, Arianna ma wsparcie kapitana Leszka Kurzmina, dawnego rywala Alezji.

Aktor w Opowieści:

* Dokonanie:
    * zatrzymała praktykę biczowania załogi (wprowadzoną przez Aurum) i stoczyła pojedynek z Szymonem Wanadem, psychopatycznym marine. Pokonała go i oswoiła. Przestała jeść w kantynie XD.


### Kapitan Verlen i Królowa Kosmicznej Chwały

* **uid:** 220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly, _numer względny_: 8
* **daty:** 0100-03-08 - 0100-03-14
* **obecni:** Alezja Dumorin, Arianna Verlen, Arnulf Perikas, Daria Czarnewik, Erwin Pies, Grażyna Burgacz, Klarysa Jirnik, Klaudiusz Terienak, Leona Astrienko, Maja Samszar, OO Królowa Kosmicznej Chwały, Szymon Wanad, Władawiec Diakon

Streszczenie:

Królowa Kosmicznej Chwały to najbardziej dysfunkcyjna jednostka pod kontrolą Orbitera - służy do łamania karier obiecujących oficerów i sabotażu programu kosmicznego Aurum (oba z woli sił specjalnych Aurum). Trafiła na królową Arianna i zaczęła robić porządek. Poprzednią panią kapitan zdecydowała się odratować, współpracując z Darią z inżynierii rozmontować wyścigi psów i stwierdziła, że doprowadzi do tego, by Królowa zaczęła skutecznie działać podczas ćwiczeń. Aha, tu poznała się z Leoną Astrienko ;-).

Aktor w Opowieści:

* Dokonanie:
    * 22-23 lata; została kapitan Królowej Kosmicznej Chwały (jednostki 'Aurum' pod kontrolą Orbitera). Dała szansę poprzedniej kapitan (Alezji) mimo, że ta jest w beznadziejnym stanie i pokonała marine. Wprowadziła dyskomfort na Królowej i pokazała innym oficerom, że Siły Specjalne Orbitera sabotują Królową by program kosmiczny Aurum się nie mógł odbyć.


### Lustrzane odbicie Eleny

* **uid:** 210324-lustrzane-odbicie-eleny, _numer względny_: 7
* **daty:** 0097-07-05 - 0097-07-08
* **obecni:** Arianna Verlen, Elena Verlen, Lucjusz Blakenbauer, Michał Perikas, Przemysław Czapurt, Romeo Verlen, Viorika Verlen

Streszczenie:

Elena, chcąc utrzymać kontrolę nad swoją mocą zaczęła brać wiktoriatę. To plus przepuszczanie magii przez lustra zniszczyło jej wzór i ją uszkodziło. Elena stała się wampirem - pożera energetycznie żołnierzy by chronić teren i wszystkich cywilów. Plus, pragnie zemsty na Blakenbauerach i na Perikasach. W swoim Skażeniu powołała Lustrzanego Golema. Viorika i Arianna dały radę wymanewrować Elenę, odkryć, że to ona stoi za Lustrzanym Golemem i z pomocą sierżanta Czapurta w Poniewierzy dały radę Elenę unieszkodliwić, by ją docelowo naprawić...

Aktor w Opowieści:

* Dokonanie:
    * stoi za zmianą nazwy miejscowości na Skałkowa Myśl. Powiązała lustra z Eleną, po czym zagadywała ją i przekonywała, by Elena nie zrobiła niczego złego ani głupiego. De facto, jej umiejętności gadania sprawiły, że Elena ze spokojem weszła w pułapkę by Czapurt mógł ją unieszkodliwić. Plus: zaszokowana relacjami Vioriki i Lucjusza.


### Studenci u Verlenów

* **uid:** 210311-studenci-u-verlenow, _numer względny_: 6
* **daty:** 0096-11-18 - 0096-11-24
* **obecni:** Apollo Verlen, Arianna Verlen, Dariusz Blakenbauer, Elena Verlen, Maja Samszar, Michał Perikas, Rafał Perikas, Rufus Samszar, Sylwia Perikas, Viorika Verlen

Streszczenie:

Verlenowie mieli wymianę studencką, współpracę z magami rodów Perikas i Samszar. Dariusz Blakenbauer chciał rozbić potencjalny sojusz, dając młodym Perikasom lustro mające zaspokoić ich ochotę na Elenę. Niestety, wszystko poszło katastrofalnie nie tak bo Elena ma anomalną magię - w wyniku eksplozji zginęło kilkanaście wił i 2 żołnierzy. Elena w ruinie psychicznej, wszyscy przerażeni. Wezwane Arianna i Viorika odkryły że za wszystkim stoi Blakenbauer i znalazły winnych wśród młodych Perikasów. Jednak - dla polityki - wszystko zwaliły na Blakenbauerów :D.

Aktor w Opowieści:

* Dokonanie:
    * 20 lat, uspokoiła zdruzgotaną zabiciem ludzi Elenę; do tego rozwaliła dwa ścigacze. Magicznie odkryła, że za wszystkim stał Dariusz Blakenbauer i dziwne lustro - że to nie sama Elena utraciła kontrolę nad mocą.
* Progresja:
    * powszechna opinia u żołnierzy i w rodzie, że ma dużo zalet, ale pilotem nigdy nie będzie... rozwaliła DWA ścigacze JEDNEGO DNIA. Acz kochana w Poniewierzy.


### Ekologia jaszczurów w Arachnoziemie

* **uid:** 230524-ekologia-jaszczurow-w-arachnoziemie, _numer względny_: 5
* **daty:** 0095-08-06 - 0095-08-09
* **obecni:** Arianna Verlen, Fabian Rzelicki, Marcel Biekakis, Mściząb Verlen, Ula Blakenbauer, Viorika Verlen

Streszczenie:

W Arachnoziemie są dwa stwory - śluzowiec i mimik. A+V wprowadziły mechanizmy ochrony ludzi i oddzielania zainfekowanych ludzi od potworów i wezwały Ulę Blakenbauer na pomoc. Ta przybyła, zrobiła biolab i stworzyła płaszczki mające uratować kogo się da; jaszczury tunelowe ściągnie od Blakenbauerów. Sytuacja jest opanowana - gdy Ula zajęła się usuwaniem śluzowców to Verlenki usunęły krystalicznego mimika. Ogólnie - Arachnoziem ustabilizowany. A całość zasług za karę dostał Mściząb.

Aktor w Opowieści:

* Dokonanie:
    * optymistycznie weszła do domu martwego nastolatka i nie tylko uniknęła śluzowca ale też złapała jego próbki, potem wezwała Ulę Blakenbauer na pomoc. Krzyżuje dane z kamer by pomóc Viorice znaleźć drugiego mimika.


### Mściząb, zabójca arachnoziemskich jaszczurów

* **uid:** 230426-mscizab-zabojca-arachnoziemskich-jaszczurow, _numer względny_: 4
* **daty:** 0095-08-03 - 0095-08-05
* **obecni:** Arianna Verlen, Atraksjusz Verlen, Emmanuelle Gęsiawiec, Mściząb Verlen, Viorika Verlen

Streszczenie:

Opiekując się Eleną, Arianna jest wyczulona na sygnały Esuriit przekazywane przez sentisieć, nawet 'wyciszone', więc A+V ruszyły do Arachnoziem. Tam się okazuje że Mściząb Verlen zrobił blokadę informacyjną bo chce pokonać potwora który tam jest bez pomocy; tyle, że zginęło już kilku ludzi m.in. flarą Esuriit 17-latek. Za wszystkim stoi zakochana w Mścizębie Emmanuelle, która jest w jego oddziale i udaje kogoś innego - ona doprowadziła do rozognienia Tunelowych Jaszczurów. Ale jak Jaszczurów zabrakło, na powierzchnię wyszło Coś Innego, potworny łowca polujący na ludzi...

Aktor w Opowieści:

* Dokonanie:
    * wyczuła ukrywane Esuriit w Arachnoziem; gdy Mściząb walczył z Vioriką, attunowała się do sentisieci i poznała prawdę co się tu dzieje. Odkryła obecność Emmanuelle. "Dobry glina" gadając z Emmanuelle.


### Karolinka, nieokiełznana świnka

* **uid:** 230419-karolinka-nieokielznana-swinka, _numer względny_: 3
* **daty:** 0095-07-19 - 0095-07-23
* **obecni:** Aleksander Samszar, Apollo Verlen, Arianna Verlen, Elena Verlen, Fantazjusz Verlen, Marcinozaur Verlen, Tymek Samszar, Ula Blakenbauer, Viorika Verlen

Streszczenie:

Zaprojektowano świnkę Karolinkę do pomszczenia Vioriki. Aria, Elena i Viorika ruszyły do Powiatu radząc sobie z figlarnym glukszwajnem. Po drodze Elena rozbiła vana w menhir, wintegrowała niewinnego Samszara w menhir, porzuciły tam świnkę i dotarły do Siewczyna. Tam Elena próbowała być słodka i współpracować z Samszarem. O dziwo, udało się przekonać Samszara by pozwolił Verlenkom zmierzyć się ze Strażnikiem. W wyniku Paradoksów, 'potwór został porwany' i uciekł do Verlenlandu a Elenie po raz pierwszy uruchomiła się moc Esuriit.

Aktor w Opowieści:

* Dokonanie:
    * po chwili zabawiania Karolinki przejęła pilotaż vana jak już Elena go rozbiła. Zamiast iść w powagę, poszła w 'pomóżmy Samszarom i naprawmy im potwora XD'. Dobre serce i klej społeczny zespołu.


### Dźwiedzie polują na ser

* **uid:** 230412-dzwiedzie-poluja-na-ser, _numer względny_: 2
* **daty:** 0095-07-12 - 0095-07-14
* **obecni:** Apollo Verlen, Arianna Verlen, Elena Verlen, Marcinozaur Verlen, Ula Blakenbauer, Viorika Verlen

Streszczenie:

Koszarów Chłopięcy ma problem - Apollo i Marcinozaur się kłócą o płaszczkę Blakenbauerów. Więc przybywa Arianna i Viorika na prośbę Apollo. Nie tylko łagodzą sprawę i poznają prawdę (chodzi o honor Eleny i by pokazać wartość Blakenbauerów Verlenom i vice versa) ale jeszcze robią wyzwanie między Ulą Blakenbauer i Verlenami. Okazuje się, że Ula jest niesamowicie silna, ale taktyka Vioriki, dźwiedzie Verlenów i umiejętności ścigaczowania Arianny (w co nadal nikt nie wierzy bo ona nie umie pilotować XD) wygrały konflikt bezkrwawo. Poszukiwania pochodzenia Szeptomandry trwają, zajmuje się tym Marcinozaur i Ula.

Aktor w Opowieści:

* Dokonanie:
    * dyplomacja wewnątrzverlenowa między Marcinozaurem i Apollo, łagodzenie potencjalnych problemów i docelowo świetny rajd ścigaczem by pozyskać ser od Uli nie walcząc z jej potworami.


### Sentiobsesja

* **uid:** 210224-sentiobsesja, _numer względny_: 1
* **daty:** 0092-08-03 - 0092-08-06
* **obecni:** Apollo Verlen, Arianna Verlen, Lucjusz Blakenbauer, Milena Blakenbauer, Mścigrom Verlen, Przemysław Czapurt, Viorika Verlen

Streszczenie:

Apollo Verlen, sam w Holdzie Bastion był jedynym sentitienem. Gdy uderzyły siły Mileny Blakenbauer, wściekł się i wpadł w sentiobsesję - chce atakować Blakenbauerów. Na szczęście na miejsce przybyły Arianna i Viorika, które rozwiązały sprawę, odkryły sekret Apolla (3 kochanki) i ściągnęły go pułapką, po czym... pokonały i przywróciły. Aha, potem odzyskały brakujący oddział kosztem śpiewu i tańca Apolla przed Mileną Blakenbauer.

Aktor w Opowieści:

* Dokonanie:
    * 16 lat, zwana "Aria". Zaprzyjaźniła się z żołnierzami i najczęściej wykorzystuje swoje umiejętności retoryczne i to, że jest słodka do osiągania celów. Przekonała Apolla do zwalczenia sentiobsesji a potem oddała Apolla Milenie by odzyskać oddział ;-).
* Progresja:
    * jest pupilką sierżanta Przemysława Czapurta rodu Verlen. To znaczy, że traktuje ją ostro i twardo ;-).
    * "młodsza siorka" żołnierzy w Holdzie Bastion. Swoja laska. Fajna, wpada z nimi w kłopoty i mimo imienia (Aria) nie umie śpiewać pieśni patriotycznych w karcerze.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 91, @: 0112-07-29
    1. Nierzeczywistość    : 2, @: 0111-08-08
    1. Primus    : 91, @: 0112-07-29
        1. Sektor Astoriański    : 87, @: 0112-07-29
            1. Astoria, Kosmos    : 1, @: 0110-09-17
                1. Brama Trzypływów    : 1, @: 0110-09-17
            1. Astoria, Orbita    : 25, @: 0112-07-25
                1. Kontroler Pierwszy    : 24, @: 0112-07-25
                    1. Akademia Orbitera    : 1, @: 0110-10-19
                    1. Arena Kalaternijska    : 1, @: 0110-12-04
                    1. Hangary Alicantis    : 3, @: 0111-11-27
                    1. Laboratoria Dekontaminacyjne    : 1, @: 0112-01-27
                    1. Sektor 22    : 1, @: 0111-05-29
                        1. Dom Uciech Wszelakich    : 1, @: 0111-05-29
                        1. Kasyno Stanisława    : 1, @: 0111-05-29
                    1. Sektor 25    : 1, @: 0112-07-25
                        1. Bar Solatium    : 1, @: 0112-07-25
                    1. Sektor 43    : 1, @: 0111-02-13
                        1. Tor wyścigowy ścigaczy    : 1, @: 0111-02-13
                    1. Sektor 49    : 1, @: 0112-02-08
                    1. Sektor 57    : 1, @: 0111-11-19
                        1. Mordownia    : 1, @: 0111-11-19
            1. Astoria, Pierścień Zewnętrzny    : 6, @: 0112-07-13
                1. Laboratorium Kranix    : 1, @: 0112-07-13
                1. Poligon Stoczni Neotik    : 3, @: 0112-03-04
                1. Stacja Medyczna Atropos    : 1, @: 0112-07-13
                1. Stocznia Neotik    : 5, @: 0112-03-04
                    1. Zewnętrzny dok ixioński    : 1, @: 0112-02-23
            1. Astoria    : 16, @: 0111-07-02
                1. Sojusz Letejski, NW    : 6, @: 0111-01-16
                    1. Ruiniec    : 6, @: 0111-01-16
                        1. Ortus-Conticium    : 1, @: 0110-12-28
                        1. Pustynia Kryształowa    : 1, @: 0111-01-10
                        1. Trzeci Raj, okolice    : 2, @: 0110-12-23
                            1. Kopiec Nojrepów    : 2, @: 0110-12-23
                            1. Zdrowa Ziemia    : 1, @: 0110-12-23
                        1. Trzeci Raj    : 5, @: 0111-01-16
                            1. Barbakan    : 1, @: 0110-12-23
                            1. Centrala Ataienne    : 1, @: 0110-12-23
                            1. Mały Kosmoport    : 1, @: 0110-12-23
                            1. Ratusz    : 1, @: 0110-12-23
                            1. Stacja Nadawcza    : 1, @: 0110-12-23
                1. Sojusz Letejski    : 11, @: 0111-07-02
                    1. Aurum    : 11, @: 0111-07-02
                        1. Powiat Niskowzgórza    : 1, @: 0111-01-10
                            1. Domena Arłacz    : 1, @: 0111-01-10
                                1. Posiadłość Arłacz    : 1, @: 0111-01-10
                                    1. Baterie Puryfikacji Krwi    : 1, @: 0111-01-10
                                    1. Małe lądowisko    : 1, @: 0111-01-10
                        1. Powiat Samszar    : 1, @: 0095-07-23
                            1. Siewczyn    : 1, @: 0095-07-23
                                1. Północne obrzeża    : 1, @: 0095-07-23
                                    1. Spichlerz Jedności    : 1, @: 0095-07-23
                            1. Wielkie Kwiatowisko    : 1, @: 0095-07-23
                                1. Menhir Centralny    : 1, @: 0095-07-23
                        1. Sentipustkowie Pierwotne    : 1, @: 0092-08-06
                        1. Świat Dżungli    : 1, @: 0092-08-06
                            1. Ostropnącz    : 1, @: 0092-08-06
                        1. Verlenland    : 9, @: 0111-07-02
                            1. Arachnoziem    : 2, @: 0095-08-09
                                1. Bar Łeb Jaszczura    : 1, @: 0095-08-05
                                1. Garnizon    : 1, @: 0095-08-09
                                1. Kopalnia    : 2, @: 0095-08-09
                                1. Stacja Pociągu Maglev    : 1, @: 0095-08-09
                            1. Hold Bastion    : 1, @: 0092-08-06
                                1. Barbakan    : 1, @: 0092-08-06
                                1. Karcer    : 1, @: 0092-08-06
                                1. Karczma Szczur    : 1, @: 0092-08-06
                            1. Hold Karaan, obrzeża    : 1, @: 0111-06-26
                                1. Lądowisko    : 1, @: 0111-06-26
                            1. Hold Karaan    : 2, @: 0111-07-02
                                1. Barbakan Centralny    : 1, @: 0111-06-26
                                1. Krypta Plugastwa    : 2, @: 0111-07-02
                                1. Szpital Centralny    : 1, @: 0111-07-02
                            1. Koszarów Chłopięcy, okolice    : 1, @: 0095-07-14
                                1. Las Wszystkich Niedźwiedzi    : 1, @: 0095-07-14
                                1. Skały Koszarowe    : 1, @: 0095-07-14
                            1. Koszarów Chłopięcy    : 1, @: 0095-07-14
                            1. Mikrast    : 1, @: 0097-07-08
                            1. Poniewierz, obrzeża    : 1, @: 0096-11-24
                                1. Jaskinie Poniewierskie    : 1, @: 0096-11-24
                            1. Poniewierz, północ    : 1, @: 0096-11-24
                                1. Osiedle Nadziei    : 1, @: 0096-11-24
                            1. Poniewierz    : 3, @: 0111-07-02
                                1. Arena    : 1, @: 0097-07-08
                                1. Dom Uciech Wszelakich    : 1, @: 0096-11-24
                                1. Zamek Gościnny    : 3, @: 0111-07-02
                            1. Skałkowa Myśl    : 1, @: 0097-07-08
                            1. VirtuFortis    : 1, @: 0095-07-23
                                1. Wielki Plac Miraży    : 1, @: 0095-07-23
                        1. Wielkie Księstwo Aktenir    : 1, @: 0111-05-29
                            1. Pałac Jasnego Ognia    : 1, @: 0111-05-29
            1. Brama Kariańska    : 6, @: 0112-01-17
            1. Brama Trzypływów    : 1, @: 0111-08-08
            1. Elang, księżyc Astorii    : 2, @: 0112-04-30
                1. EtAur Zwycięska    : 2, @: 0112-04-30
                    1. Magazyny wewnętrzne    : 1, @: 0112-04-30
                    1. Sektor bioinżynierii    : 1, @: 0112-04-30
                        1. Biolab    : 1, @: 0112-04-30
                        1. Komory żywieniowe    : 1, @: 0112-04-30
                        1. Skrzydło medyczne    : 1, @: 0112-04-30
                    1. Sektor inżynierski    : 1, @: 0112-04-30
                        1. Panele słoneczne i bateriownia    : 1, @: 0112-04-30
                        1. Podtrzymywanie życia    : 1, @: 0112-04-30
                        1. Serwisownia    : 1, @: 0112-04-30
                        1. Stacja pozyskiwania wody    : 1, @: 0112-04-30
                    1. Sektor mieszkalny    : 1, @: 0112-04-30
                        1. Barbakan    : 1, @: 0112-04-30
                            1. Administracja    : 1, @: 0112-04-30
                            1. Ochrona    : 1, @: 0112-04-30
                            1. Rdzeń AI    : 1, @: 0112-04-30
                        1. Centrum Rozrywki    : 1, @: 0112-04-30
                        1. Podtrzymywanie życia    : 1, @: 0112-04-30
                        1. Pomieszczenia mieszkalne    : 1, @: 0112-04-30
                        1. Przedszkole    : 1, @: 0112-04-30
                        1. Stołówki    : 1, @: 0112-04-30
                        1. Szklarnie    : 1, @: 0112-04-30
                    1. Sektor przeładunkowy    : 1, @: 0112-04-30
                        1. Atraktory małych pakunków    : 1, @: 0112-04-30
                        1. Magazyny    : 1, @: 0112-04-30
                        1. Pieczara Gaulronów    : 1, @: 0112-04-30
                        1. Punkt celny    : 1, @: 0112-04-30
                        1. Stacja grazerów    : 1, @: 0112-04-30
                        1. Stacja przeładunkowa    : 1, @: 0112-04-30
                        1. Starport    : 1, @: 0112-04-30
                    1. Ukryte sektory    : 1, @: 0112-04-30
            1. Iorus    : 5, @: 0112-04-09
                1. Iorus, pierścień    : 5, @: 0112-04-09
                    1. Keldan Voss    : 5, @: 0112-04-09
            1. Kosmiczna Pustka    : 1, @: 0108-05-08
            1. Krwawa Baza Piracka    : 1, @: 0112-01-03
            1. Neikatis    : 2, @: 0112-02-11
                1. Dystrykt Lennet    : 1, @: 0112-02-11
                1. Dystrykt Quintal    : 1, @: 0111-01-29
                    1. Arkologia Aspiria    : 1, @: 0111-01-29
            1. Obłok Lirański    : 12, @: 0112-07-29
                1. Anomalia Kolapsu, orbita    : 8, @: 0112-07-29
                    1. Planetoida Kazmirian    : 1, @: 0100-07-14
                    1. SC Nonarion Nadziei    : 1, @: 0100-07-14
                        1. Moduł ExpanLuminis    : 1, @: 0100-07-14
                    1. Sortownia Seibert    : 1, @: 0112-07-25
                    1. Strefa Biur HR    : 1, @: 0100-09-18
                    1. Strefa Upiorów Orbitera    : 4, @: 0100-09-18
                        1. Planetoida Kazmirian    : 2, @: 0100-09-12
                        1. Planetoida Lodowca    : 4, @: 0100-09-18
                1. Anomalia Kolapsu    : 2, @: 0110-11-09
            1. Pas Omszawera    : 1, @: 0110-10-04
                1. Kolonia Samojed    : 1, @: 0110-10-04
                    1. Stacja Astropociągów    : 1, @: 0110-10-04
                    1. Zona Czarna    : 1, @: 0110-10-04
                    1. Zona Mieszkalna    : 1, @: 0110-10-04
            1. Pas Teliriański    : 4, @: 0111-09-14
                1. Planetoidy Kazimierza    : 3, @: 0111-09-14
                    1. Planetoida Asimear    : 3, @: 0111-09-14
                        1. Stacja Lazarin    : 2, @: 0111-09-14
                1. Stacja Telira-Melusit VII    : 1, @: 0111-03-07
            1. Stocznia Kariańska    : 1, @: 0112-01-17
        1. Sektor Lacarin    : 1, @: 0110-11-22
        1. Sektor Mevilig    : 3, @: 0112-01-20
            1. Chmura Piranii    : 2, @: 0112-01-17
            1. Keratlia    : 1, @: 0112-01-17
            1. Planetoida Kalarfam    : 2, @: 0112-01-20
        1. Sektor Noviter    : 1, @: 0112-01-10
        1. Zagubieni w Kosmosie    : 4, @: 0111-04-08
            1. Crepuscula    : 4, @: 0111-04-08
                1. Pasmo Zmroku    : 4, @: 0111-04-08
                    1. Baza Noktiańska Zona Tres    : 4, @: 0111-04-08
                    1. Podwodna Brama Eteryczna    : 1, @: 0111-03-15

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Klaudia Stryk        | 64 | ((200106-infernia-martyn-hiwasser; 200122-kiepski-altruistyczny-terroryzm; 200408-o-psach-i-krysztalach; 200415-sabotaz-miecza-swiatla; 200429-porwanie-cywila-z-kokitii; 200610-ixiacka-wersja-malictrix; 200624-ratujmy-castigator; 200708-problematyczna-elena; 200715-sabotaz-netrahiny; 200729-nocna-krypta-i-emulatorka; 200819-sekrety-orbitera-historia-prawdziwa; 200826-nienawisc-do-swin; 200909-arystokratka-w-ladowni-na-swinie; 200916-smierc-raju; 200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz; 201104-sabotaz-swini; 201111-mychainee-na-netrahinie; 201118-anastazja-bohaterka; 201125-wyscig-jako-randka; 201210-pocalunek-aspirii; 201216-krypta-i-wyjec; 201230-pulapka-z-anastazji; 210106-sos-z-haremu; 210120-sympozjum-zniszczenia; 210127-porwanie-anastazji-z-odkupienia; 210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear; 210421-znudzona-zaloga-inferni; 210428-infekcja-serenit; 210512-ewakuacja-z-serenit; 210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210616-nieudana-infiltracja-inferni; 210630-listy-od-fanow; 210707-po-drugiej-stronie-bramy; 210721-pierwsza-bia-mag; 210804-infernia-jest-nasza; 210818-siostrzenica-morlana; 210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej; 210922-ostatnia-akcja-bohaterki; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica; 211020-kurczakownia; 211027-rzieza-niszczy-infernie; 211110-romans-dzieki-esuriit; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide; 220126-keldan-voss-kolonia-saitaera; 220202-sekrety-keldan-voss; 220216-polityka-rujnuje-pallide-voss; 220223-stabilizacja-keldan-voss; 220309-upadek-eleny; 220316-potwor-czy-choroba-na-etaur; 220330-etaur-i-przyneta-na-krypte; 220610-ratujemy-porywaczy-eleny; 220615-lewiatan-przy-seibert; 220622-lewiatan-za-pandore)) |
| Eustachy Korkoran    | 63 | ((200122-kiepski-altruistyczny-terroryzm; 200408-o-psach-i-krysztalach; 200415-sabotaz-miecza-swiatla; 200429-porwanie-cywila-z-kokitii; 200624-ratujmy-castigator; 200708-problematyczna-elena; 200715-sabotaz-netrahiny; 200722-wielki-kosmiczny-romans; 200729-nocna-krypta-i-emulatorka; 200819-sekrety-orbitera-historia-prawdziwa; 200826-nienawisc-do-swin; 200909-arystokratka-w-ladowni-na-swinie; 200916-smierc-raju; 200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz; 201104-sabotaz-swini; 201111-mychainee-na-netrahinie; 201118-anastazja-bohaterka; 201210-pocalunek-aspirii; 201216-krypta-i-wyjec; 201230-pulapka-z-anastazji; 210106-sos-z-haremu; 210120-sympozjum-zniszczenia; 210127-porwanie-anastazji-z-odkupienia; 210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear; 210421-znudzona-zaloga-inferni; 210428-infekcja-serenit; 210512-ewakuacja-z-serenit; 210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210609-sekrety-kariatydy; 210616-nieudana-infiltracja-inferni; 210707-po-drugiej-stronie-bramy; 210714-baza-zona-tres; 210721-pierwsza-bia-mag; 210728-w-cieniu-nocnej-krypty; 210804-infernia-jest-nasza; 210818-siostrzenica-morlana; 210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica; 211020-kurczakownia; 211110-romans-dzieki-esuriit; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide; 220126-keldan-voss-kolonia-saitaera; 220202-sekrety-keldan-voss; 220216-polityka-rujnuje-pallide-voss; 220223-stabilizacja-keldan-voss; 220309-upadek-eleny; 220316-potwor-czy-choroba-na-etaur; 220330-etaur-i-przyneta-na-krypte; 220610-ratujemy-porywaczy-eleny; 220615-lewiatan-przy-seibert; 220622-lewiatan-za-pandore)) |
| Elena Verlen         | 55 | ((200708-problematyczna-elena; 200715-sabotaz-netrahiny; 200722-wielki-kosmiczny-romans; 200819-sekrety-orbitera-historia-prawdziwa; 200909-arystokratka-w-ladowni-na-swinie; 200916-smierc-raju; 200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy; 201230-pulapka-z-anastazji; 210106-sos-z-haremu; 210120-sympozjum-zniszczenia; 210127-porwanie-anastazji-z-odkupienia; 210210-milosc-w-rodzie-verlen; 210218-infernia-jako-goldarion; 210311-studenci-u-verlenow; 210317-arianna-podbija-asimear; 210324-lustrzane-odbicie-eleny; 210331-elena-z-rodu-verlen; 210414-dekralotyzacja-asimear; 210421-znudzona-zaloga-inferni; 210428-infekcja-serenit; 210512-ewakuacja-z-serenit; 210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210616-nieudana-infiltracja-inferni; 210630-listy-od-fanow; 210707-po-drugiej-stronie-bramy; 210714-baza-zona-tres; 210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej; 210922-ostatnia-akcja-bohaterki; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica; 211020-kurczakownia; 211027-rzieza-niszczy-infernie; 211110-romans-dzieki-esuriit; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide; 220126-keldan-voss-kolonia-saitaera; 220202-sekrety-keldan-voss; 220216-polityka-rujnuje-pallide-voss; 220223-stabilizacja-keldan-voss; 220309-upadek-eleny; 220316-potwor-czy-choroba-na-etaur; 220610-ratujemy-porywaczy-eleny; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221130-astralna-flara-w-strefie-duchow; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe; 230412-dzwiedzie-poluja-na-ser; 230419-karolinka-nieokielznana-swinka)) |
| Martyn Hiwasser      | 33 | ((200106-infernia-martyn-hiwasser; 200408-o-psach-i-krysztalach; 200415-sabotaz-miecza-swiatla; 200819-sekrety-orbitera-historia-prawdziwa; 200826-nienawisc-do-swin; 200909-arystokratka-w-ladowni-na-swinie; 201104-sabotaz-swini; 201111-mychainee-na-netrahinie; 201118-anastazja-bohaterka; 201125-wyscig-jako-randka; 201216-krypta-i-wyjec; 201230-pulapka-z-anastazji; 210106-sos-z-haremu; 210120-sympozjum-zniszczenia; 210127-porwanie-anastazji-z-odkupienia; 210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear; 210421-znudzona-zaloga-inferni; 210428-infekcja-serenit; 210512-ewakuacja-z-serenit; 210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210707-po-drugiej-stronie-bramy; 210714-baza-zona-tres; 210721-pierwsza-bia-mag; 210901-stabilizacja-bramy-eterycznej; 210922-ostatnia-akcja-bohaterki; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica; 211020-kurczakownia; 211027-rzieza-niszczy-infernie; 220610-ratujemy-porywaczy-eleny)) |
| Leona Astrienko      | 29 | ((200408-o-psach-i-krysztalach; 200429-porwanie-cywila-z-kokitii; 200610-ixiacka-wersja-malictrix; 200624-ratujmy-castigator; 200722-wielki-kosmiczny-romans; 200729-nocna-krypta-i-emulatorka; 200819-sekrety-orbitera-historia-prawdziwa; 200826-nienawisc-do-swin; 210106-sos-z-haremu; 210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210421-znudzona-zaloga-inferni; 210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210616-nieudana-infiltracja-inferni; 210804-infernia-jest-nasza; 210818-siostrzenica-morlana; 210901-stabilizacja-bramy-eterycznej; 210922-ostatnia-akcja-bohaterki; 210929-grupa-ekspedycyjna-kellert; 211110-romans-dzieki-esuriit; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy; 220610-ratujemy-porywaczy-eleny; 220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221012-kapitan-verlen-i-niezapowiedziana-inspekcja)) |
| Kamil Lyraczek       | 13 | ((191025-nocna-krypta-i-bohaterka; 200106-infernia-martyn-hiwasser; 200408-o-psach-i-krysztalach; 200826-nienawisc-do-swin; 200909-arystokratka-w-ladowni-na-swinie; 201118-anastazja-bohaterka; 201216-krypta-i-wyjec; 210414-dekralotyzacja-asimear; 210526-morderstwo-na-inferni; 210616-nieudana-infiltracja-inferni; 211020-kurczakownia; 211215-sklejanie-inferni-do-kupy; 220105-to-nie-pulapka-na-nereide)) |
| OO Infernia          | 13 | ((201210-pocalunek-aspirii; 201230-pulapka-z-anastazji; 210127-porwanie-anastazji-z-odkupienia; 210218-infernia-jako-goldarion; 210428-infekcja-serenit; 210512-ewakuacja-z-serenit; 210825-uszkodzona-brama-eteryczna; 210929-grupa-ekspedycyjna-kellert; 211020-kurczakownia; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy; 220622-lewiatan-za-pandore)) |
| Antoni Kramer        | 12 | ((200429-porwanie-cywila-z-kokitii; 200708-problematyczna-elena; 200722-wielki-kosmiczny-romans; 200729-nocna-krypta-i-emulatorka; 200826-nienawisc-do-swin; 210218-infernia-jako-goldarion; 210526-morderstwo-na-inferni; 210804-infernia-jest-nasza; 210818-siostrzenica-morlana; 210922-ostatnia-akcja-bohaterki; 210929-grupa-ekspedycyjna-kellert; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Daria Czarnewik      | 12 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221019-kapitan-verlen-i-pierwszy-ruch-statku; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Izabela Zarantel     | 12 | ((200819-sekrety-orbitera-historia-prawdziwa; 200909-arystokratka-w-ladowni-na-swinie; 200916-smierc-raju; 200923-magiczna-burza-w-raju; 201021-noktianie-rodu-arlacz; 210616-nieudana-infiltracja-inferni; 210630-listy-od-fanow; 210922-ostatnia-akcja-bohaterki; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide)) |
| Maria Naavas         | 12 | ((201125-wyscig-jako-randka; 210120-sympozjum-zniszczenia; 210818-siostrzenica-morlana; 211110-romans-dzieki-esuriit; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide; 220316-potwor-czy-choroba-na-etaur; 220330-etaur-i-przyneta-na-krypte; 220622-lewiatan-za-pandore)) |
| Leszek Kurzmin       | 10 | ((200624-ratujmy-castigator; 200708-problematyczna-elena; 200819-sekrety-orbitera-historia-prawdziwa; 220928-kapitan-verlen-i-pojedynek-z-marine; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221214-astralna-flara-kontra-domina-lucis; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Maja Samszar         | 10 | ((210311-studenci-u-verlenow; 220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221019-kapitan-verlen-i-pierwszy-ruch-statku; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221130-astralna-flara-w-strefie-duchow; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Anastazja Sowińska   | 9 | ((200909-arystokratka-w-ladowni-na-swinie; 200916-smierc-raju; 200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz; 201104-sabotaz-swini; 201118-anastazja-bohaterka; 201210-pocalunek-aspirii; 201216-krypta-i-wyjec)) |
| Viorika Verlen       | 9 | ((210210-milosc-w-rodzie-verlen; 210224-sentiobsesja; 210311-studenci-u-verlenow; 210324-lustrzane-odbicie-eleny; 210331-elena-z-rodu-verlen; 230412-dzwiedzie-poluja-na-ser; 230419-karolinka-nieokielznana-swinka; 230426-mscizab-zabojca-arachnoziemskich-jaszczurow; 230524-ekologia-jaszczurow-w-arachnoziemie)) |
| Arnulf Perikas       | 8 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221130-astralna-flara-w-strefie-duchow)) |
| Diana Arłacz         | 8 | ((201021-noktianie-rodu-arlacz; 201104-sabotaz-swini; 201118-anastazja-bohaterka; 201125-wyscig-jako-randka; 201216-krypta-i-wyjec; 210106-sos-z-haremu; 210120-sympozjum-zniszczenia; 210127-porwanie-anastazji-z-odkupienia)) |
| Władawiec Diakon     | 8 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| AK Nocna Krypta      | 7 | ((191025-nocna-krypta-i-bohaterka; 200729-nocna-krypta-i-emulatorka; 201210-pocalunek-aspirii; 201216-krypta-i-wyjec; 201230-pulapka-z-anastazji; 210728-w-cieniu-nocnej-krypty; 210825-uszkodzona-brama-eteryczna)) |
| Olgierd Drongon      | 7 | ((200708-problematyczna-elena; 200722-wielki-kosmiczny-romans; 201125-wyscig-jako-randka; 210120-sympozjum-zniszczenia; 210630-listy-od-fanow; 210818-siostrzenica-morlana; 210922-ostatnia-akcja-bohaterki)) |
| Roland Sowiński      | 7 | ((210512-ewakuacja-z-serenit; 210609-sekrety-kariatydy; 210616-nieudana-infiltracja-inferni; 210922-ostatnia-akcja-bohaterki; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211215-sklejanie-inferni-do-kupy)) |
| Kajetan Kircznik     | 6 | ((221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221130-astralna-flara-w-strefie-duchow; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| OO Astralna Flara    | 6 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| OO Athamarein        | 6 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Otto Azgorn          | 6 | ((210421-znudzona-zaloga-inferni; 210526-morderstwo-na-inferni; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica; 211020-kurczakownia; 211027-rzieza-niszczy-infernie)) |
| Aleksandra Termia    | 5 | ((200826-nienawisc-do-swin; 201014-krystaliczny-gniew-elizy; 210804-infernia-jest-nasza; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica)) |
| Annika Pradis        | 5 | ((220126-keldan-voss-kolonia-saitaera; 220202-sekrety-keldan-voss; 220216-polityka-rujnuje-pallide-voss; 220223-stabilizacja-keldan-voss; 220309-upadek-eleny)) |
| Apollo Verlen        | 5 | ((210210-milosc-w-rodzie-verlen; 210224-sentiobsesja; 210311-studenci-u-verlenow; 230412-dzwiedzie-poluja-na-ser; 230419-karolinka-nieokielznana-swinka)) |
| Diana d'Infernia     | 5 | ((210804-infernia-jest-nasza; 210825-uszkodzona-brama-eteryczna; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik)) |
| Erwin Pies           | 5 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221019-kapitan-verlen-i-pierwszy-ruch-statku; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Flawia Blakenbauer   | 5 | ((210616-nieudana-infiltracja-inferni; 210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej; 211020-kurczakownia; 211027-rzieza-niszczy-infernie)) |
| Grażyna Burgacz      | 5 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Janus Krzak          | 5 | ((210707-po-drugiej-stronie-bramy; 210714-baza-zona-tres; 210721-pierwsza-bia-mag; 210728-w-cieniu-nocnej-krypty; 210901-stabilizacja-bramy-eterycznej)) |
| OO Królowa Kosmicznej Chwały | 5 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221019-kapitan-verlen-i-pierwszy-ruch-statku; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Raoul Lavanis        | 5 | ((210707-po-drugiej-stronie-bramy; 220216-polityka-rujnuje-pallide-voss; 220330-etaur-i-przyneta-na-krypte; 220610-ratujemy-porywaczy-eleny; 220622-lewiatan-za-pandore)) |
| Tomasz Sowiński      | 5 | ((210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear; 210421-znudzona-zaloga-inferni; 210818-siostrzenica-morlana)) |
| Adam Szarjan         | 4 | ((211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik)) |
| Alezja Dumorin       | 4 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Ataienne             | 4 | ((200916-smierc-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz; 201104-sabotaz-swini)) |
| Damian Orion         | 4 | ((200722-wielki-kosmiczny-romans; 200729-nocna-krypta-i-emulatorka; 200819-sekrety-orbitera-historia-prawdziwa; 201230-pulapka-z-anastazji)) |
| Ellarina Samarintael | 4 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze; 221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis; 221221-astralna-flara-i-nowy-komodor)) |
| Gabriel Lodowiec     | 4 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis)) |
| Marian Tosen         | 4 | ((210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210609-sekrety-kariatydy; 210616-nieudana-infiltracja-inferni)) |
| OO Tivr              | 4 | ((210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 211117-porwany-trismegistos; 220622-lewiatan-za-pandore)) |
| OO Żelazko           | 4 | ((200708-problematyczna-elena; 200722-wielki-kosmiczny-romans; 210818-siostrzenica-morlana; 210922-ostatnia-akcja-bohaterki)) |
| Romeo Verlen         | 4 | ((210210-milosc-w-rodzie-verlen; 210324-lustrzane-odbicie-eleny; 210331-elena-z-rodu-verlen; 221019-kapitan-verlen-i-pierwszy-ruch-statku)) |
| Szczepan Myrczek     | 4 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Szymon Wanad         | 4 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221130-astralna-flara-w-strefie-duchow)) |
| Tadeusz Ursus        | 4 | ((200722-wielki-kosmiczny-romans; 200819-sekrety-orbitera-historia-prawdziwa; 200826-nienawisc-do-swin; 210428-infekcja-serenit)) |
| Dariusz Krantak      | 3 | ((200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy; 210127-porwanie-anastazji-z-odkupienia)) |
| Eliza Ira            | 3 | ((200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz)) |
| Hubert Kerwelenios   | 3 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221130-astralna-flara-w-strefie-duchow)) |
| Jolanta Sowińska     | 3 | ((210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210421-znudzona-zaloga-inferni)) |
| Klarysa Jirnik       | 3 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221130-astralna-flara-w-strefie-duchow)) |
| Marian Fartel        | 3 | ((200916-smierc-raju; 200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy)) |
| Mateus Sarpon        | 3 | ((220126-keldan-voss-kolonia-saitaera; 220202-sekrety-keldan-voss; 220216-polityka-rujnuje-pallide-voss)) |
| Medea Sowińska       | 3 | ((200429-porwanie-cywila-z-kokitii; 210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej)) |
| NekroTAI Zarralea    | 3 | ((221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis; 221221-astralna-flara-i-nowy-komodor)) |
| OO Netrahina         | 3 | ((200715-sabotaz-netrahiny; 210818-siostrzenica-morlana; 210901-stabilizacja-bramy-eterycznej)) |
| Persefona d'Infernia | 3 | ((200708-problematyczna-elena; 210421-znudzona-zaloga-inferni; 210428-infekcja-serenit)) |
| Romana Arnatin       | 3 | ((210721-pierwsza-bia-mag; 210728-w-cieniu-nocnej-krypty; 210728-w-cieniu-nocnej-krypty)) |
| Stefan Torkil        | 3 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221019-kapitan-verlen-i-pierwszy-ruch-statku; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Szczepan Kaltaben    | 3 | ((220126-keldan-voss-kolonia-saitaera; 220216-polityka-rujnuje-pallide-voss; 220223-stabilizacja-keldan-voss)) |
| TAI Rzieza d'K1      | 3 | ((210630-listy-od-fanow; 211013-szara-nawalnica; 211027-rzieza-niszczy-infernie)) |
| Tomasz Ruppok        | 3 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej; 221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Ula Blakenbauer      | 3 | ((230412-dzwiedzie-poluja-na-ser; 230419-karolinka-nieokielznana-swinka; 230524-ekologia-jaszczurow-w-arachnoziemie)) |
| Wawrzyn Rewemis      | 3 | ((211110-romans-dzieki-esuriit; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko)) |
| AK Serenit           | 2 | ((210428-infekcja-serenit; 210512-ewakuacja-z-serenit)) |
| AK Wyjec             | 2 | ((201210-pocalunek-aspirii; 201216-krypta-i-wyjec)) |
| Anastazja Sowińska Dwa | 2 | ((201230-pulapka-z-anastazji; 210127-porwanie-anastazji-z-odkupienia)) |
| Bogdan Anatael       | 2 | ((210512-ewakuacja-z-serenit; 210630-listy-od-fanow)) |
| Dariusz Blakenbauer  | 2 | ((210311-studenci-u-verlenow; 210331-elena-z-rodu-verlen)) |
| Gerwazy Kircznik     | 2 | ((221102-astralna-flara-i-porwanie-na-karsztarinie; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Gilbert Bloch        | 2 | ((210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej)) |
| Juliusz Sowiński     | 2 | ((201021-noktianie-rodu-arlacz; 201210-pocalunek-aspirii)) |
| Karol Reichard       | 2 | ((210526-morderstwo-na-inferni; 211110-romans-dzieki-esuriit)) |
| Kasandra Destrukcja Diakon | 2 | ((211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik)) |
| Kirea Rialirat       | 2 | ((221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis)) |
| Klaudiusz Terienak   | 2 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Leo Kasztop          | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221130-astralna-flara-w-strefie-duchow)) |
| Lucjusz Blakenbauer  | 2 | ((210224-sentiobsesja; 210324-lustrzane-odbicie-eleny)) |
| Lutus Amerin         | 2 | ((211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide)) |
| Malictrix d'Pandora  | 2 | ((200610-ixiacka-wersja-malictrix; 210414-dekralotyzacja-asimear)) |
| Marcel Kulgard       | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Marcinozaur Verlen   | 2 | ((230412-dzwiedzie-poluja-na-ser; 230419-karolinka-nieokielznana-swinka)) |
| Mariusz Bulterier    | 2 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Martyna Bianistek    | 2 | ((210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear)) |
| Michał Perikas       | 2 | ((210311-studenci-u-verlenow; 210324-lustrzane-odbicie-eleny)) |
| Mściząb Verlen       | 2 | ((230426-mscizab-zabojca-arachnoziemskich-jaszczurow; 230524-ekologia-jaszczurow-w-arachnoziemie)) |
| Natalia Aradin       | 2 | ((211124-prototypowa-nereida-natalii; 220105-to-nie-pulapka-na-nereide)) |
| OA Zguba Tytanów     | 2 | ((201210-pocalunek-aspirii; 201230-pulapka-z-anastazji)) |
| OE Falołamacz        | 2 | ((210428-infekcja-serenit; 210512-ewakuacja-z-serenit)) |
| Ola Szerszeń         | 2 | ((220615-lewiatan-przy-seibert; 220622-lewiatan-za-pandore)) |
| OO Castigator        | 2 | ((200624-ratujmy-castigator; 200729-nocna-krypta-i-emulatorka)) |
| OO Loricatus         | 2 | ((221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| OO Wesoły Wieprzek   | 2 | ((201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz)) |
| Przemysław Czapurt   | 2 | ((210224-sentiobsesja; 210324-lustrzane-odbicie-eleny)) |
| Rafael Galwarn       | 2 | ((210609-sekrety-kariatydy; 210630-listy-od-fanow)) |
| Rafał Armadion       | 2 | ((200916-smierc-raju; 201104-sabotaz-swini)) |
| Robert Garwen        | 2 | ((200916-smierc-raju; 201104-sabotaz-swini)) |
| Rufus Komczirp       | 2 | ((200715-sabotaz-netrahiny; 201111-mychainee-na-netrahinie)) |
| Salazar Bolza        | 2 | ((221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| SCA Hadiah Emas      | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| SCA Płetwal Błękitny | 2 | ((210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear)) |
| SP Pallida Voss      | 2 | ((220126-keldan-voss-kolonia-saitaera; 220216-polityka-rujnuje-pallide-voss)) |
| Tal Marczak          | 2 | ((210526-morderstwo-na-inferni; 210707-po-drugiej-stronie-bramy)) |
| Tomasz Dojnicz       | 2 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221130-astralna-flara-w-strefie-duchow)) |
| Tomasz Kaltaben      | 2 | ((220223-stabilizacja-keldan-voss; 220309-upadek-eleny)) |
| Ulisses Kalidon      | 2 | ((210714-baza-zona-tres; 210728-w-cieniu-nocnej-krypty)) |
| Vigilus Mevilig      | 2 | ((210929-grupa-ekspedycyjna-kellert; 211020-kurczakownia)) |
| Wanessa Pyszcz       | 2 | ((200916-smierc-raju; 201021-noktianie-rodu-arlacz)) |
| Zygfryd Maus         | 2 | ((211117-porwany-trismegistos; 220126-keldan-voss-kolonia-saitaera)) |
| Ada Wyrocznik        | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Adam Chrząszczewicz  | 1 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja)) |
| Adam Nerawol         | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| Adragain Ferrias     | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Aida Serenit         | 1 | ((210512-ewakuacja-z-serenit)) |
| AK Nox Ignis         | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| AK Rodivas           | 1 | ((201230-pulapka-z-anastazji)) |
| AK Salamin           | 1 | ((200708-problematyczna-elena)) |
| Alan Nierkamin       | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Alara Ehmes          | 1 | ((200429-porwanie-cywila-z-kokitii)) |
| Aleksander Samszar   | 1 | ((230419-karolinka-nieokielznana-swinka)) |
| Aleksy Sartaran      | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Arkadia Verlen       | 1 | ((211124-prototypowa-nereida-natalii)) |
| Artur Traffal        | 1 | ((210804-infernia-jest-nasza)) |
| Atraksjusz Verlen    | 1 | ((230426-mscizab-zabojca-arachnoziemskich-jaszczurow)) |
| Atrius Kurunen       | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| Axel Nargan          | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |
| BIA Solitaria d'Zona Tres | 1 | ((210721-pierwsza-bia-mag)) |
| BIA XXX d'Zona Tres  | 1 | ((210714-baza-zona-tres)) |
| Brunhilda Verlen     | 1 | ((210210-milosc-w-rodzie-verlen)) |
| Celina Szilat        | 1 | ((200916-smierc-raju)) |
| Dominik Ogryz        | 1 | ((200106-infernia-martyn-hiwasser)) |
| Dominika Perikas     | 1 | ((220316-potwor-czy-choroba-na-etaur)) |
| Donald Parziarz      | 1 | ((201210-pocalunek-aspirii)) |
| Emilia Kariamon      | 1 | ((200122-kiepski-altruistyczny-terroryzm)) |
| Emmanuelle Gęsiawiec | 1 | ((230426-mscizab-zabojca-arachnoziemskich-jaszczurow)) |
| Eszara d'Seibert     | 1 | ((220615-lewiatan-przy-seibert)) |
| Fabian Rzelicki      | 1 | ((230524-ekologia-jaszczurow-w-arachnoziemie)) |
| Fantazjusz Verlen    | 1 | ((230419-karolinka-nieokielznana-swinka)) |
| Feliks Walrond       | 1 | ((210526-morderstwo-na-inferni)) |
| Finis Vitae          | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| Franciszek Maszkiet  | 1 | ((210804-infernia-jest-nasza)) |
| Frank Mgrot          | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Franz Verlen         | 1 | ((210210-milosc-w-rodzie-verlen)) |
| Gerard Adanor        | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| Gunnar Brunt         | 1 | ((201125-wyscig-jako-randka)) |
| Halina Szkwalnik     | 1 | ((201111-mychainee-na-netrahinie)) |
| Helena Adanor        | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| Henryk Sowiński      | 1 | ((201230-pulapka-z-anastazji)) |
| Hestia d'Atropos     | 1 | ((220610-ratujemy-porywaczy-eleny)) |
| Hestia d'Neotik      | 1 | ((211208-o-krok-za-daleko)) |
| Hind Szug Traf       | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Horacy Aktenir       | 1 | ((210106-sos-z-haremu)) |
| Jakub Bulgocz        | 1 | ((220610-ratujemy-porywaczy-eleny)) |
| Jamon Korab          | 1 | ((211117-porwany-trismegistos)) |
| Jarosław Szarjan     | 1 | ((211208-o-krok-za-daleko)) |
| Jolanta Arłacz       | 1 | ((201021-noktianie-rodu-arlacz)) |
| Jolanta Kopiec       | 1 | ((210922-ostatnia-akcja-bohaterki)) |
| Jonasz Parys         | 1 | ((220615-lewiatan-przy-seibert)) |
| Julia Aktenir        | 1 | ((210106-sos-z-haremu)) |
| Julian Muszel        | 1 | ((200722-wielki-kosmiczny-romans)) |
| Kalira d'Trismegistos | 1 | ((211117-porwany-trismegistos)) |
| Kaspian Certisarius  | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| Katra Igneus         | 1 | ((201210-pocalunek-aspirii)) |
| Kijara d'Esuriit     | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Klara Gwozdnik       | 1 | ((211117-porwany-trismegistos)) |
| Klaus Rumak          | 1 | ((201021-noktianie-rodu-arlacz)) |
| Konrad Wolczątek     | 1 | ((200722-wielki-kosmiczny-romans)) |
| Kormonow Voss        | 1 | ((220126-keldan-voss-kolonia-saitaera)) |
| Krucjusz Verlen      | 1 | ((210210-milosc-w-rodzie-verlen)) |
| Krystian Blakenbauer | 1 | ((210331-elena-z-rodu-verlen)) |
| Lamia Akacja         | 1 | ((220330-etaur-i-przyneta-na-krypte)) |
| Lana Mirkinin        | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Laura Orion          | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Leszek Czarban       | 1 | ((220330-etaur-i-przyneta-na-krypte)) |
| Llarnagraht          | 1 | ((210414-dekralotyzacja-asimear)) |
| Lothar Diakon        | 1 | ((200715-sabotaz-netrahiny)) |
| Maciej Żarand        | 1 | ((210804-infernia-jest-nasza)) |
| Maciek Kalmarzec     | 1 | ((211222-kult-saitaera-w-neotik)) |
| Marcel Biekakis      | 1 | ((230524-ekologia-jaszczurow-w-arachnoziemie)) |
| Marcelina Trzęsiel   | 1 | ((221019-kapitan-verlen-i-pierwszy-ruch-statku)) |
| Maria Gołąb          | 1 | ((201021-noktianie-rodu-arlacz)) |
| Marianna Lemurczak   | 1 | ((200923-magiczna-burza-w-raju)) |
| Mariusz Tubalon      | 1 | ((210414-dekralotyzacja-asimear)) |
| Martauron Attylla    | 1 | ((200122-kiepski-altruistyczny-terroryzm)) |
| Mateusz Sowiński     | 1 | ((200415-sabotaz-miecza-swiatla)) |
| Melania Akacja       | 1 | ((220316-potwor-czy-choroba-na-etaur)) |
| Melwin Sito          | 1 | ((200610-ixiacka-wersja-malictrix)) |
| Michał Teriakin      | 1 | ((210630-listy-od-fanow)) |
| Milena Blakenbauer   | 1 | ((210224-sentiobsesja)) |
| Miłosz Klinek        | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Mira Anastel         | 1 | ((211117-porwany-trismegistos)) |
| Mirela Orion         | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Morrigan d'Tirakal   | 1 | ((210421-znudzona-zaloga-inferni)) |
| Mścigrom Verlen      | 1 | ((210224-sentiobsesja)) |
| Natalia Miszryk      | 1 | ((200122-kiepski-altruistyczny-terroryzm)) |
| Nataniel Morlan      | 1 | ((210818-siostrzenica-morlana)) |
| Nikodem Sowiński     | 1 | ((200923-magiczna-burza-w-raju)) |
| OA Bakałarz          | 1 | ((210120-sympozjum-zniszczenia)) |
| OA Odkupienie        | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| OE Lord Savaron      | 1 | ((210630-listy-od-fanow)) |
| OE Piękna Elena      | 1 | ((210428-infekcja-serenit)) |
| Ofelia Morlan        | 1 | ((210818-siostrzenica-morlana)) |
| Olena Orion          | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| Oliwia Karelan       | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| ON Spatium Gelida    | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| OO Alaya             | 1 | ((210519-osiemnascie-oczu)) |
| OO Alkaris           | 1 | ((200624-ratujmy-castigator)) |
| OO Aurelion          | 1 | ((200708-problematyczna-elena)) |
| OO Galaktyczny Tucznik | 1 | ((200909-arystokratka-w-ladowni-na-swinie)) |
| OO Kanagar           | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| OO Kariatyda         | 1 | ((210609-sekrety-kariatydy)) |
| OO Karsztarin        | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| OO Kastor            | 1 | ((220126-keldan-voss-kolonia-saitaera)) |
| OO Mfumo             | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| OO Minerwa           | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| OO Omega Septius     | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| OO Opresor           | 1 | ((210616-nieudana-infiltracja-inferni)) |
| OO Optymistyczny Żuk | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| OO Pandora           | 1 | ((220622-lewiatan-za-pandore)) |
| OO Samotność Gwiazd  | 1 | ((210804-infernia-jest-nasza)) |
| OO Straszliwy Pająk  | 1 | ((220622-lewiatan-za-pandore)) |
| OO Szalony Rumak     | 1 | ((201021-noktianie-rodu-arlacz)) |
| OO Trasman           | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| OO Tucznik Trzeci    | 1 | ((221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| OO Tvarana           | 1 | ((200715-sabotaz-netrahiny)) |
| OO Welgat            | 1 | ((200722-wielki-kosmiczny-romans)) |
| Percival Diakon      | 1 | ((200715-sabotaz-netrahiny)) |
| Persefona d'Loricatus | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Rafał Grambucz       | 1 | ((210428-infekcja-serenit)) |
| Rafał Perikas        | 1 | ((210311-studenci-u-verlenow)) |
| Remigiusz Błyszczyk  | 1 | ((211013-szara-nawalnica)) |
| Remigiusz Falorin    | 1 | ((210630-listy-od-fanow)) |
| Robert Arłacz        | 1 | ((201021-noktianie-rodu-arlacz)) |
| Rozalia Teirik       | 1 | ((210106-sos-z-haremu)) |
| Rozalia Wączak       | 1 | ((200624-ratujmy-castigator)) |
| Rufus Niegnat        | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| Rufus Samszar        | 1 | ((210311-studenci-u-verlenow)) |
| Rufus Warkoczyk      | 1 | ((221019-kapitan-verlen-i-pierwszy-ruch-statku)) |
| Rzeźnik Parszywiec Diakon | 1 | ((220622-lewiatan-za-pandore)) |
| Sabina Servatel      | 1 | ((200819-sekrety-orbitera-historia-prawdziwa)) |
| Sabrina Ferrias      | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Saitaer              | 1 | ((220105-to-nie-pulapka-na-nereide)) |
| Sargon Niiris        | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| Sarian Xadaar        | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |
| SC Fecundatis        | 1 | ((210818-siostrzenica-morlana)) |
| SC Światłodóbr       | 1 | ((210818-siostrzenica-morlana)) |
| SC Trismegistos      | 1 | ((211117-porwany-trismegistos)) |
| SCA Isigtand         | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Sebastian Alarius    | 1 | ((200826-nienawisc-do-swin)) |
| Sebastian Namczek    | 1 | ((200408-o-psach-i-krysztalach)) |
| Seraf Verlen         | 1 | ((210210-milosc-w-rodzie-verlen)) |
| Seweryn Atanair      | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| SP Plugawy Jaszczur  | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| SP Światło Nadziei   | 1 | ((220223-stabilizacja-keldan-voss)) |
| Stefan Jamniczek     | 1 | ((201118-anastazja-bohaterka)) |
| Suwan Chankar        | 1 | ((220316-potwor-czy-choroba-na-etaur)) |
| Sylwia Perikas       | 1 | ((210311-studenci-u-verlenow)) |
| Szczepan Myksza      | 1 | ((200715-sabotaz-netrahiny)) |
| Szczepan Olgrod      | 1 | ((200106-infernia-martyn-hiwasser)) |
| TAI Marszałek Grzmotoszpon Trzeci | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| TAI XT-723 d'K1      | 1 | ((210630-listy-od-fanow)) |
| TAI Zefiris          | 1 | ((210630-listy-od-fanow)) |
| Tristan Rialirat     | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |
| Tymek Samszar        | 1 | ((230419-karolinka-nieokielznana-swinka)) |
| Tymoteusz Czerw      | 1 | ((220316-potwor-czy-choroba-na-etaur)) |
| Wioletta Keiril      | 1 | ((200106-infernia-martyn-hiwasser)) |
| Wojciech Kuszar      | 1 | ((191025-nocna-krypta-i-bohaterka)) |
# Olaf Zuchwały
## Identyfikator

Id: 2306-olaf-zuchwaly

## Sekcja Opowieści

### Nawoływanie Trzęsawiska

* **uid:** 200417-nawolywanie-trzesawiska, _numer względny_: 10
* **daty:** 0110-08-08 - 0110-08-10
* **obecni:** Gabriel Ursus, Ignacy Myrczek, Napoleon Bankierz, Olaf Zuchwały, Pięknotka Diakon, Sabina Kazitan, Strażniczka Alair

Streszczenie:

Trzęsawisko Zjawosztup zostało Skrzywdzone przez ostatnie wydarzenia. Zaczęło wabić Sabinę i Ignacego by ich pożreć. Szczęśliwie, Pięknotka zdążyła dotrzeć do Sabiny (która wabiona szukała Ignacego) i uratowała ich przed Trzęsawiskiem. Dodatkowo Pięknotka poznała sekret Sabiny i zdobyła nad nią absolutną kontrolę.

Aktor w Opowieści:

* Dokonanie:
    * znalazł nić porozumienia z Sabiną Kazitan, mówiąc jej, że nawet on - noktianin - został zaakceptowany. Sabina uciekła od niego, bo bała się zaprzyjaźnić.


### Kontrpolowanie Pięknotki - pułapka

* **uid:** 191103-kontrpolowanie-pieknotki-pulapka, _numer względny_: 9
* **daty:** 0110-06-20 - 0110-06-27
* **obecni:** Damian Orion, Józef Małmałaz, Ksenia Kirallen, Lilia Ursus, Marek Puszczok, Mariusz Trzewń, Minerwa Metalia, Olaf Zuchwały, Pięknotka Diakon, Rafał Roszczeniok

Streszczenie:

Pięknotka poluje na noktiańskiego zabójcę. W ciągu tygodnia udało jej się złożyć do kupy pułapkę, rozpocząć linię przerzutową artefaktów do Aurum, połączyć Orbiter, Lilię, Minerwę oraz Pustogor i doprowadzić do tego, że docelowo Małmałaz ma zostać złapany i doprowadzony do Pustogoru w zemście za krzywdę Alana. To był bardzo trudny tydzień, politycznie.

Aktor w Opowieści:

* Dokonanie:
    * postawił się Pięknotce - nie chce, by Enklawy ucierpiały przez Małmałaza. Powiedział Pięknotce i Kseni to, co one potrzebowały wiedzieć - ale nie zdradził niczego z Enklaw.


### Zagubiony efemerydyta

* **uid:** 190917-zagubiony-efemerydyta, _numer względny_: 8
* **daty:** 0110-06-17 - 0110-06-18
* **obecni:** Gabriel Ursus, Jan Uszczar, Marek Puszczok, Olaf Zuchwały, Pięknotka Diakon

Streszczenie:

Młody efemerydyta uciekł z Eterni. Był przyciskany by dołączyć do mafii więc czasem w panice rzucał niewłaściwe zaklęcia w złych momentach. Pięknotka go znalazła i zajęła się nim - oddała go Orbiterowi, bo tam się może przydać. Mafia musi się obejść smakiem.

Aktor w Opowieści:

* Dokonanie:
    * właściciel baru, który nie chce krzywdy Janka ale nie da się okradać. Współpracuje z Pięknotką by złapać 17-latka.


### Wojna Kajrata

* **uid:** 190622-wojna-kajrata, _numer względny_: 7
* **daty:** 0110-05-14 - 0110-05-17
* **obecni:** Ernest Kajrat, Liliana Bankierz, Nikola Kirys, Olaf Zuchwały, Pięknotka Diakon, Serafina Ira

Streszczenie:

Kajrat zaeskalował; ściągnął Lilianę i powiedział Pięknotce, że Liliana zostanie jego podwładną jeśli Serafina nie osiągnie sukcesu. Pięknotka poznała historię Serafiny - kiedyś agentka Pustogoru, w wyniku burzy w Pacyfice straciła przyjaciół. Teraz jest cieniem dawnej osoby. Po drodze, w Wolnych Ptakach, Pięknotkę zaatakowała grupa wolnych magów; uruchomił się Cień i Pięknotka zrobiła masakrę. To sprawiło większe problemy i niedogadanie między Enklawami a Pustogorem

Aktor w Opowieści:

* Dokonanie:
    * który ciężko pracował nad integracją ludzi i magów z Enklaw z Pustogorem, ale jego plany właśnie legły w gruzach przez działania Pięknotki.


### Pierwszy Emulator Orbitera

* **uid:** 190502-pierwszy-emulator-orbitera, _numer względny_: 6
* **daty:** 0110-04-10 - 0110-04-12
* **obecni:** Alan Bartozol, Bożymir Szczupak, Minerwa Metalia, Nikola Kirys, Olaf Zuchwały, Pięknotka Diakon

Streszczenie:

Idąc śladami Elizy Pięknotka natrafiła na Nikolę - o której dawni przyjaciele (Olaf) mówią, że "zginęła", coś jej zrobiono. Pięknotka doszła do tego, że Nikola jest projektem Emulator; dowiedziała się, że Emulatory były budowane m.in. przez Minerwę. Pięknotka doszła do tego, że zintegrowana z Finis Vitae Nikola została częściowo uwolniona przez wpływ Arazille na Finis Vitae. Gdy spotkała się z Nikolą - szok. Cień zareagował. Nikola odzyskała wolność po kontakcie z Cieniem, ale Pięknotka go utrzymała. Cień ma w sobie "emocje" spętanych Emulatorów. Pięknotce udało się wyperswadować Nikoli budzenie autowara lub niszczenie Astorii; zwiadowczyni jednak odjechała w ogromnej konfuzji i nie wiedząc, co teraz robić.

Aktor w Opowieści:

* Dokonanie:
    * ma ogromny uraz do Orbitera za to, co stało się Nikoli dawno temu; powiedział Pięknotce, że Nikola nie żyje a to co tam jest to duch.


### Sabotaż szeptów Elizy

* **uid:** 190429-sabotaz-szeptow-elizy, _numer względny_: 5
* **daty:** 0110-04-06 - 0110-04-09
* **obecni:** Alan Bartozol, Aleksander Rugczuk, Diana Tevalier, Eliza Ira, Erwin Galilien, Karla Mrozik, Lucjusz Blakenbauer, Minerwa Metalia, Olaf Zuchwały, Pięknotka Diakon

Streszczenie:

Chevaleresse wpakowała się w "kult Elizy" grając z ludźmi online; powiedziała o tym Pięknotce. Pięknotka odkryła, że Eliza rozprzestrzenia swoje wpływy przez kryształy w formie biżuterii; wraz z Alanem, Erwinem i (nadal ranną) Minerwą zaprojektowali mechanizm uszkadzający sieć krystaliczną Elizy. Udało im się kupić jakiś miesiąc czasu, aż Orbiter i Pustogor znajdą lepsze rozwiązanie. Sama Chevaleresse ma się nie spotykać z takimi tam. Ale może z nimi grać online.

Aktor w Opowieści:

* Dokonanie:
    * kiedyś wysoki oficer Inwazji, teraz spokojny barman chcący współpracować z Pustogorem. Nie chce powrotu wojny. Pomógł Pięknotce w pozyskaniu Kryształu Elizy.


### Budowa ixiońskiego mimika

* **uid:** 190424-budowa-ixionskiego-mimika, _numer względny_: 4
* **daty:** 0110-03-30 - 0110-04-01
* **obecni:** Aleksander Rugczuk, Erwin Galilien, Karla Mrozik, Minerwa Metalia, Olaf Zuchwały, Pięknotka Diakon

Streszczenie:

W świetle narastających napięć w Pustogorze i czarnych artefaktów na wolności, Pięknotka wykorzystuje (za aprobatą Barbakanu) mimika który zdominował Marcela. Minerwa go psychotronicznie osłabia z perspektywy morderstw i infekuje go energią ixiońską. Pięknotka straciła go przy Dzielnicy Uciechy, ale i tak zrobił swoje (choć straty materialne są większe). Zainfekowany przez mimika wykrzyczał, że Eliza Ira wróciła oraz Saitaer się przebudził. Niepokojące dla Pięknotki.

Aktor w Opowieści:

* Dokonanie:
    * kiedyś, członek Inwazji Noctis. Teraz sympatyczny barman; postawił się terrorformowi/ mimikowi i powiedział, że nie o to walczyła Eliza. Skończył ciężko ranny.
* Progresja:
    * tydzień w szpitalu


### Pustogorski Konflikt

* **uid:** 190422-pustogorski-konflikt, _numer względny_: 3
* **daty:** 0110-03-29 - 0110-03-30
* **obecni:** Erwin Galilien, Karla Mrozik, Marcel Nieciesz, Olaf Zuchwały, Pięknotka Diakon, Wojmił Siwywilk

Streszczenie:

Napięcia na linii Barbakan - Miasteczkowcy w Pustogorze są silne; działania Wiktora jedynie przyspieszyły konflikt. Na rynek dostało się sporo niebezpiecznych artefaktów i Pięknotka musiała pomóc grupie Miasteczkowców. Zdesperowani, złapali terminusa i zamknęli go w piwnicy bo podczas jednego z rajdów na Miasteczkowców magowie z Fortu Mikado porwali dwie dziewczyny. Okazało się, że za tym stoi zaawansowany mimik symbiotyczny; Pięknotka z pomocą Cienia pokonała Skażonego terminusa i uwolniła czarodziejki.

Aktor w Opowieści:

* Dokonanie:
    * wyrósł na nieformalnego przywódcę Miasteczkowców; negocjował z Pięknotką i powiedział jej o tragedii dwóch Miasteczkowiczanek.


### Morderczyni-jednej-plotki

* **uid:** 190101-morderczyni-jednej-plotki, _numer względny_: 2
* **daty:** 0109-12-12 - 0109-12-16
* **obecni:** Aleksander Iczak, Erwin Galilien, Karol Szurnak, Olaf Zuchwały, Pięknotka Diakon, Teresa Mieralit

Streszczenie:

Na temat Pięknotki i jej salonu zaczęto rozpuszczać nieprzyjemne plotki. Pięknotka zlokalizowała jedno ze źródeł i je pokazowo zniszczyła, zmuszając maga do przepraszania i płaczu na kolanach. Dodatkowo, Czerwone Myszy oraz Dare Shiver zaczęli interesować się Pięknotką i jej salonem. A sama Pięknotka przecięła "największą nemesis" Adeli Kirys.

Aktor w Opowieści:

* Dokonanie:
    * właściciel Góskiej Szalupy. Ma topór. Stylizuje się na kuriozum godne Pustogoru. Fajny facet. Pomógł Pięknotce znaleźć Szurnaka i zrobić pojedynek.


### Reality show z zaskoczenia

* **uid:** 230618-reality-show-z-zaskoczenia, _numer względny_: 1
* **daty:** 0083-10-02 - 0083-10-07
* **obecni:** Alan Klart, Lily Sanarton, Maja Wurmramin, Maks Ardyceń, Olaf Zuchwały, Roman Wyrkmycz

Streszczenie:

Grupa porwanych przegrywów życiowych obudziła się na nieznanym terenie, w kształcie planszy 3x3, gdzie wraz z innymi grupami przegrywów mieli rywalizować o to, który zespół przetrwa najdłużej. Olaf ustabilizował swój zespół na poziomie morale i jako dekadianin filozoficznie akceptuje trudny czas. Domyślił się, że to reality show i próbował wpłynąć na widzów. Zespół zszedł z gór do lasu namorzynowego, ale zdecydowali się wrócić w góry i uratować kogo się da...

Aktor w Opowieści:

* Dokonanie:
    * zapijaczony stoczony dekadianin, porwany do reality show. O dziwo, integruje zespół i podnosi mu morale zamiast zginąć w piątej minucie. Uratował sporo osób przed błędami i śmiercią, po czym zrobił z nich zgrany oddział. Wpadł na reality show i kombinował jak przekonać magów by wszyscy przetrwali. Zaadaptował się i próbuje wszystkich uratować.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 10, @: 0110-08-10
    1. Primus    : 10, @: 0110-08-10
        1. Sektor Astoriański    : 10, @: 0110-08-10
            1. Astoria    : 10, @: 0110-08-10
                1. Sojusz Letejski, SW    : 2, @: 0110-05-17
                    1. Granica Anomalii    : 2, @: 0110-05-17
                        1. Pacyfika, obrzeża    : 1, @: 0110-04-12
                        1. Wolne Ptaki    : 2, @: 0110-05-17
                            1. Królewska Baza    : 2, @: 0110-05-17
                1. Sojusz Letejski    : 10, @: 0110-08-10
                    1. Szczeliniec    : 9, @: 0110-08-10
                        1. Powiat Pustogorski    : 8, @: 0110-08-10
                            1. Pustogor    : 7, @: 0110-08-10
                                1. Dolina Uciech    : 1, @: 0110-06-18
                                1. Eksterior    : 4, @: 0110-08-10
                                    1. Dzielnica Uciechy    : 1, @: 0110-04-01
                                    1. Fort Mikado    : 1, @: 0110-04-01
                                    1. Miasteczko    : 4, @: 0110-08-10
                                        1. Górska Szalupa    : 1, @: 0110-03-30
                                        1. Knajpa Górska Szalupa    : 1, @: 0110-08-10
                                    1. Zamek Weteranów    : 1, @: 0110-08-10
                                1. Fort Mikado    : 1, @: 0110-06-18
                                1. Gabinet Pięknotki    : 1, @: 0109-12-16
                                1. Interior    : 3, @: 0110-04-09
                                    1. Bunkry Barbakanu    : 2, @: 0110-04-09
                                    1. Dzielnica Mieszkalna    : 1, @: 0110-03-30
                                    1. Laboratorium Senetis    : 2, @: 0110-04-09
                                1. Knajpa Górska Szalupa    : 2, @: 0110-06-18
                                1. Rdzeń    : 5, @: 0110-08-10
                                    1. Barbakan    : 2, @: 0110-04-01
                                    1. Szpital Terminuski    : 3, @: 0110-08-10
                            1. Zaczęstwo    : 3, @: 0110-08-10
                                1. Kawiarenka Leopold    : 1, @: 0110-05-17
                                1. Nieużytki Staszka    : 2, @: 0110-08-10
                        1. Pustogor    : 1, @: 0110-06-27

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Pięknotka Diakon     | 9 | ((190101-morderczyni-jednej-plotki; 190422-pustogorski-konflikt; 190424-budowa-ixionskiego-mimika; 190429-sabotaz-szeptow-elizy; 190502-pierwszy-emulator-orbitera; 190622-wojna-kajrata; 190917-zagubiony-efemerydyta; 191103-kontrpolowanie-pieknotki-pulapka; 200417-nawolywanie-trzesawiska)) |
| Erwin Galilien       | 4 | ((190101-morderczyni-jednej-plotki; 190422-pustogorski-konflikt; 190424-budowa-ixionskiego-mimika; 190429-sabotaz-szeptow-elizy)) |
| Minerwa Metalia      | 4 | ((190424-budowa-ixionskiego-mimika; 190429-sabotaz-szeptow-elizy; 190502-pierwszy-emulator-orbitera; 191103-kontrpolowanie-pieknotki-pulapka)) |
| Karla Mrozik         | 3 | ((190422-pustogorski-konflikt; 190424-budowa-ixionskiego-mimika; 190429-sabotaz-szeptow-elizy)) |
| Alan Bartozol        | 2 | ((190429-sabotaz-szeptow-elizy; 190502-pierwszy-emulator-orbitera)) |
| Aleksander Rugczuk   | 2 | ((190424-budowa-ixionskiego-mimika; 190429-sabotaz-szeptow-elizy)) |
| Gabriel Ursus        | 2 | ((190917-zagubiony-efemerydyta; 200417-nawolywanie-trzesawiska)) |
| Marek Puszczok       | 2 | ((190917-zagubiony-efemerydyta; 191103-kontrpolowanie-pieknotki-pulapka)) |
| Nikola Kirys         | 2 | ((190502-pierwszy-emulator-orbitera; 190622-wojna-kajrata)) |
| Alan Klart           | 1 | ((230618-reality-show-z-zaskoczenia)) |
| Aleksander Iczak     | 1 | ((190101-morderczyni-jednej-plotki)) |
| Bożymir Szczupak     | 1 | ((190502-pierwszy-emulator-orbitera)) |
| Damian Orion         | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Diana Tevalier       | 1 | ((190429-sabotaz-szeptow-elizy)) |
| Eliza Ira            | 1 | ((190429-sabotaz-szeptow-elizy)) |
| Ernest Kajrat        | 1 | ((190622-wojna-kajrata)) |
| Ignacy Myrczek       | 1 | ((200417-nawolywanie-trzesawiska)) |
| Jan Uszczar          | 1 | ((190917-zagubiony-efemerydyta)) |
| Józef Małmałaz       | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Karol Szurnak        | 1 | ((190101-morderczyni-jednej-plotki)) |
| Ksenia Kirallen      | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Lilia Ursus          | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Liliana Bankierz     | 1 | ((190622-wojna-kajrata)) |
| Lily Sanarton        | 1 | ((230618-reality-show-z-zaskoczenia)) |
| Lucjusz Blakenbauer  | 1 | ((190429-sabotaz-szeptow-elizy)) |
| Maja Wurmramin       | 1 | ((230618-reality-show-z-zaskoczenia)) |
| Maks Ardyceń         | 1 | ((230618-reality-show-z-zaskoczenia)) |
| Marcel Nieciesz      | 1 | ((190422-pustogorski-konflikt)) |
| Mariusz Trzewń       | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Napoleon Bankierz    | 1 | ((200417-nawolywanie-trzesawiska)) |
| Rafał Roszczeniok    | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Roman Wyrkmycz       | 1 | ((230618-reality-show-z-zaskoczenia)) |
| Sabina Kazitan       | 1 | ((200417-nawolywanie-trzesawiska)) |
| Serafina Ira         | 1 | ((190622-wojna-kajrata)) |
| Strażniczka Alair    | 1 | ((200417-nawolywanie-trzesawiska)) |
| Teresa Mieralit      | 1 | ((190101-morderczyni-jednej-plotki)) |
| Wojmił Siwywilk      | 1 | ((190422-pustogorski-konflikt)) |
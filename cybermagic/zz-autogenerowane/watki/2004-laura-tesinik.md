# Laura Tesinik
## Identyfikator

Id: 2004-laura-tesinik

## Sekcja Opowieści

### Pierwszy tajemniczy wielbiciel Liliany

* **uid:** 230212-pierwszy-tajemniczy-wielbiciel-liliany, _numer względny_: 9
* **daty:** 0111-10-18 - 0111-10-20
* **obecni:** Julia Kardolin, Laura Tesinik, Liliana Bankierz, Mariusz Kupieczka, Triana Porzecznik

Streszczenie:

Liliana, Triana, Julia pracują nad formą integracji scutiera i lancera by Liliana mogła pokonać (lub choć wytrzymać) Arkadię Verlen. Triana dostała alarm ze swojej zaginiętej drony o nieprzytomnych ludziach na Nieużytkach. Okazuje się, że portret Liliany wyssał tych ludzi. Okazuje się, że student AMZ, Mariusz, zrobił portret by Lilianę poderwać, ale spartaczył; porter wysysa. Potem portret został ukradziony. Liliana dalej podejrzewa spiski. Gdy Liliana i Mariusz się konfrontują, okazuje się że były 3 portrety. Liliana zachowuje się nikczemnie wobec Mariusza, więc się szybko odkochał. Ona nie skojarzyła, że komuś się może podobać.

Aktor w Opowieści:

* Dokonanie:
    * ściągnięta jako terminuska do problemów z portretem Liliany. Z uwagi na przeszłość w Cieniaszczycie powiedziała Julii dane które pomogą jej i Lilianie w dojściu do tego kto stoi za tym wszystkim. Nie podaje własnych opinii i chowa się za aspektami prawnymi. Nie jest ich koleżanką.


### Serafina staje za Wydrami

* **uid:** 210831-serafina-staje-za-wydrami, _numer względny_: 8
* **daty:** 0111-07-03 - 0111-07-06
* **obecni:** Daniel Terienak, Halina Sermniek, Karolina Terienak, Laura Tesinik, Lorena Gwozdnik, Marysia Sowińska, Rafał Torszecki, Serafina Ira

Streszczenie:

Karolina MUSI poznać prawdę co się stało z jej bratem. Odkryła, że istnieje grupa Wydry polująca na Rekiny, ludzi skrzywdzonych przez Rekiny w przeszłości. I pomaga im Serafina Ira. Z Marysią porozmawiały z Serafiną i przekonały ją, by ta działała wolniej - i drugą linią napuściły na Serafinę terminusów z Pustogoru.

Aktor w Opowieści:

* Dokonanie:
    * z rozkazu Tukana wysłała Marysi info jak zginęli ludzie tworzący mandragorę; niewyjaśnione. Pustogor ich ukrył? Laura nie jest szczęśliwa z tego jak działa Tukan, ale jest oportunistką.


### Mandragora nienawidzi Rekinów

* **uid:** 210824-mandragora-nienawidzi-rekinow, _numer względny_: 7
* **daty:** 0111-06-29 - 0111-07-01
* **obecni:** Amelia Sowińska, Daniel Terienak, Ekaterina Zajcew, Karolina Terienak, Laura Tesinik, Marysia Sowińska, Sensacjusz Diakon, Tomasz Tukan

Streszczenie:

Mandragora nienawidzi Rekinów i jej ofiarą został Damian Terienak. Idąc za jego śladem Marysia i Karolina znalazły mandragorę i pnączoszpona oraz z pomocą dwóch uczennic terminusa rozwaliły problem. Dowiedziały się, że mandragora konkretnie celuje w Amelię Sowińską, która opuściła ten teren dawno temu (i która była dość okrutna).

Aktor w Opowieści:

* Dokonanie:
    * cyniczna do bólu na Tukana i Ekaterinę. Dobrze zaplanowała pułapkę na pnączoszpona. Bezradna w obliczu duchów. Doskonała taktycznie.


### Zgubiony holokryształ w lesie

* **uid:** 210817-zgubiony-holokrysztal-w-lesie, _numer względny_: 6
* **daty:** 0111-06-18 - 0111-06-20
* **obecni:** Alan Bartozol, Cyryl Perikas, Kacper Bankierz, Laura Tesinik, Liliana Bankierz, Mariusz Trzewń, Marysia Sowińska, Rupert Mysiokornik, Stella Armadion, Tomasz Tukan

Streszczenie:

Jeden z przerzucanych przez mafię przedmiotów do Aurum to ko-matryca Kuratorów, jako broń. Ale Cyryl - kurier - był ofiarą terminuskiego polowania więc ją odrzucił w Lesie Trzęsawnym gdzie znalazła ją Stella przekonana, że to noktiańska technologia. Marysia wypchnęła Kacpra Bankierza z mafii (i tego terenu) a Tukan znalazł i usunął ko-matrycę m.in. używając Laury.

Aktor w Opowieści:

* Dokonanie:
    * uruchomiona przez Tukana by znalazła ko-matrycę Kuratorów wśród studentów AMZ; znalazła ślad wskazujący na Lilianę i Stellę. Przyjaciółka Liliany "od zawsze".
* Progresja:
    * przyjaciółka Liliany Bankierz "od zawsze".


### Dezinhibitor dla Sabiny

* **uid:** 201006-dezinhibitor-dla-sabiny, _numer względny_: 5
* **daty:** 0110-10-03 - 0110-10-05
* **obecni:** Aranea Diakon, Gabriel Ursus, Ignacy Myrczek, Justynian Diakon, Karolina Erenit, Laura Tesinik, Lorena Gwozdnik, Napoleon Bankierz, Rafał Kumczek, Sabina Kazitan

Streszczenie:

Zanim Sabina wróci do Aurum, Rekiny Paladyni chcieli ją przyskrzynić - podać jej dezinhibitor w towarzystwie Myrczka i to nagrać, z nadzieją, że się ją zamknie. Gabriel i Laura wykryli intrygę i zatrzymali ją, nie dopuszczając do dezinhibicji Sabiny. O dziwo, tien Kazitan chciała Myrczka CHRONIĆ.

Aktor w Opowieści:

* Dokonanie:
    * głos sumienia Gabriela jak chodzi o Sabinę Kazitan - tym razem jest niewinna. Wykryła skanując serratusa dronami, że Sabina złamała parol.
* Progresja:
    * znajomość i szacunek wśród uczniów Akademii Magii w Zaczęstwie. Jest "jedną z nich", ale fajniejsza (transfer student kiedyś).
    * ma materiały do szantażu i "wiszą jej" Rekiny z frakcji Justyniana Diakona.


### Bardzo straszna mysz

* **uid:** 200616-bardzo-straszna-mysz, _numer względny_: 4
* **daty:** 0110-09-14 - 0110-09-17
* **obecni:** Diana Lauris, Franek Bulterier, Gabriel Ursus, Henryk Wkrąż, Ksenia Kirallen, Laura Tesinik, Matylda Sęk

Streszczenie:

Kilka Rekinów chciało się podlizać Gerwazemu Lemurczakowi i postraszyć Dianę i Melindę. Diana w odpowiedzi sprawiła, że ich eksperymentalna 'mysz Esuriit' uciekła i zaczęła stanowić prawdziwe zagrożenie. W REAKCJI NA TO Laura i Gabriel rozbroili sytuację, wykryli obecność Diany i zatarli wszelkie ślady. Diana jednak nie wybacza i nie zapomina a Henryk też chce iść w temat dalej...

Aktor w Opowieści:

* Dokonanie:
    * ratowała reputację Tukana jak mogła; integrowała się z Krystalizatorem i wykryła, że twórcą myszy Esuriit jest Henryk ale w cieniu stoi Diana.


### Dom dla Melindy

* **uid:** 200524-dom-dla-melindy, _numer względny_: 3
* **daty:** 0110-09-09 - 0110-09-12
* **obecni:** Diana Lemurczak, Diana Tevalier, Franek Bulterier, Gerwazy Lemurczak, Laura Tesinik, Mariusz Trzewń, Melinda Teilert, Pięknotka Diakon

Streszczenie:

Rodzina chce Melindę z powrotem, Melinda nie chce wracać. Pięknotka szuka Melindy i znalazła ją w Arkadii. Gdy poznała sytuację, we współpracy z Dianą wsadziła Melindę Alanowi przy użyciu Chevaleresse. Wszyscy są zadowoleni, choć Melinda musi opuścić Podwiert - ale wreszcie jest bezpieczna.

Aktor w Opowieści:

* Dokonanie:
    * Tukan chciał chronić Melindę przed Aurum. Pięknotka też. Laura całkowicie z własnej inicjatywy zablokowała ekstradycję Melindy i dostała podziękowania od obu.


### Tajna baza Orbitera?

* **uid:** 200510-tajna-baza-orbitera, _numer względny_: 2
* **daty:** 0110-09-03 - 0110-09-07
* **obecni:** Alina Anakonda, Artur Kołczond, Gabriel Ursus, Ignacy Myrczek, Kallista Exolon, Laura Tesinik, Mariusz Trzewń, Natalia Tessalon, Pięknotka Diakon, Sabina Kazitan, Talarand d'Irrydius, Teresa Mieralit, Tymon Grubosz

Streszczenie:

Natalia Tessalon chce przetransportować rannego brata do Aurum, ale manewr zablokowała Laura (terminuska-prawniczka) na prośbę Pięknotki. Natalia i Sabina się ścięły, przez co ucierpiał Myrczek a Sabina trafiła do aresztu. Teresa Mieralit zapewni, że Myrczek nie ma głowy do Sabiny. Pięknotka z Tymonem poszukali kto ciężko zmiażdżył Gabriela; udało się im znaleźć tajną bazę Orbitera w której jest Emulatorka, która się do tego przyznała.

Aktor w Opowieści:

* Dokonanie:
    * miała okazję użyć swoich zaawansowanych skilli prawnych by stawić czoło Aurum i WYGRAĆ - nie przeniosą Tadeusza. Prawo jest jej bronią. Pomogła Pięknotce :D.


### Infiltrator poluje na TAI Minerwy

* **uid:** 200425-inflitrator-poluje-na-tai-minerwy, _numer względny_: 1
* **daty:** 0110-08-20 - 0110-08-22
* **obecni:** Erwin Galilien, Gabriel Ursus, Laura Tesinik, Minerwa Metalia, Pięknotka Diakon, Strażniczka Alair, Tomasz Tukan

Streszczenie:

Problemy na Trzęsawisku wpływają na Cyberszkołę w Zaczęstwie, co uszkadza możliwości TAI "Strażniczki", czyli Eszary d'AlephAiren. Tymczasem jakaś tajemnicza siła uszkadza cywilne TAI, które usprawniała Minerwa. Pięknotka odpiera Tukana, który chce udowodnić winę Minerwy i odkrywa, że za tym wszystkim stoi ktoś z Aurum, ktoś kto posiada profesjalnego pajęczego Infiltratora Iniekcyjnego, zaprojektowanego do uszkadzania AI. Dzięki temu Pięknotka wyciągnęła Minerwę z aresztu.

Aktor w Opowieści:

* Dokonanie:
    * niedoświadczona terminuska i asystentka Tukana; psychotroniczka, która zna chyba wszystkie regulacje i działa zgodnie z każdą jedną z nich.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 9, @: 0111-10-20
    1. Primus    : 9, @: 0111-10-20
        1. Sektor Astoriański    : 9, @: 0111-10-20
            1. Astoria    : 9, @: 0111-10-20
                1. Sojusz Letejski    : 9, @: 0111-10-20
                    1. Szczeliniec    : 9, @: 0111-10-20
                        1. Powiat Jastrzębski    : 1, @: 0110-10-05
                            1. Jastrząbiec, okolice    : 1, @: 0110-10-05
                                1. Blokhaus Widmo    : 1, @: 0110-10-05
                        1. Powiat Pustogorski    : 9, @: 0111-10-20
                            1. Czemerta, okolice    : 1, @: 0110-09-07
                                1. Baza Irrydius    : 1, @: 0110-09-07
                                1. Fortifarma Irrydia    : 1, @: 0110-09-07
                                1. Studnia Irrydiańska    : 1, @: 0110-09-07
                            1. Czółenko    : 2, @: 0111-06-20
                                1. Bunkry    : 1, @: 0110-09-17
                                1. Pola Północne    : 1, @: 0110-09-17
                            1. Podwiert    : 6, @: 0111-07-06
                                1. Dzielnica Luksusu Rekinów    : 3, @: 0111-07-06
                                    1. Serce Luksusu    : 1, @: 0111-07-01
                                        1. Lecznica Rannej Rybki    : 1, @: 0111-07-01
                                1. Klub Arkadia    : 1, @: 0110-09-12
                                1. Las Trzęsawny    : 3, @: 0111-07-06
                                    1. Jeziorko Mokre    : 2, @: 0111-07-06
                                1. Osiedle Leszczynowe    : 1, @: 0110-09-17
                                    1. Sklep z reliktami Fantasmagoria    : 1, @: 0110-09-17
                                1. Osiedle Tęczy    : 1, @: 0111-07-06
                            1. Pustogor    : 1, @: 0110-09-07
                                1. Eksterior    : 1, @: 0110-09-07
                                    1. Miasteczko    : 1, @: 0110-09-07
                                        1. Knajpa Górska Szalupa    : 1, @: 0110-09-07
                                1. Rdzeń    : 1, @: 0110-09-07
                                    1. Szpital Terminuski    : 1, @: 0110-09-07
                            1. Zaczęstwo    : 3, @: 0111-10-20
                                1. Akademia Magii, kampus    : 3, @: 0111-10-20
                                    1. Arena Treningowa    : 1, @: 0110-10-05
                                    1. Artefaktorium    : 1, @: 0111-10-20
                                    1. Budynek Centralny    : 1, @: 0110-08-22
                                1. Cyberszkoła    : 1, @: 0110-08-22
                                1. Dzielnica Kwiecista    : 1, @: 0111-10-20
                                    1. Rezydencja Porzeczników    : 1, @: 0111-10-20
                                        1. Garaż Groźnych Eksperymentów    : 1, @: 0111-10-20
                                1. Dzielnica Ogrodów    : 1, @: 0111-10-20
                                    1. Wielki Szpital Magiczny    : 1, @: 0111-10-20
                                1. Nieużytki Staszka    : 2, @: 0111-10-20
                                1. Sypialnia Szczelińca    : 1, @: 0110-08-22

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Gabriel Ursus        | 4 | ((200425-inflitrator-poluje-na-tai-minerwy; 200510-tajna-baza-orbitera; 200616-bardzo-straszna-mysz; 201006-dezinhibitor-dla-sabiny)) |
| Mariusz Trzewń       | 3 | ((200510-tajna-baza-orbitera; 200524-dom-dla-melindy; 210817-zgubiony-holokrysztal-w-lesie)) |
| Marysia Sowińska     | 3 | ((210817-zgubiony-holokrysztal-w-lesie; 210824-mandragora-nienawidzi-rekinow; 210831-serafina-staje-za-wydrami)) |
| Pięknotka Diakon     | 3 | ((200425-inflitrator-poluje-na-tai-minerwy; 200510-tajna-baza-orbitera; 200524-dom-dla-melindy)) |
| Tomasz Tukan         | 3 | ((200425-inflitrator-poluje-na-tai-minerwy; 210817-zgubiony-holokrysztal-w-lesie; 210824-mandragora-nienawidzi-rekinow)) |
| Daniel Terienak      | 2 | ((210824-mandragora-nienawidzi-rekinow; 210831-serafina-staje-za-wydrami)) |
| Franek Bulterier     | 2 | ((200524-dom-dla-melindy; 200616-bardzo-straszna-mysz)) |
| Ignacy Myrczek       | 2 | ((200510-tajna-baza-orbitera; 201006-dezinhibitor-dla-sabiny)) |
| Karolina Terienak    | 2 | ((210824-mandragora-nienawidzi-rekinow; 210831-serafina-staje-za-wydrami)) |
| Liliana Bankierz     | 2 | ((210817-zgubiony-holokrysztal-w-lesie; 230212-pierwszy-tajemniczy-wielbiciel-liliany)) |
| Lorena Gwozdnik      | 2 | ((201006-dezinhibitor-dla-sabiny; 210831-serafina-staje-za-wydrami)) |
| Sabina Kazitan       | 2 | ((200510-tajna-baza-orbitera; 201006-dezinhibitor-dla-sabiny)) |
| Alan Bartozol        | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Alina Anakonda       | 1 | ((200510-tajna-baza-orbitera)) |
| Amelia Sowińska      | 1 | ((210824-mandragora-nienawidzi-rekinow)) |
| Aranea Diakon        | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Artur Kołczond       | 1 | ((200510-tajna-baza-orbitera)) |
| Cyryl Perikas        | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Diana Lauris         | 1 | ((200616-bardzo-straszna-mysz)) |
| Diana Lemurczak      | 1 | ((200524-dom-dla-melindy)) |
| Diana Tevalier       | 1 | ((200524-dom-dla-melindy)) |
| Ekaterina Zajcew     | 1 | ((210824-mandragora-nienawidzi-rekinow)) |
| Erwin Galilien       | 1 | ((200425-inflitrator-poluje-na-tai-minerwy)) |
| Gerwazy Lemurczak    | 1 | ((200524-dom-dla-melindy)) |
| Halina Sermniek      | 1 | ((210831-serafina-staje-za-wydrami)) |
| Henryk Wkrąż         | 1 | ((200616-bardzo-straszna-mysz)) |
| Julia Kardolin       | 1 | ((230212-pierwszy-tajemniczy-wielbiciel-liliany)) |
| Justynian Diakon     | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Kacper Bankierz      | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Kallista Exolon      | 1 | ((200510-tajna-baza-orbitera)) |
| Karolina Erenit      | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Ksenia Kirallen      | 1 | ((200616-bardzo-straszna-mysz)) |
| Mariusz Kupieczka    | 1 | ((230212-pierwszy-tajemniczy-wielbiciel-liliany)) |
| Matylda Sęk          | 1 | ((200616-bardzo-straszna-mysz)) |
| Melinda Teilert      | 1 | ((200524-dom-dla-melindy)) |
| Minerwa Metalia      | 1 | ((200425-inflitrator-poluje-na-tai-minerwy)) |
| Napoleon Bankierz    | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Natalia Tessalon     | 1 | ((200510-tajna-baza-orbitera)) |
| Rafał Kumczek        | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Rafał Torszecki      | 1 | ((210831-serafina-staje-za-wydrami)) |
| Rupert Mysiokornik   | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Sensacjusz Diakon    | 1 | ((210824-mandragora-nienawidzi-rekinow)) |
| Serafina Ira         | 1 | ((210831-serafina-staje-za-wydrami)) |
| Stella Armadion      | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Strażniczka Alair    | 1 | ((200425-inflitrator-poluje-na-tai-minerwy)) |
| Talarand d'Irrydius  | 1 | ((200510-tajna-baza-orbitera)) |
| Teresa Mieralit      | 1 | ((200510-tajna-baza-orbitera)) |
| Triana Porzecznik    | 1 | ((230212-pierwszy-tajemniczy-wielbiciel-liliany)) |
| Tymon Grubosz        | 1 | ((200510-tajna-baza-orbitera)) |
# Sensacjusz Diakon
## Identyfikator

Id: 2109-sensacjusz-diakon

## Sekcja Opowieści

### Gdy zabraknie prądu Rekinom

* **uid:** 211207-gdy-zabraknie-pradu-rekinom, _numer względny_: 6
* **daty:** 0111-08-26 - 0111-08-27
* **obecni:** Arkadia Verlen, Arnold Kłaczek, Daniel Terienak, Henryk Wkrąż, Hestia d'Rekiny, Karolina Terienak, Lorena Gwozdnik, Marysia Sowińska, Natalia Tessalon, Sensacjusz Diakon, Urszula Arienik

Streszczenie:

Ponieważ Amelia nie przekazała Marysi informacji o tym, że ta powinna przejąć obowiązki zarządcze to Marysia jako administratorka nie dostawała informacji. Przez to odcięło im prąd. Marysia przywróciła prąd podpisując (standardową) umowę z dystrybutorem podwierckim a Karolina uratowała ludzką część Dzielnicy Rekinów od pobicia przez wkurzonych magów. Próba obniżenia rachunków przez współpracę z mafią została przez dziewczyny zażegnana używając Arkadii.

Aktor w Opowieści:

* Dokonanie:
    * ma awaryjny generator prądu w szpitalu; Marysia dostała do niego dostęp by przywrócić prąd. Ale przez to musiał wsadzić Lorenę w glizdę medyczną.


### Satarail pomaga Marysi

* **uid:** 211102-satarail-pomaga-marysi, _numer względny_: 5
* **daty:** 0111-08-05 - 0111-08-06
* **obecni:** Ernest Namertel, Karolina Terienak, Lorena Gwozdnik, Marek Samszar, Marysia Sowińska, Rafał Torszecki, Sensacjusz Diakon, Wiktor Satarail

Streszczenie:

Satarail uderzył - zainfekował Owadem który wypełzł z Torszeckiego Samszara (który kiedyś ukradł somnibela Olgi). Zainfekowane Rekiny zaczęły dewastować dzielnicę. Karo skutecznie rozwala owady (i Lorenę), po czym Sensacjusza w glistę medyczną. Marysia samodzielnie leci jako owad wyłączyć Samszara. Torszecki jest rozgrzeszony - "nie był sobą". Ale polowanie na Torszeckiego czas zacząć.

Aktor w Opowieści:

* Dokonanie:
    * wyleczył Karolinę z infekcji i używa Diakońskiej Glisty Medycznej. Sam jest zarażony - Karo i Marysia wepchnęły go w glistę. Nawet zarażony jest lekarzem.


### Koszt ratowania Torszeckiego

* **uid:** 211026-koszt-ratowania-torszeckiego, _numer względny_: 4
* **daty:** 0111-07-28 - 0111-08-01
* **obecni:** Ignacy Myrczek, Ksenia Kirallen, Marysia Sowińska, Olga Myszeczka, Paweł Szprotka, Rafał Torszecki, Sensacjusz Diakon, Teresa Mieralit, Wiktor Satarail

Streszczenie:

By ratować Torszeckiego, Marysia wchodzi w sojusz z Wiktorem Satarailem. On dał jej podskórnego robaka którego dostanie Torszecki, by uzasadnić dziwne zachowanie. Wiktor "dostanie swoją zapłatę" od "kogoś winnego". Marysia przekonała też Sensacjusza, że ona x Torszecki. Zaczęły się też pojawiać takie plotki...

Aktor w Opowieści:

* Dokonanie:
    * Marysia go wkręciła, że ona x Torszecki i to łyknął. Pozwolił im się spotykać i kibicuje gorąco ich związkowi (mezaliansowi?).
* Progresja:
    * zmienia opinię na temat Marysi Sowińskiej. To zagubiona młoda kobieta, przemierzająca skomplikowany polityczny świat. Ale zdolna do miłości. Nie to co Amelia.


### Torszecki pokazał kręgosłup

* **uid:** 211012-torszecki-pokazal-kregoslup, _numer względny_: 3
* **daty:** 0111-07-25 - 0111-07-26
* **obecni:** Amelia Sowińska, Jolanta Sowińska, Karolina Terienak, Lucjan Sowiński, Marysia Sowińska, Rafał Torszecki, Sensacjusz Diakon

Streszczenie:

Marysia dostała nową "prośbę" ze strony Dworu Sowińskich - pozyskać próbkę krwi "córki Morlana" z Pustogoru. Tymczasem Karolina poszła do Torszeckiego (do lecznicy) by ten wreszcie miał kręgosłup. Wymusiła na nim pomoc w znalezieniu mordercy ścigacza. Torszecki nie chcąc krzywdy Karoliny się jej przyznał - to on. Chciał zrobić Ernest vs Mafia by chronić Marysię; skrzyżował w głowie, że Ernest x Amelia to Esuriit, więc to samo spotka biedną Marysię... Karo i Marysia mają teraz problem - jak to rozplątać?

Aktor w Opowieści:

* Dokonanie:
    * chroni Torszeckiego przed Karoliną; uważa ją (nieuczciwie) za posłańca Marysi Sowińskiej.


### Mandragora nienawidzi Rekinów

* **uid:** 210824-mandragora-nienawidzi-rekinow, _numer względny_: 2
* **daty:** 0111-06-29 - 0111-07-01
* **obecni:** Amelia Sowińska, Daniel Terienak, Ekaterina Zajcew, Karolina Terienak, Laura Tesinik, Marysia Sowińska, Sensacjusz Diakon, Tomasz Tukan

Streszczenie:

Mandragora nienawidzi Rekinów i jej ofiarą został Damian Terienak. Idąc za jego śladem Marysia i Karolina znalazły mandragorę i pnączoszpona oraz z pomocą dwóch uczennic terminusa rozwaliły problem. Dowiedziały się, że mandragora konkretnie celuje w Amelię Sowińską, która opuściła ten teren dawno temu (i która była dość okrutna).

Aktor w Opowieści:

* Dokonanie:
    * jest PEWNY, że Marysia Sowińska współpracuje z mafią. Świetny lekarz, opłacany przez wszystkie rody Aurum by ratować głupie Rekiny. Nie chce tu być. Zapalił się pomagając Danielowi - Daniel był dotknięty mandragorą, co Sensacjusz wykrył i naprawił. Aha, uważa, że Marysia jest jak Amelia.


### Glizda, która leczy

* **uid:** 211120-glizda-ktora-leczy, _numer względny_: 1
* **daty:** 0108-04-03 - 0108-04-14
* **obecni:** Amanda Kajrat, Amelia Sowińska, Ernest Kajrat, Feliks Keksik, Justynian Diakon, Kacper Bankierz, Karol Pustak, Oliwia Lemurczak, Roland Sowiński, Sabina Kazitan, Sensacjusz Diakon, Stella Sowińska

Streszczenie:

Sensacjusz Diakon został wysłany na prowincję pod przykrywką Lekarza Rekinów, by dojść ze Stellą do tego czemu Amelia i Roland Sowińscy zachowują się nietypowo. Na miejscu okazało się, że Roland jest w stanie ostrej wojny z mafią (która nie traktuje tego poważnie; Kajrat się bawi z Rekinami) a Amelia po prostu nie w pełni sobie radzi i próbuje opanować sytuację. Roland jest Skażony Esuriit; Sensacjusz przetransportował go glizdą do Aurum i sam, uwięziony w bojowej formie, deeskalował konflikt z mafią. A Amelia jest tymczasowym dowódcą Rekinów...

Aktor w Opowieści:

* Dokonanie:
    * inżynier biowojenny, który na skutek "pomyłki" trafił na prowincję (Podwiert) być lekarzem Rekinów. Odkrył, że Roland jest Esuriitowy i wpakował go w glizdę i uratował. Potem w trybie bojowym "wilkołaka" odpędził mafię - zmienił im cost-to-benefit ratio.
* Progresja:
    * ma fałszywe dossier u Sowińskich - jest wybitnym lekarzem, kobieciarzem i ma niezwykłą charyzmę. Zesłany na prowincję. Sęk w tym, że TAMTEN Sensacjusz nie żyje. Pomylili osobę.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 6, @: 0111-08-27
    1. Primus    : 6, @: 0111-08-27
        1. Sektor Astoriański    : 6, @: 0111-08-27
            1. Astoria    : 6, @: 0111-08-27
                1. Sojusz Letejski    : 6, @: 0111-08-27
                    1. Szczeliniec    : 6, @: 0111-08-27
                        1. Powiat Pustogorski    : 6, @: 0111-08-27
                            1. Czarnopalec    : 1, @: 0111-08-01
                                1. Pusta Wieś    : 1, @: 0111-08-01
                            1. Czółenko    : 1, @: 0111-08-27
                                1. Generatory Keriltorn    : 1, @: 0111-08-27
                            1. Podwiert    : 6, @: 0111-08-27
                                1. Dzielnica Luksusu Rekinów    : 6, @: 0111-08-27
                                    1. Sektor Brudu i Nudy    : 1, @: 0111-08-27
                                        1. Komputerownia    : 1, @: 0111-08-27
                                    1. Serce Luksusu    : 6, @: 0111-08-27
                                        1. Apartamentowce Elity    : 1, @: 0111-07-26
                                        1. Arena Amelii    : 1, @: 0111-08-27
                                        1. Lecznica Rannej Rybki    : 5, @: 0111-08-06
                                1. Kompleks Korporacyjny    : 1, @: 0111-08-27
                                    1. Dystrybutor Prądu Ozitek    : 1, @: 0111-08-27
                                    1. Elektrownia Węglowa Szarpien    : 1, @: 0111-08-27
                                1. Las Trzęsawny    : 2, @: 0111-07-01
                                    1. Jeziorko Mokre    : 1, @: 0111-07-01
                            1. Pustogor    : 1, @: 0111-08-06
                                1. Rdzeń    : 1, @: 0111-08-06
                                    1. Szpital Terminuski    : 1, @: 0111-08-06
                            1. Zaczęstwo    : 1, @: 0111-08-01
                                1. Akademia Magii, kampus    : 1, @: 0111-08-01
                                    1. Akademik    : 1, @: 0111-08-01

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Marysia Sowińska     | 5 | ((210824-mandragora-nienawidzi-rekinow; 211012-torszecki-pokazal-kregoslup; 211026-koszt-ratowania-torszeckiego; 211102-satarail-pomaga-marysi; 211207-gdy-zabraknie-pradu-rekinom)) |
| Karolina Terienak    | 4 | ((210824-mandragora-nienawidzi-rekinow; 211012-torszecki-pokazal-kregoslup; 211102-satarail-pomaga-marysi; 211207-gdy-zabraknie-pradu-rekinom)) |
| Amelia Sowińska      | 3 | ((210824-mandragora-nienawidzi-rekinow; 211012-torszecki-pokazal-kregoslup; 211120-glizda-ktora-leczy)) |
| Rafał Torszecki      | 3 | ((211012-torszecki-pokazal-kregoslup; 211026-koszt-ratowania-torszeckiego; 211102-satarail-pomaga-marysi)) |
| Daniel Terienak      | 2 | ((210824-mandragora-nienawidzi-rekinow; 211207-gdy-zabraknie-pradu-rekinom)) |
| Lorena Gwozdnik      | 2 | ((211102-satarail-pomaga-marysi; 211207-gdy-zabraknie-pradu-rekinom)) |
| Wiktor Satarail      | 2 | ((211026-koszt-ratowania-torszeckiego; 211102-satarail-pomaga-marysi)) |
| Amanda Kajrat        | 1 | ((211120-glizda-ktora-leczy)) |
| Arkadia Verlen       | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Arnold Kłaczek       | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Ekaterina Zajcew     | 1 | ((210824-mandragora-nienawidzi-rekinow)) |
| Ernest Kajrat        | 1 | ((211120-glizda-ktora-leczy)) |
| Ernest Namertel      | 1 | ((211102-satarail-pomaga-marysi)) |
| Feliks Keksik        | 1 | ((211120-glizda-ktora-leczy)) |
| Henryk Wkrąż         | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Hestia d'Rekiny      | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Ignacy Myrczek       | 1 | ((211026-koszt-ratowania-torszeckiego)) |
| Jolanta Sowińska     | 1 | ((211012-torszecki-pokazal-kregoslup)) |
| Justynian Diakon     | 1 | ((211120-glizda-ktora-leczy)) |
| Kacper Bankierz      | 1 | ((211120-glizda-ktora-leczy)) |
| Karol Pustak         | 1 | ((211120-glizda-ktora-leczy)) |
| Ksenia Kirallen      | 1 | ((211026-koszt-ratowania-torszeckiego)) |
| Laura Tesinik        | 1 | ((210824-mandragora-nienawidzi-rekinow)) |
| Lucjan Sowiński      | 1 | ((211012-torszecki-pokazal-kregoslup)) |
| Marek Samszar        | 1 | ((211102-satarail-pomaga-marysi)) |
| Natalia Tessalon     | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Olga Myszeczka       | 1 | ((211026-koszt-ratowania-torszeckiego)) |
| Oliwia Lemurczak     | 1 | ((211120-glizda-ktora-leczy)) |
| Paweł Szprotka       | 1 | ((211026-koszt-ratowania-torszeckiego)) |
| Roland Sowiński      | 1 | ((211120-glizda-ktora-leczy)) |
| Sabina Kazitan       | 1 | ((211120-glizda-ktora-leczy)) |
| Stella Sowińska      | 1 | ((211120-glizda-ktora-leczy)) |
| Teresa Mieralit      | 1 | ((211026-koszt-ratowania-torszeckiego)) |
| Tomasz Tukan         | 1 | ((210824-mandragora-nienawidzi-rekinow)) |
| Urszula Arienik      | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
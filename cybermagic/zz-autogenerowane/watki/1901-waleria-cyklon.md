# Waleria Cyklon
## Identyfikator

Id: 1901-waleria-cyklon

## Sekcja Opowieści

### Adieu, Cieniaszczycie

* **uid:** 181227-adieu-cieniaszczycie, _numer względny_: 5
* **daty:** 0109-11-27 - 0109-11-30
* **obecni:** Amadeusz Sowiński, Atena Sowińska, Brygida Maczkowik, Erwin Galilien, Lilia Ursus, Mirela Niecień, Moktar Gradon, Pietro Dwarczan, Pięknotka Diakon, Waleria Cyklon, Zbigniew Burzycki

Streszczenie:

Pięknotka zdobyła nowe ciało dla Minerwy (acz do jego uruchomienia człowiek musi być poświęcony!), przekonała Amadeusza do założenia świątyni Arazille do blokowania Finis Vitae oraz wepchnęła Lilię jako asystentkę Atenie. Do tego rozkochała solidnie Erwina i zamknęła wszystkie wątki, by móc wracać do Pustogoru.

Aktor w Opowieści:

* Dokonanie:
    * w krew wchodzi jej porywanie magów - wpierw Lilia, potem Erwin. Planowała jak ściągnąć Julię do Łysych Psów - z powodzeniem.


### Finis Vitae

* **uid:** 181226-finis-vitae, _numer względny_: 4
* **daty:** 0109-11-17 - 0109-11-26
* **obecni:** Amadeusz Sowiński, Arazille, Atena Sowińska, Moktar Gradon, Pięknotka Diakon, Waleria Cyklon

Streszczenie:

Moktar zregenerował Pięknotkę i powiedział jej, że chce od niej uratowania swojego Skażonego oficera. Pięknotka dowiedziała się od Amadeusza o autowarze Finis Vitae pod Cieniaszczytem; Saitaer nie może nic wiedzieć. Pięknotka doprowadziła do tego, że Lilia wróciła do Cieniaszczytu (courtesy of Moktar), uratowała spod ziemi tego oficera, oraz zobaczyła miejsce, które będzie chyba do końca życia widziała w koszmarach. Gdyby nie Atena, zginęłaby na tej akcji.

Aktor w Opowieści:

* Dokonanie:
    * pozyskała Lilię dla Moktara oraz wraz z Pięknotką opracowywała plan jak wyciągnąć Dariusza od autowara (nie wie o autowarze).


### Tajemniczy ołtarz Moktara

* **uid:** 181218-tajemniczy-oltarz-moktara, _numer względny_: 3
* **daty:** 0109-11-10 - 0109-11-12
* **obecni:** Atena Sowińska, Bogdan Szerl, Moktar Grodan, Pięknotka Diakon, Waleria Cyklon

Streszczenie:

Pięknotka śniła o lustrach; w poszukiwaniu informacji co się dzieje dowiedziała się, że do wyczyszczenia Saitaera wykorzystana jest energia Arazille. O dziwo, Moktar kontroluje energię Arazille a przynajmniej używa jej jako broni. Pięknotka wróciła w ręce Bogdana i przekonała Atenę, by ta poddała się rekonstrukcji - by uwolniła się od Saitaera.

Aktor w Opowieści:

* Dokonanie:
    * osłabiona; pomaga Pięknotce przystosować się do świata gdzie śnią jej się lustra. Zmasakrowała maga, który ośmielił się ją zaatakować swoim swarmem.


### Kajdan na Moktara

* **uid:** 181209-kajdan-na-moktara, _numer względny_: 2
* **daty:** 0109-11-01 - 0109-11-02
* **obecni:** Pietro Dwarczan, Pięknotka Diakon, Waleria Cyklon

Streszczenie:

Pięnotka próbowała osłonić się przed potencjalnym atakiem Moktara i poszła szukać jakiejś jego słabości - i spotkała się w pojedynku snajperów z Walerią Cyklon. Wygrała dzięki lepszemu sprzętowi (power suit), acz skończyła w złym stanie. Wycofała się i Walerię przed atakiem dziwnych nojrepów i wybudziła Walerię; dostała swój dowód, przeskanowała pamięć Walerii i uratowała bazę Moktara przed zniszczeniem przez te dziwne nojrepy. Ogólnie, sukces.

Aktor w Opowieści:

* Dokonanie:
    * wiecznie lojalna Moktarowi; chroniła jego bazę przed Pięknotką a potem weszła z nią w sojusz przed nacierającymi nojrepami.
* Progresja:
    * ciężko ranna, wymaga regeneracji i w niełaskach Moktara z uwagi na fail, utratę bazy i dowód jaki ma Pięknotka mogący zniszczyć Psy.


### Swaty w cieniu potwora

* **uid:** 181125-swaty-w-cieniu-potwora, _numer względny_: 1
* **daty:** 0109-10-28 - 0109-10-31
* **obecni:** Lilia Ursus, Moktar Gradon, Pietro Dwarczan, Pięknotka Diakon, Romuald Czurukin, Waleria Cyklon, Wioletta Kalazar

Streszczenie:

Pięknotka bardzo chciała przespać się z Pietrem; niestety, spotkała Lilię na bazarze i dowiedziała się o dziwnych nojrepach z symbolem Orbitera. Lilia wyraźnie czuje się niedoceniana i pogardzana. Cóż, na szczęście doprowadziła Pięknotkę do Wioletty. Gdy Pietro wyznał (załamanej) Pięknotce że ma miętę do Wioletty, Pięknotka zdecydowała się ich skojarzyć. Lilia jako detektyw miała pomóc. Lilia wpadła jednak w szpony Moktara, który zaczął ją łamać. Pięknotka i Pietro uratowali Lilię, acz Pietro skończył w szpitalu. Pięknotka, ku swemu zdziwieniu, dała radę kogoś poderwać - Wiolettę. To nie były złe trzy dni...

Aktor w Opowieści:

* Dokonanie:
    * pierwszy oficer Moktara, której nie podobała się personalna wendetta Moktara wobec bogu ducha winnej Lilii.
* Progresja:
    * Moktar ustawił ją jako pierwszą oficer Łysych Psów. Dał jej prawo do planowania i decydowania o ruchach Psów a ona ogranicza jego psychopatyczne ruchy.
    * szanuje Pięknotkę jako przeciwnika. Nie "lubi" jej, ale nie życzy jej źle.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 5, @: 0109-11-30
    1. Primus    : 5, @: 0109-11-30
        1. Sektor Astoriański    : 5, @: 0109-11-30
            1. Astoria    : 5, @: 0109-11-30
                1. Sojusz Letejski, NW    : 5, @: 0109-11-30
                    1. Ruiniec    : 5, @: 0109-11-30
                        1. Colubrinus Meditech    : 1, @: 0109-11-12
                        1. Colubrinus Psiarnia    : 2, @: 0109-11-30
                        1. Diamentowa Forteca    : 1, @: 0109-10-31
                        1. Skalny Labirynt    : 2, @: 0109-11-26
                        1. Studnia Bez Dna    : 1, @: 0109-11-26
                        1. Świątynia Bez Dna    : 1, @: 0109-11-30
                1. Sojusz Letejski    : 5, @: 0109-11-30
                    1. Przelotyk    : 5, @: 0109-11-30
                        1. Przelotyk Wschodni    : 5, @: 0109-11-30
                            1. Cieniaszczyt    : 5, @: 0109-11-30
                                1. Arena Nadziei Tęczy    : 1, @: 0109-10-31
                                1. Bazar Wschodu Astorii    : 1, @: 0109-10-31
                                1. Knajpka Szkarłatny Szept    : 2, @: 0109-11-30
                                1. Kompleks Nukleon    : 3, @: 0109-11-30
                                1. Mordownia Czaszka Kralotha    : 1, @: 0109-10-31
                                1. Mrowisko    : 4, @: 0109-11-30

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Pięknotka Diakon     | 5 | ((181125-swaty-w-cieniu-potwora; 181209-kajdan-na-moktara; 181218-tajemniczy-oltarz-moktara; 181226-finis-vitae; 181227-adieu-cieniaszczycie)) |
| Atena Sowińska       | 3 | ((181218-tajemniczy-oltarz-moktara; 181226-finis-vitae; 181227-adieu-cieniaszczycie)) |
| Moktar Gradon        | 3 | ((181125-swaty-w-cieniu-potwora; 181226-finis-vitae; 181227-adieu-cieniaszczycie)) |
| Pietro Dwarczan      | 3 | ((181125-swaty-w-cieniu-potwora; 181209-kajdan-na-moktara; 181227-adieu-cieniaszczycie)) |
| Amadeusz Sowiński    | 2 | ((181226-finis-vitae; 181227-adieu-cieniaszczycie)) |
| Lilia Ursus          | 2 | ((181125-swaty-w-cieniu-potwora; 181227-adieu-cieniaszczycie)) |
| Arazille             | 1 | ((181226-finis-vitae)) |
| Bogdan Szerl         | 1 | ((181218-tajemniczy-oltarz-moktara)) |
| Brygida Maczkowik    | 1 | ((181227-adieu-cieniaszczycie)) |
| Erwin Galilien       | 1 | ((181227-adieu-cieniaszczycie)) |
| Mirela Niecień       | 1 | ((181227-adieu-cieniaszczycie)) |
| Moktar Grodan        | 1 | ((181218-tajemniczy-oltarz-moktara)) |
| Romuald Czurukin     | 1 | ((181125-swaty-w-cieniu-potwora)) |
| Wioletta Kalazar     | 1 | ((181125-swaty-w-cieniu-potwora)) |
| Zbigniew Burzycki    | 1 | ((181227-adieu-cieniaszczycie)) |
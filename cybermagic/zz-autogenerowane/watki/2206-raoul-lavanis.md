# Raoul Lavanis
## Identyfikator

Id: 2206-raoul-lavanis

## Sekcja Opowieści

### Lewiatan za Pandorę

* **uid:** 220622-lewiatan-za-pandore, _numer względny_: 5
* **daty:** 0112-07-26 - 0112-07-29
* **obecni:** Arianna Verlen, Eustachy Korkoran, Klaudia Stryk, Maria Naavas, Ola Szerszeń, OO Infernia, OO Pandora, OO Straszliwy Pająk, OO Tivr, Raoul Lavanis, Rzeźnik Parszywiec Diakon

Streszczenie:

PRZESZŁOŚĆ: Ola Szerszeń opiekuje się Eleną w Regeneratorze; okazuje się być biosyntem (zakazane eksperymenty Termii). Rzeźnik Diakon to łagodny i jowialny kapitan Straszliwego Pająka, acz znienawidzony za przeszłość zdrajcy Orbitera oraz Noctis. TERAŹNIEJSZOŚĆ: Lewiatan zostaje zestrzelony torpedą anihilacyjną, dzięki Raoulowi ratujemy wielu ludzi na orbicie Lewiatana. Malictrix d'Pandora zostaje zniszczona przez Lewiatana, ale Klaudia ma jego mikrokopię dzięki Ariannie.

Aktor w Opowieści:

* Dokonanie:
    * wszedł na pokład jednostek tworzących Lewiatana i uratował tyle osób ile się dało; świetny advancer. Spacer kosmiczny, uruchamianie kapsuł ratunkowych itp.


### Ratujemy porywaczy Eleny

* **uid:** 220610-ratujemy-porywaczy-eleny, _numer względny_: 4
* **daty:** 0112-07-11 - 0112-07-13
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Hestia d'Atropos, Jakub Bulgocz, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Raoul Lavanis

Streszczenie:

Elena jest już zbyt zdestabilizowana by funkcjonować niezależnie, więc wpierw zajęli się nią w Laboratorium Kranix a po zbudowaniu dla niej Containment Chamber przesłali ją do stacji medycznej Atropos do regeneracji. Stamtąd ją i Martyna porwał Syndykat Aureliona (ale Martyn przewidział JAKIŚ problem i ostrzegł Klaudię). Elena by zniszczyła okręt napastników, ale Zespół skutecznie odkrył problemy na Atropos, przesłuchali dowódcę, zneutralizowali miragenta i dotarli do okrętu Aureliona zdobywając jeńców i zatrzymując Elenę przed przebudzeniem i Spustoszeniem statku.

Aktor w Opowieści:

* Dokonanie:
    * bardzo zauważliwy; zauważył, że "Elena" (miragent) jest źle podpięty do Containment Chamber. A potem w kosmosie zrobił insercję i wszedł na statek Aureliona z Leoną i Eustachym.


### EtAur i przynęta na Kryptę

* **uid:** 220330-etaur-i-przyneta-na-krypte, _numer względny_: 3
* **daty:** 0112-04-27 - 0112-04-30
* **obecni:** Arianna Verlen, Eustachy Korkoran, Klaudia Stryk, Lamia Akacja, Leszek Czarban, Maria Naavas, Raoul Lavanis

Streszczenie:

Okazało się, że "potwór" to były faeril z Neikatis zarażony bronią biologiczną. Próbował doprowadzić do katastrofy by Orbiter lub Eternia zabrali się za leczenie tego cholerstwa, pomagał mu Antoni, p.o. TAI na EtAur. Maria doszła do biostruktury, Klaudia przejęła systemy bazy, Eustachy przechwycił gościa, Arianna zdobyła bazę politycznie. A Klaudia zrobiła raport, by Orbiter nie atakował więcej neutralnych i potencjalnie sojuszniczych sił nie-Orbiterowych w kosmosie. Jako, że Lamii nic się nie stało, Melania jest szczęśliwa.

Aktor w Opowieści:

* Dokonanie:
    * chroni Marię na EtAur; odparł z Eustachym "potwora", po czym ucierpiał jak Eustachy wypalał fagi (phage) Marii.


### Polityka rujnuje Pallidę Voss

* **uid:** 220216-polityka-rujnuje-pallide-voss, _numer względny_: 2
* **daty:** 0112-03-27 - 0112-03-29
* **obecni:** Annika Pradis, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Mateus Sarpon, Raoul Lavanis, SP Pallida Voss, Szczepan Kaltaben

Streszczenie:

Arianna we współpracy z Mateusem planują uratować zakładników pallidańskich od radykałów keldańskich. Ale by to osiągnąć Eustachy i Klaudia infiltrują wpierw Pallidę Voss, bo okazuje się, że Annikę wrabiają jej własni podwładni. Klaudia zdobyła wszystkie potrzebne dowody, Eustachy chciał ratować ludzi... i Paradoks (po sabotowaniu generatorów Memoriam) spowodował straszne straty w ludziach i przesunął Pallidę Voss w Anomalię Kosmiczną. Oops. Ale dzięki drakolitom (keldanitom) udało się sporo ludzi uratować.

Aktor w Opowieści:

* Dokonanie:
    * infiltruje z Eustachym Pallidę Voss; wpadł w kłopoty bo nie ufali Annice (karta była prawidłowa), ale sam się uwolnił.


### Po drugiej stronie Bramy

* **uid:** 210707-po-drugiej-stronie-bramy, _numer względny_: 1
* **daty:** 0111-03-13 - 0111-03-15
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Janus Krzak, Klaudia Stryk, Martyn Hiwasser, Raoul Lavanis, Tal Marczak

Streszczenie:

Infernia próbując przetransportować eksperta Eteru na konferencję przez Bramę uległa katastrofie - przeniosła się GDZIEŚ INDZIEJ, pod wodę i zniknęła Klaudia. Nie uległa katastrofalnym uszkodzeniom, ale po zachęceniu pintką Lewiatana dotarła do archaicznej podwodnej noktiańskiej bazy. Tam znajduje się Klaudia, ale coś jest z nią nie tak. Sprzęgnięta z bazą? Tak czy inaczej, infiltracja bazy dopiero się zacznie. Trzeba znaleźć Klaudię i wrócić do domu.

Aktor w Opowieści:

* Dokonanie:
    * ludzki noktiański advancer Inferni; wraz z Eleną rozpracował starą bazę noktiańską. W grupie roboczej tłumaczącej sigile tej dziwnej archaicznej bazy.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 5, @: 0112-07-29
    1. Primus    : 5, @: 0112-07-29
        1. Sektor Astoriański    : 5, @: 0112-07-29
            1. Astoria, Pierścień Zewnętrzny    : 1, @: 0112-07-13
                1. Laboratorium Kranix    : 1, @: 0112-07-13
                1. Stacja Medyczna Atropos    : 1, @: 0112-07-13
            1. Brama Kariańska    : 1, @: 0111-03-15
            1. Elang, księżyc Astorii    : 1, @: 0112-04-30
                1. EtAur Zwycięska    : 1, @: 0112-04-30
                    1. Magazyny wewnętrzne    : 1, @: 0112-04-30
                    1. Sektor bioinżynierii    : 1, @: 0112-04-30
                        1. Biolab    : 1, @: 0112-04-30
                        1. Komory żywieniowe    : 1, @: 0112-04-30
                        1. Skrzydło medyczne    : 1, @: 0112-04-30
                    1. Sektor inżynierski    : 1, @: 0112-04-30
                        1. Panele słoneczne i bateriownia    : 1, @: 0112-04-30
                        1. Podtrzymywanie życia    : 1, @: 0112-04-30
                        1. Serwisownia    : 1, @: 0112-04-30
                        1. Stacja pozyskiwania wody    : 1, @: 0112-04-30
                    1. Sektor mieszkalny    : 1, @: 0112-04-30
                        1. Barbakan    : 1, @: 0112-04-30
                            1. Administracja    : 1, @: 0112-04-30
                            1. Ochrona    : 1, @: 0112-04-30
                            1. Rdzeń AI    : 1, @: 0112-04-30
                        1. Centrum Rozrywki    : 1, @: 0112-04-30
                        1. Podtrzymywanie życia    : 1, @: 0112-04-30
                        1. Pomieszczenia mieszkalne    : 1, @: 0112-04-30
                        1. Przedszkole    : 1, @: 0112-04-30
                        1. Stołówki    : 1, @: 0112-04-30
                        1. Szklarnie    : 1, @: 0112-04-30
                    1. Sektor przeładunkowy    : 1, @: 0112-04-30
                        1. Atraktory małych pakunków    : 1, @: 0112-04-30
                        1. Magazyny    : 1, @: 0112-04-30
                        1. Pieczara Gaulronów    : 1, @: 0112-04-30
                        1. Punkt celny    : 1, @: 0112-04-30
                        1. Stacja grazerów    : 1, @: 0112-04-30
                        1. Stacja przeładunkowa    : 1, @: 0112-04-30
                        1. Starport    : 1, @: 0112-04-30
                    1. Ukryte sektory    : 1, @: 0112-04-30
            1. Iorus    : 1, @: 0112-03-29
                1. Iorus, pierścień    : 1, @: 0112-03-29
                    1. Keldan Voss    : 1, @: 0112-03-29
            1. Obłok Lirański    : 1, @: 0112-07-29
                1. Anomalia Kolapsu, orbita    : 1, @: 0112-07-29
        1. Zagubieni w Kosmosie    : 1, @: 0111-03-15
            1. Crepuscula    : 1, @: 0111-03-15
                1. Pasmo Zmroku    : 1, @: 0111-03-15
                    1. Baza Noktiańska Zona Tres    : 1, @: 0111-03-15
                    1. Podwodna Brama Eteryczna    : 1, @: 0111-03-15

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 5 | ((210707-po-drugiej-stronie-bramy; 220216-polityka-rujnuje-pallide-voss; 220330-etaur-i-przyneta-na-krypte; 220610-ratujemy-porywaczy-eleny; 220622-lewiatan-za-pandore)) |
| Eustachy Korkoran    | 5 | ((210707-po-drugiej-stronie-bramy; 220216-polityka-rujnuje-pallide-voss; 220330-etaur-i-przyneta-na-krypte; 220610-ratujemy-porywaczy-eleny; 220622-lewiatan-za-pandore)) |
| Klaudia Stryk        | 5 | ((210707-po-drugiej-stronie-bramy; 220216-polityka-rujnuje-pallide-voss; 220330-etaur-i-przyneta-na-krypte; 220610-ratujemy-porywaczy-eleny; 220622-lewiatan-za-pandore)) |
| Elena Verlen         | 3 | ((210707-po-drugiej-stronie-bramy; 220216-polityka-rujnuje-pallide-voss; 220610-ratujemy-porywaczy-eleny)) |
| Maria Naavas         | 2 | ((220330-etaur-i-przyneta-na-krypte; 220622-lewiatan-za-pandore)) |
| Martyn Hiwasser      | 2 | ((210707-po-drugiej-stronie-bramy; 220610-ratujemy-porywaczy-eleny)) |
| Annika Pradis        | 1 | ((220216-polityka-rujnuje-pallide-voss)) |
| Hestia d'Atropos     | 1 | ((220610-ratujemy-porywaczy-eleny)) |
| Jakub Bulgocz        | 1 | ((220610-ratujemy-porywaczy-eleny)) |
| Janus Krzak          | 1 | ((210707-po-drugiej-stronie-bramy)) |
| Lamia Akacja         | 1 | ((220330-etaur-i-przyneta-na-krypte)) |
| Leona Astrienko      | 1 | ((220610-ratujemy-porywaczy-eleny)) |
| Leszek Czarban       | 1 | ((220330-etaur-i-przyneta-na-krypte)) |
| Mateus Sarpon        | 1 | ((220216-polityka-rujnuje-pallide-voss)) |
| Ola Szerszeń         | 1 | ((220622-lewiatan-za-pandore)) |
| OO Infernia          | 1 | ((220622-lewiatan-za-pandore)) |
| OO Pandora           | 1 | ((220622-lewiatan-za-pandore)) |
| OO Straszliwy Pająk  | 1 | ((220622-lewiatan-za-pandore)) |
| OO Tivr              | 1 | ((220622-lewiatan-za-pandore)) |
| Rzeźnik Parszywiec Diakon | 1 | ((220622-lewiatan-za-pandore)) |
| SP Pallida Voss      | 1 | ((220216-polityka-rujnuje-pallide-voss)) |
| Szczepan Kaltaben    | 1 | ((220216-polityka-rujnuje-pallide-voss)) |
| Tal Marczak          | 1 | ((210707-po-drugiej-stronie-bramy)) |
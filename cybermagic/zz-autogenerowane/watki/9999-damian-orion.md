# Damian Orion
## Identyfikator

Id: 9999-damian-orion

## Sekcja Opowieści

### Pułapka z Anastazji

* **uid:** 201230-pulapka-z-anastazji, _numer względny_: 7
* **daty:** 0111-05-21 - 0111-05-22
* **obecni:** AK Nocna Krypta, AK Rodivas, Anastazja Sowińska Dwa, Arianna Verlen, Damian Orion, Elena Verlen, Eustachy Korkoran, Henryk Sowiński, Klaudia Stryk, Martyn Hiwasser, OA Zguba Tytanów, OO Infernia

Streszczenie:

Krypta "oddała Anastazję", zmieniając cywilny statek Rodivas w pułapkę na Ariannę.

Aktor w Opowieści:

* Dokonanie:
    * ostrzegł Ariannę, że pojawiła się Anastazja na statku cywilnym Rodivas. Czemu? Bo nie podoba mu się to, że Aurum się miesza w nie swoje sprawy i tępi Ariannę.


### Sekrety Orbitera - historia prawdziwa

* **uid:** 200819-sekrety-orbitera-historia-prawdziwa, _numer względny_: 6
* **daty:** 0110-11-26 - 0110-12-04
* **obecni:** Arianna Verlen, Damian Orion, Elena Verlen, Eustachy Korkoran, Izabela Zarantel, Klaudia Stryk, Leona Astrienko, Leszek Kurzmin, Martyn Hiwasser, Sabina Servatel, Tadeusz Ursus

Streszczenie:

Elena wyzwała Leonę na pojedynek. Klaudia i Arianna wkręciły Tadeusza. Tadeusz - arcymag Eterni - używa Simulacrum w walce, więc Zespół zaczął montować plan jak Leona może jednak wygrać z Simulacrum. I wszystko skończyło się tym, że Leona wygrała, Elena jest wolna od Tadeusza i powstała holodrama "Sekrety Orbitera"...

Aktor w Opowieści:

* Dokonanie:
    * chcąc pomóc Elenie, dostarczył Mirelę sprzężoną z Zodiac; zmienili Leonę w anty-Tadeuszowego cirrusa.


### Nocna Krypta i Emulatorka

* **uid:** 200729-nocna-krypta-i-emulatorka, _numer względny_: 5
* **daty:** 0110-11-16 - 0110-11-22
* **obecni:** AK Nocna Krypta, Antoni Kramer, Arianna Verlen, Damian Orion, Eustachy Korkoran, Kijara d'Esuriit, Klaudia Stryk, Laura Orion, Leona Astrienko, Mirela Orion, OO Castigator, OO Minerwa

Streszczenie:

Laura, Emulatorka na Nocnej Krypcie została Skażona przez Kijarę. Laura opanowana wizjami Esuriit chciała zniszczyć Eternię. Nie dało się jej powstrzymać - Mirela, Zespół - byli za słabi. Arianna wezwała Kryptę pod ogniem Castigatora i wypaliła Kryptę i Kijarę do zera, uszkadzając "zdrowie psychiczne Krypty". Zginęło kilkudziesięciu ludzi, ale Arianna dostała podziękowanie od Admiralicji. Damian Orion jest rozczarowany.

Aktor w Opowieści:

* Dokonanie:
    * chciał uratować Laurę. Okazało się, że Arianna wybrała bezpieczeństwo. Jest rozczarowany Arianną, ale nadal na poprawnym poziomie.


### Wielki Kosmiczny Romans

* **uid:** 200722-wielki-kosmiczny-romans, _numer względny_: 4
* **daty:** 0110-11-12 - 0110-11-15
* **obecni:** Antoni Kramer, Arianna Verlen, Damian Orion, Elena Verlen, Eustachy Korkoran, Julian Muszel, Konrad Wolczątek, Leona Astrienko, Olgierd Drongon, OO Welgat, OO Żelazko, Tadeusz Ursus

Streszczenie:

Pojawiła się plotka, że Arianna, Elena i Eustachy mają romans. Plotka pochodzi od Leony, ale ktoś ją wykorzystał. Rozwiązując plotkę Zespół doszedł do tego, że Elena ma niestabilną energię magiczną i na niektórych upiornie mocno działa, poznali też elementy przeszłości Eleny. Po wyplątaniu Eleny z jednego obsesyjnego adoratora (i wpakowanie plotki że Olgierd z Żelazka aspiruje do jej ręki), Arianna zdecydowała się z Eustachym, Klaudią i Martynem pomóc Kirasjerom w uratowaniu zaginionej Emulatorki z Nocnej Krypty...

Aktor w Opowieści:

* Dokonanie:
    * najlepszy "przyjaciel" Eleny - on daje jej statki kosmiczne licząc, że dołączy do NeoMil (np. jako Emulatorka) a ona przyjmuje statki, ale nie dołączy. Poprosił Ariannę, by ta pomogła mu uratować Laurę Orion z Nocnej Krypty.


### Krucjata Chevaleresse

* **uid:** 200202-krucjata-chevaleresse, _numer względny_: 3
* **daty:** 0110-07-20 - 0110-07-23
* **obecni:** Ataienne, Damian Orion, Diana Tevalier, Keraina d'Orion, Malictrix d'Itaran, Mariusz Trzewń, Mateusz Kardamacz, Minerwa Metalia, Pięknotka Diakon, Talia Aegis, Tomasz Tukan

Streszczenie:

Chevaleresse wykradła broń Alana i uzbroiła Liberitias, gdzie działa też Ataienne przeciw Kardamaczowi, Grzymościowi i krzywdzicielowi Alana. Pięknotka doszła do serca problemu i materii, acz potrzebowała wsparcia Orbitera - dokładniej, Kerainy. Dzięki Kerainie wprowadziła Malictrix do Kompleksu Itaran. Teraz to tylko kwestia czasu aż Grzymość zostanie zniszczony.

Aktor w Opowieści:

* Dokonanie:
    * oddał Kerainę do pokonania Grzymościa; zmartwiony niestabilnością Ataienne. Oddelegował Kerainę do monitorowania Ataienne.


### Kontrpolowanie Pięknotki - pułapka

* **uid:** 191103-kontrpolowanie-pieknotki-pulapka, _numer względny_: 2
* **daty:** 0110-06-20 - 0110-06-27
* **obecni:** Damian Orion, Józef Małmałaz, Ksenia Kirallen, Lilia Ursus, Marek Puszczok, Mariusz Trzewń, Minerwa Metalia, Olaf Zuchwały, Pięknotka Diakon, Rafał Roszczeniok

Streszczenie:

Pięknotka poluje na noktiańskiego zabójcę. W ciągu tygodnia udało jej się złożyć do kupy pułapkę, rozpocząć linię przerzutową artefaktów do Aurum, połączyć Orbiter, Lilię, Minerwę oraz Pustogor i doprowadzić do tego, że docelowo Małmałaz ma zostać złapany i doprowadzony do Pustogoru w zemście za krzywdę Alana. To był bardzo trudny tydzień, politycznie.

Aktor w Opowieści:

* Dokonanie:
    * smutno mu, że informacja o Cieniu wypłynęła i Małmałaz na Cienia poluje. Współpracuje z Pięknotką - ona da mu znać, on wjedzie trotylem i napalmem i porwie Małmałaza.


### Bardzo nieudane porwania

* **uid:** 190503-bardzo-nieudane-porwania, _numer względny_: 1
* **daty:** 0110-04-14 - 0110-04-16
* **obecni:** Damian Orion, Karla Mrozik, Minerwa Metalia, Mirela Orion, Nikola Kirys, Pięknotka Diakon

Streszczenie:

Kirasjerzy Orbitera dostali zadanie odbicia Minerwy i Nikoli, ale w mieszkaniu Minerwy czekała Pięknotka. Porwała z zaskoczenia Emulatorkę, uwolniła ją Cieniem i gdy Kirasjerzy uderzyli by po cichu odzyskać agentkę, Pięknotka poprosiła o pomoc Epirjon. W wyniku Pięknotce udało się ewakuować z Emulatorką, uwolniła ją Cieniem i wsadziła do Pustogoru. Potem z Karlą wynegocjowała ochronę dla Minerwy i Nikoli i jeszcze z tymi dwoma załatwiła by to było możliwe. Kirasjerzy zostali odepchnięci przez JEDNĄ terminuskę Pustogoru.

Aktor w Opowieści:

* Dokonanie:
    * dowódca Kirasjerów; zaplanował operację, dowodził operacją, stracił Mirelę i nie przewidział Epirjona. Drugi raz nie zrobi tego błędu.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 7, @: 0111-05-22
    1. Primus    : 7, @: 0111-05-22
        1. Sektor Astoriański    : 7, @: 0111-05-22
            1. Astoria, Orbita    : 3, @: 0111-05-22
                1. Kontroler Pierwszy    : 2, @: 0110-12-04
                    1. Arena Kalaternijska    : 1, @: 0110-12-04
                    1. Hangary Alicantis    : 1, @: 0110-11-15
            1. Astoria    : 3, @: 0110-07-23
                1. Sojusz Letejski, SW    : 1, @: 0110-04-16
                    1. Granica Anomalii    : 1, @: 0110-04-16
                        1. Pacyfika, obrzeża    : 1, @: 0110-04-16
                1. Sojusz Letejski    : 3, @: 0110-07-23
                    1. Szczeliniec    : 3, @: 0110-07-23
                        1. Powiat Jastrzębski    : 1, @: 0110-07-23
                            1. Jastrząbiec, okolice    : 1, @: 0110-07-23
                                1. Klinika Iglica    : 1, @: 0110-07-23
                                    1. Kompleks Itaran    : 1, @: 0110-07-23
                                1. TechBunkier Sarrat    : 1, @: 0110-07-23
                            1. Kalbark    : 1, @: 0110-07-23
                                1. Escape Room Lustereczko    : 1, @: 0110-07-23
                        1. Powiat Pustogorski    : 2, @: 0110-07-23
                            1. Podwiert    : 1, @: 0110-04-16
                                1. Kosmoport    : 1, @: 0110-04-16
                            1. Pustogor    : 1, @: 0110-04-16
                                1. Interior    : 1, @: 0110-04-16
                                    1. Bunkry Barbakanu    : 1, @: 0110-04-16
                                1. Rdzeń    : 1, @: 0110-04-16
                                    1. Szpital Terminuski    : 1, @: 0110-04-16
                            1. Zaczęstwo, obrzeża    : 1, @: 0110-04-16
                            1. Zaczęstwo    : 2, @: 0110-07-23
                                1. Osiedle Ptasie    : 1, @: 0110-04-16
                        1. Pustogor    : 1, @: 0110-06-27
            1. Obłok Lirański    : 1, @: 0110-11-22
        1. Sektor Lacarin    : 1, @: 0110-11-22

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 4 | ((200722-wielki-kosmiczny-romans; 200729-nocna-krypta-i-emulatorka; 200819-sekrety-orbitera-historia-prawdziwa; 201230-pulapka-z-anastazji)) |
| Eustachy Korkoran    | 4 | ((200722-wielki-kosmiczny-romans; 200729-nocna-krypta-i-emulatorka; 200819-sekrety-orbitera-historia-prawdziwa; 201230-pulapka-z-anastazji)) |
| Elena Verlen         | 3 | ((200722-wielki-kosmiczny-romans; 200819-sekrety-orbitera-historia-prawdziwa; 201230-pulapka-z-anastazji)) |
| Klaudia Stryk        | 3 | ((200729-nocna-krypta-i-emulatorka; 200819-sekrety-orbitera-historia-prawdziwa; 201230-pulapka-z-anastazji)) |
| Leona Astrienko      | 3 | ((200722-wielki-kosmiczny-romans; 200729-nocna-krypta-i-emulatorka; 200819-sekrety-orbitera-historia-prawdziwa)) |
| Minerwa Metalia      | 3 | ((190503-bardzo-nieudane-porwania; 191103-kontrpolowanie-pieknotki-pulapka; 200202-krucjata-chevaleresse)) |
| Pięknotka Diakon     | 3 | ((190503-bardzo-nieudane-porwania; 191103-kontrpolowanie-pieknotki-pulapka; 200202-krucjata-chevaleresse)) |
| AK Nocna Krypta      | 2 | ((200729-nocna-krypta-i-emulatorka; 201230-pulapka-z-anastazji)) |
| Antoni Kramer        | 2 | ((200722-wielki-kosmiczny-romans; 200729-nocna-krypta-i-emulatorka)) |
| Mariusz Trzewń       | 2 | ((191103-kontrpolowanie-pieknotki-pulapka; 200202-krucjata-chevaleresse)) |
| Martyn Hiwasser      | 2 | ((200819-sekrety-orbitera-historia-prawdziwa; 201230-pulapka-z-anastazji)) |
| Mirela Orion         | 2 | ((190503-bardzo-nieudane-porwania; 200729-nocna-krypta-i-emulatorka)) |
| Tadeusz Ursus        | 2 | ((200722-wielki-kosmiczny-romans; 200819-sekrety-orbitera-historia-prawdziwa)) |
| AK Rodivas           | 1 | ((201230-pulapka-z-anastazji)) |
| Anastazja Sowińska Dwa | 1 | ((201230-pulapka-z-anastazji)) |
| Ataienne             | 1 | ((200202-krucjata-chevaleresse)) |
| Diana Tevalier       | 1 | ((200202-krucjata-chevaleresse)) |
| Henryk Sowiński      | 1 | ((201230-pulapka-z-anastazji)) |
| Izabela Zarantel     | 1 | ((200819-sekrety-orbitera-historia-prawdziwa)) |
| Józef Małmałaz       | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Julian Muszel        | 1 | ((200722-wielki-kosmiczny-romans)) |
| Karla Mrozik         | 1 | ((190503-bardzo-nieudane-porwania)) |
| Keraina d'Orion      | 1 | ((200202-krucjata-chevaleresse)) |
| Kijara d'Esuriit     | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Konrad Wolczątek     | 1 | ((200722-wielki-kosmiczny-romans)) |
| Ksenia Kirallen      | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Laura Orion          | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Leszek Kurzmin       | 1 | ((200819-sekrety-orbitera-historia-prawdziwa)) |
| Lilia Ursus          | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Malictrix d'Itaran   | 1 | ((200202-krucjata-chevaleresse)) |
| Marek Puszczok       | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Mateusz Kardamacz    | 1 | ((200202-krucjata-chevaleresse)) |
| Nikola Kirys         | 1 | ((190503-bardzo-nieudane-porwania)) |
| OA Zguba Tytanów     | 1 | ((201230-pulapka-z-anastazji)) |
| Olaf Zuchwały        | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Olgierd Drongon      | 1 | ((200722-wielki-kosmiczny-romans)) |
| OO Castigator        | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| OO Infernia          | 1 | ((201230-pulapka-z-anastazji)) |
| OO Minerwa           | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| OO Welgat            | 1 | ((200722-wielki-kosmiczny-romans)) |
| OO Żelazko           | 1 | ((200722-wielki-kosmiczny-romans)) |
| Rafał Roszczeniok    | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Sabina Servatel      | 1 | ((200819-sekrety-orbitera-historia-prawdziwa)) |
| Talia Aegis          | 1 | ((200202-krucjata-chevaleresse)) |
| Tomasz Tukan         | 1 | ((200202-krucjata-chevaleresse)) |
# Leo Kasztop
## Identyfikator

Id: 9999-leo-kasztop

## Sekcja Opowieści

### Astralna Flara w strefie duchów

* **uid:** 221130-astralna-flara-w-strefie-duchow, _numer względny_: 5
* **daty:** 0100-08-07 - 0100-08-10
* **obecni:** Arianna Verlen, Arnulf Perikas, Daria Czarnewik, Elena Verlen, Ellarina Samarintael, Gabriel Lodowiec, Hubert Kerwelenios, Kajetan Kircznik, Kirea Rialirat, Klarysa Jirnik, Leo Kasztop, Maja Samszar, NekroTAI Zarralea, OO Astralna Flara, OO Athamarein, Szymon Wanad, Tomasz Dojnicz

Streszczenie:

Planetoida Kazmirian została przejęta przez Orbiter. Daria ostrzegła Nonarion (Leo), że Orbiter nadal poluje na TAI i zdobyła informację o 'strefie duchów'. Z Mają doszły do tego, że 'strefa duchów' jest świetnym maskowaniem; Lodowiec wyprowadził tam Orbiter a Arianna stworzyła nekroTAI z Zarralei d'Isigtand. Używając advancera przechwycili samotną savarankę (której Lodowiec nie umie przesłuchać) i stanęło na tym, że 'strefa duchów' jest bazą noktiańską zbudowaną dookoła jednostki wsparcia, 'Domina Lucis'. Lodowiec chce doprowadzić noktian do sprawiedliwości Orbitera, Arianna i Daria są skonfliktowane. Mają jeńca - Kireę, lokalną młodą savarankę.

Aktor w Opowieści:

* Dokonanie:
    * przekazał Darii informacje o Strefie Duchów i planetoidzie TKO-4271; po tym jak Daria ostrzegła go o działaniach Orbitera wobec neikatiańskich TAI przekazał info gdzie trzeba.


### Astralna Flara dociera do Nonariona Nadziei

* **uid:** 221116-astralna-flara-dociera-do-nonariona-nadziei, _numer względny_: 4
* **daty:** 0100-07-11 - 0100-07-14
* **obecni:** Alan Nierkamin, Alezja Dumorin, Arianna Verlen, Arnulf Perikas, Daria Czarnewik, Gabriel Lodowiec, Grażyna Burgacz, Kajetan Kircznik, Leo Kasztop, Leszek Kurzmin, Maja Samszar, Marcel Kulgard, OO Astralna Flara, OO Athamarein, SCA Hadiah Emas, SCA Isigtand, Szymon Wanad, Tomasz Ruppok, Władawiec Diakon

Streszczenie:

Flara dociera do Nonariona, który wygląda jak absolutny śmieć. Morale na Flarze ucierpiało - aż do plotek o Egzotycznych Pięknościach, co sprawiło Ariannie kolejne problemy. Alan, eks-lokals powiedział Darii że jest jakaś forma standaryzacji na Nonarionie - coś tam jest, coś "bogatego" jako frakcja. Daria zdobyła od Leo planetoidę którą może użyć jako bazę dla Orbitera, ale w drodze tam zboczyli by ratować uszkodzone statki lokalne. I uratowali jednostkę niewolniczą i zniszczony grazer. Komodor Lodowiec - sensowny koleś - ma dylemat moralny. Neikatiańska TAI oraz niewolnicy.

Aktor w Opowieści:

* Dokonanie:
    * postarzał się; powiedział Darii, że 'nie jest już stąd'. Ale sprzedał jej dobrą planetoidę, choć z anomalią. Będzie z nią współpracował.


### Ailira, niezależna handlarka wodą

* **uid:** 221113-ailira-niezalezna-handlarka-woda, _numer względny_: 3
* **daty:** 0090-04-24 - 0090-04-30
* **obecni:** Ailira Niiris, Daria Czarnewik, Filip Szukurkor, Iga Mikikot, Jakub Uprzężnik, Julia Karnit, Kaspian Certisarius, Leo Kasztop, Ludwik Trójkadur, Nastia Barbatov, Patryk Lapszyn, Safira d'Hiyori

Streszczenie:

Hiyori kończą się pieniądze. Nastia i Jakub biorą robotę najemniczą u byłego eks Nastii. Kaspian jest P.O. Daria składa Hiyori z różnych dostawców i możliwie tanich a dobrych komponentów. Na Kaspiana poluje niejaka Julia uważając go za komandosa advancera mordercę. Okazuje się też, że z 6 statków co miały wodę Ailiry 4 nie wróciły. Ailira została pobita, straciła grazera i sama sprzedała się sarderytom na części. Gdy Kaspian o tym usłyszał, absolutnie zakazał Hiyori coś z tym robić. Czekamy na Ogdena i Nastię. Jednak stacja Nonarion to zło.

Aktor w Opowieści:

* Dokonanie:
    * kryminalista mający coś pozytywnego w przeszłości z Ogdenem; wyraźnie próbuje pomóc Hiyori odpychając ich od Ailiry i znajdując subzlecenia. Całkowicie amoralny, tylko chrupki się liczą. Ale ostrzegł Darię przed zleceniem.


### Niebezpieczna woda na Hiyori

* **uid:** 221111-niebezpieczna-woda-na-hiyori, _numer względny_: 2
* **daty:** 0090-04-19 - 0090-04-23
* **obecni:** Ailira Niiris, Daria Czarnewik, Iga Mikikot, Jakub Uprzężnik, Kaspian Certisarius, Leo Kasztop, Nastia Barbatov, Ogden Barbatov, Patryk Lapszyn, Safira d'Hiyori, SCA Hiyori

Streszczenie:

Załoga Hiyori kupiła lokalizację nowej jednostki w Anomalii na salvage. Jednak podczas lotu okazało się, że zakupiona woda (od stałego dostawcy) jest niebezpieczna; ma bioformy reagujące z energią. Hiyori pozbyła się wody, wysłała sygnał SOS i zamknęli wodę w pojemnikach biocontain. Ogden poleciał do Orbitera użyć starych kontaktów i sprzedać a Nastia skonfrontowała się ze sprzedawcą wody. Leo (sprzedawca informacji) ich ostrzegł, żeby nie brali tanich pożyczek - coś się dzieje na Nonarionie.

Aktor w Opowieści:

* Dokonanie:
    * zmartwił się tym, że Hiyori może mieć problemy finansowe. Ostrzegł Nastię, by nie brała tanich pożyczek bo dzieje się tu coś dziwnego.


### Derelict Okarantis: wejście

* **uid:** 221022-derelict-okarantis-wejscie, _numer względny_: 1
* **daty:** 0090-01-03 - 0090-01-14
* **obecni:** Daria Czarnewik, Iga Mikikot, Jakub Uprzężnik, Kaspian Certisarius, Leo Kasztop, Nastia Barbatov, Ogden Barbatov, Patryk Lapszyn, Safira d'Hiyori, SCA Hiyori

Streszczenie:

SCA Salvager Hiyori wlatuje do Anomalii Kolapsu, by dostać się do nieznanego derelicta Okarantis. Podczas operacji salvagowania główny advancer został ranny i pojawiła się konieczność rozbicia Okarantis na dwie części - anomalną i "bezpieczną". Zespół ma wejście na bezpieczną część Okarantis i może rozpocząć salvagowanie.

Aktor w Opowieści:

* Dokonanie:
    * sprzedawca sekretów na Nonarionie Nadziei; |ENCAO:  0-+00 |Intrygancki, polityka;;Nie odracza| VALS: Self-direction >> Stimulation, Tradition| DRIVE: Wygrać w rywalizacji)|; sprzedał Barbatovowi informacje o derelikcie Okarantis znajdującym się w Anomalii Kolapsu. Zna się z Ogdenem dobrze.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 5, @: 0100-08-10
    1. Primus    : 5, @: 0100-08-10
        1. Sektor Astoriański    : 5, @: 0100-08-10
            1. Obłok Lirański    : 5, @: 0100-08-10
                1. Anomalia Kolapsu, orbita    : 5, @: 0100-08-10
                    1. Planetoida Kazmirian    : 1, @: 0100-07-14
                    1. SC Nonarion Nadziei    : 4, @: 0100-07-14
                        1. Moduł ExpanLuminis    : 2, @: 0100-07-14
                        1. Moduł Remedianin    : 1, @: 0090-04-30
                    1. Strefa Upiorów Orbitera    : 1, @: 0100-08-10
                        1. Planetoida Kazmirian    : 1, @: 0100-08-10
                        1. Planetoida Lodowca    : 1, @: 0100-08-10
                1. Anomalia Kolapsu    : 1, @: 0090-01-14
                    1. Cmentarzysko Statków    : 1, @: 0090-01-14

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Daria Czarnewik      | 5 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221130-astralna-flara-w-strefie-duchow)) |
| Iga Mikikot          | 3 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda)) |
| Jakub Uprzężnik      | 3 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda)) |
| Kaspian Certisarius  | 3 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda)) |
| Nastia Barbatov      | 3 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda)) |
| Patryk Lapszyn       | 3 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda)) |
| Safira d'Hiyori      | 3 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda)) |
| Ailira Niiris        | 2 | ((221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda)) |
| Arianna Verlen       | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221130-astralna-flara-w-strefie-duchow)) |
| Arnulf Perikas       | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221130-astralna-flara-w-strefie-duchow)) |
| Gabriel Lodowiec     | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221130-astralna-flara-w-strefie-duchow)) |
| Kajetan Kircznik     | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221130-astralna-flara-w-strefie-duchow)) |
| Maja Samszar         | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221130-astralna-flara-w-strefie-duchow)) |
| Ogden Barbatov       | 2 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori)) |
| OO Astralna Flara    | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221130-astralna-flara-w-strefie-duchow)) |
| OO Athamarein        | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221130-astralna-flara-w-strefie-duchow)) |
| SCA Hiyori           | 2 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori)) |
| Szymon Wanad         | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221130-astralna-flara-w-strefie-duchow)) |
| Alan Nierkamin       | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Alezja Dumorin       | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Elena Verlen         | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| Ellarina Samarintael | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| Filip Szukurkor      | 1 | ((221113-ailira-niezalezna-handlarka-woda)) |
| Grażyna Burgacz      | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Hubert Kerwelenios   | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| Julia Karnit         | 1 | ((221113-ailira-niezalezna-handlarka-woda)) |
| Kirea Rialirat       | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| Klarysa Jirnik       | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| Leszek Kurzmin       | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Ludwik Trójkadur     | 1 | ((221113-ailira-niezalezna-handlarka-woda)) |
| Marcel Kulgard       | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| NekroTAI Zarralea    | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| SCA Hadiah Emas      | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| SCA Isigtand         | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Tomasz Dojnicz       | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| Tomasz Ruppok        | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Władawiec Diakon     | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
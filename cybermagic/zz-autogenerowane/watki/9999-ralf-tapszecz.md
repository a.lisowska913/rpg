# Ralf Tapszecz
## Identyfikator

Id: 9999-ralf-tapszecz

## Sekcja Opowieści

### Atak na Kidirona

* **uid:** 230614-atak-na-kidirona, _numer względny_: 6
* **daty:** 0093-03-22 - 0093-03-24
* **obecni:** Ardilla Korkoran, Bartłomiej Korkoran, Eustachy Korkoran, Kalia Awiter, Lycoris Kidiron, OO Infernia, Rafał Kidiron, Ralf Tapszecz, SAN Szare Ostrze, Stanisław Uczantor

Streszczenie:

Gdy Wujek się obudził, powiedział że z Infernią mogą być ostre problemy jeśli spada poniżej poziomu Memoriam. Zmartwił się hipotezą Ardilli że Ralf może być magiem Nihilusa, ale Ardilla walczyła o dalsze spotykanie się z Ralfem. Zamach na Kidirona. Większość jego sztabu umiera, ale sam Kidiron przeżywa, acz ciężko ranny; ratuje go Ardilla i pozyskuje info o jego mrocznych planach. Kalia bierze na siebie morale i propagandę, ale przez to wpada na celownik Infiltratora który ją porywa. Eustachy próbuje odbić Kalię z rąk Infiltratora w koloidzie...

Aktor w Opowieści:

* Dokonanie:
    * Ardilla podejrzewa go o bycie magiem Nihilusa. On jeszcze nie wie. Gdy doszło do ataku na arkologię, Ralf od razu pojawił się przy Ardilli i chroni ją i tylko ją. Skutecznie zgasił pożar drzew w arkologii, zaczynając od drzew które Ardilla lubi najbardziej.


### Zdrada rozrywająca arkologię

* **uid:** 230329-zdrada-rozrywajaca-arkologie, _numer względny_: 5
* **daty:** 0093-03-14 - 0093-03-16
* **obecni:** Amelia Sarkaldir, Ardilla Korkoran, BIA Prometeus, Celina Lertys, Eustachy Korkoran, Ewelina Paroknis, Feliks Kidiron, Franciszek Pietraszczyk, Iwona Paroknis, Jonasz Paroknis, Kalia Awiter, Katarzyna Falernik, Małgorzata Maratelus, OO Infernia, Rafał Kidiron, Ralf Tapszecz, Szczepan Falernik, Wojciech Grzebawron

Streszczenie:

Nastolatka miała pamiętnik czytany przez macochę. Ojciec to ignorował. Nastolatka napisała w pamiętniku, że ojciec zdradza macochę z ekspertką hydrologii. Zrobił się shitstorm który doprowadził do poważnych problemów - m.in. macocha (dyrektor skrzydła medycznego) zaczęła robić błędy co wykryła Celina. Kidiron podejrzewa spisek - ktoś próbuje skłócić ważne rodziny, a także shackować Prometeusa (BIA arkologii). Rozmowa z ich eks-piratami kończy się tym, że to nie oni - za tym stoi wyspecjalizowana siła z innej arkologii lub noktianie. 

Ślady prowadzą do pamiętnika nastolatki, który shackowany przez ekspertkę Inferni (i upubliczniony przez problemy na linii Infernia - Prometeus) pokazują, że najpewniej nastolatka za wszystkim stoi sama. Uratowana przez tajemniczego maga Interis przed samobójstwem nastolatka przyznała na czym polegał problem i ujawnia, że pomaga jej Feliks Kidiron, najmłodszy z rodu i jej jedyny przyjaciel. Czyli to nie spisek. Ale jak to teraz naprawić?

Aktor w Opowieści:

* Dokonanie:
    * wiedząc, że pamiętnik Eweliny został upubliczniony i wiedząc, że Ardilli na Ewelinie zależy był w pobliżu Eweliny gdy ta próbowała się zabić. Nie miał siły, by przenieść Ewelinę z Ardillą do bezpiecznego miejsca (savaranin jest słaby fizycznie). Uważa, że Ewelina zachowała się "nieefektywnie".


### Bardzo nieudane porwanie Inferni

* **uid:** 230315-bardzo-nieudane-porwanie-inferni, _numer względny_: 4
* **daty:** 0093-03-06 - 0093-03-09
* **obecni:** Ardilla Korkoran, Eustachy Korkoran, Hubert Grzebawron, Mariusz Dobrowąs, Nadia Sekernik, OO Infernia, Rafał Kidiron, Ralf Tapszecz, SAN Szare Ostrze, Wojciech Grzebawron

Streszczenie:

Mimo braku wujka, Infernia działa sprawnie. Wujek jest pod opieką medyczną, ale Celina nie ufając Kidironowi, chce być z nim cały czas. Infernia otrzymuje wezwanie od CES Mineralis, gdyż stacja została zaatakowana przez Trianai. Eustachy wysyła Czarne Hełmy i komandosów z Inferni, którzy wspólnie działają, ale Trianai jest za dużo. Eustachy wabi więc Trianai za pomocą energii magicznej, tracąc przytomność po połączeniu z koordynatorem Trianai (magiem), zabijając go. Stacja CES ucierpiała w Paradoksie Eustachego, powodując wiele ofiar. Ardilla koordynuje operację ratunkową, ale gdy okazuje się, że część załogi z CES atakuje mostek próbując ukraść statek, Ardilla wyłącza Infernię i z Ralfem wieją do kanałów Inferni.

Eustachy budzi się w niewoli, przekonuje oprychów, że jedynie on potrafi sterować Infernią (ucierpiał przy tym nieco). Ardilla i Ralf - na prośbę Eustachego - sabotują generatory Memoriam, pozwalając Inferni uwolnić się spod kontroli wrogów. Wywiązuje się strzelanina, w wyniku której napastnicy zostają pokonani - Eustachy kontroluje sytuację. Eustachy decyduje, że ich karą będzie służba na Inferni, ale jeden z nich odchodzi ostrzegać przed atakiem na Infernię. To wszystkim pasuje.

Ekipa wraca na statek, gdzie Kidiron proponuje przesłuchanie pojmanych piratów, obiecując immunitet w zamian za współpracę. Eustachy zgadza się, ale tylko z ochotnikami i Ardillą jako gwarantem bezpieczeństwa. Piraci zgadzają się współpracować mimo użycia neuroobroży przy przesłuchaniu przez Kidirona. Gdy Zespół próbuje montować nowe generatory Memoriam - nie da się. Nie mogą być zamontowane, gdyż się palą. Infernia wykrywa próbę montażu nowych, ale nie zauważa starych.

Aktor w Opowieści:

* Dokonanie:
    * uczestniczył w akcji ratunkowej (ale przede wszystkim pilnował Ardillę), gdy mostek był zaatakowany to szybkim atakiem zranił dwóch napastników po oaczach. Potem współpraca z Ardillą w sabotowaniu generatorów Memoriam i dywersji.


### Terrorystka w Ambasadorce

* **uid:** 230215-terrorystka-w-ambasadorce, _numer względny_: 3
* **daty:** 0093-02-22 - 0093-02-23
* **obecni:** Ardilla Korkoran, Bartłomiej Korkoran, Eustachy Korkoran, Kalia Awiter, Magda Misteria Sarbanik, Rafał Kidiron, Ralf Tapszecz, Tobiasz Lobrak, Tymon Korkoran

Streszczenie:

Kalia i Ardilla weszły do Ambasadorki by pomóc jeśli są w stanie, odcięły ich wiły i Ardilla ich poddaje. Misteria - napastniczka - okazuje się być czarodziejką roślin. Ona chce odzyskać wiły i zarzucić Kidironowi zniszczenie arkologii oraz porwanie wił. Jej celem jest ujawnienie prawdy o Kidironie, ukaranie Tobiasza (patrona Ambasadorki który robi złe rzeczy) i zabezpieczenie wił. Wujek chce zastąpić Ardillę, ale ona odmawia. Zespół (poza Ardillą) planuje zinfiltrować Ambasadorkę i usunąć maga (Misterię). 

Eustachy wpada na pomysł kontrolowanego obniżenia poziomu tlenu wewnątrz. Ardilla przekonuje Misterię do wymiany rannych na wujka. Ralf stanowi kanał komunikacyjny. Misteria tworzy Czarne Serce, kosztem wił i energii. Planują przekonać ludzi o winie Kidironów, wydostać się z Ambasadorki i stworzyć reportaż z Kalią. Ralf mówi o laserze na dachu, którym może zniszczyć czarodziejkę. Tobiasz wzbudza w sercu Misterii terror. Eustachy i Kidiron oczekują na rozwój sytuacji. W wyniku negocjacji, Kidiron zgadza się współpracować pod warunkiem, że Misteria nie będzie działać przeciwko niemu czy arkologii.

Kalia tworzy reportaż o Kidironie i arkologii Lirvint. Kidiron obiecuje bezpieczne wyjście dla Misterii i wił, pod warunkiem zapewnienia nieagresji. Ardilla próbuje przekonać Misterię do ewakuacji, ale ona nie chce opuścić miejsca bez rozwiązania sprawy z Tobiaszem. Czarne Serce wpływa na Misterię, więc wujek ją obezwładnia wstrzykując jej jakieś świństwo, sam zostaje poraniony przez wiły. Kidiron dotrzymał słowa odnośnie ewakuacji, acz postawił na Misterii tracker.

Aktor w Opowieści:

* Dokonanie:
    * w jakiś sposób dał radę nawiązać połączenie z Ardillą gdy ona była w Ambasadorce pełznąc korytarzami. Nawiązał link Eustachy - Ardilla i zadbał o bezpieczeństwo wszystkich.


### Pierwsza randka Eustachego

* **uid:** 230208-pierwsza-randka-eustachego, _numer względny_: 2
* **daty:** 0093-02-14 - 0093-02-21
* **obecni:** Ardilla Korkoran, Bartłomiej Korkoran, Eustachy Korkoran, Franciszek Pietraszczyk, Kalia Awiter, Marcin Pietraszczyk, OO Infernia, Rafał Kidiron, Ralf Tapszecz

Streszczenie:

Wujek opieprza Eustachego za krzywdę niewinnych noktian, ale Kidiron się za Eustachym wstawia. Ardilla socjalizuje Ralfa i ratuje młodego Marcinka który się zaklinował w rurze Szczurowiska; okazuje się, że wszyscy kojarzą Infernię jako symbol nadziei. Potem Eustachy idzie (z przymusu) na randkę z Kalią; okazuje się, że Kalia zmanipulowała by Eustachy wygrał bo chce dać mu jeden piękny dzień. Wszystkie dowody podsłuchała i złapała Ardilla. Gdy Eustachy poczuł chore emanacje magiczne z Ambasadorki, porzucił Kalię i pobiegł do Inferni.

Aktor w Opowieści:

* Dokonanie:
    * powoli zbliża się do Ardilli. Wyraźnie ma problemy z konfliktem kultury savarańskiej (musisz być użyteczny) a tym co _chce_. Przyznał się Ardilli że widział oczy Nihilusa i to go zmieniło. Wyraźnie chce pomóc innym, ale nie rozumie dlaczego warto pomóc komuś kto nie ma już potencjału - ale CHCE pomóc. Bardzo skonfliktowany. WIE, że mag (Eustachy) nie może być sam. Ma problem z pozycjonowaniem Bartłomieja Korkorana jako bohatera lub głupca marnującego zasoby.


### Wyłączone generatory Memoriam Inferni

* **uid:** 230201-wylaczone-generatory-memoriam-inferni, _numer względny_: 1
* **daty:** 0093-02-10 - 0093-02-12
* **obecni:** Ardilla Korkoran, Bartłomiej Korkoran, Celina Lertys, Ernest Puszczowiec, Eustachy Korkoran, Jan Lertys, Kalia Awiter, Kalista Luminis, Kratos Coruscatis, OO Infernia, Rafał Kidiron, Ralf Tapszecz, Tymon Korkoran

Streszczenie:

Eustachy przebudzony przez Infernię po burzy piaskowej poszedł za jej podszeptami i wyłączył generatory Memoriam. Infernia go Dotknęła - jest to anomalia Interis uwięziona w mechanicznej skorupie. Tylko Eustachy może Infernią sterować. Więc gdy pojawiła się misja pomocy bazie noktiańskiej Coruscatis, Eustachy musiał lecieć. Kidiron chciał aresztowania Kallisty Luminis a Wujek chciał pomocy noktianom. Okazało się, że na miejscu sformował się amalgamoid trianai i Infernia została ostrzelana; acz odpowiedziała ogniem. Zespół pomógł i ewakuował noktian i zdestabilizował reaktor fuzyjny by wysadzić wszystko. Pod wpływem Ardilli i Celiny Eustachy nie oddał noktian Kidironowi a wysadził ich w bezpiecznym miejscu. Kolejny dowód, że arkologia Nativis wykorzystuje żywność jako broń, co jest niezgodne z wszelkimi konwencjami.

Aktor w Opowieści:

* Dokonanie:
    * nowy dodatek do Inferni, mag (domena: DOMINUJĄCA ROZPACZ) i kultysta Interis. Noktianin, savaranin. (ENCAO:  -00-+ |Bezbarwny, przezroczysty;;Buja w obłokach| VALS: Conformity, Humility >> Benevolence| DRIVE: Piękno zniszczenia). Świetny w bezszelestności i infiltracji, nigdy się nie skarży, polubił się z Ardillą. Wujek dodał go do Inferni by mu pomóc.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 6, @: 0093-03-24
    1. Primus    : 6, @: 0093-03-24
        1. Sektor Astoriański    : 6, @: 0093-03-24
            1. Neikatis    : 6, @: 0093-03-24
                1. Dystrykt Glairen    : 6, @: 0093-03-24
                    1. Arkologia Lirvint    : 1, @: 0093-02-23
                    1. Arkologia Nativis    : 5, @: 0093-03-24
                        1. Dzielnica Luksusu    : 4, @: 0093-03-24
                            1. Ambasadorka Ukojenia    : 2, @: 0093-02-23
                            1. Ogrody Wiecznej Zieleni    : 3, @: 0093-03-24
                            1. Stacja holosymulacji    : 1, @: 0093-02-21
                        1. Stara Arkologia Wschodnia    : 3, @: 0093-03-24
                            1. Blokhaus F    : 2, @: 0093-03-16
                                1. Szczurowisko    : 2, @: 0093-03-16
                    1. Arkologia Sarviel, okolice    : 1, @: 0093-02-12
                        1. Krater Coruscatis    : 1, @: 0093-02-12
                    1. CES Mineralis    : 1, @: 0093-03-09

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ardilla Korkoran     | 6 | ((230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230315-bardzo-nieudane-porwanie-inferni; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona)) |
| Eustachy Korkoran    | 6 | ((230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230315-bardzo-nieudane-porwanie-inferni; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona)) |
| Rafał Kidiron        | 6 | ((230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230315-bardzo-nieudane-porwanie-inferni; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona)) |
| Kalia Awiter         | 5 | ((230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona)) |
| OO Infernia          | 5 | ((230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230315-bardzo-nieudane-porwanie-inferni; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona)) |
| Bartłomiej Korkoran  | 4 | ((230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230614-atak-na-kidirona)) |
| Celina Lertys        | 2 | ((230201-wylaczone-generatory-memoriam-inferni; 230329-zdrada-rozrywajaca-arkologie)) |
| Franciszek Pietraszczyk | 2 | ((230208-pierwsza-randka-eustachego; 230329-zdrada-rozrywajaca-arkologie)) |
| SAN Szare Ostrze     | 2 | ((230315-bardzo-nieudane-porwanie-inferni; 230614-atak-na-kidirona)) |
| Tymon Korkoran       | 2 | ((230201-wylaczone-generatory-memoriam-inferni; 230215-terrorystka-w-ambasadorce)) |
| Wojciech Grzebawron  | 2 | ((230315-bardzo-nieudane-porwanie-inferni; 230329-zdrada-rozrywajaca-arkologie)) |
| Amelia Sarkaldir     | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| BIA Prometeus        | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Ernest Puszczowiec   | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Ewelina Paroknis     | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Feliks Kidiron       | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Hubert Grzebawron    | 1 | ((230315-bardzo-nieudane-porwanie-inferni)) |
| Iwona Paroknis       | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Jan Lertys           | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Jonasz Paroknis      | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Kalista Luminis      | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Katarzyna Falernik   | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Kratos Coruscatis    | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Lycoris Kidiron      | 1 | ((230614-atak-na-kidirona)) |
| Magda Misteria Sarbanik | 1 | ((230215-terrorystka-w-ambasadorce)) |
| Małgorzata Maratelus | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Marcin Pietraszczyk  | 1 | ((230208-pierwsza-randka-eustachego)) |
| Mariusz Dobrowąs     | 1 | ((230315-bardzo-nieudane-porwanie-inferni)) |
| Nadia Sekernik       | 1 | ((230315-bardzo-nieudane-porwanie-inferni)) |
| Stanisław Uczantor   | 1 | ((230614-atak-na-kidirona)) |
| Szczepan Falernik    | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Tobiasz Lobrak       | 1 | ((230215-terrorystka-w-ambasadorce)) |
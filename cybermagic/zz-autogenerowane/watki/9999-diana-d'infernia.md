# Diana d'Infernia
## Identyfikator

Id: 9999-diana-d'infernia

## Sekcja Opowieści

### Kult Saitaera w Neotik

* **uid:** 211222-kult-saitaera-w-neotik, _numer względny_: 5
* **daty:** 0112-02-24 - 0112-02-25
* **obecni:** Adam Szarjan, Arianna Verlen, Diana d'Infernia, Elena Verlen, Eustachy Korkoran, Izabela Zarantel, Kasandra Destrukcja Diakon, Klaudia Stryk, Lutus Amerin, Maciek Kalmarzec, Maria Naavas

Streszczenie:

Klaudia wróciła z bronią anty-Serenitową od Tosena, Arianna z feromonami Marii nadała Inferni nową kulturę - miłośnicy lolitek x kultyści x noktianie. Elena uziemiona w Inferni zinfiltrowała Stocznię ukryta przez Klaudię i odkryła niższe Spustoszenie + kult Saitaera. Nereida jest czysta od Saitaera; przetransferowana na Infernię dla Eleny i uzbrojona. Klaudia ma listę jednostek potencjalnie w rękach Saitaera / Spustoszonych. Przygotowania do kontrataku skończone.

Aktor w Opowieści:

* Dokonanie:
    * nadal jest przekonana, że jest silniejsza niż ixioński potwór Saitaera na Neotik. "Skoro nawet TA LASKA (Elena) to przetrwała to ja to zjem.". Chce współpracować z Eustachym. "Kochana lolitka Inferni".
* Progresja:
    * absolutnie nie znosi Eleny. Nie chce z nią współpracować, pomagać itp. Tylko na wyraźny rozkaz Eustachego.
    * aktywnie CHCE pomóc Eustachemu. Nie wykonuje rozkazów ze strachu, ale też bo chce.


### Sklejanie Inferni do kupy

* **uid:** 211215-sklejanie-inferni-do-kupy, _numer względny_: 4
* **daty:** 0112-02-21 - 0112-02-23
* **obecni:** Adam Szarjan, Arianna Verlen, Diana d'Infernia, Elena Verlen, Eustachy Korkoran, Kamil Lyraczek, Kasandra Destrukcja Diakon, Leona Astrienko, Maria Naavas, OO Infernia, Roland Sowiński

Streszczenie:

Ixioński wariant Inferni okazuje się całkiem słodki i pod kontrolą Eustachego. Morale Inferni jest w ruinie. Z pomocą Marii Arianna przesuwa kulturę Inferni w stronę "kultyści + noktianie + miłośnicy psychopatycznej lolitki Diany". Eustachy wpada w pułapkę Leony, która chce go wyleczyć czerwiem Esuriit - ale Eustachy przekonuje ją, że ma Dianę pod kontrolą. Jednak gdzieś w tle znajduje się prawdziwy ixioński potwór na stoczni Neotik.

Aktor w Opowieści:

* Dokonanie:
    * przekonana, że może POŻREĆ Serenit; nieco nie ma balansu między wiedzą i mocą. Dość słodka, jak na ixiońską stabilną efemerydę. Modeluje charakter po Leonie; psychopatyczna słodka lolitka.
* Progresja:
    * jest słodsza i poczciwa w brutalny sposób, co dla nowych jest STRASZNE. "Młodsza siostrzyczka", creepy like fuck. W stylu Eustachego. Ale JEST słodsza.


### O krok za daleko

* **uid:** 211208-o-krok-za-daleko, _numer względny_: 3
* **daty:** 0112-02-19 - 0112-02-20
* **obecni:** Adam Szarjan, Arianna Verlen, Diana d'Infernia, Elena Verlen, Eustachy Korkoran, Hestia d'Neotik, Jarosław Szarjan, Klaudia Stryk, Leona Astrienko, OO Infernia, Wawrzyn Rewemis

Streszczenie:

Klaudia doszła do tego, że Hestia d'Neotik oszukuje Stocznię; w okolicy są niewielkie statki koloidowe o których Stocznia nie wie. Arianna zintegrowała Eustachego z Infernią ixiońsko, niestety anomalizując Infernię. Elena ciężko Skażona ixionem i (w swoim mniemaniu) porzucona przez Eustachego. Eustachy ujarzmiając Infernię rozbijał ją o różne asteroidy, co powoduje śmierć części załogi. Infernia ma zerowe morale, ale jest opanowana. Przyjazna Anomalia Kosmiczna z tymczasową bazą w Stoczni Neotik.

Aktor w Opowieści:

* Dokonanie:
    * anomalizacja ixiońska zmieniła ją w TAI Inferni; integruje Persefonę, Morrigan i wiele innych bytów. Wszechobecny na Inferni byt, kontroluje Infernię totalnie.


### Uszkodzona Brama Eteryczna

* **uid:** 210825-uszkodzona-brama-eteryczna, _numer względny_: 2
* **daty:** 0111-11-30 - 0111-12-03
* **obecni:** AK Nocna Krypta, Arianna Verlen, Diana d'Infernia, Elena Verlen, Eustachy Korkoran, Flawia Blakenbauer, Gilbert Bloch, Klaudia Stryk, Medea Sowińska, ON Spatium Gelida, OO Infernia, OO Mfumo

Streszczenie:

Infernia została przechwycona przez Medeę lecąc na Anomalię Kolapsu i przekierowana na uszkodzoną Bramę Eteryczną (gdzie, jak się okazuje, są pasożyty i flota noktiańska która nie doleciała). Na miejscu Infernia uniknęła efemeryd i channelując Kryptę uratowała wszystkich efemerydami. Gdy Krypta pojawiła się na serio, Infernia zwiała paląc silniki. Statki noktiańskie w Bramie mają tether na Infernię.

Aktor w Opowieści:

* Dokonanie:
    * powołana przez efekt Paradoksu Eustachego, nacisnęła za niego spust (którego nie chciał naciskać) i spowodowała kłopoty z efemerydami. Rozwiana przez Elenę. Obiecała Eustachemu, że Elena i Eustachy będą razem.


### Infernia jest nasza!

* **uid:** 210804-infernia-jest-nasza, _numer względny_: 1
* **daty:** 0111-04-23 - 0111-04-26
* **obecni:** Aleksandra Termia, Antoni Kramer, Arianna Verlen, Artur Traffal, Diana d'Infernia, Eustachy Korkoran, Franciszek Maszkiet, Klaudia Stryk, Leona Astrienko, Maciej Żarand, OO Samotność Gwiazd

Streszczenie:

Ku wielkiemu niezadowoleniu Zespołu, Infernia miała im być odebrana i złomowana. Mieli dostać nowy tańszy statek. Zespół odkrył że za tym stoi komodor Traffal z sił Termii który przy użyciu Inferni chciał złapać w potrzask arystokratę eternijskiego który był kultystą magii. Zespół odzyskał Infernię, przesunął kultystę w kierunku na Ariannę i uciemiężył komodora Traffala - będzie miał pecha na K1.

Aktor w Opowieści:

* Dokonanie:
    * psychotyczna aparycja Diany Arłacz; wróciła wezwana przez Eustachego magią Paradoksu. Zraniła Żaranda infiltrującego Infernię i z radością wylizywała jego ranę. Potem tańczyła dla Eustachego. Arianna ją "egzorcyzmowała" magią. Ale wróci :-).


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 5, @: 0112-02-25
    1. Primus    : 5, @: 0112-02-25
        1. Sektor Astoriański    : 5, @: 0112-02-25
            1. Astoria, Orbita    : 1, @: 0111-04-26
                1. Kontroler Pierwszy    : 1, @: 0111-04-26
            1. Astoria, Pierścień Zewnętrzny    : 3, @: 0112-02-25
                1. Poligon Stoczni Neotik    : 1, @: 0112-02-20
                1. Stocznia Neotik    : 3, @: 0112-02-25
                    1. Zewnętrzny dok ixioński    : 1, @: 0112-02-23
            1. Brama Kariańska    : 1, @: 0111-12-03

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 5 | ((210804-infernia-jest-nasza; 210825-uszkodzona-brama-eteryczna; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik)) |
| Eustachy Korkoran    | 5 | ((210804-infernia-jest-nasza; 210825-uszkodzona-brama-eteryczna; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik)) |
| Elena Verlen         | 4 | ((210825-uszkodzona-brama-eteryczna; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik)) |
| Klaudia Stryk        | 4 | ((210804-infernia-jest-nasza; 210825-uszkodzona-brama-eteryczna; 211208-o-krok-za-daleko; 211222-kult-saitaera-w-neotik)) |
| Adam Szarjan         | 3 | ((211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik)) |
| Leona Astrienko      | 3 | ((210804-infernia-jest-nasza; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy)) |
| OO Infernia          | 3 | ((210825-uszkodzona-brama-eteryczna; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy)) |
| Kasandra Destrukcja Diakon | 2 | ((211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik)) |
| Maria Naavas         | 2 | ((211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik)) |
| AK Nocna Krypta      | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| Aleksandra Termia    | 1 | ((210804-infernia-jest-nasza)) |
| Antoni Kramer        | 1 | ((210804-infernia-jest-nasza)) |
| Artur Traffal        | 1 | ((210804-infernia-jest-nasza)) |
| Flawia Blakenbauer   | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| Franciszek Maszkiet  | 1 | ((210804-infernia-jest-nasza)) |
| Gilbert Bloch        | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| Hestia d'Neotik      | 1 | ((211208-o-krok-za-daleko)) |
| Izabela Zarantel     | 1 | ((211222-kult-saitaera-w-neotik)) |
| Jarosław Szarjan     | 1 | ((211208-o-krok-za-daleko)) |
| Kamil Lyraczek       | 1 | ((211215-sklejanie-inferni-do-kupy)) |
| Lutus Amerin         | 1 | ((211222-kult-saitaera-w-neotik)) |
| Maciej Żarand        | 1 | ((210804-infernia-jest-nasza)) |
| Maciek Kalmarzec     | 1 | ((211222-kult-saitaera-w-neotik)) |
| Medea Sowińska       | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| ON Spatium Gelida    | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| OO Mfumo             | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| OO Samotność Gwiazd  | 1 | ((210804-infernia-jest-nasza)) |
| Roland Sowiński      | 1 | ((211215-sklejanie-inferni-do-kupy)) |
| Wawrzyn Rewemis      | 1 | ((211208-o-krok-za-daleko)) |
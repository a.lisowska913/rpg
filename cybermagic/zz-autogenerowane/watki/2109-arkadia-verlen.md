# Arkadia Verlen
## Identyfikator

Id: 2109-arkadia-verlen

## Sekcja Opowieści

### Prototypowa Nereida Natalii

* **uid:** 211124-prototypowa-nereida-natalii, _numer względny_: 8
* **daty:** 0112-02-14 - 0112-02-18
* **obecni:** Adam Szarjan, Arianna Verlen, Arkadia Verlen, Elena Verlen, Eustachy Korkoran, Izabela Zarantel, Klaudia Stryk, Leona Astrienko, Maria Naavas, Natalia Aradin, OO Infernia, Roland Sowiński, Wawrzyn Rewemis

Streszczenie:

Kampania reklamowa "dołącz do Inferni" zrobiona przez Izę przy użyciu Eustachego jako modela - sukces. Klaudia zdobyła z tego nowych załogantów (acz to jeszcze potrwa). Szarjan uwolnił Klaudię z zarzutów, potrzebuje pomocy w stoczni - jego przyjaciółka Natalia ma problem z prototypem myśliwca klasy Nereida i została ixiońsko zintegrowana. Infernia próbowała Nereidę uratować, ale niestety skończyło się to poważnym uszkodzeniem Nereidy i stworzeniem ixiońskiego kokonu, z którego Natalia musi się wykluć...

Aktor w Opowieści:

* Dokonanie:
    * wysłała fanmail na orbitę, który dostała Iza Zarantel. Chce więcej Arianny. Mniej Eustachego i Klaudii ;-).


### Supersupertajny plan Loreny

* **uid:** 220730-supersupertajny-plan-loreny, _numer względny_: 7
* **daty:** 0111-09-26 - 0111-09-30
* **obecni:** Arkadia Verlen, Daniel Terienak, Karolina Terienak, Liliana Bankierz, Marsen Gwozdnik, Władysław Owczarek, Żorż d'Namertel

Streszczenie:

Marsen Gwozdnik wszedł na teren by pomóc Lorenie (która nie wie) uważając Lorenę za najlepszego taktyka na świecie (którym nie jest). Wyedytował część kontraktów okolicznych firm z Eternią, by spowodować niechęć firm do Eterni (a Ernest próbuje pomóc by Mimoza nie była jedyną która pomaga). Karo zastawiła na Marsena pułapkę i wzięła jako wsparcie Arkadię. Marsen natomiast wziął Lancera i Lilianę. Skończyło się na rannej Arkadii, sprzedanym Lancerze, rannej Lilianie i tym, że Karo przekonała Marsenę że jest... agentką Loreny?

Aktor w Opowieści:

* Dokonanie:
    * PROPER PSYCHO. By chronić mały biznes podwiercki z Karo, uderzyła nożem w "Marka" (to była Liliana) a potem zmierzyła się z Lancerem w reaktywnym pancerzu przewracając go. Ranna, ale "warto było".
* Progresja:
    * ranna w walce z Lancerem który miał reaktywny pancerz. Tydzień kuracji, trochę z glizdą Sensacjusza. ŁA~TWE~ZWYCIĘS~TWO (szeoki uśmiech)!


### Gdy zabraknie prądu Rekinom

* **uid:** 211207-gdy-zabraknie-pradu-rekinom, _numer względny_: 6
* **daty:** 0111-08-26 - 0111-08-27
* **obecni:** Arkadia Verlen, Arnold Kłaczek, Daniel Terienak, Henryk Wkrąż, Hestia d'Rekiny, Karolina Terienak, Lorena Gwozdnik, Marysia Sowińska, Natalia Tessalon, Sensacjusz Diakon, Urszula Arienik

Streszczenie:

Ponieważ Amelia nie przekazała Marysi informacji o tym, że ta powinna przejąć obowiązki zarządcze to Marysia jako administratorka nie dostawała informacji. Przez to odcięło im prąd. Marysia przywróciła prąd podpisując (standardową) umowę z dystrybutorem podwierckim a Karolina uratowała ludzką część Dzielnicy Rekinów od pobicia przez wkurzonych magów. Próba obniżenia rachunków przez współpracę z mafią została przez dziewczyny zażegnana używając Arkadii.

Aktor w Opowieści:

* Dokonanie:
    * poinformowana przez Karo, że Natalia Tessalon chce wrobić Rekiny w ciągnięcie prądu od mafii. Zrobiła ostrą mowę roku. Gdy Natalia powiedziała, że nie wiedziała że to z mafii to Arkadia jej uwierzyła.
* Progresja:
    * jej burzliwe wystąpienie NAPRAWDĘ wzbudziło niezadowolenie mafii. Zwłaszcza po działaniach przeciwko Majkłapcowi.


### Waśń o ryby w Majkłapcu

* **uid:** 211127-waśń-o-ryby-w-majklapcu, _numer względny_: 5
* **daty:** 0111-08-15 - 0111-08-20
* **obecni:** Arkadia Verlen, Daniel Terienak, Genowefa Krecik, Iwan Zawtrak, Karolina Terienak, Ksenia Kirallen, Paweł Szprotka, Rafał Torszecki, Stella Amakirin

Streszczenie:

Daniel chciał rozwiązać problem w Majkłapcu, gdzie koty poraniły ryby. Okazało się, że to wina mafii, której właściciel kociarni nie chce płacić okupu. Karolina zebrała drużynę, uratowali zatrute wściekłością koty i zaatakowali siedzibę małego oddziałka mafii. Po zdobyciu dowodów (i ucieczce) przekazali temat Ksenii.

Aktor w Opowieści:

* Dokonanie:
    * gdy usłyszała, że może bić i pomóc komuś to dołączyła do Karo. Gdy Daniel przypadkiem odpalił cichy alarm, zaatakowała mafię FRONTALNIE - i Karo ją przechwyciła ścigaczem. Dobra dywersja. Potem przekazała Ksenii info o Stelli z mafii.


### Przybycie Rekina z Eterni

* **uid:** 210921-przybycie-rekina-z-eterni, _numer względny_: 4
* **daty:** 0111-07-16 - 0111-07-21
* **obecni:** Amelia Sowińska, Arkadia Verlen, Ernest Namertel, Jolanta Sowińska, Karol Pustak, Karolina Terienak, Lucjan Sowiński, Marysia Sowińska, Nataniel Morlan, Rafał Torszecki, Tomasz Tukan

Streszczenie:

Do Rekinów dołącza tien z Eterni. Marysia dowiaduje się, że to kwestia intryg jej kuzynki, Amelii, próbującej deeskalować problemy na linii Morlan - Jolanta. Dowiedziała się od Amelii, że to nie Amelia stała za morderstwami w Podwiercie, kogoś chroni. Oraz... że Amelia jest zakochana (Amelia nie wie że Marysia wie). Ale na Marysię spadła ochrona Ernesta. Więc... usunęła wszystkie zagrożenia o których pomyślała (Arkadia) i zaczęła działania mające poprawić reputację Eternianina, by Rekiny go przyjęły.

Aktor w Opowieści:

* Dokonanie:
    * uznana za zagrożenie dla Ernesta przez Marysię (słusznie). Tukan zakręcił, by uczestniczyła w terminuskich akcjach poza Podwiertem. Ona jest szczęśliwa, Tukan ma profity, Pustogor ma kompetentnego "agenta".
* Progresja:
    * Arkadia się przyda Pustogorowi i będzie zneutralizowana na 30 dni. Plus wyjdzie zadowolona, zdrowa, z podniesioną opinią w Pustogorze.


### Rekin wspiera mafię

* **uid:** 210713-rekin-wspiera-mafie, _numer względny_: 3
* **daty:** 0111-06-02 - 0111-06-05
* **obecni:** Julia Kardolin, Kacper Bankierz, Marysia Sowińska, Rafał Torszecki, Tomasz Tukan, Urszula Miłkowicz

Streszczenie:

Gabriel Ursus powiedział Marysi, że jakiś Rekin uczestniczy w przemycie nielegalnych rzeczy w Podwiercie, na terenie eksterytorialnym. I będzie rajd i przechwycą te Rekiny. Marysia dotarła do tego, że są tajne magazyny (chronione przez anomalię) pod kontrolą Kacpra Bankierza. Rozmowa z Kacprem nic nie dała, więc Marysia doprowadziła do uwolnienia Arkadii z więzienia i zrobiła fałszywe ślady - to Kacper stał za jej odurzeniem. Plus, Marysia odbudowała kontakt z Tukanem...

Aktor w Opowieści:

* Dokonanie:
    * AKTOR NIEOBECNY NA SESJI
* Progresja:
    * uwolniona z Kazamatów dzięki Marysi (o czym nie wie); święcie przekonana, że Kacper Bankierz jest odpowiedzialny za jej otrucie (dzięki Marysi ofc). Odporna na dowody.


### Verlenka na grzybkach

* **uid:** 210622-verlenka-na-grzybkach, _numer względny_: 2
* **daty:** 0111-05-26 - 0111-05-27
* **obecni:** Arkadia Verlen, Ignacy Myrczek, Julia Kardolin, Liliana Bankierz, Marek Samszar, Marysia Sowińska, Rafał Torszecki, Różewicz Diakon, Triana Porzecznik, Urszula Miłkowicz

Streszczenie:

Zespół AMZ składał projekt zaliczeniowy, gdy zaatakowała Arkadia Verlen na grzybkach ze swojego Lancera, uszkadzając projekt (robota). Julia wezwała Ulę (terminuskę) na pomoc, Ula unieszkodliwiła Marka (który podmienił Arkadię). Jako, że Julia i Triana były zajęte ratowaniem projektu, Marysia rzuciła się na znalezienie "kto atakuje Julię". Dotarła do niewinności Marka, dotarła do Arkadii i z pomocą Uli unieszkodliwiła Arkadię. Jedyne straty - ranna Ula i ranny Torszecki. Ale kto podał Arkadii te grzybki i jak?

Aktor w Opowieści:

* Dokonanie:
    * pod wpływem grzybków wpierw zestrzeliła robota kultywacyjnego a potem zapolowała na Ulę po komunikacie od Marysi. Mistrzyni noży i genialna kinetka. Assassin-build.
* Progresja:
    * straciła servar klasy Lancer. Ma ABSOLUTNEGO bana na jakąkolwiek broń. Nie wolno jej używać ŻADNEJ broni. Plus, tydzień prac społecznych.


### Skradziony kot Olgi

* **uid:** 210615-skradziony-kot-olgi, _numer względny_: 1
* **daty:** 0111-05-09 - 0111-05-11
* **obecni:** Arkadia Verlen, Julia Kardolin, Liliana Bankierz, Marek Samszar, Marysia Sowińska, Olga Myszeczka, Paweł Szprotka, Wiktor Satarail

Streszczenie:

Marysia i Julia mają imprezę karaoke w Lesie Trzęsawnym, ale Marek nie dotarł - walczy na magię z Lilianą. Julia puściła drony Triany, które wpadły w berserk. By Tymon się nie zainteresował tym co się dzieje w lesie, Marysia i Julia chciały rozwiązać problem - okazało się, że Marek ukradł somnibela Oldze Myszeczce i dał go Arkadii Verlen w prezencie. Marysia i Julia przekonały Arkadię do oddania kota, oddały go Oldze, po czym Arkadia zerwała z Markiem i go ciężko pobiła. Acz Paweł Szprotka (który próbował odzyskać kota od Marka i którego broniła Liliana) boi się o życie, bo niektóre Rekiny się na niego uwzięły za karę...

Aktor w Opowieści:

* Dokonanie:
    * Rekin. Dziewczyna Marka, który dał jej ukradzionego kota i rywalka (wróg?) Marysi Sowińskiej. Jak się dowiedziała o ukradzionym somnibelu, oddała go i ciężko pobiła Marka, zrywając z nim.
* Progresja:
    * zerwała z Markiem Samszarem i go solidnie pobiła za kradzież somnibela Oldze Myszeczce.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 7, @: 0112-02-18
    1. Primus    : 7, @: 0112-02-18
        1. Sektor Astoriański    : 7, @: 0112-02-18
            1. Astoria, Orbita    : 1, @: 0112-02-18
                1. Kontroler Pierwszy    : 1, @: 0112-02-18
            1. Astoria, Pierścień Zewnętrzny    : 1, @: 0112-02-18
                1. Poligon Stoczni Neotik    : 1, @: 0112-02-18
                1. Stocznia Neotik    : 1, @: 0112-02-18
            1. Astoria    : 6, @: 0111-09-30
                1. Sojusz Letejski    : 6, @: 0111-09-30
                    1. Szczeliniec    : 6, @: 0111-09-30
                        1. Powiat Pustogorski    : 6, @: 0111-09-30
                            1. Czarnopalec    : 1, @: 0111-05-11
                                1. Pusta Wieś    : 1, @: 0111-05-11
                            1. Czółenko    : 1, @: 0111-08-27
                                1. Generatory Keriltorn    : 1, @: 0111-08-27
                            1. Majkłapiec    : 1, @: 0111-08-20
                                1. Farma Krecik    : 1, @: 0111-08-20
                                1. Kociarnia Zawtrak    : 1, @: 0111-08-20
                                1. Wegefarma Myriad    : 1, @: 0111-08-20
                                1. Zakład Paprykarski Majkłapiec    : 1, @: 0111-08-20
                            1. Podwiert    : 5, @: 0111-09-30
                                1. Dzielnica Luksusu Rekinów    : 4, @: 0111-08-27
                                    1. Obrzeża Biedy    : 2, @: 0111-05-27
                                        1. Domy Ubóstwa    : 1, @: 0111-05-27
                                        1. Hotel Milord    : 1, @: 0111-05-11
                                    1. Sektor Brudu i Nudy    : 1, @: 0111-08-27
                                        1. Komputerownia    : 1, @: 0111-08-27
                                    1. Serce Luksusu    : 2, @: 0111-08-27
                                        1. Apartamentowce Elity    : 1, @: 0111-07-21
                                        1. Arena Amelii    : 1, @: 0111-08-27
                                1. Kompleks Korporacyjny    : 2, @: 0111-09-30
                                    1. Dystrybutor Prądu Ozitek    : 1, @: 0111-08-27
                                    1. Elektrownia Węglowa Szarpien    : 1, @: 0111-08-27
                                    1. Zakład Recyklingu Owczarek    : 1, @: 0111-09-30
                                1. Las Trzęsawny    : 2, @: 0111-05-27
                                    1. Schron TRZ-17    : 1, @: 0111-05-11
                            1. Pustogor    : 1, @: 0111-05-27
                                1. Rdzeń    : 1, @: 0111-05-27
                                    1. Barbakan    : 1, @: 0111-05-27
                                        1. Kazamaty    : 1, @: 0111-05-27
                            1. Zaczęstwo    : 2, @: 0111-05-27
                                1. Las Trzęsawny    : 1, @: 0111-05-27
                                1. Nieużytki Staszka    : 2, @: 0111-05-27

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Karolina Terienak    | 4 | ((210921-przybycie-rekina-z-eterni; 211127-waśń-o-ryby-w-majklapcu; 211207-gdy-zabraknie-pradu-rekinom; 220730-supersupertajny-plan-loreny)) |
| Marysia Sowińska     | 4 | ((210615-skradziony-kot-olgi; 210622-verlenka-na-grzybkach; 210921-przybycie-rekina-z-eterni; 211207-gdy-zabraknie-pradu-rekinom)) |
| Daniel Terienak      | 3 | ((211127-waśń-o-ryby-w-majklapcu; 211207-gdy-zabraknie-pradu-rekinom; 220730-supersupertajny-plan-loreny)) |
| Liliana Bankierz     | 3 | ((210615-skradziony-kot-olgi; 210622-verlenka-na-grzybkach; 220730-supersupertajny-plan-loreny)) |
| Rafał Torszecki      | 3 | ((210622-verlenka-na-grzybkach; 210921-przybycie-rekina-z-eterni; 211127-waśń-o-ryby-w-majklapcu)) |
| Julia Kardolin       | 2 | ((210615-skradziony-kot-olgi; 210622-verlenka-na-grzybkach)) |
| Marek Samszar        | 2 | ((210615-skradziony-kot-olgi; 210622-verlenka-na-grzybkach)) |
| Paweł Szprotka       | 2 | ((210615-skradziony-kot-olgi; 211127-waśń-o-ryby-w-majklapcu)) |
| Adam Szarjan         | 1 | ((211124-prototypowa-nereida-natalii)) |
| Amelia Sowińska      | 1 | ((210921-przybycie-rekina-z-eterni)) |
| Arianna Verlen       | 1 | ((211124-prototypowa-nereida-natalii)) |
| Arnold Kłaczek       | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Elena Verlen         | 1 | ((211124-prototypowa-nereida-natalii)) |
| Ernest Namertel      | 1 | ((210921-przybycie-rekina-z-eterni)) |
| Eustachy Korkoran    | 1 | ((211124-prototypowa-nereida-natalii)) |
| Genowefa Krecik      | 1 | ((211127-waśń-o-ryby-w-majklapcu)) |
| Henryk Wkrąż         | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Hestia d'Rekiny      | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Ignacy Myrczek       | 1 | ((210622-verlenka-na-grzybkach)) |
| Iwan Zawtrak         | 1 | ((211127-waśń-o-ryby-w-majklapcu)) |
| Izabela Zarantel     | 1 | ((211124-prototypowa-nereida-natalii)) |
| Jolanta Sowińska     | 1 | ((210921-przybycie-rekina-z-eterni)) |
| Karol Pustak         | 1 | ((210921-przybycie-rekina-z-eterni)) |
| Klaudia Stryk        | 1 | ((211124-prototypowa-nereida-natalii)) |
| Ksenia Kirallen      | 1 | ((211127-waśń-o-ryby-w-majklapcu)) |
| Leona Astrienko      | 1 | ((211124-prototypowa-nereida-natalii)) |
| Lorena Gwozdnik      | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Lucjan Sowiński      | 1 | ((210921-przybycie-rekina-z-eterni)) |
| Maria Naavas         | 1 | ((211124-prototypowa-nereida-natalii)) |
| Marsen Gwozdnik      | 1 | ((220730-supersupertajny-plan-loreny)) |
| Natalia Aradin       | 1 | ((211124-prototypowa-nereida-natalii)) |
| Natalia Tessalon     | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Nataniel Morlan      | 1 | ((210921-przybycie-rekina-z-eterni)) |
| Olga Myszeczka       | 1 | ((210615-skradziony-kot-olgi)) |
| OO Infernia          | 1 | ((211124-prototypowa-nereida-natalii)) |
| Roland Sowiński      | 1 | ((211124-prototypowa-nereida-natalii)) |
| Różewicz Diakon      | 1 | ((210622-verlenka-na-grzybkach)) |
| Sensacjusz Diakon    | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Stella Amakirin      | 1 | ((211127-waśń-o-ryby-w-majklapcu)) |
| Tomasz Tukan         | 1 | ((210921-przybycie-rekina-z-eterni)) |
| Triana Porzecznik    | 1 | ((210622-verlenka-na-grzybkach)) |
| Urszula Arienik      | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Urszula Miłkowicz    | 1 | ((210622-verlenka-na-grzybkach)) |
| Wawrzyn Rewemis      | 1 | ((211124-prototypowa-nereida-natalii)) |
| Wiktor Satarail      | 1 | ((210615-skradziony-kot-olgi)) |
| Władysław Owczarek   | 1 | ((220730-supersupertajny-plan-loreny)) |
| Żorż d'Namertel      | 1 | ((220730-supersupertajny-plan-loreny)) |
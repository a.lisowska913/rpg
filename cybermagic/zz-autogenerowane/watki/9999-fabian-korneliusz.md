# Fabian Korneliusz
## Identyfikator

Id: 9999-fabian-korneliusz

## Sekcja Opowieści

### Ziarno Kuratorów na Karnaxianie

* **uid:** 230530-ziarno-kuratorow-na-karnaxianie, _numer względny_: 5
* **daty:** 0109-10-06 - 0109-10-07
* **obecni:** Anastazy Termann, Fabian Korneliusz, Helmut Szczypacz, Klaudia Stryk, Kurator Sarkamair, Leo Mikirnik, Martyn Hiwasser, OO Serbinius, SC Karnaxian

Streszczenie:

Kurator Sarkamair rozstawił Ziarna - mikrofabrykatory atakujące TAI - i złowił salvager Karnaxian. Skonstruował Alexandrię i zaczął rekonstruować silniki by odlecieć do bazy o bliżej nieokreślonej lokalizacji. Serbinius zainteresował się Karnaxianem poza kursem i nie dał się zmylić optymistyczną Semlą i projekcją załogi. Gdy Serbinius odparł atak Kuratora (co było dość trudne), zrobili intruzję by uratować uwięzioną w Aleksandrii załogę. Niestety, nie udało im się - jedyne co mogli zrobić to zniszczyć Karnaxian i ostrzec wszystkich przed nową strategią Kuratorów, a zwłaszcza Sarkamaira.

Aktor w Opowieści:

* Dokonanie:
    * bardzo zależy mu by uratować wszystkich ludzi. Jak zawsze, słucha bardziej doświadczonych agentów Orbitera (Leo, Klaudia, Martyn). Zależy mu na uratowaniu jednostki przejętej przez Kuratorów do tego stopnia, że zagroził bezpieczeństwu Serbiniusa - dopiero jak Leo mu uzmysłowił, że jak Karnaxian odleci to zabierze _away party_ ze sobą, Fabian zmienił zdanie.
* Progresja:
    * niezadowolenie na Leo Mikirnika; jego zdaniem Leo nie dba o ludzi i idzie za daleko. Ma ranę psychiczną po tym jak nie uratował ludzi od Kuratora Sarkamaira i projektował to na Leo.


### Helmut i nieoczekiwana awaria Lancera

* **uid:** 230528-helmut-i-nieoczekiwana-awaria-lancera, _numer względny_: 4
* **daty:** 0109-09-23 - 0109-09-26
* **obecni:** Anastazy Termann, Fabian Korneliusz, Helmut Szczypacz, Klaudia Stryk, Martyn Hiwasser, Miranda Termann, Nikodem Dewiremicz, OO Serbinius

Streszczenie:

Helmut próbuje optymalizować substrat fabrykatorów na Serbiniusie, co niepokoi Klaudię. Tymczasem pojawia się seria niewielkich awarii na różnych jednostkach, Serbinius pomaga. Klaudia zauważa wzór, ale dopiero jak członek Serbiniusa ma awarię jetpacka znajduje dowód - to Anomalia Statystyczna, niewykrywalna na Primusie. Współpracując z teoretykiem z Orbitera mapują obszar tej Anomalii i wyciągają wszystkich z kłopotów.

Aktor w Opowieści:

* Dokonanie:
    * dowódca który mimo zmęczenia całej załogi nie ignoruje alarmu Semli na jednostce cywilnej (acz warknął na Klaudię by się spieszyła). Dba o swoją załogę, acz jeśli się da to się chce podlizać Mirandzie Termann i innym siłom wyższym.


### Rozszczepiona Persefona na Itorwienie

* **uid:** 230521-rozszczepiona-persefona-na-itorwienie, _numer względny_: 3
* **daty:** 0109-09-15 - 0109-09-17
* **obecni:** Emilia Ibris, Fabian Korneliusz, Helmut Szczypacz, Iskander Matorin, Karol Brinik, Klaudia Stryk, Martyn Hiwasser, Mojra Karstall, OO Itorwien, OO Serbinius, Tadeusz Arkaladis

Streszczenie:

Itorwien, 'pługośmieciarka' Orbitera miała problem z przemytnikami - ktoś otworzył przemycane narkotyki i cała jednostka była Skażona (lifesupport). Persefona została ograniczona przez psychotronika na pokładzie, więc nie mogła nic zrobić. Ciśnienie i uszkodzenie Persi doprowadziło do jej rozszczepienia. Serbinius uratował Itorwien, bez strat (poza winnym psychotronikiem zabitym przez rozszczepioną Persefonę).

Aktor w Opowieści:

* Dokonanie:
    * niecierpliwy, ale sensowny - co prawda nie dał Klaudii dość czasu na 100% danych, ale skutecznie przejął kontrolę nad Itorwienem z pomocą Klaudii i killware. Poproszony przez Klaudię, dał pozytywną rekomendację noktiańskiemu advancerowi z Itorwiena.


### Młodziaki na Savarańskim statku handlowym

* **uid:** 220925-mlodziaki-na-savaranskim-statku-handlowym, _numer względny_: 2
* **daty:** 0109-05-24 - 0109-05-25
* **obecni:** Dawid Aximar, Fabian Korneliusz, Klaudia Stryk, Klaudiusz Zanęcik, Martyn Hiwasser, OO Serbinius, Perdius Aximar, SC Hektor 17

Streszczenie:

Młody Orbiterowiec wraz z dziewczyną zatrudnili się na savarańskiej jednostce SC Hektor 17. Ojciec owego Orbiterowca jest mentorem Fabiana Korneliusza, więc Zespół udał się wyciągać tą dwójkę. Klaudia doszła do tego, że to neonoktiańska jednostka i współpracując z kapitanem tej jednostki wyciągnęła młodych oraz ukryła niegroźne naruszenia na 'Hektorze 13' które Fabian by wykorzystał.

Aktor w Opowieści:

* Dokonanie:
    * syn jego mentora wpakował się na statek cywilny z dziewczyną i 'zniknął'. Fabian lokalizuje (Klaudią) jednostkę cywilną 'Hektor 13' i robi wszystko, by wyciągnąć te osoby. Jest skłonny nawet zadziałać niezgodnie z celami Orbitera. I nie zauważył że 'Hektor 13' to jednostka neonoktiańska.


### Chory piesek na statku Luxuritias

* **uid:** 220716-chory-piesek-na-statku-luxuritias, _numer względny_: 1
* **daty:** 0109-05-05 - 0109-05-07
* **obecni:** Achellor Santorinus, Bożena Kokorobant, Fabian Korneliusz, Henryk Urkon, Klaudia Stryk, Martyn Hiwasser, OO Serbinius, Roberto Santorinus, SL Rajasee Bagh, Teodor Margrabarz

Streszczenie:

Młody panicz dał sygnał alarmowy, że plaga na pokładzie statku Luxuritias opuszczającego Valentinę. Serbinius poleciał zbadać sprawę. Na miejscu okazało się, że chłopak chciał by jego psa zobaczył weterynarz. A JEDNAK NIE. Faktycznie jest plaga - z Neikatis. Zespół wszedł w sojusz z szefem ochrony statku i plaga została powstrzymana a statek odholowany do Atropos na fumigację (Śmiechowica to typ grzyba).

Aktor w Opowieści:

* Dokonanie:
    * młody (24) porucznik Orbitera, dowódca Martyna i Klaudii na OO Serbinius; niekoniecznie utrzymuje język na wodzy. Petty one, cieszy się z nieszczęścia Luxuritias i z przyjemnością chowa się za procedurami. Ale współpracuje z Luxuritias by zatrzymać plagę Śmiechowicy na ich jednostce.
* Progresja:
    * ma jakieś problemy z Valentiną z uwagi na swoją gorącą głowę i ego; nie dostaje stamtąd wsparcia czy pomocy.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 5, @: 0109-10-07
    1. Primus    : 5, @: 0109-10-07
        1. Sektor Astoriański    : 5, @: 0109-10-07
            1. Astoria, Pierścień Zewnętrzny    : 2, @: 0109-09-26

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Klaudia Stryk        | 5 | ((220716-chory-piesek-na-statku-luxuritias; 220925-mlodziaki-na-savaranskim-statku-handlowym; 230521-rozszczepiona-persefona-na-itorwienie; 230528-helmut-i-nieoczekiwana-awaria-lancera; 230530-ziarno-kuratorow-na-karnaxianie)) |
| Martyn Hiwasser      | 5 | ((220716-chory-piesek-na-statku-luxuritias; 220925-mlodziaki-na-savaranskim-statku-handlowym; 230521-rozszczepiona-persefona-na-itorwienie; 230528-helmut-i-nieoczekiwana-awaria-lancera; 230530-ziarno-kuratorow-na-karnaxianie)) |
| OO Serbinius         | 5 | ((220716-chory-piesek-na-statku-luxuritias; 220925-mlodziaki-na-savaranskim-statku-handlowym; 230521-rozszczepiona-persefona-na-itorwienie; 230528-helmut-i-nieoczekiwana-awaria-lancera; 230530-ziarno-kuratorow-na-karnaxianie)) |
| Helmut Szczypacz     | 3 | ((230521-rozszczepiona-persefona-na-itorwienie; 230528-helmut-i-nieoczekiwana-awaria-lancera; 230530-ziarno-kuratorow-na-karnaxianie)) |
| Anastazy Termann     | 2 | ((230528-helmut-i-nieoczekiwana-awaria-lancera; 230530-ziarno-kuratorow-na-karnaxianie)) |
| Achellor Santorinus  | 1 | ((220716-chory-piesek-na-statku-luxuritias)) |
| Bożena Kokorobant    | 1 | ((220716-chory-piesek-na-statku-luxuritias)) |
| Dawid Aximar         | 1 | ((220925-mlodziaki-na-savaranskim-statku-handlowym)) |
| Emilia Ibris         | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Henryk Urkon         | 1 | ((220716-chory-piesek-na-statku-luxuritias)) |
| Iskander Matorin     | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Karol Brinik         | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Klaudiusz Zanęcik    | 1 | ((220925-mlodziaki-na-savaranskim-statku-handlowym)) |
| Kurator Sarkamair    | 1 | ((230530-ziarno-kuratorow-na-karnaxianie)) |
| Leo Mikirnik         | 1 | ((230530-ziarno-kuratorow-na-karnaxianie)) |
| Miranda Termann      | 1 | ((230528-helmut-i-nieoczekiwana-awaria-lancera)) |
| Mojra Karstall       | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Nikodem Dewiremicz   | 1 | ((230528-helmut-i-nieoczekiwana-awaria-lancera)) |
| OO Itorwien          | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Perdius Aximar       | 1 | ((220925-mlodziaki-na-savaranskim-statku-handlowym)) |
| Roberto Santorinus   | 1 | ((220716-chory-piesek-na-statku-luxuritias)) |
| SC Hektor 17         | 1 | ((220925-mlodziaki-na-savaranskim-statku-handlowym)) |
| SC Karnaxian         | 1 | ((230530-ziarno-kuratorow-na-karnaxianie)) |
| SL Rajasee Bagh      | 1 | ((220716-chory-piesek-na-statku-luxuritias)) |
| Tadeusz Arkaladis    | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Teodor Margrabarz    | 1 | ((220716-chory-piesek-na-statku-luxuritias)) |
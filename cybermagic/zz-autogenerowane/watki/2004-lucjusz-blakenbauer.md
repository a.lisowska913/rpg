# Lucjusz Blakenbauer
## Identyfikator

Id: 2004-lucjusz-blakenbauer

## Sekcja Opowieści

### Kraloth w parku Janor

* **uid:** 201025-kraloth-w-parku-janor, _numer względny_: 15
* **daty:** 0110-10-25 - 0110-10-27
* **obecni:** Hestia d'Janor, Lucjusz Blakenbauer, Maciej Oczorniak, Minerwa Metalia, Pięknotka Diakon

Streszczenie:

Dużo ciężkiej pracy Lucjusza i Minerwy, ale Pięknotka wróciła do "normy", acz potrzebuje regularnych transfuzji. Pięknotka oddała Minerwie Erwina; sama myśli jak zamieszkać z Lucjuszem i nie udawać jego narzeczonej. Chciała skończyć sprawę w parku rozrywki; z Minerwą odkryły że za wszystkim stoi cholerny kraloth. Porwały terminusa przejętego przez kralotha by on wezwał SOS z Pustogoru.

Aktor w Opowieści:

* Dokonanie:
    * naprawił Pięknotkę, używając matrycy Blakenbauer do kontroli sił Saitaera. Zaprosił Pięknotkę do Rezydencji; tu będzie bezpieczna.


### Rozbrojenie bomby w Kalbarku

* **uid:** 200222-rozbrojenie-bomby-w-kalbarku, _numer względny_: 14
* **daty:** 0110-07-23 - 0110-07-28
* **obecni:** Aleksandra Szklarska, Ataienne, Bartłomiej Małczarek, Diana Tevalier, Lucjusz Blakenbauer, Mateusz Kardamacz, Pięknotka Diakon, Tymon Grubosz

Streszczenie:

Kalbark okazał się jedną wielką bombą. Liberatis, silnie uzbrojeni. Kardamacz, kontroluje całe miasto. Terminusi, skażeni kralotycznie. I Chevaleresse na którą wszystko spadnie jeśli przyjdzie wsparcie. Pięknotka rozmontowała tą bombę, ściągnęła Ataienne (oficjalnie) i Zespół dał radę rozproszyć Liberatis, wycofać Chevaleresse i odzyskać kontrolę nad Barbakanem. Pięknotka zebrała wsparcie i na polu bitwy został na serio tylko Kardamacz.

Aktor w Opowieści:

* Dokonanie:
    * znalazł chorobę Chevaleresse; nie lubi kralotycznych środków jak to co jej podali. Pomoże Pięknotce w ratowaniu terminusów z Kalbarka.


### Strażniczka przez łzy

* **uid:** 200226-strazniczka-przez-lzy, _numer względny_: 13
* **daty:** 0110-07-25 - 0110-07-28
* **obecni:** Alicja Kiermacz, Alina Anakonda, Crystal d'Corieris, Darek Ampieczak, Eliza Farnorz, Kastor Miczokan, Lucjusz Blakenbauer, Rafał Muczor

Streszczenie:

Po zniknięciu Kajrata i falowaniu energii Esuriit w Czółenku, Pustogor przekształcił Alicję Kiermacz w Strażniczkę Esuriit wbrew Lucjuszowi Blakenbauerowi. Podczas transformacji Alicja prawie umarła i uległa ostrej cyborgizacji. Ale ogólnie się udało - choć mało kto wie, że Alicja żyje.

Aktor w Opowieści:

* Dokonanie:
    * stanął przeciwko TAI Crystal, Alinie i Pustogorowi w ochronie Alicji przed losem Strażniczki Esuriit. I przegrał.
* Progresja:
    * traci wiarę w to, że Pustogor chce dobrze. Wpada w kłopoty przez wejście w kolizję z agentami specjalnymi Pustogoru. Traci też stanowisko elitarnego lekarza.


### Wygrany kontrakt

* **uid:** 200311-wygrany-kontrakt, _numer względny_: 12
* **daty:** 0110-07-16 - 0110-07-19
* **obecni:** Amanda Kajrat, Dagmara Doberman, Ernest Kajrat, Liliana Bankierz, Lucjusz Blakenbauer, Minerwa Metalia, Ziemowit Zięba

Streszczenie:

Ernest Kajrat jest w stanie śmiertelnym; Lucjusz i Minerwa trzymają go przy życiu. Tymczasem firma dostarczająca jedzenie "Zygmunt Zając" wygrała przetarg (na który nie startowała) odnośnie jedzenia dla zixionizowanej Amandy Kajrat w rękach Grzymościa. Współpraca Blakenbauera i "Zająca" doprowadza do uwolnienia Amandy i kupienia szansy Ernestowi...

Aktor w Opowieści:

* Dokonanie:
    * próbuje utrzymać przy życiu Ernesta Kajrata i opracowuje z magami z "Zająca" jak wyciągnąć zixionizowaną Amandę Kajrat z rąk Grzymościa jedzeniem


### Polowanie na Pięknotkę

* **uid:** 190901-polowanie-na-pieknotke, _numer względny_: 11
* **daty:** 0110-06-17 - 0110-06-20
* **obecni:** Alan Bartozol, Diana Tevalier, Józef Małmałaz, Lucjusz Blakenbauer, Mariusz Trzewń, Minerwa Metalia, Pięknotka Diakon

Streszczenie:

Noktiański zabójca chciał pozyskać Cienia; pokonał Pięknotkę, ale Alan go zestrzelił. Potem zabójca złapał Chevaleresse i korzystając z niej jako zakładniczki wysłał Alana do szpitala. Pięknotka doszła do tego, jak się nazywa z pomocą Minerwy i Cienia.

Aktor w Opowieści:

* Dokonanie:
    * doprowadził Pięknotkę do porządku i pozwolił Minerwie czarować w szpitalu. Bardzo potem tego żałował (odkażanie sali).


### Szczur który chroni

* **uid:** 190505-szczur-ktory-chroni, _numer względny_: 10
* **daty:** 0110-04-18 - 0110-04-20
* **obecni:** Adela Kirys, Alan Bartozol, Ernest Kajrat, Kasjan Czerwoczłek, Krystian Namałłek, Lucjusz Blakenbauer, Oliwia Namałłek, Pięknotka Diakon, Wiktor Satarail

Streszczenie:

Zaczęło się od Skażenia proszkiem kralotycznym Nieużytków Staszka. Potem Pięknotka współpracując z mafią doszła do tego, że Adela i terminus stoją za tą sprawą. Potem Pięknotka uratowała Adelę i terminusa wprowadzając szturmowych terminusów Pustogoru na mafię. Gdy "Cień wyrwał się spod kontroli", Alan ją strzelił z działa strumieniowego. I tak skończyło się rumakowanie na tydzień - ale wszyscy są bezpieczni.

Aktor w Opowieści:

* Dokonanie:
    * skażenie kralotyczne było tak silne, że aż musiał przyjść na Nieużytki Staszka robić ratunkowe sytuacje.


### Sabotaż szeptów Elizy

* **uid:** 190429-sabotaz-szeptow-elizy, _numer względny_: 9
* **daty:** 0110-04-06 - 0110-04-09
* **obecni:** Alan Bartozol, Aleksander Rugczuk, Diana Tevalier, Eliza Ira, Erwin Galilien, Karla Mrozik, Lucjusz Blakenbauer, Minerwa Metalia, Olaf Zuchwały, Pięknotka Diakon

Streszczenie:

Chevaleresse wpakowała się w "kult Elizy" grając z ludźmi online; powiedziała o tym Pięknotce. Pięknotka odkryła, że Eliza rozprzestrzenia swoje wpływy przez kryształy w formie biżuterii; wraz z Alanem, Erwinem i (nadal ranną) Minerwą zaprojektowali mechanizm uszkadzający sieć krystaliczną Elizy. Udało im się kupić jakiś miesiąc czasu, aż Orbiter i Pustogor znajdą lepsze rozwiązanie. Sama Chevaleresse ma się nie spotykać z takimi tam. Ale może z nimi grać online.

Aktor w Opowieści:

* Dokonanie:
    * overridowany z decyzją o utrzymanie Minerwy w szpitalu; opieprzył Karlę jak nikt za to, że wróciła bardziej ranna


### Uwięzienie Saitaera

* **uid:** 181230-uwiezienie-saitaera, _numer względny_: 8
* **daty:** 0109-12-06 - 0109-12-08
* **obecni:** Karla Mrozik, Kreacjusz Diakon, Lucjusz Blakenbauer, Minerwa Metalia, Pięknotka Diakon

Streszczenie:

Pięknotka wróciła do Pustogoru, będąc po drodze zaatakowana przez latającego pnączoszpona. Odkryła, że Saitaer poważnie zmienił już Minerwę. Zmartwiła się tym, jak bardzo ta Minerwa różni się od jej przyjaciółki. Ta sprawa jest ponad siły Pięknotki - poszła do Karli, przełożonej Pustogoru. Tam dowiedziała się, że Saitaer to broń i że być może to on stoi za problemami Trzęsawiska Zjawosztup. Pustogor pojmał Saitaera i przejął wskrzeszoną Minerwę do leczenia.

Aktor w Opowieści:

* Dokonanie:
    * wpierw upewnił się, że Pięknotka nie jest pod wpływem żadnego bóstwa a potem zajął się Minerwą. Choć ona przekracza jego możliwości.


### Odklątwianie Ateny

* **uid:** 181112-odklatwianie-ateny, _numer względny_: 7
* **daty:** 0109-10-22 - 0109-10-25
* **obecni:** Amadeusz Sowiński, Atena Sowińska, Cezary Zwierz, Lucjusz Blakenbauer, Minerwa Diakon, Pięknotka Diakon, Saitaer

Streszczenie:

Atenie się nie poprawia a jej echo atakuje Pięknotkę. By zrozumieć co się dzieje, Pięknotka rozmawia z "terrorformem" (Saitaerem) i dowiaduje się o krwawym klątwożycie. Następnie używając wpływów Adama Szarjana wyrywa Atenę spod opieki lekarza i transportuje ją do Cieniaszczytu, by tam można było jej pomóc. By jej pomóc i odkryć napastnika potrzeba jest moc kralotyczna - Atena się nie zgadza, lecz Pięknotka ją przekonała. Atena ulega rekonstrukcji kralotycznej a Amadeusz ma mniej więcej namiar na twórcę klątwożyta.

Aktor w Opowieści:

* Dokonanie:
    * lekarz dbający twardo o swoich pacjentów. Starł się z Pięknotką; dopiero wpływy i złoto Adama Szarjana zmusiły go do oddania Ateny jako pacjentki.


### Protomag z Trzęsawisk

* **uid:** 180817-protomag-z-trzesawisk, _numer względny_: 6
* **daty:** 0109-09-07 - 0109-09-09
* **obecni:** Alan Bartozol, Atena Sowińska, Erwin Galilien, Felicja Melitniek, Lucjusz Blakenbauer, Miedwied Zajcew, Pięknotka Diakon

Streszczenie:

Protomag skrzywdził kierowcę z lokalnej mafii Zajcewów. Miedwied i Pięknotka poszli szukać Felicji - protomaga na której eksperymentowali - na Trzęsawisku Zjawosztup. Tam napotkali na terminusów Czerwonych Myszy którzy też chcieli Felicję. Współpraca Zespołu z Ateną Sowińską doprowadziła do przekazania Felicji im i Miedwied został jej opiekunem. Niestety, Trzęsawisko w wyniku pojawienia się Felicji się rozpaliło - jest gorące i będzie emitować Skażeńce...

Aktor w Opowieści:

* Dokonanie:
    * współpracujący z mafią Miedwieda lekarz wybitnej klasy. Przede wszystkim skupiony na potrzebach pacjentów, nie da nikomu zrobić krzywdy.


### Szmuglowanie Antonelli

* **uid:** 210813-szmuglowanie-antonelli, _numer względny_: 5
* **daty:** 0108-12-07 - 0108-12-19
* **obecni:** Antonella Temaris, Bruno Baran, Cień Brighton, Flawia Blakenbauer, Jolanta Sowińska, Lucjusz Blakenbauer, SC Fecundatis, Tomasz Sowiński

Streszczenie:

Antonella Temaris stała się problemem politycznym na linii Sowińscy - Nataniel Morlan. By nie została oddana, Tomasz, Jolanta i Flawia weszli we współpracę z Cieniem Brightonem, przemytnikiem. Przemycili Flawię na orbitę (Brighton skłonił Jolantę, by ta poleciała z nimi!), po czym zgubili ewentualny pościg na Valentinie. A drugą linią Flawia przekonała Lucjusza, by ten przygotował szpital terminuski w Pustogorze na zmianę Wzoru Antonelli, by ją naprawić...

Aktor w Opowieści:

* Dokonanie:
    * już objął szpital terminuski w Pustogorze; przekonał Pustogor do tego by pomóc Antonelli (zasoby Sowińskich + kontakty z orbitą poza Orbiterem).
* Progresja:
    * ma dostęp do krwi Antonelli Temaris, z czego wynika - Wzór pasujący do Nataniela Morlana.


### Lustrzane odbicie Eleny

* **uid:** 210324-lustrzane-odbicie-eleny, _numer względny_: 4
* **daty:** 0097-07-05 - 0097-07-08
* **obecni:** Arianna Verlen, Elena Verlen, Lucjusz Blakenbauer, Michał Perikas, Przemysław Czapurt, Romeo Verlen, Viorika Verlen

Streszczenie:

Elena, chcąc utrzymać kontrolę nad swoją mocą zaczęła brać wiktoriatę. To plus przepuszczanie magii przez lustra zniszczyło jej wzór i ją uszkodziło. Elena stała się wampirem - pożera energetycznie żołnierzy by chronić teren i wszystkich cywilów. Plus, pragnie zemsty na Blakenbauerach i na Perikasach. W swoim Skażeniu powołała Lustrzanego Golema. Viorika i Arianna dały radę wymanewrować Elenę, odkryć, że to ona stoi za Lustrzanym Golemem i z pomocą sierżanta Czapurta w Poniewierzy dały radę Elenę unieszkodliwić, by ją docelowo naprawić...

Aktor w Opowieści:

* Dokonanie:
    * chłopak Vioriki; był pod ręką gdy Lustrzany Golem zranił Michała Perikasa. Poza standardowym sarkazmem, udało mu się utrzymać Michała przy życiu i zmienić go w Ropucha.


### Wiktoriata

* **uid:** 210306-wiktoriata, _numer względny_: 3
* **daty:** 0093-10-21 - 0093-10-30
* **obecni:** Apollo Verlen, Lucjusz Blakenbauer, Przemysław Czapurt, Viorika Verlen, Wiktor Blakenbauer

Streszczenie:

Po przegranym pojedynku na oddziały z Apollem, Viorika trafia do papierologii w Poniewierzy. Tam dowiaduje się, że Blakenbauerowie chyba porywają ludzi. Współpracując z Lucjuszem Blakenbauerem, odkrywa prawdę - to psychoza wywołana przez narkotyki bojowe sprzedawane Verlenom przez Blakenbauerów. Przypadkowo z Lucjuszem rozbija siatkę szpiegowską Blakenbauerów na terenie Verlenów. Plus, zalicza początek romansu z Lucjuszem.

Aktor w Opowieści:

* Dokonanie:
    * dotrzymuje słowa; pomaga Viorice uratować zaginionych. Rozbija (przypadkowo) siatkę szpiegowską Blakenbauerów na terenie Verlenów. Identyfikuje wiktoriatę, detoksyfikuje psychotycznych żołnierzy Verlenów i ratuje kogo się da.
* Progresja:
    * przypadkowo wykrył tajną operację sił specjalnych Blakenbauerów w Trójkącie Chaosu i ją SPALIŁ. Współpracując z Vioriką VERLEN. Powiedział jej o wiłach. Szefostwo jest z niego MEGA niezadowolone. Podpadł. Bardzo.
    * Viorika i ogólnie Verlenowie zauważyli, że jeśli może to pomoże - zwłaszcza ludziom i słabszym od siebie. To jego zdecydowana słabość. Tajnym agentem nie będzie, ale dzięki temu można mu zaufać bardziej niż przeciętnemu Blakenbauerowi.
    * jakieś pół roku po tym wydarzeniu został parą z Vioriką Verlen.


### Umierająca farma biovatów

* **uid:** 210302-umierajaca-farma-biovatow, _numer względny_: 2
* **daty:** 0093-06-19 - 0093-06-21
* **obecni:** Frezja Amanit, Lucjusz Blakenbauer, Selena Walecznik, Viorika Verlen

Streszczenie:

Lucjusz Blakenbauer nie chciał dopuścić do śmierci jednej biovatowej farmy. Potrzebował vitae. Zaatakował dyskretnie ród Verlen, pobierając vitae z żołnierzy w Wremłowie. Viorika była akurat na wakacjach, zobaczyła anomalię w sentisieci i odparła ruchy Lucjusza; używając kombinacji sentisieci i taktyki zmusiła go do kapitulacji. Może zabrać to co zdobył, ale musi przekazać zapas języcznika i jej wisi coś małego.

Aktor w Opowieści:

* Dokonanie:
    * chciał pomóc ludziom z biovatów by nie zostali zniszczeni, więc zinfiltrował Verlen syfonując z odpoczywających żołnierzy _vitae_. Znaleziony i pokonany przez Viorikę.


### Sentiobsesja

* **uid:** 210224-sentiobsesja, _numer względny_: 1
* **daty:** 0092-08-03 - 0092-08-06
* **obecni:** Apollo Verlen, Arianna Verlen, Lucjusz Blakenbauer, Milena Blakenbauer, Mścigrom Verlen, Przemysław Czapurt, Viorika Verlen

Streszczenie:

Apollo Verlen, sam w Holdzie Bastion był jedynym sentitienem. Gdy uderzyły siły Mileny Blakenbauer, wściekł się i wpadł w sentiobsesję - chce atakować Blakenbauerów. Na szczęście na miejsce przybyły Arianna i Viorika, które rozwiązały sprawę, odkryły sekret Apolla (3 kochanki) i ściągnęły go pułapką, po czym... pokonały i przywróciły. Aha, potem odzyskały brakujący oddział kosztem śpiewu i tańca Apolla przed Mileną Blakenbauer.

Aktor w Opowieści:

* Dokonanie:
    * 17 lat, UB. Próbuje stabilizować Milenkę; taki trochę nerd. Przekonał Milenkę, że jednak najlepszą opcją jest oddanie oddziału rodowi Verlen. Z góry patrzy na standardowe bioformy, mogą być ulepszone.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 15, @: 0110-10-27
    1. Primus    : 15, @: 0110-10-27
        1. Sektor Astoriański    : 15, @: 0110-10-27
            1. Astoria    : 15, @: 0110-10-27
                1. Sojusz Letejski    : 15, @: 0110-10-27
                    1. Aurum    : 5, @: 0108-12-19
                        1. Imperium Sowińskich    : 1, @: 0108-12-19
                            1. Krystalitium    : 1, @: 0108-12-19
                                1. Klub Eksplozja    : 1, @: 0108-12-19
                                1. Pałac Świateł    : 1, @: 0108-12-19
                        1. Sentipustkowie Pierwotne    : 1, @: 0092-08-06
                        1. Świat Dżungli    : 1, @: 0092-08-06
                            1. Ostropnącz    : 1, @: 0092-08-06
                        1. Verlenland    : 4, @: 0097-07-08
                            1. Hold Bastion    : 1, @: 0092-08-06
                                1. Barbakan    : 1, @: 0092-08-06
                                1. Karcer    : 1, @: 0092-08-06
                                1. Karczma Szczur    : 1, @: 0092-08-06
                            1. Mikrast    : 1, @: 0097-07-08
                            1. Poniewierz    : 2, @: 0097-07-08
                                1. Archiwum Poniewierskie    : 1, @: 0093-10-30
                                1. Arena    : 1, @: 0097-07-08
                                1. Dom Uciech Wszelakich    : 1, @: 0093-10-30
                                1. Garnizon    : 1, @: 0093-10-30
                                1. Magazyny Anomalii    : 1, @: 0093-10-30
                                1. Zamek Gościnny    : 1, @: 0097-07-08
                            1. Skałkowa Myśl    : 1, @: 0097-07-08
                            1. Trójkąt Chaosu    : 1, @: 0093-10-30
                            1. Wremłowo    : 1, @: 0093-06-21
                                1. rachityczny lasek z jaskiniami    : 1, @: 0093-06-21
                    1. Przelotyk    : 1, @: 0109-10-25
                        1. Przelotyk Wschodni    : 1, @: 0109-10-25
                            1. Cieniaszczyt    : 1, @: 0109-10-25
                                1. Kompleks Nukleon    : 1, @: 0109-10-25
                                1. Pałac Szkarłatnego Światła    : 1, @: 0109-10-25
                    1. Szczeliniec    : 11, @: 0110-10-27
                        1. Powiat Jastrzębski    : 2, @: 0110-10-27
                            1. Kalbark    : 1, @: 0110-07-28
                                1. Autoklub Piękna    : 1, @: 0110-07-28
                                1. Mini Barbakan    : 1, @: 0110-07-28
                            1. Praszalek, okolice    : 1, @: 0110-10-27
                                1. Lasek Janor    : 1, @: 0110-10-27
                                1. Park rozrywki Janor    : 1, @: 0110-10-27
                        1. Powiat Pustogorski    : 10, @: 0110-10-27
                            1. Podwiert    : 2, @: 0110-04-20
                                1. Bastion Pustogoru    : 1, @: 0110-04-20
                                1. Kopalnia Terposzy    : 1, @: 0109-12-08
                                1. Magazyny sprzętu ciężkiego    : 1, @: 0109-12-08
                                1. Odlewnia    : 1, @: 0110-04-20
                                1. Sensoplex    : 1, @: 0110-04-20
                            1. Pustogor, okolice    : 2, @: 0110-10-27
                                1. Rezydencja Blakenbauerów    : 2, @: 0110-10-27
                            1. Pustogor    : 7, @: 0110-07-28
                                1. Eksterior    : 2, @: 0110-04-09
                                    1. Miasteczko    : 2, @: 0110-04-09
                                1. Interior    : 1, @: 0110-04-09
                                    1. Bunkry Barbakanu    : 1, @: 0110-04-09
                                    1. Laboratorium Senetis    : 1, @: 0110-04-09
                                1. Rdzeń    : 7, @: 0110-07-28
                                    1. Barbakan    : 1, @: 0109-12-08
                                    1. Szpital Terminuski    : 7, @: 0110-07-28
                            1. Zaczęstwo    : 3, @: 0110-07-19
                                1. Nieużytki Staszka    : 3, @: 0110-07-19
                                1. Osiedle Ptasie    : 1, @: 0110-06-20
                        1. Trzęsawisko Zjawosztup    : 2, @: 0110-04-20
            1. Stacja Valentina    : 1, @: 0108-12-19

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Pięknotka Diakon     | 8 | ((180817-protomag-z-trzesawisk; 181112-odklatwianie-ateny; 181230-uwiezienie-saitaera; 190429-sabotaz-szeptow-elizy; 190505-szczur-ktory-chroni; 190901-polowanie-na-pieknotke; 200222-rozbrojenie-bomby-w-kalbarku; 201025-kraloth-w-parku-janor)) |
| Minerwa Metalia      | 5 | ((181230-uwiezienie-saitaera; 190429-sabotaz-szeptow-elizy; 190901-polowanie-na-pieknotke; 200311-wygrany-kontrakt; 201025-kraloth-w-parku-janor)) |
| Alan Bartozol        | 4 | ((180817-protomag-z-trzesawisk; 190429-sabotaz-szeptow-elizy; 190505-szczur-ktory-chroni; 190901-polowanie-na-pieknotke)) |
| Viorika Verlen       | 4 | ((210224-sentiobsesja; 210302-umierajaca-farma-biovatow; 210306-wiktoriata; 210324-lustrzane-odbicie-eleny)) |
| Diana Tevalier       | 3 | ((190429-sabotaz-szeptow-elizy; 190901-polowanie-na-pieknotke; 200222-rozbrojenie-bomby-w-kalbarku)) |
| Przemysław Czapurt   | 3 | ((210224-sentiobsesja; 210306-wiktoriata; 210324-lustrzane-odbicie-eleny)) |
| Apollo Verlen        | 2 | ((210224-sentiobsesja; 210306-wiktoriata)) |
| Arianna Verlen       | 2 | ((210224-sentiobsesja; 210324-lustrzane-odbicie-eleny)) |
| Atena Sowińska       | 2 | ((180817-protomag-z-trzesawisk; 181112-odklatwianie-ateny)) |
| Ernest Kajrat        | 2 | ((190505-szczur-ktory-chroni; 200311-wygrany-kontrakt)) |
| Erwin Galilien       | 2 | ((180817-protomag-z-trzesawisk; 190429-sabotaz-szeptow-elizy)) |
| Karla Mrozik         | 2 | ((181230-uwiezienie-saitaera; 190429-sabotaz-szeptow-elizy)) |
| Adela Kirys          | 1 | ((190505-szczur-ktory-chroni)) |
| Aleksander Rugczuk   | 1 | ((190429-sabotaz-szeptow-elizy)) |
| Aleksandra Szklarska | 1 | ((200222-rozbrojenie-bomby-w-kalbarku)) |
| Alicja Kiermacz      | 1 | ((200226-strazniczka-przez-lzy)) |
| Alina Anakonda       | 1 | ((200226-strazniczka-przez-lzy)) |
| Amadeusz Sowiński    | 1 | ((181112-odklatwianie-ateny)) |
| Amanda Kajrat        | 1 | ((200311-wygrany-kontrakt)) |
| Antonella Temaris    | 1 | ((210813-szmuglowanie-antonelli)) |
| Ataienne             | 1 | ((200222-rozbrojenie-bomby-w-kalbarku)) |
| Bartłomiej Małczarek | 1 | ((200222-rozbrojenie-bomby-w-kalbarku)) |
| Bruno Baran          | 1 | ((210813-szmuglowanie-antonelli)) |
| Cezary Zwierz        | 1 | ((181112-odklatwianie-ateny)) |
| Cień Brighton        | 1 | ((210813-szmuglowanie-antonelli)) |
| Crystal d'Corieris   | 1 | ((200226-strazniczka-przez-lzy)) |
| Dagmara Doberman     | 1 | ((200311-wygrany-kontrakt)) |
| Darek Ampieczak      | 1 | ((200226-strazniczka-przez-lzy)) |
| Elena Verlen         | 1 | ((210324-lustrzane-odbicie-eleny)) |
| Eliza Farnorz        | 1 | ((200226-strazniczka-przez-lzy)) |
| Eliza Ira            | 1 | ((190429-sabotaz-szeptow-elizy)) |
| Felicja Melitniek    | 1 | ((180817-protomag-z-trzesawisk)) |
| Flawia Blakenbauer   | 1 | ((210813-szmuglowanie-antonelli)) |
| Frezja Amanit        | 1 | ((210302-umierajaca-farma-biovatow)) |
| Hestia d'Janor       | 1 | ((201025-kraloth-w-parku-janor)) |
| Jolanta Sowińska     | 1 | ((210813-szmuglowanie-antonelli)) |
| Józef Małmałaz       | 1 | ((190901-polowanie-na-pieknotke)) |
| Kasjan Czerwoczłek   | 1 | ((190505-szczur-ktory-chroni)) |
| Kastor Miczokan      | 1 | ((200226-strazniczka-przez-lzy)) |
| Kreacjusz Diakon     | 1 | ((181230-uwiezienie-saitaera)) |
| Krystian Namałłek    | 1 | ((190505-szczur-ktory-chroni)) |
| Liliana Bankierz     | 1 | ((200311-wygrany-kontrakt)) |
| Maciej Oczorniak     | 1 | ((201025-kraloth-w-parku-janor)) |
| Mariusz Trzewń       | 1 | ((190901-polowanie-na-pieknotke)) |
| Mateusz Kardamacz    | 1 | ((200222-rozbrojenie-bomby-w-kalbarku)) |
| Michał Perikas       | 1 | ((210324-lustrzane-odbicie-eleny)) |
| Miedwied Zajcew      | 1 | ((180817-protomag-z-trzesawisk)) |
| Milena Blakenbauer   | 1 | ((210224-sentiobsesja)) |
| Minerwa Diakon       | 1 | ((181112-odklatwianie-ateny)) |
| Mścigrom Verlen      | 1 | ((210224-sentiobsesja)) |
| Olaf Zuchwały        | 1 | ((190429-sabotaz-szeptow-elizy)) |
| Oliwia Namałłek      | 1 | ((190505-szczur-ktory-chroni)) |
| Rafał Muczor         | 1 | ((200226-strazniczka-przez-lzy)) |
| Romeo Verlen         | 1 | ((210324-lustrzane-odbicie-eleny)) |
| Saitaer              | 1 | ((181112-odklatwianie-ateny)) |
| SC Fecundatis        | 1 | ((210813-szmuglowanie-antonelli)) |
| Selena Walecznik     | 1 | ((210302-umierajaca-farma-biovatow)) |
| Tomasz Sowiński      | 1 | ((210813-szmuglowanie-antonelli)) |
| Tymon Grubosz        | 1 | ((200222-rozbrojenie-bomby-w-kalbarku)) |
| Wiktor Blakenbauer   | 1 | ((210306-wiktoriata)) |
| Wiktor Satarail      | 1 | ((190505-szczur-ktory-chroni)) |
| Ziemowit Zięba       | 1 | ((200311-wygrany-kontrakt)) |
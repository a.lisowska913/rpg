# Ignacy Myrczek
## Identyfikator

Id: 9999-ignacy-myrczek

## Sekcja Opowieści

### Odbudowa według Justyniana

* **uid:** 211123-odbudowa-wedlug-justyniana, _numer względny_: 14
* **daty:** 0111-08-07 - 0111-08-16
* **obecni:** Azalia Sernat d'Namertel, Daniel Terienak, Ernest Namertel, Ignacy Myrczek, Justynian Diakon, Karol Pustak, Karolina Terienak, Marysia Sowińska, Rafał Torszecki

Streszczenie:

Marysia się tymczasowo wprowadziła do Ernesta, by go trochę uspokoić i przekonać do Rekinów. Tymczasem Justynian Diakon przejął kontrolę operacyjną i zaczął odbudowywać Dzielnicę Rekinów po ruinie jaką zrobił tam Wiktor Satarail. Marysia skontaktowała się z Justynianem i przekazała mu plan Azalii d'Namertel jako swój, by odzyskać kontrolę i pozycję. Justynian zaakceptował jej plan i powiedział jej, że Torszeckiego biją. Do tego Marysię odwiedził Pustak jako herold Myrczka - prosi, by Marysia skontaktowała go z Sabiną Kazitan, miłością Myrczka...

Aktor w Opowieści:

* Dokonanie:
    * zebrał się na odwagę i poprosił Marysię (przez Karola Pustaka) o skontaktowanie go z jego miłością - Sabiną Kazitan.


### Koszt ratowania Torszeckiego

* **uid:** 211026-koszt-ratowania-torszeckiego, _numer względny_: 13
* **daty:** 0111-07-28 - 0111-08-01
* **obecni:** Ignacy Myrczek, Ksenia Kirallen, Marysia Sowińska, Olga Myszeczka, Paweł Szprotka, Rafał Torszecki, Sensacjusz Diakon, Teresa Mieralit, Wiktor Satarail

Streszczenie:

By ratować Torszeckiego, Marysia wchodzi w sojusz z Wiktorem Satarailem. On dał jej podskórnego robaka którego dostanie Torszecki, by uzasadnić dziwne zachowanie. Wiktor "dostanie swoją zapłatę" od "kogoś winnego". Marysia przekonała też Sensacjusza, że ona x Torszecki. Zaczęły się też pojawiać takie plotki...

Aktor w Opowieści:

* Dokonanie:
    * okazuje się, że dorabia sobie jako kucharz i jest współlokatorem Pawła Szprotki.


### Verlenka na grzybkach

* **uid:** 210622-verlenka-na-grzybkach, _numer względny_: 12
* **daty:** 0111-05-26 - 0111-05-27
* **obecni:** Arkadia Verlen, Ignacy Myrczek, Julia Kardolin, Liliana Bankierz, Marek Samszar, Marysia Sowińska, Rafał Torszecki, Różewicz Diakon, Triana Porzecznik, Urszula Miłkowicz

Streszczenie:

Zespół AMZ składał projekt zaliczeniowy, gdy zaatakowała Arkadia Verlen na grzybkach ze swojego Lancera, uszkadzając projekt (robota). Julia wezwała Ulę (terminuskę) na pomoc, Ula unieszkodliwiła Marka (który podmienił Arkadię). Jako, że Julia i Triana były zajęte ratowaniem projektu, Marysia rzuciła się na znalezienie "kto atakuje Julię". Dotarła do niewinności Marka, dotarła do Arkadii i z pomocą Uli unieszkodliwiła Arkadię. Jedyne straty - ranna Ula i ranny Torszecki. Ale kto podał Arkadii te grzybki i jak?

Aktor w Opowieści:

* Dokonanie:
    * jego grzybki były użyte do wpłynięcia na Arkadię; pracuje nad robotem kultywacyjnym i jest bezwartościowy pod ostrzałem.


### Grzybopreza

* **uid:** 210323-grzybopreza, _numer względny_: 11
* **daty:** 0111-04-18 - 0111-04-19
* **obecni:** Ignacy Myrczek, Julia Kardolin, Kacper Bankierz, Marysia Sowińska, Napoleon Bankierz, Triana Porzecznik, Urszula Miłkowicz

Streszczenie:

Trianie "uciekł" źle skonfigurowany techno-pies ratujący ludzi. Marysia Sowińska (Rekin) porwała Myrczka na pomysł Julii, zrobili super-trufle i Triana z Julią dotarły do psa i go unieczynniły. Tymczasem Marysia i Myrczek wymanewrowały Ulę (uczennicę terminusa) i sprowokowali ją do wezwania wsparcia przeciw ad-hocowej czystej grzyboprezie Rekinów. Nawet Pakt o tym napisał.

Aktor w Opowieści:

* Dokonanie:
    * porwany przez Marysię Sowińską ścigaczem z akademika w nocy, pomógł Trianie zrobić super-śmierdzące wybuchające trufle i grzyby do grzybo-imprezy. Potem poszedł na grzyboprezę i nawet ZATAŃCZYŁ Z MARYSIĄ SOWIŃSKĄ! Ogólnie, super dzień.
* Progresja:
    * Urszula Miłkowicz, uczennica terminusa ma z nim kosę.


### Pojedynek: Akademia - Rekiny

* **uid:** 201013-pojedynek-akademia-rekiny, _numer względny_: 10
* **daty:** 0110-10-10 - 0110-10-18
* **obecni:** Aleksander Bemucik, Ignacy Myrczek, Julia Kardolin, Justynian Diakon, Kacper Bankierz, Liliana Bankierz, Napoleon Bankierz, Remor 340D, Robert Pakiszon, Stella Armadion, Teresa Mieralit

Streszczenie:

Kolejna ustawka między Rekinami a uczniami AMZ mogłaby się skończyć bardzo źle, więc grupa uczniów spróbowała wprowadzić sposób rozwiązywania konfliktu przez turnieje między Rekinami i AMZ. Nie tylko im się to udało - użycie starego ścigacza wojskowego Remor 340D i echo emocji wojen noktiańskich sprawiło, że Rekiny się unormowały i zainwestowano w budowę Toru Wyścigowego Pamięci w Podwiercie.

Aktor w Opowieści:

* Dokonanie:
    * Liliana używa tego, że Sabina z nim "zerwała" jako przykładu tego że trzeba tępić Rekiny.


### Dezinhibitor dla Sabiny

* **uid:** 201006-dezinhibitor-dla-sabiny, _numer względny_: 9
* **daty:** 0110-10-03 - 0110-10-05
* **obecni:** Aranea Diakon, Gabriel Ursus, Ignacy Myrczek, Justynian Diakon, Karolina Erenit, Laura Tesinik, Lorena Gwozdnik, Napoleon Bankierz, Rafał Kumczek, Sabina Kazitan

Streszczenie:

Zanim Sabina wróci do Aurum, Rekiny Paladyni chcieli ją przyskrzynić - podać jej dezinhibitor w towarzystwie Myrczka i to nagrać, z nadzieją, że się ją zamknie. Gabriel i Laura wykryli intrygę i zatrzymali ją, nie dopuszczając do dezinhibicji Sabiny. O dziwo, tien Kazitan chciała Myrczka CHRONIĆ.

Aktor w Opowieści:

* Dokonanie:
    * nie chce zrezygnować z miłości do Sabiny. Zachęcony przez Kumczka do strasznego planu - eliksir "tortur" na seksbocie (na serio: dezinhibitor na Lorenie XD). Sabina z nim "zerwała", wparowała tam, opieprzyła go i powiedziała by nie robił głupot.
* Progresja:
    * dostawca środków psychoaktywnych do "Latających Rekinów" z Aurum. Lubiany przez Rekiny i studentów Akademii Magii.


### Tajna baza Orbitera?

* **uid:** 200510-tajna-baza-orbitera, _numer względny_: 8
* **daty:** 0110-09-03 - 0110-09-07
* **obecni:** Alina Anakonda, Artur Kołczond, Gabriel Ursus, Ignacy Myrczek, Kallista Exolon, Laura Tesinik, Mariusz Trzewń, Natalia Tessalon, Pięknotka Diakon, Sabina Kazitan, Talarand d'Irrydius, Teresa Mieralit, Tymon Grubosz

Streszczenie:

Natalia Tessalon chce przetransportować rannego brata do Aurum, ale manewr zablokowała Laura (terminuska-prawniczka) na prośbę Pięknotki. Natalia i Sabina się ścięły, przez co ucierpiał Myrczek a Sabina trafiła do aresztu. Teresa Mieralit zapewni, że Myrczek nie ma głowy do Sabiny. Pięknotka z Tymonem poszukali kto ciężko zmiażdżył Gabriela; udało się im znaleźć tajną bazę Orbitera w której jest Emulatorka, która się do tego przyznała.

Aktor w Opowieści:

* Dokonanie:
    * stanął w obronie Sabiny przed Natalią, za co Sabina walnęła mu szpicrutą w twarz. Wyraźnie się w Sabinie podkochuje i chce ją "uratować od jej złej natury".


### Wojna Trzęsawiska

* **uid:** 200418-wojna-trzesawiska, _numer względny_: 7
* **daty:** 0110-08-12 - 0110-08-17
* **obecni:** Gabriel Ursus, Ignacy Myrczek, Karla Mrozik, Pięknotka Diakon, Sabina Kazitan, Wiktor Satarail

Streszczenie:

Trzęsawisko odpowiedziało na atak tworząc nową bioformę na bazie Sabiny Kazitan; zaczęło przyciągać ludzi do siebie. Z uwagi na wysokie niebezpieczeństwo Karla chciała zbombardować Trzęsawisko z orbity, jak kiedyś. Pięknotka zrobiła kanał negocjacyjny Pustogor - Wiktor Satarail; Wiktor pomoże Pięknotce uspokoić Trzęsawisko, ale nie będzie bombardowania z orbity. Plus, Pustogor pomoże mu sformować nową bioformę...

Aktor w Opowieści:

* Dokonanie:
    * przestał się bać Sabiny Kazitan; gdy ona leży z nim w szpitalu to czyta jej książki i opowiada o grzybach.


### Nawoływanie Trzęsawiska

* **uid:** 200417-nawolywanie-trzesawiska, _numer względny_: 6
* **daty:** 0110-08-08 - 0110-08-10
* **obecni:** Gabriel Ursus, Ignacy Myrczek, Napoleon Bankierz, Olaf Zuchwały, Pięknotka Diakon, Sabina Kazitan, Strażniczka Alair

Streszczenie:

Trzęsawisko Zjawosztup zostało Skrzywdzone przez ostatnie wydarzenia. Zaczęło wabić Sabinę i Ignacego by ich pożreć. Szczęśliwie, Pięknotka zdążyła dotrzeć do Sabiny (która wabiona szukała Ignacego) i uratowała ich przed Trzęsawiskiem. Dodatkowo Pięknotka poznała sekret Sabiny i zdobyła nad nią absolutną kontrolę.

Aktor w Opowieści:

* Dokonanie:
    * szukał kobiety, której grozi coś złego (zew Trzęsawiska). Skończył w Szpitalu Terminuskim w Pustogorze, na sali z Sabiną Kazitan. Przerażony.


### Arystokraci na Trzęsawisku

* **uid:** 200414-arystokraci-na-trzesawisku, _numer względny_: 5
* **daty:** 0110-07-31 - 0110-08-01
* **obecni:** Franciszek Leszczowik, Ignacy Myrczek, Mariusz Trzewń, Pięknotka Diakon, Sabina Kazitan, Strażniczka Alair, Talia Aegis

Streszczenie:

Franciszek z Aurum dostosował Sabinę do swoich upodobań jako seksbombę. Sabina zdecydowała się go zniszczyć - doprowadziła go do Wysysacza Esuriit i wskazała, że na Trzęsawisku może zdobyć coś co mu się przyda. Franciszek stracił część świty na Trzęsawisku; gdyby nie Pięknotka, zginąłby tam. A tak tylko skończył z traumą a Sabinie się upiekło - roboty społeczne na 2 tygodnie.

Aktor w Opowieści:

* Dokonanie:
    * dał się skusić na zarobek by pomóc znaleźć grzyby na Trzęsawisku; niestety, skusił się na Sabinę Kazitan a skończył ratowany przez Pięknotkę.


### Test z etyki

* **uid:** 200326-test-z-etyki, _numer względny_: 4
* **daty:** 0110-07-25 - 0110-07-27
* **obecni:** Aniela Kark, Berenika Wrążowiec, Ignacy Myrczek, Liliana Bankierz, Napoleon Bankierz, Teresa Mieralit

Streszczenie:

Liliana eskalowała swoją krucjatę przeciw "Zygmuntowi Zającowi", włączając do działania Ignacego Myrczka. Teresa Mieralit, nauczycielka m.in. etyki, zrobiła z tego przypadku egzamin dla dwóch uczennic kończących już swoją naukę w Akademii Magii. Skończyło się na stworzeniu anomalnego impa robiącego zdjęć śpiącej Lilianie i jeszcze większym podgrzaniu atmosfery. Ale - konserwy ZZ faktycznie posiadają dziwne substraty.

Aktor w Opowieści:

* Dokonanie:
    * AMZ; zgnębiony przez Lilianę, próbuje pomóc jej testując konserwy "Zygmunta Zająca". Bardzo pasywna rola - ona każe, on się boi i jej słucha.


### Korupcja z arystokratki

* **uid:** 191112-korupcja-z-arystokratki, _numer względny_: 3
* **daty:** 0110-06-30 - 0110-07-01
* **obecni:** Ataienne, Ignacy Myrczek, Mariusz Trzewń, Minerwa Metalia, Persefona d'Jastrząbiec, Pięknotka Diakon, Sabina Kazitan, Tadeusz Sklerzec

Streszczenie:

Po wydarzeniach z Grzymościem oczy Pięknotki padły na Jastrząbiec - tam dzieją się dziwne rzeczy z narkotykami. Okazało się, że to Ignacy Myrczek próbuje rozpaczliwie udowodnić arystokratce z Aurum, że nie zrobił jej krzywdy świadomie a pod wpływem narkotyków. Pięknotka wpadła (z pomocą Ataienne) i zrobiła porządek - ukarała burmistrza, Myrczka, arystokratkę i w sumie wszystkich. Ma smak popiołu w ustach, tu nie było nikogo sensownego...

Aktor w Opowieści:

* Dokonanie:
    * nerd od grzybów. Robił rozpaczliwe eksperymenty z narkotykami, by udowodnić arystokratce, że nie zrobił jej krzywdy - to były narkotyki. Pięknotka dała mu wpiernicz.


### Terminuska czy kosmetyczka?

* **uid:** 181027-terminuska-czy-kosmetyczka, _numer względny_: 2
* **daty:** 0109-10-07 - 0109-10-11
* **obecni:** Adela Kirys, Arnulf Poważny, Brygida Maczkowik, Ignacy Myrczek, Minerwa Metalia, Pięknotka Diakon

Streszczenie:

Jednoczesny atak na reputację Ateny (przez cybergrzyby) oraz wielki konkurs kosmetyczny w Pustogorze wpłynęły stosunkowo źle na sytuację Pięknotki. Nie rozdwoiła się, ale obie rzeczy udało jej się rozwiązać - acz jest wycieńczona. Odkryła konspirację wymierzoną przeciwko Atenie - ale nie wie kto i gdzie próbuje Atenę usunąć.

Aktor w Opowieści:

* Dokonanie:
    * grzybomag, który wplątał się w polityczny atak na Atenę z chęci pomocy Brygidzie oraz z miłości do grzybów. Już więcej nie będzie tak eksperymentował.


### Dwa tygodnie szkoły

* **uid:** 180929-dwa-tygodnie-szkoly, _numer względny_: 1
* **daty:** 0109-09-17 - 0109-09-19
* **obecni:** Adela Kirys, Arnulf Poważny, Atena Sowińska, Erwin Galilien, Felicja Melitniek, Ignacy Myrczek, Miedwied Zajcew, Napoleon Bankierz, Pięknotka Diakon, Roland Grzymość

Streszczenie:

Felicja uczy się w szkole magów, ale nie ma przyjaciół i przez to zachowuje się chaotycznie. Erwin Galilien wpakował się w kłopoty i przegrał power suit "Nutkę" w Kasynie; ma przynieść niebezpieczne grzyby. Uczniowie ze szkoły magów też chodzą do tego Kasyna. Połączone siły Zespołu i Ateny wyplątały Erwina z kłopotów i utrudniły Kasynu zwabianie młodych uczniów ze szkoły magów.

Aktor w Opowieści:

* Dokonanie:
    * czarodziej który marzy o grzybach. Felicja podłożyła mu pod poduszkę kaktusa bo myślała że on jej zrobił numer pierwszy. Na końcu - przeprosiła.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 14, @: 0111-08-16
    1. Primus    : 14, @: 0111-08-16
        1. Sektor Astoriański    : 14, @: 0111-08-16
            1. Astoria    : 14, @: 0111-08-16
                1. Sojusz Letejski    : 14, @: 0111-08-16
                    1. Szczeliniec    : 14, @: 0111-08-16
                        1. Powiat Jastrzębski    : 2, @: 0110-10-05
                            1. Jastrząbiec, okolice    : 1, @: 0110-10-05
                                1. Blokhaus Widmo    : 1, @: 0110-10-05
                            1. Jastrząbiec    : 1, @: 0110-07-01
                                1. Hotel Stacja Kosmiczna    : 1, @: 0110-07-01
                                1. Ratusz    : 1, @: 0110-07-01
                        1. Powiat Pustogorski    : 13, @: 0111-08-16
                            1. Czarnopalec    : 1, @: 0111-08-01
                                1. Pusta Wieś    : 1, @: 0111-08-01
                            1. Czemerta, okolice    : 1, @: 0110-09-07
                                1. Baza Irrydius    : 1, @: 0110-09-07
                                1. Fortifarma Irrydia    : 1, @: 0110-09-07
                                1. Studnia Irrydiańska    : 1, @: 0110-09-07
                            1. Czemerta    : 1, @: 0110-08-17
                            1. Podwiert    : 5, @: 0111-08-16
                                1. Dzielnica Luksusu Rekinów    : 3, @: 0111-08-16
                                    1. Fortyfikacje Rolanda    : 1, @: 0111-08-16
                                    1. Obrzeża Biedy    : 2, @: 0111-08-16
                                        1. Domy Ubóstwa    : 2, @: 0111-08-16
                                        1. Hotel Milord    : 1, @: 0111-08-16
                                        1. Stadion Lotników    : 1, @: 0111-08-16
                                        1. Stajnia Rumaków    : 1, @: 0111-08-16
                                    1. Sektor Brudu i Nudy    : 1, @: 0111-08-16
                                        1. Komputerownia    : 1, @: 0111-08-16
                                        1. Konwerter Magielektryczny    : 1, @: 0111-08-16
                                        1. Magitrownia Pogardy    : 1, @: 0111-08-16
                                        1. Skrytki Czereśniaka    : 1, @: 0111-08-16
                                    1. Serce Luksusu    : 2, @: 0111-08-16
                                        1. Apartamentowce Elity    : 1, @: 0111-08-16
                                        1. Arena Amelii    : 1, @: 0111-08-16
                                        1. Fontanna Królewska    : 1, @: 0111-08-16
                                        1. Kawiarenka Relaks    : 1, @: 0111-08-16
                                        1. Lecznica Rannej Rybki    : 2, @: 0111-08-16
                                1. Las Trzęsawny    : 1, @: 0111-05-27
                                1. Sensoplex    : 1, @: 0110-10-18
                                1. Tor Wyścigowy Pamięci    : 1, @: 0110-10-18
                            1. Pustogor    : 4, @: 0111-05-27
                                1. Arena Szalonego Króla    : 1, @: 0109-10-11
                                1. Eksterior    : 2, @: 0110-09-07
                                    1. Miasteczko    : 2, @: 0110-09-07
                                        1. Knajpa Górska Szalupa    : 2, @: 0110-09-07
                                    1. Zamek Weteranów    : 1, @: 0110-08-10
                                1. Laboratorium Senetis    : 1, @: 0109-10-11
                                1. Rdzeń    : 3, @: 0111-05-27
                                    1. Barbakan    : 1, @: 0111-05-27
                                        1. Kazamaty    : 1, @: 0111-05-27
                                    1. Szpital Terminuski    : 2, @: 0110-09-07
                            1. Zaczęstwo    : 10, @: 0111-08-01
                                1. Akademia Magii, kampus    : 8, @: 0111-08-01
                                    1. Akademik    : 2, @: 0111-08-01
                                    1. Arena Treningowa    : 2, @: 0110-10-05
                                    1. Artefaktorium    : 1, @: 0110-10-18
                                    1. Audytorium    : 1, @: 0110-10-18
                                    1. Budynek Centralny    : 2, @: 0110-07-27
                                        1. Skrzydło Loris    : 1, @: 0110-07-27
                                1. Biurowiec Gorland    : 1, @: 0111-04-19
                                1. Cyberszkoła    : 1, @: 0109-10-11
                                1. Dzielnica Kwiecista    : 1, @: 0111-04-19
                                    1. Rezydencja Porzeczników    : 1, @: 0111-04-19
                                        1. Podziemne Laboratorium    : 1, @: 0111-04-19
                                1. Kasyno Marzeń    : 1, @: 0109-09-19
                                1. Las Trzęsawny    : 1, @: 0111-05-27
                                1. Nieużytki Staszka    : 5, @: 0111-05-27
                                1. Park Centralny    : 1, @: 0111-04-19
                                    1. Jezioro Gęsie    : 1, @: 0111-04-19
                        1. Trzęsawisko Zjawosztup    : 3, @: 0110-08-17
                            1. Laboratorium W Drzewie    : 1, @: 0110-08-17

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Pięknotka Diakon     | 7 | ((180929-dwa-tygodnie-szkoly; 181027-terminuska-czy-kosmetyczka; 191112-korupcja-z-arystokratki; 200414-arystokraci-na-trzesawisku; 200417-nawolywanie-trzesawiska; 200418-wojna-trzesawiska; 200510-tajna-baza-orbitera)) |
| Napoleon Bankierz    | 6 | ((180929-dwa-tygodnie-szkoly; 200326-test-z-etyki; 200417-nawolywanie-trzesawiska; 201006-dezinhibitor-dla-sabiny; 201013-pojedynek-akademia-rekiny; 210323-grzybopreza)) |
| Sabina Kazitan       | 6 | ((191112-korupcja-z-arystokratki; 200414-arystokraci-na-trzesawisku; 200417-nawolywanie-trzesawiska; 200418-wojna-trzesawiska; 200510-tajna-baza-orbitera; 201006-dezinhibitor-dla-sabiny)) |
| Gabriel Ursus        | 4 | ((200417-nawolywanie-trzesawiska; 200418-wojna-trzesawiska; 200510-tajna-baza-orbitera; 201006-dezinhibitor-dla-sabiny)) |
| Marysia Sowińska     | 4 | ((210323-grzybopreza; 210622-verlenka-na-grzybkach; 211026-koszt-ratowania-torszeckiego; 211123-odbudowa-wedlug-justyniana)) |
| Teresa Mieralit      | 4 | ((200326-test-z-etyki; 200510-tajna-baza-orbitera; 201013-pojedynek-akademia-rekiny; 211026-koszt-ratowania-torszeckiego)) |
| Julia Kardolin       | 3 | ((201013-pojedynek-akademia-rekiny; 210323-grzybopreza; 210622-verlenka-na-grzybkach)) |
| Justynian Diakon     | 3 | ((201006-dezinhibitor-dla-sabiny; 201013-pojedynek-akademia-rekiny; 211123-odbudowa-wedlug-justyniana)) |
| Liliana Bankierz     | 3 | ((200326-test-z-etyki; 201013-pojedynek-akademia-rekiny; 210622-verlenka-na-grzybkach)) |
| Mariusz Trzewń       | 3 | ((191112-korupcja-z-arystokratki; 200414-arystokraci-na-trzesawisku; 200510-tajna-baza-orbitera)) |
| Rafał Torszecki      | 3 | ((210622-verlenka-na-grzybkach; 211026-koszt-ratowania-torszeckiego; 211123-odbudowa-wedlug-justyniana)) |
| Adela Kirys          | 2 | ((180929-dwa-tygodnie-szkoly; 181027-terminuska-czy-kosmetyczka)) |
| Arnulf Poważny       | 2 | ((180929-dwa-tygodnie-szkoly; 181027-terminuska-czy-kosmetyczka)) |
| Kacper Bankierz      | 2 | ((201013-pojedynek-akademia-rekiny; 210323-grzybopreza)) |
| Laura Tesinik        | 2 | ((200510-tajna-baza-orbitera; 201006-dezinhibitor-dla-sabiny)) |
| Minerwa Metalia      | 2 | ((181027-terminuska-czy-kosmetyczka; 191112-korupcja-z-arystokratki)) |
| Strażniczka Alair    | 2 | ((200414-arystokraci-na-trzesawisku; 200417-nawolywanie-trzesawiska)) |
| Triana Porzecznik    | 2 | ((210323-grzybopreza; 210622-verlenka-na-grzybkach)) |
| Urszula Miłkowicz    | 2 | ((210323-grzybopreza; 210622-verlenka-na-grzybkach)) |
| Wiktor Satarail      | 2 | ((200418-wojna-trzesawiska; 211026-koszt-ratowania-torszeckiego)) |
| Aleksander Bemucik   | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Alina Anakonda       | 1 | ((200510-tajna-baza-orbitera)) |
| Aniela Kark          | 1 | ((200326-test-z-etyki)) |
| Aranea Diakon        | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Arkadia Verlen       | 1 | ((210622-verlenka-na-grzybkach)) |
| Artur Kołczond       | 1 | ((200510-tajna-baza-orbitera)) |
| Ataienne             | 1 | ((191112-korupcja-z-arystokratki)) |
| Atena Sowińska       | 1 | ((180929-dwa-tygodnie-szkoly)) |
| Azalia Sernat d'Namertel | 1 | ((211123-odbudowa-wedlug-justyniana)) |
| Berenika Wrążowiec   | 1 | ((200326-test-z-etyki)) |
| Brygida Maczkowik    | 1 | ((181027-terminuska-czy-kosmetyczka)) |
| Daniel Terienak      | 1 | ((211123-odbudowa-wedlug-justyniana)) |
| Ernest Namertel      | 1 | ((211123-odbudowa-wedlug-justyniana)) |
| Erwin Galilien       | 1 | ((180929-dwa-tygodnie-szkoly)) |
| Felicja Melitniek    | 1 | ((180929-dwa-tygodnie-szkoly)) |
| Franciszek Leszczowik | 1 | ((200414-arystokraci-na-trzesawisku)) |
| Kallista Exolon      | 1 | ((200510-tajna-baza-orbitera)) |
| Karla Mrozik         | 1 | ((200418-wojna-trzesawiska)) |
| Karol Pustak         | 1 | ((211123-odbudowa-wedlug-justyniana)) |
| Karolina Erenit      | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Karolina Terienak    | 1 | ((211123-odbudowa-wedlug-justyniana)) |
| Ksenia Kirallen      | 1 | ((211026-koszt-ratowania-torszeckiego)) |
| Lorena Gwozdnik      | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Marek Samszar        | 1 | ((210622-verlenka-na-grzybkach)) |
| Miedwied Zajcew      | 1 | ((180929-dwa-tygodnie-szkoly)) |
| Natalia Tessalon     | 1 | ((200510-tajna-baza-orbitera)) |
| Olaf Zuchwały        | 1 | ((200417-nawolywanie-trzesawiska)) |
| Olga Myszeczka       | 1 | ((211026-koszt-ratowania-torszeckiego)) |
| Paweł Szprotka       | 1 | ((211026-koszt-ratowania-torszeckiego)) |
| Persefona d'Jastrząbiec | 1 | ((191112-korupcja-z-arystokratki)) |
| Rafał Kumczek        | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Remor 340D           | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Robert Pakiszon      | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Roland Grzymość      | 1 | ((180929-dwa-tygodnie-szkoly)) |
| Różewicz Diakon      | 1 | ((210622-verlenka-na-grzybkach)) |
| Sensacjusz Diakon    | 1 | ((211026-koszt-ratowania-torszeckiego)) |
| Stella Armadion      | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Tadeusz Sklerzec     | 1 | ((191112-korupcja-z-arystokratki)) |
| Talarand d'Irrydius  | 1 | ((200510-tajna-baza-orbitera)) |
| Talia Aegis          | 1 | ((200414-arystokraci-na-trzesawisku)) |
| Tymon Grubosz        | 1 | ((200510-tajna-baza-orbitera)) |
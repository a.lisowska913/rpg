# Napoleon Bankierz
## Identyfikator

Id: 9999-napoleon-bankierz

## Sekcja Opowieści

### Akt, o którym Marysia nie wie

* **uid:** 211228-akt-o-ktorym-marysia-nie-wie, _numer względny_: 9
* **daty:** 0111-09-05 - 0111-09-08
* **obecni:** Daniel Terienak, Ernest Namertel, Karolina Terienak, Keira Amarco d'Namertel, Liliana Bankierz, Lorena Gwozdnik, Marysia Sowińska, Napoleon Bankierz, Rafał Torszecki

Streszczenie:

Torszecki ostrzegł Marysię, że jej akt krąży w Zaczęstwie. Ona to olała. Ale jak Ernest zaczął na niego polować to Marysia się zainteresowała. Zdecydowała się aktem odbić Ernesta Amelii. Zaflirtowała z Ernestem, obiecała mu akt specjalnie dla niego i że odda mu oryginał tego. Karo pokonała Napoleona, znalazła link do aktu który ostatecznie zdobył Torszecki. Daniel dla Karo zbił Torszeckiego i odzyskał akt. Marysia zaflirtowała z Ernestem ;-). Torszecki, który chciał dobrze i chronił Marysię skończył w szpitalu.

Aktor w Opowieści:

* Dokonanie:
    * zmierzył się z Karo na arenie i przegrał ostro, ale Karo podniosła go reputacyjnie. Powiedział jej o akcie Marysi i o tym, że ów należy do Liliany. Należy do miłośników piękna.


### Grzybopreza

* **uid:** 210323-grzybopreza, _numer względny_: 8
* **daty:** 0111-04-18 - 0111-04-19
* **obecni:** Ignacy Myrczek, Julia Kardolin, Kacper Bankierz, Marysia Sowińska, Napoleon Bankierz, Triana Porzecznik, Urszula Miłkowicz

Streszczenie:

Trianie "uciekł" źle skonfigurowany techno-pies ratujący ludzi. Marysia Sowińska (Rekin) porwała Myrczka na pomysł Julii, zrobili super-trufle i Triana z Julią dotarły do psa i go unieczynniły. Tymczasem Marysia i Myrczek wymanewrowały Ulę (uczennicę terminusa) i sprowokowali ją do wezwania wsparcia przeciw ad-hocowej czystej grzyboprezie Rekinów. Nawet Pakt o tym napisał.

Aktor w Opowieści:

* Dokonanie:
    * próbował uratować porwanego przez Marysię Myrczka; niestety, Marysia dużo lepiej nawiguje ścigaczem niż Napoleon. Po dostaniu wiązką śluzu skończył w jeziorze Zaczęstwa. Wezwał Ulę (terminuskę) na swoje miejsce.


### Pojedynek: Akademia - Rekiny

* **uid:** 201013-pojedynek-akademia-rekiny, _numer względny_: 7
* **daty:** 0110-10-10 - 0110-10-18
* **obecni:** Aleksander Bemucik, Ignacy Myrczek, Julia Kardolin, Justynian Diakon, Kacper Bankierz, Liliana Bankierz, Napoleon Bankierz, Remor 340D, Robert Pakiszon, Stella Armadion, Teresa Mieralit

Streszczenie:

Kolejna ustawka między Rekinami a uczniami AMZ mogłaby się skończyć bardzo źle, więc grupa uczniów spróbowała wprowadzić sposób rozwiązywania konfliktu przez turnieje między Rekinami i AMZ. Nie tylko im się to udało - użycie starego ścigacza wojskowego Remor 340D i echo emocji wojen noktiańskich sprawiło, że Rekiny się unormowały i zainwestowano w budowę Toru Wyścigowego Pamięci w Podwiercie.

Aktor w Opowieści:

* Dokonanie:
    * podczas ustawki ostrzegł nauczycieli o efemerydzie; miłośnik historii i wojskowości, tu: ŚWIETNY pilot ścigacza który trafił na podium walcząc z Rekinami.
* Progresja:
    * nie ma sprzężenia z sentisiecią Bankierz, ale był uczony na arystokratę Aurum.


### Dezinhibitor dla Sabiny

* **uid:** 201006-dezinhibitor-dla-sabiny, _numer względny_: 6
* **daty:** 0110-10-03 - 0110-10-05
* **obecni:** Aranea Diakon, Gabriel Ursus, Ignacy Myrczek, Justynian Diakon, Karolina Erenit, Laura Tesinik, Lorena Gwozdnik, Napoleon Bankierz, Rafał Kumczek, Sabina Kazitan

Streszczenie:

Zanim Sabina wróci do Aurum, Rekiny Paladyni chcieli ją przyskrzynić - podać jej dezinhibitor w towarzystwie Myrczka i to nagrać, z nadzieją, że się ją zamknie. Gabriel i Laura wykryli intrygę i zatrzymali ją, nie dopuszczając do dezinhibicji Sabiny. O dziwo, tien Kazitan chciała Myrczka CHRONIĆ.

Aktor w Opowieści:

* Dokonanie:
    * traktuje Laurę i Gabriela z szacunkiem; są tam gdzie on chce być. Po ćwiczeniach na Arenie wskazał kto pomagał Myrczkowi - arystokrata i Karolina Erenit.


### Nawoływanie Trzęsawiska

* **uid:** 200417-nawolywanie-trzesawiska, _numer względny_: 5
* **daty:** 0110-08-08 - 0110-08-10
* **obecni:** Gabriel Ursus, Ignacy Myrczek, Napoleon Bankierz, Olaf Zuchwały, Pięknotka Diakon, Sabina Kazitan, Strażniczka Alair

Streszczenie:

Trzęsawisko Zjawosztup zostało Skrzywdzone przez ostatnie wydarzenia. Zaczęło wabić Sabinę i Ignacego by ich pożreć. Szczęśliwie, Pięknotka zdążyła dotrzeć do Sabiny (która wabiona szukała Ignacego) i uratowała ich przed Trzęsawiskiem. Dodatkowo Pięknotka poznała sekret Sabiny i zdobyła nad nią absolutną kontrolę.

Aktor w Opowieści:

* Dokonanie:
    * z rozkazu Teresy Mieralit pilnował Myrczka by nic mu się nie stało gdy ten szukał kobiety której coś grozi; bezpiecznie doprowadził go do Pięknotki.


### Test z etyki

* **uid:** 200326-test-z-etyki, _numer względny_: 4
* **daty:** 0110-07-25 - 0110-07-27
* **obecni:** Aniela Kark, Berenika Wrążowiec, Ignacy Myrczek, Liliana Bankierz, Napoleon Bankierz, Teresa Mieralit

Streszczenie:

Liliana eskalowała swoją krucjatę przeciw "Zygmuntowi Zającowi", włączając do działania Ignacego Myrczka. Teresa Mieralit, nauczycielka m.in. etyki, zrobiła z tego przypadku egzamin dla dwóch uczennic kończących już swoją naukę w Akademii Magii. Skończyło się na stworzeniu anomalnego impa robiącego zdjęć śpiącej Lilianie i jeszcze większym podgrzaniu atmosfery. Ale - konserwy ZZ faktycznie posiadają dziwne substraty.

Aktor w Opowieści:

* Dokonanie:
    * AMZ; podrywa dziewczyny na arenie ćwicząc, ale jak przychodzi temat Liliany to przekierował się na nią by chronić Myrczka. Chronił Zespół przed insektami; solidnie pokąsany.


### Ixioński Transorganik

* **uid:** 190127-ixionski-transorganik, _numer względny_: 3
* **daty:** 0110-01-28 - 0110-01-29
* **obecni:** Erwin Galilien, Karolina Erenit, Kirył Najłalmin, Minerwa Metalia, Napoleon Bankierz, Pięknotka Diakon, Saitaer, Tymon Grubosz, Wiktor Satarail, Wojtek Kurczynos

Streszczenie:

Minerwa próbowała pomóc Karolinie Erenit, ale niestety użyła artefaktu ixiońskiego i przekształciła wrednego Wojtka Kurczynosa w transorganika. Pięknotka z pomocą Epirjona przeteleportowała siebie i Wojtka na Trzęsawisko i tam z pomocą Wiktora Sataraila odwrócili tą transorganizację. Ten konkretny problem udało się dyskretnie rozwiązać.

Aktor w Opowieści:

* Dokonanie:
    * obudzony przez Pięknotkę w środku nocy, natychmiast jej odpowiedział na pytanie gdzie mieszka Karolina Erenit.


### Chrońmy Karolinę przed uczniami

* **uid:** 190113-chronmy-karoline-przed-uczniami, _numer względny_: 2
* **daty:** 0110-01-04 - 0110-01-05
* **obecni:** Adela Kirys, Arnulf Poważny, Karolina Erenit, Liliana Bankierz, Napoleon Bankierz, Pięknotka Diakon, Teresa Mieralit

Streszczenie:

Karla chciała pozbyć się Pięknotki jeszcze raz. Wysłała na papierkową robotę do Szkoły Magów w Zaczęstwie. Tam okazało się, że kandydat na terminusa (Napoleon) próbował uratować człowieka, Karolinę Erenit przed innymi uczniami szkoły magów. Niestety, eliksir który zamówił u Adeli miał efekty uboczne. Pięknotka gładko to rozwiązała i nikt nie miał problemów. Poza tym, że Karolina nadal nie jest chroniona.

Aktor w Opowieści:

* Dokonanie:
    * chciał chronić Karolinę Erenit przed innymi magami, zdobył eliksir od Adeli ale źle wyspecyfikował. Zaraził się, zdemolował pokój i - na szczęście - dyrektor wszystko wyciszył.


### Dwa tygodnie szkoły

* **uid:** 180929-dwa-tygodnie-szkoly, _numer względny_: 1
* **daty:** 0109-09-17 - 0109-09-19
* **obecni:** Adela Kirys, Arnulf Poważny, Atena Sowińska, Erwin Galilien, Felicja Melitniek, Ignacy Myrczek, Miedwied Zajcew, Napoleon Bankierz, Pięknotka Diakon, Roland Grzymość

Streszczenie:

Felicja uczy się w szkole magów, ale nie ma przyjaciół i przez to zachowuje się chaotycznie. Erwin Galilien wpakował się w kłopoty i przegrał power suit "Nutkę" w Kasynie; ma przynieść niebezpieczne grzyby. Uczniowie ze szkoły magów też chodzą do tego Kasyna. Połączone siły Zespołu i Ateny wyplątały Erwina z kłopotów i utrudniły Kasynu zwabianie młodych uczniów ze szkoły magów.

Aktor w Opowieści:

* Dokonanie:
    * kandydat na terminusa w szkole magów, dostał od Felicji (protomag) i jej nie trawi, ale przemowa Ateny obudziła w nim uczucia wielkości i zachwytu do Ateny.
* Progresja:
    * stał się wrogi Felicji (bo go uderzyła) i jest zakochany w słowach Ateny - chce pod nią służyć na Epirjonie.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 9, @: 0111-09-08
    1. Primus    : 9, @: 0111-09-08
        1. Sektor Astoriański    : 9, @: 0111-09-08
            1. Astoria    : 9, @: 0111-09-08
                1. Sojusz Letejski    : 9, @: 0111-09-08
                    1. Szczeliniec    : 9, @: 0111-09-08
                        1. Powiat Jastrzębski    : 1, @: 0110-10-05
                            1. Jastrząbiec, okolice    : 1, @: 0110-10-05
                                1. Blokhaus Widmo    : 1, @: 0110-10-05
                        1. Powiat Pustogorski    : 9, @: 0111-09-08
                            1. Podwiert    : 3, @: 0111-09-08
                                1. Dzielnica Luksusu Rekinów    : 1, @: 0111-09-08
                                    1. Serce Luksusu    : 1, @: 0111-09-08
                                        1. Apartamentowce Elity    : 1, @: 0111-09-08
                                        1. Lecznica Rannej Rybki    : 1, @: 0111-09-08
                                1. Sensoplex    : 1, @: 0110-10-18
                                1. Tor Wyścigowy Pamięci    : 1, @: 0110-10-18
                            1. Pustogor    : 1, @: 0110-08-10
                                1. Eksterior    : 1, @: 0110-08-10
                                    1. Miasteczko    : 1, @: 0110-08-10
                                        1. Knajpa Górska Szalupa    : 1, @: 0110-08-10
                                    1. Zamek Weteranów    : 1, @: 0110-08-10
                                1. Rdzeń    : 1, @: 0110-08-10
                                    1. Szpital Terminuski    : 1, @: 0110-08-10
                            1. Zaczęstwo    : 9, @: 0111-09-08
                                1. Akademia Magii, kampus    : 7, @: 0111-09-08
                                    1. Akademik    : 2, @: 0111-09-08
                                    1. Arena Treningowa    : 3, @: 0111-09-08
                                    1. Artefaktorium    : 1, @: 0110-10-18
                                    1. Audytorium    : 1, @: 0110-10-18
                                    1. Budynek Centralny    : 2, @: 0110-07-27
                                        1. Skrzydło Loris    : 1, @: 0110-07-27
                                    1. Las Trzęsawny    : 1, @: 0111-09-08
                                1. Biurowiec Gorland    : 1, @: 0111-04-19
                                1. Cyberszkoła    : 1, @: 0110-01-29
                                1. Dzielnica Kwiecista    : 1, @: 0111-04-19
                                    1. Rezydencja Porzeczników    : 1, @: 0111-04-19
                                        1. Podziemne Laboratorium    : 1, @: 0111-04-19
                                1. Kasyno Marzeń    : 1, @: 0109-09-19
                                1. Nieużytki Staszka    : 3, @: 0111-04-19
                                1. Osiedle Ptasie    : 1, @: 0110-01-29
                                1. Park Centralny    : 1, @: 0111-04-19
                                    1. Jezioro Gęsie    : 1, @: 0111-04-19
                        1. Trzęsawisko Zjawosztup    : 1, @: 0110-01-29
                            1. Głodna Ziemia    : 1, @: 0110-01-29
                            1. Laboratorium W Drzewie    : 1, @: 0110-01-29

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ignacy Myrczek       | 6 | ((180929-dwa-tygodnie-szkoly; 200326-test-z-etyki; 200417-nawolywanie-trzesawiska; 201006-dezinhibitor-dla-sabiny; 201013-pojedynek-akademia-rekiny; 210323-grzybopreza)) |
| Liliana Bankierz     | 4 | ((190113-chronmy-karoline-przed-uczniami; 200326-test-z-etyki; 201013-pojedynek-akademia-rekiny; 211228-akt-o-ktorym-marysia-nie-wie)) |
| Pięknotka Diakon     | 4 | ((180929-dwa-tygodnie-szkoly; 190113-chronmy-karoline-przed-uczniami; 190127-ixionski-transorganik; 200417-nawolywanie-trzesawiska)) |
| Karolina Erenit      | 3 | ((190113-chronmy-karoline-przed-uczniami; 190127-ixionski-transorganik; 201006-dezinhibitor-dla-sabiny)) |
| Teresa Mieralit      | 3 | ((190113-chronmy-karoline-przed-uczniami; 200326-test-z-etyki; 201013-pojedynek-akademia-rekiny)) |
| Adela Kirys          | 2 | ((180929-dwa-tygodnie-szkoly; 190113-chronmy-karoline-przed-uczniami)) |
| Arnulf Poważny       | 2 | ((180929-dwa-tygodnie-szkoly; 190113-chronmy-karoline-przed-uczniami)) |
| Erwin Galilien       | 2 | ((180929-dwa-tygodnie-szkoly; 190127-ixionski-transorganik)) |
| Gabriel Ursus        | 2 | ((200417-nawolywanie-trzesawiska; 201006-dezinhibitor-dla-sabiny)) |
| Julia Kardolin       | 2 | ((201013-pojedynek-akademia-rekiny; 210323-grzybopreza)) |
| Justynian Diakon     | 2 | ((201006-dezinhibitor-dla-sabiny; 201013-pojedynek-akademia-rekiny)) |
| Kacper Bankierz      | 2 | ((201013-pojedynek-akademia-rekiny; 210323-grzybopreza)) |
| Lorena Gwozdnik      | 2 | ((201006-dezinhibitor-dla-sabiny; 211228-akt-o-ktorym-marysia-nie-wie)) |
| Marysia Sowińska     | 2 | ((210323-grzybopreza; 211228-akt-o-ktorym-marysia-nie-wie)) |
| Sabina Kazitan       | 2 | ((200417-nawolywanie-trzesawiska; 201006-dezinhibitor-dla-sabiny)) |
| Aleksander Bemucik   | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Aniela Kark          | 1 | ((200326-test-z-etyki)) |
| Aranea Diakon        | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Atena Sowińska       | 1 | ((180929-dwa-tygodnie-szkoly)) |
| Berenika Wrążowiec   | 1 | ((200326-test-z-etyki)) |
| Daniel Terienak      | 1 | ((211228-akt-o-ktorym-marysia-nie-wie)) |
| Ernest Namertel      | 1 | ((211228-akt-o-ktorym-marysia-nie-wie)) |
| Felicja Melitniek    | 1 | ((180929-dwa-tygodnie-szkoly)) |
| Karolina Terienak    | 1 | ((211228-akt-o-ktorym-marysia-nie-wie)) |
| Keira Amarco d'Namertel | 1 | ((211228-akt-o-ktorym-marysia-nie-wie)) |
| Kirył Najłalmin      | 1 | ((190127-ixionski-transorganik)) |
| Laura Tesinik        | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Miedwied Zajcew      | 1 | ((180929-dwa-tygodnie-szkoly)) |
| Minerwa Metalia      | 1 | ((190127-ixionski-transorganik)) |
| Olaf Zuchwały        | 1 | ((200417-nawolywanie-trzesawiska)) |
| Rafał Kumczek        | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Rafał Torszecki      | 1 | ((211228-akt-o-ktorym-marysia-nie-wie)) |
| Remor 340D           | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Robert Pakiszon      | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Roland Grzymość      | 1 | ((180929-dwa-tygodnie-szkoly)) |
| Saitaer              | 1 | ((190127-ixionski-transorganik)) |
| Stella Armadion      | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Strażniczka Alair    | 1 | ((200417-nawolywanie-trzesawiska)) |
| Triana Porzecznik    | 1 | ((210323-grzybopreza)) |
| Tymon Grubosz        | 1 | ((190127-ixionski-transorganik)) |
| Urszula Miłkowicz    | 1 | ((210323-grzybopreza)) |
| Wiktor Satarail      | 1 | ((190127-ixionski-transorganik)) |
| Wojtek Kurczynos     | 1 | ((190127-ixionski-transorganik)) |
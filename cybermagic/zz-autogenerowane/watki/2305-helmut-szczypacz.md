# Helmut Szczypacz
## Identyfikator

Id: 2305-helmut-szczypacz

## Sekcja Opowieści

### 

* **uid:** 230699-PLC-dla-kota-PLC, _numer względny_: 4
* **daty:** 0109-10-18 - 0109-10-18
* **obecni:** Anastazy Termann, Fabian Korneliusz, Helmut Szczypacz, Helmut Szczypacz, Klaudia Stryk, Kurator Sarkamair, Martyn Hiwasser, OO Serbinius, SC Karnaxian, SC Tnakraz, TAI Selena d'Tnakraz

Streszczenie:



Aktor w Opowieści:

* Dokonanie:
    * 


### Ziarno Kuratorów na Karnaxianie

* **uid:** 230530-ziarno-kuratorow-na-karnaxianie, _numer względny_: 3
* **daty:** 0109-10-06 - 0109-10-07
* **obecni:** Anastazy Termann, Fabian Korneliusz, Helmut Szczypacz, Klaudia Stryk, Kurator Sarkamair, Leo Mikirnik, Martyn Hiwasser, OO Serbinius, SC Karnaxian

Streszczenie:

Kurator Sarkamair rozstawił Ziarna - mikrofabrykatory atakujące TAI - i złowił salvager Karnaxian. Skonstruował Alexandrię i zaczął rekonstruować silniki by odlecieć do bazy o bliżej nieokreślonej lokalizacji. Serbinius zainteresował się Karnaxianem poza kursem i nie dał się zmylić optymistyczną Semlą i projekcją załogi. Gdy Serbinius odparł atak Kuratora (co było dość trudne), zrobili intruzję by uratować uwięzioną w Aleksandrii załogę. Niestety, nie udało im się - jedyne co mogli zrobić to zniszczyć Karnaxian i ostrzec wszystkich przed nową strategią Kuratorów, a zwłaszcza Sarkamaira.

Aktor w Opowieści:

* Dokonanie:
    * (NIEOBECNY), siedzi na Kontrolerze Pierwszym i dostaje opiernicz za POTENCJALNE ZABICIE ANASTAZEGO. Miranda nie wybacza ;-).


### Helmut i nieoczekiwana awaria Lancera

* **uid:** 230528-helmut-i-nieoczekiwana-awaria-lancera, _numer względny_: 2
* **daty:** 0109-09-23 - 0109-09-26
* **obecni:** Anastazy Termann, Fabian Korneliusz, Helmut Szczypacz, Klaudia Stryk, Martyn Hiwasser, Miranda Termann, Nikodem Dewiremicz, OO Serbinius

Streszczenie:

Helmut próbuje optymalizować substrat fabrykatorów na Serbiniusie, co niepokoi Klaudię. Tymczasem pojawia się seria niewielkich awarii na różnych jednostkach, Serbinius pomaga. Klaudia zauważa wzór, ale dopiero jak członek Serbiniusa ma awarię jetpacka znajduje dowód - to Anomalia Statystyczna, niewykrywalna na Primusie. Współpracując z teoretykiem z Orbitera mapują obszar tej Anomalii i wyciągają wszystkich z kłopotów.

Aktor w Opowieści:

* Dokonanie:
    * koniecznie chce optymalizować wszystkie zużycia materiałów w Lancerach; jego niewielkie modyfikacje prowadzą do tego, że gdy Anastazy zostaje Uciekinierem to nie da się udowodnić jego niewinności. Na szczęście Klaudia umiała - znalazła Anomalię Statystyczną.


### Rozszczepiona Persefona na Itorwienie

* **uid:** 230521-rozszczepiona-persefona-na-itorwienie, _numer względny_: 1
* **daty:** 0109-09-15 - 0109-09-17
* **obecni:** Emilia Ibris, Fabian Korneliusz, Helmut Szczypacz, Iskander Matorin, Karol Brinik, Klaudia Stryk, Martyn Hiwasser, Mojra Karstall, OO Itorwien, OO Serbinius, Tadeusz Arkaladis

Streszczenie:

Itorwien, 'pługośmieciarka' Orbitera miała problem z przemytnikami - ktoś otworzył przemycane narkotyki i cała jednostka była Skażona (lifesupport). Persefona została ograniczona przez psychotronika na pokładzie, więc nie mogła nic zrobić. Ciśnienie i uszkodzenie Persi doprowadziło do jej rozszczepienia. Serbinius uratował Itorwien, bez strat (poza winnym psychotronikiem zabitym przez rozszczepioną Persefonę).

Aktor w Opowieści:

* Dokonanie:
    * combat engineer / heavy weapon specialist z Serbiniusa; konfiguruje lancery na Serbiniusie i pełni rolę inżyniera gdy Serbinius ratuje jednostki. Tym razem - pomógł Fabianowi przejąć kontrolę nad Itorwienem.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 5, @: 0109-10-18
    1. Primus    : 5, @: 0109-10-18
        1. Sektor Astoriański    : 5, @: 0109-10-18
            1. Astoria, Pierścień Zewnętrzny    : 2, @: 0109-09-26

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Fabian Korneliusz    | 4 | ((230521-rozszczepiona-persefona-na-itorwienie; 230528-helmut-i-nieoczekiwana-awaria-lancera; 230530-ziarno-kuratorow-na-karnaxianie; 230699-PLC-dla-kota-PLC)) |
| Klaudia Stryk        | 4 | ((230521-rozszczepiona-persefona-na-itorwienie; 230528-helmut-i-nieoczekiwana-awaria-lancera; 230530-ziarno-kuratorow-na-karnaxianie; 230699-PLC-dla-kota-PLC)) |
| Martyn Hiwasser      | 4 | ((230521-rozszczepiona-persefona-na-itorwienie; 230528-helmut-i-nieoczekiwana-awaria-lancera; 230530-ziarno-kuratorow-na-karnaxianie; 230699-PLC-dla-kota-PLC)) |
| OO Serbinius         | 4 | ((230521-rozszczepiona-persefona-na-itorwienie; 230528-helmut-i-nieoczekiwana-awaria-lancera; 230530-ziarno-kuratorow-na-karnaxianie; 230699-PLC-dla-kota-PLC)) |
| Anastazy Termann     | 3 | ((230528-helmut-i-nieoczekiwana-awaria-lancera; 230530-ziarno-kuratorow-na-karnaxianie; 230699-PLC-dla-kota-PLC)) |
| Kurator Sarkamair    | 2 | ((230530-ziarno-kuratorow-na-karnaxianie; 230699-PLC-dla-kota-PLC)) |
| SC Karnaxian         | 2 | ((230530-ziarno-kuratorow-na-karnaxianie; 230699-PLC-dla-kota-PLC)) |
| Emilia Ibris         | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Iskander Matorin     | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Karol Brinik         | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Leo Mikirnik         | 1 | ((230530-ziarno-kuratorow-na-karnaxianie)) |
| Miranda Termann      | 1 | ((230528-helmut-i-nieoczekiwana-awaria-lancera)) |
| Mojra Karstall       | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Nikodem Dewiremicz   | 1 | ((230528-helmut-i-nieoczekiwana-awaria-lancera)) |
| OO Itorwien          | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| SC Tnakraz           | 1 | ((230699-PLC-dla-kota-PLC)) |
| Tadeusz Arkaladis    | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| TAI Selena d'Tnakraz | 1 | ((230699-PLC-dla-kota-PLC)) |
# Arnulf Poważny
## Identyfikator

Id: 9999-arnulf-poważny

## Sekcja Opowieści

### Liliana w świecie dokumentów

* **uid:** 190820-liliana-w-swiecie-dokumentow, _numer względny_: 10
* **daty:** 0110-06-30 - 0110-07-04
* **obecni:** Adela Pieczar, Arnulf Poważny, Liliana Bankierz, Szymon Jaszczurzec, Tadeusz Kruszawiecki, Tymon Grubosz

Streszczenie:

Tiamenat wyprodukował eksperymentalnego, pomocnego mimika. Chcieli przesłać go do Trzeciego Raju gdzie by się przydał, ale ingerencja Liliany sprawiła, że mimik zniknął. A Tymon próbował rozpaczliwie niczego nie zauważyć, lecz dzięki działaniu Zespołu (magów ze Szkoły Magii) musiał rozwiązać problem. Ech, te dzieci. Ale Liliana odzyskała dobre imię.

Aktor w Opowieści:

* Dokonanie:
    * dyrektor siwiejący przez beztroskie działania Liliany. Próbuje zapewnić by interesy wszystkich były spełnione - co rzadko wychodzi tak jak by chciał.


### Uciekający seksbot

* **uid:** 190519-uciekajacy-seksbot, _numer względny_: 9
* **daty:** 0110-04-21 - 0110-04-22
* **obecni:** Arnulf Poważny, Eliza Ira, Ernest Kajrat, Liliana Bankierz, Ossidia Saitis, Pięknotka Diakon, Saitaer, Teresa Mieralit, Tomasz Tukan

Streszczenie:

Ernest z mafii przybył do szkoły magów w Zaczęstwie szukając swojego seksbota. Pięknotka deeskalowała sytuację i poszła z neuronautą Tomaszem szukać; okazało się, że seksbot jest bardzo zmodyfikowany i bardzo świadomy - do tego stopnia, że seksbotowi zaczęła pomagać Eliza Ira. Pięknotka i Tomasz rozmontowali problem, ale Pięknotka nie miała serca oddać seksbota sadystycznemu Ernestowi. W rozpaczy, poprosiła o pomoc Saitaera, który odpowiedział...

Aktor w Opowieści:

* Dokonanie:
    * dyrektor, który przedkłada potencjalnie żywego seksbota nad pozycję i nad zadowolenie Ernesta Kajrata z mafii.


### Nie da się odrzucić mocy

* **uid:** 190206-nie-da-sie-odrzucic-mocy, _numer względny_: 8
* **daty:** 0110-02-16 - 0110-02-19
* **obecni:** Arnulf Poważny, Karolina Erenit, Kasjopea Maus, Kornel Garn, Minerwa Metalia, Pięknotka Diakon, Tymon Grubosz

Streszczenie:



Aktor w Opowieści:

* Dokonanie:
    * 


### Chrońmy Karolinę przed uczniami

* **uid:** 190113-chronmy-karoline-przed-uczniami, _numer względny_: 7
* **daty:** 0110-01-04 - 0110-01-05
* **obecni:** Adela Kirys, Arnulf Poważny, Karolina Erenit, Liliana Bankierz, Napoleon Bankierz, Pięknotka Diakon, Teresa Mieralit

Streszczenie:

Karla chciała pozbyć się Pięknotki jeszcze raz. Wysłała na papierkową robotę do Szkoły Magów w Zaczęstwie. Tam okazało się, że kandydat na terminusa (Napoleon) próbował uratować człowieka, Karolinę Erenit przed innymi uczniami szkoły magów. Niestety, eliksir który zamówił u Adeli miał efekty uboczne. Pięknotka gładko to rozwiązała i nikt nie miał problemów. Poza tym, że Karolina nadal nie jest chroniona.

Aktor w Opowieści:

* Dokonanie:
    * dyrektor skłonny do osłony swoich uczniów nawet, jeśli coś zbroili. Będzie współpracował z Pięknotką, bo udowodniła, że zależy jej na dobru a nie prawie.
* Progresja:
    * będzie współpracował z Pięknotką jako terminuską zanim z jakimkolwiek innym terminusem.
    * stracił trochę szacunku w oczach innych uczniów; myślą, że nie rozpoznał Pięknotki jako terminuski. Cóż.


### Terminuska czy kosmetyczka?

* **uid:** 181027-terminuska-czy-kosmetyczka, _numer względny_: 6
* **daty:** 0109-10-07 - 0109-10-11
* **obecni:** Adela Kirys, Arnulf Poważny, Brygida Maczkowik, Ignacy Myrczek, Minerwa Metalia, Pięknotka Diakon

Streszczenie:

Jednoczesny atak na reputację Ateny (przez cybergrzyby) oraz wielki konkurs kosmetyczny w Pustogorze wpłynęły stosunkowo źle na sytuację Pięknotki. Nie rozdwoiła się, ale obie rzeczy udało jej się rozwiązać - acz jest wycieńczona. Odkryła konspirację wymierzoną przeciwko Atenie - ale nie wie kto i gdzie próbuje Atenę usunąć.

Aktor w Opowieści:

* Dokonanie:
    * skutecznie osłonił reputację Ateny; nie miał pojęcia, że jego uczeń uczestniczył w akcji wymierzonej w Atenę.


### Dwa tygodnie szkoły

* **uid:** 180929-dwa-tygodnie-szkoly, _numer względny_: 5
* **daty:** 0109-09-17 - 0109-09-19
* **obecni:** Adela Kirys, Arnulf Poważny, Atena Sowińska, Erwin Galilien, Felicja Melitniek, Ignacy Myrczek, Miedwied Zajcew, Napoleon Bankierz, Pięknotka Diakon, Roland Grzymość

Streszczenie:

Felicja uczy się w szkole magów, ale nie ma przyjaciół i przez to zachowuje się chaotycznie. Erwin Galilien wpakował się w kłopoty i przegrał power suit "Nutkę" w Kasynie; ma przynieść niebezpieczne grzyby. Uczniowie ze szkoły magów też chodzą do tego Kasyna. Połączone siły Zespołu i Ateny wyplątały Erwina z kłopotów i utrudniły Kasynu zwabianie młodych uczniów ze szkoły magów.

Aktor w Opowieści:

* Dokonanie:
    * dyrektor szkoły magów. Dobry mag, któremu zależy na uczniach (min Felicji). Próbuje współpracować z opiekunami i chronić uczniów.


### Czarodziejka, która jednak może się zabić

* **uid:** 211019-czarodziejka-ktora-jednak-moze-sie-zabic, _numer względny_: 4
* **daty:** 0084-12-20 - 0084-12-24
* **obecni:** Arnulf Poważny, Klaudia Stryk, Ksenia Kirallen, Mariusz Trzewń, Maryla Koternik, Talia Aegis, Teresa Mieralit

Streszczenie:

Nastroje antynoktiańskie się nasilają. Pojawiły się seksboty do bicia, w kształcie noktian i noktianek (co strasznie zestresowało Teresę). Klaudia zaproponowała Arnulfowi powiększenie kadry o Weteranów i przejmując to prawie się zarżnęła (uratował ją Trzewń). Maryla szuka informacji o Teresie - szuka ukrytego noktianina na AMZ, ale Trzewń wykluczył Teresę dzięki dokumentom sformowanym przez Arnulfa. Klaudia i Ksenia pracują nad petycją, by stworzyć Dom Weteranów Noctis.

Aktor w Opowieści:

* Dokonanie:
    * opiekuje się Teresą; dał jej ubrania po córce. Zgodził się na plan Klaudii by zapewnić AMZ kadrę ze strony Weteranów. Wyraźnie przeładowany.


### Nastolatka w bieliźnie na dachu w burzy

* **uid:** 211017-nastolatka-w-bieliznie-na-dachu-w-burzy, _numer względny_: 3
* **daty:** 0084-12-14 - 0084-12-15
* **obecni:** Arnulf Poważny, Klaudia Stryk, Ksenia Kirallen, Sasza Morwowiec, Teresa Mieralit, Waldemar Grzymość

Streszczenie:

W środku nocy Klaudia i Ksenia widzą Teresę w środku burzy na dachu Złomiarium. Poszły ją ściągnąć i natknęły się na terminusa. Ksenia odciągnęła Saszę od Teresy, Klaudia Teresę wysłała do ich pokoju w akademiku (gdzie Teresa spisana za nieletnią prostytucję). Sasza wyjaśnił, że nadmiar lojalności wobec AMZ jest szkodliwy. Klaudia ogromnym wysiłkiem spowolniła Saszę i odwróciła jego uwagę. Potem Klaudia i Ksenia zdecydowały, że zsocjalizują małą noktiankę - i zdobyły trzyosobowy pokój w akademiku mimo protestów Teresy.

Aktor w Opowieści:

* Dokonanie:
    * przespał infiltrację AMZ przez Saszę. Gdy się obudził, Teresa była podejrzana o prostytucję i Klaudia + Ksenia chciały ją w pokoju. Arnulf bardzo starał się nie łączyć tych zdarzeń i dał im trzyosobowy pokój...


### Ukryta wychowanka Arnulfa

* **uid:** 211010-ukryta-wychowanka-arnulfa, _numer względny_: 2
* **daty:** 0084-12-11 - 0084-12-12
* **obecni:** Arnulf Poważny, Felicjan Szarak, Klaudia Stryk, Ksenia Kirallen, Sasza Morwowiec, Strażniczka Alair, Talia Aegis, Teresa Mieralit, Tymon Grubosz

Streszczenie:

Dyrektor Arnulf Poważny ma wychowankę - Teresę Mieralit, noktiańską piętnastonastolatkę która mieszka w AMZ. Strażniczka Alair ją wykryła i wpadła w pętlę - komponenty TAI uznały ją za potencjalne zagrożenie, BIA za osobę do ochrony. Strażniczka wycofała drony do osłony AI Core i zaatakowały Teresę. Klaudia zorientowała się w problemie, obudziła hipernetem Arnulfa. Arnulf osłonił Teresę, Klaudia wezwała Talię i w grupie udało im się opanować niesforne TAI (bo Klaudia zanomalizowała konwertery energii Strażniczki, która czerpie energię z Trzęsawiska).

Aktor w Opowieści:

* Dokonanie:
    * przyznał się Talii i Tymonowi do tego, że ukrywa małą noktiankę (Teresę Mieralit). Wygnał terminusa Saszę z terenu szkoły, robiąc sobie w nim wroga. Samemu zestrzelił serię dron Strażniczki.
* Progresja:
    * ma wroga w Saszy Morwowcu (terminus). Uważa go za ko-konspiratora Tymona.


### Nowa Strażniczka AMZ

* **uid:** 210926-nowa-strazniczka-amz, _numer względny_: 1
* **daty:** 0084-06-14 - 0084-06-26
* **obecni:** Albert Kalandryk, Arnulf Poważny, Klaudia Stryk, Ksenia Kirallen, Mariusz Trzewń, Strażniczka Alair, Talia Aegis, Tymon Grubosz

Streszczenie:

Klaudia - młoda grzeczna administratorka AMZ - przypadkiem odkrywa konspirację dyrektora, terminusa i noktianki by uratować umierającą Eszarę. Po konfrontacji z dyrektorem pomaga im by wszystko działało. Trzewń uniknął amnestyków.

Aktor w Opowieści:

* Dokonanie:
    * 36 lat; nowy dyrektor AMZ (poprzedni zginął na wojnie). Ma jakąś przeszłość z siłami specjalnymi Pustogoru. Współpracuje z Tymonem i Talią nad zregenerowaniem hybrydowej Eszary jako Strażniczki Alair. Nie ufa noktiance, ale skłonny zaryzykować.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 10, @: 0110-07-04
    1. Primus    : 10, @: 0110-07-04
        1. Sektor Astoriański    : 10, @: 0110-07-04
            1. Astoria    : 10, @: 0110-07-04
                1. Sojusz Letejski    : 10, @: 0110-07-04
                    1. Szczeliniec    : 10, @: 0110-07-04
                        1. Powiat Pustogorski    : 10, @: 0110-07-04
                            1. Pustogor    : 2, @: 0109-10-11
                                1. Arena Szalonego Króla    : 1, @: 0109-10-11
                                1. Laboratorium Senetis    : 1, @: 0109-10-11
                                1. Zamek Weteranów    : 1, @: 0084-06-26
                            1. Zaczęstwo    : 10, @: 0110-07-04
                                1. Akademia Magii, kampus    : 10, @: 0110-07-04
                                    1. Akademik    : 4, @: 0084-12-24
                                    1. Budynek Centralny    : 5, @: 0110-07-04
                                        1. Skrzydło Loris    : 2, @: 0110-07-04
                                    1. Domek dyrektora    : 2, @: 0110-02-19
                                    1. Złomiarium    : 2, @: 0084-12-15
                                1. Arena Migświatła    : 1, @: 0084-06-26
                                1. Cyberszkoła    : 3, @: 0110-07-04
                                1. Kasyno Marzeń    : 1, @: 0109-09-19
                                1. Klub Poetycki Sucharek    : 1, @: 0110-07-04
                                1. Kwatera Terminusa    : 1, @: 0110-02-19
                                1. Nieużytki Staszka    : 2, @: 0109-10-11
                                1. Papierówka    : 1, @: 0110-07-04
                        1. Trzęsawisko Zjawosztup    : 1, @: 0109-10-11

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Pięknotka Diakon     | 5 | ((180929-dwa-tygodnie-szkoly; 181027-terminuska-czy-kosmetyczka; 190113-chronmy-karoline-przed-uczniami; 190206-nie-da-sie-odrzucic-mocy; 190519-uciekajacy-seksbot)) |
| Teresa Mieralit      | 5 | ((190113-chronmy-karoline-przed-uczniami; 190519-uciekajacy-seksbot; 211010-ukryta-wychowanka-arnulfa; 211017-nastolatka-w-bieliznie-na-dachu-w-burzy; 211019-czarodziejka-ktora-jednak-moze-sie-zabic)) |
| Klaudia Stryk        | 4 | ((210926-nowa-strazniczka-amz; 211010-ukryta-wychowanka-arnulfa; 211017-nastolatka-w-bieliznie-na-dachu-w-burzy; 211019-czarodziejka-ktora-jednak-moze-sie-zabic)) |
| Ksenia Kirallen      | 4 | ((210926-nowa-strazniczka-amz; 211010-ukryta-wychowanka-arnulfa; 211017-nastolatka-w-bieliznie-na-dachu-w-burzy; 211019-czarodziejka-ktora-jednak-moze-sie-zabic)) |
| Tymon Grubosz        | 4 | ((190206-nie-da-sie-odrzucic-mocy; 190820-liliana-w-swiecie-dokumentow; 210926-nowa-strazniczka-amz; 211010-ukryta-wychowanka-arnulfa)) |
| Adela Kirys          | 3 | ((180929-dwa-tygodnie-szkoly; 181027-terminuska-czy-kosmetyczka; 190113-chronmy-karoline-przed-uczniami)) |
| Liliana Bankierz     | 3 | ((190113-chronmy-karoline-przed-uczniami; 190519-uciekajacy-seksbot; 190820-liliana-w-swiecie-dokumentow)) |
| Talia Aegis          | 3 | ((210926-nowa-strazniczka-amz; 211010-ukryta-wychowanka-arnulfa; 211019-czarodziejka-ktora-jednak-moze-sie-zabic)) |
| Ignacy Myrczek       | 2 | ((180929-dwa-tygodnie-szkoly; 181027-terminuska-czy-kosmetyczka)) |
| Karolina Erenit      | 2 | ((190113-chronmy-karoline-przed-uczniami; 190206-nie-da-sie-odrzucic-mocy)) |
| Mariusz Trzewń       | 2 | ((210926-nowa-strazniczka-amz; 211019-czarodziejka-ktora-jednak-moze-sie-zabic)) |
| Minerwa Metalia      | 2 | ((181027-terminuska-czy-kosmetyczka; 190206-nie-da-sie-odrzucic-mocy)) |
| Napoleon Bankierz    | 2 | ((180929-dwa-tygodnie-szkoly; 190113-chronmy-karoline-przed-uczniami)) |
| Sasza Morwowiec      | 2 | ((211010-ukryta-wychowanka-arnulfa; 211017-nastolatka-w-bieliznie-na-dachu-w-burzy)) |
| Strażniczka Alair    | 2 | ((210926-nowa-strazniczka-amz; 211010-ukryta-wychowanka-arnulfa)) |
| Adela Pieczar        | 1 | ((190820-liliana-w-swiecie-dokumentow)) |
| Albert Kalandryk     | 1 | ((210926-nowa-strazniczka-amz)) |
| Atena Sowińska       | 1 | ((180929-dwa-tygodnie-szkoly)) |
| Brygida Maczkowik    | 1 | ((181027-terminuska-czy-kosmetyczka)) |
| Eliza Ira            | 1 | ((190519-uciekajacy-seksbot)) |
| Ernest Kajrat        | 1 | ((190519-uciekajacy-seksbot)) |
| Erwin Galilien       | 1 | ((180929-dwa-tygodnie-szkoly)) |
| Felicja Melitniek    | 1 | ((180929-dwa-tygodnie-szkoly)) |
| Felicjan Szarak      | 1 | ((211010-ukryta-wychowanka-arnulfa)) |
| Kasjopea Maus        | 1 | ((190206-nie-da-sie-odrzucic-mocy)) |
| Kornel Garn          | 1 | ((190206-nie-da-sie-odrzucic-mocy)) |
| Maryla Koternik      | 1 | ((211019-czarodziejka-ktora-jednak-moze-sie-zabic)) |
| Miedwied Zajcew      | 1 | ((180929-dwa-tygodnie-szkoly)) |
| Ossidia Saitis       | 1 | ((190519-uciekajacy-seksbot)) |
| Roland Grzymość      | 1 | ((180929-dwa-tygodnie-szkoly)) |
| Saitaer              | 1 | ((190519-uciekajacy-seksbot)) |
| Szymon Jaszczurzec   | 1 | ((190820-liliana-w-swiecie-dokumentow)) |
| Tadeusz Kruszawiecki | 1 | ((190820-liliana-w-swiecie-dokumentow)) |
| Tomasz Tukan         | 1 | ((190519-uciekajacy-seksbot)) |
| Waldemar Grzymość    | 1 | ((211017-nastolatka-w-bieliznie-na-dachu-w-burzy)) |
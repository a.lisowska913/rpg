# OO Królowa Kosmicznej Chwały
## Identyfikator

Id: 9999-oo-królowa-kosmicznej-chwały

## Sekcja Opowieści

### Kapitan Verlen i koniec przygody na Królowej

* **uid:** 221026-kapitan-verlen-i-koniec-przygody-na-krolowej, _numer względny_: 5
* **daty:** 0100-04-10 - 0100-04-17
* **obecni:** Antoni Kramer, Arianna Verlen, Arnulf Perikas, Daria Czarnewik, Erwin Pies, Klaudiusz Terienak, Leszek Kurzmin, OO Królowa Kosmicznej Chwały, OO Tucznik Trzeci, Stefan Torkil, Tomasz Ruppok

Streszczenie:

Po długiej i ciężkiej pracy Arianna i Daria doprowadziły Królową do 50% sprawności nominalnej. Radość i wysokie morale załogi. Gdy Arnulf przyszedł do Arianny mówiąc o fabrykacji narkotyków i alkoholu, ona kazała to 'zatruć'. Zmotywowała Ruppoka, by dał jej szansę i znalazła sposób jak częściowo odzyskać materiały do fabrykatora po rozmowie z Arnulfem - sojusz z bliską Anomalii Kolapsu stacją Nonarion; poprosiła Kurzmina by to załatwił. TAK PRZESTRASZYŁA siły specjalne Orbitera, że Ariannę i załogę przenieśli bliżej Nonariona by sabotować program kosmiczny Aurum. Koniec przygody na Królowej :-).

Aktor w Opowieści:

* Dokonanie:
    * wreszcie zaczęła działać, choć jest jeszcze brzydsza niż kiedykolwiek. Faktycznie statek patchworkowy.
* Progresja:
    * doprowadzona do 50% funkcjonowania wszystkich systemów; jeszcze 1-2 miesiące w dokach i będzie w pełni sprawna.
    * całkowicie poprzerzucana załoga, by nie była zbyt skuteczna. Arianna i większość załogi -> Astralna Flara.


### Kapitan Verlen i pierwszy ruch statku

* **uid:** 221019-kapitan-verlen-i-pierwszy-ruch-statku, _numer względny_: 4
* **daty:** 0100-03-25 - 0100-04-06
* **obecni:** Arianna Verlen, Daria Czarnewik, Erwin Pies, Maja Samszar, Marcelina Trzęsiel, OO Królowa Kosmicznej Chwały, Romeo Verlen, Rufus Warkoczyk, Stefan Torkil

Streszczenie:

Arianna włączyła ćwiczenia dla Królowej - prawie wyszło, ale ludzie popanikowali a statek odmówił posłuszeństwa i załoga zaczęła się bić. Arianna ich przećwiczyła w działaniach awaryjnych i oddała panikarza medykowi - niestety, Paradoks zmienił Marcela w Marcelinę... ale następnym razem ćwiczenia powinny się już udać. Daria ma sprawę pod kontrolą i Arianna też raczej jest pewna sukcesu.

Aktor w Opowieści:

* Dokonanie:
    * ruszyła. Aktywna i w miarę sprawna, ale załoga nieprzeszkolona. Arianna przerwała ćwiczenia by wszystkich doprowadzić do zdolności działania.


### Kapitan Verlen i niezapowiedziana inspekcja

* **uid:** 221012-kapitan-verlen-i-niezapowiedziana-inspekcja, _numer względny_: 3
* **daty:** 0100-03-19 - 0100-03-23
* **obecni:** Adam Chrząszczewicz, Arianna Verlen, Arnulf Perikas, Daria Czarnewik, Hubert Kerwelenios, Leona Astrienko, Maja Samszar, Mariusz Bulterier, OO Królowa Kosmicznej Chwały, Szczepan Myrczek, Tomasz Dojnicz, Władawiec Diakon

Streszczenie:

Podczas skanu kadłuba Królowej advancerzy z Darią wykryli scrambler TAI, destabilizator silników itp. Arianna stoczyła bitwę ćwiczebną z marine, ale pojawiła się inspekcja (która nie była zapowiedziana bo Maja nie przekazała informacji). Daria reanimowała TAI, wszystko częściowo poszło nie tak. Ale o dziwo audytor chciał pomóc Ariannie, która stanęła między Orbiterem (wsparcie) i Aurum (my sami sobie poradzimy). Arianna upewniła się, że Władawiec nie stał za operacją - najpewniej Maja. Ale Królowa jest w lepszym stanie niż kiedykolwiek, tylko, że Arianna je konserwy (by nie wpaść pod feromony itp)

Aktor w Opowieści:

* Dokonanie:
    * okazuje się, że było mnóstwo małych sabotowanych rzeczy na zewnątrz; m.in. scrambler TAI. Po usunięciu, TAI Semla zaczęła działać.


### Kapitan Verlen i pojedynek z marine

* **uid:** 220928-kapitan-verlen-i-pojedynek-z-marine, _numer względny_: 2
* **daty:** 0100-03-16 - 0100-03-18
* **obecni:** Alezja Dumorin, Arianna Verlen, Arnulf Perikas, Daria Czarnewik, Erwin Pies, Grażyna Burgacz, Leona Astrienko, Leszek Kurzmin, Maja Samszar, OO Królowa Kosmicznej Chwały, Stefan Torkil, Szymon Wanad, Tomasz Ruppok, Władawiec Diakon

Streszczenie:

Królowa miała się przesunąć, ale prawie się rozbiła; ta jednostka jest rozkradziona. Arianna zatrzymała biczowanie załogantów i doszła do tego, że załoga jest niemrawa i skonfliktowana bo część ludzi jest z Aurum a część nie. Perfect storm of suck. By zatrzymać Wanada przed "wygraniem cnoty koleżanki inżyniera" stoczyła z nim pojedynek i wygrała. Leona ostrzegła, by Arianna nic nie jadła i nie piła. Czyli coś jest w jedzeniu. To tłumaczy czemu Królowa jest tak zdegenerowaną jednostką. Aha, Arianna ma wsparcie kapitana Leszka Kurzmina, dawnego rywala Alezji.

Aktor w Opowieści:

* Dokonanie:
    * część sprzętu rozsprzedana, ledwo sprawna TAI Semla (w trybie uśpionym), załoga jest w konflikcie: Aurum - nie-Aurum i niekoniecznie chcą współpracować.


### Kapitan Verlen i Królowa Kosmicznej Chwały

* **uid:** 220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly, _numer względny_: 1
* **daty:** 0100-03-08 - 0100-03-14
* **obecni:** Alezja Dumorin, Arianna Verlen, Arnulf Perikas, Daria Czarnewik, Erwin Pies, Grażyna Burgacz, Klarysa Jirnik, Klaudiusz Terienak, Leona Astrienko, Maja Samszar, OO Królowa Kosmicznej Chwały, Szymon Wanad, Władawiec Diakon

Streszczenie:

Królowa Kosmicznej Chwały to najbardziej dysfunkcyjna jednostka pod kontrolą Orbitera - służy do łamania karier obiecujących oficerów i sabotażu programu kosmicznego Aurum (oba z woli sił specjalnych Aurum). Trafiła na królową Arianna i zaczęła robić porządek. Poprzednią panią kapitan zdecydowała się odratować, współpracując z Darią z inżynierii rozmontować wyścigi psów i stwierdziła, że doprowadzi do tego, by Królowa zaczęła skutecznie działać podczas ćwiczeń. Aha, tu poznała się z Leoną Astrienko ;-).

Aktor w Opowieści:

* Dokonanie:
    * najbardziej dysfunkcyjna jednostka pod kontrolą Orbitera - służy do łamania karier obiecujących oficerów i sabotażu programu kosmicznego Aurum. Arianna przejęła nad tą jednostką dowodzenie.
* Progresja:
    * jeszcze 5 dni zajmie przebudowywanie torów i sensorów itp by doszła do jakiegokolwiek funkcjonowania.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 5, @: 0100-04-17
    1. Primus    : 5, @: 0100-04-17
        1. Sektor Astoriański    : 5, @: 0100-04-17

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 5 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221019-kapitan-verlen-i-pierwszy-ruch-statku; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Daria Czarnewik      | 5 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221019-kapitan-verlen-i-pierwszy-ruch-statku; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Arnulf Perikas       | 4 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Erwin Pies           | 4 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221019-kapitan-verlen-i-pierwszy-ruch-statku; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Maja Samszar         | 4 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221019-kapitan-verlen-i-pierwszy-ruch-statku)) |
| Leona Astrienko      | 3 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221012-kapitan-verlen-i-niezapowiedziana-inspekcja)) |
| Stefan Torkil        | 3 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221019-kapitan-verlen-i-pierwszy-ruch-statku; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Władawiec Diakon     | 3 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221012-kapitan-verlen-i-niezapowiedziana-inspekcja)) |
| Alezja Dumorin       | 2 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine)) |
| Grażyna Burgacz      | 2 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine)) |
| Klaudiusz Terienak   | 2 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Leszek Kurzmin       | 2 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Szymon Wanad         | 2 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine)) |
| Tomasz Ruppok        | 2 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Adam Chrząszczewicz  | 1 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja)) |
| Antoni Kramer        | 1 | ((221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Hubert Kerwelenios   | 1 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja)) |
| Klarysa Jirnik       | 1 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly)) |
| Marcelina Trzęsiel   | 1 | ((221019-kapitan-verlen-i-pierwszy-ruch-statku)) |
| Mariusz Bulterier    | 1 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja)) |
| OO Tucznik Trzeci    | 1 | ((221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Romeo Verlen         | 1 | ((221019-kapitan-verlen-i-pierwszy-ruch-statku)) |
| Rufus Warkoczyk      | 1 | ((221019-kapitan-verlen-i-pierwszy-ruch-statku)) |
| Szczepan Myrczek     | 1 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja)) |
| Tomasz Dojnicz       | 1 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja)) |
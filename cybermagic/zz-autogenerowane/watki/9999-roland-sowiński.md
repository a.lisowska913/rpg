# Roland Sowiński
## Identyfikator

Id: 9999-roland-sowiński

## Sekcja Opowieści

### EtAur - dziwnie ważny węzeł

* **uid:** 220706-etaur-dziwnie-wazny-wezel, _numer względny_: 12
* **daty:** 0112-07-15 - 0112-07-17
* **obecni:** Aleksandra Termia, Eustachy Korkoran, Klaudia Stryk, Maria Naavas

Streszczenie:

Maria zainteresowała się planem adm. Termii wskazującym na to, że ta zdradza Orbiter z Syndykatem Aureliona. Eustachy (do którego przyszła) zapytał Termię wprost i doszedł do tego, że Termia szuka koloidowej bazy Aureliona na księżycu Elang swymi bezdusznymi metodami. Eustachy i Klaudia wskazali Termii, że baza EtAur jest potencjalnie punktem kontaktowym Syndykatu z Astorią i tu jest ryzyko. Eustachy doprowadził do kontaktu Termia - EtAur, ale też gra na współpracę Inferni i EtAur.

Aktor w Opowieści:

* Dokonanie:
    * AKTOR NIEOBECNY NA SESJI
* Progresja:
    * wpakował na Infernię kilku kompetentnych i brzydkich mechaników. Nie wie, że Klaudia i Eustachy wiedzą, że to jego ludzie.


### Sklejanie Inferni do kupy

* **uid:** 211215-sklejanie-inferni-do-kupy, _numer względny_: 11
* **daty:** 0112-02-21 - 0112-02-23
* **obecni:** Adam Szarjan, Arianna Verlen, Diana d'Infernia, Elena Verlen, Eustachy Korkoran, Kamil Lyraczek, Kasandra Destrukcja Diakon, Leona Astrienko, Maria Naavas, OO Infernia, Roland Sowiński

Streszczenie:

Ixioński wariant Inferni okazuje się całkiem słodki i pod kontrolą Eustachego. Morale Inferni jest w ruinie. Z pomocą Marii Arianna przesuwa kulturę Inferni w stronę "kultyści + noktianie + miłośnicy psychopatycznej lolitki Diany". Eustachy wpada w pułapkę Leony, która chce go wyleczyć czerwiem Esuriit - ale Eustachy przekonuje ją, że ma Dianę pod kontrolą. Jednak gdzieś w tle znajduje się prawdziwy ixioński potwór na stoczni Neotik.

Aktor w Opowieści:

* Dokonanie:
    * NIEOBECNY, ale wprowadził dwie kategorie agentów na Infernię. Okazuje się, że zewnętrzna kategoria (leszcze) mają być "typowi", ale wewnętrzna kategoria - dokładna analiza czego Infernia chce się dowiedzieć, systemów Inferni itp.


### Prototypowa Nereida Natalii

* **uid:** 211124-prototypowa-nereida-natalii, _numer względny_: 10
* **daty:** 0112-02-14 - 0112-02-18
* **obecni:** Adam Szarjan, Arianna Verlen, Arkadia Verlen, Elena Verlen, Eustachy Korkoran, Izabela Zarantel, Klaudia Stryk, Leona Astrienko, Maria Naavas, Natalia Aradin, OO Infernia, Roland Sowiński, Wawrzyn Rewemis

Streszczenie:

Kampania reklamowa "dołącz do Inferni" zrobiona przez Izę przy użyciu Eustachego jako modela - sukces. Klaudia zdobyła z tego nowych załogantów (acz to jeszcze potrwa). Szarjan uwolnił Klaudię z zarzutów, potrzebuje pomocy w stoczni - jego przyjaciółka Natalia ma problem z prototypem myśliwca klasy Nereida i została ixiońsko zintegrowana. Infernia próbowała Nereidę uratować, ale niestety skończyło się to poważnym uszkodzeniem Nereidy i stworzeniem ixiońskiego kokonu, z którego Natalia musi się wykluć...

Aktor w Opowieści:

* Dokonanie:
    * chciał się wprosić na Infernię, ale Arianna nie chciała. Wsadził Inferni kilku kompetentnych ludzi, co wszystkich zdziwiło (kompetentnych czemu? O_O). Protekcjonalny wobec Arianny, "małej buntowniczki" jak się mu przyznała.


### Porwany Trismegistos

* **uid:** 211117-porwany-trismegistos, _numer względny_: 9
* **daty:** 0112-02-09 - 0112-02-11
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Izabela Zarantel, Jamon Korab, Kalira d'Trismegistos, Klara Gwozdnik, Klaudia Stryk, Leona Astrienko, Maria Naavas, Mira Anastel, OO Tivr, Roland Sowiński, SC Trismegistos, Zygfryd Maus

Streszczenie:

Wiadomość od pirata że porwał Trismegistos poderwała Ariannę i Tivr. Historia nie trzymała się kupy. Załoga Inferni (Tivru) zorientowała się, że coś jest nie tak - opowieść o statku kupieckim który pozbył się miragenta brzmiała DZIWNIE. Okazało się, że Trismegistos jest statkiem kaperskim. Gdy Trismegistos zażądał poddania się Tivru Klaudia zrobiła sygnał "lol leci na Was Serenit" i TAI Trismegistosa spanikowała. Wolna TAI, współpracująca z fareilem i drakolitką. Wolny dystrykt. Arianna dała im żyć - ale odzyskała porwanych arystokratów i komory adaptacji.

Aktor w Opowieści:

* Dokonanie:
    * goguś i dandys, który jednak dziwnie od razu zorientował się, że mają do czynienia z wolną TAI (Kalira). Prosił Ariannę, by ta koniecznie zniszczyła Kalirę.


### Ostatnia akcja bohaterki

* **uid:** 210922-ostatnia-akcja-bohaterki, _numer względny_: 8
* **daty:** 0111-12-19 - 0112-01-03
* **obecni:** Antoni Kramer, Arianna Verlen, Elena Verlen, Izabela Zarantel, Jolanta Kopiec, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Olgierd Drongon, OO Żelazko, Roland Sowiński

Streszczenie:

Z Martynem skontaktowała się tien Kopiec prosząc o ostatnią misję zanim Esuriit ją pożre. Martyn zorganizował lot Infernią. Ustabilizował Jolantę, by ta dała radę dolecieć słowami i opowieściami, przy okazji tworząc piękną opowieść o Eterni i eulogię dla Jolanty - bohaterki wojennej - w Sekretach Orbitera (przy okazji wyszła jego przeszłość). Infernia zniszczyła Krwawą Bazę Piratów kosztem dewastacji Żelazka. Jolanta zniszczyła krwawego maga. Sektor jest bezpieczniejszy, Eternia szczęśliwa a Arianna ma profity.

Aktor w Opowieści:

* Dokonanie:
    * NIC NIE ZROBIŁ. Ale Arianna powiedziała Olgierdowi, że się do niej zaleca i Olgierd zrobi mu wpierdol.


### Nieudana infiltracja Inferni

* **uid:** 210616-nieudana-infiltracja-inferni, _numer względny_: 7
* **daty:** 0111-11-22 - 0111-11-27
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Flawia Blakenbauer, Izabela Zarantel, Kamil Lyraczek, Klaudia Stryk, Leona Astrienko, Marian Tosen, OO Opresor, Roland Sowiński

Streszczenie:

Marian Tosen poprosił Infernię o to, by sprawdzili co się dzieje w Anomalii Kolapsu - podejrzewa anomalie memetyczne, skoro Orbiter traci tam statki  (m.in. OO Opresor) i pojawiają się jednostki nieoznaczonej frakcji. Tymczasem Roland Sowiński chce na pokład. Arianna wybiła mu to z głowy, ale wykryli agentkę wśród jego guwernantek - Flawię Blakenbauer, która miała się nim opiekować i jest trochę zniewolona przez Sowińskich. By zadośćuczynić po tym co Elena zrobiła i trochę Flawii pomóc, Flawia dołączyła do Inferni. "Oficjalnie" udało jej się Infernię zinfiltrować...

Aktor w Opowieści:

* Dokonanie:
    * koniecznie chciał lecieć Infernią na SPECJALNĄ MISJĘ. Jak się okazało (Kamil <3) jak jest niebezpieczna, już nie chciał. Zwolnił guwernantkę (Flawię), bo Arianna zrobiła scenę że ta niby go okradła.


### Sekrety Kariatydy

* **uid:** 210609-sekrety-kariatydy, _numer względny_: 6
* **daty:** 0111-11-05 - 0111-11-08
* **obecni:** Arianna Verlen, Eustachy Korkoran, Marian Tosen, OO Kariatyda, Rafael Galwarn, Roland Sowiński

Streszczenie:

Arianna chce Tivr do swojej floty. Do tego celu zdecydowała się poznać sekrety Kariatydy - czemu Walrond tak unika tego statku i nazwy? Niestety, Arianna i Eustachy rozognili opowieść o "Orbiterze, który porzucił swoich w Anomalii Kolapsu" a Arianna dowiedziała się dyskretnie, że OO Kariatyda to specjalny statek - nie można powiedzieć, że był zniszczony. Ten statek został porwany przez TAI i uciekł do Anomalii Kolapsu. Arianna, promienna mistrzyni PR, zmieniła to w "KONKURS. Wyślij KARIATYDA i wygrasz przejażdżkę luksusowym statkiem". Aha, Roland Sowiński (uratowany przez Ariannę z Odłamka Serenita) chce się z nią ożenić.

Aktor w Opowieści:

* Dokonanie:
    * oczarowany przez Ariannę i wdzięczny jej za uratowanie z Odłamka Serenita zdecydował się z nią zaręczyć. Ale sfinansował jej wyprawę do Anomalii Kolapsu. Kiepskim sprzętem...


### Ewakuacja z Serenit

* **uid:** 210512-ewakuacja-z-serenit, _numer względny_: 5
* **daty:** 0111-09-25 - 0111-10-04
* **obecni:** Aida Serenit, AK Serenit, Arianna Verlen, Bogdan Anatael, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Martyn Hiwasser, OE Falołamacz, OO Infernia, Roland Sowiński

Streszczenie:

Klaudia spowolniła Serenit, po czym Elena weszła zdalnym Entropikiem na pokład Falołamacza. Arianna zmanipulowała ludzi by dało się ich uratować. Samobójcza pintka zniechęciła Falołamacz do dalszej walki. Nie wszystkich udało się uratować (ale kapitana i arystokratę Sowińskich tak). Niestety, Infernia skończyła jako kosmiczny wrak i musiała wysłać SOS...

Aktor w Opowieści:

* Dokonanie:
    * uczestniczył w akcji kombinowanej Eternia - Aurum do zrobienia floty i badania anomalii kosmicznych. Uratowany przez Infernię z Falołamacza zanim stała mu się terminalna krzywda.


### Nie-Roland i niewolnicy na Valentinie

* **uid:** 220611-nie-roland-i-niewolnicy-na-valentinie, _numer względny_: 4
* **daty:** 0111-03-10 - 0111-03-12
* **obecni:** Ewelina Sowińska, Roland Sowiński

Streszczenie:

Roland został poproszony przez Ewelinę Sowińską o pomoc z dyskretną sprawą na Valentinie. Udał się tam jako swój agent, odkrył, że Ewelina weszła w handel niewolnikami i osoby które krzywdzą niewolników są załatwione 'klątwą'. Roland odkrył że problemem była klatka w Slave Pens - więc ją wzmocnił i odłożył na Valentinę by mocniej uciemiężyć łowców niewolników itp. (za skrzywdzenie tienów eternijskich). Po czym zdjął Ewelinę z niewolnictwa i przepiął ją na zarządzanie domu publicznego na Valentinie. Aha, założył kanał spedycyjny przerzutu lekkiego narkotyku XD.

Aktor w Opowieści:

* Dokonanie:
    * udając swojego agenta wzbudził strach w tien Ewelinie Sowińskiej na Valentinie i odstraszył ją od handlu niewolnikami, przepinając na domy publiczne. Wykrył Anomalię Krwi która mściła się za krzywdę niewolników, po czym ją wzmocnił i odłożył na Valentinę - by wrobić szlachetne części Orbitera że to oni. Nikt nie wie że tam był osobiście.
* Progresja:
    * na tym etapie ma już zaawansowany aparat szpiegów i fałszywych Masek - tożsamości, przedmiotów, niewielkich jednostek gdzie może być kimś innym.
    * modus operandi: wchodzi jako jeden ze swoich agentów. Raczej unika powiedzenia że jest Rolandem.
    * kanał spedycyjny proszku z błękitnej żaby Aurum, znajomości na Valentinie i Ewelina która z nim współpracuje.


### Szpieg szpiegowi szpiegiem

* **uid:** 211229-szpieg-szpiegowi-szpiegiem, _numer względny_: 3
* **daty:** 0111-01-03 - 0111-01-07
* **obecni:** Berenika Roldan, Estella Evans, Roland Sowiński

Streszczenie:

Zarówno Roland i Berenika zostali wysłani na stocznię Neotik - całkowicie niezależnie - przez swoich pracodawców, by dowiedzieć się, jakie projekty są aktualnie przeprowadzane przez Orbiter. Każde ze swoją legendą, zabrali się do pracy. Przeżyli napaść Gwardii Ochrony Orbitera, która zaatakowała stację, aby obronić ją przed napaścią z zewnątrz. Oboje osiągnęli swój cel wykradając dane. Przy okazji Berenika ożywiła Hestię stacji (nie zamierzone), a Roland nabawił się zainteresowania anomaliami.

Aktor w Opowieści:

* Dokonanie:
    * szpieg z Aurum jako boytoy Estelli Evans z Valentiny; fatalnie się skrada czy włamuje, ale jest szybki, groźny w walce i jest mistrzem aktorstwa. Zarządzał ryzykiem, walczył z intruzami i ogólnie tak zamieszał, że nikt o nic go nie podejrzewa. Zdobył informacje, których szukał.
* Progresja:
    * zainteresowanie anomaliami w wyniku tego, co znalazł w zdobytych materiałach


### Glizda, która leczy

* **uid:** 211120-glizda-ktora-leczy, _numer względny_: 2
* **daty:** 0108-04-03 - 0108-04-14
* **obecni:** Amanda Kajrat, Amelia Sowińska, Ernest Kajrat, Feliks Keksik, Justynian Diakon, Kacper Bankierz, Karol Pustak, Oliwia Lemurczak, Roland Sowiński, Sabina Kazitan, Sensacjusz Diakon, Stella Sowińska

Streszczenie:

Sensacjusz Diakon został wysłany na prowincję pod przykrywką Lekarza Rekinów, by dojść ze Stellą do tego czemu Amelia i Roland Sowińscy zachowują się nietypowo. Na miejscu okazało się, że Roland jest w stanie ostrej wojny z mafią (która nie traktuje tego poważnie; Kajrat się bawi z Rekinami) a Amelia po prostu nie w pełni sobie radzi i próbuje opanować sytuację. Roland jest Skażony Esuriit; Sensacjusz przetransportował go glizdą do Aurum i sam, uwięziony w bojowej formie, deeskalował konflikt z mafią. A Amelia jest tymczasowym dowódcą Rekinów...

Aktor w Opowieści:

* Dokonanie:
    * tak bardzo chciał pomóc wszystkim i pokonać mafię, że dał się wrobić Sabinie Kazitan i wkręcił się w Esuriit. Jego Skażenie doprowadziło do eskalacji działań z mafią i w konsekwencji wysłania tu Sensacjusza i Stelli. 23 lata.
* Progresja:
    * Skażenie Esuriit. Zmienia to jego Wzór trochę, nawet po wyleczeniu i regeneracji. Powrót do Aurum.


### Nie taki bezkarny młody tien

* **uid:** 211212-nie-taki-bezkarny-mlody-tien, _numer względny_: 1
* **daty:** 0101-04-10 - 0101-04-13
* **obecni:** Alojzy Lemurczak, Klara Gwozdnik, Nikczemniczka Diakon, Roland Sowiński

Streszczenie:

Roland x Klara, w Swawolniku. Żywe imprezy i swawole młodych tienów Aurum zakończyły się atakiem Stalowej Kompanii, która porwała 6 magów. Roland zebrał grupę by ich odbić, nie dogonił i odniósł mniejszy sukces - ale wpływając na dziką sentisieć uratowali ukochaną Klarę Rolanda przed porwaniem (i 2 innych magów) i Aurum ma jeńców.

Aktor w Opowieści:

* Dokonanie:
    * 16 lat. Próbuje być paladynem, choć trochę nie ma serca. Ma relację z Klarą Gwozdnik. Słucha się poleceń porywaczy, uspokaja i stabilizuje, ale jak tylko opuścili miejsce ataku to organizuje grupę pościgową. Gdy nie wyszło dogonienie, użyli dzikiej sentisieci do zatrzymania choć części napastników i ukochanej Klary. Popisuje się jak cholera.
* Progresja:
    * zmuszony do zerwania tajnego związku z Klarą Gwozdnik przez centralę Sowińskich. Jako posłuszny paladyn, wypełnił rozkaz. Ma w końcu 16 lat.
    * narobił sobie wrogów w Aurum we WSZYSTKICH rodach, łącznie ze swoim - odrzucił tych, co uznał za niekompetentnych i też nie dogonił porywaczy. Acz uratował 3 magów.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 11, @: 0112-02-23
    1. Primus    : 11, @: 0112-02-23
        1. Sektor Astoriański    : 11, @: 0112-02-23
            1. Astoria, Orbita    : 3, @: 0112-02-18
                1. Kontroler Pierwszy    : 3, @: 0112-02-18
                    1. Hangary Alicantis    : 1, @: 0111-11-27
            1. Astoria, Pierścień Zewnętrzny    : 3, @: 0112-02-23
                1. Poligon Stoczni Neotik    : 1, @: 0112-02-18
                1. Stocznia Neotik    : 3, @: 0112-02-23
                    1. Zewnętrzny dok ixioński    : 1, @: 0112-02-23
            1. Astoria    : 2, @: 0108-04-14
                1. Sojusz Letejski    : 2, @: 0108-04-14
                    1. Aurum    : 1, @: 0101-04-13
                        1. Pięciokąt Ichtis    : 1, @: 0101-04-13
                            1. Powiat Dzikiej Sieci    : 1, @: 0101-04-13
                            1. Powiat Pentalis    : 1, @: 0101-04-13
                                1. Swawolnik    : 1, @: 0101-04-13
                    1. Szczeliniec    : 1, @: 0108-04-14
                        1. Powiat Pustogorski    : 1, @: 0108-04-14
                            1. Podwiert    : 1, @: 0108-04-14
                                1. Dzielnica Luksusu Rekinów    : 1, @: 0108-04-14
                                    1. Serce Luksusu    : 1, @: 0108-04-14
                                        1. Lecznica Rannej Rybki    : 1, @: 0108-04-14
                                1. Las Trzęsawny    : 1, @: 0108-04-14
            1. Krwawa Baza Piracka    : 1, @: 0112-01-03
            1. Neikatis    : 1, @: 0112-02-11
                1. Dystrykt Lennet    : 1, @: 0112-02-11
            1. Stacja Valentina    : 1, @: 0111-03-12
                1. Krwawa Arena    : 1, @: 0111-03-12
                1. Slave Pens    : 1, @: 0111-03-12
                1. Speluna Smutny Rajder    : 1, @: 0111-03-12

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 7 | ((210512-ewakuacja-z-serenit; 210609-sekrety-kariatydy; 210616-nieudana-infiltracja-inferni; 210922-ostatnia-akcja-bohaterki; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211215-sklejanie-inferni-do-kupy)) |
| Elena Verlen         | 6 | ((210512-ewakuacja-z-serenit; 210616-nieudana-infiltracja-inferni; 210922-ostatnia-akcja-bohaterki; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211215-sklejanie-inferni-do-kupy)) |
| Eustachy Korkoran    | 6 | ((210512-ewakuacja-z-serenit; 210609-sekrety-kariatydy; 210616-nieudana-infiltracja-inferni; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211215-sklejanie-inferni-do-kupy)) |
| Klaudia Stryk        | 5 | ((210512-ewakuacja-z-serenit; 210616-nieudana-infiltracja-inferni; 210922-ostatnia-akcja-bohaterki; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii)) |
| Leona Astrienko      | 5 | ((210616-nieudana-infiltracja-inferni; 210922-ostatnia-akcja-bohaterki; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211215-sklejanie-inferni-do-kupy)) |
| Izabela Zarantel     | 4 | ((210616-nieudana-infiltracja-inferni; 210922-ostatnia-akcja-bohaterki; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii)) |
| Maria Naavas         | 3 | ((211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211215-sklejanie-inferni-do-kupy)) |
| OO Infernia          | 3 | ((210512-ewakuacja-z-serenit; 211124-prototypowa-nereida-natalii; 211215-sklejanie-inferni-do-kupy)) |
| Adam Szarjan         | 2 | ((211124-prototypowa-nereida-natalii; 211215-sklejanie-inferni-do-kupy)) |
| Kamil Lyraczek       | 2 | ((210616-nieudana-infiltracja-inferni; 211215-sklejanie-inferni-do-kupy)) |
| Klara Gwozdnik       | 2 | ((211117-porwany-trismegistos; 211212-nie-taki-bezkarny-mlody-tien)) |
| Marian Tosen         | 2 | ((210609-sekrety-kariatydy; 210616-nieudana-infiltracja-inferni)) |
| Martyn Hiwasser      | 2 | ((210512-ewakuacja-z-serenit; 210922-ostatnia-akcja-bohaterki)) |
| Aida Serenit         | 1 | ((210512-ewakuacja-z-serenit)) |
| AK Serenit           | 1 | ((210512-ewakuacja-z-serenit)) |
| Alojzy Lemurczak     | 1 | ((211212-nie-taki-bezkarny-mlody-tien)) |
| Amanda Kajrat        | 1 | ((211120-glizda-ktora-leczy)) |
| Amelia Sowińska      | 1 | ((211120-glizda-ktora-leczy)) |
| Antoni Kramer        | 1 | ((210922-ostatnia-akcja-bohaterki)) |
| Arkadia Verlen       | 1 | ((211124-prototypowa-nereida-natalii)) |
| Berenika Roldan      | 1 | ((211229-szpieg-szpiegowi-szpiegiem)) |
| Bogdan Anatael       | 1 | ((210512-ewakuacja-z-serenit)) |
| Diana d'Infernia     | 1 | ((211215-sklejanie-inferni-do-kupy)) |
| Ernest Kajrat        | 1 | ((211120-glizda-ktora-leczy)) |
| Estella Evans        | 1 | ((211229-szpieg-szpiegowi-szpiegiem)) |
| Ewelina Sowińska     | 1 | ((220611-nie-roland-i-niewolnicy-na-valentinie)) |
| Feliks Keksik        | 1 | ((211120-glizda-ktora-leczy)) |
| Flawia Blakenbauer   | 1 | ((210616-nieudana-infiltracja-inferni)) |
| Jamon Korab          | 1 | ((211117-porwany-trismegistos)) |
| Jolanta Kopiec       | 1 | ((210922-ostatnia-akcja-bohaterki)) |
| Justynian Diakon     | 1 | ((211120-glizda-ktora-leczy)) |
| Kacper Bankierz      | 1 | ((211120-glizda-ktora-leczy)) |
| Kalira d'Trismegistos | 1 | ((211117-porwany-trismegistos)) |
| Karol Pustak         | 1 | ((211120-glizda-ktora-leczy)) |
| Kasandra Destrukcja Diakon | 1 | ((211215-sklejanie-inferni-do-kupy)) |
| Mira Anastel         | 1 | ((211117-porwany-trismegistos)) |
| Natalia Aradin       | 1 | ((211124-prototypowa-nereida-natalii)) |
| Nikczemniczka Diakon | 1 | ((211212-nie-taki-bezkarny-mlody-tien)) |
| OE Falołamacz        | 1 | ((210512-ewakuacja-z-serenit)) |
| Olgierd Drongon      | 1 | ((210922-ostatnia-akcja-bohaterki)) |
| Oliwia Lemurczak     | 1 | ((211120-glizda-ktora-leczy)) |
| OO Kariatyda         | 1 | ((210609-sekrety-kariatydy)) |
| OO Opresor           | 1 | ((210616-nieudana-infiltracja-inferni)) |
| OO Tivr              | 1 | ((211117-porwany-trismegistos)) |
| OO Żelazko           | 1 | ((210922-ostatnia-akcja-bohaterki)) |
| Rafael Galwarn       | 1 | ((210609-sekrety-kariatydy)) |
| Sabina Kazitan       | 1 | ((211120-glizda-ktora-leczy)) |
| SC Trismegistos      | 1 | ((211117-porwany-trismegistos)) |
| Sensacjusz Diakon    | 1 | ((211120-glizda-ktora-leczy)) |
| Stella Sowińska      | 1 | ((211120-glizda-ktora-leczy)) |
| Wawrzyn Rewemis      | 1 | ((211124-prototypowa-nereida-natalii)) |
| Zygfryd Maus         | 1 | ((211117-porwany-trismegistos)) |
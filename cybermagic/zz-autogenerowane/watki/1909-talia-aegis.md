# Talia Aegis
## Identyfikator

Id: 1909-talia-aegis

## Sekcja Opowieści

### Arystokraci na Trzęsawisku

* **uid:** 200414-arystokraci-na-trzesawisku, _numer względny_: 10
* **daty:** 0110-07-31 - 0110-08-01
* **obecni:** Franciszek Leszczowik, Ignacy Myrczek, Mariusz Trzewń, Pięknotka Diakon, Sabina Kazitan, Strażniczka Alair, Talia Aegis

Streszczenie:

Franciszek z Aurum dostosował Sabinę do swoich upodobań jako seksbombę. Sabina zdecydowała się go zniszczyć - doprowadziła go do Wysysacza Esuriit i wskazała, że na Trzęsawisku może zdobyć coś co mu się przyda. Franciszek stracił część świty na Trzęsawisku; gdyby nie Pięknotka, zginąłby tam. A tak tylko skończył z traumą a Sabinie się upiekło - roboty społeczne na 2 tygodnie.

Aktor w Opowieści:

* Dokonanie:
    * okazuje się, że pomogła Tymonowi sformować Strażniczkę Alair i doprowadzić ją do pełnego funkcjonowania.


### Krucjata Chevaleresse

* **uid:** 200202-krucjata-chevaleresse, _numer względny_: 9
* **daty:** 0110-07-20 - 0110-07-23
* **obecni:** Ataienne, Damian Orion, Diana Tevalier, Keraina d'Orion, Malictrix d'Itaran, Mariusz Trzewń, Mateusz Kardamacz, Minerwa Metalia, Pięknotka Diakon, Talia Aegis, Tomasz Tukan

Streszczenie:

Chevaleresse wykradła broń Alana i uzbroiła Liberitias, gdzie działa też Ataienne przeciw Kardamaczowi, Grzymościowi i krzywdzicielowi Alana. Pięknotka doszła do serca problemu i materii, acz potrzebowała wsparcia Orbitera - dokładniej, Kerainy. Dzięki Kerainie wprowadziła Malictrix do Kompleksu Itaran. Teraz to tylko kwestia czasu aż Grzymość zostanie zniszczony.

Aktor w Opowieści:

* Dokonanie:
    * nie stoi za działaniami przeciw Pustogorowi ani Grzymościowi tym razem; wyjaśniła Pięknotce wymagania na TAI 3. Przyjrzy się "dziwnej TAI" w okolicy.


### Kto wrobił Alana

* **uid:** 190830-kto-wrobil-alana, _numer względny_: 8
* **daty:** 0110-05-30 - 0110-06-01
* **obecni:** Alan Bartozol, Diana Tevalier, Karolina Erenit, Pięknotka Diakon, Talia Aegis, Wojciech Zermann

Streszczenie:

Ktoś wrabia Alana. Okazało się, że to nie Alan jest celem - to Chevaleresse. Podpadła kilku magom z Aurum w vircie i stwierdzili że ją nastraszą - ale najpierw muszą odsunąć Alana na bok. Pięknotka z Tymonem zastawili pułapkę współpracując z Chevaleresse jako przynętą. Udało się - magowie Aurum wpadli. Przy okazji - Karolina pomagała magom Aurum bo uznała, że to pomoże terminusom i Chevaleresse najbardziej. Karolina bowiem nienawidzi arystokracji magów.

Aktor w Opowieści:

* Dokonanie:
    * chciała poważnie uszkodzić Alana za scrambler AI, ale jako, że to było podłożone... odpuściła. Nie będzie atakować niewinnego.


### Migświatło psychotroniczek

* **uid:** 190828-migswiatlo-psychotroniczek, _numer względny_: 7
* **daty:** 0110-02-07 - 0110-02-09
* **obecni:** Artur Michasiewicz, Ernest Kajrat, Marek Puszczok, Minerwa Metalia, Pięknotka Diakon, Talia Aegis, Tymon Grubosz

Streszczenie:

Gdy Kajrat jest w więzieniu, nikt nie kontroluje przepływu technologii noktiańskiej. Talia ostrzegła Pięknotkę, że do holo-walki AI dodała subturingowe BIA klasy Szponowiec, z żądania gościa od Grzymościa. Okazało się, że to niegroźny hazard jest - ale są tam dwie dziwne walczące AI: BIA oraz militarna, stworzona przez Minerwę. Pięknotka chciała tylko zabić BIA, ale przypadkowo z Minerwą wysadziły Arenę Migświatła. Tymon zabrał Minerwie jej TAI. Powiedział, że jeszcze nie czas by takie rzeczy robiła.

Aktor w Opowieści:

* Dokonanie:
    * przyszła do Pięknotki bo nie chce by subturingowa BIA którą musiała zbudować dla Puszczoka stała się zagrożeniem.


### Ostatnia misja Tarna

* **uid:** 190928-ostatnia-misja-tarna, _numer względny_: 6
* **daty:** 0110-01-26 - 0110-01-27
* **obecni:** BIA Tarn, Eustachy Mrownik, Hestia d'Tiamenat, Pedro Ronfak, Talia Aegis, Wiktor Satarail

Streszczenie:

Wiktor Satarail poproszony przez Pięknotkę skupił się na zniszczeniu Tarna. Zaatakował Tiamenat subtelnie, skażeniem biologicznym i hackerem uszkodził percepcję TAI Hestii. Tarn się obudził i odparł atak Wiktora; w wyniku ten go Zatruł. Ale Tarn wynegocjował życie - bo nikomu nie zrobił krzywdy. Wiktor się na to zgodził, jeśli Tarn opuści ten teren raz na zawsze - poleci w kosmos.

Aktor w Opowieści:

* Dokonanie:
    * robiła wszystko by uratować Tarna i... w sumie się udało. Odkażała skażeńców Wiktora, budowała antidotum itp.


### Rozpaczliwe ratowanie BII

* **uid:** 190827-rozpaczliwe-ratowanie-bii, _numer względny_: 5
* **daty:** 0110-01-17 - 0110-01-20
* **obecni:** BIA Tarn, Ernest Kajrat, Marek Puszczok, Mariusz Trzewń, Pięknotka Diakon, Sławomir Niejadek, Talia Aegis, Tymon Grubosz

Streszczenie:

Talia próbowała utrzymać przy życiu BIA 3 generacji. Nie mając surowców, użyła tej BIA do zdobycia rzeczy odżywczych z Tiamenat - zrzucając winę na Wiktora Sataraila. Pięknotka poszła za śladem i gdy dotarła do Talii, Kajrat wziął na siebie winę (za co Pięknotka go aresztowała). Następnie poprosiła Wiktora Sataraila by ten ochronił swoje dobre imię - i zniszczył śmiertelnie niebezpieczną BIA.

Aktor w Opowieści:

* Dokonanie:
    * rozpaczliwie chciała uratować BIA 3 generacji; udawała, że to Wiktor Satarail zaatakował Tiamenat - a to była ona. Uratowana przed więzieniem przez Ernesta Kajrata.


### Sekret Samanty Arienik

* **uid:** 220119-sekret-samanty-arienik, _numer względny_: 4
* **daty:** 0085-07-26 - 0085-07-28
* **obecni:** Arazille, Błażej Arienik, Felicjan Szarak, Klaudia Stryk, Ksenia Kirallen, Maria Arienik, Samanta Arienik, Sławomir Arienik, Talia Aegis, Urszula Arienik

Streszczenie:

Gdy doszło do katastrofy w Podwiercie i pojawiły się dziwne karaluchy magiczne to Klaudia, Ksenia i Felicjan ruszyli do pomocy - wraz z dziewczyną Felicjana, Samantą. Samanta uruchomiła sprzęt Arieników by pomóc. Podczas operacji czyszczenia okazało się, że z Samantą coś jest nie tak; "psychotronika uszkodzona"? Samanta i Klaudia szybko ruszyły do Talii Aegis - tylko ona może pomóc znaleźć odpowiedź. Okazało się, że Samanta nie żyje od dawna; z rodu Arienik przetrwał tylko Sławomir i Ula. Talii udało się przenieść ród Arienik w "żywe TAI". Sławomir umarł. Głową rodziny jest teraz młoda Ula, z pomocą trzech TAI - kiedyś jej rodziny. Wpływ Arazille - odepchnięty. Jednak podczas naprawy Samanty Coś Się Stało z seksbotami nad którymi pracowała Talia dla mafii...

Aktor w Opowieści:

* Dokonanie:
    * podjęła się naprawy Samanty Arienik i przeniesienie ją w TAI. Ustabilizowała jej pamięć, usunęła inhibitory i przeniosła do "bycia żywą TAI". Uratowała WSZYSTKICH Arieników którzy byli zamknięci w miragentach. Wielki sukces. Acz coś zepsuła z seksbotami nad którymi pracowała dla mafii...


### Czarodziejka, która jednak może się zabić

* **uid:** 211019-czarodziejka-ktora-jednak-moze-sie-zabic, _numer względny_: 3
* **daty:** 0084-12-20 - 0084-12-24
* **obecni:** Arnulf Poważny, Klaudia Stryk, Ksenia Kirallen, Mariusz Trzewń, Maryla Koternik, Talia Aegis, Teresa Mieralit

Streszczenie:

Nastroje antynoktiańskie się nasilają. Pojawiły się seksboty do bicia, w kształcie noktian i noktianek (co strasznie zestresowało Teresę). Klaudia zaproponowała Arnulfowi powiększenie kadry o Weteranów i przejmując to prawie się zarżnęła (uratował ją Trzewń). Maryla szuka informacji o Teresie - szuka ukrytego noktianina na AMZ, ale Trzewń wykluczył Teresę dzięki dokumentom sformowanym przez Arnulfa. Klaudia i Ksenia pracują nad petycją, by stworzyć Dom Weteranów Noctis.

Aktor w Opowieści:

* Dokonanie:
    * ochraniana przez jakichś typów (mafię), konfiguruje seksboty na "mroczniejsze przyjemności".
* Progresja:
    * ma opiekę i ochronę jakichś typów (mafii) - chronią ją przed napaścią i demonstrantami. Zajmuje się seksbotami.


### Ukryta wychowanka Arnulfa

* **uid:** 211010-ukryta-wychowanka-arnulfa, _numer względny_: 2
* **daty:** 0084-12-11 - 0084-12-12
* **obecni:** Arnulf Poważny, Felicjan Szarak, Klaudia Stryk, Ksenia Kirallen, Sasza Morwowiec, Strażniczka Alair, Talia Aegis, Teresa Mieralit, Tymon Grubosz

Streszczenie:

Dyrektor Arnulf Poważny ma wychowankę - Teresę Mieralit, noktiańską piętnastonastolatkę która mieszka w AMZ. Strażniczka Alair ją wykryła i wpadła w pętlę - komponenty TAI uznały ją za potencjalne zagrożenie, BIA za osobę do ochrony. Strażniczka wycofała drony do osłony AI Core i zaatakowały Teresę. Klaudia zorientowała się w problemie, obudziła hipernetem Arnulfa. Arnulf osłonił Teresę, Klaudia wezwała Talię i w grupie udało im się opanować niesforne TAI (bo Klaudia zanomalizowała konwertery energii Strażniczki, która czerpie energię z Trzęsawiska).

Aktor w Opowieści:

* Dokonanie:
    * przed jej domem pojawiają się jakieś manifestacje i próby napaści nocą..? Tak czy inaczej, dowiedziawszy się o problemach Strażniczki natychmiast w nocy się zebrała by jej pomóc. Jej wezwanie wzbudziło zaskoczenie Ksenii i Felicjana.


### Nowa Strażniczka AMZ

* **uid:** 210926-nowa-strazniczka-amz, _numer względny_: 1
* **daty:** 0084-06-14 - 0084-06-26
* **obecni:** Albert Kalandryk, Arnulf Poważny, Klaudia Stryk, Ksenia Kirallen, Mariusz Trzewń, Strażniczka Alair, Talia Aegis, Tymon Grubosz

Streszczenie:

Klaudia - młoda grzeczna administratorka AMZ - przypadkiem odkrywa konspirację dyrektora, terminusa i noktianki by uratować umierającą Eszarę. Po konfrontacji z dyrektorem pomaga im by wszystko działało. Trzewń uniknął amnestyków.

Aktor w Opowieści:

* Dokonanie:
    * 47 lat; ekspert od TAI i BIA. Pomaga Arnulfowi i Tymonowi zrobić Strażniczkę Alair łącząc technologie BIA z Eszarą. W zamian za to - oni pomagają jej uratować jak najwięcej BIA i TAI; składowane są w Złomiarium w AMZ.
* Progresja:
    * uczy Klaudię Stryk o TAI/BIA, wykorzystywaniu magitechu w stylu noktiańskim i odnośnie wolności AI.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 10, @: 0110-08-01
    1. Primus    : 10, @: 0110-08-01
        1. Sektor Astoriański    : 10, @: 0110-08-01
            1. Astoria    : 10, @: 0110-08-01
                1. Sojusz Letejski    : 10, @: 0110-08-01
                    1. Szczeliniec    : 10, @: 0110-08-01
                        1. Powiat Jastrzębski    : 1, @: 0110-07-23
                            1. Jastrząbiec, okolice    : 1, @: 0110-07-23
                                1. Klinika Iglica    : 1, @: 0110-07-23
                                    1. Kompleks Itaran    : 1, @: 0110-07-23
                                1. TechBunkier Sarrat    : 1, @: 0110-07-23
                            1. Kalbark    : 1, @: 0110-07-23
                                1. Escape Room Lustereczko    : 1, @: 0110-07-23
                        1. Powiat Pustogorski    : 10, @: 0110-08-01
                            1. Podwiert    : 1, @: 0085-07-28
                            1. Pustogor    : 1, @: 0084-06-26
                                1. Zamek Weteranów    : 1, @: 0084-06-26
                            1. Zaczęstwo    : 10, @: 0110-08-01
                                1. Akademia Magii, kampus    : 4, @: 0110-08-01
                                    1. Akademik    : 3, @: 0084-12-24
                                    1. Budynek Centralny    : 1, @: 0084-12-12
                                    1. Domek dyrektora    : 1, @: 0084-12-24
                                    1. Złomiarium    : 1, @: 0084-06-26
                                1. Arena Migświatła    : 2, @: 0110-02-09
                                1. Kompleks Tiamenat    : 2, @: 0110-01-27
                                    1. Budynek Wołkowca    : 1, @: 0110-01-27
                                    1. Centralny Biolab    : 1, @: 0110-01-27
                                    1. Centrum Dowodzenia    : 1, @: 0110-01-27
                                1. Nieużytki Staszka    : 1, @: 0084-06-26
                        1. Trzęsawisko Zjawosztup    : 1, @: 0110-08-01

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Mariusz Trzewń       | 5 | ((190827-rozpaczliwe-ratowanie-bii; 200202-krucjata-chevaleresse; 200414-arystokraci-na-trzesawisku; 210926-nowa-strazniczka-amz; 211019-czarodziejka-ktora-jednak-moze-sie-zabic)) |
| Pięknotka Diakon     | 5 | ((190827-rozpaczliwe-ratowanie-bii; 190828-migswiatlo-psychotroniczek; 190830-kto-wrobil-alana; 200202-krucjata-chevaleresse; 200414-arystokraci-na-trzesawisku)) |
| Klaudia Stryk        | 4 | ((210926-nowa-strazniczka-amz; 211010-ukryta-wychowanka-arnulfa; 211019-czarodziejka-ktora-jednak-moze-sie-zabic; 220119-sekret-samanty-arienik)) |
| Ksenia Kirallen      | 4 | ((210926-nowa-strazniczka-amz; 211010-ukryta-wychowanka-arnulfa; 211019-czarodziejka-ktora-jednak-moze-sie-zabic; 220119-sekret-samanty-arienik)) |
| Tymon Grubosz        | 4 | ((190827-rozpaczliwe-ratowanie-bii; 190828-migswiatlo-psychotroniczek; 210926-nowa-strazniczka-amz; 211010-ukryta-wychowanka-arnulfa)) |
| Arnulf Poważny       | 3 | ((210926-nowa-strazniczka-amz; 211010-ukryta-wychowanka-arnulfa; 211019-czarodziejka-ktora-jednak-moze-sie-zabic)) |
| Strażniczka Alair    | 3 | ((200414-arystokraci-na-trzesawisku; 210926-nowa-strazniczka-amz; 211010-ukryta-wychowanka-arnulfa)) |
| BIA Tarn             | 2 | ((190827-rozpaczliwe-ratowanie-bii; 190928-ostatnia-misja-tarna)) |
| Diana Tevalier       | 2 | ((190830-kto-wrobil-alana; 200202-krucjata-chevaleresse)) |
| Ernest Kajrat        | 2 | ((190827-rozpaczliwe-ratowanie-bii; 190828-migswiatlo-psychotroniczek)) |
| Felicjan Szarak      | 2 | ((211010-ukryta-wychowanka-arnulfa; 220119-sekret-samanty-arienik)) |
| Marek Puszczok       | 2 | ((190827-rozpaczliwe-ratowanie-bii; 190828-migswiatlo-psychotroniczek)) |
| Minerwa Metalia      | 2 | ((190828-migswiatlo-psychotroniczek; 200202-krucjata-chevaleresse)) |
| Teresa Mieralit      | 2 | ((211010-ukryta-wychowanka-arnulfa; 211019-czarodziejka-ktora-jednak-moze-sie-zabic)) |
| Alan Bartozol        | 1 | ((190830-kto-wrobil-alana)) |
| Albert Kalandryk     | 1 | ((210926-nowa-strazniczka-amz)) |
| Arazille             | 1 | ((220119-sekret-samanty-arienik)) |
| Artur Michasiewicz   | 1 | ((190828-migswiatlo-psychotroniczek)) |
| Ataienne             | 1 | ((200202-krucjata-chevaleresse)) |
| Błażej Arienik       | 1 | ((220119-sekret-samanty-arienik)) |
| Damian Orion         | 1 | ((200202-krucjata-chevaleresse)) |
| Eustachy Mrownik     | 1 | ((190928-ostatnia-misja-tarna)) |
| Franciszek Leszczowik | 1 | ((200414-arystokraci-na-trzesawisku)) |
| Hestia d'Tiamenat    | 1 | ((190928-ostatnia-misja-tarna)) |
| Ignacy Myrczek       | 1 | ((200414-arystokraci-na-trzesawisku)) |
| Karolina Erenit      | 1 | ((190830-kto-wrobil-alana)) |
| Keraina d'Orion      | 1 | ((200202-krucjata-chevaleresse)) |
| Malictrix d'Itaran   | 1 | ((200202-krucjata-chevaleresse)) |
| Maria Arienik        | 1 | ((220119-sekret-samanty-arienik)) |
| Maryla Koternik      | 1 | ((211019-czarodziejka-ktora-jednak-moze-sie-zabic)) |
| Mateusz Kardamacz    | 1 | ((200202-krucjata-chevaleresse)) |
| Pedro Ronfak         | 1 | ((190928-ostatnia-misja-tarna)) |
| Sabina Kazitan       | 1 | ((200414-arystokraci-na-trzesawisku)) |
| Samanta Arienik      | 1 | ((220119-sekret-samanty-arienik)) |
| Sasza Morwowiec      | 1 | ((211010-ukryta-wychowanka-arnulfa)) |
| Sławomir Arienik     | 1 | ((220119-sekret-samanty-arienik)) |
| Sławomir Niejadek    | 1 | ((190827-rozpaczliwe-ratowanie-bii)) |
| Tomasz Tukan         | 1 | ((200202-krucjata-chevaleresse)) |
| Urszula Arienik      | 1 | ((220119-sekret-samanty-arienik)) |
| Wiktor Satarail      | 1 | ((190928-ostatnia-misja-tarna)) |
| Wojciech Zermann     | 1 | ((190830-kto-wrobil-alana)) |
# Adela Kirys
## Identyfikator

Id: 1810-adela-kirys

## Sekcja Opowieści

### Mimik śni o Esuriit

* **uid:** 190527-mimik-sni-o-esuriit, _numer względny_: 12
* **daty:** 0110-05-03 - 0110-05-05
* **obecni:** Adela Kirys, Erwin Galilien, Mirela Satarail, Pięknotka Diakon, Wiktor Satarail

Streszczenie:

Mirela uciekła Wiktorowi Satarailowi z Trzęsawiska, więc ów poprosił o pomoc Pięknotkę. Mirela wyczuła Esuriit w Czółenku i chciała nakarmić głód; Pięknotka jednak dała radę ją znaleźć i nie dopuścić do Pożarcia Mireli przez Esuriit; zamiast tego ściągnęła oddział terminusów którzy sami rozwiązali problem. No i wycofała Mirelę z powrotem do Wiktora.

Aktor w Opowieści:

* Dokonanie:
    * chce być taka jak Wiktor, ale ów ją zbywa; dała Pięknotce trochę swoich kosmetyków by Mirela się jej nie bała. Aha, zaprzyjaźniła się z Mirelą.


### Szczur który chroni

* **uid:** 190505-szczur-ktory-chroni, _numer względny_: 11
* **daty:** 0110-04-18 - 0110-04-20
* **obecni:** Adela Kirys, Alan Bartozol, Ernest Kajrat, Kasjan Czerwoczłek, Krystian Namałłek, Lucjusz Blakenbauer, Oliwia Namałłek, Pięknotka Diakon, Wiktor Satarail

Streszczenie:

Zaczęło się od Skażenia proszkiem kralotycznym Nieużytków Staszka. Potem Pięknotka współpracując z mafią doszła do tego, że Adela i terminus stoją za tą sprawą. Potem Pięknotka uratowała Adelę i terminusa wprowadzając szturmowych terminusów Pustogoru na mafię. Gdy "Cień wyrwał się spod kontroli", Alan ją strzelił z działa strumieniowego. I tak skończyło się rumakowanie na tydzień - ale wszyscy są bezpieczni.

Aktor w Opowieści:

* Dokonanie:
    * chciała ratować Krystiana i Oliwię i zmontowała najdzikszą konspirację proszku kralotycznego, która się rozpadła na jej oczach. Trafiła na Trzęsawisko do Wiktora.
* Progresja:
    * dostaje lekcje szamaństwa, zielarstwa i alchemii u Wiktora Sataraila, który przy okazji pokazuje jej jak ewoluować w coś sensownego


### Czarodziejka z woli Saitaera

* **uid:** 190202-czarodziejka-z-woli-saitaera, _numer względny_: 10
* **daty:** 0110-01-31 - 0110-02-04
* **obecni:** Adela Kirys, Karolina Erenit, Minerwa Metalia, Pięknotka Diakon, Saitaer, Sławomir Muczarek, Tymon Grubosz, Wojtek Kurczynos

Streszczenie:

Wojtek Kurczynos ZNOWU dobierał się do Karoliny Erenit. Przez machinacje Sławka i Adeli Saitaer dokonał transfuzji mocy magicznej od Wojtka do Karoliny. Pięknotka drastycznie ukrywała wszystkie wpływy wszystkich osób, by ucierpiał tylko winny Wojtek. Nawet współpracowała z Saitaerem, by terroformizować Wojtka. Wszystko, by Karolina zachowała moc magiczną. ALE - doprowadziła do tego, że spojrzeli na Karolinę Mausowie. W ten sposób Karradrael uniemożliwił hold Saitaera na Karolinie.

Aktor w Opowieści:

* Dokonanie:
    * bardzo jej zależało by pomóc Karolinie Erenit. Przekonała Sławka i dała mu eliksir. Czyli - drugi raz zrobiła ten sam błąd, eliksir na nieznany target.
* Progresja:
    * sporo z tematów powiązanych z utratą przez Wojtka mocy zostało jej przypisane. Jest formalne śledztwo w jej kierunku.


### Skorpipedy królewskiego xirathira

* **uid:** 190119-skorpipedy-krolewskiego-xirathira, _numer względny_: 9
* **daty:** 0110-01-13 - 0110-01-14
* **obecni:** Adela Kirys, Olga Myszeczka, Pięknotka Diakon, Sławomir Muczarek, Waldemar Mózg, Wiktor Satarail

Streszczenie:

Czerwone Myszy poprosiły Pięknotkę o pomoc - pojawiły się na ich terenie niebezpieczne skorpipedy i działania Adeli jedynie pogarszają sprawę. Okazało się, że to działania Wiktora Sataraila, który chce pomóc Oldze Myszeczce w utrzymaniu terenu i odparciu harrasserów z Myszy. Pięknotka odkręciła sprawę i trochę poprawiła relację z Wiktorem. Acz jest sceptyczna wobec samych Myszy...

Aktor w Opowieści:

* Dokonanie:
    * lekko nadwyrężyła zaufanie Alana i poprosiła o próbkę skorpipeda by zrobić przeciw niemu truciznę. Niestety, Wiktor jest o kilka klas lepszy od Adeli.
* Progresja:
    * jej poczucie własnej wartości i umiejętności biomantycznych jest już naprawdę niskie.


### Chrońmy Karolinę przed uczniami

* **uid:** 190113-chronmy-karoline-przed-uczniami, _numer względny_: 8
* **daty:** 0110-01-04 - 0110-01-05
* **obecni:** Adela Kirys, Arnulf Poważny, Karolina Erenit, Liliana Bankierz, Napoleon Bankierz, Pięknotka Diakon, Teresa Mieralit

Streszczenie:

Karla chciała pozbyć się Pięknotki jeszcze raz. Wysłała na papierkową robotę do Szkoły Magów w Zaczęstwie. Tam okazało się, że kandydat na terminusa (Napoleon) próbował uratować człowieka, Karolinę Erenit przed innymi uczniami szkoły magów. Niestety, eliksir który zamówił u Adeli miał efekty uboczne. Pięknotka gładko to rozwiązała i nikt nie miał problemów. Poza tym, że Karolina nadal nie jest chroniona.

Aktor w Opowieści:

* Dokonanie:
    * stworzyła eliksir dla Karoliny, proszona przez Napoleona. Niestety, eliksir wszedł w interakcję z polem magicznym Karoliny (nie wiedziała że Karolina ma takie pole).
* Progresja:
    * traci trochę pewności siebie - widzi, że mogła zostać pozwana za to co zrobiła sama z tym eliksirem dla Karoliny Erenit (bez sprawdzenia osobiście)


### A może pustogorska mafia?

* **uid:** 190106-a-moze-pustogorska-mafia, _numer względny_: 7
* **daty:** 0109-12-23 - 0109-12-25
* **obecni:** Adela Kirys, Amadeusz Sowiński, Kasjan Czerwoczłek, Pięknotka Diakon, Roland Grzymość, Tadeusz Rupczak, Waldemar Mózg

Streszczenie:

Adela poprosiła Pięknotkę o uratowanie młodego podkochującego się w niej Kasjana od wydalenia z Pustogoru - to on zrobił atak na Pięknotkę. Niestety, nie dało się tego zrobić - ale Pięknotka oddała go Pietro w Cieniaszczycie. Przy okazji, Pięknotka nie ma surowców na bagna, zapewniła utrzymanie relacji z Czerwonymi Myszami i pozyskanie sponsorów.

Aktor w Opowieści:

* Dokonanie:
    * która dla uratowania Kasjana przyszła prosić Pięknotkę. Niestety, nie dało się go uratować - ale pomogła go przekonać, by nie szedł w mafię a w Cieniaszczyt.


### Turyści na Trzęsawisku

* **uid:** 190105-turysci-na-trzesawisku, _numer względny_: 6
* **daty:** 0109-12-21 - 0109-12-23
* **obecni:** Alan Bartozol, Pięknotka Diakon, Wiktor Satarail

Streszczenie:

Grupa turystów spoza Pustogoru odwiedziała Trzęsawisko wraz z trzema łowcami nagród. Pięknotka i Alan poszli ich ratować. W samą porę - sygnał SOS pokazał, że jest już późno. Udało się wszystkich uratować, za co... Pięknotce ktoś zdemolował gabinet. Pięknotka i Alan doszli do pewnego porozumienia w sprawie Adeli - znają jej imię. Acz chyba Alan się o nią martwi.

Aktor w Opowieści:

* Dokonanie:
    * AKTOR NIEOBECNY NA SESJI
* Progresja:
    * jest nią zainteresowana mafia i ciemniejsze interesy.


### Morderczyni-jednej-plotki

* **uid:** 190101-morderczyni-jednej-plotki, _numer względny_: 5
* **daty:** 0109-12-12 - 0109-12-16
* **obecni:** Aleksander Iczak, Erwin Galilien, Karol Szurnak, Olaf Zuchwały, Pięknotka Diakon, Teresa Mieralit

Streszczenie:

Na temat Pięknotki i jej salonu zaczęto rozpuszczać nieprzyjemne plotki. Pięknotka zlokalizowała jedno ze źródeł i je pokazowo zniszczyła, zmuszając maga do przepraszania i płaczu na kolanach. Dodatkowo, Czerwone Myszy oraz Dare Shiver zaczęli interesować się Pięknotką i jej salonem. A sama Pięknotka przecięła "największą nemesis" Adeli Kirys.

Aktor w Opowieści:

* Dokonanie:
    * AKTOR NIEOBECNY NA SESJI
* Progresja:
    * jest przerażona Pięknotką. Boi się stanąć przeciwko niej. Nie zrobi już tego. Widzi, że to nie ta liga. Przestała mówić o "największej nemesis".


### Terminuska czy kosmetyczka?

* **uid:** 181027-terminuska-czy-kosmetyczka, _numer względny_: 4
* **daty:** 0109-10-07 - 0109-10-11
* **obecni:** Adela Kirys, Arnulf Poważny, Brygida Maczkowik, Ignacy Myrczek, Minerwa Metalia, Pięknotka Diakon

Streszczenie:

Jednoczesny atak na reputację Ateny (przez cybergrzyby) oraz wielki konkurs kosmetyczny w Pustogorze wpłynęły stosunkowo źle na sytuację Pięknotki. Nie rozdwoiła się, ale obie rzeczy udało jej się rozwiązać - acz jest wycieńczona. Odkryła konspirację wymierzoną przeciwko Atenie - ale nie wie kto i gdzie próbuje Atenę usunąć.

Aktor w Opowieści:

* Dokonanie:
    * dała z siebie wszystko by wygrać konkurs na najlepszą kosmetyczkę - i o włos! Ale Pięknotka była jednak lepsza. Za to, Adela jest druga.
* Progresja:
    * drugie miejsce kosmetyczek w Pustogorze. Ma klientów i nie grozi jej już upadek czy katastrofa. Przekonana, że Pięknotka wygrała przez użycie Ateny.


### Decyzja Minerwy

* **uid:** 181024-decyzja-minerwy, _numer względny_: 3
* **daty:** 0109-10-01 - 0109-10-04
* **obecni:** Adam Szarjan, Adela Kirys, Lilia Ursus, Minerwa Metalia, Pięknotka Diakon

Streszczenie:

Atena i Erwin są w ruinie. Tymczasem servar Erwina wyrwał się na wolność. Okazało się, że Minerwa - dawna miłość Erwina - powróciła. Adam próbował ją zneutralizować a Pięknotka - uratować. Skończyło się na tym, że Minerwa, Nutka i terrorform współistnieją w servarze Erwina, Adama boli głowa a Pięknotka cieszy się, że tymczasowo to nie jej problem. Mistrzyni psychotroniki wróciła.

Aktor w Opowieści:

* Dokonanie:
    * nieco zaplątana w tą sprawę; dała radę zrobić makijaż servarowi, po czym poprawiła na bardzo udany makijaż (przebijając Pięknotkę)


### Dwa tygodnie szkoły

* **uid:** 180929-dwa-tygodnie-szkoly, _numer względny_: 2
* **daty:** 0109-09-17 - 0109-09-19
* **obecni:** Adela Kirys, Arnulf Poważny, Atena Sowińska, Erwin Galilien, Felicja Melitniek, Ignacy Myrczek, Miedwied Zajcew, Napoleon Bankierz, Pięknotka Diakon, Roland Grzymość

Streszczenie:

Felicja uczy się w szkole magów, ale nie ma przyjaciół i przez to zachowuje się chaotycznie. Erwin Galilien wpakował się w kłopoty i przegrał power suit "Nutkę" w Kasynie; ma przynieść niebezpieczne grzyby. Uczniowie ze szkoły magów też chodzą do tego Kasyna. Połączone siły Zespołu i Ateny wyplątały Erwina z kłopotów i utrudniły Kasynu zwabianie młodych uczniów ze szkoły magów.

Aktor w Opowieści:

* Dokonanie:
    * współpracuje z kasynem i Rolandem Grzymościem. Jej rola jest jeszcze nieznana.


### Komary i kosmetyki

* **uid:** 180815-komary-i-kosmetyki, _numer względny_: 1
* **daty:** 0109-09-02 - 0109-09-05
* **obecni:** Adela Kirys, Erwin Galilien, Pięknotka Diakon

Streszczenie:

Adela Kirys wpadła w kłopoty. Stworzyła komary napromieniowujące ludzi na terenie Czystym. Sęk w tym, że poszlaki wskazywały na Pięknotkę. Pięknotka ostrzeżona przez Galiliena odkryła problem - po czym poprosiła Erwina o Skażenie kremu tworzącego te komary (włam + podrzut). Sukces - Adela musiała wycofać całą partię a reputacja Pięknotki nie ucierpiała. A i Nurek Szczeliny się przydał.

Aktor w Opowieści:

* Dokonanie:
    * była uczennica Pięknotki powtarzająca jej błędy ale nadzieja sprawia, że ignoruje potencjalne ryzyko. Wpadła w kłopoty, bo musiała wszystko wycofać.
* Progresja:
    * jej zapowiadająca się dobrze kariera kosmetyczki została uszkodzona przez konieczność wycofania partii kremu (bo tworzył magiczne komary) (bo Erwin go zatruł)


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 10, @: 0110-05-05
    1. Primus    : 10, @: 0110-05-05
        1. Sektor Astoriański    : 10, @: 0110-05-05
            1. Astoria    : 10, @: 0110-05-05
                1. Sojusz Letejski    : 10, @: 0110-05-05
                    1. Szczeliniec    : 10, @: 0110-05-05
                        1. Powiat Pustogorski    : 10, @: 0110-05-05
                            1. Czarnopalec    : 1, @: 0110-01-14
                                1. Pusta Wieś    : 1, @: 0110-01-14
                            1. Czółenko    : 1, @: 0110-05-05
                                1. Bunkry    : 1, @: 0110-05-05
                                1. Tancbuda    : 1, @: 0110-05-05
                            1. Podwiert    : 2, @: 0110-04-20
                                1. Bastion Pustogoru    : 1, @: 0110-04-20
                                1. Bunkier Rezydenta    : 1, @: 0110-01-14
                                1. Odlewnia    : 1, @: 0110-04-20
                                1. Sensoplex    : 1, @: 0110-04-20
                            1. Przywiesław    : 1, @: 0109-09-05
                                1. Przychodnia    : 1, @: 0109-09-05
                            1. Pustogor    : 4, @: 0109-12-25
                                1. Arena Szalonego Króla    : 1, @: 0109-10-11
                                1. Barbakan    : 1, @: 0109-12-25
                                1. Gabinet Pięknotki    : 1, @: 0109-12-25
                                1. Inkubator Szamsar    : 1, @: 0109-10-04
                                1. Knajpa Górska Szalupa    : 1, @: 0109-12-25
                                1. Laboratorium Senetis    : 1, @: 0109-10-11
                                1. Miasteczko    : 1, @: 0109-10-04
                            1. Zaczęstwo    : 5, @: 0110-04-20
                                1. Akademia Magii, kampus    : 4, @: 0110-02-04
                                    1. Budynek Centralny    : 3, @: 0110-02-04
                                1. Cyberszkoła    : 2, @: 0110-02-04
                                1. Kasyno Marzeń    : 1, @: 0109-09-19
                                1. Nieużytki Staszka    : 2, @: 0110-04-20
                                1. Osiedle Ptasie    : 1, @: 0110-02-04
                            1. Żarnia    : 1, @: 0110-01-14
                                1. Żernia    : 1, @: 0110-01-14
                        1. Trzęsawisko Zjawosztup    : 5, @: 0110-05-05

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Pięknotka Diakon     | 10 | ((180815-komary-i-kosmetyki; 180929-dwa-tygodnie-szkoly; 181024-decyzja-minerwy; 181027-terminuska-czy-kosmetyczka; 190106-a-moze-pustogorska-mafia; 190113-chronmy-karoline-przed-uczniami; 190119-skorpipedy-krolewskiego-xirathira; 190202-czarodziejka-z-woli-saitaera; 190505-szczur-ktory-chroni; 190527-mimik-sni-o-esuriit)) |
| Arnulf Poważny       | 3 | ((180929-dwa-tygodnie-szkoly; 181027-terminuska-czy-kosmetyczka; 190113-chronmy-karoline-przed-uczniami)) |
| Erwin Galilien       | 3 | ((180815-komary-i-kosmetyki; 180929-dwa-tygodnie-szkoly; 190527-mimik-sni-o-esuriit)) |
| Minerwa Metalia      | 3 | ((181024-decyzja-minerwy; 181027-terminuska-czy-kosmetyczka; 190202-czarodziejka-z-woli-saitaera)) |
| Wiktor Satarail      | 3 | ((190119-skorpipedy-krolewskiego-xirathira; 190505-szczur-ktory-chroni; 190527-mimik-sni-o-esuriit)) |
| Ignacy Myrczek       | 2 | ((180929-dwa-tygodnie-szkoly; 181027-terminuska-czy-kosmetyczka)) |
| Karolina Erenit      | 2 | ((190113-chronmy-karoline-przed-uczniami; 190202-czarodziejka-z-woli-saitaera)) |
| Kasjan Czerwoczłek   | 2 | ((190106-a-moze-pustogorska-mafia; 190505-szczur-ktory-chroni)) |
| Napoleon Bankierz    | 2 | ((180929-dwa-tygodnie-szkoly; 190113-chronmy-karoline-przed-uczniami)) |
| Roland Grzymość      | 2 | ((180929-dwa-tygodnie-szkoly; 190106-a-moze-pustogorska-mafia)) |
| Sławomir Muczarek    | 2 | ((190119-skorpipedy-krolewskiego-xirathira; 190202-czarodziejka-z-woli-saitaera)) |
| Waldemar Mózg        | 2 | ((190106-a-moze-pustogorska-mafia; 190119-skorpipedy-krolewskiego-xirathira)) |
| Adam Szarjan         | 1 | ((181024-decyzja-minerwy)) |
| Alan Bartozol        | 1 | ((190505-szczur-ktory-chroni)) |
| Amadeusz Sowiński    | 1 | ((190106-a-moze-pustogorska-mafia)) |
| Atena Sowińska       | 1 | ((180929-dwa-tygodnie-szkoly)) |
| Brygida Maczkowik    | 1 | ((181027-terminuska-czy-kosmetyczka)) |
| Ernest Kajrat        | 1 | ((190505-szczur-ktory-chroni)) |
| Felicja Melitniek    | 1 | ((180929-dwa-tygodnie-szkoly)) |
| Krystian Namałłek    | 1 | ((190505-szczur-ktory-chroni)) |
| Lilia Ursus          | 1 | ((181024-decyzja-minerwy)) |
| Liliana Bankierz     | 1 | ((190113-chronmy-karoline-przed-uczniami)) |
| Lucjusz Blakenbauer  | 1 | ((190505-szczur-ktory-chroni)) |
| Miedwied Zajcew      | 1 | ((180929-dwa-tygodnie-szkoly)) |
| Mirela Satarail      | 1 | ((190527-mimik-sni-o-esuriit)) |
| Olga Myszeczka       | 1 | ((190119-skorpipedy-krolewskiego-xirathira)) |
| Oliwia Namałłek      | 1 | ((190505-szczur-ktory-chroni)) |
| Saitaer              | 1 | ((190202-czarodziejka-z-woli-saitaera)) |
| Tadeusz Rupczak      | 1 | ((190106-a-moze-pustogorska-mafia)) |
| Teresa Mieralit      | 1 | ((190113-chronmy-karoline-przed-uczniami)) |
| Tymon Grubosz        | 1 | ((190202-czarodziejka-z-woli-saitaera)) |
| Wojtek Kurczynos     | 1 | ((190202-czarodziejka-z-woli-saitaera)) |
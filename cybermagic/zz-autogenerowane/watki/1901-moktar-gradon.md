# Moktar Gradon
## Identyfikator

Id: 1901-moktar-gradon

## Sekcja Opowieści

### Odzyskana agentka Orbitera

* **uid:** 190724-odzyskana-agentka-orbitera, _numer względny_: 6
* **daty:** 0110-06-08 - 0110-06-11
* **obecni:** Adam Szarjan, Aida Serenit, Julia Morwisz, Mirela Niecień, Mirela Orion, Moktar Gradon, Pięknotka Diakon

Streszczenie:

Aidę porwała frakcja Cieniaszczytu zainspirowana przez Julię - by ratować Emulatorkę. Ale Aida, jak się okazało, jest nędznym cywilem i to takim uratowanym z kosmosu. Pięknotka wygrała z championem kralotha przez zanęcenie Moktara - a zwabiła go dając mu walkę z Mirelą. Ogólnie, wszyscy są zadowoleni..?

Aktor w Opowieści:

* Dokonanie:
    * champion Pięknotki do walki z kralotycznym championem przekupiony walką z Kirasjerką. Pokonał wszystko z czym walczył i się doskonale bawił.


### Adieu, Cieniaszczycie

* **uid:** 181227-adieu-cieniaszczycie, _numer względny_: 5
* **daty:** 0109-11-27 - 0109-11-30
* **obecni:** Amadeusz Sowiński, Atena Sowińska, Brygida Maczkowik, Erwin Galilien, Lilia Ursus, Mirela Niecień, Moktar Gradon, Pietro Dwarczan, Pięknotka Diakon, Waleria Cyklon, Zbigniew Burzycki

Streszczenie:

Pięknotka zdobyła nowe ciało dla Minerwy (acz do jego uruchomienia człowiek musi być poświęcony!), przekonała Amadeusza do założenia świątyni Arazille do blokowania Finis Vitae oraz wepchnęła Lilię jako asystentkę Atenie. Do tego rozkochała solidnie Erwina i zamknęła wszystkie wątki, by móc wracać do Pustogoru.

Aktor w Opowieści:

* Dokonanie:
    * przekonał (?!) Pięknotkę, że jeśli walka ma się toczyć z Saitaerem, Pięknotka CHCE by Psy miały Julię na pełnej mocy.


### Finis Vitae

* **uid:** 181226-finis-vitae, _numer względny_: 4
* **daty:** 0109-11-17 - 0109-11-26
* **obecni:** Amadeusz Sowiński, Arazille, Atena Sowińska, Moktar Gradon, Pięknotka Diakon, Waleria Cyklon

Streszczenie:

Moktar zregenerował Pięknotkę i powiedział jej, że chce od niej uratowania swojego Skażonego oficera. Pięknotka dowiedziała się od Amadeusza o autowarze Finis Vitae pod Cieniaszczytem; Saitaer nie może nic wiedzieć. Pięknotka doprowadziła do tego, że Lilia wróciła do Cieniaszczytu (courtesy of Moktar), uratowała spod ziemi tego oficera, oraz zobaczyła miejsce, które będzie chyba do końca życia widziała w koszmarach. Gdyby nie Atena, zginęłaby na tej akcji.

Aktor w Opowieści:

* Dokonanie:
    * po zregenerowaniu Pięknotki dał jej zadanie uratowania Dariusza; co więcej, oddał jej życie Lilii i wyjaśnił problem podziemi.


### Czyszczenie toksycznych związków

* **uid:** 181225-czyszczenie-toksycznych-zwiazkow, _numer względny_: 3
* **daty:** 0109-11-12 - 0109-11-16
* **obecni:** Atena Sowińska, Bogdan Szerl, Julia Morwisz, Moktar Gradon, Pięknotka Diakon, Romuald Czurukin

Streszczenie:

Pięknotka ściągnęła Erwina na detoks od Saitaera, acz dała mu informację o Kreacjuszu Diakonie. Saitaer planuje zbudować sprawne ciało dla Minerwy. Atena jest zregenerowana w dość anielskiej formie i dostaje informacje o Orbiterze od Julii. Pięknotka próbuje naprawić pary Romuald x Julia oraz Wioletta x Pietro. Moktar uwalnia Pięknotkę od wpływu Bogdana, masakrując swojego agenta w brutalny sposób.

Aktor w Opowieści:

* Dokonanie:
    * wszedł do Colubrinus Meditech sprawdzić o co chodzi z Julią i uratował Pięknotkę od Bogdana. Przez MOMENT stracił kontrolę. Channelował Arazille i Saitaera.
* Progresja:
    * czuje do Pięknotki niechętny szacunek - wystarczająco godny przeciwnik.


### Kajdan na Moktara

* **uid:** 181209-kajdan-na-moktara, _numer względny_: 2
* **daty:** 0109-11-01 - 0109-11-02
* **obecni:** Pietro Dwarczan, Pięknotka Diakon, Waleria Cyklon

Streszczenie:

Pięnotka próbowała osłonić się przed potencjalnym atakiem Moktara i poszła szukać jakiejś jego słabości - i spotkała się w pojedynku snajperów z Walerią Cyklon. Wygrała dzięki lepszemu sprzętowi (power suit), acz skończyła w złym stanie. Wycofała się i Walerię przed atakiem dziwnych nojrepów i wybudziła Walerię; dostała swój dowód, przeskanowała pamięć Walerii i uratowała bazę Moktara przed zniszczeniem przez te dziwne nojrepy. Ogólnie, sukces.

Aktor w Opowieści:

* Dokonanie:
    * AKTOR NIEOBECNY NA SESJI
* Progresja:
    * ma ogromną wiedzę o dziwnych nojrepach i całej tej sprawie z Orbiterem Pierwszym
    * wie, że Pięknotka ma dowód mogący poważnie uszkodzić jak nie zniszczyć Łyse Psy. Musi działać przeciw niej ostrożniej.
    * Podzielił się z Pięknotką strefami wpływów; jak długo nie wchodzą sobie w drogę, Pięknotka jest bezpieczna.


### Swaty w cieniu potwora

* **uid:** 181125-swaty-w-cieniu-potwora, _numer względny_: 1
* **daty:** 0109-10-28 - 0109-10-31
* **obecni:** Lilia Ursus, Moktar Gradon, Pietro Dwarczan, Pięknotka Diakon, Romuald Czurukin, Waleria Cyklon, Wioletta Kalazar

Streszczenie:

Pięknotka bardzo chciała przespać się z Pietrem; niestety, spotkała Lilię na bazarze i dowiedziała się o dziwnych nojrepach z symbolem Orbitera. Lilia wyraźnie czuje się niedoceniana i pogardzana. Cóż, na szczęście doprowadziła Pięknotkę do Wioletty. Gdy Pietro wyznał (załamanej) Pięknotce że ma miętę do Wioletty, Pięknotka zdecydowała się ich skojarzyć. Lilia jako detektyw miała pomóc. Lilia wpadła jednak w szpony Moktara, który zaczął ją łamać. Pięknotka i Pietro uratowali Lilię, acz Pietro skończył w szpitalu. Pięknotka, ku swemu zdziwieniu, dała radę kogoś poderwać - Wiolettę. To nie były złe trzy dni...

Aktor w Opowieści:

* Dokonanie:
    * potwór. Złapał sobie Lilię, by ją złamać. Stoczył walkę z Pięknotką, w wyniku której Pietro wyniósł Lilię. Nie osiągnął sukcesu - ale obiecał Pięknotce, że się spotkają.
* Progresja:
    * epicka ballada Romualda sprawiła, że jest postrzegany jako jeszcze straszniejszy potwór. Z czym Moktar dobrze się czuje.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 5, @: 0110-06-11
    1. Primus    : 5, @: 0110-06-11
        1. Sektor Astoriański    : 5, @: 0110-06-11
            1. Astoria    : 5, @: 0110-06-11
                1. Sojusz Letejski, NW    : 4, @: 0109-11-30
                    1. Ruiniec    : 4, @: 0109-11-30
                        1. Colubrinus Meditech    : 1, @: 0109-11-16
                        1. Colubrinus Psiarnia    : 2, @: 0109-11-30
                        1. Diamentowa Forteca    : 1, @: 0109-10-31
                        1. Skalny Labirynt    : 1, @: 0109-11-26
                        1. Studnia Bez Dna    : 1, @: 0109-11-26
                        1. Świątynia Bez Dna    : 1, @: 0109-11-30
                1. Sojusz Letejski    : 5, @: 0110-06-11
                    1. Przelotyk    : 5, @: 0110-06-11
                        1. Przelotyk Wschodni    : 5, @: 0110-06-11
                            1. Cieniaszczyt    : 5, @: 0110-06-11
                                1. Arena Nadziei Tęczy    : 1, @: 0109-10-31
                                1. Bazar Wschodu Astorii    : 1, @: 0109-10-31
                                1. Knajpka Szkarłatny Szept    : 2, @: 0110-06-11
                                1. Kompleks Nukleon    : 4, @: 0110-06-11
                                1. Mordownia Czaszka Kralotha    : 1, @: 0109-10-31
                                1. Mrowisko    : 4, @: 0109-11-30
                            1. Przejściak    : 1, @: 0110-06-11
                                1. Hotel Pirat    : 1, @: 0110-06-11

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Pięknotka Diakon     | 5 | ((181125-swaty-w-cieniu-potwora; 181225-czyszczenie-toksycznych-zwiazkow; 181226-finis-vitae; 181227-adieu-cieniaszczycie; 190724-odzyskana-agentka-orbitera)) |
| Atena Sowińska       | 3 | ((181225-czyszczenie-toksycznych-zwiazkow; 181226-finis-vitae; 181227-adieu-cieniaszczycie)) |
| Waleria Cyklon       | 3 | ((181125-swaty-w-cieniu-potwora; 181226-finis-vitae; 181227-adieu-cieniaszczycie)) |
| Amadeusz Sowiński    | 2 | ((181226-finis-vitae; 181227-adieu-cieniaszczycie)) |
| Julia Morwisz        | 2 | ((181225-czyszczenie-toksycznych-zwiazkow; 190724-odzyskana-agentka-orbitera)) |
| Lilia Ursus          | 2 | ((181125-swaty-w-cieniu-potwora; 181227-adieu-cieniaszczycie)) |
| Mirela Niecień       | 2 | ((181227-adieu-cieniaszczycie; 190724-odzyskana-agentka-orbitera)) |
| Pietro Dwarczan      | 2 | ((181125-swaty-w-cieniu-potwora; 181227-adieu-cieniaszczycie)) |
| Romuald Czurukin     | 2 | ((181125-swaty-w-cieniu-potwora; 181225-czyszczenie-toksycznych-zwiazkow)) |
| Adam Szarjan         | 1 | ((190724-odzyskana-agentka-orbitera)) |
| Aida Serenit         | 1 | ((190724-odzyskana-agentka-orbitera)) |
| Arazille             | 1 | ((181226-finis-vitae)) |
| Bogdan Szerl         | 1 | ((181225-czyszczenie-toksycznych-zwiazkow)) |
| Brygida Maczkowik    | 1 | ((181227-adieu-cieniaszczycie)) |
| Erwin Galilien       | 1 | ((181227-adieu-cieniaszczycie)) |
| Mirela Orion         | 1 | ((190724-odzyskana-agentka-orbitera)) |
| Wioletta Kalazar     | 1 | ((181125-swaty-w-cieniu-potwora)) |
| Zbigniew Burzycki    | 1 | ((181227-adieu-cieniaszczycie)) |
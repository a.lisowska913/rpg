# Kalia Awiter
## Identyfikator

Id: 9999-kalia-awiter

## Sekcja Opowieści

### Atak na Kidirona

* **uid:** 230614-atak-na-kidirona, _numer względny_: 6
* **daty:** 0093-03-22 - 0093-03-24
* **obecni:** Ardilla Korkoran, Bartłomiej Korkoran, Eustachy Korkoran, Kalia Awiter, Lycoris Kidiron, OO Infernia, Rafał Kidiron, Ralf Tapszecz, SAN Szare Ostrze, Stanisław Uczantor

Streszczenie:

Gdy Wujek się obudził, powiedział że z Infernią mogą być ostre problemy jeśli spada poniżej poziomu Memoriam. Zmartwił się hipotezą Ardilli że Ralf może być magiem Nihilusa, ale Ardilla walczyła o dalsze spotykanie się z Ralfem. Zamach na Kidirona. Większość jego sztabu umiera, ale sam Kidiron przeżywa, acz ciężko ranny; ratuje go Ardilla i pozyskuje info o jego mrocznych planach. Kalia bierze na siebie morale i propagandę, ale przez to wpada na celownik Infiltratora który ją porywa. Eustachy próbuje odbić Kalię z rąk Infiltratora w koloidzie...

Aktor w Opowieści:

* Dokonanie:
    * zostawiła Eustachemu podpowiedź, że Kidiron robi coś groźnego. Jak był zamach na Kidirona, spanikowała gdy została ranna. Po rozmowie z Eustachym, odpaliła fałszywą wiadomość od Kidirona że jest bezpieczny itp. Powiedziała Eustachemu gdzie jest Kidiron.
* Progresja:
    * została ranna, na szczęście nie bardzo ciężko w zamachu na Kidirona. Za to jest porwana XD.
    * zna więcej sekretów i skrytek Kidirona niż ktokolwiek inny.


### Zdrada rozrywająca arkologię

* **uid:** 230329-zdrada-rozrywajaca-arkologie, _numer względny_: 5
* **daty:** 0093-03-14 - 0093-03-16
* **obecni:** Amelia Sarkaldir, Ardilla Korkoran, BIA Prometeus, Celina Lertys, Eustachy Korkoran, Ewelina Paroknis, Feliks Kidiron, Franciszek Pietraszczyk, Iwona Paroknis, Jonasz Paroknis, Kalia Awiter, Katarzyna Falernik, Małgorzata Maratelus, OO Infernia, Rafał Kidiron, Ralf Tapszecz, Szczepan Falernik, Wojciech Grzebawron

Streszczenie:

Nastolatka miała pamiętnik czytany przez macochę. Ojciec to ignorował. Nastolatka napisała w pamiętniku, że ojciec zdradza macochę z ekspertką hydrologii. Zrobił się shitstorm który doprowadził do poważnych problemów - m.in. macocha (dyrektor skrzydła medycznego) zaczęła robić błędy co wykryła Celina. Kidiron podejrzewa spisek - ktoś próbuje skłócić ważne rodziny, a także shackować Prometeusa (BIA arkologii). Rozmowa z ich eks-piratami kończy się tym, że to nie oni - za tym stoi wyspecjalizowana siła z innej arkologii lub noktianie. 

Ślady prowadzą do pamiętnika nastolatki, który shackowany przez ekspertkę Inferni (i upubliczniony przez problemy na linii Infernia - Prometeus) pokazują, że najpewniej nastolatka za wszystkim stoi sama. Uratowana przez tajemniczego maga Interis przed samobójstwem nastolatka przyznała na czym polegał problem i ujawnia, że pomaga jej Feliks Kidiron, najmłodszy z rodu i jej jedyny przyjaciel. Czyli to nie spisek. Ale jak to teraz naprawić?

Aktor w Opowieści:

* Dokonanie:
    * szybko ściągnięta przez Ardillę by pomóc Ewelinie. Kalia współczuje młodej nastolatce i pomoże jej poprawić reputację i uniknąć najgorszych konsekwencji. Pierwszy raz w życiu zeszła do Szczurowiska, z Ardillą.


### Terrorystka w Ambasadorce

* **uid:** 230215-terrorystka-w-ambasadorce, _numer względny_: 4
* **daty:** 0093-02-22 - 0093-02-23
* **obecni:** Ardilla Korkoran, Bartłomiej Korkoran, Eustachy Korkoran, Kalia Awiter, Magda Misteria Sarbanik, Rafał Kidiron, Ralf Tapszecz, Tobiasz Lobrak, Tymon Korkoran

Streszczenie:

Kalia i Ardilla weszły do Ambasadorki by pomóc jeśli są w stanie, odcięły ich wiły i Ardilla ich poddaje. Misteria - napastniczka - okazuje się być czarodziejką roślin. Ona chce odzyskać wiły i zarzucić Kidironowi zniszczenie arkologii oraz porwanie wił. Jej celem jest ujawnienie prawdy o Kidironie, ukaranie Tobiasza (patrona Ambasadorki który robi złe rzeczy) i zabezpieczenie wił. Wujek chce zastąpić Ardillę, ale ona odmawia. Zespół (poza Ardillą) planuje zinfiltrować Ambasadorkę i usunąć maga (Misterię). 

Eustachy wpada na pomysł kontrolowanego obniżenia poziomu tlenu wewnątrz. Ardilla przekonuje Misterię do wymiany rannych na wujka. Ralf stanowi kanał komunikacyjny. Misteria tworzy Czarne Serce, kosztem wił i energii. Planują przekonać ludzi o winie Kidironów, wydostać się z Ambasadorki i stworzyć reportaż z Kalią. Ralf mówi o laserze na dachu, którym może zniszczyć czarodziejkę. Tobiasz wzbudza w sercu Misterii terror. Eustachy i Kidiron oczekują na rozwój sytuacji. W wyniku negocjacji, Kidiron zgadza się współpracować pod warunkiem, że Misteria nie będzie działać przeciwko niemu czy arkologii.

Kalia tworzy reportaż o Kidironie i arkologii Lirvint. Kidiron obiecuje bezpieczne wyjście dla Misterii i wił, pod warunkiem zapewnienia nieagresji. Ardilla próbuje przekonać Misterię do ewakuacji, ale ona nie chce opuścić miejsca bez rozwiązania sprawy z Tobiaszem. Czarne Serce wpływa na Misterię, więc wujek ją obezwładnia wstrzykując jej jakieś świństwo, sam zostaje poraniony przez wiły. Kidiron dotrzymał słowa odnośnie ewakuacji, acz postawił na Misterii tracker.

Aktor w Opowieści:

* Dokonanie:
    * dzielnie weszła ratować Ambasadorkę, później stworzyła reportaż mający na celu ujawnienie prawdy o Kidironie i arkologii Lirvint mimo niechęci do Misterii. Współpracowała z Ardillą i pomogła ewakuować osoby z Ambasadorki, będąc gwarantką że nic złego się Misterii nie stanie z ręki Kidirona. Mimo, że jest po stronie Kidirona, współpracując z Misterią stworzyła ruch oporu przeciw Kidironowi.


### Pierwsza randka Eustachego

* **uid:** 230208-pierwsza-randka-eustachego, _numer względny_: 3
* **daty:** 0093-02-14 - 0093-02-21
* **obecni:** Ardilla Korkoran, Bartłomiej Korkoran, Eustachy Korkoran, Franciszek Pietraszczyk, Kalia Awiter, Marcin Pietraszczyk, OO Infernia, Rafał Kidiron, Ralf Tapszecz

Streszczenie:

Wujek opieprza Eustachego za krzywdę niewinnych noktian, ale Kidiron się za Eustachym wstawia. Ardilla socjalizuje Ralfa i ratuje młodego Marcinka który się zaklinował w rurze Szczurowiska; okazuje się, że wszyscy kojarzą Infernię jako symbol nadziei. Potem Eustachy idzie (z przymusu) na randkę z Kalią; okazuje się, że Kalia zmanipulowała by Eustachy wygrał bo chce dać mu jeden piękny dzień. Wszystkie dowody podsłuchała i złapała Ardilla. Gdy Eustachy poczuł chore emanacje magiczne z Ambasadorki, porzucił Kalię i pobiegł do Inferni.

Aktor w Opowieści:

* Dokonanie:
    * 21-letnia śliczna i inteligentna influencerka / idolka / inspiratorka Nativis. (ENCAO:  +--00 |Stoicka;;Radosna, cieszy się i daje radość;;Dzikie i spontaniczne pomysły| VALS: Universalism, Stimulation, Conformity | DRIVE: Hopebringer: Dać każdemu coś o co warto walczyć). Zmanipulowała loterią by wziąć Eustachego na randkę. Zmartwiona tym, że Eustachy nie ma niczego w arkologii na czym mu naprawdę zależy, chciała dać mu jeden dobry, udany dzień i zlinkować go mocniej z arkologią. Robi solidny research na temat tego co się dzieje.
* Progresja:
    * ma suknię pozwalającą jej na holoprojektor oraz na zmianę wyglądu. Oraz zapewnia wszystkie kamery itp. na media społecznościowe.


### Wyłączone generatory Memoriam Inferni

* **uid:** 230201-wylaczone-generatory-memoriam-inferni, _numer względny_: 2
* **daty:** 0093-02-10 - 0093-02-12
* **obecni:** Ardilla Korkoran, Bartłomiej Korkoran, Celina Lertys, Ernest Puszczowiec, Eustachy Korkoran, Jan Lertys, Kalia Awiter, Kalista Luminis, Kratos Coruscatis, OO Infernia, Rafał Kidiron, Ralf Tapszecz, Tymon Korkoran

Streszczenie:

Eustachy przebudzony przez Infernię po burzy piaskowej poszedł za jej podszeptami i wyłączył generatory Memoriam. Infernia go Dotknęła - jest to anomalia Interis uwięziona w mechanicznej skorupie. Tylko Eustachy może Infernią sterować. Więc gdy pojawiła się misja pomocy bazie noktiańskiej Coruscatis, Eustachy musiał lecieć. Kidiron chciał aresztowania Kallisty Luminis a Wujek chciał pomocy noktianom. Okazało się, że na miejscu sformował się amalgamoid trianai i Infernia została ostrzelana; acz odpowiedziała ogniem. Zespół pomógł i ewakuował noktian i zdestabilizował reaktor fuzyjny by wysadzić wszystko. Pod wpływem Ardilli i Celiny Eustachy nie oddał noktian Kidironowi a wysadził ich w bezpiecznym miejscu. Kolejny dowód, że arkologia Nativis wykorzystuje żywność jako broń, co jest niezgodne z wszelkimi konwencjami.

Aktor w Opowieści:

* Dokonanie:
    * influencerka z Nativis. Aktywnie próbuje pójść do łóżka z Eustachym i go podbić, ale to on musi poprosić bo ona jest influencerką.


### To, co zostało po burzy

* **uid:** 230104-to-co-zostalo-po-burzy, _numer względny_: 1
* **daty:** 0092-10-26 - 0092-10-28
* **obecni:** Aniela Myszawcowa, Anna Seiren, Antoni Grzypf, Cyprian Kugrak, Eustachy Korkoran, JAN Seiren, JAN Uśmiech Kamili, Kalia Awiter, Michał Uszwon, Rafał Kidiron, Rufus Seiren, Zofia d'Seiren

Streszczenie:

Neikatis miewa burze piaskowe które z uwagi na strukturę piasku powodują efekty magiczne i po których pojawiają się krótkotrwałe anomalie. Salvagerzy zbierają te anomalie i drenują je, by energię dostarczyć arkologii. Eustachy został przydzielony do salvagera "Seiren", by uczyć się jak wygląda życie w arkologii. Niestety, współwłaścicielka Seiren została porwana przez dowódcę innego salvagera i odjechali w burzę piaskową by ratować JEGO żonę. Seiren jedzie w kierunku na sygnał drugiego salvagera przez burzę, w której odbijają się dziwne głosy i sygnały komunikacyjne. Napotykają ów "Uśmiech Kamili", ale część osób jest martwa i nie ma śladów życia...

Aktor w Opowieści:

* Dokonanie:
    * młoda dama, bardzo zainteresowana Eustachym. Chce się z nim umówić na kolację, ale on stawia ;-).


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 6, @: 0093-03-24
    1. Primus    : 6, @: 0093-03-24
        1. Sektor Astoriański    : 6, @: 0093-03-24
            1. Neikatis    : 6, @: 0093-03-24
                1. Dystrykt Glairen    : 6, @: 0093-03-24
                    1. Arkologia Lirvint    : 1, @: 0093-02-23
                    1. Arkologia Nativis, okolice    : 1, @: 0092-10-28
                        1. Szepczące Wydmy    : 1, @: 0092-10-28
                    1. Arkologia Nativis    : 6, @: 0093-03-24
                        1. Bar Śrubka z Masła    : 1, @: 0092-10-28
                        1. Dzielnica Luksusu    : 4, @: 0093-03-24
                            1. Ambasadorka Ukojenia    : 2, @: 0093-02-23
                            1. Ogrody Wiecznej Zieleni    : 3, @: 0093-03-24
                            1. Stacja holosymulacji    : 1, @: 0093-02-21
                        1. Stara Arkologia Wschodnia    : 3, @: 0093-03-24
                            1. Blokhaus F    : 2, @: 0093-03-16
                                1. Szczurowisko    : 2, @: 0093-03-16
                    1. Arkologia Sarviel, okolice    : 1, @: 0093-02-12
                        1. Krater Coruscatis    : 1, @: 0093-02-12

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Eustachy Korkoran    | 6 | ((230104-to-co-zostalo-po-burzy; 230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona)) |
| Rafał Kidiron        | 6 | ((230104-to-co-zostalo-po-burzy; 230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona)) |
| Ardilla Korkoran     | 5 | ((230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona)) |
| Ralf Tapszecz        | 5 | ((230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona)) |
| Bartłomiej Korkoran  | 4 | ((230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230614-atak-na-kidirona)) |
| OO Infernia          | 4 | ((230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona)) |
| Celina Lertys        | 2 | ((230201-wylaczone-generatory-memoriam-inferni; 230329-zdrada-rozrywajaca-arkologie)) |
| Franciszek Pietraszczyk | 2 | ((230208-pierwsza-randka-eustachego; 230329-zdrada-rozrywajaca-arkologie)) |
| Tymon Korkoran       | 2 | ((230201-wylaczone-generatory-memoriam-inferni; 230215-terrorystka-w-ambasadorce)) |
| Amelia Sarkaldir     | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Aniela Myszawcowa    | 1 | ((230104-to-co-zostalo-po-burzy)) |
| Anna Seiren          | 1 | ((230104-to-co-zostalo-po-burzy)) |
| Antoni Grzypf        | 1 | ((230104-to-co-zostalo-po-burzy)) |
| BIA Prometeus        | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Cyprian Kugrak       | 1 | ((230104-to-co-zostalo-po-burzy)) |
| Ernest Puszczowiec   | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Ewelina Paroknis     | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Feliks Kidiron       | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Iwona Paroknis       | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Jan Lertys           | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| JAN Seiren           | 1 | ((230104-to-co-zostalo-po-burzy)) |
| JAN Uśmiech Kamili   | 1 | ((230104-to-co-zostalo-po-burzy)) |
| Jonasz Paroknis      | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Kalista Luminis      | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Katarzyna Falernik   | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Kratos Coruscatis    | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Lycoris Kidiron      | 1 | ((230614-atak-na-kidirona)) |
| Magda Misteria Sarbanik | 1 | ((230215-terrorystka-w-ambasadorce)) |
| Małgorzata Maratelus | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Marcin Pietraszczyk  | 1 | ((230208-pierwsza-randka-eustachego)) |
| Michał Uszwon        | 1 | ((230104-to-co-zostalo-po-burzy)) |
| Rufus Seiren         | 1 | ((230104-to-co-zostalo-po-burzy)) |
| SAN Szare Ostrze     | 1 | ((230614-atak-na-kidirona)) |
| Stanisław Uczantor   | 1 | ((230614-atak-na-kidirona)) |
| Szczepan Falernik    | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Tobiasz Lobrak       | 1 | ((230215-terrorystka-w-ambasadorce)) |
| Wojciech Grzebawron  | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Zofia d'Seiren       | 1 | ((230104-to-co-zostalo-po-burzy)) |
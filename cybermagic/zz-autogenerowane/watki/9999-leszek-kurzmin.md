# Leszek Kurzmin
## Identyfikator

Id: 9999-leszek-kurzmin

## Sekcja Opowieści

### Sekrety Orbitera - historia prawdziwa

* **uid:** 200819-sekrety-orbitera-historia-prawdziwa, _numer względny_: 10
* **daty:** 0110-11-26 - 0110-12-04
* **obecni:** Arianna Verlen, Damian Orion, Elena Verlen, Eustachy Korkoran, Izabela Zarantel, Klaudia Stryk, Leona Astrienko, Leszek Kurzmin, Martyn Hiwasser, Sabina Servatel, Tadeusz Ursus

Streszczenie:

Elena wyzwała Leonę na pojedynek. Klaudia i Arianna wkręciły Tadeusza. Tadeusz - arcymag Eterni - używa Simulacrum w walce, więc Zespół zaczął montować plan jak Leona może jednak wygrać z Simulacrum. I wszystko skończyło się tym, że Leona wygrała, Elena jest wolna od Tadeusza i powstała holodrama "Sekrety Orbitera"...

Aktor w Opowieści:

* Dokonanie:
    * by pomóc Ariannie, wziął na siebie Leonę, mówiąc jej że wykryli koloidowy statek Eterni.


### Problematyczna Elena

* **uid:** 200708-problematyczna-elena, _numer względny_: 9
* **daty:** 0110-10-29 - 0110-11-03
* **obecni:** AK Salamin, Antoni Kramer, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Leszek Kurzmin, Olgierd Drongon, OO Aurelion, OO Żelazko, Persefona d'Infernia

Streszczenie:

Na Infernię trafiła nowa podoficer - podporucznik Elena Verlen, z Castigatora. W konkursie z Eustachym prawie zniszczyli Infernię. Niedługo potem Infernia (podłatana) poleciała do Anomalii Kolapsu uratować Aurelion, który natknął się na anomaliczny kiedyś-krążownik Orbitera Salamin. Arianna uratowała kogo się da i skłoniła Salamin do samozniszczenia; Persefona d'Salamin miała jeszcze dość psychotronicznej świadomości i miłości (?) do swojej martwej kapitan.

Aktor w Opowieści:

* Dokonanie:
    * dowódca Castigatora; wysłał Elenę do Arianny jako załogantkę. A Kramer zaakceptował, bo Elena miała wszystkie papiery w porządku i nieposzlakowaną opinię.


### Ratujmy Castigator

* **uid:** 200624-ratujmy-castigator, _numer względny_: 8
* **daty:** 0110-10-15 - 0110-10-19
* **obecni:** Arianna Verlen, Eustachy Korkoran, Klaudia Stryk, Leona Astrienko, Leszek Kurzmin, OO Alkaris, OO Castigator, Rozalia Wączak

Streszczenie:

Skażona przez Wiktora Sataraila Rozalia ruszyła zniszczyć Castigator. Tymczasem Arianna poproszona przez przyjaciela (kapitana Castigatora) doprowadziła terrorem Castigator do prawidłowego działania i strasznie zwiększyła napięcia między Aurum i Orbiterem. Gdy Rozalia i Alkaris uderzyli, Castigator był jakoś gotowy. Castigator odparł Alkaris i wszystkich uratowano przez stopniowe wysadzanie fragmentów Castigatora i kupowanie czasu.

Aktor w Opowieści:

* Dokonanie:
    * kapitan Castigatora i przyjaciel Arianny; poprosił ją o pomoc w "oczyszczeniu" Castigatora z naleśniczej arystokracji. Mocno jej ufa... i ją friendzonował.


### Gdy HR reedukuje niewłaściwą osobę

* **uid:** 230111-gdy-hr-reedukuje-niewlasciwa-osobe, _numer względny_: 7
* **daty:** 0100-09-15 - 0100-09-18
* **obecni:** Adragain Ferrias, AK Nox Ignis, Aleksy Sartaran, Arianna Verlen, Daria Czarnewik, Elena Verlen, Grażyna Burgacz, Kajetan Kircznik, Lana Mirkinin, Leszek Kurzmin, Maja Samszar, Miłosz Klinek, OO Astralna Flara, OO Athamarein, OO Loricatus, Persefona d'Loricatus, Sabrina Ferrias, Salazar Bolza, Szczepan Myrczek, Władawiec Diakon

Streszczenie:

Niestety, Kircznik (lekarz) przestraszył Sargona i młodych infiltratorów Lodowcem, Bolzą i ogólnie duchami noktiańskimi. Okazało się, że Grażyna jest ekspertem od duchów i jest w stanie zrobić egzorcyzm. Niedługo potem (i chwilę przed egzorcyzmem) pojawiła się szybka korweta z Biur HR i poprosili o pomoc w odparciu Anomalii Kosmicznej. Myrczek sprawdził jej pamięć i Athamarein i Flara poleciały na starcie z Nox Ignis. Nawiązały kontakt z Biurami HR, przechwyciły biednego męża pilotki Nox Ignis i poznały prawdę o Destructor Animarum i wpływie Syndykatu Aureliona na Biura HR. Kurzmin pobił rekordy Athamarein, Arianna skutecznie zaplanowała wypalenie pilotki Nox Ignis przez własny statek. Tylko advancer Miłosz (badający ślady Nox Ignis) jest MIA.

Aktor w Opowieści:

* Dokonanie:
    * pewny umiejętności Athamarein; chciał walczyć z Nox Ignis, ale pod wpływem pomysłów Arianny zmienił Athamarein w super-szybką jednostkę kurierską i wymanewrował Nox Ignis nie walcząc z nią bezpośrednio. CREEPED OUT przez Destructor Animarum. Zabił Adragaina, nie chciał widzieć tego upiornego pustego uśmiechu.
* Progresja:
    * solidny opierdol od kmdr Salazara Bolzy za śmierć Adragaina Ferriasa. Emocje zwyciężyły nad możliwością zbadania jak działa Destructor Animarum.


### Astralna Flara i nowy komodor

* **uid:** 221221-astralna-flara-i-nowy-komodor, _numer względny_: 6
* **daty:** 0100-09-09 - 0100-09-12
* **obecni:** Arianna Verlen, Daria Czarnewik, Elena Verlen, Ellarina Samarintael, Kajetan Kircznik, Kaspian Certisarius, Leszek Kurzmin, Maja Samszar, NekroTAI Zarralea, OO Astralna Flara, OO Athamarein, OO Loricatus, Salazar Bolza, Sargon Niiris, Władawiec Diakon

Streszczenie:

Po "uratowaniu" noktian z Dominy Lucis Arianna dostała medal a Lodowca zastąpił komodor Bolza, zimny drakolita. Gdy grupa wydzielona wróciła na teren Anomalii Kolapsu okazało się, że planetoida Kazmirian ma squatterów; Zespół skutecznie ich przejął odzyskując kontrolę nad systemami Semli d'Kazmirian. Na miejscu - TKO-4271 - okazało się że Zarralea jest zniszczona. Orbiter odzyskał swoją bazę, acz Loricatis musi współdzielić Persefonę. Z innej beczki: link Elena - Władawiec się pięknie rozwija.

Aktor w Opowieści:

* Dokonanie:
    * to on zaproponował sojusz ze squatterami i oddanie im bazy. Twórca modyfikowanego planu jak odzyskać Kazmirian by wywołać największy szok u squatterów. Oddał Ariannie możliwość spięcia z Kazmirian, by jej załoga mogła ćwiczyć (Bolza jest bardzo wymagający). Doszedł do tego, że najpewniej to Bolza stoi za zniszczeniem Zarralei i przekazał informację o tym Ariannie.


### Astralna Flara kontra Domina Lucis

* **uid:** 221214-astralna-flara-kontra-domina-lucis, _numer względny_: 5
* **daty:** 0100-08-11 - 0100-08-13
* **obecni:** Arianna Verlen, Axel Nargan, Daria Czarnewik, Ellarina Samarintael, Gabriel Lodowiec, Kirea Rialirat, Leszek Kurzmin, NekroTAI Zarralea, OO Astralna Flara, OO Athamarein, Sarian Xadaar, Tristan Rialirat

Streszczenie:

Lodowiec dostał rozkazy pojmać / zabić dowódcę Dominy Lucis. Użył nekroTAI by odebrać resztkę nadziei noktianom (you will serve alive or dead) i jakkolwiek sporo noktian się poddało, główni popełnili samobójstwo. Orbiter zajął Dominę Lucis i Strefę Duchów, acz kosztem reputacji. Ogromny, bezkrwawy sukces wzmacniający Pax Orbiter dzięki propagandzie i Zarralei.

Aktor w Opowieści:

* Dokonanie:
    * logistycznie przeprowadził operację poddawania się noktian, by na pewno nie ucierpiał nikt niewinny i nie dało się zniszczyć Flary ani Athamarein. Wykonywał wielokrotne skany aż doszedł do tego, że teren jest silnie zaminowany. Wiedział, że Domina Lucis nie będzie bez defensyw.


### Egzotyczna Piękność na Astralnej Flarze

* **uid:** 221123-egzotyczna-pieknosc-na-astralnej-flarze, _numer względny_: 4
* **daty:** 0100-07-15 - 0100-07-18
* **obecni:** Arianna Verlen, Arnulf Perikas, Daria Czarnewik, Ellarina Samarintael, Erwin Pies, Frank Mgrot, Gabriel Lodowiec, Gerwazy Kircznik, Hind Szug Traf, Kajetan Kircznik, Leszek Kurzmin, Marcel Kulgard, OO Astralna Flara, OO Athamarein, SCA Hadiah Emas, Szczepan Myrczek, Władawiec Diakon

Streszczenie:

Flara naprawia Hadiah Emas, a po drodze komodor Lodowiec rozplątuje wszystkie wątki - niewolnik z Orbitera? (okazał się dezerterem), autonomiczna TAI Mirtaela d'Hadiah Emas? (zostaje Ograniczona przez psychotronika Athamarein) itp. Arianna zarządza Flarą, wprowadzając Ellarinę jako maskotkę od morale i stabilizując wszystko by jakoś zespół dopasował się do realiów Nonariona. To nie jest ani Aurum ani Orbiter, to coś zupełnie innego.

Aktor w Opowieści:

* Dokonanie:
    * dobrze pracuje z dokumentami i przetwarza dane; znalazł dla Gabriela Lodowca info, że jeden z niewolników to koleś z Orbitera. Przeprowadził dyskretną operację Ograniczenia Mirtaeli, TAI Hadiah Emas z rozkazu Gerwazego Kircznika.


### Astralna Flara dociera do Nonariona Nadziei

* **uid:** 221116-astralna-flara-dociera-do-nonariona-nadziei, _numer względny_: 3
* **daty:** 0100-07-11 - 0100-07-14
* **obecni:** Alan Nierkamin, Alezja Dumorin, Arianna Verlen, Arnulf Perikas, Daria Czarnewik, Gabriel Lodowiec, Grażyna Burgacz, Kajetan Kircznik, Leo Kasztop, Leszek Kurzmin, Maja Samszar, Marcel Kulgard, OO Astralna Flara, OO Athamarein, SCA Hadiah Emas, SCA Isigtand, Szymon Wanad, Tomasz Ruppok, Władawiec Diakon

Streszczenie:

Flara dociera do Nonariona, który wygląda jak absolutny śmieć. Morale na Flarze ucierpiało - aż do plotek o Egzotycznych Pięknościach, co sprawiło Ariannie kolejne problemy. Alan, eks-lokals powiedział Darii że jest jakaś forma standaryzacji na Nonarionie - coś tam jest, coś "bogatego" jako frakcja. Daria zdobyła od Leo planetoidę którą może użyć jako bazę dla Orbitera, ale w drodze tam zboczyli by ratować uszkodzone statki lokalne. I uratowali jednostkę niewolniczą i zniszczony grazer. Komodor Lodowiec - sensowny koleś - ma dylemat moralny. Neikatiańska TAI oraz niewolnicy.

Aktor w Opowieści:

* Dokonanie:
    * szczęśliwy, że mają sensownego komodora;


### Kapitan Verlen i koniec przygody na Królowej

* **uid:** 221026-kapitan-verlen-i-koniec-przygody-na-krolowej, _numer względny_: 2
* **daty:** 0100-04-10 - 0100-04-17
* **obecni:** Antoni Kramer, Arianna Verlen, Arnulf Perikas, Daria Czarnewik, Erwin Pies, Klaudiusz Terienak, Leszek Kurzmin, OO Królowa Kosmicznej Chwały, OO Tucznik Trzeci, Stefan Torkil, Tomasz Ruppok

Streszczenie:

Po długiej i ciężkiej pracy Arianna i Daria doprowadziły Królową do 50% sprawności nominalnej. Radość i wysokie morale załogi. Gdy Arnulf przyszedł do Arianny mówiąc o fabrykacji narkotyków i alkoholu, ona kazała to 'zatruć'. Zmotywowała Ruppoka, by dał jej szansę i znalazła sposób jak częściowo odzyskać materiały do fabrykatora po rozmowie z Arnulfem - sojusz z bliską Anomalii Kolapsu stacją Nonarion; poprosiła Kurzmina by to załatwił. TAK PRZESTRASZYŁA siły specjalne Orbitera, że Ariannę i załogę przenieśli bliżej Nonariona by sabotować program kosmiczny Aurum. Koniec przygody na Królowej :-).

Aktor w Opowieści:

* Dokonanie:
    * współpracuje z Arianną. Ona zapewnia linię 'Aurum - Tucznik Trzeci', on linię 'Tucznik Trzeci - Nonarion'. I tak powstanie udany handel na którym wszyscy wygrają.


### Kapitan Verlen i pojedynek z marine

* **uid:** 220928-kapitan-verlen-i-pojedynek-z-marine, _numer względny_: 1
* **daty:** 0100-03-16 - 0100-03-18
* **obecni:** Alezja Dumorin, Arianna Verlen, Arnulf Perikas, Daria Czarnewik, Erwin Pies, Grażyna Burgacz, Leona Astrienko, Leszek Kurzmin, Maja Samszar, OO Królowa Kosmicznej Chwały, Stefan Torkil, Szymon Wanad, Tomasz Ruppok, Władawiec Diakon

Streszczenie:

Królowa miała się przesunąć, ale prawie się rozbiła; ta jednostka jest rozkradziona. Arianna zatrzymała biczowanie załogantów i doszła do tego, że załoga jest niemrawa i skonfliktowana bo część ludzi jest z Aurum a część nie. Perfect storm of suck. By zatrzymać Wanada przed "wygraniem cnoty koleżanki inżyniera" stoczyła z nim pojedynek i wygrała. Leona ostrzegła, by Arianna nic nie jadła i nie piła. Czyli coś jest w jedzeniu. To tłumaczy czemu Królowa jest tak zdegenerowaną jednostką. Aha, Arianna ma wsparcie kapitana Leszka Kurzmina, dawnego rywala Alezji.

Aktor w Opowieści:

* Dokonanie:
    * chce pomóc Ariannie (nawet dyskretnie) by ratować Królową Kosmicznej Chwały i Alezję. Rywalizował z Alezją w Akademii, nie wierzy jak bardzo upadła.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 10, @: 0110-12-04
    1. Primus    : 10, @: 0110-12-04
        1. Sektor Astoriański    : 10, @: 0110-12-04
            1. Astoria, Orbita    : 3, @: 0110-12-04
                1. Kontroler Pierwszy    : 3, @: 0110-12-04
                    1. Akademia Orbitera    : 1, @: 0110-10-19
                    1. Arena Kalaternijska    : 1, @: 0110-12-04
                    1. Hangary Alicantis    : 1, @: 0110-11-03
            1. Obłok Lirański    : 6, @: 0110-11-03
                1. Anomalia Kolapsu, orbita    : 5, @: 0100-09-18
                    1. Planetoida Kazmirian    : 1, @: 0100-07-14
                    1. SC Nonarion Nadziei    : 1, @: 0100-07-14
                        1. Moduł ExpanLuminis    : 1, @: 0100-07-14
                    1. Strefa Biur HR    : 1, @: 0100-09-18
                    1. Strefa Upiorów Orbitera    : 3, @: 0100-09-18
                        1. Planetoida Kazmirian    : 1, @: 0100-09-12
                        1. Planetoida Lodowca    : 3, @: 0100-09-18
                1. Anomalia Kolapsu    : 1, @: 0110-11-03

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 10 | ((200624-ratujmy-castigator; 200708-problematyczna-elena; 200819-sekrety-orbitera-historia-prawdziwa; 220928-kapitan-verlen-i-pojedynek-z-marine; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221214-astralna-flara-kontra-domina-lucis; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Daria Czarnewik      | 7 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221214-astralna-flara-kontra-domina-lucis; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| OO Astralna Flara    | 5 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221214-astralna-flara-kontra-domina-lucis; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| OO Athamarein        | 5 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221214-astralna-flara-kontra-domina-lucis; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Władawiec Diakon     | 5 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Arnulf Perikas       | 4 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Elena Verlen         | 4 | ((200708-problematyczna-elena; 200819-sekrety-orbitera-historia-prawdziwa; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Kajetan Kircznik     | 4 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Maja Samszar         | 4 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Ellarina Samarintael | 3 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze; 221214-astralna-flara-kontra-domina-lucis; 221221-astralna-flara-i-nowy-komodor)) |
| Erwin Pies           | 3 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Eustachy Korkoran    | 3 | ((200624-ratujmy-castigator; 200708-problematyczna-elena; 200819-sekrety-orbitera-historia-prawdziwa)) |
| Gabriel Lodowiec     | 3 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221214-astralna-flara-kontra-domina-lucis)) |
| Grażyna Burgacz      | 3 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221116-astralna-flara-dociera-do-nonariona-nadziei; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Klaudia Stryk        | 3 | ((200624-ratujmy-castigator; 200708-problematyczna-elena; 200819-sekrety-orbitera-historia-prawdziwa)) |
| Leona Astrienko      | 3 | ((200624-ratujmy-castigator; 200819-sekrety-orbitera-historia-prawdziwa; 220928-kapitan-verlen-i-pojedynek-z-marine)) |
| Tomasz Ruppok        | 3 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej; 221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Alezja Dumorin       | 2 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Antoni Kramer        | 2 | ((200708-problematyczna-elena; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Marcel Kulgard       | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| NekroTAI Zarralea    | 2 | ((221214-astralna-flara-kontra-domina-lucis; 221221-astralna-flara-i-nowy-komodor)) |
| OO Królowa Kosmicznej Chwały | 2 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| OO Loricatus         | 2 | ((221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Salazar Bolza        | 2 | ((221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| SCA Hadiah Emas      | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Stefan Torkil        | 2 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Szczepan Myrczek     | 2 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Szymon Wanad         | 2 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Adragain Ferrias     | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| AK Nox Ignis         | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| AK Salamin           | 1 | ((200708-problematyczna-elena)) |
| Alan Nierkamin       | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Aleksy Sartaran      | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Axel Nargan          | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |
| Damian Orion         | 1 | ((200819-sekrety-orbitera-historia-prawdziwa)) |
| Frank Mgrot          | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Gerwazy Kircznik     | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Hind Szug Traf       | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Izabela Zarantel     | 1 | ((200819-sekrety-orbitera-historia-prawdziwa)) |
| Kaspian Certisarius  | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| Kirea Rialirat       | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |
| Klaudiusz Terienak   | 1 | ((221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Lana Mirkinin        | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Leo Kasztop          | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Martyn Hiwasser      | 1 | ((200819-sekrety-orbitera-historia-prawdziwa)) |
| Miłosz Klinek        | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Olgierd Drongon      | 1 | ((200708-problematyczna-elena)) |
| OO Alkaris           | 1 | ((200624-ratujmy-castigator)) |
| OO Aurelion          | 1 | ((200708-problematyczna-elena)) |
| OO Castigator        | 1 | ((200624-ratujmy-castigator)) |
| OO Tucznik Trzeci    | 1 | ((221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| OO Żelazko           | 1 | ((200708-problematyczna-elena)) |
| Persefona d'Infernia | 1 | ((200708-problematyczna-elena)) |
| Persefona d'Loricatus | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Rozalia Wączak       | 1 | ((200624-ratujmy-castigator)) |
| Sabina Servatel      | 1 | ((200819-sekrety-orbitera-historia-prawdziwa)) |
| Sabrina Ferrias      | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Sargon Niiris        | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| Sarian Xadaar        | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |
| SCA Isigtand         | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Tadeusz Ursus        | 1 | ((200819-sekrety-orbitera-historia-prawdziwa)) |
| Tristan Rialirat     | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |
# Eustachy Korkoran
## Identyfikator

Id: 2004-eustachy-korkoran

## Sekcja Opowieści

### Lewiatan za Pandorę

* **uid:** 220622-lewiatan-za-pandore, _numer względny_: 77
* **daty:** 0112-07-26 - 0112-07-29
* **obecni:** Arianna Verlen, Eustachy Korkoran, Klaudia Stryk, Maria Naavas, Ola Szerszeń, OO Infernia, OO Pandora, OO Straszliwy Pająk, OO Tivr, Raoul Lavanis, Rzeźnik Parszywiec Diakon

Streszczenie:

PRZESZŁOŚĆ: Ola Szerszeń opiekuje się Eleną w Regeneratorze; okazuje się być biosyntem (zakazane eksperymenty Termii). Rzeźnik Diakon to łagodny i jowialny kapitan Straszliwego Pająka, acz znienawidzony za przeszłość zdrajcy Orbitera oraz Noctis. TERAŹNIEJSZOŚĆ: Lewiatan zostaje zestrzelony torpedą anihilacyjną, dzięki Raoulowi ratujemy wielu ludzi na orbicie Lewiatana. Malictrix d'Pandora zostaje zniszczona przez Lewiatana, ale Klaudia ma jego mikrokopię dzięki Ariannie.

Aktor w Opowieści:

* Dokonanie:
    * kapitan Inferni, przejął dowodzenie nad Olą Szerszeń i postrzelał torpedami anihilacyjnymi do Lewiatana.


### Lewiatan przy Seibert

* **uid:** 220615-lewiatan-przy-seibert, _numer względny_: 76
* **daty:** 0112-07-23 - 0112-07-25
* **obecni:** Arianna Verlen, Eszara d'Seibert, Eustachy Korkoran, Jonasz Parys, Klaudia Stryk, Ola Szerszeń

Streszczenie:

W okolicach Anomalii Kolapsu obudził się Lewiatan. Ruszył na sortownię Seibert, pożerając kilka jednostek. Grupa Infernia, która miała ćwiczenia (i pokonała koloidową korwetę Zająca 3) dała radę zlokalizować Lewiatana, zabezpieczyć się przed aspektem bazyliszka, zdobyć trochę jego energii do badań i przygotować się do neutralizacji. Jakoś.

Aktor w Opowieści:

* Dokonanie:
    * organizuje Dni Otwarte na Inferni, znajduje sposób jak przechwycić energię z Lewiatana używając kontrolowanego zwarcia i zlokalizował koloidową korwetę pyłem magnetycznym i zderzaniem się Tivrem.


### EtAur - dziwnie ważny węzeł

* **uid:** 220706-etaur-dziwnie-wazny-wezel, _numer względny_: 75
* **daty:** 0112-07-15 - 0112-07-17
* **obecni:** Aleksandra Termia, Eustachy Korkoran, Klaudia Stryk, Maria Naavas

Streszczenie:

Maria zainteresowała się planem adm. Termii wskazującym na to, że ta zdradza Orbiter z Syndykatem Aureliona. Eustachy (do którego przyszła) zapytał Termię wprost i doszedł do tego, że Termia szuka koloidowej bazy Aureliona na księżycu Elang swymi bezdusznymi metodami. Eustachy i Klaudia wskazali Termii, że baza EtAur jest potencjalnie punktem kontaktowym Syndykatu z Astorią i tu jest ryzyko. Eustachy doprowadził do kontaktu Termia - EtAur, ale też gra na współpracę Inferni i EtAur.

Aktor w Opowieści:

* Dokonanie:
    * gdy Maria przyszła doń z konspiracją Termia - Aurelion, poszedł do Termii bezpośrednio. Powiedział jej, że nie pójdzie za jej planem; lepiej znaleźć koloidową bazę Aureliona używając przemytników i jego środków z bazy EtAur. Zbudował sojusz Infernia - EtAur i gra pod siebie i pod Infernię.


### Ratujemy porywaczy Eleny

* **uid:** 220610-ratujemy-porywaczy-eleny, _numer względny_: 74
* **daty:** 0112-07-11 - 0112-07-13
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Hestia d'Atropos, Jakub Bulgocz, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Raoul Lavanis

Streszczenie:

Elena jest już zbyt zdestabilizowana by funkcjonować niezależnie, więc wpierw zajęli się nią w Laboratorium Kranix a po zbudowaniu dla niej Containment Chamber przesłali ją do stacji medycznej Atropos do regeneracji. Stamtąd ją i Martyna porwał Syndykat Aureliona (ale Martyn przewidział JAKIŚ problem i ostrzegł Klaudię). Elena by zniszczyła okręt napastników, ale Zespół skutecznie odkrył problemy na Atropos, przesłuchali dowódcę, zneutralizowali miragenta i dotarli do okrętu Aureliona zdobywając jeńców i zatrzymując Elenę przed przebudzeniem i Spustoszeniem statku.

Aktor w Opowieści:

* Dokonanie:
    * skutecznie zsabotował statek Aureliona sprawiając, że popanikowali i zaczęli się ewakuować. Potem uwolnił Martyna i uspokoił budzącą się Elenę.


### EtAur i przynęta na Kryptę

* **uid:** 220330-etaur-i-przyneta-na-krypte, _numer względny_: 73
* **daty:** 0112-04-27 - 0112-04-30
* **obecni:** Arianna Verlen, Eustachy Korkoran, Klaudia Stryk, Lamia Akacja, Leszek Czarban, Maria Naavas, Raoul Lavanis

Streszczenie:

Okazało się, że "potwór" to były faeril z Neikatis zarażony bronią biologiczną. Próbował doprowadzić do katastrofy by Orbiter lub Eternia zabrali się za leczenie tego cholerstwa, pomagał mu Antoni, p.o. TAI na EtAur. Maria doszła do biostruktury, Klaudia przejęła systemy bazy, Eustachy przechwycił gościa, Arianna zdobyła bazę politycznie. A Klaudia zrobiła raport, by Orbiter nie atakował więcej neutralnych i potencjalnie sojuszniczych sił nie-Orbiterowych w kosmosie. Jako, że Lamii nic się nie stało, Melania jest szczęśliwa.

Aktor w Opowieści:

* Dokonanie:
    * szybko zdążył uratować Raoula i Marię przed "potworem" - z Raoulem odgonił stwora.
* Progresja:
    * NIECHĘĆ ze strony purystów Orbitera. PRO-LUDZKA grupa Orbitera, Eternia i Aurum uważają Infernię za siłę której wiele zawdzięczają.


### Potwór czy choroba na EtAur?

* **uid:** 220316-potwor-czy-choroba-na-etaur, _numer względny_: 72
* **daty:** 0112-04-24 - 0112-04-26
* **obecni:** Arianna Verlen, Dominika Perikas, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Maria Naavas, Melania Akacja, Suwan Chankar, Tymoteusz Czerw

Streszczenie:

Na stacji EtAur pojawiły się poważne problemy - potwór? Choroba? Suwan próbuje ustabilizować co się da, ale nic nie może zrobić. Arianna poproszona o pomoc przybyła, ale nikt nie chce z nią praktycznie współpracować. Klaudia doszła do przyczyny problemów - choroba, ale potwór jest wektorem. Arianna doszła do transmisji po kanałach magicznych. Świetnie.

Aktor w Opowieści:

* Dokonanie:
    * uniknął aresztowania przez siły na EtAur, podzielił siły i szuka potwora? Choroby? Aha, uniknął oświadczyn Eleny.


### Upadek Eleny

* **uid:** 220309-upadek-eleny, _numer względny_: 71
* **daty:** 0112-04-03 - 0112-04-09
* **obecni:** Annika Pradis, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Tomasz Kaltaben

Streszczenie:

Keldan Voss została ustabilizowana. Annika będzie mieć szansę powodzenia - zostaje przełożoną, z łącznikiem z keldanitami Mateusem. Odbudowany jej link z pallidanami. Pallidanie mają ogólnie "spokój". Tomasz Kaltaben będzie ją wspierał a on sam dostanie 3 magów Orbitera co go wspierają (i są agentami). Elena niestety zabrała trzy życia odzyskując naturalną urodę. Klaudia ciężko pracuje by zbudować dla Eleny Detox Chamber - ona nie jest OK.

Aktor w Opowieści:

* Dokonanie:
    * podnosi morale Anniki - ma próbować. Zmusił ją do wzięcia odpowiedzialności za Keldan Voss i jej przyszłość. Annika za tym poszła. Pomógł dziewczynie.


### Stabilizacja Keldan Voss

* **uid:** 220223-stabilizacja-keldan-voss, _numer względny_: 70
* **daty:** 0112-03-30 - 0112-04-02
* **obecni:** Annika Pradis, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, SP Światło Nadziei, Szczepan Kaltaben, Tomasz Kaltaben

Streszczenie:

Ewakuacja pallidan na Keldan Voss się udało, acz Eustachy musiał zniszczyć anomalnych napastników z Mgły i naprawić morale Anniki. Elenie (słyszącej szepty) udało się zniszczyć BIA i wprowadzić na jej miejsce amalgamat szamana. Arianna ma polityczną kontrolę nad sytuacją a dzięki Eustachemu udało się sytuację ustabilizować militarnie. Dzięki Klaudii wiedzą co i jak. Teraz już tylko zostaje zostawić Keldan Voss w stabilnej formie.

Aktor w Opowieści:

* Dokonanie:
    * na szybko montuje w Keldan Voss generatory Memoriam, by móc ewakuować nadmiar pallidan z Kastora. Po ostrzeżeniu Eleny zrobił killzone i ochronił pallidan podnosząc im morale.


### Polityka rujnuje Pallidę Voss

* **uid:** 220216-polityka-rujnuje-pallide-voss, _numer względny_: 69
* **daty:** 0112-03-27 - 0112-03-29
* **obecni:** Annika Pradis, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Mateus Sarpon, Raoul Lavanis, SP Pallida Voss, Szczepan Kaltaben

Streszczenie:

Arianna we współpracy z Mateusem planują uratować zakładników pallidańskich od radykałów keldańskich. Ale by to osiągnąć Eustachy i Klaudia infiltrują wpierw Pallidę Voss, bo okazuje się, że Annikę wrabiają jej własni podwładni. Klaudia zdobyła wszystkie potrzebne dowody, Eustachy chciał ratować ludzi... i Paradoks (po sabotowaniu generatorów Memoriam) spowodował straszne straty w ludziach i przesunął Pallidę Voss w Anomalię Kosmiczną. Oops. Ale dzięki drakolitom (keldanitom) udało się sporo ludzi uratować.

Aktor w Opowieści:

* Dokonanie:
    * zaprzyjaźnił ze sobą Annikę i poznał jej historię i co ona myśli. Infiltruje Pallidę Voss, sabotował generatory Memoriam i niestety Paradoks gdy dotarł do "sali medycznej" gdzie krojono drakolitów spowodował Kaskadę Paradoksu. Konieczność ewakuacji statku... cholerni kultyści Saitaera.


### Sekrety Keldan Voss

* **uid:** 220202-sekrety-keldan-voss, _numer względny_: 68
* **daty:** 0112-03-24 - 0112-03-26
* **obecni:** Annika Pradis, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Mateus Sarpon

Streszczenie:

Zespół skutecznie ewakuował pallidan z Keldan Voss, po drodze orientując się że część porwanych przez Mgły osób została Skażona i zmieniona w Kroczących w Mgle. Eustachy wydobył jednego do badań dla Marii. Co ciekawe - pallidanie i keldanici widzą zupełnie inne wersje historii ("snajper" vs "porwania") a do tego Mgły powodują jeszcze większą nierzeczywistość i złudzenia. A Arianna politycznie a Klaudia naukowo dochodzą do tego co jest prawdą...

Aktor w Opowieści:

* Dokonanie:
    * Ewakuuje Annikę i pallidan z Keldan Voss na orbitę, do Kastora i jednostek wspierających. Podczas ewakuacji zaatakował mgłę i wyciągnął jednego z Kroczących do badań dla Marii. Naprawił generator osłaniany przez Elenę.


### Keldan Voss, kolonia Saitaera

* **uid:** 220126-keldan-voss-kolonia-saitaera, _numer względny_: 67
* **daty:** 0112-03-19 - 0112-03-22
* **obecni:** Annika Pradis, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Kormonow Voss, Mateus Sarpon, OO Kastor, SP Pallida Voss, Szczepan Kaltaben, Zygfryd Maus

Streszczenie:

Kramer wysłał załogę Inferni na Keldan Voss (stację pod kontrolą pallidan i źródło kryształów vitium), bo tam mogą więcej wiedzieć o anomalizacji ludzi i magów. Co więcej, jest poważny problem polityczny i techniczny - pallidanie zwalczają lokalnych drakolitów i kolonia ma problem typu wojna domowa. Na miejscu okazało się, że są CO NAJMNIEJ dwa stronnictwa a Klaudia uznała, że lokalna Hestia nie jest Hestią. Więc czym jest?

Aktor w Opowieści:

* Dokonanie:
    * przesłuchał Szczepana Kaltabena; przypadkiem Annika uznała go za dowódcę sił Orbitera i ufnie oddała się pod jego dowodzenie.


### To nie pułapka na Nereidę...

* **uid:** 220105-to-nie-pulapka-na-nereide, _numer względny_: 66
* **daty:** 0112-02-28 - 0112-03-04
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Izabela Zarantel, Kamil Lyraczek, Klaudia Stryk, Lutus Amerin, Maria Naavas, Natalia Aradin, Saitaer

Streszczenie:

Plan - dorwanie tajemniczych koloidowych korwet. Wiemy, że to pułapka, więc zróbmy kontr-pułapkę. Ale okazało się, że to nie pułapka na Nereidę a na Infernię. Standardowe badania wprowadziły w Infernię (Dianę) memetic payload i Diana prawie wpadła pod kontrolę Saitaera - odrzucenie kokonu z Natalią sprawiło, że udało się ciężko uszkodzoną Infernię uratować. Zwalczając ixion Arianna sięgnęła do Esuriit przez Elenę; Infernia by zginęła gdyby nie samobójcza akcja Izabeli i Kamila. Elena wchłonęła tyle energii ile była w stanie, przekraczając swój Wzór. ALE - Infernia przetrwała. Przy okazji, Natalia się wykluła. Ani osoba ani statek kosmiczny - mordercza anomalia kosmiczna.

Aktor w Opowieści:

* Dokonanie:
    * zintegrował się z zamrożoną Infernią (payload memetyczny) by uratować Dianę przed korupcją Saitaera. Ciężko ranny, ale udało mu się. Ratował życie WSZYSTKICH ryzykując swoim oraz Infernią. Opracował genialny plan, ale naprzeciw niemu stanął osobiście Saitaer.
* Progresja:
    * ciężko ranny, sprzężony z Infernią. Przeżył tylko dzięki niesamowitej skuteczności i bezwzględności Marii Naavas.


### Kult Saitaera w Neotik

* **uid:** 211222-kult-saitaera-w-neotik, _numer względny_: 65
* **daty:** 0112-02-24 - 0112-02-25
* **obecni:** Adam Szarjan, Arianna Verlen, Diana d'Infernia, Elena Verlen, Eustachy Korkoran, Izabela Zarantel, Kasandra Destrukcja Diakon, Klaudia Stryk, Lutus Amerin, Maciek Kalmarzec, Maria Naavas

Streszczenie:

Klaudia wróciła z bronią anty-Serenitową od Tosena, Arianna z feromonami Marii nadała Inferni nową kulturę - miłośnicy lolitek x kultyści x noktianie. Elena uziemiona w Inferni zinfiltrowała Stocznię ukryta przez Klaudię i odkryła niższe Spustoszenie + kult Saitaera. Nereida jest czysta od Saitaera; przetransferowana na Infernię dla Eleny i uzbrojona. Klaudia ma listę jednostek potencjalnie w rękach Saitaera / Spustoszonych. Przygotowania do kontrataku skończone.

Aktor w Opowieści:

* Dokonanie:
    * przekonał Dianę, że Elena musi być uziemiona w Inferni i sprawił, że Diana CHCE współpracować z Eustachym. Bardzo zajmował się Dianą by ją stabilizować i usprawnić.
* Progresja:
    * aspekt Nihilusa (Vigilus - Nihilus). Załoga i Kult jest przekonana, że jest Aspektem Niszczyciela.


### Sklejanie Inferni do kupy

* **uid:** 211215-sklejanie-inferni-do-kupy, _numer względny_: 64
* **daty:** 0112-02-21 - 0112-02-23
* **obecni:** Adam Szarjan, Arianna Verlen, Diana d'Infernia, Elena Verlen, Eustachy Korkoran, Kamil Lyraczek, Kasandra Destrukcja Diakon, Leona Astrienko, Maria Naavas, OO Infernia, Roland Sowiński

Streszczenie:

Ixioński wariant Inferni okazuje się całkiem słodki i pod kontrolą Eustachego. Morale Inferni jest w ruinie. Z pomocą Marii Arianna przesuwa kulturę Inferni w stronę "kultyści + noktianie + miłośnicy psychopatycznej lolitki Diany". Eustachy wpada w pułapkę Leony, która chce go wyleczyć czerwiem Esuriit - ale Eustachy przekonuje ją, że ma Dianę pod kontrolą. Jednak gdzieś w tle znajduje się prawdziwy ixioński potwór na stoczni Neotik.

Aktor w Opowieści:

* Dokonanie:
    * nie udało mu się ustabilizować załogi, ALE znalazł ixiońskiego potwora analizując systemy Stoczni (wpadając w pułapkę Kasandry/Leony). Gdy był związany, przekonał Leonę, że kontroluje Dianę. Więcej - zarejestrował dla potomności, że FAKTYCZNIE Diana nic głupiego nie zrobiła gdy był torturowany.


### O krok za daleko

* **uid:** 211208-o-krok-za-daleko, _numer względny_: 63
* **daty:** 0112-02-19 - 0112-02-20
* **obecni:** Adam Szarjan, Arianna Verlen, Diana d'Infernia, Elena Verlen, Eustachy Korkoran, Hestia d'Neotik, Jarosław Szarjan, Klaudia Stryk, Leona Astrienko, OO Infernia, Wawrzyn Rewemis

Streszczenie:

Klaudia doszła do tego, że Hestia d'Neotik oszukuje Stocznię; w okolicy są niewielkie statki koloidowe o których Stocznia nie wie. Arianna zintegrowała Eustachego z Infernią ixiońsko, niestety anomalizując Infernię. Elena ciężko Skażona ixionem i (w swoim mniemaniu) porzucona przez Eustachego. Eustachy ujarzmiając Infernię rozbijał ją o różne asteroidy, co powoduje śmierć części załogi. Infernia ma zerowe morale, ale jest opanowana. Przyjazna Anomalia Kosmiczna z tymczasową bazą w Stoczni Neotik.

Aktor w Opowieści:

* Dokonanie:
    * ixiońsko zintegrowany z Infernią; zdalnie się z nią łączy. Zdominował Infernię i pokazał jej, gdzie jej miejsce, acz kosztem 46 osób z załogi.
* Progresja:
    * zdalnie łączy się z Infernią; potrafi się z nią integrować i mieć sentisprzężenie, nawet zdalnie. Infernia się go słucha. I go lubi. POTĘŻNY BONUS przy działaniu z Infernią gdy sentisprzężony.


### Prototypowa Nereida Natalii

* **uid:** 211124-prototypowa-nereida-natalii, _numer względny_: 62
* **daty:** 0112-02-14 - 0112-02-18
* **obecni:** Adam Szarjan, Arianna Verlen, Arkadia Verlen, Elena Verlen, Eustachy Korkoran, Izabela Zarantel, Klaudia Stryk, Leona Astrienko, Maria Naavas, Natalia Aradin, OO Infernia, Roland Sowiński, Wawrzyn Rewemis

Streszczenie:

Kampania reklamowa "dołącz do Inferni" zrobiona przez Izę przy użyciu Eustachego jako modela - sukces. Klaudia zdobyła z tego nowych załogantów (acz to jeszcze potrwa). Szarjan uwolnił Klaudię z zarzutów, potrzebuje pomocy w stoczni - jego przyjaciółka Natalia ma problem z prototypem myśliwca klasy Nereida i została ixiońsko zintegrowana. Infernia próbowała Nereidę uratować, ale niestety skończyło się to poważnym uszkodzeniem Nereidy i stworzeniem ixiońskiego kokonu, z którego Natalia musi się wykluć...

Aktor w Opowieści:

* Dokonanie:
    * model i twarz kampanii "dołącz do Inferni". Opracował plan przechwycenia Nereidy, ale pod wpływem gniewu złamał plan i zestrzelił okrutnie Nereidę. Zintegrowany z Infernią poranił "nowych" członków załogi.
* Progresja:
    * jest solidnym bohaterem. Twardy i nie warto zaczepiać. Wszystko dzięki kampanii Izy.


### Porwany Trismegistos

* **uid:** 211117-porwany-trismegistos, _numer względny_: 61
* **daty:** 0112-02-09 - 0112-02-11
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Izabela Zarantel, Jamon Korab, Kalira d'Trismegistos, Klara Gwozdnik, Klaudia Stryk, Leona Astrienko, Maria Naavas, Mira Anastel, OO Tivr, Roland Sowiński, SC Trismegistos, Zygfryd Maus

Streszczenie:

Wiadomość od pirata że porwał Trismegistos poderwała Ariannę i Tivr. Historia nie trzymała się kupy. Załoga Inferni (Tivru) zorientowała się, że coś jest nie tak - opowieść o statku kupieckim który pozbył się miragenta brzmiała DZIWNIE. Okazało się, że Trismegistos jest statkiem kaperskim. Gdy Trismegistos zażądał poddania się Tivru Klaudia zrobiła sygnał "lol leci na Was Serenit" i TAI Trismegistosa spanikowała. Wolna TAI, współpracująca z fareilem i drakolitką. Wolny dystrykt. Arianna dała im żyć - ale odzyskała porwanych arystokratów i komory adaptacji.

Aktor w Opowieści:

* Dokonanie:
    * udawał kapitana Tivra, opracował kilka planów wejścia na Trismegistos i teatralnie przekonał wszystkich, że Serenit to tajna broń Orbitera (lub jego?).


### Romans dzięki Esuriit

* **uid:** 211110-romans-dzieki-esuriit, _numer względny_: 60
* **daty:** 0112-02-07 - 0112-02-08
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Karol Reichard, Klaudia Stryk, Leona Astrienko, Maria Naavas, Wawrzyn Rewemis

Streszczenie:

Infernia nie ma dość załogi, by móc działać, więc Termia przekazuje Ariannie Tivr. Leona nie może sobie wybaczyć, że jej nie było i to się stało, więc się wypisuje z pomocą Eustachego (co powoduje plotki Leona x Eustachy). Elena ucieka do Sektora 49 na K1 by być samotna; Arianna organizuje ostre poszukiwania. Eustachy znajduje Elenę by ją pocieszyć. Elena w końcu przyznaje Eustachemu, że go kocha. Maria Naavas z woli Martyna tymczasowo dołącza do załogi Arianny.

Aktor w Opowieści:

* Dokonanie:
    * wydostał Leonę ze szpitala na jej prośbę. Potem jednak przekierował się na Elenę, która uciekła i potrzebowała pomocy. Próbował ją uspokoić i wyjaśnić, że będzie dobrze. Faktycznie, uspokoił ją trochę. Pokazał że jej ufa. Acz się napromieniował (co jest nieprzyjemne).
* Progresja:
    * plotki, że zadaje się z Leoną na Inferni. Że ona jest jego stałą partnerką. Kinky pair of doom.


### Kurczakownia

* **uid:** 211020-kurczakownia, _numer względny_: 59
* **daty:** 0112-01-18 - 0112-01-20
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Flawia Blakenbauer, Kamil Lyraczek, Klaudia Stryk, Martyn Hiwasser, OO Infernia, Otto Azgorn, Vigilus Mevilig

Streszczenie:

By uratować jak najwięcej osób w Sektorze Mevilig, Eustachy zaprojektował Kurczakownię - maszynę do oddzielania mechanicznego głów i rdzenia od reszty człowieka. Arianna starła się ze Zbawicielem-Niszczycielem jako aspekt Zbawiciela, wspierana przez Działo Rozpaczy (Eustachy). Udało im się sporo uratować (40% populacji Planetoidy Kalarfam), acz załoga Inferni będzie mieć koszmary senne. Klaudia zebrała dość danych, by zapobiec naturalnej manifestacji Zbawiciela-Niszczyciela w Sektorze Astoriańskim i zrobić kontrakcję jak co.

Aktor w Opowieści:

* Dokonanie:
    * pod wpływem Esuriit zmasakrował z komandosami grupę lokalsów na wrakowisku. Potem na zimno opracował Kurczakownię - oddzielanie mechanicznego mięsa. Gdy Arianna była dominowana przez Zbawiciela-Niszczyciela, strzelił weń z Działa Rozpaczy, uszkadzając je na zawsze.


### Szara nawałnica

* **uid:** 211013-szara-nawalnica, _numer względny_: 58
* **daty:** 0112-01-12 - 0112-01-17
* **obecni:** Aleksandra Termia, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Martyn Hiwasser, Otto Azgorn, Remigiusz Błyszczyk, TAI Rzieza d'K1

Streszczenie:

Zespół przygotował się na wejście do Mevilig. Rzieza odkrył jak działają Piranie i że tamten sektor jest opanowany przez TAI 5+ generacji. Niestety, Infernia musiała uciekać przed Rziezą. Na miejscu Eustachy znalazł skuteczny sposób przemykania między Piraniami, dotarli do planetoidy Kalarfam gdzie znaleźli bazę ludzi. Mechaniczny "smok" został zniszczony przez Infernię, pobrali lokalsów i Arianna przekonała ich do zmiany kultu. Aha, anomalny wybuch torpedy anihilacyjnej Arianna przekierowała na planetę, wypowiadając ostrą wojnę TAI 5+ generacji...

Aktor w Opowieści:

* Dokonanie:
    * refitował Infernię by móc wysadzić torpedę Rziezy, znalazł sposób jak omijać skutecznie Piranie, sondami sejsmicznymi znalazł bazę ludzi w Kalarfam i użył torpedy anihilacyjnej by zabić "smoka".


### Grupa Ekspedycyjna Kellert

* **uid:** 210929-grupa-ekspedycyjna-kellert, _numer względny_: 57
* **daty:** 0112-01-07 - 0112-01-10
* **obecni:** Adam Nerawol, Aleksandra Termia, Antoni Kramer, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Olena Orion, OO Infernia, OO Omega Septius, Otto Azgorn, TAI Marszałek Grzmotoszpon Trzeci, Vigilus Mevilig

Streszczenie:

Termia wysłała siły do sektora "Noviter", ale to był inny sektor (Mevilig). Straciła bezzałogowce, potem grupę ekspedycyjną Kellert. Kramer wysłał Infernię zakoloidowaną; okazało się, że ten sektor ma "strażnika Esuriit" zrobionego przez ludzi i grupę Piranii, sterowanych przez TAI 4 poziomu. Inferni udało się zebrać dane i wrócić z siedmioma uratowanymi członkami OO Savera, gdzie większość załogi została corruptowana przez Vigilusa...

Aktor w Opowieści:

* Dokonanie:
    * sterował Piraniami używając "torped z sonarem", robiąc przejście dla Inferni. Potem manipulował lokalizacją fragmentu statku z Vigilusem by Martyn mógł ratować ludzi.


### Stabilizacja Bramy Eterycznej

* **uid:** 210901-stabilizacja-bramy-eterycznej, _numer względny_: 56
* **daty:** 0111-12-05 - 0111-12-08
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Flawia Blakenbauer, Gilbert Bloch, Janus Krzak, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Medea Sowińska, OO Kanagar, OO Netrahina, OO Trasman, Seweryn Atanair

Streszczenie:

Infernia wróciła z innymi jednostkami naprawić Bramę Kariańską. Eustachy ufortyfikował i uniewidocznił pintki; Arianna pozyskała dane z reanimowanego magią krążownika Orbitera. Przez Bramę przeszła ON Catarina; Klaudia ją zlokalizowała i wystawiła Netrahinie i Niobe. Niestety, Arianna hiperlinkowała się z ON Spatium Gelida. Dla nich minęło tylko kilka godzin, dla nas - kilkadziesiąt lat...

Aktor w Opowieści:

* Dokonanie:
    * mistrzowski plan i inżynieria by ochronić pinasy przed efemerydami i naprawić bezproblemowo Bramę Kariańską; Elena pod Esuriit chce założyć mu pas cnoty.


### Uszkodzona Brama Eteryczna

* **uid:** 210825-uszkodzona-brama-eteryczna, _numer względny_: 55
* **daty:** 0111-11-30 - 0111-12-03
* **obecni:** AK Nocna Krypta, Arianna Verlen, Diana d'Infernia, Elena Verlen, Eustachy Korkoran, Flawia Blakenbauer, Gilbert Bloch, Klaudia Stryk, Medea Sowińska, ON Spatium Gelida, OO Infernia, OO Mfumo

Streszczenie:

Infernia została przechwycona przez Medeę lecąc na Anomalię Kolapsu i przekierowana na uszkodzoną Bramę Eteryczną (gdzie, jak się okazuje, są pasożyty i flota noktiańska która nie doleciała). Na miejscu Infernia uniknęła efemeryd i channelując Kryptę uratowała wszystkich efemerydami. Gdy Krypta pojawiła się na serio, Infernia zwiała paląc silniki. Statki noktiańskie w Bramie mają tether na Infernię.

Aktor w Opowieści:

* Dokonanie:
    * świadek tego jak "Diana" i Elena o niego walczyły; autor pomysłu, by Infernię zamaskować jako noktiański statek by efemerydy Infernię wspierały. Po raz pierwszy UNIKAŁ walki i modulował sygnaturę ekranowania tak, by efemerydy nie zbliżały się do Inferni.


### Nieudana infiltracja Inferni

* **uid:** 210616-nieudana-infiltracja-inferni, _numer względny_: 54
* **daty:** 0111-11-22 - 0111-11-27
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Flawia Blakenbauer, Izabela Zarantel, Kamil Lyraczek, Klaudia Stryk, Leona Astrienko, Marian Tosen, OO Opresor, Roland Sowiński

Streszczenie:

Marian Tosen poprosił Infernię o to, by sprawdzili co się dzieje w Anomalii Kolapsu - podejrzewa anomalie memetyczne, skoro Orbiter traci tam statki  (m.in. OO Opresor) i pojawiają się jednostki nieoznaczonej frakcji. Tymczasem Roland Sowiński chce na pokład. Arianna wybiła mu to z głowy, ale wykryli agentkę wśród jego guwernantek - Flawię Blakenbauer, która miała się nim opiekować i jest trochę zniewolona przez Sowińskich. By zadośćuczynić po tym co Elena zrobiła i trochę Flawii pomóc, Flawia dołączyła do Inferni. "Oficjalnie" udało jej się Infernię zinfiltrować...

Aktor w Opowieści:

* Dokonanie:
    * dobry glina i zły glina jednocześnie - zdecentrował i ogłupił Flawię, po czym pokazał jej, że Infernia ma świetny system zabezpieczeń przed infiltracją. Szacun!


### Siostrzenica Morlana

* **uid:** 210818-siostrzenica-morlana, _numer względny_: 53
* **daty:** 0111-11-15 - 0111-11-19
* **obecni:** Antoni Kramer, Arianna Verlen, Eustachy Korkoran, Klaudia Stryk, Leona Astrienko, Maria Naavas, Nataniel Morlan, Ofelia Morlan, Olgierd Drongon, OO Netrahina, OO Żelazko, SC Fecundatis, SC Światłodóbr, Tomasz Sowiński

Streszczenie:

Tomasz Sowiński próbuje uratować Ofelię Morlan przed Natanielem Morlanem. Nie ma kogo poprosić a Ofelia służy na Netrahinie; poprosił więc Ariannę. Arianna skanując echo pamięci Jolanty w Inferni zdobywa informacje o programie kosmicznym Orbitera, przekazuje to Kramerowi, wykorzystuje wsparcie Olgierda i robią ćwiczenia Żelazko - Netrahina. Ratują Ofelię przed porwaniem przez koloidowy statek Kruków Kasandry.

Aktor w Opowieści:

* Dokonanie:
    * fabrykuje sygnaturę statku koloidowego, po czym magią ratuje Ofelię przed Eidolonami (eksplozja kierunkowa wywala ich ze statku).


### Sekrety Kariatydy

* **uid:** 210609-sekrety-kariatydy, _numer względny_: 52
* **daty:** 0111-11-05 - 0111-11-08
* **obecni:** Arianna Verlen, Eustachy Korkoran, Marian Tosen, OO Kariatyda, Rafael Galwarn, Roland Sowiński

Streszczenie:

Arianna chce Tivr do swojej floty. Do tego celu zdecydowała się poznać sekrety Kariatydy - czemu Walrond tak unika tego statku i nazwy? Niestety, Arianna i Eustachy rozognili opowieść o "Orbiterze, który porzucił swoich w Anomalii Kolapsu" a Arianna dowiedziała się dyskretnie, że OO Kariatyda to specjalny statek - nie można powiedzieć, że był zniszczony. Ten statek został porwany przez TAI i uciekł do Anomalii Kolapsu. Arianna, promienna mistrzyni PR, zmieniła to w "KONKURS. Wyślij KARIATYDA i wygrasz przejażdżkę luksusowym statkiem". Aha, Roland Sowiński (uratowany przez Ariannę z Odłamka Serenita) chce się z nią ożenić.

Aktor w Opowieści:

* Dokonanie:
    * by zdobyć pieniądze i środki na wyprawę do Anomalii Kolapsu przy marnych środkach z Aurum rozpalił arystokratki opowieścią o Kariatydzie.


### Morderstwo na Inferni

* **uid:** 210526-morderstwo-na-inferni, _numer względny_: 51
* **daty:** 0111-10-26 - 0111-11-01
* **obecni:** Antoni Kramer, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Feliks Walrond, Kamil Lyraczek, Karol Reichard, Klaudia Stryk, Leona Astrienko, Marian Tosen, Martyn Hiwasser, OO Tivr, Otto Azgorn, Tal Marczak

Streszczenie:

Noktiański mechanik Inferni, Tal Marczak, chciał zniszczyć OO Tivr (własność noktiańskiej rodziny Tala). Noktianie trzymają się razem - inni mechanicy Inferni doprowadzili do morderstwa Tala na K1 i wzięli amnestyki, by nikt nie wiedział co się stało. Zespół Inferni skutecznie doszedł do tego o co chodzi, odpowiedzialnych za morderstwo noktian przeniesiono na Żelazko a Arianna poważnie opieprzyła swoją noktiańską załogę. Będzie lepiej w przyszłości.

Aktor w Opowieści:

* Dokonanie:
    * chciał leniwie zrzucić poszukiwania Tala na wszystkich innych, ale jak Tal zginął to wyciągnął kluczowe informacje rozmawiając ze swoimi technikami.


### Osiemnaście Oczu

* **uid:** 210519-osiemnascie-oczu, _numer względny_: 50
* **daty:** 0111-10-09 - 0111-10-20
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Leona Astrienko, Marian Tosen, Martyn Hiwasser, OO Alaya, OO Tivr

Streszczenie:

OO Alaya została zainfekowana anomalią memetyczną, infohazardem. Marian Tosen użył Zespołu by zneutralizować infohazard. Zespół przeszedł przez serię iteracji używając amnestyków by w końcu rozwiązać problem Alayi - amnestyki na całą Alayę. Do końca Zespół nie ma pojęcia czym ta anomalia była (bo wiedza ich by zaraziła). Udało się ewakuować większość Alayi.

Aktor w Opowieści:

* Dokonanie:
    * operator roju robotów (który sam skonstruował) celem dywersji, mapowania drogi i unieszkodliwiania. Twórca planów taktycznych.
* Progresja:
    * wysokie uznanie ze strony Mariana Tosena z grupy antymemetyczej Orbitera.


### Ewakuacja z Serenit

* **uid:** 210512-ewakuacja-z-serenit, _numer względny_: 49
* **daty:** 0111-09-25 - 0111-10-04
* **obecni:** Aida Serenit, AK Serenit, Arianna Verlen, Bogdan Anatael, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Martyn Hiwasser, OE Falołamacz, OO Infernia, Roland Sowiński

Streszczenie:

Klaudia spowolniła Serenit, po czym Elena weszła zdalnym Entropikiem na pokład Falołamacza. Arianna zmanipulowała ludzi by dało się ich uratować. Samobójcza pintka zniechęciła Falołamacz do dalszej walki. Nie wszystkich udało się uratować (ale kapitana i arystokratę Sowińskich tak). Niestety, Infernia skończyła jako kosmiczny wrak i musiała wysłać SOS...

Aktor w Opowieści:

* Dokonanie:
    * zrobił reaktywny pancerz na Inferni, ostrzelał Falołamacz i wprowadził tam Entropika po czym odstrzelił ładownię gdzie są ludzie. Inżynier-artylerzysta w pełnej mocy.


### Infekcja Serenit

* **uid:** 210428-infekcja-serenit, _numer względny_: 48
* **daty:** 0111-09-24 - 0111-09-25
* **obecni:** AK Serenit, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Martyn Hiwasser, OE Falołamacz, OE Piękna Elena, OO Infernia, Persefona d'Infernia, Rafał Grambucz, Tadeusz Ursus

Streszczenie:

Ciężko uszkodzona Infernia wraca do domu i dostaje SOS od Ursusa - OE Falołamacz "zniknął", coś z anomaliami. Gdy Zespół dotarł do Falołamacza, próbowali dowiedzieć się co się dzieje - faktycznie mamy anomaliczny okręt. Podczas badań Falołamacza Morrigan przejęła Persefonę Inferni; Eustachy musiał poważnie uszkodzić Infernię by nie zniszczyli Falołamacza. Po kosmicznym spacerze Eleny i Klaudii doszli do tego co się stało - Falołamacz staje się Odłamkiem Serenit. I jeszcze da się ludzi ratować. Ale Infernia jest w bardzo złym stanie...

Aktor w Opowieści:

* Dokonanie:
    * rozpaczliwie przekierował ogień Inferni (Morrigan) na resztki Goldariona, sabotował Infernię by odciąć Morrigan i osłaniał artyleryjsko Klaudię i Elenę jak uciekały z Falołamacza przed Odłamkiem Serenit.
* Progresja:
    * bonus do niszczenia Inferni. Nie "pozorowanego" a "faktycznego". Eustachy potrafi niszczyć / sabotować Infernię jak nikt inny.


### Znudzona załoga Inferni

* **uid:** 210421-znudzona-zaloga-inferni, _numer względny_: 47
* **daty:** 0111-09-18 - 0111-09-21
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Jolanta Sowińska, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Morrigan d'Tirakal, Otto Azgorn, Persefona d'Infernia, Tomasz Sowiński

Streszczenie:

Po raz pierwszy od dawna Infernia jest w stanie rozprzężenia - wracają z daleka po trudnej misji i nic im nie grozi. Jako, że załoga ma straszne tarcia (Leona poluje na "słabe jednostki", komandosi Verlenów chcą zatrzymać Leonę, noktianie vs astorianie...), Arianna autoryzuje Eustachego do zrobienia "Tirakala" jako niebezpieczną sytuację. Eustachy reanimuje Tirakala, ale niestety cholerstwo zanomalizowało - przywrócił Morrigan. By Zespół nie rozwiązał Tirakala za szybko, Eustachy trochę sabotował Infernię co Elena przeczytała jako... umówienie się na randkę. Gdy Klaudia wykryła, że na INFERNI pojawiła się straszna anomalia (Morrigan) próbowali to usunąć - ale Eustachy przecież sabotował Infernię! Udało im się opanować Morrigan, ale kosztem uszkodzeń i obrażeń na Inferni. I scenek kompromitujących Jolantę Sowińską. Infernia jest uszkodzona, ale się trzyma.

Aktor w Opowieści:

* Dokonanie:
    * reanimując Tirakala zreanimował Morrigan. Sabotował Infernię i przez przypadek dał Elenie znać że mu na niej zależy. Gdy się z nim skonfrontowała, przypadkowo ją odstraszył (bo "niegodne z dowódcą").


### Dekralotyzacja Asimear

* **uid:** 210414-dekralotyzacja-asimear, _numer względny_: 46
* **daty:** 0111-09-05 - 0111-09-14
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Kamil Lyraczek, Klaudia Stryk, Llarnagraht, Malictrix d'Pandora, Mariusz Tubalon, Martyn Hiwasser, Martyna Bianistek, SCA Płetwal Błękitny, Tomasz Sowiński

Streszczenie:

Wiedząc, że Jolanta jest pod wpływem kralotha Infernia sfabrykowała kralotyczny alergizator. Arianna zdradziła się Aesimar i powiedziała, że ma zamiar zniszczyć kralotha z ramienia Orbitera. Udało się kralotha zmiażdżyć i zepchnąć do podziemi Asimear, choć ze sporymi stratami ludzkimi; tymczasem Tirakal uciekł na Płetwala. Elena zinfiltrowała Płetwala, ale nie mogła zniszczyć Tirakala nie krzywdząc ludzi. Destrukcję Tirakala Infernia zostawiła więc Malictrix.

Aktor w Opowieści:

* Dokonanie:
    * taktycznie dowodzi operacją 'dekralotyzacja Asimear', używając sił porządkowych Lazarin i sił Inferni. Potem desantuje Elenę w Eidolonie na Płetwala, z dywersją odwracającą uwagę Morrigan.


### Arianna podbija Asimear

* **uid:** 210317-arianna-podbija-asimear, _numer względny_: 45
* **daty:** 0111-08-20 - 0111-09-04
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Jolanta Sowińska, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Martyna Bianistek, SCA Płetwal Błękitny, Tomasz Sowiński

Streszczenie:

"Płetwal Błękitny" ma na pieńku z "Goldarionem"; by nie złamać konspiracji, Klaudia potraktowała Płetwala Bazyliszkiem. Tymczasem pojawiła się wiadomość, że Arianna chce zniszczyć Asimear i żąda wydania Sowińskiej. Prawdziwa Arianna złamała konspirę, skontaktowała się z Jolantą Sowińską, po czym ją porwała. Okazało się, że Jolanta jest kontrolowana przez kralotycznego pasożyta...

Aktor w Opowieści:

* Dokonanie:
    * udając Aleksandra Leszerta irytował Martynę (kapitan Płetwala), by utrzymać konspirację - nie są Infernią, są Goldarionem. Polubił bycie kapitanem.


### Porwanie cywila z Kokitii

* **uid:** 200429-porwanie-cywila-z-kokitii, _numer względny_: 44
* **daty:** 0111-08-03 - 0111-08-08
* **obecni:** Alara Ehmes, Antoni Kramer, Arianna Verlen, Eustachy Korkoran, Klaudia Stryk, Leona Astrienko, Medea Sowińska

Streszczenie:

Infernia jakoś nie może zostać naprawiona a skarga Mateusza Sowińskiego nie pomaga. Po śledztwie Zespołu okazuje się, że Infernia jest kanibalizowana przez Niobe, do jakiejś misji sił specjalnych. Arianna z ekipą wkręciła się na tą misję, przekonując Medeę Sowińską, że bez niej to się nie uda. Misja polegała na ataku na Kokitię pomiędzy Bramami i ekstrakcja z Kokitii cywila, udając piratów. Dzięki załodze Inferni udało się to lepiej niż ktokolwiek mógłby się spodziewać i niż Medea zaplanowała sama.

Aktor w Opowieści:

* Dokonanie:
    * stał lojalnie za Arianną w konflikcie z Medeą oraz mistrzowsko skołował Kokitię strzałami artylerii - nigdy nie wiedzieli, że ktokolwiek ich zaatakował.
* Progresja:
    * łaskawe oko Sił Specjalnych, frakcja "Gorący Lód" oraz Medei Sowińskiej. Też plotki że współpracuje z tymi radykałami sił specjalnych Orbitera.


### Infernia jako Goldarion

* **uid:** 210218-infernia-jako-goldarion, _numer względny_: 43
* **daty:** 0111-07-19 - 0111-08-03
* **obecni:** Antoni Kramer, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Jolanta Sowińska, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, OO Infernia, Tomasz Sowiński

Streszczenie:

Infernia udaje Goldariona, by przetransportować tajną przesyłkę Sowińskich do Jolanty Sowińskiej na planetoidzie Asimear. Tomasz Sowiński na Inferni okazuje się być dużym dzieckiem, łatwo jest wmanipulowany w otwarcie paczki - jest tam nietypowy antynanitkowy Entropik (którego psuje Elena przez błędną integrację). W tle wszystkiego - próba zdobycia niezależnej od Orbitera floty przez Aurum i jeden znikający advancer Kramera na Asimear...

Aktor w Opowieści:

* Dokonanie:
    * udaje dowódcę Goldariona; przebudował Infernię w Czarny Grom a Czarny Grom w Goldariona. Zdobył szacun Tomasza Sowińskiego, który go uważa za swego chłopa.


### Sympozjum Zniszczenia

* **uid:** 210120-sympozjum-zniszczenia, _numer względny_: 42
* **daty:** 0111-06-03 - 0111-06-07
* **obecni:** Arianna Verlen, Diana Arłacz, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Maria Naavas, Martyn Hiwasser, OA Bakałarz, Olgierd Drongon

Streszczenie:

Maria Naavas próbuje spiknąć Ariannę i Olgierda i do pomocy używa Eustachego. Później - Eustachy, Diana, Klaudia i Elena są na Sympozjum Zniszczenia OA Bakałarz. Próbują przebić się przez anomalną materię. Diana odkrywa słabość owej materii do Esuriit i się Skaża; Skażenie rozlewa się po Bakałarzu i Zespół dostaje opiernicz za niekompetencję (większość na Elenę). Zespół w gniewie, nienawiści i Esuriit fabrykuje Emiter Plagi Nienawiści - nową broń na pokład Inferni, piekielnie niebezpieczną...

Aktor w Opowieści:

* Dokonanie:
    * nakłonił Dianę do zabawy Esuriit i przekroczenia granicy - po raz pierwszy ever. Potem wyciągnął Dianę z obsesji Esuriit. I z Klaudią zrobił Emiter Plagi Nienawiści.
* Progresja:
    * reputacja "świetni w niszczeniu i destrukcji, ale idźcie na bok i nie psujcie więcej". Takie trochę dzieci od zniszczenia.


### SOS z haremu

* **uid:** 210106-sos-z-haremu, _numer względny_: 41
* **daty:** 0111-05-28 - 0111-05-29
* **obecni:** Arianna Verlen, Diana Arłacz, Elena Verlen, Eustachy Korkoran, Horacy Aktenir, Julia Aktenir, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Rozalia Teirik

Streszczenie:

Pałacyk rodu Aktenir został infestowany przez kralotha. Julia, zesłana z Orbitera za zerwanie z przeciwnikiem Eustachego, wysłała SOS do Orbitera. Infernia rusza Julii na pomoc. Gdy odkryli że chodzi o kralotha to zamiast wchodzić na kralotyczny teren, zorganizowali plan "arystokratki na orgię" i wyciągneli kralotycznego niewolnika (Horacego Aktenira) na otwartą przestrzeń, po czym Arianna uruchomiła obcą sentisieć a Eustachy zawołał do Julii rozmytej w środku...

Aktor w Opowieści:

* Dokonanie:
    * rozkochał w sobie Rozalię, po czym skłonił ją do pójścia z nią na Infernię. Pozwolił na to, by jego reputacja ucierpiała ("pornmaster") by ratować Julię Aktenir przed kralothem.
* Progresja:
    * wabi dziewczyny na orgię, potem nic z tego nie ma. Wtf. Z jednej strony reputacja skandalisty, z drugiej reputacja "nie kończy". Ale skojarzenie Eustachy - orgia zostaje.
    * zakochują się w nim beznadziejnie Rozalia Teirik i Julia Aktenir. Ale Eustachy ostro podpada Elenie.
    * wdzięczność rodu arystokratycznego Aktenir; potencjalni sojusznicy.


### Porwanie Anastazji z Odkupienia

* **uid:** 210127-porwanie-anastazji-z-odkupienia, _numer względny_: 40
* **daty:** 0111-05-24 - 0111-05-25
* **obecni:** Anastazja Sowińska Dwa, Arianna Verlen, Dariusz Krantak, Diana Arłacz, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Martyn Hiwasser, OA Odkupienie, OO Infernia, Rufus Niegnat, SP Plugawy Jaszczur

Streszczenie:

Arianna, Eustachy i Klaudia oddali "Anastazję" na OA Odkupienie tylko po to, by ją stamtąd porwać. Nawet załoga Inferni o tym nie miała pojęcia. Okazało się, że Odkupienie jest sentisieciowanym specjalnym statkiem Aurum, gdzie ci co narazili się Sowińskim mają szansę na "odkupienie".

Aktor w Opowieści:

* Dokonanie:
    * sabotował Infernię by odwrócić uwagę Eleny, skłócał Elenę i Dianę by nie wiedziały o akcji odbijania Anastazji a potem magią (wybuchów) odpychał Dariusza by kupić Klaudii czas.


### Pułapka z Anastazji

* **uid:** 201230-pulapka-z-anastazji, _numer względny_: 39
* **daty:** 0111-05-21 - 0111-05-22
* **obecni:** AK Nocna Krypta, AK Rodivas, Anastazja Sowińska Dwa, Arianna Verlen, Damian Orion, Elena Verlen, Eustachy Korkoran, Henryk Sowiński, Klaudia Stryk, Martyn Hiwasser, OA Zguba Tytanów, OO Infernia

Streszczenie:

Krypta "oddała Anastazję", zmieniając cywilny statek Rodivas w pułapkę na Ariannę.

Aktor w Opowieści:

* Dokonanie:
    * przesterował systemy Inferni, by zdążyć uratować Zgubę Tytanów przed AK Rodivas. Potem próbował "uratować Anastazję", ale zrobił za duże zniszczenia bo się popisywał.
* Progresja:
    * pod wrażeniem umiejętności technicznych Eleny - jej pilotażu, jej ognistego profesjonalizmu, jej skłonności do ryzyka.


### Mychainee na Netrahinie

* **uid:** 201111-mychainee-na-netrahinie, _numer względny_: 38
* **daty:** 0111-05-07 - 0111-05-10
* **obecni:** Arianna Verlen, Eustachy Korkoran, Halina Szkwalnik, Klaudia Stryk, Martyn Hiwasser, Rufus Komczirp

Streszczenie:

Komczirp pod wpływem anomalicznych grzybów (Mychainee) próbował porwać Ariannę by ta pomogła Netrahinie. Arianna nie dała się porwać, ale z Zespołem wbiła na Netrahinę i zniszczyła anomalię. Niestety, Netrahina została bardzo poważnie uszkodzona, ale nie ma dużych strat w ludziach.

Aktor w Opowieści:

* Dokonanie:
    * zaimponował Dianie selfie z wybuchami; uszkodził Netrahinę bardziej niż musiał. Zaplanował jak wbić na Netrahinę. Mistrz artylerzysta; wpakował Saabar na torpedę abordażową.


### Infernia jest nasza!

* **uid:** 210804-infernia-jest-nasza, _numer względny_: 37
* **daty:** 0111-04-23 - 0111-04-26
* **obecni:** Aleksandra Termia, Antoni Kramer, Arianna Verlen, Artur Traffal, Diana d'Infernia, Eustachy Korkoran, Franciszek Maszkiet, Klaudia Stryk, Leona Astrienko, Maciej Żarand, OO Samotność Gwiazd

Streszczenie:

Ku wielkiemu niezadowoleniu Zespołu, Infernia miała im być odebrana i złomowana. Mieli dostać nowy tańszy statek. Zespół odkrył że za tym stoi komodor Traffal z sił Termii który przy użyciu Inferni chciał złapać w potrzask arystokratę eternijskiego który był kultystą magii. Zespół odzyskał Infernię, przesunął kultystę w kierunku na Ariannę i uciemiężył komodora Traffala - będzie miał pecha na K1.

Aktor w Opowieści:

* Dokonanie:
    * wkradł się na Infernię by nikt mu jej nie zabrał, po czym przejął nad nią kontrolę i się z nią magicznie neurosprzęgł, po czym złapał w pułapkę kultystę magii - eternianina tien Żaranda.
* Progresja:
    * ma możliwość magią "neurosprzężenia" z Infernią przez anomalny rdzeń Inferni.


### W cieniu Nocnej Krypty

* **uid:** 210728-w-cieniu-nocnej-krypty, _numer względny_: 36
* **daty:** 0111-03-22 - 0111-04-08
* **obecni:** AK Nocna Krypta, Arianna Verlen, Atrius Kurunen, Eustachy Korkoran, Finis Vitae, Gerard Adanor, Helena Adanor, Janus Krzak, Oliwia Karelan, Romana Arnatin, Romana Arnatin, Ulisses Kalidon

Streszczenie:

Jak wrócić z tajnej noktiańskiej bazy oderwanej od świata i Bram? Oczywiście, Nocną Kryptą. Arianna wezwała Kryptę i Infernia schowała się w jej cieniu przenosząc się między rzeczywistościami. Po drodze udało się Inferni doprowadzić do zniszczenia niewielkiej niegroźnej floty używając Krypty, trafiła do przeszłości Krypty i widziała Finis Vitae - ale wróciła przez Anomalię Kolapsu do domu. Bo Krypta jest połączona z Anomalią Kolapsu. Aha, część noktian z Inferni została w Zonie Tres.

Aktor w Opowieści:

* Dokonanie:
    * Pozyskując złom w Nierzeczywistości zapewnił efemerydę Diany Inferni; też wyrwał Kryptę z koszmarnego snu, by Arianna mogła wrócić do domu.
* Progresja:
    * Blizna "Krwawy". Gdy sytuacja jest zła, nie patrzy na nic i nikogo, ratuje swoją skórę - robi się ultrapragmatyczny i nie przejmuje się komu robi krzywdę.
    * Jest powiązany historycznie z Infernią. Gdy Infernia anomalizowała, to on i jego zespół/rodzina doprowadzili Infernię do działania. A Infernia wymaga okresowych uszkodzeń.


### Pierwsza BIA mag

* **uid:** 210721-pierwsza-bia-mag, _numer względny_: 35
* **daty:** 0111-03-19 - 0111-03-21
* **obecni:** Arianna Verlen, BIA Solitaria d'Zona Tres, Eustachy Korkoran, Janus Krzak, Klaudia Stryk, Martyn Hiwasser, Romana Arnatin

Streszczenie:

Nie wiadomo gdzie jest rogue planet. Nie ma sensownej opcji powrotu. Infernia zaskarbiła sobie współpracę z BIA, ale podczas pracy BIA wykryła magię. Martyn przekonał BIA, że tkanka magiczna to "wszczepy" - BIA pozwoliła na używanie magii i faktycznie, to pomogło w naprawianiu bazy. Martyn i Eustachy rozdzielili Klaudię i BIA, ale Klaudia jest w stazie a BIA uzyskała moce magiczne. Jak wrócić? Ano, jedyną opcją jest w sumie w cieniu Nocnej Krypty...

Aktor w Opowieści:

* Dokonanie:
    * naprawia Zonę Tres - wpierw używając narzędzi a potem wspomagając się magią. Potem pomaga Martynowi rozdzielić Klaudię i BIA.


### Baza Zona Tres

* **uid:** 210714-baza-zona-tres, _numer względny_: 34
* **daty:** 0111-03-16 - 0111-03-18
* **obecni:** Arianna Verlen, BIA XXX d'Zona Tres, Elena Verlen, Eustachy Korkoran, Janus Krzak, Martyn Hiwasser, Ulisses Kalidon

Streszczenie:

Podczas eksploracji archaicznej noktiańskiej bazy Infernia doszła do tego, że to mityczna Zona Tres - odpowiednik "area 51", z badaniami nad magią. Infernia przez przypadek uruchomiła Bię. Zespół zamaskował Infernię jako Alivię Nocturnę i Arianna przekonała Bię do wstępnej współpracy. Okazało się, że Bia jest chora i nie postrzega wszystkiego prawidłowo, Klaudia jest z Bią sprzężona (oops). By dowiedzieć się więcej, Infernia wyleciała na orbitę i okazało się, że są na rogue planet - planecie która wypadła z orbity i zagubiła się w kosmosie...

Aktor w Opowieści:

* Dokonanie:
    * odnalazł do których wzmacniaczy łączy się Klaudia, zamaskował w komputerach Zony Tres Infernię jako Alivię Nocturnę oraz nic nie wysadził - poza lodem planety pod wodą której się znajdują.


### Po drugiej stronie Bramy

* **uid:** 210707-po-drugiej-stronie-bramy, _numer względny_: 33
* **daty:** 0111-03-13 - 0111-03-15
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Janus Krzak, Klaudia Stryk, Martyn Hiwasser, Raoul Lavanis, Tal Marczak

Streszczenie:

Infernia próbując przetransportować eksperta Eteru na konferencję przez Bramę uległa katastrofie - przeniosła się GDZIEŚ INDZIEJ, pod wodę i zniknęła Klaudia. Nie uległa katastrofalnym uszkodzeniom, ale po zachęceniu pintką Lewiatana dotarła do archaicznej podwodnej noktiańskiej bazy. Tam znajduje się Klaudia, ale coś jest z nią nie tak. Sprzęgnięta z bazą? Tak czy inaczej, infiltracja bazy dopiero się zacznie. Trzeba znaleźć Klaudię i wrócić do domu.

Aktor w Opowieści:

* Dokonanie:
    * Absolutny MVP; utrzymał Infernię przed zmiażdżeniem pod wodą i przekształcił pintkę w "randkę" by Lewiatan się nie zaprzyjaźnił z Infernią za bardzo.


### Krypta i Wyjec

* **uid:** 201216-krypta-i-wyjec, _numer względny_: 32
* **daty:** 0111-01-29 - 0111-01-31
* **obecni:** AK Nocna Krypta, AK Wyjec, Anastazja Sowińska, Arianna Verlen, Diana Arłacz, Eustachy Korkoran, Kamil Lyraczek, Klaudia Stryk, Martyn Hiwasser

Streszczenie:

Podczas walki Nocnej Krypty i Wyjca Zespół wbił się na Nocną Kryptę, by wyleczyć się z Różowej Plagi. Na miejscu okazało się, że Krypta ma "nowe programowanie" (leczenie lub 'zachowanie'), nowe mechanizmy (MIRV, anomalne działo, anioły) oraz że jest groźniejsza niż kiedykolwiek. Arianna zostawiła na pokładzie Krypty Anastazję, by Krypta naprawiła jej Wzór, po czym oszukała Sowińskich, że niby Krypta porwała Anastazję.

Aktor w Opowieści:

* Dokonanie:
    * mistrz planu inscenizacji "Krypta porwała Anastazję", taktyk. Ogłuszył Martyna, by ten nie wiedział o tym że zostawiają Anastazję. Dużo wybuchów ;-).


### Pocałunek Aspirii

* **uid:** 201210-pocalunek-aspirii, _numer względny_: 31
* **daty:** 0111-01-26 - 0111-01-29
* **obecni:** AK Nocna Krypta, AK Wyjec, Anastazja Sowińska, Arianna Verlen, Donald Parziarz, Eustachy Korkoran, Juliusz Sowiński, Katra Igneus, Klaudia Stryk, OA Zguba Tytanów, OO Infernia

Streszczenie:

Juliusz Sowiński zażądał oddania Anastazji, ale ona nie chce wracać - jej reputacja i stan są w ruinie. Anastazja chce być jak Elena, ku zgryzocie Juliusza. Tymczasem Arianna zastawiła pułapkę na koloidowy statek - "Pocałunek Aspirii" - i go zestrzeliła. Po czym wezwała Nocną Kryptę by zdjąć z nich klątwę miłości i naprawić co się da z "Pocałunku" i AK Wyjec.

Aktor w Opowieści:

* Dokonanie:
    * przebudował (konstrukcja) Infernię by wyglądała jak Blask Aurum. Potem - zestrzelił Pocałunek Aspirii.


### Anastazja - bohaterką?

* **uid:** 201118-anastazja-bohaterka, _numer względny_: 30
* **daty:** 0111-01-22 - 0111-01-25
* **obecni:** Anastazja Sowińska, Arianna Verlen, Diana Arłacz, Eustachy Korkoran, Kamil Lyraczek, Klaudia Stryk, Martyn Hiwasser, Stefan Jamniczek

Streszczenie:

Anastazja nie została bohaterką. Wpierw Eustachy wplątał się w pojedynek o Dianę z jakimś szlachcicem - skończyło się różową mgiełką miłości i pospieszną ewakuacją Inferni z Kontrolera Pierwszego, potem epicki plan Izy z piratami rozpadł się, bo na Infernię polował jakiś statek koloidowy. Ale przetrwali - i mają zamiar to wszystko rozplątać.

Aktor w Opowieści:

* Dokonanie:
    * wpierw sponiewierał Jamniczka w walce na szpady z nieuczciwą pomocą, a potem rozkochał w sobie pół świata Paradoksem Arianny XD.
* Progresja:
    * ma stały bonus +3 w kontaktach do wszystkich kobiet.
    * zakochuje się w nim Julia, ukochana Stefana Jamniczka. Ma też przechlapane u stronników Jamniczka.


### Sabotaż świni

* **uid:** 201104-sabotaz-swini, _numer względny_: 29
* **daty:** 0111-01-13 - 0111-01-16
* **obecni:** Anastazja Sowińska, Arianna Verlen, Ataienne, Diana Arłacz, Eustachy Korkoran, Klaudia Stryk, Martyn Hiwasser, Rafał Armadion, Robert Garwen

Streszczenie:

Sabotażysta - miragent - podjął próbę zniszczenia Arianny gdy ta ładowała świnie na Wesołego Wieprzka używając wybuchowej, anomalnej świni. Zespół przetrwał, choć "Wieprzek" został zniszczony. Arianna pojmała miragenta i przekazała go do Kontrolera Pierwszego, acz kosztem ofiar w ludziach. No i nie było dość świń by je wziąć na orbitę XD.

Aktor w Opowieści:

* Dokonanie:
    * opanowany żądzą bycia paladynem przypadkowo uratował załogę Wieprza przed sabotażem miragenta; potem używając Diany jako baterii zdekombinował anomalię i zmusił ją do odlotu.


### Noktianie rodu Arłacz

* **uid:** 201021-noktianie-rodu-arlacz, _numer względny_: 28
* **daty:** 0111-01-07 - 0111-01-10
* **obecni:** Anastazja Sowińska, Arianna Verlen, Ataienne, Diana Arłacz, Eliza Ira, Eustachy Korkoran, Izabela Zarantel, Jolanta Arłacz, Juliusz Sowiński, Klaudia Stryk, Klaus Rumak, Maria Gołąb, OO Szalony Rumak, OO Wesoły Wieprzek, Robert Arłacz, Wanessa Pyszcz

Streszczenie:

By uzyskać współpracę Elizy Iry, Arianna zdecydowała się "porwać" noktian rodu Arłacz wykorzystywanych do produkcji czystej krwi dla magów. Z uwagi na działania Marii Gołąb plan został odrzucony i wykryty, ale znalezienie haków na Arłaczy rozwiązało ten problem - Arianna i Arłacze współpracują. Nie doszło do porozumienia z Elizą Irą, ale Eliza się wycofała z Trzeciego Raju, ogłaszając się protektorką tego terenu. Sytuacja w miarę ustabilizowana.

Aktor w Opowieści:

* Dokonanie:
    * poderwał Dianę Arłacz, która wygrała konkurs; zbałamucił ją i uciekła z nim na Infernię. Wysadził altankę Roberta Arłacza by pokazać Dianie co umie.
* Progresja:
    * potężne uczucia opiekuńcze wobec Diany Arłacz. ONA MUSI MIEĆ DOBRZE.


### Krystaliczny gniew Elizy

* **uid:** 201014-krystaliczny-gniew-elizy, _numer względny_: 27
* **daty:** 0111-01-02 - 0111-01-05
* **obecni:** Aleksandra Termia, Anastazja Sowińska, Arianna Verlen, Ataienne, Dariusz Krantak, Elena Verlen, Eliza Ira, Eustachy Korkoran, Klaudia Stryk, Marian Fartel, OO Wesoły Wieprzek

Streszczenie:

Pomoc humanitarna przybyła do Trzeciego Raju wraz z zabójcami Anastazji. Mindwarpowani komandosi noktiańscy prawie zabili Elenę, ale Anastazja została ochroniona. Do akcji weszła Eliza Ira - zażądała oddania jej Trzeciego Raju i noktian. Zespół przygotowuje się do nie-oddania Raju i uratowania jak najwięcej noktian z Aurum (jakkolwiek to nie brzmi).

Aktor w Opowieści:

* Dokonanie:
    * rozbroił kaskadowo bomby, po czym gdy Elena wysłała SOS kupił czas Ariannie - robiąc carpet bombing terenu z Eleną. Poranił ją, ale uratował przed śmiercią. Też strzelił Dariuszowi w nogę (odstrzeliwując ją) za cierpienie Eleny.


### Magiczna burza w Raju

* **uid:** 200923-magiczna-burza-w-raju, _numer względny_: 26
* **daty:** 0110-12-24 - 0110-12-28
* **obecni:** Anastazja Sowińska, Arianna Verlen, Dariusz Krantak, Elena Verlen, Eliza Ira, Eustachy Korkoran, Izabela Zarantel, Klaudia Stryk, Marian Fartel, Marianna Lemurczak, Nikodem Sowiński

Streszczenie:

Zbliża się burza magiczna. Nie ma schronienia ani zapasów. Zespół wysłał SOS na Kontroler i do Nikodema Sowińskiego (Anastazja). Zbudowali co się da by odeprzeć burzę magiczną i im się to udało bez strat w ludziach. O dziwo, siła która im pomogła to Eliza Ira (acz dyskretnie). Anastazja wierzy że Nikodem jest jej najlepszym przyjacielem, ale on pragnie jej zguby...

Aktor w Opowieści:

* Dokonanie:
    * robi pranka Elenie by ubrudzić Anastazję (ubrudził Elenę). Odpowiednio niszczy i fortyfikuje fragmenty Raju, by przetrwać burzę. Znalazł dowód sabotażu.


### Śmierć Raju

* **uid:** 200916-smierc-raju, _numer względny_: 25
* **daty:** 0110-12-21 - 0110-12-23
* **obecni:** Anastazja Sowińska, Arianna Verlen, Ataienne, Celina Szilat, Elena Verlen, Eustachy Korkoran, Izabela Zarantel, Klaudia Stryk, Marian Fartel, Rafał Armadion, Robert Garwen, Wanessa Pyszcz

Streszczenie:

Najważniejszym problemem Raju jest pozbycie się bombardowania nojrepów. Arianna zaproponowała zuchwały pomysł - w kontrolowany sposób niech nojrepy wejdą do miasteczka i wezmą czego szukają. Udało jej się wszystkich przekonać. Sukces - nojrepy się wycofały, ale Tucznik nie jest zdolny do latania a Trzeci Raj jest zniszczony. Została tylko Ataienne i ruina. Ataienne rozpaliła hipnotycznie morale trzeciorajowców - przetrwają to wszystko.

Aktor w Opowieści:

* Dokonanie:
    * wysadził silniki Tucznika by odwrócić uwagę Izy od Anastazji; potem zdekombinował Tucznikowi osłony anty-glukszwajnowe do kierowania nojrepami.


### Arystokratka w ładowni na świnie

* **uid:** 200909-arystokratka-w-ladowni-na-swinie, _numer względny_: 24
* **daty:** 0110-12-15 - 0110-12-20
* **obecni:** Anastazja Sowińska, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Izabela Zarantel, Kamil Lyraczek, Klaudia Stryk, Martyn Hiwasser, OO Galaktyczny Tucznik

Streszczenie:

Zespół wyciągnął Martyna z więzienia (legalnie). Wpakowali się na Galaktycznego Tucznika by przenieść świnie na orbitę... ale poleciała z nimi dziennikarka Izabela (o czym wiedzą) i 15letnia Anastazja Sowińska (o czym nie wiedzą). Przy lądowaniu, atak nojrepów na Tucznika spowodował katastrofę - seria Paradoksów Anastazji, uszkodzenia Tucznika itp. Zespół uratował Anastazję (choć kosztem ran Martyna) i wylądował Tucznikiem na Astorii. To jest sukces.

Aktor w Opowieści:

* Dokonanie:
    * zrobił epicki konkurs która arystokratka może z nim iść na kawę by wyciągnąć Martyna, po czym... zwiał na Tucznika. Zestrzelił większość rakiet nojrepów używając flaka z działek Tucznika (które normalnie nie miały flaka, ale od czego jest magiem?). Paradoksem uzbroił anomalię terroru.
* Progresja:
    * jego renoma kobieciarza i miłośnika arystokratek wymaga sprawdzenia ;-).
    * rywale - nie jest taki dobry. Co więcej, podrywa dziewczyny i jest wobec nich bucem. Negatywne konsekwencje sławy reality show ;-).
    * niechętnie widziany na Orbiterze chwilowo, acz pożądany przez fanki.


### Nienawiść do świń

* **uid:** 200826-nienawisc-do-swin, _numer względny_: 23
* **daty:** 0110-12-08 - 0110-12-14
* **obecni:** Aleksandra Termia, Antoni Kramer, Arianna Verlen, Eustachy Korkoran, Kamil Lyraczek, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Sebastian Alarius, Tadeusz Ursus

Streszczenie:

Po pojedynku Kramer opieprzył Ariannę i dał jej dyskretną tajną misję - ma niby w niesławie wozić świnie, naprawdę ma pomóc Orbiterowi rozwiązać spisek w Trzecim Raju. Ale wysłanie Arianny na świnie wywołało bunt na Kontrolerze Pierwszym; Arianna musiała rozpaczliwie deeskalować, nawet budząc Leonę. Udało się bunt opanować, acz wysokim kosztem - pierwszy post-noktiański kapitan nie żyje, załoga Inferni częściowo zesłana będzie do Trzeciego Raju a Arianna miała bardzo żenujące wejście.

Aktor w Opowieści:

* Dokonanie:
    * udowodnił, że nie umie zmusić Martyna i Klaudii do gadania jak nie chcą - ale robi świetną pirotechnikę Ariannie, by ta miała dobre wejście.


### Sekrety Orbitera - historia prawdziwa

* **uid:** 200819-sekrety-orbitera-historia-prawdziwa, _numer względny_: 22
* **daty:** 0110-11-26 - 0110-12-04
* **obecni:** Arianna Verlen, Damian Orion, Elena Verlen, Eustachy Korkoran, Izabela Zarantel, Klaudia Stryk, Leona Astrienko, Leszek Kurzmin, Martyn Hiwasser, Sabina Servatel, Tadeusz Ursus

Streszczenie:

Elena wyzwała Leonę na pojedynek. Klaudia i Arianna wkręciły Tadeusza. Tadeusz - arcymag Eterni - używa Simulacrum w walce, więc Zespół zaczął montować plan jak Leona może jednak wygrać z Simulacrum. I wszystko skończyło się tym, że Leona wygrała, Elena jest wolna od Tadeusza i powstała holodrama "Sekrety Orbitera"...

Aktor w Opowieści:

* Dokonanie:
    * robił żarliwe przemowy przekonujące Leonę oraz Elenę - oraz wraz z Martynem, przebudował Leonę by mogła pokonać Tadeusza. Plus, załatwił wzmocnienie Inferni w lepsze czujniki dzięki hazardowi.


### Nocna Krypta i Emulatorka

* **uid:** 200729-nocna-krypta-i-emulatorka, _numer względny_: 21
* **daty:** 0110-11-16 - 0110-11-22
* **obecni:** AK Nocna Krypta, Antoni Kramer, Arianna Verlen, Damian Orion, Eustachy Korkoran, Kijara d'Esuriit, Klaudia Stryk, Laura Orion, Leona Astrienko, Mirela Orion, OO Castigator, OO Minerwa

Streszczenie:

Laura, Emulatorka na Nocnej Krypcie została Skażona przez Kijarę. Laura opanowana wizjami Esuriit chciała zniszczyć Eternię. Nie dało się jej powstrzymać - Mirela, Zespół - byli za słabi. Arianna wezwała Kryptę pod ogniem Castigatora i wypaliła Kryptę i Kijarę do zera, uszkadzając "zdrowie psychiczne Krypty". Zginęło kilkudziesięciu ludzi, ale Arianna dostała podziękowanie od Admiralicji. Damian Orion jest rozczarowany.

Aktor w Opowieści:

* Dokonanie:
    * dużo wysadzał - kierunkowe miny by dostać się na Kryptę, fortyfikacja korytarza przed ghulami Esuriit, a potem lepiszcze Ariannie i Klaudii by wyszły ze stupora ;-).


### Wielki Kosmiczny Romans

* **uid:** 200722-wielki-kosmiczny-romans, _numer względny_: 20
* **daty:** 0110-11-12 - 0110-11-15
* **obecni:** Antoni Kramer, Arianna Verlen, Damian Orion, Elena Verlen, Eustachy Korkoran, Julian Muszel, Konrad Wolczątek, Leona Astrienko, Olgierd Drongon, OO Welgat, OO Żelazko, Tadeusz Ursus

Streszczenie:

Pojawiła się plotka, że Arianna, Elena i Eustachy mają romans. Plotka pochodzi od Leony, ale ktoś ją wykorzystał. Rozwiązując plotkę Zespół doszedł do tego, że Elena ma niestabilną energię magiczną i na niektórych upiornie mocno działa, poznali też elementy przeszłości Eleny. Po wyplątaniu Eleny z jednego obsesyjnego adoratora (i wpakowanie plotki że Olgierd z Żelazka aspiruje do jej ręki), Arianna zdecydowała się z Eustachym, Klaudią i Martynem pomóc Kirasjerom w uratowaniu zaginionej Emulatorki z Nocnej Krypty...

Aktor w Opowieści:

* Dokonanie:
    * wplątany w romans którego nie miał, groziło mu nawet że ma znaleźć dziewczynę na szybko. Wyplątał Elenę ze strasznego pojedynku używając Leony jako straszaka.
* Progresja:
    * w oczach Olgierda Drongona jest platonicznie zakochany w Ariannie typu 'senpai notice me'. Olgierd patrzy na to z litościwym pobłażaniem i więcej mu wybacza.
    * dostał opinię "fetyszysty z gminu" wśród arystokracji Aurum i Eterni - podrywa tylko szlachcianki, nie interesują go inne kobiety. Co gorsza, jest bardzo skuteczny w podrywaniu.


### Sabotaż Netrahiny

* **uid:** 200715-sabotaz-netrahiny, _numer względny_: 19
* **daty:** 0110-11-06 - 0110-11-09
* **obecni:** Arianna Verlen, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Lothar Diakon, OO Netrahina, OO Tvarana, Percival Diakon, Rufus Komczirp, Szczepan Myksza

Streszczenie:

Arianna dostała prośbę o pojawienie się na Netrahinie, dalekosiężnym krążowniku Orbitera jako mediator. Na miejscu okazało się, że to TAI Persefona jest sabotażystką Netrahiny - sygnały z Anomalii Kolapsu ją "uwolniły". Arianna i Klaudia dostały od Persefony koordynaty i kod uwalniający, po czym zamaskowały to co się stało - zniszczyły Persefonę i uszkodziły Netrahinę.

Aktor w Opowieści:

* Dokonanie:
    * sabotował Netrahinę, by odwrócić uwagę Persefony; musiał też wycofać się z pojedynku z Eleną i zdjął gniew Eleny z siebie przekierowując go na Ariannę.
* Progresja:
    * hańba; musiał z podkulonym ogonem wycofać się z pojedynku z Eleną i ją przeprosić. Elena wygrała moralnie.


### Problematyczna Elena

* **uid:** 200708-problematyczna-elena, _numer względny_: 18
* **daty:** 0110-10-29 - 0110-11-03
* **obecni:** AK Salamin, Antoni Kramer, Arianna Verlen, Elena Verlen, Eustachy Korkoran, Klaudia Stryk, Leszek Kurzmin, Olgierd Drongon, OO Aurelion, OO Żelazko, Persefona d'Infernia

Streszczenie:

Na Infernię trafiła nowa podoficer - podporucznik Elena Verlen, z Castigatora. W konkursie z Eustachym prawie zniszczyli Infernię. Niedługo potem Infernia (podłatana) poleciała do Anomalii Kolapsu uratować Aurelion, który natknął się na anomaliczny kiedyś-krążownik Orbitera Salamin. Arianna uratowała kogo się da i skłoniła Salamin do samozniszczenia; Persefona d'Salamin miała jeszcze dość psychotronicznej świadomości i miłości (?) do swojej martwej kapitan.

Aktor w Opowieści:

* Dokonanie:
    * wszedł z Eleną w pętlę kłótni o kompetencje i prawie wysadził Infernię by jej pokazać, że jest lepszy. Potem wykazał się rozpaczliwymi umiejętnościami naprawy statku.


### Ratujmy Castigator

* **uid:** 200624-ratujmy-castigator, _numer względny_: 17
* **daty:** 0110-10-15 - 0110-10-19
* **obecni:** Arianna Verlen, Eustachy Korkoran, Klaudia Stryk, Leona Astrienko, Leszek Kurzmin, OO Alkaris, OO Castigator, Rozalia Wączak

Streszczenie:

Skażona przez Wiktora Sataraila Rozalia ruszyła zniszczyć Castigator. Tymczasem Arianna poproszona przez przyjaciela (kapitana Castigatora) doprowadziła terrorem Castigator do prawidłowego działania i strasznie zwiększyła napięcia między Aurum i Orbiterem. Gdy Rozalia i Alkaris uderzyli, Castigator był jakoś gotowy. Castigator odparł Alkaris i wszystkich uratowano przez stopniowe wysadzanie fragmentów Castigatora i kupowanie czasu.

Aktor w Opowieści:

* Dokonanie:
    * miał sabotować Castigator przed arystokratami, a faktycznie niszczył go stopniowo by powstrzymać anomaliczną Rozalię z Alkarisa. Wielki sukces - uratował Castigator wybuchami.
* Progresja:
    * zaimponował Leszkowi Kurzminowi swoim wyczuciem sytuacji i tym, że kapitan im pomoże. A potem - pokonując Rozalię / Azalię d'Alkaris skażoną ixionem.


### Sabotaż Miecza Światła

* **uid:** 200415-sabotaz-miecza-swiatla, _numer względny_: 16
* **daty:** 0110-10-08 - 0110-10-10
* **obecni:** Arianna Verlen, Eustachy Korkoran, Klaudia Stryk, Martyn Hiwasser, Mateusz Sowiński

Streszczenie:

Zniszczona Infernia została uratowana przez Miecz Światła, który zaraz potem leciał ratować inny statek przed AK Serenit. W odpowiedzi Arianna doprowadziła do sabotażu Miecza Światła i zniszczenia reputacji komodora Sowińskiego by ratować Miecz Światła przed Serenitem. Jej się upiekło, acz pojawiło się więcej ofiar śmiertelnych...

Aktor w Opowieści:

* Dokonanie:
    * sabotował skutecznie silniki Miecza Światła. Zbyt skutecznie - doprowadził do kaskadowych zniszczeń i sabotażu poczciwego statku ratunkowego.


### O psach i kryształach

* **uid:** 200408-o-psach-i-krysztalach, _numer względny_: 15
* **daty:** 0110-09-29 - 0110-10-04
* **obecni:** Arianna Verlen, Eustachy Korkoran, Kamil Lyraczek, Klaudia Stryk, Leona Astrienko, Martyn Hiwasser, Sebastian Namczek

Streszczenie:

Infernia - QShip - została wysłana na rutynową misję zapobieżenia zamieszkom na stacji Samojed. Na miejscu okazało się, że jest zainfekowany anomalnymi kryształami krążownik Leotis. Arianna Verlen ciężko uszkodziła stację Samojed i prawie zniszczyła Infernię, ale dała radę zniszczyć Leotis poświęcając więcej niż ktokolwiek mógł chcieć.

Aktor w Opowieści:

* Dokonanie:
    * odkrystalił Sebastiana i innych przechwyconych członków Leotis; potem sprawił że Infernia - QShip - była w stanie pokonać ciężki statek jakim był Leotis.


### Kiepski altruistyczny terroryzm

* **uid:** 200122-kiepski-altruistyczny-terroryzm, _numer względny_: 14
* **daty:** 0110-09-15 - 0110-09-17
* **obecni:** Arianna Verlen, Emilia Kariamon, Eustachy Korkoran, Klaudia Stryk, Martauron Attylla, Natalia Miszryk

Streszczenie:

Emilia z Martauronem zdobyli statek kosmiczny Luxuritiasu, Królową Chmur. Infernia - zamaskowany statek - pomogła go odzyskać Luxuritiasowi. Twist polega na tym, że załoga Inferni POMOGŁA Emilii ukryć wszystkich ludzi i ich ewakuować - by Luxuritias niczego nie wiedział. Sercem Infernia jest za Emilią, profesjonalnie pomaga Luxuritiasowi.

Aktor w Opowieści:

* Dokonanie:
    * inżynier zniszczenia; wpierw uszkodził Persefonę a potem doprowadził do tego, że Persefona 2 myślała, że zniszczyła napastników.


### Atak na Kidirona

* **uid:** 230614-atak-na-kidirona, _numer względny_: 13
* **daty:** 0093-03-22 - 0093-03-24
* **obecni:** Ardilla Korkoran, Bartłomiej Korkoran, Eustachy Korkoran, Kalia Awiter, Lycoris Kidiron, OO Infernia, Rafał Kidiron, Ralf Tapszecz, SAN Szare Ostrze, Stanisław Uczantor

Streszczenie:

Gdy Wujek się obudził, powiedział że z Infernią mogą być ostre problemy jeśli spada poniżej poziomu Memoriam. Zmartwił się hipotezą Ardilli że Ralf może być magiem Nihilusa, ale Ardilla walczyła o dalsze spotykanie się z Ralfem. Zamach na Kidirona. Większość jego sztabu umiera, ale sam Kidiron przeżywa, acz ciężko ranny; ratuje go Ardilla i pozyskuje info o jego mrocznych planach. Kalia bierze na siebie morale i propagandę, ale przez to wpada na celownik Infiltratora który ją porywa. Eustachy próbuje odbić Kalię z rąk Infiltratora w koloidzie...

Aktor w Opowieści:

* Dokonanie:
    * bukiet czerwonych róż od Kalii okazał się być dyskretną wiadomością, którą przeoczył. Z wujkiem przegadał stan Inferni. Gdy był zamach na Kidirona, połączył się z Kalią i ją uspokoił; Kalia prawidłowo podnosi morale i zostaje porwana przez Infiltratora. Jego magia dała skrzydło Lancerów z Inferni; tak uzbrojony, leci za Infiltratorem.


### Zdrada rozrywająca arkologię

* **uid:** 230329-zdrada-rozrywajaca-arkologie, _numer względny_: 12
* **daty:** 0093-03-14 - 0093-03-16
* **obecni:** Amelia Sarkaldir, Ardilla Korkoran, BIA Prometeus, Celina Lertys, Eustachy Korkoran, Ewelina Paroknis, Feliks Kidiron, Franciszek Pietraszczyk, Iwona Paroknis, Jonasz Paroknis, Kalia Awiter, Katarzyna Falernik, Małgorzata Maratelus, OO Infernia, Rafał Kidiron, Ralf Tapszecz, Szczepan Falernik, Wojciech Grzebawron

Streszczenie:

Nastolatka miała pamiętnik czytany przez macochę. Ojciec to ignorował. Nastolatka napisała w pamiętniku, że ojciec zdradza macochę z ekspertką hydrologii. Zrobił się shitstorm który doprowadził do poważnych problemów - m.in. macocha (dyrektor skrzydła medycznego) zaczęła robić błędy co wykryła Celina. Kidiron podejrzewa spisek - ktoś próbuje skłócić ważne rodziny, a także shackować Prometeusa (BIA arkologii). Rozmowa z ich eks-piratami kończy się tym, że to nie oni - za tym stoi wyspecjalizowana siła z innej arkologii lub noktianie. 

Ślady prowadzą do pamiętnika nastolatki, który shackowany przez ekspertkę Inferni (i upubliczniony przez problemy na linii Infernia - Prometeus) pokazują, że najpewniej nastolatka za wszystkim stoi sama. Uratowana przez tajemniczego maga Interis przed samobójstwem nastolatka przyznała na czym polegał problem i ujawnia, że pomaga jej Feliks Kidiron, najmłodszy z rodu i jej jedyny przyjaciel. Czyli to nie spisek. Ale jak to teraz naprawić?

Aktor w Opowieści:

* Dokonanie:
    * użył Inferni by przechwycić Skorpiona Szczepana (rakietą go wpakował w dół); potem załatwił override danych medycznych od Kidirona. Wypytał Wojtka o spiski piratów i ogólnie 100% łyknął, że sprawa z pamiętnikiem Eweliny to poważny spisek przeciwko arkologii.


### Bardzo nieudane porwanie Inferni

* **uid:** 230315-bardzo-nieudane-porwanie-inferni, _numer względny_: 11
* **daty:** 0093-03-06 - 0093-03-09
* **obecni:** Ardilla Korkoran, Eustachy Korkoran, Hubert Grzebawron, Mariusz Dobrowąs, Nadia Sekernik, OO Infernia, Rafał Kidiron, Ralf Tapszecz, SAN Szare Ostrze, Wojciech Grzebawron

Streszczenie:

Mimo braku wujka, Infernia działa sprawnie. Wujek jest pod opieką medyczną, ale Celina nie ufając Kidironowi, chce być z nim cały czas. Infernia otrzymuje wezwanie od CES Mineralis, gdyż stacja została zaatakowana przez Trianai. Eustachy wysyła Czarne Hełmy i komandosów z Inferni, którzy wspólnie działają, ale Trianai jest za dużo. Eustachy wabi więc Trianai za pomocą energii magicznej, tracąc przytomność po połączeniu z koordynatorem Trianai (magiem), zabijając go. Stacja CES ucierpiała w Paradoksie Eustachego, powodując wiele ofiar. Ardilla koordynuje operację ratunkową, ale gdy okazuje się, że część załogi z CES atakuje mostek próbując ukraść statek, Ardilla wyłącza Infernię i z Ralfem wieją do kanałów Inferni.

Eustachy budzi się w niewoli, przekonuje oprychów, że jedynie on potrafi sterować Infernią (ucierpiał przy tym nieco). Ardilla i Ralf - na prośbę Eustachego - sabotują generatory Memoriam, pozwalając Inferni uwolnić się spod kontroli wrogów. Wywiązuje się strzelanina, w wyniku której napastnicy zostają pokonani - Eustachy kontroluje sytuację. Eustachy decyduje, że ich karą będzie służba na Inferni, ale jeden z nich odchodzi ostrzegać przed atakiem na Infernię. To wszystkim pasuje.

Ekipa wraca na statek, gdzie Kidiron proponuje przesłuchanie pojmanych piratów, obiecując immunitet w zamian za współpracę. Eustachy zgadza się, ale tylko z ochotnikami i Ardillą jako gwarantem bezpieczeństwa. Piraci zgadzają się współpracować mimo użycia neuroobroży przy przesłuchaniu przez Kidirona. Gdy Zespół próbuje montować nowe generatory Memoriam - nie da się. Nie mogą być zamontowane, gdyż się palą. Infernia wykrywa próbę montażu nowych, ale nie zauważa starych.

Aktor w Opowieści:

* Dokonanie:
    * dowodził Infernią by ratować CES Mineralis; próbował się połączyć z koordynatorem Trianai by zwabić wszystkie w pułapkę, ale zarezonował z wrogim magiem i eksplodowało. Potem był przesłuchiwany, wypalili mu świnki na pośladkach, ale gdy Memoriam padły to przejął kontrolę nad sytuacją i przechwycił piratów. Dołączył ich do załogi Inferni.


### Terrorystka w Ambasadorce

* **uid:** 230215-terrorystka-w-ambasadorce, _numer względny_: 10
* **daty:** 0093-02-22 - 0093-02-23
* **obecni:** Ardilla Korkoran, Bartłomiej Korkoran, Eustachy Korkoran, Kalia Awiter, Magda Misteria Sarbanik, Rafał Kidiron, Ralf Tapszecz, Tobiasz Lobrak, Tymon Korkoran

Streszczenie:

Kalia i Ardilla weszły do Ambasadorki by pomóc jeśli są w stanie, odcięły ich wiły i Ardilla ich poddaje. Misteria - napastniczka - okazuje się być czarodziejką roślin. Ona chce odzyskać wiły i zarzucić Kidironowi zniszczenie arkologii oraz porwanie wił. Jej celem jest ujawnienie prawdy o Kidironie, ukaranie Tobiasza (patrona Ambasadorki który robi złe rzeczy) i zabezpieczenie wił. Wujek chce zastąpić Ardillę, ale ona odmawia. Zespół (poza Ardillą) planuje zinfiltrować Ambasadorkę i usunąć maga (Misterię). 

Eustachy wpada na pomysł kontrolowanego obniżenia poziomu tlenu wewnątrz. Ardilla przekonuje Misterię do wymiany rannych na wujka. Ralf stanowi kanał komunikacyjny. Misteria tworzy Czarne Serce, kosztem wił i energii. Planują przekonać ludzi o winie Kidironów, wydostać się z Ambasadorki i stworzyć reportaż z Kalią. Ralf mówi o laserze na dachu, którym może zniszczyć czarodziejkę. Tobiasz wzbudza w sercu Misterii terror. Eustachy i Kidiron oczekują na rozwój sytuacji. W wyniku negocjacji, Kidiron zgadza się współpracować pod warunkiem, że Misteria nie będzie działać przeciwko niemu czy arkologii.

Kalia tworzy reportaż o Kidironie i arkologii Lirvint. Kidiron obiecuje bezpieczne wyjście dla Misterii i wił, pod warunkiem zapewnienia nieagresji. Ardilla próbuje przekonać Misterię do ewakuacji, ale ona nie chce opuścić miejsca bez rozwiązania sprawy z Tobiaszem. Czarne Serce wpływa na Misterię, więc wujek ją obezwładnia wstrzykując jej jakieś świństwo, sam zostaje poraniony przez wiły. Kidiron dotrzymał słowa odnośnie ewakuacji, acz postawił na Misterii tracker.

Aktor w Opowieści:

* Dokonanie:
    * ostrzegł Kalię o problemie, zdobył broń, aktywował alarm na Inferni i przeprowadził okrążenie budynku Ambasadorki. Przyczynił się do rozwiązania konfliktu poprzez pomysł obniżenia poziomu tlenu oraz mediację między Kidironem i Ardillą.


### Pierwsza randka Eustachego

* **uid:** 230208-pierwsza-randka-eustachego, _numer względny_: 9
* **daty:** 0093-02-14 - 0093-02-21
* **obecni:** Ardilla Korkoran, Bartłomiej Korkoran, Eustachy Korkoran, Franciszek Pietraszczyk, Kalia Awiter, Marcin Pietraszczyk, OO Infernia, Rafał Kidiron, Ralf Tapszecz

Streszczenie:

Wujek opieprza Eustachego za krzywdę niewinnych noktian, ale Kidiron się za Eustachym wstawia. Ardilla socjalizuje Ralfa i ratuje młodego Marcinka który się zaklinował w rurze Szczurowiska; okazuje się, że wszyscy kojarzą Infernię jako symbol nadziei. Potem Eustachy idzie (z przymusu) na randkę z Kalią; okazuje się, że Kalia zmanipulowała by Eustachy wygrał bo chce dać mu jeden piękny dzień. Wszystkie dowody podsłuchała i złapała Ardilla. Gdy Eustachy poczuł chore emanacje magiczne z Ambasadorki, porzucił Kalię i pobiegł do Inferni.

Aktor w Opowieści:

* Dokonanie:
    * Rafał Kidiron broni jego decyzji przed wujkiem. Poszedł na randkę z Kalią (niechętnie, bo Kidiron mu kazał). Powiedział Kalii, że lubi wybuchy i że w sumie ma coś w arkologii na czym mu zależy. Trochę z nią flirtuje, ale tak po swojemu. Zdobył od Kalii co ona wie o Inferni. Gdy poczuł emanację magiczną z Ambasadorki, zostawił Kalię samą przy Ambasadorce i pobiegł w kierunku Inferni.
* Progresja:
    * w arkologii Nativis najbardziej zależy mu w sumie na tym że jego ród i wujek   jest podziwiany i szanowany. Że jest ostoją bezpieczeństwa dla arkologii. Też sama arkologia i jej działanie.


### Wyłączone generatory Memoriam Inferni

* **uid:** 230201-wylaczone-generatory-memoriam-inferni, _numer względny_: 8
* **daty:** 0093-02-10 - 0093-02-12
* **obecni:** Ardilla Korkoran, Bartłomiej Korkoran, Celina Lertys, Ernest Puszczowiec, Eustachy Korkoran, Jan Lertys, Kalia Awiter, Kalista Luminis, Kratos Coruscatis, OO Infernia, Rafał Kidiron, Ralf Tapszecz, Tymon Korkoran

Streszczenie:

Eustachy przebudzony przez Infernię po burzy piaskowej poszedł za jej podszeptami i wyłączył generatory Memoriam. Infernia go Dotknęła - jest to anomalia Interis uwięziona w mechanicznej skorupie. Tylko Eustachy może Infernią sterować. Więc gdy pojawiła się misja pomocy bazie noktiańskiej Coruscatis, Eustachy musiał lecieć. Kidiron chciał aresztowania Kallisty Luminis a Wujek chciał pomocy noktianom. Okazało się, że na miejscu sformował się amalgamoid trianai i Infernia została ostrzelana; acz odpowiedziała ogniem. Zespół pomógł i ewakuował noktian i zdestabilizował reaktor fuzyjny by wysadzić wszystko. Pod wpływem Ardilli i Celiny Eustachy nie oddał noktian Kidironowi a wysadził ich w bezpiecznym miejscu. Kolejny dowód, że arkologia Nativis wykorzystuje żywność jako broń, co jest niezgodne z wszelkimi konwencjami.

Aktor w Opowieści:

* Dokonanie:
    * skuszony przez Infernię, wyłączył generatory Memoriam i Infernia oplotła jego duszę. Okazuje się też, że ma kumpli którzy z nim piją by "skapnęły" mu dziewczyny które chcą go podrywać. Gdy Infernia zaczęła odpalać rakiety, musiał udawać że to ON strzelał. Skupił się na ratowaniu okolicznych arkologii by zniszczyć amalgamoid; mniej ważni noktianie niż zniszczenie NAWET kosztem tego że musi oszukać Ardillę i innych używając Inferni. Potem z noktianami przesterował (sabotował) reaktor fuzyjny i zniszczył Coruscatis. Ale jednak wybrał Ardillę; nie oddał noktian Kidironom.
* Progresja:
    * Infernia (anomalia Interis) ma macki w jego głowie. Tylko on może ją pilotować.
    * zbrodnie "wojenne"; Infernia strzeliła rakietami i zginęło kilkunastu noktian. Kidiron osłania, ale to jest czyn jakiego Wujek i wielu nie wybaczy.
    * oszukał Ardillę i resztę Zespołu Infernią i udawał, że Amalgamoid nie uczestniczył w operacji. To kiedyś wyjdzie; będzie rift z jego rodem, gdy opuści Nativis.


### Osy w CES Purdont

* **uid:** 220817-osy-w-ces-purdont, _numer względny_: 7
* **daty:** 0093-01-23 - 0093-01-24
* **obecni:** Ardilla Korkoran, Celina Lertys, Eustachy Korkoran, Jan Lertys, Joachim Puriur, Kamil Wraczok, Kordian Olgator, VN Karglondel

Streszczenie:

Eustachy przejął dowodzenie nad obroną CES i sealował część przejść. Postawili serię pułapek i defensyw - i wtedy poza Trianai pojawili się komandosi z zewnątrz, z łazika (crawlera) Karglondel. Czegoś szukają pod bazą. Magia Eustachego naprowadziła ich prosto na rdzeń Trianai i Ardilla wykorzystała komandosów jako dywersję dla Trianai gdy ratowała Darię przed ostateczną transformacją w istotę Trianai (antidotum złożyła Celina). Udało im się przetrwać do przybycia Inferni i uratowali wszystkich kto był ważny. Komandosi nie przeżyli ;-).

Aktor w Opowieści:

* Dokonanie:
    * przejął kontrolę nad obroną CES Purdont, magią zakłócił komunikację napastników-komandosów, dowiedział się czemu atakują i kto. Utrzymał CES Purdont aż Infernia przybyła na ratunek.


### Infernia taksówką dla Lycoris

* **uid:** 220720-infernia-taksowka-dla-lycoris, _numer względny_: 6
* **daty:** 0093-01-20 - 0093-01-22
* **obecni:** Ardilla Korkoran, Bartłomiej Korkoran, Celina Lertys, Czesław Żuczek, Daria Raizis, Eustachy Korkoran, Jan Lertys, Kamil Wraczok, Lycoris Kidiron, OO Infernia, Rafał Kidiron, Wiktor Turkalis

Streszczenie:

Plaga Trianai uderzyła w CES Purdont; Infernia p.d. Bartłomieja Korkorana ruszyła jako taksówka dla Lycoris ale młodzi trafili do Purdont a Infernia szuka Lycoris i Wiktora. Gdy młodzi doszli do Plagi i przebili się do łączności to poinformowali Nativis i wujka o ryzyku. Dostali skonfliktowane polecenia, ale mają przetrwać. Jednak dla części z nich to nieakceptowalne, bo nie zostawią zarażonej koleżanki na pożarcie Pladze Trianai mimo znajdującego się gdzieś w cieniu Inteligentnego Koordynatora Trianai...

Aktor w Opowieści:

* Dokonanie:
    * PAST: nakablował na Ardillę za co opieprzył go wujek (never betray a FAMILY!) po czym próbował się wkraść do laboratorium i złapał go niekompetentny zwykle Kamil. ACTUAL: 20 lat; doszedł do obecności Plagi Trianai na CES Purdont i współpracując z Zespołem przebił się do centrum komunikacyjnego usuwając osy a nawet ainshkera. Przełamywał komputery używając też do pomocy magii - dzięki temu sabotaż Plagi Trianai nie dała rady odciąć bazy. Dowodzi, jako następny po Bartłomieju w rodzie Korkoran.


### Whispraith w jaskiniach Neikatis

* **uid:** 230125-whispraith-w-jaskiniach-neikatis, _numer względny_: 5
* **daty:** 0092-10-29 - 0092-11-02
* **obecni:** Aniela Myszawcowa, Anna Seiren, Antoni Grzypf, Cyprian Kugrak, Eustachy Korkoran, JAN Seiren, JAN Uśmiech Kamili, Kornelia Lichitis, Michał Uszwon, Rufus Seiren, Zofia d'Seiren

Streszczenie:

...Zespół był zmuszony się rozdzielić, by wykonać wszystkie operacje. Uratowali dowódcę "Uśmiechu" i głównego inżyniera, choć przez rozdzielenie się Upiór Piasków dopadł inżyniera Seiren. Próbując odzyskać inżyniera i znaleźć Annę, Zespół stracił dowódcę Seirena, inżyniera i jednego z salvagerów - ale udało im się zniszczyć fizyczną formę whispraitha (dzięki magii Eustachego), Annę (której szukali) i te dwie wcześniej uratowane osoby z "Uśmiechu". Potem przeczekali burzę w piaskach - nie chcieli, by whispraith zainfekował arkologię. Ogólnie - sukces? Neikatis to jednak mordercza planeta.

Aktor w Opowieści:

* Dokonanie:
    * użył zaklęcia by reanimować zniszczony Lancer Rufusa. Dzięki temu zniszczył fizyczną formę whispraitha i samego Lancera (resztką woli Rufusa). Technicznie przejął dowodzenie nad Seiren. Opracował plan - przeczekują burzę, by whispraith się rozproszył.


### To, co zostało po burzy

* **uid:** 230104-to-co-zostalo-po-burzy, _numer względny_: 4
* **daty:** 0092-10-26 - 0092-10-28
* **obecni:** Aniela Myszawcowa, Anna Seiren, Antoni Grzypf, Cyprian Kugrak, Eustachy Korkoran, JAN Seiren, JAN Uśmiech Kamili, Kalia Awiter, Michał Uszwon, Rafał Kidiron, Rufus Seiren, Zofia d'Seiren

Streszczenie:

Neikatis miewa burze piaskowe które z uwagi na strukturę piasku powodują efekty magiczne i po których pojawiają się krótkotrwałe anomalie. Salvagerzy zbierają te anomalie i drenują je, by energię dostarczyć arkologii. Eustachy został przydzielony do salvagera "Seiren", by uczyć się jak wygląda życie w arkologii. Niestety, współwłaścicielka Seiren została porwana przez dowódcę innego salvagera i odjechali w burzę piaskową by ratować JEGO żonę. Seiren jedzie w kierunku na sygnał drugiego salvagera przez burzę, w której odbijają się dziwne głosy i sygnały komunikacyjne. Napotykają ów "Uśmiech Kamili", ale część osób jest martwa i nie ma śladów życia...

Aktor w Opowieści:

* Dokonanie:
    * gatlingiem uratował Rufusa przed dużym krystalnikiem; potem wzmocnił Skorpiona częściami Inferni magią i dogadał się z Kidironem by mogli jechać w burzę.


### Ona chce dziecko Eustachego

* **uid:** 221006-ona-chce-dziecko-eustachego, _numer względny_: 3
* **daty:** 0092-09-20 - 0092-09-24
* **obecni:** Ardilla Korkoran, Ava Kieras, Emban Dolamor, Eustachy Korkoran, Lerten Kieras, Maks Selert, Michał Kervendal, OO Infernia, Staszek Zakraton, VN Exerinn

Streszczenie:

Rift - wujek vs Kidironowie w sprawie Robaków i Dziewczynki. Eustachy rozdarty, acz staje za wujkiem. Z woli Kidironów Eustachy wziął Infernię i poleciał do crawlera o nazwie Exerinn by odzyskać Robaki które uciekły. Tam Eustachy poznał jak żyją Sarderyci z Exerinna i jak radzą sobie z trudnościami (m.in. Farighanami - nekroborgami Neikatis). Po tym jak Infernia pomogła Exerinnowi i Eustachy oddał geny Avie, Robaki wróciły z Exerinna na Infernię i Eustachy ukrył ich życie przed Kidironami - fałszywka, że stali się Farighanami.

Aktor w Opowieści:

* Dokonanie:
    * zarówno wujek jak i Rafał Kidiron próbują wygrać jego duszę. Dowodził Infernią i się z nią zintegrował; pomógł crawlerowi Exerinn, oddał geny Avie i uratował kilka osób używając bunker-bustera z EMP i wbił się do miejsca gdzie Farighanowie ufortyfikowali się przed atakiem agentów Exerinn.
* Progresja:
    * oddał swoje geny Avie z Exerinna. Czyli najpewniej będzie miał dziecko :D.


### Dziewczynka Trianai

* **uid:** 220914-dziewczynka-trianai, _numer względny_: 2
* **daty:** 0092-09-10 - 0092-09-11
* **obecni:** Ardilla Korkoran, Bartłomiej Korkoran, Eustachy Korkoran, Karina Nezerin, Stanisław Uczantor

Streszczenie:

Po zniszczeniu Robaków okazało się, że w tunelach Starej Arkologii zniknęło dwóch nastolatków. Ardilla i Eustachy poszli znaleźć owych nastolatków - faktycznie, coś tam jest. Eustachy zastawił pułapkę i prawie zabił pietnastolatkę zmienioną w Trianai. Udało im się wydobyć ją i dostarczyć Kidironom, choć wykazali się dużą bezwzględnością. Większość zdominowanych przez dziewczynkę ludzi udało się uratować. Ale kto jej to zrobił i czemu?

Aktor w Opowieści:

* Dokonanie:
    * po przyjęciu zadania odnalezienia dwójki zaginionych nastolatków przejął dowodzenie, rozstrzelał piętnastolatkę Trianai (z trudem się powstrzymał by jej nie zabić) i odniósł do Kidironów z Ardillą. Wykazał się absolutną skutecznością i zimną bezwzględnością.
* Progresja:
    * opinia niesamowicie bezwzględnego. Nieważne co - jest zadanie to wykona. Wzbudza strach oraz szacunek w Arkologii Nativis.


### Czarne Hełmy i Robaki

* **uid:** 220831-czarne-helmy-i-robaki, _numer względny_: 1
* **daty:** 0092-08-15 - 0092-08-27
* **obecni:** Ardilla Korkoran, Celina Lertys, Eustachy Korkoran, Jan Lertys, Stanisław Uczantor, Tymon Korkoran, Wojciech Czerpń

Streszczenie:

Tymon Korkoran chce pomóc Arkologii Nativis by zniszczyć kult śmierci Robaków. Zespół doszedł do tego że to nie kult śmierci; to grupa radykalnych ludzi uważających że coś jest nie tak z żywnością i robiących badania żywności by do tego dojść. Nevertheless, Eustachy zrobił straszny sabotaż (ranni itp) by ludzie znienawidzili Robaki, Ardilla jedną osobę wyciągnęła z Robaków by nic jej się nie stało i ogólnie Robaki zostały zmiażdżone. Tymon poszedł do góry w hierarchii Kidironów, acz Dziadek i Wujek są przeciw tak ostrym działaniom.

Aktor w Opowieści:

* Dokonanie:
    * sabotował kult Robaków bombą w analizatorze żywności i zaeskalował konflikt Kidironowie - Robaki, wszystką chwałą (i konsekwencjami) obarczył kuzyna Tymona. Zrobił ostry sabotaż szyn w wyniku którego sporo rannych i zniszczeń -> wszystko na Tymona a oficjalnie Robaki.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 77, @: 0112-07-29
    1. Nierzeczywistość    : 2, @: 0111-08-08
    1. Primus    : 77, @: 0112-07-29
        1. Sektor Astoriański    : 73, @: 0112-07-29
            1. Astoria, Kosmos    : 1, @: 0110-09-17
                1. Brama Trzypływów    : 1, @: 0110-09-17
            1. Astoria, Orbita    : 21, @: 0112-07-25
                1. Kontroler Pierwszy    : 20, @: 0112-07-25
                    1. Akademia Orbitera    : 1, @: 0110-10-19
                    1. Arena Kalaternijska    : 1, @: 0110-12-04
                    1. Hangary Alicantis    : 3, @: 0111-11-27
                    1. Sektor 22    : 1, @: 0111-05-29
                        1. Dom Uciech Wszelakich    : 1, @: 0111-05-29
                        1. Kasyno Stanisława    : 1, @: 0111-05-29
                    1. Sektor 25    : 1, @: 0112-07-25
                        1. Bar Solatium    : 1, @: 0112-07-25
                    1. Sektor 49    : 1, @: 0112-02-08
                    1. Sektor 57    : 1, @: 0111-11-19
                        1. Mordownia    : 1, @: 0111-11-19
            1. Astoria, Pierścień Zewnętrzny    : 6, @: 0112-07-13
                1. Laboratorium Kranix    : 1, @: 0112-07-13
                1. Poligon Stoczni Neotik    : 3, @: 0112-03-04
                1. Stacja Medyczna Atropos    : 1, @: 0112-07-13
                1. Stocznia Neotik    : 5, @: 0112-03-04
                    1. Zewnętrzny dok ixioński    : 1, @: 0112-02-23
            1. Astoria    : 7, @: 0111-05-29
                1. Sojusz Letejski, NW    : 6, @: 0111-01-16
                    1. Ruiniec    : 6, @: 0111-01-16
                        1. Ortus-Conticium    : 1, @: 0110-12-28
                        1. Pustynia Kryształowa    : 1, @: 0111-01-10
                        1. Trzeci Raj, okolice    : 2, @: 0110-12-23
                            1. Kopiec Nojrepów    : 2, @: 0110-12-23
                            1. Zdrowa Ziemia    : 1, @: 0110-12-23
                        1. Trzeci Raj    : 5, @: 0111-01-16
                            1. Barbakan    : 1, @: 0110-12-23
                            1. Centrala Ataienne    : 1, @: 0110-12-23
                            1. Mały Kosmoport    : 1, @: 0110-12-23
                            1. Ratusz    : 1, @: 0110-12-23
                            1. Stacja Nadawcza    : 1, @: 0110-12-23
                1. Sojusz Letejski    : 2, @: 0111-05-29
                    1. Aurum    : 2, @: 0111-05-29
                        1. Powiat Niskowzgórza    : 1, @: 0111-01-10
                            1. Domena Arłacz    : 1, @: 0111-01-10
                                1. Posiadłość Arłacz    : 1, @: 0111-01-10
                                    1. Baterie Puryfikacji Krwi    : 1, @: 0111-01-10
                                    1. Małe lądowisko    : 1, @: 0111-01-10
                        1. Wielkie Księstwo Aktenir    : 1, @: 0111-05-29
                            1. Pałac Jasnego Ognia    : 1, @: 0111-05-29
            1. Brama Kariańska    : 6, @: 0112-01-17
            1. Brama Trzypływów    : 1, @: 0111-08-08
            1. Elang, księżyc Astorii    : 2, @: 0112-04-30
                1. EtAur Zwycięska    : 2, @: 0112-04-30
                    1. Magazyny wewnętrzne    : 1, @: 0112-04-30
                    1. Sektor bioinżynierii    : 1, @: 0112-04-30
                        1. Biolab    : 1, @: 0112-04-30
                        1. Komory żywieniowe    : 1, @: 0112-04-30
                        1. Skrzydło medyczne    : 1, @: 0112-04-30
                    1. Sektor inżynierski    : 1, @: 0112-04-30
                        1. Panele słoneczne i bateriownia    : 1, @: 0112-04-30
                        1. Podtrzymywanie życia    : 1, @: 0112-04-30
                        1. Serwisownia    : 1, @: 0112-04-30
                        1. Stacja pozyskiwania wody    : 1, @: 0112-04-30
                    1. Sektor mieszkalny    : 1, @: 0112-04-30
                        1. Barbakan    : 1, @: 0112-04-30
                            1. Administracja    : 1, @: 0112-04-30
                            1. Ochrona    : 1, @: 0112-04-30
                            1. Rdzeń AI    : 1, @: 0112-04-30
                        1. Centrum Rozrywki    : 1, @: 0112-04-30
                        1. Podtrzymywanie życia    : 1, @: 0112-04-30
                        1. Pomieszczenia mieszkalne    : 1, @: 0112-04-30
                        1. Przedszkole    : 1, @: 0112-04-30
                        1. Stołówki    : 1, @: 0112-04-30
                        1. Szklarnie    : 1, @: 0112-04-30
                    1. Sektor przeładunkowy    : 1, @: 0112-04-30
                        1. Atraktory małych pakunków    : 1, @: 0112-04-30
                        1. Magazyny    : 1, @: 0112-04-30
                        1. Pieczara Gaulronów    : 1, @: 0112-04-30
                        1. Punkt celny    : 1, @: 0112-04-30
                        1. Stacja grazerów    : 1, @: 0112-04-30
                        1. Stacja przeładunkowa    : 1, @: 0112-04-30
                        1. Starport    : 1, @: 0112-04-30
                    1. Ukryte sektory    : 1, @: 0112-04-30
            1. Iorus    : 5, @: 0112-04-09
                1. Iorus, pierścień    : 5, @: 0112-04-09
                    1. Keldan Voss    : 5, @: 0112-04-09
            1. Neikatis    : 15, @: 0112-02-11
                1. Dystrykt Glairen    : 13, @: 0093-03-24
                    1. Arkologia Lirvint    : 1, @: 0093-02-23
                    1. Arkologia Nativis, okolice    : 2, @: 0092-11-02
                        1. Szepczące Wydmy    : 2, @: 0092-11-02
                    1. Arkologia Nativis    : 10, @: 0093-03-24
                        1. Bar Śrubka z Masła    : 1, @: 0092-10-28
                        1. Dzielnica Luksusu    : 4, @: 0093-03-24
                            1. Ambasadorka Ukojenia    : 2, @: 0093-02-23
                            1. Ogrody Wiecznej Zieleni    : 3, @: 0093-03-24
                            1. Stacja holosymulacji    : 1, @: 0093-02-21
                        1. Stara Arkologia Wschodnia    : 5, @: 0093-03-24
                            1. Blokhaus E    : 1, @: 0092-08-27
                            1. Blokhaus F    : 3, @: 0093-03-16
                                1. Szczurowisko    : 2, @: 0093-03-16
                            1. Stare Wejście Północne    : 1, @: 0092-09-11
                    1. Arkologia Sarviel, okolice    : 1, @: 0093-02-12
                        1. Krater Coruscatis    : 1, @: 0093-02-12
                    1. CES Mineralis    : 1, @: 0093-03-09
                    1. CES Purdont, okolice    : 1, @: 0093-01-22
                        1. Wiertło Ekopoezy Delta    : 1, @: 0093-01-22
                    1. CES Purdont    : 2, @: 0093-01-24
                        1. Kafeteria    : 1, @: 0093-01-22
                        1. Laboratorium Ekopoezy    : 1, @: 0093-01-22
                        1. Life Support    : 1, @: 0093-01-22
                        1. System łączności    : 1, @: 0093-01-22
                    1. Ogród Zwłok Exerinna    : 1, @: 0092-09-24
                1. Dystrykt Lennet    : 1, @: 0112-02-11
                1. Dystrykt Quintal    : 1, @: 0111-01-29
                    1. Arkologia Aspiria    : 1, @: 0111-01-29
            1. Obłok Lirański    : 6, @: 0112-07-29
                1. Anomalia Kolapsu, orbita    : 2, @: 0112-07-29
                    1. Sortownia Seibert    : 1, @: 0112-07-25
                1. Anomalia Kolapsu    : 2, @: 0110-11-09
            1. Pas Omszawera    : 1, @: 0110-10-04
                1. Kolonia Samojed    : 1, @: 0110-10-04
                    1. Stacja Astropociągów    : 1, @: 0110-10-04
                    1. Zona Czarna    : 1, @: 0110-10-04
                    1. Zona Mieszkalna    : 1, @: 0110-10-04
            1. Pas Teliriański    : 3, @: 0111-09-14
                1. Planetoidy Kazimierza    : 3, @: 0111-09-14
                    1. Planetoida Asimear    : 3, @: 0111-09-14
                        1. Stacja Lazarin    : 2, @: 0111-09-14
            1. Stocznia Kariańska    : 1, @: 0112-01-17
        1. Sektor Lacarin    : 1, @: 0110-11-22
        1. Sektor Mevilig    : 3, @: 0112-01-20
            1. Chmura Piranii    : 2, @: 0112-01-17
            1. Keratlia    : 1, @: 0112-01-17
            1. Planetoida Kalarfam    : 2, @: 0112-01-20
        1. Sektor Noviter    : 1, @: 0112-01-10
        1. Zagubieni w Kosmosie    : 4, @: 0111-04-08
            1. Crepuscula    : 4, @: 0111-04-08
                1. Pasmo Zmroku    : 4, @: 0111-04-08
                    1. Baza Noktiańska Zona Tres    : 4, @: 0111-04-08
                    1. Podwodna Brama Eteryczna    : 1, @: 0111-03-15

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 63 | ((200122-kiepski-altruistyczny-terroryzm; 200408-o-psach-i-krysztalach; 200415-sabotaz-miecza-swiatla; 200429-porwanie-cywila-z-kokitii; 200624-ratujmy-castigator; 200708-problematyczna-elena; 200715-sabotaz-netrahiny; 200722-wielki-kosmiczny-romans; 200729-nocna-krypta-i-emulatorka; 200819-sekrety-orbitera-historia-prawdziwa; 200826-nienawisc-do-swin; 200909-arystokratka-w-ladowni-na-swinie; 200916-smierc-raju; 200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz; 201104-sabotaz-swini; 201111-mychainee-na-netrahinie; 201118-anastazja-bohaterka; 201210-pocalunek-aspirii; 201216-krypta-i-wyjec; 201230-pulapka-z-anastazji; 210106-sos-z-haremu; 210120-sympozjum-zniszczenia; 210127-porwanie-anastazji-z-odkupienia; 210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear; 210421-znudzona-zaloga-inferni; 210428-infekcja-serenit; 210512-ewakuacja-z-serenit; 210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210609-sekrety-kariatydy; 210616-nieudana-infiltracja-inferni; 210707-po-drugiej-stronie-bramy; 210714-baza-zona-tres; 210721-pierwsza-bia-mag; 210728-w-cieniu-nocnej-krypty; 210804-infernia-jest-nasza; 210818-siostrzenica-morlana; 210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica; 211020-kurczakownia; 211110-romans-dzieki-esuriit; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide; 220126-keldan-voss-kolonia-saitaera; 220202-sekrety-keldan-voss; 220216-polityka-rujnuje-pallide-voss; 220223-stabilizacja-keldan-voss; 220309-upadek-eleny; 220316-potwor-czy-choroba-na-etaur; 220330-etaur-i-przyneta-na-krypte; 220610-ratujemy-porywaczy-eleny; 220615-lewiatan-przy-seibert; 220622-lewiatan-za-pandore)) |
| Klaudia Stryk        | 59 | ((200122-kiepski-altruistyczny-terroryzm; 200408-o-psach-i-krysztalach; 200415-sabotaz-miecza-swiatla; 200429-porwanie-cywila-z-kokitii; 200624-ratujmy-castigator; 200708-problematyczna-elena; 200715-sabotaz-netrahiny; 200729-nocna-krypta-i-emulatorka; 200819-sekrety-orbitera-historia-prawdziwa; 200826-nienawisc-do-swin; 200909-arystokratka-w-ladowni-na-swinie; 200916-smierc-raju; 200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz; 201104-sabotaz-swini; 201111-mychainee-na-netrahinie; 201118-anastazja-bohaterka; 201210-pocalunek-aspirii; 201216-krypta-i-wyjec; 201230-pulapka-z-anastazji; 210106-sos-z-haremu; 210120-sympozjum-zniszczenia; 210127-porwanie-anastazji-z-odkupienia; 210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear; 210421-znudzona-zaloga-inferni; 210428-infekcja-serenit; 210512-ewakuacja-z-serenit; 210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210616-nieudana-infiltracja-inferni; 210707-po-drugiej-stronie-bramy; 210721-pierwsza-bia-mag; 210804-infernia-jest-nasza; 210818-siostrzenica-morlana; 210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica; 211020-kurczakownia; 211110-romans-dzieki-esuriit; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide; 220126-keldan-voss-kolonia-saitaera; 220202-sekrety-keldan-voss; 220216-polityka-rujnuje-pallide-voss; 220223-stabilizacja-keldan-voss; 220309-upadek-eleny; 220316-potwor-czy-choroba-na-etaur; 220330-etaur-i-przyneta-na-krypte; 220610-ratujemy-porywaczy-eleny; 220615-lewiatan-przy-seibert; 220622-lewiatan-za-pandore; 220706-etaur-dziwnie-wazny-wezel)) |
| Elena Verlen         | 42 | ((200708-problematyczna-elena; 200715-sabotaz-netrahiny; 200722-wielki-kosmiczny-romans; 200819-sekrety-orbitera-historia-prawdziwa; 200909-arystokratka-w-ladowni-na-swinie; 200916-smierc-raju; 200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy; 201230-pulapka-z-anastazji; 210106-sos-z-haremu; 210120-sympozjum-zniszczenia; 210127-porwanie-anastazji-z-odkupienia; 210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear; 210421-znudzona-zaloga-inferni; 210428-infekcja-serenit; 210512-ewakuacja-z-serenit; 210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210616-nieudana-infiltracja-inferni; 210707-po-drugiej-stronie-bramy; 210714-baza-zona-tres; 210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica; 211020-kurczakownia; 211110-romans-dzieki-esuriit; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide; 220126-keldan-voss-kolonia-saitaera; 220202-sekrety-keldan-voss; 220216-polityka-rujnuje-pallide-voss; 220223-stabilizacja-keldan-voss; 220309-upadek-eleny; 220316-potwor-czy-choroba-na-etaur; 220610-ratujemy-porywaczy-eleny)) |
| Martyn Hiwasser      | 29 | ((200408-o-psach-i-krysztalach; 200415-sabotaz-miecza-swiatla; 200819-sekrety-orbitera-historia-prawdziwa; 200826-nienawisc-do-swin; 200909-arystokratka-w-ladowni-na-swinie; 201104-sabotaz-swini; 201111-mychainee-na-netrahinie; 201118-anastazja-bohaterka; 201216-krypta-i-wyjec; 201230-pulapka-z-anastazji; 210106-sos-z-haremu; 210120-sympozjum-zniszczenia; 210127-porwanie-anastazji-z-odkupienia; 210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear; 210421-znudzona-zaloga-inferni; 210428-infekcja-serenit; 210512-ewakuacja-z-serenit; 210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210707-po-drugiej-stronie-bramy; 210714-baza-zona-tres; 210721-pierwsza-bia-mag; 210901-stabilizacja-bramy-eterycznej; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica; 211020-kurczakownia; 220610-ratujemy-porywaczy-eleny)) |
| Leona Astrienko      | 24 | ((200408-o-psach-i-krysztalach; 200429-porwanie-cywila-z-kokitii; 200624-ratujmy-castigator; 200722-wielki-kosmiczny-romans; 200729-nocna-krypta-i-emulatorka; 200819-sekrety-orbitera-historia-prawdziwa; 200826-nienawisc-do-swin; 210106-sos-z-haremu; 210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210421-znudzona-zaloga-inferni; 210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210616-nieudana-infiltracja-inferni; 210804-infernia-jest-nasza; 210818-siostrzenica-morlana; 210901-stabilizacja-bramy-eterycznej; 210929-grupa-ekspedycyjna-kellert; 211110-romans-dzieki-esuriit; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy; 220610-ratujemy-porywaczy-eleny)) |
| OO Infernia          | 20 | ((201210-pocalunek-aspirii; 201230-pulapka-z-anastazji; 210127-porwanie-anastazji-z-odkupienia; 210218-infernia-jako-goldarion; 210428-infekcja-serenit; 210512-ewakuacja-z-serenit; 210825-uszkodzona-brama-eteryczna; 210929-grupa-ekspedycyjna-kellert; 211020-kurczakownia; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy; 220622-lewiatan-za-pandore; 220720-infernia-taksowka-dla-lycoris; 221006-ona-chce-dziecko-eustachego; 230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230315-bardzo-nieudane-porwanie-inferni; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona)) |
| Maria Naavas         | 12 | ((210120-sympozjum-zniszczenia; 210818-siostrzenica-morlana; 211110-romans-dzieki-esuriit; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide; 220316-potwor-czy-choroba-na-etaur; 220330-etaur-i-przyneta-na-krypte; 220622-lewiatan-za-pandore; 220706-etaur-dziwnie-wazny-wezel)) |
| Ardilla Korkoran     | 11 | ((220720-infernia-taksowka-dla-lycoris; 220817-osy-w-ces-purdont; 220831-czarne-helmy-i-robaki; 220914-dziewczynka-trianai; 221006-ona-chce-dziecko-eustachego; 230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230315-bardzo-nieudane-porwanie-inferni; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona)) |
| Kamil Lyraczek       | 11 | ((200408-o-psach-i-krysztalach; 200826-nienawisc-do-swin; 200909-arystokratka-w-ladowni-na-swinie; 201118-anastazja-bohaterka; 201216-krypta-i-wyjec; 210414-dekralotyzacja-asimear; 210526-morderstwo-na-inferni; 210616-nieudana-infiltracja-inferni; 211020-kurczakownia; 211215-sklejanie-inferni-do-kupy; 220105-to-nie-pulapka-na-nereide)) |
| Antoni Kramer        | 10 | ((200429-porwanie-cywila-z-kokitii; 200708-problematyczna-elena; 200722-wielki-kosmiczny-romans; 200729-nocna-krypta-i-emulatorka; 200826-nienawisc-do-swin; 210218-infernia-jako-goldarion; 210526-morderstwo-na-inferni; 210804-infernia-jest-nasza; 210818-siostrzenica-morlana; 210929-grupa-ekspedycyjna-kellert)) |
| Izabela Zarantel     | 10 | ((200819-sekrety-orbitera-historia-prawdziwa; 200909-arystokratka-w-ladowni-na-swinie; 200916-smierc-raju; 200923-magiczna-burza-w-raju; 201021-noktianie-rodu-arlacz; 210616-nieudana-infiltracja-inferni; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide)) |
| Anastazja Sowińska   | 9 | ((200909-arystokratka-w-ladowni-na-swinie; 200916-smierc-raju; 200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz; 201104-sabotaz-swini; 201118-anastazja-bohaterka; 201210-pocalunek-aspirii; 201216-krypta-i-wyjec)) |
| Rafał Kidiron        | 8 | ((220720-infernia-taksowka-dla-lycoris; 230104-to-co-zostalo-po-burzy; 230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230315-bardzo-nieudane-porwanie-inferni; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona)) |
| Diana Arłacz         | 7 | ((201021-noktianie-rodu-arlacz; 201104-sabotaz-swini; 201118-anastazja-bohaterka; 201216-krypta-i-wyjec; 210106-sos-z-haremu; 210120-sympozjum-zniszczenia; 210127-porwanie-anastazji-z-odkupienia)) |
| AK Nocna Krypta      | 6 | ((200729-nocna-krypta-i-emulatorka; 201210-pocalunek-aspirii; 201216-krypta-i-wyjec; 201230-pulapka-z-anastazji; 210728-w-cieniu-nocnej-krypty; 210825-uszkodzona-brama-eteryczna)) |
| Aleksandra Termia    | 6 | ((200826-nienawisc-do-swin; 201014-krystaliczny-gniew-elizy; 210804-infernia-jest-nasza; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica; 220706-etaur-dziwnie-wazny-wezel)) |
| Bartłomiej Korkoran  | 6 | ((220720-infernia-taksowka-dla-lycoris; 220914-dziewczynka-trianai; 230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230614-atak-na-kidirona)) |
| Kalia Awiter         | 6 | ((230104-to-co-zostalo-po-burzy; 230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona)) |
| Ralf Tapszecz        | 6 | ((230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230315-bardzo-nieudane-porwanie-inferni; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona)) |
| Roland Sowiński      | 6 | ((210512-ewakuacja-z-serenit; 210609-sekrety-kariatydy; 210616-nieudana-infiltracja-inferni; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211215-sklejanie-inferni-do-kupy)) |
| Annika Pradis        | 5 | ((220126-keldan-voss-kolonia-saitaera; 220202-sekrety-keldan-voss; 220216-polityka-rujnuje-pallide-voss; 220223-stabilizacja-keldan-voss; 220309-upadek-eleny)) |
| Celina Lertys        | 5 | ((220720-infernia-taksowka-dla-lycoris; 220817-osy-w-ces-purdont; 220831-czarne-helmy-i-robaki; 230201-wylaczone-generatory-memoriam-inferni; 230329-zdrada-rozrywajaca-arkologie)) |
| Diana d'Infernia     | 5 | ((210804-infernia-jest-nasza; 210825-uszkodzona-brama-eteryczna; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik)) |
| Janus Krzak          | 5 | ((210707-po-drugiej-stronie-bramy; 210714-baza-zona-tres; 210721-pierwsza-bia-mag; 210728-w-cieniu-nocnej-krypty; 210901-stabilizacja-bramy-eterycznej)) |
| Otto Azgorn          | 5 | ((210421-znudzona-zaloga-inferni; 210526-morderstwo-na-inferni; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica; 211020-kurczakownia)) |
| Raoul Lavanis        | 5 | ((210707-po-drugiej-stronie-bramy; 220216-polityka-rujnuje-pallide-voss; 220330-etaur-i-przyneta-na-krypte; 220610-ratujemy-porywaczy-eleny; 220622-lewiatan-za-pandore)) |
| Tomasz Sowiński      | 5 | ((210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear; 210421-znudzona-zaloga-inferni; 210818-siostrzenica-morlana)) |
| Adam Szarjan         | 4 | ((211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik)) |
| Ataienne             | 4 | ((200916-smierc-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz; 201104-sabotaz-swini)) |
| Damian Orion         | 4 | ((200722-wielki-kosmiczny-romans; 200729-nocna-krypta-i-emulatorka; 200819-sekrety-orbitera-historia-prawdziwa; 201230-pulapka-z-anastazji)) |
| Flawia Blakenbauer   | 4 | ((210616-nieudana-infiltracja-inferni; 210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej; 211020-kurczakownia)) |
| Jan Lertys           | 4 | ((220720-infernia-taksowka-dla-lycoris; 220817-osy-w-ces-purdont; 220831-czarne-helmy-i-robaki; 230201-wylaczone-generatory-memoriam-inferni)) |
| Marian Tosen         | 4 | ((210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210609-sekrety-kariatydy; 210616-nieudana-infiltracja-inferni)) |
| Olgierd Drongon      | 4 | ((200708-problematyczna-elena; 200722-wielki-kosmiczny-romans; 210120-sympozjum-zniszczenia; 210818-siostrzenica-morlana)) |
| OO Tivr              | 4 | ((210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 211117-porwany-trismegistos; 220622-lewiatan-za-pandore)) |
| Tadeusz Ursus        | 4 | ((200722-wielki-kosmiczny-romans; 200819-sekrety-orbitera-historia-prawdziwa; 200826-nienawisc-do-swin; 210428-infekcja-serenit)) |
| Dariusz Krantak      | 3 | ((200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy; 210127-porwanie-anastazji-z-odkupienia)) |
| Eliza Ira            | 3 | ((200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz)) |
| Jolanta Sowińska     | 3 | ((210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210421-znudzona-zaloga-inferni)) |
| Leszek Kurzmin       | 3 | ((200624-ratujmy-castigator; 200708-problematyczna-elena; 200819-sekrety-orbitera-historia-prawdziwa)) |
| Marian Fartel        | 3 | ((200916-smierc-raju; 200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy)) |
| Mateus Sarpon        | 3 | ((220126-keldan-voss-kolonia-saitaera; 220202-sekrety-keldan-voss; 220216-polityka-rujnuje-pallide-voss)) |
| Medea Sowińska       | 3 | ((200429-porwanie-cywila-z-kokitii; 210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej)) |
| OO Netrahina         | 3 | ((200715-sabotaz-netrahiny; 210818-siostrzenica-morlana; 210901-stabilizacja-bramy-eterycznej)) |
| OO Żelazko           | 3 | ((200708-problematyczna-elena; 200722-wielki-kosmiczny-romans; 210818-siostrzenica-morlana)) |
| Persefona d'Infernia | 3 | ((200708-problematyczna-elena; 210421-znudzona-zaloga-inferni; 210428-infekcja-serenit)) |
| Romana Arnatin       | 3 | ((210721-pierwsza-bia-mag; 210728-w-cieniu-nocnej-krypty; 210728-w-cieniu-nocnej-krypty)) |
| Stanisław Uczantor   | 3 | ((220831-czarne-helmy-i-robaki; 220914-dziewczynka-trianai; 230614-atak-na-kidirona)) |
| Szczepan Kaltaben    | 3 | ((220126-keldan-voss-kolonia-saitaera; 220216-polityka-rujnuje-pallide-voss; 220223-stabilizacja-keldan-voss)) |
| Tymon Korkoran       | 3 | ((220831-czarne-helmy-i-robaki; 230201-wylaczone-generatory-memoriam-inferni; 230215-terrorystka-w-ambasadorce)) |
| Wawrzyn Rewemis      | 3 | ((211110-romans-dzieki-esuriit; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko)) |
| AK Serenit           | 2 | ((210428-infekcja-serenit; 210512-ewakuacja-z-serenit)) |
| AK Wyjec             | 2 | ((201210-pocalunek-aspirii; 201216-krypta-i-wyjec)) |
| Anastazja Sowińska Dwa | 2 | ((201230-pulapka-z-anastazji; 210127-porwanie-anastazji-z-odkupienia)) |
| Aniela Myszawcowa    | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| Anna Seiren          | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| Antoni Grzypf        | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| Cyprian Kugrak       | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| Franciszek Pietraszczyk | 2 | ((230208-pierwsza-randka-eustachego; 230329-zdrada-rozrywajaca-arkologie)) |
| Gilbert Bloch        | 2 | ((210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej)) |
| JAN Seiren           | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| JAN Uśmiech Kamili   | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| Juliusz Sowiński     | 2 | ((201021-noktianie-rodu-arlacz; 201210-pocalunek-aspirii)) |
| Kamil Wraczok        | 2 | ((220720-infernia-taksowka-dla-lycoris; 220817-osy-w-ces-purdont)) |
| Karol Reichard       | 2 | ((210526-morderstwo-na-inferni; 211110-romans-dzieki-esuriit)) |
| Kasandra Destrukcja Diakon | 2 | ((211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik)) |
| Lutus Amerin         | 2 | ((211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide)) |
| Lycoris Kidiron      | 2 | ((220720-infernia-taksowka-dla-lycoris; 230614-atak-na-kidirona)) |
| Martyna Bianistek    | 2 | ((210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear)) |
| Michał Uszwon        | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| Natalia Aradin       | 2 | ((211124-prototypowa-nereida-natalii; 220105-to-nie-pulapka-na-nereide)) |
| OA Zguba Tytanów     | 2 | ((201210-pocalunek-aspirii; 201230-pulapka-z-anastazji)) |
| OE Falołamacz        | 2 | ((210428-infekcja-serenit; 210512-ewakuacja-z-serenit)) |
| Ola Szerszeń         | 2 | ((220615-lewiatan-przy-seibert; 220622-lewiatan-za-pandore)) |
| OO Castigator        | 2 | ((200624-ratujmy-castigator; 200729-nocna-krypta-i-emulatorka)) |
| OO Wesoły Wieprzek   | 2 | ((201014-krystaliczny-gniew-elizy; 201021-noktianie-rodu-arlacz)) |
| Rafał Armadion       | 2 | ((200916-smierc-raju; 201104-sabotaz-swini)) |
| Robert Garwen        | 2 | ((200916-smierc-raju; 201104-sabotaz-swini)) |
| Rufus Komczirp       | 2 | ((200715-sabotaz-netrahiny; 201111-mychainee-na-netrahinie)) |
| Rufus Seiren         | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| SAN Szare Ostrze     | 2 | ((230315-bardzo-nieudane-porwanie-inferni; 230614-atak-na-kidirona)) |
| SCA Płetwal Błękitny | 2 | ((210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear)) |
| SP Pallida Voss      | 2 | ((220126-keldan-voss-kolonia-saitaera; 220216-polityka-rujnuje-pallide-voss)) |
| Tal Marczak          | 2 | ((210526-morderstwo-na-inferni; 210707-po-drugiej-stronie-bramy)) |
| Tomasz Kaltaben      | 2 | ((220223-stabilizacja-keldan-voss; 220309-upadek-eleny)) |
| Ulisses Kalidon      | 2 | ((210714-baza-zona-tres; 210728-w-cieniu-nocnej-krypty)) |
| Vigilus Mevilig      | 2 | ((210929-grupa-ekspedycyjna-kellert; 211020-kurczakownia)) |
| Wanessa Pyszcz       | 2 | ((200916-smierc-raju; 201021-noktianie-rodu-arlacz)) |
| Wojciech Grzebawron  | 2 | ((230315-bardzo-nieudane-porwanie-inferni; 230329-zdrada-rozrywajaca-arkologie)) |
| Zofia d'Seiren       | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| Zygfryd Maus         | 2 | ((211117-porwany-trismegistos; 220126-keldan-voss-kolonia-saitaera)) |
| Adam Nerawol         | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| Aida Serenit         | 1 | ((210512-ewakuacja-z-serenit)) |
| AK Rodivas           | 1 | ((201230-pulapka-z-anastazji)) |
| AK Salamin           | 1 | ((200708-problematyczna-elena)) |
| Alara Ehmes          | 1 | ((200429-porwanie-cywila-z-kokitii)) |
| Amelia Sarkaldir     | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Arkadia Verlen       | 1 | ((211124-prototypowa-nereida-natalii)) |
| Artur Traffal        | 1 | ((210804-infernia-jest-nasza)) |
| Atrius Kurunen       | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| Ava Kieras           | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| BIA Prometeus        | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| BIA Solitaria d'Zona Tres | 1 | ((210721-pierwsza-bia-mag)) |
| BIA XXX d'Zona Tres  | 1 | ((210714-baza-zona-tres)) |
| Bogdan Anatael       | 1 | ((210512-ewakuacja-z-serenit)) |
| Celina Szilat        | 1 | ((200916-smierc-raju)) |
| Czesław Żuczek       | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Daria Raizis         | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Dominika Perikas     | 1 | ((220316-potwor-czy-choroba-na-etaur)) |
| Donald Parziarz      | 1 | ((201210-pocalunek-aspirii)) |
| Emban Dolamor        | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| Emilia Kariamon      | 1 | ((200122-kiepski-altruistyczny-terroryzm)) |
| Ernest Puszczowiec   | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Eszara d'Seibert     | 1 | ((220615-lewiatan-przy-seibert)) |
| Ewelina Paroknis     | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Feliks Kidiron       | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Feliks Walrond       | 1 | ((210526-morderstwo-na-inferni)) |
| Finis Vitae          | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| Franciszek Maszkiet  | 1 | ((210804-infernia-jest-nasza)) |
| Gerard Adanor        | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| Halina Szkwalnik     | 1 | ((201111-mychainee-na-netrahinie)) |
| Helena Adanor        | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| Henryk Sowiński      | 1 | ((201230-pulapka-z-anastazji)) |
| Hestia d'Atropos     | 1 | ((220610-ratujemy-porywaczy-eleny)) |
| Hestia d'Neotik      | 1 | ((211208-o-krok-za-daleko)) |
| Horacy Aktenir       | 1 | ((210106-sos-z-haremu)) |
| Hubert Grzebawron    | 1 | ((230315-bardzo-nieudane-porwanie-inferni)) |
| Iwona Paroknis       | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Jakub Bulgocz        | 1 | ((220610-ratujemy-porywaczy-eleny)) |
| Jamon Korab          | 1 | ((211117-porwany-trismegistos)) |
| Jarosław Szarjan     | 1 | ((211208-o-krok-za-daleko)) |
| Joachim Puriur       | 1 | ((220817-osy-w-ces-purdont)) |
| Jolanta Arłacz       | 1 | ((201021-noktianie-rodu-arlacz)) |
| Jonasz Paroknis      | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Jonasz Parys         | 1 | ((220615-lewiatan-przy-seibert)) |
| Julia Aktenir        | 1 | ((210106-sos-z-haremu)) |
| Julian Muszel        | 1 | ((200722-wielki-kosmiczny-romans)) |
| Kalira d'Trismegistos | 1 | ((211117-porwany-trismegistos)) |
| Kalista Luminis      | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Karina Nezerin       | 1 | ((220914-dziewczynka-trianai)) |
| Katarzyna Falernik   | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Katra Igneus         | 1 | ((201210-pocalunek-aspirii)) |
| Kijara d'Esuriit     | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Klara Gwozdnik       | 1 | ((211117-porwany-trismegistos)) |
| Klaus Rumak          | 1 | ((201021-noktianie-rodu-arlacz)) |
| Konrad Wolczątek     | 1 | ((200722-wielki-kosmiczny-romans)) |
| Kordian Olgator      | 1 | ((220817-osy-w-ces-purdont)) |
| Kormonow Voss        | 1 | ((220126-keldan-voss-kolonia-saitaera)) |
| Kornelia Lichitis    | 1 | ((230125-whispraith-w-jaskiniach-neikatis)) |
| Kratos Coruscatis    | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Lamia Akacja         | 1 | ((220330-etaur-i-przyneta-na-krypte)) |
| Laura Orion          | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Lerten Kieras        | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| Leszek Czarban       | 1 | ((220330-etaur-i-przyneta-na-krypte)) |
| Llarnagraht          | 1 | ((210414-dekralotyzacja-asimear)) |
| Lothar Diakon        | 1 | ((200715-sabotaz-netrahiny)) |
| Maciej Żarand        | 1 | ((210804-infernia-jest-nasza)) |
| Maciek Kalmarzec     | 1 | ((211222-kult-saitaera-w-neotik)) |
| Magda Misteria Sarbanik | 1 | ((230215-terrorystka-w-ambasadorce)) |
| Maks Selert          | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| Malictrix d'Pandora  | 1 | ((210414-dekralotyzacja-asimear)) |
| Małgorzata Maratelus | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Marcin Pietraszczyk  | 1 | ((230208-pierwsza-randka-eustachego)) |
| Maria Gołąb          | 1 | ((201021-noktianie-rodu-arlacz)) |
| Marianna Lemurczak   | 1 | ((200923-magiczna-burza-w-raju)) |
| Mariusz Dobrowąs     | 1 | ((230315-bardzo-nieudane-porwanie-inferni)) |
| Mariusz Tubalon      | 1 | ((210414-dekralotyzacja-asimear)) |
| Martauron Attylla    | 1 | ((200122-kiepski-altruistyczny-terroryzm)) |
| Mateusz Sowiński     | 1 | ((200415-sabotaz-miecza-swiatla)) |
| Melania Akacja       | 1 | ((220316-potwor-czy-choroba-na-etaur)) |
| Michał Kervendal     | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| Mira Anastel         | 1 | ((211117-porwany-trismegistos)) |
| Mirela Orion         | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| Morrigan d'Tirakal   | 1 | ((210421-znudzona-zaloga-inferni)) |
| Nadia Sekernik       | 1 | ((230315-bardzo-nieudane-porwanie-inferni)) |
| Natalia Miszryk      | 1 | ((200122-kiepski-altruistyczny-terroryzm)) |
| Nataniel Morlan      | 1 | ((210818-siostrzenica-morlana)) |
| Nikodem Sowiński     | 1 | ((200923-magiczna-burza-w-raju)) |
| OA Bakałarz          | 1 | ((210120-sympozjum-zniszczenia)) |
| OA Odkupienie        | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| OE Piękna Elena      | 1 | ((210428-infekcja-serenit)) |
| Ofelia Morlan        | 1 | ((210818-siostrzenica-morlana)) |
| Olena Orion          | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| Oliwia Karelan       | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| ON Spatium Gelida    | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| OO Alaya             | 1 | ((210519-osiemnascie-oczu)) |
| OO Alkaris           | 1 | ((200624-ratujmy-castigator)) |
| OO Aurelion          | 1 | ((200708-problematyczna-elena)) |
| OO Galaktyczny Tucznik | 1 | ((200909-arystokratka-w-ladowni-na-swinie)) |
| OO Kanagar           | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| OO Kariatyda         | 1 | ((210609-sekrety-kariatydy)) |
| OO Kastor            | 1 | ((220126-keldan-voss-kolonia-saitaera)) |
| OO Mfumo             | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| OO Minerwa           | 1 | ((200729-nocna-krypta-i-emulatorka)) |
| OO Omega Septius     | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| OO Opresor           | 1 | ((210616-nieudana-infiltracja-inferni)) |
| OO Pandora           | 1 | ((220622-lewiatan-za-pandore)) |
| OO Samotność Gwiazd  | 1 | ((210804-infernia-jest-nasza)) |
| OO Straszliwy Pająk  | 1 | ((220622-lewiatan-za-pandore)) |
| OO Szalony Rumak     | 1 | ((201021-noktianie-rodu-arlacz)) |
| OO Trasman           | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| OO Tvarana           | 1 | ((200715-sabotaz-netrahiny)) |
| OO Welgat            | 1 | ((200722-wielki-kosmiczny-romans)) |
| Percival Diakon      | 1 | ((200715-sabotaz-netrahiny)) |
| Rafael Galwarn       | 1 | ((210609-sekrety-kariatydy)) |
| Rafał Grambucz       | 1 | ((210428-infekcja-serenit)) |
| Remigiusz Błyszczyk  | 1 | ((211013-szara-nawalnica)) |
| Robert Arłacz        | 1 | ((201021-noktianie-rodu-arlacz)) |
| Rozalia Teirik       | 1 | ((210106-sos-z-haremu)) |
| Rozalia Wączak       | 1 | ((200624-ratujmy-castigator)) |
| Rufus Niegnat        | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| Rzeźnik Parszywiec Diakon | 1 | ((220622-lewiatan-za-pandore)) |
| Sabina Servatel      | 1 | ((200819-sekrety-orbitera-historia-prawdziwa)) |
| Saitaer              | 1 | ((220105-to-nie-pulapka-na-nereide)) |
| SC Fecundatis        | 1 | ((210818-siostrzenica-morlana)) |
| SC Światłodóbr       | 1 | ((210818-siostrzenica-morlana)) |
| SC Trismegistos      | 1 | ((211117-porwany-trismegistos)) |
| Sebastian Alarius    | 1 | ((200826-nienawisc-do-swin)) |
| Sebastian Namczek    | 1 | ((200408-o-psach-i-krysztalach)) |
| Seweryn Atanair      | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| SP Plugawy Jaszczur  | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| SP Światło Nadziei   | 1 | ((220223-stabilizacja-keldan-voss)) |
| Staszek Zakraton     | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| Stefan Jamniczek     | 1 | ((201118-anastazja-bohaterka)) |
| Suwan Chankar        | 1 | ((220316-potwor-czy-choroba-na-etaur)) |
| Szczepan Falernik    | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Szczepan Myksza      | 1 | ((200715-sabotaz-netrahiny)) |
| TAI Marszałek Grzmotoszpon Trzeci | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| TAI Rzieza d'K1      | 1 | ((211013-szara-nawalnica)) |
| Tobiasz Lobrak       | 1 | ((230215-terrorystka-w-ambasadorce)) |
| Tymoteusz Czerw      | 1 | ((220316-potwor-czy-choroba-na-etaur)) |
| VN Exerinn           | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| VN Karglondel        | 1 | ((220817-osy-w-ces-purdont)) |
| Wiktor Turkalis      | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Wojciech Czerpń      | 1 | ((220831-czarne-helmy-i-robaki)) |
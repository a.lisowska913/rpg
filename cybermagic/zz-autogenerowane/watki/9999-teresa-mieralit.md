# Teresa Mieralit
## Identyfikator

Id: 9999-teresa-mieralit

## Sekcja Opowieści

### The goose from hell

* **uid:** 230303-the-goose-from-hell, _numer względny_: 12
* **daty:** 0111-10-24 - 0111-10-26
* **obecni:** Alex Deverien, Alicja Trawlis, Carmen Deverien, Julia Kardolin, kot-pacyfikator Tobias, Paweł Szprotka, Teresa Mieralit

Streszczenie:

A cat-pacifier named Tobias belonging to Carmen and Alex got shot at by some random illegal hunters. Carmen, Alex, and Julia are tasked by their ethics teacher to deal with an anomalous goose created by Paweł that has become a menace (created to protect other animals). The group devises a plan to capture the goose using a cage and specially made food. Despite having to traverse a ruined building, they manage to trap the goose, and the story concludes with Alex investigating a mysterious girl who seemed to be somehow connected to the goose.

Aktor w Opowieści:

* Dokonanie:
    * An ethics teacher who recruited Carmen, Alex, and Julia to solve the goose problem. She was the only one who had a good night sleep while the students were sleeping on the top of a building guarding the captured goose.


### Koszt ratowania Torszeckiego

* **uid:** 211026-koszt-ratowania-torszeckiego, _numer względny_: 11
* **daty:** 0111-07-28 - 0111-08-01
* **obecni:** Ignacy Myrczek, Ksenia Kirallen, Marysia Sowińska, Olga Myszeczka, Paweł Szprotka, Rafał Torszecki, Sensacjusz Diakon, Teresa Mieralit, Wiktor Satarail

Streszczenie:

By ratować Torszeckiego, Marysia wchodzi w sojusz z Wiktorem Satarailem. On dał jej podskórnego robaka którego dostanie Torszecki, by uzasadnić dziwne zachowanie. Wiktor "dostanie swoją zapłatę" od "kogoś winnego". Marysia przekonała też Sensacjusza, że ona x Torszecki. Zaczęły się też pojawiać takie plotki...

Aktor w Opowieści:

* Dokonanie:
    * dla Pawła Szprotki jest "Damą w Błękicie". Chroni go i daje mu pracę, bo chce jego powodzenia. Tak jak kiedyś Klaudia i Ksenia chroniły ją. I ofc Arnulf.


### Pojedynek: Akademia - Rekiny

* **uid:** 201013-pojedynek-akademia-rekiny, _numer względny_: 10
* **daty:** 0110-10-10 - 0110-10-18
* **obecni:** Aleksander Bemucik, Ignacy Myrczek, Julia Kardolin, Justynian Diakon, Kacper Bankierz, Liliana Bankierz, Napoleon Bankierz, Remor 340D, Robert Pakiszon, Stella Armadion, Teresa Mieralit

Streszczenie:

Kolejna ustawka między Rekinami a uczniami AMZ mogłaby się skończyć bardzo źle, więc grupa uczniów spróbowała wprowadzić sposób rozwiązywania konfliktu przez turnieje między Rekinami i AMZ. Nie tylko im się to udało - użycie starego ścigacza wojskowego Remor 340D i echo emocji wojen noktiańskich sprawiło, że Rekiny się unormowały i zainwestowano w budowę Toru Wyścigowego Pamięci w Podwiercie.

Aktor w Opowieści:

* Dokonanie:
    * miesiąc temu katalitycznie rozproszyła efemerydę złożoną przez bitwę studentów AMZ vs Rekiny. Zgodziła się by Napoleon pożyczył ścigacz z artefaktorium.


### Tajna baza Orbitera?

* **uid:** 200510-tajna-baza-orbitera, _numer względny_: 9
* **daty:** 0110-09-03 - 0110-09-07
* **obecni:** Alina Anakonda, Artur Kołczond, Gabriel Ursus, Ignacy Myrczek, Kallista Exolon, Laura Tesinik, Mariusz Trzewń, Natalia Tessalon, Pięknotka Diakon, Sabina Kazitan, Talarand d'Irrydius, Teresa Mieralit, Tymon Grubosz

Streszczenie:

Natalia Tessalon chce przetransportować rannego brata do Aurum, ale manewr zablokowała Laura (terminuska-prawniczka) na prośbę Pięknotki. Natalia i Sabina się ścięły, przez co ucierpiał Myrczek a Sabina trafiła do aresztu. Teresa Mieralit zapewni, że Myrczek nie ma głowy do Sabiny. Pięknotka z Tymonem poszukali kto ciężko zmiażdżył Gabriela; udało się im znaleźć tajną bazę Orbitera w której jest Emulatorka, która się do tego przyznała.

Aktor w Opowieści:

* Dokonanie:
    * poproszona przez Pięknotkę, by skupić się na Myrczku. On ma trochę za dużo czasu i podkochuje się w Sabinie Kazitan, co do niczego nie prowadzi. Obiecała, że go od niej odsunie.


### Test z etyki

* **uid:** 200326-test-z-etyki, _numer względny_: 8
* **daty:** 0110-07-25 - 0110-07-27
* **obecni:** Aniela Kark, Berenika Wrążowiec, Ignacy Myrczek, Liliana Bankierz, Napoleon Bankierz, Teresa Mieralit

Streszczenie:

Liliana eskalowała swoją krucjatę przeciw "Zygmuntowi Zającowi", włączając do działania Ignacego Myrczka. Teresa Mieralit, nauczycielka m.in. etyki, zrobiła z tego przypadku egzamin dla dwóch uczennic kończących już swoją naukę w Akademii Magii. Skończyło się na stworzeniu anomalnego impa robiącego zdjęć śpiącej Lilianie i jeszcze większym podgrzaniu atmosfery. Ale - konserwy ZZ faktycznie posiadają dziwne substraty.

Aktor w Opowieści:

* Dokonanie:
    * Nauczycielka etyki ORAZ agentka Dare Shiver. Wpierw dostarczyła Lilianie narzędzia do robienia problemów a potem poszczuła ją dwoma uczennicami. I nic nie musiała robić.


### Uciekający seksbot

* **uid:** 190519-uciekajacy-seksbot, _numer względny_: 7
* **daty:** 0110-04-21 - 0110-04-22
* **obecni:** Arnulf Poważny, Eliza Ira, Ernest Kajrat, Liliana Bankierz, Ossidia Saitis, Pięknotka Diakon, Saitaer, Teresa Mieralit, Tomasz Tukan

Streszczenie:

Ernest z mafii przybył do szkoły magów w Zaczęstwie szukając swojego seksbota. Pięknotka deeskalowała sytuację i poszła z neuronautą Tomaszem szukać; okazało się, że seksbot jest bardzo zmodyfikowany i bardzo świadomy - do tego stopnia, że seksbotowi zaczęła pomagać Eliza Ira. Pięknotka i Tomasz rozmontowali problem, ale Pięknotka nie miała serca oddać seksbota sadystycznemu Ernestowi. W rozpaczy, poprosiła o pomoc Saitaera, który odpowiedział...

Aktor w Opowieści:

* Dokonanie:
    * pancerz dyrektora w szkole magów, zwalczająca Ernesta i biorąca potencjalny ogień na siebie. Co udowadnia, że jest baaardzo nierozsądna.


### Chrońmy Karolinę przed uczniami

* **uid:** 190113-chronmy-karoline-przed-uczniami, _numer względny_: 6
* **daty:** 0110-01-04 - 0110-01-05
* **obecni:** Adela Kirys, Arnulf Poważny, Karolina Erenit, Liliana Bankierz, Napoleon Bankierz, Pięknotka Diakon, Teresa Mieralit

Streszczenie:

Karla chciała pozbyć się Pięknotki jeszcze raz. Wysłała na papierkową robotę do Szkoły Magów w Zaczęstwie. Tam okazało się, że kandydat na terminusa (Napoleon) próbował uratować człowieka, Karolinę Erenit przed innymi uczniami szkoły magów. Niestety, eliksir który zamówił u Adeli miał efekty uboczne. Pięknotka gładko to rozwiązała i nikt nie miał problemów. Poza tym, że Karolina nadal nie jest chroniona.

Aktor w Opowieści:

* Dokonanie:
    * nauczycielka magii leczniczej i katalistka w Szkole Magów. Pomogła Lilianie.


### Stalker i Czerwone Myszy

* **uid:** 190102-stalker-i-czerwone-myszy, _numer względny_: 5
* **daty:** 0109-12-17 - 0109-12-20
* **obecni:** Jan Kramczuk, Pięknotka Diakon, Teresa Mieralit, Waldemar Mózg

Streszczenie:

Myszy napuściły Kramczuka, by ten wlazł nocą do Pięknotki - udało mu się, nie jest zbyt chroniona. Pięknotce spodobał się ów reporter-infiltrator i by się z nim bliżej poznać, przeszła się z nim na Zjawosztup. Tam, Toń pokazała Pięknotce Saitaera i sprzęgła go z nią ponownie. Dodatkowo, Pięknotka pozbyła się wszelkiego terenu a Myszy się podzieliły - część z nich wspiera Adelę i chce, by Adela przerosła Pięknotkę a część wspiera Pięknotkę z nadzieją, że dołączy do Myszy.

Aktor w Opowieści:

* Dokonanie:
    * przyszła skonsumować darmowy kupon od Pięknotki i wyszła jako megaepicka reklama gabinetu Pięknotki. Pięknotka przeszła samą siebie.


### Morderczyni-jednej-plotki

* **uid:** 190101-morderczyni-jednej-plotki, _numer względny_: 4
* **daty:** 0109-12-12 - 0109-12-16
* **obecni:** Aleksander Iczak, Erwin Galilien, Karol Szurnak, Olaf Zuchwały, Pięknotka Diakon, Teresa Mieralit

Streszczenie:

Na temat Pięknotki i jej salonu zaczęto rozpuszczać nieprzyjemne plotki. Pięknotka zlokalizowała jedno ze źródeł i je pokazowo zniszczyła, zmuszając maga do przepraszania i płaczu na kolanach. Dodatkowo, Czerwone Myszy oraz Dare Shiver zaczęli interesować się Pięknotką i jej salonem. A sama Pięknotka przecięła "największą nemesis" Adeli Kirys.

Aktor w Opowieści:

* Dokonanie:
    * nauczycielka w szkole magów. Bardzo (zbyt) zainteresowana Mrocznymi Drżeniami Cieniaszczytu, cokolwiek to jest. Agentka Czerwonych Myszy i aspekt Dare Shiver.


### Czarodziejka, która jednak może się zabić

* **uid:** 211019-czarodziejka-ktora-jednak-moze-sie-zabic, _numer względny_: 3
* **daty:** 0084-12-20 - 0084-12-24
* **obecni:** Arnulf Poważny, Klaudia Stryk, Ksenia Kirallen, Mariusz Trzewń, Maryla Koternik, Talia Aegis, Teresa Mieralit

Streszczenie:

Nastroje antynoktiańskie się nasilają. Pojawiły się seksboty do bicia, w kształcie noktian i noktianek (co strasznie zestresowało Teresę). Klaudia zaproponowała Arnulfowi powiększenie kadry o Weteranów i przejmując to prawie się zarżnęła (uratował ją Trzewń). Maryla szuka informacji o Teresie - szuka ukrytego noktianina na AMZ, ale Trzewń wykluczył Teresę dzięki dokumentom sformowanym przez Arnulfa. Klaudia i Ksenia pracują nad petycją, by stworzyć Dom Weteranów Noctis.

Aktor w Opowieści:

* Dokonanie:
    * powoli się oswaja z Klaudią i Ksenią, przez co je odpycha. Przestraszona tym, że pojawiły się seksboty "skrzywdź noktiankę". Świetna w negamagii, ma naturalny talent; rozbiła nałożony na nią geas.
* Progresja:
    * miała na sobie geas uniemożliwiający jej zabicie się, założony przez jej ojca przed śmiercią. Geas już nie działa, rozproszony przez jej negamagię.
    * nie ma pieniędzy, nie pożyczy i dlatego uważa swoje ciuchy za praktyczne. Arnulf dał jej ubrania po córce i nie kupiła nic nowego.


### Nastolatka w bieliźnie na dachu w burzy

* **uid:** 211017-nastolatka-w-bieliznie-na-dachu-w-burzy, _numer względny_: 2
* **daty:** 0084-12-14 - 0084-12-15
* **obecni:** Arnulf Poważny, Klaudia Stryk, Ksenia Kirallen, Sasza Morwowiec, Teresa Mieralit, Waldemar Grzymość

Streszczenie:

W środku nocy Klaudia i Ksenia widzą Teresę w środku burzy na dachu Złomiarium. Poszły ją ściągnąć i natknęły się na terminusa. Ksenia odciągnęła Saszę od Teresy, Klaudia Teresę wysłała do ich pokoju w akademiku (gdzie Teresa spisana za nieletnią prostytucję). Sasza wyjaśnił, że nadmiar lojalności wobec AMZ jest szkodliwy. Klaudia ogromnym wysiłkiem spowolniła Saszę i odwróciła jego uwagę. Potem Klaudia i Ksenia zdecydowały, że zsocjalizują małą noktiankę - i zdobyły trzyosobowy pokój w akademiku mimo protestów Teresy.

Aktor w Opowieści:

* Dokonanie:
    * noktiańska czarodziejka z deathwish?; trochę się boi astorian i trochę ich nienawidzi, więc jest na uboczu. Gdy terminus infiltrował AMZ rozebrała się do bielizny (by nie uszkodzić ubrania) i schowała się na dachu w burzy na Złomiarium dla dreszczyka. Uratowana przez Klaudię, podejrzana o prostytucję i współpracę z Grzymościem (o którym nawet nie wie), skończyła śpiąc na łóżku Klaudii a potem - w pokoju z nią i Ksenią. Całkowicie dzika, niezsocjalizowana.
* Progresja:
    * spisana za podejrzenie prostytucji (SRS!). Podejrzewa ją o to Sasza i pół AMZ po nocnym spacerze w "lekkim stroju".
    * mieszka w akademiku AMZ z Ksenią i Klaudią.


### Ukryta wychowanka Arnulfa

* **uid:** 211010-ukryta-wychowanka-arnulfa, _numer względny_: 1
* **daty:** 0084-12-11 - 0084-12-12
* **obecni:** Arnulf Poważny, Felicjan Szarak, Klaudia Stryk, Ksenia Kirallen, Sasza Morwowiec, Strażniczka Alair, Talia Aegis, Teresa Mieralit, Tymon Grubosz

Streszczenie:

Dyrektor Arnulf Poważny ma wychowankę - Teresę Mieralit, noktiańską piętnastonastolatkę która mieszka w AMZ. Strażniczka Alair ją wykryła i wpadła w pętlę - komponenty TAI uznały ją za potencjalne zagrożenie, BIA za osobę do ochrony. Strażniczka wycofała drony do osłony AI Core i zaatakowały Teresę. Klaudia zorientowała się w problemie, obudziła hipernetem Arnulfa. Arnulf osłonił Teresę, Klaudia wezwała Talię i w grupie udało im się opanować niesforne TAI (bo Klaudia zanomalizowała konwertery energii Strażniczki, która czerpie energię z Trzęsawiska).

Aktor w Opowieści:

* Dokonanie:
    * 15 lat; disruptorka magii i paramedyk; noktianka pod opieką dyrektora Arnulfa (jego wychowanka). Ma niewyparzoną gębę. Jej obecność spowodowała kolizję w Strażniczce - elementy BIA wykryły jako "friend", elementy TAI jako "foe".


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 12, @: 0111-10-26
    1. Primus    : 12, @: 0111-10-26
        1. Sektor Astoriański    : 12, @: 0111-10-26
            1. Astoria    : 12, @: 0111-10-26
                1. Sojusz Letejski    : 12, @: 0111-10-26
                    1. Szczeliniec    : 12, @: 0111-10-26
                        1. Powiat Pustogorski    : 12, @: 0111-10-26
                            1. Czarnopalec    : 1, @: 0111-08-01
                                1. Pusta Wieś    : 1, @: 0111-08-01
                            1. Czemerta, okolice    : 1, @: 0110-09-07
                                1. Baza Irrydius    : 1, @: 0110-09-07
                                1. Fortifarma Irrydia    : 1, @: 0110-09-07
                                1. Studnia Irrydiańska    : 1, @: 0110-09-07
                            1. Podwiert    : 2, @: 0111-08-01
                                1. Dzielnica Luksusu Rekinów    : 1, @: 0111-08-01
                                    1. Serce Luksusu    : 1, @: 0111-08-01
                                        1. Lecznica Rannej Rybki    : 1, @: 0111-08-01
                                1. Sensoplex    : 1, @: 0110-10-18
                                1. Tor Wyścigowy Pamięci    : 1, @: 0110-10-18
                            1. Pustogor    : 3, @: 0110-09-07
                                1. Barbakan    : 1, @: 0109-12-20
                                1. Eksterior    : 1, @: 0110-09-07
                                    1. Miasteczko    : 1, @: 0110-09-07
                                        1. Knajpa Górska Szalupa    : 1, @: 0110-09-07
                                1. Gabinet Pięknotki    : 2, @: 0109-12-20
                                1. Knajpa Górska Szalupa    : 1, @: 0109-12-16
                                1. Rdzeń    : 1, @: 0110-09-07
                                    1. Szpital Terminuski    : 1, @: 0110-09-07
                            1. Zaczęstwo    : 9, @: 0111-10-26
                                1. Akademia Magii, kampus    : 8, @: 0111-08-01
                                    1. Akademik    : 4, @: 0111-08-01
                                    1. Arena Treningowa    : 1, @: 0110-07-27
                                    1. Artefaktorium    : 1, @: 0110-10-18
                                    1. Audytorium    : 1, @: 0110-10-18
                                    1. Budynek Centralny    : 4, @: 0110-07-27
                                        1. Skrzydło Loris    : 2, @: 0110-07-27
                                    1. Domek dyrektora    : 1, @: 0084-12-24
                                    1. Złomiarium    : 1, @: 0084-12-15
                                1. Akademia Magii    : 1, @: 0111-10-26
                                1. Nieużytki Staszka    : 2, @: 0111-10-26
                        1. Trzęsawisko Zjawosztup    : 1, @: 0109-12-20
                            1. Toń Pustki    : 1, @: 0109-12-20

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arnulf Poważny       | 5 | ((190113-chronmy-karoline-przed-uczniami; 190519-uciekajacy-seksbot; 211010-ukryta-wychowanka-arnulfa; 211017-nastolatka-w-bieliznie-na-dachu-w-burzy; 211019-czarodziejka-ktora-jednak-moze-sie-zabic)) |
| Pięknotka Diakon     | 5 | ((190101-morderczyni-jednej-plotki; 190102-stalker-i-czerwone-myszy; 190113-chronmy-karoline-przed-uczniami; 190519-uciekajacy-seksbot; 200510-tajna-baza-orbitera)) |
| Ignacy Myrczek       | 4 | ((200326-test-z-etyki; 200510-tajna-baza-orbitera; 201013-pojedynek-akademia-rekiny; 211026-koszt-ratowania-torszeckiego)) |
| Ksenia Kirallen      | 4 | ((211010-ukryta-wychowanka-arnulfa; 211017-nastolatka-w-bieliznie-na-dachu-w-burzy; 211019-czarodziejka-ktora-jednak-moze-sie-zabic; 211026-koszt-ratowania-torszeckiego)) |
| Liliana Bankierz     | 4 | ((190113-chronmy-karoline-przed-uczniami; 190519-uciekajacy-seksbot; 200326-test-z-etyki; 201013-pojedynek-akademia-rekiny)) |
| Klaudia Stryk        | 3 | ((211010-ukryta-wychowanka-arnulfa; 211017-nastolatka-w-bieliznie-na-dachu-w-burzy; 211019-czarodziejka-ktora-jednak-moze-sie-zabic)) |
| Napoleon Bankierz    | 3 | ((190113-chronmy-karoline-przed-uczniami; 200326-test-z-etyki; 201013-pojedynek-akademia-rekiny)) |
| Julia Kardolin       | 2 | ((201013-pojedynek-akademia-rekiny; 230303-the-goose-from-hell)) |
| Mariusz Trzewń       | 2 | ((200510-tajna-baza-orbitera; 211019-czarodziejka-ktora-jednak-moze-sie-zabic)) |
| Paweł Szprotka       | 2 | ((211026-koszt-ratowania-torszeckiego; 230303-the-goose-from-hell)) |
| Sasza Morwowiec      | 2 | ((211010-ukryta-wychowanka-arnulfa; 211017-nastolatka-w-bieliznie-na-dachu-w-burzy)) |
| Talia Aegis          | 2 | ((211010-ukryta-wychowanka-arnulfa; 211019-czarodziejka-ktora-jednak-moze-sie-zabic)) |
| Tymon Grubosz        | 2 | ((200510-tajna-baza-orbitera; 211010-ukryta-wychowanka-arnulfa)) |
| Adela Kirys          | 1 | ((190113-chronmy-karoline-przed-uczniami)) |
| Aleksander Bemucik   | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Aleksander Iczak     | 1 | ((190101-morderczyni-jednej-plotki)) |
| Alex Deverien        | 1 | ((230303-the-goose-from-hell)) |
| Alicja Trawlis       | 1 | ((230303-the-goose-from-hell)) |
| Alina Anakonda       | 1 | ((200510-tajna-baza-orbitera)) |
| Aniela Kark          | 1 | ((200326-test-z-etyki)) |
| Artur Kołczond       | 1 | ((200510-tajna-baza-orbitera)) |
| Berenika Wrążowiec   | 1 | ((200326-test-z-etyki)) |
| Carmen Deverien      | 1 | ((230303-the-goose-from-hell)) |
| Eliza Ira            | 1 | ((190519-uciekajacy-seksbot)) |
| Ernest Kajrat        | 1 | ((190519-uciekajacy-seksbot)) |
| Erwin Galilien       | 1 | ((190101-morderczyni-jednej-plotki)) |
| Felicjan Szarak      | 1 | ((211010-ukryta-wychowanka-arnulfa)) |
| Gabriel Ursus        | 1 | ((200510-tajna-baza-orbitera)) |
| Jan Kramczuk         | 1 | ((190102-stalker-i-czerwone-myszy)) |
| Justynian Diakon     | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Kacper Bankierz      | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Kallista Exolon      | 1 | ((200510-tajna-baza-orbitera)) |
| Karol Szurnak        | 1 | ((190101-morderczyni-jednej-plotki)) |
| Karolina Erenit      | 1 | ((190113-chronmy-karoline-przed-uczniami)) |
| kot-pacyfikator Tobias | 1 | ((230303-the-goose-from-hell)) |
| Laura Tesinik        | 1 | ((200510-tajna-baza-orbitera)) |
| Maryla Koternik      | 1 | ((211019-czarodziejka-ktora-jednak-moze-sie-zabic)) |
| Marysia Sowińska     | 1 | ((211026-koszt-ratowania-torszeckiego)) |
| Natalia Tessalon     | 1 | ((200510-tajna-baza-orbitera)) |
| Olaf Zuchwały        | 1 | ((190101-morderczyni-jednej-plotki)) |
| Olga Myszeczka       | 1 | ((211026-koszt-ratowania-torszeckiego)) |
| Ossidia Saitis       | 1 | ((190519-uciekajacy-seksbot)) |
| Rafał Torszecki      | 1 | ((211026-koszt-ratowania-torszeckiego)) |
| Remor 340D           | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Robert Pakiszon      | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Sabina Kazitan       | 1 | ((200510-tajna-baza-orbitera)) |
| Saitaer              | 1 | ((190519-uciekajacy-seksbot)) |
| Sensacjusz Diakon    | 1 | ((211026-koszt-ratowania-torszeckiego)) |
| Stella Armadion      | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Strażniczka Alair    | 1 | ((211010-ukryta-wychowanka-arnulfa)) |
| Talarand d'Irrydius  | 1 | ((200510-tajna-baza-orbitera)) |
| Tomasz Tukan         | 1 | ((190519-uciekajacy-seksbot)) |
| Waldemar Grzymość    | 1 | ((211017-nastolatka-w-bieliznie-na-dachu-w-burzy)) |
| Waldemar Mózg        | 1 | ((190102-stalker-i-czerwone-myszy)) |
| Wiktor Satarail      | 1 | ((211026-koszt-ratowania-torszeckiego)) |
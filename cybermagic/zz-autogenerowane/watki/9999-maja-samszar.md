# Maja Samszar
## Identyfikator

Id: 9999-maja-samszar

## Sekcja Opowieści

### Gdy HR reedukuje niewłaściwą osobę

* **uid:** 230111-gdy-hr-reedukuje-niewlasciwa-osobe, _numer względny_: 13
* **daty:** 0100-09-15 - 0100-09-18
* **obecni:** Adragain Ferrias, AK Nox Ignis, Aleksy Sartaran, Arianna Verlen, Daria Czarnewik, Elena Verlen, Grażyna Burgacz, Kajetan Kircznik, Lana Mirkinin, Leszek Kurzmin, Maja Samszar, Miłosz Klinek, OO Astralna Flara, OO Athamarein, OO Loricatus, Persefona d'Loricatus, Sabrina Ferrias, Salazar Bolza, Szczepan Myrczek, Władawiec Diakon

Streszczenie:

Niestety, Kircznik (lekarz) przestraszył Sargona i młodych infiltratorów Lodowcem, Bolzą i ogólnie duchami noktiańskimi. Okazało się, że Grażyna jest ekspertem od duchów i jest w stanie zrobić egzorcyzm. Niedługo potem (i chwilę przed egzorcyzmem) pojawiła się szybka korweta z Biur HR i poprosili o pomoc w odparciu Anomalii Kosmicznej. Myrczek sprawdził jej pamięć i Athamarein i Flara poleciały na starcie z Nox Ignis. Nawiązały kontakt z Biurami HR, przechwyciły biednego męża pilotki Nox Ignis i poznały prawdę o Destructor Animarum i wpływie Syndykatu Aureliona na Biura HR. Kurzmin pobił rekordy Athamarein, Arianna skutecznie zaplanowała wypalenie pilotki Nox Ignis przez własny statek. Tylko advancer Miłosz (badający ślady Nox Ignis) jest MIA.

Aktor w Opowieści:

* Dokonanie:
    * o dziwo nie zna się na duchach; tym się nie interesowała. Ale wie, że Grażyna tak. Za to pomaga Darii złożyć ekrany ochronne chroniące je przed banshee Nox Ignis.


### Astralna Flara i nowy komodor

* **uid:** 221221-astralna-flara-i-nowy-komodor, _numer względny_: 12
* **daty:** 0100-09-09 - 0100-09-12
* **obecni:** Arianna Verlen, Daria Czarnewik, Elena Verlen, Ellarina Samarintael, Kajetan Kircznik, Kaspian Certisarius, Leszek Kurzmin, Maja Samszar, NekroTAI Zarralea, OO Astralna Flara, OO Athamarein, OO Loricatus, Salazar Bolza, Sargon Niiris, Władawiec Diakon

Streszczenie:

Po "uratowaniu" noktian z Dominy Lucis Arianna dostała medal a Lodowca zastąpił komodor Bolza, zimny drakolita. Gdy grupa wydzielona wróciła na teren Anomalii Kolapsu okazało się, że planetoida Kazmirian ma squatterów; Zespół skutecznie ich przejął odzyskując kontrolę nad systemami Semli d'Kazmirian. Na miejscu - TKO-4271 - okazało się że Zarralea jest zniszczona. Orbiter odzyskał swoją bazę, acz Loricatis musi współdzielić Persefonę. Z innej beczki: link Elena - Władawiec się pięknie rozwija.

Aktor w Opowieści:

* Dokonanie:
    * spięła się z Kazmirian, ma informacje kto jest w środku i przestawiła lifesupport by wszystkich uśpić i unieszkodliwić. Dobre ćwiczenie retransmitując z Athamarein. Wyjątkowo dobrze jej poszło.


### Astralna Flara w strefie duchów

* **uid:** 221130-astralna-flara-w-strefie-duchow, _numer względny_: 11
* **daty:** 0100-08-07 - 0100-08-10
* **obecni:** Arianna Verlen, Arnulf Perikas, Daria Czarnewik, Elena Verlen, Ellarina Samarintael, Gabriel Lodowiec, Hubert Kerwelenios, Kajetan Kircznik, Kirea Rialirat, Klarysa Jirnik, Leo Kasztop, Maja Samszar, NekroTAI Zarralea, OO Astralna Flara, OO Athamarein, Szymon Wanad, Tomasz Dojnicz

Streszczenie:

Planetoida Kazmirian została przejęta przez Orbiter. Daria ostrzegła Nonarion (Leo), że Orbiter nadal poluje na TAI i zdobyła informację o 'strefie duchów'. Z Mają doszły do tego, że 'strefa duchów' jest świetnym maskowaniem; Lodowiec wyprowadził tam Orbiter a Arianna stworzyła nekroTAI z Zarralei d'Isigtand. Używając advancera przechwycili samotną savarankę (której Lodowiec nie umie przesłuchać) i stanęło na tym, że 'strefa duchów' jest bazą noktiańską zbudowaną dookoła jednostki wsparcia, 'Domina Lucis'. Lodowiec chce doprowadzić noktian do sprawiedliwości Orbitera, Arianna i Daria są skonfliktowane. Mają jeńca - Kireę, lokalną młodą savarankę.

Aktor w Opowieści:

* Dokonanie:
    * dekodowała dane dostarczone przez Darię; doszła do tego, że wszystko wskazuje na sojusz noktian i TAI? Niestety, za słaba - nie ma na to dowodów. Ma gwiazdki w oczach - potencjalnie walczą z NOKTIANAMI WROGAMI LUDZKOŚCI! Zachowała dla Arianny dyskrecję o danych. Zrobiła potrawę dla noktianki Kirei; nieco za mocną, ale jej doświadczenia z niewolnikami noktiańskimi się sprawdza.


### Astralna Flara dociera do Nonariona Nadziei

* **uid:** 221116-astralna-flara-dociera-do-nonariona-nadziei, _numer względny_: 10
* **daty:** 0100-07-11 - 0100-07-14
* **obecni:** Alan Nierkamin, Alezja Dumorin, Arianna Verlen, Arnulf Perikas, Daria Czarnewik, Gabriel Lodowiec, Grażyna Burgacz, Kajetan Kircznik, Leo Kasztop, Leszek Kurzmin, Maja Samszar, Marcel Kulgard, OO Astralna Flara, OO Athamarein, SCA Hadiah Emas, SCA Isigtand, Szymon Wanad, Tomasz Ruppok, Władawiec Diakon

Streszczenie:

Flara dociera do Nonariona, który wygląda jak absolutny śmieć. Morale na Flarze ucierpiało - aż do plotek o Egzotycznych Pięknościach, co sprawiło Ariannie kolejne problemy. Alan, eks-lokals powiedział Darii że jest jakaś forma standaryzacji na Nonarionie - coś tam jest, coś "bogatego" jako frakcja. Daria zdobyła od Leo planetoidę którą może użyć jako bazę dla Orbitera, ale w drodze tam zboczyli by ratować uszkodzone statki lokalne. I uratowali jednostkę niewolniczą i zniszczony grazer. Komodor Lodowiec - sensowny koleś - ma dylemat moralny. Neikatiańska TAI oraz niewolnicy.

Aktor w Opowieści:

* Dokonanie:
    * absolutnie załamana stanem Nonariona - jej pierwsza myśl to coup de grace. Pełni rolę comms experta bez problemu.


### Astralna Flara i porwanie na Karsztarinie

* **uid:** 221102-astralna-flara-i-porwanie-na-karsztarinie, _numer względny_: 9
* **daty:** 0100-05-30 - 0100-06-05
* **obecni:** Ada Wyrocznik, Alezja Dumorin, Arianna Verlen, Arnulf Perikas, Daria Czarnewik, Elena Verlen, Gerwazy Kircznik, Grażyna Burgacz, Hubert Kerwelenios, Kajetan Kircznik, Klarysa Jirnik, Maja Samszar, Mariusz Bulterier, OO Karsztarin, OO Optymistyczny Żuk, Szczepan Myrczek, Władawiec Diakon

Streszczenie:

Arianna otrzymała nowy okręt - jednostkę wsparcia Astralna Flara. Jako pilota dostała Elenę. Elena ma problemy z aklimatyzacją na Orbiterze; chce być najlepsza i walczy o to. Nie rozmawia z ludźmi. Arianna zdecydowała się poprosić Alezję o pomoc w tej dziedzinie. Tymczasem przy ćwiczeniach na Karsztarinie okazało się, że Flara jest tam w czasie porwania - Zespołowi Flary udało się zatrzymać porwanie, choć operacja była niesamowicie niebezpieczna - niedoświadczony Zespół vs 4 komandosów Syndykatu Aureliona.

Aktor w Opowieści:

* Dokonanie:
    * próbuje przekonać Ariannę, by zamówiła specjalne substraty do jedzenia. Done, uda się - ale ona czasem dokarmi oficerów swoimi potrawami. Nie jest fanką Eleny więc chciała by Daria sprawdziła funkcje komunikacyjne - okazało się, że to sygnał przez relay na Karsztarinie. Stąd wiedzą o porwaniu.


### Kapitan Verlen i pierwszy ruch statku

* **uid:** 221019-kapitan-verlen-i-pierwszy-ruch-statku, _numer względny_: 8
* **daty:** 0100-03-25 - 0100-04-06
* **obecni:** Arianna Verlen, Daria Czarnewik, Erwin Pies, Maja Samszar, Marcelina Trzęsiel, OO Królowa Kosmicznej Chwały, Romeo Verlen, Rufus Warkoczyk, Stefan Torkil

Streszczenie:

Arianna włączyła ćwiczenia dla Królowej - prawie wyszło, ale ludzie popanikowali a statek odmówił posłuszeństwa i załoga zaczęła się bić. Arianna ich przećwiczyła w działaniach awaryjnych i oddała panikarza medykowi - niestety, Paradoks zmienił Marcela w Marcelinę... ale następnym razem ćwiczenia powinny się już udać. Daria ma sprawę pod kontrolą i Arianna też raczej jest pewna sukcesu.

Aktor w Opowieści:

* Dokonanie:
    * przekonana przez Ariannę że ma do wyboru - być posłuszną lub jeść konserwy. Maja to foodie. Robi ciasta. Kocha jedzenie. Arianna jej obiecała, że pomoże jej się zemścić na Romeo - ale Maja ma Ariannie nie przeszkadzać. Nieufna, ale zneutralizowana.


### Kapitan Verlen i niezapowiedziana inspekcja

* **uid:** 221012-kapitan-verlen-i-niezapowiedziana-inspekcja, _numer względny_: 7
* **daty:** 0100-03-19 - 0100-03-23
* **obecni:** Adam Chrząszczewicz, Arianna Verlen, Arnulf Perikas, Daria Czarnewik, Hubert Kerwelenios, Leona Astrienko, Maja Samszar, Mariusz Bulterier, OO Królowa Kosmicznej Chwały, Szczepan Myrczek, Tomasz Dojnicz, Władawiec Diakon

Streszczenie:

Podczas skanu kadłuba Królowej advancerzy z Darią wykryli scrambler TAI, destabilizator silników itp. Arianna stoczyła bitwę ćwiczebną z marine, ale pojawiła się inspekcja (która nie była zapowiedziana bo Maja nie przekazała informacji). Daria reanimowała TAI, wszystko częściowo poszło nie tak. Ale o dziwo audytor chciał pomóc Ariannie, która stanęła między Orbiterem (wsparcie) i Aurum (my sami sobie poradzimy). Arianna upewniła się, że Władawiec nie stał za operacją - najpewniej Maja. Ale Królowa jest w lepszym stanie niż kiedykolwiek, tylko, że Arianna je konserwy (by nie wpaść pod feromony itp)

Aktor w Opowieści:

* Dokonanie:
    * znalazła perfekcyjny sposób na usunięcie Arianny - nie powiedziała jej o inspekcji i wpakowała ją w trening z marine. Potem pousuwała dowody. Ma sprawę osobistą, nie tylko z uwagi na Władawca. Ale - Arianna nie została usunięta a reaktywacja Semli sprawia, że możliwości zaszkodzenia Ariannie się Mai skończyły.


### Kapitan Verlen i pojedynek z marine

* **uid:** 220928-kapitan-verlen-i-pojedynek-z-marine, _numer względny_: 6
* **daty:** 0100-03-16 - 0100-03-18
* **obecni:** Alezja Dumorin, Arianna Verlen, Arnulf Perikas, Daria Czarnewik, Erwin Pies, Grażyna Burgacz, Leona Astrienko, Leszek Kurzmin, Maja Samszar, OO Królowa Kosmicznej Chwały, Stefan Torkil, Szymon Wanad, Tomasz Ruppok, Władawiec Diakon

Streszczenie:

Królowa miała się przesunąć, ale prawie się rozbiła; ta jednostka jest rozkradziona. Arianna zatrzymała biczowanie załogantów i doszła do tego, że załoga jest niemrawa i skonfliktowana bo część ludzi jest z Aurum a część nie. Perfect storm of suck. By zatrzymać Wanada przed "wygraniem cnoty koleżanki inżyniera" stoczyła z nim pojedynek i wygrała. Leona ostrzegła, by Arianna nic nie jadła i nie piła. Czyli coś jest w jedzeniu. To tłumaczy czemu Królowa jest tak zdegenerowaną jednostką. Aha, Arianna ma wsparcie kapitana Leszka Kurzmina, dawnego rywala Alezji.

Aktor w Opowieści:

* Dokonanie:
    * koordynowała sygnały między Arianną a Leszkiem Kurzminem. Aktywnie Arianny nie sabotowała, ale nie pomagała.


### Kapitan Verlen i Królowa Kosmicznej Chwały

* **uid:** 220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly, _numer względny_: 5
* **daty:** 0100-03-08 - 0100-03-14
* **obecni:** Alezja Dumorin, Arianna Verlen, Arnulf Perikas, Daria Czarnewik, Erwin Pies, Grażyna Burgacz, Klarysa Jirnik, Klaudiusz Terienak, Leona Astrienko, Maja Samszar, OO Królowa Kosmicznej Chwały, Szymon Wanad, Władawiec Diakon

Streszczenie:

Królowa Kosmicznej Chwały to najbardziej dysfunkcyjna jednostka pod kontrolą Orbitera - służy do łamania karier obiecujących oficerów i sabotażu programu kosmicznego Aurum (oba z woli sił specjalnych Aurum). Trafiła na królową Arianna i zaczęła robić porządek. Poprzednią panią kapitan zdecydowała się odratować, współpracując z Darią z inżynierii rozmontować wyścigi psów i stwierdziła, że doprowadzi do tego, by Królowa zaczęła skutecznie działać podczas ćwiczeń. Aha, tu poznała się z Leoną Astrienko ;-).

Aktor w Opowieści:

* Dokonanie:
    * oficer łącznościowy Królowej i tien; nie przekazała informacji o przybyciu Arianny Alezji. Stoi lojalnie za Władawcem.


### Studenci u Verlenów

* **uid:** 210311-studenci-u-verlenow, _numer względny_: 4
* **daty:** 0096-11-18 - 0096-11-24
* **obecni:** Apollo Verlen, Arianna Verlen, Dariusz Blakenbauer, Elena Verlen, Maja Samszar, Michał Perikas, Rafał Perikas, Rufus Samszar, Sylwia Perikas, Viorika Verlen

Streszczenie:

Verlenowie mieli wymianę studencką, współpracę z magami rodów Perikas i Samszar. Dariusz Blakenbauer chciał rozbić potencjalny sojusz, dając młodym Perikasom lustro mające zaspokoić ich ochotę na Elenę. Niestety, wszystko poszło katastrofalnie nie tak bo Elena ma anomalną magię - w wyniku eksplozji zginęło kilkanaście wił i 2 żołnierzy. Elena w ruinie psychicznej, wszyscy przerażeni. Wezwane Arianna i Viorika odkryły że za wszystkim stoi Blakenbauer i znalazły winnych wśród młodych Perikasów. Jednak - dla polityki - wszystko zwaliły na Blakenbauerów :D.

Aktor w Opowieści:

* Dokonanie:
    * 15 lat, uroczy, mały diabełek, w co nikt nie wierzy. Poza Eleną, która widziała Maję w akcji. Podżegaczka.


### Piękna Diakonka i rytuał nirwany kóz

* **uid:** 230606-piekna-diakonka-i-rytual-nirwany-koz, _numer względny_: 3
* **daty:** 0095-08-15 - 0095-08-18
* **obecni:** AJA Szybka Strzała, Dźwiedź Łagodne Słowo, Elena Samszar, Herbert Samszar, Impresja Ignicja Incydencja Diakon, Karolinus Samszar, Maks Samszar

Streszczenie:

Itria Diakon ma kolejny projekt - nirwana kóz u Samszarów. Jej obecność spowodowała chaos w Forcie Tawalizer. Karolinus i Elena S. dotarli tam by dowiedzieć się o tajnych podziemnych bazach Samszarów od Herberta, ale musieli wpierw rozwiązać problem z Itrią. W końcu doprowadzili do rytuału kóz (dzięki Elenie i jej researchowi) i trochę naprawili sytuację. Czas spotkać się z Herbertem poza terenem sentisieci i poznać sekrety badań nad duchami.

Aktor w Opowieści:

* Dokonanie:
    * AKTOR NIEOBECNY NA SESJI
* Progresja:
    * Romeo poinformował ją o działaniach Eleny S. i że on nie ma zamiaru się więcej zadawać z jej 'crazy bloodline'.


### Romeo, dyskretny instalator Supreme Missionforce

* **uid:** 230523-romeo-dyskretny-instalator-supreme-missionforce, _numer względny_: 2
* **daty:** 0095-08-09 - 0095-08-11
* **obecni:** AJA Szybka Strzała, Albert Samszar, Elena Samszar, Karolinus Samszar, Maja Samszar, Nataniel Samszar, Romeo Verlen

Streszczenie:

Strzała ma za zadanie wychować i nauczyć współpracy Karolinusa i Eleny. Karolinus jest wezwany do interwencji, gdy Maja przesyła sygnał SOS. Maja twierdzi, że niechcący mogła zabić kolegę. Ekipa dociera na miejsce, gdzie odkrywają ciężarówkę z zaawansowanym sprzętem. Maja jest w stanie panicznym, a jej magia wymyka się spod kontroli - próbuje przebić dziwne pole siłowe otaczające dziurę w piwnicy biurowca. Karolinus i Elena zmuszają Maję do opuszczenia miejsca, obiecując uratować Romeo. W międzyczasie, Strzała zabiera Maję do Ogrodów Medytacyjnych by nie miała problemów z ojcem. Elena zyskuje na czasie przed ojcem Mai (Albertem), dopóki Maja nie jest bezpiecznie umieszczona w Ogrodach. Potem wydobywają Romeo z podziemi zauważając, że ktoś tam ma tajną dziwną bazę do eksperymentów na duchach.

Aktor w Opowieści:

* Dokonanie:
    * uroczy diabełek (UR: creativity), nieszczęśliwa bo musi być "elegancką damą" a ma talent w Supreme Missionforce. Opieprzyła Romeo we wściekłości gdy on ją wyśmiał (bo nikogo nie miała) i Romeo przyjechał jej to zamontować. Znalazła miejsce, ale jak się okazało że jest tam ukryta baza i Romeo wpadł w dziurę, spanikowała. Wezwała Karolinusa (bo nikogo nie miała) i nie chciała zostawić Romeo (KTÓREGO NAWET NIE LUBI!). Lojalna. W końcu - nie wpadła w kłopoty, bo z rozkazu Karolinusa i Strzały schowała się w Ogrodach Medytacji XD


### Niepotrzebny ratunek Mai

* **uid:** 230328-niepotrzebny-ratunek-mai, _numer względny_: 1
* **daty:** 0095-06-30 - 0095-07-02
* **obecni:** AJA Szybka Strzała, Apollo Verlen, Bonifacy Samszar, Fiona Szarstasz, Karolinus Samszar, Maja Samszar, Romeo Verlen, Viorika Verlen

Streszczenie:

Tydzień po ucieczce z Verlenlandu, Maja, kuzynka Karolinusa, zostaje porwana. Karolinus próbuje zdobyć informacje i nagrania z porwania. Odkrywa, że Maja miała kontakt z Romeo Verlenem, a jej rodzice kasują informacje o nieeleganckim zachowaniu córki. Karolinus łączy się z Romeem, który twierdzi, że Maja jest bezpieczna w mieście VirtuFortis (i ogólnie jest OK). Karolinus wyrusza tam ze swoim zespołem na pokładzie Szybkiej Strzały. 

Niestety, podczas Paradoksu udało się przypadkowo Karolinusowi zaatakować duchami o kształcie Mai Verlenland. Docierają w końcu do VirtuFortis i Karolinus kontaktuje się z Mają. Maja twierdzi, że jest porwana, ale Fiona wyczuwa, że Maja ściemnia. Okazuje się, że Maja uciekła z domu i przyjechała na turniej Supreme Missionforce, żeby zmierzyć się z Romeem. Karolinus przekonuje Maję, żeby wzięła udział w turnieju, a on odbierze ją po tygodniu. Reputacja Karolinusa wzrasta wśród Verlenów, ale spada wśród Samszarów. Fiona zostaje uważana za zły wpływ na Karolinusa.

Aktor w Opowieści:

* Dokonanie:
    * ma ZATARG z Romeem (ma zespół w Supreme Missionforce) i pojechała walczyć z nim w VirtuFortis uciekając przed rodzicami. Karolinus jej pomógł, co się mu u Mai bardzo zapisała na plus.
* Progresja:
    * rodzice Mai są z tych... "moja córka jest elegancka, grzeczna i jest prawdziwą arystokratką Aurum." więc "Verlenowie musieli porwać".


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 12, @: 0100-09-18
    1. Primus    : 12, @: 0100-09-18
        1. Sektor Astoriański    : 12, @: 0100-09-18
            1. Astoria    : 3, @: 0096-11-24
                1. Sojusz Letejski    : 3, @: 0096-11-24
                    1. Aurum    : 3, @: 0096-11-24
                        1. Powiat Samszar    : 1, @: 0095-08-11
                            1. Karmazynowy Świt, okolice    : 1, @: 0095-08-11
                                1. Centrum Danych Symulacji Zarządzania    : 1, @: 0095-08-11
                                    1. Techbunkier Arvitas    : 1, @: 0095-08-11
                                        1. Kontrola bezpieczeństwa (1)    : 1, @: 0095-08-11
                                        1. Kwatery mieszkalne (1)    : 1, @: 0095-08-11
                            1. Karmazynowy Świt    : 1, @: 0095-08-11
                        1. Verlenland    : 2, @: 0096-11-24
                            1. Poniewierz, obrzeża    : 1, @: 0096-11-24
                                1. Jaskinie Poniewierskie    : 1, @: 0096-11-24
                            1. Poniewierz, północ    : 1, @: 0096-11-24
                                1. Osiedle Nadziei    : 1, @: 0096-11-24
                            1. Poniewierz    : 1, @: 0096-11-24
                                1. Dom Uciech Wszelakich    : 1, @: 0096-11-24
                                1. Zamek Gościnny    : 1, @: 0096-11-24
                            1. VirtuFortis    : 1, @: 0095-07-02
                                1. Akademia VR Aegis    : 1, @: 0095-07-02
                                1. Bastion Przyrzeczny    : 1, @: 0095-07-02
                                1. Bazarek Lokalny    : 1, @: 0095-07-02
                                1. Stadion Sportowy    : 1, @: 0095-07-02
                                1. Wielki Plac Miraży    : 1, @: 0095-07-02
            1. Obłok Lirański    : 4, @: 0100-09-18
                1. Anomalia Kolapsu, orbita    : 4, @: 0100-09-18
                    1. Planetoida Kazmirian    : 1, @: 0100-07-14
                    1. SC Nonarion Nadziei    : 1, @: 0100-07-14
                        1. Moduł ExpanLuminis    : 1, @: 0100-07-14
                    1. Strefa Biur HR    : 1, @: 0100-09-18
                    1. Strefa Upiorów Orbitera    : 3, @: 0100-09-18
                        1. Planetoida Kazmirian    : 2, @: 0100-09-12
                        1. Planetoida Lodowca    : 3, @: 0100-09-18

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 10 | ((210311-studenci-u-verlenow; 220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221019-kapitan-verlen-i-pierwszy-ruch-statku; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221130-astralna-flara-w-strefie-duchow; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Daria Czarnewik      | 9 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221019-kapitan-verlen-i-pierwszy-ruch-statku; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221130-astralna-flara-w-strefie-duchow; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Władawiec Diakon     | 7 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Arnulf Perikas       | 6 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221130-astralna-flara-w-strefie-duchow)) |
| Elena Verlen         | 5 | ((210311-studenci-u-verlenow; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221130-astralna-flara-w-strefie-duchow; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Grażyna Burgacz      | 5 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Kajetan Kircznik     | 5 | ((221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221130-astralna-flara-w-strefie-duchow; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Alezja Dumorin       | 4 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Leszek Kurzmin       | 4 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| OO Astralna Flara    | 4 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221130-astralna-flara-w-strefie-duchow; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| OO Athamarein        | 4 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221130-astralna-flara-w-strefie-duchow; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| OO Królowa Kosmicznej Chwały | 4 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221019-kapitan-verlen-i-pierwszy-ruch-statku)) |
| Szymon Wanad         | 4 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221130-astralna-flara-w-strefie-duchow)) |
| Erwin Pies           | 3 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221019-kapitan-verlen-i-pierwszy-ruch-statku)) |
| Hubert Kerwelenios   | 3 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221130-astralna-flara-w-strefie-duchow)) |
| Klarysa Jirnik       | 3 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221130-astralna-flara-w-strefie-duchow)) |
| Leona Astrienko      | 3 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly; 220928-kapitan-verlen-i-pojedynek-z-marine; 221012-kapitan-verlen-i-niezapowiedziana-inspekcja)) |
| Romeo Verlen         | 3 | ((221019-kapitan-verlen-i-pierwszy-ruch-statku; 230328-niepotrzebny-ratunek-mai; 230523-romeo-dyskretny-instalator-supreme-missionforce)) |
| Szczepan Myrczek     | 3 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221102-astralna-flara-i-porwanie-na-karsztarinie; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| AJA Szybka Strzała   | 2 | ((230328-niepotrzebny-ratunek-mai; 230523-romeo-dyskretny-instalator-supreme-missionforce)) |
| Apollo Verlen        | 2 | ((210311-studenci-u-verlenow; 230328-niepotrzebny-ratunek-mai)) |
| Ellarina Samarintael | 2 | ((221130-astralna-flara-w-strefie-duchow; 221221-astralna-flara-i-nowy-komodor)) |
| Gabriel Lodowiec     | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221130-astralna-flara-w-strefie-duchow)) |
| Karolinus Samszar    | 2 | ((230328-niepotrzebny-ratunek-mai; 230523-romeo-dyskretny-instalator-supreme-missionforce)) |
| Leo Kasztop          | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221130-astralna-flara-w-strefie-duchow)) |
| Mariusz Bulterier    | 2 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| NekroTAI Zarralea    | 2 | ((221130-astralna-flara-w-strefie-duchow; 221221-astralna-flara-i-nowy-komodor)) |
| OO Loricatus         | 2 | ((221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Salazar Bolza        | 2 | ((221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Stefan Torkil        | 2 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221019-kapitan-verlen-i-pierwszy-ruch-statku)) |
| Tomasz Dojnicz       | 2 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221130-astralna-flara-w-strefie-duchow)) |
| Tomasz Ruppok        | 2 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Viorika Verlen       | 2 | ((210311-studenci-u-verlenow; 230328-niepotrzebny-ratunek-mai)) |
| Ada Wyrocznik        | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Adam Chrząszczewicz  | 1 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja)) |
| Adragain Ferrias     | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| AK Nox Ignis         | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Alan Nierkamin       | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Albert Samszar       | 1 | ((230523-romeo-dyskretny-instalator-supreme-missionforce)) |
| Aleksy Sartaran      | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Bonifacy Samszar     | 1 | ((230328-niepotrzebny-ratunek-mai)) |
| Dariusz Blakenbauer  | 1 | ((210311-studenci-u-verlenow)) |
| Elena Samszar        | 1 | ((230523-romeo-dyskretny-instalator-supreme-missionforce)) |
| Fiona Szarstasz      | 1 | ((230328-niepotrzebny-ratunek-mai)) |
| Gerwazy Kircznik     | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Kaspian Certisarius  | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| Kirea Rialirat       | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| Klaudiusz Terienak   | 1 | ((220921-kapitan-verlen-i-krolowa-kosmicznej-chwaly)) |
| Lana Mirkinin        | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Marcel Kulgard       | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Marcelina Trzęsiel   | 1 | ((221019-kapitan-verlen-i-pierwszy-ruch-statku)) |
| Michał Perikas       | 1 | ((210311-studenci-u-verlenow)) |
| Miłosz Klinek        | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Nataniel Samszar     | 1 | ((230523-romeo-dyskretny-instalator-supreme-missionforce)) |
| OO Karsztarin        | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| OO Optymistyczny Żuk | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Persefona d'Loricatus | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Rafał Perikas        | 1 | ((210311-studenci-u-verlenow)) |
| Rufus Samszar        | 1 | ((210311-studenci-u-verlenow)) |
| Rufus Warkoczyk      | 1 | ((221019-kapitan-verlen-i-pierwszy-ruch-statku)) |
| Sabrina Ferrias      | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Sargon Niiris        | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| SCA Hadiah Emas      | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| SCA Isigtand         | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Sylwia Perikas       | 1 | ((210311-studenci-u-verlenow)) |
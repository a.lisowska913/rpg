# Julia Morwisz
## Identyfikator

Id: 9999-julia-morwisz

## Sekcja Opowieści

### Niespodziewany wpływ Aidy

* **uid:** 190804-niespodziewany-wplyw-aidy, _numer względny_: 6
* **daty:** 0110-06-26 - 0110-06-28
* **obecni:** Amanda Kajrat, Gabriel Ursus, Julia Morwisz, Ossidia Saitis, Pięknotka Diakon, Saitaer

Streszczenie:

Po złapaniu Aidy przez Hralwagha z Cieniaszczytu okazało się, że Aida wpłynęła na samego Hralwagha. Potwór splugawił swoich kontrolerów z Cieniaszczytu. Pięknotka współpracując z Ossidią i Saitaerem pokonała Hralwagha zanim komukolwiek stała się krzywda. Dodatkowo - Ossidia porwała Amandę Kajrat dla Saitaera; Ossidia dalej poluje na Ernesta Kajrata.

Aktor w Opowieści:

* Dokonanie:
    * przekonała kilka osób w Cieniaszczycie do ratowania Emulatorów Orbitera; niestety, Orbiter odcina dostęp środków a Hralwagh zwariował. Przestała robić cokolwiek.


### Odzyskana agentka Orbitera

* **uid:** 190724-odzyskana-agentka-orbitera, _numer względny_: 5
* **daty:** 0110-06-08 - 0110-06-11
* **obecni:** Adam Szarjan, Aida Serenit, Julia Morwisz, Mirela Niecień, Mirela Orion, Moktar Gradon, Pięknotka Diakon

Streszczenie:

Aidę porwała frakcja Cieniaszczytu zainspirowana przez Julię - by ratować Emulatorkę. Ale Aida, jak się okazało, jest nędznym cywilem i to takim uratowanym z kosmosu. Pięknotka wygrała z championem kralotha przez zanęcenie Moktara - a zwabiła go dając mu walkę z Mirelą. Ogólnie, wszyscy są zadowoleni..?

Aktor w Opowieści:

* Dokonanie:
    * opracowała plan uratowania Emulatorki a porwała Aidę. Cóż. Pomogła Pięknotce odkręcić sprawę, ale nie do końca jej wyszło.


### Adieu, Cieniaszczycie

* **uid:** 181227-adieu-cieniaszczycie, _numer względny_: 4
* **daty:** 0109-11-27 - 0109-11-30
* **obecni:** Amadeusz Sowiński, Atena Sowińska, Brygida Maczkowik, Erwin Galilien, Lilia Ursus, Mirela Niecień, Moktar Gradon, Pietro Dwarczan, Pięknotka Diakon, Waleria Cyklon, Zbigniew Burzycki

Streszczenie:

Pięknotka zdobyła nowe ciało dla Minerwy (acz do jego uruchomienia człowiek musi być poświęcony!), przekonała Amadeusza do założenia świątyni Arazille do blokowania Finis Vitae oraz wepchnęła Lilię jako asystentkę Atenie. Do tego rozkochała solidnie Erwina i zamknęła wszystkie wątki, by móc wracać do Pustogoru.

Aktor w Opowieści:

* Dokonanie:
    * AKTOR NIEOBECNY NA SESJI
* Progresja:
    * zostaje Łysym Psem, ale z wyboru. Może mu powiedzieć "nie". Jest jak Waleria a nie jak zabawka.


### Czyszczenie toksycznych związków

* **uid:** 181225-czyszczenie-toksycznych-zwiazkow, _numer względny_: 3
* **daty:** 0109-11-12 - 0109-11-16
* **obecni:** Atena Sowińska, Bogdan Szerl, Julia Morwisz, Moktar Gradon, Pięknotka Diakon, Romuald Czurukin

Streszczenie:

Pięknotka ściągnęła Erwina na detoks od Saitaera, acz dała mu informację o Kreacjuszu Diakonie. Saitaer planuje zbudować sprawne ciało dla Minerwy. Atena jest zregenerowana w dość anielskiej formie i dostaje informacje o Orbiterze od Julii. Pięknotka próbuje naprawić pary Romuald x Julia oraz Wioletta x Pietro. Moktar uwalnia Pięknotkę od wpływu Bogdana, masakrując swojego agenta w brutalny sposób.

Aktor w Opowieści:

* Dokonanie:
    * w trójkąciku z Romualdem i Pięknotką. Źródło informacji dla Ateny. Ogólnie, nędzny los jak na Emulatora.


### Wolna od terrorforma

* **uid:** 181216-wolna-od-terrorforma, _numer względny_: 2
* **daty:** 0109-11-04 - 0109-11-10
* **obecni:** Atena Sowińska, Bogdan Szerl, Julia Morwisz, Mirela Niecień, Pięknotka Diakon

Streszczenie:

Pięknotka chciała uwolnić się od terrorforma - udało jej się, gdyż oddała się we władzę straszliwego mindwarpa Łysych Psów, Bogdana Szerla. Szerl wraz z kralothami zrekonstruowali Wzór Pięknotki - teraz jest odporna na wpływy terrorforma, ale jednocześnie jest bardzo zmieniona. Sensual, dangerous, chemical. Przy okazji Julia (Emulator) oddała serce Pięknotce a Atena zatrzymała swój przydział na Epirjon.

Aktor w Opowieści:

* Dokonanie:
    * wpierw wykorzystana przez Pięknotkę jako ostateczna linia obrony (zadziałało), potem jako wsparcie do wyciągnięcia (nie zadziałało, Pięknotka wyciągała ją). Oddała kontrolę nad sobą Pięknotce.
* Progresja:
    * straszna słabość i oddanie wobec Pięknotki Diakon. Nie potrafi odmówić jej rozkazów. Co więcej, pragnie tych rozkazów.


### Romuald i Julia

* **uid:** 181118-romuald-i-julia, _numer względny_: 1
* **daty:** 0109-10-26 - 0109-10-28
* **obecni:** Adam Szarjan, Julia Morwisz, Pietro Dwarczan, Pięknotka Diakon, Romuald Czurukin

Streszczenie:

Pięknotka pomogła Romualdowi uratować jego ukochaną Julię. Po drodze był też groźny robot bojowy. Docelowo okazało się, że "Julia" jest Emulatorem - agentem Orbitera Pierwszego której celem było zaprzyjaźnić się z Ateną i Pięknotką by je obserwować. Pięknotka zneutralizowała Emulatora Romualdem i jego miłością plus Pryzmat, acz dwa razy skończyła w szpitalu podczas jednej sesji (rekord).

Aktor w Opowieści:

* Dokonanie:
    * Emulator wysłana by zaprzyjaźnić się z Pięknotką. Slightly creepy. Zakochała się w Romualdzie i ostro pobiła Pięknotkę. Uzyskała wolność.
* Progresja:
    * jest wolna. Jako Emulator wyrwała się spod kontroli koordynatorów, jak długo jest w Cieniaszczycie
    * jest niezdolna do zrobienia jakiejkolwiek krzywdy ani Romualdowi ani Pięknotce - nawet by ratować ich życie, nawet pod kontrolą Krwi.


## Sekcja Światowości

### Zwiedzony świat

1. Lokalizacja: (ile razy), @ (data ostatniej wizyty)

1. Świat    : 5, @: 0110-06-28
    1. Primus    : 5, @: 0110-06-28
        1. Sektor Astoriański    : 5, @: 0110-06-28
            1. Astoria    : 5, @: 0110-06-28
                1. Sojusz Letejski, NW    : 2, @: 0109-11-16
                    1. Ruiniec    : 2, @: 0109-11-16
                        1. Colubrinus Meditech    : 2, @: 0109-11-16
                1. Sojusz Letejski    : 5, @: 0110-06-28
                    1. Przelotyk    : 4, @: 0110-06-11
                        1. Przelotyk Wschodni    : 4, @: 0110-06-11
                            1. Cieniaszczyt    : 4, @: 0110-06-11
                                1. Knajpka Szkarłatny Szept    : 3, @: 0110-06-11
                                1. Kompleks Nukleon    : 4, @: 0110-06-11
                                1. Mrowisko    : 1, @: 0109-11-16
                                1. Wewnątrzzbocze Zachodnie    : 1, @: 0109-10-28
                                    1. Panorama Światła    : 1, @: 0109-10-28
                            1. Przejściak    : 1, @: 0110-06-11
                                1. Hotel Pirat    : 1, @: 0110-06-11
                    1. Szczeliniec    : 1, @: 0110-06-28
                        1. Powiat Pustogorski    : 1, @: 0110-06-28
                            1. Podwiert, okolice    : 1, @: 0110-06-28
                                1. Bioskładowisko podziemne    : 1, @: 0110-06-28
                            1. Podwiert    : 1, @: 0110-06-28
                                1. Magazyny sprzętu ciężkiego    : 1, @: 0110-06-28

### Relacje Aktor - Aktor

| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Pięknotka Diakon     | 5 | ((181118-romuald-i-julia; 181216-wolna-od-terrorforma; 181225-czyszczenie-toksycznych-zwiazkow; 190724-odzyskana-agentka-orbitera; 190804-niespodziewany-wplyw-aidy)) |
| Adam Szarjan         | 2 | ((181118-romuald-i-julia; 190724-odzyskana-agentka-orbitera)) |
| Atena Sowińska       | 2 | ((181216-wolna-od-terrorforma; 181225-czyszczenie-toksycznych-zwiazkow)) |
| Bogdan Szerl         | 2 | ((181216-wolna-od-terrorforma; 181225-czyszczenie-toksycznych-zwiazkow)) |
| Mirela Niecień       | 2 | ((181216-wolna-od-terrorforma; 190724-odzyskana-agentka-orbitera)) |
| Moktar Gradon        | 2 | ((181225-czyszczenie-toksycznych-zwiazkow; 190724-odzyskana-agentka-orbitera)) |
| Romuald Czurukin     | 2 | ((181118-romuald-i-julia; 181225-czyszczenie-toksycznych-zwiazkow)) |
| Aida Serenit         | 1 | ((190724-odzyskana-agentka-orbitera)) |
| Amanda Kajrat        | 1 | ((190804-niespodziewany-wplyw-aidy)) |
| Gabriel Ursus        | 1 | ((190804-niespodziewany-wplyw-aidy)) |
| Mirela Orion         | 1 | ((190724-odzyskana-agentka-orbitera)) |
| Ossidia Saitis       | 1 | ((190804-niespodziewany-wplyw-aidy)) |
| Pietro Dwarczan      | 1 | ((181118-romuald-i-julia)) |
| Saitaer              | 1 | ((190804-niespodziewany-wplyw-aidy)) |
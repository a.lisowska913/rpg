---
categories: profile
factions: 
owner: public
title: Artur Michasiewicz
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190828-migswiatlo-psychotroniczek   | kwatermistrz Pustogorskiego Barbakanu; Pięknotka mu podpadła bo znowu rozwaliła power suit. Na szczęście skończyło się tylko na serii form i dokumentów. | 0110-02-07 - 0110-02-09 |
| 200509-rekin-z-aurum-i-fortifarma   | kwatermistrz Barbakanu w Pustogorze. Opieprzył Pięknotkę za niszczenie sprzętu bez sensu i kazał jej robić dokumenty aż się nauczy. Nie lubi jej. | 0110-08-26 - 0110-08-31 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 200509-rekin-z-aurum-i-fortifarma   | podpadła mu Pięknotka Diakon. Nie szanuje sprzętu i go uszkadza a nie musi. | 0110-08-31

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Pięknotka Diakon     | 2 | ((190828-migswiatlo-psychotroniczek; 200509-rekin-z-aurum-i-fortifarma)) |
| Artur Kołczond       | 1 | ((200509-rekin-z-aurum-i-fortifarma)) |
| Ernest Kajrat        | 1 | ((190828-migswiatlo-psychotroniczek)) |
| Erwin Galilien       | 1 | ((200509-rekin-z-aurum-i-fortifarma)) |
| Gabriel Ursus        | 1 | ((200509-rekin-z-aurum-i-fortifarma)) |
| Marek Puszczok       | 1 | ((190828-migswiatlo-psychotroniczek)) |
| Minerwa Metalia      | 1 | ((190828-migswiatlo-psychotroniczek)) |
| Natalia Tessalon     | 1 | ((200509-rekin-z-aurum-i-fortifarma)) |
| Sabina Kazitan       | 1 | ((200509-rekin-z-aurum-i-fortifarma)) |
| Tadeusz Tessalon     | 1 | ((200509-rekin-z-aurum-i-fortifarma)) |
| Talia Aegis          | 1 | ((190828-migswiatlo-psychotroniczek)) |
| Tymon Grubosz        | 1 | ((190828-migswiatlo-psychotroniczek)) |
---
categories: profile
factions: 
owner: public
title: Izabela Selentik
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220809-20-razy-za-duzo-esuriit      | Isabel; Sage; got information from the workers at watering hole about the problems plaguing Sunflower plant and later managed to extract information from manifests - although there is an amazing forger, she managed to piece stuff together and infer a Persephone-assisted tank. Ah, and she knows where to go ;-). | 0111-09-26 - 0111-10-02 |
| 220819-tank-as-a-love-letter        | Mimosa's agent; by series of discussions got to Henryk and revealed he has a girlfriend; she made everything work by good discussions and detecting what Astorian Flare is. | 0111-10-03 - 0111-10-04 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Michał Kabarniec     | 2 | ((220809-20-razy-za-duzo-esuriit; 220819-tank-as-a-love-letter)) |
| Mimoza Diakon        | 2 | ((220809-20-razy-za-duzo-esuriit; 220819-tank-as-a-love-letter)) |
| Talia Mikrit         | 2 | ((220809-20-razy-za-duzo-esuriit; 220819-tank-as-a-love-letter)) |
| Ariel Kubunczak      | 1 | ((220819-tank-as-a-love-letter)) |
| Arkadiusz Terienak   | 1 | ((220819-tank-as-a-love-letter)) |
| AU Flara Astorii     | 1 | ((220819-tank-as-a-love-letter)) |
| Daniel Terienak      | 1 | ((220819-tank-as-a-love-letter)) |
| Henryk Wkrąż         | 1 | ((220819-tank-as-a-love-letter)) |
| Iwona Perikas        | 1 | ((220819-tank-as-a-love-letter)) |
| Karol Walgoryn       | 1 | ((220809-20-razy-za-duzo-esuriit)) |
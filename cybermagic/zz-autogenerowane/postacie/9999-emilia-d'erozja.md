---
categories: profile
factions: 
owner: public
title: Emilia d'Erozja
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220723-polowanie-na-szczury-w-nativis | superzaawansowany android p.o. TAI jednostki OA Erozja Ego. Lojalna Karolowi Lertysowi, oddana docelowo Bartłomiejowi Korkoranowi. Nieobecna. | 0087-05-03 - 0087-05-12 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Celina Lertys        | 1 | ((220723-polowanie-na-szczury-w-nativis)) |
| Jan Lertys           | 1 | ((220723-polowanie-na-szczury-w-nativis)) |
| Karol Lertys         | 1 | ((220723-polowanie-na-szczury-w-nativis)) |
| Laurencjusz Kidiron  | 1 | ((220723-polowanie-na-szczury-w-nativis)) |
| OA Erozja Ego        | 1 | ((220723-polowanie-na-szczury-w-nativis)) |
| Tymon Korkoran       | 1 | ((220723-polowanie-na-szczury-w-nativis)) |
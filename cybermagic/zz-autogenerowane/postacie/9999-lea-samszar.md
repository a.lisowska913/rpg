---
categories: profile
factions: 
owner: public
title: Lea Samszar
---

# {{ page.title }}


# Generated: 



## Fiszki


* czarodziejka, źródło problemów, 24 | @ 230325-ten-nawiedzany-i-ta-ukryta
* ENCAO:  +-+-0 |Arogancja i poczucie wyższości;;Kłótliwa;;Królowa lodu;;Overextending| VALS: Face, Universalism >> Hedonism| DRIVE: Deal with pests | @ 230325-ten-nawiedzany-i-ta-ukryta
* styl: Adelicia (Rental Magica) | @ 230325-ten-nawiedzany-i-ta-ukryta
* przewaga: ogromna siła magii - Horrory (efemerydy strachu i koszmarów na podstawie sacrifice) | @ 230325-ten-nawiedzany-i-ta-ukryta

### Wątki


rekiny-a-akademia

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230325-ten-nawiedzany-i-ta-ukryta   | Rekin; kiedyś pomogła Michałowi Klabaczowi i się z nim zaprzyjaźniła ale jako że tienka nie może przyjaźnić się z człowiekiem, poświęciła go. Widząc, że magowie krzywdzą Michała, zostawiła mu plany zrobienia antymagicznej bransolety, ale gdy tchnęła weń efemerydę, Paradoks nałożył EfemeHorrora. Próbowała sprawić, by ktoś jej przyprowadził Michała do pomocy, ale Ola nie rozumiała jej hintów. W końcu Terienakowie przyprowadzili Daniela, acz Lea i Karo się pogryzły o styl komunikacji i Daniel mediował. | 0111-10-12 - 0111-10-13 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Aleksandra Burgacz   | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Daniel Terienak      | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Franek Bulterier     | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Karolina Terienak    | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Michał Klabacz       | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Nadia Uprewien       | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Oliwier Czepek       | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
| Rupert Mysiokornik   | 1 | ((230325-ten-nawiedzany-i-ta-ukryta)) |
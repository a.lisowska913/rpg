---
categories: profile
factions: 
owner: public
title: OO Astralna Flara
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221116-astralna-flara-dociera-do-nonariona-nadziei | sprawna nade wszystko, bo rozpalona wizją Egzotycznych Piękności na Nonarionie; rozpuściła większość zapasów wody by ratować Athamarein przed anomalnymi asteroidami grawitacyjnymi. | 0100-07-11 - 0100-07-14 |
| 221123-egzotyczna-pieknosc-na-astralnej-flarze | posłużyła do ratowania Hadiah Emas; Daria tymczasowo przebudowała Flarę z zewnętrz, by móc naprawić Emas i nikomu by się nie stała krzywda. | 0100-07-15 - 0100-07-18 |
| 221130-astralna-flara-w-strefie-duchow | jako jednostka wsparcia skutecznie fabrykowała drony do badania Strefy Duchów a potem klatki na Kireę (savarankę) | 0100-08-07 - 0100-08-10 |
| 221214-astralna-flara-kontra-domina-lucis | konstrukcja serii sond i skanerów, by zrozumieć niespodzianki Strefy Duchów. Konstrukcja containment chambers do złapania noktian. Konstrukcja transmiterów itp. Ogólnie, potężna jednostka konstrukcyjna pełnej mocy by nie dało się ze Strefy Duchów uciec ani jednemu noktianinowi. | 0100-08-11 - 0100-08-13 |
| 221221-astralna-flara-i-nowy-komodor | zasiliła energią bazę Orbitera na Kazmirian; zaczęła się integrować z planami Dominy Lucis. | 0100-09-09 - 0100-09-12 |
| 230111-gdy-hr-reedukuje-niewlasciwa-osobe | manewruje z Athamarein, by Nox Ignis nie była w stanie dogonić żadnej z jednostek. Uczestniczyła w operacji 'wypalenia pilotki Nox Ignis'. | 0100-09-15 - 0100-09-18 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 221102-astralna-flara-i-porwanie-na-karsztarinie | korzystając z uprawnień kapitańskich Arianna ściągnęła wyjątkowo dużo dobrych substratów jedzenia. Dla Mai, która będzie karmić oficerów :D. | 0100-06-05
| 221102-astralna-flara-i-porwanie-na-karsztarinie | wymaga 2 tygodni naprawy w dokach; uszkodzona po ostrych manewrach rotacyjnych Arianny. | 0100-06-05
| 221116-astralna-flara-dociera-do-nonariona-nadziei | dostaje ekstra 5 marines (z woli kmdr Lodowca); | 0100-07-14
| 221123-egzotyczna-pieknosc-na-astralnej-flarze | spory koszt surowcowy podczas naprawy Hadiah Emas, zwłaszcza w obszarze rzeczy związanych z life support i podstawowe metale. | 0100-07-18
| 221130-astralna-flara-w-strefie-duchow | straciła trochę materiałów na próbę odbudowy Zarralei i pracy nad Isigtand a potem 'klatka' na Kireę dla Lodowca; ma jakieś 85% pełnego capacity. | 0100-08-10

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 6 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Daria Czarnewik      | 6 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| OO Athamarein        | 6 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Kajetan Kircznik     | 5 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221130-astralna-flara-w-strefie-duchow; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Leszek Kurzmin       | 5 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221214-astralna-flara-kontra-domina-lucis; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Ellarina Samarintael | 4 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze; 221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis; 221221-astralna-flara-i-nowy-komodor)) |
| Gabriel Lodowiec     | 4 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis)) |
| Maja Samszar         | 4 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221130-astralna-flara-w-strefie-duchow; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Władawiec Diakon     | 4 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Arnulf Perikas       | 3 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221130-astralna-flara-w-strefie-duchow)) |
| Elena Verlen         | 3 | ((221130-astralna-flara-w-strefie-duchow; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| NekroTAI Zarralea    | 3 | ((221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis; 221221-astralna-flara-i-nowy-komodor)) |
| Grażyna Burgacz      | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Kirea Rialirat       | 2 | ((221130-astralna-flara-w-strefie-duchow; 221214-astralna-flara-kontra-domina-lucis)) |
| Leo Kasztop          | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221130-astralna-flara-w-strefie-duchow)) |
| Marcel Kulgard       | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| OO Loricatus         | 2 | ((221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Salazar Bolza        | 2 | ((221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| SCA Hadiah Emas      | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Szczepan Myrczek     | 2 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Szymon Wanad         | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221130-astralna-flara-w-strefie-duchow)) |
| Adragain Ferrias     | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| AK Nox Ignis         | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Alan Nierkamin       | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Aleksy Sartaran      | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Alezja Dumorin       | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Axel Nargan          | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |
| Erwin Pies           | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Frank Mgrot          | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Gerwazy Kircznik     | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Hind Szug Traf       | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Hubert Kerwelenios   | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| Kaspian Certisarius  | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| Klarysa Jirnik       | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| Lana Mirkinin        | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Miłosz Klinek        | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Persefona d'Loricatus | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Sabrina Ferrias      | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Sargon Niiris        | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| Sarian Xadaar        | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |
| SCA Isigtand         | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Tomasz Dojnicz       | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| Tomasz Ruppok        | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Tristan Rialirat     | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |
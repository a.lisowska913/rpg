---
categories: profile
factions: 
owner: public
title: Rafał Roszczeniok
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 191103-kontrpolowanie-pieknotki-pulapka | terminus-cień Minerwy. Przez Pięknotkę przekonany, że jeśli pozwoli Minerwie na swobodę to może dorwać Grzymościa. Roszczeniok jest powiązany z Fortem Mikado i chce więcej niż niańczyć. | 0110-06-20 - 0110-06-27 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Damian Orion         | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Józef Małmałaz       | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Ksenia Kirallen      | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Lilia Ursus          | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Marek Puszczok       | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Mariusz Trzewń       | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Minerwa Metalia      | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Olaf Zuchwały        | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Pięknotka Diakon     | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
---
categories: profile
factions: 
owner: public
title: Apollo Verlen
---

# {{ page.title }}


# Generated: 



## Fiszki


* mag, sybrianin: "żyje się raz, piękne panny!" (UWIELBIANY przez żołnierzy i ludzi) | @ 230124-kjs-wygrac-za-wszelka-cene
* (ENCAO:  ++-00 |Mało punktualny.;;Beztroski i swobodny| VALS: Face, Hedonism >> Security | DRIVE: Uwielbienie Tłumów) | @ 230124-kjs-wygrac-za-wszelka-cene
* core wound: never good enough. Nie jest tak dobry jak mógłby być. Niezaakceptowany przez swoje słabości. Nie dość dobry jako Verlen. | @ 230124-kjs-wygrac-za-wszelka-cene
* core lie: jeśli nie zrobisz wszystkiego pókiś młody, nigdy niczego nie zrobisz! Czas płynie i tracisz życie! | @ 230124-kjs-wygrac-za-wszelka-cene
* styl: głośny, płomienny, wszechobecny - i Johnny Bravo ;-) | @ 230124-kjs-wygrac-za-wszelka-cene
* "żyjemy raz, więc żyjmy pełnią życia, razem!!!" - dArtagnan "we're gonna be drinking" | @ 230124-kjs-wygrac-za-wszelka-cene
* SPEC: dowodzący oficer, morale, walka bezpośrednia | @ 230124-kjs-wygrac-za-wszelka-cene

### Wątki


kajis
mroczna-strona-verlenow

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210224-sentiobsesja                 | 24 lata, zawsze blisko żołnierskiej braci. BWR. Trzy kochanki które o sobie nie wiedzą. Poprosił, by inni sentitienowie opuścili ten teren, co Mścigrom zrobił; potem Apollo wpadł w sentiobsesję przez Milenę Blakenbauer. Uratowany przez Ariannę i Viorikę. | 0092-08-03 - 0092-08-06 |
| 210306-wiktoriata                   | używając umiejętności społecznych i nieuczciwej przewagi zmiażdżył Viorikę w pojedynku oddziałów. Ona była lepsza taktycznie, jego żołnierze jego uwielbiali. Plus, wkręcił Viorikę. | 0093-10-21 - 0093-10-30 |
| 230124-kjs-wygrac-za-wszelka-cene   | młody kobieciarz (28). Jak tylko pojawił się na terenie Koszarowa, zaczął korzystać ze swojego statusu i umiejętności i dobrze planować czas. Acz przy okazji FAKTYCZNIE podnosi umiejętności Emilii specjalnymi treningami. | 0095-06-23 - 0095-06-25 |
| 230307-kjs-stymulanty-szeptomandry  | tien Verlen, który okazał się być całkiem normalny i dość rozumiał co jest grane. Nie chciał zrobić nikomu krzywdy i ogólnie dobrze przyjął Zespół. Zniszczył Szeptomandrę i ustabilizował teren. Zapłaci Kajis za zrobienie dowcipu komuś innemu. Nie pytał o "turystów", bo niekoniecznie chciał wiedzieć, acz swoje podejrzewał. | 0095-06-26 - 0095-06-29 |
| 230328-niepotrzebny-ratunek-mai     | przekonany przez Romeo, poszedł na zrobienie dowcipu Samszarom. Okup za Maję? "1001 gęsich piór o odpowiednim odcieniu". Oczywiście, "bo Karolinus". | 0095-06-30 - 0095-07-02 |
| 230412-dzwiedzie-poluja-na-ser      | jest święcie przekonany, że Blakenbauerowie chcieli skrzywdzić Elenę; wziął na siebie ogień reputacyjny by chronić Elenę. To spowodowało konflikt z Marcinozaurem (który to konflikt Marcinozaur wygrywa dzięki Uli). Ściągnął Ariannę i Viorikę do rozwiązania sprawy. Główny dyplomata Verlenów XD. | 0095-07-12 - 0095-07-14 |
| 230419-karolinka-nieokielznana-swinka | w tle współpracował z Vioriką i Arianną, by ukryć sytuację z Eleną. Wziął na siebie sprawę z podłożeniem świni i ze wtopieniem Tymka Samszara w menhir. Jako 'mszczenie się za Viorikę'. | 0095-07-19 - 0095-07-23 |
| 210311-studenci-u-verlenow          | wykazał się kompetencją opanowując zniszczony Poniewierz po erupcji energii Eleny. Większość czasu jednak spędzał z Sylwią Perikas ;-). Na końcu wybrał rodzinę nad Sylwię - ona zdradziła jego zaufanie. Plus, mały research odnośnie lustra i Blakenbauera. | 0096-11-18 - 0096-11-24 |
| 210210-milosc-w-rodzie-verlen       | chce wyjść za Milenę Blakenbauer (ze wzajemnością). | 0111-06-23 - 0111-06-26 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 210224-sentiobsesja                 | dostał opiernicz od sierżanta za sentiobsesję. Dostał wpiernicz od kochanek. Śpiewał i tańczył w stroju maid-chan dla Mileny Blakenbauer. Bardzo, bardzo zły okres. | 0092-08-06

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Viorika Verlen       | 7 | ((210210-milosc-w-rodzie-verlen; 210224-sentiobsesja; 210306-wiktoriata; 210311-studenci-u-verlenow; 230328-niepotrzebny-ratunek-mai; 230412-dzwiedzie-poluja-na-ser; 230419-karolinka-nieokielznana-swinka)) |
| Arianna Verlen       | 5 | ((210210-milosc-w-rodzie-verlen; 210224-sentiobsesja; 210311-studenci-u-verlenow; 230412-dzwiedzie-poluja-na-ser; 230419-karolinka-nieokielznana-swinka)) |
| Elena Verlen         | 5 | ((210210-milosc-w-rodzie-verlen; 210311-studenci-u-verlenow; 230124-kjs-wygrac-za-wszelka-cene; 230412-dzwiedzie-poluja-na-ser; 230419-karolinka-nieokielznana-swinka)) |
| KDN Kajis            | 2 | ((230124-kjs-wygrac-za-wszelka-cene; 230307-kjs-stymulanty-szeptomandry)) |
| Lidia Nemert         | 2 | ((230124-kjs-wygrac-za-wszelka-cene; 230307-kjs-stymulanty-szeptomandry)) |
| Lucas Oktromin       | 2 | ((230124-kjs-wygrac-za-wszelka-cene; 230307-kjs-stymulanty-szeptomandry)) |
| Lucjusz Blakenbauer  | 2 | ((210224-sentiobsesja; 210306-wiktoriata)) |
| Maja Samszar         | 2 | ((210311-studenci-u-verlenow; 230328-niepotrzebny-ratunek-mai)) |
| Marcinozaur Verlen   | 2 | ((230412-dzwiedzie-poluja-na-ser; 230419-karolinka-nieokielznana-swinka)) |
| Marta Krissit        | 2 | ((230124-kjs-wygrac-za-wszelka-cene; 230307-kjs-stymulanty-szeptomandry)) |
| Przemysław Czapurt   | 2 | ((210224-sentiobsesja; 210306-wiktoriata)) |
| Romeo Verlen         | 2 | ((210210-milosc-w-rodzie-verlen; 230328-niepotrzebny-ratunek-mai)) |
| Ula Blakenbauer      | 2 | ((230412-dzwiedzie-poluja-na-ser; 230419-karolinka-nieokielznana-swinka)) |
| Żaneta Krawędź       | 2 | ((230124-kjs-wygrac-za-wszelka-cene; 230307-kjs-stymulanty-szeptomandry)) |
| AJA Szybka Strzała   | 1 | ((230328-niepotrzebny-ratunek-mai)) |
| Aleksander Samszar   | 1 | ((230419-karolinka-nieokielznana-swinka)) |
| Bonifacy Samszar     | 1 | ((230328-niepotrzebny-ratunek-mai)) |
| Brunhilda Verlen     | 1 | ((210210-milosc-w-rodzie-verlen)) |
| Dariusz Blakenbauer  | 1 | ((210311-studenci-u-verlenow)) |
| Emilia Lawendowiec   | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Fantazjusz Verlen    | 1 | ((230419-karolinka-nieokielznana-swinka)) |
| Fiona Szarstasz      | 1 | ((230328-niepotrzebny-ratunek-mai)) |
| Franciszek Korel     | 1 | ((230307-kjs-stymulanty-szeptomandry)) |
| Franz Verlen         | 1 | ((210210-milosc-w-rodzie-verlen)) |
| Karol Atenuatia      | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Karolinus Samszar    | 1 | ((230328-niepotrzebny-ratunek-mai)) |
| Krucjusz Verlen      | 1 | ((210210-milosc-w-rodzie-verlen)) |
| Michał Perikas       | 1 | ((210311-studenci-u-verlenow)) |
| Michał Waczarek      | 1 | ((230307-kjs-stymulanty-szeptomandry)) |
| Milena Blakenbauer   | 1 | ((210224-sentiobsesja)) |
| Mścigrom Verlen      | 1 | ((210224-sentiobsesja)) |
| Rafał Perikas        | 1 | ((210311-studenci-u-verlenow)) |
| Rufus Samszar        | 1 | ((210311-studenci-u-verlenow)) |
| Seraf Verlen         | 1 | ((210210-milosc-w-rodzie-verlen)) |
| Serena Krissit       | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Sylwia Perikas       | 1 | ((210311-studenci-u-verlenow)) |
| Tymek Samszar        | 1 | ((230419-karolinka-nieokielznana-swinka)) |
| Wiktor Blakenbauer   | 1 | ((210306-wiktoriata)) |
| Wojciech Namczak     | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Zuzanna Kraczamin    | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
---
categories: profile
factions: 
owner: public
title: Ola Klamka
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210904-broszka-dla-eternianki       | przyjaciółka Martyna; nie dostała wymarzonej roli. Pomogła Martynowi wybrać maksymalnie skuteczną biżuterię dla Kaliny by zrobić z niej baronetkę mody. Niestety, ma złą reputację jako aktorka i mało talentu. | 0075-09-10 - 0075-09-21 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Dagmara Kamyk        | 1 | ((210904-broszka-dla-eternianki)) |
| Jolanta Kopiec       | 1 | ((210904-broszka-dla-eternianki)) |
| Kalina Rota d'Kopiec | 1 | ((210904-broszka-dla-eternianki)) |
| Martyn Hiwasser      | 1 | ((210904-broszka-dla-eternianki)) |
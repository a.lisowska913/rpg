---
categories: profile
factions: 
owner: public
title: Adalbert Brześniak
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210209-wolna-tai-na-k1              | polityk który próbował włączyć TAI do struktur Orbitera jako obywateli. Chciał TAI Neitę jako pracownika. Odkrył Rziezę w jej Atrium i skończył jako pracownik Rziezy. Dafuq. | 0111-07-02 - 0111-07-07 |
| 211114-admiral-gruby-wieprz         | współpracuje z Sabiną Kazitan (?!) w celu zbudowania bezpiecznego miejsca dla TAI w Aurum. Pomaga Klaudii w ewakuacji virtsystemu do Aurum. Ma sporo wspólnego z siłami ratującymi AI z K1. | 0111-12-11 - 0111-12-16 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Klaudia Stryk        | 2 | ((210209-wolna-tai-na-k1; 211114-admiral-gruby-wieprz)) |
| Andrea Burgacz       | 1 | ((210209-wolna-tai-na-k1)) |
| Izabela Zarantel     | 1 | ((211114-admiral-gruby-wieprz)) |
| Janusz Parzydeł      | 1 | ((210209-wolna-tai-na-k1)) |
| Leona Astrienko      | 1 | ((210209-wolna-tai-na-k1)) |
| Martyn Hiwasser      | 1 | ((210209-wolna-tai-na-k1)) |
| Miki Katasair        | 1 | ((210209-wolna-tai-na-k1)) |
| Rafael Galwarn       | 1 | ((211114-admiral-gruby-wieprz)) |
| Roman Panracz        | 1 | ((211114-admiral-gruby-wieprz)) |
| Rzieza d'K1          | 1 | ((211114-admiral-gruby-wieprz)) |
| Sabina Kazitan       | 1 | ((211114-admiral-gruby-wieprz)) |
| TAI Neita Lairossa   | 1 | ((210209-wolna-tai-na-k1)) |
| TAI Rzieza d'K1      | 1 | ((210209-wolna-tai-na-k1)) |
---
categories: profile
factions: 
owner: public
title: Rafał Armadion
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200916-smierc-raju                  | noktianin zajmujący się glukszwajnami w Trzecim Raju; na polecenie przekazania świń na pokład Tucznika do redukcji Skażenia zrobił to z przyjemnością. Poczciwy. | 0110-12-21 - 0110-12-23 |
| 201104-sabotaz-swini                | jako Rafał maskował się sabotujący miragent, co oznacza, że Rafał nie żyje od pewnego czasu. KIA. | 0111-01-13 - 0111-01-16 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Anastazja Sowińska   | 2 | ((200916-smierc-raju; 201104-sabotaz-swini)) |
| Arianna Verlen       | 2 | ((200916-smierc-raju; 201104-sabotaz-swini)) |
| Ataienne             | 2 | ((200916-smierc-raju; 201104-sabotaz-swini)) |
| Eustachy Korkoran    | 2 | ((200916-smierc-raju; 201104-sabotaz-swini)) |
| Klaudia Stryk        | 2 | ((200916-smierc-raju; 201104-sabotaz-swini)) |
| Robert Garwen        | 2 | ((200916-smierc-raju; 201104-sabotaz-swini)) |
| Celina Szilat        | 1 | ((200916-smierc-raju)) |
| Diana Arłacz         | 1 | ((201104-sabotaz-swini)) |
| Elena Verlen         | 1 | ((200916-smierc-raju)) |
| Izabela Zarantel     | 1 | ((200916-smierc-raju)) |
| Marian Fartel        | 1 | ((200916-smierc-raju)) |
| Martyn Hiwasser      | 1 | ((201104-sabotaz-swini)) |
| Wanessa Pyszcz       | 1 | ((200916-smierc-raju)) |
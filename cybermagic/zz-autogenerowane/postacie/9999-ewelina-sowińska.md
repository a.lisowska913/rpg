---
categories: profile
factions: 
owner: public
title: Ewelina Sowińska
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220611-nie-roland-i-niewolnicy-na-valentinie | mało ważna kilkunasta córka, trafiła na ambasadorkę Aurum na Valentinie. Źle jej było ale coś osiągnęła - ustawiła ród Sowińskich na Valentinie. Weszła w handel niewolnikami bo handler ze strony Sowińskich chciał za dużo. Wezwała Rolanda do pomocy z tajemniczą krzywdą jej ludzi. Zeszła z niewolnictwa na dom publiczny i postawiła się handlerowi. | 0111-03-10 - 0111-03-12 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 220611-nie-roland-i-niewolnicy-na-valentinie | czuje ulgę wobec Rolanda Sowińskiego, że nie przybył osobiście i że jej wysłał szemranego agenta (spoiler: to był on w przebraniu). BOI SIĘ ROLANDA STRASZNIE. | 0111-03-12

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Roland Sowiński      | 1 | ((220611-nie-roland-i-niewolnicy-na-valentinie)) |
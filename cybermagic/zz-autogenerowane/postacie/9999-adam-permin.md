---
categories: profile
factions: 
owner: public
title: Adam Permin
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210330-pletwalowcy-na-goldarionie   | oficer bezpieczeństwa Goldariona; opieprzył poważnie Aleksandra ostrzegając go, że budujący się resentyment Płetwal - Goldarion zagraża Goldarionowi i reputacji Leszerta. | 0109-01-04 - 0109-01-19 |
| 210108-ratunkowa-misja-goldariona   | oficer porządkowy Goldariona. mtg_B. Nieetyczny jak cholera, niechlujny - ale nie zgadza się na twarde narkotyki, jak długo dostanie inną formę zapłaty. Dba o statek po swojemu. | 0111-06-11 - 0111-06-17 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 210330-pletwalowcy-na-goldarionie   | w relacji z Aleksandrem Leszertem (swoim kapitanem) | 0109-01-19

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Aleksander Leszert   | 2 | ((210108-ratunkowa-misja-goldariona; 210330-pletwalowcy-na-goldarionie)) |
| Elena Verlen         | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Feliks Przędz        | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Janusz Krumlod       | 1 | ((210330-pletwalowcy-na-goldarionie)) |
| Jonatan Piegacz      | 1 | ((210330-pletwalowcy-na-goldarionie)) |
| Kamil Frederico      | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Karolina Kartusz     | 1 | ((210330-pletwalowcy-na-goldarionie)) |
| Klaudia Stryk        | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Martyn Hiwasser      | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Martyna Bianistek    | 1 | ((210330-pletwalowcy-na-goldarionie)) |
| Oliwia Pietrova      | 1 | ((210108-ratunkowa-misja-goldariona)) |
| SC Goldarion         | 1 | ((210330-pletwalowcy-na-goldarionie)) |
| SCA Goldarion        | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Semla d'Goldarion    | 1 | ((210108-ratunkowa-misja-goldariona)) |
| SL Uśmiechnięta      | 1 | ((210108-ratunkowa-misja-goldariona)) |
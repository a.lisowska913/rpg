---
categories: profile
factions: 
owner: public
title: Kalina Rota d'Kopiec
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210904-broszka-dla-eternianki       | tajemnicza femme fatale dla Martyna; dzięki działaniom Martyna uzyskała broszkę dla swojej pani. Nie chce wolności od Jolanty Kopiec. Przez moment bardzo popularna jako egzotyczna eternijka dzięki Martynowi. | 0075-09-10 - 0075-09-21 |
| 210918-pierwsza-bitwa-martyna       | uczestniczyła w akcji tien Kopiec - atak na dziwną jednostkę noktiańską i została odcięta, przysypana i ranna. Martyn przyszedł jej na pomoc. Jolanta spytała ją czy chce iść z Martynem - Kalina powiedziała TAK. Więc jest wolna (acz w arkin Jolanty). | 0080-11-23 - 0080-11-28 |
| 211016-zlamane-serce-martyna        | KIA. Ukochana żona Martyna, w pierwszych etapach ciąży, zastrzelona przez Adelę. | 0085-08-31 - 0085-09-06 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 210904-broszka-dla-eternianki       | wyszła z niełaski Jolanty Kopiec; zostaje dalej na tym terenie. Rozpoznawana jako egzotyczna eternijka. | 0075-09-21
| 210918-pierwsza-bitwa-martyna       | nie jest odpięta z sieci Jolanty, ale jest wolna. Partnerka Martyna Hiwassera. | 0080-11-28

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Jolanta Kopiec       | 3 | ((210904-broszka-dla-eternianki; 210918-pierwsza-bitwa-martyna; 211016-zlamane-serce-martyna)) |
| Martyn Hiwasser      | 3 | ((210904-broszka-dla-eternianki; 210918-pierwsza-bitwa-martyna; 211016-zlamane-serce-martyna)) |
| Adela Kołczan        | 1 | ((211016-zlamane-serce-martyna)) |
| Adrian Kozioł        | 1 | ((211016-zlamane-serce-martyna)) |
| Amadeusz Martel d'Kopiec | 1 | ((211016-zlamane-serce-martyna)) |
| Dagmara Kamyk        | 1 | ((210904-broszka-dla-eternianki)) |
| Jonasz Staran        | 1 | ((210918-pierwsza-bitwa-martyna)) |
| Ola Klamka           | 1 | ((210904-broszka-dla-eternianki)) |
| OO Invictus          | 1 | ((210918-pierwsza-bitwa-martyna)) |
| Pandemiusz Diakon    | 1 | ((210918-pierwsza-bitwa-martyna)) |
| Szczepan Kutarcjusz  | 1 | ((210918-pierwsza-bitwa-martyna)) |
| Wanessa Ogarek       | 1 | ((211016-zlamane-serce-martyna)) |
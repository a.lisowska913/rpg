---
categories: profile
factions: 
owner: public
title: Brygida Maczkowik
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 180906-nikt-nie-spi-w-swoim-lozku   | dama do towarzystwa w Przywiesławiu; chciała stać się członkiem grupy tanecznej ale została odrzucona. W konsekwencji duch jej ojca powrócił i zabijał. | 0109-09-09 - 0109-09-11 |
| 180912-inkarnata-nienawisci         | dama do towarzystwa w Przywiesławiu; zintegrowana z Inkarnatą Nienawiści, atakuje i odbiera ciepłe uczucia ludziom. Skończyła jako adiutantka Ateny. | 0109-09-14 - 0109-09-15 |
| 181027-terminuska-czy-kosmetyczka   | dążąc do coraz większej siły, wplątała się w polityczny atak na jej opiekunkę - Atenę. Współpracowała z Ignacym Myrczkiem. | 0109-10-07 - 0109-10-11 |
| 181227-adieu-cieniaszczycie         | tak strasznie się bała zostać sama w Cieniaszczycie że wybłagała u Pięknotki wstawiennictwo - terminuska załatwiła jej opiekę Pietro Dwarczana. | 0109-11-27 - 0109-11-30 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 180912-inkarnata-nienawisci         | zintegrowana z Inkarnatą (viciniusem). Stała się viciniusem, nie ma powrotu w 100% do świata ludzi. | 0109-09-15
| 181027-terminuska-czy-kosmetyczka   | zaczyna budować relację z Ateną jako adiutantka. Jeszcze walczy, ale Atena cierpliwie przekonuje Brygidę do siebie. | 0109-10-11
| 181227-adieu-cieniaszczycie         | skończyła jako wolna viciniuska żyjąca w Cieniaszczycie, pod opieką Pietra Dwarczana. Jej wątek jest (chyba) zakończony. | 0109-11-30

## Plany


| Opowieść | Plan | Końcowa data |
| ---- | ---- | ---- |
| 181027-terminuska-czy-kosmetyczka   | musi stać się groźniejsza, silniejsza i lepiej przystosowana jako vicinius w świecie magów; ale nie ma w niej zła, tylko pragnienie bezpieczeństwa | 0109-10-11

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Pięknotka Diakon     | 4 | ((180906-nikt-nie-spi-w-swoim-lozku; 180912-inkarnata-nienawisci; 181027-terminuska-czy-kosmetyczka; 181227-adieu-cieniaszczycie)) |
| Atena Sowińska       | 3 | ((180906-nikt-nie-spi-w-swoim-lozku; 180912-inkarnata-nienawisci; 181227-adieu-cieniaszczycie)) |
| Adela Kirys          | 1 | ((181027-terminuska-czy-kosmetyczka)) |
| Amadeusz Sowiński    | 1 | ((181227-adieu-cieniaszczycie)) |
| Arnulf Poważny       | 1 | ((181027-terminuska-czy-kosmetyczka)) |
| Erwin Galilien       | 1 | ((181227-adieu-cieniaszczycie)) |
| Grażyna Sirwąg       | 1 | ((180906-nikt-nie-spi-w-swoim-lozku)) |
| Ignacy Myrczek       | 1 | ((181027-terminuska-czy-kosmetyczka)) |
| Lilia Ursus          | 1 | ((181227-adieu-cieniaszczycie)) |
| Mieszko Weiner       | 1 | ((180906-nikt-nie-spi-w-swoim-lozku)) |
| Minerwa Metalia      | 1 | ((181027-terminuska-czy-kosmetyczka)) |
| Mirela Niecień       | 1 | ((181227-adieu-cieniaszczycie)) |
| Moktar Gradon        | 1 | ((181227-adieu-cieniaszczycie)) |
| Pietro Dwarczan      | 1 | ((181227-adieu-cieniaszczycie)) |
| Staś Kruszawiecki    | 1 | ((180912-inkarnata-nienawisci)) |
| Szczepan Mensic      | 1 | ((180906-nikt-nie-spi-w-swoim-lozku)) |
| Tadeusz Kruszawiecki | 1 | ((180906-nikt-nie-spi-w-swoim-lozku)) |
| Waleria Cyklon       | 1 | ((181227-adieu-cieniaszczycie)) |
| Zbigniew Burzycki    | 1 | ((181227-adieu-cieniaszczycie)) |
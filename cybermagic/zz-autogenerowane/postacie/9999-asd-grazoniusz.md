---
categories: profile
factions: 
owner: public
title: ASD Grazoniusz
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190123-skazenie-grazoniusza         | wydobywczo-badawczy statek, który miał nieszczęście natknąć się na Saitaera. Stał się statkiem flagowym sił Saitaera. | 0079-03-30 - 0079-04-01 |
| 190123-parszywa-ekspedycja          | pełnoprawny statek widmo, upiornie creepy. Ma kultystów, terrorformy i obłąkane AI. Na Grazoniusza wpakowała się ekspecja z Astorii i zdobyli TAI Persefonę - a raczej, TAI Alicję. | 0109-12-20 - 0109-12-22 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 190123-skazenie-grazoniusza         | stał się statkiem flagowym Saitaera. Jest praktycznie niezniszczalny (regeneruje się) i potrafi przemieszczać się w dziwny sposób. | 0079-04-01
| 190123-parszywa-ekspedycja          | uzyskał elitarnego terrorforma, stworzonego z agenta ekspedycji zwanego "Johnem". | 0109-12-22

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Alicja Sowińska      | 2 | ((190123-parszywa-ekspedycja; 190123-skazenie-grazoniusza)) |
| ASD Bubuta           | 1 | ((190123-parszywa-ekspedycja)) |
---
categories: profile
factions: 
owner: public
title: Eliza Farnorz
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190530-porwana-foodfluencerka       | ludzka przyjaciółka Alicji, początkująca aktorka. Niezbyt rozsądna; zdradziła Alicję jej najlojalniejszym fanom, przez co Alicja została porwana. Zaaranżowała swoje porwanie. | 0110-04-01 - 0110-04-02 |
| 190619-esuriit-w-sercu-alicji       | chciała rozpaczliwie uratować relację z Alicją. Niemożliwe. Za to energia Esuriit Alicji prawie ją zabiła powołując skrzydłochłepta. Zespół ją uratował. | 0110-04-12 - 0110-04-14 |
| 200226-strazniczka-przez-lzy        | przyjaciółka Alicji została donorem części nowej tkanki Alicji jako cyber-strażniczka. Silnie straumatyzowana. | 0110-07-25 - 0110-07-28 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Alicja Kiermacz      | 3 | ((190530-porwana-foodfluencerka; 190619-esuriit-w-sercu-alicji; 200226-strazniczka-przez-lzy)) |
| Rafał Muczor         | 2 | ((190619-esuriit-w-sercu-alicji; 200226-strazniczka-przez-lzy)) |
| Alina Anakonda       | 1 | ((200226-strazniczka-przez-lzy)) |
| Crystal d'Corieris   | 1 | ((200226-strazniczka-przez-lzy)) |
| Darek Ampieczak      | 1 | ((200226-strazniczka-przez-lzy)) |
| Jacek Brzytwa        | 1 | ((190530-porwana-foodfluencerka)) |
| Kastor Miczokan      | 1 | ((200226-strazniczka-przez-lzy)) |
| Lucjusz Blakenbauer  | 1 | ((200226-strazniczka-przez-lzy)) |
| Małgorzata Wisus     | 1 | ((190530-porwana-foodfluencerka)) |
| Tymon Grubosz        | 1 | ((190619-esuriit-w-sercu-alicji)) |
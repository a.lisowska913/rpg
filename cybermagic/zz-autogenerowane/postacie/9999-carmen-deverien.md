---
categories: profile
factions: 
owner: public
title: Carmen Deverien
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230303-the-goose-from-hell          | Used a spell to hide the rat in the bar to avoid having problems and found the goose's location by surfing through city-wide groups on hypernet. | 0111-10-24 - 0111-10-26 |
| 230331-an-unfortunate-ratnapping    | heavily uses telekinetic abilities; she helps locate the rat nest with social media, transports the captured rats via telekinesis, and accidentally replaces the defeated Guardian's soul with a random rat's one. | 0111-11-04 - 0111-11-06 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Alex Deverien        | 2 | ((230303-the-goose-from-hell; 230331-an-unfortunate-ratnapping)) |
| Julia Kardolin       | 2 | ((230303-the-goose-from-hell; 230331-an-unfortunate-ratnapping)) |
| kot-pacyfikator Tobias | 2 | ((230303-the-goose-from-hell; 230331-an-unfortunate-ratnapping)) |
| Paweł Szprotka       | 2 | ((230303-the-goose-from-hell; 230331-an-unfortunate-ratnapping)) |
| Alicja Trawlis       | 1 | ((230303-the-goose-from-hell)) |
| Radosław Turkamenin  | 1 | ((230331-an-unfortunate-ratnapping)) |
| Teresa Mieralit      | 1 | ((230303-the-goose-from-hell)) |
---
categories: profile
factions: 
owner: public
title: Sasza Morwowiec
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 211010-ukryta-wychowanka-arnulfa    | terminus; od dawna poluje na Tymona i uważa go za szkodnika. Gdy skonfrontował się z Tymonem w AMZ, Arnulf powiedział że to jego sprawa i Saszę wygnał - robiąc sobie z Saszy wroga. | 0084-12-11 - 0084-12-12 |
| 211017-nastolatka-w-bieliznie-na-dachu-w-burzy | terminus chcący wykazać zdradę Arnulfa i Tymona; przeciwny samodzielności AMZ, ma grudge do Tymona i wierzy, że Tymon współpracuje z Grzymościem. Anulował misję, by ratować lunatyczkę w AMZ (to była Teresa i jej nie dorwał). Powiedział Klaudii, że szuka linka Tymon - Grzymość. Wbrew pozorom, całkiem spoko koleś. Nie nienawidzi noktian. Ale żąda porządku. | 0084-12-14 - 0084-12-15 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 211010-ukryta-wychowanka-arnulfa    | święcie przekonany, że Tymon Grubosz i Arnulf Poważny współpracują nad czymś przeciwko Pustogorowi. Nie podejrzewa Talii (bo to noktianka). | 0084-12-12

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arnulf Poważny       | 2 | ((211010-ukryta-wychowanka-arnulfa; 211017-nastolatka-w-bieliznie-na-dachu-w-burzy)) |
| Klaudia Stryk        | 2 | ((211010-ukryta-wychowanka-arnulfa; 211017-nastolatka-w-bieliznie-na-dachu-w-burzy)) |
| Ksenia Kirallen      | 2 | ((211010-ukryta-wychowanka-arnulfa; 211017-nastolatka-w-bieliznie-na-dachu-w-burzy)) |
| Teresa Mieralit      | 2 | ((211010-ukryta-wychowanka-arnulfa; 211017-nastolatka-w-bieliznie-na-dachu-w-burzy)) |
| Felicjan Szarak      | 1 | ((211010-ukryta-wychowanka-arnulfa)) |
| Strażniczka Alair    | 1 | ((211010-ukryta-wychowanka-arnulfa)) |
| Talia Aegis          | 1 | ((211010-ukryta-wychowanka-arnulfa)) |
| Tymon Grubosz        | 1 | ((211010-ukryta-wychowanka-arnulfa)) |
| Waldemar Grzymość    | 1 | ((211017-nastolatka-w-bieliznie-na-dachu-w-burzy)) |
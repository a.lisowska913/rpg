---
categories: profile
factions: 
owner: public
title: Romana Arnatin
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210721-pierwsza-bia-mag             | noktiański lekarz na Inferni, wsparcie Martyna. Wraz z Solitarią pomogła Martynowi używać noktiańskiej technologii w obawie, że ten skrzywdzi Klaudię. | 0111-03-19 - 0111-03-21 |
| 210728-w-cieniu-nocnej-krypty       | zdecydowała się zostać w Zonie Tres i opuściła załogę Inferni wraz z kilkunastoma noktianami. | 0111-03-22 - 0111-04-08 |
| 210728-w-cieniu-nocnej-krypty       | zdecydowała się zostać w Zonie Tres, opuściła Infernię z kilkunastoma noktianami. | 0111-03-22 - 0111-04-08 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 2 | ((210721-pierwsza-bia-mag; 210728-w-cieniu-nocnej-krypty)) |
| Eustachy Korkoran    | 2 | ((210721-pierwsza-bia-mag; 210728-w-cieniu-nocnej-krypty)) |
| Janus Krzak          | 2 | ((210721-pierwsza-bia-mag; 210728-w-cieniu-nocnej-krypty)) |
| AK Nocna Krypta      | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| Atrius Kurunen       | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| BIA Solitaria d'Zona Tres | 1 | ((210721-pierwsza-bia-mag)) |
| Finis Vitae          | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| Gerard Adanor        | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| Helena Adanor        | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| Klaudia Stryk        | 1 | ((210721-pierwsza-bia-mag)) |
| Martyn Hiwasser      | 1 | ((210721-pierwsza-bia-mag)) |
| Oliwia Karelan       | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| Ulisses Kalidon      | 1 | ((210728-w-cieniu-nocnej-krypty)) |
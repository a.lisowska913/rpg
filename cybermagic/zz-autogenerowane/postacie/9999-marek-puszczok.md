---
categories: profile
factions: 
owner: public
title: Marek Puszczok
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190827-rozpaczliwe-ratowanie-bii    | mag Grzymościa; twinowany z BIA. Zaatakował dla BIA Tiamenat i ukradł rzeczy potrzebne dla BIA do przetrwania. Też schował BIA w miejscu kontrolowanym przez Grzymościa. | 0110-01-17 - 0110-01-20 |
| 190828-migswiatlo-psychotroniczek   | kocha hazard; zmusił Talię by ta naprawiła dla niego BIA klasy Szponowiec. Trochę wygrywał, ale potem wszystko się skończyło (bo Pięknotka to wysadziła). | 0110-02-07 - 0110-02-09 |
| 190917-zagubiony-efemerydyta        | ma zwerbować Janka do mafii. Nacisnął nań, ale młody nie uciekł z Pustogoru. Ku swemu niezadowoleniu, nie złapał ptaszka. Nie wyszedł na światło dzienne; nikt o nim nie wie. | 0110-06-17 - 0110-06-18 |
| 191103-kontrpolowanie-pieknotki-pulapka | przerzucił Małmałaza do Aurum; okazuje się, że w imieniu Grzymościa robi elementy kanału przerzutowego do Aurum jako mięsień (nie jako głowa). | 0110-06-20 - 0110-06-27 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Pięknotka Diakon     | 4 | ((190827-rozpaczliwe-ratowanie-bii; 190828-migswiatlo-psychotroniczek; 190917-zagubiony-efemerydyta; 191103-kontrpolowanie-pieknotki-pulapka)) |
| Ernest Kajrat        | 2 | ((190827-rozpaczliwe-ratowanie-bii; 190828-migswiatlo-psychotroniczek)) |
| Mariusz Trzewń       | 2 | ((190827-rozpaczliwe-ratowanie-bii; 191103-kontrpolowanie-pieknotki-pulapka)) |
| Minerwa Metalia      | 2 | ((190828-migswiatlo-psychotroniczek; 191103-kontrpolowanie-pieknotki-pulapka)) |
| Olaf Zuchwały        | 2 | ((190917-zagubiony-efemerydyta; 191103-kontrpolowanie-pieknotki-pulapka)) |
| Talia Aegis          | 2 | ((190827-rozpaczliwe-ratowanie-bii; 190828-migswiatlo-psychotroniczek)) |
| Tymon Grubosz        | 2 | ((190827-rozpaczliwe-ratowanie-bii; 190828-migswiatlo-psychotroniczek)) |
| Artur Michasiewicz   | 1 | ((190828-migswiatlo-psychotroniczek)) |
| BIA Tarn             | 1 | ((190827-rozpaczliwe-ratowanie-bii)) |
| Damian Orion         | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Gabriel Ursus        | 1 | ((190917-zagubiony-efemerydyta)) |
| Jan Uszczar          | 1 | ((190917-zagubiony-efemerydyta)) |
| Józef Małmałaz       | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Ksenia Kirallen      | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Lilia Ursus          | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Rafał Roszczeniok    | 1 | ((191103-kontrpolowanie-pieknotki-pulapka)) |
| Sławomir Niejadek    | 1 | ((190827-rozpaczliwe-ratowanie-bii)) |
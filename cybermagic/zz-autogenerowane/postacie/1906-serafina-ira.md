---
categories: profile
factions: 
owner: public
title: Serafina Ira
---

# {{ page.title }}


# Read: 

## Postać

### Ogólny pomysł (3)

Iluzjonistka oraz performerka efektownych koncertów. Jej drugie oblicze to SBN "Banshee", soniczna wojowniczka używająca ludzkich i mechanicznych agentów. Powiązana z Cieniaszczytem i przestępcami Pustogorskimi.

### Czego chce a nie ma (3)

* niech ogień nie gaśnie, wolna i aktywna; każdy ruch to potencjalne zagrożenie, łatwo wpaść w beznadziejność
* uwolnić ofiary Pustogoru i Orbitera, ratować słabszych; jej przyjaciele zostali uwięzieni przez Pustogor
* dać radość i ukojenie; nie przejdzie obojętnie koło cierpienia, nawet wroga

### Sposób działania (3)

* chaos - odwracanie uwagi, iluzje, kłamstwa, aktorstwo, muzyka dysonansu, iluzyjny świat
* miłość - hipnoza, piękno muzyki, czar i urok, aktorstwo
* zniszczenie - broń soniczna, servar 'Banshee'
* agenci - konstrukty, fani i miłośnicy performerki

### Zasoby i otoczenie (3)

* servar 'Banshee': lekki i trudny do wykrycia, szybki servar soniczny
* agenci: zarówno ludzcy fani jak i konstrukty zbudowane dla niej przez Błękitne Niebo
* ukochana Cieniaszczytu: sponsorzy i bogacze, którzy kochają jej sztukę

### Magia (3)

#### Gdy kontroluje energię

* Iluzje oraz magia soniczna
* Zdalna kontrola urządzeń i mechanizmów

#### Gdy traci kontrolę

* Zimny gniew soniczny - destrukcja i rezonans
* Melodia i muzyka
* Iluzje, pokazujące jej prawdziwe uczucia i naturę

### Powiązane frakcje

{{ page.factions }}

## Opis

Przyjęła ród Ira, gdyż za wszelką cenę pragnęła odzyskać swoich przyjaciół z Pacyfiki - a przechwycił ich Orbiter z pomocą Pustogoru.

(8 min)


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190616-anomalna-serafina            | Cieniaszczycka Banshee, czarodziejka soniczna i iluzji. Mag rodu. Uśmiechnięta i wesoła piosenkarka, która zbiera anomalie a w sercu ma nienawiść do Pustogoru. | 0110-05-08 - 0110-05-11 |
| 190622-wojna-kajrata                | wbrew sobie pomogła Pięknotce z tym jak stabilizować krystaliczną anomalię, nie chce być zła. Nie uzyskała trzeciej anomalii. Poznaliśmy na tej sesji jej historię. | 0110-05-14 - 0110-05-17 |
| 190623-noc-kajrata                  | doprowadzana do paranoi przez Kajrata zaryzykowała i zintegrowała Esuriit ze swoim wzorem. Uzyskała aspekt Banshee. Esuriit jeszcze bardziej ją podradykalizowało. | 0110-05-18 - 0110-05-21 |
| 210831-serafina-staje-za-wydrami    | bardzo osobiście wzięła to, że Pustogor ukrył zniknięcie i śmierć niewinnych. Wspiera Wydry. Dała się przekonać Marysi, by 2 tygodnie poczekać z uzbrajaniem Wydr. Radykalna - żąda publicznego shamingu tych, co stoją za zabiciem niewinnych (czyli najpewniej Amelii Sowińskiej). Zgodziła się na spotkanie osobiste z Marysią, o dziwo. | 0111-07-03 - 0111-07-06 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 190616-anomalna-serafina            | Banshee (servar) osiągnął pełną moc jako iluzyjno-soniczny generator obrazu i dźwięku. | 0110-05-11
| 190623-noc-kajrata                  | uzyskała Aspekt Banshee przez wpływ Esuriit i manipulację Kajrata na jej Wzór Ira | 0110-05-21

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ernest Kajrat        | 3 | ((190616-anomalna-serafina; 190622-wojna-kajrata; 190623-noc-kajrata)) |
| Pięknotka Diakon     | 3 | ((190616-anomalna-serafina; 190622-wojna-kajrata; 190623-noc-kajrata)) |
| Liliana Bankierz     | 2 | ((190622-wojna-kajrata; 190623-noc-kajrata)) |
| Antoni Żuwaczka      | 1 | ((190616-anomalna-serafina)) |
| Daniel Terienak      | 1 | ((210831-serafina-staje-za-wydrami)) |
| Halina Sermniek      | 1 | ((210831-serafina-staje-za-wydrami)) |
| Karolina Terienak    | 1 | ((210831-serafina-staje-za-wydrami)) |
| Krystian Namałłek    | 1 | ((190616-anomalna-serafina)) |
| Laura Tesinik        | 1 | ((210831-serafina-staje-za-wydrami)) |
| Lorena Gwozdnik      | 1 | ((210831-serafina-staje-za-wydrami)) |
| Marysia Sowińska     | 1 | ((210831-serafina-staje-za-wydrami)) |
| Nikola Kirys         | 1 | ((190622-wojna-kajrata)) |
| Olaf Zuchwały        | 1 | ((190622-wojna-kajrata)) |
| Ossidia Saitis       | 1 | ((190623-noc-kajrata)) |
| Rafał Torszecki      | 1 | ((210831-serafina-staje-za-wydrami)) |
| Ronald Grzymość      | 1 | ((190616-anomalna-serafina)) |
| Tomasz Tukan         | 1 | ((190616-anomalna-serafina)) |
---
categories: profile
factions: 
owner: public
title: Wiktor Blakenbauer
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210306-wiktoriata                   | BG; 20. Twórca super-skutecznego psychoaktywnego środku o nazwie wiktoriata; rozprzestrzenia przez wiły. By dorobić i eksperymentować, sprzedaje go do Verlenów. Zatrzymany przez Lucjusza, bo wiktoriata ma psychotyczne efekty uboczne. | 0093-10-21 - 0093-10-30 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Apollo Verlen        | 1 | ((210306-wiktoriata)) |
| Lucjusz Blakenbauer  | 1 | ((210306-wiktoriata)) |
| Przemysław Czapurt   | 1 | ((210306-wiktoriata)) |
| Viorika Verlen       | 1 | ((210306-wiktoriata)) |
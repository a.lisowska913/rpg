---
categories: profile
factions: 
owner: public
title: Dominika Perikas
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220316-potwor-czy-choroba-na-etaur  | oficjalnie poprosiła Ariannę o pomoc, bo widzi, że z sytuacją można sobie nie poradzić... | 0112-04-24 - 0112-04-26 |
| 220323-zatruta-furia-gaulronow      | jest bardzo przeciwna bioformom, eksperymentom na ludziach itp; Orbiter wrobił ją w to że to niby ONA stoi za zmianą dostawcy. A ona ochrania | 1111-11-11 - 1111-11-11 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Suwan Chankar        | 2 | ((220316-potwor-czy-choroba-na-etaur; 220323-zatruta-furia-gaulronow)) |
| Arianna Verlen       | 1 | ((220316-potwor-czy-choroba-na-etaur)) |
| Elena Verlen         | 1 | ((220316-potwor-czy-choroba-na-etaur)) |
| Erwin Mumurnik       | 1 | ((220323-zatruta-furia-gaulronow)) |
| Eustachy Korkoran    | 1 | ((220316-potwor-czy-choroba-na-etaur)) |
| Feliks Ketran        | 1 | ((220323-zatruta-furia-gaulronow)) |
| Graniec Borgon       | 1 | ((220323-zatruta-furia-gaulronow)) |
| Klaudia Stryk        | 1 | ((220316-potwor-czy-choroba-na-etaur)) |
| Lamia Akacja         | 1 | ((220323-zatruta-furia-gaulronow)) |
| Maciej Parczak       | 1 | ((220323-zatruta-furia-gaulronow)) |
| Maria Naavas         | 1 | ((220316-potwor-czy-choroba-na-etaur)) |
| Melania Akacja       | 1 | ((220316-potwor-czy-choroba-na-etaur)) |
| TAI Nephthys         | 1 | ((220323-zatruta-furia-gaulronow)) |
| Tymoteusz Czerw      | 1 | ((220316-potwor-czy-choroba-na-etaur)) |
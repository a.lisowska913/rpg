---
categories: profile
factions: 
owner: public
title: Emmanuelle Gęsiawiec
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230426-mscizab-zabojca-arachnoziemskich-jaszczurow | z pomocą innego Verlena zaaranżowała wyjście Jaszczurów Tunelowych w Arachnoziem by Mściząb mógł je zabić i zostać bohaterem. All for love. Niestety, robiąc to wyciągnęła na powierzchnię Łowcę. Ukrywała się jako 'Maciek' - gdy Arianna ją skonfrontowała, prawda wyszła na jaw i Mściząb się jej wyparł. | 0095-08-03 - 0095-08-05 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 1 | ((230426-mscizab-zabojca-arachnoziemskich-jaszczurow)) |
| Atraksjusz Verlen    | 1 | ((230426-mscizab-zabojca-arachnoziemskich-jaszczurow)) |
| Mściząb Verlen       | 1 | ((230426-mscizab-zabojca-arachnoziemskich-jaszczurow)) |
| Viorika Verlen       | 1 | ((230426-mscizab-zabojca-arachnoziemskich-jaszczurow)) |
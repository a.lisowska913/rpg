---
categories: profile
factions: 
owner: public
title: Michał Uszwon
---

# {{ page.title }}


# Generated: 



## Fiszki


* salvager Serien (hired muscle / engineer) faeril: robota czeka. | @ 230104-to-co-zostalo-po-burzy
* (ENCAO: 00-0+ |Zakłóca spokój; nie toleruje spokoju i nudy| VALS: Face >> Achievement| DRIVE: Poznać sekret Rufusa - skąd ma Seiren?) | @ 230104-to-co-zostalo-po-burzy
* "Jak masz czas gadać to masz czas " | @ 230104-to-co-zostalo-po-burzy
* kadencja: twardy, nie okazuje słabości, gardzi mamlasami | @ 230104-to-co-zostalo-po-burzy
* salvager Seiren (hired muscle / engineer) faeril: robota czeka. | @ 230125-whispraith-w-jaskiniach-neikatis
* "Jak masz czas gadać to masz czas robić" "Zamknij mordę." | @ 230125-whispraith-w-jaskiniach-neikatis

### Wątki


historia-eustachego
arkologia-nativis
plagi-neikatis

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230104-to-co-zostalo-po-burzy       | inżynier Seiren. Cytat "zamknij mordę". Nie boi się ryzyka, wraz z Cyprianem blokują ruchy Rufusa (który chce ryzykować bo Anna). Naprawia z Rufusem systemy Uśmiechu Kamili. | 0092-10-26 - 0092-10-28 |
| 230125-whispraith-w-jaskiniach-neikatis | whispraith go Dotknął; zaczął starcie z Rufusem i uciekł w piaski. Whispraith pozwolił mu dotrzeć do Anny i go opętał. | 0092-10-29 - 0092-11-02 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Aniela Myszawcowa    | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| Anna Seiren          | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| Antoni Grzypf        | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| Cyprian Kugrak       | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| Eustachy Korkoran    | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| JAN Seiren           | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| JAN Uśmiech Kamili   | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| Rufus Seiren         | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| Zofia d'Seiren       | 2 | ((230104-to-co-zostalo-po-burzy; 230125-whispraith-w-jaskiniach-neikatis)) |
| Kalia Awiter         | 1 | ((230104-to-co-zostalo-po-burzy)) |
| Kornelia Lichitis    | 1 | ((230125-whispraith-w-jaskiniach-neikatis)) |
| Rafał Kidiron        | 1 | ((230104-to-co-zostalo-po-burzy)) |
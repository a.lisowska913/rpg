---
categories: profile
factions: 
owner: public
title: Donald Parziarz
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 201117-morszczat-zostaje-piratem    | kapitan piratów spoza Astorii używający koloidowego statku; chce sławy i ludzi. Złapany na przynętę przez Zespół - chce zapolować na Ariannę Verlen. | 0111-01-11 - 0111-01-15 |
| 201210-pocalunek-aspirii            | kiedyś jeden z oficerów sił pokojowych Orbitera w arkologii Aspirii, teraz kapitan piratów. Stracił pojazd i czść załogi do Inferni. | 0111-01-26 - 0111-01-29 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| AK Nocna Krypta      | 1 | ((201210-pocalunek-aspirii)) |
| AK Wyjec             | 1 | ((201210-pocalunek-aspirii)) |
| Anastazja Sowińska   | 1 | ((201210-pocalunek-aspirii)) |
| Arianna Verlen       | 1 | ((201210-pocalunek-aspirii)) |
| Bożena Kaldesz       | 1 | ((201117-morszczat-zostaje-piratem)) |
| Eustachy Korkoran    | 1 | ((201210-pocalunek-aspirii)) |
| Juliusz Sowiński     | 1 | ((201210-pocalunek-aspirii)) |
| Katra Igneus         | 1 | ((201210-pocalunek-aspirii)) |
| Klaudia Stryk        | 1 | ((201210-pocalunek-aspirii)) |
| Ludwik Kiron         | 1 | ((201117-morszczat-zostaje-piratem)) |
| OA Zguba Tytanów     | 1 | ((201210-pocalunek-aspirii)) |
| OO Infernia          | 1 | ((201210-pocalunek-aspirii)) |
| Siergiej Rożen       | 1 | ((201117-morszczat-zostaje-piratem)) |
| Zbigniew Morszczat   | 1 | ((201117-morszczat-zostaje-piratem)) |
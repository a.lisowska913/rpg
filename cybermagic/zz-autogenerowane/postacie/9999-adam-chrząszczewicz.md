---
categories: profile
factions: 
owner: public
title: Adam Chrząszczewicz
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221012-kapitan-verlen-i-niezapowiedziana-inspekcja | inspektor z Orbitera sprawdzający Królową; mimo katastrofalnego stanu Królowej i tego że wszystko poszło nie tak,chce pomóc Ariannie wyciągnąć jednostkę. Nie chce zniszczyć reputacji Arianny i tylko dał jej naganę. (ENCAO: --+00, trudny do zaskoczenia i polega na sobie, Power > Family, Preserver: "this planet is dying..."). | 0100-03-19 - 0100-03-23 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 1 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja)) |
| Arnulf Perikas       | 1 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja)) |
| Daria Czarnewik      | 1 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja)) |
| Hubert Kerwelenios   | 1 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja)) |
| Leona Astrienko      | 1 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja)) |
| Maja Samszar         | 1 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja)) |
| Mariusz Bulterier    | 1 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja)) |
| OO Królowa Kosmicznej Chwały | 1 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja)) |
| Szczepan Myrczek     | 1 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja)) |
| Tomasz Dojnicz       | 1 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja)) |
| Władawiec Diakon     | 1 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja)) |
---
categories: profile
factions: 
owner: public
title: Azonia Arris
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 191211-kryzys-energii-na-szotaron   | opracowała źródło energii oparte na Esuriit; jako jedyny mag została uratowana przez Cienie Szotaron i ewakuowana w stronę stacji Dorszant. Noktianka. | 0110-06-29 - 0110-07-02 |
| 191218-kijara-corka-szotaron        | uratowana przez Dorszantowców i wykorzystana jako przynęta na Kijarę; przetrwała, acz z pewnym uszczerbkiem psychicznym. | 0110-07-04 - 0110-07-07 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Eszara d'Dorszant    | 1 | ((191218-kijara-corka-szotaron)) |
| Felicja Taranit      | 1 | ((191218-kijara-corka-szotaron)) |
| Filip Szczatken      | 1 | ((191218-kijara-corka-szotaron)) |
| Jaromir Uczkram      | 1 | ((191218-kijara-corka-szotaron)) |
| Kijara d'Esuriit     | 1 | ((191218-kijara-corka-szotaron)) |
| Oliwier Pszteng      | 1 | ((191218-kijara-corka-szotaron)) |
| Rafał Kirlat         | 1 | ((191218-kijara-corka-szotaron)) |
| Szymon Szelmer       | 1 | ((191218-kijara-corka-szotaron)) |
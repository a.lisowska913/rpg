---
categories: profile
factions: 
owner: public
title: Robert Garwen
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190418-spozniona-wojna-elizy        | szeryf z Barbakanu Trzeciego Raju; to on obstawiał by nikogo nie zabijać a dać szansę, więc go zesłali do Trzeciego Raju | 0110-03-16 - 0110-03-18 |
| 190813-niesmiertelny-komandos-saitaera | z uwagi na brak środków i pieniędzy w Trzecim Raju akceptuje dziwne działania Aurum z nadzieją na pieniądze. | 0110-05-20 - 0110-05-25 |
| 200916-smierc-raju                  | zaufał Ariannie, że ta da radę opanować nojrepy w Trzecim Raju; teraz Raj jest efektywnie zniszczony. On jednak jest tym, co się podpisał pod rozkazami. | 0110-12-21 - 0110-12-23 |
| 201104-sabotaz-swini                | dał się przekonać Ariannie, że sabotażyści pochodzą z "Szalonego Rumaka", choć z oporami. I dobrze że z oporami, bo Arianna ściemniała. | 0111-01-13 - 0111-01-16 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 201104-sabotaz-swini                | stracił trochę opinii na temat Arianny Verlen; musiała się pomylić odnośnie sabotażystów i jej błędne decyzje doprowadziły do śmierci niewinnego astorianina. | 0111-01-16

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Anastazja Sowińska   | 2 | ((200916-smierc-raju; 201104-sabotaz-swini)) |
| Arianna Verlen       | 2 | ((200916-smierc-raju; 201104-sabotaz-swini)) |
| Ataienne             | 2 | ((200916-smierc-raju; 201104-sabotaz-swini)) |
| Eustachy Korkoran    | 2 | ((200916-smierc-raju; 201104-sabotaz-swini)) |
| Klaudia Stryk        | 2 | ((200916-smierc-raju; 201104-sabotaz-swini)) |
| Rafał Armadion       | 2 | ((200916-smierc-raju; 201104-sabotaz-swini)) |
| Alfred Cerulean      | 1 | ((190813-niesmiertelny-komandos-saitaera)) |
| Celina Szilat        | 1 | ((200916-smierc-raju)) |
| Damian Drób          | 1 | ((190418-spozniona-wojna-elizy)) |
| Diana Arłacz         | 1 | ((201104-sabotaz-swini)) |
| Elena Verlen         | 1 | ((200916-smierc-raju)) |
| Eliza Ira            | 1 | ((190418-spozniona-wojna-elizy)) |
| Izabela Zarantel     | 1 | ((200916-smierc-raju)) |
| Maria Gołąb          | 1 | ((190813-niesmiertelny-komandos-saitaera)) |
| Marian Fartel        | 1 | ((200916-smierc-raju)) |
| Martyn Hiwasser      | 1 | ((201104-sabotaz-swini)) |
| Robert Arłacz        | 1 | ((190813-niesmiertelny-komandos-saitaera)) |
| Wanessa Pyszcz       | 1 | ((200916-smierc-raju)) |
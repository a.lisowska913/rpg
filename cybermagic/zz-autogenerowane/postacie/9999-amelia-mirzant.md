---
categories: profile
factions: 
owner: public
title: Amelia Mirzant
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 191108-ukojenie-aleksandrii         | majsterkowiczka i cinkciarz. Konstruktor najwyższej klasy - hiper-EMP oraz hiper-neurotoksyna. Przejęła kontrolę nad Aleksandrią. | 0110-07-14 - 0110-07-16 |
| 191113-jeden-problem-dwie-rodziny   | katalistka, która przejęła kontrolę nad całym terenem i biznesowo wszystkim steruje. Dogaduje się z Aleksandrią, nie jest jej częścią. Pośrednio steruje Lemurczakiem. | 0110-09-26 - 0110-10-01 |
| 191126-smierc-aleksandrii           | zaplątała się w temat Aleksandrii i by się wyplątać musiała mnóstwo negocjować, manipulować itp. Sukces. Acz skończyła z "przyjaznym TAI" - echem Aleksandrii na które poluje Aurum. | 0110-10-06 - 0110-10-09 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 191113-jeden-problem-dwie-rodziny   | największy zwycięzca. Kontroluje Kamila Lemurczaka, pozbyła się zagrożenia na tym terenie i jeszcze nikt nie wie o jej Aleksandrii. | 0110-10-01
| 191126-smierc-aleksandrii           | uzyskała nową "przyjaciółkę" - Olę d'Amelia. Wyplątała się z ewentualnych win, ale nie jest już królową Kruczańca. Szkoda :-). | 0110-10-09

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Kamil Lemurczak      | 2 | ((191113-jeden-problem-dwie-rodziny; 191126-smierc-aleksandrii)) |
| Klara Baszcz         | 2 | ((191108-ukojenie-aleksandrii; 191113-jeden-problem-dwie-rodziny)) |
| Leszek Baszcz        | 2 | ((191108-ukojenie-aleksandrii; 191113-jeden-problem-dwie-rodziny)) |
| Paweł Kukułnik       | 2 | ((191108-ukojenie-aleksandrii; 191113-jeden-problem-dwie-rodziny)) |
| Teresa Marszalnik    | 2 | ((191113-jeden-problem-dwie-rodziny; 191126-smierc-aleksandrii)) |
| Daniela Baszcz       | 1 | ((191108-ukojenie-aleksandrii)) |
| Karol Kszatniak      | 1 | ((191113-jeden-problem-dwie-rodziny)) |
| Kasjopea Maus        | 1 | ((191126-smierc-aleksandrii)) |
| Kinga Stryk          | 1 | ((191108-ukojenie-aleksandrii)) |
| Ola d'Amelia         | 1 | ((191126-smierc-aleksandrii)) |
| Sabina Kazitan       | 1 | ((191113-jeden-problem-dwie-rodziny)) |
| Sebastian Kuralsz    | 1 | ((191126-smierc-aleksandrii)) |
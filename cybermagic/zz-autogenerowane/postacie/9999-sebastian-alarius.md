---
categories: profile
factions: 
owner: public
title: Sebastian Alarius
---

# {{ page.title }}


# Generated: 



## Fiszki


* (klarkartianin), oddział Alarius | @ 230102-elwira-koszmar-nox-ignis
* "Noctis might have forsaken us, but we still can do so much more!", "Those planetoids will be useful for us all." | @ 230102-elwira-koszmar-nox-ignis
* Wojna Deoriańska: odrzucił Noctis, które odrzuciło prawdę. Szuka nowego życia. | @ 230102-elwira-koszmar-nox-ignis

### Wątki


historia-talii
koszmar-nox-ignis
wojna-deorianska

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200826-nienawisc-do-swin            | kapitan Orbitera noktiańskiego pochodzenia zwykle transportujący glukszwajny między Trzecim Rajem a Orbiterem; poszedł zbyt legalistycznie i Raj ryzykuje śmierć przez brak środków. Niestety, zginął (KIA) w więzieniu podczas zamieszek. | 0110-12-08 - 0110-12-14 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Aleksandra Termia    | 1 | ((200826-nienawisc-do-swin)) |
| Antoni Kramer        | 1 | ((200826-nienawisc-do-swin)) |
| Arianna Verlen       | 1 | ((200826-nienawisc-do-swin)) |
| Eustachy Korkoran    | 1 | ((200826-nienawisc-do-swin)) |
| Kamil Lyraczek       | 1 | ((200826-nienawisc-do-swin)) |
| Klaudia Stryk        | 1 | ((200826-nienawisc-do-swin)) |
| Leona Astrienko      | 1 | ((200826-nienawisc-do-swin)) |
| Martyn Hiwasser      | 1 | ((200826-nienawisc-do-swin)) |
| Tadeusz Ursus        | 1 | ((200826-nienawisc-do-swin)) |
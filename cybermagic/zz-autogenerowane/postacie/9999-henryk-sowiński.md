---
categories: profile
factions: 
owner: public
title: Henryk Sowiński
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 201020-przygoda-randka-i-porwanie   | inteligentniejszy brat; ma ogromne parcie na szkło w VirtStream. Rozwiązywał większość zagadek i wyciągnął wszystkie wnioski co miał gdy prowadzony za rękę. | 0110-10-21 - 0110-10-23 |
| 201230-pulapka-z-anastazji          | dowódca OO Zguby Tytanów; całość sławy i chwały za odbicie Rodivasa spada na niego - zaakceptował plan Arianny. Ma dość tego, że Anastazji nie ma - postawił ultimatum Ariannie. | 0111-05-21 - 0111-05-22 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 201020-przygoda-randka-i-porwanie   | VirtStream był wyjątkowo popularny, dostał sporo subskrybcji. Nauczył się sporo o anomaliach i pułapkach przestrzennych. | 0110-10-23
| 201230-pulapka-z-anastazji          | sława i chwała za zdobycie Rodivasa i "odzyskanie Anastazji", którą "straciła Arianna". Łaska ze strony Sowińskich. | 0111-05-22

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| AK Nocna Krypta      | 1 | ((201230-pulapka-z-anastazji)) |
| AK Rodivas           | 1 | ((201230-pulapka-z-anastazji)) |
| Anastazja Sowińska Dwa | 1 | ((201230-pulapka-z-anastazji)) |
| Arianna Verlen       | 1 | ((201230-pulapka-z-anastazji)) |
| Damian Orion         | 1 | ((201230-pulapka-z-anastazji)) |
| Daniel Sowiński      | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Elena Verlen         | 1 | ((201230-pulapka-z-anastazji)) |
| Eustachy Korkoran    | 1 | ((201230-pulapka-z-anastazji)) |
| Gabriel Ursus        | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Kacper Bankierz      | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Klaudia Stryk        | 1 | ((201230-pulapka-z-anastazji)) |
| Laurencjusz Sorbian  | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Liliana Bankierz     | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Martyn Hiwasser      | 1 | ((201230-pulapka-z-anastazji)) |
| OA Zguba Tytanów     | 1 | ((201230-pulapka-z-anastazji)) |
| OO Infernia          | 1 | ((201230-pulapka-z-anastazji)) |
| Robert Pakiszon      | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Robinson Porzecznik  | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Tomasz Tukan         | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Triana Porzecznik    | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Tymon Grubosz        | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Urszula Miłkowicz    | 1 | ((201020-przygoda-randka-i-porwanie)) |
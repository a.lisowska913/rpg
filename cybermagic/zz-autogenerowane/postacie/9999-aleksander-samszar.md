---
categories: profile
factions: 
owner: public
title: Aleksander Samszar
---

# {{ page.title }}


# Generated: 



## Fiszki


* mag, obrońca ludzi i duchów | @ 230419-karolinka-nieokielznana-swinka
* (ENCAO:  -0++0 |Nie wychyla się;;Skryty;;Metodyczny;;Praworządny;;Ugodowy, unika konfliktów| VALS: Conformity, Universalism| DRIVE: Odbudowa i odnowa) | @ 230419-karolinka-nieokielznana-swinka
* styl: Emiya Shirou (Fate Stay Night) | @ 230419-karolinka-nieokielznana-swinka
* Mag materii i duchów, piercer of flesh and souls, mag bojowy i dowódca oddziału duchów | @ 230419-karolinka-nieokielznana-swinka

### Wątki


waśnie-samszar-verlen
waśnie-blakenbauer-verlen
legendy-verlenlandu

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230419-karolinka-nieokielznana-swinka | 36 lat, nie dał się nabrać na 'uroczą Elenkę', ale pomógł Verlenkom walczyć z Hybrydą w Siewczynie. Zauważył, że Verlenki mają ten sam cel co on, więc... | 0095-07-19 - 0095-07-23 |
| 230516-karolinka-raciczki-zemsty-verlenow | czyści Siewczyn do końca i wyjaśnił, co zrobiły Verlenki. Doszedł do tego, że były też na Kwiatowisku i wysłał tam Karolinusa. Dostarczył poproszony substancję do uśpienia glukszwajnów. Zadowolony z działań Karolinusa i Eleny - pokonali Lemurczaka (nie oni, ale on tego nie wie). | 0095-07-29 - 0095-07-31 |
| 230613-zaginiecie-psychotronika-cede | cichy sojusznik Karolinusa i Strzały przeciwko Mrocznym Samszarom; "zdradził" Karolinusa i nadał Fabianowi info że Karolinus się interesuje Cede. A potem dostarczył Strzale anty-amnestyki. | 0095-08-02 - 0095-08-05 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Karolinus Samszar    | 2 | ((230516-karolinka-raciczki-zemsty-verlenow; 230613-zaginiecie-psychotronika-cede)) |
| AJA Szybka Strzała   | 1 | ((230613-zaginiecie-psychotronika-cede)) |
| Amara Zegarzec       | 1 | ((230516-karolinka-raciczki-zemsty-verlenow)) |
| Apollo Verlen        | 1 | ((230419-karolinka-nieokielznana-swinka)) |
| Arianna Verlen       | 1 | ((230419-karolinka-nieokielznana-swinka)) |
| Cede Burian          | 1 | ((230613-zaginiecie-psychotronika-cede)) |
| Celina Burian        | 1 | ((230613-zaginiecie-psychotronika-cede)) |
| Elena Samszar        | 1 | ((230516-karolinka-raciczki-zemsty-verlenow)) |
| Elena Verlen         | 1 | ((230419-karolinka-nieokielznana-swinka)) |
| Fabian Samszar       | 1 | ((230613-zaginiecie-psychotronika-cede)) |
| Fantazjusz Verlen    | 1 | ((230419-karolinka-nieokielznana-swinka)) |
| Franciszek Chartowiec | 1 | ((230516-karolinka-raciczki-zemsty-verlenow)) |
| Lara Ukraptin        | 1 | ((230613-zaginiecie-psychotronika-cede)) |
| Ludmiła Zegarzec     | 1 | ((230516-karolinka-raciczki-zemsty-verlenow)) |
| Marcinozaur Verlen   | 1 | ((230419-karolinka-nieokielznana-swinka)) |
| Tymek Samszar        | 1 | ((230419-karolinka-nieokielznana-swinka)) |
| Ula Blakenbauer      | 1 | ((230419-karolinka-nieokielznana-swinka)) |
| Viorika Verlen       | 1 | ((230419-karolinka-nieokielznana-swinka)) |
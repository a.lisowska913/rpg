---
categories: profile
factions: 
owner: public
title: Julia Kardolin
---

# {{ page.title }}


# Read: 

## Kim jest

### W kilku zdaniach

Scrapper, mechanik.
Szczur i szalona konstruktorka.

### Co się rzuca w oczy

* 

## Mechanika

### Znaczące Czyny i Osiągnięcia (3)

* 
* 

### Co się rzuca w oczy: Atuty i Przewagi (3, 6)

* PRACA: 
* PRACA: Mechanik (w szczególności ścigacze)
* SERCE: 
* SERCE: Uwielbia karaoke
* ATUT: 
* ATUT: 

### Charakterystyczne zasoby (3)

* COŚ: 
* KTOŚ: 
* WIEM: 
* OPINIA: 

### Typowe problemy z którymi sobie nie radzi (-3)

* CIAŁO: 
* CIAŁO: 
* SERCE: 
* SERCE: 
* STRACH: 
* STRACH:
* MAGIA: 
* INNI: 

### Specjalne

* .

## Inne

### Wygląd

.

### Coś Więcej

* .

### Endgame

* .


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 201013-pojedynek-akademia-rekiny    | technomantka i psychotroniczka; uczennica Akademii Magicznej Zaczęstwa. Postawiła ścigacz Remor340D i pomogła Napoleonowi wygrać turniej Rekinów. Rekiny często u niej zamawiają tuning ścigaczy i ich naprawę, specjalizuje się w ścigaczach. | 0110-10-10 - 0110-10-18 |
| 210323-grzybopreza                  | wymyśliła Myrczka do odzyskania techno-psa, po czym namówiła Myrczka do pomocy. Docelowo wymknęła się z Trianą i przechwyciła psa - kosztem zatruflowania okolic biurowców w Zaczęstwie. | 0111-04-18 - 0111-04-19 |
| 210406-potencjalnie-eksterytorialny-seksbot | 3 mc temu ratuje Trianę przed kocimi uszkami ostrym kebabem, teraz świetnie się skrada po złomowisku i uruchamia na wpół martwe urządzenia by spowolnić szabrowników aż Franek przyleci. | 0111-04-23 - 0111-04-24 |
| 210518-porywaczka-miragentow        | zbadała ścigacz Marysi (odkrywając Lorenę) i włamała się z Trianą do klubu Arkadia (Diana Lemurczak). Aha, i unieszkodliwiła magicznie ścigacz Loreny. | 0111-05-03 - 0111-05-04 |
| 210615-skradziony-kot-olgi          | zaczyna od używania dron Triany (kiepski pomysł, wpadły w berserk), szuka danych o somnibelu w bibliotece AMZ i ogólnie pozyskuje informacje od uczniów AMZ. | 0111-05-09 - 0111-05-11 |
| 210622-verlenka-na-grzybkach        | wezwała terminusów (Ulę) jak była ostrzeliwana i odwróciła railgun Lancera Arkadii. Potem przejęła dowodzenie nad zespołem - muszą oddać robota na czas XD | 0111-05-26 - 0111-05-27 |
| 210713-rekin-wspiera-mafie          | uratowała Torszeckiego przed anomalią mentalną używając ścigacza Franka Bulteriera nad którym pracowała. Uciekła z nim wysoko w powietrze i zanim zapomniała o anomalii zawiadomiła Marysię. | 0111-06-02 - 0111-06-05 |
| 230212-pierwszy-tajemniczy-wielbiciel-liliany | AMZ. Gdy Liliana i Triana chcą lecieć ratować nastolatków, ona wezwała służby (SRZ). Negocjuje z Laurą by ta powiedziała co może, dowiaduje się od nastolatków skąd zwinęli niebezpieczny portret a potem deeskaluje Lilianę by nie skrzywdziła Mariusza. Uzmysławia Lilianie, że Mariuszowi się mogła podobać, że to nie musi być spisek. | 0111-10-18 - 0111-10-20 |
| 230303-the-goose-from-hell          | Built a breakdancing mechanical monkey as a distraction, carried a cage, and set up the trap for the goose. | 0111-10-24 - 0111-10-26 |
| 230331-an-unfortunate-ratnapping    | skilled in creating traps, she designs a trap to catch the magical rats and singlehandedly solves the main rat nest in the warehouses. | 0111-11-04 - 0111-11-06 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 210518-porywaczka-miragentow        | ma ogromne ułatwienie do wkradania się i przełamywania zabezpieczeń Diany w Arkadii (i nie tylko). Neurosprzężenie Diany daje Julii przewagę przy łamaniu jej zabezpieczeń. | 0111-05-04

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Marysia Sowińska     | 6 | ((210323-grzybopreza; 210406-potencjalnie-eksterytorialny-seksbot; 210518-porywaczka-miragentow; 210615-skradziony-kot-olgi; 210622-verlenka-na-grzybkach; 210713-rekin-wspiera-mafie)) |
| Triana Porzecznik    | 5 | ((210323-grzybopreza; 210406-potencjalnie-eksterytorialny-seksbot; 210518-porywaczka-miragentow; 210622-verlenka-na-grzybkach; 230212-pierwszy-tajemniczy-wielbiciel-liliany)) |
| Liliana Bankierz     | 4 | ((201013-pojedynek-akademia-rekiny; 210615-skradziony-kot-olgi; 210622-verlenka-na-grzybkach; 230212-pierwszy-tajemniczy-wielbiciel-liliany)) |
| Ignacy Myrczek       | 3 | ((201013-pojedynek-akademia-rekiny; 210323-grzybopreza; 210622-verlenka-na-grzybkach)) |
| Kacper Bankierz      | 3 | ((201013-pojedynek-akademia-rekiny; 210323-grzybopreza; 210713-rekin-wspiera-mafie)) |
| Paweł Szprotka       | 3 | ((210615-skradziony-kot-olgi; 230303-the-goose-from-hell; 230331-an-unfortunate-ratnapping)) |
| Urszula Miłkowicz    | 3 | ((210323-grzybopreza; 210622-verlenka-na-grzybkach; 210713-rekin-wspiera-mafie)) |
| Alex Deverien        | 2 | ((230303-the-goose-from-hell; 230331-an-unfortunate-ratnapping)) |
| Arkadia Verlen       | 2 | ((210615-skradziony-kot-olgi; 210622-verlenka-na-grzybkach)) |
| Carmen Deverien      | 2 | ((230303-the-goose-from-hell; 230331-an-unfortunate-ratnapping)) |
| kot-pacyfikator Tobias | 2 | ((230303-the-goose-from-hell; 230331-an-unfortunate-ratnapping)) |
| Lorena Gwozdnik      | 2 | ((210406-potencjalnie-eksterytorialny-seksbot; 210518-porywaczka-miragentow)) |
| Marek Samszar        | 2 | ((210615-skradziony-kot-olgi; 210622-verlenka-na-grzybkach)) |
| Napoleon Bankierz    | 2 | ((201013-pojedynek-akademia-rekiny; 210323-grzybopreza)) |
| Rafał Torszecki      | 2 | ((210622-verlenka-na-grzybkach; 210713-rekin-wspiera-mafie)) |
| Teresa Mieralit      | 2 | ((201013-pojedynek-akademia-rekiny; 230303-the-goose-from-hell)) |
| Aleksander Bemucik   | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Alicja Trawlis       | 1 | ((230303-the-goose-from-hell)) |
| Barnaba Burgacz      | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Diana Lemurczak      | 1 | ((210518-porywaczka-miragentow)) |
| Feliks Keksik        | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Franek Bulterier     | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Henryk Wkrąż         | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Justynian Diakon     | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Laura Tesinik        | 1 | ((230212-pierwszy-tajemniczy-wielbiciel-liliany)) |
| Mariusz Kupieczka    | 1 | ((230212-pierwszy-tajemniczy-wielbiciel-liliany)) |
| Olga Myszeczka       | 1 | ((210615-skradziony-kot-olgi)) |
| Radosław Turkamenin  | 1 | ((230331-an-unfortunate-ratnapping)) |
| Remor 340D           | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Robert Pakiszon      | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Różewicz Diakon      | 1 | ((210622-verlenka-na-grzybkach)) |
| Rupert Mysiokornik   | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Sabina Kazitan       | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Stella Armadion      | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Tomasz Tukan         | 1 | ((210713-rekin-wspiera-mafie)) |
| Wiktor Satarail      | 1 | ((210615-skradziony-kot-olgi)) |
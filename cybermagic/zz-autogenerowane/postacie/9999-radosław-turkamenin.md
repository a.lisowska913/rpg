---
categories: profile
factions: 
owner: public
title: Radosław Turkamenin
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230331-an-unfortunate-ratnapping    | Straż Harmonii; an officer of Guardians' cell, who confronts the team about the rat problem; extremely paranoid about the mages. | 0111-11-04 - 0111-11-06 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Alex Deverien        | 1 | ((230331-an-unfortunate-ratnapping)) |
| Carmen Deverien      | 1 | ((230331-an-unfortunate-ratnapping)) |
| Julia Kardolin       | 1 | ((230331-an-unfortunate-ratnapping)) |
| kot-pacyfikator Tobias | 1 | ((230331-an-unfortunate-ratnapping)) |
| Paweł Szprotka       | 1 | ((230331-an-unfortunate-ratnapping)) |
---
categories: profile
factions: 
owner: public
title: OO Loricatus
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221230-dowody-na-istnienie-nox-ignis | ciężka fregata szturmowa Orbitera p.d. Franza Szczypiornika, w dobrym stanie i zdolna do działania autonomicznego bez jednostek wsparcia. | 0082-07-28 - 0082-08-01 |
| 230102-elwira-koszmar-nox-ignis     | dociera oficjalnie do CON Ratio Spei. | 0082-08-02 - 0082-08-05 |
| 221221-astralna-flara-i-nowy-komodor | ciężka fregata i okręt flagowy komodora Bolzy. Na pokładzie 4 entropiki. | 0100-09-09 - 0100-09-12 |
| 230111-gdy-hr-reedukuje-niewlasciwa-osobe | wyłączona z akcji; jej Persefona służy do utrzymania bazy na Planetoidzie Lodowca. | 0100-09-15 - 0100-09-18 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| AK Nox Ignis         | 3 | ((221230-dowody-na-istnienie-nox-ignis; 230102-elwira-koszmar-nox-ignis; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Aletia Nix           | 2 | ((221230-dowody-na-istnienie-nox-ignis; 230102-elwira-koszmar-nox-ignis)) |
| Arianna Verlen       | 2 | ((221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Daria Czarnewik      | 2 | ((221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Dominik Łarnisz      | 2 | ((221230-dowody-na-istnienie-nox-ignis; 230102-elwira-koszmar-nox-ignis)) |
| Elena Verlen         | 2 | ((221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Franz Szczypiornik   | 2 | ((221230-dowody-na-istnienie-nox-ignis; 230102-elwira-koszmar-nox-ignis)) |
| Kajetan Kircznik     | 2 | ((221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Leszek Kurzmin       | 2 | ((221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Maja Samszar         | 2 | ((221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Medea Sowińska       | 2 | ((221230-dowody-na-istnienie-nox-ignis; 230102-elwira-koszmar-nox-ignis)) |
| OO Astralna Flara    | 2 | ((221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| OO Athamarein        | 2 | ((221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Persefona d'Loricatus | 2 | ((221230-dowody-na-istnienie-nox-ignis; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Salazar Bolza        | 2 | ((221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Talia Derwisz        | 2 | ((221230-dowody-na-istnienie-nox-ignis; 230102-elwira-koszmar-nox-ignis)) |
| Tatiana Ozariat      | 2 | ((221230-dowody-na-istnienie-nox-ignis; 230102-elwira-koszmar-nox-ignis)) |
| Tristan Ozariat      | 2 | ((221230-dowody-na-istnienie-nox-ignis; 230102-elwira-koszmar-nox-ignis)) |
| Władawiec Diakon     | 2 | ((221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Adragain Ferrias     | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Aida Liminis         | 1 | ((230102-elwira-koszmar-nox-ignis)) |
| Aleksy Sartaran      | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Brunon Szwagacz      | 1 | ((230102-elwira-koszmar-nox-ignis)) |
| Ellarina Samarintael | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| Grażyna Burgacz      | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Kaspian Certisarius  | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| Katrina Komczirp     | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |
| Lana Mirkinin        | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Miłosz Klinek        | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| NekroTAI Zarralea    | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| Riaon Diralik        | 1 | ((230102-elwira-koszmar-nox-ignis)) |
| Sabrina Ferrias      | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Sargon Niiris        | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| Sarian Xadaar        | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |
| Szczepan Myrczek     | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Wawrzyn Rewemis      | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |
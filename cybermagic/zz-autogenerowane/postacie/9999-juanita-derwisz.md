---
categories: profile
factions: 
owner: public
title: Juanita Derwisz
---

# {{ page.title }}


# Generated: 



## Fiszki


* prowodyrka "Domów Mirkali" i bizneswoman. Prawniczka. | @ 230620-karolinus-sedzia-mirkali
* styl: WR. "Mirkala jest warta więcej niż tylko te zioła. Samszarowie nie są dyktatorami." | @ 230620-karolinus-sedzia-mirkali
* Pozyskała inwestora. Ma wsparcie Diakonów. | @ 230620-karolinus-sedzia-mirkali

### Wątki


szamani-rodu-samszar

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230620-karolinus-sedzia-mirkali     | 40-50 lat, bizneswoman i prawniczka. Jej przyjaciel pochodzi z Mirkali i przyszła pomóc jego (kiepskiej) rodzinie, ale Starszyzna Mirkali ich zabiła. Juanita zdecydowała się uwolnić młodych z Mirkali (która jest trochę kołchozem zielarstwa) i zniszczyć Starszyznę ekonomicznie. Karolinus i Strzała ją Złamali i pokazali jej że to jest trudniejsza sprawa. Juanita opuszcza teren Samszarów z inwestycjami i w ogóle. | 0095-04-15 - 0095-04-18 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 230620-karolinus-sedzia-mirkali     | broken; opuszcza teren Samszarów na stałe | 0095-04-18

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Agnieszka Klirpin    | 1 | ((230620-karolinus-sedzia-mirkali)) |
| AJA Szybka Strzała   | 1 | ((230620-karolinus-sedzia-mirkali)) |
| Filip Klirpin        | 1 | ((230620-karolinus-sedzia-mirkali)) |
| Karolinus Samszar    | 1 | ((230620-karolinus-sedzia-mirkali)) |
---
categories: profile
factions: 
owner: public
title: Marcel Biekakis
---

# {{ page.title }}


# Generated: 



## Fiszki


* ENCAO:  -++0+ |Bezbarwny, przezroczysty;;Ekscentryczny, tatuaże WSZYSTKICH symboli ochronnych | VALS: Humility, Benevolence >> Security, Power| DRIVE: Polecenia Verlenów zasilają tatuaże ochronne | @ 230524-ekologia-jaszczurow-w-arachnoziemie
* Czarny Kieł, w oddziale Mścizęba | @ 230524-ekologia-jaszczurow-w-arachnoziemie

### Wątki


waśnie-blakenbauer-verlen
legendy-verlenlandu
mroczna-strona-verlenow

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230524-ekologia-jaszczurow-w-arachnoziemie | nieco dziwny tymczasowy dowódca Czarnych Kłów Mścizęba, zachowuje się normalnie ale ma mnóstwo tatuaży defensywnych. Jako jedyny Kieł ufa Viorice, bo... ufa wszystkim Verlenom. Dobrze zaraportował co i jak. | 0095-08-06 - 0095-08-09 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 1 | ((230524-ekologia-jaszczurow-w-arachnoziemie)) |
| Fabian Rzelicki      | 1 | ((230524-ekologia-jaszczurow-w-arachnoziemie)) |
| Mściząb Verlen       | 1 | ((230524-ekologia-jaszczurow-w-arachnoziemie)) |
| Ula Blakenbauer      | 1 | ((230524-ekologia-jaszczurow-w-arachnoziemie)) |
| Viorika Verlen       | 1 | ((230524-ekologia-jaszczurow-w-arachnoziemie)) |
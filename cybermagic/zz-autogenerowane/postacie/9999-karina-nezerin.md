---
categories: profile
factions: 
owner: public
title: Karina Nezerin
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220914-dziewczynka-trianai          | piętnastoletnia protomag-trianai; szukała miłości i rodziny infekując ludzi. Zaciekawiona Ardillą weszła w pułapkę Eustachego i została ciężko ranna i ze strasznym bólem. Mindwarp; jej krew i pocałunki uzależniają. Skonfliktowana między naturą Trianai i ludzką pamięcią. | 0092-09-10 - 0092-09-11 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ardilla Korkoran     | 1 | ((220914-dziewczynka-trianai)) |
| Bartłomiej Korkoran  | 1 | ((220914-dziewczynka-trianai)) |
| Eustachy Korkoran    | 1 | ((220914-dziewczynka-trianai)) |
| Stanisław Uczantor   | 1 | ((220914-dziewczynka-trianai)) |
---
categories: profile
factions: 
owner: public
title: Sara Mazirin
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230418-zywy-artefakt-w-gwiazdoczach | pod podszeptami Neidrii zrobiła sobie tatuaż Esuriit i zaczęła jako Adelaida Samszar ratować świat, malutkimi czynami naraz (Paweł nie oddał książki do biblioteki na czas - zakochany w tomiku niedźwiedziowej poezji Verlenów). Ciężko wyssana z energii życiowej, Elena przechwyciła ją sentisiecią zanim naprawdę coś jej się stało. | 0094-10-04 - 0094-10-06 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Adelaida Samszar     | 1 | ((230418-zywy-artefakt-w-gwiazdoczach)) |
| Antonina Blakenbauer | 1 | ((230418-zywy-artefakt-w-gwiazdoczach)) |
| Elena Samszar        | 1 | ((230418-zywy-artefakt-w-gwiazdoczach)) |
| Joachim Pulkmocz     | 1 | ((230418-zywy-artefakt-w-gwiazdoczach)) |
| Neidria Lazvarin     | 1 | ((230418-zywy-artefakt-w-gwiazdoczach)) |
| Robinson Porzecznik  | 1 | ((230418-zywy-artefakt-w-gwiazdoczach)) |
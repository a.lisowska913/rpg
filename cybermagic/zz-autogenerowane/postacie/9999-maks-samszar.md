---
categories: profile
factions: 
owner: public
title: Maks Samszar
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230502-strasznolabedz-atakuje-granice | rozstawił proceduralnie Fort Tawalizer i nie dał Viorice się cicho wślizgnąć; dobrze to zrobił. 22-letni mag przyzwyczajony do pewnej siły potworów; łabędź był silniejszy. Dał się przekonać Viorice, ale przez głupie proceduralne podejście i ego V. nie uczestniczyła w walce od początku - więcej rannych. Dał się jej przekonać do wzięcie dźwiedzia konsultanta bezpieczeństwa. | 0095-07-17 - 0095-07-19 |
| 230606-piekna-diakonka-i-rytual-nirwany-koz | (22); całkowicie zakręcony przez Itrię; dla niej zorganizował imprezę, zniszczył morale żołnierzy, aresztował dźwiedzia i ogólnie nie zachował się jakby się spodziewać po dowódcy bazy. Nauczony przez Elenę, przeprowadził rytuał nirwany kóz przebierając się za kozę. Na tej sesji - żałosny. | 0095-08-15 - 0095-08-18 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 230606-piekna-diakonka-i-rytual-nirwany-koz | dostał solidny opieprz od rodu, bo Karolinus na niego nadał. Czemu? Bo Maks zagraża terenowi jaki ma chronić przez cholerną Itrię i jest 'zbyt thirsty'. | 0095-08-18
| 230606-piekna-diakonka-i-rytual-nirwany-koz | to, że przebrał się za kozę by pójść do łóżka z Itrią Diakon, potem przeprowadził rytuał i ściągnął potwora NIE zostało mu zapomniane przez żołnierzy i w okolicy. | 0095-08-18

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| AJA Szybka Strzała   | 1 | ((230606-piekna-diakonka-i-rytual-nirwany-koz)) |
| Dźwiedź Łagodne Słowo | 1 | ((230606-piekna-diakonka-i-rytual-nirwany-koz)) |
| Elena Samszar        | 1 | ((230606-piekna-diakonka-i-rytual-nirwany-koz)) |
| Elena Verlen         | 1 | ((230502-strasznolabedz-atakuje-granice)) |
| Herbert Samszar      | 1 | ((230606-piekna-diakonka-i-rytual-nirwany-koz)) |
| Impresja Ignicja Incydencja Diakon | 1 | ((230606-piekna-diakonka-i-rytual-nirwany-koz)) |
| Karolinus Samszar    | 1 | ((230606-piekna-diakonka-i-rytual-nirwany-koz)) |
| Marcinozaur Verlen   | 1 | ((230502-strasznolabedz-atakuje-granice)) |
| Szymon Kapeć         | 1 | ((230502-strasznolabedz-atakuje-granice)) |
| Ula Blakenbauer      | 1 | ((230502-strasznolabedz-atakuje-granice)) |
| Viorika Verlen       | 1 | ((230502-strasznolabedz-atakuje-granice)) |
---
categories: profile
factions: 
owner: public
title: Wędziwój Verlen
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230517-tchorzliwy-leonidas-na-niedzwiedziowisku | ekspert od robienia wędlin z potworów, Verlen drugiej klasy; kuzyn Leonidasa i znajomy Fantazjusza. Gdy pojawiła się "Blakenbestia" sformowana Paradoksem Leonidasa, zebrał dzieciaki do kupy i dał im zajęcie, potem przekonał młodych by uwierzyli w "czempiona" (nie mówiąc że to Leonidas) i korzystając z "niewidzialności" spokojnie doprowadził "Blakenbestię" pod wodospad. | 0095-03-24 - 0095-03-26 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Dźwiedź Ciężka Łapa  | 1 | ((230517-tchorzliwy-leonidas-na-niedzwiedziowisku)) |
| Dźwiedź Lekka Stopa  | 1 | ((230517-tchorzliwy-leonidas-na-niedzwiedziowisku)) |
| Dźwiedź Palisadowspinacz | 1 | ((230517-tchorzliwy-leonidas-na-niedzwiedziowisku)) |
| Leonidas Verlen      | 1 | ((230517-tchorzliwy-leonidas-na-niedzwiedziowisku)) |
| Strużenka Verlen     | 1 | ((230517-tchorzliwy-leonidas-na-niedzwiedziowisku)) |
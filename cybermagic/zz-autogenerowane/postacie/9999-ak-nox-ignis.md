---
categories: profile
factions: 
owner: public
title: AK Nox Ignis
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221230-dowody-na-istnienie-nox-ignis | pierwszy raz Orbiter się o niej dowiaduje jako o 'anomalnej superbroni noktiańskiej'. Nieobecna na sesji. | 0082-07-28 - 0082-08-01 |
| 230102-elwira-koszmar-nox-ignis     | straszliwa anomalia żywiąca się pilotem i zastępująca w jego głowie pamięć czegoś na swój awatar, Elwirę. Niby nieobecna na sesji, ale pożera Tristana Ozariata. | 0082-08-02 - 0082-08-05 |
| 230111-gdy-hr-reedukuje-niewlasciwa-osobe | skonfliktowana z pilotką (Sabriną). Chce zniszczyć Biura HR, ale chce też uratować Adragaina (męża Sabriny). Przez sprzeczne instynkty ta Anomalia nie była w stanie osiągnąć żadnego z celów. Wróciła do Anomalii Kolapsu po zjedzeniu Sabriny. | 0100-09-15 - 0100-09-18 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| OO Loricatus         | 3 | ((221230-dowody-na-istnienie-nox-ignis; 230102-elwira-koszmar-nox-ignis; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Aletia Nix           | 2 | ((221230-dowody-na-istnienie-nox-ignis; 230102-elwira-koszmar-nox-ignis)) |
| Dominik Łarnisz      | 2 | ((221230-dowody-na-istnienie-nox-ignis; 230102-elwira-koszmar-nox-ignis)) |
| Franz Szczypiornik   | 2 | ((221230-dowody-na-istnienie-nox-ignis; 230102-elwira-koszmar-nox-ignis)) |
| Medea Sowińska       | 2 | ((221230-dowody-na-istnienie-nox-ignis; 230102-elwira-koszmar-nox-ignis)) |
| Persefona d'Loricatus | 2 | ((221230-dowody-na-istnienie-nox-ignis; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Talia Derwisz        | 2 | ((221230-dowody-na-istnienie-nox-ignis; 230102-elwira-koszmar-nox-ignis)) |
| Tatiana Ozariat      | 2 | ((221230-dowody-na-istnienie-nox-ignis; 230102-elwira-koszmar-nox-ignis)) |
| Tristan Ozariat      | 2 | ((221230-dowody-na-istnienie-nox-ignis; 230102-elwira-koszmar-nox-ignis)) |
| Adragain Ferrias     | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Aida Liminis         | 1 | ((230102-elwira-koszmar-nox-ignis)) |
| Aleksy Sartaran      | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Arianna Verlen       | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Brunon Szwagacz      | 1 | ((230102-elwira-koszmar-nox-ignis)) |
| Daria Czarnewik      | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Elena Verlen         | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Grażyna Burgacz      | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Kajetan Kircznik     | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Katrina Komczirp     | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |
| Lana Mirkinin        | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Leszek Kurzmin       | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Maja Samszar         | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Miłosz Klinek        | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| OO Astralna Flara    | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| OO Athamarein        | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Riaon Diralik        | 1 | ((230102-elwira-koszmar-nox-ignis)) |
| Sabrina Ferrias      | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Salazar Bolza        | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Sarian Xadaar        | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |
| Szczepan Myrczek     | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Wawrzyn Rewemis      | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |
| Władawiec Diakon     | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
---
categories: profile
factions: 
owner: public
title: Kaspian Certisarius
---

# {{ page.title }}


# Generated: 



## Fiszki


* 37 lat, advancer, noktianin (adastranin); próbuje być daleko od wszystkich | @ 221022-derelict-okarantis-wejscie
* ENCAO:  -+-00 |Skryty;;Bardzo Ostrożny;;Nie widzi nadziei| VALS: Tradition, Family, Hedonism| DRIVE: Corrupted contagion (coś na K1) | @ 221022-derelict-okarantis-wejscie

### Wątki


historia_darii
salvagerzy_anomalii_kolapsu

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221022-derelict-okarantis-wejscie   | 37 lat, advancer, noktianin (adastranin); próbuje być daleko od wszystkich; |ENCAO: -+-00 |Skryty;;Bardzo Ostrożny;;Nie widzi nadziei| VALS: Tradition, Family, Hedonism| DRIVE: Corrupted contagion (coś na K1)| bardzo ostrożnie podszedł do derelicta, ale nie spodziewał się 'roślinnej anomalii'. Świetny advancer z nanitkami w żyłach. Bierze wszystkie brudne roboty bez narzekania i w sumie prawie nic nie mówi. | 0090-01-03 - 0090-01-14 |
| 221111-niebezpieczna-woda-na-hiyori | wzbudził 'zainteresowanie' Igi, bo flirtuje i erp z Safirą. Ale potem jak było trzeba to świetnie dowodził operacją kosmiczną dekontaminacji Hiyori z niebezpiecznej wody ORAZ manewrował dronami jak doświadczony żołnierz. | 0090-04-19 - 0090-04-23 |
| 221113-ailira-niezalezna-handlarka-woda | został tymczasowym P.O. kapitana ku swej wielkiej żałości. Daria go wrobiła. Dowodził bardzo delikatnie sugestiami ale 'do whatever'. Siedział na zewnątrz Hiyori większość czasu, może, bo Julia na niego polowała a może, bo robił coś innego - wynosił dziwny sprzęt. Gdy dowiedział się o śmierci Ailiry ZAKAZAŁ zgłębiania tematu i był wyjątkowo zdecydowany jak na niego. Czyżby faktycznie był komandosem - advancerem - mordercą? | 0090-04-24 - 0090-04-30 |
| 221221-astralna-flara-i-nowy-komodor | jest dowód, że eks-advancer z Hiyori znajdował się w bazie "duchów" noktiańskich. Nie znaleziono jego ciała, nie znaleziono go wśród pojmanych. Więc Noctis ma jeszcze jedną bazę w okolicy... | 0100-09-09 - 0100-09-12 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 221022-derelict-okarantis-wejscie   | ranny; będzie potrzebował 1 dnia regeneracji na stacji (mimo nanitek) po powrocie do cywilizacji. | 0090-01-14
| 221111-niebezpieczna-woda-na-hiyori | zdradził się przed Ogdenem i Darią ze swoich militarnych manewrów dronami. Wiedzą, że ma coś wspólnego z wojskiem. | 0090-04-23

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Daria Czarnewik      | 4 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda; 221221-astralna-flara-i-nowy-komodor)) |
| Iga Mikikot          | 3 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda)) |
| Jakub Uprzężnik      | 3 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda)) |
| Leo Kasztop          | 3 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda)) |
| Nastia Barbatov      | 3 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda)) |
| Patryk Lapszyn       | 3 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda)) |
| Safira d'Hiyori      | 3 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda)) |
| Ailira Niiris        | 2 | ((221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda)) |
| Ogden Barbatov       | 2 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori)) |
| SCA Hiyori           | 2 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori)) |
| Arianna Verlen       | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| Elena Verlen         | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| Ellarina Samarintael | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| Filip Szukurkor      | 1 | ((221113-ailira-niezalezna-handlarka-woda)) |
| Julia Karnit         | 1 | ((221113-ailira-niezalezna-handlarka-woda)) |
| Kajetan Kircznik     | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| Leszek Kurzmin       | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| Ludwik Trójkadur     | 1 | ((221113-ailira-niezalezna-handlarka-woda)) |
| Maja Samszar         | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| NekroTAI Zarralea    | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| OO Astralna Flara    | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| OO Athamarein        | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| OO Loricatus         | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| Salazar Bolza        | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| Sargon Niiris        | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| Władawiec Diakon     | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
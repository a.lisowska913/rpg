---
categories: profile
factions: 
owner: public
title: Alex Deverien
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230303-the-goose-from-hell          | Streetwise character with first aid experience, noticed the projectile wound on Tobias, and threw food into the cage to capture the goose. Also investigated the mysterious girl. Amazing in maneuevering on damaged buildings. | 0111-10-24 - 0111-10-26 |
| 230331-an-unfortunate-ratnapping    | skilled in stealth and combat, who helps capture rats, confronts the Guardians of Harmony, and retrieves his cat Tobias after a scuffle with a member of Guardians of Harmony. | 0111-11-04 - 0111-11-06 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Carmen Deverien      | 2 | ((230303-the-goose-from-hell; 230331-an-unfortunate-ratnapping)) |
| Julia Kardolin       | 2 | ((230303-the-goose-from-hell; 230331-an-unfortunate-ratnapping)) |
| kot-pacyfikator Tobias | 2 | ((230303-the-goose-from-hell; 230331-an-unfortunate-ratnapping)) |
| Paweł Szprotka       | 2 | ((230303-the-goose-from-hell; 230331-an-unfortunate-ratnapping)) |
| Alicja Trawlis       | 1 | ((230303-the-goose-from-hell)) |
| Radosław Turkamenin  | 1 | ((230331-an-unfortunate-ratnapping)) |
| Teresa Mieralit      | 1 | ((230303-the-goose-from-hell)) |
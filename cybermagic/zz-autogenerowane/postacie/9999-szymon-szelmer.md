---
categories: profile
factions: 
owner: public
title: Szymon Szelmer
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 191120-mafia-na-stacji-gorniczej    | Szaman. Mistrz negocjacji z Orbiterem oraz osoba kalibrująca się z anomalią by móc wyciągnąć Oliwiera i by nic nie wyleciało w powietrze. | 0110-06-14 - 0110-06-25 |
| 191218-kijara-corka-szotaron        | szaman Dorszant; ten łagodny z natury mag skaził się Esuriit by tylko rzucić _death spell_ na wszystkich których w kosmosie dorwałaby Kijara. | 0110-07-04 - 0110-07-07 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Eszara d'Dorszant    | 2 | ((191120-mafia-na-stacji-gorniczej; 191218-kijara-corka-szotaron)) |
| Jaromir Uczkram      | 2 | ((191120-mafia-na-stacji-gorniczej; 191218-kijara-corka-szotaron)) |
| Oliwier Pszteng      | 2 | ((191120-mafia-na-stacji-gorniczej; 191218-kijara-corka-szotaron)) |
| Rafał Kirlat         | 2 | ((191120-mafia-na-stacji-gorniczej; 191218-kijara-corka-szotaron)) |
| Azonia Arris         | 1 | ((191218-kijara-corka-szotaron)) |
| Felicja Taranit      | 1 | ((191218-kijara-corka-szotaron)) |
| Filip Szczatken      | 1 | ((191218-kijara-corka-szotaron)) |
| Gerwazy Kruczkut     | 1 | ((191120-mafia-na-stacji-gorniczej)) |
| Hestia d'Dorszant    | 1 | ((191120-mafia-na-stacji-gorniczej)) |
| Janet Erwon          | 1 | ((191120-mafia-na-stacji-gorniczej)) |
| Kamelia Termit       | 1 | ((191120-mafia-na-stacji-gorniczej)) |
| Kijara d'Esuriit     | 1 | ((191218-kijara-corka-szotaron)) |
| Stefan Ukrand        | 1 | ((191120-mafia-na-stacji-gorniczej)) |
---
categories: profile
factions: 
owner: public
title: Fiona Szarstasz
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230314-brudna-konkurencja-w-arachnoziem | (eks-verlenlandrynka; ma niesamowity talent do wykrywania kłamstwa ale też świetnie kłamie, teraz: guwernantka Karolinusa). W firmie brata odkryła sabotaż i pomogła mu odbudować sytuację. Test słabego ogniwa i zawężenie do Kacpra i Ani. | 0095-06-20 - 0095-06-22 |
| 230328-niepotrzebny-ratunek-mai     | pomogła w odkryciu sytuacji Mai i w tym, że Maja ściemnia. Dawała dobre rady Karolinusowi jak działać w Verlenlandzie. I za całokształt została wycofana od Karolinusa bo ma na niego zły wpływ XD. | 0095-06-30 - 0095-07-02 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 230314-brudna-konkurencja-w-arachnoziem | spalona w rodzinnym miasteczku, acz odzyskała sympatię brata i rodziny z Arachnoziem. | 0095-06-22

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| AJA Szybka Strzała   | 2 | ((230314-brudna-konkurencja-w-arachnoziem; 230328-niepotrzebny-ratunek-mai)) |
| Karolinus Samszar    | 2 | ((230314-brudna-konkurencja-w-arachnoziem; 230328-niepotrzebny-ratunek-mai)) |
| Ania Turabnik        | 1 | ((230314-brudna-konkurencja-w-arachnoziem)) |
| Apollo Verlen        | 1 | ((230328-niepotrzebny-ratunek-mai)) |
| Bonifacy Samszar     | 1 | ((230328-niepotrzebny-ratunek-mai)) |
| Fircjusz Szarstasz   | 1 | ((230314-brudna-konkurencja-w-arachnoziem)) |
| Julita Mopsarin      | 1 | ((230314-brudna-konkurencja-w-arachnoziem)) |
| Kacper Aczramin      | 1 | ((230314-brudna-konkurencja-w-arachnoziem)) |
| Laura Turabnik       | 1 | ((230314-brudna-konkurencja-w-arachnoziem)) |
| Maja Samszar         | 1 | ((230328-niepotrzebny-ratunek-mai)) |
| Romeo Verlen         | 1 | ((230328-niepotrzebny-ratunek-mai)) |
| Viorika Verlen       | 1 | ((230328-niepotrzebny-ratunek-mai)) |
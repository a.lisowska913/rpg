---
categories: profile
factions: 
owner: public
title: Wojciech Namczak
---

# {{ page.title }}


# Generated: 



## Fiszki


* (ten od głupich Verlenów) | @ 230124-kjs-wygrac-za-wszelka-cene
* (ENCAO:  000-+ |Sarkastyczny i złośliwy;;Łatwo wywrzeć na nim wrażenie| VALS: Stimulation, Conformity >> Humility| DRIVE: Mam rację.) | @ 230124-kjs-wygrac-za-wszelka-cene
* (ten od głupich Verlenów) <-- DOSTAŁ WPIERDOL OD ELENY BO MAGIA | @ 230307-kjs-stymulanty-szeptomandry

### Wątki


kajis
mroczna-strona-verlenow

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230124-kjs-wygrac-za-wszelka-cene   | agresywny i nabuzowany chłopak podkochujący się w Emilii; zabolało go, że Apollo udziela jej "prywatnych lekcji" więc powiedział to na głos i wdał się w bitwę z Eleną. Bez magii. Mimo, że Elena ostro walczyła, ale w końcu straciła kontrolę i skrzywdziła go magią. Skończył w szpitalu na 2 tygodnie. | 0095-06-23 - 0095-06-25 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Apollo Verlen        | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Elena Verlen         | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Emilia Lawendowiec   | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Karol Atenuatia      | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| KDN Kajis            | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Lidia Nemert         | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Lucas Oktromin       | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Marta Krissit        | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Serena Krissit       | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Zuzanna Kraczamin    | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Żaneta Krawędź       | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
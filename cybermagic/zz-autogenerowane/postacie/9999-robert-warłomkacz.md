---
categories: profile
factions: 
owner: public
title: Robert Warłomkacz
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230103-protomag-z-mrocznego-wolu    | komodor który chce się przespać z Adelą; dowiedział się dla Adeli, że nikt o Neli i jej działaniach nie wie ale Lena się interesowała jej historią i udostępniła wiedzę Lenie. | 0111-09-14 - 0111-09-17 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Adela Myrias         | 1 | ((230103-protomag-z-mrocznego-wolu)) |
| Juliusz Akramantanis | 1 | ((230103-protomag-z-mrocznego-wolu)) |
| Krzysztof Workisz    | 1 | ((230103-protomag-z-mrocznego-wolu)) |
| Lena Ifirentis       | 1 | ((230103-protomag-z-mrocznego-wolu)) |
| Maja Szewieczak      | 1 | ((230103-protomag-z-mrocznego-wolu)) |
| Marianna Atrain      | 1 | ((230103-protomag-z-mrocznego-wolu)) |
| Nadja Kilmodrian     | 1 | ((230103-protomag-z-mrocznego-wolu)) |
| Nela Kaltaner        | 1 | ((230103-protomag-z-mrocznego-wolu)) |
| OO Mroczny Wół       | 1 | ((230103-protomag-z-mrocznego-wolu)) |
---
categories: profile
factions: 
owner: public
title: Albert Rybowąż
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220525-dzieci-z-arinkarii-odpychaja-piratow | 16, zmodyfikowany drakolicko; podrywa Romkę (bo jest egzotyczna); odpowiedzialny wpierw za ochronę dzieci przy ataku piratów a potem kontrolował śluzy by piraci nie wyszli. Zaatakował wibroostrzem pirata i solidnie wylądował na ścianie. Uczy się by być inżynierem i maintainerem bazy. | 0105-03-07 - 0105-03-08 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Adam Wudrak          | 1 | ((220525-dzieci-z-arinkarii-odpychaja-piratow)) |
| Bartek Wudrak        | 1 | ((220525-dzieci-z-arinkarii-odpychaja-piratow)) |
| Bruno Wesper         | 1 | ((220525-dzieci-z-arinkarii-odpychaja-piratow)) |
| Kaja Czmuch          | 1 | ((220525-dzieci-z-arinkarii-odpychaja-piratow)) |
| Koralina Szprot      | 1 | ((220525-dzieci-z-arinkarii-odpychaja-piratow)) |
| Romana Kundel        | 1 | ((220525-dzieci-z-arinkarii-odpychaja-piratow)) |
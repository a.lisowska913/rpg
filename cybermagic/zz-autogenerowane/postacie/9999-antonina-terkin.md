---
categories: profile
factions: 
owner: public
title: Antonina Terkin
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220512-influencerskie-mikotki-z-talio | medical Talio; silnie i dyskretnie współpracuje z Ognikami przeciwko ochronie Talio i cicho podpowiada Karze, że Jacek jest w skrzydle medycznym i nie chce wyjść. Wściekła na Karę, bo ta wzięła szczura do medical. | 0104-02-24 - 0104-03-01 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Aurus Czarmadon      | 1 | ((220512-influencerskie-mikotki-z-talio)) |
| Elwira Piscernik     | 1 | ((220512-influencerskie-mikotki-z-talio)) |
| Jacek Ożgor          | 1 | ((220512-influencerskie-mikotki-z-talio)) |
| Kamil Czarmadon      | 1 | ((220512-influencerskie-mikotki-z-talio)) |
| Kara Prazdnik        | 1 | ((220512-influencerskie-mikotki-z-talio)) |
| Mirella Czarmadon    | 1 | ((220512-influencerskie-mikotki-z-talio)) |
| Tytus Ramkon         | 1 | ((220512-influencerskie-mikotki-z-talio)) |
---
categories: profile
factions: 
owner: public
title: Wojciech Tuczmowil
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 181220-upiorny-servar               | kustosz cmentarzyska servarów dostrojony do owego cmentarzyska. Chwilowo trafił do więzienia za zbudowanie bojowego servara dla Beaty. | 0109-10-27 - 0109-10-29 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 181220-upiorny-servar               | za zbudowanie terrorystycznego servara trafił do więzienia | 0109-10-29

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Aleksandra Garwen    | 1 | ((181220-upiorny-servar)) |
| Alicja Wielżak       | 1 | ((181220-upiorny-servar)) |
| Artur Śrubek         | 1 | ((181220-upiorny-servar)) |
| Beata Wielinek       | 1 | ((181220-upiorny-servar)) |
| Bogdan Daneb         | 1 | ((181220-upiorny-servar)) |
| Celina Szaczyr       | 1 | ((181220-upiorny-servar)) |
| Gabriel Krajczok     | 1 | ((181220-upiorny-servar)) |
| Jerzy Cieniż         | 1 | ((181220-upiorny-servar)) |
| Michał Wypras        | 1 | ((181220-upiorny-servar)) |
| Patrycja Karzec      | 1 | ((181220-upiorny-servar)) |
| Ryszard Januszewicz  | 1 | ((181220-upiorny-servar)) |
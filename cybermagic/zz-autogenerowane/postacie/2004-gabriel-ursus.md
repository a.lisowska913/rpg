---
categories: profile
factions: 
owner: public
title: Gabriel Ursus
---

# {{ page.title }}


# Read: 

## Kim jest

### Paradoksalny Koncept

Uczeń terminusa, który bardzo lubi sprzęt ciężki i TAI - ale elektronika go zawsze zawodzi. Charyzmatyczny arystokrata, który zwiał przed swoją eks by zostać terminusem. Bezbłędnie znajduje prawdę - acz to, co znajduje sprawia mu ogromne zgryzoty.

### Motto

"Każdy ma swoje miejsce w którym osiągnie mistrzostwo - i pomogę Ci znaleźć Twoje."

## Mechanika

### Czym osiąga sukcesy (3)

* ATUT: Szperacz. Potrafi wszystko wywęszyć czy wykryć, nawet sekrety. Wspiera się magią katalityczną - świadomie lub instynktownie. Jakoś zawsze dojdzie do prawdy.
* ATUT: Naturalnie przejmuje dowodzenie i ma posłuch u innych; wyjątkowo charyzmatyczny. To, że jest arystokratą Aurum jedynie pomaga.
* SŁABA: Nie radzi sobie dobrze z pilotażem pojazdów. Owszem, potrafi - ale jakoś ZAWSZE ma pecha i jeśli on pilotuje to stanie się coś złego. Dotyczy też servarów.
* SŁABA: Jest trochę zbyt uczciwy i honorowy - nie radzi sobie z intrygami i z kłamaniem. Jeśli ma udawać kogoś kim nie jest, to mu to strasznie nie wychodzi.

### O co walczy (3)

* ZA: Porządek i harmonia. Każdy ma swoje miejsce w społeczeństwie, nikt nie powinien się obawiać zgłosić problemu wyżej.
* ZA: Zrozumieć, co sprawia, że tak kochana przez niego technologia go "zdradza" i regularnie zawodzi. To jego wstydliwy sekret.
* VS: Degeneraci z Aurum. Jedną rzeczą jest zdrowa arystokracja, ale plugastwo należy wytępić. Jest terminusem, by chronić kuzynkę i ród.
* VS: Jeśli jesteś w stanie coś zrobić, masz obowiązek to zrobić. Nie możesz stać z boku. Acz długoterminowe cele są ważniejsze niż krótkoterminowe.

### Znaczące Czyny (3)

* Nie chcąc by komukolwiek stała się krzywda poszedł sam uratować swoją eks - dostał solidny wpiernicz, ale uratował jej honor i godność.
* Wykrył z niemałej odległości, że Sabina Kazitan włada zakazanymi rytuałami. Tylko z uwagi na rozkaz nie powiedział o tym nikomu innemu.
* Miał możliwość zyskania władzy i wpływów jeśli dołączy do Lemurczaka. Odmówił - zasady są ważniejsze. Plus, Lilia powinna rządzić, nie on.

### Kluczowe Zasoby (3)

* COŚ: Najlepszej klasy uzbrojenie i sprzęt elektroniczny. Taki, który go zawodzi. Ale - jest co najmniej o klasę lepszy niż alternatywny.
* KTOŚ: Lilia, jego ulubiona (i nieco dzika) kuzynka, która wie absolutnie wszystko o funkcjonowaniu rodów Aurum i skupiła się na rozbudowie majątku.
* WIEM: Niekwestionowany ekspert (teoretyk) od różnego rodzaju broni i pojazdów. Naprawdę, kocha ten temat. Po prostu mu to nie wychodzi.
* OPINIA: Sztywny, acz można na nim polegać. Zrobi to, co jest słuszne, nieważne ile osób przy tym nie ucierpi.

## Inne

### Wygląd

.

### Coś Więcej

.


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190917-zagubiony-efemerydyta        | katalista wspierający Pięknotkę. Dużo biega i zbiera informacji - z Fortu Mikado lub innych miejsc. | 0110-06-17 - 0110-06-18 |
| 190726-bardzo-niebezpieczne-skladowisko | kuzyn Lilii, terminus-szperacz; idealista. Doskonale zauważa rzeczy (ale Amandy - nie). Dokładny, nieco biurokratyczny i trochę naiwny. | 0110-06-22 - 0110-06-24 |
| 190804-niespodziewany-wplyw-aidy    | dowodził terminuskimi praktykantami ze średnią kompetencją do momentu pojawienia się Pięknotki. | 0110-06-26 - 0110-06-28 |
| 200417-nawolywanie-trzesawiska      | nienawidzi Sabiny Kazitan; wykrył dla Pięknotki jej sekret - płynnie kontroluje czarną magię. Przekonany, by ukryć ten sekret przed wszystkimi poza Karlą. | 0110-08-08 - 0110-08-10 |
| 200418-wojna-trzesawiska            | walczył w obronie Czemerty przed bioformami Trzęsawiska; został ranny jako pilot awiana. | 0110-08-12 - 0110-08-17 |
| 200425-inflitrator-poluje-na-tai-minerwy | przywołany do pomocy przez Pięknotkę by przenieść uszkodzonego elektronicznego DJa do Erwina; tam odkryli obecność Infiltratora Iniekcyjnego Aurum. | 0110-08-20 - 0110-08-22 |
| 200509-rekin-z-aurum-i-fortifarma   | źródło informacji o arystokracji Aurum i jak myślą o "prowincji". Jako katalista, pomógł Pięknotce znaleźć miejsce związane z Krwawym Potworem. Załamany losem Tadeusza Tessalona. | 0110-08-26 - 0110-08-31 |
| 200510-tajna-baza-orbitera          | zauważył różnice między aurami, spotkał się z Natalią przy Fortifarmie Irrydii i został sklupany przez Kallistę. Stracił o tym pamięć i skończył w Szpitalu Terminuskim. | 0110-09-03 - 0110-09-07 |
| 200616-bardzo-straszna-mysz         | arystokrata, który bardzo próbował rozbroić mysz Esuriit zanim ktokolwiek ucierpi. Ściągnął Laurę i katalitycznie chronił klatkę. Laura nad nim dominuje na akcji. | 0110-09-14 - 0110-09-17 |
| 201006-dezinhibitor-dla-sabiny      | skonfliktowany między zemstą na Sabinie Kazitan a zrobienie tego co należy. W końcu wybrał ochronę Myrczka i opieprzenie Rekinów nad zemstę nad Sabiną. | 0110-10-03 - 0110-10-05 |
| 201011-narodziny-paladynki-saitaera | nie potrafił zostawić Sabiny Kazitan samej w spokoju; Pięknotka zmusiła go do porzucenia wszelkich vendett. Rody Ursus i Kazitan nie są już w stanie wojny. | 0110-10-08 - 0110-10-09 |
| 201020-przygoda-randka-i-porwanie   | ściągnął Ulę by dać jej kontakty z Aurum jako "dziewczyna do porwania", załatwił papiery z Tukanem (!?), przedłożył Aurum i relacje nad Pustogor - ale stał dzielnie przeciw efemerydzie i nie zostawił Uli samej; nie uciekł przed Tymonem. | 0110-10-21 - 0110-10-23 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 200509-rekin-z-aurum-i-fortifarma   | eks Natalii Tessalon. Lubi ją i jej brata. Niechętnie walczy / działa przeciwko nim. Kiedyś ją zdradził i z nim zerwała, potem uciekł "na prowincję". | 0110-08-31
| 201006-dezinhibitor-dla-sabiny      | Lorena Gwozdnik, Rekin, zakochała się w nim na zabój z uwagi na dezinhibitor. Gorzej niż miłość, to "oddanie". | 0110-10-05
| 201011-narodziny-paladynki-saitaera | zrezygnował z walki przeciw Sabinie Kazitan. Ani on ani jego ród nie będą z nią walczyć i są przed nią zabezpieczeni. | 0110-10-09
| 201020-przygoda-randka-i-porwanie   | poważnie nadwątlił zaufanie Pięknotki, Tymona i dużej ilości terminusów Pustogoru. Przedłożył interesy Aurum - dwóch smarkaczy - nad dobro regionu. | 0110-10-23

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Pięknotka Diakon     | 9 | ((190726-bardzo-niebezpieczne-skladowisko; 190804-niespodziewany-wplyw-aidy; 190917-zagubiony-efemerydyta; 200417-nawolywanie-trzesawiska; 200418-wojna-trzesawiska; 200425-inflitrator-poluje-na-tai-minerwy; 200509-rekin-z-aurum-i-fortifarma; 200510-tajna-baza-orbitera; 201011-narodziny-paladynki-saitaera)) |
| Sabina Kazitan       | 6 | ((200417-nawolywanie-trzesawiska; 200418-wojna-trzesawiska; 200509-rekin-z-aurum-i-fortifarma; 200510-tajna-baza-orbitera; 201006-dezinhibitor-dla-sabiny; 201011-narodziny-paladynki-saitaera)) |
| Ignacy Myrczek       | 4 | ((200417-nawolywanie-trzesawiska; 200418-wojna-trzesawiska; 200510-tajna-baza-orbitera; 201006-dezinhibitor-dla-sabiny)) |
| Laura Tesinik        | 4 | ((200425-inflitrator-poluje-na-tai-minerwy; 200510-tajna-baza-orbitera; 200616-bardzo-straszna-mysz; 201006-dezinhibitor-dla-sabiny)) |
| Amanda Kajrat        | 2 | ((190726-bardzo-niebezpieczne-skladowisko; 190804-niespodziewany-wplyw-aidy)) |
| Artur Kołczond       | 2 | ((200509-rekin-z-aurum-i-fortifarma; 200510-tajna-baza-orbitera)) |
| Erwin Galilien       | 2 | ((200425-inflitrator-poluje-na-tai-minerwy; 200509-rekin-z-aurum-i-fortifarma)) |
| Minerwa Metalia      | 2 | ((200425-inflitrator-poluje-na-tai-minerwy; 201011-narodziny-paladynki-saitaera)) |
| Napoleon Bankierz    | 2 | ((200417-nawolywanie-trzesawiska; 201006-dezinhibitor-dla-sabiny)) |
| Natalia Tessalon     | 2 | ((200509-rekin-z-aurum-i-fortifarma; 200510-tajna-baza-orbitera)) |
| Olaf Zuchwały        | 2 | ((190917-zagubiony-efemerydyta; 200417-nawolywanie-trzesawiska)) |
| Saitaer              | 2 | ((190804-niespodziewany-wplyw-aidy; 201011-narodziny-paladynki-saitaera)) |
| Strażniczka Alair    | 2 | ((200417-nawolywanie-trzesawiska; 200425-inflitrator-poluje-na-tai-minerwy)) |
| Tomasz Tukan         | 2 | ((200425-inflitrator-poluje-na-tai-minerwy; 201020-przygoda-randka-i-porwanie)) |
| Tymon Grubosz        | 2 | ((200510-tajna-baza-orbitera; 201020-przygoda-randka-i-porwanie)) |
| Agaton Ociegor       | 1 | ((201011-narodziny-paladynki-saitaera)) |
| Aleksander Muniakiewicz | 1 | ((201011-narodziny-paladynki-saitaera)) |
| Alina Anakonda       | 1 | ((200510-tajna-baza-orbitera)) |
| Aranea Diakon        | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Artur Michasiewicz   | 1 | ((200509-rekin-z-aurum-i-fortifarma)) |
| Cezary Alentik       | 1 | ((190726-bardzo-niebezpieczne-skladowisko)) |
| Daniel Sowiński      | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Diana Lauris         | 1 | ((200616-bardzo-straszna-mysz)) |
| Ernest Kajrat        | 1 | ((190726-bardzo-niebezpieczne-skladowisko)) |
| Franek Bulterier     | 1 | ((200616-bardzo-straszna-mysz)) |
| Henryk Sowiński      | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Henryk Wkrąż         | 1 | ((200616-bardzo-straszna-mysz)) |
| Jan Uszczar          | 1 | ((190917-zagubiony-efemerydyta)) |
| Julia Morwisz        | 1 | ((190804-niespodziewany-wplyw-aidy)) |
| Justynian Diakon     | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Kacper Bankierz      | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Kallista Exolon      | 1 | ((200510-tajna-baza-orbitera)) |
| Karla Mrozik         | 1 | ((200418-wojna-trzesawiska)) |
| Karolina Erenit      | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Ksenia Kirallen      | 1 | ((200616-bardzo-straszna-mysz)) |
| Laurencjusz Sorbian  | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Liliana Bankierz     | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Lorena Gwozdnik      | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Marek Puszczok       | 1 | ((190917-zagubiony-efemerydyta)) |
| Mariusz Trzewń       | 1 | ((200510-tajna-baza-orbitera)) |
| Matylda Sęk          | 1 | ((200616-bardzo-straszna-mysz)) |
| Mirela Orion         | 1 | ((190726-bardzo-niebezpieczne-skladowisko)) |
| Ossidia Saitis       | 1 | ((190804-niespodziewany-wplyw-aidy)) |
| Rafał Kumczek        | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Robert Pakiszon      | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Robinson Porzecznik  | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Roland Grzymość      | 1 | ((190726-bardzo-niebezpieczne-skladowisko)) |
| Tadeusz Tessalon     | 1 | ((200509-rekin-z-aurum-i-fortifarma)) |
| Talarand d'Irrydius  | 1 | ((200510-tajna-baza-orbitera)) |
| Teresa Mieralit      | 1 | ((200510-tajna-baza-orbitera)) |
| Triana Porzecznik    | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Urszula Miłkowicz    | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Wiktor Satarail      | 1 | ((200418-wojna-trzesawiska)) |
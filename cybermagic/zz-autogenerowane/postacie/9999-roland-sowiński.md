---
categories: profile
factions: 
owner: public
title: Roland Sowiński
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 211212-nie-taki-bezkarny-mlody-tien | 16 lat. Próbuje być paladynem, choć trochę nie ma serca. Ma relację z Klarą Gwozdnik. Słucha się poleceń porywaczy, uspokaja i stabilizuje, ale jak tylko opuścili miejsce ataku to organizuje grupę pościgową. Gdy nie wyszło dogonienie, użyli dzikiej sentisieci do zatrzymania choć części napastników i ukochanej Klary. Popisuje się jak cholera. | 0101-04-10 - 0101-04-13 |
| 211120-glizda-ktora-leczy           | tak bardzo chciał pomóc wszystkim i pokonać mafię, że dał się wrobić Sabinie Kazitan i wkręcił się w Esuriit. Jego Skażenie doprowadziło do eskalacji działań z mafią i w konsekwencji wysłania tu Sensacjusza i Stelli. 23 lata. | 0108-04-03 - 0108-04-14 |
| 211229-szpieg-szpiegowi-szpiegiem   | szpieg z Aurum jako boytoy Estelli Evans z Valentiny; fatalnie się skrada czy włamuje, ale jest szybki, groźny w walce i jest mistrzem aktorstwa. Zarządzał ryzykiem, walczył z intruzami i ogólnie tak zamieszał, że nikt o nic go nie podejrzewa. Zdobył informacje, których szukał. | 0111-01-03 - 0111-01-07 |
| 220611-nie-roland-i-niewolnicy-na-valentinie | udając swojego agenta wzbudził strach w tien Ewelinie Sowińskiej na Valentinie i odstraszył ją od handlu niewolnikami, przepinając na domy publiczne. Wykrył Anomalię Krwi która mściła się za krzywdę niewolników, po czym ją wzmocnił i odłożył na Valentinę - by wrobić szlachetne części Orbitera że to oni. Nikt nie wie że tam był osobiście. | 0111-03-10 - 0111-03-12 |
| 210512-ewakuacja-z-serenit          | uczestniczył w akcji kombinowanej Eternia - Aurum do zrobienia floty i badania anomalii kosmicznych. Uratowany przez Infernię z Falołamacza zanim stała mu się terminalna krzywda. | 0111-09-25 - 0111-10-04 |
| 210609-sekrety-kariatydy            | oczarowany przez Ariannę i wdzięczny jej za uratowanie z Odłamka Serenita zdecydował się z nią zaręczyć. Ale sfinansował jej wyprawę do Anomalii Kolapsu. Kiepskim sprzętem... | 0111-11-05 - 0111-11-08 |
| 210616-nieudana-infiltracja-inferni | koniecznie chciał lecieć Infernią na SPECJALNĄ MISJĘ. Jak się okazało (Kamil <3) jak jest niebezpieczna, już nie chciał. Zwolnił guwernantkę (Flawię), bo Arianna zrobiła scenę że ta niby go okradła. | 0111-11-22 - 0111-11-27 |
| 210922-ostatnia-akcja-bohaterki     | NIC NIE ZROBIŁ. Ale Arianna powiedziała Olgierdowi, że się do niej zaleca i Olgierd zrobi mu wpierdol. | 0111-12-19 - 0112-01-03 |
| 211117-porwany-trismegistos         | goguś i dandys, który jednak dziwnie od razu zorientował się, że mają do czynienia z wolną TAI (Kalira). Prosił Ariannę, by ta koniecznie zniszczyła Kalirę. | 0112-02-09 - 0112-02-11 |
| 211124-prototypowa-nereida-natalii  | chciał się wprosić na Infernię, ale Arianna nie chciała. Wsadził Inferni kilku kompetentnych ludzi, co wszystkich zdziwiło (kompetentnych czemu? O_O). Protekcjonalny wobec Arianny, "małej buntowniczki" jak się mu przyznała. | 0112-02-14 - 0112-02-18 |
| 211215-sklejanie-inferni-do-kupy    | NIEOBECNY, ale wprowadził dwie kategorie agentów na Infernię. Okazuje się, że zewnętrzna kategoria (leszcze) mają być "typowi", ale wewnętrzna kategoria - dokładna analiza czego Infernia chce się dowiedzieć, systemów Inferni itp. | 0112-02-21 - 0112-02-23 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 211212-nie-taki-bezkarny-mlody-tien | zmuszony do zerwania tajnego związku z Klarą Gwozdnik przez centralę Sowińskich. Jako posłuszny paladyn, wypełnił rozkaz. Ma w końcu 16 lat. | 0101-04-13
| 211212-nie-taki-bezkarny-mlody-tien | narobił sobie wrogów w Aurum we WSZYSTKICH rodach, łącznie ze swoim - odrzucił tych, co uznał za niekompetentnych i też nie dogonił porywaczy. Acz uratował 3 magów. | 0101-04-13
| 211120-glizda-ktora-leczy           | Skażenie Esuriit. Zmienia to jego Wzór trochę, nawet po wyleczeniu i regeneracji. Powrót do Aurum. | 0108-04-14
| 211229-szpieg-szpiegowi-szpiegiem   | zainteresowanie anomaliami w wyniku tego, co znalazł w zdobytych materiałach | 0111-01-07
| 220611-nie-roland-i-niewolnicy-na-valentinie | na tym etapie ma już zaawansowany aparat szpiegów i fałszywych Masek - tożsamości, przedmiotów, niewielkich jednostek gdzie może być kimś innym. | 0111-03-12
| 220611-nie-roland-i-niewolnicy-na-valentinie | modus operandi: wchodzi jako jeden ze swoich agentów. Raczej unika powiedzenia że jest Rolandem. | 0111-03-12
| 220611-nie-roland-i-niewolnicy-na-valentinie | kanał spedycyjny proszku z błękitnej żaby Aurum, znajomości na Valentinie i Ewelina która z nim współpracuje. | 0111-03-12
| 220706-etaur-dziwnie-wazny-wezel    | wpakował na Infernię kilku kompetentnych i brzydkich mechaników. Nie wie, że Klaudia i Eustachy wiedzą, że to jego ludzie. | 0112-07-17

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 7 | ((210512-ewakuacja-z-serenit; 210609-sekrety-kariatydy; 210616-nieudana-infiltracja-inferni; 210922-ostatnia-akcja-bohaterki; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211215-sklejanie-inferni-do-kupy)) |
| Elena Verlen         | 6 | ((210512-ewakuacja-z-serenit; 210616-nieudana-infiltracja-inferni; 210922-ostatnia-akcja-bohaterki; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211215-sklejanie-inferni-do-kupy)) |
| Eustachy Korkoran    | 6 | ((210512-ewakuacja-z-serenit; 210609-sekrety-kariatydy; 210616-nieudana-infiltracja-inferni; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211215-sklejanie-inferni-do-kupy)) |
| Klaudia Stryk        | 5 | ((210512-ewakuacja-z-serenit; 210616-nieudana-infiltracja-inferni; 210922-ostatnia-akcja-bohaterki; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii)) |
| Leona Astrienko      | 5 | ((210616-nieudana-infiltracja-inferni; 210922-ostatnia-akcja-bohaterki; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211215-sklejanie-inferni-do-kupy)) |
| Izabela Zarantel     | 4 | ((210616-nieudana-infiltracja-inferni; 210922-ostatnia-akcja-bohaterki; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii)) |
| Maria Naavas         | 3 | ((211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211215-sklejanie-inferni-do-kupy)) |
| OO Infernia          | 3 | ((210512-ewakuacja-z-serenit; 211124-prototypowa-nereida-natalii; 211215-sklejanie-inferni-do-kupy)) |
| Adam Szarjan         | 2 | ((211124-prototypowa-nereida-natalii; 211215-sklejanie-inferni-do-kupy)) |
| Kamil Lyraczek       | 2 | ((210616-nieudana-infiltracja-inferni; 211215-sklejanie-inferni-do-kupy)) |
| Klara Gwozdnik       | 2 | ((211117-porwany-trismegistos; 211212-nie-taki-bezkarny-mlody-tien)) |
| Marian Tosen         | 2 | ((210609-sekrety-kariatydy; 210616-nieudana-infiltracja-inferni)) |
| Martyn Hiwasser      | 2 | ((210512-ewakuacja-z-serenit; 210922-ostatnia-akcja-bohaterki)) |
| Aida Serenit         | 1 | ((210512-ewakuacja-z-serenit)) |
| AK Serenit           | 1 | ((210512-ewakuacja-z-serenit)) |
| Alojzy Lemurczak     | 1 | ((211212-nie-taki-bezkarny-mlody-tien)) |
| Amanda Kajrat        | 1 | ((211120-glizda-ktora-leczy)) |
| Amelia Sowińska      | 1 | ((211120-glizda-ktora-leczy)) |
| Antoni Kramer        | 1 | ((210922-ostatnia-akcja-bohaterki)) |
| Arkadia Verlen       | 1 | ((211124-prototypowa-nereida-natalii)) |
| Berenika Roldan      | 1 | ((211229-szpieg-szpiegowi-szpiegiem)) |
| Bogdan Anatael       | 1 | ((210512-ewakuacja-z-serenit)) |
| Diana d'Infernia     | 1 | ((211215-sklejanie-inferni-do-kupy)) |
| Ernest Kajrat        | 1 | ((211120-glizda-ktora-leczy)) |
| Estella Evans        | 1 | ((211229-szpieg-szpiegowi-szpiegiem)) |
| Ewelina Sowińska     | 1 | ((220611-nie-roland-i-niewolnicy-na-valentinie)) |
| Feliks Keksik        | 1 | ((211120-glizda-ktora-leczy)) |
| Flawia Blakenbauer   | 1 | ((210616-nieudana-infiltracja-inferni)) |
| Jamon Korab          | 1 | ((211117-porwany-trismegistos)) |
| Jolanta Kopiec       | 1 | ((210922-ostatnia-akcja-bohaterki)) |
| Justynian Diakon     | 1 | ((211120-glizda-ktora-leczy)) |
| Kacper Bankierz      | 1 | ((211120-glizda-ktora-leczy)) |
| Kalira d'Trismegistos | 1 | ((211117-porwany-trismegistos)) |
| Karol Pustak         | 1 | ((211120-glizda-ktora-leczy)) |
| Kasandra Destrukcja Diakon | 1 | ((211215-sklejanie-inferni-do-kupy)) |
| Mira Anastel         | 1 | ((211117-porwany-trismegistos)) |
| Natalia Aradin       | 1 | ((211124-prototypowa-nereida-natalii)) |
| Nikczemniczka Diakon | 1 | ((211212-nie-taki-bezkarny-mlody-tien)) |
| OE Falołamacz        | 1 | ((210512-ewakuacja-z-serenit)) |
| Olgierd Drongon      | 1 | ((210922-ostatnia-akcja-bohaterki)) |
| Oliwia Lemurczak     | 1 | ((211120-glizda-ktora-leczy)) |
| OO Kariatyda         | 1 | ((210609-sekrety-kariatydy)) |
| OO Opresor           | 1 | ((210616-nieudana-infiltracja-inferni)) |
| OO Tivr              | 1 | ((211117-porwany-trismegistos)) |
| OO Żelazko           | 1 | ((210922-ostatnia-akcja-bohaterki)) |
| Rafael Galwarn       | 1 | ((210609-sekrety-kariatydy)) |
| Sabina Kazitan       | 1 | ((211120-glizda-ktora-leczy)) |
| SC Trismegistos      | 1 | ((211117-porwany-trismegistos)) |
| Sensacjusz Diakon    | 1 | ((211120-glizda-ktora-leczy)) |
| Stella Sowińska      | 1 | ((211120-glizda-ktora-leczy)) |
| Wawrzyn Rewemis      | 1 | ((211124-prototypowa-nereida-natalii)) |
| Zygfryd Maus         | 1 | ((211117-porwany-trismegistos)) |
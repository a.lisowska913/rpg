---
categories: profile
factions: 
owner: public
title: Baltazar Rączniak
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190208-herbata-grzyby-i-mimik       | terminus lokalny; coś tam zawalił w przeszłości i musi zajmować się tym miejscem. Nieszczęśliwy, nie lubi "Centrali" w Pustogorze. | 0110-03-03 - 0110-03-05 |
| 190213-wygasniecie-starego-autosenta | tchórz a nie terminus. Ale w sumie chciał wysadzić autosenta zakazaną bronią. Opóźnił zespół i... skończył z orderem | 0110-03-07 - 0110-03-08 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Antoni Kotomin       | 2 | ((190208-herbata-grzyby-i-mimik; 190213-wygasniecie-starego-autosenta)) |
| Dariusz Bankierz     | 2 | ((190208-herbata-grzyby-i-mimik; 190213-wygasniecie-starego-autosenta)) |
| Jadwiga Pszarnik     | 2 | ((190208-herbata-grzyby-i-mimik; 190213-wygasniecie-starego-autosenta)) |
| Pięknotka Diakon     | 2 | ((190208-herbata-grzyby-i-mimik; 190213-wygasniecie-starego-autosenta)) |
| Almeda Literna       | 1 | ((190208-herbata-grzyby-i-mimik)) |
| Atena Sowińska       | 1 | ((190213-wygasniecie-starego-autosenta)) |
| Rafał Bobowiec       | 1 | ((190213-wygasniecie-starego-autosenta)) |
| Stach Sosnowiecki    | 1 | ((190208-herbata-grzyby-i-mimik)) |
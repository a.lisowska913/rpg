---
categories: profile
factions: 
owner: public
title: Kamil Lemurczak
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 191113-jeden-problem-dwie-rodziny   | przybył na wezwanie swojego oddanego Karola znęcać się nad Sabiną Kazitan. Prawie mu się udało - tyle, że skończył w Aleksandrii. | 0110-09-26 - 0110-10-01 |
| 191126-smierc-aleksandrii           | zachowywał się jak dobry człowiek przez wpływ Aleksandrii. Doprowadził do Skażenia Aleksandrii; wyciągnęli go po jej zniszczeniu. Uszkodzenie psychologiczne. | 0110-10-06 - 0110-10-09 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 191113-jeden-problem-dwie-rodziny   | zniewolony przez Aleksandrię, stał się wiecznie szczęśliwym agentem Amelii Mirzant | 0110-10-01
| 191126-smierc-aleksandrii           | poważnie uszkodzony psychologicznie przez Aleksandrię. Aktualny stan mentalny / popyt na Esuriit nieznany. | 0110-10-09

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Amelia Mirzant       | 2 | ((191113-jeden-problem-dwie-rodziny; 191126-smierc-aleksandrii)) |
| Teresa Marszalnik    | 2 | ((191113-jeden-problem-dwie-rodziny; 191126-smierc-aleksandrii)) |
| Karol Kszatniak      | 1 | ((191113-jeden-problem-dwie-rodziny)) |
| Kasjopea Maus        | 1 | ((191126-smierc-aleksandrii)) |
| Klara Baszcz         | 1 | ((191113-jeden-problem-dwie-rodziny)) |
| Leszek Baszcz        | 1 | ((191113-jeden-problem-dwie-rodziny)) |
| Ola d'Amelia         | 1 | ((191126-smierc-aleksandrii)) |
| Paweł Kukułnik       | 1 | ((191113-jeden-problem-dwie-rodziny)) |
| Sabina Kazitan       | 1 | ((191113-jeden-problem-dwie-rodziny)) |
| Sebastian Kuralsz    | 1 | ((191126-smierc-aleksandrii)) |
---
categories: profile
factions: 
owner: public
title: Ludmiła Zegarzec
---

# {{ page.title }}


# Generated: 



## Fiszki


* starsza siostra Amary | @ 230516-karolinka-raciczki-zemsty-verlenow
* ENCAO:  00+-+ |Egocentryczny;;Oportunista;;Przewidujący, trudny do zaskoczenia| VALS: Stimulation, Power >> Humility| DRIVE: Ferengi | @ 230516-karolinka-raciczki-zemsty-verlenow
* styl: przedsiębiorcza, zna swoje prawa | @ 230516-karolinka-raciczki-zemsty-verlenow
* uwielbia hotel i teren kwiatów i menhirów | @ 230516-karolinka-raciczki-zemsty-verlenow
* typowa reakcja: "wszystko dlatego, bo nie próbujesz zmaksymalizować korzyści" | @ 230516-karolinka-raciczki-zemsty-verlenow

### Wątki


waśnie-samszar-verlen
waśnie-blakenbauer-verlen

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230516-karolinka-raciczki-zemsty-verlenow | główna właścicielka hotelu Odpoczynek Pszczół. Sprzedała Karolinusowi wyszywaną świnię, ściągnęła tu dziennikarza i wycieczki... she's milking the glukszwajn to extremes. | 0095-07-29 - 0095-07-31 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Aleksander Samszar   | 1 | ((230516-karolinka-raciczki-zemsty-verlenow)) |
| Amara Zegarzec       | 1 | ((230516-karolinka-raciczki-zemsty-verlenow)) |
| Elena Samszar        | 1 | ((230516-karolinka-raciczki-zemsty-verlenow)) |
| Franciszek Chartowiec | 1 | ((230516-karolinka-raciczki-zemsty-verlenow)) |
| Karolinus Samszar    | 1 | ((230516-karolinka-raciczki-zemsty-verlenow)) |
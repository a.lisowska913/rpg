---
categories: profile
factions: 
owner: public
title: Alicja Kłębek
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 180724-skorpiony-spod-ziemi         | mieszkanka Żarni; kierowniczka sklepu. Kiedyś zabiła przypadkiem brata i doprowadziła do Skorpipedów. Jakub kazał jej się przyznać magią. | 0109-08-23 - 0109-08-25 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 180724-skorpiony-spod-ziemi         | wypada na margines społeczeństwa w Żarni z uwagi na zabicie brata ORAZ przyznanie się do tego. | 0109-08-25

## Plany


| Opowieść | Plan | Końcowa data |
| ---- | ---- | ---- |
| 180724-skorpiony-spod-ziemi         | znaleźć sposób by wrócić do społeczności, a na pewno by nie stracić dostępu do sklepu | 0109-08-25

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Bronisława Strzelczyk | 1 | ((180724-skorpiony-spod-ziemi)) |
| Jakub Wirus          | 1 | ((180724-skorpiony-spod-ziemi)) |
| Olga Myszeczka       | 1 | ((180724-skorpiony-spod-ziemi)) |
| Roman Kłębek         | 1 | ((180724-skorpiony-spod-ziemi)) |
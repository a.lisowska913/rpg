---
categories: profile
factions: 
owner: public
title: Teresa Marszalnik
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190817-kwiaty-w-sluzbie-puryfikacji |  | 0110-06-30 - 0110-07-05 |
| 191113-jeden-problem-dwie-rodziny   | manipulatorka najwyższej klasy. To ona doprowadziła do tego, że Sabina przybyła, Karol ściągnął Lemurczaka a Amelia chciała Lemurczaka uwięzić Aleksandrią. | 0110-09-26 - 0110-10-01 |
| 191126-smierc-aleksandrii           | wyplątała się raz na zawsze z problemów z Kamilem Lemurczakiem i Skażoną Aleksandrią. A przynajmniej, tak na to wygląda. Nic nie da się jej przypiąć. | 0110-10-06 - 0110-10-09 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 191113-jeden-problem-dwie-rodziny   | uwolniona od Kamila Lemurczaka. To, że dalej ma władzę nic nie zmienia - Aleksandria nie zrobi jej krzywdy. | 0110-10-01

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Amelia Mirzant       | 2 | ((191113-jeden-problem-dwie-rodziny; 191126-smierc-aleksandrii)) |
| Kamil Lemurczak      | 2 | ((191113-jeden-problem-dwie-rodziny; 191126-smierc-aleksandrii)) |
| Erwin Galilien       | 1 | ((190817-kwiaty-w-sluzbie-puryfikacji)) |
| Karol Kszatniak      | 1 | ((191113-jeden-problem-dwie-rodziny)) |
| Kasjopea Maus        | 1 | ((191126-smierc-aleksandrii)) |
| Klara Baszcz         | 1 | ((191113-jeden-problem-dwie-rodziny)) |
| Kornel Garn          | 1 | ((190817-kwiaty-w-sluzbie-puryfikacji)) |
| Leszek Baszcz        | 1 | ((191113-jeden-problem-dwie-rodziny)) |
| Nataniel Marszalnik  | 1 | ((190817-kwiaty-w-sluzbie-puryfikacji)) |
| Ola d'Amelia         | 1 | ((191126-smierc-aleksandrii)) |
| Paweł Kukułnik       | 1 | ((191113-jeden-problem-dwie-rodziny)) |
| Pięknotka Diakon     | 1 | ((190817-kwiaty-w-sluzbie-puryfikacji)) |
| Sabina Kazitan       | 1 | ((191113-jeden-problem-dwie-rodziny)) |
| Sebastian Kuralsz    | 1 | ((191126-smierc-aleksandrii)) |
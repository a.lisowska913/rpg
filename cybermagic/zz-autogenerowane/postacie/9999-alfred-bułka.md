---
categories: profile
factions: 
owner: public
title: Alfred Bułka
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190115-skazony-pnaczoszpon          | służący / opiekun Kornela, który próbował go chronić przed atakiem bestii. Wysłany na bagno, dowiedział się trochę i się zaraził. | 0110-01-06 - 0110-01-08 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Alan Bartozol        | 1 | ((190115-skazony-pnaczoszpon)) |
| Kornel Szczepanik    | 1 | ((190115-skazony-pnaczoszpon)) |
| Kornelia Weiner      | 1 | ((190115-skazony-pnaczoszpon)) |
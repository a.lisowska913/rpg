---
categories: profile
factions: 
owner: public
title: OO Welgat
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200722-wielki-kosmiczny-romans      | Statek zwiadowczy wspomagający, krótkozasięgowy; detektor koloidowców. Silne sensory. Nie poleci daleko bez wsparcia floty. Ma najbardziej romantyczną i rozplotkowaną załogę we flocie. | 0110-11-12 - 0110-11-15 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Antoni Kramer        | 1 | ((200722-wielki-kosmiczny-romans)) |
| Arianna Verlen       | 1 | ((200722-wielki-kosmiczny-romans)) |
| Damian Orion         | 1 | ((200722-wielki-kosmiczny-romans)) |
| Elena Verlen         | 1 | ((200722-wielki-kosmiczny-romans)) |
| Eustachy Korkoran    | 1 | ((200722-wielki-kosmiczny-romans)) |
| Julian Muszel        | 1 | ((200722-wielki-kosmiczny-romans)) |
| Konrad Wolczątek     | 1 | ((200722-wielki-kosmiczny-romans)) |
| Leona Astrienko      | 1 | ((200722-wielki-kosmiczny-romans)) |
| Olgierd Drongon      | 1 | ((200722-wielki-kosmiczny-romans)) |
| OO Żelazko           | 1 | ((200722-wielki-kosmiczny-romans)) |
| Tadeusz Ursus        | 1 | ((200722-wielki-kosmiczny-romans)) |
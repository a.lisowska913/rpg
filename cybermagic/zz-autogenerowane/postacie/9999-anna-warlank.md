---
categories: profile
factions: 
owner: public
title: Anna Warlank
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190912-rexpapier-i-wloknin          | szefowa w Rexpapier ponad Dariuszem; mści się na Włókninie, bo ojciec Dariusza doprowadził do szybszej śmierci ojca Anny gdy pracowali nad Włókninem. Mastermind. | 0110-06-17 - 0110-06-21 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Dariusz Kuromin      | 1 | ((190912-rexpapier-i-wloknin)) |
| Eleonora Rdeść       | 1 | ((190912-rexpapier-i-wloknin)) |
| Jarek Gułanczak      | 1 | ((190912-rexpapier-i-wloknin)) |
| Kasjopea Maus        | 1 | ((190912-rexpapier-i-wloknin)) |
| Ksenia Kirallen      | 1 | ((190912-rexpapier-i-wloknin)) |
| Mateusz Urszank      | 1 | ((190912-rexpapier-i-wloknin)) |
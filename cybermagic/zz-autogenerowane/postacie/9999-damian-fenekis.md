---
categories: profile
factions: 
owner: public
title: Damian Fenekis
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230425-klotnie-sasiadow-w-wanczarku | sołtys; przymila się do maga i ma pasję do jabłek, acz nie chce mówić o magicznych problemach jabłoni; wyraźnie boi się Karolinusa. Ale nie widać by coś zbroił. Jest zbyt... polityczno-miękko-poczciwy. | 0095-05-16 - 0095-05-19 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| AJA Szybka Strzała   | 1 | ((230425-klotnie-sasiadow-w-wanczarku)) |
| Artemis Lawellan     | 1 | ((230425-klotnie-sasiadow-w-wanczarku)) |
| Artur Lawellan       | 1 | ((230425-klotnie-sasiadow-w-wanczarku)) |
| Elea Brzozecka       | 1 | ((230425-klotnie-sasiadow-w-wanczarku)) |
| Ilfons Lawellan      | 1 | ((230425-klotnie-sasiadow-w-wanczarku)) |
| Karolinus Samszar    | 1 | ((230425-klotnie-sasiadow-w-wanczarku)) |
| Olga Fenekis         | 1 | ((230425-klotnie-sasiadow-w-wanczarku)) |
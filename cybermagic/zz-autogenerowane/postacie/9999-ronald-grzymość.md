---
categories: profile
factions: 
owner: public
title: Ronald Grzymość
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190616-anomalna-serafina            | może i mafioso, ale patriota. Nie pozwoli na to, by Kajrat podpalił Pustogor - zdecydował się go zatrzymać. | 0110-05-08 - 0110-05-11 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Antoni Żuwaczka      | 1 | ((190616-anomalna-serafina)) |
| Ernest Kajrat        | 1 | ((190616-anomalna-serafina)) |
| Krystian Namałłek    | 1 | ((190616-anomalna-serafina)) |
| Pięknotka Diakon     | 1 | ((190616-anomalna-serafina)) |
| Serafina Ira         | 1 | ((190616-anomalna-serafina)) |
| Tomasz Tukan         | 1 | ((190616-anomalna-serafina)) |
---
categories: profile
factions: 
owner: public
title: Kacper Wontarczyk
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 180701-dwa-rejsy-w-potrzebie        | dobrej klasy mechanik, człowiek. Dużo statków i statków eterycznych widział. Chętnie pomagał załodze Krwawego Wojownika, potem wyłączył im silniki by Gladiator został opanowany i nie zrobił nikomu krzywdy. Zaprzyjaźnił się z Kaliną. | 0109-08-07 - 0109-08-11 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 180701-dwa-rejsy-w-potrzebie        | dostał ofertę pracy od Luxuritiasu, ale ją odrzucił. Nie lubi korporacji. Uszkodził Wojownika przy akcjach, ale nie był obciążony. | 0109-08-11

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Emilia Kariamon      | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Fabian Komczatkow    | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Gerard Weiner        | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Kalina Rotmistrz     | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Krystian Moborok     | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Liwia Barbaturran    | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Marcjanna Maszotka   | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Martauron Attylla    | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Sabrina Powsimrożek  | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Tymon Barbaturran    | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
| Wojciech Zajcew      | 1 | ((180701-dwa-rejsy-w-potrzebie)) |
---
categories: profile
factions: 
owner: public
title: Dariusz Kuromin
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190906-wypadek-w-kramamczu          | właściciel Włóknina Kramamczy; w rozpaczy powiedział że zaatakował go Wiktor, a tak naprawdę to był sabotaż. | 0110-06-14 - 0110-06-15 |
| 190912-rexpapier-i-wloknin          | w pełni współpracuje. Przestraszony, chętny by ten koszmar się jak najszybciej skończył. Jego ojciec skrzywdził ojca Anny. | 0110-06-17 - 0110-06-21 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Kasjopea Maus        | 2 | ((190906-wypadek-w-kramamczu; 190912-rexpapier-i-wloknin)) |
| Ksenia Kirallen      | 2 | ((190906-wypadek-w-kramamczu; 190912-rexpapier-i-wloknin)) |
| Anna Warlank         | 1 | ((190912-rexpapier-i-wloknin)) |
| Eleonora Rdeść       | 1 | ((190912-rexpapier-i-wloknin)) |
| Jarek Gułanczak      | 1 | ((190912-rexpapier-i-wloknin)) |
| Mariusz Trzewń       | 1 | ((190906-wypadek-w-kramamczu)) |
| Mateusz Urszank      | 1 | ((190912-rexpapier-i-wloknin)) |
| Pięknotka Diakon     | 1 | ((190906-wypadek-w-kramamczu)) |
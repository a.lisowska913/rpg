---
categories: profile
factions: 
owner: public
title: Rufus Komczirp
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200715-sabotaz-netrahiny            | kapitan Tvarany (już eks, bo zezłomowana). Uważa Ariannę za absolutną bohaterkę. Powiedział jej o co chodzi z Netrahiną i oddając Elenie statek, stracił go. | 0110-11-06 - 0110-11-09 |
| 201111-mychainee-na-netrahinie      | mimo wpływu Mychainee miał dość woli, by ściągnąć na Netrahinę Ariannę. Acz próbował ją porwać... | 0111-05-07 - 0111-05-10 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 200715-sabotaz-netrahiny            | dostał straszny OPR za utratę Tvarany i oddanie jej Elenie. Potencjalnie może być zdegradowany za niekompetencję. | 0110-11-09

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 2 | ((200715-sabotaz-netrahiny; 201111-mychainee-na-netrahinie)) |
| Eustachy Korkoran    | 2 | ((200715-sabotaz-netrahiny; 201111-mychainee-na-netrahinie)) |
| Klaudia Stryk        | 2 | ((200715-sabotaz-netrahiny; 201111-mychainee-na-netrahinie)) |
| Elena Verlen         | 1 | ((200715-sabotaz-netrahiny)) |
| Halina Szkwalnik     | 1 | ((201111-mychainee-na-netrahinie)) |
| Lothar Diakon        | 1 | ((200715-sabotaz-netrahiny)) |
| Martyn Hiwasser      | 1 | ((201111-mychainee-na-netrahinie)) |
| OO Netrahina         | 1 | ((200715-sabotaz-netrahiny)) |
| OO Tvarana           | 1 | ((200715-sabotaz-netrahiny)) |
| Percival Diakon      | 1 | ((200715-sabotaz-netrahiny)) |
| Szczepan Myksza      | 1 | ((200715-sabotaz-netrahiny)) |
---
categories: profile
factions: 
owner: public
title: Bartłomiej Korkoran
---

# {{ page.title }}


# Generated: 



## Fiszki


* wuj i twarda łapka rządząca Infernią | faeril: "Infernia służy Korkoranom jako awatar Bezimiennej Pani." | @ 230201-wylaczone-generatory-memoriam-inferni
* (ENCAO: +-000 |Bezkompromisowy, nieustępliwy;;Ciekawski|Family, Benevolence, Self-direction > Achievement, Tradition, Humility| DRIVE: Inkwizytor: ujawnianie bolesnych prawd) | @ 230201-wylaczone-generatory-memoriam-inferni
* "Nasza Infernia ma za zadanie dać nam wolność oraz pomagać innym!" | @ 230201-wylaczone-generatory-memoriam-inferni
* wuj i twarda łapka rządząca Infernią (WG: "System służy ludziom") | faeril: "Infernia służy Korkoranom jako awatar Bezimiennej Pani." | @ 230208-pierwsza-randka-eustachego

### Wątki


historia-eustachego
arkologia-nativis
infernia-jej-imieniem
zbrodnie-kidirona

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220914-dziewczynka-trianai          | wujek poprosił Eustachego i Ardillę, by oni rozwiązali problem znikających nastolatków w Starej Arkologii. Nie wierzy w Ducha Arkologii. | 0092-09-10 - 0092-09-11 |
| 220720-infernia-taksowka-dla-lycoris | PAST: łagodny wujek opiekujący się Eustachym i Adrillą, który jednak rozdziela opierdol gdy zasłużyli; bardzo ważna jest dlań rodzina. ACTUAL: dba o to, by Janek mógł spotkać się z Darią i próbuje chronić swoich młodych podopiecznych. Jednak zaufał im i oddał im do działania zainfekowany przez Trianai CES Purdont (na usprawiedliwienie, nie wie z czym ma do czynienia). | 0093-01-20 - 0093-01-22 |
| 230201-wylaczone-generatory-memoriam-inferni | moralny, chce pomóc noktianom i przygarnął młodego noktianina, Ralfa Tapszecza. Chce by Infernia czyniła dobro. Bardzo rozczarowany Tymonem, który wzgardził Celiną i Ardillą - spoliczkował go i oddał dowodzenie misją Ardilli. | 0093-02-10 - 0093-02-12 |
| 230208-pierwsza-randka-eustachego   | próbuje pokazać Eustachemu, że E. jest lepszy i skuteczniejszy. Że nie musiał zabijać tych noktian. Próbuje przekonać Eustachego pod kątem honoru i prawości. Ale Kidiron się zgadza z Eustachym - Bartłomiej wygląda na starszego niż jest. Widzi, że przegrywa wojnę o Eustachego i nie wie czemu. DOWIADUJE SIĘ, że Infernia jest pod mentalną kontrolą Eustachego. | 0093-02-14 - 0093-02-21 |
| 230215-terrorystka-w-ambasadorce    | chciał zastąpić Ardillę jako zakładnik, ale nie wyszło. Więc wymienił się na grupę rannych ludzi. W momencie w którym Misteria już nie była racjonalna zaatakował ją i obezwładnił, ciężko ranny przez wiły. Dzięki temu pomógł doprowadzić do ewakuacji wszystkich z Ambasadorki. | 0093-02-22 - 0093-02-23 |
| 230614-atak-na-kidirona             | wreszcie zregenerowany. Nie wiedział nic o teorii 'Ralf jest magiem Nihilusa'. Próbuje chronić Eustachego przed Infernią, Infernię przed memoriam, Ardillę przed Ralfem. Po raz pierwszy potraktował Eustachego i Ardillę jak dorosłych. Gdy był atak na Arkologię, przejął kontrolę tymczasowo. Z łóżka w szpitalu. | 0093-03-22 - 0093-03-24 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 220831-czarne-helmy-i-robaki        | coś jest nie tak, Kidironowie i Robaki nie mogli zrobić tego sabotażu; zajmie się tym i wejdzie w głąb. Podejrzewa Tymona Korkorana. | 0092-08-27
| 220720-infernia-taksowka-dla-lycoris | w przeszłości znał się z Wiktorem Turkalisem. Lubią się. | 0093-01-22

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ardilla Korkoran     | 6 | ((220720-infernia-taksowka-dla-lycoris; 220914-dziewczynka-trianai; 230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230614-atak-na-kidirona)) |
| Eustachy Korkoran    | 6 | ((220720-infernia-taksowka-dla-lycoris; 220914-dziewczynka-trianai; 230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230614-atak-na-kidirona)) |
| Rafał Kidiron        | 5 | ((220720-infernia-taksowka-dla-lycoris; 230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230614-atak-na-kidirona)) |
| Kalia Awiter         | 4 | ((230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230614-atak-na-kidirona)) |
| OO Infernia          | 4 | ((220720-infernia-taksowka-dla-lycoris; 230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230614-atak-na-kidirona)) |
| Ralf Tapszecz        | 4 | ((230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230614-atak-na-kidirona)) |
| Celina Lertys        | 2 | ((220720-infernia-taksowka-dla-lycoris; 230201-wylaczone-generatory-memoriam-inferni)) |
| Jan Lertys           | 2 | ((220720-infernia-taksowka-dla-lycoris; 230201-wylaczone-generatory-memoriam-inferni)) |
| Lycoris Kidiron      | 2 | ((220720-infernia-taksowka-dla-lycoris; 230614-atak-na-kidirona)) |
| Stanisław Uczantor   | 2 | ((220914-dziewczynka-trianai; 230614-atak-na-kidirona)) |
| Tymon Korkoran       | 2 | ((230201-wylaczone-generatory-memoriam-inferni; 230215-terrorystka-w-ambasadorce)) |
| Czesław Żuczek       | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Daria Raizis         | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Ernest Puszczowiec   | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Franciszek Pietraszczyk | 1 | ((230208-pierwsza-randka-eustachego)) |
| Kalista Luminis      | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Kamil Wraczok        | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Karina Nezerin       | 1 | ((220914-dziewczynka-trianai)) |
| Kratos Coruscatis    | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Magda Misteria Sarbanik | 1 | ((230215-terrorystka-w-ambasadorce)) |
| Marcin Pietraszczyk  | 1 | ((230208-pierwsza-randka-eustachego)) |
| SAN Szare Ostrze     | 1 | ((230614-atak-na-kidirona)) |
| Tobiasz Lobrak       | 1 | ((230215-terrorystka-w-ambasadorce)) |
| Wiktor Turkalis      | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
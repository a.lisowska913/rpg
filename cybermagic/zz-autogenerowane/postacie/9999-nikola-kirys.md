---
categories: profile
factions: 
owner: public
title: Nikola Kirys
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190427-zrzut-w-pacyfice             | była agentka Inwazji Noctis, świetny pilot i Nurek Szczeliny. Porusza się płynnie po Pacyfice i zdobyła swój ścigacz; wykryta przez Erwina i Pięknotkę | 0110-04-03 - 0110-04-05 |
| 190502-pierwszy-emulator-orbitera   | Emulatorka, która po dotknięciu Cienia odzyskała wolność. Kiedyś sprzężona z Finis Vitae, Minerwa zrobiła z niej Pierwszą Emulatorkę. | 0110-04-10 - 0110-04-12 |
| 190503-bardzo-nieudane-porwania     | polowali na nią Kirasjerzy; przez pół roku chroni Pustogor i Wolne Ptaki przed anomaliami i problemami, za co ma ochronę przed Orbiterem. | 0110-04-14 - 0110-04-16 |
| 190622-wojna-kajrata                | powiedziała Pięknotce na czym polega historia Serafiny. | 0110-05-14 - 0110-05-17 |
| 190626-upadek-enklawy-floris        | nadal konsekwentnie pomaga Enklawom; jej serce widocznie jest po tej stronie a nie po stronie cywilizacji i Pustogoru. Ostrzegła o ataku plus zapewniła ochronę Hubertowi. | 0110-05-24 - 0110-05-27 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 190502-pierwszy-emulator-orbitera   | Emulatorka; po dotknięciu Cienia odzyskała wolność od Orbitera Pierwszego (ku potencjalnej zgubie tej organizacji) | 0110-04-12
| 190503-bardzo-nieudane-porwania     | jak długo współpracuje z Pustogorem chroniąc Wolne Ptaki i nie robiąc nic anty-Pustogorowi, chroniona przed Orbiterem. Uziemiona na tym terenie. | 0110-04-16

## Plany


| Opowieść | Plan | Końcowa data |
| ---- | ---- | ---- |
| 190502-pierwszy-emulator-orbitera   | wyłączone plany: Finis Vitae, destrukcja dookoła, zniszczenie Astorii, natychmiastowa integracja z Cieniem. | 0110-04-12
| 190502-pierwszy-emulator-orbitera   | możliwe plany: szlak przemytu, zemsta na odpowiedzialnych magach Orbitera (nie na wszystkich), zniszczenie reputacji Orbitera. | 0110-04-12
| 190503-bardzo-nieudane-porwania     | zostają jej plany: ochrona lokalnych, zemsta na odpowiedzialnych magach Orbitera (nie na wszystkich), zniszczenie reputacji Orbitera | 0110-04-16

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Pięknotka Diakon     | 4 | ((190427-zrzut-w-pacyfice; 190502-pierwszy-emulator-orbitera; 190503-bardzo-nieudane-porwania; 190622-wojna-kajrata)) |
| Minerwa Metalia      | 3 | ((190427-zrzut-w-pacyfice; 190502-pierwszy-emulator-orbitera; 190503-bardzo-nieudane-porwania)) |
| Ernest Kajrat        | 2 | ((190622-wojna-kajrata; 190626-upadek-enklawy-floris)) |
| Olaf Zuchwały        | 2 | ((190502-pierwszy-emulator-orbitera; 190622-wojna-kajrata)) |
| Alan Bartozol        | 1 | ((190502-pierwszy-emulator-orbitera)) |
| Ariela Sirmin        | 1 | ((190626-upadek-enklawy-floris)) |
| Bożymir Szczupak     | 1 | ((190502-pierwszy-emulator-orbitera)) |
| Damian Orion         | 1 | ((190503-bardzo-nieudane-porwania)) |
| Erwin Galilien       | 1 | ((190427-zrzut-w-pacyfice)) |
| Hubert Kraborów      | 1 | ((190626-upadek-enklawy-floris)) |
| Jolanta Teresis      | 1 | ((190626-upadek-enklawy-floris)) |
| Karla Mrozik         | 1 | ((190503-bardzo-nieudane-porwania)) |
| Konrad Czukajczek    | 1 | ((190626-upadek-enklawy-floris)) |
| Liliana Bankierz     | 1 | ((190622-wojna-kajrata)) |
| Marcel Sowiński      | 1 | ((190626-upadek-enklawy-floris)) |
| Mirela Orion         | 1 | ((190503-bardzo-nieudane-porwania)) |
| Roman Rymtusz        | 1 | ((190626-upadek-enklawy-floris)) |
| Serafina Ira         | 1 | ((190622-wojna-kajrata)) |
| Szymon Maszczor      | 1 | ((190626-upadek-enklawy-floris)) |
| Wargun Ira           | 1 | ((190626-upadek-enklawy-floris)) |
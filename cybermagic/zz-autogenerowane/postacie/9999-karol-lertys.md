---
categories: profile
factions: 
owner: public
title: Karol Lertys
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220723-polowanie-na-szczury-w-nativis | 73-letni "dziadek" Celiny i Jana; kiedyś kapitan aspiriańskiego statku OA Erozja Ego. Gdy został zdradzony i otruty, lojaliści go wydobyli ze statku i został "dziadkiem" gdy prawdziwi rodzice zginęli. Potem został inżynierem Nativis i się ukrywa. Nadal jest jedynym zdolnym do aktywacji Erozji Ego. | 0087-05-03 - 0087-05-12 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 220831-czarne-helmy-i-robaki        | coś jest nie tak, Kidironowie i Robaki nie mogli zrobić tego sabotażu; zajmie się tym i wejdzie w głąb. Podejrzewa Tymona Korkorana. | 0092-08-27

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Celina Lertys        | 1 | ((220723-polowanie-na-szczury-w-nativis)) |
| Emilia d'Erozja      | 1 | ((220723-polowanie-na-szczury-w-nativis)) |
| Jan Lertys           | 1 | ((220723-polowanie-na-szczury-w-nativis)) |
| Laurencjusz Kidiron  | 1 | ((220723-polowanie-na-szczury-w-nativis)) |
| OA Erozja Ego        | 1 | ((220723-polowanie-na-szczury-w-nativis)) |
| Tymon Korkoran       | 1 | ((220723-polowanie-na-szczury-w-nativis)) |
---
categories: profile
factions: 
owner: public
title: BIA Tarn
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190827-rozpaczliwe-ratowanie-bii    | BIA 3 poziomu; którą za wszelką cenę próbuje uratować Talia. Na tej sesji wielki nieobecny - przedmiot, nie podmiot. | 0110-01-17 - 0110-01-20 |
| 190928-ostatnia-misja-tarna         | pełnił rolę koordynatora taktycznego, naukowca, jednostki walki, dywersji oraz negocjatora. Pokazał potęgę BIA poziom 3. Nikogo nie skrzywdził. | 0110-01-26 - 0110-01-27 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 190928-ostatnia-misja-tarna         | zregenerowany; spróbuje uciec w kosmos. | 0110-01-27

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Talia Aegis          | 2 | ((190827-rozpaczliwe-ratowanie-bii; 190928-ostatnia-misja-tarna)) |
| Ernest Kajrat        | 1 | ((190827-rozpaczliwe-ratowanie-bii)) |
| Eustachy Mrownik     | 1 | ((190928-ostatnia-misja-tarna)) |
| Hestia d'Tiamenat    | 1 | ((190928-ostatnia-misja-tarna)) |
| Marek Puszczok       | 1 | ((190827-rozpaczliwe-ratowanie-bii)) |
| Mariusz Trzewń       | 1 | ((190827-rozpaczliwe-ratowanie-bii)) |
| Pedro Ronfak         | 1 | ((190928-ostatnia-misja-tarna)) |
| Pięknotka Diakon     | 1 | ((190827-rozpaczliwe-ratowanie-bii)) |
| Sławomir Niejadek    | 1 | ((190827-rozpaczliwe-ratowanie-bii)) |
| Tymon Grubosz        | 1 | ((190827-rozpaczliwe-ratowanie-bii)) |
| Wiktor Satarail      | 1 | ((190928-ostatnia-misja-tarna)) |
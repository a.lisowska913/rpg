---
categories: profile
factions: 
owner: public
title: Julia Aktenir
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210106-sos-z-haremu                 | eks Stefana Jamniczka; miłośniczka kryptologii i sygnałów oraz silnie kontrolująca sentisieć. Udało jej się wysłać sygnały do przyjaciółki w Orbiterze i ją uratowali. Przed korupcją uciekła roztapiając się w sentisieci; wyciągnął ją stamtąd "kochający Eustachy". | 0111-05-28 - 0111-05-29 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 210106-sos-z-haremu                 | beznadziejnie zakochana w Eustachym Korkoranie. Uratował ją z macek kralotha i z koszmarnego sprzężenia z sentisiecią. | 0111-05-29

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 1 | ((210106-sos-z-haremu)) |
| Diana Arłacz         | 1 | ((210106-sos-z-haremu)) |
| Elena Verlen         | 1 | ((210106-sos-z-haremu)) |
| Eustachy Korkoran    | 1 | ((210106-sos-z-haremu)) |
| Horacy Aktenir       | 1 | ((210106-sos-z-haremu)) |
| Klaudia Stryk        | 1 | ((210106-sos-z-haremu)) |
| Leona Astrienko      | 1 | ((210106-sos-z-haremu)) |
| Martyn Hiwasser      | 1 | ((210106-sos-z-haremu)) |
| Rozalia Teirik       | 1 | ((210106-sos-z-haremu)) |
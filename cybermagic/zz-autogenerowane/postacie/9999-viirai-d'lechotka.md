---
categories: profile
factions: 
owner: public
title: Viirai d'Lechotka
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220814-stary-kot-a-trzesawisko      | TAI starej generacji (bardzo autonomiczna i z opiniami), z Przeniesieniem Ixiońskim. Odcięła Ulę i Karo od informacji z fortifarmy; chciała, by terminusi ją zabili. Dała się przekonać Uli i Karo, że te chcą uratować kota i pomogła im bardzo w tym celu. | 0111-10-08 - 0111-10-09 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Alan Bartozol        | 1 | ((220814-stary-kot-a-trzesawisko)) |
| Ariela Lechot        | 1 | ((220814-stary-kot-a-trzesawisko)) |
| Diana Tevalier       | 1 | ((220814-stary-kot-a-trzesawisko)) |
| Iwan Zawtrak         | 1 | ((220814-stary-kot-a-trzesawisko)) |
| Karolina Terienak    | 1 | ((220814-stary-kot-a-trzesawisko)) |
| Kot Rozbójnik        | 1 | ((220814-stary-kot-a-trzesawisko)) |
| Marian Lechot        | 1 | ((220814-stary-kot-a-trzesawisko)) |
| Urszula Miłkowicz    | 1 | ((220814-stary-kot-a-trzesawisko)) |
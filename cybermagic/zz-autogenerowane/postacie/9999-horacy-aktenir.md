---
categories: profile
factions: 
owner: public
title: Horacy Aktenir
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210106-sos-z-haremu                 | tien Aktenir, wprowadził do Pałacu Jasnego Ognia kralotha i wyłączył sentisieć. Chciał skorumpować Julię Aktenir. Wpadł w pułapkę, skuszony Eustachym i Dianą. | 0111-05-28 - 0111-05-29 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 1 | ((210106-sos-z-haremu)) |
| Diana Arłacz         | 1 | ((210106-sos-z-haremu)) |
| Elena Verlen         | 1 | ((210106-sos-z-haremu)) |
| Eustachy Korkoran    | 1 | ((210106-sos-z-haremu)) |
| Julia Aktenir        | 1 | ((210106-sos-z-haremu)) |
| Klaudia Stryk        | 1 | ((210106-sos-z-haremu)) |
| Leona Astrienko      | 1 | ((210106-sos-z-haremu)) |
| Martyn Hiwasser      | 1 | ((210106-sos-z-haremu)) |
| Rozalia Teirik       | 1 | ((210106-sos-z-haremu)) |
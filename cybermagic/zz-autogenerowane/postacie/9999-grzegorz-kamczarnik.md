---
categories: profile
factions: 
owner: public
title: Grzegorz Kamczarnik
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190326-arcymag-w-raju               | oficer Orbitera Pierwszego na orbicie; ostrzegł Fergusa i Olgę o Elizie i przesłał im jako wsparcie Castigatora. | 0110-04-07 - 0110-04-08 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ataienne             | 1 | ((190326-arcymag-w-raju)) |
| Dawid Szardak        | 1 | ((190326-arcymag-w-raju)) |
| Eliza Ira            | 1 | ((190326-arcymag-w-raju)) |
| Fergus Salien        | 1 | ((190326-arcymag-w-raju)) |
| Olga Leszcz          | 1 | ((190326-arcymag-w-raju)) |
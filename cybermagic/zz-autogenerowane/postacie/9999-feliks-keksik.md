---
categories: profile
factions: 
owner: public
title: Feliks Keksik
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 211120-glizda-ktora-leczy           | ksywa "Pożeracz". Miał 5 minut sławy - pierwszy odcinek "Opowiastek z Aurum" to był wywiad z nim. A opowiadał jak młody basza na prowincji... | 0108-04-03 - 0108-04-14 |
| 200520-figurka-a-kopie-zapasowe     | "Pożeracz". Ten, który faktycznie ukradł figurkę Żabboga i ją uruchomił w HardBack. Wyprztykał się ze środków by skończyć jako pożyteczny idiota - i wie o tym. | 0109-10-13 - 0109-10-20 |
| 200513-trzyglowec-kontra-melinda    | niewielki i mało znaczący mentalista, każe zwać się "Pożeraczem" i chodzi na iluzjach. Pomógł Fantasmagorii i jego wizerunek "Pożeracza" trafił do Aurum. HAŃBA. | 0109-11-08 - 0109-11-17 |
| 210406-potencjalnie-eksterytorialny-seksbot | ROK TEMU: żałosny jak zawsze. Sabina Kazitan się nad nim znęca przyzywając bloba traum. Marysia go uratowała, Odkaziła i pozwoliła mu patrzeć jak Sabina Skaziła się tym samym blobem :D. Po czym -> lekarz. | 0111-04-23 - 0111-04-24 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 200520-figurka-a-kopie-zapasowe     | wyprztykał się z surowców, zrobił sobie wroga w Dianie, stracił sporo reputacji i gdyby nie Mariusz Grabarz, rodzice kazaliby mu wracać do domu w hańbie. | 0109-10-20

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Diana Lemurczak      | 2 | ((200513-trzyglowec-kontra-melinda; 200520-figurka-a-kopie-zapasowe)) |
| Mariusz Grabarz      | 2 | ((200513-trzyglowec-kontra-melinda; 200520-figurka-a-kopie-zapasowe)) |
| Melinda Teilert      | 2 | ((200513-trzyglowec-kontra-melinda; 200520-figurka-a-kopie-zapasowe)) |
| Sabina Kazitan       | 2 | ((210406-potencjalnie-eksterytorialny-seksbot; 211120-glizda-ktora-leczy)) |
| Adam Cześń           | 1 | ((200513-trzyglowec-kontra-melinda)) |
| Amanda Kajrat        | 1 | ((211120-glizda-ktora-leczy)) |
| Amelia Sowińska      | 1 | ((211120-glizda-ktora-leczy)) |
| Aranea Diakon        | 1 | ((200520-figurka-a-kopie-zapasowe)) |
| Barnaba Burgacz      | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Ernest Kajrat        | 1 | ((211120-glizda-ktora-leczy)) |
| Franek Bulterier     | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Gerwazy Lemurczak    | 1 | ((200520-figurka-a-kopie-zapasowe)) |
| Henryk Wkrąż         | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Jan Łowicz           | 1 | ((200513-trzyglowec-kontra-melinda)) |
| Julia Kardolin       | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Justynian Diakon     | 1 | ((211120-glizda-ktora-leczy)) |
| Kacper Bankierz      | 1 | ((211120-glizda-ktora-leczy)) |
| Karol Pustak         | 1 | ((211120-glizda-ktora-leczy)) |
| Katja Nowik          | 1 | ((200513-trzyglowec-kontra-melinda)) |
| Kinga Kruk           | 1 | ((200513-trzyglowec-kontra-melinda)) |
| Lorena Gwozdnik      | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Marysia Sowińska     | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Natasza Aniel        | 1 | ((200513-trzyglowec-kontra-melinda)) |
| Oliwia Lemurczak     | 1 | ((211120-glizda-ktora-leczy)) |
| Rafał Torszecki      | 1 | ((200513-trzyglowec-kontra-melinda)) |
| Roland Sowiński      | 1 | ((211120-glizda-ktora-leczy)) |
| Rupert Mysiokornik   | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Sensacjusz Diakon    | 1 | ((211120-glizda-ktora-leczy)) |
| Stella Sowińska      | 1 | ((211120-glizda-ktora-leczy)) |
| Triana Porzecznik    | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
---
categories: profile
factions: 
owner: public
title: Karol Brinik
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230521-rozszczepiona-persefona-na-itorwienie | pierwszy oficer Itorwien; mag Aurum (nie tien), który mógł mieć proste życie ale marzył o kosmosie. Dał szansę Tadeuszowi i Mojrze (którą zmarnowali). Zarażony, uszkodził Lancera Fabiana; skończył ogłuszony i poparzony. | 0109-09-15 - 0109-09-17 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Emilia Ibris         | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Fabian Korneliusz    | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Helmut Szczypacz     | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Iskander Matorin     | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Klaudia Stryk        | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Martyn Hiwasser      | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Mojra Karstall       | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| OO Itorwien          | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| OO Serbinius         | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Tadeusz Arkaladis    | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
---
categories: profile
factions: 
owner: public
title: SC Goldarion
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210330-pletwalowcy-na-goldarionie   | kiepskojakościowy patchworkowy średni transportowiec na 60 osób załogi klasy Prosperion. Ostry refit załogi Płetwala i Goldariona, co się skończyło opóźnieniami, awariami - ale jest przynajmniej bieżąca woda... | 0109-01-04 - 0109-01-19 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Adam Permin          | 1 | ((210330-pletwalowcy-na-goldarionie)) |
| Aleksander Leszert   | 1 | ((210330-pletwalowcy-na-goldarionie)) |
| Janusz Krumlod       | 1 | ((210330-pletwalowcy-na-goldarionie)) |
| Jonatan Piegacz      | 1 | ((210330-pletwalowcy-na-goldarionie)) |
| Karolina Kartusz     | 1 | ((210330-pletwalowcy-na-goldarionie)) |
| Martyna Bianistek    | 1 | ((210330-pletwalowcy-na-goldarionie)) |
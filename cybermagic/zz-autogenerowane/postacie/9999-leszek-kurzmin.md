---
categories: profile
factions: 
owner: public
title: Leszek Kurzmin
---

# {{ page.title }}


# Generated: 



## Fiszki


* dowódca jednostki | @ 221123-egzotyczna-pieknosc-na-astralnej-flarze
* ENCAO: 0-+++ | | @ 221123-egzotyczna-pieknosc-na-astralnej-flarze
* ENCAO: 0-+++ | Żyje według własnych przekonań;; Lojalny i proaktywny| VALS: Benevolence, Humility >> Face| DRIVE: Właściwe miejsce w rzeczywistości. | @ 221214-astralna-flara-kontra-domina-lucis
* dowódca jednostki (atarienin defensor) TACTICAL NERD | @ 221221-astralna-flara-i-nowy-komodor

### Wątki


historia-arianny

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220928-kapitan-verlen-i-pojedynek-z-marine | chce pomóc Ariannie (nawet dyskretnie) by ratować Królową Kosmicznej Chwały i Alezję. Rywalizował z Alezją w Akademii, nie wierzy jak bardzo upadła. | 0100-03-16 - 0100-03-18 |
| 221026-kapitan-verlen-i-koniec-przygody-na-krolowej | współpracuje z Arianną. Ona zapewnia linię 'Aurum - Tucznik Trzeci', on linię 'Tucznik Trzeci - Nonarion'. I tak powstanie udany handel na którym wszyscy wygrają. | 0100-04-10 - 0100-04-17 |
| 221116-astralna-flara-dociera-do-nonariona-nadziei | szczęśliwy, że mają sensownego komodora; | 0100-07-11 - 0100-07-14 |
| 221123-egzotyczna-pieknosc-na-astralnej-flarze | dobrze pracuje z dokumentami i przetwarza dane; znalazł dla Gabriela Lodowca info, że jeden z niewolników to koleś z Orbitera. Przeprowadził dyskretną operację Ograniczenia Mirtaeli, TAI Hadiah Emas z rozkazu Gerwazego Kircznika. | 0100-07-15 - 0100-07-18 |
| 221214-astralna-flara-kontra-domina-lucis | logistycznie przeprowadził operację poddawania się noktian, by na pewno nie ucierpiał nikt niewinny i nie dało się zniszczyć Flary ani Athamarein. Wykonywał wielokrotne skany aż doszedł do tego, że teren jest silnie zaminowany. Wiedział, że Domina Lucis nie będzie bez defensyw. | 0100-08-11 - 0100-08-13 |
| 221221-astralna-flara-i-nowy-komodor | to on zaproponował sojusz ze squatterami i oddanie im bazy. Twórca modyfikowanego planu jak odzyskać Kazmirian by wywołać największy szok u squatterów. Oddał Ariannie możliwość spięcia z Kazmirian, by jej załoga mogła ćwiczyć (Bolza jest bardzo wymagający). Doszedł do tego, że najpewniej to Bolza stoi za zniszczeniem Zarralei i przekazał informację o tym Ariannie. | 0100-09-09 - 0100-09-12 |
| 230111-gdy-hr-reedukuje-niewlasciwa-osobe | pewny umiejętności Athamarein; chciał walczyć z Nox Ignis, ale pod wpływem pomysłów Arianny zmienił Athamarein w super-szybką jednostkę kurierską i wymanewrował Nox Ignis nie walcząc z nią bezpośrednio. CREEPED OUT przez Destructor Animarum. Zabił Adragaina, nie chciał widzieć tego upiornego pustego uśmiechu. | 0100-09-15 - 0100-09-18 |
| 200624-ratujmy-castigator           | kapitan Castigatora i przyjaciel Arianny; poprosił ją o pomoc w "oczyszczeniu" Castigatora z naleśniczej arystokracji. Mocno jej ufa... i ją friendzonował. | 0110-10-15 - 0110-10-19 |
| 200708-problematyczna-elena         | dowódca Castigatora; wysłał Elenę do Arianny jako załogantkę. A Kramer zaakceptował, bo Elena miała wszystkie papiery w porządku i nieposzlakowaną opinię. | 0110-10-29 - 0110-11-03 |
| 200819-sekrety-orbitera-historia-prawdziwa | by pomóc Ariannie, wziął na siebie Leonę, mówiąc jej że wykryli koloidowy statek Eterni. | 0110-11-26 - 0110-12-04 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 230111-gdy-hr-reedukuje-niewlasciwa-osobe | solidny opierdol od kmdr Salazara Bolzy za śmierć Adragaina Ferriasa. Emocje zwyciężyły nad możliwością zbadania jak działa Destructor Animarum. | 0100-09-18

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 10 | ((200624-ratujmy-castigator; 200708-problematyczna-elena; 200819-sekrety-orbitera-historia-prawdziwa; 220928-kapitan-verlen-i-pojedynek-z-marine; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221214-astralna-flara-kontra-domina-lucis; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Daria Czarnewik      | 7 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221214-astralna-flara-kontra-domina-lucis; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| OO Astralna Flara    | 5 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221214-astralna-flara-kontra-domina-lucis; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| OO Athamarein        | 5 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221214-astralna-flara-kontra-domina-lucis; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Władawiec Diakon     | 5 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Arnulf Perikas       | 4 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Elena Verlen         | 4 | ((200708-problematyczna-elena; 200819-sekrety-orbitera-historia-prawdziwa; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Kajetan Kircznik     | 4 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Maja Samszar         | 4 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221116-astralna-flara-dociera-do-nonariona-nadziei; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Ellarina Samarintael | 3 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze; 221214-astralna-flara-kontra-domina-lucis; 221221-astralna-flara-i-nowy-komodor)) |
| Erwin Pies           | 3 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Eustachy Korkoran    | 3 | ((200624-ratujmy-castigator; 200708-problematyczna-elena; 200819-sekrety-orbitera-historia-prawdziwa)) |
| Gabriel Lodowiec     | 3 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze; 221214-astralna-flara-kontra-domina-lucis)) |
| Grażyna Burgacz      | 3 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221116-astralna-flara-dociera-do-nonariona-nadziei; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Klaudia Stryk        | 3 | ((200624-ratujmy-castigator; 200708-problematyczna-elena; 200819-sekrety-orbitera-historia-prawdziwa)) |
| Leona Astrienko      | 3 | ((200624-ratujmy-castigator; 200819-sekrety-orbitera-historia-prawdziwa; 220928-kapitan-verlen-i-pojedynek-z-marine)) |
| Tomasz Ruppok        | 3 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej; 221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Alezja Dumorin       | 2 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Antoni Kramer        | 2 | ((200708-problematyczna-elena; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Marcel Kulgard       | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| NekroTAI Zarralea    | 2 | ((221214-astralna-flara-kontra-domina-lucis; 221221-astralna-flara-i-nowy-komodor)) |
| OO Królowa Kosmicznej Chwały | 2 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| OO Loricatus         | 2 | ((221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Salazar Bolza        | 2 | ((221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| SCA Hadiah Emas      | 2 | ((221116-astralna-flara-dociera-do-nonariona-nadziei; 221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Stefan Torkil        | 2 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Szczepan Myrczek     | 2 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Szymon Wanad         | 2 | ((220928-kapitan-verlen-i-pojedynek-z-marine; 221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Adragain Ferrias     | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| AK Nox Ignis         | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| AK Salamin           | 1 | ((200708-problematyczna-elena)) |
| Alan Nierkamin       | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Aleksy Sartaran      | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Axel Nargan          | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |
| Damian Orion         | 1 | ((200819-sekrety-orbitera-historia-prawdziwa)) |
| Frank Mgrot          | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Gerwazy Kircznik     | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Hind Szug Traf       | 1 | ((221123-egzotyczna-pieknosc-na-astralnej-flarze)) |
| Izabela Zarantel     | 1 | ((200819-sekrety-orbitera-historia-prawdziwa)) |
| Kaspian Certisarius  | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| Kirea Rialirat       | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |
| Klaudiusz Terienak   | 1 | ((221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| Lana Mirkinin        | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Leo Kasztop          | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Martyn Hiwasser      | 1 | ((200819-sekrety-orbitera-historia-prawdziwa)) |
| Miłosz Klinek        | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Olgierd Drongon      | 1 | ((200708-problematyczna-elena)) |
| OO Alkaris           | 1 | ((200624-ratujmy-castigator)) |
| OO Aurelion          | 1 | ((200708-problematyczna-elena)) |
| OO Castigator        | 1 | ((200624-ratujmy-castigator)) |
| OO Tucznik Trzeci    | 1 | ((221026-kapitan-verlen-i-koniec-przygody-na-krolowej)) |
| OO Żelazko           | 1 | ((200708-problematyczna-elena)) |
| Persefona d'Infernia | 1 | ((200708-problematyczna-elena)) |
| Persefona d'Loricatus | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Rozalia Wączak       | 1 | ((200624-ratujmy-castigator)) |
| Sabina Servatel      | 1 | ((200819-sekrety-orbitera-historia-prawdziwa)) |
| Sabrina Ferrias      | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Sargon Niiris        | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| Sarian Xadaar        | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |
| SCA Isigtand         | 1 | ((221116-astralna-flara-dociera-do-nonariona-nadziei)) |
| Tadeusz Ursus        | 1 | ((200819-sekrety-orbitera-historia-prawdziwa)) |
| Tristan Rialirat     | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |
---
categories: profile
factions: 
owner: public
title: Tytus Ramkon
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220512-influencerskie-mikotki-z-talio | dowódca ochrony Talio; udaje brutalnego psychopatę; w praktyce jest łagodniejszy niż się zdaje po prostu ta strategia działa. Zaangażował Elwirę do rozwiązania "kto chce porwać mikotki" i "czemu znikają Ogniki". | 0104-02-24 - 0104-03-01 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Antonina Terkin      | 1 | ((220512-influencerskie-mikotki-z-talio)) |
| Aurus Czarmadon      | 1 | ((220512-influencerskie-mikotki-z-talio)) |
| Elwira Piscernik     | 1 | ((220512-influencerskie-mikotki-z-talio)) |
| Jacek Ożgor          | 1 | ((220512-influencerskie-mikotki-z-talio)) |
| Kamil Czarmadon      | 1 | ((220512-influencerskie-mikotki-z-talio)) |
| Kara Prazdnik        | 1 | ((220512-influencerskie-mikotki-z-talio)) |
| Mirella Czarmadon    | 1 | ((220512-influencerskie-mikotki-z-talio)) |
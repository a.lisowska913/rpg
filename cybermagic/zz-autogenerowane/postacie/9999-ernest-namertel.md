---
categories: profile
factions: 
owner: public
title: Ernest Namertel
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210921-przybycie-rekina-z-eterni    | rekin z Eterni, kiedyś mentorowany przez Morlana więc niezły w walce; zakochany ze wzajemnością w Amelii Sowińskich (ich związek jest sekretem). | 0111-07-16 - 0111-07-21 |
| 210928-wysadzony-zywy-scigacz       | naiwny wobec ludzi i polityki, ale świetny w walce i taktyce. Ma gdzieś protokoły, chce pomagać. Wierzy, że Marysia i Amelia są "dobre" i zaprzyjaźnił się z Karoliną. Bardzo przyjacielsko traktuje swój arkin. Gdy jego ścigacz (żywy) został wysadzony, prawie uruchomił simulacrum. Marysia przekonała go, by oddał jej śledztwo. Rozpacza w ciszy. | 0111-07-22 - 0111-07-23 |
| 211102-satarail-pomaga-marysi       | nie chciał pomagać w ratowaniu Rekinów, ale zrobił to bo Amelia by to zrobiła. Nadal - nawet jego oddział nie dał rady przebić się przez zarażone Rekiny. | 0111-08-05 - 0111-08-06 |
| 211123-odbudowa-wedlug-justyniana   | Marysia przekonała go by dać szansę Rekinom i się jednak jakoś integrował. Na jego prośbę Azalia opracowała rozwiązanie jak odbudować Dzielnicę. | 0111-08-07 - 0111-08-16 |
| 211228-akt-o-ktorym-marysia-nie-wie | chciał zdobyć akt Marysi (wysłał Keirę) i może poprosić o akt Amelii artystkę (Lorenę). Ale po rozmowie z Marysią zaczął na nią trochę inaczej patrzeć; został cząstkowo uwodzony. Zainteresował się nią. | 0111-09-05 - 0111-09-08 |
| 220111-marysiowa-hestia-rekinow     | zaplanował, przekonany przez Karo, odbicie Melissy z potencjalnego kultu jako ćwiczenia. Wysłał jedną Keirę. | 0111-09-12 - 0111-09-15 |
| 220222-plaszcz-ochronny-mimozy      | chce ochronić i wyczyścić wszystkich z kralotyzacji; przejmuje kontrolę nad terenem Rekinów. Mimoza staje mu na drodze. Ścierają się w nim impulsy: Esuriit - dobro. | 0111-09-17 - 0111-09-21 |
| 220802-gdy-prawnik-przyjdzie-po-rekiny | podejrzewa Mimozę o wszystko co najgorsze - uważa, to ONA stoi za Marsenem Gwozdnikiem i Loreną. Pod wpływem Marysi oddał jej temat i zerwał zaręczyny z Amelią. | 0111-10-05 - 0111-10-07 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 210928-wysadzony-zywy-scigacz       | Marysia i Karolina są jedynymi Rekinami jakim ufa i lubi. Inni są niebezpieczni lub tolerowalni. | 0111-07-23
| 210928-wysadzony-zywy-scigacz       | następne 7 dni regeneracji po Skażeniu Esuriit; nie udało mu się uratować Daina. | 0111-07-23
| 211123-odbudowa-wedlug-justyniana   | daje szansę Rekinom. Marysia z pomocą Karoliny dały radę go przekonać, że Rekiny nie są tak złe - i Amelia by tego chciała. | 0111-08-16
| 211127-waśń-o-ryby-w-majklapcu      | jego nazwiskiem i Eternią podobno straszy się lokalny biznes w Majkłapcu. Robota Karoliny i Daniela. | 0111-08-20
| 211228-akt-o-ktorym-marysia-nie-wie | zaczął patrzeć na Marysię Sowińską inaczej. Może nie tylko na zimną damę, ale też na interesującą partnerkę w łóżku..? Ma jej akt i mu się bardzo podoba. | 0111-09-08
| 220222-plaszcz-ochronny-mimozy      | uznał za PERSONALNĄ obrazę i PERSONALNY afront to, że Mimoza zwinęła mu Lorenę. I Ernest nie może dostać swojego aktu. STARCIE z Mimozą Diakon. | 0111-09-21
| 220802-gdy-prawnik-przyjdzie-po-rekiny | spodobała mu się Marysia Sowińska BARDZIEJ niż Amelia, póki tu jest. To znaczy, że Marysia go odbija | 0111-10-07
| 220802-gdy-prawnik-przyjdzie-po-rekiny | będzie zdawał się na Marysię w sprawach politycznych (+1Vg w takich sytuacjach) | 0111-10-07
| 220802-gdy-prawnik-przyjdzie-po-rekiny | zerwał zaręczyny z Amelią, bo Marysia też jest w jego życiu i Amelia nie jest już tą jedyną | 0111-10-07
| 220802-gdy-prawnik-przyjdzie-po-rekiny | zacietrzewiony na Mimozę, Marsena i Lorenę. Oni stoją na drodze do lepszego świata i konspirują przeciw niemu. | 0111-10-07

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Marysia Sowińska     | 8 | ((210921-przybycie-rekina-z-eterni; 210928-wysadzony-zywy-scigacz; 211102-satarail-pomaga-marysi; 211123-odbudowa-wedlug-justyniana; 211228-akt-o-ktorym-marysia-nie-wie; 220111-marysiowa-hestia-rekinow; 220222-plaszcz-ochronny-mimozy; 220802-gdy-prawnik-przyjdzie-po-rekiny)) |
| Karolina Terienak    | 7 | ((210921-przybycie-rekina-z-eterni; 210928-wysadzony-zywy-scigacz; 211102-satarail-pomaga-marysi; 211123-odbudowa-wedlug-justyniana; 211228-akt-o-ktorym-marysia-nie-wie; 220111-marysiowa-hestia-rekinow; 220222-plaszcz-ochronny-mimozy)) |
| Rafał Torszecki      | 5 | ((210921-przybycie-rekina-z-eterni; 210928-wysadzony-zywy-scigacz; 211102-satarail-pomaga-marysi; 211123-odbudowa-wedlug-justyniana; 211228-akt-o-ktorym-marysia-nie-wie)) |
| Keira Amarco d'Namertel | 4 | ((210928-wysadzony-zywy-scigacz; 211228-akt-o-ktorym-marysia-nie-wie; 220111-marysiowa-hestia-rekinow; 220222-plaszcz-ochronny-mimozy)) |
| Lorena Gwozdnik      | 3 | ((211102-satarail-pomaga-marysi; 211228-akt-o-ktorym-marysia-nie-wie; 220222-plaszcz-ochronny-mimozy)) |
| Azalia Sernat d'Namertel | 2 | ((210928-wysadzony-zywy-scigacz; 211123-odbudowa-wedlug-justyniana)) |
| Daniel Terienak      | 2 | ((211123-odbudowa-wedlug-justyniana; 211228-akt-o-ktorym-marysia-nie-wie)) |
| Karol Pustak         | 2 | ((210921-przybycie-rekina-z-eterni; 211123-odbudowa-wedlug-justyniana)) |
| Liliana Bankierz     | 2 | ((211228-akt-o-ktorym-marysia-nie-wie; 220111-marysiowa-hestia-rekinow)) |
| Amelia Sowińska      | 1 | ((210921-przybycie-rekina-z-eterni)) |
| Arkadia Verlen       | 1 | ((210921-przybycie-rekina-z-eterni)) |
| Diana Tevalier       | 1 | ((220111-marysiowa-hestia-rekinow)) |
| Franek Bulterier     | 1 | ((210928-wysadzony-zywy-scigacz)) |
| Hestia d'Rekiny      | 1 | ((220111-marysiowa-hestia-rekinow)) |
| Hipolit Umadek       | 1 | ((220802-gdy-prawnik-przyjdzie-po-rekiny)) |
| Ignacy Myrczek       | 1 | ((211123-odbudowa-wedlug-justyniana)) |
| Jeremi Sowiński      | 1 | ((220111-marysiowa-hestia-rekinow)) |
| Jolanta Sowińska     | 1 | ((210921-przybycie-rekina-z-eterni)) |
| Justynian Diakon     | 1 | ((211123-odbudowa-wedlug-justyniana)) |
| Lucjan Sowiński      | 1 | ((210921-przybycie-rekina-z-eterni)) |
| Marek Samszar        | 1 | ((211102-satarail-pomaga-marysi)) |
| Mimoza Elegancja Diakon | 1 | ((220222-plaszcz-ochronny-mimozy)) |
| Napoleon Bankierz    | 1 | ((211228-akt-o-ktorym-marysia-nie-wie)) |
| Nataniel Morlan      | 1 | ((210921-przybycie-rekina-z-eterni)) |
| Sensacjusz Diakon    | 1 | ((211102-satarail-pomaga-marysi)) |
| Tomasz Tukan         | 1 | ((210921-przybycie-rekina-z-eterni)) |
| Wiktor Satarail      | 1 | ((211102-satarail-pomaga-marysi)) |
| Żorż d'Namertel      | 1 | ((210928-wysadzony-zywy-scigacz)) |
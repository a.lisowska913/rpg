---
categories: profile
factions: 
owner: public
title: Lycoris Kidiron
---

# {{ page.title }}


# Generated: 



## Fiszki


* 57 lat (ze wspomaganiami drakolickimi dającymi jej efektywność 3x-latki) | @ 230201-wylaczone-generatory-memoriam-inferni
* (ENCAO: +0+-0 | Proaktywna;; Pierwsza w działaniu | Security, Achievement > Conformism | DRIVE: Kapitan Ahab (perfekcyjna arkologia)) | @ 230201-wylaczone-generatory-memoriam-inferni
* ekspert od bioinżynierii i terraformacji, pionier (zdolna do przeżycia w trudnym terenie) | @ 230201-wylaczone-generatory-memoriam-inferni
* "Nikt, kto próbował się dopasować do innych nie doprowadził do postępu. Dzięki mnie Nativis będzie bezpieczna." | @ 230201-wylaczone-generatory-memoriam-inferni

### Wątki


historia-eustachego
arkologia-nativis
infernia-jej-imieniem
zbrodnie-kidirona

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220712-pasozytnicze-osy-w-nativis   | naukowiec i ekspert od bioinżynierii i terraformacji; oryginał spowodował Plagę pod wpływem Pasożyta. Miragent zrozumiał cel Pasożyta - rozprzestrzenić Pasożyta do innych arkologii - i współpracując z Rafałem uniemożliwił tą sytuację. | 0092-10-29 - 0092-11-07 |
| 220720-infernia-taksowka-dla-lycoris | wysłana przez Rafała do CES Purdont bo coś nie tak z terraformacją; zniknęła z Wiktorem w okolicach Wiertła Ekopoezy Delta w okolicach burzy piaskowej. | 0093-01-20 - 0093-01-22 |
| 230614-atak-na-kidirona             | próbowała przejąć kontrolę nad arkologią gdy Rafał "zginął". Przetrwała i ogólnie dobrze że tam była, ale nie nadaje się do dowodzenia arkologią. | 0093-03-22 - 0093-03-24 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Rafał Kidiron        | 3 | ((220712-pasozytnicze-osy-w-nativis; 220720-infernia-taksowka-dla-lycoris; 230614-atak-na-kidirona)) |
| Ardilla Korkoran     | 2 | ((220720-infernia-taksowka-dla-lycoris; 230614-atak-na-kidirona)) |
| Bartłomiej Korkoran  | 2 | ((220720-infernia-taksowka-dla-lycoris; 230614-atak-na-kidirona)) |
| Eustachy Korkoran    | 2 | ((220720-infernia-taksowka-dla-lycoris; 230614-atak-na-kidirona)) |
| OO Infernia          | 2 | ((220720-infernia-taksowka-dla-lycoris; 230614-atak-na-kidirona)) |
| Celina Lertys        | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Czesław Żuczek       | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Damian Marlinczak    | 1 | ((220712-pasozytnicze-osy-w-nativis)) |
| Daria Raizis         | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Jan Lertys           | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Jarlow Gurdacz       | 1 | ((220712-pasozytnicze-osy-w-nativis)) |
| Kalia Awiter         | 1 | ((230614-atak-na-kidirona)) |
| Kamil Wraczok        | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
| Ralf Tapszecz        | 1 | ((230614-atak-na-kidirona)) |
| SAN Szare Ostrze     | 1 | ((230614-atak-na-kidirona)) |
| Serentina d'Remora   | 1 | ((220712-pasozytnicze-osy-w-nativis)) |
| Stanisław Uczantor   | 1 | ((230614-atak-na-kidirona)) |
| Wiktor Turkalis      | 1 | ((220720-infernia-taksowka-dla-lycoris)) |
---
categories: profile
factions: 
owner: public
title: Michał Kabarniec
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220809-20-razy-za-duzo-esuriit      | Mike; Scout; managed to spread the people so Esuriit-manifesting truck won't damage them and spun a lie upon a lie upon a lie... he became a local hero of sorts and his problem "because of daughter, you know" became a public secret. | 0111-09-26 - 0111-10-02 |
| 220819-tank-as-a-love-letter        | Mimosa's agent; planned a sting operation which revealed link Iwona - Daniel - Henryk. Found out that mafia is doing something here, which is worrying. | 0111-10-03 - 0111-10-04 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 220809-20-razy-za-duzo-esuriit      | the best recognized stealth operator. Local hero of Sunflower plant. People are interested in his "because of that issue with daughter, you know". | 0111-10-02

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Izabela Selentik     | 2 | ((220809-20-razy-za-duzo-esuriit; 220819-tank-as-a-love-letter)) |
| Mimoza Diakon        | 2 | ((220809-20-razy-za-duzo-esuriit; 220819-tank-as-a-love-letter)) |
| Talia Mikrit         | 2 | ((220809-20-razy-za-duzo-esuriit; 220819-tank-as-a-love-letter)) |
| Ariel Kubunczak      | 1 | ((220819-tank-as-a-love-letter)) |
| Arkadiusz Terienak   | 1 | ((220819-tank-as-a-love-letter)) |
| AU Flara Astorii     | 1 | ((220819-tank-as-a-love-letter)) |
| Daniel Terienak      | 1 | ((220819-tank-as-a-love-letter)) |
| Henryk Wkrąż         | 1 | ((220819-tank-as-a-love-letter)) |
| Iwona Perikas        | 1 | ((220819-tank-as-a-love-letter)) |
| Karol Walgoryn       | 1 | ((220809-20-razy-za-duzo-esuriit)) |
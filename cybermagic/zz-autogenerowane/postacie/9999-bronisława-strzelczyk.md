---
categories: profile
factions: 
owner: public
title: Bronisława Strzelczyk
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 180724-skorpiony-spod-ziemi         | znając świetnie teren, odkryła przeszłość rodziny Alicji w Żarni; wsparcie oraz agregatorka okolicznych plotek | 0109-08-23 - 0109-08-25 |
| 180814-trufle-z-kosmosu             | dzięki jej wiedzy o życiu na wsi i znajomości Olgi Myszeczki, udało się bezproblemowo wyżreć podłe trufle glukszwajnami. | 0109-08-30 - 0109-09-01 |
| 180821-programista-mimo-woli        | stworzyła Cyberszkołę Efektem Skażenia, po czym zniszczyła tamtejszą bezbronną efemerydę glukszwajnem. | 0109-09-06 - 0109-09-07 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 180724-skorpiony-spod-ziemi         | Olga Myszeczka została jej bliższą koleżanką - toleruje ją i okazjonalnie pomoże. | 0109-08-25
| 180821-programista-mimo-woli        | właścicielka niewielkiego i bardzo figlarnego glukszwajna; opuścił Olgę i przyszedł do nowej przyjaciółki | 0109-09-07

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Jakub Wirus          | 2 | ((180724-skorpiony-spod-ziemi; 180814-trufle-z-kosmosu)) |
| Pięknotka Diakon     | 2 | ((180814-trufle-z-kosmosu; 180821-programista-mimo-woli)) |
| Roman Kłębek         | 2 | ((180724-skorpiony-spod-ziemi; 180814-trufle-z-kosmosu)) |
| Alicja Kłębek        | 1 | ((180724-skorpiony-spod-ziemi)) |
| Arkadiusz Mocarny    | 1 | ((180814-trufle-z-kosmosu)) |
| Atena Sowińska       | 1 | ((180821-programista-mimo-woli)) |
| Kirył Najłalmin      | 1 | ((180814-trufle-z-kosmosu)) |
| Olga Myszeczka       | 1 | ((180724-skorpiony-spod-ziemi)) |
| Staś Kruszawiecki    | 1 | ((180821-programista-mimo-woli)) |
| Tadeusz Kruszawiecki | 1 | ((180821-programista-mimo-woli)) |
---
categories: profile
factions: 
owner: public
title: Sarian Xadaar
---

# {{ page.title }}


# Generated: 



## Fiszki


* pierwszy oficer i dowódca Dominy Lucis (dekadianin) | @ 221130-astralna-flara-w-strefie-duchow
* ENCAO:  0-+00 |Niemożliwy do zatrzymania;;Lojalny i oddany grupie| VALS: Benevolence, Tradition >> Face| DRIVE: Niezłomna Forteca | @ 221130-astralna-flara-w-strefie-duchow
* "Victorious, to the end!!!" | @ 221130-astralna-flara-w-strefie-duchow

### Wątki


historia-arianny

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221230-dowody-na-istnienie-nox-ignis | znajduje się na Ratio Spei; przechwycił Katrinę Komczirp. Jego aktualne plany są nieznane; jest poszukiwany przez Orbiter. | 0082-07-28 - 0082-08-01 |
| 221214-astralna-flara-kontra-domina-lucis | KIA; zniszczył "Optymistyczny Uśmiech" (medical Orbitera) podczas wojny; dekadianin; przebudził kpt Nargana w obliczu Zarralei i Orbitera. Zginął z ręki serpentisa by nie wpaść w ręce Orbitera. | 0100-08-11 - 0100-08-13 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| AK Nox Ignis         | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |
| Aletia Nix           | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |
| Arianna Verlen       | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |
| Axel Nargan          | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |
| Daria Czarnewik      | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |
| Dominik Łarnisz      | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |
| Ellarina Samarintael | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |
| Franz Szczypiornik   | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |
| Gabriel Lodowiec     | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |
| Katrina Komczirp     | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |
| Kirea Rialirat       | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |
| Leszek Kurzmin       | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |
| Medea Sowińska       | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |
| NekroTAI Zarralea    | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |
| OO Astralna Flara    | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |
| OO Athamarein        | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |
| OO Loricatus         | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |
| Persefona d'Loricatus | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |
| Talia Derwisz        | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |
| Tatiana Ozariat      | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |
| Tristan Ozariat      | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |
| Tristan Rialirat     | 1 | ((221214-astralna-flara-kontra-domina-lucis)) |
| Wawrzyn Rewemis      | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |
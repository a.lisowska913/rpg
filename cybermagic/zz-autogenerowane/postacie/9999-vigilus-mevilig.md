---
categories: profile
factions: 
owner: public
title: Vigilus Mevilig
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210929-grupa-ekspedycyjna-kellert   | stworzony przez ludzi byt Esuriit mający na celu chronić (?), manifestujący się jako zbiór za dużej ilości kończyn wychodzących z mechanizmów. Bóstwo? Widzi twarze. Kolokuje. | 0112-01-07 - 0112-01-10 |
| 211020-kurczakownia                 | stawił czoła Ariannie przy swoim ołtarzu, ale został Zmieniony przez Działo Rozpaczy Eustachego. Hibernacja 1 tydzień, po czym się odbuduje jako coś nowego. | 0112-01-18 - 0112-01-20 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 2 | ((210929-grupa-ekspedycyjna-kellert; 211020-kurczakownia)) |
| Elena Verlen         | 2 | ((210929-grupa-ekspedycyjna-kellert; 211020-kurczakownia)) |
| Eustachy Korkoran    | 2 | ((210929-grupa-ekspedycyjna-kellert; 211020-kurczakownia)) |
| Klaudia Stryk        | 2 | ((210929-grupa-ekspedycyjna-kellert; 211020-kurczakownia)) |
| Martyn Hiwasser      | 2 | ((210929-grupa-ekspedycyjna-kellert; 211020-kurczakownia)) |
| OO Infernia          | 2 | ((210929-grupa-ekspedycyjna-kellert; 211020-kurczakownia)) |
| Otto Azgorn          | 2 | ((210929-grupa-ekspedycyjna-kellert; 211020-kurczakownia)) |
| Adam Nerawol         | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| Aleksandra Termia    | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| Antoni Kramer        | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| Flawia Blakenbauer   | 1 | ((211020-kurczakownia)) |
| Kamil Lyraczek       | 1 | ((211020-kurczakownia)) |
| Leona Astrienko      | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| Olena Orion          | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| OO Omega Septius     | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| TAI Marszałek Grzmotoszpon Trzeci | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
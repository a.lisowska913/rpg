---
categories: profile
factions: 
owner: public
title: Talia Derwisz
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221230-dowody-na-istnienie-nox-ignis | cyberwspomagana infiltratorka wysokiej klasy z ukrytymi implantami; nie ma mocy magicznej tylko echo sentisieci. Dowodzi suboperacją infiltracji Ratio Spei i znalezienia Katriny Komczirp (czyt. dowodzi Dominikiem). | 0082-07-28 - 0082-08-01 |
| 230102-elwira-koszmar-nox-ignis     | doszła do tego jakie są frakcje noktiańskie na Ratio Spei; zainteresowanie oficera noktiańskiego. W stałej komunikacji z Franzem dzięki Medei. Przebrana za tajemniczą Elwirę dotarła do Tristana i poznała sekret Nox Ignis - i zobaczyła jak szalony i Skażony jest Tristan. | 0082-08-02 - 0082-08-05 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 221230-dowody-na-istnienie-nox-ignis | zintegrowana sentisiecią z Medeą Sowińską, dzięki czemu mają dwustronną komunikację i może coś jeszcze. | 0082-08-01

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| AK Nox Ignis         | 2 | ((221230-dowody-na-istnienie-nox-ignis; 230102-elwira-koszmar-nox-ignis)) |
| Aletia Nix           | 2 | ((221230-dowody-na-istnienie-nox-ignis; 230102-elwira-koszmar-nox-ignis)) |
| Dominik Łarnisz      | 2 | ((221230-dowody-na-istnienie-nox-ignis; 230102-elwira-koszmar-nox-ignis)) |
| Franz Szczypiornik   | 2 | ((221230-dowody-na-istnienie-nox-ignis; 230102-elwira-koszmar-nox-ignis)) |
| Medea Sowińska       | 2 | ((221230-dowody-na-istnienie-nox-ignis; 230102-elwira-koszmar-nox-ignis)) |
| OO Loricatus         | 2 | ((221230-dowody-na-istnienie-nox-ignis; 230102-elwira-koszmar-nox-ignis)) |
| Tatiana Ozariat      | 2 | ((221230-dowody-na-istnienie-nox-ignis; 230102-elwira-koszmar-nox-ignis)) |
| Tristan Ozariat      | 2 | ((221230-dowody-na-istnienie-nox-ignis; 230102-elwira-koszmar-nox-ignis)) |
| Aida Liminis         | 1 | ((230102-elwira-koszmar-nox-ignis)) |
| Brunon Szwagacz      | 1 | ((230102-elwira-koszmar-nox-ignis)) |
| Katrina Komczirp     | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |
| Persefona d'Loricatus | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |
| Riaon Diralik        | 1 | ((230102-elwira-koszmar-nox-ignis)) |
| Sarian Xadaar        | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |
| Wawrzyn Rewemis      | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |
---
categories: profile
factions: 
owner: public
title: Adrian Wężoskór
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190226-korporacyjna-wojna-w-mmo     | negocjator Sensus. Chce uniknąć wojny, ale nie da się osłabić. | 0110-03-13 - 0110-03-16 |
| 190406-bardzo-kosztowne-lzy         | oficer Sensus; współpracował z Pięknotką by uratować kompleks Sensus w Podwiercie. | 0110-03-17 - 0110-03-22 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Alan Bartozol        | 1 | ((190226-korporacyjna-wojna-w-mmo)) |
| Antoni Kotomin       | 1 | ((190226-korporacyjna-wojna-w-mmo)) |
| Dariusz Bankierz     | 1 | ((190226-korporacyjna-wojna-w-mmo)) |
| Eliza Kotlet         | 1 | ((190226-korporacyjna-wojna-w-mmo)) |
| Ida Tiara            | 1 | ((190406-bardzo-kosztowne-lzy)) |
| Kermit Szperacz      | 1 | ((190226-korporacyjna-wojna-w-mmo)) |
| Mi Ruda              | 1 | ((190226-korporacyjna-wojna-w-mmo)) |
| Pięknotka Diakon     | 1 | ((190406-bardzo-kosztowne-lzy)) |
| Rafał Królewski      | 1 | ((190226-korporacyjna-wojna-w-mmo)) |
| Wiktor Satarail      | 1 | ((190406-bardzo-kosztowne-lzy)) |
| Wojciech Słabizna    | 1 | ((190226-korporacyjna-wojna-w-mmo)) |
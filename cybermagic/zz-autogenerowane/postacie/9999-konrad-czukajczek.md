---
categories: profile
factions: 
owner: public
title: Konrad Czukajczek
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190626-upadek-enklawy-floris        | technomanta Floris, który stał się techorganicznym Skażeńcem ignorowanym przez Maszynę i wspieranym przez nią. Nieszczęśliwy jak cholera z tego powodu. | 0110-05-24 - 0110-05-27 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ariela Sirmin        | 1 | ((190626-upadek-enklawy-floris)) |
| Ernest Kajrat        | 1 | ((190626-upadek-enklawy-floris)) |
| Hubert Kraborów      | 1 | ((190626-upadek-enklawy-floris)) |
| Jolanta Teresis      | 1 | ((190626-upadek-enklawy-floris)) |
| Marcel Sowiński      | 1 | ((190626-upadek-enklawy-floris)) |
| Nikola Kirys         | 1 | ((190626-upadek-enklawy-floris)) |
| Roman Rymtusz        | 1 | ((190626-upadek-enklawy-floris)) |
| Szymon Maszczor      | 1 | ((190626-upadek-enklawy-floris)) |
| Wargun Ira           | 1 | ((190626-upadek-enklawy-floris)) |
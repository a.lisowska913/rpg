---
categories: profile
factions: 
owner: public
title: Szczepan Kaltaben
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220126-keldan-voss-kolonia-saitaera | pallidanin; drugi po Annice; na orbicie. Trzyma się ŚLEPO rozkazów Anniki nawet za cenę śmierci dużej ilości ludzi i postawienia się Eustachemu. | 0112-03-19 - 0112-03-22 |
| 220216-polityka-rujnuje-pallide-voss | okazuje się, że konspiruje przeciwko Annice i chce ją zniszczyć. | 0112-03-27 - 0112-03-29 |
| 220223-stabilizacja-keldan-voss     | powiedział Ariannie WSZYSTKO, oddał się w jej ręce wierząc, że to i tylko to go uratuje. | 0112-03-30 - 0112-04-02 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Annika Pradis        | 3 | ((220126-keldan-voss-kolonia-saitaera; 220216-polityka-rujnuje-pallide-voss; 220223-stabilizacja-keldan-voss)) |
| Arianna Verlen       | 3 | ((220126-keldan-voss-kolonia-saitaera; 220216-polityka-rujnuje-pallide-voss; 220223-stabilizacja-keldan-voss)) |
| Elena Verlen         | 3 | ((220126-keldan-voss-kolonia-saitaera; 220216-polityka-rujnuje-pallide-voss; 220223-stabilizacja-keldan-voss)) |
| Eustachy Korkoran    | 3 | ((220126-keldan-voss-kolonia-saitaera; 220216-polityka-rujnuje-pallide-voss; 220223-stabilizacja-keldan-voss)) |
| Klaudia Stryk        | 3 | ((220126-keldan-voss-kolonia-saitaera; 220216-polityka-rujnuje-pallide-voss; 220223-stabilizacja-keldan-voss)) |
| Mateus Sarpon        | 2 | ((220126-keldan-voss-kolonia-saitaera; 220216-polityka-rujnuje-pallide-voss)) |
| SP Pallida Voss      | 2 | ((220126-keldan-voss-kolonia-saitaera; 220216-polityka-rujnuje-pallide-voss)) |
| Kormonow Voss        | 1 | ((220126-keldan-voss-kolonia-saitaera)) |
| OO Kastor            | 1 | ((220126-keldan-voss-kolonia-saitaera)) |
| Raoul Lavanis        | 1 | ((220216-polityka-rujnuje-pallide-voss)) |
| SP Światło Nadziei   | 1 | ((220223-stabilizacja-keldan-voss)) |
| Tomasz Kaltaben      | 1 | ((220223-stabilizacja-keldan-voss)) |
| Zygfryd Maus         | 1 | ((220126-keldan-voss-kolonia-saitaera)) |
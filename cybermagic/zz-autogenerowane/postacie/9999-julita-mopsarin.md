---
categories: profile
factions: 
owner: public
title: Julita Mopsarin
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230314-brudna-konkurencja-w-arachnoziem | czarodziejka mentalna zatrudniona przez EnMilStrukt by ArachnoBuild nie uzyskał kontraktu. Jakkolwiek osiągnęła cel, Karolinus ją dopadł i magią zmienił ją w Fionę (jak i pół miasta). W chaosie dała radę się wymknąć. | 0095-06-20 - 0095-06-22 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| AJA Szybka Strzała   | 1 | ((230314-brudna-konkurencja-w-arachnoziem)) |
| Ania Turabnik        | 1 | ((230314-brudna-konkurencja-w-arachnoziem)) |
| Fiona Szarstasz      | 1 | ((230314-brudna-konkurencja-w-arachnoziem)) |
| Fircjusz Szarstasz   | 1 | ((230314-brudna-konkurencja-w-arachnoziem)) |
| Kacper Aczramin      | 1 | ((230314-brudna-konkurencja-w-arachnoziem)) |
| Karolinus Samszar    | 1 | ((230314-brudna-konkurencja-w-arachnoziem)) |
| Laura Turabnik       | 1 | ((230314-brudna-konkurencja-w-arachnoziem)) |
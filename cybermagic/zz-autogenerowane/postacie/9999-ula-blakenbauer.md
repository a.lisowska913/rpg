---
categories: profile
factions: 
owner: public
title: Ula Blakenbauer
---

# {{ page.title }}


# Generated: 



## Fiszki


* czarodziejka, arcymag płaszczek, senti-infuser | @ 230412-dzwiedzie-poluja-na-ser
* (ENCAO:  -0+0+ |Nie cierpi 'small talk';;;;Nieobliczalna, nie wiadomo co zrobi| VALS: Universalism, Self-direction >> Power| DRIVE: Duma i bycie uznaną) | @ 230412-dzwiedzie-poluja-na-ser
* styl: Raven z DC x Suspiria | @ 230412-dzwiedzie-poluja-na-ser

### Wątki


waśnie-samszar-verlen
waśnie blakenbauer-verlen
legendy-verlenlandu

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230412-dzwiedzie-poluja-na-ser      | ma teraz 22 lata. Arogancka mistrzyni płaszczek i sentiinfuzji. Współpracuje z Marcinozaurem szukając śladów Szeptomandry. Gdy Viorika zaproponowała jej wyzwanie, poszła w mimiki (jaskinia jest JEDNYM mimikiem). Pokonana przez verleńską taktykę i dźwiedzie, czego by się NIGDY nie spodziewała. Acz zaimponowała siłą ognia i magii. | 0095-07-12 - 0095-07-14 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Apollo Verlen        | 1 | ((230412-dzwiedzie-poluja-na-ser)) |
| Arianna Verlen       | 1 | ((230412-dzwiedzie-poluja-na-ser)) |
| Elena Verlen         | 1 | ((230412-dzwiedzie-poluja-na-ser)) |
| Marcinozaur Verlen   | 1 | ((230412-dzwiedzie-poluja-na-ser)) |
| Viorika Verlen       | 1 | ((230412-dzwiedzie-poluja-na-ser)) |
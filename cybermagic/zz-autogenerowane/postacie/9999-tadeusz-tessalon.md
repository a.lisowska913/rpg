---
categories: profile
factions: 
owner: public
title: Tadeusz Tessalon
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200509-rekin-z-aurum-i-fortifarma   | chciał pomścić despekt spowodowany przez Kołczonda; zintegrował się z Krwawym Potworem i po tym jak posiekał go Cień Pięknotki, został ciężko porażony. | 0110-08-26 - 0110-08-31 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 200509-rekin-z-aurum-i-fortifarma   | utracił część kontroli nad magią i część władzy nad ciałem. Nigdy nie będzie jak był kiedyś. | 0110-08-31

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Artur Kołczond       | 1 | ((200509-rekin-z-aurum-i-fortifarma)) |
| Artur Michasiewicz   | 1 | ((200509-rekin-z-aurum-i-fortifarma)) |
| Erwin Galilien       | 1 | ((200509-rekin-z-aurum-i-fortifarma)) |
| Gabriel Ursus        | 1 | ((200509-rekin-z-aurum-i-fortifarma)) |
| Natalia Tessalon     | 1 | ((200509-rekin-z-aurum-i-fortifarma)) |
| Pięknotka Diakon     | 1 | ((200509-rekin-z-aurum-i-fortifarma)) |
| Sabina Kazitan       | 1 | ((200509-rekin-z-aurum-i-fortifarma)) |
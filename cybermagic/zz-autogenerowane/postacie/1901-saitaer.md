---
categories: profile
factions: 
owner: public
title: Saitaer
---

# {{ page.title }}


# Read: 

## Postać

### Ogólny pomysł (3)

Terrorform. Godling. Bóg. Istota o **dwie kategorie** silniejsza niż powinna być. Pasożyt z wymarłego świata, który zainfekował Astorię. Kiedyś, bóg typu support - rekonstruktor. Mistrz techorganiki i magii krwi o aspekcie "Rekonstrukcja".

### Motywacja (gniew/wartość, zmiana, sposób) (3)

* PERFECTION/ADAPTABILITY; wszelkie życie jest nieśmiertelne i adaptujące; rekonstrukcja życia do techorganiki
* mój świat (Erelis) jest martwy; mój świat ożyje; wyssanie energii z Primusa by oddać ją Erelisowi
* niekompatybilność rzeczywistości ze Wzorem; cała rzeczywistość jest spójna ze Wzorem; Aspekt Rekonstrukcji

### Wyróżniki (3)

* Rekonstruktor. Potrafi przebudować i przetransformować wszystkie istoty w techorganiczne amalgamaty.
* Mistrz Magii Krwi. To, co my nazywamy "magią krwi" jest kanwą Saitaera - jego naturalną formą manipulacji rzeczywistości
* Skażenie. Boska energia potrafi przekształcić ciało i umysł jego oponentów.

### Zasoby i otoczenie (3)

* Nieliczni wyznawcy, ale zawsze wyznawcy

### Magia (3)

#### Gdy kontroluje energię

* otwiera portale
* transformuje istoty, tworzy życie
* Skaża magów i ludzi, jest w końcu bogiem

#### Gdy traci kontrolę

* nie dotyczy

### Powiązane frakcje

{{ page.factions }}

## Opis

.


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 181112-odklatwianie-ateny           | zamieszkujący ciało terrorforma corruptor oraz władca magii krwi. Okrutny. Chce wskrzesić swój świat i Naznaczył Atenę, Pięknotkę i Erwina. Klasyfikacja: demon? Bóstwo? | 0109-10-22 - 0109-10-25 |
| 190112-eksperymenty-na-wilach       | obudzony przez krew umierającej wiły i modlitwę Pięknotki, zbudował ołtarz na Trzęsawisku Zjawosztup poświęcając odbudowaną wiłę. | 0109-12-27 - 0109-12-30 |
| 190116-wypalenie-saitaera-z-trzesawiska | po raz kolejny odparty; tym razem wypalony z Wiktora Sataraila i z Trzęsawiska Zjawosztup. Nie podjął rękawicy Karradraela by rozpocząć wojnę totalną. | 0110-01-06 - 0110-01-08 |
| 190127-ixionski-transorganik        | zgłosił się do Pięknotki - zmusił ją do naprawy ixiońskiego transorganika który był Wojtkiem. Czemu? Bo to maladaptacja. | 0110-01-28 - 0110-01-29 |
| 190202-czarodziejka-z-woli-saitaera | robił eksperyment w transfuzji energii maga do człowieka. Aktywnie pomagał Pięknotce w maskowaniu tego eksperymentu. Niestety, nie dostał tego co chciał. | 0110-01-31 - 0110-02-04 |
| 190519-uciekajacy-seksbot           | wezwany przez Pięknotkę, odpowiedział, że pomoże seksbotowi i da mu albo wolność albo dobrą śmierć. O krok bliżej do ewolucji Pięknotki. | 0110-04-21 - 0110-04-22 |
| 190804-niespodziewany-wplyw-aidy    | wezwany przez Pięknotkę, zniszczył Hralwagha dla niej; pozyskał sporo kralotycznego adaptogenu na swoje eksperymenty. | 0110-06-26 - 0110-06-28 |
| 201011-narodziny-paladynki-saitaera | gdy Pięknotka umierała w walce z Cieniem, powiedział jej, że jej może pomóc. Pięknotka opierała się mu do końca, ale ją wskrzesił jako paladynkę - acz nie ma nad nią pełni kontroli. | 0110-10-08 - 0110-10-09 |
| 220105-to-nie-pulapka-na-nereide    | zaplanował przechwycenie Inferni, Eleny, lub Arianny. Co najmniej Natalii. Co prawda prawie zniszczył te wszystkie jednostki, ale nie udało mu się uzyskać ani jednego agenta. Nawet Natalia jest wolną istotą. | 0112-02-28 - 0112-03-04 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 181225-czyszczenie-toksycznych-zwiazkow | ma warm lead do Kreacjusza Diakona, osoby zajmującej się farmami klonów w okolicach Pustogoru. Wykona swój plan. | 0109-11-16
| 181226-finis-vitae                  | ma 10 dni spokojnej pracy nad swoimi mrocznymi planami. Tylko on, Minerwa oraz Kreacjusz. | 0109-11-26
| 181227-adieu-cieniaszczycie         | zapewnił odpowiednie spowolnienie Pięknotki przy jej powrocie do Pustogoru. Ma dość czasu - Minerwę i infekcję rzeczywistości. | 0109-11-30
| 181230-uwiezienie-saitaera          | uwięziony przez Pustogor. Pytanie na jak długo. | 0109-12-08
| 190202-czarodziejka-z-woli-saitaera | dowiedział się sporo odnośnie wykorzystania energii ixiońskiej do transfuzji energii magicznej od maga do człowieka. | 0110-02-04

## Plany


| Opowieść | Plan | Końcowa data |
| ---- | ---- | ---- |
| 181225-czyszczenie-toksycznych-zwiazkow | przejąć kontrolę nad pomniejszymi salami do klonowania. | 0109-11-16
| 190127-ixionski-transorganik        | wykorzystać ixiońską energię rezydualną na Wojtku Kurczynosie. | 0110-01-29
| 190202-czarodziejka-z-woli-saitaera | wszystkie plany w uzyskaniu nowej agentki (Karoliny Erenit) się nie powiodły. Trudno, nie dostał czego chciał. | 0110-02-04

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Pięknotka Diakon     | 8 | ((181112-odklatwianie-ateny; 190112-eksperymenty-na-wilach; 190116-wypalenie-saitaera-z-trzesawiska; 190127-ixionski-transorganik; 190202-czarodziejka-z-woli-saitaera; 190519-uciekajacy-seksbot; 190804-niespodziewany-wplyw-aidy; 201011-narodziny-paladynki-saitaera)) |
| Minerwa Metalia      | 3 | ((190127-ixionski-transorganik; 190202-czarodziejka-z-woli-saitaera; 201011-narodziny-paladynki-saitaera)) |
| Alan Bartozol        | 2 | ((190112-eksperymenty-na-wilach; 190116-wypalenie-saitaera-z-trzesawiska)) |
| Gabriel Ursus        | 2 | ((190804-niespodziewany-wplyw-aidy; 201011-narodziny-paladynki-saitaera)) |
| Karolina Erenit      | 2 | ((190127-ixionski-transorganik; 190202-czarodziejka-z-woli-saitaera)) |
| Ossidia Saitis       | 2 | ((190519-uciekajacy-seksbot; 190804-niespodziewany-wplyw-aidy)) |
| Sławomir Muczarek    | 2 | ((190112-eksperymenty-na-wilach; 190202-czarodziejka-z-woli-saitaera)) |
| Tymon Grubosz        | 2 | ((190127-ixionski-transorganik; 190202-czarodziejka-z-woli-saitaera)) |
| Wiktor Satarail      | 2 | ((190116-wypalenie-saitaera-z-trzesawiska; 190127-ixionski-transorganik)) |
| Wojtek Kurczynos     | 2 | ((190127-ixionski-transorganik; 190202-czarodziejka-z-woli-saitaera)) |
| Adela Kirys          | 1 | ((190202-czarodziejka-z-woli-saitaera)) |
| Agaton Ociegor       | 1 | ((201011-narodziny-paladynki-saitaera)) |
| Aleksander Muniakiewicz | 1 | ((201011-narodziny-paladynki-saitaera)) |
| Amadeusz Sowiński    | 1 | ((181112-odklatwianie-ateny)) |
| Amanda Kajrat        | 1 | ((190804-niespodziewany-wplyw-aidy)) |
| Arianna Verlen       | 1 | ((220105-to-nie-pulapka-na-nereide)) |
| Arnulf Poważny       | 1 | ((190519-uciekajacy-seksbot)) |
| ASD Centurion        | 1 | ((190116-wypalenie-saitaera-z-trzesawiska)) |
| Atena Sowińska       | 1 | ((181112-odklatwianie-ateny)) |
| Cezary Zwierz        | 1 | ((181112-odklatwianie-ateny)) |
| Elena Verlen         | 1 | ((220105-to-nie-pulapka-na-nereide)) |
| Eliza Ira            | 1 | ((190519-uciekajacy-seksbot)) |
| Ernest Kajrat        | 1 | ((190519-uciekajacy-seksbot)) |
| Erwin Galilien       | 1 | ((190127-ixionski-transorganik)) |
| Eustachy Korkoran    | 1 | ((220105-to-nie-pulapka-na-nereide)) |
| Hieronim Maus        | 1 | ((190116-wypalenie-saitaera-z-trzesawiska)) |
| Izabela Zarantel     | 1 | ((220105-to-nie-pulapka-na-nereide)) |
| Julia Morwisz        | 1 | ((190804-niespodziewany-wplyw-aidy)) |
| Kamil Lyraczek       | 1 | ((220105-to-nie-pulapka-na-nereide)) |
| Karradrael           | 1 | ((190116-wypalenie-saitaera-z-trzesawiska)) |
| Kasjopea Maus        | 1 | ((190116-wypalenie-saitaera-z-trzesawiska)) |
| Kirył Najłalmin      | 1 | ((190127-ixionski-transorganik)) |
| Klaudia Stryk        | 1 | ((220105-to-nie-pulapka-na-nereide)) |
| Kornel Garn          | 1 | ((190112-eksperymenty-na-wilach)) |
| Liliana Bankierz     | 1 | ((190519-uciekajacy-seksbot)) |
| Lucjusz Blakenbauer  | 1 | ((181112-odklatwianie-ateny)) |
| Lutus Amerin         | 1 | ((220105-to-nie-pulapka-na-nereide)) |
| Maria Naavas         | 1 | ((220105-to-nie-pulapka-na-nereide)) |
| Minerwa Diakon       | 1 | ((181112-odklatwianie-ateny)) |
| Napoleon Bankierz    | 1 | ((190127-ixionski-transorganik)) |
| Natalia Aradin       | 1 | ((220105-to-nie-pulapka-na-nereide)) |
| Sabina Kazitan       | 1 | ((201011-narodziny-paladynki-saitaera)) |
| Teresa Mieralit      | 1 | ((190519-uciekajacy-seksbot)) |
| Tomasz Tukan         | 1 | ((190519-uciekajacy-seksbot)) |
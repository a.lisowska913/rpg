---
categories: profile
factions: 
owner: public
title: Tobiasz Lobrak
---

# {{ page.title }}


# Generated: 



## Fiszki


* (nie Nativis), 44 lata | @ 230215-terrorystka-w-ambasadorce
* (ENCAO:  ---0+ |Lekceważy obowiązki;;Skryty;;Marzycielski | VALS: Face, Power >> Face| DRIVE: TORMENT) | @ 230215-terrorystka-w-ambasadorce
* dostawca dużej ilości luksusowych środków do Nativis po niższej cenie | @ 230215-terrorystka-w-ambasadorce
* uwielbia znęcać się nad kobietami. Upodobał sobie Misterię. | @ 230215-terrorystka-w-ambasadorce
* "Możesz mnie zabić, ale będę ZAWSZE żył w twojej głowie, mała" | @ 230215-terrorystka-w-ambasadorce

### Wątki


historia-eustachego
arkologia-nativis
zbrodnie-kidirona

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230215-terrorystka-w-ambasadorce    | straszy Misterię, że ona wpadnie w jego ręce. Obiecuje jej to. Zamknięty w panic roomie, Kidiron go wyciągnął. Ma powiązanie z Syndykatem Aureliona, ale chyba gra na dwa fronty bo Kidiron daje mu okazję zaspokojenia mroku. | 0093-02-22 - 0093-02-23 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 230215-terrorystka-w-ambasadorce    | wie, że Misteria chciała go zabić. Od tej pory Tobiasz skupia się na próbie pozyskania Misterii i założenia jej neuroobroży. She WILL comply. | 0093-02-23

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ardilla Korkoran     | 1 | ((230215-terrorystka-w-ambasadorce)) |
| Bartłomiej Korkoran  | 1 | ((230215-terrorystka-w-ambasadorce)) |
| Eustachy Korkoran    | 1 | ((230215-terrorystka-w-ambasadorce)) |
| Kalia Awiter         | 1 | ((230215-terrorystka-w-ambasadorce)) |
| Magda Misteria Sarbanik | 1 | ((230215-terrorystka-w-ambasadorce)) |
| Rafał Kidiron        | 1 | ((230215-terrorystka-w-ambasadorce)) |
| Ralf Tapszecz        | 1 | ((230215-terrorystka-w-ambasadorce)) |
| Tymon Korkoran       | 1 | ((230215-terrorystka-w-ambasadorce)) |
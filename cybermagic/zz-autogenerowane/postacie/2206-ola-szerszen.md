---
categories: profile
factions: 
owner: public
title: Ola Szerszeń
---

# {{ page.title }}


# Read: 

## Kim jest

### W kilku zdaniach

Ruda augmentowana kapitan Wścibskiego Wiewióra, córka projektu "Kwiat Paproci" i admirał Termii. Nieludzka, walczy o to by ludzie mogli być ludźmi i wszelkie terrageny były wykorzystywane do swoich perfekcyjnych ról w ramach zjednoczonej ludzkości. Żądli jak szerszeń, odpychając od siebie wszystkich ale lojalna swoim przełożonym. W czasie wolnym - próbuje zrozumieć niemoralne plany Orbitera by je zniszczyć.

### Co się rzuca w oczy

* Motto: "Marnujemy życia zwykłych bohaterów zamiast dopasować magitech do potrzeby. Tylko czysta ludzkość przetrwa kolejnego Saitaera."
    * Widoczna hipokrytka: mówi o czystych ludziach a ma mechaniczną dłoń prawą i augmentację oczu.
* Doskonale współpracuje z naukowcami i steruje "Wścibskim Wiewiórem" z niesamowitą precyzją.
* Nie ma opcji się z nią zaprzyjaźnić czy się do niej zbliżyć.
* Work-life balance polega na tym, że ona dzieli swój czas na "pracę" i "więcej pracy". I sen.
* Jej zdaniem - lepsza śmierć niż Skażenie / za głęboka augmentacja.

### Jak sterować postacią

* Złośliwa jak szerszeń. Żądli na lewo i prawo by zostawili ją samą w spokoju. Nie integruje się, nie socjalizuje się.
    * stupid plastered smile and obviously fake optimism
* Wobec załogi i w sytuacjach taktycznych jest opanowana, z iskierką szaleństwa. She is a presence. Does not hit down.
* Zafascynowana wszystkimi augmentacjami i AI, ale twardo twierdzi, że rolą człowieka jest zostać człowiekiem.
    * jest jawną zwolenniczką teorii 'terragenów' a nie ludzi - budowa istot do potrzeb / upliftowanie.
* Visibly shows her disappointment.
* Zdecydowana i bezpośrednia. NIGDY nie manipuluje / oszukuje za plecami swoich przełożonych.    
* Próbuje oczyścić Orbiter (i sferę ludzkości) z chorych eksperymentów, augmentacji, anomalizacji itp.

## Mechanika

### Znaczące Czyny i Osiągnięcia (3)

* Współorganizatorka kampanii "augmentacja gorsza od śmierci" - przygotowała dane i badania i zniechęciła sporo ludzi i agentów Orbitera do augmentacji magią czy technologią.
* Podczas wypadku wahadłowca w dokach neurozintegrowała się z pojazdami ratunkowymi i uratowała kilka osób. Poparzenia wysokiego stopnia.
* 

### Co się rzuca w oczy: Atuty, Przewagi, Zasoby (3, 6)

* PRACA: kapitan Orbitera - potrafi porwać za sobą innych, jest niezłomna i nieustraszona
* PRACA: ZŁOŚLIWA JAK CHOLERA. Mistrzyni odpychania ludzi, wyprowadzania ich z równowagi, prowokowania i ściągania uwagi na siebie
* PRACA: naukowiec; zafascynowana szczególnie AI i augmentacjami
* ATUT: neurosprzężona. Oraz twinowana z TAI nieznanej klasy - daje jej to niezwykłą precyzję, prędkość obliczeniową itp.
* AUGMENT: zielone, mechaniczne oczy dające jej nieludzki wygląd, lepszą detekcję i widzenie w ciemnościach
* AUGMENT: prawa dłoń jest syntetyczna, daje jej lepsze połączenie z urządzeniami badawczymi i technologiami
* COŚ: kapitan statku "Wścibski Wiewiór", badawczego i bardzo precyzyjnego z sił admirał Termii.

### Typowe problemy z którymi sobie nie radzi; Słabości (-3)

* CORE WOUND: "I am not even real. I am a blasphemy created by corrupted mother. I am not a person. I should not exist, but... I want to."
* CORE LIE: Lepsza śmierć niż Skażenie / za duża augmentacja. Jako, że dla niej za późno - jest już stracona.
* Wrażliwa na ataki organiczne oraz psychotroniczne. Wrażliwa na ataki ze wszystkich możliwych kanałów.
* Nadwrażliwa na energię magiczną i na magię. Jej struktura jest mało stabilna.
* Nie ma nadziei. Nie ma w niej przekonania, że jest w stanie cokolwiek zrobić. Falls to despair. "Never had a real chance at all"
* She cannot get used to losing agents, friends and soldiers.

### Serce i Wartości (3)

* Wartości
    * TAK: Universalism (każdy ma swoją rolę i każdemu dać szansę i miejsce), Tradition ("duty bounds all of us" + "purity of form"), Security ("no-one should fall!")
    * NIE: Stimulation + Achievement ("you could does not mean you SHOULD")
    * Dla każdej istoty chętnej do działania w strukturach ludzkości Ola znajdzie właściwe miejsce i dobrze wykorzysta
    * Skonfliktowana między nienawiścią do Termii ("jak mogłaś") a tym, że Termia dała jej to co ona chce dać innym.
    * Staje naprzeciw wszelkim próbom augmentacji odbierającej osobowość / człowieczeństwo / przyszłość. Staje naprzeciw za silnym eksperymentom.
* Ocean
    * E:0, N:+, C:+, A:+, O:+
    * Wiecznie paranoiczna, że jej status wyjdzie na jaw. Żądli defensywnie na lewo i prawo - i jest w tym świetna. Długo chowa urazę.
    * Zdecydowana i dość bezpośrednia. NIGDY nie manipuluje / oszukuje za plecami swoich przełożonych.    
* Silnik
    * Uratować czystość ludzkości - każda osoba uratowana od niepotrzebnej augmentacji / Skażenia to jej prywatny sukces
    * Projekt 'terragen' - jedność i sojusz dla wszelkich istot. TAI, ludzie, androidy itp.

### Magia (3M)

#### W czym jest świetna

* brak

#### Jak się objawia utrata kontroli

* brak

## Inne

### Wygląd

* Krótkie, rude włosy, zdrowa cera, piegowata.
* Augmentowane, jasnozielonkawe oczy dające jej lepsze spektrum i widzenie w ciemnościach.
* Syntetyczna prawa dłoń, dające jej interfejsowanie się z konsolami i urządzeniami badawczymi.

### Coś Więcej

* Stworzona jako element projektu "Kwiat Paproci" (nieśmiertelność) jako tajuk (klon z elementami syntetycznymi z imprintem oryginału; po nazwiskach naukowców) admirał Aleksandry Termii.

### Endgame

* ?


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220615-lewiatan-przy-seibert        | Arianna dostała ją w prezencie od Termii, wraz ze Wścibskim Wiewiórem. W niełaskach u Termii, nazwana 'jak ja ale naiwna', kiedyś twarz kampanii anty-augmentacji. Brutalnie postawiła Klaudię gdy tą złapał bazyliszek Lewiatana. Kompetentna kapitan która chce ratować wszystkich. | 0112-07-23 - 0112-07-25 |
| 220622-lewiatan-za-pandore          | analizowała Regenerator Eleny, próbując zadbać o jej komfort nawet jeśli Elena nic nie czuje / nie wie. Złośliwa, napadła słownie Eustachego i channeluje to jako słodka korpoentuzjastyczna idiotka. Ma autorepair system; nie pozwala nikomu się zbadać ani zbliżyć do siebie. Dowodzi Wścibskim Wiewiórem. | 0112-07-26 - 0112-07-29 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 220622-lewiatan-za-pandore          | wyszło na jaw, że jest jakąś formą biosynta, eksperymentu admirał Termii. Jej reputacja (która i tak nie istniała) w strzępach. Potencjalnie - nie ma praw. | 0112-07-29

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 2 | ((220615-lewiatan-przy-seibert; 220622-lewiatan-za-pandore)) |
| Eustachy Korkoran    | 2 | ((220615-lewiatan-przy-seibert; 220622-lewiatan-za-pandore)) |
| Klaudia Stryk        | 2 | ((220615-lewiatan-przy-seibert; 220622-lewiatan-za-pandore)) |
| Eszara d'Seibert     | 1 | ((220615-lewiatan-przy-seibert)) |
| Jonasz Parys         | 1 | ((220615-lewiatan-przy-seibert)) |
| Maria Naavas         | 1 | ((220622-lewiatan-za-pandore)) |
| OO Infernia          | 1 | ((220622-lewiatan-za-pandore)) |
| OO Pandora           | 1 | ((220622-lewiatan-za-pandore)) |
| OO Straszliwy Pająk  | 1 | ((220622-lewiatan-za-pandore)) |
| OO Tivr              | 1 | ((220622-lewiatan-za-pandore)) |
| Raoul Lavanis        | 1 | ((220622-lewiatan-za-pandore)) |
| Rzeźnik Parszywiec Diakon | 1 | ((220622-lewiatan-za-pandore)) |
---
categories: profile
factions: 
owner: public
title: Emilia Lawendowiec
---

# {{ page.title }}


# Generated: 



## Fiszki


* aktualna czempionka | @ 230124-kjs-wygrac-za-wszelka-cene
* (ENCAO:  -0-0+ |Mało punktualna.;;Chętna do eksperymentowania| VALS: Achievement, Conformity| DRIVE: Queen Bee) | @ 230124-kjs-wygrac-za-wszelka-cene
* chce być najlepsza i dlatego bierze prywatne lekcje od Apollo. Płaci za to ciałem. | @ 230124-kjs-wygrac-za-wszelka-cene

### Wątki


kajis
mroczna-strona-verlenow

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230124-kjs-wygrac-za-wszelka-cene   | aktualna championka Koszarowa Chłopięcego; uczestniczy w specjalnych treningach Apollo Verlena. | 0095-06-23 - 0095-06-25 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Apollo Verlen        | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Elena Verlen         | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Karol Atenuatia      | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| KDN Kajis            | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Lidia Nemert         | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Lucas Oktromin       | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Marta Krissit        | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Serena Krissit       | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Wojciech Namczak     | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Zuzanna Kraczamin    | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Żaneta Krawędź       | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
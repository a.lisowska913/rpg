---
categories: profile
factions: 
owner: public
title: Malictrix d'Pandora
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200610-ixiacka-wersja-malictrix     | zrzucona przez Sarantę na stację Telira-Melusit VII. Doszło do transferu ixiackiego; nabrała sadystycznych cech i szczątków osobowości. | 0111-03-03 - 0111-03-07 |
| 210414-dekralotyzacja-asimear       | szczęśliwa jak świnia w błocie; na zlecenie Arianny zrobiła AI-kill i przechodząc przez Entropika Jolanty ("Eleny") trafiła Bazyliszkiem w Tirakala wysmażając Morrigan d'Tirakal. I Entropika też ;-). | 0111-09-05 - 0111-09-14 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 2 | ((200610-ixiacka-wersja-malictrix; 210414-dekralotyzacja-asimear)) |
| Klaudia Stryk        | 2 | ((200610-ixiacka-wersja-malictrix; 210414-dekralotyzacja-asimear)) |
| Elena Verlen         | 1 | ((210414-dekralotyzacja-asimear)) |
| Eustachy Korkoran    | 1 | ((210414-dekralotyzacja-asimear)) |
| Kamil Lyraczek       | 1 | ((210414-dekralotyzacja-asimear)) |
| Leona Astrienko      | 1 | ((200610-ixiacka-wersja-malictrix)) |
| Llarnagraht          | 1 | ((210414-dekralotyzacja-asimear)) |
| Mariusz Tubalon      | 1 | ((210414-dekralotyzacja-asimear)) |
| Martyn Hiwasser      | 1 | ((210414-dekralotyzacja-asimear)) |
| Martyna Bianistek    | 1 | ((210414-dekralotyzacja-asimear)) |
| Melwin Sito          | 1 | ((200610-ixiacka-wersja-malictrix)) |
| SCA Płetwal Błękitny | 1 | ((210414-dekralotyzacja-asimear)) |
| Tomasz Sowiński      | 1 | ((210414-dekralotyzacja-asimear)) |
---
categories: profile
factions: 
owner: anadia
title: Marysia Sowińska
---

# {{ page.title }}


# Read: 

## Kim jest

### W kilku zdaniach

Dość psotna arystokratka bardzo skupiona na tym, by zachować neutralność i niezależność. Bardzo chce wyjść z cienia swojej kuzynki Amelii i bardzo irytuje ją porównywanie do niej. 

Nie chce wychodzić za mąż i chce zachować równowagę między siłami - oraz być największą rybą w stawie, nieważne jak mały staw by nie był.

### Co się rzuca w oczy

* Bardzo elegancko ubrana.
* Nie jest fanką krzywdzenia kogokolwiek (poza Torszeckim).
* Gdy musi, jest bezwzględna. Gdy może, jest sympatyczna.
* Zadaje się z osobami różnych stanów, niespecjalnie dba o zasady i etykietę.
* Ma tendencje do bardzo nietypowych i momentami ryzykownych działań. Przekonana, że sobie sama poradzi (ku rozpaczy osób z Aurum którym na niej zależy).
* Nie jest przekonana, że komuś z Aurum na niej zależy.

### Jak sterować postacią

* wybiera raczej ogólne dobro niż opinię o swoim życiu prywatnym (plotki można uciszać ;) )
* chce by się z nią liczono (przynajmniej na poziomie Pustogoru)
* będzie się starała jak najdłużej utrzymać status "Szwajcarii" (nie wybierać stron i nie pokazać co uważa)
* Marysia jak była mała miała jako większość przyjaciół koty. Stąd upodobała sobie koty jako kochaną formę i przyjaciół.

## Mechanika

### Znaczące Czyny i Osiągnięcia (3)

* W Aurum, zwykle nie traktowano jej poważnie. By podsłuchiwać o czym inni mówią, zmieniała się w kota i zakradała w miejsca pozornie niemożliwe.
* W Aurum kolega Marysi był oskarżony o coś czego nie zrobił. Marysia wiedziała że jest niewinny (bo była z nim wtedy). Wściekła się, zebrała dowody, po czym publicznie go uniewinniła. Potem dostała OPR od możnych rodu, bo przez to ucierpiał ktoś inny kto był jej lojalny.
* Rafael Bankierz chciał dobrze wyjść za Marysię i zrobić z niej kochankę. Marysi się nie podobało jego podejście. Więc nie powiedziała "nie", ale tak manewrowała, że on się zbłaźnił strasznie a ona wysłała sygnał. "Nie bawcie się tak ze mną."

### Co się rzuca w oczy: Atuty i Przewagi (3, 6)

* AKCJA: świetnie radzi sobie w przestrzeni (czy na ścigaczu czy jako osa). Istota powietrza; świetnie czuje się w powietrzu.
* AKCJA: doskonale czyta innych. Świetnie wykrywa intencje - czemu ktoś coś mówi czy robi.
* AKCJA: imperatrix: władcza i rozkazująca, gdy chce, potrafi ZMUSIĆ lub KAZAĆ. Ludzie się jej słuchają bo za nią stoi potęga rodu.
* AKCJA: wybitna aktorka i kłamca pierwszej wody. Powie to, co inni chcą usłyszeć a oni powiedzią jej to, co ona chce usłyszeć. W czym oni powiedzą prawdę. Mistrzowska manipulatorka.
* AKCJA: zwierzę polityczne: wie skąd wieje wiatr i kto, z kim, dlaczego i za ile. Umie te rzeczy rozgrywać.
* COŚ: bogactwo i reputacja wynikająca z Rodu Sowińskich. Jest w rodzie lubiana, acz nie jest uważaną za ważną.

### Serce i Wartości (3)

* Samosterowność
    * Nikt nigdy nie będzie jej mówił co ma robić. Będzie na szczycie albo wcale.
    * Lepiej być królem wśród Rekinów niż szprotką wśród Sowińskich 
    * Nie wolno odrzucać nietypowych, może skandalicznych możliwości i metod
* Potęga
    * Będę miała taki wpływ że to ja decyduje o losie innych a nie oni o moim.
    * Dopóki nie mam odpowiedniej mocy, będę „neutralna”. Rozgrywam innych.
    * Chronię innych i mam prestiż, bo oni budują moją potęgę - i są warci ochrony
* Uniwersalizm
    * Nieważne jakiego ktoś jest stanu, ma znaczenie tylko co wnosi i jaką jest osobą
    * Ja decyduje kto ma wartość i z kim się zadaję a nie jakieś zasady lub ktoś inny
    * Żadna organizacja nie może zdominować jednostki. Równowaga pomiędzy siłami.

### Typowe problemy z którymi sobie nie radzi; Słabości (-3)

* CORE WOUND: ""
* CORE LIE: ""
* ma dużą słabość do zwierzaków, nie pozwoli by robione była zwierzakom krzywda
* ma dużą słabość do bliskich osób (Karolina, NIE Torszecki) i im też pomoże i nie da zrobić krzywdy.
* da się ją skusić na bardzo ryzykowne manewry samym faktem, że nikt tego się nie spodziewa

### Magia (3M)

#### W czym jest świetna

* mentalistka: czytanie pamięci, przerażanie, kontrola uczuć, dominacja (kontrola umysłów, rozkazy)
* morfka: transformowanie siebie i innych w zwierzaki. Musi mieć prototyp ("coś co istnieje").
* bioaktorka: potrafi zmieniać wygląd - inne włosy, postura, wielkość, twarz...
* elementalistka Powietrza: latanie, poruszanie się w przestrzeni, wysysanie powietrza (bąbel próżniowy)
* tarcze mentalne i ukrywanie własnych emocji

#### Jak się objawia utrata kontroli

* osoby postronne zauważają jak osoby / wydarzenia co do których Marysia nie chce by były znaczące stają się znaczące ("Torszecki x Marysia = WM" lub "Amelia dużo zrobiła")
* manifestacja Powietrza jako tornada, wichry, "rzeczy lecą w powietrze" itp.
* istoty dookoła przekształcają się w słodkie kotki. Czasem łącznie z Marysią.

### Specjalne

* .

## Inne

### Wygląd

* 

### Coś Więcej

* .

### Endgame

* .


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210323-grzybopreza                  | porwała Myrczka z okna akademika, wymanewrowała Napoleona na ścigaczu (wrzucając go do jeziora), po czym zorganizowała grzyboprezę i ściągnęła na nią Kacpra Bankierza - na złość terminusce. | 0111-04-18 - 0111-04-19 |
| 210406-potencjalnie-eksterytorialny-seksbot | rok temu uratowała Pożeracza przed torturami ze strony Sabiny a 3 miesiące temu zatrzymała agresywnie napalonego Mysiokornika każąc Lorenie go upić. Teraz: zakazała używać rytuałów na seksboty. | 0111-04-23 - 0111-04-24 |
| 210518-porywaczka-miragentow        | ostrzelana przez miragenta zrobiła świetne uniki, przeraziła Lorenę i zmusiła ją do wyznania prawdy o miragencie i przekonała Dianę do oddania jej miragenta. Ogólnie, dobry dzień ;-). | 0111-05-03 - 0111-05-04 |
| 210615-skradziony-kot-olgi          | ratuje swoje karaoke deeskalując sytuację w Lesie Trzęsawnym oraz odkrywa, że Marek ukradł somnibela Oldze. Przekonała Arkadię do oddania somnibela. | 0111-05-09 - 0111-05-11 |
| 210622-verlenka-na-grzybkach        | przeprowadziła dochodzenie - kto chciał zestrzelić robota kultywacyjnego Julii? Znalazła Arkadię na grzybkach i współpracując z Ulą ją unieszkodliwiła podstępem i fortelem. | 0111-05-26 - 0111-05-27 |
| 210713-rekin-wspiera-mafie          | władczyni marionetek; Torszeckim znalazła Kacpra Bankierza, gdy ów nie chciał przestać współpracować z mafią to używając Uli dotarła do Tukana, uwolniła nim Arkadię i poszczuła nią Kacpra. | 0111-06-02 - 0111-06-05 |
| 210720-porwanie-daniela-terienaka   | uratowała Torszeckiego przed Karoliną. Wykryła, że brata Karoliny (Daniela) porwała Ula. Zmusiła Ulę do współpracy politycznym manewrem i zaintrygowana terminuską doszła do tego kim Ula jest i skąd ta nienawiść. | 0111-06-10 - 0111-06-12 |
| 210817-zgubiony-holokrysztal-w-lesie | dowiedziawszy się o ko-matrycy Kuratorów wypchnęła Kacpra Bankierza z mafii (i tego terenu). Potem przekazała Tukanowi wolną rękę - niech usunie ko-matrycę i uderzy w mafię. | 0111-06-18 - 0111-06-20 |
| 210824-mandragora-nienawidzi-rekinow | wpierw przeskanowała umysł nieprzytomnego Daniela by dowiedzieć się co go "zeżarło", potem broniąc Ekaterinę przed mandragorą postawiła jej potężną tarczę mentalną. Chroni Rekiny przed mandragorą z powinności i z ciekawości. | 0111-06-29 - 0111-07-01 |
| 210831-serafina-staje-za-wydrami    | używa systemów Sowińskich by odkryć informację o Serafinie irze, przy użyciu Torszeckiego się z nią spotkała i przekonała do spowolnienia wspierania Wydr przeciw Rekinom. | 0111-07-03 - 0111-07-06 |
| 210921-przybycie-rekina-z-eterni    | odkryła sekrety Amelii Sowińskiej - zarówno ten o zakazanej miłości jak i to, że to nie Amelia zabiła ludzi. Zaczęła pracę nad akceptacją Ernesta wśród Rekinów i manipulacjami pousuwała większość oczywistych zagrożeń. Okazuje się, że ma kuzyna Lucjana który ją lubi. | 0111-07-16 - 0111-07-21 |
| 210928-wysadzony-zywy-scigacz       | przekonała do siebie Ernesta i używając Torszeckiego wyciągnęła z Ernesta jego sekrety i relację z Amelią. Przekonała go też, żeby nie robił nic sam. Gdy Ernest stracił nad sobą kontrolę, zatrzymała manifestację simulacrum i przekonała (ponownie) by oddał jej śledztwo. Dobija ją porównanie z Amelią - "że obie są paladynkami". Nonsens. Amelia nie jest ;p. | 0111-07-22 - 0111-07-23 |
| 211012-torszecki-pokazal-kregoslup  | dostała nowe zadanie od Dworu Sowińskich - zdobyć krew "córki Morlana" ze szpitala Pustogorskiego. Dotarło do niej, że jej traktowanie Torszeckiego częściowo spowodowało tą katastrofę ze zniszczeniem ścigacza Ernesta. | 0111-07-25 - 0111-07-26 |
| 211026-koszt-ratowania-torszeckiego | wzięła Pawła Szprotkę pod ochronę przed Eternią, po czym wynegocjowała z Satarailem, że on da jej Robaka do zainfekowania Torszeckiego (by go chronić). Wszystko zaczyna wyglądać jakby podkochiwała się w Torszeckim. | 0111-07-28 - 0111-08-01 |
| 211102-satarail-pomaga-marysi       | wyczaiła, że z Sensacjuszem jest coś nie tak (infekcja Owadem), ściągnęła Karolinę i z Karo wrzuciła go do glisty (sama wpadając). Sama poleciała jako owad pokonać zOwadzonego Samszara. Ona chce naprawić to SAMA. | 0111-08-05 - 0111-08-06 |
| 211123-odbudowa-wedlug-justyniana   | tymczasowo przeprowadziła się do Ernesta; przekonała go do dania szansy Rekinom. Dowiedziawszy się o działaniach Justyniana pokazała mu, że ma lepszy plan od niego (plan Azalii). Będzie walczyć o utrzymanie władzy. | 0111-08-07 - 0111-08-16 |
| 211207-gdy-zabraknie-pradu-rekinom  | nie wiedziała, że musi być Oficjalnym Przedstawicielem Aurum (dzięki, Amelia), więc nim nie była i odcięli prąd. Szybko załatwiła prąd z innego źródła i deeskalowała ryzyko, że ktoś na nią spojrzy; zrzuciła ogień na Arieników. | 0111-08-26 - 0111-08-27 |
| 211221-chevaleresse-infiltruje-rekiny | obserwuje Chevaleresse przez Hestię i z nią dyskutuje. Przekonała Justyniana, że trzeba zmiażdżyć Kult Ośmiornicy i odzyskać Stasia Arienika. | 0111-09-02 - 0111-09-03 |
| 211228-akt-o-ktorym-marysia-nie-wie | gdy dowiedziała się, że jej akt krąży po Zaczęstwie to wykorzystała go do sprawienia, by Ernest nie chciał spać po nocy ;-). Uwodzi Ernesta, by odbić go Amelii. Dla niej Daniel zbił Torszeckiego który ją chronił. | 0111-09-05 - 0111-09-08 |
| 220111-marysiowa-hestia-rekinow     | zamawia wzmocnienia dla Hestii, znajduje dziewczynę dla Myrczka i doprowadza do silnej korupcji Hestii - Hestia jest jej przyjaciółką i sojuszniczką teraz. | 0111-09-12 - 0111-09-15 |
| 220222-plaszcz-ochronny-mimozy      | by doprowadzić do stabilizacji Ernesta, chce zapewnić by Lorena narysowała ten obiecany akt XD. Deeskaluje konflikt Mimoza - Ernest (tymczasowo), ale stabilizuje relację swoją z Mimozą. | 0111-09-17 - 0111-09-21 |
| 220802-gdy-prawnik-przyjdzie-po-rekiny | wprowadziła się do Ernesta, splątała go ze sobą i przekierowała na siebie jego uczucia. Jednocześnie przekonała go, żeby nie atakował Mimozy tylko dał jej rozwiązać sprawę. | 0111-10-05 - 0111-10-07 |
| 220816-jak-wsadzic-ule-alanowi      | skomplikowany problem - Mimoza blokuje części do Hestii bo ratuje region, Karo prosi o wsadzenie Uli Alanowi ale audyt i vistermin... Marysia zbiera dane i się nieco miota. | 0111-10-08 - 0111-10-09 |
| 220923-wasale-zza-muru-pustogorskiego-PLC |  | 0111-10-10 - 0111-10-13 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 210323-grzybopreza                  | Urszula Miłkowicz, uczennica terminusa ma z nią kosę. Ale Kacper Bankierz ma u niej duży szacunek za pojechanie Napoleona a potem wpakowanie terminusów w "pułapkę" czystą grzyboprezą Myrczka. | 0111-04-19
| 210518-porywaczka-miragentow        | ma dostęp do superkomputera Sowińskich w swoich apartamentach w Podwiercie. A tam większość plotek z Aurum ;-). | 0111-05-04
| 210622-verlenka-na-grzybkach        | jawnie kolaboruje z terminusami wykorzystując ich jako broń przeciwko Arkadii. "Wrogowie Sowińskich kończą w pierdlu". Jest bezwzględną Sowińską. | 0111-05-27
| 210824-mandragora-nienawidzi-rekinow | w oczach Rekinów Marysia wygnała Kacpra by przejąć mafię. | 0111-07-01
| 210824-mandragora-nienawidzi-rekinow | Amelia jej na tyle nie lubiła, że wywaliła ją na prowincję by była Rekinem. | 0111-07-01
| 210824-mandragora-nienawidzi-rekinow | Sensacjusz Diakon jest święcie przekonany, że plotki o mafii x Marysi to 100% prawda. I uważa, że Marysia jest jak Amelia. | 0111-07-01
| 210921-przybycie-rekina-z-eterni    | osobiście odpowiedzialna by Ernestowi Namertelowi nic się nie stało (manipulacja Amelii). Jej słaby punkt. | 0111-07-21
| 210921-przybycie-rekina-z-eterni    | ma świetny materiał do szantażu / mezaliansu - Amelia Sowińska kocha się w Erneście Namertelu z Eterni, a przecież Amelia ma dostęp do rzeczy których Eternia nie powinna nigdy wiedzieć. | 0111-07-21
| 210928-wysadzony-zywy-scigacz       | ma przyjaźń i zaufanie Ernesta Namertela. | 0111-07-23
| 211026-koszt-ratowania-torszeckiego | pojawiają się plotki na temat jej romansu z Torszeckim. Na razie ignoruje. | 0111-08-01
| 211026-koszt-ratowania-torszeckiego | Sensacjusz Diakon uważa ją za zagubioną młodą kobietę, ale zdolną do miłości. Nie to co Amelia. Czyli Sensacjusz jest potencjalnym sojusznikiem. | 0111-08-01
| 211102-satarail-pomaga-marysi       | zdjęcie w śluzie od glisty trafiło do prasy. Są plotki, że wszystko dla Torszeckiego. Rekiny kibicują cicho ich zakazanej miłości (czyli Torszeckiemu). | 0111-08-06
| 211123-odbudowa-wedlug-justyniana   | podobno jest "zabawką" Ernesta bo on jest taki dobry w łóżku. | 0111-08-16
| 211123-odbudowa-wedlug-justyniana   | stworzyła tak dobry plan odbudowy Dzielnicy Rekinów, że Rekiny uznają jej wartość (plan wymyśliła Azalia ale Marysia go przywłaszczyła). | 0111-08-16
| 211207-gdy-zabraknie-pradu-rekinom  | została Oficjalnym Przedstawicielem Aurum wśród Rekinów. Współpracuje z nią Hestia d'Rekiny. | 0111-08-27
| 211228-akt-o-ktorym-marysia-nie-wie | wyciągnęła od Liliany Bankierz informację pośrednio (trackerem) kto jest autorem aktu Marysi (Lorena). Przez to zaskarbiła sobie jej wściekłość i zemstę. | 0111-09-08
| 220111-marysiowa-hestia-rekinow     | Rekiny widzą, że Marysia stoi za "państwem policyjnym" wśród Rekinów. | 0111-09-15
| 220802-gdy-prawnik-przyjdzie-po-rekiny | zamieszkała w Apartamencie Ernesta, "by mieć nań większy wpływ". Rekiny uważają że to dobry pomysł. | 0111-10-07

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Karolina Terienak    | 15 | ((210720-porwanie-daniela-terienaka; 210824-mandragora-nienawidzi-rekinow; 210831-serafina-staje-za-wydrami; 210921-przybycie-rekina-z-eterni; 210928-wysadzony-zywy-scigacz; 211012-torszecki-pokazal-kregoslup; 211102-satarail-pomaga-marysi; 211123-odbudowa-wedlug-justyniana; 211207-gdy-zabraknie-pradu-rekinom; 211221-chevaleresse-infiltruje-rekiny; 211228-akt-o-ktorym-marysia-nie-wie; 220111-marysiowa-hestia-rekinow; 220222-plaszcz-ochronny-mimozy; 220816-jak-wsadzic-ule-alanowi; 220923-wasale-zza-muru-pustogorskiego-PLC)) |
| Rafał Torszecki      | 11 | ((210622-verlenka-na-grzybkach; 210713-rekin-wspiera-mafie; 210720-porwanie-daniela-terienaka; 210831-serafina-staje-za-wydrami; 210921-przybycie-rekina-z-eterni; 210928-wysadzony-zywy-scigacz; 211012-torszecki-pokazal-kregoslup; 211026-koszt-ratowania-torszeckiego; 211102-satarail-pomaga-marysi; 211123-odbudowa-wedlug-justyniana; 211228-akt-o-ktorym-marysia-nie-wie)) |
| Ernest Namertel      | 8 | ((210921-przybycie-rekina-z-eterni; 210928-wysadzony-zywy-scigacz; 211102-satarail-pomaga-marysi; 211123-odbudowa-wedlug-justyniana; 211228-akt-o-ktorym-marysia-nie-wie; 220111-marysiowa-hestia-rekinow; 220222-plaszcz-ochronny-mimozy; 220802-gdy-prawnik-przyjdzie-po-rekiny)) |
| Daniel Terienak      | 7 | ((210720-porwanie-daniela-terienaka; 210824-mandragora-nienawidzi-rekinow; 210831-serafina-staje-za-wydrami; 211123-odbudowa-wedlug-justyniana; 211207-gdy-zabraknie-pradu-rekinom; 211228-akt-o-ktorym-marysia-nie-wie; 220923-wasale-zza-muru-pustogorskiego-PLC)) |
| Lorena Gwozdnik      | 7 | ((210406-potencjalnie-eksterytorialny-seksbot; 210518-porywaczka-miragentow; 210831-serafina-staje-za-wydrami; 211102-satarail-pomaga-marysi; 211207-gdy-zabraknie-pradu-rekinom; 211228-akt-o-ktorym-marysia-nie-wie; 220222-plaszcz-ochronny-mimozy)) |
| Julia Kardolin       | 6 | ((210323-grzybopreza; 210406-potencjalnie-eksterytorialny-seksbot; 210518-porywaczka-miragentow; 210615-skradziony-kot-olgi; 210622-verlenka-na-grzybkach; 210713-rekin-wspiera-mafie)) |
| Tomasz Tukan         | 6 | ((210713-rekin-wspiera-mafie; 210720-porwanie-daniela-terienaka; 210817-zgubiony-holokrysztal-w-lesie; 210824-mandragora-nienawidzi-rekinow; 210921-przybycie-rekina-z-eterni; 220816-jak-wsadzic-ule-alanowi)) |
| Liliana Bankierz     | 5 | ((210615-skradziony-kot-olgi; 210622-verlenka-na-grzybkach; 210817-zgubiony-holokrysztal-w-lesie; 211228-akt-o-ktorym-marysia-nie-wie; 220111-marysiowa-hestia-rekinow)) |
| Sensacjusz Diakon    | 5 | ((210824-mandragora-nienawidzi-rekinow; 211012-torszecki-pokazal-kregoslup; 211026-koszt-ratowania-torszeckiego; 211102-satarail-pomaga-marysi; 211207-gdy-zabraknie-pradu-rekinom)) |
| Urszula Miłkowicz    | 5 | ((210323-grzybopreza; 210622-verlenka-na-grzybkach; 210713-rekin-wspiera-mafie; 210720-porwanie-daniela-terienaka; 220816-jak-wsadzic-ule-alanowi)) |
| Arkadia Verlen       | 4 | ((210615-skradziony-kot-olgi; 210622-verlenka-na-grzybkach; 210921-przybycie-rekina-z-eterni; 211207-gdy-zabraknie-pradu-rekinom)) |
| Ignacy Myrczek       | 4 | ((210323-grzybopreza; 210622-verlenka-na-grzybkach; 211026-koszt-ratowania-torszeckiego; 211123-odbudowa-wedlug-justyniana)) |
| Keira Amarco d'Namertel | 4 | ((210928-wysadzony-zywy-scigacz; 211228-akt-o-ktorym-marysia-nie-wie; 220111-marysiowa-hestia-rekinow; 220222-plaszcz-ochronny-mimozy)) |
| Triana Porzecznik    | 4 | ((210323-grzybopreza; 210406-potencjalnie-eksterytorialny-seksbot; 210518-porywaczka-miragentow; 210622-verlenka-na-grzybkach)) |
| Amelia Sowińska      | 3 | ((210824-mandragora-nienawidzi-rekinow; 210921-przybycie-rekina-z-eterni; 211012-torszecki-pokazal-kregoslup)) |
| Hestia d'Rekiny      | 3 | ((211207-gdy-zabraknie-pradu-rekinom; 211221-chevaleresse-infiltruje-rekiny; 220111-marysiowa-hestia-rekinow)) |
| Kacper Bankierz      | 3 | ((210323-grzybopreza; 210713-rekin-wspiera-mafie; 210817-zgubiony-holokrysztal-w-lesie)) |
| Laura Tesinik        | 3 | ((210817-zgubiony-holokrysztal-w-lesie; 210824-mandragora-nienawidzi-rekinow; 210831-serafina-staje-za-wydrami)) |
| Marek Samszar        | 3 | ((210615-skradziony-kot-olgi; 210622-verlenka-na-grzybkach; 211102-satarail-pomaga-marysi)) |
| Rupert Mysiokornik   | 3 | ((210406-potencjalnie-eksterytorialny-seksbot; 210817-zgubiony-holokrysztal-w-lesie; 211221-chevaleresse-infiltruje-rekiny)) |
| Wiktor Satarail      | 3 | ((210615-skradziony-kot-olgi; 211026-koszt-ratowania-torszeckiego; 211102-satarail-pomaga-marysi)) |
| Alan Bartozol        | 2 | ((210817-zgubiony-holokrysztal-w-lesie; 211221-chevaleresse-infiltruje-rekiny)) |
| Azalia Sernat d'Namertel | 2 | ((210928-wysadzony-zywy-scigacz; 211123-odbudowa-wedlug-justyniana)) |
| Barnaba Burgacz      | 2 | ((210406-potencjalnie-eksterytorialny-seksbot; 211221-chevaleresse-infiltruje-rekiny)) |
| Diana Tevalier       | 2 | ((211221-chevaleresse-infiltruje-rekiny; 220111-marysiowa-hestia-rekinow)) |
| Ekaterina Zajcew     | 2 | ((210824-mandragora-nienawidzi-rekinow; 220923-wasale-zza-muru-pustogorskiego-PLC)) |
| Franek Bulterier     | 2 | ((210406-potencjalnie-eksterytorialny-seksbot; 210928-wysadzony-zywy-scigacz)) |
| Henryk Wkrąż         | 2 | ((210406-potencjalnie-eksterytorialny-seksbot; 211207-gdy-zabraknie-pradu-rekinom)) |
| Jolanta Sowińska     | 2 | ((210921-przybycie-rekina-z-eterni; 211012-torszecki-pokazal-kregoslup)) |
| Justynian Diakon     | 2 | ((211123-odbudowa-wedlug-justyniana; 211221-chevaleresse-infiltruje-rekiny)) |
| Karol Pustak         | 2 | ((210921-przybycie-rekina-z-eterni; 211123-odbudowa-wedlug-justyniana)) |
| Lucjan Sowiński      | 2 | ((210921-przybycie-rekina-z-eterni; 211012-torszecki-pokazal-kregoslup)) |
| Mariusz Trzewń       | 2 | ((210720-porwanie-daniela-terienaka; 210817-zgubiony-holokrysztal-w-lesie)) |
| Napoleon Bankierz    | 2 | ((210323-grzybopreza; 211228-akt-o-ktorym-marysia-nie-wie)) |
| Olga Myszeczka       | 2 | ((210615-skradziony-kot-olgi; 211026-koszt-ratowania-torszeckiego)) |
| Paweł Szprotka       | 2 | ((210615-skradziony-kot-olgi; 211026-koszt-ratowania-torszeckiego)) |
| Żorż d'Namertel      | 2 | ((210928-wysadzony-zywy-scigacz; 211221-chevaleresse-infiltruje-rekiny)) |
| Arnold Kłaczek       | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Cyryl Perikas        | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Diana Lemurczak      | 1 | ((210518-porywaczka-miragentow)) |
| Feliks Keksik        | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Halina Sermniek      | 1 | ((210831-serafina-staje-za-wydrami)) |
| Hipolit Umadek       | 1 | ((220802-gdy-prawnik-przyjdzie-po-rekiny)) |
| Jeremi Sowiński      | 1 | ((220111-marysiowa-hestia-rekinow)) |
| Ksenia Kirallen      | 1 | ((211026-koszt-ratowania-torszeckiego)) |
| Marcel Nieciesz      | 1 | ((220923-wasale-zza-muru-pustogorskiego-PLC)) |
| Melissa Durszenko    | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Mimoza Diakon        | 1 | ((220816-jak-wsadzic-ule-alanowi)) |
| Mimoza Elegancja Diakon | 1 | ((220222-plaszcz-ochronny-mimozy)) |
| Natalia Tessalon     | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
| Nataniel Morlan      | 1 | ((210921-przybycie-rekina-z-eterni)) |
| Różewicz Diakon      | 1 | ((210622-verlenka-na-grzybkach)) |
| Sabina Kazitan       | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Santino Mysiokornik  | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Serafina Ira         | 1 | ((210831-serafina-staje-za-wydrami)) |
| Staś Arienik         | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Stella Armadion      | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Teresa Mieralit      | 1 | ((211026-koszt-ratowania-torszeckiego)) |
| Urszula Arienik      | 1 | ((211207-gdy-zabraknie-pradu-rekinom)) |
---
categories: profile
factions: 
owner: public
title: Marek Samszar
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210615-skradziony-kot-olgi          | Rekin. Chciał zrobić dziewczynie (Arkadii) piękny prezent i zwinął "nieważnym lokalsom" somnibela dla Arkadii. Potem chciał lecieć na karaoke i skończył w starciu z Lilianą. Chciał eskalować Justynianem Diakonem, ale skończył pobity przez Arkadię za kradzież. | 0111-05-09 - 0111-05-11 |
| 210622-verlenka-na-grzybkach        | niewinny, ale wziął na siebie winę (przejął od Arkadii Lancera), by Arkadia mogła szukać bezpiecznie tego co ją otruł grzybkami. | 0111-05-26 - 0111-05-27 |
| 211102-satarail-pomaga-marysi       | zainfekowany Owadem Sataraila (za kradzież somnibela Olgi) został przekształcony w potwora. Przejął grupę Rekinów i zaczął dewastację Dzielnicy. Zatrzymany przez użądlenie ze strony Marysi i wielką glistę Sensacjusza. Skończył w szpitalu terminuskim w Pustogorze. | 0111-08-05 - 0111-08-06 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 210615-skradziony-kot-olgi          | Arkadia Verlen z nim zerwała i go solidnie pobiła za kradzież somnibela Oldze Myszeczce. | 0111-05-11
| 211102-satarail-pomaga-marysi       | 3 miesiące rekonstrukcji w Szpitalu Terminuskim w Pustogorze. To jest cena za stanięcie na drodze Wiktorowi Satarailowi. | 0111-08-06

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Marysia Sowińska     | 3 | ((210615-skradziony-kot-olgi; 210622-verlenka-na-grzybkach; 211102-satarail-pomaga-marysi)) |
| Arkadia Verlen       | 2 | ((210615-skradziony-kot-olgi; 210622-verlenka-na-grzybkach)) |
| Julia Kardolin       | 2 | ((210615-skradziony-kot-olgi; 210622-verlenka-na-grzybkach)) |
| Liliana Bankierz     | 2 | ((210615-skradziony-kot-olgi; 210622-verlenka-na-grzybkach)) |
| Rafał Torszecki      | 2 | ((210622-verlenka-na-grzybkach; 211102-satarail-pomaga-marysi)) |
| Wiktor Satarail      | 2 | ((210615-skradziony-kot-olgi; 211102-satarail-pomaga-marysi)) |
| Ernest Namertel      | 1 | ((211102-satarail-pomaga-marysi)) |
| Ignacy Myrczek       | 1 | ((210622-verlenka-na-grzybkach)) |
| Karolina Terienak    | 1 | ((211102-satarail-pomaga-marysi)) |
| Lorena Gwozdnik      | 1 | ((211102-satarail-pomaga-marysi)) |
| Olga Myszeczka       | 1 | ((210615-skradziony-kot-olgi)) |
| Paweł Szprotka       | 1 | ((210615-skradziony-kot-olgi)) |
| Różewicz Diakon      | 1 | ((210622-verlenka-na-grzybkach)) |
| Sensacjusz Diakon    | 1 | ((211102-satarail-pomaga-marysi)) |
| Triana Porzecznik    | 1 | ((210622-verlenka-na-grzybkach)) |
| Urszula Miłkowicz    | 1 | ((210622-verlenka-na-grzybkach)) |
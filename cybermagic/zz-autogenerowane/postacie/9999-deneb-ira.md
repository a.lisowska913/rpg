---
categories: profile
factions: 
owner: public
title: Deneb Ira
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210820-fecundatis-w-domenie-barana  | XO Tamary Mardius, chwilowo dowodzi Mardiusowcami. Wycofał wszystkie bazy i skrył się w fortecy, której Blakvel nie do końca może złamać. Miragent podszywając się za niego wprowadził 20% jego oddziału do Aleksandrii... | 0108-12-30 - 0109-01-02 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Antonella Temaris    | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Antos Kuramin        | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Bruno Baran          | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Cień Brighton        | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Damian Szczugor      | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Elsa Kułak           | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Flawia Blakenbauer   | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Jolanta Sowińska     | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Kara Szamun          | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Leon Kantor          | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Rick Varias          | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Tamara Mardius       | 1 | ((210820-fecundatis-w-domenie-barana)) |
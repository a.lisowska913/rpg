---
categories: profile
factions: 
owner: public
title: Gustaf Profnos
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200507-anomalna-figurka-zabboga     | wytracił sporo pieniędzy; nie ma jak ich odzyskać i teraz firma Nadgranica może przejąć fortifarmę jego dziadków. | 0109-09-19 - 0109-09-26 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Damian Polwonien     | 1 | ((200507-anomalna-figurka-zabboga)) |
| Jan Łowicz           | 1 | ((200507-anomalna-figurka-zabboga)) |
| Kinga Kruk           | 1 | ((200507-anomalna-figurka-zabboga)) |
| Melinda Teilert      | 1 | ((200507-anomalna-figurka-zabboga)) |
| Natasza Aniel        | 1 | ((200507-anomalna-figurka-zabboga)) |
| Tomasz Tukan         | 1 | ((200507-anomalna-figurka-zabboga)) |
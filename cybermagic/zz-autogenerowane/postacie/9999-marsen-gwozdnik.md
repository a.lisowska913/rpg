---
categories: profile
factions: 
owner: public
title: Marsen Gwozdnik
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220730-supersupertajny-plan-loreny  | pojawił się dyskretnie w Podwiercie i uznał, że Marysia x Ernest chcą przejąć teren (a on nienawidzi Eterni) i atakują Lorenę. Więc pozmieniał część kontraktów biznesowych między Ernestem i lokalnym biznesem by zastawić pułapkę na Eternię i wzmocnić Lorenę (podejrzewa że ona ma Chytry Plan). Arkadia pokonała jego Lancera, Karo z nim porozmawiała i wzięła do tymczasowej niewoli. | 0111-09-26 - 0111-09-30 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 220730-supersupertajny-plan-loreny  | wierzy, że Lorena Gwozdnik jest najlepszym strategiem i że Ernest x Marysia walczą przeciw Lorenie i próbują ją osłabić. | 0111-09-30
| 220730-supersupertajny-plan-loreny  | chce zniszczyć sojusz Marysia x Ernest i nie dopuścić, by oni objęli ten teren w imię Aurum. | 0111-09-30
| 220730-supersupertajny-plan-loreny  | wierzy, że Karolina Terienak jest agentką Loreny Gwozdnik. | 0111-09-30

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arkadia Verlen       | 1 | ((220730-supersupertajny-plan-loreny)) |
| Daniel Terienak      | 1 | ((220730-supersupertajny-plan-loreny)) |
| Karolina Terienak    | 1 | ((220730-supersupertajny-plan-loreny)) |
| Liliana Bankierz     | 1 | ((220730-supersupertajny-plan-loreny)) |
| Władysław Owczarek   | 1 | ((220730-supersupertajny-plan-loreny)) |
| Żorż d'Namertel      | 1 | ((220730-supersupertajny-plan-loreny)) |
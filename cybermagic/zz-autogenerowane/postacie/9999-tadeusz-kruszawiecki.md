---
categories: profile
factions: 
owner: public
title: Tadeusz Kruszawiecki
---

# {{ page.title }}


# Generated: 



## Fiszki


* dyrektor Cyberszkoły | @ 230212-pierwszy-tajemniczy-wielbiciel-liliany
* (ENCAO:  +-0+- |Swojski, prosty;;Ugodowy, unika konfliktów;;Kobieciarz| VALS: Hedonism, Face | DRIVE: Pomóc dzieciakom, integracja społeczeństwa) | @ 230212-pierwszy-tajemniczy-wielbiciel-liliany
* "Nieważne, magowie czy nie. Te dzieci mają potencjał i dla nich to wszystko robimy. Nieważne jak to się kończy, grunt, by nie ucierpiały" | @ 230212-pierwszy-tajemniczy-wielbiciel-liliany

### Wątki


rekiny-a-akademia
akademia-magiczna-zaczestwa
aniela-arienik

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 180821-programista-mimo-woli        | dyrektor Cyberszkoły w Zaczęstwie. Pod wpływem magii mentalnej; mały Staś będzie mógł rysować i nie będzie musiał być programistą. | 0109-09-06 - 0109-09-07 |
| 180906-nikt-nie-spi-w-swoim-lozku   | dyrektor, który serio nie powinien sypiać z Brygidą. Prawie zginął od ducha. Nadal creepy. Grażyna ma na niego materiały do szantażu. | 0109-09-09 - 0109-09-11 |
| 181101-wojna-o-uczciwe-polfinaly    | dyrektor został tymczasowo zcyborgizowany. Po 3 dniach wróci do normy. Nadal chroni szkołę całym sercem. | 0109-10-16 - 0109-10-18 |
| 190820-liliana-w-swiecie-dokumentow | dość ufny dyrektor, który z przyjemnością pomagał magom w neutralizacji mimika symbiotycznego jak tylko mógł. | 0110-06-30 - 0110-07-04 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 181030-zaczestwiacy-czy-karolina    | wieczorami pozwala wykorzystywać Cyberszkołę ludziom, którzy robią tam fajne rzeczy póki nie psują (a szkoła się sama regeneruje). | 0109-10-15

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Pięknotka Diakon     | 3 | ((180821-programista-mimo-woli; 180906-nikt-nie-spi-w-swoim-lozku; 181101-wojna-o-uczciwe-polfinaly)) |
| Atena Sowińska       | 2 | ((180821-programista-mimo-woli; 180906-nikt-nie-spi-w-swoim-lozku)) |
| Tymon Grubosz        | 2 | ((181101-wojna-o-uczciwe-polfinaly; 190820-liliana-w-swiecie-dokumentow)) |
| Adela Pieczar        | 1 | ((190820-liliana-w-swiecie-dokumentow)) |
| Alan Bartozol        | 1 | ((181101-wojna-o-uczciwe-polfinaly)) |
| Arnulf Poważny       | 1 | ((190820-liliana-w-swiecie-dokumentow)) |
| Bronisława Strzelczyk | 1 | ((180821-programista-mimo-woli)) |
| Brygida Maczkowik    | 1 | ((180906-nikt-nie-spi-w-swoim-lozku)) |
| Damian Podpalnik     | 1 | ((181101-wojna-o-uczciwe-polfinaly)) |
| Grażyna Sirwąg       | 1 | ((180906-nikt-nie-spi-w-swoim-lozku)) |
| Karolina Erenit      | 1 | ((181101-wojna-o-uczciwe-polfinaly)) |
| Liliana Bankierz     | 1 | ((190820-liliana-w-swiecie-dokumentow)) |
| Marlena Maja Leszczyńska | 1 | ((181101-wojna-o-uczciwe-polfinaly)) |
| Mieszko Weiner       | 1 | ((180906-nikt-nie-spi-w-swoim-lozku)) |
| Staś Kruszawiecki    | 1 | ((180821-programista-mimo-woli)) |
| Szczepan Mensic      | 1 | ((180906-nikt-nie-spi-w-swoim-lozku)) |
| Szymon Jaszczurzec   | 1 | ((190820-liliana-w-swiecie-dokumentow)) |
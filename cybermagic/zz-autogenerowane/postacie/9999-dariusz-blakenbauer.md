---
categories: profile
factions: 
owner: public
title: Dariusz Blakenbauer
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210311-studenci-u-verlenow          | 26 lat, BUR; przestraszony potencjalnym sojuszem trzech rodów przeciw Blakenbauerom chciał zrobić kompromitującą intrygę rozrywającą rody używającą lustra i wił. Nie wiedział o magii Eleny i doprowadził do śmierci 3 osób i 14 wił rękami (magią) Eleny. | 0096-11-18 - 0096-11-24 |
| 210331-elena-z-rodu-verlen          | pomaga Verlenom w Poniewierzy jako poszukiwacz, artefaktor i katalista; przyznał Viorice że Elena pośrednio zabiła 47 osób w domenie Blakenbauerów a oni zniszczyli jej reputację na Orbiterze. | 0111-06-29 - 0111-07-02 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 2 | ((210311-studenci-u-verlenow; 210331-elena-z-rodu-verlen)) |
| Elena Verlen         | 2 | ((210311-studenci-u-verlenow; 210331-elena-z-rodu-verlen)) |
| Viorika Verlen       | 2 | ((210311-studenci-u-verlenow; 210331-elena-z-rodu-verlen)) |
| Apollo Verlen        | 1 | ((210311-studenci-u-verlenow)) |
| Krystian Blakenbauer | 1 | ((210331-elena-z-rodu-verlen)) |
| Maja Samszar         | 1 | ((210311-studenci-u-verlenow)) |
| Michał Perikas       | 1 | ((210311-studenci-u-verlenow)) |
| Rafał Perikas        | 1 | ((210311-studenci-u-verlenow)) |
| Romeo Verlen         | 1 | ((210331-elena-z-rodu-verlen)) |
| Rufus Samszar        | 1 | ((210311-studenci-u-verlenow)) |
| Sylwia Perikas       | 1 | ((210311-studenci-u-verlenow)) |
---
categories: profile
factions: 
owner: public
title: Liliana Bankierz
---

# {{ page.title }}


# Read: 

## Postać

### Ogólny pomysł (3)

* Iluzjonistka i kinetka która bardzo chciała być Diakonką i lekarzem, ale Matryca się nie przyjęła.
* Uczennica w Szkole Magów w Zaczęstwie bardzo skupiająca się na pięknie i gracji. Potrafi zafascynować pięknem, do łez. Obezwładniające piękno.
* Bibliotekarka. Potrafi przetwarzać i porządkować informacje. Poradzi sobie z każdą bazą wiedzy.

### Motywacja (gniew/wartość, zmiana, sposób) (3)

* BEAUTY/INDEPENDENCE; piękno dla każdego znaczy coś innego i każdy może być swoim ideałem piękna; wspiera różnorodność i otacza się pięknem
* BOLDNESS/BRILLIANCE; każdy ma prawo dążyć do czego pragnie a wiedza powinna być dostępna; działa ryzykownie i próbuje zawsze być z przodu, wszystko jest jej sprawą
* chciała być Diakonką i lekarzem; stanie się Diakonką i idealną wersją siebie; próbuje znaleźć sposób jak się upięknić i stać się wolną

### Wyróżniki (3)

* Wybitna pamięć. Wystarczy, że raz coś zobaczy i będzie w stanie to odtworzyć. Pamięta wszystko co czytała.
* Wizualizacje. Potrafi w odpowiedni sposób łączyć dane i wyciągać z nich wnioski. Potrafi przekonywać danymi.
* Bardzo oczytana. Mnóstwo czyta i dużo wie - w bardzo wielu obszarach, ale przede wszystkim w obszarze zarządzania wiedzą.
* Skupia się na wynikach, nie na ludziach. Przy całym swoim pragnieniu bycia Diakonką po prostu nie myśli jak Diakon.

### Zasoby i otoczenie (3)

* Zawsze ma przy sobie magiczną broń. Nie umie dobrze walczyć (wcale), ale ta broń może przeważyć.
* Mól książkowy, ma dostęp do biblioteki wiedzy o Astorii, liczbach, historii i viciniusach. Przydaje się zwłaszcza do iluzji.
* Często chodzi na upiększających zaklęciach; ma też eleganckie i ładne stroje. Bardzo dba o to, by właściwie wyglądać i robić wrażenie.

### Magia (3)

#### Gdy kontroluje energię

* Iluzjonistka: świetnie robi wizualizacje, upiększa się oraz bardzo skutecznie tworzy miraże różnego rodzaju. Przede wszystkim zmysł wzroku - obezwładniające piękno.
* Kinetka: potrafi podnosić i utrzymywać w powietrzu sporo naprawdę ciężkich rzeczy. Świetna w przeprowadzkach i pracy z ogromną ilością książek.
* Mentalistka: zwłaszcza w obszarze czytania myśli i zdobywania różnych faktów. Lub wysłanie informacji do celu. Ewentualnie, wzmocnienie iluzji.

#### Gdy traci kontrolę

* Ona i osoby dookoła niej tracą możliwość zorientowania się co jest iluzją a co jest rzeczywistością
* Zaczyna wierzyć, że żyje w pięknym świecie o którym zawsze marzyła - zaczyna wierzyć w swoje bajki
* Czasem, w drastycznych wypadkach, jej iluzje stają się tymczasowo prawdą - zwłaszcza w najbardziej niefortunnych okolicznościach

### Powiązane frakcje

{{ page.factions }}

## Opis

(18 min)


# Generated: 



## Fiszki


* Iluzjonistka i kinetka która bardzo chciała być Diakonką i lekarzem; przetwarza i porządkuje informacje ale kocha piękno. Wizualizacje, odwaga, pamięć. | @ 230212-pierwszy-tajemniczy-wielbiciel-liliany
* (ENCAO:  ++--+ | Idealistyczna;;bezkompromisowa;;śmiała i odważna| VALS: Self-direction, Benevolence > Tradition | DRIVE: Piękno, Fairness) | @ 230212-pierwszy-tajemniczy-wielbiciel-liliany
* "Rzeczywistość jest jedynie wariantem tego, co mogę zobaczyć. Rzeczywistość NIE wygra z marzeniami." | @ 230212-pierwszy-tajemniczy-wielbiciel-liliany

### Wątki


rekiny-a-akademia
akademia-magiczna-zaczestwa
aniela-arienik

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190113-chronmy-karoline-przed-uczniami | chciała pomóc Karolinie tak jak Napoleon, ale wezwała terminusa. Też się zaraziła i zdemolowała pokój. Dostała niesłuszną reputację. Kiedyś Diakonka, ale ciało odrzuciła tą krew. | 0110-01-04 - 0110-01-05 |
| 190519-uciekajacy-seksbot           | żądała od Pięknotki, by ona pomogła biednemu seksbotowi. Nie udało jej się zmusić Pięknotki do niczego. Najpewniej uczestniczyła w wykradzeniu seksbota Ernestowi. | 0110-04-21 - 0110-04-22 |
| 190622-wojna-kajrata                | za to co zrobiła z seksbotem Kajrata ów ją przechwycił; czuje dziwną energię od Kajrata ale nie umie jej nazwać czy określić. | 0110-05-14 - 0110-05-17 |
| 190623-noc-kajrata                  | trafiła w strefę wpływów Kajrata; zobaczyła że ma do czynienia z potworem. Pięknotka uratowała ją przed losem służki Kajrata. | 0110-05-18 - 0110-05-21 |
| 190820-liliana-w-swiecie-dokumentow | nadmiar bohaterstwa i żądzy przygód. Zaczęła od zrobienia artefaktu, potem wpakowała się w linię spedycyjną Noctis - Trzeci Raj i skończyła jako przynęta na mimika. Tydzień w szpitalu. | 0110-06-30 - 0110-07-04 |
| 200311-wygrany-kontrakt             | chciała udowodnić, że jedzenie firmy "Zygmunt Zając" jest niejadalne (mimo że trafia na stołówkę); włamała się i zebrała dowody. Upokorzona na wizji, stała się wrogiem "Zająca" na całe życie. | 0110-07-16 - 0110-07-19 |
| 200326-test-z-etyki                 | AMZ; przekonana o tym że "Zygmunt Zając" ją prześladuje i wysyła na nią agentów. Zdekomponowała ich konserwy i wyhodowała z nich straszne rzeczy przy pomocy Myrczka. Imp od zjęć ją prześladuje. | 0110-07-25 - 0110-07-27 |
| 201013-pojedynek-akademia-rekiny    | prowodyrka i podżegaczka bitew między Rekinami i studentami AMZ. Spróbowała zapalić studentów do ostrzejszej wojny, ale Robert i Julia zmienili to w zawody. | 0110-10-10 - 0110-10-18 |
| 201020-przygoda-randka-i-porwanie   | odegrała scenę "och nie, Upiór mnie opętał", po czym przebrana za gotkę leciała na pomoc porwanej Trianie! Pokazała dużą siłę jako kinetka. Zdyscyplinowana - gdy terminuska kazała, Liliana się wycofała. | 0110-10-21 - 0110-10-23 |
| 210615-skradziony-kot-olgi          | nie akceptuje tego, że na Pawle znęca się Samszar. Weszła z nim w starcie magiczne w Lesie Trzęsawnym. Przygotowuje walkę i zemstę przeciwko Samszarowi. Zneutralizowana przez okrucieństwo Arkadii na Samszara. | 0111-05-09 - 0111-05-11 |
| 210622-verlenka-na-grzybkach        | podejrzewa, że uszkodzenie robota kultywacyjnego to plan firmy Zygmunt Zając. Ale odda się pod dowodzenie Julii by dostarczyć robota na czas. | 0111-05-26 - 0111-05-27 |
| 210817-zgubiony-holokrysztal-w-lesie | była na wycieczce AMZ dowodzonej przez Alana Bartozola; zauważyła że Stella coś tam znalazła i przekazała to Laurze gdy ta spytała. | 0111-06-18 - 0111-06-20 |
| 211228-akt-o-ktorym-marysia-nie-wie | należy do miłośników piękna. Ma akt od Marysi Sowińskiej i ma bliski kontakt z Loreną Gwozdnik. Nie wydała Marysi Loreny jako artystki, ale przekazała Lorenie zdjęcia z trackerem. Nie daruje Marysi. | 0111-09-05 - 0111-09-08 |
| 220111-marysiowa-hestia-rekinow     | twórca strasznych ulotek "Marysia Sowińska wróg wszelkiej wolności i podliz". Zrobiła artykuł w "Plotkach Royalsów" Pustaka. | 0111-09-12 - 0111-09-15 |
| 220730-supersupertajny-plan-loreny  | chciała uderzyć w Marysię i Ernesta (za Lorenę) i pomóc Marsenowi, acz nie wiedziała co Marsen robi i co planuje dla lokalnego biznesu (bo by nie pomogła). Zailuzjowała Marsenowego lancera i sama poszła jako Marek Samszar. Zaatakowana przez Arkadię nożem z zaskoczenia, skończyła w gliździe. | 0111-09-26 - 0111-09-30 |
| 230212-pierwszy-tajemniczy-wielbiciel-liliany | AMZ. Jej portret zrobiony przez Mariusza wysysa ludzi. Podejrzewa że to Eternia, Rekiny lub Zygmunt Zając. Koreluje dane statystyczne by dojść do tego kto za tym stoi, potem atakuje Mariusza słownie. Nie wierzy, że może się komukolwiek podobać - nie jest już Diakonką (ale tego nie mówi). | 0111-10-18 - 0111-10-20 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 190113-chronmy-karoline-przed-uczniami | dostała całkowicie nieuzasadnioną opinię osoby zdolnej do walki z terminusem w power suicie bez niczego (przez erupcję energii w Pięknotkę). | 0110-01-05
| 190622-wojna-kajrata                | straumatyzowana przez Kajrata, który może zabrać jej wszystko jeśli do niego nie dołączy. | 0110-05-17
| 210817-zgubiony-holokrysztal-w-lesie | przyjaciółka Laury Tesinik "od zawsze". | 0111-06-20
| 211228-akt-o-ktorym-marysia-nie-wie | Marysia Sowińska wyciągnęła od niej informację pośrednio (trackerem) kto jest autorem aktu Marysi (Lorena). Przez to zaskarbiła sobie jej wściekłość i zemstę. | 0111-09-08
| 211228-akt-o-ktorym-marysia-nie-wie | przyjaciółka Loreny Gwozdnik. Połączyło je upodobanie do piękna. | 0111-09-08
| 220730-supersupertajny-plan-loreny  | solidnie ranna nożem Arkadii; dobre 2 tygodnie w gliździe Sensacjusza. | 0111-09-30

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Marysia Sowińska     | 5 | ((210615-skradziony-kot-olgi; 210622-verlenka-na-grzybkach; 210817-zgubiony-holokrysztal-w-lesie; 211228-akt-o-ktorym-marysia-nie-wie; 220111-marysiowa-hestia-rekinow)) |
| Ernest Kajrat        | 4 | ((190519-uciekajacy-seksbot; 190622-wojna-kajrata; 190623-noc-kajrata; 200311-wygrany-kontrakt)) |
| Julia Kardolin       | 4 | ((201013-pojedynek-akademia-rekiny; 210615-skradziony-kot-olgi; 210622-verlenka-na-grzybkach; 230212-pierwszy-tajemniczy-wielbiciel-liliany)) |
| Napoleon Bankierz    | 4 | ((190113-chronmy-karoline-przed-uczniami; 200326-test-z-etyki; 201013-pojedynek-akademia-rekiny; 211228-akt-o-ktorym-marysia-nie-wie)) |
| Pięknotka Diakon     | 4 | ((190113-chronmy-karoline-przed-uczniami; 190519-uciekajacy-seksbot; 190622-wojna-kajrata; 190623-noc-kajrata)) |
| Teresa Mieralit      | 4 | ((190113-chronmy-karoline-przed-uczniami; 190519-uciekajacy-seksbot; 200326-test-z-etyki; 201013-pojedynek-akademia-rekiny)) |
| Arkadia Verlen       | 3 | ((210615-skradziony-kot-olgi; 210622-verlenka-na-grzybkach; 220730-supersupertajny-plan-loreny)) |
| Arnulf Poważny       | 3 | ((190113-chronmy-karoline-przed-uczniami; 190519-uciekajacy-seksbot; 190820-liliana-w-swiecie-dokumentow)) |
| Ignacy Myrczek       | 3 | ((200326-test-z-etyki; 201013-pojedynek-akademia-rekiny; 210622-verlenka-na-grzybkach)) |
| Kacper Bankierz      | 3 | ((201013-pojedynek-akademia-rekiny; 201020-przygoda-randka-i-porwanie; 210817-zgubiony-holokrysztal-w-lesie)) |
| Karolina Terienak    | 3 | ((211228-akt-o-ktorym-marysia-nie-wie; 220111-marysiowa-hestia-rekinow; 220730-supersupertajny-plan-loreny)) |
| Tomasz Tukan         | 3 | ((190519-uciekajacy-seksbot; 201020-przygoda-randka-i-porwanie; 210817-zgubiony-holokrysztal-w-lesie)) |
| Triana Porzecznik    | 3 | ((201020-przygoda-randka-i-porwanie; 210622-verlenka-na-grzybkach; 230212-pierwszy-tajemniczy-wielbiciel-liliany)) |
| Daniel Terienak      | 2 | ((211228-akt-o-ktorym-marysia-nie-wie; 220730-supersupertajny-plan-loreny)) |
| Ernest Namertel      | 2 | ((211228-akt-o-ktorym-marysia-nie-wie; 220111-marysiowa-hestia-rekinow)) |
| Keira Amarco d'Namertel | 2 | ((211228-akt-o-ktorym-marysia-nie-wie; 220111-marysiowa-hestia-rekinow)) |
| Laura Tesinik        | 2 | ((210817-zgubiony-holokrysztal-w-lesie; 230212-pierwszy-tajemniczy-wielbiciel-liliany)) |
| Marek Samszar        | 2 | ((210615-skradziony-kot-olgi; 210622-verlenka-na-grzybkach)) |
| Ossidia Saitis       | 2 | ((190519-uciekajacy-seksbot; 190623-noc-kajrata)) |
| Rafał Torszecki      | 2 | ((210622-verlenka-na-grzybkach; 211228-akt-o-ktorym-marysia-nie-wie)) |
| Robert Pakiszon      | 2 | ((201013-pojedynek-akademia-rekiny; 201020-przygoda-randka-i-porwanie)) |
| Serafina Ira         | 2 | ((190622-wojna-kajrata; 190623-noc-kajrata)) |
| Stella Armadion      | 2 | ((201013-pojedynek-akademia-rekiny; 210817-zgubiony-holokrysztal-w-lesie)) |
| Tymon Grubosz        | 2 | ((190820-liliana-w-swiecie-dokumentow; 201020-przygoda-randka-i-porwanie)) |
| Urszula Miłkowicz    | 2 | ((201020-przygoda-randka-i-porwanie; 210622-verlenka-na-grzybkach)) |
| Adela Kirys          | 1 | ((190113-chronmy-karoline-przed-uczniami)) |
| Adela Pieczar        | 1 | ((190820-liliana-w-swiecie-dokumentow)) |
| Alan Bartozol        | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Aleksander Bemucik   | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Amanda Kajrat        | 1 | ((200311-wygrany-kontrakt)) |
| Aniela Kark          | 1 | ((200326-test-z-etyki)) |
| Berenika Wrążowiec   | 1 | ((200326-test-z-etyki)) |
| Cyryl Perikas        | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Dagmara Doberman     | 1 | ((200311-wygrany-kontrakt)) |
| Daniel Sowiński      | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Diana Tevalier       | 1 | ((220111-marysiowa-hestia-rekinow)) |
| Eliza Ira            | 1 | ((190519-uciekajacy-seksbot)) |
| Gabriel Ursus        | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Henryk Sowiński      | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Hestia d'Rekiny      | 1 | ((220111-marysiowa-hestia-rekinow)) |
| Jeremi Sowiński      | 1 | ((220111-marysiowa-hestia-rekinow)) |
| Justynian Diakon     | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Karolina Erenit      | 1 | ((190113-chronmy-karoline-przed-uczniami)) |
| Laurencjusz Sorbian  | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Lorena Gwozdnik      | 1 | ((211228-akt-o-ktorym-marysia-nie-wie)) |
| Lucjusz Blakenbauer  | 1 | ((200311-wygrany-kontrakt)) |
| Mariusz Kupieczka    | 1 | ((230212-pierwszy-tajemniczy-wielbiciel-liliany)) |
| Mariusz Trzewń       | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Marsen Gwozdnik      | 1 | ((220730-supersupertajny-plan-loreny)) |
| Minerwa Metalia      | 1 | ((200311-wygrany-kontrakt)) |
| Nikola Kirys         | 1 | ((190622-wojna-kajrata)) |
| Olaf Zuchwały        | 1 | ((190622-wojna-kajrata)) |
| Olga Myszeczka       | 1 | ((210615-skradziony-kot-olgi)) |
| Paweł Szprotka       | 1 | ((210615-skradziony-kot-olgi)) |
| Remor 340D           | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Robinson Porzecznik  | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Różewicz Diakon      | 1 | ((210622-verlenka-na-grzybkach)) |
| Rupert Mysiokornik   | 1 | ((210817-zgubiony-holokrysztal-w-lesie)) |
| Saitaer              | 1 | ((190519-uciekajacy-seksbot)) |
| Szymon Jaszczurzec   | 1 | ((190820-liliana-w-swiecie-dokumentow)) |
| Tadeusz Kruszawiecki | 1 | ((190820-liliana-w-swiecie-dokumentow)) |
| Wiktor Satarail      | 1 | ((210615-skradziony-kot-olgi)) |
| Władysław Owczarek   | 1 | ((220730-supersupertajny-plan-loreny)) |
| Ziemowit Zięba       | 1 | ((200311-wygrany-kontrakt)) |
| Żorż d'Namertel      | 1 | ((220730-supersupertajny-plan-loreny)) |
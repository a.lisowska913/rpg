---
categories: profile
factions: 
owner: public
title: Teresa Mieralit
---

# {{ page.title }}


# Generated: 



## Fiszki


* nauczycielka magii leczniczej i katalistka (disruptorka magii) w Szkole Magów | @ 230303-the-goose-from-hell
* (ENCAO:  +0+-+ |Manipulatorka i meddler;; Kreatywna, tworząca wiecznie coś nowego| VALS: Benevolence, Achievement >> Face, Security| DRIVE: AMZ będzie najskuteczniejsze) | @ 230303-the-goose-from-hell

### Wątki


akademia-magiczna-zaczestwa

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 211010-ukryta-wychowanka-arnulfa    | 15 lat; disruptorka magii i paramedyk; noktianka pod opieką dyrektora Arnulfa (jego wychowanka). Ma niewyparzoną gębę. Jej obecność spowodowała kolizję w Strażniczce - elementy BIA wykryły jako "friend", elementy TAI jako "foe". | 0084-12-11 - 0084-12-12 |
| 211017-nastolatka-w-bieliznie-na-dachu-w-burzy | noktiańska czarodziejka z deathwish?; trochę się boi astorian i trochę ich nienawidzi, więc jest na uboczu. Gdy terminus infiltrował AMZ rozebrała się do bielizny (by nie uszkodzić ubrania) i schowała się na dachu w burzy na Złomiarium dla dreszczyka. Uratowana przez Klaudię, podejrzana o prostytucję i współpracę z Grzymościem (o którym nawet nie wie), skończyła śpiąc na łóżku Klaudii a potem - w pokoju z nią i Ksenią. Całkowicie dzika, niezsocjalizowana. | 0084-12-14 - 0084-12-15 |
| 211019-czarodziejka-ktora-jednak-moze-sie-zabic | powoli się oswaja z Klaudią i Ksenią, przez co je odpycha. Przestraszona tym, że pojawiły się seksboty "skrzywdź noktiankę". Świetna w negamagii, ma naturalny talent; rozbiła nałożony na nią geas. | 0084-12-20 - 0084-12-24 |
| 190101-morderczyni-jednej-plotki    | nauczycielka w szkole magów. Bardzo (zbyt) zainteresowana Mrocznymi Drżeniami Cieniaszczytu, cokolwiek to jest. Agentka Czerwonych Myszy i aspekt Dare Shiver. | 0109-12-12 - 0109-12-16 |
| 190102-stalker-i-czerwone-myszy     | przyszła skonsumować darmowy kupon od Pięknotki i wyszła jako megaepicka reklama gabinetu Pięknotki. Pięknotka przeszła samą siebie. | 0109-12-17 - 0109-12-20 |
| 190113-chronmy-karoline-przed-uczniami | nauczycielka magii leczniczej i katalistka w Szkole Magów. Pomogła Lilianie. | 0110-01-04 - 0110-01-05 |
| 190519-uciekajacy-seksbot           | pancerz dyrektora w szkole magów, zwalczająca Ernesta i biorąca potencjalny ogień na siebie. Co udowadnia, że jest baaardzo nierozsądna. | 0110-04-21 - 0110-04-22 |
| 200326-test-z-etyki                 | Nauczycielka etyki ORAZ agentka Dare Shiver. Wpierw dostarczyła Lilianie narzędzia do robienia problemów a potem poszczuła ją dwoma uczennicami. I nic nie musiała robić. | 0110-07-25 - 0110-07-27 |
| 200510-tajna-baza-orbitera          | poproszona przez Pięknotkę, by skupić się na Myrczku. On ma trochę za dużo czasu i podkochuje się w Sabinie Kazitan, co do niczego nie prowadzi. Obiecała, że go od niej odsunie. | 0110-09-03 - 0110-09-07 |
| 201013-pojedynek-akademia-rekiny    | miesiąc temu katalitycznie rozproszyła efemerydę złożoną przez bitwę studentów AMZ vs Rekiny. Zgodziła się by Napoleon pożyczył ścigacz z artefaktorium. | 0110-10-10 - 0110-10-18 |
| 211026-koszt-ratowania-torszeckiego | dla Pawła Szprotki jest "Damą w Błękicie". Chroni go i daje mu pracę, bo chce jego powodzenia. Tak jak kiedyś Klaudia i Ksenia chroniły ją. I ofc Arnulf. | 0111-07-28 - 0111-08-01 |
| 230303-the-goose-from-hell          | An ethics teacher who recruited Carmen, Alex, and Julia to solve the goose problem. She was the only one who had a good night sleep while the students were sleeping on the top of a building guarding the captured goose. | 0111-10-24 - 0111-10-26 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 211017-nastolatka-w-bieliznie-na-dachu-w-burzy | spisana za podejrzenie prostytucji (SRS!). Podejrzewa ją o to Sasza i pół AMZ po nocnym spacerze w "lekkim stroju". | 0084-12-15
| 211017-nastolatka-w-bieliznie-na-dachu-w-burzy | mieszka w akademiku AMZ z Ksenią i Klaudią. | 0084-12-15
| 211019-czarodziejka-ktora-jednak-moze-sie-zabic | miała na sobie geas uniemożliwiający jej zabicie się, założony przez jej ojca przed śmiercią. Geas już nie działa, rozproszony przez jej negamagię. | 0084-12-24
| 211019-czarodziejka-ktora-jednak-moze-sie-zabic | nie ma pieniędzy, nie pożyczy i dlatego uważa swoje ciuchy za praktyczne. Arnulf dał jej ubrania po córce i nie kupiła nic nowego. | 0084-12-24

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arnulf Poważny       | 5 | ((190113-chronmy-karoline-przed-uczniami; 190519-uciekajacy-seksbot; 211010-ukryta-wychowanka-arnulfa; 211017-nastolatka-w-bieliznie-na-dachu-w-burzy; 211019-czarodziejka-ktora-jednak-moze-sie-zabic)) |
| Pięknotka Diakon     | 5 | ((190101-morderczyni-jednej-plotki; 190102-stalker-i-czerwone-myszy; 190113-chronmy-karoline-przed-uczniami; 190519-uciekajacy-seksbot; 200510-tajna-baza-orbitera)) |
| Ignacy Myrczek       | 4 | ((200326-test-z-etyki; 200510-tajna-baza-orbitera; 201013-pojedynek-akademia-rekiny; 211026-koszt-ratowania-torszeckiego)) |
| Ksenia Kirallen      | 4 | ((211010-ukryta-wychowanka-arnulfa; 211017-nastolatka-w-bieliznie-na-dachu-w-burzy; 211019-czarodziejka-ktora-jednak-moze-sie-zabic; 211026-koszt-ratowania-torszeckiego)) |
| Liliana Bankierz     | 4 | ((190113-chronmy-karoline-przed-uczniami; 190519-uciekajacy-seksbot; 200326-test-z-etyki; 201013-pojedynek-akademia-rekiny)) |
| Klaudia Stryk        | 3 | ((211010-ukryta-wychowanka-arnulfa; 211017-nastolatka-w-bieliznie-na-dachu-w-burzy; 211019-czarodziejka-ktora-jednak-moze-sie-zabic)) |
| Napoleon Bankierz    | 3 | ((190113-chronmy-karoline-przed-uczniami; 200326-test-z-etyki; 201013-pojedynek-akademia-rekiny)) |
| Julia Kardolin       | 2 | ((201013-pojedynek-akademia-rekiny; 230303-the-goose-from-hell)) |
| Mariusz Trzewń       | 2 | ((200510-tajna-baza-orbitera; 211019-czarodziejka-ktora-jednak-moze-sie-zabic)) |
| Paweł Szprotka       | 2 | ((211026-koszt-ratowania-torszeckiego; 230303-the-goose-from-hell)) |
| Sasza Morwowiec      | 2 | ((211010-ukryta-wychowanka-arnulfa; 211017-nastolatka-w-bieliznie-na-dachu-w-burzy)) |
| Talia Aegis          | 2 | ((211010-ukryta-wychowanka-arnulfa; 211019-czarodziejka-ktora-jednak-moze-sie-zabic)) |
| Tymon Grubosz        | 2 | ((200510-tajna-baza-orbitera; 211010-ukryta-wychowanka-arnulfa)) |
| Adela Kirys          | 1 | ((190113-chronmy-karoline-przed-uczniami)) |
| Aleksander Bemucik   | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Aleksander Iczak     | 1 | ((190101-morderczyni-jednej-plotki)) |
| Alex Deverien        | 1 | ((230303-the-goose-from-hell)) |
| Alicja Trawlis       | 1 | ((230303-the-goose-from-hell)) |
| Alina Anakonda       | 1 | ((200510-tajna-baza-orbitera)) |
| Aniela Kark          | 1 | ((200326-test-z-etyki)) |
| Artur Kołczond       | 1 | ((200510-tajna-baza-orbitera)) |
| Berenika Wrążowiec   | 1 | ((200326-test-z-etyki)) |
| Carmen Deverien      | 1 | ((230303-the-goose-from-hell)) |
| Eliza Ira            | 1 | ((190519-uciekajacy-seksbot)) |
| Ernest Kajrat        | 1 | ((190519-uciekajacy-seksbot)) |
| Erwin Galilien       | 1 | ((190101-morderczyni-jednej-plotki)) |
| Felicjan Szarak      | 1 | ((211010-ukryta-wychowanka-arnulfa)) |
| Gabriel Ursus        | 1 | ((200510-tajna-baza-orbitera)) |
| Jan Kramczuk         | 1 | ((190102-stalker-i-czerwone-myszy)) |
| Justynian Diakon     | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Kacper Bankierz      | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Kallista Exolon      | 1 | ((200510-tajna-baza-orbitera)) |
| Karol Szurnak        | 1 | ((190101-morderczyni-jednej-plotki)) |
| Karolina Erenit      | 1 | ((190113-chronmy-karoline-przed-uczniami)) |
| kot-pacyfikator Tobias | 1 | ((230303-the-goose-from-hell)) |
| Laura Tesinik        | 1 | ((200510-tajna-baza-orbitera)) |
| Maryla Koternik      | 1 | ((211019-czarodziejka-ktora-jednak-moze-sie-zabic)) |
| Marysia Sowińska     | 1 | ((211026-koszt-ratowania-torszeckiego)) |
| Natalia Tessalon     | 1 | ((200510-tajna-baza-orbitera)) |
| Olaf Zuchwały        | 1 | ((190101-morderczyni-jednej-plotki)) |
| Olga Myszeczka       | 1 | ((211026-koszt-ratowania-torszeckiego)) |
| Ossidia Saitis       | 1 | ((190519-uciekajacy-seksbot)) |
| Rafał Torszecki      | 1 | ((211026-koszt-ratowania-torszeckiego)) |
| Remor 340D           | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Robert Pakiszon      | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Sabina Kazitan       | 1 | ((200510-tajna-baza-orbitera)) |
| Saitaer              | 1 | ((190519-uciekajacy-seksbot)) |
| Sensacjusz Diakon    | 1 | ((211026-koszt-ratowania-torszeckiego)) |
| Stella Armadion      | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Strażniczka Alair    | 1 | ((211010-ukryta-wychowanka-arnulfa)) |
| Talarand d'Irrydius  | 1 | ((200510-tajna-baza-orbitera)) |
| Tomasz Tukan         | 1 | ((190519-uciekajacy-seksbot)) |
| Waldemar Grzymość    | 1 | ((211017-nastolatka-w-bieliznie-na-dachu-w-burzy)) |
| Waldemar Mózg        | 1 | ((190102-stalker-i-czerwone-myszy)) |
| Wiktor Satarail      | 1 | ((211026-koszt-ratowania-torszeckiego)) |
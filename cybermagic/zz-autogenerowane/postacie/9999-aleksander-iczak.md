---
categories: profile
factions: 
owner: public
title: Aleksander Iczak
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190101-morderczyni-jednej-plotki    | krawiec współpracujący z Pięknotką w sprawie egzotycznych strojów z różnych miejsc. Pomaga jej w dodaniu kolorytu do strojów pań w Pustogorze. | 0109-12-12 - 0109-12-16 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Erwin Galilien       | 1 | ((190101-morderczyni-jednej-plotki)) |
| Karol Szurnak        | 1 | ((190101-morderczyni-jednej-plotki)) |
| Olaf Zuchwały        | 1 | ((190101-morderczyni-jednej-plotki)) |
| Pięknotka Diakon     | 1 | ((190101-morderczyni-jednej-plotki)) |
| Teresa Mieralit      | 1 | ((190101-morderczyni-jednej-plotki)) |
---
categories: profile
factions: 
owner: public
title: Ofelia Morlan
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210818-siostrzenica-morlana         | z Tomaszem i Jolantą Sowińskimi ratowała szlachtę Eterni przed Natanielem Morlanem przemycając ich przez Netrahinę (działa z cargo na Netrahinie). Kruki Kasandry wrobiły ją w prawdziwy przemyt ludzi i chciały porwać w zamieszaniu; udało się Zespołowi ją uratować. | 0111-11-15 - 0111-11-19 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 210818-siostrzenica-morlana         | ewakuowana z Netrahiny na Kontroler Pierwszy; jest bezpieczna, pod opieką admirała Kramera. | 0111-11-19

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Antoni Kramer        | 1 | ((210818-siostrzenica-morlana)) |
| Arianna Verlen       | 1 | ((210818-siostrzenica-morlana)) |
| Eustachy Korkoran    | 1 | ((210818-siostrzenica-morlana)) |
| Klaudia Stryk        | 1 | ((210818-siostrzenica-morlana)) |
| Leona Astrienko      | 1 | ((210818-siostrzenica-morlana)) |
| Maria Naavas         | 1 | ((210818-siostrzenica-morlana)) |
| Nataniel Morlan      | 1 | ((210818-siostrzenica-morlana)) |
| Olgierd Drongon      | 1 | ((210818-siostrzenica-morlana)) |
| OO Netrahina         | 1 | ((210818-siostrzenica-morlana)) |
| OO Żelazko           | 1 | ((210818-siostrzenica-morlana)) |
| SC Fecundatis        | 1 | ((210818-siostrzenica-morlana)) |
| SC Światłodóbr       | 1 | ((210818-siostrzenica-morlana)) |
| Tomasz Sowiński      | 1 | ((210818-siostrzenica-morlana)) |
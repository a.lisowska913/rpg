---
categories: profile
factions: 
owner: public
title: Staś Arienik
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190709-somnibel-uciekl-arienikom    | 15-16letni arystokrata, arogancki. Wpłynął nań somnibel. Podkochuje się w Małgosi, inną arystokratką. Ogólnie, nic ciekawego. | 0110-06-01 - 0110-06-03 |
| 211221-chevaleresse-infiltruje-rekiny | 17 lat; FAZA NA BUNT! Uciekł z Barnabą Burgaczem do Rekinów by być jednym z nich. Babu i Mysiokornik go osłaniali przed Justynianem, ale spotkał się z Karo... skończył posikany ze strachu i nie chce mieć nic wspólnego z Rekinami. | 0111-09-02 - 0111-09-03 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 211221-chevaleresse-infiltruje-rekiny | przerażony Karoliną Terienak, wielbiciel Barnaby Burgacza, nie chce mieć nic wspólnego z Rekinami BO KAROLINA. | 0111-09-03

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Alan Bartozol        | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Barnaba Burgacz      | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Diana Tevalier       | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Ernest Kajrat        | 1 | ((190709-somnibel-uciekl-arienikom)) |
| Hestia d'Rekiny      | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Jan Revlen           | 1 | ((190709-somnibel-uciekl-arienikom)) |
| Justynian Diakon     | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Karolina Terienak    | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Ksawery Wojnicki     | 1 | ((190709-somnibel-uciekl-arienikom)) |
| Marysia Sowińska     | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Melissa Durszenko    | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Pięknotka Diakon     | 1 | ((190709-somnibel-uciekl-arienikom)) |
| Rupert Mysiokornik   | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Santino Mysiokornik  | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
| Tomasz Tukan         | 1 | ((190709-somnibel-uciekl-arienikom)) |
| Urszula Arienik      | 1 | ((190709-somnibel-uciekl-arienikom)) |
| Żorż d'Namertel      | 1 | ((211221-chevaleresse-infiltruje-rekiny)) |
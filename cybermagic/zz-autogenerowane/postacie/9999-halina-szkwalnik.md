---
categories: profile
factions: 
owner: public
title: Halina Szkwalnik
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 201111-mychainee-na-netrahinie      | dowódca OO Saabar przypisanego do Netrahiny, potem AK Saabar. Zintegrowana z Saabar przez Mychainee. Uratowana przez oddział Inferni, kosztem dewastacji Netrahiny. | 0111-05-07 - 0111-05-10 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 1 | ((201111-mychainee-na-netrahinie)) |
| Eustachy Korkoran    | 1 | ((201111-mychainee-na-netrahinie)) |
| Klaudia Stryk        | 1 | ((201111-mychainee-na-netrahinie)) |
| Martyn Hiwasser      | 1 | ((201111-mychainee-na-netrahinie)) |
| Rufus Komczirp       | 1 | ((201111-mychainee-na-netrahinie)) |
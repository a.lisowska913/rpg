---
categories: profile
factions: 
owner: public
title: Katrina Komczirp
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221230-dowody-na-istnienie-nox-ignis | przeprowadziła samodzielną operację mającą dowiedzieć się o lokalizacji Nox Ignis i o tym czym jest Nox Ignis. Wymanewrowała siły Tristana i wysłała sygnał poruszając się po pancerzu Ratio Spei. Ale wpadła w czyjeś ręce. | 0082-07-28 - 0082-08-01 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| AK Nox Ignis         | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |
| Aletia Nix           | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |
| Dominik Łarnisz      | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |
| Franz Szczypiornik   | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |
| Medea Sowińska       | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |
| OO Loricatus         | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |
| Persefona d'Loricatus | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |
| Sarian Xadaar        | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |
| Talia Derwisz        | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |
| Tatiana Ozariat      | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |
| Tristan Ozariat      | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |
| Wawrzyn Rewemis      | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |
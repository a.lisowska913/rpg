---
categories: profile
factions: 
owner: public
title: Wiktor Szurmak
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 211122-czolenkowe-esuriit-w-amz     | stary nauczyciel AMZ, skostniały, lubi kary cielesne i straszyć. Nauczyciel puryfikacji - podzbiorze katalizy. Uczniowie się go boją bardziej niż nauczycieli wojny. | 0083-10-13 - 0083-10-22 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Dariusz Drewniak     | 1 | ((211122-czolenkowe-esuriit-w-amz)) |
| Ernest Termann       | 1 | ((211122-czolenkowe-esuriit-w-amz)) |
| Felicjan Szarak      | 1 | ((211122-czolenkowe-esuriit-w-amz)) |
| Klaudia Stryk        | 1 | ((211122-czolenkowe-esuriit-w-amz)) |
| Ksenia Kirallen      | 1 | ((211122-czolenkowe-esuriit-w-amz)) |
| Mariusz Trzewń       | 1 | ((211122-czolenkowe-esuriit-w-amz)) |
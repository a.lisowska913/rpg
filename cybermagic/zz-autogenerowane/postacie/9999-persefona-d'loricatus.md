---
categories: profile
factions: 
owner: public
title: Persefona d'Loricatus
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221230-dowody-na-istnienie-nox-ignis | z natury cicha i zdyscyplinowana, lubi z Wawrzynem oglądać i komentować różne operacje cybernetyzacji. | 0082-07-28 - 0082-08-01 |
| 230111-gdy-hr-reedukuje-niewlasciwa-osobe | silniejsza psychotronicznie niż typowa Persefona; posłużyła do połączenia z bazą na Planetoidzie Lodowca by ją uruchomić póki nie przybędzie właściwa TAI. | 0100-09-15 - 0100-09-18 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| AK Nox Ignis         | 2 | ((221230-dowody-na-istnienie-nox-ignis; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| OO Loricatus         | 2 | ((221230-dowody-na-istnienie-nox-ignis; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Adragain Ferrias     | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Aleksy Sartaran      | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Aletia Nix           | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |
| Arianna Verlen       | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Daria Czarnewik      | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Dominik Łarnisz      | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |
| Elena Verlen         | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Franz Szczypiornik   | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |
| Grażyna Burgacz      | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Kajetan Kircznik     | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Katrina Komczirp     | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |
| Lana Mirkinin        | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Leszek Kurzmin       | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Maja Samszar         | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Medea Sowińska       | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |
| Miłosz Klinek        | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| OO Astralna Flara    | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| OO Athamarein        | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Sabrina Ferrias      | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Salazar Bolza        | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Sarian Xadaar        | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |
| Szczepan Myrczek     | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Talia Derwisz        | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |
| Tatiana Ozariat      | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |
| Tristan Ozariat      | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |
| Wawrzyn Rewemis      | 1 | ((221230-dowody-na-istnienie-nox-ignis)) |
| Władawiec Diakon     | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
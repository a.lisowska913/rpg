---
categories: profile
factions: 
owner: public
title: Serentina d'Remora
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220712-pasozytnicze-osy-w-nativis   | dedykowana inteligentna TAI połączona z szybkim ścigaczem klasy Remora; wyprowadziła miragenta Lycoris z kłopotów i opracowała plan jak uratować ludzi i jaki jest cel Pasożyta. | 0092-10-29 - 0092-11-07 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Damian Marlinczak    | 1 | ((220712-pasozytnicze-osy-w-nativis)) |
| Jarlow Gurdacz       | 1 | ((220712-pasozytnicze-osy-w-nativis)) |
| Lycoris Kidiron      | 1 | ((220712-pasozytnicze-osy-w-nativis)) |
| Rafał Kidiron        | 1 | ((220712-pasozytnicze-osy-w-nativis)) |
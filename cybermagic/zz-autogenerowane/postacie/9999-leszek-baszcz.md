---
categories: profile
factions: 
owner: public
title: Leszek Baszcz
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 191108-ukojenie-aleksandrii         | pisarz, który stracił córkę. Opętany żałością, zintegrował się z Aleksandrią i córka wróciła. Stał się organicznym Kuratorem. | 0110-07-14 - 0110-07-16 |
| 191113-jeden-problem-dwie-rodziny   | całkowicie nieświadomy rzeczywistości; żyje w swoim świecie, świecie Aleksandrii. Prawie pożarła go nieumarła córka Dotknięta przez Esuriit a on nic nie wiedział. | 0110-09-26 - 0110-10-01 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Amelia Mirzant       | 2 | ((191108-ukojenie-aleksandrii; 191113-jeden-problem-dwie-rodziny)) |
| Klara Baszcz         | 2 | ((191108-ukojenie-aleksandrii; 191113-jeden-problem-dwie-rodziny)) |
| Paweł Kukułnik       | 2 | ((191108-ukojenie-aleksandrii; 191113-jeden-problem-dwie-rodziny)) |
| Daniela Baszcz       | 1 | ((191108-ukojenie-aleksandrii)) |
| Kamil Lemurczak      | 1 | ((191113-jeden-problem-dwie-rodziny)) |
| Karol Kszatniak      | 1 | ((191113-jeden-problem-dwie-rodziny)) |
| Kinga Stryk          | 1 | ((191108-ukojenie-aleksandrii)) |
| Sabina Kazitan       | 1 | ((191113-jeden-problem-dwie-rodziny)) |
| Teresa Marszalnik    | 1 | ((191113-jeden-problem-dwie-rodziny)) |
---
categories: profile
factions: 
owner: public
title: Konrad Wolczątek
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200722-wielki-kosmiczny-romans      | oficer, artylerzysta OO Welgat; zakochany w Elenie Verlen; miał z nią mieć pojedynek o miłość. Eustachy miłość wybił mu z głowy tłumacząc, że Elena jest w trójkącie między Olgierdem Drongonem a Leoną... czy jakoś tak. | 0110-11-12 - 0110-11-15 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Antoni Kramer        | 1 | ((200722-wielki-kosmiczny-romans)) |
| Arianna Verlen       | 1 | ((200722-wielki-kosmiczny-romans)) |
| Damian Orion         | 1 | ((200722-wielki-kosmiczny-romans)) |
| Elena Verlen         | 1 | ((200722-wielki-kosmiczny-romans)) |
| Eustachy Korkoran    | 1 | ((200722-wielki-kosmiczny-romans)) |
| Julian Muszel        | 1 | ((200722-wielki-kosmiczny-romans)) |
| Leona Astrienko      | 1 | ((200722-wielki-kosmiczny-romans)) |
| Olgierd Drongon      | 1 | ((200722-wielki-kosmiczny-romans)) |
| OO Welgat            | 1 | ((200722-wielki-kosmiczny-romans)) |
| OO Żelazko           | 1 | ((200722-wielki-kosmiczny-romans)) |
| Tadeusz Ursus        | 1 | ((200722-wielki-kosmiczny-romans)) |
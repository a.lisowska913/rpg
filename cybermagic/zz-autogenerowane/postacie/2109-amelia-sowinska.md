---
categories: profile
factions: 
owner: public
title: Amelia Sowińska
---

# {{ page.title }}


# Read: 

## Kim jest

### W kilku zdaniach

TODO

### Co się rzuca w oczy

* TODO

### Jak sterować postacią

* TODO

## Mechanika

### Znaczące Czyny i Osiągnięcia (3)

* Gdy Roland był Skażony Esuriit i doprowadził do serii śmierci i cierpienia, wzięła wszystko na siebie. On jest paladynem, ona administratorką. Jej reputacji to obojętne, jego mogłoby to zniszczyć.
* Przybyła do Rekinów i zastała grupę zblazowaną bez cienia koordynacji czy planu. Zbudowała im system, który jest używany do dzisiaj. Dała im kierunek i powód istnienia, a nie „wygnani arystokraci”. Kierunek przez nią nadany jest taki jak Roland kazał jej nadać - by pomagali.
* Widząc eskalujący konflikt Morlan - Jolanta Sowińska wprowadziła siebie na dwór Morlana by neutralizować Jolantę. Bo działania Jolanty źle rzutują na ród Sowińskich, ale też po to, by zrozumieć gdzie zrobiła błąd z Rolandem. Jak mogłaby to naprawić. I jak zemścić się na Oliwii.

### Co się rzuca w oczy: Atuty i Przewagi (3, 6)

* CECHA: uprawnienia inspektora hipernetu. Może przechwytywać i dekodować dalekosiężne komunikaty
* AKCJA: organizuje i zajmuje się logistyką. Dbałość o detale. Potrafi sprawnie zarządzać grupą osób
* AKCJA: świetna manipulatorka na dworze. Potrafi używając struktur Aurum umieścić kogo chce gdzie chce i wpakować w kłopoty. 
* AKCJA: przetwarza informacje. Potrafi dużo ukryć lub wydobyć z komunikacji i dokumentów.
* AKCJA: dobra aktorka i negocjatorka. Potrafi być słodka, zmotywować, zainspirować, sprawić, że myślisz, że jej na Tobie zależy. W rozmowie dostaje co chce.
* COŚ: mnóstwo kompromitujących materiałów i sekretów na różne osoby w Aurum. Głównie z hipernetu. Dlatego „nie może zdradzić”.

### Serce i Wartości (3)

* Tradycja 
    * Łańcuch dowodzenia jest święty. Wykonuję rozkazy nawet jak mi się nie podobają, Ty też będziesz.
    * Każdy ma swoje miejsce w maszynie zwanej systemem. Moją odpowiedzialnością jest zadbanie, by każdy był jak najlepiej wykorzystany.
    * Jeśli coś złego się dzieje, ZAWSZE odpowiedzialny jest przełożony. Ważna jest intencja, nie słowa.
* Twarz
    * Reputacja jest WSZYSTKIM. Jest ważniejsza niż życie czy cierpienie. Nie umiesz jej utrzymać - nie jesteś godną arystokratką.
    * Jeśli coś zawaliłam, muszę naprawić. Nie godzi się uciekać od swoich porażek. She owns it and requires others to own it, too.
    * Ważniejsze jest jak widzą cię Ci wyżej niż ci niżej. Wymagaj szacunku i szanuj lepszych od siebie.
    * Możesz wybaczać, ale zawsze publicznie się zemścij. Pokaż innym, że nie mogą Cię wykorzystywać.
* Stymulacja
    * Zabawa, rozrywka i wysokie morale nie są bonusem czy elementem efektywności. To kwestia moralności.
    * Jeśli można coś zrobić fajne i interesujące kosztem niewielkiej straty efektywności, warto to zrobić. 
    * Marzy i szuka gorącej, romantycznej miłości. Wierzy w miłość i pragnie uniesień.

### Typowe problemy z którymi sobie nie radzi; Słabości (-3)

* CORE WOUND: "Wszyscy traktują mnie jak TAI. Amelia rozwiąże problem. Nikt nie zauważa we mnie osoby. Nikomu na mnie nie zależy. Gdybym zniknęła, wymieniliby mnie na Persefonę."
* CORE LIE: "Jeżeli rozwiążę bardzo trudny problem sama w sposób unikalny to mnie zauważą. Będę warta miłości. Ktoś mnie pokocha."
* pod wpływem stresu wpada w lekką paranoję. Nie oddelegowuje. Wszystko zrobi sama. Da się ją przeładować.
* strasznie dumna, pyszna. Nie jest trudno wpakować ją w pułapkę „ja siama”. Nienawidzi przyznania się do słabości i zrobi dużo, by ukryć swoje wszelkie braki
* mimo, że jest dobrą aktorką, nie dba o fasadę wobec osób które uważa za niższe.

### Magia (3M)

#### W czym jest świetna

* TODO

#### Jak się objawia utrata kontroli

* TODO

### Specjalne

* .

## Inne

### Wygląd

* ?

### Coś Więcej

* .

### Endgame

* 


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 211120-glizda-ktora-leczy           | trzymała w kupie i organizowała Rekiny zgodnie z życzeniami Rolanda mimo Esuriit i jego chorych pomysłów. Nie dopuszczała, by jego żądania Entropików przeszły dalej. Ukrywa koszt reputacyjny za wszelką cenę. Daje się manipulować Sabinie Kazitan / Oliwii Lemurczak. | 0108-04-03 - 0108-04-14 |
| 210824-mandragora-nienawidzi-rekinow | wielka nieobecna; kiedyś była na tym terenie jako Rekin, kochana przez Rekiny i znienawidzona przez miejscowych. Duchy pomordowanych tworzące mandragorę oskarżyły Marysię o bycie Amelią. | 0111-06-29 - 0111-07-01 |
| 210921-przybycie-rekina-z-eterni    | ona trafia do Eterni "na wymianę", do Morlana; próbuje chronić swojego kochanego Ernesta i zamiast do Aurum przekierowała go do Podwiertu. Próbuje deeskalować starcie Morlan - Jolka Sowińska. Okazuje się, że Amelia ma dostęp do rzeczy jakich Eternia nie powinna nigdy wiedzieć (dokumenty, uprawnienia), więc popełnia "zdradę" lub "mezalians". AMELIA idzie za sercem. Skonfliktowana politycznie z Jolantą Sowińską. Aha, przyznała się w emocjach Marysi, że to NIE ONA stoi za morderstwami ludzi w Podwiercie. Kogoś chroni. Aha, Amelia wrobiła Marysię przez Aurum w to że ta PERSONALNIE odpowiada za bezpieczeństwo Ernesta. | 0111-07-16 - 0111-07-21 |
| 211012-torszecki-pokazal-kregoslup  | zaplanowała by pokazać Morlanowi krew "córki Morlana" by udowodnić, że to dobry pomysł. Przekonała Dwór Sowińskich. | 0111-07-25 - 0111-07-26 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 211120-glizda-ktora-leczy           | wściekła na Stellę Sowińską. Ta jej przekazała dowodzenie Rekinami w najgorszym momencie. Zemści się kiedyś. Tak, jak wściekła na Oliwię Lemurczak na której TEŻ się zemści. | 0108-04-14
| 210824-mandragora-nienawidzi-rekinow | naprawdę nie lubi Marysi Sowińskiej. | 0111-07-01
| 220111-marysiowa-hestia-rekinow     | odzyskała częściowo kontakt z Rekinami i Aurum (bo konsultowali się wrt Hestii itp) | 0111-09-15
| 220802-gdy-prawnik-przyjdzie-po-rekiny | Ernest zerwał z nią zaręczyny, bo nie jest jedyną dziewczyną w jego życiu (Marysia). Plotki jej mówią o tym że to wina Marysi. | 0111-10-07

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Karolina Terienak    | 3 | ((210824-mandragora-nienawidzi-rekinow; 210921-przybycie-rekina-z-eterni; 211012-torszecki-pokazal-kregoslup)) |
| Marysia Sowińska     | 3 | ((210824-mandragora-nienawidzi-rekinow; 210921-przybycie-rekina-z-eterni; 211012-torszecki-pokazal-kregoslup)) |
| Sensacjusz Diakon    | 3 | ((210824-mandragora-nienawidzi-rekinow; 211012-torszecki-pokazal-kregoslup; 211120-glizda-ktora-leczy)) |
| Jolanta Sowińska     | 2 | ((210921-przybycie-rekina-z-eterni; 211012-torszecki-pokazal-kregoslup)) |
| Karol Pustak         | 2 | ((210921-przybycie-rekina-z-eterni; 211120-glizda-ktora-leczy)) |
| Lucjan Sowiński      | 2 | ((210921-przybycie-rekina-z-eterni; 211012-torszecki-pokazal-kregoslup)) |
| Rafał Torszecki      | 2 | ((210921-przybycie-rekina-z-eterni; 211012-torszecki-pokazal-kregoslup)) |
| Tomasz Tukan         | 2 | ((210824-mandragora-nienawidzi-rekinow; 210921-przybycie-rekina-z-eterni)) |
| Amanda Kajrat        | 1 | ((211120-glizda-ktora-leczy)) |
| Arkadia Verlen       | 1 | ((210921-przybycie-rekina-z-eterni)) |
| Daniel Terienak      | 1 | ((210824-mandragora-nienawidzi-rekinow)) |
| Ekaterina Zajcew     | 1 | ((210824-mandragora-nienawidzi-rekinow)) |
| Ernest Kajrat        | 1 | ((211120-glizda-ktora-leczy)) |
| Ernest Namertel      | 1 | ((210921-przybycie-rekina-z-eterni)) |
| Feliks Keksik        | 1 | ((211120-glizda-ktora-leczy)) |
| Justynian Diakon     | 1 | ((211120-glizda-ktora-leczy)) |
| Kacper Bankierz      | 1 | ((211120-glizda-ktora-leczy)) |
| Laura Tesinik        | 1 | ((210824-mandragora-nienawidzi-rekinow)) |
| Nataniel Morlan      | 1 | ((210921-przybycie-rekina-z-eterni)) |
| Oliwia Lemurczak     | 1 | ((211120-glizda-ktora-leczy)) |
| Roland Sowiński      | 1 | ((211120-glizda-ktora-leczy)) |
| Sabina Kazitan       | 1 | ((211120-glizda-ktora-leczy)) |
| Stella Sowińska      | 1 | ((211120-glizda-ktora-leczy)) |
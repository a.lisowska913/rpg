---
categories: profile
factions: 
owner: public
title: Rufus Niegnat
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210127-porwanie-anastazji-z-odkupienia | groźny dowódca Plugawego Jaszczura; tu symulowany przez Persefonę przez anomalię Klaudii. Lubi niewolników, zwłaszcza płci żeńskiej. | 0111-05-24 - 0111-05-25 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Anastazja Sowińska Dwa | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| Arianna Verlen       | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| Dariusz Krantak      | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| Diana Arłacz         | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| Elena Verlen         | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| Eustachy Korkoran    | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| Klaudia Stryk        | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| Martyn Hiwasser      | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| OA Odkupienie        | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| OO Infernia          | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| SP Plugawy Jaszczur  | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
---
categories: profile
factions: 
owner: public
title: Staszek Zakraton
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221006-ona-chce-dziecko-eustachego  | Nativis / Robak; ekspert od energii (41), złośliwy i żartujący sobie z ich sytuacji na crawlerze Exerinn. Wrócił na Infernię; życie na Exerinnie nie dla niego. Stracił nogę by zamaskować, że żyje. | 0092-09-20 - 0092-09-24 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ardilla Korkoran     | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| Ava Kieras           | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| Emban Dolamor        | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| Eustachy Korkoran    | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| Lerten Kieras        | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| Maks Selert          | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| Michał Kervendal     | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| OO Infernia          | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| VN Exerinn           | 1 | ((221006-ona-chce-dziecko-eustachego)) |
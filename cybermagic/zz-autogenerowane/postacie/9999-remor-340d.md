---
categories: profile
factions: 
owner: public
title: Remor 340D
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 201013-pojedynek-akademia-rekiny    | stary astoriański ścigacz; sprzężony psychotronicznie z czasem wojny noktiańskiej. Posłużył jako pojazd Napoleona Bankierza i pokazał Rekinom czym jest honor. | 0110-10-10 - 0110-10-18 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Aleksander Bemucik   | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Ignacy Myrczek       | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Julia Kardolin       | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Justynian Diakon     | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Kacper Bankierz      | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Liliana Bankierz     | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Napoleon Bankierz    | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Robert Pakiszon      | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Stella Armadion      | 1 | ((201013-pojedynek-akademia-rekiny)) |
| Teresa Mieralit      | 1 | ((201013-pojedynek-akademia-rekiny)) |
---
categories: profile
factions: 
owner: public
title: Karol Pustak
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 211120-glizda-ktora-leczy           | młody miłośnik arystokracji Aurum z okolic Pustogoru. Założył "Opowiastki z Aurum" i zrobił wywiad z Pożeraczem. To był sygnał tego, że Roland stracił kontrolę nad Rekinami. | 0108-04-03 - 0108-04-14 |
| 210921-przybycie-rekina-z-eterni    | uczeń AMZ i miłośnik "Royalsów". Wie więcej o arystokracji Aurum niż oni sami. | 0111-07-16 - 0111-07-21 |
| 211123-odbudowa-wedlug-justyniana   | goniec Ignacego Myrczka do Marysi Sowińskiej; przyszedł w pełnej liberii i strzygł uszami za plotkami. | 0111-08-07 - 0111-08-16 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Amelia Sowińska      | 2 | ((210921-przybycie-rekina-z-eterni; 211120-glizda-ktora-leczy)) |
| Ernest Namertel      | 2 | ((210921-przybycie-rekina-z-eterni; 211123-odbudowa-wedlug-justyniana)) |
| Justynian Diakon     | 2 | ((211120-glizda-ktora-leczy; 211123-odbudowa-wedlug-justyniana)) |
| Karolina Terienak    | 2 | ((210921-przybycie-rekina-z-eterni; 211123-odbudowa-wedlug-justyniana)) |
| Marysia Sowińska     | 2 | ((210921-przybycie-rekina-z-eterni; 211123-odbudowa-wedlug-justyniana)) |
| Rafał Torszecki      | 2 | ((210921-przybycie-rekina-z-eterni; 211123-odbudowa-wedlug-justyniana)) |
| Amanda Kajrat        | 1 | ((211120-glizda-ktora-leczy)) |
| Arkadia Verlen       | 1 | ((210921-przybycie-rekina-z-eterni)) |
| Azalia Sernat d'Namertel | 1 | ((211123-odbudowa-wedlug-justyniana)) |
| Daniel Terienak      | 1 | ((211123-odbudowa-wedlug-justyniana)) |
| Ernest Kajrat        | 1 | ((211120-glizda-ktora-leczy)) |
| Feliks Keksik        | 1 | ((211120-glizda-ktora-leczy)) |
| Ignacy Myrczek       | 1 | ((211123-odbudowa-wedlug-justyniana)) |
| Jolanta Sowińska     | 1 | ((210921-przybycie-rekina-z-eterni)) |
| Kacper Bankierz      | 1 | ((211120-glizda-ktora-leczy)) |
| Lucjan Sowiński      | 1 | ((210921-przybycie-rekina-z-eterni)) |
| Nataniel Morlan      | 1 | ((210921-przybycie-rekina-z-eterni)) |
| Oliwia Lemurczak     | 1 | ((211120-glizda-ktora-leczy)) |
| Roland Sowiński      | 1 | ((211120-glizda-ktora-leczy)) |
| Sabina Kazitan       | 1 | ((211120-glizda-ktora-leczy)) |
| Sensacjusz Diakon    | 1 | ((211120-glizda-ktora-leczy)) |
| Stella Sowińska      | 1 | ((211120-glizda-ktora-leczy)) |
| Tomasz Tukan         | 1 | ((210921-przybycie-rekina-z-eterni)) |
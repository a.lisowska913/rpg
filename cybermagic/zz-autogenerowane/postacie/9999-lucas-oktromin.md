---
categories: profile
factions: 
owner: public
title: Lucas Oktromin
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230124-kjs-wygrac-za-wszelka-cene   | traktuje Elenę jak małą gówniarę, zero szacunku, nawet jak ona się stara. Odmówił jej pojedynku i współpracy, acz zapewnił z jej strony pomoc w zdobyciu baterii do Kajisa. | 0095-06-23 - 0095-06-25 |
| 230307-kjs-stymulanty-szeptomandry  | (30) przekonał Apollo Verlena do tego, by ten pomógł zestrzelić Szeptomandrę, że dają mu wszystko na talerzu i że potrzebują baterie irianium. Aha, są tylko turystami. | 0095-06-26 - 0095-06-29 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Apollo Verlen        | 2 | ((230124-kjs-wygrac-za-wszelka-cene; 230307-kjs-stymulanty-szeptomandry)) |
| KDN Kajis            | 2 | ((230124-kjs-wygrac-za-wszelka-cene; 230307-kjs-stymulanty-szeptomandry)) |
| Lidia Nemert         | 2 | ((230124-kjs-wygrac-za-wszelka-cene; 230307-kjs-stymulanty-szeptomandry)) |
| Marta Krissit        | 2 | ((230124-kjs-wygrac-za-wszelka-cene; 230307-kjs-stymulanty-szeptomandry)) |
| Żaneta Krawędź       | 2 | ((230124-kjs-wygrac-za-wszelka-cene; 230307-kjs-stymulanty-szeptomandry)) |
| Elena Verlen         | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Emilia Lawendowiec   | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Franciszek Korel     | 1 | ((230307-kjs-stymulanty-szeptomandry)) |
| Karol Atenuatia      | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Michał Waczarek      | 1 | ((230307-kjs-stymulanty-szeptomandry)) |
| Serena Krissit       | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Wojciech Namczak     | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Zuzanna Kraczamin    | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
---
categories: profile
factions: 
owner: public
title: Hubert Kerwelenios
---

# {{ page.title }}


# Generated: 



## Fiszki


* advancer, sarderyta | @ 221026-kapitan-verlen-i-koniec-przygody-na-krolowej
* ENCAO:  -0+-0 |Kontemplacyjny, refleksyjny;;Kompleks paladyna| VALS: Humility, Tradition >> Stimulation| DRIVE: Odbudowa i odnowa | @ 221026-kapitan-verlen-i-koniec-przygody-na-krolowej

### Wątki


historia-arianny
program-kosmiczny-aurum

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221012-kapitan-verlen-i-niezapowiedziana-inspekcja | sarderycki advancer; nie ma uprawnień, ale to nieważne. Bardzo zależy mu by być lepszym. Nie opuszcza go dobry humor. Spacer kosmiczny z Darią i innymi advancerami by analizować poszycie Królowej. (ENCAO: +0-+-, Optymistyczny i wesoły, Achievement nie Humility, silnik: Wanderlust). | 0100-03-19 - 0100-03-23 |
| 221102-astralna-flara-i-porwanie-na-karsztarinie | współpracując z Eleną, skutecznie zrobił insercję i unieszkodliwił komandosów Aureliona próbujących porwać kadeta z Karsztarina. | 0100-05-30 - 0100-06-05 |
| 221130-astralna-flara-w-strefie-duchow | insercja na TKO-4271, przechwycenie Kirei i ewakuacja nanowłóknem. Doskonała operacja advancerska. | 0100-08-07 - 0100-08-10 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 3 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221130-astralna-flara-w-strefie-duchow)) |
| Arnulf Perikas       | 3 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221130-astralna-flara-w-strefie-duchow)) |
| Daria Czarnewik      | 3 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221130-astralna-flara-w-strefie-duchow)) |
| Maja Samszar         | 3 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221130-astralna-flara-w-strefie-duchow)) |
| Elena Verlen         | 2 | ((221102-astralna-flara-i-porwanie-na-karsztarinie; 221130-astralna-flara-w-strefie-duchow)) |
| Kajetan Kircznik     | 2 | ((221102-astralna-flara-i-porwanie-na-karsztarinie; 221130-astralna-flara-w-strefie-duchow)) |
| Klarysa Jirnik       | 2 | ((221102-astralna-flara-i-porwanie-na-karsztarinie; 221130-astralna-flara-w-strefie-duchow)) |
| Mariusz Bulterier    | 2 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Szczepan Myrczek     | 2 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Tomasz Dojnicz       | 2 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221130-astralna-flara-w-strefie-duchow)) |
| Władawiec Diakon     | 2 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja; 221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Ada Wyrocznik        | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Adam Chrząszczewicz  | 1 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja)) |
| Alezja Dumorin       | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Ellarina Samarintael | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| Gabriel Lodowiec     | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| Gerwazy Kircznik     | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Grażyna Burgacz      | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Kirea Rialirat       | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| Leo Kasztop          | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| Leona Astrienko      | 1 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja)) |
| NekroTAI Zarralea    | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| OO Astralna Flara    | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| OO Athamarein        | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| OO Karsztarin        | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| OO Królowa Kosmicznej Chwały | 1 | ((221012-kapitan-verlen-i-niezapowiedziana-inspekcja)) |
| OO Optymistyczny Żuk | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Szymon Wanad         | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
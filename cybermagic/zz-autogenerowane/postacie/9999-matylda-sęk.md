---
categories: profile
factions: 
owner: public
title: Matylda Sęk
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200616-bardzo-straszna-mysz         | burmistrz Czółenka, człowiek. Widziała mysz Esuriit i owa mysz pokazywała się jako straszliwy niepokonywalny potwór. Wezwała SOS z Pustogoru. | 0110-09-14 - 0110-09-17 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Diana Lauris         | 1 | ((200616-bardzo-straszna-mysz)) |
| Franek Bulterier     | 1 | ((200616-bardzo-straszna-mysz)) |
| Gabriel Ursus        | 1 | ((200616-bardzo-straszna-mysz)) |
| Henryk Wkrąż         | 1 | ((200616-bardzo-straszna-mysz)) |
| Ksenia Kirallen      | 1 | ((200616-bardzo-straszna-mysz)) |
| Laura Tesinik        | 1 | ((200616-bardzo-straszna-mysz)) |
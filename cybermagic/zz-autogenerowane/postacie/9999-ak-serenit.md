---
categories: profile
factions: 
owner: public
title: AK Serenit
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210428-infekcja-serenit             | leci w kierunku na OE Falołamacz (który staje się "odłamkiem Serenit"). | 0111-09-24 - 0111-09-25 |
| 210512-ewakuacja-z-serenit          | Klaudia odwróciła jego uwagę co kupiło czas. Pozyskał Falołamacz jako jeden ze swoich statków. Sposób infekcji: zastępuje advancera swoim konstruktem i wprowadza Serenit na statek macierzysty. | 0111-09-25 - 0111-10-04 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 210512-ewakuacja-z-serenit          | WIE o istnieniu Eleny Verlen i o Inferni. Jeśli jest gdzieś w pobliżu, może na nich zapolować. | 0111-10-04

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 2 | ((210428-infekcja-serenit; 210512-ewakuacja-z-serenit)) |
| Elena Verlen         | 2 | ((210428-infekcja-serenit; 210512-ewakuacja-z-serenit)) |
| Eustachy Korkoran    | 2 | ((210428-infekcja-serenit; 210512-ewakuacja-z-serenit)) |
| Klaudia Stryk        | 2 | ((210428-infekcja-serenit; 210512-ewakuacja-z-serenit)) |
| Martyn Hiwasser      | 2 | ((210428-infekcja-serenit; 210512-ewakuacja-z-serenit)) |
| OE Falołamacz        | 2 | ((210428-infekcja-serenit; 210512-ewakuacja-z-serenit)) |
| OO Infernia          | 2 | ((210428-infekcja-serenit; 210512-ewakuacja-z-serenit)) |
| Aida Serenit         | 1 | ((210512-ewakuacja-z-serenit)) |
| Bogdan Anatael       | 1 | ((210512-ewakuacja-z-serenit)) |
| OE Piękna Elena      | 1 | ((210428-infekcja-serenit)) |
| Persefona d'Infernia | 1 | ((210428-infekcja-serenit)) |
| Rafał Grambucz       | 1 | ((210428-infekcja-serenit)) |
| Roland Sowiński      | 1 | ((210512-ewakuacja-z-serenit)) |
| Tadeusz Ursus        | 1 | ((210428-infekcja-serenit)) |
---
categories: profile
factions: 
owner: public
title: Fabian Korneliusz
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220716-chory-piesek-na-statku-luxuritias | młody (24) porucznik Orbitera, dowódca Martyna i Klaudii na OO Serbinius; niekoniecznie utrzymuje język na wodzy. Petty one, cieszy się z nieszczęścia Luxuritias i z przyjemnością chowa się za procedurami. Ale współpracuje z Luxuritias by zatrzymać plagę Śmiechowicy na ich jednostce. | 0109-05-05 - 0109-05-07 |
| 220925-mlodziaki-na-savaranskim-statku-handlowym | syn jego mentora wpakował się na statek cywilny z dziewczyną i 'zniknął'. Fabian lokalizuje (Klaudią) jednostkę cywilną 'Hektor 13' i robi wszystko, by wyciągnąć te osoby. Jest skłonny nawet zadziałać niezgodnie z celami Orbitera. I nie zauważył że 'Hektor 13' to jednostka neonoktiańska. | 0109-05-24 - 0109-05-25 |
| 230521-rozszczepiona-persefona-na-itorwienie | niecierpliwy, ale sensowny - co prawda nie dał Klaudii dość czasu na 100% danych, ale skutecznie przejął kontrolę nad Itorwienem z pomocą Klaudii i killware. Poproszony przez Klaudię, dał pozytywną rekomendację noktiańskiemu advancerowi z Itorwiena. | 0109-09-15 - 0109-09-17 |
| 230528-helmut-i-nieoczekiwana-awaria-lancera | dowódca który mimo zmęczenia całej załogi nie ignoruje alarmu Semli na jednostce cywilnej (acz warknął na Klaudię by się spieszyła). Dba o swoją załogę, acz jeśli się da to się chce podlizać Mirandzie Termann i innym siłom wyższym. | 0109-09-23 - 0109-09-26 |
| 230530-ziarno-kuratorow-na-karnaxianie | bardzo zależy mu by uratować wszystkich ludzi. Jak zawsze, słucha bardziej doświadczonych agentów Orbitera (Leo, Klaudia, Martyn). Zależy mu na uratowaniu jednostki przejętej przez Kuratorów do tego stopnia, że zagroził bezpieczeństwu Serbiniusa - dopiero jak Leo mu uzmysłowił, że jak Karnaxian odleci to zabierze _away party_ ze sobą, Fabian zmienił zdanie. | 0109-10-06 - 0109-10-07 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 220716-chory-piesek-na-statku-luxuritias | ma jakieś problemy z Valentiną z uwagi na swoją gorącą głowę i ego; nie dostaje stamtąd wsparcia czy pomocy. | 0109-05-07
| 230530-ziarno-kuratorow-na-karnaxianie | niezadowolenie na Leo Mikirnika; jego zdaniem Leo nie dba o ludzi i idzie za daleko. Ma ranę psychiczną po tym jak nie uratował ludzi od Kuratora Sarkamaira i projektował to na Leo. | 0109-10-07

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Klaudia Stryk        | 5 | ((220716-chory-piesek-na-statku-luxuritias; 220925-mlodziaki-na-savaranskim-statku-handlowym; 230521-rozszczepiona-persefona-na-itorwienie; 230528-helmut-i-nieoczekiwana-awaria-lancera; 230530-ziarno-kuratorow-na-karnaxianie)) |
| Martyn Hiwasser      | 5 | ((220716-chory-piesek-na-statku-luxuritias; 220925-mlodziaki-na-savaranskim-statku-handlowym; 230521-rozszczepiona-persefona-na-itorwienie; 230528-helmut-i-nieoczekiwana-awaria-lancera; 230530-ziarno-kuratorow-na-karnaxianie)) |
| OO Serbinius         | 5 | ((220716-chory-piesek-na-statku-luxuritias; 220925-mlodziaki-na-savaranskim-statku-handlowym; 230521-rozszczepiona-persefona-na-itorwienie; 230528-helmut-i-nieoczekiwana-awaria-lancera; 230530-ziarno-kuratorow-na-karnaxianie)) |
| Helmut Szczypacz     | 3 | ((230521-rozszczepiona-persefona-na-itorwienie; 230528-helmut-i-nieoczekiwana-awaria-lancera; 230530-ziarno-kuratorow-na-karnaxianie)) |
| Anastazy Termann     | 2 | ((230528-helmut-i-nieoczekiwana-awaria-lancera; 230530-ziarno-kuratorow-na-karnaxianie)) |
| Achellor Santorinus  | 1 | ((220716-chory-piesek-na-statku-luxuritias)) |
| Bożena Kokorobant    | 1 | ((220716-chory-piesek-na-statku-luxuritias)) |
| Dawid Aximar         | 1 | ((220925-mlodziaki-na-savaranskim-statku-handlowym)) |
| Emilia Ibris         | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Henryk Urkon         | 1 | ((220716-chory-piesek-na-statku-luxuritias)) |
| Iskander Matorin     | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Karol Brinik         | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Klaudiusz Zanęcik    | 1 | ((220925-mlodziaki-na-savaranskim-statku-handlowym)) |
| Kurator Sarkamair    | 1 | ((230530-ziarno-kuratorow-na-karnaxianie)) |
| Leo Mikirnik         | 1 | ((230530-ziarno-kuratorow-na-karnaxianie)) |
| Miranda Termann      | 1 | ((230528-helmut-i-nieoczekiwana-awaria-lancera)) |
| Mojra Karstall       | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Nikodem Dewiremicz   | 1 | ((230528-helmut-i-nieoczekiwana-awaria-lancera)) |
| OO Itorwien          | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Perdius Aximar       | 1 | ((220925-mlodziaki-na-savaranskim-statku-handlowym)) |
| Roberto Santorinus   | 1 | ((220716-chory-piesek-na-statku-luxuritias)) |
| SC Hektor 17         | 1 | ((220925-mlodziaki-na-savaranskim-statku-handlowym)) |
| SC Karnaxian         | 1 | ((230530-ziarno-kuratorow-na-karnaxianie)) |
| SL Rajasee Bagh      | 1 | ((220716-chory-piesek-na-statku-luxuritias)) |
| Tadeusz Arkaladis    | 1 | ((230521-rozszczepiona-persefona-na-itorwienie)) |
| Teodor Margrabarz    | 1 | ((220716-chory-piesek-na-statku-luxuritias)) |
---
categories: profile
factions: 
owner: public
title: Romeo Verlen
---

# {{ page.title }}


# Generated: 



## Fiszki


* młody mag (rok starszy od Eleny) skupiający się na ostrej rywalizacji i głupich dowcipach. Świetny w vircie i grach. Perfekcyjny w zwielokatnianiu swojej uwagi. | @ 230328-niepotrzebny-ratunek-mai
* ENCAO:  +00-+ |Brutalny, grubiański;;Wszystko jest rywalizacją;;Innowacyjny| VALS: Face, Tradition >> Benevolence| DRIVE: Red Queen Race | @ 230328-niepotrzebny-ratunek-mai
* styl: BW; uporządkowany wobec systemu ale jako Verlen ceni ruchy wbrew systemowi które są zrobione dla wielkiej pasji. Czyli ceni RU. | @ 230328-niepotrzebny-ratunek-mai
* ENCAO:  +00-+ |Konfliktowy;; drama queen;;Z poczuciem humoru;; płomienny | VALS: Stimulation, Tradition >> Universalism | DRIVE: folk hero ratujący Maję (BWR) | @ 230523-romeo-dyskretny-instalator-supreme-missionforce
* styl: (BWR), wesoły i niezwykle agresywny, combat engineer z ogromną ilością sprzętu bojowego | @ 230523-romeo-dyskretny-instalator-supreme-missionforce

### Wątki


waśnie-samszar-verlen
szamani-rodu-samszar
mroczna-wiedza-samszarów

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230328-niepotrzebny-ratunek-mai     | przekonał Viorikę do utrzymania Mai na czas Supreme Missionforce, przekonał Apolla do idiotycznego okupu by kupić czas i wygadał się niefortunnie Karolinusowi że temat z Mają to nie jest proste porwanie. | 0095-06-30 - 0095-07-02 |
| 230523-romeo-dyskretny-instalator-supreme-missionforce | waleczny acz pełen pasji (BWR: individual, order, emotion). Nie lubi Mai Samszar, ale chciał podjechać jej zamontować Supreme Missionforce Chambers by ta mogła ćwiczyć i być lepsza. By "nie było wstydu jak przeciw niej walczy". Pożyczył ciężką ciężarówkę, monterów i podjechał do Karmazynowego Świtu (z 3 monterami i bardziej doświadczonymi ludźmi). A potem z Mają znalazł Dziwną Ukrytą Bazę i się uwięził XD. Odpalił techno i wyładowania by nie znalazły go potwory i schował się w kwaterach mieszkalnych. Jak Samszarowie wyszli, pomógł im z jednym potworem i w "kanapce" został ewakuowany przez Samszarów. | 0095-08-09 - 0095-08-11 |
| 210324-lustrzane-odbicie-eleny      | przez Czapurta jest w jednym oddziale z Eleną. Dokucza konsekwentnie Elenie oraz pod koniec autoryzował unieruchomienie Eleny (on, Viorika i Arianna). | 0097-07-05 - 0097-07-08 |
| 221019-kapitan-verlen-i-pierwszy-ruch-statku | KIEDYŚ zniszczył jedzenie Mai i poderwał jej kuzynkę. A potem walczył nie fair. Więc Maja i kilka Samszarów ma na niego krechę i chcą go pojechać. Przez niego Arianna ma teraz problemy z Mają Samszar. | 0100-03-25 - 0100-04-06 |
| 210210-milosc-w-rodzie-verlen       | kochał się kiedyś w Elenie, teraz jej wieczny rywal. Przegrał z nią pojedynek na pilotaż. Waleczny i odważny, oddany rodowi. BW. | 0111-06-23 - 0111-06-26 |
| 210331-elena-z-rodu-verlen          | zmanipulowany przez Ariannę, wyznał Elenie miłość i raz na zawsze zamknął z nią tą chorą rywalizację. Teraz Elena nim trochę gardzi i się lituje, nie nienawidzi... | 0111-06-29 - 0111-07-02 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 230606-piekna-diakonka-i-rytual-nirwany-koz | dostał opieprz od Vioriki za to, że Elena S. wysłała do niej "omg zawiódł operację". Nie dlatego, że zawiódł operację. Dlatego, że R. pokazał tak dużą słabość że się E.S. odważyła napisać. | 0095-08-18

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 4 | ((210210-milosc-w-rodzie-verlen; 210324-lustrzane-odbicie-eleny; 210331-elena-z-rodu-verlen; 221019-kapitan-verlen-i-pierwszy-ruch-statku)) |
| Viorika Verlen       | 4 | ((210210-milosc-w-rodzie-verlen; 210324-lustrzane-odbicie-eleny; 210331-elena-z-rodu-verlen; 230328-niepotrzebny-ratunek-mai)) |
| Elena Verlen         | 3 | ((210210-milosc-w-rodzie-verlen; 210324-lustrzane-odbicie-eleny; 210331-elena-z-rodu-verlen)) |
| Maja Samszar         | 3 | ((221019-kapitan-verlen-i-pierwszy-ruch-statku; 230328-niepotrzebny-ratunek-mai; 230523-romeo-dyskretny-instalator-supreme-missionforce)) |
| AJA Szybka Strzała   | 2 | ((230328-niepotrzebny-ratunek-mai; 230523-romeo-dyskretny-instalator-supreme-missionforce)) |
| Apollo Verlen        | 2 | ((210210-milosc-w-rodzie-verlen; 230328-niepotrzebny-ratunek-mai)) |
| Karolinus Samszar    | 2 | ((230328-niepotrzebny-ratunek-mai; 230523-romeo-dyskretny-instalator-supreme-missionforce)) |
| Albert Samszar       | 1 | ((230523-romeo-dyskretny-instalator-supreme-missionforce)) |
| Bonifacy Samszar     | 1 | ((230328-niepotrzebny-ratunek-mai)) |
| Brunhilda Verlen     | 1 | ((210210-milosc-w-rodzie-verlen)) |
| Daria Czarnewik      | 1 | ((221019-kapitan-verlen-i-pierwszy-ruch-statku)) |
| Dariusz Blakenbauer  | 1 | ((210331-elena-z-rodu-verlen)) |
| Elena Samszar        | 1 | ((230523-romeo-dyskretny-instalator-supreme-missionforce)) |
| Erwin Pies           | 1 | ((221019-kapitan-verlen-i-pierwszy-ruch-statku)) |
| Fiona Szarstasz      | 1 | ((230328-niepotrzebny-ratunek-mai)) |
| Franz Verlen         | 1 | ((210210-milosc-w-rodzie-verlen)) |
| Krucjusz Verlen      | 1 | ((210210-milosc-w-rodzie-verlen)) |
| Krystian Blakenbauer | 1 | ((210331-elena-z-rodu-verlen)) |
| Lucjusz Blakenbauer  | 1 | ((210324-lustrzane-odbicie-eleny)) |
| Marcelina Trzęsiel   | 1 | ((221019-kapitan-verlen-i-pierwszy-ruch-statku)) |
| Michał Perikas       | 1 | ((210324-lustrzane-odbicie-eleny)) |
| Nataniel Samszar     | 1 | ((230523-romeo-dyskretny-instalator-supreme-missionforce)) |
| OO Królowa Kosmicznej Chwały | 1 | ((221019-kapitan-verlen-i-pierwszy-ruch-statku)) |
| Przemysław Czapurt   | 1 | ((210324-lustrzane-odbicie-eleny)) |
| Rufus Warkoczyk      | 1 | ((221019-kapitan-verlen-i-pierwszy-ruch-statku)) |
| Seraf Verlen         | 1 | ((210210-milosc-w-rodzie-verlen)) |
| Stefan Torkil        | 1 | ((221019-kapitan-verlen-i-pierwszy-ruch-statku)) |
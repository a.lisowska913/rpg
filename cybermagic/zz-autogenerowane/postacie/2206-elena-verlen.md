---
categories: profile
factions: 
owner: public
title: Elena Verlen
---

# {{ page.title }}


# Read: 

## Kim jest

### W kilku zdaniach

Kiedyś - arystokratka Aurum rodu Verlen, uciekająca przed swoją przeszłością w powinność i zasady. Dzisiaj - Koordynator Spustoszenia strain Elena. Świetny pilot małych VTOLi. Bombardowana przez Ixion oraz Esuriit, bardzo wspomagana przez visiat, wiecznie próbuje zachować osobowość ale przegrywa tę walkę. Z zawodu - neurosprzężony advancer.

### Co się rzuca w oczy

* Transorganiczna istota, zintegrowana z senti-eidolonem. Wygląda jak człowiek, ale widać transorganikę.
* Absurdalnie skupiona na rywalizacji. Praktycznie nie wycofuje się ze starcia, niezależnie od jej stanu.
* Szanuje kompetencję i umiejętności, nawet jeśli nie lubi osoby. Nie cierpi pozerstwa.
* Arogancka, nie wierzy, że ktoś może być lepszy od niej. Jeśli jest - trenuje do padnięcia. Ale wygra.
* Nie potrafi kontrolować swojego temperamentu.
* Żąda bycia docenioną i walczy o jak najlepszą karierę - oraz o to, by wszyscy działali jak najlepiej.
* Ma kiepską opinię o nie-militarnych siłach ludzkości. Jest tu dla walki.

### Jak sterować postacią

* SPEC: Każdy test zawiera 3 Or -> czy Spustoszenie nie przejmie nad nią kontroli i czy nie objawi się przez Destrukcję czy przez Asymilację / Adaptację.
* Esuriit - nieskończony głód - objawia się u niej w kierunku na Eustachego i **bycie najlepszą**, **uratowanie wszystkich** oraz **kontrolę nad sobą**. Prosta do wyzwolenia.
* Elena stała się bardzo radykalna. Nie ceni życia innych. PRÓBUJE je cenić, ale straciła tą umiejętność.
* Próbuje, bardzo próbuje być lojalna Orbiterowi, Ariannie i przyjaciołom. Bardzo próbuje.
* Próbuje... ratować. Nie do końca pamięta CZEMU oraz JAK ale próbuje. Zwłaszcza dzieci. Anastazja...
* Nie potrafi kontrolować swojego temperamentu. Nie kontroluje swoich impulsów. Zapomina. Gubi. Traci... nie pamięta.

## Mechanika

### Znaczące Czyny i Osiągnięcia (3)

* W pojedynku z Eustachym (naprawa-sabotaż) wspomagana visiatem utrzymała się naprawiając Infernię aż Persefona musiała ich odciąć.
* Jako oficer Castigatora była TERROREM arystokracji Aurum; niby jedna z nich, ale upokarzała i tępiła tworząc jakiś porządek.
* "Lot Walkirii" małą korwetką, gdy była sprowokowana przez Ariannę. Całkowicie szkodliwy lot dla statku, ale pobiła rekord tej korwety.

### Co się rzuca w oczy: Atuty, Przewagi, Zasoby (3, 6)

* SPEC: Każdy test zawiera 3 Or -> czy Spustoszenie nie przejmie nad nią kontroli i czy nie objawi się przez Destrukcję czy przez Asymilację / Adaptację.
* CECHA: Koordynator Spustoszenia. Potrafi przejąć kontrolę nad kimś / mechanizmem. Potrafi się integrować. Hive Queen.
* CECHA: Transorganiczny Terrorform. Zmiennokształtna, zdolna do asymilacji. Integrując się ze statkiem stanowi z nim jedność.
* PRACA: Wybitny pilot małych jednostek oraz servarów. Zwłaszcza teraz, przy specjalnych zdolnościach asymilacji.
* ATUT: Perfekcyjna kontrola przestrzeni: zawsze CZUJE gdzie jest i co jest dookoła niej.
* ATUT: Wspomaganie visiańskie: silne emocje (gniew, strach) i ból to uruchamiają - pilotaż, reakcje, walka. Dotyka Esuriit i Ixionu.

### Typowe problemy z którymi sobie nie radzi; Słabości (-3)

* CORE WOUND: "Tracę wszystko, na czym mi zależy. Niszczę wszystkich, których kocham. Zniszczyłam nawet siebie i nic nie zostało."
* CORE LIE: "Jestem w stanie poradzić sobie z tym wszystkim sama. Mam wszystko pod kontrolą. Nie zmieniam się bardziej."
* Łatwa do sprowokowania do działania - zwłaszcza do ratowania, stosunkowo przewidywalna.
* Absolutnie nie umie się opanować jak zaczyna działać, jest wirem destrukcji. I **musi** być najlepsza.
* SERCE: Rana związana z arystokracją i pasożytami; żąda wysokiej klasy zachowań i kompetencji.
* CIAŁO: Bardzo wrażliwa na wszystkie bronie anty-anomalne przez głęboką integrację visiańską.
* SERCE: Trywialna do sprowokowania - po prostu NIE UMIE zostawić żadnej urazy na boku.
* SERCE: Nie radzi sobie z konfliktami inaczej niż przez eskalację lub unikanie. Zwykle eskaluje.
* MAGIA: Jej energia manifestuje się instynktownie, często przez Ixion / Esuriit / Spustoszenie. Nie ma już magii dynamicznej.
* Wszystkie testy przez Eustachego mają stabilną, skuteczną kategorię wyżej. Kocha go. Chce się z nim zjednoczyć. Spustoszyć go, Ariannę...

### Serce i Wartości (3)

* Wartości
    * TAK: Tradycja (Powinność), Osiągnięcia, Samosterowność
    * NIE: Hedonizm, Pokora
    * Zawsze pragnęła być wolna, być najlepsza - wymagała od siebie i innych jak najwięcej. Działać w strukturach Orbitera, najlepszy advancer.
    * Nigdy nie rozumiała tych, którzy akceptują to co ich spotyka i tych którzy pragną najwięcej dla siebie. Zwalczała ich. WALCZ I WYGRYWAJ! Daj wszystko i WIĘCEJ!
* Ocean
    * E:0, N:+, C:-, A:0, O:0
    * Agresywna i impulsywna, mająca uczucia na dłoni i walcząca z negatywnymi uczuciami. Bardzo się wszystkim przejmuje.
    * Gorsza w planowaniu, lepsza w szybkim działaniu i reagowaniu. Wpierw działa, potem myśli.
* Silnik
    * Dorównać swojej kuzynce (Ariannie). Przekroczyć Ariannę. Znaleźć dla siebie miejsce w strukturach Orbitera.
    * Uratować i ochronić wszystkich swoich przyjaciół... i innych. Jest w stanie. By ich nie spotkało to co ją.
    * Dobrze zginąć. (DEATHWISH!)

## Inne

### Wygląd

.

### Coś Więcej

* Wiem, że ucieka od czegoś w Aurum. Nie wiem jeszcze czego. <-- przeszłość, Verlen / Blakenbauer
* Ma romans z Eustachym ;-). Tzn. nie ma. Ale są plotki. <-- nieaktualne

### Endgame

* Stanie się AI Nocnej Krypty
* Stanie się Koordynatorem Spustoszenia i będzie szerzyć mrok w galaktyce
* Stanie się Koordynatorem Spustoszenia po stronie Orbitera


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230124-kjs-wygrac-za-wszelka-cene   | 15 lat. Pomalowana na emo. W drobnym mundurze, mającym do niej pasować, z odznakami za wytrwałość i umiejętność pilotowania. Broni honoru Verlenów bijąc się z Wojtkiem (19-latkiem), ale nie ma siły fizycznej mimo techniki; wygrała przez automatyczne użycie magii. Chciała pomocy Lucasa i Zespołu by odkryć co jest dopalaczem w okolicy, ale potraktowali ją jak dziecko. Nie dała się ponieść emocjom, odeszła. Sama rozwiąże problem mimo nich. | 0095-06-23 - 0095-06-25 |
| 230412-dzwiedzie-poluja-na-ser      | (nieobecna) gdy zbliżała się do Szeptomandry, zaczęła mieć halucynacje i krzyżówki tego co było naprawdę z Szeptami. Gdyby nie Apollo, skrzywdziłaby swoich żołnierzy. Dlatego Apollo ją odesłał. | 0095-07-12 - 0095-07-14 |
| 230502-strasznolabedz-atakuje-granice | pała entuzjazmem do KAŻDEJ operacji Vioriki, też polowanie na łabędzia. Służy jako zasób 'cuteness' do przekonania Maksa. Jako jedyny mag wspiera oddział Verlenów walczący z łabędziem, ledwo sobie radzi i robi SKRAJNIE ryzykowne manewry. Ale się udało. | 0095-07-17 - 0095-07-19 |
| 230419-karolinka-nieokielznana-swinka | lat 15, superpoważna agentka specjalnej operacji 'pomścić Viorikę', pilotowała vana (aż się rozbiła o menhir; Karolinka wyssała jej energię). Wkleiła Tymka Samszara w menhir. Potem słodyczą (plan Arianny) próbowała przekonać Aleksandra Samszara do współpracy. I na końcu - Paradoks. Kłębek powagi zespołu. | 0095-07-19 - 0095-07-23 |
| 210311-studenci-u-verlenow          | 16 lat, próbowała opanować młodych, lecz nie miała autorytetu. Potem pod wpływem rezonansu z lustrem (podłożonym przez Blakenbauera i Perikasa) doszło do erupcji jej mocy i zginęły 2 osoby i 14 wił. Schowała się w jaskini; Arianna i Viorika ją wyciągały. Tak bardzo, bardzo pragnęła się wykazać i być jak Arianna. | 0096-11-18 - 0096-11-24 |
| 210324-lustrzane-odbicie-eleny      | perfekcyjna kontrola ciała, umysłu i magii dzięki wiktoriacie; ukrywa ślady pod ostrym gotyckim makijażem. Zabija i "znika" żołnierzy, by chronić cywilów. Nie straciła jednak resztek duszy. Ostatecznie - zamknięta z Arianną (przez Ariannę) na Arenie Poniewierskiej aż przybył Czapurt z oddziałem by ją unieszkodliwić. | 0097-07-05 - 0097-07-08 |
| 221004-samotna-w-programie-advancer | najmłodsza w programie Advancer Orbitera na K1; unika wszystkich by nikogo nie zranić i chce wygrywać samodzielnie. Kompetencyjnie świetna, emocjonalnie jak sprężyna. W końcu, odrzucona przez wszystkich poza Sandrą wybucha i jej moc magiczna zadaje straszne rany wszystkim obecnym aż Sandra ją zneutralizowała. | 0099-03-22 - 0099-03-31 |
| 221102-astralna-flara-i-porwanie-na-karsztarinie | klasyczny kij w tyłku, disillusioned naleśnictwem Orbitera; ma nadzieję, że z Arianną wyprowadzą tą jednostkę jak Verleńską. Chyba ma trzy tryby - uczy się, jest na akcji i śpi. Tak chciała się wykazać w grze z innymi advancerami że straciła kontrolę nad magią, ale się wybroniła przed Arianną. Stała się arogancka, nigdy nie była. Nieskończenie głodna bycia NAJLEPSZĄ. Gdy robili super groźną insercję przy wirującym statku, jej moc zintegrowała skafandry i udało im się bezpiecznie wejść. | 0100-05-30 - 0100-06-05 |
| 221130-astralna-flara-w-strefie-duchow | płynnie przesunęła Flarę by Klara ewakuowała Huberta nanowłóknem. Świetnie pilotuje. | 0100-08-07 - 0100-08-10 |
| 221221-astralna-flara-i-nowy-komodor | skonfrontowała się z Arianną - nie podoba jej się Dark Awakening Zarralei. Straciła trochę wiary w Orbitera, ale Arianna robi co może by Elena te rzeczy utrzymała. Wraz z Władawcem przeprowadziła niesamowity taniec z szablami, po czym uciekła ze wstydu. NAPRAWDĘ jest szczęśliwa podczas tańca. | 0100-09-09 - 0100-09-12 |
| 230111-gdy-hr-reedukuje-niewlasciwa-osobe | bierze się każdej możliwej misji by tylko NIE rozmawiać z Arianną odnośnie tego co się działo ostatnio ;-) | 0100-09-15 - 0100-09-18 |
| 200708-problematyczna-elena         | alias Mirokin; kompetentny pilot, automatycznie używająca swojej magii. Ma kij w tyłku i uważa zespół Inferni za niekompetentny. Wysłana na Infernię przez Leszka z nadzieją, że Arianna sobie poradzi. | 0110-10-29 - 0110-11-03 |
| 200715-sabotaz-netrahiny            | alias Mirokin; przesterowała korwetę i pobiła rekordy prędkości (za co zapłacił dowódca korwety a chwałę wzięła Arianna). Potem - wykazała się mistrzostwem sabotażu. | 0110-11-06 - 0110-11-09 |
| 200722-wielki-kosmiczny-romans      | alias Mirokin; o jej rękę ubiega się sporo adoratorów, ale ona NIE CHCE. Nie do końca kontrolując magię, emitowała Esuriit. Skończyła z reputacją w strzępach, ale powiedziała Ariannie prawdę. | 0110-11-12 - 0110-11-15 |
| 200819-sekrety-orbitera-historia-prawdziwa | alias Mirokin; wyzwała Leonę na pojedynek. Potem przez Ariannę i Klaudię ryzykowała, że skończy u Tadeusza. POTEM pocałowała Tadeusza przez Eustachego. Jest nieszczęśliwa, ale ma jednego absztyfikanta mniej. | 0110-11-26 - 0110-12-04 |
| 200909-arystokratka-w-ladowni-na-swinie | alias Mirokin; w lekkiej depresji, że będzie świnie wozić; służyła jako zastępstwo Eustachego. Niezbyt aktywna. | 0110-12-15 - 0110-12-20 |
| 200916-smierc-raju                  | alias Mirokin; przede wszystkim babysitter Anastazji. Pomagała Eustachemu zdekombinować osłony z Tucznika. | 0110-12-21 - 0110-12-23 |
| 200923-magiczna-burza-w-raju        | alias Mirokin; oswaja Anastazję; pozwoliła jej nazwać swój myśliwiec ("Gwieździsty Ptak") i powoli ją oswaja. Przyjęła smolistego gluta na siebie by chronić Anastazję. Zła na Eustachego. | 0110-12-24 - 0110-12-28 |
| 201014-krystaliczny-gniew-elizy     | alias Mirokin; gdy zabójcy próbowali zabić Anastazję, oddała sporo zdrowia i użyła pełnej mocy (łącznie z Esuriit) by ratować i osłaniać Anastazję. Prawie umarła. | 0111-01-02 - 0111-01-05 |
| 201224-nieprawdopodobny-zbieg-okolicznosci | alias Mirokin; słaba, ale chce uratować Anastazję za wszelką cenę. W walce z Grzegorzem go unieszkodliwiła, acz została bardziej ranna. | 0111-02-15 - 0111-02-16 |
| 210707-po-drugiej-stronie-bramy     | advancerka; zinfiltrowała z Raoulem opuszczoną archaiczną bazę noktiańską. Zrobiła lay of the land i napotkała Klaudiokształtną anomalię..? | 0111-03-13 - 0111-03-15 |
| 210714-baza-zona-tres               | advancuje do głębszych elementów bazy, po czym ratuje Ulissesa (noktiańskiego komputerowca) przed autosystemami zbrojowni. Wycofała bezpiecznie swoją grupę. | 0111-03-16 - 0111-03-18 |
| 201230-pulapka-z-anastazji          | alias Mirokin; pilotowała Infernię najszybciej, jak tylko potrafiła. Udało jej się nie uszkodzić ani nie zniszczyć tego pięknego statku. | 0111-05-21 - 0111-05-22 |
| 210127-porwanie-anastazji-z-odkupienia | alias Mirokin; nie zgadza się na złe traktowanie pretorian Anastazji. Nie zgadza się na skłócanie jej z Dianą przez Eustachego. Protestowała do Arianny. Odsunięta z akcji, bo zbyt paladyńska. | 0111-05-24 - 0111-05-25 |
| 210106-sos-z-haremu                 | alias Mirokin; bardzo próbowała uratować Rozalię przed zakochaniem się w Eustachym - nie udało jej się. Jest na siebie zła i zła na Eustachego. | 0111-05-28 - 0111-05-29 |
| 210120-sympozjum-zniszczenia        | alias Mirokin; próbowała rozwiązać problem Skażenia Esuriit na Bakałarzu + ostrzegła innych; nie tylko jej się nie udało skoordynować Zespołu, co jeszcze spadło na nią że to ona winna. | 0111-06-03 - 0111-06-07 |
| 210108-ratunkowa-misja-goldariona   | alias Mirokin; jak już przebiła się przez niesprawne Eidolony, pokazała co potrafi jako advancer - zinfiltrowała "Uśmiechniętą", zebrała informacje i zniszczyła Skażoną Bię. | 0111-06-11 - 0111-06-17 |
| 210210-milosc-w-rodzie-verlen       | alias Mirokin; okazuje się, że jest lubiana w Verlen, czego nie umie zasymilować. Połączyła się z sentisiecią, wyzwała Romeo na pojedynek - i wygrała. By nie przejęło jej Esuriit, "wyłączyła" się. Cierpi na wieczny kompleks "ja siama". | 0111-06-23 - 0111-06-26 |
| 210331-elena-z-rodu-verlen          | skonfrontowała się ze swoją przeszłością i czynami vs Blakenbauer. Też z miłością Romea do niej. Po dużej ilości łez i działań Arianny, przeprosiła Blakenbauerów (ze wzajemnością) i wróciła do rodu. | 0111-06-29 - 0111-07-02 |
| 210218-infernia-jako-goldarion      | JUŻ NIE MIROKIN; chciała się zaopiekować biednym Sowińskim by ten nie cierpiał od Leony (wbrew sobie), jednak skończyła w szoku i medbay po integracji z dziwnym Entropikiem. | 0111-07-19 - 0111-08-03 |
| 210317-arianna-podbija-asimear      | mistrzowsko sterowała pinasą na Infernię, uciekając od tajemniczych jednostek na Asimear. | 0111-08-20 - 0111-09-04 |
| 210414-dekralotyzacja-asimear       | opieprzyła Eustachego że w ramach działań na Asimear nie optymalizował przeżywalności ludzi. Potem infiltruje Płetwala, przejęła Entropika i wdała się w "taniec" z Tirakalem. Ten grożąc ludziom wymanewrował ją i skończyła w kosmosie - ale podpięła Tirakal do Entropika dając czysty strzał Malictrix. | 0111-09-05 - 0111-09-14 |
| 210421-znudzona-zaloga-inferni      | zaplanowała konfrontację z Eustachym by powiedział jej czy mu na niej zależy. Doprowadziła do tego. Gdy ona chciała on nie chciał. Gdy on chciał to zorientowała się że to niegodne i uciekła XD. | 0111-09-18 - 0111-09-21 |
| 210428-infekcja-serenit             | zażenowana "Piękną Eleną" Tadeusza Ursusa; robi kosmiczny spacer na Falołamacz i ratuje Klaudię przed złapaniem przez Serenit (mistrzyni manewrów i uników). | 0111-09-24 - 0111-09-25 |
| 210512-ewakuacja-z-serenit          | połączona z Eidolonem i sympatią z Entropikiem zdalnie poruszała Entropikiem na Falołamaczu przez sympatię, odpierając ataki Serenita na swój umysł. Uratowała wielu, acz zapłaciła szpitalem. | 0111-09-25 - 0111-10-04 |
| 210519-osiemnascie-oczu             | wyszła z pozycji advancera by ostrzec zespół; jej omnidetekcja jednak sprawiła, że od razu wpadła pod infohazard. Jako infektant chroniła life support, próbując Skazić Eustachego. | 0111-10-09 - 0111-10-20 |
| 210526-morderstwo-na-inferni        | Eustachy poprosił ją by znalazła Tala Marczaka i przygotowała wnioski formalne. Elena rozmawiając z Klaudią i Arianną próbowała wyprostować to szaleństwo - PO CO TO WSZYSTKO. | 0111-10-26 - 0111-11-01 |
| 210630-listy-od-fanow               | dokonała głębokiej infiltracji nowym Eidolonem bazy Eterni na K1, rozpracowała jej układ, podpięła tamte TAI do K1 (dla Rziezy) i została ranna uciekając przez Żelazko. | 0111-11-10 - 0111-11-13 |
| 210616-nieudana-infiltracja-inferni | dawno, dawno temu zrobiła KATASTROFALNĄ krzywdę Flawii, wpakowała ją w długi i doprowadziła do jej dewastacji. | 0111-11-22 - 0111-11-27 |
| 210825-uszkodzona-brama-eteryczna   | widząc "Dianę" "napastującą" Eustachego, zaatakowała ją by chronić Eustachego. OCZYWIŚCIE tylko dlatego i wcale się nie przejmowała czy coś Eustachemu nie jest lub nie robi. | 0111-11-30 - 0111-12-03 |
| 210901-stabilizacja-bramy-eterycznej | wykazała się mistrzowskim pilotażem ściągając echo krążownika Perforator, po czym pobierając Ariannę spod Bramy i wprowadzając ją na ów Perforator. Ale potem Skażona Esuriit krzyczała, że Eustachemu trzeba założyć pas cnoty i padła do Leony, acz ją poturbowała. CO ZA WSTYD (ten pas...) | 0111-12-05 - 0111-12-08 |
| 210922-ostatnia-akcja-bohaterki     | MVP manewrów. Wprowadziła Ptakiem ekipę do Krwawej Bazy Piratów, odciągała ogień i ostrzeliwała cele W TEJ BAZIE. | 0111-12-19 - 0112-01-03 |
| 210929-grupa-ekspedycyjna-kellert   | zinfiltrowała Skażoną OO Savera. Dała się Zobaczyć Vigilusowi, ale skutecznie odkryła położenie wszystkiego na Saverze i zdołała uciec zanim została rozsiekana przez krwawe "bóstwo". | 0112-01-07 - 0112-01-10 |
| 211013-szara-nawalnica              | wraz z komandosami Verlenów przechwytywała lokalsów ze śmieciostateczku w Kalarfam na Infernię. Niewielka rola. | 0112-01-12 - 0112-01-17 |
| 211020-kurczakownia                 | akcja infiltracyjna Kultu Esuriit z Flawią. Udało jej się znaleźć gdzie są przetrzymywani Orbiterowcy, acz wpadła w kłopoty. W zamieszaniu wywołanym pojawieniem się Arianny udało jej i Flawii się uciec. | 0112-01-18 - 0112-01-20 |
| 211027-rzieza-niszczy-infernie      | obudzona rozpaczliwie przez Ariannę. Zdecentrowana. Próbowała wszystkich uratować i wlała Esuriit do laboratorium (eksplozja Paradoksu). Z trudem powstrzymała manifestację Zbawiciela-Niszczyciela. Potężnie Skażona i przekształcona przez Esuriit; resztę operacji dobierała się do śpiącego Eustachego. NIE MOŻE TEGO SOBIE WYBACZYĆ. | 0112-01-22 - 0112-01-27 |
| 211110-romans-dzieki-esuriit        | złamana, nie może się pogodzić z tym co zrobiła (Esuriit) i co się z nią stało (Esuriit). Ucieka do Sektora 49. Arianna organizuje grę wojenną by ją znaleźć. W końcu Elena przyznaje Eustachemu, że go kocha. Ale nie usłyszała tego z powrotem, więc wypłakuje oczy, że Eustachy wybrał LEONĘ nad nią. | 0112-02-07 - 0112-02-08 |
| 211117-porwany-trismegistos         | Arianna jej zaufała, by ta pilotowała Tivr. I faktycznie, najszybszy pilot. Plus, spacer kosmiczny w Eidolonie pozwolił jej podpiąć Klaudię do Trismegistosa. | 0112-02-09 - 0112-02-11 |
| 211124-prototypowa-nereida-natalii  | nie umie dojść do siebie przez sytuację z Eustachym, ale jak trzeba było ratować Natalię z Nereidy, nie wahała się ani przez sekundę. Sama w kosmosie w Eidolonie czekała aż Eustachy ściągnię na nią Nereidę. Jednak nie ściągnął, więc siedziała sama w kosmosie i kontemplowała życie. | 0112-02-14 - 0112-02-18 |
| 211208-o-krok-za-daleko             | gdy Arianny moc wychodzi poza kontrolę, gdy wpływa na Eustachego - próbowała powstrzymać Paradoks Arianny. Potężne Skażenie ixiońskie. Nie jest już czarodziejką. W rozpaczy próbowała zatrzymać Eustachego przed pójściem z "Dianą" - ale Eustachy wybrał Infernię nad nią. Ma złamane serce, Infernia jej nie lubi i ogólnie nie wie co robić. | 0112-02-19 - 0112-02-20 |
| 211215-sklejanie-inferni-do-kupy    | chciała opuścić Infernię po tym wszystkim; Arianna przekonała ją, że jak będzie dowodzić Tivrem to pomoże Orbiterowi. Poważnie Skażona Ixionem i Esuriit; nie ma statusu już czarodziejki. Nie ma dla niej powrotu do domu. | 0112-02-21 - 0112-02-23 |
| 211222-kult-saitaera-w-neotik       | uziemiona przez Klaudię i Ariannę w Inferni zinfiltrowała Stocznię Neotik i odkryła Kult Saitaera oraz niższe Spustoszenie. | 0112-02-24 - 0112-02-25 |
| 220105-to-nie-pulapka-na-nereide    | w kryzysowej i krytycznej sytuacji przekierowywała energie w lewą i prawą pomagając Izie, Klaudii i Kamilowi. Jej wzór uległ uszkodzeniu w sposób nieosiągalny. Poświęciła się dla Inferni i przyjaciół. ŻYJE ALE. | 0112-02-28 - 0112-03-04 |
| 220126-keldan-voss-kolonia-saitaera | przeszła przez przyspieszone testy psychologiczne, ale nie jest z nią w porządku. | 0112-03-19 - 0112-03-22 |
| 220202-sekrety-keldan-voss          | Osłania Eustachego. Nie reaguje na phase shift. Zestrzeliła serię dron górniczych skuteczniej niż kiedykolwiek. Widzi niestety też rzeczy których nie ma... albo jest tam więcej niż widać. | 0112-03-24 - 0112-03-26 |
| 220216-polityka-rujnuje-pallide-voss | destabilizuje się; chce NISZCZYĆ dla dywersji (Klaudia ją zatrzymała). Głównie działała jako prom transportujący na lewo i prawo, acz energia ją trochę nosi. | 0112-03-27 - 0112-03-29 |
| 220223-stabilizacja-keldan-voss     | wykryła atak Mgły na Eustachego; Klaudia przez nią (omni) przepuściła sygnał dla wyczyszczenia. Usłyszała WSZYSTKO. Szepty Saitaera. Zinfiltrowała BIA, nie wyszło jej wprowadzenie BIA, ale zintegrowała szamana z BIA. Coraz bardziej ma cechy terrorforma a nie maga. | 0112-03-30 - 0112-04-02 |
| 220309-upadek-eleny                 | dla Eustachego, dla piękna, dla przyszłości pożarła kultystów. Ale porwała Kroczącego we Mgle, wbijając mu neurokontrolę ixiońską palcem w układ nerwowy (creepy as hell). | 0112-04-03 - 0112-04-09 |
| 220316-potwor-czy-choroba-na-etaur  | pyta Eustachego, kiedy ten zamierza jej się oświadczyć. Jej szaleństwo rośnie. Wyłączona, wsadzona do biovatu na regenerację. | 0112-04-24 - 0112-04-26 |
| 220610-ratujemy-porywaczy-eleny     | gdy ratowała agenta Inferni, Spustoszenie zniszczyło jej pamięć. Pokonała (nie zabijając) sporo agentów, zanim Arianna do niej dotarła. W Kranix ją zregenerowali i zrobili jej containment chamber. Śpi. Porwana przez Syndykat Aureliona, Inferni udało się ją odbić zanim się przebudziła. | 0112-07-11 - 0112-07-13 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 230412-dzwiedzie-poluja-na-ser      | okazuje się wyjątkowo podatna na Szeptomandrę i ataki tego typu | 0095-07-14
| 230419-karolinka-nieokielznana-swinka | po raz pierwszy uruchomiła jej się moc Esuriit gdy interaktowała ze Strażnikiem / Hybrydą. | 0095-07-23
| 210311-studenci-u-verlenow          | powszechnie uważana w Verlenlandzie za niebezpieczną. Verlenka, która straciła kontrolę. Elena - przecież przyjazne stworzenie - się całkowicie oddaliła od "przeciętnego człowieka". Znienawidzona w Poniewierzy. | 0096-11-24
| 210324-lustrzane-odbicie-eleny      | narkotyki i przepuszczanie swoją energię przez lustra ZNISZCZYŁO jej Wzór. Co najmniej pół roku naprawy. | 0097-07-08
| 210324-lustrzane-odbicie-eleny      | stała się (tymczasowo) wampirem. Zabiera życie żołnierzy; zabiła 20 osób, które jej zaufały i były po jej stronie... jest uznana za POTWORA. | 0097-07-08
| 210324-lustrzane-odbicie-eleny      | STRASZNE uprzedzenie do rodu Blakenbauerów, Blakenbauerów, każdego Blakenbauera, narzędzi Blakenbauerów i rzeczy na literę "B". PLUS arystokracji Aurum. | 0097-07-08
| 221004-samotna-w-programie-advancer | przeniesiona z programu 'advancer' do innego programu po utracie kontroli i zadaniu ciężkich ran i NAGANA DO AKT. | 0099-03-31
| 221004-samotna-w-programie-advancer | złożyła pierwszy raz prośbę o neurosprzężenie | 0099-03-31
| 221004-samotna-w-programie-advancer | opinia niebezpiecznej i niestabilnej, ale też o bardzo dużym potencjale | 0099-03-31
| 221102-astralna-flara-i-porwanie-na-karsztarinie | ma w papierach, że jest świetnym pilotem i advancerem, ale nikt nie chce z nią pracować. Ma nagany i przesunięcia. Chwalona jak działa solo. Else - nagany. | 0100-06-05
| 221102-astralna-flara-i-porwanie-na-karsztarinie | wchodząc w warp Astralną Flarą zrobiła to szybciej i skuteczniej niż inni. Już zaczyna mieć opinię 'superstar' na Flarze. | 0100-06-05
| 221116-astralna-flara-dociera-do-nonariona-nadziei | trochę się otwiera na Władawca Diakona - on jest dobry w zapasach i ona MUSI go pokonać! | 0100-07-14
| 221221-astralna-flara-i-nowy-komodor | nadal uważa, że Arianna wie lepiej i zrobiła co powinna, ale wie już że Arianna inaczej postrzega to co ważne niż ona - dla Eleny liczy się 'działanie prawidłowo', dla Arianny 'jak najwięcej żyć które przetrwają'. Zaczyna się rift między nimi. Ale jest uspokojona przez Ariannę odnośnie Orbitera i decyzji Orbitera. | 0100-09-12
| 200715-sabotaz-netrahiny            | nie tylko jest Mistrzynią Wkurzania Eustachego ale i wygrała z nim - wycofał się z pojedynku i ją przeprosił. Morale do góry. | 0110-11-09
| 200715-sabotaz-netrahiny            | dzięki Eustachemu święcie przekonana, że Arianna jest pozerem; bierze jej chwałę i wszystkich. Zawiść wobec Arianny. | 0110-11-09
| 200722-wielki-kosmiczny-romans      | opinia "tej, która nie kontroluje swojej magii" i "podrywa na lewo i prawo - ale to nie jej wina, jej magia tak działa". Nie jest brana poważnie. Jej opinia - w RUINIE. | 0110-11-15
| 200722-wielki-kosmiczny-romans      | potrafi emitować Esuriit; nie ma pełnej kontroli nad magią, ale jest w stanie magię świetnie instynktownie używać. Acz nie zatrzyma się pod wpływem emocji. | 0110-11-15
| 200722-wielki-kosmiczny-romans      | dostała Iquitas od Damiana Oriona do przetestowania. | 0110-11-15
| 200722-wielki-kosmiczny-romans      | Eustachy jej pomógł, odplątał jej z krwawego pojedynku arystokratycznego przeciw Konradowi Wolczątkowi. Nie jest taki zły O_O. | 0110-11-15
| 200819-sekrety-orbitera-historia-prawdziwa | Tadeusz Ursus się od niej odczepi. Leona wygrała z Tadeuszem. Dzięki Eustachemu i Ariannie Elena ma jeden (poważny) problem z głowy. | 0110-12-04
| 200819-sekrety-orbitera-historia-prawdziwa | jest święcie przekonana, że Eustachy Korkoran jest w niej zakochany - tylko dlatego jego działania takie jakie są mają sens. | 0110-12-04
| 200819-sekrety-orbitera-historia-prawdziwa | jej reputacja jest na stałe - to podfruwajka; raz całuje a raz strzela. Gorrrąca. Potwierdzone przez całowanie Tadeusza przed walką. | 0110-12-04
| 200819-sekrety-orbitera-historia-prawdziwa | ma NISKĄ opinię o Ariannie. Nie wie, ile Arianna zrobiła by ją z tego wyciągnąć. Myśli że wszystko Eustachy XD. | 0110-12-04
| 200923-magiczna-burza-w-raju        | wzięła na siebie obowiązek dbania o Anastazję; w końcu to jej przeszła wersja w pewien sposób. To teraz jej 'klucz', najważniejsza rzecz. Wychować młodą. | 0110-12-28
| 201014-krystaliczny-gniew-elizy     | bardzo ciężko ranna, przepalona i bez cienia kontroli energii. Co najmniej miesiąc bardzo ograniczonej aktywności. | 0111-01-05
| 201224-nieprawdopodobny-zbieg-okolicznosci | osobisty wróg w Grzegorzu Chropście - skrzywdziła i pobiła łapczaka (który zaatakował ją pierwszy). | 0111-02-16
| 210127-porwanie-anastazji-z-odkupienia | WIE, że Eustachy próbował skłócić ją i Dianę. Nie wie, czemu. Też zachowanie Arianny i Klaudii wobec Pretorian Anastazji ją zdrażniło. Ogólnie, silny cios w relacje z dowództwem Inferni. | 0111-05-25
| 210106-sos-z-haremu                 | STRASZNIE rozczarowana Eustachym, który rozkochał w sobie biedną Rozalię, która mu nie zawiniła i nigdy nie może go mieć... | 0111-05-29
| 210120-sympozjum-zniszczenia        | wszystkie niepowodzenia z Esuriit i z opanowaniem tego przez Zespół spadły na nią, mimo, że nie ma z tym nic wspólnego. Duży cios reputacyjny; sama z tego już nie wyjdzie. | 0111-06-07
| 210120-sympozjum-zniszczenia        | bardzo, bardzo rozczarowana Eustachym i jego podejściem do Diany. Nie zatrzymał jej. Nie opanował jej. I wszystko spada na Elenę. A mógł zadziałać. | 0111-06-07
| 210120-sympozjum-zniszczenia        | reputacja "świetni w niszczeniu i destrukcji, ale idźcie na bok i nie psujcie więcej". Takie trochę dzieci od zniszczenia. | 0111-06-07
| 210108-ratunkowa-misja-goldariona   | dostaje dostęp do trzech średniej klasy Eidolonów należących do ArcheoPrzędz. Zrobiła też na cywilach z ArcheoPrzędz duże wrażenie. | 0111-06-17
| 210210-milosc-w-rodzie-verlen       | wyszło na jaw, że została pilotem by dokuczyć Romeo i że kiedyś miała za najlepszą przyjaciółkę TAI. Bardzo dziecinna była jak była mała. Nie dała się opętać Esuriit, odcinając układ nerwowy. | 0111-06-26
| 210210-milosc-w-rodzie-verlen       | jest akceptowana taka jaka jest w rodzie Verlen i ma opcję powrotu - jeśli przeprosi (Blakenbauerów, ze wszystkich rodów). Co sprawia, że jest bardzo skonfliktowana. | 0111-06-26
| 210210-milosc-w-rodzie-verlen       | potrafi się połączyć z sentisiecią Verlen i jest w tym mocniejsza niż się wydaje. Ale nie akceptuje rodu i siebie, więc połączenie jest słabsze. Potencjalnie najmocniejszy link z sentisiecią. | 0111-06-26
| 210331-elena-z-rodu-verlen          | W PRZESZŁOŚCI zniszczyła skutecznie reputację Blakenbauerów w Orbiterze. Nie mają wstępu na orbitę. | 0111-07-02
| 210331-elena-z-rodu-verlen          | Blakenbauerowie W PRZESZŁOŚCI zapłacili ogromne sumy by niszczyć reputację Eleny na Orbiterze. To sprawia, że wszystko co złe automatycznie przylepia się do niej. | 0111-07-02
| 210331-elena-z-rodu-verlen          | dotarło do niej co zrobiła. Trauma, bo skrzywdziła ogromną ilość niewinnych Blakenbauerów. Ale nie chce się już mścić... za to potrzebuje WIĘCEJ DYSCYPLINY! | 0111-07-02
| 210512-ewakuacja-z-serenit          | po neurosprzężeniu i sentisprzężeniu i Dotyku Serenit skończyła na dwa tygodnie w szpitalu na regenerację i rekalibrację Wzoru. | 0111-10-04
| 210512-ewakuacja-z-serenit          | Serenit wie o jej istnieniu. Jeśli będzie gdzieś blisko, może na nią zapolować. | 0111-10-04
| 210512-ewakuacja-z-serenit          | traci dostęp do sentisprzężonego Eidolona - ów wymaga naprawy ("regeneracji?") w Verlenlandzie. 2 miesiące od teraz. | 0111-10-04
| 210519-osiemnascie-oczu             | wysokie uznanie ze strony Mariana Tosena z grupy antymemetyczej Orbitera. | 0111-10-20
| 210630-listy-od-fanow               | dostała eternijski Eidolon z eternijskim logiem na stałe, w zamian za promocję Eterni. Przynajmniej ten Eidolon jest w dobrym stanie. | 0111-11-13
| 210630-listy-od-fanow               | 3 dni regeneracji i naprawy Eidolona po akcji na K1. Bycie w szpitalu zaczyna być dla niej irytujące. | 0111-11-13
| 210616-nieudana-infiltracja-inferni | uważa, że Eustachy flirtuje z KAŻDYM. A zwłaszcza z Flawią >.>. | 0111-11-27
| 210616-nieudana-infiltracja-inferni | straszne poczucie winy za to co zrobiła kiedyś Flawii. Ale nie jest w stanie nic z tym zrobić. | 0111-11-27
| 211020-kurczakownia                 | TRAUMA. Kurczakowanie - rekurczakowanie. Dla niej to jest... straszne. | 0112-01-20
| 211027-rzieza-niszczy-infernie      | straszna rekonstrukcja Esuriit. Libido++. Obsesja w stronę Eustachego. Elena nie czuje, że kontroluje swoje ciało i umysł. Przebudowa Paradoksu na Eustachego. | 0112-01-27
| 211110-romans-dzieki-esuriit        | załamana i zrozpaczona. Eustachy wybrał LEONĘ nad nią. Ze wszystkich osób. LEONĘ! | 0112-02-08
| 211110-romans-dzieki-esuriit        | jej zdaniem byłoby lepiej, gdyby NIE ISTNIAŁA. Zabiła tyle niewinnych osób. Esuriit ją pożera. Chce odejść w noc... ale nie może i nie chce. Eustachy lub śmierć. | 0112-02-08
| 211117-porwany-trismegistos         | zaimponowała noktianom na pokładzie Tivra, że świetnie pilotuje tą jednostkę. Z ogromnym tempem i wydajnością. | 0112-02-11
| 211208-o-krok-za-daleko             | próbowała pochłonąć Eksplozję Paradoksu Arianny; napromieniowana ixionem. Staje się viciniusem. NOWA KARTA POSTACI, pełna rekonstrukcja. | 0112-02-20
| 211208-o-krok-za-daleko             | rana mentalna od Arianny (she didn't give a meow) i od Eustachego (odrzucił i wzgardził; wybrał Infernię, ranił ludzi). A ona dla nich wszystko. | 0112-02-20
| 211208-o-krok-za-daleko             | Infernia ją zwalcza, nie chce jej na pokładzie. Ixion i Esuriit... | 0112-02-20
| 211215-sklejanie-inferni-do-kupy    | NOWA KARTA POSTACI. Skażenie Ixionem i Esuriit. Zmiana wyglądu. Nie ma dla niej powrotu do Verlenlandu. Nie jest już czarodziejką. | 0112-02-23
| 220105-to-nie-pulapka-na-nereide    | zniszczony Wzór, pomiędzy ixionem i esuriit. Dwa potężne ryczące oceany a pomiędzy nimi jedna mała osobowość Eleny. | 0112-03-04
| 220105-to-nie-pulapka-na-nereide    | perfekcyjny pilot Nereidy. Perfekcyjna anomalna integracja z pojazdami. Jej eidolon i ona stanowią jedność. | 0112-03-04
| 220223-stabilizacja-keldan-voss     | obsesja na punkcie swojej urody; słyszy szepty Keldan Voss. Szepty Saitaera? Szepty Mgieł? Nawet ona nie wie. | 0112-04-02
| 220309-upadek-eleny                 | słowami Mateusa - "w imieniu Saitaera POŚWIĘCASZ. Ona nie POŚWIĘCA. Ona ZABIERA. Zabiera przeznaczenie innych by wzmacniać swoje". Odzyskała swoje piękno zabierając 3 życia. | 0112-04-09
| 220316-potwor-czy-choroba-na-etaur  | contained. Znajduje się w specjalnym biovacie gdzie jest regenerowana i reanimowana do formy 'kontrolowanego viciniusa'. | 0112-04-26
| 220610-ratujemy-porywaczy-eleny     | jej pierwsze wystąpienie jako Spustoszenie Strain Elena. | 0112-07-13

## Plany


| Opowieść | Plan | Końcowa data |
| ---- | ---- | ---- |
| 210311-studenci-u-verlenow          | ma krew na rękach - zabiła 2 ludzi. Jest wstrząśnięta. Zrobi WSZYSTKO by kontrolować swoją energię magiczną i moc; nie chce być potworem. Nie chce zabijać. Nie chce być bronią w cudzych rękach. | 0096-11-24
| 210311-studenci-u-verlenow          | MUSI pomścić to, co jej zrobił Dariusz Blakenbauer i młodzi magowie rodu Perikas. Dla swojej chorej chuci młodzi są odpowiedzialni za śmierć ludzi. | 0096-11-24

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 55 | ((200708-problematyczna-elena; 200715-sabotaz-netrahiny; 200722-wielki-kosmiczny-romans; 200819-sekrety-orbitera-historia-prawdziwa; 200909-arystokratka-w-ladowni-na-swinie; 200916-smierc-raju; 200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy; 201230-pulapka-z-anastazji; 210106-sos-z-haremu; 210120-sympozjum-zniszczenia; 210127-porwanie-anastazji-z-odkupienia; 210210-milosc-w-rodzie-verlen; 210218-infernia-jako-goldarion; 210311-studenci-u-verlenow; 210317-arianna-podbija-asimear; 210324-lustrzane-odbicie-eleny; 210331-elena-z-rodu-verlen; 210414-dekralotyzacja-asimear; 210421-znudzona-zaloga-inferni; 210428-infekcja-serenit; 210512-ewakuacja-z-serenit; 210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210616-nieudana-infiltracja-inferni; 210630-listy-od-fanow; 210707-po-drugiej-stronie-bramy; 210714-baza-zona-tres; 210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej; 210922-ostatnia-akcja-bohaterki; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica; 211020-kurczakownia; 211027-rzieza-niszczy-infernie; 211110-romans-dzieki-esuriit; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide; 220126-keldan-voss-kolonia-saitaera; 220202-sekrety-keldan-voss; 220216-polityka-rujnuje-pallide-voss; 220223-stabilizacja-keldan-voss; 220309-upadek-eleny; 220316-potwor-czy-choroba-na-etaur; 220610-ratujemy-porywaczy-eleny; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221130-astralna-flara-w-strefie-duchow; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe; 230412-dzwiedzie-poluja-na-ser; 230419-karolinka-nieokielznana-swinka)) |
| Klaudia Stryk        | 44 | ((200708-problematyczna-elena; 200715-sabotaz-netrahiny; 200819-sekrety-orbitera-historia-prawdziwa; 200909-arystokratka-w-ladowni-na-swinie; 200916-smierc-raju; 200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy; 201224-nieprawdopodobny-zbieg-okolicznosci; 201230-pulapka-z-anastazji; 210106-sos-z-haremu; 210108-ratunkowa-misja-goldariona; 210120-sympozjum-zniszczenia; 210127-porwanie-anastazji-z-odkupienia; 210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear; 210421-znudzona-zaloga-inferni; 210428-infekcja-serenit; 210512-ewakuacja-z-serenit; 210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210616-nieudana-infiltracja-inferni; 210630-listy-od-fanow; 210707-po-drugiej-stronie-bramy; 210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej; 210922-ostatnia-akcja-bohaterki; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica; 211020-kurczakownia; 211027-rzieza-niszczy-infernie; 211110-romans-dzieki-esuriit; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide; 220126-keldan-voss-kolonia-saitaera; 220202-sekrety-keldan-voss; 220216-polityka-rujnuje-pallide-voss; 220223-stabilizacja-keldan-voss; 220309-upadek-eleny; 220316-potwor-czy-choroba-na-etaur; 220610-ratujemy-porywaczy-eleny)) |
| Eustachy Korkoran    | 42 | ((200708-problematyczna-elena; 200715-sabotaz-netrahiny; 200722-wielki-kosmiczny-romans; 200819-sekrety-orbitera-historia-prawdziwa; 200909-arystokratka-w-ladowni-na-swinie; 200916-smierc-raju; 200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy; 201230-pulapka-z-anastazji; 210106-sos-z-haremu; 210120-sympozjum-zniszczenia; 210127-porwanie-anastazji-z-odkupienia; 210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear; 210421-znudzona-zaloga-inferni; 210428-infekcja-serenit; 210512-ewakuacja-z-serenit; 210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210616-nieudana-infiltracja-inferni; 210707-po-drugiej-stronie-bramy; 210714-baza-zona-tres; 210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica; 211020-kurczakownia; 211110-romans-dzieki-esuriit; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide; 220126-keldan-voss-kolonia-saitaera; 220202-sekrety-keldan-voss; 220216-polityka-rujnuje-pallide-voss; 220223-stabilizacja-keldan-voss; 220309-upadek-eleny; 220316-potwor-czy-choroba-na-etaur; 220610-ratujemy-porywaczy-eleny)) |
| Martyn Hiwasser      | 24 | ((200819-sekrety-orbitera-historia-prawdziwa; 200909-arystokratka-w-ladowni-na-swinie; 201230-pulapka-z-anastazji; 210106-sos-z-haremu; 210108-ratunkowa-misja-goldariona; 210120-sympozjum-zniszczenia; 210127-porwanie-anastazji-z-odkupienia; 210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear; 210421-znudzona-zaloga-inferni; 210428-infekcja-serenit; 210512-ewakuacja-z-serenit; 210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210707-po-drugiej-stronie-bramy; 210714-baza-zona-tres; 210901-stabilizacja-bramy-eterycznej; 210922-ostatnia-akcja-bohaterki; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica; 211020-kurczakownia; 211027-rzieza-niszczy-infernie; 220610-ratujemy-porywaczy-eleny)) |
| Leona Astrienko      | 18 | ((200722-wielki-kosmiczny-romans; 200819-sekrety-orbitera-historia-prawdziwa; 210106-sos-z-haremu; 210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210421-znudzona-zaloga-inferni; 210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210616-nieudana-infiltracja-inferni; 210901-stabilizacja-bramy-eterycznej; 210922-ostatnia-akcja-bohaterki; 210929-grupa-ekspedycyjna-kellert; 211110-romans-dzieki-esuriit; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy; 220610-ratujemy-porywaczy-eleny)) |
| Izabela Zarantel     | 11 | ((200819-sekrety-orbitera-historia-prawdziwa; 200909-arystokratka-w-ladowni-na-swinie; 200916-smierc-raju; 200923-magiczna-burza-w-raju; 210616-nieudana-infiltracja-inferni; 210630-listy-od-fanow; 210922-ostatnia-akcja-bohaterki; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide)) |
| OO Infernia          | 11 | ((201230-pulapka-z-anastazji; 210127-porwanie-anastazji-z-odkupienia; 210218-infernia-jako-goldarion; 210428-infekcja-serenit; 210512-ewakuacja-z-serenit; 210825-uszkodzona-brama-eteryczna; 210929-grupa-ekspedycyjna-kellert; 211020-kurczakownia; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy)) |
| Maria Naavas         | 8 | ((210120-sympozjum-zniszczenia; 211110-romans-dzieki-esuriit; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide; 220316-potwor-czy-choroba-na-etaur)) |
| Kamil Lyraczek       | 7 | ((200909-arystokratka-w-ladowni-na-swinie; 210414-dekralotyzacja-asimear; 210526-morderstwo-na-inferni; 210616-nieudana-infiltracja-inferni; 211020-kurczakownia; 211215-sklejanie-inferni-do-kupy; 220105-to-nie-pulapka-na-nereide)) |
| Viorika Verlen       | 7 | ((210210-milosc-w-rodzie-verlen; 210311-studenci-u-verlenow; 210324-lustrzane-odbicie-eleny; 210331-elena-z-rodu-verlen; 230412-dzwiedzie-poluja-na-ser; 230419-karolinka-nieokielznana-swinka; 230502-strasznolabedz-atakuje-granice)) |
| Antoni Kramer        | 6 | ((200708-problematyczna-elena; 200722-wielki-kosmiczny-romans; 210218-infernia-jako-goldarion; 210526-morderstwo-na-inferni; 210922-ostatnia-akcja-bohaterki; 210929-grupa-ekspedycyjna-kellert)) |
| Otto Azgorn          | 6 | ((210421-znudzona-zaloga-inferni; 210526-morderstwo-na-inferni; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica; 211020-kurczakownia; 211027-rzieza-niszczy-infernie)) |
| Roland Sowiński      | 6 | ((210512-ewakuacja-z-serenit; 210616-nieudana-infiltracja-inferni; 210922-ostatnia-akcja-bohaterki; 211117-porwany-trismegistos; 211124-prototypowa-nereida-natalii; 211215-sklejanie-inferni-do-kupy)) |
| Annika Pradis        | 5 | ((220126-keldan-voss-kolonia-saitaera; 220202-sekrety-keldan-voss; 220216-polityka-rujnuje-pallide-voss; 220223-stabilizacja-keldan-voss; 220309-upadek-eleny)) |
| Apollo Verlen        | 5 | ((210210-milosc-w-rodzie-verlen; 210311-studenci-u-verlenow; 230124-kjs-wygrac-za-wszelka-cene; 230412-dzwiedzie-poluja-na-ser; 230419-karolinka-nieokielznana-swinka)) |
| Flawia Blakenbauer   | 5 | ((210616-nieudana-infiltracja-inferni; 210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej; 211020-kurczakownia; 211027-rzieza-niszczy-infernie)) |
| Maja Samszar         | 5 | ((210311-studenci-u-verlenow; 221102-astralna-flara-i-porwanie-na-karsztarinie; 221130-astralna-flara-w-strefie-duchow; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Olgierd Drongon      | 5 | ((200708-problematyczna-elena; 200722-wielki-kosmiczny-romans; 210120-sympozjum-zniszczenia; 210630-listy-od-fanow; 210922-ostatnia-akcja-bohaterki)) |
| Adam Szarjan         | 4 | ((211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik)) |
| Anastazja Sowińska   | 4 | ((200909-arystokratka-w-ladowni-na-swinie; 200916-smierc-raju; 200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy)) |
| Daria Czarnewik      | 4 | ((221102-astralna-flara-i-porwanie-na-karsztarinie; 221130-astralna-flara-w-strefie-duchow; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Diana Arłacz         | 4 | ((201224-nieprawdopodobny-zbieg-okolicznosci; 210106-sos-z-haremu; 210120-sympozjum-zniszczenia; 210127-porwanie-anastazji-z-odkupienia)) |
| Diana d'Infernia     | 4 | ((210825-uszkodzona-brama-eteryczna; 211208-o-krok-za-daleko; 211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik)) |
| Kajetan Kircznik     | 4 | ((221102-astralna-flara-i-porwanie-na-karsztarinie; 221130-astralna-flara-w-strefie-duchow; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Leszek Kurzmin       | 4 | ((200708-problematyczna-elena; 200819-sekrety-orbitera-historia-prawdziwa; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Tomasz Sowiński      | 4 | ((210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear; 210421-znudzona-zaloga-inferni)) |
| Aleksandra Termia    | 3 | ((201014-krystaliczny-gniew-elizy; 210929-grupa-ekspedycyjna-kellert; 211013-szara-nawalnica)) |
| Damian Orion         | 3 | ((200722-wielki-kosmiczny-romans; 200819-sekrety-orbitera-historia-prawdziwa; 201230-pulapka-z-anastazji)) |
| Dariusz Krantak      | 3 | ((200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy; 210127-porwanie-anastazji-z-odkupienia)) |
| Janus Krzak          | 3 | ((210707-po-drugiej-stronie-bramy; 210714-baza-zona-tres; 210901-stabilizacja-bramy-eterycznej)) |
| Jolanta Sowińska     | 3 | ((210218-infernia-jako-goldarion; 210317-arianna-podbija-asimear; 210421-znudzona-zaloga-inferni)) |
| Marcinozaur Verlen   | 3 | ((230412-dzwiedzie-poluja-na-ser; 230419-karolinka-nieokielznana-swinka; 230502-strasznolabedz-atakuje-granice)) |
| Marian Fartel        | 3 | ((200916-smierc-raju; 200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy)) |
| Marian Tosen         | 3 | ((210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 210616-nieudana-infiltracja-inferni)) |
| Mateus Sarpon        | 3 | ((220126-keldan-voss-kolonia-saitaera; 220202-sekrety-keldan-voss; 220216-polityka-rujnuje-pallide-voss)) |
| OO Astralna Flara    | 3 | ((221130-astralna-flara-w-strefie-duchow; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| OO Athamarein        | 3 | ((221130-astralna-flara-w-strefie-duchow; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| OO Tivr              | 3 | ((210519-osiemnascie-oczu; 210526-morderstwo-na-inferni; 211117-porwany-trismegistos)) |
| OO Żelazko           | 3 | ((200708-problematyczna-elena; 200722-wielki-kosmiczny-romans; 210922-ostatnia-akcja-bohaterki)) |
| Persefona d'Infernia | 3 | ((200708-problematyczna-elena; 210421-znudzona-zaloga-inferni; 210428-infekcja-serenit)) |
| Raoul Lavanis        | 3 | ((210707-po-drugiej-stronie-bramy; 220216-polityka-rujnuje-pallide-voss; 220610-ratujemy-porywaczy-eleny)) |
| Romeo Verlen         | 3 | ((210210-milosc-w-rodzie-verlen; 210324-lustrzane-odbicie-eleny; 210331-elena-z-rodu-verlen)) |
| Szczepan Kaltaben    | 3 | ((220126-keldan-voss-kolonia-saitaera; 220216-polityka-rujnuje-pallide-voss; 220223-stabilizacja-keldan-voss)) |
| Tadeusz Ursus        | 3 | ((200722-wielki-kosmiczny-romans; 200819-sekrety-orbitera-historia-prawdziwa; 210428-infekcja-serenit)) |
| TAI Rzieza d'K1      | 3 | ((210630-listy-od-fanow; 211013-szara-nawalnica; 211027-rzieza-niszczy-infernie)) |
| Ula Blakenbauer      | 3 | ((230412-dzwiedzie-poluja-na-ser; 230419-karolinka-nieokielznana-swinka; 230502-strasznolabedz-atakuje-granice)) |
| Wawrzyn Rewemis      | 3 | ((211110-romans-dzieki-esuriit; 211124-prototypowa-nereida-natalii; 211208-o-krok-za-daleko)) |
| Władawiec Diakon     | 3 | ((221102-astralna-flara-i-porwanie-na-karsztarinie; 221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| AK Nocna Krypta      | 2 | ((201230-pulapka-z-anastazji; 210825-uszkodzona-brama-eteryczna)) |
| AK Serenit           | 2 | ((210428-infekcja-serenit; 210512-ewakuacja-z-serenit)) |
| Anastazja Sowińska Dwa | 2 | ((201230-pulapka-z-anastazji; 210127-porwanie-anastazji-z-odkupienia)) |
| Arnulf Perikas       | 2 | ((221102-astralna-flara-i-porwanie-na-karsztarinie; 221130-astralna-flara-w-strefie-duchow)) |
| Ataienne             | 2 | ((200916-smierc-raju; 201014-krystaliczny-gniew-elizy)) |
| Bogdan Anatael       | 2 | ((210512-ewakuacja-z-serenit; 210630-listy-od-fanow)) |
| Dariusz Blakenbauer  | 2 | ((210311-studenci-u-verlenow; 210331-elena-z-rodu-verlen)) |
| Eliza Ira            | 2 | ((200923-magiczna-burza-w-raju; 201014-krystaliczny-gniew-elizy)) |
| Ellarina Samarintael | 2 | ((221130-astralna-flara-w-strefie-duchow; 221221-astralna-flara-i-nowy-komodor)) |
| Gilbert Bloch        | 2 | ((210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej)) |
| Grażyna Burgacz      | 2 | ((221102-astralna-flara-i-porwanie-na-karsztarinie; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Hubert Kerwelenios   | 2 | ((221102-astralna-flara-i-porwanie-na-karsztarinie; 221130-astralna-flara-w-strefie-duchow)) |
| Karol Reichard       | 2 | ((210526-morderstwo-na-inferni; 211110-romans-dzieki-esuriit)) |
| Kasandra Destrukcja Diakon | 2 | ((211215-sklejanie-inferni-do-kupy; 211222-kult-saitaera-w-neotik)) |
| Klarysa Jirnik       | 2 | ((221102-astralna-flara-i-porwanie-na-karsztarinie; 221130-astralna-flara-w-strefie-duchow)) |
| Lutus Amerin         | 2 | ((211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide)) |
| Martyna Bianistek    | 2 | ((210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear)) |
| Medea Sowińska       | 2 | ((210825-uszkodzona-brama-eteryczna; 210901-stabilizacja-bramy-eterycznej)) |
| Michał Perikas       | 2 | ((210311-studenci-u-verlenow; 210324-lustrzane-odbicie-eleny)) |
| Natalia Aradin       | 2 | ((211124-prototypowa-nereida-natalii; 220105-to-nie-pulapka-na-nereide)) |
| NekroTAI Zarralea    | 2 | ((221130-astralna-flara-w-strefie-duchow; 221221-astralna-flara-i-nowy-komodor)) |
| OE Falołamacz        | 2 | ((210428-infekcja-serenit; 210512-ewakuacja-z-serenit)) |
| OO Karsztarin        | 2 | ((221004-samotna-w-programie-advancer; 221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| OO Loricatus         | 2 | ((221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| OO Netrahina         | 2 | ((200715-sabotaz-netrahiny; 210901-stabilizacja-bramy-eterycznej)) |
| Salazar Bolza        | 2 | ((221221-astralna-flara-i-nowy-komodor; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| SCA Płetwal Błękitny | 2 | ((210317-arianna-podbija-asimear; 210414-dekralotyzacja-asimear)) |
| SP Pallida Voss      | 2 | ((220126-keldan-voss-kolonia-saitaera; 220216-polityka-rujnuje-pallide-voss)) |
| Szczepan Myrczek     | 2 | ((221102-astralna-flara-i-porwanie-na-karsztarinie; 230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Tal Marczak          | 2 | ((210526-morderstwo-na-inferni; 210707-po-drugiej-stronie-bramy)) |
| Tomasz Kaltaben      | 2 | ((220223-stabilizacja-keldan-voss; 220309-upadek-eleny)) |
| Vigilus Mevilig      | 2 | ((210929-grupa-ekspedycyjna-kellert; 211020-kurczakownia)) |
| Zygfryd Maus         | 2 | ((211117-porwany-trismegistos; 220126-keldan-voss-kolonia-saitaera)) |
| Ada Wyrocznik        | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Adam Nerawol         | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| Adam Permin          | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Adragain Ferrias     | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Aida Serenit         | 1 | ((210512-ewakuacja-z-serenit)) |
| AK Nox Ignis         | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| AK Rodivas           | 1 | ((201230-pulapka-z-anastazji)) |
| AK Salamin           | 1 | ((200708-problematyczna-elena)) |
| Aleksander Leszert   | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Aleksander Samszar   | 1 | ((230419-karolinka-nieokielznana-swinka)) |
| Aleksy Sartaran      | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Alezja Dumorin       | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Arkadia Verlen       | 1 | ((211124-prototypowa-nereida-natalii)) |
| BIA XXX d'Zona Tres  | 1 | ((210714-baza-zona-tres)) |
| Brunhilda Verlen     | 1 | ((210210-milosc-w-rodzie-verlen)) |
| Celina Szilat        | 1 | ((200916-smierc-raju)) |
| Dominika Perikas     | 1 | ((220316-potwor-czy-choroba-na-etaur)) |
| Emilia Lawendowiec   | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Fantazjusz Verlen    | 1 | ((230419-karolinka-nieokielznana-swinka)) |
| Feliks Przędz        | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Feliks Walrond       | 1 | ((210526-morderstwo-na-inferni)) |
| Franz Verlen         | 1 | ((210210-milosc-w-rodzie-verlen)) |
| Gabriel Lodowiec     | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| Gerwazy Kircznik     | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Grzegorz Chropst     | 1 | ((201224-nieprawdopodobny-zbieg-okolicznosci)) |
| Henryk Sowiński      | 1 | ((201230-pulapka-z-anastazji)) |
| Hestia d'Atropos     | 1 | ((220610-ratujemy-porywaczy-eleny)) |
| Hestia d'Neotik      | 1 | ((211208-o-krok-za-daleko)) |
| Horacy Aktenir       | 1 | ((210106-sos-z-haremu)) |
| Hubert Mirsz         | 1 | ((221004-samotna-w-programie-advancer)) |
| Jakub Bulgocz        | 1 | ((220610-ratujemy-porywaczy-eleny)) |
| Jamon Korab          | 1 | ((211117-porwany-trismegistos)) |
| Jarosław Szarjan     | 1 | ((211208-o-krok-za-daleko)) |
| Jasmina Perikas      | 1 | ((201224-nieprawdopodobny-zbieg-okolicznosci)) |
| Jolanta Kopiec       | 1 | ((210922-ostatnia-akcja-bohaterki)) |
| Julia Aktenir        | 1 | ((210106-sos-z-haremu)) |
| Julian Muszel        | 1 | ((200722-wielki-kosmiczny-romans)) |
| Kacper Wentel        | 1 | ((221004-samotna-w-programie-advancer)) |
| Kalira d'Trismegistos | 1 | ((211117-porwany-trismegistos)) |
| Kamil Frederico      | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Karol Atenuatia      | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Kaspian Certisarius  | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| KDN Kajis            | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Kirea Rialirat       | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| Klara Gwozdnik       | 1 | ((211117-porwany-trismegistos)) |
| Konrad Wolczątek     | 1 | ((200722-wielki-kosmiczny-romans)) |
| Kormonow Voss        | 1 | ((220126-keldan-voss-kolonia-saitaera)) |
| Krucjusz Verlen      | 1 | ((210210-milosc-w-rodzie-verlen)) |
| Krystian Blakenbauer | 1 | ((210331-elena-z-rodu-verlen)) |
| Lana Mirkinin        | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Lara Kiriczko        | 1 | ((221004-samotna-w-programie-advancer)) |
| Leo Kasztop          | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| Lidia Nemert         | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Llarnagraht          | 1 | ((210414-dekralotyzacja-asimear)) |
| Lothar Diakon        | 1 | ((200715-sabotaz-netrahiny)) |
| Lucas Oktromin       | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Lucjusz Blakenbauer  | 1 | ((210324-lustrzane-odbicie-eleny)) |
| Maciek Kalmarzec     | 1 | ((211222-kult-saitaera-w-neotik)) |
| Maks Samszar         | 1 | ((230502-strasznolabedz-atakuje-granice)) |
| Malictrix d'Pandora  | 1 | ((210414-dekralotyzacja-asimear)) |
| Marianna Lemurczak   | 1 | ((200923-magiczna-burza-w-raju)) |
| Mariusz Bulterier    | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Mariusz Tubalon      | 1 | ((210414-dekralotyzacja-asimear)) |
| Marta Krissit        | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Melania Akacja       | 1 | ((220316-potwor-czy-choroba-na-etaur)) |
| Michał Teriakin      | 1 | ((210630-listy-od-fanow)) |
| Michał Warkoczak     | 1 | ((221004-samotna-w-programie-advancer)) |
| Miłosz Klinek        | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Mira Anastel         | 1 | ((211117-porwany-trismegistos)) |
| Morrigan d'Tirakal   | 1 | ((210421-znudzona-zaloga-inferni)) |
| Nikodem Sowiński     | 1 | ((200923-magiczna-burza-w-raju)) |
| OA Bakałarz          | 1 | ((210120-sympozjum-zniszczenia)) |
| OA Odkupienie        | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| OA Zguba Tytanów     | 1 | ((201230-pulapka-z-anastazji)) |
| OE Lord Savaron      | 1 | ((210630-listy-od-fanow)) |
| OE Piękna Elena      | 1 | ((210428-infekcja-serenit)) |
| Olena Orion          | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| Oliwia Pietrova      | 1 | ((210108-ratunkowa-misja-goldariona)) |
| ON Spatium Gelida    | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| OO Alaya             | 1 | ((210519-osiemnascie-oczu)) |
| OO Aurelion          | 1 | ((200708-problematyczna-elena)) |
| OO Galaktyczny Tucznik | 1 | ((200909-arystokratka-w-ladowni-na-swinie)) |
| OO Kanagar           | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| OO Kastor            | 1 | ((220126-keldan-voss-kolonia-saitaera)) |
| OO Mfumo             | 1 | ((210825-uszkodzona-brama-eteryczna)) |
| OO Omega Septius     | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| OO Opresor           | 1 | ((210616-nieudana-infiltracja-inferni)) |
| OO Optymistyczny Żuk | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| OO Trasman           | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| OO Tvarana           | 1 | ((200715-sabotaz-netrahiny)) |
| OO Welgat            | 1 | ((200722-wielki-kosmiczny-romans)) |
| OO Wesoły Wieprzek   | 1 | ((201014-krystaliczny-gniew-elizy)) |
| Percival Diakon      | 1 | ((200715-sabotaz-netrahiny)) |
| Persefona d'Loricatus | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Przemysław Czapurt   | 1 | ((210324-lustrzane-odbicie-eleny)) |
| Rafael Galwarn       | 1 | ((210630-listy-od-fanow)) |
| Rafał Armadion       | 1 | ((200916-smierc-raju)) |
| Rafał Grambucz       | 1 | ((210428-infekcja-serenit)) |
| Rafał Perikas        | 1 | ((210311-studenci-u-verlenow)) |
| Remigiusz Błyszczyk  | 1 | ((211013-szara-nawalnica)) |
| Remigiusz Falorin    | 1 | ((210630-listy-od-fanow)) |
| Robert Garwen        | 1 | ((200916-smierc-raju)) |
| Rozalia Teirik       | 1 | ((210106-sos-z-haremu)) |
| Rufus Komczirp       | 1 | ((200715-sabotaz-netrahiny)) |
| Rufus Niegnat        | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| Rufus Samszar        | 1 | ((210311-studenci-u-verlenow)) |
| Sabina Servatel      | 1 | ((200819-sekrety-orbitera-historia-prawdziwa)) |
| Sabrina Ferrias      | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Saitaer              | 1 | ((220105-to-nie-pulapka-na-nereide)) |
| Sandra Kantarelo     | 1 | ((221004-samotna-w-programie-advancer)) |
| Sargon Niiris        | 1 | ((221221-astralna-flara-i-nowy-komodor)) |
| SC Trismegistos      | 1 | ((211117-porwany-trismegistos)) |
| SCA Goldarion        | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Semla d'Goldarion    | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Seraf Verlen         | 1 | ((210210-milosc-w-rodzie-verlen)) |
| Serena Krissit       | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Seweryn Atanair      | 1 | ((210901-stabilizacja-bramy-eterycznej)) |
| SL Uśmiechnięta      | 1 | ((210108-ratunkowa-misja-goldariona)) |
| SP Plugawy Jaszczur  | 1 | ((210127-porwanie-anastazji-z-odkupienia)) |
| SP Światło Nadziei   | 1 | ((220223-stabilizacja-keldan-voss)) |
| Suwan Chankar        | 1 | ((220316-potwor-czy-choroba-na-etaur)) |
| Sylwia Perikas       | 1 | ((210311-studenci-u-verlenow)) |
| Szczepan Myksza      | 1 | ((200715-sabotaz-netrahiny)) |
| Szymon Kapeć         | 1 | ((230502-strasznolabedz-atakuje-granice)) |
| Szymon Wanad         | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| TAI Marszałek Grzmotoszpon Trzeci | 1 | ((210929-grupa-ekspedycyjna-kellert)) |
| TAI XT-723 d'K1      | 1 | ((210630-listy-od-fanow)) |
| TAI Zefiris          | 1 | ((210630-listy-od-fanow)) |
| Tomasz Dojnicz       | 1 | ((221130-astralna-flara-w-strefie-duchow)) |
| Tymek Samszar        | 1 | ((230419-karolinka-nieokielznana-swinka)) |
| Tymon Krakdacz       | 1 | ((221004-samotna-w-programie-advancer)) |
| Tymoteusz Czerw      | 1 | ((220316-potwor-czy-choroba-na-etaur)) |
| Ulisses Kalidon      | 1 | ((210714-baza-zona-tres)) |
| Wanessa Pyszcz       | 1 | ((200916-smierc-raju)) |
| Wojciech Namczak     | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Zuzanna Kraczamin    | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
| Żaneta Krawędź       | 1 | ((230124-kjs-wygrac-za-wszelka-cene)) |
---
categories: profile
factions: 
owner: public
title: Magda Patiril
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 180708-niewidzialne-potwory-z-rzeki | rezydentka; utrzymuje rzekę Błękitkę w bezpieczeństwie dla okolicy; uszkodziła jej się magitrownia i trochę padła reputacja. | 0109-08-18 - 0109-08-20 |
| 180718-msciwa-ryba-z-eteru          | nigdy nie zależało jej na obrazie który straciła; jej posiadłość została uszkodzona i obłożona klątwą. Co gorsza, dostała po reputacji. | 0109-08-25 - 0109-08-27 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 180718-msciwa-ryba-z-eteru          | jej posiadłość jest obłożona klątwą Kamiennej Ryby. Cóż, Magda wykorzysta to do fundraisingu jako atrakcję turystyczną | 0109-08-27

## Plany


| Opowieść | Plan | Końcowa data |
| ---- | ---- | ---- |
| 180718-msciwa-ryba-z-eteru          | wykorzystać klątwę jako atrakcję turystyczną; na Frontierze nie jest łatwo o patronów czy złoto a to jest jeden ze sposobów. | 0109-08-27

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Joachim Kozioro      | 2 | ((180708-niewidzialne-potwory-z-rzeki; 180718-msciwa-ryba-z-eteru)) |
| Kalina Rotmistrz     | 2 | ((180708-niewidzialne-potwory-z-rzeki; 180718-msciwa-ryba-z-eteru)) |
| Antoni Zajcew        | 1 | ((180708-niewidzialne-potwory-z-rzeki)) |
| Borys Perikas        | 1 | ((180718-msciwa-ryba-z-eteru)) |
| Lawenda Weiner       | 1 | ((180718-msciwa-ryba-z-eteru)) |
| Majka Perikas        | 1 | ((180718-msciwa-ryba-z-eteru)) |
| Stach Sosnowiecki    | 1 | ((180708-niewidzialne-potwory-z-rzeki)) |
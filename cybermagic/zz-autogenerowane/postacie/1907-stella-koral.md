---
categories: profile
factions: 
owner: kic
title: Stella Koral
---

# {{ page.title }}


# Read: 

## Postać

### Ogólny pomysł (3)

Pilot i górnik. Sprzężona ze swoim statkiem, Mikado.
Scrapper, który zrobi laser górniczy z niczego.
Uwielbia ciężkie lasery górnicze.
Archertypy: Scrapper, Neurosprzężony pilot.

### Czego chce a nie ma (3)

* dla siebie: reputacja najlepszego statku górniczego
* dla siebie: wobody eksploracji
* dla sibie: niezależności i akcpetacji
* dla innych: wolności dla AI
* dla innych: bezpieczeństwa załogi

### Sposób działania (3)

* prowizorka - konstrukcje z byle czego, adaptacja sprzętu, ad-hoc elektronika, konstrukcja psychotroniki
* chaos - odwracanie uwagi, bomby, dywersja na odległość, chemikalia, pułapki
* opętane neuromaszyny - przejmowanie kontroli, zostawianie bomb logicznych, konstrukcja psychotroniki
* survival - ocena sytuacji, znajdowanie zasobów, S&R

### Zasoby i otoczenie (3)

* Mikado - statek górniczy z sarkastyczym AI 'Eva'

### Magia (3)

#### Gdy kontroluje energię

* Wspomagane konstrukcje z niczego
* Neuronautka eksplorująca bazy danych i umysły AI
* Skan i wykrywanie niebezpieczeństw

#### Gdy traci kontrolę

* Mściwe golemy, pół-świadome maszyny
* Złomowisko dookoła

### Powiązane frakcje

{{ page.factions }}

## Opis




# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190802-statek-zjada-statki          | neuropilot i scrapper Mikado; zintegrowała się z Serenitem by kupić czas Zespołowi na pokonanie Overminda i ewakuację Laury, Aidy i Michała na Mikado. | 0087-08-09 - 0087-08-12 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 190802-statek-zjada-statki          | stała się współdzielącym z Evą bytem zamieszkującym Mikado. Mikado ma teraz dwóch pilotów. | 0087-08-12

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Aida Serenit         | 1 | ((190802-statek-zjada-statki)) |
| Eva d'Mikado         | 1 | ((190802-statek-zjada-statki)) |
| Ignaś Orbita         | 1 | ((190802-statek-zjada-statki)) |
| Laura Prunal         | 1 | ((190802-statek-zjada-statki)) |
| Michał Dusiciel      | 1 | ((190802-statek-zjada-statki)) |
| Mikado               | 1 | ((190802-statek-zjada-statki)) |
| Serenit              | 1 | ((190802-statek-zjada-statki)) |
| Sia d'Cranis         | 1 | ((190802-statek-zjada-statki)) |
| Travis Longhorn      | 1 | ((190802-statek-zjada-statki)) |
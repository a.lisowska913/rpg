---
categories: profile
factions: 
owner: public
title: Karolina Erenit
---

# {{ page.title }}


# Read: 

## Paradoks

potężna ixiońska czarodziejka identyfikująca się ze światem ludzi

## Motywacja

### O co walczy

* kontroluje swoją moc, nikogo nie skrzywdzi i wie co się stanie gdy rzuci czar
* znalazła swoje miejsce w nowym świecie- ma pracę i jest przydatna

### Przeciw czemu walczy

* magowie pomiatają ludźmi a ci nawet o tym nie wiedzą
* zatraci się w tym nowym świecie i straci człowieczeństwo

## Działania

### Specjalność

* ogromna moc magiczna
* energia ixiońska
* świetny strateg
* perfekcyjne poruszanie się w świecie ludzi

### Słabość

* gubi się w świecie magii, źle ocenia sytuacje społeczne i faktyczne
* odpycha od siebie innych
* boi się samej siebie

### Akcje

* cicha, wycofana samotniczka; taki nerd
* gra w strategie w virt
* zaprzyjaźnia się z ludźmi, nie magami.
* działa jak człowiek, nie mag
* magią włada jak nieprecyzyjnym dwuręcznym młotem

### Znaczące czyny

* wyszła na sam szczyt Zaczęstwiaków w Supreme Missionforce; dowodzi ekipą
* nie spotkała się z energią czy czarem nie do przełamania / Skażenia; jest na ścieżce maga bojowego
* zastawiła pułapkę na studenciaków podjadających w drogim hotelu i zmusiła Szkołę Magów do publicznej reakcji

## Mechanika

### Archetypy

strateg virt, zbyt genialny mag

### Motywacje

?

## Inne

### Wygląd

?


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 181030-zaczestwiacy-czy-karolina    | młoda tsundere, studentka sieci i internetu. Uważa, że internet jest DZIWNY. Chce zniechęcić chłopaka (Damiana) do gry w Supreme Missionforce. | 0109-10-14 - 0109-10-15 |
| 181101-wojna-o-uczciwe-polfinaly    | rozerwana magicznie ze studnią życzeń, podskoczyła jej uroda drastycznie plus w ciągu miesiąca będzie wybitnym graczem SupMis. | 0109-10-16 - 0109-10-18 |
| 190113-chronmy-karoline-przed-uczniami | była źle traktowana przez magów (nie wie o tym), więc Napoleon Bankierz próbował ją chronić eliksirem Adeli Kirys. Stała się wektorem toksyny z Trzęsawiska. Tymczasowo. | 0110-01-04 - 0110-01-05 |
| 190127-ixionski-transorganik        | stały target Wojtka Kurczynosa i kilku innych magów Szkoły Magów w Zaczęstwie. Jako, że jest człowiekiem, nic nie pamięta. | 0110-01-28 - 0110-01-29 |
| 190202-czarodziejka-z-woli-saitaera | przestała być ofiarą. Gdy stała się czarodziejką, zaczęła działać inaczej - pomiędzy strachem a agresją. Dotknął ją Karradrael, by wyczyścić Saitaera. | 0110-01-31 - 0110-02-04 |
| 190206-nie-da-sie-odrzucic-mocy     |  | 0110-02-16 - 0110-02-19 |
| 190217-chevaleresse                 | w Cyberszkole zaatakowana przez Dianę, potem pchnięta na ziemię przez Tymona gdy ów chciał przerwać jej czar. Ogólnie, nie jej dzień ;-). | 0110-02-24 - 0110-02-26 |
| 190830-kto-wrobil-alana             | zwinęła Chevaleresse artefakt Alana i oddała go magom Aurum by wpakować ich w katastrofalne kłopoty. | 0110-05-30 - 0110-06-01 |
| 201006-dezinhibitor-dla-sabiny      | samotna, ciepła wobec Myrczka. Myrczek wymiauczał sobie jej pomoc przy dezinhibitorze. Wybitny taktyk, powiedziała Zespołowi czemu coś jej śmierdzi w planie Myrczka. Zintegrowana magią z Gabrielem, sama poczuła jak podły plan miał Kumczek i umysł Loreny Gwozdnik. | 0110-10-03 - 0110-10-05 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 181030-zaczestwiacy-czy-karolina    | polubiła Supreme Missionforce i z radością gra z Zaczęstwiakami - a zwłaszcza z Lią. | 0109-10-15
| 181101-wojna-o-uczciwe-polfinaly    | utraciła połączenie ze Studnią Życzeń; nie działają w jej okolicy już dziwne magiczne efekty | 0109-10-18
| 181101-wojna-o-uczciwe-polfinaly    | ma nadnaturalne umiejętności grania w Supreme Missionforce; do tego jest naprawdę piękną dziewczyną przez zrost mocy Pięknotki i Tymona | 0109-10-18
| 190202-czarodziejka-z-woli-saitaera | stała się czarodziejką mocą Saitaera; odebrała moc Wojtkowi Kurczynosowi. | 0110-02-04
| 190202-czarodziejka-z-woli-saitaera | mocą Karradraela wyczyszczona z wpływów Saitaera - Władca Rekonstrukcji nie ma na niej holdu. | 0110-02-04
| 190202-czarodziejka-z-woli-saitaera | nielubiana przez wielu magów zwłaszcza w szkole z uwagi na korelację z jej uzyskaniem mocy i utratą mocy przez Wojtka. | 0110-02-04

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Pięknotka Diakon     | 7 | ((181101-wojna-o-uczciwe-polfinaly; 190113-chronmy-karoline-przed-uczniami; 190127-ixionski-transorganik; 190202-czarodziejka-z-woli-saitaera; 190206-nie-da-sie-odrzucic-mocy; 190217-chevaleresse; 190830-kto-wrobil-alana)) |
| Tymon Grubosz        | 5 | ((181101-wojna-o-uczciwe-polfinaly; 190127-ixionski-transorganik; 190202-czarodziejka-z-woli-saitaera; 190206-nie-da-sie-odrzucic-mocy; 190217-chevaleresse)) |
| Alan Bartozol        | 3 | ((181101-wojna-o-uczciwe-polfinaly; 190217-chevaleresse; 190830-kto-wrobil-alana)) |
| Minerwa Metalia      | 3 | ((190127-ixionski-transorganik; 190202-czarodziejka-z-woli-saitaera; 190206-nie-da-sie-odrzucic-mocy)) |
| Napoleon Bankierz    | 3 | ((190113-chronmy-karoline-przed-uczniami; 190127-ixionski-transorganik; 201006-dezinhibitor-dla-sabiny)) |
| Adela Kirys          | 2 | ((190113-chronmy-karoline-przed-uczniami; 190202-czarodziejka-z-woli-saitaera)) |
| Arnulf Poważny       | 2 | ((190113-chronmy-karoline-przed-uczniami; 190206-nie-da-sie-odrzucic-mocy)) |
| Damian Podpalnik     | 2 | ((181030-zaczestwiacy-czy-karolina; 181101-wojna-o-uczciwe-polfinaly)) |
| Diana Tevalier       | 2 | ((190217-chevaleresse; 190830-kto-wrobil-alana)) |
| Marlena Maja Leszczyńska | 2 | ((181101-wojna-o-uczciwe-polfinaly; 190217-chevaleresse)) |
| Saitaer              | 2 | ((190127-ixionski-transorganik; 190202-czarodziejka-z-woli-saitaera)) |
| Wojtek Kurczynos     | 2 | ((190127-ixionski-transorganik; 190202-czarodziejka-z-woli-saitaera)) |
| Aranea Diakon        | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Erwin Galilien       | 1 | ((190127-ixionski-transorganik)) |
| Gabriel Ursus        | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Ignacy Myrczek       | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Justynian Diakon     | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Kasjopea Maus        | 1 | ((190206-nie-da-sie-odrzucic-mocy)) |
| Kirisu Gero          | 1 | ((181030-zaczestwiacy-czy-karolina)) |
| Kirył Najłalmin      | 1 | ((190127-ixionski-transorganik)) |
| Kornel Garn          | 1 | ((190206-nie-da-sie-odrzucic-mocy)) |
| Laura Tesinik        | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Lia Sagabello        | 1 | ((181030-zaczestwiacy-czy-karolina)) |
| Liliana Bankierz     | 1 | ((190113-chronmy-karoline-przed-uczniami)) |
| Lorena Gwozdnik      | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Mariusz Kozaczek     | 1 | ((181030-zaczestwiacy-czy-karolina)) |
| Michał Krutkiwąs     | 1 | ((181030-zaczestwiacy-czy-karolina)) |
| Rafał Kumczek        | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Sabina Kazitan       | 1 | ((201006-dezinhibitor-dla-sabiny)) |
| Sławomir Muczarek    | 1 | ((190202-czarodziejka-z-woli-saitaera)) |
| Tadeusz Kruszawiecki | 1 | ((181101-wojna-o-uczciwe-polfinaly)) |
| Talia Aegis          | 1 | ((190830-kto-wrobil-alana)) |
| Teresa Mieralit      | 1 | ((190113-chronmy-karoline-przed-uczniami)) |
| Wiktor Satarail      | 1 | ((190127-ixionski-transorganik)) |
| Wojciech Zermann     | 1 | ((190830-kto-wrobil-alana)) |
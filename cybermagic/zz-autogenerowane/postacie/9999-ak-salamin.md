---
categories: profile
factions: 
owner: public
title: AK Salamin
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 200708-problematyczna-elena         | kiedyś krążownik Orbitera i carrier ŁeZ, teraz Anomalia Kosmiczna. Żyje przeszłością; porwał Aureliona i chciał niszczyć "noktian". Arianna wydała mu rozkaz samozniszczenia, który spełnił. | 0110-10-29 - 0110-11-03 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Antoni Kramer        | 1 | ((200708-problematyczna-elena)) |
| Arianna Verlen       | 1 | ((200708-problematyczna-elena)) |
| Elena Verlen         | 1 | ((200708-problematyczna-elena)) |
| Eustachy Korkoran    | 1 | ((200708-problematyczna-elena)) |
| Klaudia Stryk        | 1 | ((200708-problematyczna-elena)) |
| Leszek Kurzmin       | 1 | ((200708-problematyczna-elena)) |
| Olgierd Drongon      | 1 | ((200708-problematyczna-elena)) |
| OO Aurelion          | 1 | ((200708-problematyczna-elena)) |
| OO Żelazko           | 1 | ((200708-problematyczna-elena)) |
| Persefona d'Infernia | 1 | ((200708-problematyczna-elena)) |
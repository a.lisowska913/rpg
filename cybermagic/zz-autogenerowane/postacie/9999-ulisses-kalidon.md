---
categories: profile
factions: 
owner: public
title: Ulisses Kalidon
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210714-baza-zona-tres               | noktiański spec od komputerów na Inferni; uratowany przez Elenę przed systemami zabezpieczeń w zbrojowni Zony Tres. Myślał że przejmie a tu klops ;-). | 0111-03-16 - 0111-03-18 |
| 210728-w-cieniu-nocnej-krypty       | wie sporo o Finis Vitae i o historii Noctis i upadku Noctis. Można powiedzieć, "koneser" historii Noctis i jednostek noktiańskich. | 0111-03-22 - 0111-04-08 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 2 | ((210714-baza-zona-tres; 210728-w-cieniu-nocnej-krypty)) |
| Eustachy Korkoran    | 2 | ((210714-baza-zona-tres; 210728-w-cieniu-nocnej-krypty)) |
| Janus Krzak          | 2 | ((210714-baza-zona-tres; 210728-w-cieniu-nocnej-krypty)) |
| Romana Arnatin       | 2 | ((210728-w-cieniu-nocnej-krypty; 210728-w-cieniu-nocnej-krypty)) |
| AK Nocna Krypta      | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| Atrius Kurunen       | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| BIA XXX d'Zona Tres  | 1 | ((210714-baza-zona-tres)) |
| Elena Verlen         | 1 | ((210714-baza-zona-tres)) |
| Finis Vitae          | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| Gerard Adanor        | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| Helena Adanor        | 1 | ((210728-w-cieniu-nocnej-krypty)) |
| Martyn Hiwasser      | 1 | ((210714-baza-zona-tres)) |
| Oliwia Karelan       | 1 | ((210728-w-cieniu-nocnej-krypty)) |
---
categories: profile
factions: 
owner: public
title: Tymon Korkoran
---

# {{ page.title }}


# Generated: 



## Fiszki


* egzaltowany lekkoduch i następca Bartłomieja | @ 230201-wylaczone-generatory-memoriam-inferni
* (ENCAO:  +--00 |Co chce, weźmie;;Niecierpliwy;;Egzaltowany | Stimulation, Tradition > Face, Humility | TAK: Być lepszym od Bartłomieja w oczach Kidirona) | @ 230201-wylaczone-generatory-memoriam-inferni
* "Infernia jest moja i należy do mnie. Bartłomiej marnuje jej potencjał, a dzięki niej Nativis może rządzić Neikatis!" | @ 230201-wylaczone-generatory-memoriam-inferni
* egzaltowany lekkoduch i już nie następca Bartłomieja | @ 230208-pierwsza-randka-eustachego

### Wątki


historia-eustachego
arkologia-nativis
infernia-jej-imieniem
zbrodnie-kidirona

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220723-polowanie-na-szczury-w-nativis | lekkoduch rozdarty między wujem Bartłomiejem i Laurencjuszem Kidironem (15); został słupem dla Celiny odnośnie polowania na szczury, bo chce zaimponować swojej eks-dziewczynie że robi coś przydatnego (i zarobić za nic). | 0087-05-03 - 0087-05-12 |
| 220831-czarne-helmy-i-robaki        | kuzyn Eustachego; bardzo chce się przypodobać Kidironom; Eustachy wpakował go w pułapkę by Robaki go sklepały. Tak dąży do chwały i potęgi że wpadł w to jak śliwka w kompot. De facto został "tym przez kogo rozwiązano konflikt" w oczach Kidironów i Nativis. | 0092-08-15 - 0092-08-27 |
| 230201-wylaczone-generatory-memoriam-inferni | jedyny świadek tego, że Eustachy COŚ ZROBIŁ z Infernią. Wszystko się mu rozpada - Kidiron skupia się na Eustachym, Infernia wypada z rąk. Eksplodował na Celinę nazywając ją znajdką i wujek nim wzgardził. Potem sklupany przez Janka. Stracił WSZYSTKO. | 0093-02-10 - 0093-02-12 |
| 230215-terrorystka-w-ambasadorce    | regularnie próbował się wkraść do Ambasadorki podglądać dziewczyny i znalazł ścieżkę którą powiedział Ralfowi. Nie wiadomo czy być z niego dumnym czy nim gardzić. | 0093-02-22 - 0093-02-23 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 220831-czarne-helmy-i-robaki        | przesunięty na zupełnie inną pozycję, wielka chwała u Kidironów. PLUS nieufność ojca (Bartłomieja Korkorana) i Karola Lertysa (dziadka Celiny i Janka) | 0092-08-27

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ardilla Korkoran     | 3 | ((220831-czarne-helmy-i-robaki; 230201-wylaczone-generatory-memoriam-inferni; 230215-terrorystka-w-ambasadorce)) |
| Celina Lertys        | 3 | ((220723-polowanie-na-szczury-w-nativis; 220831-czarne-helmy-i-robaki; 230201-wylaczone-generatory-memoriam-inferni)) |
| Eustachy Korkoran    | 3 | ((220831-czarne-helmy-i-robaki; 230201-wylaczone-generatory-memoriam-inferni; 230215-terrorystka-w-ambasadorce)) |
| Jan Lertys           | 3 | ((220723-polowanie-na-szczury-w-nativis; 220831-czarne-helmy-i-robaki; 230201-wylaczone-generatory-memoriam-inferni)) |
| Bartłomiej Korkoran  | 2 | ((230201-wylaczone-generatory-memoriam-inferni; 230215-terrorystka-w-ambasadorce)) |
| Kalia Awiter         | 2 | ((230201-wylaczone-generatory-memoriam-inferni; 230215-terrorystka-w-ambasadorce)) |
| Rafał Kidiron        | 2 | ((230201-wylaczone-generatory-memoriam-inferni; 230215-terrorystka-w-ambasadorce)) |
| Ralf Tapszecz        | 2 | ((230201-wylaczone-generatory-memoriam-inferni; 230215-terrorystka-w-ambasadorce)) |
| Emilia d'Erozja      | 1 | ((220723-polowanie-na-szczury-w-nativis)) |
| Ernest Puszczowiec   | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Kalista Luminis      | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Karol Lertys         | 1 | ((220723-polowanie-na-szczury-w-nativis)) |
| Kratos Coruscatis    | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Laurencjusz Kidiron  | 1 | ((220723-polowanie-na-szczury-w-nativis)) |
| Magda Misteria Sarbanik | 1 | ((230215-terrorystka-w-ambasadorce)) |
| OA Erozja Ego        | 1 | ((220723-polowanie-na-szczury-w-nativis)) |
| OO Infernia          | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Stanisław Uczantor   | 1 | ((220831-czarne-helmy-i-robaki)) |
| Tobiasz Lobrak       | 1 | ((230215-terrorystka-w-ambasadorce)) |
| Wojciech Czerpń      | 1 | ((220831-czarne-helmy-i-robaki)) |
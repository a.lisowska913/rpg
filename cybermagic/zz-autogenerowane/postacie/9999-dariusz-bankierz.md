---
categories: profile
factions: 
owner: public
title: Dariusz Bankierz
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190208-herbata-grzyby-i-mimik       | mag-mafioso, dowodzi Herbastem. Dobry organizator i ogólnie nie jest szczególnie ZŁY. Taki kindermafioso. Nie poszerzył wpływów i potęgi, ale doprowadził do wygnania Jadwigi. | 0110-03-03 - 0110-03-05 |
| 190213-wygasniecie-starego-autosenta | okazało się, że posiada umiejętności hackerskie. Nawiązał kontakt z autosentem i dowiedział się co jest grane. | 0110-03-07 - 0110-03-08 |
| 190226-korporacyjna-wojna-w-mmo     | dał się skusić by wziąć tematy związane z Rosą Volant na siebie; ma teraz przeciw sobie dwie | 0110-03-13 - 0110-03-16 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 190226-korporacyjna-wojna-w-mmo     | będzie zaatakowany przez siły zarówno Rexpapier jak i Sensus, niestety... | 0110-03-16

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Antoni Kotomin       | 3 | ((190208-herbata-grzyby-i-mimik; 190213-wygasniecie-starego-autosenta; 190226-korporacyjna-wojna-w-mmo)) |
| Baltazar Rączniak    | 2 | ((190208-herbata-grzyby-i-mimik; 190213-wygasniecie-starego-autosenta)) |
| Jadwiga Pszarnik     | 2 | ((190208-herbata-grzyby-i-mimik; 190213-wygasniecie-starego-autosenta)) |
| Pięknotka Diakon     | 2 | ((190208-herbata-grzyby-i-mimik; 190213-wygasniecie-starego-autosenta)) |
| Adrian Wężoskór      | 1 | ((190226-korporacyjna-wojna-w-mmo)) |
| Alan Bartozol        | 1 | ((190226-korporacyjna-wojna-w-mmo)) |
| Almeda Literna       | 1 | ((190208-herbata-grzyby-i-mimik)) |
| Atena Sowińska       | 1 | ((190213-wygasniecie-starego-autosenta)) |
| Eliza Kotlet         | 1 | ((190226-korporacyjna-wojna-w-mmo)) |
| Kermit Szperacz      | 1 | ((190226-korporacyjna-wojna-w-mmo)) |
| Mi Ruda              | 1 | ((190226-korporacyjna-wojna-w-mmo)) |
| Rafał Bobowiec       | 1 | ((190213-wygasniecie-starego-autosenta)) |
| Rafał Królewski      | 1 | ((190226-korporacyjna-wojna-w-mmo)) |
| Stach Sosnowiecki    | 1 | ((190208-herbata-grzyby-i-mimik)) |
| Wojciech Słabizna    | 1 | ((190226-korporacyjna-wojna-w-mmo)) |
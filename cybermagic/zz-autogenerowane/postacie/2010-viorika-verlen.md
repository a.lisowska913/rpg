---
categories: profile
factions: 
owner: public
title: Viorika Verlen
---

# {{ page.title }}


# Read: 

## Kim jest

### W kilku zdaniach



### Co się rzuca w oczy

* 

## Mechanika

### Znaczące Czyny i Osiągnięcia (3)

* Kiedy młodziutka Elena wpadła w zasadzkę konkurencyjnego gangu dzieciaków, Viorika zorganizowała odsiecz i odbiła kuzynkę.
* Po odkryciu, że Lucjusz działał na terenie Verlenów, używając senti sieci go wypłoszyła i osaczyła oddziałami, które zmotywowała.
* 

### Co się rzuca w oczy: Atuty i Przewagi (3, 6)

* PRACA: Dowódca i szturmowiec.
* PRACA: Potrafi przechytrzyć praktycznie każdego.
* SERCE: Niezależnie od okoliczności daje z siebie wszystko, idąc na front.
* SERCE: We wszystkim dąży do intentsywnej rozrywki, ale nie kosztem zatracenia.
* ATUT: Dobrze zintegrowana z senti siecią.
* ATUT: 

### Charakterystyczne zasoby (3)

* COŚ: 
* KTOŚ: Jej własne oddziały Verlenów
* WIEM: 
* OPINIA: 

### Typowe problemy z którymi sobie nie radzi (-3)

* CIAŁO: 
* CIAŁO: 
* SERCE: 
* SERCE: 
* STRACH: Boi się bezradności. Robi co może, żeby mieć odpowiedź na różne sytuacje.
* STRACH:
* MAGIA: 
* INNI: 

### Specjalne

* .

## Inne

### Wygląd

.

### Coś Więcej

* .

### Endgame

* .


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210224-sentiobsesja                 | 17 lat, zwana "Wirek". Umysł taktyczny i bardzo mocno powiązany z sentisiecią Verlen. Przejęła kontrolę nad sentisiecią i bez chwili wahania uderzyła nią w Apollo by wyrwać go z sentisieci. Mniej społeczna niż Arianna, bardziej intelektualna. | 0092-08-03 - 0092-08-06 |
| 210302-umierajaca-farma-biovatow    | Sentisiecią znalazła obecność Lucjusza Blakenbauera i jego podłych planów we Wremłowie a taktyką i umiejętnościami dowódczymi złapała wszystkie jego płaszczki - nawet te, które były przeznaczone do prześlizgnięcia się. | 0093-06-19 - 0093-06-21 |
| 210306-wiktoriata                   | nieświadomie rozwaliła plan sił specjalnych Blakenbauerów oraz wraz z Lucjuszem (którego ściągnęła) uratowała przed psychotycznymi żołnierzami Verlenów grupę cywili Blakenbauerów i Verlenów. | 0093-10-21 - 0093-10-30 |
| 230328-niepotrzebny-ratunek-mai     | uratowała Maję Samszar po jej spotkaniu z potworem oraz gdy Karolinus rozpoczął "inwazję efemerycznych Maj" wymanewrowała je na potwora i obroniła VirtuFortis. | 0095-06-30 - 0095-07-02 |
| 230412-dzwiedzie-poluja-na-ser      | wydobyła od Apollo, co się działo z Eleną. Potem skłoniła Ulę do wyzwania Verlenów a nie robienia losowych płaszczek. Opracowała plan z dźwiedziami, by całkowicie uniknąć walki. Grand Strategist, zostawiła działanie Ariannie. | 0095-07-12 - 0095-07-14 |
| 230502-strasznolabedz-atakuje-granice | wiedząc, że Marcinozaurowi uciekł łabędź, zdecydowała się mu pomóc i wziąć Elenę (ćwiczenia). Nie udało jej się tego zrobić dyskretnie. Używa 'cuteness' Eleny jako zasobu do przekonania Maksa. Gdy musiała oddać sprzęt, zaufała swoim ludziom (planom) i Elenie. Współpracuje z Maksem po wszystkim, daje mu dźwiedzia konsultanta bezpieczeństwa. | 0095-07-17 - 0095-07-19 |
| 230419-karolinka-nieokielznana-swinka | musztrowała Karolinkę by zająć czas; przekonała Zespół by współpracować z Samszarami. Bardzo nie chce być na operacji 'mścimy Viorikę', ale Marcinozaur... Taktyk i siła ognia zespołu. | 0095-07-19 - 0095-07-23 |
| 230426-mscizab-zabojca-arachnoziemskich-jaszczurow | gdy Mściząb (17) ją zaczepił, poszła z nim na ostro i zdewastowała w walce. Opierniczyła Mścizęba i próbowała pokazać mu zniszczenia jakie powoduje. "Zły glina" gadając z Emmanuelle. | 0095-08-03 - 0095-08-05 |
| 230524-ekologia-jaszczurow-w-arachnoziemie | przepytała żołnierzy i Kłów odnośnie sytuacji, sprawdziła dokumenty żołnierzy by izolować śluzińce, zaopiekowała się pacyfistyczną Ulą i po włączeniu Mścizęba w akcję usuwania krystalicznego mimika opracowała jak go usunąć i go rozstrzelała. | 0095-08-06 - 0095-08-09 |
| 210311-studenci-u-verlenow          | 21 lat, podczas walki z echem Eleny sprzęgła je z lustrem, po czym odnalazła to lustro używając Eleny. Uspokajała i zajmowała się Eleną. | 0096-11-18 - 0096-11-24 |
| 210324-lustrzane-odbicie-eleny      | jej ukrytym chłopakiem jest Lucjusz Blakenbauer. Uratowała Michała Perikasa używając jetpacka, zbadała sentisieć pod kątem Skalniaczków i ich wpływu oraz odkryła sekretne Miejsce Kaźni Eleny. | 0097-07-05 - 0097-07-08 |
| 210210-milosc-w-rodzie-verlen       | starsza od Arianny i stopniem i wiekiem, przyjaciółka Arianny i Eleny. Mistrzowsko steruje sentisiecią i stabilizuje wszystko, by Arianna i Elena nie ucierpiały. Nieco złośliwa ;-). | 0111-06-23 - 0111-06-26 |
| 210331-elena-z-rodu-verlen          | dogadała się z Dariuszem Blakenbauerem i doprowadziła do tego, że on nie chce wojny z Eleną i sam lobbuje za pokojem w swoim rodzie. | 0111-06-29 - 0111-07-02 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 210224-sentiobsesja                 | chwała jej planom; już w tak młodym wieku uznawana za świetnego stratega znajdującego słabe punkty u przeciwnika. | 0092-08-06
| 210306-wiktoriata                   | zneutralizowała tajną siatkę szpiegowsko-infiltracyjną Blakenbauerów w formie wił, wprowadzając własne, czyste, z innego źródła. Dzięki, Lucjusz :D. Za to dostała pochwałę z kontrwywiadu. | 0093-10-30
| 210306-wiktoriata                   | po zniszczeniu planu z wiłami uznana przez Blakenbauerów za "mistrzynię szpiegów". She is the spymistress, kontrwywiad. Udaje niewinną i słodką, ale w rzeczywistości jej umysł stoi za dużą ilością kontr-planów. | 0093-10-30
| 210306-wiktoriata                   | jakieś pół roku po tym wydarzeniu została parą z Lucjuszem Blakenbauerem. | 0093-10-30
| 230516-karolinka-raciczki-zemsty-verlenow | zdaniem Verlenów, odpowiednio pomszczona za Karolinusa. Nic nie musi robić w sprawie tego co on robił do tej pory. | 0095-07-31

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 9 | ((210210-milosc-w-rodzie-verlen; 210224-sentiobsesja; 210311-studenci-u-verlenow; 210324-lustrzane-odbicie-eleny; 210331-elena-z-rodu-verlen; 230412-dzwiedzie-poluja-na-ser; 230419-karolinka-nieokielznana-swinka; 230426-mscizab-zabojca-arachnoziemskich-jaszczurow; 230524-ekologia-jaszczurow-w-arachnoziemie)) |
| Apollo Verlen        | 7 | ((210210-milosc-w-rodzie-verlen; 210224-sentiobsesja; 210306-wiktoriata; 210311-studenci-u-verlenow; 230328-niepotrzebny-ratunek-mai; 230412-dzwiedzie-poluja-na-ser; 230419-karolinka-nieokielznana-swinka)) |
| Elena Verlen         | 7 | ((210210-milosc-w-rodzie-verlen; 210311-studenci-u-verlenow; 210324-lustrzane-odbicie-eleny; 210331-elena-z-rodu-verlen; 230412-dzwiedzie-poluja-na-ser; 230419-karolinka-nieokielznana-swinka; 230502-strasznolabedz-atakuje-granice)) |
| Lucjusz Blakenbauer  | 4 | ((210224-sentiobsesja; 210302-umierajaca-farma-biovatow; 210306-wiktoriata; 210324-lustrzane-odbicie-eleny)) |
| Romeo Verlen         | 4 | ((210210-milosc-w-rodzie-verlen; 210324-lustrzane-odbicie-eleny; 210331-elena-z-rodu-verlen; 230328-niepotrzebny-ratunek-mai)) |
| Ula Blakenbauer      | 4 | ((230412-dzwiedzie-poluja-na-ser; 230419-karolinka-nieokielznana-swinka; 230502-strasznolabedz-atakuje-granice; 230524-ekologia-jaszczurow-w-arachnoziemie)) |
| Marcinozaur Verlen   | 3 | ((230412-dzwiedzie-poluja-na-ser; 230419-karolinka-nieokielznana-swinka; 230502-strasznolabedz-atakuje-granice)) |
| Przemysław Czapurt   | 3 | ((210224-sentiobsesja; 210306-wiktoriata; 210324-lustrzane-odbicie-eleny)) |
| Dariusz Blakenbauer  | 2 | ((210311-studenci-u-verlenow; 210331-elena-z-rodu-verlen)) |
| Maja Samszar         | 2 | ((210311-studenci-u-verlenow; 230328-niepotrzebny-ratunek-mai)) |
| Michał Perikas       | 2 | ((210311-studenci-u-verlenow; 210324-lustrzane-odbicie-eleny)) |
| Mściząb Verlen       | 2 | ((230426-mscizab-zabojca-arachnoziemskich-jaszczurow; 230524-ekologia-jaszczurow-w-arachnoziemie)) |
| AJA Szybka Strzała   | 1 | ((230328-niepotrzebny-ratunek-mai)) |
| Aleksander Samszar   | 1 | ((230419-karolinka-nieokielznana-swinka)) |
| Atraksjusz Verlen    | 1 | ((230426-mscizab-zabojca-arachnoziemskich-jaszczurow)) |
| Bonifacy Samszar     | 1 | ((230328-niepotrzebny-ratunek-mai)) |
| Brunhilda Verlen     | 1 | ((210210-milosc-w-rodzie-verlen)) |
| Emmanuelle Gęsiawiec | 1 | ((230426-mscizab-zabojca-arachnoziemskich-jaszczurow)) |
| Fabian Rzelicki      | 1 | ((230524-ekologia-jaszczurow-w-arachnoziemie)) |
| Fantazjusz Verlen    | 1 | ((230419-karolinka-nieokielznana-swinka)) |
| Fiona Szarstasz      | 1 | ((230328-niepotrzebny-ratunek-mai)) |
| Franz Verlen         | 1 | ((210210-milosc-w-rodzie-verlen)) |
| Frezja Amanit        | 1 | ((210302-umierajaca-farma-biovatow)) |
| Karolinus Samszar    | 1 | ((230328-niepotrzebny-ratunek-mai)) |
| Krucjusz Verlen      | 1 | ((210210-milosc-w-rodzie-verlen)) |
| Krystian Blakenbauer | 1 | ((210331-elena-z-rodu-verlen)) |
| Maks Samszar         | 1 | ((230502-strasznolabedz-atakuje-granice)) |
| Marcel Biekakis      | 1 | ((230524-ekologia-jaszczurow-w-arachnoziemie)) |
| Milena Blakenbauer   | 1 | ((210224-sentiobsesja)) |
| Mścigrom Verlen      | 1 | ((210224-sentiobsesja)) |
| Rafał Perikas        | 1 | ((210311-studenci-u-verlenow)) |
| Rufus Samszar        | 1 | ((210311-studenci-u-verlenow)) |
| Selena Walecznik     | 1 | ((210302-umierajaca-farma-biovatow)) |
| Seraf Verlen         | 1 | ((210210-milosc-w-rodzie-verlen)) |
| Sylwia Perikas       | 1 | ((210311-studenci-u-verlenow)) |
| Szymon Kapeć         | 1 | ((230502-strasznolabedz-atakuje-granice)) |
| Tymek Samszar        | 1 | ((230419-karolinka-nieokielznana-swinka)) |
| Wiktor Blakenbauer   | 1 | ((210306-wiktoriata)) |
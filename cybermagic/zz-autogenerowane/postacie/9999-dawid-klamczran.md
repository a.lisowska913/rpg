---
categories: profile
factions: 
owner: public
title: Dawid Klamczran
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190720-klatwa-czarnej-piramidy      | lekarz zwany 'Doc', utrzymał przy życiu wszystkich (co wyglądało na niemożliwe). Wykrył, że Jacek jest Skażony i pod wpływem efemerydy. | 0110-06-04 - 0110-06-06 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Jacek Cisrak         | 1 | ((190720-klatwa-czarnej-piramidy)) |
| Mel Mirsiak          | 1 | ((190720-klatwa-czarnej-piramidy)) |
| Samuel Czałczak      | 1 | ((190720-klatwa-czarnej-piramidy)) |
| Tatiana Cisrak       | 1 | ((190720-klatwa-czarnej-piramidy)) |
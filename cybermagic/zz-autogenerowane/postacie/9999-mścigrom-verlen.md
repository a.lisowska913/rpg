---
categories: profile
factions: 
owner: public
title: Mścigrom Verlen
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210224-sentiobsesja                 | 28 lat, dał się przekonać kuzynowi i wyjechał z Holdu Bastion, przez co naraził Apolla na sentiobsesję. Powinien wiedzieć lepiej. Bardzo spolegliwy i chętny do pomocy swoim. | 0092-08-03 - 0092-08-06 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Apollo Verlen        | 1 | ((210224-sentiobsesja)) |
| Arianna Verlen       | 1 | ((210224-sentiobsesja)) |
| Lucjusz Blakenbauer  | 1 | ((210224-sentiobsesja)) |
| Milena Blakenbauer   | 1 | ((210224-sentiobsesja)) |
| Przemysław Czapurt   | 1 | ((210224-sentiobsesja)) |
| Viorika Verlen       | 1 | ((210224-sentiobsesja)) |
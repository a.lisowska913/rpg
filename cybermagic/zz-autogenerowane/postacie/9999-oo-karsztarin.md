---
categories: profile
factions: 
owner: public
title: OO Karsztarin
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221004-samotna-w-programie-advancer | wrak kosmiczny; stacja; jednostka treningowa dla Orbitera. | 0099-03-22 - 0099-03-31 |
| 221102-astralna-flara-i-porwanie-na-karsztarinie | jednostka treningowa, na której doszło do próby porwania wartościowego kadeta przez Syndykat Aureliona. Wprowadzili relay sygnałów + załatwili strażników. | 0100-05-30 - 0100-06-05 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Elena Verlen         | 2 | ((221004-samotna-w-programie-advancer; 221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Ada Wyrocznik        | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Alezja Dumorin       | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Arianna Verlen       | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Arnulf Perikas       | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Daria Czarnewik      | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Gerwazy Kircznik     | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Grażyna Burgacz      | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Hubert Kerwelenios   | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Hubert Mirsz         | 1 | ((221004-samotna-w-programie-advancer)) |
| Kacper Wentel        | 1 | ((221004-samotna-w-programie-advancer)) |
| Kajetan Kircznik     | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Klarysa Jirnik       | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Lara Kiriczko        | 1 | ((221004-samotna-w-programie-advancer)) |
| Maja Samszar         | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Mariusz Bulterier    | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Michał Warkoczak     | 1 | ((221004-samotna-w-programie-advancer)) |
| OO Optymistyczny Żuk | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Sandra Kantarelo     | 1 | ((221004-samotna-w-programie-advancer)) |
| Szczepan Myrczek     | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
| Tymon Krakdacz       | 1 | ((221004-samotna-w-programie-advancer)) |
| Władawiec Diakon     | 1 | ((221102-astralna-flara-i-porwanie-na-karsztarinie)) |
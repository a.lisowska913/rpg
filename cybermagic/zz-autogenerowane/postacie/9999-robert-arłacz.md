---
categories: profile
factions: 
owner: public
title: Robert Arłacz
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190813-niesmiertelny-komandos-saitaera | młody chłopak cierpiący na terminalną chorobę; wstrzyknięto mu nanitkową krew Ceruleana by go uratować. Porwany przez Ceruleana i uratowany przez Zespół; najpewniej zostanie Mausem. | 0110-05-20 - 0110-05-25 |
| 201021-noktianie-rodu-arlacz        | następca Arłaczy, kontroluje sentisieć, dokuczał Dianie - za co ona użyła Eustachego do wysadzenia jego altanki. Wymaga transfuzji bo Skażenie ixionem. | 0111-01-07 - 0111-01-10 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 201021-noktianie-rodu-arlacz        | wymaga ciągłych transfuzji (dwóch dziennie) by nie transformować w saitisa; ma niesamowitą kontrolę nad sentisiecią. | 0111-01-10

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Maria Gołąb          | 2 | ((190813-niesmiertelny-komandos-saitaera; 201021-noktianie-rodu-arlacz)) |
| Alfred Cerulean      | 1 | ((190813-niesmiertelny-komandos-saitaera)) |
| Anastazja Sowińska   | 1 | ((201021-noktianie-rodu-arlacz)) |
| Arianna Verlen       | 1 | ((201021-noktianie-rodu-arlacz)) |
| Ataienne             | 1 | ((201021-noktianie-rodu-arlacz)) |
| Diana Arłacz         | 1 | ((201021-noktianie-rodu-arlacz)) |
| Eliza Ira            | 1 | ((201021-noktianie-rodu-arlacz)) |
| Eustachy Korkoran    | 1 | ((201021-noktianie-rodu-arlacz)) |
| Izabela Zarantel     | 1 | ((201021-noktianie-rodu-arlacz)) |
| Jolanta Arłacz       | 1 | ((201021-noktianie-rodu-arlacz)) |
| Juliusz Sowiński     | 1 | ((201021-noktianie-rodu-arlacz)) |
| Klaudia Stryk        | 1 | ((201021-noktianie-rodu-arlacz)) |
| Klaus Rumak          | 1 | ((201021-noktianie-rodu-arlacz)) |
| OO Szalony Rumak     | 1 | ((201021-noktianie-rodu-arlacz)) |
| OO Wesoły Wieprzek   | 1 | ((201021-noktianie-rodu-arlacz)) |
| Robert Garwen        | 1 | ((190813-niesmiertelny-komandos-saitaera)) |
| Wanessa Pyszcz       | 1 | ((201021-noktianie-rodu-arlacz)) |
---
categories: profile
factions: 
owner: public
title: Alicja Trawlis
---

# {{ page.title }}


# Generated: 



## Fiszki


* uciekła Rekinowi; laska od pracy i tymczasowy szczur uliczny (19). | @ 230303-the-goose-from-hell
* (ENCAO:  -0+-- |Bezbarwny;;Agresywny;;Bardzo dużo się przygotowuje| VALS: Self-direction, Security| DRIVE: Uciec Rekinom) | @ 230303-the-goose-from-hell
* "Nie wrócę do niego. Poradzę sobie sama." | @ 230303-the-goose-from-hell
* styl: _predator - prey, 0 - 100_ | @ 230303-the-goose-from-hell

### Wątki


akademia-magiczna-zaczestwa

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230303-the-goose-from-hell          | She is 19; escaped a Shark and hides in the ruined building in Zaczęstwo. Not a mage, but a competent street rat. Avoids mages and strongly dislikes them. | 0111-10-24 - 0111-10-26 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Alex Deverien        | 1 | ((230303-the-goose-from-hell)) |
| Carmen Deverien      | 1 | ((230303-the-goose-from-hell)) |
| Julia Kardolin       | 1 | ((230303-the-goose-from-hell)) |
| kot-pacyfikator Tobias | 1 | ((230303-the-goose-from-hell)) |
| Paweł Szprotka       | 1 | ((230303-the-goose-from-hell)) |
| Teresa Mieralit      | 1 | ((230303-the-goose-from-hell)) |
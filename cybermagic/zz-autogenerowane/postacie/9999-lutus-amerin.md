---
categories: profile
factions: 
owner: public
title: Lutus Amerin
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 211222-kult-saitaera-w-neotik       | tymczasowy kapitan Tivr, kiedyś noktianin. Przyleciał z Klaudią Tivrem na Neotik by wesprzeć Ariannę. | 0112-02-24 - 0112-02-25 |
| 220105-to-nie-pulapka-na-nereide    | trwały kapitan Tivr; obronił Tivr przed anomaliami i magią. Tivr i Lutus mają nieskończoną wiarę w Ariannę i jej moralność. | 0112-02-28 - 0112-03-04 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 2 | ((211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide)) |
| Elena Verlen         | 2 | ((211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide)) |
| Eustachy Korkoran    | 2 | ((211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide)) |
| Izabela Zarantel     | 2 | ((211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide)) |
| Klaudia Stryk        | 2 | ((211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide)) |
| Maria Naavas         | 2 | ((211222-kult-saitaera-w-neotik; 220105-to-nie-pulapka-na-nereide)) |
| Adam Szarjan         | 1 | ((211222-kult-saitaera-w-neotik)) |
| Diana d'Infernia     | 1 | ((211222-kult-saitaera-w-neotik)) |
| Kamil Lyraczek       | 1 | ((220105-to-nie-pulapka-na-nereide)) |
| Kasandra Destrukcja Diakon | 1 | ((211222-kult-saitaera-w-neotik)) |
| Maciek Kalmarzec     | 1 | ((211222-kult-saitaera-w-neotik)) |
| Natalia Aradin       | 1 | ((220105-to-nie-pulapka-na-nereide)) |
| Saitaer              | 1 | ((220105-to-nie-pulapka-na-nereide)) |
---
categories: profile
factions: 
owner: public
title: Eleonora Rdeść
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190912-rexpapier-i-wloknin          | nieludzko lojalna wobec Jarka; uratował ją kiedyś. Jest skłonna wziąć całą winę na siebie by tylko jemu nie stała się krzywda. | 0110-06-17 - 0110-06-21 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Anna Warlank         | 1 | ((190912-rexpapier-i-wloknin)) |
| Dariusz Kuromin      | 1 | ((190912-rexpapier-i-wloknin)) |
| Jarek Gułanczak      | 1 | ((190912-rexpapier-i-wloknin)) |
| Kasjopea Maus        | 1 | ((190912-rexpapier-i-wloknin)) |
| Ksenia Kirallen      | 1 | ((190912-rexpapier-i-wloknin)) |
| Mateusz Urszank      | 1 | ((190912-rexpapier-i-wloknin)) |
---
categories: profile
factions: 
owner: public
title: SC Królowa Przygód
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220327-wskrzeszenie-krolowej-przygod | średni transportowiec na którym znajduje się TAI Miranda Ceres. Ma sekret. Aktualnie: w ruinie, będzie odbudowany przez blakvelowców. | 0108-02-25 - 0108-03-11 |
| 220329-mlodociani-i-pirat-na-krolowej | naprawiona przez blakvelowców, przeprowadziła maiden voyage przez Asimear i po zestrzeleniu kilku Strachów uczestniczyła w akcji destrukcji piratów Berdysza. | 0108-06-22 - 0108-07-03 |
| 220405-lepsza-kariera-dla-romki     | ma zakaz odlotu; znajduje się chwilowo w głównej bazie Blakvelowców niedaleko Szamunczak. Nadal baza Zespołu. | 0108-07-09 - 0108-07-11 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 220329-mlodociani-i-pirat-na-krolowej | dostaje do załogi nowego psychotronika (Wojciech Kaznodzieja) i medyka (Helena Banbadan). Ale najpewniej opuści ją Seweryn Grzęźlik. | 0108-07-03

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Miranda Ceres        | 3 | ((220327-wskrzeszenie-krolowej-przygod; 220329-mlodociani-i-pirat-na-krolowej; 220405-lepsza-kariera-dla-romki)) |
| Anna Szrakt          | 2 | ((220329-mlodociani-i-pirat-na-krolowej; 220405-lepsza-kariera-dla-romki)) |
| Damian Szczugor      | 2 | ((220327-wskrzeszenie-krolowej-przygod; 220405-lepsza-kariera-dla-romki)) |
| Gotard Kicjusz       | 2 | ((220327-wskrzeszenie-krolowej-przygod; 220329-mlodociani-i-pirat-na-krolowej)) |
| Helena Banbadan      | 2 | ((220329-mlodociani-i-pirat-na-krolowej; 220405-lepsza-kariera-dla-romki)) |
| Łucjan Torwold       | 2 | ((220327-wskrzeszenie-krolowej-przygod; 220329-mlodociani-i-pirat-na-krolowej)) |
| Prokop Umarkon       | 2 | ((220327-wskrzeszenie-krolowej-przygod; 220329-mlodociani-i-pirat-na-krolowej)) |
| Romana Kundel        | 2 | ((220329-mlodociani-i-pirat-na-krolowej; 220405-lepsza-kariera-dla-romki)) |
| Seweryn Grzęźlik     | 2 | ((220327-wskrzeszenie-krolowej-przygod; 220329-mlodociani-i-pirat-na-krolowej)) |
| Wojciech Kaznodzieja | 2 | ((220329-mlodociani-i-pirat-na-krolowej; 220405-lepsza-kariera-dla-romki)) |
| Antos Kuramin        | 1 | ((220405-lepsza-kariera-dla-romki)) |
| Bartek Wudrak        | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Berdysz Rozdzieracz  | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Kara Prazdnik        | 1 | ((220329-mlodociani-i-pirat-na-krolowej)) |
| Ursyn Uszat          | 1 | ((220405-lepsza-kariera-dla-romki)) |
---
categories: profile
factions: 
owner: public
title: Michał Teriakin
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210630-listy-od-fanow               | 12-latek, który napisał fanmail. Przekształcony w Pilota Eternijskiego przez cyber-laboratoria na K1. Wierzył, że Arianna może go uratować - i dzięki temu (?) sygnał do niej dotarł. | 0111-11-10 - 0111-11-13 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 1 | ((210630-listy-od-fanow)) |
| Bogdan Anatael       | 1 | ((210630-listy-od-fanow)) |
| Elena Verlen         | 1 | ((210630-listy-od-fanow)) |
| Izabela Zarantel     | 1 | ((210630-listy-od-fanow)) |
| Klaudia Stryk        | 1 | ((210630-listy-od-fanow)) |
| OE Lord Savaron      | 1 | ((210630-listy-od-fanow)) |
| Olgierd Drongon      | 1 | ((210630-listy-od-fanow)) |
| Rafael Galwarn       | 1 | ((210630-listy-od-fanow)) |
| Remigiusz Falorin    | 1 | ((210630-listy-od-fanow)) |
| TAI Rzieza d'K1      | 1 | ((210630-listy-od-fanow)) |
| TAI XT-723 d'K1      | 1 | ((210630-listy-od-fanow)) |
| TAI Zefiris          | 1 | ((210630-listy-od-fanow)) |
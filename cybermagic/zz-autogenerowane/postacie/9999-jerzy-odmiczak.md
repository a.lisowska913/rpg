---
categories: profile
factions: 
owner: public
title: Jerzy Odmiczak
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220420-samobojstwo-kapitana-wielkiego-weza | były kapitan Wielkiego Węża; popełnił samobójstwo pod wpływem kill agenta memetycznego. Nigdy nie zabiłby się "tak po prostu", chronił załogę. Ale przed czym? KIA. | 0108-07-20 - 0108-07-26 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Alan Falkam          | 1 | ((220420-samobojstwo-kapitana-wielkiego-weza)) |
| Antoni Krutacz       | 1 | ((220420-samobojstwo-kapitana-wielkiego-weza)) |
| Berdysz Rozpruwacz   | 1 | ((220420-samobojstwo-kapitana-wielkiego-weza)) |
| Filip Gościc         | 1 | ((220420-samobojstwo-kapitana-wielkiego-weza)) |
| Kornelia Sanoros     | 1 | ((220420-samobojstwo-kapitana-wielkiego-weza)) |
| Lila Cziras          | 1 | ((220420-samobojstwo-kapitana-wielkiego-weza)) |
| Maja Kormoran        | 1 | ((220420-samobojstwo-kapitana-wielkiego-weza)) |
| Pola Mornak          | 1 | ((220420-samobojstwo-kapitana-wielkiego-weza)) |
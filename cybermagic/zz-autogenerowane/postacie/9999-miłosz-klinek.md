---
categories: profile
factions: 
owner: public
title: Miłosz Klinek
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230111-gdy-hr-reedukuje-niewlasciwa-osobe | świetny pilot korwety z załogi Loricatus i dobry advancer; dotarł do śladów zniszczeń po Nox Ignis i przekazał Athamarein i Flarze informacje. Po czym padł. Ranny i przechwycony przez jakąś siłę. | 0100-09-15 - 0100-09-18 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Adragain Ferrias     | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| AK Nox Ignis         | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Aleksy Sartaran      | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Arianna Verlen       | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Daria Czarnewik      | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Elena Verlen         | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Grażyna Burgacz      | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Kajetan Kircznik     | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Lana Mirkinin        | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Leszek Kurzmin       | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Maja Samszar         | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| OO Astralna Flara    | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| OO Athamarein        | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| OO Loricatus         | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Persefona d'Loricatus | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Sabrina Ferrias      | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Salazar Bolza        | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Szczepan Myrczek     | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
| Władawiec Diakon     | 1 | ((230111-gdy-hr-reedukuje-niewlasciwa-osobe)) |
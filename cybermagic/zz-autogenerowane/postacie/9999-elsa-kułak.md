---
categories: profile
factions: 
owner: public
title: Elsa Kułak
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210820-fecundatis-w-domenie-barana  | dowodzi społecznością Szamunczaka, chwilowo zagubiona i pije na smutno. Nie wie jak odeprzeć Blakvelową potęgę i straszliwe Strachy... zwierza się Brightonowi. | 0108-12-30 - 0109-01-02 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Antonella Temaris    | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Antos Kuramin        | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Bruno Baran          | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Cień Brighton        | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Damian Szczugor      | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Deneb Ira            | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Flawia Blakenbauer   | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Jolanta Sowińska     | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Kara Szamun          | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Leon Kantor          | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Rick Varias          | 1 | ((210820-fecundatis-w-domenie-barana)) |
| Tamara Mardius       | 1 | ((210820-fecundatis-w-domenie-barana)) |
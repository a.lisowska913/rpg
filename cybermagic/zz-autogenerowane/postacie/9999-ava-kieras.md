---
categories: profile
factions: 
owner: public
title: Ava Kieras
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221006-ona-chce-dziecko-eustachego  | Exerinn; PR Exerinna i od załadunku. (28) Załatwiła z Eustachym i Ardillą oddanie Robaków (jak chcą), geny Eustachego i by Eustachy pomógł im pokonać Farighanów i odzyskać swoich ludzi. Zamiast jednego oka wszczep typu oko muchy. | 0092-09-20 - 0092-09-24 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ardilla Korkoran     | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| Emban Dolamor        | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| Eustachy Korkoran    | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| Lerten Kieras        | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| Maks Selert          | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| Michał Kervendal     | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| OO Infernia          | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| Staszek Zakraton     | 1 | ((221006-ona-chce-dziecko-eustachego)) |
| VN Exerinn           | 1 | ((221006-ona-chce-dziecko-eustachego)) |
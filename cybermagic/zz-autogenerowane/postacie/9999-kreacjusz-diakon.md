---
categories: profile
factions: 
owner: public
title: Kreacjusz Diakon
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 181230-uwiezienie-saitaera          | znajdujący się w Podwiercie właściciel niewielkiej hodowli Diakońskiej. Poprosił Pięknotkę o pomoc w ratowaniu ludzi w kopalni Podwiert. | 0109-12-06 - 0109-12-08 |
| 200502-po-co-atakuja-minerwe        | kuzyn Pięknotki; dostarczył jej biolab do dyskretnej analizy Infiltratora. Oczekiwał Infiltratora jako zapłaty; alas, Infiltrator uciekł. | 0110-08-22 - 0110-08-24 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 181225-czyszczenie-toksycznych-zwiazkow | Saitaer przejął nad nim kontrolę; stał się agentem Saitaera. | 0109-11-16

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Minerwa Metalia      | 2 | ((181230-uwiezienie-saitaera; 200502-po-co-atakuja-minerwe)) |
| Pięknotka Diakon     | 2 | ((181230-uwiezienie-saitaera; 200502-po-co-atakuja-minerwe)) |
| Feliks Mirtan        | 1 | ((200502-po-co-atakuja-minerwe)) |
| Halina Sermniek      | 1 | ((200502-po-co-atakuja-minerwe)) |
| Karla Mrozik         | 1 | ((181230-uwiezienie-saitaera)) |
| Lucjusz Blakenbauer  | 1 | ((181230-uwiezienie-saitaera)) |
| Nikodem Larwent      | 1 | ((200502-po-co-atakuja-minerwe)) |
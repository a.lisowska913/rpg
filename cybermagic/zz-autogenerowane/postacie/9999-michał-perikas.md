---
categories: profile
factions: 
owner: public
title: Michał Perikas
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210311-studenci-u-verlenow          | 17 lat, zdolniejszy i sprytniejszy od Rafała; podłożył lustro i wymanewrował Elenę, podkładając jej drugie lustro do pokoju. Bardziej sprytny. Słabość do twardych dziewczyn ;-). | 0096-11-18 - 0096-11-24 |
| 210324-lustrzane-odbicie-eleny      | gdy był na akcji z Arianną i Vioriką, został zaatakowany przez Lustrzanego Golema Eleny. Ranny, uratowany przez Viorikę i Lucjusza, potem Lucjusz na dwa tygodnie zmienił go w Ropucha by jego wzór się nie uszkodził. | 0097-07-05 - 0097-07-08 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 210324-lustrzane-odbicie-eleny      | zmieniony w Ropucha na 2 tygodnie, ciężka regeneracja. Bo lustrzany golem Eleny mógłby uszkodzić jego wzór. | 0097-07-08

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Arianna Verlen       | 2 | ((210311-studenci-u-verlenow; 210324-lustrzane-odbicie-eleny)) |
| Elena Verlen         | 2 | ((210311-studenci-u-verlenow; 210324-lustrzane-odbicie-eleny)) |
| Viorika Verlen       | 2 | ((210311-studenci-u-verlenow; 210324-lustrzane-odbicie-eleny)) |
| Apollo Verlen        | 1 | ((210311-studenci-u-verlenow)) |
| Dariusz Blakenbauer  | 1 | ((210311-studenci-u-verlenow)) |
| Lucjusz Blakenbauer  | 1 | ((210324-lustrzane-odbicie-eleny)) |
| Maja Samszar         | 1 | ((210311-studenci-u-verlenow)) |
| Przemysław Czapurt   | 1 | ((210324-lustrzane-odbicie-eleny)) |
| Rafał Perikas        | 1 | ((210311-studenci-u-verlenow)) |
| Romeo Verlen         | 1 | ((210324-lustrzane-odbicie-eleny)) |
| Rufus Samszar        | 1 | ((210311-studenci-u-verlenow)) |
| Sylwia Perikas       | 1 | ((210311-studenci-u-verlenow)) |
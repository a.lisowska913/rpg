---
categories: profile
factions: 
owner: public
title: Ludwik Trójkadur
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221113-ailira-niezalezna-handlarka-woda | (ENCAO: +0-+0 | Pomysłowy;;Idealistyczny| VALS: Power, Family, Security | DRIVE: Impress the superior) (sybrianin). Jeden z Trójbraci, którzy próbują wejść na rynek części z Anomalii Kolapsu po odświeżeniu. Bardzo tanie części (40-50%), ale max. 66-80% pewności i ufności. Daria chciała im pomóc i część ich elementów dodała do Hiyori po uczciwej niskiej cenie. | 0090-04-24 - 0090-04-30 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 221113-ailira-niezalezna-handlarka-woda | wie, jak bardzo Daria pomogła im w rozkręceniu biznesu przez wzięcie części do Hiyori i dobre przeliczenie na co można sobie pozwolić a na co nie. | 0090-04-30

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ailira Niiris        | 1 | ((221113-ailira-niezalezna-handlarka-woda)) |
| Daria Czarnewik      | 1 | ((221113-ailira-niezalezna-handlarka-woda)) |
| Filip Szukurkor      | 1 | ((221113-ailira-niezalezna-handlarka-woda)) |
| Iga Mikikot          | 1 | ((221113-ailira-niezalezna-handlarka-woda)) |
| Jakub Uprzężnik      | 1 | ((221113-ailira-niezalezna-handlarka-woda)) |
| Julia Karnit         | 1 | ((221113-ailira-niezalezna-handlarka-woda)) |
| Kaspian Certisarius  | 1 | ((221113-ailira-niezalezna-handlarka-woda)) |
| Leo Kasztop          | 1 | ((221113-ailira-niezalezna-handlarka-woda)) |
| Nastia Barbatov      | 1 | ((221113-ailira-niezalezna-handlarka-woda)) |
| Patryk Lapszyn       | 1 | ((221113-ailira-niezalezna-handlarka-woda)) |
| Safira d'Hiyori      | 1 | ((221113-ailira-niezalezna-handlarka-woda)) |
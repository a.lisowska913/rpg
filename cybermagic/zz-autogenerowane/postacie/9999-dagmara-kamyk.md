---
categories: profile
factions: 
owner: public
title: Dagmara Kamyk
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210904-broszka-dla-eternianki       | właścicielka Toru Wyścigowego w Klamży; wymieniła starą broszkę na eternijskie ozdoby dzięki czemu wzrosła w popularności. Kobieca, troszkę próżna, ale dobrotliwa. Trochę oczarowana przez Martyna. Trochę. | 0075-09-10 - 0075-09-21 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Jolanta Kopiec       | 1 | ((210904-broszka-dla-eternianki)) |
| Kalina Rota d'Kopiec | 1 | ((210904-broszka-dla-eternianki)) |
| Martyn Hiwasser      | 1 | ((210904-broszka-dla-eternianki)) |
| Ola Klamka           | 1 | ((210904-broszka-dla-eternianki)) |
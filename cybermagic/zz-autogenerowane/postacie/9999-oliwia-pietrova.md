---
categories: profile
factions: 
owner: public
title: Oliwia Pietrova
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210108-ratunkowa-misja-goldariona   | poprzednia advancerka Goldariona/ArcheoPrzędz. Złapana i torturowana przez drakolitów, wprowadziła groźny wirus w Eidolona. Opuściła Goldariona po tej akcji. | 0111-06-11 - 0111-06-17 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Adam Permin          | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Aleksander Leszert   | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Elena Verlen         | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Feliks Przędz        | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Kamil Frederico      | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Klaudia Stryk        | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Martyn Hiwasser      | 1 | ((210108-ratunkowa-misja-goldariona)) |
| SCA Goldarion        | 1 | ((210108-ratunkowa-misja-goldariona)) |
| Semla d'Goldarion    | 1 | ((210108-ratunkowa-misja-goldariona)) |
| SL Uśmiechnięta      | 1 | ((210108-ratunkowa-misja-goldariona)) |
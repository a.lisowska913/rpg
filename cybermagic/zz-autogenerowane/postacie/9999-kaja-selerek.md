---
categories: profile
factions: 
owner: public
title: Kaja Selerek
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 190402-eksperymentalny-power-suit   | wybitna konstruktorka power suitów z Orbitera. Oddała Pięknotce swoje dzieło - "Cień". Skończyła w szpitalu, pocięta przez Cienia. | 0110-03-11 - 0110-03-13 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ataienne             | 1 | ((190402-eksperymentalny-power-suit)) |
| Erwin Galilien       | 1 | ((190402-eksperymentalny-power-suit)) |
| Minerwa Metalia      | 1 | ((190402-eksperymentalny-power-suit)) |
| Pięknotka Diakon     | 1 | ((190402-eksperymentalny-power-suit)) |
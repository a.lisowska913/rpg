---
categories: profile
factions: 
owner: public
title: Filip Szczatken
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 191218-kijara-corka-szotaron        | po tym, jak mafia uratowała jego rodzinę przed Esuriit, własnymi i ciężkimi stratami, został im lojalny i oddał dowodzenie. | 0110-07-04 - 0110-07-07 |
| 191123-echo-eszary-na-dorszancie    | były dowódca stacji. Zaczął współpracować z mafią odkąd zagrozili jego rodzinie. Więc chyba dobrze się skończyło..? | 0110-07-11 - 0110-07-14 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Eszara d'Dorszant    | 2 | ((191123-echo-eszary-na-dorszancie; 191218-kijara-corka-szotaron)) |
| Rafał Kirlat         | 2 | ((191123-echo-eszary-na-dorszancie; 191218-kijara-corka-szotaron)) |
| Azonia Arris         | 1 | ((191218-kijara-corka-szotaron)) |
| Felicja Taranit      | 1 | ((191218-kijara-corka-szotaron)) |
| Gerwazy Kruczkut     | 1 | ((191123-echo-eszary-na-dorszancie)) |
| Hestia Ain d'Dorszant | 1 | ((191123-echo-eszary-na-dorszancie)) |
| Hestia Dis d'Dorszant | 1 | ((191123-echo-eszary-na-dorszancie)) |
| Jaromir Uczkram      | 1 | ((191218-kijara-corka-szotaron)) |
| Kamelia Termit       | 1 | ((191123-echo-eszary-na-dorszancie)) |
| Kijara d'Esuriit     | 1 | ((191218-kijara-corka-szotaron)) |
| Oliwier Pszteng      | 1 | ((191218-kijara-corka-szotaron)) |
| Szymon Szelmer       | 1 | ((191218-kijara-corka-szotaron)) |
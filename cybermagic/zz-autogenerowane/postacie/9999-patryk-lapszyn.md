---
categories: profile
factions: 
owner: public
title: Patryk Lapszyn
---

# {{ page.title }}


# Generated: 



## Fiszki


* 28 lat, puryfikator + scientist; nie mag (sybrianin); próbuje wejść Idze lub Darii do majtek | @ 221022-derelict-okarantis-wejscie
* ENCAO:  00-+- |Tendencje do podkradania;;Pomaga innym| VALS: Self-direction, Stimulation >> Security| DRIVE: Wolność od innych | @ 221022-derelict-okarantis-wejscie

### Wątki


historia_darii
salvagerzy_anomalii_kolapsu

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 221022-derelict-okarantis-wejscie   | 28 lat, puryfikator + scientist; nie mag (sybrianin); próbuje wejść Idze lub Darii do majtek; |ENCAO: 00-+- |Tendencje do podkradania;;Pomaga innym| VALS: Self-direction, Stimulation >> Security| DRIVE: Wolność od innych|; | 0090-01-03 - 0090-01-14 |
| 221111-niebezpieczna-woda-na-hiyori | pomagał Darii w kalibracji detektorów BIO, przeczytał instrukcję. Potem z Darią pomagał w kalibracji Hiyori by pozbyć się 'węgorzy'. | 0090-04-19 - 0090-04-23 |
| 221113-ailira-niezalezna-handlarka-woda | nie miał wiele roboty; asystował Leo w sprawach dobrej jednostki dla Hiyori i wkręcał się w łaski Leo. | 0090-04-24 - 0090-04-30 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Daria Czarnewik      | 3 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda)) |
| Iga Mikikot          | 3 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda)) |
| Jakub Uprzężnik      | 3 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda)) |
| Kaspian Certisarius  | 3 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda)) |
| Leo Kasztop          | 3 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda)) |
| Nastia Barbatov      | 3 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda)) |
| Safira d'Hiyori      | 3 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda)) |
| Ailira Niiris        | 2 | ((221111-niebezpieczna-woda-na-hiyori; 221113-ailira-niezalezna-handlarka-woda)) |
| Ogden Barbatov       | 2 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori)) |
| SCA Hiyori           | 2 | ((221022-derelict-okarantis-wejscie; 221111-niebezpieczna-woda-na-hiyori)) |
| Filip Szukurkor      | 1 | ((221113-ailira-niezalezna-handlarka-woda)) |
| Julia Karnit         | 1 | ((221113-ailira-niezalezna-handlarka-woda)) |
| Ludwik Trójkadur     | 1 | ((221113-ailira-niezalezna-handlarka-woda)) |
---
categories: profile
factions: 
owner: public
title: Berenika Roldan
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 211229-szpieg-szpiegowi-szpiegiem   | szpieg z Eterni w Neotik jako technik; specjalistka od zamków, włamów itp. Ukryła przed Rolandem fakt, że jest magiem. Do samego końca wierzył, że Berenika jest po prostu zwykłym technikiem. Zdobyła informacje, których szukała. Niechcący ożywiła Hestię stacji. | 0111-01-03 - 0111-01-07 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 211229-szpieg-szpiegowi-szpiegiem   | szacun za dobrze zrobioną akcję i informacę o innym agencie - choć nie byli w stanie go namierzyć | 0111-01-07

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Estella Evans        | 1 | ((211229-szpieg-szpiegowi-szpiegiem)) |
| Roland Sowiński      | 1 | ((211229-szpieg-szpiegowi-szpiegiem)) |
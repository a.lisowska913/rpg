---
categories: profile
factions: 
owner: public
title: Triana Porzecznik
---

# {{ page.title }}


# Read: 

## Kim jest

### W kilku zdaniach

Bardzo ambitna młoda konstruktorka, próbująca dorównać sławie i potędze ojca i babci. Często wpada w straszne kłopoty, próbując się jakoś wykazać. Kochliwa i romantyczna, zawsze jest przemęczona kolejnymi działaniami. Nie włada potężną magią dynamiczną, ale jej narzędzia są świetnym fokusem. Ku jej utrapieniu, jest lepszym dekonstruktorem niż wynalazcą.

### Co się rzuca w oczy

* Zawsze ma pełno roboty i wystające jakieś dziwne urządzenia z nadmiarowych kieszeni.
* Dobrze ubrana modnisia, ale bardzo rzadko naprawdę czysta; tu trochę smaru, tu kurz.
* Całkowicie rozkleja się na filmach; nie toleruje cierpienia - a zwłaszcza zwierząt.
* Bardzo przyjacielska; łatwo się zaprzyjaźnia i chętnie pomaga. Nie trzyma uraz.
* Jej głównym zmysłem jest dotyk; nie ma też szczególnego dystansu - to krępuje sporo osób.
* Zwykle wygląda, jakby nie spała trzy noce. Jej roztrzepanie jedynie wzmacnia ten efekt.
* Nie lubi rywalizacji ani konfliktów, ale zawsze próbuje zrobić coś większego i bardziej efektownego niż kiedyś.

## Mechanika

### Znaczące Czyny i Osiągnięcia (3)

* Używając zrobionych z ojcem dystrybutorów energii magicznej schowała przed terminusami anomalnego kiciusia z Trzęsawiska którego szukali; prawie Skaziła małe osiedle. 
* W projekt na dwudniowym hackatonie wsadziła 41 godzin - tylko po to, by zaspać w dniu oddania projektu i skończyć z dyskwalifikacją.
* Na imprezie przez moment została królową imprezy - ale szybko oddała tytuł koleżance, której na tym bardzo zależało, by się lepiej bawić.

### Co się rzuca w oczy: Atuty i Przewagi (3, 6)

* PRACA: Majsterkowanie i gadżety - szycie, składanie domku, elektronika. Złota rączka - złoży i naprawi sporo.
* PRACA: Efekty specjalne i robienie piorunującego wrażenia - strojem, światłem, nastrojem.
* SERCE: Do każdego problemu zaproponować jeszcze bardziej efektowną i złożoną maszynę. W końcu jest z rodu wynalazców.
* SERCE: Udowodnić, że jest godna bycia wnuczką swojej babci i córką swego ojca! Każde wyzwanie jest w stanie pokonać! Musi stworzyć epicki wynalazek!
* ATUT: Seksowna i lubi być na świeczniku. Potrafi i lubi robić wrażenie strojem i umiejętnościami.
* ATUT: Ma naturalny talent do dekonstrukcji, znajdowania słabych punktów w konstrukcjach i rozkładania ich na kawałki.

### Charakterystyczne zasoby (3)

* COŚ: Ogromna ilość świetnych starych eksperymentów, planów i narzędzi zrobionych przez jej babcię, imienniczkę.
* KTOŚ: Ojciec - Triana wierzy, że jest on wybitnym naukowcem. W rzeczywistości ojciec Triany, Robinson, jest niezły... ale to wszystko.
* WIEM: ?
* OPINIA: Zdolna i pracowita dziewczyna, która jak się czegoś podejmie to to zrobi. Trochę zwariowana, ale jaki Porzecznik nie jest?

### Typowe problemy z którymi sobie nie radzi (-3)

* CIAŁO: ZAWSZE jest przemęczona i niekoniecznie kontaktuje. Albo myśli ma gdzieś w dziwnym wynalazku, albo nie spała bo sprawdzała teorię.
* SERCE: Przeszacowuje swoje (i innych) umiejętności i nieco lekko podchodzi do zagrożeń; podejmuje się za dużych wyzwań.
* SERCE: Romantyczna ponad miarę. Zawsze szuka swojej efektownej prawdziwej miłości. Skłonna do poświęceń i efektów specjalnych.
* SERCE: Kocha zwierzęta. Za żadne skarby nie dopuści do cierpienia czy Skażenia jakiegokolwiek zwierzaka.
* STRACH: Uważa, że nie jest dość dobra - nie dorównuje do innych Porzeczników. Musi być lepsza! Musi się wykazać!
* MAGIA: Kiepsko kontroluje magię dynamiczną; potrzebuje narzędzi i urządzeń jako fokusa.
* UMYSŁ: Zwykle zapomina wszystko - a zwłaszcza to, że coś komuś obiecała jak nie jest to istotne.

### Specjalne

* .

## Inne

### Wygląd

.

### Coś Więcej

* .

### Endgame

* .


# Generated: 



## Fiszki


* kochliwa i romantyczna konstruktorka która jest świetnie ubrana i lubi dotykać ludzi. | @ 230212-pierwszy-tajemniczy-wielbiciel-liliany
* (ENCAO:  ++0++ | Przyjazna i przytulna;; Roztrzepana;; Wiecznie śpiąca| VALS: Achievement, Family | DRIVE: Pomóc tym co pomocy potrzebują, PUPPY!!!) | @ 230212-pierwszy-tajemniczy-wielbiciel-liliany
* seksowna majsterkowiczka lubiąca być na świeczniku ale jeszcze bardziej lubiąca przyjaciół o talencie do dekonstrukcji | @ 230212-pierwszy-tajemniczy-wielbiciel-liliany
* "Kiedyś dorównam ojcu w konstrukcjach - na pewno mogę zalać problem energią. Tylko muszę wziąć coś by nie spać aż 5 godzin." | @ 230212-pierwszy-tajemniczy-wielbiciel-liliany

### Wątki


rekiny-a-akademia
akademia-magiczna-zaczestwa
aniela-arienik

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 201020-przygoda-randka-i-porwanie   | zaprojektowała double date z Lilianą i Sowińskimi; wyłączyła alarmy w Rezydencji i stworzyła opowieść o Upiorze. Porwana przez pomyłkę przez agentów Zespołu. | 0110-10-21 - 0110-10-23 |
| 210323-grzybopreza                  | odrestaurowała ratunkowego psa wojskowego (K9Epickor), który dał w długą. Pomogła Myrczkowi zrobić super-trufle. Następnie z pomocą Julii wyłączyła K9 i go zwinęła, unikając uwagi Uli (uczennicy terminusa). | 0111-04-18 - 0111-04-19 |
| 210406-potencjalnie-eksterytorialny-seksbot | statystka. 3 miesiące temu zalana, spała i Wkrąż dorysowywał jej kocie uszka. Teraz: gdy potrzebna była interwencja na złomowisku, zakładała i kalibrowała servara za długo i problem się rozwiązał. | 0111-04-23 - 0111-04-24 |
| 210518-porywaczka-miragentow        | chciała oglądać horror a zrobiła ŁZy nad którymi straciła kontrolę (i które obróciły się przeciw wszystkim). Drony mają autonaprawę oprogramowania i w ogóle. | 0111-05-03 - 0111-05-04 |
| 210622-verlenka-na-grzybkach        | pracuje nad robotem kultywacyjnym; zupełnie nie radzi sobie jak jest ostrzeliwana przez Lancera. | 0111-05-26 - 0111-05-27 |
| 230212-pierwszy-tajemniczy-wielbiciel-liliany | AMZ. Chce pomóc Lilianie w zrobieniu servara zdolnego do starcia z Arkadią, ale nie wie jak. Jej zagubiona drona wysłała sygnał SOS bo ludzie są nieprzytomni. Po policzeniu prawdopodobieństw, to jest możliwe i Triana śpi spokojnie. | 0111-10-18 - 0111-10-20 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 210323-grzybopreza                  | pozyskała w miarę sprawnego technopsa militarnego służącego do ratowania ludzi. Nazwała go "K9Epickor". Przy AI pomagała Julia Kardolin. | 0111-04-19
| 210406-potencjalnie-eksterytorialny-seksbot | ma włączony na stałe monitoring Złomowiska w Zaczęstwie. By nikt nie szabrował... lub by coś nieupoważnionego nie wyszło. | 0111-04-24

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Julia Kardolin       | 5 | ((210323-grzybopreza; 210406-potencjalnie-eksterytorialny-seksbot; 210518-porywaczka-miragentow; 210622-verlenka-na-grzybkach; 230212-pierwszy-tajemniczy-wielbiciel-liliany)) |
| Marysia Sowińska     | 4 | ((210323-grzybopreza; 210406-potencjalnie-eksterytorialny-seksbot; 210518-porywaczka-miragentow; 210622-verlenka-na-grzybkach)) |
| Liliana Bankierz     | 3 | ((201020-przygoda-randka-i-porwanie; 210622-verlenka-na-grzybkach; 230212-pierwszy-tajemniczy-wielbiciel-liliany)) |
| Urszula Miłkowicz    | 3 | ((201020-przygoda-randka-i-porwanie; 210323-grzybopreza; 210622-verlenka-na-grzybkach)) |
| Ignacy Myrczek       | 2 | ((210323-grzybopreza; 210622-verlenka-na-grzybkach)) |
| Kacper Bankierz      | 2 | ((201020-przygoda-randka-i-porwanie; 210323-grzybopreza)) |
| Lorena Gwozdnik      | 2 | ((210406-potencjalnie-eksterytorialny-seksbot; 210518-porywaczka-miragentow)) |
| Arkadia Verlen       | 1 | ((210622-verlenka-na-grzybkach)) |
| Barnaba Burgacz      | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Daniel Sowiński      | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Diana Lemurczak      | 1 | ((210518-porywaczka-miragentow)) |
| Feliks Keksik        | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Franek Bulterier     | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Gabriel Ursus        | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Henryk Sowiński      | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Henryk Wkrąż         | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Laura Tesinik        | 1 | ((230212-pierwszy-tajemniczy-wielbiciel-liliany)) |
| Laurencjusz Sorbian  | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Marek Samszar        | 1 | ((210622-verlenka-na-grzybkach)) |
| Mariusz Kupieczka    | 1 | ((230212-pierwszy-tajemniczy-wielbiciel-liliany)) |
| Napoleon Bankierz    | 1 | ((210323-grzybopreza)) |
| Rafał Torszecki      | 1 | ((210622-verlenka-na-grzybkach)) |
| Robert Pakiszon      | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Robinson Porzecznik  | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Różewicz Diakon      | 1 | ((210622-verlenka-na-grzybkach)) |
| Rupert Mysiokornik   | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Sabina Kazitan       | 1 | ((210406-potencjalnie-eksterytorialny-seksbot)) |
| Tomasz Tukan         | 1 | ((201020-przygoda-randka-i-porwanie)) |
| Tymon Grubosz        | 1 | ((201020-przygoda-randka-i-porwanie)) |
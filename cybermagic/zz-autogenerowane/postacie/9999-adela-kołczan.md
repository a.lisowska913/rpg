---
categories: profile
factions: 
owner: public
title: Adela Kołczan
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 210912-medyk-przeciwko-cieniaszczytowi | dziewczyna Adriana i ochroniarz Martyna; ostra żyleta, która pomogła Martynowi na prośbę chłopaka i przez Paradoks Martyna się w nim śmiertelnie zakochała. NADAL nie chce go chronić. Nie da się. HE'S FUCKING CRAZY. | 0079-09-13 - 0079-09-16 |
| 211016-zlamane-serce-martyna        | KIA. Skażona Paradoksem Martyna dawno temu, zabiła Kalinę. Potem udała się robić domek marzeń dla niej i Martyna. Martyn ją zabił przytulając i mówiąc, że wszystko będzie dobrze. | 0085-08-31 - 0085-09-06 |

## Progresja


| Opowieść | Progresja | Końcowa data |
| ---- | ---- | ---- |
| 210912-medyk-przeciwko-cieniaszczytowi | przez Paradoks Martyna Hiwassera śmiertelnie się w nim zakochała. To zniszczyło jej związek z Adrianem Koziołem i doprowadziło do dalszych komplikacji, bo Martyn nie chciał jej wykorzystać. | 0079-09-16

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Adrian Kozioł        | 2 | ((210912-medyk-przeciwko-cieniaszczytowi; 211016-zlamane-serce-martyna)) |
| Martyn Hiwasser      | 2 | ((210912-medyk-przeciwko-cieniaszczytowi; 211016-zlamane-serce-martyna)) |
| Amadeusz Martel d'Kopiec | 1 | ((211016-zlamane-serce-martyna)) |
| Franciszek Tocz      | 1 | ((210912-medyk-przeciwko-cieniaszczytowi)) |
| Irena Czakram        | 1 | ((210912-medyk-przeciwko-cieniaszczytowi)) |
| Jolanta Kopiec       | 1 | ((211016-zlamane-serce-martyna)) |
| Kalina Rota d'Kopiec | 1 | ((211016-zlamane-serce-martyna)) |
| Kasia Pieprz         | 1 | ((210912-medyk-przeciwko-cieniaszczytowi)) |
| Paweł Pieprz         | 1 | ((210912-medyk-przeciwko-cieniaszczytowi)) |
| Wanessa Ogarek       | 1 | ((211016-zlamane-serce-martyna)) |
---
categories: profile
factions: 
owner: public
title: Ralf Tapszecz
---

# {{ page.title }}


# Generated: 



## Fiszki


* mag (domena: DOMINUJĄCA ROZPACZ) i nowy dodatek do Inferni, kultysta Interis. Noktianin, savaranin. | @ 230201-wylaczone-generatory-memoriam-inferni
* (ENCAO:  -00-+ |Bezbarwny, przezroczysty;;Złośliwy;;Buja w obłokach| VALS: Conformity, Humility >> Benevolence| DRIVE: Piękno zniszczenia) | @ 230201-wylaczone-generatory-memoriam-inferni
* "Nasze porażki nie mają znaczenia. Tak czy inaczej zwyciężymy." | @ 230201-wylaczone-generatory-memoriam-inferni
* mag (domena: ROZPACZ + KINEZA) i nowy dodatek do Inferni, kultysta Interis. Noktianin, savaranin. | @ 230315-bardzo-nieudane-porwanie-inferni
* (ENCAO:  -00-+ |Bezbarwny, przezroczysty;;Buja w obłokach| VALS: Conformity, Humility >> Benevolence| DRIVE: Piękno zniszczenia) | @ 230315-bardzo-nieudane-porwanie-inferni
* mag (domena: rozpacz + kineza). Noktianin, savaranin. Pomaga Ardilli, wyraźnie się nią zainteresował (personalnie) i ją lubi. | @ 230329-zdrada-rozrywajaca-arkologie

### Wątki


historia-eustachego
arkologia-nativis
infernia-jej-imieniem
zbrodnie-kidirona

## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230201-wylaczone-generatory-memoriam-inferni | nowy dodatek do Inferni, mag (domena: DOMINUJĄCA ROZPACZ) i kultysta Interis. Noktianin, savaranin. (ENCAO:  -00-+ |Bezbarwny, przezroczysty;;Buja w obłokach| VALS: Conformity, Humility >> Benevolence| DRIVE: Piękno zniszczenia). Świetny w bezszelestności i infiltracji, nigdy się nie skarży, polubił się z Ardillą. Wujek dodał go do Inferni by mu pomóc. | 0093-02-10 - 0093-02-12 |
| 230208-pierwsza-randka-eustachego   | powoli zbliża się do Ardilli. Wyraźnie ma problemy z konfliktem kultury savarańskiej (musisz być użyteczny) a tym co _chce_. Przyznał się Ardilli że widział oczy Nihilusa i to go zmieniło. Wyraźnie chce pomóc innym, ale nie rozumie dlaczego warto pomóc komuś kto nie ma już potencjału - ale CHCE pomóc. Bardzo skonfliktowany. WIE, że mag (Eustachy) nie może być sam. Ma problem z pozycjonowaniem Bartłomieja Korkorana jako bohatera lub głupca marnującego zasoby. | 0093-02-14 - 0093-02-21 |
| 230215-terrorystka-w-ambasadorce    | w jakiś sposób dał radę nawiązać połączenie z Ardillą gdy ona była w Ambasadorce pełznąc korytarzami. Nawiązał link Eustachy - Ardilla i zadbał o bezpieczeństwo wszystkich. | 0093-02-22 - 0093-02-23 |
| 230315-bardzo-nieudane-porwanie-inferni | uczestniczył w akcji ratunkowej (ale przede wszystkim pilnował Ardillę), gdy mostek był zaatakowany to szybkim atakiem zranił dwóch napastników po oaczach. Potem współpraca z Ardillą w sabotowaniu generatorów Memoriam i dywersji. | 0093-03-06 - 0093-03-09 |
| 230329-zdrada-rozrywajaca-arkologie | wiedząc, że pamiętnik Eweliny został upubliczniony i wiedząc, że Ardilli na Ewelinie zależy był w pobliżu Eweliny gdy ta próbowała się zabić. Nie miał siły, by przenieść Ewelinę z Ardillą do bezpiecznego miejsca (savaranin jest słaby fizycznie). Uważa, że Ewelina zachowała się "nieefektywnie". | 0093-03-14 - 0093-03-16 |
| 230614-atak-na-kidirona             | Ardilla podejrzewa go o bycie magiem Nihilusa. On jeszcze nie wie. Gdy doszło do ataku na arkologię, Ralf od razu pojawił się przy Ardilli i chroni ją i tylko ją. Skutecznie zgasił pożar drzew w arkologii, zaczynając od drzew które Ardilla lubi najbardziej. | 0093-03-22 - 0093-03-24 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ardilla Korkoran     | 6 | ((230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230315-bardzo-nieudane-porwanie-inferni; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona)) |
| Eustachy Korkoran    | 6 | ((230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230315-bardzo-nieudane-porwanie-inferni; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona)) |
| Rafał Kidiron        | 6 | ((230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230315-bardzo-nieudane-porwanie-inferni; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona)) |
| Kalia Awiter         | 5 | ((230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona)) |
| OO Infernia          | 5 | ((230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230315-bardzo-nieudane-porwanie-inferni; 230329-zdrada-rozrywajaca-arkologie; 230614-atak-na-kidirona)) |
| Bartłomiej Korkoran  | 4 | ((230201-wylaczone-generatory-memoriam-inferni; 230208-pierwsza-randka-eustachego; 230215-terrorystka-w-ambasadorce; 230614-atak-na-kidirona)) |
| Celina Lertys        | 2 | ((230201-wylaczone-generatory-memoriam-inferni; 230329-zdrada-rozrywajaca-arkologie)) |
| Franciszek Pietraszczyk | 2 | ((230208-pierwsza-randka-eustachego; 230329-zdrada-rozrywajaca-arkologie)) |
| SAN Szare Ostrze     | 2 | ((230315-bardzo-nieudane-porwanie-inferni; 230614-atak-na-kidirona)) |
| Tymon Korkoran       | 2 | ((230201-wylaczone-generatory-memoriam-inferni; 230215-terrorystka-w-ambasadorce)) |
| Wojciech Grzebawron  | 2 | ((230315-bardzo-nieudane-porwanie-inferni; 230329-zdrada-rozrywajaca-arkologie)) |
| Amelia Sarkaldir     | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| BIA Prometeus        | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Ernest Puszczowiec   | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Ewelina Paroknis     | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Feliks Kidiron       | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Hubert Grzebawron    | 1 | ((230315-bardzo-nieudane-porwanie-inferni)) |
| Iwona Paroknis       | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Jan Lertys           | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Jonasz Paroknis      | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Kalista Luminis      | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Katarzyna Falernik   | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Kratos Coruscatis    | 1 | ((230201-wylaczone-generatory-memoriam-inferni)) |
| Lycoris Kidiron      | 1 | ((230614-atak-na-kidirona)) |
| Magda Misteria Sarbanik | 1 | ((230215-terrorystka-w-ambasadorce)) |
| Małgorzata Maratelus | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Marcin Pietraszczyk  | 1 | ((230208-pierwsza-randka-eustachego)) |
| Mariusz Dobrowąs     | 1 | ((230315-bardzo-nieudane-porwanie-inferni)) |
| Nadia Sekernik       | 1 | ((230315-bardzo-nieudane-porwanie-inferni)) |
| Stanisław Uczantor   | 1 | ((230614-atak-na-kidirona)) |
| Szczepan Falernik    | 1 | ((230329-zdrada-rozrywajaca-arkologie)) |
| Tobiasz Lobrak       | 1 | ((230215-terrorystka-w-ambasadorce)) |
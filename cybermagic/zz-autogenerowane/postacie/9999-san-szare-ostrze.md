---
categories: profile
factions: 
owner: public
title: SAN Szare Ostrze
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 230315-bardzo-nieudane-porwanie-inferni | okręt bojowy Kidirona, należy do Arkologii Nativis. Nieobecny, jest w innym miejscu. (rekord by zapamiętać nazwę jednostki) | 0093-03-06 - 0093-03-09 |
| 230614-atak-na-kidirona             | wykorzystana w przeszłości do polowana na noktiańskie dzieci przez Kidirona, co widać na rysunkach noktiańskich dzieci. | 0093-03-22 - 0093-03-24 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Ardilla Korkoran     | 2 | ((230315-bardzo-nieudane-porwanie-inferni; 230614-atak-na-kidirona)) |
| Eustachy Korkoran    | 2 | ((230315-bardzo-nieudane-porwanie-inferni; 230614-atak-na-kidirona)) |
| OO Infernia          | 2 | ((230315-bardzo-nieudane-porwanie-inferni; 230614-atak-na-kidirona)) |
| Rafał Kidiron        | 2 | ((230315-bardzo-nieudane-porwanie-inferni; 230614-atak-na-kidirona)) |
| Ralf Tapszecz        | 2 | ((230315-bardzo-nieudane-porwanie-inferni; 230614-atak-na-kidirona)) |
| Bartłomiej Korkoran  | 1 | ((230614-atak-na-kidirona)) |
| Hubert Grzebawron    | 1 | ((230315-bardzo-nieudane-porwanie-inferni)) |
| Kalia Awiter         | 1 | ((230614-atak-na-kidirona)) |
| Lycoris Kidiron      | 1 | ((230614-atak-na-kidirona)) |
| Mariusz Dobrowąs     | 1 | ((230315-bardzo-nieudane-porwanie-inferni)) |
| Nadia Sekernik       | 1 | ((230315-bardzo-nieudane-porwanie-inferni)) |
| Stanisław Uczantor   | 1 | ((230614-atak-na-kidirona)) |
| Wojciech Grzebawron  | 1 | ((230315-bardzo-nieudane-porwanie-inferni)) |
---
categories: profile
factions: 
owner: public
title: Klaudiusz Zanęcik
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 220925-mlodziaki-na-savaranskim-statku-handlowym | syn oficera Orbitera; chciał uciec z dziewczyną i przeżyć przygodę ale wpakował się na jednostkę savarańską Hektor 13. Wyciągnęła go Klaudia i Fabian. | 0109-05-24 - 0109-05-25 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Dawid Aximar         | 1 | ((220925-mlodziaki-na-savaranskim-statku-handlowym)) |
| Fabian Korneliusz    | 1 | ((220925-mlodziaki-na-savaranskim-statku-handlowym)) |
| Klaudia Stryk        | 1 | ((220925-mlodziaki-na-savaranskim-statku-handlowym)) |
| Martyn Hiwasser      | 1 | ((220925-mlodziaki-na-savaranskim-statku-handlowym)) |
| OO Serbinius         | 1 | ((220925-mlodziaki-na-savaranskim-statku-handlowym)) |
| Perdius Aximar       | 1 | ((220925-mlodziaki-na-savaranskim-statku-handlowym)) |
| SC Hektor 17         | 1 | ((220925-mlodziaki-na-savaranskim-statku-handlowym)) |
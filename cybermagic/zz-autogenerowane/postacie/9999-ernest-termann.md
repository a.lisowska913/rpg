---
categories: profile
factions: 
owner: public
title: Ernest Termann
---

# {{ page.title }}


# Generated: 



## Dokonania


| Opowieść | Dokonanie | Daty |
| ---- | ---- | ---- |
| 211122-czolenkowe-esuriit-w-amz     | dyrektor AMZ przed Arnulfem; starszy mag nie wierzący w technologię (zwłaszcza TAI) a raczej w głęboką magię. Używał Oczu do monitorowania AMZ. Gdy pojawił się Agent Esuriit, skupił się na zwalczaniu go magią i na ratowaniu tylu uczniów ilu jest w stanie, mimo, że to byli ci, którzy się nie ewakuowali. KIA broniąc grupki uczniów. | 0083-10-13 - 0083-10-22 |

## Relacje Aktor - Aktor


| Z kim | Intensywność | Opowieści |
| ---- | ---- | ---- |
| Dariusz Drewniak     | 1 | ((211122-czolenkowe-esuriit-w-amz)) |
| Felicjan Szarak      | 1 | ((211122-czolenkowe-esuriit-w-amz)) |
| Klaudia Stryk        | 1 | ((211122-czolenkowe-esuriit-w-amz)) |
| Ksenia Kirallen      | 1 | ((211122-czolenkowe-esuriit-w-amz)) |
| Mariusz Trzewń       | 1 | ((211122-czolenkowe-esuriit-w-amz)) |
| Wiktor Szurmak       | 1 | ((211122-czolenkowe-esuriit-w-amz)) |